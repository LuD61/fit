#ifdef  IDENT 
static char ident[] = "@(#) fnb.c  VLUGLOTSA   16.06.98 by SE.TEC"
#endif

// 171109 : bissel finetuning

/* 081008 : XP-Probl. loesen  ( tfaktor ) */

/* 140207 : Eich-Block :
123456789012345-> start Netto-Block
A0000058 001002N
: ich erwarte einen bis zu 14 Zeichen langen Block ( evtl. mit inneren blanks )
*/
/***************************************************************************/
/* Programmname :     fnb.c                                                */
/*-------------------------------------------------------------------------*/
/* Funktion :        Applikationsschnittstelle (API)                       */
/*                   Initialisierung der Schnittstelle und des Geraetes    */
/*-------------------------------------------------------------------------*/
/* Revision : 1.01    Datum : 08.07.98  erstellt von :  SE.TEC             */
/*-------------------------------------------------------------------------*/
/* Kurzbeschreibung : Hauptprogramm                                        */
/*                                                                         */
/* - Lesen der Schnittstellenparameter                                     */
/* - Senden der Geraete-Initialisierung, Ergebnisbewertung                 */
/*-------------------------------------------------------------------------*/
/* Deklaration: void main(int argc, char *argv[])                          */
/*                                                                         */                                          
/* Eingabe : par1      :  fnb ->                                           */
/* fnb00 : Init der Schnittstelle und des Geraetes Return : errcode        */
/* fnb02 : Fehlerkorrektur + Errcode                                       */
/* fnb03 : Nullstellen + Errcode                                           */
/* fnb04 : Tarieren + Errcode                                              */
/* fnb05 : Tara loeschen + Errcode                                         */                    
/* fnb06 : Waagenumschaltung                                               */  
                                                 
/* fnb08 : wiegen MIT Ruhewertung (ohne ist Mist !!) Return : Gewichtswert */
/* fnb11 : Postenregistrierung                                             */
/* fnb23 : Tarieren mit Wertvorgabe                                        */
/*                                                                         */
/* Eingabe : par2 : Dialog-Directory : da steht die Schnittstelleninfo !!  */
/* Eingabe : par3 : (OPTIONAL) Inputwert(Fixformat: VZ+5+.+3) (fnb6:1|2|3) */
/* Ausgabe : (OPTIONAL) outdatei ( mit Errcode + Wert )                    */
/*                                                                         */
/* EXIT Status       : TYerrno err: Fehlerstatus                           */
/*       = RET_OK : kein Fehler                                            */
/*       = 1    : Parameterfehler                                          */
/*       = 2    : sonstiger Fehler -> weiter spezifizierbar                */
/* Aufruf : fnb par1 par2 [ par3 ]                                        */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* Aenderungsstand :                                                       */
/* Revision : 1.0  Datum : dd.mm.yyyy  geaendert von :  SE.TEC             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* 180701   : Soehnle- S20 -Variante
/*       STANDARD INCLUDE'S               */

#include "getr_01.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>
#include "paras.h"
#ifdef DLL
#include "fnb.h"
#endif

DCB a_CommDcb ; /* Aktiv */
DCB g_CommDcb ; /* Originalsave */
COMMTIMEOUTS a_CommTmo ;
COMMTIMEOUTS g_CommTmo ;
BOOL fSuccess ;

int testxx=0 ;   /* oder 0 */
int testkk=0 ;   /* oder 0 */
int testschnitt=0  ;
static int schoninit = -1 ;
char wertbuffer [ 120] ;
FILE *fperr ;
char logtext [120] ;
/* externe Deklarationen                 */

extern char dialogdir[] ;
extern int fnb_nr ;
extern int dev_type ;
extern int kommastellen ;
extern int tfaktor ;    /* 081008 */
extern int dalibi ;     // 020109
extern int dfw_ablauf ; // 271108
extern int dtrenner ;   // 161208
extern int dtarfakt ;   // 181208
extern int dtartime ;   // 290310
extern TYedvConf edv_conf ;
extern TYedvChan edv_chan ;
extern void get_defax (void) ;

#define ACKi     0x06
#define NAKi     0x15
#define CRi      0x0d
#define LFi      0x0a
#define SYNi     0x16

int initschnitt ( void ) ;
int fnb0  ( void )       ;  /* init */
int fnb2  ( void )       ;  /* init */
int fnb3  ( void )       ;  /* nullstellen */
int fnb4  ( void )       ;  /* tarieren */
int fnb5  ( void )       ;  /* tara loeschen */
int fnb5n  ( void )       ;  /* tara loeschen fuer s30 */
int fnb5nn  ( void )       ;  /* tara loeschen fuer s20 */
int fnb6  ( int  )       ;  /* waage umschalten */
unsigned fnb7a ( void )  ;
unsigned wagnr ( char *, int) ;
int fnb8  ( void )       ;  /* wiegen */
int fnb11 ( void )       ;  /* Postenreg. */
int fnb23 ( double)      ;  /* Tarieren mit Wert */
int fnb33 ( char *);      // Hilfsfunktion zur Eingabe beliebiger Strings

double inpkonv ( char * ) ;
void logschreib ( void ) ;
u_long eigexit ( int ) ;
int itlgew ( int ) ;
int ds20gew ( int ) ;
long nap (long) ;
void sleep ( unsigned );
char * itltar ( char * , double ) ; 
int wartack ( int ) ;
int quitN ( void ) ;
int quitT ( void ) ;
int leseleer (int) ;
int ergebnis ( double ergebmat[], char *ergebchar[] ) ;

int ergebschreib ;      // 190209

void acksend (void) ;

double wertn ;  /* wert-netto  */
double wertb ;  /* wert-brutto */
double wertt ;  /* wert-tara   */
double werte ;  /* eichspeich  */
char lakeyi [2]  ;
char dialdatin[21] ;

int os_write (HANDLE fd , char *buf , unsigned nbyte ) ;
int os_read  (HANDLE fd , char *buf , unsigned nbyte ) ;

/*-------------------------------------------------------------------------*/

HANDLE schnittstelle;
#ifdef MAINI
void main (int argc , char *argv[] ) 
#else
#ifdef DLL
EXPORT int fnb (int argc, char *argv[])
#else
int fnbi (int argc , char *argv[] )
#endif
#endif
{

  char  * srptr ;
  int strlaenge ;
  int retcode   ;
  retcode = 0 ;
// Identifikation ist wichtig .......... 
  if ( argc < 2 )
  {
     printf ( " 190412 : Ich bin ein FNB fuer SOEHNLE S30 (2761 + S20 )  \n" ) ;
     retcode = eigexit (1) ;

  }

  srptr = getenv("testmode");
  if (srptr == NULL)
  {
     srptr = getenv("testmade");
     if (srptr == NULL)
     {
        testxx = 0;
        testkk = 0;
     }
     else
     {
        testxx = atoi(srptr) ;
        testkk = testxx ;        // atoi(srptr) ;
     }
  }
  else
  {
        testxx = atoi(srptr) ;
        testkk = testxx ;       // atoi(srptr) ;
  }

  if ( testxx > 0 )
  {
     if (testxx == 5 )
     {
        testschnitt = 1 ;
        testkk = 0 ;
        testxx = 0 ;
     }
     else
// testkk muss immer mit 1 anfangen,sonst wird die logfile nicht korrekt geoffnet
     { 
        testxx = 1 ; testkk = 1 ;
     }
  }
  else
  {
     testxx = 0 ; testkk = 0 ;
  }

  if ( argc < 3 )     /* prog, fnb, dialogdir */ 
  { sprintf ( logtext , "Zuwenig Argumente : %d \n" , argc ) ;
    logschreib () ;
    retcode = eigexit ( 1 ) ;
  }
  if (! retcode )
  {
    fnb_nr = -1 ;
    fnb_nr = atoi ( argv[1] ) ;
    sprintf ( logtext , "FNB-Nummer : %d\n" , fnb_nr ) ;
    logschreib () ;
    srptr = argv[2] ;
    strlaenge = strlen ( srptr ) ;
    if ( strlaenge == 0 ) retcode = eigexit ( 3 ) ;
  } 
  if (!retcode)
  {
    if ( strlaenge > 127 ) strlaenge = 127 ; 
    memset ( dialogdir , '\0' , 128 ) ;
    memcpy ( dialogdir , srptr , strlaenge ) ;
    if ( strlaenge < 70 )
    {
      sprintf ( logtext , "%s\n" , dialogdir ) ;
    }
    else
    {
      sprintf ( logtext , "Langer directory-Name \n " ) ;
    }

    logschreib () ;

    strlaenge = initschnitt() ; 
    if ( strlaenge ) retcode = eigexit ( strlaenge ) ;
  } 
  if ( !retcode )
  {
    switch ( fnb_nr )
    { 
      case 0 :                  /* Initialisierung Schnittstelle + geraet */
        retcode = eigexit ( fnb0() ) ;  break ;

      case 2 :                  /* Fehler-reset */
        retcode = eigexit ( fnb2() ) ; break ;

      case 3 :                  /* Nullstellen */
        retcode = eigexit ( fnb3() ) ; break ;

      case 4 :                  /* Tarieren */
        retcode = eigexit ( fnb4() ) ; break ;

      case 5 :                  /* Tara loeschen */

        if ( dev_type == GE_S20 )
        {       // 200906
            retcode = eigexit ( fnb5nn() ) ; break ;
        };

        if ( dev_type == GE_S30 )
        {       // 050906
            retcode = eigexit ( fnb5n() ) ; break ;
        };
        retcode = eigexit ( fnb5() ) ; break ;

      case 6 :                  /* Waagen-Umschaltung */
        if ( argc < 4 )
        { 
          sprintf ( logtext , " Keine Waagennummer zum Umschalten\n" );
          logschreib () ;
          retcode = eigexit ( 6 ) ; break ;
        }
        retcode = eigexit ( fnb6 ( atoi(argv[3]) ) ) ; break ;
      case 7 :    /* wird immer auf "mit Ruhewertung" gelegt */
      case 8 :                  /* Wiegen ohne Ruhewertung */
        retcode = eigexit ( fnb8() ) ; break ;

      case 11 :                  /* Postenregistrierung */
        retcode = eigexit ( fnb11() ) ; break ;

      case 23 :
        if ( argc < 4 )
        { 
          sprintf ( logtext , " Kein Wert : ich nehme 0 \n" );
          logschreib () ;
          if ( dev_type == GE_S20 )
          {       // 200906
               retcode = eigexit ( fnb5nn() ) ; break ;
          } ;
          if ( dev_type == GE_S30 )
          {       // 050906
               retcode = eigexit ( fnb5n() ) ; break ;
          } ;
          retcode = eigexit ( fnb5() ) ; break ;
        }
        retcode = eigexit ( fnb23 ( inpkonv(argv[3]) ) ) ; break ;

     case 33 :
        if ( argc < 4 )
        { 
           sprintf ( lakeyi ,"" ) ;
           memset ( dialdatin ,'\0' ,21 ) ;
           sprintf ( logtext , " Kein Wert : ich nehme Leerstring \n" );
           logschreib () ;
           retcode = eigexit ( fnb33 ( NULL) ) ; break ;
        }
        else
        {
           sprintf ( lakeyi , "" ) ;
           memset ( dialdatin ,'\0' ,21 ) ;
           retcode = eigexit ( fnb33 ( argv[3] ) ) ;
        }
        break ;

      default : retcode = eigexit ( (int) 4 ) ; break ; /* Error-return */
    }    /* Ende switch */        
  }     /* retcode */

#ifndef MAINI
 if ( testkk == 2 )
 {
  sprintf ( logtext , "exit mit %d\n" , retcode ) ;
  logschreib() ;
  fclose (fperr) ;
  testkk = 1 ;
 }
 return ( retcode ) ;
#endif

}       /* END main */  

/* ##################################################################### */
/* FNB0 : Initialisierung der Schnittstelle und des Geraetes             */
/* ##################################################################### */

fnb0() 
{
sprintf ( logtext , "fnb0 gestartet \n" ) ;
logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;
return ( fnb2() ) ;
}       /* end fnb0 */

/* ##################################################################### */
/* FNB2 : Initialisierung der Schnittstelle und des Geraetes             */
/* ##################################################################### */

fnb2()
{
 int retanz,reti,schleife ;
 char kdostring[10] ; 
 unsigned ch ;
  sprintf ( logtext , "fnb2 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;
  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
  ch = '<' ;
  kdostring[reti++] = ch ;
  ch = 'r' ;     /* reset anhaengige Befehle */
  kdostring[reti++] = ch ;
  ch = '>' ;    
  kdostring[reti++] = ch ;

  for ( schleife = 3 ;schleife >= 0 ; schleife --)
  {
     retanz = os_write (schnittstelle, kdostring , reti ) ;
     sprintf ( logtext , "Init retanz  : %d ; %s \n" , retanz, kdostring ) ;
     logschreib () ;
     if ( retanz == reti )
     {
        if ( ! wartack(500))
        {
           sleep ( 1 ) ;
           return( 0 ) ;
        }
     }
  }
  return ( -1 ) ;
}       /* end fnb2 */
/* ##################################################################### */
/* FNB3 : Nullstellen des Geraetes :                                     */
/* ##################################################################### */
fnb3()
{
 int retanz, reti,schleife ;
 char kdostring[10] ; 
 unsigned ch ;


 sprintf ( logtext , "fnb3 gestartet \n" ) ;
 logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
  ch = '<' ;
  kdostring[reti++] = ch ;
  ch = 'z' ;     /* Nullstellen */
  kdostring[reti++] = ch ;
  ch = '>' ;    
  kdostring[reti++] = ch ;

  for ( schleife = 3 ;schleife >= 0 ; schleife --)
  {
    retanz = os_write (schnittstelle, kdostring , reti ) ;
    sprintf ( logtext , "Init retanz  : %d ; %s \n" , retanz, kdostring ) ;
    logschreib () ;
    if ( retanz == reti )
    {
      if ( ! quitN()) return ( 0) ;
    }
  }
return ( -1 ) ;

}       /* end fnb3 */

/* ##################################################################### */
/* FNB4 : Tarieren                                                       */
/* ##################################################################### */
fnb4()
{

 int retanz,reti,schleife ;
 char kdostring[20] ; 
 unsigned ch ;

   sprintf ( logtext , "fnb4 gestartet \n" ) ;
   logschreib () ;

   memset ( kdostring , '\0' , 20 ) ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

   reti = 0 ;
   ch = '<' ;
   kdostring[reti++] = ch ;
   ch = 't' ;
   kdostring[reti++] = ch ;
   ch = '>' ;
   kdostring[reti++] = ch ; 
  for ( schleife = 3 ;schleife >= 0 ; schleife --)
  {
     retanz = os_write (schnittstelle, kdostring , reti ) ; 
     sprintf ( logtext , "retanz : %d ; %s \n" , retanz, kdostring ) ;
     logschreib () ;
// 290310     if ( retanz == reti ) if ( ! quitT()) return ( 0) ;
     if ( retanz == reti ) if ( ! wartack(dtartime)) return ( 0) ;      // 290310
   } ;
   return ( -1 ) ;
}       /* end fnb4 */

/* ##################################################################### */
/* FNB5 : Tara Loeschen                                                  */
// alles in einem String geht bei 9600 schon nicht mehr gut ... 
/* ##################################################################### */
fnb5()
{
 int retanz,reti,schleife ;
 char kdostring[20] ; 
 unsigned ch ;
  sprintf ( logtext , "fnb5 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

  memset ( kdostring , '\0' , 20 ) ;
  reti = 0 ;
  ch = '<' ;
  kdostring[reti++] = ch ;
  ch = '1'  ; 
  kdostring[reti++] = ch ;
  ch = '5'  ; 
  kdostring[reti++] = ch ;
  ch = '9'  ; 
  kdostring[reti++] = ch ;
  ch = '5'  ; 
  kdostring[reti++] = ch ;
  ch = '>' ;
  kdostring[reti++] = ch ;
  for ( schleife = 3 ;schleife >= 0 ; schleife --)
  {
     retanz = os_write (schnittstelle, kdostring , reti ) ; 
     sprintf ( logtext , "fnb5 : retanz : %d ; %s \n" , retanz, kdostring ) ;
     logschreib () ;
     if ( retanz == reti )
     {
        if ( !wartack(500) ) break ;
     }
  }
  if ( schleife < 0 ) return (-2) ;

  memset ( kdostring , '\0' , 20 ) ;
  reti = 0 ;
  ch = '<' ;
  kdostring[reti++] = ch ;
  ch = '1' ;
  kdostring[reti++] = ch ;
  ch = '8' ;
  kdostring[reti++] = ch ;
  ch = '9' ;
  kdostring[reti++] = ch ;
  ch = '8' ;
  kdostring[reti++] = ch ;
  ch = '>' ;
  kdostring[reti++] = ch ;
  for ( schleife = 3 ;schleife >= 0 ; schleife --)
  {
     retanz = os_write (schnittstelle, kdostring , reti ) ; 
     sprintf ( logtext , "fnb5 : retanz : %d ; %s \n" , retanz, kdostring ) ;
     logschreib () ;
     if ( retanz == reti )
     {
        if ( !wartack(500) ) break ;
     }
  }
  if ( schleife < 0 ) return (-2) ;
  return ( 0 ) ;
}       /* end fnb5 */

/* ##################################################################### */
/* FNB5n : Tara Loeschen     mit tC                                   */
/* ##################################################################### */
fnb5n()
{
 int retanz,reti,schleife ;
 char kdostring[20] ; 
 unsigned ch ;
  sprintf ( logtext , "fnb5 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

  memset ( kdostring , '\0' , 20 ) ;
  reti = 0 ;
  ch = '<' ;
  kdostring[reti++] = ch ;
  ch = 't'  ; 
  kdostring[reti++] = ch ;
  ch = 'C'  ; 
  kdostring[reti++] = ch ;
  ch = '>' ;
  kdostring[reti++] = ch ;

  for ( schleife = 3 ;schleife >= 0 ; schleife --)
  {
     retanz = os_write (schnittstelle, kdostring , reti ) ; 
     sprintf ( logtext , "retanz : %d ; %s \n" , retanz, kdostring ) ;
     logschreib () ;
     if ( retanz == reti ) if ( ! quitT()) return ( 0) ;
   } ;
   return ( -1 ) ;
}       /* end fnb5n */

/* ##################################################################### */
/* FNB5nn : Tara Loeschen     mit R    -> ohne Quittung                  */
/* ##################################################################### */
fnb5nn()        // 200906
{
 int retanz,reti ;
 char kdostring[20] ; 
 unsigned ch ;
  sprintf ( logtext , "fnb5 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

  memset ( kdostring , '\0' , 20 ) ;
  reti = 0 ;
  ch = '<' ;
  kdostring[reti++] = ch ;
  ch = 'R'  ; 
  kdostring[reti++] = ch ;
  ch = '>' ;
  kdostring[reti++] = ch ;

  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "retanz : %d ; %s \n" , retanz, kdostring ) ;
  logschreib () ;
  return ( 0 ) ;
}       /* end fnb5nn */

/* ##################################################################### */
/* FNB6 : Waagenumschaltung                                              */
/* ##################################################################### */
fnb6( int waagennummer ) 
{
 int retanz,reti,schleife ;
 char kdostring[15] ; 
 unsigned ch ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

  sprintf ( logtext , "fnb6 gestartet \n" ) ;
  logschreib () ;
  memset ( kdostring , '\0' , 15 ) ;
  reti = 0;
  ch = '<';
  kdostring[reti++] = ch ;
  ch = '1';
  kdostring[reti++] = ch ;
  ch = 'E';
  kdostring[reti++] = ch ;
  ch = '9';
  kdostring[reti++] = ch ;
  ch = 'E';
  kdostring[reti++] = ch ;
  ch = '>';
  kdostring[reti++] = ch ; 

  if ( waagennummer == 1 ) ch = 0x31 ;
  if ( waagennummer == 2 ) ch = 0x32 ;
  if ( waagennummer == 3 ) ch = 0x33 ;

  for ( schleife = 5 ;schleife >= 0 ; schleife --)
  {
     retanz = os_write (schnittstelle, kdostring , reti ) ; 
     sprintf ( logtext , "fnb6 retanz : %d ; %s \n" , retanz, kdostring ) ;
     logschreib () ;
     if ( retanz == reti )
     {
        if ( !wartack(500) )
        {
           if ( fnb7a() == ch ) return (0) ;
        }
     }
   }
     
  return ( -1 ) ;
}       /* end fnb6 */

/* ##################################################################### */
/* FNB7a : Lesen ohne Ruhewertung, um aktuelle Waage zu lesen            */
/* ##################################################################### */
unsigned fnb7a()
{
 int retanz,reti,schleife ;
 char kdostring[10] ; 
 unsigned ch ;

  sprintf ( logtext , "fnb7a gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
  ch = '<' ;
  kdostring[reti++] = ch ;
  ch = 'A'  ;
  kdostring[reti++] = ch ;
  ch = '>' ;
  kdostring[reti++] = ch ;
  for ( schleife = 3 ;schleife >= 0 ; schleife --)
  {
      retanz = os_write (schnittstelle, kdostring , reti ) ; 
      sprintf ( logtext , "retanz Init : %d \n" , retanz ) ;
      logschreib () ;
      if ( retanz == reti )
      {
          memset ( wertbuffer , '\0' , 120 ) ;
          retanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
          sprintf ( logtext , "%s\n" , wertbuffer ) ;
          logschreib () ;
          if ( retanz )
          {
             return ( wagnr(wertbuffer,retanz) ) ;
          }
      }

   }
  return ( (unsigned) 0 ) ;
}


unsigned wagnr( char * wertbuffer, int retanz )
{
int i ,ergeb;
 for ( i= 0 ; i < retanz ; i++ )
 {  ergeb = wertbuffer[i] ;
   if ( ergeb == 'W' )
    {
      ergeb = wertbuffer[i+1] ;
      sprintf ( logtext ,"ergeb : %d %s\n" , ergeb, wertbuffer ) ;
      logschreib () ;
      return  (unsigned) ergeb  ;
    }
 }
 return  0 ;
}
/* ##################################################################### */
/* FNB8 : Wiegen (mit Ruhewertung)                                       */
/* ##################################################################### */

fnb8()
{
 int retanz,reti,schleife ;
 char kdostring[10] ; 
 unsigned ch ;
// #endif
  sprintf ( logtext , "fnb8 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;
  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
  ch = '<' ;
  kdostring[reti++] = ch ;
  ch = 'A' ;
  kdostring[reti++] = ch ;
  ch = '>' ;
  kdostring[reti++] = ch ;
  for ( schleife = 3 ;schleife >= 0 ; schleife --)
  {
      retanz = os_write (schnittstelle, kdostring , reti ) ; 
      sprintf ( logtext , "retanz Init : %d \n" , retanz ) ;
      logschreib () ;
      if ( retanz == reti )
      {
          memset ( wertbuffer , '\0' , 120 ) ;
          retanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
          sprintf ( logtext , "%s\n" , wertbuffer ) ;
          logschreib () ;
          if ( retanz )
          {  ergebschreib = 1 ; // 190209
             return ( ds20gew(retanz )) ;
          }
      }

   }
   return ( 0 ) ;
}       /* end fnb8 */
/* ##################################################################### */
/* FNB11 : Postenregistrierung                                           */
/* ##################################################################### */
fnb11()
{
 int retanz,reti,schleife ;
  char kdostring[15] ; 
  unsigned ch ;
  sprintf ( logtext , "fnb11 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

// 271108 A dfw-Ablauf dazu : negative oder == 0 - Werte vorlesen und dann
// keine Registrierung anstarten 
   if ( dfw_ablauf )
   {
        memset ( kdostring , '\0' , 10 ) ;
        reti = 0 ;
        ch = '<' ;
        kdostring[reti++] = ch ;
        ch = 'A'  ;
        kdostring[reti++] = ch ;
        ch = '>' ;
        kdostring[reti++] = ch ;
        for ( schleife = 3 ;schleife >= 0 ; schleife --)
        {
            retanz = os_write (schnittstelle, kdostring , reti ) ; 
            sprintf ( logtext , "retanz Init : %d \n" , retanz ) ;
            logschreib () ;
            if ( retanz == reti )
            {
               memset ( wertbuffer , '\0' , 120 ) ;
               retanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
               sprintf ( logtext , "%s\n" , wertbuffer ) ;
               logschreib () ;
               if ( retanz )
               {    ergebschreib = 0 ;  // 190109
                    ds20gew(retanz ) ;
                    break ;
               }
            }

        }
        if ( wertn <= 0.0 )
        {
            sprintf ( logtext , "dfw-Abbruch %lf \n" , wertn ) ;
            logschreib () ;
            return ( -35 ) ;
        }
  }
// 271108 E

  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
  ch = '<' ;
  kdostring[reti++] = ch ;
// 020109 : Alibispeicher bedienen im S30-neu
  if ( dalibi )
  ch = 'X' ;
  else
  ch = 'B'  ;
  kdostring[reti++] = ch ;
  ch = '>' ;
  kdostring[reti++] = ch ;
  for ( schleife = 3 ;schleife >= 0 ; schleife-- )
  {
      retanz = os_write (schnittstelle, kdostring , reti ) ; 
      sprintf ( logtext , "retanz Init : %d \n" , retanz ) ;
      logschreib () ;
      if ( retanz == reti )
      {
          memset ( wertbuffer , '\0' , 120 ) ;
          retanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
          sprintf ( logtext , "%s\n" , wertbuffer ) ;
          logschreib () ;
          if ( retanz )
          {  ergebschreib = 1 ; // 190209
             return ( ds20gew(retanz) ) ;
          }
      }

   }
   return ( 0 ) ;

}       /* end fnb11 */

/* ##################################################################### */
/* FNB23 : Tarieren mit Wertvorgabe                                      */
/* ##################################################################### */

fnb23 ( double targew )
{
int reti ;
char kdostring[25] ;
unsigned char ch,ch1 ;
int x1 , x2 , x3 ;
int inti1, inti2, inti3 ;       // 181208
char kostri[20] ;
char form [20] ;                // 241208
  sprintf ( logtext , "fnb23 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;


// 050109  printf ( "Targew : %ld \n" , targew );

  sprintf ( logtext , "targew : %lf \n" , targew ) ;
  logschreib () ;

  if ( targew < 0.0 ) targew = 0.0 ;    // 181208 : notbremse

  if ( dev_type == GE_S30 )
  {     // 050906
        reti = fnb5n() ;
  }
  else
  {
        if ( dev_type == GE_S20 )
        {       // 200906
                reti = fnb5nn() ;
        }
        else
        {
                reti = fnb5() ;       // immer erst mal loeschen, sonst klemmts 
        }
  }
  if ( targew == 0.0L ) { return reti ; }   // geht sicher schneller ...

// 181208 A
   if ( dev_type == GE_S30 )
   {
      if ( dtrenner > 0 )       // 191208
                                // trenner-Ablauf kann eigentlich wieder weg
      {                         // dann wird mit "tarfakt" gearbeitet
          memset ( kdostring , '\0' , 25 ) ;
          sprintf ( kostri , "%1.3lf" , targew ) ;
      }
      else
      { // dann wird mit tarfakt gearbeitet , jedoch ohne Trenner
        // default ist ohne trenner und mit Faktor = 100
        dtrenner = 0 ;
        inti1 = (int) ( targew * dtarfakt ) ;
        inti2 = (int) ( targew * dtarfakt * 10 ) ;
        inti3 = inti1 * 10 ;
        if (( inti2 - inti3 ) > 4 )  inti1 ++ ;
        sprintf ( kostri , "%1.0d" , inti1 ) ;
      }

      reti = 0 ;
      x2 = strlen ( kostri ) ;
      ch1 = '<' ;
      kdostring[reti ++ ] = ch1 ;
      ch1 = 't' ;
      kdostring[reti ++ ] = ch1 ;
      for ( x1 = 0 ; x1 < x2 ; x1++ )
      {
          ch = kostri [ x1 ] ;
          if ( ch == '.' || ch == ',' )
          {
             if ( dtrenner == 1 || dtrenner == 2 )
             {
               if ( dtrenner == 1 ) ch = ',' ;
               if ( dtrenner == 2 ) ch = '.' ;
               kdostring[reti ++ ] = ch ;
             }
          }
          else
          {
             if ( ch > 0x2f && ch < 0x3a )
             {
                 kdostring[reti ++ ] = ch ;
              }
          }
      }     // for-schleife
      ch = '>' ;
      kdostring[reti ++ ] = ch ;
      for ( x3 = 3 ;x3 >= 0 ; x3 --)
      {
           x1 = os_write (schnittstelle, kdostring , reti ) ; 
           sprintf ( logtext , "fnb23 : reti : %d ; %s \n" , x1, kdostring ) ;
           logschreib () ;
           if ( reti == x1 )
           {
              if ( !wartack(500) ) break ;
           }
      }
      return (0) ;         
   }
// 181208 E

// die s20 rundet und schneidet alleine ab ....
  memset ( kdostring , '\0' , 25 ) ;
  sprintf ( kostri , "%1.3lf" , targew ) ;

// 241208 A : mal kommastellen auswerten .....
   memset ( kdostring , '\0' , 25 ) ;
   sprintf ( form , "%%-01.%dlf" , kommastellen ) ;
   sprintf ( kostri , form , targew ) ;
// 241208 E

   reti = 0 ;
   x2 = strlen ( kostri ) ;
   ch1 = '<' ;
   kdostring[reti ++ ] = ch1 ;

  for ( x1 = 0 ; x1 < x2 ; x1++ )
  {
     ch = kostri [ x1 ] ;
     if ( ch == '.' || ch == ',' )
     {
        ch1 = '0' ;
        kdostring[reti ++ ] = ch1 ;
        ch1 = 'A' ;
        kdostring[reti ++ ] = ch1 ;
        ch1 = '8' ;
        kdostring[reti ++ ] = ch1 ;
        ch1 = 'A' ;
        kdostring[reti ++ ] = ch1 ;

     }
     else
     {
        if ( ch > 0x2f && ch < 0x3a )
        {
           ch1 = '0' ;
           kdostring[reti ++ ] = ch1 ;
           kdostring[reti ++ ] = ch ;
           ch1 = '8' ;
           kdostring[reti ++ ] = ch1 ;
           kdostring[reti ++ ] = ch ;
        }
     }
  }     // for-schleife
  ch = '>' ;
  kdostring[reti ++ ] = ch ;
  for ( x3 = 3 ;x3 >= 0 ; x3 --)
  {
     x1 = os_write (schnittstelle, kdostring , reti ) ; 
     sprintf ( logtext , "fnb23 : reti : %d ; %s \n" , x1, kdostring ) ;
     logschreib () ;
     if ( reti == x1 )
     {
        if ( !wartack(500) ) break ;
     }
  }
  if ( x3 < 0 ) return (-2) ;

  memset ( kdostring , '\0' , 20 ) ;
  reti = 0 ;
  ch = '<' ;
  kdostring[reti++] = ch ;
  ch = '1' ;
  kdostring[reti++] = ch ;
  ch = '8' ;
  kdostring[reti++] = ch ;
  ch = '9' ;
  kdostring[reti++] = ch ;
  ch = '8' ;
  kdostring[reti++] = ch ;
  ch = '>' ;
  kdostring[reti++] = ch ;
  for ( x3 = 3 ;x3 >= 0 ; x3 --)
  {
     x1 = os_write (schnittstelle, kdostring , reti ) ; 
     sprintf ( logtext , "fnb23 : reti : %d ; %s \n" , reti, kdostring ) ;
     logschreib () ;
     if ( x1 == reti )
     {
        if ( !wartack(500) ) break ;
     }
  }
  if ( x3 < 0 ) return (-2) ;
  return ( 0 ) ;        // 181208 : aus "-2" wurde "0"
}    /* end fnb23 */


/* der Test faellt aus, weil das 2760 es halt geistig nicht raffelt
// 020109 : fnb23testabl  zum test - inaktiv

fnb23testabl ( double targew )
{
int reti ;
char kdostring[25] ;
unsigned char ch,ch1 ;
int x1 , x2 , x3 ,x4 ;
int inti1, inti2, inti3 ;       // 181208
char kostri[20] ;
char form [20] ;                // 241208
int testabl ;                   // 020109
testabl = 0 ;                   // 020109

  sprintf ( logtext , "fnb23 gestartet \n" ) ;
  logschreib () ;

// im TEXTBLOCK

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

// im TEXTBLOCK

  printf ( "Targew : %ld \n" , targew );

  sprintf ( logtext , "targew : %lf \n" , targew ) ;
  logschreib () ;

  if ( targew < 0.0 ) targew = 0.0 ;    // 181208 : notbremse

  if ( dev_type == GE_S30 )
  {     // 050906
        reti = fnb5n() ;
  }
  else
  {
        if ( dev_type == GE_S20 )
        {       // 200906
                reti = fnb5nn() ;
        }
        else
        {
                reti = fnb5() ;       // immer erst mal loeschen, sonst klemmts 
        }
  }

// im TEXTBLOCK

  if ( targew == 0.0L ) { return reti ; }   // geht sicher schneller ...

// 181208 A
   if ( dev_type == GE_S30 )
   {
      if ( dtrenner > 0 )       // 191208
                                // trenner-Ablauf kann eigentlich wieder weg
      {                         // dann wird mit "tarfakt" gearbeitet
          memset ( kdostring , '\0' , 25 ) ;
          sprintf ( kostri , "%1.3lf" , targew ) ;
// im TEXTBLOCK
      }
      else
      { // dann wird mit tarfakt gearbeitet , jedoch ohne Trenner
        // default ist ohne trenner und mit Faktor = 100
        dtrenner = 0 ;
        inti1 = (int) ( targew * dtarfakt ) ;
        inti2 = (int) ( targew * dtarfakt * 10 ) ;
        inti3 = inti1 * 10 ;
        if (( inti2 - inti3 ) > 4 )  inti1 ++ ;
        sprintf ( kostri , "%1.0d" , inti1 ) ;
      }

// im TEXTBLOCK

      reti = 0 ;
      x2 = strlen ( kostri ) ;
      ch1 = '<' ;
      kdostring[reti ++ ] = ch1 ;
      ch1 = 't' ;
      kdostring[reti ++ ] = ch1 ;
      for ( x1 = 0 ; x1 < x2 ; x1++ )
      {
          ch = kostri [ x1 ] ;
          if ( ch == '.' || ch == ',' )
          {
             if ( dtrenner == 1 || dtrenner == 2 )
             {
               if ( dtrenner == 1 ) ch = ',' ;
               if ( dtrenner == 2 ) ch = '.' ;
               kdostring[reti ++ ] = ch ;
// im TEXTBLOCK
             }
          }
          else
          {
             if ( ch > 0x2f && ch < 0x3a )
             {
                 kdostring[reti ++ ] = ch ;
              }
          }
      }     // for-schleife

// im TEXTBLOCK

      ch = '>' ;
      kdostring[reti ++ ] = ch ;
      for ( x3 = 3 ;x3 >= 0 ; x3 --)
      {
           x1 = os_write (schnittstelle, kdostring , reti ) ; 
           sprintf ( logtext , "fnb23 : reti : %d ; %s \n" , x1, kdostring ) ;
           logschreib () ;
           if ( reti == x1 )
           {
              if ( !wartack(500) ) break ;
           }
      }
      return (0) ;         
   }
// 181208 E

// im TEXTBLOCK

// die s20 rundet und schneidet alleine ab ....
  memset ( kdostring , '\0' , 25 ) ;
  sprintf ( kostri , "%1.3lf" , targew ) ;

// 241208 : mal kommastellen auswerten .....
   memset ( kdostring , '\0' , 25 ) ;
   sprintf ( form , "%%-01.%dlf" , kommastellen ) ;
   sprintf ( kostri , form , targew ) ;
// 241208 

// im TEXTBLOCK

// 241208  reti = 0 ;
   x2 = strlen ( kostri ) ;
// 241208  ch1 = '<' ;
// 241208  kdostring[reti ++ ] = ch1 ;

  for ( x1 = 0 ; x1 < x2 ; x1++ )
  {
     ch = kostri [ x1 ] ;
     if ( ch == '.' || ch == ',' )
     {
        reti = 0 ;
        ch = '<' ;
        kdostring[reti ++] = ch ;
        ch1 = '0' ;
        kdostring[reti ++ ] = ch1 ;
        ch1 = 'A' ;
        kdostring[reti ++ ] = ch1 ;
        ch1 = '8' ;
// im TEXTBLOCK
        kdostring[reti ++ ] = ch1 ;
        ch1 = 'A' ;
        kdostring[reti ++ ] = ch1 ;
        ch = '>' ;
        kdostring[reti ++ ] = ch ;
        x4 = os_write (schnittstelle, kdostring , reti ) ; 
        sprintf ( logtext , "fnb23 : reti : %d ; %s \n" , x4, kdostring ) ;
        logschreib () ;

// im TEXTBLOCK

        for ( x3 = 3 ;x3 >= 0 ; x3 --)
        {
            if ( reti == x4 )
            {
                if ( !wartack(500) ) break ;
            }
        }
        if ( x3 < 0 ) return (-2) ;

     }
     else
     {
        if ( ch > 0x2f && ch < 0x3a )
        {
           reti = 0 ;
           ch1 = '<' ;
           kdostring[reti ++ ] = ch1 ;
           ch1 = '0' ;
           kdostring[reti ++ ] = ch1 ;
           kdostring[reti ++ ] = ch ;
           ch1 = '8' ;
           kdostring[reti ++ ] = ch1 ;
           kdostring[reti ++ ] = ch ;
           ch1 = '>' ;
           kdostring[reti ++ ] = ch1 ;
           x4 = os_write (schnittstelle, kdostring , reti ) ; 
           sprintf ( logtext , "fnb23 : reti : %d ; %s \n" , x4, kdostring ) ;
           logschreib () ;

// im TEXTBLOCK

           for ( x3 = 3 ;x3 >= 0 ; x3 --)
           {
              if ( reti == x4 )
             {
                if ( !wartack(500) ) break ;
             }
           }
           if ( x3 < 0 ) return (-2) ;
        }
     }
  }     // for-schleife
//  ch = '>' ;
//  kdostring[reti ++ ] = ch ;
//  for ( x3 = 3 ;x3 >= 0 ; x3 --)
//  {
//     x1 = os_write (schnittstelle, kdostring , reti ) ; 
//     sprintf ( logtext , "fnb23 : reti : %d ; %s \n" , x1, kdostring ) ;
//     logschreib () ;
//     if ( reti == x1 )
//     {
//        if ( !wartack(500) ) break ;
//     }
//  }
//  if ( x3 < 0 ) return (-2) ;

// im TEXTBLOCK

  memset ( kdostring , '\0' , 20 ) ;
  reti = 0 ;
  ch = '<' ;
  kdostring[reti++] = ch ;
  ch = '1' ;
  kdostring[reti++] = ch ;
  ch = '8' ;
  kdostring[reti++] = ch ;
  ch = '9' ;
  kdostring[reti++] = ch ;
  ch = '8' ;
  kdostring[reti++] = ch ;
  ch = '>' ;
  kdostring[reti++] = ch ;
  for ( x3 = 3 ;x3 >= 0 ; x3 --)
  {
     x1 = os_write (schnittstelle, kdostring , reti ) ; 
     sprintf ( logtext , "fnb23 : reti : %d ; %s \n" , reti, kdostring ) ;
     logschreib () ;
     if ( x1 == reti )
     {
        if ( !wartack(500) ) break ;
     }
  }
  if ( x3 < 0 ) return (-2) ;
  return ( 0 ) ;        // 181208 : aus "-2" wurde "0"

// im TEXTBLOCK

}       /* end fnb23 */

// 290699 A

/* ##################################################################### */
/* FNB33 : Test-Tool : beliebigen String senden und Anrwort leerpollen   */
/* ##################################################################### */
fnb33(char * eingabe )
{

   int retanz ;
   int zeig, quell ;
   char kdostring[100] ; 
   unsigned ch ;
   if ( eingabe == NULL )
   {
       retanz = 0 ;
       sprintf ( logtext , "fnb33 gestartet : Leerstring \n" ) ;
   }
   else
   {
       retanz = strlen ( eingabe ) ;
       sprintf ( logtext , "fnb33 gestartet : %s \n" , eingabe ) ;
   }
   logschreib () ;

   zeig = 0 ;
   quell = 0 ;
   memset ( kdostring , '\0' , 100 ) ;
   retanz = strlen (eingabe) ;
   while ( quell < retanz )
   {
      ch = eingabe[quell++] ;
      if ( ch < 0x20 ) ch = 0x20 ;
      if ( ch > 0x7f ) ch = 0x20 ;
      kdostring[zeig++] = ch ;
   }

   retanz = os_write (schnittstelle, kdostring , zeig ) ; 
   sprintf ( logtext , "retanz 33 : %d , KDO : %s \n" , retanz, kdostring ) ;
   logschreib () ;
   if ( retanz != zeig ) return ( -1 ) ;
    memset ( wertbuffer , '\0' , 120 ) ;
    retanz = os_read ( schnittstelle, wertbuffer , 110 ) ;
    sprintf ( logtext , "retanz 33 : %d : %s \n" , retanz, wertbuffer ) ;
    logschreib () ;

return ( 0 ) ;
}       /* end fnb33 */

initschnitt()
{  

if ( schoninit > 0 ) return ( 0 ) ;

if ( testschnitt )
{
   printf ( "INITIALISIERUNG\n" ) ;
   sprintf ( logtext , "INITIALISIERUNG\n" ) ;
   logschreib();
}
schoninit = 1 ;
edv_conf.baudrate = 9600 ;
edv_conf.databits = 7 ;
edv_conf.parity = EVEN ;
edv_conf.stopbits = 1 ;
edv_conf.flowcontrol = 1 ;
sprintf ( edv_chan.ddId , "COM1" ) ;
kommastellen = 2 ;
get_defax () ;
// 251008 : testxx wird gegbnenfalls modifiziert,
// die lokale Einstellung kann nur  einschalten oder aendern
  if ( testxx > 0 )
  {
     if ( testkk > 0 )  // es ist bereits vorher was gelaufen
     {
        if ( testxx == 5 )
        {
           testxx = 0 ; testschnitt = 1 ;
        }
        else
        {
           testxx = 1; testschnitt = 0 ;     
        }
     }
     else       // erstmalig aktivieren
     {
        testkk = 1 ;
        if ( testxx == 5 )
        {
           testxx = 0 ; testschnitt = 1 ;
        }
        else
        {
           testxx = 1 ; testschnitt = 0 ;     
        }
     }
  }
  else
  {
     if (( testkk > 0 ) && (testschnitt == 0 ))
        testxx = 1 ;    // Ausgangszustand herstellen
  }
// 251008 E

sprintf ( logtext , "Vor CreateFile \n" ) ;
logschreib () ;
    schnittstelle = CreateFile ( (LPCTSTR) edv_chan.ddId ,
                        GENERIC_READ | GENERIC_WRITE ,
                        0 , /* prevents the file from being shared */
                        NULL ,  /* no security attributes */
                        OPEN_EXISTING , /* comm devices must use "OPEN_EXISTING */
                        0 , /* not overlapped I/O */
                        NULL /* hTemplate must be NULL for comm devices */
                        ) ;
    if ( schnittstelle == INVALID_HANDLE_VALUE ) 
    return ( (int) 12 ) ;

    memset (( void *) &a_CommDcb, '\0', sizeof (a_CommDcb)) ;
    memset (( void *) &a_CommTmo, '\0', sizeof (a_CommTmo)) ;
    sprintf ( logtext , "Vor GetCommState \n" ) ;
    logschreib () ;
    fSuccess = GetCommState(schnittstelle, &g_CommDcb) ;
    if ( fSuccess )
    {  
        a_CommDcb = g_CommDcb ;
    }
    else
    {
        return ( (int) 13 ) ;
    }
    sprintf ( logtext , "Vor GetTimeouts \n" ) ;
    logschreib () ;
    fSuccess = GetCommTimeouts(schnittstelle, &g_CommTmo) ;
    if ( fSuccess )
    {  
        a_CommTmo = g_CommTmo ;
    }
    else
    {
        return ( (int) 14 ) ;
    }
    if ( edv_conf.flowcontrol > 1 )
    {   /* flow control on */;  }
    else
    {   /* flow control off */
        /* Monitoring CTS signal */
        a_CommDcb.fOutxCtsFlow = FALSE ;
        
        /* Monitoring DSR signal */
        a_CommDcb.fOutxDsrFlow = FALSE ;

        /* DTR flow control */  // #221100 : enable->disable
        a_CommDcb.fDtrControl = DTR_CONTROL_DISABLE;

        /* DSR signal sensitivity */
        a_CommDcb.fDsrSensitivity = FALSE ;

        /* RTS flow control */
        a_CommDcb.fRtsControl = RTS_CONTROL_DISABLE ;
    }
    a_CommDcb.BaudRate = (DWORD) edv_conf.baudrate ;
    a_CommDcb.ByteSize = (BYTE) edv_conf.databits ;
    switch ( edv_conf.parity ) 
    {
        case NONE: a_CommDcb.Parity = NOPARITY ;
                break ;
        case EVEN: a_CommDcb.Parity = EVENPARITY ;
                break ;
        case ODD: a_CommDcb.Parity = ODDPARITY ; 
                break ;
        default : a_CommDcb.Parity = NOPARITY ; 
     }    
     switch ( edv_conf.stopbits )
     {
        case 1 : a_CommDcb.StopBits = ONESTOPBIT ;
                break ;
        case 2 : a_CommDcb.StopBits = TWOSTOPBITS ;
                break ;
        default : a_CommDcb.StopBits = ONESTOPBIT ;
     }
     fSuccess = SetCommState ( schnittstelle, &a_CommDcb ) ;
     if ( !fSuccess )  { return ( (int) 14 ) ; }
/* RITO : max. Pause zw. 2 Zeichen -> Endekenn.(9600 ca. 1 millsek/byte)  */  
/* RTTM und RTTC kooperieren */
/* Produkt = maximale Transfer-Time, dann wird (evtl.teilweise) geliefert */
/* RTTM : erweitert TO durch : RTTM * Anzahl Zeichen */
/* RTTC :erweitert TO durch : ( RTTM * AZ ) + RTTC   */
/* das sind gerade die Werte fuer 9600 */               /* 081008 : tfaktor dazu */
     a_CommTmo.ReadIntervalTimeout = 30 * tfaktor ;         /* 30 millisec -> Endekenn !! */
     a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;          /* x 2 ( Zeit je Byte+x */ 
     a_CommTmo.ReadTotalTimeoutConstant = 30000 ; /* 30 sek warten */ /* 30 millisec (RITO) */ 
     switch ( edv_conf.baudrate )
     {
     case 2400 :
     a_CommTmo.ReadIntervalTimeout = 50 * tfaktor ;         /* 50 msec -> Endekenn !! 081008 */
     a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;          /* x 2 ( Zeit je Byte+x */ 
           break ;
     case 4800 :
     a_CommTmo.ReadIntervalTimeout = 40 * tfaktor ;         /* 40 msec -> Endekenn !! 081008 */
     a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;    /* x 2 ( Zeit je Byte+x */ 
           break ;
     }
     a_CommTmo.WriteTotalTimeoutMultiplier = 100 ;       /* x 100 */
     a_CommTmo.WriteTotalTimeoutConstant = 100;          /* 100 millisec */                    

     fSuccess = SetCommTimeouts ( schnittstelle, &a_CommTmo ) ;
     if ( ! fSuccess ) return ( (int) 15 ) ;
     return ( leseleer (1) ) ;
}
/* #################################################################### */
/*                      Schreiben des Puffers                           */
/* #################################################################### */

int os_write (HANDLE fd , char *buf , unsigned nNumberOfBytesToWrite) 
{
DWORD   NumberOfBytesWritten ;
BOOL rc ;       /* returncode */
char hilfbuf [120] ;    // 220699 
  rc = WriteFile ( fd , (LPVOID) buf, (DWORD) nNumberOfBytesToWrite,
       (LPDWORD) &NumberOfBytesWritten , NULL ) ; 
  if (rc == FALSE)     
  {     sprintf ( logtext , "Fehler beim WriteFile\n" ) ; }
  if ( testschnitt )
  {

           memset ( hilfbuf , '\0' , 120 ) ;
           if ( NumberOfBytesWritten > 118 )
                memcpy ( hilfbuf , buf , 118 ) ;
           else
               memcpy ( hilfbuf , buf , NumberOfBytesWritten ) ;

               printf ( "WW %3.0d %s \n" , NumberOfBytesWritten , hilfbuf ) ;
               sprintf ( logtext, "WW %3.0d %s \n" , NumberOfBytesWritten , hilfbuf ) ;
               logschreib () ; 

  }

  return ( (int) NumberOfBytesWritten) ;
}

/* #################################################################### */
/*                        Lesen des Puffers                             */
/* #################################################################### */

int os_read (HANDLE fd , char *buf , unsigned nNumberOfBytesToRead)
{
DWORD NumberOfBytesRead ;
BOOL rc ;     /* returncode */
char hilfbuf [145] ;    // 251008 120->145
   rc = ReadFile ( fd , (LPVOID) buf , (DWORD) nNumberOfBytesToRead,
        (LPDWORD) &NumberOfBytesRead , NULL ) ;
   if (rc == FALSE )
   { sprintf ( logtext , "Fehler beim ReadFile \n" ) ; }
   if ( testschnitt )
   {
      if ( rc == FALSE )
      {
        printf ( "RR  -1 Fehler Lese-Zugriff \n" ) ;
        sprintf ( logtext , "RR  -1 Fehler Lese-Zugriff \n" ) ;
        logschreib () ;
      }
      else
      {
        if ( NumberOfBytesRead  == 0L )
        {
             printf ( "RR  00 NIX gelesen \n" ) ;
             sprintf ( logtext ,"RR  00 NIX gelesen \n" ) ;
             logschreib () ;
        }
        else
        {
           memset ( hilfbuf , '\0' , 145 ) ;
           if ( NumberOfBytesRead > 118 )
                memcpy ( hilfbuf , buf , 138 ) ;
           else
                memcpy ( hilfbuf , buf , NumberOfBytesRead ) ;
           printf ( "RR %3.0d %s \n" , NumberOfBytesRead , hilfbuf ) ;
           sprintf ( logtext , "RR %3.0d %s \n" , NumberOfBytesRead , hilfbuf ) ;
           logschreib () ;

        }
      }
   }
   return ( (int) NumberOfBytesRead ) ; 
}   

/* Alte Variante 

void logschreib ( void )
{   
char erfnam [20] ;
if ( testxx ) printf ( "%s\n" , logtext ) ;
if ( !testkk ) return ;
if ( testkk == 1 )
 {
 sprintf ( erfnam , "logfile" ) ;
 if ( ( fperr = fopen(erfnam, "at" )) == NULL ) return ;   // APPEND_TEXT
 } ;
 testkk = 2 ;
 fprintf ( fperr , logtext ) ;
}
< ---------- */

// 251008 : neue Variante

void logschreib ( void )
{   
char erfnam [20] ;
char writepuffer [200] ;

char timestr[100] ; 
if ( testxx ) printf ( "%s : %s\n" , _strtime(timestr),logtext ) ; 
if ( !testkk ) return ;
if ( testkk == 1 )
 {
 sprintf ( erfnam , "logfile" ) ;
 if ( ( fperr = fopen(erfnam, "at" )) == NULL ) return ;   // APPEND_TEXT 
 } ;
 testkk = 2 ;
 sprintf ( writepuffer, "%s : %s", _strtime(timestr),logtext ) ; 
 fprintf ( fperr , writepuffer ) ;
}

u_long eigexit ( int para  )
{
 if ( testkk == 2 )
 {
  sprintf ( logtext , "exit mit %d\n" , para ) ;
  logschreib() ;
  fclose (fperr) ;
 }

// unnoetig : fSuccess = SetCommState ( schnittstelle, &g_CommDcb ) ;
// unnoetig : fSuccess = SetCommTimeouts ( schnittstelle, &g_CommTmo ) ;

#ifdef MAINI
  exit ( (u_long) para ) ;
#else
  return ( (int) para ) ;
#endif
}

double inpkonv ( char *argl )
{
 int i, k ;
 k = strlen ( argl ) ;
 for ( i = 0 ; i < k ; i ++ )
 { if ( argl[i] == ',' ) argl[i] = '.' ; } 
 
  sprintf ( logtext , "Para3 : %s\n" , argl ) ;
  logschreib() ;
  return ( atof ( argl ) ) ; 
} 


/**  funktioniert erst mal ohne log. Quittungen, 
bei Bedarf waere erweitertes Handling noetig **/

// vorerst : EDV-Format richtig einstellen !!!
// Speicher 24 :
//  1818(tara)
//  2202(netto)
//  2104(Uxxx->Status)
//  0018(Grosso-brutto)
//  3400(end)

// Es werden Zahlen zw. "T", "N", "G" und "kg" gesucht
// 140207 : bzw. Nach "A" als Alibi-Nummer

int ds20gew ( int strleng )
{
 int i , j , k , l ;    /* i = Quellpointer */
                        /* j = Status 0 = Suche nach Blockanfang
                                      1 = Suche nach Wertanfang  
                                      2 = Transfer  aktiv 
                                      3 = Suche nach Trenner
                         */
                         // l = Anzah Werte gefunden
 double gewicht ;
 unsigned char art ;
 unsigned char zch ;
 char ausgabepuffer[46] ;
 char zwipu [16] ;

 FILE *fpergeb ;
 char ergebnam [128] ;

  strcpy ( ergebnam, dialogdir  ) ;
  if ( ergebschreib )   // 190209 Name modifizieren 
      strcat ( ergebnam , "\\ergebnis" ) ;
  else
      strcat ( ergebnam , "\\ergebnis" ) ;

//      strcat ( ergebnam , "\\ergebzwi" ) ;
    // jedenfalls erst mal datei weg und irgendein wert drin->
    // es kommt kein Fehler "ergebnisdatei nicht vorhanden"

  remove ( ergebnam ) ;  
  if ( ( fpergeb = fopen(ergebnam, "at" )) == NULL ) return ( -1 ) ; 
  memset ( ausgabepuffer , '\0' , 46 ) ;
  i=j=k=l=0 ;
  for ( i = 0 ; i < strleng ; i ++ )
  { zch = wertbuffer[i] ;
    zch &= 0x7f ;              /* Pari-Strip */
    if ( j == 0 )        /* Identifikation */
    {
      switch (zch)
      {
         case 'G' :  art = 'B' ; i++ ; j = 1 ;  /* Brutto */ break ;
         case 'N' :  art = 'N' ; i++ ; j = 1 ;  /* Netto */ break ;
         case 'T' :  art = 'T' ; i++ ; j = 1 ;  /* Tara */ break ;
         case  CRi:  art = ' ' ;       j = 0 ;  /* Stringende */ break ;
                                                         /* ACHTUNG */
         case 'A' :  art = 'E' ; i++ ; j = 1 ;  /* Eich-Speich */ break ;
// 140207         default  :  art = ' ' ; i++ ; j = 3 ;  /* Blindblock */ break ;
         default  :  art = ' ' ; /* i++ */ ; j = 0 ;  /* Blindzeichen */ break ;
                                /* 181208 */
       } continue ;
    }

    if ( j == 1 )        /* Wertanfang suchen suchen,sollte sofort klappen */
    {
        if (( zch > 0x2f && zch < 0x3a ) || zch == ',' || \
                zch == '.' || zch == '+' || zch == '-' )
        { 
          memset ( zwipu , '\0' , 16 ) ; k= 0 ; j = 2 ;
          if ( zch == ',' ) zch = '.' ;
          zwipu[k] = zch ;  k++ ;
        } continue ;
    }
    if ( j == 2 ) 
    {
      if (( zch > 0x2f && zch < 0x3a ) || zch == ',' || \
             zch == '.' || zch == '+' || zch == '-' )
      { 
        if ( zch == ',' ) zch = '.' ;
        zwipu[k] = zch ; k++ ;
      }
      else    /* Das war das ende */
      {
        i++ ; i++ ; j = 0 ;
        gewicht = atof ( zwipu ) ;
        sprintf ( zwipu , "%c%12.3lf\n" , art , gewicht ) ;
        switch ( art )
        {
        case 'B':
                wertb = gewicht ; break ;
        case 'N':
                wertn = gewicht ; break ;
        case 'T':
                wertt = gewicht ; break ;
        case 'E':
                werte = gewicht ; i-- ; i-- ; i-- ; break ; // 190412 : statusbits folgen ohne blank
                /*  hier folgt keine Einheit mehr  */

        }
        fprintf ( fpergeb , zwipu ) ; 
        l = l++ ;
      }
      continue ;
    }
    if ( j == 3 )
    { if ( zch == 0x03 || zch == CRi ) j = 0 ;  
      continue ;
    }
  }
  fclose ( fpergeb ) ;
  if ( l )  
  { 
     sprintf ( logtext , "brutto %lf ,netto %lf , tara %lf, eich %lf \n" , wertb,wertn,wertt,werte ) ;
     logschreib () ;
     return ( 0 ) ;
  } 
  return ( -1 ) ;
}

/* nap und sleep gibbet wohl nicht unter win */
long nap ( long n ) 
{ 
  Sleep ((DWORD) n );
  return ( (long) 0);
}

void sleep (unsigned secund ) 
{ 
 long ret=0L ;
 ret= nap( 1000*secund) ;
 return ;
}

int quitN ()
{
   return (0);
}

int quitT ()
{
   return (wartack(500));
   return (0);
}

int wartack (int wartemillis )
{
int i, retanz ;
unsigned ch ;
// entspr. Inputwartemillis = 100 ;     // das ist ja wohl lang genug
/*  Endekennung 2400 ca. 4 millsek/byte)-> hier soll nur auf ACK-telegramm
        reagiert werden */                              /* 081008 tfaktor */
    a_CommTmo.ReadIntervalTimeout = 30 * tfaktor ;         /* 30 millisec -> Endekenn !!*/ 
    a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;          /* x 2 ( Zeit je Byte+x */ 
    a_CommTmo.ReadTotalTimeoutConstant = wartemillis ;
/* 100199 : frueher fix auf  500 ;  0,5 sek warten (RITO) */
    a_CommTmo.WriteTotalTimeoutMultiplier = 100 ;       /* x 100 */ 
    a_CommTmo.WriteTotalTimeoutConstant = 100;          /* 100 millisec */                    
    fSuccess = SetCommTimeouts ( schnittstelle, &a_CommTmo ) ;
    if ( ! fSuccess ) return ( (int) 18 ) ;

    memset ( wertbuffer , '\0' , 120 ) ;
    retanz = os_read ( schnittstelle, wertbuffer , 1 ) ;
    sprintf ( logtext , "wartack : WEPU : %s\n", wertbuffer ) ;
    logschreib () ;


    ch = NAKi ;         // "irgendein" ack aus dem Empfangspuffer
    for ( i=0 ; i < retanz;i++)
    {
      ch = wertbuffer[i] ;
      if ( ch == ACKi ) break ;
    }

//    if ( retanz == 0 ) ch = NAKi  ; // timeout 

    /* Das sind die Werte fuer 9600 */                  /* 081008 tfaktor */
    a_CommTmo.ReadIntervalTimeout = 30 * tfaktor ;         /* 30 millisec -> Endekenn !!*/ 
    a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;          /* x 2 ( Zeit je Byte+x */ 
    a_CommTmo.ReadTotalTimeoutConstant = 30000 ; /* 30 sek warten */ /* 30 millisec (RITO) */ 
    a_CommTmo.WriteTotalTimeoutMultiplier = 100 ;       /* x 100 */
    a_CommTmo.WriteTotalTimeoutConstant = 100;          /* 100 millisec */                    
    switch ( edv_conf.baudrate )
    {
      case 2400 :
          a_CommTmo.ReadIntervalTimeout = 50 * tfaktor ;         /* 50 millisec -> Endekenn !! 081008 */
          a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;          /* x 2 ( Zeit je Byte+x */ 
        break ;
      case 4800 :
          a_CommTmo.ReadIntervalTimeout = 40 * tfaktor ;         /* 40 millisec -> Endekenn !! 081008 */
          a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;          /* x 2 ( Zeit je Byte+x */ 
        break ;
    }
    fSuccess = SetCommTimeouts ( schnittstelle, &a_CommTmo ) ;
    if ( ! fSuccess ) return ( (int) 19 ) ;

    if ( ch == ACKi ) return ( 0 ) ;

    return ( -33 ) ;
}

int leseleer (int wartemillis )
{
int retanz ;
/*  Endekennung 2400 ca. 4 millsek/byte)  reagiert werden */
    a_CommTmo.ReadIntervalTimeout = wartemillis;
    a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;
    a_CommTmo.ReadTotalTimeoutConstant = wartemillis + 1 ;
    a_CommTmo.WriteTotalTimeoutMultiplier = 100 ;       /* x 100 */ 
    a_CommTmo.WriteTotalTimeoutConstant = 100;          /* 100 millisec */                    
    fSuccess = SetCommTimeouts ( schnittstelle, &a_CommTmo ) ;
    if ( ! fSuccess ) return ( (int) 18 ) ;

    memset ( wertbuffer , '\0' , 100 ) ;
    retanz = os_read ( schnittstelle, wertbuffer , 95 ) ;
    sprintf ( logtext , "leseleer : WEPU : %s\n", wertbuffer ) ;
    logschreib () ;

    /* Das sind die Werte fuer 9600 */                  /* 081008 tfaktor */
    a_CommTmo.ReadIntervalTimeout = 30 * tfaktor ;         /* 30 millisec -> Endekenn !!*/ 
    a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;          /* x 2 ( Zeit je Byte+x */ 
    a_CommTmo.ReadTotalTimeoutConstant = 20000 ; /* 20 sek warten */ /* 30 millisec (RITO) */ 
// Uebersteuert die 30 sek aus initschnitt !!
    a_CommTmo.WriteTotalTimeoutMultiplier = 100 ;       /* x 100 */
    a_CommTmo.WriteTotalTimeoutConstant = 100;          /* 100 millisec */                    
    switch ( edv_conf.baudrate )
    {
        case 2400 :
            a_CommTmo.ReadIntervalTimeout = 50 * tfaktor ;         /* 50 millisec -> Endekenn !! 081008 */
            a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;          /* x 2 ( Zeit je Byte+x */ 
                break ;
        case 4800 :
            a_CommTmo.ReadIntervalTimeout = 40 * tfaktor ;         /* 40 millisec -> Endekenn !! 081008 */
            a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;          /* x 2 ( Zeit je Byte+x */ 
                break ;
    }
    fSuccess = SetCommTimeouts ( schnittstelle, &a_CommTmo ) ;
    if ( ! fSuccess ) return ( (int) 19 ) ;
    return ( 0 ) ; 
}

void acksend (void)
{
char acck [2] ;
int retanz ;
// return ;
sprintf ( logtext , "acksend \n" ) ;
logschreib () ;
acck[0] = ACKi /* 0x06 */ ;
acck[1] = '\0' ;
retanz = os_write (schnittstelle, acck , 1 ) ; 
}
