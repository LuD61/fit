# Makefile fuer "fnb.exe": Funktionen fuer angeschlossene Waagen (vorerst ITL)

# SE.TEC

.SUFFIXES: .obj .c

#CC   = bcc -ml
CC = cl
#LINK = bcc
LINK = link

#-DLDEU=1
CFLAGS = -c -W3 -DMAINI=1 -DWIN=1 -DWIN32=1 -DITL=1 -DSTBIZ=1 -Ic:\user\cs\dummywaage
PROGRAMM = fnbdummy.exe 
GUILIBS= user32.lib gdi32.lib winmm.lib comdlg32.lib comctl32.lib
RCVARS=-r -DWIN32

OBJ = fnbdummy.obj \
	global.obj \
	get_defa.obj


$(PROGRAMM): $(OBJ)
         $(CC) -Fefnbdummy.exe $(OBJ) $(GUILIBS)
.c .obj :
	$(CC) $(CFLAGS) $*.c


#CLEAN:
#        del $(OBJ)
