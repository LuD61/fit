// dummywaageDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "dummywaage.h"
#include "dummywaageDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


void CdummywaageDlg::OnStartTimer() 
{
   m_nTimer = SetTimer(1, 500, 0);
}

void CdummywaageDlg::OnStopTimer() 
{
   KillTimer(m_nTimer);   
}

void CdummywaageDlg::OnTimer(UINT nIDEvent) 
{
//   MessageBeep(0xFFFFFFFF);   // Beep
   lesedatei () ;
   UpdateData(FALSE) ;

}




// CdummywaageDlg-Dialogfeld



CdummywaageDlg::CdummywaageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CdummywaageDlg::IDD, pParent)
	, v_brutto(0)
	, v_netto(0)
	, v_tara(0)
	, v_action(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CdummywaageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDCBRUTTO, m_brutto);
	DDX_Text(pDX, IDCBRUTTO, v_brutto);
	DDX_Control(pDX, IDC_NETTO, m_netto);
	DDX_Text(pDX, IDC_NETTO, v_netto);
	DDX_Control(pDX, IDC_TARA, m_tara);
	DDX_Text(pDX, IDC_TARA, v_tara);
	DDX_Control(pDX, IDC_AKTION, m_action);
	DDX_Text(pDX, IDC_AKTION, v_action);
}

BEGIN_MESSAGE_MAP(CdummywaageDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_EN_KILLFOCUS(IDCBRUTTO, &CdummywaageDlg::OnEnKillfocusCbrutto)
	ON_EN_KILLFOCUS(IDC_NETTO, &CdummywaageDlg::OnEnKillfocusNetto)
	ON_EN_KILLFOCUS(IDC_TARA, &CdummywaageDlg::OnEnKillfocusTara)
	ON_BN_CLICKED(IDOK, &CdummywaageDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CdummywaageDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_SETZEN, &CdummywaageDlg::OnBnClickedSetzen)
	ON_BN_CLICKED(IDC_TLOESCH, &CdummywaageDlg::OnBnClickedTloesch)
	ON_WM_TIMER () 
	ON_EN_SETFOCUS(IDCBRUTTO, &CdummywaageDlg::OnEnSetfocusCbrutto)
	ON_EN_SETFOCUS(IDC_NETTO, &CdummywaageDlg::OnEnSetfocusNetto)
	ON_EN_SETFOCUS(IDC_TARA, &CdummywaageDlg::OnEnSetfocusTara)
END_MESSAGE_MAP()


void CdummywaageDlg::lesedatei()
{
	v_netto = 0 ;
	v_brutto = 0 ;
	v_tara = 0 ;
	v_action = "" ;

// erstelle Dateiname, lese alle Werte

	FILE *fp ;
	char *waakt;
    char ibuffer [512];

    waakt = getenv ("WAAKT");
    if (waakt == (char *) 0)
    {
		waakt = "C:\\WAAKT";
    }

	sprintf (ibuffer, "%s\\dummywaage.txt", waakt ) ;
	fp = fopen (ibuffer, "r");
    if (fp == NULL)	// nichts gefunden .....
	{
		return ;
	}

    if ( fgets (ibuffer, 511, fp))
    {
		for ( int i = 0 ; i < 4 ; i++ )
		{
			// 1. Zeile = brutto 
			if ( i == 0 )
			{
				v_brutto = atof ( ibuffer ) ;
				if ( fgets (ibuffer, 511, fp)) continue; else break ;
	
			}
			if ( i == 1 )
			{
				v_netto = atof ( ibuffer ) ;
				if ( fgets (ibuffer, 511, fp)) continue; else break ;
	
			}
			if ( i == 2 )
			{
				v_tara = atof ( ibuffer ) ;
				if ( fgets (ibuffer, 511, fp)) continue; else break ;
	
			}
			if ( i == 3 )
			{
				ibuffer[ 105] = '\0' ;	// Notbremse
				v_action.Format ( "%s" , ibuffer) ;
			}
		}
	}
	fclose (fp) ;

}

void CdummywaageDlg::schreibedatei()
{
// erstelle Dateiname , schreibe alle Werte



	FILE *fp ;
	char *waakt;
    char ibuffer [512];

    waakt = getenv ("WAAKT");
    if (waakt == (char *) 0)
    {
                    waakt = "C:\\WAAKT";
    }

	sprintf (ibuffer, "%s\\dummywaage.txt", waakt ) ;
	unlink( ibuffer ) ;
	fp = fopen (ibuffer, "w+");
    if (fp == NULL)	// was kaputt .....
	{
		return ;
	}

	sprintf ( ibuffer, "%1.3f\n", v_brutto ) ;
	fputs( ibuffer , fp ) ;
	sprintf ( ibuffer, "%1.3f\n", v_netto ) ;
	fputs( ibuffer , fp ) ;
	sprintf ( ibuffer, "%1.3f\n", v_tara ) ;
	fputs( ibuffer , fp ) ;

	sprintf ( ibuffer, "%s", v_action.GetBuffer() );
	fputs( ibuffer , fp ) ;
	fclose (fp) ;

}


// CdummywaageDlg-Meldungshandler

BOOL CdummywaageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen
	lesedatei () ;
	UpdateData(FALSE) ;
	schreibedatei () ;
	OnStartTimer() ;

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CdummywaageDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CdummywaageDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CdummywaageDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CdummywaageDlg::OnEnKillfocusCbrutto()
{

	OnStopTimer() ;
	UpdateData(TRUE) ;
	if ( v_brutto != a_brutto )
	{
		v_netto = v_brutto - v_tara ;
		v_action.Format ("Manuell brutto : %1.3f", v_brutto ); 
		schreibedatei() ;
	}

	UpdateData(FALSE) ;
	OnStartTimer() ;
}

void CdummywaageDlg::OnEnKillfocusNetto()
{
	OnStopTimer() ;
	UpdateData(TRUE) ;

	if ( v_netto != a_netto )
	{ 
		v_brutto = v_netto + v_tara ;
		v_action.Format ("Manuell netto : %1.3f", v_netto ); 
		schreibedatei() ;
	}

	UpdateData(FALSE) ;
	OnStartTimer() ;
}

void CdummywaageDlg::OnEnKillfocusTara()
{
	OnStopTimer() ;
	UpdateData(TRUE) ;
	if ( v_tara != a_tara )
	{
		v_netto = v_brutto - v_tara ;
		v_action.Format ("Manuell tara : %1.3f", v_tara ); 
		schreibedatei() ;
	}
	OnStartTimer() ;
	UpdateData(FALSE) ;
}

void CdummywaageDlg::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	//	OnOK();
}

void CdummywaageDlg::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	OnStopTimer() ;
	OnCancel();
}

void CdummywaageDlg::OnBnClickedSetzen()
{
	OnStopTimer() ;
	UpdateData(TRUE) ;

	v_tara = v_brutto ;
	v_netto = 0 ;
	v_action.Format ("Alles ins tara : %1.3f", v_tara ); 
	schreibedatei() ;

	UpdateData(FALSE) ;
	OnStartTimer() ;

}

void CdummywaageDlg::OnBnClickedTloesch()
{
	OnStopTimer() ;

	UpdateData(TRUE) ;

	v_netto = v_brutto ;
	v_tara = 0 ;
	v_action.Format ("Tara l�schen : %1.3f", v_brutto ); 
	schreibedatei() ;

	UpdateData(FALSE) ;
	OnStartTimer() ;
}

void CdummywaageDlg::OnEnSetfocusCbrutto()
{
	UpdateData(TRUE) ;
	a_brutto = v_brutto ;
	OnStopTimer() ;
}

void CdummywaageDlg::OnEnSetfocusNetto()
{
	UpdateData(TRUE) ;
	a_netto = v_netto ;
	OnStopTimer() ;
}

void CdummywaageDlg::OnEnSetfocusTara()
{
	UpdateData(TRUE) ;
	a_tara = v_tara ;
	OnStopTimer() ;
}
