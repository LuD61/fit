// dummywaageDlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"


// CdummywaageDlg-Dialogfeld
class CdummywaageDlg : public CDialog
{
// Konstruktion
public:
	CdummywaageDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_DUMMYWAAGE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnKillfocusCbrutto();
	afx_msg void OnEnKillfocusNetto();
	afx_msg void OnEnChangeTara();
	afx_msg void OnEnKillfocusTara();
	CEdit m_brutto;
	double v_brutto;
	double a_brutto;
	CEdit m_netto;
	double v_netto;
	double a_netto;
	CEdit m_tara;
	double v_tara;
	double a_tara;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();


	void lesedatei(void) ;
	void schreibedatei(void) ;
	UINT_PTR m_nTimer;
	void OnStartTimer() ; 
	void OnStopTimer() ; 

	 
	afx_msg void OnTimer(UINT_PTR nIDEvent ) ;  

	CEdit m_action;
	CString v_action;
	afx_msg void OnBnClickedSetzen();
	afx_msg void OnBnClickedTloesch();
	afx_msg void OnEnSetfocusCbrutto();
	afx_msg void OnEnSetfocusNetto();
	afx_msg void OnEnSetfocusTara();
};
