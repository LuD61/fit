#ifdef  IDENT 
static char ident[] = "@(#) fnb.c  VLUGLOTSA   16.06.98 by SE.TEC"
#endif
/***************************************************************************/
/* Programmname :     fnb.c                                                */
/*-------------------------------------------------------------------------*/
/* Funktion :        Applikationsschnittstelle (API)                       */
/*                   Initialisierung der Schnittstelle und des Geraetes    */
/*-------------------------------------------------------------------------*/
/* Revision : 1.01    Datum : 08.07.98  erstellt von :  SE.TEC            */
/*-------------------------------------------------------------------------*/
/* Kurzbeschreibung : Hauptprogramm                                        */
/*                                                                         */
/* - Lesen der Schnittstellenparameter                                     */
/* - Senden der Geraete-Initialisierung, Ergebnisbewertung                 */
/*-------------------------------------------------------------------------*/
/* Deklaration: void main(int argc, char *argv[])                          */
/*                                                                         */                                          
/* Eingabe : par1      :  fnb ->                                           */
/* fnb00 : Init der Schnittstelle und des Geraetes Return : errcode        */
/* fnb02 : Fehlerkorrektur + Errcode                                       */
/* fnb03 : Nullstellen + Errcode                                           */
/* fnb04 : Tarieren + Errcode                                              */
/* fnb05 : Tara loeschen + Errcode                                         */                    
/* fnb06 : Waagenumschaltung                                               */  
                                                 
/* fnb08 : wiegen MIT Ruhewertung (ohne ist Mist !!) Return : Gewichtswert */
/* fnb11 : Postenregistrierung                                             */
/* fnb23 : Tarieren mit Wertvorgabe                                        */
/*                                                                         */
/* Eingabe : par2 : Dialog-Directory : da steht die Schnittstelleninfo !!  */
/* Eingabe : par3 : (OPTIONAL) Inputwert(Fixformat: VZ+5+.+3) (fnb6:1|2|3) */
/* Ausgabe : (OPTIONAL) outdatei ( mit Errcode + Wert )                    */
/*                                                                         */
/* EXIT Status       : TYerrno err: Fehlerstatus                           */
/*       = RET_OK : kein Fehler                                            */
/*       = 1    : Parameterfehler                                          */
/*       = 2    : sonstiger Fehler -> weiter spezifizierbar                */
/* Aufruf : fnb par1 par2 [ par3 ]                                        */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* Aenderungsstand :                                                       */
/* Revision : 1.0  Datum : dd.mm.yyyy  geaendert von :  SE.TEC             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* am 27.04.98 : nur ITL realisiert ->                                     */
/* 04.05.98 : Device-Lesen vorerst nur als dummy  */
/* 16.06.98 : ID7 - Variante ( vorerst ueber bedingte  Kompilierung ) */
/*       STANDARD INCLUDE'S               */
// 190700 : Einbau Datum + Uhrzeit wegen Eichspeich :

// Bedingung : Transferstring aufblasen und "deutsches Datum+Zeit" aktivieren

#include "getr_01.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>
#include "paras.h"

#ifdef DLL
#include "fnb.h"
#endif

DCB a_CommDcb ; /* Aktiv */
DCB g_CommDcb ; /* Originalsave */
COMMTIMEOUTS a_CommTmo ;
COMMTIMEOUTS g_CommTmo ;
BOOL fSuccess ;

int testxx=0 ;   /* oder 0 */
int testjg=0 ;   /* oder 0 */
int testschnitt=0  ;        //220699
static int schoninit = -1 ;
char wertbuffer [ 141] ;        // 041208
FILE *fperr ;
char logtext [122] ;    // 160108 : 120->122
/* externe Deklarationen                 */

extern char dialogdir[] ;
extern int fnb_nr ;
extern int dev_type ;
extern int sics ;       //290699 
extern int dhs ;        //130902
extern int kommastellen ;
extern int tfaktor ;    // 230608
extern TYedvConf edv_conf ;
extern TYedvChan edv_chan ;
extern void get_defax (void) ;

#define ACKi     0x06
#define CRi      0x0d
#define LFi      0x0a
#define SYNi     0x16

int initschnitt ( void ) ;
int fnb0  ( void )       ;  /* init */
int fnb2  ( void )       ;  /* init */
int fnb3  ( void )       ;  /* nullstellen */
int fnb4  ( void )       ;  /* tarieren */
int fnb5  ( void )       ;  /* tara loeschen */
int fnb6  ( int  )       ;  /* waage umschalten */
int fnb8  ( void )       ;  /* wiegen */
int fnb11 ( void )       ;  /* Postenreg. */
int fnb23 ( double)      ;  /* Tarieren mit Wert */
int fnb33 ( char *);      // Hilfsfunktion zur Eingabe beliebiger Strings

int fnb200( int , char *);      /* 200 String senden */
                                /* 199 : string zu AW090 ( Einschaltmeldung) */
                                /* fnb 201 bis 206 entsprechen einigen
                                   Dialog-modi des id7 */

double inpkonv ( char * ) ;
void logschreib ( void ) ;
u_long eigexit ( int ) ;
int itlgew ( int ) ;
int id7gew ( int , char * , int ) ;
int id7dialog ( int , char * , int ) ;
long nap (long) ;
void sleep ( unsigned );
char * itltar ( char * , double ) ;
int id7tar ( double ) ;
int holak ( int ) ;     /* #100199 */
int quittung ( int ) ;
int ergebnis ( double ergebmat[], char *ergebchar[] ) ;
void acksend (void) ;

double wertn ;  /* wert-netto  */
double wertb ;  /* wert-brutto */
double wertt ;  /* wert-tara   */
double werte ;  /* eichspeich  */
char lakeyi [2]  ;
char dialdatin[21] ;

int os_write (HANDLE fd , char *buf , unsigned nbyte ) ;
int os_read  (HANDLE fd , char *buf , unsigned nbyte ) ;

/*-------------------------------------------------------------------------*/

HANDLE schnittstelle;

#ifdef MAINI
void main (int argc , char *argv[] ) 
#else
#ifdef DLL
EXPORT int fnb (int argc, char *argv[])
#else
 int fnbi (int argc , char *argv[] )
#endif
#endif
{

  char  * srptr ;
  int strlaenge ;
  int retcode   ;
  retcode = 0 ;
// Identifikation ist wichtig .......... 
  if ( argc < 2 )
  {
#ifdef ID72
     printf ( " 281208: Ich bin ein FNB fuer ID2,ID5 ohne Appl.block in der Antwort \n" ) ;
     retcode = eigexit (1) ;
#endif
#ifdef ID7
     printf ( " 281208: Ich bin ein FNB fuer ID2,ID5,ID7 .. mit Appl.block in der Antwort \n" ) ;
     retcode = eigexit (1) ;
#endif
#ifdef ITL
     printf ( " 190700: Ich bin ein FNB fuer ITL+ITE ( ite per DEVI, dann mit Eichspeich) \n" ) ;
     retcode = eigexit (1) ;
#endif
  }

// 160699 A
  srptr = getenv("testmode");
  if (srptr == NULL)
  {
     srptr = getenv("testmade");
     if (srptr == NULL)
     {
        testxx = 0;
        testjg = 0;
     }
     else
     {
        testxx = atoi(srptr) ;
        testjg = testxx ;        // atoi(srptr) ;
     }
  }
  else
  {
        testxx = atoi(srptr) ;
        testjg = testxx  ;       // atoi(srptr) ;
  }

  if ( testxx > 0 )
  {
     if (testxx == 5 )
     {
        testschnitt = 1 ;
        testjg = 1 ;
        testxx = 0 ;
     }
     else
// testjg muss immer mit 1 anfangen,sonst wird die logfile nicht korrekt geoffnet
     { 
        testxx = 1 ; testjg = 1 ;
     }
  }
  else
  {
       testxx = 0 ; testjg = 0 ;
  }
// 160699 E

  if ( argc < 3 )     /* prog, fnb, dialogdir */ 
  { sprintf ( logtext , "Zuwenig Argumente : %d \n" , argc ) ;
    logschreib () ;
    retcode = eigexit ( 1 ) ;
  }
  if (! retcode )
  {
    fnb_nr = -1 ;
    fnb_nr = atoi ( argv[1] ) ;
    sprintf ( logtext , "FNB-Nummer : %d\n" , fnb_nr ) ;
    logschreib () ;
    srptr = argv[2] ;
    strlaenge = strlen ( srptr ) ;
    if ( strlaenge == 0 ) retcode = eigexit ( 3 ) ;
  } 
  if (!retcode)
  {
    if ( strlaenge > 127 ) strlaenge = 127 ; 
    memset ( dialogdir , '\0' , 128 ) ;
    memcpy ( dialogdir , srptr , strlaenge ) ;
    if ( strlaenge < 70 )
    {
      sprintf ( logtext , "%s\n" , dialogdir ) ;
    }
    else
    {
      sprintf ( logtext , "Langer directory-Name \n " ) ;
    }

    logschreib () ;

    strlaenge = initschnitt() ; 
    if ( strlaenge ) retcode = eigexit ( strlaenge ) ;
  } 
  if ( !retcode )
  {
    switch ( fnb_nr )
    { 
      case 0 :                  /* Initialisierung Schnittstelle + geraet */
        retcode = eigexit ( fnb0() ) ;  break ;

      case 2 :                  /* Fehler-reset */
        retcode = eigexit ( fnb2() ) ; break ;

      case 3 :                  /* Nullstellen */
        retcode = eigexit ( fnb3() ) ; break ;

      case 4 :                  /* Tarieren */
        retcode = eigexit ( fnb4() ) ; break ;

      case 5 :                  /* Tara loeschen */
        retcode = eigexit ( fnb5() ) ; break ;

      case 6 :                  /* Waagen-Umschaltung */
        if ( argc < 4 )
        { 
          sprintf ( logtext , " Keine Waagennummer zum Umschalten\n" );
          logschreib () ;
          retcode = eigexit ( 6 ) ; break ;
        }
        retcode = eigexit ( fnb6 ( atoi(argv[3]) ) ) ; break ;
      case 7 :    /* wird immer auf "mit Ruhewertung" gelegt */
      case 8 :                  /* Wiegen ohne Ruhewertung */
        retcode = eigexit ( fnb8() ) ; break ;

      case 11 :                  /* Postenregistrierung */
        retcode = eigexit ( fnb11() ) ; break ;

      case 23 :
        if ( argc < 4 )
        { 
          sprintf ( logtext , " Kein Wert : ich nehme 0 \n" );
          logschreib () ;
          retcode = eigexit ( fnb5() ) ; break ;
        }
        retcode = eigexit ( fnb23 ( inpkonv(argv[3]) ) ) ; break ;

// 290699 A

     case 33 :
        if ( argc < 4 )
        { 
           sprintf ( lakeyi ,"" ) ;
           memset ( dialdatin ,'\0' ,21 ) ;
           sprintf ( logtext , " Kein Wert : ich nehme Leerstring \n" );
           logschreib () ;
           retcode = eigexit ( fnb33 ( NULL) ) ; break ;
        }
        else
        {
           sprintf ( lakeyi , "" ) ;
           memset ( dialdatin ,'\0' ,21 ) ;
           retcode = eigexit ( fnb33 ( argv[3] ) ) ;
        }
        break ;
// 290699 E

     case 199 :         /* like 200 aber nach aw90 ( Text 20 )  */
     case 200 :         /* Display-String ohne zu warten */
     case 201 :         /* " " nur Display-string mit Quittung */
     case 202 :         /* "A" Alpha-Eingabe ->
                funkioniert nur 3 * im aktuellen firmware-Release  */
     case 203 :         /* "G" Genaral-Numeric */
     case 204 :         /* "R" Real-Numeric */
     case 205 :         /* "N" Natural-Numeric (0..9) */
     case 206 :         /* "Q"  Taste 0 / 1 (nein/ja) */
        if ( argc < 4 )
        { 
           sprintf ( lakeyi ,"" ) ;
           memset ( dialdatin ,'\0' ,21 ) ;
           sprintf ( logtext , " Kein Wert : ich nehme Leerstring \n" );
           logschreib () ;
           retcode = eigexit ( fnb200 ( fnb_nr , NULL) ) ; break ;
        }
        else
        {
           sprintf ( lakeyi , "" ) ;
           memset ( dialdatin ,'\0' ,21 ) ;
           retcode = eigexit ( fnb200 ( fnb_nr , argv[3]) ) ;
        }
        break ;

      default : retcode = eigexit ( (int) 4 ) ; break ; /* Error-return */
    }    /* Ende switch */        
  }     /* retcode */
#ifndef MAINI
 if ( testjg == 2 )
 {
  sprintf ( logtext , "exit mit %d\n" , retcode ) ;
  logschreib() ;
  fclose (fperr) ;
  testjg = 1 ;
 }
 return ( retcode ) ;
#endif

}       /* END main */  

/* ##################################################################### */
/* FNB0 : Initialisierung der Schnittstelle und des Geraetes             */
/* ##################################################################### */

fnb0() 
{
sprintf ( logtext , "fnb0 gestartet \n" ) ;
logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;
#ifdef ITL
 return ( fnb2 ()) ;
#endif
#ifdef ID7
 return ( fnb2 ()) ;
#endif
}       /* end fnb0 */

/* ##################################################################### */
/* FNB2 : Initialisierung der Schnittstelle und des Geraetes             */
/* ##################################################################### */

fnb2()
{
#ifdef ITL
 int retanz,reti ;
 char kdostring[10] ; 
 unsigned ch ;
#endif
  sprintf ( logtext , "fnb2 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

#ifdef ITL
  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
  ch = 0x71 ;     /* 'q' */
  kdostring[reti++] = ch ;
  ch = 0x20 ;     /* space */
  kdostring[reti++] = ch ;
  ch = CRi /* 0x0d */ ;
  kdostring[reti++] = ch ; 
  ch = LFi /* 0x0a */ ;
  kdostring[reti++] = ch ;
  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "Init retanz  : %d ; %s \n" , retanz, kdostring ) ;
  logschreib () ;
  if ( retanz == reti ) 
  {
  sleep ( 8 ) ;
  return ( 0 ) ;
  }
  return ( -1 ) ;
#endif
#ifdef ID7
  return ( holak (500) ) ;
#endif
}       /* end fnb2 */
/* ##################################################################### */
/* FNB3 : Nullstellen des Geraetes :                                     */
/* ##################################################################### */
fnb3()
{
#ifdef ITL
 int retanz, reti ;
 char kdostring[10] ; 
 unsigned ch ;
#endif
 sprintf ( logtext , "fnb3 gestartet \n" ) ;
 logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

#ifdef ITL
  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
  ch = 0x71 ;     /* 'q' */
  kdostring[reti++] = ch ;
  ch = 0x21 ;     /* '!' */
  kdostring[reti++] = ch ;
  ch = CRi /* 0x0d */ ;
  kdostring[reti++] = ch ; 
  ch = LFi /* 0x0a */ ;
  kdostring[reti++] = ch ;
  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "fnb3 retanz : %d ; %s \n" , retanz, kdostring ) ;
  logschreib () ;
  if ( retanz == reti ) return ( 0 ) ;
  return ( -1 ) ;
#endif
#ifdef ID7
/* Nicht realisiert, weil nicht benutzt */
        return ( 0 ) ;
#endif
}       /* end fnb3 */

/* ##################################################################### */
/* FNB4 : Tarieren                                                       */
/* ##################################################################### */
fnb4()
{

 int retanz,reti ;
 char kdostring[10] ; 
 unsigned ch ;
   sprintf ( logtext , "fnb4 gestartet \n" ) ;
   logschreib () ;
   memset ( kdostring , '\0' , 10 ) ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

#ifdef ITL
   reti = 0 ;
   ch = 'q' /* 0x71 */ ; 
   kdostring[reti++] = ch ;
   ch = 0x22 ;     /* '"' */
   kdostring[reti++] = ch ;
   ch = CRi /* 0x0d */ ;
   kdostring[reti++] = ch ; 
   ch = LFi /* 0x0a */ ;
   kdostring[reti++] = ch ;
   retanz = os_write (schnittstelle, kdostring , reti ) ; 
   sprintf ( logtext , "retanz : %d ; %s \n" , retanz, kdostring ) ;
   logschreib () ;
   if ( retanz == reti ) return ( 0 ) ;
   return ( -1 ) ;
#endif
#ifdef ID7
   reti = 0 ;
   ch = 'T' /* 0x54 */ ;
   kdostring[reti++] = ch ;
   ch = CRi /* 0x0d */ ;
   kdostring[reti++] = ch ; 
   ch = LFi /* 0x0a */ ;
   kdostring[reti++] = ch ;
   retanz = os_write (schnittstelle, kdostring , reti ) ;
   if ( retanz )
   {
      sprintf ( logtext , "Tarieren retanz : %d ; %s \n" , retanz,kdostring ) ;
   }
      else sprintf ( logtext , "Tarier-Schreiben-Error " ) ;
   logschreib () ;
   if ( retanz != reti ) return ( -1 ) ;
   return ( quittung ( 4 ) ) ;
#endif
}       /* end fnb4 */

/* ##################################################################### */
/* FNB5 : Tara Loeschen                                                  */
/* ##################################################################### */
fnb5()
{
 int retanz,reti ;
 char kdostring[10] ; 
 unsigned ch ;
  sprintf ( logtext , "fnb5 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;
#ifdef ITL
  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
  ch = 'q' /* 0x71 */ ;
  kdostring[reti++] = ch ;
  ch = 0x23 ;     /* '#' */
  kdostring[reti++] = ch ;
  ch = CRi /* 0x0d */ ;
  kdostring[reti++] = ch ; 
  ch = LFi /* 0x0a */ ;
  kdostring[reti++] = ch ;
  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "fnb5 : retanz : %d ; %s \n" , retanz, kdostring ) ;
  logschreib () ;
  if ( retanz == reti ) return ( 0 ) ;
  return ( -1 ) ;
#endif
#ifdef ID7
   memset ( kdostring , '\0' , 10 ) ;   // 220699 
   reti = 0 ;
   ch = 'T' /* 0x54 */ ;
   kdostring[reti++] = ch ;
   ch = 0x20 ;
   kdostring[reti++] = ch ;
   ch = CRi /* 0x0d */ ;
   kdostring[reti++] = ch ; 
   ch = LFi /* 0x0a */ ;
   kdostring[reti++] = ch ;
   retanz = os_write (schnittstelle, kdostring , reti ) ; 
   if ( retanz )
   {
      sprintf ( logtext , "retanz Tara loeschen : %d , %s \n" , retanz,kdostring ) ;
   }
      else sprintf ( logtext , "Tarier-Schreiben-Error " ) ;
   logschreib () ;
   if ( retanz != reti ) return ( -1 ) ;
   return ( quittung ( 5 ) ) ;
#endif 
}       /* end fnb5 */
/* ##################################################################### */
/* FNB6 : Waagenumschaltung                                              */
/* ##################################################################### */
fnb6( int waagennummer ) 
{
#ifdef ITL
 int retanz,reti ;
 char kdostring[15] ; 
 unsigned ch ;
#endif
#ifdef ID7      // realisiert 220699 
 int retanz,reti ;
 char kdostring[15] ; 
 unsigned ch ;
#endif

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

  sprintf ( logtext , "fnb6 gestartet \n" ) ;
  logschreib () ;
#ifdef ITL
  memset ( kdostring , '\0' , 15 ) ;
  reti = 0 ;
  ch = 'q' /* 0x71 */ ;
  kdostring[reti++] = ch ;
  ch = 'V' /* 0x56 */ ;
  kdostring[reti++] = ch ;
  ch = '1' /* 0x31 */ ;
  if ( waagennummer == 1 ) ch = 0x31 ;
  if ( waagennummer == 2 ) ch = 0x32 ;
  if ( waagennummer == 3 ) ch = 0x33 ;
  kdostring[reti++] = ch ;
  ch = CRi /* 0x0d */ ;
  kdostring[reti++] = ch ; 
  ch = LFi /* 0x0a */ ;
  kdostring[reti++] = ch ;
  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "fnb6 retanz : %d ; %s \n" , retanz, kdostring ) ;
  logschreib () ;
  if ( retanz == reti ) return ( 0 ) ;
  return ( -1 ) ;
#endif
#ifdef ID7
// realisiert 220699 : AW010...  Antowrt : SA_n
  memset ( kdostring , '\0' , 15 ) ;
  reti = 0 ;
  ch = 'A' /* 0x41 */ ;                    /* 'A' */
  kdostring[reti++] = ch ;
  ch = 'W' /* 0x57 */ ;                    /* 'W' */
  kdostring[reti++] = ch ;
  ch = '0' /* 0x30 */ ;                    /* '0' */
  kdostring[reti++] = ch ;
  ch = '1' /* 0x31 */ ;                    /* '1' */
  kdostring[reti++] = ch ;
  ch = '0' /* 0x30 */ ;                    /* '0' */
  kdostring[reti++] = ch ;
  ch = ' ' /* 0x20 */ ;                    /* ' ' */       
  kdostring[reti++] = ch ;
  ch = '1' /* 0x31 */ ;
  if ( waagennummer == 1 ) ch = 0x31 ;
  if ( waagennummer == 2 ) ch = 0x32 ;
  if ( waagennummer == 3 ) ch = 0x33 ;
  kdostring[reti++] = ch ;
  ch = CRi /* 0x0d */ ;
  kdostring[reti++] = ch ; 
  ch = LFi /* 0x0a */ ;
  kdostring[reti++] = ch ;

  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "fnb6 retanz : %d ; %s \n" , retanz , kdostring ) ;
  logschreib () ;
  if ( retanz != reti ) return ( -1 ) ;
  return (quittung (6) ) ;
        return ( 0 ) ;
#endif
}       /* end fnb6 */
/* ##################################################################### */
/* FNB8 : Wiegen (mit Ruhewertung)                                       */
/* ##################################################################### */
fnb8()
{
// #ifdef ITL
 int retanz,reti ;
 char kdostring[10] ; 
 unsigned ch ;
// #endif
  sprintf ( logtext , "fnb8 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;
#ifdef ITL
  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
  ch = 'q' /* 0x71 */ ;     /* 'q' */
  kdostring[reti++] = ch ;
  ch = '$' /* 0x24 */ ;     /* '$' */
  kdostring[reti++] = ch ;
  ch = CRi /* 0x0d */ ;
  kdostring[reti++] = ch ; 
  ch = LFi /* 0x0a */ ;
  kdostring[reti++] = ch ;
  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "retanz Init : %d \n" , retanz ) ;
  logschreib () ;
  if ( retanz != reti ) return ( -1 ) ;
  memset ( wertbuffer , '\0' , 120 ) ;
  retanz = os_read ( schnittstelle, wertbuffer , 80 ) ;
  sprintf ( logtext , "%s" , wertbuffer ) ;
  logschreib () ;
  if ( retanz < 12 )  return ( 12 ) ;   /* Errorreturn */
  return ( itlgew(retanz) ) ;
#endif
#ifdef ID7
  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
  ch = 0x53 ;     /* 'S' -> Lesen mit Ruhewertung(SI = dynamisch) */
  kdostring[reti++] = ch ;
  ch = 0x49 ;
  kdostring[reti++] = ch ; 
  ch = CRi /* 0x0d */ ;
  kdostring[reti++] = ch ; 
  ch = LFi /* 0x0a */ ;
  kdostring[reti++] = ch ;

  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "retanz Init : %d \n" , retanz ) ;
  logschreib () ;
  if ( retanz != reti ) return ( -1 ) ;
  return (quittung (8) ) ;
#endif

#ifdef ID7
/* mit Reg. gibt es hier nicht extra ??-> doch siehe oben  */
        return ( fnb11() ) ;
#endif
}       /* end fnb8 */
/* ##################################################################### */
/* FNB11 : Postenregistrierung                                           */
/* ##################################################################### */
fnb11()
{
 int retanz,reti ;
  char kdostring[15] ; 
  unsigned ch ;
  sprintf ( logtext , "fnb11 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;
#ifdef ITL
  memset ( kdostring , '\0' , 15 ) ;
  reti = 0 ;
  ch = 'q' /* 0x71 */ ;      /* "q" */
  kdostring[reti++] = ch ;
  ch = 'Z' /* 0x5a */ ;      /* "Z" */
  if ( dev_type == GE_ITE )
  {
     ch = 'Y' /* 0x59 */ ; /* "Y" -> addierend fuer ite, loest eichspeich aus */
  }
  kdostring[reti++] = ch ;
  ch = CRi /* 0x0d */ ;
  kdostring[reti++] = ch ; 
  ch = LFi /* 0x0a */ ;
  kdostring[reti++] = ch ;
  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "retanz Init : %d \n" , retanz ) ;
  logschreib () ;
  if ( retanz != reti ) return ( -1 ) ;
  memset ( wertbuffer , '\0' , 120 ) ;
  retanz = os_read ( schnittstelle, wertbuffer , 80 ) ;
  sprintf ( logtext , "%s" , wertbuffer ) ;
  logschreib () ;
  if ( retanz < 12 )  return ( 12 ) ;   /* Errorreturn */
  return ( itlgew(retanz) ) ;
#endif
#ifdef ID7
  memset ( kdostring , '\0' , 15 ) ;
  reti = 0 ;
  ch = 'S' /* 0x53 */ ;     /* 'S' */
  kdostring[reti++] = ch ;
  ch = 'X' /* 0x58 */ ;     /* 'X' */
  kdostring[reti++] = ch ;
  ch = CRi /* 0x0d */ ;
  kdostring[reti++] = ch ; 
  ch = LFi /* 0x0a */ ;
  kdostring[reti++] = ch ;
  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "fnb11 retanz Init : %d %s \n" , retanz , kdostring ) ;
  logschreib () ;
  if ( retanz != reti ) return ( -1 ) ;
  return (quittung (11) ) ;
#endif
}       /* end fnb11 */

/* ##################################################################### */
/* FNB23 : Tarieren mit Wertvorgabe                                      */
/* ##################################################################### */
fnb23 ( double targew )
{
#ifdef ITL
int retanz ;
char kdostring[20] ;
#endif

  sprintf ( logtext , "fnb23 gestartet \n" ) ;
  logschreib () ;

wertb = -9999.9999 ;
wertn = -9999.9999 ;
wertt = -9999.9999 ;
werte = -9999.9999 ;

#ifdef ITL
  if ( targew == 0.0L ) { return (fnb5() ) ;}   /* Targew = 0 erzeugt Fehler */
  retanz = os_write (schnittstelle, itltar (kdostring ,targew ) , 14 ) ; 
  sprintf ( logtext , "retanz Init : %d \n" , retanz ) ;
  logschreib () ;
  if ( retanz != 14 ) return ( -1 ) ;
  return ( 0 ) ;
#endif
#ifdef ID7
  sprintf ( logtext , "targew : %lf \n" , targew ) ;
  logschreib () ;
  if ( targew == 0.0L ) { return (fnb5() ) ;}
  return ( id7tar ( targew ) ) ;
#endif
}       /* end fnb23 */


// 290699 A

/* ##################################################################### */
/* FNB33 : Test-Tool : beliebigen String senden und Anrwort leerpollen   */
/* ##################################################################### */
fnb33(char * eingabe )
{
   int retanz ;
   int zeig, quell ;
   char kdostring[100] ; 
   unsigned ch ;
   if ( eingabe == NULL )
   {
       retanz = 0 ;
       sprintf ( logtext , "fnb33 gestartet : Leerstring \n" ) ;
   }
   else
   {
       retanz = strlen ( eingabe ) ;
       sprintf ( logtext , "fnb33 gestartet : %s \n" , eingabe ) ;
   }
   logschreib () ;

   zeig = 0 ;
   quell = 0 ;
   memset ( kdostring , '\0' , 100 ) ;
#ifdef ID7
   while ( quell < retanz )
   {
      ch = eingabe[quell++] ;
      if ( ch < 0x20 ) ch = 0x20 ;
      if ( ch > 0x7f ) ch = 0x20 ;
      kdostring[zeig++] = ch ;
   }

   ch = CRi /* 0x0d */ ;
   kdostring[zeig++] = ch ; 
   ch = LFi /* 0x0a */ ;
   kdostring[zeig++] = ch ;
   retanz = os_write (schnittstelle, kdostring , zeig ) ; 
   sprintf ( logtext , "retanz 33 : %d , KDO : %s \n" , retanz, kdostring ) ;
   logschreib () ;
   if ( retanz != zeig ) return ( -1 ) ;
   return (id7dialog (33, kdostring , zeig ) ) ;
#endif

#ifdef ITL
   while ( quell < retanz )
   {
      ch = eingabe[quell++] ;
      if ( ch < 0x20 ) ch = 0x20 ;
      if ( ch > 0x7f ) ch = 0x20 ;
      kdostring[zeig++] = ch ;
   }

   ch = CRi /* 0x0d */ ;
   kdostring[zeig++] = ch ; 
   ch = LFi /* 0x0a */ ;
   kdostring[zeig++] = ch ;
   retanz = os_write (schnittstelle, kdostring , zeig ) ; 
   sprintf ( logtext , "retanz 33 : %d , KDO : %s \n" , retanz, kdostring ) ;
   logschreib () ;
   if ( retanz != zeig ) return ( -1 ) ;
   retanz = os_read ( schnittstelle, wertbuffer , 80 ) ;
   return ( 0 ) ;
#endif
}       /* end fnb33 */

// 290699 E

/* ##################################################################### */
/* FNB200 : Texte laden und Werte holen                                  */
/* ##################################################################### */
fnb200(int imodus, char * eingabe )
{
#ifdef ID7
   int retanz ;
   int zeig, quell ;
   char kdostring[30] ; 
   unsigned ch ;
#endif
   sprintf ( logtext , "fnb200 gestartet imodus : %d \n" , imodus ) ;
   logschreib () ;

#ifdef ID7
   zeig = 0 ;
   memset ( kdostring , '\0' , 30 ) ;
//   ch = ACK ;
//   kdostring[zeig++] = ch ;
   ch = 'D' ;     /* 0x44 */
   kdostring[zeig++] = ch ;
   if ( imodus == 199 )
   { zeig = 0 ;
        ch = 'A' /* 0x41 */ ; kdostring[zeig++] = ch ;
        ch = 'W' /* 0x57 */ ; kdostring[zeig++] = ch ;
        ch = '0' /* 0x30 */ ; kdostring[zeig++] = ch ;
        ch = '9' /* 0x39 */ ; kdostring[zeig++] = ch ;
        ch = '0' /* 0x30 */ ; kdostring[zeig++] = ch ;
   }
   switch (imodus)
   {
     case 200 :         /* " " nur Display-string */
     case 201 :         /* " " nur Display-String (mit Quittung) */
            break ;
     case 202 :         /* "A" Alpha-Eingabe */
        ch = 'A' /* 0x41 */ ; kdostring[zeig++] = ch ; break ;

     case 203 :         /* "G" Genaral-Numeric */
        ch = 'G' /* 0x47 */ ; kdostring[zeig++] = ch ; break ;

     case 204 :         /* "R" Real-Numeric */
        ch = 'R' /* 0x52 */ ; kdostring[zeig++] = ch ; break ;

     case 205 :         /* "N" Natural-Numeric (0..9) */
        ch = 'N' /* 0x4e */ ; kdostring[zeig++] = ch ; break ;

     case 206 :         /* "Q"  Taste 0 / 1 (nein/ja) */
        ch = 'Q' /* 0x51 */ ; kdostring[zeig++] = ch ; break ;
   } ;

   ch = 0x20 ;     /* ' ' */
   kdostring[zeig++] = ch ;

   if ( eingabe == NULL )
   {
      ch = 0x20 ;
//      kdostring[zeig++] = ch ;
   }
   else
   {
      retanz = strlen ( eingabe ) ;
      quell = 0 ;
      if ( retanz > 20 ) retanz = 20 ;
      while ( quell < retanz )
      {
         ch = eingabe[quell++] ;
         if ( ch < 0x20 ) ch = 0x20 ;
         if ( ch > 0x7f ) ch = 0x20 ;
         kdostring[zeig++] = ch ;
      }
   }

   ch = CRi      /* 0x0d */ ;
   kdostring[zeig++] = ch ; 
   ch = LFi      /* 0x0a */ ;
   kdostring[zeig++] = ch ;
   retanz = os_write (schnittstelle, kdostring , zeig ) ; 
   sprintf ( logtext , "retanz Init : %d , KDO : %s \n" , retanz, kdostring ) ;
   logschreib () ;
   if ( retanz != zeig ) return ( -1 ) ;
   return (id7dialog (imodus, kdostring , zeig ) ) ;
#endif
}       /* end fnb200 */

int ergebnis ( double ergebmat[4] , char * ergebchar[2] ) 
{
 int gefunden ;
  gefunden = 0 ;
  ergebmat[0] = wertb ;
  ergebmat[1] = wertn ;
  ergebmat[2] = wertt ;
  ergebmat[3] = werte ;

  if ( wertb != -9999.9999L )  gefunden = 1 ;
  if ( wertn != -9999.9999L )  gefunden = 1 ;
  if ( wertt != -9999.9999L )  gefunden = 1 ;

  sprintf ( ergebchar[0] , "%s" , lakeyi ) ;

  if ( ( (int) strlen ( dialdatin)) > 0)
     sprintf ( ergebchar[1] , "%s" , dialdatin  ) ;
  else
  { 
    sprintf ( ergebchar[1] , "" , dialdatin ) ;
  } 
  return gefunden ;
}

initschnitt()
{  

if ( schoninit > 0 ) return ( 0 ) ;

if ( testschnitt )
{
   printf ( "INITIALISIERUNG\n" ) ;  
   sprintf ( logtext , "INITIALISIERUNG\n" ) ; 
   logschreib();
}
schoninit = 1 ;
edv_conf.baudrate = 9600 ;
edv_conf.databits = 7 ;
edv_conf.parity = EVEN ;
edv_conf.stopbits = 1 ;
edv_conf.flowcontrol = 1 ;
sprintf ( edv_chan.ddId , "COM1" ) ;
kommastellen = 2 ;
get_defax () ;
// 041208 A  testxx wird gegbnenfalls modifiziert,
// die lokale Einstellung kann nur  einschalten oder aendern
  if ( testxx > 0 )
  {
     if ( testjg > 0 )  // es ist bereits vorher was gelaufen
     {
        if ( testxx == 5 )
        {
           testxx = 0 ; testschnitt = 1 ;
        }
        else
        {
           testxx = 1; testschnitt = 0 ;     
        }
     }
     else       // erstmalig aktivieren
     {
        testjg = 1 ;
        if ( testxx == 5 )
        {
           testxx = 0 ; testschnitt = 1 ;
        }
        else
        {
           testxx = 1 ; testschnitt = 0 ;     
        }
     }
  }
  else
  {
     if (( testjg > 0 ) && (testschnitt == 0 ))
        testxx = 1 ;    // Ausgangszustand herstellen
  }
// 251008

// 041208 E
sprintf ( logtext , "Vor CreateFile \n" ) ;
logschreib () ;
    schnittstelle = CreateFile ( (LPCTSTR) edv_chan.ddId ,
                        GENERIC_READ | GENERIC_WRITE ,
                        0 , /* prevents the file from being shared */
                        NULL ,  /* no security attributes */
                        OPEN_EXISTING , /* comm devices must use "OPEN_EXISTING */
                        0 , /* not overlapped I/O */
                        NULL /* hTemplate must be NULL for comm devices */
                        ) ;
    if ( schnittstelle == INVALID_HANDLE_VALUE ) 
    return ( (int) 12 ) ;

    memset (( void *) &a_CommDcb, '\0', sizeof (a_CommDcb)) ;
    memset (( void *) &a_CommTmo, '\0', sizeof (a_CommTmo)) ;
    sprintf ( logtext , "Vor GetCommState \n" ) ;
    logschreib () ;
    fSuccess = GetCommState(schnittstelle, &g_CommDcb) ;
    if ( fSuccess )
    {  
        a_CommDcb = g_CommDcb ;
    }
    else
    {
        return ( (int) 13 ) ;
    }
    sprintf ( logtext , "Vor GetTimeouts \n" ) ;
    logschreib () ;
    fSuccess = GetCommTimeouts(schnittstelle, &g_CommTmo) ;
    if ( fSuccess )
    {  
        a_CommTmo = g_CommTmo ;
    }
    else
    {
        return ( (int) 14 ) ;
    }
    if ( edv_conf.flowcontrol > 1 )
    {   /* flow control on */;  }
    else
    {   /* flow control off */
        /* Monitoring CTS signal */
        a_CommDcb.fOutxCtsFlow = FALSE ;
        
        /* Monitoring DSR signal */
        a_CommDcb.fOutxDsrFlow = FALSE ;

        /* DTR flow control */   // 221100 : enable->disable

        if ( dhs )              // 130902 : STEUERBAR ...
            a_CommDcb.fDtrControl = DTR_CONTROL_DISABLE;
        else
            a_CommDcb.fDtrControl = DTR_CONTROL_ENABLE;


        /* DSR signal sensitivity */
        a_CommDcb.fDsrSensitivity = FALSE ;

        /* RTS flow control */
        a_CommDcb.fRtsControl = RTS_CONTROL_DISABLE ;
        if ( ! dhs )            // 130902 
             a_CommDcb.fRtsControl = RTS_CONTROL_DISABLE ;
        else
            a_CommDcb.fRtsControl = RTS_CONTROL_ENABLE ;

    }
    a_CommDcb.BaudRate = (DWORD) edv_conf.baudrate ;
    a_CommDcb.ByteSize = (BYTE) edv_conf.databits ;
    switch ( edv_conf.parity ) 
    {
        case NONE: a_CommDcb.Parity = NOPARITY ;
                break ;
        case EVEN: a_CommDcb.Parity = EVENPARITY ;
                break ;
        case ODD: a_CommDcb.Parity = ODDPARITY ; 
                break ;
        default : a_CommDcb.Parity = NOPARITY ; 
     }    
     switch ( edv_conf.stopbits )
     {
        case 1 : a_CommDcb.StopBits = ONESTOPBIT ;
                break ;
        case 2 : a_CommDcb.StopBits = TWOSTOPBITS ;
                break ;
        default : a_CommDcb.StopBits = ONESTOPBIT ;
     }
     fSuccess = SetCommState ( schnittstelle, &a_CommDcb ) ;
     if ( !fSuccess )  { return ( (int) 14 ) ; }
/* RITO : max. Pause zw. 2 Zeichen -> Endekenn.(9600 ca. 1 millsek/byte)  */  
/* RTTM und RTTC kooperieren */
/* Produkt = maximale Transfer-Time, dann wird (evtl.teilweise) geliefert */
/* RTTM : erweitert TO durch : RTTM * Anzahl Zeichen */
/* RTTC :erweitert TO durch : ( RTTM * AZ ) + RTTC   */
/* das sind gerade die Werte fuer 9600 */  /* 230608 : tfaktor */
     a_CommTmo.ReadIntervalTimeout = 30 * tfaktor ;         /* 30 millisec -> Endekenn !!*/
     a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;          /* x 2 ( Zeit je Byte+x */ 
     a_CommTmo.ReadTotalTimeoutConstant = 30000 ; /* 30 sek warten */ /* 30 millisec (RITO) */ 
     switch ( edv_conf.baudrate )
     {
     case 2400 :
     a_CommTmo.ReadIntervalTimeout = 50 * tfaktor ;         /* 50 msec -> Endekenn !!*/
     a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;          /* x 2 ( Zeit je Byte+x */ 
           break ;
     case 4800 :
     a_CommTmo.ReadIntervalTimeout = 40 * tfaktor ;         /* 40 msec -> Endekenn !!*/
     a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;    /* x 2 ( Zeit je Byte+x */ 
           break ;
     }
     a_CommTmo.WriteTotalTimeoutMultiplier = 100 ;       /* x 100 */
     a_CommTmo.WriteTotalTimeoutConstant = 100;          /* 100 millisec */                    

     fSuccess = SetCommTimeouts ( schnittstelle, &a_CommTmo ) ;
     if ( ! fSuccess ) return ( (int) 15 ) ;
#ifdef ITL 
     return ( (int) 0 ) ;
#endif
                    
#ifdef ID7
     return ( holak ( 96000 / edv_conf.baudrate ) ) ;
#endif
}
/* #################################################################### */
/*                      Schreiben des Puffers                           */
/* #################################################################### */

int os_write (HANDLE fd , char *buf , unsigned nNumberOfBytesToWrite) 
{
DWORD   NumberOfBytesWritten ;
BOOL rc ;       /* returncode */
char hilfbuf [120] ;    // 220699 
  rc = WriteFile ( fd , (LPVOID) buf, (DWORD) nNumberOfBytesToWrite,
       (LPDWORD) &NumberOfBytesWritten , NULL ) ; 
  if (rc == FALSE)     
  {     sprintf ( logtext , "Fehler beim WriteFile\n" ) ; }
  if ( testschnitt )    // 220699 
  {

           memset ( hilfbuf , '\0' , 120 ) ;
           if ( NumberOfBytesWritten > 118 )
                memcpy ( hilfbuf , buf , 118 ) ;
           else
               memcpy ( hilfbuf , buf , NumberOfBytesWritten ) ;

               printf ( "WW %3.0d %s \n" , NumberOfBytesWritten , hilfbuf ) ;
               sprintf ( logtext, "WW %3.0d %s \n" , NumberOfBytesWritten , hilfbuf ) ;
               logschreib () ; 
  }

  return ( (int) NumberOfBytesWritten) ;
}

/* #################################################################### */
/*                        Lesen des Puffers                             */
/* #################################################################### */

int os_read (HANDLE fd , char *buf , unsigned nNumberOfBytesToRead)
{
DWORD NumberOfBytesRead ;
BOOL rc ;     /* returncode */
char hilfbuf [120] ;    // 220699 
   rc = ReadFile ( fd , (LPVOID) buf , (DWORD) nNumberOfBytesToRead,
        (LPDWORD) &NumberOfBytesRead , NULL ) ;
   if (rc == FALSE )
   { sprintf ( logtext , "Fehler beim ReadFile \n" ) ; }
   if ( testschnitt )   // 220699 
   {
      if ( rc == FALSE )
      {
        printf ( "RR  -1 Fehler Lese-Zugriff \n" ) ;
        sprintf ( logtext , "RR  -1 Fehler Lese-Zugriff \n" ) ;
        logschreib () ;
      }
      else
      {
        if ( NumberOfBytesRead  == 0L )
        {
             printf ( "RR  00 NIX gelesen \n" ) ;
             sprintf ( logtext ,"RR  00 NIX gelesen \n" ) ;
             logschreib () ;

        }
        else
        {
           memset ( hilfbuf , '\0' , 120 ) ;
           if ( NumberOfBytesRead > 118 )
                memcpy ( hilfbuf , buf , 118 ) ;
           else
                memcpy ( hilfbuf , buf , NumberOfBytesRead ) ;
           printf ( "RR %3.0d %s \n" , NumberOfBytesRead , hilfbuf ) ;
           sprintf ( logtext , "RR %3.0d %s \n" , NumberOfBytesRead , hilfbuf ) ;
           logschreib () ;
        }
      }
   }
   return ( (int) NumberOfBytesRead ) ; 
}   

void logschreib ( void )
{   
char erfnam [20] ;
char writepuffer [200] ;

char timestr[100] ; 
if ( testxx ) printf ( "%s : %s\n" , _strtime(timestr),logtext ) ; 

if ( !testjg ) return ;
if ( testjg == 1 )
 {
 sprintf ( erfnam , "logfile" ) ;
 if ( ( fperr = fopen(erfnam, "at" )) == NULL ) return ;   /* APPEND_TEXT */
 } ;
 testjg = 2 ;
 sprintf ( writepuffer, "%s : %s", _strtime(timestr),logtext ) ; 
 fprintf ( fperr , writepuffer ) ;
}

u_long eigexit ( int para  )
{
 if ( testjg == 2 )
 {
  sprintf ( logtext , "exit mit %d\n" , para ) ;
  logschreib() ;
  fclose (fperr) ;
 }

// unnoetig : fSuccess = SetCommState ( schnittstelle, &g_CommDcb ) ;
// unnoetig : fSuccess = SetCommTimeouts ( schnittstelle, &g_CommTmo ) ;

#ifdef MAINI
  exit ( (u_long) para ) ;
#else
 if ( testjg == 2 )
 {

	testjg = 1 ; 
 }	
  return ( (int) para ) ;
#endif
}

double inpkonv ( char *argl )
{
 int i, k ;
 k = strlen ( argl ) ;
 for ( i = 0 ; i < k ; i ++ )
 { if ( argl[i] == ',' ) argl[i] = '.' ; } 
 
  sprintf ( logtext , "Para3 : %s\n" , argl ) ;
  logschreib() ;
  return ( atof ( argl ) ) ; 
} 

char * itltar ( char * kdostring , double targew )  
{
  char kostri[20] ;
  unsigned char ch ;
  int x1 , x2 , x3 ;
  char form[20] ;
  memset ( kdostring , '\0' , 20 ) ;
  memset ( kostri, '\0' , 20 ) ;
// so war das einmal : sprintf ( kostri , "%5.2lf" , targew ) ;
  sprintf(form   , "%%%d.%dlf" , ( 7 - kommastellen) , kommastellen ) ;
  sprintf(kostri , form        , targew ) ;
  ch = 0x71 ;     /* 'q' */
  kdostring[0] = ch ;
  ch = 0x53 ;     /* "S" */
  kdostring[1] = ch ;
  ch = 0x20 ; ;
  kdostring[2] = ch ;
  kdostring[3] = ch ;
  kdostring[4] = ch ;
  kdostring[5] = ch ;
  ch = 0x30 ;
  kdostring[6] = ch ;
  ch = ',' ;
  kdostring[7] = ch ;
  ch = 0x30 ;
  kdostring[8] = ch ;
  kdostring[9] = ch ;
  ch = 'k' ;
  kdostring[10]= ch ;
  ch = 'g' ;
  kdostring[11]= ch ;
  ch = CRi /* 0x0d */ ;
  kdostring[12] = ch ; 
  ch = LFi /* 0x0a */ ;
  kdostring[13] = ch ;
  x3 = strlen (kostri) ; 
  for ( x1 = 0 ; x1 < x3 ; x1++ )
  { if ( kostri[x1] == ',' || kostri[x1] == '.' )
    { 
      x2 = x1 ; 
      x1++; 
      if ( kostri[x1] > 0x30 && kostri[x1] < 0x3a )
      { ch = kostri[x1] ; kdostring[8] = ch ; }
      x1++; 
      if ( kostri[x1] > 0x30 && kostri[x1] < 0x3a )
      { ch = kostri[x1] ; kdostring[9] = ch ; 
      }
      for ( x1 = 0 ; x1 < x2 ; x1++ )
      {
        if ( kostri[x1] > 0x30 ) 
        {  ch = kostri[x1] ;   
          kdostring [ 7 - x2 + x1 ] = ch ; 
          while ( 1 )  
          { x1++ ;
            if ( x1 >= x2 ) break ;
            ch = kostri[x1] ;
            kdostring [ 7 - x2 + x1 ] = ch ;
          }             /* end while */
          break ;
        }               /* end if erste stelle gefunden */
      }               /* end for suche erste Stelle */
      break ;
    }                   /* end Komma gefunden */
  }                    /* end Suche nach dem komma */
  sprintf ( logtext ,"%s" , kdostring ) ;
  logschreib () ;
  return ( kdostring ) ;
}
/** id7tar macht das Tarieren mit Wert **/

int id7tar ( double targew )  
{
  char kdostring[20] ;
  char kostri[20] ;
  unsigned char ch ;
  int x1 , x2 , x3 , retanz ;
  char form[20] ;
  memset ( kdostring , '\0' , 20 ) ;
  sprintf ( form , "%%-01.%dlf" , kommastellen ) ;
  sprintf ( kostri , form , targew ) ;

  x2 = 0 ;
  ch = 0x54 ;     /* 'T' */
  kdostring[x2++] = ch ;
  ch = 0x20 ;     /* " " */
  kdostring[x2++] = ch ;

  x1 = strlen ( kostri ) ;
  x3 = 0 ;
  for ( x2 = 2 ; x3 < x1 ; )
  { ch = kostri[x3++] ;
    kdostring[x2++] = ch ;
  }

  ch = 0x20 ;
  kdostring[x2++] = ch ;
  ch = 'k' ;
  kdostring[x2++] = ch ;
  ch = 'g' ;
  kdostring[x2++] = ch ;
  ch = 0x20 ;
  kdostring[x2++] = ch ;
  ch = CRi /* 0x0d */ ;
  kdostring[x2++] = ch ; 
  ch = LFi /* 0x0a */ ;
  kdostring[x2++] = ch ;
  sprintf ( logtext ,"Commandostring zum Tarieren : %s" , kdostring ) ;
  logschreib () ;
  retanz = os_write (schnittstelle, kdostring , x2 ) ; 
  sprintf ( logtext , "x2 , retanz Init : %d %d \n" , x2,retanz ) ;
  logschreib () ;
  if ( retanz != x2 ) return ( -1 ) ;
  return ( quittung ( 23) ) ;
}

/** "itlgew" funktioniert erst mal ohne log. Quittungen, 
bei Bedarf waere erweitertes Handling noetig **/
/* wichtig ist der Block-Trenner "0x03" = ETX */
/* Probewiegen waehrend des INITS waere denkbar fuer komplette Dialoge */ 
/* Fixes Format : B/N/T , 8 VK , "." , 3 NK , 0x0d, 0x0a => 15 zeichen */

int itlgew (int strleng ) 
{
 int i , j , k , l ;    /* i = Quellpointer */
                        /* j = Status 0 = Suche nach Blockanfang
                                      1 = Suche nach Wertanfang  
                                      2 = Transfer  aktiv 
                                      3 = Suche nach Trenner
                         */
 double gewicht ;
 unsigned char art ;
 unsigned char zch ;
 char ausgabepuffer[46] ;
 char zwipu [16] ;

 FILE *fpergeb ;
 char ergebnam [128] ;
  strcpy ( ergebnam, dialogdir  ) ;
  strcat ( ergebnam , "\\ergebnis" ) ;
  remove ( ergebnam ) ;  
  if ( ( fpergeb = fopen(ergebnam, "at" )) == NULL ) return ( -1 ) ; 
  memset ( ausgabepuffer , '\0' , 46 ) ;
  i=j=k=l=0 ;
  for ( i = 0 ; i < strleng ; i ++ )
  { zch = wertbuffer[i] ;
    zch &= 0x7f ;              /* Pari-Strip */
    if ( j == 0 )        /* Identifikation */
    {
      switch (zch)
      {
         case 0x2b :  art = 'B' ; i++ ; j = 1 ;  /* Brutto "+" */ break ;
         case 0x2c :  art = 'N' ; i++ ; j = 1 ;  /* Netto  "," */ break ;
         case 0x2d :  art = 'N' ; i++ ; j = 1 ;  /* Netto  "-" */ break ;
         case 0x2e :  art = 'T' ; i++ ; j = 1 ;  /* Tara   "." */ break ;
         case 0x2f :  art = 'T' ; i++ ; j = 1 ;  /* Tara   "/" */ break ;
         case  ETX :  art = ' ' ;       j = 0 ;  /* ETX erkannt */break ; /* ACHTUNG */

         case 0x5a :  if ( dev_type == GE_ITE )
                        {
                           art = 'E' ;
                           i++ ; j = 1 ;  /* Eich-Speich-ite */
                           break ;
                        }  /* hier geht es dann weiter mit default-zweig */

         default   :  art = ' ' ; i++ ; j = 3 ;  /* Blindblock */ break ;
       } continue ;
    }
    if ( j == 1 )        /* Wertanfang suchen suchen */
    {
        if (( zch > 0x30 && zch < 0x3a ) || zch == ',' || zch == '.' )
        { 
          memset ( zwipu , '\0' , 16 ) ; k= 0 ; j = 2 ;
          if ( zch == ',' ) zch = '.' ;
          zwipu[k] = zch ;  k++ ;
        } continue ;
    }
    if ( j == 2 ) 
    {
      if (( zch > 0x2f && zch < 0x3a ) || zch == ',' ||zch == '.' )
      { 
        if ( zch == ',' ) zch = '.' ;
        zwipu[k] = zch ; k++ ;
      }
      else    /* Das war das ende */
      {
        i++ ; i++ ; j = 0 ;
        gewicht = atof ( zwipu ) ;
        sprintf ( zwipu , "%c%12.3lf\n" , art , gewicht ) ;
        switch ( art )
        {
        case 'B':
                wertb = gewicht ; break ;
        case 'N':
                wertn = gewicht ; break ;
        case 'T':
                wertt = gewicht ; break ;
        case 'E':
                werte = gewicht ; i-- ; i-- ; break ;
                /*  hier folgt keine Einheit mehr  */

        }
        fprintf ( fpergeb , zwipu ) ; 
        l = l++ ;
      }
      continue ;
    }
    if ( j == 3 )
    { if ( zch == 0x03 || zch == 0x0a ) j = 0 ;  
      continue ;
    }
  }
  fclose ( fpergeb ) ;
  if ( l )  
  { 
     sprintf ( logtext , "brutto %lf ,netto %lf , tara %lf, eich %lf \n" , wertb,wertn,wertt,werte ) ;
     logschreib () ;
     return ( 0 ) ;
  } 
  return ( -1 ) ;
}

/* Fixes Format : B/N/T , 8 VK , "." , 3 NK , 0x0d, 0x0a => 15 zeichen */


// ########################################################################
// ########################################################################

int id7gew ( int modus , char * wertbuffer , int strleng ) 
{
 int i , j , k , l ;    /* i = Quellpointer */
                     /* eigentlich ab pos 4 in 21-er Schritten suchbar */
 double gewicht ;
 unsigned char art ;
 unsigned char zch ;
 char blonu[4] ;
 char vergl[4] ;
 char ausgabepuffer[46] ;
 char zwipu [16] ;


 FILE *fpergeb ;
 char ergebnam [128] ;

   strcpy ( ergebnam, dialogdir  ) ;
   strcat ( ergebnam , "\\ergebnis" ) ;
   remove ( ergebnam ) ;  
   if ( ( fpergeb = fopen(ergebnam, "at" )) == NULL ) return ( -1 ) ; 
   sprintf ( logtext , "Datei geoffnet %s\n" , ergebnam ) ;
   logschreib() ;
   memset ( ausgabepuffer , '\0' , 46 ) ;
   j=k=l=0 ;
#ifdef ID72
// das id2 hat wohl in alten Programmstaenden ein etwas anderes Format
// laut DOKU sollte es aber identisch sein ....
if ( modus == 11 )
 {
   sprintf ( logtext , "Im id72 -Modus" ) ;
   logschreib() ;
   i = 3 ;
   while ( i < strleng )
   {  zch = wertbuffer[i] ;
      zch &= 0x7f ;            /* Pari-Strip */
      i++ ;
      sprintf ( logtext , "Zeichen = %c , %x ", zch , zch ) ;
      logschreib() ;

      if ( zch == 0x42 || zch == 0x47 )        /* "B" = Brutto, "G" = Grosso */
      {  art = 'B' ; }
      else
      {
        if ( zch == 0x4E )
        { art = 'N' ; }
        else
        {
           if ( zch == 0x54 )
           { art = 'T'; }
           else
           {  continue ;
           } ;
        } ;
      } ;
      sprintf ( logtext , "Art = %c ", art   ) ;
      logschreib() ;

      memset ( zwipu , '\0' , 16 ) ;
      for ( k = 0 ; k < 12; )
      {   if ( i == strleng ) break ; 
           zch = wertbuffer[i++] ;
           if ( zch == ',' ) zch = '.' ;
           zwipu[k++] = zch ;
      }
      gewicht = atof ( zwipu ) ;
      sprintf ( logtext , "das gefunden %lf %s\n" , gewicht , zwipu ) ;
      logschreib() ;
      sprintf ( zwipu , "%c%12.3lf\n" , art , gewicht ) ;
      switch ( art )
        {
        case 'B':
                wertb = gewicht ; break ;
        case 'N':
                wertn = gewicht ; break ;
        case 'T':
                wertt = gewicht ; break ;
      }
      fprintf ( fpergeb , zwipu ) ; 
      l = l++ ;
   }    /* Schleife in strleng */
   fclose ( fpergeb ) ;
   if ( l )  
   { 

    /* acksend () ; -> bereits vorher passiert */
    return ( 0 ) ;
   } 
   return ( -1 ) ;
 }              /* modus == 11 fuer ID2/5 */

#endif
if ( modus == 11 )
 {
   i = 4 ;
   while ( i < strleng )
   {  zch = wertbuffer[i] ;
      zch &= 0x7f ;                  /* Pari-Strip */
      i++ ;

      if ( zch != 0x41 )
      {
         continue ;
      }
      /* Notbremse */  if ( i > ( strleng - 8 ) ) break ;       // 160108 16->8
      memset ( blonu ,'\0' , 4 ) ;
      blonu[0] = wertbuffer[i++] ; 
      blonu[1] = wertbuffer[i++] ; 
      blonu[2] = wertbuffer[i++] ;
      i++ ;
      art = 'X' ;
#ifdef MAINI
      sprintf ( logtext , "was gefunden %d %s\n" , i , blonu ) ;
      logschreib() ;
#endif
      sprintf ( vergl , "011" ) ;
      if ( (strcmp ( blonu , vergl )) == 0 ) art = 'B' ;        /* Brutto */
      else
      {
         sprintf ( vergl , "012" ) ;
         if ( (strcmp ( blonu , vergl ) ) == 0 ) art = 'N' ;    /* Netto */
         else
         {
            sprintf ( vergl , "013" ) ;
            if ( (strcmp ( blonu , vergl)) == 0 ) art = 'T' ;   /* Tara  */
// #190700 A
            else
            {
               sprintf ( vergl , "019" ) ;
               if ( (strcmp ( blonu , vergl)) == 0 ) art = 'E' ;   /* EichSpeich  */
// #160902 A
               else
               {
                  sprintf ( vergl , "010" ) ;  // das muesste erst noch konfig. werden
                                               // Auswertung gibt es auch noch nicht 
                  if ( (strcmp ( blonu , vergl)) == 0 ) art = 'W' ;   /* Waagen-Nummer */
                  else
                  {  // seit data2000 : Alibi-Nummer "098"
                     sprintf ( vergl , "098" ) ;
                     if ( (strcmp ( blonu , vergl)) == 0 ) art = 'e' ;   /* EichSpeich  */
// #160902 A
                  }
               }
// #160902 E

            }
// #190700 E
         }
      }
      if ( art == 'X' ) continue ;

      memset ( zwipu , '\0' , 16 ) ;

      if ( art == 'E' )
      {
          if ( strleng < 103 )
          {    // NOTBREMSE, falls Telegramm defekt oder falsche konfig
              sprintf ( logtext , "Unerwartetes Telegrammformat mit A019 \n" ) ;
              logschreib() ;

                art = 'X';
                continue  ;
          }
          i += 6 ;
          k = 0 ;
          // dd
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          i ++ ;
          // mm
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          i ++ ;
          // yy
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          i += 8 ;
          // hh
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          i ++ ;
          // mm
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          i ++ ;
          // ss
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          zch = wertbuffer[i++] ;
          zwipu[k++] = zch ;
          
      }
      else
      {
// #190700 E
// 190902 A
        if ( art == 'e' )
        {       // 7 Stellen ohne Fuehrungsnullen -> getestet am 160108 
          art = 'E' ;
          for ( k = 0 ; k < 7 ; )
          {
            zch = wertbuffer[i++] ;
            zwipu[k++] = zch ;
          }

        }
        else
        {
// 190902 E

          for ( k = 0 ; k < 10 ; )
          {
            zch = wertbuffer[i++] ;
            if ( zch == ',' ) zch = '.' ;
            zwipu[k++] = zch ;
          }     // ende for-schleife
        }       // 190902 ende alle Gewichte oder 'e'
      }         // 190700 : if 'E' oder anderes 

      gewicht = atof ( zwipu ) ;
      sprintf ( logtext , "das gefunden %lf %s\n" , gewicht , zwipu ) ;
      logschreib() ;
      if ( art == 'E')                  // 060900
          sprintf ( zwipu , "%c%012.0lf\n" , art , gewicht ) ;
      else
          sprintf ( zwipu , "%c%12.3lf\n" , art , gewicht ) ;
      switch ( art )
      {
         case 'B':
             wertb = gewicht ; break ;
        case 'N':
             wertn = gewicht ; break ;
        case 'T':
             wertt = gewicht ; break ;
        case 'E':
             werte = gewicht ; break ;
      }
      fprintf ( fpergeb , zwipu ) ; 
      l = l++ ;
   }    /* Schleife in strleng */
   fclose ( fpergeb ) ;
   if ( l )  
   { 
      sprintf ( logtext , "alles zusammen %lf %lf %lf %lf\n" , wertb, wertn, wertt, werte ) ;
      logschreib() ;

    /* acksend () ; -> bereits vorher passiert */
    return ( 0 ) ;
   } 
   return ( -1 ) ;
 }              /* modus == 11 */


if ( modus == 8 )
 {
   i = 3 ;

   zch = wertbuffer[i] ;
   zch &= 0x7f ;                  /* Pari-Strip */
   i++ ;
   if ( i > ( strleng - 10 ) )    /* Notbremse */
   {
      fclose ( fpergeb ) ;
      return  -1 ;
   }
   art = 'N' ;       /* das ist immer irgendwie ein Netto-Gewicht */
#ifdef MAINI
   sprintf ( logtext , "was gefunden %s\n" , wertbuffer ) ;
   logschreib() ;
#endif
   memset ( zwipu , '\0' , 16 ) ;
   for ( k = 0 ; k < 10 ; )
   {
      zch = wertbuffer[i++] ;
      if ( zch == ',' ) zch = '.' ;
      zwipu[k++] = zch ;
   }
   gewicht = atof ( zwipu ) ;
   sprintf ( logtext , "das gefunden %lf %s\n" , gewicht , zwipu ) ;
   logschreib() ;
   sprintf ( zwipu , "%c%12.3lf\n" , art , gewicht ) ;
   switch ( art )
   {
       case 'B': wertb = gewicht ; break ;
       case 'N': wertn = gewicht ; break ;
       case 'T': wertt = gewicht ; break ;
   }
   fprintf ( fpergeb , zwipu ) ; 
   fclose ( fpergeb ) ;
   return ( 0 ) ;
 }             /* modus = 8 */
  fclose ( fpergeb ) ; 
  return ( -1 ) ;
}

/* nap und sleep gibbet wohl nicht unter win */
long nap ( long n ) 
{ 
  Sleep ((DWORD) n );
  return ( (long) 0);
}

void sleep (unsigned secund ) 
{ 
 long ret=0L ;
 ret= nap( 1000*secund) ;
 return ;
}

/* Reihenfolge : erst mal leerlesen , dann "SYN" senden,
 dann auf "SYN"/"ACK" warten,
 zum Schluss noch ein ACK senden */

int holak (int wartemillis )
{
int retanz ;
char kdostring[41] ;
unsigned ch ;
int schleife ;
/*  Endekennung 2400 ca. 4 millsek/byte)-> hier soll nur auf 1 Zeichen
        reagiert werden */
    a_CommTmo.ReadIntervalTimeout = 30;         /* 30 millisec -> Endekenn !!*/ 
    a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;          /* x 2 ( Zeit je Byte+x */ 
    a_CommTmo.ReadTotalTimeoutConstant = wartemillis ;
/* 100199 : frueher fix auf  500 ;  0,5 sek warten (RITO) */
    a_CommTmo.WriteTotalTimeoutMultiplier = 100 ;       /* x 100 */ 
    a_CommTmo.WriteTotalTimeoutConstant = 100;          /* 100 millisec */                    
    fSuccess = SetCommTimeouts ( schnittstelle, &a_CommTmo ) ;
    if ( ! fSuccess ) return ( (int) 18 ) ;

    memset ( wertbuffer , '\0' , 120 ) ;
    retanz = os_read ( schnittstelle, wertbuffer , 80 ) ;
    sprintf ( logtext , "holak : WEPU : %s\n", wertbuffer ) ;
    logschreib () ;

     memset ( kdostring , '\0' , 41 ) ;
     ch = SYNi /* 0x16 */ ;
     kdostring[0] = ch ;

     retanz = os_write (schnittstelle, kdostring , 1 ) ; 
     sprintf ( logtext , "retanz Init : %d \n" , retanz ) ;
     logschreib () ;
     if ( retanz != 1 )
     return ( -1 ) ;
     for ( schleife = 10 ; schleife > 0 ; schleife -- )
     { 
          sprintf ( logtext , "schleife : %d \n" , schleife ) ;
          logschreib () ;
        memset ( wertbuffer , '\0' , 120 ) ;    /* 220699 30 -> 80 */
        retanz = os_read ( schnittstelle, wertbuffer , 80 ) ;

// 290699 A
        if ( sics )
        {
            if ( retanz == 0 )
            {
               retanz = 1 ;
               wertbuffer[0] = ACKi /* 0x06 */ ;
            }
        }
// 290699 E

        if ( retanz > 0 )
        { retanz-- ;
          sprintf ( logtext , "INI-Schnitt-wertbuffer : %s \n" , wertbuffer ) ;
          logschreib () ;
          ch = wertbuffer[retanz] ;
          if ( ch == ACKi /* 0x06 */ )
            {    
// 220699 : Leerlesen(vor allem sinnlose Tasten), holak ist nur mit ID7 aktiv
// #ifndef ID72
     retanz = 1 ;
     while ( retanz )
     {
       memset ( kdostring , '\0' , 41 ) ;
       ch = ACKi /* 0x06 */ ; 
       kdostring[0] = ch ;
       retanz = os_write (schnittstelle, kdostring , 1 ) ; 
       sprintf ( logtext , "retanz Init2 : %d \n" , retanz ) ;
       retanz = os_read ( schnittstelle, wertbuffer , 80 ) ;
     }
// #endif
// 220699 : Ende Leerlesen

               /* Das sind die Werte fuer 9600 */  /* 230608 */
               a_CommTmo.ReadIntervalTimeout = 30 * tfaktor ;         /* 30 millisec -> Endekenn !!*/ 
               a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;          /* x 2 ( Zeit je Byte+x */ 
               a_CommTmo.ReadTotalTimeoutConstant = 30000 ; /* 30 sek warten */ /* 30 millisec (RITO) */ 
               a_CommTmo.WriteTotalTimeoutMultiplier = 100 ;       /* x 100 */
               a_CommTmo.WriteTotalTimeoutConstant = 100;          /* 100 millisec */                    
               switch ( edv_conf.baudrate )
               {
                  case 2400 :
                     a_CommTmo.ReadIntervalTimeout = 50 * tfaktor ;         /* 50 millisec -> Endekenn !!*/
                     a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;          /* x 2 ( Zeit je Byte+x */ 
                           break ;
                 case 4800 :
                     a_CommTmo.ReadIntervalTimeout = 40 * tfaktor ;         /* 40 millisec -> Endekenn !!*/
                     a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;          /* x 2 ( Zeit je Byte+x */ 
                           break ;
               }
               fSuccess = SetCommTimeouts ( schnittstelle, &a_CommTmo ) ;
               if ( ! fSuccess ) return ( (int) 19 ) ;
               return ( 0 ) ; 
            }
        }
    }
return ( -33 ) ;
}

/* Vorerst fuer id7 */
int quittung ( int imodus )
{
int dretanz ;
unsigned ch ;
int pointi ;

#ifndef MAINI
M145:
#endif

   memset(wertbuffer, '\0', 120 ) ;

// 200699 : Start imodus == 6 ( schreiben Appl-block ,Antwort ist "AB"
   if ( imodus == 6 )
   {  /* Schreiben Appl-Block */
      dretanz = os_read ( schnittstelle, wertbuffer , 80 ) ;
      pointi = 0 ;
      if ( dretanz > 0 )
      {
         sprintf ( logtext , "wertbuffer(1) : %s \n" , wertbuffer ) ;
         logschreib () ;
         ch = wertbuffer [ dretanz - 1 ] ;
         if ( ch == ACKi /* 0x06 */ )      /* ACK am Schluss */
         {
            memset ( wertbuffer , '\0' , 40 ) ;
            dretanz = os_read ( schnittstelle, wertbuffer , 80 ) ;
          // eigentlich muesste man noch die Antwort auswerten
            if ( ! dretanz ) return ( -1 ) ;
                acksend () ;
         }
         else
         {
             acksend () ;
             pointi = 0 ;
             while ( pointi < dretanz )
             {
               ch = wertbuffer[pointi] ;
               if ( ch == ACKi /* 0x06 */ )
               {
                 pointi ++ ;
               }
               else
               {
                 break ;
               }
             }
         }
         
         sprintf ( logtext , "wertbuffer(2) : %s \n" , wertbuffer ) ;
         logschreib () ;
         ch = wertbuffer [ pointi + 0 ] ;

         if ( ch == 0x41 )  /* "A" */
         {  ch = wertbuffer[pointi + 1] ;
            if ( ch == 0x42 ) /* "B" */
                  return ( 0 ) ;
         }
      }
   }            /* imodus 6 */
// 200699 : Ende imodus == 6 ( schreiben Appl-block ,Antwort ist "AB"

   if ( imodus == 4 || imodus == 5 || imodus == 23 )
   {  /* Tarieren oder Tarieren mit Wert */
      dretanz = os_read ( schnittstelle, wertbuffer , 80 ) ;
      pointi = 0 ;
      if ( dretanz > 0 )
      {
         sprintf ( logtext , "wertbuffer(1) : %s \n" , wertbuffer ) ;
         logschreib () ;
         ch = wertbuffer [dretanz - 1 ] ;
         if ( ch == ACKi /* 0x06 */ )      /* ACK am Schluss */
         {
#ifdef MAINI
            acksend () ;  // 220699 
            memset ( wertbuffer , '\0' , 40 ) ;
            dretanz = os_read ( schnittstelle, wertbuffer , 80 ) ;
#else
// manchmal steht auch ein ACK am Ende der Antwort 
            acksend () ;
            if ( dretanz < 4 )
            {
                memset ( wertbuffer , '\0' , 40 ) ;
                dretanz = os_read ( schnittstelle, wertbuffer , 80 ) ;
            }
#endif

            if ( ! dretanz ) return ( -1 ) ;
         }
         else
         {
//#ifdef MAINI        220699 : dise Alternative mal ausgebaut
//             ch = wertbuffer[0] ;
//             if ( ch != ACK /* 0x06 */ )  return ( -1 ) ;
//             pointi = 1 ;
//#else
             acksend () ;
             ch = wertbuffer[0] ;
             if ( ch == ACKi /* 0x06 */ ) pointi = 1 ; else pointi = 0 ;
//#endif

         }
         
         sprintf ( logtext , "wertbuffer(2) : %s \n" , wertbuffer ) ;
         logschreib () ;
         ch = wertbuffer [ pointi + 0 ] ;

#ifndef MAINI 
         if  ( ch == 0x4b )  goto M145 ;  /* Leerlesen zus. Tastatureingaben */
#endif
         if ( ch == 0x54 )  /* "T" */
         {  ch = wertbuffer[pointi + 1] ;
            if ( ch == 0x42 ) /* "B" */
                  return ( 0 ) ;
         }
// 130902 : neu dazu : manche Waegebruecken antworten bei Tarieren zu 0 mit "ZA"
         if ( ch == 0x5a )  /* "Z" */
         {  ch = wertbuffer[pointi + 1] ;
            if ( ch == 0x41 ) /* "A" */
                  return ( 0 ) ;
         }



      }
   }            /* imodus 4,5,23 */



   /* vorher wurde "SX" gesendet */
   if ( imodus == 11 )
   {
// 190700 100 -> 119
      dretanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
      pointi = 0 ;
      if ( dretanz > 0 )
      {
         sprintf ( logtext , "wertbuffer(1) : %s \n" , wertbuffer ) ;
         logschreib () ;
         ch = wertbuffer [dretanz - 1 ] ;
         if ( ch == ACKi /* 0x06 */)
         {
//#ifdef MAINI       diese Alternative mal ausgebaut 220699   
//            memset ( wertbuffer , '\0' , 100 ) ;
//            dretanz = os_read ( schnittstelle, wertbuffer , 100 ) ;
//#else
            acksend () ;
            if ( dretanz < 4 )
            {
               // #190700 100 -> 120
                memset ( wertbuffer , '\0' , 120 ) ;
                dretanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
            }
//#endif
            if ( ! dretanz ) return ( -1 ) ;

         }
         else
         {
//#ifdef MAINI          // 220699 : zusammengefasst
//             ch = wertbuffer[0] ;
//#ifndef ID72 
//             if ( ch != ACKi /* 0x06 */ )  return ( -1 ) ;
//#endif
//
//             pointi = 1 ;
//#else
             acksend () ;
             ch = wertbuffer[0] ;
             if ( ch == ACKi /* 0x06 */ ) pointi = 1 ; else pointi = 0 ;
//#endif
         }       
         sprintf ( logtext , "wertbuffer(2) : %s \n" , wertbuffer ) ;
         logschreib () ;
         ch = wertbuffer[pointi + 0] ;
#ifndef MAINI 
         if  ( ch == 0x4b )  goto M145 ;  /* Leerlesen zus. Tastatureingaben */
#endif
         if ( ch == 0x53 )      /* 'S' */
         { ch = wertbuffer[pointi + 1] ;
            if ( ch == 0x58 )    /* 'X' */
            { ch = wertbuffer[pointi + 2] ;
               if ( ch == 0x49 )  /* 'I' */
               {
                   return ( -1 ) ;
               }
               return ( id7gew ( 11 , wertbuffer, dretanz)) ;
            }
         }
      }
   }             /* imodus 11 */
/* #################################### */
   /* vorher wurde "S" gesendet */
   if ( imodus == 8 )
   {  dretanz = os_read ( schnittstelle, wertbuffer , 100 ) ;
      pointi = 0 ;
      if ( dretanz > 0 )
      {
         sprintf ( logtext , "wertbuffer(1) : %s \n" , wertbuffer ) ;
         logschreib () ;
         ch = wertbuffer [dretanz - 1 ] ;
         if ( ch == ACKi /* 0x06 */ )
         {
// #ifdef MAINI       220699 : Alternative ausgebaut 
//            memset ( wertbuffer , '\0' , 100 ) ;
// 230108 : wech damit   dretanz = os_read ( schnittstelle, wertbuffer , 100 ) ;
// #else
            acksend () ;
            if ( dretanz < 4 )
            {
                memset ( wertbuffer , '\0' , 100 ) ;
                dretanz = os_read ( schnittstelle, wertbuffer , 100 ) ;
            }
// #endif
            if ( ! dretanz ) return ( -1 ) ;

         }
         else
         {
// #ifdef MAINI       220699 : Alternative ausgebaut
//             ch = wertbuffer[0] ;
//             if ( ch != ACKi /* 0x06 */ )  return ( -1 ) ;
//             pointi = 1 ;
// #else
#ifndef ID72
             acksend () ;
#endif
             ch = wertbuffer[0] ;
             if ( ch == ACKi /* 0x06 */ ) pointi = 1 ; else pointi = 0 ;
// #endif
         }       
         sprintf ( logtext , "wertbuffer(2) : %s \n" , wertbuffer ) ;
         logschreib () ;
         ch = wertbuffer[pointi + 0] ;
#ifndef MAINI 
         if  ( ch == 0x4b )  goto M145 ;  /* Leerlesen zus. Tastatureingaben */
#endif 
         if ( ch == 0x53 )      /* 'S' */
         {
            ch = wertbuffer[pointi + 1] ;
            if ( ch == 0x49 )  /* 'I' ungueltig */
            {
               return ( -1 ) ;
            }
            if ( ch == 0x44 || ch == 0x20 )    /* 'D' oder ' ' */
            {
               return ( id7gew ( 8 , wertbuffer, dretanz)) ;
            }
         }
      }
   }             /* imodus 11 */
/* #################################### */
   return ( -1 ) ;
}

/* Es werden je nach Dialogart unterschiedliche Antworten gesendet bzw.
           erwartet, ausserdem wird eine fkt - taste erwartet */

int id7dialog ( int imodus , char * kdostring, int zeig ) 
{
int retanz ;
unsigned ch ;
int pointi, pointo, schleif ;

   schleif = retanz = 0 ;
   while ( !retanz )
   {
      /* Achtung -> diese Schleife ist unendlich !! */
      memset ( wertbuffer , '\0' , 120 ) ;
      
      retanz = os_read ( schnittstelle, wertbuffer , 80 ) ;

      if ( retanz > 0 )
      {
         sprintf ( logtext , "wertbuffer(0) : %s \n" , wertbuffer ) ;
         logschreib () ;
// 290699 A 
         if ( imodus == 33 )
         {
                acksend () ;
                retanz = 0 ;
                continue ;
          }
// 290699 E
         ch = wertbuffer [retanz - 1 ] ;     /* vorhergehendes ueberlesen */
         if ( ch == ACKi /* 0x06 */ )
         {
            acksend () ;
            retanz = 0 ;
            continue ;
         }
         if ( ch == SYNi /* 0x16 */ )
         {
             wertbuffer[0] = SYNi /* 0x16 */ ;
             retanz = os_write (schnittstelle, wertbuffer , 1 ) ;
             retanz = 0 ;
             continue ;
         }
         pointi = 0 ;
         if ( imodus == 199 )
         {
            sprintf ( logtext , "Wertpuffer (199) %s \n" , wertbuffer ) ;
            logschreib () ;
            acksend () ;
            return ( 0 ) ;
         }
         while ( pointi < retanz )
         {     /* als Antwort muss jetzt "DB" = Display geschrieben kommen */

            ch = wertbuffer[pointi++] ;
            if ( ch == 0x44 )           /* Suche nach Start "D" */
            {
                /* Bei imodus = 200 wird keine Tasten-Quittung abgewartet */
               ch = wertbuffer[pointi++];
               if ( ch == 0x42 )
               {
                  sprintf ( logtext , "DB empfangen %s \n" , wertbuffer ) ;
                  logschreib () ;
                  if ( imodus == 200 ) { acksend () ; return ( 0 ) ; }
                  acksend() ;
                  retanz = 0 ; continue ;

               }
               
            }
            if ( ch == 0x4b ) /* Textheader-Antwort ("K") */ break ;
         }              /* while - Leerlesen */
         if ( ( pointi >= retanz ) && ( retanz > 0 ) )
         {
            /* unerwuenschtes Zeugs empfangen : noch mal senden */
            holak (500) ;
            retanz = os_write (schnittstelle, kdostring , zeig ) ;
            sprintf ( logtext , "retanz Init : %d , KDO : %s \n" , retanz, kdostring ) ;
            logschreib () ;
            retanz = 0 ;
         }
      }         /* if retanz > 0 */
   }            /* while 1 */

   pointo = pointi = 0 ;
   sprintf ( logtext , "wertbuffer(1) : %s , retanz %d \n" , wertbuffer,retanz  ) ;
   logschreib () ;
   while ( pointi < ( retanz - 2 ) )
   {
      ch = wertbuffer [pointi++ ] ;
      if ( ch == 0x4b ) /* "K" : Text-Header erkannt */
      {
         ch = wertbuffer [pointi++ ] ;
         if ( ch == 0x46 )      /* "F" : Tastencode */
         {
            pointi ++ ;
            lakeyi[0] = wertbuffer[pointi++] ;
            while ( pointi < retanz )
            {
               ch = wertbuffer[pointi++] ;
               if ( ch == CRi /* 0x0d */ || ch == LFi /* 0x0a */ ) break ;
               if ( ch == '\0' ) return ( 0 ) ;
            }
            sprintf ( logtext , "Taste : %s ; dialdatin : %s \n" , lakeyi , dialdatin ) ;
            logschreib () ;
            continue ;
         }
         if ( ch == 0x20 )      /* Antwortstring */
         {  pointo = 0 ;
            pointi ++ ;
            while ( (pointi < ( retanz - 2 ) ) && ( pointo < 20 )  )
            {
               ch = wertbuffer[pointi++] ;
               if ( ch == CRi /* 0x0d */ || ch == LFi /* 0x0a */ ) break ;
               if ( ch == '\0' )
               {
                  return ( 0 ) ;
               }
               dialdatin[pointo++] = ch ;
            }
            sprintf ( logtext , "Taste : %s ; dialdatin : %s \n" , lakeyi , dialdatin ) ;
            logschreib () ;
            continue ;
         }
      }         /* "K" erkannt */
   }            /* while kleiner ... */
 
   if (  ( strlen(dialdatin) > 0 ) && ( lakeyi[0] == 0x20 ) ) lakeyi[0] = '^' ;
   sprintf ( logtext , "Taste : %s ; dialdatin : %s \n" , lakeyi , dialdatin ) ;
   logschreib () ;
   acksend () ;
   return ( 0 ) ;
}

void acksend (void)
{
char acck [2] ;
int retanz ;
// return ;
sprintf ( logtext , "acksend \n" ) ;
logschreib () ;
acck[0] = ACKi /* 0x06 */ ;
acck[1] = '\0' ;
retanz = os_write (schnittstelle, acck , 1 ) ; 
}
