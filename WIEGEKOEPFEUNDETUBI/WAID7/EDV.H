#ifdef  IDENT
static char ident[] = "@(#) edv.h  VLUGLOTSA   27.04.98 by SE.TEC"
#endif      
#ifndef _EDVH_
#define _EDVH_
/***************************************************************************/
/* Programmname :  EDV.H                                                   */
/*-------------------------------------------------------------------------*/
/* Funktion :  Includes on HDLC Level                                      */
/*-------------------------------------------------------------------------*/
/* Revision : 1.00     Datum : 27.04.98   erstellt von : SE.TEC            */
/*-------------------------------------------------------------------------*/
/* Aenderungsstand :                                                       */
/*-------------------------------------------------------------------------*/


/*-*******************************************************************/
/* Daten-Typen */
/*********************************************************************/

typedef struct { char    ddId[64];    /* device name or host name */
                 char    IPAddr[64];  /* IP address rev. */
                 u_long  PortAddr;         /* Port address rev.*/
               } TYedvChan;

/* Zur Beschreibung der Schnittstellen-spezifischen Parameter (varParBlk): rev. 4.30 */
typedef struct {                        /* Parameterblock fuer Telegrammart */
                short   kennung;        /* Telegrammart */
                u_short  size;          /* Telegrammgroesse */
                u_short  paketSize;     /* Paketiergroesse */
               } TYedvCtrl;

typedef struct {
                u_char dstAddr;
                u_char srcAddr;
               } TYedvAddr;             /* HDLC-Adressenpaar */

typedef struct {
                u_long baudrate;       /* 1200, 4800, 9600, 19200 rev. 5.30           */
                u_long pfTimer;        /* Zeitkonstante fuer P/F-Timer                */
                u_long frameTimer;     /*       "       fuer Frame-Timer              */
                u_long synTime;        /* Synczeichen-Frequenz                        */
                u_long idleDisc;       /* Zeit bis zum automatischen Verbindungsabbau */
                u_char parity;         /* Paritaet rev. 3.30                          */
                u_char databits;       /* 7,8 datenbit rev. 3.30                      */
                u_char stopbits;       /* 1,2 stopbit  rev. 3.30                      */
                u_char blockcheck;     /* EDV_LRC oder EDV_CRC                        */
                u_char flowcontrol;    /* HE flowcontrol rev. 3.81                        */
                u_char retryLimit;     /* Wiederholungszaehler (Max)                  */
                u_char windowSize;     /* HDLC-Fenstergroesse rev. 2.12               */
                TYbool synChar;        /* Synczeichen senden (J/N)                    */
                TYbool modem;          /* mit/ohne Modem-Manager rev. 2.12            */
                u_short rtmo;          /* read timeout rev. 4.21 */
                char hwRel[6];         /* Hardware-Release                            */
                char swRel[6];         /* Software-Release                            */
                char controller[6];
                u_short devtype;       /* Devicetyp  1 = tty 0 = rs422                */
                TYedvCtrl edvCtrl;     /* rev. 4.30 */
               } TYedvConf;

typedef struct {
                u_short iFramesSent;   /* erfolgreich gesendete I-Rahmen */
                u_short iFramesRecvd;  /* korrekt empfangene I-Rahmen */
                u_short uiFramesSent;  /* erfolgreich gesendete UI-Rahmen */
                u_short uiFramesRecvd; /* korrekt empfangene UI-Rahmen */
                u_short retries;       /* Wiederholungen */
                u_short rejectsSent;   /* gesendete Zurueckweisungen */
                u_short rejectsRecvd;  /* empfangene Zurueckweisungen */
                u_short overflows;     /* Anzahl der Empfangspufferueberlaeufe */
               } TYedvStat;

#define EVEN 'e'
#define ODD  'o'
#define NONE 'n'

#endif
