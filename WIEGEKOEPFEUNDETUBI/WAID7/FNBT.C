#ifdef  IDENT   /* rev. 3.51 */
static char ident[] = "@(#) fnbt.c  VLUGLOTSA   25.04.98 by SE.TEC"
#endif


/***************************************************************************/
/* Programmname :     fnb.c      TEST -Ablauf                              */
/*-------------------------------------------------------------------------*/
/* Funktion :        Applikationsschnittstelle (API)                       */
/*                   Initialisierung der Schnittstelle und des Geraetes    */
/*-------------------------------------------------------------------------*/
/* Revision : 1.00    Datum : 25.041.98  erstellt von :  SE.TEC            */
/*-------------------------------------------------------------------------*/
/* Kurzbeschreibung : Hauptprogramm                                        */
/*                                                                         */
/* - Lesen der Schnittstellenparameter                                     */
/* - Senden der Geraete-Initialisierung, Ergebnisbewertung                 */
/*-------------------------------------------------------------------------*/
/* Deklaration: void main(int argc, char *argv[])                          */
/*                                                                         */                                          
/* Eingabe : par1      :  fnb ->                                           */
/* fnb00 : Init der Schnittstelle und des Geraetes Return : errcode        */
/* fnb02 : Fehlerkorrektur + Errcode                                       */
/* fnb03 : Nullstellen + Errcode                                           */
/* fnb04 : Tarieren + Errcode                                              */
/* fnb05 : Tara loeschen + Errcode                                         */                    
/* fnb06 : Waagenumschaltung                                               */  
                                                 
/* fnb08 : wiegen MIT Ruhewertung (ohne ist Mist !!) Return : Gewichtswert */
/* fnb11 : Postenregistrierung                                             */
/* fnb23 : Tarieren mit Wertvorgabe                                        */
/*                                                                         */
/* Eingabe : par2 : Dialog-Directory : da steht die Schnittstelleninfo !!  */
/* Eingabe : par3 : (OPTIONAL) Inputwert(Fixformat: VZ+5+.+3) (fnb6:1|2|3) */
/* Ausgabe : (OPTIONAL) outdatei ( mit Errcode + Wert )                    */
/*                                                                         */
/* EXIT Status       : TYerrno err: Fehlerstatus                           */
/*       = RET_OK : kein Fehler                                            */
/*       = 1    : Parameterfehler                                          */
/*       = 2    : sonstiger Fehler -> weiter spezifizierbar                */
/* Aufruf : fnb0 par1 par2 [ par3 ]                                        */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* Aenderungsstand :                                                       */
/* Revision : 1.0  Datum : dd.mm.yyyy  geaendert von :  SE.TEC             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* am 27.04.98 : nur ITL realisiert ->                                     */
/* 04.05.98 : Device-Lesen vorerst nur als dummy  */

/*       STANDARD INCLUDE'S               */
#include "getr_01.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>

/* externe Deklarationen                 */

extern int fnbi ( int argc , char *argv[] )  ;
extern int ergebnis ( double ergebmat[] , char *ergebchar[] ) ;

/*-------------------------------------------------------------------------*/

void main (int argc , char *argv[] ) 
{
int argcx ;
char *argvx[4] ;
char mops0[40] ;
char mops1[40] ;
char mops2[80] ;
char mops3[40] ;

double ergebmat[4] ;
char  *ergebchar[2] ;

char    ilakey[21] ;
char dialdatin[21] ;
int ret ;

ergebchar[0] = ilakey ;
ergebchar[1] = dialdatin ;
argvx[0] = mops0 ;
argvx[1] = mops1 ;
argvx[2] = mops2 ;
argvx[3] = mops3 ;
argcx = 3 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "2" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;

fnbi ( argcx , argvx) ;
printf ( "2 gesendet \n" ) ;
Sleep ( 5000 ) ;
argcx = 3 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "11" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;

fnbi ( argcx , argvx) ;
printf ( "11 gesendet \n" ) ;
Sleep ( 5000 ) ;
argcx = 4 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "203" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;
sprintf ( mops3 , "Test Eingabe" ) ;
fnbi ( argcx , argvx ) ;

sprintf ( mops3 , " " ) ;
ret = ergebnis ( ergebmat , ergebchar ) ;
printf ( "Ergebnisse : Taste %s Eingabe %s \n",ergebchar[0],ergebchar[1] ) ;

argcx = 3 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "11" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;
fnbi ( argcx , argvx ) ;

ret = ergebnis ( ergebmat , ergebchar ) ;
printf ( "Ergebnisse : Brutto %lf Netto %lf Tara %lf\n",ergebmat[0],ergebmat[1],ergebmat[2]) ;

fnbi ( argcx , argvx) ;
Sleep ( 2000 ) ;
argcx = 4 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "200" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;

fnbi ( argcx , argvx ) ;

sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "2" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;

fnbi ( argcx , argvx) ;

Sleep ( 1000 ) ;
argcx = 4 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "203" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;
sprintf ( mops3 , "knoten" ) ;
fnbi ( argcx , argvx ) ;
 
Sleep ( 1000 ) ;
argcx = 4 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "203" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;
sprintf ( mops3 , "knoten" ) ;
fnbi ( argcx , argvx ) ;
Sleep ( 1000 ) ;
argcx = 4 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "203" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;
sprintf ( mops3 , "knoten" ) ;
fnbi ( argcx , argvx ) ;
Sleep ( 1000 ) ;
argcx = 4 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "203" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;
sprintf ( mops3 , "knoten" ) ;
fnbi ( argcx , argvx ) ;
Sleep ( 1000 ) ;
argcx = 4 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "203" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;
sprintf ( mops3 , "knoten" ) ;
fnbi ( argcx , argvx ) ;
Sleep ( 1000 ) ;
argcx = 4 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "203" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;
sprintf ( mops3 , "knoten" ) ;
fnbi ( argcx , argvx ) ;
Sleep ( 1000 ) ;
argcx = 4 ;
sprintf ( mops0 , "mist" ) ;
sprintf ( mops1 , "203" ) ;
sprintf ( mops2 , "c:\\user\\cs\\id7" ) ;
sprintf ( mops3 , "knoten" ) ;
fnbi ( argcx , argvx ) ;
}

