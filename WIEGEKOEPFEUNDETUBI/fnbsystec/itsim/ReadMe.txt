========================================================================
    KONSOLENANWENDUNG: itsim-Projekt�bersicht
========================================================================

Diese itsim-Anwendung wurde vom Anwendungs-Assistenten
f�r Sie erstellt.  

Diese Datei bietet eine �bersicht �ber den Inhalt der einzelnen Dateien,
aus denen Ihre itsim�Anwendung besteht.


itsim.vcproj
    Dies ist die Hauptprojektdatei f�r VC++-Projekte, die mit dem Anwendungs-
    Assistenten generiert werden. 
    Sie enth�lt Informationen �ber die Version von Visual C++, mit der die
    Datei generiert wurde, sowie �ber die Plattformen, Konfigurationen und 
    Projektfeatures, die im Anwendungs-Assistenten ausgew�hlt wurden.

itsim.cpp
    Dies ist die Hauptquelldatei der Anwendung.

/////////////////////////////////////////////////////////////////////////////
Weitere Standarddateien:

StdAfx.h, StdAfx.cpp
    Mit diesen Dateien werden eine vorkompilierte Header (PCH)-Datei
    mit dem Namen "itsim.pch" und eine 
    vorkompilierte Typendatei mit dem Namen "StdAfx.obj" erstellt.

/////////////////////////////////////////////////////////////////////////////
Weitere Hinweise:

Der Anwendungs-Assistent weist Sie mit "TODO:"-Kommentaren auf Teile des
Quellcodes hin, die Sie erg�nzen oder anpassen sollten.

/////////////////////////////////////////////////////////////////////////////