// itsim.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"

#include <stdio.h>
#include <time.h>
#include "winsock2.h"

typedef struct { int kg; int g; } GEWICHT;

static GEWICHT gBrutto;
static GEWICHT gTara;
static GEWICHT gNetto;

static int nWaage = 1;
static int nIdent;
static int dMonat;
static int dTag;
static time_t t;
static int eCode;
static int wCode;

static char szTime[200];
static char szErrMsg[200];
static char cmd[3];
static char szIP[25];
static int nPort;

static int bytesSent;
static int bytesRecv = SOCKET_ERROR;

static char sendbuf[300] = "";
static char recvbuf[300] = "";

int nWSAerr;
static WSADATA wsaData;
static SOCKET m_socket;
static SOCKET AcceptSocket;

static sockaddr_in service;

void CalcNetto( void )
{
	gNetto.kg = gBrutto.kg - gTara.kg;
	gNetto.g = gBrutto.g - gTara.g;
	printf( "B = %4d.%03dkg, T = %4d.%03dkg, N = %4d.%03dkg\n",
			gBrutto.kg, gBrutto.g, gTara.kg, gTara.g, gNetto.kg, gNetto.g );
}

void FormResponse( void )
{
	cmd[0] = recvbuf[1];
	cmd[1] = recvbuf[2];
	cmd[2] = '\0';

	if ( cmd[0] == 'R' )
	{
		t = time( NULL );
		strftime( szTime, sizeof szTime, "%d.%m.%y%H:%M", localtime( &t ) );
		nIdent++;
		CalcNetto();
		//       <000031.01.1209:47   51    50.8     0.0    50.8kg     0   53533>
		printf( "<         1         2         3         4         5         6   \r\n" );
		printf( "<12345678901234567890123456789012345678901234567890123456789012>\r\n" );
		printf( "<EEWWTT.MM.JJHH:MMIIIIWBBBBBBBBTTTTTTTTNNNNNNNNEETTBTTTCCCCCCCC>\r\n" );
		sprintf( sendbuf, "<0000%s%4d1%4d.%03d%4d.%03d%4d.%03dkgTTBTTTCCCCCCCC>\r\n",
			szTime, nIdent, gBrutto.kg, gBrutto.g, gTara.kg, gTara.g, gNetto.kg, gNetto.g );
	}
	else if ( strcmp( cmd, "TA" ) == 0 )
	{
		gTara.kg = gNetto.kg;
		gTara.g = gNetto.g;
		CalcNetto();
		sprintf( sendbuf, "<00>\r\n" );
	}
	else if ( strcmp( cmd, "TC" ) == 0 )
	{
		gTara.kg = 0;
		gTara.g = 0;
		CalcNetto();
		sprintf( sendbuf, "<00>\r\n" );
	}
	else if ( strcmp( cmd, "SZ" ) == 0 )
	{
		gBrutto.kg = 0;
		gBrutto.g = 0;
		CalcNetto();
		sprintf( sendbuf, "<00>\r\n" );
	}
}

int ReceiveAndSend( void )
{
	nWSAerr = WSAStartup( MAKEWORD(2,2), &wsaData );
	if ( nWSAerr != NO_ERROR )
	{
		FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM, NULL, nWSAerr, 0, szErrMsg, sizeof szErrMsg, NULL );
		printf( _T("Error at WSAStartup(): %d %s\n"), nWSAerr, szErrMsg );
		return 0;
	}

	// Create a socket.
	m_socket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
	if ( m_socket == INVALID_SOCKET )
	{
		nWSAerr = WSAGetLastError();
		FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM, NULL, nWSAerr, 0, szErrMsg, sizeof szErrMsg, NULL );
		printf( _T("Error at socket(): %d %s\n"), nWSAerr, szErrMsg );
		WSACleanup();
		return 0;
	}

	// Bind the socket.

	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr( szIP );
	service.sin_port = htons( nPort );

	if ( bind( m_socket, (SOCKADDR*) &service, sizeof(service) ) == SOCKET_ERROR )
	{
		nWSAerr = WSAGetLastError();
		FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM, NULL, nWSAerr, 0, szErrMsg, sizeof szErrMsg, NULL );
		printf( _T("Error at bind(): %d %s\n"), nWSAerr, szErrMsg );
		closesocket(m_socket);
		return 0;
	}

	if ( listen( m_socket, 1 ) == SOCKET_ERROR )
	{
		nWSAerr = WSAGetLastError();
		FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM, NULL, nWSAerr, 0, szErrMsg, sizeof szErrMsg, NULL );
		printf( _T("Error at listen(): %d %s\n"), nWSAerr, szErrMsg );
		closesocket(m_socket);
		return 0;
	}


	for (;;)
	{
		printf( "Waiting for a client to connect to %s:%d ...\n", szIP, nPort );
		AcceptSocket = SOCKET_ERROR;
		while ( AcceptSocket == SOCKET_ERROR )
		{
			AcceptSocket = accept( m_socket, NULL, NULL );
		}
		printf( "Client Connected.\n");

		bytesRecv = recv( AcceptSocket, recvbuf, 32, 0 );
		printf( "Bytes Recv: %ld String Recv: %s\n", bytesRecv, recvbuf );

		FormResponse();
		printf( sendbuf );
		printf( "%d Bytes to send ...\n", strlen(sendbuf) );

		bytesSent = send( AcceptSocket, sendbuf, (int)strlen(sendbuf), 0 );
		printf( "... Bytes Sent: %ld\n", bytesSent );

		closesocket(AcceptSocket);
	}
	return 1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	char * p;
	if ( argc < 2 )
	{
		printf( "%s ip:port\n", argv[0] );
		return 1;
	}
	strcpy( szIP, argv[1] );
	p = strchr( szIP, ':' );
	if ( p == NULL )
	{
		printf( "%s ip:port\n" );
		return 1;
	}
	nPort = atoi( p+1 );
	*p = '\0';

	nIdent = 0;
	gBrutto.kg = 3;
	gBrutto.g = 125;
	gTara.kg = 0;
	gTara.g = 125;

	ReceiveAndSend();

	WSACleanup();

	return 0;
}
