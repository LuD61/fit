// fnbsystecdlltest.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"

#include <stdio.h> 
#include <windows.h> 
 
typedef int (*FNBPROC)( int, char*[] );

TCHAR dllname[200];

int _tmain(int argc, _TCHAR* argv[])
{
    HINSTANCE hinstLib; 
    FNBPROC fnbproc; 
    BOOL fFreeResult, fRunTimeLinkSuccess = FALSE;

	if (argc < 3)
	{
	    _tprintf( "Zu wenig Argumente.\n" );
		return 1;
	}
 
	_stprintf( dllname, TEXT("%s\\fnb.dll"), argv[2] );
	_tprintf( TEXT("Lade DLL %s ... \n"), dllname );
    hinstLib = LoadLibrary(dllname); 
 
    if (hinstLib == NULL) 
    { 
	    _tprintf( "... hat leider nicht funktioniert.\n" );
		return 1;
    } 
	_tprintf( "... OK \n" );
	_tprintf( "Hole fnb-Funktionsadresse ... \n" );
    
	fnbproc = (FNBPROC) GetProcAddress( hinstLib, TEXT("fnb") ); 
    if (fnbproc == NULL) 
    { 
	    _tprintf( "... hat leider nicht funktioniert.\n" );
        fFreeResult = FreeLibrary( hinstLib ); 
		return 1;
    } 
 	_tprintf( "... OK \n" );
    fRunTimeLinkSuccess = TRUE;

	_tprintf( "Rufe fnb ... \n" );
	(fnbproc)( argc, argv );

    fFreeResult = FreeLibrary(hinstLib); 
 
	return 0;
}

