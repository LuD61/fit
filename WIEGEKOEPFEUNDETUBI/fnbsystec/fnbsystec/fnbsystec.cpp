// fnbsystec.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include <stdio.h>
#include "winsock2.h"

typedef struct { int kg; int g; } GEWICHT;

static GEWICHT gBrutto;
static GEWICHT gTara;
static GEWICHT gNetto;

static int nWaage = 1;
static int nIdent;
static int dMonat;
static int dTag;

static int eCode;
static int wCode;

static int nLogDevice;
static FILE *fperr = stderr;

static int nPort;
static TCHAR szIP[20];

static char sendbuf[300];
static char recvbuf[300];

static TCHAR waaktdir[300];
static TCHAR inifile[300];
static TCHAR logfile[300];
static TCHAR ergebnis[300];
static TCHAR szMessage[300];
static TCHAR szErrMsg[300];

static SOCKET m_socket = INVALID_SOCKET;

static int connected = 0;

void usage( void )
{
    _tprintf( "\n" );
    _tprintf( "\t\t fnb.exe\n" );
    _tprintf( "\t\t fnb fuer Waage Systec IT4000E per Ethernet\n" ); 
    _tprintf( "\n" );
    _tprintf( "\t\t AUFRUFVARIANTEN:\n" );
    _tprintf( "\t\t fnb  0 WAAKTDIR\t\t... Initialisierung\n" );
    _tprintf( "\t\t fnb  2 WAAKTDIR\t\t... Initialisierung\n" );
    _tprintf( "\t\t fnb  3 WAAKTDIR\t\t... Nullstellen\n" );
    _tprintf( "\t\t fnb  4 WAAKTDIR\t\t... Tarieren[waage]\n" );
    _tprintf( "\t\t fnb  5 WAAKTDIR\t\t... Tara loeschen[waage]\n" );
    _tprintf( "\t\t fnb  6 WAAKTDIR WaageNr\t... Waage waehlen\n" );
    _tprintf( "\t\t fnb  8 WAAKTDIR\t\t... Wiegen auf bewegter Waage\n" );
    _tprintf( "\t\t fnb 11 WAAKTDIR\t\t... Wiegen auf unbewegter Waage\n" );
    _tprintf( "\t\t fnb 23 WAAKTDIR Tara\t\t... Tarieren mit Vorgabewert\n" );
    _tprintf( "\n" );
	_tprintf( "\t\t Initialisierungsdatei:\t WAAKTDIR\\fnb.ini\n" ); 
	_tprintf( "\t\t Ergebnisdatei:\t\t WAAKTDIR\\ergebnis\n" ); 
	_tprintf( "\t\t Logdevice:\t\t 2 --> stderr\n" ); 
	_tprintf( "\t\t \t\t\t 3 --> WAAKTDIR\\fnb.log\n" ); 
	_tprintf( "\t\t \t\t\t 4 --> stderr + WAAKTDIR\\fnb.log\n" ); 
    _tprintf( "\n" );
    _tprintf( "\t\t VERSION:\n" ); 
    _tprintf( "\t\t %s, Setec GmbH G. Koenig\n", __DATE__ ); 
    _tprintf( "\n" );
}

void printlog( TCHAR *szMsg, ... )
{
	if (nLogDevice < 1) return;

	va_list ap;
	va_start( ap, szMsg );
	wvsprintf( szMessage, szMsg, ap ); 
	va_end( ap );

	if (nLogDevice == 4) _ftprintf( stderr, _T("%s"), szMessage );
	_ftprintf( fperr, _T("%s"), szMessage );
	fflush( fperr );
}

void ReadIni( void )
{
	nLogDevice = GetPrivateProfileInt( _T("LOG"), _T("DEVICE"), 1, inifile );
	switch (nLogDevice)
	{
	case 2:
		fperr = stderr;
		printlog( _T("Log --> stderr\n") );
		break;
	case 3:
	case 4: 
		fperr = fopen( logfile, "w" );
		printlog( _T("Log --> %s\n"), logfile );
		break;
	}
	printlog( _T("Initialsierungsdatei: %s\n"), inifile );
	GetPrivateProfileString( _T("WAAGE"), _T("IP"),	_T("0.0.0.0"), szIP, sizeof szIP, inifile );
	nPort = GetPrivateProfileInt( _T("WAAGE"), _T("PORT"), 0, inifile );
	printlog( _T("Waage: IP = %s, PORT = %d\n"), szIP, nPort );
}

void WriteErgebnis( void )
{
	FILE *fp;
	printlog( _T("B%9d.%03d\n"), gBrutto.kg, gBrutto.g );
	printlog( _T("T%9d.%03d\n"), gTara.kg, gTara.g );
	printlog( _T("N%9d.%03d\n"), gNetto.kg, gNetto.g );
	printlog( _T("E     %02d%02d%04d\n"), dMonat, dTag, nIdent );
	fp = fopen( ergebnis, "w" );
	if (fp == NULL)
	{
		printlog( _T("Fehler bei fopen(%s , ""w""\n"), ergebnis );
	    return;
	}
	_ftprintf( fp, _T("B%9d.%03d\n"), gBrutto.kg, gBrutto.g );
	_ftprintf( fp, _T("T%9d.%03d\n"), gTara.kg, gTara.g );
	_ftprintf( fp, _T("N%9d.%03d\n"), gNetto.kg, gNetto.g );
	_ftprintf( fp, _T("E     %02d%02d%04d\n"), dMonat, dTag, nIdent );
	fclose( fp );
}

void PrintErrorcode( int eCode )
{
	printlog( _T("Errorcode: ") );
	switch( eCode )
	{
	case 00: printlog( _T("00: Kein Fehler\n") ); break;
	case 11: printlog( _T("11: Allgemeiner Waagenfehler (z.B. Verbindung zur Lastzelle gest�rt)\n") ); break;
	case 12: printlog( _T("12: Waage in �berlast (Gewicht �berschreitet den maximalen W�gebereich)\n") ); break;
	case 13: printlog( _T("13: Waage in Bewegung (nach 10 Sek. keine Ruhe)\n") ); break;
	case 14: printlog( _T("14: Waage nicht verf�gbar (z.B. nur eine Waage kalibriert)\n") ); break;
	case 15: printlog( _T("15: Tarierungsfehler (z.B. Taragewichts-Formatierung falsch)\n") ); break;
	case 16: printlog( _T("16: Gewichts-Drucker nicht bereit (offline)\n") ); break;
	case 17: printlog( _T("17: Druckmuster enth�lt ung�ltiges Kommando\n") ); break;
	case 18: printlog( _T("18: Nullstellfehler (au�erhalb Nullstellbereich oder Waage in Bewegung)\n") ); break;
	case 31: printlog( _T("31: �bertragungsfehler (z.B. Datensatz zu lang oder Time-out)\n") ); break;
	case 32: printlog( _T("32: Ung�ltiger Befehl\n") ); break;
	case 33: printlog( _T("33: Ung�ltiger Parameter\n") ); break;
	default: printlog( _T("%02d: Unbekannter Errorcode\n"), eCode ); break;
	}
}

void PrintWaagenstatus( int wCode )
{
	printlog( _T("Waagenstatus: ") );
	switch( wCode )
	{
	case 0: printlog( _T("00: in Ruhe, Brutto > 0\n") ); break;
	case 1: printlog( _T("10: in Bewegung, Brutto > 0\n") ); break;
	case 2: printlog( _T("01: in Ruhe, Brutto < 0\n") ); break;
	case 3: printlog( _T("11: in Bewegung, Brutto < 0\n") ); break;
	default: printlog( _T("%02d: Unbekannter Waagenzustand\n"), wCode ); break;
	}
}

int GetNumber( char * szNumber, size_t len )
{
	char szN[10];
    memcpy( szN, szNumber, len );
	szN[len] = '\0';
	return atoi( szN );
}

GEWICHT GetGewicht( char *szGew, size_t len )
{
	GEWICHT g;
	char szG[21];
	char *p;

    memcpy( szG, szGew, len );
	szG[len] = '\0';
    g.kg = atoi( szG );
	g.g = 0;
	p = strchr( szG, ',' );
	if ( p == NULL )
	{
		p = strchr( szG, '.' );
	}
	if ( p == NULL )
	{
		return g;
	}
	if ( strlen(p) < 2 )
	{
		return g;
	}
	p++;
	switch ( strlen(p) )
	{
	case 1: g.g = 100 * atoi(p); break;
	case 2: g.g = 10 * atoi(p); break;
	case 3: g.g = atoi(p); break;
	}
	printlog( _T("Gewichtswertformatierung: %s --> %9d.%03d\n"), szG, g.kg, g.g );
	return g;

}

int Auswertung( size_t len )
{
	printlog( _T("Laenge der Quittung: %d\n"), len );
	if ( len < strlen( "<nn>\n" ) )
	{
		printlog( _T("Seltsame Quittung erhalten: Zu kurz.\n") );
        return 0;
	}
	if ( recvbuf[0] != '<' )
	{
		printlog( _T("Seltsame Quittung erhalten: Beginnt nicht mit '<'.\n") );
        return 0;
	}
	if ( recvbuf[len-2] != '>' )
	{
		printlog( _T("Seltsame Quittung erhalten: Endet nicht mit '>'.\n") );
        return 0;
	}
    eCode = GetNumber( recvbuf + strlen("<"), strlen("EE") );
	PrintErrorcode( eCode );
	if ( eCode != 0 )
	{
		return 0;
	}
	if ( len < strlen("<EEWWTT.MM.JJHH:MMIIIIWBBBBBBBBTTTTTTTTNNNNNNNNEETTBTTTCCCCCCCC>\n") )
	{
        return 1; // Kurze Quittung nur mit Status
	}

    wCode = GetNumber( recvbuf + strlen("<EE"), strlen("WW") );
	PrintWaagenstatus( wCode );
    
	gBrutto = GetGewicht( recvbuf + strlen("<EEWWTT.MM.JJHH:MMIIIIW"), strlen("BBBBBBBB") );
    gTara = GetGewicht( recvbuf + strlen("<EEWWTT.MM.JJHH:MMIIIIWBBBBBBBB"), strlen("TTTTTTTT") );
    gNetto = GetGewicht( recvbuf + strlen("<EEWWTT.MM.JJHH:MMIIIIWBBBBBBBBTTTTTTTT"), strlen("NNNNNNNN") );
    nIdent = GetNumber( recvbuf + strlen("<EEWWTT.MM.JJHH:MM"), strlen("IIII") );
    dMonat = GetNumber( recvbuf + strlen("<EEWWTT."), strlen("MM") );
    dTag = GetNumber( recvbuf + strlen("<EEWW"), strlen("TT") );
	WriteErgebnis();
	return 1;
}

int InitializeConnection( void )
{
	if ( connected ) return 1;

	// Initialize Winsock.
	WSADATA wsaData;
	int iResult = WSAStartup( MAKEWORD(2,2), &wsaData );
	if ( iResult != NO_ERROR )
	{
		printlog( _T("Error at WSAStartup()\n") );
	}

	// Create a socket.
	m_socket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );

	if ( m_socket == INVALID_SOCKET )
	{
		printlog( _T("Error at socket(): %ld\n"), WSAGetLastError() );
		WSACleanup();
		return 0;
	}

	// Connect to a server.
	sockaddr_in clientService;

	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = inet_addr( (char*) szIP );
	clientService.sin_port = htons( nPort );

	if ( connect( m_socket, (SOCKADDR*) &clientService, sizeof(clientService) ) == SOCKET_ERROR )
	{
		printlog( _T("Failed to connect.\n") );
		WSACleanup();
		return 0;
	}
	connected = 1;
	return 1;
}

int SendAndReceive( void )
{
	int bytesSent, bytesRecv;
	char *p, *q;
	long nWSAerr;

	InitializeConnection();

	printlog( _T("PC --> IT : %s\n"), sendbuf );
	bytesSent = send( m_socket, sendbuf, (int) strlen(sendbuf), 0 );

    memset( recvbuf, '\0', sizeof(recvbuf) );
   
	for( p = recvbuf; ; p += bytesRecv )
	{
		bytesRecv = recv( m_socket, p, (int) sizeof(recvbuf), 0 );
		//printlog( _T("Bytes Recv: %d\n", bytesRecv );
		if ( bytesRecv == 0 || bytesRecv == WSAECONNRESET )
		{
			printlog( _T("Connection Closed.\n") );
			return 0;
		}
		if ( bytesRecv < 0 )
		{
			nWSAerr = WSAGetLastError();
            FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM, NULL, nWSAerr, 0, szErrMsg, sizeof szErrMsg, NULL );
		    printlog( _T("Error at recv(): %ld %s\n"), nWSAerr, szErrMsg );
			return 0;
		}
		q = strchr( p, '\n' );
		if ( q != NULL )
		{
			printlog( _T("PC <-- IT : %s"), recvbuf );
            break;
		}
	}
	closesocket( m_socket );
	return Auswertung( (int)(q - recvbuf) );
}

int WaageNullStellen( void )
{
	printlog( _T("Waage Nullstellen ...\n") );
	sprintf( sendbuf, "<SZ%1d>", nWaage );
	return SendAndReceive();
}

int Tarieren( void )
{
	printlog( _T("Tarieren ...\n") );
	sprintf( sendbuf, "<TA%1d>", nWaage );
	return SendAndReceive();
}

int TaraLoeschen( void )
{
	printlog( _T("Tarieren ...\n") );
	sprintf( sendbuf, "<TC%1d>", nWaage );
	return SendAndReceive();
}

int WaageWaehlen( int n )
{
	nWaage = n;
	printlog( _T("Waage Nr %d waehlen ...\n"), nWaage );
	sprintf( sendbuf, "<SS%1d>", nWaage );
	return SendAndReceive();
}

int WiegenInUnruhe( void )
{
	printlog( _T("Wiegen auf bewegter Waage ...\n") );
	sprintf( sendbuf, "<RM%1d>", nWaage );
	return SendAndReceive();
}

int WiegenInRuhe( void )
{
	printlog( _T("Wiegen auf unbewegter Waage ...\n") );
	sprintf( sendbuf, "<RN%1d>", nWaage );
	return SendAndReceive();
}

int TarierenMitWert( char * szTara )
{
 	printlog( _T("Tarieren mit %s kg ...\n"), szTara );
    gTara = GetGewicht( szTara, strlen(szTara) );
	//sprintf( sendbuf, "<TM%04d,%03d%01d>", gTara.kg, gTara.g, nWaage );
	sprintf( sendbuf, "<TM%04d.%03d>", gTara.kg, gTara.g );
	return SendAndReceive();
}

#ifdef FNBSYSTECDLL_EXPORTS
#ifdef __cplusplus    // If used by C++ code, 
extern "C" {          // we need to export the C interface
#endif
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved )
{
    return TRUE;
}

_declspec (dllexport) int fnb( int argc, char *argv[] );

_declspec (dllexport) int fnb( int argc, char *argv[] )
#else
int _tmain(int argc, _TCHAR* argv[])
#endif
{
	int n;
	if ( argc < 3 )
	{
		usage();
		return 0;
	} 
	n = atoi( argv[1] );
	_tcscpy( waaktdir, argv[2] );
	_stprintf( inifile, _T("%s\\fnb.ini"), waaktdir );
	_stprintf( logfile, _T("%s\\fnb.log"), waaktdir );
	_stprintf( ergebnis, _T("%s\\ergebnis"), waaktdir );

	ReadIni();

	remove( ergebnis );  // Bei Fehler bleibt sonst alte Ergebnis Datei! GK 20130222

	switch( n )
	{
	case  3: printlog( _T("fnb %d %s\n"), n, waaktdir );
             WaageNullStellen();
             break;

	case  4: printlog( _T("fnb %d %s\n"), n, waaktdir );
		     Tarieren(); 
			 break;

	case  5: printlog( _T("fnb %d %s\n"), n, waaktdir );
		     TaraLoeschen();
		     break;

	case  6: if ( argc < 4 )
			 {
				 usage();
				 return 0;
			 }
			 printlog( _T("fnb %d %s %s\n"), n, waaktdir, argv[3] ); 
		     WaageWaehlen( atoi( argv[3] ) );
			 break;

	case  8: printlog( _T("fnb %d %s \tErgebnis --> %s\n"), n, waaktdir, ergebnis );
		     WiegenInUnruhe();
		     break;

	case 11: printlog( _T("fnb %d %s \tErgebnis --> %s\n"), n, waaktdir, ergebnis );
		     WiegenInRuhe();
		     break;

	case 23: if ( argc < 4 )
			 {
				 usage();
				 return 0;
			 }
			 printlog( _T("fnb %d %s %s\n"), n, waaktdir, argv[3] ); 
		     TarierenMitWert( argv[3] );
			 break;

	default: printlog( _T("Funktion Nr %d ist nicht implementiert.\n"), n );
		     break;
	}

	WSACleanup();

	return 0;
}
#ifdef FNBSYSTECDLL_EXPORTS
#ifdef __cplusplus
}
#endif
#endif

