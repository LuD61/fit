#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include "getr_01.h"
#include "paras.h" 
extern char dialogdir [] ;
extern TYedvConf edv_conf ;
extern TYedvChan edv_chan ;
extern char logtext[] ;
extern int dev_type ;
extern int kommastellen ;
extern int mitquit ;
extern int tfaktor ;

extern int dbsoh ;      // 060410
extern int dbez1 ;      // 060410
extern int dbez2 ;      // 060410
extern int dbbt  ;      // 060410

extern void logschreib(void) ;
extern int testxx ;
extern int sics ;        // 290699
extern long dsleep ;     // 050202
static char *what = "@(#) get_defa.c 160698/290699 JG"  ;

void upshifts( char *) ;

void get_defax ( void )
{ 
 char item_name[40] ;  
 char item_value[40] ;
 char buf[140] ;
 char *p ;
 FILE *fp ;

  memset ( buf , '\0' , 140 ) ;
  strcpy ( buf , dialogdir ) ; 
  strcat ( buf , "\\ergebnis" ) ;
  remove ( buf ) ;

  memset ( buf , '\0' , 140 ) ;
  strcpy ( buf , dialogdir ) ; 
  strcat ( buf , "\\commdef" ) ;
  fp=fopen ( buf , "r" ) ;
  sprintf ( logtext ,"buf : %s\n", buf ) ;
  logschreib () ;
  dev_type = 0 ;
  dsleep = 0L ;
  mitquit = 0 ; // 070706
  sics = 0 ;    // 290699
  tfaktor = 1 ;

  dbsoh = 0 ;           // 060410 : kein soh-Zeichen
  dbez1 = 0xd ;         // 060410 : 0xd 1. Ende-Zeichen ( 12 dez. )
  dbez2 = 0xa ;         // 060410 : 0xa 2. Ende-Zeichen ( 10 dez. )
  dbbt  = 03 ;          // 060410 : 03 Blocktrenner -ohne geht nix !!!!


  if ( fp == NULL ) return ;
  while ( fgets ( buf , sizeof(buf) , fp) != NULL ) 
  {
    p= strtok ( buf, " \t" ) ;
    upshifts (p) ;
    sprintf ( logtext ,"INPUT : %s\n", p ) ;
    logschreib () ;

 /* =============   MITQUIT ============================== */
    sprintf ( item_name, MITQUIT ) ;
    if ( strcmp ( p,item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      if ( strlen(item_value) > 0 )
      {
          sprintf ( item_name, SE_1 ) ;
          if ( strcmp (item_value , item_name ) == 0 )
                mitquit = 1 ;
      }
      sprintf ( logtext ,"MITQUIT : %d\n", mitquit ) ;
      logschreib () ;
      continue ;
    }           /* nicht MITQUIT */

// 060410 : BBT-Zeichen ( dezimal oder keines )
 /* =============   BSOH ============================== */
    sprintf ( item_name, "BBT" ) ;
    if ( strcmp ( p,item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      if ( strlen(item_value) > 0 )
      {
        dbbt = atoi ( p ) ;
//        if ( dbbt < 1 ) dbbt = 3 ;      // Zwangsfuehrung
        sprintf ( logtext ,"BBT : %d\n", dbbt ) ;
        logschreib () ;
        continue ;
      }
    }           /* nicht BBT */


// 060410 : SOH-Zeichen ( dezimal oder keines )
 /* =============   BSOH ============================== */
    sprintf ( item_name, "BSOH" ) ;
    if ( strcmp ( p,item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      if ( strlen(item_value) > 0 )
      {
        dbsoh = atoi ( p ) ;
        sprintf ( logtext ,"SOH : %d\n", dbsoh ) ;
        logschreib () ;
        continue ;
      }
    }           /* nicht BSOH */

// 060410 : 1.EndeZeichen-Zeichen ( defa :0xd : dezimal oder keines )
 /* =============   BEZ1 ============================== */
    sprintf ( item_name, "BEZ1" ) ;
    if ( strcmp ( p,item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      if ( strlen(item_value) > 0 )
      {
        dbez1 = atoi ( p ) ;
        sprintf ( logtext ,"EZ1 : %d\n", dbez1 ) ;
        logschreib () ;
        continue ;
      }
    }           /* nicht BEZ1 */

// 060410 : 2.EndeZeichen-Zeichen ( defa :0xa : dezimal oder keines )
 /* =============   BEZ2 ============================== */
    sprintf ( item_name, "BEZ2" ) ;
    if ( strcmp ( p,item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      if ( strlen(item_value) > 0 )
      {
        dbez2 = atoi ( p ) ;
        sprintf ( logtext ,"EZ2 : %d\n", dbez2 ) ;
        logschreib () ;
        continue ;
      }
    }           /* nicht BEZ2 */

 /* =============   MITQUIT ============================== */
    sprintf ( item_name, MITQUIT ) ;
    if ( strcmp ( p,item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      if ( strlen(item_value) > 0 )
      {
          sprintf ( item_name, SE_1 ) ;
          if ( strcmp (item_value , item_name ) == 0 )
                mitquit = 1 ;
      }
      sprintf ( logtext ,"MITQUIT : %d\n", mitquit ) ;
      logschreib () ;
      continue ;
    }           /* nicht MITQUIT */

 /* =============   SLEEP ============================== */
    sprintf ( item_name, SLEEP ) ;
    if ( strcmp ( p,item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      if ( strlen(item_value) > 0 )
          dsleep = atol ( p ) ;
      sprintf ( logtext ,"SLEEP : %ld\n", dsleep ) ;
      logschreib () ;
      continue ;
    }           /* nicht SLEEP */

 /* =============   BAUDRATE ============================== */
    sprintf ( item_name, SE_BAUD ) ;
    if ( strcmp ( p,item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      sprintf ( item_name, SE_9600 ) ;
      sprintf ( logtext ,"BAUD : %s\n", item_value ) ;
      logschreib () ;
      if ( strcmp (item_value , item_name ) == 0 )
      edv_conf.baudrate = 9600 ;
      else
      { sprintf ( item_name, SE_2400 ) ;
        if ( strcmp ( item_value , item_name ) == 0 )
        edv_conf.baudrate = 2400 ;
        else
        { sprintf ( item_name, SE_4800 ) ;
          if ( strcmp ( item_value , item_name ) == 0 )
          edv_conf.baudrate = 4800 ;
          else
          { sprintf ( item_name, SE_19200 ) ;
            if ( strcmp ( item_value , item_name ) == 0 )
            edv_conf.baudrate = 19200 ;
          }             /* nicht 4800 */
        }               /* nicht 2400 */
      }                 /* nicht 9600 */
      continue ;
    }           /* nicht BAUD */
 /* =============   GERAET ============================== */
    sprintf ( item_name, SE_DEVI ) ;
    if ( strcmp ( p,item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      upshifts ( p ) ;
      strcpy ( item_value, p ) ;
      sprintf ( item_name, SE_ITL ) ;
      sprintf ( logtext ,"GERAET : %s\n", item_value ) ;
      logschreib () ;
      if ( strcmp (item_value , item_name ) == 0 )
      {dev_type = GE_ITL ; }
      else
      { sprintf ( item_name, SE_ITE ) ;
        if ( strcmp ( item_value , item_name ) == 0 )
        { dev_type = GE_ITE ; }
        else
        { sprintf ( item_name, SE_ID7 ) ;
          if ( strcmp ( item_value , item_name ) == 0 )
          { dev_type = GE_ID7; }
        }
      }
      continue ;
    }                   /* nicht GERAET */
/* =============   PARITY ============================== */
    sprintf ( item_name, SE_PARI ) ;
    if ( strcmp ( p , item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      upshifts (p) ;    // 260901
      strcpy ( item_value, p ) ;
// 260901      upshifts (p) ;
      sprintf ( logtext ,"PARI : %s\n", item_value ) ;
      logschreib () ;
      sprintf ( item_name, SE_ODD ) ;
      if ( strcmp (item_value , item_name ) == 0 )
      edv_conf.parity  = ODD ;
      else
      { sprintf ( item_name, SE_EVEN ) ;
        if ( strcmp ( item_value , item_name ) == 0 )
        edv_conf.parity = EVEN ;
        else
        { sprintf ( item_name, SE_NONE ) ;
          if ( strcmp ( item_value , item_name ) == 0 )
          edv_conf.parity = NONE ;
        }
      }
      continue ;
    }
/* =============   WORDLAENGE ============================== */
    sprintf ( item_name, SE_DATAB ) ;
    if ( strcmp ( p , item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      upshifts (p) ;
      sprintf ( logtext ,"DATAB : %s\n", item_value ) ;
      logschreib () ;
      sprintf ( item_name, SE_7 ) ;
      if ( strcmp (item_value , item_name ) == 0 )
      edv_conf.databits = 7 ;
      else
      { sprintf ( item_name, SE_8 ) ;
        if ( strcmp ( item_value , item_name ) == 0 )
        edv_conf.databits = 8 ;         /* hier stand frueher mal "baudrate"  */
      }
      continue ;
    }
/* =============   STOPPBITS ============================== */
    sprintf ( item_name, SE_STOP ) ;
    if ( strcmp ( p , item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      sprintf ( logtext ,"STOP : %s\n", item_value ) ;
      logschreib () ;
      sprintf ( item_name, SE_1 ) ;
      if ( strcmp (item_value , item_name ) == 0 )
      edv_conf.stopbits = 1 ;
      else
      { sprintf ( item_name, SE_2 ) ;
        if ( strcmp ( item_value , item_name ) == 0 )
        edv_conf.stopbits = 2 ;
      }
      continue ;
    }
/* =============   KOMMASTELLEN ============================== */
    sprintf ( item_name, SE_KOMMA ) ;
    if ( strcmp ( p , item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      sprintf ( logtext ,"KOMMA : %s\n", item_value ) ;
      logschreib () ;
      sprintf ( item_name, SE_1 ) ;
      if ( strcmp (item_value , item_name ) == 0 )
      kommastellen = 1 ;
      else
      { sprintf ( item_name, SE_2 ) ;
        if ( strcmp ( item_value , item_name ) == 0 )
        kommastellen = 2 ;
        else
        { sprintf ( item_name, SE_3 ) ;
          if ( strcmp ( item_value , item_name ) == 0 )
          kommastellen = 3 ;
          // 190106 A
          else
          { sprintf ( item_name, SE__1 ) ;
             if ( strcmp ( item_value , item_name ) == 0 )
             kommastellen = -1 ;
          }
          // 190106 E
        }
      }
      continue ;
    }
/* ============= Schnittstellenname ============================== */
    sprintf ( item_name, SE_COM ) ;
    if ( strcmp ( p , item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      sprintf ( logtext ,"Schnitt-Stelle : %s\n", item_value ) ;
      logschreib () ;
      strcpy ( edv_chan.ddId, item_value )  ;
      continue ;
    }
// 310708 A
/* ============= TFAKTOR  ================= */
    sprintf ( item_name, SE_TFAKTOR ) ;
    if ( strcmp ( p , item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      sprintf ( logtext ,"TFAKTOR : %s\n", item_value ) ;
      logschreib () ;
      tfaktor = atoi ( item_value ) ;
      if ( tfaktor < 1 || tfaktor > 10 )
      tfaktor = 1 ;
      continue ;
    }

// 310708 E

// 251008 A
/* ============= TESTMODE  ================= */
    sprintf ( item_name, SE_TESTMODE ) ;
    if ( strcmp ( p , item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      sprintf ( logtext ,"TESTMODE : %s\n", item_value ) ;
      logschreib () ;
      testxx = atoi ( item_value ) ;
      if ( testxx < 0 || testxx > 10 )          // 281208 : vorher token testmode komplett removen 
      testxx = 1 ;
      continue ;
    }
// 251008 E

// 290699 A
/* ============= SICS-Protokoll ( normal : MMR ) ================= */
    sprintf ( item_name, SE_SICS ) ;
    if ( strcmp ( p , item_name ) == 0 )
    {
      p = strtok (NULL, " \t\n" ) ;                
      strcpy ( item_value, p ) ;
      sprintf ( logtext ,"SICS : %s\n", item_value ) ;
      logschreib () ;
      sprintf ( item_name, SE_1 ) ;
      if ( strcmp (item_value , item_name ) == 0 )
      sics = 1 ;
      else
      { sprintf ( item_name, SE_2 ) ;
        if ( strcmp ( item_value , item_name ) == 0 )
        sics = 2 ;
        else
        { sprintf ( item_name, SE_3 ) ;
          if ( strcmp ( item_value , item_name ) == 0 )
          sics = 3 ;
        }
      }
      continue ;
    }
// 290699 E 
  }
  fclose ( fp ) ;
}

/** upshiften gegen tippfaule menschen **/    

void upshifts ( char *p )
{
int i, k ;
 k=strlen(p) ;
 for ( i = 0 ; i < k ; i++ )
 p[i] = toupper(p[i]) ;
}
