/* defines fuer Parameterdatei */
#define SE_COM "COM"

#define SE_DEVI "DEVI"

#define SE_ITL  "ITL"
#define SE_ITE  "ITE"
#define SE_ID7  "ID7"
#define SE_MCI  "MCI"

#define SE_SICS "SICS"

#define GE_ITL  1
#define GE_ITE  2
#define GE_ID7  3
#define GE_MCI  4

#define SE_BAUD "BAUD"
#define SE_19200 "19200"
#define SE_9600 "9600"
#define SE_4800 "4800"
#define SE_2400 "2400"

#define SLEEP "SLEEP"

#define MITQUIT "MITQUIT"

#define SE_STOP "STOP"
#define SE_1 "1"
#define SE_2 "2"
#define SE__1 "-1"

#define SE_TFAKTOR "TFAKTOR"
#define SE_TESTMODE "TESTMODE"

#define SE_KOMMA "KOMMA"
#define SE_3 "3"

#define SE_DATAB "DATAB"
#define SE_8 "8"
#define SE_7 "7"

#define SE_PARI "PARI"
#define SE_ODD "ODD"
#define SE_EVEN "EVEN"
#define SE_NONE "NONE"
