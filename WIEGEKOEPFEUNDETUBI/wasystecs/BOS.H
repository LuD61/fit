#ifndef     __BOSH_
#define     __BOSH_
#ifdef  IDENT  
static char ident[] = "@(#) bos.h  VLUGLOTSA   27.04.98 by SE.TEC"
#endif 
/***************************************************************************/
/* Programmname :  BOS.H                                                   */
/*-------------------------------------------------------------------------*/
/* Funktion :  scale OS-specific defines                                   */
/*-------------------------------------------------------------------------*/
/* Revision : 1.00     Datum : 27.04.98   erstellt von : SE.TEC            */
/*-------------------------------------------------------------------------*/
/* Aenderungsstand :                                                       */
/*-------------------------------------------------------------------------*/

/* ***************************************************************** */
/* Daten-Typen */
/* ****************************************************************** */

#ifndef u_char
#define u_char unsigned char
#endif

#ifndef u_short
#define u_short unsigned short
#endif

#ifndef u_long
#define u_long unsigned long
#endif

#ifndef u_int
#define u_int unsigned int
#endif


typedef u_short TYerrno;         /* Datentyp 'Fehler' */

typedef  char  TYbool;        /* boolscher Datentyp  */

typedef  short TYrscID;       /* Datentyp Resource-ID */

typedef struct {
                u_short maxSize; /* Maximale Groesse Buffer */
                u_short usedSize; /* Benutzte Groesse Buffer */
                void   *bufAddr; /* Ptr. auf Buffer */
               } TYbuffer;
/**********************************************************************
* Wertebereiche der Datentypen (siehe Compilerhandbuch CC68000 2-1 ff.)
*
*          Dezimal                         Hexadezimal
*
* u_char:  0 .. 255                        0 .. FF
* char:    -128 .. +127                    80 .. +7F
* u_short: 0 .. 65535                      0 .. FFFF
* short:   -32768 .. +32767                8000 .. +7FFF
* u_long:  0 .. 4294 967295                0 .. FFFF FFFF
* long:    -2147 483648 .. +2147 483647    8000 0000 .. +7FFF FFFF
*
********************************************************************** */

/* ******************************************************************* */
/* KONSTANTEN */
/* ******************************************************************* */
/* ASCII-Steuerzeichen */

#define NUL       (char)   0x00
#define SOH       (char)   0x01
#define STX       (char)   0x02
#define ETX       (char)   0x03
#define EOT       (char)   0x04
#define ENQ       (char)   0x05
#define ACK       (char)   0x06
#define BEL       (char)   0x07
#define BS        (char)   0x08
#define HT        (char)   0x09
#define LF        (char)   0x0A     /* Linefeed */
#define VT        (char)   0x0B
#define FF        (char)   0x0C
#define CR        (char)   0x0D     /* Carrige Return */
#define SO        (char)   0x0E
#define SI        (char)   0x0F
#define DLE       (char)   0x10
#define DC1       (char)   0x11
#define DC2       (char)   0x12
#define DC3       (char)   0x13
#define DC4       (char)   0x14
#define NAK       (char)   0x15
#define SYN       (char)   0x16
#define ETB       (char)   0x17
#define CAN       (char)   0x18
#define EM        (char)   0x19
#define SUB       (char)   0x1A
#define ESC       (char)   0x1B
#define FS        (char)   0x1C
#define GS        (char)   0x1D
#define RS        (char)   0x1E
#define US        (char)   0x1F

#define DEL       (char)   0x7F
/*********************************************************************/
#define NIL       0L    /* NIL-Pointer */

#ifndef FALSE
#define FALSE     (TYbool) 0
#endif

#ifndef TRUE
#define TRUE      (TYbool) 1
#endif

#define  OFF      0  /* Konstanten fuer Bitfelder */
#define  ON       1

#define NO_HDL    -1    /* No Handle */
#define NO_ID     -1    /* No ID */

/*********************************************************************/
/* Macro's */
/*-********************************************************************
* Allgemeine Warnung zu Makros: Sie sollten grundsaetzlich nicht
* geschachtelt werden, um Seiteneffekte auf Variable zu vermeiden.
* Ebenfalls ist von der Anwendung irgendeines Operators auf ein
* Makroargument abzusehen, da hierdurch ungewollt Variablen veraendert
* werden koennen
**********************************************************************/

#define  Abs(x)      ((x)>=0?(x):-(x)) /* math. Absolutwert */

/* x positiv --> Sign = 1; x 0 --> Sign = 0; x negativ --> Sign = -1 */

#define Sign(x)      ((x)>0?1:(x)==0?0:(-1))))     /* Vorzeichen */

/* Folgende Makros koennen Seiteneffekte verursachen */

#define Min(a,b)  ((a)<=(b)?(a):(b))   /* Minimalwert */
#define Max(a,b)  ((a)>(b)?(a):(b)) /* Maximalwert */

/* Dimension eines eindimensionalen Arrays bestimmen */

#define Dim(Array)   (sizeof(Array)/sizeof(Array[0]))

/* Bitmanipulationsmakros, erwarten ein u_char/u_short/u_long, keine  */
/* Bereichspruefung des Bits.                                       */
/* Die Bits sind von 0 in der Wertigkeit aufsteigend.               */

#define BitSet(var,bit)    ((var)|=(bit))    /* Setzen */
#define BitClr(var,bit)    ((var)&=(~(bit)))       /* Loeschen */
#define BitInvert(var,bit) ((var)^=(bit))          /* Toggeln */
#define BitGet(var,bit)    ((var)&(bit)?ON:OFF) /* Abfrage */
/*********************************************************************/
/*DOC

Zur Kennzeichnung werden 4 ASCII-Zeichen verwendet, welche in 4 Bytes
hintereinander abgelegt werden.
Diese 4 Byte koennen auch als 32 Bit-Wert (u_long) interpretiert werden,
sodass auf diese Weise schnelle Vergleiche durchgefuehrt werden
koennen.

DOC*/

typedef u_long TYsymbol;
/**********************************************************************/
/* Parametrisierter Makro, um bequem die folgenden Konstanten */
/* aufbauen zu koennen  */

#define SymbolMake(a,b,c,d)   (TYsymbol)(((a)<<24)|((b)<<16)|((c)<<8)|(d))

/* Long-Konstanten Symbole */

/* Beispiel */

#define TOKN SymbolMake('T','O','K','N')
#define DRVR SymbolMake('D','R','V','R')
/**********************************************************************/
#endif
