#ifdef  IDENT 
static char ident[] = "@(#) fnb.c  VLUGLOTSA   02.05.2012 by SE.TEC"
#endif

/* XP-Probl. loesen  ( tfaktor ) */

/***************************************************************************/
/* Programmname :     fnb.c                                                */
/*-------------------------------------------------------------------------*/
/* Funktion :        Applikationsschnittstelle (API)                       */
/*                   Initialisierung der Schnittstelle und des Geraetes    */
/*-------------------------------------------------------------------------*/
/* Revision : 1.01    Datum : 02.05.2012  erstellt von :  SE.TEC             */
/*-------------------------------------------------------------------------*/
/* Kurzbeschreibung : Hauptprogramm                                        */
/*                                                                         */
/* - Lesen der Schnittstellenparameter                                     */
/* - Senden der Geraete-Initialisierung, Ergebnisbewertung                 */
/*-------------------------------------------------------------------------*/
/* Deklaration: void main(int argc, char *argv[])                          */
/*                                                                         */                                          
/* Eingabe : par1      :  fnb ->                                           */
/* nix fnb00 : Init der Schnittstelle und des Geraetes Return : errcode    */
/* nix fnb02 : Fehlerkorrektur + Errcode                                   */
/* fnb03 : Nullstellen + Errcode                                           */
/* fnb04 : Tarieren + Errcode                                              */
/* fnb05 : Tara loeschen + Errcode                                         */                    
/* fnb06 : Waagenumschaltung                                               */  
/* fnb07 : umgelenkt auf fnb8                                              */ 
/* fnb08 : wiegen MIT Ruhewertung (ohne ist Mist !!) Return : Gewichtswert */
/* fnb11 : Postenregistrierung                                             */
/* fnb23 : Tarieren mit Wertvorgabe                                        */
/*                                                                         */
/* Eingabe : par2 : Dialog-Directory : da steht die Schnittstelleninfo !!  */
/* Eingabe : par3 : (OPTIONAL) Inputwert(Fixformat: VZ+5+.+3) (fnb6:1|2|3) */
/* Ausgabe : (OPTIONAL) outdatei ( mit Errcode + Wert )                    */
/*                                                                         */
/* EXIT Status       : TYerrno err: Fehlerstatus                           */
/*       = RET_OK : kein Fehler                                            */
/*       = 1    : Parameterfehler                                          */
/*       = 2    : sonstiger Fehler -> weiter spezifizierbar                */
/* Aufruf : fnb par1 par2 [ par3 ]                                        */
/*                                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* Aenderungsstand :                                                       */
/* Revision : 1.0  Datum : dd.mm.yyyy  geaendert von :  SE.TEC             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*       STANDARD INCLUDE'S               */

#include "getr_01.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>
#include "paras.h"
#ifdef DLL
#include "fnb.h"
#endif

DCB a_CommDcb ; /* Aktiv */
DCB g_CommDcb ; /* Originalsave */
COMMTIMEOUTS a_CommTmo ;
COMMTIMEOUTS g_CommTmo ;
BOOL fSuccess ;

int testxx=0 ;   /* oder 0 */
int testkk=0 ;   /* oder 0 */
int testschnitt=0  ;
static int schoninit = -1 ;
char wertbuffer [ 120] ;
FILE *fperr ;
char logtext [150] ;
/* externe Deklarationen                 */

extern char dialogdir[] ;
extern int fnb_nr ;
extern int dev_type ;
extern int kommastellen ;
extern int tfaktor ; 
extern int dalibi ; 
extern int dfw_ablauf ; 
extern int dtrenner ;
extern int dtarfakt ; 
extern int dtartime ; 

extern int start_z ;	// 040314
extern int end_z ;	// 040314

extern TYedvConf edv_conf ;
extern TYedvChan edv_chan ;
extern void get_defax (void) ;

#define CRi      0x0d
#define LFi      0x0a


static int schleife = 0  ;

 
unsigned cwag_nr = '1' ;	// gesetzte Waagennummer, default == "1"
static int eCode ;
static int wCode ;


int Auswertung( size_t len ) ;

typedef struct { int kg; int g; } GEWICHT;

static GEWICHT gBrutto;
static GEWICHT gTara;
static GEWICHT gNetto;

static int nWaage = 1;
static int nIdent;
static int dMonat;
static int dTag;


int initschnitt ( void ) ;
int fnb0  ( void )       ;  /* init */
int fnb2  ( void )       ;  /* init */
int fnb3  ( void )       ;  /* nullstellen */
int fnb4  ( void )       ;  /* tarieren */
int fnb5  ( void )       ;  /* tara loeschen */
int fnb6  ( int  )       ;  /* waage umschalten */


int fnb8  ( void )       ;  /* wiegen */
int fnb11 ( void )       ;  /* Postenreg. */
int fnb23 ( double)      ;  /* Tarieren mit Wert */
int fnb33 ( char *);      // Hilfsfunktion zur Eingabe beliebiger Strings

double inpkonv ( char * ) ;

void logschreib ( void ) ;

u_long eigexit ( int ) ;

long nap (long) ;
void sleep ( unsigned );

int leseleer (int) ;
int ergebnis ( double ergebmat[], char *ergebchar[] ) ;

int ergebschreib ; 

char lakeyi [2]  ;
char dialdatin[21] ;

int os_write (HANDLE fd , char *buf , unsigned nbyte ) ;
int os_read  (HANDLE fd , char *buf , unsigned nbyte ) ;

/*-------------------------------------------------------------------------*/

HANDLE schnittstelle;


void printlog( char * inputstring  )
{
    sprintf ( logtext , "%s", inputstring ) ;
    logschreib () ;
}




#ifdef MAINI
void main (int argc , char *argv[] ) 
#else
#ifdef DLL
EXPORT int fnb (int argc, char *argv[])
#else
int fnbi (int argc , char *argv[] )
#endif
#endif
{

  char  * srptr ;
  int strlaenge ;
  int retcode   ;


  int counti ;	/* 210313 */	


  retcode = 0 ;
// Identifikation ist wichtig .......... 
  if ( argc < 2 )
  {
     printf ( " 040314 : Ich bin ein FNB fuer SYSTEC (IT1000)  \n" ) ;
     retcode = eigexit (1) ;

  }

  srptr = getenv("testmode");
  if (srptr == NULL)
  {
     srptr = getenv("testmade");
     if (srptr == NULL)
     {
        testxx = 0;
        testkk = 0;
     }
     else
     {
        testxx = atoi(srptr) ;
        testkk = testxx ;        // atoi(srptr) ;
     }
  }
  else
  {
        testxx = atoi(srptr) ;
        testkk = testxx ;       // atoi(srptr) ;
  }

  if ( testxx > 0 )
  {
     if (testxx == 5 )
     {
        testschnitt = 1 ;
        testkk = 0 ;
        testxx = 0 ;
     }
     else
// testkk muss immer mit 1 anfangen,sonst wird die logfile nicht korrekt geoffnet
     { 
        testxx = 1 ; testkk = 1 ;
     }
  }
  else
  {
     testxx = 0 ; testkk = 0 ;
  }

  if ( argc < 3 )     /* prog, fnb, dialogdir */ 
  { sprintf ( logtext , "Zuwenig Argumente : %d \n" , argc ) ;
    logschreib () ;
    retcode = eigexit ( 1 ) ;
  }
  if (! retcode )
  {
    fnb_nr = -1 ;
    fnb_nr = atoi ( argv[1] ) ;
    sprintf ( logtext , "FNB-Nummer : %d\n" , fnb_nr ) ;
    logschreib () ;
    srptr = argv[2] ;
    strlaenge = strlen ( srptr ) ;
    if ( strlaenge == 0 ) retcode = eigexit ( 3 ) ;
  } 
  if (!retcode)
  {
    if ( strlaenge > 127 ) strlaenge = 127 ; 
    memset ( dialogdir , '\0' , 128 ) ;
    memcpy ( dialogdir , srptr , strlaenge ) ;
    if ( strlaenge < 70 )
    {
      sprintf ( logtext , "%s\n" , dialogdir ) ;
    }
    else
    {
      sprintf ( logtext , "Langer directory-Name \n " ) ;
    }

    logschreib () ;

    strlaenge = initschnitt() ; 
    if ( strlaenge ) retcode = eigexit ( strlaenge ) ;
  } 
  if ( !retcode )
  {
    switch ( fnb_nr )
    { 

//      case 0 :                  /* Initialisierung Schnittstelle + geraet */
//        retcode = eigexit ( fnb0() ) ;  break ;

//      case 2 :                  /* Fehler-reset */
//        retcode = eigexit ( fnb2() ) ; break ;

      case 3 :                  /* Nullstellen */
        retcode = eigexit ( fnb3() ) ; break ;


      case 4 :                  /* Tarieren */
        retcode = eigexit ( fnb4() ) ; break ;

      case 5 :                  /* Tara loeschen */
        retcode = eigexit ( fnb5() ) ; break ;

      case 6 :                  /* Waagen-Umschaltung */
        if ( argc < 4 )
        { 
          sprintf ( logtext , " Keine Waagennummer zum Umschalten\n" );
          logschreib () ;
          retcode = eigexit ( 6 ) ; break ;
        }
        retcode = eigexit ( fnb6 ( atoi(argv[3]) ) ) ; break ;
      case 7 :    /* wird immer auf "mit Ruhewertung" gelegt */
      case 8 :                  /* Wiegen ohne Ruhewertung */
        retcode = eigexit ( fnb8() ) ; break ;

      case 11 :                  /* Postenregistrierung */
/* 21032013 A : 4 Versuche */
#ifdef MAINI
      for ( counti = 0 ; counti < 3 ; counti ++ )
      {
        if ( fnb11() ) 
		retcode = eigexit (0) ;

         sprintf ( logtext , " Counti : %d\n", counti );
         logschreib () ;
 
      }
        retcode = eigexit ( fnb11() ) ; break ;
#else
        retcode = eigexit ( fnb11() ) ; break ;
#endif
/* 21032013 E : 4 Versuche */


      case 23 :
        if ( argc < 4 )
        { 
          sprintf ( logtext , " Kein Wert : ich nehme 0 \n" );
          logschreib () ;
          retcode = eigexit ( fnb5() ) ; break ;
        }
        retcode = eigexit ( fnb23 ( inpkonv(argv[3]) ) ) ; break ;

     case 33 :
        if ( argc < 4 )
        { 
           sprintf ( lakeyi ,"" ) ;
           memset ( dialdatin ,'\0' ,21 ) ;
           sprintf ( logtext , " Kein Wert : ich nehme Leerstring \n" );
           logschreib () ;
           retcode = eigexit ( fnb33 ( NULL) ) ; break ;
        }
        else
        {
           sprintf ( lakeyi , "" ) ;
           memset ( dialdatin ,'\0' ,21 ) ;
           retcode = eigexit ( fnb33 ( argv[3] ) ) ;
        }
        break ;

      default :
           sprintf ( logtext , " Funktion %d nicht realsiert \n", fnb_nr );
           logschreib () ;

		 retcode = eigexit ( (int) 4 ) ; break ; /* Error-return */
    }    /* Ende switch */        
  }     /* retcode */

#ifndef MAINI
 if ( testkk == 2 )
 {
  sprintf ( logtext , "exit mit %d\n" , retcode ) ;
  logschreib() ;
  fclose (fperr) ;
  testkk = 1 ;
 }
 return ( retcode ) ;
#endif

}       /* END main */  

/* ##################################################################### */
/* FNB0 : Initialisierung der Schnittstelle und des Geraetes             */
/* ##################################################################### */

/* ##################################################################### */
/* FNB2 : Initialisierung der Schnittstelle und des Geraetes             */
/* ##################################################################### */

/* ##################################################################### */
/* FNB3 : Nullstellen des Geraetes :                                     */
/* ##################################################################### */
fnb3()
{
 int retanz, reti ;
 char kdostring[15] ; 
 unsigned ch ;


 sprintf ( logtext , "fnb3 gestartet \n" ) ;
 logschreib () ;


  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
  // ch = '<' ;
  ch = start_z ;	// 040314
  kdostring[reti++] = ch ;
  ch = 'S' ;     /* Nullstellen */
  kdostring[reti++] = ch ;
  ch = 'Z' ;     /* Nullstellen */
  kdostring[reti++] = ch ;
  ch = cwag_nr ;    
  kdostring[reti++] = ch ;
//  ch = '>' ;    
  ch = end_z ;	// 040314    
  kdostring[reti++] = ch ;

    retanz = os_write (schnittstelle, kdostring , reti ) ;
    sprintf ( logtext , "Init retanz  : %d ; %s \n" , retanz, kdostring ) ;
    logschreib () ;
    schleife = 3 ;	// max 3 Versuche
    if ( retanz == reti )
    {
         memset ( wertbuffer , '\0' , 120 ) ;
         retanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
         sprintf ( logtext , "%s\n" , wertbuffer ) ;
         logschreib () ;
         return Auswertung (retanz ) ;
 
    }
return ( -1 ) ;

}       /* end fnb3 */

/* ##################################################################### */
/* FNB4 : Tarieren                                                       */
/* ##################################################################### */
fnb4()
{

 int retanz,reti ;
 char kdostring[20] ; 
 unsigned ch ;

   sprintf ( logtext , "fnb4 gestartet \n" ) ;
   logschreib () ;

   memset ( kdostring , '\0' , 20 ) ;


   reti = 0 ;
 // ch = '<' ;
    ch = start_z ;	// 040314
    kdostring[reti++] = ch ;
   ch = 'T' ;
   kdostring[reti++] = ch ;
   ch = 'A' ;
   kdostring[reti++] = ch ;
   ch = cwag_nr ;
   kdostring[reti++] = ch ;
//  ch = '>' ;    
   ch = end_z ;	// 040314    
   kdostring[reti++] = ch ; 
   retanz = os_write (schnittstelle, kdostring , reti ) ; 
   sprintf ( logtext , "retanz : %d ; %s \n" , retanz, kdostring ) ;
   logschreib () ;

   schleife = 3 ;	// max 3 Versuche

   if ( retanz == reti )
   {
         memset ( wertbuffer , '\0' , 120 ) ;
         retanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
         sprintf ( logtext , "%s\n" , wertbuffer ) ;
         logschreib () ;
         return Auswertung (retanz ) ;  

   }
   return ( -1 ) ;
}       /* end fnb4 */

/* ##################################################################### */
/* FNB5 : Tara Loeschen                                                  */
/* ##################################################################### */
fnb5()
{
 int retanz,reti ;
 char kdostring[20] ; 
 unsigned ch ;
  sprintf ( logtext , "fnb5 gestartet \n" ) ;
  logschreib () ;


  memset ( kdostring , '\0' , 20 ) ;
  reti = 0 ;
 // ch = '<' ;
   ch = start_z ;	// 040314
   kdostring[reti++] = ch ;
  ch = 'T'  ; 
  kdostring[reti++] = ch ;
  ch = 'C'  ; 
  kdostring[reti++] = ch ;
  ch = cwag_nr  ; 
  kdostring[reti++] = ch ;
//  ch = '>' ;    
   ch = end_z ;	// 040314    
  kdostring[reti++] = ch ;

  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "fnb5 : retanz : %d ; %s \n" , retanz, kdostring ) ;
  logschreib () ;

    schleife = 3 ;	// max 3 Versuche

   if ( retanz == reti )
   {
         memset ( wertbuffer , '\0' , 120 ) ;
         retanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
         sprintf ( logtext , "%s\n" , wertbuffer ) ;
         logschreib () ;
         return Auswertung (retanz ) ;

   }
	return -1 ;

}       /* end fnb5 */

/* ##################################################################### */
/* FNB6 : Waagenumschaltung                                              */
/* ##################################################################### */
fnb6( int waagennummer ) 
{
  int retanz,reti ;
  char kdostring[15] ; 
  unsigned ch ;

  unsigned chw ;	// 230313 - im folgenden mehrfach benutzt
  char waagdatei[141] ;	// 230313
  FILE *fpw ;		// 230313


  sprintf ( logtext , "fnb6 gestartet : Waage : %d \n", waagennummer ) ;
  logschreib () ;
  memset ( kdostring , '\0' , 15 ) ;
  reti = 0;
 // ch = '<' ;
   ch = start_z ;	// 040314
   kdostring[reti++] = ch ;
  ch = 'S';
  kdostring[reti++] = ch ;
  ch = 'S';
  kdostring[reti++] = ch ;

  chw = 0x31 ;	// Notbremse 	
  if ( waagennummer == 1 ) chw = 0x31 ;
  if ( waagennummer == 2 ) chw = 0x32 ;
  if ( waagennummer == 3 ) chw = 0x33 ;
  if ( waagennummer == 4 ) chw = 0x34 ;
  if ( waagennummer == 5 ) chw = 0x35 ;
  if ( waagennummer == 6 ) chw = 0x36 ;

  kdostring[reti++] = chw ;
//  ch = '>' ;    
  ch = end_z ;	// 040314    
  kdostring[reti++] = ch ;


  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "fnb6 retanz : %d ; %s \n" , retanz, kdostring ) ;
  logschreib () ;

   schleife = 3 ;	// max 3 Versuche

   if ( retanz == reti )
   {
        memset ( wertbuffer , '\0' , 120 ) ;
        retanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
        sprintf ( logtext , "%s\n" , wertbuffer ) ;
        logschreib () ;

// 230313 A

  	memset ( waagdatei , '\0' , 140 ) ;
  	strcpy ( waagdatei , dialogdir ) ; 
  	strcat ( waagdatei , "\\waagnummer" ) ;
  	fpw=fopen ( waagdatei , "w" ) ;
  	if ( fpw == NULL ) 
  	{
		// irgendein toedliches Problem 
 	}
  	else
  	{
		sprintf ( waagdatei, "%c\n" , chw  ) ;
		fprintf ( fpw, waagdatei ) ;
		fclose ( fpw ) ;
  	}
// 230313 E

        return Auswertung (retanz ) ;

   }


  return ( -1 ) ;
}       /* end fnb6 */


/* ##################################################################### */
/* FNB8 : Wiegen (mit Ruhewertung)     -> mal erst ohne Ruhe ....                                  */
/* ##################################################################### */

fnb8()
{
 int retanz,reti ;
 char kdostring[10] ; 
 unsigned ch ;

  sprintf ( logtext , "fnb8 gestartet \n" ) ;
  logschreib () ;

  memset ( kdostring , '\0' , 10 ) ;
  reti = 0 ;
// ch = '<' ;
  ch = start_z ;	// 040314
  kdostring[reti++] = ch ;
  ch = 'R' ;
  kdostring[reti++] = ch ;
  ch = 'M' ;
  kdostring[reti++] = ch ;
  ch = cwag_nr ;
  kdostring[reti++] = ch ;
//  ch = '>' ;    
  ch = end_z ;	// 040314    
  kdostring[reti++] = ch ;

  retanz = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "retanz : %d ; %s \n" , retanz, kdostring ) ;
  logschreib () ;
  schleife = 3 ;	// max 3 Versuche

  if ( retanz == reti )
  {
       memset ( wertbuffer , '\0' , 120 ) ;
       retanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
       sprintf ( logtext , "%s\n" , wertbuffer ) ;
       logschreib () ;
       return Auswertung (retanz ) ; 

   }
   return ( 0 ) ;
}       /* end fnb8 */
/* ##################################################################### */
/* FNB11 : Postenregistrierung                                           */
/* ##################################################################### */
fnb11()
{
 int retanz,reti ;
  char kdostring[15] ; 
  unsigned ch ;
  sprintf ( logtext , "fnb11 gestartet \n" ) ;
  logschreib () ;



  memset ( kdostring , '\0' , 15 ) ;
  reti = 0 ;
// ch = '<' ;
  ch = start_z ;	// 040314
  kdostring[reti++] = ch ;
  ch = 'R'  ;
  kdostring[reti++] = ch ;
  ch = 'N'  ;
  kdostring[reti++] = ch ;
  ch = cwag_nr  ;
  kdostring[reti++] = ch ;
//  ch = '>' ;    
  ch = end_z ;	// 040314    
  kdostring[reti++] = ch ;


   retanz = os_write (schnittstelle, kdostring , reti ) ; 
   sprintf ( logtext , "retanz : %d ; %s \n" , retanz, kdostring ) ;
   logschreib () ;

   schleife = 3 ;	// max 3 Versuche

   if ( retanz == reti )
   {
       memset ( wertbuffer , '\0' , 120 ) ;
       retanz = os_read ( schnittstelle, wertbuffer , 119 ) ;
       sprintf ( logtext , "%s\n" , wertbuffer ) ;
       logschreib () ;
       return Auswertung (retanz ) ;

   }
   return ( 0 ) ;

}       /* end fnb11 */

/* ##################################################################### */
/* FNB23 : Tarieren mit Wertvorgabe                                      */
/* ##################################################################### */


fnb23 ( double targew )
{
int reti ;
char kdostring[25] ;
unsigned char ch,ch1 ;
int x1 , x2 , x3 ;
char kostri[20] ;

  sprintf ( logtext , "fnb23 gestartet \n" ) ;
  logschreib () ;


  sprintf ( logtext , "targew : %lf \n" , targew ) ;
  logschreib () ;

  if ( targew < 0.0 )   targew = 0.0 ;

  if ( targew == 0 )	
	return fnb5() ;
   

  memset ( kdostring , '\0' , 25 ) ;
  sprintf ( kostri , "%04.3lf" , targew ) ;


 
  dtrenner = 2 ;	// immer mit Punkt ...
  reti = 0 ;
  x2 = strlen ( kostri ) ;
 

  // die fuell-Null wird ignoriert, falls precision-Wert angegeben wurde ...
  // 3-nk-stellen passen jetzt immer fuehr-Nullen erzeugen	  

  for ( x3 = 0 ; x3 < x2 ; x3 ++ )
  {

     ch = kostri [ x3 ] ;
     if ( ch == '.' || ch == ',' )
	break ;
  }
  
  x3 = 4 - x3 ;	// konstant 4 stellen vor dem komma 	

// ch1 = '<' ;
  ch1 = start_z ;	// 040314
  kdostring[reti ++ ] = ch1 ;
  ch1 = 'T' ;
  kdostring[reti ++ ] = ch1 ;
  ch1 = 'M' ;
  kdostring[reti ++ ] = ch1 ;
  while ( x3 > 0 )
  {

    ch1 = '0' ;
    kdostring[reti ++ ] = ch1 ;
    x3 -- ;
  }
 
  for ( x1 = 0 ; x1 < x2 ; x1++ )
  {
     ch = kostri [ x1 ] ;
     if ( ch == '.' || ch == ',' )
     {
         if ( dtrenner == 1 || dtrenner == 2 )
         {
            if ( dtrenner == 1 ) ch = ',' ;
            if ( dtrenner == 2 ) ch = '.' ;
            kdostring[reti ++ ] = ch ;
         }
     }
     else
     {
        if ( ch > 0x2f && ch < 0x3a )
        {
             kdostring[reti ++ ] = ch ;
        }
     }
  }     // for-schleife


  ch = cwag_nr ;	// 230313
  kdostring[reti ++ ] = ch ;	// 230313

//  ch = '>' ;    
  ch = end_z ;	// 040314    
  kdostring[reti ++ ] = ch ;
 
  x1 = os_write (schnittstelle, kdostring , reti ) ; 
  sprintf ( logtext , "fnb23 : reti : %d ; %s \n" , x1, kdostring ) ;
  logschreib () ;
 
  schleife = 3 ;	// max 3 Versuche
 
  if ( reti == x1 )
  {
      memset ( wertbuffer , '\0' , 120 ) ;
      x1 = os_read ( schnittstelle, wertbuffer , 119 ) ;
      sprintf ( logtext , "%s\n" , wertbuffer ) ;
      logschreib () ;
      return Auswertung (x1 ) ; 
  }
      return (0) ;         
}    /* end fnb23 */



/* ##################################################################### */
/* FNB33 : Test-Tool : beliebigen String senden und Anrwort leerpollen   */
/* ##################################################################### */
fnb33(char * eingabe )
{

   int retanz ;
   int zeig, quell ;
   char kdostring[100] ; 
   unsigned ch ;
   if ( eingabe == NULL )
   {
       retanz = 0 ;
       sprintf ( logtext , "fnb33 gestartet : Leerstring \n" ) ;
   }
   else
   {
       retanz = strlen ( eingabe ) ;
       sprintf ( logtext , "fnb33 gestartet : %s \n" , eingabe ) ;
   }
   logschreib () ;

   zeig = 0 ;
   quell = 0 ;
   memset ( kdostring , '\0' , 100 ) ;
   retanz = strlen (eingabe) ;
   while ( quell < retanz )
   {
      ch = eingabe[quell++] ;
      if ( ch < 0x20 ) ch = 0x20 ;
      if ( ch > 0x7f ) ch = 0x20 ;
      kdostring[zeig++] = ch ;
   }

   retanz = os_write (schnittstelle, kdostring , zeig ) ; 
   sprintf ( logtext , "retanz 33 : %d , KDO : %s \n" , retanz, kdostring ) ;
   logschreib () ;
   if ( retanz != zeig ) return ( -1 ) ;
    memset ( wertbuffer , '\0' , 120 ) ;
    retanz = os_read ( schnittstelle, wertbuffer , 110 ) ;
    sprintf ( logtext , "retanz 33 : %d : %s \n" , retanz, wertbuffer ) ;
    logschreib () ;

return ( 0 ) ;
}       /* end fnb33 */

initschnitt()
{

// 230313 A

char waagdatei[141] ;
 FILE *fpw ;

  memset ( waagdatei , '\0' , 140 ) ;
  strcpy ( waagdatei , dialogdir ) ; 
  strcat ( waagdatei , "\\waagnummer" ) ;
  fpw=fopen ( waagdatei , "r" ) ;
  if ( fpw == NULL ) 
  {
	// Datei gibbet nicht, die '1' bleibt stehen 
  }
  else
  {
 	if ( fgets ( waagdatei , sizeof(waagdatei) , fpw) != NULL )
	if ( waagdatei[0] == '1' ) 
		cwag_nr = '1' ;
	if ( waagdatei[0] == '2' )
		cwag_nr = '2' ;
	if ( waagdatei[0] == '3' ) 
		cwag_nr = '3' ;
	if ( waagdatei[0] == '4' )
		cwag_nr = '4' ;

	fclose ( fpw ) ;
  }




// 230313 E  

if ( schoninit > 0 ) return ( 0 ) ;

if ( testschnitt )
{
   printf ( "INITIALISIERUNG\n" ) ;
   sprintf ( logtext , "INITIALISIERUNG\n" ) ;
   logschreib();
}
schoninit = 1 ;
edv_conf.baudrate = 9600 ;
edv_conf.databits = 7 ;
edv_conf.parity = EVEN ;
edv_conf.stopbits = 1 ;
edv_conf.flowcontrol = 1 ;
sprintf ( edv_chan.ddId , "COM1" ) ;
kommastellen = 2 ;
get_defax () ;
// 251008 : testxx wird gegbnenfalls modifiziert,
// die lokale Einstellung kann nur  einschalten oder aendern
  if ( testxx > 0 )
  {
     if ( testkk > 0 )  // es ist bereits vorher was gelaufen
     {
        if ( testxx == 5 )
        {
           testxx = 0 ; testschnitt = 1 ;
        }
        else
        {
           testxx = 1; testschnitt = 0 ;     
        }
     }
     else       // erstmalig aktivieren
     {
        testkk = 1 ;
        if ( testxx == 5 )
        {
           testxx = 0 ; testschnitt = 1 ;
        }
        else
        {
           testxx = 1 ; testschnitt = 0 ;     
        }
     }
  }
  else
  {
     if (( testkk > 0 ) && (testschnitt == 0 ))
        testxx = 1 ;    // Ausgangszustand herstellen
  }
// 251008 E

sprintf ( logtext , "Vor CreateFile \n" ) ;
logschreib () ;
    schnittstelle = CreateFile ( (LPCTSTR) edv_chan.ddId ,
                        GENERIC_READ | GENERIC_WRITE ,
                        0 , /* prevents the file from being shared */
                        NULL ,  /* no security attributes */
                        OPEN_EXISTING , /* comm devices must use "OPEN_EXISTING */
                        0 , /* not overlapped I/O */
                        NULL /* hTemplate must be NULL for comm devices */
                        ) ;
    if ( schnittstelle == INVALID_HANDLE_VALUE ) 
    return ( (int) 12 ) ;

    memset (( void *) &a_CommDcb, '\0', sizeof (a_CommDcb)) ;
    memset (( void *) &a_CommTmo, '\0', sizeof (a_CommTmo)) ;
    sprintf ( logtext , "Vor GetCommState \n" ) ;
    logschreib () ;
    fSuccess = GetCommState(schnittstelle, &g_CommDcb) ;
    if ( fSuccess )
    {  
        a_CommDcb = g_CommDcb ;
    }
    else
    {
        return ( (int) 13 ) ;
    }
    sprintf ( logtext , "Vor GetTimeouts \n" ) ;
    logschreib () ;
    fSuccess = GetCommTimeouts(schnittstelle, &g_CommTmo) ;
    if ( fSuccess )
    {  
        a_CommTmo = g_CommTmo ;
    }
    else
    {
        return ( (int) 14 ) ;
    }
    if ( edv_conf.flowcontrol > 1 )
    {   /* flow control on */;  }
    else
    {   /* flow control off */
        /* Monitoring CTS signal */
        a_CommDcb.fOutxCtsFlow = FALSE ;
        
        /* Monitoring DSR signal */
        a_CommDcb.fOutxDsrFlow = FALSE ;

        /* DTR flow control */  // #221100 : enable->disable
        a_CommDcb.fDtrControl = DTR_CONTROL_DISABLE;

        /* DSR signal sensitivity */
        a_CommDcb.fDsrSensitivity = FALSE ;

        /* RTS flow control */
        a_CommDcb.fRtsControl = RTS_CONTROL_DISABLE ;
    }
    a_CommDcb.BaudRate = (DWORD) edv_conf.baudrate ;
    a_CommDcb.ByteSize = (BYTE) edv_conf.databits ;
    switch ( edv_conf.parity ) 
    {
        case NONE: a_CommDcb.Parity = NOPARITY ;
                break ;
        case EVEN: a_CommDcb.Parity = EVENPARITY ;
                break ;
        case ODD: a_CommDcb.Parity = ODDPARITY ; 
                break ;
        default : a_CommDcb.Parity = NOPARITY ; 
     }    
     switch ( edv_conf.stopbits )
     {
        case 1 : a_CommDcb.StopBits = ONESTOPBIT ;
                break ;
        case 2 : a_CommDcb.StopBits = TWOSTOPBITS ;
                break ;
        default : a_CommDcb.StopBits = ONESTOPBIT ;
     }
     fSuccess = SetCommState ( schnittstelle, &a_CommDcb ) ;
     if ( !fSuccess )  { return ( (int) 14 ) ; }
/* RITO : max. Pause zw. 2 Zeichen -> Endekenn.(9600 ca. 1 millsek/byte)  */  
/* RTTM und RTTC kooperieren */
/* Produkt = maximale Transfer-Time, dann wird (evtl.teilweise) geliefert */
/* RTTM : erweitert TO durch : RTTM * Anzahl Zeichen */
/* RTTC :erweitert TO durch : ( RTTM * AZ ) + RTTC   */
/* das sind gerade die Werte fuer 9600 */               /* 081008 : tfaktor dazu */
     a_CommTmo.ReadIntervalTimeout = 30 * tfaktor ;         /* 30 millisec -> Endekenn !! */
     a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;          /* x 2 ( Zeit je Byte+x */ 
     a_CommTmo.ReadTotalTimeoutConstant = 30000 ; /* 30 sek warten */ /* 30 millisec (RITO) */ 
     switch ( edv_conf.baudrate )
     {
     case 2400 :
     a_CommTmo.ReadIntervalTimeout = 50 * tfaktor ;         /* 50 msec -> Endekenn !! 081008 */
     a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;          /* x 2 ( Zeit je Byte+x */ 
           break ;
     case 4800 :
     a_CommTmo.ReadIntervalTimeout = 40 * tfaktor ;         /* 40 msec -> Endekenn !! 081008 */
     a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;    /* x 2 ( Zeit je Byte+x */ 
           break ;
     }
     a_CommTmo.WriteTotalTimeoutMultiplier = 100 ;       /* x 100 */
     a_CommTmo.WriteTotalTimeoutConstant = 100;          /* 100 millisec */                    

     fSuccess = SetCommTimeouts ( schnittstelle, &a_CommTmo ) ;
     if ( ! fSuccess ) return ( (int) 15 ) ;
     return ( leseleer (1) ) ;
}
/* #################################################################### */
/*                      Schreiben des Puffers                           */
/* #################################################################### */

int os_write (HANDLE fd , char *buf , unsigned nNumberOfBytesToWrite) 
{
DWORD   NumberOfBytesWritten ;
BOOL rc ;       /* returncode */
char hilfbuf [120] ;    // 220699 
  rc = WriteFile ( fd , (LPVOID) buf, (DWORD) nNumberOfBytesToWrite,
       (LPDWORD) &NumberOfBytesWritten , NULL ) ; 
  if (rc == FALSE)     
  {     sprintf ( logtext , "Fehler beim WriteFile\n" ) ; }
  if ( testschnitt )
  {

           memset ( hilfbuf , '\0' , 120 ) ;
           if ( NumberOfBytesWritten > 118 )
                memcpy ( hilfbuf , buf , 118 ) ;
           else
               memcpy ( hilfbuf , buf , NumberOfBytesWritten ) ;

               printf ( "WW %3.0d %s \n" , NumberOfBytesWritten , hilfbuf ) ;
               sprintf ( logtext, "WW %3.0d %s \n" , NumberOfBytesWritten , hilfbuf ) ;
               logschreib () ; 

  }

  return ( (int) NumberOfBytesWritten) ;
}

/* #################################################################### */
/*                        Lesen des Puffers                             */
/* #################################################################### */

int os_read (HANDLE fd , char *buf , unsigned nNumberOfBytesToRead)
{
DWORD NumberOfBytesRead ;
BOOL rc ;     /* returncode */
char hilfbuf [145] ;    // 251008 120->145
   rc = ReadFile ( fd , (LPVOID) buf , (DWORD) nNumberOfBytesToRead,
        (LPDWORD) &NumberOfBytesRead , NULL ) ;
   if (rc == FALSE )
   { sprintf ( logtext , "Fehler beim ReadFile \n" ) ; }
   if ( testschnitt )
   {
      if ( rc == FALSE )
      {
        printf ( "RR  -1 Fehler Lese-Zugriff \n" ) ;
        sprintf ( logtext , "RR  -1 Fehler Lese-Zugriff \n" ) ;
        logschreib () ;
      }
      else
      {
        if ( NumberOfBytesRead  == 0L )
        {
             printf ( "RR  00 NIX gelesen \n" ) ;
             sprintf ( logtext ,"RR  00 NIX gelesen \n" ) ;
             logschreib () ;
        }
        else
        {
           memset ( hilfbuf , '\0' , 145 ) ;
           if ( NumberOfBytesRead > 118 )
                memcpy ( hilfbuf , buf , 138 ) ;
           else
                memcpy ( hilfbuf , buf , NumberOfBytesRead ) ;
           printf ( "RR %3.0d %s \n" , NumberOfBytesRead , hilfbuf ) ;
           sprintf ( logtext , "RR %3.0d %s \n" , NumberOfBytesRead , hilfbuf ) ;
           logschreib () ;

        }
      }
   }
   return ( (int) NumberOfBytesRead ) ; 
}   

/* Alte Variante 

void logschreib ( void )
{   
char erfnam [20] ;
if ( testxx ) printf ( "%s\n" , logtext ) ;
if ( !testkk ) return ;
if ( testkk == 1 )
 {
 sprintf ( erfnam , "logfile" ) ;
 if ( ( fperr = fopen(erfnam, "at" )) == NULL ) return ;   // APPEND_TEXT
 } ;
 testkk = 2 ;
 fprintf ( fperr , logtext ) ;
}
< ---------- */

// 251008 : neue Variante

void logschreib ( void )
{   
char erfnam [20] ;
char writepuffer [200] ;

char timestr[100] ; 
if ( testxx ) printf ( "%s : %s\n" , _strtime(timestr),logtext ) ; 
if ( !testkk ) return ;
if ( testkk == 1 )
 {
 sprintf ( erfnam , "logfile" ) ;
 if ( ( fperr = fopen(erfnam, "at" )) == NULL ) return ;   // APPEND_TEXT 
 } ;
 testkk = 2 ;
 sprintf ( writepuffer, "%s : %s", _strtime(timestr),logtext ) ; 
 fprintf ( fperr , writepuffer ) ;
}

u_long eigexit ( int para  )
{
 if ( testkk == 2 )
 {
  sprintf ( logtext , "exit mit %d\n" , para ) ;
  logschreib() ;
  fclose (fperr) ;
 }

// unnoetig : fSuccess = SetCommState ( schnittstelle, &g_CommDcb ) ;
// unnoetig : fSuccess = SetCommTimeouts ( schnittstelle, &g_CommTmo ) ;

#ifdef MAINI
  exit ( (u_long) para ) ;
#else
  return ( (int) para ) ;
#endif
}

double inpkonv ( char *argl )
{
 int i, k ;
 k = strlen ( argl ) ;
 for ( i = 0 ; i < k ; i ++ )
 { if ( argl[i] == ',' ) argl[i] = '.' ; } 
 
  sprintf ( logtext , "Para3 : %s\n" , argl ) ;
  logschreib() ;
  return ( atof ( argl ) ) ; 
} 



/* nap und sleep gibbet wohl nicht unter win */
long nap ( long n ) 
{ 
  Sleep ((DWORD) n );
  return ( (long) 0);
}

void sleep (unsigned secund ) 
{ 
 long ret=0L ;
 ret= nap( 1000*secund) ;
 return ;
}

int leseleer (int wartemillis )
{
int retanz ;
/*  Endekennung 2400 ca. 4 millsek/byte)  reagiert werden */
    a_CommTmo.ReadIntervalTimeout = wartemillis;
    a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;
    a_CommTmo.ReadTotalTimeoutConstant = wartemillis + 1 ;
    a_CommTmo.WriteTotalTimeoutMultiplier = 100 ;       /* x 100 */ 
    a_CommTmo.WriteTotalTimeoutConstant = 100;          /* 100 millisec */                    
    fSuccess = SetCommTimeouts ( schnittstelle, &a_CommTmo ) ;
    if ( ! fSuccess ) return ( (int) 18 ) ;

    memset ( wertbuffer , '\0' , 100 ) ;
    retanz = os_read ( schnittstelle, wertbuffer , 95 ) ;
    sprintf ( logtext , "leseleer : WEPU : %s\n", wertbuffer ) ;
    logschreib () ;

    /* Das sind die Werte fuer 9600 */                  /* 081008 tfaktor */
    a_CommTmo.ReadIntervalTimeout = 30 * tfaktor ;         /* 30 millisec -> Endekenn !!*/ 
    a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;          /* x 2 ( Zeit je Byte+x */ 
    a_CommTmo.ReadTotalTimeoutConstant = 20000 ; /* 20 sek warten */ /* 30 millisec (RITO) */ 
// Uebersteuert die 30 sek aus initschnitt !!
    a_CommTmo.WriteTotalTimeoutMultiplier = 100 ;       /* x 100 */
    a_CommTmo.WriteTotalTimeoutConstant = 100;          /* 100 millisec */                    
    switch ( edv_conf.baudrate )
    {
        case 2400 :
            a_CommTmo.ReadIntervalTimeout = 50 * tfaktor ;         /* 50 millisec -> Endekenn !! 081008 */
            a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;          /* x 2 ( Zeit je Byte+x */ 
                break ;
        case 4800 :
            a_CommTmo.ReadIntervalTimeout = 40 * tfaktor ;         /* 40 millisec -> Endekenn !! 081008 */
            a_CommTmo.ReadTotalTimeoutMultiplier = 2 ;          /* x 2 ( Zeit je Byte+x */ 
                break ;
    }
    fSuccess = SetCommTimeouts ( schnittstelle, &a_CommTmo ) ;
    if ( ! fSuccess ) return ( (int) 19 ) ;
    return ( 0 ) ; 
}


void PrintErrorcode( int eCode )
{
	printlog( "Errorcode: " );
	switch( eCode )
	{
	case 00: printlog( "00: Kein Fehler\n" ); break;
	case 11: printlog( "11: Allgemeiner Waagenfehler (z.B. Verbindung zur Lastzelle gest�rt)\n" ); break;
	case 12: printlog( "12: Waage in �berlast (Gewicht �berschreitet den maximalen W�gebereich)\n" ); break;
	case 13: printlog( "13: Waage in Bewegung (nach 10 Sek. keine Ruhe)\n" ); break;
	case 14: printlog( "14: Waage nicht verf�gbar (z.B. nur eine Waage kalibriert)\n" ); break;
	case 15: printlog( "15: Tarierungsfehler (z.B. Taragewichts-Formatierung falsch)\n" ); break;
	case 16: printlog( "16: Gewichts-Drucker nicht bereit (offline)\n" ); break;
	case 17: printlog( "17: Druckmuster enth�lt ung�ltiges Kommando\n" ); break;
	case 18: printlog( "18: Nullstellfehler (au�erhalb Nullstellbereich oder Waage in Bewegung)\n" ); break;
	case 31: printlog( "31: �bertragungsfehler (z.B. Datensatz zu lang oder Time-out)\n" ); break;
	case 32: printlog( "32: Ung�ltiger Befehl\n" ); break;
	case 33: printlog( "33: Ung�ltiger Parameter\n" ); break;
	default:
     		sprintf ( logtext ,"%02d: Unbekannter Errorcode\n", eCode ); 
 			logschreib () ; break ;
	}
}

void PrintWaagenstatus( int wCode )
{
	printlog( "Waagenstatus: " );
	switch( wCode )
	{
	case 0: printlog( "00: in Ruhe, Brutto > 0\n" ); break;
	case 1: printlog( "10: in Bewegung, Brutto > 0\n" ); break;
	case 2: printlog( "01: in Ruhe, Brutto < 0\n" ); break;
	case 3: printlog( "11: in Bewegung, Brutto < 0\n" ); break;
	default: 
    		sprintf ( logtext ,"%02d: Unbekannter Waagenzustand\n", wCode );
 			logschreib () ; break ;
	}
}

int GetNumber( char * szNumber, size_t len )
{
	char szN[10];
    memcpy( szN, szNumber, len );
	szN[len] = '\0';
	return atoi( szN );
}

GEWICHT GetGewicht( char *szGew, size_t len )
{
	GEWICHT g;
	char szG[21];
	char *p;

    memcpy( szG, szGew, len );
	szG[len] = '\0';
    g.kg = atoi( szG );
	g.g = 0;
	p = strchr( szG, ',' );
	if ( p == NULL )
	{
		p = strchr( szG, '.' );
	}
	if ( p == NULL )
	{
		return g;
	}
	if ( strlen(p) < 2 )
	{
		return g;
	}
	p++;
	switch ( strlen(p) )
	{
	case 1: g.g = 100 * atoi(p); break;
	case 2: g.g = 10 * atoi(p); break;
	case 3: g.g = atoi(p); break;
	}
	sprintf ( logtext , "Gewichtswertformatierung: %s --> %9d.%03d\n", szG, g.kg, g.g );
 	logschreib( ) ;
	return g;

}

void WriteErgebnis( void )
{
	FILE *fp;
        char ergebnam[300] ;	
 
 	strcpy ( ergebnam, dialogdir  ) ;
      	strcat ( ergebnam , "\\ergebnis" ) ;

	sprintf ( logtext, "B%9d.%03d\n", gBrutto.kg, gBrutto.g );
	logschreib();	
	sprintf ( logtext, "T%9d.%03d\n", gTara.kg, gTara.g );
	logschreib();	
	sprintf ( logtext, "N%9d.%03d\n", gNetto.kg, gNetto.g );
	logschreib();	
	sprintf ( logtext, "E     %02d%02d%04d\n", dMonat, dTag, nIdent );
	logschreib();	

 	remove ( ergebnam ) ;  
 
	fp = fopen( ergebnam, "w" ) ;

	if (fp == NULL)
	{
		sprintf ( logtext ,"Fehler bei fopen(%s , ""w""\n", ergebnam );
		logschreib () ;
	    return;
	}
	fprintf( fp, "B%9d.%03d\n", gBrutto.kg, gBrutto.g );
	fprintf( fp, "T%9d.%03d\n", gTara.kg, gTara.g );
	fprintf( fp, "N%9d.%03d\n", gNetto.kg, gNetto.g );
	fprintf( fp, "E     %02d%02d%04d\n", dMonat, dTag, nIdent );
	fclose( fp );
}




#define ACKi     0x06
#define NAKi     0x15



void acksend (void)
{
char acck [2] ;
int retanz ;
// return ;
sprintf ( logtext , "acksend \n" ) ;
logschreib () ;
acck[0] = ACKi /* 0x06 */ ;
acck[1] = '\0' ;
retanz = os_write (schnittstelle, acck , 1 ) ; 
}

void naksend (void)
{
char nak [2] ;
int retanz ;
// return ;
sprintf ( logtext , "naksend \n" ) ;
logschreib () ;
nak[0] = NAKi /* 0x06 */ ;
nak[1] = '\0' ;
retanz = os_write (schnittstelle, nak , 1 ) ; 
}

int Auswertung( size_t len )
{


// Basis-Problem : "\n" ist nur 1 lang und nicht 2 ( like 0a0d )

	acksend () ;

	sprintf ( logtext , "Laenge der Quittung: %d\n", len );
	logschreib () ;
//XXX	if ( len < strlen( "<nn>\n" ) )
	if ( len < 6 )
	{
		printlog( "Seltsame Quittung erhalten: Zu kurz.\n" );
            return 0;
	}
//	if ( wertbuffer[0] != '<' )	// 040314
	if ( wertbuffer[0] != start_z )
	{
		printlog( "Seltsame Quittung erhalten: Beginnt nicht mit '<'.\n" );
        return 0;
	}
//xxx	if ( wertbuffer[len-2] != '>' )
//	if ( wertbuffer[len-3] != '>' )	// 040314
	if ( wertbuffer[len-3] != end_z )
	{
		printlog( "Seltsame Quittung erhalten: Endet nicht mit '>'.\n" );
        return 0;
	}
    eCode = GetNumber( wertbuffer + strlen("<"), strlen("EE") );
	PrintErrorcode( eCode );
	if ( eCode != 0 )
	{
		return 0;
	}
	if ( len < strlen("<EEWWTT.MM.JJHH:MMIIIIWBBBBBBBBTTTTTTTTNNNNNNNNEETTBTTTCCCCCCCC>\n") )
	{
        return 1; // Kurze Quittung nur mit Status
	}

    wCode = GetNumber( wertbuffer + strlen("<EE"), strlen("WW") );
	PrintWaagenstatus( wCode );
    
	gBrutto = GetGewicht( wertbuffer + strlen("<EEWWTT.MM.JJHH:MMIIIIW"), strlen("BBBBBBBB") );
        gTara = GetGewicht( wertbuffer + strlen("<EEWWTT.MM.JJHH:MMIIIIWBBBBBBBB"), strlen("TTTTTTTT") );
        gNetto = GetGewicht( wertbuffer + strlen("<EEWWTT.MM.JJHH:MMIIIIWBBBBBBBBTTTTTTTT"), strlen("NNNNNNNN") );
        nIdent = GetNumber( wertbuffer + strlen("<EEWWTT.MM.JJHH:MM"), strlen("IIII") );
        dMonat = GetNumber( wertbuffer + strlen("<EEWWTT."), strlen("MM") );
        dTag = GetNumber( wertbuffer + strlen("<EEWW"), strlen("TT") );
	WriteErgebnis();
	return 1;
}