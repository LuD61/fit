#ifndef _FNB_DEF
#define _FNB_DEF

#ifdef _CPP
#define EXPORT extern "C" _declspec (dllexport)
#else
#define EXPORT _declspec (dllexport)
#endif

EXPORT int  fnb (int argc, char *argv[]);

#endif

