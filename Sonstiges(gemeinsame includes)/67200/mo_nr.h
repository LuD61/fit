#ifndef _MO_NRDEF
#define _MO_NRDEF

class AutoNrClass
{
public :
        AutoNrClass ()
        {
        }
        long GetNr   (long);
        long GetNrLs (long);
        long GetNrBest (long);
};
#endif
