#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "strfkt.h"
#include "wmaskc.h"
#include "mo_auto.h"
#include "auto_nr.h"

AUTO_NR_CLASS AutoNrClass;


int AUTO_CLASS::nvholid (short mdn, short fil, char *nr_nam)
/**
Naechste freie Numnmer holen.
**/
{

             memcpy (&auto_nr, &auto_nr_null, sizeof (struct AUTO_NR));
             auto_nr.mdn = mdn;
             auto_nr.fil = fil;
             strcpy (auto_nr.nr_nam, nr_nam);
             clipped (auto_nr.nr_nam); 
             dsqlstatus = AutoNrClass.dbreadfirst_1 ();
		     if (dsqlstatus < 0) return dsqlstatus;

             if (dsqlstatus == 100)
             {
                          dsqlstatus = AutoNrClass.dbreadfirst_2 ();
						  if (dsqlstatus) return dsqlstatus;
             }
             if (auto_nr.satz_kng == 1)
             {
                          AutoNrClass.dbdelete ();
             }
             else
             {
                          AutoNrClass.dbdelete ();
				          if (auto_nr.nr_nr >= auto_nr.max_wert) return -1;
                          auto_nr.nr_nr += (long) 1;
                          AutoNrClass.dbupdate_2 ();
                          auto_nr.nr_nr -= (long) 1;
             }
             return dsqlstatus;
}
            
int AUTO_CLASS::nvanmprf (short mdn, short fil, char *nr_nam, long nr_nr,
                          long max_wert, long nr_char_lng, char *fest_teil)
/**
Neu Nummer generieren.
**/
{
             auto_nr.mdn      = mdn;             
             auto_nr.fil      = fil;             
             strcpy (auto_nr.nr_nam, nr_nam);             
             dsqlstatus = AutoNrClass.dbdelete_nr_nam ();
             auto_nr.nr_nr    = nr_nr;             
             auto_nr.max_wert = max_wert;             
             auto_nr.nr_char_lng  = nr_char_lng;             
             sprintf (auto_nr.nr_char, "%ld", nr_char_lng);
             strcpy (auto_nr.fest_teil, fest_teil);
             strcpy (auto_nr.nr_komb, " ");
             auto_nr.delstatus = 0;
             auto_nr.satz_kng = 2;
             dsqlstatus = AutoNrClass.dbupdate ();
             return dsqlstatus;
}

int AUTO_CLASS::nveinid (short mdn, short fil, char *nr_nam, long nr_nr)
/**
Nummer freigeben.
**/
{
             auto_nr.mdn      = mdn;             
             auto_nr.fil      = fil;             
             strcpy (auto_nr.nr_nam, nr_nam);             
           
             dsqlstatus = AutoNrClass.dbreadfirst_0 ();
             auto_nr.nr_nr    = nr_nr;             
             auto_nr.satz_kng = 1;
             dsqlstatus = AutoNrClass.dbupdate ();
             return dsqlstatus;
}  


