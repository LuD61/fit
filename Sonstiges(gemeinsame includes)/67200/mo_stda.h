#ifndef _MO_STDA_DEF
#define _MO_STDA_DEF

class StndAuf {
         private :
             HANDLE hMainInst;
			 int SortMode;
         public :
			 StndAuf () : SortMode (0)
            {
            }
 
            void SetSortMode (int mode)
			{
				SortMode = max (0, mode);
			}
            int StdAuftrag (HWND, short, short, long, short);
            double GetStda (void);
            double GetStdme (void);
            double GetStdpr_vk (void);
            BOOL GetStdRow (int, double *, double *, double *);
};
#endif