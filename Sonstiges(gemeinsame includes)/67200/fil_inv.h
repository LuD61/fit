#ifndef _FIL_INV_DEF
#define _FIL_INV_DEF

#include "dbclass.h"

struct FIL_INV {
   short     delstatus;
   double    a;
   long      dat;
   short     fil;
   short     mdn;
   double    me;
   short     me_einh;
   double    ums_ek_bew;
   double    ums_vk_bew;
   long      posi;
   long      rowid;
};
extern struct FIL_INV fil_inv, fil_inv_null;

#line 7 "fil_inv.rh"

class FIL_INV_CLASS : public DB_CLASS
{
       private :
               int cursor_o;
               int del_best_curs;
               void prepare (void);
               void prepare_o (char *);
       public :
               FIL_INV_CLASS () : DB_CLASS (), cursor_o (-1), del_best_curs (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_o (char *);
               int dbread_o (void);
               int dbdeletebest (void);
               int dbclose (void);
               int dbclose_o (void);
               int dbinsert (void);

};
#endif
