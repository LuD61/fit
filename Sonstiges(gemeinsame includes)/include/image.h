#ifndef _IMAGE_DEF
#define _IMAGE_DEF
#include "mo_draw.h"

class IMG
{
     private :
        BMAP bmp;
        BMAP msk;       
        BMAP sel;
        BMAP selmsk;
        HWND hWnd;
      public :
        IMG (HWND, char *, char *);
        IMG (HWND, HBITMAP, HBITMAP);
        void Print (HWND, int, int);
        static char *getImageNr (char *, int);
     
};
#endif              