#ifndef _VPEN_DEF
#define _VPEN_DEF
#include "vector.h"
#include "RPen.h"

class VPen : public CVector
{
private :
//	HPEN hPen;
public :
	VPen ();
	~VPen ();
	BOOL Exist (RPen*);
	BOOL CanDestroy (HPEN);
};
#endif