#ifndef _WMESS_DEF
#define _WMESS_DEF
#include "wmaskc.h"

class WMESS 
{
           private :
			    mfont stdfont;
                char  **warten;
				mfont **wartenfont;
           public :
			   WMESS () : warten (NULL), wartenfont (NULL)
			   {
				   stdfont.FontName       = "Times New Roman";
				   stdfont.FontHeight     = 150;
				   stdfont.FontWidth      = 0;
				   stdfont.FontAttribute  = 0;
				   stdfont.FontColor      = BLACKCOL;
				   stdfont.FontBkColor    = LTGRAYCOL;
				   stdfont.PosMode        = 0;
				   stdfont.hFont          = 0;

			   }
               void Message (HANDLE, HWND, char *);
               void Free (void);
               void Destroy (void);
};

#endif
