     
#define MAX_VARS 1000

typedef struct
     {
         short  preped;             /* Kennzeichen, ob schon benutzt     */
         char   cutyp;              /* Cursor-Typ U = update I - insert  */
         struct sqlda *q_desc;      /* Zeiger auf sqlda-Struktur         */
         char  *sqlwerte;           /* Bereich fuer Werte aus select     */
         short *vind;               /* Bereich fuer Zeiger aud SQLWerte  */
         char  *vformat;            /* Bereich fuer Formate zu sqlwerte  */
         int (*p_curs) (char *, ...); 
                                         /* Cursor vorbereiten           */
         int (*o_curs) (short);          /* Cursor oeffnen               */
         int (*f_curs) (short,int, ...); /* Cursor lesen                 */
         int (*c_curs) (short);          /* Cursor schliessen            */
         int (*e_curs) (short);          /* Cursor ausfuehren            */
     } CURSOR;


typedef struct
      {
         short quest;
         struct sqlda *quest_desc;
      } OP_CURS;

typedef struct
      {
         short typ;
         short len;
         char *var;
      } SVAR;
       

typedef struct
    {
         char name[20];                /* Name eines Feldes                 */
         char format[10];              /* Format eines Feldes               */
    } FORMAT;
