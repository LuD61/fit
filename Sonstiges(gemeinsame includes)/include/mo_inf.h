#ifndef _LIZENZ_DEF
#define _LIZENZ_DEF
#include <windows.h>
#include "cmask.h"
#include "dbclass.h"


class KINFO : public DB_CLASS
{
          private :
			   MESSCLASS Msg;
			   int lines;
          public :
			  KINFO () : DB_CLASS (), lines (5)
               {
               }
               void InfoKun (HANDLE, HWND, short, long);
               void InfoKunF (HANDLE, HWND, short, long, char *);
};
#endif
