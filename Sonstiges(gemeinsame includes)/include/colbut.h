#ifndef _COLBUT_DEF
#define _COLBUT_DEF
#include "wmaskc.h"

#define NOCOLPRESS 17
#define ACTCOLPRESS 18
#define NOCOLINACTIVE 64
#define NOCOLBORDER 128

void InitColDef (void);
void EnableColDef (BOOL);
BOOL IsEnabledColDef (void);
void SetColBorderM (BOOL);
HWND CreateColButton (HWND, ColButton *, int, int, int, int, HFONT, HINSTANCE, DWORD);
void ActivateCBNoBorder (ColButton *, BOOL);
#endif
