#define STRING 0
#define SHORT  1
#define LONG   2
#define DOUBLE 4
#define STRUKT 10

union FADR
        { char *cfeld;
          short *sfeld;
          long *ifeld;
          double *dfeld;
        };

/* Keyzusammensetzung                         */

typedef struct
        { short feldbeg [5];               /*  Offset im Satz      */
          short feldlen [5];               /*  Laenge des Feldes   */
          short feldtyp [5];               /*  Feldtyp eines Feldes */
          short keysize;                   /*  Laenge eines Keyelemets */
        } KEYS;

/* Keyaufbau                                   */

typedef struct
        { short keylen;
          long doffs;
          char keyflag;
          char keyfeld[1];
        } KEY;

/* Informationen ueber KEY                 */

struct KEYINFO
        {
             short reclen;
             short keyanz;
             KEYS keys [10];
             long keyoffs[10];
             char keyname[20] [10];
             short zwsize;
             short gensize;
        };

void set_compall (int);
void unset_compall (void);
int setiops (char[]);
int setiop (char);
int set_idxmod (int);
int get_zwsize (void);
int get_gensize (void);
int set_zwsize (int);
int set_gensize (int);
int fil_keyinfo (short, short, KEYS *, short);
int idxname (short, char *);
FILE *createkey (char *);
FILE *openkey (char *);
int selectkey (FILE *);
int closekey (FILE *);
int insert_idx (short inr, long recoffs, char *satz);
void getkeys (KEYS *, int);
int fetch_first (short, long *, char *);
int fetch_min (short, long *, char *);
int fetch_max (short, long *, char *);
int fetch_next (short, long *);
int fetch_next_idx (short, long *);
int fetch_prior_idx (short, long *);
int key_gen (short, FILE *);
