#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <windows.h>
#include <richedit.h>
#include <richole.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "strfkt.h"
#include "cmask.h"
#include "lbox.h"
#include "colbut.h"
#include "rdonly.h"
#include "inflib.h"

static DWORD BuOk = 900;
static jrhstart = 70;
static jrh1 = 1900;
static jrh2 = 2000;


static BOOL IsMon31 (int mon)
/**
Monat im Datum pruefen.
**/
{
         static int mon31[] = {1,3,5,7,8,10,12};
         int i;

         for (i = 0; i < 7; i ++)
         {
                      if (mon == mon31[i]) return TRUE;
         }
         return FALSE;
}

static  IsMon30 (int mon)
/**
Monat im Datum pruefen.
**/
{
         static int mon30[] = {4,6,9,11};
         int i;

         for (i = 0; i < 4; i ++)
         {
                      if (mon == mon30[i]) return TRUE;
         }
         return FALSE;
}

static  IsMon29 (int mon, int jr)
/**
Monat im Datum pruefen.
**/
{
         static int sjr = 1954;
         int jrdiff; 

         if (mon != 2) return FALSE; 

         jrdiff = jr - sjr;
         if (jr % 4 == 0) return TRUE; 
         return FALSE;
}
 

static  IsMon28 (int mon, int jr)
/**
Monat im Datum pruefen.
**/
{
         static int sjr = 1954;
         int jrdiff; 

         if (mon != 2) return FALSE; 

         jrdiff = jr - sjr;
         if (jr % 4) return TRUE; 
         return FALSE;
}

void BORDER::PrintHLine (HWND MainWindow, HDC hdc, int x, int y, int cx)
{
      HPEN hPenG;
      HPEN oldPen;

      hPenG = CreatePen (PS_SOLID, 0, Col);

      oldPen = SelectObject (hdc, hPenG);

      if (cx > 0)
      {
          MoveToEx (hdc, x,y, NULL);
          LineTo (hdc, x + cx, y);
      }


      SelectObject (hdc, oldPen);
      DeleteObject (hPenG);
}


void BORDER::PrintRaisedHLine (HWND MainWindow, HDC hdc, int x, int y, int cx)
{
      HPEN hPenG;
      HPEN hPenW;
      HPEN oldPen;

      hPenG = CreatePen (PS_SOLID, 0, Col);
      hPenW = CreatePen (PS_SOLID, 0, HiCol);

      oldPen = SelectObject (hdc, hPenG);
      
      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x + cx, y);


      SelectObject (hdc, hPenW);

      y ++;
      x ++;
      cx --;

      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x + cx, y);

      SelectObject (hdc, oldPen);
      DeleteObject (hPenG);
      DeleteObject (hPenW);
}


void BORDER::PrintVLine (HWND MainWindow, HDC hdc, int x, int y, int cy)
{
      HPEN hPenG;
      HPEN oldPen;

      hPenG = CreatePen (PS_SOLID, 0, Col);

      oldPen = SelectObject (hdc, hPenG);


      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x, y + cy);
 

      SelectObject (hdc, oldPen);
      DeleteObject (hPenG);
}

void BORDER::PrintRaisedVLine (HWND MainWindow, HDC hdc, int x, int y, int cy)
{
      HPEN hPenG;
      HPEN hPenW;
      HPEN oldPen;

      hPenG = CreatePen (PS_SOLID, 0, Col);
      hPenW = CreatePen (PS_SOLID, 0, HiCol);

      oldPen = SelectObject (hdc, hPenG);
      

      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x, y + cy);

      SelectObject (hdc, hPenW);

      y ++;
      x ++;
      cy --;

      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x, y + cy);

      SelectObject (hdc, oldPen);
      DeleteObject (hPenG);
      DeleteObject (hPenW);
}


void BORDER::PrintLine (HWND MainWindow, HDC hdc, int x, int y, int cx, int cy)
{
      HPEN hPenG;
      HPEN oldPen;

      hPenG = CreatePen (PS_SOLID, 0, Col);

      oldPen = SelectObject (hdc, hPenG);

      if (cx > 0)
      {
          MoveToEx (hdc, x,y, NULL);
          LineTo (hdc, x + cx, y);
      }

      if (cy > 0)
      {
          MoveToEx (hdc, x,y, NULL);
          LineTo (hdc, x, y + cy);
 
          if (cx > 0)
          {
                MoveToEx (hdc, x + cx, y, NULL);
                LineTo (hdc, x + cx, y + cy);

                MoveToEx (hdc, x, y + cy, NULL);
                LineTo (hdc, x + cx, y + cy);
          }
      }

      SelectObject (hdc, oldPen);
      DeleteObject (hPenG);
}

void BORDER::PrintRaised (HWND MainWindow, HDC hdc, int x, int y, int cx, int cy)
{
      HPEN hPenG;
      HPEN hPenW;
      HPEN oldPen;

      hPenG = CreatePen (PS_SOLID, 0, Col);
      hPenW = CreatePen (PS_SOLID, 0, HiCol);

      oldPen = SelectObject (hdc, hPenG);
      
      if (cx > 0)
      {
            MoveToEx (hdc, x,y, NULL);
            LineTo (hdc, x + cx, y);
      }

      if (cy > 0)
      {
            MoveToEx (hdc, x,y, NULL);
            LineTo (hdc, x, y + cy);

            if (cx > 0)
            {
                  SelectObject (hdc, hPenW);
                  MoveToEx (hdc, x + cx, y, NULL);
                  LineTo (hdc, x + cx, y + cy);
 
                  MoveToEx (hdc, x, y + cy, NULL);
                  LineTo (hdc, x + cx, y + cy);
            }
      }

      SelectObject (hdc, hPenW);

      y ++;
      x ++;
      cx --;
      cy --;

      if (cx > 0)
      {
            MoveToEx (hdc, x,y, NULL);
            LineTo (hdc, x + cx, y);
      }

      if (cy > 0)
      {
            MoveToEx (hdc, x,y, NULL);
            LineTo (hdc, x, y + cy);

            if (cx > 0)
            {
                   SelectObject (hdc, hPenG);
                   MoveToEx (hdc, x + cx - 1, y, NULL);
                   LineTo (hdc, x + cx - 1, y + cy - 1);

                   MoveToEx (hdc, x, y + cy - 1, NULL);
                   LineTo (hdc, x + cx, y + cy - 1);
            }
      }
      SelectObject (hdc, oldPen);
      DeleteObject (hPenG);
      DeleteObject (hPenW);
}

void BORDER::PrintHigh (HWND MainWindow, HDC hdc, int x, int y, int cx, int cy)
{
      HPEN hPenG;
      HPEN hPenW;
      HPEN oldPen;

      hPenG = CreatePen (PS_SOLID, Width, Col);
      hPenW = CreatePen (PS_SOLID, Width, HiCol);

      oldPen = SelectObject (hdc, hPenW);

      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x + cx, y);

      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x, y + cy);


      SelectObject (hdc, hPenG);

      MoveToEx (hdc, x + cx, y, NULL);
      LineTo (hdc, x + cx, y + cy);

      MoveToEx (hdc, x, y + cy, NULL);
      LineTo (hdc, x + cx, y + cy);

      SelectObject (hdc, hPenW);

      y ++;
      x ++;
      cx --;
      cy --;

      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x + cx, y);

      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x, y + cy);

      SelectObject (hdc, hPenG);

      MoveToEx (hdc, x + cx - 1, y, NULL);
      LineTo (hdc, x + cx - 1, y + cy - 1);

      MoveToEx (hdc, x, y + cy - 1, NULL);
      LineTo (hdc, x + cx, y + cy - 1);

      SelectObject (hdc, oldPen);
      DeleteObject (hPenG);
      DeleteObject (hPenW);
}

void BORDER::PrintLow (HWND MainWindow, HDC hdc, int x, int y, int cx, int cy)
{
      HPEN hPenG;
      HPEN hPenW;
      HPEN oldPen;

      hPenG = CreatePen (PS_SOLID, Width, Col);
      hPenW = CreatePen (PS_SOLID, Width, HiCol);

      oldPen = SelectObject (hdc, hPenG);

      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x + cx, y);

      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x, y + cy);


      SelectObject (hdc, hPenW);

      MoveToEx (hdc, x + cx, y, NULL);
      LineTo (hdc, x + cx, y + cy);

      MoveToEx (hdc, x, y + cy, NULL);
      LineTo (hdc, x + cx, y + cy);

      SelectObject (hdc, hPenG);

      y ++;
      x ++;
      cx --;
      cy --;

      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x + cx, y);

      MoveToEx (hdc, x,y, NULL);
      LineTo (hdc, x, y + cy);

      SelectObject (hdc, hPenW);

      MoveToEx (hdc, x + cx - 1, y , NULL);
      LineTo (hdc, x + cx - 1, y + cy - 1);

      MoveToEx (hdc, x, y + cy - 1, NULL);
      LineTo (hdc, x + cx, y + cy - 1);


      SelectObject (hdc, oldPen);
      DeleteObject (hPenG);
      DeleteObject (hPenW);
}


void BORDER::Print (HWND MainWindow, HDC hdc, int x, int y, int cx, int cy)
{
      switch (Type)
      {
            case LINE :
               PrintLine (MainWindow, hdc, x,y, cx, cy);
               break;
            case RAISED :
               PrintRaised (MainWindow, hdc, x,y, cx, cy);
               break;
            case HIGH :
               PrintHigh (MainWindow, hdc, x,y, cx, cy);
               break;
            case LOW :
               PrintLow (MainWindow, hdc, x,y, cx, cy);
               break;
            case HLINE :
               PrintHLine (MainWindow, hdc, x,y, cx);
               break;
            case VLINE :
               PrintHLine (MainWindow, hdc, x,y, cy);
               break;
            case RAISEDHLINE :
               PrintRaisedHLine (MainWindow, hdc, x,y, cx);
               break;
            case RAISEDVLINE :
               PrintRaisedVLine (MainWindow, hdc, x,y, cy);
               break;
      }
}

void FILLEDBORDER::FillRect (HDC hdc, int x, int y, int cx, int cy)
{
      RECT rect;
      HBRUSH hBrush;

      rect.left   = x + 1;
      rect.top    = y + 1;
      rect.right  = x + cx;
      rect.bottom = y + cy;
      hBrush = CreateSolidBrush (FillCol);
      ::FillRect (hdc, &rect, hBrush);
      DeleteObject (hBrush);
}

void FILLEDBORDER::Print (HWND MainWindow, HDC hdc, int x, int y, int cx, int cy)
{
      FillRect (hdc, x,y, cx, cy);
      BORDER::Print (MainWindow, hdc, x, y, cx, cy);
}

int CFIELD::fspace = 5;
int CFIELD::fspaceh = 4;
int CFORM::fspace = 5;
int CFORM::fspaceh = 4;
int CFORM::bottomspace = 3;

BOOL CFIELD::DecimalIsKomma = FALSE;
 
void CFIELD::TimeFormat (void)
/**
Zeitfeld formatieren.
**/
{
         char hour [3];
         char min [3];
         int h, m;
         char buffer [20];
         char *p;

         strcpy (hour, "  ");
         strcpy (min,  "  ");
         strcpy (buffer, (char *) this->feld);
         p = strchr (buffer, ':');
         if (p == NULL);
         {
                p = strchr (buffer, '.');
         }
         if (p == NULL)
         {
             memcpy (hour, buffer, 2);
             memcpy (min,  &buffer[2], 2);
         }
         else
         {
             *p = (char) 0;
             p += 1;
             strcpy (hour, buffer);
             strcpy (min, p);
         }
         h = atoi (hour);
         m = atoi (min);
         h = h % 24;
         m = m % 60;
         sprintf ((char *) this->feld, "%02d:%02d", h, m);
}




void CFIELD::DatFormat (void)
/**
Datumsfeld formatieren.
**/
{
      char tags [3];
      char mons [3];
      char jrs [3];
      short tag;
      short mon;
      short jr;
      char *punkt;

      if (memcmp (this->feld, "  ", 2) <= 0)
      {
                 return;
      }

      if (punkt = strchr ((char *) this->feld, '.'))
      {
                 tag = atoi ((char *) this->feld);
                 punkt += 1;
                 mon = atoi (punkt);
                 punkt = strchr (punkt, '.');
                 if (punkt == 0)
                 {
                              jr = jrh1;
                 }
                 else
                 {
                               punkt += 1;
                               jr = atoi (punkt);
                 }
      }
      else
      {
                 memcpy (tags, (char *) this->feld, 2);
                 tags[2] = 0;
                 memcpy (mons, (char *) this->feld + 2, 2);
                 mons[2] = 0;
                 strcpy (jrs, (char *) this->feld + 4);
                 tag = atoi (tags);
                 mon = atoi (mons);
                 jr = atoi (jrs);
      }

      if (jr < 100)
      {
             if (jr < jrhstart)
             {
                  jr += jrh2;
             }
             else
             {
                  jr += jrh1;
             }
      }


      if (mon > 12 || mon < 1) 
      {
                 strcpy ((char *) this->feld, "02.01.1899");
                 return;
      }

      if (tag < 1)
      {
                 strcpy ((char *) this->feld, "01.01.1899");
                 return;
      }
                 
      if (IsMon31 (mon) && tag > 31) 
      {
                 strcpy ((char *) this->feld, "01.01.1899");
                 return;
      }
      if (IsMon30 (mon) && tag > 30) 
      {
                 strcpy ((char *) this->feld, "01.01.1899");
                 return;
      }

      if (IsMon29 (mon, jr) && tag > 29) 
      {
                 strcpy ((char *) this->feld, "01.01.1899");
                 return;
      }

      if (IsMon28 (mon, jr) && tag > 28) 
      {
                 strcpy ((char *) this->feld, "01.01.1899");
                 return;
      }

      if (jr < 100)
      {
             if (jr < jrhstart)
             {
                  jr += jrh2;
             }
             else
             {
                  jr += jrh1;
             }
      }

      if (memcmp (this->picture, "dd.mm.yyyy", 10) == 0)
      {
                  sprintf ((char *) this->feld, "%02hd.%02hd.%04hd",
                                  tag,mon,jr);
      }
      else if (memcmp (this->picture, "dd.mm.yy", 8) == 0)
      {
                  sprintf ((char *) this->feld, "%02hd.%02hd.%02hd",
                                  tag,mon,jr % 100);
      }
                   
      else if (memcmp (this->picture, "ddmmyyyy", 8) == 0)
      {
                  sprintf ((char *) this->feld, "%02hd%02hd%04hd",
                                  tag,mon,jr);
      }
      else if (memcmp (this->picture, "ddmmyy", 6) == 0)
      {
                  sprintf ((char *) this->feld, "%02hd%02hd%02hd",
                                  tag,mon,jr % 100);
      }
      else
      {
                  strcpy ((char *) this->feld, "01.01.1900");
      }
}


void CFIELD::ToFormat (void)
/**
Feld formatiert in dest uebertragen.
**/
{
//        char format[16];
        char buffer [512];
        short  swert;
        int    iwert;
        long   lwert;
        double dwert;
/*
        int vkomma;
        int nkomma;
*/
        char *punkt;
        static BOOL DecOk = FALSE;

        if (DecOk == FALSE)
        {
            if (getenv ("DECIMALKOMMA") != NULL)
            {
                DecimalIsKomma =  atoi (getenv ("DECIMALKOMMA"));
            }
            DecOk = TRUE;
        }

        if (this->attribut == CREMOVED)
        {
                    return;
        }
        if (this->attribut == CCOLBUTTON)
        {
                    return;
        }
        if (strlen (this->picture) == NULL)
        {
                    if (this->length > 0 && this->length < 511)
                    {
                            memset (buffer, ' ', this->length);
                            memcpy (buffer,  (char *) this->feld,
                                    strlen ((char *) feld));
                            buffer [this->length] = 0;
							strcpy ((char *) this->feld, buffer);
                    }
                    return;
        }
        if (memcmp (this->picture, "dd", 2) == 0)
        {
                    DatFormat ();
                    return;
        }
        if (memcmp (this->picture, "hh", 2) == 0)
        {
                    TimeFormat ();
                    return;
        }

        if (this->picture[0] != '%')
        {
                    return;
        }

		if (strstr (this->picture, "hd"))
		{
			swert = atoi ((char *) feld);
			sprintf ((char *) feld, picture, swert);
		}
		else if (strstr (this->picture, "ld"))
		{
			lwert = atol ((char *) feld);
			sprintf ((char *) feld, picture, lwert);
		}
		else if (strstr (this->picture, "d"))
		{
			iwert = atoi ((char *) feld);
			sprintf ((char *) feld, picture, iwert);
		}
		else if (strstr (this->picture, "lf"))
		{
			dwert = ratod ((char *) feld);
			sprintf ((char *) feld, picture, dwert);
            if (DecimalIsKomma)
            {
                      punkt = strchr ((char *) feld, '.');
                      if (punkt) *punkt = ',';
            }
		}
/*
        vkomma = nkomma = 0;
        vkomma = atoi (this->picture + 1);
        punkt = strchr (this->picture, '.');
        if (punkt)
        {
                    nkomma = atoi (punkt + 1);
        }
		else if (punkt = strchr (this->picture, ','))		
        {
                    nkomma = atoi (punkt + 1);
        }
        sprintf (format, "%c%d.%dlf", '%', vkomma, nkomma);

        dwert = (double) ratod ((char *) this->feld);
        sprintf (buffer, format, dwert);
        memset ((char *) this->feld, ' ', this->length);
        memcpy ((char *) this->feld, buffer, strlen (buffer));
        punkt = (char *) this->feld + this->length;
        *punkt = 0;
        return;
*/
}

int CFIELD::GetLine (void)
{
      POINT p; 

      TEXTMETRIC tm;
      HFONT hFont, oldfont;

      if ((BkMode & ES_MULTILINE) == 0)
      {
          return 0;
      }

      HDC hdc = GetDC (hWnd);
	  if (mFont)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else
	  {
		          hFont = CreateStdFont (NULL);
	  }
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);
      DeleteObject (SelectObject (hdc, oldfont));
      ReleaseDC (hWnd, hdc);

      GetCaretPos (&p);
      return p.y / tm.tmHeight;
}


int CFIELD::GetCX (void)
{
	  TEXTMETRIC tm;
	  HDC hdc;
	  HFONT hFont, oldfont;
      SIZE size;

	  hdc = GetDC (hWnd);

      if (attribut == CBORDER)
      {
         if (PosmFont == NULL)
         {
             PosmFont = mFont;
         }
 	     if (PosmFont)
         {
            hFont = SetDeviceFont (hdc, PosmFont, &tm);
         }
	     else
         {
	        hFont = CreateStdFont (NULL);
         }
      }
      else
      {
 	     if (mFont)
         {
            hFont = SetDeviceFont (hdc, mFont, &tm);
         }
	     else
         {
	        hFont = CreateStdFont (NULL);
         }
      }
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

	  if (this->cxorg == 0)
	  {
              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
	  }
	  else if (Pixelorg == FALSE)
	  {
              GetTextExtentPoint32 (hdc, "X", 1 , &size);
			  size.cx *= this->cxorg;
	  }
	  else
	  {
		      size.cx = this->cxorg;
	  }
	  DeleteObject (SelectObject (hdc, oldfont));
	  ReleaseDC (hWnd, hdc);
      return (size.cx);
}


int CFIELD::GetCY (void)
{
	  TEXTMETRIC tm;
	  HDC hdc;
	  HFONT hFont, oldfont;
      int cy;
//      RECT rect;


	  hdc = GetDC (hWnd);


      if (attribut == CBORDER)
      {
         if (PosmFont == NULL)
         {
             PosmFont = mFont;
         }
 	     if (PosmFont)
         {
            hFont = SetDeviceFont (hdc, PosmFont, &tm);
         }
	     else
         {
	        hFont = CreateStdFont (NULL);
         }
      }
      else
      {
 	     if (mFont)
         {
            hFont = SetDeviceFont (hdc, mFont, &tm);
         }
	     else
         {
	        hFont = CreateStdFont (NULL);
         }
      }
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

      if (attribut == CCOMBOBOX)
      {
          cy = 0;
      }
      else
      {
	      cy = this->cyorg;
      }
	  if (cy == 0)
	  {
//		     cy = (int) (double) ((double) tm.tmHeight * 1.3);
		     cy = tm.tmHeight + fspaceh;
	  }
      else if (attribut == CLISTBOX)
      {
/*
             if (hWnd != NULL)
             {
                 GetClientRect (hWnd, &rect);
                 cy = rect.bottom;
             }
*/
             if (Pixelorg == FALSE)
             {
  		         cy *= (int) (double) ((double) tm.tmHeight * 1.3);
             }
      }
	  else if (Pixelorg == FALSE)
	  {
		     cy *= (int) (double) ((double) tm.tmHeight);
	  }


	  DeleteObject (SelectObject (hdc, oldfont));
	  ReleaseDC (hWnd, hdc);
      return (cy);
}

int CFIELD::GetX (void)
{
	  TEXTMETRIC tm;
	  RECT rect;
	  HDC hdc;
      SIZE size;
	  int x;
	  int cx;

	  hdc = GetDC (hWnd);

      if (PosmFont == NULL)
      {
            PosmFont = mFont;
      }
 	  if (PosmFont)
	  {
            PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
	        PoshFont = CreateStdFont (NULL);
	  }

 	  if (mFont)
	  {
            hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else
	  {
	        hFont = CreateStdFont (NULL);
	  }
      oldfont = SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);

	  if (this->x == -1) 
	  {
              
              DeleteObject (SelectObject (hdc, hFont));
	          cx = this->cx;
 	          if (cx == 0)
			  {
                     GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			         cx = size.cx;
			  }
	          else if (Pixel == FALSE)
			  {
                     GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		             cx *= size.cx;
			  }
	          GetClientRect (hWndMain, &rect);
		      x = max (0, (rect.right - cx) / 2);

	          DeleteObject (SelectObject (hdc, oldfont));
	          ReleaseDC (hWnd, hdc);
			  return x;
	  }

      x = this->xorg;
	  if (Pixelorg == FALSE)
	  {
              GetTextExtentPoint32 (hdc, "X", 1 , &size);
			  size.cx *= this->xorg;
	  }
	  else
	  {
		      size.cx = this->xorg;
	  }
      DeleteObject (hFont);
	  DeleteObject (SelectObject (hdc, oldfont));
	  ReleaseDC (hWnd, hdc);
      return (size.cx);
}

int CFIELD::GetXP (void)
{
      return Chs.GetX ();
}

int CFIELD::GetYP (void)
{
      return Chs.GetY ();
}

int CFIELD::GetCXP (void)
{
	  return Chs.GetCX ();
}

int CFIELD::GetCYP (void)
{
	  return Chs.GetCY ();
}

int CFIELD::GetY (void)
{
	  TEXTMETRIC tm;
	  HDC hdc;
	  HFONT PoshFont, oldfont;
      int y;


	  hdc = GetDC (hWnd);

      if (PosmFont == NULL)
      {
            PosmFont = mFont;
      }
 	  if (PosmFont)
	  {
            PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
	        PoshFont = CreateStdFont (NULL);
	  }

      oldfont = SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);

	  y = this->yorg;
	  if (y == -1) return y;

	  if (Pixelorg == FALSE)
	  {
//		     y *= (int) (double) ((double) tm.tmHeight * 1.3);
		     y *= (tm.tmHeight + fspace);
	  }
	  DeleteObject (SelectObject (hdc, oldfont));
	  ReleaseDC (hWnd, hdc);
      return (y);
}

int CFIELD::GetHeight (void)
{
	  int y, cy;

	  CFORM *cForm;

	  if (attribut == CFFORM)
	  {
		  cForm = (CFORM *) feld;
		  cForm->GetRect (&cx, &cy);
		  return (cy);
	  }
	  y  = GetY ();
	  cy = GetCY ();
	  if (y != -1)
	  {
		  return y + cy;
	  }
	  return cy;
}

int CFIELD::GetWidth (void)
{
	  int x, cx, cy;
	  CFORM *cForm;

	  if (attribut == CFFORM)
	  {
		  cForm = (CFORM *) feld;
		  cForm->GetRect (&cx, &cy);
		  return (cx);
	  }

	  x  = GetX ();
	  cx = GetCX ();
	  if (x != -1)
	  {
		  return x + cx;
	  }
	  return cx;
}

int CFIELD::GetHeightText (void)
{
	  int y, cy;

	  CFORM *cForm;

      if (Pixelorg) return GetHeight ();

	  if (attribut == CFFORM)
	  {
		  cForm = (CFORM *) feld;
		  cForm->GetRectText (&cx, &cy);
		  return (cy);
	  }
	  y  = GetYorg ();
	  cy = GetCYorg ();
	  if (y != -1)
	  {
		  return y + cy;
	  }
	  return cy;
}


int CFIELD::GetWidthText (void)
{
	  int x, cx, cy;
	  CFORM *cForm;

      if (Pixelorg) return GetWidth ();

	  if (attribut == CFFORM)
	  {
		  cForm = (CFORM *) feld;
		  cForm->GetRectText (&cx, &cy);
		  return (cx);
	  }

	  x  = GetXorg ();
	  cx = GetCXorg ();
	  if (x != -1)
	  {
		  return x + cx;
	  }
	  return cx;
}


void CFIELD::Setxpplus (int plus)
{

	  CFORM *cForm;

	  if (attribut == CFFORM)
	  {
		  cForm = (CFORM *) feld;
		  cForm->Setxpplus (plus);
		  return;
	  }
      xpplus = plus;
}

void CFIELD::Setypplus (int plus)
{

	  CFORM *cForm;

	  if (attribut == CFFORM)
	  {
		  cForm = (CFORM *) feld;
		  cForm->Setypplus (plus);
		  return;
	  }
      ypplus = plus;
}

void CFIELD::MoveToFoot (HWND MainWindow)
{
	  TEXTMETRIC tm;
	  SIZE size;
	  RECT rect;

	  if (attribut == CFFORM)
	  {
     	  CFORM *cForm = (CFORM *) feld;
          cForm->MoveToFoot (MainWindow);
          return;
      }

      HDC hdc = GetDC (MainWindow);

	  if (mFont)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PosmFont)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }
	  GetClientRect (MainWindow, &rect);
      HFONT oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

	  if (cx == 0)
	  {
              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			  cx = size.cx;
	  }
	  else if (Pixel == FALSE)
	  {
              GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		      cx *= size.cx;
	  }

	  if (cy == 0)
	  {
		  cy = tm.tmHeight + fspaceh;
	  }
	  else if (Pixel == FALSE)
	  {
		  cy *= tm.tmHeight;
	  }

      SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  if (this->x == -1)
	  {
		         x = max (0, (rect.right - cx) / 2);
	  }
	  else if (Pixel == FALSE)
	  {
                 x *= size.cx;
	  }

      y = rect.bottom - cy - CFORM::bottomspace;
      xorg = x;
      yorg = y;
      cxorg = cx;
      cyorg = cy;
      Pixel = TRUE;
      Pixelorg = TRUE;
      SelectObject (hdc, oldfont);
      DeleteObject (hFont);
      DeleteObject (PoshFont);
      ReleaseDC (MainWindow, hdc);
}
     

void CFIELD::Move (void)
/**
Feld an aktuelle Position bewegen.
**/
{
	  CFORM *cForm;
	  int x1, y1, cx1, cy1;


//	  if (hWnd == NULL) return;
	  if (attribut == CFFORM)
	  {
		  cForm = (CFORM *) feld;
	      cForm->Move ();
		  return;
	  }
	  if (hWnd == NULL) return;
	  x1 = xp + xstart;
	  y1 = yp + start;
	  cx1 = cxp;
	  cy1 = cyp;
	  ::MoveWindow (hWnd, x1, y1, cx1, cy1, TRUE);
}

void CFIELD::MoveWindow (void)
/**
Element an neue Groesse des Haupfensters anpassen.
**/
{
	  CFORM *cForm;

	  if (hWndMain == NULL) return;

	  if (attribut == CFFORM)
	  {
		  cForm = (CFORM *) feld;

		  if (x == -1) 
		  {
              if (y < 0)
              {
			        cForm->CentX (hWndMain);
              }
              else
              {
			        cForm->Centx (hWndMain);
              }
		  }

		  if (x == -2) 
		  {
			  cForm->Rightx (hWndMain);
		  }

          if (y == -1) 
		  {
              if (x < 0)
              {
			        cForm->CentY (hWndMain);
              }
              else
              {
			        cForm->Centy (hWndMain);
              }
		  }

          if (y == -2) 
		  {
			  cForm->Bottomy (hWndMain);
		  }

	      cForm->MoveWindow ();
		  return;
	  }

	  if (hWnd == NULL) return;
/*
	  if (Chs.Getchsizex ());
	  else if (Chs.Getchsizey ());
	  else if (Chs.Getchposx ());
	  else if (Chs.Getchposy ());
	  else
	  {
		  return;
	  }
*/

	  Chs.NewSize (hWndMain);
	  Chs.NewPos (hWndMain);
	  ::MoveWindow (hWnd, Chs.GetX (), Chs.GetY (), Chs.GetCX (), Chs.GetCY (), TRUE);
}

void CFIELD::Setchsize (int modex, int modey)
/**
Element an neue Groesse des Haupfensters anpassen.
**/
{
      Chs.Setchsize (modex, modey); 
}

void CFIELD::Setchpos (int modex, int modey)
/**
Element an neue Groesse des Haupfensters anpassen.
**/
{
      Chs.Setchpos (modex, modey); 
}

void CFIELD::Invalidate (BOOL mode)
{
      RECT rect;

      if (hWndMain == NULL) return;

      if (attribut != CDISPLAYONLY &&
          attribut != CBORDER)
      {
          if (hWnd != NULL)
          {
                 InvalidateRect (hWnd, NULL, mode);
          }
          return;
      }

      if (Pixel == FALSE) return;
      

      rect.left = xp;
      rect.top = yp;
      rect.right = xp + cxp;
      rect.bottom = yp + cyp;
      InvalidateRect (hWndMain, &rect, mode);
}


void CFIELD::display (HWND MainWindow,HDC hdc)
{
	  CFORM *cForm;

	  this->hWndMain = MainWindow;

	  switch (attribut)
	  {
	     case CDISPLAYONLY :
		      displaytxt (MainWindow, hdc);
		      break;
	     case CBORDER :
		      displayborder (MainWindow, hdc);
		      break;
		 case CBUTTON :
			  displaybu (MainWindow, hdc);
			  break;
		 case CSTATIC :
			  displaystatic (MainWindow, hdc);
			  break;
		 case CEDIT :
			  displayedit (MainWindow, hdc);
			  break;
		 case CRIEDIT :
			  displayredit (MainWindow, hdc);
			  break;
		 case CCOMBOBOX :
			  displaycombobox (MainWindow, hdc);
			  break;
		 case CCOLBUTTON :
			  displaycolbu (MainWindow, hdc);
			  break;
		 case CREADONLY :
			  displayrdonly (MainWindow, hdc);
			  break;
		 case CCAPTBORDER :
			  displaycp (MainWindow, hdc);
			  break;
		 case CLISTBOX :
			  displaylbox (MainWindow, hdc);
			  break;
		 case CFFORM :
			  cForm = (CFORM *) feld;

              if (NewPosNeeded)
              {

                 cForm->SetStart (0);  
                 cForm->xSetStart (0);  
                 start = xstart = 0;

			     if (x == -1) 
                 {
                      if (y < 0)
                      {
			                  cForm->CentX (hWndMain);
                      }
                      else
                      {
			                  cForm->Centx (hWndMain);
                      }
                 }
			     else if (x == -2) 
                 {
				     cForm->Rightx (MainWindow);
                 }

                 if (y == -1) 
                 {
                     if (x < 0)
                     {
				              cForm->CentY (MainWindow);
                     }
                     else
                     {
				              cForm->Centy (MainWindow);
                     }
                 }

                 else if (y == -2) 
                 {
				     cForm->Bottomy (MainWindow);
                 }
                 if (hWnd != NULL)
                 {
 	                 cForm->MoveWindow ();
//			         cForm->display  (MainWindow, hdc);
                 }
                 else
                 {
			         cForm->display  (MainWindow, hdc);
                 }
                 NewPosNeeded = FALSE;

              }
              else
              {
			     cForm->display  (MainWindow, hdc);
              }
              hWnd = MainWindow;
			  break;
	  }
	  return;
}

void CFIELD::display (void)
{
      HDC hdc;
      
      hdc = GetDC (hWndMain);
      display (hWndMain, hdc);
      ReleaseDC (hWndMain, hdc);
}


void CFIELD::display (HWND hWndMain)
{
      HDC hdc;
      
      hdc = GetDC (hWndMain);
      display (hWndMain, hdc);
      ReleaseDC (hWndMain, hdc);
}


void CFIELD::SetNewPos (BOOL b)
{
	  CFORM *cForm;


	  if (attribut == CFFORM)
	  {
          NewPosNeeded = b;
		  cForm = (CFORM *) feld;
		  cForm->SetNewPos (b);
		  return;
	  }
      NewPosNeeded = b;
}

void CFIELD::destroy (void)
{
	  HDC hdc;
	  CFORM *cForm;


	  if (attribut == CFFORM)
	  {
          hWnd = NULL;
		  cForm = (CFORM *) feld;
		  cForm->destroy ();

          x = xorg;
          y = yorg;
          cx = cxorg;
          cy = cyorg;
          Pixel = Pixelorg;
          NewPosNeeded = TRUE;
          AbsPos = FALSE;
          PosSet = FALSE;

		  return;
	  }

	  hdc = GetDC (hWnd); 
	  if (oldfont)
	  {
              SelectObject (hdc, oldfont);
              ::SendMessage (hWnd, WM_SETFONT, (WPARAM) oldfont, 0);
			  oldfont = NULL;
	  }
	  if (hWnd)
	  {
		   DestroyWindow (hWnd);
		   hWnd = NULL;
	  }
	  if (PoshFont != NULL && PoshFont != hFont)
	  {
		   DeleteObject (PoshFont);
		   PoshFont = NULL;
	  }
	  if (hFont != NULL)
	  {
		   DeleteObject (hFont);
		   hFont = NULL;
	  }
	  ReleaseDC (hWnd, hdc);
      if (QuikInfo)
      {
           DestroyWindow (QuikInfo);
           QuikInfo = NULL;
      }

      x = xorg;
      y = yorg;
      cx = cxorg;
      cy = cyorg;
      Pixel = Pixelorg;
      NewPosNeeded = TRUE;
      AbsPos = FALSE;
      PosSet = FALSE;
}

void CFIELD::displaybu (HWND MainWindow,HDC hdc)
{
	  TEXTMETRIC tm;
	  int x, y;
	  int cx, cy;
	  SIZE size;
      DWORD bustyle;
	  RECT rect;
	  HANDLE hMainInst;

      if (hWnd) 
      {
          InvalidateRect (hWnd, NULL, FALSE);
          UpdateWindow (hWnd);
          return;
      }
      else
      {
          hFont = NULL;
          PoshFont = NULL;
      }

	  if (hFont == NULL && mFont != NULL)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else if (hFont == NULL)
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PoshFont == NULL && PosmFont != NULL)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }

	  GetClientRect (MainWindow, &rect);
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

	  cx = this->cx;
	  if (cx == 0)
	  {
              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			  cx = size.cx;
	  }
	  else if (Pixel == FALSE)
	  {
              GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		      cx *= size.cx;
	  }

      cy = this->cy;
	  if (cy == 0)
	  {
//		  cy = (int) (double) ((double) tm.tmHeight * 1.3);
		  cy =  tm.tmHeight + fspaceh;
	  }
	  else if (Pixel == FALSE)
	  {
		  cy *= tm.tmHeight;
	  }

      SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  if (this->x == -1)
	  {
		         x = max (0, (rect.right - cx) / 2);
	  }
	  else if (Pixel == FALSE)
	  {
                 x = this->x * size.cx;
	  }
	  else
	  {
		         x = this->x;
	  }

	  if (this->y == -1 && this->cy == 0)
	  {
//		         y = max (0, (rect.bottom - (int) (double) 
//					         ((double) tm.tmHeight * 1.3) / 2));
		         y = max (0, (rect.bottom - (tm.tmHeight + fspace)) / 2);
	  }

	  if (this->y == -1 && this->cy && Pixel == FALSE)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * this->cy) / 2));
	  }
	  else if (this->y == -1 && this->cy && Pixel)
	  {
		         y = max (0, (rect.bottom - this->cy) / 2);
	  }

	  else if (Pixel == FALSE)
	  {
//                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
                 y = this->y * (tm.tmHeight + fspace);
	  }
	  else
	  {
                 y = (int) (double) ((double) this->y);
	  }


      x += xstart;
	  y += start;
      y += ypplus;
	  xp = x;
	  yp = y;
	  cxp = cx;
	  cyp = cy;
	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);
 	  bustyle = BkMode;

      hMainInst = (HANDLE) GetWindowLong (MainWindow, GWL_HINSTANCE);
      hWnd = CreateWindow ("BUTTON",
                           (char *) feld,
                           WS_CHILD | WS_VISIBLE | bustyle,
                           x, y,
                           cx, cy,
                           MainWindow,
                           (HMENU) ID,
                           hMainInst,
                           NULL);

      ::SendMessage (hWnd,WM_SETFONT, (WPARAM) hFont, 0);

//      if (BkMode & DISABLED) Disabled = TRUE;
      if (Disabled)
      {
                        EnableWindow (hWnd, FALSE);
      }
      else
      {
                        EnableWindow (hWnd, TRUE);
      }

      if (BkMode & BS_BITMAP && bitmap != NULL)
      {
          ::SendMessage (hWnd,BM_SETIMAGE, (WPARAM) IMAGE_BITMAP, 
								                       (LPARAM) bitmap);
      }
      SetText ();
}

void CFIELD::displaystatic (HWND MainWindow,HDC hdc)
{
	  TEXTMETRIC tm;
	  int x, y;
	  int cx, cy;
	  SIZE size;
      DWORD bustyle;
	  RECT rect;
	  HANDLE hMainInst;

      if (hWnd) 
      {
          InvalidateRect (hWnd, NULL, FALSE);
          UpdateWindow (hWnd);
          return;
      }
      else
      {
          hFont = NULL;
          PoshFont = NULL;
      }

	  if (hFont == NULL && mFont != NULL)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else if (hFont == NULL)
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PoshFont == NULL && PosmFont != NULL)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }
	  GetClientRect (MainWindow, &rect);
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

	  cx = this->cx;
	  if (cx == 0)
	  {
              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			  cx = size.cx;
	  }
	  else if (Pixel == FALSE)
	  {
              GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		      cx *= size.cx;
	  }

      cy = this->cy;
	  if (cy == 0)
	  {
//		  cy = (int) (double) ((double) tm.tmHeight * 1.3);
		  cy = tm.tmHeight + fspaceh;
	  }
	  else if (Pixel == FALSE)
	  {
		  cy *= tm.tmHeight;
	  }

      SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  if (this->x == -1)
	  {
		         x = max (0, (rect.right - cx) / 2);
	  }
	  else if (Pixel == FALSE)
	  {
                 x = this->x * size.cx;
	  }
	  else
	  {
		         x = this->x;
	  }

	  if (this->y == -1 && this->cy == 0)
	  {
//		         y = max (0, (rect.bottom - (int) (double) 
//					         ((double) tm.tmHeight * 1.3) / 2));
		         y = max (0, (rect.bottom - (tm.tmHeight + fspace)) / 2);
	  }

	  if (this->y == -1 && this->cy && Pixel == FALSE)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * this->cy) / 2));
	  }
	  else if (this->y == -1 && this->cy && Pixel)
	  {
		         y = max (0, (rect.bottom - this->cy) / 2);
	  }

	  else if (Pixel == FALSE)
	  {
//                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
                 y = this->y * (tm.tmHeight + fspace);
	  }
	  else
	  {
                 y = (int) (double) ((double) this->y);
	  }


      x += xstart;
	  y += start;
	  xp = x;
	  yp = y;
	  cxp = cx;
	  cyp = cy;
	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);
	  bustyle = BkMode;
      hMainInst = (HANDLE) GetWindowLong (MainWindow, GWL_HINSTANCE);
      hWnd = CreateWindow ("STATIC",
                           (char *) feld,
                           WS_CHILD | WS_VISIBLE | bustyle,
                           x, y,
                           cx, cy,
                           MainWindow,
                           (HMENU) ID,
                           hMainInst,
                           NULL);

      ::SendMessage (hWnd,WM_SETFONT, (WPARAM) hFont, 0);
      hFont = NULL;
      PoshFont = NULL;
}


void CFIELD::CreateQuikInfos (HWND hMainWindow)
{
     BOOL bSuccess ;
     TOOLINFO ti ;
     HINSTANCE hMainInst;

     hMainInst = (HANDLE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
     if (QuikInfo) DestroyWindow (QuikInfo);
     QuikInfo = CreateWindow (TOOLTIPS_CLASS,
                                "",
                                WS_POPUP,
                                0, 0,
                                0, 0,
                                hMainWindow,
                                NULL,
                                hMainInst,
                                NULL);


     ZeroMemory (&ti, sizeof (TOOLINFO)) ;
     ti.cbSize = sizeof (TOOLINFO) ;
     ti.uFlags = TTF_IDISHWND | TTF_CENTERTIP | TTF_SUBCLASS ;
     ti.hwnd   = hMainWindow;
     ti.uId    = (UINT) (HWND) hWnd;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
}

int CFIELD::GetComboPos (void)
{
       int i;
       if (Combobox == NULL) return 0;

       clipped ((char *) feld);
       for (i = 0; Combobox[i]; i ++)
       {
           if (strcmp ((char *) feld, clipped (Combobox[i])) == 0)
           {
               return i;
           }
       }
       return 0;
}

void CFIELD::SetPosCombo (int pos)
{
       int i;

       if (Combobox == NULL)
       {
           strcpy ((char *) feld, " ");
           return;
       }

       for (i = 0; Combobox[i]; i ++);
       if (pos >= i)
       {
           strcpy ((char *) feld, " ");
       }
       else
       {
           strcpy ((char *) feld, Combobox [pos]);
       }
       if (hWnd != NULL)
       {
           ::SendMessage (hWnd, CB_SETCURSEL, pos, 0l);
       }
}


void CFIELD::FillComboBox (void)
{
       int i;

       ::SendMessage (hWnd, CB_RESETCONTENT, 0l, 0l); 

       if (Combobox == NULL) return;

       for (i = 0; Combobox[i]; i ++)
       {
           ::SendMessage (hWnd, CB_ADDSTRING, 0, 
                           (LPARAM) Combobox[i]);
       }
}

void CFIELD::SetLimitText (int limit)
{
       if (attribut == CEDIT ||
           attribut == CRIEDIT)
       {
           ::SendMessage (hWnd, EM_SETLIMITTEXT, (WPARAM) limit, 0l);
       }
}


void CFIELD::SetText ()
{
	   CFORM *cForm;

 	   if (attribut == CFFORM)
       {
		  cForm = (CFORM *) feld;
          cForm->SetText ();
          return;
       }

       if (attribut == CEDIT ||
           attribut == CRIEDIT)
       {
           if ((BkMode & ES_AUTOHSCROLL) == 0)
           {
   	               ToFormat ();
           }
           clipped ((char *) feld);
           SetWindowText (hWnd, (char *) feld);
       }
       else if (attribut == CREADONLY ||
                attribut == CSTATIC)
       {
   	       ToFormat ();
           clipped ((char *) feld);
           char *p = (char *) feld;
           if (strlen (p) <= 1 && p[0] == ' ') p[0] = 0;
           SetWindowText (hWnd, (char *) feld);
       }
       else if (attribut == CCOMBOBOX)
       {
           clipped ((char *) feld);
           if ((BkMode & CBS_DROPDOWNLIST) == CBS_DROPDOWNLIST)
           {
                if (strlen ((char *) feld) == 0)
                {
                    strcpy ((char *) feld, " ");
                }
                ::SendMessage (hWnd, CB_SELECTSTRING, 0,
                                    (LPARAM) clipped ((char *) feld));
           }
           else
           {
                 if ((BkMode & ES_AUTOHSCROLL) == 0)
				 {
   	                     ToFormat ();
				 }
                 clipped ((char *) feld);
                 SetWindowText (hWnd, (char *) feld);
           }
       }
       else if (attribut == CBUTTON && (BkMode & (BS_AUTOCHECKBOX | BS_AUTORADIOBUTTON |
                                                  BS_CHECKBOX | BS_RADIOBUTTON)))
       {
           if (hWnd)
           {
              if (*((char *) checkboxfeld) == 'J')
              {
                  ::SendMessage (hWnd, BM_SETCHECK, (WPARAM) BST_CHECKED, 0l);
              }
              else
              {
                  ::SendMessage (hWnd, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0l);
              }
           }
       }
       else if (attribut == CDISPLAYONLY)
       {
           display ();
       }
       else if (hWnd != NULL)
       {
		  InvalidateRect (hWnd, NULL, TRUE);
		  UpdateWindow (hWnd);
       }
}

void CFIELD::GetText ()
{

	   CFORM *cForm;
       int idx;

 	   if (attribut == CFFORM)
       {
		  cForm = (CFORM *) feld;
          cForm->GetText ();
          return;
       }

       if (hWnd == NULL)
       {
           return;
       }
       if (attribut == CEDIT ||
           attribut == CRIEDIT)
       {
           GetWindowText (hWnd, (char *) feld, length);
       }
       else if (attribut == CCOMBOBOX)
       {
           if ((BkMode & CBS_DROPDOWNLIST) == CBS_DROPDOWNLIST)
           {
                idx = ::SendMessage (hWnd, CB_GETCURSEL, 0, 0);
                if (idx >= 0)
                {
                    ::SendMessage (hWnd, CB_GETLBTEXT, idx, (LPARAM) feld);
                }
           }
           else
           {
                GetWindowText (hWnd, (char *) feld, length);
           }
       }
       else if (attribut == CBUTTON && (BkMode & (BS_AUTOCHECKBOX | BS_AUTORADIOBUTTON |
                                        BS_CHECKBOX | BS_RADIOBUTTON)))
       {
           if (hWnd)
           {
               if (::SendMessage (hWnd, BM_GETCHECK, 0l, 0l))
              {
                     strcpy ((char *) checkboxfeld, "J");
              }
              else
              {
                     strcpy ((char *) checkboxfeld, "N"); 
              }
           }
       }
}

void CFIELD::displayedit (HWND MainWindow,HDC hdc)
{
	  TEXTMETRIC tm;
	  int x, y;
	  int cx, cy;
	  SIZE size;
      DWORD bustyle;
	  RECT rect;
	  HANDLE hMainInst;
	  int frx, fry;
	  extern void SetRDOMargins (LRESULT);

      if (hWnd) 
      {
          InvalidateRect (hWnd, NULL, FALSE);
          UpdateWindow (hWnd);
          return;
      }
      else
      {
          hFont = NULL;
          PoshFont = NULL;
      }

	  if (hFont == NULL && mFont != NULL)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else if (hFont == NULL)
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PoshFont == NULL && PosmFont != NULL)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }
	  GetClientRect (MainWindow, &rect);
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  cx = this->cx;
	  if (cx == 0)
	  {
              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			  cx = size.cx;
	  }
	  else if (Pixel == FALSE)
	  {
		      cx *= size.cx;
	  }

      cy = this->cy;
	  if (cy == 0)
	  {
//		  cy = (int) (double) ((double) tm.tmHeight * 1.3);
		  cy = tm.tmHeight + fspaceh;
	  }
	  else if (Pixel == FALSE)
	  {
		  cy *= tm.tmHeight;
	  }

      SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  if (this->x == -1)
	  {
		         x = max (0, (rect.right - cx) / 2);
	  }
	  else if (Pixel == FALSE)
	  {
//                 x = this->x * tm.tmAveCharWidth;
                 x = this->x * size.cx;
	  }
	  else
	  {
		         x = this->x;
	  }

	  if (this->y == -1 && this->cy == 0)
	  {
//		         y = max (0, (rect.bottom - (int) (double) 
//					         ((double) tm.tmHeight * 1.3) / 2));
		         y = max (0, (rect.bottom - (tm.tmHeight + fspace)) / 2);
	  }

	  if (this->y == -1 && this->cy && Pixel == FALSE)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * this->cy) / 2));
	  }
	  else if (this->y == -1 && this->cy && Pixel)
	  {
		         y = max (0, (rect.bottom - this->cy) / 2);
	  }

	  else if (Pixel == FALSE)
	  {
//                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
                 y = this->y * (tm.tmHeight + fspace);
	  }
	  else
	  {
                 y = (int) (double) ((double) this->y);
	  }


      x += xstart;
	  y += start;
	  xp = x;
	  yp = y;
	  cxp = cx;
	  cyp = cy;
	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);
	  bustyle = BkMode;
	  ToFormat ();
      hMainInst = (HANDLE) GetWindowLong (MainWindow, GWL_HINSTANCE);
      if (strchr (picture, 'f') != NULL ||
          strchr (picture, 'd') != NULL && strchr (picture, 'm') == NULL)
      {
          bustyle;
      }
      hWnd = CreateWindowEx (BkModeEx,
		                    "EDIT",
                           "",
						   WS_CHILD | WS_VISIBLE | bustyle,
                           x, y,
                           cx, cy,
                           MainWindow,
                           (HMENU) ID,
                           hMainInst,
                           NULL);
	  if (UpDown)
	  {
	            GetClientRect (hWnd, &rect); 
				frx = GetSystemMetrics (SM_CXEDGE);
				fry = GetSystemMetrics (SM_CYEDGE);
//                x += rect.right - size.cx - frx;
                x += cx;
                cx = size.cx;
                cy = rect.bottom;

                hWndcl = CreateUpDownControl
                                       (
                                        WS_CHILD | WS_VISIBLE | 
										ES_RIGHT | ES_MULTILINE |
										UDS_SETBUDDYINT,
                                        x,  y + fry,
                                        cx, cy,
                                        MainWindow,
                                        ID + 1000,
                                        hMainInst,
                                        hWnd,
                                        UpMax,
                                        UpMin,
                                        UpValue);
	  }

      ::SendMessage (hWnd,WM_SETFONT, (WPARAM) hFont, 0);
//      if (BkMode & DISABLED) Disabled = TRUE;
      if (Disabled)
      {
                        EnableWindow (hWnd, FALSE);
                        Disabled = TRUE;
      }
      else
      {
                        EnableWindow (hWnd, TRUE);
                        Disabled = FALSE;
      }
	  SetRDOMargins (::SendMessage (hWnd, EM_GETMARGINS, 0, 0l));
      SetText ();
}

void CFIELD::displayredit (HWND MainWindow,HDC hdc)
{
	  TEXTMETRIC tm;
	  int x, y;
	  int cx, cy;
	  SIZE size;
      DWORD bustyle;
	  RECT rect;
	  HANDLE hMainInst;

      if (hWnd) 
      {
          InvalidateRect (hWnd, NULL, FALSE);
          UpdateWindow (hWnd);
          return;
      }
      else
      {
          hFont = NULL;
          PoshFont = NULL;
      }

	  if (hFont == NULL && mFont != NULL)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else if (hFont == NULL)
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PoshFont == NULL && PosmFont != NULL)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }

	  GetClientRect (MainWindow, &rect);
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

	  cx = this->cx;
	  if (cx == 0)
	  {
              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			  cx = size.cx;
	  }
	  else if (Pixel == FALSE)
	  {
              GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		      cx *= size.cx;
	  }

      cy = this->cy;
	  if (cy == 0)
	  {
//		  cy = (int) (double) ((double) tm.tmHeight * 1.3);
		  cy = tm.tmHeight + fspaceh;
	  }
	  else if (Pixel == FALSE)
	  {
		  cy *= tm.tmHeight;
	  }

      SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  if (this->x == -1)
	  {
		         x = max (0, (rect.right - cx) / 2);
	  }
	  else if (Pixel == FALSE)
	  {
                 x = this->x * size.cx;
	  }
	  else
	  {
		         x = this->x;
	  }

	  if (this->y == -1 && this->cy == 0)
	  {
//		         y = max (0, (rect.bottom - (int) (double) 
//					         ((double) tm.tmHeight * 1.3) / 2));
		         y = max (0, (rect.bottom - (tm.tmHeight + fspace)) / 2);
	  }

	  if (this->y == -1 && this->cy && Pixel == FALSE)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * this->cy) / 2));
	  }
	  else if (this->y == -1 && this->cy && Pixel)
	  {
		         y = max (0, (rect.bottom - this->cy) / 2);
	  }

	  else if (Pixel == FALSE)
	  {
//                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
                 y = this->y * (tm.tmHeight + fspace);
	  }
	  else
	  {
                 y = (int) (double) ((double) this->y);
	  }


      x += xstart;
	  y += start;
	  xp = x;
	  yp = y;
	  cxp = cx;
	  cyp = cy;
	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);
	  bustyle = BkMode;
      hMainInst = (HANDLE) GetWindowLong (MainWindow, GWL_HINSTANCE);
      hWnd = CreateWindowEx (WS_EX_CLIENTEDGE,
		                    "RichEdit",
                           (char *) feld,
                           WS_CHILD | WS_VISIBLE | bustyle,
                           x, y,
                           cx, cy,
                           MainWindow,
                           (HMENU) ID,
                           hMainInst,
                           NULL);

      ::SendMessage (hWnd,WM_SETFONT, (WPARAM) hFont, 0);
      SetText ();
}

void CFIELD::displaycombobox (HWND MainWindow,HDC hdc)
{
	  TEXTMETRIC tm;
	  int x, y;
	  int cx, cy;
	  SIZE size;
      DWORD bustyle;
	  RECT rect;
	  HANDLE hMainInst;

      if (hWnd) 
      {
          InvalidateRect (hWnd, NULL, FALSE);
          UpdateWindow (hWnd);
          return;
      }

      else
      {
          hFont = NULL;
          PoshFont = NULL;
      }

	  if (hFont == NULL && mFont != NULL)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else if (hFont == NULL)
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PoshFont == NULL && PosmFont != NULL)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }
	  GetClientRect (MainWindow, &rect);
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  cx = this->cx;
	  if (cx == 0)
	  {
              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			  cx = size.cx;
	  }
	  else if (Pixel == FALSE)
	  {
		      cx *= size.cx;
	  }

      cy = this->cy;
	  if (cy == 0)
	  {
//		  cy = (int) (double) ((double) tm.tmHeight * 1.3);
		  cy = tm.tmHeight * fspaceh;
	  }
	  else if (Pixel == FALSE)
	  {
		  cy *= tm.tmHeight;
	  }

      SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  if (this->x == -1)
	  {
		         x = max (0, (rect.right - cx) / 2);
	  }
	  else if (Pixel == FALSE)
	  {
//                 x = this->x * tm.tmAveCharWidth;
                 x = this->x * size.cx;
	  }
	  else
	  {
		         x = this->x;
	  }

	  if (this->y == -1 && this->cy == 0)
	  {
//		         y = max (0, (rect.bottom - (int) (double) 
//					         ((double) tm.tmHeight * 1.3) / 2));
		         y = max (0, (rect.bottom - (tm.tmHeight + fspace)) / 2);
	  }

	  if (this->y == -1 && this->cy && Pixel == FALSE)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * this->cy) / 2));
	  }
	  else if (this->y == -1 && this->cy && Pixel)
	  {
		         y = max (0, (rect.bottom - this->cy) / 2);
	  }

	  else if (Pixel == FALSE)
	  {
//                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
                 y = this->y * (tm.tmHeight + fspace);
	  }
	  else
	  {
                 y = (int) (double) ((double) this->y);
	  }


      x += xstart;
	  y += start;
	  xp = x;
	  yp = y;
//      cx += GetSystemMetrics (SM_CXVSCROLL) + 5;
	  cxp = cx;
	  cyp = cy;
	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);
	  bustyle = BkMode;
	  ToFormat ();
      hMainInst = (HANDLE) GetWindowLong (MainWindow, GWL_HINSTANCE);
      hWnd = CreateWindowEx (WS_EX_CLIENTEDGE,
		                    "COMBOBOX",
                           "",
						   WS_CHILD | WS_VISIBLE | bustyle,
                           x, y,
                           cx, cy,
                           MainWindow,
                           (HMENU) ID,
                           hMainInst,
                           NULL);

      FillComboBox ();

      ::SendMessage (hWnd,WM_SETFONT, (WPARAM) hFont, 0);
      if (Disabled)
      {
                        EnableWindow (hWnd, FALSE);
                        Disabled = TRUE;
      }
      else
      {
                        EnableWindow (hWnd, TRUE);
                        Disabled = FALSE;
      }
      SetText ();
}

void CFIELD::displaycolbu (HWND MainWindow,HDC hdc)
{
	  TEXTMETRIC tm;
	  ColButton *Cub;
	  int x, y;
	  int cx, cy;
	  SIZE size;
      DWORD bustyle;
	  RECT rect;
	  HANDLE hMainInst;

      if (hWnd) 
      {
          InvalidateRect (hWnd, NULL, TRUE);
          UpdateWindow (hWnd);
          return;
      }

      else
      {
          hFont = NULL;
          PoshFont = NULL;
      }

	  if (hFont == NULL && mFont != NULL)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else if (hFont == NULL)
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PoshFont == NULL && PosmFont != NULL)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }
	  GetClientRect (MainWindow, &rect);
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

	  cx = this->cx;
	  if (cx == 0)
	  {
              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			  cx = size.cx;
	  }
	  else if (Pixel == FALSE)
	  {
              GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		      cx *= size.cx;
	  }

      cy = this->cy;
	  if (cy == 0)
	  {
//		  cy = (int) (double) ((double) tm.tmHeight * 1.3);
		  cy = tm.tmHeight + fspaceh;
	  }
	  else if (Pixel == FALSE)
	  {
		  cy *= tm.tmHeight;
	  }

      SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  if (this->x == -1)
	  {
		         x = max (0, (rect.right - cx) / 2);
	  }
	  else if (Pixel == FALSE)
	  {
                 x = this->x * size.cx;
	  }
	  else
	  {
		         x = this->x;
	  }

	  if (this->y == -1 && this->cy == 0)
	  {
//		         y = max (0, (rect.bottom - (int) (double) 
//					         ((double) tm.tmHeight * 1.3) / 2));
		         y = max (0, (rect.bottom - (tm.tmHeight + fspace)) / 2);
	  }

	  if (this->y == -1 && this->cy && Pixel == FALSE)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * this->cy) / 2));
	  }
	  else if (this->y == -1 && this->cy && Pixel)
	  {
		         y = max (0, (rect.bottom - this->cy) / 2);
	  }

	  else if (Pixel == FALSE)
	  {
//                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
                 y = this->y * (tm.tmHeight + fspace);
	  }
	  else
	  {
                 y = (int) (double) ((double) this->y);
	  }


	  bustyle = BkMode;
      hMainInst = (HANDLE) GetWindowLong (MainWindow, GWL_HINSTANCE);

      x += xstart;
	  y += start;
      y += ypplus;
	  xp = x;
	  yp = y;
	  cxp = cx;
	  cyp = cy;
	  Cub = (ColButton *) feld;
	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);

	  hWnd = CreateColButton (MainWindow, Cub, x, y, cx, cy, hFont, hMainInst, ID); 

      ::SendMessage (hWnd,WM_SETFONT, (WPARAM) hFont, 0);
//      if (BkMode & DISABLED) Disabled = TRUE;
      if (Disabled)
      {
                        EnableWindow (hWnd, FALSE);
      }
      else
      {
                        EnableWindow (hWnd, TRUE);
      }
}

void CFIELD::displayrdonly (HWND MainWindow,HDC hdc)
{
	  TEXTMETRIC tm;
	  int x, y;
	  int cx, cy;
	  SIZE size;
      DWORD bustyle;
	  RECT rect;
	  HANDLE hMainInst;
	  int lmargin = 6;
	  int rmargin = 5;

      if (hWnd) 
      {
          InvalidateRect (hWnd, NULL, TRUE);
          UpdateWindow (hWnd);
          return;
      }

      else
      {
          hFont = NULL;
          PoshFont = NULL;
      }

	  if (hFont == NULL && mFont != NULL)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else if (hFont == NULL)
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PoshFont == NULL && PosmFont != NULL)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }
/*
	  if (mFont)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PosmFont)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }
*/
	  GetClientRect (MainWindow, &rect);
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

	  ROSetBkColor (mFont->FontBkColor);
	  ROSetAktBack (mFont->FontBkColor);
	  ROSetAktVor (mFont->FontColor);
	  cx = this->cx;
	  if (cx == 0)
	  {

              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			  cx = size.cx;
	  }
	  else if (Pixel == FALSE)
	  {
              GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		      cx *= size.cx;
	  }

      cy = this->cy;
	  if (cy == 0)
	  {
//		  cy = (int) (double) ((double) tm.tmHeight * 1.3);
		  cy = tm.tmHeight + fspaceh;
	  }
	  else if (Pixel == FALSE)
	  {
		  cy *= tm.tmHeight;
	  }

      SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  if (this->x == -1)
	  {
		         x = max (0, (rect.right - cx) / 2);
	  }
	  else if (Pixel == FALSE)
	  {
                 x = this->x * size.cx;
	  }
	  else
	  {
		         x = this->x;
	  }

	  if (this->y == -1 && this->cy == 0)
	  {
//		         y = max (0, (rect.bottom - (int) (double) 
//					         ((double) tm.tmHeight * 1.3) / 2));
		         y = max (0, (rect.bottom - (tm.tmHeight + fspace)) / 2);
	  }

	  if (this->y == -1 && this->cy && Pixel == FALSE)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * this->cy) / 2));
	  }
	  else if (this->y == -1 && this->cy && Pixel)
	  {
		         y = max (0, (rect.bottom - this->cy) / 2);
	  }

	  else if (Pixel == FALSE)
	  {
//                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
                 y = this->y * (tm.tmHeight + fspace);
	  }
	  else
	  {
                 y = (int) (double) ((double) this->y);
	  }

	  bustyle = BkMode;
	  ToFormat ();
      hMainInst = (HANDLE) GetWindowLong (MainWindow, GWL_HINSTANCE);

      x += xstart;
	  y += start;
	  xp = x;
	  yp = y;
	  cxp = cx;
	  cyp = cy;
	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);
	  if (BkMode & TRANSPARENT)
	  {
	       hWnd = CreateRdOnlyEx (MainWindow, (char *) feld, x, y, cx, cy, 
			                      hFont, hMainInst, ID,
								  NULL, BkMode); 
	  }
	  else
	  {
//	       hWnd = CreateRdOnly (MainWindow, (char *) feld, x, y, cx, cy, hFont, hMainInst, ID); 
	       hWnd = CreateRdOnlyEx (MainWindow, (char *) feld, x, y, cx, cy, 
			                      hFont, hMainInst, ID,
								  WS_EX_CLIENTEDGE, BkMode); 
	  }
	  GetClientRect (hWnd, &rect);
	  cx = rect.right;
	  cxp = cx;
	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);
      ::SendMessage (hWnd,WM_SETFONT, (WPARAM) hFont, 0);
}

void CFIELD::displaycp (HWND MainWindow,HDC hdc)
{
	  TEXTMETRIC tm;
	  int x, y;
	  int cx, cy;
	  SIZE size;
	  RECT rect;
	  HANDLE hMainInst;

      if (hWnd) 
      {
          InvalidateRect (hWnd, NULL, FALSE);
          UpdateWindow (hWnd);
          return;
      }

      else
      {
          hFont = NULL;
          PoshFont = NULL;
      }

	  if (hFont == NULL && mFont != NULL)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else if (hFont == NULL)
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PoshFont == NULL && PosmFont != NULL)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }

	  GetClientRect (MainWindow, &rect);
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

	  cx = this->cx;
	  if (cx == 0)
	  {
              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			  cx = size.cx;
              GetTextExtentPoint32 (hdc, "X", 1 , &size);
			  cx += 3 * size.cx;
	  }
	  else if (Pixel == FALSE)
	  {
              GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		      cx *= size.cx;
	  }

      cy = this->cy;
	  if (cy == 0)
	  {
//		  cy = (int) (double) ((double) tm.tmHeight * 1.3);
		  cy = tm.tmHeight + fspaceh;
	  }
	  else if (Pixel == FALSE)
	  {
//		  cy = (int) (double) ((double) cy * tm.tmHeight * 1.3);
		  cy = cy * (tm.tmHeight + fspace);
	  }

      SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  if (this->x == -1)
	  {
		         x = max (0, (rect.right - cx) / 2);
	  }
	  else if (Pixel == FALSE)
	  {
                 x = this->x * size.cx;
	  }
	  else
	  {
		         x = this->x;
	  }

	  if (this->y == -1 && this->cy == 0)
	  {
//		         y = max (0, (rect.bottom - (int) (double) 
//					         ((double) tm.tmHeight * 1.3) / 2));
		         y = max (0, (rect.bottom - (tm.tmHeight + fspace)) / 2);
	  }

	  if (this->y == -1 && this->cy && Pixel == FALSE)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * this->cy) / 2));
	  }
	  else if (this->y == -1 && this->cy && Pixel)
	  {
		         y = max (0, (rect.bottom - this->cy) / 2);
	  }

	  else if (Pixel == FALSE)
	  {
//                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
                 y = this->y * (tm.tmHeight + fspace);
	  }
	  else
	  {
                 y = (int) (double) ((double) this->y);
	  }


      x += xstart;
	  y += start;
	  xp = x;
	  yp = y;
	  cxp = cx;
	  cyp = cy;
	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);
      hMainInst = (HANDLE) GetWindowLong (MainWindow, GWL_HINSTANCE);
      hWnd = CreateWindow ("Button",
                           (char *) feld,
                           WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP,
                           x, y,
                           cx, cy,
                           MainWindow,
                           (HMENU) ID,
                           hMainInst,
                           NULL);

      ::SendMessage (hWnd,WM_SETFONT, (WPARAM) hFont, 0);
//      if (BkMode & DISABLED) Disabled = TRUE;
      if (Disabled)
      {
                        EnableWindow (hWnd, FALSE);
      }
      else
      {
                        EnableWindow (hWnd, TRUE);
      }
}

void CFIELD::displaylbox (HWND MainWindow,HDC hdc)
{
	  TEXTMETRIC tm;
	  int x, y;
	  int cx, cy;
	  SIZE size;
	  RECT rect;
	  HANDLE hMainInst;

      if (hWnd) 
      {
          InvalidateRect (hWnd, NULL, FALSE);
          UpdateWindow (hWnd);
          return;
      }

      else
      {
          hFont = NULL;
          PoshFont = NULL;
      }

	  if (hFont == NULL && mFont != NULL)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else if (hFont == NULL)
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PoshFont == NULL && PosmFont != NULL)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }

	  GetClientRect (MainWindow, &rect);
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

	  cx = this->cx;
	  if (cx == 0)
	  {
              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			  cx = size.cx;
              GetTextExtentPoint32 (hdc, "X", 1 , &size);
			  cx += 3 * size.cx;
	  }
	  else if (Pixel == FALSE)
	  {
              GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		      cx *= size.cx;
	  }

      cy = this->cy;
      
	  if (cy == 0)
	  {
//		  cy = (int) (double) ((double) tm.tmHeight * 1.3);
		  cy = tm.tmHeight + fspaceh;
	  }
	  else if (Pixel == FALSE)
	  {
		  cy *= tm.tmHeight;
	  }

      SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
	  if (this->x == -1)
	  {
		         x = max (0, (rect.right - cx) / 2);
	  }
	  else if (Pixel == FALSE)
	  {
                 x = this->x * size.cx;
	  }
	  else
	  {
		         x = this->x;
	  }

	  if (this->y == -1 && this->cy == 0)
	  {
//		         y = max (0, (rect.bottom - (int) (double) 
//					         ((double) tm.tmHeight * 1.3) / 2));
		         y = max (0, (rect.bottom - (tm.tmHeight + fspace)) / 2);
	  }

	  if (this->y == -1 && this->cy && Pixel == FALSE)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * this->cy) / 2));
	  }
	  else if (this->y == -1 && this->cy && Pixel)
	  {
		         y = max (0, (rect.bottom - this->cy) / 2);
	  }

	  else if (Pixel == FALSE)
	  {
//                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
                 y = this->y * (tm.tmHeight + fspace);
	  }
	  else
	  {
                 y = (int) (double) ((double) this->y);
	  }


      cy -= 1;
      x += xstart;
	  y += start;
	  xp = x;
	  yp = y;
	  cxp = cx;
	  cyp = cy;
      cy += GetSystemMetrics (SM_CYHSCROLL) / 2;
	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);
	  ::SetImage (Image);
      SetListFontBefore (hFont);
      RegisterLboxM (MainWindow);  
	  hMainInst = (HANDLE) GetWindowLong (MainWindow, GWL_HINSTANCE);
      if (beforecreate)
      {
          (*beforecreate) ();
      }

      ::EnableSort (SortEnable);
      if (sortproc)
      {
          SetSortProc (sortproc);
      }
      hWnd  = CreateWindowEx (
                           WS_EX_CLIENTEDGE, 
		                   "lboxm",
                           (char *) feld,
                           WS_CHILD,
                           x, y,
                           cx, cy,
                           MainWindow,
                           (HMENU) ID,
                           hMainInst,
                           NULL);

      ::SendMessage (hWnd,WM_SETFONT, (WPARAM) hFont, 0);
      if (RowAttr != NULL)
      {
          ::SendMessage (hWnd, LB_ROWATTR, 0,  
                                 (LPARAM) (char *) RowAttr);
      }
      ShowWindow (hWnd, SW_SHOWNORMAL);
      UpdateWindow (hWnd);
      SelectObject (hdc, oldfont);
	  DeleteObject (hFont);
      if (PoshFont != hFont) DeleteObject (PoshFont);
}


void CFIELD::displaytxt (HWND MainWindow, HDC hdc)
{
	  HFONT hFont, oldfont;
	  RECT rect;
	  TEXTMETRIC tm;
	  int x, y, cx, cy;
	  SIZE size;

	  if (strcmp (name, "kalk_gr_txt") == 0)
	  {
              hWndMain = MainWindow;
	  }
      hWndMain = MainWindow;
	  if (mFont)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  if (PosmFont)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }

	  GetClientRect (MainWindow, &rect);
	  oldfont = SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 

      x  = this->x;
      y  = this->y;
	  cx = this->cx;
	  cy = this->cy;
      if (PosSet == FALSE)
      {
   	     this->Pixel = Pixelorg;
         this->x = this->xorg;
         this->y = this->yorg;
         this->cx = this->cxorg;
         this->cy = this->cyorg;

	     if (cx == 0)
         {
			  cx = strlen ((char *) feld);
         }

	     if (this->xorg == -1)
         {
                 GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
		         x = max (0, (rect.right - size.cx) / 2);
                 GetTextExtentPoint32 (hdc, "X", 1 , &size); 
         }
	     else if (Pixel == FALSE) 
         {
                 GetTextExtentPoint32 (hdc, "X", 1 , &size); 
                 x = this->x * size.cx;
                 cx = this->cxorg * size.cx;
         }
	     else
         {
		         x = this->x;
         }

	     if (this->yorg == -1 && this->cy == 0)
         {
//		         y = max (0, (rect.bottom - (int) (double) 
//					         ((double) tm.tmHeight * 1.3)) / 2);
		         y = max (0, (rect.bottom - (tm.tmHeight + fspace)) / 2);
         }

	     else if (this->yorg == -1 && this->cy && Pixel == FALSE)
         {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * cy)) / 2);
                 cy = this->cyorg * tm.tmHeight;
         }
	     else if (this->yorg == -1 && this->cy && Pixel)
         {
		         y = max (0, (rect.bottom - this->cy) / 2);
         }
	     else if (Pixel == FALSE)
         {
//                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
                 y = this->y * (tm.tmHeight + fspace);
                 cy = this->cyorg * tm.tmHeight;
         }
	     else
         {
                 y = (int) (double) ((double) this->y);
         }

         this->x = x;
         this->y = y;
         this->cx = cx;
         this->cy = cy;
         PosSet = TRUE;
      }


      x += xstart;
      y += start;

      ::SetBkMode (hdc, BkMode);

	  SelectObject (hdc, hFont);
	  xp = x;
	  yp = y;
	  cxp = cx;
	  cyp = cy;
 	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);

       if (Shadow)
      { 
           SetTextColor (hdc,ShadowColor);
           TextOut (hdc, x + shx, y + shy, (char *) feld, strlen ((char *) feld));
      }

 	  if (mFont)
	  {
                  SetTextColor (hdc,mFont->FontColor);
                  SetBkColor (hdc,mFont->FontBkColor);
	  }
	  else
	  {
                  SetTextColor (hdc,BLACKCOL);

	  }
      Pixel = TRUE;
      TextOut (hdc, x, y, (char *) feld, strlen ((char *) feld));
      SelectObject (hdc, oldfont);
	  DeleteObject (hFont);
	  DeleteObject (PoshFont);

}

void CFIELD::displayborder (HWND MainWindow, HDC hdc)
{
	  HFONT hFont, oldfont;
	  RECT rect;
	  TEXTMETRIC tm;
	  int x, y, cx, cy;
	  SIZE size;

      if (mFont)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else
	  {
		          hFont = CreateStdFont (NULL);
	  }
	  
	  if (PosmFont)
	  {
                  PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
		          PoshFont = hFont;
	  }

	  GetClientRect (MainWindow, &rect);
	  oldfont = SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size); 

      x  = this->x;
      y  = this->y;
	  cx = this->cx;
	  cy = this->cy;
      if (PosSet == FALSE)
      {
	     if (cx == 0)
         {
			  cx = strlen ((char *) feld);
         }

	     if (this->xorg == -1)
         {
                 GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
		         x = max (0, (rect.right - size.cx) / 2);
                 GetTextExtentPoint32 (hdc, "X", 1 , &size); 
         }
	     else if (Pixel == FALSE) 
         {
                 GetTextExtentPoint32 (hdc, "X", 1 , &size); 
                 x = this->x * size.cx;
                 cx = this->cxorg * size.cx;
         }
	     else
         {
		         x = this->x;
         }

	     if (this->yorg == -1 && this->cy == 0)
         {
//		         y = max (0, (rect.bottom - (int) (double) 
//					         ((double) tm.tmHeight * 1.3)) / 2);
		         y = max (0, (rect.bottom - (tm.tmHeight + fspace)) / 2);
         }

	     else if (this->yorg == -1 && this->cy && Pixel == FALSE)
         {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * cy)) / 2);
                 cy = this->cyorg * tm.tmHeight;
         }
	     else if (this->yorg == -1 && this->cy && Pixel)
         {
		         y = max (0, (rect.bottom - this->cy) / 2);
         }
	     else if (Pixel == FALSE)
         {
//                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
                 y = this->y * (tm.tmHeight + fspace);
                 cy = this->cyorg * tm.tmHeight;
         }
	     else
         {
                 y = (int) (double) ((double) this->y);
         }

         this->x = x;
         this->y = y;
         this->cx = cx;
         this->cy = cy;
         PosSet = TRUE;
      }


      if (strcmp (name, "Line1") == 0)
      {
          y = y;
      }
      x += xstart;
      y += start;
      if (Border != NULL)
      {
                  Border->Print (MainWindow, hdc, x,y, cx, cy);
      }

      ::SetBkMode (hdc, BkMode);
	  if (mFont)
	  {
                  SetTextColor (hdc,mFont->FontColor);
                  SetBkColor (hdc,mFont->FontBkColor);
	  }
	  else
	  {
                  SetTextColor (hdc,BLACKCOL);

	  }

	  SelectObject (hdc, hFont);
	  xp = x;
	  yp = y;
	  cxp = cx;
	  cyp = cy;
	  Chs.SetValues (x, y, cx, cy);
	  Chs.SetSize (MainWindow);

      x += size.cx;
      if (BkMode == OPAQUE)
      {
           y -= tm.tmHeight / 2;
      }

      TextOut (hdc, x, y, (char *) feld, strlen ((char *) feld));
      SelectObject (hdc, oldfont);
	  DeleteObject (hFont);
	  DeleteObject (PoshFont);
}

int CFIELD::dobefore (DWORD ID)
{
	  CFORM *cform;
	  
	  if (this->ID == ID)
	  {
	       return this->before ();
	  }
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  return cform->before (ID);
	  }
	  return -1;
}

int CFIELD::dobefore (char *name)
{
	  CFORM *cform;
	  
	  if (strcmp (this->name, name) == 0)
	  {
	       return this->before ();
	  }
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  return cform->before (name);
	  }
	  return -1;
}

int CFIELD::doafter (DWORD ID)
{
	  CFORM *cform;
	  
	  if (this->ID == ID)
	  {
	       return this->after ();
	  }
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  return cform->after (ID);
	  }
	  return -1;
}

int CFIELD::doafter (char *name)
{
	  CFORM *cform;
	  
	  if (strcmp (this->name, name) == 0)
	  {
	       return this->after ();
	  }
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  return cform->after (name);
	  }
	  return -1;
}

HWND CFIELD::GethWndID (DWORD ID)
{
	  CFORM *cform;

	  if (this->ID == ID) 
	  {
		  return hWnd;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (hWnd = cform->GethWndID (ID)) return hWnd;
	  }
	  return NULL;
}


HWND CFIELD::GethWndName (char *name)
{
	  CFORM *cform;

	  if (strcmp (this->name, name) == 0) 
	  {
		  return hWnd;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (hWnd = cform->GethWndName (name)) return hWnd;
	  }
	  return NULL;
}

BOOL CFIELD::IsID (DWORD ID)
{
	  CFORM *cform;

	  if (this->ID == ID) 
	  {
		  return TRUE;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (cform->IsID (ID)) return TRUE;
	  }
	  return FALSE;
}

CFIELD *CFIELD::IsCfield (DWORD ID)
{
	  CFORM *cform;
	  CFIELD *field;

	  if (this->ID == ID) 
	  {
		  return this;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (field = cform->GetCfield (ID)) 
		  {
			  return field;
		  }
	  }
	  return NULL;
}

CFIELD *CFIELD::IsCfield (char *name)
{
	  CFORM *cform;
	  CFIELD *field;

	  if (strcmp (this->name, name) == 0) 
	  {
		  return this;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (field = cform->GetCfield (name)) 
		  {
			  return field;
		  }
	  }
	  return NULL;
}

DWORD CFIELD::GetAttrhWnd (HWND hWnd)
{
	  CFORM *cform;
      HWND hWndParent;
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  return cform->GetAttribut ();
	  }
	  if (this->hWnd == hWnd)
	  {
		  return attribut;
	  }
      hWndParent = GetParent (hWnd);
      if (this->hWnd == hWndParent)
	  {
		  return attribut;
	  }
	  return NOATTRIBUT;
}

char *CFIELD::IsCurrentID (DWORD ID)
{
	  CFORM *cform;
	  char *name;

	  if (this->ID == ID) 
	  {
		  return name;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (name = cform->GetName (ID)) return name;
	  }
	  return NULL;
}


DWORD CFIELD::IsCurrentName (char *name)
{
	  CFORM *cform;
	  DWORD ID;

	  if (strcmp (this->name, name) == 0) 
	  {
		  return this->ID;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (ID = cform->GetID (name)) return ID;
	  }
	  return 0;
}

DWORD CFIELD::IsCurrenthWnd (HWND hWnd)
{
	  CFORM *cform;
	  DWORD ID;

	  if (this->hWnd == hWnd) 
	  {
		  return this->ID;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (ID = cform->GetID (hWnd)) return ID;
	  }
	  return 0;
}

BOOL CFIELD::SetFocusID (DWORD ID)
{
	  CFORM *cform;

	  if (this->ID == ID) 
	  {
          if (hWnd != 0)
          {
		          SetFocus ();
          }
		  return TRUE;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (cform->SetCurrentID (ID)) return TRUE;
	  }
	  return FALSE;
}

void CFIELD::SetFieldPicture (char *name, char *picture)
{
	  CFORM *cform;

	  if (strcmp (this->name, name) == 0) 
	  {
		  SetPicture (picture);
		  return;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  cform->SetFieldPicture (name, picture);
	  }
}

void CFIELD::SetFieldBkMode (char *name, int BkMode)
{
	  CFORM *cform;

	  if (strcmp (this->name, name) == 0) 
	  {
		  SetBkMode (BkMode);
		  return;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  cform->SetFieldBkMode (name, BkMode);
	  }
}


BOOL CFIELD::SetFocusName (char *name)
{
	  CFORM *cform;

	  if (strcmp (this->name, name) == 0) 
	  {
          if (hWnd != 0)
          {
   		         SetFocus ();
          }
		  return TRUE;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (cform->SetCurrentName (name)) return TRUE;
	  }
	  return FALSE;
}


void CFIELD::SetFocus (void)
{
	  DWORD ID;
      HDC hdc;
	  CFORM *cform;

	  if (attribut == CFFORM)
	  {
	          cform = (CFORM *) feld;
			  ID = cform->GetFirstID ();
              cform->SetCurrentID (ID); 
			  return;
	  }
      if (hWnd != 0)
      {
             ::SetFocus (hWnd);
      }
      else
      {
              hdc = GetDC (hWndMain);
              display (hWndMain, hdc);
              ReleaseDC (hWndMain, hdc);

      }
	  if (attribut == CEDIT && (BkMode & ES_MULTILINE) == 0)
	  {
          ::SendMessage (hWnd, EM_SETSEL, (WPARAM) 0, (LPARAM) -1);
	  }

	  if (attribut == CCOMBOBOX)
	  {
          ::SendMessage (hWnd, CB_SETEDITSEL, (WPARAM) 0, MAKELPARAM (0, -1));
	  }

	  if (attribut == CLISTBOX)
	  {
          ::SendMessage (hWnd, CB_SETEDITSEL, (WPARAM) 0, MAKELONG (-1, 0));
	  }
}


void CFIELD::SetSel (void)
{
	  CFORM *cform;

	  if (attribut == CFFORM)
	  {
	          cform = (CFORM *) feld;
              cform->SetSel (); 
			  return;
	  }

	  if (attribut == CEDIT && (BkMode & ES_MULTILINE) == 0)
	  {
          ::SendMessage (hWnd, EM_SETSEL, (WPARAM) 0, (LPARAM) -1);
	  }

	  if (attribut == CCOMBOBOX)
	  {
          ::SendMessage (hWnd, CB_SETEDITSEL, (WPARAM) 0, MAKELPARAM (-1, 0));
	  }

}

void CFIELD::Update (void)
{
	  CFORM *cform;

	  if (attribut == CFFORM)
	  {
		      cform = (CFORM *) feld;
              cform->Update (); 
			  return;
	  }
	  if (hWnd) 
	  {
          if (attribut == CREADONLY)
		  {
 	                ToFormat ();
		  }
		  InvalidateRect (hWnd, NULL, TRUE);
		  UpdateWindow (hWnd);
	  }
}
	   


BOOL CFIELD::EnableID (DWORD ID, BOOL flag)
{
	  CFORM *cform;

	  if (this->ID == ID) 
	  {
		  Enable (flag);
		  return TRUE;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (cform->EnableID (ID, flag)) return TRUE;
	  }
	  return FALSE;
}


BOOL CFIELD::EnableName (char *name, BOOL flag)
{
	  CFORM *cform;

	  if (strcmp (this->name, name) == 0) 
	  {
		  Enable (flag);
		  return TRUE;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (cform->EnableName (name, flag)) return TRUE;
	  }
	  return FALSE;
}


void CFIELD::Enable (BOOL flag)
{
     if (hWnd)
     {
	      EnableWindow (hWnd, flag);
     }
     Disabled = !flag;

	 switch (attribut)
	 {
		 case CCOLBUTTON :
               ActivateCBNoBorder ((ColButton *) feld, flag);
	     case CEDIT :
         case CRIEDIT :
		 case CBUTTON :
		 case CLISTBOX :
  	     case CCOMBOBOX :
               tabstop = flag;
               if (tabstopex == FALSE)
               {
                   tabstop = FALSE;
               }
	}

}

BOOL CFIELD::CheckID (DWORD ID, BOOL flag)
{
	  CFORM *cform;

	  if (this->ID == ID) 
	  {
		  Check (flag);
		  return TRUE;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (cform->CheckID (ID, flag)) return TRUE;
	  }
	  return FALSE;
}


BOOL CFIELD::CheckName (char *name, BOOL flag)
{
	  CFORM *cform;

	  if (strcmp (this->name, name) == 0) 
	  {
		  Check (flag);
		  return TRUE;
	  }
	  
	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  if (cform->CheckName (name, flag)) return TRUE;
	  }
	  return FALSE;
}


void CFIELD::Check (BOOL flag)
{
//    ::SendMessage (hWnd,  BM_SETCHECK, (WPARAM) flag, 0l);
    ::PostMessage (hWnd,  BM_SETCHECK, (WPARAM) flag, 0l);
}


void CFIELD::enter (void)
{
	  return;
}

int CFIELD::GetNextPos (HWND pWnd)
{
	  HDC hdc;
	  TEXTMETRIC tm;
      HFONT hFont;
	  SIZE size;

	  if (Pixel == FALSE)
	  {
		    return -1;
	  }
	  hdc = GetDC (pWnd);
      if (PosmFont == NULL)
      {
            PosmFont = mFont;
      }
 	  if (mFont)
	  {
            hFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
	        hFont = CreateStdFont (NULL);
	  }
      oldfont = SelectObject (hdc, hFont);
      GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size);
	  DeleteObject (SelectObject (hdc, oldfont));
	  ReleaseDC (pWnd, hdc);
    
      return this->x + size.cx + 2; 	  
}


void CFIELD::SetAbsPos (void)
{
	  HDC hdc;
	  TEXTMETRIC tm;
      HFONT hFont;
	  SIZE size;
//  	  CFORM *cform;

	  if (AbsPos)
	  {
		  return;
	  }
/*
	  if (attribut == CFFORM)
      {
		cform = (CFORM *) feld;
		return cform->SetAbsPos ();
      }
*/

	  hdc = GetDC (hWnd);
      if (PosmFont == NULL)
      {
            PosmFont = mFont;
      }
 	  if (PosmFont)
	  {
            hFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
	        hFont = CreateStdFont (NULL);
	  }
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size);
	  DeleteObject (SelectObject (hdc, oldfont));
	  ReleaseDC (hWnd, hdc);

      if (x) this->x  = xorg;
	  if (y) this->y  = yorg;
      this->cx = cxorg;
	  this->cy = cyorg;
	  this->Pixel = Pixelorg;
      xset = yset = FALSE;
	  if (Pixel == FALSE)
	  {
		      if (this->x >= 0)
			  {
			         this->x = xorg * size.cx;
			  }
			  if (this->y >= 0)
			  {
//			          this->y = (int) (double) ((double) this->yorg * tm.tmHeight * 1.3);
			          this->y = this->yorg * (tm.tmHeight + fspace);
			  }
              if (this->cx == 0)
              {
                      GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
 			          cx = size.cx;
              }
              else
              {
			          this->cx *= size.cx;
              }
 	          if (cy == 0)
			  {
//		                cy = (int) (double) ((double) tm.tmHeight * 1.3);
		                cy = tm.tmHeight + fspaceh;
			  }
	          else
			  {
		                cy *= tm.tmHeight;
			  }
	  }
	  Pixel = TRUE;
	  AbsPos = TRUE;
      PosSet = TRUE;
      if (this->x >= 0)
	  {
 	           this->x += x;
	  }
	  if (this->y >= 0)
	  {
	           this->y += y;
	  }

      xp = x;
      yp = y;
      cxp = cx;
      cyp = cy;
	  Chs.SetValues (this->x, this->y, this->cx, this->cy);
}


void CFIELD::SetAbsPosX (int x)
{
	  HDC hdc;
	  TEXTMETRIC tm;
	  SIZE size;
	  SIZE ssize;
      HFONT hFont;
      HFONT PoshFont;
  	  CFORM *cform;

	  if (attribut == CFFORM)
      {
		cform = (CFORM *) feld;
		cform->SetAbsPosX (x);
        return;
      }


	  hdc = GetDC (hWnd);
      if (PosmFont == NULL)
      {
          PosmFont = mFont;
      }
 	  if (PosmFont)
	  {
            PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
	        PoshFont = CreateStdFont (NULL);
	  }
      if (attribut != CBORDER)
      {
 	      if (mFont)
          {
            hFont = SetDeviceFont (hdc, mFont, &tm);
            hFont = hFont;
          }
	      else
          {
	        hFont = CreateStdFont (NULL);
          }
      }
      oldfont = SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size);
	  DeleteObject (SelectObject (hdc, oldfont));
      GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &ssize); 

      if (x) this->x  = xorg;
      this->cx = cxorg;
	  this->Pixel = Pixelorg;
	  if (Pixel == FALSE)
	  {
		      if (this->x >= 0 && xset == FALSE)
			  {
			          this->x = xorg * size.cx;
			  }
              if (attribut != CBORDER)
              {
                       oldfont = SelectObject (hdc, hFont);
	                   GetTextMetrics (hdc, &tm);
                       GetTextExtentPoint32 (hdc, "X", 1 , &size);
	                   DeleteObject (SelectObject (hdc, oldfont));
                       GetTextExtentPoint32 (hdc, (char *) feld, 
                                      strlen ((char *) feld) , &ssize); 
              }
              if (this->cx == 0)
              {
 			          cx = ssize.cx;
              }
              else
              {
			          this->cx *= size.cx;
              }
	  }
	  ReleaseDC (hWnd, hdc);
	  Pixel = TRUE;
	  AbsPos = TRUE;
      PosSet = TRUE;
      if (this->x >= 0)
	  {
 	           this->x += x;
	  }

	  Chs.SetValues (this->x, this->y, this->cx, this->cy);
//	  Chs.SetSize (hWndMain);
}

void CFIELD::SetAbsPosY (int y)
{
	  HDC hdc;
	  TEXTMETRIC tm;
	  SIZE size;
	  SIZE ssize;
      HFONT hFont;
      HFONT PoshFont;
  	  CFORM *cform;

	  if (attribut == CFFORM)
      {
		cform = (CFORM *) feld;
		cform->SetAbsPosY (y);
        return;
      }


	  hdc = GetDC (hWnd);
      if (PosmFont == NULL)
      {
          PosmFont = mFont;
      }
 	  if (PosmFont)
	  {
            PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
	        PoshFont = CreateStdFont (NULL);
	  }
      if (attribut != CBORDER)
      {
 	      if (mFont)
          {
            hFont = SetDeviceFont (hdc, mFont, &tm);
            hFont = hFont;
          }
	      else
          {
	        hFont = CreateStdFont (NULL);
          }
      }
      oldfont = SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size);
	  DeleteObject (SelectObject (hdc, oldfont));
      GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &ssize); 

	  if (y) this->y  = yorg;
	  this->cy = cyorg;
	  this->Pixel = Pixelorg;
	  if (Pixel == FALSE)
	  {
			  if (this->y >= 0 && yset == FALSE)
			  {
			          this->y = this->yorg * (tm.tmHeight + fspace);
			  }
              if (attribut != CBORDER)
              {
                       oldfont = SelectObject (hdc, hFont);
	                   GetTextMetrics (hdc, &tm);
                       GetTextExtentPoint32 (hdc, "X", 1 , &size);
	                   DeleteObject (SelectObject (hdc, oldfont));
                       GetTextExtentPoint32 (hdc, (char *) feld, 
                                      strlen ((char *) feld) , &ssize); 
              }
 	          if (cy == 0)
			  {
//		                cy = (int) (double) ((double) tm.tmHeight * 1.3);
		                cy = tm.tmHeight + fspaceh;
			  }
	          else
			  {
		                cy *= tm.tmHeight;
			  }
	  }
	  ReleaseDC (hWnd, hdc);
	  Pixel = TRUE;
	  AbsPos = TRUE;
      PosSet = TRUE;
	  if (this->y >= 0)
	  {
	           this->y += y;
	  }

	  Chs.SetValues (this->x, this->y, this->cx, this->cy);
//	  Chs.SetSize (hWndMain);
}

void CFIELD::SetAbsPos (int x, int y)
{
	  HDC hdc;
	  TEXTMETRIC tm;
	  SIZE size;
	  SIZE ssize;
      HFONT hFont;
      HFONT PoshFont;
  	  CFORM *cform;

	  if (attribut == CFFORM)
      {
		cform = (CFORM *) feld;
		cform->SetAbsPos (x, y);
        return;
      }


	  hdc = GetDC (hWnd);
      if (PosmFont == NULL)
      {
          PosmFont = mFont;
      }
 	  if (PosmFont)
	  {
            PoshFont = SetDeviceFont (hdc, PosmFont, &tm);
	  }
	  else
	  {
	        PoshFont = CreateStdFont (NULL);
	  }
      if (attribut != CBORDER)
      {
 	      if (mFont)
          {
            hFont = SetDeviceFont (hdc, mFont, &tm);
            hFont = hFont;
          }
	      else
          {
	        hFont = CreateStdFont (NULL);
          }
      }
      oldfont = SelectObject (hdc, PoshFont);
	  GetTextMetrics (hdc, &tm);
      GetTextExtentPoint32 (hdc, "X", 1 , &size);
	  DeleteObject (SelectObject (hdc, oldfont));
      GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &ssize); 

      if (x) this->x  = xorg;
	  if (y) this->y  = yorg;
      this->cx = cxorg;
	  this->cy = cyorg;
	  this->Pixel = Pixelorg;
	  if (Pixel == FALSE)
	  {
		      if (this->x >= 0 && xset == FALSE)
			  {
			          this->x = xorg * size.cx;
			  }
			  if (this->y >= 0 && yset == FALSE)
			  {
			          this->y = this->yorg * (tm.tmHeight + fspace);
			  }
              if (attribut != CBORDER)
              {
                       oldfont = SelectObject (hdc, hFont);
	                   GetTextMetrics (hdc, &tm);
                       GetTextExtentPoint32 (hdc, "X", 1 , &size);
	                   DeleteObject (SelectObject (hdc, oldfont));
                       GetTextExtentPoint32 (hdc, (char *) feld, 
                                      strlen ((char *) feld) , &ssize); 
              }
              if (this->cx == 0)
              {
 			          cx = ssize.cx;
              }
              else
              {
			          this->cx *= size.cx;
              }
 	          if (cy == 0)
			  {
		                cy = tm.tmHeight + fspaceh;
			  }
	          else
			  {
		                cy *= tm.tmHeight;
			  }
	  }
	  ReleaseDC (hWnd, hdc);
	  Pixel = TRUE;
	  AbsPos = TRUE;
      PosSet = TRUE;
      if (this->x >= 0)
	  {
 	           this->x += x;
	  }
	  if (this->y >= 0)
	  {
	           this->y += y;
	  }

	  Chs.SetValues (this->x, this->y, this->cx, this->cy);
//	  Chs.SetSize (hWndMain);
}


DWORD CFIELD::GetFirstID (void)
{
	CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		return cform->GetFirstID ();
	}
	if (attribut == CCAPTBORDER) return 0;
	if (attribut == CDISPLAYONLY) return 0;
	if (attribut == CSTATIC) return 0;
	return ID;
}
 
void CFIELD::SetFeld (char * feld)
{
 	  CFORM *cform;
	  ColButton *Cub;

	  switch (attribut)
	  {
	     case CEDIT :
	     case CRIEDIT :
			  this->feld = feld;
		      break;
	     case CDISPLAYONLY :
			  this->feld = feld;
		      break;
	     case CSTATIC :
			  this->feld = feld;
		      break;
	     case CREADONLY :
			  this->feld = feld;
		      break;
		 case CBUTTON :
			  this->feld = feld;
			  break;
		 case CCOMBOBOX :
			  this->feld = feld;
			  break;
		 case CCOLBUTTON :
			  Cub = (ColButton *) this->feld;
			  Cub->text1 = (char *) feld;
			  break;
		 case CCAPTBORDER :
			  break;
		 case CLISTBOX :
			  break;
		 case CFFORM :
			  cform = (CFORM *) feld;
			  break;
	  }
}


BOOL CFIELD::SetFeld (char *feld, DWORD ID)
{
	  CFORM *cform;

	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) this->feld;
		  return cform->SetFeld (feld, ID);
	  }
	  if (this->ID == ID)
	  {
		  SetFeld (feld);
		  return TRUE;
	  }
	  return FALSE;
}


BOOL CFIELD::SetUpDown (BOOL mode, int max, int min, int value, DWORD ID)
{
	  CFORM *cform;

	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) this->feld;
		  return cform->SetUpDown (mode, max, min, value, ID);
	  }
	  if (this->ID == ID)
	  {
		  SetUpDown (mode, max, min, value);
		  return TRUE;
	  }
	  return FALSE;
}

char *CFIELD::GetFeld (DWORD ID)
{
	  CFORM *cform;

	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) this->feld;
		  return cform->GetFeld (ID);
	  }
	  if (this->ID == ID)
	  {
		  return GetFeld ();
	  }
	  return NULL;
}

BOOL CFIELD::SetImage (IMG **img, DWORD ID)
{
	  CFORM *cform;

	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  return cform->SetImage (img, ID);
	  }
	  if (this->ID == ID)
	  {
		  SetImage (img);
		  return TRUE;
	  }
	  return FALSE;
}

IMG **CFIELD::GetImage (DWORD ID)
{
	  CFORM *cform;

	  if (attribut == CFFORM)
	  {
		  cform = (CFORM *) feld;
		  return cform->GetImage (ID);
	  }
	  if (this->ID == ID)
	  {
		  return GetImage ();
	  }
	  return NULL;
}


int CFIELD::NextField (int currentfield)
{
	int current;
	CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		current = cform->NextField ();
		if (current == 0)
		{
			currentfield ++;
		}
	}
    else
	{
		currentfield ++;
	}
	return currentfield;
}

int CFIELD::PriorField (int currentfield)
{
	int current;
	CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		current = cform->PriorField ();
		if (current < 0)
		{
			currentfield --;
		}
	}
	else
	{
		currentfield --;
	}
	return currentfield;
}


void CFIELD::SetMaxStart (int maxstart)
{
    CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		cform->SetMaxStart (maxstart);
		return;
	}
	this->maxstart = maxstart;
}

void CFIELD::SetMinStart (int minstart)
{
    CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		cform->SetMinStart (minstart);
		return;
	}
	this->minstart = minstart;
}


void CFIELD::SetStart (int start)
{
    CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		cform->SetStart (start);
		return;
	}
	this->start = start;
}
void CFIELD::xSetMaxStart (int xmaxstart)
{
    CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		cform->xSetMaxStart (xmaxstart);
		return;
	}
	this->xmaxstart = xmaxstart;
}

void CFIELD::xSetMinStart (int xminstart)
{
    CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		cform->xSetMinStart (xminstart);
		return;
	}
	this->xminstart = xminstart;
}


void CFIELD::xSetStart (int xstart)
{
    CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		cform->xSetStart (xstart);
		return;
	}
	this->xstart = xstart;
}

void CFIELD::StartPlus (int plus)
{
    CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		cform->StartPlus (plus);
		return;
	}

	this->start += plus;
	if (this->start > maxstart)
	{
		this->start = maxstart;
	}
}

void CFIELD::StartMinus (int minus)
{
    CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		cform->StartMinus (minus);
		return;
	}

	this->start -= minus;
	if (this->start < minstart) this->start = 0;
}


void CFIELD::xStartPlus (int xplus)
{
    CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		cform->xStartPlus (xplus);
		return;
	}

	this->xstart += xplus;
	if (this->xstart > xmaxstart)
	{
		this->xstart = xmaxstart;
	}
}

void CFIELD::xStartMinus (int xminus)
{
    CFORM *cform;

	if (attribut == CFFORM)
    {
		cform = (CFORM *) feld;
		cform->xStartMinus (xminus);
		return;
	}

	this->xstart -= xminus;
	if (this->xstart < xminstart) this->xstart = xminstart;
}

void CFIELD::CallInfo (void)
{
#ifndef NINFO       
	   char where [256];
       char value [512];

       strncpy (value, (char *) feld, 511);
       value[511] = 0;
       clipped (value);
       if ((strlen (value) < 2) && value[0] <= ' ') return;
	   if (strlen (name))
	   {
            sprintf (where, "where %s = %s", name, (char *) feld);
     	     _CallInfoEx (hWndMain, NULL, name, where, 0l); 
	   }
#endif
       return; 
}

LRESULT CFIELD::SendMessage( UINT msg, WPARAM wParam, LPARAM lParam)
{
        if (hWnd != NULL)
        {
            return ::SendMessage (hWnd, msg, wParam, lParam);
        }
        return NULL;
}
 
BOOL CFIELD::PostMessage( UINT msg, WPARAM wParam, LPARAM lParam)
{
        if (hWnd != NULL)
        {
            return ::PostMessage (hWnd, msg, wParam, lParam);
        }
        return NULL;
}
 

int CFORM::GetTm (MFONT *mf, HWND hWnd)
{
	  HDC hdc;
	  MFONT *mfont;
	  HFONT oldfont;
	  TEXTMETRIC tm;
	  int Height;
	  
	  if (mf) 
	  {
		  mfont = mf;
	  }
	  else if (mFont)
	  {
		  mfont = mFont;
	  }
	  else
	  {
		  mfont = cfield[0]->GetFont ();
	  }
	  if (mfont == NULL)
	  {
		  return 0;
	  }
	  hdc = GetDC (hWnd);
      hFont = SetDeviceFont (hdc, mfont, &tm);
      oldfont = SelectObject (hdc, hFont);
//      Height = (int) (double) ((double) tm.tmHeight * 1.3);  
      Height = tm.tmHeight + fspaceh;  
	  DeleteObject (SelectObject (hdc, oldfont));
	  return Height;
}


void CFORM::TmStartPlus (int plus, MFONT *mf, HWND hWnd)
{
	  HDC hdc;
	  MFONT *mfont;
	  HFONT oldfont;
	  TEXTMETRIC tm;
	  int Height;
	  int i, j;
	  
	  if (mf) 
	  {
		  mfont = mf;
	  }
	  else if (mFont)
	  {
		  mfont = mFont;
	  }
	  else
	  {
		  mfont = cfield[0]->GetFont ();
	  }
	  if (mfont == NULL)
	  {
		  return;
	  }
	  hdc = GetDC (hWnd);
      hFont = SetDeviceFont (hdc, mfont, &tm);
      oldfont = SelectObject (hdc, hFont);
//      Height = (int) (double) ((double) tm.tmHeight * 1.3);  
      Height = tm.tmHeight + fspaceh;  
	  DeleteObject (SelectObject (hdc, oldfont));
	  ReleaseDC (hWnd, hdc);
      for (i = 0; i < fieldanz; i ++)
	  {
          for (j = 0; j < plus; j ++)
		  {
		            cfield[i]->StartPlus (Height);
		  }
	  }
}

void CFORM::TmStartMinus (int minus, MFONT *mf, HWND hWnd)
{
	  HDC hdc;
	  MFONT *mfont;
	  HFONT oldfont;
	  TEXTMETRIC tm;
	  int Height;
	  int i, j;
	  
	  if (mf) 
	  {
		  mfont = mf;
	  }
	  else if (mFont)
	  {
		  mfont = mFont;
	  }
	  else
	  {
		  mfont = cfield[0]->GetFont ();
	  }
	  if (mfont == NULL)
	  {
		  return;
	  }
	  hdc = GetDC (hWnd);
      hFont = SetDeviceFont (hdc, mfont, &tm);
      oldfont = SelectObject (hdc, hFont);
//      Height = (int) (double) ((double) tm.tmHeight * 1.3);  
      Height = tm.tmHeight + fspaceh;  
	  DeleteObject (SelectObject (hdc, oldfont));
	  ReleaseDC (hWnd, hdc);
      for (i = 0; i < fieldanz; i ++)
	  {
          for (j = 0; j < minus; j ++)
		  {
		            cfield[i]->StartMinus (Height);
		  }
	  }
}

int CFORM::xGetTm (MFONT *mf, HWND hWnd)
{
	  HDC hdc;
	  MFONT *mfont;
	  HFONT oldfont;
	  TEXTMETRIC tm;
	  SIZE size;
	  
	  if (mf) 
	  {
		  mfont = mf;
	  }
	  else if (mFont)
	  {
		  mfont = mFont;
	  }
	  else
	  {
		  mfont = cfield[0]->GetFont ();
	  }
	  if (mfont == NULL)
	  {
		  return 0;
	  }
	  hdc = GetDC (hWnd);
      hFont = SetDeviceFont (hdc, mfont, &tm);
      oldfont = SelectObject (hdc, hFont);
      GetTextExtentPoint32 (hdc, "X", 1 , &size);
	  DeleteObject (SelectObject (hdc, oldfont));
      return size.cx;  
}

void CFORM::xTmStartPlus (int plus, MFONT *mf, HWND hWnd)
{
	  HDC hdc;
	  MFONT *mfont;
	  HFONT oldfont;
	  TEXTMETRIC tm;
	  SIZE size;
	  int Width;
	  int i, j;
	  
	  if (mf) 
	  {
		  mfont = mf;
	  }
	  else if (mFont)
	  {
		  mfont = mFont;
	  }
	  else
	  {
		  mfont = cfield[0]->GetFont ();
	  }
	  if (mfont == NULL)
	  {
		  return;
	  }
	  hdc = GetDC (hWnd);
      hFont = SetDeviceFont (hdc, mfont, &tm);
      oldfont = SelectObject (hdc, hFont);
      GetTextExtentPoint32 (hdc, "X", 1 , &size);
      Width = size.cx;  
	  DeleteObject (SelectObject (hdc, oldfont));
	  ReleaseDC (hWnd, hdc);
      for (i = 0; i < fieldanz; i ++)
	  {
          for (j = 0; j < plus; j ++)
		  {
		            cfield[i]->xStartPlus (Width);
		  }
	  }
}

void CFORM::xTmStartMinus (int minus, MFONT *mf, HWND hWnd)
{
	  HDC hdc;
	  MFONT *mfont;
	  HFONT oldfont;
	  TEXTMETRIC tm;
	  SIZE size;
	  int Width;
	  int i, j;
	  
	  if (mf) 
	  {
		  mfont = mf;
	  }
	  else if (mFont)
	  {
		  mfont = mFont;
	  }
	  else
	  {
		  mfont = cfield[0]->GetFont ();
	  }
	  if (mfont == NULL)
	  {
		  return;
	  }
	  hdc = GetDC (hWnd);
      hFont = SetDeviceFont (hdc, mfont, &tm);
      oldfont = SelectObject (hdc, hFont);
      GetTextExtentPoint32 (hdc, "X", 1 , &size);
      Width = size.cx;  
	  DeleteObject (SelectObject (hdc, oldfont));
	  ReleaseDC (hWnd, hdc);
      for (i = 0; i < fieldanz; i ++)
	  {
          for (j = 0; j < minus; j ++)
		  {
		            cfield[i]->xStartMinus (Width);
		  }
	  }
}

int CFORM::before (DWORD ID)
{
	int i;
	int ret;

	ret = 0;
	for (i = 0; i < fieldanz; i ++)
	{
		if (ret = cfield[i]->dobefore (ID) != -1) break;
	}
	return ret;
}

int CFORM::before (char *name)
{
	int i;
	int ret;

	ret = 0;
	for (i = 0; i < fieldanz; i ++)
	{
		if (ret = cfield[i]->dobefore (name) != -1) break;
	}
	return ret;
}

int CFORM::after (DWORD ID)
{
	int i;
	int ret;

	ret = 0;
	for (i = 0; i < fieldanz; i ++)
	{
		ret = cfield[i]->doafter (ID);
		if (ret != -1) break;
	}
	return ret;
}

int CFORM::after (char *name)
{
	int i;
	int ret;

	ret = 0;
	for (i = 0; i < fieldanz; i ++)
	{
		if (ret = cfield[i]->doafter (name) != -1) break;
	}
	return ret;
}

void CFORM::SetMaxStart (int maxstart)
{
	int i;

	this->maxstart = maxstart;

	for (i = 0; i < fieldanz; i ++)
	{
             cfield [i]->SetMaxStart (maxstart);
	}
}

void CFORM::SetMinStart (int minstart)
{
	int i;

	this->minstart = minstart;
	for (i = 0; i < fieldanz; i ++)
	{
             cfield [i]->SetMinStart (minstart);
	}
}

void CFORM::SetStart (int start)
{
	int i;

	this->start = start;
	for (i = 0; i < fieldanz; i ++)
	{
             cfield [i]->SetStart (start);
	}
}

int CFORM::GetStart (void)
{
	return this->start;
}

void CFORM::StartPlus (int plus)
{
	int i;

	this->start += plus;
	if (this->start > maxstart)
	{
						this->start = maxstart;
	}
	for (i = 0; i < fieldanz; i ++)
	{
             cfield [i]->StartPlus (plus);
	}
}

void CFORM::StartMinus (int minus)
{
	int i;

	this->start -= minus;
	if (this->start < minstart)
	{
			this->start = minstart;
	}
	for (i = 0; i < fieldanz; i ++)
	{
             cfield [i]->StartMinus (minus);
	}
}

void CFORM::xSetMaxStart (int xmaxstart)
{
	int i;

	this->xmaxstart = xmaxstart;
	for (i = 0; i < fieldanz; i ++)
	{
             cfield [i]->xSetMaxStart (maxstart);
	}
}

void CFORM::xSetMinStart (int xminstart)
{
	int i;

	this->xminstart = xminstart;
	for (i = 0; i < fieldanz; i ++)
	{
             cfield [i]->xSetMinStart (xminstart);
	}
}

void CFORM::xSetStart (int xstart)
{
	int i;

	this->xstart = xstart;
	for (i = 0; i < fieldanz; i ++)
	{
             cfield [i]->xSetStart (xstart);
	}
}


int CFORM::xGetStart (void)
{
	return this->xstart;
}

void CFORM::MoveToFoot (HWND MainWindow)
{
    for (int i = 0; i < fieldanz; i ++)
    {
        cfield[i]->MoveToFoot (MainWindow);
    }
}

void CFORM::Move (void)
{
	int i;

	for (i = 0; i < fieldanz; i ++)
	{
		    cfield[i]->Move ();
	}
}

void CFORM::MoveWindow (void)
{
	int i;

	for (i = 0; i < fieldanz; i ++)
	{
		    cfield[i]->MoveWindow ();
	}
}

void CFORM::Setchsize (DWORD ID, int modex, int modey)
{
	int i;
	CFORM *Cform;

	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->GetAttribut () == CFFORM)
		{
			Cform = (CFORM *) cfield[i]->GetFeld ();
			Cform->Setchsize (ID, modex, modey);
		}
		else if (cfield[i]->GetID () == ID)
		{
			cfield[i]->Setchsize (modex, modey);
			break;
		}
	}
}

void CFORM::Setchpos (DWORD ID, int modex, int modey)
{
	int i;
	CFORM *Cform;

	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->GetAttribut () == CFFORM)
		{
			Cform = (CFORM *) cfield[i]->GetFeld ();
			Cform->Setchpos (ID, modex, modey);
		}
		else if (cfield[i]->GetID () == ID)
		{
			cfield[i]->Setchpos (modex, modey);
			break;
		}
	}
}

void CFORM::Setchsize (char *name, int modex, int modey)
{
	int i;
	CFORM *Cform;

	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->GetAttribut () == CFFORM)
		{
			Cform = (CFORM *) cfield[i]->GetFeld ();
			Cform->Setchsize (name, modex, modey);
		}
		else if (strcmp (cfield[i]->GetName (),name) == 0)
		{
			cfield[i]->Setchsize (modex, modey);
			break;
		}
	}
}

void CFORM::Setchpos (char *name, int modex, int modey)
{
	int i;
	CFORM *Cform;

	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->GetAttribut () == CFFORM)
		{
			Cform = (CFORM *) cfield[i]->GetFeld ();
			Cform->Setchpos (name, modex, modey);
		}
		else if (strcmp (cfield[i]->GetName (), name) == 0)
		{
			cfield[i]->Setchpos (modex, modey);
			break;
		}
	}
}

void CFORM::Setchsize (int modex, int modey)
{
	int i;
	CFORM *Cform;

	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->GetAttribut () == CFFORM)
		{
			Cform = (CFORM *) cfield[i]->GetFeld ();
			Cform->Setchsize (modex, modey);
		}
		cfield[i]->Setchsize (modex, modey);
	}
}

void CFORM::Setchpos (int modex, int modey)
{
	int i;
	CFORM *Cform;

	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->GetAttribut () == CFFORM)
		{
			Cform = (CFORM *) cfield[i]->GetFeld ();
			Cform->Setchpos (modex, modey);
		}
		cfield[i]->Setchpos (modex, modey);
	}
}


void CFORM::xStartPlus (int xplus)
{
	int i;

	this->xstart += xplus;
	if (this->xstart > xmaxstart)
	{
			this->xstart = xmaxstart;
	}
	for (i = 0; i < fieldanz; i ++)
	{
             cfield [i]->xStartPlus (xplus);
	}
}

void CFORM::xStartMinus (int xminus)
{
	int i;

	this->xstart -= xminus;
	if (this->xstart < xminstart)
	{
			this->xstart = xminstart;
	}
	for (i = 0; i < fieldanz; i ++)
	{
             cfield [i]->xStartMinus (xminus);
	}
}


void CFORM::display (HWND MainWindow, HDC hdc)
{
	int i;
	int x;

	hWndMain = MainWindow;
	if (layout == XFLOW)
	{
		cfield[0]->SetAbsPos ();
		for (i = 1; i < fieldanz; i ++)
		{
			x = cfield[i - 1]->GetNextPos (MainWindow);
   		    cfield[i]->SetAbsPos ();
            cfield[i]->SetPX (x);
		}
	}
		
	for (i = 0; i < fieldanz; i ++)
	{
		cfield[i]->display (MainWindow, hdc);
	}
}

void CFORM::display (void)
{
    HDC hdc;
    if (hWndMain == NULL) return;

    hdc = GetDC (hWndMain);
    display (hWndMain, hdc);
    ReleaseDC (hWndMain, hdc);
}


void CFORM::displaytxt (HWND MainWindow, HDC hdc)
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
		cfield[i]->displaytxt (MainWindow, hdc);
	}
}

void CFORM::GetRectText (int *cx, int *cy)
{
	int i;
	int cx0, cy0;

	*cx = 0;
	*cy = 0;
	for (i = 0; i < fieldanz; i ++)
	{
		cx0 = cfield[i]->GetWidthText ();
		if (cx0 > *cx) *cx = cx0; 
		cy0 = cfield[i]->GetHeightText ();
		if (cy0 > *cy) *cy = cy0; 
	}
}

void CFORM::GetRect (int *cx, int *cy)
{
	int i;
	int cx0, cy0;

	*cx = 0;
	*cy = 0;
	for (i = 0; i < fieldanz; i ++)
	{
		cx0 = cfield[i]->GetWidth ();
		if (cx0 > *cx) *cx = cx0; 
		cy0 = cfield[i]->GetHeight ();
		if (cy0 > *cy) *cy = cy0; 
	}
}

void CFORM::GetRect (RECT *rect)
{
	int i;
	int cx0, cy0;
	int cx, cy;
	int x0, y0;
	int x, y;
	CFORM *Cform;
	RECT rect0;

	cx = 0;
	cy = 0;
	if (cfield[0]->GetAttribut () == CFFORM)
	{
		Cform = (CFORM *) cfield[0]->GetFeld ();
		Cform->GetRect (&rect0);
		x = rect0.left;
		y = rect0.top;
	}
	else
	{
	    x = cfield[0]->GetX ();
	    y = cfield[0]->GetY ();
	}
	for (i = 0; i < fieldanz; i ++)
	{
	    if (cfield[i]->GetAttribut () == CFFORM)
		{
		          Cform = (CFORM *) cfield[i]->GetFeld ();
		          Cform->GetRect (&rect0);
		          x0 = rect0.left;
		          y0 = rect0.top;
				  cx0 = rect0.right  - x0;
				  cy0 = rect0.bottom - y0;
		}
		else
		{
	              x0 = cfield[i]->GetX ();
	              y0 = cfield[i]->GetY ();
		          cx0 = cfield[i]->GetWidth ();
		          cy0 = cfield[i]->GetHeight ();
		}
		if (x0 < x) x = x0; 
		if (cx0 > cx) cx = cx0; 
		if (y0 < y) y = y0; 
		if (cy0 > cy) cy = cy0; 
	}
	rect->left   = x;
	rect->top    = y;
	rect->right  = cx - x;
	rect->bottom = cy - y;
}

void CFORM::GetRectP (RECT *rect)
{
	int i;
	int cx0, cy0;
	int cx, cy;
	int x0, y0;
	int x, y;
	CFORM *Cform;
	RECT rect0;

	cx = 0;
	cy = 0;
	if (cfield[0]->GetAttribut () == CFFORM)
	{
		Cform = (CFORM *) cfield[0]->GetFeld ();
		Cform->GetRectP (&rect0);
		x = rect0.left;
		y = rect0.top;
	}
	else
	{
	    x = cfield[0]->GetXP ();
	    y = cfield[0]->GetYP ();
	}
	for (i = 0; i < fieldanz; i ++)
	{
	    if (cfield[i]->GetAttribut () == CFFORM)
		{
		          Cform = (CFORM *) cfield[i]->GetFeld ();
		          Cform->GetRectP (&rect0);
		          x0 = rect0.left;
		          y0 = rect0.top;
				  cx0 = rect0.right;
				  cy0 = rect0.bottom;
		}
		else
		{
	              x0 = cfield[i]->GetXP ();
	              y0 = cfield[i]->GetYP ();
		          cx0 = cfield[i]->GetCXP () + x0;
		          cy0 = cfield[i]->GetCYP () + y0;
		}
		if (x0 < x) x = x0;
		if (cx0 > cx) cx = cx0; 
		if (y0 < y) y = y0; 
		if (cy0 > cy) cy = cy0; 
	}
	rect->left   = x;
	rect->top    = y;
	rect->right  = cx;
	rect->bottom = cy;
}

void CFORM::SetAbsPos (int x, int y)
{
    int i;
	for (i = 0; i < fieldanz; i ++)
	{
		cfield [i]->SetAbsPos (x, y);
	}
}

void CFORM::SetAbsPosX (int x)
{
    int i;
	for (i = 0; i < fieldanz; i ++)
	{
		cfield [i]->SetAbsPosX (x);
	}
}


void CFORM::SetAbsPosY (int y)
{
    int i;
	for (i = 0; i < fieldanz; i ++)
	{
		cfield [i]->SetAbsPosY (y);
	}
}




void CFORM::CentX (HWND MainWindow)
{
	int cx, cy;
	int x;
	RECT rect;
	int i;

	if (MainWindow == NULL) return;
	GetRect (&cx, &cy);
	GetClientRect (MainWindow, &rect);
	x = max (0, (rect.right - cx) / 2); 
	for (i = 0; i < fieldanz; i ++)
	{
        cfield [i]->SetAbsPosX (x);
	}
}

void CFORM::Centx (HWND MainWindow)
{
	int cx, cy;
	int x;
	RECT rect;
	int i;

	if (MainWindow == NULL) return;
	GetRect (&cx, &cy);
	GetClientRect (MainWindow, &rect);
	x = max (0, (rect.right - cx) / 2); 
	for (i = 0; i < fieldanz; i ++)
	{
        cfield [i]->SetAbsPos (x, 0);
	}
}

void CFORM::Rightx (HWND MainWindow)
{
	int cx, cy;
	int x;
	RECT rect;
	int i;

	if (MainWindow == NULL) return;
	GetRect (&cx, &cy);
	GetClientRect (MainWindow, &rect);
	x = rect.right - cx - 3; 
	for (i = 0; i < fieldanz; i ++)
	{
		cfield [i]->SetAbsPos (x, 0);
	}
}

void CFORM::CentY (HWND MainWindow)
{
	int cx, cy;
	int y;
	RECT rect;
	int i;

	if (MainWindow == NULL) return;
	GetRect (&cx, &cy);
	GetClientRect (MainWindow, &rect);
	y = max (0, (rect.bottom - cy) / 2); 
	for (i = 0; i < fieldanz; i ++)
	{
		cfield [i]->SetAbsPosY (y);
	}
}

void CFORM::Centy (HWND MainWindow)
{
	int cx, cy;
	int y;
	RECT rect;
	int i;

	if (MainWindow == NULL) return;
	GetRect (&cx, &cy);
	GetClientRect (MainWindow, &rect);
	y = max (0, (rect.bottom - cy) / 2); 
	for (i = 0; i < fieldanz; i ++)
	{
        cfield[i]->SetXOK (TRUE);
		cfield [i]->SetAbsPos (0, y);
        cfield[i]->SetXOK (FALSE);
	}
}

void CFORM::Bottomy (HWND MainWindow)
{
	int cx, cy;
	int y;
	RECT rect;
	int i;

	if (MainWindow == NULL) return;
	GetRect (&cx, &cy);
	GetClientRect (MainWindow, &rect);
	y = rect.bottom - cy - 3; 
	for (i = 0; i < fieldanz; i ++)
	{
        cfield[i]->SetXOK (TRUE);
		cfield [i]->SetAbsPos (0, y);
        cfield[i]->SetXOK (FALSE);
	}
}


void CFORM::Centx (HWND MainWindow, int i)
{
	int cx, cy;
	int x;
	RECT rect;

	if (MainWindow == NULL) return;
	GetRect (&cx, &cy);
	GetClientRect (MainWindow, &rect);
	x = max (0, (rect.right - cx) / 2); 
    cfield [i]->SetAbsPos (x, 0);
//    cfield [i]->SetAbsPosX (x);
}

void CFORM::Rightx (HWND MainWindow, int i)
{
	int cx, cy;
	int x;
	RECT rect;

	if (MainWindow == NULL) return;
	GetRect (&cx, &cy);
	GetClientRect (MainWindow, &rect);
	x = rect.right - cx - 3; 
	cfield [i]->SetAbsPos (x, 0);
}

void CFORM::Centy (HWND MainWindow, int i)
{
	int cx, cy;
	int y;
	RECT rect;

	if (MainWindow == NULL) return;
	GetRect (&cx, &cy);
	GetClientRect (MainWindow, &rect);
	y = max (0, (rect.bottom - cy) / 2); 
    cfield [i]->SetAbsPos (0, y);
//    cfield [i]->SetAbsPosY (y);
}

void CFORM::Bottomy (HWND MainWindow, int i)
{
	int cx, cy;
	int y;
	RECT rect;

	if (MainWindow == NULL) return;
	GetRect (&cx, &cy);
	GetClientRect (MainWindow, &rect);
	y = rect.bottom - cy - bottomspace; 
	cfield [i]->SetAbsPos (0, y);
}

DWORD CFORM::GetFirstID ()
{
	DWORD ID;
	int i;
    ID = 0;
	i = 0;
	while (ID == 0)
	{
	    ID = cfield[i]->GetFirstID ();
		i ++;
		if (i == fieldanz) break;
	}
	return ID;
}
void CFORM::Update (int current)
{
	cfield[current]->Update ();
}

void CFORM::Update (void)
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
	      cfield[i]->Update ();
	}
}

void CFORM::SetFeld (char * feld, int current)
{
	cfield[current]->SetFeld (feld);
}

BOOL CFORM::SetFeld (char * feld, DWORD ID)
{
	int i;

	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->SetFeld (feld, ID)) return TRUE;
	}
	return FALSE;
}

BOOL CFORM::SetUpDown (BOOL mode, int max, int min, int value, DWORD ID)
{
	int i;

	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->SetUpDown (mode, max, min, value, ID)) return TRUE;
	}
	return FALSE;
}

char *CFORM::GetFeld (DWORD ID)
{
	int i;
	char *feld;

	for (i = 0; i < fieldanz; i ++)
	{
		feld = cfield[i]->GetFeld (ID);
		if (feld != NULL) return feld;
	}
	return NULL;
}

BOOL CFORM::SetImage (IMG **img, DWORD ID)
{
	int i;

	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->SetImage (img, ID)) return TRUE;
	}
	return FALSE;
}

IMG **CFORM::GetImage (DWORD ID)
{
	int i;
	IMG **img;

	for (i = 0; i < fieldanz; i ++)
	{
		if (img = cfield[i]->GetImage (ID)) return img;
	}
	return NULL;
}


int CFORM::NextField (void)
{
	int current;
    static BOOL firstround = TRUE;

	current = cfield[currentfield]->NextField (currentfield);
	if (current == currentfield) 
	{
		return currentfield;
	}

	currentfield = current;
	if (currentfield == fieldanz)
	{
        if (firstround)
        {
		     currentfield = 0;
             firstround = FALSE;
        }
        else
        {
             firstround = TRUE;
             return 0;
        }
	}
    if (cfield[currentfield]->IsDisabled ())
    {
		return NextField ();
	}
	if (cfield[currentfield]->GetAttribut () == CCAPTBORDER) 
	{
		return NextField ();
	}
	if (cfield[currentfield]->GetAttribut () == CBORDER) 
	{
		return NextField ();
	}
	if (cfield[currentfield]->GetAttribut () == CDISPLAYONLY) 
	{
		return NextField ();
	}
	if (cfield[currentfield]->GetAttribut () == CSTATIC) 
	{
		return NextField ();
	}
	if (cfield[currentfield]->GetAttribut () == CREADONLY) 
	{
		return NextField ();
	}
	if (cfield[currentfield]->GetAttribut () == CREMOVED) 
	{
		return NextField ();
	}
	if (cfield[currentfield]->GetTabstop () == FALSE) 
	{
		return NextField ();
	}
	cfield[currentfield]->SetFocus ();
	SetFocus ();
	return currentfield;
}

void CFORM::SetFocus (void)
{
	cfield[currentfield]->SetFocus ();
}

void CFORM::SetSel (void)
{
	cfield[currentfield]->SetSel ();
}

int CFORM::SetFirstFocus (void)
{
	  CFORM *cForm;

	currentfield = 0;

	if (cfield[currentfield]->GetAttribut () == CCAPTBORDER) 
	{
		return NextField ();
	}
	if (cfield[currentfield]->GetAttribut () == CBORDER) 
	{
		return NextField ();
	}
	if (cfield[currentfield]->GetAttribut () == CDISPLAYONLY) 
	{
		return NextField ();
	}
	if (cfield[currentfield]->GetAttribut () == CSTATIC) 
	{
		return NextField ();
	}
	if (cfield[currentfield]->GetAttribut () == CREADONLY) 
	{
		return NextField ();
	}
	if (cfield[currentfield]->GetAttribut () == CFFORM) 
	{
 	    cForm = (CFORM *) cfield[currentfield]->GetFeld ();
		return cForm->SetFirstFocus ();
	}
	if (cfield[currentfield]->GetTabstop () == FALSE) 
	{
		return NextField ();
	}
	cfield[currentfield]->SetFocus ();
	return currentfield;
}

int CFORM::PriorField (void)
{
	int current;
    static BOOL firstround = TRUE;

	current = cfield[currentfield]->PriorField (currentfield);
	if (current == currentfield) return currentfield;
	currentfield = current;

	if (currentfield < 0)
	{
        if (firstround == FALSE) 
        {
            firstround = TRUE;
            return 0;
        }
        firstround = FALSE;
		currentfield = fieldanz - 1;
        if (cfield[currentfield]->IsDisabled ())
        {
		    return PriorField ();
        }
		if (cfield[currentfield]->GetAttribut () == CCAPTBORDER)
		{
			return PriorField ();
		}
  	    if (cfield[currentfield]->GetAttribut () == CBORDER) 
        {
		    return PriorField ();
        }
		if (cfield[currentfield]->GetAttribut () == CDISPLAYONLY)
		{
			return PriorField ();
		}
		if (cfield[currentfield]->GetAttribut () == CSTATIC)
		{
			return PriorField ();
		}
		if (cfield[currentfield]->GetAttribut () == CREADONLY)
		{
			return PriorField ();
		}
  	    if (cfield[currentfield]->GetTabstop () == FALSE) 
        {
		    return PriorField ();
        }
        cfield[currentfield]->SetFocus ();
		return currentfield;
	}
    if (cfield[currentfield]->IsDisabled ())
    {
		    return PriorField ();
    }
	if (cfield[currentfield]->GetAttribut () == CCAPTBORDER)
	{
		return PriorField ();
	}
	if (cfield[currentfield]->GetAttribut () == CBORDER)
	{
		return PriorField ();
	}
	if (cfield[currentfield]->GetAttribut () == CDISPLAYONLY)
	{
		return PriorField ();
	}
	if (cfield[currentfield]->GetAttribut () == CSTATIC)
	{
		return PriorField ();
	}
	if (cfield[currentfield]->GetAttribut () == CREADONLY)
	{
		return PriorField ();
	}
    if (cfield[currentfield]->GetTabstop () == FALSE) 
    {
        return PriorField ();
    }
	cfield[currentfield]->SetFocus ();
	return currentfield;
}

int CFORM::NextFormField (void)
{
	currentfield ++;
	if (currentfield == fieldanz)
	{
		currentfield = 0;
	}
	if (cfield[currentfield]->GetAttribut () == CCAPTBORDER) 
	{
		return NextFormField ();
	}
	if (cfield[currentfield]->GetAttribut () == CDISPLAYONLY) 
	{
		return NextFormField ();
	}
	if (cfield[currentfield]->GetAttribut () == CSTATIC) 
	{
		return NextFormField ();
	}
	if (cfield[currentfield]->GetAttribut () == CREADONLY) 
	{
		return NextFormField ();
	}
	cfield[currentfield]->SetFocus ();
	return currentfield;
}

int CFORM::PriorFormField (void)
{
	currentfield --;
	if (currentfield < 0)
	{
		currentfield = fieldanz - 1;
		if (cfield[currentfield]->GetAttribut () == CCAPTBORDER)
		{
			return PriorFormField ();
		}
		if (cfield[currentfield]->GetAttribut () == CDISPLAYONLY)
		{
			return PriorFormField ();
		}
		if (cfield[currentfield]->GetAttribut () == CSTATIC)
		{
			return PriorFormField ();
		}
		if (cfield[currentfield]->GetAttribut () == CREADONLY)
		{
			return PriorFormField ();
		}


 	    cfield[currentfield]->SetFocus ();
		return -1;
	}
	if (cfield[currentfield]->GetAttribut () == CCAPTBORDER)
	{
		return PriorFormField ();
	}
	if (cfield[currentfield]->GetAttribut () == CDISPLAYONLY)
	{
		return PriorFormField ();
	}
	if (cfield[currentfield]->GetAttribut () == CSTATIC)
	{
		return PriorFormField ();
	}
	if (cfield[currentfield]->GetAttribut () == CREADONLY)
	{
		return PriorFormField ();
	}
	cfield[currentfield]->SetFocus ();
	return currentfield;
}

DWORD CFORM::GetAttribut (void)
/**
Attribut des Feldes mit dem Focus holen.
**/
{
	DWORD attr;
	HWND hWnd;
	int i;

	hWnd = GetFocus ();
	for (i = 0; i < fieldanz; i ++)
	{
	         attr = cfield[i]->GetAttrhWnd (hWnd);
			 if (attr != NOATTRIBUT) 
			 {
				 currentfield = i;
				 return attr;
			 }
	}
	return NOATTRIBUT;
}


BOOL CFORM::SetCurrentID (DWORD ID)
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->SetFocusID (ID))
		{
			currentfield = i;
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CFORM::IsID (DWORD ID)
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->IsID (ID)) 
        {
            currentfield = i;
            return TRUE;
        }
	}
	return FALSE;
}


BOOL CFORM::SetCurrentFieldID (DWORD ID)
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->IsID (ID))
		{
			currentfield = i;
			return TRUE;
		}
	}
	return FALSE;
}


BOOL CFORM::SetCurrentName (char *name)
{
	int i;

	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->SetFocusName (name)) 
		{
			currentfield = i;
			return TRUE;
		}
	}
	return FALSE;
}

void CFORM::SetFieldPicture (char *name, char *picture)
{
	int i;

	for (i = 0; i < fieldanz; i ++)
	{
		cfield[i]->SetFieldPicture (name, picture); 
	}
	return;
}

void CFORM::SetFieldBkMode (char *name, int BkMode)
{
	int i;

	for (i = 0; i < fieldanz; i ++)
	{
		cfield[i]->SetFieldBkMode (name, BkMode); 
	}
	return;
}

BOOL CFORM::EnableID (DWORD ID, BOOL flag)
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->EnableID (ID, flag)) return TRUE;
	}
	return FALSE;
}

BOOL CFORM::EnableName (char *name, BOOL flag)
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->EnableName (name, flag)) return TRUE;
	}
	return FALSE;
}

BOOL CFORM::CheckID (DWORD ID, BOOL flag)
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->CheckID (ID, flag)) return TRUE;
	}
	return FALSE;
}

BOOL CFORM::CheckName (char *name, BOOL flag)
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
		if (cfield[i]->CheckName (name, flag)) return TRUE;
	}
	return FALSE;
}

DWORD CFORM::GetID (char *name)
{
	int i;
	int ID;

	for (i = 0; i < fieldanz; i ++)
	{
		if (ID = cfield[i]->IsCurrentName (name)) return ID;
	}
	return 0;
}

DWORD CFORM::GetID (HWND hWnd)
{
	int i;
	int ID;

	for (i = 0; i < fieldanz; i ++)
	{
		if (ID = cfield[i]->IsCurrenthWnd (hWnd)) return ID;
	}
	return 0;
}

CFIELD *CFORM::GetCfield (DWORD ID)
{
	int i;
	CFIELD *field;

	for (i = 0; i < fieldanz; i ++)
	{
		if (field = cfield[i]->IsCfield (ID))
		{
			return field;
		}
	}
	return NULL;
}

CFIELD *CFORM::GetCfield (char *Name)
{
	int i;
	CFIELD *field;

	for (i = 0; i < fieldanz; i ++)
	{
		if (field = cfield[i]->IsCfield (Name))
		{
			return field;
		}
	}
	return NULL;
}

char * CFORM::GetName (DWORD ID)
{
	int i;
	char *name;

	for (i = 0; i < fieldanz; i ++)
	{
		if (name = cfield[i]->IsCurrentID (ID)) return name;
	}
	return NULL;
}


HWND CFORM::GethWndID (DWORD ID)
{
	int i;
	HWND hWnd;

	for (i = 0; i < fieldanz; i ++)
	{
		if (hWnd = cfield[i]->GethWndID (ID)) return hWnd;
	}
	return NULL;
}

void CFORM::SetText ()
{
    int i;
	for (i = 0; i < fieldanz; i ++)
	{
        cfield[i]->SetText ();
    }
}

void CFORM::GetText ()
{
    int i;
	for (i = 0; i < fieldanz; i ++)
	{
        cfield[i]->GetText ();
    }
}

void CFORM::SetNewPos (BOOL b)
{
    int i;

    if (this == NULL) return;
    NewPosNeeded = b;
	for (i = 0; i < fieldanz; i ++)
	{
        cfield[i]->SetNewPos (b);
    }
}


HWND CFORM::GethWndName (char *name)
{
	int i;
	HWND hWnd;

	for (i = 0; i < fieldanz; i ++)
	{
		if (hWnd = cfield[i]->GethWndName (name)) return hWnd;
	}
	return NULL;
}

void CFORM::Setxpplus (int plus)
{
	int i;

	for (i = 0; i < fieldanz; i ++)
	{
        cfield[i]->Setxpplus (plus);
	}
}

void CFORM::Setypplus (int plus)
{
	int i;

	for (i = 0; i < fieldanz; i ++)
	{
        cfield[i]->Setypplus (plus);
	}
}

void CFORM::SetFieldanz (void)
{
    int i;

    for (i = 0; cfield[i]; i ++);
    fieldanz = i;
}

void CFORM::destroy ()
{
	int i;
    if (this == NULL) return;
	for (i = 0; i < fieldanz; i ++)
	{
		cfield[i]->destroy ();
	}
}

void CFORM::ScrollInsert (int pos)
{
    int i;

    for (i = fieldanz; i > pos; i --)
    {
        cfield[i] = cfield[i - 1];
    }
}



BOOL CFORM::InsertCfield (CFIELD *cf, char *name)
{
      int i;
	  CFORM *cForm;
      static CFIELD *cField = NULL;
      int x,y, cx;

      for (i = 0; cfield[i] != NULL; i ++)
      {
          if (strcmp (name, cfield[i]->GetName ()) == 0)
          {
              cx = cf->GetCXorg ();
              x = cfield[i]->GetXorg () - cx;
              y = cfield[i]->GetYorg ();
              cf->SetX (x);
              cf->SetY (y);
              ScrollInsert (i);
              cfield[i] = cf;
              if (cField != NULL)
              {
                  x = cField->GetXorg ();
                  y = cField->GetYorg ();
		          if (x == -1) 
                  {
		               Centx (hWndMain, i);
                  }
			      else if (x == -2) 
                  {
				     Rightx (hWndMain, i);
                  }

                  if (y == -1) 
                  {
 	                   Centy (hWndMain, i);
                  }

                  else if (y == -2) 
                  {
				      Bottomy (hWndMain, i);
                  }
              }
              cfield[i]->SetStart (start); 
              cfield[i]->xSetStart (xstart); 
              cfield[i]->display (hWndMain); 
              fieldanz ++;
              cField = NULL;
              return TRUE;
          }
          if (cfield[i]->GetAttribut () == CFFORM)
          {
              cField = cfield[i];
              cForm = (CFORM *) cfield[i]->GetFeld ();
              if (cForm->InsertCfield (cf, name))
              {
                  cField = NULL;
                  return TRUE;
              }
          }
      }
      cField = NULL;
      return FALSE;
}

BOOL CFORM::InsertCfieldAt (CFIELD *cf, char *name)
{
      int i;
	  CFORM *cForm;

      for (i = 0; cfield[i] != NULL; i ++)
      {
          if (strcmp (name, cfield[i]->GetName ()) == 0)
          {
              ScrollInsert (i);
              cfield[i] = cf;
              cfield[i]->SetStart (start); 
              cfield[i]->xSetStart (xstart); 
              cfield[i]->display (hWndMain); 
              fieldanz ++;
              return TRUE;
          }
          if (cfield[i]->GetAttribut () == CFFORM)
          {
              cForm = (CFORM *) cfield[i]->GetFeld ();
              if (cForm->InsertCfield (cf, name))
              {
                  return TRUE;
              }
          }
      }
      return FALSE;
}


void CFORM::ScrollRemove (int pos)
{
    int i;

    for (i = pos + 1; i < fieldanz ; i ++)
    {
        cfield[i - 1] = cfield[i];
    }
    cfield [fieldanz - 1] = NULL;
}


BOOL CFORM::RemoveCfield (char *name)
{
      int i;
	  CFORM *cForm;


      for (i = 0; cfield[i] != NULL; i ++)
      {
          if (strcmp (name, cfield[i]->GetName ()) == 0)
          {
              cfield[i]->destroy ();
              ScrollRemove (i);
              fieldanz --;
              return TRUE;
          }
          if (cfield[i]->GetAttribut () == CFFORM)
          {
              cForm = (CFORM *) cfield[i]->GetFeld ();
              if (cForm->RemoveCfield (name))
              {
                  return TRUE;
              }
          }
      }
      return FALSE;
}


void CFORM::enter (void)
{
	return;
}

static form *TextForm;
static HWND TextWindow;

CFIELD **CTextField;
CFORM *CTextForm;

void WFORM::ShowDlg (HDC hdc, form *frm)
/**
Dialogtext anzeigen.
**/
{
	      TEXTMETRIC tm;
          int i;
		  int x, y;
		  HFONT hFont, oldfont;;


		  if (frm->font)
		  {
                   hFont = SetDeviceFont (hdc, frm->font, &tm);
		  }
		  else
		  {
			       hFont = CreateStdFont (NULL);
		  }

		  oldfont = SelectObject (hdc, hFont);
		  for (i = 0; i < frm->fieldanz; i ++)
		  {
			      x = frm->mask[i].pos[1] * tm.tmAveCharWidth;
				  y = (int) (double) ((double) frm->mask[i].pos[0] * tm.tmHeight * 1.3);
//				  y = frm->mask[i].pos[0] * (tm.tmHeight+ fspace);
                  SetBkMode (hdc, TRANSPARENT);
				  if (frm->font)
				  {
                            SetTextColor (hdc,frm->font->FontColor);
				  }
				  else
				  {
                            SetTextColor (hdc,BLACKCOL);
				  }
				  if (frm->mask[i].length == 0)
				  {
                            TextOut (hdc, x, y, frm->mask[i].item->GetFeldPtr (), 
								strlen (frm->mask[i].item->GetFeldPtr ()));
				  }
				  else
				  {
                            TextOut (hdc, x, y, frm->mask[i].item->GetFeldPtr (),
								frm->mask[i].length);
				  }
		  }
		  DeleteObject (SelectObject (hdc, oldfont));
}


void WFORM::ProcessMessages (void)
{
	      MSG msg;

          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
				  break;
			  }
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
		  }
}

CALLBACK WFORM::Proc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					ShowDlg (hdc, TextForm);
                    EndPaint (hWnd, &ps);
					break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

CALLBACK WFORM::CProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					CTextForm->display (hWnd, hdc);
                    EndPaint (hWnd, &ps);
					break;
              
              case WM_COMMAND :
                    if (LOWORD (wParam) == BuOk)
                    {
                           PostQuitMessage (0);
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


HWND MESSCLASS::OpenWindow (HANDLE hInstance, HWND hMainWindow, char **text,mfont *WindowFont)
{
	      TEXTMETRIC tm;
		  int cx, cy, x, y;
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  int i;
		  HFONT hFont, oldfont;
		  HDC hdc;
		  SIZE size;
		  RECT rect;

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) Proc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "MessWind";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }
	      
		  cx = 0;
		  hdc = GetDC (hMainWindow);
          hFont = SetDeviceFont (hdc, WindowFont, &tm);
          oldfont = SelectObject (hdc, hFont); 
		  for (i = 0; i < tlines; i ++)
		  {
              GetTextExtentPoint32 (hdc, text[i], strlen (text[i]) , &size); 
			  if (size.cx > cx)
			  {
				  cx = size.cx;
			  }
		  }
          GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		  cx += 4 * size.cx;
		  DeleteObject (SelectObject (hdc, oldfont));
		  ReleaseDC (hMainWindow, hdc);
		  cy = (int) (double) ((double) tlines  * tm.tmHeight * 1.3);  
//		  cy = tlines  * (tm.tmHeight fspace);  

		  GetWindowRect (hMainWindow, &rect);
		  x = max (0, (rect.right - rect.left - cx) / 2) + rect.left;
		  y = max (0, (rect.bottom - rect.top - cy) / 2) + rect.top;

          return (CreateWindowEx (0, 
                                 "MessWind",
                                 "",
                                 WS_VISIBLE | WS_DLGFRAME | WS_POPUP,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL));
}

void MESSCLASS::MessageText (HANDLE hInstance, HWND hWnd, char **text, mfont *WindowFont)
{
	      static form *frm;
		  int lines;
		  int i, j;
		  int start;

		  for (tlines = 0; text[tlines]; tlines ++);
		  hMainWindow = hWnd;
		  hMainInst = hInstance;
		  frm = (form *) GlobalAlloc (GMEM_FIXED, sizeof (form));
		  if (frm == NULL) return;
		  
	      lines = 10;
		  if (tlines < lines)
		  {
			  lines = tlines;
		  }

		  frm->mask = (field *) GlobalAlloc (GMEM_FIXED, tlines * sizeof (field));
		  if (frm->mask == NULL)
		  {
			  GlobalFree (frm);
			  return;
		  }

		  frm->fieldanz = tlines;
		  frm->frmstart = 0;
		  frm->frmscroll = 0;
          frm->caption = "";
		  frm->before = NULL;
		  frm->after = NULL;
	      frm->cbfield = NULL;
		  frm->font = WindowFont;
		  for (i = 0; i < frm->fieldanz; i ++)
		  {
			  frm->mask[i].length = 0;
			  frm->mask[i].rows = 0;
			  frm->mask[i].pos[0] = i;
			  frm->mask[i].pos[1] = 1;
			  frm->mask[i].feldid = NULL;
			  frm->mask[i].picture = "";
			  frm->mask[i].attribut = DISPLAYONLY;
			  frm->mask[i].before = NULL;
			  frm->mask[i].after = NULL;
			  frm->mask[i].BuId = 0;
		  }
          start = 0;
		  for (i = start, j = 0; j < tlines; i ++, j ++)
		  {
			  frm->mask[j].item->SetFeldPtr (text[i]);
		  }

		  TextForm = frm;
		  SetwForm (frm);
		  EnableWindows (hMainWindow, FALSE);
          MessageBeep (MB_ICONEXCLAMATION);
          TextWindow = OpenWindow (hInstance, hMainWindow, text, WindowFont);
		  InvalidateRect (TextWindow, NULL, TRUE);
		  UpdateWindow (TextWindow);
}


HWND MESSCLASS::COpenWindow (HANDLE hInstance, HWND hMainWindow, char **text, mfont **textfont)
{
	      TEXTMETRIC tm;
		  int cx, cy, x, y;
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  int i;
		  HFONT hFont, oldfont;
		  HDC hdc;
		  SIZE size;
		  RECT rect;

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CMessWind";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }
	      
		  cx = 0;
		  cy = 0;
	      hdc = GetDC (hMainWindow);
		  for (i = 0; text[i]; i ++)
		  {
              hFont = SetDeviceFont (hdc, textfont[i], &tm);
 		      cy += (int) (double) ((double) tm.tmHeight * 1.3);  
              oldfont = SelectObject (hdc, hFont); 
              GetTextExtentPoint32 (hdc, text[i], strlen (text[i]) , &size); 
			  if (size.cx > cx)
			  {
				  cx = size.cx;
			  }
              GetTextExtentPoint32 (hdc, "X", 1 , &size); 
   		      DeleteObject (SelectObject (hdc, oldfont));
		  }

		  if (i < tlines)
		  {
 		      cy += (int) (double) ((double) tm.tmHeight * 1.3);  
 		      hdc = GetDC (hMainWindow);
              hFont = SetDeviceFont (hdc, textfont[0], &tm);
 		      cy += (int) (double) ((double) tm.tmHeight * 2);  
			  DeleteObject (hFont);
		  }
	      ReleaseDC (hMainWindow, hdc);
	
		  cx += 4 * size.cx;
          
		  GetWindowRect (hMainWindow, &rect);
		  x = max (0, (rect.right - rect.left - cx) / 2) + rect.left;
		  y = max (0, (rect.bottom - rect.top - cy) / 2) + rect.top;

          return (CreateWindowEx (0, 
                                 "CMessWind",
                                 "",
                                 WS_VISIBLE | WS_DLGFRAME | WS_POPUP,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL));
}


void MESSCLASS::CMessageText (HANDLE hInstance, HWND hWnd, char **text, 
							  mfont **textfont, mfont *WindowFont)
{
		  int lines;
		  int i;

		  for (tlines = 0; text[tlines]; tlines ++);

		  hMainWindow = hWnd;
		  hMainInst = hInstance;

	      lines = 10;
		  if (tlines < lines)
		  {
			  lines = tlines;
		  }

          CTextField = (CFIELD **) GlobalAlloc (GMEM_FIXED, tlines * sizeof (CFIELD *));
          CTextForm  = new CFORM (tlines, CTextField); 

		  for (i = 0; i < tlines; i ++)
		  {
                      CTextField[i] = new CFIELD ("Text",
						                          text[i], 
						                          0, 0, 1, i, 
												  NULL, "", DISPLAYONLY,
										          NULL, textfont[i], FALSE, TRANSPARENT); 		  
		  }
		  SetcwForm (CTextForm);
//		  EnableWindows (hMainWindow, FALSE);
          TextWindow = COpenWindow (hInstance, hMainWindow, text, textfont);
		  InvalidateRect (TextWindow, NULL, TRUE);
		  UpdateWindow (TextWindow);
}


void MESSCLASS::CMessageTextOK (HANDLE hInstance, HWND hWnd, char **text, 
								mfont **textfont, BOOL signal)
{
	      TEXTMETRIC tm;
		  HFONT hFont, oldfont;
		  SIZE size;
		  HDC hdc;
		  int lines;
		  int i;
		  int x,y, cx;

		  for (tlines = 0; text[tlines]; tlines ++);

		  hMainWindow = hWnd;
		  hMainInst = hInstance;

	      lines = 10;
		  if (tlines < lines)
		  {
			  lines = tlines;
		  }

          CTextField = (CFIELD **) GlobalAlloc (GMEM_FIXED, (tlines + 1) * sizeof (CFIELD *));
          CTextForm  = new CFORM (tlines + 1, CTextField); 

	      hdc = GetDC (hMainWindow);
		  y = 0;
		  for (i = 0; i < tlines; i ++)
		  {
                      hFont = SetDeviceFont (hdc, textfont[i], &tm);
                      oldfont = SelectObject (hdc, hFont); 
                      GetTextExtentPoint32 (hdc, "X", 1 , &size); 
       		          DeleteObject (SelectObject (hdc, oldfont));
					  x = size.cx;
                      CTextField[i] = new CFIELD ("Text",
						                          text[i], 
						                          0, 0, x, y, 
												  NULL, "", DISPLAYONLY,
										          NULL, textfont[i], TRUE, TRANSPARENT); 		  
 		              y += (int) (double) ((double) tm.tmHeight * 1.3);  
		  }
          hFont = SetDeviceFont (hdc, textfont[0], &tm);
          y += (int) (double) ((double) tm.tmHeight * 1.3);  
          oldfont = SelectObject (hdc, hFont); 
          GetTextExtentPoint32 (hdc, "X", 1 , &size); 
          DeleteObject (SelectObject (hdc, oldfont));
		  x = size.cx;
          cx = 10 * size.cx;
          CTextField[i] = new CFIELD ("BuOK",
			                          "  OK  ",
			                          cx, 0, -1, y,
									  NULL, "", CBUTTON,
									  BuOk, textfont [i], TRUE, TRANSPARENT);
          ReleaseDC (hMainWindow, hdc);
		  tlines += 2;
		  SetcwForm (CTextForm);
		  EnableWindows (hMainWindow, FALSE);
		  if (signal)
		  {
                   MessageBeep (MB_ICONEXCLAMATION);
		  }
          TextWindow = COpenWindow (hInstance, hMainWindow, text, textfont);
		  InvalidateRect (TextWindow, NULL, TRUE);
		  UpdateWindow (TextWindow);
}


HWND MESSCLASS::COpenWindowF (HANDLE hInstance, HWND hMainWindow, CFORM *cform)
{
		  int cx, cy, x, y;
		  static BOOL registered = FALSE;
          WNDCLASS wc;
		  RECT rect;

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CMessWind";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }
	      
		  cx = 0;
		  cy = 0;

          cform->GetRect (&cx, &cy);
	
		  cx += 10;
		  cy += 20;
          
		  GetWindowRect (hMainWindow, &rect);
		  x = max (0, (rect.right - rect.left - cx) / 2) + rect.left;
		  y = max (0, (rect.bottom - rect.top - cy) / 2) + rect.top;

          return (CreateWindowEx (0, 
                                 "CMessWind",
                                 "",
                                 WS_VISIBLE | WS_DLGFRAME | WS_POPUP,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL));
}


void MESSCLASS::CMessageTextF (HANDLE hInstance, HWND hWnd, CFORM *TextForm, BOOL signal) 
{
	      int lines;

		  hMainWindow = hWnd;
		  hMainInst = hInstance;

	      lines = 10;
		  if (tlines < lines)
		  {
			  lines = tlines;
		  }

          CTextForm  = TextForm; 

		  SetcwForm (CTextForm);
		  EnableWindows (hMainWindow, FALSE);
		  if (signal)
		  {
                   MessageBeep (MB_ICONEXCLAMATION);
		  }
          TextWindow = COpenWindowF (hInstance, hMainWindow, TextForm);
		  InvalidateRect (TextWindow, NULL, TRUE);
		  UpdateWindow (TextWindow);
}


void MESSCLASS::DestroyText (void)
{
 		  EnableWindows (hMainWindow, TRUE);
		  DestroyWindow (TextWindow);
		  GlobalFree (TextForm->mask);
		  GlobalFree (TextForm);
}

void MESSCLASS::CDestroyText ()
{
	      int i;
		  int lines;

 		  EnableWindows (hMainWindow, TRUE);
		  CTextForm->destroy ();
          lines = CTextForm->GetFieldanz ();

          for (i = 0; i < lines; i ++)
		  {
			  delete CTextField[i];
		  }
		  GlobalFree (CTextField);
		  delete (CTextForm);
		  DestroyWindow (TextWindow);
}
        
void MESSCLASS::CDestroyTextF ()
{
 		  EnableWindows (hMainWindow, TRUE);
		  CTextForm->destroy ();
		  DestroyWindow (TextWindow);
}
