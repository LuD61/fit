package DataInterface;

public interface Statement {

	String getSelectStatement();
	
	String getInsertStatement();
	
	String getUpdateStatement();
}
