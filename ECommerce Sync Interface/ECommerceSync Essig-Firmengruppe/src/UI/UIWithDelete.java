package UI;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

import Import.ImportDataGenerator;

public class UIWithDelete extends JPanel implements ActionListener {
	
	
	JSpinner spinner = null;
    JSpinner spinner2 = null;
	
	
	public UIWithDelete() {		
				
		this.setLayout(new GridLayout(0, 2));
		
		JLabel jlbLabel1 = new JLabel("Anfangsdatum: ", JLabel.LEFT);
		// We can position of the text, relative to the icon:
		jlbLabel1.setVerticalTextPosition(JLabel.BOTTOM);
		jlbLabel1.setHorizontalTextPosition(JLabel.LEFT);
		
		JLabel jlbLabel2 = new JLabel("Enddatum: ", JLabel.LEFT);
		// We can position of the text, relative to the icon:
		jlbLabel1.setVerticalTextPosition(JLabel.BOTTOM);
		jlbLabel1.setHorizontalTextPosition(JLabel.LEFT);
		
	    // Create a date spinner
	    SpinnerDateModel dateModel = new SpinnerDateModel();
	    SpinnerDateModel dateModel2 = new SpinnerDateModel();
	    this.spinner = new JSpinner(dateModel);
	    this.spinner2 = new JSpinner(dateModel2);
	
	    Calendar calendar = new GregorianCalendar();
	    this.spinner.setValue(calendar.getTime());
		
		this.add(jlbLabel1);
		this.add(this.spinner);		
		
		this.add(jlbLabel2);
		this.add(this.spinner2);
	    
	    final JButton button = new JButton("Auftragsdaten importieren");
	    button.addActionListener(this);
	    this.add(button);	
	}
	
	
	public void actionPerformed(ActionEvent evt) {
	    Object src = evt.getSource();
	    
		final ImportDataGenerator importDataGenerator = new ImportDataGenerator();
		
		final boolean simulation = false;
		
		final Date beginDate = (Date) this.spinner.getValue();
		final Date endDate = (Date) this.spinner2.getValue();
		
		try {
			importDataGenerator.startProcess(simulation, beginDate, endDate);
		} catch (SQLException e) {
			e.printStackTrace();
		}    
			
		}
	}




