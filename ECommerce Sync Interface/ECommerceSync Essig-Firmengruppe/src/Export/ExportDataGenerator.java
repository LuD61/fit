package Export;

import java.io.IOException;
import java.io.Reader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JOptionPane;

import fit.informixconnector.InformixConnector;
import Connection.MySQLConnectorMYSQL;
import DataEnumerations.ShippingState;
import DataEnumerations.TaxClass;
import ExportData.Categorie;
import ExportData.Customer;
import ExportData.Product;
import ExportData.ProductAttributes;
import ExportData.ProductDescription;
import ExportData.ProductOption;
import ExportData.ProductOptionValue;
import ExportData.ProductOptionValuesToProductOptions;
import ExportData.ProductsCustomers;
import MailService.SendJavaMail;

public class ExportDataGenerator {	
	
	HashMap<Integer, int[]> customerProductsRelation = null;
	public int productOptionsCounter = 1;
	
	public void startProcess(final boolean withDialog) {
		
		final SendJavaMail sendJavaMail = new SendJavaMail();
		
		if ( withDialog ) {
			int ok = JOptionPane.showConfirmDialog(null, "M�chten Sie den Export jetzt starten?", "Exportvorgang starten?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (ok == JOptionPane.NO_OPTION) {
				System.out.println("Synchonisation durch Benutzer abgebrochen!");
				JOptionPane.showMessageDialog(null, "Synchronisation abgebrochen", "Vorgang abgebrochen", JOptionPane.ERROR_MESSAGE);
				System.exit(0);
			}
		}		
		
		InformixConnector informixConnector = new InformixConnector();
		informixConnector.createConnection();
		MySQLConnectorMYSQL mySQLConnector = new MySQLConnectorMYSQL();	
		mySQLConnector.createConnection("127.0.0.1", "10101", "db222645_6", "db222645_6", "essig2009", true);
		
		ArrayList<Customer> customers = null;
		ArrayList<Categorie> categories = null;
		ArrayList<Product> products = null;
		ArrayList<ProductOption> productOptions = null;
		ArrayList<ProductsCustomers> productCustomers = null;
		
		try {
			customers = this.getCustomers(informixConnector);
			productCustomers = this.getProductsCustomers(informixConnector);
			categories = this.getCategories(informixConnector);
			products = this.getProducts(informixConnector);
			productOptions = this.getProductOptions(informixConnector);
		} catch (SQLException e) {
			e.printStackTrace();
			if ( withDialog ) {
				JOptionPane.showMessageDialog(null, "Synchronisation mit Fehler beendet, bitte Verbindung zur Datenbank pr�fen!", "Fehler", JOptionPane.ERROR_MESSAGE);	
			}
			sendJavaMail.sendEmail("Fehler beim Senden der Stammdaten.", "Fehler beim Senden der Stammdaten", "info@essig-firmengruppe.de");
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			if ( withDialog ) {
				JOptionPane.showMessageDialog(null, "Synchronisation mit Fehler beendet, bitte Verbindung zur Datenbank pr�fen!", "Fehler", JOptionPane.ERROR_MESSAGE);	
			}
			sendJavaMail.sendEmail("Fehler beim Senden der Stammdaten.", "Fehler beim Senden der Stammdaten", "info@essig-firmengruppe.de");
			System.exit(0);
		}		
		informixConnector.closeConnection();		
		
		this.insertCustomersToMySQL(customers, mySQLConnector);
		System.out.println("Customer successful inserted");
		this.insertProductsCustomersToMySQL(productCustomers, mySQLConnector);
		System.out.println("Products_to_Customer successful inserted");
		this.insertCategoriesToMySQL(categories, mySQLConnector);
		System.out.println("Categories successful inserted");
		this.insertProductsToMySQL(products, mySQLConnector);
		System.out.println("Products successful inserted");
		this.insertProductOptionsToMySQL(productOptions, mySQLConnector);
		System.out.println("ProductsOptions successful inserted");
		
		this.insertDataFromTMPInRealTables(mySQLConnector);
		System.out.println("Data copied from temporary tables into real tables");
		
		mySQLConnector.closeConnection();
		
		System.out.println("Synchonisation erfolgreich");
		
		if ( withDialog ) {
			JOptionPane.showMessageDialog(null, "Synchonisation erfolgreich", "Information", JOptionPane.INFORMATION_MESSAGE);	
		}		
		
		sendJavaMail.sendEmail("Stammdaten erfolgreich an den WebShop gesendet.", "Synchronisation der Stammdaten erfolgreich", "m.wiesler-lang@essig-firmengruppe.de");
		
		System.exit(0);
		
	}	
	
	
	
	private ArrayList<ProductsCustomers> getProductsCustomers(InformixConnector informixConnector) throws SQLException {
		
		final ArrayList<ProductsCustomers> productsCustomers = new ArrayList<ProductsCustomers>();
		
		final String stmt =
				"select ipr.a, ipr.kun" +
				" from " +
				"ipr, kun, ag, a_bas" +
				" where " +
				"ipr.kun = kun.kun" +
				" and " +
				"ipr.a = a_bas.a" +
				" and " +
				"a_bas.ag = ag.ag" +
				" and " +
				"kun.shop_kz == 'J'" +
				" and " +
				"ag.verk_beg between today+2 and today+20" +
				" and " +
				"ag.shop_kz = 'J'" +
				" order by " + 
				"ipr.a, ipr.kun" +				
				";";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);		
		
		while(resultSet.next()) {			
			final int a = resultSet.getInt("a");
			final int kun = resultSet.getInt("kun");
			
			final ProductsCustomers proCustomers = new ProductsCustomers(a, kun);
			productsCustomers.add(proCustomers);
		}
		
		return productsCustomers;
	}



	private ArrayList<Customer> getCustomers(final InformixConnector informixConnector) throws SQLException, IOException {
		
		final ArrayList<Customer> customers = new ArrayList<Customer>();
		
		final String stmt =
				"select kun, kun_krz1, kun_bran, shop_kz, shop_passw, adr_krz, adr_nam1, adr_nam2, anr, fax, geb_dat, land, ort1, plz, staat, str, tel, email, adr_nam3, mobil" +
				" from " +
				"kun, adr" +
				" where " +
				"kun.adr3 = adr.adr" +
				" and " +
				"kun.shop_kz == 'J'" +
				";";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		
		
		//Administrator User wird als Kunde angelegt.
		final Customer administrator = new Customer(3900, 0, "SE.TEC GmbH", 1, "Daniel", "Heil", "Kapellenstrasse 2", "78727", "Oberndorf a.N.", 81, 0, "07423 - 81095 - 0", "07423 - 81095 - 10", new Date());
		administrator.setEmailAdress("daniel.heil@setec-systeme.de");
		administrator.setPassword("t0pfit");
		administrator.setCustomerId(1);
		customers.add(administrator);
		
		
		while(resultSet.next()) {
			
			try {				
				final int kun = resultSet.getInt("kun");			
				final String shop_passw = resultSet.getString("shop_passw").trim();			
				final String adr_nam1 = resultSet.getString("adr_nam1").trim();
				final int anr = resultSet.getInt("anr");
				final String fax = resultSet.getString("fax");
				final Date geb_dat = resultSet.getDate("geb_dat");
				final String ort1 = resultSet.getString("ort1").trim();
				final String plz = resultSet.getString("plz").trim();
				final String str = resultSet.getString("str").trim();
				final String tel = resultSet.getString("tel");
				final String email = resultSet.getString("email").trim();
				
				final Customer customer = new Customer(kun, 2, adr_nam1, anr, "", "", str, plz, ort1, 81, 0, tel, fax, geb_dat);
				customer.setEmailAdress(email);
				customer.setPassword(shop_passw);
				customers.add(customer);
				
			} catch (Exception e){				
				e.printStackTrace();
				continue;				
			}
		}	
		return customers;	
	}
	
	
	
	private void insertCustomersToMySQL(final ArrayList<Customer> customers, final MySQLConnectorMYSQL mySQLConnector) {
		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_customers; CREATE TABLE tmp_customers LIKE customers;");
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_address_book; CREATE TABLE tmp_address_book LIKE address_book;");		
		
		final Iterator<Customer> it_customers = customers.iterator();
		
		boolean firstCustomerRun = true;
		StringBuilder stringBuilderCustomer = new StringBuilder();
		StringBuilder stringBuilderAddress = new StringBuilder();
		
		while (it_customers.hasNext()) {
			Customer customer = (Customer) it_customers.next();
			if ( firstCustomerRun ) {
				stringBuilderCustomer.append(customer.getInsertStatement());
				stringBuilderAddress.append(customer.getCustomerAddress().getInsertStatement());
				firstCustomerRun = false;
			} else {
				stringBuilderCustomer.append(customer.getValuesStatement());
				stringBuilderAddress.append(customer.getCustomerAddress().getValuesStatement());
			}			
		}
		
		mySQLConnector.executeUpdate(stringBuilderCustomer.toString());
		mySQLConnector.executeUpdate(stringBuilderAddress.toString());
	}
	
	
	
	private ArrayList<Categorie> getCategories(final InformixConnector informixConnector) throws SQLException {
		
		SimpleDateFormat agFormat = new SimpleDateFormat("EEEE | dd.MM.yyyy");
		SimpleDateFormat kwString = new SimpleDateFormat("w | yyyy");
		final ArrayList<Categorie> categories = new ArrayList<Categorie>();
		
		final String stmt = 
					"select wg.wg, wg_bz2, wg.verk_beg wgverkbeg, wg.verk_end, ag, ag_bz1, ag.verk_beg agverkbeg" +
					" from " +
					"ag, wg" +
					" where " +
					"wg.wg = ag.wg" +
					" and " +
					"ag.verk_beg between today+2 and today+20" +
					" and " +
					"ag.shop_kz = 'J' " +
					"order by wgverkbeg, agverkbeg;";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		
		int wg_counter = 1;
		int ag_counter = 1;
		int last_wg = 0;
		
		while(resultSet.next()) {

			final int wg = resultSet.getInt("wg");
			Date wg_verk_beg = resultSet.getDate("wgverkbeg");			
			String wg_bz2 = "KW " + kwString.format(wg_verk_beg);
			final int ag = resultSet.getInt("ag");
			Date ag_verk_beg = resultSet.getDate("agverkbeg");
			String ag_bz1 = agFormat.format(ag_verk_beg);
						
			if ( last_wg != wg || wg_counter == 1 ) {
				ag_counter = 1;
				final Categorie wg_categorie = new Categorie(wg, 0, wg_bz2, wg_bz2, wg_counter, wg_verk_beg);
				categories.add(wg_categorie);
				wg_counter++;
			}			
			
			final Categorie ag_categorie = new Categorie(ag, wg, ag_bz1, ag_bz1, ag_counter, ag_verk_beg);
			categories.add(ag_categorie);
			ag_counter++;
			
			last_wg = wg;			
		}		
		return categories;	


	}	
	
	
	
	private void insertCategoriesToMySQL(ArrayList<Categorie> categories, final MySQLConnectorMYSQL mySQLConnector) {
		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_categories_description; CREATE TABLE tmp_categories_description LIKE categories_description;");
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_categories; CREATE TABLE tmp_categories LIKE categories;");
		final Iterator<Categorie> it_categories = categories.iterator();
		
		while (it_categories.hasNext()) {
			Categorie categorie = (Categorie) it_categories.next();			
			mySQLConnector.executeUpdate(categorie.getInsertStatement());			
		}
	}
	
	
	
	private ArrayList<Product> getProducts(final InformixConnector informixConnector) throws SQLException {
		
		final ArrayList<Product> products = new ArrayList<Product>();
		
		final String stmt = 
					"select a_bas.a, a_bz1, a_bz2, a_bas.ag, bild, produkt_info, brennwert, eiweiss, kh, fett, zutat" +
					" from " +
					"a_bas, a_bas_erw, ag" +
					" where " +
					"a_bas.ag = ag.ag" +
					" and " +
					"ag.verk_beg between today+2 and today+20" +
					" and " +
					"ag.shop_kz = 'J' " +
					" and " +
					"a_bas.a = a_bas_erw.a" +
					" order by a_bas.a;";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		
		while(resultSet.next()) {

			final int a = resultSet.getInt("a");
			String a_bz1 = resultSet.getString("a_bz1").trim();
			final int ag = resultSet.getInt("ag");
			//String bild = resultSet.getString("bild");
			//if (bild != "") {
			//	File file = new File(bild);
			//	file.getName();	
			//}
			
			String products_description = "";
			String produkt_info = resultSet.getString("produkt_info");
			products_description = resultSet.getString("zutat");
//			try {				
//				products_description = this.getString(resultSet, "txt");
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
			
			final ProductDescription productDescription = new ProductDescription(a, a_bz1, products_description, produkt_info);
			final Product product = new Product(a, "noimage.gif", 0, 0, TaxClass.STANDARD19, false, 0, ShippingState.DAY, productDescription, ag);
			products.add(product);
		}	
		
		return products;
		
	}


		
		
	private void insertProductsToMySQL(ArrayList<Product> products, final MySQLConnectorMYSQL mySQLConnector) {		
		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_products_description; CREATE TABLE tmp_products_description LIKE products_description;");
		
		StringBuilder productsStmt = new StringBuilder();
		StringBuilder productsToCategoriesStmt = new StringBuilder();
		
		final Iterator<Product> it_products = products.iterator();
		boolean firstRun = true;
		
		while (it_products.hasNext()) {
			Product product = (Product) it_products.next();
			if ( firstRun ) {
				productsStmt.append(product.getInsertStatement());
				productsToCategoriesStmt.append(product.getProductCategorieInsertStatement());
				firstRun = false;
			} else {
				productsStmt.append(product.getValuesStatement());
				productsToCategoriesStmt.append(product.getProductCategorieValuesStatement());
			}			
		}
		
		boolean firstRunDesc = true;
		StringBuilder productsDesc = new StringBuilder();
		
		for (int i = 0; i < products.size(); i++) {
			
			Product product = products.get(i);
			
			if ( firstRunDesc ) {
				productsDesc.append(product.getProductDescription().getInsertStatement());
				firstRunDesc = false;
			} else {
				productsDesc.append(product.getProductDescription().getValuesStatement());
			}			
						
			if ( i % 100 == 0 ) {
				mySQLConnector.executeUpdate(productsDesc.toString());
				productsDesc = new StringBuilder();
				firstRunDesc = true;
			}			
		}
		
		if ( productsDesc.length() > 0 ) {
			mySQLConnector.executeUpdate(productsDesc.toString());	
		}		
		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_products; CREATE TABLE tmp_products LIKE products;");
		mySQLConnector.executeUpdate(productsStmt.toString());		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_products_to_categories; CREATE TABLE tmp_products_to_categories LIKE products_to_categories;");
		mySQLConnector.executeUpdate(productsToCategoriesStmt.toString());		
		
		mySQLConnector.executeUpdate("UPDATE tmp_products SET products_image := 'fisch.jpg'; ");
		
		mySQLConnector.executeUpdate("UPDATE tmp_products p, tmp_products_description pd SET products_image := 'salat.jpg'" +
										" WHERE p.products_id = pd.products_id" +
										" AND pd.products_name LIKE '%salat%'; ");
		
		mySQLConnector.executeUpdate("UPDATE tmp_products p, tmp_products_description pd SET products_image := 'suppe.jpg'" +
										" WHERE p.products_id = pd.products_id" +
										" AND pd.products_name LIKE '%suppe%'; ");

		mySQLConnector.executeUpdate("UPDATE tmp_products p, tmp_products_description pd SET products_image := 'dessert.jpg'" +
										" WHERE p.products_id = pd.products_id" +
										" AND pd.products_name LIKE '%dessert%'; ");
	
	}
	
	
	
	private ArrayList<ProductOption> getProductOptions(final InformixConnector informixConnector) throws SQLException {
		
		final ArrayList<ProductOption> productOptions = new ArrayList<ProductOption>();
		
		final String stmt = 
					"select a, a_beilage, sort, a_beilage_bez" +
					" from " +
					"shop_beilagen" +
					" order by a, sort" +
					";";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		int last_a = 0;
		int products_options_id = 1;
		int id = 1;
		ProductOption productOption = null;
		
		while(resultSet.next()) {

			final int a = resultSet.getInt("a");
			final int a_beilage = resultSet.getInt("a_beilage");
			final String a_beilage_bez = resultSet.getString("a_beilage_bez").trim();
			
			if ( last_a != a || products_options_id == 1 ) {
				productOption = new ProductOption(products_options_id, "Beilagen", a);
				productOptions.add(productOption);
				products_options_id++;
			}
			
			productOption.addProductionOptionValues(a_beilage, a_beilage_bez);
			productOption.addProductAttributes(id, a_beilage);
			productOption.addProductOptionValuesToProductOptions(id, a_beilage);
			last_a = a;
			id++;
		}	
		
		return productOptions;
	}	
	
	
	
	
	private void insertProductOptionsToMySQL(ArrayList<ProductOption> productOptions, final MySQLConnectorMYSQL mySQLConnector) {
		
		boolean firstRun = true;
		boolean firstRun2 = true;
		boolean firstRun3 = true;
		boolean firstRun4 = true;
		
		StringBuilder productsOptionStmt = new StringBuilder();
		StringBuilder productsOptionValueStmt = new StringBuilder();
		StringBuilder productsOptionValuesToProductOptionsStmt = new StringBuilder();
		StringBuilder productsAttributesStmt = new StringBuilder();				
		
		final Iterator<ProductOption> it_productOptions = productOptions.iterator();		
				
		while (it_productOptions.hasNext()) {
			
			ProductOption productOption = (ProductOption) it_productOptions.next();
			
			if ( firstRun ) {
				
				productsOptionStmt.append(productOption.getInsertStatement());

				// ProductOptionValues				
				Iterator<ProductOptionValue> it = productOption.productOptionValues.iterator();
				while (it.hasNext()) {
					ProductOptionValue productOptionValue = it.next();
					if ( firstRun2 ) {
						String productOptionValueStmt = productOptionValue.getInsertStatement();
						productsOptionValueStmt.append(productOptionValueStmt);
						firstRun2 = false;
					} else {
						String productOptionValueStmt = productOptionValue.getValuesStatement();
						productsOptionValueStmt.append(productOptionValueStmt);
					}			
				}
				
				// ProductOptionValues2ProductOptions
				Iterator<ProductOptionValuesToProductOptions> it2 = productOption.productOptionValuesToProductOptions.iterator();
				while (it2.hasNext()) {
					ProductOptionValuesToProductOptions productOptionValuesToProductOptions = it2.next();
					if ( firstRun3 ) {
						String productOptionValuesToProductOptionsStmt = productOptionValuesToProductOptions.getInsertStatement();
						productsOptionValuesToProductOptionsStmt.append(productOptionValuesToProductOptionsStmt);
						firstRun3 = false;
					} else {
						String productOptionValuesToProductOptionsStmt = productOptionValuesToProductOptions.getValuesStatement();
						productsOptionValuesToProductOptionsStmt.append(productOptionValuesToProductOptionsStmt);
					}
				}
				
				//ProductAttributes				
				Iterator<ProductAttributes> it3 = productOption.productAttributes.iterator();
				while (it3.hasNext()) {
					ProductAttributes productAttribute = it3.next();
					if ( firstRun4 ) {
						String productAttributesStmt = productAttribute.getInsertStatement();
						productsAttributesStmt.append(productAttributesStmt);
						firstRun4 = false;
					} else {
						String productAttributesStmt = productAttribute.getValuesStatement();
						productsAttributesStmt.append(productAttributesStmt);
					}					
				}
				
				firstRun = false;
				
			} else {
				
				productsOptionStmt.append(productOption.getValuesStatement());						

				// ProductOptionValues				
				Iterator<ProductOptionValue> it = productOption.productOptionValues.iterator();
				while (it.hasNext()) {
					ProductOptionValue productOptionValue = it.next();
					String productOptionValueStmt = productOptionValue.getValuesStatement();
					productsOptionValueStmt.append(productOptionValueStmt);
				}		
				
				// ProductOptionValues2ProductOptions
				Iterator<ProductOptionValuesToProductOptions> it2 = productOption.productOptionValuesToProductOptions.iterator();
				while (it2.hasNext()) {
					ProductOptionValuesToProductOptions productOptionValuesToProductOptions = it2.next();
					String productOptionValuesToProductOptionsStmt = productOptionValuesToProductOptions.getValuesStatement();
					productsOptionValuesToProductOptionsStmt.append(productOptionValuesToProductOptionsStmt);
				}
				
				//ProductAttributes
				Iterator<ProductAttributes> it3 = productOption.productAttributes.iterator();
				while (it3.hasNext()) {
					ProductAttributes productAttribute = it3.next();
					String productAttributesStmt = productAttribute.getValuesStatement();
					productsAttributesStmt.append(productAttributesStmt);					
				}				
			}					
		}
		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_products_options; CREATE TABLE tmp_products_options LIKE products_options;");		
		mySQLConnector.executeUpdate(productsOptionStmt.toString());
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_products_options_values; CREATE TABLE tmp_products_options_values LIKE products_options_values;");
		mySQLConnector.executeUpdate(productsOptionValueStmt.toString());
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_products_options_values_to_products_options; CREATE TABLE tmp_products_options_values_to_products_options LIKE products_options_values_to_products_options;");
		mySQLConnector.executeUpdate(productsOptionValuesToProductOptionsStmt.toString());
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_products_attributes; CREATE TABLE tmp_products_attributes LIKE products_attributes;");
		mySQLConnector.executeUpdate(productsAttributesStmt.toString());
		
	}
	
	
	
	private void insertProductsCustomersToMySQL(final ArrayList<ProductsCustomers> productsCustomers, final MySQLConnectorMYSQL mySQLConnector) {
		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_products_to_customers; CREATE TABLE tmp_products_to_customers LIKE products_to_customers;");		

		StringBuilder stringBuilder = new StringBuilder();
		
		
		for (int i = 0; i < productsCustomers.size(); i++) {			
			final ProductsCustomers pc = productsCustomers.get(i);
			if ( i == 0 ) {
				stringBuilder.append(pc.getInsertStatement());
			} else {
				stringBuilder.append(pc.getValuesStatement());
			}			
		}
		
		mySQLConnector.executeUpdate(stringBuilder.toString());
	}
	
	
	
	private String getString(ResultSet rs, String columnName) throws SQLException, IOException {
		Reader r = rs.getCharacterStream(columnName);
		if (r == null)
			return null;
		
		StringBuilder tmp = new StringBuilder();
		char[] buf = new char[512];
		int lu;
		do {
			lu = r.read(buf);
			if (lu > 0)
				tmp.append(buf, 0, lu);
		} while (lu >= 0);
		return tmp.toString();
	}
	
	
	private void insertDataFromTMPInRealTables(final MySQLConnectorMYSQL mySQLConnector) {
		
		mySQLConnector.executeUpdate("DELETE FROM customers; INSERT INTO customers SELECT * FROM tmp_customers;");
		mySQLConnector.executeUpdate("DELETE FROM address_book; INSERT INTO address_book SELECT * FROM tmp_address_book;");
		
		mySQLConnector.executeUpdate("DELETE FROM categories_description; INSERT INTO categories_description SELECT * FROM tmp_categories_description;");
		mySQLConnector.executeUpdate("DELETE FROM categories; INSERT INTO categories SELECT * FROM tmp_categories;");
		
		mySQLConnector.executeUpdate("DELETE FROM products_description; INSERT INTO products_description SELECT * FROM tmp_products_description;");
		mySQLConnector.executeUpdate("DELETE FROM products_to_categories; INSERT INTO products_to_categories SELECT * FROM tmp_products_to_categories;");
		mySQLConnector.executeUpdate("DELETE FROM products; INSERT INTO products SELECT * FROM tmp_products;");
		
		mySQLConnector.executeUpdate("DELETE FROM products_options_values; INSERT INTO products_options_values SELECT * FROM tmp_products_options_values;");
		mySQLConnector.executeUpdate("DELETE FROM products_options_values_to_products_options; INSERT INTO products_options_values_to_products_options SELECT * FROM tmp_products_options_values_to_products_options;");
		mySQLConnector.executeUpdate("DELETE FROM products_options; INSERT INTO products_options SELECT * FROM tmp_products_options;");
		mySQLConnector.executeUpdate("DELETE FROM products_attributes; INSERT INTO products_attributes SELECT * FROM tmp_products_attributes;");
		
		mySQLConnector.executeUpdate("DELETE FROM products_to_customers; INSERT INTO products_to_customers SELECT * FROM tmp_products_to_customers;");
	}

	
	
	
	
}


