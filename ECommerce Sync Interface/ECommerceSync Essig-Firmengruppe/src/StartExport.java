import Export.ExportDataGenerator;

/**
 * 
 */

/**
 * @author danielh
 *
 */
public class StartExport {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		final boolean withDialog = Boolean.valueOf(args[0]);
		
		final ExportDataGenerator exportDataGenerator = new ExportDataGenerator();
		exportDataGenerator.startProcess(withDialog);
	}

}
