package ExportData;

import DataInterface.Statement;

public class ProductDescription implements Statement {

	private int products_id;
	private int language_id = 2;
	private String products_name;
	private String products_description;
	private String products_short_description;
	private String products_keywords;
	private String products_meta_title;
	private String products_meta_description;
	private String products_meta_keywords;
	private String products_url = "";
	private int products_viewed = 1000;

	public ProductDescription(final int products_Id,
			final String products_name, final String products_description,
			final String products_short_description) {
		
		this.products_id = products_Id;
		this.products_name = products_name;
		this.products_description = products_description;
		this.products_short_description = products_short_description;
		this.products_keywords = this.products_short_description;
		this.products_meta_title = this.products_name;
		this.products_meta_description = this.products_description;
		this.products_meta_keywords = this.products_short_description;
				
	}

	@Override
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String stmt =
				"INSERT IGNORE INTO `tmp_products_description`" +
				"(`products_id`," +
				"`language_id`," +
				"`products_name`," +
				"`products_description`," +
				"`products_short_description`," +
				"`products_keywords`," +
				"`products_meta_title`," +
				"`products_meta_description`," +
				"`products_meta_keywords`," +
				"`products_url`," +
				"`products_viewed`" +
				
				") VALUES ('" +
				this.products_id + "','" +
				this.language_id + "','" +
				this.products_name + "','" +
				this.products_description + "','" +
				this.products_short_description + "','" +
				this.products_keywords + "','" +
				this.products_meta_title + "','" +
				this.products_meta_description + "','" +
				this.products_meta_keywords + "','" +
				this.products_url + "','" +
				this.products_viewed +
				"')";
			
			return stmt;
	}


	public String getValuesStatement() {
		
		String stmt =
				" ,('" +
				this.products_id + "','" +
				this.language_id + "','" +
				this.products_name + "','" +
				this.products_description + "','" +
				this.products_short_description + "','" +
				this.products_keywords + "','" +
				this.products_meta_title + "','" +
				this.products_meta_description + "','" +
				this.products_meta_keywords + "','" +
				this.products_url + "','" +
				this.products_viewed +
				"')";
			
			return stmt;
	}
	
	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `products_description`;";
		return stmt;
	}

}
