package ExportData;

import DataInterface.Statement;

public class CustomerStatus implements Statement {
	
	private int customers_status_id;
	private int language_id = 2;
	private String customers_status_name;
	private int customers_status_public = 0;
	private int customers_status_min_order = 0;
	private int customers_status_max_order = 0;
	private String customers_status_image = "customer_status.gif";
	private int customers_status_discount = 0;
	private String customers_status_ot_discount_flag = "0";	
	private int customers_status_ot_discount = 0;
	private String customers_status_graduated_prices = "1";
	private int customers_status_show_price = 1;
	private int customers_status_show_price_tax = 1;
	private int customers_status_add_tax_ot = 0;
	private String customers_status_payment_unallowed = "";
	private String customers_status_shipping_unallowed = "";
	private int customers_status_discount_attributes = 0;
	private int customers_fsk18 = 1;
	private int customers_fsk18_display = 1;
	private int customers_status_write_reviews = 1; //TODO Bewertungen schreiben
	private int customers_status_read_reviews = 1; //TODO Bewertungen lesen
	
	
	public CustomerStatus(final int customerStatusId, final String customerStatusName) {
		this.customers_status_id = customerStatusId;
		this.customers_status_name = customerStatusName;
	}


	@Override
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getInsertStatement() {
		
		String stmt =
				"INSERT INTO `customers_status` (`customers_status_id`" +
				", `language_id`" +
				", `customers_status_name`" +
				", `customers_status_public`" +
				", `customers_status_min_order`" +
				", `customers_status_max_order`" +
				", `customers_status_image`" +
				", `customers_status_discount`" +
				", `customers_status_ot_discount_flag`" +
				", `customers_status_ot_discount`" +
				", `customers_status_graduated_prices`" +
				", `customers_status_show_price`" +
				", `customers_status_show_price_tax`" +
				", `customers_status_add_tax_ot`" +
				", `customers_status_payment_unallowed`" +
				", `customers_status_shipping_unallowed`" +
				", `customers_status_discount_attributes`" +
				", `customers_fsk18`" +
				", `customers_fsk18_display`" +
				", `customers_status_write_reviews`" +
				", `customers_status_read_reviews`) " +
				
				"VALUES ('" + this.customers_status_id +
				"','" + this.language_id + 
				"','" + this.customers_status_name +
				"','" + this.customers_status_public + 
				"','" + this.customers_status_min_order +
				"','" + this.customers_status_max_order +
				"','" + this.customers_status_image + 
				"','" + this.customers_status_discount +
				"','" + this.customers_status_ot_discount_flag +
				"','" + this.customers_status_ot_discount +
				"','" + this.customers_status_graduated_prices +
				"','" + this.customers_status_show_price +
				"','" + this.customers_status_show_price_tax +
				"','" + this.customers_status_add_tax_ot +
				"','" + this.customers_status_payment_unallowed +
				"','" + this.customers_status_shipping_unallowed +
				"','" + this.customers_status_discount_attributes +
				"','0'" +
				",'1'" +
				",'1'" +
				",'1'" +
				",'1');";
		
				
		return stmt;
	}


	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `customers_status`;";
		return stmt;
	}
	
	

}
