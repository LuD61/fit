package ExportData;


import java.util.Date;

import DataInterface.Statement;

public class Categorie implements Statement {

	private int categories_id;
	private String categories_image;
	private int parent_id;
	private int categories_status = 1;
	private String categories_template = "categorie_listing.html";

	// TODO customer_groups
	private int group_permission_0 = 1;
	private int group_permission_1 = 0;
	private int group_permission_2 = 1;

	private String listing_template = "product_listing_v1.html";
	private int sort_order;
	private String products_sorting = "p.products_id";
	private String product_sorting2 = "ASC";
	private Date date_added;
	
	private CategorieDescription categorieDescription;

	
	
	public Categorie(final int id, final int parentId,
			final String categories_name,
			final String categories_heading_title, final int sort_order, final Date verk_beg) {
		this.categories_id = id;
		this.parent_id = parentId;
		this.sort_order = sort_order;
		this.date_added = new java.sql.Date(verk_beg.getTime());
		this.categorieDescription = new CategorieDescription(id,
				categories_name, categories_heading_title);
	}

	@Override
	public String getSelectStatement() {
		String description = this.categorieDescription.getSelectStatement();
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {

		String descriptionStmt = this.categorieDescription.getInsertStatement();

		String stmt = "INSERT INTO `tmp_categories`	(`categories_id`,"
				+ "`categories_image`," + "`parent_id`,"
				+ "`categories_status`," + "`categories_template`,"
				+ "`group_permission_0`," + "`group_permission_1`,"
				+ "`group_permission_2`," + "`listing_template`,"
				+ "`sort_order`," + "`products_sorting`,"
				+ "`products_sorting2`," + "`date_added`,"
				+ "`last_modified`) " +

				"VALUES (" +
				"'" + this.categories_id
				+ "','"
				+ this.categories_image
				+ "','"
				+ this.parent_id
				+ "','"
				+ this.categories_status
				+ "','"
				+ this.categories_template
				+ "','"
				+ this.group_permission_0
				+ "','"
				+ this.group_permission_1
				+ "','"
				+ this.group_permission_2
				+ "','"
				+ this.listing_template
				+ "','"
				+ this.sort_order
				+ "','"
				+ this.products_sorting
				+ "','"
				+ this.product_sorting2
				+ "','" + this.date_added + " 00:00:00" + "'"
				+ ",'0000-00-00 00:00:00'" 
				+ ");";

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(descriptionStmt);
		stringBuilder.append(stmt);

		return stringBuilder.toString();
	}

	@Override
	public String getUpdateStatement() {
		return null;
	}
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `categories`;";
		return stmt;
	}

	public void setCategories_image(String categories_image) {
		this.categories_image = categories_image;
	}

	public CategorieDescription getCategorieDescription() {
		return this.categorieDescription;
	}

}
