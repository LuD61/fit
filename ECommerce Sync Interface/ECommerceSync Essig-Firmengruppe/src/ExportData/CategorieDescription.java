package ExportData;

import DataInterface.Statement;

public class CategorieDescription implements Statement {

	private int categories_id;
	private int language_id = 2;
	private String categories_name;
	private String categories_heading_title;
	private String categories_description; //HTML
	private String categories_meta_title;
	private String categories_meta_description;
	private String categories_meta_keywords;
	
	
	public CategorieDescription(final int categories_id, final String categories_name, final String categories_heading_title) {
		this.categories_id = categories_id;
		this.categories_name = categories_name;
		this.categories_heading_title = categories_heading_title;
		this.setCategories_description(categories_name);
		this.setCategories_meta_description(categories_name);
		this.setCategories_meta_keywords(categories_name);
		this.setCategories_meta_title(categories_name);
	}
	
	@Override
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String stmt =
			"INSERT INTO `tmp_categories_description`" +
			"(`categories_id`," +
			"`language_id`," +
			"`categories_name`," +
			"`categories_heading_title`," +
			"`categories_description`," +
			"`categories_meta_title`," +
			"`categories_meta_description`," +
			"`categories_meta_keywords`" +
			")" +
			
			"VALUES('" +
			this.categories_id + "','" +
			this.language_id + "','" +
			this.categories_name + "','" +
			this.categories_heading_title + "','" +
			this.categories_description + "','" +
			this.categories_meta_title + "','" +
			this.categories_meta_description + "','" +
			this.categories_meta_keywords +
			"');";
		
		return stmt;
	}

	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `categories_description`;";
		return stmt;
	}
	


	public void setCategories_description(String categories_description) {
		this.categories_description = categories_description;
	}


	public void setCategories_meta_title(String categories_meta_title) {
		this.categories_meta_title = categories_meta_title;
	}


	public void setCategories_meta_description(
			String categories_meta_description) {
		this.categories_meta_description = categories_meta_description;
	}


	public void setCategories_meta_keywords(String categories_meta_keywords) {
		this.categories_meta_keywords = categories_meta_keywords;
	}
	
	

}
