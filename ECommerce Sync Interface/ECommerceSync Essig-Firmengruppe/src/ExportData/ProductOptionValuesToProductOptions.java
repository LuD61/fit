package ExportData;

import DataInterface.Statement;

public class ProductOptionValuesToProductOptions implements Statement {
	
	
	private int products_options_id = 2;
	private int products_options_values_id;
	private int id;
	
	public ProductOptionValuesToProductOptions(final int id, final int products_options_id, final int products_options_values_id) {
		this.id = id;
		this.products_options_id = products_options_id;
		this.products_options_values_id = products_options_values_id;
	}	
	

	@Override
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String stmt = 
		
				"INSERT IGNORE INTO `tmp_products_options_values_to_products_options`" +
				"(`products_options_values_to_products_options_id`," +
				"`products_options_id`," +
				"`products_options_values_id`" +
				
				") VALUES ('" + this.id + "'," +
				"'" + this.products_options_id + "'," +
				"'" + this.products_options_values_id + "'" +
				")";
				
				
		return stmt;
	}
	
	public String getValuesStatement() {
		
		String stmt =				
				" ,('" + this.id + "'," +
				"'" + this.products_options_id + "'," +
				"'" + this.products_options_values_id + "'" +
				")";				
				
		return stmt;
	}

	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `products_options_values_to_products_options`;";
		return stmt;
	}
	
	

}
