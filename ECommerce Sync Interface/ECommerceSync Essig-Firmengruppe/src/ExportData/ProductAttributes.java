package ExportData;

import DataInterface.Statement;

public class ProductAttributes implements Statement {

	private int id;
	private int products_id;
	private int options_id;
	private int options_values_id;

	private double options_values_price;
	private String price_prefix;

	private String attributes_model = "";
	private int attributes_stock = 0;

	private double options_values_weight;
	private String weight_prefix;

	private int sortorder;

	public ProductAttributes(final int id, final int products_id, final int options_id,
			final int options_values_id, final int sortOrder) {
		
		this.id = id;
		this.products_id = products_id;
		this.options_id = options_id;
		this.options_values_id = options_values_id;
		this.options_values_price = 0;
		this.price_prefix = "+";
		this.options_values_weight = 0;
		this.weight_prefix = "+";
		this.sortorder = sortOrder;
		this.attributes_model = String.valueOf(options_values_id);
	}

	@Override
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String stmt = 
				"INSERT IGNORE INTO `tmp_products_attributes`" +
				"(`products_attributes_id`," +
				"`products_id`," +
				"`options_id`," +
				"`options_values_id`," +
				"`options_values_price`," +
				"`price_prefix`," +
				"`attributes_model`," +
				"`attributes_stock`," +
				"`options_values_weight`," +
				"`weight_prefix`," +
				"`sortorder`" +
				")	VALUES (" +
				"'" + this.id + "'," +
				"'" + this.products_id + "'," +
				"'" + this.options_id + "'," +
				"'" + this.options_values_id + "'," +
				"'" + this.options_values_price + "'," +
				"'" + this.price_prefix + "'," +
				"'" + this.attributes_model + "'," +
				"'" + this.attributes_stock + "'," +
				"'" + this.options_values_weight + "'," +
				"'" + this.weight_prefix + "'," +
				"'" + this.sortorder + "'" +
				")";
		
		return stmt;
	}

	public String getValuesStatement() {
		
		String stmt = " ,(" +
				"'" + this.id + "'," +
				"'" + this.products_id + "'," +
				"'" + this.options_id + "'," +
				"'" + this.options_values_id + "'," +
				"'" + this.options_values_price + "'," +
				"'" + this.price_prefix + "'," +
				"'" + this.attributes_model + "'," +
				"'" + this.attributes_stock + "'," +
				"'" + this.options_values_weight + "'," +
				"'" + this.weight_prefix + "'," +
				"'" + this.sortorder + "'" +
				")";
		
		return stmt;
	}
	
	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `products_attributes`;";
		return stmt;
	}

}
