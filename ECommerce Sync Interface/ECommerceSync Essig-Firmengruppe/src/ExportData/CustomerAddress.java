package ExportData;

import java.util.Date;

import DataEnumerations.Country;
import DataEnumerations.Gender;
import DataEnumerations.Zones;
import DataInterface.Statement;

public class CustomerAddress implements Statement {

	private int address_book_id;
	private int customers_id;
	private String entry_gender;
	private String entry_company;
	private String entry_firstname;
	private String entry_lastname;
	private String entry_street_address;
	private String entry_suburb = "";
	private String entry_postcode;
	private String entry_city;
	private String entry_state = "";
	private int entry_country_id;
	private int entry_zone_id;
	private Date address_date_added;
	private Date address_last_modified;

	public CustomerAddress(final int customerId, final String company,
			final String firstName, final String lastName, final String street,
			final String postCode, final String city, final String gender,
			final int country, final int zone) {

		this.entry_company = company;
		this.address_book_id = customerId;
		this.customers_id = customerId;
		this.entry_firstname = firstName;
		this.entry_lastname = lastName;
		this.entry_street_address = street;
		this.entry_postcode = postCode;
		this.entry_city = city;
		this.entry_gender = gender;		
		this.address_date_added = new Date();
		this.address_last_modified = new Date();
		this.entry_country_id = country;
		this.entry_zone_id = zone;
	}

	@Override
	public String getSelectStatement() {
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String stmt = 
				
				"INSERT IGNORE INTO `tmp_address_book` (`address_book_id`" +
				", `customers_id`" +
				", `entry_gender`" +
				", `entry_company`" +
				", `entry_firstname`" +
				", `entry_lastname`" +
				", `entry_street_address`" +
				", `entry_suburb`" +
				", `entry_postcode`" +
				", `entry_city`" +
				", `entry_state`" +
				", `entry_country_id`" +
				", `entry_zone_id`" +
				", `address_date_added`" +
				", `address_last_modified`)" +
				
				" VALUES ('" + this.address_book_id +
				"','" + this.customers_id +
				"','" + this.entry_gender +
				"','" + this.entry_company +
				"','" + this.entry_firstname +
				"','" + this.entry_lastname +
				"','" + this.entry_street_address +
				"',NULL" +
				",'" + this.entry_postcode +
				"','" + this.entry_city +
				"','" + this.entry_state +
				"','" + this.entry_country_id +
				"','" + this.entry_zone_id +
				"','0000-00-00 00:00:00'" +
				",'0000-00-00 00:00:00')";
		
		return stmt;
	}
	
	
	public String getValuesStatement() {
		
		String stmt = 
				" ,('" + this.address_book_id +
				"','" + this.customers_id +
				"','" + this.entry_gender +
				"','" + this.entry_company +
				"','" + this.entry_firstname +
				"','" + this.entry_lastname +
				"','" + this.entry_street_address +
				"',NULL" +
				",'" + this.entry_postcode +
				"','" + this.entry_city +
				"','" + this.entry_state +
				"','" + this.entry_country_id +
				"','" + this.entry_zone_id +
				"','0000-00-00 00:00:00'" +
				",'0000-00-00 00:00:00')";
		
		return stmt;
	}

	@Override
	public String getUpdateStatement() {
		return null;
	}
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `address_book`;";
		return stmt;
	}

}
