package ExportData;

import java.util.Date;

import Converter.MD5Converter;
import DataEnumerations.Country;
import DataEnumerations.Gender;
import DataEnumerations.Zones;
import DataInterface.Statement;

public class Customer implements Statement {

	private int customers_id;
	private String customers_cid;
	private String customers_vat_id = "";
	private int customers_vat_id_status = 0;
	private String customers_warning = "";
	private int customers_status;
	private String customers_gender;
	private String customers_firstname;
	private String customers_lastname;
	private Date customers_dob;
	private String customers_email_address = "";
	private int customers_default_address_id;
	private String customers_telephone;
	private String customers_fax;
	private String customers_password; // MD5 Hash
	private String customers_newsletter = "";
	private String customers_newsletter_mode = "0";
	private String member_flag = "0";
	private String delete_user = "0";
	private int account_type = 0;
	private String password_request_key = "";
	private String payment_unallowed = "";
	private String shipping_unallowed = "";
	private int refferers_id = 0;
	private Date customers_date_added;
	private Date customers_last_modified;
	private String companyName;

	private CustomerAddress customerAddress;

	public Customer(final int customerId, final int customers_status,
			final String company, final int gender, final String firstName,
			final String lastName, final String street, final String postCode,
			final String city, final int country, final int zone,
			final String tel, final String fax, final Date birthDay) {

		this.customers_id = customerId;
		this.customers_cid = String.valueOf(customerId);
		this.customers_status = customers_status;
		
		if ( gender == 1) {
			this.customers_gender = "m";
		} else if ( gender == 3 ){
			this.customers_gender = "f";
		} else {
			this.customers_gender = "m";
		}
		
		this.customers_firstname = firstName;
		this.customers_lastname = lastName;
		this.customers_dob = birthDay;
		this.customers_default_address_id = customerId;
		this.companyName = company;
		
		this.customerAddress = new CustomerAddress(customerId, company,
				firstName, lastName, street, postCode, city, this.customers_gender, 81, 0);
		this.customers_telephone = tel;
		this.customers_fax = fax;
		this.customers_date_added = new Date();
		this.customers_last_modified = new Date();
	}

	public void setPassword(final String password) {
		this.customers_password = MD5Converter.md5(password);
	}

	public void setUStId(final String ust_id) {
		this.customers_vat_id = ust_id;
	}

	public void setEmailAdress(final String email) {
		this.customers_email_address = email;
	}

	@Override
	public String getSelectStatement() {
		String customerAdressStatement = this.customerAddress
				.getSelectStatement();
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String customerStmt = 
				"INSERT IGNORE INTO `tmp_customers` (`customers_id`, `customers_cid`, `customers_vat_id`, `customers_vat_id_status`, `customers_warning`,`customers_status`, `customers_gender`, `customers_firstname`, `customers_lastname`, `customers_dob`, `customers_email_address`, `customers_default_address_id`, `customers_telephone`, `customers_fax`, `customers_password`, `customers_newsletter`, `customers_newsletter_mode`, `member_flag`, `delete_user`, `account_type`, `password_request_key`, `payment_unallowed`, `shipping_unallowed`, `refferers_id`, `customers_date_added`, `customers_last_modified`) " +
				"VALUES ('" + this.customers_id +
						"','" + this.customers_cid +
						"','" + this.customers_vat_id +
						"','" + this.customers_vat_id_status +
						"','" + this.customers_warning + 
						"','" + this.customers_status +
						"','" + this.customers_gender +
						"','" + this.customers_firstname +
						"','" + this.customers_lastname +
						"','0000-00-00 00:00:00'" +
						",'" + this.customers_email_address +
						"','" + this.customers_default_address_id + 
						"','" + this.customers_telephone + 
						"','" + this.customers_fax +
						"','" + this.customers_password +
						"','" + this.customers_newsletter + 
						"','" + this.customers_newsletter_mode +
						"','" + this.member_flag +
						"','" + this.delete_user +
						"','" + this.account_type + 
						"','" + this.password_request_key +
						"','" + this.payment_unallowed +
						"','" + this.shipping_unallowed +
						"','" + this.refferers_id +
						"','0000-00-00 00:00:00'" +
						",'0000-00-00 00:00:00')";
		
		return customerStmt;
	}

	public String getValuesStatement() {
		
		String customerStmt = 
				" ,('" + this.customers_id +
						"','" + this.customers_cid +
						"','" + this.customers_vat_id +
						"','" + this.customers_vat_id_status +
						"','" + this.customers_warning + 
						"','" + this.customers_status +
						"','" + this.customers_gender +
						"','" + this.customers_firstname +
						"','" + this.customers_lastname +
						"','0000-00-00 00:00:00'" +
						",'" + this.customers_email_address +
						"','" + this.customers_default_address_id + 
						"','" + this.customers_telephone + 
						"','" + this.customers_fax +
						"','" + this.customers_password +
						"','" + this.customers_newsletter + 
						"','" + this.customers_newsletter_mode +
						"','" + this.member_flag +
						"','" + this.delete_user +
						"','" + this.account_type + 
						"','" + this.password_request_key +
						"','" + this.payment_unallowed +
						"','" + this.shipping_unallowed +
						"','" + this.refferers_id +
						"','0000-00-00 00:00:00'" +
						",'0000-00-00 00:00:00')";
		
		return customerStmt;
	}

	@Override
	public String getUpdateStatement() {
		String customerAdressStatement = this.customerAddress
				.getUpdateStatement();
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `customers`;";
		return stmt;
	}
	
	
	public String getCustomerCID() {
		return this.customers_cid;
	}
	
	
	public int getCustomerStatus() {
		return this.customers_status;
	}
	
	public String getCompanyName() {
		return this.companyName;
	}
	
	
	public void setCustomerId(final int customers_Id) {
		this.customers_id = customers_Id;
	}
	
	
	public CustomerAddress getCustomerAddress() {
		return this.customerAddress;
	}
	

}
