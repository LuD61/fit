package ExportData;


import java.util.Date;

import DataEnumerations.ShippingState;
import DataEnumerations.TaxClass;
import DataInterface.Statement;


public class Product implements Statement {

	private int products_id;
	private String products_ean = "";
	private int products_quantity = 0;
	private int products_shippingtime; // shippingState Enumeration
	private String products_model = "";
	private int group_permission_0 = 1;
	private int group_permission_1 = 0;
	private int group_permission_2 = 1;
	private int products_sort = 0;
	private String products_image;
	private double products_price;
	private double products_discount_allowed = 0;
	private Date products_date_added;
	private Date products_last_modified;
	private Date products_date_available = null;
	private double products_weight;
	private int products_status = 1;
	private int products_tax_class_id; // tax_class Enumeration
	private String product_template = "product_info_v1.html";
	private String options_template = "product_options_selection.html";
	private int manufacturers_id = 0;
	private int products_ordered = 10;
	private int products_fsk18 = 0;
	private int products_vpe = 0; // products_vpe Enumeration
	private int products_vpe_status = 0;
	private double products_vpe_value = 0;
	private int products_startpage;
	private int products_startpage_sort;
	private int categorie;

	private ProductDescription productDescription;

	public Product(final int productId, final String products_image,
			final double products_price, final double products_weight,
			final TaxClass tax, final boolean showOnStartPage,
			final int startPagePosition, final ShippingState shippingState,
			final ProductDescription productDescription, final int categorie) {

		this.products_id = productId;
		this.products_image = products_image;
		this.products_price = products_price;
		this.products_weight = products_weight;
		this.products_tax_class_id = tax.getCode();
		this.products_model = String.valueOf(productId);

		if (showOnStartPage) {
			this.products_startpage = 1;
		} else {
			this.products_startpage = 0;
		}

		this.products_startpage_sort = startPagePosition;

		this.products_date_added = new java.sql.Date(new Date().getTime());
		this.products_last_modified = new java.sql.Date(new Date().getTime());
		this.products_date_available = new java.sql.Date(new Date().getTime());

		this.products_shippingtime = shippingState.getCode();

		this.productDescription = productDescription;
		
		this.categorie = categorie;

	}

	@Override
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {

		String productStmt =
				"INSERT IGNORE INTO `tmp_products`" +
				"(`products_id`," +
				"`products_ean`," +
				"`products_quantity`," +
				"`products_shippingtime`," +
				"`products_model`," +
				"`group_permission_0`," +
				"`group_permission_1`," +
				"`group_permission_2`," +
				"`products_sort`," +
				"`products_image`," +
				"`products_price`," +
				"`products_discount_allowed`," +
				"`products_date_added`," +
				"`products_last_modified`," +
				"`products_date_available`," +
				"`products_weight`," +
				"`products_status`," +
				"`products_tax_class_id`," +
				"`product_template`," +
				"`options_template`," +
				"`manufacturers_id`," +
				"`products_ordered`," +
				"`products_fsk18`," +
				"`products_vpe`," +
				"`products_vpe_status`," +
				"`products_vpe_value`," +
				"`products_startpage`," +
				"`products_startpage_sort`)" +
				
				"VALUES	(" +
				"'" + this.products_id + "'," +
				"'" + this.products_ean + "'," +
				"'" + this.products_quantity + "'," +
				"'" + this.products_shippingtime + "'," +
				"'" + this.products_model + "'," +
				"'" + this.group_permission_0 + "'," +
				"'" + this.group_permission_1 + "'," +
				"'" + this.group_permission_2 + "'," +
				"'" + this.products_sort + "'," +
				"'" + this.products_image + "'," +
				"'" + this.products_price + "'," +
				"'" + this.products_discount_allowed + "'," +
				"'" + this.products_date_added + "'," +
				"'" + this.products_last_modified + "'," +
				"'" + this.products_date_available + "'," +
				"'" + this.products_weight + "'," +
				"'" + this.products_status + "'," +
				"'" + this.products_tax_class_id + "'," +
				"'" + this.product_template + "'," +
				"'" + this.options_template + "'," +
				"'" + this.manufacturers_id + "'," +
				"'" + this.products_ordered + "'," +
				"'" + this.products_fsk18 + "'," +
				"'" + this.products_vpe + "'," +
				"'" + this.products_vpe_status + "'," +
				"'" + this.products_vpe_value + "'," +
				"'" + this.products_startpage + "'," +
				"'" + this.products_startpage_sort + "'" +
				")";
		
		return productStmt;
	}
	
	
	public String getValuesStatement() {

		String productStmt =
				" ,(" +
				"'" + this.products_id + "'," +
				"'" + this.products_ean + "'," +
				"'" + this.products_quantity + "'," +
				"'" + this.products_shippingtime + "'," +
				"'" + this.products_model + "'," +
				"'" + this.group_permission_0 + "'," +
				"'" + this.group_permission_1 + "'," +
				"'" + this.group_permission_2 + "'," +
				"'" + this.products_sort + "'," +
				"'" + this.products_image + "'," +
				"'" + this.products_price + "'," +
				"'" + this.products_discount_allowed + "'," +
				"'" + this.products_date_added + "'," +
				"'" + this.products_last_modified + "'," +
				"'" + this.products_date_available + "'," +
				"'" + this.products_weight + "'," +
				"'" + this.products_status + "'," +
				"'" + this.products_tax_class_id + "'," +
				"'" + this.product_template + "'," +
				"'" + this.options_template + "'," +
				"'" + this.manufacturers_id + "'," +
				"'" + this.products_ordered + "'," +
				"'" + this.products_fsk18 + "'," +
				"'" + this.products_vpe + "'," +
				"'" + this.products_vpe_status + "'," +
				"'" + this.products_vpe_value + "'," +
				"'" + this.products_startpage + "'," +
				"'" + this.products_startpage_sort + "'" +
				")";
		
		return productStmt;
	}

	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `products`;";
		return stmt;
	}
	
	
	public ProductDescription getProductDescription() {		
		return this.productDescription;
	}
	
	
	public String getProductCategorieInsertStatement() {
		
		String productStmt =
				"INSERT IGNORE INTO `tmp_products_to_categories`" +
				"(`products_id`," +
				"`categories_id`)" +				
				"VALUES	(" +
				"'" + this.products_id + "'," +
				"'" + this.categorie + "'" +				
				")";
		
		return productStmt;		
	}


	public String getProductCategorieValuesStatement() {
		
		String productStmt =				
				" ,(" +
				"'" + this.products_id + "'," +
				"'" + this.categorie + "'" +				
				")";
		
		return productStmt;		
	}
	
}
