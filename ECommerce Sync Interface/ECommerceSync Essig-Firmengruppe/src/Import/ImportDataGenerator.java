package Import;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import Connection.MySQLConnectorMYSQL;
import ImportData.Order;
import ImportData.OrderProduct;
import MailService.SendJavaMail;

import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import fit.informixconnector.InformixConnector;


public class ImportDataGenerator {

	private static final Logger logger = Logger.getLogger(ImportDataGenerator.class.getName()); 
	Handler fh = null; 
	
	public void startProcess(final boolean simulation, final Date beginDate, final Date endDate) throws SQLException {
		
		try {
			Date now = new Date();
			SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH.mm.ss");
			String nowString = simpleDateTimeFormat.format(now); 
			fh = new FileHandler("d:/User/fit/bin/ImportOrdersLog/ImportOrders " + nowString + ".log");
			logger.addHandler(fh);
			fh.setFormatter(new SimpleFormatter());
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		final SendJavaMail sendJavaMail = new SendJavaMail();
		
		MySQLConnectorMYSQL mySQLConnector = new MySQLConnectorMYSQL();
		mySQLConnector.createConnection("127.0.0.1", "10101", "db222645_6", "db222645_6", "essig2009", true);
		
		logger.info("MySQL Connection created.");
		
		ArrayList<Order> orders = null;
		try {
			orders = this.getOrders(mySQLConnector, simulation, beginDate, endDate);
		} catch (SQLException e) {
			e.printStackTrace();
			mySQLConnector.closeConnection();
			logger.warning("Error while reading data from MySQL." + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			mySQLConnector.closeConnection();
			logger.warning("Error while reading data from MySQL." + e.getMessage());
		}
		
		mySQLConnector.closeConnection();
		
		if ( orders.isEmpty() ) {
			
			logger.warning("No orders for Import available.");
			sendJavaMail.sendEmail("Keine Auftraege zum Import vorhanden.", "Keine Auftraege vorhanden f�r den Zeitraum: " + beginDate + " - " + endDate, "info@essig-firmengruppe.de");
			
		} else {
			
			InformixConnector informixConnector = new InformixConnector();
			informixConnector.createConnection();
			
			try {
				this.insertToInformix(orders, informixConnector, simulation);
			} catch (Exception e) {
				e.printStackTrace();
				informixConnector.closeConnection();
				logger.warning("Error while executing INSERT to Informix database." + e.getMessage());
				sendJavaMail.sendEmail("Fehler beim INSERT in Informix DB", "Fehler beim INSERT in Informix DB. Bitte �berpr�fen Sie die Informix Datenbank Verbindung", "info@essig-firmengruppe.de");
			}			
			
			informixConnector.closeConnection();
			
			System.out.println("Auftraege erfolgreich importiert");
			String addText = "";
			if ( simulation ) {
				addText = "ohne LOESCHEN";
			} else {
				addText = "mit LOESCHEN";
			}
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
			SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH.mm.ss");
			String beginDateString = simpleDateFormat.format(beginDate);
			String endDateString = simpleDateFormat.format(endDate);
			Date now = new Date();
			String nowString = simpleDateTimeFormat.format(now);
			
			logger.info(orders.size() + " Orders are inserted to Informix Database");
			sendJavaMail.sendEmail("Auftraege erfolgreich um " + nowString + " | " + addText + " importiert. Es wurden " + orders.size() + " Auftraege importiert.", "Auftragsimport erfolgreich f�r den Zeitraum: " + beginDateString + " - " + endDateString, "info@essig-firmengruppe.de");
		}		
				
		System.exit(0);
	}
	
	
	private ArrayList<Order> getOrders(final MySQLConnectorMYSQL mySQLConnector, final boolean simulation,
			final Date beginDate, final Date endDate) throws SQLException, IOException {
		
		ArrayList<Order> orders = new ArrayList<Order>();
		
		final java.sql.Date beginDateSQL = new java.sql.Date(beginDate.getTime());
		final java.sql.Date endDateSQL = new java.sql.Date(endDate.getTime());
		
		final String stmt = "SELECT DISTINCT o.`orders_id`" +
							", o.`customers_cid`" +
							", o.`comments`" +
							", o.`date_purchased`" +
							", o.`customers_ip`" +
							" FROM " +
							"orders as o, orders_products as op" +
							" WHERE " +
							"o.orders_id = op.orders_id" +
							" and " +
							"op.liefdat " +
							"between " + "'" + beginDateSQL + " 00:00:00" + "'" + " and " + "'" + endDateSQL + " 00:00:00" + "'" +
							" and " +
							"op.orders_products_status = '1';";
		
		final StringBuilder updateStringBuilder = new StringBuilder();
		
		final ResultSet ordersResultSet = mySQLConnector.executeStatement(stmt);
		
		logger.info("Execute Orders statement: " + stmt);
		logger.info("Following orders are selected to export for " + beginDate + " to " + endDate);
		
		while(ordersResultSet.next()) {
			
			final int orders_id = ordersResultSet.getInt("orders_id");
			final String customers_cid = this.getString(ordersResultSet, "customers_cid");
			String comments = this.getString(ordersResultSet, "comments");
			final Date date_purchased = ordersResultSet.getDate("date_purchased");
			final String customers_ip = ordersResultSet.getString("customers_ip");
			
			Order order = new Order(orders_id, customers_cid, "", date_purchased, customers_ip);
			
			comments = comments.replaceAll("[\\r\\n]", "");
			if ( comments.length() > 255 ) {
				comments = comments.substring(0, 254);
			}
			
			order.setComments(comments);
			
			logger.info("Order ID " + orders_id);
			
			ArrayList<OrderProduct> orderProducts = this.selectOrderProducts(orders_id, mySQLConnector, simulation, updateStringBuilder, beginDateSQL, endDateSQL);
			order.setOrderProducts(orderProducts);
			orders.add(order);
		}		
		return orders;	
	}
	
	
	
	
	
	private ArrayList<OrderProduct> selectOrderProducts(final int orders_id, final MySQLConnectorMYSQL mySQLConnector, 
			final boolean simulation, StringBuilder updateStringBuilder, final java.sql.Date beginDateSQL, final java.sql.Date endDateSQL) throws SQLException, IOException {
		
		ArrayList<OrderProduct> orderProducts = new ArrayList<OrderProduct>();
		
		final String stmt = "SELECT `orders_products_id`," +
				"`orders_id`," +
				"`products_id`," +
				"`products_model`," +
				"`products_name`," +
				"`products_quantity`," +
				"`liefdat`," +
				"`comments` " +
				"FROM `orders_products`" +
				"WHERE `orders_id` = " + orders_id +				
				" and " +
				"`liefdat` " +
				"between " + "'" + beginDateSQL + " 00:00:00" + "'" + " and " + "'" + endDateSQL + " 00:00:00" + "'" +				
				" and " +
				"orders_products_status = '1'" +
				";";
		

		
		final ResultSet ordersResultSet = mySQLConnector.executeStatement(stmt);
		logger.info("Execute OrdersProducts statement: " + stmt);
		
		while(ordersResultSet.next()) {
			
			final int orders_products_id = ordersResultSet.getInt("orders_products_id");
			final int products_id = ordersResultSet.getInt("products_id");
			String products_model = null;
			products_model = this.getString(ordersResultSet, "products_model");
			final String products_name = ordersResultSet.getString("products_name");
			final int products_quantity = ordersResultSet.getInt("products_quantity");
			final Date liefdat = ordersResultSet.getDate("liefdat");
			String comments = ordersResultSet.getString("comments");
			
			logger.info("Orders Products ID " + orders_products_id);
			
			final OrderProduct orderProduct = new OrderProduct(orders_products_id, products_id, products_model, products_name, products_quantity, liefdat, "");
			String products_model_beilage = this.selectOrderProductAttributes(orders_id, orders_products_id, mySQLConnector);
			orderProduct.setProducts_model_beilage(products_model_beilage);
						
			comments = comments.replaceAll("[\\r\\n]", "");
			if ( comments.length() > 80 ) {
				comments = comments.substring(0, 79);
			}
			orderProduct.setComments(comments);
			orderProducts.add(orderProduct);
			
			if (!simulation ) {
				
				final String updateStmt = 
						"UPDATE `orders_products` SET " +
						"orders_products_status = '2'" +
						" WHERE " +
						"`orders_products_id` = '" + orders_products_id + "'; ";
				
				mySQLConnector.executeUpdate(updateStmt);
				
				final ResultSet count =mySQLConnector.executeStatement("SELECT COUNT(*) FROM orders_products WHERE orders_id = '" + orders_id + "' AND orders_products_status = '1'; ");
				int cnt = 0;
				
				while (count.next()) {
					cnt = count.getInt("count(*)");					
				}
				
				if ( cnt == 0 ) {
					
					final String orderUpdateStmt = 
							"UPDATE `orders` SET " +
							"orders_status = '2'" +
							" WHERE " +
							"`orders_id` = '" + orders_id + "'; ";
					
					mySQLConnector.executeUpdate(orderUpdateStmt);
				}
				
				
			}			
			
		}	
		return orderProducts;		
	}
	
	
	
	private String getString(ResultSet rs, String columnName) throws SQLException, IOException {
		Reader r = rs.getCharacterStream(columnName);
		if (r == null)
			return null;
		
		StringBuilder tmp = new StringBuilder();
		char[] buf = new char[512];
		int lu;
		do {
			lu = r.read(buf);
			if (lu > 0)
				tmp.append(buf, 0, lu);
		} while (lu >= 0);
		return tmp.toString();
	}
	
	
	
	private String selectOrderProductAttributes(final int orders_id, final int orders_products_id, final MySQLConnectorMYSQL mySQLConnector) {
		
		String products_options = "";
		String products_options_values = "";
		String attributes_model = "";
		
		String stmt = 	"SELECT `products_options`, " +
						"`products_options_values`" +
						" FROM `orders_products_attributes`" +
						" WHERE `orders_id` = " + orders_id +
						" AND `orders_products_id` = " + orders_products_id + ";";
		
		final ResultSet ordersResultSet = mySQLConnector.executeStatement(stmt);
		
		try {
			while(ordersResultSet.next()) {			
				products_options = ordersResultSet.getString("products_options");
				products_options_values = ordersResultSet.getString("products_options_values");
				
				stmt = 	"SELECT pa.`attributes_model`" +
						" FROM `products_options` AS po, `products_options_values` AS pov, `products_attributes` AS pa" +
						" WHERE po.`products_options_name` = '" + products_options +
						"' AND pov.`products_options_values_name` = '" + products_options_values + 
						"' AND pa.`options_id` = po.`products_options_id`" +
						" AND pa.`options_values_id` = `products_options_values_id`;";
				
				final ResultSet beilage = mySQLConnector.executeStatement(stmt);
				
				while(beilage.next()) {			
					attributes_model = beilage.getString("attributes_model");
				}
			}			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return attributes_model;		
	}
	
	
	
	private void insertToInformix(final ArrayList<Order> orders, final InformixConnector informixConnector, final boolean simulation) throws Exception {
		
		final Iterator<Order> it_orders = orders.iterator();
		
		short gueltig = 0;
		
		if (simulation) {
			gueltig = 0;
		} else {
			gueltig = 1;
		}
		
		//TODO guelitg Flag muss in shopaufp verschoben werden, aufgrund der M�glichkeit nur einzelne Positionen abzurufen 
		//TODO + orders_products_id als zus�tzliches Feld in shopaufp, wird ben�tigt f�r korrektes L�schen + liefdat f�r den Abruf!
		
		while (it_orders.hasNext()) {
			
			Order order = (Order) it_orders.next();
			
			Date sqlDate = new java.sql.Date(order.getDate_purchased().getTime());
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
			String datePurchasedString = simpleDateFormat.format(sqlDate);
			
			String shopaufkDeleteStmt = "delete from shopaufk where orders_id = " + order.getOrders_id() + ";";
			
			String shopaufkInsertstmt = 
					"INSERT INTO shopaufk" +
					"(orders_id," +
					"kun," +
					"comments," +
					"lieferdat," +
					"auf," +
					"gueltig," +
					"stat" +
					")	VALUES (" +
					order.getOrders_id() + "," +
					order.getCustomers_cid() + ",'" +
					order.getComments() + "','" +
					datePurchasedString + "'," +
					0 + "," +
					gueltig + "," +
 					0 +
					"); ";
			
			informixConnector.executeStatement(shopaufkDeleteStmt);			
			informixConnector.executeStatement(shopaufkInsertstmt);
			
			Iterator<OrderProduct> it_orderProducts = order.getOrderProducts().iterator();			
			
			while (it_orderProducts.hasNext()) {
				OrderProduct orderProduct = (OrderProduct) it_orderProducts.next();
				final int orders_products_id = orderProduct.getOrders_products_id();
				
				Date sqlLiefDate = new java.sql.Date(orderProduct.getLiefdat().getTime());
				String liefDateString = simpleDateFormat.format(sqlLiefDate);
				
				String shopaufpDeleteStmt = "delete from shopaufp where orders_id = " + order.getOrders_id() + " and orders_product_id = " + orders_products_id + ";";
				
				String shopaufpInsertStmt = 
						"INSERT INTO shopaufp" +
						"(orders_id," +
						"a," +
						"stk," +
						"a_beilage," +
						"auf," +
						"gueltig," +
						"stat," +
						"lieferdat," +
						"orders_product_id," +
						"comments" +
						")	VALUES (" +
						order.getOrders_id() + "," +
						orderProduct.getProducts_model() + "," +
						orderProduct.getProducts_quantity() + "," +
						orderProduct.getProducts_model_beilage() + "," +
						0 + "," +
						gueltig + "," +
						0 + ",'" +
						liefDateString + "'," +
						orders_products_id + ",'" +
						orderProduct.getComments() +
						"'); ";
				
				informixConnector.executeStatement(shopaufpDeleteStmt);
				informixConnector.executeStatement(shopaufpInsertStmt);
				
			}			
		}		
		
	} 
	
}
