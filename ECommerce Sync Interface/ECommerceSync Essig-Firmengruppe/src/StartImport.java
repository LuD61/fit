import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import Import.ImportDataGenerator;
import UI.UINotDelete;
import UI.UIWithDelete;

import javax.swing.JFrame;
import javax.swing.JLabel;


/**
 * 
 */

/**
 * @author danielh
 *
 */
public class StartImport extends JFrame{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		final boolean simulation = Boolean.valueOf(args[0]);
		final boolean withDialog = Boolean.valueOf(args[1]);
		
		if ( withDialog ) {
			
			JFrame frame = new JFrame();
			
			if ( !simulation ) {
				frame.setContentPane(new UIWithDelete());
				frame.setTitle("Auftragsexport mit Loeschen ...");
			} else {
				frame.setContentPane(new UINotDelete());
				frame.setTitle("Auftragsexport ohne loeschen ...");
			}
			
			
			
			frame.addWindowListener(new WindowAdapter() {

				// Shows code to Add Window Listener
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});		
			
			frame.setDefaultLookAndFeelDecorated(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			frame.setSize(200, 200);
			frame.pack();
			frame.setVisible(true);			
			
		} else {
			
			final ImportDataGenerator importDataGenerator = new ImportDataGenerator();
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.DAY_OF_MONTH, -1);
			Date date = cal.getTime();
			
			try {
				importDataGenerator.startProcess(simulation, date, date);
			} catch (SQLException e) {
				e.printStackTrace();
			}			
			
		}
				
		
	}	
}
	
	

	
