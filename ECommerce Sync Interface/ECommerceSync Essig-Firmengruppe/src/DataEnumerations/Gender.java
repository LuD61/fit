package DataEnumerations;

public enum Gender {
	
	m(0),
	w(1);

		 
	private int code;

	private Gender(int c) {
		code = c;
	}
 
	public int getCode() {
		return code;
	}
}
