package DataEnumerations;

public enum ShippingState {
	
	DAY(1),
	WEEK(2);

		 
	private int code;

	private ShippingState(int c) {
		code = c;
	}
 
	public int getCode() {
		return code;
	}

}
