package Connection;

import java.sql.Connection;
import java.sql.ResultSet;

public interface Connector {
	
	public void createConnection();
	
	public void closeConnection();
	
	public ResultSet executeStatement(final String statement);
	
	public Connection getConnection();

}
