package Connection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;
	


public class InformixConnector implements Connector {
	
	/**
	 * @param args
	 */
	
	private Connection cn;
	
		
	public void createConnection() {
	
		final String sDsn = "bws";
	    final String dbUsr = "informix";
	    final String dbPwd = "t0pfit";
	    //final String dbPwd = "t0p.setec";
	 
	     try { 
		    System.out.println("* Treiber laden");		    
	    	Class.forName("sun.jdbc.odbc.JdbcOdbcDriver").newInstance();
	    } 
	    catch (Exception e) { 
	        System.err.println("Unable to load driver."); 
	        e.printStackTrace();
	        System.out.println("Keine Verbindung zur Informix Datenbank m�glich, bitte Verbindung zur Datenbank pr�fen!");
	        System.exit(0);
	    }
	    
	    try { 
		    System.out.println("* Verbindung aufbauen");	    
		    this.cn = DriverManager.getConnection( "jdbc:odbc:" + sDsn, dbUsr, dbPwd );    
	    } catch (SQLException sqle) { 
	        System.out.println("SQLException: " + sqle.getMessage()); 
	        System.out.println("SQLState: " + sqle.getSQLState()); 
	        System.out.println("VendorError: " + sqle.getErrorCode()); 
	        sqle.printStackTrace();
	        System.out.println("Keine Verbindung zur Informix Datenbank m�glich, bitte Verbindung zur Datenbank pr�fen!");
	        System.exit(0);
	    }   
	}


	@Override
	public void closeConnection() {
		
		try {
			this.cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
	}


	@Override
	public ResultSet executeStatement(final String statement) {
		
		Statement stmt = null;
		ResultSet resultSet = null;
		
		try {
			stmt = this.cn.createStatement();
			System.out.println(statement);
			resultSet = stmt.executeQuery(statement);
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return resultSet;
	}


	@Override
	public Connection getConnection() {
		return this.cn;
	}
	
}
