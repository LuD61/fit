package Connection;
import java.io.IOException;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.LocalPortForwarder;





public class SSHConnection
{
	
	private Connection conn;
	
	
	
	public void startSSHConnection()
	{
		String hostname = "essig-firmengruppe.de";
		String username = "ssh-396969-essig";
		String password = "essig2009";

		try
		{
			/* Create a connection instance */

			conn = new Connection(hostname);

			/* Now connect */

			conn.connect();

			/* Authenticate.
			 * If you get an IOException saying something like
			 * "Authentication method password not supported by the server at this stage."
			 * then please check the FAQ.
			 */

			boolean isAuthenticated = conn.authenticateWithPassword(username, password);

			if (isAuthenticated == false)
				throw new IOException("Authentication failed.");

			/* Create a session */
			
			LocalPortForwarder lpf1 = conn.createLocalPortForwarder(10101, "mysql5.essig-firmengruppe.de", 3306);
			conn.requestRemotePortForwarding("127.0.0.1", 10101, "mysql5.essig-firmengruppe.de", 3306);
		}
		catch (IOException e)
		{
			e.printStackTrace(System.err);
			System.exit(2);
		}
	}
	
	
	public void close() {

		/* Close the connection */

		conn.close();
		
	}
}
