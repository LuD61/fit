package ExportData;

import DataInterface.Statement;

public class Bestellvorschlag implements Statement {

	private double quantity;
	private int kun;
	private int products_id;
	
	
	public Bestellvorschlag(double quantity, int kun, int products_id) {
		this.quantity = quantity;
		this.kun = kun;
		this.products_id = products_id;
	}


	@Override
	public String getSelectStatement() {
		return null;
	}

	@Override
	public String getInsertStatement() {

		String stmt = "INSERT INTO `tmp_xt_plg_hi_suggestion`"
				+ "(" +
				"`suggestion_id`," +
				"`products_id`," + 
				"`products_quantity`," + 
				"`customers_id`" +
				") VALUES (" + 0 + 
				"'" + 
				this.products_id + 
				"'," + 
				"'"	+ 
				this.quantity + 
				"'," + 
				"'" + 
				this.kun + 
				"'" +  
				");";

		return stmt;
	}

	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `xt_plg_hi_suggestion`;";
		return stmt;
	}

}
