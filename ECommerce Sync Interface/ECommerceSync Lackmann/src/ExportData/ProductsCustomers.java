package ExportData;

import DataInterface.Statement;

public class ProductsCustomers implements Statement {
	
	
	private int products_id;;
	private int customers_id;
	
	
	public ProductsCustomers(final int products_id, final int customers_id) {
		this.products_id = products_id;
		this.customers_id = customers_id;
	}	
	

	@Override
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String stmt = 
				"insert into `tmp_products_to_customers`" +
				"(`products_id`," +
				"`customers_id`" +
				") values (" +
				"'" + this.products_id + "'," +
				"'" + this.customers_id + "'" +
				"); ";
		
		return stmt;
	}

	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}	

}
