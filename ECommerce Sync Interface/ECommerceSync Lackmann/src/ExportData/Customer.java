package ExportData;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import Converter.MD5Converter;
import DataInterface.Statement;

public class Customer implements Statement {

	private String customers_cid;
	private String customers_vat_id = "";
	private int customers_vat_id_status = 0;
	private int customers_status;
	private String customers_email_address = "";
	private String customers_password; // MD5 Hash
	private int account_type = 0;
	private String password_request_key = "";
	private String payment_unallowed = "";
	private String shipping_unallowed = "";
	private String companyName;

	private ArrayList<CustomerAddress> customerAddresses;
	private String language;
	private int customers_view_orders;
	private String customers_liefertag;
	private java.sql.Date date_added;
	private java.sql.Date last_modified;

	public Customer(final int customerId, final int customers_status,
			final String company, final String language_code, final String customers_liefertag, final String view_orders) {

		this.customers_cid = String.valueOf(customerId);
		this.customers_status = customers_status;
		this.customerAddresses = new ArrayList<CustomerAddress>();
		
		this.companyName = company;
		
		this.language = language_code;
		this.customers_liefertag = customers_liefertag;
		
		if (view_orders == "J") {
			this.customers_view_orders = 1;	
		} else {
			this.customers_view_orders = 0;
		}
		this.date_added = new java.sql.Date(new Date().getTime());
		this.last_modified = new java.sql.Date(new Date().getTime());
	}

	public void setPassword(final String password) {
		if ( !password.isEmpty() ) {
			this.customers_password = MD5Converter.md5(password);
		}		
	}
	
	public void addCustomerAddress(final CustomerAddress customerAddress) {
		this.customerAddresses.add(customerAddress);
	}

	public void setUStId(final String ust_id) {
		this.customers_vat_id = ust_id;
	}

	public void setEmailAdress(final String email) {
		this.customers_email_address = email;
	}

	@Override
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String customerStmt = 
				"INSERT INTO `tmp_customers` (`customers_id`,  `external_id`, `customers_cid`, `customers_vat_id`, `customers_vat_id_status`,`customers_status`, " +
				"`customers_email_address`, `customers_password`, `account_type`, `password_request_key`, `payment_unallowed`, `shipping_unallowed`, " +
				"`date_added`, `last_modified`, `shop_id`, `customers_default_currency`, `customers_default_language`, `campaign_id`, `customers_liefertag`, " +
				"`customers_view_orders`, `vt_watchlist_settings`) " +
				"VALUES ('" + Integer.valueOf(this.customers_cid).intValue() + 
						"','shop" +
						"','" + this.customers_cid +
						"','" + this.customers_vat_id +
						"','" + this.customers_vat_id_status + 
						"','" + this.customers_status +
						"','" + this.customers_email_address +
						"','" + this.customers_password +
						"','" + this.account_type +
						"','" + this.password_request_key +
						"','" + this.payment_unallowed +
						"','" + this.shipping_unallowed +
						"','" + this.date_added + "'" +
						",'" + this.last_modified + "'" +
						",'" + 1 +
						"','EUR'" +  
						",'" + this.language +
						"','" + 0 +
						"','" + this.customers_liefertag + 
						"','" + this.customers_view_orders +
						"','')";			
				
		return customerStmt;
	}
	
	
	public String getValuesStatement() {
		
		String customerStmt =",('" + Integer.valueOf(this.customers_cid).intValue() + 
						"','shop" +
						"','" + this.customers_cid +
						"','" + this.customers_vat_id +
						"','" + this.customers_vat_id_status + 
						"','" + this.customers_status +
						"','" + this.customers_email_address +
						"','" + this.customers_password +
						"','" + this.account_type +
						"','" + this.password_request_key +
						"','" + this.payment_unallowed +
						"','" + this.shipping_unallowed +
						"','" + this.date_added + "'" +
						",'" + this.last_modified + "'" +
						",'" + 1 +
						"','EUR'" +  
						",'" + this.language +
						"','" + 0 +
						"','" + this.customers_liefertag + 
						"','" + this.customers_view_orders +
						"','')";			
				
		return customerStmt;
	}

	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `customers`;";
		return stmt;
	}
	
	
	public String getCustomerCID() {
		return this.customers_cid;
	}
	
	
	public int getCustomerStatus() {
		return this.customers_status;
	}
	
	public String getCompanyName() {
		return this.companyName;
	}
	
	public Iterator<CustomerAddress> getCustomerAddresses() {
		return this.customerAddresses.iterator();
	}

}
