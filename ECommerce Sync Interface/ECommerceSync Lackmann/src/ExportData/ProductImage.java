package ExportData;

import java.util.Date;

import DataInterface.Statement;

public class ProductImage implements Statement {
	

	private String type;
	private String file;
	private java.sql.Date date_added;
	private java.sql.Date last_modified;


	public ProductImage(final String file, final String type) {
		this.type = type;
		this.file = file;
		this.date_added = new java.sql.Date(new Date().getTime());
		this.last_modified = new java.sql.Date(new Date().getTime());
	}
	

	@Override
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String stmt = "INSERT INTO `xt_media` (" +
				"`id`," +
				"`file`," +
				"`type`," +
				"`class`," +
				"`download_status`," +
				"`status`," +
				"`owner`," +
				"`date_added`," +
				"`last_modified`," +
				"`max_dl_count`," +
				"`max_dl_days`," +
				"`total_downloads`) " +
				"VALUES (" + 0 + "," +
						"'" + this.file + "'," +
						"'images'," +
						"'" + this.type + "'," +
						"'free'," +
						"'true'," +
						"'1'," +
						"'" + this.date_added + "'," +
						"'" + this.last_modified + "'," +
						"'0'," +
						"'0'," +
						"'0')"; 
		
		return stmt;
	}
	
	
	public String getValuesStatement() {
		
		String stmt = ",(" + 0 + "," +
						"'" + this.file + "'," +
						"'images'," +
						"'" + this.type + "'," +
						"'free'," +
						"'true'," +
						"'1'," +
						"'" + this.date_added + "'," +
						"'" + this.last_modified + "'," +
						"'0'," +
						"'0'," +
						"'0')"; 
		
		return stmt;
	}

	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `products_image`;";
		return stmt;
	}
	
	

}
