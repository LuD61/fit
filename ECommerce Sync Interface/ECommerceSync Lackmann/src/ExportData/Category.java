package ExportData;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class Category {

	private int categories_id;
	private String categories_image;
	private int parent_id;
	private int categories_status = 1;
	private String categories_template = "";
	private String listing_template = "";
	private int sort_order;
	private String products_sorting = "model-available";
	private String product_sorting2 = "ASC";	
	
	private ArrayList<CategoryDescription> categorieDescriptions;
	private java.sql.Date date_added;
	private java.sql.Date last_modified;	
	private ArrayList<Category2> underCategories = null;
	
	
	public Category(final int id, final int parentId,
			final String categories_name,
			final String categories_heading_title, final short sort_order, final String image, final String defaultLanguageCode) {
		this.categorieDescriptions = new ArrayList<CategoryDescription>();
		this.categories_id = id;
		this.parent_id = parentId;
		this.sort_order = sort_order;
		this.categories_image = image;
		CategoryDescription categorieDescription = new CategoryDescription(id,
				categories_name, categories_heading_title, defaultLanguageCode);
		this.categorieDescriptions.add(categorieDescription);
		this.date_added = new java.sql.Date(new Date().getTime());
		this.last_modified = new java.sql.Date(new Date().getTime());
		this.underCategories = new ArrayList<Category2>();
		
	}

	
	public void addCategoryToCategory(final Category2 category2) {
		
		this.underCategories.add(category2);		
	}
	
	
	public Iterator<Category2> getWGCategories() {
		return this.underCategories.iterator();
	}

	
	public String getInsertStatement() {

		String stmt = "INSERT INTO `tmp_categories`	(`categories_id`, `external_id`, `permission_id`, `categories_owner`,"
				+ "`categories_image`," + "`parent_id`,"
				+ "`categories_status`," + "`categories_template`,"
				+ "`listing_template`,"
				+ "`sort_order`," + "`products_sorting`,"
				+ "`products_sorting2`," + "`date_added`,"
				+ "`last_modified`) " +

				"VALUES (" +
				"'" + 0
				+ "','shop','0"
				+ "','1"
				+ "','"
				+ this.categories_image
				+ "','"
				+ 0
				+ "','"
				+ this.categories_status
				+ "','"
				+ this.categories_template
				+ "','"
				+ this.listing_template
				+ "','"
				+ this.sort_order
				+ "','"
				+ this.products_sorting
				+ "','"
				+ this.product_sorting2
				+ "','" + this.date_added + "'"
				+ ",'" + this.last_modified + "'" 
				+ ");";	

		return stmt;
	}

	
	public String getDeleteStatement() {
		final String stmt = "Delete from `categories`;";
		return stmt;
	}

	public void setCategories_image(String categories_image) {
		this.categories_image = categories_image;
	}

	public Iterator<CategoryDescription> getCategorieDescriptions() {
		return this.categorieDescriptions.iterator();
	}
	
	public void addCategoryDescription(final CategoryDescription categorieDescription) {
		this.categorieDescriptions.add(categorieDescription);
	}
	
	public int getCategorieId() {
		return this.categories_id;
	}

}
