package ExportData;

import java.util.ArrayList;
import java.util.Iterator;

public class ProductPriceGroups {


	private ArrayList<ProductPriceGroup> productPriceGroups;


	public ProductPriceGroups() {
		
		this.productPriceGroups = new ArrayList<ProductPriceGroup>();
	}
	
	
	public void addProductPriceGroup(final ProductPriceGroup productPriceGroup) {
		
		this.productPriceGroups.add(productPriceGroup);		
	}
	
	
	public void addProductPricesToPriceGroup(final int pr_gr_stuf, final ProductPrice product_price) {
		
		for (int i = 0; i < this.productPriceGroups.size(); i++) {
			if ( this.productPriceGroups.get(i).getPr_gr_stuf() == pr_gr_stuf ) {
				this.productPriceGroups.get(i).addProductPrice(product_price);
			}
		}		
	}	
	
	public ArrayList<Integer> getPriceGroupArray(final int a) {
		
		ArrayList<Integer> priceGroups = new ArrayList<Integer>();
		
		Iterator<ProductPriceGroup> it = this.productPriceGroups.iterator();
		
		while (it.hasNext()) {
			ProductPriceGroup productPriceGroup = (ProductPriceGroup) it.next();			
			if ( productPriceGroup.isAInPriceGroup(a) ) {
				priceGroups.add(Integer.valueOf(productPriceGroup.getPr_gr_stuf()));
			}				
		}		
		return priceGroups;	
	}
	
	
	public ArrayList<ProductPriceGroup> getProductPriceGroups() {
		
		return this.productPriceGroups;
	}
	
}
