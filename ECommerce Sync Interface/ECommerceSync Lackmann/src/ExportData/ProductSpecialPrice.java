package ExportData;


import java.util.ArrayList;
import java.util.Date;


public class ProductSpecialPrice {
	
	private int products_id;	
	private ArrayList<Integer> pr_gr_stuf = null;
	private double aki_pr_eu;
	private Date aki_von;
	private Date aki_bis;
	private int[] priceGroups;
	
	
	public ProductSpecialPrice(int products_id, int pr_gr_stuf,
			double aki_pr_eu, Date aki_von, Date aki_bis) {
		
		this.products_id = products_id;
		this.pr_gr_stuf = new ArrayList<Integer>();
		this.addPriceGroup(pr_gr_stuf);
		this.priceGroups = new int[25];		
		this.aki_pr_eu = aki_pr_eu;
		this.aki_von = new java.sql.Date(aki_von.getTime());
		this.aki_bis = new java.sql.Date(aki_bis.getTime());
	}
	
		
	public String getSelectStatement() {
		return null;
	}
	
	
	private void getPriceGroupArray() {
		
		this.priceGroups[0] = 0;
		
		for (int i = 1; i < this.priceGroups.length; i++) {
			if ( this.pr_gr_stuf.contains(i) ) {
				this.priceGroups[i] = 1;
			} else {
				this.priceGroups[i] = 0;
			}
		}
	}

	
	public String getInsertStatement() {
		
		this.getPriceGroupArray();

		String productStmt =
				"INSERT INTO `tmp_xt_products_price_special`" +
				"(`id`,	`products_id`,`specials_price`,	`date_available`,`date_expired`,`status`," +
				"`group_permission_all`," +
				"`group_permission_1`," +
				"`group_permission_2`," +
				"`group_permission_3`," +
				"`group_permission_4`," +
				"`group_permission_5`," +
				"`group_permission_6`," +
				"`group_permission_7`," +
				"`group_permission_8`," +
				"`group_permission_9`," +
				"`group_permission_10`," +
				"`group_permission_11`," +
				"`group_permission_12`," +
				"`group_permission_13`," +
				"`group_permission_14`," +
				"`group_permission_15`," +
				"`group_permission_16`," +
				"`group_permission_17`," +
				"`group_permission_18`," +
				"`group_permission_98`," +
				"`group_permission_99`" +
				") VALUES (0," +
				"'" + this.products_id + "'," +
				"'" + this.aki_pr_eu + "'," +
				"'" + this.aki_von + "'," +
				"'" + this.aki_bis + "'," +
				"'1'," +
				"'" + this.priceGroups[0] + "'," +
				"'" + this.priceGroups[1] + "'," +
				"'" + this.priceGroups[2] + "'," +
				"'" + this.priceGroups[3] + "'," +
				"'" + this.priceGroups[4] + "'," +
				"'" + this.priceGroups[5] + "'," +
				"'" + this.priceGroups[6] + "'," +
				"'" + this.priceGroups[7] + "'," +
				"'" + this.priceGroups[8] + "'," +
				"'" + this.priceGroups[9] + "'," +
				"'" + this.priceGroups[10] + "'," +
				"'" + this.priceGroups[11] + "'," +
				"'" + this.priceGroups[12] + "'," +
				"'" + this.priceGroups[13] + "'," +
				"'" + this.priceGroups[14] + "'," +
				"'" + this.priceGroups[15] + "'," +
				"'" + this.priceGroups[16] + "'," +
				"'" + this.priceGroups[17] + "'," +
				"'" + this.priceGroups[18] + "'," +
				"'" + 0 + "'," +
				"'" + 0 + "')";			
		
		return productStmt;
	}

	
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `xt_products_price_special`;";
		return stmt;
	}
	
	
	public void addPriceGroup(final int group) {
		this.pr_gr_stuf.add(group);
	}

}
