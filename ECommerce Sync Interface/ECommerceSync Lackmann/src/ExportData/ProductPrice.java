package ExportData;

public class ProductPrice {


	private int a;
	private int pr_gr_stuf;
	private double vk_pr_eu;
	private double ld_pr_eu;


	public ProductPrice(final int a, final int pr_gr_stuf, final double vk_pr_eu, final double ld_pr_eu) {
		
		this.setA(a);
		this.setPr_gr_stuf(pr_gr_stuf);
		this.setVk_pr_eu(vk_pr_eu);
		this.setLd_pr_eu(ld_pr_eu);
	}

	
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getInsertStatement(final int products_id) {
		
		String stufe = "";
		double price = 0; 
		
		if ( this.pr_gr_stuf == 0 ) {
			stufe = "all";
			price = this.ld_pr_eu;
		} else {
			stufe = String.valueOf(this.pr_gr_stuf);
			price = this.vk_pr_eu;
		}
		
		 
		
		String stmt =
				"INSERT INTO tmp_products_price_group_" + stufe +
				"(id, products_id, discount_quantity, price) " +
				"VALUES(" +
				"0, " + products_id +
				", 1," + price +
				")";
			
		return stmt;
	}
	
	
	public String getValuesStatement(final int products_id) {
		
		String stufe = "";
		double price = 0; 
		
		if ( this.pr_gr_stuf == 0 ) {
			stufe = "all";
			price = this.ld_pr_eu;
		} else {
			stufe = String.valueOf(this.pr_gr_stuf);
			price = this.vk_pr_eu;
		}
		
		 
		
		String stmt = ",(" +
				"0, " + products_id +
				", 1," + price +
				")";
			
		return stmt;
	}

	
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}	
	
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `products_description`;";
		return stmt;
	}


	public int getA() {
		return a;
	}


	public void setA(int a) {
		this.a = a;
	}


	public int getPr_gr_stuf() {
		return pr_gr_stuf;
	}


	public void setPr_gr_stuf(int pr_gr_stuf) {
		this.pr_gr_stuf = pr_gr_stuf;
	}


	public double getVk_pr_eu() {
		return vk_pr_eu;
	}


	public void setVk_pr_eu(double vk_pr_eu) {
		this.vk_pr_eu = vk_pr_eu;
	}


	public double getLd_pr_eu() {
		return ld_pr_eu;
	}


	public void setLd_pr_eu(double ld_pr_eu) {
		this.ld_pr_eu = ld_pr_eu;
	}

}
