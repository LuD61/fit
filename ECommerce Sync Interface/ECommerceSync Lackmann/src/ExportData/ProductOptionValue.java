package ExportData;

import DataInterface.Statement;

public class ProductOptionValue implements Statement {
	
	
	private int products_options_values_id;
	private int language_id = 2;
	private String products_options_values_name;
	
	
	public ProductOptionValue(final int products_options_values_id, final String products_options_values_name) {
		this.products_options_values_id = products_options_values_id;
		this.products_options_values_name = products_options_values_name;
	}	
	

	@Override
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String stmt = 
				"insert into `tmp_products_options_values`" +
				"(`products_options_values_id`," +
				"`language_id`," +
				"`products_options_values_name`" +
				") values (" +
				"'" + this.products_options_values_id + "'," +
				"'" + this.language_id + "'," +
				"'" + this.products_options_values_name + "'" +
				"); ";
		
		return stmt;
	}

	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `products_options_values`;";
		return stmt;
	}
	
	

}
