package ExportData;

import java.util.ArrayList;
import java.util.Iterator;

import DataInterface.Statement;

public class ProductOption implements Statement {

	private int products_options_id;
	private int language_id = 2;
	private String products_options_name;
	private int products_id;

	public ArrayList<ProductOptionValue> productOptionValues;
	public ArrayList<ProductOptionValuesToProductOptions> productOptionValuesToProductOptions;
	public ArrayList<ProductAttributes> productAttributes;

	private int sortOrder = 1;

	public ProductOption(final int products_options_id,
			final String products_options_name, final int products_id) {
		this.products_options_id = products_options_id;
		this.products_options_name = products_options_name;
		this.products_id = products_id;
		this.productOptionValues = new ArrayList<ProductOptionValue>();
		this.productOptionValuesToProductOptions = new ArrayList<ProductOptionValuesToProductOptions>();
		this.productAttributes = new ArrayList<ProductAttributes>();
	}

	public void addProductionOptionValues(final int productOptionValueId,
			final String productOptionValueName) {
		ProductOptionValue productOptionValue = new ProductOptionValue(
				productOptionValueId, productOptionValueName);
		this.productOptionValues.add(productOptionValue);		
	}
	
	
	public void addProductOptionValuesToProductOptions(final int id, final int productOptionValueId) {
		
		ProductOptionValuesToProductOptions productOptionValuesToProductOptions = new ProductOptionValuesToProductOptions(id,
				this.products_options_id, productOptionValueId);		
		this.productOptionValuesToProductOptions.add(productOptionValuesToProductOptions);
	}
	
	
	public void addProductAttributes(final int id, final int productOptionValueId) {
		final ProductAttributes productAttributes = new ProductAttributes(id,
				this.products_id, this.products_options_id, productOptionValueId,
				this.sortOrder);
		this.productAttributes.add(productAttributes);
		this.sortOrder++;
	}

	@Override
	public String getSelectStatement() {
		// TODO Zusätzliche Implemntierung für Schreiben der
		// products_options_values_to_products_options.
		return null;
	}

	@Override
	public String getInsertStatement() {

		String stmt = "INSERT INTO `tmp_products_options`"
				+ "(`products_options_id`," + "`language_id`,"
				+ "`products_options_name`," + "`products_options_sortorder`"
				+ ") VALUES (" + "'" + this.products_options_id + "'," + "'"
				+ this.language_id + "'," + "'" + this.products_options_name
				+ "'," + "'0'" + ");";

		return stmt;
	}

	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `products_options`;";
		return stmt;
	}

}
