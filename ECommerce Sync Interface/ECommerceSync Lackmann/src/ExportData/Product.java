package ExportData;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import DataEnumerations.TaxClass;


public class Product {

	
	private int products_id;	
	private String products_ean = "";	
	private double products_quantity;	
	private int products_shippingtime; // shippingState Enumeration
	private String products_model = "";
	private int products_sort;
	private String products_image;
	private double products_price;
	private Date date_added;
	private Date last_modified;
	private Date date_available;
	private double products_weight;
	private int products_status;
	private int products_tax_class_id; // tax_class Enumeration
	private String product_template = "";
	private int manufacturers_id;
	private int products_ordered;
	private int products_fsk18;
	private int products_vpe; // products_vpe Enumeration
	private int products_vpe_status;
	private double products_vpe_value;
	private int products_startpage;
	private int products_startpage_sort;
	private ArrayList<ProductDescription> productDescriptions;
	private String external_id;
	private int permission_id;
	private int products_owner;
	private int products_average_quantity;
	private String products_master_model;
	private int products_master_flag;
	private String products_option_template;
	private String products_option_list_template;
	private int[] priceGroups;
	private String product_list_template;
	private int products_transactions;
	private double products_average_rating;
	private int products_rating_count;
	private int products_digital;
	private int flag_has_specials;
	private int products_serials;
	private int total_downloads;
	private String products_unit_per_package;
	private String products_packing_units;
	private String products_packing_units_except;
	private int max_products_units;
	private int max_products_per_customer_status;
	private double products_recommended_price;
	private String products_packung_pro_karton;
	private int products_tvads;
	private int products_tvads_sort;
	private int products_newprod;
	private int products_newprod_sort;
	private int products_aktion;
	private int products_aktion_sort;
	private int categorie;
	private int internalProductsId;
	private short shop_wg1 = 0;
	private short shop_wg2 = 0;
	private short shop_wg3 = 0;
	private short shop_wg4 = 0;
	private short shop_wg5 = 0;
	private double pfandVK = 0;

	public Product(final int productId, final String products_image,
			final double products_price, final double products_weight,
			final TaxClass tax, final boolean showOnStartPage,
			final int startPagePosition, final ArrayList<ProductDescription> productDescriptions, final int categorie, String products_ean, 
			double products_quantity, int products_vpe,	double products_vpe_value, String inhalt_pro_karton, String products_packing_units, String packung_pro_karton,
			final int tv, final int aktion, final int neuesProdukt, final ArrayList<Integer> prices, final int meldBestand, final Date date_added, final Date last_modified, int has_special, double pfandVK) {

		this.internalProductsId = productId;
		this.priceGroups = new int[25];
		this.products_id = productId;
		this.external_id = String.valueOf(productId);
		this.permission_id = 1;
		this.products_owner = 1;
		this.products_ean = products_ean;
		this.products_quantity = products_quantity;
		this.products_average_quantity = meldBestand;
		this.products_shippingtime = 1;
		this.products_model = String.valueOf(productId);
		this.products_master_model = "";
		this.products_master_flag = 0;
		this.products_option_template = "";
		this.products_option_list_template = "";
		this.products_sort = 0;		
		this.products_image = products_image;
		this.products_price = products_price;
		this.date_added = new java.sql.Date(date_added.getTime());
		this.last_modified = new java.sql.Date(last_modified.getTime());
		this.setDate_available(new java.sql.Date(new Date().getTime()));
		this.products_weight = products_weight;
		this.products_status = 1;
		this.products_tax_class_id = tax.getCode();
		this.product_template = "";
		this.product_list_template = "";
		this.manufacturers_id = 0;
		this.products_ordered = 0; //TODO
		this.products_transactions = 0; //TODO
		this.products_fsk18 = 0;
		this.products_vpe = products_vpe;
		this.products_vpe_status = 0;
		this.products_vpe_value = products_vpe_value;
		this.products_startpage_sort = startPagePosition;
		this.products_average_rating = 0;
		this.products_rating_count = 0;
		this.products_digital = 0;
		this.flag_has_specials = has_special;
		this.products_serials = 0;
		this.total_downloads = 0;
		this.products_unit_per_package = inhalt_pro_karton;
		this.products_packing_units = products_packing_units;
		this.products_packing_units_except = "";
		this.max_products_units = 0;
		this.max_products_per_customer_status = 0;
		this.products_recommended_price = this.products_price;
		this.products_packung_pro_karton = packung_pro_karton;
		this.products_tvads = tv;
		this.products_tvads_sort = 0;
		this.products_newprod = neuesProdukt;
		this.products_newprod_sort = 0;
		this.products_aktion = aktion;
		this.products_aktion_sort = 0;
		this.pfandVK = pfandVK;
		
		if (showOnStartPage) {
			this.products_startpage = 1;
		} else {
			this.products_startpage = 0;
		}

		this.productDescriptions = productDescriptions;
		
		this.categorie = categorie;
		
		
		for (int i = 0; i < this.priceGroups.length; i++) {
			if ( prices.contains(Integer.valueOf(i))) {
				this.priceGroups[i] = 1;	
			} else {
				this.priceGroups[i] = 0;
			}			 
		}
	}

	
	public int getCategorie() {
		return this.categorie;
	}

	
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public String getInsertStatement() {

		String productStmt =
				"INSERT INTO `tmp_products`" +
				"(`products_id`," +
				"`external_id`," +
				"`permission_id`," +
				"`products_owner`," +
				"`products_ean`," +
				"`products_quantity`," +
				"`products_average_quantity`," +
				"`products_shippingtime`," +
				"`products_model`," +
				"`products_master_model`," +
				"`products_master_flag`," +
				"`products_option_template`," +
				"`products_option_list_template`," +
				"`price_flag_graduated_all`," +
				"`price_flag_graduated_1`," +
				"`price_flag_graduated_2`," +
				"`price_flag_graduated_3`," +
				"`price_flag_graduated_4`," +
				"`price_flag_graduated_5`," +
				"`price_flag_graduated_6`," +
				"`price_flag_graduated_7`," +
				"`price_flag_graduated_8`," +
				"`price_flag_graduated_9`," +
				"`price_flag_graduated_10`," +
				"`price_flag_graduated_11`," +
				"`price_flag_graduated_12`," +
				"`price_flag_graduated_13`," +
				"`price_flag_graduated_14`," +
				"`price_flag_graduated_15`," +
				"`price_flag_graduated_16`," +
				"`price_flag_graduated_17`," +
				"`price_flag_graduated_18`," +
				"`price_flag_graduated_98`," +
				"`price_flag_graduated_99`," +
				"`products_sort`," +
				"`products_image`," +
				"`products_price`," +
				"`date_added`," +
				"`last_modified`," +
				"`date_available`," +
				"`products_weight`," +
				"`products_status`," +
				"`products_tax_class_id`," +
				"`product_template`," +
				"`product_list_template`," +
				"`manufacturers_id`," +
				"`products_ordered`," +
				"`products_transactions`," +
				"`products_fsk18`," +
				"`products_vpe`," +
				"`products_vpe_status`," +
				"`products_vpe_value`," +
				"`products_startpage`," +
				"`products_startpage_sort`," +
				"`products_average_rating`," +
				"`products_rating_count`," +
				"`products_digital`," +
				"`flag_has_specials`," +
				"`products_serials`," +
				"`total_downloads`," +
				"`products_unit_per_package`," +
				"`products_recommended_price`," +
				"`products_packung_pro_karton`," +
				"`products_tvads`," +
				"`products_tvads_sort`," +
				"`products_newprod`," +
				"`products_newprod_sort`," +
				"`products_aktion`," +
				"`products_aktion_sort`," +
				"`products_packing_units`," +
				"`products_packing_units_except`," +
				"`max_products_units`," +
				"`max_products_per_customer_status`," +
				"`products_returnable`" +
				")" +
				
				"VALUES	(" +
				"'" + this.products_id + "'," +
				"'" + this.external_id + "'," +
				"'" + this.permission_id + "'," +
				"'" + this.products_owner + "'," +
				"'" + this.products_ean + "'," +
				"'" + this.products_quantity + "'," +
				"'" + this.products_average_quantity + "'," +
				"'" + this.products_shippingtime + "'," +
				"'" + this.products_model + "'," +
				"'" + this.products_master_model + "'," +
				"'" + this.products_master_flag + "'," +
				"'" + this.products_option_template + "'," +
				"'" + this.products_option_list_template + "'," +
				"'" + this.priceGroups[0] + "'," +
				"'" + this.priceGroups[1] + "'," +
				"'" + this.priceGroups[2] + "'," +
				"'" + this.priceGroups[3] + "'," +
				"'" + this.priceGroups[4] + "'," +
				"'" + this.priceGroups[5] + "'," +
				"'" + this.priceGroups[6] + "'," +
				"'" + this.priceGroups[7] + "'," +
				"'" + this.priceGroups[8] + "'," +
				"'" + this.priceGroups[9] + "'," +
				"'" + this.priceGroups[10] + "'," +
				"'" + this.priceGroups[11] + "'," +
				"'" + this.priceGroups[12] + "'," +
				"'" + this.priceGroups[13] + "'," +
				"'" + this.priceGroups[14] + "'," +
				"'" + this.priceGroups[15] + "'," +
				"'" + this.priceGroups[16] + "'," +
				"'" + this.priceGroups[17] + "'," +
				"'" + this.priceGroups[18] + "'," +
				"'0'," +
				"'0'," +
				"'" + this.products_sort + "'," +
				"'" + this.products_image + "'," +
				"'" + this.products_price + "'," +
				"'" + this.date_added + "'," +
				"'" + this.last_modified + "'," +
				"'0000-00-00 00:00:00'," +
				"'" + this.products_weight + "'," +
				"'" + this.products_status + "'," +
				"'" + this.products_tax_class_id + "'," +
				"'" + this.product_template + "'," +
				"'" + this.product_list_template + "'," +
				"'" + this.manufacturers_id + "'," +
				"'" + this.products_ordered + "'," +
				"'" + this.products_transactions + "'," +
				"'" + this.products_fsk18 + "'," +
				"'" + this.products_vpe + "'," +
				"'" + this.products_vpe_status + "'," +
				"'" + this.products_vpe_value + "'," +
				"'" + this.products_startpage + "'," +
				"'" + this.products_startpage_sort + "'," +
				"'" + this.products_average_rating + "'," +
				"'" + this.products_rating_count + "'," +
				"'" + this.products_digital + "'," +
				"'" + this.flag_has_specials + "'," +
				"'" + this.products_serials + "'," +
				"'" + this.total_downloads + "'," +
				"'" + this.products_unit_per_package + "'," +
				"'" + this.products_recommended_price + "'," +
				"'" + this.products_packung_pro_karton + "'," +
				"'" + this.products_tvads + "'," +
				"'" + this.products_tvads_sort + "'," +
				"'" + this.products_newprod + "'," +
				"'" + this.products_newprod_sort + "'," +
				"'" + this.products_aktion + "'," +
				"'" + this.products_aktion_sort + "'," +
				"'" + this.products_packing_units + "'," +
				"'" + this.products_packing_units_except + "'," +
				"'" + this.max_products_units + "'," +
				"'" + this.max_products_per_customer_status + "'," +
				"'" + this.pfandVK + "'" +
				"); ";				
		
		return productStmt;
	}

	
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `products`;";
		return stmt;
	}
	
	
	public Iterator<ProductDescription> getProductDescriptionsIterator() {
		
		return this.productDescriptions.iterator();
	}
	
	
	public ArrayList<ProductDescription> getProductDescriptions() {
		
		return this.productDescriptions;
	}
	
	
	public void addProductDescription(final ProductDescription productDescription) {
		
		this.productDescriptions.add(productDescription);
	}
	
	
	public void replaceProductDescription(final ProductDescription productDescription) {
		
		for (int i = 0; i < this.productDescriptions.size(); i++) {
			ProductDescription productDescription2 = this.productDescriptions.get(i);
			if ( productDescription2.getLanguage_id().equals(productDescription.getLanguage_id()) ) {
				String products_order_unit_default = productDescription2.getProducts_order_unit();
				String products_packung_default = productDescription2.getProducts_packung();
				productDescription.setProducts_order_unit(products_order_unit_default);
				productDescription.setProducts_packung(products_packung_default);
				this.productDescriptions.set(i, productDescription);
			}
		}	
	}
	
	
	public String getProductCategorieInsertStatement(final int product_id, final int autoCategoryId) {
		
		String productStmt =
				"INSERT INTO `tmp_products_to_categories`" +
				"(`products_id`," +
				"`categories_id`)" +				
				"VALUES	(" +
				"'" + product_id + "'," +
				"'" + autoCategoryId + "'" +				
				"); ";
		
		return productStmt;		
	}
	
	
	public void setProductsId(final int productsId) {
		this.products_id = productsId;
	}
	
	
	public int getProductId() {
		return this.products_id;
	}
	
	public int getInternalProductsId() {
		return this.internalProductsId;
	}


	/**
	 * @return the date_available
	 */
	public Date getDate_available() {
		return date_available;
	}


	/**
	 * @param date_available the date_available to set
	 */
	public void setDate_available(Date date_available) {
		this.date_available = date_available;
	}


	/**
	 * @return the shop_wg1
	 */
	public int getShop_wg1() {
		return shop_wg1;
	}


	/**
	 * @param shop_wg1 the shop_wg1 to set
	 */
	public void setShop_wg1(short shop_wg1) {
		this.shop_wg1 = shop_wg1;
	}


	/**
	 * @return the shop_wg2
	 */
	public int getShop_wg2() {
		return shop_wg2;
	}


	/**
	 * @param shop_wg2 the shop_wg2 to set
	 */
	public void setShop_wg2(short shop_wg2) {
		this.shop_wg2 = shop_wg2;
	}


	/**
	 * @return the shop_wg3
	 */
	public int getShop_wg3() {
		return shop_wg3;
	}


	/**
	 * @param shop_wg3 the shop_wg3 to set
	 */
	public void setShop_wg3(short shop_wg3) {
		this.shop_wg3 = shop_wg3;
	}


	/**
	 * @return the shop_wg4
	 */
	public int getShop_wg4() {
		return shop_wg4;
	}


	/**
	 * @param shop_wg4 the shop_wg4 to set
	 */
	public void setShop_wg4(short shop_wg4) {
		this.shop_wg4 = shop_wg4;
	}


	/**
	 * @return the shop_wg5
	 */
	public int getShop_wg5() {
		return shop_wg5;
	}


	/**
	 * @param shop_wg5 the shop_wg5 to set
	 */
	public void setShop_wg5(short shop_wg5) {
		this.shop_wg5 = shop_wg5;
	}
	
	
	public int[] getPriceGroups() {
		
		return this.priceGroups;		
	}

}
