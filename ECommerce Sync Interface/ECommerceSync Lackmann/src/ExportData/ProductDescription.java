package ExportData;

public class ProductDescription {

	private int products_id;
	private String language_id;
	private String products_name;
	private String products_description;
	private String products_short_description;
	private String products_keywords;
	private String products_url = "";
	private String products_order_unit;
	private String products_meat_type;
	private String products_packung;

	public ProductDescription(final int products_Id, final String languageCode,
			final String products_name, final String products_description,
			final String products_short_description, final String products_order_unit,
			final String products_meat_type, final String products_packung) {
		
		this.setProducts_id(products_Id);
		this.products_name = products_name;
		this.products_description = products_description;
		this.products_short_description = products_short_description;
		this.products_keywords = this.products_short_description;
		this.setProducts_order_unit(products_order_unit);
		this.setProducts_packung(products_packung);
		this.products_meat_type = products_meat_type;
		this.language_id = languageCode;
	}


	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getInsertStatement(final int product_id) {
		
		String stmt =
				"INSERT INTO `tmp_products_description`" +
				"(`products_id`," +
				"`language_code`," +
				"`products_name`," +
				"`products_description`," +
				"`products_short_description`," +
				"`products_keywords`," +
				"`products_url`," +
				"`products_order_unit`," +
				"`products_meat_type`," +
				"`products_packung`" +				
				") VALUES ('" +
				product_id + "','" +
				this.language_id + "','" +
				this.products_name + "','" +
				this.products_description + "','" +
				this.products_short_description + "','" +
				this.products_keywords + "','" +
				this.products_url + "','" +
				this.getProducts_order_unit() + "','" +
				this.products_meat_type + "','" +
				this.getProducts_packung() +
				"'); ";
			
			return stmt;
	}


	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}	
	
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `products_description`;";
		return stmt;
	}


	/**
	 * @return the products_id
	 */
	public String getLanguage_id() {
		return this.language_id;
	}
	
	
	/**
	 * @return the products_id
	 */
	public int getProducts_id() {
		return this.products_id;
	}


	/**
	 * @param products_id the products_id to set
	 */
	public void setProducts_id(int products_id) {
		this.products_id = products_id;
	}


	/**
	 * @return the products_order_unit
	 */
	public String getProducts_order_unit() {
		return products_order_unit;
	}


	/**
	 * @param products_order_unit the products_order_unit to set
	 */
	public void setProducts_order_unit(String products_order_unit) {
		this.products_order_unit = products_order_unit;
	}


	/**
	 * @return the products_packung
	 */
	public String getProducts_packung() {
		return products_packung;
	}


	/**
	 * @param products_packung the products_packung to set
	 */
	public void setProducts_packung(String products_packung) {
		this.products_packung = products_packung;
	}

}
