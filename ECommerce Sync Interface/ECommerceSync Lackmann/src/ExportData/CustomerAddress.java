package ExportData;

import java.util.Date;

import DataInterface.Statement;

public class CustomerAddress {

	private int address_book_id;
	private int customers_id;
	private String entry_gender;
	private String entry_company;
	private String entry_company2;
	private String entry_company3;
	private String entry_phone;
	private String entry_fax;
	private Date entry_dob;
	private String entry_firstname;
	private String entry_lastname;
	private String entry_street_address;
	private String entry_postcode;
	private String entry_city;
	private String entry_country_id;
	
	public CustomerAddress(final int customerId, final String company,
			final String firstName, final String lastName, final String street,
			final String postCode, final String city, final String gender,
			final String countryCode, final String phone, final String fax, final Date birthday, final String company2, final String company3, final int addressBookId) {

		this.entry_company = company;
		this.address_book_id = addressBookId;
		this.customers_id = customerId;
		this.entry_firstname = firstName;
		this.entry_lastname = lastName;
		this.entry_street_address = street;
		this.entry_postcode = postCode;
		this.entry_city = city;
		this.entry_gender = gender;
		this.entry_country_id = countryCode;
		this.entry_company2 = company2;
		this.entry_company3 = company3;
		this.entry_dob = birthday;
		this.entry_phone = phone;
		this.entry_fax = fax;
	}

	
	public String getSelectStatement() {
		return null;
	}

	
	public String getInsertStatement() {
		
		String stmt = 
				
				"INSERT INTO `tmp_address_book` (`address_book_id`, `external_id`" +
				", `customers_id`" +
				", `customers_gender`" +
				", `customers_dob`" +
				", `customers_phone`" +
				", `customers_fax`" +
				", `customers_company`" +
				", `customers_company_2`" +
				", `customers_company_3`" +
				", `customers_firstname`" +
				", `customers_lastname`" +
				", `customers_street_address`" +
				", `customers_suburb`" +
				", `customers_postcode`" +
				", `customers_city`" +
				", `customers_country_code`" +
				", `address_class`" +
				", `date_added`" +
				", `last_modified`)" +
				
				" VALUES ('" + 0 +
				"',' " + this.address_book_id + 
				"','" + this.customers_id +
				"','" + this.entry_gender +
				"','0000-00-00 00:00:00" +
				"','" + this.entry_phone +
				"','" + this.entry_fax +
				"','" + this.entry_company +
				"','" + this.entry_company2 +
				"','" + this.entry_company3 +
				"','" + this.entry_firstname +
				"','" + this.entry_lastname +
				"','" + this.entry_street_address +
				"',NULL" +
				",'" + this.entry_postcode +
				"','" + this.entry_city +
				"','" + this.entry_country_id +
				"','default" +
				"','0000-00-00 00:00:00'" +
				",'0000-00-00 00:00:00')";
		
		return stmt;
	}
	
	
	public String getValuesStatement() {
		
		String stmt = ",('" + 0 +
				"',' " + this.address_book_id + 
				"','" + this.customers_id +
				"','" + this.entry_gender +
				"','0000-00-00 00:00:00" +
				"','" + this.entry_phone +
				"','" + this.entry_fax +
				"','" + this.entry_company +
				"','" + this.entry_company2 +
				"','" + this.entry_company3 +
				"','" + this.entry_firstname +
				"','" + this.entry_lastname +
				"','" + this.entry_street_address +
				"',NULL" +
				",'" + this.entry_postcode +
				"','" + this.entry_city +
				"','" + this.entry_country_id +
				"','default" +
				"','0000-00-00 00:00:00'" +
				",'0000-00-00 00:00:00')";
		
		return stmt;
	}

	
	public String getUpdateStatement() {
		return null;
	}
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `address_book`;";
		return stmt;
	}

}
