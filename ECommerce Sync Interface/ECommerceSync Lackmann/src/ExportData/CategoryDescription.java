package ExportData;

public class CategoryDescription {

	private int categories_id;
	private String language_code;
	private String categories_name;
	private String categories_heading_title;
	private String categories_description;
	
	
	public CategoryDescription(final int categories_id, final String categories_name, final String categories_heading_title, final String languageCode) {
		this.setCategories_id(categories_id);
		this.categories_name = categories_name;
		this.categories_heading_title = categories_heading_title;
		this.setLanguage_code(languageCode);
		this.setCategories_description(categories_name);
	}
	
	
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public String getInsertStatement(final int autoCategoryId) {
		
		String stmt =
			"INSERT INTO `tmp_categories_description`" +
			"(`categories_id`," +
			"`language_code`," +
			"`categories_name`," +
			"`categories_heading_title`," +
			"`categories_description`" +
			")" +			
			"VALUES('" +
			autoCategoryId + "','" +
			this.getLanguage_code() + "','" +
			this.categories_name + "','" +
			this.categories_heading_title + "','" +
			this.categories_description +
			"');";
		
		return stmt;
	}

	
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `categories_description`;";
		return stmt;
	}
	


	public void setCategories_description(String categories_description) {
		this.categories_description = categories_description;
	}


	/**
	 * @return the categories_id
	 */
	public int getCategories_id() {
		return categories_id;
	}


	/**
	 * @param categories_id the categories_id to set
	 */
	public void setCategories_id(int categories_id) {
		this.categories_id = categories_id;
	}


	/**
	 * @return the language_code
	 */
	public String getLanguage_code() {
		return language_code;
	}


	/**
	 * @param language_code the language_code to set
	 */
	private void setLanguage_code(String language_code) {
		this.language_code = language_code;
	}	

}
