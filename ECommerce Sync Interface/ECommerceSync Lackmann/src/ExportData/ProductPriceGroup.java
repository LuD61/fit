package ExportData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import Export.ExportDataGenerator;

public class ProductPriceGroup {

	private int pr_gr_stuf;
	private ArrayList<ProductPrice> productPrices;


	public ProductPriceGroup(final int pr_gr_stuf) {
		
		this.productPrices = new ArrayList<ProductPrice>();
		this.pr_gr_stuf = pr_gr_stuf;
	}	
	
	public void addProductPrice(final ProductPrice productPrice) {
		this.productPrices.add(productPrice);		
	}
	
	public boolean isAInPriceGroup(final int a) {
		
		Iterator<ProductPrice> it_productPrice = this.productPrices.iterator();
		while (it_productPrice.hasNext()) {
			ProductPrice productPrice = (ProductPrice) it_productPrice.next();
			if ( productPrice.getA() == a ) {
				return true;
			}
		}
		return false;
	}

	public int getPr_gr_stuf() {
		return pr_gr_stuf;
	}
	
	
	public String getInsertStatement(final ExportDataGenerator exportDataGenerator) {
		
		Iterator<ProductPrice> productsPrice = this.productPrices.iterator();
		StringBuilder stringBuilder = new StringBuilder();
		boolean firstRun = true;
		
		while (productsPrice.hasNext()) {
			ProductPrice productPrice = (ProductPrice) productsPrice.next();
			final int a = productPrice.getA();
			//final int autoProductId = exportDataGenerator.getAutoProductsId(a);
			
			if ( firstRun ) {
				stringBuilder.append(productPrice.getInsertStatement(a));
				firstRun = false;
			} else {
				stringBuilder.append(productPrice.getValuesStatement(a));
			}			
		}
		
		return stringBuilder.toString() + ";";
	}
	
}
