package MailService;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;


public class SendJavaMail {	
	
	public void sendEmail(final String htmlContent, final String subject, final String to) {		

		Properties props = new Properties();
	    props.put("mail.smtp.host", "setec-systeme.de");
	    props.put("mail.from", "servicemail@setec-systeme.de");
	    props.put("mail.protocol.port", "587");
	    props.put("mail.protocol.user", "web1178p1");
	    
	    props.put("mail.smtp.auth", "true"); 

	    Authenticator auth = new SMTPAuth(); 

	    Session session = Session.getDefaultInstance(props, auth); 

	    try {    	
	    	
	        MimeMultipart content = new MimeMultipart( "alternative" );
	        MimeBodyPart html = new MimeBodyPart();
	        html.setContent(htmlContent, "text/html");
	        content.addBodyPart( html );	        
	        
	        MimeMessage msg = new MimeMessage(session);
	        msg.setSubject(subject);
	        msg.setFrom();
	        msg.setRecipients(Message.RecipientType.TO, to);
	        msg.setRecipients(Message.RecipientType.CC, "daniel.heil@setec-systeme.de");
	        msg.setContent( content );
	        msg.setHeader( "MIME-Version" , "1.0" );
	        msg.setHeader( "Content-Type" , content.getContentType() );
	        msg.setHeader( "X-Mailer", "Java-Mailer V 1.60217733" );
	        msg.setSentDate( new Date() );
	                 

	        Transport.send(msg);
	        
	    } catch (MessagingException mex) {
	    	
	        System.out.println("send failed, exception: " + mex);
	    }
	    
	}
}