package ImportData;

import java.util.Date;

import DataInterface.Statement;

public class NewCustomerAddressShop implements Statement {

	private int address_book_id;
	private int customers_id;
	private int entry_gender;
	private String entry_company;
	private String entry_firstname;
	private String entry_lastname;
	private String entry_street_address;
	private String entry_postcode;
	private String entry_city;
	private String entry_state = "";
	private String entry_country_id;
	private int entry_zone_id;
	private String email;
	private String tel;
	private String fax;
	
	
	public NewCustomerAddressShop(final int address_id, final String company,
			final String firstName, final String lastName, final String street,
			final String postCode, final String city, final String gender,
			final String country, final String email, final String tel, final String fax) {

		this.entry_company = company;
		this.address_book_id = address_id;
		this.entry_firstname = firstName;
		this.entry_lastname = lastName;
		this.entry_street_address = street;
		this.entry_postcode = postCode;
		this.entry_city = city;
		
		if ( gender == "m") {
			this.entry_gender = 1;
		} else if ( gender == "f" ){
			this.entry_gender = 3;
		} else {
			this.entry_gender = 1;
		}
		
		this.entry_country_id = country;
		this.email = email;
		this.tel = tel;
		this.fax = fax;
	}

	@Override
	public String getSelectStatement() {
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String stmt = 
				
				"INSERT INTO adr" +
				"(" +
				"adr,adr_krz,adr_nam1,adr_nam2,adr_typ," +
				"adr_verkt,anr,delstatus,fax,fil,geb_dat,land,mdn," +
				"merkm_1,merkm_2,merkm_3,merkm_4,merkm_5," +
				"modem,ort1,ort2,partner,pf,plz,staat,str,tel,telex,txt_nr,plz_postf,plz_pf," +
				"iln,email,swift,adr_nam3,mobil, iban) " +
				"VALUES((Select Min(adr) from adr) - 1" +
				",'"+ this.entry_company + "','" + this.entry_company + "','',10," +
				"''," + this.entry_gender + ",0,'" + this.fax + "',0,'',0,1," +
				"'  ','  ','  ','  ','  '," +
				"'','" + this.entry_city + "','','','','" + this.entry_postcode + "',0,'" + this.entry_street_address + "','" + this.tel + "','',0,'',''," +
				"'','" + this.email + "','','(null)','(null)',''); ";
		
		return stmt;
	}

	@Override
	public String getUpdateStatement() {
		return null;
	}
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `address_book`;";
		return stmt;
	}

}
