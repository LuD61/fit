package ImportData;

import java.util.ArrayList;
import java.util.Iterator;


public class Orders {
	
	private ArrayList<Order> orders = null;
	
	public Orders() {		
		this.orders = new ArrayList<Order>();
	}
	
	
	public void addOrder(final Order order) {
		this.orders.add(order);
	}
	
	public Iterator<Order> getOrders() {
		return this.orders.iterator();
	}
	
	public Order getOrderById(final int orders_Id) {
		Iterator<Order> itOrders = this.orders.iterator();
		while (itOrders.hasNext()) {
			Order order = (Order) itOrders.next();
			if ( order.getOrders_id() == orders_Id ) {
				return order;
			}
		}
		return null;		
	}
	
	public int getSize() {
		return this.orders.size();
	}
	
	public boolean isEmpty() {
		return this.orders.isEmpty();
	}
	
}
