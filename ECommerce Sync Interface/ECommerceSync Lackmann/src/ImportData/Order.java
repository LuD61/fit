package ImportData;

import java.util.ArrayList;
import java.util.Date;



public class Order {
	

	private int orders_id;
	private String customers_cid;
	private String comments;
	private Date date_purchased;
	private String customers_ip;
	private int delivery_address_book_id;
	private int billing_address_book_id;
	
	
	
	private ArrayList<OrderProduct> orderProducts;
	
	
	public Order(final int orders_id, final String customers_cid, final String comments, final Date date_purchased, final String customers_ip, 
			final int delivery_address_book_id) {		
		
		this.setOrders_id(orders_id);
		this.setCustomers_cid(customers_cid);
		this.setComments(comments);
		this.setDate_purchased(date_purchased);
		this.setCustomers_ip(customers_ip);
		this.setDelivery_address_book_id(delivery_address_book_id);
		this.orderProducts = new ArrayList<OrderProduct>();
	}
	
	
	public void addOrderProducts(final OrderProduct orderProduct) {
		this.orderProducts.add(orderProduct);
	}
	
	
	public ArrayList<OrderProduct> getOrderProducts() {
		return this.orderProducts;
	}


	public int getOrders_id() {
		return orders_id;
	}


	public void setOrders_id(int orders_id) {
		this.orders_id = orders_id;
	}


	public int getCustomers_cid() {
		int intValue = 0;
		
		if ( this.customers_cid != "" ) {
			try {
				intValue = Integer.valueOf(this.customers_cid).intValue();
			} catch ( Exception e ){
				intValue = 0;
			}			
		}
		return intValue;
	}


	public void setCustomers_cid(String customers_cid) {
		this.customers_cid = customers_cid;
	}


	public String getComments() {
		return comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}


	public Date getDate_purchased() {
		return date_purchased;
	}


	public void setDate_purchased(Date date_purchased) {
		this.date_purchased = date_purchased;
	}


	public String getCustomers_ip() {
		return customers_ip;
	}


	public void setCustomers_ip(String customers_ip) {
		this.customers_ip = customers_ip;
	}


	public int getDelivery_address_book_id() {
		return delivery_address_book_id;
	}


	public void setDelivery_address_book_id(int delivery_address_book_id) {
		this.delivery_address_book_id = delivery_address_book_id;
	}


	public int getBilling_address_book_id() {
		return billing_address_book_id;
	}


	public void setBilling_address_book_id(int billing_address_book_id) {
		this.billing_address_book_id = billing_address_book_id;
	}

}
