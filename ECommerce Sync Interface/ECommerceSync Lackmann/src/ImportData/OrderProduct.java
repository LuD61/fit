package ImportData;

import java.util.Date;

public class OrderProduct {

	
	private int orders_products_id;
	private int products_id;
	private String products_model;
	private String products_name;
	private int products_quantity;
	private String products_model_beilage;
	private Date liefdat;
	private String comments;

	
	
	public OrderProduct(final int orders_products_id, final int products_id,
			final String products_model, final String products_name,
			final int products_quantity, final Date liefdat, final String comments) {
		
		this.setOrders_products_id(orders_products_id);
		this.setProducts_id(products_id);
		this.setProducts_model(products_model);
		this.setProducts_name(products_name);
		this.setProducts_quantity(products_quantity);
		this.setLiefdat(liefdat);
		this.setComments(comments);
	}



	public double getProducts_model_beilage() {
		
		int intValue = 0;
		
		if ( this.products_model_beilage != "" ) {
			try {
				intValue = Integer.valueOf(this.products_model_beilage).intValue();
			} catch ( Exception e ){
				intValue = 0;
			}			
		}
		return intValue;
		
	}



	public void setProducts_model_beilage(String products_model_beilage) {
		this.products_model_beilage = products_model_beilage;
	}



	public int getProducts_quantity() {
		return products_quantity;
	}



	public void setProducts_quantity(int products_quantity) {
		this.products_quantity = products_quantity;
	}



	public String getProducts_name() {
		return products_name;
	}



	public void setProducts_name(String products_name) {
		this.products_name = products_name;
	}



	public double getProducts_model() {
		int intValue = 0;
		
		if ( this.products_model != "" ) {
			try {
				intValue = Integer.valueOf(this.products_model).intValue();
			} catch ( Exception e ){
				intValue = 0;
			}			
		}
		return intValue;
	}



	public void setProducts_model(String products_model) {
		this.products_model = products_model;
	}



	public int getProducts_id() {
		return products_id;
	}



	public void setProducts_id(int products_id) {
		this.products_id = products_id;
	}



	public int getOrders_products_id() {
		return orders_products_id;
	}



	public void setOrders_products_id(int orders_products_id) {
		this.orders_products_id = orders_products_id;
	}



	public Date getLiefdat() {
		return liefdat;
	}



	public void setLiefdat(Date liefdat) {
		this.liefdat = liefdat;
	}



	public String getComments() {
		return comments;
	}



	public void setComments(String comments) {
		this.comments = comments;
	}

}
