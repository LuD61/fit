package ImportData;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import Converter.MD5Converter;
import DataEnumerations.Country;
import DataEnumerations.Gender;
import DataEnumerations.Zones;
import DataInterface.Statement;

public class NewCustomerShop implements Statement {

	private int customers_id;
	private String customers_cid;
	private String customers_vat_id = "";
	private int customers_vat_id_status = 0;
	private int customers_status;
	private String customers_gender;
	private String customers_firstname;
	private String customers_lastname;
	private String customers_email_address = "";
	private int customers_default_address_id;
	private String customers_telephone;
	private String customers_fax;
	private String customers_password; // MD5 Hash
	private String companyName;

	private ArrayList<NewCustomerAddressShop> customerAddresses;
	private int address_id;

	public NewCustomerShop(final int customers_id, final String company, final String gender, final String firstName,
			final String lastName, final String street, final String postCode,
			final String city, final String country,
			final String tel, final String fax, final String customers_default_language, final String customers_liefertag, final String eMail, final int address_id) {
		
		this.customers_id = customers_id;
		this.customerAddresses = new ArrayList<NewCustomerAddressShop>();
		this.customers_gender = gender;		
		this.setCustomers_firstname(firstName);
		this.setCustomers_lastname(lastName);
		this.companyName = company;	
		this.customers_telephone = tel;
		this.customers_fax = fax;
		this.address_id = address_id;
		
		//TODO DefaultAdresse
		NewCustomerAddressShop customerAddress = new NewCustomerAddressShop(this.address_id, company,
				firstName, lastName, street, postCode, city, this.customers_gender, country, eMail, tel, fax);		
		this.addCustomerAddress(customerAddress);

	}

	public void setPassword(final String password) {
		this.customers_password = password;
	}
	
	public void addCustomerAddress(final NewCustomerAddressShop customerAddress) {
		this.customerAddresses.add(customerAddress);
	}

	public void setUStId(final String ust_id) {
		this.customers_vat_id = ust_id;
	}

	public void setEmailAdress(final String email) {
		this.setCustomers_email_address(email);
	}

	@Override
	public String getSelectStatement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsertStatement() {
		
		String customerStmt = "INSERT INTO " +
				"fit.kun(mdn,fil,kun,adr1,adr2,adr3,kun_seit,txt_nr1,frei_txt1,kun_krz1,kun_bran,kun_krz2,kun_krz3,kun_typ,bbn,pr_stu,pr_lst,vereinb,inka_nr," +
				"vertr1,vertr2,statk_period,a_period,sprache,txt_nr2,frei_txt2,freifeld1,freifeld2,tou,vers_art,lief_art,fra_ko_ber,rue_schei,form_typ1,auflage1," +
				"freifeld3,freifeld4,zahl_art,zahl_ziel,form_typ2,auflage2,txt_nr3,frei_txt3,nr_bei_rech,rech_st,sam_rech,einz_ausw,gut,rab_schl,bonus1,bonus2,tdm_grenz1," +
				"tdm_grenz2,jr_plan_ums,deb_kto,kred_lim,inka_zaehl,bank_kun,blz,kto_nr,hausbank,kun_of_po,kun_of_lf,kun_of_best,delstatus,kun_bran2,rech_fuss_txt,ls_fuss_txt,ust_id," +
				"rech_kopf_txt,ls_kopf_txt,gn_pkt_kz,sw_rab,bbs,inka_nr2,sw_fil_gr,sw_fil,ueb_kz,modif,kun_leer_kz,ust_id16,iln,waehrung,pr_ausw,pr_hier,pr_ausw_ls,pr_ausw_re,kun_gr1,kun_gr2," +
				"eg_kz,bonitaet,kred_vers,kst,edi_typ,sedas_dta,sedas_kz,sedas_umf,sedas_abr,sedas_gesch,sedas_satz,sedas_med,sedas_nam,sedas_abk1,sedas_abk2,sedas_abk3,sedas_nr1,sedas_nr2," +
				"sedas_nr3,sedas_vb1,sedas_vb2,sedas_vb3,sedas_iln,kond_kun,kun_schema,plattform,be_log,stat_kun,ust_nummer,fak_kz,fak_nr,rechiln,hirarchie1,hirarchie2,hirarchie3,hirarchie4," +
				"etikett,shop_kz,shop_passw,cmr,trakober,pal_bew, shop_history) VALUES(" +
				"1,0,(Select Min(kun) from kun) - 1,(Select Min(adr) from adr) - 1,(Select Min(adr) from adr) - 1,(Select Min(adr) from adr) - 1," +
				"'10.06.1999',0,'','" + this.companyName + "','0','" + this.companyName + "','" + this.companyName + "',9,0,99,0,'     ',0," +
				"0,0,'0','0',0,0,'','','',0,'0',0,'0',0,'2',0," +
				"'','',8,8,'1',2,0,'','','4',2,5,1,'',0,0,0,0," +
				"0,'0',0,0,'',0,'',1,0,0,0,0,'0',0,0,'',0,0,'','N','',0,0,0,'N','B',0,'2222222','',2,0,'1',0,0,2,2,0,0,0,0,' ',0,'  ',0,0,0,0,0,'',0,0,0,'','',''," +
				"0,0,0,'',0,1,'','',9999,'','',0,'',0,0,0,0,0,'J','',0,0,0,'N');";
			
				
		return customerStmt;			
	}

	@Override
	public String getUpdateStatement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getDeleteStatement() {
		final String stmt = "Delete from `customers`;";
		return stmt;
	}
	
	
	public String getCustomerCID() {
		return this.customers_cid;
	}
	
	
	public int getCustomerStatus() {
		return this.customers_status;
	}
	
	public String getCompanyName() {
		return this.companyName;
	}
	
	public Iterator<NewCustomerAddressShop> getAddresses() {
		
		return this.customerAddresses.iterator();
	}

	public String getCustomers_email_address() {
		return customers_email_address;
	}

	public void setCustomers_email_address(String customers_email_address) {
		this.customers_email_address = customers_email_address;
	}

	public String getCustomers_firstname() {
		return customers_firstname;
	}

	public void setCustomers_firstname(String customers_firstname) {
		this.customers_firstname = customers_firstname;
	}

	public String getCustomers_lastname() {
		return customers_lastname;
	}

	public void setCustomers_lastname(String customers_lastname) {
		this.customers_lastname = customers_lastname;
	}
	

}
