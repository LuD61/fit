package ImportData;

public class ProductLanguage {

	
	private int products_id;
	private String products_name;
	private String products_description;
	private String language_code;
	
	
	public ProductLanguage(final int products_id, final String products_name, final String products_description, final String language_code) {
		
		this.setProducts_id(products_id);
		this.setProducts_name(products_name);
		this.setProducts_description(products_description);
		this.setLanguage_code(language_code);
	}


	/**
	 * @return the products_id
	 */
	public int getProducts_id() {
		return products_id;
	}


	/**
	 * @param products_id the products_id to set
	 */
	public void setProducts_id(int products_id) {
		this.products_id = products_id;
	}


	/**
	 * @return the products_name
	 */
	public String getProducts_name() {
		return products_name;
	}


	/**
	 * @param products_name the products_name to set
	 */
	public void setProducts_name(String products_name) {
		this.products_name = products_name;
	}


	/**
	 * @return the products_description
	 */
	public String getProducts_description() {
		return products_description;
	}


	/**
	 * @param products_description the products_description to set
	 */
	public void setProducts_description(String products_description) {
		this.products_description = products_description;
	}


	/**
	 * @return the language_code
	 */
	public String getLanguage_code() {
		return language_code;
	}


	/**
	 * @param language_code the language_code to set
	 */
	public void setLanguage_code(String language_code) {
		this.language_code = language_code;
	}
	
	
	


}
