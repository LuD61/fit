package Import;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import Connection.MySQLConnectorMYSQL;
import ImportData.NewCustomerAddressShop;
import ImportData.NewCustomerShop;
import ImportData.Order;
import ImportData.OrderProduct;
import ImportData.Orders;
import MailService.SendJavaMail;

import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import fit.informixconnector.InformixConnector;


public class ImportDataGenerator {

	private static final Logger logger = Logger.getLogger(ImportDataGenerator.class.getName()); 
	Handler fh = null;
	private Orders orders;
	
	public void startProcess(final boolean simulation, final Date beginDate, final Date endDate, final String mySQLComputerName, 
			final String mySQLPortNumber, final String mySQLDatabaseName, final String mySQLUserName, final String mySQLPassword, final String emailTO) throws SQLException {
		
		try {
			Date now = new Date();
			SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH.mm.ss");
			String nowString = simpleDateTimeFormat.format(now); 
			fh = new FileHandler("d:/User/fit/bin/ImportOrdersLog/ImportOrders " + nowString + ".log");
			logger.addHandler(fh);
			fh.setFormatter(new SimpleFormatter());
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		final SendJavaMail sendJavaMail = new SendJavaMail();
		
		MySQLConnectorMYSQL mySQLConnector = new MySQLConnectorMYSQL();
		mySQLConnector.createConnection(mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword, true);
		logger.info("MySQL Connection created.");
		InformixConnector informixConnector = new InformixConnector();
		informixConnector.createConnection();
		logger.info("Informix Connection created.");		
		
		ArrayList<NewCustomerShop> newCustomers = new ArrayList<NewCustomerShop>();
		
		try {
			this.orders = new Orders();			
			this.getOrders(mySQLConnector);
			this.getOrderProducts(mySQLConnector);
			newCustomers = this.getNewCustomers(mySQLConnector);
		} catch (SQLException e) {
			e.printStackTrace();
			mySQLConnector.closeConnection();
			informixConnector.closeConnection();
			logger.warning("Error while reading data from MySQL." + e.getMessage());
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			mySQLConnector.closeConnection();
			informixConnector.closeConnection();
			logger.warning("Error while reading data from MySQL." + e.getMessage());
			System.exit(1);
		}		
		
		
		if ( this.orders.isEmpty() ) {
			
			logger.warning("No orders for Import available.");
			sendJavaMail.sendEmail("Keine Auftraege zum Import vorhanden.", "Keine Auftraege vorhanden f�r den Zeitraum: " + beginDate + " - " + endDate, emailTO);
			
			if ( !newCustomers.isEmpty() ) {
				this.insertCustomersToInformix(informixConnector, newCustomers, emailTO);
				logger.info(newCustomers.size() + " NewCustomers inserted to Informix Database");
				this.updateCustomerStatus(mySQLConnector);
				logger.info(newCustomers.size() + " Customers updated in WebShop to customers_status = 99");
			} else {
				logger.warning("No new customers for Import available.");
			}
			
		} else {
			
			try {
				
				this.insertOrdersToInformix(informixConnector, simulation);
				logger.info(this.orders.getSize() + " Orders are inserted to Informix Database");
				
				if ( !simulation ) {
					this.updateOrderStatus(mySQLConnector);
					logger.info(this.orders.getSize() + " Orders status updated to order_status = 40");
				}
				
				if ( !newCustomers.isEmpty() ) {
					this.insertCustomersToInformix(informixConnector, newCustomers, emailTO);
					logger.info(newCustomers.size() + " NewCustomers inserted to Informix Database");
					this.updateCustomerStatus(mySQLConnector);
					logger.info(newCustomers.size() + " Customers updated in WebShop to customers_status = 99");
				} else {
					logger.warning("No new customers for Import available.");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				informixConnector.closeConnection();
				mySQLConnector.closeConnection();
				logger.warning("Error while executing INSERT to Informix database." + e.getMessage());
				sendJavaMail.sendEmail("Fehler beim INSERT in Informix DB", "Fehler beim INSERT in Informix DB. Bitte �berpr�fen Sie die Informix Datenbank Verbindung", emailTO);
				System.exit(1);
			}		
			
			System.out.println("Auftraege erfolgreich importiert");
			String addText = "";
			if ( simulation ) {
				addText = "ohne LOESCHEN";
			} else {
				addText = "mit LOESCHEN";
			}
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
			SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH.mm.ss");
			String beginDateString = simpleDateFormat.format(beginDate);
			String endDateString = simpleDateFormat.format(endDate);
			Date now = new Date();
			String nowString = simpleDateTimeFormat.format(now);
			
			sendJavaMail.sendEmail("Auftraege erfolgreich um " + nowString + " | " + addText + " importiert. Es wurden " + this.orders.getSize() + " Auftraege importiert.", "Auftragsimport erfolgreich f�r den Zeitraum: " + beginDateString + " - " + endDateString, emailTO);
		}
		
		informixConnector.closeConnection();
		mySQLConnector.closeConnection();				
		System.exit(0);
	}
	
	
	private void insertCustomersToInformix(final InformixConnector informixConnector, final ArrayList<NewCustomerShop> newCustomerShops, final String emailTO) {
		
		Iterator<NewCustomerShop> it_newCustomers = newCustomerShops.iterator();
		String newCustomers = "";
		
		while (it_newCustomers.hasNext()) {
			
			NewCustomerShop newCustomerShop = (NewCustomerShop) it_newCustomers
					.next();
			
			newCustomers = newCustomers + newCustomerShop.getCompanyName() + " " + newCustomerShop.getCustomers_firstname() + " " + 
			newCustomerShop.getCustomers_lastname() + " " + newCustomerShop.getCustomers_email_address() + " | ";
			final String customerStmt = newCustomerShop.getInsertStatement();
			informixConnector.executeStatement(customerStmt);
			
			Iterator<NewCustomerAddressShop> it_customerAddress = newCustomerShop.getAddresses();
			while (it_customerAddress.hasNext()) {
				NewCustomerAddressShop customerAddress = (NewCustomerAddressShop) it_customerAddress
						.next();
				String customerAdressStatement = customerAddress.getInsertStatement();
				informixConnector.executeStatement(customerAdressStatement);
			}
			
		}		
				
		
		final SendJavaMail sendJavaMail = new SendJavaMail();
		sendJavaMail.sendEmail("Es wurden neu registrierte Kunden aus dem Shop in fit importiert. Bitte �berpr�fen Sie diese: " + newCustomers, "Neue Kunden aus Shop in fit importiert" , emailTO);
	}


	private ArrayList<NewCustomerShop> getNewCustomers(final MySQLConnectorMYSQL mySQLConnector) throws SQLException {
		
		ArrayList<NewCustomerShop> newCustomerShops = new ArrayList<NewCustomerShop>();
		
		final String stmt = "SELECT c.customers_cid" +
							", c.customers_vat_id" +
							", c.customers_email_address" +
							", c.customers_password" +
							", c.date_added" +
							", c.customers_default_language" +
							", c.customers_liefertag" +
							", a.customers_phone" +
							", a.customers_fax" +
							", a.customers_company" +
							", a.customers_gender" +
							", a.customers_firstname" +
							", a.customers_lastname" +
							", a.customers_street_address" +
							", a.customers_postcode" +
							", a.customers_city" +
							", a.customers_country_code" +
							" FROM " +
							"xt_customers as c, xt_customers_addresses as a" +
							" WHERE " +
							"c.customers_id = a.customers_id" +
							" AND " +
							"c.customers_status = 98;";
		
		final ResultSet newCustomersResultSet = mySQLConnector.executeStatement(stmt);
		
		logger.info("Execute NewCustomers statement: " + stmt);
			
		while(newCustomersResultSet.next()) {
			
			final String customers_cid = newCustomersResultSet.getString("customers_cid");
			final String customers_vat_id = newCustomersResultSet.getString("customers_vat_id");
			final String customers_email_address = newCustomersResultSet.getString("customers_email_address");
			final String customers_password = newCustomersResultSet.getString("customers_password");
			final String customers_default_language = newCustomersResultSet.getString("customers_default_language");
			final String customers_liefertag = newCustomersResultSet.getString("customers_liefertag");
			final String customers_phone = newCustomersResultSet.getString("customers_phone");
			final String customers_fax = newCustomersResultSet.getString("customers_fax");
			final String customers_company = newCustomersResultSet.getString("customers_company");
			final String customers_gender = newCustomersResultSet.getString("customers_gender");
			final String customers_firstname = newCustomersResultSet.getString("customers_firstname");
			final String customers_lastname = newCustomersResultSet.getString("customers_lastname");
			final String customers_street_address = newCustomersResultSet.getString("customers_street_address");
			final String customers_postcode = newCustomersResultSet.getString("customers_postcode");
			final String customers_city = newCustomersResultSet.getString("customers_city");
			final String customers_country_code = newCustomersResultSet.getString("customers_country_code");
			
			NewCustomerShop newCustomer = new NewCustomerShop(1, customers_company, customers_gender, customers_firstname, customers_lastname, customers_street_address, 
					customers_postcode, customers_city, customers_country_code, customers_phone, customers_fax, customers_default_language, customers_liefertag, customers_email_address, 1);
			newCustomer.setUStId(customers_vat_id);
			newCustomer.setPassword(customers_password);
			
			newCustomerShops.add(newCustomer);
			
			logger.info("NewCustomer " + customers_company + " | " + customers_cid + " added to Informix."); 
		}	
		
		return newCustomerShops;
	
	}


	private void updateOrderStatus(final MySQLConnectorMYSQL mySQLConnector) {
		
		StringBuilder stringBuilder = new StringBuilder();
		Iterator<Order> updateOrders = this.orders.getOrders();		
		
		while (updateOrders.hasNext()) {
			Order order = (Order) updateOrders.next();
			final String orderUpdateStmt = 
					"UPDATE `xt_orders` SET " +
					"orders_status = '40'" +
					" WHERE " +
					"orders_id = '" + order.getOrders_id() + "'; ";
			
			stringBuilder.append(orderUpdateStmt);		
		}
		if ( stringBuilder.length() > 0 ) {
			mySQLConnector.execute(stringBuilder.toString());	
		}		
	}
	
	
	
	private void updateCustomerStatus(final MySQLConnectorMYSQL mySQLConnector) {
		
		final String customerUpdateStmt = 
					"UPDATE `xt_customers` SET " +
					"customers_status = '99'" +
					" WHERE " +
					"customers_status = '98';";
			
		mySQLConnector.execute(customerUpdateStmt);		
	}


	private void getOrders(final MySQLConnectorMYSQL mySQLConnector) throws SQLException, IOException {
		
		final String stmt = "SELECT o.`orders_id`" +
							", o.`customers_cid`" +
							", o.`comments`" +
							", o.`date_purchased`" +
							", o.`customers_ip`" +
							", a.`external_id`" +
							" FROM " +
							"xt_orders as o, xt_customers_addresses a" +
							" WHERE " +
							" o.`customers_id` = a.customers_id " +
							" AND " +
							"o.orders_status != '40';";
		
		final ResultSet ordersResultSet = mySQLConnector.executeStatement(stmt);
		
		logger.info("Execute Orders statement: " + stmt);
		
		while(ordersResultSet.next()) {
			
			final int orders_id = ordersResultSet.getInt("orders_id");
			final String customers_cid = this.getString(ordersResultSet, "customers_cid");
			final String comments = this.getString(ordersResultSet, "comments");
			final Date date_purchased = ordersResultSet.getDate("date_purchased");
			final String customers_ip = ordersResultSet.getString("customers_ip");
			String external_id = ordersResultSet.getString("external_id");
			int delivery_address_book_id = 0;
			if ( external_id != null && !external_id.isEmpty()) {
				external_id = external_id.trim();	
				delivery_address_book_id = Integer.valueOf(external_id);
			} else {
				delivery_address_book_id = Integer.valueOf(customers_cid);
			}		
			
			Order order = new Order(orders_id, customers_cid, comments, date_purchased, customers_ip, delivery_address_book_id);
			this.orders.addOrder(order);
			
			logger.info("Order ID " + orders_id);
		}		
	}
	
	
	
	
	
	private void getOrderProducts(final MySQLConnectorMYSQL mySQLConnector) throws SQLException, IOException {
				
		final String stmt = "SELECT op.`orders_products_id`," +
				"op.`orders_id`," +
				"op.`products_id`," +
				"op.`products_model`," +
				"op.`products_name`," +
				"op.`products_quantity` " +
				" FROM " +
				"xt_orders as o, xt_orders_products as op" +
				" WHERE " +
				"o.orders_id = op.orders_id" +
				" and " +
				"o.orders_status != '40';";	

		
		final ResultSet ordersResultSet = mySQLConnector.executeStatement(stmt);
		logger.info("Execute OrdersProducts statement: " + stmt);
		
		while(ordersResultSet.next()) {
			
			final int orders_products_id = ordersResultSet.getInt("orders_products_id");
			final int orders_id = ordersResultSet.getInt("orders_id");
			final int products_id = ordersResultSet.getInt("products_id");
			String products_model = null;
			products_model = this.getString(ordersResultSet, "products_model");
			final String products_name = ordersResultSet.getString("products_name");
			final int products_quantity = ordersResultSet.getInt("products_quantity");
			final Date liefdat = new Date();
			final String comments = "";
			
			logger.info("Orders Products ID " + orders_products_id);
			
			final OrderProduct orderProduct = new OrderProduct(orders_products_id, products_id, products_model, products_name, products_quantity, liefdat, comments);
			Order order = this.orders.getOrderById(orders_id);
			order.addOrderProducts(orderProduct);			
		}		
	}
	
	
	
	private String getString(ResultSet rs, String columnName) throws SQLException, IOException {
		Reader r = rs.getCharacterStream(columnName);
		if (r == null)
			return null;
		
		StringBuilder tmp = new StringBuilder();
		char[] buf = new char[512];
		int lu;
		do {
			lu = r.read(buf);
			if (lu > 0)
				tmp.append(buf, 0, lu);
		} while (lu >= 0);
		return tmp.toString();
	}
	
	
	
	private String selectOrderProductAttributes(final int orders_id, final int orders_products_id, final MySQLConnectorMYSQL mySQLConnector) {
		
		String products_options = "";
		String products_options_values = "";
		String attributes_model = "";
		
		String stmt = 	"SELECT `products_options`, " +
						"`products_options_values`" +
						" FROM `orders_products_attributes`" +
						" WHERE `orders_id` = " + orders_id +
						" AND `orders_products_id` = " + orders_products_id + ";";
		
		final ResultSet ordersResultSet = mySQLConnector.executeStatement(stmt);
		
		try {
			while(ordersResultSet.next()) {			
				products_options = ordersResultSet.getString("products_options");
				products_options_values = ordersResultSet.getString("products_options_values");
				
				stmt = 	"SELECT pa.`attributes_model`" +
						" FROM `products_options` AS po, `products_options_values` AS pov, `products_attributes` AS pa" +
						" WHERE po.`products_options_name` = '" + products_options +
						"' AND pov.`products_options_values_name` = '" + products_options_values + 
						"' AND pa.`options_id` = po.`products_options_id`" +
						" AND pa.`options_values_id` = `products_options_values_id`;";
				
				final ResultSet beilage = mySQLConnector.executeStatement(stmt);
				
				while(beilage.next()) {			
					attributes_model = beilage.getString("attributes_model");
				}
			}			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return attributes_model;		
	}
	
	
	
	private void insertOrdersToInformix(final InformixConnector informixConnector, final boolean simulation) throws Exception {
		
		short gueltig = 0;
		
		if (simulation) {
			gueltig = 0;
		} else {
			gueltig = 1;
		}
		
		//TODO guelitg Flag muss in shopaufp verschoben werden, aufgrund der M�glichkeit nur einzelne Positionen abzurufen 
		//TODO + orders_products_id als zus�tzliches Feld in shopaufp, wird ben�tigt f�r korrektes L�schen + liefdat f�r den Abruf!
		
		Iterator<Order >it_orders = this.orders.getOrders(); 
		
		while (it_orders.hasNext()) {
			
			Order order = (Order) it_orders.next();
			
			Date sqlDate = new java.sql.Date(order.getDate_purchased().getTime());
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
			String datePurchasedString = simpleDateFormat.format(sqlDate);
			
			String shopaufkDeleteStmt = "delete from shopaufk where orders_id = " + order.getOrders_id() + ";";
			
			String shopaufkInsertstmt = 
					"INSERT INTO shopaufk" +
					"(orders_id," +
					"kun," +
					"comments," +
					"lieferdat," +
					"auf," +
					"gueltig," +
					"stat," +
					"adr" +
					")	VALUES (" +
					order.getOrders_id() + "," +
					order.getCustomers_cid() + ",'" +
					"','" +
					datePurchasedString + "'," +
					0 + "," +
					gueltig + "," +
 					0 + "," +
 					order.getDelivery_address_book_id() +
					"); ";
			
			informixConnector.executeStatement(shopaufkDeleteStmt);			
			informixConnector.executeStatement(shopaufkInsertstmt);
			
			Iterator<OrderProduct> it_orderProducts = order.getOrderProducts().iterator();			
			
			while (it_orderProducts.hasNext()) {
				OrderProduct orderProduct = (OrderProduct) it_orderProducts.next();
				final int orders_products_id = orderProduct.getOrders_products_id();
				
				Date sqlLiefDate = new java.sql.Date(orderProduct.getLiefdat().getTime());
				String liefDateString = simpleDateFormat.format(sqlLiefDate);
				
				String shopaufpDeleteStmt = "delete from shopaufp where orders_id = " + order.getOrders_id() + " and orders_product_id = " + orders_products_id + ";";
				
				String shopaufpInsertStmt = 
						"INSERT INTO shopaufp" +
						"(orders_id," +
						"a," +
						"stk," +
						"a_beilage," +
						"auf," +
						"gueltig," +
						"stat," +
						"lieferdat," +
						"orders_product_id," +
						"kun" +
						")	VALUES (" +
						order.getOrders_id() + "," +
						orderProduct.getProducts_model() + "," +
						orderProduct.getProducts_quantity() + "," +
						0 + "," +
						0 + "," +
						gueltig + "," +
						0 + ",'" +
						liefDateString + "'," +
						orders_products_id + "," +
						order.getCustomers_cid() +
						"); ";
				
				informixConnector.executeStatement(shopaufpDeleteStmt);
				informixConnector.executeStatement(shopaufpInsertStmt);
				
			}			
		}		
		
	}
	
	
	private void createDirectory() throws IOException { 
        String path = "c:/ImportOrdersLog/"; 
        String fileName = "test.txt"; 
        String dirName = "bums"; 
        File file = new File(path + dirName + "/" + fileName); 
        File dir = new File(path + dirName); 

        if (dir.mkdir()) { 
            System.out.println("Datei erstellt: " + file.createNewFile()); 
        } else { 
            System.out.println(dir + " konnte nicht erstellt werden"); 
        } 
    } 
	
}
