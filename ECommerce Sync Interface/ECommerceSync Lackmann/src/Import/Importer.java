package Import;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import Import.ImportDataGenerator;
import UI.UINotDelete;
import UI.UIWithDelete;

import javax.swing.JFrame;


/**
 * 
 */

/**
 * @author danielh
 *
 */
public class Importer extends JFrame implements ActionListener {	
	
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private boolean simulation = false;
	private ImportDataGenerator importDataGenerator = null;
	private String informixComputerName;
	private String informixServer;
	private String informixUserName;
	private String informixPassword;
	private String mySQLComputerName;
	private String mySQLPortNumber;
	private String mySQLDatabaseName;
	private String mySQLUserName;
	private String mySQLPassword;
	private String emailTO;

	
	public void startImport (final boolean withDialog, final boolean simulation, final String mySQLComputerName, final String mySQLPortNumber, 
			final String mySQLDatabaseName, final String mySQLUserName, final String mySQLPassword, final String emailTO) {		
		
		this.simulation = simulation;
		this.mySQLComputerName = mySQLComputerName;
		this.mySQLPortNumber = mySQLPortNumber;
		this.mySQLDatabaseName = mySQLDatabaseName;
		this.mySQLUserName = mySQLUserName;
		this.mySQLPassword = mySQLPassword;
		this.emailTO = emailTO;
		
		
		if ( withDialog ) {
			
			JFrame frame = new JFrame();
			
			if ( !simulation ) {
				frame.setContentPane(new UIWithDelete(this));
				frame.setTitle("Auftragsexport mit Loeschen ...");
			} else {
				frame.setContentPane(new UINotDelete(this));
				frame.setTitle("Auftragsexport ohne loeschen ...");
			}
			
			
			
			frame.addWindowListener(new WindowAdapter() {

				// Shows code to Add Window Listener
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});		
			
			frame.setDefaultLookAndFeelDecorated(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			frame.setSize(200, 200);
			frame.pack();
			frame.setVisible(true);			
			
		} else {			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			//cal.add(Calendar.DAY_OF_MONTH, -1);
			Date date = cal.getTime();
			
			this.startProcess(date, date);
		}
	}	
	
	
	private void startProcess(final Date beginDate, final Date endDate) {
		
		this.importDataGenerator = new ImportDataGenerator();
		
		try {
			this.importDataGenerator.startProcess(this.simulation, beginDate, endDate, 
					this.mySQLComputerName, this.mySQLPortNumber, this.mySQLDatabaseName, this.mySQLUserName, this.mySQLPassword, this.emailTO);
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
	
	public void importLanguages(final String mySQLComputerName, final String mySQLPortNumber, 
			final String mySQLDatabaseName, final String mySQLUserName, final String mySQLPassword) {
		
		ImportLanguageGenerator importLanguageGenerator = new ImportLanguageGenerator();
		
		try {
			importLanguageGenerator.startProcess(mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword);
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
	
	
	public void actionPerformed(ActionEvent evt) {
	    
		Object src = evt.getSource();
		
		//final Date beginDate = (Date) this.spinner.getValue();
		//final Date endDate = (Date) this.spinner2.getValue();
		final Date beginDate = new Date();
		final Date endDate = new Date();
		
		startProcess(beginDate, endDate);
	}
	
}
	
	

	
