package Import;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import Connection.MySQLConnectorMYSQL;
import ImportData.ProductLanguage;

import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import fit.informixconnector.InformixConnector;


public class ImportLanguageGenerator {

	private static final Logger logger = Logger.getLogger(ImportLanguageGenerator.class.getName()); 
	Handler fh = null;
	
	public void startProcess(final String mySQLComputerName, final String mySQLPortNumber, 
			final String mySQLDatabaseName, final String mySQLUserName, final String mySQLPassword) throws SQLException {
		
		try {
			Date now = new Date();
			SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH.mm.ss");
			String nowString = simpleDateTimeFormat.format(now); 
			fh = new FileHandler("d:/User/fit/bin/ImportOrdersLog/ImportLanguages " + nowString + ".log");
			logger.addHandler(fh);
			fh.setFormatter(new SimpleFormatter());
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		MySQLConnectorMYSQL mySQLConnector = new MySQLConnectorMYSQL();
		mySQLConnector.createConnection(mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword, true);
		
		logger.info("MySQL Connection created.");
		
		
		ArrayList<ProductLanguage> languages = new ArrayList<ProductLanguage>();
		try {			
			languages =  this.getLanguage(mySQLConnector);
		} catch (SQLException e) {
			e.printStackTrace();
			mySQLConnector.closeConnection();
			logger.warning("Error while reading data from MySQL." + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			mySQLConnector.closeConnection();
			logger.warning("Error while reading data from MySQL." + e.getMessage());
		}		
		
		
		if ( languages.isEmpty() ) {
			
			logger.warning("No languanges for Import available.");
			
		} else {			
			
			InformixConnector informixConnector = new InformixConnector();
			informixConnector.createConnection();
			
			try {
				
				this.insertLanguagesToInformix(informixConnector, languages);
				logger.info(languages.size() + " Orders are inserted to Informix Database");			
				
			} catch (Exception e) {
				e.printStackTrace();
				informixConnector.closeConnection();
				logger.warning("Error while executing INSERT to Informix database." + e.getMessage());
			}			
			
			informixConnector.closeConnection();
			
			System.out.println("Sprachen aus Shop erfolgreich importiert");
		}		
		mySQLConnector.closeConnection();				
		System.exit(0);
	}
	
	
	private ArrayList<ProductLanguage> getLanguage(final MySQLConnectorMYSQL mySQLConnector) throws SQLException, IOException {
		
		final ArrayList<ProductLanguage> productLanguages = new ArrayList<ProductLanguage>();
		
		
		//TODO
		final String stmt = "SELECT o.`products_description`" +
							", o.`products_short_description`" +
							", p.`products_model`" +
							", o.`language_code`" +
							", o.`products_name`" +
							" FROM " +
							"xt_products_description as o, xt_products p" +
							" WHERE p.products_id = o.products_id" +
							" AND " +
							"o.products_name != ''" +
							" AND " +
							"p.products_model != ''" +
							" AND " +
							"p.products_model != '-1'" +
							" AND " +
							"(o.`language_code` = 'en' " +
							" OR " +
							"o.`language_code` = 'ru');";
		
		final ResultSet ordersResultSet = mySQLConnector.executeStatement(stmt);
		
		logger.info("Execute Orders statement: " + stmt);
		
		while(ordersResultSet.next()) {
			
			String product_model = ordersResultSet.getString("products_model");
			if ( product_model != null ) {
				product_model = product_model.trim();
				final int product_id = Integer.valueOf(product_model).intValue();
				
				final String product_description = ordersResultSet.getString("products_description");
				final String product_name = ordersResultSet.getString("products_name");
				final String language_code = ordersResultSet.getString("language_code");
				
				final ProductLanguage productLanguage = new ProductLanguage(product_id, product_name, product_description, language_code);
				productLanguages.add(productLanguage);
				
			} else {
				
				continue;
			}			
			
		}
		
		return productLanguages;
	}
	
	
	
	
	
	private void insertLanguagesToInformix(final InformixConnector informixConnector, final ArrayList<ProductLanguage> productLanguages) throws Exception {
		
		
		Iterator<ProductLanguage> it_productLanguages = productLanguages.iterator();
		
		informixConnector.executeStatement("delete from asprache;");
		
		while (it_productLanguages.hasNext()) {
			ProductLanguage productLanguage = (ProductLanguage) it_productLanguages
					.next();
			
			short sprache = 0;
			
			if ( productLanguage.getLanguage_code().equals("en") ) {
				sprache = 16;
			} else if ( productLanguage.getLanguage_code().equals("ru") ) {
				sprache = 17;
			}
				
			String insertstmt = 
					"INSERT INTO asprache" +
					"(mdn," +
					"a," +
					"sprache," +
					"a_bz1," +
					"a_bz2," +
					"produkt_info" +
					")	VALUES (" +
					1 + "," +
					productLanguage.getProducts_id() + "," +
					sprache + ",'" +
					productLanguage.getProducts_name().getBytes().toString() + "','" +
					"','" +
					productLanguage.getProducts_description().getBytes().toString() + "'" +
					"); ";
			
					
			informixConnector.executeStatement(insertstmt);			
		}		
	}	
}
