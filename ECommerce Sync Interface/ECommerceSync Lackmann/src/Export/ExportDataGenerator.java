package Export;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.JOptionPane;

import fit.informixconnector.InformixConnector;

import Connection.MySQLConnectorMYSQL;
import Converter.PtabReader;
import DataEnumerations.TaxClass;
import ExportData.Bestellvorschlag;
import ExportData.Category;
import ExportData.Category2;
import ExportData.CategoryDescription;
import ExportData.Customer;
import ExportData.CustomerAddress;
import ExportData.Product;
import ExportData.ProductDescription;
import ExportData.ProductImage;
import ExportData.ProductPrice;
import ExportData.ProductPriceGroup;
import ExportData.ProductPriceGroups;
import ExportData.ProductSpecialPrice;


public class ExportDataGenerator {	
	
	private ArrayList<Product> products = null;
	private String[] permissions = new String[] {"group_permission_98", "group_permission_99"};
	HashMap<Integer, int[]> customerProductsRelation = null;
	public int productOptionsCounter = 1;
	private ProductPriceGroups productPriceGroups;
	private ArrayList<ProductSpecialPrice> productSpecialPrices = null;
	private ArrayList<Bestellvorschlag> bestellVorschlaege = null;
	private HashMap<Integer, Integer> categoriesRelation = null;
	private HashMap<Integer, Double> pfandArtikel = null;
	private ArrayList<Integer> specialProducts = null;
	private ArrayList<ProductImage> productImages = null;
	private static final Logger logger = Logger.getLogger(ExportDataGenerator.class.getName());
	Handler fh = null;
	
	public boolean startProcess(final boolean withDialog, final int transferType, final String mySQLComputerName, 
			final String mySQLPortNumber, final String mySQLDatabaseName, final String mySQLUserName, final String mySQLPassword,
			final InformixConnector informixConnector) {		
		
		this.categoriesRelation = new HashMap<Integer, Integer>();
		this.pfandArtikel = new HashMap<Integer, Double>();
		
		try {
			Date now = new Date();
			SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH.mm.ss");
			String nowString = simpleDateTimeFormat.format(now); 
			fh = new FileHandler("d:/User/fit/bin/ExportOrdersLog/Epxort" + transferType + "_" + nowString + ".log");
			logger.addHandler(fh);
			fh.setFormatter(new SimpleFormatter());
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		logger.info("Informix Connection created");
		MySQLConnectorMYSQL mySQLConnector = new MySQLConnectorMYSQL();
		mySQLConnector.createConnection(mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword, true);
		logger.info("MySQL Connection created");
		
		boolean transferTypeReturn = false;
		
		switch (transferType) {
		case 1:
			transferTypeReturn = this.executeTransferType1(informixConnector, withDialog, mySQLConnector);
			break;
		case 2:
			transferTypeReturn = this.executeTransferType2(informixConnector, withDialog, mySQLConnector);
			break;
		case 3:
			transferTypeReturn = this.executeTransferType3(informixConnector, withDialog, mySQLConnector);
			break;
		}
		
		return transferTypeReturn;		
	}
	
	
	
	
	private boolean executeTransferType1(final InformixConnector informixConnector, final boolean withDialog, final MySQLConnectorMYSQL mySQLConnector) {
		
		ArrayList<Customer> customers = null;
		ArrayList<Category> categories = null;
		HashMap<String, Double> stockInformations = null;
		this.productImages = new ArrayList<ProductImage>();
		this.productPriceGroups = new ProductPriceGroups();
		this.productSpecialPrices = new ArrayList<ProductSpecialPrice>();
		this.bestellVorschlaege = new ArrayList<Bestellvorschlag>();
		this.specialProducts = new ArrayList<Integer>();
		
		logger.info("Execute Transfer Type 1 - Export ALL");
		
		try {
			customers = this.getCustomers(informixConnector);
			logger.info("Execute Transfer Type 1 - getCustomers successful");
			final ArrayList<CategoryDescription> categoryDescriptionsHWG = this.getAdditionalCategorieDescription();
			final ArrayList<CategoryDescription> categoryDescriptionsWG = this.getAdditionalCategorie2Description();
			categories = this.getCategories(informixConnector, categoryDescriptionsHWG, categoryDescriptionsWG);
			logger.info("Execute Transfer Type 1 - getCategories successful");
			this.getPriceGroups(informixConnector);
			logger.info("Execute Transfer Type 1 - getPriceGroups successful");
			this.getSpecialPrices(informixConnector);
			logger.info("Execute Transfer Type 1 - getSpecialPriceGroups successful");
			this.getPfandProducts(informixConnector);
			logger.info("Execute Transfer Type 1 - getPfandProducts successful");
			//this.getOrderExample(informixConnector);
			//logger.info("Execute Transfer Type 1 - getOrderExample successful");
			this.products = this.getProducts(informixConnector);
			logger.info("Execute Transfer Type 1 - getProducts successful");
			this.products = this.getAdditionalProductsDescription(informixConnector, this.products);
			logger.info("Execute Transfer Type 1 - getAdditionalDescriptions successful");
			this.products = this.getAdditionalShopCategories(informixConnector, this.products);
			logger.info("Execute Transfer Type 1 - getAdditionalCategories successful");			
			stockInformations = this.getStockInformation(informixConnector);
			logger.info("Execute Transfer Type 1 - getStockInformations successful");
		} catch (SQLException e) {
			e.printStackTrace();
			if ( withDialog ) {
				JOptionPane.showMessageDialog(null, "Synchronisation mit Fehler beendet, bitte Verbindung zur Datenbank pr�fen!", "Fehler", JOptionPane.ERROR_MESSAGE);	
			}
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			if ( withDialog ) {
				JOptionPane.showMessageDialog(null, "Synchronisation mit Fehler beendet, bitte Verbindung zur Datenbank pr�fen!", "Fehler", JOptionPane.ERROR_MESSAGE);	
			}
			return false;
		}		
		informixConnector.closeConnection();
		logger.info("Informix Connection closed.");
		
		this.insertCustomersToMySQL(customers, mySQLConnector);
		logger.info("Execute Transfer Type 1 - Customers successful insert to MySQL");
		this.insertCategoriesToMySQL(categories, mySQLConnector);
		logger.info("Execute Transfer Type 1 - Categories successful insert to MySQL");
		this.insertProductsToMySQL(products, mySQLConnector);
		logger.info("Execute Transfer Type 1 - Products successful insert to MySQL");
		this.updatetmpStockInformation(mySQLConnector, stockInformations);
		logger.info("Execute Transfer Type 1 - Stock successful updated to MySQL");
		this.insertProductPricesToMySQL(mySQLConnector);
		logger.info("Execute Transfer Type 1 - ProductPrices successful insert to MySQL");
		this.insertProductsSpecialPricesToMySQL(mySQLConnector);
		logger.info("Execute Transfer Type 1 - ProductSpecialPrices successful insert to MySQL");
		//this.insertOrderExamplesToMySQL(mySQLConnector);
		logger.info("Execute Transfer Type 1 - Product Order examples successful insert to MySQL");
		this.insertDataFromTMPInRealTables(mySQLConnector);
		logger.info("Execute Transfer Type 1 - TMP data insert to real tables in MySQL");		
		this.insertProductImages(mySQLConnector);
		logger.info("Execute Transfer Type 1 - ProductImages successful insert to MySQL");
		
		mySQLConnector.closeConnection();
		logger.info("MySQL Connection closed.");
		
		return true;
		
	}
	
	
	
	private boolean executeTransferType2(final InformixConnector informixConnector, final boolean withDialog, final MySQLConnectorMYSQL mySQLConnector) {
		
		ArrayList<Customer> customers = null;
		
		try {
			customers = this.getCustomers(informixConnector);
			logger.info("Execute Transfer Type 2 - getCustomers successful");
		} catch (SQLException e) {
			e.printStackTrace();
			if ( withDialog ) {
				JOptionPane.showMessageDialog(null, "Synchronisation mit Fehler beendet, bitte Verbindung zur Datenbank pr�fen!", "Fehler", JOptionPane.ERROR_MESSAGE);	
			}
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			if ( withDialog ) {
				JOptionPane.showMessageDialog(null, "Synchronisation mit Fehler beendet, bitte Verbindung zur Datenbank pr�fen!", "Fehler", JOptionPane.ERROR_MESSAGE);	
			}
			return false;
		}		
		informixConnector.closeConnection();		
		logger.info("Informix Connection closed.");
		
		this.insertCustomersToMySQL(customers, mySQLConnector);
		logger.info("Execute Transfer Type 2 - Customers successful insert to MySQL");		
		this.insertDataFromTMPInRealTables2(mySQLConnector);
		logger.info("Execute Transfer Type 2 - TMP data insert to real tables in MySQL");
		mySQLConnector.closeConnection();
		logger.info("MySQL Connection closed.");
		
		return true;
		
	}
	
	
	
	private boolean executeTransferType3(final InformixConnector informixConnector, final boolean withDialog, final MySQLConnectorMYSQL mySQLConnector) {
		
		HashMap<String, Double> stockInformations = null;
		
		try {
			stockInformations = this.getStockInformation(informixConnector);
			logger.info("Execute Transfer Type 3 - getStockInformation successful");
		} catch (SQLException e) {
			e.printStackTrace();
			if ( withDialog ) {
				JOptionPane.showMessageDialog(null, "Synchronisation mit Fehler beendet, bitte Verbindung zur Datenbank pr�fen!", "Fehler", JOptionPane.ERROR_MESSAGE);	
			}
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			if ( withDialog ) {
				JOptionPane.showMessageDialog(null, "Synchronisation mit Fehler beendet, bitte Verbindung zur Datenbank pr�fen!", "Fehler", JOptionPane.ERROR_MESSAGE);	
			}
			return false;
		}		
		informixConnector.closeConnection();		
		logger.info("Informix Connection closed.");
		
		this.updateStockInformation(mySQLConnector, stockInformations);
		logger.info("Execute Transfer Type 3 - StockInformation successful updated in MySQL");
		
		mySQLConnector.closeConnection();
		logger.info("MySQL Connection closed.");
		
		return true;
		
	}
	
	
	
	private void getPriceGroups(final InformixConnector informixConnector) throws SQLException {
		
		final String stmt =
				"select a_bas.a, pr_gr_stuf, vk_pr_eu, ld_pr_eu" +
				" from ipr, a_bas" +
				" where a_bas.a = ipr.a	and a_bas.shop_kz = 'J' and kun_pr = 0 and pr_gr_stuf != 0" +
				" order by pr_gr_stuf";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);		
		boolean firstCycle = true;
		ProductPrice productPrice = null;
		int last_pr_gr_stuf = 0;
		
		while(resultSet.next()) {
			
			final int a = resultSet.getInt("a");
			final int pr_gr_stuf = resultSet.getInt("pr_gr_stuf");			
			final double vk_pr_eu = resultSet.getDouble("vk_pr_eu");
			final double ld_pr_eu = resultSet.getDouble("ld_pr_eu");			
			
			if ( last_pr_gr_stuf != pr_gr_stuf || firstCycle ) {
				final  ProductPriceGroup productPriceGroup = new ProductPriceGroup(pr_gr_stuf);
				this.productPriceGroups.addProductPriceGroup(productPriceGroup);				
			}
			
			productPrice = new ProductPrice(a, pr_gr_stuf, vk_pr_eu, ld_pr_eu);
			this.productPriceGroups.addProductPricesToPriceGroup(pr_gr_stuf, productPrice);
			
			firstCycle = false;
			last_pr_gr_stuf = pr_gr_stuf;
		}
	}


	private void getSpecialPrices(final InformixConnector informixConnector) throws SQLException {
		
		ProductSpecialPrice productSpecialPrice = null;
		
		final String stmt =
				"select akiprgrstp.a, pr_gr_stuf, aki_pr_eu, aki_von, aki_bis" +
				" from akiprgrstk, akiprgrstp, a_bas" +
				" where today between aki_von and aki_bis" +
				" and akiprgrstk.aki_nr = akiprgrstp.aki_nr " +
				" and a_bas.a = akiprgrstp.a and a_bas.shop_kz = 'J'" +
				" order by akiprgrstp.a";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		
		while(resultSet.next()) {
			
			final int a = resultSet.getInt("a");
			final int pr_gr_stuf = resultSet.getInt("pr_gr_stuf");			
			final double aki_pr_eu = resultSet.getDouble("aki_pr_eu");
			final Date aki_von = resultSet.getDate("aki_von");
			final Date aki_bis = resultSet.getDate("aki_bis");
			
			productSpecialPrice = new ProductSpecialPrice(a, pr_gr_stuf, aki_pr_eu, aki_von, aki_bis);
			this.productSpecialPrices.add(productSpecialPrice);
			
			if ( !specialProducts.contains(a) ) {
				specialProducts.add(a);
			}
		}
	}
	

	private void getPfandProducts(final InformixConnector informixConnector) throws SQLException {
		
		final String stmt =
				"select a_bas.a, ipr.vk_pr_eu " +
				"from a_bas, ipr " +
				"where a_bas.a = ipr.a " +
				"and a_bas.a_typ = 4 " +
				"and ipr.kun_pr = 0 " +
				"and ipr.kun = 0 " +
				"and ipr.pr_gr_stuf = 1;";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		
		while(resultSet.next()) {
			
			final int a = resultSet.getInt("a");			
			final double vk_pr_eu = resultSet.getDouble("vk_pr_eu");
			
			if ( !this.pfandArtikel.containsKey(a) ) {
				this.pfandArtikel.put(a, vk_pr_eu);	
			}			
		}
	}
	
	
	private void getOrderExample(final InformixConnector informixConnector) throws SQLException {
		
		Bestellvorschlag bestellvorschlag = null;
		
		final String stmt =
				"SELECT a, kun, me" +
				" from best_vorschlag" +
				" order by a";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		
		while(resultSet.next()) {
			
			final int a = resultSet.getInt("a");
			final int kun = resultSet.getInt("kun");			
			final double me = resultSet.getDouble("me");
			
			bestellvorschlag = new Bestellvorschlag(me, kun, a);
			this.bestellVorschlaege.add(bestellvorschlag);
		}
	}
	
	
	
	private ArrayList<Customer> getCustomers(final InformixConnector informixConnector) throws SQLException, IOException {
		
		final ArrayList<Customer> customers = new ArrayList<Customer>();
		
		final String stmt =
				"select kun, kun_krz1, kun_bran, shop_kz, ust_id, shop_passw, adr_krz, adr_nam1, adr_nam2, anr, fax, geb_dat, land, ort1, plz, staat, str, tel, email, adr_nam3, mobil, sprache," +
				"adr_nam3, pr_stu, shop_history, adr" +
				" from " +
				"kun, adr" +
				" where " +
				"kun.adr1 = adr.adr" +
				" and " +
				"kun.shop_kz == 'J'" +
				";";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);				
		
		while(resultSet.next()) {
			
			try {				
				final int kun = resultSet.getInt("kun");
				final int adr = resultSet.getInt("adr");
				String shop_passw = resultSet.getString("shop_passw");
				if (shop_passw != null) {
					shop_passw.trim();
				} else {
					shop_passw = "";
				}
				
				String adr_nam1 = resultSet.getString("adr_nam1");
				if (adr_nam1 != null) {
					adr_nam1 = adr_nam1.trim();
					adr_nam1 = this.replaceSpecialCharacter(adr_nam1);					
				}
				String adr_nam2 = resultSet.getString("adr_nam2");
				if (adr_nam2 != null) {
					adr_nam2 = adr_nam2.trim();
					adr_nam2 = this.replaceSpecialCharacter(adr_nam2);
				}
				String adr_nam3 = resultSet.getString("adr_nam3");
				if (adr_nam3 != null) {
					adr_nam3 = adr_nam3.trim();
					adr_nam3 = this.replaceSpecialCharacter(adr_nam3);
				}
				final int anr = resultSet.getInt("anr");
				final int pr_stu = resultSet.getInt("pr_stu");				
				
				String fax = resultSet.getString("fax");
				if (fax != null) {
					fax = fax.trim();
				}
				final Date geb_dat = resultSet.getDate("geb_dat");
				String ort1 = resultSet.getString("ort1");
				if (ort1 != null) {
					ort1 = ort1.trim();
					ort1 = this.replaceSpecialCharacter(ort1);
				}
				String plz = resultSet.getString("plz");
				if (plz != null) {
					plz = plz.trim();
				}
				String str = resultSet.getString("str");
				if (str != null) {
					str = str.trim();
					str = this.replaceSpecialCharacter(str);
				}
				String ust_id = resultSet.getString("ust_id");
				if (ust_id != null) {
					ust_id = ust_id.trim();
				}
				String tel = resultSet.getString("tel");
				if (tel != null) {
					tel = tel.trim();
				}
				String email = resultSet.getString("email");
				if (email != null) {
					email = email.trim();
				}
				final String sprache = String.valueOf(resultSet.getInt("sprache"));
				String languageCode = PtabReader.getPtabValue(informixConnector, "sprache", sprache);
				if (languageCode != null) {
					languageCode = languageCode.trim();
				}
				final String staat = String.valueOf(resultSet.getInt("staat"));
				String countryCode = PtabReader.getPtabValue(informixConnector, "staat", staat);
				if (countryCode != null) {
					countryCode = countryCode.trim();
				}
				String shop_orders_history = resultSet.getString("shop_history");
				if (shop_orders_history != null) {
					shop_orders_history = shop_orders_history.trim();
				}
				final String liefertag = "DO";				
				
				final Customer customer = new Customer(kun, pr_stu, adr_nam1, languageCode, liefertag, shop_orders_history);
				customer.setEmailAdress(email);
				if (shop_passw != null) {
					shop_passw = shop_passw.trim();
				}
				customer.setPassword(shop_passw);				
				customer.setUStId(ust_id);				
				
				String customers_gender = "";
				if ( anr == 1) {
					customers_gender = "m";
				} else if ( anr == 3 ){
					customers_gender = "f";
				} else {
					customers_gender = "m";
				}
				
				CustomerAddress customerAddress = new CustomerAddress(kun, adr_nam1, "", "", str, plz, ort1, customers_gender, 
						countryCode, tel, fax, geb_dat, adr_nam2, adr_nam3, adr);
				
				customer.addCustomerAddress(customerAddress);				
				customers.add(customer);
				
			} catch (Exception e){				
				e.printStackTrace();
				continue;				
			}
		}	
		return customers;	
	}
	
	
	
	private void insertCustomersToMySQL(final ArrayList<Customer> customers, final MySQLConnectorMYSQL mySQLConnector) {
		
		mySQLConnector.execute("DROP TABLE IF EXISTS tmp_customers");
		mySQLConnector.execute("CREATE TABLE tmp_customers LIKE xt_customers");
		mySQLConnector.execute("DROP TABLE IF EXISTS tmp_address_book");
		mySQLConnector.execute("CREATE TABLE tmp_address_book LIKE xt_customers_addresses;");
		
		
		final Iterator<Customer> it_customers = customers.iterator();
		
		boolean firstCustomerRun = true;
		boolean firstAddressRun = true;
		StringBuilder sb_customerStmt = new StringBuilder();
		StringBuilder sb_customerAddressStmt = new StringBuilder(); 
		
		while (it_customers.hasNext()) {			
			Customer customer = (Customer) it_customers.next();
			if ( firstCustomerRun ) {
				String customerStmt = customer.getInsertStatement();
				sb_customerStmt.append(customerStmt);
				firstCustomerRun = false;
				
				Iterator<CustomerAddress> it_customerAddress = customer.getCustomerAddresses();
				while (it_customerAddress.hasNext()) {
					CustomerAddress customerAddress = (CustomerAddress) it_customerAddress
							.next();
					
					if ( firstAddressRun ) {
						String customerAddressStmt = customerAddress.getInsertStatement();
						sb_customerAddressStmt.append(customerAddressStmt);
						firstAddressRun = false;
					} else {
						String customerAddressStmt = customerAddress.getValuesStatement();
						sb_customerAddressStmt.append(customerAddressStmt);						
					}					
				}				
				
			} else {
				String customerStmt = customer.getValuesStatement();
				sb_customerStmt.append(customerStmt);
				
				Iterator<CustomerAddress> it_customerAddress = customer.getCustomerAddresses();
				while (it_customerAddress.hasNext()) {
					CustomerAddress customerAddress = (CustomerAddress) it_customerAddress
							.next();
					
					if ( firstAddressRun ) {
						String customerAddressStmt = customerAddress.getInsertStatement();
						sb_customerAddressStmt.append(customerAddressStmt);
						firstAddressRun = false;
					} else {
						String customerAddressStmt = customerAddress.getValuesStatement();
						sb_customerAddressStmt.append(customerAddressStmt);						
					}					
				}
			}
		}
		
		String finalStmtcustomers = sb_customerStmt.toString() + ";";		
		String finalStmtcustomersAddresses = sb_customerAddressStmt.toString() + ";";
		
		mySQLConnector.executeUpdate(finalStmtcustomers);
		mySQLConnector.executeUpdate(finalStmtcustomersAddresses);
				
	}
	
	
	
	private ArrayList<Category> getCategories(final InformixConnector informixConnector, 
			final ArrayList<CategoryDescription> categoryDescriptionsHWG, final ArrayList<CategoryDescription> categoryDescriptionsWG) throws SQLException {
		
		final ArrayList<Category> categories = new ArrayList<Category>();
		
		final String stmt = 
					"select hwg.hwg, hwg_bz1, hwg_bz2, wg, wg_bz1, wg_bz2, shop_sort" +
					" from " +
					"hwg, wg" +
					" where " +
					"hwg.hwg = wg.hwg" +
					" and " +
					"wg.shop_kz = 'J'" +
					" order by hwg, wg;";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		
		int last_hwg = 0;
		boolean firstCycle = true;
		Category hwg_categorie = null;
		
		while(resultSet.next()) {

			final int hwg = resultSet.getInt("hwg");
			final int wg = resultSet.getInt("wg");
			final short shop_sort = resultSet.getShort("shop_sort");			
			String hwg_bz1 = resultSet.getString("hwg_bz1").trim();
			final String hwg_bz2 = resultSet.getString("hwg_bz2").trim();
			hwg_bz1 = hwg_bz1 + " " + hwg_bz2;
			String wg_bz1 = resultSet.getString("wg_bz1").trim();
			final String wg_bz2 = resultSet.getString("wg_bz2").trim();
			wg_bz1 = wg_bz1 + " " + wg_bz2;
			
			if ( last_hwg != hwg || firstCycle ) {
				final String hwgImage = hwg + "hwg.png";
				hwg_categorie = new Category(hwg, 0, hwg_bz1, hwg_bz1, shop_sort, hwgImage, "de");
				
				CategoryDescription enCategoryDescription = this.getCategoryDescription(categoryDescriptionsHWG, hwg, "en");
				if ( enCategoryDescription != null ) {
					hwg_categorie.addCategoryDescription(enCategoryDescription);	
				} else {
					enCategoryDescription = new CategoryDescription(hwg, hwg_bz1, hwg_bz1, "en");
					hwg_categorie.addCategoryDescription(enCategoryDescription);	
				}
				
				CategoryDescription ruCategoryDescription = this.getCategoryDescription(categoryDescriptionsHWG, hwg, "ru");
				if ( ruCategoryDescription != null ) {
					hwg_categorie.addCategoryDescription(ruCategoryDescription);	
				} else {
					ruCategoryDescription = new CategoryDescription(hwg, hwg_bz1, hwg_bz1, "ru");
					hwg_categorie.addCategoryDescription(ruCategoryDescription);	
				}

				categories.add(hwg_categorie);
			}			
			
			final String wgImage = wg + "wg.png";
			final Category2 wg_categorie = new Category2(wg, hwg, wg_bz1, wg_bz1, 0, wgImage, "de");
			
			CategoryDescription enCategoryDescription = this.getCategoryDescription(categoryDescriptionsWG, wg, "en");
			if ( enCategoryDescription != null ) {
				wg_categorie.addCategoryDescription(enCategoryDescription);	
			} else {
				enCategoryDescription = new CategoryDescription(wg, wg_bz1, wg_bz1, "en");
				wg_categorie.addCategoryDescription(enCategoryDescription);	
			}
			
			CategoryDescription ruCategoryDescription = this.getCategoryDescription(categoryDescriptionsWG, wg, "ru");
			if ( ruCategoryDescription != null ) {
				wg_categorie.addCategoryDescription(ruCategoryDescription);	
			} else {
				ruCategoryDescription = new CategoryDescription(wg, wg_bz1, wg_bz1, "ru");
				wg_categorie.addCategoryDescription(ruCategoryDescription);	
			}

			hwg_categorie.addCategoryToCategory(wg_categorie);			
			
			firstCycle = false;
			last_hwg = hwg;			
		}		
		return categories;
	}	
	
	
	
	private void insertCategoriesToMySQL(ArrayList<Category> categories, final MySQLConnectorMYSQL mySQLConnector) {
		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_categories_description");
		mySQLConnector.executeUpdate("CREATE TABLE tmp_categories_description LIKE xt_categories_description;");
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_categories_permission");
		mySQLConnector.executeUpdate("CREATE TABLE tmp_categories_permission LIKE xt_categories_permission;");
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_categories");
		mySQLConnector.executeUpdate("CREATE TABLE tmp_categories LIKE xt_categories;");
		
		final Iterator<Category> it_categories = categories.iterator();
		
		while (it_categories.hasNext()) {			
			Category hwg_category = (Category) it_categories.next();			
			final int hwg_autoCategoryId = mySQLConnector.execute(hwg_category.getInsertStatement());
			this.insertCategoryDescriptions(hwg_autoCategoryId, mySQLConnector, hwg_category.getCategorieDescriptions());
			
			final Iterator<Category2> wgCategories = hwg_category.getWGCategories();
			while (wgCategories.hasNext()) {
				Category2 wg_category = (Category2) wgCategories.next();
				final int wg_autoCategoryId = mySQLConnector.execute(wg_category.getInsertStatement(hwg_autoCategoryId));
				this.insertCategoryDescriptions(wg_autoCategoryId, mySQLConnector, wg_category.getCategorieDescriptions());
				this.categoriesRelation.put(wg_category.getCategorieId(), wg_autoCategoryId);
			}		
		}
	}
	

	private void insertProductsSpecialPricesToMySQL(final MySQLConnectorMYSQL mySQLConnector) {
		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_xt_products_price_special");
		mySQLConnector.executeUpdate("CREATE TABLE tmp_xt_products_price_special LIKE xt_products_price_special;");
		
		Iterator<ProductSpecialPrice> proIterator = this.productSpecialPrices.iterator();
		
		while (proIterator.hasNext()) {			
			ProductSpecialPrice proprice = (ProductSpecialPrice) proIterator.next();			
			String stmt = proprice.getInsertStatement();
			mySQLConnector.executeUpdate(stmt);	
		}
	}

	
	private void insertOrderExamplesToMySQL(final MySQLConnectorMYSQL mySQLConnector) {
		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_xt_plg_hi_suggestion");
		mySQLConnector.executeUpdate("CREATE TABLE tmp_xt_plg_hi_suggestion LIKE xt_plg_hi_suggestion;");
		
		Iterator<Bestellvorschlag> it_bestellVorschlag = this.bestellVorschlaege.iterator();
		
		while (it_bestellVorschlag.hasNext()) {			
			Bestellvorschlag bestellVorschlag = (Bestellvorschlag) it_bestellVorschlag.next();			
			String stmt = bestellVorschlag.getInsertStatement();
			mySQLConnector.executeUpdate(stmt);	
		}
	}
	
	
	private void insertCategoryDescriptions(final int autoId, final MySQLConnectorMYSQL mySQLConnector, final Iterator<CategoryDescription> it_categoriedesc ) {
		
		while (it_categoriedesc.hasNext()) {
			
			CategoryDescription categorieDescription = (CategoryDescription) it_categoriedesc
					.next();
			mySQLConnector.executeUpdate(categorieDescription.getInsertStatement(autoId));				
		}
		
		for (int i = 0; i < permissions.length; i++) {
			
			String customerPermissionStatement = "INSERT INTO tmp_categories_permission (pid,permission,pgroup) VALUES('" + autoId + "','1','" + permissions[i] + "')";
			mySQLConnector.executeUpdate(customerPermissionStatement);
		}
	}
	
	
	
	private ArrayList<Product> getProducts(final InformixConnector informixConnector) throws SQLException, IOException {
		
		final ArrayList<Product> products = new ArrayList<Product>();
		
		final String stmt = 
					"select a_bas.a, a_bas.a_bz1 as a_bz1, a_bas.a_bz2 as a_bz2, ag.wg as wg, " +
					"a_bas.bild as bild, a_bas.produkt_info as produkt_info, a_bas.mwst as mwst" +
					", atexte.txt, a_eig.me_einh_ek, " +
					"a_hndw.me_einh_kun, a_bas.a_gew, a_eig.inh as a_eig_inh, " +
					"a_hndw.inh as a_hdnw_inh, a_hndw.a_pfa as a_pfa, ipr.ld_pr_eu, " +
					"min_bestellmenge, staffelung, a_ean.ean, " +
					"inh_karton, packung_karton, pp_a_bz1, pp_a_bz2," +
					"shop_aktion, shop_tv, shop_neu_bis, shop_agew, meld_bestand, a_bas.me_einh as meeinh, a_bas.akv as akv, a_bas.bearb as bearb" +
					" from " +
					"a_bas, outer atexte, ag, wg, outer a_eig, outer a_hndw, outer a_bas_erw, outer a_lgr, " +
					"outer ipr, outer a_ean" +
					" where " +
					"a_bas.ag = ag.ag" +
					" and " +
					"ag.wg = wg.wg" +
					" and " +
					"a_bas.a = a_ean.a" +
					" and " +
					"a_bas.shop_kz = 'J' " +
					" and " +
					"a_bas.a = atexte.a" +
					" and " +
					"a_bas.a = a_eig.a" +
					" and " +
					"a_bas.a = a_hndw.a" +
					" and " +
					"a_bas.a = a_bas_erw.a" +
					" and " +
					"a_bas.a = ipr.a" +
					" and " +
					"a_bas.mdn = ipr.mdn" +
					" and " +
					"a_bas.mdn = a_eig.mdn" +
					" and " +
					"a_bas.mdn = a_hndw.mdn" +
					" and " +
					"a_bas.mdn = a_lgr.mdn" +
					" and " +
					"a_bas.a = a_lgr.a" +
					" and " +
					"a_lgr.lgr = 2" +
					" and " +
					"ipr.pr_gr_stuf = 0" +
					" and " +
					"ipr.kun = 0" +
					" and " +
					"a_bas.delstatus = 0" +
					" order by a_bas.a;";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		
		while(resultSet.next()) {

			double pfandVK = 0; 
			final int a = resultSet.getInt("a");
			int a_pfa = resultSet.getInt("a_pfa");
			if ( a_pfa > 0 ) {
				pfandVK = this.pfandArtikel.get(a_pfa);								
			}			
			
			String ean = resultSet.getString("ean");
			if ( ean == null ) {
				ean = "";
			} else {
				ean = ean.trim();
				ean = this.calculateEANPruefziffer(ean);
			}
			
			final Date akv = resultSet.getDate("akv");
			final Date bearb = resultSet.getDate("bearb");
			
			int tv = 0;
			int aktion = 0;
			int neuesProdukt = 0;
			
			final Date now = new Date();			
			final Date shop_neu_bis = resultSet.getDate("shop_neu_bis");
			if ( shop_neu_bis != null ) {
				if ( now.before(shop_neu_bis) ) {
					neuesProdukt = 1;	
				}				
			}
			
			
			String shop_aktion = resultSet.getString("shop_aktion");
			if ( shop_aktion != null ) {
				shop_aktion = shop_aktion.trim();
				if ( shop_aktion.equals("J")) {
					aktion = 1;
				}				
			}
			
			String shop_tv = resultSet.getString("shop_tv");
			if ( shop_tv != null ) {
				shop_tv = shop_tv.trim();
				if ( shop_tv.equals("J")) {
					tv = 1;
				}				
			}			
			
			double min_bestellmenge = resultSet.getDouble("min_bestellmenge");
			String sMin_bestellmenge = "1.0";
			if ( min_bestellmenge > 0 ) {
				sMin_bestellmenge = String.valueOf(min_bestellmenge).replace("," , ".");				
			}
			double staffelung = resultSet.getDouble("staffelung");
			String sStaffelung = "1.0";
			if ( staffelung > 0 ) {
				sStaffelung = String.valueOf(staffelung).replace(",", ".");				
			}
			final String products_packing_units = sMin_bestellmenge + ";" + sStaffelung; 
			
			String a_bz1 = resultSet.getString("a_bz1");
			if ( a_bz1 != null ) {
				a_bz1 = a_bz1.trim();
				a_bz1 = this.replaceSpecialCharacter(a_bz1);
			} else {
				a_bz1 = "";
			}
			String a_bz2 = resultSet.getString("a_bz2");
			if ( a_bz2 != null ) {
				a_bz2 = a_bz2.trim();
				a_bz2 = this.replaceSpecialCharacter(a_bz2);	
			} else {
				a_bz2 = "";
			}
			
			String pp_a_bz1 = resultSet.getString("pp_a_bz1");
			if ( pp_a_bz1 != null ) {
				pp_a_bz1 = pp_a_bz1.trim();
				pp_a_bz1 = this.replaceSpecialCharacter(pp_a_bz1);	
			} else {
				pp_a_bz1 = "";
			}
			
			String pp_a_bz2 = resultSet.getString("pp_a_bz2");
			if ( pp_a_bz2 != null ) {
				pp_a_bz2 = pp_a_bz2.trim();
				pp_a_bz2 = this.replaceSpecialCharacter(pp_a_bz2);	
			} else {
				pp_a_bz2 = "";
			}
			
			final String a_bz = a_bz1 + " " + a_bz2;
			final String pp_a_bz = pp_a_bz1 + " " + pp_a_bz2;
			final int wg = resultSet.getInt("wg");
			int me_einh = 0;
			
//			double inh = 0;
//			try {
//				me_einh = resultSet.getShort("me_einh_ek");
//				inh = resultSet.getDouble("a_eig_inh");
//				me_einh_ptitem = "me_einh_ek";
//			} catch (Exception e) {
//				me_einh = resultSet.getShort("me_einh_kun");
//				inh = resultSet.getDouble("a_hdnw_inh");
//				me_einh_ptitem = "me_einh_kun";
//			}
			
//			if ( me_einh == 0 ) {
//				me_einh = resultSet.getShort("meeinh");
//			}
			
			me_einh = resultSet.getShort("meeinh");
			String me_einh_ptitem = "me_einh";
			
			String mengenEinheit = PtabReader.getPtabValuePtBez(informixConnector, me_einh_ptitem, String.valueOf(me_einh));
			if (mengenEinheit != null) {
				mengenEinheit = mengenEinheit.trim();
			} else {
				mengenEinheit = "";
			}
			
			short mwst = resultSet.getShort("mwst");
			
			double a_gew = 0;
			a_gew = resultSet.getDouble("a_gew");
			
			int meldBestand = 10;
			double dMeldBestand = resultSet.getDouble("meld_bestand");
			meldBestand = Double.valueOf(dMeldBestand).intValue();
			
			
			double shop_a_gew = 0;
			shop_a_gew = resultSet.getDouble("shop_agew");
			
			short temp_inh_karton = resultSet.getShort("inh_karton");
			String inhaltProKarton = "";			
			if ( temp_inh_karton != 0 ) {
				inhaltProKarton = String.valueOf(temp_inh_karton);
			}
			
			short temp_packung_karton = resultSet.getShort("packung_karton");
			String packungProKarton = "";
			if ( temp_packung_karton != 0 ) {
				packungProKarton = String.valueOf(temp_packung_karton);
			}
			
			String bild = resultSet.getString("bild");
			if (bild != null) {
				bild = bild.trim();
				String[] bildParts = bild.split("\\\\");
				if ( bildParts.length > 0 ) {
					bild = bildParts[bildParts.length-1];
					if ( bild.equals("bilder") ) {
						bild = "";
					} else {
						final ProductImage productImage = new ProductImage(bild, "product");
						
						if ( !this.productImages.contains(bild) ) {
							this.productImages.add(productImage);	
						}	
					}
				}										
			} else {
				bild = "";
			}		
			
			double products_price = resultSet.getDouble("ld_pr_eu");
			String prefix_products_packung = "";
			
			if ( me_einh == 1 ) {				
				products_price = products_price * a_gew;
				
			}
			
			if ( me_einh == 2 ) {				
				prefix_products_packung = "~ ";				
			}
			
			String products_description = resultSet.getString("produkt_info");
			if ( products_description != null ) {
				products_description.trim();	
			} else {
				products_description = "";
			}
			
			final String languageCode = "de";
			final String products_order_unit = mengenEinheit;
			final String products_meat_type = "";
			
			final DecimalFormat df = new DecimalFormat("####0.000");
			final String products_packung = prefix_products_packung + df.format(shop_a_gew);			
			final double products_quantity = 0;
			final String products_ean = ean;		
			
			ArrayList<ProductDescription> productDescriptions = new ArrayList<ProductDescription>();
			
			//Default ProductDescriptions
			final ProductDescription defaultProductDescription = new ProductDescription(a, languageCode, a_bz, products_description, pp_a_bz, products_order_unit, products_meat_type, products_packung);
			productDescriptions.add(defaultProductDescription);
			final ProductDescription enProductDescription = new ProductDescription(a, "en", a_bz, products_description, pp_a_bz, products_order_unit, products_meat_type, products_packung);
			productDescriptions.add(enProductDescription);
			final ProductDescription ruProductDescription = new ProductDescription(a, "ru", a_bz, products_description, pp_a_bz, products_order_unit, products_meat_type, products_packung);
			productDescriptions.add(ruProductDescription);			
			
			ArrayList<Integer> priceGroups = this.productPriceGroups.getPriceGroupArray(a);
			
			TaxClass tax = TaxClass.LOWER7;
			if ( mwst == 1 ) {
				tax = TaxClass.STANDARD19;
			}
			
			int has_special = 0;
			if ( specialProducts.contains(a) ) {
				aktion = 1;
				has_special = 1;
			}
			
			final Product product = new Product(a, bild, products_price, a_gew, tax, true, 0, productDescriptions, wg, products_ean, products_quantity, 
					me_einh, a_gew, inhaltProKarton, products_packing_units, 
					packungProKarton, tv, aktion, neuesProdukt, priceGroups, meldBestand, akv, bearb, has_special, pfandVK);
			products.add(product);
		}
		return products;		
	}
	
	
	
	private ArrayList<Product> getAdditionalProductsDescription(final InformixConnector informixConnector, ArrayList<Product> products) throws SQLException, IOException {
		
		final String stmt = "select a.a as a " +
				"				, a.mdn as mdn " +
				"				, a.a_bz1 as asbz1 " +
				"				, a.a_bz2 as asbz2 " +
				"				, a.ppabz1 as ppabz1 " +
				"				, a.ppabz2 as ppabz2 " +
				"				, a.produkt_info as asprodukt_info " +
				"				, a.sprache as assprache " +
				"				from asprache a " +
				"				order by a.a;";
		
		
		MySQLConnectorMYSQL mySQLConnectorMYSQL = new MySQLConnectorMYSQL();
		mySQLConnectorMYSQL.createConnection("172.16.1.30", "3307", "xt_shop", "root", "Dlbm18x%", false);
		//mySQLConnectorMYSQL.createConnection("localhost", "3307", "xt_shop", "root", "Dlbm18x%", false);
		
		final ResultSet resultSet = mySQLConnectorMYSQL.executeStatement(stmt);
		
		while(resultSet.next()) {

			final int a = resultSet.getInt("a");
			final String a_bz1 = resultSet.getString("asbz1");
			final String a_bz2 = resultSet.getString("asbz2");
			final String ppabz1 = resultSet.getString("ppabz1");
			final String ppabz2 = resultSet.getString("ppabz2");
			final String products_description = resultSet.getString("asprodukt_info");
			final short spracheId = resultSet.getShort("assprache");
			
			final String a_bz = a_bz1.trim() + " " + a_bz2.trim();
			final String pp_a_bz = ppabz1.trim() + " " + ppabz2.trim();
			final String products_order_unit = "";
			final String products_meat_type = "";
			final String products_packung = "";
			
			ProductDescription productDescription = null;
			if ( spracheId == 16 ) {
				productDescription = new ProductDescription(a, "en", a_bz, products_description, pp_a_bz, products_order_unit, products_meat_type, products_packung);				
			} else if ( spracheId == 17 ) {
				productDescription = new ProductDescription(a, "ru", a_bz, products_description, pp_a_bz, products_order_unit, products_meat_type, products_packung);				
			} else {
				continue;
			}
			
			final Iterator<Product> it_products = products.iterator();
			while (it_products.hasNext()) {
				Product product = (Product) it_products.next();
				if ( product.getInternalProductsId() == a && a_bz1 != null ) {
					product.replaceProductDescription(productDescription);					
				}
			}
		}
		mySQLConnectorMYSQL.closeConnection();
		return products;		
	}
	
	
	private ArrayList<CategoryDescription> getAdditionalCategorieDescription() throws SQLException, IOException {
		
		final ArrayList<CategoryDescription> categoryDescriptions = new ArrayList<CategoryDescription>();
		MySQLConnectorMYSQL mySQLConnectorMYSQL = new MySQLConnectorMYSQL();
		//mySQLConnectorMYSQL.createConnection("localhost", "3307", "xt_shop", "root", "Dlbm18x%", false);
		mySQLConnectorMYSQL.createConnection("172.16.1.30", "3307", "xt_shop", "root", "Dlbm18x%", false);
		
		final String stmt_hwg = "select hwg " +
				"				, hwg_bz1 " +
				"				, hwg_bz2 " +
				"				, sprache " +
				"				from hwgsprache " +
				"				order by hwg;";
		
		
		final ResultSet resultSet = mySQLConnectorMYSQL.executeStatement(stmt_hwg);
		
		while(resultSet.next()) {
			
			int hwg_aktuell = resultSet.getShort("hwg");
			String hwgbz1 = resultSet.getString("hwg_bz1");
			String hwgbz2 = resultSet.getString("hwg_bz2");
			final short spracheId = resultSet.getShort("sprache");
			
			CategoryDescription categoryDescription = null;
			if ( spracheId == 16 ) {
				categoryDescription = new CategoryDescription(hwg_aktuell, hwgbz1, hwgbz1, "en");				
			} else if ( spracheId == 17 ) {
				categoryDescription = new CategoryDescription(hwg_aktuell, hwgbz1, hwgbz1, "ru");				
			} else {
				continue;
			}
			
			categoryDescriptions.add(categoryDescription);			
		}
		mySQLConnectorMYSQL.closeConnection();
		return categoryDescriptions;
	}
	
	
	
	private ArrayList<CategoryDescription> getAdditionalCategorie2Description() throws SQLException, IOException {
		
		final ArrayList<CategoryDescription> categoryDescriptions = new ArrayList<CategoryDescription>();
		MySQLConnectorMYSQL mySQLConnectorMYSQL = new MySQLConnectorMYSQL();
		//mySQLConnectorMYSQL.createConnection("localhost", "3307", "xt_shop", "root", "Dlbm18x%", false);
		mySQLConnectorMYSQL.createConnection("172.16.1.30", "3307", "xt_shop", "root", "Dlbm18x%", false);
		
		
		final String stmt_wg = "select wg " +
				"				, wg_bz1 " +
				"				, wg_bz2 " +
				"				, sprache " +
				"				from wgsprache " +
				"				order by wg;";
		
		
		final ResultSet resultSet = mySQLConnectorMYSQL.executeStatement(stmt_wg);
		
		while(resultSet.next()) {
			
			int wg_aktuell = resultSet.getShort("wg");
			String wgbz1 = resultSet.getString("wg_bz1");
			String wgbz2 = resultSet.getString("wg_bz2");
			final short spracheId = resultSet.getShort("sprache");
			
			CategoryDescription categoryDescription = null;
			if ( spracheId == 16 ) {
				categoryDescription = new CategoryDescription(wg_aktuell, wgbz1, wgbz1, "en");				
			} else if ( spracheId == 17 ) {
				categoryDescription = new CategoryDescription(wg_aktuell, wgbz1, wgbz1, "ru");				
			} else {
				continue;
			}
			
			categoryDescriptions.add(categoryDescription);			
		}
		mySQLConnectorMYSQL.closeConnection();
		return categoryDescriptions;
	}	
	
	
	
	private HashMap<String, Double> getStockInformation(final InformixConnector informixConnector) throws SQLException, IOException {
		
		final HashMap<String, Double> stockInformation = new HashMap<String, Double>();
		
		final String stmt = 
					"select a_bas.a, SUM(cron_best.me) as me, a_bas.me_einh as meeinh, a_bas.a_gew as agew" +
					" from " +
					"a_bas, cron_best" +
					" where " +
					"a_bas.a = cron_best.a" +
					" and " +
					"a_bas.shop_kz = 'J'" +
					" group by a_bas.a" +
					" order by a_bas.a;";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		
		while(resultSet.next()) {
			
			final String a = String.valueOf(resultSet.getInt("a"));
			double me = resultSet.getDouble("me");
			final double me_einh = resultSet.getShort("meeinh");
			final double a_gew = resultSet.getDouble("agew");
			
			if ( me_einh == 1 ) {
				int x =  Double.valueOf(me / a_gew).intValue();
				me = x;
			}			
			stockInformation.put(a, me);
		}		
		return stockInformation;		
	}

		
		
	private void insertProductsToMySQL(ArrayList<Product> products, final MySQLConnectorMYSQL mySQLConnector) {
		
		mySQLConnector.execute("DROP TABLE IF EXISTS tmp_products_description");
		mySQLConnector.execute("CREATE TABLE tmp_products_description LIKE xt_products_description;");
		mySQLConnector.execute("DROP TABLE IF EXISTS tmp_products_to_categories");
		mySQLConnector.execute("CREATE TABLE tmp_products_to_categories LIKE xt_products_to_categories;");
		mySQLConnector.execute("DROP TABLE IF EXISTS tmp_products");
		mySQLConnector.execute("CREATE TABLE tmp_products LIKE xt_products;");
		mySQLConnector.execute("DROP TABLE IF EXISTS tmp_media");
		mySQLConnector.execute("CREATE TABLE tmp_media LIKE xt_media;");		
		mySQLConnector.execute("DROP TABLE IF EXISTS tmp_products_permission");
		mySQLConnector.execute("CREATE TABLE tmp_products_permission LIKE xt_products_permission;");
		
		final Iterator<Product> it_products = products.iterator();		
				
		while (it_products.hasNext()) {
			
			Product product = (Product) it_products.next();
			//int product_id = mySQLConnector.execute(product.getInsertStatement());
			mySQLConnector.executeUpdate(product.getInsertStatement());
			int product_id = product.getProductId();
			if ( product_id == -1 ) {
				logger.warning("Attention products_id = -1 for product: " + product.getInternalProductsId());
				continue;
			}
			
			try {
				Number autoCategoryId = this.categoriesRelation.get(product.getCategorie());	
				if ( autoCategoryId != null ) {
					mySQLConnector.executeUpdate(product.getProductCategorieInsertStatement(product_id, autoCategoryId.intValue()));	
				}				
			} catch ( Exception e ) {
				e.printStackTrace();
			}
			//product.setProductsId(product_id);
			
			
			try {
				
				int shop_wg1 = product.getShop_wg1();
				if ( shop_wg1 > 0 ) {
					int autoCategoryId1 = this.categoriesRelation.get(product.getShop_wg1());
					if ( autoCategoryId1 > 0 ) {
						mySQLConnector.executeUpdate(product.getProductCategorieInsertStatement(product_id, autoCategoryId1));				
					}				
				}
				
				int shop_wg2 = product.getShop_wg2();
				if ( shop_wg2 > 0 ) {
					int autoCategoryId2 = this.categoriesRelation.get(product.getShop_wg2());
					if ( autoCategoryId2 > 0 ) {
						mySQLConnector.executeUpdate(product.getProductCategorieInsertStatement(product_id, autoCategoryId2));				
					}				
				}
				
				int shop_wg3 = product.getShop_wg3();
				if ( shop_wg3 > 0 ) {
					int autoCategoryId3 = this.categoriesRelation.get(product.getShop_wg3());
					if ( autoCategoryId3 > 0 ) {
						mySQLConnector.executeUpdate(product.getProductCategorieInsertStatement(product_id, autoCategoryId3));				
					}				
				}
				
				int shop_wg4 = product.getShop_wg4();
				if ( shop_wg4 > 0 ) {
					int autoCategoryId4 = this.categoriesRelation.get(product.getShop_wg4());
					if ( autoCategoryId4 > 0 ) {
						mySQLConnector.executeUpdate(product.getProductCategorieInsertStatement(product_id, autoCategoryId4));				
					}				
				}
							
				int shop_wg5 = product.getShop_wg5();
				if ( shop_wg5 > 0 ) {
					int autoCategoryId5 = this.categoriesRelation.get(product.getShop_wg5());
					if ( autoCategoryId5 > 0 ) {
						mySQLConnector.executeUpdate(product.getProductCategorieInsertStatement(product_id, autoCategoryId5));				
					}				
				}
		
				
			} catch ( Exception e) {
				e.printStackTrace();				
			}
			
					
			try {
				Iterator<ProductDescription> it_ProductDescription = product.getProductDescriptionsIterator();
				
				while (it_ProductDescription.hasNext()) {
					ProductDescription productDescription = (ProductDescription) it_ProductDescription.next();
					mySQLConnector.executeUpdate(productDescription.getInsertStatement(product_id));			
				}				
			} catch ( Exception e ) {
				e.printStackTrace();
			}
			
			
			
			try {				
				int[] pricegroups = product.getPriceGroups();
				
				if ( pricegroups.length > 0 ) {
					
					for (int i = 0; i < pricegroups.length; i++) {
						if ( pricegroups[i] == 0 ) {
							String priceGroup = "group_permission_" + String.valueOf(i);
							String productsPermissionStatement1 = "INSERT INTO tmp_products_permission (pid,permission,pgroup) VALUES('" + product_id + "','1','" + priceGroup + "'); ";
							mySQLConnector.executeUpdate(productsPermissionStatement1);						
						}			 
					}
					
				}
				
			} catch ( Exception e ) {
				e.printStackTrace();
			}	
			
			
			for (int i = 0; i < permissions.length; i++) {
				String productsPermissionStatement2 = "INSERT INTO tmp_products_permission (pid,permission,pgroup) VALUES('" + product_id + "','1','" + permissions[i] + "'); ";
				mySQLConnector.executeUpdate(productsPermissionStatement2);
			}		
			
		}			
	}
	
	
	private ArrayList<Product> getAdditionalShopCategories(final InformixConnector informixConnector, final ArrayList<Product> products) throws SQLException {
		
		final String stmt = 
					"select a_bas.a, shop_wg1, shop_wg2, shop_wg3, shop_wg4, shop_wg5" +
					" from " +
					"a_bas_erw, a_bas" +
					" where " +
					"a_bas_erw.a = a_bas.a" +
					" and " +
					"a_bas.shop_kz = 'J'" +
					" order by a_bas.a;";
		
		final ResultSet resultSet = informixConnector.executeQuery(stmt);
		
		while(resultSet.next()) {
			
			short shop_wg1 = 0;
			short shop_wg2 = 0;
			short shop_wg3 = 0;
			short shop_wg4 = 0;
			short shop_wg5 = 0;			

			final int a = resultSet.getInt("a");
			shop_wg1 = resultSet.getShort("shop_wg1");
			shop_wg2 = resultSet.getShort("shop_wg2");
			shop_wg3 = resultSet.getShort("shop_wg3");
			shop_wg4 = resultSet.getShort("shop_wg4");
			shop_wg5 = resultSet.getShort("shop_wg5");
			
			final Iterator<Product> it_products = products.iterator();
			while (it_products.hasNext()) {
				Product product = (Product) it_products.next();
				
				if ( product.getInternalProductsId() == a ) {					
					product.setShop_wg1(shop_wg1);
					product.setShop_wg2(shop_wg2);
					product.setShop_wg3(shop_wg3);
					product.setShop_wg4(shop_wg4);
					product.setShop_wg5(shop_wg5);					
				}
			}			
		}		
		return products;
	}	
	
	
	
	private void insertProductImages(final MySQLConnectorMYSQL mysqlConnector) {		
		
		StringBuilder stringBuilder = new StringBuilder();
		boolean firstRun = true;
		
		Iterator<ProductImage> images = this.productImages.iterator();
		while (images.hasNext()) {
			ProductImage productImage = (ProductImage) images.next();
			
			if ( firstRun ) {
				stringBuilder.append(productImage.getInsertStatement());
				firstRun = false;
			} else {
				stringBuilder.append(productImage.getValuesStatement());
			}			
		}
		if ( stringBuilder.length() > 0 ) {
			mysqlConnector.executeUpdate(stringBuilder.toString() + ";");			
		}		
	}
	
	
	
	private void updateStockInformation(final MySQLConnectorMYSQL mySQLConnector, final HashMap<String, Double> stockInformations) {
		
		Iterator<String> products = stockInformations.keySet().iterator();
		StringBuilder stringBuilder = new StringBuilder();
		
		while (products.hasNext()) {
			final String a = (String) products.next();
			final int quantity = stockInformations.get(a).intValue();
			
			final String orderUpdateStmt = 
					"UPDATE `xt_products` SET " +
					"products_quantity = '" + quantity + "'" +
					" WHERE " +
					"products_model = '" + a + "'; ";
			
			stringBuilder.append(orderUpdateStmt);			
		}
		
		mySQLConnector.executeUpdate(stringBuilder.toString());
	}
	
	
	private void updatetmpStockInformation(final MySQLConnectorMYSQL mySQLConnector, final HashMap<String, Double> stockInformations) {
		
		Iterator<String> products = stockInformations.keySet().iterator();
		StringBuilder stringBuilder = new StringBuilder();
		
		while (products.hasNext()) {
			final String a = (String) products.next();
			final int quantity = stockInformations.get(a).intValue();
			
			final String orderUpdateStmt = 
					"UPDATE `tmp_products` SET " +
					"products_quantity = '" + quantity + "'" +
					" WHERE " +
					"products_model = '" + a + "'; ";
			
			stringBuilder.append(orderUpdateStmt);			
		}
		
		mySQLConnector.executeUpdate(stringBuilder.toString());
	}



	private void insertProductPricesToMySQL(final MySQLConnectorMYSQL mySQLConnector) {
				
		for (int i = 0; i < this.productPriceGroups.getProductPriceGroups().size(); i++) {
			String group = "";
			if ( this.productPriceGroups.getProductPriceGroups().get(i).getPr_gr_stuf() == 0 ) {
				group = "all";
			} else {
				group = String.valueOf(this.productPriceGroups.getProductPriceGroups().get(i).getPr_gr_stuf());
			}			
			mySQLConnector.executeUpdate("DROP TABLE IF EXISTS tmp_products_price_group_" + group);
			mySQLConnector.executeUpdate("CREATE TABLE tmp_products_price_group_" + group + " LIKE xt_products_price_group_" + group);
		}		
		
		Iterator<ProductPriceGroup> it_productPriceGroups = this.productPriceGroups.getProductPriceGroups().iterator();
		
		while (it_productPriceGroups.hasNext()) {
			ProductPriceGroup productPriceGroup = (ProductPriceGroup) it_productPriceGroups
					.next();
			mySQLConnector.executeUpdate(productPriceGroup.getInsertStatement(this));			
		}
	}	
	
	
	
	private void insertDataFromTMPInRealTables(final MySQLConnectorMYSQL mySQLConnector) {
		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS xt_tmp_customers");
		mySQLConnector.executeUpdate("CREATE TABLE xt_tmp_customers LIKE xt_customers");
		mySQLConnector.executeUpdate("INSERT INTO xt_tmp_customers SELECT * FROM xt_customers");
		mySQLConnector.executeUpdate("DELETE FROM xt_customers");		
		mySQLConnector.executeUpdate("INSERT INTO xt_customers SELECT * FROM tmp_customers");
		mySQLConnector.executeUpdate("UPDATE xt_customers, xt_tmp_customers SET xt_customers.customers_password=xt_tmp_customers.customers_password WHERE xt_customers.customers_email_address = xt_tmp_customers.customers_email_address and xt_customers.customers_email_address != ''");
		mySQLConnector.executeUpdate("UPDATE xt_customers, xt_tmp_customers SET xt_customers.customers_password=xt_tmp_customers.customers_password WHERE xt_customers.customers_cid = xt_tmp_customers.customers_cid and xt_customers.customers_cid != ''");
		
		mySQLConnector.executeUpdate("DELETE FROM xt_customers_addresses");
		mySQLConnector.executeUpdate("INSERT INTO xt_customers_addresses SELECT * FROM tmp_address_book");
		
		mySQLConnector.executeUpdate("DELETE FROM xt_categories_description");
		mySQLConnector.executeUpdate("INSERT INTO xt_categories_description SELECT * FROM tmp_categories_description");
		mySQLConnector.executeUpdate("DELETE FROM xt_categories_permission");
		mySQLConnector.executeUpdate("INSERT INTO xt_categories_permission SELECT * FROM tmp_categories_permission");
		mySQLConnector.executeUpdate("DELETE FROM xt_categories");
		mySQLConnector.executeUpdate("INSERT INTO xt_categories SELECT * FROM tmp_categories");
		
		mySQLConnector.executeUpdate("DELETE FROM xt_products_price_special");
		mySQLConnector.executeUpdate("INSERT INTO xt_products_price_special SELECT * FROM tmp_xt_products_price_special");
//		mySQLConnector.executeUpdate("DELETE FROM xt_plg_hi_suggestion");
//		mySQLConnector.executeUpdate("INSERT INTO xt_plg_hi_suggestion SELECT * FROM tmp_xt_plg_hi_suggestion");
		mySQLConnector.executeUpdate("DELETE FROM xt_products_description");
		mySQLConnector.executeUpdate("INSERT INTO xt_products_description SELECT * FROM tmp_products_description");
		mySQLConnector.executeUpdate("DELETE FROM xt_products_to_categories");
		mySQLConnector.executeUpdate("INSERT INTO xt_products_to_categories SELECT * FROM tmp_products_to_categories");
		mySQLConnector.executeUpdate("DELETE FROM xt_products");
		mySQLConnector.executeUpdate("INSERT INTO xt_products SELECT * FROM tmp_products");
		mySQLConnector.executeUpdate("DELETE FROM xt_products_permission");
		mySQLConnector.executeUpdate("INSERT INTO xt_products_permission SELECT * FROM tmp_products_permission");
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS xt_tmp_media");
		mySQLConnector.executeUpdate("CREATE TABLE xt_tmp_media LIKE xt_media");
		mySQLConnector.executeUpdate("INSERT INTO xt_tmp_media SELECT * FROM xt_media where class = 'category'");
		mySQLConnector.executeUpdate("DELETE FROM xt_media");
		mySQLConnector.executeUpdate("INSERT INTO xt_media SELECT * FROM xt_tmp_media");		
		
		for (int i = 0; i < this.productPriceGroups.getProductPriceGroups().size(); i++) {
			String group = "";
			if ( this.productPriceGroups.getProductPriceGroups().get(i).getPr_gr_stuf() == 0 ) {
				group = "all";
			} else {
				group = String.valueOf(this.productPriceGroups.getProductPriceGroups().get(i).getPr_gr_stuf());
			}
			
			mySQLConnector.executeUpdate("DELETE FROM xt_products_price_group_" + group);
			mySQLConnector.executeUpdate("INSERT INTO xt_products_price_group_" + group + " SELECT * FROM tmp_products_price_group_"+ group);
		}
		
		mySQLConnector.executeUpdate("DELETE FROM xt_seo_url where link_type != 3");
		
		final String seoInsertStmt1 = "REPLACE INTO xt_seo_url (url_md5, url_text, language_code, link_type, link_id)" +
				" SELECT" +
				"  MD5(CONCAT('de/',cd.categories_name, cd.categories_id)) AS url_md5" +
				" , CONCAT('de/',cd.categories_name, cd.categories_id) AS url_text" +
				" , 'de' AS language_code" +
				" , 2 AS link_type" +
				" , cd.categories_id AS link_id" +
				" FROM xt_categories_description cd, xt_categories c" +
				" WHERE c.parent_id = 0" +
				" AND cd.categories_id = c.categories_id" +
				" AND cd.language_code = 'de';";
				
		final String seoInsertStmt2 = "REPLACE INTO xt_seo_url (url_md5, url_text, language_code, link_type, link_id)" +
				" SELECT" +
				"  MD5(CONCAT('ru/',cd.categories_name, cd.categories_id)) AS url_md5" +
				" , CONCAT('ru/',cd.categories_name, cd.categories_id) AS url_text" +
				" , 'ru' AS language_code" +
				" , 2 AS link_type" +
				" , cd.categories_id AS link_id" +
				" FROM xt_categories_description cd, xt_categories c" +
				" WHERE c.parent_id = 0" +
				" AND cd.categories_id = c.categories_id" +
				" AND cd.language_code = 'de';";

		final String seoInsertStmt3 = "REPLACE INTO xt_seo_url (url_md5, url_text, language_code, link_type, link_id)" +
				" SELECT" +
				" MD5(CONCAT('de/', (SELECT TRIM(categories_name) FROM xt_categories_description WHERE c.parent_id = categories_id AND language_code = 'de' ), '/', cd.categories_name, cd.categories_id)) AS url_md5" +
				" , CONCAT('de/', (SELECT TRIM(categories_name) FROM xt_categories_description WHERE c.parent_id = categories_id AND language_code = 'de' ), '/', cd.categories_name, cd.categories_id) AS url_text" +
				" , 'de' AS language_code" +
				" , 2 AS link_type" +
				" , cd.categories_id AS link_id" +
				" FROM xt_categories_description cd, xt_categories c" +
				" WHERE c.parent_id > 0" +
				" AND cd.categories_id = c.categories_id" +
				" AND cd.language_code = 'de';";
				
		final String seoInsertStmt4 = "REPLACE INTO xt_seo_url (url_md5, url_text, language_code, link_type, link_id)" +
				" SELECT" +
				" MD5(CONCAT('ru/', (SELECT TRIM(categories_name) FROM xt_categories_description WHERE c.parent_id = categories_id AND language_code = 'ru' ), '/', cd.categories_name, cd.categories_id)) AS url_md5" +
				" , CONCAT('ru/', (SELECT TRIM(categories_name) FROM xt_categories_description WHERE c.parent_id = categories_id AND language_code = 'ru' ), '/', cd.categories_name, cd.categories_id) AS url_text" +
				" , 'ru' AS language_code" +
				" , 2 AS link_type" +
				" , cd.categories_id AS link_id" +
				" FROM xt_categories_description cd, xt_categories c" +
				" WHERE c.parent_id > 0" +
				" AND cd.categories_id = c.categories_id" +
				" AND cd.language_code = 'de';";

		final String seoInsertStmt5 = "REPLACE INTO xt_seo_url (url_md5, url_text, language_code, link_type, link_id)" +
				" SELECT" +
				"  MD5(CONCAT('ru/',cd.products_name, c.products_model)) AS url_md5" +
				" , CONCAT('ru/',cd.products_name, c.products_model) AS url_text" +
				" , 'ru' AS language_code" +
				" , 1 AS link_type" +
				" , cd.products_id AS link_id " +
				" FROM xt_products_description cd, xt_products c" +
				" WHERE cd.products_id = c.products_id" +
				" AND cd.language_code = 'de';";


		final String seoInsertStmt6 = "REPLACE INTO xt_seo_url (url_md5, url_text, language_code, link_type, link_id)" +
				" SELECT" +
				"  MD5(CONCAT('de/',cd.products_name, c.products_model)) AS url_md5" +
				" , CONCAT('de/',cd.products_name, c.products_model) AS url_text" +
				" , 'de' AS language_code" +
				" , 1 AS link_type" +
				" , cd.products_id AS link_id " +
				" FROM xt_products_description cd, xt_products c" +
				" WHERE cd.products_id = c.products_id" +
				" AND cd.language_code = 'de';";
		
		mySQLConnector.executeUpdate(seoInsertStmt1);
		mySQLConnector.executeUpdate(seoInsertStmt2);
		mySQLConnector.executeUpdate(seoInsertStmt3);
		mySQLConnector.executeUpdate(seoInsertStmt4);
		mySQLConnector.executeUpdate(seoInsertStmt5);
		mySQLConnector.executeUpdate(seoInsertStmt6);
		
	}
	
	
	
	private void insertDataFromTMPInRealTables2(final MySQLConnectorMYSQL mySQLConnector) {
		
		mySQLConnector.executeUpdate("DROP TABLE IF EXISTS xt_tmp_customers");
		mySQLConnector.executeUpdate("CREATE TABLE xt_tmp_customers LIKE xt_customers");
		mySQLConnector.executeUpdate("INSERT INTO xt_tmp_customers SELECT * FROM xt_customers");
		mySQLConnector.executeUpdate("DELETE FROM xt_customers");		
		mySQLConnector.executeUpdate("INSERT INTO xt_customers SELECT * FROM tmp_customers");
		mySQLConnector.executeUpdate("UPDATE xt_customers, xt_tmp_customers SET xt_customers.customers_password=xt_tmp_customers.customers_password WHERE xt_customers.customers_email_address = xt_tmp_customers.customers_email_address and xt_customers.customers_email_address != ''");
		mySQLConnector.executeUpdate("UPDATE xt_customers, xt_tmp_customers SET xt_customers.customers_password=xt_tmp_customers.customers_password WHERE xt_customers.customers_cid = xt_tmp_customers.customers_cid and xt_customers.customers_cid != ''");
	
		mySQLConnector.executeUpdate("DELETE FROM xt_customers_addresses");
		mySQLConnector.executeUpdate("INSERT INTO xt_customers_addresses SELECT * FROM tmp_address_book");
		
	}
	
	
	
	private String replaceSpecialCharacter ( String s ) {
		
		String s2 = s.replace("'", "");
		return s2;		
	}
	
	
	public int getAutoProductsId(final int products_id) {
		
		Iterator<Product> products = this.products.iterator();
		while (products.hasNext()) {
			Product product = (Product) products.next();
			if ( product.getInternalProductsId() == products_id ) {
				return product.getProductId();
			}
		}
		return -1;
	}
	
	public CategoryDescription getCategoryDescription(final ArrayList<CategoryDescription> categoryDescriptions, final int categorie, final String languageCode) {

		final Iterator<CategoryDescription> it_cIterator = categoryDescriptions.iterator();
		while (it_cIterator.hasNext()) {
			CategoryDescription categoryDescription = (CategoryDescription) it_cIterator
					.next();
			if ( categoryDescription.getCategories_id() == categorie && categoryDescription.getLanguage_code().equals(languageCode) ) {
				return categoryDescription;
			}
		}
		return null;
		
	}
	
	
	public String calculateEANPruefziffer(final String ean) {
	
		
		int eanPruefZiffer = 0;
		int[] eanZiffern = new int[12];
		int[] mal = new int[12];
		String eanMitPruef = "";
		
		mal[0] = 1;
		mal[1] = 3;
		mal[2] = 1;
		mal[3] = 3;
		mal[4] = 1;
		mal[5] = 3;
		mal[6] = 1;
		mal[7] = 3;
		mal[8] = 1;
		mal[9] = 3;
		mal[10] = 1;
		mal[11] = 3;
		
		char[] charEAN = ean.toCharArray();
		
		if ( charEAN.length >= 12 ) {
			
			for (int i = 0; i < 12; i++) {
				char tempEAN = charEAN[i];
				String sEan = String.valueOf(tempEAN);
				eanMitPruef = eanMitPruef + sEan;
				int iEan = Integer.valueOf(sEan).intValue();
				eanZiffern[i] = iEan * mal[i];				
			}
			
			for (int i = 0; i < eanZiffern.length; i++) {
				eanPruefZiffer += eanZiffern[i];
			}
			
			eanPruefZiffer = eanPruefZiffer % 10;
			
			if ( eanPruefZiffer != 0 ) {
				eanPruefZiffer = eanPruefZiffer - 10;
				eanPruefZiffer = eanPruefZiffer * (-1);
			}
			
			String eanP = String.valueOf(eanPruefZiffer);
			eanMitPruef = eanMitPruef + eanP;
		}	
		return eanMitPruef;		
	}	
}



