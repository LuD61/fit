package DataEnumerations;

public enum LanguageCode {
	
	de(10),
	ru(20);

		 
	private int code;

	private LanguageCode(int c) {
		code = c;
	}
 
	public int getCode() {
		return code;
	}
}
