package DataEnumerations;

public enum TaxClass {
	
	STANDARD19(1),
	LOWER7(2);

		 
	private int code;

	private TaxClass(int c) {
		code = c;
	}
 
	public int getCode() {
		return code;
	}

}
