package DataEnumerations;

public enum Prefix {
	
	PLUS("+"),
	MINUS("-");

		 
	private String code;

	private Prefix(String c) {
		code = c;
	}
 
	public String getCode() {
		return code;
	}

}
