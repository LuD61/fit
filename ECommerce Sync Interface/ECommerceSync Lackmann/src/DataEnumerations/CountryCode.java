package DataEnumerations;

public enum CountryCode {
	
	m(0),
	w(1);

		 
	private int code;

	private CountryCode(int c) {
		code = c;
	}
 
	public int getCode() {
		return code;
	}
}
