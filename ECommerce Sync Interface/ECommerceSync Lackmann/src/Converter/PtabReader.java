package Converter;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import fit.informixconnector.InformixConnector;

public class PtabReader {
	
	public static String getPtabValue(final InformixConnector informixConnector, final String ptItem, final String ptWert) throws SQLException, IOException {
		
		final String stmt = "select ptwer2 from ptabn where ptitem = '" + ptItem + "' and ptwert = '" + ptWert + "';";
		final ResultSet resultSet = informixConnector.executeQuery(stmt);				
		
		String result = null;
		
		while(resultSet.next()) {
			
			try {			
				result = resultSet.getString("ptwer2");				
			} catch (Exception e){				
				e.printStackTrace();				
			}
		}	
		return result;
	}
	
	
	public static String getPtabValuePtBez(final InformixConnector informixConnector, final String ptItem, final String ptWert) throws SQLException, IOException {
		
		final String stmt = "select ptbez from ptabn where ptitem = '" + ptItem + "' and ptwert = '" + ptWert + "';";
		final ResultSet resultSet = informixConnector.executeQuery(stmt);				
		
		String result = null;
		
		while(resultSet.next()) {
			
			try {			
				result = resultSet.getString("ptbez");				
			} catch (Exception e){				
				e.printStackTrace();				
			}
		}	
		return result;
	}
	
}
