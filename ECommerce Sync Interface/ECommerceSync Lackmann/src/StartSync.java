import javax.swing.JOptionPane;

import fit.informixconnector.InformixConnector;

import Connection.SSHConnection;
import Export.ExportDataGenerator;
import Import.Importer;
import MailService.SendJavaMail;

/**
 * 
 */

/**
 * @author danielh
 *
 */
public class StartSync {

	/**
	 * @param args
	 */
	
	private static InformixConnector informixConnector;
	
	public static void main(String[] args) {
		
		final boolean withDialog = Boolean.valueOf(args[0]);
		final int transferType = Integer.valueOf(args[1]);
		
		//MySQL Connection Settings
		final String mySQLComputerName = String.valueOf(args[2]);
		final String mySQLPortNumber = String.valueOf(args[3]);
		final String mySQLDatabaseName = String.valueOf(args[4]);
		final String mySQLUserName = String.valueOf(args[5]);
		final String mySQLPassword = String.valueOf(args[6]);
		
		final String eMailTo = String.valueOf(args[7]);
		
		informixConnector = new InformixConnector();
		
		// 1 = Vollst�ndiger Export
		
		// 2 = Nur Kundendaten
		
		// 3 = Nur Bestandsdaten
		
		// 4 = Auftragsabruf Simulation
		
		// 5 = Auftragsabruf mit L�schen

		switch ( transferType ) {
		
		case 1:
			startExport(withDialog, transferType, mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword, eMailTo);
			break;
		case 2:
			startExport(withDialog, transferType, mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword, eMailTo);
			break;
		case 3:
			startExport(withDialog, transferType, mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword, eMailTo);
			break;
		case 4:
			startImport(withDialog, true, mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword, eMailTo, transferType);
			break;
		case 5:
			startImport(withDialog, false, mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword, eMailTo, transferType);
			break;
		case 6:
			startImport(withDialog, false, mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword, eMailTo, transferType);
			break;
			
		}		
	}
	
	
	
	private static void startExport (final boolean withDialog, final int transferType, final String mySQLComputerName, 
			final String mySQLPortNumber, final String mySQLDatabaseName, final String mySQLUserName, final String mySQLPassword, final String emailTO) {
		
		SendJavaMail sendJavaMail = new SendJavaMail();		
		informixConnector.createConnection();
		
		
		if ( withDialog ) {
			int ok = JOptionPane.showConfirmDialog(null, "M�chten Sie den Export jetzt starten?", "Exportvorgang starten?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (ok == JOptionPane.NO_OPTION) {
				System.out.println("Synchonisation durch Benutzer abgebrochen!");
				JOptionPane.showMessageDialog(null, "Synchronisation abgebrochen", "Vorgang abgebrochen", JOptionPane.ERROR_MESSAGE);
				System.exit(0);
			}
		}
		
		ExportDataGenerator exportDataGenerator = new ExportDataGenerator();
		boolean transferTypeReturn = exportDataGenerator.startProcess(withDialog, transferType, mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword, informixConnector);
		
		if ( withDialog && transferTypeReturn ) {
			JOptionPane.showMessageDialog(null, "Synchonisation erfolgreich", "Information", JOptionPane.INFORMATION_MESSAGE);
			sendJavaMail.sendEmail("Stammdaten erfolgreich an den WebShop gesendet.", "Synchronisation der Stammdaten erfolgreich", emailTO);
			System.exit(0);
		} else if ( transferTypeReturn ) {
			sendJavaMail.sendEmail("Stammdaten erfolgreich an den WebShop gesendet.", "Synchronisation der Stammdaten erfolgreich", emailTO);			
			System.exit(0);
		} else {
			JOptionPane.showMessageDialog(null, "Synchonisation nicht erfolgreich", "Error", JOptionPane.ERROR_MESSAGE);
			sendJavaMail.sendEmail("Stammdaten nicht erfolgreich an den WebShop gesendet.", "Synchronisation der Stammdaten fehlgeschlagen", emailTO);
			System.exit(1);
		}
		
		informixConnector.closeConnection();
		
	}
	
	
	public static void startImport (final boolean withDialog, final boolean simulation, final String mySQLComputerName, final String mySQLPortNumber, 
			final String mySQLDatabaseName, final String mySQLUserName, final String mySQLPassword, final String emailTO, final int transferType) {
		
		informixConnector.createConnection();
		Importer importer = new Importer();
		
		if ( transferType == 6 ) {
			importer.importLanguages(mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword);
		} else {
			importer.startImport(withDialog, simulation, mySQLComputerName, mySQLPortNumber, mySQLDatabaseName, mySQLUserName, mySQLPassword, emailTO);			
		}		
		
		informixConnector.closeConnection();
		
	}

}
