echo off
cd %TEIS%\rsf\weldr351

echo "mo_weldr351"
echo "Programm weldr351 Modul mo_weldr351" > tmp
rscomp -d -i %TEIS%\include -m %BWS%\module/ -o mo_weldr351  mo_weldr351  >> tmp
if errorlevel 1 goto ende
copy mo_weldr351.rsm %BWS%\module\weldr351 > tmp
copy mo_weldr351.rsm %BWS%\module > tmp

echo "sweldr351"
echo "Programm weldr351 Modul sweldr351" > tmp
rscomp -d -i %TEIS%\include -m %BWS%\module/ -o sweldr351  sweldr351  >> tmp
if errorlevel 1 goto ende
copy sweldr351.rsm %BWS%\module\weldr351 > tmp
copy sweldr351.rsm %BWS%\module > tmp

echo "Linklauf"
echo "Programm weldr351" > tmp
rslink -d -m %BWS%/module -o weldr351 sweldr351 >> tmp
if errorlevel 1 goto ende

copy weldr351.rsr %BWS%\bin > tmp

del weldr351.rsr > tmp
del *.rsm > tmp

echo "                                                fertig"
:ende
if errorlevel 1 goto ERR:
goto OK
:ERR
notepad tmp
set AUSSTIEG=1
rem cd %TEIS%\cmd
exit
:END
echo "FEHLER bei weldr351">>%TEIS%\cmd\__comp.log
:OK

