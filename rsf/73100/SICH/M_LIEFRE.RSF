module m_liefre
/*
--------------------------------------------------------------------------------
-
-       BSA  Bizerba Software- und Automationssysteme GmbH
-       Vorstadtstr. 59, 7465 Geislingen, Tel.: 07433/2413
-
--------------------------------------------------------------------------------
-
-       Modulname               :       m_liefre
-
-       Hauptproceduren         :       monitor_ausgabe
-                                       druck_ausgabe
-
-       Autor                   :       Ralf Moebius
- 
-       Modifikationsdatum      :       
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       #819    1.302   21.05.92        RM         Behandlung Nullvalues im 
-                                                  Datumfeld
-       #894    1.302   16.06.92        RM         Erweiterung Fehleranalyse
-       #1270   1.400  12.01.93        RM         Erweiterung table lief_retba
-                                                  (Ersetzen pers ---> err_txt)
-       #1383   1.400  10.03.93        KB         .* in .% aendern bei selects
-       #1646   1.402c   17.08.93   RM           Stellenerweiterung und Item-
-                                                modifikation bei wastat_tmp
--------------------------------------------------------------------------------
-                                                                   
-   Modulbeschreibung: Modul zur Arbeit am temp_table f}r Programm 73100 u.74100
-                                                                  
-                             - Erstellen und arbeiten mit create-String 
-                             - Erstellen und arbeiten mit insert-String
-                             - Erstellen und arbeiten mit drop-String 
-                             - Bildschirmausgabe Abbuchungsanalyse    
-                             - Druckausgabe Abbuchungsanalyse    
- 
-------------------------------------------------------------------------------
*/
/*------------------------- EXPORTS ------------------------------------------*/
export
  procedure monitor_ausgabe
  procedure druck_ausgabe
end


/*------------------------- IMPORTS ------------------------------------------*/
import module mo_men_m   end

import module mo_inkey     end

import module mo_creat    end

import module mo_repor    end


/*------------------------ DATENBANKVEREINBARUNGEN --------------------------*/
database bws
  table lief_retba    end
  table wastat_tmp    end
  table a_bas         end
  table adr           end
  table kun           end
  table mdn           end
  table fil           end
  table lsp           end
  table retp          end
end


/*------------------------------ INCLUDES -----------------------------------*/
#INCLUDE "messdef"

/*-------------------------- DATENVEREINBARUNGEN ----------------------------*/
data
  field i           smallint               /* Laufvariable                   */
  field anz_ds      integer                /* Anzahl DS im temp_table        */
  field dret        smallint               /* R}ckkehrcode                   */
  field zwmdn_name  like adr.adr_krz       /* Mandantenkurzbezeichnung       */
  field zwfk_name   like adr.adr_krz       /* Filialen/Kundenkurzbezeichnung */
  field zwls_ret    like wastat_tmp.kopf_3 /* Text: Lieferschein oder Retoure*/
  field zwdate      like wastat_tmp.ztr    /* Liefer- bzw. Retourendatum     */
  field zwa         like wastat_tmp.nr_13  /* Artikelnummer                  */
  field zwa_bez     like wastat_tmp.pos_bz /* Artikelbezeichnung             */
  field zwmeng_anz  like wastat_tmp.gew_tmp/* Menge bzw. Anzahl  #1646       */
  field zwvk_pr     like wastat_tmp.sp_fil_ek /* VK - Preis                  */
  field zwlad_pr    like wastat_tmp.sp_fil_ek /* EK - Preis                  */
  field tty_name    char (16)              /* Ger{tename                     */
  field tmp_name    char (16)              /* Name des temp_tables           */
  field zwkopf_1    like wastat_tmp.kopf_1 /* ZwSp Mandant und Kurzname      */
  field zwkopf_2    like wastat_tmp.kopf_2 /* ZwSp Filiale/Kunde und Kurzname*/
  field zwkopf_3    like wastat_tmp.kopf_3 /* ZwSp f}r LS/Ret.Text und Nummer*/
  field zwkopf_4    like wastat_tmp.kopf_4 /* Artikel und Artikelnummer      */
  field zwkopf_5    like wastat_tmp.kopf_5 /* Fehleranalyse  #894            */
  field create_str1 char (250)             /* Createstring                   */
  field insert_str1 char (250)             /* Insertstring1                  */
  field insert_str2 char (250)             /* Insertstring2                  */
  field insert_str3 char (250)             /* Insertstring3                  */
  field delete_str  char ( 80)             /* Delete-String                  */
  field drop_str    char ( 80)             /* Drop-String                    */
  field text6       char ( 75)             /* Anzeigestring f}r ctext6       */
  field and_part    char ( 30)             /* Selektion }ber Kunden o.Filiale*/
end


constant
  ctext1           value    "Lieferschein Nr.:"
  ctext2           value    "Retouren Nr.:"
  ctext3           value    "\R Ausdruck kann nicht erfolgen ---> RETURN \N"
  ctext4           value    "Filiale:"
  ctext5           value    "Kunde:"
  ctext6           value    "Datens{tze werden selektiert. DS Nr.:"
  ctext7           value    "Fehleranalyse:"       /*-------- #894 -------*/
end


/*------------------------ PROCEDUREN -------------------------------------*/
/** -------------------------------------------------------------------------
-	Procedure	: anz_anzeige   
-
-	In		: nummer    : Zahl hinter Ausgabetext
-
-	Out		: -
-			  
-	Beschreibung    : Anzeige der Anzahl DS des temp_tables
------------------------------------------------------------------------- **/
procedure anz_anzeige  

  parameter
    field nummer   integer  
  end

field zahl_str  char (6)

    if "" = "@(#) m_liefre.rsf	1.402c	17.08.93	by KB" end
				
  let text6    = ctext6
  let zahl_str = nummer

  open window anzeige (22, 80)
  set window (1 , 77, 22, 1)

  putstr (text6, zahl_str, strlen (text6) + 2, 0)
  display pos ( 0, 0 ) value (text6)
  refresh

  close window anzeige      

end            /* procedure anz_anzeige   */


/** -------------------------------------------------------------------------
-	Procedure	: prep_cursor
-
-	In		: - 
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : preparen der Cursoren                            
-			  
-	Aufruf in proc  : wert_zuordnung
------------------------------------------------------------------------- **/
procedure prep_cursor

field order_part  char (30)

  let order_part = " order by lief_retba.nr"

/*------------------------ #894 A ---------------------------------------
	where lief_retba.delstatus = 1
*/
  prepare cursor lief_ret from sql
	select #lief_retba.% from lief_retba
	where lief_retba.delstatus <> 0
	end
	field and_part
	field order_part

  if sqlstatus < 0
        return (sqlstatus)
  end
/*------------------------ #894 E -------------------------------------*/

  prepare cursor mdn_adr from sql
	select #mdn.adr from mdn
	where mdn.mdn = $lief_retba.mdn
	end
  
  if sqlstatus < 0
        return (sqlstatus)
  end

  /* auf adr in adr unique !!! */
  prepare cursor mdn_adr_krz from sql
	select #adr.adr_krz from adr
	where adr.adr = $mdn.adr   
	end
  
  if sqlstatus < 0
        return (sqlstatus)
  end

  prepare cursor fil_adr from sql
	select #fil.adr from fil
	where fil.fil = $lief_retba.kun and
              fil.mdn = $lief_retba.mdn
	end

  if sqlstatus < 0
        return (sqlstatus)
  end

  prepare cursor kun_adr from sql
	select #kun.kun_krz1 from kun
        where kun.mdn = $lief_retba.mdn and
	      kun.fil = $lief_retba.fil and
	      kun.kun = $lief_retba.kun
	end
  
  if sqlstatus < 0
        return (sqlstatus)
  end

  prepare cursor fil_adr_krz from sql
	select #adr.adr_krz from adr
	where adr.adr = $fil.adr   
	end
  
  if sqlstatus < 0
        return (sqlstatus)
  end

  prepare cursor lsp_curs from sql
	select #lsp.a, #lsp.lief_me, #lsp.ls_vk_pr, #lsp.ls_lad_pr
			from lsp
	where lsp.mdn = $lief_retba.mdn  and
	      lsp.ls  = $lief_retba.nr
	end
  
  if sqlstatus < 0
        return (sqlstatus)
  end

  prepare cursor retp_curs from sql
	select #retp.a, #retp.ret_me, #retp.ret_vk_pr, #retp.ret_lad_pr
			from retp
	where retp.mdn = $lief_retba.mdn  and
	      retp.ret = $lief_retba.nr
	end
  
  if sqlstatus < 0
        return (sqlstatus)
  end

  /* auf a in a_bas unique !!! */
  prepare cursor art_bez from sql
	select #a_bas.a_bz1 from a_bas
	where a_bas.a = $zwa
	end
  
  if sqlstatus < 0
        return (sqlstatus)
  end

  return (0)

end	/* procedure prep_cursor	*/


/** -------------------------------------------------------------------------
-	Procedure	: loe_cursor
-
-	In		: - 
-
-	Out		: -
-			  
-	Beschreibung    : closen und erasen der Cursoren                   
-			  
-	Aufruf in proc  : wert_zuordnung
------------------------------------------------------------------------- **/
procedure loe_cursor

  close cursor lief_ret 
  close cursor mdn_adr 
  close cursor mdn_adr_krz 
  close cursor fil_adr 
  close cursor kun_adr 
  close cursor fil_adr_krz 
  close cursor lsp_curs 
  close cursor retp_curs 
  close cursor art_bez 

  erase cursor lief_ret 
  erase cursor mdn_adr 
  erase cursor mdn_adr_krz 
  erase cursor fil_adr 
  erase cursor kun_adr 
  erase cursor fil_adr_krz 
  erase cursor lsp_curs 
  erase cursor retp_curs 
  erase cursor art_bez 

end	/* procedure loe_cursor	*/


/** -------------------------------------------------------------------------
-	Procedure	: adressen_fetch
-
-	In		: - 
-
-	Out		: -
-			  
-	Beschreibung    : Selektion von Adressen Mandant, Filiale, Kunde   
-			  
-	Aufruf in proc  : wert_zuordnung
------------------------------------------------------------------------- **/
procedure adressen_fetch

  init table mdn
  init table adr

  close cursor mdn_adr 
  fetch cursor mdn_adr
  close cursor mdn_adr_krz 
  fetch cursor mdn_adr_krz
  let zwmdn_name = adr.adr_krz
  perform hk_pruefen (zwmdn_name) returning (zwmdn_name)

  if lief_retba.kun_fil 

     	init table fil
     	init table adr
  	close cursor fil_adr 
  	fetch cursor fil_adr 
  	close cursor fil_adr_krz 
  	fetch cursor fil_adr_krz 
  	let zwfk_name = adr.adr_krz

  else

        init table kun
  	close cursor kun_adr 
  	fetch cursor kun_adr 
  	let zwfk_name = kun.kun_krz1

  end

  perform hk_pruefen (zwfk_name) returning (zwfk_name)

end	/* procedure adressen_fetch	*/


/** -------------------------------------------------------------------------
-	Procedure	: lsp_retp_fetch
-
-	In		: - 
-
-	Out		: -
-			  
-	Beschreibung    : Selektion aus lsp oder retp entsprechend 
-                         lief_retba.blg_typ
-			  
-	Aufruf in proc  : wert_zuordnung
------------------------------------------------------------------------- **/
procedure lsp_retp_fetch

  if lief_retba.blg_typ = "X"
	init table retp
        fetch cursor retp_curs
	let zwa         = retp.a
	let zwmeng_anz  = retp.ret_me
	let zwvk_pr     = retp.ret_vk_pr
	let zwlad_pr    = retp.ret_lad_pr
  else  
        init table lsp
	fetch cursor lsp_curs
	let zwa         = lsp.a
	let zwmeng_anz  = lsp.lief_me
	let zwvk_pr     = lsp.ls_vk_pr
	let zwlad_pr    = lsp.ls_lad_pr
  end

end	/* procedure lsp_retp_fetch	*/


/** -------------------------------------------------------------------------
-	Procedure	: hk_pruefen 
-
-	In		: zu pruefender String
-
-	Out		: bereinigter String
-			  
-	Beschreibung    : Zeichen " aus strings entfernen, durch ' ersetzen
-			  
-	Aufruf in proc  :  adressen_fetch
------------------------------------------------------------------------- **/
procedure hk_pruefen

parameter
  field par char (200)
end

field hk_pos  smallint
field hk_str  char(1)
field tr_str  char(1)

  let hk_str = "\""
  let tr_str = "'"

  let hk_pos = instring (par, hk_str, 1, 1)

  if hk_pos > 0

	repeat
		putstr (par, tr_str, hk_pos,0)
		let hk_pos = instring (par, hk_str, 1, 1)
	until hk_pos = 0

  end

  return (clipped(par))

end  /* procedure hk_pruefen	*/


/** -------------------------------------------------------------------------
-	Procedure	: druck_not_pos
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Mitteilung, da~ Ausdruck nicht m|glich, mit
-                         ENTER-Best{tigung
-			  
-	Aufruf in proc  : monitor_ausgabe, druck_ausgabe
------------------------------------------------------------------------- **/
procedure druck_not_pos

  open window window_mitt (22,78)
  set bordered window ( 3, 46, 8, 16)
  display pos ( 0, 1) value (ctext3)
  refresh
  repeat
	perform g_inkey
  until SYSKEY = KEYCR
  close window window_mitt

end            /* procedure druck_not_pos */


/** -------------------------------------------------------------------------
-	Procedure	: inp_tty_name   
-
-	In		: -
-
-	Out		: sysstatus
-			  
-	Beschreibung    : Eingabe des tty - Namens
-			  
-	Aufruf in proc  : temp_table_ver
------------------------------------------------------------------------- **/
procedure inp_tty_name

  open channel terminal pipe ("basename `tty`", "r")
  if sysstatus
	return (sysstatus)
  end
  read  (tty_name)
  if sysstatus
  	close channel
	return (sysstatus)
  end
  close channel

  return (0)

end  	/* procedure inp_tty_name	*/


/** -------------------------------------------------------------------------
-	Procedure	: str_zuordnung
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Strings f}r temporaeres table f}llen                 
-			  
-	Aufruf in proc  : temp_table_fuellen
------------------------------------------------------------------------- **/
procedure str_zuordnung

field zwstring  char (30)

  init zwkopf_1, zwkopf_2, zwkopf_3, zwkopf_4, zwkopf_5  /*----- #894 ---*/

  let zwstring = lief_retba.mdn
  putstr (zwkopf_1, zwstring, 6 - strlen (zwstring), 0 )
  putstr (zwkopf_1, zwmdn_name, 8, 0)

  if lief_retba.kun_fil 
	let zwstring = ctext4
  else  let zwstring = ctext5
  end

  putstr (zwkopf_2, zwstring, 0, 0 )
  let zwstring = lief_retba.kun
  putstr (zwkopf_2, zwstring, 14 - strlen (zwstring), 0 )
  let zwstring = zwfk_name
  putstr (zwkopf_2, zwstring, 16, 0 )

/*---------------------------- #819 A --------------------------------------*/
  if IS NULL (lief_retba.dat)
  	let zwdate   = "        "
  else  let zwdate   = lief_retba.dat
  end
/*---------------------------- #819 E --------------------------------------*/
  if lief_retba.blg_typ = "X"
	let zwls_ret = ctext2
  else  let zwls_ret = ctext1
  end
 
  let zwstring = zwls_ret         
  putstr (zwkopf_3, zwstring, 0, 0 )
  let zwstring = lief_retba.nr 
  putstr (zwkopf_3, zwstring, 20, 0 )

/*--------------------- #894 A ------------------------------------------*/
  let zwstring = ctext7
  putstr (zwkopf_5, zwstring, 0 ,0 )
  let zwstring = lief_retba.delstatus
  putstr (zwkopf_5, zwstring, 17, 0)
/*--------------------- #1270 A ------------------------------------------ 
  let zwstring = lief_retba.pers     
*/
  let zwstring = lief_retba.err_txt
/*--------------------- #1270 E ------------------------------------------*/
  putstr (zwkopf_5, zwstring, 25, 0)
/*--------------------- #894 E ------------------------------------------*/

  let zwstring = zwa
  putstr (zwkopf_4, zwstring, 14 - strlen (zwstring), 0 )
  let zwstring = zwa_bez          
  putstr (zwkopf_4, zwstring, 16, 0 )

end	/* procedure str_zuordnung 	*/


/** -------------------------------------------------------------------------
-	Procedure	: temp_create 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Create temporaeres table in Anlehnung an wastat_tmp  
-			  
-	Aufruf in proc  : temp_table_ver
------------------------------------------------------------------------- **/
procedure temp_create

field zus_str char (3)

  let zus_str  = i
  let tmp_name = tty_name
  putstr (tmp_name, zus_str, 0, 0 )

  let create_str1 =
"create temp table " # clipped(tmp_name) # "(gew_tmp decimal(12,3), kopf_1 char(24), kopf_2 char(36), kopf_3 char (36), kopf_4 char (36), sp_fil_ek decimal ( 8,2), sp_hk_tkost decimal ( 8,2), ztr char(8), kopf_5 char (36))"
 /*--- #894/#1646 -----*/

end	/* procedure temp_create	*/


/** -------------------------------------------------------------------------
-	Procedure	: temp_table_ver
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Sukzessives create des temporaeren table bis Name ok 
-			  
-	Aufruf in proc  : wert_zuordnung
------------------------------------------------------------------------- **/
procedure temp_table_ver

  init i

  perform inp_tty_name returning (dret)
  if dret <> 0
	return (dret)
  end

  let sqlstatus = 1

  while (sqlstatus <> 0 and i < 999)

    	let i = i + 1
    	perform temp_create 

#INCLUDE "unset_sq"
  	execute from sql
        	end
		field create_str1
#INCLUDE "set_sql"

  end

  if sqlstatus < 0
        return (sqlstatus)
  end

  return (0)

end	/* procedure temp_table_ver */


/** -------------------------------------------------------------------------
-	Procedure	: temp_table_fuellen
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Insert in temp_table		
-			  
-	Aufruf in proc  : wert_zuordnung
------------------------------------------------------------------------- **/
procedure temp_table_fuellen

field str1  char (1)

 init insert_str1
 init insert_str2
 init insert_str3

  let str1 = "\""
  perform str_zuordnung      

  let insert_str1 =
"insert into " # clipped(tmp_name) # " (gew_tmp, kopf_1, kopf_2, kopf_3, kopf_4, sp_fil_ek, sp_hk_tkost, ztr, kopf_5)"       /*------------- #894/#1646----*/

  let insert_str2 =
                " values ( " #
		clipped (zwmeng_anz) # "," #  
		str1 # clipped (zwkopf_1) # str1 # "," #
		str1 # clipped (zwkopf_2) # str1 # "," #
		str1 # clipped (zwkopf_3) # str1 # "," #
		str1 # clipped (zwkopf_4) # str1 # "," 

  let insert_str3 = 
		clipped (zwvk_pr) # "," #
		clipped (zwlad_pr) # "," #
		str1 # clipped (zwdate) # str1 # "," #
		str1 # clipped (zwkopf_5) # str1 # ")" /*--------- #894 -----*/

#INCLUDE "unset_sq"
  execute from sql
	end
	field insert_str1
	field insert_str2
	field insert_str3
#INCLUDE "set_sql"

  if sqlstatus < 0
        return (sqlstatus)
  end

  return (0)

end	/* procedure temp_table_fuellen 	*/


/** -------------------------------------------------------------------------
-	Procedure	: drop_table 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : tmp_table deleten und dropen		
-			  
-	Aufruf in proc  : monitor_ausgabe, druck_ausgabe
------------------------------------------------------------------------- **/
procedure drop_table
 
  let delete_str =
"delete from " # clipped(tmp_name) 

  let drop_str =
"drop table " # clipped(tmp_name) 

#INCLUDE "unset_sq"
  execute from sql
	end
	field delete_str

  execute from sql
	end
	field drop_str


#INCLUDE "set_sql"

end	/* procedure drop_table	*/


/** -------------------------------------------------------------------------
-	Procedure	: wert_zuordnung
-
-	In		: -
-
-	Out		: R}ckkehrcode
-			  
-	Beschreibung    : Werte selektieren ind in tmp_table einf}gen
-			  
-	Aufruf in proc  : ausgabe_vorbereiten
------------------------------------------------------------------------- **/
procedure wert_zuordnung

  init anz_ds
  perform anz_anzeige (anz_ds)

  perform temp_table_ver returning (dret)
  if dret <> 0
	return (dret)
  end
  perform prep_cursor returning (dret)    
  if dret <> 0
	return (dret)
  end

  fetch cursor lief_ret

  while sqlstatus = 0

	perform adressen_fetch

        perform lsp_retp_fetch

	while sqlstatus = 0
		let a_bas.a_bz1 = ""
		close cursor art_bez
		fetch cursor art_bez
		let zwa_bez = a_bas.a_bz1
		perform hk_pruefen (zwa_bez) returning (zwa_bez)
		perform temp_table_fuellen returning (dret)
		if dret <> 0
			return (dret)
		end
	        perform lsp_retp_fetch	
/*----------------------- #894 A --------------------------------------------
		let lief_retba.dat = ""
------------------------- #894 E ------------------------------------------*/
	end

	close cursor lsp_curs
	close cursor retp_curs

	let anz_ds = anz_ds + 1
	perform anz_anzeige (anz_ds)
        fetch cursor lief_ret

  end

  perform loe_cursor

  return (0)

end	/* procedure wert_zuordnung	*/


/** -------------------------------------------------------------------------
-	Procedure	: ausgabe_vorbereiten
-
-	In		: -
-
-	Out		: R}ckkehrcode
-			  
-	Beschreibung    : Ausgabe des Format 74100 auf dem Bildschirm oder auf
-                         Drucker vorbereiten
-			  
-	Aufruf in proc  : monitor_ausgabe, druck_ausgabe
------------------------------------------------------------------------- **/
procedure ausgabe_vorbereiten

field report_str char (5)

  perform wert_zuordnung returning (dret)
  if dret
	return (dret)
  end
  let report_str = "74100"
  let temp_table_name = tmp_name
  perform frm_file_laden (report_str) returning (dret)
  if dret
	return (dret)
  end

  return (0)

end	/* procedure ausgabe_vorbereiten	*/


/** -------------------------------------------------------------------------
-	Procedure	: monitor_ausgabe
-
-	In		: Kunden- oder Filialauswertung
-
-	Out		: -
-			  
-	Beschreibung    : Ausgabe des Format 74100 auf dem Bildschirm
-			  
-	Aufruf in       : Include anz_ds_wa.rsi 
------------------------------------------------------------------------- **/
procedure monitor_ausgabe

parameter 
  field par smallint
end

  if par = 1                      /* Kundenauswertung */
	let and_part = " and lief_retba.kun_fil = 0"
  else
	let and_part = " and lief_retba.kun_fil = 1"
  end

  perform ausgabe_vorbereiten returning (dret)
  if dret
	perform druck_not_pos	
	return 
  end

  perform starte_report ( 1 2 1 2 0 ) returning (dret)
  if dret = 1 or dret > 3
	perform druck_not_pos	
	return
  end

  perform drop_table

end	/* procedure monitor_ausgabe	*/


/** -------------------------------------------------------------------------
-	Procedure	: druck_ausgabe
-
-	In		: Kunden- oder Filialauswertung
-
-	Out		: -
-			  
-	Beschreibung    : Ausgabe des Format 74100 }ber Drucker        
-			  
-	Aufruf in       : Include anz_ds_wa.rsi 
------------------------------------------------------------------------- **/
procedure druck_ausgabe

parameter 
  field par smallint
end

  if par = 1                      /* Kundenauswertung */
	let and_part = " and lief_retba.kun_fil = 0"
  else
	let and_part = " and lief_retba.kun_fil = 1"
  end

  perform ausgabe_vorbereiten returning (dret)
  if dret
	perform druck_not_pos	
	return 
  end

  perform starte_report ( 3 2 1 2 0 ) returning (dret)
  if dret = 1 or dret > 3
	perform druck_not_pos	
	return
  end

  perform drop_table

end	/* procedure druck_ausgabe	*/


