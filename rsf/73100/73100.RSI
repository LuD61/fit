/***
--------------------------------------------------------------------------------
-
-       BSA  Bizerba Software- und Automationssysteme GmbH
-       Vorstadtstr. 59, 7465 Geislingen, Tel.: 07433/2413
-
--------------------------------------------------------------------------------
-
-       Includename             :       73100.rsi  
-
-       Autor                   :       Ralf Moebius
-       Erstellungsdatum        :       22.08.91
-       Modifikationsdatum      :       
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-        #RM2    1.0    29.10.91        RM         Umsatz+SA-Retouren bei taegl.
-        #RM3    1.0    13.11.91        RM         Bezug: EK-Prozente auf EK-   
-                                                         Umsatz gesamt
-        #09     1.228  10.12.91        RM         Einfuehren sukz. temp_table  
-                                                       Namen
-        #126    1.230  17.12.91        RM         Prozentwertberechnung fuer
-                                                  VK-Wert bei Wechsel Kund/Fil
-        #290    1.300a 07.02.92        RM         Umlautbehandlung          
-        #429    1.301  19.02.92        RM         Erweiterung Listenkopf
-                                                  Bereich von ... bis       
-        #490    1.301  05.03.92        RM         Information ueber Belegung
-                                                  der Tabelle lief_retba
-        #522    1.301  11.03.92        RM         verbale Procedurebeschreibung
-        #561    1.301  30.03.92        RM         Jahr von 2- auf 4-stellig
-        #920    1.302  16.07.92        RM         Erweiterung der Abbuchung   
-                                                  auf Statistik-Jahrestabellen
-        #1037   1.302  10.09.92        RM         Erweiterung der Auswertung  
-                                                  auf Rankinggruppen und
-                                                  Gesamtumsatz
-        #1646   1.402   17.08.93       RM         Stellenerweiterung und Item-
-                                                  modifikation bei wastat_tmp
-        #1746   1.402   20.10.93       RM         Einfuehrung der Behandlung
-                                                  der Gruenen-Punkt-Gebuehr
-        #1827   1.402c   24.01.94      RM         Einfuehrung Kundenlieferungen
-                                                  ueber Filialen             
-        #1860   1.402c   26.01.94      RM         Modifikation %-Berechnung
-                                                  fuer Teilumsatz je Kunde
-                                                  (temp_table - Arbeit)        
-	#5000	$$version$$	09.03.98	JG	sonderabl.
--------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-                                                                           
-   Includebeschreibung:  Daten- und Datenbankvereinbarungen fuer            
-                         die Include 731X.rsi und den Sourcefile s73100.rsf 
-                                                                          
-                                                                         
-------------------------------------------------------------------------------
***/


/*----------------------- Datenbankvereinbarung -------------------------*/
database bws
  table mdn               end
  table fil               end
  table kun               end
  table adr               end
  table a_bas             end
  table a_tag_wa          end
  table a_wo_wa           end
  table a_mo_wa           end
  table a_jr_wa           end      /*--------------- #920 ----------------*/
  table wg_tag_wa         end
  table wg_wo_wa          end
  table wg_mo_wa          end
  table wg_jr_wa          end      /*--------------- #920 ----------------*/
  table ag                end
  table wg                end
  table hwg               end
  table rg                end      /*--------------- #1037 ---------------*/
  table rg_zuord          end      /*--------------- #1037 ---------------*/
  table ptabn             end
  table wastat_tmp        end
  table lief_retba        end                   /*------- #490 --------*/
end

/*
  table fil_bewg          end
  table del_wa            end
  table sys_inst          end
  table item              end
*/

/*----------------------- Datenvereinbarung ---------------------------*/
data
/*
+  field zwmdn           smallint      /* Zwischenspeicher fuer Mandant */
+  field zwfil           smallint      /* Zwischenspeicher fuer Filiale */
+  field zwkun           integer       /* Zwischenspeicher fuer Kunde   */
+  field zwfk            integer       /* Zwischenspeicher fuer Fil/Kun */
+  field zwa             decimal (13 0)/* Zwischenspeicher fuer Artikel */
+  field min_a           decimal (13 0)/* relevanter Min-Wert Artikelnr.*/
+  field min_ag          smallint      /* relevanter Min-Wert AG-Nummer */
+  field min_wg          smallint      /* relevanter Min-Wert WG-Nummer */
+  field min_hwg         smallint      /* relevanter Min-Wert HWG-Nummer*/
+  field zwag            smallint      /* Zwischenspeicher fuer AG      */
+  field zwwg            smallint      /* Zwischenspeicher fuer WG      */
+  field zwhwg           smallint      /* Zwischenspeicher fuer HWG     */
+  field mdn_von         smallint      /* ZwSp fuer Mandant-von         */
+  field mdn_bis         smallint      /* ZwSp fuer Mandant-bis         */
+  field fil_von         smallint      /* ZwSp fuer Filiale-von         */
+  field fil_bis         smallint      /* ZwSp fuer Filiale-bis         */
+  field kun_von         integer       /* ZwSp fuer Kunde_von           */
+  field kun_bis         integer       /* ZwSp fuer Kunde_bis           */
+  field es_wert         smallint      /* akt.Ebenenwert ag,wg,hwg      */
+  field ei_wert         integer       /* aktuelle Artikelnummer (lfd)  */
+  field meng_einh_num   smallint      /* Mengeneinheit aus a_bas       */
+  field zwname          char (16)     /* Kunden/Filialkurzname         */
+  field sum_ums         decimal (12 2)/* Summe Umsatz VK               */
+  field sum_ums_ek      decimal (12 2)/* Summe Umsatz EK    #RM3       */
+  field sum_ret         decimal (12 2)/* Summe Retouren VK  #RM2       */
+  field sum_sa          decimal (12 2)/* Summe SA       VK  #RM2       */
+  field meng_anz        char (1)      /* G: Gewichtart. H: Handartikel */
+  field mdn_name        char (16)     /* Mandantenkurzname             */
*/
/* #5001 A */
   field a1	decimal(13,0)
   field a2	decimal(13,0)

   field a1ums_vk	decimal(15,2)
   field a1me_vk	decimal(15,2)
   field a1me_vk_ret	decimal(15,2)
   field a1ums_vk_ret	decimal(15,2)
   field a2ums_vk	decimal(15,2)
   field a2me_vk	decimal(15,2)
   field a2me_vk_ret	decimal(15,2)
   field a2ums_vk_ret	decimal(15,2)

  field xgefunden	smallint


/* #500E A */


  field zwmdn         like mdn.mdn /* Zwischenspeicher fuer Mandant */
  field zwfil         like fil.fil /* Zwischenspeicher fuer Filiale */
  field zwkun         like kun.kun /* Zwischenspeicher fuer Kunde   */
  field zwfk          like kun.kun /* Zwischenspeicher fuer Fil/Kun */
  field zwa           like a_bas.a /* Zwischenspeicher fuer Artikel */
  field min_a         like a_bas.a /* relevanter Min-Wert Artikelnr.*/
  field min_ag        like a_bas.ag /* relevanter Min-Wert AG-Nummer */
  field min_wg        like a_bas.wg /* relevanter Min-Wert WG-Nummer */
  field min_hwg       like a_bas.hwg /* relevanter Min-Wert HWG-Nummer*/
  field zwag          like a_bas.ag /* Zwischenspeicher fuer AG      */
  field zwwg          like a_bas.wg /* Zwischenspeicher fuer WG      */
  field zwhwg         like a_bas.hwg /* Zwischenspeicher fuer HWG     */
  field zwrg          like rg.rg     /* Zwischenspeicher fuer RG #1037 */
  field mdn_von       like mdn.mdn /* ZwSp fuer Mandant-von         */
  field mdn_bis       like mdn.mdn /* ZwSp fuer Mandant-bis         */
  field fil_von       like fil.fil /* ZwSp fuer Filiale-von         */
  field fil_bis       like fil.fil /* ZwSp fuer Filiale-bis         */
  field kun_von       like kun.kun /* ZwSp fuer Kunde_von           */
  field kun_bis       like kun.kun /* ZwSp fuer Kunde_bis           */
  field meng_einh_num like a_bas.me_einh /* Mengeneinheit aus a_bas       */
  field zwname        like kun.kun_krz1 /* Kunden/Filialkurzname         */
  field sum_ums       like wg_mo_wa.ums_vk_mo /* Summe Umsatz VK              */
  field sum_ums_ek    like wg_mo_wa.ums_fil_ek_mo /* Summe Umsatz EK  #RM3    */
  field sum_ret       like wg_mo_wa.ums_ret_mo /* Summe Retouren VK  #RM2     */
  field sum_sa        like wg_mo_wa.ums_vk_sa_mo /* Summe SA VK  #RM2     */
  field meng_anz      like ptabn.ptwert /* G: Gewichtart. H: Handartikel */
  field mdn_name      like adr.adr_krz /* Mandantenkurzname             */

  field zwnum         like a_bas.a     /* ZwSp fuer Verdichtungsebene   */
  field zwnr_se       like a_bas.a     /* ZwSp fuer zw_sum_nr           */
  field zwadr_str     like wastat_tmp.zw_sum_txt /* ZwSp fuer Kopf_4    */

  field zwjahr          smallint      /* Zwischenspeicher fuer Jahr    */
  field zjahr           smallint      /* Zwischenspeicher fuer Jahr    */
  field zwmo            smallint      /* Zwischenspeicher fuer Monat   */
  field zwwo            smallint      /* Zwischenspeicher fuer Woche   */
  field zwdat           date          /* Zwischenspeicher fuer Datum   */
  field jahr_von        smallint      /* vierstelliges jahr_von        */
  field jahr_bis        smallint      /* vierstelliges jahr_bis        */
  field datum		date          /* Abspeicherung des Datums      */
  field l_datum		date          /* letzter Tag des Jahres        */
  field datum_von       date          /* Beginn der Auswertungsperiode */
  field datum_bis       date          /* Ende   der Auswertungsperiode */
  field datum_str	char (10)     /* Datumsstring                  */

  field dret            smallint      /* Rueckgabewert     #1827       */
  field ret             smallint      /* Rueckgabewert                 */
  field sql_stat        smallint      /* Zwischenspeicher fuer SQL-Stat*/
  field in_work         smallint      /* Transaktionsschalter 0/1      */
  field mdn_flag        smallint      /* mess_menue.mdn = 0 --> flag=1 */
/*------------------------ #1827 A ------------------------------------*/
  field fil_flag        smallint      /* mess_menue.fil = 0 --> flag=1 */
  field kuf_flag        smallint      /* Kundenauswertungen ueb.Filiale*/
  field sys_not_kuf     smallint      /* Systemparameterstatus KLuF    */
  field fsel_flag       smallint      /* Selektionsflag furt KLuF:     */
                                      /* 0: konkrete Fil.; 1: alle Fil.*/
/*------------------------ #1827 E ------------------------------------*/
  field alle_kund_flag  smallint      /* alle Kunden in Auswahl        */
  field jahr_flag       smallint      /* Frage, ob > 1999 oder nicht   */
  field dat_flag        smallint      /* Datumsart: Tag,Woche,Mon,Jahr */
  field fk_flag         smallint      /* Filial- oder Kundenselektion  */
  field zwfk_flag       smallint      /* ZwSp Filiale/Kunde            */
  field pvergl_flag     smallint      /* Periodenvergleich (J/N)       */
  field sum_flag        smallint      /* Summierung im Periodenint.J/N */
  field list_flag       integer       /* Listentypauswahl              */
  field list_flag1      integer       /* Listentypauswahl              */
  field moni_flag       smallint      /* Bildschirm oder Drucker       */
  field first_flag      smallint      /* Bearbeitungszustand Dat.interv*/
  field erst_flag       smallint      /* Flag fuer Erstdurchlauf       */
  field ausw_flag       smallint      /* Auswahlflag fuer Bearb.ebene  */
  field spezi_flag      smallint      /* Auswahlflag fuer Spezifikation #1037 */
  field md_text         char (39)     /* Bildschirm- oder Druckerliste */
  field jahr_falsch     smallint      /* Frage, ob Jahresintervall i.O.*/
  field selekt0         smallint      /* current field des Filial/Kund */
  field selekt1         smallint      /* current field des Menues1     */
  field selekt2         smallint      /* current field des Menues2     */
  field selekt3         smallint      /* current field Spezifik.menue #1037*/
  field men_ueb         char (39)     /* Listentypmenue-Ueberschrift   */
  field men_zei1        char (39)     /* Listentypmenue 1.Zeile        */
  field men_zei2        char (39)     /* Listentypmenue 2.Zeile        */
  field men_zei3        char (39)     /* Listentypmenue 3.Zeile        */
  field men_zei4        char (39)     /* Listentypmenue 4.Zeile        */
  field zw_str          char (4)      /* Hilfsstring      #561         */
  field report_str      char (5)      /* Listenformat                  */
  field out_range	smallint      /* Range-Ueberpruefung	       */
  field i               smallint      /* Laufvariable                  */
  field l               integer       /* Laufvariable #1860 ---> integ */
  field p               smallint      /* Laufvariable                  */
  field k               smallint      /* Pruefvariable                 */
  field fa1_flag        smallint      /* prepare Cursor J/N            */
  field fa2_flag        smallint      /* prepare Cursor J/N            */
  field fa3_flag        smallint      /* prepare Cursor J/N            */
  field fa4_flag        smallint      /* prepare Cursor J/N            */
  field fa5_flag        smallint      /* prepare Cursor J/N            */
  field fag1_flag       smallint      /* prepare Cursor J/N            */
  field fag2_flag       smallint      /* prepare Cursor J/N            */
  field fag3_flag       smallint      /* prepare Cursor J/N            */
  field fag4_flag       smallint      /* prepare Cursor J/N            */
  field fag5_flag       smallint      /* prepare Cursor J/N            */
  field fag6_flag       smallint      /* prepare Cursor J/N            */
  field fwg1_flag       smallint      /* prepare Cursor J/N            */
  field fwg2_flag       smallint      /* prepare Cursor J/N            */
  field fwg3_flag       smallint      /* prepare Cursor J/N            */
  field fwg4_flag       smallint      /* prepare Cursor J/N            */
  field fhwg1_flag      smallint      /* prepare Cursor J/N            */
  field fhwg2_flag      smallint      /* prepare Cursor J/N            */
  field fhwg3_flag      smallint      /* prepare Cursor J/N            */
  field fhwg4_flag      smallint      /* prepare Cursor J/N            */

  field ausstieg_flag   smallint                /*---------- #1860 ---------*/
  field sel_anz         integer       /* Selektionsanzahl    #1860     */
  field sel_eben        integer       /* Selektionsanzahl    #1860     */
/*------------------------- #09  A ------------------------------------------*/
  field q               smallint      /* Laufvariable                  */
  field first_fill      smallint      /* Cursor schon prepared J/N     */

  field tmp_name        char ( 20)    /* tmp_table-Name                    */
  field create_str1     char (255)    /* create-String                     */
  field create_str2     char (255)    /* create-String                     */
  field create_str3     char (255)    /* create-String                     */
  field create_str4     char (255)    /* create-String                     */
  field create_str5     char (255)    /* create-String                     */
  field create_str6     char (255)    /* create-String    #1827            */
  field insert_str1     char (255)    /* insert-String                     */
  field insert_str2     char (255)    /* insert-String                     */
  field insert_str3     char (255)    /* insert-String                     */
  field insert_str4     char (255)    /* insert-String                     */
  field insert_str5     char (255)    /* insert-String                     */
  field insert_str6     char (255)    /* insert-String                     */
  field insert_str7     char (255)    /* insert-String                     */
  field insert_str8     char (255)    /* insert-String                     */
  field insert_str9     char (255)    /* insert-String    #1827            */
  field delete_str      char ( 80)    /* delete-String                     */
  field drop_str        char ( 80)    /* drop-String                       */
/*------------------------- #126 A ------------------------------------------*/
  field lese_str1       char (255)    /* String zum Lesen des temp_tables  */
  field lese_str2       char (255)    /* String zum Lesen des temp_tables  */
  field modi_str        char (255)    /* String zum Lesen des temp_tables  */
/*------------------------- #126 E ------------------------------------------*/
/*------------------------- #09  E ------------------------------------------*/

/*------------------------- #1827 A -----------------------------------------*/
  field sys_par_exit    smallint  /* Status d. Systemparameters */
  field sys_par_wert    char(1)   /* Wert d. Systemparameters   */
  field sys_par_bez     char(32)  /* Bez. d. Systemparameters   */
/*------------------------- #1827 E -----------------------------------------*/

  field dvertrgrupp     smallint value 0        // 100310
  field dkubragrupp     smallint value 0        // 190514

  field from_par_int	smallint	/* #5000 */
  field mekumpar        smallint        /* 280208 */
  field mue_par_int	smallint	/* #5000 */
  field druckziel	smallint	/* #5000 */
  field ahwg		char ( 2 )	/* #5000 */
  field titel1_str      char ( 20)    /* Filial- oder Kundenauswertung #1037*/
  field zwrg_typ   like rg_zuord.rg_zuord /* ZwSp N - Art.Nr; G - AG-Nr. #1037*/
  field zwztr            like wastat_tmp.ztr    /*----------- #1037 ---------*/

/* Vereinbarungen, die auch im temp_table erscheinen		       */	
  field typus            like wastat_tmp.a_typ
  field anz_ret          like wastat_tmp.a_anz_ret
  field anz_ums          like wastat_tmp.a_anz_dec
  field anz_sa           like wastat_tmp.a_anz_sa 
  field del_status       like wastat_tmp.delstatus
  field meng_einh        like wastat_tmp.einh 
  field me_vk_son	 like wastat_tmp.gew_tmp	/* --- #5000 --- */
  field me_vk            like wastat_tmp.gew_tmp	/* --- #1646 --- */
  field me_ret_son       like wastat_tmp.gew_ret 	/* --- #5000 --- */
  field me_ret           like wastat_tmp.gew_ret 
  field me_vk_sa_son     like wastat_tmp.gew_sa 	/* --- #5000 --- */
  field me_vk_sa         like wastat_tmp.gew_sa 
  field kopf_mdn         like wastat_tmp.kopf_1 
  field fk_von_bis       like wastat_tmp.kopf_2 
  field dat_vb_str       like wastat_tmp.kopf_3 
  field adr_str          like wastat_tmp.kopf_4 
  field ber_str          like wastat_tmp.kopf_5     /*--------- #429 ---------*/
  field zwkost_kz        like wastat_tmp.kost_kz 
  field zwnummer         like wastat_tmp.nr_13 
  field zwbez            like wastat_tmp.pos_bz
  field zwpos_bz1        like wastat_tmp.pos_bz1 
  field proz_ums         like wastat_tmp.proz_ges
  field proz_ret         like wastat_tmp.proz_ret
  field proz_ret_fil     like wastat_tmp.proz_sp_fil_ek_ret
  field proz_sa          like wastat_tmp.proz_sa 
  field proz_sa_fil      like wastat_tmp.proz_sp_fil_ek_sa 
  field proz_fil_ek      like wastat_tmp.proz_sp_fil_ek 
  field proz_hk_tkost    like wastat_tmp.proz_sp_hk_tkost 
  field proz_hk_vkost    like wastat_tmp.proz_sp_hk_vkost 
  field proz_sk_tkost    like wastat_tmp.proz_sp_sk_tkost
  field proz_sk_vkost    like wastat_tmp.proz_sp_sk_vkost
  field sp_fil_ek        like wastat_tmp.sp_fil_ek 
  field sp_hk_tkost      like wastat_tmp.sp_hk_tkost
  field sp_hk_vkost      like wastat_tmp.sp_hk_vkost 
  field sp_sk_tkost      like wastat_tmp.sp_sk_tkost
  field sp_sk_vkost      like wastat_tmp.sp_sk_vkost
  field ums_fil_ek       like wastat_tmp.ums_fil_ek 
  field ums_vk           like wastat_tmp.ums_vk
  field ums_hk_tkost     like wastat_tmp.ums_hk_tkost
  field ums_hk_vkost     like wastat_tmp.ums_hk_vkost
  field ums_ret          like wastat_tmp.ums_ret 
  field ums_fil_ek_ret   like wastat_tmp.ums_fil_ek_ret
  field ums_vk_sa        like wastat_tmp.ums_vk_sa 
  field ums_fil_ek_sa    like wastat_tmp.ums_fil_ek_sa 
  field ums_sk_tkost     like wastat_tmp.ums_sk_tkost
  field ums_sk_vkost     like wastat_tmp.ums_sk_vkost
  field dat_str          like wastat_tmp.ztr
  field nr_se            like wastat_tmp.zw_sum_nr 
  field hwg_str          like wastat_tmp.zw_sum_txt 
  field gn_pkt_gbr_ges   like wastat_tmp.gn_pkt_gbr_ges   /*---- #1746 ---*/
  field zzfil            like wastat_tmp.fil /* bei KLuF       #1827          */
  field fil_kurzn        like wastat_tmp.lief /* bei KLuF      #1827          */
  field kluf_str         like wastat_tmp.kopf_6 /* bei KLuF    #1827          */

  field dat_dreh         like wastat_tmp.ztr    /*---------- #1860 ---------*/
  field zdat_dreh        like wastat_tmp.ztr    /*---------- #1860 ---------*/
end

/*********************** Kommentar zu wastat_tmp ***********************
  typus           /* Artikeltyp                    */
  anz_ums         /* Anzahl Umsatz                 */
  anz_ret         /* Anzahl Retouren               */
  anz_sa          /* Anzahl SA                     */
  del_status      /* delstatus                     */
  meng_einh       /* Mengeneinheit                 */
  me_vk           /* Menge Umsatz gesamt           */
  me_ret          /* Menge   Retouren              */
  me_vk_sa        /* Menge Umsatz   SA             */
  kopf_mdn        /* Mandantenummer und -kurzname  */
  fk_von_bis      /* String: Fil/Kun von ... bis   */
  dat_vb_str      /* String: Datum von .. bis      */
  adr_str         /* Adressnummer + Kurzname       */
  ber_str         /* Bereich vonn ... bis a,ag usw */       /*----- #429 ---*/
  zwkost_kz       /* Kostenkennzeichen fuer Artikel*/
  zwnummer        /* ZwSp fuer a, ag, wg, hwg      */
  zwbez           /* Bezeichnung a, ag, wg, hwg    */
  zwpos_bz1       /* ZwSp fuer a, ag, wg, hwg      */
  proz_ums        /* proz.Anteil des Ums.am Ums.ges*/
  proz_ret        /* proz.Anteil der Ret.am Ums.ges*/
  proz_ret_fil    /* proz.Anteil der Ret.zum EK    */
  proz_sa         /* proz.Anteil der SA  am Ums.ges*/
  proz_sa_fil     /* proz.Anteil der SA  zum EK    */
  proz_fil_ek     /* proz.Anteil des Ums.zum EK    */
  proz_hk_tkost   /* proz.Anteil HK Teilkosten     */
  proz_hk_vkost   /* proz.Anteil HK Vollkosten     */
  proz_sk_tkost   /* proz.Anteil SK Teilkosten     */
  proz_sk_vkost   /* proz.Anteil SK Vollkosten     */
  sp_fil_ek       /* Ges.umsatz - Ums.zum Fil. EK  */
  sp_hk_tkost     /* Ges.umsatz - Ums.Herst.kost TK*/
  sp_hk_vkost     /* Ges.umsatz - Ums.Herst.kost VK*/
  sp_sk_tkost     /* Ges.umsatz - Ums.Selbstkost TK*/
  sp_sk_vkost     /* Ges.umsatz - Ums.Selbstkost VK*/
  ums_fil_ek      /* Umsatz zum Filial EK          */
  ums_vk          /* Umsatz zum VK  Gesamtumsatz   */
  ums_hk_tkost    /* Umsatz HK Teilkosten          */
  ums_hk_vkost    /* Umsatz HK Vollkosten          */
  ums_ret         /* Umsatz Retouren               */
  ums_fil_ek_ret  /* Umsatz Retouren Filial-EK     */
  ums_vk_sa       /* Umsatz zum SK SA              */
  ums_fil_ek_sa   /* Umsatz zum EK Filial-SA       */
  ums_sk_tkost    /* Umsatz SK Teilkosten          */
  ums_sk_vkost    /* Umsatz SK Vollkosten          */
  dat_str         /* Datumsstring                  */
  nr_se           /* Nummer der Summenebene AG/WG/H*/
  hwg_str         /* Nummer + Kurzname WG oder HWG */
  gn_pkt_gbr_ges  /* Gruene-Punkt-Gebuehr          */      /*------ #1746 ----*/
  zzfil           /* Filialnummer fuer KLuF        */      /*------ #1827 ----*/
  fil_kurzn       /* Filialkurzname fuer KLuF      */      /*------ #1827 ----*/
  kluf_str        /* Filialselektion fuer KLuF     */      /*------ #1827 ----*/
**************************************************************************/
/*- zusaetlich, zur Datumsstringsortierung und nicht in wastat_tmp #1860 --*/
/*- dat_dreh wie wastat_tmp.ztr char (8)                                 --*/

constant

  BILDSCHIRM	value  1		/* #5000 */
  DATEI		value  2		/* #5000 */
  DRUCKER	value  3		/* #5000 */

  text0     value "          "            /* Loeschtexte                   */
  text1     value "                                         "      
  text2     value "Summieren im Periodenintervall:     (J/N)"                  
  text3     value "Tempor{res table wird aufgebaut. DS-Nr.: "                  
  text30    value "von"                                        /*--- #1860 ---*/
  text31    value "Verdichtung:                        "       /*--- #1860 ---*/
  text32    value "Ebenensummierung:                   "       /*--- #1860 ---*/
  text4     value "     keine Daten in Auswahl"
  text5     value "bis"
  text6     value "Woche von"
  text7     value "Monat von"
  text8     value "Jahr von"
  text9     value "Zeitraum vom"
  text10    value "von 0 bis 9999"         /*----------- #1827 -------------*/
  ftext1    value "Mandant"
  ftext2    value "Filiale"
  ftext3    value "Kunde  "
  ltext0    value "Zentrale       "
  ltext00   value "Hauptfiliale   "        /*---------- #1827 -------------*/
  ltext1    value "Filialen"
  ltext2    value "Kunden"
  ltext3    value "Artikel"
  ltext4    value "AG"
  ltext5    value "WG"
  ltext6    value "HWG"
  ltext7    value "Umsatz gesamt                                     "
  ltext8    value "  1   schmal einzel              ."
  ltext9    value "  2   schmal verdichtet          ."
  ltext10   value "  3   breit mit HK/SK einzel     ."
  ltext11   value "  4   breit mit HK/SK verdichtet ."
/*-------------------------- #1037 A ----------------------------------------*/
  ltext12   value "  1   Filialen  ."   
  ltext13   value "  2   Kunden    ."   
  ltext14   value "  1   Umsatz gesamt    ."           
  ltext15   value "  2   Hauptwarengruppe ."           
  ltext16   value "  3   Warengruppe      ."           
  ltext17   value "  4   Artikelgruppe    ."           
  ltext18   value "  5   Artikel          ."           
  ltext19   value "  6   RG Artikelgruppe ."           
  ltext20   value "  7   RG Artikel       ."           
  ltext21   value "  1   Filiale          ."           
  ltext22   value "  1   Kunde            ."           
  ltext23   value "  2   Umsatz gesamt    ."           
  ltext24   value "  2   Hauptwarengruppe ."           
  ltext25   value "  2   Warengruppe      ."           
  ltext26   value "  2   Artikelgruppe    ."           
  ltext27   value "  2   Artikel          ."           
  ltext28   value "  2   RG Artikelgruppe ."           
  ltext29   value "  2   RG Artikel       ."           
  ltext30   value "Hauptwarengruppe:"
  ltext31   value "Warengruppe:     "
  ltext32   value "Artikelgruppe:   "
  ltext33   value "Artikel:         "
  ltext34   value "RG Artikelgruppe:"
  ltext35   value "RG Artikel:      "
  ttext1    value "\R Auswahl \N"
  ttext2    value "\R Kundenauswertung \N"
  ttext3    value "\R Filialauswertung \N"
  ttext4    value " Ebenen "
  ttext5    value "\R Listentypen - Bildschirmauswertung \N"
  ttext6    value "\R  Listentypen - Druckerauswertung   \N"
  ttext7    value "\R  Spezifikation  \N"
  crg_typ1  value "N"
  crg_typ2  value "G"
  atext1    value "J"
  atext2    value "N"
/*-------------------------- #1037 E ----------------------------------------*/

  CSYS0    value "0"             /*-------------- #1827 ----------------------*/



end

procedure dummy_proc

    if "" = "@(#)  73100.rsi  	1.402d	14.05.98	by RM" end
								
end
