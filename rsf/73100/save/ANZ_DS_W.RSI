
/***
--------------------------------------------------------------------------------
-
-       BSA  Bizerba Software- und Automationssysteme GmbH
-       Vorstadtstr. 59, 7465 Geislingen, Tel.: 07433/2413
-
--------------------------------------------------------------------------------
-
-       Includename             :       anz_ds_wa.rsi  
-
-       Hauptprocedur           :       anz_liefretba     
-
-       Autor                   :       Ralf Moebius
-       Erstellungsdatum        :       05.03.92
-       Modifikationsdatum      :       
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       #522    1.301   11.03.92        RM         verbale Procedurebeschreibung
-       #526    1.301   17.03.92        RM         Druckeinbindung Belegung
-                                                  von table lief_retba
-       #624    1.301   09.04.92        RM         Modifikation Einstiegspunkt
-                                                  der Analyse table lief_retba
-       #894    1.302   16.06.92        RM         Erweiterung Fehleranalyse
--------------------------------------------------------------------------------
-
-       Includebeschreibung     :     - ermittelt Anzahl der Datensaetze, die
-                                       noch nicht abgebucht wurden (table
-                                       lief_retba), aber durch Lieferschein-/
-                                       Retourenpflege schon freigegeben wurden
-
--------------------------------------------------------------------------------
***/

/*---------------------- DATENVEREINBARUNGEN ---------------------------*/
data
  field rret           smallint /* R}ckgabeparameter  #526                    */
  field anz_null       integer  /* Anz. noch nicht eingebuchter DS status = 0 */
  field anz_eins       integer  /* Anz. nicht einbuchbarer #894 DS status <>0 */
  field count_null_str char (8) /* Anz. noch nicht eingebuchter DS als String */
  field count_eins_str char (8) /* Anz. nicht einbuchbarer      DS als String */
  field vtext2         char (56)/* variabler Inhalt von ctext2                */
  field vtext11        char (56)/* variabler Inhalt von ctext11               */
  field utext          char (32)/* ]berschriftstext     #526                  */
  field and_part       char (30)/* Selektion }ber Filialen oder Kunden   #526 */
end


/*-------------------- CONSTANTENVEREINBARUNGEN ------------------------*/
constant
  ctext0  value "Es sind von freigegebenen Lieferscheinen oder"
  ctext1  value "Es ist von freigegebenen Lieferscheinen oder"
  ctext2  value "Retouren "
  ctext3  value "Datensatz"
  ctext4  value "Datens{tze"
  ctext5  value "noch \Unicht\N eingebucht."
  ctext6  value "alle Datens{tze eingebucht."
  ctext7  value "Dadurch sind Bestandesf}hrung, Filialabrechnung"
  ctext8  value "und Statistik-Warenausgang zur Zeit noch unvoll-"
  ctext9  value "st{ndig. Ist die Einbuchung eingebunden, l{uft" 
  ctext10 value "diese zeitversetzt im Hintergrund."
  ctext11 value "Es gibt "
  ctext12 value "(Lieferscheine/Retouren), die" 
  ctext13 value "(Lieferschein oder Retoure), der" 
  ctext14 value "aus datentechnischen Gr}nden \Unicht\N eingebucht werden"
  ctext15 value "k|nnen! Bestandesf}hrung, Filialabrechnung und"
  ctext16 value "kann! Bestandesf}hrung, Filialabrechnung und"
  ctext17 value "Statistik-Warenausgang k|nnen damit unvollst{ndig"
  ctext18 value "bleiben!"
  ctext19 value "Bitte informieren Sie den Service!"  
  ctext20 value "Bestandesf}hrung, Filialabrechnung und Statistik-"
  ctext21 value "Warenausgang sind damit vollst{ndig!"
  ktext20 value "Bestandesf}hrung und Statistik-Warenausgang sind" /*-- #526 -*/
  ktext21 value "damit vollst{ndig!"                       /*--- #526 --------*/
  ctext22 value "\R weiter mit RETURN \N"
  ctext23 value "\R\B Bitte warten ... \N"
  ctext24 value "F7 - Bildschirmausgabe; F8 - Druckausgabe oder"  /*-- #526 --*/

  utext1  value "\R Abbuchungsanalyse - Filialen \N"   /*------ #526 ----*/
  utext2  value "\R Abbuchungsanalyse - Kunden \N"     /*------ #526 ----*/
end


/*------------------------- PROCEDUREN ---------------------------------*/

/*----------------------- Datenbankselektionen -------------------------*/
/** -------------------------------------------------------------------------
-	Procedure	: count_null 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Ermitteln der noch nicht eingebuchten Datensaetze 
-                         mit delstat = 0 
------------------------------------------------------------------------- **/
procedure count_null

    if "" = "@(#) anz_ds_wa.rsi	1.400	16.06.92	by RM" end

  prepare cursor curs_null from sql
	select count(*) #anz_null from lief_retba
	where lief_retba.delstatus = 0
	end
	field and_part     
				/*-------------- #526 ---------------*/

  fetch cursor curs_null
  close cursor curs_null
  erase cursor curs_null

end	/* procedure count_null	*/


/** -------------------------------------------------------------------------
-	Procedure	: count_eins 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Ermitteln der nicht einbuchbaren Datensaetze mit 
-                         delstat <> 0  #894
------------------------------------------------------------------------- **/
procedure count_eins

/*------------------- #894 A ----------------------------------------------
	where lief_retba.delstatus = 1
*/
  prepare cursor curs_eins from sql
	select count(*) #anz_eins from lief_retba
	where lief_retba.delstatus <> 0
	end
	field and_part     
				/*-------------- #526 ---------------*/
/*------------------ #894 E ---------------------------------------------*/

  fetch cursor curs_eins
  close cursor curs_eins
  erase cursor curs_eins

end	/* procedure count_eins	*/


/** -------------------------------------------------------------------------
-	Procedure	: bitte_warten
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : 'Bitte warten ...' aufblenden waehrend der 
-                         Datenbankselektion  
------------------------------------------------------------------------- **/
procedure bitte_warten

  open window wind_bw ( 22, 78)
  set bordered window ( 3, 22, 9, 28)

  display pos (0, 1) value (ctext23)
  refresh

end	/* procedure bitte_warten */


/*----------------------- Texte aufblenden -----------------------------*/
/** -------------------------------------------------------------------------
-	Procedure	: taste_return
-
-	In		: Zeilenposition von 'Return'
-
-	Out		: -
-			  
-	Beschreibung    : 'Return' einblenden und auf Eingabe warten
------------------------------------------------------------------------- **/
procedure taste_return

parameter
  field z_pos smallint
end

  display pos ( z_pos, 34) value (ctext22)
  refresh

  init taste

  while taste <> KEYCR 
  	perform g_inkey
/*-------------------- #526 A -----------------------------------------*/
	if anz_eins > 0
		switch taste
			case KEY7
				close window okno
				perform disp_fkt ( 7 -1 )
				perform disp_fkt ( 8 -1 )
				perform disp_fkt ( 9 -1 )
				perform monitor_ausgabe (fk_flag)
				let taste = KEYCR
				break
			case KEY8
				close window okno
				perform disp_fkt ( 7 -1 )
				perform disp_fkt ( 8 -1 )
				perform disp_fkt ( 9 -1 )
				perform druck_ausgabe (fk_flag)
				let taste = KEYCR
				break
			case KEY9
				perform waehle_drucker returning (rret)
				let taste = KEY9  
				break
		end
	end
/*-------------------- #526 E -----------------------------------------*/
  end

end	/* procedure taste_return	*/


/*----------------------- #624 A -------------------------------------------
/** -------------------------------------------------------------------------
-	Procedure	: moment_1     
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : alle Datensaetze aus lief_retba schon eingebucht,
-                         kein delstatus <> 0  #894
------------------------------------------------------------------------- **/
procedure moment_1

  open window okno (22, 78)              /*------------- #526 ----------*/
  set bordered window (10, 56, 5, 12)
  display center title (utext)           /*------------- #526 ----------*/

  display pos ( 1, 1) value (ctext0)
  display pos ( 2, 1) value (ctext2)
  display pos ( 2,10) value (ctext6)
/*-------------------------- #526 A ------------------------------------*/
  if fk_flag    /* Kunden */
	  display pos ( 3, 1) value (ktext20)
	  display pos ( 4, 1) value (ktext21)
  else
	  display pos ( 3, 1) value (ctext20)
	  display pos ( 4, 1) value (ctext21)
  end
/*-------------------------- #526 E ------------------------------------*/

  perform taste_return (6)

/*------------------------- #526 A --------------------*/
  if taste = KEYCR
  	close window okno
	perform disp_fkt ( 7 -1 )    /*---------- #624 ---------*/
	perform disp_fkt ( 8 -1 )    /*---------- #624 ---------*/
	perform disp_fkt ( 9 -1 )    /*---------- #624 ---------*/
  end
  refresh
/*------------------------- #526 E --------------------*/

end	/* procedure moment_1	*/
------------------------- #624 E -----------------------------------------*/


/** -------------------------------------------------------------------------
-	Procedure	: moment_2     
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : alle Datensaetze aus lief_retba schon eingebucht,
-                         delstatus <> 0 vorhanden  #894 
------------------------------------------------------------------------- **/
procedure moment_2

/*-------------------- #526 A -----------------------------------------*/
  perform disp_fkt ( 7  9 )
  perform disp_fkt ( 8 14 )
  perform disp_fkt ( 9  9 )
  refresh
/*-------------------- #526 E -----------------------------------------*/

  open window okno (22, 78)                   /*--------- #526 -----*/
  set bordered window (12, 56, 4, 12)
  display center title (utext)           /*------------- #526 ----------*/

  let count_eins_str = anz_eins
  putstr (vtext11, clipped(count_eins_str), 9, 0)
  if anz_eins = 1
  	putstr (vtext11, ctext3, strlen (vtext11) + 2, 0)
  	putstr (vtext11, ctext13, strlen (vtext11) + 2, 0)
  else
  	putstr (vtext11, ctext4, strlen (vtext11) + 2, 0)
  	putstr (vtext11, ctext12, strlen (vtext11) + 2, 0)
  end
  display pos ( 1, 1) value (vtext11)
  display pos ( 2, 1) value (ctext14)
  if anz_eins = 1
  	display pos ( 3, 1) value (ctext16)
  else
  	display pos ( 3, 1) value (ctext15)
  end
  display pos ( 4, 1) value (ctext17)
  display pos ( 5, 1) value (ctext18)
  display pos ( 6, 1) value (ctext19)
  display pos ( 7, 1) value (ctext24)  /*------------- #526 -----------*/

  perform taste_return (9)

/*------------------------- #526 A --------------------*/
  if taste = KEYCR
  	close window okno
	perform disp_fkt ( 7 -1 )    /*---------- #624 ---------*/
	perform disp_fkt ( 8 -1 )    /*---------- #624 ---------*/
	perform disp_fkt ( 9 -1 )    /*---------- #624 ---------*/
  end
  refresh
/*------------------------- #526 E --------------------*/

end	/* procedure moment_2	*/


/** -------------------------------------------------------------------------
-	Procedure	: moment_3     
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : noch ein o.mehrere Datensaetze in lief_retba , 
-                         kein delstatus <> 0 vorhanden   #894
------------------------------------------------------------------------- **/
procedure moment_3

  open window okno (22, 78)                   /*--------- #526 -----*/
  set bordered window (12, 56, 4, 12)
  display center title (utext)           /*------------- #526 ----------*/

  let count_null_str = anz_null
  putstr (vtext2, clipped(count_null_str), 10, 0)
  if anz_null = 1
  	display pos ( 1, 1) value (ctext1)
  	putstr (vtext2, ctext3, strlen (vtext2) + 2, 0)
  else
  	display pos ( 1, 1) value (ctext0)
  	putstr (vtext2, ctext4, strlen (vtext2) + 2, 0)
  end
  putstr (vtext2, ctext5, strlen (vtext2) + 2, 0)
  display pos ( 2, 1) value (vtext2)
  display pos ( 3, 1) value (ctext7)
  display pos ( 4, 1) value (ctext8)
  display pos ( 5, 1) value (ctext9)
  display pos ( 6, 1) value (ctext10)

  perform taste_return (8)

/*------------------------- #526 A --------------------*/
  if taste = KEYCR
  	close window okno
  end
  refresh
/*------------------------- #526 E --------------------*/

end	/* procedure moment_3	*/


/** -------------------------------------------------------------------------
-	Procedure	: moment_4     
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : noch ein o.mehrere Datensaetze in lief_retba,
-                         delstatus <> 0 vorhanden    #894
------------------------------------------------------------------------- **/
procedure moment_4

/*-------------------- #526 A -----------------------------------------*/
  perform disp_fkt ( 7  9 )
  perform disp_fkt ( 8 14 )
  perform disp_fkt ( 9  9 )
  refresh
/*-------------------- #526 E -----------------------------------------*/

  open window okno (22, 78)                   /*--------- #526 -----*/
  set bordered window (19, 56, 1, 12)
  display center title (utext)           /*------------- #526 ----------*/

  let count_null_str = anz_null
  let count_eins_str = anz_eins
  putstr (vtext2, clipped(count_null_str), 10, 0)
  if anz_null = 1
  	display pos ( 1, 1) value (ctext1)
  	putstr (vtext2, ctext3, strlen (vtext2) + 2, 0)
  else
  	display pos ( 1, 1) value (ctext0)
  	putstr (vtext2, ctext4, strlen (vtext2) + 2, 0)
  end
  putstr (vtext2, ctext5, strlen (vtext2) + 2, 0)
  display pos ( 2, 1) value (vtext2)
  display pos ( 3, 1) value (ctext7)
  display pos ( 4, 1) value (ctext8)
  display pos ( 5, 1) value (ctext9)
  display pos ( 6, 1) value (ctext10)

  putstr (vtext11, clipped(count_eins_str), 9, 0)
  if anz_eins = 1
  	putstr (vtext11, ctext3, strlen (vtext11) + 2, 0)
  	putstr (vtext11, ctext13, strlen (vtext11) + 2, 0)
  else
  	putstr (vtext11, ctext4, strlen (vtext11) + 2, 0)
  	putstr (vtext11, ctext12, strlen (vtext11) + 2, 0)
  end
  display pos ( 8, 1) value (vtext11)
  display pos ( 9, 1) value (ctext14)
  if anz_eins = 1
  	display pos (10, 1) value (ctext16)
  else
  	display pos (10, 1) value (ctext15)
  end
  display pos (11, 1) value (ctext17)
  display pos (12, 1) value (ctext18)
  display pos (13, 1) value (ctext19)
  display pos (14, 1) value (ctext24)  /*------------- #526 -----------*/

  perform taste_return (16)

/*------------------------- #526 A --------------------*/
  if taste = KEYCR
  	close window okno
	perform disp_fkt ( 7 -1 )    /*---------- #624 ---------*/
	perform disp_fkt ( 8 -1 )    /*---------- #624 ---------*/
	perform disp_fkt ( 9 -1 )    /*---------- #624 ---------*/
  end
  refresh
/*------------------------- #526 E --------------------*/

end	/* procedure moment_4	*/


/*-----------------------------------------------------------------------*/
/*-----------------------------------------------------------------------*/
/*---------------------- Hauptprocedure ---------------------------------*/
/*-----------------------------------------------------------------------*/
/*-----------------------------------------------------------------------*/
/** -------------------------------------------------------------------------
-	Procedure	: anz_liefretba
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Hauptprocedure
------------------------------------------------------------------------- **/
procedure anz_liefretba

  let vtext2  = ctext2
  let vtext11 = ctext11

  perform bitte_warten

/*---------------------- #526 A -----------------------------------------*/
  if fk_flag                      /* Kundenauswertung */
	let utext = utext2
	let and_part = " and lief_retba.kun_fil = 0"
   else
	let utext = utext1
	let and_part = " and lief_retba.kun_fil = 1"
  end
/*---------------------- #526 E -----------------------------------------*/

  perform count_null
  perform count_eins

  close window wind_bw
  refresh

  if anz_eins = 0 and anz_null = 0
/*---------------------- #624 A ------------------------------------
  	perform moment_1
------------------------ #624 E ----------------------------------*/
        return
  end

  if anz_eins = 0 
  	perform moment_3
        return
  end

  if anz_null = 0
  	perform moment_2
        return
  end

  perform moment_4

end	/* procedure anz_liefretba	*/
