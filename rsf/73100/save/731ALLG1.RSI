
/***
--------------------------------------------------------------------------------
-
-       BSA  Bizerba Software- und Automationssysteme GmbH
-       Vorstadtstr. 59, 7465 Geislingen, Tel.: 07433/2413
-
--------------------------------------------------------------------------------
-
-       Includename             :       731allg1.rsi
-
-       Hauptproceduren         :      
-
-       Autor                   :       Ralf Moebius
-       Erstellungsdatum        :       20.08.91
-       Modifikationsdatum      :       
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       #09     1.228   11.12.91        RM         Einfuehren eines sukzessiven
-                                                  temp_table-Namen
-       #428    1.301   19.02.92        RM         Stringbearbeitung zur 
-                                                  Sortierermoeglichung      
-       #429    1.301   19.02.92        RM         Erweiterung Listenkopf
-                                                  Bereich von ... bis       
-       #490    1.301   05.03.92        RM         Information ueber Belegung
-                                                  der Tabelle lief_retba
-       #522    1.301   11.03.92        RM         verbale Procedurebeschreibung
-       #593    1.301   09.04.92        RM         Warnung bei Seitenbreite > 80
-       #624    1.301   09.04.92        RM         Modifikation Einstiegspunkt
-                                                  der Analyse table lief_retba
-       #643    1.301   14.04.92        RM         Integration Modusanzeige
-                                                  entspr. Maskenrichtlinie
-       #890    1.302   25.05.92        RM         #593 r}ckg{ngig gemacht, da
-                                                  im Listengenerator integriert
-       #1037   1.302   10.09.92        RM         Erweiterung der Auswertung  
-                                                  auf Rankinggruppen und
-                                                  Gesamtumsatz
-       #1057   1.302   28.09.92        RM         [ndern Selektionskriterium  
-                                                  bei table adr (unique-Key auf
-                                                  Feld adr)               
-       #1130   1.302   06.11.92        RM         Erh|hung Stelligkeit bei
-                                                  Range-Pr}fung 
-	#1521   1.401   04.06.93        RM       Erweiterung Selektionskriterium
-                                                zur Eindeutigkeit der Kunden-
-                                                adresse              
-       #1860   1.402d   26.01.94       RM         Modifikation %-Berechnung
-                                                  fuer Teilumsatz je Kunde
-                                                  (temp_table - Arbeit)        
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-                                                
-  Includebeschreibung: Include zur Dateiarbeit und allg. Selektionen  
-                              - Fehlerbehandlung, Kommentare, Mitteilungen
-                              - Transaktionen                          
-                                              
--------------------------------------------------------------------------------
***/


/*------------------------- PROCEDUREN ----------------------------------*/
/** -------------------------------------------------------------------------
-	Procedure	: nachricht  
-
-	In		: Nachrichtennummer
-
-	Out		: -
-			  
-	Beschreibung    : Kommentarbehandlung mit ENTER - Bestaetigung
------------------------------------------------------------------------- **/
procedure nachricht

  parameter
    field nummer1   smallint
  end

    if "" = "@(#) 731allg1.rsi 	1.402d	16.02.94	by RM" end
								
  perform set_com_file (mess_menue.prog_nr)   /*------ #643 --------*/
  set  message section "RDMCOMMENT"
  perform disp_comm ( nummer1 0)
  refresh
  repeat 
	perform g_inkey
  until taste = KEYCR	
  perform disp_comm (  0 0 )
  refresh

end            /* procedure nachricht */


/** -------------------------------------------------------------------------
-	Procedure	: fehler      
-
-	In		: nummer2:  Fehlernummer
-                         text2  :  Zusatztext
-
-	Out		: -
-			  
-	Beschreibung    : Fehlerbehandlung              
------------------------------------------------------------------------- **/
procedure fehler    

  parameter
    field nummer2    smallint
    field text2      char (30)
  end

  perform set_com_file (mess_menue.prog_nr)   /*------ #643 --------*/
  set message section "RDMFEHLER" 
  display errortext ( nummer2, text2 )
  refresh
  pause (3)            
/*---------------------------- #09 A -------------------------------------*/
  perform drop_table
/*---------------------------- #09 E -------------------------------------*/
  perform programm_ende

end            /* procedure fehler */


/** --------------------- #1827 A -------------------------------------------
-	Procedure	: fehler_oa      
-
-	In		: nummer21:  Fehlernummer
-                         text21  :  Zusatztext
-
-	Out		: -
-			  
-	Beschreibung    : Fehlerbehandlung ohne Abbruch 
------------------------------------------------------------------------- **/
procedure fehler_oa    

  parameter
    field nummer21    smallint
    field text21      char (30)
  end

  perform set_com_file (mess_menue.prog_nr)  
  set message section "RDMFEHLER" 
  display errortext ( nummer21, text21 )
  refresh
  repeat 
	perform g_inkey
  until taste = KEYCR	
  perform disp_comm (  0 0 )
  refresh

end            /* procedure fehler_oa */
/*--------------------------- #1827 E --------------------------------------*/


/** -------------------------------------------------------------------------
-	Procedure	: hilfe       
-
-	In		: Hilfenummer
-
-	Out		: -
-			  
-	Beschreibung    : Hilfen                             
------------------------------------------------------------------------- **/
procedure hilfe     

  parameter
    field nummer3    smallint
  end

  perform set_hlp_file (mess_menue.prog_nr)   /*------ #643 --------*/
  set message section "HILFE" 
  display help ( nummer3 )
  refresh
/*------------------------ #624 A --------------------------------------
  perform anz_liefretba          /*----------------- #490 ------------*/
-------------------------- #624 E ------------------------------------*/

end            /* procedure hilfe */


/** -------------------------------------------------------------------------
-	Procedure	: kommentar  
-
-	In		: Kommentarnummer
-
-	Out		: -
-			  
-	Beschreibung    : Kommentarbehandlung ohne ENTER - Bestaetigung
------------------------------------------------------------------------- **/
procedure kommentar

  parameter
    field nummer4   smallint
  end

  perform set_com_file (mess_menue.prog_nr)   /*------ #643 --------*/
  set  message section "RDMKOMMENTAR"
  perform disp_comm ( nummer4 0)
  refresh

end            /* procedure kommentar */


/** -------------------------------------------------------------------------
-	Procedure	: kommentar_var
-
-	In		: text       : Ausgabetext
-                         nummer6    : Zahl hinter Ausgabetext
-
-	Out		: -
-			  
-	Beschreibung    : Kommentarbehandlung ohne ENTER - Bestaetigung 
-                         mit variabler Anzeige
------------------------------------------------------------------------- **/
procedure kommentar_var

  parameter
    field text     char (70)
    field nummer6   integer  
  end

field zahl_str  char (6)

  let zahl_str = nummer6

  open window kommentar_v ( 22, 80)
  set window (1 , 77, 22, 1)

  putstr (text, zahl_str, strlen (text) + 2, 0)
/*--------------------------- #1860 A ---------------------------------------*/
  if instring (text, text3, 1, 0) = 0    /* Ebenensummierung oder Verdichtung */
  	putstr (text, text30, strlen (text) + 2, 0)
  	if instring (text, text32, 1, 0) = 0         /* Verdichtung     */
	        if spezi_flag <> 2                   /* nur Verdichtung */
			let sel_eben = sel_anz
		end
  		putstr (text, sel_eben, strlen (text) + 2, 0) 
        else    putstr (text, sel_anz, strlen (text) + 2, 0) 
        end 
  end 
/*--------------------------- #1860 E ---------------------------------------*/
  display pos ( 0, 0 ) value (text)
  refresh

  close window kommentar_v

end            /* procedure kommentar_var */


/** -------------------------------------------------------------------------
-	Procedure	: mitteilung 
-
-	In		: Mitteilungstext
-
-	Out		: -
-			  
-	Beschreibung    : Mitteilungen mit Pause, ohne ENTER - Bestaetigung
------------------------------------------------------------------------- **/
procedure mitteilung 

  parameter
    field text_m char (46)
  end

  let text_m = "\R " # text_m # " \N"
  open window w_mitt (22,78)
  set bordered window ( 3, 36, 8, 21)
    display pos ( 0, 1) value (text_m)
    refresh
  pause (2)
  close window w_mitt

end            /* procedure mitteilung */


/** -------------------------------------------------------------------------
-	Procedure	: range_pr   
-
-       In              : par0  decimal ( 13, 0 )      Uebergabewert
-                         par1  decimal ( 13, 0 )      untere Grenze
-                         par2  decimal ( 13, 0 )      obere  Grenze
-
-	Out		: -
-			  
-	Beschreibung    : Range - Ueberpruefung		
------------------------------------------------------------------------- **/
procedure range_pr

/*------------------------ #1130 A --------------------------------------
parameter
  field par0  integer             /* Uebergabewert */
  field par1  integer             /* untere Grenze */
  field par2  integer             /* obere  Grenze */
end
*/
parameter
  field par0  decimal ( 13, 0 )      /* Uebergabewert */
  field par1  decimal ( 13, 0 )      /* untere Grenze */
  field par2  decimal ( 13, 0 )      /* obere  Grenze */
end
/*------------------------ #1130 E ---------------------------------------*/

  init out_range

  if par0 < par1 or par0 > par2
	perform nachricht (4)
	let out_range = 1
  end

end	/* procedure range_pr	*/
 

/** ----------------- #1827 A -----------------------------------------------
-	Procedure	: fil_kurzname
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Selektion von Filialkurznamen bei KLuF 
------------------------------------------------------------------------- **/
procedure fil_kurzname           

  init fil_kurzn   

  if kuf_flag = 0 or fk_flag = 0
	return
  end

  execute from sql
  select #adr.adr_krz from adr, fil
  	where fil.fil        = $zzfil  
          and fil.mdn        = $zwmdn 
          and fil.adr        = adr.adr      
  end

  if sqlstatus = 0
	let fil_kurzn = adr.adr_krz
  end

end	/* procedure fil_kurzname */
/*------------------------ #1827 E ----------------------------------------*/

 
/** -------------------------------------------------------------------------
-	Procedure	: fk_kurzname
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Selektion von Kunden/Filialkurznamen 
------------------------------------------------------------------------- **/
procedure fk_kurzname           

  if fk_flag			/* Kundenintervall			*/

        init kun.kun_krz1

/*----------------------- #1521 ------------------------------------*/
  	prepare cursor fk_kurz from sql
         	select #kun.kun_krz1
			from kun
		where   kun.mdn        = $zwmdn  and
		        kun.fil        > -1      and
			kun.kun        = $zwfk      
		end

  	if sqlstatus < 0
		close cursor fk_kurz
		erase cursor fk_kurz
		perform fehler (10, "kun")
  	end

  else                       	/* Filialintervall			*/

        init adr.adr_krz 

  	prepare cursor fk_kurz from sql
         	select #adr.adr_krz, #fil.fil
			from adr, fil
		where fil.fil        = $zwfk         and
		      fil.mdn        = $zwmdn        and
	      	      fil.adr        = adr.adr      
		end

/*---------------------------- #1057 A ---------------------------------------
	      	      fil.adr        = adr.adr       and
	      	      adr.fil        = $zwfk         and
                      adr.mdn        = $zwmdn
------------------------------ #1057 E -------------------------------------*/
  	if sqlstatus < 0
		close cursor fk_kurz
		erase cursor fk_kurz
		perform fehler (10, "adr,fil")
  	end

  end

end	/* procedure fk_kurzname	*/


/*---------------------------- #1057 A ------------------------------------*/  
/** -------------------------------------------------------------------------
-	Procedure	: fetch_kurzname
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Selektion von Kunden/Filialkurznamen 
------------------------------------------------------------------------- **/
procedure fetch_kurzname           

  init adr.adr_krz, kun.kun_krz1
  close cursor fk_kurz
  fetch cursor fk_kurz

end	/* procedure fetch_kurzname */
/*---------------------------- #1057 E ------------------------------------*/  


/** -------------------------------------------------------------------------
-	Procedure	: fil_kun_nr_name
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Erstellung String fuer Filialen/Kundennummer 
-                         und -kurzbezeichnung
------------------------------------------------------------------------- **/
procedure fil_kun_nr_name

field zw_str   char ( 8)  


  let zw_str = zwfk
  /*------------------------ #428 A ------------------------------------*/
  init adr_str
  if fk_flag                /* Kunde */
	putstr (adr_str, zw_str, 9 - strlen (zw_str), 0)
  else  putstr (adr_str, zw_str, 5 - strlen (zw_str), 0)
  end
  /*------------------------ #428 E ------------------------------------*/
  putstr (adr_str, zwname, strlen (adr_str) + 3, 0)

end	/* procedure fil_kun_nr_name	*/


/*------------------------ #429 A ------------------------------*/
/** -------------------------------------------------------------------------
-	Procedure	: bereich_vb 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Erstellung String fuer Bereich von ... bis 
------------------------------------------------------------------------- **/
procedure bereich_vb

field wert_str1   char (13)
field bis_str1    char (3)         

  init ber_str     
  let bis_str1  = text5                  /*----- #1037 -------*/
  let wert_str1 = dwert_von
  putstr (ber_str, clipped(wert_str1), 2, 0)
  putstr (ber_str, bis_str1, strlen(ber_str) + 2, 0)
  let wert_str1 = dwert_bis
  putstr (ber_str, clipped(wert_str1), strlen(ber_str) + 2, 0)

end	/* procedure bereich_vb	*/
/*------------------------ #429 E ------------------------------*/


/** -------------------------------------------------------------------------
-	Procedure	: fil_kun_vb 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Erstellung String fuer Filiale/Kunde von ... bis
------------------------------------------------------------------------- **/
procedure fil_kun_vb

field wert_str   char (8)
field bis_str    char (3)         

  init fk_von_bis
  let bis_str  = text5                  /*----- #1037 -------*/
  let wert_str = dkund_von
  /*------------------------ #428 A ------------------------------------*/
  putstr (fk_von_bis, wert_str, 1, 0)
  putstr (fk_von_bis, bis_str, strlen(fk_von_bis) + 2, 0)
  let wert_str = dkund_bis
  putstr (fk_von_bis, wert_str, strlen(fk_von_bis) + 2, 0)
  /*------------------------ #428 E ------------------------------------*/

end	/* procedure fil_kun_vb	*/


/** --------------------- #1827 A -------------------------------------------
-	Procedure	: kluf_vb 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Erstellung String fuer Filialintervall bei KLuF
------------------------------------------------------------------------- **/
procedure kluf_vb

field hstr1 char (4)

  init kluf_str
  if kuf_flag = 0
	return
  end

  let kluf_str = zzfil
  putstr (kluf_str, fil_kurzn, strlen (kluf_str) + 2, 0)
  move kluf_str to fil_kurzn

  if  fsel_flag
	move text10 to kluf_str
  end

end	/* procedure kluf_vb	*/
/*--------------------------- #1827 E --------------------------------------*/


/** -------------------------------------------------------------------------
-	Procedure	: ausstieg_jn
-
-	In		: -
-
-	Out		: temp_table-Fuellen abbrechen J/N
-			  
-	Beschreibung    : Abbruch bei temp_table fuellen              
------------------------------------------------------------------------- **/
procedure ausstieg_jn

  init ausstieg_flag                 /*--------------- #1860  ----------*/
  if KEYSTATUS = KEY11
     	perform disp_fkt ( 11 -1 )
	perform drop_table    
        let ausstieg_flag = 1        /*--------------- #1860  ----------*/
	return (1)
  end
  return (0)

end          /* procedure ausstieg_jn */


/*--------------------- #890 A ----------------------------------------------
/*--------------------- #593 A --------------------------------------------*/
/** -------------------------------------------------------------------------
-	Procedure	: druck_breite
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : warnt bei Druckbreite gr|~er als 80 Zeichen  
-			  r}ckg{ngig gemacht, explizit im Listengenerator
------------------------------------------------------------------------- **/
procedure druck_breite
 
/* return bei Listen schmaler als 80 Zeichen */
  if moni_flag = 1 or list_flag = 0
	return
  end

  if (ausw_flag = 1 and list_flag = 1) or            /* Formate 73140, 73190 */
     (ausw_flag = 1 and list_flag = 2) or            /* Formate 73141, 73191 */
     (ausw_flag = 1 and list_flag = 4 and fk_flag = 0) or  /* Format 73193 */
     (ausw_flag = 2 and list_flag = 2 and fk_flag = 1) or  /* Format 73131 */
     (ausw_flag = 4 and list_flag = 2 and fk_flag = 0)     /* Format 73161 */
	return
  end

  perform disp_fkt ( 9 9 )
  perform set_com_file (mess_menue.prog_nr)   /*------ #643 --------*/
  set  message section "RDMCOMMENT"
  perform disp_comm ( 5 0)
  refresh
  repeat 
	perform g_inkey
  until taste = KEYCR or taste = KEY9
  perform disp_comm (  0 0 )
  perform disp_fkt ( 9 -1 )
  refresh
  if taste = KEY9
	perform disp_akt (68)                  /*-------- #643 ---------*/
	perform waehle_drucker returning (ret)
	perform disp_akt (22)                  /*-------- #643 ---------*/
  end
  let SYSKEY = KEYCR 

end	/* procedure druck_breite	*/
/*--------------------- #593 E --------------------------------------------*/
----------------------- #890 E --------------------------------------------*/

