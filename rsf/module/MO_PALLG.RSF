/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_pallg
-
-	Autor			:	Adolf Kraushaar /Juergen Simmchen
-	Erstellungsdatum	:	01.02.90
-	Modifikationsdatum	:	29.09.90
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    01.02.90    Kraushaar	Modularisierung
-	#2      1.00    08.03.90    Simmchen	Modularisierung
-	#3	1.00	30.08.90    Simmchen    Fehler bei Loeschen auf letzter
-						Zeile 
-	#4	1.00	28.09.90    B.Menge     CATERR gehoert in mo_cons
-	#5	1.00	29.10.90    Simmchen    pageup, pagedown           
-	#6	1.00	27.11.90    Szilagyi    insert line                
-	#7	1.00	10.01.91    Simmchen    Fehler bei Datentyp dconoff
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:  Proceduren fuer die Positionsverarbeitung
-
------------------------------------------------------------------------------
*/

module mo_pallg

export
	proc poserase
	proc poslesen
	proc posverlassen
	proc daschlue_cr_mamain2
	proc dbtocat
	proc cattodb
	proc catdelete
	proc dnext_mamain2
	proc dprev_mamain2
	proc anfuegen
	proc showpos
	proc poswechsel
	proc anzeigendown
	proc anzeigenup
	proc anzeigencompress
	proc pagedown
	proc pageup
end

import module mo_caall
end

import module mo_dbspe
	proc dispformpos
	proc position_einfuegen
	proc positionen_loeschen
	proc fetchscrollpos
	proc initformpos
	proc inittablepos
	proc recreaddefpos
end

import module mo_dball
	proc rollbacktrans
end

import module mo_caspe
	proc cmpkeypos
	proc backkeypos
	proc savekeypos
	proc initkeypos
	proc concat_insert
	proc concat_append
	proc movetocat
	proc movetodb
	proc relationpos
	proc ptabpos
	proc tstposschluessel
end

import module mo_dapos
end

import module mo_cons
end

import module mo_meld
	proc disp_sql
	proc disp_msg
	proc set_com_file 
end

IMPORT module mo_ma_ut
	proc disp_akt
END

/*--- #4 Diese Konstante gehoert in mo_cons ---
CONSTANT 
 	CATERR value 150
end
     --- Diese Konstante gehoert in mo_cons ---*/

/*---------------------------------------------------------------------------
-									  
-	Procedure	: poserase
-
-	In		: - 
-
-	Out		: -
-			  
-	Beschreibung	: Positionsbereich loeschen.
-							
---------------------------------------------------------------------------*/

PROCEDURE poserase
	erase (dbegzeil dendzeil-dbegzeil+danzzeil)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: showpos
-
-	In		: - 
-
-	Out		: -
-			  
-	Beschreibung	: Positionsaetze anzeigen
-							
---------------------------------------------------------------------------*/

PROCEDURE showpos

	field dsqlstatus smallint 
	field dret smallint 

	let drpzeil = dbegzeil
	perform poserase 

	let dsqlstatus = proc fetchscrollpos(CRDFIRST)
	if dsqlstatus
		if dsqlstatus = CENSQL100 return end
		perform disp_sql(dsqlstatus)
		return 
	end

	repeat
		perform movetocat

		perform relationpos returning (dret)
		perform ptabpos returning (dret)

		perform dispformpos (drpzeil drpspal)
		let drpzeil = drpzeil + danzzeil

		let dsqlstatus = proc fetchscrollpos(CRDNEXT)
		if dsqlstatus <> 0 and dsqlstatus <> CENSQL100
			perform disp_sql(dsqlstatus)
			return 
		end

	until dsqlstatus = CENSQL100 or drpzeil > dendzeil
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: poslesen
-
-	In		: dflglock 
-
-	Out		: -
-			  
-	Beschreibung	: Concatsaetze nacheinander lesen und anzeigen 
-							
---------------------------------------------------------------------------*/

PROCEDURE poslesen

PARAMETER
	field dflglock smallint 
END
field daktfile char(65)

	perform dbtocat 

/*----- Concatsaetze nacheinander lesen und anzeigen ------------------*/

	perform firstconcat
	let dcatstatus = catstatus 
	if not dcatstatus
		perform anzeigendown
		let dcatfirst = CFALSE
	else

/*------------- Wenn keine Saetze da => auf ANFUEGEN schalten ----------*/

		perform poserase
		perform defpositab
		perform initkeypos
		let dcatfirst = CTRUE
		let dflgposmod = CKPANFUEGEN
	end

	if dflglock = CTRUE

/*------------- Wenn Datensatz gesperrt nur ANZEIGEN zulassen ----------*/

		environment ("BWSMESG",daktfile)
		putstr (daktfile, "/"     , strlen(daktfile)+1,1)
		putstr (daktfile, "akt_me.cmg", 0 ,0)			
		set message file daktfile			       

		perform disp_akt (message(3))
		perform set_com_file (arg(0))
		select window wiverb
		let dedimod = CKANZEIGEN
		let dflgposmod = CKPANZEIGEN
	end
	return 
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: posverlassen
-
-	In		: dmenue 
-			  dflglock
-
-	Out		: -
-			  
-	Beschreibung	: Positionsbearbeitung abschliessen 
-							
---------------------------------------------------------------------------*/

PROCEDURE posverlassen

PARAMETER 
	field dmenue smallint 
	field dflglock smallint
END

field dsqlstatus smallint 

	if syskey = KEY5

/*------------- Nur Concat loeschen ---------------------------------------*/

		perform catdelete
		return
	end

	if syskey = KEY6 
		if dmenue <> CKANZEIGEN

				if dflglock = CTRUE

/*------------------------------------- Nur Concat loeschen ---------------*/

					perform catdelete
				else

/*------------------------------------- Positionssaetze in Datenbank ------*/

					let dsqlstatus = proc positionen_loeschen()
					perform cattodb
				end
		else

/*----------------------------- Nur Concat loeschen ---------------*/

				perform catdelete
		end
		return
	end
END

/*------------------------------------------------------------------------------
-
-	Procedure		: dbtocat 
-
-	In			: -
-
-	Out			: -
-			  
-	Modulbeschreibung	:  Bearbeitung des Trigger AFTER Schluessel-
-				   feld und KEYCR auf der Maske mamain2.
-
------------------------------------------------------------------------------
*/

PROCEDURE daschlue_cr_mamain2

field dstrange smallint
field dret smallint

	if dflgposmod = CKPANZEIGEN 
		return(CREPEAT)
	end

/*----- Test ob sich der Schlueselwert geaendert hat --------------------*/

	if dflgposmod = CKPMODIFIZIEREN
		let dret = proc cmpkeypos()
		if dret = CFALSE
			perform disp_msg(CENBED001,1)
			perform backkeypos
			return(CREPEAT)
		end
	end

/*----- Positionsschluessel testen ----------------------------------------*/

	if dflgposmod <> CKPMODIFIZIEREN 
		let dstrange = proc tstposschluessel()
		if dstrange = CFALSE
			perform disp_msg(CENBED008,1)
			return(CREPEAT)
		end
		perform dispformpos (drpzeil drpspal)
	end

/*----- Schluesselfeld verlassen ------------------------------------------*/

	let dposkey = CFALSE
	return(CCONTINUE)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: dbtocat 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest alle Positionsaetze
			 ueber den scrool  cursor cusmain2  und fuegt diese
			 als Kette in die Concatdatei
-							
---------------------------------------------------------------------------*/


PROCEDURE dbtocat

	field dsqlstatus smallint 

	let dsqlstatus = proc fetchscrollpos(CRDFIRST)
	if dsqlstatus
		if dsqlstatus = CENSQL100
			if dedimod = CKBEARBEITEN
				return 
			end
		else
			perform disp_sql(dsqlstatus)
		end
		return 
	end

	repeat
		perform movetocat
		let dcatstatus = proc concat_append() 
		if dcatstatus
			perform disp_msg (CATERR,1)
			return 
		end
		let dsqlstatus = proc fetchscrollpos(CRDNEXT)
		if dsqlstatus <> 0 and dsqlstatus <> CENSQL100
			perform disp_sql(dsqlstatus)
			return 
		end

	until dsqlstatus = CENSQL100

END

/*-------------------------------------------------------------------------
-
-	Operatorname		: cattodb     
-
-
-	In			: -
-
-	Out			: -
-			  
-	Beschreibung		: Die Procedure liest alle Positionssaetze
-				  aus der Concatdatei fuegt diese in die 
-				  Datenbank ein und loescht den Concatsatz.
-
-------------------------------------------------------------------------*/

PROCEDURE cattodb
	
	field dsqlstatus smallint 

	perform firstconcat
	let dcatstatus = catstatus 
	if dcatstatus
		if dcatstatus = 1
			return
		else
			perform disp_msg (CATERR,1)
			let dsqlstatus = proc rollbacktrans()
			return
		end
	end

	perform fetchconcat
	let dcatstatus = catstatus
	if dcatstatus
		perform disp_msg (CATERR,1)
		let dsqlstatus = proc rollbacktrans()
	end

	repeat
		perform movetodb

		let dsqlstatus = proc position_einfuegen ()
		if dsqlstatus return end

		perform deleteconcat
		let dcatstatus = catstatus
		if not catstatus
			perform fetchconcat
			let dcatstatus = catstatus
			if dcatstatus
				let dsqlstatus = proc rollbacktrans()
				perform disp_msg (CATERR,1)
				return
			end
		else
			if dcatstatus <> 1
				perform disp_msg (CATERR,1)
				return
			end
		end

	until dcatstatus <> 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: catdelete     
-
-
-	In			: -
-
-	Out			: -
-			  
-	Beschreibung		: Die Procedure loescht alle Positionssaetze
-				  aus der Concatdatei
-
-------------------------------------------------------------------------*/

PROCEDURE catdelete

	perform firstconcat
	let dcatstatus = catstatus
	if dcatstatus
		if dcatstatus = 1
			return
		else
			perform disp_msg (CATERR,1)
			return
		end
	end

	repeat
		perform deleteconcat
		let dcatstatus = catstatus
		if dcatstatus
			if dcatstatus <> 1
				perform disp_msg (CATERR,1)
				return
			end
		end

	until dcatstatus <> 0
END


/*---------------------------------------------------------------------------
-									  
-	Procedure	: dnext_mamain2
-
-	In		: -
-
-	Out		: dret
-			  
-	Beschreibung	: Bearbeitung Trigger KEYNEXT bei der Daten-
-				  eingabe der Maske mamain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE dnext_mamain2

field dconoff integer		/* #7 */
field dret smallint

	if dflgposmod = CKPANZEIGEN 
		return(CCONTINUE)
	end

/*----- Test ob sich der Schlueselwert geaendert hat --------------------*/

	if dflgposmod = CKPMODIFIZIEREN
		let dret = proc cmpkeypos()
		if dret = CFALSE
			perform disp_msg(CENBED001,1)
			perform backkeypos
			return(CREPEAT)
		end
	end

	if drpzeil = dendzeil

/*------------- aktuelle Position ist auf letzter Zeile ---------------*/

		let dconoff = catoffset
		perform nextconcat
		let dcatstatus = catstatus
		if dcatstatus

/*--------------------- Keine naechste Position vorhanden -------------*/

			perform  offsetconcat(dconoff)
			perform fetchconcat
			let dcatstatus = catstatus
			return(CREPEAT)
		end

/*------------- auf erste Zeile schalten und die naechsten Saetze anz.-*/

		let drpzeil = dbegzeil
                erase(drpzeil (dendzeil - drpzeil + danzzeil)) //#111199
		perform anzeigendown
		return(CBREAK)
	end

/*----- aktuelle Position ist nicht die 1. Zeile --------------------*/

	perform nextconcat
	let dcatstatus = catstatus
	if dcatstatus

/*------------- Keine naechste Position vorhanden --------------------*/

		perform lastconcat
		perform fetchconcat
		return(CREPEAT)
	end

/*----- naechste Position lesen und in neachster Zeile darstellen ---*/

	perform fetchconcat
	let drpzeil = drpzeil + danzzeil 
	return(CBREAK)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: dprev_mamain2
-
-	In		: -
-
-	Out		: dret
-			  
-	Beschreibung	: Bearbeitung Trigger KEYPREV bei der Daten-
-				  eingabe der Maske mamain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE dprev_mamain2

field dconoff integer		/* #7 */
field dret smallint

	if dflgposmod = CKPANZEIGEN or dflgposmod = CKPEINFUEGEN
		return(CCONTINUE)
	end

	if drpzeil = dbegzeil

/*------------- aktuelle Zeile = 1. Zeile --------------------------------*/

		perform prevconcat
		if catstatus

/*--------------------- keine Position mehr da ---------------------------*/

			perform firstconcat
			perform fetchconcat
			return(CREPEAT)
		end	

/*------------- auf letzte Zeile stellen und Positionen anzeigen ---------*/
/* adolf */

		let drpzeil = dendzeil
		perform anzeigenup
		return(CBREAK)
	end

/*----- aktuelle Zeile ist nicht 1. Zeile --------------------------------*/

	perform prevconcat
	let drpzeil = drpzeil - danzzeil 
	perform fetchconcat
	return(CBREAK)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: anfuegen
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Neue Position anfuegen.
-							
---------------------------------------------------------------------------*/

PROC anfuegen

/*----- Maske initialisieren und ausgeben ------------------------------*/

	let drpzeil = drpzeil + danzzeil
	if drpzeil > dendzeil
		perform poserase
		let drpzeil = dbegzeil
		perform anzeigendown
	else
		insert line (drpzeil danzzeil)
		delete line (dendzeil+1 danzzeil)
	end
	perform defpositab	
	perform initkeypos
	let dflgposmod = CKPANFUEGEN
	return
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: poswechsel
-
-	In		: dsavemodus 
-
-	Out		: dsavemodus
			  dret
-			  
-	Beschreibung	: Bereitet einen Positionswechsel fuer die Positions
-			  verarbeitung in Abhaengigkeit des aktuellen
-			  Verarbeitungsmodus vor
-			  Einfuegen, Loeschen, Anhaengen, Modifizieren
-			  in der Concat-Datei
-							
---------------------------------------------------------------------------*/

PROC poswechsel

PARAMETER
	field dsavemodus smallint
END

field dret smallint
field dstatus smallint

	if dposkey = CTRUE

/*------------- Schluesselwert testen -----------------------------------*/
		if dflgposmod = CKPANFUEGEN
			let dret = proc tstposschluessel()
			if dret = CFALSE
				perform disp_msg(CENBED008,1)
				return(CREPEAT dsavemodus)
			end 
			perform dispformpos (drpzeil drpspal)
		end

		if dflgposmod = CKPMODIFIZIEREN 

			let dret = proc cmpkeypos()
			if dret = CFALSE
				perform disp_msg(CENBED008,1)
				perform backkeypos
				return(CREPEAT  dsavemodus)
			end
			if dcatfirst = CTRUE
			/* noch kein Positionsatz vorhanden */
				return(CREPEAT dsavemodus)
			end
		end
	else
		let dcatfirst = CFALSE
	end


/*----w Entsprechend der Pflegefunktion Aktion durchfuehren ------------*/

	switch dflgposmod

		case CKPEINFUEGEN
			let dstatus = proc concat_insert()
			perform dispformpos (drpzeil drpspal)
			perform nextconcat
			let drpzeil = drpzeil + danzzeil
			if drpzeil > dendzeil
				let drpzeil = dbegzeil
				perform anzeigendown
			end
			insert line (drpzeil danzzeil)
			delete line (dendzeil+1 danzzeil)
			perform defpositab
			perform initkeypos
			break

		case CKPANFUEGEN
			let dstatus = proc concat_append()
			perform dispformpos (drpzeil drpspal)
			break

		case CKPLOESCHEN
			if dsavemodus <> CKPANFUEGEN		/* #3 */
				perform deleteconcat
				let dstatus = catstatus
			else
				let dstatus = 1			/* #3 */
			end
			if dstatus
				if dstatus <> 1
					perform disp_msg(CATERR,1)
					break
				else
					perform lastconcat
					let dsavemodus = CKPMODIFIZIEREN  /*#3*/
					let dstatus = catstatus
					if dstatus
						let dsavemodus =  CKPANFUEGEN
						let dcatfirst = CTRUE
						perform defpositab
						perform initkeypos
						break
					end
					if drpzeil = dbegzeil
						let drpzeil = dendzeil
						perform anzeigenup
					else
						delete line (drpzeil danzzeil)
						/* #6 */
						insert line (dendzeil danzzeil)
						let drpzeil = drpzeil - danzzeil
						perform fetchconcat
					end
					break
				end
			end
                        perform anzeigendown
			break

		case CKPMODIFIZIEREN
			perform updateconcat
			break

		otherwise
			break

	end

	return(CBREAK dsavemodus)
END	

/*-------------------------------------------------------------------------
-
-	Operatorname		: anzeigendown 
-
-
-	In			: -
-
-	Out			: -
-			  
-	Beschreibung		: Die Procedure zeigt ab einer Zeilen-
-				  position absteigend die naechsten Pos-
-				  itionen an.
-
-------------------------------------------------------------------------*/

PROCEDURE anzeigendown


	field drpzsav smallint
	field dconoff integer		/* #7 */

	let drpzsav = drpzeil
//        /*** weg wg. Dr. Watson
	erase (drpzeil dendzeil-drpzeil+danzzeil)
//        ***/
	let dconoff = catoffset
	perform fetchconcat
	perform dispformpos (drpzeil drpspal)
	while drpzeil < dendzeil
		perform nextconcat
		if catstatus
			let drpzeil = 1000
		else
			perform fetchconcat
			let drpzeil = drpzeil + danzzeil
			perform dispformpos (drpzeil drpspal)
		end
	end
	perform offsetconcat (dconoff)
	perform fetchconcat
	let drpzeil = drpzsav
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: anzeigenup
-
-
-	In			: -
-
-	Out			: -
-			  
-	Beschreibung		: Die Procedure zeigt ab einer Zeilen-
-				  position aufsteigend die naechsten Pos-
-				  itionen an.
-
-------------------------------------------------------------------------*/

PROCEDURE anzeigenup

	field drpzsav smallint
	field dconoff integer		/* #7 */

	let drpzsav = drpzeil
	let dconoff = catoffset
	perform fetchconcat
	perform dispformpos (drpzeil drpspal)
	while drpzeil > dbegzeil
		perform prevconcat
		if catstatus
			let drpzeil = -1000
		else
			perform fetchconcat
			let drpzeil = drpzeil - danzzeil
			perform dispformpos (drpzeil drpspal)
		end
	end
	perform offsetconcat (dconoff)
	perform fetchconcat
	let drpzeil = drpzsav
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: anzeigencompress
-
-
-	In			: -
-
-	Out			: -
-			  
-	Beschreibung		: Die Procedure kompremiert die Position-	
-				  anzeige wenn der Editiermodus ANFUEGEN
-				  oder EINFUEGEN war.
-
-------------------------------------------------------------------------*/

PROCEDURE anzeigencompress

	field dconoff integer		/* #7 */

	let dconoff = catoffset

	if dedimod = CKANZEIGEN
		delete line (drpzeil danzzeil)
		/* #6 */
		insert line (dendzeil danzzeil)
		perform nextconcat
		let dcatstatus = catstatus
		if dcatstatus
			perform  offsetconcat (dconoff)
			if drpzeil = dbegzeil
				let drpzeil = dendzeil
				perform anzeigenup
			else
				perform  fetchconcat
				let drpzeil = drpzeil - danzzeil
				perform anzeigendown
			end
		else
			perform anzeigendown
		end
	end

	if dedimod = CKBEARBEITEN
		perform anzeigendown
	end

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: pagedown
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: In der Positionsbearbeitung eine Seite 
-			  weiter blaettern
-							
---------------------------------------------------------------------------*/

proc pagedown

	field drpzsav smallint
	field dconoff integer		/* #7 */
	field dsave_offset integer		/* #7 */
	let drpzsav = drpzeil
	let drpzeil = dbegzeil

	let dconoff = catoffset 
	let dsave_offset = catoffset 
	while drpzeil <= dendzeil
		perform nextconcat
		if catstatus
			let drpzeil = 1000 
			/*if dflgposmod = CKPMODIFIZIEREN
				let dflgposmod = CKPANFUEGEN
			end */
		else
/* #5
			perform fetchconcat
*/
			let dconoff = catoffset
			if dendzeil - drpzsav = drpzeil - dbegzeil
				break
			end 
			let drpzeil = drpzeil + danzzeil
		end
	end
	if drpzeil = 1000
	/*     drpzeil nicht dendzeil */
		perform offsetconcat (dsave_offset)
		perform fetchconcat
		let drpzeil = drpzsav
	else
		perform offsetconcat (dconoff)
		perform fetchconcat
		let drpzeil = dbegzeil
	end
	perform anzeigendown
	return
end

/*---------------------------------------------------------------------------
-									  
-	Procedure	: pageup
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: In der Positionsbearbeitung eine Seite 
-			  zurueck blaettern
-							
---------------------------------------------------------------------------*/

proc pageup


	field drpzsav smallint
	field dconoff integer		/* #7 */

	let drpzsav = drpzeil
	if drpzsav > dbegzeil
		while drpzeil > dbegzeil
			perform prevconcat
			if catstatus 
				let drpzeil = -1000
			else
/* #5
				perform fetchconcat
*/
				let dconoff = catoffset
				let drpzeil = drpzeil - danzzeil
			end
		end
	end
	let drpzeil = dendzeil
	let drpzsav = drpzeil
	let dconoff = catoffset
	while drpzeil >= dbegzeil
		perform prevconcat
		if catstatus
			let drpzeil = -1000
		else
/* #5
			perform fetchconcat
*/
			let dconoff = catoffset
			/*if drpzeil < drpzsav - (dendzeil - dbegzeil)

				let dconoff = catoffset
				let drpzsav = drpzeil
			end */
			let drpzeil = drpzeil - danzzeil
		end
	end
	perform offsetconcat (dconoff)
	perform fetchconcat
	let drpzeil = dbegzeil
	perform anzeigendown
	if dflgposmod = CKPANFUEGEN
		let dflgposmod = CKPMODIFIZIEREN
	end
	return
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: defpositab
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure besetzt die Tabellenwerte mit
-				  den Defaultwerten 
-
-------------------------------------------------------------------------*/

PROCEDURE defpositab

field dsqlstatus smallint

	perform savekeypos

	perform initformpos
	perform recreaddefpos returning (dsqlstatus)
	if dsqlstatus 
		perform inittablepos
		perform disp_sql (dsqlstatus)
	end

	perform movetocat

	perform backkeypos
END

procedure what_kennung
if "" = "@(#) mo_pallg.rsf	1.400	05.11.91	by KB>" end
end
