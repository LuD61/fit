

module mo_dr20


/***

ACHTUNG : dieses Modul funktioniert bei aktiviertem re_deb_par nur korrekt,
        wenn Kunden- gleich Debitoren-Nummer ist

--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       mo_dr20.rsf
-
-       Exports                 :       proc redr20
-
-       Interne Prozeduren      :  	procedure druckintern
-					procedure kopf1druck
-					procedure kleinerkopf
-					procedure anzeliste
-					procedure listenpositionen
-					procedure anzesumme
-					procedure neueseite
-					procedure belegsumme
-					procedure belegkopf
-					procedure drustri 
-					procedure fetch_lg_temp_lesen
-					procedure warladen
-					
-
-       Autor                   :       JG
-       Erstellungsdatum        :       01.10.92
-       Modifikationsdatum      :      
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :    
-       Betriebssystem          :     
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version	Datum		Name	Beschreibung
-       ----------------------------------------------------------------
-	#1072	1.302	02.10.92	JG	Erstellung
-	#1143	1.400	23.11.92	JG	Daba-Aenderungen
-	#1431	1.401	05.04.93	KB	Erweiterung lief_me von 4 auf 5 Vorkommastellen
-	#1498	1.401	26.05.93	JG	lspt neu (tt anders)
-	#2405	2.000c	26.06.95	JG	frm_stz_par
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Module zur Rechnungs-Druckausgabe
-		 			(hart-ersatz fuer mo_report)
-				    organisiert spezifisches Ausgabeformat    
-				    "HESS" / Muenchweiler                      
- 
-	dr_par.lst_id		Kunde
-		1		standard
-		17		Kaltenbach
-		18		Gretenkord
-		19		Bindseil
-		20		Hess
WMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWM
WMW                                                                        MWM
WMW             Sondermodul fuer Firma HESS / Muenchweiler                 MWM
WMW                                                                        MWM
WMW  Layout modifiziert und zusaetzliche Ausgabe von Warengruppen-Summen   MWM
WMW                                                                        MWM
WMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWM
-
-
-			proc redr20
-				Exportprocedure, organisiert die Ausgabe
-				-  IN	: formulartyp
-						 56100 Netto-R. mit Rab.
-						 56101 Netto-Gutsch. mit Rab.
-						 56102 Netto-Barv. mit Rab.
-						 56105 Netto-R. ohne Rab.
-						 56106 Netto-Gutsch. ohne Rab.
-						 56107 Netto-Barv. ohne Rab.
-
-						 56110 Brutto-R. mit Rab.
-						 56111 Brutto-Gutsch. mit Rab.
-						 56112 Brutto-Barv. mit Rab.
-						 56115 Brutto-R. ohne Rab.
-						 56116 Brutto-Gutsch. ohne Rab.
-						 56117 Brutto-Barv. ohne Rab.
-					: silentprint
-						  0 = Normal
-						  1 = Positionen unterdrueckt
-				- OUT	: 0 = o.k.
-					: 1 = nicht erfolgreich
-			
-			proc druckintern	:Eigentliche Listenausgabe 
-			proc kopf1druck		:Kopf auf 1. Seite
-			proc kleinerkopf	:Kopf auf jeder Seite
-			proc anzeliste		:Ausgabe Listenteil
-			proc listenpositionen	:Ausgabe Listenpositionen
-			proc anzesumme		:Ausgabe Summenteil
-			proc neueseite		:Organisation neue Seite
-			proc belegsumme		:Ausgabe Belegsumme
-			proc belegkopf		:Ausgabe Belegkopf
-			proc drustri 		: *.str-Dateien abarbeiten
-			proc fetch_lg_temp_lesen :Satz aus temp-t. lesen
-			proc warladen		:warengruppen summieren
-
-
--------------------------------------------------------------------------------
##D2A
##D2E
##D3A
##D3E
##D4A
##D4E
##D5A
##D5E
##D6A
##D6E
##D7A
##D7E

***/


EXPORT
proc redr20
END

IMPORT module mo_teta5
END

IMPORT module mo_druck 
END

IMPORT module mo_men_m
END


IMPORT module mo_ptab
database bws
proc getptab
END

IMPORT module mo_koful
database bws
END

IMPORT module mo_sys_p        /* #2405 */
END


database bws
table adr end
table dr_par end
table a_bas  end
table lspt end	/* #1498 */
table retpt end	/* #1498 */
end

CONSTANT	
	CFALSE		value 0
	CTRUE		value 1
END
	

data

/* #2405 A */

field	par_int_wert	smallint
field	sys_par_wert	char(1)
field	sys_par_bez	char(10)

/* #2405 E */


/* -- #1072
	field absender  char( 50 )	/* string fuer absender */
---- */
	field spooler   char( 50 )	/* string fuer druckerpfad */
	field dret	smallint	/* returnwert von procedures */
	field line_merk smallint	/* merkzelle fuer LINECOUNTER */
	field needzeil	smallint	/* merkzelle fuer freien Fussplatz */
	field i smallint		/* stringlaenge + allg. var. */
	field dmenuefile	char (60)
	field dfusszeilc	char (80)
	
	field liste_typ smallint	/* schalter fuer Listenart */
	field silent_print smallint	/* Zeilenunterdr. */
	field hilfzeit	char ( 8 )	/* hilfszelle fuer Zeiterfassung */
	field zeike 	char (30 )	/* hilfszeile fuer Zeilenbezeichnung */

	field dseite	smallint	/* Seitennummerierung */
	field summe1	decimal (12,2)	/* Gesamtsumme */
	field summe2	decimal (12,2)	/* Belegsumme  */
	field summe3	decimal (12,2)	/* Rabattsumme */ 
	field gruppe 	smallint	/* lg_temp.ret - gruppung je beleg */

	field dszi_fett_ein	char ( 4 )
	field dszi_fett_aus	char ( 4 )
	field dszi_pica    	char ( 4 )
	field dszi_elite   	char ( 4 )
	field dszi_schmal_ein	char ( 4 )
	field dszi_schmal_aus	char ( 4 )


/* ---- fuer form kopf1 (Rechnungsadresse)  -----  */

	field jd3anr	char    ( 9 )
	field jd3nam1	like	adr.adr_nam1
	field jd3nam2	like	adr.adr_nam2
	field jd3strpf	char	( 8 )
	field jd3pf		like	adr.pf
	field jd3str		like	adr.str
	field jd3plzort		char (30)

/* -------- fuer form liste1 ------------------------------------------ */

	field jdnetto		decimal (12,2)


/* -------- fuer form fuss0 ------------------------------------------- */

	field j0wert decimal ( 12,2 )

/* -------- fuer form fuss2 ------------------------------------------- */

	field j2wert decimal ( 12,2 )
	field j2proz char (6)
	field j2kennz smallint

/* -------- fuer form liste3 -(Kopf- Fuss- und Positionstexte) --------- */

	field dzusatztext char ( 60 )

/* -------- fuer form fuss ( Gesamt-Fuss ) --------- */

  field  dwargr1 decimal (10,2)
  field  dwargr2 decimal (10,2)
  field  dwargr3 decimal (10,2)
  field  dwargr4 decimal (10,2)
  field  dwargr5 decimal (10,2)
  field  dwargr6 decimal (10,2)
  field  dwargr7 decimal (10,2)
  field  dwargr8 decimal (10,2)
  field  dwargr9 decimal (10,2)
  field	 dmwst2w decimal (11,2)	/* Warenwert mwst 2 */
  field	 dmwst2b decimal (11,2)	/* Steuerbetrag 2 */
  field	 dmwst1w decimal (11,2)	/* Warenwert mwst 1 */
  field	 dmwst1b decimal (11,2)	/* Steuerbetrag 1 */
  field	 dges1w  decimal (11,2)	/* Gsamt-Warenwert */
  field	 dges1b  decimal (11,2)	/* Gesamt-Steuer-Betrag */
  field  dgesges decimal (11,2) /* Rechnungs-Endbetrag */

/* -------------------------------------------------------------------- */

        field cursortext        char ( 100 )

end

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

form kopf1b	/* Rechnungskopf */

   field j1ko3_anr	use jd3anr	pos (00 05 )	length 9

   field j1ko3_nam1	use jd3nam1	pos (01 05 )	length 32  
   field j1ko3_nam2	use jd3nam2	pos (02 05 )	length 32 
   field j1ko3_str	use jd3str	pos (03 05 )	length 32
   field j1ko3_plzort	use jd3plzort	pos ( 05 05 )	length 32
end	/* form kopf1b */


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

form kopf2	/* kurzkopf */


   field j2koseite	use dseite		pos ( 3 29 ) picture "&&&"
   field j2korech	use lg_temp.rech	pos ( 0 69 ) picture "&&&&&&&&"
   field j2kolieferdat	use lg_temp.lieferdat	pos ( 3 67 ) picture "dd.mm.yy"
   field j2kokun	use lg_temp.kun		pos ( 3 53 ) picture "-------&"

end	/* form kopf2 */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

form liste1		/* Liste mit Rabatten */

   field j1lia		use lg_temp.a		pos ( 0  1 ) 
			picture "----&"		   /* Artikelnummer */
  
   field j1si1		use	dszi_elite	pos ( 0  6 )
			length	4	/* elite on */
   field j1lia_bz1	use lg_temp.a_bz1	pos ( 0  10 ) 
			length 24		       /* Artikelbezeichnung */
   field j1so1		use	dszi_pica	pos ( 0 36 )
			length 4	/* pica on */
   field j1liret_lad_pr	use lg_temp.ret_lad_pr	pos ( 0  44)
			picture "--&.&&"	blankfill    /*Zeilenrabatt %*/

   field j1si11		use	dszi_elite	pos ( 1  6 )
			length	4	/* elite on */
   field j1lia_bz2	use lg_temp.a_bz2	pos ( 1  10 ) 
			length 24		       /* Artikelbezeichnung2 */
   field j1so11		use	dszi_pica	pos ( 1 36 )
			length 4	/* pica on */

   field j1lime 	use lg_temp.lief_me	pos ( 0  56) 
			picture "----&.&&&"	  	      /* Liefermenge */
/* #1072 -----
+   field j1lime_bz	use lg_temp.lief_me_bz	pos ( 0  42) 
+			length 5		         /* Mengenbezeichnung */
+   field j1lierf_kz	use lg_temp.erf_kz	pos ( 0 48 ) /* Handkennz.  */
+			length 1
------- */

   field j1lils_vk_pr	use lg_temp.ls_vk_pr	pos ( 0  65)
			picture "----&.&&"		      /* Artikelpreis */

   field j1lisa_kz	use lg_temp.sa_kz_sint	pos ( 0 73 ) /* Sonderpreis */
			length 1	/* #1143 */

   field j1linetto	use jdnetto		pos ( 0  76) 
			picture "------&.&&"		       /* Zeilennetto */ /* #1431  05.04.93  kb */

   field j1lifreifeld1	use lg_temp.freifeld1	pos ( 0  87 )
			length 2				   /* Mwst-Kz */
end	/* form liste1 */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

form liste2		/* Liste ohne Rabatte */
   field j2lia		use lg_temp.a		pos ( 0  1 ) 
			picture "----&"		   /* Artikelnummer */

   field j2si1		use	dszi_elite	pos ( 0  6 )
			length	4	/* elite on */
   field j2lia_bz1	use lg_temp.a_bz1	pos ( 0  10 ) 
			length 24		       /* Artikelbezeichnung */
   field j2so1		use	dszi_pica	pos ( 0 36 )
			length 4	/* pica on */

   field j2si11		use	dszi_elite	pos ( 1  6 )
			length	4
   field j2lia_bz2	use lg_temp.a_bz2	pos ( 1  10 ) 
			length 24		       /* Artikelbezeichnung */
   field j2so11		use	dszi_pica	pos ( 1 36 )
			length 4	/* pica on */

   field j2lime 	use lg_temp.lief_me	pos ( 0  56) 
			picture "----&.&&&"	  	      /* Liefermenge */
/* #1072 ----
+   field j2lime_bz	use lg_temp.lief_me_bz	pos ( 0  43) 
+			length 5		         /* Mengenbezeichnung */
+   field j2lierf_kz	use lg_temp.erf_kz	pos ( 0 50 ) /* Erf-KZ */
+			length 1
+--- */

   field j2lils_vk_pr	use lg_temp.ls_vk_pr	pos ( 0  65) 
			picture "-----&.&&"		      /* Artikelpreis */ /* #1431  05.04.93  kb */
   field j2lisa_kz	use lg_temp.sa_kz_sint	pos ( 0 74 ) /* Sonderpreis */ /* #1431  05.04.93  kb */
			length 1	/* #1143 */

   field j2liret_me	use lg_temp.ret_me	pos ( 0  76)
			picture "------&.&&"	       /* Zeilenbetrag brutto */ /* #1431  05.04.93  kb */
   field j2lifreifeld1	use lg_temp.freifeld1	pos ( 0  87)
			length 2				   /* Mwst-Kz */
end	/* form liste2 */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

form liste3
   field j3lidszi	use dszi_elite	pos ( 0  6 )
				length	4
   field j3lilsp_txt	use dzusatztext	pos ( 0  10 )
			length 60
   field j3lidszo	use dszi_pica	pos ( 0  70 )
				length	4
end	/* form liste3 */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

form uebertrag

   field jusumm		use summe1		pos ( 0  00 )
			picture "-------&.&&"
end		/* form uebertrag */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

form fuss0	/* maske fuer jew. summen-ausdruck */
   field f0wert		use j0wert		pos ( 0 0 )
			picture "--------&.&&"
end	/* form fuss0 */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

form fuss1
   field f1einz_ausw	use lg_temp.einz_ausw	pos ( 0 28 )
			picture "<------&.&&"
   field f1kun_typ	use lg_temp.kun_typ	pos ( 0 07 )
			picture "--&.&&"
end		/* form fuss1 */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

form fuss

   field fdszii		use dszi_elite	pos ( 0  0 ) length 4

   field fwargr1	use dwargr1	pos ( 1 7 ) picture "----&.&&"
			blankfill

   field fwargr4	use dwargr4	pos ( 4 7 ) picture "----&.&&"
			blankfill

   field fwargr7	use dwargr7	pos ( 7 7 ) picture "----&.&&"
			blankfill

   field fwargr2	use dwargr2	pos ( 1 23 ) picture "----&.&&"
			blankfill

   field fwargr5	use dwargr5	pos ( 4 23 ) picture "----&.&&"
			blankfill

   field fwargr8	use dwargr8	pos ( 7 23 ) picture "----&.&&"
			blankfill

   field fwargr3	use dwargr3	pos ( 1 38 ) picture "----&.&&"
			blankfill

   field fwargr6	use dwargr6	pos ( 4 38 ) picture "----&.&&"
			blankfill

   field fwargr9	use dwargr9	pos ( 7 38 ) picture "----&.&&"
			blankfill

   field fmwst2w	use dmwst2w	pos ( 1 57 ) picture "------&.&&" /* #1431  05.04.93  kb */
			blankfill

   field fmwst2b	use dmwst2b	pos ( 4 57 ) picture "------&.&&" /* #1431  05.04.93  kb */
			blankfill

   field fmwst1w	use dmwst1w	pos ( 1 71 ) picture "------&.&&" /* #1431  05.04.93  kb */
			blankfill

   field fmwst1b	use dmwst1b	pos ( 4 71 ) picture "------&.&&" /* #1431  05.04.93  kb */
			blankfill

   field fges1w		use dges1w	pos ( 1 84 ) picture "------&.&&"
			blankfill

   field fges1b		use dges1b	pos ( 4 84 ) picture "------&.&&"
			blankfill

   field fgesges	use dgesges	pos ( 7 83 ) picture "-------&.&&"
			blankfill

   field fdszio		use dszi_pica	pos ( 8 0 ) length 4

end	/* form fuss */

form beleg
   field bsumme2	use summe2		pos ( 0 0)
			picture "---------&.&&" /* #1431  05.04.93  kb */
end		/* end form beleg */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "messdef"

/**--------------------------------------------------------------------------
-	Procedure : redr20
-     	In        : listetyp	Formatname analog Format fuer Formulargenerator
-		    silentprint 0 = Listenpositionen werden gedruckt
-				1 = Listenpositionen werden unterdrueckt
-	Out       : 0 = o.k.
-		    1 = nicht erfolgreich
-	Funktion  : Export-Funktion, organisiert Druckausgabe
---------------------------------------------------------------------------**/

procedure redr20

parameter
	field listetyp char (5)	/* uebergabe des Formatnamens */
	field silentprint	smallint	/* datenzeilen weg */
end

if ""	= "@(#) mo_dr20.rsf	2.000c	22.05.95	by JG"	end

	let silent_print = silentprint

	switch listetyp
		case "56100"	let liste_typ = 0	break
		case "56101"	let liste_typ = 1	break
		case "56102"	let liste_typ = 2	break
		case "56105"	let liste_typ = 5	break
		case "56106"	let liste_typ = 6	break
		case "56107"	let liste_typ = 7	break

		case "56110"	let liste_typ = 10	break	
		case "56111"	let liste_typ = 11	break
		case "56112"	let liste_typ = 12	break
		case "56115"	let liste_typ = 15	break
		case "56116"	let liste_typ = 16	break
		case "56117"	let liste_typ = 17	break
	   otherwise return (1)	/* ungueltiger Listentyp */
	end
	switch liste_typ
		case 2
		case 7
		case 12
		case 17		let dr_par.form_typ = "B" break

		otherwise	let dr_par.form_typ = "R" break
	end
	/* ---- hole Parametersatz ------------ */
	execute from sql
		select #dr_par.% from dr_par
			where @dr_par.form_typ
	end
	if sqlstatus
 	    return (1)
	end
			

/* ist bereits passiert -----
		let dret = proc lese_steuerzeichen ()
----------- */
/* alles lesen und steuerzeichen kappen  */

	let dszi_fett_ein = dsz_fett_ein
	let dszi_fett_aus = dsz_fett_aus
	let dszi_elite    = dsz_elite
	let dszi_pica     = dsz_pica
	let dszi_schmal_ein = dsz_schmal_ein
	let dszi_schmal_aus = dsz_schmal_aus
	let spooler = dsz_spooler

/* #2405 A */
	perform sys_par_holen ( "frm_stz_par")
	returning ( par_int_wert , sys_par_wert , sys_par_bez )
	if par_int_wert = 0
		let sys_par_wert = "0"
	end
	if sys_par_wert > "0"
		let dr_par.lst_lng = dsz_seitenlaenge 
		let dr_par.seite_lng = dsz_papierlaenge 
	end
/* #2405 E */



	init dmenuefile
	environment ("BWSWIN" , dmenuefile)
	putstr (dmenuefile , "/56100f.win" strlen (dmenuefile) + 1  , 11 )
	init dfusszeilc
	open channel druckefuss mode read file (dmenuefile)
	if not sysstatus
		read (dfusszeilc)
		close channel druckefuss
	end

	open print ausgabe dimension ( dr_par.seite_lng , 120 )
     	leftmargin ( 0 ) pipe (spooler ,"w")

	let dret = proc druckintern()
	return (dret)
end

/**--------------------------------------------------------------------------
-	Procedure : druckintern
-     	In        : -
-	Out       : -          
-	Funktion  : eigentliche Listenausgabe
---------------------------------------------------------------------------**/
procedure druckintern

/* LINECOUNTER steht nach open print auf 1 */
	print value (dsz_init) 
 	print pos ( 0 20 )value (dszi_pica)
	skip

/* * --------------------------------------------------- * */
	init dseite gruppe summe1 summe2 summe3

	init dwargr1 dwargr2 dwargr3 dwargr4
	init dwargr5 dwargr6 dwargr7 dwargr8 dwargr9

	close cursor lg_temp_lesen
	erase cursor lg_temp_lesen
/* #120199 : order by dazu */
	let cursortext =
                "select  * from "#clipped(lgnamext) # " order by rech,ls "
	prepare cursor lg_temp_lesen dynamic from sql
	end
	field cursortext
	init table lg_temp
	perform fetch_lg_temp_lesen
	if sqlstatus 
		return (1)	/* table leer oder tot */
	end


/* * --------------------------------------------------- * */
	perform kopf1druck 
	
/* ---- Anzeige gesamte Liste ----- */

	perform anzeliste

     	perform anzesumme
	close cursor lg_temp_lesen
	erase cursor lg_temp_lesen
	flush print 
	close print ausgabe
	return ( 0 )
end

/** -------------------------------------------------------------------------
-	Procedure : kopf1druck
-     	In        : -
-	Out       : -          
-	Funktion  : Bereitstellen der Adressen entsprechend Parametern 
-                   und Ausdruck des Listenkopfes 1. Seite 
-------------------------------------------------------------------------- **/

procedure kopf1druck

     field kopfhilf smallint
     field adrhilf like adr.adr

	init form kopf1b

	let line_merk = LINECOUNTER /* steht immer auf 1 */

/* #1072 -----
	perform absender_holen
----------- */

		prepare cursor jadrhol from sql
		select #adr.%
			from adr where adr.adr = $adrhilf
		end
	if lg_temp.adr3 > 0
		let adrhilf = lg_temp.adr3
		close cursor jadrhol
		fetch cursor jadrhol
		if sqlstatus
		     init jd3nam1 jd3nam2 jd3strpf jd3pf jd3str jd3plzort 
			init jd3anr
		else
			let dret = proc getptab ("anr" adr.anr )
			if  dret
				let jd3anr = ptabn.ptbez
			else
				init jd3anr
			end

			let jd3nam1	= adr.adr_nam1
			let jd3nam2	= adr.adr_nam2
			let jd3pf		= adr.pf
			init jd3str

			if adr.pf > ""
				let jd3str		= "Postfach"
				putstr (jd3str,jd3pf,strlen(jd3str) + 2
				,strlen(jd3pf))
			else
				let jd3str		= adr.str
				init jd3strpf
			end
			let jd3plzort		= adr.plz
			let i = strlen (jd3plzort)
			putstr (jd3plzort,adr.ort1,i + 2 ,36)	
		end
	else
		init jd3nam1 jd3nam2 jd3strpf jd3pf jd3str jd3plzort 
	end
	close cursor jadrhol
	erase cursor jadrhol
end

/**--------------------------------------------------------------------------
-	Procedure : AdressDruck 
-     	In        : -
-	Out       : -          
-	Funktion  : Druck der Adresse
---------------------------------------------------------------------------**/

PROCEDURE AdressDruck 

	let LINECOUNTER = 09
	print pos ( 0 0 ) value (dszi_elite)
	skip
	print form kopf1b
	let LINECOUNTER = 16
	print pos ( 0 0 ) value (dszi_pica)
	let LINECOUNTER = 14	
end



/**--------------------------------------------------------------------------
-	Procedure : kleinerkopf
-     	In        : -
-	Out       : -          
-	Funktion  : Druck des Listenkopfes auf jeder Seite
---------------------------------------------------------------------------**/

procedure kleinerkopf

	let dseite = dseite + 1
	switch liste_typ
	     case 0
	     case 2
	     case 10	
	     case 12	
	     case 5
	     case 7
	     case 15	
	     case 17	
		perform AdressDruck
	     break
	end
	let line_merk = LINECOUNTER
	print pos (  0 0 ) value (dszi_pica)
	print pos ( 0 63 ) value ("Nr. :")
	print pos (  0 99) value (dszi_elite)
	print pos ( 00 00 ) form kopf2
	print pos ( 03 21 ) value ("Seite:")

	let LINECOUNTER = 20
	if summe1 <> 0	/* ]bertrag drucken */
		print pos ( 0 0 ) value (dszi_fett_ein)
		print pos ( 0 20 ) value (dszi_pica)
		skip
		print pos ( 0 46 ) value ("]bertrag")
		print pos ( 0 65 ) form uebertrag	
		print pos ( 0 90 ) value (dszi_fett_aus) 
		skip
	else
		skip
	end
end


/** -------------------------------------------------------------------------
-	Procedure : anzeliste
-     	In        : -
-	Out       : -          
-	Funktion  : Organisation der Ausgabe des Listenteils
-------------------------------------------------------------------------- **/

procedure anzeliste

	let gruppe = lg_temp.ret
	let gruppe = gruppe - 1	/* jedenfalls verschieden */
	perform kleinerkopf
		let needzeil =  55

	if lg_temp.ag > 0
	    close cursor zusatztexte
	    erase cursor zusatztexte
	    prepare cursor zusatztexte from sql
		select #ls_txt.% from ls_txt
		where ls_txt.nr = $lg_temp.ag
		  and ls_txt.zei > 0
		  order by zei
	    end
	    fetch cursor zusatztexte

	    if not sqlstatus
/* --
		skip
-- */
		while not sqlstatus
	    		let dzusatztext = ls_txt.txt
	    		print form liste3
			skip
			fetch cursor zusatztexte
		end
		skip
	    end
	end
	
	init sqlstatus
	while not sqlstatus
		perform listenpositionen
		perform fetch_lg_temp_lesen
	end
end

/** -------------------------------------------------------------------------
-	Procedure : listenpositionen
-     	In        : -
-	Out       : -          
-	Funktion  : Organisation der Ausgabe der Positionen, der Beleginfos
			der Warengruppensortierung und des Seitenumbruchs
-------------------------------------------------------------------------- **/

procedure listenpositionen
	field anzahl	integer	/* #1489 */
	init anzahl
    if silent_print = 0
/* #1498 A */
	if lg_temp.abt > 0	/* textnummer vorhanden */
	    execute into anzahl from sql
		select count (*) from lspt
		where lspt.nr = $lg_temp.abt
		  and zei > 0
	    end
	else
		if lg_temp.retp_txt > 0	/* textnummer vorhanden */
		    execute into anzahl from sql
			select count (*) from retpt
			where retpt.nr = $lg_temp.retp_txt
			  and zei > 0
		    end
		end
	end
/* #1498 E */
/* #1498 ---------------------
= if (lg_temp.vertr1 > "") let anzahl = 8
= elseif (lg_temp.pr_lst > "") let anzahl = 7
= elseif (lg_temp.pr_stu > "") let anzahl = 6
= elseif (lg_temp.txt_nr3 > "") let anzahl = 5
= elseif (lg_temp.txt_nr2 > "") let anzahl = 4	
= elseif (lg_temp.txt_nr1 > "") let anzahl = 3
= elseif (lg_temp.adr1 > "") let anzahl = 2
= elseif (lg_temp.lsp_txt > "") let anzahl = 1
= end
---- #1498 ------ */

   end

	if gruppe <> lg_temp.ret
		if summe2 <> 0
			perform belegsumme
		end
	
		if LINECOUNTER > (needzeil -  2 - anzahl) 
			let LINECOUNTER = LINECOUNTER - 1
			perform neueseite
		end
		

		if lg_temp.frei_txt1 <> ""
		perform belegkopf
		end
		let gruppe = lg_temp.ret
		init summe2
	end

	if LINECOUNTER > (needzeil -  2 - anzahl) 
		perform neueseite
	end
	let summe1 = summe1 + lg_temp.ret_vk_pr + lg_temp.ret_me
	if lg_temp.sa_kz_sint = "*" 	/* #1143 */
		let lg_temp.sa_kz_sint = "A"	/* #1143 */
	else
		let lg_temp.sa_kz_sint = ""	/* #1143 */
	end
	switch liste_typ
	   case 10
	   case 11
	   case 12
	   case 0
	   case 1
	   case 2	let jdnetto = lg_temp.ret_vk_pr + lg_temp.ret_me
			let summe2 = jdnetto + summe2
			let summe3 = summe3 + lg_temp.ret_vk_pr
			perform warladen(0)	/* aus jdnetto */
			if silent_print = 0
				if (lg_temp.a_bz2 > "")
				   set form field liste1.j1si11 REMOVED (OFF)
				   set form field liste1.j1so11 REMOVED (OFF)
				   set form field liste1.j1lia_bz2 REMOVED (OFF)
				else
				   set form field liste1.j1si11 REMOVED (ON)
				   set form field liste1.j1so11 REMOVED (ON)
				   set form field liste1.j1lia_bz2 REMOVED (ON)
				end

				print pos ( 0 0 ) form liste1

				skip
				if (lg_temp.a_bz2 > "")
					skip
				end
			end
		break
	   case 15
	   case 16
	   case 17
	   case 5
	   case 6
	   case 7
			let summe2 = summe2 + lg_temp.ret_me
			perform warladen(1)	/* aus lg_temp.ret_me */
			if silent_print = 0
				if (lg_temp.a_bz2 > "")
				   set form field liste2.j2si11 REMOVED (OFF)
				   set form field liste2.j2so11 REMOVED (OFF)
				   set form field liste2.j2lia_bz2 REMOVED (OFF)
				else
				   set form field liste2.j2si11 REMOVED (ON)
				   set form field liste2.j2so11 REMOVED (ON)
				   set form field liste2.j2lia_bz2 REMOVED (ON)
				end

				print pos ( 0 0 ) form liste2
				skip
				if (lg_temp.a_bz2 > "" )
					skip
				end
			end
		break
	end

    if silent_print = 0
	if anzahl > 0
/* #1498 ---------
=    let dzusatztext = lg_temp.lsp_txt print form liste3 skip
=    if anzahl > 1 let dzusatztext = lg_temp.adr1 print form liste3 skip
=	if anzahl > 2 let dzusatztext = lg_temp.txt_nr1 print form liste3 skip
=	    if anzahl > 3 let dzusatztext = lg_temp.txt_nr2 print form liste3
=    		skip
=		if anzahl > 4 let dzusatztext = lg_temp.txt_nr3
=    		    print form liste3 skip
=		    if anzahl > 5 let dzusatztext = lg_temp.pr_stu
=    			print form liste3 skip
=			if anzahl > 6 let dzusatztext = lg_temp.pr_lst
=    			    print form liste3 skip
=			    if anzahl > 7 let dzusatztext = lg_temp.vertr1
=    				print form liste3 skip
=/*
==				if anzahl > 8 let dzusatztext = lg_temp. vertr2
==    				    print form liste3 skip
==				    if anzahl > 9 let dzusatztext = 
==					lg_temp.sam_rech print form liste3 skip
==				    end	/* 10 */
==				end	/* 9 */
=*/
=			    end	/* 8 */
=			end	/* 7 */
=		    end	/* 6 */
=		end	/* 5 */
=	    end	/* 4 */
=	end	/* 3 */
=    end	/* 2 */
= ----------- #1498 --- */
/* #1498 A */
	   close cursor zusatztexte
	   erase cursor zusatztexte
	   if lg_temp.abt > 0
		   prepare cursor zusatztexte from sql
		   select #lspt.% from lspt
		    where lspt.nr = $lg_temp.abt
			  and lspt.zei > 0
			  order by zei
			end

	   	   fetch cursor zusatztexte
	   	   if not sqlstatus	/* texte vorhanden */
		     while not sqlstatus
	    		let dzusatztext = lspt.txt
	    		print form liste3
			skip
			fetch cursor zusatztexte
		     end
	          end		/* texte vorhanden */
	  else
		if lg_temp.retp_txt > 0
		   prepare cursor zusatztexte from sql
		   select #retpt.% from retpt
		    where retpt.nr = $lg_temp.retp_txt
			  and retpt.zei > 0
			  order by zei
			end
	   	   fetch cursor zusatztexte
	   	   if not sqlstatus	/* texte vorhanden */
		     while not sqlstatus
	    		let dzusatztext = retpt.txt
	    		print form liste3
			skip
			fetch cursor zusatztexte
		     end
	          end		/* texte vorhanden */
		end
	end
/* #1498 E */
	end	/* 1 */
    end
	
	if LINECOUNTER > needzeil
		perform neueseite
	end
end

/**--------------------------------------------------------------------------
-	Procedure : anzesumme
-     	In        : -
-	Out       : -          
-	Funktion  : Ausgabe des Summenteils
---------------------------------------------------------------------------**/

procedure anzesumme
	field i	smallint	/* allg parameter */
	field anzahl integer

	if summe2 <> 0
		perform  belegsumme
	end

	if lg_temp.hwg > 0	/* textnummer vorhanden */
	    init anzahl
	    execute into anzahl from sql
		select count (*) from ls_txt
		where ls_txt.nr = $lg_temp.hwg
		  and zei > 0
	    end
	    if anzahl > 0	/* textzahl groesser 0 */
		if LINECOUNTER > (needzeil -  3 - anzahl)
			perform neueseite
		end
		close cursor zusatztexte
		erase cursor zusatztexte
		prepare cursor zusatztexte from sql
		select #ls_txt.% from ls_txt
		where ls_txt.nr = $lg_temp.hwg
		  and ls_txt.zei > 0
		  order by zei
		end
		fetch cursor zusatztexte

		if not sqlstatus	/* texte vorhanden */
			skip
			while not sqlstatus
	    			let dzusatztext = ls_txt.txt
	    			print form liste3
				skip
				fetch cursor zusatztexte
			end
		end		/* texte vorhanden */
	    end			/* textzahl groesser 0 */
	end			/* textnummer vorhanden */
	
	print pos ( 0  99 ) value (dszi_pica) 
/* ---
	skip
---- */

/* ======= End-Rabatt-Zeile ======= */

	if lg_temp.deb_kto <> 0
		let summe3 = summe3 + lg_temp.deb_kto
		print pos ( 0 12 ) value ("% Rabatt aus DM")
		let j0wert = lg_temp.deb_kto
		print pos ( 0 64 ) form fuss0
		print pos ( 0 00 ) form fuss1
	end


	let LINECOUNTER = needzeil + 2

/* ======  Fuss ====== */

	perform warendlad
	let dmwst1w = lg_temp.bonus1
	let dmwst1b = lg_temp.bonus2
	let dmwst2w = lg_temp.freifeld2
	let dmwst2b = lg_temp.jr_plan_ums
	let dges1w  = dmwst1w + dmwst2w
	let dges1b  = dmwst1b + dmwst2b
	let dgesges = lg_temp.kred_lim
	print form fuss

/* -----
+	let line_merk = LINECOUNTER
+	let dret = proc drustri ("56100" 1)
+	if dret = CTRUE
+		let LINECOUNTER = 67
+	end
---- */

	if silent_print = 0
		let LINECOUNTER = 66
		print pos ( 0 , 0 ) value
	     (dszi_schmal_ein# clipped (dfusszeilc) #dszi_schmal_aus#dszi_pica)
	end
	skip
	nextpage

end

/**--------------------------------------------------------------------------
-	Procedure : neueseite
-     	In        : -
-	Out       : -          
-	Funktion  : Organisation Seitenumbruch (Uebertrag + kleiner Kopf)
---------------------------------------------------------------------------**/

procedure neueseite
	skip
	print pos (00 00 ) value (dszi_fett_ein)
	print pos (00 10 ) value (dszi_pica)
	skip
	let LINECOUNTER = 64
	print pos ( 0 65 ) value ("]bertrag")
	skip
	print pos (00 00) value (dszi_fett_aus)
	nextpage
	perform kleinerkopf
end

/**--------------------------------------------------------------------------
-	Procedure : belegsumme
-     	In        : -
-	Out       : -          
-	Funktion  : Belegsummenzeile ausgeben
---------------------------------------------------------------------------**/

procedure belegsumme
	print pos ( 0 1  ) value (dszi_fett_ein # dszi_pica)
	print pos ( 0 68 ) form beleg
	print pos ( 0 50 ) value ("Belegsumme")
	print pos ( 0 99 ) value (dszi_fett_aus)
	skip 
end

/**--------------------------------------------------------------------------
-	Procedure : belegkopf
-     	In        : -
-	Out       : -          
-	Funktion  : Belegkopfzeile ausgeben
---------------------------------------------------------------------------**/

procedure belegkopf
	print pos ( 0 0 ) value
	(dszi_fett_ein # dszi_pica#lg_temp.frei_txt1 # dszi_fett_aus)
	skip
end


/**--------------------------------------------------------------------------
-	Procedure : drustri
-     	In        : dstrfile	Name der Datei
-		    dextnr	Extension-Nr. -1 => .str
-					0 .. 9   => .st0 .. .st9
-	Out       : CFALSE = Fehler  / CTRUE = o.k.
-	Funktion  : Ausgabe Item-Datei auf Drucker 
---------------------------------------------------------------------------**/

procedure drustri 
parameter
	field dstrfile  char(10)
	field dextnr    integer
end
field dsl       char(1)  value  "/"
field alt_zeil smallint   /* zur berechnung der relativ-position */
field ddisptx1 char(98) 
field dpgmitem char(50)
field danzitem smallint 
field dlen     smallint 
field dmldistxt       smallint 
field dreturn     smallint

table titem
	field disptxt	char(98)
	field zeile	smallint
	field spalte	smallint
end

form mait
	field mitnr integer pos(0 0) length 1
end
	init dpgmitem  alt_zeil
	environment ("BWSITEM",dpgmitem)
	putstr (dpgmitem, dsl     , strlen(dpgmitem)+1,strlen(dsl))
	putstr (dpgmitem, dstrfile, strlen(dpgmitem)+1,strlen(dstrfile))
/*----- Extension bilden --------------------------------------------------*/
	putstr (dpgmitem,".str", strlen(dpgmitem)+1,4)
	if dextnr <> -1
		let mitnr = dextnr
		let dlen  = strlen(dpgmitem) 
		putfield (dpgmitem,mitnr,dlen)
	end
/*----- Stringdatei oeffnen -----------------------------------------------*/
	open channel chmsk separator "$" file (dpgmitem,"r")
	if sysstatus 
		let dreturn     = CFALSE
		return (dreturn)
	end
/*----- Alle Strings lesen und in die file schreiben -----------------*/

	read table titem          /* erste zeile alt_zeil setzen */
	if not sysstatus 
		let dmldistxt = strlen(titem.disptxt)
		if dmldistxt  /* laenge muss groesser 0 sein */
		   let ddisptx1 = titem.disptxt
		   let LINECOUNTER = LINECOUNTER + titem.zeile
		   print pos (0  , titem.spalte ) value ( ddisptx1 )
		   let alt_zeil = titem.zeile
		end
		
		while CTRUE
		   read table titem
		   if sysstatus 
			break 
		   end
		   let dmldistxt = strlen(titem.disptxt)
		   if dmldistxt = 0 
			continue 

		   end
		   let ddisptx1 = titem.disptxt
		   let LINECOUNTER = LINECOUNTER + titem.zeile - alt_zeil
		   print pos (0  , titem.spalte ) value ( ddisptx1 )
		   let alt_zeil = titem.zeile
		end
	end
	close channel chmsk
	let dreturn = CTRUE
	return (dreturn)
END
/* --- #1072 -----
/* --------------------------------------------------------------------------
-	Porcedure : absender_holen
-     	In        : -
-	Out       : -          
-	Funktion  : Lesen der Datei "56100.st2" in das feld "absender"
--------------------------------------------------------------------------- */

porcedure absender_holen
field dpgmitem char(50)
field dstrfile char (12) value "/56100.st2"

	init absender
	init dpgmitem
	environment ("BWSITEM",dpgmitem)
	putstr (dpgmitem, dstrfile, strlen(dpgmitem)+1,strlen(dstrfile))
	open channel chmsk file (dpgmitem,"r")
	if sysstatus 
		init absender 
		return 
	end

	read (absender)
	if sysstatus
		init absender 
	end

	close channel chmsk
end
--- */

/**--------------------------------------------------------------------------
-	Procedure : fetch_lg_temp_lesen
-     	In        : -
-	Out       : -          
-	Funktion  : Lesen naechster Satz aus temp-table 
---------------------------------------------------------------------------**/

procedure fetch_lg_temp_lesen
	select cursor lg_temp_lesen
	fetch cursor lg_temp_lesen
	if sqlstatus 
		return
	end
	if sqlcount = 0
		let sqlstatus = 100
		return
	end
/* #956 -- raus ins include ---- */

#include 	"templad"

	init sqlstatus
end

/*##D8A*/
/**--------------------------------------------------------------------------
-	Procedure : warladen
-     	In        : 0 = aus jdnetto
-		  : 1 = aus lg_temp.ret_me
-	Out       : -          
-	Funktion  : Aufsummieren der einzelnen Warengruppen
---------------------------------------------------------------------------**/
/*##D8E*/

PROCEDURE warladen
parameter
	field dschalter smallint
end

	execute from sql
		select #a_bas.% from a_bas
			where a_bas.a = $lg_temp.a
	end
	switch a_bas.wg

	case 1
	      if dschalter
		let dwargr1 = dwargr1 + lg_temp.ret_me
	      else
		let dwargr1 = dwargr1 + jdnetto
	      end
	   break
	case 2
	      if dschalter
		let dwargr2 = dwargr2 + lg_temp.ret_me
	      else
		let dwargr2 = dwargr2 + jdnetto
	      end
	   break
	case 3
	      if dschalter
		let dwargr3 = dwargr3 + lg_temp.ret_me
	      else
		let dwargr3 = dwargr3 + jdnetto
	      end
	   break
	case 4
	      if dschalter
		let dwargr4 = dwargr4 + lg_temp.ret_me
	      else
		let dwargr4 = dwargr4 + jdnetto
	      end
	   break
	case 5
	      if dschalter
		let dwargr5 = dwargr5 + lg_temp.ret_me
	      else
		let dwargr5 = dwargr5 + jdnetto
	      end
	   break
	case 6
	      if dschalter
		let dwargr6 = dwargr6 + lg_temp.ret_me
	      else
		let dwargr6 = dwargr6 + jdnetto
	      end
	   break
	case 7
	      if dschalter
		let dwargr7 = dwargr7 + lg_temp.ret_me
	      else
		let dwargr7 = dwargr7 + jdnetto
	      end
	   break
	case 8
	      if dschalter
		let dwargr8 = dwargr8 + lg_temp.ret_me
	      else
		let dwargr8 = dwargr8 + jdnetto
	      end
	   break
	case 9
	      if dschalter
		let dwargr9 = dwargr9 + lg_temp.ret_me
	      else
		let dwargr9 = dwargr9 + jdnetto
	      end
	   break
	otherwise break
	end
end

/*##D8A*/
/**--------------------------------------------------------------------------
-	Procedure : warendlad
-     	In        : -
-	Out       : -          
-	Funktion  : Eventuell alles noch ueber Endrabatt ziehen 
---------------------------------------------------------------------------**/
/*##D8E*/

PROCEDURE warendlad

	field dhilfsfeld decimal (7,3)

	if lg_temp.deb_kto <> 0

		let dhilfsfeld = (100 - lg_temp.kun_typ ) / 100

		if dwargr1 <> 0
			let dwargr1 = dwargr1 * dhilfsfeld
		end
		if dwargr2 <> 0
			let dwargr2 = dwargr2 * dhilfsfeld
		end
		if dwargr3 <> 0
			let dwargr3 = dwargr3 * dhilfsfeld
		end
		if dwargr4 <> 0
			let dwargr4 = dwargr4 * dhilfsfeld
		end
		if dwargr5 <> 0
			let dwargr5 = dwargr5 * dhilfsfeld
		end
		if dwargr6 <> 0
			let dwargr6 = dwargr6 * dhilfsfeld
		end
		if dwargr7 <> 0
			let dwargr7 = dwargr7 * dhilfsfeld
		end
		if dwargr8 <> 0
			let dwargr8 = dwargr8 * dhilfsfeld
		end
		if dwargr9 <> 0
			let dwargr9 = dwargr9 * dhilfsfeld
		end
	end
end



