//#160201 Mehrsprachigkeit
/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_item
-
-	Interne Operatoren	:	proc dispitem
-					proc printitem
-
-	Externe Operatoren	:	-
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	21.11.89
-	Modifikationsdatum	:	21.11.89
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    24.02.89    Simmchen	
-	#2      1.00    21.11.89    Simmchen	Modularisierung
-	#3      1.00    29.11.90    Simmchen    variable Itemtextlaenge
-	#4	1.0	14.08.90    F.Knippel 	Proc druckitem 
-	#5	1.00	28.08.90    Seichter	Ergaenzung Proc. printitem
-	#6	$$version$$	26.06.96	JG	Kommemtare eingebaut
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:  Das Modul beinhaltet Proceduren die fuer
-				   die Bearbeitung der Masken .itm Dateien
-				   zur Verfuegung gestellt werden.
-
------------------------------------------------------------------------------
*/

MODULE mo_item

/*----- Export Section -----------------------------------------------------*/

export 
	proc dispitem
	proc druckitem /*#4,kn*/
	proc printitem
        field mehrsprachig
end

IMPORT module mo_sys_p end
IMPORT module mo_men_m
        table mess_menue
END

database bws
        table itm end
end
/*----- Import Section -----------------------------------------------------*/

/*----- Lokale Deklarationen Modul -----------------------------------------*/
field k smallint
field sys_par_wert     smallint
field sys_par_bez      char (20)
        field mehrsprachig   smallint   //#160201
	field dsl       char(1)  value  "/"

	field CTRUE     smallint value 1
	field CFALSE    smallint value 0

/*-------------------------------------------------------------------------*/
/*----- P R O C E D U R E N -----------------------------------------------*/
/*-------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
-									  
-	Procedure	: dispitem
-
-	In		: ditmfile	char(10)	Itemfilename
-			  dextnr	smallint	Extension Nr.
-
-	Out		: CTRUE/CFALSE	smallint        ok, n.ok
-
-	Beschreibung    : Die Procedure liest die Maskenfeldnamen aus der
-			  Masken-Item-Datei und gibt diese auf dem Bild-
-			  schirm aus.
-
-			  Der Dateiname wird folgendermassen gebildet :
-
-			  1. Pfad	  - Environmentvariable  BWSITEM
-			  2. Programm     - ditmfile
-			  3. Extension 1  - Konstant ".it<dextnr>"
-			  4. Extension 2  - dextnr 
-					    bei dexntr = -1 ist die Extension
-					    = .itm
-			 falls das erste Zeichen einer Zeile ein "#" ist,
-			wird diese Zeile (shell-like) als Kommentar gewertet
---------------------------------------------------------------------------*/

PROCEDURE dispitem

/*----- Parameter Deklaration ---------------------------------------------*/

parameter
	field ditmfile  char(10)
	field dextnr    integer
end

/*----- Lokale Deklarationen Procedure ------------------------------------*/

field ddisptx1 char(19) 
field ddisptx2 char(11) 
field dpgmitem char(40)
field dpgmitem_eu char(40)
field danzitem smallint 
field dlen     smallint 
field di       smallint 
field dret     smallint
field sprache_geholt smallint
field sprache smallint
field itm_name char(16)
field ieuro smallint
field euro_gelesen smallint
field sys_par_wert	char (1)
field sys_par_bez	char (20)
field k smallint

table titem
	field disptxt	char(19)
	field zeile	smallint
	field spalte	smallint
        field attribut char(6)                //#160201
end

form madimain1
        field disptx1 use ddisptx1 pos (0 0) DISPLAYONLY
end

form mait
	field mitnr integer pos(0 0) length 1
end
if dextnr <> -1
     let itm_name = clipped(ditmfile)
               #".it"
               #dextnr
else
     let itm_name = clipped(ditmfile)#".itm"
end
               
if not sprache_geholt  //#160201
   perform sys_par_holen ("mehrsprachig")         //#160201
             returning (k sys_par_wert sys_par_bez)
     
   if k  
              let mehrsprachig = sys_par_wert
   else
              init mehrsprachig
   end
   let sprache_geholt = 1
   if mehrsprachig > 0
        execute into sprache from sql
                select zahl from sys_ben where
                        pers = $mess_menue.pers
        end
        if sprache > 0
                execute into sprache from sql
                select ptwert from ptabn
                where ptitem = "sprache" and
                   ptwert = $sprache
                end
                if sqlstatus
                        init sprache
                end
        end
        prepare cursor cu_hole_item from sql
                select #itm.% from itm
                where sprache = $sprache
                and name = $itm_name
        end
    end
end
if mehrsprachig = 1
     downshift(itm_name)
end
/*----- Code --------------------------------------------------------------*/

	if not euro_gelesen
                perform sys_par_holen ("europreis" )
                        returning ( k, sys_par_wert, sys_par_bez )
                let ieuro = sys_par_wert
                if ((k = 0) or (sys_par_wert = "0"))
                        let ieuro = 0 
                end
		let euro_gelesen = 1
	end
        init dpgmitem dpgmitem_eu

	environment ("BWSITEM",dpgmitem)
	putstr (dpgmitem, dsl     , strlen(dpgmitem)+1,strlen(dsl))
	putstr (dpgmitem, ditmfile, strlen(dpgmitem)+1,strlen(ditmfile))
	putstr (dpgmitem,".itm", strlen(dpgmitem)+1,4)


	environment ("BWSITEM",dpgmitem_eu)
	putstr (dpgmitem_eu, dsl     , strlen(dpgmitem_eu)+1,strlen(dsl))
	putstr (dpgmitem_eu, "euro/"     , strlen(dpgmitem_eu)+1,5)
	putstr (dpgmitem_eu, ditmfile, strlen(dpgmitem_eu)+1,strlen(ditmfile))
	putstr (dpgmitem_eu,".itm", strlen(dpgmitem_eu)+1,4)

/*----- Extension bilden --------------------------------------------------*/


	if dextnr <> -1
		let mitnr = dextnr
		let dlen  = strlen(dpgmitem) 
		putfield (dpgmitem,mitnr,dlen)
		let dlen  = strlen(dpgmitem_eu) 
		putfield (dpgmitem_eu,mitnr,dlen)

	end

/*----- Itemdatei oeffnen -----------------------------------------------*/
        if not mehrsprachig
if not ieuro or ieuro = 2 
	open channel chmsk separator "$" file (dpgmitem,"r")
	if sysstatus 
		let dret     = CFALSE
		return (dret)
	end
else
	open channel chmsk separator "$" file (dpgmitem_eu,"r")
	if sysstatus
		open channel chmsk separator "$" file (dpgmitem,"r")
		if sysstatus 
			let dret     = CFALSE
			return (dret)
		end
	end
end
        else
                close cursor cu_hole_item
        end
/*----- Alle Items lesen und in die Matrixvariablen lesen -----------------*/


	while CTRUE
                if mehrsprachig
                        fetch cursor cu_hole_item
                        if sqlstatus break end
                        let titem.disptxt = itm.text
                        let titem.zeile = itm.zeile
                        let titem.spalte = itm.spalte
                        let titem.attribut = itm.attribut
                else
                        read table titem
                        if sysstatus break end
                end


                if strlen (titem.disptxt) = 0
			continue
		end
                if instring(titem.disptxt,"#",1,1)  = 1
			continue
		end

                let ddisptx1 = titem.disptxt
		let dlen = strlen(ddisptx1)

		set form field disptx1 length (dlen)
                set form field disptx1 highlight (OFF)
                switch titem.attribut    //#160201
                case "F"
                        set form field disptx1 highlight (ON)
                        set form field disptx1 black
                        break
                case "blue"
                        set form field disptx1 blue
                        break
                case "F,blue"
                        set form field disptx1 highlight (ON)
                        set form field disptx1 blue
                        break
                case "red"
                        set form field disptx1 red
                        break
                case "F,red"
                        set form field disptx1 highlight (ON)
                        set form field disptx1 red
                        break
                otherwise
                        set form field disptx1 highlight (OFF)
                        set form field disptx1 black
                        break
                end


		display pos(titem.zeile titem.spalte) madimain1.disptx1

	end

	close channel chmsk

	let dret = CTRUE

	return (dret)

END



/*---------------------------------------------------------------------------
-									  
-	Procedure	: druckitem
-
-	In		: ditmfile	char(10)	Itemfilename
-			  dextnr	smallint	Extension Nr.
-
-	Out		: CTRUE/CFALSE	smallint        ok, n.ok
-                         dretzeile char ()             Zeile mit Item
-
-	Beschreibung    : Die Procedure liest die Maskenfeldnamen aus der
-			  Masken-Item-Datei und schreibt diese 
-			in einen String. Der String dient als 
-			Kopfzeile fuer eine Druckausgabe 
-
-			  Der Dateiname wird folgendermassen gebildet :
-
-			  1. Pfad	  - Environmentvariable  BWSITEM
-			  2. Programm     - ditmfile
-			  3. Extension 1  - Konstant ".it<dextnr>"
-			  4. Extension 2  - dextnr 
-					    bei dexntr = -1 ist die Extension
-					    = .itm
-			Die Maske sollte als Zeile den Wert Null haben. 
-			Diese Routine baut dann den String automatisch auf. 
-			Wird in Zeile ein Wert angegeben, so wird er als 
-			Laenge des Textes interpretiert. 
-							
----------------------------------------------------------------------*/


PROCEDURE druckitem /* #4,kn */

/*----- Parameter Deklaration ---------------------------------------------*/

parameter
	field ditmfile  char(10)
	field dextnr    integer
end

/*----- Lokale Deklarationen Procedure ------------------------------------*/
CONSTANT 
	CZEILENLAENGE value 251
END

form mait
	field mitnr integer pos(0 0) length 1
end

field dpgmitem char(40)
field danzitem smallint 
field dlen     smallint 
field dret     smallint
field dspalte  smallint
field dretzeile char (CZEILENLAENGE ) 

table titem
	field disptxt	char(19)
	field zeile	smallint
	field spalte	smallint
end


/*----- Code --------------------------------------------------------------*/

	let dret = CTRUE

	init dpgmitem
	init dretzeile 
/* 
	Fuehrt zu Problemen beim Drucken, wenn die Rufende Fkt eine andere Stringlaenge hat als diese Routine. 

	let dlen = 1 
	while dlen < CZEILENLAENGE 
		putchar ( dretzeile ,dlen," ")
		let dlen = dlen +1
	end
		putchar ( dretzeile ,CZEILENLAENGE ,".")
*/

	environment ("BWSITEM",dpgmitem)
	putstr (dpgmitem, dsl     , strlen(dpgmitem)+1,strlen(dsl))
	putstr (dpgmitem, ditmfile, strlen(dpgmitem)+1,strlen(ditmfile))

/*----- Extension bilden --------------------------------------------------*/


	if dextnr <> -1
		let mitnr = dextnr
		let dlen  = strlen(dpgmitem) 
		putfield (dpgmitem,mitnr,dlen+1)

	end
	putstr (dpgmitem,".itm", strlen(dpgmitem)+1,4)

/*----- Itemdatei oeffnen -----------------------------------------------*/

	open channel chmsk separator "$" file (dpgmitem,"r")
	if sysstatus 
		let dret     = CFALSE
	end

/*----- Alle Items lesen und in die Matrixvariablen lesen -----------------*/


	while dret = CTRUE

		read table titem
		if sysstatus break end

		if titem.zeile >0 
			let dlen = titem.zeile 
		else 
			let dlen = strlen (titem.disptxt ) 
		end 
		if titem.spalte +dlen < CZEILENLAENGE 

			let dspalte = titem.spalte  + 1 
			putstr (dretzeile ," " ,titem.spalte ,1 )
			putstr (dretzeile ,titem.disptxt ,dspalte ,dlen )

		end
	end

	close channel chmsk

	return (dret , dretzeile )

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: printitem
-
-	In		: ditmfile	char(10)	Itemfilename
-			  dextnr	smallint	Extension Nr.
-
-	Out		: CTRUE/CFALSE	smallint        ok, n.ok
-
-	Beschreibung    : Die Procedure liest die Maskenfeldnamen aus der
-			  Masken-Item-Datei und gibt diese auf dem Drucker
-			  aus.
-
-			  Der Dateiname wird folgendermassen gebildet :
-
-			  1. Pfad	  - Environmentvariable  BWSITEM
-			  2. Programm     - ditmfile
-			  3. Extension 1  - Konstant ".pt<dextnr>"
-			  4. Extension 2  - dextnr 
-					    bei dexntr = -1 ist die Extension
-					    = .ptm
-							
---------------------------------------------------------------------------*/

PROCEDURE printitem

/*----- Parameter Deklaration ---------------------------------------------*/

parameter
	field ditmfile  char(10)
	field dextnr    integer
end

/*----- Lokale Deklarationen Procedure ------------------------------------*/

field ddisptx1 char(19) 
field ddisptx2 char(11) 
field dpgmitem char(40)
field danzitem smallint 
field dlen     smallint 
field di       smallint 
field dret     smallint
field sprache_geholt smallint
field sprache smallint
field itm_name char(16)

table titem
	field disptxt	char(19)
	field zeile	smallint
	field spalte	smallint
        field attribut char(6)                //#160201
end

form madimain1
        field disptx1 use ddisptx1 pos (0 0) DISPLAYONLY
end

form mait
	field mitnr integer pos(0 0) length 1
end
if dextnr <> -1
     let itm_name = clipped(ditmfile)
               #".pt"
               #dextnr
else
     let itm_name = clipped(ditmfile)#".ptm"
end
               
if not sprache_geholt  //#160201
   perform sys_par_holen ("mehrsprachig")         //#160201
             returning (k sys_par_wert sys_par_bez)
     
   if k  
              let mehrsprachig = sys_par_wert
   else
              init mehrsprachig
   end
   let sprache_geholt = 1
   if mehrsprachig > 0
        execute into sprache from sql
                select zahl from sys_ben where
                        pers = $mess_menue.pers
        end
        if sprache > 0
                execute into sprache from sql
                select ptwert from ptabn
                where ptitem = "sprache" and
                   ptwert = $sprache
                end
                if sqlstatus
                        init sprache
                end
        end
        prepare cursor cu_hole_item from sql
                select #itm.% from itm
                where sprache = $sprache
                and name = $itm_name
        end
    end
end
/*----- Code --------------------------------------------------------------*/

	init dpgmitem

	environment ("BWSITEM",dpgmitem)
	putstr (dpgmitem, dsl     , strlen(dpgmitem)+1,strlen(dsl))
	putstr (dpgmitem, ditmfile, strlen(dpgmitem)+1,strlen(ditmfile))

/*----- Extension bilden --------------------------------------------------*/

        putstr (dpgmitem,".ptm", strlen(dpgmitem)+1,4)

	if dextnr <> -1
		let mitnr = dextnr
		let dlen  = strlen(dpgmitem) 
		putfield (dpgmitem,mitnr,dlen)

	end

/*----- Itemdatei oeffnen -----------------------------------------------*/
        if not mehrsprachig
           open channel chmsk separator "$" file (dpgmitem,"r")
           if sysstatus 
		let dret     = CFALSE
		return (dret)
           end
        else
                close cursor cu_hole_item
        end
/*----- Alle Items lesen und in die Matrixvariablen lesen -----------------*/


	while CTRUE
                if mehrsprachig
                        fetch cursor cu_hole_item
                        if sqlstatus break end
                        let titem.disptxt = itm.text
                        let titem.zeile = itm.zeile
                        let titem.spalte = itm.spalte
                        let titem.attribut = itm.attribut
                else
                        read table titem
                        if sysstatus break end
                end


                if strlen (titem.disptxt) = 0
			continue
		end
                if instring(titem.disptxt,"#",1,1)  = 1
			continue
		end

                let ddisptx1 = titem.disptxt
		let dlen = strlen(ddisptx1)

		set form field disptx1 length (dlen)

		print pos(titem.zeile titem.spalte) madimain1.disptx1

	end

	close channel chmsk

	let dret = CTRUE

	return (dret)

END


procedure what_kennung
if "" = "@(#) mo_item.rsf	VLUGLOTSA	26.06.96	by JG>" end
end
