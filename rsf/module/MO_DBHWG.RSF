/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbhwg
-
-
-	Externe Operatoren	:	-
-
-	Autor			:	Axel Seichter
-	Erstellungsdatum	:	04.05.90
-	Modifikationsdatum	:	04.07.90
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    26.04.90   Knippel      HWG-Bezeichnung lesen
-	#2      2.00    04.07.90   Seichter     weitere Prozeduren
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle hwg
-
-
------------------------------------------------------------------------------
*/

module mo_dbhwg

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_hwg
	proc prepread_hwg
	proc putmxkey_hwg
	proc recupdate_hwg
	proc recdelete_hwg
	proc recinsert_hwg
	proc recread_hwg
	proc getmxkey_hwg
	proc recreaddef_hwg
	proc inittable_hwg
	proc backkey_hwg
	proc savekey_hwg
	proc initkey_hwg
	proc cmpkey_hwg
	proc clrdelstatus_hwg
	proc getdelstatus_hwg
	proc fetchcuu_hwg
	proc fetchread_hwg
	proc erasecursor_hwg
	proc closeread_hwg
	proc closeupdate_hwg
	proc einfuegen_hwg
	proc prepscroll_hwg
	proc fetchfirst_hwg
	proc fetchnext_hwg
	proc recreadpos_hwg_hwg_bz1
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
	field dret
end

IMPORT module mo_cons
end

IMPORT module mo_meld
	proc disp_sql
	proc disp_msg
end

#include "colaenge"

database bws
	table    hwg end
end

field dsavekey 	 like hwg.hwg 
field ddefakey   like hwg.hwg value "-1"
field dcurskey	 like hwg.hwg dimension(CMAXMATRIX)
field dsql	 smallint

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_hwg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_hwg.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_hwg

	prepare cursor cuu_hwg from sql
		select #hwg.% from hwg
		where @hwg.hwg for update
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_hwg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor cur_hwg.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_hwg

	prepare cursor cur_hwg from sql
		select #hwg.* from hwg
		where @hwg.hwg
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_hwg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_hwg
parameter field danz smallint end

	let dcurskey[danz] = hwg.hwg

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_hwg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_hwg
parameter field dzeig smallint end

	let hwg.hwg = dcurskey[dzeig]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_hwg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_hwg gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_hwg

	execute from sql 
		update hwg
		set @hwg.*
		where current of cuu_hwg
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_hwg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_hwg gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_hwg

	let hwg.delstatus = -1

	execute from sql
		update hwg
		set @hwg.delstatus
		where current of cuu_hwg
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_hwg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_hwg

	execute from sql 
		insert into hwg (%hwg.*)
		values ($hwg.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_hwg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_hwg
	execute from sql
		select #hwg.* from hwg 
		where @hwg.hwg and
		      hwg.delstatus <> -1
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_hwg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_hwg

	let hwg.hwg = ddefakey

	perform recread_hwg returning (dsql)
	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_hwg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_hwg

	init table hwg
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_hwg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_hwg

	init hwg.hwg
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_hwg
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_hwg

	if dsavekey = hwg.hwg
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_hwg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_hwg

	let hwg.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_hwg
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_hwg

	return (hwg.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_hwg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_hwg.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_hwg

	fetch cursor cur_hwg
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_hwg 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_hwg
-			  ,cur_hwg und cus_hwg.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_hwg

	erase cursor cus_hwg
	erase cursor cuu_hwg
	erase cursor cur_hwg
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_hwg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_hwg.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_hwg

	fetch cursor cuu_hwg
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_hwg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_hwg. 
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_hwg

	close cursor cur_hwg
	return (sqlstatus) 
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeupdate_hwg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den update cursor cuu_hwg. 
-							
---------------------------------------------------------------------------*/

PROCEDURE closeupdate_hwg

	close cursor cuu_hwg
	return (sqlstatus) 
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: einfuegen_hwg
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure fuehrt die Aktionen aus,
-				  die fuer das Einfuegen eines Datensatzes
-				  in die Tabelle hwg erforderlich sind.
-
-------------------------------------------------------------------------*/

PROCEDURE einfuegen_hwg

/*----- Datensatz einfuegen oder nur update ------------------------------*/

	perform recinsert_hwg returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

	perform closeupdate_hwg returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

	perform fetchcuu_hwg returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

/*----- Defaultsatzbesetzung ---------------------------------------------*/

	perform defmaintab_hwg
	perform clrdelstatus_hwg

	return
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: defmaintab_hwg
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure besetzt die Tabellenwerte mit
-				  den Defaultwerten oder Kopierwerten
-
-------------------------------------------------------------------------*/

PROCEDURE defmaintab_hwg

	perform savekey_hwg

	perform recreaddef_hwg returning (dsql)
	if dsql 
		perform inittable_hwg
		perform disp_sql (dsql)
	end

	perform backkey_hwg
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: savekey_hwg
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure sichert den Wert des
-				  Schluesselfeldes.
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_hwg

	let dsavekey = hwg.hwg
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: backkey_hwg
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure schreibt den Wert in das 
-				  Schluesselfeld zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_hwg

	let hwg.hwg = dsavekey
END


/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscroll_hwg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscroll_hwg

	prepare scroll cursor cus_hwg from sql
		select #hwg.% from hwg
		where end
		sql hwg.hwg <> -1 and hwg.hwg > 0 end
		sql order by hwg.hwg end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchfirst_hwg 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest den ersten Satz von cursor
-			  cus_hwg.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchfirst_hwg

	fetch first cursor cus_hwg

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchnext_hwg 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest den naechsten Satz von cursor
-			  cus_hwg.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchnext_hwg

	fetch next cursor cus_hwg

	return (sqlstatus)
END


/*---------------------------------------------------------------------------
-	Procedure	: recreadpos_hwg_hwg_bz1
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure liest einen die Hauptwarengruppenbezeichnung 
-							
---------------------------------------------------------------------------*/

PROCEDURE recreadpos_hwg_hwg_bz1

	execute from sql 
		select #hwg.hwg_bz1 ,#hwg.me_einh from hwg
		where @hwg.hwg
		end

	return (sqlstatus)
END
procedure what_kennung
if "" = "@(#) mo_dbhwg.rsf	1.400	05.11.91	by RO>" end
end
