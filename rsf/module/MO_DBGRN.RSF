/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbgrndp
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	30.03.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    30.03.90   Simmchen	Modularisierung
-	#1195   1.400   08.12.92   AR		SQL-Anweisung
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle grnd_prk , grnd_prp
-
-
------------------------------------------------------------------------------
*/

module mo_dbgrn

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_grndp
	proc prepread_grndp
	proc putmxkey_grndp
	proc recupdate_grndp
	proc recdelete_grndp
	proc recinsert_grndp
	proc recread_grndp
	proc getmxkey_grndp
	proc recreaddef_grndp
	proc inittable_grndp
	proc savekey_grndp
	proc backkey_grndp
	proc initkey_grndp
	proc cmpkey_grndp
	proc clrdelstatus_grndp
	proc getdelstatus_grndp
	proc fetchcuu_grndp
	proc fetchread_grndp
	proc erasecursor_grndp
	proc closeread_grndp
	proc closeupdate_grndp

	proc prepscrollpos_grndp
	proc prepupdatepos_grndp
	proc closescrollpos_grndp
	proc closeupdatepos_grndp
	proc fetchupdatepos_grndp
	proc fetchscrollpos_grndp
	proc recupdatepos_grndp
	proc recreadpos_grndp
	proc inittablepos_grndp
	proc recreaddefpos_grndp
	proc position_einfuegen_grndp
	proc position_loeschen_grndp
	proc positionen_loeschen_grndp
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
end

IMPORT module mo_cons
end

database bws
	table grnd_prk end
	table grnd_prp end
end

#include "colaenge"

field dsavekey1	 like grnd_prk.mdn
field dsavekey2	 like grnd_prk.fil
field dsavekey3	 like grnd_prk.grnd_pr
field ddefakey   like grnd_prk.grnd_pr value -1
field dsavekeyp  like grnd_prp.a 
field ddefakeyp  like grnd_prp.a value -1
field dcurskey	 like grnd_prk.grnd_pr dimension(CMAXMATRIX)

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_grndp
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_grndp.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_grndp

	prepare cursor cuu_grndp from sql
		select #grnd_prk.* from grnd_prk
		where @grnd_prk.mdn and
		      @grnd_prk.fil and
		      @grnd_prk.grnd_pr for update
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_grndp
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cur_grndp.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_grndp

	prepare cursor cur_grndp from sql
		select #grnd_prk.* from grnd_prk
		where @grnd_prk.mdn and
		      @grnd_prk.fil and
	              @grnd_prk.grnd_pr 
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_grndp
-
-	In		: danzahl     smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_grndp

parameter field  danzahl smallint end

	let dcurskey[danzahl] = grnd_prk.grnd_pr

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_grndp
-
-	In		: dzeiger   smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_grndp

parameter field  dzeiger smallint end

	let grnd_prk.grnd_pr = dcurskey[dzeiger]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_grndp
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_grndp gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_grndp

	execute from sql 
		update grnd_prk
		set @grnd_prk.*
		where current of cuu_grndp
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_grndp
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_grndp gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_grndp

	let grnd_prk.delstatus = -1

	execute from sql
		update grnd_prk
		set @grnd_prk.delstatus
		where current of cuu_grndp
		end

	if sqlstatus
		return (sqlstatus)
	end
	
	execute from sql 
		delete from grnd_prp
		where grnd_prp.mdn = $grnd_prk.mdn and
		      grnd_prp.fil = $grnd_prk.fil and
		      grnd_prp.grnd_pr = $grnd_prk.grnd_pr
	end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_grndp
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_grndp

	execute from sql 
		insert into grnd_prk (%grnd_prk.*)
		values ($grnd_prk.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_grndp
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_grndp
	execute from sql
		select #grnd_prk.* from grnd_prk 
		where @grnd_prk.mdn and 
		      @grnd_prk.fil and
		      @grnd_prk.grnd_pr and
		      grnd_prk.delstatus <> -1
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_grndp
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_grndp

field dsql smallint
field dsavfil like grnd_prk.fil 
field dsavmdn like grnd_prk.mdn 

	let dsavfil = grnd_prk.fil
	let dsavmdn = grnd_prk.mdn

	let grnd_prk.grnd_pr = ddefakey

	perform recread_grndp returning (dsql)
	if dsql = CENSQL100
		if grnd_prk.fil <> 0
			let grnd_prk.fil = 0
			perform recread_grndp returning (dsql)
			if dsql = CENSQL100
				if grnd_prk.mdn <> 0
					let grnd_prk.mdn = 0
					perform recread_grndp returning (dsql)
				end
			end
		end
	end

	let grnd_prk.fil = dsavfil
	let grnd_prk.mdn = dsavmdn 

	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_grndp
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_grndp

	init table grnd_prk
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: savekey_grndp
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure sichert den alten Schluessel-
-			  wert.   
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_grndp

	let dsavekey1 = grnd_prk.mdn
	let dsavekey2 = grnd_prk.fil
	let dsavekey3 = grnd_prk.grnd_pr
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: backkey_grndp
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt den gesicherten  
-			  Schluesselwert zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_grndp

	let grnd_prk.mdn     = dsavekey1
	let grnd_prk.fil     = dsavekey2
	let grnd_prk.grnd_pr = dsavekey3
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_grndp
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_grndp

	init grnd_prk.grnd_pr
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_grndp
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_grndp

	if dsavekey3 = grnd_prk.grnd_pr
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_grndp
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_grndp

	let grnd_prk.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_grndp
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_grndp

	return (grnd_prk.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_grndp 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_grndp.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_grndp

	fetch cursor cur_grndp
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_grndp 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_grndp
-			  ,cur_grndp und cus_grndp.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_grndp

	erase cursor cus_grndp
	erase cursor cuu_grndp
	erase cursor cur_grndp
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_grndp 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_grndp.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_grndp

	fetch cursor cuu_grndp
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_grndp 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_grndp.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_grndp

	close cursor cur_grndp
	return (sqlstatus) 
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeupdate_grndp 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den update cursor cuu_grndp.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeupdate_grndp

	close cursor cuu_grndp
	return (sqlstatus) 
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_grndp
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_grndp

	/* #1195
	prepare scroll cursor cusmain2 from
		sql select #grnd_prp.* from grnd_prp 
		where grnd_prp.mdn     = $grnd_prk.mdn     and
		      grnd_prp.fil     = $grnd_prk.fil     and
		      grnd_prp.grnd_pr = $grnd_prk.grnd_pr end
		sql order by grnd_prp.a end
	*/
	prepare scroll cursor cusmain2 from
		sql
		select #grnd_prp.* from grnd_prp 
		where grnd_prp.mdn     = $grnd_prk.mdn     and
		      grnd_prp.fil     = $grnd_prk.fil     and
		      grnd_prp.grnd_pr = $grnd_prk.grnd_pr
		order by
			grnd_prp.a
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closescrollpos_grndp
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure schlisset den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE closescrollpos_grndp

	close cursor cusmain2
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdatepos_grndp
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor fuer
-			  die Positionen.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdatepos_grndp

	prepare cursor cuu_grndp_pos from
		sql select #grnd_prp.% from grnd_prp 
		    where @grnd_prp.mdn and
			  @grnd_prp.fil and
		          @grnd_prp.grnd_pr and
		          @grnd_prp.a for update end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeupdatepos_grndp
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure loescht den update cursor fuer
-			  die Positionen.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeupdatepos_grndp

	close cursor cuu_grndp_pos
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recreadpos_grndp

-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recreadpos_grndp
	execute from sql
		select #grnd_prp.* from grnd_prp where 
		@grnd_prp.mdn and
		@grnd_prp.fil and
		@grnd_prp.grnd_pr and
		@grnd_prp.a 
		end

	return (sqlstatus)
END
/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdatepos_grndp
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure schreibt einen Positionsaetze
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdatepos_grndp

	execute from sql
		update grnd_prp set @grnd_prp.% where current of cuu_grndp_pos
		end
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchupdatepos_grndp 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest einen Positionsaetze for update
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchupdatepos_grndp

	fetch cursor cuu_grndp_pos
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: position_loeschen_grndp
-
-
-	In			: -
-
-	Out			: sqlstatus		
-			  
-	Beschreibung		: Die Procedure loescht einen Positions- 
-				  satz aus der Datenbank.             
-
-------------------------------------------------------------------------*/

PROCEDURE position_loeschen_grndp

	execute from sql
		delete from grnd_prp
		where grnd_prp.mdn     = $grnd_prk.mdn     and
		      grnd_prp.fil     = $grnd_prk.fil     and
		      @grnd_prp.grnd_pr and
		      @grnd_prp.a 
	end
	return (sqlstatus)
END


/*-------------------------------------------------------------------------
-
-	Operatorname		: positionen_loeschen_grndp
-
-
-	In			: -
-
-	Out			: sqlstatus		
-			  
-	Beschreibung		: Die Procedure loescht alle Positions- 
-				  saetze aus der Datenbank.             
-
-------------------------------------------------------------------------*/

PROCEDURE positionen_loeschen_grndp

	execute from sql
		delete from grnd_prp
		where grnd_prp.mdn     = $grnd_prk.mdn     and
		      grnd_prp.fil     = $grnd_prk.fil     and
		      grnd_prp.grnd_pr = $grnd_prk.grnd_pr
	end
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: position_einfuegen_grndp
-
-
-	In			: -
-
-	Out			: dsqlstatus		
-			  
-	Beschreibung		: Die Procedure fuegt einen Positionssatz
-				  in die Datenbanktabelle ein.          
-
-------------------------------------------------------------------------*/

PROCEDURE position_einfuegen_grndp

	field dsqlstatus smallint

	execute from sql 
		insert into grnd_prp (%grnd_prp.*)
		values ($grnd_prp.*)
	end

	let dsqlstatus = sqlstatus
	if sqlstatus
		if dsqlstatus <> CENSQL239
			execute from sql
				rollback work
			end
		else
			execute from sql
				update grnd_prp set @grnd_prp.*
				where @grnd_prp.mdn and
		      		      @grnd_prp.fil and
		                      @grnd_prp.grnd_pr and
		                      @grnd_prp.a
			end
			let dsqlstatus = sqlstatus
		end
	end
	return (dsqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchscrollpos_grndp
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest alle Positionsaetze
-			  ueber den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchscrollpos_grndp
parameter
	field curmod smallint
end
	if curmod = CRDFIRST
		fetch first cursor cusmain2
	elseif curmod = CRDNEXT
		fetch next cursor cusmain2
	end
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorplnr	: recreaddefpos_grndp
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddefpos_grndp

field dsql smallint
field dsavfil like grnd_prp.fil 
field dsavmdn like grnd_prp.mdn 

	let dsavfil = grnd_prp.fil
	let dsavmdn = grnd_prp.mdn

	init table grnd_prp

	let grnd_prp.grnd_pr = -1
	let grnd_prp.a       = -1

	perform recreadpos_grndp returning (dsql)
	if dsql = CENSQL100
		if grnd_prp.fil <> 0
			let grnd_prp.fil = 0
			perform recreadpos_grndp returning (dsql)
			if dsql = CENSQL100
				if grnd_prp.mdn <> 0
					let grnd_prp.mdn = 0
					perform recreadpos_grndp
							 returning (dsql)
				end
			end
		end
	end

	let grnd_prp.fil = dsavfil
	let grnd_prp.mdn = dsavmdn 

	return (0)
END

/*-------------------------------------------------------------------------
-
-	Operatorplnr	: inittablepos_grndp
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittablepos_grndp

	init table grnd_prp
END
procedure what_kennung
if "" = "@(#) mo_dbgrndp.rsf	1.400	08.12.92	by AR" end
end
