/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbrabst
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	21.03.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    21.03.90   Simmchen	Modularisierung
	#2	1.00	09-04-90   Knippel 	Anpassung Rabattstufe
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle rab_stufek , rab_stufep 
-
-
------------------------------------------------------------------------------
*/

module mo_dbrab

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_rabst
	proc prepread_rabst
	proc putmxkey_rabst
	proc recupdate_rabst
	proc recdelete_rabst
	proc recinsert_rabst
	proc recread_rabst
	proc recread_rabst_pos
	proc getmxkey_rabst
	proc recreaddef_rabst
	proc inittable_rabst
	proc savekey_rabst
	proc backkey_rabst
	proc initkey_rabst
	proc cmpkey_rabst
	proc clrdelstatus_rabst
	proc getdelstatus_rabst
	proc fetchcuu_rabst
	proc fetchread_rabst
	proc erasecursor_rabst
	proc closeread_rabst
	proc fetchfirst_rabst
	proc fetchnext_rabst

/*
 	proc get_rabatt
*/

end

/*----- Import Section ------------------------------------------------------*/



IMPORT module mo_cons
end

database bws
	table rab_stufek end
	table rab_stufep end
end

#include "colaenge"

field dsavekey  	 like rab_stufek.rab_nr 
field dsavekey1 	 like rab_stufek.mdn
field dsavekey2 	 like rab_stufek.fil
field ddefakey   like rab_stufek.rab_nr value "-1"
field dsavekeyp 	 like rab_stufep.rab_nr 
field dsavekeyp1 	 like rab_stufep.mdn
field dsavekeyp2 	 like rab_stufep.fil
field ddefakeyp  like rab_stufep.rab_nr value "-1"
field dcurskey	 like rab_stufek.rab_nr dimension(CMAXMATRIX)

CONSTANT
	CRAB_TYP value "K"
END
/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_rabst
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_rabst.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_rabst

	let rab_stufek.rab_typ = CRAB_TYP

	prepare cursor cuu_rabst from sql
		select #rab_stufek.* from rab_stufek
		where @rab_stufek.rab_nr 
		and   @rab_stufek.mdn 
		and   @rab_stufek.fil 
		and   @rab_stufek.rab_typ for update
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_rabst
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cur_rabst.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_rabst

	let rab_stufek.rab_typ = CRAB_TYP
	prepare cursor cur_rabst from sql
		select #rab_stufek.* from rab_stufek
		where @rab_stufek.rab_nr 
		and   @rab_stufek.mdn 
		and   @rab_stufek.fil 
		and   @rab_stufek.rab_typ
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_rabst
-
-	In		: danzahl    smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_rabst

parameter field danzahl smallint end

	let dcurskey[danzahl] = rab_stufek.rab_nr

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_rabst
-
-	In		: dzeiger   smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_rabst

parameter field dzeiger smallint end

	let rab_stufek.rab_nr = dcurskey[dzeiger]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_rabst
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_rabst gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_rabst

	let rab_stufek.ae_dat = SYSDATE
	let rab_stufek.rab_typ = CRAB_TYP
	execute from sql 
		update rab_stufek
		set @rab_stufek.*
		where current of cuu_rabst
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_rabst
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_rabst gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_rabst

	let rab_stufek.delstatus = -1
	let rab_stufek.rab_typ = CRAB_TYP

	execute from sql
		update rab_stufek
		set @rab_stufek.delstatus
		where current of cuu_rabst
		end

	if sqlstatus
		return (sqlstatus)
	end

	let rab_stufek.rab_typ = CRAB_TYP
	
	execute from sql 
		delete from rab_stufep
		where rab_stufep.rab_nr  =
		     $rab_stufek.rab_nr 
		and   @rab_stufep.mdn 
		and   @rab_stufep.fil 
		and   rab_stufep.rab_typ =
		      $rab_stufek.rab_typ
	end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_rabst
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_rabst
	let rab_stufek.rab_typ = CRAB_TYP

	execute from sql 
		insert into rab_stufek (%rab_stufek.*)
		values ($rab_stufek.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_rabst
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_rabst
	let rab_stufek.rab_typ = CRAB_TYP
	execute from sql
		select #rab_stufek.* from rab_stufek 
		where @rab_stufek.rab_nr 
		and   @rab_stufek.mdn
		and   @rab_stufek.fil
		and   @rab_stufek.rab_typ 
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_rabst_pos
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_rabst_pos
	let rab_stufep.rab_typ = CRAB_TYP
	execute from sql
		select #rab_stufep.* from rab_stufep where 
		@rab_stufep.rab_nr and
		@rab_stufep.mdn and 
		@rab_stufep.fil and 
		@rab_stufep.rab_typ
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_rabst
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_rabst

field dsql smallint

	let rab_stufek.rab_typ = CRAB_TYP
	let rab_stufek.rab_nr  = ddefakey

	perform recread_rabst returning (dsql)
	if dsql =100 
		let rab_stufek.mdn = 0
		perform recread_rabst returning (dsql)
		if dsql =100 
			let rab_stufek.mdn = 0
			let rab_stufek.fil = 0
			perform recread_rabst returning (dsql)
		end
	end
	
	let rab_stufek.ae_dat = SYSDATE
	let rab_stufek.rab_typ = CRAB_TYP
	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_rabst
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_rabst

	init table rab_stufek
	let rab_stufek.rab_typ = CRAB_TYP

END

/*-------------------------------------------------------------------------
-
-	Operatorname	: savekey_rabst
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure sichert den alten Schluessel-
-			  wert.   
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_rabst

	let dsavekey = rab_stufek.rab_nr
	let dsavekeyp1 	 = rab_stufek.mdn
	let dsavekeyp2 	 = rab_stufek.fil
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: backkey_rabst
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt den gesicherten  
-			  Schluesselwert zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_rabst

	let rab_stufek.rab_nr = dsavekey
	let rab_stufek.rab_typ = CRAB_TYP
	let  rab_stufek.mdn = dsavekeyp1 	
	let  rab_stufek.fil = dsavekeyp2 	
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_rabst
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_rabst

	init rab_stufek.rab_nr
	let rab_stufek.rab_typ = CRAB_TYP
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_rabst
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_rabst

	if dsavekey = rab_stufek.rab_nr
	and dsavekeyp1 	 = rab_stufek.mdn
	and dsavekeyp2 	 = rab_stufek.fil
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_rabst
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_rabst

	let rab_stufek.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_rabst
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_rabst

	return (rab_stufek.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_rabst 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_rabst.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_rabst

	fetch cursor cur_rabst
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_rabst 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_rabst
-			  ,cur_rabst und cus_rabst.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_rabst

	erase cursor cus_rabst
	erase cursor cuu_rabst
	erase cursor cur_rabst
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_rabst 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_rabst.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_rabst

	fetch cursor cuu_rabst
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_rabst 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_rabst.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_rabst

	close cursor cur_rabst
	return (sqlstatus) 
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchfirst_rabst 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest den ersten Satz von cursor
-			  cus_rabst.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchfirst_rabst

	fetch first cursor cus_rabst

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchnext_rabst 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest den naechsten Satz von cursor
-			  cus_auf.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchnext_rabst

	fetch next cursor cus_rabst

	return (sqlstatus)
END
procedure what_kennung
if "" = "@(#) mo_dbrabst.rsf	1.400	05.11.91	by KB>" end
end
