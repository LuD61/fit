/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_rekalk
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	19.06.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    19.06.90   Simmchen	Modularisierung
-	#2	1.00	27.11.90   Simmchen     a_typ2 = 1
-	#3	1.00	12.02.91   Simmchen     neue Proceduren
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Tabellen a_kalk_*  fuer Preiskalkulationen
-				    auf Grundlage der Zerlegung,Rezeptur,
-				    Handelsware
-
------------------------------------------------------------------------------
*/

module mo_rekal

/*----- Export Section ------------------------------------------------------*/

EXPORT
	proc prepscrollpos_a_kalk_hwg_hndw
	proc prepscrollpos_a_kalk_wg_hndw
	proc prepscrollpos_a_kalk_ag_hndw
	proc prepscrollpos_a_kalk_hwg
	proc prepscrollpos_a_kalk_wg
	proc prepscrollpos_a_kalk_ag
	proc fetchscrollpos_a_kalkhndw
	proc closescrollpos_a_kalkhndw

	proc prepscrollpos_a_kalk_eig_rez
	proc prepscrollpos_a_kalk_eig_schnitt
	proc fetchscrollpos_a_kalk_eig
	proc closescrollpos_a_kalk_eig

/* #3 A */
	proc prepscrollpos_kalk_hwg
	proc prepscrollpos_kalk_wg
	proc prepscrollpos_kalk_ag
	proc prepscrollpos_kalk_rez
	proc prepscrollpos_kalk_schnitt
	proc fetchscrollpos_kalk
	proc closescrollpos_kalk

/* #3 E */

end

/*----- Import Section ------------------------------------------------------*/

import module mo_cons
end

import module mo_dba_e
	database bws
end

import module mo_dbscn
	database bws
end

import module mo_dba_m
	database bws
end

import module mo_dbhwg
	database bws
end

import module mo_dbwg
	database bws
end

import module mo_dbag
	database bws
end

import module mo_dba_b
	database bws
end

import module mo_dbake
	database bws
end

import module mo_dbakh
	database bws
end

#include "colaenge"

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_a_kalk_hwg_hndw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_hndw nach Hauptwarengruppe.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_a_kalk_hwg_hndw

	prepare scroll cursor cus_kalk_hndw from sql
		select #a_bas.a,#a_bas.a_typ,#a_bas.a_typ2,
		       #a_bas.a_bz1,#a_bas.a_bz2,
		       #a_bas.kost_kz,
		       #a_kalkhndw.%
		      from a_bas,a_kalkhndw
		where a_bas.hwg      = $hwg.hwg and
		      (a_bas.a_typ = 1 or a_bas.a_typ2 = 1) and
		      @a_kalkhndw.mdn and
		      @a_kalkhndw.fil and
		      a_kalkhndw.a  = a_bas.a 
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_a_kalk_hwg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_hndw nach Hauptwarengruppe.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_a_kalk_hwg

	prepare scroll cursor cus_kalk_hndw from sql
		select #a_bas.a,#a_bas.a_typ,#a_bas.a_typ2,
		       #a_bas.a_bz1,#a_bas.a_bz2,
		       #a_bas.kost_kz
		      from a_bas
		where a_bas.hwg      = $hwg.hwg and
		      a_bas.a_typ < 12
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_kalk_hwg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_kalk nach Hauptwarengruppe.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_kalk_hwg

	prepare scroll cursor cus_kalk from sql
		select #a_bas.a,#a_bas.a_typ,#a_bas.a_typ2,
		       #a_bas.a_bz1,#a_bas.a_bz2,
		       #a_bas.kost_kz
		      from a_bas
		where a_bas.hwg      = $hwg.hwg and
		      a_bas.a_typ < 12
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_a_kalk_wg_hndw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_hndw nach Warengruppe.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_a_kalk_wg_hndw

	prepare scroll cursor cus_kalk_hndw from sql
		select #a_bas.a,#a_bas.a_typ,#a_bas.a_typ2,
		       #a_bas.a_bz1,#a_bas.a_bz2,
		       #a_bas.kost_kz,
		       #a_kalkhndw.%
		      from a_bas,a_kalkhndw
		where a_bas.wg      = $wg.wg and
		      (a_bas.a_typ = 1 or a_bas.a_typ2 = 1) and
		      @a_kalkhndw.mdn and
		      @a_kalkhndw.fil and
		      a_kalkhndw.a = a_bas.a 
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_a_kalk_wg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_hndw nach Warengruppe.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_a_kalk_wg

	prepare scroll cursor cus_kalk_hndw from sql
		select #a_bas.a,#a_bas.a_typ,#a_bas.a_typ2,
		       #a_bas.a_bz1,#a_bas.a_bz2,
		       #a_bas.kost_kz
		      from a_bas
		where a_bas.wg      = $wg.wg and
		      a_bas.a_typ < 12
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_kalk_wg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_kalk nach Warengruppe.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_kalk_wg

	prepare scroll cursor cus_kalk from sql
		select #a_bas.a,#a_bas.a_typ,#a_bas.a_typ2,
		       #a_bas.a_bz1,#a_bas.a_bz2,
		       #a_bas.kost_kz
		      from a_bas
		where a_bas.wg      = $wg.wg and
		      a_bas.a_typ < 12
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_a_kalk_ag_hndw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_hndw nach Artikelgruppe.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_a_kalk_ag_hndw

	prepare scroll cursor cus_kalk_hndw from sql
		select #a_bas.a,#a_bas.a_typ,#a_bas.a_typ2,
		       #a_bas.a_bz1,#a_bas.a_bz2,
		       #a_bas.kost_kz,
		       #a_kalkhndw.%
		      from a_bas,a_hndw,a_kalkhndw
		where a_bas.ag      = $ag.ag and
		      (a_bas.a_typ = 1 or a_bas.a_typ2 = 1) and
		      @a_kalkhndw.mdn and
		      @a_kalkhndw.fil and
		      a_kalkhndw.a = a_bas.a 
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_a_kalk_ag
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_hndw nach Artikelgruppe.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_a_kalk_ag

	prepare scroll cursor cus_kalk_hndw from sql
		select #a_bas.a,#a_bas.a_typ,#a_bas.a_typ2,
		       #a_bas.a_bz1,#a_bas.a_bz2,
		       #a_bas.kost_kz
		      from a_bas
		where a_bas.ag      = $ag.ag and
		      a_bas.a_typ < 12
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_kalk_ag
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_kalk nach Warengruppe.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_kalk_ag

	prepare scroll cursor cus_kalk from sql
		select #a_bas.a,#a_bas.a_typ,#a_bas.a_typ2,
		       #a_bas.a_bz1,#a_bas.a_bz2,
		       #a_bas.kost_kz
		      from a_bas
		where a_bas.ag      = $ag.ag and
		      a_bas.a_typ < 12
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closescrollpos_kalk
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schlieste den read cursor
-			  cus_kalk
-							
---------------------------------------------------------------------------*/

PROCEDURE closescrollpos_kalk

	close cursor cus_kalk
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closescrollpos_a_kalkhndw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schlieste den read cursor
-			  cus_hndw
-							
---------------------------------------------------------------------------*/

PROCEDURE closescrollpos_a_kalkhndw

	close cursor cus_kalk_hndw
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchscrollpos_kalk
-
-	In		: dmode		smallint
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Satz ueber den cursor
-			  cus_kalk
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchscrollpos_kalk

PARAMETER
	field dmode smallint
END

	if dmode = CRDFIRST
		fetch first cursor cus_kalk
	else
		fetch next cursor cus_kalk 
	end
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchscrollpos_a_kalkhndw
-
-	In		: dmode		smallint
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Satz ueber den cursor
-			  cus_kalk_hndw
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchscrollpos_a_kalkhndw

PARAMETER
	field dmode smallint
END

	if dmode = CRDFIRST
		fetch first cursor cus_kalk_hndw
	else
		fetch next cursor cus_kalk_hndw 
	end
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_a_kalk_eig_rez
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_kalk_eig nach Eigenprodukten/Rezeptur.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_a_kalk_eig_rez

	prepare scroll cursor cus_kalk_eig from sql
		select #a_eig.rez,#a_eig.a,
		       #a_kalk_eig.%,
		       #a_bas.a,#a_bas.a_bz1,#a_bas.a_bz2,#a_bas.kost_kz
		       from a_eig,a_kalk_eig,a_bas
		where @a_eig.rez and
		      @a_kalk_eig.mdn and
		      @a_kalk_eig.fil and
		      a_kalk_eig.a = a_bas.a and
		      a_bas.a = a_eig.a
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_kalk_rez
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_kalk nach Eigenprodukten/Rezeptur.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_kalk_rez

	prepare scroll cursor cus_kalk from sql
		select #a_eig.rez,#a_eig.a,
		       #a_bas.a,#a_bas.a_bz1,#a_bas.a_bz2,#a_bas.kost_kz,
		       #a_bas.a_typ,#a_bas.a_typ2
		       from a_eig,a_bas
		where @a_eig.rez and
		      a_bas.a = a_eig.a
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_a_kalk_eig_schnitt
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_kalk_eig nach Eigenprodukten/Zerlegung.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_a_kalk_eig_schnitt

	prepare scroll cursor cus_kalk_eig from sql
		select distinct
		       #zerm.schnitt,#zerm.zerm_mat,
		       #a_mat.mat,#a_mat.a,
		       #a_kalk_eig.%,
		       #a_bas.a,#a_bas.a_bz1,#a_bas.a_bz2,#a_bas.kost_kz
		from   zerm,a_mat,a_kalk_eig,a_bas
		where  @zerm.schnitt and
		       a_mat.mat = zerm.zerm_mat and
		       @a_kalk_eig.mdn and
		       @a_kalk_eig.fil and
		       a_kalk_eig.a = a_mat.a and
		       a_bas.a = a_kalk_eig.a
		order by a_bas.a end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_kalk_schnitt
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cus_kalk nach Eigenprodukten/Zerlegung.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_kalk_schnitt

	prepare scroll cursor cus_kalk from sql
		select distinct
		       #zerm.schnitt,#zerm.zerm_mat,
		       #a_mat.mat,#a_mat.a,
		       #a_bas.a,#a_bas.a_bz1,#a_bas.a_bz2,#a_bas.kost_kz,
		       #a_bas.a_typ,#a_bas.a_typ2
		from   zerm,a_mat,a_bas
		where  @zerm.schnitt and
		       a_mat.mat = zerm.zerm_mat and
		       a_bas.a = a_mat.a
		order by a_bas.a end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closescrollpos_a_kalk_eig
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schlieste den read cursor
-			  cus_kalk_eig
-							
---------------------------------------------------------------------------*/

PROCEDURE closescrollpos_a_kalk_eig

	close cursor cus_kalk_eig
END
/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchscrollpos_a_kalk_eig
-
-	In		: dmode		smallint
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Satz ueber den cursor
-			  cus_kalk_eig
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchscrollpos_a_kalk_eig

PARAMETER
	field dmode smallint
END

	if dmode = CRDFIRST
		fetch first cursor cus_kalk_eig
	else
		fetch next cursor cus_kalk_eig 
	end
	return (sqlstatus)
END

procedure what_kennung
if "" = "@(#) mo_rekalk.rsf	1.400	05.11.91	by AR" end
end
