/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbkoart
-
-	Externe Operatoren	:	-
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	22.02.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1	1.00	22.02.90   Simmchen	Modularisierung
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle kost_art
-
-
------------------------------------------------------------------------------
*/

module mo_dbkoa

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_koart
	proc prepread_koart
	proc putmxkey_koart
	proc recupdate_koart
	proc recdelete_koart
	proc recinsert_koart
	proc recread_koart
	proc getmxkey_koart
	proc recreaddef_koart
	proc inittable_koart
	proc backkey_koart
	proc savekey_koart
	proc initkey_koart
	proc cmpkey_koart
	proc clrdelstatus_koart
	proc getdelstatus_koart
	proc fetchcuu_koart
	proc fetchread_koart
	proc erasecursor_koart
	proc closeread_koart
	proc closeupdate_koart
	proc einfuegen_koart
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
	field dret
end

IMPORT module mo_cons
end

IMPORT module mo_meld
	proc disp_sql
	proc disp_msg
end

#include "colaenge"

database bws
	table    kost_art end
end

field dsavekey1	 like kost_art.mdn 
field dsavekey2	 like kost_art.fil 
field dsavekey3	 like kost_art.kost_art 
field ddefakey   like kost_art.kost_art value "-1"
field dcurskey	 like kost_art.kost_art dimension(CMAXMATRIX)
field dsql	 smallint

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_koart
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_koart.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_koart

/* Ro 06.12.93    
	prepare cursor cuu_koart from sql
		select #kost_art.% from kost_art
		where @kost_art.mdn and 
		      @kost_art.fil and
		      @kost_art.kost_art for update 
		end
*/
/* Ro 06.12.93    */
if "" = "@(#) mo_dbkoart.rsf	1.402	06.12.93	by RO" end
	prepare cursor cuu_koart from sql
		select #kost_art.% from kost_art
		where @kost_art.kost_art for update 
		end
/* Ro 06.12.93    */

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_koart
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor cur_koart.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_koart

/* Ro 06.12.93
	prepare cursor cur_koart from sql
		select #kost_art.* from kost_art
		where @kost_art.mdn and
		      @kost_art.fil and
		      @kost_art.kost_art 
		end
*/

/* Ro 06.12.93 */
	prepare cursor cur_koart from sql
		select #kost_art.* from kost_art
		where @kost_art.kost_art 
		end
/* Ro 06.12.93 */
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_koart
-
-	In		: danzahl   smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_koart

parameter field danzahl smallint end

	let dcurskey[danzahl] = kost_art.kost_art

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_koart
-
-	In		: dzeiger smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_koart

parameter field dzeiger smallint end

	let kost_art.kost_art = dcurskey[dzeiger]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_koart
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_koart gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_koart

	execute from sql 
		update kost_art
		set @kost_art.*
		where current of cuu_koart
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_koart
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_koart gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_koart

	let kost_art.delstatus = -1

	execute from sql
		update kost_art
		set @kost_art.delstatus
		where current of cuu_koart
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_koart
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_koart

	execute from sql 
		insert into kost_art (%kost_art.*)
		values ($kost_art.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_koart
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_koart

/* Ro 06.12.93 
	execute from sql
		select #kost_art.* from kost_art where
		@kost_art.mdn and 
	        @kost_art.fil and
	        @kost_art.kost_art and
		kost_art.delstatus <> -1
		end
*/

/* Ro 06.12.93  */
	execute from sql
		select #kost_art.* from kost_art where
	        @kost_art.kost_art and
		kost_art.delstatus <> -1
		end
/* Ro 06.12.93  */

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_koart
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_koart

field dsavfil like kost_art.fil 
field dsavmdn like kost_art.mdn 

	let dsavfil = kost_art.fil
	let dsavmdn = kost_art.mdn

	let kost_art.kost_art = ddefakey

	perform recread_koart returning (dsql)
	if dsql = CENSQL100
		if kost_art.fil <> 0
			let kost_art.fil = 0
			perform recread_koart returning (dsql)
			if dsql = CENSQL100
				if kost_art.mdn <> 0
					let kost_art.mdn = 0
					perform recread_koart returning (dsql)
				end
			end
		end
	end
	let kost_art.fil = dsavfil
	let kost_art.mdn = dsavmdn 
	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_koart
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_koart

	init table kost_art
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_koart
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_koart

	init kost_art.kost_art
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_koart
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_koart

	if dsavekey3 = kost_art.kost_art
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_koart
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_koart

	let kost_art.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_koart
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_koart

	return (kost_art.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_koart
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_koart.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_koart

	fetch cursor cur_koart
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_koart 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_koart
-			  ,cur_koart und cus_koart.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_koart

	erase cursor cus_koart
	erase cursor cuu_koart
	erase cursor cur_koart
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_koart 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_koart.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_koart

	fetch cursor cuu_koart
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_koart 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_koart. 
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_koart

	close cursor cur_koart
	return (sqlstatus) 
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeupdate_koart 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den update cursor cuu_koart. 
-							
---------------------------------------------------------------------------*/

PROCEDURE closeupdate_koart

	close cursor cuu_koart
	return (sqlstatus) 
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: einfuegen_koart
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure fuehrt die Aktionen aus,
-				  die fuer das Einfuegen eines Datensatzes
-				  in die Tabelle kost_art erforderlich sind.
-
-------------------------------------------------------------------------*/

PROCEDURE einfuegen_koart

/*----- Datensatz einfuegen oder nur update ------------------------------*/

	perform recinsert_koart returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

	perform closeupdate_koart returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

	perform fetchcuu_koart returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

/*----- Defaultsatzbesetzung ---------------------------------------------*/

	perform defmaintab_koart
	perform clrdelstatus_koart

	return
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: defmaintab_koart
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure besetzt die Tabellenwerte mit
-				  den Defaultwerten oder Kopierwerten
-
-------------------------------------------------------------------------*/

PROCEDURE defmaintab_koart

	perform savekey_koart

	perform recreaddef_koart returning (dsql)
	if dsql 
		perform inittable_koart
		perform disp_sql (dsql)
	end

	perform backkey_koart
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: savekey_koart
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure sichert den Wert des
-				  Schluesselfeldes.
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_koart

	let dsavekey1 = kost_art.mdn
	let dsavekey2 = kost_art.fil
	let dsavekey3 = kost_art.kost_art
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: backkey_koart
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure schreibt den Wert in das 
-				  Schluesselfeld zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_koart

	let kost_art.mdn      = dsavekey1
	let kost_art.fil      = dsavekey2
	let kost_art.kost_art = dsavekey3
END

procedure what_kennung
if "" = "@(#) mo_dbkoart.rsf	1.402	05.11.91	by BSA>" end
end
