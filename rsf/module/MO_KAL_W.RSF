module mo_kal_w

/***
--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       mo_kal_wo.rsf  
-
        Exports                 :       proc kal_woche       

-       Externe Prozeduren      :       
-
-       Autor                   :       B. Beck
-       Erstellungsdatum        :       24.10.90
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-               21.12.90                B.Beck     abgeschlossen und uebergeben
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Berechnet aus Datumsparameter die Kalender-
                                    woche

        Eingabe                 :   datum     date 
        Ausgabe                 :   kalwoch   smallint
-
--------------------------------------------------------------------------------
***/

/** Exports                   **/

export
       proc kal_woche
end

/**
/*-------------------------------------------------------------------*/
/* 	Programm Kalenderwoche                                       */
/*-------------------------------------------------------------------*/
	Diese Routine berechnet die Kalenderwoche aus dem Datum     
**/

proc kal_woche
parameter  
field    datum          date      /* Uebergabe des aktuellen Datums   */
end

field    jahrtage       date      /* Umwandlung des Jahres in Tage    */
field    vjahrtage      date      /* Umwandlung des Vorjahres in Tage */
field    wochtag        smallint  /* Wochentag des 01.01. im Jahr     */
                                  /*So=0,Mo=1,Di=2,Mi=3,Do=4,Fr=5,Sa=6*/
field    vwochtag       smallint  /* Wochentag des 01.01. im Vorjahr  */
field    nwochtag       smallint  /* Wochentag des 01.01. im Nach.jahr*/
field    tage           smallint  /* Tage fuer Kalenderwochenrechnung */
field    kalwoch        smallint  /* Kalenderwoche                    */

let jahrtage  = "01.01."#year (datum)
let vjahrtage = "01.01."#(year (datum) - 1)
let wochtag  =  weekday (jahrtage)
let vwochtag =  weekday (vjahrtage)
let nwochtag =  weekday ("01.01."#(year (datum) + 1))
                                  /*Mo=1,Di=2,Mi=3,Do=4,Fr=5,Sa=6,So=7*/
if  wochtag = 0  let  wochtag  = 7 end
if vwochtag = 0  let vwochtag  = 7 end
if wochtag > 4    /* Fr,Sa oder So gehoert die Woche zum Vorjahr */
   if datum < (jahrtage + 8 - wochtag) /* Liegt Datum vor KW 1 */
       if vwochtag > 4            /* Wochentag des 01.01. vom Vorjahr */
                                  /* dann ist es KW = 53 */
          let tage = datum - vjahrtage + vwochtag - 8
       else
                                  /* KW = 52 */
          let tage = datum - vjahrtage + vwochtag - 1
       end
   else                           /* KW = 1 bis 52 oder KW1 von naechstem Jahr*/
       let tage = datum - jahrtage + wochtag - 7
       if tage > 364 and nwochtag < 5       /* 52 * 7 = 364 */
          let tage = 1            /* KW = 1 von naechstem Jahr */
       end
   end
else                              /* Woche von 1.1. ist bereits in KW = 1 */
   let tage = datum - jahrtage + wochtag /* KW = 1 bis KW = 53 */
   if tage > 364 and nwochtag < 5 /* KW1 von naechstem Jahr */
      let tage = 1
   end
end
let kalwoch = (tage + 6) / 7      /* KW berechnen, ohne KW0 */
return (kalwoch)
end /* kal_woche */
procedure what_kennung
if "" ="@(#) mo_kal_wo.rsf 	1.400	09.12.92	by XM" end
end
