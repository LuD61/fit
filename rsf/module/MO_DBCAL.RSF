/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbcal
-
-	Interne Operatoren	:	proc prepupdate	
-					proc prepread
-					proc prepscroll
-					proc putmxkey
-					proc recupdate
-					proc recdelete
-					proc recinsert
-					proc recread
-					proc getmxkey
-					proc recreaddef
-					proc inittable
-					proc savekey
-					proc backkey
-					proc initkey
-					proc cmpkey
-					proc clrdelstatus
-					proc getdelstatus
-					proc getauswst
-					proc initform
-					proc dispform
-
-	Externe Operatoren	:	-
-
-	Autor			:	Juergen Simmchen / Axel Seichter
-	Erstellungsdatum	:	16.01.90
-	Modifikationsdatum	:	09.02.90
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    16.01.90   Simmchen	Modularisierung
-	#2      2.00    09.02.90   Seichter	weitere Modularisierung und
-						Anpassung an Vertreterstamm
-	#2      3.00    16.02.90   Kraushaar	Anpassung an Leitsaetze
-	#3		?.?11-05-90 Knippel     Ergaenzung um proc read_cal_wt1
-	#3		12.04.91   Seichter     Tuning der proc read_cal_wt1
-	#1990	1.402h	31.03.9	JG	read_cal_wt2 ergaenzt 
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-				    fuer Tabelle cal
-
-
------------------------------------------------------------------------------
*/

module mo_dbcal

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws


	proc prepupdate_cal
	proc prepread_cal
	proc putmxkey_cal
	proc recupdate_cal
	proc recdelete_cal
	proc recinsert_cal
	proc recread_cal
	proc getmxkey_cal
	proc recreaddef_cal
	proc inittable_cal
	proc initkey_cal
	proc cmpkey_cal
	proc backkey_cal
	proc savekey_cal
	proc clrdelstatus_cal
	proc getdelstatus_cal
	proc fetchcuu_cal
	proc fetchread_cal
	proc erasecursor_cal
	proc closeread_cal
	proc read_cal_wt2	/* #1990 */
		/* jg lese naechsten Werktag auf bel.  Anfangsdatum !! */
	proc read_cal_wt1		/* kn lese naechsten Werktag */
	proc read_cal_wt		/* kn lese naechsten Werktag */
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
end

IMPORT module mo_cons
end


#include "colaenge"

database bws
	table    cal		end
end

field dsavekey 	 like cal.cal 
field ddefakey   like cal.cal value "01.01.1900"        // J2000
field dcurskey	 like cal.cal dimension(CMAXMATRIX)
field danzahl	 smallint
field dzeiger	 smallint 
field cal_text	 char(56) 
field jahr       smallint

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_cal
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor cuu_cal.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_cal

	prepare cursor cuu_cal from sql
		select #cal.* from cal
		where @cal.cal for update
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
e									  
-	Procedure	: prepread_cal
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor cur_cal.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_cal

	prepare cursor cur_cal from sql
		select #cal.* from cal
		where @cal.cal
		end

	return (sqlstatus)
END
/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_cal
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_cal

		let dcurskey[danzahl] = cal.cal

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_cal 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_cal

		let cal.cal = dcurskey[dzeiger]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_cal 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_cal gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_cal

	execute from sql 
		update cal
		set @cal.*
		where current of cuu_cal
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_cal 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_cal gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_cal

	let cal.delstatus = -1

	execute from sql
		update cal
		set @cal.delstatus
		where current of cuu_cal
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_cal 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_cal

	execute from sql 
		insert into cal (%cal.*)
		values ($cal.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_cal 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_cal
	execute from sql
		select #cal.* from cal where @cal.cal
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_cal
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_cal

field dsql smallint

	let cal.cal = ddefakey

	perform recread_cal returning (dsql)
	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_cal
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_cal  

	init table cal
END


/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_cal
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_cal

	init cal.cal
END



/*-------------------------------------------------------------------------
-
-	Operatorname	: savekey_cal
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure sichert den alten Schluessel-
-			  wert.   
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_cal

	let dsavekey = cal.cal
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: backkey_cal
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt den gesicherten  
-			  Schluesselwert zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_cal

	let cal.cal = dsavekey
END


/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_cal
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_cal

	if dsavekey = cal.cal
		return (CTRUE)
	end

	return (CFALSE)
END
/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_cal
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_cal

	let cal.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_cal
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_cal

	return (cal.delstatus)
END


/*-------------------------------------------------------------------------
-
-	Operatorname	: getauswst
-
-	In		: dstatus		smallint
-						0   Werte aus table
-						<>0 Initstring
-
-	Out		: Laenge des Schluesselfeldes (als konkreter Wert)
-						smallint
-			  mxstring		char	  zusammengesetzter 
-							  String aus im Matrix-
-							  menue auszugebenden
-							  Feldern 
-							  (max. 80 Zeichen)
-			  danzahl		smallint  Anzahl der Datensaetze
-						          einschl. geloeschte 
-							  Saetze
-			  
-	Beschreibung	: Die Procedure liest cursor cus_cal absolut
-			  und gibt string fuer matrixmenue zurueck
-
-------------------------------------------------------------------------*/

PROCEDURE getauswst

parameter field dstatus		smallint	end

field dummyc char (8)           // J2000

	if dstatus
		return (8, " ", danzahl) 
	end
// J2000 
        let dummyc = PICTURE ( cal.cal, "dd.mm.yy")
//        return (8, cal.cal # " " 
        return (8, dummyc # " " 
			# cal.tag_kenn # " ", danzahl)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_cal 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_cal.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_cal

	fetch cursor cur_cal
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_cal 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_cal
-			  ,cur_cal und cus_cal.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_cal

	erase cursor cus_cal
	erase cursor cuu_cal
	erase cursor cur_cal
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_cal 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_cal.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_cal

	fetch cursor cuu_cal
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_cal 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_cal.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_cal

	close cursor cur_cal
	return (sqlstatus) 
END
/* kn 11-05-90 */

/*---------------------------------------------------------------------------
-									  
-	Procedure	: read_cal_wt1
-
-	In		: -
-
-	Out		: datum 
-			  
-	Beschreibung    : Die Procedure gibt den Werktag nach sysdate aus.
-			  Es wird 3-mal versucht, den naechsten Tag nach SYSDATE
-			  einzulesen und zu pruefen, ob es ein Werktag ist
-			  (schneller indizierter Zugriff). Erst wenn dabei kein
-			  Werktag gefunden wird, wird auf die alte (langsame)
-			  Art ueber min gesucht.
-							
---------------------------------------------------------------------------*/

PROCEDURE read_cal_wt1

field	dsql	smallint
field	dz	smallint
 
	let dz = 1

	while dz < 4
		let cal.cal = SYSDATE + dz
		perform recread_cal returning (dsql)
		if dsql = 0 and cal.arb_std > 0 and cal.tag_kenn = "WT"
			return (cal.cal)
		end
		let dz = dz + 1
	end
		
	let cal.cal = SYSDATE 
	execute from sql
		select min(#cal.cal) from cal
		where cal.arb_std > 0 
		and cal.tag_kenn = "WT" 
		and cal.cal > $cal.cal
		end
	if sqlstatus 
		let cal.cal = SYSDATE 
	end
	return (cal.cal )

END

/* kn 11-05-90 */

/* #1990 A */
/*---------------------------------------------------------------------------
-									  
-	Procedure	: read_cal_wt2
-
-	In		: datum, auf dem aufgesetzt werden soll ....
-
-	Out		: datum 
-			  
-	Beschreibung    : gibt den Werktag nach inputdate aus. Es wird 3-mal
-			  versucht, den naechsten Tag nach inputdate
-			  einzulesen und zu pruefen, ob es ein Werktag ist
-			  (schneller indizierter Zugriff). Erst wenn dabei kein
-			  Werktag gefunden wird, wird auf die alte (langsame)
-			  Art ueber min gesucht.
-							
---------------------------------------------------------------------------*/

PROCEDURE read_cal_wt2
parameter
	field inputdate date
end

field	dsql	smallint
field	dz	smallint
 
	let dz = 1

	while dz < 4
		let cal.cal = inputdate + dz
		perform recread_cal returning (dsql)
		if dsql = 0 and cal.arb_std > 0 and cal.tag_kenn = "WT"
			return (cal.cal)
		end
		let dz = dz + 1
	end
		
	let cal.cal = inputdate 
	execute from sql
		select min(#cal.cal) from cal
		where cal.arb_std > 0 
		and cal.tag_kenn = "WT" 
		and cal.cal > $cal.cal
		end
	if ((sqlstatus ) or (is null (cal.cal)))
		let cal.cal = inputdate 
	end

	return (cal.cal )

END

/* #1990 E*/

/* kn 21-08-90 */

/*---------------------------------------------------------------------------
-									  
-	Procedure	: read_cal_wt 
-
-	In		: -
-
-	Out		: datum 
-			  
-	Beschreibung    : Die Procedure gibt den folgenden Werktag aus.
-							
---------------------------------------------------------------------------*/

PROCEDURE read_cal_wt
parameter
	field dcal like cal.cal 
end
	let cal.cal = dcal
 

	execute from sql
		select min(#cal.cal) from cal
		where cal.arb_std > 0 
		and cal.tag_kenn = "WT" 
		and cal.cal >= $cal.cal
		end
	if sqlstatus 
		let cal.cal = dcal 
	end
	return (cal.cal )

END

/* kn 11-05-90 */

procedure what_kennung
if "" = "@(#) mo_dbcal.rsf	1.402h	31.03.94	by KB>" end
end
