/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbzerw
-
-	Interne Operatoren	:	proc prepupdate_zerw
-					proc prepread_zerw
-					proc putmxkey_zerw
-					proc recupdate_zerw
-					proc recdelete_zerw
-					proc recinsert_zerw
-					proc recread_zerw
-					proc getmxkey_zerw
-					proc recreaddef_zerw
-					proc inittable_zerw
-					proc initkey_zerw
-					proc cmpkey_zerw
-					proc clrdelstatus_zerw
-					proc getdelstatus_zerw
-					proc fetchcuu_zerw
-					proc fetchread_zerw
-					proc erasecursor_zerw
-					proc closeread_zerw
-					proc closeupdate_zerw
-					proc einfuegen_zerw
-					proc defmaintab_zerw
-					proc savekey_zerw
-					proc backkey_zerw
-
-	Externe Operatoren	:	-
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	22.02.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1	1.00	22.02.90   Simmchen	Modularisierung
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle zerw
-
-
------------------------------------------------------------------------------
*/

module mo_dbzew

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_zerw
	proc prepread_zerw
	proc putmxkey_zerw
	proc recupdate_zerw
	proc recdelete_zerw
	proc recinsert_zerw
	proc recread_zerw
	proc getmxkey_zerw
	proc recreaddef_zerw
	proc inittable_zerw
	proc backkey_zerw
	proc savekey_zerw
	proc initkey_zerw
	proc cmpkey_zerw
	proc clrdelstatus_zerw
	proc getdelstatus_zerw
	proc fetchcuu_zerw
	proc fetchread_zerw
	proc erasecursor_zerw
	proc closeread_zerw
	proc closeupdate_zerw
	proc einfuegen_zerw

	proc zerwcp      
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
	field dret
end

IMPORT module mo_cons
end

IMPORT module mo_meld
	proc disp_sql
	proc disp_msg
end

#include "colaenge"

database bws
	table    zerw end
end

field dsavekey 	 like zerw.zer_part 
field dsavekey1 	 like zerw.mci_anl_nr
field dsavekey2 	 like zerw.waa_lfd  
field dsavekey3 	 like zerw.mat
field ddefakey   like zerw.zer_part value "-1"
field dcurskey	 like zerw.zer_part 	dimension(CMAXMATRIX)
field dcurskey1	 like zerw.mci_anl_nr 	dimension(CMAXMATRIX)
field dcurskey2	 like zerw.waa_lfd 	dimension(CMAXMATRIX)
field dcurskey3	 like zerw.mat 		dimension(CMAXMATRIX)
field dsql	 smallint

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_zerw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_zerw.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_zerw

	prepare cursor cuu_zerw from sql
		select #zerw.% from zerw
		where @zerw.mdn 
		and @zerw.zer_part 
		and @zerw.zer_dat
		and @zerw.mci_anl_nr
		and @zerw.waa_lfd
		for update
		end

	return (sqlstatus)

return(0)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_zerw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor cur_zerw.
-							
		and @zerw.mci_anl_nr and @zerw.mat
---------------------------------------------------------------------------*/

PROCEDURE prepread_zerw
	prepare cursor cur_zerw from sql
		select #zerw.* from zerw
		where @zerw.mdn 
		and @zerw.zer_part 
		and @zerw.zer_dat
		and @zerw.mci_anl_nr
		and @zerw.waa_lfd
		end

	return (sqlstatus)
return(0)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_zerw
-
-	In		: danzahl   smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_zerw

parameter field danzahl smallint end

	let dcurskey[danzahl] =  zerw.zer_part 	
	let dcurskey1[danzahl] =  zerw.mci_anl_nr 
	let dcurskey2[danzahl] =  zerw.waa_lfd 	
	let dcurskey3[danzahl] =  zerw.zer_dat 

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_zerw
-
-	In		: dzeiger smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_zerw

parameter field dzeiger smallint end

	let   zerw.zer_part  = dcurskey[dzeiger] 
	let   zerw.mci_anl_nr = dcurskey1[dzeiger] 
	let   zerw.waa_lfd =  	dcurskey2[dzeiger] 
	let   zerw.zer_dat =  dcurskey3[dzeiger] 

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_zerw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_zerw gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_zerw

	let zerw.kng_vdt = "H"
	execute from sql 
		update zerw
		set @zerw.*
		where current of cuu_zerw
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_zerw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_zerw gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_zerw

	let zerw.delstatus = -1

	execute from sql
		update zerw
		set @zerw.delstatus
		where current of cuu_zerw
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_zerw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_zerw

	execute from sql 
		insert into zerw (%zerw.*)
		values ($zerw.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_zerw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
		and @zerw.mci_anl_nr and @zerw.mat
---------------------------------------------------------------------------*/

PROCEDURE recread_zerw
	execute from sql
		select #zerw.* from zerw 
		where @zerw.mdn 
		and @zerw.zer_part 
		and @zerw.zer_dat
		and @zerw.mci_anl_nr
		and @zerw.waa_lfd
		end
	return (sqlstatus)

END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_zerw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_zerw

	let zerw.zer_part = ddefakey

	perform recread_zerw returning (dsql)
	return (dsql)

return(0)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_zerw
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_zerw

	init table zerw
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_zerw
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_zerw

	init zerw.zer_part
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_zerw
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_zerw

	if   dsavekey = zerw.zer_part
	and  dsavekey1 	 = zerw.mci_anl_nr 
	and  dsavekey2 	 = zerw.waa_lfd
	and  dsavekey3 	 = zerw.zer_dat
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_zerw
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_zerw

	let zerw.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_zerw
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_zerw

	return (zerw.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_zerw
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_zerw.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_zerw

	fetch cursor cur_zerw
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_zerw 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_zerw
-			  ,cur_zerw und cus_zerw.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_zerw

	erase cursor cus_zerw
	erase cursor cuu_zerw
	erase cursor cur_zerw
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_zerw 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_zerw.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_zerw

	fetch cursor cuu_zerw
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_zerw 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_zerw. 
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_zerw

	close cursor cur_zerw
	return (sqlstatus) 
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeupdate_zerw 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den update cursor cuu_zerw. 
-							
---------------------------------------------------------------------------*/

PROCEDURE closeupdate_zerw

	close cursor cuu_zerw
	return (sqlstatus) 
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: einfuegen_zerw
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure fuehrt die Aktionen aus,
-				  die fuer das Einfuegen eines Datensatzes
-				  in die Tabelle zerw erforderlich sind.
-
-------------------------------------------------------------------------*/

PROCEDURE einfuegen_zerw

/*----- Datensatz einfuegen oder nur update ------------------------------*/

	perform recinsert_zerw returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

	perform closeupdate_zerw returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

	perform fetchcuu_zerw returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

/*----- Defaultsatzbesetzung ---------------------------------------------*/

	perform defmaintab_zerw
	perform clrdelstatus_zerw

	return
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: defmaintab_zerw
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure besetzt die Tabellenwerte mit
-				  den Defaultwerten oder Kopierwerten
-
-------------------------------------------------------------------------*/

PROCEDURE defmaintab_zerw

	perform savekey_zerw

	perform recreaddef_zerw returning (dsql)
	if dsql 
		perform inittable_zerw
		perform disp_sql (dsql)
	end

	perform backkey_zerw
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: savekey_zerw
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure sichert den Wert des
-				  Schluesselfeldes.
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_zerw

	let dsavekey = zerw.zer_part
	let dsavekey1 	 = zerw.mci_anl_nr
	let dsavekey2 	 = zerw.waa_lfd  
	let dsavekey3 	 = zerw.zer_dat
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: backkey_zerw
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure schreibt den Wert in das 
-				  Schluesselfeld zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_zerw

	let zerw.zer_part = dsavekey
	let zerw.mci_anl_nr =  dsavekey1 	  
	let zerw.waa_lfd  =  dsavekey2 	  
	let zerw.zer_dat = dsavekey3 
END


/*---------------------------------------------------------------------------
-									  
-	Procedure	: zerwcp       
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf 
-		die Partienr. durch 
-							
---------------------------------------------------------------------------*/

PROCEDURE zerwcp      

PARAMETER 
	field dzer_part	like zer_part
	field dazer_part	like zer_part
END

	execute from sql 
		update zerw
		set zer_part = $dzer_part 
		where zer_part = $dazer_part 
	end

	return (sqlstatus)
END

procedure what_kennung
if "" = "@(#) mo_dbzerw.rsf	1.400	05.11.91	by AR" end
end
