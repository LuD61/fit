/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_rekost
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	19.06.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    19.06.90   Simmchen	Modularisierung
-	#1143   1.400   19.11.92   AR           Item-Tabellen-Zuordnung
                                                kost_zuordp -> kostzuordp
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    Kostenrelationen
-
-
------------------------------------------------------------------------------
*/

module mo_rekos

/*----- Export Section ------------------------------------------------------*/

EXPORT
	proc prepscroll_kopos
	proc fetchfirst_kopos
	proc fetchnext_kopos
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_cons
end

IMPORT module mo_dbkob
	database bws
end

IMPORT module mo_dbkoz
	database bws
END

field kopos0 smallint
#include "colaenge"

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscroll_kopos 
-
-	In		: dbearb_weg    char(4)      Bearbeitungsweg
-	                  dzuord_kz     char(13)     Zuordungskz
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor fuer
-			  alle Kostenpositionen aus der Kostenzuordnung/
-			  Kostenbereichen.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscroll_kopos

parameter
	field dmdn       like kostzuordp.mdn      
	field dfil       like kostzuordp.fil      
	field dbearb_weg like kostzuordp.bearb_weg
	field dzuord_kz  like kostzuordp.zuord_kz
end

	prepare scroll cursor cus_kopos from sql
		select #kostzuordp.%,#kost_bchp.% from kostzuordp,kost_bchp
		where kostzuordp.mdn       = $dmdn and
		      kostzuordp.fil       = $dfil and
		      kostzuordp.bearb_weg = $dbearb_weg and
		      kostzuordp.zuord_kz  = $dzuord_kz  and
		      kost_bchp.mdn         = kostzuordp.mdn      and
		      kost_bchp.fil         = kostzuordp.fil      and
		      kost_bchp.kost_bch    = kostzuordp.kost_bch and
		      kost_bchp.kost_art    = kostzuordp.kost_art
		order by kostzuordp.lfd end

        prepare scroll cursor cus_kopos0 from sql
		select #kostzuordp.%,#kost_bchp.% from kostzuordp,kost_bchp
                where kostzuordp.mdn       = 0 and
                      kostzuordp.fil       = 0 and
		      kostzuordp.bearb_weg = $dbearb_weg and
		      kostzuordp.zuord_kz  = $dzuord_kz  and
		      kost_bchp.mdn         = kostzuordp.mdn      and
		      kost_bchp.fil         = kostzuordp.fil      and
		      kost_bchp.kost_bch    = kostzuordp.kost_bch and
		      kost_bchp.kost_art    = kostzuordp.kost_art
		order by kostzuordp.lfd end
/* #1143 hinter kost_art end entfernt, lf_nr -| lfd, sql order -> order */

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchfirst_kopos 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest den ersten Satz von cursor
-			  cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchfirst_kopos

	fetch first cursor cus_kopos

        if sqlstatus = 100 //#130700
                fetch first cursor cus_kopos0
                let kopos0 = 1
        else
                init kopos0
        end
                
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchnext_kopos 
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest den naechsten Satz von cursor
-			  cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchnext_kopos
    if kopos0
        fetch next cursor cus_kopos0
    else
	fetch next cursor cus_kopos
    end
    return (sqlstatus)
END

procedure what_kennung
if "" = "@(#) mo_rekost.rsf     FIT   13.07.00        by LuD" end
end
