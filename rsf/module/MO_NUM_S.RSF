
module mo_num_s
/***
--------------------------------------------------------------------------------
-
-       BIZERBA         System - Engeneering       TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       mo_num_str
-
-       Exportierte Prozeduren  :       num_eingabe
-
-                   Parameter   :       par1       Laenge des Maskenfeldes
-                                       par2       Zeile  des Maskenfeldes
-                                       par3       Spalte des Maskenfeldes
-
-       Exportierte Variablen   :       number        
-
-       Interne Prozeduren      :       input_abfr     
-
-       Autor                   :       RDM          
-       Erstellungsdatum        :       07.06.91
-       Modifikationsdatum      :               
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       #179    1.230                   RM         Loeschen eines ueberflues-  
-                                                  sigen Invers-Zeichens in
-                                                  Eingabemaske
-       #522    1.301   11.03.92        RM         verbale Procedurebeschreibung
-       #1129   1.400   06.11.92        RM         Integration Funktionstasten
-                                                  F10 und F11 (optional)      
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       : - Uebergabe einer integer-Zahl zur Masken-
-                                   eingabe;
-                                    -1 : Ausdruck   "*         (alle)"
-                                    >0 : numerischer Wert             
-
-
--------------------------------------------------------------------------------
***/
 

export 
  procedure num_eingabe 
  field number 
end

import module mo_men_m end    /* proc programm_einstieg, programm_ende */
                                /* proc rahmen_oeffnen                   */
import module mo_inkey end

/*----------------------- Datenvereinbarung -----------------------------*/
data
  field zk          char (70)         /* Eingabezeichenkette             */
  field tast_flag   smallint          /* Tasteninp_abfranz		 */
  field number      integer           /* Uebergabezahl - Ergebnis        */
end

constant
  text0   value "          "          /* Loeschtext		         */
end

/*----------------------- Includes --------------------------------------*/
#INCLUDE "messdef"


/*------------------------- PROCEDUREN ----------------------------------*/
/** -------------------------------------------------------------------------
-	Procedure	: inp_abfr    
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : numerische Eingabe selektieren      
------------------------------------------------------------------------- **/
procedure inp_abfr

    if "" = "@(#) mo_num_str.rsf	1.400	06.11.92	by KB" end

  init tast_flag

  switch taste
	case 48              /* "0" */
	case 49              /* "1" */
	case 50              /* "2" */
	case 51              /* "3" */
	case 52              /* "4" */
	case 53              /* "5" */
	case 54              /* "6" */
	case 55              /* "7" */
	case 56              /* "8" */
	case 57              /* "9" */
		let tast_flag = 1
		break
  end

end


/** -------------------------------------------------------------------------
-	Procedure	: num_eingabe
-
-	In 		: par1: Laenge des Maskenfeldes
-         	   	  par2: Zeile des Maskenfeldes
-         	   	  par3: Spalte des Maskenfeldes
-
-	Out		: -
-			  
-	Beschreibung    : realisieren Eingabe, numerisch oder "*"
------------------------------------------------------------------------- **/
procedure num_eingabe

parameter
  field par1   smallint
  field par2   smallint
  field par3   smallint
end

field zei char (1) dimension (70)   /* Zeichen der Eingabezeichenkette  */
field i   smallint                  /* Laufvariable            		*/
field j   smallint                  /* Laufvariable            		*/
  
  init i, j, taste, number
  init zei, zk

  display pos ( par2 par3 ) value (text0)
  display pos ( par2 par3) value ("\R \N")
  refresh

  while taste <> KEYCR and taste <> KEY5 and taste <> KEYUP and taste <> 42 
		and taste <> KEY17 and taste <> KEYHELP
		and taste <> KEY10 and taste <> KEY11    /*------ #1129 ----*/
	perform g_inkey
	switch taste
		case 42			/* taste = "*"			*/
			if i = 0
				let zk = "*"
			end
			break
		case KEYLEFT
  			display pos ( par2, par3 + par1) value (" ")    
			refresh /*---- #179 -------*/
			if i > 0
				let i = i - 1
				let zei[i] = " "
				for j = 0 to par1 - 1
					display pos ( par2, par3 + j)
			 			value (zei[j])
				end
				display pos ( par2, par3 + i) 
					value ("\R"#zei[i]#"\N")
         			refresh
			end	
		case KEYHELP
		case KEYUP
		case KEYCR
		case KEY10             /*------------ #1129 ----------*/
		case KEY11             /*------------ #1129 ----------*/
		case KEY5
		case KEY17
			break
	end
	perform inp_abfr 
	if i = par1                  /* dimensionieren der Eingabe	*/
		init tast_flag
	end
	if tast_flag 
		putchar (zei[i], 1, taste)
		for j = 0 to par1 - 1
			display pos ( par2, par3 + j) value (zei[j])
		end
		let i = i + 1
  		display pos ( par2, par3 + i) value ("\R"#zei[i]#"\N")
  		refresh
	end
  end

  display pos ( par2, par3 + par1) value (" ")    /*---- #179 -------*/
  display pos ( par2, par3) value (" ")           /*---- #1129 ------*/   
  refresh

  if taste = KEY5 or taste = KEYUP or taste = KEY17 or taste = KEYHELP
		or taste = KEY10 or taste = KEY11    /*------ #1129 ----*/
	return
  end

  if i > 0                 /*   zk <> "*"         			*/
  	for j = 0 to i - 1
		putstr ( zk, zei[j], 0, 0)
	end
	let number = zk
  else  
	let number = -1
	display pos ( par2 par3 ) value ("*  (alle)")
        refresh
  end

end 	/* procedure num_eingabe	*/
