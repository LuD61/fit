/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbschn
-
-	Autor			:	Peter Dahmen
-	Erstellungsdatum	:	23.03.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle schnitt , zerm
-
-
------------------------------------------------------------------------------
*/

module mo_dbscn

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_schn
	proc prepread_schn
	proc putmxkey_schn
	proc recupdate_schn
	proc recdelete_schn
	proc recinsert_schn
	proc recread_schn
	proc recread_schn_pos
	proc getmxkey_schn
	proc recreaddef_schn
	proc inittable_schn
	proc savekey_schn
	proc backkey_schn
	proc initkey_schn
	proc cmpkey_schn
	proc clrdelstatus_schn
	proc getdelstatus_schn
	proc fetchcuu_schn
	proc fetchread_schn
	proc erasecursor_schn
	proc closeread_schn
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_dbspe
	field danzahl
	field dzeiger
end

IMPORT module mo_data 
	field dsttrans
end

IMPORT module mo_cons
end

database bws
	table schnitt end
	table zerm end
	table stndkalk end
end

#include "colaenge"

field dsavekey 	 like schnitt.schnitt 
field ddefakey   like schnitt.schnitt value "-1"
field dsavekeyp  like zerm.schnitt 
field ddefakeyp  like zerm.schnitt value "-1"
field dcurskey	 like schnitt.schnitt dimension(CMAXMATRIX)
field dsql	 smallint

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_schn
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_schn.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_schn

	prepare cursor cuu_schn from sql
		select #schnitt.* from schnitt
		where @schnitt.schnitt
		and   @schnitt.mdn
		for update
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_schn
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cur_schn.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_schn

	prepare cursor cur_schn from sql
		select #schnitt.* from schnitt
		where @schnitt.schnitt
			and  @schnitt.mdn
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_schn
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_schn

	let dcurskey[danzahl] = schnitt.schnitt

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_schn
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_schn

	let schnitt.schnitt = dcurskey[dzeiger]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_schn
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_schn gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_schn

	let schnitt.stat = 2		/* status modifiziert*/
	execute from sql 
		update schnitt
		set @schnitt.*
		where current of cuu_schn
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_schn
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_schn gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_schn

	let schnitt.delstatus = -1

	execute from sql
		update schnitt
		set @schnitt.delstatus
		where current of cuu_schn
		end

	if sqlstatus
		return (sqlstatus)
	end
	
	execute from sql 
		update zerm
		set zerm.delstatus = $schnitt.delstatus
		where zerm.schnitt = $schnitt.schnitt
		and zerm.mdn = $schnitt.mdn
	end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_schn
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_schn

	perform recread_schn returning (dsql)
	if dsql = 0  /* Der Datensatz ist schon vorhanden! */
		return(0)
	end

	let schnitt.stat = 1		/* status angelegt*/
	execute from sql 
		insert into schnitt (%schnitt.*)
		values ($schnitt.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_schn
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_schn
	execute from sql
		select #schnitt.* from schnitt where @schnitt.schnitt
		and @schnitt.mdn
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_schn_pos
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_schn_pos
	execute from sql
		select #zerm.* from zerm where 
		zerm.mdn = $schnitt.mdn  and
		zerm.schnitt = $schnitt.schnitt
		and zerm.zerm_aus_mat = $zerm.zerm_aus_mat
		and zerm.zerm_mat = $zerm.zerm_mat
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_schn
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_schn

field dsql smallint

	let schnitt.schnitt = ddefakey

	perform recread_schn returning (dsql)
	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_schn
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_schn

	init table schnitt
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: savekey_schn
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure sichert den alten Schluessel-
-			  wert.   
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_schn

	let dsavekey = schnitt.schnitt
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: backkey_schn
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt den gesicherten  
-			  Schluesselwert zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_schn

	let schnitt.schnitt = dsavekey
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_schn
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_schn

	init schnitt.schnitt
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_schn
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_schn

	if dsavekey = schnitt.schnitt
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_schn
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_schn

	let schnitt.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_schn
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_schn

	return (schnitt.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_schn 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_schn.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_schn

	fetch cursor cur_schn
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_schn 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_schn
-			  ,cur_schn und cus_schn.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_schn

	erase cursor cus_schn
	erase cursor cuu_schn
	erase cursor cur_schn
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_schn 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_schn.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_schn

	fetch cursor cuu_schn
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_schn 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_schn.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_schn

	close cursor cur_schn
	return (sqlstatus) 
END

procedure what_kennung
if "" = "@(#) mo_dbschn.rsf	1.400	05.11.91	by AR" end
end
