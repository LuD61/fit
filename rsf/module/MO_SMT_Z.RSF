module mo_smt_z
/*
--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       mo_smt_zu.rsf
-
        Exports                 :       proc sortiment
					proc neues_smt
					proc loesesmt
					proc loeschesmt
					proc nextsortiment
					proc lastsortiment
                                        proc smt_dfue
                                        proc save_smt
                                        proc schreibe_160_t
					field smt_flag
					field ausw2_offs

-       Interne Prozeduren      :       proc smtattr
                                        proc smtquery
                                        proc smtcursor
                                        proc loeschesmt
                                        proc nextsortiment
                                        proc lastsortiment
                                        proc erstesmtzeile
					proc neusmtcursor
					proc waehlesmt
-
-       Externe Prozeduren      :       proc save_fkt
                                        proc disp_fkt
					proc disp_all_fkt
                                        proc restore_fkt
					proc auswahlbeginn
					proc auswaehlen
-
-       Autor                   :       Wille
-       Erstellungsdatum        :       03.05.90
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-	#544    1.301    26.03.92    RO		Umbau neue DFUE.
-	#588    1.301    03.04.92    RO		Umbau neue DFUE mdn -1 ber.
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Bearbeitung der Sortimentszuordnung von
				    Zentrale zu Mandant und von Mandant zu
				    Filiale.
-
--------------------------------------------------------------------------------
*/

/** Exports                   **/

export
       proc sortiment
       proc neues_smt
       proc loesesmt
       proc nextsortiment
       proc lastsortiment
       proc loeschesmt
       proc smt_dfue    /* #544 */
       proc save_smt    /* #544 */
       proc schreibe_160_t /* #544 */
       field smt_flag
       field ausw2_offs
end

/** Imports                   **/

import module mo_ma_ut
end

import module mo_meld end

import module mo_inkey end

import module mo_men_m end

/* #544 A */
import module mo_w160
end
/* #544 E */

/* #544 A */
import module mo_funk
          proc waitwindow
end

import module mo_peri end
/* #544 E */


/** Datenbankdeklaratione      **/

database bws
    table hwg end
    table smt_zuord end
    table fil_kost  end
    table mdn       end
    table fil       end
end
function
      OpenListWindow
      OpenListWindowBu
      OpenVListWindowBu
      InsertListRow
      EnterQueryListBox
end

#define MODIF
/*
#define FTEST
*/

/** Konstanten     **/

/** Globale Variable            **/

field hwg_ram              smallint dimension (5000)   /* #544 */
field hwg_ok               smallint dimension (5000)   /* #544 */

field smt_flag             smallint
field where_teil           char (5)
field lisanfang            smallint
field sortwindow           smallint     /* Flag, ob Window auswahl_2 geoeffnet
                                           ist    */
field ausw2_offs           smallint     /* Offset fuer die Maske mzauswahl_2  */
field hwg_flag             smallint     /* Flag fuer hwg-Tabelle in
					   mzauswahl_2 */
field smtauswahl           smallint     /* Flag, ob die Auswahl > 0 ist      */

field alt_smt              char (2)     /* #544 */
field smt_anz              smallint     /* #544 */
field waitw                smallint     /* #544 */
field k                    smallint     /* #544 */


/** Masken          **/
/** Anzeigemaske fuer mehrzeilige Auswahl.
    Proceduren in Include-File 'auswahlmz.rsi     **/

/** Maske fuer Neuwahl Sortiment                  **/

form mzauswahl_1
field invzeile     value " "              pos (0 1)  length 72
field ahwg        use smt_zuord.hwg       pos (0 1)  length 4  blankfill
                                                     rightjust
field a2hwg       use hwg.hwg             pos (0 1)  length 4  blankfill
                                                     rightjust
field abz1        use hwg.hwg_bz1         pos (0 6)  length 24
field abz2        use hwg.hwg_bz2         pos (0 31) length 24
field azuord_dat  use smt_zuord.zuord_dat pos (0 58) picture "dd.mm.yy"
end


/** Maske fuer Liste Sortiment        **/

form mzauswahl_2
field invzeile     value " "               pos (0 1)  length 72
field lhwg        use smt_zuord.hwg        pos (0 1)  length 4  blankfill
                                                      rightjust
field l2hwg       use hwg.hwg              pos (0 1)  length 4  blankfill
                                                      rightjust
field lbz1        use hwg.hwg_bz1          pos (0 13) length 24 displayonly
field lbz2        use hwg.hwg_bz2          pos (0 38) length 24 displayonly
field lzuord_dat  use smt_zuord.zuord_dat  pos (0 65) picture "dd.mm.yy"
                                                      displayonly
end


/** Maske fuer Liste Kostenart        **/

form mzauswahl_3
field dummy      value "" pos (0 0)
end

/** Maske fuer Liste Filialleitung          **/

form mzauswahl_4
field dummy      value "" pos (0 0)
end

form mzauswahl_5
field dummy5      value "" pos (0 0)
end

/** includes            **/

#include "messdef"
#include "auswahlm.rsi"
/** Sortiment fuer Mandant anzeigen     **/

procedure sortiment
     parameter
     field smdn          smallint
     field sfil          smallint
     field svkst         smallint
     field ssmt_kz       char (1)
     field beart         smallint
     end

if "" = "@(#) mo_smt_zu.rsf	1.400	18.08.92	by RO" end
     if smt_flag and not beart
		  return
     end

     perform setoffs (ausw2_offs)

     perform smtattr (sfil svkst ssmt_kz)

     perform save_fkt
     perform disp_fkt (-1 0)
     perform loeschesmt(2)
     if not TRUE
             perform setszuo (smdn sfil svkst ssmt_kz)
	     if not proc smtquery (smdn sfil svkst ssmt_kz)
		  perform restore_fkt
		  return
             end

     else
	     init query from form mzauswahl_2
	     init where_teil
	     perform smtcursor (smdn sfil svkst ssmt_kz)
	     let smt_flag = TRUE
     end

     let maxzeilen[2] = 13
     let auswahl[2] = 1
     perform leseliste (maxzeilen[2] 2)
     perform restore_fkt
end

/** Offset fuer aswahl setzen                          **/

procedure setoffs
     parameter
     field aoffs          smallint
     end

     set form field mzauswahl_2.invzeile
			       pos (aoffs 1)
     set form field lhwg       pos (aoffs 1)
     set form field l2hwg      pos (aoffs 1)
     set form field lbz1       pos (aoffs 13)
     set form field lbz2       pos (aoffs 38)
     set form field lzuord_dat pos (aoffs 65)
end

/** Je nachdem ob Mandant- oder Filialbearbeitung Felder setzen    **/

procedure setszuo
     parameter
     field smdn        smallint
     field sfil        smallint
     field svkst       smallint
     field ssmt_kz     char (1)
     end

     if sfil = 0 and ssmt_kz = "G"
		       display comment "Auswahl ueber Sortiment der Zentrale eingeben"
		       let smt_zuord.mdn = 0
		       let smt_zuord.fil = 0
     elseif (sfil = 0 and ssmt_kz = "T") or
            (sfil <> 0 and ssmt_kz = "G")
		       display comment "Auswahl ueber Mandantensortiment eingeben"
		       let smt_zuord.mdn = smdn
		       let smt_zuord.fil = 0
     elseif sfil <> 0 and ssmt_kz = "T"
		       display comment "Auswahl ueber Filialsortiment eingeben"
		       let smt_zuord.mdn = smdn
		       let smt_zuord.fil = sfil
     end
end

/** Attribute fuer die Maskenfelder von Sortiment setzen     **/

procedure smtattr
     parameter
     field sfil     smallint
     field svkst    smallint
     field ssmt_kz  char (1)
     end

     field fskz      char (1)
     field mskz      char (1)


     if ssmt_kz = "G" and sfil = 0 and svkst = 0
                     set form field l2hwg removed (OFF)
                     set form field lhwg  removed (ON)
     else
                     set form field l2hwg removed (ON)
                     set form field lhwg  removed (OFF)
     end

     if sfil <> 0 or svkst <> 0
		     set form field a2hwg removed (ON)
     else
		     set form field ahwg removed (ON)
		     set form field azuord_dat removed (ON)
     end
end


/** Query fuer Sortiment erfassen.
    Bei smt_kz = G ueber Sortiment des Mandanten sonst
    ueber Sortiment der Filiale                         **/


procedure smtquery
    parameter
    field  smdn      smallint
    field  sfil      smallint
    field  svkst     smallint
    field  ssmt_kz   char (1)
    end

    init query from form mzauswahl_2
    enter  pos (1 0) query from lhwg l2hwg lzuord_dat
    actions
	 case key5
		 return(0)
         case key12
		 break
    end
    if status query from form mzauswahl_2
		 move "and" to where_teil
    else
		 init where_teil
    end
    perform smtcursor (smdn sfil svkst ssmt_kz)
    return(1)
end

/** Cursor fuer Sortiment-Query vorbereiten      **/

procedure smtcursor
    parameter
    field  smdn      smallint
    field  sfil      smallint
    field  svkst     smallint
    field  ssmt_kz   char (1)
    end

    if ssmt_kz = "G"
                     perform testsmt_kz (smdn sfil svkst)
			     returning  (smdn sfil svkst)
    end

    if ssmt_kz = "G" and smdn = 0
         prepare scroll cursor mzauswahl_2 from sql
              select #hwg.hwg, #hwg.hwg_bz1, #hwg.hwg_bz2
              from hwg
	      where hwg.hwg > 0
         end
         field where_teil
         query from form mzauswahl_2
         sql order by hwg.hwg end
         set form field l2hwg removed (OFF)
         set form field lhwg  removed (ON)
    else
         prepare scroll cursor mzauswahl_2 from sql
                       select #smt_zuord.hwg, #smt_zuord.zuord_dat,
			      #hwg.hwg_bz1, #hwg.hwg_bz2
                       from smt_zuord,hwg
                       where smt_zuord.mdn = $smdn and smt_zuord.fil = $sfil
		       and smt_zuord.verk_st = $svkst
     	               and smt_zuord.hwg = hwg.hwg
         end
         field where_teil
         query from form mzauswahl_2
         sql order by smt_zuord.hwg end
         set form field l2hwg removed (ON)
         set form field lhwg  removed (OFF)
     end
end

/** Testen des uebergeordneten smt_kz             **/

procedure testsmt_kz
    parameter
    field  smdn      smallint
    field  sfil      smallint
    field  svkst     smallint
    end



    if sfil <> 0 and svkst <> 0
	       let svkst = 0
	       execute from sql
		       select #fil.smt_kz from fil
		       where fil.fil = $sfil
                       and   fil.mdn = $smdn
               end
	       if fil.smt_kz = "T"
		       return (smdn sfil svkst)
               else
		       let sfil = 0
               end
    end

    if (svkst <> 0 and sfil = 0) or
       (svkst = 0  and sfil <> 0)
	       let svkst = 0
	       let sfil = 0
	       execute from sql
		       select #mdn.smt_kz from mdn
		       where mdn.mdn = $smdn
               end
	       if mdn.smt_kz = "T"
		       return (smdn sfil svkst)
               end
    end
    let smdn = 0
    return (smdn sfil svkst)
end

/** Sortiment-Anzeige vom Bildschirm loeschen         **/

procedure loeschesmt
     parameter
     field  czeiger   smallint
     end

     field zaehler    smallint

     init form mzauswahl_2 form mzauswahl_3 smt_flag
     let zaehler = 1
     while zaehler < wlaenge[czeiger]
		  switch czeiger
			 case 2
		                display pos (zaehler 0) form mzauswahl_2
				break
                         case 3
		                display pos (zaehler 0) form mzauswahl_3
				break
                  end
		  let zaehler = zaehler + 1
     end
     refresh
end


/** Sortiment vorwaerts blaettern       **/

procedure nextsortiment
     parameter
     field   czeiger     smallint
     field   scrolltyp   smallint
     end

     field lines         smallint

     if auswlast[2]
		 return
     end

     if scrolltyp = 1
		 let lines = 1
     else
		 let lines = wlaenge[czeiger] - 1
     end

     let auswahl[czeiger] = lisanfang + lines
     perform leseliste (maxzeilen[czeiger] czeiger)
     return
end

/** Sortiment rueckwaerts blaettern       **/

procedure lastsortiment
     parameter
     field    czeiger      smallint
     field    scrolltyp    smallint
     end

     field lines         smallint

     if lisanfang = 1
		 return
     end

     if scrolltyp = 1
		 let lines = 1
     else
		 let lines = maxzeilen[czeiger] + 1
     end

     let auswahl[czeiger] = lisanfang - lines
     if auswahl[czeiger] < 1
		 let auswahl[czeiger] = 1
     end
     perform leseliste (maxzeilen[czeiger] czeiger)
     return
end

/** Sortiment neu zuordnen                **/

procedure neues_smt
     parameter
     field smdn       smallint
     field sfil       smallint
     field svkst      smallint
     end

     field   taste          smallint
     field   remove         smallint

     init taste
     let remove = proc testoben(smdn sfil svkst)
     perform save_fkt
     perform disp_all_fkt (-1 -1 -1 -1 1 -1)
     select window mamain1
     perform setoffs (ausw2_offs + 1)
     while taste <> key5
		  set form field lhwg  removed (OFF)
		  set form field l2hwg removed (ON)

		  close cursor mzauswahl_2
		  let auswahl[2] = 1
		  perform leseliste(maxzeilen[2] - 1 2)
		  set form field lhwg  removed (not remove)
		  set form field l2hwg removed (remove)

		  init  form mzauswahl_2
		  display pos (0 0) form mzauswahl_2
		  enter pos (0 0) form mzauswahl_2
		  actions
                        case key17
                                if in_work
                                           perform rollback_work
                                end
                                perform programm_ende
			case key5
				let taste = syskey
				break
                        case key10
                                perform setoffs (ausw2_offs)
                                init form mzauswahl_2
                                perform sort_query (smdn sfil svkst)
                                perform setoffs (ausw2_offs + 1)
				if syskey = key5 or smtauswahl = 0
				       init form mzauswahl_2
		                       display pos (0 0) form mzauswahl_2
				       nextfield lhwg
                                end
                                if smt_zuord.hwg = 0
                                       let smt_zuord.hwg = hwg.hwg
                                end
				perform disp_fkt (10 1)
                        after lhwg
			after l2hwg
                                if not remove
                                        let smt_zuord.hwg = hwg.hwg
                                end

				if not proc fetchsmt (smdn sfil svkst)
				       nextfield lhwg
                                end
                                let smt_zuord.zuord_dat = sysdate
				break
                   end
		   if taste = key5
				break
                   end
		   if not remove
				let smt_zuord.hwg = hwg.hwg
                   end
		   perform smtzuord (smdn sfil svkst)
     end
     set form field lhwg  removed (OFF)
     set form field l2hwg removed (ON)
     perform loeschesmt (2)
     perform setoffs (ausw2_offs)
     close cursor mzauswahl_2
     let auswahl[2] = 1
     perform leseliste(maxzeilen[2] 2)
     perform restore_fkt
end

/** Uebergeordnetes Sortiment holen             **/

procedure fetchsmt
     parameter
     field smdn       smallint
     field sfil       smallint
     field svkst      smallint
     end

     if hwg.hwg = 0 and smt_zuord.hwg = 0
              return (0)
     end
     if proc smt_vorhanden (smdn sfil svkst)
              perform disp_msg (483 0)     /* Nummer schon zugeordnet  */
              return (0)
     end
     if sfil <> 0 or svkst <> 0
             perform testneusmt (smdn sfil svkst)
	              returning  (smdn sfil svkst)
     end

     if sfil = 0 and svkst = 0
	      execute from sql
		       select #hwg.hwg, #hwg.hwg_bz1, #hwg.hwg_bz2 from hwg
		       where @hwg.hwg
               end
     elseif svkst <> 0 and sfil <> 0
	       execute from sql
		       select #smt_zuord.hwg ,#hwg.hwg_bz1, #hwg.hwg_bz2
		       from smt_zuord, hwg
		       where @smt_zuord.hwg and hwg.hwg = smt_zuord.hwg
                       and smt_zuord.mdn     = $smdn
                       and smt_zuord.fil     = $sfil
                       and smt_zuord.verk_st = 0
               end
     else
	       execute from sql
		       select #smt_zuord.hwg ,#hwg.hwg_bz1, #hwg.hwg_bz2
		       from smt_zuord, hwg
		       where @smt_zuord.hwg and hwg.hwg = smt_zuord.hwg
                       and smt_zuord.mdn     = $smdn
                       and smt_zuord.fil     = 0
                       and smt_zuord.verk_st = 0
               end
     end
     if sqlstatus
	       display message "Falsche Nummer"
	       return (0)
     end
     return (1)
end

/** Test, ob die Nummer schon exisitiert
**/

procedure smt_vorhanden
parameter
field  smdn            smallint
field  sfil            smallint
field  svkst           smallint
end

      execute from sql
              select #smt_zuord.hwg from smt_zuord
              where smt_zuord.mdn     = $smdn
              and   smt_zuord.fil     = $sfil
              and   smt_zuord.verk_st = $svkst
              and   @smt_zuord.hwg
      end
      if sqlstatus = 0
              return (1)
      end
      return (0)
end
/** Query auf uebergeordnetes Sortiment         **/

procedure sort_query
     parameter
     field smdn       smallint
     field sfil       smallint
     field svkst      smallint
     end

     perform disp_fkt (-1 0)
     perform setszuo (smdn sfil svkst "G")
     init query from form mzauswahl_2
     set form field lhwg  removed (not sfil)
     set form field l2hwg removed (sfil)
     enter pos (1 0) query from lhwg l2hwg
     actions
		 case key5
			 return
                 case key12
			 break
     end
     if status query from form mzauswahl_2
		 move "and" to where_teil
     else
		 init where_teil
     end
     perform neusmtcursor (smdn sfil svkst)
     perform waehlesmt(smdn sfil svkst)
     let smt_zuord.zuord_dat = sysdate
     display pos (1 0) lzuord_dat
     erase cursor mzauswahl_1
     return
end

/** Erste Zeile der Liste am Bildschirm wieder schreiben.
    Notwendig nach einer Queryeingabe in der Ersten Zeile   **/

procedure erstesmtzeile
      set form field lhwg  removed (OFF)
      set form field l2hwg removed (ON)
      fetch absolute lisanfang cursor mzauswahl_2
      display pos (1 0) form mzauswahl_2
      refresh
end

/** Cursor fuer Neuzuordnung aufbauen      **/

procedure neusmtcursor
     parameter
     field smdn       smallint
     field sfil       smallint
     field svkst      smallint
     end

     field rmdn       smallint
     field rfil       smallint
     field rvkst      smallint

     if sfil <> 0 or svkst <> 0
                 perform testneusmt(smdn sfil svkst)
			 returning (rmdn rfil rvkst)
     end
     if rfil = 0 and rvkst = 0
		 set form field ahwg removed (ON)
		 set form field a2hwg removed (OFF)
     else
		 set form field ahwg removed (OFF)
		 set form field a2hwg removed (ON)
     end

     if rfil <> 0 and rvkst <> 0
                 prepare scroll cursor mzauswahl_1 from sql
                                 select #smt_zuord.hwg, #smt_zuord.zuord_dat,
					 #hwg.hwg_bz1, #hwg.hwg_bz2
                                 from smt_zuord,hwg
                                 where smt_zuord.mdn = $smdn
                                 and smt_zuord.fil = $sfil
				 and smt_zuord.verk_st = 0
     	                         and smt_zuord.hwg = hwg.hwg
                                 and not
                                 ( hwg.hwg in
                                 ( select smt_zuord.hwg from smt_zuord
                                   where smt_zuord.mdn     = $smdn
                                   and   smt_zuord.fil     = $sfil
                                   and   smt_zuord.verk_st = $svkst
                                 )
                                 )
                  end
                  field where_teil
                  query from form mzauswahl_2
                  sql order by smt_zuord.hwg end
     elseif (rfil <> 0 and rvkst  = 0)
                 prepare scroll cursor mzauswahl_1 from sql
                                 select #smt_zuord.hwg, #smt_zuord.zuord_dat,
					 #hwg.hwg_bz1, #hwg.hwg_bz2
                                 from smt_zuord,hwg
                                 where smt_zuord.mdn = $smdn
                                 and smt_zuord.fil = 0
			       	 and smt_zuord.verk_st = 0
     	                         and smt_zuord.hwg = hwg.hwg
                                 and not
                                 ( hwg.hwg in
                                 ( select smt_zuord.hwg from smt_zuord
                                   where smt_zuord.mdn     = $smdn
                                   and   smt_zuord.fil     = $sfil
                                   and   smt_zuord.verk_st = $svkst
                                 )
                                 )

                  end
                  field where_teil
                  query from form mzauswahl_2
                  sql order by smt_zuord.hwg end
     elseif (rfil = 0  and rvkst <> 0)
                 prepare scroll cursor mzauswahl_1 from sql
                                 select #smt_zuord.hwg, #smt_zuord.zuord_dat,
					 #hwg.hwg_bz1, #hwg.hwg_bz2
                                 from smt_zuord,hwg
                                 where smt_zuord.mdn = $smdn
                                 and smt_zuord.fil = 0
			       	 and smt_zuord.verk_st = 0
     	                         and smt_zuord.hwg = hwg.hwg
                                 and not
                                 ( hwg.hwg in
                                 ( select smt_zuord.hwg from smt_zuord
                                   where smt_zuord.mdn     = $smdn
                                   and   smt_zuord.fil     = $sfil
                                   and   smt_zuord.verk_st = $svkst
                                 )
                                 )

                  end
                  field where_teil
                  query from form mzauswahl_2
                  sql order by smt_zuord.hwg end
     elseif rfil = 0 and rvkst = 0
                 prepare scroll cursor mzauswahl_1  from sql
                                 select #hwg.hwg, #hwg.hwg_bz1, #hwg.hwg_bz2
                                 from hwg
			         where hwg.hwg > 0
                                 and not
                                 ( hwg.hwg in
                                 ( select smt_zuord.hwg from smt_zuord
                                   where smt_zuord.mdn     = $smdn
                                   and   smt_zuord.fil     = $sfil
                                   and   smt_zuord.verk_st = $svkst
                                 )
                                 )
                  end
                  field where_teil
                  query from form mzauswahl_2
                  sql order by hwg.hwg end
     end
end

/** Test bei Cursoraufbau fuer uebergeordnete Zuordnung,
    welche Zuordnung benutzt werden muss          **/


procedure testneusmt
    parameter
    field   smdn      smallint
    field   sfil      smallint
    field   svkst     smallint
    end

    if svkst <> 0 and sfil <> 0
	     execute from sql
		  select #fil.smt_kz from fil
			 where fil.fil = $sfil
                         and   fil.mdn = $smdn
             end
             if fil.smt_kz = "T"
			 return(smdn sfil svkst)
             end
	     let sfil = 0
    end

    execute from sql
	  select #mdn.smt_kz from mdn
		 where mdn.mdn = $smdn
    end
    if mdn.smt_kz = "T"
		 return(smdn sfil svkst)
    end
    let sfil  = 0
    let svkst = 0
    return(smdn sfil svkst)
end

/** Test, ob fuer Zuordnungen aus hwg oder smt_zi=uord gelesen
    werden muss     **/

procedure testoben
    parameter
    field   smdn       smallint
    field   sfil       smallint
    field   svkst      smallint
    end


    if sfil = 0 and svkst = 0
		    return (0)
    end

    init fil.smt_kz mdn.smt_kz
    if svkst <> 0 and sfil <> 0
		  execute from sql
			  select #fil.smt_kz from fil
			  where fil.fil = $sfil
                          and   fil.mdn = $smdn
                  end
                  if fil.smt_kz = "T"
			  return(1)
                  end
    end

    execute from sql
	    select #mdn.smt_kz from mdn
	    where mdn.mdn = $smdn
    end
    if mdn.smt_kz = "T"
	    return(1)
    end
    return(0)
end

/** Aus uebergeordnetem Sortiment auswaehlen    **/

procedure waehlesmt
     parameter
     field smdn       smallint
     field sfil       smallint
     field svkst      smallint
     end

     let maxzeilen[1] = 16
     open window auswahl_1 dimension(16 74)
     if proc auswahlbegin(67 1 1) = 0
           let smtauswahl = FALSE
           close window auswahl_1
	   erase cursor mzauswahl_1
	   perform erstesmtzeile
	   return
     end
    display pos (0 1) title "\R HWG  Bezeichnung1             Bezeichnung2              Zuord.Dat \N"
    refresh
     let auswahl[2] = 1
     if proc auswaehlen(1 1) = 0
           let smtauswahl = FALSE
           close window auswahl_1
	   erase cursor mzauswahl_1
	   return
     end
     let smtauswahl = TRUE
     close window auswahl_1
     erase cursor mzauswahl_1
     close cursor mzauswahl_2
     return
end

/** Ein in auswahl_1 auswgewaehler Satz wird dem Filialsortiment
    zugeordnet     **/

procedure smtzuord
     parameter
     field smdn       smallint
     field sfil       smallint
     field svkst      smallint
     end

     set form field lhwg  removed (OFF)
     set form field l2hwg removed (ON)
     let smt_zuord.mdn = smdn
     let smt_zuord.fil = sfil
     let smt_zuord.verk_st = svkst
     if sfil = 0
	        let smt_zuord.hwg = hwg.hwg
     end
     insert into smt_zuord
     if sqlstatus
		display message sql (sqlstatus)
     end
     close cursor mzauswahl_2
     let auswahl[2] = 1
     perform leseliste (maxzeilen[2] - 1 2)
end

/** Sortiment  loesen                    **/

procedure loesesmt
     parameter
     field smdn     smallint
     field sfil     smallint
     field svkst    smallint
     end

     let smt_zuord.mdn = smdn
     let smt_zuord.fil = sfil
     let smt_zuord.verk_st = svkst
     perform save_fkt
     perform disp_fkt (-1 0)
     let auswahl[2] = lisanfang
     if not proc auswaehlenalt(1 2)
		       perform restore_fkt
		       return
     end
     execute from sql
	     delete from smt_zuord
	     where @smt_zuord.mdn and @smt_zuord.fil and @smt_zuord.verk_st
		   and @smt_zuord.hwg
     end
     if sqlstatus
	     display message sql (sqlstatus)
	     perform restore_fkt
             return
     end
     perform loeschesmt(2)
     close cursor mzauswahl_2
     let auswahl[2] = 1
     perform leseliste (maxzeilen[2] 2)
     perform restore_fkt
     return
end



/** Lesen des Scroll_Cursors liste und zeilenweises Anzeigen der Maske
    liste im aktuellen Window
**/

procedure leseliste
parameter
field   zeilen    smallint
field   czeiger   smallint
end

   field zzaehler    smallint

   let lisanfang  = auswahl[czeiger]
   let wlaenge[czeiger] = proc fillwindow(1 czeiger zeilen)
   refresh
   return
end


/** Proceduren fuer die Zeilenanzeige mit Attribut fuer das
    Auswahl_Modul auswahlmz.rsi         **/

procedure ausattr_1
   parameter
   field attr    smallint
   end

   set form mzauswahl_1 reverse (attr)
end

procedure ausattr_2
   parameter
   field attr    smallint
   end

   set form mzauswahl_2 reverse (attr)
end

procedure ausattr_3
   parameter
   field attr    smallint
   end

   set form mzauswahl_3 reverse (attr)
end

procedure ausattr_4
   parameter
   field attr    smallint
   end

   set form mzauswahl_4 reverse (attr)
end

procedure ausattr_5
   parameter
   field attr    smallint
   end

   set form mzauswahl_5 reverse (attr)
end

/* #544 A */
procedure smt_dfue
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       smt_dfue
-
-       In              :
-
-       Out             :       sqlstatus
-
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Bei direkter DFUE Artikel Updaten oder Loeschen
--------------------------------------------------------------------------------
**/
parameter
field  smdn      smallint
field  sfil      smallint
field  neu_smt   like mdn.smt_kz
end

#ifndef MODIF
        return
#end


/* #588 A */
       if smdn = -1 or sfil = -1
                          return
       end
/* #588 E */

       if alt_smt = "G" and neu_smt = "G"
                          return
       end
       if alt_smt = ""
                          return
       end
       if proc test_peri (smdn sfil) = 0
                          return
       end
       if alt_smt = "T" and
              neu_smt = "T"
                     perform smtttot (smdn sfil)
       elseif alt_smt = "G"
                     perform smtgtot (smdn sfil)
       else
                     perform smtttog (smdn sfil)
       end
end


procedure save_smt
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       save_smt
-
-       In              :
-
-       Out             :       sqlstatus
-
-
-       Errorcodes      :       -
-
-       Beschreibung    :       HWG's aus Sortiment beim Lesen sichern.
--------------------------------------------------------------------------------
**/
parameter
field  smt_mdn      smallint
field  smt_fil      smallint
field  smt_smt      like mdn.smt_kz
end

#ifndef MODIF
        return
#end

        let alt_smt = smt_smt
        init smt_anz waitw
        if smt_smt <> ""
                      perform hwg_merken (smt_mdn smt_fil smt_smt)
        end
end

procedure hwg_merken
/**
HWG's des Sortiment einer Filiale merken.
**/
parameter
field  smt_mdn      smallint
field  smt_fil      smallint
field  smt_smt      like mdn.smt_kz
end

        field i   smallint

        if smt_smt = "G" and smt_fil > 0
                 execute into smt_smt from sql
                         select mdn.smt_kz from mdn
                         where mdn.mdn = $smt_mdn
                 end
                 init smt_fil
        end

        if smt_smt = "G"
                  prepare cursor hole_smt
                          into smt_zuord.hwg from sql
                          select hwg.hwg from hwg
                  end
        else
                 prepare cursor hole_smt from sql
                         select #smt_zuord.hwg from smt_zuord
                         where smt_zuord.mdn     = $smt_mdn
                         and   smt_zuord.fil     = $smt_fil
                         and   smt_zuord.verk_st = 0
                 end
        end

        init i
        fetch cursor hole_smt
        while not sqlstatus
                let hwg_ram[i] = smt_zuord.hwg
                let i = i + 1
                fetch cursor hole_smt
        end
        let smt_anz = i
        erase cursor hole_smt
end

procedure smtgtot
/**
Artikel bei Wechsel von Sortimentskennzeichen G zu T fuer DFUE
aufbereiten
**/
parameter
field  smdn       smallint
field  sfil       smallint
end

       field  rwert    smallint
       field  i        smallint

       prepare cursor hole_smt from sql
                     select #smt_zuord.hwg from smt_zuord
                     where smt_zuord.mdn     = $smdn
                     and   smt_zuord.fil     = $sfil
                     and   smt_zuord.verk_st = 0
       end

       fetch cursor hole_smt
       while not sqlstatus
                     let rwert = proc in_save_hwg (smt_zuord.hwg)
                     fetch cursor hole_smt
       end

/* Geloeschte Zuordnungen aufbereiten    */

      let i  = 0
      while i < smt_anz
                    if not hwg_ok[i]
                                perform hole_artikel (smdn sfil hwg_ram[i] "L")
                    end
                    let i = i + 1
      end
      erase cursor hole_smt
end


procedure smtttog
/**
Artikel bei Wechsel von Sortimentskennzeichen T zu G fuer DFUE
aufbereiten
**/
parameter
field  smdn       smallint
field  sfil       smallint
end

       field  mdnsmt     like mdn.smt_kz
       field  i          smallint


       init mdnsmt
       if sfil > 0
                    execute
                           into mdnsmt from sql
                           select mdn.smt_kz from mdn
                           where mdn.mdn = $smdn
                    end
       end

       if sfil = 0 or mdnsmt = "G"
                    prepare cursor hole_smt
                               into smt_zuord.hwg from sql
                               select hwg.hwg from hwg
                    end
       else
                    prepare cursor hole_smt from sql
                               select #smt_zuord.hwg from smt_zuord
                               where smt_zuord.mdn     = $smdn
                               and   smt_zuord.fil     = 0
                               and   smt_zuord.verk_st = 0
                    end
       end

       fetch cursor hole_smt
       while not sqlstatus
              if not proc in_save_hwg (smt_zuord.hwg)
                           perform hole_artikel (smdn sfil
                                                 smt_zuord.hwg "B")
              end
              fetch cursor hole_smt
        end
        erase cursor hole_smt
end


procedure smtttot
/**
Artikel bei Aenderungen im Sortiment fuer Kenzeichen T aufbereiten.
aufbereiten
**/
parameter
field  smdn       smallint
field  sfil       smallint
end

       field i    smallint

       init hwg_ok
/* Neue Zuordnungen                    */
       prepare cursor hole_smt from sql
               select #smt_zuord.hwg from smt_zuord
               where smt_zuord.mdn     = $smdn
               and   smt_zuord.fil     = $sfil
               and   smt_zuord.verk_st = 0
       end

       fetch cursor hole_smt
       while not sqlstatus
               if not proc in_save_hwg (smt_zuord.hwg)
                            perform hole_artikel (smdn sfil
                                                  smt_zuord.hwg "B")
               end
               fetch cursor hole_smt
        end
        erase cursor hole_smt

/* Geloeschte Zuordnungen aufbereiten    */

        let i  = 0
        while i < smt_anz
               if not hwg_ok[i]
                           perform hole_artikel (smdn sfil hwg_ram[i] "L")
               end
               let i = i + 1
        end
end


procedure hole_artikel
/**
Artikel zu HWG holen und fuer Uebertragung aufbereiten.
**/
parameter
field  smdn     smallint
field  sfil     smallint
field  shwg     smallint
field  smodif   char (2)
end

      field art     decimal (13 0)

      if not waitw
               perform waitwindow
                   ("Die Daten werden zum Uebertragen vorbereitet" "blink")
               let waitw = 1
      end
      prepare cursor hole_a
                  into art from sql
                  select a_bas.a from a_bas
                  where a_bas.hwg = $shwg
      end

      fetch cursor hole_a
      while not sqlstatus
               let k = proc  schreibe_160_t
               (art
                -1
                smdn
                0
                sfil
                smodif
                sysdate
                0
                1)
                fetch cursor hole_a
      end
      erase cursor hole_a
end

procedure schreibe_160_t
/**
Procedure gibt die Parameter von schreibe_160 auf der Standartausgabe aus.
**/
parameter
field   art       decimal (13 0)
field   mgruppe   smallint
field   mandant   smallint
field   fgruppe   smallint
field   filiale   smallint
field   dmodif    char (2)
field   ddatum    date
field   smt_flag  smallint
field   wait_flag smallint
end
      field cr      char (2)
      field sopen   smallint value 0

#ifdef FTEST
      if not sopen
                create channel ausgabe separator "/" mode write file
                                     ("fifo_dat")
                if sysstatus
                              open channel
                                   ausgabe separator "/"
                                   mode write file ("fifo_dat")
                end
                putchar (cr,1,10)
                let sopen = 1
      else
                select channel ausgabe
      end

      write (art
             mgruppe
             mandant
             fgruppe
             filiale
             dmodif
             ddatum
             smt_flag
             wait_flag
             )
#else
      let k = proc schreibe_160_fifo (art
                                      mgruppe
                                      mandant
                                      fgruppe
                                      filiale
                                      dmodif
                                     ddatum
                                      smt_flag
                                      wait_flag
                                      )
      if k
                   perform disp_msg (sysstatus 1)
      end
#end
      return (0)
end

procedure in_save_hwg
/**
Test, ob eine hwg im RAM gespeichert wurde.
**/
parameter
field   shwg     smallint
end

          field  i  smallint

          let i = 0
          while i < smt_anz
                     if shwg = hwg_ram[i]
                                 let hwg_ok[i] = 1
                                 return (1)
                     end
                     let i = i + 1
          end
          return (0)
end

PROC awm_werte_1
    init awm_buffer
    putstr (awm_buffer hwg.hwg 0 0)
    putstr (awm_buffer hwg.hwg_bz1 7 0)
    putstr (awm_buffer hwg.hwg_bz2 31 0)
    putstr (awm_buffer smt_zuord.zuord_dat 58 0)
END
PROC awm_werte_2
END
PROC awm_werte_3
END
PROC awm_werte_4
END
PROC awm_werte_5
END
/* #544 E */
