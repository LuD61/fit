module mo_pict
/***
--------------------------------------------------------------------------------
-
-       BSA  Bizerba Software- und Automationssysteme GmbH
-       Vorstadtstr. 59, 7465 Geislingen, Tel.: 07433/2413
-
--------------------------------------------------------------------------------
-
-       Modulname               :       mo_pict
-
-       Interne Prozeduren      :       
-
-       Exportierte Prozeduren  :       procedure waehle_picture
-
-       Autor                   :       Kurt Beck
-       Erstellungsdatum        :       31.05.90
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       1       1.00    18.06.90        Kurt       ROSI 3.0.1d
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :       Picture editieren
-
--------------------------------------------------------------------------------
***/

export 
   procedure waehle_picture
end

import module mo_men_m end
import module mo_ma_ut end

import module mo_ptab end
import module mo_get_p end

#   include "messdef"

/* ff */
field  abbruch        smallint   
field  i              smallint
field  j              smallint
field  k              smallint
field  l              smallint
field  pic_ges_stell       smallint
field  pic_vor_stell       smallint
field  pic_nach_stell      smallint
field  pic_vor_fest        smallint
field  pic_vor_null_zeich  char(1)
field  pic_nach_fest       smallint
field  pic_nach_null_zeich char(1)
field  pic_ergebnis        char(22)
field  pic_typ             smallint
field  null_char           char(1)
field  aktion_sel          smallint /* Selekt. Punkt der Prueftabelle   */
field  fehler              smallint
field  pstat1              smallint
field  pstat2              smallint
field  punkt_pos           smallint
field  anz_zeilen          smallint


screen picture_screen
 
 Zahlenformat waehlen 
 
 Gesamtstellen    :  <p1>
 Nachkommastellen :  <p2>

 Feste Vorkommast.:  <p3>
 Zeichen fuer
 Nullunterdrueck. :  <p4>

 Feste Nachkommast:  <p5>
 Zeichen fuer 
 Nullunterdrueck. :  <p6>

 Ergebnis:  <p7                  >
end

form picture_maske
   field  p1  smallint length 2 range (1 to 20)
   field  p2  smallint length 2 range (0 to 19)
   field  p3  smallint length 2 range (0 to 20)
   field  p4  char(1) length 1 /* range(" ","*","-") */
   field  p5  smallint length 2 range (0 to 19)
   field  p6  char(1) length 1 /* range(" ","*","-") */
   field  p7  char(20) length 20 displayonly
end

/* ff */
procedure waehle_null_char

if "" = "@(#) mo_pict	1.400	18.07.91	by KB>" end

   perform disp_fkt (9,-1) /* Ansehen */
   select window picture_window
   perform mxmptab ("null_char") returning (pstat1, pstat2)
   if not pstat1 /* fehler */
      display errortext ("Fehler bei mxmptab")
      return
   else
      let null_char = ptabn.ptwert
   end
   perform disp_fkt (9,1) /* Ansehen */

   select window picture_window

end

/* ff */
procedure read_picture

   let punkt_pos = instring(pic_ergebnis "." 1)

   if not punkt_pos
      let pic_ges_stell = strlen (pic_ergebnis)
      let punkt_pos = pic_ges_stell + 1
   else
      let pic_ges_stell = strlen (pic_ergebnis) - 1
   end

   let pic_vor_stell = punkt_pos - 1
   let pic_nach_stell = pic_ges_stell - pic_vor_stell

   let i = instring(pic_ergebnis "&" 1)
   if i > 0 and i < punkt_pos
      let pic_vor_fest = punkt_pos - i
   else
      init pic_vor_fest
   end

   let i = punkt_pos + 1
   let j = instring(pic_ergebnis "&" i)
   while instring(pic_ergebnis "&" i)
      let i = i + 1
   end

   let pic_nach_fest = i - 1 - punkt_pos
   
   init pic_vor_null_zeich, pic_nach_null_zeich

   if (pic_vor_stell - pic_vor_fest)
      getstr (pic_ergebnis, pic_vor_null_zeich, 1, 1)
   end
   if (pic_nach_stell - pic_nach_fest)
      let j = punkt_pos + pic_nach_fest + 1
      getstr (pic_ergebnis, pic_nach_null_zeich, j, 1)
   end

   let p1 = pic_ges_stell
   let p2 = pic_nach_stell
   let p3 = pic_vor_fest
   let p4 = pic_vor_null_zeich
   let p5 = pic_nach_fest
   let p6 = pic_nach_null_zeich
   let p7 = pic_ergebnis

   if p4 = "-"
      let p4 = " "
   end
   if p6 = "-"
      let p6 = " "
   end

end

/* ff */
procedure update_picture

   if p4 = " "
      let p4 = "-"
   end
   if p6 = " "
      let p6 = "-"
   end

   let pic_vor_stell = p1 - p2

   init p7
   init i
   while i < (pic_vor_stell - p3)
      putstr(p7,p4,0,0)
      let i = i + 1
   end
   
   init i
   while i < p3
      putstr(p7,"&",0,0)
      let i = i + 1
   end

   if p2
      putstr(p7,".",0,0)
   end
   
   init i
   while i < p5
      putstr(p7,"&",0,0)
      let i = i + 1
   end

   init i
   while i < (p2 - p5)
      putstr(p7,p6,0,0)
      let i = i + 1
   end
   
   display form picture_maske
   refresh

end
/* ff */
procedure waehle_picture
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       waehle_picture
-
-       In              :       pfad            char (80)       Suchpfad
                                prefix          char (14)       Vorsilbe incl.
                                                                Wildcard
                                argument        char (14)       Dateiname
                                suffix          char (14)       Nachsilbe incl.
                                                                Wildcard
-
-       Out             :       Datei mit Pfad  char
                                fehler          smallint        1 Abbruch, key5
                                                                2 kein Zugriff
                                                                  auf Platte
-
-       Beschreibung    :       Es wird ein ls abgesetzt mit 
                                pfad # prefix # argument # suffix
                                Ueber enter matrix menue kann ein Eintrag
                                gewaehlt werden, der dann komplett mit Pfad
                                zurueckgegeben wird.
-
--------------------------------------------------------------------------------
**/
   parameter
      field  mo_pic_ergebnis      char (22)
   end

   perform save_fkt
   perform disp_fkt (-1,-1)

   let pic_ergebnis = mo_pic_ergebnis /* global ueber's ganze modul */

   let anz_zeilen = 16
   open window picture_window (anz_zeilen,36)
   perform set_komm_pos
   set bordered window (anz_zeilen + 2,36,2,40)
   display pos (0,-1) title "\R Format waehlen \N"
   display screen picture_screen, form picture_maske

   perform read_picture
   perform update_picture
   display form picture_maske
   refresh

   init i
   enter form picture_maske

   actions
#     include "std_fkt"
      case key17
         perform programm_ende
         break
      case key5
         perform disp_fkt (9,-1)
         break
      case key12
         let pic_ergebnis = p7
         break

      before p4
      before p6
         perform disp_fkt (9,1) /* Ansehen */
         select window picture_window
         continue

      after p1 and keyup
         if p1 < (p3 + p5)
            display errortext "Zu wenig Gesamtstellen!"
            nextfield p1
         else
            perform update_picture
            if p5 = p2
               nextfield p5
            else
               nextfield p6
            end
         end
         continue

      after p1 and keydown
      after p1 and keycr
         if p1 < (p3 + p5)
            display errortext "Zu wenig Gesamtstellen!"
            nextfield p1
         else
            perform update_picture
         end
         continue

      after p2 and keyup
      after p2 and keydown
      after p2 and keycr
         if p2 > p1 or p2 < p5 or p2 > (p1 - p3)
            display errortext "Unzulaessige Eingabe!"
            nextfield p2
         else
            perform update_picture
         end
         continue

      after p3 and keyup
         if p3 > (p1 - p2)
            display errortext "Zu viele Vorkommastellen !"
            nextfield p3
         else
            perform update_picture
         end
         continue

      after p3 and keydown
      after p3 and keycr
         if p3 > (p1 - p2)
            display errortext "Zu viele Vorkommastellen !"
            nextfield p3
         else
            perform update_picture
            if p3 = (p1 - p2)
               nextfield p5
            end
         end
         continue

      after p4 and keyup
      after p4 and keydown
      after p4 and keycr
         let pstat1 = proc getptab ("null_char",p4)
         if not pstat1 /* fehler */
            display errortext ("Fehler bei getptab")
            nextfield p4
         else
            let p4 = ptabn.ptwert
            display form picture_maske
         end
         perform update_picture
         perform disp_fkt (9,-1)
         select window picture_window
         continue

      after p5 and keyup
         if p5 > p2
            display errortext "Zu viele feste Nachkommastellen !"
            nextfield p5
         else
            perform update_picture
            if p3 = (p1 - p2)
               nextfield p3
            end
         end
         continue

      after p5 and keydown
      after p5 and keycr
         if p5 > p2
            display errortext "Zu viele feste Nachkommastellen !"
            nextfield p5
         else
            perform update_picture
            if p5 = p2
               nextfield p1
            end
         end
         continue

      after p6 and keyup
         let pstat1 = proc getptab ("null_char",p6)
         if not pstat1 /* fehler */
            display errortext ("Fehler bei getptab")
            nextfield p6
         else
            let p6 = ptabn.ptwert
            display form picture_maske
         end
         perform update_picture
         perform disp_fkt (9,-1)
         select window picture_window
         continue

      after p6 and keydown
      after p6 and keycr
         let pstat1 = proc getptab ("null_char",p6)
         if not pstat1 /* fehler */
            display errortext ("Fehler bei getptab")
            nextfield p6
         else
            let p6 = ptabn.ptwert
            display form picture_maske
         end
         perform update_picture
         perform disp_fkt (9,-1)
         select window picture_window
         nextfield p1
         continue

      after p6 and key9
         perform waehle_null_char
         let p6 = null_char
         continue

      after p4 and key9
         perform waehle_null_char
         let p4 = null_char
         continue

   end

   select window picture_window
   close window /* picture_window */

   perform restore_fkt

   return (pic_ergebnis)

end /* waehle_picture */
