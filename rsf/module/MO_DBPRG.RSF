/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbprgrs
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	30.03.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    30.03.90   Simmchen	Modularisierung
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle prgrstufk , pr
-
-
------------------------------------------------------------------------------
*/

module mo_dbprg

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_prgrs
	proc prepread_prgrs
	proc putmxkey_prgrs
	proc recupdate_prgrs
	proc recdelete_prgrs
	proc recinsert_prgrs
	proc recread_prgrs
	proc recread_prgrs_pos
	proc getmxkey_prgrs
	proc recreaddef_prgrs
	proc inittable_prgrs
	proc savekey_prgrs
	proc backkey_prgrs
	proc initkey_prgrs
	proc cmpkey_prgrs
	proc clrdelstatus_prgrs
	proc getdelstatus_prgrs
	proc fetchcuu_prgrs
	proc fetchread_prgrs
	proc erasecursor_prgrs
	proc closeread_prgrs
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
end

IMPORT module mo_cons
end

IMPORT module mo_dbpr
	database bws
end

database bws
	table prgrstufk end
end

#include "colaenge"

field dsavekey 	 like prgrstufk.pr_gr_stuf
field ddefakey   like prgrstufk.pr_gr_stuf value -1
field dsavekeyp  like pr.a 
field ddefakeyp  like pr.a value -1
field dcurskey	 like prgrstufk.pr_gr_stuf dimension(CMAXMATRIX)

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_prgrs
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_prgrs.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_prgrs

	prepare cursor cuu_prgrs from sql
		select #prgrstufk.* from prgrstufk
		where @prgrstufk.mdn and
		      @prgrstufk.pr_gr_stuf for update
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_prgrs
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cur_prgrs.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_prgrs

	prepare cursor cur_prgrs from sql
		select #prgrstufk.* from prgrstufk
		where @prgrstufk.mdn and
		      @prgrstufk.pr_gr_stuf
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_prgrs
-
-	In		: danzahl	smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_prgrs

parameter field danzahl smallint end

	let dcurskey[danzahl] = prgrstufk.pr_gr_stuf

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_prgrs
-
-	In		: dzeiger	smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_prgrs

parameter field dzeiger smallint end

	let prgrstufk.pr_gr_stuf = dcurskey[dzeiger]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_prgrs
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_prgrs gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_prgrs

	execute from sql 
		update prgrstufk
		set @prgrstufk.*
		where current of cuu_prgrs
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_prgrs
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_prgrs gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_prgrs

	let prgrstufk.delstatus = -1

	execute from sql
		update prgrstufk
		set @prgrstufk.delstatus
		where current of cuu_prgrs
		end

	if sqlstatus
		return (sqlstatus)
	end
	
	execute from sql 
		delete from pr
		where pr.mdn        = $prgrstufk.mdn and
		      pr.kun_pr     = 0                and
		      pr.pr_gr_stuf = $prgrstufk.pr_gr_stuf
	end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_prgrs
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_prgrs

	execute from sql 
		insert into prgrstufk (%prgrstufk.*)
		values ($prgrstufk.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_prgrs
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_prgrs
	execute from sql
		select #prgrstufk.* from prgrstufk 
		where @prgrstufk.mdn and
		      @prgrstufk.pr_gr_stuf
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_prgrs_pos
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_prgrs_pos
	execute from sql
		select #pr.* from pr where 
		@pr.mdn        and
		pr.kun_pr = 0  and
		@pr.pr_gr_stuf and
		@pr.a 
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_prgrs
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_prgrs

field dsql smallint

	let prgrstufk.pr_gr_stuf = ddefakey

	perform recread_prgrs returning (dsql)
	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_prgrs
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_prgrs

	init table prgrstufk
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: savekey_prgrs
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure sichert den alten Schluessel-
-			  wert.   
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_prgrs

	let dsavekey = prgrstufk.pr_gr_stuf
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: backkey_prgrs
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt den gesicherten  
-			  Schluesselwert zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_prgrs

	let prgrstufk.pr_gr_stuf = dsavekey
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_prgrs
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_prgrs

	init prgrstufk.pr_gr_stuf
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_prgrs
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_prgrs

	if dsavekey = prgrstufk.pr_gr_stuf
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_prgrs
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_prgrs

	let prgrstufk.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_prgrs
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_prgrs

	return (prgrstufk.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_prgrs 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_prgrs.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_prgrs

	fetch cursor cur_prgrs
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_prgrs 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_prgrs
-			  ,cur_prgrs und cus_prgrs.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_prgrs

	erase cursor cus_prgrs
	erase cursor cuu_prgrs
	erase cursor cur_prgrs
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_prgrs 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_prgrs.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_prgrs

	fetch cursor cuu_prgrs
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_prgrs 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_prgrs.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_prgrs

	close cursor cur_prgrs
	return (sqlstatus) 
END

procedure what_kennung
if "" = "@(#) mo_dbprgrs.rsf	1.400	05.11.91	by RO>" end
end
