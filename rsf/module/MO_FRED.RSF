


/***
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Vorstadtstr. 59
-	7465 Geislingen
-	Tel.: 07433/2413-14
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_fred
-
-	Interne Operatoren	:	fracht_edit
-
-	Autor			:	JG
-	Erstellungsdatum	:	05.02.92 
-
-	Projekt			:	BWS
-	Version			:	1.3
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	@oooo	1.3	14.02.92	JG	druckerwahl fest auf fracht.cfg
-						 und paket.cfg fuer diese Abl.
-	@xxxx	1.3	18.02.92	JG	feldlaenge 50 -> 55
-	#1431	1.400	30.03.93	KB	Menge von 4 auf 5 Stellen
-	#1593	1.402	07.07.93	JG	paket_par
-	#1757	1.402	15.11.93	JG	gesamt-Gewicht 
-	#1841	1.402h	09.02.94	JG	kun_bran2
-	#1948	1.402h	18.03.94	JG	alternativ-Ablauf ploener
-	#1990	1.402h	31.03.94	JG	Ploener-Liefer-Datum
------------------------------------------------------------------------------
-
-	Modulbeschreibung	: Frachtbriefe auf Basis Lieferschein
-					editieren,
-					Frachtbrief und Aufkleber drucken
-
- 	Aufruf			: perform fracht_edit 
-						(mdn,fil,kun,ls,dat,disp)
-					  returning (dret)
-
-
***/

MODULE mo_fred

/*----- Export Section -----------------------------------------------------*/

export 
	proc fracht_edit
	field paketmerker	/* #1593 */
	field dalternative	/* #1948 */
				/* erstmalig fuer ploener = 24 */
end

/*----- Import Section -----------------------------------------------------*/


import module mo_fraef	/* maskenmodul */
end

import module mo_allg_
end

import module mo_druck
end

import module mo_repor
	field drucker_name
end

import module mo_inkey
end

import module mo_item
end

import module mo_ma_ut
end

import module mo_men_m
end

import module mo_meld
end

import module mo_work
end

import module mo_funk
	proc janein
end

import module mo_frach
end

/* #1990 A */
import module mo_dbcal
	proc read_cal_wt2
end
/* #1990 A */
/* #2453 A */
import module mo_sys_p
end
/* #2453 A */
/*----- Lokale Deklarationen Modul -----------------------------------------*/

database bws
/* aus mo_fracht kommend : table fracht end */
	table mdn end
	table fil end
	table kun end
	table ptabn end
	table a_kun end
	table a_bas end
	table adr end
	table lsk end
	table lsp end
	table a_hndw end
	table a_eig end
	table a_eig_div end
end
/*----- Include-Files ------------------------------------------------------*/
#include "errdef"	/* allg. Errordefinitionen */
#include "messdef"	/* allg. Menuemanagerdef. */

/*----- Prozeduruebergreifende Datendeklarationen --------------------------*/
field dret 		smallint 		/* allg. Return-Code */
field updatemerker	smallint	/* TRUE = updaten */
field wahlmerker	smallint       /*TRUE = es wurde der drucker geaendert*/
field aktuell		smallint	/* TRUE = updaten */
  field dfredmdn	like fracht.mdn		/* Mandant */
  field dfredfil	like fracht.fil		/* Filiale */
  field dfredkun	like fracht.kun		/* Kunde   */
  field dfredls		like fracht.ls		/* Lieferschein */
  field dfredliefdat	like fracht.dat		/* Ls-datum     */
  field dfreddisp	smallint	/* aus disp TRUE=nur Anzeige mdn/fil */
  field feldlaenge	smallint	/* @xxxx */

  field paketmerker	smallint 	/* #1593 */ /* 1 = nur paketaufkleber */
						    /* 0 = kompletter Ablauf */
  field dalternative	smallint	/* #1948 */ /* z.Z. =24 = ploener */
  /* z.B. = 24 => dann anzahl nicht entsprechend Rechenvorschrift, sondern aus
		lsp.leer_pos */

table save_fracht like fracht end

field d2453sch	smallint
field d2453gul	smallint
field sys_par_wert char (1) 
field sys_par_bez  char (20)


/*---------------------------------------------------------------------------
-	Procedure	: fracht_edit
-	In		: mdn		Mandant 
			  fil		Filiale 
			  kun		Kunde
			  ls		Lieferschein
			  dat		Lieferdatum
			  disp		TRUE = nur Anzeige mdn/fil
					FALSE = Selektion entsprechend Maske 
	WMWMWMWMWMW 		z.Z nicht realisiert  	WMWMWMW
-	Beschreibung    : Bereitstellung default-Bestueckung, Modifikation
			  und Ergaenzung des Frachtbriefes
			  Druck der Frachtbriefe und Aufkleber nach entspr.
			  Wahl
---------------------------------------------------------------------------*/

proc fracht_edit
parameter
  field fredmdn		like fracht.mdn		/* Mandant */
  field fredfil		like fracht.fil		/* Filiale */
  field fredkun		like fracht.kun		/* Kunde   */
  field fredls		like fracht.ls		/* Lieferschein */
  field fredliefdat	like fracht.dat		/* Ls-datum     */
  field freddisp	smallint		/* TRUE = nur Anzeige mdn/fil */
end
if "" = "@(#) mo_fred	2.000h	19.12.95	by KB" end
	perform save_fkt  
  let dfredmdn = fredmdn
  let dfredfil = fredfil
  let dfredkun = fredkun
  let dfredls = fredls
  let dfredliefdat = fredliefdat
  let dfreddisp	= freddisp	
  
  let lock_mode = 1
  let wahlmerker = FALSE
  let feldlaenge = 55	/* @xxxx */

perform save_fkt
perform disp_fkt ( -1 0 )
perform disp_fkt ( 8 14 )

/* #1757 A */
if paketmerker = 0
	perform disp_fkt ( 9 40 )	/* Uebernehemn */
end
/* #1757 E */

open window fracht_window dimension (18,76)
set window dimension (18,76)
	offset (2,1)	/* alt 3,1 */
perform dispitem ("frachte", -1) 
	returning (dret)
	refresh
perform set_komm_pos
/* #1593 A */
	if paketmerker = 1
		perform paketablauf
	else
/* #1593 E */
		perform bearbeiten
		perform cursorenerasen	/* #1593 */
/* #1593 */ end
	perform restore_fkt
end

procedure bearbeiten

	field merkefrei_txt like fracht.frei_txt1
	field hilfefrei_txt like fracht.frei_txt1
	field steuerung	 char (1)
	field dnachdruck char (1)

	let steuerung = "E"

while TRUE
	init updatemerker aktuell 
	perform cursorenpreparen
	perform fuelle_kopf
	let dret = proc deffrachtbrief(1)	/* #1593 */
	if dret = -250
		perform disp_msg ( -250 1 )
		perform rollbackwork
		return
	end
	if dret
		perform disp_sql(dret)
		perform rollbackwork
		return
	end
	

	enter form mafrae
/* ----------------------- ACTIONS ------------------ */
	actions


#include "std_fkt"
		before enter
			perform statanzeigen
			init syskey
		continue

	case KEY17
		perform rollbackwork
		perform programm_ende
		let steuerung = "E"
		break
	case KEY5
		if not aktuell
			perform commitwork
			let steuerung = "E"
			break 
		end
		if proc janein ("[nderungen  abspeichern ?")
			perform frachtbrief_speichern(1)
		else
			perform rollbackwork
			let dret = proc deffrachtbrief(1)	/* #1593 */
			init aktuell
		end
		nextfield mafrae.m1adr_nam1

/* #1757 A */
	case KEY9
		perform  frachtbriefaktualisieren
		perform frachtbrief_speichern(1)
		let dret = proc deffrachtbrief(1)
		display form mafrae
		refresh
		continue current	/* weiter gehts */
/* #1757 E */


	case KEY8	/* drucken */
		perform frachtbrief_speichern(0)
		let steuerung = "D"	/* druck-routinen */
		break
			
	case KEY12
		perform frachtbrief_speichern(1)
		let dret = proc deffrachtbrief(1)	/* #1593 */
		display form mafrae
		refresh
		continue current	/* weiter gehts */
/* -------------- Rundlauf --------------------------- */
	after mafrae.m1aufanz
		
		if syskey = keyup
			continue
		else
			nextfield mafrae.m1frei_txt1 /* ein stueck nach oben */
		end

	after mafrae.m1adr_nam1
		if LASTCOLPOS > 0 let aktuell = TRUE end
		continue

	after mafrae.m1adr_nam2
		if LASTCOLPOS > 0 let aktuell = TRUE end
		continue

	after mafrae.m1str
		if LASTCOLPOS > 0 let aktuell = TRUE end
		continue

	after mafrae.m1plz
		if LASTCOLPOS > 0 let aktuell = TRUE end
		continue

	after mafrae.m1ort1
		if LASTCOLPOS > 0 let aktuell = TRUE end
		continue


	after mafrae.m1lief_term
		if LASTCOLPOS > 0 let aktuell = TRUE end
		continue

	after mafrae.m1dat
		if LASTCOLPOS > 0 let aktuell = TRUE end
		continue

	after mafrae.m1krz_txt	/* verfuegung */
		if LASTCOLPOS > 0 let aktuell = TRUE end
		continue

	after mafrae.m1tel
		if LASTCOLPOS > 0 let aktuell = TRUE end
		continue

	after mafrae.m1wrt	
		if LASTCOLPOS > 0 let aktuell = TRUE end
		continue


	before mafrae.m1frei_txt1
		let merkefrei_txt = fracht.frei_txt1	continue

	after mafrae.m1frei_txt1
		if LASTCOLPOS > 0
			let aktuell = TRUE	
/* @xxxx */		if LASTCOLPOS < feldlaenge	/* feldende erreicht */
				init hilfefrei_txt
/* @xxxx */			getstr (merkefrei_txt,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
/* @xxxx */			putstr (fracht.frei_txt1,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
				display form mafrae refresh
			end
		end
		if SYSKEY = KEYDOWN	let COLPOS = LASTCOLPOS end
		continue

	before mafrae.m1frei_txt2
		let merkefrei_txt = fracht.frei_txt2	continue

	after mafrae.m1frei_txt2
		if LASTCOLPOS > 0
			let aktuell = TRUE	
/* @xxxx */		if LASTCOLPOS < feldlaenge	/* feldende erreicht */
				init hilfefrei_txt
/* @xxxx */			getstr (merkefrei_txt,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
/* @xxxx */			putstr (fracht.frei_txt2,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
				display form mafrae refresh
			end
		end
		if ((SYSKEY = KEYDOWN) or (SYSKEY = KEYUP))
			let COLPOS = LASTCOLPOS	
		end
		continue

	before mafrae.m1frei_txt3
		let merkefrei_txt = fracht.frei_txt3	continue

	after mafrae.m1frei_txt3
		if LASTCOLPOS > 0
			let aktuell = TRUE	
/* @xxxx */		if LASTCOLPOS < feldlaenge	/* feldende erreicht */
				init hilfefrei_txt
/* @xxxx */			getstr (merkefrei_txt,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
/* @xxxx */			putstr (fracht.frei_txt3,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
				display form mafrae refresh
			end
		end
		if ((SYSKEY = KEYDOWN) or (SYSKEY = KEYUP))
			let COLPOS = LASTCOLPOS	
		end
		continue

	before mafrae.m1frei_txt4
		let merkefrei_txt = fracht.frei_txt4	continue

	after mafrae.m1frei_txt4
		if LASTCOLPOS > 0
			let aktuell = TRUE	
/* @xxxx */		if LASTCOLPOS < feldlaenge	/* feldende erreicht */
				init hilfefrei_txt
/* @xxxx */			getstr (merkefrei_txt,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
/* @xxxx */			putstr (fracht.frei_txt4,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
				display form mafrae refresh
			end
		end
		if ((SYSKEY = KEYDOWN) or (SYSKEY = KEYUP))
			let COLPOS = LASTCOLPOS	
		end
		continue

	before mafrae.m1frei_txt5
		let merkefrei_txt = fracht.frei_txt5	continue

	after mafrae.m1frei_txt5
		if LASTCOLPOS > 0
			let aktuell = TRUE	
/* @xxxx */		if LASTCOLPOS < feldlaenge	/* feldende erreicht */
				init hilfefrei_txt
/* @xxxx */			getstr (merkefrei_txt,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
/* @xxxx */			putstr (fracht.frei_txt5,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
				display form mafrae refresh
			end
		end
		if ((SYSKEY = KEYDOWN) or (SYSKEY = KEYUP))
			let COLPOS = LASTCOLPOS	
		end
		continue

	before mafrae.m1frei_txt6
		let merkefrei_txt = fracht.frei_txt6	continue

	after mafrae.m1frei_txt6
		if LASTCOLPOS > 0
			let aktuell = TRUE	
/* @xxxx */		if LASTCOLPOS < feldlaenge	/* feldende erreicht */
				init hilfefrei_txt
/* @xxxx */			getstr (merkefrei_txt,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
/* @xxxx */			putstr (fracht.frei_txt6,hilfefrei_txt,
					LASTCOLPOS + 1 feldlaenge - LASTCOLPOS)
				display form mafrae refresh
			end
		end
		if SYSKEY = KEYUP	let COLPOS = LASTCOLPOS	end
		continue


/* ----------------------- actions ------------------ */
	end
	if steuerung = "E"
		break
	end
	move table fracht to table save_fracht
	if ((dpaufkl = "J") and (dpaufanz > 0))
/* ------- @oooo ----------------------------------------
=		perform druwa ("      Paketaufkleber      ")
------------------------------------------------------ */
/* #1593 /* @ooo */	perform frdrucker_waehlen ( "paket.cfg" ) */
/* macht jetzt der gunter bei sich nochmal .... */
		perform paket_drucken
				(fracht.mdn,fracht.fil,fracht.kun,
					fracht.dat,fracht.stat,dpaufanz)
			returning ( dret )
	end
/* ------- @oooo ----------------------------------
=	perform druwa ("       Frachtbriefe       ")
------------------------------------------------------ */
/* #1593 /* @ooo */	perform frdrucker_waehlen ( "fracht.cfg" ) */
/* macht jetzt der gunter bei sich nochmal .... */
	if fracht.stat = 1
		let dnachdruck = "J"
	else
		let dnachdruck = "N"
	end
	move table save_fracht to table fracht
	perform druck_ausgabe 
		(fracht.mdn,fracht.fil,fracht.dat,
			fracht.kun,fracht.kun,dnachdruck)
		returning ( dret )
	move table save_fracht to table fracht
/* -------------------- windows usw. neu anlegen ------------------------- */
	perform set_com_file (mess_menue.prog_nr)
	perform set_hlp_file (mess_menue.prog_nr)

	break	/* nicht mehr zurueck nach drucken S.H. 17.02.91 */

/* #1593 dummies muessen weg ----
= open window fracht_window dimension(18,76) set window dimension(18,76)
= offset (2,1) perform set_komm_pos perform dispitem ("frachte", -1) 
= returning (dret) /* continue */
=----------------- */
end	/* while true */

/* @ooo ----- nicht mehr noetig ------------------------
= if wahlmerker = TRUE perform druwa (" Lieferscheine/Rechnungen ")
= let dret = proc waehle_drucker() end
=--------------------------------------------------------------- */
	close window fracht_window
	perform restore_fkt
	return 
end




procedure fuelle_kopf

/* ------------------ Mandant -------------------------------- */
	init table fracht
	execute from sql
		select #mdn.mdn , #adr.adr_krz
		from mdn, adr
		where mdn.mdn = $dfredmdn
		   and adr.adr = mdn.adr
	end
	if sqlstatus	/* es koennte nur mdn-adr fehlen */
		if dfredmdn = 0
			let dpmdn_krz = "Zentrale"
		else
			init dpmdn_krz
		end
	else
		let dpmdn_krz = adr.adr_krz
	end
	let fracht.mdn = dfredmdn

/* ------------------ Filiale -------------------------------- */
	execute from sql
		select #fil.fil , #adr.adr_krz
		from fil, adr
		where fil.mdn = $dfredmdn
		  and fil.fil = $dfredfil
		  and adr.adr = fil.adr
	end
	if sqlstatus	/*es koennte adr fehlen oder filiale 0 nicht angelegt*/
		if dfredfil = 0 and dfredmdn <> 0
			let dpfil_krz = "Mandant"
		else
			init dpfil_krz
		end
	else
		let dpfil_krz = adr.adr_krz
	end
	let fracht.fil = dfredfil
	
/* --------------- Kunde -------------------- */

	execute from sql
	select #kun.% from kun where 
		kun.mdn = $dfredmdn
	    and kun.fil = $dfredfil
	    and kun.kun = $dfredkun
	end
	
	let fracht.kun = dfredkun
	display mafrae.m1mdn
	display mafrae.m1fil
	display mafrae.mp1mdn_krz
	display mafrae.mp1fil_krz
	display mafrae.m1kun
	display form mafrae
	let dpaufkl = "N"

	refresh
end	/* procedure fuelle_kopf */

procedure deffrachtbrief
parameter
	field errflag smallint	/* #1593 */
end

field yaehler   smallint	/* zaehler, wievielte LS-Pos. */
field gesamtsum smallint	/* gesamtzahl verp.einh. */
field verpackeinh decimal (6,1)	/* rechenvariable fuer verp.einh */
field verpackeinh2 decimal (6,1) /* hilfsvariable fuer runden */
field zeilstring char (60)	/* hilfsstring zum zusammenstellen */
field hilfstring char (09)	/* hilfsstring zum kappen usw */
field hilfstring2 char (09)	/* hilfsstring fuer runden  */
field a_kun_status smallint	/* merker, ob artikelnr in a_kun existiert */
	
	perform beginwork
	prepare cursor fre_update_cursor from sql
		select #fracht.% from fracht
		where @fracht.mdn
		  and @fracht.fil
		  and @fracht.kun
		  and fracht.ls = $dfredls
			for update
	end
	close cursor fre_update_cursor
	fetch cursor fre_update_cursor
	if not sqlstatus 
		let updatemerker = TRUE
		let aktuell = FALSE
		perform statanzeigen
		display form mafrae
		refresh
		
		return (0)
	end
	let updatemerker = FALSE
	if  sqlstatus 	<> 100
		let aktuell = FALSE
		return  ( sqlstatus )
	end
	let aktuell = TRUE	/* neuerfassung */
	let fracht.lief_term = dfredliefdat
/* #1990 A */
	if dalternative = 24
		let fracht.dat = proc read_cal_wt2 (dfredliefdat)
	else
/* #1990 E  */
		let fracht.dat = dfredliefdat
/* #1990 */ end
	let fracht.stat = 0 

	execute from sql
			select #adr.% from adr where adr.adr = $kun.adr2
	end
	if sqlstatus 
		init table adr
		if errflag 	/* #1593 bedingt ... */
			display message ("Adresse nicht gefunden " )
			refresh
			perform g_inkey
			display message ("                          " )
			refresh
		end
	end
	let fracht.adr_nam1 = adr.adr_nam1
	let fracht.adr_nam2 = adr.adr_nam2
	let fracht.str	    = adr.str
	let fracht.plz      = adr.plz
	let fracht.ort1     = adr.ort1
/* --------- gibts noch gar nicht 
=	let fracht.tel 	    = kun.tel
------------- */
	if errflag 	/* #1593 bedingt ... */
		display form mafrae
		refresh
	end


	init yaehler
	init gesamtsum

	init fracht.lief_gew	/* #1757 */
	
	close cursor fre_lsp_cursor
	fetch cursor fre_lsp_cursor
	while not sqlstatus
/* #1757 ----- nach weiter unten, weil erst mal alles ausgewertet wird ----
= if errflag 	/* #1593 bedingt ... */ if yaehler >= 6
= /* max 6 positionen einpflegbar */ display message
= ("Es gibt mehr als 6 Positionen " ) refresh perform g_inkey
= display message ("                                 " ) refresh return ( 0 )
= end end
= --------------------- */
		init zeilstring
		close cursor fre_a_kun
		fetch cursor fre_a_kun
/* 1841 A */
		if sqlstatus
		    if kun.kun_bran2 > "0"
			close cursor fre_a_bran
			fetch cursor fre_a_bran
		    end
		end
/* #1841 E */
		if sqlstatus 
			let a_kun_status = FALSE
		else
			let a_kun_status = TRUE
		end
		if a_kun_status	/* a_kun-Satz vorhanden */
			let hilfstring = a_kun.a_kun	/* kappen */
			let zeilstring = hilfstring
			if a_kun.inh = 0	/* div. durch 0 */
				let a_kun.inh = 1
			end
/* #1948 A */
			if dalternative = 24
				if is null (lsp.leer_pos)
					let lsp.leer_pos = 0
				end
				let verpackeinh = lsp.leer_pos
				let verpackeinh2 = verpackeinh
			else
/* #1948 E */
			   let verpackeinh =  (lsp.lief_me / a_kun.inh)
			   let verpackeinh2 = INT (lsp.lief_me / a_kun.inh)
/* #1948 */		end
			if  verpackeinh2 < verpackeinh
				let verpackeinh = verpackeinh2 + 1
			end
			let gesamtsum = gesamtsum + verpackeinh
			let hilfstring = picture(verpackeinh, "-###&") /*#1431*/
			putstr (zeilstring,hilfstring,11,5) /* #1431 */
/* #1757 A */
			close cursor fre_a_bas
			fetch cursor fre_a_bas

			if a_bas.hnd_gew  = "G"
			    let fracht.lief_gew = fracht.lief_gew + lsp.lief_me
			else
			    let fracht.lief_gew = fracht.lief_gew + ROUND (
			     ( lsp.lief_me * a_bas.a_gew ) AT 3 )
			end
/* #1757 E */
			if yaehler < 6	/* #1757 : nur 6 Zeilen noetig */
			   close cursor fred_ptabn
			   fetch cursor fred_ptabn
			   if not sqlstatus
				putstr (zeilstring,ptabn.ptbezk,17,9) /*#1431*/
			   end
			   if a_kun.a_bz1 > " "
				putstr (zeilstring,a_kun.a_bz1,27,24) /*#1431*/
			
			   else
/* #1757 nicht mehr noetig ------------
=				close cursor fre_a_bas
=				fetch cursor fre_a_bas
=				if not sqlstatus
--------------- */
					putstr (zeilstring,a_bas.a_bz1,27,24)
					/* #1431 */
/* #1757 ---------------------	end ----------- */
			   end
			end			/* #1757 */
		else
			/* kunden-artikel nummer uebergehen,
			me_einh und verp.einh aus a_hndw,a_eig,a_eig_div */

			init table a_bas
			close cursor fre_a_bas
			fetch cursor fre_a_bas
/* #1757 A */
			if a_bas.hnd_gew  = "G"
			    let fracht.lief_gew = fracht.lief_gew + lsp.lief_me
			else
			    let fracht.lief_gew = fracht.lief_gew + ROUND (
			     ( lsp.lief_me * a_bas.a_gew ) AT 3 )
			end
/* #1757 E */
			switch a_bas.a_typ
			case 1
			   close cursor fre_a_hndw
			   fetch cursor fre_a_hndw
			   if not sqlstatus
			     let a_kun.me_einh_kun = a_hndw.me_einh_kun
			     if a_hndw.inh = 0	/* div. durch 0 */
				let a_hndw.inh = 1
			     end
			     let verpackeinh =  (lsp.lief_me / a_hndw.inh)
			     let verpackeinh2 = INT (lsp.lief_me / a_hndw.inh)
			   else
			     let a_kun.me_einh_kun = a_bas.me_einh
			     let verpackeinh =  (lsp.lief_me)
			     let verpackeinh2 = INT (lsp.lief_me)
			   end
			   break
			case 2
			  close cursor fre_a_eig
			  fetch cursor fre_a_eig
			  if not sqlstatus
			    let a_kun.me_einh_kun = a_eig.me_einh_ek
			    if a_eig.inh = 0	/* div. durch 0 */
				let a_eig.inh = 1
			    end
			    let verpackeinh =  (lsp.lief_me / a_eig.inh)
			    let verpackeinh2 = INT (lsp.lief_me / a_eig.inh)
			  else
			    let a_kun.me_einh_kun = a_bas.me_einh
			    let verpackeinh =  (lsp.lief_me)
			    let verpackeinh2 = INT (lsp.lief_me)
			  end
			  break
			case 3
			  close cursor fre_a_eig_div
			  fetch cursor fre_a_eig_div
			  if not sqlstatus
			    let a_kun.me_einh_kun = a_eig_div.me_einh_ek
			    if a_eig_div.inh = 0	/* div. durch 0 */
				let a_eig_div.inh = 1
			    end
			    let verpackeinh =  (lsp.lief_me / a_eig_div.inh)
			    let verpackeinh2 = INT (lsp.lief_me / a_eig_div.inh)
			  else
			    let a_kun.me_einh_kun = a_bas.me_einh
			    let verpackeinh =  (lsp.lief_me)
			    let verpackeinh2 = INT (lsp.lief_me)
			  end
			  break
			otherwise
			    let a_kun.me_einh_kun = a_bas.me_einh
			    let verpackeinh =  (lsp.lief_me)
			    let verpackeinh2 = INT (lsp.lief_me)
			  break
			end
/* #1948 A */
			if dalternative = 24
			    if is null (lsp.leer_pos)
					let lsp.leer_pos = 0
			    end
			    let verpackeinh = lsp.leer_pos
			    let verpackeinh2 = verpackeinh
			end
/* #1948 E */
			if  verpackeinh2 < verpackeinh
				let verpackeinh = verpackeinh2 + 1
			end
			let gesamtsum = gesamtsum + verpackeinh
			let hilfstring = picture(verpackeinh, "####&") /*#1431*/
			putstr (zeilstring,hilfstring,11,5) /* #1431 */
			if yaehler < 6 /* #1757 mehr ist nicht noetig .. */
			   close cursor fred_ptabn
			   fetch cursor fred_ptabn
			   if not sqlstatus
				putstr(zeilstring,ptabn.ptbezk,17,9) /*#1431*/
			   else
				putstr(zeilstring,lsp.lief_me_bz,17,0) /*#1431*/
			   end
			   putstr (zeilstring,a_bas.a_bz1,27,24) /* #1431 */
			end	/* #1757 */
		end
		switch yaehler
		   case 0
			let fracht.frei_txt1 = zeilstring
			break
		   case 1
			let fracht.frei_txt2 = zeilstring
			break
		   case 2
			let fracht.frei_txt3 = zeilstring
			break
		   case 3
			let fracht.frei_txt4 = zeilstring
			break
		   case 4
			let fracht.frei_txt5 = zeilstring
			break
		   case 5
			let fracht.frei_txt6 = zeilstring
			break
		   otherwise break
		end
		if errflag	/* #1593 bedingt .... */
			display form mafrae
			refresh
		end
		
		let yaehler = yaehler + 1
		fetch cursor fre_lsp_cursor
	end	/* while not sqlstatus */
	let fracht.anz = gesamtsum
	if errflag	/* #1593 bedingt .... */
		display form mafrae
		refresh
	end
/* #1757 A */
	if errflag 
	   if yaehler >= 6	/* max 6 positionen einpflegbar */
		display message ("Es gibt mehr als 6 Positionen " )
		refresh
		perform g_inkey
		display message ("                                 " )
		refresh
	   end
	end
/* #1757 E */

	return ( 0 )


end /* deffrachtbrief */

procedure cursorenpreparen
	close cursor fre_lsp_cursor
	erase cursor fre_lsp_cursor
	prepare cursor fre_lsp_cursor from sql
		select #lsp.% from lsp
		 where lsp.ls = $dfredls 
		   and lsp.mdn = $dfredmdn
		   and lsp.fil  = $dfredfil
			order by lsp.a
	end

	close cursor fre_a_kun
	erase cursor fre_a_kun
	prepare cursor fre_a_kun from sql
		select #a_kun.% from a_kun
		 where a_kun.mdn = $dfredmdn
		   and a_kun.fil = $dfredfil
		   and a_kun.kun = $dfredkun
		   and a_kun.a   = $lsp.a
	end

/* 1841 A */
	close cursor fre_a_bran
	erase cursor fre_a_bran
	prepare cursor fre_a_bran from sql
		select #a_kun.% from a_kun
		 where a_kun.mdn = $dfredmdn
		   and a_kun.fil = $dfredfil
		   and a_kun.kun = 0
		   and a_kun.kun_bran2 = $kun.kun_bran2
		   and a_kun.a   = $lsp.a
	end
/* kundenstamm wird in fuelle_kopf jedenfalls vor 1.fetch aktualisiert */
/* 1841 E */

	close cursor fre_a_bas 
	erase cursor fre_a_bas
	prepare cursor fre_a_bas from sql
		select #a_bas.% from a_bas
		 where a_bas.a   = $lsp.a
	end

	close cursor fred_ptabn
	erase cursor fred_ptabn
	prepare cursor fred_ptabn from sql
		select #ptabn.% from ptabn
		 where ptabn.ptitem = "me_einh"
		   and ptabn.ptlfnr = $a_kun.me_einh_kun
	end

	close cursor fre_a_hndw
	erase cursor fre_a_hndw
	prepare cursor fre_a_hndw from sql
		select #a_hndw.% from a_hndw
		 where a_hndw.a = $lsp.a
		   and a_hndw.delstatus = 0
	end

	close cursor fre_a_eig
	erase cursor fre_a_eig
	prepare cursor fre_a_eig from sql
		select #a_eig.% from a_eig
		 where a_eig.a = $lsp.a
		   and a_eig.delstatus = 0
	end

	close cursor fre_a_eig_div
	erase cursor fre_a_eig_div
	prepare cursor fre_a_eig_div from sql
		select #a_eig_div.% from a_eig_div
		 where a_eig_div.a = $lsp.a
		   and a_eig_div.delstatus = 0
	end
	
end	/* procedure cursorenpreparen */

procedure statanzeigen
	if fracht.stat = 0
		let dpstat = "(offen)"
	else
		let dpstat = "(gedruckt)"
	end
	display mafrae.m1stat
	display mafrae.mp1stat
	refresh
end

procedure frachtbrief_speichern

parameter field warten smallint end

	let fracht.ls = dfredls

	if updatemerker
		execute from sql
		update fracht
			set @fracht.%
			where current of fre_update_cursor
		end
	else
		execute from sql
		insert into fracht ( %fracht.*)
			values ( $fracht.*)
		end
	end
	if not sqlstatus and sqlerror (2) > 0
		init aktuell 
		let updatemerker = TRUE
		perform commitwork
		perform disp_msg ( 0191 warten )
		select window fracht_window
		perform set_komm_pos
	elseif sqlstatus = -250
		init aktuell
		perform rollbackwork
		perform disp_msg (-250 1 )
	else
		init aktuell
		display errortext (sqlstatus)
		perform rollbackwork
	end
end

/* @oooo ---------- nicht mehr erwuenscht ---------
=procedure druwa
=parameter field zweck char ( 27 ) end
=
=field text1	char (27)
=field text2	char (27)
=
=form hw_win	default reverse
=	field m1text1	use text1
=			pos ( 1 1 ) length 27
=	field m1text2	use text2 
=			pos ( 2 1 ) length 27
=end
=
=	let text1 = "      Druckerwahl fuer      "
=	let text2 = zweck
=
=	open window hinweis_window (04,29)
=	set bordered window ( 06,31,8,26)
=	display form hw_win
=	refresh
=	perform g_inkey
=	let wahlmerker = TRUE
=	init syskey
=close window hinweis_window
=end
=--------- @oooo ------- nicht mehr erwuenscht ---- */

/* #1593 ----------
=/* --------- @oooo ---- A ---- */
=procedure frdrucker_waehlen
=parameter field namensfeld char (20) end
=field int_dr_nam char ( 100 )
=
=   init int_dr_nam
=
=   environment ("FRMPATH",int_dr_nam)
=   putstr (int_dr_nam, "\"     , strlen(int_dr_nam)+1,1)
=   putstr (int_dr_nam, namensfeld  , strlen(int_dr_nam)+1,strlen(namensfeld))
=   let drucker_name = int_dr_nam
=end
=/* --------- @oooo ---- E ---- */
------------ */
/* #1593 A */
procedure  cursorenerasen
	close cursor fre_lsp_cursor
	erase cursor fre_lsp_cursor

	close cursor fre_a_kun
	erase cursor fre_a_kun
/* 1841 A */
	close cursor fre_a_bran
	erase cursor fre_a_bran
/* 1841 E */
	close cursor fre_a_bas 
	erase cursor fre_a_bas 

	close cursor fred_ptabn
	erase cursor fred_ptabn

	close cursor fre_a_hndw
	erase cursor fre_a_hndw

	close cursor fre_a_eig
	erase cursor fre_a_eig

	close cursor fre_a_eig_div
	erase cursor fre_a_eig_div
end

/* #1593 A */

procedure paketablauf

field	dpgesanzsave like fracht.anz

/* #2453 A */
	if d2453gul = 0 
	    perform sys_par_holen ("tol_par")
		returning ( dret , sys_par_wert , sys_par_bez )
	    if dret = 0
		let d2453sch = 0 
	    else
		let d2453sch = sys_par_wert
	    end 
	    let d2453gul = 1
	end
	
/* #2453 E */


/* es muss trotzdem erst mal ein default-fracht-satz erzeugt werden */
	init updatemerker aktuell 
	perform cursorenpreparen
	perform fuelle_kopf
	display form mafrae
	refresh
	erase (  7 ,8  )
	let dret = proc deffrachtbrief(0)
	if dret = -250
		perform disp_msg ( -250 1 )
		perform rollbackwork
		return
	end
	display form mafrae
	refresh
	erase (  7 ,8  )
	if dret <> 0
		perform disp_msg ( 100 1 )
		perform rollbackwork
		return
	end
	perform frachtbrief_speichern(0)

	let dpaufkl = "J"
	let dpaufanz = 1
	let dpgesanzsave = fracht.anz		/* #2453 */

	enter form mapaket
/* ----------------------- ACTIONS ------------------ */
	actions
    before enter
	display form mapaket
	continue

#include "std_fkt"


	after mapaket.m2aufanz 
/* #2453 A */
		if d2453sch = 1
		    if ((dpaufanz <> fracht.anz)  and (dpaufanz > 0 ))	
			let fracht.anz = dpaufanz
			display mafrae.m1anz
			refresh
		    end
		end
/* #2453 E */
		nextfield mapaket.m2aufkl

	case key5
		break

	case key17
		perform programm_ende

	case key12
	case key8	/* drucken */
/* #2453 A */
		if d2453sch = 1
		    if ((dpaufanz <> fracht.anz)  and (dpaufanz > 0 ))	
			let fracht.anz = dpaufanz
			display mafrae.m1anz
			refresh
		    end
		    if  dpgesanzsave <> fracht.anz
			let dpgesanzsave = fracht.anz
			let dret = proc deffrachtbrief(0)
			if dret = -250
			    perform disp_msg ( -250 1 )
			    perform rollbackwork
			    return
			end
			let fracht.anz = dpgesanzsave
			perform frachtbrief_speichern(0)
		    end
		end
/* #2453 E */
		if ((dpaufkl = "J") and (dpaufanz > 0))
				perform paket_drucken
				(fracht.mdn,fracht.fil,fracht.kun,
					fracht.dat,fracht.stat,dpaufanz)
				returning ( dret )
			break
		else
			continue current 
		end

	end	/* actions */
	perform set_com_file (mess_menue.prog_nr)
	perform set_hlp_file (mess_menue.prog_nr)
END

/* #1593 E */


/* #1757 A */

procedure  frachtbriefaktualisieren

/** neuerstellen der anzahl und des lief_gew */

field yaehler   smallint	/* zaehler, wievielte LS-Pos. */
field gesamtsum smallint	/* gesamtzahl verp.einh. */
field verpackeinh decimal (6,1)	/* rechenvariable fuer verp.einh */
field verpackeinh2 decimal (6,1) /* hilfsvariable fuer runden */
field a_kun_status smallint	/* merker, ob artikelnr in a_kun existiert */
	
/* --	let updatemerker = TRUE -- */
	let aktuell = TRUE
	let fracht.lief_term = dfredliefdat
/* #1990 A */
	if dalternative = 24
		let fracht.dat = proc read_cal_wt2 (dfredliefdat)
	else
/* #1990 E  */
		let fracht.dat = dfredliefdat
/* #1990 */ end


	init yaehler
	init gesamtsum

	init fracht.lief_gew
	
	close cursor fre_lsp_cursor
	fetch cursor fre_lsp_cursor
	while not sqlstatus
		close cursor fre_a_kun
		fetch cursor fre_a_kun
/* 1841 A */
		if sqlstatus
		    if kun.kun_bran2 > "0"
			close cursor fre_a_bran
			fetch cursor fre_a_bran
		    end
		end
/* 1841 E */
		if sqlstatus 
			let a_kun_status = FALSE
		else
			let a_kun_status = TRUE
		end
		if a_kun_status	/* a_kun-Satz vorhanden */
			if a_kun.inh = 0	/* div. durch 0 */
				let a_kun.inh = 1
			end
/* #1948 A */
			if dalternative = 24
			    if is null (lsp.leer_pos)
					let lsp.leer_pos = 0
			    end
			    let verpackeinh = lsp.leer_pos
			    let verpackeinh2 = verpackeinh
			else
/* #1948 E */
			    let verpackeinh =  (lsp.lief_me / a_kun.inh)
			    let verpackeinh2 = INT (lsp.lief_me / a_kun.inh)
/* #1948 */		end
			if  verpackeinh2 < verpackeinh
				let verpackeinh = verpackeinh2 + 1
			end
			let gesamtsum = gesamtsum + verpackeinh
			close cursor fre_a_bas
			fetch cursor fre_a_bas

			if a_bas.hnd_gew  = "G"
			    let fracht.lief_gew = fracht.lief_gew + lsp.lief_me
			else
			    let fracht.lief_gew = fracht.lief_gew + ROUND (
			     ( lsp.lief_me * a_bas.a_gew ) AT 3 )
			end
		else
			/* kunden-artikel nummer uebergehen,
			me_einh und verp.einh aus a_hndw,a_eig,a_eig_div */

			init table a_bas
			close cursor fre_a_bas
			fetch cursor fre_a_bas
			if a_bas.hnd_gew  = "G"
			    let fracht.lief_gew = fracht.lief_gew + lsp.lief_me
			else
			    let fracht.lief_gew = fracht.lief_gew + ROUND (
			     ( lsp.lief_me * a_bas.a_gew ) AT 3 )
			end
			switch a_bas.a_typ
			case 1
			   close cursor fre_a_hndw
			   fetch cursor fre_a_hndw
			   if not sqlstatus
			     if a_hndw.inh = 0	/* div. durch 0 */
				let a_hndw.inh = 1
			     end
			     let verpackeinh =  (lsp.lief_me / a_hndw.inh)
			     let verpackeinh2 = INT (lsp.lief_me / a_hndw.inh)
			   else
			     let verpackeinh =  (lsp.lief_me)
			     let verpackeinh2 = INT (lsp.lief_me)
			   end
			   break
			case 2
			  close cursor fre_a_eig
			  fetch cursor fre_a_eig
			  if not sqlstatus
			    if a_eig.inh = 0	/* div. durch 0 */
				let a_eig.inh = 1
			    end
			    let verpackeinh =  (lsp.lief_me / a_eig.inh)
			    let verpackeinh2 = INT (lsp.lief_me / a_eig.inh)
			  else
			    let verpackeinh =  (lsp.lief_me)
			    let verpackeinh2 = INT (lsp.lief_me)
			  end
			  break
			case 3
			  close cursor fre_a_eig_div
			  fetch cursor fre_a_eig_div
			  if not sqlstatus
			    if a_eig_div.inh = 0	/* div. durch 0 */
				let a_eig_div.inh = 1
			    end
			    let verpackeinh =  (lsp.lief_me / a_eig_div.inh)
			    let verpackeinh2 = INT (lsp.lief_me / a_eig_div.inh)
			  else
			    let verpackeinh =  (lsp.lief_me)
			    let verpackeinh2 = INT (lsp.lief_me)
			  end
			  break
			otherwise
			    let verpackeinh =  (lsp.lief_me)
			    let verpackeinh2 = INT (lsp.lief_me)
			  break
			end
/* #1948 A */
			if dalternative = 24
			    if is null (lsp.leer_pos)
					let lsp.leer_pos = 0
			    end
			    let verpackeinh = lsp.leer_pos
			    let verpackeinh2 = verpackeinh
			end
/* #1948 E */
			if  verpackeinh2 < verpackeinh
				let verpackeinh = verpackeinh2 + 1
			end
			let gesamtsum = gesamtsum + verpackeinh
		end
		let yaehler = yaehler + 1
		fetch cursor fre_lsp_cursor
	end	/* while not sqlstatus */
	let fracht.anz = gesamtsum
	display form mafrae
	refresh
end /* frachtbriefaktualisieren */

/* #1757 E */
