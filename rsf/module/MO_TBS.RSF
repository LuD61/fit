module mo_tbs
/*---------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_tbs
-
-	Interne Operatoren	:	siehe EXPORT
-
-	Externe Operatoren	:	-
-					
-	Autor			:	Bernard Menge
-	Erstellungsdatum	:	13.06.90
-	Modifikationsdatum	:	00.00.90
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1
-       #1143   1.400 17.11.92    GF  Anpassung an neue Items            
-       #1383   1.400 24.02.93    GF  * durch % in SQL ersetzen          
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:  
-	stellt Prozeduren fuer die Verwendung von Textbausteinen auf
-	Anwendungsebene zur Verfuegung. 
-	Fuer die Erfassung und Pflege steht ein selbstaendiges Pflegeprogramm
-	zur Verfuegung, welches Prozeduren aus diesem Modul verwendet.
-
-	anzahl_zeilen:
-	gibt an, wieviel Zeilen ein TBS unter Beruecksichtigung der vorgegebenen
-	Anzahl von Spalten/Zeichen aufweist.
-
-	txt_zeil_holen:
-	holt die angegebene Textzeile mit der angegebenen Anzahl von Spalten
-	des angegebenen TBS
-
-	txt_zeil_sichern:
-	sichert die angegebene Textzeile mit der angegebenen Anzahl von Spalten
-	des angegebenen TBS
-
-	freitext_editieren:
-	stellt ein Window einstellbarer Groesse und Position zur Verfuegung,
-	in dem der angegebene TBS erfasst und gepflegt werden kann.
-
-	ref_pflegen:
-	setzt den Referenzzaehler um den uebergebenen Wert herauf bzw.
-	herab. Es koennen nur TBS mit Zaehlerstand 0 geloescht werden.
-	
-
----------------------------------------------------------------------------*/

/*----- Export Section -----------------------------------------------------*/

export 
	proc anzahl_zeilen
	proc txt_zeil_holen
	proc txt_zeil_sichern
	proc freitext_editieren
/*	proc ref_pflegen
*/
	form text_edit
		field dzeile1	
		field dzeile2	
		field dzeile3	
		field dzeile4	
		field dzeile5	
		field dzeile6	
		field dzeile7	
		field dzeile8	
		field dzeile9	
		field dzeile10
		field dzeile11
		field dzeile12
		field dzeile13
		field dzeile14
		field dzeile15
		field dzeile16
		field dzeile17

end

/*----- Import Section -----------------------------------------------------*/

import module mo_cons
end

import module mo_meld
	proc disp_msg
end

import module mo_ma_ut
	proc disp_fkt
	proc save_fkt
	proc restore_fkt
end

/*----- Lokale Deklarationen Modul -----------------------------------------*/

database bws
	table tbs_kopf end
	table tbs_elem end
end

field dfehler	smallint
field dzeil_ges	smallint
field dstat01	smallint
field dstat02	smallint
field dstat03	smallint
field ddummy	smallint
field dindex	smallint
field dzeilen_zaehler	smallint	/* aktuelle Zeile des TBS, */
					/* nicht des Windows       */


field dtext_zeile	char(80)

field dzeilen	char(80)	dimension(17)	/* Matrix fuer Textzeilen  */
						/* des Windows             */

field dzeile1	char(80)		/* Textzeilen des Windows */
field dzeile2	char(80)
field dzeile3	char(80)
field dzeile4	char(80)
field dzeile5	char(80)
field dzeile6	char(80)
field dzeile7	char(80)
field dzeile8	char(80)
field dzeile9	char(80)
field dzeile10	char(80)
field dzeile11	char(80)
field dzeile12	char(80)
field dzeile13	char(80)
field dzeile14	char(80)
field dzeile15	char(80)
field dzeile16	char(80)
field dzeile17	char(80)

form text_edit
	field	dfeld1	use dzeile1  pos( 0 0)
	field	dfeld2	use dzeile2  pos( 1 0)
	field	dfeld3	use dzeile3  pos( 2 0)
	field	dfeld4	use dzeile4  pos( 3 0)
	field	dfeld5	use dzeile5  pos( 4 0)
	field	dfeld6	use dzeile6  pos( 5 0)
	field	dfeld7	use dzeile7  pos( 6 0)
	field	dfeld8	use dzeile8  pos( 7 0)
	field	dfeld9	use dzeile9  pos( 8 0)
	field	dfeld10	use dzeile10 pos( 9 0)
	field	dfeld11	use dzeile11 pos(10 0)
	field	dfeld12	use dzeile12 pos(11 0)
	field	dfeld13	use dzeile13 pos(12 0)
	field	dfeld14	use dzeile14 pos(13 0)
	field	dfeld15	use dzeile15 pos(14 0)
	field	dfeld16	use dzeile16 pos(15 0)
	field	dfeld17	use dzeile17 pos(16 0)
end
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/*----- P R O C E D U R E N -----------------------------------------------*/
/*-------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
-									  
-	Procedure	: 	anzahl_zeilen
-
-	In		:	dtbs		integer  Textbaustein
-				dsp_anz		smallint Anzahl Spalten/Zeichen
-
-	Out		:	dzeil_anz	smallint Azahl Zeilen
-				dstatus		smallint 0 = i.O.
-							 sonst Fehler
-			  
-	Beschreibung    :
-	gibt an, wieviele Zeilen fuer die Darstellung des TBS bei der
-	vorgegebenen Spaltenanzahl benoetigt.
-
-	!!!
-	Aenderungen in dieser Prozedur muessen evtl. in der fast identischen 
-	Prozedur TXT_ZEIL_HOLEN nachgezogen werden.
-	siehe auch TXT_ZEIL_SICHERN
-	!!!
---------------------------------------------------------------------------*/

PROCEDURE anzahl_zeilen

/*--- Parameter-Deklaration ---*/

parameter
	field dtbs	like tbs_kopf.tbs	/* Textbaustein */
	field dsp_anz	smallint		/* Anzahl Spalten (Zeichen) */
						/* je Zeile                 */
end

/*--- lokale Deklarationen ---*/

field dstatus		smallint	/* hat Element holen geklappt ? */
field delem_nr		like tbs_elem.tbs_elem_nr
					/* aktuelles Textelement        */
field dze_akt		smallint	/* aktuelle Zeile               */
field dtext_element	like tbs_elem.tbs_elem_txt
					/* aktuelles Textelement aus DB */
field dbuffer		char(80)	/* Buffer fuer Umsetzung        */
					/* TBS-Elemente in Ausgabezeile */

/*--- Start Programm ---*/
if "" = "@(#) mo_tbs.rsf	1.400	24.02.93	by GF" end
let dstatus    = 0
let delem_nr   = 1
let dze_akt    = 0
let dbuffer    = ""

/*--- Test, ob Uebergabeparameter in Ordnung ---*/
if dsp_anz < 1
	let dstatus = -33
	return (dze_akt, dstatus)
end /* if */

perform tbs_elem_holen (dtbs, delem_nr) returning (dtext_element, dstatus)
if dstatus <> 0 
	return (dze_akt, dstatus)
end /* if */

while not sqlstatus
perform tbs_elem_holen (dtbs, delem_nr) returning (dtext_element, dstatus)
if dstatus = 100 /* letzter Satz erreicht */
        let dstatus = 0 /* weil mind. 1 Satz gefunden */
        return (dze_akt, dstatus)
end
if dstatus <> 0 
	return (dze_akt, dstatus)
end /* if */
let dze_akt = dze_akt + 1
let delem_nr = delem_nr + 1
end /* of while */

/*--- formale Deklaration Prozedurende ---*/
let dstatus = 0
return (dze_akt, dstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	:	txt_zeil_holen 
-
-	In		:	dtbs	integer		Textbaustein
-				dze_nr	smallint	Zeilennummer
-				dsp_anz	smallint	Anzahl Spalten/Zeichen
-
-	Out		:	dbuffer	char(n)		Textzeile
-				dstatus	smallint	0 = i.O.
-							sonst Fehler
-			  
-	Beschreibung    :
-	TBS werden in der Datenbank als eine Anzahl von Textelementen
-	gespeichert. Fuer die Anwendung werden diese Elemente in eine
-	Anzahl von Ausgabezeilen umgebaut. Die Anwendung gibt die Anzahl
-	der benoetigten Spalten (Zeichen) je Zeile vor. Es werden
-	Wortumbrueche vorgenommen und Line-Feeds beachtet. Die Anwendung
-	muss den Textbaustein zeilenweise unter Angabe der jeweiligen
-	Zeilennummer holen. Die Prozedur ANZAHL_ZEILEN liefert die Anzahl
-	Zeilen eines TBS fuer eine vorzugebende Anzahl Spalten (Zeichen)
-	je Zeile.
-
-	!!!
-	Aenderungen in dieser Prozedur muessen evtl. in der fast identischen 
-	Prozedur ANZAHL_ZEILEN nachgezogen werden.
-	siehe auch TXT_ZEIL_SICHERN
-	!!!
---------------------------------------------------------------------------*/

procedure txt_zeil_holen

/*--- Parameter-Deklaration ---*/

parameter
	field dtbs	like tbs_kopf.tbs	/* Textbaustein */
	field dze_nr	smallint		/* Zeilennummer */
	field dsp_anz	smallint		/* Anzahl Spalten (Zeichen) */
						/* je Zeile                 */
end

/*--- lokale Deklarationen ---*/

field dstatus		smallint	/* hat Element holen geklappt ? */
field delem_nr		like tbs_elem.tbs_elem_nr
					/* Nr. aktuelles Textelement    */
field dze_akt		smallint	/* aktuelle Zeile               */

field dtext_element	like tbs_elem.tbs_elem_txt
					/* aktuelles Textelement aus DB */
field dbuffer		char(80)	/* Buffer fuer Umsetzung        */
					/* TBS-Elemente in Ausgabezeile */

/*--- Start Programm ---*/
let delem_nr   = 1
let dze_akt    = 1
let dbuffer    = ""

/*--- Test, ob Uebergabeparameter in Ordnung ---*/
if dsp_anz < 1
	let dstatus = -33
	return (dze_akt, dstatus)
end /* if */

perform tbs_elem_holen (dtbs, dze_nr) returning (dtext_element, dstatus)
if dstatus <> CFALSE
	let dbuffer = ""
	return (dbuffer, dstatus)
end /* if */
let dbuffer = dtext_element
return (dbuffer, dstatus)

end /* of procedure */

/*---------------------------------------------------------------------------
-									  
-	Procedure	:	txt_zeil_sichern 
-
-	In		:	dtbs	integer		Textbaustein
-				dze_nr	smallint	Zeilennummer
-				dsp_anz	smallint	Anzahl Spalten/Zeichen
-				dzeil_inh char(80)	Zeilentext
-
-	Out		:	dstatus	smallint	0 = i.O.
-							sonst Fehler
-			  
-	Beschreibung    :
-	Sichert die angegebene Textzeile. Es werden alle betroffenen TBS-
-	Elemente bezueglich der sie betreffenden Teilstuecke aktualisiert.
-
-	!!!
-	siehe auch TXT_ZEIL_HOLEN
-	siehe auch ANZAHL_ZEILEN
-	!!!
---------------------------------------------------------------------------*/

procedure txt_zeil_sichern

/*--- Parameter-Deklaration ---*/

parameter
	field dtbs	like tbs_kopf.tbs	/* Textbaustein */
	field dze_nr	smallint		/* Zeilennummer */
	field dsp_anz	smallint		/* Anzahl Spalten (Zeichen) */
						/* je Zeile                 */
	field dzeil_inh	char(80)		/* Zeilentext   */
end

/*--- lokale Deklarationen ---*/

field dstatus		smallint	/* hat Element holen geklappt ? */
field delem_nr		like tbs_elem.tbs_elem_nr
					/* aktuelles Textelement        */
field dze_akt		smallint	/* aktuelle Zeile               */
field dindex_te		smallint	/* Laufindex Tabellenelement    */
field dindex_bu		smallint	/* Laufindex Buffer             */
field dumbruch		smallint	/* Position des Wortumbruchs    */
field dtest		smallint	/* Puffer fuer Konv. ASCII(255)  */
field ddummy		smallint        /* Rosi-Scheiss                 */

field dtext_element	like tbs_elem.tbs_elem_txt
					/* aktuelles Textelement aus DB */
field dbuffer		char(80)	/* Buffer fuer Umsetzung        */
					/* TBS-Elemente in Ausgabezeile */
field drest_sichern	char(80)	/* Wortrest sichern bei Zeilen- */
					/* umbruch                      */

/*--- Start Programm ---*/
let delem_nr   = 1
let dze_akt    = 1
let dindex_bu  = 1
let dindex_te  = 1
let dbuffer    = ""

/*--- Test, ob Uebergabeparameter in Ordnung ---*/
if dsp_anz < 1
	let dstatus = -33
	return (dstatus)
end /* if */

/*--- 1. Textelement holen ---*/
perform tbs_elem_holen (dtbs, dze_nr) returning (dtext_element, dstatus)

if dstatus <> CFALSE and dstatus <> 100
	return (dstatus)
end /* if */
let dstatus = 0
let dtext_element = dzeil_inh
perform tbs_elem_sichern (dtbs, dze_nr, 
			   dtext_element)
		   returning (dstatus)
return (dstatus)

end /* of procedure */

/*---------------------------------------------------------------------------
-									  
-	Procedure	: freitext_editieren
-
-	In		: dtbs		integer   TBS
-			  doffs_zeil	smallint  Window Offset Zeile
-			  doffs_sp	smallint  Window Offset Spalte
-			  danz_zeil	smallint  Window Anzahl Zeilen
-			  danz_sp	smallint  Window Anzahl Spalten
-			  dtxtmod       smallint  Anzeigen   = 1
-						  Bearbeiten = 2
-
-	Out		:
-			  
-	Beschreibung    :
-
---------------------------------------------------------------------------*/

procedure freitext_editieren

/*--- Parameter-Deklaration ---*/

parameter
	field	dtbs		integer
	field	doffs_zeil	smallint
	field	doffs_sp	smallint
	field	danz_zeil	smallint
	field	danz_sp		smallint
	field	dtxtmod		smallint
end

/*--- lokale Deklarationen ---*/

field	dfenster_zaehler	smallint	/* gibt die akt. Zeile im */
						/* Fenster an             */
field	dlastcolpos		smallint	/* sichert lastcolpos     */
field	dteilstring		char(80)	/* fuer Einfuegen 	  */

/*--- Start Programm ---*/
/*--- Test, ob Uebergabeparameter in Ordnung ---*/
/*--- max. 17 Zeilen und 80 Spalten          ---*/
if danz_zeil  > 18           or
   danz_sp    > 80           or
   doffs_zeil > 18-danz_zeil or
   doffs_sp   > 80-danz_sp
	/*--- Fehler: saubere Positionierung im ---*/
	/*--- Verarbeitungsteil nicht moeglich  ---*/
	let dfehler = 100 /* Dummy, entspr. Nummer muss noch vergeben werden */
	perform disp_msg (dfehler, 1)
end /* if */

/*--- nicht benoetigte Maskenfelder removen und Feldlaenge setzen ---*/
perform maske_setzen (danz_zeil, danz_sp)

init form text_edit
display pos(doffs_zeil doffs_sp) form text_edit

/*---Bearbeitung entspr. Modus ---*/
switch dtxtmod
	case 1
		/*--- Anzeigen ---*/
		/*--- Der Textbeginn wird, soweit das Window dies      ---*/
		/*--- erlaubt, angezeigt. Der Text kann nicht komplett ---*/
		/*--- angesehen werden. Dazu Bearbeiten waehlen        ---*/

		/*--- Anzahl Zeilen feststellen ---*/
		perform anzahl_zeilen (dtbs, danz_sp)
		   returning (dzeil_ges, dstat01)

		if dstat01 = CFALSE
			/*--- Zeilen mit Text fuellen ---*/
			for dindex = 1 to dzeil_ges
				perform txt_zeil_holen (dtbs, dindex, danz_sp)
				   returning (dtext_zeile, dstat02)

				if dstat02 = CFALSE
					perform maske_fuellen
				else
					/*--- Fehler bei Lesen TBS ---*/
					let dfehler = 100
/* bmj Dummy, entspr. Nummer muss noch vergeben werden */
					perform disp_msg (dfehler, 1)
				end /* if */
			end /* of for */

		else
			if dstat01 <> 100
				/*--- Fehler bei Lesen TBS ---*/
				let dfehler = 100
/* bmj Dummy, entspr. Nummer muss noch vergeben werden */
				perform disp_msg (dfehler, 1)
			end /* if */

			/*--- kein TBS-Element vorhanden ---*/
			init form text_edit

		end /* if */

		/*--- Maske ausgeben ---*/
		display pos(doffs_zeil, doffs_sp) form text_edit
		break

	case 2
		/*--- Bearbeiten incl. Einfuegen ---*/

		/*--- Funktionstastenbelegung sichern u. neue ausgeben ---*/
		perform save_fkt
		select window wiverb
		perform disp_fkt (6,3)
		select window wiverb
		perform disp_fkt (7,23)
		select window wiverb
		perform disp_fkt (8,40)
		select window wiverb
		perform disp_fkt (9,23)
		select window wiverb
		perform disp_fkt (10,42)
		select window wiverb

		/*--- Anzahl Zeilen feststellen ---*/
		perform anzahl_zeilen (dtbs, danz_sp)
		   returning (dzeil_ges, dstat01)

		/*--- TBS anzeigen ---*/
		if dstat01 = CFALSE
			/*--- Zeilen mit Text fuellen ---*/
			for dindex = 1 to dzeil_ges
				perform txt_zeil_holen (dtbs, dindex, danz_sp)
				   returning (dtext_zeile, dstat02)

				if dstat02 = CFALSE
					perform maske_fuellen
				else
					/*--- Fehler bei Lesen TBS ---*/
					let dfehler = 100
/* bmj Dummy, entspr. Nummer muss noch vergeben werden */
					perform disp_msg (dfehler, 1)
				end /* if */
			end /* of for */

		else
			if dstat01 <> 100
				/*--- Fehler bei Lesen TBS ---*/
				let dfehler = 100
/* bmj Dummy, entspr. Nummer muss noch vergeben werden */
				perform disp_msg (dfehler, 1)
			end /* if */

			/*--- kein TBS-Element vorhanden ---*/
			init form text_edit

		end /* if */

		/*--- Maske ausgeben ---*/
		display pos(doffs_zeil, doffs_sp) form text_edit


		let dzeilen_zaehler = 1
		let dfenster_zaehler = 1

		if dzeil_ges = 0
			let dzeil_ges = 1
		end

		enter pos(doffs_zeil, doffs_sp) form text_edit
		actions

		case KEY

			switch SYSKEY

				case 13056 /*-- =KEYCR ROSI-Fehler --*/
					if dzeilen_zaehler >= dzeil_ges
						let dzeil_ges = dzeil_ges + 1
					end
					if dfenster_zaehler >= danz_zeil
					/*-- um eine Zeile scrollen --*/
					else
						let dfenster_zaehler = 
							dfenster_zaehler + 1
					end
					perform zeile_in_matrix

					/*--- ascii(255) in Var. einbauen ---*/
					putchar (dzeilen[dzeilen_zaehler] 
						lastcolpos+1 255)
					let dzeilen[dzeilen_zaehler] = 
						dzeilen[dzeilen_zaehler]
						[1 lastcolpos+1]
					perform zeile_aus_matrix
					display pos(doffs_zeil doffs_sp) 
								form text_edit
					perform txt_zeil_sichern 
					       (dtbs, dzeilen_zaehler, 
					       danz_sp,
					       dzeilen[dzeilen_zaehler])
				   		returning (dstat02)
					if dstat02
						perform disp_msg(CENBED017,1)
					end	
					let dzeilen_zaehler = 
							dzeilen_zaehler + 1
					break

				case KEYDOWN
					perform zeile_in_matrix
					perform txt_zeil_sichern 
					       (dtbs, dzeilen_zaehler, 
					       danz_sp,
					       dzeilen[dzeilen_zaehler])
				   		returning (dstat02)
					if dstat02
						perform disp_msg(CENBED017,1)
					end	
					if dzeilen_zaehler >= dzeil_ges
					/*-- rausgehen in Kopf (Rundlauf) --*/
						continue (KEY6)
					end
					if dfenster_zaehler >= danz_zeil
					/*-- um eine Zeile scrollen --*/
					else
						let dfenster_zaehler = 
							dfenster_zaehler + 1
					end
					let dzeilen_zaehler = 
							dzeilen_zaehler + 1
					perform zeile_in_matrix
					if strlen(dzeilen[dzeilen_zaehler])
								< lastcolpos + 1
					       let lastcolpos = 
						strlen(dzeilen[dzeilen_zaehler])
					end
					break
		
				case KEYUP
					if dzeilen_zaehler = 1
					/*-- rausgehen in Kopf --*/
						continue (KEY6)
					end
					if dfenster_zaehler = 1
					/*-- um eine Zeile scrollen --*/
					else
						let dfenster_zaehler = 
							dfenster_zaehler - 1
					end
					perform zeile_in_matrix
					perform txt_zeil_sichern 
					       (dtbs, dzeilen_zaehler, 
					       danz_sp,
					       dzeilen[dzeilen_zaehler])
				   		returning (dstat02)
					if dstat02
						perform disp_msg(CENBED017,1)
					end	
					let dzeilen_zaehler = 
							dzeilen_zaehler - 1
					perform zeile_in_matrix
					if strlen(dzeilen[dzeilen_zaehler])
								< lastcolpos + 1
					       let lastcolpos = 
						strlen(dzeilen[dzeilen_zaehler])
					end
					break

				case KEYLEFT
					let colpos = lastcolpos
					if colpos = 0
						if dzeilen_zaehler = 1
							let lastcolpos = 1
							break
						end
						if dfenster_zaehler = 1
						/*-- um eine Zeile scrollen --*/
						else
							let dfenster_zaehler = 
							    dfenster_zaehler - 1
						end
						let dzeilen_zaehler = 
							dzeilen_zaehler - 1
						perform zeile_in_matrix
						let lastcolpos = 
						strlen(dzeilen[dzeilen_zaehler])
						let colpos = lastcolpos
						continue (KEYUP)
					end
					break

				case KEYRIGHT
					let colpos = lastcolpos
					perform zeile_in_matrix
					if colpos = strlen
						  (dzeilen[dzeilen_zaehler])
						if dzeilen_zaehler >= dzeil_ges
						      let lastcolpos = strlen
						      (dzeilen[dzeilen_zaehler])
						      - 1
						      break
						end
						if dfenster_zaehler >= danz_zeil
						/*-- um eine Zeile scrollen --*/
						else
							let dfenster_zaehler = 
							    dfenster_zaehler + 1
						end
						let dzeilen_zaehler = 
							dzeilen_zaehler + 1
						let lastcolpos = 0
						let colpos = lastcolpos
						continue (KEYDOWN)
					end
					break

				case KEY7
					/*-- Links-Tab --*/
					let dlastcolpos = lastcolpos
					while CTRUE
					    let lastcolpos = lastcolpos - 1
					    if lastcolpos % 8 <= 0
					        if lastcolpos > 0 
							break
					        end
						if dlastcolpos <> 0
							break
						end
						if dzeilen_zaehler = 1
							break
						end
						if dfenster_zaehler = 1
						/*-- um eine Zeile scrollen --*/
						else
							let dfenster_zaehler = 
							    dfenster_zaehler - 1
						end
						perform zeile_in_matrix
						perform txt_zeil_sichern 
						       (dtbs, dzeilen_zaehler, 
						       danz_sp,
						       dzeilen[dzeilen_zaehler])
							returning (dstat02)
						if dstat02
							perform disp_msg
								(CENBED017,1)
						end	
						let dzeilen_zaehler = 
							dzeilen_zaehler - 1
						let lastcolpos = danz_sp
						while lastcolpos % 8 <> 0
						    let lastcolpos = 
								lastcolpos - 1
						    if lastcolpos = 0
							break
						    end
						end
						let colpos = lastcolpos
						continue (KEYUP)
					    end
					end
					let colpos = lastcolpos
					continue (KEYUP)

				case KEYTAB
				case KEY8
					/*-- Rechts-Tab --*/
					while CTRUE
					    let lastcolpos = lastcolpos + 1
					    if lastcolpos % 8 = 0
						break
					    end
					    if lastcolpos >= danz_sp
						if dzeilen_zaehler >= dzeil_ges
						      break
						end
						if dfenster_zaehler >= danz_zeil
						/*-- um eine Zeile scrollen --*/
						else
							let dfenster_zaehler = 
							    dfenster_zaehler + 1
						end
						perform zeile_in_matrix
						perform txt_zeil_sichern 
						       (dtbs, dzeilen_zaehler, 
						       danz_sp,
						       dzeilen[dzeilen_zaehler])
							returning (dstat02)
						if dstat02
							perform disp_msg
								(CENBED017,1)
						end	
						let dzeilen_zaehler = 
							dzeilen_zaehler + 1
						let lastcolpos = 0
						let colpos = lastcolpos
						continue (KEYDOWN)
					    end
					end
					let colpos = lastcolpos
					continue

				case KEYINS
				case KEY10
					/*-- Einfuegen --*/
					init dteilstring
					perform zeile_in_matrix
					getstr (dzeilen[dzeilen_zaehler]
				             dteilstring lastcolpos+1
				             strlen (dzeilen[dzeilen_zaehler]))
					putstr (dzeilen[dzeilen_zaehler]
					     " " lastcolpos+1 1)
					putstr (dzeilen[dzeilen_zaehler]
					     dteilstring lastcolpos+2 0)
					perform zeile_aus_matrix
					display pos(doffs_zeil doffs_sp) 
								form text_edit
					break

				case KEYDEL
				case KEY9
					/*-- Loeschen --*/
					init dteilstring
					perform zeile_in_matrix
				        if lastcolpos >= strlen 
						     (dzeilen[dzeilen_zaehler])
						break
					end
					if lastcolpos = 0
						getstr (dzeilen[dzeilen_zaehler]
					             dteilstring 2 strlen
						     (dzeilen[dzeilen_zaehler]))
						let dzeilen[dzeilen_zaehler] = 
						     clipped(dteilstring)
					else
						getstr (dzeilen[dzeilen_zaehler]
				             	     dteilstring lastcolpos+2
				                     strlen 
						     (dzeilen[dzeilen_zaehler]))
						putstr (dzeilen[dzeilen_zaehler]
					     	     dteilstring lastcolpos+1 0)
						putstr (dzeilen[dzeilen_zaehler]
					     	     " " strlen 
						     (dzeilen[dzeilen_zaehler])
						     1)
					end
					perform zeile_aus_matrix
					display pos(doffs_zeil doffs_sp) 
								form text_edit
					break

			end
			let colpos = lastcolpos
			continue (SYSKEY)

		case KEY5
			/*-- Abbruch --*/
			break

		case KEY6
		case KEY12
			/*-- zurueck in den Kopf --*/
			perform zeile_in_matrix
			perform txt_zeil_sichern 
			       (dtbs, dzeilen_zaehler, 
			       danz_sp,
			       dzeilen[dzeilen_zaehler])
				returning (dstat02)
			if dstat02
				perform disp_msg(CENBED017,1)
			end
			init colpos
			break

	end /* enter form */

end /* of switch */

/*--- Funktionstastenbelegung zuruecksetzen ---*/
perform restore_fkt
select window wiverb

return (SYSKEY)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: maske_setzen
-
-	In		: danz_zeil	smallint	Anzahl Zeilen
-			  danz_sp	smallint	Anzahl Spalten je Zeile
-
-	Out		: -
-			  
-	Beschreibung    :
-
---------------------------------------------------------------------------*/

PROCEDURE maske_setzen

/*--- Parameter-Deklaration ---*/

parameter
	field	danz_zeil	smallint
	field	danz_sp		smallint
end

/*--- Felder auf richtige Laenge bringen ---*/
switch danz_zeil

	case 17
		set form field dfeld17 length (danz_sp)
	case 16
		set form field dfeld16 length (danz_sp)
	case 15
		set form field dfeld15 length (danz_sp)
	case 14
		set form field dfeld14 length (danz_sp)
	case 13
		set form field dfeld13 length (danz_sp)
	case 12
		set form field dfeld12 length (danz_sp)
	case 11
		set form field dfeld11 length (danz_sp)
	case 10
		set form field dfeld10 length (danz_sp)
	case 9
		set form field dfeld9  length (danz_sp)
	case 8
		set form field dfeld8  length (danz_sp)
	case 7
		set form field dfeld7  length (danz_sp)
	case 6
		set form field dfeld6  length (danz_sp)
	case 5
		set form field dfeld5  length (danz_sp)
	case 4
		set form field dfeld4  length (danz_sp)
	case 3
		set form field dfeld3  length (danz_sp)
	case 2
		set form field dfeld2  length (danz_sp)
	case 1
		set form field dfeld1  length (danz_sp)
		break
end /* of switch */

switch danz_zeil
	case 1
		set form field dfeld2 removed (on)
	case 2
		set form field dfeld3 removed (on)
	case 3
		set form field dfeld4 removed (on)
	case 4
		set form field dfeld5 removed (on)
	case 5
		set form field dfeld6 removed (on)
	case 6
		set form field dfeld7 removed (on)
	case 7
		set form field dfeld8 removed (on)
	case 8
		set form field dfeld9 removed (on)
	case 9
		set form field dfeld10 removed (on)
	case 10
		set form field dfeld11 removed (on)
	case 11
		set form field dfeld12 removed (on)
	case 12
		set form field dfeld13 removed (on)
	case 13
		set form field dfeld14 removed (on)
	case 14
		set form field dfeld15 removed (on)
	case 15
		set form field dfeld16 removed (on)
	case 16
		set form field dfeld17 removed (on)
		break
end /* of switch */

return

END /* of procedure */

/*---------------------------------------------------------------------------
-									  
-	Procedure	: maske_fuellen
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    :
-
---------------------------------------------------------------------------*/

PROCEDURE maske_fuellen

/*--- Text der Zeile zuordnen ---*/
switch dindex
	case 1
		let dzeile1 = dtext_zeile
		break
	case 2
		let dzeile2 = dtext_zeile
		break
	case 3
		let dzeile3 = dtext_zeile
		break
	case 4
		let dzeile4 = dtext_zeile
		break
	case 5
		let dzeile5 = dtext_zeile
		break
	case 6
		let dzeile6 = dtext_zeile
		break
	case 7
		let dzeile7 = dtext_zeile
		break
	case 8
		let dzeile8 = dtext_zeile
		break
	case 9
		let dzeile9 = dtext_zeile
		break
	case 10
		let dzeile10 = dtext_zeile
		break
	case 11
		let dzeile11 = dtext_zeile
		break
	case 12
		let dzeile12 = dtext_zeile
		break
	case 13
		let dzeile13 = dtext_zeile
		break
	case 14
		let dzeile14 = dtext_zeile
		break
	case 15
		let dzeile15 = dtext_zeile
		break
	case 16
		let dzeile16 = dtext_zeile
		break
	case 17
		let dzeile17 = dtext_zeile
		break
end /* of switch */

END /* of procedure */

/*---------------------------------------------------------------------------
-									  
-	Procedure	: tbs_elem_holen
-
-	In		: dtbs		integer		TBS
-			  delem_nr	smallint	TBS-Element-Nr.
-
-	Out		: TBS-Element
-			  Status
-			  
-	Beschreibung    : holt ein angegebenes Textelement
-
---------------------------------------------------------------------------*/

PROCEDURE tbs_elem_holen

/*--- Parameter-Deklaration ---*/

parameter
	field dtbs	integer
	field delem_nr	smallint
end

/*--- TE lesen ---*/
/* A 1383 */
execute from sql
	select #tbs_elem.% from tbs_elem
	   where tbs_elem.tbs         = $dtbs
	   and   tbs_elem.tbs_elem_nr = $delem_nr
end
/* E 1383 */

/*--- Satz gibt es nicht ---*/
if sqlstatus = 100
	let tbs_elem.tbs_elem_txt = ""
end /* if */

return (tbs_elem.tbs_elem_txt, sqlstatus)

END /* of procedure */

/*---------------------------------------------------------------------------
-									  
-	Procedure	: tbs_elem_sichern
-
-	In		: dtbs		integer		TBS
-			  delem_nr	smallint	TBS-Element
-			  dtext_element	char(80)	Text
-
-	Out		: Status
-			  
-	Beschreibung    :
-
---------------------------------------------------------------------------*/

PROCEDURE tbs_elem_sichern

/*--- Parameter-Deklaration ---*/

parameter
	field dtbs		integer
	field delem_nr		integer
	field dtext_element	char(80)
end

/*--- lokale Deklarationen ---*/

field dbuffer	char(80)
field dstatus	smallint

/*--- Pruefen ob Satz schon vorhanden ---*/
perform tbs_elem_holen (dtbs, delem_nr) returning (dbuffer, dstatus)

switch dstatus
	case 0
		/*--- Satz aktualisieren ---*/
		execute from sql
			select #tbs_elem.% from tbs_elem
			   where tbs_elem.tbs         = $dtbs
			   and   tbs_elem.tbs_elem_nr = $delem_nr
			   for update
		end
		
		execute from sql
			update tbs_elem
			   set  tbs_elem.tbs_elem_txt = $dtext_element
			   where tbs_elem.tbs         = $dtbs
			   and   tbs_elem.tbs_elem_nr = $delem_nr
		end

		break

	case 100
		/*--- Satz einfuegen ---*/
/* A 1383 */
		execute from sql
			insert into tbs_elem (%tbs_elem.%)
			values ($dtbs, $delem_nr, $dtext_element, $tbs_elem.delstatus)
		end
/* E 1383 */
		break

end /* switch */

return (sqlstatus)

END /* of procedure */

/*---------------------------------------------------------------------------
-									  
-	Procedure	: zeile_in_matrix
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure schreibt den Inhalt der entsprechenden
-			  Variablen dzeile? in die Matrix dzeilen[?] in
-			  Abhaengigkeit von der aktuellen Zeilenposition
-			  (dzeilen_zaehler).
-
---------------------------------------------------------------------------*/

PROCEDURE zeile_in_matrix

	switch dzeilen_zaehler
		case 1
			let dzeilen[1] = dzeile1
			break
		case 2
			let dzeilen[2] = dzeile2
			break
		case 3
			let dzeilen[3] = dzeile3
			break
		case 4
			let dzeilen[4] = dzeile4
			break
		case 5
			let dzeilen[5] = dzeile5
			break
		case 6
			let dzeilen[6] = dzeile6
			break
		case 7
			let dzeilen[7] = dzeile7
			break
		case 8
			let dzeilen[8] = dzeile8
			break
		case 9
			let dzeilen[9] = dzeile9
			break
		case 10
			let dzeilen[10] = dzeile10
			break
		case 11
			let dzeilen[11] = dzeile11
			break
		case 12
			let dzeilen[12] = dzeile12
			break
		case 13
			let dzeilen[13] = dzeile13
			break
		case 14
			let dzeilen[14] = dzeile14
			break
		case 15
			let dzeilen[15] = dzeile15
			break
		case 16
			let dzeilen[16] = dzeile16
			break
		case 17
			let dzeilen[17] = dzeile17
			break
	end
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: zeile_aus_matrix
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure schreibt den Inhalt der Matrix 
-			  dzeilen[?] in die entsprechende Variable dzeile? in
-			  Abhaengigkeit von der aktuellen Zeilenposition
-			  (dzeilen_zaehler).
-
---------------------------------------------------------------------------*/

PROCEDURE zeile_aus_matrix

	switch dzeilen_zaehler
		case 1
			let dzeile1 = dzeilen[1]
			break
		case 2
			let dzeile2 = dzeilen[2]
			break
		case 3
			let dzeile3 = dzeilen[3]
			break
		case 4
			let dzeile4 = dzeilen[4]
			break
		case 5
			let dzeile5 = dzeilen[5]
			break
		case 6
			let dzeile6 = dzeilen[6]
			break
		case 7
			let dzeile7 = dzeilen[7]
			break
		case 8
			let dzeile8 = dzeilen[8]
			break
		case 9
			let dzeile9 = dzeilen[9]
			break
		case 10
			let dzeile10 = dzeilen[10]
			break
		case 11
			let dzeile11 = dzeilen[11]
			break
		case 12
			let dzeile12 = dzeilen[12]
			break
		case 13
			let dzeile13 = dzeilen[13]
			break
		case 14
			let dzeile14 = dzeilen[14]
			break
		case 15
			let dzeile15 = dzeilen[15]
			break
		case 16
			let dzeile16 = dzeilen[16]
			break
		case 17
			let dzeile17 = dzeilen[17]
			break
	end
END
