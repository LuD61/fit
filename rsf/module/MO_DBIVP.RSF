/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbivpr
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	30.05.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    30.05.90   Simmchen	Modularisierung
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle iv_pr
-
-
------------------------------------------------------------------------------
*/

module mo_dbivp

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws
	proc prepscrollpos_ivpr
	proc prepscrollpos_ivpr_gue
	proc prepscrollpos_ivpr_a
	proc prepscrollpos_ivpr_a_ivprgr
	proc prepupdatepos_ivpr
	proc fetchscrollpos_ivpr
	proc recupdatepos_ivpr
	proc recdeletepos_ivpr
	proc recinsertpos_ivpr
	proc fetchupdatepos_ivpr
	proc closeupdatepos_ivpr
	proc closescrollpos_ivpr
	proc recreadpos_ivpr
	proc recreaddefpos_ivpr
	proc inittablepos_ivpr
	proc position_einfuegen_ivpr
	proc position_loeschen_ivpr
	proc positionen_loeschen_ivpr
	proc positionen_loeschen_ivpr_a
	proc positionen_loeschen_ivpr_a_ivprgr
end

/*----- Import Section ------------------------------------------------------*/

import module mo_cons
end

database bws
	table iv_pr end
end

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_ivpr
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_ivpr

	prepare scroll cursor cusmain2 from
		sql select #iv_pr.% from iv_pr
		where @iv_pr.mdn and
		      @iv_pr.pr_gr_stuf and
		      @iv_pr.kun_pr end
		sql order by iv_pr.a end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_ivpr_gue
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_ivpr_gue

	prepare scroll cursor cusmain2 from
		sql select #iv_pr.% from iv_pr
		where @iv_pr.mdn and
		      @iv_pr.pr_gr_stuf and
		      @iv_pr.kun_pr and
		      @iv_pr.gue_ab end
		sql order by iv_pr.a end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_ivpr_a
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_ivpr_a

	prepare scroll cursor cusmain2 from
		sql select #iv_pr.% from iv_pr
		where @iv_pr.mdn and
		      @iv_pr.a and
		      iv_pr.pr_gr_stuf <> 0 and
		      iv_pr.kun_pr <> 0 end
		sql order by iv_pr.kun_pr end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_ivpr_a_ivprgr
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_ivpr_a_ivprgr

	prepare scroll cursor cusmain2 from
		sql select #iv_pr.% from iv_pr
		where @iv_pr.mdn and
		      @iv_pr.a and
		      iv_pr.kun_pr = 0 end
		sql order by iv_pr.pr_gr_stuf end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdatepos_ivpr 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor Positionen.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdatepos_ivpr

	prepare cursor cuu_ivpr from sql
		select #iv_pr.% from iv_pr
		where @iv_pr.mdn and
		      @iv_pr.pr_gr_stuf and
		      @iv_pr.kun_pr and
		      @iv_pr.a for update end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closupdatepos_ivpr
-
-	In		: -
-
-	Out		: -	
-			  
-	Beschreibung    : Die Procedure schliesst die update cursor.  
-							
---------------------------------------------------------------------------*/

PROCEDURE closeupdatepos_ivpr

	close cursor cuu_ivpr
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recreadpos_ivpr
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Positionssatz 
-							
---------------------------------------------------------------------------*/

PROCEDURE recreadpos_ivpr

	execute from sql 
		select #iv_pr.% from iv_pr
		where @iv_pr.mdn and
		      @iv_pr.pr_gr_stuf and
		      @iv_pr.kun_pr and
		      @iv_pr.a
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdeletepos_ivpr
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Delete auf den aktuellen
-			  Positionssatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdeletepos_ivpr

	execute from sql 
		delete from iv_pr where current of cuu_ivpr
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: recinsertpos_ivpr
-
-
-	In			: -
-
-	Out			: dsqlstatus		
-			  
-	Beschreibung		: Die Procedure fuegt einen Positionssatz
-				  in die Datenbanktabelle ein.          
-
-------------------------------------------------------------------------*/

PROCEDURE recinsertpos_ivpr

	execute from sql 
		insert into iv_pr (%iv_pr.*)
		values ($iv_pr.*)
	end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdatepos_ivpr
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  Positionssatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdatepos_ivpr

	execute from sql 
		update iv_pr set @iv_pr.% where current of cuu_ivpr
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchupdatepos_ivpr
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest einen Positionssatz for update
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchupdatepos_ivpr

	fetch cursor cuu_ivpr

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: position_loeschen_ivpr
-
-
-	In			: -
-
-	Out			: sqlstatus		
-			  
-	Beschreibung		: Die Procedure loescht eine Position- 
-				  satz aus der Datenbank.             
-
-------------------------------------------------------------------------*/

PROCEDURE position_loeschen_ivpr

	execute from sql
		delete from iv_pr
		where @iv_pr.mdn and
		      @iv_pr.pr_gr_stuf and
		      @iv_pr.kun_pr and
		      @iv_pr.a
	end
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: positionen_loeschen_ivpr
-
-
-	In			: -
-
-	Out			: sqlstatus		
-			  
-	Beschreibung		: Die Procedure loescht alle Positions- 
-				  saetze aus der Datenbank.             
-
-------------------------------------------------------------------------*/

PROCEDURE positionen_loeschen_ivpr

	execute from sql
		delete from iv_pr
		where @iv_pr.mdn and
		      @iv_pr.pr_gr_stuf and
		      @iv_pr.kun_pr 
	end
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: positionen_loeschen_ivpr_a
-
-
-	In			: -
-
-	Out			: sqlstatus		
-			  
-	Beschreibung		: Die Procedure loescht alle Positions- 
-				  saetze aus der Datenbank fuer einen Artikel.
-
-------------------------------------------------------------------------*/

PROCEDURE positionen_loeschen_ivpr_a

	execute from sql
		delete from iv_pr
		where @iv_pr.mdn and
		      @iv_pr.a and
		      iv_pr.kun_pr <> 0 and
		      iv_pr.pr_gr_stuf <> 0
	end
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: positionen_loeschen_ivpr_a_ivprgr
-
-
-	In			: -
-
-	Out			: sqlstatus		
-			  
-	Beschreibung		: Die Procedure loescht alle Positions- 
-				  saetze aus der Datenbank fuer einen Artikel.
-
-------------------------------------------------------------------------*/

PROCEDURE positionen_loeschen_ivpr_a_ivprgr

	execute from sql
		delete from iv_pr
		where @iv_pr.mdn and
		      @iv_pr.a and
		      iv_pr.kun_pr = 0
	end
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: position_einfuegen_ivpr
-
-
-	In			: -
-
-	Out			: dsqlstatus		
-			  
-	Beschreibung		: Die Procedure fuegt einen Positionssatz
-				  in die Datenbanktabelle ein.          
-
-------------------------------------------------------------------------*/

PROCEDURE position_einfuegen_ivpr

	field dsqlstatus smallint

	execute from sql 
		insert into iv_pr (%iv_pr.*)
		values ($iv_pr.*)
	end

	let dsqlstatus = sqlstatus
	if sqlstatus
		if dsqlstatus <> CENSQL239
			execute from sql
				rollback work
			end
		else
			execute from sql
				update iv_pr set @iv_pr.*
				where @iv_pr.mdn and
				      @iv_pr.pr_gr_stuf and
				      @iv_pr.kun_pr and
				      @iv_pr.a
			end

			let dsqlstatus = sqlstatus
		end
	end
	return (dsqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closescrollpos_ivpr
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure schlisset den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE closescrollpos_ivpr

	close cursor cusmain2
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchscrollpos_ivpr
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest alle Positionsaetze
-			  ueber den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchscrollpos_ivpr
parameter
	field curmod smallint
end
	if curmod = CRDFIRST
		fetch first cursor cusmain2
	elseif curmod = CRDNEXT
		fetch next cursor cusmain2
	end
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorplnr	: recreaddefpos_ivpr
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddefpos_ivpr

field dsql smallint
field dsavmdn like iv_pr.mdn 

	let dsavmdn = iv_pr.mdn

	let iv_pr.a = -1

	perform recreadpos_ivpr returning (dsql)
	if dsql = CENSQL100
		if iv_pr.mdn <> 0
			let iv_pr.mdn = 0
			perform recreadpos_ivpr returning (dsql)
		end
	end

	let iv_pr.mdn = dsavmdn

	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorplnr	: inittablepos_ivpr
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittablepos_ivpr

	init table iv_pr
END
procedure what_kennung
if "" = "@(#) mo_dbivpr.rsf	1.400	05.11.91	by RO>" end
end
