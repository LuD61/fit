/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbivprg
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	30.05.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    30.05.90   Simmchen	Modularisierung
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle ivprgrstk , iv_pr
-
-
------------------------------------------------------------------------------
*/

module mo_dbivg

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_ivprg
	proc prepread_ivprg
	proc putmxkey_ivprg
	proc recupdate_ivprg
	proc recdelete_ivprg
	proc recinsert_ivprg
	proc recread_ivprg
	proc recread_ivprg_pos
	proc getmxkey_ivprg
	proc recreaddef_ivprg
	proc inittable_ivprg
	proc savekey_ivprg
	proc backkey_ivprg
	proc initkey_ivprg
	proc cmpkey_ivprg
	proc clrdelstatus_ivprg
	proc getdelstatus_ivprg
	proc fetchcuu_ivprg
	proc fetchread_ivprg
	proc erasecursor_ivprg
	proc closeread_ivprg
	proc closeupdate_ivprg
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
end

IMPORT module mo_cons
end

IMPORT module mo_dbivp
	database bws
end

database bws
	table ivprgrstk end
end

#include "colaenge"

field dsavekey1	 like ivprgrstk.pr_gr_stuf
field dsavekey2	 like ivprgrstk.mdn
field ddefakey   like ivprgrstk.pr_gr_stuf value -1
field dsavekeyp  like iv_pr.a 
field ddefakeyp  like iv_pr.a value -1
field dcurskey	 like ivprgrstk.pr_gr_stuf dimension(CMAXMATRIX)

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_ivprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_ivprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_ivprg

	prepare cursor cuu_ivprg from sql
		select #ivprgrstk.* from ivprgrstk
		where @ivprgrstk.mdn and
		      @ivprgrstk.pr_gr_stuf for update 
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_ivprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cur_ivprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_ivprg

	prepare cursor cur_ivprg from sql
		select #ivprgrstk.* from ivprgrstk
		where @ivprgrstk.mdn and
		      @ivprgrstk.pr_gr_stuf 
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_ivprg
-
-	In		: danzahl	smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_ivprg

parameter field danzahl smallint end

	let dcurskey[danzahl] = ivprgrstk.pr_gr_stuf

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_ivprg
-
-	In		: dzeiger	smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_ivprg

parameter field dzeiger smallint end

	let ivprgrstk.pr_gr_stuf = dcurskey[dzeiger]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_ivprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_ivprg gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_ivprg

	execute from sql 
		update ivprgrstk
		set @ivprgrstk.%
		where current of cuu_ivprg
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_ivprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_ivprg gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_ivprg

	let ivprgrstk.delstatus = -1

	execute from sql
		update ivprgrstk
		set @ivprgrstk.delstatus
		where current of cuu_ivprg
		end

	if sqlstatus
		return (sqlstatus)
	end
	
	execute from sql 
		delete from iv_pr
		where iv_pr.mdn        = $ivprgrstk.mdn and
		      iv_pr.kun_pr     = 0                and
		      iv_pr.pr_gr_stuf = $ivprgrstk.pr_gr_stuf
	end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_ivprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_ivprg

	execute from sql 
		insert into ivprgrstk (%ivprgrstk.*)
		values ($ivprgrstk.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_ivprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_ivprg
	execute from sql
		select #ivprgrstk.* from ivprgrstk 
		where @ivprgrstk.mdn and
		      @ivprgrstk.pr_gr_stuf and
		      ivprgrstk.delstatus <> -1
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_ivprg_pos
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_ivprg_pos
	execute from sql
		select #iv_pr.% from iv_pr where 
		@iv_pr.mdn        and
		iv_pr.kun_pr = 0  and
		@iv_pr.pr_gr_stuf and
		@iv_pr.a 
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_ivprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_ivprg

field dsql smallint
field dsavmdn like ivprgrstk.mdn 

	let dsavmdn = ivprgrstk.mdn

	let ivprgrstk.pr_gr_stuf = ddefakey

	perform recread_ivprg returning (dsql)
	if dsql = CENSQL100
		if ivprgrstk.mdn <> 0
			let ivprgrstk.mdn = 0
			perform recread_ivprg returning (dsql)
		end
	end

	let ivprgrstk.mdn = dsavmdn 

	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_ivprg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_ivprg

	init table ivprgrstk
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: savekey_ivprg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure sichert den alten Schluessel-
-			  wert.   
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_ivprg

	let dsavekey1 = ivprgrstk.pr_gr_stuf
	let dsavekey2 = ivprgrstk.mdn
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: backkey_ivprg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt den gesicherten  
-			  Schluesselwert zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_ivprg

	let ivprgrstk.pr_gr_stuf = dsavekey1
	let ivprgrstk.mdn        = dsavekey2
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_ivprg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_ivprg

	init ivprgrstk.pr_gr_stuf
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_ivprg
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_ivprg

	if dsavekey1 = ivprgrstk.pr_gr_stuf
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_ivprg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_ivprg

	let ivprgrstk.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_ivprg
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_ivprg

	return (ivprgrstk.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_ivprg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_ivprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_ivprg

	fetch cursor cur_ivprg
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_ivprg 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_ivprg
-			  ,cur_ivprg und cus_ivprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_ivprg

	erase cursor cus_ivprg
	erase cursor cuu_ivprg
	erase cursor cur_ivprg
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_ivprg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_ivprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_ivprg

	fetch cursor cuu_ivprg
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_ivprg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_ivprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_ivprg

	close cursor cur_ivprg
	return (sqlstatus) 
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeupdate_ivprg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den update cursor cuu_ivprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeupdate_ivprg

	close cursor cuu_ivprg
	return (sqlstatus) 
END

procedure what_kennung
if "" = "@(#) mo_dbivprg.rsf	1.400	05.11.91	by RO>" end
end
