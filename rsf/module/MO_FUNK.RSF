module mo_funk

/***
--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       mo_funk.rsf
-
        Exports                 :
					proc janein
					proc janein_2
					proc endabfrage
                                        proc mkstring
                                        proc auswahl_br
                                        proc auswahl_ps
                                        proc disp_comm
                                        proc waitwindow
                                        proc get_message
                                        proc as_string  
                                        field parastack
         
        Imports                 :
                                        module mo_men_man
                                        module mo_ma_ut

-       Interne Prozeduren      :  
-
-       Externe Prozeduren      :    
                                        
-
-       Autor                   :       WR    
-       Erstellungsdatum        :       17.09.90
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
        #159    1.234   22.01.92        RO         Neu Procedure as_string
                                                   hinzugefuegt.
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Verschiedene Spezialfunktionen.
-
--------------------------------------------------------------------------------
***/

/** Exports                   **/

export
        proc janein
        proc janein_2
        proc endabfrage
        proc mkstring
        proc auswahl_br
        proc auswahl_ps
        proc waitwindow
        proc disp_comm
        proc get_message
        proc as_string  /* #159 */
        field parastack
end

import module mo_work end
import module mo_men_m end
        

/* Konstanten                                              */

   constant
         HK value "\""
     
/* Groesse fuer die Berechnung der Auswahl-Window-Breite  */

        AUSW_DIFF value -3
   end

/* Glabalvariablen                                         */

/*  Stack fuer variable Parameteruebergabe                */

field  parastack  char (80)  dimension (20)


/* Die Procedure gibt das Maximum von zwei Zahlen zurueck  */

procedure maxi
    parameter
    field wert1     integer
    field wert2     integer
    end

if "" = "@(#) mo_funk.rsf	1.402a	12.01.94	by RO" end

    if wert1 > wert2
		   return(wert1)
    end
    return(wert2)
end



/* Window fuer Abfrage J/N, der Text wird uebergeben  */


procedure janein
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       janein
-
-       In              :    
                                text
-
-       Out             :       auswahl      0  = Nein
-                                            1  = Ja
-       Globals         :       - 
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Anzeige eines Textes im Fenster, der mit
                                Ja oder Nein beantwortwet werden kann.
                                Rueckgabewert (Ja oder Nein) waehlen.
-
--------------------------------------------------------------------------------
**/

    parameter 
    field textnr      char(60)
    end

screen endabfrage 
< ja  >   < nein >
end

   maske wtext
   field wtext   char(60)  pos (1 1)  highlight  length 60 displayonly
   end
                                             
   menue endabfrage
   field ja    value "  Ja  "   pos ja   length 6   
   field nein  value " Nein " pos nein length 6
   end

   field  az      smallint
   field  taste   char (1)
   field  wbreite smallint
   field  sp      smallint
   field  tlen    smallint
   field  text    char (60)
   field  text_nr  smallint

    if strlen (textnr) > 0 and 
        (is smallint (textnr) or
        is integer (textnr))
                    let text_nr = textnr
                    let text = proc get_message (text_nr)
    else
                    let text = textnr
    end

    let tlen = strlen(text) + 6
    let wbreite = proc maxi(tlen 18)
    let wbreite = wbreite + 8
    open window endabfrage dimension (4 wbreite )
    let sp = (76 - wbreite - 2) / 2
    set  bordered window  dimension (5 wbreite )
			   offset    (7 sp)
    let wtext = text
    set form field wtext length (tlen)
    let sp = (wbreite - tlen - 8) / 2
    display pos(0 sp) wtext
    let sp = (wbreite - 18) / 2
    display pos (3 sp) screen endabfrage
    init az
    enter pos (3 sp) menue endabfrage use az
    actions
    /***
          case key17
                  perform rollbackwork
                  perform programm_ende
         ***/
	  case key5
                 continue
    end
    close window endabfrage
    return((az + 1) % 2)
end

procedure janein_2
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       janein
-
-       In              :    
                                text1
                                text2
-
-       Out             :       auswahl      0  = Nein
-                                            1  = Ja
-       Globals         :       - 
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Zweizeilige Anzeige eines Textes im Fenster, 
                                der mit
                                Ja oder Nein beantwortwet werden kann.
                                Rueckgabewert (Ja oder Nein) waehlen.
-
--------------------------------------------------------------------------------
**/

    parameter 
    field textnr1     char(60)
    field textnr2     char(60)
    end

screen endabfrage 
<ja  >  <nein  >
end

   maske wtext
   field wtext1   char(76)  pos (0 0)  length 60
   field wtext2   char(76)  pos (1 0)  length 60
   end
                                             
   menue endabfrage
   field ja    value " Ja "   pos ja   length 4   
   field nein  value " Nein " pos nein length 6
   end

   field  az      smallint
   field  taste   char (1)
   field  wbreite smallint
   field  sp      smallint
   field  tlen    smallint
   field  text1   char (76)
   field  text2   char (76)
   field  txt_nr1 smallint
   field  txt_nr2 smallint

    if strlen (textnr1) > 0 and 
        (is smallint (textnr1) or
        is integer (textnr1))
                    let txt_nr1 = textnr1
                    let text1 = proc get_message (txt_nr1)
    else
                    let text1 = textnr1
    end
    if strlen (textnr2) > 0 and 
        (is smallint (textnr2) or
        is integer (textnr2))
                    let txt_nr2 = textnr2
                    let text2 = proc get_message (txt_nr2)
    else
                    let text2 = textnr2
    end

    let tlen = proc maxi (strlen(text1) strlen (text2))
    let wbreite = proc maxi(tlen 18)
    open window endabfrage dimension (4 wbreite)
    let sp = (76 - wbreite - 2) / 2
    set bordered window    dimension (6 wbreite + 2)
			   offset    (7 sp)
    let wtext1 = text1
    let wtext2 = text2
    set form field wtext1 length (tlen)
    set form field wtext2 length (tlen)
    let sp = (wbreite - tlen) / 2
    display pos(0 sp) wtext1
    display pos(0 sp) wtext2
    let sp = (wbreite - 18) / 2
    display pos (3 sp) screen endabfrage
    init az
    enter pos (3 sp) menue endabfrage use az
    actions
          case key17
                  perform rollbackwork
                  perform programm_ende 
	  case key5
		  close window endabfrage
		  return(0)
    end
    close window endabfrage
    return((az + 1) % 2)
end


/* Beim Druecken der Taste f5 wird die Meldung aufgeblendet, dass
   saemtliche Aenderungen wieder rueckgaengig gemacht werden       */

procedure endabfrage
/**
--------------------------------------------------------------------------------
-
-       Procedure       :     
-
-       In              :    
-
-       Out             :       -
-
-       Globals         :       - 
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Spezielle Abfrage nach Abbruch bei Druecken der
                                Taste F5
-
--------------------------------------------------------------------------------
**/


screen endabfrage 

 Die gešnderten Daten werden nicht gespeichert.
              Trotzdem abbrechen ?

                <ja  >  <nein  >
end
                                             
   menue endabfrage
   field ja    value " Ja "   pos ja   length 4   
   field nein  value " Nein " pos nein length 6
   end

   field  az     smallint
   field  taste  char (1)


    open window endabfrage dimension (6 48)
    set bordered window    dimension (8 50)
			 offset    (5 13)
    display screen endabfrage
    init az
    enter menue endabfrage use az
    close window endabfrage
    return((az + 1) % 2)
end

/** String zusammenbauen                                        **/

procedure mkstring
/**
--------------------------------------------------------------------------------
-
-       Procedure       :      mkstring   
-
-       In              :       string
-
-       Out             :       zielstring
-
-       Globals         :       parastck 
-
-       Errorcodes      :       -
-
-       Beschreibung    :       String mit Variableninhalten zusammenbauen.
                                Die Varuablen werden im String mit $ gekenn-
                                zeichnet.   
                                Um die Anzahl der Parameter variabel zu
                                gestalten wird als Parameter nur der String
                                uebergeben. Die Inhalte der Variablen im String
                                muessen vorher in der Matrix parastack in
                                richtiger Reihenfolge gespeichert werden.
                                Im String stehen $s und $c fuer String-
                                variable. Sie werden im Zielstring in 
                                Anfueherungszeichen gesetzt.
                                Beispiel :
                                      let lief = 1
                                      let mdn = 3
                                      let parastack[0] = lief
                                      let parastack[1] = mdn
                                      let string = proc mkstring 
                                        (where lief = $s and mdn = $d)

                                Inhalt von string :
                                      "where lief = "1" and mdn = 3"
-
--------------------------------------------------------------------------------
**/


    parameter
    field string     char (1000)
    end

    field ps           smallint
    field altpos       smallint
    field strlaenge    smallint
    field zielstring   char (4000)
    field ustring      char (1000)
    field paranr       smallint
    field p_typ        char (1)
    field lauf         smallint
    field parawert     char (256)

    init zielstring paranr lauf
    let strlaenge = strlen (string)
    move 1 to ps altpos
    let ps = instring (string,"$",ps,1)
    if ps = 0
                 return (string)
    end
    
    while ps 
             if ps = altpos
                 init ustring
             else
                 getstr (string,ustring,altpos,ps - altpos)
             end
             if lauf = 0
                 let lauf = 1
                 if strlen(ustring)
                           let zielstring = ustring
                 end
             else
                 let zielstring = clipped (zielstring) # " " # ustring
             end
             getstr (string,p_typ,ps + 1,1)
             if p_typ = "s" or p_typ = "c"
                 let parawert = HK # clipped (parastack[paranr]) # HK
             else
                 let parawert = parastack[paranr]
             end
             let zielstring = clipped (zielstring) # " " # clipped (parawert) 
             let ps = ps + 2
             let altpos = ps 
             if ps > strlaenge
                 return (zielstring)
             end
             let ps = instring(string,"$",ps,1)
             let paranr = paranr + 1
     end
     getstr (string,ustring,altpos,strlaenge)
     let zielstring = clipped (zielstring)  # " " # ustring
     return (zielstring)
end


procedure auswahl_br
/**
--------------------------------------------------------------------------------
-
-       Procedure       :      auswahl_br   
-
-       In              :      ps
                               laenge
-
-       Out             :      breite
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :      Ermittelt die Windowbreite, die dem Auswahl-
                               Modul uebergeben werden muss.
                               Als Parameter werden die Position und die
                               Laenge des letzten Feldes der Maske uebergeben. 
-
--------------------------------------------------------------------------------
**/


    parameter
    field  ps      smallint
    field  laenge  smallint
    end

    return (ps + laenge + AUSW_DIFF)
end


procedure auswahl_ps
/**
--------------------------------------------------------------------------------
-
-       Procedure       :      auswahl_br   
-
-       In              :      br
                               ps
                               laenge
-
-       Out             :      breite
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :      Ermittelt die Windowposition, die dem Auswahl-
                               Modul uebergeben werden muss.
-
--------------------------------------------------------------------------------
**/


    parameter
    field  br      smallint
    field  ps      smallint
    field  laenge  smallint
    end

    return ((br - (ps + laenge + AUSW_DIFF)) / 2)
end


procedure waitwindow
/**
--------------------------------------------------------------------------------
-
-       Procedure       :      wait   
-
-       In              :      text
                               attribute
-
-       Out             :  
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Warteinfo im Window anzeigen 
                           
-
--------------------------------------------------------------------------------
**/

parameter
field  textnr       char (76)
field  attr         char (20)
end


field  dwait        char (12)
field  dtext        char (76)
field  txt_nr       integer
field  len          smallint
field  wlen         smallint
field  wpos         smallint
field  wzeilen      smallint

form warteinfo
    field text1  use dwait                     pos (0 1)
    displayonly
    field text2  use dtext                     pos (0 16) 
    displayonly
end

    let dwait = "BITTE warten"
    
    if strlen (textnr) > 0 and 
        (is smallint (textnr) or
        is integer (textnr))
                    let txt_nr = textnr
                    let dtext = proc get_message (txt_nr)
    else
                    let dtext = textnr
    end

    
    let len = strlen (dtext)
    if len = 0
                    let wzeilen = 1
    else
                    let wzeilen = 2
    end

    if len > 12
                    let wlen = len + 2
    else
                    let wlen = 14
    end

    let wpos = (78 - wlen) / 2
    
    if len > 0
               set form field text2 length (len + 4)
    end
    set form field text1 length (wlen - 2)

    upshift (attr)
    switch attr
                    case "BLINK"
                            set form warteinfo highlight (ON)
                            break
                    case "REVERSE"
                            set form warteinfo reverse (ON)
                            break
                    case "NORMAL"
                            set form warteinfo normal (ON)
                            break
                    case "HIGHLIGHT"
                            set form warteinfo highlight (ON)
                            break
                    case "UNDERLINE"
                            set form warteinfo UNDERLINE (ON)
                            break
    end
    /**************   
    open window warteinfo (2 wlen)
    set  window (wzeilen + 2 wlen + 6 9 wpos)
    *****************/
    select window kommentar
    display text1
    if len > 0
                   display text2
    end
    /**
    close window warteinfo
    **/
    select window mamain1
end


procedure disp_comm
/**
--------------------------------------------------------------------------------
-
-       Procedure       :      disp_comm   
-
-       In              :      text
                               attribute
-
-       Out             :  
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Kommentar anzeigen  
                           
-
--------------------------------------------------------------------------------
**/

parameter
field  textnr       integer
field  attr         smallint
end
 

       if attr
                   display comment (textnr)
       else
                   display comment (proc get_message (textnr))
       end
end


procedure get_message
/**
--------------------------------------------------------------------------------
-
-       Procedure       :     get_message
-
-       In              :     text_nr       /* Nummer der Meldung   */  
-
-       Out             :     text
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Meldung aus Text-Datei holen ohne Attribute  
                           
-
--------------------------------------------------------------------------------
**/

parameter
field    text_nr            integer
end

    field  dtext            char (76)
    field  atext            char (76)
    field  i1               smallint
    field  i2               smallint

    let dtext = message (text_nr)
    let i1 = instring (dtext,")",1)
    if i1 = 0
             return (dtext)
    end

    let i1 = i1 + 2
    let i2 = instring (dtext, "\N",i1)
    if i2 = 0
             getstr(dtext,atext,i1,0)
    else
            let i2 = i2 - i1
            getstr(dtext,atext,i1,i2)
    end
    return (atext)
end

/* #159 A */
procedure as_string
/**
--------------------------------------------------------------------------------
-
-       Procedure       :     as_string
-
-       In              :     string      
-
-       Out             :     umgewandelter string
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :      String in '"' setzen und 
                               '"' im String kennzeichnen.
                           
-
--------------------------------------------------------------------------------
**/
parameter
field string  char (100) 
end

       field retstring char (100) 
       field i    smallint
       field j    smallint
       field slen smallint
       field zeichen char (1)

       init retstring
       let slen = strlen (string)

       move 1 to i j
       putstr (retstring,"\"",j,1)
       while i <= slen
                  getstr (string,zeichen,i,1)
                  if zeichen = "\""
                             putstr (retstring,zeichen,j,1)
                  end 
                  putstr (retstring,zeichen,j,1)
       end
       putstr (retstring,"\"",j,1)
       return (clipped (retstring))
end
/* #159 E */    
