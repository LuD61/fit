
/***
100106 : ab jetzt "ECHTER" Leergutsaldo
Achtung : kun_leer = 1 nur akt. Display, kun_leer = 2 mit Entlastung
kun_leer = 3  ohne Entlastung bei Faktura ( !? Standard ?! )
leer_konto braucht kun_leer> 1 
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Geisental 12, 4630 Bochum 1, Tel: 0234/5065-250
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_leerdr
-
-	Autor			:	JG
-	Erstellungsdatum	:	06.06.95
-	Modifikation		:    
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#2399	2.0	06.06.95	JG	Erstellung
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:  Prozeduren fuer das erweiterte Leergut-Saldo

-	aufgerufene FORMATE
-	---------------------------------------------------------------------
-
-
------------------------------------------------------------------------------
-
##D2A
-	Datenbanktabellen	:  
-
##D2E

##D3A
	logische verknuepfungen 
##D3E

##D4A
-	Tabellenname	Felder			Zugriff (r-read,u-update)
-       ----------------------------------------------------------------------
##D4E
##D5A
##D5E
-
##D6A
-	aufgerufene Module	        
-       ----------------------------------------------------------------------
##D6E
##D7A
##D7E
-------------------------------------------------------------------------- ***/

MODULE mo_leerd

/*--- Export Section ---*/
export
	proc eleer_aktual
	proc eleer_druck
	proc entlastung
	database bws
end

/*--- Import Section ---*/
IMPORT module mo_men_m
	field in_work
END


IMPORT module mo_data 
END

IMPORT module mo_ktou   // 100106 
END

IMPORT module mo_komma
END

IMPORT module mo_cons 
END

IMPORT module mo_meld
	proc disp_sql
	proc disp_msg
END

IMPORT module mo_ma_ut
END

IMPORT module mo_item
	proc printitem
END

IMPORT module mo_dba_b
END

IMPORT module mo_dbls
END

IMPORT module mo_dbkun
database bws
END


IMPORT module mo_sys_p
END

DATABASE bws	
table kun_leerg end
table leer_lsdr end
table leer_bel  end     // 100106 
END

/*--- lokale Deklarationen Modul ---*/

CONSTANT
MAXLEERG   value 30	/* mehr passt nicht sinnvoll auf Papier  */
END

DATA 

	field k	smallint	/* allgemeine return-Variable */

	field daktivity	smallint	/* Art der Bearbeitung */
        field draiff    smallint        // 281101
        field dlekto    smallint        // 100106
        field alles_weg smallint        // 120809

        field dmdn_save like lsk.mdn    /* Zugriffsoptimierung */
	field dls_save	like lsk.ls	/* Zugriffsoptimierung */
        field draiffzeil integer        // 281101
	field duplkz	smallint	/* #quark */
END

table dglobal		/* globaler Zugriff auf sel-krit's */
	field dmdn	like lsk.mdn
	field dfil 	like lsk.fil
	field dkz	smallint
	field dls	like lsk.ls
	field dkun_fil	like lsk.kun_fil
	field dkunfil	like kun.kun
        field dhkunfil  like kun.kun    // 100106 : Hauptkunde
	field dkunleer	smallint	/* parameterwert */
	field da	like lsp.a
	field dlief_me	decimal (10,3)
	field dpers_nam	char(16)
end

table leer_int DIMENSION (MAXLEERG)
	field a		decimal (13,0)
	field zug	integer
	field abg	integer
	field bsd	integer
	field preis	decimal (8,2)
	field dstat	smallint
end


/*--- Prozeduren ---*/
/*--- Prozeduren ---*/
/*--- Prozeduren ---*/

/*##D8A*/
/** -------------------------------------------------------------------------
-
-	Procedure	: eleer_aktual
-	In		:  field dmdn	Mandant
-			   field dfil 	Filiale
-			   field dkz	LS-Status
-			   field dls	LS-Nr
-			   field dkun_fil kun_fil
-			   field dkunfil  Kun- oder Fil-Nummer
-			   field dkunleer  Kundenstamm-Parameter
-			   field da  Artikel-Nummer
-			   field dlief_me  Mengen-maessig
-	Out		: -
-	Beschreibung	: sammelt die Werte und stellt den aktuellen Zustand
-			 in einer Matrix bereit
-			 darf aber auch nur beim Uebergang oder
-			 bei der Druck-Sammelei bestueckt werden 
-------------------------------------------------------------------------- **/
/*##D8E*/

PROCEDURE eleer_aktual

parameter
	field dmdn	like lsk.mdn
	field dfil 	like lsk.fil
	field dkz	smallint
	field dls	like lsk.ls
	field dkun_fil	like lsk.kun_fil
	field dkunfil	like kun.kun
	field dkunleer	smallint
	field da	decimal (13,0)
	field dlief_me	decimal (10,3)
	field dpers_nam	char(16)
	field dpreis	decimal(8,2)
        field dtou      like lsk.tou    // 100106
end

field dpointer		integer		/* pointer in der Tabelle */
field dpointerex	integer	/* Hilfs-Pointer in der Tabelle */

breakpoint
	let dglobal.dmdn = dmdn
	let dglobal.dfil = dfil
	let dglobal.dkz = dkz
	let dglobal.dls = dls
	let dglobal.dkun_fil = dkun_fil
	let dglobal.dkunfil = dkunfil

	let dglobal.dkunleer = dkunleer
	let dglobal.da = da
	let dglobal.dlief_me = dlief_me
	let dglobal.dpers_nam = dpers_nam

if (( dmdn_save <> dmdn) or (dls_save <> dls)) 
	init table leer_int
	let dmdn_save = dmdn 
	let dls_save = dls 

end

if not proc abbrechen() 
	return
end

// 100106 A
        if (( dlekto = 0 ) or ( dtou = 0 )) 
            let dglobal.dhkunfil = dglobal.dkunfil
        else
            execute from sql
              select #tou.eigentour,#tou.lgkonto
                from tou
               where tou.tou = $dtou
            end
            if (( not sqlstatus ) and
                       ( tou.eigentour = 0 ) and ( tou.lgkonto > 0 ))
                let dglobal.dhkunfil = tou.lgkonto
            else    // sqlstatus oder alles andere
                let dglobal.dhkunfil = dglobal.dkunfil
            end
                  
        end
// 100106 E

    init dpointer
    while (dpointer < MAXLEERG)

	if (leer_int[dpointer].a < 0.5)
	    let leer_int[dpointer].a = da
	    if (dlief_me >= 0) 
		let leer_int[dpointer].zug = dlief_me
	    else
		let leer_int[dpointer].abg =  0 - dlief_me
	    end
	    let leer_int[dpointer].preis =   dpreis
	    break
	end
	if (((leer_int[dpointer].a - da) < 0.5 )  and
	       ((leer_int[dpointer].a - da) > -0.5 ))	/* gefunden */
	    if dlief_me >= 0 
		let leer_int[dpointer].zug = 
		leer_int[dpointer].zug + dlief_me
	    else
		let leer_int[dpointer].abg = 
		leer_int[dpointer].abg - dlief_me
	    end
	    let leer_int[dpointer].preis =   dpreis
	    break
	end
	if (leer_int[dpointer].a < da)		/* weitersuchen */
	    let dpointer = dpointer + 1 
	    continue
	end

	if (leer_int[dpointer].a > da)		/* einfuegen in diese Stelle */
	    let dpointerex = MAXLEERG - 1  
	    while (dpointerex > dpointer)
	       let leer_int[ dpointerex ].a = leer_int[ dpointerex - 1].a 

	       let leer_int[ dpointerex ].zug = leer_int[dpointerex - 1].zug 
	       let leer_int[ dpointerex ].abg = leer_int[dpointerex - 1].abg 
	       let leer_int[ dpointerex ].bsd = leer_int[dpointerex - 1].bsd 
	       let leer_int[ dpointerex ].preis=leer_int[dpointerex - 1].preis 

	       let dpointerex = dpointerex - 1

	       continue
	    end
	    let leer_int[ dpointer ].a = da

		let leer_int[dpointer].zug = 0 
		let leer_int[dpointer].abg = 0 
		let leer_int[dpointer].bsd = 0 
	    if dlief_me >= 0 
		let leer_int[dpointer].zug = dlief_me 
	    else
		let leer_int[dpointer].abg = 0 - dlief_me
	    end
	    let leer_int[dpointer].preis =   dpreis
	    break
	end	/* ende einfuegen */

    end		/* ende der Suchschleife */

end



/** --------------------------------------------------------------------
-	PROC : abbrechen 
-	IN   : -
-	OUT  : FALSE/TRUE => Abbrechen oder weitermachen 
--------------------------------------------------------------------- **/

PROCEDURE abbrechen

if (dglobal.dkunleer = 0)
	return (0)		/* im Stamm ausgeschaltet */
end

if (daktivity = 0 )
	perform aktivieren
end
if (daktivity = 0 )	/* es ist nix zu tun */
	return ( 0 )
end
return (1)
end

/** -----------------------------------------------------------------------
-	Procedure	: aktivieren
-	In		: -
-			  -
-	Out		: -
-	Beschreibung	: liest bei bedarf den Systemparameter .. 
----------------------------------------------------------------------- **/

PROCEDURE aktivieren

field dmerker	smallint


field sys_par_wert char (1)
field sys_par_bez char (10)

if dmerker = 3		/* sys_par bereits gelesen */
	return
end

	perform sys_par_holen ( "kun_leer" )  returning 
			( k , sys_par_wert , sys_par_bez )
	if k = 0
		let sys_par_wert = "0"
	end
	let daktivity = sys_par_wert
// 281101
        perform sys_par_holen ( "raiff" )  returning 
			( k , sys_par_wert , sys_par_bez )
	if k = 0
		let sys_par_wert = "0"
	end
        let draiff = sys_par_wert


// 100106
        perform sys_par_holen ( "leer_konto" )  returning 
			( k , sys_par_wert , sys_par_bez )
	if k = 0
		let sys_par_wert = "0"
	end
        let dlekto = sys_par_wert

// 120809

        perform sys_par_holen ( "leer_alle" )  returning 
			( k , sys_par_wert , sys_par_bez )
	if k = 0
		let sys_par_wert = "0"
	end

        let alles_weg = sys_par_wert

   let dmerker = 3

END


/** -----------------------------------------------------------------------
-	Procedure	: werte_holen
-	In		: -
-			  -
-	Out		: -
-	Beschreibung	: liest bei bedarf die werte aus aus leer_lsdr
-			  und updatet , was noetig ist ... 
----------------------------------------------------------------------- **/
procedure werte_holen

field dpointer		integer		/* pointer in der Tabelle */
field dpointerex	integer	/* Hilfs-Pointer in der Tabelle */
field dgeszeilen		integer	/* Anzahl gueltiger Zeilen */
field dgeszeilwert		integer	/* Anzahl Zeilen <> 0 */

if daktivity < 2
	return ( -1 -1 )
end

init dgeszeilen
init dgeszeilwert

close cursor lesewerte
erase cursor lesewerte
prepare cursor lesewerte from sql
select #leer_lsdr.% from leer_lsdr
	where 
	leer_lsdr.ls = $dglobal.dls and
	leer_lsdr.blg_typ = "L" and
	leer_lsdr.mdn = $dglobal.dmdn and
	leer_lsdr.a >  0
end
// 281101 A
let draiffzeil = 0
if draiff
    execute into draiffzeil from sql
     select count(*) from leer_lsdr
	where 
	leer_lsdr.ls = $dglobal.dls and
	leer_lsdr.blg_typ = "L" and
	leer_lsdr.mdn = $dglobal.dmdn and
	leer_lsdr.a >  0
    end
end
// 281101 E

fetch cursor lesewerte
if sqlstatus	/* noch nix da gewesen */
    close cursor stammlesen
    erase cursor stammlesen
    // 100106 dkunfil->dhkunfil
    prepare cursor stammlesen from sql
    select #kun_leerg.% from kun_leerg
    where
    	kun_leerg.a > 0				and 
        kun_leerg.kun = $dglobal.dhkunfil        and
    	kun_leerg.kun_fil = $dglobal.dkun_fil	and
    	kun_leerg.mdn = $dglobal.dmdn
    end
    fetch cursor stammlesen
    while not sqlstatus
	init dpointer
	while (dpointer < MAXLEERG)
	   if (leer_int[dpointer].a < 0.5)		/* inserten */
		let leer_int[dpointer].a = kun_leerg.a
		let leer_int[dpointer].bsd = kun_leerg.bsd_stk
		let leer_int[dpointer].abg = 0
		let leer_int[dpointer].zug = 0
		perform leer_lsdrins(
			leer_int[dpointer].a
			leer_int[dpointer].bsd
			leer_int[dpointer].zug
			leer_int[dpointer].abg
		    )	
		let leer_int[dpointer].dstat = 1	/* aktualisierung geschehen */
		break
	   end	/* = 0 */
	   if (((leer_int[dpointer].a - kun_leerg.a) < 0.5) and
	       ((leer_int[dpointer].a - kun_leerg.a) > -0.5)) 	/* gefunden */
		let leer_int[dpointer].bsd = kun_leerg.bsd_stk
        	perform leer_lsdrins(
			leer_int[dpointer].a
			leer_int[dpointer].bsd
			leer_int[dpointer].zug
			leer_int[dpointer].abg
			    )	
			let leer_int[dpointer].dstat = 1
					/* aktualisierung geschehen */
		perform kun_leergupd(
			leer_int[dpointer].a
			leer_int[dpointer].bsd
			leer_int[dpointer].zug
			leer_int[dpointer].abg
			    )	
	    	break
	    end	/* = a */
	    if (leer_int[dpointer].a < kun_leerg.a)	/* weitersuchen */
		let dpointer = dpointer + 1 
		continue
	    end

	    if (leer_int[dpointer].a > kun_leerg.a)
						 /* einfuegen in diese Stelle */
		let dpointerex = MAXLEERG - 1  
		while (dpointerex > dpointer)
	           let leer_int[ dpointerex ].a = leer_int[ dpointerex - 1 ].a 
	           let leer_int[ dpointerex ].zug =
					leer_int[ dpointerex - 1 ].zug 
	           let leer_int[ dpointerex ].abg =
					leer_int[ dpointerex - 1 ].abg 
	           let leer_int[ dpointerex ].bsd =
					leer_int[ dpointerex - 1 ].bsd 
	           let leer_int[ dpointerex ].dstat =
					leer_int[dpointerex - 1 ].dstat
	           let leer_int[ dpointerex ].preis =
					leer_int[dpointerex - 1 ].preis
	           let dpointerex = dpointerex - 1

	           continue
		end		/* end while verschieben */
		let leer_int[ dpointer ].a = kun_leerg.a
		let leer_int[ dpointer ].zug = 0
		let leer_int[ dpointer ].abg = 0
		let leer_int[dpointer].bsd = kun_leerg.bsd_stk
		let leer_int[ dpointer ].dstat = 1
		perform leer_lsdrins(
			leer_int[dpointer].a
			leer_int[dpointer].bsd
			leer_int[dpointer].zug
			leer_int[dpointer].abg
	    	)	
		break
	    end	/* end  einfuegen */
	end	/* end while MAXLEERG */
 	fetch cursor stammlesen
    end		/* ende der such-schleife */
    init dpointer
    while dpointer <  MAXLEERG
    	if ((leer_int[dpointer].a > 0.5) and (leer_int[dpointer].dstat = 0)) 
		perform leer_lsdrins(
			leer_int[dpointer].a
			leer_int[dpointer].bsd
			leer_int[dpointer].zug
			leer_int[dpointer].abg
	    	)	
		perform kun_leergins(
			leer_int[dpointer].a
			leer_int[dpointer].bsd
			leer_int[dpointer].zug
			leer_int[dpointer].abg
	    	)	
		let leer_int[dpointer].dstat = 1
	end	
	if (leer_int[dpointer].a > 0.5)
		let dgeszeilen = dgeszeilen + 1
	end

	if (
	     ( leer_int[dpointer].bsd  - leer_int[dpointer].abg 
		+ leer_int[dpointer].zug
	     ) <> 0 
	   )
		let dgeszeilwert = dgeszeilwert + 1
	end
	let dpointer = dpointer + 1 
    end
else	/* aktualiseren der vorhandenen Eintrage .. */
    while not sqlstatus

	init dpointer
	while (dpointer < MAXLEERG)
	    if (leer_int[dpointer].a < 0.5)
		/* Saetze ,die nicht in diesem Beleg beeinflusst werden */
		let leer_int[dpointer].a = leer_lsdr.a
		let leer_int[dpointer].bsd = leer_lsdr.stk
		let leer_int[dpointer].zug = leer_lsdr.me_stk_zu
		let leer_int[dpointer].abg = leer_lsdr.me_stk_abn
		let leer_int[dpointer].dstat = 1 /* aktualisierung unnoetig */
		break
	   end
	   if (((leer_int[dpointer].a - leer_lsdr.a) < 0.5) and
	      ((leer_int[dpointer].a - leer_lsdr.a) > -0.5))	/* gefunden */
		let leer_int[dpointer].bsd = leer_lsdr.stk
	/* #quark A */
	    if duplkz = 1 		/* falls Nachdruck */
		if ((leer_lsdr.me_stk_zu - leer_lsdr.me_stk_abn
		 - leer_int[dpointer].zug + leer_int[dpointer].abg ) <> 0 )

/* 300908 : im rosi-teil nie aktuell geworden, im c-teil nicht sinnvoll
                        perform kun_leergupd(
			leer_int[dpointer].a
			leer_int[dpointer].bsd
			leer_int[dpointer].zug  - leer_lsdr.me_stk_zu 
			leer_int[dpointer].abg  - leer_lsdr.me_stk_abn
			    )
< ----- */
			perform leer_lsdrupd(
			leer_int[dpointer].a
			leer_int[dpointer].bsd 
			leer_int[dpointer].zug 
			leer_int[dpointer].abg
			    )	
	        end		
/* #quark E */

	    else	/* aufsummieren */
		perform kun_leergupd(
			leer_int[dpointer].a
			leer_int[dpointer].bsd
			leer_int[dpointer].zug  /*qwerz + leer_lsdr.me_stk_zu */
			leer_int[dpointer].abg  /*qwerz + leer_lsdr.me_stk_abn*/
			    )	
			let leer_int[dpointer].zug =
			leer_int[dpointer].zug + leer_lsdr.me_stk_zu
			let leer_int[dpointer].abg = 
			leer_int[dpointer].abg + leer_lsdr.me_stk_abn
		perform leer_lsdrupd(
			leer_int[dpointer].a
			leer_int[dpointer].bsd 
			leer_int[dpointer].zug 
		/* #qwerz2 */
			leer_int[dpointer].abg 
			    )	
	     end		/* #quark */
		let leer_int[dpointer].dstat = 1 /* aktualisierung geschehen */
	    	break
	   end
	   if (leer_int[dpointer].a < leer_lsdr.a)	/* weitersuchen */
		let dpointer = dpointer + 1 
		continue
	   end

	   if (leer_int[dpointer].a > leer_lsdr.a) /*einfuegen in diese Stelle*/
		let dpointerex = MAXLEERG - 1  
		while (dpointerex > dpointer)
		    let leer_int[ dpointerex ].a = leer_int[ dpointerex - 1 ].a 
		    let leer_int[ dpointerex ].zug =
					leer_int[ dpointerex - 1 ].zug 
		    let leer_int[ dpointerex ].abg =
					leer_int[ dpointerex - 1 ].abg 
		    let leer_int[ dpointerex ].bsd =
					leer_int[ dpointerex - 1 ].bsd 
		    let leer_int[ dpointerex ].dstat =
					leer_int[dpointerex - 1 ].dstat
		    let leer_int[ dpointerex ].preis =
					leer_int[dpointerex - 1 ].preis
		    let dpointerex = dpointerex - 1

	            continue
		end
		let leer_int[ dpointer ].a = leer_lsdr.a
		let leer_int[ dpointer ].zug = leer_lsdr.me_stk_zu
		let leer_int[ dpointer ].abg = leer_lsdr.me_stk_abn
		let leer_int[dpointer].bsd = leer_lsdr.stk
		let leer_int[ dpointer ].dstat = 1	/* updaten unnoetig */
		break
	    end	/* ende einfuegen */
	end	/* ende MAXLEERG */

	fetch cursor lesewerte 
    end		/* ende der Suchschleife */
    init dpointer
    while dpointer <  MAXLEERG
    	if ((leer_int[dpointer].a > 0.5) and (leer_int[dpointer].dstat = 0)) 
		perform leer_lsdrins(
			leer_int[dpointer].a
			leer_int[dpointer].bsd
			leer_int[dpointer].zug
			leer_int[dpointer].abg
	    	)	
		perform kun_leergins(
			leer_int[dpointer].a
			leer_int[dpointer].bsd
			leer_int[dpointer].zug
			leer_int[dpointer].abg
	    	)	
		let leer_int[dpointer].dstat = 1
	end	
	if (leer_int[dpointer].a > 0.5)
		let dgeszeilen = dgeszeilen + 1
	end
	if (
	     ( leer_int[dpointer].bsd  - leer_int[dpointer].abg 
		+ leer_int[dpointer].zug
	     ) <> 0 
	   )
		let dgeszeilwert = dgeszeilwert + 1
	end
	let dpointer = dpointer + 1 
    end
end

	return (dgeszeilen,dgeszeilwert)
end


/*##D8A*/
/** -----------------------------------------------------------------------
-	Procedure	: eleer_druck
-	In		:  field dmdn	Mandant
-			   field dfil 	Filiale
-			   field dkz	Erst/Nachdruck ( duplikat )
-			   field dls	LS-Nr
-			   field dkun_fil kun_fil
-			   field dkunfil  kun/fil-Nummer
-			   field dkunleer  Kundenstamm-Parameter
-			   field dpers_nam  Benutzer-Name
-			   field dmodus	0	= Werte holen,
-						  Saetze aktualiseren
-			                1	= nur ausgeben
-	Out		: - ( Anzahl gueltiger Saetze ) 
-			  - ( Pointer in der Matrix => nicht ausgewertet )
-			  - a = Artikel
-			  - bsd
-			  - zug
-			  - abg
-			
-	Beschreibung	: holt diverse Werte, gibt sie zurueck
-			  
-	
----------------------------------------------------------------------- **/
/*##D8E*/

PROCEDURE eleer_druck

parameter
	field dmdn	like lsk.mdn
	field dfil 	like lsk.fil
	field dkz	smallint
	field dls	like lsk.ls
	field dkun_fil	like lsk.kun_fil
	field dkunfil	like kun.kun
	field dkunleer	smallint
	field dmodus	smallint
	field dpers_nam	char(16)
        field dtou      like lsk.tou    // 100106
end

field zeilzahl integer	/* gesamtzahl */
field zeilakt integer	/* aktuelle Zeile */
field zeilguelt integer	/* Zeilen <> 0 */

breakpoint

let duplkz = dkz	/* #quark */

let dglobal.dkunleer = dkunleer

if not proc abbrechen() 
	let zeilzahl = -1
	return ( zeilzahl , -1 , 0 , 0 , 0 , 0 )
end

let dglobal.dmdn = dmdn
let dglobal.dfil = dfil
let dglobal.dkz = dkz
let dglobal.dls = dls
let dglobal.dkun_fil = dkun_fil
let dglobal.dkunfil = dkunfil

// 100106 A
        if (( dlekto = 0 ) or ( dtou =  0 ))
            let dglobal.dhkunfil = dglobal.dkunfil
        else
            execute from sql
              select #tou.eigentour,#tou.lgkonto
               from tou
               where tou.tou = $dtou
            end
            if (( not sqlstatus ) and
                         ( tou.eigentour = 0 ) and ( tou.lgkonto > 0 ))
                    let dglobal.dhkunfil = tou.lgkonto
            else    // sqlstatus oder alles andere
                    let dglobal.dhkunfil = dglobal.dkunfil
            end
                  
        end
// 100106 E

let dglobal.dpers_nam = dpers_nam

if dmdn_save <> dmdn or (dls_save <> dls)	/* noch nicht aktualisiert */
	init table leer_int
	let dmdn_save = dmdn 
	let dls_save = dls 
	perform werte_holen returning (zeilzahl , zeilguelt )
	let zeilakt = 0
elseif dmodus = 0 
	perform werte_holen returning (zeilzahl , zeilguelt )
	let zeilakt = 0
end
/* hier Anfang fuer jede Runde */
if zeilguelt > 0
  while   (
	    (zeilakt <= zeilzahl ) and
	    (
		(leer_int[zeilakt].bsd +
		 leer_int[zeilakt].zug -
		 leer_int[zeilakt].abg
		)
		 = 0
	    )
	)
            let zeilakt = zeilakt + 1
  end
else
   let zeilakt = zeilzahl + 1
end

let zeilakt = zeilakt + 1

if zeilakt > zeilzahl
	let zeilakt = MAXLEERG
	let zeilzahl = -1
	let zeilguelt = -1
        let draiffzeil = 0      // 240103
end
    if ( draiff )       // 281101 : raiff zusaetzlich
        if ( zeilguelt = -1 )
            if draiffzeil > 0
                return (  draiffzeil * 2 , zeilakt , 
                    leer_int[zeilakt - 1].a ,
                    leer_int[zeilakt - 1].bsd ,
                    leer_int[zeilakt - 1].zug ,
                    leer_int[zeilakt - 1].abg
                   )
             else
                return ( zeilguelt , zeilakt , 
                    leer_int[zeilakt - 1].a ,
                    leer_int[zeilakt - 1].bsd ,
                    leer_int[zeilakt - 1].zug ,
                    leer_int[zeilakt - 1].abg
                   )
             end
        else
                return (  ( zeilguelt + draiffzeil ) * 2 , zeilakt , 
                    leer_int[zeilakt - 1].a ,
                    leer_int[zeilakt - 1].bsd ,
                    leer_int[zeilakt - 1].zug ,
                    leer_int[zeilakt - 1].abg
                   )
        end
    end
    return ( zeilguelt , zeilakt , 
           leer_int[zeilakt - 1].a ,
           leer_int[zeilakt - 1].bsd ,
           leer_int[zeilakt - 1].zug ,
           leer_int[zeilakt - 1].abg
          )
END /* of eleer_druck */

/** -----------------------------------------------------------------------
-	Procedure	: kun_leergins
-	In		: a , bsd ,zug,abg
-			  -
-	Out		: -
-	Beschreibung	: Anlegen eines entsprechenden Satzes in kun_leerg
----------------------------------------------------------------------- **/
procedure kun_leergins
PARAMETER
    field xa like kun_leerg.a
    field xbsd	integer
    field xzug	integer
    field xabg	integer
END

	let kun_leerg.delstatus = 0
	let kun_leerg.mdn = dglobal.dmdn
	let kun_leerg.fil = dglobal.dfil
	let kun_leerg.kun_fil = dglobal.dkun_fil
        let kun_leerg.kun = dglobal.dhkunfil

        // 100106 dkunfil->dhkunfil ab jetzt Verbuchung auf Hauptkunde

	let kun_leerg.a = xa
	let kun_leerg.bsd_stk = xbsd + xzug - xabg
	let kun_leerg.bsd_wrt = 0	/* mal bissel unscharf .. */
	if xzug  <> 0
		let kun_leerg.lief_dat = SYSDATE
		let kun_leerg.ben_lief = dglobal.dpers_nam
	end

	if xabg  <> 0
		let kun_leerg.ret_dat = SYSDATE
		let kun_leerg.ben_ret = dglobal.dpers_nam
	end
// 100106 A
        if dlekto
        breakpoint
            init table leer_bel
            let leer_bel.mdn =  dglobal.dmdn
            let leer_bel.kun_fil = dglobal.dkun_fil
            let leer_bel.nr =  dglobal.dls
            let leer_bel.blg_typ =  "L"
            let leer_bel.a = xa
            let leer_bel.anzahl = xzug 
            let leer_bel.bestand = 0
            let leer_bel.datum = SYSDATE
            gettime ( leer_bel.zeit )
            let leer_bel.pers_nam =  dglobal.dpers_nam
            let leer_bel.kun =  dglobal.dkunfil
            let leer_bel.hkun = dglobal.dhkunfil
            let leer_bel.delstatus = 0
       end
// 100106 E
	execute from sql
		insert into kun_leerg (%kun_leerg.*) values ($kun_leerg.%)
	end

	if sqlstatus	/* darf eigentlich nicht sein */
// 100106 dkunfil->dhkunfil
	        execute from sql
		    select #kun_leerg.% from kun_leerg 
			where kun_leerg.a = $xa
                          and kun_leerg.kun = $dglobal.dhkunfil
			  and kun_leerg.kun_fil = $dglobal.dkun_fil
			  and kun_leerg.mdn = $dglobal.dmdn
		end
		/* mal bissel unscharf .. */
		if (kun_leerg.bsd_stk <> 0)
   	      	    let kun_leerg.bsd_wrt = kun_leerg.bsd_wrt * 
			(kun_leerg.bsd_stk + xzug - xabg)
	      	    let kun_leerg.bsd_wrt=kun_leerg.bsd_wrt / kun_leerg.bsd_stk
		end

                let leer_bel.bestand = bsd_stk  // 100106

		let kun_leerg.bsd_stk = kun_leerg.bsd_stk + xzug - xabg
		if xzug  <> 0
			let kun_leerg.lief_dat = SYSDATE
			let kun_leerg.ben_lief = dglobal.dpers_nam
		end
	
		if xabg  <> 0
			let kun_leerg.ret_dat = SYSDATE
			let kun_leerg.ben_ret = dglobal.dpers_nam
		end
 // 100106 dkunfil->dhkunfil
    		execute from sql
		    update  kun_leerg set  @kun_leerg.%
			where kun_leerg.a = $xa
                          and kun_leerg.kun = $dglobal.dhkunfil
       		          and kun_leerg.kun_fil = $dglobal.dkun_fil
			  and kun_leerg.mdn = $dglobal.dmdn
		end
        end
// 100106 A
        if dlekto
            if ( xzug <> 0 )
                execute from sql
                    insert into leer_bel (%leer_bel.*) values ($leer_bel.%)
                end
            end
            let leer_bel.bestand = leer_bel.bestand + xzug
            let leer_bel.anzahl = 0 - xabg
            if ( xabg <> 0 )
                execute from sql
                    insert into leer_bel (%leer_bel.*) values ($leer_bel.%)
                end
            end
        end
// 100106 E

end

/** -----------------------------------------------------------------------
-	Procedure	: kun_leergupd
-	In		: a , bsd, zug, abg
-			  -
-	Out		: -
-	Beschreibung	: Updaten eines entsprechenden Satzes in kun_leerg
----------------------------------------------------------------------- **/
procedure kun_leergupd
PARAMETER
    field xa like kun_leerg.a
    field xbsd	integer
    field xzug	integer
    field xabg	integer
END
	field obueberhaupt 	smallint
	let obueberhaupt= 0

// 100106 A
        if dlekto
            init table leer_bel
            let leer_bel.mdn =  dglobal.dmdn
            let leer_bel.kun_fil = dglobal.dkun_fil
            let leer_bel.nr =  dglobal.dls
            let leer_bel.blg_typ = "L"
            let leer_bel.a = xa
            let leer_bel.anzahl = xzug 
            let leer_bel.bestand = 0
            let leer_bel.datum = SYSDATE
            gettime ( leer_bel.zeit )
            let leer_bel.pers_nam =  dglobal.dpers_nam
            let leer_bel.kun =  dglobal.dkunfil
            let leer_bel.hkun = dglobal.dhkunfil
            let leer_bel.delstatus = 0
       end
// 100106 E

	if xzug  <> 0
	    let obueberhaupt= 1
// 100106 dkunfil->dhkunfil
	    execute from sql
	       select #kun_leerg.% from kun_leerg 
		where kun_leerg.a = $xa
                  and kun_leerg.kun = $dglobal.dhkunfil
		  and kun_leerg.kun_fil = $dglobal.dkun_fil
		  and kun_leerg.mdn = $dglobal.dmdn
	    end
	    let kun_leerg.lief_dat = SYSDATE
	    let kun_leerg.ben_lief = dglobal.dpers_nam
	end

	if xabg  <> 0
	    if obueberhaupt = 0
		let obueberhaupt = 1
// 100106 dkunfil->dhkunfil
	    	execute from sql
	           select #kun_leerg.% from kun_leerg 
		    where kun_leerg.a = $xa
                      and kun_leerg.kun = $dglobal.dhkunfil
		      and kun_leerg.kun_fil = $dglobal.dkun_fil
		      and kun_leerg.mdn = $dglobal.dmdn
	        end
	    end
	    let kun_leerg.ret_dat = SYSDATE
	    let kun_leerg.ben_ret = dglobal.dpers_nam
	end
	/* mal bissel unscharf .. */

        if ( obueberhaupt = 1 )

           let leer_bel.bestand = kun_leerg.bsd_stk     // 100106

	   if kun_leerg.bsd_stk <> 0
	      let kun_leerg.bsd_wrt = kun_leerg.bsd_wrt * 
		(kun_leerg.bsd_stk + xzug - xabg)
	      let kun_leerg.bsd_wrt = kun_leerg.bsd_wrt / kun_leerg.bsd_stk
	   end
	   let kun_leerg.bsd_stk = kun_leerg.bsd_stk + xzug - xabg
// 100106 dkunfil->dhkunfil
    	   execute from sql
	    update  kun_leerg set  @kun_leerg.%
		where kun_leerg.a = $xa
                  and kun_leerg.kun = $dglobal.dhkunfil
       	          and kun_leerg.kun_fil = $dglobal.dkun_fil
		  and kun_leerg.mdn = $dglobal.dmdn
	   end

// 100106 A
            if dlekto
                if ( xzug <> 0 )
                    execute from sql
                        insert into leer_bel (%leer_bel.*) values ($leer_bel.%)
                    end
                end
                let leer_bel.bestand = leer_bel.bestand + xzug
                let leer_bel.anzahl = 0 - xabg
                if ( xabg <> 0 )
                    execute from sql
                        insert into leer_bel (%leer_bel.*) values ($leer_bel.%)
                    end
                end
            end
// 100106 E

	end
end

/** -----------------------------------------------------------------------
-	Procedure	: leer_lsdrins
-	In		: a , bsd ,zug,abg
-			  -
-	Out		: -
-	Beschreibung	: Anlegen eines entsprechenden Satzes in leer_lsdr
----------------------------------------------------------------------- **/
procedure leer_lsdrins
PARAMETER
    field xa like leer_lsdr.a
    field xbsd	integer
    field xzug	integer
    field xabg	integer
END

	let leer_lsdr.mdn = dglobal.dmdn
	let leer_lsdr.fil = dglobal.dfil
	let leer_lsdr.ls = dglobal.dls
	let leer_lsdr.blg_typ = "L" 
	let leer_lsdr.a = xa
	let leer_lsdr.stat = 0
	let leer_lsdr.stk = xbsd 
	let leer_lsdr.me_stk_zu = xzug
	let leer_lsdr.me_stk_abn = xabg

	execute from sql
		insert into leer_lsdr (%leer_lsdr.*) values ($leer_lsdr.%)
	end
        if not sqlstatus and draiff
             let draiffzeil = draiffzeil + 1
        end

	if sqlstatus	/* darf eigentlich nicht sein */

		execute from sql
		    update leer_lsdr  set @leer_lsdr.%  
			where 
			leer_lsdr.ls = $dglobal.dls and 
			leer_lsdr.blg_typ = "L"  and
			leer_lsdr.mdn = $dglobal.dmdn and
			leer_lsdr.a = $xa
		end
	end
end

/** -----------------------------------------------------------------------
-	Procedure	: leer_lsdrupd
-	In		: a , bsd ,zug,abg
-			  -
-	Out		: -
-	Beschreibung	: Updaten eines entsprechenden Satzes in leer_lsdr
----------------------------------------------------------------------- **/
procedure leer_lsdrupd
PARAMETER
    field xa like leer_lsdr.a
    field xbsd	integer
    field xzug	integer
    field xabg	integer
END

	if (( leer_lsdr.a = xa ) and (leer_lsdr.stk = xbsd) and 
		( leer_lsdr.me_stk_zu = xzug ) and (leer_lsdr.me_stk_abn = xabg ))
		return
	end
	execute from sql
	    update leer_lsdr
		 set	leer_lsdr.me_stk_zu = $xzug,
			leer_lsdr.me_stk_abn = $xabg,
			leer_lsdr.stk = $xbsd   
			where 
			leer_lsdr.ls = $dglobal.dls and 
			leer_lsdr.blg_typ = "L"  and
			leer_lsdr.mdn = $dglobal.dmdn and
			leer_lsdr.a = $xa
	end
end

/** -----------------------------------------------------------------------
-	Procedure	: entlastung
-	In		: 
-			  -
-	Out		: -
-	Beschreibung	: Bedingte Rueckbuchung der Werte wegen Fakturierung 
-       120809 : Es sollten bereits vorher die globalen Variablen zum
-       aktuellen Beleg gelaufen sein, sonst gibt es ja nichts zum entlasten
----------------------------------------------------------------------- **/
PROCEDURE entlastung 
PARAMETER 
field dmdn 	like kun_leerg.mdn
field dfil	like kun_leerg.fil
field dkun_fil	like kun_leerg.kun_fil
field dkun	like kun_leerg.kun
field dnummer   integer      // 120809
END
   field intpointer	integer 
   field bsd_neu	integer
   field da like kun_leerg.a
   field buchmenge like kun_leerg.bsd_stk
   field buchpreis like kun_leerg.bsd_wrt

	if daktivity <> 2 
		return	/* nur, falls Entlastung angesagt ist */
	end
	if dmenue <> CKBEARBEITEN
		return	/* nicht bei Nachdruck */
	end

        let dkun =  dglobal.dhkunfil     // 120809 : wegen hkun-Mechanik

	let intpointer = 0
	while intpointer < MAXLEERG
	    if (leer_int[intpointer].a < 0.5)
		let intpointer = intpointer + 1
		continue
	    end

            if ( alles_weg = 0 )         // 120809 : falls aktiv,
                                        // dann auch 0-Preis entlasten
                if ((leer_int[intpointer].preis <   0.001 ) and
                    (leer_int[intpointer].preis >  -0.001 ))
                    let intpointer = intpointer + 1
                    continue
                end
	    end

	    let bsd_neu = leer_int[intpointer].zug - leer_int[intpointer].abg

	    if bsd_neu = 0
		let intpointer = intpointer + 1
		continue
	    end

	    let da = leer_int[intpointer].a

	    let buchmenge = 0 - bsd_neu 
	    let buchpreis = buchmenge * leer_int[intpointer].preis

	   if ( buchmenge > 0 )
		let kun_leerg.lief_dat = SYSDATE
		execute from sql
		update kun_leerg
		    set kun_leerg.bsd_stk = (kun_leerg.bsd_stk + $buchmenge),
		    kun_leerg.bsd_wrt = (kun_leerg.bsd_wrt + $buchpreis) ,
		    kun_leerg.lief_dat = $kun_leerg.lief_dat
		where 
			kun_leerg.a =  $da and
			kun_leerg.kun = $dkun and
			kun_leerg.kun_fil = 0 and
			kun_leerg.mdn = $dmdn
    		end
	    else
		let kun_leerg.ret_dat = SYSDATE
    		execute from sql
    		update kun_leerg
		    set kun_leerg.bsd_stk = (kun_leerg.bsd_stk + $buchmenge),
		    kun_leerg.bsd_wrt = (kun_leerg.bsd_wrt + $buchpreis) ,
		    kun_leerg.ret_dat = $kun_leerg.ret_dat
		where 
			kun_leerg.a =  $da and
			kun_leerg.kun = $dkun and
			kun_leerg.kun_fil = 0 and
			kun_leerg.mdn = $dmdn
    		end

   	    end

// 120809 A
// die global-Werte sollten bereits passen, weil vorher mit eleer_aktual das
// Leergut aufgebaut wurde

            if ( dlekto )
breakpoint
                init table leer_bel
                let leer_bel.mdn =  dglobal.dmdn
                let leer_bel.kun_fil = dglobal.dkun_fil
                let leer_bel.nr =  dnummer
                let leer_bel.blg_typ =  "F"     // "R" ist fuer Retouren besetzt
                let leer_bel.a = da
                let leer_bel.anzahl = buchmenge 
                let leer_bel.bestand = 0
//              // lese den letzten Bestand 
                execute into leer_bel.bestand from sql
                  select kun_leerg.bsd_stk from  kun_leerg
                   where 
			kun_leerg.a =  $da and
			kun_leerg.kun = $dkun and
			kun_leerg.kun_fil = 0 and
			kun_leerg.mdn = $dmdn
                end
                let leer_bel.bestand = leer_bel.bestand + buchmenge 
// 
                let leer_bel.datum = SYSDATE
                gettime ( leer_bel.zeit )
                let leer_bel.pers_nam =  dglobal.dpers_nam
                let leer_bel.kun =  dglobal.dkunfil
                let leer_bel.hkun = dglobal.dhkunfil
                let leer_bel.delstatus = 0

            if ( buchmenge <> 0 )
                execute from sql
                    insert into leer_bel (%leer_bel.*) values ($leer_bel.%)
                end
            end
        end
// 120809 E

	    let intpointer = intpointer + 1

	end	/* while in der Matrix */

if "" = "@(#) mo_leerdr	2.0	20.06.95	by JG" end

end
		
