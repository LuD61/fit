/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbzercp
-
-	Autor			:	Juergen Simmchen/f.Knippel
-	Erstellungsdatum	:	21.03.90/20-08-90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    21.03.90   Simmchen	Modularisierung
-	#2	1.00	09-04-90   Knippel 	Anpassung Zerlegepartiezusammenst.
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle zercp
-
-
------------------------------------------------------------------------------
*/

module mo_dbzec

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws
	proc prepcuu_zercp
	proc prepcus_zercp
	proc recread_zercp
	proc recupdate_zercp
	proc  fetch_cus_zercp
	proc  recdel_zercp
end

/*----- Import Section ------------------------------------------------------*/



IMPORT module mo_cons
	field CENSQL100
	field CRDFIRST
	field CRDNEXT
	field CTRUE  
	field CFALSE 
END



database bws
	table zercp end
end



/*---------------------------------------------------------------------------
-									  
-	Procedure	:prepcuu_zercp 
-
-	In		: -
-
-	Out		: 
-			  
-	Beschreibung    : 
-			Alle Saetze zur Partie in Zera 
---------------------------------------------------------------------------*/

PROCEDURE prepcuu_zercp
parameter 
	field 	dzer_part like zercp.zer_part
end
	prepare cursor cuu_zercp from sql
		select #zercp.* from zercp
		where  zercp.zer_partcp = $dzer_part
		and @mdn
		for update 
		end
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	:prepcus_zercp 
-
-	In		: -
-
-	Out		: 
-			  
-	Beschreibung    : 
-			Alle Saetze zur Partie in Zera 
---------------------------------------------------------------------------*/

PROCEDURE prepcus_zercp
parameter 
	field 	dmdn like zercp.mdn
	field 	dzer_part like zercp.zer_part
end
	prepare scroll cursor cus_zercp from sql
		select #zercp.* from zercp
		where  zercp.zer_partcp = $dzer_part
		and  zercp.mdn = $dmdn
		end

	return (sqlstatus )
END




/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_zercp
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
-			lese einen Datensatz fuer Update				
---------------------------------------------------------------------------*/

PROCEDURE recread_zercp
	fetch cursor cuu_zercp
	return (sqlstatus )
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_zercp
-
-	In		: -
-[
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
-		Update des Datensatzes 
---------------------------------------------------------------------------*/


PROCEDURE recupdate_zercp

	execute from sql 
		update zercp
		set @zercp.*
		where current of cuu_zercp
	end
	if sqlstatus =0 
		close cursor cuu_zercp
	end 

	return (sqlstatus)
END
/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdel_zercp
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
-		delete alle Datensaezte, die nichtmehr gebraucht werden 
---------------------------------------------------------------------------*/


PROCEDURE recdel_zercp
parameter
	field 	dmdn like zercp.mdn
	field 	dzer_partcp like zercp.zer_part
end

	execute from sql
		delete from zercp 
		where zercp.zer_partcp = $dzer_partcp
		and @mdn
	end
	return (sqlstatus )
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetch_cus_zercp
-
-	In		: - dnext
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
---------------------------------------------------------------------------*/


PROCEDURE fetch_cus_zercp
parameter
	field dmod	smallint
end
	if dmod  = CRDFIRST
		fetch first cursor cus_zercp
	end
	if dmod  = CRDNEXT 
		fetch next cursor cus_zercp
	end

	return ( sqlstatus )
END




procedure what_kennung
if "" = "@(#) mo_dbzercp.rsf	1.400	05.11.91	by AR" end
end
