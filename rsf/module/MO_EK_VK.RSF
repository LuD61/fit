module mo_ek_vk

/***
--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       mo_ek_vk.rsf
-
        Exports                 :
                                        proc fetch_ek_vk


-       Interne Prozeduren      :  
                                        
-
-       Autor                   :       Wille
-       Erstellungsdatum        :       03.11.92
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   EK und VK aus 
                                    a_pr_holen.
-
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1305   1.400   22.01.93    RO		Aktionsplanung bei 
                                                Lieferpreisen.

-       
--------------------------------------------------------------------------------
***/

/** Exports                   **/

export
        proc fetch_ek_vk              /* #1005 */
        proc hole_aktion  
        proc hole_gueltig
        proc hole_gueltig_lief       /* #1305 */
end

import module mo_grupp 
end

database bws
table a_pr end
table akt_krz end
table akt_kopf end
table akt_pos end
end

field  ek_vk_prep             smallint 
field  akt_ek_vk_prep         smallint 
field  waehr_prim             char (1) value "1"        // 101001


procedure fetch_ek_vk 
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       fetch_ek_vk
-
-       In              :       mgruppe
                                mandant
                                fgruppe 
                                filiale
                                artikel
-
-       Out             :       pr_ek
                                pr_vk
-
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Preis aus a_pr holen.
                                Wenn Sonderaktionen existieren, werden
                                die Preise aus akt_krz zurueckgegeben,
                                sonst die Preise aus a_pr
                                a_pr wird nicht veraendert.
-
--------------------------------------------------------------------------------
**/
parameter
field  mgruppe            smallint
field  mandant            smallint
field  fgruppe            smallint
field  filiale            smallint
field  artikel            decimal (13 0)
end

       table apr           like a_pr end
       table aktkrz        like akt_krz end
       field prek          decimal (8 4)
       field prvk          decimal (6 2) 
       field prek_eu       decimal (8 4)                // 101001
       field prvk_eu       decimal (6 2)                // 101001
       field sa            smallint

if "" = "@(#) mo_ek_vk.rsf	1.400	22.01.93	by Ro" end
       init prek prvk
       init prek_eu prvk_eu             // 101001

// 101001
       if mandant > 0
            execute into waehr_prim from sql
                    select waehr_prim from mdn
                    where mdn = $mandant
            end
       end     

/* Cursor fuer a_pr vorbereiten                     */

       if ek_vk_prep = 0
// 101001 : diverse euro-felder dazu
            prepare cursor ek_vk 
               into apr.mdn_gr,apr.mdn, apr.fil_gr,apr.fil,
                    apr.akt,apr.lad_akv,
                    apr.pr_ek, apr.pr_vk,
                    apr.pr_ek_euro, apr.pr_vk_euro, 
                    apr.a
               from sql
               select a_pr.mdn_gr,a_pr.mdn, a_pr.fil_gr,a_pr.fil,
                    a_pr.akt,a_pr.lad_akv,
                    a_pr.pr_ek, a_pr.pr_vk,
                    a_pr.pr_ek_euro, a_pr.pr_vk_euro, 
                    a_pr.a
               from a_pr
               where a_pr.a     = $artikel
               and (a_pr.mdn_gr = $mgruppe
               or   a_pr.mdn_gr = 0)
               and (a_pr.mdn    = $mandant
               or   a_pr.mdn    = 0)
               and (a_pr.fil_gr = $fgruppe
               or   a_pr.fil_gr = 0)
               and (a_pr.fil    = $filiale
               or   a_pr.fil    = 0)
            end
            let ek_vk_prep = 1
       else
            close cursor ek_vk
       end

/* Cursor fuer akt_krz vorbereiten                     */
// 101001 : diverse euro-Felder dazu
       if akt_ek_vk_prep = 0
            prepare cursor akt_ek_vk
              into aktkrz.pr_ek_sa, aktkrz.pr_vk_sa, 
                   aktkrz.pr_ek_sa_euro, aktkrz.pr_vk_sa_euro 
              from sql 
              select akt_krz.pr_ek_sa, akt_krz.pr_vk_sa,
                     akt_krz.pr_ek_sa_euro, akt_krz.pr_vk_sa_euro
              from akt_krz
              where akt_krz.mdn_gr = $apr.mdn_gr
              and   akt_krz.mdn    = $apr.mdn
              and   akt_krz.fil_gr = $apr.fil_gr
              and   akt_krz.fil    = $apr.fil
              and   akt_krz.a      = $apr.a
              and   akt_krz.lad_akv_sa = "1"
            end
            let akt_ek_vk_prep = 1
       else
            close cursor akt_ek_vk
       end

/* Gruppen holen                           */

       if mgruppe = -1 
                     let mgruppe = proc fetch_mdn_gr (mandant)
       end
       if fgruppe = -1 
                     let fgruppe = proc fetch_fil_gr (mandant filiale)
       end

/* Preise lesen                                      */

       fetch cursor ek_vk
       if sqlstatus
                    return (prek prvk 0)
       end

       if apr.pr_ek = -1
                    let sqlstatus = 100
                    return (prek prvk 0)
       end

       let prek = apr.pr_ek
       let prvk = apr.pr_vk
       let prek_eu = apr.pr_ek_euro             // 101001
       let prvk_eu = apr.pr_vk_euro             // 101001

// 101001
       if waehr_prim = "2"
               let prek = prek_eu
               let prvk = prvk_eu
       end

       init sa
       if apr.lad_akv = "1"
             if apr.akt = 0
                    fetch cursor akt_ek_vk
                    if sqlstatus = 0
                               let prek = aktkrz.pr_ek_sa  
                               let prvk = aktkrz.pr_vk_sa
// 101001 A
                               let prek_eu = aktkrz.pr_ek_sa_euro  
                               let prvk_eu = aktkrz.pr_vk_sa_euro 
                               if waehr_prim = "2"
                                        let prek = prek_eu
                                        let prvk = prvk_eu
                               end
// 101001 E
                               let sa = 1           
                    end
             end
             if apr.akt > 0
                    perform hole_aktion (apr.akt apr.a prek prvk)
                              returning (prek prvk sa)
             end
       end

       if apr.akt = -1
             perform hole_gueltig (apr.mdn_gr apr.mdn apr.fil_gr apr.fil
                                   apr.a 
                                   prek prvk)
                        returning (prek prvk sa)
       end

       if prek = 0 and prvk = 0
                    if filiale > 0
                               perform fetch_ek_vk
                                       (mgruppe mandant fgruppe 0 artikel)
                               returning (prek prvk sa)
                    elseif fgruppe > 0
                               perform fetch_ek_vk
                                       (mgruppe mandant 0 0 artikel)
                               returning (prek prvk sa)
                    elseif mandant > 0
                               perform fetch_ek_vk
                                       (mgruppe 0 0 0 artikel)
                               returning (prek prvk sa)
                    elseif mgruppe > 0
                               perform fetch_ek_vk
                                       (0 0 0 0 artikel)
                               returning (prek prvk sa)
                    end
       end
       return (prek prvk sa)
end

/* #1005 E */

procedure hole_aktion
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       hole_aktion
-
-       In              :       aktion
                                artikel
                                prek
                                prvk
-
-       Out             :       prek
                                prvk
                                sa
-
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Aktionspreis holen.
-
--------------------------------------------------------------------------------
**/
parameter
field   aktion      like a_pr.akt
field   artikel     like a_pr.a
field   prek        like a_pr.pr_ek
field   prvk        like a_pr.pr_vk
end

        field ek   like a_pr.pr_ek
        field vk   like a_pr.pr_vk
        field ek_eu  like a_pr.pr_ek_euro          // 101001
        field vk_eu  like a_pr.pr_vk_euro          // 101001

        execute into
                  ek, vk, ek_eu, vk_eu from sql
                  select akt_pos.pr_ek_sa, akt_pos.pr_vk_sa,
                        akt_pos.pr_ek_sa_euro, 
                        akt_pos.pr_vk_sa_euro
                   from akt_pos
                  where akt_pos.akt = $aktion
                  and   akt_pos.a   = $artikel
        end
        if sqlstatus = 0
//101001
                  if waehr_prim = "2"
                          return (ek_eu vk_eu 1)
                  end
                  return (ek vk 1)
        end

        return (prek prvk 0)
end

procedure hole_gueltig
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       hole_gueltig
-
-       In              :       mgruppe
                                mandant 
                                fgruppe 
                                filiale
                                artikel 
                                prek
                                prvk
-
-       Out             :       prek
                                prvk
                                sa
-
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Aktionspreis von hoeherer Ebene mit als
                                gueltig gekennzeichneter Aktion holen.
-
--------------------------------------------------------------------------------
**/
parameter
field mgruppe like a_pr.mdn_gr
field mandant like a_pr.mdn
field fgruppe like a_pr.fil_gr
field filiale like a_pr.fil
field artikel like a_pr.a
field prek    like a_pr.pr_ek
field prvk    like a_pr.pr_vk
end

        field aktion  like a_pr.akt
        field ek      like a_pr.pr_ek
        field vk      like a_pr.pr_vk
        field ek_eu   like a_pr.pr_ek_euro              // 101001 
        field vk_eu   like a_pr.pr_vk_euro              // 101001
        field sa      smallint

        prepare cursor akt_kopf_rd
                  into aktion from sql
                  select akt_kopf.akt from akt_kopf
                  where akt_kopf.mdn    = $mandant
                  and   akt_kopf.fil_gr = $fgruppe
                  and   akt_kopf.fil    = $filiale
                  and   akt_kopf.lad_akv_sa = "1"
                  and   akt_kopf.gue        = "J"
         end

         repeat
                  if filiale > 0
                            init filiale
                  elseif fgruppe > 0
                            init fgruppe
                  elseif mandant > 0
                            init mandant
                  else
                            return (prek prvk 0)
                  end

                  close cursor akt_kopf_rd
                  fetch cursor akt_kopf_rd 
        until sqlstatus <> 100 
        if sqlstatus
                 erase cursor akt_kopf_rd
                 return (prek prvk 0)
        end
        perform hole_aktion (aktion artikel prek prvk)
                 returning (ek vk sa)
        erase cursor akt_kopf_rd
        return (ek vk sa)
end

/* #1305 A */

procedure hole_gueltig_lief
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       hole_gueltig_lief
-
-       In              :       mgruppe
                                mandant 
                                fgruppe 
                                filiale
                                artikel 
                                prek
                                prvk
-
-       Out             :       prek
                                prvk
                                sa
-
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Aktionspreis von hoeherer Ebene mit als
                                gueltig gekennzeichneter Aktion holen.
-
--------------------------------------------------------------------------------
**/
parameter
field mgruppe like a_pr.mdn_gr
field mandant like a_pr.mdn
field fgruppe like a_pr.fil_gr
field filiale like a_pr.fil
field artikel like a_pr.a
field prek    like a_pr.pr_ek
field prvk    like a_pr.pr_vk
end

        field aktion  like a_pr.akt
        field ek      like a_pr.pr_ek
        field vk      like a_pr.pr_vk
        field ek_eu   like a_pr.pr_ek_euro      // 101001 
        field vk_eu   like a_pr.pr_vk_euro      // 101001
        field sa      smallint

        prepare cursor akt_kopf_rd
                  into aktion from sql
                  select akt_kopf.akt from akt_kopf
                  where akt_kopf.mdn    = $mandant
                  and   akt_kopf.fil_gr = $fgruppe
                  and   akt_kopf.fil    = $filiale
                  and   akt_kopf.lief_akv_sa = "1"
                  and   akt_kopf.gue        = "J"
         end

         repeat
                  if filiale > 0
                            init filiale
                  elseif fgruppe > 0
                            init fgruppe
                  elseif mandant > 0
                            init mandant
                  else
                            return (prek prvk 0)
                  end

                  close cursor akt_kopf_rd
                  fetch cursor akt_kopf_rd 
        until sqlstatus <> 100 
        if sqlstatus
                 erase cursor akt_kopf_rd
                 return (prek prvk 0)
        end
        perform hole_aktion (aktion artikel prek prvk)
                 returning (ek vk sa)
        erase cursor akt_kopf_rd
        return (ek vk sa)
end

/* #1305 E */

