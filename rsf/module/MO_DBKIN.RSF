
/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbkinfo
-
-	Interne Operatoren	:	proc prepupdate_kun_info
-					proc prepread_kun_info
-					proc putmxkey_kun_info
-					proc recupdate_kun_info
-					proc recdelete_kun_info
-					proc recinsert_kun_info
-					proc recread_kun_info
-					proc getmxkey_kun_info
-					proc recreaddef_kun_info
-					proc inittable_kun_info
-					proc initkey_kun_info
-					proc cmpkey_kun_info
-					proc savekey_kun_info
-					proc backkey_kun_info
-					proc clrdelstatus_kun_info
-					proc getdelstatus_kun_info
-					proc fetchcuu_kun_info
-					proc fetchread_kun_info
-					proc erasecursor_kun_info
-					proc closeread_kun_info
-					proc closeupdate_kun_info
-					proc einfuegen_kun_info
-					proc defmaintab_kun_info
-
-	Externe Operatoren	:	proc mo_meld.disp_sql
-
-	Autor			:	Ruediger Becker / Axel Seichter
-	Erstellungsdatum	:	19.02.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1	1.00	19.02.90   Becker /	Modularisierung
-				   Seichter
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul fuer die zusaetzliche
-				    Verwendung der 
-				    Tabelle kun_info
-
-
------------------------------------------------------------------------------
*/

module mo_dbkin

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_kun_info
	proc prepread_kun_info
	proc putmxkey_kun_info
	proc recupdate_kun_info
	proc recdelete_kun_info
	proc recinsert_kun_info
	proc recread_kun_info
	proc getmxkey_kun_info
	proc recreaddef_kun_info
	proc inittable_kun_info
	proc backkey_kun_info
	proc savekey_kun_info
	proc initkey_kun_info
	proc cmpkey_kun_info
	proc clrdelstatus_kun_info
	proc getdelstatus_kun_info
	proc fetchcuu_kun_info
	proc fetchread_kun_info
	proc erasecursor_kun_info
	proc closeread_kun_info
	proc closeupdate_kun_info
	proc einfuegen_kun_info
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
	field dret
end

IMPORT module mo_cons
end

IMPORT module mo_meld
	proc disp_sql
end

#include "colaenge"

database bws
	table    kun_info	end
end

field dsavekey1 	 like kun_info.mdn 
field dsavekey2 	 like kun_info.fil 
field dsavekey3 	 like kun_info.mdn 
field ddefakey   like kun_info.kun value "-1"
field dcurskey	 like kun_info.kun dimension(CMAXMATRIX)
field dsql	 smallint

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_kun_info
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_kun_info.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_kun_info

	prepare cursor cuu_kun_info from sql
		select #kun_info.% from kun_info
		where @kun_info.mdn and @kun_info.fil and @kun_info.kun 
		for update
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_kun_info
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor cur_kun_info.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_kun_info

	prepare cursor cur_kun_info from sql
		select #kun_info.* from kun_info
		where @kun_info.mdn and @kun_info.fil and @kun_info.kun 
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_kun_info
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_kun_info
parameter field danz	smallint	end

		let dcurskey[danz] = kun_info.kun

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_kun_info
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_kun_info
parameter field dzeig	smallint	end

		let kun_info.kun = dcurskey[dzeig]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_kun_info
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_kun_info gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_kun_info

	execute from sql 
		update kun_info
		set @kun_info.*
		where current of cuu_kun_info
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_kun_info
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_kun_info gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_kun_info

	let kun_info.delstatus = -1

	execute from sql
		update kun_info
		set @kun_info.delstatus
		where current of cuu_kun_info
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_kun_info
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_kun_info

	execute from sql 
		insert into kun_info (%kun_info.*)
		values ($kun_info.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_kun_info
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_kun_info
	execute from sql
		select #kun_info.* from kun_info 
		where @kun_info.mdn and @kun_info.fil and @kun_info.kun 
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_kun_info
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_kun_info

field dsavfil like kun_info.fil 
field dsavmdn like kun_info.mdn 

	let dsavfil = kun_info.fil
	let dsavmdn = kun_info.mdn

	let kun_info.kun = ddefakey

	perform recread_kun_info returning (dsql)
	if dsql = CENSQL100
		if kun_info.fil <> 0
			let kun_info.fil = 0
			perform recread_kun_info returning (dsql)
		end
		if dsql = CENSQL100
			if kun_info.mdn <> 0
				let kun_info.mdn = 0
				perform recread_kun_info returning (dsql)
			end
		end
	end
	let kun_info.fil = dsavfil
	let kun_info.mdn = dsavmdn 
	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_kun_info
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_kun_info

	init table kun_info
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_kun_info
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_kun_info

	init kun_info.kun
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_kun_info
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_kun_info

	if dsavekey3 = kun_info.kun
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_kun_info
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_kun_info

	let kun_info.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_kun_info
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_kun_info

	return (kun_info.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_kun_info
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_kun_info.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_kun_info

	fetch cursor cur_kun_info
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_kun_info 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_kun_info
-			  ,cur_kun_info und cus_kun_info.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_kun_info

	erase cursor cus_kun_info
	erase cursor cuu_kun_info
	erase cursor cur_kun_info
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_kun_info 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_kun_info.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_kun_info

	fetch cursor cuu_kun_info
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_kun_info 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_kun_info. 
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_kun_info

	close cursor cur_kun_info
	return (sqlstatus) 
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeupdate_kun_info 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den update cursor cuu_kun_info. 
-							
---------------------------------------------------------------------------*/

PROCEDURE closeupdate_kun_info

	close cursor cuu_kun_info
	return (sqlstatus) 
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: einfuegen_kun_info
-
-	In			: deinmode	smallint
-						CTRUE  - nicht einfuegen,sondern
-							 delstatus modifizieren
-						CFALSE - wirklich einfuegen
-
-	Out			: -
-
-	Beschreibung		: Die Procedure fuehrt die Aktionen aus,
-				  die fuer das Einfuegen eines Datensatzes
-				  in die Tabelle kun_info erforderlich sind.
-
-------------------------------------------------------------------------*/

PROCEDURE einfuegen_kun_info

	parameter	field	deinmode smallint	end

/*----- Datensatz einfuegen oder nur update ------------------------------*/

	if deinmode = CFALSE
		perform recinsert_kun_info returning (dsql)
		if dsql
			perform disp_sql (dsql)
			let dret = CFALSE
			return
		end
	end

	perform closeupdate_kun_info returning (dsql)

	perform fetchcuu_kun_info returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

/*----- Defaultsatzbesetzung ---------------------------------------------*/

	perform defmaintab_kun_info
	perform clrdelstatus_kun_info

	let dret = CTRUE
	return
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: defmaintab_kun_info
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure besetzt die Tabellenwerte mit
-				  den Defaultwerten oder Kopierwerten
-
-------------------------------------------------------------------------*/

PROCEDURE defmaintab_kun_info

	perform savekey_kun_info

	perform recreaddef_kun_info returning (dsql)
	if dsql 
		perform inittable_kun_info
		perform disp_sql (dsql)
	end

	perform backkey_kun_info
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: savekey_kun_info
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure sichert den Wert des
-				  Schluesselfeldes.
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_kun_info

	let dsavekey1 = kun_info.mdn
	let dsavekey2 = kun_info.fil
	let dsavekey3 = kun_info.kun
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: backkey_kun_info
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure schreibt den Wert in das 
-				  Schluesselfeld zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_kun_info

	let kun_info.mdn = dsavekey1
	let kun_info.fil = dsavekey2
	let kun_info.kun = dsavekey3
END
procedure what_kennung
if "" = "@(#) mo_dbkinfo.rsf	1.400	05.11.91	by RO>" end
end
