/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbtbsk
-
-	Interne Operatoren	:	proc prepupdate_tbsk
-					proc prepread_tbsk
-					proc putmxkey_tbsk
-					proc recupdate_tbsk
-					proc recdelete_tbsk
-					proc recinsert_tbsk
-					proc recread_tbsk
-					proc getmxkey_tbsk
-					proc recreaddef_tbsk
-					proc inittable_tbsk
-					proc initkey_tbsk
-					proc cmpkey_tbsk
-					proc clrdelstatus_tbsk
-					proc getdelstatus_tbsk
-					proc fetchcuu_tbsk
-					proc fetchread_tbsk
-					proc erasecursor_tbsk
-					proc closeread_tbsk
-					proc closeupdate_tbsk
-					proc einfuegen_tbsk
-					proc defmaintab_tbsk
-					proc savekey_tbsk
-					proc backkey_tbsk
-
-	Externe Operatoren	:	-
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	22.02.90
-	Modifikationsdatum	:	13.06.90
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1	1.00	22.02.90   Simmchen	Modularisierung
-	#2	1.00	06.06.90   B.Menge      fuer Schriftarten
-	#3	1.00	13.06.90   B.Menge      fuer TBS
-       #1143   1.400   17.11.92   GF  Anpassung an neue Items            
-       #1195   1.400   09.12.92   GF  Aenderung der Sql-Statements          
-       #1383   1.400   23.02.93   GF  * durch % in SQL ersetzen             
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle tbsk
-
-
------------------------------------------------------------------------------
*/

module mo_dbtbs

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_tbsk
	proc prepread_tbsk
	proc putmxkey_tbsk
	proc recupdate_tbsk
	proc recdelete_tbsk
	proc recinsert_tbsk
	proc recread_tbsk
	proc getmxkey_tbsk
	proc recreaddef_tbsk
	proc inittable_tbsk
	proc backkey_tbsk
	proc savekey_tbsk
	proc initkey_tbsk
	proc cmpkey_tbsk
	proc clrdelstatus_tbsk
	proc getdelstatus_tbsk
	proc fetchcuu_tbsk
	proc fetchread_tbsk
	proc erasecursor_tbsk
	proc closeread_tbsk
	proc closeupdate_tbsk
	proc einfuegen_tbsk
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
	field dret
end

IMPORT module mo_cons
end

IMPORT module mo_meld
	proc disp_sql
	proc disp_msg
end

#include "colaenge"

database bws
	table    tbs_kopf end
end

field dsavekey 	 like tbs_kopf.tbs 
field ddefakey   like tbs_kopf.tbs value "-1"
field dcurskey	 like tbs_kopf.tbs dimension(CMAXMATRIX)
field dsql	 smallint

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_tbsk
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_tbsk.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_tbsk

if "" = "@(#) mo_dbtbsk.rsf	1.400	24.02.93	by GF" end
	prepare cursor cuu_tbsk from sql
		select #tbs_kopf.% from tbs_kopf
		where @tbs_kopf.tbs for update
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_tbsk
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor cur_tbsk.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_tbsk

/* A 1383 */
	prepare cursor cur_tbsk from sql
		select #tbs_kopf.% from tbs_kopf
		where @tbs_kopf.tbs
		end
/* E 1383 */

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_tbsk
-
-	In		: danzahl   smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_tbsk

parameter field danzahl smallint end

	let dcurskey[danzahl] = tbs_kopf.tbs

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_tbsk
-
-	In		: dzeiger smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_tbsk

parameter field dzeiger smallint end

	let tbs_kopf.tbs = dcurskey[dzeiger]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_tbsk
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_tbsk gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_tbsk

/* A 1383 */
	execute from sql 
		update tbs_kopf
		set @tbs_kopf.%
		where current of cuu_tbsk
		end
/* E 1383 */

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_tbsk
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_tbsk gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_tbsk

	let tbs_kopf.delstatus = -1

	execute from sql
		update tbs_kopf
		set @tbs_kopf.delstatus
		where current of cuu_tbsk
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_tbsk
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_tbsk

/* A 1383 */
	execute from sql 
		insert into tbs_kopf (%tbs_kopf.%)
		values ($tbs_kopf.%)
		end
/* E 1383 */

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_tbsk
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_tbsk
/* A 1383 */
	execute from sql
		select #tbs_kopf.% from tbs_kopf where @tbs_kopf.tbs
		end
/* E 1383 */

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_tbsk
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_tbsk

	let tbs_kopf.tbs = ddefakey

	perform recread_tbsk returning (dsql)
	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_tbsk
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_tbsk

	init table tbs_kopf
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_tbsk
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_tbsk

	init tbs_kopf.tbs
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_tbsk
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_tbsk

	if dsavekey = tbs_kopf.tbs
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_tbsk
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_tbsk

	let tbs_kopf.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_tbsk
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_tbsk

	return (tbs_kopf.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_tbsk
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_tbsk.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_tbsk

	fetch cursor cur_tbsk
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_tbsk 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_tbsk
-			  ,cur_tbsk und cus_tbsk.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_tbsk

	erase cursor cus_tbsk
	erase cursor cuu_tbsk
	erase cursor cur_tbsk
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_tbsk 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_tbsk.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_tbsk

	fetch cursor cuu_tbsk
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_tbsk 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_tbsk. 
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_tbsk

	close cursor cur_tbsk
	return (sqlstatus) 
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeupdate_tbsk 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den update cursor cuu_tbsk. 
-							
---------------------------------------------------------------------------*/

PROCEDURE closeupdate_tbsk

	close cursor cuu_tbsk
	return (sqlstatus) 
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: einfuegen_tbsk
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure fuehrt die Aktionen aus,
-				  die fuer das Einfuegen eines Datensatzes
-				  in die Tabelle tbs_kopf.erforderlich sind.
-
-------------------------------------------------------------------------*/

PROCEDURE einfuegen_tbsk

/*----- Datensatz einfuegen oder nur update ------------------------------*/

	perform recinsert_tbsk returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

	perform closeupdate_tbsk returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

	perform fetchcuu_tbsk returning (dsql)
	if dsql
		perform disp_sql (dsql)
		let dret = CFALSE
		return
	end

/*----- Defaultsatzbesetzung ---------------------------------------------*/

	perform defmaintab_tbsk
	perform clrdelstatus_tbsk

	return
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: defmaintab_tbsk
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure besetzt die Tabellenwerte mit
-				  den Defaultwerten oder Kopierwerten
-
-------------------------------------------------------------------------*/

PROCEDURE defmaintab_tbsk

	perform savekey_tbsk

	perform recreaddef_tbsk returning (dsql)
	if dsql 
		perform inittable_tbsk
		perform disp_sql (dsql)
	end

	perform backkey_tbsk
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: savekey_tbsk
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure sichert den Wert des
-				  Schluesselfeldes.
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_tbsk

	let dsavekey = tbs_kopf.tbs
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: backkey_tbsk
-
-	In			: -
-
-	Out			: -
-
-	Beschreibung		: Die Procedure schreibt den Wert in das 
-				  Schluesselfeld zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_tbsk

	let tbs_kopf.tbs = dsavekey
END
