/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbzera
-
-	Autor			:	Friedhelm Knippel 
-	Erstellungsdatum	:	29.05.90
-	Modifikationsdatum	:	29.05.90
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-       #1143    1.400    16.11.92  AR   Item-Tabellen-Aenderung zera.a_kz
------------------------------------------------------------------------------
-
-	Modulbeschreibung	: 
-		Alle Proceduren dieses Moduls sind als 
-		Datenbankmodul speziell fuer die Tabelle
-				Zerlepartie Ausgangs-Material (zera )
-
-	Verwendung in 
-					zer_part	Zerlegepartie
-
------------------------------------------------------------------------------
*/

module  mo_dbzea

EXPORT
	database bws
	proc prepcus_zera_all 
	proc prepcuu_zera 
	proc prepcus_tazera 
	proc recread_zera
	proc recupdate_zera
	proc recdel_zera_all
	proc recdel_zera
	proc clear_zera
	proc recins_zera  
	proc fetch_cus_zera
	proc fetch_cus_tazera

	proc tstzera_mat 

	table tazera
end

IMPORT module mo_cons
	field CENSQL100
	field CRDFIRST
	field CRDNEXT
	field CTRUE  
	field CFALSE 
END


database bws
	table zera   end
end

table  tazera like zera end



/*---------------------------------------------------------------------------
-									  
-	Procedure	:prepcus_zera_all 
-
-	In		: -
-
-	Out		: 
-			  
-	Beschreibung    : 
-			Alle Saetze zur Partie in Zera 
---------------------------------------------------------------------------*/

PROCEDURE prepcus_zera_all
parameter 
	field 	dmdn  like zera.mdn 
	field 	dzer_part like zera.zer_part
end
	prepare scroll cursor cus_zera_all from sql
                select #zera.% from zera
		where  zera.zer_part = $dzer_part
		and   zera.mdn = $dmdn
		end
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	:prepcus_tazera
-
-	In		: -
-
-	Out		: 
-			  
-	Beschreibung    : 
-			Alle Saetze zur Partie in Zera 
---------------------------------------------------------------------------*/

PROCEDURE prepcus_tazera
parameter 
	field 	dmdn like zera.mdn
	field 	dzer_part like zera.zer_part
end
	prepare scroll cursor cus_tazera
        into 
                 tazera.mdn,
                 tazera.zer_part,
                 tazera.mat,
                 tazera.a_bz1,
                 tazera.mat_o_b,
                 tazera.mat_pr_vk,
                 tazera.mat_pr_tk,
                 tazera.plan_gew,
                 tazera.soll_gew,
                 tazera.delstatus,
                 tazera.kng,
                 tazera.a_kz,
                 tazera.schnitt,
                 tazera.zera_gew_ant
	from sql
                select
                 mdn,
                 zer_part,
                 mat,
                 a_bz1,
                 mat_o_b,
                 mat_pr_vk,
                 mat_pr_tk,
                 plan_gew,
                 soll_gew,
                 delstatus,
                 kng,
                 a_kz,
                 schnitt,
                 zera_gew_ant


                 from zera
		where  zera.zer_part = $dzer_part
		and zera.mdn = $dmdn 
		end
return (sqlstatus )
END


/*---------------------------------------------------------------------------
-									  
-	Procedure	:prepcuu_zera 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
-			Update cursor in zera
---------------------------------------------------------------------------*/

PROCEDURE prepcuu_zera
	prepare cursor cuu_zera from sql
		select #zera.* from zera
		where @zera.zer_part 
		and   @zera.mat
		and   @zera.mdn
		and   @zera.schnitt
		for update
		end
END


/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_zera
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
-			lese einen Datensatz fuer Update				
---------------------------------------------------------------------------*/

PROCEDURE recread_zera
	fetch cursor cuu_zera
	return (sqlstatus )
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_zera
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
-		Update des Datensatzes 
---------------------------------------------------------------------------*/


PROCEDURE recupdate_zera

	execute from sql 
		update zera
		set @zera.*
		where current of cuu_zera
	end
	if sqlstatus =0 
		close cursor cuu_zera
	end 

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdel_zera_all
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
-		delete alle Datensaezte, die nichtmehr gebraucht werden 
---------------------------------------------------------------------------*/


PROCEDURE recdel_zera_all
parameter
	field 	dmdn like zera.mdn
	field 	dzer_part like zera.zer_part
end

	execute from sql
		delete from zera 
		where zera.zer_part = $dzer_part 
		and zera.mdn = $dmdn
	end
	return (sqlstatus )
END


/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdel_zera
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
-		delete alle Datensaezte, die nichtmehr gebraucht werden 
---------------------------------------------------------------------------*/


PROCEDURE recdel_zera
parameter
	field 	dmdn like zera.mdn
	field 	dzer_part like zera.zer_part
end

	execute from sql
		delete from zera 
		where zera.zer_part = $dzer_part 
		  and zera.mdn = $dmdn 
		  and zera.soll_gew = 0
		  and zera.plan_gew = 0
	end
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: clear_zera
-
-	In		: -
-
-	Out		: 
-			  
-	Beschreibung    : 
-			setzt die Werte auf Null, die neu bestimmt werden 
---------------------------------------------------------------------------*/


PROCEDURE clear_zera
	let zera.soll_gew = 0
	let zera.plan_gew = 0
END



/*---------------------------------------------------------------------------
-									  
-	Procedure	: recins_zera
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
-			einen Datensatz in Tabelle zera einfuegen 
---------------------------------------------------------------------------*/


PROCEDURE recins_zera  
	execute from sql
		insert into zera ( %zera.* ) values ( $zera.* )
	end
	return ( sqlstatus )
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetch_cus_zera
-
-	In		: - dnext
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
---------------------------------------------------------------------------*/


PROCEDURE fetch_cus_zera
parameter
	field dmod	smallint
end
	if dmod  = CRDFIRST
		fetch first cursor cus_zera_all
	end
	if dmod  = CRDNEXT 
		fetch next cursor cus_zera_all
	end

	return ( sqlstatus )
END


/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetch_cus_tazera
-
-	In		: - dnext
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : 
---------------------------------------------------------------------------*/


PROCEDURE fetch_cus_tazera
parameter
	field dmod	smallint
end
	if dmod  = CRDFIRST
		fetch first cursor cus_tazera
	end
	if dmod  = CRDNEXT 
		fetch next cursor cus_tazera
	end

	return ( sqlstatus )
END


/*---------------------------------------------------------------------------
-									  
-	Procedure	: tstzera_mat
-
-	In		: - dmat	Materialnr.
-			  - dzer_part	Partienr.
-
-	Out		: CTRUE/CFALSE - Datensatz  vorhanden 
-			  
-	Beschreibung    : 
-			Prueft ob das Material in der Zerlgepartie vorkommt
---------------------------------------------------------------------------*/

PROCEDURE tstzera_mat 
parameter 
	field 	dmdn      like zera.mdn     
	field 	dmat      like zera.mat     
	field 	dzer_part like zera.zer_part
end

	execute from sql 
		select #zera.a_bz1 ,#zera.mat_pr_vk from zera
		where  zera.zer_part = $dzer_part
		and    zera.mat  = $dmat 
		and    zera.mdn  = $dmdn 
	end
	if sqlstatus 
		init zera.a_bz1
		init zera.mat_pr_vk  
	end 

	return (sqlstatus ,zera.a_bz1 ,zera.mat_pr_vk )
END



procedure what_kennung
if "" = "@(#) mo_dbzera.rsf	1.400	05.11.91	by AR" end
end
