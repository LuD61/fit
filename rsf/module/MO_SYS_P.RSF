module mo_sys_p

/***
--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       syspar_hol
-
-       Interne Prozeduren      :       sys_par_holen
-
-       Externe Prozeduren      :       ---- 
-
-       Autor                   :       Norbert
-       Erstellungsdatum        :       17.08.90
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       1       1.00    TT.MM.JJ                   
-       #1143   1.400 17.11.92    GF  Anpassung an neue Items            
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       : Programm sucht Systemparameter und gibt dessen
-                                 Werte zurueck.
-
--------------------------------------------------------------------------------
***/

export
      proc sys_par_holen
end

/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sys_par_holen
-
-       In              :       sys_par_entry   char(24)    Parametername
-
-       Out             :       sys_par_wert    char(1)     Parameterwert
-                               sys_par_bez     char(32)    Parameterbezeichnung
-                               sys_par_exit    smallint    Ergeb. des Zugriffs:-                                                           0 = nicht gefunden
-                                                           1 = gefunden
-
-       Globals         :       Name            Datentyp        Beschreibung
-
-       Errorcodes      :       Name            Nr.             Beschreibung
-
-       Beschreibung    :       Aufruf: perform sys_par_holen (name)
-                                       returning ( exitcode
-                                                 , sys_par_wert
-                                                 , sys_par_bez )
-                               Die Tabelle sys_par wird angewaehlt!
-
--------------------------------------------------------------------------------
**/
 
data

    field  sys_par_entry   char(24)        /* Einstieg in sys_par             */

    field  sys_par_wert    char(1)         /* Wert des Parameters             */

    field  sys_par_bez     char(32)        /* Bezeichnung des Parameters      */

    field  sys_par_rc      smallint        /* Return_code ( = sqlstatus)      */

    field  sys_par_exit    smallint        /* Ergebnis des Zugriffs:          */
                                           /* 0 = sys_par nicht gefunden      */
                                           /* 1 = sys_par gefunden            */

end /* data */

database bws 
      table sys_par end
end

/*                                                                           */
/*   Systemparameter nach gueltigem Item durchsuchen                         */
/*                                                                           */

proc sys_par_holen

parameter

    field  sys_par_entry   char(24)

end

if "" = "@(#) mo_sys_par.rsf	1.400	17.11.92	by GF" end
      init      sys_par_rc 
              , sys_par_exit
              , sys_par_wert
              , sys_par_bez

      execute from
      sql

              select  #sys_par.%
                from   sys_par
               where   sys_par.sys_par_nam = $sys_par_entry and
                       sys_par.delstatus = 0

      end

      let sys_par_wert = sys_par.sys_par_wrt
      let sys_par_bez  = sys_par.sys_par_besch

      if            sqlstatus    <>   0    and /* wird sowiso abgefangen */
                    sqlstatus    <> 100 

         display    errortext   sql (sqlstatus)

         let        sys_par_rc        =  sqlstatus

         return (
                    sys_par_exit
                  , sys_par_wert
                  , sys_par_bez
                )  
      end

      if            sqlstatus    =  100

         return (
                    sys_par_exit
                  , sys_par_wert
                  , sys_par_bez
                )  
      end

      let sys_par_exit         =    1   /* TRUE */

      return (
                    sys_par_exit
                  , sys_par_wert
                  , sys_par_bez
             )  

end /* proc sys_par_holen */
