/*
-----------------------------------------------------------------------------
-
-       BSA Bizerba Software- und Automationssysteme GmbH
-       Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-       Modulname               :       mo_dbspe
-
-       Interne Operatoren      :       proc prepupdate 
-                                       proc prepread
-                                       proc prepscroll
-                                       proc putmxkey
-                                       proc recupdate
-                                       proc recdelete
-                                       proc recinsert
-                                       proc recread
-                                       proc getmxkey
-                                       proc recreaddef
-                                       proc inittable
-                                       proc savekey
-                                       proc backkey
-                                       proc initkey
-                                       proc cmpkey
-                                       proc clrdelstatus
-                                       proc getdelstatus
-                                       proc getauswst
-                                       proc initform
-                                       proc dispform
-                                       proc fetchcuu
-                                       proc fetchread
-                                       proc erasecursor
-                                       proc closeread
-                                       proc fetchfirst
-                                       proc fetchnext
-                                       proc adrwahl
-
-       Externe Operatoren      :       -
-
-       Autor                   :       Juergen Simmchen / Axel Seichter
-       Erstellungsdatum        :       16.01.90
-       Modifikationsdatum      :       09.02.90
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386 
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd. -  Version Datum       Name        Beschreibung
-       ----------------------------------------------------------------------
-       #1      1.00    16.01.90   Simmchen     Modularisierung
-       #2      2.00    09.02.90   Seichter     weitere Modularisierung und
-                                               Anpassung an Vertreterstamm
-       #130    1.234    30.01.92   GF  
-       #129    1.300a   03.02.92   RO          Adresstyp pruefen.      
-       #295    1.300a   03.02.92     RO                Adress-Typ fuer Lieferadresee
						korrigieren.
-       #895  1.302     24.06.92     RO         Adress-Typ 25 nicht ueberschreiben.
-
------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   (alle Proceduren dieses Moduls muessen
-                                   an die verwendeten Tabellennamen angepasst
-                                   werden:)    
-
-                                   Datenbankmodul speziell 
-                                   fuer Tabelle kun
-
-
------------------------------------------------------------------------------
*/

module mo_dbspe

/*----- Export Section ------------------------------------------------------*/

EXPORT
	field danzahl
	field dzeiger
	field dflgadr
	field CKUN_INFO
        proc position_einfuegen
        proc positionen_loeschen
        proc fetchscrollpos
        proc initformpos
        proc inittablepos
        proc recreaddefpos
        proc dispformpos

	proc prepupdate
	proc prepread
	proc prepscroll
	proc putmxkey
	proc recupdate
	proc recdelete
	proc recinsert
	proc recread
	proc getmxkey
	proc recreaddef
	proc inittable
	proc savekey
	proc backkey
	proc initkey
	proc cmpkey
	proc clrdelstatus
	proc getdelstatus
	proc getauswst
	proc initform
	proc dispform
	proc fetchcuu
	proc fetchread
	proc erasecursor
	proc closeread
	proc fetchfirst
	proc fetchnext
        proc adrwahl
	proc krzwahl
	
	proc getmdnfil
	proc sys_par_holen
	proc movekrztoadr
	field dadr_typ        /* #129 */
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
	field di
end

IMPORT module mo_cons
end

IMPORT module m_16500f 
end

IMPORT module mo_dbkun
	database bws
	proc prepupdate_kun
	proc prepread_kun
	proc recupdate_kun
	proc recdelete_kun
	proc recinsert_kun
	proc recread_kun
	proc recreaddef_kun
	proc inittable_kun
	proc initkey_kun
	proc clrdelstatus_kun
	proc fetchcuu_kun
	proc fetchread_kun
	proc erasecursor_kun
	proc closeread_kun
	proc putmxkey_kun
	proc getmxkey_kun
end

IMPORT module mo_dbfil
	database bws
END

IMPORT module mo_dbmdn
	database bws
END

IMPORT module mo_sysp
END

IMPORT module mo_dbadr 
	database bws
	proc prepupdate_adr
	proc prepread_adr
	proc recupdate_adr
	proc recdelete_adr
	proc recinsert_adr
	proc recread_adr
	proc recreaddef_adr
	proc inittable_adr
	proc initkey_adr
	proc clrdelstatus_adr
	proc fetchcuu_adr
	proc fetchread_adr
	proc erasecursor_adr
	proc closeread_adr
	proc closeupdate_adr
end

IMPORT module mo_dbkin
	database bws
	proc prepupdate_kun_info
	proc prepread_kun_info
	proc recupdate_kun_info
	proc recdelete_kun_info
	proc recinsert_kun_info
	proc recread_kun_info
	proc recreaddef_kun_info
	proc inittable_kun_info
	proc initkey_kun_info
	proc clrdelstatus_kun_info
	proc fetchcuu_kun_info
	proc fetchread_kun_info
	proc erasecursor_kun_info
	proc closeread_kun_info
end

#include "colaenge"

field dsavekey   like kun.kun 
field ddefakey   like kun.kun value "-1"
field dcurskey   like kun.kun dimension(CMAXMATRIX)
field danzahl    smallint
field dzeiger    smallint
field dsql       smallint
field dflgadr    smallint
field CKUN_INFO  smallint       /* Systemschalter zum Anlegen
						   der Kundeninfodatensaetze */
field dadr_typ   smallint       /* #129 */

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : prepupdate
-
-       In              : -
-
-       Out             : dsql  smallint
-                         
-       Beschreibung    : Die Procedure prepariert die update cursor.
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE prepupdate

if "" = "@(#) mo_dbspe.rsf	1.401	24.06.92	by RO" end
	perform prepupdate_kun returning (dsql)

	if dsql
		return (dsql)
	end

	perform adrwahl

	perform prepupdate_adr returning (dsql)

	if CKUN_INFO
		if dsql
			return (dsql)
		end

		let kun_info.kun = kun.kun

		perform prepupdate_kun_info returning (dsql)
	end

	return (dsql)
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : prepread
-
-       In              : -
-
-       Out             : dsql  smallint
-                         
-       Beschreibung    : Die Procedure prepariert die read cursor.
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE prepread

	perform prepread_kun returning (dsql)

	if dsql
		return (dsql)
	end

	perform adrwahl

	perform prepread_adr returning (dsql)

	return (dsql)
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : prepscroll 
-
-       In              : -
-
-       Out             : sqlstatus     smallint
-                         
-       Beschreibung    : Die Procedure prepariert den scroll cursor.
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE prepscroll

field where_part char(80)

/*------------- Pruefung, ob wirkliche Eingabe erfolgt ist ----------------*/

	if query from mamain1.m1kun <> -1 
				and query from mamain1.m1adr1 <> -1
	    let where_part = "and adr.adr = kun.adr1 and kun.kun >= 0"
	else
	    let where_part = "and kun.kun = -1 and adr.adr = kun.adr1"
	end

	if dflgadr = 1
	    if query from mamain1.m1kun <> -1 
				and query from mamain1.m1adr2 <> -1
		let where_part = "and adr.adr = kun.adr2 and kun.kun >= 0"
	    else
		let where_part = "and kun.kun = -1 and adr.adr = kun.adr2"
	    end
	end

	if dflgadr = 2
	    if query from mamain1.m1kun <> -1 
				and query from mamain1.m1adr3 <> -1
		let where_part = "and adr.adr = kun.adr3 and kun.kun >= 0"
	    else
		let where_part = "and kun.kun = -1 and adr.adr = kun.adr3"
	    end
	end

	prepare scroll cursor cus_kun from sql
		select #kun.%,#adr.% from kun,adr
		where end
		query from form mamain1
		field where_part
		sql and @kun.mdn and @kun.fil end
		sql order by kun.kun,adr.adr end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : putmxkey 
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Schluesselwert Matrix lesen
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE putmxkey

	perform putmxkey_kun(danzahl)

END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : getmxkey 
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Schluesselwert Matrix lesen
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE getmxkey

	perform getmxkey_kun(dzeiger)

END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : recupdate 
-
-       In              : -
-
-       Out             : dsql  smallint
-                         
-       Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-                         mit dem update cursor gelesenen Datensatz
-                         durch.  
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE recupdate

	perform recupdate_kun returning (dsql)

	if dsql
		return(dsql)
	end

	perform adrwahl
	perform movekrztoadr
	perform recupdate_adr returning (dsql)

	return (dsql)
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : recdelete 
-
-       In              : -
-
-       Out             : dsql  smallint
-                         
-       Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-                         mit dem update cursor gelesenen Datensatz
-                         durch.  
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE recdelete

field  dsavewert smallint

	perform recdelete_kun returning (dsql)
	
	if dsql
		return(dsql)
	end
	let dsavewert = dflgadr
	init di dflgadr

	while di < 3
		perform closeupdate_adr returning (dsql)
		perform adrwahl
		perform fetchcuu_adr returning (dsql)

/*---------- Defaultsatz darf nicht geloescht werden ---------------------*/

		if adr.adr <> -1
			perform recdelete_adr returning (dsql)
			if dsql
				let dflgadr = dsavewert
				return(dsql)
			end
		end
		let dflgadr = dflgadr + 1
		let di = di + 1
	end
	let dflgadr = dsavewert

	if kun.kun <> -1
		let kun_info.kun = kun.kun

		perform recdelete_kun_info returning (dsql)
	end

	return (dsql)
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : recinsert 
-
-       In              : -
-
-       Out             : dsql  smallint
-                         
-       Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-                         satzes durch.  
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE recinsert

	perform recinsert_kun returning (dsql)

	if dsql
		return(dsql)
	end
	let dflgadr = 9

	perform adrwahl

	if CKUN_INFO
		let kun_info.kun = kun.kun

		perform recinsert_kun_info returning (dsql)
	end

	return (dsql)

END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : recread 
-
-       In              : -
-
-       Out             : dsql  smallint
-                         
-       Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread
	
	perform recread_kun returning (dsql)

	if dsql
		return(dsql)
	end

	perform adrwahl

	perform recread_adr returning (dsql)

	return (dsql)
END

/*-------------------------------------------------------------------------
-
-       Operatorname    : recreaddef
-
-       In              : -
-
-       Out             : dsql  smallint
-                         
-       Beschreibung    : Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef
	
	perform recreaddef_kun returning (dsql)

	if dsql
		return(dsql)
	end

	perform adrwahl

	perform recreaddef_adr returning (dsql)

	let dflgadr = 0
	return (dsql)
END

/*-------------------------------------------------------------------------
-
-       Operatorname    : inittable
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable  

	perform inittable_kun
	perform inittable_adr
	let kun.mdn = mdn.mdn
	let kun.fil = fil.fil           /* geaendert #130 vorher: =0 */
					/* 2 Zeilen eingefuegt, GF, 29.01.92 */
	let adr.mdn = mdn.mdn
	let adr.fil = fil.fil
	if CKUN_INFO
		perform inittable_kun_info
		let kun_info.mdn = mdn.mdn
		let kun_info.fil = fil.fil              
	end
END

/*-------------------------------------------------------------------------
-
-       Operatorname    : savekey
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Die Procedure sichert den alten Schluessel-
-                         wert.   
-
-------------------------------------------------------------------------*/

PROCEDURE savekey

	let dsavekey = kun.kun
END

/*-------------------------------------------------------------------------
-
-       Operatorname    : backkey
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Die Procedure setzt den gesicherten  
-                         Schluesselwert zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey

	let kun.kun = dsavekey
END

/*-------------------------------------------------------------------------
-
-       Operatorname    : initkey
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Die Procedure initialisiert den Schluessel-
-                         wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey

	perform initkey_kun
	perform initkey_adr
	if CKUN_INFO
		perform initkey_kun_info
	end
END

/*-------------------------------------------------------------------------
-
-       Operatorname    : cmpkey
-
-       In              : -
-
-       Out             : CTRUE/CFALSE  smallint
-                         
-       Beschreibung    : Die Procedure prueft den gesicherten       
-                         Schluesselwert gegen den aktuelle
-                         Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey

	if dsavekey = kun.kun
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-       Operatorname    : clrdelstatus
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Die Procedure setzt die delete Statusvariable
-                         auf den Wert 0. Damit ist der Datensatz
-                         wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus

	perform clrdelstatus_kun
	perform clrdelstatus_adr
	if CKUN_INFO
		perform clrdelstatus_kun_info
	end
END

/*-------------------------------------------------------------------------
-
-       Operatorname    : getdelstatus
-
-       In              : -
-
-       Out             : delstatus             smallint         0     o.k.
-                                                               -1     geloescht
-                         
-       Beschreibung    : Die Procedure setzt die allgemeine delete  
-                         Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus

	return (kun.delstatus)
END


/*-------------------------------------------------------------------------
-
-       Operatorname    : getauswst
-
-       In              : dstatus               smallint
-                                               0   Werte aus table
-                                               <>0 Initstring
-
-       Out             : Laenge des Schluesselfeldes (als konkreter Wert)
-                                               smallint
-                         mxstring              char      zusammengesetzter 
-                                                         String aus im Matrix-
-                                                         menue auszugebenden
-                                                         Feldern 
-                                                         (max. 80 Zeichen)
-                         danzahl               smallint  Anzahl der Datensaetze
-                                                         einschl. geloeschte 
-                                                         Saetze
-                         
-       Beschreibung    : Die Procedure liest cursor absolut
-                         und gibt string fuer matrixmenue zurueck
-
-------------------------------------------------------------------------*/

PROCEDURE getauswst

parameter field dstatus         smallint        end

auto field dhilf char(100)
	if dstatus
		return (8, " ", danzahl) 
	end
       /*
	return (8, picture(kun.kun,"-#######") # " " 
			# picture(adr.adr,"-#######")
			# " " # adr.adr_nam1 # " ", danzahl)
			*/

	putstr(dhilf,picture(kun.kun,"-########"),1,8)
	putstr(dhilf,adr.adr_nam1,10,30)
	putstr(dhilf,adr.plz,41,6)
	putstr(dhilf,adr.ort1,47,30)
	return (8,dhilf,danzahl)
	/****
	return (8, picture(kun.kun,"-#######")  
			# " " #
			adr.adr_nam1 # " "#
			adr.plz#" "#
			adr.ort1
			, danzahl)
	*******/

END

/*-------------------------------------------------------------------------
-
-       Operatorname    : initform
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Die Procedure initialisiert die Maske mamain1  
-
-------------------------------------------------------------------------*/

PROCEDURE initform

	init form mamain1

END

/*-------------------------------------------------------------------------
-
-       Operatorname    : dispform
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Die Procedure gibt die Maske mamain1 aus  
-
-------------------------------------------------------------------------*/

PROCEDURE dispform

	display form mamain1

END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : fetchread 
-
-       In              : -
-
-       Out             : dsql  smallint
-                         
-       Beschreibung    : Die Procedure liest einen Datensatz ueber die read
-                         cursor.
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE fetchread

	perform fetchread_kun returning (dsql)

	if dsql
		return(dsql)
	end

	perform adrwahl

	perform fetchread_adr returning (dsql)

	return (dsql)
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : erasecursor 
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Die Procedure loescht alle cursor
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE erasecursor

	perform erasecursor_kun
	perform erasecursor_adr
	if CKUN_INFO
		perform erasecursor_kun_info
	end
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : fetchcuu 
-
-       In              : -
-
-       Out             : dsql  smallint
-                         
-       Beschreibung    : Die Procedure liest einen Datensatz ueber die 
-                         update cursor.
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu

field dsaveflg  smallint

	perform fetchcuu_kun returning (dsql)

	if dsql
		return(dsql)
	end

	perform adrwahl

	perform fetchcuu_adr returning (dsql)

	if dsql = 100
		let dsaveflg = dflgadr
		let dflgadr = 9
		perform adrwahl
		let dflgadr = dsaveflg
		perform fetchread_adr returning (dsql)
	end

	if CKUN_INFO
		let kun_info.kun = kun.kun

		perform fetchcuu_kun_info returning (dsql)
	end

	return (dsql)
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : closeread 
-
-       In              : -
-
-       Out             : dsql  smallint
-                         
-       Beschreibung    : Die Procedure schliesst die read cursor.  
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE closeread

	perform closeread_kun returning (dsql)

	if dsql
		return(dsql)
	end

	perform adrwahl

	perform closeread_adr returning (dsql)

	return (dsql)
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : fetchfirst 
-
-       In              : -
-
-       Out             : sqlstatus
-                         
-       Beschreibung    : Die Procedure liest den ersten Satz von cursor
-                         cus_kun.
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE fetchfirst

	fetch first cursor cus_kun
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : fetchnext 
-
-       In              : -
-
-       Out             : sqlstatus
-                         
-       Beschreibung    : Die Procedure liest den naechsten Satz von cursor
-                         cus_kun.
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE fetchnext

	fetch next cursor cus_kun
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : adrwahl 
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Die Procedure waehlt in Abhaengigkeit vom Flag 
-                         dflgadr die zugehoerige Adresse aus.
-                         dflgadr = 0   =>      normale Adresse 
-                                 = 1   =>      Lieferadresse
-                                 = 2   =>      Rechnungsadresse
-                                 = 9   =>      Defaultsatz
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE adrwahl

	if dflgadr = 1
		let adr.adr = kun.adr2
/* #295 
		let adr.adr_typ = 21
*/
		let dadr_typ = 21    /* #129 */

/* #295 A */
		if kun.adr2 = kun.adr1
			/* #895 let adr.adr_typ = 20  */
		else
			let adr.adr_typ = 21
		end
/* #234 E */
      
		return
	end
	if dflgadr = 2
		let adr.adr = kun.adr3
/* #295
		let adr.adr_typ = 22
*/
		let dadr_typ = 22    /* #129 */
     
/* #295 A */
		if kun.adr3 = kun.adr1
			/* #895 let adr.adr_typ = 20  */
		else
			let adr.adr_typ = 22
		end
/* #295 E */
		return
	end
	if dflgadr = 9
		let adr.adr = -1
		let adr.adr_typ = 20
		let dadr_typ = 20    /* #129 */
		return
	end
	let adr.adr = kun.adr1
/* #987 A */
	if adr.adr_typ <> 25
		let adr.adr_typ = 20
	end
	let dadr_typ = 20    /* #129 */
/* #987 E */
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : krzwahl 
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Die Procedure ordnet in Abhaengigkeit vom Flag 
-                         dflgadr den zugehoerigen Kurznamem zu.
-                         dflgadr = 0   =>      kun.kun_krz1 
-                                 = 1   =>      kun.kun_krz2
-                                 = 2   =>      kun.kun_krz3
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE krzwahl

	if dflgadr = 1
		let kun.kun_krz2 = adr.adr_krz
		return
	end
	if dflgadr = 2
		let kun.kun_krz3 = adr.adr_krz
		return
	end
	let kun.kun_krz1 = adr.adr_krz
END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : getmdnfil 
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : 
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE getmdnfil

	let kun.mdn = mdn.mdn
	let kun.fil = fil.fil

END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : sys_par_holen 
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : liest den Systemparameter aus der Tabelle sysp
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE sys_par_holen

field dreturn smallint

	perform get_sys_par("KUN_INFO") returning(dreturn,CKUN_INFO)

END

/*---------------------------------------------------------------------------
-                                                                         
-       Procedure       : movekrztoadr 
-
-       In              : -
-
-       Out             : -
-                         
-       Beschreibung    : Die Procedure ordnet in Abhaengigkeit vom Flag 
-                         dflgadr den zugehoerigen Kurznamem zu.
-                         dflgadr = 0   =>      kun.kun_krz1 
-                                 = 1   =>      kun.kun_krz2
-                                 = 2   =>      kun.kun_krz3
-                                                       
---------------------------------------------------------------------------*/

PROCEDURE movekrztoadr

	if dflgadr = 1
		let  adr.adr_krz = kun.kun_krz2
		return
	end
	if dflgadr = 2
		let  adr.adr_krz = kun.kun_krz3
		return
	end
	let  adr.adr_krz = kun.kun_krz1
END

PROC dispformpos
parameter field a smallint
         field b smallint
end
return
END


PROC position_einfuegen
return(0)
END

PROC positionen_loeschen
return(0)
END

PROC fetchscrollpos
parameter field a smallint end
return(0)
END


PROC initformpos
return
END
PROC inittablepos
return
END
PROC recreaddefpos
return(0)
END

