/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbipr
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	29.05.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    29.05.90   Simmchen	Modularisierung
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle ipr
-
-
------------------------------------------------------------------------------
*/

module mo_dbipr

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws
	proc prepscrollpos_ipr
	proc prepscrollpos_ipr_a
	proc prepscrollpos_ipr_a_prgr
	proc prepupdatepos_ipr
	proc fetchscrollpos_ipr
	proc recupdatepos_ipr
	proc recupdatepos_ipr_aktion
	proc recdeletepos_ipr
	proc recinsertpos_ipr
	proc fetchupdatepos_ipr
	proc closeupdatepos_ipr
	proc closescrollpos_ipr
	proc recreadpos_ipr
	proc recreaddefpos_ipr
	proc inittablepos_ipr
	proc position_einfuegen_ipr
	proc position_loeschen_ipr
	proc positionen_loeschen_ipr
	proc positionen_loeschen_ipr_a
	proc positionen_loeschen_ipr_a_prgr
end

/*----- Import Section ------------------------------------------------------*/

import module mo_cons
end

database bws
	table ipr end
end

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_ipr
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_ipr

	prepare scroll cursor cusmain2 from
		sql select #ipr.% from ipr
		where @ipr.mdn and
		      @ipr.pr_gr_stuf and
		      @ipr.kun_pr end
		sql order by ipr.a end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_ipr_a
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_ipr_a

	prepare scroll cursor cusmain2 from
		sql select #ipr.% from ipr
		where @ipr.mdn and
		      @ipr.a and
		      ipr.pr_gr_stuf <> 0 and
		      ipr.kun_pr <> 0 end
		sql order by ipr.kun_pr end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscrollpos_ipr_a_prgr
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepscrollpos_ipr_a_prgr

	prepare scroll cursor cusmain2 from
		sql select #ipr.% from ipr
		where @ipr.mdn and
		      @ipr.a and
		      ipr.kun_pr = 0 end
		sql order by ipr.pr_gr_stuf end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdatepos_ipr 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor Positionen.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdatepos_ipr

	prepare cursor cuu_ipr from sql
		select #ipr.% from ipr
		where @ipr.mdn and
		      @ipr.pr_gr_stuf and
		      @ipr.kun_pr and
		      @ipr.a for update end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closupdatepos_ipr
-
-	In		: -
-
-	Out		: -	
-			  
-	Beschreibung    : Die Procedure schliesst die update cursor.  
-							
---------------------------------------------------------------------------*/

PROCEDURE closeupdatepos_ipr

	close cursor cuu_ipr
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recreadpos_ipr
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Positionssatz 
-							
---------------------------------------------------------------------------*/

PROCEDURE recreadpos_ipr

	execute from sql 
		select #ipr.% from ipr
		where @ipr.mdn and
		      @ipr.pr_gr_stuf and
		      @ipr.kun_pr and
		      @ipr.a
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdeletepos_ipr
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Delete auf den aktuellen
-			  Positionssatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdeletepos_ipr

	execute from sql 
		delete from ipr where current of cuu_ipr
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdatepos_ipr
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  Positionssatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdatepos_ipr

	execute from sql 
		update ipr set @ipr.% where current of cuu_ipr
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdatepos_ipr_aktion
-
-	In		: -
-
-	Out		: dsql	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf dem Feld
-			  Aktion durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdatepos_ipr_aktion

	execute from sql 
		update ipr set @ipr.aktion_nr where 
		@ipr.mdn and
		@ipr.pr_gr_stuf and
		@ipr.kun_pr and
		@ipr.a
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchupdatepos_ipr
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest einen Positionssatz for update
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchupdatepos_ipr

	fetch cursor cuu_ipr

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: recinsertpos_ipr
-
-
-	In			: -
-
-	Out			: dsqlstatus		
-			  
-	Beschreibung		: Die Procedure fuegt einen Positionssatz
-				  in die Datenbanktabelle ein.          
-
-------------------------------------------------------------------------*/

PROCEDURE recinsertpos_ipr

	execute from sql 
		insert into ipr (%ipr.*)
		values ($ipr.*)
	end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: position_loeschen_ipr
-
-
-	In			: -
-
-	Out			: sqlstatus		
-			  
-	Beschreibung		: Die Procedure loescht einen Positions- 
-				  satz aus der Datenbank.             
-
-------------------------------------------------------------------------*/

PROCEDURE position_loeschen_ipr

	execute from sql
		delete from ipr
		where @ipr.mdn and
		      @ipr.pr_gr_stuf and
		      @ipr.kun_pr and
		      @ipr.a
	end
	return (sqlstatus)
END
/*-------------------------------------------------------------------------
-
-	Operatorname		: positionen_loeschen_ipr
-
-
-	In			: -
-
-	Out			: sqlstatus		
-			  
-	Beschreibung		: Die Procedure loescht alle Positions- 
-				  saetze aus der Datenbank.             
-
-------------------------------------------------------------------------*/

PROCEDURE positionen_loeschen_ipr

	execute from sql
		delete from ipr
		where @ipr.mdn and
		      @ipr.pr_gr_stuf and
		      @ipr.kun_pr 
	end
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: positionen_loeschen_ipr_a
-
-
-	In			: -
-
-	Out			: sqlstatus		
-			  
-	Beschreibung		: Die Procedure loescht alle Positions- 
-				  saetze aus der Datenbank fuer einen Artikel.
-
-------------------------------------------------------------------------*/

PROCEDURE positionen_loeschen_ipr_a

	execute from sql
		delete from ipr
		where @ipr.mdn and
		      @ipr.a and
		      ipr.kun_pr <> 0 and
		      ipr.pr_gr_stuf <> 0
	end
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: positionen_loeschen_ipr_a_prgr
-
-
-	In			: -
-
-	Out			: sqlstatus		
-			  
-	Beschreibung		: Die Procedure loescht alle Positions- 
-				  saetze aus der Datenbank fuer einen Artikel.
-
-------------------------------------------------------------------------*/

PROCEDURE positionen_loeschen_ipr_a_prgr

	execute from sql
		delete from ipr
		where @ipr.mdn and
		      @ipr.a and
		      ipr.kun_pr = 0
	end
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname		: position_einfuegen_ipr
-
-
-	In			: -
-
-	Out			: dsqlstatus		
-			  
-	Beschreibung		: Die Procedure fuegt einen Positionssatz
-				  in die Datenbanktabelle ein.          
-
-------------------------------------------------------------------------*/

PROCEDURE position_einfuegen_ipr

	field dsqlstatus smallint

	execute from sql 
		insert into ipr (%ipr.*)
		values ($ipr.*)
	end

	let dsqlstatus = sqlstatus
	if sqlstatus
		if dsqlstatus <> CENSQL239
			execute from sql
				rollback work
			end
		else
			execute from sql
				update ipr set @ipr.*
				where @ipr.mdn and
				      @ipr.pr_gr_stuf and
				      @ipr.kun_pr and
				      @ipr.a
			end

			let dsqlstatus = sqlstatus
		end
	end
	return (dsqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closescrollpos_ipr
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure schlisset den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE closescrollpos_ipr

	close cursor cusmain2
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchscrollpos_ipr
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung    : Die Procedure liest alle Positionsaetze
-			  ueber den scroll cursor cusmain2.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchscrollpos_ipr
parameter
	field curmod smallint
end
	if curmod = CRDFIRST
		fetch first cursor cusmain2
	elseif curmod = CRDNEXT
		fetch next cursor cusmain2
	end
	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorplnr	: recreaddefpos_ipr
-
-	In		: -
-
-	Out		: sqlstatus
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddefpos_ipr

field dsql smallint
field dsavmdn like ipr.mdn 

	let dsavmdn = ipr.mdn

	let ipr.a = -1

	perform recreadpos_ipr returning (dsql)
	if dsql = CENSQL100
		if ipr.mdn <> 0
			let ipr.mdn = 0
			perform recreadpos_ipr returning (dsql)
		end
	end

	let ipr.mdn = dsavmdn

	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorplnr	: inittablepos_ipr
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittablepos_ipr

	init table ipr
END
procedure what_kennung
if "" = "@(#) mo_dbipr.rsf	1.400	05.11.91	by RO>" end
end
