/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbleits
-
-	Interne Operatoren	:	proc prepupdate	
-					proc prepread
-					proc putmxkey
-					proc recupdate
-					proc recdelete
-					proc recinsert
-					proc recread
-					proc getmxkey
-					proc recreaddef
-					proc inittable
-					proc savekey
-					proc backkey
-					proc initkey
-					proc cmpkey
-					proc clrdelstatus
-					proc getdelstatus
-					proc getauswst
-					proc initform
-					proc dispform
-
-	Externe Operatoren	:	-
-
-	Autor			:	Juergen Simmchen / Axel Seichter
-	Erstellungsdatum	:	16.01.90
-	Modifikationsdatum	:	09.02.90
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    16.01.90   Simmchen	Modularisierung
-	#2      2.00    09.02.90   Seichter	weitere Modularisierung und
-						Anpassung an Vertreterstamm
-	#2      3.00    16.02.90   Kraushaar	Anpassung an Leitsaetze
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-				    fuer Tabelle leits
-
-
------------------------------------------------------------------------------
*/

module mo_dblei

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws


	proc prepupdate_leits
	proc prepread_leits
	proc putmxkey_leits
	proc recupdate_leits
	proc recdelete_leits
	proc recinsert_leits
	proc recread_leits
	proc getmxkey_leits
	proc recreaddef_leits
	proc inittable_leits
	proc initkey_leits
	proc cmpkey_leits
	proc backkey_leits
	proc savekey_leits
	proc clrdelstatus_leits
	proc getdelstatus_leits
	proc fetchcuu_leits
	proc fetchread_leits
	proc erasecursor_leits
	proc closeread_leits
end
IMPORT module mo_cons
	field CTRUE
	field CFALSE
END
/*----- Import Section ------------------------------------------------------*/
#include "colaenge"


database bws
	table    leits	end
end

field dsavekey 	 like leits.leits 
field ddefakey   like leits.leits value "-1"
field dcurskey	 like leits.leits dimension(CMAXMATRIX)
field danzahl	 smallint
field dzeiger	 smallint 
field leits_ftxt	 char(56)

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_leits
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor cuu_leits.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_leits

	prepare cursor cuu_leits from sql
		select #leits.* from leits
		where @leits.leits for update
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_leits
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor cur_leits.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_leits

	prepare cursor cur_leits from sql
		select #leits.* from leits
		where @leits.leits
		end

	return (sqlstatus)
END
/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepscroll 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den scroll cursor cus_leits.
-							
---------------------------------------------------------------------------*/

/*
PROCEDURE prepscroll

	prepare scroll cursor cus_leits from sql
		select #leits.* from leits
		where end
		query from form mamain1
		sql order by leits.leits end

	return (sqlstatus)
END
*/
/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_leits
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_leits

		let dcurskey[danzahl] = leits.leits

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_leits 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_leits

		let leits.leits = dcurskey[dzeiger]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_leits 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_leits gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_leits

	execute from sql 
		update leits
		set @leits.*
		where current of cuu_leits
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_leits 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_leits gelesenen Datensatz
-			  durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_leits

	let leits.delstatus = -1

	execute from sql
		update leits
		set @leits.delstatus
		where current of cuu_leits
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_leits 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_leits

	execute from sql 
		insert into leits (%leits.*)
		values ($leits.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_leits 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_leits
	execute from sql
		select #leits.* from leits where @leits.leits
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_leits
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_leits

field dsql smallint

	let leits.leits = ddefakey

	perform recread_leits returning (dsql)
	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_leits
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_leits  

	init table leits
END


/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_leits
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_leits

	init leits.leits
END



/*-------------------------------------------------------------------------
-
-	Operatorname	: savekey_leits
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure sichert den alten Schluessel-
-			  wert.   
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_leits

	let dsavekey = leits.leits
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: backkey_leits
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt den gesicherten  
-			  Schluesselwert zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_leits

	let leits.leits = dsavekey
END


/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_leits
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_leits

	if dsavekey = leits.leits
		return (CTRUE)
	end

	return (CFALSE)
END
/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_leits
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_leits

	let leits.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_leits
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_leits

	return (leits.delstatus)
END


/*-------------------------------------------------------------------------
-
-	Operatorname	: getauswst
-
-	In		: dstatus		smallint
-						0   Werte aus table
-						<>0 Initstring
-
-	Out		: Laenge des Schluesselfeldes (als konkreter Wert)
-						smallint
-			  mxstring		char	  zusammengesetzter 
-							  String aus im Matrix-
-							  menue auszugebenden
-							  Feldern 
-							  (max. 80 Zeichen)
-			  danzahl		smallint  Anzahl der Datensaetze
-						          einschl. geloeschte 
-							  Saetze
-			  
-	Beschreibung	: Die Procedure liest cursor cus_leits absolut
-			  und gibt string fuer matrixmenue zurueck
-
-------------------------------------------------------------------------*/

PROCEDURE getauswst

parameter field dstatus		smallint	end

	if dstatus
		return (8, " ", danzahl) 
	end

	return (8, leits.leits # " " 
			# leits.leits_bz # " ", danzahl)

END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initform
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Maske mamain1  
-
-------------------------------------------------------------------------*/
/*
PROCEDURE initform

	init form mamain1

END
*/
/*-------------------------------------------------------------------------
-
-	Operatorname	: dispform
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure gibt die Maske mamain1 aus  
-
-------------------------------------------------------------------------*/
/*
PROCEDURE dispform

	display form mamain1

END
*/


/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_leits 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_leits.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_leits

	fetch cursor cur_leits
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_leits 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_leits
-			  ,cur_leits und cus_leits.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_leits

	erase cursor cus_leits
	erase cursor cuu_leits
	erase cursor cur_leits
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_leits 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_leits.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_leits

	fetch cursor cuu_leits
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_leits 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_leits.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_leits

	close cursor cur_leits
	return (sqlstatus) 
END
procedure what_kennung
if "" = "@(#) mo_dbleits.rsf	1.400	05.11.91	by AR" end
end
