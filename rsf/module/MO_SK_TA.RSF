module mo_sk_ta

/***
--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       mo_sk_tab.rsf
-
        Exports                 :       proc zeigebem
					proc lesesk
					proc schreibesk
                                        proc skloeschen
                                        proc oeffnesk
                                        proc sucheconende
                                        proc holeconcatdir
                                        proc init_bem_zeile
                                        proc titel
                                        proc displaybem
                                        proc clearbem
                                        field concatdir
                                        field conende
					field conmerk
                                        field windowende
                                        field bemzeile
                                        field sktext
                                        field eig_bem
                                        field eig_bem2
                                        field ind
                                        form bemerkung


-       Interne Prozeduren      :
                                        proc schreibesatz
-
-
-       Autor                   :       Wille
-       Erstellungsdatum        :       09.07.90
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Datenbank-Funktionen zum Bemerkungen
				    bearbeiten.
-
--------------------------------------------------------------------------------
***/

/* Exports                   */

export
   field bem_spalte
        proc zeigebem
	proc lesesk
	proc schreibesk
        proc skloeschen
        proc oeffnesk
        proc sucheconende
        proc holeconcatdir
        proc init_bem_zeile
        proc titel
        proc displaybem
        proc clearbem
        field concatdir
        field conende
	field conmerk
        field windowende
        field bemzeile
        field sktext
        field eig_bem
        field eig_bem2
        field ind
        form bemerkung
end

/* Imports                   */

import module mo_men_m
end

database bws
table lief_bem    end
end


constant

   SK_MAX    value    100
   BEM_TYP   value    5
   TRUE      value    1
   CONCATDIR value    "CONCATDIR"

end

/*  Globale Variable                                                 */


/* Matrix zum Bemerkungen Bearbeiten                                */

field bem_spalte smallint
field sktext          char (60)      dimension (SK_MAX)
field conende         smallint
field conmerk 	      smallint
field windowende      smallint
field ind             smallint

field concatdir       char (60)

field eig_bem         char (60)
field eig_bem2        char (60)

/* Zwei Matrixzeilen auf der Maske Mamain1                       */
form bemerkung
field mbem1   use eig_bem  pos (0 14) length 60
field mbem2   use eig_bem2 pos (1 14) length 60  removed
end

/* Lokale Variable                                                  */

field neuer_catsatz   smallint
field blank           char(60)
field altende         smallint

/* Offsets fuer Bemerkungen auf Mamain1                             */

field bemzeile        smallint      dimension (BEM_TYP)


/* Form zum Loeschen von Matrixzeilen                            */

form bemloesch
field bloesch1  value " "   pos (0 14) length 60
field bloesch2  value " "   pos (1 14) length 60
end

/** Bemerkungsfelder loeschen                                     **/

procedure clearbem


if "" = "@(#) mo_sk_tab.rsf	2.0	 	08.12.92	by Ro" end
	      display pos(bemzeile[0] 0) form bemloesch
end


/** Anzeigen von 2 Bemerkungszeilen auf der Masek Mamain1          **/

procedure zeigebem
    parameter
    field bem        integer
    field l_nr       char (16)
    field lmdn       smallint
    end

    field i          smallint

    init eig_bem eig_bem2 ind sktext
    display pos (bemzeile[0] bem_spalte) form bemerkung
    let lief_bem.lief = l_nr
    let lief_bem.mdn = lmdn
    let bem = proc oeffnesk (bem lmdn)
    init i
    close cursor bem
    for i = 0 to 1
                    fetch cursor bem
                    if sqlstatus and i = 0
                                 let neuer_catsatz = TRUE
                                 return
                    end
                    if sqlstatus = 0
                                 let sktext[i] = lief_bem.txt
                    end
    end
    perform displaybem (0)
end

/** Form Bemerkung in Maske mamain1 anzeigen                **/

procedure displaybem
     parameter
     field     tab            smallint
     end

     let eig_bem =  sktext [0]
     let eig_bem2 = sktext [1]
     display pos (bemzeile[tab] bem_spalte) form bemerkung
end
/** Cursor fuer Bemerkungen vorbereiten                 **/

procedure oeffnesk
    parameter                        /* Die Parameter werden nicht benutzt */
    field   bem         integer
    field   smdn        smallint
    end

    init neuer_catsatz
    prepare cursor bem from sql
       select #lief_bem.txt from lief_bem
              where @lief_bem.lief and @lief_bem.mdn
              and   lief_bem.lfd >= 0
              for update
    end
    return (bem)
end


/** Bemerkungen zu einem Atikel in Matrix einlesen                    **/

procedure lesesk
    parameter
    field l_nr          char (16)
    field lmdn          smallint
    end

    let lief_bem.lief = l_nr
    let lief_bem.mdn  = lmdn
    init ind sktext
    if neuer_catsatz
		return
    end
    close cursor bem
    fetch cursor bem
    while not sqlstatus and ind < SK_MAX
		let sktext[ind] = lief_bem.txt
		fetch cursor bem
	        let ind = ind + 1
    end
    let ind = ind - 1
    if ind > 0
               move ind to conende altende
    else
               init conende altende windowende
    end
    if conende < 13
	       let windowende = conende
    else
	       let windowende = 13
    end
end


/** Matrix  schreiben          **/

procedure schreibesk
      parameter
      field    tab      smallint
      end

  /**   perform sucheconende   **/
     init ind
	/* NEU 11.02 */
	if conmerk < 0
     		close cursor bem
		fetch cursor bem
      		while ind <= altende and not sqlstatus
       	        	 execute from sql
		        	delete from lief_bem
                     	   	where current of bem
       	       	 	end
               		fetch cursor bem
			let ind = ind + 1
     	 	end
		return
	end


     close cursor bem
     fetch cursor bem
     while ind <= conende

                let lief_bem.lfd = ind
/* NEU am 11.02. */
		let lief_bem.txt = sktext[ind]
                perform schreibesatz
		fetch cursor bem
		let ind = ind + 1
      end
      while ind <= altende and not sqlstatus
                execute from sql
		        delete from lief_bem
                        where current of bem
                end
                fetch cursor bem
		let ind = ind + 1
      end
end

/** Bemerkungen  fuer Lieferanten loeschen                        **/

procedure skloeschen
      parameter
      field lief_nr     char (16)
      field lmdn        smallint
      end

      execute from sql
                delete from lief_bem
                where lief_bem.lief = $lief_nr and
                      lief_bem.mdn  = $lmdn
      end
end

/** Letzen Eintrag zu Artikel in Concat-Datei suchen              **/

procedure sucheconende
     init blank
     return
end

/** Satz in Concat-Datei schreiben                                **/

procedure schreibesatz
     if sqlstatus
             execute from sql
                 insert into lief_bem
                 values ($lief_bem.%)
              end
      else
             execute from sql
                 update lief_bem
                 set @lief_bem.%
                 where current of bem
             end
      end

end

/** Titel fuer Bemerkungswindow schreiben      **/

procedure titel
    parameter
    field tab            smallint
    end

    display pos (0 -1) title "\RSonderkonditionen\N"
end

/** Offset fuer Bemerkeungen in der Maske Mamain1 initialisieren        **/

procedure init_bem_zeile

   let bemzeile[0] = 33
end


/** Pfad fuer Concat-Dateien feststellen       **/

procedure holeconcatdir
   init concatdir
   environment(CONCATDIR,concatdir)
end

