/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbkvprg
-
-	Autor			:	Juergen Simmchen
-	Erstellungsdatum	:	30.05.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    30.05.90   Simmchen	Modularisierung
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul speziell 
-
-				    fuer Tabelle kvprgrstk , kvpr
-
-
------------------------------------------------------------------------------
*/

module mo_dbkvg

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_kvprg
	proc prepread_kvprg
	proc putmxkey_kvprg
	proc recupdate_kvprg
	proc recdelete_kvprg
	proc recinsert_kvprg
	proc recread_kvprg
	proc recread_kvprg_pos
	proc getmxkey_kvprg
	proc recreaddef_kvprg
	proc inittable_kvprg
	proc savekey_kvprg
	proc backkey_kvprg
	proc initkey_kvprg
	proc cmpkey_kvprg
	proc clrdelstatus_kvprg
	proc getdelstatus_kvprg
	proc fetchcuu_kvprg
	proc fetchread_kvprg
	proc erasecursor_kvprg
	proc closeread_kvprg
	proc closeupdate_kvprg
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
end

IMPORT module mo_cons
end

IMPORT module mo_dbkvp
	database bws
end

database bws
	table kvprgrstk end
end

#include "colaenge"

field dsavekey1	 like kvprgrstk.mdn
field dsavekey2	 like kvprgrstk.pr_gr_stuf
field ddefakey   like kvprgrstk.pr_gr_stuf value -1
field dsavekeyp  like kvpr.a 
field ddefakeyp  like kvpr.a value -1
field dcurskey	 like kvprgrstk.pr_gr_stuf dimension(CMAXMATRIX)

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_kvprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_kvprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_kvprg

	prepare cursor cuu_kvprg from sql
		select #kvprgrstk.* from kvprgrstk
		where @kvprgrstk.mdn and
		      @kvprgrstk.pr_gr_stuf for update 
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_kvprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cur_kvprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_kvprg

	prepare cursor cur_kvprg from sql
		select #kvprgrstk.* from kvprgrstk
		where @kvprgrstk.mdn and
		      @kvprgrstk.pr_gr_stuf 
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_kvprg
-
-	In		: danzahl	smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_kvprg

parameter field danzahl smallint end

	let dcurskey[danzahl] = kvprgrstk.pr_gr_stuf

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_kvprg
-
-	In		: dzeiger	smallint
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_kvprg

parameter field dzeiger smallint end

	let kvprgrstk.pr_gr_stuf = dcurskey[dzeiger]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_kvprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_kvprg gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_kvprg

	execute from sql 
		update kvprgrstk
		set @kvprgrstk.*
		where current of cuu_kvprg
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_kvprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_kvprg gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_kvprg

	let kvprgrstk.delstatus = -1

	execute from sql
		update kvprgrstk
		set @kvprgrstk.delstatus
		where current of cuu_kvprg
		end

	if sqlstatus
		return (sqlstatus)
	end
	
	execute from sql 
		delete from kvpr
		where kvpr.mdn        = $kvprgrstk.mdn and
		      kvpr.pr_gr_stuf = $kvprgrstk.pr_gr_stuf
	end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_kvprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_kvprg

	execute from sql 
		insert into kvprgrstk (%kvprgrstk.*)
		values ($kvprgrstk.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_kvprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_kvprg
	execute from sql
		select #kvprgrstk.* from kvprgrstk 
		where @kvprgrstk.mdn and
		      @kvprgrstk.pr_gr_stuf and
		      kvprgrstk.delstatus <> -1
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_kvprg_pos
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_kvprg_pos
	execute from sql
		select #kvpr.* from kvpr where 
		@kvpr.mdn        and
		@kvpr.pr_gr_stuf and
		@kvpr.a 
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_kvprg
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_kvprg

field dsql smallint

	let kvprgrstk.pr_gr_stuf = ddefakey

	perform recread_kvprg returning (dsql)
	return (0)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_kvprg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_kvprg

	init table kvprgrstk
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: savekey_kvprg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure sichert den alten Schluessel-
-			  wert.   
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_kvprg

	let dsavekey1 = kvprgrstk.mdn
	let dsavekey2 = kvprgrstk.pr_gr_stuf
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: backkey_kvprg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt den gesicherten  
-			  Schluesselwert zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_kvprg

	let kvprgrstk.mdn        = dsavekey1
	let kvprgrstk.pr_gr_stuf = dsavekey2
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_kvprg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_kvprg

	init kvprgrstk.pr_gr_stuf
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_kvprg
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuelle
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_kvprg

	if dsavekey2 = kvprgrstk.pr_gr_stuf
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_kvprg
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_kvprg

	let kvprgrstk.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_kvprg
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_kvprg

	return (kvprgrstk.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_kvprg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_kvprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_kvprg

	fetch cursor cur_kvprg
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_kvprg 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_kvprg
-			  ,cur_kvprg und cus_kvprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_kvprg

	erase cursor cus_kvprg
	erase cursor cuu_kvprg
	erase cursor cur_kvprg
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_kvprg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_kvprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_kvprg

	fetch cursor cuu_kvprg
	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_kvprg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_kvprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_kvprg

	close cursor cur_kvprg
	return (sqlstatus) 
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeupdate_kvprg 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den update cursor cuu_kvprg.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeupdate_kvprg

	close cursor cuu_kvprg
	return (sqlstatus) 
END

procedure what_kennung
if "" = "@(#) mo_dbkvprg.rsf	1.400	05.11.91	by RO>" end
end
