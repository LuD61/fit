/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Harpener Hellweg 29, 4630 Bochum 1, Tel: 0234 - 507020
-
-----------------------------------------------------------------------------
-
-	Modulname		:	mo_dbvertr
-
-	Interne Operatoren	:	proc prepupdate_vertr
-					proc prepread_vertr
-					proc putmxkey_vertr
-					proc recupdate_vertr
-					proc recdelete_vertr
-					proc recinsert_vertr
-					proc recread_vertr
-					proc getmxkey_vertr
-					proc recreaddef_vertr
-					proc inittable_vertr
-					proc initkey_vertr
-					proc savekey_vertr
-					proc backkey_vertr
-					proc cmpkey_vertr
-					proc clrdelstatus_vertr
-					proc getdelstatus_vertr
-					proc fetchcuu_vertr
-					proc fetchread_vertr
-					proc erasecursor_vertr
-					proc closeread_vertr
-
-	Externe Operatoren	:	-
-
-	Autor			:	Axel Seichter
-	Erstellungsdatum	:	19.02.90
-	Modifikationsdatum	:	-
-
-	Projekt			:	BWS
-	Version			:	1.00
-	Laendervariante		:	BRD
-
-	Rechner			:	UNISYS 386
-	Betriebssystem		:	XENIX  386 
-	Sprache			:       ROSI-SQL
-
-	Aenderungsjournal	:
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-	#1      1.00    19.02.90   Becker /	Modularisierung
-				   Seichter
-	#2      1.00    21.06.90   Seichter	Anpassung an Mandant/Filiale
-
------------------------------------------------------------------------------
-
-	Modulbeschreibung	:   (alle Proceduren dieses Moduls muessen
-				    an die verwendeten Tabellennamen angepasst
-				    werden:)	
-
-				    Datenbankmodul fuer die Verwendung der
-				    Tabelle vertr
-
-
------------------------------------------------------------------------------
*/

module mo_dbver

/*----- Export Section ------------------------------------------------------*/

EXPORT
	database bws

	proc prepupdate_vertr
	proc prepread_vertr
	proc putmxkey_vertr
	proc recupdate_vertr
	proc recdelete_vertr
	proc recinsert_vertr
	proc recread_vertr
	proc getmxkey_vertr
	proc recreaddef_vertr
	proc inittable_vertr
	proc initkey_vertr
	proc savekey_vertr
	proc backkey_vertr
	proc cmpkey_vertr
	proc clrdelstatus_vertr
	proc getdelstatus_vertr
	proc fetchcuu_vertr
	proc fetchread_vertr
	proc erasecursor_vertr
	proc closeread_vertr
end

/*----- Import Section ------------------------------------------------------*/

IMPORT module mo_data 
	field dsttrans
end

IMPORT module mo_cons
end

database bws
	table vertr end
end

#include "colaenge"

field dsavekey1	 like vertr.mdn 
field dsavekey2  like vertr.fil 
field dsavekey3	 like vertr.vertr 
field ddefakey   like vertr.vertr value "-1"
field dcurskey	 like vertr.vertr dimension(CMAXMATRIX)

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepupdate_vertr
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den update cursor
-			  cuu_vertr.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepupdate_vertr

	prepare cursor cuu_vertr from sql
		select #vertr.* from vertr
		where @vertr.mdn and
		      @vertr.fil and
		      @vertr.vertr for update
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: prepread_vertr
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure prepariert den read cursor
-			  cur_vertr.
-							
---------------------------------------------------------------------------*/

PROCEDURE prepread_vertr

	prepare cursor cur_vertr from sql
		select #vertr.* from vertr
		where @vertr.mdn and
		      @vertr.fil and
		      @vertr.vertr
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: putmxkey_vertr
-
-	In		: danz	smallint	Feld danzahl aus mo_dbspez
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE putmxkey_vertr

	parameter	field	danz	smallint	end

		let dcurskey[danz] = vertr.vertr

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: getmxkey_vertr
-
-	In		: dzeig	smallint	Feld dzeiger aus mo_dbspez
-
-	Out		: -
-			  
-	Beschreibung    : Schluesselwert Matrix lesen
-							
---------------------------------------------------------------------------*/

PROCEDURE getmxkey_vertr

	parameter	field	dzeig	smallint	end

		let vertr.vertr = dcurskey[dzeig]

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recupdate_vertr
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt ein Update auf den aktuellen
-			  mit dem update cursor cuu_vertr gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recupdate_vertr

	execute from sql 
		update vertr
		set @vertr.*
		where current of cuu_vertr
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recdelete_vertr
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Loeschen auf den aktuellen
-			  mit dem update cursor cuu_vertr gelesenen
-			  Datensatz durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recdelete_vertr

	let vertr.delstatus = -1

	execute from sql
		update vertr
		set @vertr.delstatus
		where current of cuu_vertr
		end

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recinsert_vertr
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure fuehrt das Einfuegen eines Daten-
-			  satzes in die Tabelle vertr durch.  
-							
---------------------------------------------------------------------------*/

PROCEDURE recinsert_vertr

	execute from sql 
		insert into vertr (%vertr.*)
		values ($vertr.*)
		end

	return (sqlstatus)

END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: recread_vertr
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz aus der Tabelle
-			  vertr.
-
---------------------------------------------------------------------------*/

PROCEDURE recread_vertr

	execute from sql
		select #vertr.* from vertr where 
			@vertr.mdn and
			@vertr.fil and
			@vertr.vertr and
			vertr.delstatus <> -1
		end

	return (sqlstatus)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: recreaddef_vertr
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung	: Die Procedure liest den Defaultsatz aus der Tabelle
-			  vertr.
-
-------------------------------------------------------------------------*/

PROCEDURE recreaddef_vertr

field dsql    smallint
field dsavfil like vertr.fil 
field dsavmdn like vertr.mdn 

	let dsavfil = vertr.fil
	let dsavmdn = vertr.mdn

	let vertr.vertr = ddefakey

	perform recread_vertr returning (dsql)
	if dsql = CENSQL100
		if vertr.fil <> 0
			let vertr.fil = 0
			perform recread_vertr returning (dsql)
		end
		if dsql = CENSQL100
			if vertr.mdn <> 0
				let vertr.mdn = 0
				perform recread_vertr returning (dsql)
			end
		end
	end
	let vertr.fil = dsavfil
	let vertr.mdn = dsavmdn 
	return (dsql)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: inittable_vertr
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert die Table vertr.
-
-------------------------------------------------------------------------*/

PROCEDURE inittable_vertr

	init table vertr
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: initkey_vertr
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure initialisiert den Schluessel-
-			  wert.
-
-------------------------------------------------------------------------*/

PROCEDURE initkey_vertr

	init vertr.vertr
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: savekey_vertr
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure sichert den alten Schluessel-
-			  wert.   
-
-------------------------------------------------------------------------*/

PROCEDURE savekey_vertr

	let dsavekey1 = vertr.mdn
	let dsavekey2 = vertr.fil
	let dsavekey3 = vertr.vertr
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: backkey_vertr
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt den gesicherten  
-			  Schluesselwert zurueck.
-
-------------------------------------------------------------------------*/

PROCEDURE backkey_vertr

	let vertr.mdn   = dsavekey1
	let vertr.fil   = dsavekey2
	let vertr.vertr = dsavekey3
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: cmpkey_vertr
-
-	In		: -
-
-	Out		: CTRUE/CFALSE	smallint
-			  
-	Beschreibung	: Die Procedure prueft den gesicherten       
-			  Schluesselwert gegen den aktuellen
-			  Schluesselwert.
-
-------------------------------------------------------------------------*/

PROCEDURE cmpkey_vertr

	if dsavekey3 = vertr.vertr
		return (CTRUE)
	end

	return (CFALSE)
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: clrdelstatus_vertr
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung	: Die Procedure setzt die delete Statusvariable
-			  auf den Wert 0. Damit ist der Datensatz
-			  wieder vorhanden.
-
-------------------------------------------------------------------------*/

PROCEDURE clrdelstatus_vertr

	let vertr.delstatus = 0
END

/*-------------------------------------------------------------------------
-
-	Operatorname	: getdelstatus_vertr
-
-	In		: -
-
-	Out		: delstatus		smallint	 0     o.k.
-								-1     geloescht
-			  
-	Beschreibung	: Die Procedure setzt die allgemeine delete  
-			  Statusvariable.   
-
-------------------------------------------------------------------------*/

PROCEDURE getdelstatus_vertr

	return (vertr.delstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchread_vertr 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_vertr.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchread_vertr

	fetch cursor cur_vertr

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: erasecursor_vertr 
-
-	In		: -
-
-	Out		: -
-			  
-	Beschreibung    : Die Procedure loescht die cursor cuu_vertr,
-			  cur_vertr und cus_vertr.
-							
---------------------------------------------------------------------------*/

PROCEDURE erasecursor_vertr

	erase cursor cus_vertr
	erase cursor cuu_vertr
	erase cursor cur_vertr
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: fetchcuu_vertr 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure liest einen Datensatz ueber den read
-			  cursor cur_vertr.
-							
---------------------------------------------------------------------------*/

PROCEDURE fetchcuu_vertr

	fetch cursor cuu_vertr

	return (sqlstatus)
END

/*---------------------------------------------------------------------------
-									  
-	Procedure	: closeread_vertr 
-
-	In		: -
-
-	Out		: sqlstatus	smallint
-			  
-	Beschreibung    : Die Procedure schliesst den read cursor cur_vertr.
-							
---------------------------------------------------------------------------*/

PROCEDURE closeread_vertr

	close cursor cur_vertr

	return (sqlstatus) 
END
procedure what_kennung
if "" = "@(#) mo_dbvertr.rsf	1.400	05.11.91	by Ro>" end
end
