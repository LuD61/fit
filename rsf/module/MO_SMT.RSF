
module mo_smt

/***
--------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       mo_smt.rsf
-
        Exports                 :
					proc test_smt
					proc test_pr_smt
					proc test_gr_smt
         
        Imports                 :

-       Interne Prozeduren      :  
-
-       Externe Prozeduren      :    
                                        
-
-       Autor                   :       WR    
-       Erstellungsdatum        :       22.10.90
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ROSI-SQL
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-	#206    1.234    23.01.92    RO	delstatus
-	#810    1.400    22.05.92    RO	Return-Wert bei nicht vorh, Artikeln.
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Testen, ob ein Artikel zum Sortiment einer
                                    Filiale oder eines Mandanten gehoert
				    Item a (Artikel-Nr) vorkommt.
-
--------------------------------------------------------------------------------
***/

/** Exports                   **/

export
        proc test_smt
        proc test_pr_smt
        proc test_gr_smt
end

database bws
table mdn    end
table fil    end
table a_bas  end
table a_pr   end
table smt_zuord end
end


procedure test_smt
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       test_smt
-
-       In              :    
                                mandant        Mandant
                                filiale        Filiale 
                                art            Artikel-Nummer
                                gruppe         Hauptwarengruppe
-
-       Out             :       retwert        1   Artikel im Sortiment
                                               0   Artikel nicht im Sort.
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :      Siehe Modulbeschreibung.
-
--------------------------------------------------------------------------------
**/

parameter
field  mandant         smallint
field  filiale         smallint
field  art             decimal (13 0)
field  gruppe          smallint
end


if "" = "@(#) mo_smt.rsf	1.400	22.05.92	by RO" end
       if gruppe = 0

/* #206
              execute from sql
                      select #a_bas.hwg from a_bas
                      where a_bas.a = $art
              end
*/
/* #206 A */
              execute from sql
                      select #a_bas.hwg from a_bas
                      where a_bas.a = $art
                      and a_bas.delstatus = 0
              end
/* #206 E */
              if sqlstatus
                      return (0)
              end
              let gruppe = a_bas.hwg
       end

       if filiale > 0
              if proc fil_smt_kz (mandant filiale)  = "T"
                      return (proc smt_ok (mandant filiale gruppe))
              end
       end

       if mandant > 0
              if proc mdn_smt_kz (mandant) = "T"
                      return (proc smt_ok (mandant 0 gruppe))
              end
       end

       return (1)
end
  
            
procedure fil_smt_kz
parameter
field   mandant          smallint
field   filiale          smallint
end

       execute from sql
               select #fil.smt_kz from fil
               where fil.mdn = $mandant
               and   fil.fil = $filiale
       end

       if fil.smt_kz = "T"
               return ("T")
       end

       return ("G")
end
    
            
procedure mdn_smt_kz
parameter
field   mandant          smallint
end

       execute from sql
               select #mdn.smt_kz from mdn
               where mdn.mdn = $mandant
       end

       if mdn.smt_kz = "T"
               return ("T")
       end

       return ("G")
end
    
      
procedure smt_ok
parameter
field    mandant        smallint
field    filiale        smallint
field    gruppe         smallint
end

       execute from sql              
               select #smt_zuord.hwg from smt_zuord
               where smt_zuord.mdn = $mandant
               and   smt_zuord.fil = $filiale
               and   smt_zuord.verk_st = 0
               and   smt_zuord.hwg = $gruppe
       end
       if sqlstatus
               return (0)
       end

       return (1)
end

procedure test_pr_smt
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       test_pr_smt
-
-       In              :    
                                mandant        Mandant
                                filiale        Filiale 
                                art            Artikel-Nummer
-
-       Out             :       retwert        1   Artikel im Sortiment
                                               0   Artikel nicht im Sort.
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :      Test, ob ein einzelner Artikel im Sortiment
                               eines Unternehmens ist.
                               a_pr.pr_ek = -1  : Artikel nicht im Sortiment.
-
--------------------------------------------------------------------------------
**/

parameter
field  mandant         smallint
field  filiale         smallint
field  art             decimal (13 0)
end


        init a_pr.pr_ek
        execute from sql
               select #a_pr.pr_ek from a_pr
               where a_pr.a   = $art
               and   a_pr.mdn = $mandant
               and   a_pr.fil = $filiale
       end

       if a_pr.pr_ek = -1
                   return (0)
       end
       return (1)
end

procedure test_gr_smt
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       test_gr_smt
-
-       In              :       mdn_gr         Mandantengruppe
                                mandant        Mandant
                                fil_gr         Filialgruppe
                                filiale        Filiale 
                                art            Artikel-Nummer
-
-       Out             :       retwert        1   Artikel im Sortiment
                                               0   Artikel nicht im Sort.
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :      Test, ob ein einzelner Artikel im Sortiment
                               eines Unternehmens ist.
                               a_pr.pr_ek = -1  : Artikel nicht im Sortiment.
                               Partameter mit Unternehmensgruppen.
-
--------------------------------------------------------------------------------
**/

parameter
field  m_gruppe        smallint
field  mandant         smallint
field  f_gruppe        smallint
field  filiale         smallint
field  art             decimal (13 0)
end


        init a_pr.pr_ek
        execute from sql
               select #a_pr.pr_ek from a_pr
               where a_pr.a      = $art
               and   a_pr.mdn_gr = $m_gruppe
               and   a_pr.mdn    = $mandant
               and   a_pr.fil_gr = $f_gruppe
               and   a_pr.fil    = $filiale
       end

       if a_pr.pr_ek = -1
                   return (0)
       end
       return (1)
end
