#include "stdafx.h"
#include "DbClass.h"
#include "prov_tagper.h"

struct PROV_TAG prov_tag,  prov_tag_null;
struct PROVPERIOD provperiod, provperiod_null;
extern DB_CLASS dbClass;

PROV_TAG_CLASS prov_tag_class;
PROVPERIOD_CLASS provperiod_class; 

static int anzzfelder ;

int PROVPERIOD_CLASS::insertprovperiod (void)
{
      int di = dbClass.sqlexecute (ins_cursor);

	  return di;
}

int PROVPERIOD_CLASS::updateprovperiod (void)
{
      int di = dbClass.sqlexecute (upd_cursor);

	  return di;
}

int PROVPERIOD_CLASS::openprovperiod (void)
{

		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

int PROVPERIOD_CLASS::lesewerteprovperiod (void)
{

      int di = dbClass.sqlfetch (readcursor);
// Normalerweise gibt es hier maximal einen Satz !?
	  return di;

}

void PROVPERIOD_CLASS::prepare (void)
{


// Fuer readcursor
// short   delstatus;
	dbClass.sqlin ((short    *) &provperiod.mdn , SQLSHORT,0);
	dbClass.sqlin ((long     *) &provperiod.vertr, SQLLONG,0);
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &provperiod.vondat, SQLTIMESTAMP,26);
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &provperiod.bisdat, SQLTIMESTAMP,26);

	dbClass.sqlout ((double *) &provperiod.ums_vk, SQLDOUBLE,0);
	dbClass.sqlout ((double *) &provperiod.ums_vk_sa, SQLDOUBLE,0);
	dbClass.sqlout ((double *) &provperiod.ums_ret, SQLDOUBLE,0);
	dbClass.sqlout ((double *) &provperiod.prov_ret, SQLDOUBLE,0);
	dbClass.sqlout ((double *) &provperiod.prov_vk, SQLDOUBLE,0);
	dbClass.sqlout ((double *) &provperiod.prov_vk_sa, SQLDOUBLE,0);

	
	readcursor = (short) dbClass.sqlcursor ("select "
	" sum( a_tag_wa.ums_vk_tag), sum( a_tag_wa.ums_vk_sa_tag), sum(a_tag_wa.ums_ret_tag) "
	" , ROUND(sum( a_tag_wa.ums_ret_tag * prov_tag.prov_satz/100),2) "
	" , ROUND(sum( a_tag_wa.ums_vk_tag * prov_tag.prov_satz/100),2) "
	" , ROUND(sum( a_tag_wa.ums_vk_sa_tag * prov_tag.prov_sa_satz/100),2) "
	" from a_tag_wa, prov_tag "
	" where prov_tag.mdn =a_tag_wa.mdn "
	" and prov_tag.kun =a_tag_wa.kun "
	" and prov_tag.a   =a_tag_wa.a "
	" and prov_tag.dat =a_tag_wa.dat "
	" and a_tag_wa.fil =0 "
	" and a_tag_wa.mdn = ? "
    "  and prov_tag.vertr = ? "
	" and a_tag_wa.dat between ? and ? "
	);

// Fuer Upd_cursor
// short   delstatus;

	dbClass.sqlin ((double *) &provperiod.ums_vk, SQLDOUBLE,0);
	dbClass.sqlin ((double *) &provperiod.ums_vk_sa, SQLDOUBLE,0);
	dbClass.sqlin ((double *) &provperiod.ums_ret, SQLDOUBLE,0);
	dbClass.sqlin ((double *) &provperiod.prov_ret, SQLDOUBLE,0);
	dbClass.sqlin ((double *) &provperiod.prov_vk, SQLDOUBLE,0);
	dbClass.sqlin ((double *) &provperiod.prov_vk_sa, SQLDOUBLE,0);

	dbClass.sqlin ((short    *) &provperiod.mdn , SQLSHORT,0);
	dbClass.sqlin ((long     *) &provperiod.vertr, SQLLONG,0);
	dbClass.sqlin ((short *) &provperiod.jr, SQLSHORT,0);
	dbClass.sqlin ((short *) &provperiod.mo, SQLSHORT,0);

	
	upd_cursor = (short) dbClass.sqlcursor ("update provperiod set "
	" delstatus = 0 , ums_vk = ?, ums_vk_sa = ?, ums_ret = ? "
	" , prov_ret = ?, prov_vk = ? , prov_vk_sa = ? "
	" where mdn = ? "
	" and vertr = ? "
	" and jr = ? "
	" and mo = ? "
	);
// Fuer ins_cursor
// short   delstatus;

	dbClass.sqlin ((double *) &provperiod.ums_vk, SQLDOUBLE,0);
	dbClass.sqlin ((double *) &provperiod.ums_vk_sa, SQLDOUBLE,0);
	dbClass.sqlin ((double *) &provperiod.ums_ret, SQLDOUBLE,0);
	dbClass.sqlin ((double *) &provperiod.prov_ret, SQLDOUBLE,0);
	dbClass.sqlin ((double *) &provperiod.prov_vk, SQLDOUBLE,0);
	dbClass.sqlin ((double *) &provperiod.prov_vk_sa, SQLDOUBLE,0);

	dbClass.sqlin ((short    *) &provperiod.mdn , SQLSHORT,0);
	dbClass.sqlin ((long     *) &provperiod.vertr, SQLLONG,0);
	dbClass.sqlin ((short *) &provperiod.jr, SQLSHORT,0);
	dbClass.sqlin ((short *) &provperiod.mo, SQLSHORT,0);

	
	ins_cursor = (short) dbClass.sqlcursor (" insert into provperiod "
	" ( delstatus, ums_vk, ums_vk_sa, ums_ret "
	" , prov_ret, prov_vk, prov_vk_sa "
	" , mdn, vertr, jr, mo "
	" ) values ( "
	" 0 , ?, ?, ?,  ?, ?, ?,  ?, ?, ?, ? )"
	);




}


int PROV_TAG_CLASS::leseprov_tag (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int PROV_TAG_CLASS::openprov_tag (void)
{

		if ( readcursor < 0 ) prepare ();	// ob das hier richtig ist ?

         return dbClass.sqlopen (readcursor);
}

void PROV_TAG_CLASS::prepare (void)
{


// short   delstatus;
	dbClass.sqlin ((short    *) &provperiod.mdn , SQLSHORT,0);
	dbClass.sqlin ((long     *) &provperiod.vertr, SQLLONG,0);
	dbClass.sqlin ((short    *) &provperiod.mo, SQLSHORT,0);
	dbClass.sqlin ((short    *) &provperiod.jr, SQLSHORT,0);

	dbClass.sqlout ((double *) &provperiod.ums_vk, SQLDOUBLE,0);
	dbClass.sqlout ((double *) &provperiod.ums_vk_sa, SQLDOUBLE,0);
	dbClass.sqlout ((double *) &provperiod.ums_ret, SQLDOUBLE,0);
	dbClass.sqlout ((double *) &provperiod.prov_ret, SQLDOUBLE,0);
	dbClass.sqlout ((double *) &provperiod.prov_vk, SQLDOUBLE,0);
	dbClass.sqlout ((double *) &provperiod.prov_vk_sa, SQLDOUBLE,0);

	readcursor = (short) dbClass.sqlcursor ("select "
	" ums_vk, ums_vk_sa, ums_ret, prov_ret, prov_vk, prov_vk_sa " 
	" from provperiod where mdn = ? and vertr = ? and mo = ? and jr = ? " ) ;
	
}
