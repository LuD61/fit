// 73300Dlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "73300.h"
#include "73300Dlg.h"
#include "dbclass.h"
#include "mdn.h"
#include "adr.h"
#include "form_ow.h"
#include "prov_tagper.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


extern DB_CLASS dbClass;
extern MDN_CLASS mdn_class;
extern ADR_CLASS adr_class;
extern VERTR_CLASS vertr_class;
extern PROVPERIOD_CLASS provperiod_class;


int abrejahr ;
int abremonat ;
char bufh[505] ;


DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}



// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMy73300Dlg-Dialogfeld




CMy73300Dlg::CMy73300Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMy73300Dlg::IDD, pParent)
	, v_mdnnr(_T(""))
	, v_mdnname(_T(""))
	, v_jahr(_T(""))
	, v_abmonat(_T(""))
	, v_neuabrech(FALSE)
	, v_vorschau(FALSE)
	, v_vertr(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMy73300Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MDNNR, m_mdnnr);
	DDX_Text(pDX, IDC_MDNNR, v_mdnnr);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_JAHR, m_jahr);
	DDX_Text(pDX, IDC_JAHR, v_jahr);
	DDX_Control(pDX, IDC_ABMONAT, m_abmonat);
	DDX_Text(pDX, IDC_ABMONAT, v_abmonat);
	DDX_Control(pDX, IDC_NEUABRECH, m_neuabrech);
	DDX_Check(pDX, IDC_NEUABRECH, v_neuabrech);
	DDX_Control(pDX, IDC_COMBO1, m_combi1);
	DDX_Control(pDX, IDC_VORSCHAU, m_vorschau);
	DDX_Check(pDX, IDC_VORSCHAU, v_vorschau);
	DDX_Control(pDX, IDC_VERTR, m_vertr);
	DDX_CBString(pDX, IDC_VERTR, v_vertr);
}

BEGIN_MESSAGE_MAP(CMy73300Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_EN_KILLFOCUS(IDC_MDNNR, &CMy73300Dlg::OnEnKillfocusMdnnr)
	ON_EN_KILLFOCUS(IDC_JAHR, &CMy73300Dlg::OnEnKillfocusJahr)
	ON_EN_KILLFOCUS(IDC_ABMONAT, &CMy73300Dlg::OnEnKillfocusAbmonat)
	ON_BN_CLICKED(IDC_NEUABRECH, &CMy73300Dlg::OnBnClickedNeuabrech)
	ON_CBN_KILLFOCUS(IDC_COMBO1, &CMy73300Dlg::OnCbnKillfocusCombo1)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CMy73300Dlg::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDOK, &CMy73300Dlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_DRUWA, &CMy73300Dlg::OnBnClickedDruwa)
	ON_CBN_SELCHANGE(IDC_VERTR, &CMy73300Dlg::OnCbnSelchangeVertr)
	ON_CBN_KILLFOCUS(IDC_VERTR, &CMy73300Dlg::OnCbnKillfocusVertr)
END_MESSAGE_MAP()


// CMy73300Dlg-Meldungshandler


BOOL CMy73300Dlg::PreTranslateMessage(LPMSG lpMsg)
{
	CWnd *cWnd;
	 if (lpMsg->message == WM_KEYDOWN)
	 {
		 switch (lpMsg->wParam)
			 {
		        case VK_RETURN :
					cWnd = GetFocus ();
			           if(cWnd == GetDlgItem (IDOK))
							return CDialog::PreTranslateMessage(lpMsg);

			           if(cWnd == GetDlgItem (IDCANCEL))
					   {
							OnCancel ();
 							return TRUE;
					   }

/* --->
			           if(cWnd == GetDlgItem (IDC_KUNN))
					   {	Read (); return TRUE; }
                       else if (cWnd == GetDlgItem (IDC_MDN))
                       { if (ReadMdn () == FALSE)
                       { return TRUE;
                       } }
 < ----- */
			   NextDlgCtrl();
						return TRUE;

			 case VK_F5 :
                     OnCancel ();
 			         return TRUE;
/* ----->
			 case VK_F7 :
                     OnDelete ();
 			         return TRUE;
             case VK_F9 :
		             if (OnF9 ())
                     {
                         return TRUE;
                     }
                     break;
		     case VK_F6 :
                     OnFree ();
				     return TRUE;
 <----- */

/* ---->
			 case VK_F12 :
					UpdateData( TRUE ) ;
					ladevartodaba();
					if (  tou_class.testupdtou() )
						tou_class.inserttou () ;
					else
						tou_class.updatetou () ;

					v_edithtour = 0 ;
					tou.tou = 0 ;
					UpdateData(FALSE) ;
					m_edithtour.EnableWindow (TRUE) ;
					m_buttonhtour.EnableWindow ( TRUE) ;
					m_edithtour.ModifyStyle (0, WS_TABSTOP,0) ;
					m_buttonhtour.ModifyStyle ( 0, WS_TABSTOP,0) ;
					m_edithtour.SetFocus () ;	


//                     OnOK ();
				     return TRUE;
< ----- */


 		    case VK_DOWN :
                     if (NoListCtrl (GetFocus ()))
                     {
						NextDlgCtrl();
						return TRUE;
					
                     }
				     break;
 		    case VK_UP :
                     if (NoListCtrl (GetFocus ()))
                     {
						PrevDlgCtrl();
	 					cWnd = GetFocus ();
					 	GetNextDlgTabItem(cWnd ,FALSE) ;
						return TRUE;
                     }
				     break;
		 }
	 }

	return CDialog::PreTranslateMessage(lpMsg);

}

BOOL CMy73300Dlg::NoListCtrl (CWnd *cWnd)
{
    if (((CComboBox *) cWnd->GetParent ())->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CComboBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CListBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    return TRUE;
}



BOOL CMy73300Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	mdn.mdn = 1 ;
	int i = 0;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnnr.Format(_T("1"));
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
			v_mdnname.Format(_T("                "));
	}
	else
	{
		v_mdnnr.Format(_T("        "));
		v_mdnname.Format(_T("          "));
	}

	sprintf ( form_o.form_nr,"7330B" );

	form_o.lila = 0 ;

	FORM_O_CLASS Form_o_class ;
	Form_o_class.openform_o ();
	int mehrfach_o = Form_o_class.getmehrfach_o();

	int aktuell = 0 ;
	int ziel = 0 ;
	char einzelpuffer[255] ;
	CString szdta ;

	einzelpuffer[0] = '\0' ;
	for ( int di = 0; di < 1030 && aktuell < mehrfach_o && ziel < 254 ; )
	{
		einzelpuffer[ziel] = ordersel[di] ;
		if ( ordersel[di ] == '\0' )
		{

    // Listbox mit Eintraegen fuellen
			szdta.Format("%s", einzelpuffer);
			m_combi1.InsertString(aktuell, szdta.GetBuffer(0));
			m_combi1.SetCurSel(0);
//				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->AddString(szdta.GetBuffer(0));
//				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(0);
			aktuell ++ ; ziel = -1 ; einzelpuffer[0] = '\0' ;
		}
		ziel ++ ; di ++ ;
	}

//	int num_auswahl=m_combi1.GetCurSel();


	
	vertr.mdn = mdn.mdn;
	int sqlstat = vertr_class.openallvertr ();
	sqlstat = vertr_class.leseallvertr();
	while(!sqlstat)
	{
	
			szdta.Format("%8.0d  %s",vertr.vertr,vertr.vertr_krz );
				// hier haben wir jetzt die dta und k�nnen es in die ListBox einf�gen
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_VERTR))->AddString(szdta.GetBuffer(0));
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_VERTR))->SetCurSel(0);
	
		sqlstat = vertr_class.leseallvertr () ;
	}

		
		time_t timer;
		struct tm *ltime;
		time (&timer);
 		ltime = localtime (&timer);
		sprintf ( bufh , "%02d.%02d.%04d",
			ltime->tm_mday ,ltime->tm_mon + 1 ,ltime->tm_year + 1900 );

		if ( ltime->tm_mon == 0 )
		{
			v_abmonat.Format("12") ;
			v_jahr.Format ("%d" , ltime->tm_year + 1899 );
		}
		else
		{
			v_abmonat.Format("%d",ltime->tm_mon) ;
			v_jahr.Format("%d", ltime->tm_year + 1900) ;
		}

	UpdateData(FALSE) ;


	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CMy73300Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CMy73300Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CMy73300Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMy73300Dlg::OnEnKillfocusMdnnr()
{
	UpdateData (TRUE) ;

	CString szdta ;

	int i = m_mdnnr.GetLine(0,bufh,500);
	bufh [i] = '\0' ;
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	
	while ( ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_VERTR))->DeleteString(0) > 0 );
	if (! mdn_class.lesemdn())
	{

		vertr.mdn = mdn.mdn;
		int sqlstat = vertr_class.openallvertr ();
		sqlstat = vertr_class.leseallvertr();
		if ( sqlstat )	// Es gibt keine Vertreter zu diesem Mandant
		{
			szdta.Format("0     " );
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_VERTR))->AddString(szdta.GetBuffer(0));
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_VERTR))->SetCurSel(0);
		}
		else
		{
			while(!sqlstat)
			{
					szdta.Format("%8.0d  %s",vertr.vertr,vertr.vertr_krz );
				// hier haben wir jetzt den Vertreter und k�nnen ihn in die ListBox einf�gen
				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_VERTR))->AddString(szdta.GetBuffer(0));
				((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_VERTR))->SetCurSel(0);
				sqlstat = vertr_class.leseallvertr () ;
			}
		}
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
				v_mdnname.Format("              ");
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);

		szdta.Format("0     " );
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_VERTR))->AddString(szdta.GetBuffer(0));
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_VERTR))->SetCurSel(0);
		v_mdnname.Format("              ");
		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;
}

void CMy73300Dlg::OnEnKillfocusJahr()
{
	UpdateData (TRUE) ;
	int i = m_jahr.GetLine(0,bufh,500);
	bufh [i] = '\0' ;
	if (i)	abrejahr =  atoi ( bufh );
	else abrejahr = 0 ;
	if ( abrejahr > 0 && abrejahr < 77 ) abrejahr = abrejahr + 2000 ;
	if ( abrejahr > 76 && abrejahr < 100 ) abrejahr = abrejahr + 1900 ;

	if ( abrejahr > 1900 && abrejahr < 2099 )
	{
		v_jahr.Format("%d", abrejahr ) ; 
		// alles prima
	}
	else	// fehlerhafte Eingabe
	{
		v_jahr.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;

}

void CMy73300Dlg::OnEnKillfocusAbmonat()
{
	UpdateData (TRUE) ;
	int i = m_abmonat.GetLine(0,bufh,500);
	bufh [i] = '\0' ;
	if (i)	abremonat =  atoi ( bufh );
	else abremonat = 0 ;

	if ( abremonat > 0 && abremonat < 13 )
	{
		v_abmonat.Format("%d", abremonat ) ; 
		// alles prima
	}
	else	// fehlerhafte Eingabe
	{
		v_abmonat.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;

}

void CMy73300Dlg::OnBnClickedNeuabrech()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CMy73300Dlg::OnCbnKillfocusCombo1()
{
	UpdateData(TRUE) ;
	int num_auswahl; 
	num_auswahl=m_combi1.GetCurSel();
	UpdateData(FALSE) ;
}

void CMy73300Dlg::OnCbnSelchangeCombo1()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	UpdateData(TRUE) ;
	int num_auswahl; 
		num_auswahl=m_combi1.GetCurSel();
	UpdateData(FALSE) ;

}


void neuabrechnen( long vertr, short mdn, char * ahilfdat, char * ehilfdat, short jr, short mo )
{

// prinz. wird der Listeninhalt nochmal in provperiod hineingeneriert ....

/* --->
Index : mdn, vertr, mon,jr
Werte :
ums_vk
ums_vk_sa
ums_ret
prov_ret
prov_vk -Betrag !!!!!
prov_vk_sa
select sum( a_tag_wa.ums_vk_tag), sum( a_tag_wa.ums_vk_sa_tag), sum(a_tag_wa.ums_ret_tag), ROUND(sum( a_tag_wa.ums_vk_tag * prov_tag.prov_satz / 100),2) , ROUND(sum( a_tag_wa.ums_vk_sa_tag * prov_tag.prov_sa_satz/100),2),ROUND(sum( a_tag_wa.ums_ret_tag * prov_tag.prov_satz/100),2)
from a_tag_wa, prov_tag
where 
    prov_tag.mdn =a_tag_wa.mdn
and prov_tag.kun =a_tag_wa.kun
and prov_tag.a   =a_tag_wa.a
and prov_tag.dat =a_tag_wa.dat
and a_tag_wa.fil =0

< --- */


	provperiod.vertr = vertr;
	provperiod.mdn = mdn;
	
	provperiod.bisdat.year = jr;
	provperiod.bisdat.month = mo;
	provperiod.bisdat.day = atoi (ehilfdat);
	provperiod.bisdat.hour = 0;
	provperiod.bisdat.minute = 0;
	provperiod.bisdat.second = 0;
	provperiod.bisdat.fraction = 0;

	provperiod.vondat.year = jr;
	provperiod.vondat.month = mo;
	provperiod.vondat.day = atoi (ahilfdat);
	provperiod.vondat.hour = 0;
	provperiod.vondat.minute = 0;
	provperiod.vondat.second = 0;
	provperiod.vondat.fraction = 0;
		
	provperiod.jr = jr;
	provperiod.mo = mo;
	int retcode = provperiod_class.openprovperiod();
	retcode = provperiod_class.lesewerteprovperiod();
	if ( !retcode )
	{
		dbClass.sql_mode += 2 ;	// Nicht mehr == 0, dann passiert auch nichts ....

		retcode = provperiod_class.insertprovperiod() ;
		if ( retcode )
			retcode = provperiod_class.updateprovperiod();

		dbClass.sql_mode -= 2 ;
	}

}



void CMy73300Dlg::OnBnClickedOk()
{
	UpdateData(TRUE) ;


	int i = m_jahr.GetLine(0,bufh,500);
	bufh [i] = '\0' ;
	if (i)	abrejahr =  atoi ( bufh );
	else abrejahr = 0 ;
	if ( abrejahr > 0 && abrejahr < 77 ) abrejahr = abrejahr + 2000 ;
	if ( abrejahr > 76 && abrejahr < 100 ) abrejahr = abrejahr + 1900 ;


	i = m_abmonat.GetLine(0,bufh,500);
	bufh[i] = '\0';
	if (i)	abremonat =  atoi ( bufh );
	else abremonat = 0 ;
	char ahilfdat[15];
	char ehilfdat[15];

	sprintf ( ahilfdat, "01.%02d.%d", abremonat,abrejahr );
	int monende = 30;
	switch (abremonat)
	{
	case 1 :
	case 3 :
	case 5 :
	case 7 :
	case 8 :
	case 10:
	case 12:
		monende = 31; 
		break;

	case 4 :
	case 6 :
	case 9 :
	case 11:
		monende = 30; 
		break;
	case 2 :
		monende = 28;
		if ( ! ( abrejahr % 4) )
		{
			monende = 29;
		}
		break ;
	default :
		monende = 30;
		break;
	}
	sprintf ( ehilfdat, "%02d.%02d.%d", monende, abremonat,abrejahr );

	int num_auswahl; 
	num_auswahl=m_combi1.GetCurSel();


	bufh[0] = '\0';

	long vertr = 0 ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_VERTR))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_VERTR))->GetLBText(nCurSel, bufx);

		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		i = (int) strlen ( bufh );
		// fixe formatierung : 8 stellen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 9 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[9] = '\0' ;
			}
			vertr = atol ( bufh ) ;
//			i = vertr_class.openvertr();
//			i = vertr_class.lesevertr();
		}
	}


	if ( v_neuabrech )
	{
		v_neuabrech = FALSE;
		neuabrechnen( vertr, mdn.mdn, ahilfdat, ehilfdat,abrejahr,abremonat );
	}








	FILE *fp ;

	char s1[512];
	char s2[1024];

	sprintf ( s2, "%s\\73300.lll" , getenv ( "TMPPATH"));

		fp = fopen ( s2 , "w" ) ;
		if ( fp == NULL )
		{
//			CString XA ;
//			XA.Format( "Schreibfehler: %s " ,s2 ) ;
//			MessageBox (NULL, XA, "", MB_ICONERROR);

			sprintf (s1, "Schreibfehler: %s" ,s2 ) ;
			MessageBox(s1, " ", MB_OK);

			return  ;
		}
	
		sprintf ( s1, "NAME 7330B\n" ) ; 
	
		fputs ( s1 , fp );

		fputs ( "LABEL 0\n" , fp ) ;

		if (v_vorschau)
			sprintf ( s1, "DRUCK 2\n"   ) ;	// funzt erst mit dr70001 ab xxx.07.2013
		else
			sprintf ( s1, "DRUCK 0\n"   ) ;
		fputs ( s1 , fp ) ;

		fputs ( "ANZAHL 1\n" , fp ) ;

		sprintf ( s1, "MITSORTNR %d\n"  , num_auswahl + 1 );
		fputs ( s1 , fp ) ;

		sprintf ( s1, "mdn %d %d\n", mdn.mdn , mdn.mdn   ) ;
		fputs ( s1 , fp ) ;

		sprintf ( s1, "dat %s %s\n" , ahilfdat, ehilfdat );
		fputs ( s1 , fp ) ;

		sprintf ( s1, "vertr %d %d\n" , vertr, vertr );
		fputs ( s1 , fp ) ;

		fclose ( fp) ;

		sprintf ( s1, "dr70001.exe -datei \"%s\" " , s2 );
		int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );


	OnOK();
}

void CMy73300Dlg::OnBnClickedDruwa()
{

	char s1[256] ;
		sprintf ( s1, "dr70001.exe -name 7330B -WAHL 1" );
		int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
}

void CMy73300Dlg::OnCbnSelchangeVertr()
{
		UpdateData(TRUE) ;
	int ivertr; 
	ivertr=m_vertr.GetCurSel();
	UpdateData(FALSE) ;

}

void CMy73300Dlg::OnCbnKillfocusVertr()
{
	UpdateData(TRUE) ;
	int ivertr; 
	ivertr=m_vertr.GetCurSel();
	UpdateData(FALSE) ;

}
