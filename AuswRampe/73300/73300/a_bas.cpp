#include "stdafx.h"
#include "DbClass.h"
#include "a_bas.h"

// #include "kun.h" 


extern DB_CLASS dbClass;



struct A_BAS a_bas,  a_bas_null;


static int anzzfelder ;


A_BAS_CLASS a_bas_class ;

int A_BAS_CLASS::dbcount (void)
/**
Tabelle A_BAS lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;
		 
}

int A_BAS_CLASS::lesea_bas (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int A_BAS_CLASS::opena_bas (void)
{

		if ( readcursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (readcursor);
}

void A_BAS_CLASS::prepare (void)
{

	dbClass.sqlin ((double *) &a_bas.a, SQLDOUBLE, 0);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = (short)dbClass.sqlcursor ("select count(*) from a_bas "
										"where a_bas.a = ? ");
										
	test_upd_cursor = 1;


	dbClass.sqlin ((double *) &a_bas.a, SQLDOUBLE, 0);

	dbClass.sqlout(( double *)&a_bas.a, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.mdn, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.fil, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.a_bz1, SQLCHAR, 25 ) ;
	dbClass.sqlout(( char  *)  a_bas.a_bz2, SQLCHAR, 25 ) ;
	dbClass.sqlout(( double *)&a_bas.a_gew, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.a_typ, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.a_typ2, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.abt, SQLSHORT, 0 ) ;
	dbClass.sqlout(( long  *) &a_bas.ag, SQLLONG, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.best_auto, SQLCHAR, 2 ) ;
	dbClass.sqlout(( char  *)  a_bas.bsd_kz, SQLCHAR, 2 ) ;
	dbClass.sqlout(( char  *)  a_bas.cp_aufschl, SQLCHAR, 2 ) ;
	dbClass.sqlout(( short  *)&a_bas.delstatus, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.dr_folge, SQLSHORT, 0 ) ;
	dbClass.sqlout(( long  *) &a_bas.erl_kto, SQLLONG, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.hbk_kz, SQLCHAR, 2 ) ;
	dbClass.sqlout(( short  *)&a_bas.hbk_ztr, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.hnd_gew, SQLCHAR, 2 ) ;
	dbClass.sqlout(( short  *)&a_bas.hwg, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.kost_kz, SQLCHAR, 3 ) ;
	dbClass.sqlout(( short  *)&a_bas.me_einh, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.modif, SQLCHAR, 2 ) ;
	dbClass.sqlout(( short  *)&a_bas.mwst, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *)&a_bas.plak_div, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.stk_lst_kz, SQLCHAR, 2 ) ;
	dbClass.sqlout(( double *)&a_bas.sw, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( short  *) &a_bas.teil_smt, SQLSHORT, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.we_kto, SQLLONG, 0 ) ;
	dbClass.sqlout(( short  *) &a_bas.wg, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *) &a_bas.zu_stoff, SQLSHORT, 0 ) ;
	dbClass.sqlout(( char  *)   a_bas.akv, SQLCHAR, 11 ) ; 
	dbClass.sqlout(( char  *)   a_bas.bearb, SQLCHAR, 11 ) ;
	dbClass.sqlout(( char  *)   a_bas.pers_nam, SQLCHAR , 9 ) ;
	dbClass.sqlout(( double *) &a_bas.prod_zeit, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( char  *)   a_bas.pers_rab_kz, SQLCHAR, 2 ) ;
	dbClass.sqlout(( double *) &a_bas.gn_pkt_gbr, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.kost_st, SQLLONG, 0 ) ;
	dbClass.sqlout(( char  *)   a_bas.sw_pr_kz, SQLCHAR, 2 ) ;
	dbClass.sqlout(( long  *)  &a_bas.kost_tr, SQLLONG, 0 ) ;
	dbClass.sqlout(( double *) &a_bas.a_grund, SQLDOUBLE,0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.kost_st2, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.we_kto2, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.charg_hand, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.intra_stat, SQLLONG, 0 ) ;
	dbClass.sqlout(( char  *)   a_bas.qual_kng, SQLCHAR, 5 ) ; 
	dbClass.sqlout(( char  *)   a_bas.a_bz3, SQLCHAR, 25 ) ;
	dbClass.sqlout(( short  *) &a_bas.lief_einh, SQLSHORT, 0 ) ;
	dbClass.sqlout(( double *) &a_bas.inh_lief, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.erl_kto_1, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.erl_kto_2, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.erl_kto_3, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.we_kto_1, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.we_kto_2, SQLLONG, 0 ) ;
	dbClass.sqlout(( long  *)  &a_bas.we_kto_3, SQLLONG, 0 ) ;
	dbClass.sqlout(( char  *)   a_bas.skto_f, SQLCHAR, 2 ) ;
	dbClass.sqlout(( double *) &a_bas.sk_vollk, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( double *) &a_bas.a_ersatz, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( short  *) &a_bas.a_ers_kz, SQLSHORT, 0 ) ;
	dbClass.sqlout(( short  *) &a_bas.me_einh_abverk, SQLSHORT, 0 ) ;
	dbClass.sqlout(( double *) &a_bas.inh_abverk, SQLDOUBLE, 0 ) ;
	dbClass.sqlout(( char  *)  a_bas.hnd_gew_abverk, SQLCHAR, 2 ) ;
	dbClass.sqlout(( short  *) &a_bas.ghsperre, SQLSHORT, 0 ) ;	// 011013


	readcursor = (short) dbClass.sqlcursor ("select "
	" a ,mdn ,fil ,a_bz1 ,a_bz2 ,a_gew ,a_typ ,a_typ2 ,abt ,ag ,best_auto "
	" ,bsd_kz ,cp_aufschl ,delstatus ,dr_folge ,erl_kto ,hbk_kz ,hbk_ztr "
	" ,hnd_gew , hwg ,kost_kz ,me_einh ,modif , mwst ,plak_div ,stk_lst_kz "
	" ,sw ,teil_smt ,we_kto ,wg ,zu_stoff ,akv ,bearb ,pers_nam ,prod_zeit "
	" ,pers_rab_kz ,gn_pkt_gbr ,kost_st ,sw_pr_kz ,kost_tr ,a_grund ,kost_st2 "
	" ,we_kto2 ,charg_hand ,intra_stat ,qual_kng ,a_bz3 ,lief_einh ,inh_lief "
	" ,erl_kto_1 ,erl_kto_2 ,erl_kto_3 ,we_kto_1 ,we_kto_2 ,we_kto_3 ,skto_f "
	" ,sk_vollk ,a_ersatz ,a_ers_kz ,me_einh_abverk , inh_abverk ,hnd_gew_abverk, ghsperre "

	" from a_bas where a = ? " ) ;
	
}

