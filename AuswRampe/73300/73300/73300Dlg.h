// 73300Dlg.h : Headerdatei
//

#pragma once
#include "afxwin.h"


// CMy73300Dlg-Dialogfeld
class CMy73300Dlg : public CDialog
{
// Konstruktion
public:
	CMy73300Dlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_MY73300_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnKillfocusMdnnr();
	afx_msg void OnEnKillfocusJahr();
	afx_msg void OnEnKillfocusAbmonat();
	afx_msg void OnBnClickedNeuabrech();
	CEdit m_mdnnr;
	CString v_mdnnr;
	CEdit m_mdnname;
	CString v_mdnname;
	CEdit m_jahr;
	CString v_jahr;
	CEdit m_abmonat;
	CString v_abmonat;
	CButton m_neuabrech;
	BOOL v_neuabrech;
	CComboBox m_combi1;
	afx_msg void OnCbnKillfocusCombo1();
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnBnClickedOk();
	afx_msg BOOL PreTranslateMessage(LPMSG) ;
	afx_msg BOOL NoListCtrl (CWnd *) ;

	afx_msg void OnBnClickedDruwa();
	CButton m_vorschau;
	BOOL v_vorschau;
	afx_msg void OnCbnSelchangeVertr();
	afx_msg void OnCbnKillfocusVertr();
	CComboBox m_vertr;
	CString v_vertr;
};
