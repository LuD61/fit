//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 73300.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MY73300_DIALOG              102
#define IDR_MAINFRAME                   128
#define MDN_TXT                         1000
#define IDC_MDNNR                       1001
#define IDC_MDNNAME                     1002
#define IDC_JAHRTEXT                    1003
#define IDC_JAHR                        1004
#define IDC_MONTEXT                     1005
#define IDC_ABMONAT                     1006
#define IDC_NEUABRECH                   1007
#define IDC_COMBO1                      1008
#define IDC_DRUWA                       1009
#define IDC_CHECK1                      1010
#define IDC_VORSCHAU                    1010
#define IDC_VERTRT                      1011
#define IDC_COMBO3                      1014
#define IDC_VERTR                       1014

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
