#ifndef _FORM_OW_DEF
#define _FORM_OW_DEF

// 071110 
#define FT_LOESCH -1
#define FT_STANDARD 0
#define FT_PTABN 1
#define FT_TEXTBAUST 2
#define FT_ADRESSE 3
#define FT_TEXTEXT 4
#define FT_FELDEXT 5

extern char ordersel[1032] ;

struct FORM_W {
   char     form_nr[11];
   long	    zei ;
   char     krz_txt[51];
   short	lila ; 
 
};

extern struct FORM_W form_w;

class FORM_W_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leseform_w (void);
               int openform_w (void);
			   short readcursor;
               FORM_W_CLASS () : DB_CLASS ()
               {
				   readcursor = -1 ;
               }
};

struct FORM_O {
   char     form_nr[11];
   long		zei;
   char     krz_txt[51];
   short	lila ;
 
};

extern struct FORM_O form_o;

class FORM_O_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leseform_o (void);
               int openform_o (void);
			   int getmehrfach_o(void);	// 260307
			   void putmehrfach_o(int);	// 260307
			   short readcursor;
			   int imehrfach;	// 260307
               FORM_O_CLASS () : DB_CLASS ()
               {
				   readcursor = -1 ;
				   imehrfach = -1 ;	// 260307
               }
};

// 220207 : es folgen form_te und form_t

struct FORM_TE {
   char     form_nr[11];
   short    lila;
   char     tab_nam[19];
   char     feld_nam[19];
   long		zei;
   char     krz_txt[51];
 
};

extern struct FORM_TE form_te;

class FORM_TE_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leseform_te (void);
               int openform_te (void);
			   short readcursor;
               FORM_TE_CLASS () : DB_CLASS ()
               {
				   readcursor = -1 ;
               }
};

struct FORM_T {
 short		delstatus ;
 char		form_nr[11];
 short		lila;
 char		tab_nam [19];
 char		feld_nam[19];
 char		ttab_nam[19] ;

 char		sfeld1[19];
 char		sfeld2[19];
 char		sfeld3[19];
 char		sfeld4[19];
 char		sfeld5[19];

 char		wtab1[19];
 char		wtab2[19];
 char		wtab3[19];
 char		wtab4[19];
 char		wtab5[19];

 char		wfeld1[19];
 char		wfeld2[19];
 char		wfeld3[19];
 char		wfeld4[19];
 char		wfeld5[19];

 char		ofeld1[19];
 char		ofeld2[19];
 char		ofeld3[19];
 char		ofeld4[19];
 char		ofeld5[19];

 short		odesc1 ;
 short		odesc2 ;
 short		odesc3 ;
 short		odesc4 ;
 short		odesc5 ;

};

extern struct FORM_T form_t;

class FORM_T_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int leseform_t (void);
               int openform_t (void);
			   short readcursor;
               FORM_T_CLASS () : DB_CLASS ()
               {
				   readcursor = -1 ;
               }
};

#endif