#ifndef _WA_TMP_DEF
#define _WA_TMP_DEF


struct WA_TMP {

short   delstatus;
short    mdn;
short    fil;
long    kun;
char    kun_krz1[25];
char    kun_bran2[3];
char    kun_bran2_txt[24] ;
char    kun_grp1;
char    kun_grp2;
char    vertr1;
char    vertr1_txt[25];
char    vertr2;
char    vertr2_txt[25];
long    inka_nr;
double    a;
double    a_grund;
char    a_bz1[25];
short    me_einh;
short    ag;
short    wg;
char    wg_bz1[25];
short    hwg;
short    jr;
short    mo;
double    me;
double    gew;
double    ums;
double    me_ret;
double    gew_ret;
double    ums_ret;
double    me_vm;
double    gew_vm;
double    ums_vm;
double    me_ret_vm;
double    gew_ret_vm;
double    ums_ret_vm;
double    me_lj;
double    gew_lj;
double    ums_lj;
double    me_ret_lj;
double    gew_ret_lj;
double    ums_ret_lj;
double    me_vj;
double    gew_vj;
double    ums_vj;
double    me_ret_vj;
double    gew_ret_vj;
double    ums_ret_vj;
double    me_vj_ges;
double    gew_vj_ges;
double    ums_vj_ges;
double    me_ret_vj_ges;
double    gew_ret_vj_ges;
double    ums_ret_vj_ges;
short    wo;
long    stat_kun;
short    kun_fil;
double    ums_fb;
double    ums_ret_fb;
double    ums_vm_fb;
double    ums_ret_vm_fb;
double    ums_lj_fb;
double    ums_ret_lj_fb;
double    ums_vj_fb;
double    ums_ret_vj_fb;
double    ums_vj_ges_fb;
double    ums_ret_vjgesfb;
double    ums_sk;
double    ums_sk_vm;
double    ums_sk_lj;
double    ums_sk_vj;
double    ums_sk_vj_ges;
short    lfd;
TIMESTAMP_STRUCT    akv;
long    hirarchie1;
long    hirarchie2;
long    hirarchie3;
long    hirarchie4;
double    plan_ums;
double    plan_ums_lj;
double    brums_vk;
double    brums_vk_ret;
double    ums_b4;
double    ums_b4ret;
double    brums_vk_vm;
double    brums_vk_ret_vm ;
double    ums_b4_vm;
double    ums_b4ret_vm;
double    brums_vk_lj;
double    brums_vk_ret_lj;
double    ums_b4_lj;
double    ums_b4ret_lj;
double    brums_vk_vj;
double    brums_vk_ret_vj;
double    ums_b4_vj;
double    ums_b4ret_vj;
double    brums_vk_vj_ges;
double    brums_vk_ret_vjges;
double    ums_b4_vj_ges;
double    ums_b4ret_vj_ges;
char    hwg_bz1;

} ;

extern struct WA_TMP wa_tmp, wa_tmp_null;


class WA_TMP_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int lesewa_tmp (void);
               int writewa_tmp (void);
               int openwa_tmp (void);
               WA_TMP_CLASS () : DB_CLASS ()
               {
               }
};

#endif

