#ifndef _PROV_TAG
#define _PROV_TAG

struct PROV_TAG {

short   delstatus ;
short    mdn ;
long    kun ;
long    vertr ;
double    a; 
double    prov_satz;
double    prov_sa_satz;
TIMESTAMP_STRUCT dat ;
long    ls;

};
extern struct PROV_TAG prov_tag, prov_tag_null;


class PROV_TAG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int leseprov_tag (void);
               int openprov_tag (void);
               PROV_TAG_CLASS () : DB_CLASS ()
               {
               }
};

struct PROVPERIOD {
short   delstatus;
short    mdn;
long    vertr;
short    mo;
short    jr;
double    ums_vk;
double    ums_vk_sa;
double    ums_ret;
double    prov_ret;
double    prov_vk;
double    prov_vk_sa;
TIMESTAMP_STRUCT vondat;
TIMESTAMP_STRUCT bisdat;
};
extern struct PROVPERIOD provperiod, provperiod_null;


class PROVPERIOD_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int insertprovperiod (void);
               int updateprovperiod (void);
               int lesewerteprovperiod (void);
			   int openprovperiod (void);
               PROVPERIOD_CLASS () : DB_CLASS ()
               {
               }
};

#endif

