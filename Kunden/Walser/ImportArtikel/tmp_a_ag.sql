create table "fit".a_ag 
  (
    a decimal(13,0),
    ag integer
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".a_ag from "public";

create unique index "fit".i01a_ag on "fit".a_ag (a);



