{ TABLE "Detlef L".a_grund row size = 14 number of columns = 4 index size = 18 }
create table "Detlef L".a_grund 
  (
    a decimal(13,0),
    rgb_r smallint,
    rgb_g smallint,
    rgb_b smallint
  );
revoke all on "Detlef L".a_grund from "public";

create unique index "Detlef L".i01a_grund on "Detlef L".a_grund 
    (a);




