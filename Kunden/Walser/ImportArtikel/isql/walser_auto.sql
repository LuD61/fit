create table "fit".walser_auto
  (
    nr smallint,
    offset smallint,
    a_bz2 char(24),
    a_bz3 char(24),
    tara integer,
    a_gew_fix decimal (8,3),
    a_gew_th decimal (8,3),
    me_einh_txt char(30),
    aufschlag1 decimal (8,3),
    aufschlag2 decimal (8,3),
    aufschlag3 decimal (8,3),
    aufschlag4 decimal (8,3),
    schneiddicke char(6),
    flg_fil char(6)
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".a_mat from "public";

create unique index "Administ".i01walser_auto on "fit".walser_auto (nr,offset);




