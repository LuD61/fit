#ifndef _FRACHT_DEF
#define _FRACHT_DEF

struct FRACHT {
 short mdn ;
 short fil ;
 long kun ;
 char adr_nam1[37] ;
 char adr_nam2[37] ;
 short anz  ;
 char str [37] ;
 char plz [ 9] ;
 char ort1[37] ;
 TIMESTAMP_STRUCT lief_term  ;
 char krz_txt[17] ;
 char tel[17] ;
 char wrt[21] ;
 TIMESTAMP_STRUCT dat ;
 char frei_txt1[65] ;
 char frei_txt2[65] ;
 char frei_txt3[65] ;
 char frei_txt4[65] ;
 char frei_txt5[65] ;
 char frei_txt6[65] ;
 long ls ;
 short stat ;
 double lief_gew  ;


};
extern struct FRACHT fracht, fracht_null;

class FRACHT_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               int dbcount (void);
               int lesefracht (void);
               int updatefracht (void);
               int insertfracht (void);
               int openfracht (void);
               FRACHT_CLASS () : DB_CLASS ()
               {
               }
};
#endif
