// pakdruView.cpp : Implementierung der Klasse CpakdruView
//

#include "stdafx.h"
#include "pakdru.h"

#include "pakdruDoc.h"
#include "pakdruView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CpakdruView

IMPLEMENT_DYNCREATE(CpakdruView, CView)

BEGIN_MESSAGE_MAP(CpakdruView, CView)
	// Standarddruckbefehle
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CpakdruView-Erstellung/Zerst�rung

CpakdruView::CpakdruView()
{
	// TODO: Hier Code zur Konstruktion einf�gen

}

CpakdruView::~CpakdruView()
{
}

BOOL CpakdruView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CView::PreCreateWindow(cs);
}

// CpakdruView-Zeichnung

void CpakdruView::OnDraw(CDC* /*pDC*/)
{
	CpakdruDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: Code zum Zeichnen der systemeigenen Daten hinzuf�gen
}


// CpakdruView drucken

BOOL CpakdruView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Standardvorbereitung
	return DoPreparePrinting(pInfo);
}

void CpakdruView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Zus�tzliche Initialisierung vor dem Drucken hier einf�gen
}

void CpakdruView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Bereinigung nach dem Drucken einf�gen
}


// CpakdruView-Diagnose

#ifdef _DEBUG
void CpakdruView::AssertValid() const
{
	CView::AssertValid();
}

void CpakdruView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CpakdruDoc* CpakdruView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CpakdruDoc)));
	return (CpakdruDoc*)m_pDocument;
}
#endif //_DEBUG


// CpakdruView-Meldungshandler
