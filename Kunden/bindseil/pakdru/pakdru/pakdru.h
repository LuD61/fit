// pakdru.h : Hauptheaderdatei f�r die pakdru-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error "\"stdafx.h\" vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"       // Hauptsymbole


// CpakdruApp:
// Siehe pakdru.cpp f�r die Implementierung dieser Klasse
//

class CpakdruApp : public CWinApp
{
public:
	CpakdruApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	afx_msg void OnDruckWahl();
	DECLARE_MESSAGE_MAP()
};

extern CpakdruApp theApp;