//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by pakdru.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_PAKDRUDLG                   103
#define IDR_MAINFRAME                   128
#define IDR_pakdruTYPE                  129
#define IDB_CHECK                       131
#define IDD_DRUCKDIALOG                 131
#define IDB_UNCHECK                     133
#define IDC_MANDANT                     1000
#define IDC_MDNNAME                     1001
#define IDC_DATUM                       1002
#define IDC_LIST1                       1005
#define IDC_BUTTON2                     1007
#define IDC_ALLESDRUCK                  1007
#define IDC_EDIT1                       1008
#define IDC_BELEG                       1008
#define ID_DATEI_DRUCKEREINRICHTUNG     32771
#define ID_DATEI_DRUCKEREINRICHTUNG32772 32772
#define ID_DATEI_DRUCKEREINRICHTUNG2    32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
