// pakdruDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "pakdru.h"
#include "pakdruDlg.h"
#include "strfuncs.h"
#include "dbClass.h"
#include "mdn.h"
#include "adr.h"
#include "ls.h"
#include "fracht.h"


#include "FillList.h"

extern DB_CLASS dbClass ;
extern MDN_CLASS mdn_class ;
extern ADR_CLASS adr_class ;
extern LSK_CLASS lsk_class ;
extern FRACHT_CLASS fracht_class ;

char bufh [256] ;

// CpakdruDlg

IMPLEMENT_DYNCREATE(CpakdruDlg, CFormView)

CpakdruDlg::CpakdruDlg()
	: CFormView(CpakdruDlg::IDD)
	, v_mandant(_T(""))
	, v_mdnname(_T(""))
	, v_datum(_T(""))
	, v_beleg(0)
{

}

CpakdruDlg::~CpakdruDlg()
{
}

void CpakdruDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MANDANT, m_mandant);
	DDX_Text(pDX, IDC_MANDANT, v_mandant);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_DATUM, m_datum);
	DDX_Text(pDX, IDC_DATUM, v_datum);
	DDX_Control(pDX, IDC_LIST1, m_list1);
	DDX_Control(pDX, IDC_BELEG, m_beleg);
	DDX_Text(pDX, IDC_BELEG, v_beleg);
	DDV_MinMaxLong(pDX, v_beleg, 0, 99999999);
}

BEGIN_MESSAGE_MAP(CpakdruDlg, CFormView)
	ON_BN_CLICKED(IDCANCEL, &CpakdruDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CpakdruDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_ALLESDRUCK, &CpakdruDlg::OnBnClickedAllesdruck)
	ON_EN_KILLFOCUS(IDC_MANDANT, &CpakdruDlg::OnEnKillfocusMandant)
	ON_EN_KILLFOCUS(IDC_DATUM, &CpakdruDlg::OnEnKillfocusDatum)
	ON_NOTIFY(HDN_BEGINTRACK, 0, &CpakdruDlg::OnListBegintrack)
	ON_NOTIFY(HDN_ENDDRAG, 0, &CpakdruDlg::OnListEnddrag)
	ON_EN_KILLFOCUS(IDC_BELEG, &CpakdruDlg::OnEnKillfocusBeleg)
END_MESSAGE_MAP()


// CpakdruDlg-Diagnose

#ifdef _DEBUG
void CpakdruDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CpakdruDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void CpakdruDlg::ReadMdn (void) 
{
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//	gibbet nich		PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich 		PrevDlgCtrl();
	}
		UpdateData (FALSE) ;
}

// CpakdruDlg-Meldungshandler


void CpakdruDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.


	dbClass.opendbase (_T("bws"));

	// Cursoren createn ....

	/* --->
	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_list1.SetImageList (&image, LVSIL_SMALL);   
	}
< ---- */

// 	m_list1.SetImageList (&image, LVSIL_SMALL);   
	FillList = m_list1;
	FillList.SetStyle (LVS_REPORT);
	if (m_list1.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("LS-Nr."), 1, 60, LVCFMT_RIGHT);
	FillList.SetCol (_T("Adresse"), 2, 220, LVCFMT_LEFT);
	FillList.SetCol (_T("Status"), 3, 50, LVCFMT_RIGHT);
	FillList.SetCol (_T("Anzahl"), 4, 50, LVCFMT_RIGHT);
	FillList.SetCol (_T("Aktiv"), 5, 50, LVCFMT_CENTER);
	m_list1.ColType.Add (new CColType (5, m_list1.CheckBox)) ;


	/* --->
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);
	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
//	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
//	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
//	MdnGrid.Add (c_MdnChoice);

	SchlklknrGrid.Create (this, 1, 2);
    SchlklknrGrid.SetBorder (0, 0);
    SchlklknrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Schlklknr = new CCtrlInfo (&m_schlklknr, 0, 0, 1, 1);
	SchlklknrGrid.Add (c_Schlklknr);
< ----- */
/* --->
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);
< ----- */

	/* --->
	ButtonGrid.Create (this, 5, 5);
    ButtonGrid.SetBorder (0, 0);
    ButtonGrid.SetCellHeight (20);
    ButtonGrid.SetGridSpace (0, 10);
< -- */

/* --->
	CCtrlInfo *c_Cancel = new CCtrlInfo (&m_Cancel, 0, 0, 1, 1);
	ButtonGrid.Add (c_Cancel);
	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 1, 1, 1);
	ButtonGrid.Add (c_Save);
	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 0, 2, 1, 1);
	ButtonGrid.Add (c_Delete);
	CCtrlInfo *c_Insert = new CCtrlInfo (&m_Insert, 0, 3, 1, 1);
	ButtonGrid.Add (c_Insert);
< --- */

/* --->
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_GrundPreis = new CCtrlInfo (&GrundPreis, DOCKRIGHT, 0, 5, 1); 
	CtrlGrid.Add (c_GrundPreis);

	CCtrlInfo *c_LA     = new CCtrlInfo (&m_LA, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid   = new CCtrlInfo (&AGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_LA_bz1  = new CCtrlInfo (&m_LA_bz1, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LA_bz1);
	CCtrlInfo *c_A_bz1  = new CCtrlInfo (&m_A_bz1, 1, 2, 3, 1); 
	CtrlGrid.Add (c_A_bz1);
	CCtrlInfo *c_A_bz2  = new CCtrlInfo (&m_A_bz2, 1, 3, 3, 1); 
	CtrlGrid.Add (c_A_bz2);
	CCtrlInfo *c_ButtonGrid = new CCtrlInfo (&ButtonGrid, DOCKRIGHT, 4, 1, 3); 
	CtrlGrid.Add (c_ButtonGrid);
	CCtrlInfo *c_List  = new CCtrlInfo (&m_List, 0, 4, DOCKRIGHT, DOCKBOTTOM); 
//	c_List->rightspace = 125;
	c_List->rightspace = RightListSpace;
	CtrlGrid.Add (c_List);
< ----- */

/* --->
	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();
< ---- */
/* --->
	memcpy (&Schlaklkk.schlaklkk, &schlaklkk_null, sizeof (schlaklkk));

	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = 1;
< --- */
	mdn.mdn = 1 ;
	v_mandant = "1" ;
/* ---->
	if (PersName.Trim () != "")
	{
		_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
		if (Sys_ben.dbreadfirstpers_nam () == 0)
		{
			if (Sys_ben.sys_ben.mdn != 0)
			{
				Mdn.mdn.mdn = Sys_ben.sys_ben.mdn;
				m_Mdn.SetReadOnly ();
				m_Mdn.ModifyStyle (WS_TABSTOP, 0);
			}
		}
	}
< ---- */

// 	kalkartfeldersetzen () ;
// 	Form.Show ();
 	ReadMdn ();


/* --->
	if (RemoveKun)
	{
		m_list1.DeleteColumn (m_List.PosKun);
		m_list1.ScrollPositions (2);
		m_list1.DeleteColumn (m_List.PosKunName);
		m_list1.ScrollPositions (3);
	}
< ----- */
//	EnableHeadControls (TRUE);

// return TRUE;  // so mag es oninitdialog sein .... Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten

}

void CpakdruDlg::OnBnClickedCancel()
{
	GetParent ()->DestroyWindow ();
}

void CpakdruDlg::OnBnClickedOk()
{
	// Speichern und ende
	Write () ;
	GetParent ()->DestroyWindow ();
}

void CpakdruDlg::OnBnClickedAllesdruck()
{
	Write () ;
	StapelDruck () ;
	// Speichern und Stapeldruck
}

void CpakdruDlg::OnEnKillfocusMandant()
{
	UpdateData (TRUE) ;
	int i = m_mandant.GetLine(0,bufh,500);
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;

}

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	int k = 0 ;	// 261007 : Zusatzchecks
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		// 261007 : Zusatzchecks wegen vista, nur noch bedingtes return
		if ( i < 6 ) k = -1 ;
		if ( k == 0 )
		{
			// die ersten beiden Zeichen MUESSEN ZAHLEN sein
			if ( bufi[0] > '9' || bufi[0] < '0' ||bufi[1] > '9' || bufi[1] < '0' )
				k = - 1 ;
		}
		if ( k == 0 )
		{
			if ( bufi[2] > '9' || bufi[2] < '0' )
				k = 2 ;		// Typ 8 oder typ 10 moeglich

		}
		if ( k == 0 )
		{
			// nur noch Typ 6 erlaubt 
			if (    bufi[3] > '9' || bufi[3] < '0'
				 || bufi[4] > '9' || bufi[4] < '0'
				 || bufi[5] > '9' || bufi[5] < '0' )
			{
				k = - 1 ;
			}
			else
			{	// Gueltiger Typ 6  ......
				k = 1 ;
				i = 6 ;
				bufi[6] = '\0' ;
			}
		}
		if ( k == 2 )
		{
			if ( i < 8 )
			{	
				k = -1 ;
			}
			else
			{	// 4.,5.,7.,8. Zeichen MUESSEN ZAHLEN sein, 6.Zeichen MUSS Nicht-Zahl sein
				if (   bufi[3] > '9' || bufi[3] < '0' 
					 ||bufi[4] > '9' || bufi[4] < '0'
				     ||bufi[6] > '9' || bufi[6] < '0'
				     ||bufi[7] > '9' || bufi[7] < '0'
				     || !( bufi[5] >'9' || bufi[5]< '0')
				    )
				k = - 1 ;
			}
		}
		if ( k == 2 )
		{
			if ( bufi[8] > '9' || bufi[8] < '0' )
			{	// gueltiger Typ 8 
				k = 1 ;
				i = 8 ;
				bufi[8] = '\0' ;
			}
		
		}

		if ( k == 2 )
		{
			if ( i < 10 )
			{	
				k = -1 ;
			}
			else
			{	// 9.,10. Zeichen MUESSEN ZAHLEN sein
				if (   bufi[8] > '9' || bufi[8] < '0' 
					 ||bufi[9] > '9' || bufi[9] < '0'
			       )
				{
					k = -1 ;
				}
				else
				{
					k = 1 ;
					i = 10 ;
					bufi[10] = '\0' ;
				}
			}
		}
		if ( k < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}

void CpakdruDlg::OnEnKillfocusDatum()
{

	UpdateData(TRUE) ;
	int	i = m_datum.GetLine(0,bufh,500) ;
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_datum.Format("%s",_T(bufh));
		}
	}
	Read () ;	// Saetze einlesen 
	UpdateData (FALSE) ;
	return ;
}

BOOL CpakdruDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_list1.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_list1 &&
					GetFocus ()->GetParent () != &m_list1 )
				{

					break;
			    }
				m_list1.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				m_list1.OnKeyD (VK_DOWN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				m_list1.OnKeyD (VK_UP);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
// ???				OnCancel () ;
				return TRUE;
			}
/* -----> Loeschen ist niemals .....
 			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
< ------ */
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

/* ---->
			else if (pMsg->wParam == VK_F8)
			{
				** ---->
				if (Choice != NULL)
				{	if (Choice->IsWindowVisible ())
					{	Choice->ShowWindow (SW_HIDE);
					}
					else
					{	Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{	OnChoice ();
				}
				< ----- **
				OnChoice();	// alternative zur echten selektion 
			}
			else if (pMsg->wParam == VK_F9)
			{	if (GetFocus () == &m_mdn)
				{	OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_schlklknr)
				{	OnChoice ();
					return TRUE;
				}

				m_List.OnKey9 ();
				return TRUE;
			}
< ----- */
	}

//return CDbPropertyPage::PreTranslateMessage(pMsg);
return CFormView::PreTranslateMessage(pMsg);
//	return FALSE ;
}

BOOL CpakdruDlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

	/* --->
	if (Control == &m_mdn)
	{	if (!ReadMdn ())
		{	m_mdn.SetFocus ();
			return FALSE;
		}
	}
	< ---- */
	/* ----->
	if (Control == &m_schlklknr)
	{	if (!Read ())
		{	m_schlklknr.SetFocus ();
			return FALSE;
		}
	}
	< ------ */
	/* ----->
	if (Control == &m_ckalkart)
	{	kalkartfeldernachsetzen ();
		rechnenach (); 
	}
	< -------- */

	/* ---> 
	if (Control == &m_preisek ||
		Control == &m_anzahl ||
		Control == &m_lebendgew ||
		Control == &m_schlachtgewicht ||
		Control == &m_kaltgew ||
		Control == &m_ausbeute ||
		Control == &m_verlust )
	{
		rechnenach () ;
	}
	< ---- */


	if (Control != &m_list1 &&
		Control->GetParent ()!= &m_list1)
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CpakdruDlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_list1 &&
		Control->GetParent ()!= &m_list1 )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_PrGrList.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}




/* ---> erst mal wech

BOOL CpakdruDlg::PreTranslateMessage(LPMSG lpMsg)
{

	return PreTranslateMessage(lpMsg);

	CWnd *cWnd;
	 if (lpMsg->message == WM_KEYDOWN)
	 {
		 switch (lpMsg->wParam)
			 {
		        case VK_RETURN :
					cWnd = GetFocus ();
			           if(cWnd == GetDlgItem (IDOK))
							return PreTranslateMessage(lpMsg);

** ---->
			           if(cWnd == GetDlgItem (IDCANCEL))
					   {
							OnCancel ();
 							return TRUE;
					   }
< ---- **

** --->
			           if(cWnd == GetDlgItem (IDC_KUNN))
					   {	Read (); return TRUE; }
                       else if (cWnd == GetDlgItem (IDC_MDN))
                       { if (ReadMdn () == FALSE)
                       { return TRUE;
                       } }
 < ----- **
// gibbet nich			   NextDlgCtrl();
						return TRUE;

** --->
			 case VK_F5 :
                     OnCancel ();
 			         return TRUE;
< ---- **

 ** ----->
			 case VK_F7 :
                     OnDelete ();
 			         return TRUE;
             case VK_F9 :
		             if (OnF9 ())
                     {
                         return TRUE;
                     }
                     break;
		     case VK_F6 :
                     OnFree ();
				     return TRUE;
 <----- **

** ---->
			 case VK_F12 :
					UpdateData( TRUE ) ;
					ladevartodaba();
					if (  tou_class.testupdtou() )
						tou_class.inserttou () ;
					else
						tou_class.updatetou () ;

					v_edithtour = 0 ;
					tou.tou = 0 ;
					UpdateData(FALSE) ;
					m_edithtour.EnableWindow (TRUE) ;
					m_buttonhtour.EnableWindow ( TRUE) ;
					m_edithtour.ModifyStyle (0, WS_TABSTOP,0) ;
					m_buttonhtour.ModifyStyle ( 0, WS_TABSTOP,0) ;
					m_edithtour.SetFocus () ;	


//                     OnOK ();
				     return TRUE;
< ----- **


 		    case VK_DOWN :
                     if (NoListCtrl (GetFocus ()))
                     {
// gibbet nich						NextDlgCtrl();
						return TRUE;
					
                     }
				     break;
 		    case VK_UP :
                     if (NoListCtrl (GetFocus ()))
                     {
// gibbet nich						PrevDlgCtrl();
	 					cWnd = GetFocus ();
					 	GetNextDlgTabItem(cWnd ,FALSE) ;
						return TRUE;
                     }
				     break;
		 }
	 }

return PreTranslateMessage(lpMsg);

}
< ------- */
/* ------>
BOOL CpakdruDlg::NoListCtrl (CWnd *cWnd)
{
    if (((CComboBox *) cWnd->GetParent ())->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CComboBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    else if (((CListBox *) cWnd)->GetCount () > 1)
    {
        return FALSE;
    }
    return TRUE;
}
< ---- */

void CpakdruDlg::OnListBegintrack(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);

	m_list1.StartPauseEnter ();

	*pResult = 0;
}

void CpakdruDlg::OnListEnddrag(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	
	m_list1.EndPauseEnter ();
	*pResult = 0;
}

BOOL CpakdruDlg::Read ()
{
	ReadList ();
	return TRUE;
}

/* --->
// Der Inputstring hat zwingend das Format "dd.mm.yyyyy" 
char * sqldatdstger(TIMESTAMP_STRUCT *dstakt, char *outpstr)
{
	sprintf ( outpstr, "%02d.%02d.%04d", dstakt->day, dstakt->month,dstakt->year ) ;
	if ( dstakt->year < 1901 )	// NULL bekaempfen
		sprintf ( outpstr, "  .  .    " ) ;
	return ( outpstr ) ;
}
< ----- */

void setdatum (  TIMESTAMP_STRUCT * ziel , CString quelle )
{
// Input zwingend "dd.mm.yyyy" oder blank

    char tag[] = {"01"};
    char mon[]  = {"01"};
    char jahr[]  = {"2000"};
   	int len = quelle.GetLength ();
	 if (len >=2)
	 {
		 memcpy (tag, quelle.Mid (0, 2), 2);
	 }
	 if (len >=5)
	 {
			 memcpy (mon, quelle.Mid (3, 2), 2);
	 }
	 if (len >=10)
	 {
		 memcpy (jahr, quelle.Mid (6, 4), 4);
	 }
	 else
	 {
		if (len >=10)
		{
			memcpy (jahr + 2 , quelle.Mid (6, 2), 2);
		}
	 }
	 ziel->year = atoi ( jahr ) ; if ( ziel->year > 2099 || ziel->year < 1900 ) ziel->year = 1999 ;
	 ziel->day = atoi ( tag ) ;   if ( ziel->day > 31 || ziel->day < 1 ) ziel->day = 1 ;
	 ziel->month = atoi ( mon ) ; if ( ziel->month > 12 || ziel->month < 1 ) ziel->month = 1 ;
}

BOOL CpakdruDlg::ReadList ()
{

	int nureinls = 0  ;

	m_list1.DeleteAllItems ();
	m_list1.vSelect.clear ();
	int i = 0;
	memcpy (&lsk, &lsk_null, sizeof (struct LSK));
	lsk.mdn = mdn.mdn;
 	setdatum ( & lsk.lieferdat , v_datum ) ;
	if ( v_beleg == 0 )
		nureinls = 0 ;
	else
	{
		nureinls = 1 ;
		lsk.ls = v_beleg ;
	}
//	m_List.mdn = Mdn.mdn.mdn;
//	m_List.schlklknr = Schlaklkk.schlaklkk.schlklknr; 


	int sqlret = 100 ;

	if ( nureinls )
	{
		lsk_class.openlsk () ;
		sqlret =  lsk_class.leselsk() ;	
		if ( !sqlret )	// ls gefunden .....
		{
			v_datum.Format( "%02d.%02d.%04d" , lsk.lieferdat.day , lsk.lieferdat.month, lsk.lieferdat.year ) ;
		}
	}
	else
	{
		lsk_class.openalllsk () ;
		sqlret =  lsk_class.lesealllsk() ;	
	}

//	CChoiceZuBasis HoleBezk ;
	while ( ! sqlret )
	{
		memcpy (&adr, &adr_null, sizeof (struct ADR ));
		adr.adr = lsk.adr ;
		adr_class.openadr () ;
		int sqlreta =  adr_class.leseadr() ;	

		memcpy (&fracht, &fracht_null, sizeof (struct FRACHT));
		fracht.ls = lsk.ls ;
		fracht.mdn = lsk.mdn ;
		fracht_class.openfracht () ;
		int sqlretf =  fracht_class.lesefracht() ;	
	
		FillList.InsertItem (i, 0);

/* -->
		CString pOSI;
		pOSI.Format (_T("%d"), i + 1 );
		FillList.SetItemText (pOSI.GetBuffer (), i, m_List.PosPosi);
		CString kOST_BEZ;
		kOST_BEZ = Schlaklkp.schlaklkp.kost_bez;
		FillList.SetItemText (kOST_BEZ.GetBuffer (), i, m_List.PosKost_bez);

		CString pTWERT ;
		TCHAR ptbezk [37] ; 
		pTWERT.Format (_T("%d"), Schlaklkp.schlaklkp.zubasis);
    	HoleBezk.GetPtBezk ( pTWERT.GetBuffer(), ptbezk);
	
		CString zUBASIS;
		zUBASIS.Format (_T("%d  %s"),Schlaklkp.schlaklkp.zubasis, ptbezk);
		FillList.SetItemText (zUBASIS.GetBuffer (), i, m_List.PosZubasis);

		CString zUSCHLAG;
		m_List.DoubleToString (Schlaklkp.schlaklkp.zuschlag, zUSCHLAG, 2);
		FillList.SetItemText (zUSCHLAG.GetBuffer (), i, m_List.PosZuschlag);

		double diposwert, diposwertkg ;
		switch ( Schlaklkp.schlaklkp.zubasis )
		{
//		case 0 :	// je kg
		case 1 :	// je Stck.
			diposwert = Schlaklkp.schlaklkp.zuschlag * Schlaklkk.schlaklkk.anzahl ;
			if ( Schlaklkk.schlaklkk.kaltgew != 0.0 )
			{
			    diposwertkg = (Schlaklkp.schlaklkp.zuschlag * 
					Schlaklkk.schlaklkk.anzahl) / Schlaklkk.schlaklkk.kaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;
		case 2 :	// pauschal
			diposwert = Schlaklkp.schlaklkp.zuschlag ;
			if ( Schlaklkk.schlaklkk.kaltgew != 0.0 )
			{
			    diposwertkg = Schlaklkp.schlaklkp.zuschlag / Schlaklkk.schlaklkk.kaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;

		default :	// auffang-Linie : je kg
		    diposwert = Schlaklkp.schlaklkp.zuschlag * Schlaklkk.schlaklkk.kaltgew ;
			diposwertkg = Schlaklkp.schlaklkp.zuschlag ;
			break ;
		}

		CString wERT;
		m_List.DoubleToString (diposwert, wERT, 2);
		Schlaklkp.schlaklkp.wert = CStrFuncs::StrToDouble (wERT);
		FillList.SetItemText (wERT.GetBuffer (), i, m_List.PosWert);

		CString wERTKG;
		m_List.DoubleToString (diposwertkg, wERTKG, 4);
		Schlaklkp.schlaklkp.wertkg = CStrFuncs::StrToDouble (wERTKG);
		dizuabkg += Schlaklkp.schlaklkp.wertkg ;
		FillList.SetItemText (wERTKG.GetBuffer (), i, m_List.PosWertkg);
		sqlret = Schlaklkp.dbread() ;			
		i ++;
	}

	Schlaklkk.schlaklkk.zuabkg = dizuabkg;
< ---- */
		CString pLS;
		pLS.Format (_T("%d"), lsk.ls );
		FillList.SetItemText (pLS.GetBuffer (), i, m_list1.PosLs);

		CString pBEZ1;
		pBEZ1 = adr.adr_krz;
		FillList.SetItemText (pBEZ1.GetBuffer (), i, m_list1.PosBez1);

		CString pSTAT;
		pSTAT.Format (_T("%d"), lsk.ls_stat );
		FillList.SetItemText (pSTAT.GetBuffer (), i, m_list1.PosStat);

		CString pANZ;
		pANZ.Format (_T("%d"), fracht.anz );
		FillList.SetItemText (pANZ.GetBuffer (), i, m_list1.PosAnz);

		CString pAKTIV  = _T(" ")  ;
		if ( fracht.stat )
			pAKTIV = _T("X") ;
		FillList.SetItemText (pAKTIV.GetBuffer (), i, m_list1.PosAktiv);
	
		if ( nureinls ) break ;
		sqlret =  lsk_class.lesealllsk() ;		//Schlaklkp.dbreadfirst () ;
		i ++ ;
	}
	return TRUE;
}

BOOL CpakdruDlg::InList (LSK_CLASS & lsk_class)
{
	ListRows.FirstPosition ();
/* --->
	CAkiPreise *aki_pr;
	while ((aki_pr = (CAkiPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Akiprgrstp.akiprgrstp.aki_nr == aki_pr->akiprgrstp.aki_nr &&
			Akiprgrstp.akiprgrstp.a == aki_pr->akiprgrstp.a) return TRUE;
	}
< ---- */

   return FALSE;
}
/* --->
void CpakdruDlg::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CSchlklkpos *pr;
	while ((pr = (CSchlklkpos *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Schlaklkp.schlaklkp, &pr->schlaklkp, sizeof (SCHLAKLKP));
		if (!InList (Schlaklkp))
		{
			Schlaklkp.dbdelete ();
		}
	}
}
< ----- */


void CpakdruDlg::OnEnKillfocusBeleg()
{
	UpdateData(TRUE) ;
	Read () ;	// Saetze einlesen 
	UpdateData (FALSE) ;
}


BOOL CpakdruDlg::Write ()
{
//	UpdateData(TRUE ) ; erzeugt Probeleme ?!

//	Schlaklkp.beginwork ();
	m_list1.StopEnter ();
	int count = m_list1.GetItemCount ();

	for (int i = 0; i < count; i ++)
	{

		memcpy (&fracht, &fracht_null, sizeof (struct FRACHT));

       CString Text;
		 Text = m_list1.GetItemText (i, m_list1.PosLs);
		 fracht.ls = atol ( Text ) ;			// CStrFuncs::StrToDouble (Text);
		 fracht.mdn = mdn.mdn ;
		 fracht_class.openfracht() ;
		 int sqls = fracht_class.lesefracht() ;
		 Text = m_list1.GetItemText (i, m_list1.PosAnz);
		 fracht.anz = atoi ( Text ) ;	// CStrFuncs::StrToDouble (Text);

		 Text = m_list1.GetItemText (i, m_list1.PosAktiv);
		 if ( Text !="X" )
			 fracht.stat = 0 ;
		 else
			 fracht.stat = 1 ;


		 if ( !sqls )
		 {
			 fracht_class.updatefracht () ;
		 }
		 else
		 {
			Text = m_list1.GetItemText (i, m_list1.PosLs);
			fracht.ls = atol (Text ) ;	// CStrFuncs::StrToDouble (Text);
			fracht.mdn = mdn.mdn ;
			fracht.fil = 0 ;
			memcpy ( &fracht.lief_term, &lsk.lieferdat, sizeof (  TIMESTAMP_STRUCT )) ;
			memcpy ( &fracht.dat       , &lsk.lieferdat, sizeof ( TIMESTAMP_STRUCT )) ;

			fracht_class.insertfracht () ;
		 }
	}

	return TRUE;
}

void CpakdruDlg::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	Rows.Init ();
}

DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}


void CpakdruDlg::StapelDruck ( )
{

	int nureinls ;

	memcpy (&lsk, &lsk_null, sizeof (struct LSK));
	lsk.mdn = mdn.mdn;
 	setdatum ( & lsk.lieferdat , v_datum ) ;
	if ( v_beleg == 0 )
		nureinls = 0 ;
	else
	{
		nureinls = 1 ;
		lsk.ls = v_beleg ;
	}

	int sqlret = 100 ;

	if ( nureinls )
	{
		lsk_class.openlsk () ;
		sqlret =  lsk_class.leselsk() ;	
	}
	else
	{
		lsk_class.openalllsk () ;
		sqlret =  lsk_class.lesealllsk() ;	
	}

	while ( ! sqlret )
	{
/* ---> nicht notwendig .....
		memcpy (&adr, &adr_null, sizeof (struct ADR ));
		adr.adr = lsk.adr ;
		adr_class.openadr () ;
		int sqlreta =  adr_class.leseadr() ;	
< ----- */

		memcpy (&fracht, &fracht_null, sizeof (struct FRACHT));
		fracht.ls = lsk.ls ;
		fracht.mdn = lsk.mdn ;
		fracht_class.openfracht () ;
		int sqlretf =  fracht_class.lesefracht() ;
		if ( ( ! sqlretf ) && ( fracht.anz > 0 ) && ( fracht.stat > 0 ) )
		{

			char s2 [257] ;
			char s1 [257] ;
			int testmode = 0 ;
			sprintf ( s2, "%s", getenv ( "TESTMODE" ) ) ;
			testmode = atoi ( s2) ;
			if ( testmode > 0 && testmode < 10 )
				testmode = 1 ;
			else
				testmode = 0 ;

			sprintf ( s2, "%s\\536099.prm", getenv ( "TMPPATH" ) ) ;
			FILE * fp2 ;
			fp2 = fopen ( s2 , "w" ) ;
			if ( fp2 == NULL )
			{
				return ;	// alles Kacke -1  ;
			}

			sprintf ( s1, "NAME 536099\n" ) ; 
			fputs ( s1 , fp2 );

			fputs ( "LABEL 1\n" , fp2 ) ;
			if ( testmode) 
				sprintf ( s1, "DRUCK 1\n"   ) ;
			else
				sprintf ( s1, "DRUCK 0\n"   ) ;
			fputs ( s1 , fp2 ) ;

			sprintf ( s1, "kun %ld %ld\n", lsk.kun , lsk.kun ) ;
			fputs ( s1 , fp2) ;

			sprintf ( s1, "ANZAHL %d %d\n", fracht.anz , fracht.anz ) ;
			fputs ( s1 , fp2 ) ;

			fclose ( fp2 ) ;

			sprintf ( s1, "dr70001.exe -datei \"%s\" " , s2 );
			int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );

		}

		if ( nureinls ) break ;
		sqlret =  lsk_class.lesealllsk() ;		//Schlaklkp.dbreadfirst () ;
	}
}

