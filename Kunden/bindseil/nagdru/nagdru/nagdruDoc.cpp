// pakdruDoc.cpp : Implementierung der Klasse CpakdruDoc
//

#include "stdafx.h"
#include "nagdru.h"

#include "nagdruDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CpakdruDoc

IMPLEMENT_DYNCREATE(CpakdruDoc, CDocument)

BEGIN_MESSAGE_MAP(CpakdruDoc, CDocument)
END_MESSAGE_MAP()


// CpakdruDoc-Erstellung/Zerst�rung

CpakdruDoc::CpakdruDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CpakdruDoc::~CpakdruDoc()
{
}

BOOL CpakdruDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CpakdruDoc-Serialisierung

void CpakdruDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CpakdruDoc-Diagnose

#ifdef _DEBUG
void CpakdruDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CpakdruDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CpakdruDoc-Befehle
