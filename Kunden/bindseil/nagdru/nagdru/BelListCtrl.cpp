#include "StdAfx.h"
#include "BelListCtrl.h"
#include "StrFuncs.h"
#include "resource.h"
#include "nagdruDlg.h"

CBelListCtrl::CBelListCtrl(void)
{
	m_mdn = 1;
	PosLs		= POSLS;
	PosBez1		= POSBEZ1;
	PosStat		= POSSTAT;
//	PosAnz		= POSANZ;
	PosAktiv 	= POSAKTIV;
	

	Position[0] = &PosLs;
	Position[1] = &PosBez1;
	Position[2] = &PosStat;
//	Position[3] = &PosAnz;

	Position[3] = &PosAktiv;
	Position[4] = NULL;

	MaxComboEntries = 20;

	ListRows.Init ();
}

CBelListCtrl::~CBelListCtrl(void)
{
//	CString *c;
/* --->
    ZuBasisCombo.FirstPosition ();
	while ((c = (CString *) ZuBasisCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	ZuBasisCombo.Init ();
< --- */

}

BEGIN_MESSAGE_MAP(CBelListCtrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


/* ---->
void CBelListCtrl::SetWerte ( long anzahl, double kaltgew, long schlklknr )
{
	if (( lianzahl != anzahl ) || ( dikaltgew != kaltgew )|| ( lischlklknr != schlklknr ))
	{
		CString wERTKG ;
		CString wERT;
		CString zUBASIS ;
		CString zUSCHLAG ;

		double dizuschlag ;
		int dibasis ;
		double diwert ;
		double diwertkg ;

		dizuabkg = 0.0 ; 

		int i = GetItemCount () ;
		if ( i > 0 )
		{
			lischlklknr = schlklknr ;
		};
		lianzahl = anzahl ;	
		dikaltgew = kaltgew ;
		
		while  ( i > 0 )
		{
			i -- ;

 			zUSCHLAG =  FillList.GetItemText (i, PosZuschlag);
			dizuschlag = CStrFuncs::StrToDouble (zUSCHLAG);
			CString zUBASIS ;
			zUBASIS =  FillList.GetItemText (i, PosZubasis);
			dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!
	 		switch ( dibasis )
			{
//			case 0 :	// je kg
			case 1 :	// je Stck.
				diwert = dizuschlag * lianzahl ;
				if ( dikaltgew != 0.0 )
				{
					diwertkg = (dizuschlag *	lianzahl ) / dikaltgew ;
				}
				else
				{
					diwertkg = 0.0 ;
				}
			break ;
			case 2 :	// pauschal
				diwert = dizuschlag ;
				if ( dikaltgew != 0.0 )
				{
					diwertkg = dizuschlag / dikaltgew ;
				}
				else
				{	
					diwertkg = 0.0 ;
				}
			break ;

			default :	// auffang-Linie : je kg
				diwert = dizuschlag * dikaltgew ;
				diwertkg = dizuschlag ;
			break ;
			}

			DoubleToString (diwert, wERT, 2);
			FillList.SetItemText (wERT.GetBuffer (), i, PosWert);

			DoubleToString (diwertkg, wERTKG, 4);
			FillList.SetItemText (wERTKG.GetBuffer (), i, PosWertkg);
			dizuabkg += diwertkg ;

		}
		Zuschlagsetzen  ( ) ;
	}
	else
	{
		lianzahl = anzahl ;
		dikaltgew = kaltgew ;
		lischlklknr = schlklknr ;
	}
}
< ---- */
/* ----->
void CKstArtListCtrl::Zuschlagsetzen (void) 
{
 CSchlakaDlg * CElFenst ;
CElFenst = ( CSchlakaDlg *) GetParent () ;
CElFenst->SetzeEkzerleg (dizuabkg ) ;
}
< ---- */

void CBelListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
//		StartEnter (PosAnz, 0);
		StartEnter (PosAktiv, 0);
	}
	else
	{
		return ;	// immer return, weil kein Appenden erlaubt ist
//		if (!AppendEmpty ()) return; 
//		StartEnter (PosBez1, 0);
	}

}

void CBelListCtrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
	if (col == PosBez1) return;
	if (col == PosLs) return;
	if (col == PosStat) return;

	if (col == PosAktiv)
	{
		CEditListCtrl::StartEnterCheckBox (col, row);
		return ;
	}

	CEditListCtrl::StartEnter (col, row);
/* --->
	if (col == PosZubasis)
	{
		if (IsWindow (ListComboBox.m_hWnd))
		{
			StopEnter ();
		}

//		CString zUBASIS ;
//		zUBASIS =  FillList.GetItemText (row, PosZubasis);	// == col //
//		int dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!

	 //		CEditListCtrl::StartEnterCombo (col, row, &ZuBasisCombo);
//		ListComboBox.SetCurSel( dibasis ) ;
//		oldsel = ListComboBox.GetCurSel ();
		return ;
	}
< --- */
/* ---->
	if (col == PosZuschlag || col == PosKost_bez)
	{
		CEditListCtrl::StartEnter (col, row);
	}
< ----- */
}

void CBelListCtrl::StopEnter ()
{

	CEditListCtrl::StopEnter ();

	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	/* ---->
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = _T("");
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
	}
	< ---- */
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		int check = ListCheckBox.GetCheck ();
        if (check == BST_CHECKED)
		{
			FillList.SetItemText (_T("X"), EditRow, EditCol);
		}
		else
		{
			FillList.SetItemText (_T(" "), EditRow, EditCol);
		}
		ListCheckBox.DestroyWindow ();
	}

	/* ---->
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		int count = GetItemCount ();
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	< ----- */
}

void CBelListCtrl::SetSel (CString& Text)
{

/* ---->
   if (EditCol == PosAnz )   // ||  EditCol == PosLdPr ||  EditCol == PosEkProz)
   {
		ListEdit.SetSel (0, -1);
   }
< ----- */
/* ----->
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
//		ListEdit.SetSel (cpos, cpos);
   }
< ------ */
   }

void CBelListCtrl::FormatText (CString& Text)
{
/* ------>

  if (EditCol == PosAnz)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
< ------ */

/* ---
    if (EditCol == PosZuschlag)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
< ---- */
/* --->
   if (EditCol == PosZuschlag || EditCol == PosZubasis )
   {
	   double dizuschlag ;
	   short dibasis ;
	   if ( EditCol == PosZuschlag )
	   {
			dizuschlag = StrToDouble (Text) ;
			CString zUBASIS ;
			zUBASIS =  FillList.GetItemText (EditRow, PosZubasis);
			dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!
	   }
	   else
	   {

 			CString zUSCHLAG =  FillList.GetItemText (EditRow, PosZuschlag);
			dizuschlag = CStrFuncs::StrToDouble (zUSCHLAG);
			CString zUBASIS ;
			zUBASIS =  FillList.GetItemText (EditRow, PosZubasis);
			dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!
	   }
		double diposwert, diposwertkg ;
		switch ( dibasis )
		{
//		case 0 :	// je kg
		case 1 :	// je Stck.
			diposwert = dizuschlag * lianzahl ;
			if ( dikaltgew != 0.0 )
			{
			    diposwertkg = (dizuschlag *	lianzahl ) / dikaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;
		case 2 :	// pauschal
			diposwert = dizuschlag ;
			if ( dikaltgew != 0.0 )
			{
			    diposwertkg = dizuschlag / dikaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;

		default :	// auffang-Linie : je kg
		    diposwert = dizuschlag * dikaltgew ;
			diposwertkg = dizuschlag ;
			break ;
		}

		CString wERT;
		DoubleToString (diposwert, wERT, 2);
//		Schlaklkp.schlaklkp.wert = CStrFuncs::StrToDouble (wERT);
// 		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosWert);
		FillList.SetItemText (wERT.GetBuffer (), EditRow, PosWert);

		CString wERTKG;
		DoubleToString (diposwertkg, wERTKG, 4);
//		Schlaklkp.schlaklkp.wertkg = CStrFuncs::StrToDouble (wERTKG);
//		FillList.SetItemText (wERTKG.GetBuffer (), i, m_list1.PosWertkg);
		FillList.SetItemText (wERTKG.GetBuffer (), EditRow, PosWertkg);
	}

< ---- */

	/* --->
   CString weRTKG ;
   dizuabkg = 0.0 ; 
 	for (int i = GetItemCount()  ; i > 0 ; )
	{
		i -- ;
		weRTKG =  FillList.GetItemText (i, PosWertkg);
		dizuabkg += CStrFuncs::StrToDouble (weRTKG);
	}

	Zuschlagsetzen  ( ) ;

< ----- */

/*
    else if (EditCol == 4)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
*/

}

void CBelListCtrl::NextRow ()
{
//	CString Zuschlag = GetItemText (EditRow, PosZuschlag);
//	double 	dZuschlag = StrToDouble (Zuschlag) ;

	int count = GetItemCount ();

/* ->
	if (EditCol == PosKun)
	{
		ReadKunName ();
	}
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< --- */
/* ---->
	if (EditCol == PosZubasis)
	{
//		ReadZuBasis ();	// ReadPrGrStuf
	}
	SetEditText ();
< ----- */

//	TestIprIndex ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;

	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);

	if (GetKeyState (VK_SHIFT) < 0)
	{
		BOOL &b = vSelect[EditRow - 1];
		b = TRUE;
		FillList.SetItemImage (EditRow - 1, 1);
	}
	else if (GetKeyState (VK_CONTROL) == 0)
	{
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
	}
}

void CBelListCtrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();


	if (EditRow == count - 1)
	{
/* --->
		CString Zuschlag = GetItemText (EditRow, PosZuschlag);
		CString Kost_bez = GetItemText (EditRow, PosKost_bez);
		if ((StrToDouble (Zuschlag) == 0.0)
			&& (_tcslen (Kost_bez.Trim()) == 0 ))
		{
	        DeleteItem (EditRow);
		}
< ---- */
	}
	else
	{
		if (EditRow <= 0)
		{
			return;
		}
/* ---->
		if (EditCol == PosKun)
		{
			ReadKunName ();
		}
		else if (EditCol == PosKunPr)
		{
			ReadKunPr ();
		}
		else if (EditCol == PosPrGrStuf)
		{
			ReadPrGrStuf ();
		}
		TestIprIndex ();
< ---- */
	}
	StopEnter ();
 
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CBelListCtrl::LastCol ()
{
//	if (EditCol < PosAnz) return FALSE;
	if (EditCol < PosAktiv) return FALSE;
	/* --->
	if (Mode == TERMIN && EditCol >= PosLdPr)
	{
		return TRUE;
	}
	if (Aufschlag == LIST || Aufschlag == ALL)
	{
		if (EditCol < PosEkProz) return FALSE;
	}
< ---- */

	return TRUE;
}

void CBelListCtrl::OnReturn ()
{
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{
/* --->
		CString Zuschlag = GetItemText (EditRow, PosZuschlag);
		CString Kost_bez = GetItemText (EditRow, PosKost_bez);
		if ((StrToDouble (Zuschlag) == 0.0 ) 
			&&	(_tcslen(Kost_bez.Trim())== 0 ))
		{
			EditCol --;
			return;
		}
< ----- */

	}
/* --->
	if (EditCol == PosKun)
	{
		ReadKunName ();
	}
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< ---- */
	if (LastCol ())
	{
//		TestIprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
//		EditCol = 0;
		if (EditRow == rowCount)
		{
// //			EditCol = PosKost_bez;
		}
		else
		{
// //			EditCol = PosKost_bez;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
		if (EditCol == PosBez1)
		{
			EditCol ++;
		}
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CBelListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
/* ---->
	if (EditCol == PosKun)
	{
		ReadKunName ();
	}
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< ---- */

	StopEnter ();
	EditCol ++;
/* --->
	if (EditCol == PosKunName)
	{
		EditCol ++;
	}
< --- */
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CBelListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
/* ---->
	if (EditCol == PosKun)
	{
		ReadKunName ();
	}
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< -- */

	StopEnter ();
	EditCol --;
/* ---->
	if (EditCol == PosKunName)
	{
		EditCol --;
	}
< ---- */
    StartEnter (EditCol, EditRow);
}

BOOL CBelListCtrl::InsertRow ()
{
	return FALSE ;
/* ---->
	CString Zuschlag = GetItemText (EditRow, PosZuschlag);
	CString Kost_bez = GetItemText (EditRow, PosKost_bez);
	if ((GetItemCount () > 0) && 
		(StrToDouble (Zuschlag) == 0.0) &&
		 (_tcslen(Kost_bez) == 0))
	{
		return FALSE;
	}
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (_T(" 0"), EditRow, PosPosi);
	FillList.SetItemText (_T(" "), EditRow, PosKost_bez);
	FillList.SetItemText (_T("0,00"), EditRow, PosZuschlag);
	FillList.SetItemText (_T(""), EditRow, PosZubasis);
	FillList.SetItemText (_T("0,00 "), EditRow, PosWert);
	FillList.SetItemText (_T("0,00 "), EditRow, PosWertkg);

	SCHLAKLKP schlaklkp;
	memcpy (&schlaklkp, &schlaklkp_null, sizeof (SCHLAKLKP));
	schlaklkp.mdn = mdn;
	schlaklkp.schlklknr = schlklknr;

	** --->
	CPreise *p = new CPreise (CString ("0,00"), CString ("0,00"), ipr);	
	if (ActiveListRow)
	{
		ListRows.Insert (EditRow, p);
	}
	< ---- **
	StartEnter (0, EditRow);
	return TRUE;

	StartEnter (0, EditRow);
< ---- */
	return TRUE;
}

BOOL CBelListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	return FALSE ;	// es wird nie was geloescht
	return CEditListCtrl::DeleteRow ();
}

BOOL CBelListCtrl::AppendEmpty ()
{

	int rowCount = GetItemCount ();
	return FALSE ;
/* ---->
	if (rowCount > 0)
	{
		CString Zuschlag = GetItemText (EditRow, PosZuschlag);
		CString Kost_bez = GetItemText (EditRow, PosKost_bez);
		if ((StrToDouble (Zuschlag) == 0.0)
			&& (_tcslen(Kost_bez.Trim()) == 0))
		{
			return FALSE;
		}
	}
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);

	CString pOSI ;
	pOSI.Format (_T("%d"), rowCount + 1 );
	FillList.SetItemText (_T(pOSI.GetBuffer()), rowCount, PosPosi);

	FillList.SetItemText (_T(""), rowCount, PosKost_bez);
	FillList.SetItemText (_T("0,00"), rowCount, PosZuschlag);
	FillList.SetItemText (_T(""), rowCount, PosZubasis);
	FillList.SetItemText (_T("0,00"), rowCount, PosWert);
	FillList.SetItemText (_T("0,00"), rowCount, PosWertkg);
	rowCount = GetItemCount ();
	return TRUE;
< ----- */
}

void CBelListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/

}

/* --->
void CKstArtListCtrl::FillPrGrStufCombo (CVector& Values)
{
	CString *c;
    PrGrStufCombo.FirstPosition ();
	while ((c = (CString *) PrGrStufCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	PrGrStufCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		PrGrStufCombo.Add (c);
	}
}
< ---- */

/* ---->  brauchen wir gerade nicht
void CKstArtListCtrl::FillZuBasisCombo (CVector& Values)
{
	CString *c;
    ZuBasisCombo.FirstPosition ();
	while ((c = (CString *) ZuBasisCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	ZuBasisCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		ZuBasisCombo.Add (c);
	}
}

< ----- */


/* ----->
void CKstArtListCtrl::RunItemClicked (int Item)
{
**
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
**
}
< ----- */


void CBelListCtrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CBelListCtrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CBelListCtrl::OnChoice ()
{
	/* -> 
	if (EditCol == PosKun)
	{
		OnKunChoice (CString (_T("")));
	}
	else if (EditCol == PosKunPr)
	{
		OnIKunPrChoice (CString (_T("")));
	}
	else if (EditCol == PosPrGrStuf)
	{
		OnPrGrStufChoice (CString (_T("")));
	}
	< ----- */
}


/* --->
void CBelListCtrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}
< ---- */

/* --->
void CKstArtListCtrl::ReadKunName ()
{

**
	if (EditCol != PosKun) return;
    memcpy (&Kun.kun, &kun_null, sizeof (KUN));
	memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		return;
	}
    CString Text;
	SearchListCtrl.Edit.GetWindowText (Text);
	if (!CStrFuncs::IsDecimal (Text))
	{
		OnKunChoice (Text);
	    SearchListCtrl.Edit.GetWindowText (Text);
		Text.Format (_T("%ld"), atol (Text.GetBuffer ()));
	    SearchListCtrl.Edit.SetWindowText (Text);
		if (!KunChoiceStat)
		{
			EditCol --;
			return;
		}
	}
	if (atol (Text) == 0l) return;
    Kun.kun.mdn = m_mdn;
	Kun.kun.kun = atol (Text);
	if (Kun.dbreadfirst () == 0)
    {
		  KunAdr.adr.adr = Kun.kun.adr1;
		  KunAdr.dbreadfirst ();
	}
	else if (Kun.kun.kun != 0l)
	{
		MessageBox (_T("Kunde nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return;
	}
    CString KunName;
	KunName.Format (_T("%s"), KunAdr.adr.adr_krz);
	FillList.SetItemText (KunName.GetBuffer (), EditRow, PosKunName);

    memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
    Iprgrstufk.iprgrstufk.mdn = m_Mdn;
	Iprgrstufk.iprgrstufk.pr_gr_stuf = Kun.kun.pr_stu;
	if (Iprgrstufk.dbreadfirst () != 0 &&
        Iprgrstufk.iprgrstufk.pr_gr_stuf != 0)
	{
		return;
	}
	Text.Format (_T("%ld %s"), Iprgrstufk.iprgrstufk.pr_gr_stuf, Iprgrstufk.iprgrstufk.zus_bz);
	SetItemText (EditRow, PosPrGrStuf, Text);

    I_kun_prk.i_kun_prk.mdn = m_Mdn;
	I_kun_prk.i_kun_prk.kun_pr = Kun.kun.pr_lst;
	if (I_kun_prk.dbreadfirst () != 0 &&
        I_kun_prk.i_kun_prk.kun_pr != 0)
	{
		return;
	}
	Text.Format (_T("%ld %s"), I_kun_prk.i_kun_prk.kun_pr, I_kun_prk.i_kun_prk.zus_bz);
	SetItemText (EditRow, PosKunPr, Text);
< ----- **

}
 ---- */


void CBelListCtrl::GetColValue (int row, int col, CString& Text)
{
	/* ---->
    CString cText = GetItemText (row, col);
	int pos = 0;
	if (col == PosPrGrStuf)
	{
		Text = cText.Tokenize (_T(" "), pos);
	}
	else if (col == PosKunPr)
	{
		Text = cText.Tokenize (_T(" "), pos);
	}
	else
	{
		Text = cText.Trim ();
	}
	< ---- */
}

/* ---->
void CBelListCtrl::TestIprIndex ()
{
** ---->
	int Items = GetItemCount ();
	if (Items <= 1) return;
	CString Value;
	GetColValue (EditRow, PosPrGrStuf, Value);
	long rPrGrStuf = atol (_T(Value));
	GetColValue (EditRow, PosKunPr, _T(Value));
	long rKunPr = atol (_T(Value));
	GetColValue (EditRow, PosKun, _T(Value));
	long rKun = atol (_T(Value));
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosPrGrStuf, Value);
		long lPrGrStuf = atol (_T(Value));
		GetColValue (i, PosKunPr, Value);
		long lKunPr = atol (_T(Value));
		GetColValue (i, PosKun, Value);
		long lKun = atol (Value);
		if (lKun == rKun && rKun != 0)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
		else if (lKunPr == rKunPr && rKunPr != 0)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
		else if (lPrGrStuf == rPrGrStuf && lKunPr == rKunPr
				&& lKun == rKun)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
	}
	< --- **
}
< ---- */

void CBelListCtrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}


