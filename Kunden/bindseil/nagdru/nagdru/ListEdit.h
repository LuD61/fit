#pragma once
#ifndef _LIST_EDIT_DEF
#define _LIST_EDIT_DEF
#include "afxwin.h"

class CListEdit :
	public CEdit
{
public:
	CListEdit(void);
	~CListEdit(void);
protected :
	DECLARE_MESSAGE_MAP()
    afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};
#endif