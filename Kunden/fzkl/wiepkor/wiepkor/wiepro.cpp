#include "stdafx.h"
#include "DbClass.h"
#include "wiepro.h"

struct WIEPRO wiepro,  wiepro_null;

extern DB_CLASS dbClass;

WIEPRO_CLASS wiepro_class ;


int WIEPRO_CLASS::updawiepro (void)
{
	if (test_upd_cursor == -1)
    {
		prepare ();
    }
	int retcode = 0 ;
	if ( wiepro.hbk.month == 0 && wiepro.hbk.year == 0 && wiepro.hbk.day == 0  )
		retcode = dbClass.sqlexecute(upd_cursor ) ;
	else
		retcode = dbClass.sqlexecute(test_upd_cursor ) ;

	return retcode ;
}

int WIEPRO_CLASS::lesewiepro (void)
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int WIEPRO_CLASS::openwiepro (void)
{
		if ( readcursor < 0 ) prepare ();
         return dbClass.sqlopen (readcursor);
}

void WIEPRO_CLASS::prepare (void)
{
							
// modifizierbare Felder
	dbClass.sqlin ((long *) &wiepro.anz , SQLLONG ,0 ) ;
	dbClass.sqlin ((char *) wiepro.ls_ident, SQLCHAR, 21 ) ;
	dbClass.sqlin ((char *) wiepro.ls_char30, SQLCHAR, 31 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &wiepro.hbk,SQLTIMESTAMP,26 )  ;

	// Identifikation 
	dbClass.sqlin ((short *) &wiepro.mdn , SQLSHORT, 0);
	dbClass.sqlin ((long *) &wiepro.nr , SQLLONG, 0);
	dbClass.sqlin ((char *) wiepro.zeit , SQLCHAR, 7 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &wiepro.dat,SQLTIMESTAMP, 26 ) ; 


	test_upd_cursor = (short) dbClass.sqlcursor (
	"update wiepro "
    "  set anz = ? , ls_ident = ? , ls_char30 = ? , hbk = ? " 
	" where mdn = ?  and nr = ? and zeit = ? and dat = ? ") ;

// modifizierbare Felder fuer null-update 
	dbClass.sqlin ((long *) &wiepro.anz , SQLLONG ,0 ) ;
	dbClass.sqlin ((char *) wiepro.ls_ident, SQLCHAR, 21 ) ;
	dbClass.sqlin ((char *) wiepro.ls_char30, SQLCHAR, 31 ) ;
//	dbClass.sqlin ((TIMESTAMP_STRUCT *) &wiepro.hbk,SQLTIMESTAMP,26 )  ;

	// Identifikation 
	dbClass.sqlin ((short *) &wiepro.mdn , SQLSHORT, 0);
	dbClass.sqlin ((long *) &wiepro.nr , SQLLONG, 0);
	dbClass.sqlin ((char *) wiepro.zeit , SQLCHAR, 7 ) ;
	dbClass.sqlin ((TIMESTAMP_STRUCT *) &wiepro.dat,SQLTIMESTAMP, 26 ) ; 


	upd_cursor = (short) dbClass.sqlcursor (
	"update wiepro "
//    "  set anz = ? , ls_ident = ? , ls_char30 = ? , hbk = -693594 " 
    "  set anz = ? , ls_ident = ? , ls_char30 = ? , hbk = ' ' " 
	" where mdn = ?  and nr = ? and zeit = ? and dat = ? ") ;



// ################################################################

	dbClass.sqlin ((short *)&wiepro.mdn , SQLSHORT, 0);
	dbClass.sqlin ((long *) &wiepro.nr , SQLLONG, 0);
	dbClass.sqlin ((char *)  wiepro.blg_typ , SQLCHAR, 2);
	 
	dbClass.sqlout(( char * )             wiepro.zeit,SQLCHAR,7 ) ;
	dbClass.sqlout((TIMESTAMP_STRUCT * ) &wiepro.dat, SQLTIMESTAMP , 26 ) ;
	dbClass.sqlout((long * )             &wiepro.sys, SQLLONG , 0 ) ;
	dbClass.sqlout((char * )              wiepro.peri_nam,SQLCHAR, 21 ) ;
//	dbClass.sqlout((char * )			  wiepro.blg_typ,SQLCHAR, 2 ) ; 
//	dbClass.sqlout((long * )			 &wiepro.nr, SQLLONG , 0 ) ;
	dbClass.sqlout((short * )			 &wiepro.kun_fil , SQLSHORT , 0 ) ;

	dbClass.sqlout((short * )			 &wiepro.fil , SQLSHORT , 0 ) ;
//	dbClass.sqlout((short * )			 &wiepro.mdn , SQLSHORT , 0 ) ;
	dbClass.sqlout((long * )			 &wiepro.kun , SQLLONG , 0 ) ;
	dbClass.sqlout((double * )			 &wiepro.a , SQLDOUBLE , 0 );
	dbClass.sqlout((double * )			 &wiepro.gew_bto, SQLDOUBLE , 0 ) ;
	dbClass.sqlout((double * )		     &wiepro.gew_nto, SQLDOUBLE , 0 ) ;

	dbClass.sqlout((double * )			 &wiepro.tara , SQLDOUBLE , 0 );
	dbClass.sqlout((char * )			  wiepro.erf_kz,SQLCHAR, 2 ) ;
	dbClass.sqlout((char * )		      wiepro.pers,SQLCHAR, 13 ) ;
	dbClass.sqlout((double * )           &wiepro.pr, SQLDOUBLE , 0 ) ;
	dbClass.sqlout((short * )            &wiepro.delstatus , SQLSHORT , 0 ) ;

	dbClass.sqlout((char * )			  wiepro.ls_charge,SQLCHAR, 21 ) ;
	dbClass.sqlout((long * )		     &wiepro.anz, SQLLONG , 0 ) ;
	dbClass.sqlout((char * )			  wiepro.ls_ident,SQLCHAR, 21 ) ;
	dbClass.sqlout((char * )			  wiepro.ls_char30,SQLCHAR, 31 ) ;
	dbClass.sqlout((double * )			 &wiepro.nett_alt, SQLDOUBLE , 0 ) ;

	dbClass.sqlout((TIMESTAMP_STRUCT * ) &wiepro.hbk,SQLTIMESTAMP, 26 )  ;



	readcursor = (short) dbClass.sqlcursor ("select "
		"  zeit ,dat ,sys ,peri_nam ,kun_fil "
		" ,fil ,kun ,a ,gew_bto ,gew_nto "
		" ,tara ,erf_kz ,pers ,pr ,delstatus "
		" ,ls_charge ,anz ,ls_ident ,ls_char30 ,nett_alt "
		" ,hbk "
		" from wiepro where "
		" mdn = ? and nr = ? and blg_typ = ? order by dat , zeit "
		) ;
}

