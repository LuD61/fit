#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "BelListCtrl.h"



// CWiepKorDlg-Formularansicht

class CWiepKorDlg : public CFormView
{
	DECLARE_DYNCREATE(CWiepKorDlg)

protected:
	CWiepKorDlg();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CWiepKorDlg();

public:
	enum { IDD = IDD_DIALOG1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_lsnr;
public:
	long v_lsnr;
public:
	afx_msg void OnBnClickedCancel();
public:
	CBelListCtrl m_list1;
public:
	CEdit m_mdnnr;
public:
	CString v_mdnnr;
public:
	CEdit m_mdnname;
public:
	CString v_mdnname;

public:
	void ReadMdn(void) ;
	virtual BOOL Read(void);
	virtual BOOL Write(void);
	virtual BOOL ReadList(void) ;

	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);
	virtual void DestroyRows(CVector &Rows);
//	virtual BOOL InList(WIEPRO_CLASS & wiepro_class) ;	// Input nur dummy ?!
	virtual BOOL InList(void) ;	
//	virtual void StapelDruck (void) ;

	CVector DbRows;
	CVector ListRows;

	CFillList FillList ;

public:
	afx_msg void OnListBegintrack(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnListEnddrag(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnEnKillfocusLsnr();

	afx_msg BOOL PreTranslateMessage(LPMSG) ;
//	afx_msg BOOL NoListCtrl (CWnd *) ;
public:
	afx_msg void OnBnClickedOk();
};

