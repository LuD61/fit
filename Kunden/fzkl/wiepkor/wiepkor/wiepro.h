#ifndef _WIEPRO_DEF
#define _WIEPRO_DEF

struct WIEPRO {
char zeit[7] ;
TIMESTAMP_STRUCT dat ;
long sys ;
char peri_nam[21] ;
char blg_typ[2] ; 
long nr ;
short kun_fil ;
short fil ;
short mdn ;
long kun ;
double a ;
double gew_bto ;
double gew_nto ;
double tara ;
char erf_kz[2] ;
char pers[13] ;
double pr ;
short delstatus ;
char ls_charge[21] ;
long anz ;
char ls_ident[21] ;
char ls_char30[31] ;
double nett_alt ;
TIMESTAMP_STRUCT hbk  ;
};

extern struct WIEPRO wiepro, wiepro_null;

class WIEPRO_CLASS : public DB_CLASS
{
	private :
		void prepare (void);
	public :
       int lesewiepro (void);
       int openwiepro (void);
       int updawiepro (void);
       WIEPRO_CLASS () : DB_CLASS ()
       {
       }
};

#endif

