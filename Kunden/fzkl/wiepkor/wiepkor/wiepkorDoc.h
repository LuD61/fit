// wiepkorDoc.h : Schnittstelle der Klasse CwiepkorDoc
//


#pragma once


class CwiepkorDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CwiepkorDoc();
	DECLARE_DYNCREATE(CwiepkorDoc)

// Attribute
public:

// Vorg�nge
public:

// �berschreibungen
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CwiepkorDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


