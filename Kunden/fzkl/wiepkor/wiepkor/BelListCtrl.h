#pragma once
#include <vector>
#include "editlistctrl.h"
#include "dbClass.h"
#include "wiepro.h"
// #include "ChoiceZuBasis.h"

#define MAXLISTROWS 30

class CBelListCtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

/* --->
	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
	};
< ---- */
	enum LISTPOS
	{
		POSART		= 1,
		POSDAT		= 2,
		POSZEIT		= 3,
		POSES       = 4,
		POSGEW   	= 5,
		POSANZ   	= 6,
		POSHBK   	= 7,
		POSIDT   	= 8,
		POSCHR   	= 9,
		POSMOD   	= 10,
	};

	int PosArt;
    int PosDat;
	int PosZeit;
    int PosES;
	int PosGew;
	int PosAnz;
	int PosHBK;
	int PosIdt;
	int PosChr;
	int PosMod;

    int *Position[11];


//	double dikaltgew ;
//	double dizuabkg ;
//	long  lianzahl ;
//	long lischlklknr ;

// void SetWerte ( long , double, long ) ;

// void Zuschlagsetzen ( void ) ;

//	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	BOOL ActiveListRow;
	short m_mdn;
	short mdn;
//    long schlklknr;

//	int oldsel;
	std::vector<BOOL> vSelect;
//	CVector ZuBasisCombo ;
//	CVector PrGrStufCombo;
//	CVector KunPrCombo;
//	CChoiceKun *ChoiceKun;
//	BOOL ModalChoiceKun;
//	BOOL KunChoiceStat;
//	CChoiceIKunPr *ChoiceIKunPr;
//	BOOL ModalChoiceIKunPr;
//	BOOL IKunPrChoiceStat;
//	CChoiceZuBasis *ChoiceZuBasis;
//	BOOL ModalChoiceZuBasis;
//	BOOL ZuBasisChoiceStat;
	CVector ListRows;

//	KUN_CLASS Kun;
//	ADR_CLASS KunAdr;
//	I_KUN_PRK_CLASS I_kun_prk;
//	IPRGRSTUFK_CLASS Iprgrstufk;

	CBelListCtrl(void);
	~CBelListCtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
//	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
//	void FillZuBasisCombo( CVector&);
//	void FillPrGrStufCombo (CVector&);
//	void FillKunPrCombo (CVector&);
	void OnChoice ();
//	void OnKunChoice (CString &);
//    void OnIKunPrChoice (CString& Search);
//    void OnPrGrStufChoice (CString& Search);
//	void OnKey9 ();
//    void ReadKunName ();
//    void ReadKunPr ();
//    void ReadPrGrStuf ();
    void GetColValue (int row, int col, CString& Text);
//    void TestIprIndex ();
	void ScrollPositions (int pos);
	BOOL LastCol ();
};
