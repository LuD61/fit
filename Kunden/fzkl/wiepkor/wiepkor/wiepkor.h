// wiepkor.h : Hauptheaderdatei f�r die wiepkor-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error "\"stdafx.h\" vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"       // Hauptsymbole


// CwiepkorApp:
// Siehe wiepkor.cpp f�r die Implementierung dieser Klasse
//

class CwiepkorApp : public CWinApp
{
public:
	CwiepkorApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CwiepkorApp theApp;