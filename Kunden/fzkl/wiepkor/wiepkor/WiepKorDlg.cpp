// WiepKorDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "wiepkor.h"

#include "WiepKorDlg.h"

#include "strfuncs.h"
#include "dbClass.h"
#include "mdn.h"
#include "adr.h"
#include "ls.h"
#include "wiepro.h"

#include "FillList.h"

extern DB_CLASS dbClass ;
extern MDN_CLASS mdn_class ;
extern ADR_CLASS adr_class ;
extern LSK_CLASS lsk_class ;
extern WIEPRO_CLASS wiepro_class ;

char bufh [256] ;

// CWiepKorDlg

IMPLEMENT_DYNCREATE(CWiepKorDlg, CFormView)

CWiepKorDlg::CWiepKorDlg()
	: CFormView(CWiepKorDlg::IDD)
	, v_lsnr(0)
	, v_mdnnr(_T(""))
	, v_mdnname(_T(""))
{

}

CWiepKorDlg::~CWiepKorDlg()
{
}

void CWiepKorDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LSNR, m_lsnr);
	DDX_Text(pDX, IDC_LSNR, v_lsnr);
	DDV_MinMaxLong(pDX, v_lsnr, 1, 99999999);
	DDX_Control(pDX, IDC_LIST1, m_list1);
	DDX_Control(pDX, IDC_MDNNR, m_mdnnr);
	DDX_Text(pDX, IDC_MDNNR, v_mdnnr);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
}

BEGIN_MESSAGE_MAP(CWiepKorDlg, CFormView)
	ON_BN_CLICKED(IDCANCEL, &CWiepKorDlg::OnBnClickedCancel)
	ON_EN_KILLFOCUS(IDC_LSNR, &CWiepKorDlg::OnEnKillfocusLsnr)
	ON_BN_CLICKED(IDOK, &CWiepKorDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CWiepKorDlg-Diagnose

#ifdef _DEBUG
void CWiepKorDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CWiepKorDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CWiepKorDlg-Meldungshandler

void CWiepKorDlg::ReadMdn (void) 
{
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//	gibbet nich		PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich 		PrevDlgCtrl();
	}
		UpdateData (FALSE) ;
}


void CWiepKorDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	dbClass.opendbase (_T("bws"));
	// Cursoren createn ....

	/* --->
	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_list1.SetImageList (&image, LVSIL_SMALL);   
	}
< ---- */
// 	m_list1.SetImageList (&image, LVSIL_SMALL);   
	FillList = m_list1;
	FillList.SetStyle (LVS_REPORT);
	if (m_list1.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);

// Artikel, Datum,Zeit,es_nr , gewicht, Anzahl, HBK, ls_ident, ls_charge30 , modif
//	ro    ,   ro  ,  ro ,    ro , ro  , rw      rw  ,  rw   ,     rw      , ro
	FillList.SetCol (_T("Art.")  ,  1, 60, LVCFMT_RIGHT);
	FillList.SetCol (_T("Datum") ,  2, 80, LVCFMT_LEFT);
	FillList.SetCol (_T("Zeit")  ,  3, 50, LVCFMT_LEFT);
	FillList.SetCol (_T("ES-Nr") ,  4, 80, LVCFMT_LEFT);
	FillList.SetCol (_T("Gew.")  ,  5, 60, LVCFMT_RIGHT);
	FillList.SetCol (_T("Anzahl"),  6, 50, LVCFMT_RIGHT);
	FillList.SetCol (_T("HBK")   ,  7, 80, LVCFMT_LEFT);
	FillList.SetCol (_T("IDENT") ,  8, 100, LVCFMT_LEFT);
	FillList.SetCol (_T("CHARGE"),  9, 100, LVCFMT_LEFT);
	FillList.SetCol (_T("M")     , 10, 10, LVCFMT_LEFT);

	//	m_list1.ColType.Add (new CColType (5, m_list1.CheckBox)) ;


	/* --->
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);
	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
//	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
//	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
//	MdnGrid.Add (c_MdnChoice);

	SchlklknrGrid.Create (this, 1, 2);
    SchlklknrGrid.SetBorder (0, 0);
    SchlklknrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Schlklknr = new CCtrlInfo (&m_schlklknr, 0, 0, 1, 1);
	SchlklknrGrid.Add (c_Schlklknr);
< ----- */
/* --->
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);
< ----- */

	/* --->
	ButtonGrid.Create (this, 5, 5);
    ButtonGrid.SetBorder (0, 0);
    ButtonGrid.SetCellHeight (20);
    ButtonGrid.SetGridSpace (0, 10);
< -- */

/* --->
	CCtrlInfo *c_Cancel = new CCtrlInfo (&m_Cancel, 0, 0, 1, 1);
	ButtonGrid.Add (c_Cancel);
	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 1, 1, 1);
	ButtonGrid.Add (c_Save);
	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 0, 2, 1, 1);
	ButtonGrid.Add (c_Delete);
	CCtrlInfo *c_Insert = new CCtrlInfo (&m_Insert, 0, 3, 1, 1);
	ButtonGrid.Add (c_Insert);
< --- */

/* --->
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_GrundPreis = new CCtrlInfo (&GrundPreis, DOCKRIGHT, 0, 5, 1); 
	CtrlGrid.Add (c_GrundPreis);

	CCtrlInfo *c_LA     = new CCtrlInfo (&m_LA, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid   = new CCtrlInfo (&AGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_LA_bz1  = new CCtrlInfo (&m_LA_bz1, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LA_bz1);
	CCtrlInfo *c_A_bz1  = new CCtrlInfo (&m_A_bz1, 1, 2, 3, 1); 
	CtrlGrid.Add (c_A_bz1);
	CCtrlInfo *c_A_bz2  = new CCtrlInfo (&m_A_bz2, 1, 3, 3, 1); 
	CtrlGrid.Add (c_A_bz2);
	CCtrlInfo *c_ButtonGrid = new CCtrlInfo (&ButtonGrid, DOCKRIGHT, 4, 1, 3); 
	CtrlGrid.Add (c_ButtonGrid);
	CCtrlInfo *c_List  = new CCtrlInfo (&m_List, 0, 4, DOCKRIGHT, DOCKBOTTOM); 
//	c_List->rightspace = 125;
	c_List->rightspace = RightListSpace;
	CtrlGrid.Add (c_List);
< ----- */

/* --->
	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();
< ---- */
/* --->
	memcpy (&Schlaklkk.schlaklkk, &schlaklkk_null, sizeof (schlaklkk));

	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = 1;
< --- */
	mdn.mdn = 1 ;
	v_mdnnr = "1" ;
/* ---->
	if (PersName.Trim () != "")
	{
		_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
		if (Sys_ben.dbreadfirstpers_nam () == 0)
		{
			if (Sys_ben.sys_ben.mdn != 0)
			{
				Mdn.mdn.mdn = Sys_ben.sys_ben.mdn;
				m_Mdn.SetReadOnly ();
				m_Mdn.ModifyStyle (WS_TABSTOP, 0);
			}
		}
	}
< ---- */

// 	kalkartfeldersetzen () ;
// 	Form.Show ();
 	ReadMdn ();


/* --->
	if (RemoveKun)
	{
		m_list1.DeleteColumn (m_List.PosKun);
		m_list1.ScrollPositions (2);
		m_list1.DeleteColumn (m_List.PosKunName);
		m_list1.ScrollPositions (3);
	}
< ----- */
//	EnableHeadControls (TRUE);

// return TRUE;  // so mag es oninitdialog sein .... Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten

}

void CWiepKorDlg::OnBnClickedCancel()
{
	GetParent ()->DestroyWindow ();
}

BOOL CWiepKorDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_list1.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_list1 &&
					GetFocus ()->GetParent () != &m_list1 )
				{

					break;
			    }
				m_list1.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				m_list1.OnKeyD (VK_DOWN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				else if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CDateTimeCtrl)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				m_list1.OnKeyD (VK_UP);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
// ???				OnCancel () ;
				return TRUE;
			}
/* -----> Loeschen ist niemals .....
 			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
< ------ */
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

/* ---->
			else if (pMsg->wParam == VK_F8)
			{
				** ---->
				if (Choice != NULL)
				{	if (Choice->IsWindowVisible ())
					{	Choice->ShowWindow (SW_HIDE);
					}
					else
					{	Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{	OnChoice ();
				}
				< ----- **
				OnChoice();	// alternative zur echten selektion 
			}
			else if (pMsg->wParam == VK_F9)
			{	if (GetFocus () == &m_mdn)
				{	OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_schlklknr)
				{	OnChoice ();
					return TRUE;
				}

				m_List.OnKey9 ();
				return TRUE;
			}
< ----- */
	}

//return CDbPropertyPage::PreTranslateMessage(pMsg);
return CFormView::PreTranslateMessage(pMsg);
//	return FALSE ;
}

BOOL CWiepKorDlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

	/* --->
	if (Control == &m_mdn)
	{	if (!ReadMdn ())
		{	m_mdn.SetFocus ();
			return FALSE;
		}
	}
	< ---- */
	/* ----->
	if (Control == &m_schlklknr)
	{	if (!Read ())
		{	m_schlklknr.SetFocus ();
			return FALSE;
		}
	}
	< ------ */
	/* ----->
	if (Control == &m_ckalkart)
	{	kalkartfeldernachsetzen ();
		rechnenach (); 
	}
	< -------- */

	/* ---> 
	if (Control == &m_preisek ||
		Control == &m_anzahl ||
		Control == &m_lebendgew ||
		Control == &m_schlachtgewicht ||
		Control == &m_kaltgew ||
		Control == &m_ausbeute ||
		Control == &m_verlust )
	{
		rechnenach () ;
	}
	< ---- */


	if (Control != &m_list1 &&
		Control->GetParent ()!= &m_list1)
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CWiepKorDlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_list1 &&
		Control->GetParent ()!= &m_list1 )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_PrGrList.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}


BOOL CWiepKorDlg::ReadList ()
{


	m_list1.DeleteAllItems ();
	m_list1.vSelect.clear ();
	int i = 0;
	memcpy (&wiepro, &wiepro_null, sizeof (struct WIEPRO));
	wiepro.mdn = mdn.mdn;
	wiepro.nr = v_lsnr ;
	sprintf ( wiepro.blg_typ ,"L") ;

	int sqlret = 100 ;

	wiepro_class.openwiepro () ;
		sqlret =  wiepro_class.lesewiepro() ;	
	while ( ! sqlret )
	{
		FillList.InsertItem (i, 0);

/* -->
		CString pOSI;
		pOSI.Format (_T("%d"), i + 1 );
		FillList.SetItemText (pOSI.GetBuffer (), i, m_List.PosPosi);
		CString kOST_BEZ;
		kOST_BEZ = Schlaklkp.schlaklkp.kost_bez;
		FillList.SetItemText (kOST_BEZ.GetBuffer (), i, m_List.PosKost_bez);

		CString pTWERT ;
		TCHAR ptbezk [37] ; 
		pTWERT.Format (_T("%d"), Schlaklkp.schlaklkp.zubasis);
    	HoleBezk.GetPtBezk ( pTWERT.GetBuffer(), ptbezk);
	
		CString zUBASIS;
		zUBASIS.Format (_T("%d  %s"),Schlaklkp.schlaklkp.zubasis, ptbezk);
		FillList.SetItemText (zUBASIS.GetBuffer (), i, m_List.PosZubasis);

		CString zUSCHLAG;
		m_List.DoubleToString (Schlaklkp.schlaklkp.zuschlag, zUSCHLAG, 2);
		FillList.SetItemText (zUSCHLAG.GetBuffer (), i, m_List.PosZuschlag);

		double diposwert, diposwertkg ;
		switch ( Schlaklkp.schlaklkp.zubasis )
		{
//		case 0 :	// je kg
		case 1 :	// je Stck.
			diposwert = Schlaklkp.schlaklkp.zuschlag * Schlaklkk.schlaklkk.anzahl ;
			if ( Schlaklkk.schlaklkk.kaltgew != 0.0 )
			{
			    diposwertkg = (Schlaklkp.schlaklkp.zuschlag * 
					Schlaklkk.schlaklkk.anzahl) / Schlaklkk.schlaklkk.kaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;
		case 2 :	// pauschal
			diposwert = Schlaklkp.schlaklkp.zuschlag ;
			if ( Schlaklkk.schlaklkk.kaltgew != 0.0 )
			{
			    diposwertkg = Schlaklkp.schlaklkp.zuschlag / Schlaklkk.schlaklkk.kaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;

		default :	// auffang-Linie : je kg
		    diposwert = Schlaklkp.schlaklkp.zuschlag * Schlaklkk.schlaklkk.kaltgew ;
			diposwertkg = Schlaklkp.schlaklkp.zuschlag ;
			break ;
		}

		CString wERT;
		m_List.DoubleToString (diposwert, wERT, 2);
		Schlaklkp.schlaklkp.wert = CStrFuncs::StrToDouble (wERT);
		FillList.SetItemText (wERT.GetBuffer (), i, m_List.PosWert);

		CString wERTKG;
		m_List.DoubleToString (diposwertkg, wERTKG, 4);
		Schlaklkp.schlaklkp.wertkg = CStrFuncs::StrToDouble (wERTKG);
		dizuabkg += Schlaklkp.schlaklkp.wertkg ;
		FillList.SetItemText (wERTKG.GetBuffer (), i, m_List.PosWertkg);
		sqlret = Schlaklkp.dbread() ;			
		i ++;
	}

	Schlaklkk.schlaklkk.zuabkg = dizuabkg;
< ---- */

		char hilfe [55] ;

		CString pART;
		m_list1.DoubleToString (wiepro.a, pART, 0);
	//	Schlaklkp.schlaklkp.wert = CStrFuncs::StrToDouble (wERT);
		FillList.SetItemText (pART.GetBuffer (), i, m_list1.PosArt);

		CString pDAT;
		if ( wiepro.dat.day == 0 && wiepro.dat.month == 0 && wiepro.dat.year == 0)
			sprintf (hilfe, "  .  .    " ) ;
		else
			sprintf (hilfe, "%02d.%02d.%04d", wiepro.dat.day, wiepro.dat.month , wiepro.dat.year ) ;
		pDAT.Format (_T("%s") , hilfe ) ;
		FillList.SetItemText (pDAT.GetBuffer (), i, m_list1.PosDat);

		CString pZEIT;
		pZEIT = wiepro.zeit ;
		FillList.SetItemText (pZEIT.GetBuffer (), i, m_list1.PosZeit);

		CString pES;
		pES.Format (_T("%s"), wiepro.peri_nam );
		FillList.SetItemText (pES.GetBuffer (), i, m_list1.PosES);

		CString pGEW;
		m_list1.DoubleToString (wiepro.gew_nto, pGEW, 3);
	//	Schlaklkp.schlaklkp.wert = CStrFuncs::StrToDouble (wERT);
		FillList.SetItemText (pGEW.GetBuffer (), i, m_list1.PosGew);

// Ab hier beginnen die modifizierbaren Felder

		CString pANZ;
		pANZ.Format (_T("%d"), wiepro.anz );
		FillList.SetItemText (pANZ.GetBuffer (), i, m_list1.PosAnz);


		CString pHBK;
		if ( wiepro.hbk.day == 0 && wiepro.hbk.month == 0 && wiepro.hbk.year == 0)
			sprintf (hilfe, "  .  .    " ) ;
		else
			sprintf (hilfe, "%02d.%02d.%04d", wiepro.hbk.day, wiepro.hbk.month , wiepro.hbk.year ) ;
		pHBK = hilfe ;
		FillList.SetItemText (pHBK.GetBuffer (), i, m_list1.PosHBK);

		CString pIDT ;
		pIDT.Format (_T("%s"), wiepro.ls_ident) ;
		FillList.SetItemText (pIDT.GetBuffer (), i, m_list1.PosIdt);

		CString pCHR ;
		pCHR.Format (_T("%s"), wiepro.ls_char30) ;
		FillList.SetItemText (pCHR.GetBuffer (), i, m_list1.PosChr);

		CString pMOD  = _T(" ")  ;
		FillList.SetItemText (pMOD.GetBuffer (), i, m_list1.PosMod);
	
		memcpy (&wiepro, &wiepro_null, sizeof (struct WIEPRO));
		sqlret =  wiepro_class.lesewiepro() ;	
		i ++ ;
	}
	return TRUE;
}


// BOOL CWiepKorDlg::InList (WIEPRO_CLASS &wiepro_class)
 BOOL CWiepKorDlg::InList (void)
{
	ListRows.FirstPosition ();
/* --->
	CAkiPreise *aki_pr;
	while ((aki_pr = (CAkiPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Akiprgrstp.akiprgrstp.aki_nr == aki_pr->akiprgrstp.aki_nr &&
			Akiprgrstp.akiprgrstp.a == aki_pr->akiprgrstp.a) return TRUE;
	}
< ---- */

   return FALSE;
}
/* --->
void CpakdruDlg::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CSchlklkpos *pr;
	while ((pr = (CSchlklkpos *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Schlaklkp.schlaklkp, &pr->schlaklkp, sizeof (SCHLAKLKP));
		if (!InList (Schlaklkp))
		{
			Schlaklkp.dbdelete ();
		}
	}
}
< ----- */

void CWiepKorDlg::OnEnKillfocusLsnr()
{

	UpdateData(TRUE) ;
	Read () ;	// Saetze einlesen 
	UpdateData (FALSE) ;
}

void setdatum (  TIMESTAMP_STRUCT * ziel , CString quelle, int nullerlaubt )
{
// Input zwingend "dd.mm.yyyy" oder blank

	char tag[] = {"01"};
	char mon[]  = {"01"};
	char jahr[]  = {"2000"};
	if ( nullerlaubt )
	{	// Nur erlaubt, falls es im insert/update abgefangen wird 
		sprintf ( tag ,"00");
		sprintf ( mon ,"00");
		sprintf ( jahr,"0000");
	}	
	int len = quelle.GetLength ();
	if (len >=2)
	{
		memcpy (tag, quelle.Mid (0, 2), 2);
	}
	if (len >=5)
	{
		memcpy (mon, quelle.Mid (3, 2), 2);
	}
	if (len >=10)
	{
		memcpy (jahr, quelle.Mid (6, 4), 4);
	}
	else
	{
		if (len >=10)
		{
			memcpy (jahr + 2 , quelle.Mid (6, 2), 2);
		}
	}
	ziel->year = atoi ( jahr ) ;
	ziel->day = atoi ( tag ) ;
	ziel->month = atoi ( mon ) ;
	 if ( nullerlaubt && ziel->year == 0 && ziel->day == 0 && ziel->month == 0)
	 {
		 // dann halt nichts machen 
	 }
	 else
	 {
		if ( ziel->year > 2099 || ziel->year < 1900 ) ziel->year = 1999 ;
		if ( ziel->day > 31 || ziel->day < 1 ) ziel->day = 1 ;
		if ( ziel->month > 12 || ziel->month < 1 ) ziel->month = 1 ;
	 }
}

BOOL CWiepKorDlg::Read ()
{

	m_list1.StopEnter ();	
	ReadList ();
	return TRUE;
}


BOOL CWiepKorDlg::Write ()
{
//	beginwork ();
	m_list1.StopEnter ();
	int count = m_list1.GetItemCount ();

	for (int i = 0; i < count; i ++)
	{

      CString Text;
		 Text = m_list1.GetItemText (i, m_list1.PosMod);
		 if ( Text.GetBuffer()[0] != 'M' )
		 continue ;
		 memcpy (&wiepro, &wiepro_null, sizeof (struct WIEPRO));
		 // mdn,nr,dat,zeit 
		 wiepro.mdn = mdn.mdn ;
		 wiepro.nr = v_lsnr ;
		 sprintf ( wiepro.zeit , "%s" , m_list1.GetItemText (i, m_list1.PosZeit));

		 Text = m_list1.GetItemText (i, m_list1.PosDat);
		 setdatum (  &wiepro.dat , Text ,0 ) ;	// Dat MUSS sinnvll sein .....

// ab hier : aenderliche Werte .....
		 Text = m_list1.GetItemText (i, m_list1.PosHBK);
		 setdatum (  &wiepro.hbk , Text , 1 ) ;	// hbk darf sinnlos sein .....

		 Text = m_list1.GetItemText (i, m_list1.PosAnz);
		 wiepro.anz = atol ( Text.GetBuffer() ) ;			// CStrFuncs::StrToDouble (Text);

		 Text = m_list1.GetItemText (i, m_list1.PosIdt);
		 sprintf ( wiepro.ls_ident ,"%s" , Text.GetBuffer(20) ) ;
		 Text = m_list1.GetItemText (i, m_list1.PosChr);
		 sprintf ( wiepro.ls_char30 ,"%s" , Text.GetBuffer(30) ) ;	
		 wiepro_class.updawiepro() ;

/* das war das alte ....
       CString Text;
		 Text = m_list1.GetItemText (i, m_list1.PosLs);
		 fracht.ls = atol ( Text ) ;			// CStrFuncs::StrToDouble (Text);
		 fracht.mdn = mdn.mdn ;
		 fracht_class.openfracht() ;
		 int sqls = fracht_class.lesefracht() ;
		 Text = m_list1.GetItemText (i, m_list1.PosAnz);
		 fracht.anz = atoi ( Text ) ;	// CStrFuncs::StrToDouble (Text);

		 Text = m_list1.GetItemText (i, m_list1.PosAktiv);
		 if ( Text !="X" )
			 fracht.stat = 0 ;
		 else
			 fracht.stat = 1 ;


		 if ( !sqls )
		 {
			 fracht_class.updatefracht () ;
		 }
		 else
		 {
			Text = m_list1.GetItemText (i, m_list1.PosLs);
			fracht.ls = atol (Text ) ;	// CStrFuncs::StrToDouble (Text);
			fracht.mdn = mdn.mdn ;
			fracht.fil = 0 ;
			memcpy ( &fracht.lief_term, &lsk.lieferdat, sizeof (  TIMESTAMP_STRUCT )) ;
			memcpy ( &fracht.dat       , &lsk.lieferdat, sizeof ( TIMESTAMP_STRUCT )) ;

			fracht_class.insertfracht () ;
		 }
 ::::::::::::: < ------ */
	}

	return TRUE;
}

void CWiepKorDlg::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	Rows.Init ();
}
void CWiepKorDlg::OnBnClickedOk()
{
	Write () ;
	// OnOk() ;
}
