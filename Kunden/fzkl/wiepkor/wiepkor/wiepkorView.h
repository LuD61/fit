// wiepkorView.h : Schnittstelle der Klasse CwiepkorView
//


#pragma once


class CwiepkorView : public CView
{
protected: // Nur aus Serialisierung erstellen
	CwiepkorView();
	DECLARE_DYNCREATE(CwiepkorView)

// Attribute
public:
	CwiepkorDoc* GetDocument() const;

// Vorgänge
public:

// Überschreibungen
public:
	virtual void OnDraw(CDC* pDC);  // Überschrieben, um diese Ansicht darzustellen
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementierung
public:
	virtual ~CwiepkorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // Debugversion in wiepkorView.cpp
inline CwiepkorDoc* CwiepkorView::GetDocument() const
   { return reinterpret_cast<CwiepkorDoc*>(m_pDocument); }
#endif

