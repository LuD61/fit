// wiepkorView.cpp : Implementierung der Klasse CwiepkorView
//

#include "stdafx.h"
#include "wiepkor.h"

#include "wiepkorDoc.h"
#include "wiepkorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CwiepkorView

IMPLEMENT_DYNCREATE(CwiepkorView, CView)

BEGIN_MESSAGE_MAP(CwiepkorView, CView)
	// Standarddruckbefehle
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CwiepkorView-Erstellung/Zerst�rung

CwiepkorView::CwiepkorView()
{
	// TODO: Hier Code zur Konstruktion einf�gen

}

CwiepkorView::~CwiepkorView()
{
}

BOOL CwiepkorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CView::PreCreateWindow(cs);
}

// CwiepkorView-Zeichnung

void CwiepkorView::OnDraw(CDC* /*pDC*/)
{
	CwiepkorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: Code zum Zeichnen der systemeigenen Daten hinzuf�gen
}


// CwiepkorView drucken

BOOL CwiepkorView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Standardvorbereitung
	return DoPreparePrinting(pInfo);
}

void CwiepkorView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Zus�tzliche Initialisierung vor dem Drucken hier einf�gen
}

void CwiepkorView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Bereinigung nach dem Drucken einf�gen
}


// CwiepkorView-Diagnose

#ifdef _DEBUG
void CwiepkorView::AssertValid() const
{
	CView::AssertValid();
}

void CwiepkorView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CwiepkorDoc* CwiepkorView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CwiepkorDoc)));
	return (CwiepkorDoc*)m_pDocument;
}
#endif //_DEBUG


// CwiepkorView-Meldungshandler
