// wiepkorDoc.cpp : Implementierung der Klasse CwiepkorDoc
//

#include "stdafx.h"
#include "wiepkor.h"

#include "wiepkorDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CwiepkorDoc

IMPLEMENT_DYNCREATE(CwiepkorDoc, CDocument)

BEGIN_MESSAGE_MAP(CwiepkorDoc, CDocument)
END_MESSAGE_MAP()


// CwiepkorDoc-Erstellung/Zerst�rung

CwiepkorDoc::CwiepkorDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CwiepkorDoc::~CwiepkorDoc()
{
}

BOOL CwiepkorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CwiepkorDoc-Serialisierung

void CwiepkorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CwiepkorDoc-Diagnose

#ifdef _DEBUG
void CwiepkorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CwiepkorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CwiepkorDoc-Befehle
