import java.util.ArrayList;





public class Artikel {
	
	private short mdn;
	private String a_bz1;
	private String a_bz2;
	private String prp_abz1;
	private String prp_abz2;
	private String product_info;
	private int a;
	private ArrayList<Sprache> sprachenList;
	
			
	public Artikel(final short mdn, final String a_bz1, final String a_bz2, final int a, final String prp_abz1, final String prp_abz2, final String product_info) {
		
		this.setMdn(mdn);
		this.setA(a);
		this.setA_bz1(a_bz1);
		this.setA_bz2(a_bz2);
		this.setSprachenList(new ArrayList<Sprache>());
		this.setPrp_abz1(prp_abz1);
		this.setPrp_abz2(prp_abz2);
		this.setProduct_info(product_info);
		
	}


	/**
	 * @return the mdn
	 */
	public short getMdn() {
		return mdn;
	}


	/**
	 * @param mdn the mdn to set
	 */
	public void setMdn(short mdn) {
		this.mdn = mdn;
	}


	/**
	 * @return the a_bz1
	 */
	public String getA_bz1() {
		return a_bz1.trim();
	}


	/**
	 * @param a_bz1 the a_bz1 to set
	 */
	public void setA_bz1(String a_bz1) {
		this.a_bz1 = a_bz1;
	}


	/**
	 * @return the a_bz2
	 */
	public String getA_bz2() {
		return a_bz2.trim();
	}


	/**
	 * @param a_bz2 the a_bz2 to set
	 */
	public void setA_bz2(String a_bz2) {
		this.a_bz2 = a_bz2;
	}


	/**
	 * @return the a
	 */
	public int getA() {
		return a;
	}


	/**
	 * @param a the a to set
	 */
	public void setA(int a) {
		this.a = a;
	}


	/**
	 * @return the sprachenList
	 */
	public ArrayList<Sprache> getSprachenList() {
		return sprachenList;
	}


	/**
	 * @param sprachenList the sprachenList to set
	 */
	public void setSprachenList(ArrayList<Sprache> sprachenList) {
		this.sprachenList = sprachenList;
	}
	
	
	public void addSprache(final Sprache sprache) {
		this.sprachenList.add(sprache);
	}
	
	
	public Object getValueAt(int columnIndex) {		
		
		switch ( columnIndex ) {
		
		case 0:
			return this.getA();
		case 1:
			return this.getA_bz1();
		case 2:
			return this.getA_bz2();			
		}
		
		return "";
	}


	/**
	 * @return the prp_abz1
	 */
	public String getPrp_abz1() {
		return this.prp_abz1.trim();
	}


	/**
	 * @return the prp_abz1
	 */
	public String getPrp_abz2() {
		return this.prp_abz2.trim();
	}
	
	/**
	 * @param prp_abz1 the prp_abz1 to set
	 */
	public void setPrp_abz1(String prp_abz1) {
		this.prp_abz1 = prp_abz1;
	}


	/**
	 * @param prp_abz2 the prp_abz2 to set
	 */
	public void setPrp_abz2(String prp_abz2) {
		this.prp_abz2 = prp_abz2;
	}


	/**
	 * @return the product_info
	 */
	public String getProduct_info() {
		return product_info.trim();
	}


	/**
	 * @param product_info the product_info to set
	 */
	public void setProduct_info(String product_info) {
		this.product_info = product_info;
	}
	
}
