package Connection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class MySQLConnectorMYSQL {
	
	/**
	 * @param args
	 * 
	 */
	
	private Connection cn;
	
		
	public void createConnection(final String computerName, final String portNumber, final String databaseName, final String userName, final String password) { 
	 
	    try { 
		    System.out.println("* Treiber laden"); 
	    	Class.forName("com.mysql.jdbc.Driver").newInstance();
	    } 
	    catch (Exception e) { 
	        System.err.println("Unable to load driver."); 
	        e.printStackTrace(); 
	    }
	    
	    try { 
	    	System.out.println("* Verbindung aufbauen");
	    	final String url = "jdbc:mysql://" + computerName + ":"+ portNumber + "/" + databaseName + "?autoReconnect=true&allowMultiQueries=true&socketTimeout=500000";
	    	this.cn = DriverManager.getConnection(url, userName, password);
	    } 
	    catch (SQLException sqle) { 
	        System.out.println("SQLException: " + sqle.getMessage()); 
	        System.out.println("SQLState: " + sqle.getSQLState()); 
	        System.out.println("VendorError: " + sqle.getErrorCode()); 
	        sqle.printStackTrace();            
	    } 
	}


	public void closeConnection() {
		
		try {
			this.cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("SQLException: " + e.getMessage());
		}		
	}


	public ResultSet executeStatement(final String statement) {
		
		Statement stmt = null;
		ResultSet resultSet = null;
		
		try {
			stmt = this.cn.createStatement();
			resultSet = stmt.executeQuery(statement);
			System.out.println(statement);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return resultSet;
		
	}
	
	
	public int execute(final String statement) {
		
		PreparedStatement stmt = null;
		int autoIncKeyFromApi = -1;
		
		try {
			stmt = this.cn.prepareStatement(statement, PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.executeUpdate();
			System.out.println(statement);
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
		        autoIncKeyFromApi = rs.getInt(1);
		    }
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return autoIncKeyFromApi;		
	}
	
	
	public void executeUpdate(final String statement) {
		
		PreparedStatement stmt = null;
		
		try {
			stmt = this.cn.prepareStatement(statement);
			stmt.executeUpdate();
			System.out.println(statement);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("SQLException: " + e.getMessage());
		}		
	}


	public Connection getConnection() {
		return this.cn;
	}
	
}
