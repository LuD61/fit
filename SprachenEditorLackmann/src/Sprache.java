
public class Sprache {
	
	
	private short sprache;
	private String a_bz1;
	private String a_bz2;
	private String ppabz1;
	private String ppabz2;
	private String produkt_info;
	
	
	public Sprache(final short sprache, final String a_bz1, final String a_bz2, final String ppabz1, final String ppabz2, final String produkt_info) {
		
		this.sprache = sprache;
		this.a_bz1 = a_bz1;
		this.a_bz2 = a_bz2;
		this.setPpabz1(ppabz1);
		this.setPpabz2(ppabz2);
		this.produkt_info = produkt_info;
		
	}


	/**
	 * @return the sprache
	 */
	public short getSprache() {
		return sprache;
	}
	
	


	/**
	 * @return the a_bz1
	 */
	public String getA_bz1() {
		return a_bz1.trim();
	}
	
	
	public void setA_bz1(final String a_bz1) {
		this.a_bz1 = a_bz1;
	}


	/**
	 * @return the produkt_info
	 */
	public String getProdukt_info() {
		return produkt_info.trim();
	}
	
	
	public void setProdukt_info(final String produkt_info) {
		this.produkt_info = produkt_info;
	}


	/**
	 * @return the a_bz2
	 */
	public String getA_bz2() {
		return a_bz2.trim();
	}
	
	
	public void setA_bz2(final String a_bz2) {
		this.a_bz2 = a_bz2;
	}


	/**
	 * @return the pp_abz1
	 */
	public String getPpabz1() {
		return ppabz1.trim();
	}


	/**
	 * @param pp_abz1 the pp_abz1 to set
	 */
	public void setPpabz1(String pp_abz1) {
		this.ppabz1 = pp_abz1;
	}


	/**
	 * @return the ppabz2
	 */
	public String getPpabz2() {
		return ppabz2.trim();
	}


	/**
	 * @param ppabz2 the ppabz2 to set
	 */
	public void setPpabz2(String ppabz2) {
		this.ppabz2 = ppabz2;
	}
	



}
