import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.JScrollPane;
import javax.swing.JLayeredPane;
import javax.swing.JButton;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import fit.informixconnector.InformixConnector;

import javax.swing.JTextField;
import java.util.Iterator;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.UnsupportedEncodingException;

import javax.swing.JEditorPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;



public class SprachenDialog extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2321838842904587176L;
	private JLayeredPane contentPane;
	private static InformixConnector informixConnector;
	private ArtikelTabelle tableModel;
	private JTable table;
	private JTextField deBz1;
	private JTextField deBz2;
	private JTextField enBz1;
	private JTextField ruBz1;
	private JEditorPane enProduktInfo;
	private JEditorPane ruProduktInfo;
	private JEditorPane deProduktInfo;
	private JEditorPane enPPABZ1;
	private JEditorPane ruPPABZ1;
	private JEditorPane dePPABZ1;
	private JEditorPane enPPABZ2;
	private JEditorPane ruPPABZ2;
	private JEditorPane dePPABZ2;
	private Artikel sel_artikel = null;
	private boolean en_vorhanden = false;
	private boolean ru_vorhanden = false;
	private DataGenerator dataGenerator;
	private static String host;
	private static String password;
	private static String port;
	private JTextField ruBz2;
	private JTextField enBz2;

	/**
	 * Launch the application.
	 */
	
	
	public static void main(String[] args) {		
		
		try {
			host = "172.16.1.30";
			password = "Dlbm18x%";
			port = "3307";
			
			informixConnector = new InformixConnector();			
			informixConnector.createConnection();
			
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			SprachenDialog frame = new SprachenDialog();					
			frame.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	

	/**
	 * Create the frame.
	 */
	public SprachenDialog() {		
				
		dataGenerator = new DataGenerator(this.informixConnector, this.host, this.password, this.port);
		
		setTitle("Spracheneditor");	
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1105, 949);
		contentPane = new JLayeredPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		tableModel = new ArtikelTabelle(dataGenerator);
		contentPane.setLayout(null);
		
		JLabel lblEmailRechnungsversand = new JLabel("fit Spracheneditor");
		lblEmailRechnungsversand.setBounds(22, 11, 462, 50);
		lblEmailRechnungsversand.setFont(new Font("Tahoma", Font.PLAIN, 24));
		contentPane.add(lblEmailRechnungsversand);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 72, 462, 751);
		scrollPane.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				
				if ( arg0.getKeyCode() == KeyEvent.VK_DOWN || arg0.getKeyCode() == KeyEvent.VK_UP ) {
					
					System.out.println(table.getSelectedRow());
					sel_artikel = tableModel.getArtikelListe().get(table.getSelectedRow());
					
					deBz1.setText(sel_artikel.getA_bz1());
					deBz2.setText(sel_artikel.getA_bz2());
					dePPABZ1.setText(sel_artikel.getPrp_abz1());
					dePPABZ2.setText(sel_artikel.getPrp_abz2());				
					deProduktInfo.setText(sel_artikel.getProduct_info());
					
					en_vorhanden = false;
					ru_vorhanden = false;
					
					enBz1.setText("");
					enBz2.setText("");
					enPPABZ1.setText("");
					enPPABZ2.setText("");
					enProduktInfo.setText("");
					
					ruBz1.setText("");
					ruBz2.setText("");
					ruPPABZ1.setText("");
					ruPPABZ2.setText("");
					ruProduktInfo.setText("");				
					
					Iterator<Sprache> it_sprachen = sel_artikel.getSprachenList().iterator();						
					while (it_sprachen.hasNext()) {
						Sprache sprache = (Sprache) it_sprachen.next();
						final short spracheId = sprache.getSprache();
						
						if ( spracheId == 16 ) {
							enBz1.setText(sprache.getA_bz1());
							enBz2.setText(sprache.getA_bz2());
							enPPABZ1.setText(sprache.getPpabz1());
							enPPABZ2.setText(sprache.getPpabz2());
							enProduktInfo.setText(sprache.getProdukt_info());
							en_vorhanden = true;
						} else if ( spracheId == 17 ){
							ruBz1.setText(sprache.getA_bz1());
							ruBz2.setText(sprache.getA_bz2());
							ruPPABZ1.setText(sprache.getPpabz1());
							ruPPABZ2.setText(sprache.getPpabz2());
							ruProduktInfo.setText(sprache.getProdukt_info());
							ru_vorhanden = true;
						} else {
							continue;
						}					
					}					
				}
			}
		});
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(tableModel);		
		table.setRowSelectionAllowed(true);
        table.setColumnSelectionAllowed(false);
        
        JLabel lblNewLabel = new JLabel("Deutsche \u00DCbersetzung");
        lblNewLabel.setBounds(505, 34, 170, 30);
        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
        contentPane.add(lblNewLabel);
        
        deBz1 = new JTextField();
        deBz1.setBounds(656, 75, 404, 20);
        deBz1.addInputMethodListener(new InputMethodListener() {
        	public void caretPositionChanged(InputMethodEvent arg0) {
        	}
        	public void inputMethodTextChanged(InputMethodEvent arg0) {
        	}
        });
        deBz1.setBackground(Color.WHITE);
        contentPane.add(deBz1);
        deBz1.setColumns(10);
        
        deBz2 = new JTextField();
        deBz2.setBounds(656, 102, 404, 20);
        deBz2.addInputMethodListener(new InputMethodListener() {
        	public void caretPositionChanged(InputMethodEvent arg0) {
        	}
        	public void inputMethodTextChanged(InputMethodEvent arg0) {
        	}
        });
        deBz2.setBackground(Color.WHITE);
        deBz2.setColumns(10);
        contentPane.add(deBz2);
        
        JLabel lblEnglischebersetzung = new JLabel("Englische \u00DCbersetzung");
        lblEnglischebersetzung.setBounds(505, 329, 199, 30);
        lblEnglischebersetzung.setFont(new Font("Tahoma", Font.PLAIN, 14));
        contentPane.add(lblEnglischebersetzung);
        
        enBz1 = new JTextField();
        enBz1.setBounds(656, 365, 404, 20);
        enBz1.setColumns(10);
        contentPane.add(enBz1);
        
        enProduktInfo = new JEditorPane();
        enProduktInfo.setBounds(656, 521, 404, 89);
        contentPane.add(enProduktInfo);
        
        JLabel lblBezeichnung = new JLabel("Artikelbezeichnung 1");
        lblBezeichnung.setBounds(525, 368, 121, 14);
        contentPane.add(lblBezeichnung);
        
        JLabel lblBezeichnung_1 = new JLabel("Verkehrsbezeichnung 1");
        lblBezeichnung_1.setBounds(525, 427, 121, 20);
        lblBezeichnung_1.setVerticalAlignment(SwingConstants.TOP);
        lblBezeichnung_1.setHorizontalAlignment(SwingConstants.LEFT);
        contentPane.add(lblBezeichnung_1);
        
        JLabel lblProduktInfo = new JLabel("Produkt Beschreibung");
        lblProduktInfo.setBounds(525, 521, 121, 14);
        contentPane.add(lblProduktInfo);
        
        JLabel lblRussischebersetzung = new JLabel("Russische \u00DCbersetzung");
        lblRussischebersetzung.setBounds(505, 621, 199, 30);
        lblRussischebersetzung.setFont(new Font("Tahoma", Font.PLAIN, 14));
        contentPane.add(lblRussischebersetzung);
        
        JLabel lblArtikelbezeichnung = new JLabel("Artikelbezeichnung 1");
        lblArtikelbezeichnung.setBounds(525, 662, 121, 14);
        contentPane.add(lblArtikelbezeichnung);
        
        JLabel lblVerkehrsbezeichnung = new JLabel("Verkehrsbezeichnung 1");
        lblVerkehrsbezeichnung.setBounds(525, 718, 121, 14);
        contentPane.add(lblVerkehrsbezeichnung);
        
        JLabel lblProduktBeschreibung = new JLabel("Produkt Beschreibung");
        lblProduktBeschreibung.setBounds(525, 819, 121, 14);
        contentPane.add(lblProduktBeschreibung);
        
        ruProduktInfo = new JEditorPane();
        ruProduktInfo.setBounds(656, 813, 404, 89);
        contentPane.add(ruProduktInfo);
        
        ruBz1 = new JTextField();
        ruBz1.setBounds(656, 659, 404, 20);
        ruBz1.setColumns(10);
        contentPane.add(ruBz1);
        
        JButton btnNewButton = new JButton("Beenden");
        btnNewButton.setBounds(305, 879, 179, 23);
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		System.exit(0);
        	}
        });
        contentPane.add(btnNewButton);
        
        JButton button = new JButton("Eingabe speichern");
        button.setBounds(22, 879, 179, 23);
        button.addActionListener(new ActionListener() {
        	private boolean germanChanged;

			public void actionPerformed(ActionEvent e) {

        		if (! sel_artikel.getA_bz1().equals(deBz1.getText()) ){
        			sel_artikel.setA_bz1(deBz1.getText());
        			germanChanged = true;
        		}
        		
        		if (! sel_artikel.getA_bz2().equals(deBz2.getText()) ){
        			sel_artikel.setA_bz2(deBz2.getText());
        			germanChanged = true;
        		}
        		
        		if (! sel_artikel.getPrp_abz1().equals(dePPABZ1.getText()) ){
        			sel_artikel.setPrp_abz1(dePPABZ1.getText());
        			germanChanged = true;
        		}
        		
        		if (! sel_artikel.getPrp_abz2().equals(dePPABZ2.getText()) ){
        			sel_artikel.setPrp_abz2(dePPABZ2.getText());
        			germanChanged = true;
        		}
        		
        		if (! sel_artikel.getProduct_info().equals(deProduktInfo.getText()) ){
        			sel_artikel.setProduct_info(deProduktInfo.getText());
        			germanChanged = true;
        		}
        		
        		final String enB1 = enBz1.getText();
        		final String enB2 = enBz2.getText();
        		final String enpp_abz1 = enPPABZ1.getText();
        		final String enpp_abz2 = enPPABZ2.getText();
        		final String enPI = enProduktInfo.getText();
				
        		final String ruB1 = ruBz1.getText();
        		final String ruB2 = ruBz2.getText();
        		final String rupp_abz1 = ruPPABZ1.getText();
        		final String rupp_abz2 = ruPPABZ2.getText();
        		final String ruPI = ruProduktInfo.getText();
        		
        		if (!enB1.isEmpty()) {        			
        			if ( en_vorhanden ) {
        				final Sprache sprache = getSprache((short) 16);        				
        				if ( sprache != null ) {
        					sprache.setA_bz1(enB1);
            				sprache.setA_bz2(enB2);
            				sprache.setPpabz1(enpp_abz1);
            				sprache.setPpabz2(enpp_abz2);
            				sprache.setProdukt_info(enPI);
        					try {
								dataGenerator.updateValues(sprache, sel_artikel.getA(), sel_artikel.getMdn());
							} catch (UnsupportedEncodingException e1) {
								e1.printStackTrace();
							}
        				}
        			} else {
        				final short spracheId = 16;
        				final Sprache sprache = new Sprache(spracheId, enB1, enB2, enpp_abz1, enpp_abz2, enPI);
        				sel_artikel.addSprache(sprache);
        				try {
							dataGenerator.insertValues(sprache, sel_artikel.getA(), sel_artikel.getMdn());
						} catch (UnsupportedEncodingException e1) {
							e1.printStackTrace();
						}
        			}        			
        		}
        		
        		if (!ruB1.isEmpty()) {
        			if ( ru_vorhanden ) {
        				final Sprache sprache = getSprache((short) 17);
        				if ( sprache != null ) {
        					sprache.setA_bz1(ruB1);
            				sprache.setA_bz2(ruB2);
            				sprache.setPpabz1(rupp_abz1);
            				sprache.setPpabz2(rupp_abz2);
            				sprache.setProdukt_info(ruPI);
        					try {
								dataGenerator.updateValues(sprache, sel_artikel.getA(), sel_artikel.getMdn());
							} catch (UnsupportedEncodingException e1) {
								e1.printStackTrace();
							}
        				}       				
        			} else {
        				final short spracheId = 17;
        				final Sprache sprache = new Sprache(spracheId, ruB1, ruB2, rupp_abz1, rupp_abz2, ruPI);
        				sel_artikel.addSprache(sprache);
        				try {
							dataGenerator.insertValues(sprache, sel_artikel.getA(), sel_artikel.getMdn());
						} catch (UnsupportedEncodingException e1) {
							e1.printStackTrace();
						}
        			}        			
        		}
        		
        		if ( germanChanged ) {
        			dataGenerator.updateGermanValues(sel_artikel);	
        		}      		
        	}
		});
        contentPane.add(button);
        
        JLabel label = new JLabel("Artikelbezeichnung 1");
        label.setBounds(525, 76, 121, 14);
        contentPane.add(label);
        
        JLabel lblArtikelbezeichnung_1 = new JLabel("Artikelbezeichnung 2");
        lblArtikelbezeichnung_1.setBounds(525, 105, 121, 14);
        contentPane.add(lblArtikelbezeichnung_1);
        
        JLabel lblVerkehrsbezeichnung_1 = new JLabel("Verkehrsbezeichnung 1");
        lblVerkehrsbezeichnung_1.setBounds(525, 136, 121, 14);
        contentPane.add(lblVerkehrsbezeichnung_1);
        
        enPPABZ1 = new JEditorPane();
        enPPABZ1.setBounds(656, 425, 404, 40);
        contentPane.setLayer(enPPABZ1, 2);
        contentPane.add(enPPABZ1);
        
        ruPPABZ1 = new JEditorPane();
        ruPPABZ1.setBounds(656, 717, 404, 38);
        contentPane.add(ruPPABZ1);
        
        dePPABZ1 = new JEditorPane();
        dePPABZ1.setBounds(656, 133, 404, 38);
        dePPABZ1.addInputMethodListener(new InputMethodListener() {
        	public void caretPositionChanged(InputMethodEvent arg0) {
        	}
        	public void inputMethodTextChanged(InputMethodEvent arg0) {
        	}
        });
        dePPABZ1.setBackground(Color.WHITE);
        dePPABZ1.setForeground(Color.BLACK);
        contentPane.add(dePPABZ1);
        
        JLabel lblArtikelbezeichnung_2 = new JLabel("Artikelbezeichnung 2");
        lblArtikelbezeichnung_2.setBounds(525, 690, 121, 14);
        contentPane.add(lblArtikelbezeichnung_2);
        
        ruBz2 = new JTextField();
        ruBz2.setBounds(656, 687, 404, 20);
        ruBz2.setColumns(10);
        contentPane.add(ruBz2);
        
        JLabel lblArtikelbezeichnung_3 = new JLabel("Artikelbezeichnung 2");
        lblArtikelbezeichnung_3.setBounds(525, 400, 121, 14);
        contentPane.add(lblArtikelbezeichnung_3);
        
        enBz2 = new JTextField();
        enBz2.setBounds(656, 396, 404, 20);
        enBz2.setColumns(10);
        contentPane.add(enBz2);
        
        JLabel label_1 = new JLabel("Produkt Beschreibung");
        label_1.setBounds(525, 229, 121, 14);
        contentPane.add(label_1);
        
        deProduktInfo = new JEditorPane();
        deProduktInfo.setBounds(656, 229, 404, 89);
        deProduktInfo.addInputMethodListener(new InputMethodListener() {
        	public void caretPositionChanged(InputMethodEvent arg0) {
        	}
        	public void inputMethodTextChanged(InputMethodEvent arg0) {
        	}
        });
        contentPane.add(deProduktInfo);
        
        JLabel lblVerkehrsbezeichnung_2 = new JLabel("Verkehrsbezeichnung 2");
        lblVerkehrsbezeichnung_2.setBounds(525, 183, 121, 14);
        contentPane.add(lblVerkehrsbezeichnung_2);
        
        dePPABZ2 = new JEditorPane();
        dePPABZ2.setBounds(656, 180, 404, 38);
        dePPABZ2.addInputMethodListener(new InputMethodListener() {
        	public void caretPositionChanged(InputMethodEvent arg0) {
        	}
        	public void inputMethodTextChanged(InputMethodEvent arg0) {
        	}
        });
        dePPABZ2.setForeground(Color.BLACK);
        dePPABZ2.setBackground(Color.WHITE);
        contentPane.add(dePPABZ2);
        
        JLabel lblVerkehrsbezeichnung_4 = new JLabel("Verkehrsbezeichnung 2");
        lblVerkehrsbezeichnung_4.setBounds(525, 761, 121, 14);
        contentPane.add(lblVerkehrsbezeichnung_4);
        
        ruPPABZ2 = new JEditorPane();
        ruPPABZ2.setBounds(656, 760, 404, 38);
        contentPane.add(ruPPABZ2);
        
        JLabel lblVerkehrsbezeichnung_3 = new JLabel("Verkehrsbezeichnung 2");
        lblVerkehrsbezeichnung_3.setBounds(525, 472, 121, 20);
        lblVerkehrsbezeichnung_3.setVerticalAlignment(SwingConstants.TOP);
        lblVerkehrsbezeichnung_3.setHorizontalAlignment(SwingConstants.LEFT);
        contentPane.add(lblVerkehrsbezeichnung_3);
        
        enPPABZ2 = new JEditorPane();
        enPPABZ2.setBounds(656, 470, 404, 40);
        contentPane.add(enPPABZ2);
		table.getTableHeader().setResizingAllowed(true);
		
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				System.out.println(table.getSelectedRow());
				sel_artikel = tableModel.getArtikelListe().get(table.getSelectedRow());
				
				deBz1.setText(sel_artikel.getA_bz1());
				deBz2.setText(sel_artikel.getA_bz2());
				dePPABZ1.setText(sel_artikel.getPrp_abz1());
				dePPABZ2.setText(sel_artikel.getPrp_abz2());				
				deProduktInfo.setText(sel_artikel.getProduct_info());
				
				en_vorhanden = false;
				ru_vorhanden = false;
				
				enBz1.setText("");
				enBz2.setText("");
				enPPABZ1.setText("");
				enPPABZ2.setText("");
				enProduktInfo.setText("");
				
				ruBz1.setText("");
				ruBz2.setText("");
				ruPPABZ1.setText("");
				ruPPABZ2.setText("");
				ruProduktInfo.setText("");				
				
				Iterator<Sprache> it_sprachen = sel_artikel.getSprachenList().iterator();						
				while (it_sprachen.hasNext()) {
					Sprache sprache = (Sprache) it_sprachen.next();
					final short spracheId = sprache.getSprache();
					
					if ( spracheId == 16 ) {
						enBz1.setText(sprache.getA_bz1());
						enBz2.setText(sprache.getA_bz2());
						enPPABZ1.setText(sprache.getPpabz1());
						enPPABZ2.setText(sprache.getPpabz2());
						enProduktInfo.setText(sprache.getProdukt_info());
						en_vorhanden = true;
					} else if ( spracheId == 17 ){
						ruBz1.setText(sprache.getA_bz1());
						ruBz2.setText(sprache.getA_bz2());
						ruPPABZ1.setText(sprache.getPpabz1());
						ruPPABZ2.setText(sprache.getPpabz2());
						ruProduktInfo.setText(sprache.getProdukt_info());
						ru_vorhanden = true;
					} else {
						continue;
					}					
				}
				
				
				
			}
		});

	
	}
	
	private Sprache getSprache(final short spracheId) {
		
		Iterator<Sprache> sprachen = sel_artikel.getSprachenList().iterator();
		while (sprachen.hasNext()) {
			Sprache sprache = (Sprache) sprachen.next();
			if ( sprache.getSprache() == spracheId ) {
				return sprache;				
			}
		}
		return null;		
	}
}
