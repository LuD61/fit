

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import Connection.MySQLConnectorMYSQL;

import fit.informixconnector.InformixConnector;


public class DataGenerator {
	
	private InformixConnector informixConnector;
	private MySQLConnectorMYSQL mySQLConnectorMYSQL;


	public DataGenerator (final InformixConnector informixConnector, final String host, final String password, final String port) {
		this.informixConnector = informixConnector;
		this.mySQLConnectorMYSQL = new MySQLConnectorMYSQL();
		this.mySQLConnectorMYSQL.createConnection(host, port, "xt_shop", "root", password);
	}
	
	
	public ArrayList<Artikel> getValues() throws SQLException, IOException {
		
		ArrayList<Artikel> artikelListe = this.getInformixValues();
		artikelListe = this.getMySQLValues(artikelListe);
		
		return artikelListe;
	}
	
	
	public ArrayList<Artikel> getMySQLValues(ArrayList<Artikel> artikelListe) throws SQLException {
		
		
				
		final String stmt2 = "select a " +
				"				, a_bz1 " +
				"				, a_bz2 " +
				"				, ppabz1 " +
				"				, ppabz2 " +
				"				, produkt_info " +
				"				, sprache " +
				"				from asprache " +
				"				order by a;";
		
		final ResultSet resultSet = this.mySQLConnectorMYSQL.executeStatement(stmt2);
		
		while(resultSet.next()) {
			
			int a_aktuell = resultSet.getInt("a");
			String asbz1 = resultSet.getString("a_bz1");
			String asbz2 = resultSet.getString("a_bz2");
			String ppabz1 = resultSet.getString("ppabz1");
			if ( ppabz1 == null ) {
				ppabz1 = "";
			}
			String ppabz2 = resultSet.getString("ppabz2");
			if ( ppabz2 == null ) {
				ppabz2 = "";
			}
			String asproduktinfo = resultSet.getString("produkt_info");
			final short spracheId = resultSet.getShort("sprache");
			
			Iterator<Artikel> it_artikel = artikelListe.iterator();
			while (it_artikel.hasNext()) {
				Artikel artikel = (Artikel) it_artikel.next();
				
				if ( artikel.getA() == a_aktuell )  {
					Sprache sprache = new Sprache(spracheId, asbz1, asbz2, ppabz1, ppabz2, asproduktinfo);
					artikel.addSprache(sprache);
				}
			}
		}		
		return artikelListe;		
	}
	


	public ArrayList<Artikel> getInformixValues() throws SQLException, IOException {
		
		final ArrayList<Artikel> artikelListe = new ArrayList<Artikel>();

		final String stmt = "select a.a as a " +
				"				, a.mdn as mdn " +
				"				, a.a_bz1 as abz1 " +
				"				, a.a_bz2 as abz2 " +
				"				, aw.pp_a_bz1 as ppabz1 " +
				"				, aw.pp_a_bz2 as ppabz2 " +
				"				, a.produkt_info as produkt_info " +
				"				from a_bas a, outer a_bas_erw aw " +
				" 				where a.a = aw.a " +
				"				order by a.a;";
		
		final ResultSet ordersResultSet = informixConnector.executeQuery(stmt);
		boolean ersterLauf = true;
		int a_aktuell = 0;
		int a_vorher = 0;
		Artikel artikel = null;
		
		while(ordersResultSet.next()) {
			
			final short mdn = ordersResultSet.getShort("mdn");
			a_aktuell = ordersResultSet.getInt("a");
			String abz1 = ordersResultSet.getString("abz1");
			String abz2 = ordersResultSet.getString("abz2");
			String ppabz1 = ordersResultSet.getString("ppabz1");
			if ( ppabz1 == null ) {
				ppabz1 = "";
			}
			String ppabz2 = ordersResultSet.getString("ppabz2");
			if ( ppabz2 == null ) {
				ppabz2 = "";
			}
			String produkt_info = ordersResultSet.getString("produkt_info");
			if ( produkt_info == null ) {
				produkt_info = "";
			}
			
			if ( ersterLauf || a_aktuell != a_vorher ) {
				artikel = new Artikel(mdn, abz1, abz2, a_aktuell, ppabz1, ppabz2, produkt_info);
			}			
			
			if ( !artikelListe.contains(artikel) ) {
				artikelListe.add(artikel);
			}
			
			a_vorher = a_aktuell;
			ersterLauf = false;			
		}		
		
		return artikelListe;
	}
	
	
	
	
	public void insertValues(final Sprache sprache, final int a, final short mdn) throws UnsupportedEncodingException {		
		
		String insertstmt = 
				"INSERT INTO asprache" +
				"(mdn," +
				"a," +
				"sprache," +
				"a_bz1," +
				"a_bz2," +
				"ppabz1," +
				"ppabz2," +
				"produkt_info" +
				")	VALUES (" +
				mdn + "," +
				a + "," +
				sprache.getSprache() + ",'" +
				sprache.getA_bz1() + "','" +
				sprache.getA_bz2() + "','" +
				sprache.getPpabz1() + "','" +
				sprache.getPpabz2() + "','" +
				sprache.getProdukt_info() + "'" +
				"); ";						
		
		this.mySQLConnectorMYSQL.executeUpdate(insertstmt);
	}
	
	
	public void updateValues(final Sprache sprache, final int a, final short mdn) throws UnsupportedEncodingException {
		
		String updateStmt = 
				"UPDATE asprache SET a_bz1 = '" + sprache.getA_bz1() + "', a_bz2 = '" + sprache.getA_bz2() + "', " + 
						"ppabz1 = '" + sprache.getPpabz1() + "'," +
						"ppabz2 = '" + sprache.getPpabz2() + "'," +
						"produkt_info = '" + sprache.getProdukt_info() + "' where a = " + a + " and mdn = " + mdn + ";";
		
		this.mySQLConnectorMYSQL.executeUpdate(updateStmt);
	}


	public void updateGermanValues(final Artikel artikel) {
		
		String updateStmt_a_bas = 
				"UPDATE a_bas SET a_bz1 = '" + artikel.getA_bz1() + "', a_bz2 = '" + artikel.getA_bz2() + "', produkt_info" + 
						" = '" + artikel.getProduct_info() + "' where a = " + artikel.getA() + " and mdn = " + artikel.getMdn() + ";";

		String updateStmt_a_bas_erw = 
				"UPDATE a_bas_erw SET pp_a_bz1 = '" + artikel.getPrp_abz1() + "'," +
						"pp_a_bz2 = '" + artikel.getPrp_abz2() + "'" + 
						" where a = " + artikel.getA()  + ";";
		
		//TODO Errorhandling
		this.informixConnector.executeStatement(updateStmt_a_bas_erw);
		this.informixConnector.executeStatement(updateStmt_a_bas);
		
	}
	

}
