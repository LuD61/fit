#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "wmaskc.h"
#include "colbut.h"
#include "dbclass.h"
#include "cmask.h"
#include "mo_cancel.h"
#include "lbox.h"

#define BuOk      1001
#define BuCancel  1002
#define LIBOX     1003
#define BuDial    1004
#define BuAll     1005


static mfont mFont = {
                     "MS Sans Serif", 
                      100, 
                      0, 
                      0, 
                      BLACKCOL, 
                      LTGRAYCOL,
                      0, 0};


static CFIELD **_fText;
static CFORM *fText;

static CFIELD **_fButton;
static CFORM *fButton;

static CFIELD **_fWork;
static CFORM *fWork;

int (*CANCEL::CaFunc) (int) = NULL;
int (*CANCEL::AbrProc) (int) = NULL;

CANCEL::CANCEL (int cx, int cy, char *Text, char *Bu)
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;
			char *BuText;

			static char zl [5];
			static char tr [5];
			static char ln [5];
			static char *texttab [10];
			static char text [80];
			int zeile;
			char *params [5];

			vkidx = 0;
			imgidx = 0;
			Images[0] = NULL;
			CaFunc = NULL;
			AbrProc = NULL;
			BuText = "Abbruch";
			if (Bu) BuText = Bu;
 	        this->Font = &mFont;  
			
			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			sprintf (ln, "%d", cx);

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;

			x = 0;
			y = (cy - 2) * tm.tmHeight;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);

			strcpy (tr, "\n");
			for (zeile = 0;; zeile ++)
			{
				 sprintf (zl, "%d", zeile + 1);
                 params[0] = Text;
				 params[1] = text;
                 params[2] = ln;
				 params[3] = zl;
				 params[4] = tr;
				 if (getline (params) == 0) break;
				 this->cy += tm.tmHeight; 
            }
   
			_fText = new CFIELD * [zeile];

			y = 1;
			for (zeile = 0;; zeile ++)
			{
				 texttab[zeile] = new char [80];
				 sprintf (zl, "%d", zeile + 1);
                 params[0] = Text;
				 params[1] = texttab[zeile];
                 params[2] = ln;
				 params[3] = zl;
				 params[4] = tr;
				 if (getline (params) == 0) break;
 
				 clipped (texttab[zeile]);
 			     _fText[zeile] = new CFIELD ("Txt",  texttab[zeile], 
				                                 strlen (texttab [zeile]) ,cy, 
//												 size.cx, 
                                                 -1,
												 zeile * size.cy,
				                                 NULL, "", CDISPLAYONLY,
                                                 100 + y, Font, 1, 0);				    
            }
			delete texttab [zeile];
			fText       = new CFORM (zeile, _fText);
			
			_fButton    = new CFIELD * [1];
			_fButton[0] = new CFIELD ("Cancel", BuText, 
				                                  cx , cy, x * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (1, _fButton);

            _fWork    = new CFIELD * [2];
			_fWork[0] = new CFIELD ("fText", fText, 0, 0, 0, 0, NULL, "", 
				                                 CFFORM,
				                                 980, Font, 0, 0);
			_fWork[1] = new CFIELD ("Button", fButton, 0, 0, -1, -2, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (2, _fWork);
}


CANCEL::~CANCEL ()
{
	        int i;

	        fWork->destroy (); 

			if (fText)
			{
				for (i = 0; i < fText->GetFieldanz (); i ++)
				{
			              if (fText->GetCfield ()[i]->GetAttribut () != CFFORM)
						  {
							        delete fText->GetCfield () [i]->GetFeld ();
			                        delete fText->GetCfield () [i];
						  }
				}
				delete fText->GetCfield ();
		        delete fText;
				fText = NULL;
			}

			if (fButton)
			{
				for (i = 0; i < fButton->GetFieldanz (); i ++)
				{
			              if (fButton->GetCfield ()[i]->GetAttribut () != CFFORM)
						  {
			                        delete fButton->GetCfield () [i];
						  }
				}
				delete fButton->GetCfield ();
		        delete fButton;
				fButton = NULL;
			}

			if (fWork)
			{
				for (i = 0; i < fWork->GetFieldanz (); i ++)
				{
			              if (fWork->GetCfield ()[i]->GetAttribut () != CFFORM)
						  {
			                        delete fWork->GetCfield () [i];
						  }
				}
				delete fWork->GetCfield ();
		        delete fWork;
				fWork = NULL;
			}

            CaFunc   = NULL;
			for (i = 0; i < imgidx; i ++)
			{
				if (Images[i]) delete Images[i];
				Images[i] = NULL;
			}
}

void CANCEL::SetCaFunc (int (*CaF) (int))
{
	       CaFunc = CaF;
}

void CANCEL::SetAbrProc (int (*AbrP) (int))
{
	       AbrProc = AbrP;
}

void CANCEL::AddAccelerator (int vkey, int BuNr, int (*funk) (void))
{
	        Vkeys[vkidx].SethWnd (hWnd);  
            Vkeys[vkidx].SetBuId ((DWORD) 0);
	        switch (BuNr)
			{
			case 1 :
	                Vkeys[vkidx].SetBuId (BuCancel);
					break;
			}
			Vkeys[vkidx].SetVkey (vkey);
			Vkeys[vkidx].SetFunk (funk);
			if (vkidx < 20) vkidx ++;
}

void CANCEL::AddImage (HBITMAP ibmp, HBITMAP imsk)
{
	        if (imgidx == MAXIMAGE - 1) return;
			Images[imgidx] = new IMG (hWnd, ibmp, imsk);
			imgidx ++;
			Images[imgidx] = NULL;
		    fWork->SetImage (Images, LIBOX);
}

void CANCEL::SetCurrentID (DWORD ID)
{
	       if (fWork->SetCurrentID (ID))
		   {
			   currentfield = ID;
		   }
}

void CANCEL::SetCurrentName (char *name)
{
	       if (fWork->SetCurrentName (name))
		   {
			   currentfield = fWork->GetID (name);
		   }
}

void CANCEL::EnableID (DWORD ID, BOOL flag)
{
	       fWork->EnableID (ID, flag);
}

void CANCEL::EnableName (char *name, BOOL flag)
{
	       fWork->EnableName (name, flag);
}

void CANCEL::CheckID (DWORD ID, BOOL flag)
{
	       fWork->CheckID (ID, flag);
}

void CANCEL::CheckName (char *name, BOOL flag)
{
	       fWork->CheckName (name, flag);
}

BOOL CANCEL::TestButtons (HWND hWndBu)
{
	       if (hWndBu == fWork->GethWndID (BuCancel))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuCancel, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
		   return FALSE;
}

BOOL CANCEL::IsVKey (int vkey)
{
	      switch (vkey)
		  {
		         case VK_F1 :
		         case VK_F2 :
		         case VK_F3 :
		         case VK_F4 :
		         case VK_F5 :
		         case VK_F6 :
		         case VK_F7 :
		         case VK_F8 :
		         case VK_F9 :
		         case VK_F10 :
		         case VK_F11 :
		         case VK_F12 :
		         case VK_F13 :
		         case VK_F14 :
		         case VK_F15 :
		         case VK_F16 :
		         case VK_F17 :
		         case VK_F18 :
		         case VK_F19 :
		         case VK_F20 :
					 return TRUE;
		  }
		  return FALSE;
}
	       
BOOL CANCEL::TestVKeys (int vkey)
{
	      int i;

		  if (IsVKey (vkey) == FALSE)
		  {
			  return FALSE;
		  }
		  for (i = 0; i < vkidx; i ++)
		  {
			  if (Vkeys[i].DoVkey (vkey)) return TRUE;
		  }
		  return FALSE;
}

void CANCEL::ProcessMessages (void)
{
	      MSG msg;
		  HWND hWnd;

          hWnd = fWork->GethWndID (LIBOX);
		  fWork->SetCurrent (0);
          SendMessage (hWnd, LB_SETCURSEL, 0, 0L);
          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
			      if (msg.wParam == VK_TAB)
				  {

                      if (GetKeyState (VK_SHIFT) < 0)
                      {
                              syskey = KEYSTAB;
//                              fWork->PriorFormField (); 
                              fWork->PriorField (); 
					  }
					  else
					  {
					          syskey = KEYTAB;
//                              fWork->NextFormField (); 
                              fWork->NextField (); 
					  }
					  continue;
				  }

                  else if (TestVKeys (msg.wParam))
				  {
					       continue;
				  }

				  else if (msg.hwnd == fWork->GethWndName ("Liste"));
				  else if (msg.wParam == VK_DOWN)
				  {

					  syskey = KEYDOWN;
                      fWork->NextField (); 
					  continue;
				  }
				  else if (msg.wParam == VK_UP)
				  {

					  syskey = KEYUP;
                      fWork->PriorField (); 
					  continue;
				  }
				  else if (msg.wParam == VK_RETURN)
				  {

					  if (TestButtons (msg.hwnd))
					  {
					           continue;
					  }
				  }
			  }
              TranslateMessage(&msg);
			  DispatchMessage(&msg);
		  }
}


CALLBACK CANCEL::CProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					fWork->display (hWnd, hdc);
                    EndPaint (hWnd, &ps);
					break;
              case WM_SIZE :
				    MoveWindow ();
					break;
              case WM_COMMAND :
				    if (AbrProc)
					{
						    if ((*AbrProc) (0))
							{

 		                           syskey = KEY5;
							}
							break;
					}
                    if (LOWORD (wParam) == BuCancel)
                    {
						   if (CaFunc)
						   {
							   if ((*CaFunc) (0) == 0)
							   {
								   break;
							   }
						   }
 		                   syskey = KEY5;
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void CANCEL::DestroyWindow (void)
{
	       fWork->destroy ();
		   ::DestroyWindow (hWnd);
}

void CANCEL::Enable (BOOL mode)
{
	      EnableWindow (hWnd, mode);
}
						
void CANCEL::OpenWindow (HANDLE hInstance, HWND hMainWindow)
{
		  int x, y;
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  RECT rect, rect1;

		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;
//		  SetColBorderM (TRUE);

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CCancel";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }

		  GetClientRect (hMainWindow, &rect);
		  GetWindowRect (hMainWindow, &rect1);

		  x  = max (0, (rect.right - cx) / 2)  + rect1.left;
		  y  = max (0, (rect.bottom - cy) / 2) + rect1.top;

          hWnd = CreateWindowEx (0, 
                                 "CCancel",
                                 "",
                                 WS_VISIBLE | WS_POPUP | WS_THICKFRAME,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
		  ShowWindow (hWnd, SW_SHOWNORMAL);
		  UpdateWindow (hWnd);
}

void CANCEL::MoveWindow (void)
{
	      fWork->MoveWindow ();
		  fButton->Invalidate (FALSE);
}


void CA_CLASS::SetCaFunc (int (*abrproc) (int))
{
          AbrProc = NULL;
	      CaFunc  = abrproc; 
}

void CA_CLASS::SetAbrProc (int (*abrproc) (int))
{
	      CaFunc = NULL; 
          AbrProc = abrproc;
} 


BOOL CA_CLASS::Execute (HWND hWnd,int cx, int cy, char *text, char *BuText)
{
         Ca = new CANCEL (cx, cy, text, BuText);
		 if (Ca == NULL) return FALSE; 
	     Ca->SetCaFunc (CaFunc);
	     Ca->SetAbrProc (AbrProc);
         Ca->OpenWindow ((HINSTANCE) GetClassLong (hWnd,GCL_HMODULE),
			              hWnd);
		 return TRUE;
}

void CA_CLASS::Enable (BOOL mode)
{
	     if (Ca)
		 {
			 Ca->Enable (mode);
		 }
}

void CA_CLASS::Destroy (void)
{
	     if (Ca)
		 {
			 Ca->DestroyWindow ();
			 delete Ca;
			 Ca = NULL;
		 }
}
