#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "fil_inv.h"

struct FIL_INV fil_inv, fil_inv_null;

void FIL_INV_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &fil_inv.mdn, 1, 0);
            ins_quest ((char *) &fil_inv.fil, 1, 0);
            ins_quest ((char *) &fil_inv.dat, 2, 0);
            ins_quest ((char *) &fil_inv.a, 3, 0);
            ins_quest ((char *) &fil_inv.rowid, 2, 0);
    out_quest ((char *) &fil_inv.delstatus,1,0);
    out_quest ((char *) &fil_inv.a,3,0);
    out_quest ((char *) &fil_inv.dat,2,0);
    out_quest ((char *) &fil_inv.fil,1,0);
    out_quest ((char *) &fil_inv.mdn,1,0);
    out_quest ((char *) &fil_inv.me,3,0);
    out_quest ((char *) &fil_inv.me_einh,1,0);
    out_quest ((char *) &fil_inv.ums_ek_bew,3,0);
    out_quest ((char *) &fil_inv.ums_vk_bew,3,0);
    out_quest ((char *) &fil_inv.posi,2,0);
            cursor = prepare_sql ("select fil_inv.delstatus,  "
"fil_inv.a,  fil_inv.dat,  fil_inv.fil,  fil_inv.mdn,  fil_inv.me,  "
"fil_inv.me_einh,  fil_inv.ums_ek_bew,  fil_inv.ums_vk_bew,  "
"fil_inv.posi from fil_inv "

#line 30 "fil_inv.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   dat = ? "
                                  "and   a = ?"
								   "and rowid = ?");

    ins_quest ((char *) &fil_inv.delstatus,1,0);
    ins_quest ((char *) &fil_inv.a,3,0);
    ins_quest ((char *) &fil_inv.dat,2,0);
    ins_quest ((char *) &fil_inv.fil,1,0);
    ins_quest ((char *) &fil_inv.mdn,1,0);
    ins_quest ((char *) &fil_inv.me,3,0);
    ins_quest ((char *) &fil_inv.me_einh,1,0);
    ins_quest ((char *) &fil_inv.ums_ek_bew,3,0);
    ins_quest ((char *) &fil_inv.ums_vk_bew,3,0);
    ins_quest ((char *) &fil_inv.posi,2,0);
            sqltext = "update fil_inv set "
"fil_inv.delstatus = ?,  fil_inv.a = ?,  fil_inv.dat = ?,  "
"fil_inv.fil = ?,  fil_inv.mdn = ?,  fil_inv.me = ?,  "
"fil_inv.me_einh = ?,  fil_inv.ums_ek_bew = ?,  "
"fil_inv.ums_vk_bew = ?,  fil_inv.posi = ? "

#line 37 "fil_inv.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   dat = ? "
                                  "and   a = ?"
                                  "and posi = ?";


            ins_quest ((char *) &fil_inv.mdn, 1, 0);
            ins_quest ((char *) &fil_inv.fil, 1, 0);
            ins_quest ((char *) &fil_inv.dat, 2, 0);
            ins_quest ((char *) &fil_inv.a, 3, 0);
            ins_quest ((char *) &fil_inv.posi, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &fil_inv.mdn, 1, 0);
            ins_quest ((char *) &fil_inv.fil, 1, 0);
            ins_quest ((char *) &fil_inv.dat, 2, 0);
            ins_quest ((char *) &fil_inv.a, 3, 0);
            ins_quest ((char *) &fil_inv.posi, 2, 0);
            test_upd_cursor = prepare_sql ("select a from fil_inv "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   dat = ? "
                                  "and   a = ?"
                                  "and posi = ?");
            ins_quest ((char *) &fil_inv.mdn, 1, 0);
            ins_quest ((char *) &fil_inv.fil, 1, 0);
            ins_quest ((char *) &fil_inv.dat, 2, 0);
            ins_quest ((char *) &fil_inv.a, 3, 0);
            ins_quest ((char *) &fil_inv.posi, 2, 0);
            del_cursor = prepare_sql ("delete from fil_inv "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   dat = ? "
                                  "and   a = ?"
                                  "and posi = ?");
            ins_quest ((char *) &fil_inv.mdn, 1, 0);
            ins_quest ((char *) &fil_inv.fil, 1, 0);
            ins_quest ((char *) &fil_inv.dat, 2, 0);
            del_best_curs = prepare_sql ("delete from fil_inv "
                                         "where mdn = ? "
                                         "and   fil = ? "
                                         "and   dat = ? ");
    ins_quest ((char *) &fil_inv.delstatus,1,0);
    ins_quest ((char *) &fil_inv.a,3,0);
    ins_quest ((char *) &fil_inv.dat,2,0);
    ins_quest ((char *) &fil_inv.fil,1,0);
    ins_quest ((char *) &fil_inv.mdn,1,0);
    ins_quest ((char *) &fil_inv.me,3,0);
    ins_quest ((char *) &fil_inv.me_einh,1,0);
    ins_quest ((char *) &fil_inv.ums_ek_bew,3,0);
    ins_quest ((char *) &fil_inv.ums_vk_bew,3,0);
    ins_quest ((char *) &fil_inv.posi,2,0);
            ins_cursor = prepare_sql ("insert into fil_inv ("
"delstatus,  a,  dat,  fil,  mdn,  me,  me_einh,  ums_ek_bew,  ums_vk_bew,  posi) "

#line 81 "fil_inv.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?)");

#line 83 "fil_inv.rpp"
}

void FIL_INV_CLASS::prepare_o (char *order)
{

            ins_quest ((char *) &fil_inv.mdn, 1, 0);
            ins_quest ((char *) &fil_inv.fil, 1, 0);
            ins_quest ((char *) &fil_inv.dat, 2, 0);
            if (order)
            {
    out_quest ((char *) &fil_inv.delstatus,1,0);
    out_quest ((char *) &fil_inv.a,3,0);
    out_quest ((char *) &fil_inv.dat,2,0);
    out_quest ((char *) &fil_inv.fil,1,0);
    out_quest ((char *) &fil_inv.mdn,1,0);
    out_quest ((char *) &fil_inv.me,3,0);
    out_quest ((char *) &fil_inv.me_einh,1,0);
    out_quest ((char *) &fil_inv.ums_ek_bew,3,0);
    out_quest ((char *) &fil_inv.ums_vk_bew,3,0);
    out_quest ((char *) &fil_inv.posi,2,0);
                    cursor_o = prepare_sql ("select "
"fil_inv.delstatus,  fil_inv.a,  fil_inv.dat,  fil_inv.fil,  fil_inv.mdn,  "
"fil_inv.me,  fil_inv.me_einh,  fil_inv.ums_ek_bew,  "
"fil_inv.ums_vk_bew,  fil_inv.posi from fil_inv "

#line 94 "fil_inv.rpp"
                                          "where mdn = ? "
                                          "and   fil = ? "
                                          "and   dat = ? %s", order);
            }
            else
            {
    out_quest ((char *) &fil_inv.delstatus,1,0);
    out_quest ((char *) &fil_inv.a,3,0);
    out_quest ((char *) &fil_inv.dat,2,0);
    out_quest ((char *) &fil_inv.fil,1,0);
    out_quest ((char *) &fil_inv.mdn,1,0);
    out_quest ((char *) &fil_inv.me,3,0);
    out_quest ((char *) &fil_inv.me_einh,1,0);
    out_quest ((char *) &fil_inv.ums_ek_bew,3,0);
    out_quest ((char *) &fil_inv.ums_vk_bew,3,0);
    out_quest ((char *) &fil_inv.posi,2,0);
                    cursor_o = prepare_sql ("select "
"fil_inv.delstatus,  fil_inv.a,  fil_inv.dat,  fil_inv.fil,  fil_inv.mdn,  "
"fil_inv.me,  fil_inv.me_einh,  fil_inv.ums_ek_bew,  "
"fil_inv.ums_vk_bew,  fil_inv.posi from fil_inv "

#line 101 "fil_inv.rpp"
                                          "where mdn = ? "
                                          "and   fil = ? "
                                          "and   dat = ? ");
           }
}

int FIL_INV_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int FIL_INV_CLASS::dbreadfirst_o (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{

         if (cursor == -1)
         {
                this->prepare ();
         }
         if (cursor_o == -1)
         {
                this->prepare_o (order);
         }
         open_sql (cursor_o);
         fetch_sql (cursor_o);
         return (sqlstatus);
}

int FIL_INV_CLASS::dbread_o ()
/**
Ersten Satz aus Tabelle lesen.
**/
{

         fetch_sql (cursor_o);
         return (sqlstatus);
}

int FIL_INV_CLASS::dbdeletebest ()
/**
Alles Positionen einer Bestellung loeschen.
**/
{
         if (del_best_curs == -1)
         {
                     prepare ();
         }
         return execute_curs (del_best_curs);
}

int FIL_INV_CLASS::dbclose_o ()
/**
Cursor schliessen.
**/
{
         if (del_best_curs)
         {
                      close_sql (del_best_curs);
                      del_best_curs = -1;
         }
         this->DB_CLASS::dbclose ();
         return 0;
}
int FIL_INV_CLASS::dbinsert (void)
/**
In fil_inv einfuegen.
**/
{
        int dsqlstatus;
        if (ins_cursor == -1)
        {
                       prepare ();
        }
        dsqlstatus = execute_curs (ins_cursor);
        return dsqlstatus;
}


