#ifndef _SEARCHBEST_DEF
#define _SEARCHBEST_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

class SEARCHBEST
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static int idx;
           static long anz;
           static CHQEX *Query;

           int SearchPos;
           int OKPos;
           int CAPos;
           char Key [512];
           HBITMAP Sel;
           HBITMAP Msk;

        public :
           SEARCHBEST ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
           }

           ~SEARCHBEST ()
           {
                   if (Query)
                   {
                       delete Query;
                       Query = NULL;
                   }
           }

/*
           SWG *GetLine (void)
           {
               if (idx == -1) return NULL;
               return &sline;
           }
*/

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           static int SearchLst (char *);
           static int ReadLst (char *);
           BOOL GetKey (char *);
           BOOL GetKey (char *, int);
           char *GetKey (void);
           void SetParams (HINSTANCE, HWND);
           void Search (void);
           void InsertRecord (char *);
           void Search (char *, char *, char *, char *,
                         int (*)(char *), char *);
};  
#endif