#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "choisedlg.h" 

static int StdSize = STDSIZE;
static int InfoSize = 150;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


mfont ChoiseDlg::dlgfont = {
                         "ARIAL", 100, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};

mfont ChoiseDlg::dlgposfont = {
                         "ARIAL", 100, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};



mfont *ChoiseDlg::Font = &dlgposfont;


ChoiseDlg::ChoiseDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

ChoiseDlg::~ChoiseDlg ()
{
         ActiveDlg = OldDlg;
}


void ChoiseDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
//             int i;
//             int xfull, yfull;

	         Exclusive = FALSE;
             ActiveDlg = this;
}

/*
BOOL ChoiseDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (hWnd == this->hWnd)
        {
             if (NextWindow != NULL)
             {
                     EnableWindows (NextWindow, TRUE);
                     SetWindowPos (NextWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
             }
             PostQuitMessage (0);
			 fWork->destroy ();
             return TRUE;
		}
		return FALSE;
}
*/

BOOL ChoiseDlg::OnKeyReturn (void)
{
       syskey = KEY12;
       fWork->GetText ();
       DestroyWindow ();
       return TRUE;
}

BOOL ChoiseDlg::OnKey5 ()
{
       syskey = KEY5;
       fWork->GetText ();
       DestroyWindow ();
       return TRUE;
}

BOOL ChoiseDlg::OnKey12 ()
{
       syskey = KEY12;
       fWork->GetText ();
       DestroyWindow ();
       return TRUE;
}

BOOL ChoiseDlg::TestCheck (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
		 CFIELD *Cfield;
		 
	     if (HIWORD (wParam) != BN_CLICKED) return FALSE;

		 Cfield = fWork->GetCfield (LOWORD (wParam));
//		 if ((Cfield->GetBkMode () & BS_AUTOCHECKBOX) == FALSE)
		 if (strcmp (Cfield->GetName (), "cancel") != NULL)
		 {
			 strcpy ((char *) Cfield->GetCheckboxFeld (), "J");
			 return OnKey12 ();
		 }
         OnKey5 ();
		 return TRUE;
}


BOOL ChoiseDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
         
        if (fWork != NULL)
        {
			if (TestCheck (hWnd, msg, wParam, lParam)) return TRUE;
//LuD			Parent->OnCommand (hWnd, msg, wParam, lParam);
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL ChoiseDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
			OnKey5 ();
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}


void ChoiseDlg::SetWinBackground (COLORREF Col)
{
          DLG::SetWinBackground (Col);
}

void ChoiseDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void ChoiseDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

