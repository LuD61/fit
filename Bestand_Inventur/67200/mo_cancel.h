#ifndef _MO_CH_DEF
#define _MO_CH_DEF
#include "windows.h"
#include "image.h"
#include "v_key.h"

#define MAXIMAGE 50

class CANCEL
{
          private :
			  HWND hMainWindow;
			  HINSTANCE hInstance;
			  HWND hWnd;
              mfont *Font;
			  V_KEY Vkeys[20];
			  int vkidx;
			  DWORD currentfield;
			  static int (*CaFunc) (int); 
              static int (*AbrProc) (int);
              int cx, cy;
			  IMG *Images [MAXIMAGE];
			  int imgidx;
          public :
			  CANCEL (int, int, char *, char *);
			  ~CANCEL ();
			  void AddImage (HBITMAP, HBITMAP);
 		      void ProcessMessages (void);
              static void ShowDlg (HDC, form *);
              static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
			  static void SetCaFunc (int (*) (int));
			  static void SetAbrProc (int (*) (int));
              static void MoveWindow (void);
              void AddAccelerator (int, int, int (*) (void));
              void OpenWindow (HANDLE, HWND);
              void DestroyWindow (void);
			  void SetCurrentID (DWORD);
			  void SetCurrentName (char *);
			  void EnableID (DWORD, BOOL);
			  void EnableName (char *, BOOL);
			  void CheckID (DWORD, BOOL);
			  void CheckName (char *, BOOL);
              BOOL TestButtons (HWND);
              BOOL IsVKey (int);
              BOOL TestVKeys (int);
			  void Enable (BOOL);
};

class CA_CLASS
{
    private :
	     CANCEL *Ca;
	     int (*CaFunc) (int); 
	     int (*AbrProc) (int); 
    public :
	     CA_CLASS ()
		 {
			 Ca = NULL;
			 CaFunc = NULL;
			 AbrProc = NULL;
		 }
		 ~CA_CLASS ()
		 {
             Destroy ();
		 }
         void SetCaFunc (int (*) (int));
         void SetAbrProc (int (*) (int));
		 BOOL Execute (HWND, int , int, char *, char *);
		 void Destroy ();
	     void Enable (BOOL);
};                 
#endif

