#include <windows.h>
#include "searcha.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


struct SA *SEARCHA::satab = NULL;
struct SA SEARCHA::sa;
int SEARCHA::idx = -1;
long SEARCHA::aanz;
CHQEX *SEARCHA::Query = NULL;
DB_CLASS SEARCHA::DbClass; 
HINSTANCE SEARCHA::hMainInst;
HWND SEARCHA::hMainWindow;
HWND SEARCHA::awin;
int SEARCHA::SearchField = 1;
int SEARCHA::soa = 1; 
int SEARCHA::soa_bz1 = 1; 
int SEARCHA::soa_bz2 = 1; 

int SEARCHA::sorta (const void *elem1, const void *elem2)
{
	      struct SA *el1; 
	      struct SA *el2; 

	      el1 = (struct SA *) elem1;
		  el2 = (struct SA *) elem2;
          if (ratod (el1->a) > ratod (el2->a))
          {
              return 1;
          }
          if (ratod (el1->a) < ratod (el2->a))
          {
              return -1;
          }
	      return 0;
}

void SEARCHA::SortA (HWND hWnd)
{
   	   qsort (satab, aanz, sizeof (struct SA),
				   sorta);
       soa *= -1;
}

int SEARCHA::sorta_bz1 (const void *elem1, const void *elem2)
{
	      struct SA *el1; 
	      struct SA *el2; 

		  el1 = (struct SA *) elem1;
		  el2 = (struct SA *) elem2;
          clipped (el1->a_bz1);
          clipped (el2->a_bz1);
          if (strlen (el1->a_bz1) == 0 &&
              strlen (el2->a_bz1) == 0)
          {
              return 0;
          }
          if (strlen (el1->a_bz1) == 0)
          {
              return -1;
          }
          if (strlen (el2->a_bz1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->a_bz1,el2->a_bz1) * soa_bz1);
}

void SEARCHA::SortA_bz1 (HWND hWnd)
{
   	   qsort (satab, aanz, sizeof (struct SA),
				   sorta_bz1);
       soa_bz1 *= -1;
}

int SEARCHA::sorta_bz2 (const void *elem1, const void *elem2)
{
	      struct SA *el1; 
	      struct SA *el2; 

		  el1 = (struct SA *) elem1;
		  el2 = (struct SA *) elem2;
          clipped (el1->a_bz2);
          clipped (el2->a_bz2);
          if (strlen (el1->a_bz2) == 0 &&
              strlen (el2->a_bz2) == 0)
          {
              return 0;
          }
          if (strlen (el1->a_bz2) == 0)
          {
              return -1;
          }
          if (strlen (el2->a_bz2) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->a_bz2,el2->a_bz2) * soa_bz2);
}

void SEARCHA::SortA_bz2 (HWND hWnd)
{
   	   qsort (satab, aanz, sizeof (struct SA),
				   sorta_bz2);
       soa_bz2 *= -1;
}

void SEARCHA::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortA (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortA_bz1 (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortA_bz2 (hWnd);
                  SearchField = 2;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHA::FillFormat (char *buffer)
{
         sprintf (buffer, "%s %s %s",
                          "%d", 
                          "%s",
                          "%s");
}


void SEARCHA::FillCaption (char *buffer)
{
	    sprintf (buffer, " %13s  %-24s  %-24s", "Artikel", 
                          "Bezeichnung1", "Bezeichnung2" ); 
}

void SEARCHA::FillVlines (char *buffer) 
{
	    sprintf (buffer, " %15s %25s %25s", 
                         "1", "1", "1"); 
}


void SEARCHA::FillRec (char *buffer, int i)
{
 	      sprintf (buffer, " %13.0lf  |%-24s|  |%-24s|", 
                            ratod (satab[i].a), 
                            satab[i].a_bz1,
                            satab[i].a_bz2);
}


void SEARCHA::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < aanz; i ++)
       {
          FillRec (buffer, i);
	      Query->UpdateRecord (buffer, i);
       }
}


int SEARCHA::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (satab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < aanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
                   strcpy (sabuff, satab[i].a);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (ratod (sebuff) == ratod (sabuff)) break;
           }
           else if (SearchField == 1)
           {
		           if (strupcmp (sebuff, satab[i].a_bz1, len) == 0) break;
           }
           else if (SearchField == 2)
           {
		           if (strupcmp (sebuff, satab[i].a_bz2, len) == 0) break;
           }
	   }
	   if (i == aanz) return 0;
	   Query->SetSel (i);
	   return 0;
}

int SEARCHA::ReadA (char *a_bz1)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (satab) 
	  {
           delete satab;
           satab = NULL;
	  }


      idx = -1;
	  aanz = 0;
      SearchField = 1;
      Query->SetSBuff ("");
      clipped (a_bz1);
      if (strcmp (a_bz1, " ") == 0) a_bz1[0] = 0;

      sprintf (buffer, "select count (*) from a_bas where a_bz1 matches \"%s*\"", a_bz1);
      DbClass.sqlout ((int *) &aanz, 2, 0);
	  DbClass.sqlcomm (buffer);

      if (aanz == 0) aanz = 0x10000;

	  satab = new struct SA [aanz];
      clipped (a_bz1);
      if (strlen (a_bz1) == 0 ||
          a_bz1[0] <= ' ')
      {
	          sprintf (buffer, "select a, a_bz1, a_bz2 "
	 			       "from a_bas "
                       "order by a");
      }
      else
      {
	          sprintf (buffer, "select a, a_bz1, a_bz2 "
		               "hnd_gew, pr_vk, me_einh, tara "
	 			       "from a_bas "
                       "where a_bz1 = %s", a_bz1);
      }
      DbClass.sqlout ((char *) sa.a, 0, 14);
      DbClass.sqlout ((char *) sa.a_bz1, 0, 25);
      DbClass.sqlout ((char *) sa.a_bz2, 0, 25);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		  memcpy (&satab[i], &sa, sizeof (struct SA));
          FillRec (buffer, i);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      aanz = i;
	  DbClass.sqlclose (cursor);
      soa_bz1 = 1;
      SortA_bz1 (Query->GethWnd ());
      UpdateList ();
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHA::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < aanz; i ++)
      {
          FillRec (buffer, i);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHA::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHA::SearchA (void)
{
  	  int cx, cy;
	  char buffer [256];
      RECT rect;

      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
      SetSortProc (SortLst);
      EnableWindow (hMainWindow, FALSE);
      if (Query == NULL)
      {
        idx = -1;
        Query = new CHQEX (cx, cy, "Name", "");
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (ReadA);
	    Query->SetSearchLst (SearchLst);
      }
      else
      {
        Query->RestorefWork ();
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (ReadA);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      idx = Query->GetSel ();
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (satab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&sa, &satab[idx], sizeof (sa));
      SetSortProc (NULL);
      if (satab != NULL)
      {
          Query->SavefWork ();
      }
}


SA *SEARCHA::GetNextSa (void)
{
      if (idx == -1) return NULL;
      if (idx < aanz - 1) 
      {
             idx ++;
      }
      memcpy (&sa, &satab[idx], sizeof (sa));
      return &sa;
}


SA *SEARCHA::GetPriorSa (void)
{
      if (idx == -1) return NULL;
      if (idx > 0) 
      {
             idx --;
      }
      memcpy (&sa, &satab[idx], sizeof (sa));
      return &sa;
}

SA *SEARCHA::GetFirstSa (void)
{
      if (idx == -1) return NULL;
      idx = 0;
      memcpy (&sa, &satab[idx], sizeof (sa));
      return &sa;
}

SA *SEARCHA::GetLastSa (void)
{
      if (idx == -1) return NULL;
      idx = aanz - 1;
      memcpy (&sa, &satab[idx], sizeof (sa));
      return &sa;
}



