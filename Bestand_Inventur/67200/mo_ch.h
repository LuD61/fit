#ifndef _MO_CH_DEF
#define _MO_CH_DEF
#include "v_key.h"
#include "image.h"

#define MAXIMAGE 50

class CH
{
          private :
			  HWND hMainWindow;
			  HINSTANCE hInstance;
			  HWND hWnd;
			  int listpos;
              mfont *Font;
			  V_KEY Vkeys[20];
			  int vkidx;
			  DWORD currentfield;
			  static int (*CaFunc) (int); 
			  static int (*OkFunc) (int); 
			  static int (*DialFunc) (int); 
			  static int (*SpezFunc) (int); 
              int cx, cy;
			  IMG *Images [MAXIMAGE];
			  int imgidx;
          public :
			  CH (int, int, char *, char *, char *);
			  CH (int, int, char *, char *, char *, char *);
			  ~CH ();
			  int GetListpos (void)
			  {
				  return listpos;
			  }
			  void AddImage (HBITMAP, HBITMAP);
 		      void ProcessMessages (void);
              static void ShowDlg (HDC, form *);
              static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
			  static void SetCaFunc (int (*) (int));
			  static void SetOkFunc (int (*) (int));
			  static void SetDialFunc (int (*) (int));
			  static void SetSpezFunc (int (*) (int));
              static int  GetSel (void);
              static void MoveWindow (void);
              void AddAccelerator (int, int, int (*) (void));
              void OpenWindow (HANDLE, HWND);
              void DestroyWindow (void);
			  void SetCurrentID (DWORD);
			  void SetCurrentName (char *);
			  void EnableID (DWORD, BOOL);
			  void EnableName (char *, BOOL);
			  void CheckID (DWORD, BOOL);
			  void CheckName (char *, BOOL);
              void VLines (char *, int);
			  void InsertCaption (char *);
			  void InsertRecord  (char *);
			  void UpdateRecord  (char *, int);
              void GetText (char *);
              BOOL TestButtons (HWND);
              BOOL IsVKey (int);
              BOOL TestVKeys (int);
};
#endif

