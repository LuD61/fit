#pragma once
#include "afxwin.h"


// CKunWahl-Dialogfeld

class CKunWahl : public CDialog
{
	DECLARE_DYNAMIC(CKunWahl)

public:
	CKunWahl(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CKunWahl();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_kunlist;
public:
	CString v_kunlist;
public:
	afx_msg void OnLbnSelchangeKunlist();
public:
	afx_msg void OnLbnKillfocusKunlist();
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedCancel();
};
