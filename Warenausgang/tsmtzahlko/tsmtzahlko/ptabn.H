#ifndef _PTABN_DEF
#define _PTABN_DEF

struct PTABN {

char ptwert[4];
char ptitem[19];
long ptlfnr ;
char ptbez[33];
char ptbezk[9];
char ptwer1[9];
char ptwer2[9];
};

extern struct PTABN ptabn, ptabn_null;

extern class PTABN_CLASS ptabn_class;

class PTABN_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
//			   PTABN ptabn ;
               int dbcount (void);
               int leseptabn (void);
               int openptabn (void);
               int leseallptabn (void);
               int openallptabn (char *);
               PTABN_CLASS () : DB_CLASS ()
               {
               }
};

#endif

