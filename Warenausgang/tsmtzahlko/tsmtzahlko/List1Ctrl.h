#pragma once
#include <vector>
#include "editlistctrl.h"
#include "dbClass.h"
//#include "a_bas.h"
// #include "ChoiceMeEinh.h"
#include "ChoiceTsmt.h"

#define MAXLISTROWS 30

class CList1Ctrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

/* --->
	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
	};
< ---- */
	enum LISTPOS
	{
		POSTSMT			= 1,
		POSTXT   		= 2,
		POSZIEL1		= 3,
		POSSKTO1		= 4,
		POSZIEL2		= 5,
		POSSKTO2		= 6,
		POSZIEL3		= 7,
		POSSKTO3		= 8,
		POSZAHL_KOND	= 9,	
/* --->
		POSARTNR	= 1,
		POSARTBEZ	= 2,
		POSKANTE	= 3,
		POSA_KUN	= 4,
		POSA_BZ1	= 5,
		POSA_BZ2	= 6,
		POSKUN_ME_EINH = 7,
		POSKUN_INH	= 8,
		POSGEB_FAKT	= 9, 
		POSEAN      = 10,
		POSEAN_VK	= 11,
		POSLI_A		= 12,
< ---- */
	};


	int PosTsmt;
    int PosTxt;
	int PosZiel1;
	int PosSkto1;
	int PosZiel2;
	int PosSkto2;
	int PosZiel3;
	int PosSkto3;
	int PosZahl_kond;
	/* ->
	int PosArtNr;
    int PosArtBez;
	int PosKante;
	int PosA_kun;
	int PosA_bz1;
	int PosA_bz2;
	int PosKun_me_einh;
	int PosKun_inh;
	int PosGeb_fakt;
	int PosEan;
	int PosEan_VK;
	int PosLi_a;
    int *Position[15];
< --- */
    int *Position[12];


//	double dikaltgew ;
//	double dizuabkg ;
//	long  lianzahl ;
//	long lischlklknr ;

// void SetWerte ( long , double, long ) ;

// void Zuschlagsetzen ( void ) ;

//	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	BOOL ActiveListRow;
	short m_mdn;
	short mdn;
//    long schlklknr;

//	int oldsel;
	std::vector<BOOL> vSelect;
//	CVector MeEinhCombo ;
	CVector TsmtCombo ;
//	CVector PrGrStufCombo;
//	CVector KunPrCombo;
//	CChoiceKun *ChoiceKun;
//	BOOL ModalChoiceKun;
//	BOOL KunChoiceStat;
//	CChoiceIKunPr *ChoiceIKunPr;
//	BOOL ModalChoiceIKunPr;
//	BOOL IKunPrChoiceStat;
//	CChoiceMeEinh *ChoiceMeEinh;
//	BOOL ModalChoiceMeEinh;
//	BOOL MeEinhChoiceStat;

	CChoiceTsmt *ChoiceTsmt;
	BOOL ModalChoiceTsmt;
	BOOL TsmtChoiceStat;

	CVector ListRows;

	CList1Ctrl(void);
	~CList1Ctrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
//	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
//	void FillMeEinhCombo( CVector&);
	void FillTsmtCombo( CVector&);

	void OnChoice ();
//	void OnKunChoice (CString &);
//    void OnIKunPrChoice (CString& Search);
//    void OnPrGrStufChoice (CString& Search);
//	void OnKey9 ();
//    BOOL ReadArtikel ();
	BOOL ReadTsmt ();
//    void ReadKunPr ();
//    void ReadPrGrStuf ();
    void GetColValue (int row, int col, CString& Text);
//    void TestIprIndex ();
	void ScrollPositions (int pos);
	BOOL LastCol ();
};
