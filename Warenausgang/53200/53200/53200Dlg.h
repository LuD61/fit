#pragma once
#include "afxwin.h"


extern char globnutzer[ ] ;
// C53200Dlg-Formularansicht

class C53200Dlg : public CFormView
{
	DECLARE_DYNCREATE(C53200Dlg)

protected:
	C53200Dlg();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~C53200Dlg();

public:
	enum { IDD = IDD_53200DLG };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void OnInitialUpdate();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_mandant;
public:
	CString v_mandant;
public:
	CEdit m_mdnname;
public:
	CString v_mdnname;
public:
	CButton c_kunbutt;
public:
	CButton c_filbutt;
public:
	BOOL v_kunbutt;
public:
	BOOL v_filbutt;
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedCheckkun();
public:
	afx_msg void OnBnClickedCheckfil();
public:
	afx_msg void OnEnKillfocusMandant();
public:
	afx_msg void OnEnKillfocusDatum();
public:
	afx_msg void OnEnKillfocusStapnu();
public:
	afx_msg void OnBnClickedCancel();
public:
//	afx_msg void OnBnClickedAusdruck();
 	afx_msg BOOL PreTranslateMessage(LPMSG) ;
public:
	void ReadMdn(void) ;
	void StapelDruck(void) ;
	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);

public:
	CEdit m_datum;
public:
	CString v_datum;
public:
	CEdit m_stapnu;
public:
	CString v_stapnu;
	CButton m_nadru;
	BOOL v_nadru;
	afx_msg void OnBnClickednadru();
	CEdit m_bisdatum;
	CString v_bisdatum;
	CEdit m_kunde;
	long v_kunde;
	CEdit m_filiale;
	short v_filiale;
	afx_msg void OnEnKillfocusBisdatum();
	afx_msg void OnEnChangeVlsnr();
	CButton m_erstdr;
	BOOL v_erstdr;
	afx_msg void OnBnClickedErstdr();
	CEdit m_bstapnu;
	CString v_bstapnu;
	CEdit m_kunvon;
	CEdit m_kunbis;
	CString v_kunvon;
	CString v_kunbis;
	CEdit m_vlsnr;
	CString v_vlsnr;
	CEdit m_blsnr;
	CString v_blsnr;
	afx_msg void OnEnKillfocusVlsnr();
	afx_msg void OnEnKillfocusBlsnr();
};


