//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 53200.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_53200TYPE                   129
#define IDD_53200DLG                    130
#define IDD_DIALOG1                     131
#define IDD_DRUCKDIALOG                 131
#define IDC_AUSDRUCK                    1000
#define IDC_MANDANT                     1001
#define IDC_MDNNAME                     1002
#define IDC_CHECKKUN                    1003
#define IDC_CHECKFIL                    1004
#define IDC_DATUM                       1005
#define IDC_EDIT4                       1006
#define IDC_STAPNU                      1006
#define IDC_CHECK1                      1007
#define IDC_EINZEL                      1007
#define IDC_CHECK2                      1008
#define IDC_NADRU                       1008
#define IDC_EDIT1                       1009
#define IDC_BISDATUM                    1009
#define IDC_KUNDE                       1010
#define IDC_EDIT3                       1011
#define IDC_FILIALE                     1011
#define IDC_VLSNR                       1012
#define IDC_BLSNR                       1013
#define IDC_BSTAPNU                     1014
#define IDC_ERSTDR                      1015
#define IDC_KUNVON                      1016
#define IDC_EDIT5                       1017
#define IDC_KUNBIS                      1017
#define ID_DATEI_RECHNUNGSDRUCKER       32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
