// 53200Doc.h : Schnittstelle der Klasse C53200Doc
//


#pragma once


class C53200Doc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	C53200Doc();
	DECLARE_DYNCREATE(C53200Doc)

// Attribute
public:

// Vorg�nge
public:

// �berschreibungen
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~C53200Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


