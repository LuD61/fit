// 53200View.cpp : Implementierung der Klasse C53200View
//

#include "stdafx.h"
#include "53200.h"

#include "53200Doc.h"
#include "53200View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// C53200View

IMPLEMENT_DYNCREATE(C53200View, CView)

BEGIN_MESSAGE_MAP(C53200View, CView)
	// Standarddruckbefehle
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// C53200View-Erstellung/Zerst�rung

C53200View::C53200View()
{
	// TODO: Hier Code zur Konstruktion einf�gen

}

C53200View::~C53200View()
{
}

BOOL C53200View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CView::PreCreateWindow(cs);
}

// C53200View-Zeichnung

void C53200View::OnDraw(CDC* /*pDC*/)
{
	C53200Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: Code zum Zeichnen der systemeigenen Daten hinzuf�gen
		char titelstring[30];
		sprintf ( titelstring ,"53200-Stapeldruck") ;
		pDoc->SetTitle(titelstring);
}


// C53200View drucken

BOOL C53200View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Standardvorbereitung
	return DoPreparePrinting(pInfo);
}

void C53200View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Zus�tzliche Initialisierung vor dem Drucken hier einf�gen
}

void C53200View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Bereinigung nach dem Drucken einf�gen
}


// C53200View-Diagnose

#ifdef _DEBUG
void C53200View::AssertValid() const
{
	CView::AssertValid();
}

void C53200View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

C53200Doc* C53200View::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(C53200Doc)));
	return (C53200Doc*)m_pDocument;
}
#endif //_DEBUG


// C53200View-Meldungshandler
