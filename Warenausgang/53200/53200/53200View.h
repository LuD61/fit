// 53200View.h : Schnittstelle der Klasse C53200View
//


#pragma once


class C53200View : public CView
{
protected: // Nur aus Serialisierung erstellen
	C53200View();
	DECLARE_DYNCREATE(C53200View)

// Attribute
public:
	C53200Doc* GetDocument() const;

// Vorgänge
public:

// Überschreibungen
public:
	virtual void OnDraw(CDC* pDC);  // Überschrieben, um diese Ansicht darzustellen
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementierung
public:
	virtual ~C53200View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // Debugversion in 53200View.cpp
inline C53200Doc* C53200View::GetDocument() const
   { return reinterpret_cast<C53200Doc*>(m_pDocument); }
#endif

