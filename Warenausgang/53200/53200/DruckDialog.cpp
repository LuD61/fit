// DruckDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "53200.h"
#include "DruckDialog.h"

extern 	DWORD ProcWaitExec( LPSTR , WORD , int, int, int, int );
extern char * globnutzer ;
extern char * bws_default(char *) ;

// CDruckDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CDruckDialog, CDialog)

CDruckDialog::CDruckDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CDruckDialog::IDD, pParent)
{

}

CDruckDialog::~CDruckDialog()
{
}

void CDruckDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDruckDialog, CDialog)
END_MESSAGE_MAP()


// CDruckDialog-Meldungshandler
void CDruckDialog::DruckWahl(void)
{
	// 261109 : lllsfo korrekt und alles neu 
	char s1[199] ;
//	char s2[199] ;

	char *p = bws_default( "lllsfo" ) ;
    int lllsfo = 1 ;

	if ( p != NULL )
	lllsfo = atoi (p) ;
	if ( lllsfo < 1 ) lllsfo = 1 ;
	if ( lllsfo > 4 ) lllsfo = 4 ;

	sprintf ( s1, "drllls -WAHL" );
	switch ( lllsfo )
	{
	  case 1 :
		  break ;
	  case 2 :
		  sprintf ( s1, "drllls -WAHL2" );
		  break ;
	  case 3 :
		  sprintf ( s1, "drllls -WAHL3" );
		  break ;
	  case 4 :
		  sprintf ( s1, "drllls -WAHL4" );
		  break ;
	}
	int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );

// 261109	sprintf ( s1, "%s\\format\\ll\\53100.lsp", getenv("BWS") ) ;
// 261109   sprintf ( s2, "%s\\format\\ll\\53101.lsp", getenv("BWS") ) ;
// 261109 	CopyFile ( s1, s2, FALSE ) ;	// immer ueberschreiben 
	;
}


void CDruckDialog::RDruckWahl(void)
{
	char s1[199] ;
//	char s2[199] ;

	if ((strlen ( globnutzer)) > 1 )
	{
		sprintf ( s1, "dr561.exe -WAHL 1 -FORMTYP 5610000 -NUTZER %s" , globnutzer);
		int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );


// 261109 : neues dr561 brauxcht das nicht mehr sprintf ( s1, "dr561.exe -WAHL 2 -FORMTYP 5610001 -NUTZER %S" , globnutzer);
// 261109  		    ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
	}
	else
	{
		sprintf ( s1, "dr561.exe -WAHL 1 -FORMTYP 5610000" );
		int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );

// 261109		sprintf ( s1, "dr561.exe -WAHL 2 -FORMTYP 5610001" );
// 261109			ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );
	}

//	sprintf ( s1, "%s\\format\\ll\\561000.lsp", getenv("BWS") ) ;
//    sprintf ( s2, "%s\\format\\ll\\551001.lsp", getenv("BWS") ) ;
//	CopyFile ( s1, s2, FALSE ) ;	// immer ueberschreiben 
	;
}

