#pragma once


// CDruckDialog-Dialogfeld

class CDruckDialog : public CDialog
{
	DECLARE_DYNAMIC(CDruckDialog)

public:
	CDruckDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDruckDialog();

// Dialogfelddaten
	enum { IDD = IDD_DRUCKDIALOG };

public:
	virtual void DruckWahl(void ) ;
	virtual void RDruckWahl(void ) ;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
};



