// 53200Doc.cpp : Implementierung der Klasse C53200Doc
//

#include "stdafx.h"
#include "53200.h"

#include "53200Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// C53200Doc

IMPLEMENT_DYNCREATE(C53200Doc, CDocument)

BEGIN_MESSAGE_MAP(C53200Doc, CDocument)
END_MESSAGE_MAP()

// C53200Doc-Erstellung/Zerst�rung

C53200Doc::C53200Doc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

C53200Doc::~C53200Doc()
{
}

BOOL C53200Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// C53200Doc-Serialisierung

void C53200Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// C53200Doc-Diagnose

#ifdef _DEBUG
void C53200Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void C53200Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// C53200Doc-Befehle
