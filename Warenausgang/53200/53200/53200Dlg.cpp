// 53200Dlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "53200.h"
#include "53200Dlg.h"
#include "DbClass.h"
#include "mdn.h"
#include "adr.h"
#include "kun.h"
#include "ls.h"
#include "sys_par.h"


DB_CLASS dbClass ;	// Hier ist die Heimat der dbClass, alle Tabellen sind in ihrem jeweiligen Modul daheim
extern MDN_CLASS mdn_class ;
extern ADR_CLASS adr_class ;
extern LSK_CLASS lsk_class ;
extern LSP_CLASS lsp_class ;
extern KUN_CLASS kun_class ;
extern FIL_CLASS fil_class ;
extern SYS_PAR_CLASS sys_par_class ;
extern KUNBRTXTZU_CLASS kunbrtxtzu_class ;

char globnutzer [256] ;

char bufh[256] ;

#define IMAXDIM 1500 

int stapnu ;

int dnachkpreis ;

long schlussmat[IMAXDIM] ;		// Array LS-Nummern
long schluss2mat[IMAXDIM] ;		// Array Kunden-Nummern zum LS
short schluss3mat[IMAXDIM] ;	// Array lsk.fil
char schluss4mat[IMAXDIM] ;		// Kommando fuer beldr

int eigmat[IMAXDIM] ;		// Array LS-Status-Infos

// Status : == 0	// da war was, aber bitte nichts mehr machen 
// Status : == 3	// es war komplett 
// Status : == 4	// es war gedruckt
// Status : == -1	// leer 
// Status : == -2	// es gibt probleme , am besten ignorieren


char * SYSTEMDATUM ( TIMESTAMP_STRUCT * idat ) ;


// 53200Dlg

IMPLEMENT_DYNCREATE(C53200Dlg, CFormView)

C53200Dlg::C53200Dlg()
	: CFormView(C53200Dlg::IDD)
	, v_mandant(_T(""))
	, v_mdnname(_T(""))
	, v_kunbutt(FALSE)
	, v_filbutt(FALSE)
	, v_datum(_T(""))
	, v_stapnu(_T(""))
	, v_nadru(FALSE)
	, v_bisdatum(_T(""))
	, v_kunde(0)
	, v_filiale(0)
	, v_erstdr(TRUE)
	, v_bstapnu(_T(""))
	, v_kunvon(_T(""))
	, v_kunbis(_T(""))
	, v_vlsnr(_T(""))
	, v_blsnr(_T(""))
{

}

C53200Dlg::~C53200Dlg()
{
}

void C53200Dlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MANDANT, m_mandant);
	DDX_Text(pDX, IDC_MANDANT, v_mandant);
	DDV_MaxChars(pDX, v_mandant, 4);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_CHECKKUN, c_kunbutt);
	DDX_Control(pDX, IDC_CHECKFIL, c_filbutt);
	DDX_Check(pDX, IDC_CHECKKUN, v_kunbutt);
	DDX_Check(pDX, IDC_CHECKFIL, v_filbutt);
	DDX_Control(pDX, IDC_DATUM, m_datum);
	DDX_Text(pDX, IDC_DATUM, v_datum);
	DDX_Control(pDX, IDC_STAPNU, m_stapnu);
	DDX_Text(pDX, IDC_STAPNU, v_stapnu);
	DDV_MaxChars(pDX, v_stapnu, 8);
	DDX_Control(pDX, IDC_NADRU, m_nadru);
	DDX_Check(pDX, IDC_NADRU, v_nadru);
	DDX_Control(pDX, IDC_BISDATUM, m_bisdatum);
	DDX_Text(pDX, IDC_BISDATUM, v_bisdatum);
	DDX_Control(pDX, IDC_KUNDE, m_kunde);
	DDX_Text(pDX, IDC_KUNDE, v_kunde);
	DDV_MinMaxLong(pDX, v_kunde, 0, 99999999);
	DDX_Control(pDX, IDC_FILIALE, m_filiale);
	DDX_Text(pDX, IDC_FILIALE, v_filiale);
	DDV_MinMaxShort(pDX, v_filiale, 0, 9999);
	DDX_Control(pDX, IDC_ERSTDR, m_erstdr);
	DDX_Check(pDX, IDC_ERSTDR, v_erstdr);
	DDX_Control(pDX, IDC_BSTAPNU, m_bstapnu);
	DDX_Text(pDX, IDC_BSTAPNU, v_bstapnu);
	DDV_MaxChars(pDX, v_bstapnu, 8);
	DDX_Control(pDX, IDC_KUNVON, m_kunvon);
	DDX_Control(pDX, IDC_KUNBIS, m_kunbis);
	DDX_Text(pDX, IDC_KUNVON, v_kunvon);
	DDV_MaxChars(pDX, v_kunvon, 8);
	DDX_Text(pDX, IDC_KUNBIS, v_kunbis);
	DDV_MaxChars(pDX, v_kunbis, 8);
	DDX_Control(pDX, IDC_VLSNR, m_vlsnr);
	DDX_Text(pDX, IDC_VLSNR, v_vlsnr);
	DDV_MaxChars(pDX, v_vlsnr, 8);
	DDX_Control(pDX, IDC_BLSNR, m_blsnr);
	DDX_Text(pDX, IDC_BLSNR, v_blsnr);
}

BEGIN_MESSAGE_MAP(C53200Dlg, CFormView)
	ON_BN_CLICKED(IDOK, &C53200Dlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CHECKKUN, &C53200Dlg::OnBnClickedCheckkun)
	ON_BN_CLICKED(IDC_CHECKFIL, &C53200Dlg::OnBnClickedCheckfil)
	ON_EN_KILLFOCUS(IDC_MANDANT, &C53200Dlg::OnEnKillfocusMandant)
	ON_EN_KILLFOCUS(IDC_DATUM, &C53200Dlg::OnEnKillfocusDatum)
	ON_EN_KILLFOCUS(IDC_STAPNU, &C53200Dlg::OnEnKillfocusStapnu)
	ON_BN_CLICKED(IDCANCEL, &C53200Dlg::OnBnClickedCancel)
//	ON_BN_CLICKED(IDC_AUSDRUCK, &C3200Dlg::OnBnClickedAusdruck)
// ON_BN_CLICKED(IDC_EINZEL, &C53200Dlg::OnBnClickedEinzel)
ON_BN_CLICKED(IDC_NADRU, &C53200Dlg::OnBnClickednadru)
ON_EN_KILLFOCUS(IDC_BISDATUM, &C53200Dlg::OnEnKillfocusBisdatum)
ON_BN_CLICKED(IDC_ERSTDR, &C53200Dlg::OnBnClickedErstdr)
ON_EN_KILLFOCUS(IDC_VLSNR, &C53200Dlg::OnEnKillfocusVlsnr)
ON_EN_KILLFOCUS(IDC_BLSNR, &C53200Dlg::OnEnKillfocusBlsnr)
END_MESSAGE_MAP()


// 53200Dlg-Diagnose

#ifdef _DEBUG
void C53200Dlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void C53200Dlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// ALLES NUR WEGEN BWS_DEFA -ANFANG 

char *wort[25] ; // Bereich fuer Worte eines Strings
static char buffer[1000] ;
static char DefWert[256] ;

void cr_weg (char *string)

{
 for (; *string; string += 1)
 {
  if (*string == (char) 13)
    break;
  if (*string == (char) 10)
    break;
  if (*string == '\0')
    return ;
 }
 *string = 0;
 return;
}

int next_char_ci (char *string, char tzeichen, int i)
// Naechstes Zeichen != Trennzeichen suchen.

{
       for (;string [i]; i ++)
       {
               if (string[i] != tzeichen)
               {
                                   return (i);
               }
       }
       return (i);
}


short split (char *string)
{
 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
//  if (buffer == (char *) 0) buffer = (char *) malloc (0x1000);
	wz = 0;
	j = 0;
	len = (int)strlen (string);
	wz = 1;
	i = next_char_ci (string, zeichen, 0);
	if (i >= len) return (0);
	wort [wz] = buffer;
	wz ++;
	for (; i < len; i ++, j ++)
	{
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
	}
	buffer [j] = (char) 0;
	return (wz - 1);
}

int strupcmp (char *str1, char *str2, int len)
{
 short i;
 unsigned char upstr1;
 unsigned char upstr2;


	for (i = 0; i < len; i ++, str1 ++, str2 ++)
	{
		if (*str1 == 0)
			return (-1);
		if (*str2 == 0)
			return (1);

		upstr1 = (unsigned char) toupper((int) *str1);
		switch (upstr1)
		{
		case (unsigned char) '�' :
            upstr1 = '�';
			break;
		case (unsigned char) '�' :
            upstr1 = '�';
			break;
		case (unsigned char) '�' :
            upstr1 = '�';
			break;
		}

		upstr2 = (unsigned char) toupper((int) *str2);
		switch (upstr2)
		{
		case (unsigned char) '�' :
            upstr2 = '�';
			break;
		case (unsigned char) '�' :
            upstr2 = '�';
			break;
		case (unsigned char) '�' :
            upstr2 = '�';
			break;
		}
		if (upstr1 < upstr2)
		{
			return(-1);
		}
		else if (upstr1 > upstr2)
		{
			return (1);
		}
	}
	return (0);
}


char *bws_default (char *env)
/**
Wert aus Bws-default holen.
**/
{         
	char *etc;
    int anz;
    char buffer [512];
    FILE *fp;

    etc = getenv ("BWSETC");
    if (etc == (char *) 0)
    {
		etc = "C:\\USER\\FIT\\ETC";
    }
    sprintf (buffer, "%s\\bws_defa", etc);
	fp = fopen (buffer, "r");
    if (fp == NULL) return NULL;

     while (fgets (buffer, 511, fp))
     {
		cr_weg (buffer);
        anz = split (buffer);
        if (anz < 2) continue;
        if (strupcmp (wort[1], env, (int) strlen (env)) == 0)
        {
			strcpy (DefWert, wort [2]);
            fclose (fp);
			return (char *) DefWert;
        }
	}
    fclose (fp);
    return NULL;
}
char *fitgroup_def (char *env)
/**
Wert aus fitgroup.def holen.
**/
{         
	char *etc;
    int anz;
    char buffer [512];
    FILE *fp;

    etc = getenv ("BWSETC");
    if (etc == (char *) 0)
    {
		etc = "C:\\USER\\FIT\\ETC";
    }
    sprintf (buffer, "%s\\fitgroup.def", etc);
	fp = fopen (buffer, "r");
    if (fp == NULL) return NULL;

     while (fgets (buffer, 511, fp))
     {
		cr_weg (buffer);
        anz = split (buffer);
        if (anz < 2) continue;
        if (strupcmp (wort[1], env, (int) strlen (env)) == 0)
        {
			strcpy (DefWert, wort [2]);
            fclose (fp);
			return (char *) DefWert;
        }
	}
    fclose (fp);
    return NULL;
}

// ALLES NUR WEGEN BWS_DEFA - ENDE 

double ROUND ( double inwert , int prezi )
{

	if ( inwert == 0.0 ) return 0.0 ;
	double outwert ;
	double dmult ;
	double dfaktor ;
	double ddiv ;
	double ddummy ;

    dfaktor = 1 ;
	dmult = 1 ;

	for ( int i = 0 ; i < prezi ; i ++ ) dmult *= 10.0 ;

    ddiv = dmult * 2.0 ;
	dfaktor /= ddiv ;

	if ( inwert > 0.0 )
		ddummy = inwert + dfaktor ;
	else
		ddummy = inwert - dfaktor ;


	inwert = ( long )(ddummy * dmult ) ;
	outwert = inwert / dmult ;
	return  outwert ;
}

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	int k = 0 ;	// 261007 : Zusatzchecks
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		// 261007 : Zusatzchecks wegen vista, nur noch bedingtes return
		if ( i < 6 ) k = -1 ;
		if ( k == 0 )
		{
			// die ersten beiden Zeichen MUESSEN ZAHLEN sein
			if ( bufi[0] > '9' || bufi[0] < '0' ||bufi[1] > '9' || bufi[1] < '0' )
				k = - 1 ;
		}
		if ( k == 0 )
		{
			if ( bufi[2] > '9' || bufi[2] < '0' )
				k = 2 ;		// Typ 8 oder typ 10 moeglich

		}
		if ( k == 0 )
		{
			// nur noch Typ 6 erlaubt 
			if (    bufi[3] > '9' || bufi[3] < '0'
				 || bufi[4] > '9' || bufi[4] < '0'
				 || bufi[5] > '9' || bufi[5] < '0' )
			{
				k = - 1 ;
			}
			else
			{	// Gueltiger Typ 6  ......
				k = 1 ;
				i = 6 ;
				bufi[6] = '\0' ;
			}
		}
		if ( k == 2 )
		{
			if ( i < 8 )
			{	
				k = -1 ;
			}
			else
			{	// 4.,5.,7.,8. Zeichen MUESSEN ZAHLEN sein, 6.Zeichen MUSS Nicht-Zahl sein
				if (   bufi[3] > '9' || bufi[3] < '0' 
					 ||bufi[4] > '9' || bufi[4] < '0'
				     ||bufi[6] > '9' || bufi[6] < '0'
				     ||bufi[7] > '9' || bufi[7] < '0'
				     || !( bufi[5] >'9' || bufi[5]< '0')
				    )
				k = - 1 ;
			}
		}
		if ( k == 2 )
		{
			if ( bufi[8] > '9' || bufi[8] < '0' )
			{	// gueltiger Typ 8 
				k = 1 ;
				i = 8 ;
				bufi[8] = '\0' ;
			}
		
		}

		if ( k == 2 )
		{
			if ( i < 10 )
			{	
				k = -1 ;
			}
			else
			{	// 9.,10. Zeichen MUESSEN ZAHLEN sein
				if (   bufi[8] > '9' || bufi[8] < '0' 
					 ||bufi[9] > '9' || bufi[9] < '0'
			       )
				{
					k = -1 ;
				}
				else
				{
					k = 1 ;
					i = 10 ;
					bufi[10] = '\0' ;
				}
			}
		}
		if ( k < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}
		else
			j = TRUE ;

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}

// Input ist einerseits eine Datumsstrukur, die aktualisiert wird und ausserdem wird ein
// deutscher Datumsstring mit aktuellem Datum erzeugt

char systemdat[33] ;
char * SYSTEMDATUM ( TIMESTAMP_STRUCT * idat )
{
	time_t timer;
		struct tm *ltime;
		time (&timer);
 		ltime = localtime (&timer);

		idat->day = ltime->tm_mday ;
		idat->month = ltime->tm_mon + 1 ;
		idat->year = ltime->tm_year + 1900 ;
		idat->hour = 0 ;
		idat->minute = 0 ;
		idat->second = 0 ;
		idat->fraction = 0 ;

		sprintf ( systemdat , "%02d.%02d.%04d",
			idat->day, idat->month, idat->year );

	return systemdat ;
}


DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}

void C53200Dlg::ReadMdn (void) 
{
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//	gibbet nich		PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich 		PrevDlgCtrl();
	}
		UpdateData (FALSE) ;
}

void C53200Dlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.


	dbClass.opendbase (_T("bws"));

	v_filbutt = FALSE ;		// Kunde als std.
	v_kunbutt = TRUE ;
//	v_einzelbutt = TRUE ;	// 
	v_nadru = FALSE ;	// Erstdruck als std.
	v_erstdr = TRUE ;
	v_filiale = 0 ;		// nie aktiv 
	v_kunde = 0 ;		// nie aktiv
	
	m_kunde.ShowWindow(SW_HIDE);	// Einzelkunde immer tot
	m_filiale.ShowWindow(SW_HIDE);	// Einzelfil. immer tot
	m_vlsnr.ShowWindow(SW_HIDE);	// Erstdruck nach anderen Krit.
	m_blsnr.ShowWindow(SW_HIDE);	// Erstdruck nach anderen Krit.

	mdn.mdn = 1 ;
	v_mandant = "1" ;

//	toursetzen() ;

	v_stapnu.Format("0");
	v_bstapnu.Format("99999999");

	v_datum.Format("%s",SYSTEMDATUM(&lsk.lieferdat)) ; 
	v_bisdatum.Format("%s",SYSTEMDATUM(&lsk.lieferdat)) ; 

 	ReadMdn ();

	dnachkpreis = atoi ( sys_par_class.sys_par_holen ( "nachkpreis" ))  ;
	if ( dnachkpreis > 4 || dnachkpreis < 2 ) dnachkpreis = 2 ;

// Testausgabe : MessageBox(globnutzer, globnutzer, MB_OK|MB_ICONSTOP);


// return TRUE;  // so mag es oninitdialog sein .... Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten

}


// 53200Dlg-Meldungshandler

void C53200Dlg::OnBnClickedOk()
{
// Der Knopf ist mit "Ausdruck" beschriftet

	UpdateData (TRUE) ;
//	int i = m_stapnu.GetLine(0,bufh,500);
//	stapnu = 0 ;
//	if (i)	stapnu = atoi ( bufh );

	StapelDruck () ;

	UpdateData (FALSE) ;

//	GetParent ()->DestroyWindow ();
}

void C53200Dlg::OnBnClickedCheckkun()
{

	if ( v_erstdr == TRUE )
	{
		v_kunbutt = TRUE ;
		v_filbutt = FALSE ;
	}
	else
	{	// bei Nachdruck 
		if ( v_kunbutt == TRUE )
			v_kunbutt = FALSE ;
		else
			v_kunbutt = TRUE ;

	}
	UpdateData(FALSE);
}

void C53200Dlg::OnBnClickedCheckfil()
{
	if ( v_erstdr == TRUE )
	{
		v_filbutt = TRUE ;
		v_kunbutt = FALSE ;
	}
	else
	{	// bei Nachdruck 
		if ( v_filbutt == TRUE )
			v_filbutt = FALSE ;
		else
			v_filbutt = TRUE ;

	}

	UpdateData(FALSE);
}

void C53200Dlg::OnEnKillfocusMandant()
{

	UpdateData (TRUE) ;
	int i = m_mandant.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 250810 
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;
}

void C53200Dlg::OnEnKillfocusDatum()
{

	UpdateData(TRUE) ;
	int	i = m_datum.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;	// 250810 
	if (i)
	{
		if (datumcheck(bufh))
		{
			v_datum.Format("%s",_T(bufh));
		}
		else
			v_datum.Format("");

	}
//	Read () ;	// Saetze einlesen 
	UpdateData (FALSE) ;
	return ;

}

void C53200Dlg::OnEnKillfocusStapnu()
{
// einigermassen irrelevant
/* ----->
	UpdateData ( TRUE ) ;
	int i = m_stapnu.GetLine(0,bufh,500);
	if (i)	stapnu =  atoi ( bufh );
	else stapnu = 0 ;
	UpdateData ( FALSE ) ;
< ------- */

}

void C53200Dlg::OnBnClickedCancel()
{
	GetParent ()->DestroyWindow ();
}

void setdatum (  TIMESTAMP_STRUCT * ziel , CString quelle )
{
// Input zwingend "dd.mm.yyyy" oder blank

    char tag[] = {"01"};
    char mon[]  = {"01"};
    char jahr[]  = {"2000"};
   	int len = quelle.GetLength ();
	 if (len >=2)
	 {
		 memcpy (tag, quelle.Mid (0, 2), 2);
	 }
	 if (len >=5)
	 {
			 memcpy (mon, quelle.Mid (3, 2), 2);
	 }
	 if (len >=10)
	 {
		 memcpy (jahr, quelle.Mid (6, 4), 4);
	 }
	 else
	 {
		if (len >=10)
		{
			memcpy (jahr + 2 , quelle.Mid (6, 2), 2);
		}
	 }
	 ziel->year = atoi ( jahr ) ; if ( ziel->year > 2099 || ziel->year < 1900 ) ziel->year = 1999 ;
	 ziel->day = atoi ( tag ) ;   if ( ziel->day > 31 || ziel->day < 1 ) ziel->day = 1 ;
	 ziel->month = atoi ( mon ) ; if ( ziel->month > 12 || ziel->month < 1 ) ziel->month = 1 ;
	 ziel->hour = 0 ;
	 ziel->minute = 0 ;
	 ziel->second = 0 ;
	 ziel->fraction = 0 ;
}


void druckrech( short imdn, long ilsnr , int ityp )
{

char rosipath [200] ;
char kommando[299] ;
sprintf ( rosipath,"%s", getenv( "RSDIR"));
if ( ityp == 6 )	// Barverkaufskunde
	sprintf ( kommando, "%s\\bin\\rswrun beldr R %d 0 %d %s B" , rosipath, imdn, ilsnr , globnutzer ) ;
else
	sprintf ( kommando, "%s\\bin\\rswrun beldr R %d 0 %d %s R" , rosipath, imdn, ilsnr , globnutzer ) ;
	int ex_code = ProcWaitExec( (char *) kommando , SW_SHOW, -1, 0, -1, 0 );
}



// wird nur bei gefundenen Inhalten aufgerufen

void drucken (  short imdn, int ii )
{
  char s1[200] ;

  sprintf ( s1 , "%s\\53200ll.prm", getenv ( "TMPPATH" ) ) ;
  FILE * fp2 ;

	fp2 = fopen ( s1 , "w" ) ;
	if ( fp2 == NULL )
	{
		return  ;
	}


	for ( int j = 0 ;  j < ii ; j ++ )
	{
		if ( schluss4mat[j] == 'R' )
		{
			sprintf ( bufh , "R %d %d %d %s R\n" , imdn , schluss3mat[j],schlussmat[j],globnutzer ) ; 	
		}
		else
		{
			if ( schluss4mat[j] == 'B' )
			{
				sprintf ( bufh , "R %d %d %d %s B\n" , imdn , schluss3mat[j],schlussmat[j],globnutzer ) ; 	
			}
			else	// der grosse rest : normale LS 
			{
				sprintf ( bufh , "%c %d %d %d %s D\n" , schluss4mat[j] , imdn , schluss3mat[j],schlussmat[j],globnutzer ) ; 	
			}

		}

		fputs ( bufh , fp2 );
	}

	fclose ( fp2) ;

	sprintf ( bufh, "rswrun beldr S \"%s\" " , s1 );
	int ex_code = ProcWaitExec( (char *)bufh, SW_SHOW, -1, 0, -1, 0 );
}


void C53200Dlg::StapelDruck ()
{

	int garnixda ;			// 280409 : nix-da-Message handeln
	int inpoint = 0  ;			// Schreibpos in numliste
	int i ;

	lsk.mdn = mdn.mdn ;	// wurde bereits vorher verifiziert und nachgeladen 

// DATUM-RANGE

	setdatum ( & lsk.vondate , v_datum ) ;
 	setdatum ( & lsk.bisdate , v_bisdatum ) ;
	if ( lsk.bisdate.year == 2000 && lsk.bisdate.month == 1 && lsk.bisdate.day == 1 )
	{
		if ( lsk.vondate.year == 2000 && lsk.vondate.month == 1 && lsk.vondate.day == 1 )
			lsk.bisdate.year = 2088 ;	// von 01.01.2000 bis 01.01.2088
		else
			memcpy ( & lsk.vondate , & lsk.bisdate , sizeof ( TIMESTAMP_STRUCT )) ;
	}

// LS-Nummerrange

	i = m_vlsnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 250810 

	if (i)	lsk.von_ls = (long) atoi ( bufh );
	else lsk.von_ls = -22 ;

	i = m_blsnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 250810 

	if (i)	lsk.bis_ls = (long) atoi ( bufh );
	else lsk.bis_ls = -22 ;
	if ( lsk.bis_ls == -22 )
	{
		if ( lsk.von_ls == -22 )
		{
			lsk.von_ls = 1 ;
			lsk.bis_ls = 99999999 ;
		}
		else
			lsk.bis_ls = lsk.von_ls ;

	}
	else
	{
		if ( lsk.von_ls < 1 )
			lsk.von_ls = 1 ;
	}

// Kunden(Filial-)Range

	i = m_kunvon.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 250810 
	if (i)	lsk.von_kun = (long) atoi ( bufh );
	else lsk.von_kun = -22 ;

	i = m_kunbis.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 250810 
	if (i)	lsk.bis_kun = (long) atoi ( bufh );
	else lsk.bis_kun = -22 ;
	if ( lsk.bis_kun == -22 )
	{
		if ( lsk.von_kun == -22 )
		{
			lsk.von_kun = 1 ;
			lsk.bis_kun = 99999999 ;
		}
		else
			lsk.bis_kun = lsk.von_kun ;

	}
	else
	{
		if ( lsk.von_kun < 1 )
			lsk.von_kun = 1 ;
	}

// Tourenrange

	i = m_stapnu.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 250810 
	if (i)	lsk.von_tou = (long) atoi ( bufh );
	else lsk.von_tou = -22 ;

	i = m_bstapnu.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 250810 
	if (i)	lsk.bis_tou = (long) atoi ( bufh );
	else lsk.bis_tou = -22 ;
	if ( lsk.bis_tou == -22 )
	{
		if ( lsk.von_tou == -22 )
		{
			lsk.von_tou = 0 ;
			lsk.bis_tou = 99999999 ;
		}
		else
			lsk.bis_tou = lsk.von_tou ;

	}
	else
	{
		if ( lsk.von_tou < 0 )
			lsk.von_tou = 0 ;
	}

// Erst- oder nachdruck ( in 2 statements getrennt )

	if ( v_erstdr == TRUE )
	{
		lsk.von_stat = 3 ;
		lsk.bis_stat = 3 ;
	}
	else
	{
		lsk.von_stat = 4 ;
		lsk.bis_stat = 6 ;	
	}

// Kunden- oder Filial-Stapel 

	int ikunbutt = 0 ;
	int ifilbutt = 0 ;
	if (( v_kunbutt == FALSE && v_filbutt == FALSE )
		||( v_kunbutt == TRUE && v_filbutt == TRUE ))
	{
		lsk.vkufi = 0 ;
		lsk.bkufi = 1 ;
		ikunbutt = 1 ; 
		ifilbutt = 1 ;
	}
	else
	{
		if (( v_kunbutt == TRUE ) && v_filbutt == FALSE )
		{
			lsk.vkufi = 0 ;
			lsk.bkufi = 0 ;
			ikunbutt = 1 ; 
			ifilbutt = 0 ;
		};
		if (( v_kunbutt == FALSE ) && v_filbutt == TRUE )
		{
			lsk.vkufi = 1 ;
			lsk.bkufi = 1 ;
			ikunbutt = 0 ; 
			ifilbutt = 1 ;
		};
	}

//	 array initialisieren 
	for ( i = 0 ; i < IMAXDIM ; i++ )
	eigmat[i] = -1 ;

	int istatus ;
	garnixda = 0 ;
	i = 0 ;
	if ( ifilbutt == 1 )
	{
		if ( v_erstdr == TRUE )
		{
			istatus = lsk_class.openefbeding() ;
			while ( !istatus && i < IMAXDIM )
			{
				istatus = lsk_class.leseefbeding () ;
				if ( istatus ) break ;
				garnixda ++ ;

				schlussmat[i] = lsk.ls ;		// Array LS-Nummern
				schluss2mat[i]= lsk.kun ;		// Array Kunden-Nummern zum LS
				schluss3mat[i]= lsk.fil ;		// Array lsk.fil
				schluss4mat[i]= 'L' ;			// da immer filiale und immer status = komplett
												// gibt es nur Kommando "Drucken"
				i ++ ;
			}
		}
		if ( v_nadru == TRUE )
		{
			istatus = lsk_class.openfbeding() ;
			while ( !istatus && i < IMAXDIM )
			{
				istatus = lsk_class.lesefbeding () ;
				if ( istatus ) break ;
				garnixda ++ ;
				schlussmat[i] = lsk.ls ;		// Array LS-Nummern
				schluss2mat[i]= lsk.kun ;		// Array Kunden-Nummern zum LS
				schluss3mat[i]= lsk.fil ;		// Array lsk.fil
				schluss4mat[i]= 'L' ;
				if ( lsk.ls_stat > 4 )
					schluss4mat[i]= 'N' ;	// Nachdruck erzwingen
				i ++ ;
			}
		}
	}

	if ( ikunbutt == 1 )
	{
		if ( v_erstdr == TRUE )
		{
			istatus = lsk_class.openekbeding() ;
			while ( !istatus && i < IMAXDIM )
			{
				istatus = lsk_class.leseekbeding () ;
				if ( istatus ) break ;
				garnixda ++ ;

				schlussmat[i] = lsk.ls ;		// Array LS-Nummern
				schluss2mat[i]= lsk.kun ;		// Array Kunden-Nummern zum LS
				schluss3mat[i]= lsk.fil ;		// Array lsk.fil
				schluss4mat[i]= 'L' ;
				if ( kun.sam_rech == 1 )			// Sofortrechnungskunde
					schluss4mat[i]= 'R' ;
				if ( kun.kun_typ == 6 )
					schluss4mat[i] = 'B' ;		// Barverkaufskunde
				i ++ ;
			}
		}
		if ( v_nadru == TRUE )
		{
			istatus = lsk_class.openkbeding() ;
			while ( !istatus && i < IMAXDIM )
			{
				istatus = lsk_class.lesekbeding () ;
				if ( istatus ) break ;
// Nachdruck von Rechnungen aus dem Stapel nicht erlaubt,
// bitte 56100 verwenden !!!!
				if ( kun.sam_rech == 1 )			// Sofortrechnungskunde
					continue ;
				if ( kun.kun_typ == 6 )
					continue ;					// Barverkaufskunde

				garnixda ++ ;
				schlussmat[i] = lsk.ls ;		// Array LS-Nummern
				schluss2mat[i]= lsk.kun ;		// Array Kunden-Nummern zum LS
				schluss3mat[i]= lsk.fil ;		// Array lsk.fil
				schluss4mat[i]= 'L' ;
				if ( lsk.ls_stat > 4 )
				schluss4mat[i]= 'N' ;

				i ++ ;
			}
		}
	}

	if ( ! garnixda )
	{
		   MessageBox (_T("Datensatz nicht vorhanden"), NULL, MB_OK | MB_ICONERROR); 
		   return ;
	}

	drucken ( mdn.mdn , i ) ;


}


BOOL C53200Dlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				CWnd *xWnd ;
				xWnd = GetFocus ();
			           if(xWnd == GetDlgItem (IDOK)) break ;			// ok - eigentlich nichts machen
// auf den ok-Knopf verlegt ....			           if(xWnd == GetDlgItem (IDC_AUSDRUCK)) break ;	// Ausduck halt
			           if(xWnd == GetDlgItem (IDCANCEL))
					   {
							GetParent ()->DestroyWindow ();
							return TRUE ;	// Abbruch halt ( genau wie ButtonClickedCancel) 
					   }
	
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (OnKeyup ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
// ???				OnCancel () ;
				GetParent ()->DestroyWindow ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				return TRUE;
			}

	}

	return CFormView::PreTranslateMessage(pMsg);
}

BOOL C53200Dlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);

	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL C53200Dlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

/* --->
void C53200Dlg::OnBnClickedEinzel()
{
	if ( v_einzelbutt == FALSE )
	{
		v_einzelbutt = TRUE ;
	}
	else
		v_einzelbutt = FALSE ;


	UpdateData(FALSE);
}
< ---- */

void C53200Dlg::OnBnClickednadru()
{
	if ( v_nadru == FALSE )
	{
		v_erstdr = FALSE ;
		v_nadru = TRUE ;

		v_vlsnr.Format( _T(""));
		v_vlsnr.Format( _T(""));
		v_kunbutt = FALSE ;
		v_filbutt = FALSE ;
		v_stapnu.Format ( _T("")) ;
		v_bstapnu.Format(_T("")) ;
		v_datum.Format(_T("")) ;
		v_bisdatum.Format(_T("")) ;

		m_vlsnr.ShowWindow(SW_NORMAL);
		m_blsnr.ShowWindow(SW_NORMAL);
	}
	else
	{
		v_erstdr = TRUE ;
		v_nadru = FALSE ;

		if ( v_kunbutt == FALSE && v_filbutt == FALSE )
			v_kunbutt = TRUE ;

//	toursetzen() ;

//	v_stapnu.Format("0");
//	v_bstapnu.Format("99999999");

		v_datum.Format("%s",SYSTEMDATUM(&lsk.lieferdat)) ; 
		v_bisdatum.Format("%s",SYSTEMDATUM(&lsk.lieferdat)) ; 

		m_vlsnr.ShowWindow(SW_HIDE);
		m_blsnr.ShowWindow(SW_HIDE);


	}
	UpdateData(FALSE);
}

void C53200Dlg::OnEnKillfocusBisdatum()
{

	UpdateData(TRUE) ;
	int	i = m_bisdatum.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;	// 250810 

	if (i)
	{
		if (datumcheck(bufh))
		{
			v_bisdatum.Format("%s",_T(bufh));
		}
		else
			v_bisdatum.Format("");
	}
	UpdateData (FALSE) ;
	return ;
}

void C53200Dlg::OnBnClickedErstdr()
{
	if ( v_erstdr == FALSE )
	{
		v_erstdr = TRUE ;
		v_nadru = FALSE ;

		if ( v_kunbutt == FALSE && v_filbutt == FALSE )
			v_kunbutt = TRUE ;

		if ( v_kunbutt == FALSE && v_filbutt == FALSE )
			v_kunbutt = TRUE ;

//	toursetzen() ;

//	v_stapnu.Format("0");
//	v_bstapnu.Format("99999999");

		v_datum.Format("%s",SYSTEMDATUM(&lsk.lieferdat)) ; 
		v_bisdatum.Format("%s",SYSTEMDATUM(&lsk.lieferdat)) ; 

		m_vlsnr.ShowWindow(SW_HIDE);
		m_blsnr.ShowWindow(SW_HIDE);

	}
	else
	{
		v_erstdr = FALSE ;
		v_nadru = TRUE ;

		v_vlsnr.Format( _T(""));
		v_vlsnr.Format( _T(""));
		v_kunbutt = FALSE ;
		v_filbutt = FALSE ;
		v_stapnu.Format ( _T("")) ;
		v_bstapnu.Format(_T("")) ;
		v_datum.Format(_T("")) ;
		v_bisdatum.Format(_T("")) ;

		m_vlsnr.ShowWindow(SW_NORMAL);
		m_blsnr.ShowWindow(SW_NORMAL);

	}
	UpdateData(FALSE);
}

void C53200Dlg::OnEnKillfocusVlsnr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void C53200Dlg::OnEnKillfocusBlsnr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}
