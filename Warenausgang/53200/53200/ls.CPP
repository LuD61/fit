#include "StdAfx.h"

#include <windows.h>
#include "DbClass.h"

#include "ls.h"
#include "kun.h"	// beinhaltet u.a. a_bas, kun und fil 

struct LSK lsk,  lsk_null;
extern DB_CLASS dbClass;

LSK_CLASS lsk_class ;
LSP_CLASS lsp_class ;


int LSK_CLASS::openkbeding (void)
{

		if ( readabeding < 0 ) prepare ();
		
         return dbClass.sqlopen (readabeding);
}

int LSK_CLASS::lesekbeding (	)
{
      int di = dbClass.sqlfetch (readabeding);
	  return di;
}

int LSK_CLASS::opensbbeding (void)
{
		if ( readcbeding < 0 ) prepare ();
        return dbClass.sqlopen (readcbeding);
}

int LSK_CLASS::lesesbbeding (	)
{
      int di = dbClass.sqlfetch (readcbeding);
	  return di;
}


int LSK_CLASS::openekbeding (void)
{

		if ( cursor_ausw < 0 ) prepare ();
		
         return dbClass.sqlopen (cursor_ausw);
}

int LSK_CLASS::leseekbeding (	)
{
      int di = dbClass.sqlfetch (cursor_ausw);
	  return di;
}

int LSK_CLASS::openefbeding (void)
{

		if ( count_cursor < 0 ) prepare ();
		
         return dbClass.sqlopen (count_cursor);
}

int LSK_CLASS::leseefbeding (	)
{
      int di = dbClass.sqlfetch (count_cursor);
	  return di;
}

int LSK_CLASS::openfbeding (void)
{

		if ( readbbeding < 0 ) prepare ();
		
         return dbClass.sqlopen (readbbeding);
}

int LSK_CLASS::lesefbeding (	)
{
      int di = dbClass.sqlfetch (readbbeding);
	  return di;
}

int LSK_CLASS::openlsk (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

int LSK_CLASS::leselsk ( void )
{
      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int LSK_CLASS::schreibelsk ( void)
{

		if ( readcursor < 0 ) prepare ();

	
         return dbClass.sqlexecute (upd_cursor);
}


void LSK_CLASS::prepare (void)
{

#ifndef JGTEST
dbClass.sqlin ((short   *) &lsk.ls_stat, SQLSHORT, 0);
#endif
dbClass.sqlin ((double   *) &lsk.of_po, SQLDOUBLE, 0);
dbClass.sqlin ((double   *) &lsk.of_ek, SQLDOUBLE, 0);
dbClass.sqlin ((double   *) &lsk.of_po_euro, SQLDOUBLE, 0);
dbClass.sqlin ((double   *) &lsk.of_ek_euro, SQLDOUBLE, 0);
dbClass.sqlin ((double   *) &lsk.brutto, SQLDOUBLE, 0);
dbClass.sqlin ((double   *) &lsk.gew, SQLDOUBLE, 0);
// 170608 
dbClass.sqlin ((long   *) &lsk.kopf_txt, SQLLONG, 0);
dbClass.sqlin ((long   *) &lsk.fuss_txt, SQLLONG, 0);
dbClass.sqlin ((long   *) &lsk.inka_nr, SQLLONG, 0);


dbClass.sqlin  ((short   *) &lsk.mdn, SQLSHORT, 0);
dbClass.sqlin  ((short   *) &lsk.fil, SQLSHORT, 0);
dbClass.sqlin  ((long    *) &lsk.ls, SQLLONG, 0);

upd_cursor = (short) dbClass.sqlcursor ( " update lsk set "
#ifndef JGTEST
" lsk.ls_stat = ? , "
#endif
"  lsk.of_po = ? "
" , lsk.of_ek = ? "
" , lsk.of_po_euro = ? "
" , lsk.of_ek_euro = ? "
" , lsk.brutto = ? "
" , lsk.gew = ? "
" , lsk.kopf_txt = ? "
" , lsk.fuss_txt = ? "
" , lsk.inka_nr = ? "

" where lsk.mdn = ? and lsk.fil = ? and lsk.ls = ? "
) ;

// fuer readcursor 

dbClass.sqlin  ((short   *) &lsk.mdn, SQLSHORT, 0);
// dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.lieferdat, SQLTIMESTAMP, 26);
dbClass.sqlin ((long   *) &lsk.ls, SQLLONG, 0);

dbClass.sqlout ((long	 *) &lsk.ls, SQLLONG , 0 ) ;
dbClass.sqlout ((long    *) &lsk.adr, SQLLONG ,0) ;
// //dbClass.sqlout ((short *) &lsk.mdn, SQLSHORT, 0) ;
dbClass.sqlout ((long	 *) &lsk.auf, SQLLONG ,0);
dbClass.sqlout ((short   *) &lsk.kun_fil, SQLSHORT, 0);
dbClass.sqlout ((long    *) &lsk.kun, SQLLONG ,0) ;
dbClass.sqlout ((short   *) &lsk.fil, SQLSHORT, 0) ;
// dbClass.sqlout ((char    *)  lsk.feld_bz1,SQLCHAR,20) ;
dbClass.sqlout ((TIMESTAMP_STRUCT *) &lsk.lieferdat,SQLTIMESTAMP,26);
// dbClass.sqlout ((char    *)  lsk.lieferzeit,SQLCHAR, 6) ;
// dbClass.sqlout ((char    *)  lsk.hinweis,SQLCHAR, 49) ;
dbClass.sqlout ((short   *) &lsk.ls_stat, SQLSHORT, 0);
//dbClass.sqlout ((char    *)  lsk.kun_krz1,SQLCHAR, 17) ;
// dbClass.sqlout ((double  *) &lsk.auf_sum,SQLDOUBLE,0) ;
// dbClass.sqlout ((char    *)  lsk.feld_bz2,SQLCHAR, 12) ;
dbClass.sqlout ((double  *) &lsk.lim_er,SQLDOUBLE,0) ;
dbClass.sqlout ((char    *)  lsk.partner,SQLCHAR, 37 ) ;
dbClass.sqlout ((long	 *) &lsk.pr_lst, SQLLONG ,0) ;
dbClass.sqlout ((char    *)  lsk.feld_bz3,SQLCHAR, 8) ;
dbClass.sqlout ((short   *) &lsk.pr_stu, SQLSHORT, 0) ;
dbClass.sqlout ((long    *) &lsk.vertr, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.tou, SQLLONG ,0) ;
dbClass.sqlout ((char    *)  lsk.adr_nam1,SQLCHAR, 37) ;
dbClass.sqlout ((char    *)  lsk.adr_nam2,SQLCHAR, 37) ;
dbClass.sqlout ((char    *)  lsk.pf,SQLCHAR, 17) ;
dbClass.sqlout ((char    *)  lsk.str,SQLCHAR, 37) ;
dbClass.sqlout ((char    *)  lsk.plz,SQLCHAR, 9) ;
dbClass.sqlout ((char    *)  lsk.ort1,SQLCHAR, 37) ;
dbClass.sqlout ((double  *) &lsk.of_po,SQLDOUBLE,0) ;
dbClass.sqlout ((short	 *) &lsk.delstatus, SQLSHORT, 0) ;
dbClass.sqlout ((long    *) &lsk.rech, SQLLONG ,0) ;
dbClass.sqlout ((char    *)  lsk.blg_typ,SQLCHAR, 2) ;
dbClass.sqlout ((double	 *) &lsk.zeit_dec,SQLDOUBLE,0) ;
// 170608 : aktiviert 
dbClass.sqlout ((long    *) &lsk.kopf_txt, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.fuss_txt, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.inka_nr, SQLLONG ,0) ;

// dbClass.sqlout ((char    *)  lsk.auf_ext,SQLCHAR,17 ) ;
// dbClass.sqlout ((long    *) &lsk.teil_smt, SQLLONG ,0) ;
// dbClass.sqlout ((char    *)  lsk.pers_nam,SQLCHAR, 9);
// dbClass.sqlout ((double    *) &lsk.brutto, SQLLONG ,0) ;
// dbClass.sqlout ((TIMESTAMP_STRUCT *) &lsk.komm_dat,SQLTIMESTAMP,26) ;
// dbClass.sqlout ((double  *) &lsk.of_ek,SQLDOUBLE,0) ;
// dbClass.sqlout ((double	 *) &lsk.of_po_euro,SQLDOUBLE,0) ;
// dbClass.sqlout ((double  *) &lsk.of_po_fremd,SQLDOUBLE,0) ;
// dbClass.sqlout ((double  *) &lsk.of_ek_euro,SQLDOUBLE,0) ;
// dbClass.sqlout ((double  *) &lsk.of_ek_fremd,SQLDOUBLE,0) ;
// dbClass.sqlout ((short	 *) &lsk.waehrung,SQLDOUBLE,0) ;
// dbClass.sqlout ((char    *)  lsk.ueb_kz,SQLCHAR, 3);
// dbClass.sqlout ((double  *) &lsk.gew,SQLDOUBLE,0) ;
// dbClass.sqlout ((short   *) &lsk.auf_art, SQLSHORT, 0) ;
// dbClass.sqlout ((short   *) &lsk.fak_typ, SQLSHORT, 0) ;
// dbClass.sqlout ((short   *) &lsk.ccmarkt, SQLSHORT, 0) ;
// dbClass.sqlout ((long    *) &lsk.gruppe, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.tou_nr, SQLLONG ,0) ;
// dbClass.sqlout ((short   *) &lsk.wieg_kompl, SQLSHORT, 0) ;
// dbClass.sqlout ((TIMESTAMP_STRUCT *) &lsk.best_dat,SQLTIMESTAMP,26) ;
// dbClass.sqlout ((char    *)  lsk.hinweis2,SQLCHAR, 33) ;
// dbClass.sqlout ((char    *)  lsk.hinweis3,SQLCHAR, 33) ;
// dbClass.sqlout ((TIMESTAMP_STRUCT *) &lsk.fix_dat,SQLTIMESTAMP,26) ;
// dbClass.sqlout ((char    *)  lsk.komm_name ,SQLCHAR,13 ) ;
// dbClass.sqlout ((short   *) &lsk.psteuer_kz, SQLSHORT, 0) ;


// hier bewusst NICHT auf auf > 0 abgetestet !!! 

readcursor = (short) dbClass.sqlcursor ( " select " 
" ls, adr ,auf ,kun_fil ,kun ,fil "
" ,lieferdat ,ls_stat "		
" ,lim_er ,partner ,pr_lst "
" ,feld_bz3 ,pr_stu ,vertr ,tou ,adr_nam1 "
" ,adr_nam2 ,pf ,str ,plz ,ort1 "
" ,of_po ,delstatus ,rech ,blg_typ ,zeit_dec "
" ,kopf_txt ,fuss_txt ,inka_nr "	// ",auf_ext, teil_smt"
// " ,pers_nam ,brutto ,komm_dat ,of_ek ,of_po_euro "
// " ,of_po_fremd ,of_ek_euro ,of_ek_fremd ,waehrung ,ueb_kz "
// " ,gew ,auf_art ,fak_typ ,ccmarkt ,gruppe "
" , tou_nr "
// " ,tou_nr ,wieg_kompl , best_dat, hinweis2 ,hinweis3 "
// " ,komm_name ,psteuer_kz "
// " from lsk where lsk.mdn = ? and lsk.fil = 0 and lsk.ls = ? and lsk.ls_stat = ? ");

" from lsk where lsk.mdn = ? and lsk.ls = ? for update ");
// Beachte read-Cursor mit "for update" erfordert Transaktionsrahmen " 

// fuer readabeding( Kunden-Nachdruck ) 

dbClass.sqlin  ((short   *) &lsk.mdn, SQLSHORT, 0);

dbClass.sqlin  ((long   *) &lsk.von_ls, SQLLONG, 0);
dbClass.sqlin  ((long   *) &lsk.bis_ls, SQLLONG, 0);

dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.vondate, SQLTIMESTAMP, 26);
dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.bisdate, SQLTIMESTAMP, 26);

dbClass.sqlin  ((long   *) &lsk.von_kun, SQLLONG, 0);
dbClass.sqlin  ((long   *) &lsk.bis_kun, SQLLONG, 0);

dbClass.sqlin  ((long   *) &lsk.von_tou, SQLLONG, 0);
dbClass.sqlin  ((long   *) &lsk.bis_tou, SQLLONG, 0);


dbClass.sqlout ((long	 *) &lsk.ls, SQLLONG , 0 ) ;
dbClass.sqlout ((short	 *) &lsk.fil, SQLSHORT , 0 ) ;
dbClass.sqlout ((long    *) &lsk.kun, SQLLONG ,0) ;
dbClass.sqlout ((short   *) &lsk.ls_stat, SQLSHORT, 0);
dbClass.sqlout ((TIMESTAMP_STRUCT *) &lsk.lieferdat, SQLTIMESTAMP, 26);
dbClass.sqlout ((long	 *) &lsk.tou, SQLLONG , 0 ) ;

dbClass.sqlout ((long    *) &lsk.kopf_txt, SQLLONG  ,0) ;
dbClass.sqlout ((long    *) &lsk.fuss_txt, SQLLONG  ,0) ;
dbClass.sqlout ((long    *) &lsk.inka_nr,  SQLLONG  ,0) ;
dbClass.sqlout ((short   *) &kun.sam_rech, SQLSHORT ,0) ;
dbClass.sqlout ((short   *) &kun.kun_typ,  SQLSHORT ,0) ;


readabeding  = (short) dbClass.sqlcursor ( " select " 
" lsk.ls ,lsk.fil, lsk.kun ,lsk.ls_stat, lsk.lieferdat, lsk.tou "
" ,lsk.kopf_txt ,lsk.fuss_txt ,lsk.inka_nr, kun.sam_rech, kun.kun_typ "

" from lsk, kun where lsk.mdn = ? " 
" and lsk.ls between ?  and ? " 
" and lsk.lieferdat between ?  and ? " 
" and lsk.kun between ?  and ? " 
" and lsk.tou between ?  and ? " 
" and lsk.kun_fil = 0 and lsk.ls_stat between 4 and 6 "
 " and kun.kun = lsk.kun and kun.mdn = lsk.mdn "
 " and kun.sam_rech <> 1 and kun.kun_typ <> 6 "
" order by lsk.lieferdat, lsk.tou, lsk.kun, lsk.ls " );

// fuer readbbeding -Filial-Nachdruckstapel

dbClass.sqlin  ((short   *) &lsk.mdn, SQLSHORT, 0);

dbClass.sqlin  ((long   *) &lsk.von_ls, SQLLONG, 0);
dbClass.sqlin  ((long   *) &lsk.bis_ls, SQLLONG, 0);

dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.vondate, SQLTIMESTAMP, 26);
dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.bisdate, SQLTIMESTAMP, 26);

dbClass.sqlin  ((long   *) &lsk.von_kun, SQLLONG, 0);
dbClass.sqlin  ((long   *) &lsk.bis_kun, SQLLONG, 0);

dbClass.sqlin  ((long   *) &lsk.von_tou, SQLLONG, 0);
dbClass.sqlin  ((long   *) &lsk.bis_tou, SQLLONG, 0);


dbClass.sqlout ((long	 *) &lsk.ls, SQLLONG , 0 ) ;
dbClass.sqlout ((short   *) &lsk.fil, SQLSHORT, 0);
dbClass.sqlout ((long    *) &lsk.kun, SQLLONG ,0) ;
dbClass.sqlout ((short   *) &lsk.ls_stat, SQLSHORT, 0);
dbClass.sqlout ((TIMESTAMP_STRUCT  *) &lsk.lieferdat, SQLTIMESTAMP, 26);
dbClass.sqlout ((long	 *) &lsk.tou, SQLLONG , 0 ) ;

dbClass.sqlout ((long    *) &lsk.kopf_txt, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.fuss_txt, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.inka_nr, SQLLONG ,0) ;

readbbeding  = (short) dbClass.sqlcursor ( " select " 
" lsk.ls ,lsk.fil, lsk.kun ,lsk.ls_stat, lsk.lieferdat, lsk.tou "
" ,lsk.kopf_txt ,lsk.fuss_txt ,lsk.inka_nr "
" from lsk  where lsk.mdn = ? "
" and lsk.ls between ? and ? " 
" and lsk.lieferdat between ? and ? " 
" and lsk.kun between ? and ? " 
" and lsk.tou between ? and ? " 
" and lsk.kun_fil = 1 "
  " and lsk.ls_stat between 4 and 6 " 
" order by lsk.lieferdat,lsk.tou, lsk.kun, lsk.ls " );

// fuer readcbeding 

dbClass.sqlin  ((short   *) &lsk.mdn, SQLSHORT, 0);
// 180509 dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.lieferdat, SQLTIMESTAMP, 26);

dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.vondate, SQLTIMESTAMP, 26);
dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.bisdate, SQLTIMESTAMP, 26);

// dbClass.sqlin  ((char    *) kun.kun_bran, SQLCHAR, 2);
// 280409 : Umstellung auf kun.tou
	dbClass.sqlin  ((long   *) &kun.tou, SQLLONG, 0);

dbClass.sqlout ((long	 *) &lsk.ls, SQLLONG , 0 ) ;
dbClass.sqlout ((short	 *) &lsk.fil, SQLSHORT , 0 ) ;
dbClass.sqlout ((long    *) &lsk.kun, SQLLONG ,0) ;
dbClass.sqlout ((short   *) &lsk.ls_stat, SQLSHORT, 0);

dbClass.sqlout ((long    *) &lsk.kopf_txt, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.fuss_txt, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.inka_nr, SQLLONG ,0) ;

readcbeding  = (short) dbClass.sqlcursor ( " select " 
" lsk.ls ,lsk.fil, lsk.kun ,lsk.ls_stat "
" ,lsk.kopf_txt ,lsk.fuss_txt ,lsk.inka_nr "

// 180509 " from lsk, kun  where lsk.mdn = ? and lsk.lieferdat = ? " 
" from lsk, kun  where lsk.mdn = ? and lsk.lieferdat between ? and ? " 
" and lsk.kun_fil = 0 " 
" and lsk.ls_stat between 3 and 4 " 
 " and kun.kun = lsk.kun and kun.mdn = lsk.mdn and kun.tou = ? "
 " and ( kun.sam_rech =  1 or kun.kun_typ = 6) "
// nur Sofortrechnung bzw. Barkunde mit selektieren
" order by lsk.kun, lsk.ls " );
// #################################################################

// fuer cursor_ausw	-> Kunden-Erstdruck 

dbClass.sqlin  ((short   *) &lsk.mdn, SQLSHORT, 0);

dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.vondate, SQLTIMESTAMP, 26);
dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.bisdate, SQLTIMESTAMP, 26);

dbClass.sqlin  ((long   *) &lsk.von_kun, SQLLONG, 0);
dbClass.sqlin  ((long   *) &lsk.bis_kun, SQLLONG, 0);

dbClass.sqlin  ((long   *) &lsk.von_tou, SQLLONG, 0);
dbClass.sqlin  ((long   *) &lsk.bis_tou, SQLLONG, 0);

dbClass.sqlout ((long	 *) &lsk.ls, SQLLONG , 0 ) ;
dbClass.sqlout ((short	 *) &lsk.fil, SQLSHORT , 0 ) ;
dbClass.sqlout ((long    *) &lsk.kun, SQLLONG ,0) ;
dbClass.sqlout ((short   *) &lsk.ls_stat, SQLSHORT, 0);
dbClass.sqlout ((TIMESTAMP_STRUCT   *) &lsk.lieferdat, SQLTIMESTAMP, 26);
dbClass.sqlout ((long	 *) &lsk.tou, SQLLONG , 0 ) ;

dbClass.sqlout ((long    *) &lsk.kopf_txt, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.fuss_txt, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.inka_nr, SQLLONG ,0) ;
dbClass.sqlout ((short   *) &kun.sam_rech, SQLSHORT ,0) ;
dbClass.sqlout ((short   *) &kun.kun_typ, SQLSHORT ,0) ;


cursor_ausw  = (short) dbClass.sqlcursor ( " select " 
" lsk.ls ,lsk.fil, lsk.kun ,lsk.ls_stat, lsk.lieferdat, lsk.tou "
" ,lsk.kopf_txt ,lsk.fuss_txt ,lsk.inka_nr, kun.sam_rech, kun.kun_typ "

" from lsk, kun  where lsk.mdn = ? "
" and lsk.lieferdat between  ? and ? " 
" and lsk.kun between  ? and ? " 
" and lsk.tou between  ? and ? " 
" and lsk.kun_fil = 0 " 
" and lsk.ls_stat = 3 " 
 " and kun.kun = lsk.kun and kun.mdn = lsk.mdn  "
" order by lsk.lieferdat, lsk.tou, lsk.kun, lsk.ls " );

// fuer count_cursor	// Filial-Erst-Druck 
dbClass.sqlin  ((short   *) &lsk.mdn, SQLSHORT, 0);

dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.vondate, SQLTIMESTAMP, 26);
dbClass.sqlin  ((TIMESTAMP_STRUCT    *) &lsk.bisdate, SQLTIMESTAMP, 26);

dbClass.sqlin  ((long   *) &lsk.von_kun, SQLLONG, 0) ;
dbClass.sqlin  ((long   *) &lsk.bis_kun, SQLLONG, 0) ;

dbClass.sqlin  ((long   *) &lsk.von_tou, SQLLONG, 0) ;
dbClass.sqlin  ((long   *) &lsk.bis_tou, SQLLONG, 0) ;

dbClass.sqlout ((long	 *) &lsk.ls, SQLLONG , 0 ) ;
dbClass.sqlout ((short   *) &lsk.fil, SQLSHORT, 0);
dbClass.sqlout ((long    *) &lsk.kun, SQLLONG ,0) ;
dbClass.sqlout ((short   *) &lsk.ls_stat, SQLSHORT, 0);
dbClass.sqlout ((TIMESTAMP_STRUCT  *) &lsk.lieferdat, SQLTIMESTAMP, 26);
dbClass.sqlout ((long	 *) &lsk.tou, SQLLONG , 0 ) ;

dbClass.sqlout ((long    *) &lsk.kopf_txt, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.fuss_txt, SQLLONG ,0) ;
dbClass.sqlout ((long    *) &lsk.inka_nr, SQLLONG ,0) ;

count_cursor  = (short) dbClass.sqlcursor ( " select " 
" lsk.ls ,lsk.fil, lsk.kun ,lsk.ls_stat, lsk.lieferdat, lsk.tou "
" ,lsk.kopf_txt ,lsk.fuss_txt ,lsk.inka_nr "
" from lsk  where lsk.mdn = ? "
" and lsk.lieferdat between  ? and ? "
" and lsk.kun between  ? and ? "
" and lsk.tou between  ? and ? " 
" and lsk.kun_fil = 1 "
" and lsk.ls_stat = 3 " 
" order by lsk.lieferdat,lsk.tou, lsk.kun, lsk.ls " );


}

struct LSP lsp,  lsp_null;

int LSP_CLASS::closeall ( void )
{
	if ( readallcursor > -1 ) dbClass.sqlclose(readallcursor) ;
	if ( readcursor > -1 )    dbClass.sqlclose(readcursor) ;
	if ( count_cursor > -1 )    dbClass.sqlclose(count_cursor) ;
	return 0 ;
}

static int anzfelder ;


int LSP_CLASS::leselsp (void)
{

      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int LSP_CLASS::openlsp (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}


void LSP_CLASS::prepare (void)
{


dbClass.sqlin  ((short   *) &lsp.mdn , SQLSHORT, 0);
dbClass.sqlin  ((long    *) &lsp.ls  , SQLLONG, 0);

//dbClass.sqlout ((short *) &lsp.mdn, SQLSHORT, 0) ;
dbClass.sqlout ((short   *) &lsp.fil ,SQLSHORT,0) ;
// dbClass.sqlout ((long *) &lsp.ls ,SQLLONG,0) ;

dbClass.sqlout ((double	 *) &lsp.a ,SQLDOUBLE,0) ;
dbClass.sqlout ((double	 *) &lsp.auf_me,SQLDOUBLE,0) ;
dbClass.sqlout ((char	 *)  lsp.auf_me_bz,SQLCHAR, 7);
dbClass.sqlout ((double	 *) &lsp.lief_me,SQLDOUBLE,0) ;
dbClass.sqlout ((char	 *)  lsp.lief_me_bz,SQLCHAR, 7) ;
dbClass.sqlout ((double	 *) &lsp.ls_vk_pr,SQLDOUBLE,0) ;
dbClass.sqlout ((double	 *) &lsp.ls_lad_pr,SQLDOUBLE,0) ;
dbClass.sqlout ((short	 *) &lsp.delstatus,SQLSHORT,0) ;
dbClass.sqlout ((double	 *) &lsp.tara,SQLDOUBLE,0) ;
dbClass.sqlout ((long	 *) &lsp.posi,SQLLONG,0) ;
dbClass.sqlout ((long	 *) &lsp.lsp_txt,SQLLONG,0) ;
dbClass.sqlout ((short	 *) &lsp.pos_stat,SQLSHORT,0) ;
dbClass.sqlout ((short	 *) &lsp.sa_kz_sint,SQLSHORT,0) ;
dbClass.sqlout ((char	 *)  lsp.erf_kz,SQLCHAR, 2);
dbClass.sqlout ((double	 *) &lsp.prov_satz,SQLDOUBLE,0) ;
dbClass.sqlout ((short	 *) &lsp.leer_pos,SQLSHORT,0) ;
dbClass.sqlout ((TIMESTAMP_STRUCT *) &lsp.hbk_date,SQLTIMESTAMP,26 ) ; 
dbClass.sqlout ((char	 *)  lsp.ls_charge,SQLCHAR, 31) ;
dbClass.sqlout ((double	 *) &lsp.auf_me_vgl,SQLDOUBLE,0) ;
dbClass.sqlout ((double	 *) &lsp.ls_vk_euro,SQLDOUBLE,0) ;
dbClass.sqlout ((double	 *) &lsp.ls_vk_fremd,SQLDOUBLE,0);
dbClass.sqlout ((double	 *) &lsp.ls_lad_euro,SQLDOUBLE,0) ;
dbClass.sqlout ((double	 *) &lsp.ls_lad_fremd,SQLDOUBLE,0) ;
dbClass.sqlout ((double	 *) &lsp.rab_satz,SQLDOUBLE,0) ;
dbClass.sqlout ((short	 *) &lsp.me_einh_kun,SQLSHORT,0)  ;
dbClass.sqlout ((short	 *) &lsp.me_einh,SQLSHORT,0) ;
dbClass.sqlout ((short	 *) &lsp.me_einh_kun1,SQLSHORT,0) ;
dbClass.sqlout ((double	 *) &lsp.auf_me1,SQLDOUBLE,0) ;
dbClass.sqlout ((double	 *) &lsp.inh1,SQLDOUBLE,0) ;
dbClass.sqlout ((short	 *) &lsp.me_einh_kun2,SQLSHORT,0) ;
dbClass.sqlout ((double	 *) &lsp.auf_me2,SQLDOUBLE,0) ;
dbClass.sqlout ((double	 *) &lsp.inh2,SQLDOUBLE,0) ;
dbClass.sqlout ((short	 *) &lsp.me_einh_kun3,SQLSHORT,0) ;
dbClass.sqlout ((double	 *) &lsp.auf_me3,SQLDOUBLE,0) ;
dbClass.sqlout ((double	 *) &lsp.inh3,SQLDOUBLE,0) ;
dbClass.sqlout ((char	 *)  lsp.lief_me_bz_ist,SQLCHAR,12); 
dbClass.sqlout ((double	 *) &lsp.inh_ist,SQLDOUBLE,0) ;
dbClass.sqlout ((short	 *) &lsp.me_einh_ist,SQLSHORT,0) ;
dbClass.sqlout ((double	 *) &lsp.me_ist,SQLDOUBLE,0) ; 
dbClass.sqlout ((short	 *) &lsp.lager,SQLSHORT,0) ;
dbClass.sqlout ((double	 *) &lsp.lief_me1,SQLDOUBLE,0) ;
dbClass.sqlout ((double	 *) &lsp.lief_me2,SQLDOUBLE,0) ;
dbClass.sqlout ((double	 *) &lsp.lief_me3,SQLDOUBLE,0) ;
dbClass.sqlout ((short	 *) &lsp.ls_pos_kz,SQLSHORT,0) ;
dbClass.sqlout ((double	 *) &lsp.a_grund,SQLDOUBLE,0) ;
dbClass.sqlout ((char	 *)  lsp.kond_art,SQLCHAR, 5) ;
dbClass.sqlout ((long	 *) &lsp.posi_ext,SQLLONG,0) ;
dbClass.sqlout ((long	 *) &lsp.kun ,SQLLONG,0) ;
dbClass.sqlout ((short	 *) &lsp.na_lief_kz ,SQLSHORT,0) ;
dbClass.sqlout ((long	 *) &lsp.nve_posi ,SQLLONG,0);
dbClass.sqlout ((short	 *) &lsp.teil_smt,SQLSHORT,0) ;
dbClass.sqlout ((short	 *) &lsp.aufschlag,SQLSHORT,0) ;
dbClass.sqlout ((double	 *) &lsp.aufschlag_wert,SQLDOUBLE,0) ;
dbClass.sqlout ((char	 *)  lsp.ls_ident,SQLCHAR,32 ) ;

// erweiterte Cursoren fuer Minimum an Performance 

dbClass.sqlout ((short	 *) &a_bas.me_einh,SQLSHORT,0) ;
dbClass.sqlout ((double	 *) &a_bas.a_gew,SQLDOUBLE,0) ;
dbClass.sqlout ((short	 *) &a_bas.a_typ,SQLSHORT,0) ;
dbClass.sqlout ((short	 *) &a_bas.a_typ2,SQLSHORT,0) ;
dbClass.sqlout ((double	 *) &a_bas.sw,SQLDOUBLE,0) ;

// erweiterte Cursoren fuer Minimum an Performance 

readcursor = (short) dbClass.sqlcursor ( " select " 
	"  lsp.fil ,lsp.a ,auf_me ,auf_me_bz ,lief_me "
	" ,lief_me_bz ,ls_vk_pr ,ls_lad_pr ,lsp.delstatus ,tara ,posi "
	" ,lsp_txt ,pos_stat ,sa_kz_sint ,erf_kz ,prov_satz "
	" ,leer_pos ,hbk_date ,ls_charge ,auf_me_vgl ,ls_vk_euro "
	" ,ls_vk_fremd ,ls_lad_euro ,ls_lad_fremd ,rab_satz ,me_einh_kun "
	" ,lsp.me_einh ,me_einh_kun1 ,auf_me1 ,inh1 ,me_einh_kun2 "
	" ,auf_me2 ,inh2 ,me_einh_kun3 ,auf_me3 ,inh3 "
	" ,lief_me_bz_ist ,inh_ist ,me_einh_ist ,me_ist ,lager "
	" ,lief_me1 ,lief_me2 ,lief_me3 ,ls_pos_kz ,lsp.a_grund "
	" ,kond_art ,posi_ext ,kun ,na_lief_kz ,nve_posi "
	" ,lsp.teil_smt ,aufschlag ,aufschlag_wert ,ls_ident "

	" , a_bas.me_einh, a_bas.a_gew , a_bas.a_typ, a_bas.a_typ2,a_bas.sw "

	" from lsp,a_bas where lsp.mdn = ? and lsp.fil = 0 and lsp.ls = ?  and a_bas.a = lsp.a  "
	) ;


}
