// 53200.h : Hauptheaderdatei f�r die stapdru-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error "\"stdafx.h\" vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"       // Hauptsymbole


// C53200App:
// Siehe 53200.cpp f�r die Implementierung dieser Klasse
//

class C53200App : public CWinApp
{
public:
	C53200App();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	afx_msg void OnRDruckWahl();
	afx_msg void OnDruckWahl();
	DECLARE_MESSAGE_MAP()
};

extern C53200App theApp;