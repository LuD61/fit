#include "ifmxdiag.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern void LogStr( char*, ... );

EXEC SQL include sqlproto.h;
EXEC SQL include sqlhdr.h;
EXEC SQL include sqlca.h;

/* Werte fuer warn_flg */
#define WARN   1
#define NOWARN 0

/*
 *  exp_chk.ec
 */

EXEC SQL define SUCCESS 0;
EXEC SQL define WARNING 1;
EXEC SQL define NODATA 100;
EXEC SQL define RTERROR -1;

/*
 * The sqlstate_err() function checks the SQLSTATE status variable to see
 * if an error or warning has occurred following an SQL statement.
 */
int sqlstate_err( void )
{
    int err_code = RTERROR;

    if(SQLSTATE[0] == '0') /* trap '00', '01', '02' */
        {
        switch(SQLSTATE[1])
            {
            case '0': /* success - return 0 */
                err_code = SUCCESS;
                break;
            case '1': /* warning - return 1 */
                err_code = WARNING;
                break;
            case '2': /* end of data - return 100 */
                err_code = NODATA;
                break;
            default: /* error - return SQLCODE */
                break;
            }
        }
    return(err_code);
}


/*
 * The disp_sqlstate_err() function executes the GET DIAGNOSTICS
 * statement and prints the detail for each exception that is returned.
 */
void disp_sqlstate_err( void )
{
int j;

EXEC SQL BEGIN DECLARE SECTION;
    int exception_count;
    char overflow[2];
/*
    int exception_num=1;
*/
    char class_id[255];
    char subclass_id[255];
    char message[255];
    int messlen;
    char sqlstate_code[6];
    int i;
EXEC SQL END DECLARE SECTION;

    LogStr("---------------------------------");
    LogStr("-------------------------\n");
    LogStr("SQLSTATE: %s\n",SQLSTATE);
    LogStr("SQLCODE: %d\n", SQLCODE);
    LogStr("\n");
 
    EXEC SQL get diagnostics :exception_count = NUMBER,
        :overflow = MORE;
    LogStr("EXCEPTIONS:  Number=%d\t", exception_count);
    LogStr("More? %s\n", overflow);
    for (i = 1; i <= exception_count; i++)
        {
        EXEC SQL get diagnostics  exception :i
            :sqlstate_code = RETURNED_SQLSTATE,
            :class_id = CLASS_ORIGIN, :subclass_id = SUBCLASS_ORIGIN,
            :message = MESSAGE_TEXT, :messlen = MESSAGE_LENGTH;
        LogStr("- - - - - - - - - - - - - - - - - - - -\n");
        LogStr("EXCEPTION %d: SQLSTATE=%s\n", i,
            sqlstate_code);
        message[messlen-1] = '\0';
        LogStr("MESSAGE TEXT: %s\n", message);
 
        //j = byleng(class_id, stleng(class_id));
        j = byleng(class_id, strlen(class_id));
        class_id[j] = '\0';
        LogStr("CLASS ORIGIN: %s\n",class_id);
 
        j = byleng(subclass_id, strlen(subclass_id));
        subclass_id[j] = '\0';
        LogStr("SUBCLASS ORIGIN: %s\n",subclass_id);
        }
 
    LogStr("---------------------------------");
    LogStr("-------------------------\n");
}
 
void disp_error( char *stmt )
{
    LogStr("\n********Error encountered in %s********\n",
        stmt);
    disp_sqlstate_err();
}

void disp_warning( char *stmt )
{
    LogStr("\n********Warning encountered in %s********\n",
        stmt);
    disp_sqlstate_err();
}

void disp_exception( char *stmt, int sqlerr_code, int warn_flg )
{
    switch (sqlerr_code)
        {
        case SUCCESS:
        case NODATA:
            break;
        case WARNING:
            if(warn_flg)
                disp_warning(stmt);
            break;
        case RTERROR:
            disp_error(stmt);
            break;
        default:
            LogStr("\n********INVALID EXCEPTION STATE for %s********\n",
                stmt);
            break;
        }
}

/*
 * The exp_chk() function calls sqlstate_err() to check the SQLSTATE
 * status variable to see if an error or warning has occurred following
 * an SQL statement. If either condition has occurred, exp_chk()
 * calls disp_sqlstate_err() to print the detailed error information.
 *
 * This function handles exceptions as follows:
 *   runtime errors - call exit(1)
 *   warnings - continue execution, returning "1"
 *   success - continue execution, returning "0"
 *   Not Found - continue execution, returning "100"
 */
long exp_chk( char *stmt, int warn_flg )
{
    int sqlerr_code = SUCCESS;
 
    sqlerr_code = sqlstate_err();
    disp_exception(stmt, sqlerr_code, warn_flg);
 
    if(sqlerr_code == RTERROR)   /* Exception is a runtime error */
        {
        /* Exit the program after examining the error */
        LogStr("********Program terminated*******\n\n");
        exit(1);
        }
    /* else */                  /* Exception is "success", "Not Found",*/
        return(sqlerr_code);    /*  or "warning"                       */
}

/*
 * The exp_chk2() function calls sqlstate_err() to check the SQLSTATE
 * status variable to see if an error or warning has occurred following
 * an SQL statement. If either condition has occurred, exp_chk2()
 * calls disp_exception() to print the detailed error information.
 *
 * This function handles exceptions as follows:
 *   runtime errors - continue execution, returning SQLCODE (<0)
 *   warnings - continue execution, returning one (1)
 *   success - continue execution, returning zero (0)
 *   Not Found - continue execution, returning 100
 */
long exp_chk2( char *stmt, int warn_flg )
{
    int sqlerr_code = SUCCESS;
    long sqlcode;
 
    sqlcode = SQLCODE;  /* save SQLCODE in case of error */
    sqlerr_code = sqlstate_err();
    disp_exception(stmt, sqlerr_code, warn_flg);
 
    if(sqlerr_code == RTERROR)  /* if runtime error, return SQLCODE */
        sqlerr_code = sqlcode;
 
    return(sqlerr_code);
}

/*
 * The whenexp_chk() function calls sqlstate_err() to check the SQLSTATE
 * status variable to see if an error or warning has occurred following
 * an SQL statement. If either condition has occurred, whenerr_chk()
 * calls disp_sqlstate_err() to print the detailed error information.
 *
 * This function is expected to be used with the WHENEVER SQLERROR
 * statement: it executes an exit(1) when it encounters a negative
 * error code. It also assumes the presence of the "statement" global
 * variable, set by the calling program to the name of the statement
 * encountering the error.
*/
int whenexp_chk( void )
{
    int sqlerr_code = SUCCESS;
    int disp = 0;
 
    sqlerr_code = sqlstate_err();
 
    if(sqlerr_code == WARNING)
        {
        disp = 1;
        LogStr("\n********Warning encountered in %s********\n",
            statement);
        }
    else
        if(sqlerr_code == RTERROR)
            {
            LogStr("\n********Error encountered in %s********\n",
                statement);
            disp = 1;
            }
    if(disp)
        disp_sqlstate_err();
 
    if(sqlerr_code == RTERROR)
        {
        /* Exit the program after examining the error */
        LogStr("********Program terminated*******\n\n");
        exit(1);
        }
    else
        {
        if(sqlerr_code == WARNING)
            LogStr("\n********Program execution continues********\n\n");
        return(sqlerr_code);
        }

    return ( sqlerr_code ) ;	// syntax-dummy
	
}
