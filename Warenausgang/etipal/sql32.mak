!include <win32.mak>

cflags = $(cflags) -I $(INFORMIXDIR)\incl\esql -DWINDOWS -D_MBCS -GF -FD -EHsc -Gy -TP -MTd -wd4996


ifmxdiag.obj : ifmxdiag.ec
	esql.exe -dcmdl -e ifmxdiag.ec
	cl.exe $(cflags) ifmxdiag.c

sql.obj : sql.ec
	esql.exe -dcmdl -e sql.ec
	cl.exe $(cflags) sql.c
