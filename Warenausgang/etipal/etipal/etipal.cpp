#include "stdafx.h"
#include "etipal.h"
#include <stdio.h>
#include <stdlib.h>
#include <commctrl.h>
#include "..\sql.h"

#define MAX_LOADSTRING 100

// Globale Variablen:
int wmId, wmEvent;
HINSTANCE hInst;								// Aktuelle Instanz
TCHAR szTitle[MAX_LOADSTRING];					// Titelleistentext
TCHAR szWindowClass[MAX_LOADSTRING];			// Klassenname des Hauptfensters

TCHAR szLogFile[120] = _T("c:\\user\\fit\\tmp\\etidruck.log");
TCHAR szDrk[120] = _T("SCREEN");
TCHAR szEti0[120] = _T("c:\\user\\fit\\etc\\etikett0.etk");
TCHAR szEti1[120] = _T("c:\\user\\fit\\etc\\etikett1.etk");
TCHAR szEti2[120] = _T("c:\\user\\fit\\etc\\etikett2.etk");
TCHAR szPar0[120] = _T("c:\\user\\fit\\tmp\\parafile0.par");
TCHAR szPar1[120] = _T("c:\\user\\fit\\tmp\\parafile1.par");
TCHAR szPar2[120] = _T("c:\\user\\fit\\tmp\\parafile2.par");

int iDrkTyp, iAnzahl;

FILE *fpLog;

DATA_t data;

// Vorw�rtsdeklarationen der in diesem Codemodul enthaltenen Funktionen:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

INT_PTR CALLBACK	EtiDruckProc(HWND, UINT, WPARAM, LPARAM);
void ReadIniFile( void );

int APIENTRY _tWinMain(HINSTANCE hInstance,
					   HINSTANCE hPrevInstance,
					   LPTSTR    lpCmdLine,
					   int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Hier Code einf�gen.
	MSG msg;
	HACCEL hAccelTable;

	// Globale Zeichenfolgen initialisieren
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_ETIDRUCK1, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Anwendungsinitialisierung ausf�hren:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_ETIDRUCK1));

	// Hauptmeldungsschleife:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNKTION: MyRegisterClass()
//
//  ZWECK: Registriert die Fensterklasse.
//
//  KOMMENTARE:
//
//    Sie m�ssen die Funktion verwenden,  wenn Sie m�chten, dass der Code
//    mit Win32-Systemen kompatibel ist, bevor die RegisterClassEx-Funktion
//    zu Windows 95 hinzugef�gt wurde. Der Aufruf der Funktion ist wichtig,
//    damit die kleinen Symbole, die mit der Anwendung verkn�pft sind,
//    richtig formatiert werden.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ETIDRUCK1));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_ETIDRUCK1);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNKTION: InitInstance(HINSTANCE, int)
//
//   ZWECK: Speichert das Instanzenhandle und erstellt das Hauptfenster.
//
//   KOMMENTARE:
//
//        In dieser Funktion wird das Instanzenhandle in einer globalen Variablen gespeichert, und das
//        Hauptprogrammfenster wird erstellt und angezeigt.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Instanzenhandle in der globalen Variablen speichern

	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	ReadIniFile();

	return TRUE;
}

//
//  FUNKTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ZWECK:  Verarbeitet Meldungen vom Hauptfenster.
//
//  WM_COMMAND	- Verarbeiten des Anwendungsmen�s
//  WM_PAINT	- Zeichnen des Hauptfensters
//  WM_DESTROY	- Beenden-Meldung anzeigen und zur�ckgeben
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// int wmId, wmEvent; <-- globalisiert
	HDC hdc;
	PAINTSTRUCT ps;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Men�auswahl bearbeiten:
		switch (wmId)
		{
		case ID_DRUCKEN_ETIKETT0:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG0), hWnd, EtiDruckProc);
			break;
		case ID_DRUCKEN_ETIKETT1:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, EtiDruckProc);
			break;
		case ID_DRUCKEN_ETIKETT2:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG2), hWnd, EtiDruckProc);
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			fclose( fpLog );
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Hier den Zeichnungscode hinzuf�gen.
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Meldungshandler f�r Infofeld.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void ShiftFiles( TCHAR *szFile, int n )
{
	TCHAR szFile1[120],szFile2[120];

	wsprintf( szFile2, _T("%s%d"), szFile, n );
	_tremove( szFile2 );
	while ( n > 1 )
	{
		wsprintf( szFile2, _T("%s%d"), szFile, n-- );
		wsprintf( szFile1, _T("%s%d"), szFile, n  );
		_trename( szFile1, szFile2 );
	}
	wsprintf( szFile1, _T("%s%d"), szFile, 1  );
	_trename( szFile, szFile1 );
}

void LogStr( TCHAR *szFmt, ... )
{
	TCHAR szTmp[200];
	va_list ap;

	va_start( ap, szFmt );
	wvsprintf( szTmp, szFmt, ap ); 
	va_end( ap );

	fprintf( fpLog, "%s\n", szTmp );
	fflush( fpLog );

}
void ReadIniFile( void )
{
	int iMaxLogFiles;
	TCHAR szIniFile[120] = _T("c:\\user\\fit\\etc\\drebach1.etk");
	wsprintf( szIniFile, _T("%s\\etipal.ini"), _tgetenv( _T("BWSETC") ) );

	GetPrivateProfileString( _T("ETIDRUCK"), _T("LOGFILE"), szLogFile, szLogFile, sizeof szLogFile, szIniFile );
	iMaxLogFiles = GetPrivateProfileInt( _T("ETIDRUCK"), _T("MAXLOGFILES"), 1, szIniFile );
	ShiftFiles( szLogFile, iMaxLogFiles ); 

	fpLog = fopen((char*) szLogFile, "w" );

	LogStr( _T("EtiDruck, Version %s"), __TIMESTAMP__ );
	LogStr( _T("INIFILE: %s"), szIniFile );
	LogStr( _T("LOGFILE: %s, --> ...1 --> ... --> ...%d"), szLogFile, iMaxLogFiles );

	GetPrivateProfileString( _T("ETIDRUCK"), _T("DRUCKER"), szDrk, szDrk, sizeof szDrk, szIniFile );
	GetPrivateProfileString( _T("ETIDRUCK"), _T("ETIKETT0"), szEti0, szEti0, sizeof szEti1, szIniFile );
	GetPrivateProfileString( _T("ETIDRUCK"), _T("ETIKETT1"), szEti1, szEti1, sizeof szEti1, szIniFile );
	GetPrivateProfileString( _T("ETIDRUCK"), _T("ETIKETT2"), szEti2, szEti2, sizeof szEti2, szIniFile );
	GetPrivateProfileString( _T("ETIDRUCK"), _T("PARFILE0"), szPar0, szPar0, sizeof szPar0, szIniFile );
	GetPrivateProfileString( _T("ETIDRUCK"), _T("PARFILE1"), szPar1, szPar1, sizeof szPar1, szIniFile );
	GetPrivateProfileString( _T("ETIDRUCK"), _T("PARFILE2"), szPar2, szPar2, sizeof szPar2, szIniFile );
	iDrkTyp = GetPrivateProfileInt( _T("ETIDRUCK"), _T("DRUCKERTYP"), 0, szIniFile );
	iAnzahl = GetPrivateProfileInt( _T("ETIDRUCK"), _T("ANZAHL"), 0, szIniFile );

	LogStr( _T("DRUCKER: %s"), szDrk );
	LogStr( _T("DRUCKERTYP: %d"), iDrkTyp );
	LogStr( _T("ANZAHL: %d"), iAnzahl );
	LogStr( _T("ETIKETT0: %s"), szEti0 );
	LogStr( _T("ETIKETT1: %s"), szEti1 );
	LogStr( _T("ETIKETT2: %s"), szEti2 );
	LogStr( _T("PARFILE0: %s"), szPar0 );
	LogStr( _T("PARFILE1: %s"), szPar1 );
	LogStr( _T("PARFILE2: %s"), szPar2 );
}

int FormCheckDigit( char *szBuf )
{
	int c, d, i, w;
	i = (int) strlen( szBuf ) - 1;
	if ( i < 0 )
	{
		return 0;
	}
	c = 0;
	w = 1;
	for ( ; i >= 0 ; i-- )
	{
		if ( szBuf[i] < '0' || szBuf[i] > '9' )
		{
			return 0;
		}
		switch (w)
		{
		case 1: w=3; break;
		case 3: w=1; break;
		}
		d = (szBuf[i] - '0') * w;
		c += d;
	}
	c = 10 - c % 10;
	if ( c == 10 )
		c = 0;
	return c; 
}


BOOL NumCheck( char *szBuf, size_t len, int ausrichtung )
{
	size_t i, len1, len2;

	for ( i = 0; i < len ; i++ )
	{
		if (  szBuf[i] < '0' || szBuf[i] > '9' )
		{
			szBuf[i] = '\0';
			break;
		}
	}

	len1 = strlen( szBuf );
	len2 = len - len1;
	if ( len1 == len )
	{		
		return FALSE;
	}
	if ( len1 > len )
	{
		szBuf[len] = '\0';		
		return TRUE;
	}

	if ( ausrichtung  == 'R' )
	{
		memmove( szBuf + len2, szBuf, len1 );
		memset( szBuf, '0', len2 );
		szBuf[len] = '\0';
	}
	else if ( ausrichtung  == 'L' )
	{
		memset( szBuf + len1, '0', len2 );
		szBuf[len] = '\0';
	}
	return TRUE;

}

// von JG ...

BOOL DatumCheck( char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		bufi[0] = '\0' ;
		return TRUE ;
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )	// 25.02.08 : hier stand hil2 und dann hats nicht gefunzt
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}

void GetKG_NK3( char *szBuf, int *kg, int *g )
{
	char *p;

	*kg = *g = 0;

	p = strtok( szBuf, ".," );
	if ( p == NULL ) return;
    *kg = atoi( szBuf );

	p = strtok( NULL, ".," );
	*g = 0;
	if ( p == NULL ) return;
    if ( *p < '0' || *p > '9' ) return; 
	*g += 100 * ( *p - '0' );

	p++;
	if ( p == NULL ) return;
    if ( *p < '0' || *p > '9' ) return; 
	*g += 10 * ( *p - '0' );

	p++;
	if ( p == NULL ) return;
    if ( *p < '0' || *p > '9' ) return; 
	*g += ( *p - '0' );
}

void GetKG_NK2( char *szBuf, int *kg, int *g )
{
	char *p;

	*kg = *g = 0;

	p = strtok( szBuf, ".," );
	if ( p == NULL ) return;
    *kg = atoi( szBuf );

	p = strtok( NULL, ".," );
	*g = 0;
	if ( p == NULL ) return;
    if ( *p < '0' || *p > '9' ) return; 
	*g += 10 * ( *p - '0' );

	p++;
	if ( p == NULL ) return;
    if ( *p < '0' || *p > '9' ) return; 
	*g += ( *p - '0' );
}

void GetTTMMJJ( char *szDatum, int *tt, int *mm, int *jj )
{
	if ( strlen(szDatum) < 10 )
	{
		*tt = 1;
		*mm = 1;
		*jj = 1;
		return;
	}
	*tt = (szDatum[0] - '0') * 10 + szDatum[1] - '0';
	*mm = (szDatum[3] - '0') * 10 + szDatum[4] - '0';
	*jj = (szDatum[8] - '0') * 10 + szDatum[9] - '0';
}

int GetParStr( char *line, char *name, char *val, int lenmax )
{
	char *p;
	int len;
	p = strstr( line, name );

	if ( p == NULL ) return 0;

	p = line + strlen(name);
	len = (int) strlen(p);
	if ( len > lenmax ) len = lenmax;

	p[len] = '\0';
	while ( --len > 0 )
	{
		if ( p[len] != ' ' && p[len] != '\n' ) break;
		p[len] = '\0';
	}

	memcpy( val, p, len+1 );

	return 1;
}

int GetParInt( char *line, char *name, int *val )
{
	char *p;
	p = strstr( line, name );

	if ( p == NULL ) return 0;
	p = line + strlen(name);
	*val = atoi( p );

	return 1;
}

int SetParStr( char *line, char *name, char *val )
{
	char *p;
	p = strstr( line, name );

	if ( p == NULL ) return 0;

	p = line + strlen(name);
	wsprintf( p, _T("%s\n"), val );

	return 1;
}

int SetParInt( char *line, char *name, int val, int len )
{
	char *p;
	char fmt[10];
	p = strstr( line, name );

	if ( p == NULL ) return 0;

	p = line + strlen( name );
	wsprintf( fmt, _T("%%d\n") );
	if (len > 0)
	{
	    wsprintf( fmt, _T("%%0%dd\n"), len );
	}
	wsprintf( p, fmt, val );

	return 1;
}

void WriteParFile( TCHAR *szPar )
{
	char line[120];
	TCHAR szBuf[120], szParOld[120];
	FILE *fp, *fpOld;


	LogStr( _T("Rewriting %s ..."), szPar );

	wsprintf( szParOld, _T("%s_0"), szPar );
	_tremove( szParOld );
	_trename( szPar, szParOld );

	fpOld = _tfopen( szParOld, _T("r") );
	if ( fpOld == NULL )
		return;

	fp = _tfopen( szPar, _T("w") );
	if ( fp == NULL )
		return;

	for ( ; fgets( line, sizeof line, fpOld ); )
	{
		SetParInt( line, _T("$$MDN;"), data.iMdn, 0 );
		SetParStr( line, _T("$$ILN;"), data.szILN );

		SetParStr( line, _T("$$KUNNR;"), data.szKun );
		SetParStr( line, _T("$$AUFNR;"), data.szAuf );
		SetParStr( line, _T("$$LIEFNR;"), data.szLs );
		SetParStr( line, _T("$$BESTNR;"), data.szBest );
		SetParStr( line, _T("$$NAME1;"), data.szAdrName );
		SetParStr( line, _T("$$STRASSE;"), data.szAdrStr );
		SetParStr( line, _T("$$PLZ;"), data.szAdrPlz );
		SetParStr( line, _T("$$ORT;"), data.szAdrOrt );
		SetParStr( line, _T("$$GLN;"), data.szGLN );

		SetParStr( line, _T("$$ANUM;"), data.szAnum );
		SetParStr( line, _T("$$ABEZ;"), data.szAbez );
		SetParStr( line, _T("$$EAN0;"), data.szEAN );

		SetParInt( line, _T("$$STK;"), data.iStk, 0 );
		wsprintf( szBuf, _T("%04d,%02d"), data.iNettokg, data.iNettog );
		SetParStr( line, _T("$$NETTOGEW;"), szBuf );
		wsprintf( szBuf, _T("%04d,%02d"), data.iBruttokg, data.iBruttog );
		SetParStr( line, _T("$$BRUTTOGEW;"), szBuf );
		wsprintf( szBuf, _T("%02d.%02d.20%02d"), data.iMHDtt, data.iMHDmm, data.iMHDjj );
		SetParStr( line, _T("$$MHD;"), szBuf );
		SetParStr( line, _T("$$CHARGE;"), data.szCharge );
		SetParInt( line, _T("$$PALNUM;"), data.iPal, 3 );
		SetParInt( line, _T("$$PZEAN0;"), data.iPZEAN, 1 );
		SetParInt( line, _T("$$PZNVE;"), data.iPZNVE, 1 );

		fputs( line, fp );
	}
	fclose( fp );
	fclose( fpOld );
}

void PutDataToControls( HWND hDlg, TCHAR* szPar )
{
	char line[120];
	TCHAR szBuf[120];
	FILE *fp;

	LogStr(_T("Initializing Controls from %s ..."), szPar );

	fp = _tfopen( szPar, _T("r") );
	if ( fp == NULL )
		return;
	memset( (void*) &data, '\0', sizeof data );
	for ( ; fgets( line, sizeof line, fp ); )
	{		 
		if ( GetParInt( line, _T("$$MDN;"), &data.iMdn ) ) continue;
		if ( GetParStr( line, _T("$$KUNNR;"), data.szKun, sizeof data.szKun ) ) continue;
		if ( GetParStr( line, _T("$$NAME1;"), data.szAdrName, sizeof data.szAdrName ) ) continue;
		if ( GetParStr( line, _T("$$STRASSE;"), data.szAdrStr, sizeof data.szAdrStr ) ) continue;
		if ( GetParStr( line, _T("$$PLZ;"), data.szAdrPlz, sizeof data.szAdrPlz ) ) continue;
		if ( GetParStr( line, _T("$$ORT;"), data.szAdrOrt, sizeof data.szAdrOrt ) ) continue;

		if ( GetParStr( line, _T("$$EAN0;"), data.szEAN, sizeof data.szEAN ) ) continue;
		if ( GetParInt( line, _T("$$PZEAN0;"), &data.iPZEAN ) ) continue;
		if ( GetParInt( line, _T("$$STK;"), &data.iStk ) ) continue;
		if ( GetParStr( line, _T("$$NETTOGEW;"), szBuf, sizeof("kkkk.gg") ) )
		{
			GetKG_NK2( szBuf, &data.iNettokg, &data.iNettog );
			continue;
		}
		if ( GetParStr( line, _T("$$BRUTTOGEW;"), szBuf, sizeof("kkkk.gg") ) )
		{
			GetKG_NK2( szBuf, &data.iBruttokg, &data.iBruttog );
			continue;
		}
		if ( GetParStr( line, _T("$$MHD;"), szBuf, sizeof("tt.mm.jjjj") ) )
		{
			GetTTMMJJ( szBuf, &data.iMHDtt, &data.iMHDmm, &data.iMHDjj );
			continue;
		}
		if ( GetParStr( line, _T("$$CHARGE;"), data.szCharge, sizeof data.szCharge ) ) continue;

		if ( GetParStr( line, _T("$$ILN;"), data.szILN, sizeof data.szILN ) ) continue;
		if ( GetParStr( line, _T("$$LIEFNR;"), data.szLs, sizeof data.szLs ) ) continue;
		if ( GetParStr( line, _T("$$AUFNR;"), data.szAuf, sizeof data.szAuf ) ) continue;
		if ( GetParInt( line, _T("$$PALNUM;"), &data.iPal ) ) continue;
		if ( GetParInt( line, _T("$$PZNVE;"), &data.iPZNVE ) ) continue;
	}
	fclose( fp );
    
	SetDlgItemText( hDlg, IDC_EDIT_KUN, data.szKun );
	SetDlgItemText( hDlg, IDC_EDIT_ADRNAME, data.szAdrName );
	SetDlgItemText( hDlg, IDC_EDIT_ADRSTR, data.szAdrStr );
	SetDlgItemText( hDlg, IDC_EDIT_ADRPLZ, data.szAdrPlz );
	SetDlgItemText( hDlg, IDC_EDIT_ADRORT, data.szAdrOrt );
	SetDlgItemText( hDlg, IDC_EDIT_GLN, data.szGLN );

	SetDlgItemInt( hDlg, IDC_EDIT_EAN_0, 9, 0 );
	SetDlgItemText( hDlg, IDC_EDIT_EAN, data.szEAN );	
	SetDlgItemInt( hDlg, IDC_EDIT_PZEAN, data.iPZEAN, 0 );
	wsprintf( szBuf, "%d", data.iStk );
	SetDlgItemText( hDlg, IDC_EDIT_STK, szBuf );
	wsprintf( szBuf, "%04d,%02d", data.iNettokg, data.iNettog );
	SetDlgItemText( hDlg, IDC_EDIT_GEW, szBuf );
	wsprintf( szBuf, "%04d,%02d", data.iBruttokg, data.iBruttog );
	SetDlgItemText( hDlg, IDC_EDIT_BRUTTO, szBuf );
	GetTTMMJJ( data.szMHD, &data.iMHDtt, &data.iMHDmm, &data.iMHDjj );
    wsprintf( szBuf, "%02d.%02d.20%02d", data.iMHDtt, data.iMHDmm, data.iMHDjj );
	SetDlgItemText( hDlg, IDC_EDIT_MHD, szBuf );
	SetDlgItemText( hDlg, IDC_EDIT_CHRG, data.szCharge );

	SetDlgItemInt( hDlg, IDC_EDIT_NVE, 3, 0 );
	SetDlgItemText( hDlg, IDC_EDIT_ILN, data.szILN );
	SetDlgItemText( hDlg, IDC_EDIT_AUF, data.szAuf );
	SetDlgItemText( hDlg, IDC_EDIT_LS1, data.szLs );
	SetDlgItemText( hDlg, IDC_EDIT_LS2, data.szLs );
	SetDlgItemText( hDlg, IDC_EDIT_LS3, data.szLs );
	SetDlgItemText( hDlg, IDC_EDIT_BEST1, data.szBest );
	SetDlgItemText( hDlg, IDC_EDIT_BEST2, data.szBest );
	SetDlgItemText( hDlg, IDC_EDIT_ANUM, data.szAnum );
	SetDlgItemText( hDlg, IDC_EDIT_ABEZ, data.szAbez );
	wsprintf( szBuf, "%03d", data.iPal );
	SetDlgItemText( hDlg, IDC_EDIT_PAL, szBuf );
	wsprintf( szBuf, "3%7s%06ld%03d", data.szILN, atol(data.szLs), data.iPal );
	data.iPZNVE = FormCheckDigit( szBuf );
	SetDlgItemInt( hDlg, IDC_EDIT_PZNVE, data.iPZNVE, 0 );

	LogStr(_T("... done.") );

}

void GetDataFromControls( WPARAM wParam, HWND hDlg )
{
	char szBuf[120];

	switch ( LOWORD(wParam) )
	{
	case IDC_EDIT_KUN:
		GetDlgItemText( hDlg, IDC_EDIT_KUN,
			data.szKun, sizeof data.szKun );
		GetAdr( &data );
		SetDlgItemText( hDlg, IDC_EDIT_ADRNAME, data.szAdrName );
		SetDlgItemText( hDlg, IDC_EDIT_ADRSTR, data.szAdrStr );
		SetDlgItemText( hDlg, IDC_EDIT_ADRPLZ, data.szAdrPlz );
		SetDlgItemText( hDlg, IDC_EDIT_ADRORT, data.szAdrOrt );
		SetDlgItemText( hDlg, IDC_EDIT_GLN, data.szGLN );
		break;
	case IDC_EDIT_ADRNAME:
		GetDlgItemText( hDlg, IDC_EDIT_ADRNAME,
			data.szAdrName, sizeof data.szAdrName );
		break;
	case IDC_EDIT_ADRSTR:
		GetDlgItemText( hDlg, IDC_EDIT_ADRSTR,
			data.szAdrStr, sizeof data.szAdrStr );
		break;
	case IDC_EDIT_ADRPLZ:
		GetDlgItemText( hDlg, IDC_EDIT_ADRPLZ,
			data.szAdrPlz, sizeof data.szAdrPlz );
		break;
	case IDC_EDIT_ADRORT:
		GetDlgItemText( hDlg, IDC_EDIT_ADRORT,
			data.szAdrOrt, sizeof data.szAdrOrt );
		break;
	case IDC_EDIT_EAN:
		GetDlgItemText( hDlg, IDC_EDIT_EAN, data.szEAN, 13 );
		if ( NumCheck( data.szEAN, 12, 'L' ) )
		{
			SetDlgItemText( hDlg, IDC_EDIT_EAN, data.szEAN );
		}
		wsprintf( szBuf, "9%12s", data.szEAN );
		data.iPZEAN = FormCheckDigit( szBuf );
		SetDlgItemInt( hDlg, IDC_EDIT_PZEAN, data.iPZEAN, 0 );
		break;
	case IDC_EDIT_STK:
		data.iStk = GetDlgItemInt( hDlg, IDC_EDIT_STK, 0, 0 );
		wsprintf( szBuf, "%d", data.iStk );
		SetDlgItemText( hDlg, IDC_EDIT_STK, szBuf );
		break;
	case IDC_EDIT_GEW:
		GetDlgItemText( hDlg, IDC_EDIT_GEW, szBuf, 8 );
		GetKG_NK2( szBuf, &data.iNettokg, &data.iNettog );
	    wsprintf( szBuf, "%04d,%02d", data.iNettokg, data.iNettog );
		SetDlgItemText( hDlg, IDC_EDIT_GEW, szBuf );
		break;
	case IDC_EDIT_BRUTTO:
		GetDlgItemText( hDlg, IDC_EDIT_BRUTTO, szBuf, 8 );
		GetKG_NK2( szBuf, &data.iBruttokg, &data.iBruttog );
	    wsprintf( szBuf, "%04d,%02d", data.iBruttokg, data.iBruttog );
		SetDlgItemText( hDlg, IDC_EDIT_BRUTTO, szBuf );
		break;
	case IDC_EDIT_MHD:
		GetDlgItemText( hDlg, IDC_EDIT_MHD, szBuf, 11 );
		if ( DatumCheck( szBuf ) == TRUE )
		{
			SetDlgItemText( hDlg, IDC_EDIT_MHD, szBuf );
		}
		GetTTMMJJ( szBuf, &data.iMHDtt, &data.iMHDmm, &data.iMHDjj );
		break;
	case IDC_EDIT_CHRG:
		GetDlgItemText( hDlg, IDC_EDIT_CHRG, data.szCharge, 7 );
		if ( NumCheck( data.szCharge, 6, 'R' ) )
		{
			SetDlgItemText( hDlg, IDC_EDIT_CHRG, data.szCharge );
		}
		break;
	//case IDC_EDIT_ILN:
	//	GetDlgItemText( hDlg, IDC_EDIT_ILN, data.szILN, 8 );
	//	if ( NumCheck( data.szILN, 7, 'L' ) )
	//	{
	//		SetDlgItemText( hDlg, IDC_EDIT_ILN, data.szILN );
	//	}
	//	wsprintf( szBuf, "3%7s%06ld%03d", data.szILN, atol(data.szLs), data.iPal );
	//	data.iPZNVE = FormCheckDigit( szBuf );
	//	SetDlgItemInt( hDlg, IDC_EDIT_PZNVE, data.iPZNVE, 0 );
	//	break;  
	case IDC_EDIT_AUF:
		GetDlgItemText( hDlg, IDC_EDIT_AUF, data.szAuf, 7 );
		if ( NumCheck( data.szAuf, 6, 0 ) )
		{
			SetDlgItemText( hDlg, IDC_EDIT_AUF, data.szAuf );
		}
		SaveOldLsnve( &data );
		GetLsk( &data );
		data.iPal = 1;
		SetDlgItemText( hDlg, IDC_EDIT_LS1, data.szLs );
		SetDlgItemText( hDlg, IDC_EDIT_LS2, data.szLs );
		wsprintf( szBuf, "%06ld", atol(data.szLs) );
		SetDlgItemText( hDlg, IDC_EDIT_LS3, szBuf );
		SetDlgItemText( hDlg, IDC_EDIT_BEST1, data.szBest );
		SetDlgItemText( hDlg, IDC_EDIT_BEST2, data.szBest );
	    wsprintf( szBuf, "%03d", data.iPal );
	    SetDlgItemText( hDlg, IDC_EDIT_PAL, szBuf );
		wsprintf( szBuf, "3%7s%06ld%03d", data.szILN, atol(data.szLs), data.iPal );
		data.iPZNVE = FormCheckDigit( szBuf );
		SetDlgItemInt( hDlg, IDC_EDIT_PZNVE, data.iPZNVE, 0 );
		break;
	case IDC_EDIT_LS1:
		GetDlgItemText( hDlg, IDC_EDIT_LS1, data.szLs, 7 );
		if ( NumCheck( data.szLs, 6, 0 ) )
		{
			SetDlgItemText( hDlg, IDC_EDIT_LS1, data.szLs );
		}
		SaveOldLsnve( &data );
		GetLsk( &data );
		data.iPal = 1;
		SetDlgItemText( hDlg, IDC_EDIT_AUF, data.szAuf );
		SetDlgItemText( hDlg, IDC_EDIT_LS2, data.szLs );
		wsprintf( szBuf, "%06ld", atol(data.szLs) );
		SetDlgItemText( hDlg, IDC_EDIT_LS3, szBuf );
		SetDlgItemText( hDlg, IDC_EDIT_BEST1, data.szBest );
		SetDlgItemText( hDlg, IDC_EDIT_BEST2, data.szBest );
	    wsprintf( szBuf, "%03d", data.iPal );
	    SetDlgItemText( hDlg, IDC_EDIT_PAL, szBuf );
		wsprintf( szBuf, "3%7s%06ld%03d", data.szILN, atol(data.szLs), data.iPal );
		data.iPZNVE = FormCheckDigit( szBuf );
		SetDlgItemInt( hDlg, IDC_EDIT_PZNVE, data.iPZNVE, 0 );
		break;
	case IDC_EDIT_ANUM:
		GetDlgItemText( hDlg, IDC_EDIT_ANUM, data.szAnum, 7 );
		if ( NumCheck( data.szAnum, 6, 0 ) )
		{
			SetDlgItemText( hDlg, IDC_EDIT_ANUM, data.szAnum );
		}
		GetLsp( &data );
		SetDlgItemText( hDlg, IDC_EDIT_MHD, data.szMHD );
		SetDlgItemText( hDlg, IDC_EDIT_CHRG, data.szCharge );

		SetDlgItemText( hDlg, IDC_EDIT_EAN, data.szEAN );
		if ( NumCheck( data.szEAN, 12, 'L' ) )
		{
			SetDlgItemText( hDlg, IDC_EDIT_EAN, data.szEAN );
		}
		SetDlgItemText( hDlg, IDC_EDIT_ABEZ, data.szAbez );
		wsprintf( szBuf, "9%12s", data.szEAN );
		data.iPZEAN = FormCheckDigit( szBuf );
		SetDlgItemInt( hDlg, IDC_EDIT_PZEAN, data.iPZEAN, 0 );
		break;
	//case IDC_EDIT_PAL:
	//	data.iPal = GetDlgItemInt( hDlg, IDC_EDIT_PAL, 0, 0 );
	//	wsprintf( szBuf, "%03d", data.iPal );
	//	SetDlgItemText( hDlg, IDC_EDIT_PAL, szBuf );
	//	wsprintf( szBuf, "3%7s%06ld%03d", data.szILN, atol(data.szLs), data.iPal );
	//	data.iPZNVE = FormCheckDigit( szBuf );
	//	SetDlgItemInt( hDlg, IDC_EDIT_PZNVE, data.iPZNVE, 0 );
	//	break;
	}
}

void CreateDESADVProcess( LPSTR szCmdLine )
{
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );


    // Start the child process. 
    if( !CreateProcess( NULL,   // No module name (use command line). 
        TEXT(szCmdLine),  // TEXT("MyChildProcess"), // Command line. 
        NULL,             // Process handle not inheritable. 
        NULL,             // Thread handle not inheritable. 
        FALSE,            // Set handle inheritance to FALSE. 
        0,                // No creation flags. 
        NULL,             // Use parent's environment block. 
        NULL,             // Use parent's starting directory. 
        &si,              // Pointer to STARTUPINFO structure.
        &pi )             // Pointer to PROCESS_INFORMATION structure.
    ) 
    {
        printf( "CreateProcess failed (%d).\n", GetLastError() );
        return;
    }

    // Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );

    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
}

void EtiDruck( HWND hDlg, TCHAR *szEti, TCHAR *szPar )
{
	TCHAR szCmdLine[120];
	WriteParFile( szPar );
	int iVorschau;
	iVorschau = strcmp(szDrk, "SCREEN") ? 0 : 1 ;
	wsprintf( szCmdLine, _T("dr70001o.exe -name %s -datei %s -anzahl %d -dr %d -typ %d"),
		szEti, szPar, iAnzahl, iVorschau, iDrkTyp );
	LogStr(_T("Creating Process: %s"), szCmdLine) ;

	try
	{
			CreateDESADVProcess( szCmdLine );
	}
	catch(...)
	{
		MessageBoxEx( hDlg, szPar, _T(""), MB_OK, 0 );
		return;
	}
	LogStr(_T("Returned from Process: %s"), szCmdLine) ;

	char szBuf[120];

	data.iPal++;
	WriteParFile( szPar );
	wsprintf( szBuf, "%03d", data.iPal );
	SetDlgItemText( hDlg, IDC_EDIT_PAL, szBuf );
	wsprintf( szBuf, "3%7s%06ld%03d", data.szILN, atol(data.szLs), data.iPal );
	data.iPZNVE = FormCheckDigit( szBuf );
	SetDlgItemInt( hDlg, IDC_EDIT_PZNVE, data.iPZNVE, 0 );
	LogStr( _T("Naechste Palette: %d"), data.iPal ) ;
}

LRESULT CALLBACK SubclassProc(HWND hWnd, UINT uMsg, WPARAM wParam,
							  LPARAM lParam, UINT_PTR uIdSubclass, DWORD_PTR dwRefData)
{
	//LogStr(
	//	_T("%s: Wnd = %ld, Msg = %d, wP = %04X-%04X, lP = %04X-%04X, wP = %06d-%06d, lP = %06d-%06d"),
	//	_T("SubClassProc"),
	//	hWnd, uMsg,
	//	HIWORD(wParam), LOWORD(wParam),
	//	HIWORD(lParam), LOWORD(lParam),
	//	HIWORD(wParam), LOWORD(wParam),
	//	HIWORD(lParam), LOWORD(lParam) );

	switch (uMsg)
	{
	case WM_GETDLGCODE:
		return ( DLGC_WANTALLKEYS | DefSubclassProc( hWnd, uMsg, wParam, lParam ) );

	case WM_CHAR:
		//Process this message to avoid message beeps.
		if ((wParam == VK_RETURN) || (wParam == VK_TAB))
		{
			return FALSE;
		}
		break;

	case WM_KEYDOWN:
		if ((wParam == VK_RETURN) || (wParam == VK_TAB))
		{
			if ( GetDlgCtrlID( hWnd ) == IDOK )
			{
				SetFocus( GetDlgItem( GetParent( hWnd ), IDC_EDIT_ANUM ) );
				SendDlgItemMessage( GetParent( hWnd ), IDC_EDIT_ANUM, EM_SETSEL, 0, 6 );
			}
			else
			{
				PostMessage( GetParent( hWnd ), WM_NEXTDLGCTL, 0, 0 );
			}
			return FALSE;
		}
		break;
	} 
	return DefSubclassProc(hWnd, uMsg, wParam, lParam);
}

INT_PTR CALLBACK EtiDruckProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
    
	switch ( message )
	{
	case WM_INITDIALOG:
		SendDlgItemMessage( hDlg, IDC_EDIT_KUN, EM_LIMITTEXT, 8, 0 );
		SendDlgItemMessage( hDlg, IDC_EDIT_AUF, EM_LIMITTEXT, 6, 0 );
		SendDlgItemMessage( hDlg, IDC_EDIT_LS, EM_LIMITTEXT, 6, 0 );
		SendDlgItemMessage( hDlg, IDC_EDIT_LS1, EM_LIMITTEXT, 6, 0 );
		SendDlgItemMessage( hDlg, IDC_EDIT_ANUM, EM_LIMITTEXT, 6, 0 );
		SendDlgItemMessage( hDlg, IDC_EDIT_STK, EM_LIMITTEXT, 3, 0 );
		SendDlgItemMessage( hDlg, IDC_EDIT_GEW, EM_LIMITTEXT, 7, 0 );
		SendDlgItemMessage( hDlg, IDC_EDIT_BRUTTO, EM_LIMITTEXT, 7, 0 );
		SendDlgItemMessage( hDlg, IDC_EDIT_MHD, EM_LIMITTEXT, 10, 0 );
		SendDlgItemMessage( hDlg, IDC_EDIT_CHRG, EM_LIMITTEXT, 6, 0 );
		SendDlgItemMessage( hDlg, IDC_EDIT_PAL, EM_LIMITTEXT, 3, 0 );

		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_KUN ), SubclassProc, 0, 0 ); 
		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_AUF ), SubclassProc, 0, 0 ); 
		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_LS ), SubclassProc, 0, 0 ); 
		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_LS1 ), SubclassProc, 0, 0 ); 
		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_ANUM ), SubclassProc, 0, 0 ); 
		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_EAN ), SubclassProc, 0, 0 ); 
		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_STK ), SubclassProc, 0, 0 ); 
		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_GEW ), SubclassProc, 0, 0 ); 
		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_BRUTTO ), SubclassProc, 0, 0 ); 
		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_MHD ), SubclassProc, 0, 0 ); 
		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_CHRG ), SubclassProc, 0, 0 ); 
		SetWindowSubclass( GetDlgItem( hDlg, IDC_EDIT_PAL ), SubclassProc, 0, 0 ); 

		switch ( wmId )
		{
		case ID_DRUCKEN_ETIKETT0:
			PutDataToControls( hDlg, szPar0 );
            break;
		case ID_DRUCKEN_ETIKETT1: PutDataToControls( hDlg, szPar1 ); break;
		case ID_DRUCKEN_ETIKETT2: PutDataToControls( hDlg, szPar2 ); break;
		}

		return (INT_PTR)TRUE;	    

	case WM_COMMAND:
		if ( LOWORD(wParam) == IDOK )
		{
			switch ( wmId )
			{
			case ID_DRUCKEN_ETIKETT0:
				InsertLsnve( &data );
				EtiDruck( hDlg, szEti0, szPar0 );
				break;
			case ID_DRUCKEN_ETIKETT1: EtiDruck( hDlg, szEti1, szPar1 ); break;
			case ID_DRUCKEN_ETIKETT2: EtiDruck( hDlg, szEti2, szPar2 ); break;
			}
			//EndDialog(hDlg, LOWORD(wParam));
		}
		else if ( LOWORD(wParam) == IDDESADV )
		{
			TCHAR szCmdLine[120];
			wsprintf( szCmdLine, _T("desadv.exe %s %d %s"), data.szDta, data.iMdn, data.szLs );
			LogStr(_T("DESADV ...: %s"), szCmdLine) ;
			CreateDESADVProcess( szCmdLine );
			LogStr(_T("... DESADV: %s"), szCmdLine) ;
		}
		else if ( LOWORD(wParam) == IDCANCEL )
		{
			EndDialog(hDlg, LOWORD(wParam));
		}
		else if (HIWORD(wParam) == EN_KILLFOCUS)
		{
			GetDataFromControls( wParam, hDlg );
		}
		break;
	}
	return (INT_PTR) FALSE;
}