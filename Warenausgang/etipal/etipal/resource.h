//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by etipal.rc
//
#define IDC_MYICON                      2
#define IDCANCEL2                       3
#define IDD_ETIDRUCK1_DIALOG            102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_ETIDRUCK1                   107
#define IDI_SMALL                       108
#define IDC_ETIDRUCK1                   109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_DIALOG2                     130
#define IDD_DIALOG0                     131
#define IDC_EDIT_LS                     1002
#define IDC_EDIT_PAL                    1003
#define IDC_EDIT_STK                    1004
#define IDC_EDIT_MHD                    1005
#define IDC_EDIT_NVE                    1006
#define IDC_EDIT_PZNVE                  1007
#define IDC_EDIT_EAN                    1008
#define IDC_EDIT_KUN                    1009
#define IDC_EDIT_ANUM                   1010
#define IDC_EDIT_CHRG                   1011
#define IDC_EDIT_ADRNAME                1014
#define IDC_EDIT_ADRSTR                 1015
#define IDC_EDIT_ADRPLZ                 1016
#define IDC_EDIT_ADRORT                 1017
#define IDC_EDIT_ILN                    1018
#define IDC_EDIT_BEST1                  1019
#define IDC_EDIT_EAN_0                  1020
#define IDC_EDIT_GLN                    1022
#define IDC_EDIT_PZEAN                  1023
#define IDC_EDIT_KUN_2                  1024
#define IDC_EDIT_LS3                    1024
#define IDC_EDIT_GEW                    1025
#define IDC_EDIT_BRUTTO                 1026
#define IDC_EDIT_LS2                    1028
#define IDC_EDIT_ABEZ                   1029
#define IDC_EDIT_BEST2                  1030
#define IDC_EDIT_LS1                    1032
#define IDDESADV                        1033
#define IDC_EDIT1                       1034
#define IDC_EDIT_AUF                    1034
#define ID_DRUCKEN_ETIKETT1             32771
#define ID_DRUCKEN_ETIKETT2             32772
#define ID_BEENDEN_EDEKAPALETTENETIKETT 32773
#define ID_DRUCKEN_ETIKETT0             32774
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
