//#ifndef SQL_H
//#define SQL_H

typedef struct
{
	int iMdn;
	int iPZNVE;
	int iStk;
	int iNettokg;
	int iNettog;
	int iBruttokg;
	int iBruttog;
	int iMHDtt;
	int iMHDmm;
	int iMHDjj;
	char szMHD[11];
	char szCharge[7];
	char szILN[8];
	char szGLN[17];
	char szEAN[14];
	int iPal;
	int iPZEAN;
	char szKun[10];
	char szDta[10];
	char szAdrName[37];
	char szAdrStr[37];
	char szAdrPlz[8];
	char szAdrOrt[37];
	char szAuf[10];
	char szLs[10];
	char szBest[10];
	char szAnum[10];
	char szAkun[13];
	char szAbez[60];
} DATA_t;

void GetAdr( DATA_t * );
void GetLsk( DATA_t * );
void GetLsp( DATA_t * );
void InsertLsnve( DATA_t * );
void SaveOldLsnve( DATA_t *data );

//#endif
