#include "sql.h"
#include "ifmxdiag.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sqlhdr.h" 

extern void LogStr( char *, ... );

EXEC SQL include sqlproto.h;
EXEC SQL include sqlca.h;
EXEC SQL include sqlda.h;
EXEC SQL include sqlstype.h;
EXEC SQL include sqltypes.h;
EXEC SQL include decimal.h;

EXEC SQL BEGIN DECLARE SECTION;

typedef struct {
    long mdn;
	long kun;
	long adr;
	char adr_nam1[37];
	char str[37];
	char plz[9];
	char ort1[37];
	char iln[17];
	char dta[9];
} SQL_ADR_t;

SQL_ADR_t sqladr;

typedef struct {
	long ls;
	long auf;
	char auf_ext[31];
} SQL_LSK_t;

SQL_LSK_t sqllsk;

typedef struct {
	double a;
	date hbk_date;
	char ls_charge[31];
} SQL_LSP_t;

SQL_LSP_t sqllsp;

typedef struct {
    char a_kun[13];
	char ean[14];
	char a_bz1[25];
	char a_bz2[25];
	char a_bz3[25];
	char a_bz4[25];
} SQL_A_KUN_t;

SQL_A_KUN_t sqla_kun;

typedef struct {
    int mdn;
    int fil;
    long nve_posi;
    char nve[31];
    char nve_palette[31];
	long ls;
	double a;
	double me;
	int me_einh;
	int gue;
	int palette_kz;
	date mhd;
	date tag;
    double brutto_gew;
    long verp_art;
    double ps_art;
    double ps_stck;
    double ps_art1;
    double ps_stck1;
    char masternve[31];
    char ls_charge[31];
    double netto_gew;
} LSNVE_t;

LSNVE_t lsnve;

char statement[3000];

EXEC SQL END DECLARE SECTION;

int cursors_initialized = 0;

int OpenDatabase( void )
{
	EXEC SQL DATABASE BWS;
	if (SQLCODE != 0)
	{
		LogStr( "DATABASE BWS: SQLCODE=%ld\n",SQLCODE);
		exit(1);
	}
	EXEC SQL SET ISOLATION TO DIRTY READ;
	return 0;
}

void CloseDatabase( void )
{
	EXEC SQL CLOSE BWS;
	EXEC SQL DISCONNECT CURRENT; 
    LogStr( "Datenbank BWS geschlossen." );   
}

void FormQueries( void )
{
	strcpy( statement, "SELECT ADR2,SEDAS_NR3,ILN FROM KUN WHERE MDN=? AND KUN=?" );
	LogStr( "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_kun_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare kun_curs cursor for p_kun_curs;
	exp_chk( statement, WARN );

	strcpy( statement, "SELECT ADR_NAM1,STR,PLZ,ORT1 FROM ADR WHERE ADR=?" );
	LogStr( "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_adr_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare adr_curs cursor for p_adr_curs;
	exp_chk( statement, WARN );

	strcpy( statement, "SELECT AUF_EXT,LS,AUF FROM LSK WHERE MDN=? AND KUN=? AND (LS=? OR AUF=?) AND LS_STAT BETWEEN 4 AND 6" );
	LogStr( "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_lsk_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare lsk_curs cursor for p_lsk_curs;
	exp_chk( statement, WARN );

	strcpy( statement, "SELECT HBK_DATE,LS_CHARGE FROM LSP WHERE MDN=? AND LS=? AND A=?" );
	LogStr( "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_lsp_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare lsp_curs cursor for p_lsp_curs;
	exp_chk( statement, WARN );

	strcpy( statement, "SELECT A_KUN,EAN,A_BZ1,A_BZ2,A_BZ3,A_BZ4 FROM A_KUN WHERE MDN=? AND KUN=? AND A=?" );
	LogStr( "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_a_kun_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare a_kun_curs cursor for p_a_kun_curs;
	exp_chk( statement, WARN );
     
    cursors_initialized = 1;
}

void trim( char *fieldp )
{
    size_t len;
    char * cp;
	len = strlen(fieldp);
	cp = fieldp + len - 1;
	while ( len > 1 && *cp == ' ' )
	{
		*cp = '\0';
		len--, cp--;
	}
}

long QueryAdr( void )
{
	long sqlcode;   
    if ( cursors_initialized == 0 )
    {
          OpenDatabase();
          FormQueries();
    }

	sqladr.iln[0] = '\0';

	EXEC SQL open kun_curs using :sqladr.mdn,:sqladr.kun;
	if ( SQLCODE != 0 ) 
	{
		LogStr( "open kun_curs: SQLCODE=%ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch kun_curs into :sqladr.adr, :sqladr.dta, :sqladr.iln;
	sqlcode = SQLCODE;
    EXEC SQL close kun_curs;
	if ( sqlcode != 0L )
	{
		LogStr( "fetch kun_curs: mdn: %ld kun: %ld SQLCODE: %ld",
		    sqladr.mdn, sqladr.kun, sqlcode );
		return 0;
	}
	
	LogStr( "kun = %ld, adr = %ld, iln = %s", sqladr.kun, sqladr.adr, sqladr.iln );
	
	sqladr.adr_nam1[0] = '\0';
	sqladr.str[0] = '\0';
	sqladr.plz[0] = '\0';
	sqladr.ort1[0] = '\0';
	
	EXEC SQL open adr_curs using :sqladr.adr;
	if ( SQLCODE != 0 ) 
	{
		LogStr( "open adr_curs: SQLCODE=%ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch adr_curs	into :sqladr.adr_nam1,:sqladr.str,:sqladr.plz,:sqladr.ort1;
	sqlcode = SQLCODE;
    EXEC SQL close adr_curs;
	if ( sqlcode != 0L )
	{
		LogStr( "fetch adr_curs: kun: %ld adr: %ld iln: %s SQLCODE: %ld",
               sqladr.kun, sqladr.adr, sqladr.adr, sqlcode );
	}
	trim( sqladr.adr_nam1 );
	trim( sqladr.str );
	trim( sqladr.plz );
	trim( sqladr.ort1 );
	trim( sqladr.iln );
	
	return 1;
}

void GetAdr( DATA_t *data )
{
    sqladr.mdn = data->iMdn;
    sqladr.kun = atol( data->szKun );
    if ( ! QueryAdr() )
    {
        strcpy( sqladr.adr_nam1, "Adresse unbekannt!" );
	    sqladr.str[0] = '\0';
	    sqladr.plz[0] = '\0';
	    sqladr.ort1[0] = '\0';
	    sqladr.iln[0] = '\0';
    }
    strcpy( data->szAdrName, sqladr.adr_nam1 );
    strcpy( data->szAdrStr, sqladr.str);
    strcpy( data->szAdrPlz, sqladr.plz );
    strcpy( data->szAdrOrt, sqladr.ort1 );
    strcpy( data->szGLN, sqladr.iln );
    strcpy( data->szDta, sqladr.dta );
}    

long QueryLsk( void )
{
	long sqlcode;   
    if ( cursors_initialized == 0 )
    {
          OpenDatabase();
          FormQueries();
    }
	EXEC SQL open lsk_curs using :sqladr.mdn, :sqladr.kun, :sqllsk.ls, :sqllsk.auf;
	if ( SQLCODE != 0 ) 
	{
		LogStr( "open lsk_curs: SQLCODE=%ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch lsk_curs into :sqllsk.auf_ext, :sqllsk.ls, :sqllsk.auf;
	sqlcode = SQLCODE;
    EXEC SQL close lsk_curs;
	if ( sqlcode != 0L )
	{
		LogStr( "fetch lsk_curs: kun: %ld ls: %ld SQLCODE: %ld", sqladr.kun, sqllsk.ls, sqlcode );
		return 0;
	}
	
	LogStr( "kun = %ld, ls = %ld, auf = %ld, auf_ext = %s", sqladr.kun, sqllsk.ls, sqllsk.auf, sqllsk.auf_ext );
		
	return 1;
}

void GetLsk( DATA_t *data )
{
    sqllsk.ls = atol( data->szLs );
    sqllsk.auf = atol( data->szAuf );
    if ( ! QueryLsk() )
    {
        strcpy( data->szLs, "000000" );
        strcpy( data->szBest, "000000" );
    }
    sqllsk.auf_ext[6] = '\0';
    strcpy( data->szBest, sqllsk.auf_ext );
    sprintf( data->szLs, "%ld", sqllsk.ls );
    sprintf( data->szAuf, "%ld", sqllsk.auf );
}
    

long QueryLsp( void )
{
	long sqlcode;   
    if ( cursors_initialized == 0 )
    {
          OpenDatabase();
          FormQueries();
    }

	sqllsp.ls_charge[0] = '\0';

	EXEC SQL open lsp_curs using :sqladr.mdn,:sqllsk.ls, :sqllsp.a;
	if ( SQLCODE != 0 ) 
	{
		LogStr( "open lsp_curs: SQLCODE=%ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch lsp_curs into :sqllsp.hbk_date, :sqllsp.ls_charge;
	sqlcode = SQLCODE;
    EXEC SQL close lsp_curs;
	if ( sqlcode != 0L )
	{
		LogStr( "fetch lsp_curs: ls: %ld a: %d SQLCODE: %ld", sqllsk.ls, (long) sqllsp.a, sqlcode );
		return 0;
	}
	trim( sqllsp.ls_charge );
	
	LogStr( "a = %ld, charge = %s", (long) sqllsp.a, sqllsp.ls_charge );
		
	return 1;
}

long QueryAkun( void )
{
	long sqlcode;   
    if ( cursors_initialized == 0 )
    {
          OpenDatabase();
          FormQueries();
    }
	
	sqla_kun.a_kun[0] = '\0';
	sqla_kun.a_bz1[0] = '\0';
	sqla_kun.a_bz2[0] = '\0';
	sqla_kun.a_bz3[0] = '\0';
	sqla_kun.a_bz4[0] = '\0';
	
	EXEC SQL open a_kun_curs using :sqladr.mdn,:sqladr.kun,:sqllsp.a;
	if ( SQLCODE != 0 ) 
	{
		LogStr( "open a_kun_curs: SQLCODE=%ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch a_kun_curs into :sqla_kun.a_kun,:sqla_kun.ean,
	             :sqla_kun.a_bz1,:sqla_kun.a_bz2,:sqla_kun.a_bz3,:sqla_kun.a_bz4;
	sqlcode = SQLCODE;
    EXEC SQL close a_kun_curs;
	if ( sqlcode != 0L )
	{
		LogStr( "fetch a_kun_curs: kun: %ld a: %ld SQLCODE: %ld", sqladr.kun, (long) sqllsp.a, sqlcode );
		return 0;
	}
	trim( sqla_kun.a_kun );
	trim( sqla_kun.a_bz1 );
	trim( sqla_kun.a_bz2 );
	trim( sqla_kun.a_bz3 );
	trim( sqla_kun.a_bz4 );
	
	LogStr( "a_kun = %s, ean = %s, a_bz3 = %s", sqla_kun.a_kun, sqla_kun.ean, sqla_kun.a_bz3 );
	
	return 1;
}

void GetLsp( DATA_t *data )
{
    sqllsp.a = (double) atol( data->szAnum );
    if ( ! QueryLsp() )
    {
        strcpy( data->szAnum, "000000" );
        sprintf( data->szAbez, "Art %s nicht auf LS %s.",data->szAnum, data->szLs );
    }
    sprintf( data->szCharge, "%s", sqllsp.ls_charge );
    if ( rfmtdate( sqllsp.hbk_date, "dd.mm.yyyy", data->szMHD ) != 0 )
    {
        LogStr( "rfmtdate(): Konvertierungsfehler: %s\n", data->szMHD );
    }
    LogStr( "MHD = %s Charge = %s", data->szMHD, data->szCharge );

    if ( ! QueryAkun() )
    {
        sprintf( data->szAbez, "K# %s, A#: Keine Daten", data->szKun, data->szAnum );
    }
    sprintf( data->szAkun, "%s", sqla_kun.a_kun );
    sprintf( data->szEAN, "%s", sqla_kun.ean );
    if ( sqla_kun.a_bz3[0] == '\0' || sqla_kun.a_bz3[0] == ' ' )
    {
        sprintf( data->szAbez, "%s %s", sqla_kun.a_bz1, sqla_kun.a_bz2 );
        LogStr( "Abez1,2 = %s", data->szAbez );
    }
    else
    {
        sprintf( data->szAbez, "%s %s", sqla_kun.a_bz3, sqla_kun.a_bz4 );
        LogStr( "Abez3,4 = %s", data->szAbez );
    }
}    

void InsertLsnve( DATA_t *data )
{
    long nRows;
    if ( cursors_initialized == 0 )
    {
          OpenDatabase();
          FormQueries();
    }
    lsnve.mdn = data->iMdn;
    lsnve.fil = 0;
    lsnve.ls = atol(data->szLs);
    lsnve.a = (double) atol( data->szAnum );
    lsnve.nve_posi = 0;
    sprintf( lsnve.nve, "3%s%06ld%03d%d", data->szILN, lsnve.ls, data->iPal, data->iPZNVE );
    strcpy( lsnve.nve_palette, lsnve.nve );
    lsnve.me = data->iStk;
    lsnve.me_einh = 1;
    lsnve.gue = 0;
    lsnve.palette_kz = 0;
	if ( rstrdate( data->szMHD, &(lsnve.mhd) ) != 0 )
	{
        LogStr( "Konvertierungsfehler: %s --> %ld\n", data->szMHD, lsnve.mhd );
	}
	lsnve.brutto_gew = 0.01 * data->iBruttog + data->iBruttokg; // Achtung: Nur 2 Nachkommastellen
	lsnve.verp_art = 0;
	lsnve.ps_art = 0;
	lsnve.ps_stck = 0;
	lsnve.ps_art1 = 0;
	lsnve.ps_stck1 = 0;
	lsnve.masternve[0] = '\0';
    strcpy( lsnve.ls_charge, data->szCharge );
	lsnve.netto_gew = 0.01 * data->iNettog + data->iNettokg;   // Achtung: Nur 2 Nachkommastellen
    
    EXEC SQL BEGIN WORK;
       	
	EXEC SQL INSERT INTO LSNVE (MDN, FIL, NVE_POSI, 
	         NVE, NVE_PALETTE, LS,A, ME, ME_EINH, GUE, PALETTE_KZ, TAG, MHD,
	         BRUTTO_GEW, VERP_ART, PS_ART, PS_STCK, PS_ART1, PS_STCK1,
	         MASTERNVE, LS_CHARGE, NETTO_GEW)
	         VALUES (
	        :lsnve.mdn,
	        :lsnve.fil,
	        :lsnve.nve_posi,
	        :lsnve.nve,
	        :lsnve.nve_palette,
	        :lsnve.ls,
	        :lsnve.a,
	        :lsnve.me,
	        :lsnve.me_einh,
	        :lsnve.gue,
	        :lsnve.palette_kz,
	        today,
	        :lsnve.mhd,
	        :lsnve.brutto_gew,
	        :lsnve.verp_art,
	        :lsnve.ps_art,
	        :lsnve.ps_stck,
	        :lsnve.ps_art1,
	        :lsnve.ps_stck1,
	        :lsnve.masternve,
	        :lsnve.ls_charge,
	        :lsnve.netto_gew
	         );
	        
	if ( SQLCODE != 0)
	{
        EXEC SQL ROLLBACK WORK;
	    LogStr( "ROLLBACK: INSERT INTO LSNVE: SQLCODE=%ld", SQLCODE );
        CloseDatabase();
		exit(1);
	}
	nRows = sqlca.sqlerrd[2];
    EXEC SQL COMMIT WORK;
	LogStr( "COMMIT: INSERT INTO LSNVE: %ld Rows SQLCODE=%ld", nRows, SQLCODE );
}

void SaveOldLsnve( DATA_t *data )
{
    long nRows;
    if ( cursors_initialized == 0 )
    {
          OpenDatabase();
          FormQueries();
    }
    
    lsnve.mdn = data->iMdn;
    lsnve.ls = atol(data->szLs);

    EXEC SQL BEGIN WORK;
       	
	EXEC SQL UPDATE LSNVE SET MDN = 0 - MDN WHERE MDN=:lsnve.mdn AND LS=:lsnve.ls;
	if ( SQLCODE != 0)
	{
        EXEC SQL ROLLBACK WORK;
        CloseDatabase();
		exit(1);
	}
	nRows = sqlca.sqlerrd[2];
    EXEC SQL COMMIT WORK;
	LogStr( "COMMIT: UPDATE LSNVE: MDN=%d, LS=%ld : %ld Rows SQLCODE=%ld\n",
	    lsnve.mdn, lsnve.ls, nRows, SQLCODE );
}
