using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using System.Net;
using System.Threading;


namespace pazxml
{
    public partial class pazxml : Form
    {
        static string logfile = "";
        static Boolean aValueHasChanged; // siehe UpdateNodes
        static byte[] bTableWEUROPE = new byte[256];
        static byte[] bTableEEUROPE = new byte[256];
        static byte[] bTableKYRILLIC = new byte[256];
        static byte[] bTableGREEK = new byte[256];

        string pludir = "";
        string plu0dir = "";

        string ftpUri = "";
        string ftpUri0 = "";
        string ftpUser = "";
        string ftpPwd = "";

        XPathNavigator navConfig;

        private string GetNodeValue(XmlDocument doc, string xpath)
        {
            XPathNavigator nav = doc.CreateNavigator();
            nav.MoveToRoot();
            XPathNodeIterator list = nav.Select(xpath);

            WriteEventLog(String.Format("XPath: {0}", xpath));
            if (list.MoveNext() == false)
            {
                WriteEventLog(String.Format("No Node. XPATH: {0}", xpath));
                return "";
            }
            WriteEventLog(String.Format("{0} Name: {1} Value: {2}",
                     list.Current.NodeType, list.Current.Name, list.Current.Value));
            return list.Current.Value;
        }

        private string GetAttributeValue(XmlDocument doc, string xpath, string attribute)
        {
            XPathNavigator nav = doc.CreateNavigator();
            nav.MoveToRoot();
            XPathNodeIterator list = nav.Select(xpath);

            WriteEventLog(String.Format("XPath: {0}", xpath));
            if (list.MoveNext() == false)
            {
                WriteEventLog(String.Format("No Node. XPATH: {0}", xpath));
                return "";
            }
            XPathNavigator navigator2 = list.Current.Clone();
            navigator2.MoveToFirstAttribute();
            WriteEventLog(String.Format("{0} = {1}", navigator2.Name, navigator2.Value));

            while (navigator2.MoveToNextAttribute())
            {
                WriteEventLog(String.Format("{0} = {1}", navigator2.Name, navigator2.Value));
            }
            WriteEventLog(String.Format("{0} Name: {1} Value: {2}",
                     list.Current.NodeType, list.Current.Name, list.Current.Value));
            //return list.Current.Value;
            return "KYRILLIC";
        }

        private void UpdateNodes(XmlDocument doc, string xpath, string newvalue)
        {
            XPathNavigator nav = doc.CreateNavigator();
            nav.MoveToRoot();
            XPathNodeIterator list = nav.Select(xpath);
            WriteEventLog(String.Format("XPath: {0}", xpath));
            if (list.MoveNext() == false)
            {
                WriteEventLog(String.Format("No Node. XPATH: {0}", xpath));
                return;
            }
            //WriteEventLog(String.Format("{0} Name: {1} Old Value: {2}",
            //   list.Current.NodeType, list.Current.Name, list.Current.Value));
            if (newvalue != list.Current.Value)
            {
                WriteEventLog(String.Format("{0} --> {1}",
                  list.Current.Value, newvalue));
                list.Current.SetValue(newvalue);
                aValueHasChanged = true;
                //WriteEventLog(String.Format("{0} Name: {1} New Value: {2}",
                //   list.Current.NodeType, list.Current.Name, list.Current.Value));
            }
        }

        private void RemovePriceFields(XmlDocument doc)
        {
            XPathNavigator nav = doc.CreateNavigator();
            nav.MoveToRoot();
            string xpath = "/PLU/labels/label[@name='Etikett-oben']/poList";
            XPathNodeIterator list = nav.Select(xpath);
            WriteEventLog(String.Format("XPath: {0}", xpath));
            list.MoveNext();
            //WriteEventLog(String.Format("{0} Name: {1} Old Value: {2}",
            //    list.Current.NodeType, list.Current.Name, list.Current.Value));
            string poList0 = list.Current.Value;
            string poList1 = poList0.Replace(",6,", ",");
            string poList2 = poList1.Replace(",7,", ",");
            string poList3 = poList2.Replace(",8,", ",");
            string poList4 = poList3.Replace("8,", "");
            WriteEventLog(String.Format("poList {0} --> {1}", poList1, poList4));
            list.Current.SetValue(poList4);
            aValueHasChanged = true;

        }

        decimal GetGewichtsklasse(int gwpar, string typ, string name)
        {
            string strValueKg = GetConfigString(String.Format(
                    "/PazXmlConfigParameters/GEWICHTSKLASSEN/GKLASSE[@ID='{0}' and @typ='{1}']/{2}",
                    gwpar, typ, name));
            if (strValueKg.Length < 4)
            {
                return 0;
            }
            string strValue = strValueKg.Replace(" kg", "");
            decimal value = 0;
            try
            {
                value = Decimal.Parse(strValue);
            }
            catch (Exception e)
            {
                WriteEventLog(e.Message);
                value = 0;
            }
            return value;
        }

        decimal GetWeightBandValue(string name, int gwpar, decimal a_gew)
        {
            decimal nettoTol = GetGewichtsklasse(gwpar, "relativ", "NETTOTOLERANZ");

            if (name == "lowerLimit")
            {
                if (nettoTol > (decimal)0.005)
                {
                    WriteEventLog(String.Format("Gewichtsklasse {0}: {1} +/- {2} kg",
                        gwpar, a_gew, nettoTol));
                    return a_gew - nettoTol;
                }
                else
                {
                    decimal nettoMin = GetGewichtsklasse(gwpar, "absolut", "NETTOMIN");
                    WriteEventLog(String.Format("Gewichtsklasse {0}: > {1} kg", gwpar, nettoMin));
                    return nettoMin;
                }
            }

            if (name == "upperLimit")
            {
                if (nettoTol > (decimal)0.005)
                {
                    WriteEventLog(String.Format("Gewichtsklasse {0}: {1} +/- {2} kg",
                       gwpar, a_gew, nettoTol));
                    return a_gew + nettoTol;
                }
                else
                {
                    decimal nettoMax = GetGewichtsklasse(gwpar, "absolut", "NETTOMAX");
                    WriteEventLog(String.Format("Gewichtsklasse {0}: < {1} kg", gwpar, nettoMax));
                    return nettoMax;
                }
            }

            WriteEventLog(String.Format("Gewichtsklasse {0}: Ungültige Eigenschaft", gwpar));
            return 0;
        }

        private Boolean Read(OdbcDataReader r)
        {
            if (r.HasRows == false)
            {
                WriteEventLog(String.Format("Satz nicht vorhanden."));
                r.Close();
                return false;
            }
            r.Read();
            return true;

        }

        private string GetString(OdbcDataReader r, string colname)
        {
            int i = r.GetOrdinal(colname);
            if (r.IsDBNull(i))
            {
                WriteEventLog(String.Format("Null Value. {0}", colname));
                return "";
            }
            string s1 = r.GetString(i);
            string s2 = s1.Trim();
            //WriteEventLog(String.Format("{0} = \"{1}\"", colname, s2));
            return s2;
        }


        private void InitConvTables()
        {
            for (int i = 0; i < 256; i++)
            {
                bTableWEUROPE[i] = Convert.ToByte(i);
                bTableEEUROPE[i] = Convert.ToByte(i);
                bTableKYRILLIC[i] = Convert.ToByte(i);
                bTableGREEK[i] = Convert.ToByte(i);
            }

            string tableFile;
            string line;
            StreamReader sr;

            tableFile = GetConfigString("/PazXmlConfigParameters/CONVFILES/W-EUROPE");
            sr = new StreamReader(tableFile);
            while ((line = sr.ReadLine()) != null)
            {
                if (line[0] == '#')
                    continue;
                string[] split = line.Split(' ', '\t', '=');
                string s0 = split[0];
                string s1 = split[1];
                byte b0 = Byte.Parse(s0);
                byte b1 = Byte.Parse(s1);
                bTableWEUROPE[b0] = b1;
            }
            sr.Close();

            tableFile = GetConfigString("/PazXmlConfigParameters/CONVFILES/E-EUROPE");
            sr = new StreamReader(tableFile);
            while ((line = sr.ReadLine()) != null)
            {
                if (line[0] == '#')
                    continue;
                string[] split = line.Split(' ', '\t', '=');
                string s0 = split[0];
                string s1 = split[1];
                byte b0 = Byte.Parse(s0);
                byte b1 = Byte.Parse(s1);
                bTableEEUROPE[b0] = b1;
            }
            sr.Close();
            
            tableFile = GetConfigString("/PazXmlConfigParameters/CONVFILES/KYRILLIC");
            sr = new StreamReader(tableFile);
            while ((line = sr.ReadLine()) != null)
            {
                if (line[0] == '#')
                    continue;
                string[] split = line.Split(' ', '\t', '=');
                string s0 = split[0];
                string s1 = split[1];
                byte b0 = Byte.Parse(s0);
                byte b1 = Byte.Parse(s1);
                bTableKYRILLIC[b0] = b1;
            }
            sr.Close();

            tableFile = GetConfigString("/PazXmlConfigParameters/CONVFILES/GREEK");
            sr = new StreamReader(tableFile);
            while ((line = sr.ReadLine()) != null)
            {
                if (line[0] == '#')
                    continue;
                string[] split = line.Split(' ', '\t', '=');
                string s0 = split[0];
                string s1 = split[1];
                byte b0 = Byte.Parse(s0);
                byte b1 = Byte.Parse(s1);
                bTableGREEK[b0] = b1;
            }
            sr.Close();
 
            return;
        }

        private string ConvertString(string region, string s0)
        {
            string s1 = "";
            char[] c0 = s0.ToCharArray();
            char c1;
            byte b0 = 0, b1 = 0;
            for (int i = 0; i < c0.Length; i++)
            {
                b0 = Convert.ToByte(c0[i] & 0xFF);
                switch( region )
                {
                    case "W-EUROPE": b1 = bTableWEUROPE[b0]; break;
                    case "E-EUROPE": b1 = bTableEEUROPE[b0]; break;
                    case "KYRILLIC": b1 = bTableKYRILLIC[b0]; break;
                    case "GREEK": b1 = bTableGREEK[b0]; break;
                }
                c1 = (char) b1;
                s1 += c1;
                // WriteEventLog(String.Format("s0[{0}]\t= {1}\t= {2}\t-> {3}\t= {4}", i, c0[i], b0, b1, c1));
            }
            WriteEventLog(String.Format("{0} -> {1}", s0, s1));
            return s1;
        }

        private Int16 GetInt16(OdbcDataReader r, string colname)
        {
            int i = r.GetOrdinal(colname);
            if (r.IsDBNull(i))
            {
                WriteEventLog(String.Format("Null Value. {0} = 0", colname));
                return 0;
            }
            Int16 val = r.GetInt16(i);
            //WriteEventLog(String.Format("{0} = {1}", colname, val));
            return val;
        }

        private Int32 GetInt32(OdbcDataReader r, string colname)
        {
            int i = r.GetOrdinal(colname);
            if (r.IsDBNull(i))
            {
                WriteEventLog(String.Format("Null Value. {0} = 0", colname));
                return 0;
            }
            Int32 val = r.GetInt32(i);
            //WriteEventLog(String.Format("{0} = {1}", colname, val));
            return val;
        }

        private decimal GetDecimal(OdbcDataReader r, string colname)
        {
            int i = r.GetOrdinal(colname);
            if (r.IsDBNull(i))
            {
                WriteEventLog(String.Format("Null Value. {0} = 0", colname));
                return 0;
            }
            Decimal val = r.GetDecimal(i);
            //WriteEventLog(String.Format("{0} = {1}", colname, val));
            return val;
        }

        private string Ean13(decimal ean)
        {
            string s1 = ean.ToString();
            string s2 = s1.PadLeft(2, '0'); // falls s1.Length < 2
            string s3 = s2.Substring(0, 2);

            if (s3 == "28")
            {
                string s4 = s1.PadLeft(7, '0'); // falls s1.Length < 7
                string s5 = s4.Substring(0, 7) + "GG,GGG";
                return s5;
            }
            string s6 = s1.PadLeft(12, '0'); // falls s1.Length < 12
            string s7 = s6.Substring(0, 12);
            return s7;
        }

        XPathNavigator InitNavConfig()
        {
            XmlDocument doc = new XmlDocument();
            string configfile = String.Format("{0}\\pazxml\\pazxml.xml",
                Environment.GetEnvironmentVariable("BWSETC"));
            Console.WriteLine("ConfigFile: {0}", configfile);
            doc.Load(configfile);
            return doc.CreateNavigator();
        }

        private string GetConfigString(string xpath)
        {
            navConfig.MoveToRoot();
            XPathNodeIterator list = navConfig.Select(xpath);
            if (list.MoveNext() == false)
            {
                WriteEventLog(String.Format("No Node. XPATH: {0}", xpath));
                return "";
            }
            WriteEventLog(String.Format("{0} Name: {1} Value: {2}",
                    list.Current.NodeType, list.Current.Name, list.Current.Value));
            return list.Current.Value;
        }

        private void SendFile(string fileName, string uriString, string userName, string passWord)
        {
            // return;

            ManualResetEvent waitObject;
            Uri target = new Uri(String.Format("{0}\\{1}", uriString, Path.GetFileName(fileName)));
            WriteEventLog(String.Format("Sende: --> {0}", target));

            FtpState state = new FtpState();
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(target);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(userName, passWord);

            // Store the request in the object that we pass into the
            // asynchronous operations.
            state.Request = request;
            state.FileName = fileName;

            // Get the event to wait on.
            waitObject = state.OperationComplete;

            // Asynchronously get the stream for the file contents.
            request.BeginGetRequestStream(
                new AsyncCallback(EndGetStreamCallback),
                state);

            // Block the current thread until all operations are complete.
            waitObject.WaitOne();

            // The operations either completed or threw an exception.
            if (state.OperationException != null)
            {
                Console.WriteLine("Es gibt ein ftp Problem: {0}", state.StatusDescription);
                WriteEventLog(String.Format("Es gibt ein ftp Problem: {0}. Status: {1}", target, state.StatusDescription));
                // throw state.OperationException;
                Close();
            }
            else
            {
                Console.WriteLine("The operation completed - {0}", state.StatusDescription);
                WriteEventLog(String.Format("Senden beendet: {0}. Status: {1}", target, state.StatusDescription));
            }
        }

        private static void EndGetStreamCallback(IAsyncResult ar)
        {
            FtpState state = (FtpState)ar.AsyncState;

            Stream requestStream = null;
            // End the asynchronous call to get the request stream.
            try
            {
                requestStream = state.Request.EndGetRequestStream(ar);
                // Copy the file contents to the request stream.
                const int bufferLength = 2048;
                byte[] buffer = new byte[bufferLength];
                int count = 0;
                int readBytes = 0;
                FileStream stream = File.OpenRead(state.FileName);
                do
                {
                    readBytes = stream.Read(buffer, 0, bufferLength);
                    requestStream.Write(buffer, 0, readBytes);
                    count += readBytes;
                }
                while (readBytes != 0);
                Console.WriteLine("Writing {0} bytes to the stream.", count);
                // IMPORTANT: Close the request stream before sending the request.
                requestStream.Close();
                // Asynchronously get the response to the upload request.
                state.Request.BeginGetResponse(
                    new AsyncCallback(EndGetResponseCallback),
                    state
                );
            }
            // Return exceptions to the main application thread.
            catch (Exception e)
            {
                Console.WriteLine("Could not get the request stream.");
                state.OperationException = e;
                state.OperationComplete.Set();
                return;
            }

        }

        // The EndGetResponseCallback method  
        // completes a call to BeginGetResponse.
        private static void EndGetResponseCallback(IAsyncResult ar)
        {
            FtpState state = (FtpState)ar.AsyncState;
            FtpWebResponse response = null;
            try
            {
                response = (FtpWebResponse)state.Request.EndGetResponse(ar);
                response.Close();
                state.StatusDescription = response.StatusDescription;
                // Signal the main application thread that 
                // the operation is complete.
                state.OperationComplete.Set();
            }
            // Return exceptions to the main application thread.
            catch (Exception e)
            {
                Console.WriteLine("Error getting response.");
                state.OperationException = e;
                state.OperationComplete.Set();
            }
        }

        private bool ReceiveFile(string fileName, string uriString, string userName, string passWord)
        {
            Uri serverUri = new Uri(String.Format("{0}\\{1}", uriString, Path.GetFileName(fileName)));
            WriteEventLog(String.Format("Empfange: <-- {0}", serverUri));

            // The serverUri parameter should start with the ftp:// scheme.
            if (serverUri.Scheme != Uri.UriSchemeFtp)
            {
                return false;
            }
            // Get the object used to communicate with the server.
            WebClient request = new WebClient();

            request.Credentials = new NetworkCredential(userName, passWord);
            try
            {
                //byte[] newFileData = request.DownloadData(serverUri.ToString());
                //string fileString = System.Text.Encoding.UTF8.GetString(newFileData);
                //WriteEventLog(fileString);
                File.WriteAllBytes(fileName, request.DownloadData(serverUri.ToString()));
            }
            catch (WebException e)
            {
                WriteEventLog(e.ToString());
                return false;
            }
            return true;
        }

        private OdbcConnection ConnectToDatabase()
        {
            OdbcConnection connect = new OdbcConnection();
            connect.ConnectionString = GetConfigString("/PazXmlConfigParameters/DBConnectionString"); ;
            try
            {
                connect.Open();
            }
            catch (OdbcException e)
            {
                WriteEventLog(String.Format("Es gibt ein ODBC Problem: {0}", e.Message));
                return null;
            }
            return connect;
        }

        public void GetPazData(long a0, long a1)
        {
            OdbcCommand cmd = new OdbcCommand();
            cmd.Connection = ConnectToDatabase();
            cmd.CommandType = CommandType.Text;

            try
            {
                cmd.CommandText = "SET ISOLATION TO DIRTY READ";
                cmd.ExecuteNonQuery();
            }
            catch (OdbcException e)
            {
                WriteEventLog(String.Format("Es gibt ein ODBC Problem: {0}", e.Message));
            }

            long pluCount = 0;
            OdbcDataReader r;

            for (long a = a0; a <= a1; a++)
            {

                WriteEventLog(String.Format("PLU: {0}\r\n", a));
                /* GK 16.08.2013 gebindepalette hinzu */
                cmd.CommandText = String.Format(
                    "SELECT a_typ,a_bz1,a_bz2,a_gew,hbk_ztr,gebindepalette"
                    + " FROM a_bas WHERE a={0}",
                    a);
                r = cmd.ExecuteReader();
                if (Read(r) == false)
                    continue;
                Int16 a_typ = GetInt16(r, "a_typ");
                string a_bz1 = GetString(r, "a_bz1");
                string a_bz2 = GetString(r, "a_bz2");
                decimal a_gew = GetDecimal(r, "a_gew");
                Int16 hbk_ztr = GetInt16(r, "hbk_ztr");
                Int16 gebindepalette = GetInt16(r, "gebindepalette");
                r.Close();
                if (a_typ == 1)
                {
                    cmd.CommandText = String.Format(
                        "SELECT pr_ausz,tara,inh_abverk FROM a_hndw WHERE a={0}",
                        a);
                }
                else
                {
                    cmd.CommandText = String.Format(
                        "SELECT pr_ausz,tara,inh_abverk FROM a_eig WHERE a={0}",
                        a);
                }
                r = cmd.ExecuteReader();
                if (Read(r) == false)
                    continue;
                string pr_ausz = GetString(r, "pr_ausz");
                decimal tara = GetDecimal(r, "tara");
                decimal inh_abverk = GetDecimal(r, "inh_abverk");
                r.Close();
                if (pr_ausz != "J")
                {
                    continue;
                }

                cmd.CommandText = String.Format(
                    "SELECT eti_typ,a_bz3,a_bz4,ean,ean1,ausz_art,ampar,gwpar"
                    + " FROM a_kun_gx"
                    + " WHERE mdn=1 and fil=0 and kun=0 and kun_bran2=\"0\" and a={0}",
                    a);
                r = cmd.ExecuteReader();
                if (Read(r) == false)
                    continue;
                Int16 eti_typ = GetInt16(r, "eti_typ");
                string a_bz3 = GetString(r, "a_bz3");
                string a_bz4 = GetString(r, "a_bz4");
                decimal ean = GetDecimal(r, "ean");
                decimal ean1 = GetDecimal(r, "ean1");
                Int16 ausz_art = GetInt16(r, "ausz_art");
                Int32 gwpar = GetInt32(r, "gwpar");
                r.Close();

                cmd.CommandText = String.Format(
                   "SELECT ld_pr_eu FROM ipr"
                   + " WHERE mdn=1 and pr_gr_stuf=0 and kun_pr=0 and a = {0}",
                   a);
                r = cmd.ExecuteReader();
                decimal ld_pr_eu = 0;
                if (Read(r))
                {
                    ld_pr_eu = GetDecimal(r, "ld_pr_eu");
                }
                r.Close();

                aValueHasChanged = false; // wird in UpdateNodes moeglicherweise true gesetzt

                string plufilename = String.Format("{0}\\{1}.PLU", pludir, a);
                string etifilename = GetConfigString(
                    String.Format(
                        "/PazXmlConfigParameters/VORLAGEN/VORLAGE[@ID={0}]/FILE",
                        eti_typ)
                    );

                XmlDocument doc = new XmlDocument();

                Boolean newFile = false;

                Boolean updateOption = false; // false heisst: von der Vorlage ausgehen
                if (radioPLU.Checked == true)
                {
                    updateOption = true;
                }
                Boolean fileReceived = false;

                if (updateOption)// PLU Datei empfangen
                {
                    fileReceived = ReceiveFile(plufilename, ftpUri, ftpUser, ftpPwd);
                }
                if (fileReceived)
                {
                    WriteEventLog(String.Format("Lade {0}", plufilename));
                    doc.Load(plufilename);

                }
                else // oder Vorlagen Datei empfangen
                {
                    fileReceived = ReceiveFile(etifilename, ftpUri, ftpUser, ftpPwd);
                    if (fileReceived)
                    {
                        WriteEventLog(String.Format("Lade {0}", etifilename));
                        doc.Load(etifilename);
                        aValueHasChanged = true; // auf alle Faelle neue Datei schreiben
                        newFile = true;
                    }
                    else
                    {
                        continue;
                    }
                }
                string region = GetNodeValue(doc,"/PLU/printObjects/printObject[@ID=3]/generalParams/font/@region");
                UpdateNodes(doc, "/PLU/longName", a_bz1);
                UpdateNodes(doc, "/PLU/printObjects/printObject[@ID=3]/data", ConvertString(region,a_bz3));
                UpdateNodes(doc, "/PLU/printObjects/printObject[@ID=9]/data", a_bz4);
                UpdateNodes(doc, "/PLU/printObjects/printObject[@ID=1]/data", Ean13(ean));
                UpdateNodes(doc, "/PLU/printObjects/printObject[@ID=4]/data", Ean13(ean1));
                UpdateNodes(doc, "/PLU/printObjects/printObject[@ID=2]/template",
                    String.Format("%{0}%dd.%mm.%yy", hbk_ztr));

                // Vorgabe Stueckzahl Summe1 (Karton)
                Int32 stkProKarton = (Int32)Decimal.Round(inh_abverk, 0);
                UpdateNodes(doc,
                    "/PLU/totalsCounters/totalsCounter[@name='Z1: Stueck im Karton']/presetValue",
                    String.Format("{0}", stkProKarton));

                string strKartonProPal = GetNodeValue(doc,
                    "/PLU/totalsCounters/totalsCounter[@name='Z2: Kartons auf Pall.']/presetValue");
                // Int32 kartonProPal = Int32.Parse(strKartonProPal);
                // GK 16.08.2013 
                Int32 kartonProPal = gebindepalette;
                UpdateNodes(doc,
                    "/PLU/totalsCounters/totalsCounter[@name='Z2: Kartons auf Pall.']/presetValue",
                    String.Format("{0}", kartonProPal));

                Int32 stkProPal = stkProKarton * kartonProPal;
                WriteEventLog(
                    String.Format("Stueck /Palette: {0} = {1} * {2}", stkProPal, stkProKarton, kartonProPal));

                // Vorgabe Stueckzahl Summe2 (Palette)
                UpdateNodes(doc,
                    "/PLU/totalsCounters/totalsCounter[@name='Z2: Stueck auf Palette']/presetValue",
                    String.Format("{0}", stkProPal));

                // Todo: Finde die weightBand fue Event "Etikett-oben", normalerweise 4
                //PrintTextNodes(doc,
                //    "/PLU/weightBands/weightBand/eventList/eventListItem");

                string weightBandId = "4";

                // Vorgabe Festgewicht
                UpdateNodes(doc,
                    String.Format("/PLU/weightBands/weightBand[@ID={0}]/fixedValue", weightBandId),
                    String.Format("{0}", Decimal.Round(a_gew, 3)));


                UpdateNodes(doc,
                    String.Format("/PLU/weightBands/weightBand[@ID={0}]/tare", weightBandId),
                    String.Format("{0}", Decimal.Round(tara * (decimal)1000, 1)));

                UpdateNodes(doc,
                    String.Format("/PLU/weightBands/weightBand[@ID={0}]/unitPrice", weightBandId),
                    String.Format("{0}", Decimal.Round(ld_pr_eu, 2)));


                decimal lowerLimit = GetWeightBandValue("lowerLimit", gwpar, a_gew);
                UpdateNodes(doc,
                    String.Format("/PLU/weightBands/weightBand[@ID={0}]/lowerLimit", weightBandId),
                    lowerLimit.ToString());
                decimal upperLimit = GetWeightBandValue("upperLimit", gwpar, a_gew);
                UpdateNodes(doc,
                    String.Format("/PLU/weightBands/weightBand[@ID={0}]/upperLimit", weightBandId),
                    upperLimit.ToString());
                
                // fuer das FPVO Modul 20100512 gk
                UpdateNodes(doc,
                    String.Format("/PLU/averageWeights/averageWeight[@name='GWK']/targetWeight"),
                    String.Format("{0}", Decimal.Round(a_gew, 3)));

                UpdateNodes(doc,
                    String.Format("/PLU/averageWeights/averageWeight[@name='GWK']/upperLimit"),
                    upperLimit.ToString());


                if (ausz_art > 0 || gwpar > 0)
                {
                    UpdateNodes(doc,
                        String.Format("/PLU/weightBands/weightBand[@ID={0}]/typeWeight", weightBandId),
                        "fixedValue");

                    UpdateNodes(doc,
                        String.Format("/PLU/weightBands/weightBand[@ID={0}]/typePrice", weightBandId),
                        "fixedValue*unitPrice");
                }
                else
                {
                    UpdateNodes(doc,
                        String.Format("/PLU/weightBands/weightBand[@ID={0}]/typeWeight", weightBandId),
                        "weight");

                    UpdateNodes(doc,
                        String.Format("/PLU/weightBands/weightBand[@ID={0}]/typePrice", weightBandId),
                        "weight*unitPrice");
                }

                /*
                PrintNodes(doc,
                    "/PLU/printObjects/printObject/@*" +
                    "| /PLU/printObjects/printObject/type" +
                    "| /PLU/printObjects/printObject/data");

                PrintNodes(doc, "/PLU/labels/label/@*" +
                    "| /PLU/labels/label/template/@*" +
                    "| /PLU/labels/label/poList");
                */

                if (aValueHasChanged == true)
                {
                    // Aenderungsdatum
                    UpdateNodes(doc,
                        "/PLU/pluParams/date/lastModified", DateTime.Today.ToString(" dd.MM.yy"));

                    if (newFile)
                    {
                        // Erstellungsdatum
                        UpdateNodes(doc,
                            "/PLU/pluParams/date/created", DateTime.Today.ToString(" dd.MM.yy"));
                    }

                    // neue Datei schreiben
                    WriteEventLog(String.Format("Schreibe: {0}", plufilename));
                    doc.Save(plufilename);

                    // und Senden
                    SendFile(plufilename, ftpUri, ftpUser, ftpPwd);

                    // nochmal das gleiche Spiel fuer Preis 0 ...

                    RemovePriceFields(doc);
                    UpdateNodes(doc, "/PLU/printObjects/printObject[@ID=13]/data", "00");

                    // neue Datei schreiben
                    string plu0filename = String.Format("{0}\\{1}.PLU", plu0dir, a);
                    WriteEventLog(String.Format("Schreibe: {0}", plu0filename));
                    doc.Save(plu0filename);

                    // und Senden
                    SendFile(plu0filename, ftpUri0, ftpUser, ftpPwd);

                    pluCount++;
                }
                else
                {
                    WriteEventLog(String.Format("Unveraendert: {0}", plufilename));
                }
            }
            WriteEventLog(
                String.Format(
                  "Sendeauftrag PLU {0} - {1} ausgeführt.\r\n\t"
                  + " Anzahl gesendeter PLU Dateien: 2 x {2}\r\n",
                  a0, a1, pluCount)
                );
        }



        public pazxml()
        {
            InitializeComponent();
        }

        private void pazxml_Load(object sender, EventArgs e)
        {
            navConfig = InitNavConfig();
            logfile = GetConfigString("/PazXmlConfigParameters/LOGFILE");
            label1.Text = String.Format("EventLog ({0})", logfile);
            File.Delete(logfile);

            InitConvTables();

            pludir = GetConfigString("/PazXmlConfigParameters/PLUDIRS/PLUDIR");
            plu0dir = GetConfigString("/PazXmlConfigParameters/PLUDIRS/PLU0DIR");

            ftpUri = GetConfigString("/PazXmlConfigParameters/FTP/URI");
            ftpUri0 = GetConfigString("/PazXmlConfigParameters/FTP/URI0");
            ftpUser = GetConfigString("/PazXmlConfigParameters/FTP/USER");
            ftpPwd = GetConfigString("/PazXmlConfigParameters/FTP/PWD");

            label6.Text = String.Format("Ziel:   {0} ,  {1}", ftpUri, ftpUri0);

        }

        private void Senden_Click(object sender, EventArgs e)
        {
            Int32 a0 = Int32.Parse(a_von.Text);
            Int32 a1 = Int32.Parse(a_bis.Text);
            WriteEventLog(String.Format("\r\n\tSendeauftrag: PLU {0} - {1}\r\n", a0, a1));
            GetPazData(a0, a1);

        }

        private void Beenden_Click(object sender, EventArgs e)
        {
            //Close();
            Application.Exit();
        }

        private void EventLog_TextChanged(object sender, EventArgs e)
        {

        }

        private void WriteEventLog(string text)
        {
            string time = DateTime.Now.ToString("T");
            string line = String.Format("{0}:\t{1}\r\n", time, text);

            if (logfile != "")
            {
                using (StreamWriter sw = File.AppendText(logfile))
                {
                    sw.Write(line);
                }
            }
            if (text.Contains("DBConnectionString"))
            {
                return;
            }
            if (text.Contains("USER"))
            {
                return;
            }
            if (text.Contains("PWD"))
            {
                return;
            }
            EventLog.AppendText(line);
        }
    }

    public class FtpState
    {
        private ManualResetEvent wait;
        private FtpWebRequest request;
        private string fileName;
        private Exception operationException = null;
        string status;

        public FtpState()
        {
            wait = new ManualResetEvent(false);
        }

        public ManualResetEvent OperationComplete
        {
            get { return wait; }
        }

        public FtpWebRequest Request
        {
            get { return request; }
            set { request = value; }
        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }
        public Exception OperationException
        {
            get { return operationException; }
            set { operationException = value; }
        }
        public string StatusDescription
        {
            get { return status; }
            set { status = value; }
        }
    }

}