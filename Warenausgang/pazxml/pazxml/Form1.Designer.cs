﻿namespace pazxml
{
    partial class pazxml
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button Senden;
            System.Windows.Forms.Button Beenden;
            this.a_von = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.a_bis = new System.Windows.Forms.TextBox();
            this.EventLog = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.radioPLU = new System.Windows.Forms.RadioButton();
            this.radioVorlage = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            Senden = new System.Windows.Forms.Button();
            Beenden = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Senden
            // 
            Senden.Location = new System.Drawing.Point(16, 184);
            Senden.Name = "Senden";
            Senden.Size = new System.Drawing.Size(288, 46);
            Senden.TabIndex = 3;
            Senden.Text = "&Senden";
            Senden.UseVisualStyleBackColor = true;
            Senden.Click += new System.EventHandler(this.Senden_Click);
            // 
            // Beenden
            // 
            Beenden.Location = new System.Drawing.Point(456, 184);
            Beenden.Name = "Beenden";
            Beenden.Size = new System.Drawing.Size(287, 48);
            Beenden.TabIndex = 4;
            Beenden.Text = "&Beenden";
            Beenden.UseVisualStyleBackColor = true;
            Beenden.Click += new System.EventHandler(this.Beenden_Click);
            // 
            // a_von
            // 
            this.a_von.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.a_von.ImeMode = System.Windows.Forms.ImeMode.On;
            this.a_von.Location = new System.Drawing.Point(232, 136);
            this.a_von.Margin = new System.Windows.Forms.Padding(6);
            this.a_von.Name = "a_von";
            this.a_von.Size = new System.Drawing.Size(109, 26);
            this.a_von.TabIndex = 1;
            this.a_von.Text = "1";
            this.a_von.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(112, 136);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "PLU  von";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(392, 136);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "bis";
            // 
            // a_bis
            // 
            this.a_bis.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.a_bis.HideSelection = false;
            this.a_bis.ImeMode = System.Windows.Forms.ImeMode.On;
            this.a_bis.Location = new System.Drawing.Point(456, 136);
            this.a_bis.Margin = new System.Windows.Forms.Padding(6);
            this.a_bis.Name = "a_bis";
            this.a_bis.Size = new System.Drawing.Size(100, 26);
            this.a_bis.TabIndex = 2;
            this.a_bis.Text = "1";
            this.a_bis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // EventLog
            // 
            this.EventLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.EventLog.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.EventLog.Location = new System.Drawing.Point(15, 269);
            this.EventLog.Margin = new System.Windows.Forms.Padding(6);
            this.EventLog.Multiline = true;
            this.EventLog.Name = "EventLog";
            this.EventLog.ReadOnly = true;
            this.EventLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.EventLog.Size = new System.Drawing.Size(730, 394);
            this.EventLog.TabIndex = 0;
            this.EventLog.TabStop = false;
            this.EventLog.TextChanged += new System.EventHandler(this.EventLog_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(216, 240);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(95, 24);
            this.label1.TabIndex = 8;
            this.label1.Text = "Event Log";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radioPLU
            // 
            this.radioPLU.AutoSize = true;
            this.radioPLU.Location = new System.Drawing.Point(272, 56);
            this.radioPLU.Name = "radioPLU";
            this.radioPLU.Size = new System.Drawing.Size(110, 28);
            this.radioPLU.TabIndex = 0;
            this.radioPLU.Text = "PLU Datei";
            this.radioPLU.UseVisualStyleBackColor = true;
            // 
            // radioVorlage
            // 
            this.radioVorlage.AutoSize = true;
            this.radioVorlage.Checked = true;
            this.radioVorlage.Location = new System.Drawing.Point(400, 56);
            this.radioVorlage.Name = "radioVorlage";
            this.radioVorlage.Size = new System.Drawing.Size(141, 28);
            this.radioVorlage.TabIndex = 0;
            this.radioVorlage.TabStop = true;
            this.radioVorlage.Text = "Vorlage Datei";
            this.radioVorlage.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(88, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 24);
            this.label4.TabIndex = 11;
            this.label4.Text = "Ursprung:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.radioVorlage);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.radioPLU);
            this.groupBox1.Location = new System.Drawing.Point(16, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(728, 88);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(88, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 24);
            this.label6.TabIndex = 14;
            this.label6.Text = "Ziel";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(200, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(354, 24);
            this.label5.TabIndex = 13;
            this.label5.Text = "Daten an Leich u. Mehl Preisauszeichner";
            // 
            // pazxml
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 678);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(Beenden);
            this.Controls.Add(Senden);
            this.Controls.Add(this.EventLog);
            this.Controls.Add(this.a_bis);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.a_von);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "pazxml";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "PazXml";
            this.Load += new System.EventHandler(this.pazxml_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox a_von;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox a_bis;
        public System.Windows.Forms.TextBox EventLog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioPLU;
        private System.Windows.Forms.RadioButton radioVorlage;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

