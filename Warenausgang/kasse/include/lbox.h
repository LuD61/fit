#ifndef _LBOX_DEF
#define _LBOX_DEF
#include "image.h"

#define LB_CAPTSIZE  8998
#define LB_ROWSIZE   8999
#define LBN_SORT   8997
#define LB_SORT   8996

#define SCDELETE 0
#define SCINSERT 1


void SetMenSelectM (BOOL);
void RegisterLboxM (HWND hWnd);
void SetListFontM (BOOL);
void SetMenStartM (int);
void Settchar (char);
char Gettchar (void);
void SetImage (IMG **);
void SetListFontBefore (HFONT);
void SetSortProc (void (*) (int, HWND));
void EnableSort (BOOL);
BOOL SetAktMen (HWND);
BOOL GetAktMen (HWND);
void SearchList (HWND, char *);
BOOL ScrollList (HWND, int, int);
int DoInsertList (HWND, WPARAM,LPARAM);
int DoLastList (HWND, WPARAM,LPARAM);
int GetLboxText (HWND, int, char *);
#endif
