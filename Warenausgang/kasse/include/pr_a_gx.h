
#include "dbclass.h"
struct PR_AUSZ_GX {
   long      text_nr;
   short     eti_typ;
   short     sg1;
   char      txt1[71];
   short     sg2;
   char      txt2[71];
   short     sg3;
   char      txt3[71];
   short     sg4;
   char      txt4[71];
   short     sg5;
   char      txt5[71];
   short     sg6;
   char      txt6[71];
   short     sg7;
   char      txt7[71];
   short     sg8;
   char      txt8[71];
   short     sg9;
   char      txt9[71];
   short     sg10;
   char      txt10[71];
   short     sg11;
   char      txt11[71];
   short     sg12;
   char      txt12[71];
   short     sg13;
   char      txt13[71];
   short     sg14;
   char      txt14[71];
   short     sg15;
   char      txt15[71];
   short     sg16;
   char      txt16[71];
   short     sg17;
   char      txt17[71];
   short     sg18;
   char      txt18[71];
   short     sg19;
   char      txt19[71];
   short     sg20;
   char      txt20[71];
};
extern struct PR_AUSZ_GX pr_ausz_gx, pr_ausz_gx_null;

#line 3 "pr_a_gx.rh"

class PR_A_GX_CLASS : public DB_CLASS 
{
       private :
               short cursor_eti;
               void prepare (void);
               int prep_awcursor (void);
               int prep_awcursor (char *);
               void fill_aw (int);
               int CallDbAuswahl (int);
               void TestQueryTxt (char *, char *);
               void TestQueryTxtn (char *);

       public :
               PR_A_GX_CLASS () : DB_CLASS ()
               {
                       cursor_eti = -1;
               }
               int dbreadfirst (void);
               int dbreadfirst_eti (void);
               int dbread_eti (void);
               int ShowAllBu (HWND hWnd, int ws_flag);
               int ShowBuQuery (HWND hWnd, int ws_flag, char *);
               void ReadQuery (form *, char *[]);
               int PrepareQuery (char *);
               int PrepareQuery (form *, char *[]);
               int QueryBu (HWND hWnd, int ws_flag);
               int ShowAllBu (HWND hWnd, int, int, 
                              int (*) (void), void (*) (int),
                              char *, int,  char *, int,
                              form *, form *, form *);  
};

class TXTITEM : virtual public ITEM
{
         private :
   	         int GetDBItem (char *item);
         public :
			 TXTITEM (char *itname, char *value, 
				      char *ittext, int itnum) : 
                  ITEM (itname, value, ittext, itnum)
				  {
				  }	   
             void CallInfo ();
};
