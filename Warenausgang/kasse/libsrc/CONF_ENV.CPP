/*** 
--------------------------------------------------------------------------------
-
-       SE.TEC
-       Unternehmensberatung fuer systemische Entwicklung GmbH
-       Eichendorffstr. 33
-
-       78054 VS-Schwenningen
-
-       Tel.: 07720/840145     Fax: 07720/840190
-       E-Mail gvstec@ibm.net
-
--------------------------------------------------------------------------------

 Fuer dieses Programm behalten wir uns alle Rechte, auch fuer den Fall der 
 Patenterteilung und der Eintragung eines anderen gewerblichen Schutz-     
 rechtes vor. Missbraeuchliche Verwendung, wie insbesondere Vervielfael-   
 tigung und Weitergabe an Dritte ist nicht gestattet; sie kann zivil- und  
 strafrechtlich geahndet werden.                                           

--------------------------------------------------------------------------------
-
-       Modulname               :       conf_env.c
-
-       Autor                   :       F.Folmer
-       Erstellungsdatum        :       07.07.95
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BIWAK
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :      
-       Betriebssystem          :       UNIX/OS4680/DOS
-       Sprache                 :       C
-
-       Modulbeschreibung       :       Hilfsprogramm
-					Definition der Environmentvariablen
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
-       1       1.00    09.01.96        F.Folmer  Grundmodul
-       1       1.22    22.08.96        F.Folmer  conf_env lokal fuer Steuerungsprogramme
--------------------------------------------------------------------------------
***/

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#ifdef OS4680
    #define   HOME_VERZEICHNIS  "C:" 
    #define   ENV_DATEI  "CONF_ENV"      /* Datei mit Umgebungsvariablen */
#else
#ifdef UNIX
    #define   ENV_DATEI  "/user/biwak/conf_env.dat" 
    #define   HOME_VERZEICHNIS  "/user/biwak" 
    extern char * getenv ();
#else
   /*  DOS  */
    #define   HOME_VERZEICHNIS  "C:" 
    #define   ENV_DATEI  "C:/BIWAK/ETC/CONF_ENV.DAT"
    extern char * getenv ();
#endif
#endif

#ifdef STEUER_PGM
  extern short testlog;
  void bildschirm_meldung(char *,long);
  void fehler_meldung(long,long,char *);
#endif
  extern short testen;

#define   MAX_ENV    (int) 50

int    env_dat_gelesen=0;    /* Anzahl Environmentvariablen */ 
char * envname[MAX_ENV];
char * envwert[MAX_ENV];
char zeichen = ' ';
char home[64];
char datei [64];

 
/*****  Funktion  config_environment() ******/

char * config_environment(char *name)
{
FILE * fp;
int i;
char buff[256];
char * p;
char * q;
// char *edat;
char *env;


if(env_dat_gelesen == 0)
     {
     /* Environment noch nicht gelesen */

     strcpy (datei, ENV_DATEI);

#ifdef DOS
     if (edat = getenv ("CONF_ENV"))
     {
		  strcpy (datei, edat);
     }
#endif

#ifdef UNIX
     if (edat = getenv ("CONF_ENV"))
     {
		  strcpy (datei, edat);
     }
#endif
     
     fp = fopen(datei,"rb");
     if(fp == NULL)
	 {
	 /* 01.07.97 normal getenv() aufrufen FF */
	 env = getenv(name);
	 if(env != NULL)
	     return (env);
	 /* 01.07.97 normal getenv() aufrufen FF */

#ifdef STEUER_PGM
	 sprintf(buff,"%s nicht vorhanden",datei);
         if(testen != 0 || testlog != 0)
	    fprintf(stderr,"Datei %s\n",buff);
  #ifndef OS4680
	 else
	    printf("Datei %s\n",buff);
  #endif
	 fehler_meldung((long)1,(long)126,buff);
	 bildschirm_meldung(buff,(long)5);
#endif

	 return (NULL);
	 }
     for(i=0;i<MAX_ENV;i++)
	 {
	 memset(buff,0x00,256);
	 p = fgets(buff,256,fp);
	 if(p == NULL)
	    break;
	 q = buff;
	 while (*q == ' ')
	     q++;
	 strcpy(buff,q);
	 q = strchr(buff,0x0d);
	 if(q != NULL)
	     *q = '\0';
	 q = strchr(buff,0x0a);
	 if(q != NULL)
	     *q = '\0';
	 q = strchr(buff,'=');
	 if(q == NULL)
	      {
	      q = strchr(buff,' ');
	      if(q == NULL)
		   {
		   printf("Datei %s fehlerhaft\n",ENV_DATEI);
		   return (NULL);
		   }
	      }
	 *q = '\0';
	 q++;
	 envwert[i] = (char *) malloc (strlen(q) + 1);
	 if(envwert[i] == NULL)
	      {
	      printf("malloc Fehler\n");
	      return (NULL);
	      }
	 while (*q == ' ')
	      q++;
	 strcpy(envwert[i],q);

  p = &envwert[i][0];
  if (*p == '\"')
  {
	zeichen = '\"';
	strcpy (envwert[i], p + 1);
  }
  else
  {
	zeichen = ' ';
  }
  p = strchr(envwert[i],zeichen);
	 if(p != NULL)
	      *p = '\0';
	 envname[i] = (char *) malloc (strlen(buff) + 1);
	 if(envname[i] == NULL)
	      {
	      printf("malloc Fehler\n");
	      return (NULL);
	      }
	 strcpy(envname[i],buff);
	 p = strchr(envname[i],' ');
	 if(p != NULL)
	      *p = '\0';
	 }/* end of for */

     env_dat_gelesen = i;
     fclose(fp);
     }/* end of if()*/


for (i=0;i<env_dat_gelesen;i++)
     {
     if(strcmp(name,envname[i]) == 0)
     {
	   return(envwert[i]);
     }
     }/* end of for */

/* 01.07.97 normal getenv() aufrufen FF */
env = getenv(name);
if(env != NULL)
     return (env);
/* 01.07.97 normal getenv() aufrufen FF */

return(NULL);
} /* end of config_environment() */
