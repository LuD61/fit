#include <windows.h>
#include <stdio.h>
#include "KasseDlg.h"
#include "mo_arg.h"
#include "strfkt.h"
#include "mo_meld.h"
#include "colbut.h"
#include "mo_progcfg.h"
#include "cmask.h"
#include "mo_vers.h"
#include "help.h"
#include "stdfkt.h"
#include "mo_curso.h"
#include "mo_menu.h"
#ifdef DLLMAIN
#include "kasse.h"
#endif
#ifdef LIBMAIN
#include "kasse.h"
#endif


extern void UnSetRdoMargins ();

static DLG *BaseDlg;
static HWND BaseWindow;
static int Size = 120;

static KasseDlg *Dlg;
#ifdef LIBMAIN
static HWND hMainWindow;
static HINSTANCE hMainInst;
#else
HWND hMainWindow;
HINSTANCE hMainInst;
#endif
static long kun = 0;
static long rech = 0;
BOOL Rechuebergabe = FALSE;

static HWND mamain1;

static int DlgY = 1;


static void tst_arg (char *arg)
{

		  Rechuebergabe = FALSE;
          for (; *arg; arg += 1)
          {
              switch (*arg)
              {
			        case 'K' :
						if (memcmp (arg, "Kunde=", 6) == 0)
						{
							kun = atol (arg + 6);
							return;
						}
						break;
			        case 'R' :
						if (memcmp (arg, "Rech=", 5) == 0)
						{
							rech = atol (arg + 5);
							Rechuebergabe = TRUE;
							return;
						}
						break;
              }
          }

          return;
}

static void SetCubBk (COLORREF BkColor)
{
/*
	  int i;

	  for (i = 0; CubTab[i] != NULL; i ++)
	  {
		  CubTab [i]->BkColor = BkColor;
	  }
*/
}

static PROG_CFG ProgCfg ("kasse");
static COLORREF SysBackground = LTGRAYCOL;
static COLORREF Background = -1;
static COLORREF HelpBackground = DKYELLOWCOL;
static int BorderType = RAISEDBORDER;
static char Bitmap[256] = {"NULL"};
static int Bitmapmode = 1;
static BOOL DockMenue = FALSE;
static int Width = 60;
static int Height = 24;


static void GetCfgColor (COLORREF *color, char *cfg_v)
/**
Farbunterlegung fuer Listen uebertragen.
**/
{
	static char *ColTxt[] = {"BLACKCOL",
		                     "WHITECOL",
							 "BLUECOL",
							 "REDCOL",
							 "LTGRAYCOL",
							 "GREENCOL",
							 "YELLOWCOL",
							 "DKYELLOWCOL",
							 NULL};
	
	static COLORREF ColVal[] = {BLACKCOL,
		                        WHITECOL,
					  		    BLUECOL,
							    REDCOL,
							    LTGRAYCOL,
							    GREENCOL,
							    YELLOWCOL,
								DKYELLOWCOL};
	int i;
	int red, blue, green;
	char *ColR;
	int Cols = 0;

	for (i = 0; ColTxt[i]; i ++)
	{
		if (strcmp (ColTxt[i], cfg_v) == 0)
		{
			*color = ColVal[i];
			return;
		}
	}


	ColR = strstr (cfg_v, "red");
	if (ColR)
	{
		red = atoi (&ColR[3]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "blue");
	if (ColR)
	{
		blue = atoi (&ColR[4]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "green");
	if (ColR)
	{
		green = atoi (&ColR[5]);
		Cols ++;
	}
	if (Cols == 3)
	{
		*color = RGB (red, green, blue);
	}

}


static void GetCfgValues (void)
/**
Werte aus artpfleg.cfg holen.
**/
{

	   static BOOL cfgOK = FALSE;
       char cfg_v [256];

	   if (cfgOK) return;

	   cfgOK = TRUE;

       if (ProgCfg.GetCfgValue ("TouchPlus", cfg_v) == TRUE)
       {
		       KasseDlg::tp = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("Background", cfg_v) == TRUE)
       {
		             GetCfgColor (&Background, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("Width", cfg_v) == TRUE)
       {
		       Width = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("Height", cfg_v) == TRUE)
       {
		       Height = atoi (cfg_v);
	   }

}

static void GetDockParams (HWND hWnd)
{
       char *etc;
       char buffer [512];
       FILE *fp;
       int anz;
  	   RECT rect;

       if (DockMenue == FALSE) return;

       etc = getenv ("BWSETC");
       if (etc == NULL) return;

       sprintf (buffer, "%s\\fit.rct", etc); 
       fp = fopen (buffer, "r");
       if (fp == NULL) return;
       if (fgets (buffer, 511,fp) == 0)
       {
           fclose (fp);
           return;
       }
       if (fgets (buffer, 511,fp) == 0)
       {
           fclose (fp);
           return;
       }
       fclose (fp);
       anz = wsplit (buffer, " ");
       if (anz < 4) return;
       rect.left   = atoi (wort[0]);
       rect.top    = atoi (wort[1]);
       rect.right  = atoi (wort[2]);
       rect.bottom = atoi (wort[3]);
  	   rect.left ++; 
	   rect.top ++; 
	   rect.right  = rect.right  - rect.left - 2;
	   rect.bottom = rect.bottom - rect.top - 2;
       MoveWindow (hMainWindow, rect.left, rect.top, rect.right, rect.bottom, TRUE);
}


#ifdef DLLMAIN
EXPORT int WINAPI StartKasse(HANDLE hInstance, LPSTR lpszCmdLine,int nCmdShow)
#elif LIBMAIN
int WINAPI StartKasse(HANDLE hInstance, LPSTR lpszCmdLine,int nCmdShow)
#else
int    WINAPI WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
#endif
{
       char **varargs = NULL;
	   int i, anz;
       double scrfcx = 1.0; 
       double scrfcy = 1.0;
       int xfull, yfull;
	   RECT rect;
//       extern void on_dberr (int (*) (void));
#ifndef LIBMAIN
       opendbase ("bws");
	   LoadLibrary ("RICHED32.DLL");
#endif
	   if (Background == -1)
	   {
	           Background = GetSysColor (COLOR_3DFACE);
	   }
	   SysBackground = GetSysColor (COLOR_3DFACE);
	   KasseDlg::SysBkColor = SysBackground;
	   SetCubBk (SysBackground);

       GetCfgValues ();
       xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
       yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

       if (xfull > 800)
       {
            scrfcx = (double) xfull / 800; 
            scrfcy = (double) yfull / 562; 
       }

       if (xfull > 800)
       {
            scrfcx = (double) xfull / 800; 
            scrfcy = (double) yfull / 562; 
       }


	   kun = 0l;
	   rech = 0l;
       anz = wsplit (lpszCmdLine, " ");

       if (anz)
       {
	      varargs = new char * [anz]; 
             for (i = 0; i < anz; i ++)
             {
				   varargs[i] = new char [strlen (wort[i]) + 1]; 
                   strcpy (varargs[i], wort[i]);
             }
            argtst (&anz, varargs, tst_arg);
	   }


	   MENUE_CLASS *menue_class = new MENUE_CLASS ();
	   if (anz > 0)
	   {
                   menue_class->Lesesys_ben_pers (varargs[0]);
	   }
	   delete menue_class;

       hMainInst = hInstance;
       if (xfull > 800)
       {
//               BaseDlg = new DLG (-1, -1, 60, 24, "Kasse", Size, FALSE);
               BaseDlg = new DLG (-1, -1, Width, Height, "Kasse", Size, FALSE);
       }
       else if (xfull > 700)
       {
              BaseDlg = new DLG (-1, -1, 700, 560, "Kasse", Size, TRUE);
       }
       else
       {
              BaseDlg = new DLG (-1, -1, 640, 450, "Kasse", Size, TRUE);
       }
       BaseDlg->ScreenParam (scrfcx, scrfcy);
/*
       BaseDlg->SetStyle (WS_VISIBLE | WS_POPUP | 
                      WS_THICKFRAME | WS_CAPTION | WS_SYSMENU
				      | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);
*/

       BaseDlg->SetStyle (WS_VISIBLE | WS_POPUP | 
                      WS_THICKFRAME | WS_CAPTION | WS_SYSMENU
				      | WS_MINIMIZEBOX);

//       BaseDlg->SetStyle (WS_VISIBLE | WS_POPUP | 
//                      WS_THICKFRAME | WS_CAPTION);
       BaseDlg->SetWinBackground (SysBackground);
    
       hMainWindow = BaseDlg->OpenWindow (hInstance, NULL);
	   if (rech > 0) 
	   {
		   ShowWindow(hMainWindow, SW_SHOWMAXIMIZED);
		   KasseDlg::tp = 120;
	   }


	   UnSetRdoMargins ();
	   GetClientRect (hMainWindow, &rect);
       mamain1 = hMainWindow;
       if (xfull > 1000)
       {
              GetDockParams (hMainWindow);
       }
     
       DLG::hInstance = hMainInst;
	   if (rech != 0l)
	   {
		   KasseDlg::F5Enabled = FALSE;
	   }
	   else
	   {
		   KasseDlg::F5Enabled = TRUE;
	   }

       Dlg = new KasseDlg (1, DlgY, -1, -1, "Kasse", 105, FALSE,
                             BorderType, 0);

       { 
               Dlg->OpenMess (hMainWindow);
               Dlg->PrintMess ("Ready");
       } 

       Dlg->ScreenParam (scrfcx, scrfcy);

       Dlg->SetStyle (WS_VISIBLE | WS_CHILD | WS_BORDER);
//       Dlg->SetStyleEx (WS_EX_CLIENTEDGE);
       if (Background != NULL)
       {
              Dlg->SetWinBackground (Background);
       }
       if (strcmp (Bitmap, "NULL"))
       {
             Dlg->ReadBmp (hMainWindow, Bitmap, 0, 0);
             Dlg->SetBitmapmode (Bitmapmode);
       }


       BaseWindow = Dlg->OpenScrollWindow (hInstance, hMainWindow);

       LockWindowUpdate (NULL);
	   if (kun != 0l)
	   {
		   Dlg->SetKun (kun);
	   }
	   else if (rech != 0l)
	   {
		   Dlg->SetRech (rech);
	   }
	   Dlg->ProcessMessages ();
       delete Dlg;
       DestroyWindow (hMainWindow);
       delete BaseDlg;
#ifndef DLLMAIN
#ifndef LIBMAIN
	   closedbase ();
#endif
#endif
	   for (i = 0; i < anz; i ++)
	   {
		   delete varargs[i];
	   }
	   if (varargs != NULL)
	   {
	       delete varargs;
	   }
       return 0;
}
       
