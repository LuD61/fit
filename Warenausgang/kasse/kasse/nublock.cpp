#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include<winbase.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_draw.h"
#include "wmask.h"
#include "mo_meld.h"
#include "nublock.h" 

#define VK_CLS 910

static BOOL CalcOn = FALSE; 

static HANDLE    hInstance = NULL;
static HANDLE    hMainInst = NULL;
static HWND      hMainWindow = NULL;
static HWND      ParentWindow;
static HWND NumEnterWindow;

static HBITMAP   pfeill = NULL;
static HBITMAP   pfeilo = NULL;
static HBITMAP   pfeilu = NULL;
static HBITMAP   pfeilr = NULL;
static HWND eNumWindow  = NULL;
static double scrfx = 1.0;
static double scrfy = 1.0;

static NUBLOCK *ThisNuBlock = NULL;


// LONG FAR PASCAL EntNuProc(HWND,UINT, WPARAM,LPARAM);

static int (*eFormProc) (WPARAM) = NULL;

static mfont EditNumFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};


static mfont TextNumFont = {"Courier New", 200, 0, 1, BLACKCOL,
                                         LTGRAYCOL,
                                         1,
                                         NULL};

static mfont numfieldfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

static mfont ctrlfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

static mfont stornofont = {"", 150, 0, 1,
                                       RGB (255, 0, 0),
                                       BLUECOL,
                                       1,
                                       NULL};

static mfont choisefont = {"", 150, 0, 1,
                                       WHITECOL,
                                       RGB (120, 120, 0),
//                                       BLUECOL,
                                       1,
                                       NULL};

static mfont OKfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

static mfont Cafont = {"", 90, 0, 1,
                                       RGB (255, 255, 255),
                                       REDCOL,
                                       1,
                                       NULL};

static mfont ausweinhfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

static ColButton Num1      =   {"1", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton Num2      =   {"2", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton Num3      =   {"3", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton Num4      =   {"4", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton Num5      =   {"5", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton Num6      =   {"6", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton Num7      =   {"7", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton Num8      =   {"8", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton Num9      =   {"9", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton Num0      =   {"0", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton NumP     =   {",", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton NumM     =   {"-", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};


static ColButton NumLeft   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeill, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton NumRight   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilr, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton NumUp   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilo, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton NumDown   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilu, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton NumDel     =   {"DEL", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

static ColButton NumPlus   =   {"+", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton NumMinus   =   {"-", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

static ColButton NumCls    =   {"C", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

static ColButton NumAbbruch  =   {"Abbruch", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         0};

static ColButton NumOK       =   {"OK", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         BLACKCOL,
                         GREENCOL,
                         0};

static ColButton BuStorno =   {"&Storno", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

static ColButton BuChoise =   {"&Auswahl", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         RGB (120,120, 0),
                         14};

static ColButton AuswahlEinh1= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};

static ColButton AuswahlEinh2= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};

static ColButton AuswahlEinh3= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};
static ColButton AuswahlEinh4= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};
static ColButton AuswahlEinh5= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};
static ColButton AuswahlEinh6= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};
static ColButton AuswahlEinh7= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};

static int nbsb  = 85;

static int nbsx   = 5;
static int ny    = 10;
static int nbss0 = 64;
static int nbss  = 64; 
static int nbsz  = 40;
static int nbsz0 = 64;
static int nAktButton = 0;

static int donum1 ();
static int donum2 ();
static int donum3 ();
static int donum4 ();
static int donum5 ();
static int donum6 ();
static int donum7 ();
static int donum8 ();
static int donum9 ();
static int donum0 ();
static int donump ();
static int donumm ();

static int doleft ();
static int doright ();
static int doup ();
static int dodown ();
static int dodel ();
static int doabbruch ();
static int doOK ();
static int doplus ();
static int dominus ();
static int docls ();

static field _NumField[] = {
(char *) &Num1,          nbss0, nbsz0, ny, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum1, 0,
(char *) &Num2,          nbss0, nbsz0, ny, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum2, 0,
(char *) &Num3,          nbss0, nbsz0, ny, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum3, 0,
(char *) &Num4,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum4, 0,
(char *) &Num5,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum5, 0,
(char *) &Num6,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum6, 0,
(char *) &Num7,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum7, 0,
(char *) &Num8,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum8, 0,
(char *) &Num9,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum9, 0,
(char *) &Num0,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donum0, 0,
(char *) &NumP,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donump, 0,
(char *) &NumM,          nbss0, nbsz0, ny, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donumm, 0};

static form NumField = {12, 0, 0, _NumField, 0, 0, 0, 0, &numfieldfont};


static int cbss0 = 64;
static int cbss  = 64; 
static int cbsz  = 40;
static int cbsz0 = 64;
static int cbsx = nbsx + 5 * nbss0;

static field _NumControl1[] = {
(char *) &NumLeft,       cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doleft, 0,
(char *) &NumRight,       cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doright, 0,
(char *) &NumDel,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           dodel, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl1 = {3, 0, 0, _NumControl1, 0, 0, 0, 0, &ctrlfont};

static field _NumControl2[] = {
(char *) &NumPlus,       cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doplus, 0,
(char *) &NumMinus,       cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dominus, 0,
(char *) &NumCls,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           docls, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl2 = {3, 0, 0, _NumControl2, 0, 0, 0, 0, &ctrlfont};

static field _NumControl3[] = {
(char *) &NumUp,          cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doup, 0,
(char *) &NumDown,        cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dodown, 0,
(char *) &NumCls,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           docls, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl3 = {3, 0, 0, _NumControl3, 0, 0, 0, 0, &ctrlfont};

static form NumControl;


static field _CaControl[] = {
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};
static form CaControl = {1, 0, 0, _CaControl, 0, 0, 0, 0, &Cafont};

static field _OKControl[] = {
(char *) &NumOK,         cbss0 * 2,
                                cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                             doOK, 0};
static form OKControl = {1, 0, 0, _OKControl, 0, 0, 0, 0, &OKfont};


static char editbuff [40];
static double editsum;
static char eaction = ' ';

static field _EditForm [] = {
editbuff,             0, 0, 0, 0, 0, "", EDIT, 0, 0, 0};

static form EditForm = {1, 0, 0, _EditForm, 0, 0, 0, 0, &EditNumFont};


static field _TextForm [] = {
editbuff,             0, 0, 0, 0, 0, "", READONLY, 0, 0, 0};

static form TextForm = {1, 0, 0, _TextForm, 0, 0, 0, 0, &TextNumFont};


static field _fAuswahlEinh [] = {
(char *) &AuswahlEinh1,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh2,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh3,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh4,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh5,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh6,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh7,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
};

static form fAuswahlEinh = {7, 0, 0, _fAuswahlEinh, 0, 0, 0, 0, &ausweinhfont};


static field _StornoForm [] = {
(char *) &BuStorno,       100, 64, 0, 0, 0, "", COLBUTTON, 0, 0, 0};

static form StornoForm = {1, 0, 0, _StornoForm, 0, 0, 0, 0, &stornofont};



static field _ChoiseForm [] = {
(char *) &BuChoise,       100, 64, 0, 0, 0, "", COLBUTTON, 0, 0, 0};

static form ChoiseForm = {1, 0, 0, _ChoiseForm, 0, 0, 0, 0, &choisefont};

LONG FAR PASCAL IntEntNuProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
                    if (ThisNuBlock)
                    {
                        ThisNuBlock->OnPaint (hWnd, msg, wParam, lParam);
                    }
                    break;

              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    if (ThisNuBlock)
                    {
                        ThisNuBlock->OnButton (hWnd, msg, wParam, lParam);
                    }
                    return TRUE;
               case WM_COMMAND : 
			   {
                    if (ThisNuBlock)
                    {
                        ThisNuBlock->OnCommand (hWnd, msg, wParam, lParam);
                    }
			   }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

static int donum1 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '1');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '1', 0l);
        return 1;
}

static int donum2 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '2');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '2', 0l);
        return 1;
}

static int donum3 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '3');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '3', 0l);
        return 1;
}

static int donum4 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '4');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '4', 0l);
        return 1;
}

static int donum5 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '5');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '5', 0l);
        return 1;
}

static int donum6 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '6');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '6', 0l);
        return 1;
}

static int donum7 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '7');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '7', 0l);
        return 1;
}

static int donum8 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '8');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '8', 0l);
        return 1;
}

static int donum9 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '9');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '9', 0l);
        return 1;
}

static int donum0 ()
/**
Aktion bei Button ,
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '0');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '0', 0l);
        return 1;
}

static int donump ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '.');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '.', 0l);
        return 1;
}

static int donumm ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '-');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '-', 0l);
        return 1;
}


static int doleft ()
/**
Aktion bei Button links
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_LEFT);
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_LEFT, 0l);
        return 1;
}

static int doright ()
/**
Aktion bei Button rechts
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_RIGHT);
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RIGHT, 0l);
        return 1;
}

static int doup ()
/**
Aktion bei Button links
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_UP);
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_LEFT, 0l);
        return 1;
}

static int dodown ()
/**
Aktion bei Button links
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_DOWN);
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_LEFT, 0l);
        return 1;
}

static int dodel ()
/**
Aktion bei Button rechts
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_DELETE);
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_DELETE, 0l);
        return 1;
}


static int docls (void)
/**
EditFenster loeschen.
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_CLS);
		}
        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
		editsum = (double) 0.0;
		strcpy (editbuff, "");	
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

static int doplus (void)
/**
EditFenster loeschen.
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '+');
		}
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}
		eaction = '+';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

static int dominus (void)
/**
EditFenster loeschen.
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '-');
		}
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}
			    
		eaction = '-';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

static int dogleich (void)
/**
EditFenster loeschen.
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '=');
		}
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}
			    
		eaction = ' ';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

static int doabbruch ()
/**
Aktion bei Button F5
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_F5);
		}
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_F5, 0l);
        return 1;
}

static int doOK ()
/**
Aktion bei Button rechts
**/
{
	    if (CalcOn && eaction != ' ')
		{
			      dogleich ();  
				  return 1;
		}
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_RETURN);
		}
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RETURN, 0l);
        return 1;
}


void NUBLOCK::SetNuBlock (void)
{
    ThisNuBlock = this;
}

void NUBLOCK::MainInstance (HANDLE MainInst, HWND hWnd)
{
	    hInstance   = MainInst;
	    hMainInst   = MainInst;
		hMainWindow = hWnd;
}

void NUBLOCK::ScreenParam (double scrx, double scry)
{
	    scrfx = scrx;
		scrfy = scry;
}


void NUBLOCK::SetStorno (int (*sproc) (void), int BuId)
/**
Procedure fuer Storno Setzen.
**/
{
           StornoForm.mask[0].after = sproc;
           StornoForm.mask[0].BuId  = BuId;
}

void NUBLOCK::SetChoise (int (*sproc) (void), int BuId)
/**
Procedure fuer Storno Setzen.
**/
{
           ChoiseForm.mask[0].after = sproc;
           ChoiseForm.mask[0].BuId  = BuId;
}

void NUBLOCK::NumEnterBreak ()
/**
Endeflag fuer Erfassung setzen.
**/
{
        break_num_enter = 1;
}

HWND NUBLOCK::IsNumChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < NumField.fieldanz; i ++)
        {
                   if (MouseinWindow (NumField.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumField.mask[i].feldid);
                   }
        }

        for (i = 0; i < NumControl.fieldanz; i ++)
        {
                   if (MouseinWindow (NumControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < OKControl.fieldanz; i ++)
        {
                   if (MouseinWindow (OKControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (OKControl.mask[i].feldid);
                   }
        }
        for (i = 0; i < CaControl.fieldanz; i ++)
        {
                   if (MouseinWindow (CaControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (CaControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < EditForm.fieldanz; i ++)
        {
			       if (EditForm.mask[i].feldid == NULL) break; 
                   if (MouseinWindow (EditForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (EditForm.mask[i].feldid);
                   }
        }
        if (StornoForm.mask[0].feldid == 0)
        {
                   return FALSE;
        }

        for (i = 0; i < StornoForm.fieldanz; i ++)
        {
                   if (MouseinWindow (StornoForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (StornoForm.mask[i].feldid);
                   }
        }
        if (ChoiseForm.mask[0].feldid == 0)
        {
                   return FALSE;
        }

        for (i = 0; i < ChoiseForm.fieldanz; i ++)
        {
                   if (MouseinWindow (ChoiseForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (ChoiseForm.mask[i].feldid);
                   }
        }
        return NULL;
}


void NUBLOCK::RegisterNumEnter (WNDPROC EntNuProc)
/**
Fenster fuer numerische Eingabe registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  EntNuProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 120));
                   wc.lpszClassName =  "EntNum";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

int NUBLOCK::IsNumKey (MSG *msg)
/**
HotKey Testen.
**/
{
	     POINT mousepos;
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         ColButton *ColBut;
         int taste;
         char *pos;
		 int i;
         
         GetCursorPos (&mousepos);

         switch (msg->message)
         {
	          case EM_SETSEL :
				  break;   
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    NumEnterBreak ();
                                    syskey = KEY5;
                                    return TRUE;
                            case VK_DELETE :
								     docls ();
									 return TRUE;
                            case VK_LEFT :
								    if (eFormProc)
									{
			                            (*eFormProc) ((WPARAM) VK_UP);
										return TRUE;
									}
									break;
                            case VK_RIGHT :
								    if (eFormProc)
									{
			                            (*eFormProc) ((WPARAM) VK_DOWN);
										return TRUE;
									}
									break;
                            case VK_DOWN :
								    if (eFormProc && ehWnd)
									{
										SendMessage (ehWnd, WM_COMMAND, KEYDOWN, 0l);
										return TRUE;
									}
									break;

                            case VK_UP :
								    if (eFormProc && ehWnd)
									{
										SendMessage (ehWnd, WM_COMMAND, KEYUP, 0l);
										return TRUE;
									}
                                    break;
							    
                            case VK_RETURN :
								    if (CalcOn && eaction != ' ')
									{
										dogleich ();
										return TRUE;
									}
               					    if (eFormProc)
									{
//                                            (*eFormProc) ((WPARAM) VK_RETURN);
                                            NumEnterBreak ();
                                            syskey = KEYCR;
                                            return TRUE;
									}

                                    NumEnterBreak ();
                                    GetWindowText (EditForm.mask[0].feldid,
                                                   EditForm.mask [0].feld,
                                                   EditForm.mask [0].length);
                                    syskey = KEYCR;
                                    return TRUE;
                     }
                     taste = (int) msg->wParam;
                     ColBut = (ColButton *) StornoForm.mask[0].feld;
                     if (ColBut->text1)
                     {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = StornoForm.mask[0].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                     }
                     ColBut = (ColButton *) ChoiseForm.mask[0].feld;
                     if (ColBut->text1)
                     {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = ChoiseForm.mask[0].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                     }
                     return FALSE;
              }
              case WM_KEYUP :
              {
                         taste = (int) msg->wParam;
                         ColBut = (ColButton *) StornoForm.mask[0].feld;
                         if (ColBut->text1)
                         {
                             if ((pos = strchr (ColBut->text1, '&')) &&
                                ((UCHAR) toupper (*(pos + 1)) == taste))
                             {
                                   if (keyhwnd != StornoForm.mask[0].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = StornoForm.mask[0].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                         ColBut = (ColButton *) ChoiseForm.mask[0].feld;
                         if (ColBut->text1)
						 {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = ChoiseForm.mask[0].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
						 }
                         return FALSE;
              }
              case WM_CHAR :
              {
				     switch (msg->wParam)
					 {
					       case '+' :
								     doplus ();
									 return TRUE;
						   case '-' :
								     dominus ();
									 return TRUE;
					 }
				     if (eFormProc)
					 {

// Keine WM_CHAR-Meldung an eNumWindow. Die Werte kommen sondt doppelt.
						 
 	                        if (msg->hwnd == eNumWindow)return TRUE; 
						    (*eFormProc) (msg->wParam);
                            return TRUE;
					 }
					 else
					 {
                            SendMessage (EditForm.mask[0].feldid,
                                   msg->message,
                                   msg->wParam,
                                   msg->lParam);
                                   return TRUE;
					 }
               }
               case WM_LBUTTONDOWN : 
			   {
		             if (MeLPProc)
					 {
						      for (i = 0; i < fAuswahlEinh.fieldanz; i ++)
							  {
			                        if (MouseinWindow (fAuswahlEinh.mask[i].feldid, &mousepos))
									{
										          currentfield = i;
								                  return FALSE;
									}
							  }
					 }
			   }
			   break;
          }
          return FALSE;
}

void NUBLOCK::KorrMainButton (form * MainButton, double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    int i;

		for (i = 0; i < MainButton->fieldanz; i ++)
		{
			if (MainButton->mask[i].pos[0] != -1)
			{
			     MainButton->mask[i].pos[0] = (short) (double) 
					 ((double) MainButton->mask[i].pos[0] * fcy);
			}
			if (MainButton->mask[i].pos[1] != -1)
			{
			     MainButton->mask[i].pos[1] = (short) (double) 
					 ((double) MainButton->mask[i].pos[1] * fcx);

			}
	        MainButton->mask[i].rows = (short) (double) 
			 ((double) MainButton->mask[i].rows * fcy);
	        MainButton->mask[i].length = (short) (double) 
			 ((double) MainButton->mask[i].length * fcx);
		}
}


void NUBLOCK::SetParent (HWND Parent)
/**
Paren-Fenster setzen.
**/
{
	     ParentWindow = Parent;
}

void NUBLOCK::Enable (BOOL mode)
/**
Paren-Fenster setzen.
**/
{
	     EnableWindow (eNumWindow, mode);
}

void NUBLOCK::SetText (char *text, BOOL disp)
{
        if (eNumWindow == NULL) return;
        strcpy (editbuff, text);
        if (disp)
        {
            display_form (eNumWindow, &EditForm, 0, 0);
        }
}


void NUBLOCK::EnterNumBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic,
                           WNDPROC EntNuProc)
/**
Fenster fuer numerische Eingabe oeffnen.
**/
{
        RECT rect, rect1;
        MSG msg;
        TEXTMETRIC tm;
        HFONT hFont;
        HDC hdc;
        int x, y, cx, cy;
        int xfull, yfull;
        int i, j;
        int tlen;
        int elen, epos;
        int eheight;
        HCURSOR oldcursor;
        static int BitMapOK = 0;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;
		static BOOL NumKorr = FALSE; 
		DWORD border;

		if (ParentWindow == NULL) ParentWindow = hWnd;
        if (EntNuProc == NULL) EntNuProc = IntEntNuProc;

		if (NumKorr == FALSE)
		{
			      NumKorr = TRUE;
				  nbsz0 = (int) (double) ((double) nbsz0 * scrfy);
				  nbss0 = (int) (double) ((double) nbss0 * scrfx);
		          KorrMainButton (&NumField, scrfx, scrfy);
		          KorrMainButton (&NumControl1, scrfx, scrfy);
		          KorrMainButton (&NumControl2, scrfx, scrfy);
		          KorrMainButton (&NumControl3, scrfx, scrfy);
		          KorrMainButton (&StornoForm, scrfx, scrfy);
		          KorrMainButton (&ChoiseForm, scrfx, scrfy);
		          KorrMainButton (&OKControl, scrfx, scrfy);
		          KorrMainButton (&CaControl, scrfx, scrfy);
				  ctrlfont.FontHeight = (int) (double) 
					  ((double) ctrlfont.FontHeight * scrfy);
				  ctrlfont.FontWidth = (int) (double) 
					  ((double) ctrlfont.FontWidth * scrfx);
		}

		if (Num3Mode)
		{
			memcpy (&NumControl, &NumControl3, sizeof (form));
		}
		else if (CalcOn)
		{
			memcpy (&NumControl, &NumControl2, sizeof (form));
			editsum = (double) 0.0;
			eaction = ' ';
		}
		else
		{
			memcpy (&NumControl, &NumControl1, sizeof (form));
		}

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();
        tlen = strlen (text);

        if (BitMapOK == 0)
        {
                 NumLeft.bmp  = LoadBitmap (hMainInst, "pfeill");
                 NumRight.bmp = LoadBitmap (hMainInst, "pfeilr");
                 NumUp.bmp    = LoadBitmap (hMainInst, "pfeilo");
                 NumDown.bmp  = LoadBitmap (hMainInst, "pfeilu");
                 NumPlus.bmp  = LoadBitmap (hMainInst, "plus");
                 NumMinus.bmp = LoadBitmap (hMainInst, "minus");
                 NumCls.bmp = LoadBitmap (hMainInst, "clear");
                 BitMapOK = 1;
        }


        RegisterNumEnter (EntNuProc);
        if (ParentWindow == NULL || NuSizeSet)
        {
            xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
            yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
  		    cx = (int) (double) ((double) NuSize.cx * scrfx);; 
            cy = (int) (double) ((double) NuSize.cy * scrfy);
			x = (xfull - cx) / 2;
			x = max (0, x);
            y = (yfull - cy) / 2;
        }
                 
		else if (NumParent)
		{
                 GetWindowRect (NumParent, &rect);
                 GetWindowRect (ParentWindow, &rect1);
				 if (NumZent)
				 {
					 cx = rect1.right - rect1.left; 
					 x = (rect.right - cx) / 2;
					 x = max (0, x);
				 }
				 else if (Numlcx)
				 {
					 cx = Numlcx; 
					 x = (rect.right - cx) / 2;
					 x = max (0, x);
				 }
                 y = (int) (double) ((double) rect.bottom - (3 * nbsz0) - (46 * scrfy));
                 cy = rect.bottom - y;
				 if (Numlcy)
				 {
					 y -= Numlcy;
				 }
		}
		else
		{
                 GetWindowRect (ParentWindow, &rect);
                 x = rect.left;
                 y = (int) (double) ((double) rect.bottom - (3 * nbsz0) - (46 * scrfy));
                 cx = rect.right - rect.left; 
                 cy = rect.bottom - y;
		}

        ny = (int) (double) ((double) cy - (3 * nbsz0) - (30 * scrfy));
        for (i = 0,j = 0; j < NumField.fieldanz - 3; i ++,j += 3)
        {
                     NumField.mask[j].pos[0] = ny + i * nbsz0;  
                     NumField.mask[j + 1].pos[0] = ny + i * nbsz0;  
                     NumField.mask[j + 2].pos[0] = ny + i * nbsz0;
        }
        NumField.mask[j].pos[0] = ny + 2 * nbsz0;                 
        NumField.mask[j + 1].pos[0] = ny + 1 * nbsz0;
        NumField.mask[j + 2].pos[0] = ny;


        OKControl.mask[0].pos[0]  = ny;

        NumControl.mask[0].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[1].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[2].pos[0] = ny + 2 * nbsz0;
        NumControl.mask[3].pos[0] = ny + 2 * nbsz0;
        CaControl.mask[0].pos[0]  = ny + 2 * nbsz0;

        cbsx = cx - 2 * nbss0 - 5;
        OKControl.mask[0].pos[1]  = cbsx;

        NumControl.mask[0].pos[1] = cbsx;
        NumControl.mask[1].pos[1] = cbsx + nbss0;
        NumControl.mask[2].pos[1] = cbsx;
        NumControl.mask[3].pos[1] = cbsx + nbss0;
        CaControl.mask[0].pos[1] = cbsx + nbss0;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
		if (Num3Mode)
		{
		             ausweinhfont.FontHeight = 150; 
                     spezfont (&ausweinhfont);
                     hFont = SetWindowFont (hMainWindow);
                     hdc = GetDC (hMainWindow);
                     SelectObject (hdc, hFont);
                     GetTextMetrics (hdc, &tm);
                     DelFont (hFont);
                     ReleaseDC (hMainWindow, hdc);

                     eheight = 2 * tm.tmHeight;
                     elen = tm.tmAveCharWidth * 18;

                     epos = max (0, (epos - elen) / 2);
                     epos = NumField.mask[2].pos[1] + nbss0 + epos; 
                     fAuswahlEinh.mask[0].length = elen; 
					 if (scrfy > 1)
					 {
                                    fAuswahlEinh.mask[0].rows   = 
										(int) (double) ((double) eheight * 0.75);
					 }
					 else
					 {
						            ausweinhfont.FontHeight = 100; 
                                    fAuswahlEinh.mask[0].rows   = 
										(int) (double) ((double) eheight * 0.6);
					 }
                     fAuswahlEinh.mask[0].pos[1] = epos;
                     fAuswahlEinh.mask[0].pos[0] = NumField.mask[0].pos[0];
					 for (i = 1; i < fAuswahlEinh.fieldanz; i ++)
					 {
                              fAuswahlEinh.mask[i].length = elen; 
                              fAuswahlEinh.mask[i].rows   = fAuswahlEinh.mask[0].rows;
                              fAuswahlEinh.mask[i].pos[1] = epos;
                              fAuswahlEinh.mask[i].pos[0] = fAuswahlEinh.mask[i - 1].pos[0] +
								                            fAuswahlEinh.mask[0].rows   ;
					 }
		}

        EditNumFont.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&EditNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * nlen;
                 if (elen < epos) break;
                 EditNumFont.FontHeight -= 20;
                 if (EditNumFont.FontHeight <= 80) break;
        } 


        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos; 
        eheight = tm.tmHeight;
        eheight += eheight / 3;
        EditForm.mask[0].length = elen; 
        EditForm.mask[0].rows   = eheight;
        EditForm.mask[0].pos[1] = epos;
        EditForm.mask[0].pos[0] = ny;

		nlen = strlen (text);	
        TextNumFont.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&TextNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * nlen;
                 if (elen < epos) break;
                 TextNumFont.FontHeight -= 20;
                 if (TextNumFont.FontHeight <= 80) break;
        } 

                  
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos; 
        eheight = tm.tmHeight;
        eheight += eheight / 3;
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        elen = StornoForm.mask[0].length;
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos; 
        StornoForm.mask[0].pos[1] = epos;
        StornoForm.mask[0].pos[0] = cy - 30 - StornoForm.mask[0].rows;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        elen = ChoiseForm.mask[0].length;
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos; 
        ChoiseForm.mask[0].pos[1] = epos;
        ChoiseForm.mask[0].pos[0] = cy - 30 - ChoiseForm.mask[0].rows;

        if (pic)
        {
                       EditForm.mask[0].picture = pic;
        }

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DelFont (hFont);
        ReleaseDC (hMainWindow, hdc);

        elen = tm.tmAveCharWidth * strlen (text);
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (5, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos; 

		while ((epos + elen) > NumControl.mask[0].pos[1]) 
		{
			    elen -= 5;
		}
        eheight = tm.tmHeight;
        TextForm.mask[0].length = elen; 
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;
        TextForm.mask[0].pos[0] = (short)
                            EditForm.mask[0].pos[0] + 2 * (short) tm.tmHeight;
        TextForm.mask[0].feld = text;

		if (Num3Mode)
		{
                     fAuswahlEinh.mask[0].length = elen; 
                     fAuswahlEinh.mask[0].pos[1] = epos;
                     fAuswahlEinh.mask[0].pos[0] = NumField.mask[0].pos[0];
					 for (i = 1; i < fAuswahlEinh.fieldanz; i ++)
					 {
                              fAuswahlEinh.mask[i].length = elen; 
                              fAuswahlEinh.mask[i].pos[1] = epos;
                              fAuswahlEinh.mask[i].pos[0] = fAuswahlEinh.mask[i - 1].pos[0] +
								                            fAuswahlEinh.mask[i - 1].rows;
					 }
		}

        while (TRUE)
        {
                 spezfont (&TextNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * tlen;
                 if (elen < epos) break;
                 TextNumFont.FontHeight -= 20;
                 if (TextNumFont.FontHeight <= 80) break;
        } 

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DelFont (hFont);
        ReleaseDC (hMainWindow, hdc);
        elen = tm.tmAveCharWidth * strlen (text);

        TextNumFont.FontHeight -= 20;    

        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
        memcpy (editbuff, nummer, strlen (nummer));

        if (eForm == NULL) 
		{
			border = WS_POPUP | WS_CAPTION;
		}
		else
		{
			border = WS_POPUP | WS_CAPTION;
		}

        eNumWindow  = CreateWindowEx (
                              0,
                              "EntNum",
                              "",
                              border,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

		if (eNumWindow == NULL) return;

        SetClassLong (eNumWindow, GCL_WNDPROC, (long) EntNuProc);      
        if (eForm == NULL) 
		{
			create_enter_form (eNumWindow, &EditForm, 0, 0);
		}
        ShowWindow (eNumWindow, SW_SHOW);
        UpdateWindow (eNumWindow);

//        if (eForm == NULL) 
		{
//                    EnableWindows (hWnd, FALSE);
		}
        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));

		if (eForm == NULL)
		{
             SetFocus (EditForm.mask[0].feldid);
             SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
		
                          (WPARAM) 0, MAKELONG (-1, 0));
             NumEnterWindow = EditForm.mask[0].feldid;
		}
		else
		{
             SetFocus (eForm->mask[0].feldid);
             NumEnterWindow = eForm->mask[0].feldid;
			 if (SetNumCaret) (*SetNumCaret) (); 
             SetFocus (fAuswahlEinh.mask[currentfield].feldid);
		}

        EnableWindow (hWnd, FALSE);

        break_num_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsNumKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_num_enter) break;
        }
		if (eForm == 0)
		{
                 display_form (eNumWindow, &EditForm, 0, 0);
		}
        SetActiveWindow (hWnd);
        EnableWindow (hWnd, TRUE);
        CloseControls (&NumField);
        CloseControls (&NumControl);
        CloseControls (&OKControl);
        CloseControls (&CaControl);
		if (eForm == 0)
		{
                CloseControls (&EditForm);
		}
		else if (fAuswahlEinh.mask[0].feldid)
		{
                CloseControls (&fAuswahlEinh);
		}
        CloseControls (&TextForm);
        if (StornoForm.mask[0].after)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
        else if (StornoForm.mask[0].BuId)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
        if (ChoiseForm.mask[0].after)
        {
                    CloseControls (&ChoiseForm);
                    ChoiseForm.mask[0].after = NULL;
                    ChoiseForm.mask[0].BuId = 0;
        }
        else if (ChoiseForm.mask[0].BuId)
        {
                    CloseControls (&ChoiseForm);
                    ChoiseForm.mask[0].after = NULL;
                    ChoiseForm.mask[0].BuId = 0;
        }
        if (hMainWindow != NULL)
        {
            EnableWindow (hMainWindow, TRUE);
            SetActiveWindow (hMainWindow);
        }
        DestroyWindow (eNumWindow);
        eNumWindow = NULL;
        strcpy (nummer, editbuff);
        SetCursor (oldcursor);

        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
  	    eForm     = NULL;
	    ehWnd     = NULL;
	    eFormProc = NULL;
		SetNumCaret = NULL;
	    MeLPProc  = NULL;
		SetNum3 (FALSE);
		SetNumParent (NULL);
		ParentWindow = NULL;
}


void NUBLOCK::EnterCalcBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic,
                            WNDPROC EntNuProc)

/**
Numbox als rechner aufrufen.
**/
{
	     CalcOn = TRUE;
             EnterNumBox (hWnd, text, nummer, nlen, pic, EntNuProc);
	     CalcOn = FALSE;
}

int NUBLOCK::OnPaint (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
/**
Reaktion der Klasse auf Paintmeldung.
**/
{
          if (eNumWindow == NULL) return 0;
          display_form (eNumWindow, &NumField, 0, 0);
          display_form (eNumWindow, &NumControl, 0, 0);
          display_form (eNumWindow, &OKControl, 0, 0);
	  if (eForm == NULL)
	  {
                    display_form (eNumWindow, &TextForm, 0, 0);
                    display_form (eNumWindow, &EditForm, 0, 0);
		    SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                                                 (WPARAM) 0, MAKELONG (-1, 0));
	  }
	  else
	  {
                   display_form (eNumWindow, &fAuswahlEinh, 0, 0);
	  }

          display_form (eNumWindow, &CaControl, 0, 0);

          if (StornoForm.mask[0].after)
          {
                   display_form (eNumWindow, &StornoForm, 0, 0);
          }
          else if (StornoForm.mask[0].BuId)
          {
                   display_form (eNumWindow, &StornoForm, 0, 0);
          }

          if (ChoiseForm.mask[0].after)
          {
                   display_form (eNumWindow, &ChoiseForm, 0, 0);
          }
          else if (ChoiseForm.mask[0].BuId)
          {
                   display_form (eNumWindow, &ChoiseForm, 0, 0);
          }

	  if (eForm == NULL)
          {
                   SetFocus (EditForm.mask[0].feldid);
	  }
          return 0;
}

int NUBLOCK::OnButton (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
/*
Reaktion der Klasse auf Mousemeldung.
**/
{
        POINT mousepos;
        HWND enchild;
        static int SendChild = 0;
        int HasFocus = 1;

        GetCursorPos (&mousepos);
        enchild = IsNumChild (&mousepos);
        MouseTest (enchild, msg);
        if (enchild)
        {
                  SendMessage (enchild, msg, wParam, lParam);
                  return TRUE;
        }
        return TRUE;
}

int NUBLOCK::OnCommand (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
/*
Reaktion der Klasse auf Mousemeldung.
**/
{
        return 0;
}


