#ifndef _KASSEDLG_DEF
#define _KASSEDLG_DEF
#include "dlg.h"
#include "itprog.h"
#include "itfont.h"
#include "formfield.h"
#include "mo_txt.h"
#include "KasseWork.h"
#include "searchkun.h"

#define RAISEDBORDER 0
#define HIGHBORDER 1
#define LOWBORDER 2
#define HIGHCOLBORDER 3
#define LOWCOLBORDER 4
#define LINEBORDER 5

#define LS_CTL 1001 
#define RECH_CTL 1002
#define GEB_CTL 1003 
#define ZUR_CTL 1004 
#define ZAHLART_CTL 1005 
#define KUN_CTL 1006 
#define BETRAG_CTL 1007 
#define RECHBETRAG_CTL 1008 

#define IDM_CHOICE 2001
#define UP_CTL 2002 
#define DOWN_CTL 2003 
#define NUM_CTL 2004 
#define EURO1_CTL 2005 
#define EURO5_CTL 2006 
#define EURO10_CTL 2007 
#define EURO20_CTL 2008 
#define EURO50_CTL 2009 
#define EURO100_CTL 2010 
#define EUROVOLL_CTL 2011 
#define EUROZURUECK_CTL 2012 

class KasseDlg : virtual public DLG 
{
          private :
             static ItProg *After [];
             static ItProg *Before [];
             static ItFont *Font [];
             static FORMFIELD *dbfields [];
             static FORMFIELD *dbheadfields [];
             static char *EnableHead[];
             static char *EnablePos [];
		     static BitImage ImageDelete;
		     static BitImage ImageInsert;
             int    BorderType;
             static KasseWork work;
             static BOOL WriteOK;
             static char *HelpName;
             static char *ZahlArtCombo[20];
             CFORM *Toolbar2;
             MTXT *Mtxt;
			 BOOL SetFirstFocus;

             static KasseDlg *ActiveKasseDlg;
             static int pos;
             static char ptwert[];
          public :

            static HBITMAP SelBmp;
            static COLORREF SysBkColor;
		    static BOOL F5Enabled;
			static int tp;

            void SetToolbar2 (CFORM *Toolbar2)
            {
                this->Toolbar2 = Toolbar2;
            }

            CFORM *GetToolbar2 (void)
            {
                return Toolbar2;
            }
 	        KasseDlg (int, int, int, int, char *, int, BOOL);
 	        KasseDlg (int, int, int, int, char *, int, BOOL, int);
 	        KasseDlg (int, int, int, int, char *, int, BOOL, int, int);
			~KasseDlg ();
 	        void Init (int, int, int, int, char *, int, BOOL);
            BOOL OnKeyDown (void);
            BOOL OnKeyUp (void);
            BOOL OnKey1 (void);
            BOOL OnKey2 (void);
            BOOL OnKey3 (void);
            BOOL OnKey4 (void);
            BOOL OnKey5 (void);
            BOOL OnKey6 (void);
            BOOL OnLButtonDown (HWND,UINT,WPARAM,LPARAM);
            BOOL OnKey7 (void);
            BOOL OnKey9 (void);
            BOOL OnKey10 (void);
            BOOL OnKey12 (void);
            BOOL OnKeyDelete (void);
            BOOL OnKeyInsert (void);
            BOOL OnKeyPrior (void);
            BOOL OnKeyNext (void);
            BOOL OnKeyHome (void);
            BOOL OnKeyEnd (void);
            BOOL OnKeyReturn (void);
            BOOL OnPaint (HWND,HDC,UINT,WPARAM,LPARAM);
            BOOL OnActivate (HWND,UINT,WPARAM,LPARAM);
			BOOL OnCloseUp (HWND,WPARAM,LPARAM);
            BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnSysCommand (HWND,UINT,WPARAM,LPARAM);
            BOOL OnDestroy (HWND,UINT,WPARAM,LPARAM);
            void PrintComment (char *);
            HWND OpenWindow (HANDLE, HWND);
            HWND OpenScrollWindow (HANDLE, HWND);
            void SetWinBackground (COLORREF);
            void ToClipboard (void);
            void FromClipboard (void);
            void Help (void);
            void CallInfo (void);
            BOOL ShowKun (void);
			void SetKun (long);
			void SetRech (long);
            void FirstTabPos (void);
			static int TestFlag (void);
            static void ResetZahlArtCombo ();
			static int ReadKun (void);
			static int ReadRech (void);
			static int CalcZu (void);
			static void CalcEuro (double);
            static int EnterPos (void);
            static int GetPtab (void);
            static void GetCombos (void);
            static void SetCombos (void);
};
#endif
