#ifndef _RECH_TEIL_DEF
#define _RECH_TEIL_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct RECH_TEIL {
   short     delstatus;
   short     fil;
   long      kun;
   short     mdn;
   long      rech_nr;
   double    rech_bez;
   char      blg_typ[2];
   char      krz_txt[17];
   short     zahl_art;
   long      zahl_dat;
   short     waehrung;
   double    rech_bez_eur;
   double    rech_bez_fremd;
   char      pers_nam[9];
};
extern struct RECH_TEIL rech_teil, rech_teil_null;

#line 10 "rech_teil.rh"

class RECH_TEIL_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               RECH_TEIL_CLASS () : DB_CLASS ()
               {
               }
               ~RECH_TEIL_CLASS ()
               {
                   dbclose ();
               } 
               int dbreadfirst (void);
};
#endif
