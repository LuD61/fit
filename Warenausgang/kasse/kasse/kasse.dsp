# Microsoft Developer Studio Project File - Name="kasse" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=kasse - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "kasse.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "kasse.mak" CFG="kasse - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "kasse - Win32 Release" (basierend auf  "Win32 (x86) Application")
!MESSAGE "kasse - Win32 Debug" (basierend auf  "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "kasse - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "d:\informix\incl\esql" /I "..\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib /nologo /subsystem:windows /machine:I386 /out:"d:\user\fit\bin\kasse.exe" /libpath:"d:\informix\lib"

!ELSEIF  "$(CFG)" == "kasse - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /I "d:\informix\incl\esql" /I "..\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 comctl32.lib isqlt07c.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"d:\informix\lib"

!ENDIF 

# Begin Target

# Name "kasse - Win32 Release"
# Name "kasse - Win32 Debug"
# Begin Source File

SOURCE=..\libsrc\cmask.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\colbut.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\dbclass.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\DBFUNC.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\dlg.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\FIL.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\help.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\image.cpp
# End Source File
# Begin Source File

SOURCE=..\lib\inflib.lib
# End Source File
# Begin Source File

SOURCE=..\libsrc\itemc.cpp
# End Source File
# Begin Source File

SOURCE=.\kassedlg.cpp
# End Source File
# Begin Source File

SOURCE=.\kassework.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\lbox.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MDN.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_arg.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_chqex.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_CURSO.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_DRAW.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MELD.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MENU.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_progcfg.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_txt.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_wmess.cpp
# End Source File
# Begin Source File

SOURCE=.\nublock.cpp
# End Source File
# Begin Source File

SOURCE=.\prtmain.cpp
# End Source File
# Begin Source File

SOURCE=.\prtmain.rc
# End Source File
# Begin Source File

SOURCE=..\libsrc\PTAB.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\rdonly.cpp
# End Source File
# Begin Source File

SOURCE=.\rech_teil.cpp
# End Source File
# Begin Source File

SOURCE=.\searchkun.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\searchptab.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\STDFKT.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\STRFKT.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\Text.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\Vector.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\WMASK.CPP
# End Source File
# End Target
# End Project
