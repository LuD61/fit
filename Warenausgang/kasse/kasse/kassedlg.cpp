#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "wmask.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "KasseDlg.h" 
#include "colbut.h"
#include "help.h"
#include "lbox.h"
#include "ptab.h"
#include "nublock.h"
#include "Text.h"
#include "mo_chqex.h"
#ifdef BIWAK
#include "conf_env.h"
#endif
#include "mo_progcfg.h"

#define GLEICH 1
#define KLEINERGLEICH 2

static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int busize     = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;
extern Rechuebergabe ;
static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont bufont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};
static mfont bueurofont = {
                         "ARIAL", STDSIZE+ 20, 0, 3,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont rdonlyfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         WHITECOL,
                         0,
                         NULL};

static mfont hifont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLUECOL,
                         WHITECOL,
                         0,
                         NULL};

static mfont checkfonth = {
                           "ARIAL", STDSIZE, 0, 0,
                            BLACKCOL,
                            LTGRAYCOL,
                            0,
                            NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


static mfont cpfont = {
                        "Times New Roman", 200, 0, 1,
                         BLUECOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont *Font = &dlgposfont;

struct KASSEF
{
  char mdn [6];
  char kun [12];
  char kun_krz1 [20];
  char ls [12];
  char rechbetrag [20];
  char betrag [20];
  char rech [12];
  char gegeben [20];
  char zurueck [20];
  char zahl_art [6];
} kassf, kassf_null;



static ColButton CEuro1   = { "1 �",     -1, -1,  
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};

static ColButton CEuro5   = { "5 �",     -1, -1,  
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};

static ColButton CEuro10   = { "10 �",     -1, -1,  
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};

static ColButton CEuro20   = { "20 �",    -1, -1,  
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};

static ColButton CEuro50   = { "50 �",     -1, -1,  
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};

static ColButton CEuro100   = { "100 �",    -1, -1,  
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};

static ColButton CEuroVoll   = { "passend",    -1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};

static ColButton CEuroZurueck   = { "zur�cksetzen",    -1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
                             0, 
};

static ColButton Cf5   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
//                             BLUECOL,
//                             ACTCOLPRESS | NOCOLBORDER,
                             0, 
};

static ColButton Cf12   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
//                             BLUECOL,
//                             ACTCOLPRESS | NOCOLBORDER,
                             0, 
};


static ColButton CNum   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
//                             BLUECOL,
//                             ACTCOLPRESS | NOCOLBORDER,
                             0, 
};

static ColButton CDown   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
//                             BLUECOL,
//                             ACTCOLPRESS | NOCOLBORDER,
                             12, 
};
static ColButton CUp   = { "",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, -1,
                             NULL,  0, 0,
                             BLACKCOL,
                             LTGRAYCOL,
//                             BLUECOL,
//                             ACTCOLPRESS | NOCOLBORDER,
                             12, 
};
int  doffset = 1;
int euro_len = 10;
int ypos = 4;
int button_ypos = 4;
CFIELD *_fHead [] = {
                     new CFIELD ("cancel", (ColButton *) &Cf5,  8, 3, 1, 0,  NULL, "", 
                                 REMOVED,
                                 VK_F5, &bufont, 0, 0),
                     new CFIELD ("ok", (ColButton *) &Cf12,  20, 3, 1, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F12, &bufont, 0, 0),
                     new CFIELD ("num", (ColButton *) &CNum,  1, 3, 17, 0,  NULL, "", 
                                 REMOVED,
                                 NUM_CTL, &bufont, 0, 0),
                     new CFIELD ("up", (ColButton *) &CUp,  8, 3, 61, 0,  NULL, "", 
                                 REMOVED,
                                 UP_CTL, &bufont, 0, 0),
                     new CFIELD ("down", (ColButton *) &CDown,  8, 3, 69, 0,  NULL, "", 
                                 REMOVED,
                                 DOWN_CTL, &bufont, 0, 0),

                     new CFIELD ("euro1", (ColButton *) &CEuro1,  euro_len, 3, doffset, button_ypos,  NULL, "", 
                                 CCOLBUTTON,
                                 EURO1_CTL, &bueurofont, 0, 0),

                     new CFIELD ("euro5", (ColButton *) &CEuro5,  euro_len, 3, doffset + 1*euro_len, button_ypos,  NULL, "", 
                                 CCOLBUTTON,
                                 EURO5_CTL, &bueurofont, 0, 0),

                     new CFIELD ("euro10", (ColButton *) &CEuro10,  euro_len, 3, doffset + 2*euro_len, button_ypos,  NULL, "", 
                                 CCOLBUTTON,
                                 EURO10_CTL, &bueurofont, 0, 0),

                     new CFIELD ("euro20", (ColButton *) &CEuro20,  euro_len, 3, doffset + 3*euro_len, button_ypos,  NULL, "", 
                                 CCOLBUTTON,
                                 EURO20_CTL, &bueurofont, 0, 0),

                     new CFIELD ("euro50", (ColButton *) &CEuro50,  euro_len, 3, doffset + 4*euro_len, button_ypos,  NULL, "", 
                                 CCOLBUTTON,
                                 EURO50_CTL, &bueurofont, 0, 0),

                     new CFIELD ("euro100", (ColButton *) &CEuro100,  euro_len, 3, doffset + 5*euro_len, button_ypos,  NULL, "", 
                                 CCOLBUTTON,
                                 EURO100_CTL, &bueurofont, 0, 0),

                     new CFIELD ("eurovoll", (ColButton *) &CEuroVoll,  euro_len, 3, doffset + 6*euro_len, button_ypos,  NULL, "", 
                                 CCOLBUTTON,
                                 EUROVOLL_CTL, &bueurofont, 0, 0),

                     new CFIELD ("eurozurueck", (ColButton *) &CEuroZurueck,  euro_len, 3, doffset + 6*euro_len -11, button_ypos+13,  NULL, "", 
                                 CCOLBUTTON,
                                 EUROZURUECK_CTL, &bueurofont, 0, 0),

                     new CFIELD ("pos_frame", "",    40,15, 1, ypos,  NULL, "",
                                 CBORDER,   
                                 500, Font, 0, TRANSPARENT),    
                     new CFIELD ("kun_txt", "Kunde",  0, 0, 2, ypos+1,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("Kunde", kassf.kun,  10, 0, 18, ypos+1,  NULL, "%8ld", CEDIT,
                                 KUN_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("kun_choice", "", 2, 0, 28, ypos+1, NULL, "", CBUTTON,
                                  VK_F9, Font, 0, BS_BITMAP),
                     new CFIELD ("kun_krz1_txt", "Name",  0, 0, 2, ypos+2,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("kun_krz1", kassf.kun_krz1,  18, 0, 18, ypos+2,  NULL, "", CREADONLY,
                                 500, Font, 0, TRANSPARENT | WS_BORDER),
                     new CFIELD ("rech_txt", "Rechnung",  0, 0, 2, ypos+3,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("Rechnung", kassf.rech,  10, 0, 18, ypos+3,  NULL, "%8ld", CEDIT,
                                 RECH_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("ls_txt", "Lieferschein",  0, 0, 2, ypos+4,  NULL, "", 
					             CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("Lieferschein", kassf.ls,  10, 0, 18, ypos+4,  NULL, "%8ld", CREADONLY,
                                 LS_CTL, Font, 0, ES_RIGHT | TRANSPARENT | WS_BORDER),


                     new CFIELD ("rechbetrag_txt", "Rechnungsbetrag",  0, 0, 2, ypos+5,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("RechBetrag", kassf.rechbetrag,  14, 0, 18, ypos+5,  NULL, "%12.2lf", CREADONLY,
                                 RECHBETRAG_CTL, Font, 0, ES_RIGHT | TRANSPARENT | WS_BORDER),
                     new CFIELD ("betrag_txt", "Betrag",  0, 0, 2, ypos+6,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("Betrag", kassf.betrag,  14, 0, 18, ypos+6,  NULL, "%12.2lf", CREADONLY,
                                 BETRAG_CTL, Font, 0, ES_RIGHT | TRANSPARENT | WS_BORDER),

                     new CFIELD ("gegeben_txt", "gegeben",  0, 0, 2, ypos+8,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("Gegeben", kassf.gegeben,  14, 0, 18, ypos+8,  NULL, "%12.2lf", CEDIT,
                                 GEB_CTL, Font, 0, ES_RIGHT),
                     new CFIELD ("zurueck_txt", "zur�ck",  0, 0, 2, ypos+9,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("Zur�ck", kassf.zurueck,  14, 0, 18, ypos+9,  NULL, "%12.2lf", CREADONLY,
                                 ZUR_CTL, Font, 0, ES_RIGHT | TRANSPARENT | WS_BORDER),

                     new CFIELD ("zahl_art_txt", "Zahlart",  0, 0, 2, ypos+11,  NULL, "", 
                                 CDISPLAYONLY,
                                 500, Font, 0, TRANSPARENT),
                     new CFIELD ("zahl_art", kassf.zahl_art, 17, 10, 18, ypos+11,  NULL, "", CCOMBOBOX,
                                 ZAHLART_CTL, Font, 0,    CBS_DROPDOWNLIST |
                                                          WS_VSCROLL),

                     NULL,
};

CFORM fHead (33, _fHead);


CFIELD *_fPos [] = {
//                     new CFIELD ("pos_frame", "",    87,13, 1, 4,  NULL, "",
//                                 CBORDER,   
//                                 500, Font, 0, TRANSPARENT),    

                     NULL,
};



CFORM fPos (29, _fPos);

   

CFIELD *_fKasse [] = {
                     new CFIELD ("fKasse1", (CFORM *) &fHead, 0, 0, 0, 0, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     new CFIELD ("fKasse2", (CFORM *) &fPos,  0, 0, 0, 0, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,                      
};

CFORM fKasse (3, _fKasse);

CFIELD *_fKasse0 [] = { 
                     new CFIELD ("fKasse", (CFORM *) &fKasse, 0, 0, -1, 1, NULL, "", CFFORM,
                                   500, Font, 0, 0),
                     NULL,
};
CFORM fKasse0 (1, _fKasse0);

KasseWork KasseDlg::work;
char *KasseDlg::HelpName = "kasse.cmd";
KasseDlg *KasseDlg::ActiveKasseDlg = NULL;
HBITMAP KasseDlg::SelBmp;
BitImage KasseDlg::ImageDelete;
BitImage KasseDlg::ImageInsert;
COLORREF KasseDlg::SysBkColor = LTGRAYCOL;
int KasseDlg::pos = 0;
char KasseDlg::ptwert[5];
int KasseDlg::tp = 50;
BOOL KasseDlg::F5Enabled = TRUE;

int KasseDlg::EnterPos (void)
{

         return 0;
}


ItProg *KasseDlg::After [] = {
//                                   new ItProg ("Kunde",         ReadKun),
                                   new ItProg ("Gegeben",         CalcZu),
                                   new ItProg ("zahl_art",        TestFlag),
                                   NULL,
};

ItProg *KasseDlg::Before [] = {NULL,
};

ItFont *KasseDlg::Font [] = {
                                  new ItFont ("cancel",        &bufont),
                                  new ItFont ("ok",            &bufont),
                                  new ItFont ("num",           &bufont),
                                  new ItFont ("kun_krz1",      &rdonlyfont),
                                  new ItFont ("Lieferschein",  &rdonlyfont),
                                  new ItFont ("RechBetrag",    &hifont),
                                  new ItFont ("Betrag",        &rdonlyfont),
                                  new ItFont ("Zur�ck",        &rdonlyfont),
                                  NULL
};

FORMFIELD *KasseDlg::dbheadfields [] = {
		 NULL
};

FORMFIELD *KasseDlg::dbfields [] = {
//         new FORMFIELD ("adr",       kassf.adr,       (long *)    &partner.adr,       FLONG,    "%ld"),
         NULL,
};



char *KasseDlg::EnableHead [] = {
//                                "mdn",
                                 NULL
};


char *KasseDlg::EnablePos[] = {
//	                           "adr",
                               NULL,
};


char *KasseDlg::ZahlArtCombo[20] = {
                                     NULL};


                          
KasseDlg::KasseDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

KasseDlg::KasseDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

KasseDlg::KasseDlg (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel, 
                  int BorderType, int EnterM) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             this->BorderType = BorderType;
//             EnterMode = EnterM;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}

KasseDlg::~KasseDlg ()
{
          fKasse0.destroy ();
}
			 

void KasseDlg::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             PROG_CFG ProgCfg ("kasse");
             int i;
             int xfull, yfull;

			 strcpy (kassf.mdn, "1");
			 Cf5.BkColor = SysBkColor;
             Cf5.bmp     = BMAP::LoadBitmap (hInstance, "F5", "F5MASK", SysBkColor);
			 Cf12.BkColor  = SysBkColor;
             Cf12.bmp     = LoadBitmap (hInstance, "F12");
			 CNum.BkColor = SysBkColor;
             CNum.bmp     = LoadBitmap (hInstance, "Num");
			 CUp.BkColor = SysBkColor;
             CUp.bmp     = BMAP::LoadBitmap (hInstance, "arrupb", "arrupbmask",SysBkColor);
			 CDown.BkColor = SysBkColor;
             CDown.bmp     = BMAP::LoadBitmap (hInstance, "arrdownb", "arrdownbmask",SysBkColor);

             ImageDelete.Image    = BMAP::LoadBitmap (hInstance, "DEL",       "DELMASK",      SysBkColor);
             ImageDelete.Inactive = BMAP::LoadBitmap (hInstance, "DELIA",     "DELMASK",      SysBkColor);
             ImageInsert.Image    = BMAP::LoadBitmap (hInstance, "INSERT",    "INSERTMASK",   SysBkColor);
             ImageInsert.Inactive = BMAP::LoadBitmap (hInstance, "INSERTIA",  "INSERTMASK",   SysBkColor);

             ActiveKasseDlg = this; 
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        


             StdSize = Size;
             dlgfont.FontHeight    = Size;
             bufont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;
             rdonlyfont.FontHeight = Size;
             hifont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;
             cpfont.FontHeight     = cpsize;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 bufont.FontHeight = 90;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 rdonlyfont.FontHeight = 90;
                 hifont.FontHeight = 90;
                 checkfonth.FontHeight = 90;
                 cpfont.FontHeight = 150;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 bufont.FontHeight = 70;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 rdonlyfont.FontHeight = 70;
                 hifont.FontHeight = 70;
                 checkfonth.FontHeight = 70;
                 cpfont.FontHeight = 120;
             }

             dlgfont.FontHeight += tp;
             dlgposfont.FontHeight += tp;
             ltgrayfont.FontHeight += tp;
             rdonlyfont.FontHeight += tp;
             hifont.FontHeight += tp;
             checkfonth.FontHeight += tp;
             cpfont.FontHeight += tp;

             fHead.SetFieldanz ();
             fPos.SetFieldanz ();
             fKasse.SetFieldanz ();

             fHead.GetCfield ("kun_choice")->SetBitmap 
                 (LoadBitmap (hInstance, "ARRDOWN"));

             fHead.GetCfield ("kun_choice")->SetTabstop (FALSE); 
             fHead.GetCfield ("cancel")->SetTabstop (FALSE); 
             fHead.GetCfield ("ok")->SetTabstop (FALSE); 
             fHead.GetCfield ("num")->SetTabstop (FALSE); 
             fHead.GetCfield ("up")->SetTabstop (FALSE); 
             fHead.GetCfield ("down")->SetTabstop (FALSE); 
#ifdef LIBMAIN
			 fHead.GetCfield ("num")->SetAttribut (CREMOVED);
			 if (F5Enabled == FALSE)
			 {
			       fHead.GetCfield ("cancel")->SetAttribut (CREMOVED);
			 }
			 else
			 {
			       fHead.GetCfield ("cancel")->SetAttribut (CCOLBUTTON);
			 }
#endif
#ifdef DLLMAIN
			 fHead.GetCfield ("num")->SetAttribut (CREMOVED);
			 if (F5Enabled == FALSE)
			 {
			       fHead.GetCfield ("cancel")->SetAttribut (CREMOVED);
			 }
			 else
			 {
			       fHead.GetCfield ("cancel")->SetAttribut (CCOLBUTTON);
			 }
#endif


             for (i = 0; i < fHead.GetFieldanz (); i ++)
             {
//                       fHead.GetCfield ()[i]->SetFont (&dlgfont); 
//                       fHead.GetCfield ()[i]->SetPosFont (&dlgposfont); 
             }

             SetTmFont (&dlgfont);
             for (i = 0; Before[i] != NULL; i ++)
             {
                 Before[i]->SetBefore (&fKasse);
             }

             for (i = 0; After[i] != NULL; i ++)
             {
                 After[i]->SetAfter (&fKasse);
             }


             for (i = 0; Font[i] != NULL; i ++)
             {
                 Font[i]->SetFont (&fKasse);
             }
             if (BorderType == HIGHCOLBORDER)
             {

                for (i = 0; Font[i] != NULL; i ++)
                {
                 Font[i]->SetFont (&fKasse);
                }

                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, HIGH, 0, LTGRAYCOL)); 
             }
             else if (BorderType == LOWCOLBORDER)
             {

                for (i = 0; Font[i] != NULL; i ++)
                {
                 Font[i]->SetFont (&fKasse);
                }
                fPos.GetCfield ("pos_frame")->SetBorder 
                 (new FILLEDBORDER (BLACKCOL, WHITECOL, LOW, 0, LTGRAYCOL)); 
             }
             else if (BorderType == HIGHBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, HIGH); 
             }
             else if (BorderType == LOWBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LOW); 
             }
             else if (BorderType == RAISEDBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, RAISED); 
             }
             else if (BorderType == LINEBORDER)
             {
                fPos.GetCfield ("pos_frame")->SetBorder (BLACKCOL, WHITECOL, LINE); 
             }
             work.FillZahlArtCombo (ZahlArtCombo, 20);
             strcpy (kassf.zahl_art, ZahlArtCombo [0]);
			 SetFirstFocus = FALSE;
             fHead.GetCfield ("zahl_art")->SetCombobox (ZahlArtCombo); 

             Enable (&fKasse, EnableHead, TRUE); 
             Enable (&fKasse, EnablePos,  FALSE);
		     fHead.GetCfield ("Kunde")->Enable (TRUE);
		     fHead.GetCfield ("Rechnung")->Enable (TRUE);
			 NewTabStops (&fKasse0);
             SetDialog (&fKasse0);
}

void KasseDlg::SetKun (long kun)
{

	    sprintf (kassf.kun, "%ld", kun);
		fHead.SetText ();
		ReadKun ();
		ResetZahlArtCombo ();
		sprintf (kassf.zahl_art, "%hd", work.GetZahlArt ());
		SetCombos ();
		SetFirstFocus = TRUE;
}

void KasseDlg::SetRech (long rech)
{

	    sprintf (kassf.rech, "%ld", rech);
		fHead.GetCfield ("Kunde")->Enable (FALSE);
		fHead.GetCfield ("Rechnung")->Enable (FALSE);
		fHead.SetText ();
		ReadRech ();
		ResetZahlArtCombo ();
		sprintf (kassf.zahl_art, "%hd", work.GetZahlArt ());
		SetCombos ();
		SetFirstFocus = TRUE;
}

void KasseDlg::FirstTabPos (void)
{
	    if (SetFirstFocus)
		{
               UpdateTabstop (fHead.GetCfield ("Gegeben")->GethWnd ());
		}
		else
		{
               UpdateTabstop (fHead.GetCfield ("Kunde")->GethWnd ());
		}
}


void KasseDlg::ResetZahlArtCombo ()
{
        work.FillZahlArtCombo (ZahlArtCombo, 20);
        fHead.GetCfield ("zahl_art")->FillComboBox (); 
}

int KasseDlg::ReadKun (void)
{
	    long rech_nr; 
		double rech_bto_eur;
		double rech_bez_eur;

	    fHead.GetText ();
		if (atol (kassf.kun) == 0l)
		{
			return 1;
		}
	    if (work.GetKunName (kassf.kun_krz1, atoi (kassf.mdn), atol (kassf.kun)) != 0)
		{
			print_mess (2, "Kunde %ld nicht angelegt", atol (kassf.kun));
			ActiveKasseDlg->SetCurrentName ("Kunde");
 		    fHead.SetText ();
            return 0;
		}
	    if (work.GetRech (atoi (kassf.mdn), atol (kassf.kun)) != 0)
		{
			print_mess (2, "Keine Rechnung f�r Kunde %ld", atol (kassf.kun));
			ActiveKasseDlg->SetCurrentName ("Kunde");
 		    fHead.SetText ();
            return 0;
		}
		ResetZahlArtCombo (); 
		rech_nr = work.GetRechNr ();
		rech_bto_eur = work.GetRechBto ();
		rech_bez_eur = work.GetRechBez ();
		sprintf (kassf.rech, "%ld", rech_nr);
		sprintf (kassf.ls, "%ld", work.GetLs ());
		sprintf (kassf.rechbetrag, "%lf", rech_bto_eur);
		sprintf (kassf.betrag, "%lf", rech_bto_eur - rech_bez_eur);
		sprintf (kassf.gegeben, "%lf", rech_bto_eur - rech_bez_eur + ratod (kassf.zurueck));
		sprintf (kassf.zahl_art, "%hd", work.GetZahlArt ());
		SetCombos ();
		fHead.SetText ();
		ActiveKasseDlg->SetCurrentName ("Gegeben");
		return 0;
}

int KasseDlg::ReadRech (void)
{
	    long rech_nr; 
		double rech_bto_eur;
		double rech_bez_eur;

	    fHead.GetText ();
		if (atol (kassf.rech) == 0l)
		{
			return 1;
		}
	    if (work.GetRechNr (atoi (kassf.mdn), atol (kassf.rech)) != 0)
		{
			print_mess (2, "Rechnung %ld nicht gefunden", atol (kassf.rech));
			ActiveKasseDlg->SetCurrentName ("Rechnung");
 		    fHead.SetText ();
            return 0;
		}
		sprintf (kassf.kun, "%ld", work.GetKun ());
	    work.GetKunName (kassf.kun_krz1, atoi (kassf.mdn), atol (kassf.kun));
		rech_nr = work.GetRechNr ();
		rech_bto_eur = work.GetRechBto ();
		rech_bez_eur = work.GetRechBez ();
		sprintf (kassf.rech, "%ld", rech_nr);
		sprintf (kassf.ls, "%ld", work.GetLs ());
		sprintf (kassf.rechbetrag, "%lf", rech_bto_eur);
		sprintf (kassf.betrag, "%lf", rech_bto_eur - rech_bez_eur);
	//	sprintf (kassf.gegeben, "%lf", rech_bto_eur - rech_bez_eur + ratod (kassf.zurueck));
		sprintf (kassf.zahl_art, "%hd", work.GetZahlArt ());
		SetCombos ();
		fHead.SetText ();
		ActiveKasseDlg->SetCurrentName ("Gegeben");
		return 0;
}

BOOL KasseDlg::ShowKun (void)
{
        struct SKUN *skun;
		SEARCHKUNRC SearchKun; 

		if (tp > 0)
		{
		      CHQEX::SetFontSize (100 + tp / 2);
		      SearchKun.SetWsize (58, 20);
		}

        SearchKun.SetParams (DLG::hInstance, ActiveDlg->GethWnd ());
        SearchKun.Setawin (ActiveDlg->GethMainWindow ());
        SearchKun.SearchKun (atoi (kassf.mdn));
        ActiveDlg->Validate ();
		CHQEX::ResetFontSize ();
        if (syskey == KEY5)
        {
            fHead.SetCurrentName ("kun");
            return 0;
        }
        skun = SearchKun.GetSkun ();
        if (skun == NULL)
        {
            return 0;
        }
        strcpy (kassf.kun, skun->kun);
        fHead.SetText ();
        ReadKun ();
        return 0;
}

void KasseDlg::CalcEuro (double Betrag)
{
	if (Betrag == 999.9)
	{
		sprintf(kassf.gegeben, "%.2lf", ratod(kassf.betrag));
	}
	else if (Betrag == 0.0)
	{
		sprintf(kassf.gegeben, "%.2lf", 0.0);
	}
	else
	{
		sprintf(kassf.gegeben, "%.2lf", ratod(kassf.gegeben) + Betrag);
	}
	fHead.SetText ();
	CalcZu ();
}

int KasseDlg::CalcZu (void)
{
	    fHead.GetText ();
        sprintf (kassf.zurueck, "%.2lf", ratod (kassf.gegeben) -
			                             ratod (kassf.betrag));
		if (ratod (kassf.zurueck) < 0)
		{
			sprintf (kassf.zurueck, "0");
		}
		TestFlag ();
		fHead.SetText ();
		return 0;
}

BOOL KasseDlg::OnKeyReturn (void)
{
        HWND hWnd;
        
        hWnd = GetFocus ();
		if (hWnd == fHead.GetCfield ("Kunde")->GethWnd ())
		{
			if (ReadKun () == 0)
			{
				return TRUE;
			}
		}
		else if (hWnd == fHead.GetCfield ("Rechnung")->GethWnd ())
		{
			if (ReadRech () == 0)
			{
				return TRUE;
			}
		}
        return FALSE;
}

void KasseDlg::GetCombos (void)
{
        pos = fHead.GetCfield ("zahl_art")->GetComboPos ();
        strcpy (kassf.zahl_art, work.GetPtWertClass ("zahl_art", pos + 1)); 

}

void KasseDlg::SetCombos (void)
{
         sprintf (ptwert, "%hd", atoi (kassf.zahl_art));
         if (work.GetPtabClass (kassf.zahl_art, "zahl_art", ptwert) == -1)
		 {
                 fHead.GetCfield ("zahl_art")->SetPosCombo (0);
		 }
		 else
		 {
                 fHead.GetCfield ("zahl_art")->SetPosCombo (ptabn.ptlfnr - 1);
		 }
}


BOOL KasseDlg::OnKeyDown (void)
{
        HWND hWnd;
        CFIELD *Cfield;

        hWnd == GetFocus ();

        Cfield = GetCurrentCfield ();
        if (Cfield != NULL)
        {
            if (Cfield->GetAttribut () & CCOMBOBOX != 0)
            {
                return FALSE;
            }
        }
/*
        if (fHead.GetCfield ("kun")->IsDisabled () == FALSE)
        {
            return FALSE;
        }
*/

        syskey = KEYDOWN;
        return FALSE;
}


BOOL KasseDlg::OnKeyUp (void)
{
        HWND hWnd;
        CFIELD *Cfield;

        hWnd == GetFocus ();


        Cfield = GetCurrentCfield ();
        if (Cfield != NULL)
        {
            if (Cfield->GetAttribut () & CCOMBOBOX != 0)
            {
                return FALSE;
            }
        }
 
        fPos.GetText ();

        return FALSE;
}


BOOL KasseDlg::OnKey2 ()
{
        return TRUE;
}



BOOL KasseDlg::OnKeyHome ()
{
        return TRUE;
}

BOOL KasseDlg::OnKeyEnd ()
{
        return TRUE;

}
BOOL KasseDlg::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}

BOOL KasseDlg::OnKey3 ()
{
        syskey = KEY3;
        return TRUE;
}

BOOL KasseDlg::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}

BOOL KasseDlg::OnKey5 ()
{

//#ifdef LIBMAIN
		 if (F5Enabled == FALSE)
		 {
	         return 0; 
		 }

//#endif
#ifdef DLLMAIN
		 if (F5Enabled == FALSE)
		 {
	         return 0; 
		 }
#endif
        syskey = KEY5;
	    HWND hWnd = GetFocus ();
		if (hWnd != fHead.GetCfield ("Kunde")->GethWnd ())
		{
		       memcpy (&kassf, &kassf_null, sizeof (kassf));
		       strcpy (kassf.mdn, "1");
		       fWork->SetText ();
		       SetCurrentName ("Kunde");
               return TRUE;
		}

        Enable (&fKasse, EnableHead, TRUE); 
        Enable (&fKasse, EnablePos,  FALSE);
        work.RollbackWork ();
        ToForm (dbfields);
        SetCombos ();
        fPos.SetText ();
        fHead.SetCurrentName ("ls");
		PostQuitMessage (0);
        return TRUE;
}

BOOL KasseDlg::OnKey12 ()
{
        syskey = KEY12;

		if (atol (kassf.kun) == 0l ||
			atol (kassf.rech) == 0l)
		{
	 	    if (F5Enabled == FALSE)
			{
		         syskey = KEY5;
		         PostQuitMessage (0);
			}
			return TRUE;
		}
		if (TestFlag () != 0)
		{
			return TRUE;
		}
        fWork->GetText ();
        FromForm (dbfields); 
        GetCombos ();
		work.SetBezahlt (ratod (kassf.gegeben) - ratod (kassf.zurueck));
		work.SetZahlArt (atoi (kassf.zahl_art));
		if (work.UpdateRech (atoi (kassf.mdn), atol (kassf.rech)) == FALSE)
		{
		    SetCurrentName ("Gegeben");
			return TRUE;
		}
		memcpy (&kassf, &kassf_null, sizeof (kassf));
		strcpy (kassf.mdn, "1");
		fWork->SetText ();
		SetCurrentName ("Kunde");
//#ifdef LIBMAIN
 	    if (F5Enabled == FALSE)
		{
		         syskey = KEY5;
		         PostQuitMessage (0);
		}
//#endif
#ifdef DLLMAIN
 	    if (F5Enabled == FALSE)
		{
		        PostQuitMessage (0);
		}
#endif
        return TRUE;
}

BOOL KasseDlg::OnKey10 (void)
{
      return FALSE;
}


BOOL KasseDlg::OnKeyDelete ()
{
        
        return FALSE;
}



BOOL KasseDlg::OnKeyInsert ()
{
        return FALSE;
}


BOOL KasseDlg::OnKeyPrior ()
{

        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEUP, 0l);
             return TRUE;
        }
        return FALSE;
}

BOOL KasseDlg::OnKeyNext ()
{
        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEDOWN, 0l);
             return TRUE;
        }
        return FALSE;
}

BOOL KasseDlg::OnLButtonDown (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
	    NUBLOCK NuBlock;
		char Nummer [80];
		extern BOOL ColBorder;
        CFIELD *aktfield;
		BOOL mitAuswahl = FALSE;

         POINT mousepos;
		 aktfield = NULL;
         switch (msg)
         {
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);
                    if (MouseinWindow (fWork->GethWndID (GEB_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("Gegeben");
						mitAuswahl = TRUE;
					}
                    if (MouseinWindow (fWork->GethWndID (ZUR_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("Gegeben");
						mitAuswahl = TRUE;
					}
                    if (MouseinWindow (fWork->GethWndID (BETRAG_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("Gegeben");
						mitAuswahl = TRUE;
					}
                    if (MouseinWindow (fWork->GethWndID (EURO1_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("euro1");
						mitAuswahl = FALSE;
					}
                    if (MouseinWindow (fWork->GethWndID (EURO5_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("euro5");
						mitAuswahl = FALSE;
					}
                    if (MouseinWindow (fWork->GethWndID (EURO10_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("euro10");
						mitAuswahl = FALSE;
					}
                    if (MouseinWindow (fWork->GethWndID (EURO20_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("euro20");
						mitAuswahl = FALSE;
					}
                    if (MouseinWindow (fWork->GethWndID (EURO50_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("euro50");
						mitAuswahl = FALSE;
					}
                    if (MouseinWindow (fWork->GethWndID (EURO100_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("euro100");
						mitAuswahl = FALSE;
					}
                    if (MouseinWindow (fWork->GethWndID (EUROVOLL_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("eurovoll");
						mitAuswahl = FALSE;
					}
                    if (MouseinWindow (fWork->GethWndID (EUROZURUECK_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("eurozurueck");
						mitAuswahl = FALSE;
					}

					/**
                    if (MouseinWindow (fWork->GethWndID (KUN_CTL), &mousepos))
                    {
						aktfield = fHead.GetCfield ("Kunde");
					}
					**/
			  }
		 }
        if (aktfield != NULL)
        {
            aktfield->SetText ();
            PrintComment (aktfield->GetName ());
            CurrentCfield = aktfield;
		}
		else return FALSE;

		CFIELD *Cfield = GetCurrentCfield ();
		if (Cfield->GetAttribut () == CCOMBOBOX)
		{
			return FALSE;
		}
		if (mitAuswahl)
		{
			ColBorder = TRUE;
			NuBlock.SetNuBlock ();
			NuBlock.MainInstance (hInstance, hWnd);
			NuBlock.ScreenParam (scrfx, scrfy);
			NuBlock.SetNuSize (600, 250);
			NuBlock.SetParent (hWnd);
			NuBlock.EnterNumBox (hMainWindow, Cfield->GetName (), 
			                 Nummer, Cfield->GetLength (), 
			                 Cfield->GetPicture (), NULL);
			if (syskey == KEY5)
			{
				Cfield->SetFocus ();
				return TRUE;
			}
			strcpy (Cfield->GetFeld (), Nummer);
		}
		Cfield->SetText ();
		Text Cname = Cfield->GetName (); 
		if (Cname == "Kunde")
		{
			ReadKun ();
		}
		else if (Cname == "Rechnung")
		{
			ReadRech ();
		}
		else if (Cname == "Gegeben")
		{
			CalcZu ();
		}
		else if (Cname == "euro1")
		{
			CalcEuro (1.0);
		}
		else if (Cname == "euro5")
		{
			CalcEuro (5.0);
		}
		else if (Cname == "euro10")
		{
			CalcEuro (10.0);
		}
		else if (Cname == "euro20")
		{
			CalcEuro (20.0);
		}
		else if (Cname == "euro50")
		{
			CalcEuro (50.0);
		}
		else if (Cname == "euro100")
		{
			CalcEuro (100.0);
		}
		else if (Cname == "eurovoll")
		{
			CalcEuro (999.9);
		}
		else if (Cname == "eurozurueck")
		{
			CalcEuro (0.0);
		}
		else
		{
		    FocusDown ();
		}
        return TRUE;

}



BOOL KasseDlg::OnKey6 ()
{
	    NUBLOCK NuBlock;
		char Nummer [80];
		extern BOOL ColBorder;

#ifdef DLLMAIN
		return TRUE;
#elif LIBMAIN
		return TRUE;
#endif
		CFIELD *Cfield = GetCurrentCfield ();
		if (Cfield->GetAttribut () == CCOMBOBOX)
		{
			return FALSE;
		}
		ColBorder = TRUE;
        NuBlock.SetNuBlock ();
        NuBlock.MainInstance (hInstance, hWnd);
        NuBlock.ScreenParam (scrfx, scrfy);
		NuBlock.SetNuSize (600, 250);
		NuBlock.SetParent (hWnd);
        NuBlock.EnterNumBox (hMainWindow, Cfield->GetName (), 
			                 Nummer, Cfield->GetLength (), 
			                 Cfield->GetPicture (), NULL);
		if (syskey == KEY5)
		{
			Cfield->SetFocus ();
			return TRUE;
		}
		strcpy (Cfield->GetFeld (), Nummer);
		Cfield->SetText ();
        Text Cname = Cfield->GetName (); 
		if (Cname == "Kunde")
		{
			ReadKun ();
		}
		else if (Cname == "Rechnung")
		{
			ReadRech ();
		}
		else if (Cname == "Gegeben")
		{
			CalcZu ();
		}
		else
		{
		    FocusDown ();
		}
        return TRUE;
} 


BOOL KasseDlg::OnKey7 ()
{
        return FALSE;
}


BOOL KasseDlg::OnKey9 ()
{
         CFIELD *Cfield;
         HWND hWnd;

         syskey = KEY9;
         hWnd = GetFocus ();


         if (hWnd == fHead.GetCfield ("kun_choice")->GethWnd ())
         {
             return ShowKun ();
         }

         Cfield = ActiveDlg->GetCurrentCfield ();

         if (strcmp (Cfield->GetName (), "Kunde") == 0)
         {
             return ShowKun ();
         }

         return FALSE;
}


BOOL KasseDlg::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             PrintMess (MessText);
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return DLG::OnPaint (hWnd,hdc, msg, wParam,lParam);
}


BOOL KasseDlg::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}

int KasseDlg::TestFlag (void)
{
	    char ptwer1 [10];
		static BOOL InTest = FALSE;

		if (syskey == KEY5)
		{
			return 0;
		}
		if (InTest)
		{
			return 0;
		}
		InTest = TRUE;
		fHead.GetText ();
        int pos = fHead.GetCfield ("zahl_art")->GetComboPos ();
        strcpy (ptwer1, work.GetPtWer2Class ("zahl_art", pos + 1)); 
		if (atoi (ptwer1) == GLEICH)
		{
			if (ratod (kassf.gegeben) != ratod (kassf.betrag))
			{
				disp_mess ("Der Zahlbetrag und der Sollbetrag m�ssen �bereinstimmen", 2);
 			    strcpy (kassf.gegeben, kassf.betrag);
			    strcpy (kassf.zurueck, "0");
 		        fHead.SetText ();
				ActiveKasseDlg->SetCurrentName ("Gegeben");
 		        InTest = FALSE;
				return 1;
			}
		}
		else if (atoi (ptwer1) == KLEINERGLEICH &&
			     ratod (kassf.gegeben) > ratod (kassf.betrag))
		{
			disp_mess ("Der Zahlbetrag darf nicht gr��er als der Sollbetrag sein", 2);
			strcpy (kassf.gegeben, kassf.betrag);
			strcpy (kassf.zurueck, "0");
	        fHead.SetText ();
			ActiveKasseDlg->SetCurrentName ("Gegeben");
	        InTest = FALSE;
			return 1;
		}
        InTest = FALSE;
	    return 0;
}

BOOL KasseDlg::OnCloseUp (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
        if (LOWORD (wParam) == ZAHLART_CTL)
		{
			   return FALSE;
		}
	    return FALSE;
}


BOOL KasseDlg::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {

              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }

              if (LOWORD (wParam) == UP_CTL)
              {
				  PostMessage (WM_KEYDOWN, VK_UP, 0l);
                  return TRUE;
              }

              if (LOWORD (wParam) == DOWN_CTL)
              {
				  PostMessage (WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
              }

              if (LOWORD (wParam) == NUM_CTL)
              {
				  PostMessage (WM_KEYDOWN, VK_F6, 0l);
                  return TRUE;
              }
              if (LOWORD (wParam) == IDM_CHOICE)
              {
                  return OnKey9 ();
              }

              if (HIWORD (wParam) == WM_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              if (HIWORD (wParam) == EN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == WM_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == LBN_SELCHANGE)
              {
//                  return ListChanged (wParam, lParam);
              }
              else if (HIWORD (wParam) == EN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == CBN_CLOSEUP)
              {
                  return OnCloseUp (hWnd, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_SETFOCUS)
              {
                  return OnSetFocus (hWnd, msg, wParam, lParam);
              }
              else if (HIWORD (wParam) == BN_KILLFOCUS)
              {
                  return OnKillFocus (hWnd, msg, wParam, lParam);
              }
        }
        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL KasseDlg::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            syskey = KEY5;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL KasseDlg::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
             PostQuitMessage (0);
             fKasse.destroy ();
        }
        return TRUE;
}

void KasseDlg::SetWinBackground (COLORREF Col)
{
          dlgfont.FontBkColor = Col;
          cpfont.FontBkColor = Col;
          DLG::SetWinBackground (Col);
}


void KasseDlg::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void KasseDlg::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

void KasseDlg::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}



HWND KasseDlg::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
          return hWnd;
}

HWND KasseDlg::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
          return hWnd;
}

void KasseDlg::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}

void KasseDlg::CallInfo (void)
{
          DLG::CallInfo ();
}






