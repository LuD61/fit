#ifndef _KASSE_WORK_DEF
#define _KASSE_WORK_DEF
#include "dbclass.h"
#include "ptab.h"
#include "mo_txt.h"
#include "rech_teil.h"
#include "Vector.h"

class KasseWork
{
    private :
        PTAB_CLASS Ptab;
		DB_CLASS DbClass; 
		long rech_nr;
		long ls;
		long kun;
		double rech_bto_eur;
		double rech_bez_eur;
		double rech_bez;
		short zahl_art;
		double kred_lim;
		double bezahlt;
		char blg_typ[2];
		CVector Ptabs;

    public :
        KasseWork ()
        {
        }

		long GetRechNr ()
		{
			return rech_nr;
		}

		int GetZahlArt ()
		{
			return zahl_art;
		}

		void SetZahlArt (int zahl_art)
		{
			this->zahl_art = zahl_art;
		}

		long GetLs ()
		{
			return ls;
		}

		long GetKun ()
		{
			return kun;
		}

		void SetBezahlt (double bezahlt)
		{
			this->bezahlt = bezahlt;
		}

		double GetRechBto ()
		{
			return rech_bto_eur;
		}

		double GetRechBez ()
		{
			return rech_bez_eur;
		}

		
        int GetKunName (char *, short, long);
        int GetRech (short, long);
        int GetRechNr (short, long);
		BOOL UpdateRech (short, long);
        void BeginWork (void);
        void CommitWork (void);
        void RollbackWork (void);
        int GetPtabClass (char *, char *, char *);
        int GetPtab (char *, char *, char *);
        char *GetPtWertClass (char *, short);
        char *GetPtWert (char *, short);
        char *GetPtWer1Class (char *, short);
        char *GetPtWer1 (char *, short);
        char *GetPtWer2Class (char *, short);
        char *GetPtWer2 (char *, short);
        int ShowPtab (char *dest, char *item);
        void FillZahlArtCombo (char **, int);
        void FillCombo (char **, char *, int);
        void FillComboLong (char **, char *, int);
        void WriteTxt (long, MTXT *);
        int  CutLines (char *, char *, int, int);
};

#endif