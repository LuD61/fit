#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "rech_teil.h"

struct RECH_TEIL rech_teil, rech_teil_null;

void RECH_TEIL_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &rech_teil.mdn, 1, 0);
            ins_quest ((char *) &rech_teil.fil, 1, 0);
            ins_quest ((char *) &rech_teil.rech_nr, 2, 0);
    out_quest ((char *) &rech_teil.delstatus,1,0);
    out_quest ((char *) &rech_teil.fil,1,0);
    out_quest ((char *) &rech_teil.kun,2,0);
    out_quest ((char *) &rech_teil.mdn,1,0);
    out_quest ((char *) &rech_teil.rech_nr,2,0);
    out_quest ((char *) &rech_teil.rech_bez,3,0);
    out_quest ((char *) rech_teil.blg_typ,0,2);
    out_quest ((char *) rech_teil.krz_txt,0,17);
    out_quest ((char *) &rech_teil.zahl_art,1,0);
    out_quest ((char *) &rech_teil.zahl_dat,2,0);
    out_quest ((char *) &rech_teil.waehrung,1,0);
    out_quest ((char *) &rech_teil.rech_bez_eur,3,0);
    out_quest ((char *) &rech_teil.rech_bez_fremd,3,0);
    out_quest ((char *) rech_teil.pers_nam,0,9);
            cursor = prepare_sql ("select "
"rech_teil.delstatus,  rech_teil.fil,  rech_teil.kun,  rech_teil.mdn,  "
"rech_teil.rech_nr,  rech_teil.rech_bez,  rech_teil.blg_typ,  "
"rech_teil.krz_txt,  rech_teil.zahl_art,  rech_teil.zahl_dat,  "
"rech_teil.waehrung,  rech_teil.rech_bez_eur,  "
"rech_teil.rech_bez_fremd,  rech_teil.pers_nam from rech_teil "

#line 22 "rech_teil.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   rech_nr = ?" );

    ins_quest ((char *) &rech_teil.delstatus,1,0);
    ins_quest ((char *) &rech_teil.fil,1,0);
    ins_quest ((char *) &rech_teil.kun,2,0);
    ins_quest ((char *) &rech_teil.mdn,1,0);
    ins_quest ((char *) &rech_teil.rech_nr,2,0);
    ins_quest ((char *) &rech_teil.rech_bez,3,0);
    ins_quest ((char *) rech_teil.blg_typ,0,2);
    ins_quest ((char *) rech_teil.krz_txt,0,17);
    ins_quest ((char *) &rech_teil.zahl_art,1,0);
    ins_quest ((char *) &rech_teil.zahl_dat,2,0);
    ins_quest ((char *) &rech_teil.waehrung,1,0);
    ins_quest ((char *) &rech_teil.rech_bez_eur,3,0);
    ins_quest ((char *) &rech_teil.rech_bez_fremd,3,0);
    ins_quest ((char *) rech_teil.pers_nam,0,9);
            sqltext = "update rech_teil set "
"rech_teil.delstatus = ?,  rech_teil.fil = ?,  rech_teil.kun = ?,  "
"rech_teil.mdn = ?,  rech_teil.rech_nr = ?,  rech_teil.rech_bez = ?,  "
"rech_teil.blg_typ = ?,  rech_teil.krz_txt = ?,  "
"rech_teil.zahl_art = ?,  rech_teil.zahl_dat = ?,  "
"rech_teil.waehrung = ?,  rech_teil.rech_bez_eur = ?,  "
"rech_teil.rech_bez_fremd = ?,  rech_teil.pers_nam = ? "

#line 27 "rech_teil.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   rech_nr = ?";

            ins_quest ((char *) &rech_teil.mdn, 1, 0);
            ins_quest ((char *) &rech_teil.fil, 1, 0);
            ins_quest ((char *) &rech_teil.rech_nr, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &rech_teil.mdn, 1, 0);
            ins_quest ((char *) &rech_teil.fil, 1, 0);
//            ins_quest ((char *) &rech_teil.rech_nr, 2, 0);
            test_upd_cursor = prepare_sql ("select rech_nr from rech_teil "
                                  "where mdn = ? "
                                  "and   fil = ? "
//                                  "and   rech_nr = ? "
                                  "and   rech_nr = -2 "
                                  "for update");

            ins_quest ((char *) &rech_teil.mdn, 1, 0);
            ins_quest ((char *) &rech_teil.fil, 1, 0);
            ins_quest ((char *) &rech_teil.rech_nr, 2, 0);
            del_cursor = prepare_sql ("delete from rech_teil "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   rech_nr = ?");

    ins_quest ((char *) &rech_teil.delstatus,1,0);
    ins_quest ((char *) &rech_teil.fil,1,0);
    ins_quest ((char *) &rech_teil.kun,2,0);
    ins_quest ((char *) &rech_teil.mdn,1,0);
    ins_quest ((char *) &rech_teil.rech_nr,2,0);
    ins_quest ((char *) &rech_teil.rech_bez,3,0);
    ins_quest ((char *) rech_teil.blg_typ,0,2);
    ins_quest ((char *) rech_teil.krz_txt,0,17);
    ins_quest ((char *) &rech_teil.zahl_art,1,0);
    ins_quest ((char *) &rech_teil.zahl_dat,2,0);
    ins_quest ((char *) &rech_teil.waehrung,1,0);
    ins_quest ((char *) &rech_teil.rech_bez_eur,3,0);
    ins_quest ((char *) &rech_teil.rech_bez_fremd,3,0);
    ins_quest ((char *) rech_teil.pers_nam,0,9);
            ins_cursor = prepare_sql ("insert into rech_teil ("
"delstatus,  fil,  kun,  mdn,  rech_nr,  rech_bez,  blg_typ,  krz_txt,  zahl_art,  "
"zahl_dat,  waehrung,  rech_bez_eur,  rech_bez_fremd,  pers_nam) "

#line 55 "rech_teil.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?)"); 

#line 57 "rech_teil.rpp"
}
int RECH_TEIL_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

