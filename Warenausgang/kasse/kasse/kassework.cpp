#include <windows.h>
#include <stdio.h>
#include "KasseWork.h"
#include "strfkt.h"
#include "mo_menu.h"
#include "Text.h"

#define TLEN 60

class VPtab
{
  public :
	  char      ptitem[19];
      long      ptlfnr;
      char      ptwert[4];
      char      ptbez[33];
      char      ptbezk[9];
      char      ptwer1[9];
      char      ptwer2[9];
	  VPtab ();
	  ~VPtab ();
      const VPtab& operator=(struct PTABN *);
	  void CopyPtab (struct PTABN *);
};

VPtab::VPtab ()
{
}
	  
VPtab::~VPtab ()
{
}

const VPtab& VPtab::operator=(struct PTABN *ptabn)
{
	strcpy (ptitem, ptabn->ptitem);
	ptlfnr = ptabn->ptlfnr;
	strcpy (ptwert, ptabn->ptwert);
	strcpy (ptbez,  ptabn->ptbez);
	strcpy (ptbezk, ptabn->ptbezk);
	strcpy (ptwer1, ptabn->ptwer1);
	strcpy (ptwer2, ptabn->ptwer2);
	return *this;
}

void VPtab::CopyPtab (struct PTABN *ptabn)
{
	strcpy (ptabn->ptitem, ptitem);
	ptabn->ptlfnr = ptlfnr;
	strcpy (ptabn->ptwert, ptwert);
	strcpy (ptabn->ptbez,  ptbez);
	strcpy (ptabn->ptbezk, ptbezk);
	strcpy (ptabn->ptwer1, ptwer1);
	strcpy (ptabn->ptwer2, ptwer2);
}


int KasseWork::GetKunName (char *dest, short mdn, long kun)
{
    int dsqlstatus;
    int cursor;

    dest[0] = 0;


    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlin ((long *)  &kun, 2, 0);
    DbClass.sqlout ((char *) dest, 0, 17);
    DbClass.sqlout ((short *) &zahl_art, 1, 0);
    DbClass.sqlout ((double *) &kred_lim, 3, 0);

    cursor = DbClass.sqlcursor ("select adr.adr_krz, zahl_art, kred_lim from kun,adr "
                                "where kun.mdn = ? " 
                                "and   kun.kun = ? "
                                "and adr.adr = kun.adr1");
    dsqlstatus = DbClass.sqlfetch (cursor);
    DbClass.sqlclose (cursor);
	return dsqlstatus;
}

int KasseWork::GetRech (short mdn, long kun) 
{
    int dsqlstatus;
    int cursor;
	char datum [12];

	this->kun = kun;
	sysdate (datum);
    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlin ((long *)  &kun, 2, 0);
	DbClass.sqlin ((char *)  datum, 0, 11);
	DbClass.sqlout ((long *)   &rech_nr, 2, 0);
	DbClass.sqlout ((double *) &rech_bto_eur, 3, 0);
	DbClass.sqlout ((double *) &rech_bez_eur, 3, 0);
	DbClass.sqlout ((char *) blg_typ, 0, 2);
	cursor = DbClass.sqlcursor ("select rech_nr, rech_bto_eur, rech_bez_eur,"
		                        "blg_typ from rech "
		                        "where mdn = ? "
								"and fil = 0 "
								"and kun = ? "
								"and rech_dat = ? "
								"and rech_stat = 1");
	dsqlstatus = DbClass.sqlfetch (cursor);
    DbClass.sqlclose (cursor);
    DbClass.sqlin ((short *) &mdn, 1, 0);
	DbClass.sqlin ((long *)   &rech_nr, 2, 0);
	DbClass.sqlout ((long *)   &ls, 2, 0);
	cursor = DbClass.sqlcursor ("select ls from lsk "
		                        "where mdn = ? "
								"and fil = 0 "
								"and rech = ?");
	ls = 0l;
	DbClass.sqlfetch (cursor);
    DbClass.sqlclose (cursor);
    return dsqlstatus;
}

int KasseWork::GetRechNr (short mdn, long rech_nr) 
{
    int dsqlstatus;
    int cursor;
	char datum [12];

	sysdate (datum);
    DbClass.sqlin ((short *) &mdn, 1, 0);
    DbClass.sqlin ((long *)  &rech_nr, 2, 0);
//	DbClass.sqlin ((char *)  datum, 0, 11);
	DbClass.sqlout ((long *)   &kun, 2, 0);
	DbClass.sqlout ((double *) &rech_bto_eur, 3, 0);
	DbClass.sqlout ((double *) &rech_bez_eur, 3, 0);
	DbClass.sqlout ((char *) blg_typ, 0, 2);
	cursor = DbClass.sqlcursor ("select kun, rech_bto_eur, rech_bez_eur,"
		                        "blg_typ from rech "
		                        "where mdn = ? "
								"and fil = 0 "
								"and rech_nr = ? "
//								"and rech_dat = ? "
								"and rech_stat = 1");
	dsqlstatus = DbClass.sqlfetch (cursor);
    DbClass.sqlclose (cursor);
	if (dsqlstatus != 0)
	{
               return dsqlstatus;
	}
	this->rech_nr = rech_nr;
    DbClass.sqlin ((short *) &mdn, 1, 0);
	DbClass.sqlin ((long *)   &rech_nr, 2, 0);
	DbClass.sqlout ((long *)   &ls, 2, 0);
	cursor = DbClass.sqlcursor ("select ls from lsk "
		                        "where mdn = ? "
								"and fil = 0 "
								"and rech = ?");
	ls = 0l;
	DbClass.sqlfetch (cursor);
    DbClass.sqlclose (cursor);
    return dsqlstatus;
}

BOOL KasseWork::UpdateRech (short mdn, long rech_nr)
{
	RECH_TEIL_CLASS RechTeil;
	short rech_stat;
	char datum[12];

	double konversion = 0.0;
	short sqls;
	extern short sql_mode;

	sqls = sql_mode;
	if (konversion == 0.0)
	{
		DbClass.sqlin ((short *) &mdn, 1, 0);
		DbClass.sqlout ((double *) &konversion, 3, 0);
		DbClass.sqlcomm ("select konversion from mdn where mdn = ?");
	}
	BeginWork ();
	this->rech_nr = rech_nr;
	rech_bez_eur += bezahlt;
	if (rech_bez_eur < 0.0 ||
		rech_bto_eur < 0.0)
	{
		disp_mess ("Auszahlung ist nicht erlaubt", 2);
		return FALSE;
	}

	if (rech_bez_eur < rech_bto_eur &&
		kred_lim > 1.0)
	{
		disp_mess ("Der Betrag mu� vollst�ndig bezahlt werden", 2);
		return FALSE;
	}

	rech_bez = rech_bez_eur * konversion;
	if (rech_bez_eur >= rech_bto_eur)
	{
		rech_stat = 3;
	}
	else
	{
		rech_stat = 1;
	}
	DbClass.cleanin ();
	DbClass.cleanout ();
	sysdate (datum);
	beginwork ();
//	DbClass.sqlin ((double *) &rech_bez_eur, 3, 0);

	sql_mode = 1;
	DbClass.sqlin ((double *) &rech_bez_eur, 3, 0);
	DbClass.sqlin ((double *) &rech_bez, 3, 0);
	DbClass.sqlin ((short *)  &zahl_art, 1, 0);
	DbClass.sqlin ((char *)   datum, 0, 11);
	DbClass.sqlin ((short *)  &rech_stat, 1, 0);
    DbClass.sqlin ((short *)  &mdn, 1, 0);
	DbClass.sqlin ((long *)   &rech_nr, 2, 0);
	int cursor = DbClass.sqlcursor 
		            ("update rech set rech_bez_eur = ?, rech_bez = ?, "
		             "zahl_art = ?, zahl_dat = ?, "
		             "rech_stat = ? "
		             "where mdn = ? "
					 "and rech_nr = ?");
	int dsqlstatus = DbClass.sqlexecute (cursor);
	DbClass.sqlclose (cursor);
	sql_mode = sqls;
	if (dsqlstatus < 0)
	{
		disp_mess ("Der Datensatz wird im Moment bearbeitet", 2);
		return FALSE;
	}

    rech_teil.mdn = mdn;
	rech_teil.fil = 0;
	rech_teil.kun = kun;
	rech_teil.rech_nr = rech_nr;
    RechTeil.dbreadfirst ();

	rech_teil.rech_bez = bezahlt;
	rech_teil.rech_bez_eur = bezahlt;
	strcpy (rech_teil.blg_typ, blg_typ);
	rech_teil.zahl_art = zahl_art;
	rech_teil.delstatus = 0;
	rech_teil.zahl_dat = dasc_to_long (datum);
	strcpy (rech_teil.pers_nam, sys_ben.pers_nam);
    RechTeil.dbupdate ();
	CommitWork ();
    return TRUE;
}

int KasseWork::GetPtabClass (char *dest, char *item, char *wert)
{
	VPtab *vptab;

    ptabn.ptlfnr = 1;
    dest[0] = 0;
	Ptabs.FirstPosition ();
	while ((vptab = (VPtab *) Ptabs.GetNext ()) != NULL)
	{
		Text Wert = wert;
		Text Ptwert = vptab->ptwert;
		Wert.Trim ();
		Ptwert.Trim ();
		if (Ptwert == Wert)
		{
			break;
		}
	}
	if (vptab == NULL)
	{
		return -1;
	}

	vptab->CopyPtab (&ptabn);
    strcpy (dest, vptab->ptbezk);
    return 0;
}


int KasseWork::GetPtab (char *dest, char *item, char *wert)
{
    int dsqlstatus;

    ptabn.ptlfnr = 1;
    dest[0] = 0;
    dsqlstatus = Ptab.lese_ptab (item, wert);  

    if (dsqlstatus == 100)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptbezk);
    return 0;
}

char *KasseWork::GetPtWertClass (char *item, short lfd)
{
	VPtab *vptab;

	Ptabs.FirstPosition ();
	while ((vptab = (VPtab *) Ptabs.GetNext ()) != NULL)
	{
		if (vptab->ptlfnr == lfd)
		{
			break;
		}
	}
	if (vptab == NULL)
	{
		return "";
	}

	vptab->CopyPtab (&ptabn);
    return (vptab->ptwert);
}


char *KasseWork::GetPtWert (char *item, short lfd)
{
    int dsqlstatus;

    DbClass.sqlout ((char *)  ptabn.ptwert, 0, 4);
    DbClass.sqlin  ((char *)  item, 0, 20);
    DbClass.sqlin  ((short *) &lfd, 1, 0);
    dsqlstatus = DbClass.sqlcomm ("select ptwert from ptabn "
                                  "where ptitem = ? and ptlfnr = ?");
    if (dsqlstatus == 0)
    {
        return ptabn.ptwert;
    }
    return "";
}


char *KasseWork::GetPtWer1Class (char *item, short lfd)
{
	VPtab *vptab;

	Ptabs.FirstPosition ();
	while ((vptab = (VPtab *) Ptabs.GetNext ()) != NULL)
	{
		if (vptab->ptlfnr == lfd)
		{
			break;
		}
	}
	if (vptab == NULL)
	{
		return "";
	}

	vptab->CopyPtab (&ptabn);
    return (vptab->ptwer1);
}


char *KasseWork::GetPtWer1 (char *item, short lfd)
{
    int dsqlstatus;

    DbClass.sqlout ((char *)  ptabn.ptwer1, 0, 9);
    DbClass.sqlin  ((char *)  item, 0, 20);
    DbClass.sqlin  ((short *) &lfd, 1, 0);
    dsqlstatus = DbClass.sqlcomm ("select ptwer1 from ptabn "
                                  "where ptitem = ? and ptlfnr = ?");
    if (dsqlstatus == 0)
    {
        return ptabn.ptwer1;
    }
    return "";
}

char *KasseWork::GetPtWer2Class (char *item, short lfd)
{
	VPtab *vptab;

	Ptabs.FirstPosition ();
	while ((vptab = (VPtab *) Ptabs.GetNext ()) != NULL)
	{
		if (vptab->ptlfnr == lfd)
		{
			break;
		}
	}
	if (vptab == NULL)
	{
		return "";
	}

	vptab->CopyPtab (&ptabn);
    return (vptab->ptwer2);
}


char *KasseWork::GetPtWer2 (char *item, short lfd)
{
    int dsqlstatus;

    DbClass.sqlout ((char *)  ptabn.ptwer2, 0, 9);
    DbClass.sqlin  ((char *)  item, 0, 20);
    DbClass.sqlin  ((short *) &lfd, 1, 0);
    dsqlstatus = DbClass.sqlcomm ("select ptwer2 from ptabn "
                                  "where ptitem = ? and ptlfnr = ?");
    if (dsqlstatus == 0)
    {
        return ptabn.ptwer2;
    }
    return "";
}

int KasseWork::ShowPtab (char *dest, char *item)
{
    int dsqlstatus;

    dsqlstatus = Ptab.Show (item);  
    if (syskey == KEY5)
    {
        return -1;
    }
    strcpy (dest, ptabn.ptwert);
    return 0;
}


void KasseWork::FillZahlArtCombo (char **Combo, int anz)
{
    int dsqlstatus;
    char *anr;

    Ptabs.Init ();
	Ptabs.DestroyAll (); 
    int i = 0;
    dsqlstatus = Ptab.lese_ptab_all ("zahl_art");
    while (dsqlstatus == 0)
    {
		if (atoi (ptabn.ptwer1) > 1)
		{
            dsqlstatus = Ptab.lese_ptab_all ();
			continue;
		}
		else if (atoi (ptabn.ptwer1) == 1 &&
			     kred_lim > 1.0)
		{
            dsqlstatus = Ptab.lese_ptab_all ();
			continue;
		}
        anr = new char [20];
        if (anr == NULL) return;
        strcpy (anr, ptabn.ptbezk);
        Combo[i] = anr;
		VPtab *vptab = new VPtab ();
		*vptab = &ptabn;
		vptab->ptlfnr = i + 1;
        Ptabs.Add (vptab);
        i ++;
        if (i ==anz) break;
        dsqlstatus = Ptab.lese_ptab_all ();
    }
    Combo[i] = NULL;
}

void KasseWork::FillCombo (char **Combo, char *Item, int anz)
{
    int dsqlstatus;
    char *anr;
    
    int i = 0;
    dsqlstatus = Ptab.lese_ptab_all (Item);
    while (dsqlstatus == 0)
    {
        anr = new char [20];
        if (anr == NULL) return;
        strcpy (anr, ptabn.ptbezk);
        Combo[i] = anr;
        i ++;
        if (i ==anz) break;
        dsqlstatus = Ptab.lese_ptab_all ();
    }
    Combo[i] = NULL;
}

void KasseWork::FillComboLong (char **Combo, char *Item, int anz)
{
    int dsqlstatus;
    char *anr;
    
    int i = 0;
    dsqlstatus = Ptab.lese_ptab_all (Item);
    while (dsqlstatus == 0)
    {
        anr = new char [256];
        if (anr == NULL) return;
        strcpy (anr, ptabn.ptbez);
        Combo[i] = anr;
        i ++;
        if (i ==anz) break;
        dsqlstatus = Ptab.lese_ptab_all ();
    }
    Combo[i] = NULL;
}

int KasseWork::CutLines (char *txt, char *txtout, int zei, int cursor)
{
	   char *params [5];
	   char zeile[5];
	   char tlen[5];

	   if (strlen (txt) <= TLEN)
	   {
		   cr_weg (txtout);
	       strcpy (txtout, txt);
           DbClass.sqlexecute (cursor);
		   return (zei + 1);
	   }

	   params[0] = txt;
	   params[1] = txtout;
	   sprintf (zeile, "1");
	   sprintf (tlen, "%d", TLEN);
	   params[2] = tlen;
	   params[3] = zeile;
	   params[4] = NULL;
	   while (getline (params))
	   {
		   cr_weg (txtout);
           DbClass.sqlexecute (cursor);
		   zei ++;
		   sprintf (zeile, "%d", atoi (zeile) + 1);
	   }
	   return zei;
}

void KasseWork::BeginWork (void)
{
    ::beginwork ();
}

void KasseWork::CommitWork (void)
{
    ::commitwork ();
}

void KasseWork::RollbackWork (void)
{
    ::rollbackwork ();
}

