#include <windows.h>
#include "searchkun.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "Text.h"


struct SKUN *SEARCHKUNRC::skuntab = NULL;
struct SKUN SEARCHKUNRC::skun;
int SEARCHKUNRC::idx = -1;
long SEARCHKUNRC::kunanz;
CHQEX *SEARCHKUNRC::Query = NULL;
DB_CLASS SEARCHKUNRC::DbClass; 
HINSTANCE SEARCHKUNRC::hMainInst;
HWND SEARCHKUNRC::hMainWindow;
HWND SEARCHKUNRC::awin;
int SEARCHKUNRC::SearchField = 1;
int SEARCHKUNRC::sokun = 1; 
int SEARCHKUNRC::soadr_nam1 = 1; 
int SEARCHKUNRC::soplz = 1; 
int SEARCHKUNRC::soort1 = 1; 
short SEARCHKUNRC::mdn = 0; 
static BOOL ScannMode = TRUE;

int SEARCHKUNRC::sortkun (const void *elem1, const void *elem2)
{
	      struct SKUN *el1; 
	      struct SKUN *el2; 

	      el1 = (struct SKUN *) elem1;
		  el2 = (struct SKUN *) elem2;
          if (atol (el1->kun) > atol (el2->kun))
          {
              return 1 * sokun;
          }
          if (atol (el1->kun) < atol (el2->kun))
          {
              return -1 * sokun;
          }
	      return 0 * sokun;
}

void SEARCHKUNRC::SortKun (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUN),
				   sortkun);
//       sokun *= -1;
}

int SEARCHKUNRC::sortadr_nam1 (const void *elem1, const void *elem2)
{
	      struct SKUN *el1; 
	      struct SKUN *el2; 

		  el1 = (struct SKUN *) elem1;
		  el2 = (struct SKUN *) elem2;
          clipped (el1->adr_nam1);
          clipped (el2->adr_nam1);
          if (strlen (el1->adr_nam1) == 0 &&
              strlen (el2->adr_nam1) == 0)
          {
              return 0;
          }
          if (strlen (el1->adr_nam1) == 0)
          {
              return -1;
          }
          if (strlen (el2->adr_nam1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->adr_nam1,el2->adr_nam1) * soadr_nam1);
}

void SEARCHKUNRC::SortAdr_nam1 (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUN),
				   sortadr_nam1);
       soadr_nam1 *= -1;
	   ScannMode = FALSE;
 	   Query->CheckName ("check", ScannMode);
}

int SEARCHKUNRC::sortplz (const void *elem1, const void *elem2)
{
	      struct SKUN *el1; 
	      struct SKUN *el2; 

		  el1 = (struct SKUN *) elem1;
		  el2 = (struct SKUN *) elem2;
          clipped (el1->plz);
          clipped (el2->plz);
          if (strlen (el1->plz) == 0 &&
              strlen (el2->plz) == 0)
          {
              return 0;
          }
          if (strlen (el1->plz) == 0)
          {
              return -1;
          }
          if (strlen (el2->plz) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->plz,el2->plz) * soplz);
}

void SEARCHKUNRC::SortPlz (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUN),
				   sortplz);
       soplz *= -1;
	   ScannMode = FALSE;
 	   Query->CheckName ("check", ScannMode);
}

int SEARCHKUNRC::sortort1 (const void *elem1, const void *elem2)
{
	      struct SKUN *el1; 
	      struct SKUN *el2; 

		  el1 = (struct SKUN *) elem1;
		  el2 = (struct SKUN *) elem2;
          clipped (el1->ort1);
          clipped (el2->ort1);
          if (strlen (el1->ort1) == 0 &&
              strlen (el2->ort1) == 0)
          {
              return 0;
          }
          if (strlen (el1->ort1) == 0)
          {
              return -1;
          }
          if (strlen (el2->ort1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->ort1,el2->ort1) * soort1);
}

void SEARCHKUNRC::SortOrt1 (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUN),
				   sortort1);
       soort1 *= -1;
	   ScannMode = FALSE;
 	   Query->CheckName ("check", ScannMode);
}

void SEARCHKUNRC::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortKun (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortAdr_nam1 (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortPlz (hWnd);
                  SearchField = 2;
                  break;
              case 3 :
                  SortOrt1 (hWnd);
                  SearchField = 3;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHKUNRC::FillFormat (char *buffer)
{
         sprintf (buffer, "%s %s %s %s %s",
                          "%d",
						  "%s",
						  "%d",
						  "%s",
                          "%s");
}


void SEARCHKUNRC::FillCaption (char *buffer)
{
	    sprintf (buffer, " %10s  %-27s  %-10s  %-21s  %-37s", " Kunde", 
                          " Name1", 
						  " Rechnung",
                          " PLZ", 
                          "  Ort"); 
}

void SEARCHKUNRC::FillVlines (char *buffer) 
{
	    sprintf (buffer, " %11s  %28s  %11s  %22s  %38s", 
                         "1", "1", "1", "1"); 
}


void SEARCHKUNRC::FillRec (char *buffer, int i)
{
 	      sprintf (buffer, " %8ld     |%-26.26s|    %8ld    |%-20s|    |%-36s|", 
                            atol (skuntab[i].kun), 
                            skuntab[i].adr_nam1,
							atol (skuntab[i].rech),
                            skuntab[i].plz,
                            skuntab[i].ort1);
}


void SEARCHKUNRC::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < kunanz; i ++)
       {
          FillRec (buffer, i);
	      Query->UpdateRecord (buffer, i);
       }
}


int SEARCHKUNRC::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (skuntab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < kunanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
                   strcpy (sabuff, skuntab[i].kun);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (atol (sebuff) == atol (sabuff)) break;
           }
           else if (SearchField == 1)
           {
		           if (strupcmp (sebuff, skuntab[i].adr_nam1, len) == 0) break;
           }
           else if (SearchField == 2)
           {
		           if (strupcmp (sebuff, skuntab[i].plz, len) == 0) break;
           }
           else if (SearchField == 3)
           {
		           if (strupcmp (sebuff, skuntab[i].ort1, len) == 0) break;
           }
	   }
	   if (i == kunanz) return 0;
	   Query->SetSel (i);
	   return 0;
}

int SEARCHKUNRC::ReadKun (char *kun_krz1)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (skuntab) 
	  {
           delete skuntab;
           skuntab = NULL;
	  }


      idx = -1;
	  kunanz = 0;
//      SearchField = 1;
      SearchField = 0;
      Query->SetSBuff ("");
      clipped (kun_krz1);
      if (strcmp (kun_krz1, " ") == 0) kun_krz1[0] = 0;

      sprintf (buffer, "select count (*) from kun where kun_krz1 matches \"%s*\"", kun_krz1);
      DbClass.sqlout ((int *) &kunanz, 2, 0);
	  DbClass.sqlcomm (buffer);

      if (kunanz == 0) kunanz = 0x10000;

	  skuntab = new struct SKUN [kunanz];
      clipped (kun_krz1);
/*
      if (strlen (kun_krz1) == 0 ||
          kun_krz1[0] <= ' ')
*/
      {
	          sprintf (buffer, "select rech.kun, rech.rech_nr, adr.adr_nam1, adr.plz, adr.ort1 "
	 			       "from rech, kun, adr "
                       "where rech.rech_dat = today "
					   "and rech.rech_stat = 1 "
					   "and kun.kun = rech.kun "
					   "and kun.mdn = rech.mdn "
					   "and kun.mdn = %hd "
                       "and adr.adr = kun.adr1", mdn); 
      }
/*
      else
      {
	          sprintf (buffer, "select kun, adr.adr_nam1, adr.plz, adr.ort1 "
	 			       "from kun, adr "
                       "where kun.mdn = %hd "
                       "and adr.adr = kun.adr1 "
                       "and kun_krz1 matches \"%s*\"", mdn, kun_krz1);
      }
*/
      DbClass.sqlout ((char *) skun.kun, 0, 9);
      DbClass.sqlout ((char *) skun.rech, 0, 9);
      DbClass.sqlout ((char *) skun.adr_nam1, 0, 37);
      DbClass.sqlout ((char *) skun.plz, 0, 21);
      DbClass.sqlout ((char *) skun.ort1, 0, 37);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		  memcpy (&skuntab[i], &skun, sizeof (struct SKUN));
          FillRec (buffer, i);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      kunanz = i;
	  DbClass.sqlclose (cursor);
//      soadr_nam1 = 1;
      sokun = 1;
      if (kunanz > 1)
      {
//             SortAdr_nam1 (Query->GethWnd ());
             SortKun (Query->GethWnd ());
      }
      UpdateList ();
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHKUNRC::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < kunanz; i ++)
      {
          FillRec (buffer, i);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHKUNRC::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHKUNRC::SearchKun (short mdn)
{
	  char buffer [256];
      RECT rect;

      if (mdn == 0)
      {
          disp_mess ("Mandant 0 ist nicht erlaubt", 2);
          syskey = KEY5;
          return;
      }
      this->mdn = mdn;
      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      SetSortProc (SortLst);
//      EnableWindow (hMainWindow, FALSE);
      if (Query == NULL)
      {
        idx = -1;
//        Query = new CHQEX (cx, cy, "Name", "");
        Query = new CHQEX (cx, cy, "Scann-Modus");
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (ReadKun);
	    Query->SetSearchLst (SearchLst);
		ReadKun ("");
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (ReadKun);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
	  Query->SetSBuffSet (FALSE);
	  Query->CheckName ("check", ScannMode);
	  Query->ProcessMessages ();
//      EnableWindow (hMainWindow, TRUE);
      idx = Query->GetSel ();
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (skuntab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) 
	  {
 	      delete skuntab;
          skuntab = NULL;
          delete Query;
          Query = NULL;
		  return;
	  }
	  memcpy (&skun, &skuntab[idx], sizeof (skun));
      ScannMode = Query->GetCheckBoxValue ();
	  if (ScannMode)
	  {
	      char *s = Query->GetSBuff ();
		  Text Sb = s;
		  Text Kun = skun.kun;
		  Sb.TrimRight ();
		  Kun.TrimRight ();
		  if (Sb > " " && Sb != Kun)
		  {
			  print_mess (2, "Falsche Kundennummer %ld", atol (Sb.GetBuffer ()));
			  idx = -1;
		  }
	  }
      SetSortProc (NULL);
	  delete skuntab;
      skuntab = NULL;
      delete Query;
      Query = NULL;
/*
      if (skuntab != NULL)
      {
          Query->SavefWork ();
      }
*/
}


SKUN *SEARCHKUNRC::GetNextSkun (void)
{
      if (idx == -1) return NULL;
      if (idx < kunanz - 1) 
      {
             idx ++;
      }
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}


SKUN *SEARCHKUNRC::GetPriorSkun (void)
{
      if (idx == -1) return NULL;
      if (idx > 0) 
      {
             idx --;
      }
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}

SKUN *SEARCHKUNRC::GetFirstSkun (void)
{
      if (idx == -1) return NULL;
      idx = 0;
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}

SKUN *SEARCHKUNRC::GetLastSkun (void)
{
      if (idx == -1) return NULL;
      idx = kunanz - 1;
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}



