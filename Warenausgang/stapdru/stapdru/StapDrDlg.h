#pragma once
#include "afxwin.h"

#define KUNSTD 2
#define FILSTD 1

extern char globnutzer[ ] ;
// CStapDrDlg-Formularansicht

class CStapDrDlg : public CFormView
{
	DECLARE_DYNCREATE(CStapDrDlg)

protected:
	CStapDrDlg();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CStapDrDlg();

public:
	enum { IDD = IDD_STAPDRDLG };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void OnInitialUpdate();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_mandant;
public:
	CString v_mandant;
public:
	CEdit m_mdnname;
public:
	CString v_mdnname;
public:
	CButton c_kunbutt;
public:
	CButton c_filbutt;
public:
	BOOL v_kunbutt;
public:
	BOOL v_filbutt;
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedCheckkun();
public:
	afx_msg void OnBnClickedCheckfil();
public:
	afx_msg void OnEnKillfocusMandant();
public:
	afx_msg void OnEnKillfocusDatum();
public:
	afx_msg void OnEnKillfocusStapnu();
public:
	afx_msg void OnBnClickedCancel();
public:
//	afx_msg void OnBnClickedAusdruck();
 	afx_msg BOOL PreTranslateMessage(LPMSG) ;
public:
	void ReadMdn(void) ;
	void StapelDruck(void) ;
	void StapelEinzelDruck(long) ;
	virtual BOOL OnReturn (void);
	virtual BOOL OnKeyup (void);

public:

	CEdit m_datum;
	CString v_datum;
	CEdit m_stapnu;
	CString v_stapnu;
	CButton c_einzelbutt;
	BOOL v_einzelbutt;
	afx_msg void OnBnClickedEinzel();
	CButton m_einzkufi;
	BOOL v_einzkufi;
	afx_msg void OnBnClickedEinzkufi();
	CEdit m_bisdatum;
	CString v_bisdatum;
	CEdit m_kunde;
	long v_kunde;
	CEdit m_filiale;
	short v_filiale;
	afx_msg void OnEnKillfocusBisdatum();
	CEdit m_aufl;
	int v_aufl;
	afx_msg void OnEnKillfocusAufl();
	CEdit m_ebd;
	CString v_ebd;
	afx_msg void OnEnKillfocusEinzelbel();
	afx_msg void OnBnClickedEbd();
};


