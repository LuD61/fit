// DruckDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "stapdru.h"
#include "DruckDialog.h"

extern 	DWORD ProcWaitExec( LPSTR , WORD , int, int, int, int );


// CDruckDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CDruckDialog, CDialog)

CDruckDialog::CDruckDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CDruckDialog::IDD, pParent)
{

}

CDruckDialog::~CDruckDialog()
{
}

void CDruckDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDruckDialog, CDialog)
END_MESSAGE_MAP()


// CDruckDialog-Meldungshandler
void CDruckDialog::DruckWahl(void)
{
	char s1[199] ;
	char s2[199] ;

	sprintf ( s1, "dr70001.exe -NAME T53100 -WAHL" );
	int ex_code = ProcWaitExec( (char *)s1, SW_SHOW, -1, 0, -1, 0 );

	sprintf ( s1, "%s\\format\\ll\\T53100.lsp", getenv("BWS") ) ;
    sprintf ( s2, "%s\\format\\ll\\T53101.lsp", getenv("BWS") ) ;
	CopyFile ( s1, s2, FALSE ) ;	// immer ueberschreiben 
	;
}
