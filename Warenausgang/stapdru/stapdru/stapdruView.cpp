// stapdruView.cpp : Implementierung der Klasse CstapdruView
//

#include "stdafx.h"
#include "stapdru.h"

#include "stapdruDoc.h"
#include "stapdruView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CstapdruView

IMPLEMENT_DYNCREATE(CstapdruView, CView)

BEGIN_MESSAGE_MAP(CstapdruView, CView)
	// Standarddruckbefehle
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CstapdruView-Erstellung/Zerst�rung

CstapdruView::CstapdruView()
{
	// TODO: Hier Code zur Konstruktion einf�gen

}

CstapdruView::~CstapdruView()
{
}

BOOL CstapdruView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CView::PreCreateWindow(cs);
}

// CstapdruView-Zeichnung

void CstapdruView::OnDraw(CDC* /*pDC*/)
{
	CstapdruDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: Code zum Zeichnen der systemeigenen Daten hinzuf�gen
}


// CstapdruView drucken

BOOL CstapdruView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// Standardvorbereitung
	return DoPreparePrinting(pInfo);
}

void CstapdruView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Zus�tzliche Initialisierung vor dem Drucken hier einf�gen
}

void CstapdruView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: Bereinigung nach dem Drucken einf�gen
}


// CstapdruView-Diagnose

#ifdef _DEBUG
void CstapdruView::AssertValid() const
{
	CView::AssertValid();
}

void CstapdruView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CstapdruDoc* CstapdruView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CstapdruDoc)));
	return (CstapdruDoc*)m_pDocument;
}
#endif //_DEBUG


// CstapdruView-Meldungshandler
