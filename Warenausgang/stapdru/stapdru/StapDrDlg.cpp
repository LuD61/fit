// StapDrDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "stapdru.h"
#include "StapDrDlg.h"
#include "DbClass.h"
#include "mdn.h"
#include "adr.h"
#include "kun.h"
#include "ls.h"
#include "sys_par.h"


DB_CLASS dbClass ;	// Hier ist die Heimat der dbClass, alle Tabellen sind in ihrem jeweiligen Modul daheim
extern MDN_CLASS mdn_class ;
extern ADR_CLASS adr_class ;
extern LSK_CLASS lsk_class ;
extern LSP_CLASS lsp_class ;
extern KUN_CLASS kun_class ;
extern FIL_CLASS fil_class ;
extern SYS_PAR_CLASS sys_par_class ;
extern KUNBRTXTZU_CLASS kunbrtxtzu_class ;	// 170608 


char globnutzer [256] ;

char bufh[256] ;

#define IMAXDIM 1000 

int stapnu ;

int dnachkpreis ;

long schlussmat[IMAXDIM] ;	// Array LS-Nummern
long schluss2mat[IMAXDIM] ;	// Array Kunden-Nummern zum LS
int eigmat[IMAXDIM] ;		// Array LS-Status-Infos

// Status : == 0	// da war was, aber bitte nichts mehr machen 
// Status : == 3	// es war komplett 
// Status : == 4	// es war gedruckt
// Status : == -1	// leer 
// Status : == -2	// es gibt probleme , am besten ignorieren

// StapDrDlg

IMPLEMENT_DYNCREATE(CStapDrDlg, CFormView)

CStapDrDlg::CStapDrDlg()
	: CFormView(CStapDrDlg::IDD)
	, v_mandant(_T(""))
	, v_mdnname(_T(""))
	, v_kunbutt(FALSE)
	, v_filbutt(FALSE)
	, v_datum(_T(""))
	, v_stapnu(_T(""))
	, v_einzelbutt(FALSE)
	, v_einzkufi(FALSE)
	, v_bisdatum(_T(""))
	, v_kunde(0)
	, v_filiale(0)
	, v_aufl(0)
	, v_ebd (_T(""))
{

}

CStapDrDlg::~CStapDrDlg()
{
}

void CStapDrDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MANDANT, m_mandant);
	DDX_Text(pDX, IDC_MANDANT, v_mandant);
	DDV_MaxChars(pDX, v_mandant, 4);
	DDX_Control(pDX, IDC_MDNNAME, m_mdnname);
	DDX_Text(pDX, IDC_MDNNAME, v_mdnname);
	DDX_Control(pDX, IDC_CHECKKUN, c_kunbutt);
	DDX_Control(pDX, IDC_CHECKFIL, c_filbutt);
	DDX_Check(pDX, IDC_CHECKKUN, v_kunbutt);
	DDX_Check(pDX, IDC_CHECKFIL, v_filbutt);
	DDX_Control(pDX, IDC_DATUM, m_datum);
	DDX_Text(pDX, IDC_DATUM, v_datum);
	DDX_Control(pDX, IDC_STAPNU, m_stapnu);
	DDX_Text(pDX, IDC_STAPNU, v_stapnu);
	DDV_MaxChars(pDX, v_stapnu, 4);
	DDX_Control(pDX, IDC_EINZEL, c_einzelbutt);
	DDX_Check(pDX, IDC_EINZEL, v_einzelbutt);
	DDX_Control(pDX, IDC_EINZKUFI, m_einzkufi);
	DDX_Check(pDX, IDC_EINZKUFI, v_einzkufi);
	DDX_Control(pDX, IDC_BISDATUM, m_bisdatum);
	DDX_Text(pDX, IDC_BISDATUM, v_bisdatum);
	DDX_Control(pDX, IDC_KUNDE, m_kunde);
	DDX_Text(pDX, IDC_KUNDE, v_kunde);
	DDV_MinMaxLong(pDX, v_kunde, 1, 99999999);
	DDX_Control(pDX, IDC_FILIALE, m_filiale);
	DDX_Text(pDX, IDC_FILIALE, v_filiale);
	DDV_MinMaxShort(pDX, v_filiale, 1, 9999);
	DDX_Control(pDX, IDC_AUFL, m_aufl);
	DDX_Text(pDX, IDC_AUFL, v_aufl);
	DDV_MinMaxInt(pDX, v_aufl, 1, 5);
	DDX_Control(pDX, IDC_EINZELBEL, m_ebd);
	DDX_Text(pDX, IDC_EINZELBEL, v_ebd);
}

BEGIN_MESSAGE_MAP(CStapDrDlg, CFormView)
	ON_BN_CLICKED(IDOK, &CStapDrDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CHECKKUN, &CStapDrDlg::OnBnClickedCheckkun)
	ON_BN_CLICKED(IDC_CHECKFIL, &CStapDrDlg::OnBnClickedCheckfil)
	ON_EN_KILLFOCUS(IDC_MANDANT, &CStapDrDlg::OnEnKillfocusMandant)
	ON_EN_KILLFOCUS(IDC_DATUM, &CStapDrDlg::OnEnKillfocusDatum)
	ON_EN_KILLFOCUS(IDC_STAPNU, &CStapDrDlg::OnEnKillfocusStapnu)
	ON_BN_CLICKED(IDCANCEL, &CStapDrDlg::OnBnClickedCancel)
	ON_EN_KILLFOCUS(IDC_EINZELBEL, &CStapDrDlg::OnEnKillfocusEinzelbel)
	ON_BN_CLICKED(IDC_EBD, &CStapDrDlg::OnBnClickedEbd)
	ON_BN_CLICKED(IDC_EINZEL, &CStapDrDlg::OnBnClickedEinzel)
	ON_BN_CLICKED(IDC_EINZKUFI, &CStapDrDlg::OnBnClickedEinzkufi)
	ON_EN_KILLFOCUS(IDC_BISDATUM, &CStapDrDlg::OnEnKillfocusBisdatum)
	ON_EN_KILLFOCUS(IDC_AUFL, &CStapDrDlg::OnEnKillfocusAufl)
END_MESSAGE_MAP()


// StapDrDlg-Diagnose

#ifdef _DEBUG
void CStapDrDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CStapDrDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// ALLES NUR WEGEN BWS_DEFA -ANFANG 

char *wort[25] ; // Bereich fuer Worte eines Strings
static char buffer[1000] ;
static char DefWert[256] ;

void cr_weg (char *string)

{
 for (; *string; string += 1)
 {
  if (*string == (char) 13)
    break;
  if (*string == (char) 10)
    break;
  if (*string == '\0')
    return ;
 }
 *string = 0;
 return;
}

int next_char_ci (char *string, char tzeichen, int i)
// Naechstes Zeichen != Trennzeichen suchen.

{
       for (;string [i]; i ++)
       {
               if (string[i] != tzeichen)
               {
                                   return (i);
               }
       }
       return (i);
}


short split (char *string)
{
 short wz;                  // Wortzaehler  
 int i, j, len;
 static char zeichen = ' ';
//  if (buffer == (char *) 0) buffer = (char *) malloc (0x1000);
	wz = 0;
	j = 0;
	len = (int)strlen (string);
	wz = 1;
	i = next_char_ci (string, zeichen, 0);
	if (i >= len) return (0);
	wort [wz] = buffer;
	wz ++;
	for (; i < len; i ++, j ++)
	{
        if (string [i] == zeichen)
        {
                    i = next_char_ci (string, zeichen, i);
                    if (i >= len) break;
                    buffer [j] = (char) 0;
                    j ++;
                    wort [wz] = &buffer [j];
                    wz ++;
        }
        buffer [j] = string [i];
	}
	buffer [j] = (char) 0;
	return (wz - 1);
}

int strupcmp (char *str1, char *str2, int len)
{
 short i;
 unsigned char upstr1;
 unsigned char upstr2;


	for (i = 0; i < len; i ++, str1 ++, str2 ++)
	{
		if (*str1 == 0)
			return (-1);
		if (*str2 == 0)
			return (1);

		upstr1 = (unsigned char) toupper((int) *str1);
		switch (upstr1)
		{
		case (unsigned char) '�' :
            upstr1 = '�';
			break;
		case (unsigned char) '�' :
            upstr1 = '�';
			break;
		case (unsigned char) '�' :
            upstr1 = '�';
			break;
		}

		upstr2 = (unsigned char) toupper((int) *str2);
		switch (upstr2)
		{
		case (unsigned char) '�' :
            upstr2 = '�';
			break;
		case (unsigned char) '�' :
            upstr2 = '�';
			break;
		case (unsigned char) '�' :
            upstr2 = '�';
			break;
		}
		if (upstr1 < upstr2)
		{
			return(-1);
		}
		else if (upstr1 > upstr2)
		{
			return (1);
		}
	}
	return (0);
}


char *bws_default (char *env)
/**
Wert aus Bws-default holen.
**/
{         
	char *etc;
    int anz;
    char buffer [512];
    FILE *fp;

    etc = getenv ("BWSETC");
    if (etc == (char *) 0)
    {
		etc = "C:\\USER\\FIT\\ETC";
    }
    sprintf (buffer, "%s\\bws_defa", etc);
	fp = fopen (buffer, "r");
    if (fp == NULL) return NULL;

     while (fgets (buffer, 511, fp))
     {
		cr_weg (buffer);
        anz = split (buffer);
        if (anz < 2) continue;
        if (strupcmp (wort[1], env, (int) strlen (env)) == 0)
        {
			strcpy (DefWert, wort [2]);
            fclose (fp);
			return (char *) DefWert;
        }
	}
    fclose (fp);
    return NULL;
}
// ALLES NUR WEGEN BWS_DEFA - ENDE 

double ROUND ( double inwert , int prezi )
{

	if ( inwert == 0.0 ) return 0.0 ;
	double outwert ;
	double dmult ;
	double dfaktor ;
	double ddiv ;
	double ddummy ;

    dfaktor = 1 ;
	dmult = 1 ;

	for ( int i = 0 ; i < prezi ; i ++ ) dmult *= 10.0 ;

    ddiv = dmult * 2.0 ;
	dfaktor /= ddiv ;

	if ( inwert > 0.0 )
		ddummy = inwert + dfaktor ;
	else
		ddummy = inwert - dfaktor ;


	inwert = ( long )(ddummy * dmult ) ;
	outwert = inwert / dmult ;
	return  outwert ;
}

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	int k = 0 ;	// 261007 : Zusatzchecks
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		// 261007 : Zusatzchecks wegen vista, nur noch bedingtes return
		if ( i < 6 ) k = -1 ;
		if ( k == 0 )
		{
			// die ersten beiden Zeichen MUESSEN ZAHLEN sein
			if ( bufi[0] > '9' || bufi[0] < '0' ||bufi[1] > '9' || bufi[1] < '0' )
				k = - 1 ;
		}
		if ( k == 0 )
		{
			if ( bufi[2] > '9' || bufi[2] < '0' )
				k = 2 ;		// Typ 8 oder typ 10 moeglich

		}
		if ( k == 0 )
		{
			// nur noch Typ 6 erlaubt 
			if (    bufi[3] > '9' || bufi[3] < '0'
				 || bufi[4] > '9' || bufi[4] < '0'
				 || bufi[5] > '9' || bufi[5] < '0' )
			{
				k = - 1 ;
			}
			else
			{	// Gueltiger Typ 6  ......
				k = 1 ;
				i = 6 ;
				bufi[6] = '\0' ;
			}
		}
		if ( k == 2 )
		{
			if ( i < 8 )
			{	
				k = -1 ;
			}
			else
			{	// 4.,5.,7.,8. Zeichen MUESSEN ZAHLEN sein, 6.Zeichen MUSS Nicht-Zahl sein
				if (   bufi[3] > '9' || bufi[3] < '0' 
					 ||bufi[4] > '9' || bufi[4] < '0'
				     ||bufi[6] > '9' || bufi[6] < '0'
				     ||bufi[7] > '9' || bufi[7] < '0'
				     || !( bufi[5] >'9' || bufi[5]< '0')
				    )
				k = - 1 ;
			}
		}
		if ( k == 2 )
		{
			if ( bufi[8] > '9' || bufi[8] < '0' )
			{	// gueltiger Typ 8 
				k = 1 ;
				i = 8 ;
				bufi[8] = '\0' ;
			}
		
		}

		if ( k == 2 )
		{
			if ( i < 10 )
			{	
				k = -1 ;
			}
			else
			{	// 9.,10. Zeichen MUESSEN ZAHLEN sein
				if (   bufi[8] > '9' || bufi[8] < '0' 
					 ||bufi[9] > '9' || bufi[9] < '0'
			       )
				{
					k = -1 ;
				}
				else
				{
					k = 1 ;
					i = 10 ;
					bufi[10] = '\0' ;
				}
			}
		}
		if ( k < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;

}


DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                      (int) NULL,
                      (unsigned long) NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
        }
		else
        {
			      return -1;
        }				  
		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}

void CStapDrDlg::ReadMdn (void) 
{
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
//	gibbet nich		PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich 		PrevDlgCtrl();
	}
		UpdateData (FALSE) ;
}

void CStapDrDlg::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.


	dbClass.opendbase (_T("bws"));

	v_filbutt = TRUE ;		// Filiale als std.
	v_aufl = 1 ;			// 020210 : Std.-Auflage Filiale 1
							// Kunde : Std-Auflage = 2
	v_kunbutt = FALSE ;
	v_einzelbutt = TRUE ;	// Einzeljobs als std.
	v_einzkufi = FALSE ;	// Tourenstapel als std.
	v_filiale = 1 ;
	v_kunde = 1 ;

// 180509 : immer offen m_bisdatum.ShowWindow(SW_HIDE);

	m_stapnu.ShowWindow(SW_NORMAL);
	m_kunde.ShowWindow(SW_HIDE);
	m_filiale.ShowWindow(SW_HIDE);


	mdn.mdn = 1 ;
	v_mandant = "1" ;

 	ReadMdn ();

	dnachkpreis = atoi ( sys_par_class.sys_par_holen ( "nachkpreis" ))  ;
	if ( dnachkpreis > 4 || dnachkpreis < 2 ) dnachkpreis = 2 ;

// Testausgabe : MessageBox(globnutzer, globnutzer, MB_OK|MB_ICONSTOP);



// return TRUE;  // so mag es oninitdialog sein .... Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten

}


// StapDrDlg-Meldungshandler

void CStapDrDlg::OnBnClickedOk()
{
// Der Knopf wird mit "Ausdruck" beschriftet

	UpdateData (TRUE) ;
	int i = m_stapnu.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 070910
	stapnu = 0 ;
	if (i)	stapnu = atoi ( bufh );

	StapelDruck () ;

	UpdateData (FALSE) ;

//	GetParent ()->DestroyWindow ();
}

void CStapDrDlg::OnBnClickedCheckkun()
{


	if ( v_kunbutt == FALSE )	// 020210 Wechsel folgt
			v_aufl = KUNSTD  ;	// 020210

	v_kunbutt = TRUE ;
	v_filbutt = FALSE ;
	if ( v_einzkufi == TRUE )
	{
		m_bisdatum.ShowWindow(SW_NORMAL);
		m_stapnu.ShowWindow(SW_HIDE);

		m_kunde.ShowWindow(SW_NORMAL);
		m_filiale.ShowWindow(SW_HIDE);
	}
	else
	{
// 180509 : immer aktiv		m_bisdatum.ShowWindow(SW_HIDE);
		m_stapnu.ShowWindow(SW_NORMAL);

		m_kunde.ShowWindow(SW_HIDE);
		m_filiale.ShowWindow(SW_HIDE);
	}
	UpdateData(FALSE);
}

void CStapDrDlg::OnBnClickedCheckfil()
{
	if ( v_filbutt == FALSE )	// 020210 Wechsel folgt
			v_aufl = FILSTD  ;	// 020210

	v_filbutt = TRUE ;
	v_kunbutt = FALSE ;

	if ( v_einzkufi == TRUE )
	{
		m_bisdatum.ShowWindow(SW_NORMAL);
		m_stapnu.ShowWindow(SW_HIDE);

		m_kunde.ShowWindow(SW_HIDE);
		m_filiale.ShowWindow(SW_NORMAL);
	}
	else
	{
// 180509 immer aktiv :		m_bisdatum.ShowWindow(SW_HIDE);
		m_stapnu.ShowWindow(SW_NORMAL);

		m_kunde.ShowWindow(SW_HIDE);
		m_filiale.ShowWindow(SW_HIDE);
	}
	UpdateData(FALSE);
}

void CStapDrDlg::OnEnKillfocusMandant()
{

	UpdateData (TRUE) ;
	int i = m_mandant.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 070910

	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;
	mdn_class.openmdn();
	if (! mdn_class.lesemdn())
	{
		adr.adr = mdn.adr ;
		i = adr_class.openadr () ;
		i = adr_class.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich			PrevDlgCtrl();
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		MessageBox("Ung�ltige Eingabe!", " ", MB_OK|MB_ICONSTOP);
// gibbet nich		PrevDlgCtrl();
	}
	UpdateData (FALSE) ;
}

void CStapDrDlg::OnEnKillfocusDatum()
{

	UpdateData(TRUE) ;
	int	i = m_datum.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;	// 070910

	if (i)
	{
		if (datumcheck(bufh))
		{
			v_datum.Format("%s",_T(bufh));
		}
	}
//	Read () ;	// Saetze einlesen 
	UpdateData (FALSE) ;
	return ;

}

void CStapDrDlg::OnEnKillfocusStapnu()
{
	UpdateData ( TRUE ) ;
	int i = m_stapnu.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 070910

	if (i)	stapnu =  atoi ( bufh );
	else stapnu = 0 ;
	UpdateData ( FALSE ) ;
}

void CStapDrDlg::OnBnClickedCancel()
{
	GetParent ()->DestroyWindow ();
}

void setdatum (  TIMESTAMP_STRUCT * ziel , CString quelle )
{
// Input zwingend "dd.mm.yyyy" oder blank

    char tag[] = {"01"};
    char mon[]  = {"01"};
    char jahr[]  = {"2000"};
   	int len = quelle.GetLength ();
	 if (len >=2)
	 {
		 memcpy (tag, quelle.Mid (0, 2), 2);
	 }
	 if (len >=5)
	 {
			 memcpy (mon, quelle.Mid (3, 2), 2);
	 }
	 if (len >=10)
	 {
		 memcpy (jahr, quelle.Mid (6, 4), 4);
	 }
	 else
	 {
		if (len >=10)
		{
			memcpy (jahr + 2 , quelle.Mid (6, 2), 2);
		}
	 }
	 ziel->year = atoi ( jahr ) ; if ( ziel->year > 2099 || ziel->year < 1900 ) ziel->year = 1999 ;
	 ziel->day = atoi ( tag ) ;   if ( ziel->day > 31 || ziel->day < 1 ) ziel->day = 1 ;
	 ziel->month = atoi ( mon ) ; if ( ziel->month > 12 || ziel->month < 1 ) ziel->month = 1 ;
}


void druckrech( short imdn, long ilsnr , int ityp )
{

char rosipath [200] ;
char kommando[299] ;
sprintf ( rosipath,"%s", getenv( "RSDIR"));
if ( ityp == 6 )	// Barverkaufskunde
	sprintf ( kommando, "%s\\bin\\rswrun beldr R %d 0 %d %s B" , rosipath, imdn, ilsnr , globnutzer ) ;
else
	sprintf ( kommando, "%s\\bin\\rswrun beldr R %d 0 %d %s R" , rosipath, imdn, ilsnr , globnutzer ) ;
	int ex_code = ProcWaitExec( (char *) kommando , SW_SHOW, -1, 0, -1, 0 );
}




void drucken (  short imdn, BOOL filbutt , char * cnumliste , int i_aufl ,int variante )
{
  char s1[200] ;

  sprintf ( s1 , "%s\\stapdru.prm", getenv ( "TMPPATH" ) ) ;
  FILE * fp2 ;

	fp2 = fopen ( s1 , "w" ) ;
	if ( fp2 == NULL )
	{
		return  ;
	}

	if ( filbutt == TRUE )
		sprintf ( bufh, "NAME T53101\n" ) ;
	else
		sprintf ( bufh, "NAME T53100\n" ) ;
	
	fputs ( bufh , fp2 );

	fputs ( "LABEL 0\n" , fp2 ) ;
	int ix = 0 ;
	char *p = bws_default ("stapdru_druck") ;
	if ( p != NULL ) ix = atoi(p) ;
	sprintf ( bufh, "DRUCK %01d\n" , ix  ) ;
	fputs ( bufh , fp2 ) ;

	if ( variante >  0 )	// 090611
	{
		sprintf ( bufh, "MITVARIANTE %01d\n", variante   ) ;
		fputs ( bufh , fp2) ;
	}


	sprintf ( bufh, "mdn %01d %01d\n", imdn , imdn   ) ;
	fputs ( bufh , fp2) ;

	sprintf ( bufh , "ls INRANGE " ) ;
	fputs ( bufh , fp2) ;
	fputs ( cnumliste, fp2) ;
	fputs ( "\n" , fp2) ;

	if ( i_aufl > 5 || i_aufl < 1 )
	{
		fputs ( "ANZAHL 1\n" , fp2 ) ;
	}
	else
	{
		sprintf ( bufh , "ANZAHL %d\n", i_aufl ) ;
		fputs ( bufh , fp2) ;
	}

	fclose ( fp2) ;

	sprintf ( bufh, "dr70001.exe -datei \"%s\" " , s1 );
	int ex_code = ProcWaitExec( (char *)bufh, SW_SHOW, -1, 0, -1, 0 );
}

void CStapDrDlg::StapelDruck ()
{

	int garnixda ;			// 280409 : nix-da-Message handeln
	char lsnr[12] ;			// Hilfs-LS-String
	char numliste[1400] ;	// Liste der LS-Nummern 
	int inpoint = 0  ;			// Schreibpos in numliste
	int kunstartl = 0 ;			// startpos neuer Kunde in numliste
    int kunstarta = 0 ;			// Startpos neuer Kunde im Array
	long altkunde = -2 ;			// Gruppierungskunde 


	double iof_po ;
	double iof_ek ;
	double iof_po_euro ;
	double iof_ek_euro ;
	double igew ;
	double ibrutto ;
	double hime ;

	int i ;

	garnixda = 0 ;	// 280409

	if ( ! stapnu && ! v_einzkufi )	// 0 == kein Stapel, irgendwie muss man ja auch Sachen absolut abh�ngen k�nnen 010708 
	{								//  beim Einzelzeil ist wiederunm die Tour unwichtig

		MessageBox (_T("Datensatz nicht vorhanden"), NULL, MB_OK | MB_ICONERROR); 
		return ;
	}



	numliste[0] = '\0' ;

	// array initialisieren 
	for ( i = 0 ; i < IMAXDIM ; i++ )
		eigmat[i] = -1 ;

	lsk.mdn = mdn.mdn ;
 	setdatum ( & lsk.lieferdat , v_datum ) ;

	int istatus ;
	if ( v_filbutt == TRUE )
	{
		lsk.kun_fil = 1 ;	// Filial-Stapel
// 280409		fil.frm = ( short ) stapnu  ;
		fil.tou = ( long ) stapnu  ;
	
		if ( v_einzkufi )	// 280409
		{
 			setdatum ( & lsk.vondate , v_datum ) ;
 			setdatum ( & lsk.bisdate , v_bisdatum ) ;
			kun.kun = (long) v_filiale ;
			istatus = lsk_class.openefbeding() ;
			if ( !istatus ) istatus = lsk_class.leseefbeding () ;
		}
		else
		{

			setdatum ( & lsk.vondate , v_datum ) ;		// 180509
 			setdatum ( & lsk.bisdate , v_bisdatum ) ;	// 180509

			istatus = lsk_class.openfbeding() ;
			if ( !istatus ) istatus = lsk_class.lesefbeding () ;
		}
	}
	else
	{
		lsk.kun_fil = 0 ;	// Kunden-Stapel

//		kun.jr_plan_ums =  stapnu  ;
// 280409		sprintf ( kun.kun_bran ,"%01d", stapnu ) ;
		kun.tou = (long) stapnu ;
		if ( v_einzkufi ) 
		{
			setdatum ( & lsk.vondate , v_datum ) ;
 			setdatum ( & lsk.bisdate , v_bisdatum ) ;
			kun.kun = v_kunde ;
			istatus = lsk_class.openekbeding() ;
			if ( !istatus ) istatus = lsk_class.leseekbeding () ;
		}
		else
		{
			setdatum ( & lsk.vondate , v_datum ) ;		// 180509
 			setdatum ( & lsk.bisdate , v_bisdatum ) ;	// 180509
			istatus = lsk_class.openkbeding() ;
			if ( !istatus ) istatus = lsk_class.lesekbeding () ;
		}
	}

	if ( istatus )
	{
		// Einzel-Sofortrechnungen sind nicht aus dem stapel erlaubt 
		if ( v_filbutt == TRUE || v_einzkufi == TRUE )
		{
		   MessageBox (_T("Datensatz nicht vorhanden"), NULL, MB_OK | MB_ICONERROR); 
		   return ;
		}
		else
			garnixda = 1 ;	// keine "normalen" Kunden vorhanden
	}

	if ( ! garnixda )	// LS- Stapel abarbeiten
	{
//	1. Runde : lesen der LS-Saetze 
		i = 0 ;
		while ( ( ! istatus ) && ( i < IMAXDIM ) )
		{

			schlussmat[i] = lsk.ls ;
			schluss2mat[i] = lsk.kun ;
			eigmat[i]      = lsk.ls_stat ;
			if ( v_filbutt == TRUE )
			{ 
				if ( v_einzkufi )
					istatus = lsk_class.leseefbeding () ;
				else
					istatus = lsk_class.lesefbeding () ;
			}
			else
			{
				if ( v_einzkufi )
					istatus = lsk_class.leseekbeding () ;
				else
					istatus = lsk_class.lesekbeding () ;
			}

			i ++ ;
		}

// 2. Runde : LS-Saetze bei Bedarf updaten 

		i = 0 ;
		while (  i < IMAXDIM  )
		{
			if ( eigmat[i] == -1 )
			{
				if ( inpoint > 0 )
					drucken ( mdn.mdn , v_filbutt , numliste , v_aufl , 0 ) ;
				inpoint = 0 ;
				kunstartl = 0 ;
				altkunde = -2 ;
				numliste[0] = '\0' ;

// Start zusaetzlich  Fehlerausgabe :
				for ( i = 0 ; i < IMAXDIM ; i ++ )
				{
					if ( eigmat[i] == -1 )	break ;	// Fehlersuche beendet

					if ( eigmat[i] == -2 )
					{
						if ( inpoint > 0 )
						{
							strcat  (numliste , "," ) ;
						}
						sprintf ( lsnr ,"%1.0d" , schlussmat[i] ) ;

						// 080910 eigentlich ist doch eine ls-nr == 0 ohnehin Quatsch,
						// ich werde das in der Selektion schon vernichten

						if ( schlussmat[1] == 0 )
							sprintf ( lsnr ,"0" ) ;	// 070910 : das war ne boese Falle : die 0 generiert blank ....

						strcat ( numliste , lsnr ) ;
						inpoint = (int) strlen ( numliste ) ;
					}
				}
				if ( inpoint > 0 )
				{
					MessageBox(numliste,_T("LS gelockt!"), MB_OK|MB_ICONSTOP);
				}

				inpoint = 0 ;
				numliste[0] = '\0' ;

// Ende zusaetzlich : Fehlerausgabe :

				break ;	// array abgearbeitet
			}

			if ( inpoint > 990 )
			{
				numliste[kunstartl] = '\0' ;	// Abschneiden
				drucken ( mdn.mdn , v_filbutt , numliste , v_aufl , 0  ) ;
				inpoint = 0 ;
				kunstartl = 0 ;
				altkunde = -2 ;
				i = kunstarta ;
				numliste[0] = '\0' ;
			}

			if ( altkunde != schluss2mat[i])
			{
				kunstartl = inpoint ;
				kunstarta = i ;
// 290408   altkunde = schluss2mat[i] ;
// 280409 : v_einzelbutt dazu abhandeln : Job je Kunde bzw. Filiale ausloesen
				if ( v_einzelbutt)
				{
					if ( altkunde != -2 )
					{
						numliste[kunstartl] = '\0' ;	// Abschneiden
						drucken ( mdn.mdn , v_filbutt , numliste , v_aufl , 0 ) ;
						inpoint = 0 ;
						kunstartl = 0 ;
//						altkunde = -2 ;
						altkunde = schluss2mat[i] ;
						i = kunstarta ;
						numliste[0] = '\0' ;
					}
					else
						altkunde = schluss2mat[i] ;
				}
				else
					altkunde = schluss2mat[i] ;
			}

// Aktualisieren und updaten ( immer, falls Status passt 
			if ( eigmat[i] == 3 || eigmat[i] == 4 )
			{
				lsk.ls = schlussmat[i] ;
				lsk.mdn = mdn.mdn ;
				lsk.fil =  0 ;
			// LS aktualisieren + updaten
				dbClass.beginwork() ;
				istatus = lsk_class.openlsk() ;
				if ( ! istatus )
				{
					istatus = lsk_class.leselsk () ;
				}
				if ( istatus )
				{
					dbClass.rollbackwork() ;
					eigmat[i] = -2 ;	// Marker : Problem
					i ++ ;
					continue ;
				}
				if ( ! istatus )
				{
					lsp.ls = schlussmat[i] ;
					lsp.mdn = mdn.mdn ;
					lsp.fil =  0 ;
					istatus = lsp_class.openlsp () ;
				}
				if ( ! istatus ) 
				{
					istatus = lsp_class.leselsp () ;
				}
				if ( !istatus ) 
				{
					iof_po = iof_ek = iof_po_euro = iof_ek_euro = igew = ibrutto = 0.0 ;

					if ( v_filbutt == TRUE )
					{
						fil.fil = (short )lsk.kun ;
						fil.mdn = mdn.mdn ;
						istatus = fil_class.openfil() ;
						if ( !istatus )
						{
							istatus = fil_class.lesefil() ;
						}
						while ( ! istatus )
						{
							if ( a_bas.me_einh == 2 )
								hime = lsp.lief_me ;
							else
								hime = lsp.lief_me * a_bas.a_gew ;

							if (( fil.sw_kz[0] == 'J' ) && ( a_bas.sw > 0.001 ))
								hime = hime * ((100 - a_bas.sw) / 100.0 ) ;

							iof_po += ROUND ( hime * lsp.ls_lad_pr , dnachkpreis ) ;
							iof_ek += ROUND ( hime * lsp.ls_vk_pr , dnachkpreis ) ;

							iof_po_euro += ROUND ( hime * lsp.ls_lad_euro , dnachkpreis ) ;
							iof_ek_euro += ROUND ( hime * lsp.ls_vk_euro , dnachkpreis ) ;

							if ( ! strncmp( lsp.lief_me_bz,"kg",2 ) 
							  || ! strncmp( lsp.lief_me_bz,"KG",2)
							  || ! strncmp( lsp.lief_me_bz, "Kg",2))
								igew += lsp.lief_me ;	// das ist wohl noch ein FES-Ablauf ?!

							ibrutto +=  ROUND ( hime , 3 ) ;
							istatus = lsp_class.leselsp () ;
						}
						lsk.brutto = ibrutto ;
						lsk.gew = igew ;
						lsk.of_po = iof_po ;
						lsk.of_ek = iof_ek ;
						lsk.of_po_euro = iof_po_euro ;
						lsk.of_ek_euro = iof_ek_euro ;
						lsk.ls_stat = 4 ;
						int save_sql_mode = dbClass.sql_mode ;
						dbClass.sql_mode = 7 ;
						istatus = lsk_class.schreibelsk() ;
						dbClass.sql_mode = save_sql_mode ;
						if ( !istatus )
						{
							dbClass.commitwork() ;
							eigmat[i] = 4 ;
						}
						else
						{
							dbClass.rollbackwork() ;
							eigmat[i] = -2 ; // Marker : problem
							i ++ ;
							continue ;
						}					
					}
					else
					{
						kun.kun = lsk.kun ;
						kun.mdn = mdn.mdn ;
						istatus = kun_class.openkun() ;
						if ( ! istatus )
							istatus = kun_class.lesekun () ;
						while ( ! istatus )
						{
							if ( a_bas.me_einh == 2 )
								hime = lsp.lief_me ;
							else
								hime = lsp.lief_me * a_bas.a_gew ;

							if (( kun.sw_rab[0] == 'J' ) && ( a_bas.sw > 0.001 ))
								hime = hime * ((100 - a_bas.sw) / 100.0 ) ;

							iof_po += ROUND ( hime * lsp.ls_vk_pr , dnachkpreis ) ;
							iof_ek += ROUND ( hime * lsp.ls_lad_pr , dnachkpreis ) ;

							iof_po_euro += ROUND ( hime * lsp.ls_vk_euro , dnachkpreis ) ;
							iof_ek_euro += ROUND ( hime * lsp.ls_lad_euro , dnachkpreis ) ;
							if ( ! strncmp( lsp.lief_me_bz,"kg",2 ) 
							  || ! strncmp( lsp.lief_me_bz,"KG",2)
							  || ! strncmp( lsp.lief_me_bz, "Kg",2))
							igew += lsp.lief_me ;	// das ist wohl noch ein FES-Ablauf ?!

							ibrutto +=  ROUND ( hime , 3 ) ;

							istatus = lsp_class.leselsp () ;
						// summieren und updaten
						}
						lsk.brutto = ibrutto ;
						lsk.gew = igew ;
						lsk.of_po = iof_po ;
						lsk.of_ek = iof_ek ;
						lsk.of_po_euro = iof_po_euro ;
						lsk.of_ek_euro = iof_ek_euro ;
// 170608 : komplettieren
						lsk.inka_nr = kun.kun ;
						if ( kun.kun_typ == 4 || kun.kun_typ == 5 || kun.kun_typ == 7 )
							if ( kun.inka_nr > 0 )
								lsk.inka_nr = kun.inka_nr ;

						if ( lsk.kopf_txt > 0 )
						{
						// dann nichts mehr tun ......
						}
						else
						{
							if ( kun.ls_kopf_txt > 0 )
								lsk.kopf_txt = kun.ls_kopf_txt ;
						}

						if ( lsk.fuss_txt > 0 )
						{
							// dann nichts mehr tun ......
						}
						else
						{
							if ( kun.ls_fuss_txt > 0 )
								lsk.fuss_txt = kun.ls_fuss_txt ;

						}
						if ( lsk.fuss_txt == 0 || lsk.kopf_txt == 0 )
						{
							sprintf ( kunbrtxtzu.kun_bran2 ,"%s", kun.kun_bran2 ) ;
							int i = kunbrtxtzu_class.lesebrantext() ;
							if ( !i)
							{
								if ( lsk.kopf_txt == 0 && kunbrtxtzu.kopf_txt > 0 )
									lsk.kopf_txt = kunbrtxtzu.kopf_txt ;

								if ( lsk.fuss_txt == 0 && kunbrtxtzu.fuss_txt > 0 )
									lsk.fuss_txt = kunbrtxtzu.fuss_txt ;
							}
						}

						lsk.ls_stat = 4 ;
						int save_sql_mode = dbClass.sql_mode ;
						dbClass.sql_mode = 7 ;
						istatus = lsk_class.schreibelsk() ;
						dbClass.sql_mode = save_sql_mode ;
						if ( !istatus )
						{
							dbClass.commitwork() ;
							eigmat[i] = 4 ;
						}
						else
						{
							dbClass.rollbackwork() ;
							eigmat[i] = -2 ; // marker : Problem
							i ++ ;
							continue ;
						}

					}	// filiale oder kunde -komplettieren

				}		// Daten vorhanden
			}			// Status hat gepasst

			if ( inpoint > 0 )
			{
				strcat  (numliste , "," ) ;
			}
			sprintf ( lsnr ,"%1.0d" , schlussmat[i] ) ;
			strcat ( numliste , lsnr ) ;
			inpoint = (int) strlen ( numliste ) ;
			i ++ ;
		}		// Schl�ssel-Matrix abarbeiten 
	
	}			// nicht garnixda 
	// 280405 : Sofos und Baris auch innerhalb des Stapels ausloesen
	if ( v_filbutt == FALSE )
	{
		lsk.mdn = mdn.mdn ;
 		setdatum ( & lsk.lieferdat , v_datum ) ;
		int istatus ;
		lsk.kun_fil = 0 ;
		kun.tou = ( long ) stapnu  ;

		istatus = lsk_class.opensbbeding() ;
		if ( !istatus ) istatus = lsk_class.lesesbbeding () ;
		if ( istatus )
		{
			if ( garnixda )
			{
				MessageBox (_T("Datensatz nicht vorhanden"), NULL, MB_OK | MB_ICONERROR);
			}
		   return ;
		}
//	1.1. Runde : lesen der LS-Saetze 

		eigmat[0] = -1 ; 
		i = 0 ;
		while ( ( ! istatus ) && ( i < ( IMAXDIM - 1) ) )
		{
			schlussmat[i] = lsk.ls ;
			schluss2mat[i] = lsk.kun ;
			eigmat[i]      = kun.kun_typ ;	// rest kann nur noch sam_rech sein .....
			istatus = lsk_class.lesesbbeding () ;
			i ++ ;
			eigmat[i] = -1 ;
		}
		i = 0 ;
		while (  i < IMAXDIM  )
		{
			if ( eigmat[i] == -1 )
			{	// endlich fertig
				return ;
			}
			// Mandant, LS-Nummer , kun_typ 
			druckrech( mdn.mdn, schlussmat[i] , eigmat[i])  ;
			i ++ ;
		}
	}	// v_filbutt == FALSE
}

/* diesen Knopf gibbet so nicht mehr .....
void CStapDrDlg::OnBnClickedAusdruck()
{
	UpdateData (TRUE) ;
	int i = m_stapnu.GetLine(0,bufh,500);
	bufh[i] = '\0' ;	// 070910

	stapnu = 0 ;
	if (i)	stapnu = atoi ( bufh );

	StapelDruck () ;
	UpdateData (FALSE) ;

}
< ----- */

void CStapDrDlg::StapelEinzelDruck (long long_ebd)
{

	int garnixda ;			// nix-da-Message handeln
	char lsnr[12] ;			// Hilfs-LS-String
	char numliste[1400] ;	// Liste der LS-Nummern 
	int inpoint = 0  ;			// Schreibpos in numliste
	int kunstartl = 0 ;			// startpos neuer Kunde in numliste
    int kunstarta = 0 ;			// Startpos neuer Kunde im Array
	long altkunde = -2 ;			// Gruppierungskunde 


	double iof_po ;
	double iof_ek ;
	double iof_po_euro ;
	double iof_ek_euro ;
	double igew ;
	double ibrutto ;
	double hime ;

	BOOL iv_filbutt ;
	short ifil ;	

	int i ;

	garnixda = 0 ;	// 280409

	numliste[0] = '\0' ;

	// array initialisieren 
	for ( i = 0 ; i < IMAXDIM ; i++ )
		eigmat[i] = -1 ;

	lsk.mdn = mdn.mdn ;
	lsk.ls = long_ebd ;
	int istatus ;
	istatus = lsk_class.openeinzellsk() ;
	if ( !istatus ) istatus = lsk_class.leseeinzellsk () ;
	if ( istatus )
	{
		MessageBox (_T("Datensatz nicht vorhanden"), NULL, MB_OK | MB_ICONERROR);
		   return ;
	}

	ifil = lsk.fil ;
	if ( lsk.kun_fil == 1 )
		iv_filbutt = TRUE ;
	else 
		iv_filbutt = FALSE ;

	if ( iv_filbutt == TRUE )
	{
//		fil.tou = ( long ) stapnu  ;
	
		memcpy ( &lsk.vondate , &lsk.lieferdat , sizeof ( TIMESTAMP_STRUCT ) ) ;
		memcpy ( &lsk.bisdate , &lsk.lieferdat , sizeof ( TIMESTAMP_STRUCT ) ) ;
 		kun.kun = lsk.kun ;
		istatus = lsk_class.openefbeding() ;
		while ( !istatus )
		{
			istatus = lsk_class.leseefbeding () ;
			if ( !istatus && lsk.ls == long_ebd )	// es koennte ja mehrere LS an diesem Tag und dieser Filiale geben
				break ;
		}

	}
	else	// iv_filbutt = 0 -> lsk.kun_fil == 0 ;	// Kunden-Stapel
	{


		memcpy ( & lsk.vondate , &lsk.lieferdat , sizeof ( TIMESTAMP_STRUCT )) ;
		memcpy ( & lsk.bisdate , &lsk.lieferdat , sizeof ( TIMESTAMP_STRUCT )) ;
 		kun.kun = lsk.kun ;
		istatus = lsk_class.openekbeding() ;
		while ( !istatus )
		{
			istatus = lsk_class.leseekbeding () ;
			if ( !istatus && lsk.ls == long_ebd )	// es koennte ja mehrere LS an diesem Tag und diesme Kunde geben
				break ;
		}
	}

	if ( istatus )
	{
		// Einzel-Sofortrechnungen sind nicht aus dem stapel erlaubt 
			garnixda = 1 ;	// keine "normalen" Kunden vorhanden
	}

	if ( ! garnixda )	// LS- Stapel abarbeiten
	{
//	1. Runde : lesen der LS-Saetze 
		i = 0 ;
		while ( ( ! istatus ) && ( i < IMAXDIM ) )
		{

			schlussmat[i] = lsk.ls ;
			schluss2mat[i] = lsk.kun ;
			eigmat[i]      = lsk.ls_stat ;

/* input war schon der einzig gueltige Satz ........
			if ( iv_filbutt == TRUE )
			{ 
					istatus = lsk_class.leseefbeding () ;
			}
			else	// Kunden-Zeugs
			{
					istatus = lsk_class.leseekbeding () ;
			}
< --------- */

			i ++ ;
			break ;	// immer break nach einzigem Satz !!!!
		}

// 2. Runde : LS-Saetze bei Bedarf updaten 

		i = 0 ;
		while (  i < IMAXDIM  )
		{
			if ( eigmat[i] == -1 )
			{
				if ( inpoint > 0 )
					drucken ( mdn.mdn , iv_filbutt , numliste , 1 , 1 ) ;
				inpoint = 0 ;
				kunstartl = 0 ;
				altkunde = -2 ;
				numliste[0] = '\0' ;

// Start zusaetzlich  Fehlerausgabe :
				for ( i = 0 ; i < IMAXDIM ; i ++ )
				{
					if ( eigmat[i] == -1 )	break ;	// Fehlersuche beendet

					if ( eigmat[i] == -2 )
					{
						if ( inpoint > 0 )
						{
							strcat  (numliste , "," ) ;
						}
						sprintf ( lsnr ,"%1.0d" , schlussmat[i] ) ;

						// 080910 eigentlich ist doch eine ls-nr == 0 ohnehin Quatsch,
						// ich werde das in der Selektion schon vernichten

						if ( schlussmat[1] == 0 )
							sprintf ( lsnr ,"0" ) ;	// 070910 : das war ne boese Falle : die 0 generiert blank ....

						strcat ( numliste , lsnr ) ;
						inpoint = (int) strlen ( numliste ) ;
					}
				}
				if ( inpoint > 0 )
				{
					MessageBox(numliste,_T("LS gelockt!"), MB_OK|MB_ICONSTOP);
				}

				inpoint = 0 ;
				numliste[0] = '\0' ;

// Ende zusaetzlich : Fehlerausgabe :

				break ;	// array abgearbeitet
			}

			if ( inpoint > 990 )
			{
				numliste[kunstartl] = '\0' ;	// Abschneiden
				drucken ( mdn.mdn , iv_filbutt , numliste , 1 , 1 ) ;
				inpoint = 0 ;
				kunstartl = 0 ;
				altkunde = -2 ;
				i = kunstarta ;
				numliste[0] = '\0' ;
			}

			if ( altkunde != schluss2mat[i])
			{
				kunstartl = inpoint ;
				kunstarta = i ;
// 290408   altkunde = schluss2mat[i] ;
// 280409 : v_einzelbutt dazu abhandeln : Job je Kunde bzw. Filiale ausloesen
				if ( altkunde != -2 )
				{
					numliste[kunstartl] = '\0' ;	// Abschneiden
					drucken ( mdn.mdn , iv_filbutt , numliste , 1 , 1) ;
					inpoint = 0 ;
					kunstartl = 0 ;
//						altkunde = -2 ;
					altkunde = schluss2mat[i] ;
					i = kunstarta ;
					numliste[0] = '\0' ;
				}
				else
					altkunde = schluss2mat[i] ;
			}

// Aktualisieren und updaten ( immer, falls Status passt 
			if ( eigmat[i] == 3 || eigmat[i] == 4 )
			{
				lsk.ls = schlussmat[i] ;
				lsk.mdn = mdn.mdn ;
				lsk.fil =  ifil ;	// passt, da ja immer nur ein Beleg abgearbeitet wird .....
			// LS aktualisieren + updaten
				dbClass.beginwork() ;
				istatus = lsk_class.openlsk() ;
				if ( ! istatus )
				{
					istatus = lsk_class.leselsk () ;
				}
				if ( istatus )
				{
					dbClass.rollbackwork() ;
					eigmat[i] = -2 ;	// Marker : Problem
					i ++ ;
					continue ;
				}
				if ( ! istatus )
				{
					lsp.ls = schlussmat[i] ;
					lsp.mdn = mdn.mdn ;
					lsp.fil =  0 ;
					istatus = lsp_class.openlsp () ;
				}
				if ( ! istatus ) 
				{
					istatus = lsp_class.leselsp () ;
				}
				if ( !istatus ) 
				{
					iof_po = iof_ek = iof_po_euro = iof_ek_euro = igew = ibrutto = 0.0 ;

					if ( iv_filbutt == TRUE )
					{
						fil.fil = (short )lsk.kun ;
						fil.mdn = mdn.mdn ;
						istatus = fil_class.openfil() ;
						if ( !istatus )
						{
							istatus = fil_class.lesefil() ;
						}
						while ( ! istatus )
						{
							if ( a_bas.me_einh == 2 )
								hime = lsp.lief_me ;
							else
								hime = lsp.lief_me * a_bas.a_gew ;

							if (( fil.sw_kz[0] == 'J' ) && ( a_bas.sw > 0.001 ))
								hime = hime * ((100 - a_bas.sw) / 100.0 ) ;

							iof_po += ROUND ( hime * lsp.ls_lad_pr , dnachkpreis ) ;
							iof_ek += ROUND ( hime * lsp.ls_vk_pr , dnachkpreis ) ;

							iof_po_euro += ROUND ( hime * lsp.ls_lad_euro , dnachkpreis ) ;
							iof_ek_euro += ROUND ( hime * lsp.ls_vk_euro , dnachkpreis ) ;

							if ( ! strncmp( lsp.lief_me_bz,"kg",2 ) 
							  || ! strncmp( lsp.lief_me_bz,"KG",2)
							  || ! strncmp( lsp.lief_me_bz, "Kg",2))
								igew += lsp.lief_me ;	// das ist wohl noch ein FES-Ablauf ?!

							ibrutto +=  ROUND ( hime , 3 ) ;
							istatus = lsp_class.leselsp () ;
						}
						lsk.brutto = ibrutto ;
						lsk.gew = igew ;
						lsk.of_po = iof_po ;
						lsk.of_ek = iof_ek ;
						lsk.of_po_euro = iof_po_euro ;
						lsk.of_ek_euro = iof_ek_euro ;
						lsk.ls_stat = 4 ;
						int save_sql_mode = dbClass.sql_mode ;
						dbClass.sql_mode = 7 ;
						istatus = lsk_class.schreibelsk() ;
						dbClass.sql_mode = save_sql_mode ;
						if ( !istatus )
						{
							dbClass.commitwork() ;
							eigmat[i] = 4 ;
						}
						else
						{
							dbClass.rollbackwork() ;
							eigmat[i] = -2 ; // Marker : problem
							i ++ ;
							continue ;
						}					
					}
					else
					{
						kun.kun = lsk.kun ;
						kun.mdn = mdn.mdn ;
						istatus = kun_class.openkun() ;
						if ( ! istatus )
							istatus = kun_class.lesekun () ;
						while ( ! istatus )
						{
							if ( a_bas.me_einh == 2 )
								hime = lsp.lief_me ;
							else
								hime = lsp.lief_me * a_bas.a_gew ;

							if (( kun.sw_rab[0] == 'J' ) && ( a_bas.sw > 0.001 ))
								hime = hime * ((100 - a_bas.sw) / 100.0 ) ;

							iof_po += ROUND ( hime * lsp.ls_vk_pr , dnachkpreis ) ;
							iof_ek += ROUND ( hime * lsp.ls_lad_pr , dnachkpreis ) ;

							iof_po_euro += ROUND ( hime * lsp.ls_vk_euro , dnachkpreis ) ;
							iof_ek_euro += ROUND ( hime * lsp.ls_lad_euro , dnachkpreis ) ;
							if ( ! strncmp( lsp.lief_me_bz,"kg",2 ) 
							  || ! strncmp( lsp.lief_me_bz,"KG",2)
							  || ! strncmp( lsp.lief_me_bz, "Kg",2))
							igew += lsp.lief_me ;	// das ist wohl noch ein FES-Ablauf ?!

							ibrutto +=  ROUND ( hime , 3 ) ;

							istatus = lsp_class.leselsp () ;
						// summieren und updaten
						}
						lsk.brutto = ibrutto ;
						lsk.gew = igew ;
						lsk.of_po = iof_po ;
						lsk.of_ek = iof_ek ;
						lsk.of_po_euro = iof_po_euro ;
						lsk.of_ek_euro = iof_ek_euro ;
// 170608 : komplettieren
						lsk.inka_nr = kun.kun ;
						if ( kun.kun_typ == 4 || kun.kun_typ == 5 || kun.kun_typ == 7 )
							if ( kun.inka_nr > 0 )
								lsk.inka_nr = kun.inka_nr ;

						if ( lsk.kopf_txt > 0 )
						{
						// dann nichts mehr tun ......
						}
						else
						{
							if ( kun.ls_kopf_txt > 0 )
								lsk.kopf_txt = kun.ls_kopf_txt ;
						}

						if ( lsk.fuss_txt > 0 )
						{
							// dann nichts mehr tun ......
						}
						else
						{
							if ( kun.ls_fuss_txt > 0 )
								lsk.fuss_txt = kun.ls_fuss_txt ;

						}
						if ( lsk.fuss_txt == 0 || lsk.kopf_txt == 0 )
						{
							sprintf ( kunbrtxtzu.kun_bran2 ,"%s", kun.kun_bran2 ) ;
							int i = kunbrtxtzu_class.lesebrantext() ;
							if ( !i)
							{
								if ( lsk.kopf_txt == 0 && kunbrtxtzu.kopf_txt > 0 )
									lsk.kopf_txt = kunbrtxtzu.kopf_txt ;

								if ( lsk.fuss_txt == 0 && kunbrtxtzu.fuss_txt > 0 )
									lsk.fuss_txt = kunbrtxtzu.fuss_txt ;
							}
						}

						lsk.ls_stat = 4 ;
						int save_sql_mode = dbClass.sql_mode ;
						dbClass.sql_mode = 7 ;
						istatus = lsk_class.schreibelsk() ;
						dbClass.sql_mode = save_sql_mode ;
						if ( !istatus )
						{
							dbClass.commitwork() ;
							eigmat[i] = 4 ;
						}
						else
						{
							dbClass.rollbackwork() ;
							eigmat[i] = -2 ; // marker : Problem
							i ++ ;
							continue ;
						}

					}	// filiale oder kunde -komplettieren

				}		// Daten vorhanden
			}			// Status hat gepasst

			if ( inpoint > 0 )
			{
				strcat  (numliste , "," ) ;
			}
			sprintf ( lsnr ,"%1.0d" , schlussmat[i] ) ;
			strcat ( numliste , lsnr ) ;
			inpoint = (int) strlen ( numliste ) ;
			i ++ ;
		}		// Schl�ssel-Matrix abarbeiten 
	
	}			// nicht garnixda 
	// 280405 : Sofos und Baris auch innerhalb des Stapels ausloesen
/* ---------> nicht realisiert ...
	if ( iv_filbutt == FALSE )
	{
		lsk.mdn = mdn.mdn ;
 		setdatum ( & lsk.lieferdat , v_datum ) ;
		int istatus ;
		lsk.kun_fil = 0 ;
		kun.tou = ( long ) stapnu  ;

		istatus = lsk_class.opensbbeding() ;
		if ( !istatus ) istatus = lsk_class.lesesbbeding () ;
		if ( istatus )
		{
			if ( garnixda )
			{
				MessageBox (_T("Datensatz nicht vorhanden"), NULL, MB_OK | MB_ICONERROR);
			}
		   return ;
		}
//	1.1. Runde : lesen der LS-Saetze 

		eigmat[0] = -1 ; 
		i = 0 ;
		while ( ( ! istatus ) && ( i < ( IMAXDIM - 1) ) )
		{
			schlussmat[i] = lsk.ls ;
			schluss2mat[i] = lsk.kun ;
			eigmat[i]      = kun.kun_typ ;	// rest kann nur noch sam_rech sein .....
			istatus = lsk_class.lesesbbeding () ;
			i ++ ;
			eigmat[i] = -1 ;
		}
		i = 0 ;
		while (  i < IMAXDIM  )
		{
			if ( eigmat[i] == -1 )
			{	// endlich fertig
				return ;
			}
			// Mandant, LS-Nummer , kun_typ 
			druckrech( mdn.mdn, schlussmat[i] , eigmat[i])  ;
			i ++ ;
		}
	}	// v_filbutt == FALSE
<---- */
}


BOOL CStapDrDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				CWnd *xWnd ;
				xWnd = GetFocus ();
			           if(xWnd == GetDlgItem (IDOK)) break ;			// ok - eigentlich nichts machen
// auf den ok-Knopf verlegt ....			           if(xWnd == GetDlgItem (IDC_AUSDRUCK)) break ;	// Ausduck halt
			           if(xWnd == GetDlgItem (IDCANCEL))
					   {
							GetParent ()->DestroyWindow ();
							return TRUE ;	// Abbruch halt ( genau wie ButtonClickedCancel) 
					   }
	
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_TAB)
			{

				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (OnKeyup ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
// ???				OnCancel () ;
				GetParent ()->DestroyWindow ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				return TRUE;
			}

	}

	return CFormView::PreTranslateMessage(pMsg);
}

BOOL CStapDrDlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, FALSE);

	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CStapDrDlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

void CStapDrDlg::OnBnClickedEinzel()
{
	if ( v_einzelbutt == FALSE )
	{
		v_einzelbutt = TRUE ;
	}
	else
		v_einzelbutt = FALSE ;


	UpdateData(FALSE);
}

void CStapDrDlg::OnBnClickedEinzkufi()
{
	if ( v_einzkufi == FALSE )
	{
		v_einzkufi = TRUE ;
		m_bisdatum.ShowWindow(SW_NORMAL); m_stapnu.ShowWindow(SW_HIDE);
		if ( v_filbutt == TRUE )
		{
			m_kunde.ShowWindow(SW_HIDE);
			m_filiale.ShowWindow(SW_NORMAL);
		}
		else
		{
			m_kunde.ShowWindow(SW_NORMAL);
			m_filiale.ShowWindow(SW_HIDE);
		}
	}
	else
	{
		v_einzkufi = FALSE ;
// 180509 : immer aktiv : m_bisdatum.ShowWindow(SW_HIDE);
		m_stapnu.ShowWindow(SW_NORMAL);

		m_kunde.ShowWindow(SW_HIDE);
		m_filiale.ShowWindow(SW_HIDE);
	}
	UpdateData(FALSE);
}

void CStapDrDlg::OnEnKillfocusBisdatum()
{

	UpdateData(TRUE) ;
	int	i = m_bisdatum.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;	// 070910

	if (i)
	{
		if (datumcheck(bufh))
		{
			v_bisdatum.Format("%s",_T(bufh));
		}
	}
	UpdateData (FALSE) ;
	return ;
}

void CStapDrDlg::OnEnKillfocusAufl()	// 020210
{
	UpdateData (TRUE) ;
	if ( v_aufl < 1 ||v_aufl > 5 )	// Misteingabe
	{
		if ( v_filbutt == TRUE )
			v_aufl = FILSTD ; 
		if ( v_kunbutt == TRUE )
			v_aufl = KUNSTD ;
	}
	UpdateData (FALSE) ;
}
void CStapDrDlg::OnEnKillfocusEinzelbel()
{
	UpdateData(TRUE) ;
	int	i = m_ebd.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;

	long long_ebd ; 
	if (i)
			long_ebd = atol ( bufh );
	else
			long_ebd = 0 ;

	UpdateData (FALSE) ;
	return ;
}

void CStapDrDlg::OnBnClickedEbd()
{

	UpdateData(TRUE) ;

	int	i = m_ebd.GetLine(0,bufh,500) ;
	bufh[i] = '\0' ;

	long long_ebd ;
	if (i)
			long_ebd = atol ( bufh );
	else
			long_ebd = 0 ;

	if ( long_ebd > 0 )
		StapelEinzelDruck ( long_ebd ) ;

	UpdateData (FALSE) ;
	return ;
}
