// stapdru.h : Hauptheaderdatei f�r die stapdru-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error "\"stdafx.h\" vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"       // Hauptsymbole


// CstapdruApp:
// Siehe stapdru.cpp f�r die Implementierung dieser Klasse
//

class CstapdruApp : public CWinApp
{
public:
	CstapdruApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	afx_msg void OnDruckWahl();
	DECLARE_MESSAGE_MAP()
};

extern CstapdruApp theApp;