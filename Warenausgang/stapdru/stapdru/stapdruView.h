// stapdruView.h : Schnittstelle der Klasse CstapdruView
//


#pragma once


class CstapdruView : public CView
{
protected: // Nur aus Serialisierung erstellen
	CstapdruView();
	DECLARE_DYNCREATE(CstapdruView)

// Attribute
public:
	CstapdruDoc* GetDocument() const;

// Vorgänge
public:

// Überschreibungen
public:
	virtual void OnDraw(CDC* pDC);  // Überschrieben, um diese Ansicht darzustellen
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementierung
public:
	virtual ~CstapdruView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // Debugversion in stapdruView.cpp
inline CstapdruDoc* CstapdruView::GetDocument() const
   { return reinterpret_cast<CstapdruDoc*>(m_pDocument); }
#endif

