// stapdruDoc.cpp : Implementierung der Klasse CstapdruDoc
//

#include "stdafx.h"
#include "stapdru.h"

#include "stapdruDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CstapdruDoc

IMPLEMENT_DYNCREATE(CstapdruDoc, CDocument)

BEGIN_MESSAGE_MAP(CstapdruDoc, CDocument)
END_MESSAGE_MAP()


// CstapdruDoc-Erstellung/Zerst�rung

CstapdruDoc::CstapdruDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CstapdruDoc::~CstapdruDoc()
{
}

BOOL CstapdruDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CstapdruDoc-Serialisierung

void CstapdruDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CstapdruDoc-Diagnose

#ifdef _DEBUG
void CstapdruDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CstapdruDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CstapdruDoc-Befehle
