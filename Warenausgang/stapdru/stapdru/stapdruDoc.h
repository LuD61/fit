// stapdruDoc.h : Schnittstelle der Klasse CstapdruDoc
//


#pragma once


class CstapdruDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CstapdruDoc();
	DECLARE_DYNCREATE(CstapdruDoc)

// Attribute
public:

// Vorg�nge
public:

// �berschreibungen
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CstapdruDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


