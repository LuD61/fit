#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include <time.h>
#include <direct.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "message.h"
#include "ls.h"
#include "kun.h"
#include "mdn.h"
#include "fil.h"
// #include "mo_numme.h"
#include "mo_kompl.h"
#include "mo_einh.h"
#include "mo_curso.h"
#include "sys_par.h"
#include "dbclass.h"
#include "mo_lsgen.h"
#include "lsp_txt.h"
#include "lsp_txt.h"
#include "lspt.h"
#include "lspt.h"
#include "auto_nr.h"
#include "mo_auto.h"
#include "mo_nr.h"
#include "70001.h"
#include "bsd_buch.h"
#include "mo_menu.h"
#include "a_bas.h"
#include "angk.h"
#include "angp.h"
#include "mo_aufl.h"
#include "kasse.h"
#include "Process.h"
#include "MessageDialog.h"
#include "leer_lsh.h"

#define MAXWAIT 60
#define RSDIR "RSDIR"
#define MAXDEBUG 100

LS_CLASS ls_class;
static LEER_LSH_CLASS LeerLsh;
static EINH_CLASS einh_class;
static SYS_PAR_CLASS sys_par_class;
static DB_CLASS DbClass;
static LSP_TCLASS lsp_tclass;
static LSPT_CLASS lspt_class;
static AUTO_CLASS AutoClass;
static ADR_CLASS adr_class;
static AutoNrClass AutoNr;
static BSD_BUCH_CLASS BsdBuch;
static ANGK_CLASS angk_class;
static ANGP_CLASS angp_class;
extern MELDUNG Mess;

extern HWND hMainWindow;
extern HANDLE  hMainInst;

	int maxarr = 1500;
	int LsArr [1500];
	int KunArr [1500];
	int AuflageArr [1500];

/**
Auftrag auf komplett setzen.
**/

struct AP
{
	short mdn;
	short fil;
	long auf;
	long posi;
	double a;
};

static int bsd_kz = 1;
static struct AP *lsptab = NULL;
static long lsptanz;

static char rosipath [256];

static BOOL klst_dr_par = FALSE;
static BOOL drk_ls_sperr = 0;
static int lief_me_par = 0;
static int LskOK;
static BOOL AbtRemoved = TRUE;
static BOOL DrkKunRemoved = TRUE;
static BOOL LuKunRemoved = TRUE;


static int drwahl         = 0;

static int LsKomplett     = 0;
static int LSDruck         = 0;
static int LSFrDruck      = 0;
static int LSFrei         = 0;
static int LSBar          = 0;
static int LSSofort       = 0;
static int LSKasse       = 0;

static int *aufbuttons [] = {&LSDruck, &LSFrDruck, &LSFrei, &LSBar, &LSSofort, &LSKasse, 
                             NULL};

static int SetDruck0 (void);
static int SetFrDruck0 (void);
static int SetFrei0 (void);
static int SetBar0 (void);
static int SetSofortrech0 (void);
static int SetKasse0 (void);

static int (*SetDefTab[]) () = {SetDruck0, SetFrDruck0, SetFrei0, SetBar0, SetSofortrech0, 
                                SetKasse0};

static int SetDruck (void);
static int SetFrDruck (void);
static int SetFrei (void);
static int SetBar (void);
static int SetSofortrech (void);
static int SetKasse (void);
static int BreakBox (void);
static int BoxBreak (void);

static int IsDrChoise;

static ITEM iLsDruck      ("",  "Lieferschein drucken",    "", 0);
static ITEM iFrDrk       ("",  "Freigeben und drucken",   "", 0);
static ITEM iFrei        ("",  "Freigeben",               "", 0);
static ITEM iBar          ("", "Barverkauf",              "", 0);
static ITEM iSofortrech  ("", "Sofortrechnung",           "", 0);
static ITEM iKasse       ("", "Kasse",                    "", 0);
static ITEM  iOK      ("", "    OK      ",  "", 0);
static ITEM  iCancel  ("", "  Abbruch   ",  "", 0);
static ITEM  iDW      ("", "Druckerwahl ",  "", 0);


static field _cbox [] = {
&iLsDruck,            25, 2, 0,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetDruck, 101,
&iFrDrk,              25, 2, 2,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetFrDruck, 102,
&iFrei,               25, 2, 4,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetFrei, 103,
&iBar,                25, 2, 6,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetBar, 104,
&iSofortrech,         25, 2, 8,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetSofortrech, 105,
&iKasse,              25, 2,10,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetKasse, 106,
&iOK,                 15, 0, 13,  6,  0, "", BUTTON, 0, BreakBox, 107,
&iCancel,             15, 0, 13, 23, 0, "",  BUTTON, 0, BoxBreak, KEY5,
&iDW,                 15, 0, 13, 40, 0, "",  BUTTON, 0, BoxBreak, KEY7};

form cbox = {9, 0, 0, _cbox, 0, SetSofortrech0, 0, 0, NULL};

static int auf_text_par;
static int lutz_mdn_par = 0;
static int wa_pos_txt;
static BOOL SplitLs = FALSE;
static BOOL with_log = TRUE;

static char *LogName = NULL;
static char *LstName = NULL;

static Text CallBeldr = "beldr S ";

static void CreateLogfile (void)
/**
Log-Datei oeffnen.
**/
{
	      char buffer [512];
		  char count [80];
		  int c;
		  char *tmp;
		  char datum [12];
		  char zeit  [12];
		  FILE *fp;
          time_t timer;
          struct tm *ltime;

 	      static char *days [] = {"Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam", NULL};

		  if (with_log == FALSE) return;

		  sysdate (datum);
		  systime (zeit);
		  tmp = getenv ("TMPPATH");
		  if (tmp == NULL) return;

          time (&timer);
          ltime = localtime (&timer);

          sprintf (buffer, "%s\\%s", tmp, days [ltime->tm_wday]);
          _mkdir (buffer);

          strcat (buffer, "\\");
		  strcat (buffer, "count");
		  c = 0;
		  fp = fopen (buffer, "r");
		  if (fp)
		  {
			  if (fgets (count, 79, fp))
			  {
				  c = atoi (count);
			  }
			  fclose (fp);
		  }
		  if (LogName) delete LogName;
          LogName = new char [512];
		  if (LogName == NULL) return;
		  sprintf (LogName, "%s\\%s\\53100.%d", tmp, days [ltime->tm_wday], c);
		  fp = fopen (LogName, "w");
		  if (fp == NULL)
		  {
			  delete LogName;
			  LogName = NULL;
              return;
		  }
		  fprintf (fp, "Log-Datei Auftragserfassung erzeugt am %s %s\n", datum,
			                                                             zeit);
		  fclose (fp);
		  c ++;
		  if (c == 20) c = 0;
		  fp = fopen (buffer, "w");
		  fprintf (fp, "%d", c);
		  fclose (fp);
		  if (LstName) delete LstName;
		  LstName = new char [512];
		  sprintf (LstName, "%s\\%s\\liste.%d", tmp, days [ltime->tm_wday], atoi (count));
}

void CopyListe (void)
/**
Liste sichern.
**/
{
	  char quelle [512];
	  char ziel [512];

	  if (with_log == FALSE) return;
	  if (LstName == NULL) return;
	  char *tmp;

	  tmp = getenv ("TMPPATH");
	  if (tmp == NULL) return;
	  sprintf (quelle, "%s\\53100.lst", tmp);
	  sprintf (ziel, "%s", LstName);
	  CopyFile (quelle, ziel, FALSE);
}


void CloseLogfile (void)
/**
Dateiname fuer Logfile abschliessen.
**/
{
		  if (with_log == FALSE) return;
	      if (LogName == NULL) return;

	      delete LogName;
		  LogName = NULL;

	      if (LstName == NULL) return;

	      delete LstName;
		  LstName = NULL;
}


static void WriteLogfile (char * format, ...)
/**
Debugausgabe.
**/
{
          va_list args;
          FILE *logfile;

		  if (with_log == FALSE) return;
		  if (LogName == NULL)
		  {
			  CreateLogfile ();
		  }

		  logfile = fopen (LogName, "a");
		  if (logfile == NULL) return;

          va_start (args, format);
          vfprintf (logfile, format, args);
          va_end (args);
		  fclose (logfile);
}


int KomplettEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

void UnsetBox (int pos)
/**
Button auf selectierte setzen. (ausser pos);
**/
{
        int i;

        for (i = 0; aufbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (cbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *aufbuttons [i] = 0;
        }
}


int SetDruck (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       LSDruck = 0;
          }
          else
          {
                       LSDruck = 1;
          }
          if (syskey == KEYCR)
          {
                       LSDruck = 1;
                       SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       LSDruck, 0l);
                       break_enter ();
          }
          UnsetBox (0);
          return 1;
}

int SetFrDruck (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       LSFrDruck = 0;
          }
          else
          {
                       LSFrDruck = 1;
          }
          if (syskey == KEYCR)
          {
                       LSFrDruck = 1;
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       LSFrDruck, 0l);
                       break_enter ();
          }
          UnsetBox (1);
          return 1;
}

int SetFrei (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[2].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       LSFrei = 0;
          }
          else
          {
                       LSFrei = 1;
          }
          if (syskey == KEYCR)
          {
                       LSFrei = 1;
                       SendMessage (cbox.mask[2].feldid, BM_SETCHECK,
                       LSFrei, 0l);
                       break_enter ();
          }
          UnsetBox (2);
          return 1;
}

int SetBar (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[3].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       LSBar = 0;
          }
          else
          {
                       LSBar = 1;
          }
          if (syskey == KEYCR)
          {
                       LSBar = 1;
                       SendMessage (cbox.mask[3].feldid, BM_SETCHECK,
                       LSBar, 0l);
                       break_enter ();
          }
          UnsetBox (3);
          return 1;
}


int SetSofortrech (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[4].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       LSSofort = 0;
          }
          else
          {
                       LSSofort = 1;
          }
          if (syskey == KEYCR)
          {
                       LSSofort = 1;
                       SendMessage (cbox.mask[4].feldid, BM_SETCHECK,
                       LSSofort, 0l);
                       break_enter ();
          }
          UnsetBox (4);
          return 1;
}

int SetKasse (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[5].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       LSKasse = 0;
          }
          else
          {
                       LSKasse = 1;
          }
          if (syskey == KEYCR)
          {
                       LSKasse = 1;
                       SendMessage (cbox.mask[5].feldid, BM_SETCHECK,
                       LSKasse, 0l);
                       break_enter ();
          }
          UnsetBox (5);
          return 1;
}

int SetDruck0 (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          LSDruck = 1;
          SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       LSDruck, 0l);
          SetFocus (cbox.mask[0].feldid);
          SetCurrentField (0);
          UnsetBox (0);
          return 1;
}

int SetFrDruck0 (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          LSFrDruck = 1;
          SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       LSFrDruck, 0l);
          SetFocus (cbox.mask[1].feldid);
          SetCurrentField (1);
          UnsetBox (1);
          return 1;
}

int SetFrei0 (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          LSFrei = 1;
          SendMessage (cbox.mask[2].feldid, BM_SETCHECK,
                       LSFrei, 0l);
          SetFocus (cbox.mask[2].feldid);
          SetCurrentField (2);
          UnsetBox (2);
          return 1;
}

int SetBar0 (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          LSBar = 1;
          SendMessage (cbox.mask[3].feldid, BM_SETCHECK,
                       LSBar, 0l);
          SetFocus (cbox.mask[3].feldid);
          SetCurrentField (3);
          UnsetBox (3);
          return 1;
}


int SetSofortrech0 (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          LSSofort = 1;
          SendMessage (cbox.mask[4].feldid, BM_SETCHECK,
                       LSSofort, 0l);
          SetFocus (cbox.mask[4].feldid);
          SetCurrentField (4);
          UnsetBox (4);
          return 1;
}

int SetKasse0 (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          LSKasse = 1;
          SendMessage (cbox.mask[5].feldid, BM_SETCHECK,
                       LSKasse, 0l);
          SetFocus (cbox.mask[5].feldid);
          SetCurrentField (5);
          UnsetBox (5);
          return 1;
}



int BreakBox (void)
/**
EnterButton abbrechen.
**/
{
          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;
          syskey = KEYCR;
          break_enter ();
          return 1;
}

int BoxBreak (void)
/**
EnterButton abbrechen.
**/
{
	      int savecurrent;
		  HWND hWndA;
		  HWND hWndB;

		  savecurrent = currentfield;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;


          switch (syskey)
		  {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
				case KEY7 :
                       hWndA = GetActiveWindow ();
					   hWndB = AktivWindow;
  					   SetCurrentField (0);
					   KompAuf::ChoisePrinter0 ();
					   AktivWindow = hWndB;
					   SetActiveWindow (hWndA);
					   EnableWindow (GetParent (AktivWindow), FALSE);
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
					   syskey = 0;
					   return 1;
		  }

          if (current_form->mask[currentfield].BuId == KEY7)
		  {
					syskey = KEY7;
                    hWndA = GetActiveWindow ();
			        hWndB = AktivWindow;
 					SetCurrentField (0);
					KompAuf::ChoisePrinter0 ();
				    AktivWindow = hWndB;
					SetActiveWindow (hWndA);
					EnableWindow (GetParent (AktivWindow), FALSE);
	                SetButtonTab (TRUE);
 					SetCurrentField (savecurrent);
					SetCurrentFocus (savecurrent);
					return 1;
		  }
		  else if (current_form->mask[currentfield].BuId == KEY5)
		  {
					syskey = KEY5;
					break_enter ();
					return 1;
		  }
		  else if (current_form->mask[currentfield].BuId == 107)
		  {
					syskey = KEY12;
					break_enter ();
					return 1;
		  }

          return 1;
}


void SelectAktCtr (void)
/**
Aktiven Button in EnterButton selectieren.
**/
{
        syskey = KEYCR;
        if (cbox.mask[currentfield].after)
        {
                       (*cbox.mask[currentfield].after) ();
        }
        syskey = KEYCR;
}

PROG_CFG *KompAuf::ProgCfg = NULL;

BOOL KompAuf::RosiSqlDebug = FALSE; 
BOOL KompAuf::cfg_TestLiefMe0 = TRUE; 

BOOL KompAuf::KasseDirect = TRUE;

BOOL KompAuf::ll_ls_par = FALSE;
BOOL KompAuf::ll_rech_par = FALSE;
BOOL KompAuf::SaveDefault = FALSE;
HWND KompAuf::mainWnd = NULL;
long KompAuf::BarKunde = 0l;
BOOL KompAuf::KompOPDefault = KompAuf::PosAccept;   //151214  Dialog steht jetzt auf "Unbearbeitete Positionen ignorieren"
int KompAuf::lllsfo = 1;


void KompAuf::SetPlugins ()
{
	if (DlgPlugin == NULL)
	{
		DlgPlugin = LoadLibrary ("ChoiceLs.dll");
	}
	if (DlgPlugin != NULL)
	{
		GetPosMode = (int  (*) (int))
					GetProcAddress ((HMODULE) DlgPlugin, "GetPosMode");
	}
}

BOOL KompAuf::GetLiefMe0Mode ()
{
	if (GetPosMode != NULL)
	{
		return ((*GetPosMode) (KompOPDefault));
	}
	return PosAccept;
}

BOOL KompAuf::TestLiefMe0 ()
{
	static int cursor = -1;

	if (cfg_TestLiefMe0 == FALSE) return FALSE;
	if (cursor == -1)
	{
		DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		DbClass.sqlin ((short *) &lsk.fil, 1, 0);
		DbClass.sqlin ((long *) &lsk.ls, 2, 0);
		cursor = DbClass.sqlcursor ("select a from lsp where mdn = ? "
			                                           "and fil = ? "
													   "and ls = ? "
													   "and lief_me = 0");
	}
	DbClass.sqlopen (cursor);
	if (DbClass.sqlfetch (cursor) == 0)
	{
		return TRUE;
	}
	return FALSE;

}

BOOL KompAuf::TestPrVk0 (BOOL TestLeergut)
{
	static int cursor = -1;
	static int leer_cursor = -1;
	static double a = 0.0;
	BOOL ret = FALSE;

	if (cursor == -1)
	{
		DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		DbClass.sqlin ((short *) &lsk.fil, 1, 0);
		DbClass.sqlin ((long *) &lsk.ls, 2, 0);
		DbClass.sqlout ((double *) &a, 3, 0);
		cursor = DbClass.sqlcursor ("select a from lsp where mdn = ? "
			                                           "and fil = ? "
													   "and ls = ? "
													   "and ls_vk_euro = 0");
		DbClass.sqlin ((double *) &a, 3, 0);
		leer_cursor = DbClass.sqlcursor ("select a from a_bas where a = ? "
										 "and (a_typ = 11 or a_typ2 = 11)");
	}
	DbClass.sqlopen (cursor);
	while (DbClass.sqlfetch (cursor) == 0)
	{
		if (!TestLeergut)
		{
			ret = TRUE;
			break;
		}
		DbClass.sqlopen (leer_cursor);
		if (DbClass.sqlfetch (leer_cursor) == 100)
		{
			ret = TRUE;
			break;
		}
	}
	return ret;

}

BOOL KompAuf::AnalysePosMode (int PosMode)
{
    int dsqlstatus = ls_class.lese_lsp (lsk.mdn, lsk.fil, lsk.ls, "order by posi");
    while (dsqlstatus == 0)
    {
		lsp.pos_stat = 3;
        if (lsp.lief_me == 0.0)
		{
			if (PosMode == PosDelete)
			{
				ls_class.delete_lsp (lsp.mdn, lsp.fil, lsp.ls, lsp.a, lsp.posi);
			}
			if (PosMode == PosAuto)
			{
				if (lsp.auf_me_vgl > 0.0)
				{
					lsp.lief_me = lsp.auf_me_vgl;
					ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls, lsp.a, lsp.posi);
				}
			}
		}

        dsqlstatus = ls_class.lese_lsp ();
    }
	return TRUE;
}


void KompAuf::SetLutzMdnPar (int par)
{
	    lutz_mdn_par = par;
}

void KompAuf::SetAbt (BOOL flag)
{
	    AbtRemoved = flag;
}

void KompAuf::SetDrkKun (BOOL flag)
{
	    DrkKunRemoved = flag;
}

void KompAuf::SetLuKun (BOOL flag)
{
	    LuKunRemoved = flag;
}

void KompAuf::SetSplitLs (int par)
/**
Parameter fuer Liefermenge setzen.
**/
{
	       SplitLs = min (1, par);
}

void KompAuf::SetLiefMePar (int par)
/**
Parameter fuer Liefermenge setzen.
**/
{
	       lief_me_par = min (1, par);
}

void KompAuf::SetKlstPar (int par)
/**
Parameter fuer Liefermenge setzen.
**/
{
	       klst_dr_par = min (1, par);
}

void KompAuf::SetLog (int par)
/**
Parameter fuer Liefermenge setzen.
**/
{
	       with_log = min (1, par);
}

void KompAuf::SetKompDefault (int def_nr)
/**
Default fuer Auswahlmaske setzen.
**/
{
	       static int startOK = FALSE;

		   if (!startOK)
           {
			   def_start = def_nr;
			   startOK = TRUE;
		   }
	       this->def_nr = min (max (0, def_nr), 5);
		   cbox.before = SetDefTab[this->def_nr];
}

void KompAuf::SetDrkLsSperr (BOOL sp)
{
	       drk_ls_sperr = min (1, max (0, sp));
}

void KompAuf::BearbKomplett (void)
/**
Auswahl aus Komplett bearbeiten.
**/
{
          if (LSDruck)
          {
			  LsKomplett = 1;
			  def_nr = 0;
			  BeldrAufruf();
			  if (SaveDefault)
			  {
				  def_start = 0;
				  SetKompDefault (def_start);
			  }
          }
          else if (LSFrDruck)
          {
			  LsKomplett = 1;
			  def_nr = 1;
			  BeldrAufruf();
			  if (SaveDefault)
			  {
				  def_start = 1;
				  SetKompDefault (def_start);
			  }
          }
          else if (LSFrei)
          {
			  LsKomplett = 1;
			  def_nr = 2;
			  BeldrAufruf();
			  if (SaveDefault)
			  {
				  def_start = 2;
				  SetKompDefault (def_start);
			  }
          }
          else if (LSBar)
          {
			  LsKomplett = 1;
			  def_nr = 3;
			  BeldrAufruf();
			  if (SaveDefault)
			  {
				  def_start = 1;
				  SetKompDefault (def_start);
			  }
          }
          else if (LSSofort)
          {
			  LsKomplett = 1;
			  def_nr = 4;
			  BeldrAufruf();
			  if (SaveDefault)
			  {
				  def_start = 3;
				  SetKompDefault (def_start);
			  }
          }
          else if (LSKasse)
          {
			  LsKomplett = 1;
			  def_nr = 5;
			  BeldrAufruf();
			  if (SaveDefault)
			  {
				  def_start = 4;
				  SetKompDefault (def_start);
			  }
          }
		  EnableWindow (hMainWindow, TRUE);
		  SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
		  SetForegroundWindow (hMainWindow);
          return;
}


int KompAuf::SetKomplett (HWND hWnd)
/**
Komplett setzen.
**/
{

  	     KompParams ();

         save_fkt (5);
         save_fkt (7);
		 set_fkt (NULL, 7);
		 set_fkt (BreakBox, 12);
         set_fkt (KomplettEnd, 5);

		 mainWnd = hWnd;
         LSDruck = 0;
         LSFrDruck = 0;
         LSFrei = 0;
         LSBar = 0;
         LSSofort = 0;
         LsKomplett = 0;
 		 def_nr = def_start;
		 SetKompDefault (def_nr);

		 while (TRUE)
		 {
			EnableWindows (hWnd, FALSE);
			SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
			SetCaption ("Auswahl f�r Komplett");
			if (EnterButtonWindow (hWnd, &cbox, 13, 9, 15, 62) == -1)
			{
			            SetActiveWindow (hWnd);
                        SetAktivWindow (hWnd);
                        EnableWindows (hWnd, TRUE);
                        restore_fkt (5);
                        return 0;
			}

			SetActiveWindow (hWnd);
			SetAktivWindow (hWnd);
			EnableWindows (hWnd, TRUE);
			BearbKomplett ();
			break;
			SetKompDefault (def_nr + 1);
			if (KasseDirect && def_nr > 4)
			{
				 break;
			}
		 }
         restore_fkt (5);
         restore_fkt (7);
         return 0;
}

char KompAuf::DebugFile [80] = {"rosisqldebug"};

void KompAuf::CreateDebugFile ()
{
	     FILE *fp;
		 char NrName [512];
		 char buffer [80];
		 char *etc;
		 int fnr = 1;

		 etc = getenv ("BWSETC");
		 if (etc == NULL)
		 {
			 return;
		 }

		 sprintf (NrName, "%s\\rosisqlnr.txt", etc);
		 fp = fopen (NrName, "r");
		 if (fp != NULL)
		 {
			 fgets (buffer, 79, fp);
			 fnr = atoi (buffer);
			 fnr = (fnr == 0) ? 1 : fnr;
			 fclose (fp);
		 }
 	     sprintf (DebugFile, "c:\\temp\\rosisqldebug%d", fnr);
		 fnr ++;
		 if (fnr > MAXDEBUG) fnr = 1;
		 fp = fopen (NrName, "w");
		 fprintf (fp, "%d", fnr);
		 fclose (fp);
		 fp = fopen (DebugFile, "w");
		 if (fp != NULL) fclose (fp);
}



void KompAuf::BeldrAufruf (void)
/**
Aufruf von Beldr
**/
{
         char progname [256];
		 char Debug [256];


         if (RosiSqlDebug)
		 {
			 sprintf (Debug, "-q %s", DebugFile);
		 }
		 else
		 {
			 strcpy (Debug, "");
		 }


         strcpy (rosipath, getenv (RSDIR));

         if (strcmp (sys_ben.pers_nam, " ") <= 0)
         {
             strcpy (sys_ben.pers_nam, "fit");
         }
         commitwork ();
         if (LSDruck)
         {
                sprintf (progname, "%s\\BIN\\rswrun %s beldr L "
                                     "%hd %hd %ld %s D",
                                     rosipath, Debug,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     sys_ben.pers_nam);
                 ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
         }
         else if (LSFrDruck)
         {
                sprintf (progname, "%s\\BIN\\rswrun %s beldr L "
                                     "%hd %hd %ld %s X",
                                     rosipath, Debug,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     sys_ben.pers_nam);
                 ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
         }
         else if (LSFrei)
         {
                if (lsk.ls_stat < 4)
                {
                             disp_mess ("Der Lieferschein mu� zuerst gedruckt werden", 2);
                             return;
                }
                sprintf (progname, "%s\\BIN\\rswrun %s beldr L "
                                     "%hd %hd %ld %s F",
                                     rosipath, Debug,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     sys_ben.pers_nam);
                ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
         }
         else if (LSBar)
         {
                sprintf (progname, "%s\\BIN\\rswrun %s beldr R "
                                     "%hd %hd %ld %s B",
                                     rosipath, Debug,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     sys_ben.pers_nam);
                ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
          }
          else if (LSSofort)
          {
				 if (lsk.kun == BarKunde)
				 {
// Der Aufruf f�r Barkunde muss noch angepasst werden RoW 03.08.2007

					sprintf (progname, "%s\\BIN\\rswrun %s beldr R "
                                     "%hd %hd %ld %s R",
                                     rosipath, Debug,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     "bon");
				 }
				 else
				 {
					sprintf (progname, "%s\\BIN\\rswrun %s beldr R "
                                     "%hd %hd %ld %s R",
                                     rosipath, Debug,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     sys_ben.pers_nam);
				 }

                 ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
				 if (KasseDirect)
				 {
                            LSSofort = FALSE;
							LSKasse = TRUE;
 			                def_nr = 5;
			                BeldrAufruf();

				 }
          }
          else if (LSKasse)
		  {
			         DbClass.sqlout ((long *) &lsk.rech, 2, 0);  
			         DbClass.sqlin ((short *) &lsk.mdn, 1, 0);  
			         DbClass.sqlin ((short *) &lsk.fil, 1, 0);  
			         DbClass.sqlin ((long *) &lsk.ls, 2, 0);  
					 lsk.rech = 0l;
					 DbClass.sqlcomm ("select rech from lsk where mdn = ? "
						              "and fil = ? and ls = ?");
			         sprintf (progname, "-Rech=%ld %s", lsk.rech, sys_ben.pers_nam);
				     StartKasse (hMainInst, progname, SW_SHOWNORMAL);
		  }
}



void KompAuf::PrintLsGroup (char *where, BOOL directdruck)
{
	int cursor;
	Text Sql;
	Text FileName;
	char datum [11];
	char zeit [11];
    char Debug [256];
	char Bearbeiter [16];

	PrintLsGroupEx (where, directdruck);
	return;

    if (RosiSqlDebug)
	{
			 sprintf (Debug, "-q %s", DebugFile);
	}
	else
	{
			 strcpy (Debug, "");
	}

    strcpy (rosipath, getenv (RSDIR));
	CreateLogfile ();

    sysdate (datum);
	systime (zeit);
    WriteLogfile ("Bereichsdruck %s %s\n", datum, zeit);
	WriteLogfile ("%s\n", where);


    Mess.WaitWindow ("Die Lieferscheine werden gedruckt");

	DbClass.sqlout ((short *) &lsk.mdn, 1, 0);
	DbClass.sqlout ((short *) &lsk.fil, 1, 0);
	DbClass.sqlout ((long *)  &lsk.ls, 2, 0);
	Sql.Format ("select mdn, fil, ls from lsk %s", where);
	cursor = DbClass.sqlcursor (Sql.GetBuffer ());
	if (directdruck == TRUE)
	{
		strcpy(Bearbeiter, "nichtdrucken");   //beldr soll normal durchlaufen, nur nicht drucken 
	}
	else
	{
		strcpy(Bearbeiter, sys_ben.pers_nam);
	}

	while (DbClass.sqlfetch (cursor) == 0)
	{

				CProcess process;
				Text Command;
				Command.Format ("%s\\BIN\\rswrun %s beldr L "
                                     "%hd %hd %ld %s D",
                                     rosipath, Debug,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     Bearbeiter);
				process.SetCommand (Command);
				HANDLE Pid = process.Start (SW_SHOWMINIMIZED);
				if (Pid == NULL)
				{
					WriteLogfile ("Fehler beim Start von beldr\n");
					break;
				}
				else
				{
					DWORD ExitCode = process.WaitForEnd ();
					if (ExitCode != 0)
					{
						WriteLogfile ("Fehler %ld bei der Ausf�hrung von beldr\n", ExitCode, CallBeldr.GetBuffer ());
//						break;
					}
				}
	}

	DbClass.sqlclose (cursor);
//	FreeLs ();
//	Mess.Message ("Druck OK");
	Mess.CloseWaitWindow ();
    WriteLogfile (" Drucken beendet\n");
	CloseLogfile ();
}



void KompAuf::SetLsGruppe (void)
{
	Text FileName;
	char *tmp;
	//Sammellieferschein l�uft �ber die gruppe statt ls_stat, da nach Aufruf von Beldr der Status schon auf 4 hochgesetzt wurde. 
	int i = 0;
	DWORD CurrentPid = GetCurrentProcessId ();
	tmp = getenv ("TMPPATH");
	if (tmp == NULL)
	{
		FileName.Format ("$$beldr%ld.dat", CurrentPid ); 
	}
	else
	{
		FileName.Format ("%s\\$$beldr%ld.dat", tmp, CurrentPid); 
	}
	while (LsArr[i] > 0)
	{
    	DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
	    DbClass.sqlin ((short *) &lsk.fil, 1, 0);
		DbClass.sqlin ((long *)  &LsArr[i], 2, 0);
		DbClass.sqlcomm ("update lsk set gruppe = 0 where mdn = ? and fil = ? and ls = ? and gruppe <> ls_stat ");         // erst mal als "Kr�cke"
		i++;
	}
	if (!BeldrError (FileName, "gedruckt"))  
	{
		Mess.Message ("Druck OK");
	}
	else
	{
		Mess.Message ("");
	}
	IsGroup = FALSE;
    WriteLogfile (" Drucken beendet\n");
	CloseLogfile ();
	DeleteFile (FileName.GetBuffer ());

}

void KompAuf::InitLeerLsh (void)
{
		memcpy (&leer_lsh, &leer_lsh_null, sizeof (struct LEER_LSH));
		leer_lsh.mdn = lsk.mdn;
		leer_lsh.fil = lsk.fil;
		leer_lsh.ls = lsk.ls;
		strcpy (leer_lsh.blg_typ, "L");
		leer_lsh.a1 = (double) 0; leer_lsh.a2 = (double) 0; leer_lsh.a3 = (double) 0; leer_lsh.a4 = (double) 0; leer_lsh.a5 = (double) 0; leer_lsh.a6 = (double) 0; leer_lsh.a7 = (double) 0; leer_lsh.a8 = (double) 0; leer_lsh.a9 = (double) 0; leer_lsh.a10 = (double) 0; 
		leer_lsh.a11 = (double) 0; leer_lsh.a12 = (double) 0; leer_lsh.a13 = (double) 0; leer_lsh.a14 = (double) 0; leer_lsh.a15 = (double) 0; leer_lsh.a16 = (double) 0; leer_lsh.a17 = (double) 0; leer_lsh.a18 = (double) 0; leer_lsh.a19 = (double) 0; leer_lsh.a20 = (double) 0; 

		strcpy(leer_lsh.a_bez1,""); strcpy(leer_lsh.a_bez2,""); strcpy(leer_lsh.a_bez3,""); strcpy(leer_lsh.a_bez4,""); strcpy(leer_lsh.a_bez5,""); strcpy(leer_lsh.a_bez6,""); strcpy(leer_lsh.a_bez7,""); strcpy(leer_lsh.a_bez8,""); strcpy(leer_lsh.a_bez9,""); strcpy(leer_lsh.a_bez10,"");  
		strcpy(leer_lsh.a_bez11,""); strcpy(leer_lsh.a_bez12,""); strcpy(leer_lsh.a_bez13,""); strcpy(leer_lsh.a_bez14,""); strcpy(leer_lsh.a_bez15,""); strcpy(leer_lsh.a_bez16,""); strcpy(leer_lsh.a_bez17,""); strcpy(leer_lsh.a_bez18,""); strcpy(leer_lsh.a_bez19,""); strcpy(leer_lsh.a_bez20,"");  

		leer_lsh.stk1 = (double) 0; leer_lsh.stk2 = (double) 0; leer_lsh.stk3 = (double) 0; leer_lsh.stk4 = (double) 0; leer_lsh.stk5 = (double) 0; leer_lsh.stk6 = (double) 0; leer_lsh.stk7 = (double) 0; leer_lsh.stk8 = (double) 0; leer_lsh.stk9 = (double) 0; leer_lsh.stk10 = (double) 0; 
		leer_lsh.stk11 = (double) 0; leer_lsh.stk12 = (double) 0; leer_lsh.stk13 = (double) 0; leer_lsh.stk14 = (double) 0; leer_lsh.stk15 = (double) 0; leer_lsh.stk16 = (double) 0; leer_lsh.stk17 = (double) 0; leer_lsh.stk18 = (double) 0; leer_lsh.stk19 = (double) 0; leer_lsh.stk20 = (double) 0; 

		leer_lsh.stk_zu1 = (double) 0; leer_lsh.stk_zu2 = (double) 0; leer_lsh.stk_zu3 = (double) 0; leer_lsh.stk_zu4 = (double) 0; leer_lsh.stk_zu5 = (double) 0; leer_lsh.stk_zu6 = (double) 0; leer_lsh.stk_zu7 = (double) 0; leer_lsh.stk_zu8 = (double) 0; leer_lsh.stk_zu9 = (double) 0; leer_lsh.stk_zu10 = (double) 0; 
		leer_lsh.stk_zu11 = (double) 0; leer_lsh.stk_zu12 = (double) 0; leer_lsh.stk_zu13 = (double) 0; leer_lsh.stk_zu14 = (double) 0; leer_lsh.stk_zu15 = (double) 0; leer_lsh.stk_zu16 = (double) 0; leer_lsh.stk_zu17 = (double) 0; leer_lsh.stk_zu18 = (double) 0; leer_lsh.stk_zu19 = (double) 0; leer_lsh.stk_zu20 = (double) 0; 

		leer_lsh.stk_ab1 = (double) 0; leer_lsh.stk_ab2 = (double) 0; leer_lsh.stk_ab3 = (double) 0; leer_lsh.stk_ab4 = (double) 0; leer_lsh.stk_ab5 = (double) 0; leer_lsh.stk_ab6 = (double) 0; leer_lsh.stk_ab7 = (double) 0; leer_lsh.stk_ab8 = (double) 0; leer_lsh.stk_ab9 = (double) 0; leer_lsh.stk_ab10 = (double) 0; 
		leer_lsh.stk_ab11 = (double) 0; leer_lsh.stk_ab12 = (double) 0; leer_lsh.stk_ab13 = (double) 0; leer_lsh.stk_ab14 = (double) 0; leer_lsh.stk_ab15 = (double) 0; leer_lsh.stk_ab16 = (double) 0; leer_lsh.stk_ab17 = (double) 0; leer_lsh.stk_ab18 = (double) 0; leer_lsh.stk_ab19 = (double) 0; leer_lsh.stk_ab20 = (double) 0; 

		leer_lsh.pr_vk1 = (double) 0; leer_lsh.pr_vk2 = (double) 0; leer_lsh.pr_vk3 = (double) 0; leer_lsh.pr_vk4 = (double) 0; leer_lsh.pr_vk5 = (double) 0; leer_lsh.pr_vk6 = (double) 0; leer_lsh.pr_vk7 = (double) 0; leer_lsh.pr_vk8 = (double) 0; leer_lsh.pr_vk9 = (double) 0; leer_lsh.pr_vk10 = (double) 0; 
		leer_lsh.pr_vk11 = (double) 0; leer_lsh.pr_vk12 = (double) 0; leer_lsh.pr_vk13 = (double) 0; leer_lsh.pr_vk14 = (double) 0; leer_lsh.pr_vk15 = (double) 0; leer_lsh.pr_vk16 = (double) 0; leer_lsh.pr_vk17 = (double) 0; leer_lsh.pr_vk18 = (double) 0; leer_lsh.pr_vk19 = (double) 0; leer_lsh.pr_vk20 = (double) 0; 

		leer_lsh.a_gew1 = (double) 0; leer_lsh.a_gew2 = (double) 0; leer_lsh.a_gew3 = (double) 0; leer_lsh.a_gew4 = (double) 0; leer_lsh.a_gew5 = (double) 0; leer_lsh.a_gew6 = (double) 0; leer_lsh.a_gew7 = (double) 0; leer_lsh.a_gew8 = (double) 0; leer_lsh.a_gew9 = (double) 0; leer_lsh.a_gew10 = (double) 0; 
		leer_lsh.a_gew11 = (double) 0; leer_lsh.a_gew12 = (double) 0; leer_lsh.a_gew13 = (double) 0; leer_lsh.a_gew14 = (double) 0; leer_lsh.a_gew15 = (double) 0; leer_lsh.a_gew16 = (double) 0; leer_lsh.a_gew17 = (double) 0; leer_lsh.a_gew18 = (double) 0; leer_lsh.a_gew19 = (double) 0; leer_lsh.a_gew20 = (double) 0; 

}



void KompAuf::PrintLsGroupEx (char *where, BOOL directdruck)
{
	int cursor, cursorl, cursor_lsdr, i;
        extern  short sql_mode;
        short sqlm;

	char *tmp;
	Text Sql;
	Text FileName;
	char datum [11];
	char zeit [11];
    char Debug [256];
	double me_minus, me_plus, pr_vk;
	LsArr[0] = 0;
	KunArr[0] = 0;
	AuflageArr[0] = 0;



    if (RosiSqlDebug)
	{
			 sprintf (Debug, "-q %s", DebugFile);
	}
	else
	{
			 strcpy (Debug, "");
	}


    strcpy (rosipath, getenv (RSDIR));

	CreateLogfile ();

    sysdate (datum);
	systime (zeit);
    WriteLogfile ("Bereichs�bergabe %s %s\n", datum, zeit);
	WriteLogfile ("%s\n", where);

	DWORD CurrentPid = GetCurrentProcessId ();
	tmp = getenv ("TMPPATH");
	if (tmp == NULL)
	{
		FileName.Format ("$$beldr%ld.dat", CurrentPid ); 
	}
	else
	{
		FileName.Format ("%s\\$$beldr%ld.dat", tmp, CurrentPid); 
	}

	FILE *fp = fopen (FileName.GetBuffer (), "w");
	if (fp == NULL) 
	{
	    WriteLogfile ("%s kann nicht ge�ffnet werden\n", FileName.GetBuffer ());
		return;
	}


    Mess.WaitWindow ("Die Lieferscheine werden gedruckt");

	Text PersName = sys_ben.pers_nam;
	PersName.Trim ();
	if (PersName == "")
	{
		PersName = "Fit";
	}
	if (directdruck == TRUE)
	{
		beginwork();
		PersName = "nichtdrucken";

    	DbClass.sqlout ((short *) &lsk.mdn, 1, 0);
	    DbClass.sqlout ((short *) &lsk.fil, 1, 0);
		DbClass.sqlout ((long *)  &lsk.kun, 2, 0);
		DbClass.sqlout ((long *)  &lsk.ls, 2, 0);
		DbClass.sqlout ((short *)  &kun.auflage1, 1, 0);
		/*
		Sql.Format ("select distinct lsk.mdn, lsk.fil, lsk.kun, lsk.ls "
				"from lsk,lsp %s and lsp.mdn = lsk.mdn and "
				"lsp.ls = lsk.ls group by lsk.mdn, lsk.fil, lsk.kun, lsk.ls order by lsk.mdn, lsk.fil, lsk.kun, lsk.ls", where);
				*/

		Sql.Format ("select distinct lsk.mdn, lsk.fil, lsk.kun, lsk.ls, kun.auflage1 "
				"from lsk,lsp, kun %s and lsp.mdn = lsk.mdn and "
				"lsp.ls = lsk.ls and lsk.kun = kun.kun and lsk.mdn = kun.mdn and lsk.fil = kun.fil group by lsk.mdn, lsk.fil, lsk.kun, lsk.ls, kun.auflage1 order by lsk.mdn, lsk.fil, lsk.kun, lsk.ls", where);

	}
    else if (StapelTour)
	{   
		/* 26.03.12 GK */
		DbClass.sqlout ((short *) &lsk.mdn, 1, 0);
		DbClass.sqlout ((short *) &lsk.fil, 1, 0);
		DbClass.sqlout ((long *)  &kun.tou, 2, 0);
		DbClass.sqlout ((long *)  &lsk.ls, 2, 0);
 		Sql.Format ("select distinct lsk.mdn,lsk.fil,kun.tou,lsk.ls from lsk, kun %s and kun.kun = lsk.kun "
			    "group by lsk.mdn,lsk.fil,kun.tou,lsk.ls order by lsk.mdn,lsk.fil,kun.tou,lsk.ls",
				where);
	}
	else
	{
    	DbClass.sqlout ((short *) &lsk.mdn, 1, 0);
	    DbClass.sqlout ((short *) &lsk.fil, 1, 0);
		DbClass.sqlout ((long *)  &lsk.ls, 2, 0);
		Sql.Format ("select distinct lsk.mdn, lsk.fil, lsk.ls "
				"from lsk,lsp %s and lsp.mdn = lsk.mdn and "
				"lsp.ls = lsk.ls group by lsk.mdn, lsk.fil, lsk.ls order by lsk.mdn, lsk.fil, lsk.ls", where);
	}
	cursor = DbClass.sqlcursor (Sql.GetBuffer ());
//	for (i=0;i<maxarr;i++)	LsArr[i] = 0;
//	for (i=0;i<maxarr;i++)	KunArr[i] = 0;
	i = 0;
	while (DbClass.sqlfetch (cursor) == 0 )// LAC-202 && directdruck == TRUE)
	{
		LsArr[i] = lsk.ls;
		KunArr[i] = lsk.kun;
		AuflageArr[i] = kun.auflage1;
    	DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
	    DbClass.sqlin ((short *) &lsk.fil, 1, 0);
		DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
        if (directdruck == TRUE)		DbClass.sqlcomm ("update lsk set gruppe = ls_stat where mdn = ? and fil = ? and ls = ?");         // erst mal als "Kr�cke"
		i++;
		fprintf (fp, "L %hd %hd %ld %s D\n", lsk.mdn, lsk.fil, lsk.ls, PersName.GetBuffer ());
		WriteLogfile ("L %hd %hd %ld %s D\n", lsk.mdn, lsk.fil, lsk.ls, PersName.GetBuffer ());
		Sql.Format ("Lieferschein %ld", lsk.ls);
        Mess.Message (Sql.GetBuffer ());
	}
	LsArr[i] = 0;
	KunArr[i] = 0;
	AuflageArr[i] = 0;
	if (directdruck == TRUE) commitwork ();
	fclose (fp);
	DbClass.sqlclose (cursor);
	CProcess process;
	Text Command;
	Command.Format ("%s\\BIN\\rswrun %s beldr S %s", rosipath, Debug, FileName.GetBuffer ());
    process.SetCommand (Command);
	HANDLE Pid = process.Start (SW_SHOWNORMAL);
	if (Pid == NULL)
	{
	    WriteLogfile ("Fehler beim Start von %s\n", CallBeldr.GetBuffer ());
	}
	else
	{
		DWORD ExitCode = process.WaitForEnd ();
		if (ExitCode != 0)
		{
			WriteLogfile ("Fehler %ld bei der Ausf�hrung von %s\n", ExitCode, CallBeldr.GetBuffer ());
		}
	}
//	FreeLs ();
	EnableWindow (hMainWindow, TRUE);
    SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
    SetForegroundWindow (hMainWindow);

/* Hier mu� noc die R�ckgabedatei gelesen werden  */

	Mess.CloseWaitWindow ();

	// Jetzt leer_lsdr durchforsten, um Leergutbestand zu holen, welches in leer_lsh geschrieben wird f�r den Report
	if (directdruck == TRUE)
	{
		beginwork ();
		_a_bas.a = 0.0;
		long lstk = 0;
		double stk = 0.0;

		lsk.ls = 0; lsk.kun = 0;
		if (DbClass.sqlcomm ("delete from leer_lsh")  != 0) 
		{
			disp_mess ("Kein Zugriff auf feer_lsh Abbruch", 2);
			return;
		}
		InitLeerLsh ();
		for (i=0;i<maxarr;i++)
		{
			if (lsk.kun > 0 && lsk.kun != KunArr[i])
			{
			    WriteLogfile ("Leer_lsh.a1 %13.0lf leer_lsh.stk1 %4.2lf leer_lsh.a_bez1 %s \n", leer_lsh.a1, leer_lsh.stk1, leer_lsh.a_bez1);
			    WriteLogfile ("Leer_lsh.a2 %13.0lf leer_lsh.stk2 %4.2lf leer_lsh.a_bez2 %s \n", leer_lsh.a2, leer_lsh.stk2, leer_lsh.a_bez2);
			    WriteLogfile ("Leer_lsh.a3 %13.0lf leer_lsh.stk3 %4.2lf leer_lsh.a_bez3 %s \n", leer_lsh.a3, leer_lsh.stk3, leer_lsh.a_bez3);
				sqlm = sql_mode;
	             sql_mode = 0;
				 leer_lsh.a_gew20 = 1.0;
				LeerLsh.dbupdate();
				kun.auflage1 = 0;
				if (i > 0) kun.auflage1 = AuflageArr[i-1];
				leer_lsh.a_gew20 = (double) kun.auflage1;
				while (leer_lsh.a_gew20 > 1.0)
				{
					LeerLsh.dbupdate();
 				    leer_lsh.a_gew20 -= 1.0;
				}
					
	             sql_mode = sqlm;
				InitLeerLsh ();
			}
			if (LsArr[i] == 0) 
			{
				break;
			}
			lsk.kun = KunArr[i];
			lsk.ls = LsArr[i];
			leer_lsh.mdn = lsk.mdn;
			leer_lsh.fil = lsk.fil;
			leer_lsh.ls = lsk.kun;
			strcpy(leer_lsh.blg_typ ,"L");
			DbClass.sqlin ((long *) &lsk.ls, 2, 0);
			DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
			DbClass.sqlout ((double *) &_a_bas.a, 3, 0);
			DbClass.sqlout ((char *) _a_bas.a_bz1, 0, 24);
			DbClass.sqlout ((long *) &lstk, 2, 0);
			cursor_lsdr = DbClass.sqlcursor ("select leer_lsdr.a, a_bas.a_bz1, leer_lsdr.stk from leer_lsdr,a_bas where (a_bas.a_typ = 11 or a_bas.a in (7001,7003)) and leer_lsdr.a = a_bas.a and leer_lsdr.ls = ? and leer_lsdr.blg_typ = \"L\" and leer_lsdr.mdn = ?");
			while (DbClass.sqlfetch (cursor_lsdr) == 0)
			{
				stk = (double) lstk; 
				if (leer_lsh.a1 == _a_bas.a) continue;
				if (leer_lsh.a2 == _a_bas.a) continue;
				if (leer_lsh.a3 == _a_bas.a) continue;
				if (leer_lsh.a4 == _a_bas.a) continue;
				if (leer_lsh.a5 == _a_bas.a) continue;
				if (leer_lsh.a6 == _a_bas.a) continue;
				if (leer_lsh.a7 == _a_bas.a) continue;
				if (leer_lsh.a8 == _a_bas.a) continue;
				if (leer_lsh.a9 == _a_bas.a) continue;
				if (leer_lsh.a10 == _a_bas.a) continue;
				if (leer_lsh.a11 == _a_bas.a) continue;
				if (leer_lsh.a12 == _a_bas.a) continue;
				if (leer_lsh.a13 == _a_bas.a) continue;
				if (leer_lsh.a14 == _a_bas.a) continue;
				if (leer_lsh.a15 == _a_bas.a) continue;
				if (leer_lsh.a16 == _a_bas.a) continue;
				if (leer_lsh.a17 == _a_bas.a) continue;
				if (leer_lsh.a18 == _a_bas.a) continue;
				if (leer_lsh.a19 == _a_bas.a) continue;
				if (leer_lsh.a20 == _a_bas.a) continue;
				if (leer_lsh.a1 == 0.0) { leer_lsh.a1 = _a_bas.a; strncpy (leer_lsh.a_bez1, _a_bas.a_bz1, sizeof (leer_lsh.a_bez1) - 1); leer_lsh.a_bez1[sizeof (leer_lsh.a_bez1)] = 0; leer_lsh.stk1 = stk; continue; } 
				if (leer_lsh.a2 == 0.0) { leer_lsh.a2 = _a_bas.a; strncpy (leer_lsh.a_bez2, _a_bas.a_bz1, sizeof (leer_lsh.a_bez2) - 1); leer_lsh.a_bez2[sizeof (leer_lsh.a_bez2)] = 0; leer_lsh.stk2 = stk; continue; } 
				if (leer_lsh.a3 == 0.0) { leer_lsh.a3 = _a_bas.a; strncpy (leer_lsh.a_bez3, _a_bas.a_bz1, sizeof (leer_lsh.a_bez3) - 1); leer_lsh.a_bez3[sizeof (leer_lsh.a_bez3)] = 0; leer_lsh.stk3 = stk; continue; } 
				if (leer_lsh.a4 == 0.0) { leer_lsh.a4 = _a_bas.a; strncpy (leer_lsh.a_bez4, _a_bas.a_bz1, sizeof (leer_lsh.a_bez4) - 1); leer_lsh.a_bez4[sizeof (leer_lsh.a_bez4)] = 0; leer_lsh.stk4 = stk; continue; } 
				if (leer_lsh.a5 == 0.0) { leer_lsh.a5 = _a_bas.a; strncpy (leer_lsh.a_bez5, _a_bas.a_bz1, sizeof (leer_lsh.a_bez5) - 1); leer_lsh.a_bez5[sizeof (leer_lsh.a_bez5)] = 0; leer_lsh.stk5 = stk; continue; } 
				if (leer_lsh.a6 == 0.0) { leer_lsh.a6 = _a_bas.a; strncpy (leer_lsh.a_bez6, _a_bas.a_bz1, sizeof (leer_lsh.a_bez6) - 1); leer_lsh.a_bez6[sizeof (leer_lsh.a_bez6)] = 0; leer_lsh.stk6 = stk; continue; } 
				if (leer_lsh.a7 == 0.0) { leer_lsh.a7 = _a_bas.a; strncpy (leer_lsh.a_bez7, _a_bas.a_bz1, sizeof (leer_lsh.a_bez7) - 1); leer_lsh.a_bez7[sizeof (leer_lsh.a_bez7)] = 0; leer_lsh.stk7 = stk; continue; } 
				if (leer_lsh.a8 == 0.0) { leer_lsh.a8 = _a_bas.a; strncpy (leer_lsh.a_bez8, _a_bas.a_bz1, sizeof (leer_lsh.a_bez8) - 1); leer_lsh.a_bez8[sizeof (leer_lsh.a_bez8)] = 0; leer_lsh.stk8 = stk; continue; } 
				if (leer_lsh.a9 == 0.0) { leer_lsh.a9 = _a_bas.a; strncpy (leer_lsh.a_bez9, _a_bas.a_bz1, sizeof (leer_lsh.a_bez9) - 1); leer_lsh.a_bez9[sizeof (leer_lsh.a_bez9)] = 0; leer_lsh.stk9 = stk; continue; } 
				if (leer_lsh.a10 == 0.0) { leer_lsh.a10 = _a_bas.a; strncpy (leer_lsh.a_bez10, _a_bas.a_bz1, sizeof (leer_lsh.a_bez10) - 1); leer_lsh.a_bez10[sizeof (leer_lsh.a_bez10)] = 0; leer_lsh.stk10 = stk; continue; } 
				if (leer_lsh.a11 == 0.0) { leer_lsh.a11 = _a_bas.a; strncpy (leer_lsh.a_bez11, _a_bas.a_bz1, sizeof (leer_lsh.a_bez11) - 1); leer_lsh.a_bez11[sizeof (leer_lsh.a_bez11)] = 0; leer_lsh.stk11 = stk; continue; } 
				if (leer_lsh.a12 == 0.0) { leer_lsh.a12 = _a_bas.a; strncpy (leer_lsh.a_bez12, _a_bas.a_bz1, sizeof (leer_lsh.a_bez12) - 1); leer_lsh.a_bez12[sizeof (leer_lsh.a_bez12)] = 0; leer_lsh.stk12 = stk; continue; } 
				if (leer_lsh.a13 == 0.0) { leer_lsh.a13 = _a_bas.a; strncpy (leer_lsh.a_bez13, _a_bas.a_bz1, sizeof (leer_lsh.a_bez13) - 1); leer_lsh.a_bez13[sizeof (leer_lsh.a_bez13)] = 0; leer_lsh.stk13 = stk; continue; } 
				if (leer_lsh.a14 == 0.0) { leer_lsh.a14 = _a_bas.a; strncpy (leer_lsh.a_bez14, _a_bas.a_bz1, sizeof (leer_lsh.a_bez14) - 1); leer_lsh.a_bez14[sizeof (leer_lsh.a_bez14)] = 0; leer_lsh.stk14 = stk; continue; } 
				if (leer_lsh.a15 == 0.0) { leer_lsh.a15 = _a_bas.a; strncpy (leer_lsh.a_bez15, _a_bas.a_bz1, sizeof (leer_lsh.a_bez15) - 1); leer_lsh.a_bez15[sizeof (leer_lsh.a_bez15)] = 0; leer_lsh.stk15 = stk; continue; } 
				if (leer_lsh.a16 == 0.0) { leer_lsh.a16 = _a_bas.a; strncpy (leer_lsh.a_bez16, _a_bas.a_bz1, sizeof (leer_lsh.a_bez16) - 1); leer_lsh.a_bez16[sizeof (leer_lsh.a_bez16)] = 0; leer_lsh.stk16 = stk; continue; } 
				if (leer_lsh.a17 == 0.0) { leer_lsh.a17 = _a_bas.a; strncpy (leer_lsh.a_bez17, _a_bas.a_bz1, sizeof (leer_lsh.a_bez17) - 1); leer_lsh.a_bez17[sizeof (leer_lsh.a_bez17)] = 0; leer_lsh.stk17 = stk; continue; } 
				if (leer_lsh.a18 == 0.0) { leer_lsh.a18 = _a_bas.a; strncpy (leer_lsh.a_bez18, _a_bas.a_bz1, sizeof (leer_lsh.a_bez18) - 1); leer_lsh.a_bez18[sizeof (leer_lsh.a_bez18)] = 0; leer_lsh.stk18 = stk; continue; } 
				if (leer_lsh.a19 == 0.0) { leer_lsh.a19 = _a_bas.a; strncpy (leer_lsh.a_bez19, _a_bas.a_bz1, sizeof (leer_lsh.a_bez19) - 1); leer_lsh.a_bez19[sizeof (leer_lsh.a_bez19)] = 0; leer_lsh.stk19 = stk; continue; } 
				if (leer_lsh.a20 == 0.0) { leer_lsh.a20 = _a_bas.a; strncpy (leer_lsh.a_bez20, _a_bas.a_bz1, sizeof (leer_lsh.a_bez20) - 1); leer_lsh.a_bez20[sizeof (leer_lsh.a_bez20)] = 0; leer_lsh.stk20 = stk; continue; } 

			}
			DbClass.sqlclose (cursor_lsdr);
	
			me_minus = (double) 0; me_plus = (double) 0; pr_vk = (double) 0;
			DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
			DbClass.sqlin ((short *) &lsk.fil, 1, 0);
			DbClass.sqlin ((long *) &lsk.ls, 2, 0);
			DbClass.sqlout ((double *) &_a_bas.a, 3, 0);
			DbClass.sqlout ((char *) _a_bas.a_bz1, 0, 24);
			DbClass.sqlout ((double *) &me_minus, 3, 0);
			DbClass.sqlout ((double *) &me_plus, 3, 0);
			DbClass.sqlout ((double *) &pr_vk, 3, 0);
			cursorl = DbClass.sqlcursor ("select lsp.a, a_bas.a_bz1, "
						"case when lsp.lief_me < 0 then lsp.lief_me * -1 end me_plus, " 
						"case when lsp.lief_me > 0 then lsp.lief_me  end me_minus, "
						"lsp.ls_vk_pr from lsp,a_bas "
						"where lsp.mdn = ? and lsp.fil = ? and lsp.ls = ? and lsp.a = a_bas.a and a_bas.a_typ = 11" );
			while (DbClass.sqlfetch (cursorl) == 0)
			{
				if (leer_lsh.a1 == _a_bas.a || leer_lsh.a1 == 0) {leer_lsh.a1 = _a_bas.a; leer_lsh.stk_zu1 += me_plus; leer_lsh.stk_ab1 += me_minus; leer_lsh.pr_vk1 = pr_vk;  strncpy (leer_lsh.a_bez1, _a_bas.a_bz1, sizeof (leer_lsh.a_bez1) - 1); leer_lsh.a_bez1[sizeof (leer_lsh.a_bez1)] = 0; continue;} 
				if (leer_lsh.a2 == _a_bas.a || leer_lsh.a2 == 0) {leer_lsh.a2 = _a_bas.a; leer_lsh.stk_zu2 += me_plus; leer_lsh.stk_ab2 += me_minus; leer_lsh.pr_vk2 = pr_vk; strncpy (leer_lsh.a_bez2, _a_bas.a_bz1, sizeof (leer_lsh.a_bez2) - 1); leer_lsh.a_bez2[sizeof (leer_lsh.a_bez2)] = 0; continue;} 
				if (leer_lsh.a3 == _a_bas.a || leer_lsh.a3 == 0) {leer_lsh.a3 = _a_bas.a; leer_lsh.stk_zu3 += me_plus; leer_lsh.stk_ab3 += me_minus; leer_lsh.pr_vk3 = pr_vk; strncpy (leer_lsh.a_bez3, _a_bas.a_bz1, sizeof (leer_lsh.a_bez3) - 1); leer_lsh.a_bez3[sizeof (leer_lsh.a_bez3)] = 0; continue;} 
				if (leer_lsh.a4 == _a_bas.a || leer_lsh.a4 == 0) {leer_lsh.a4 = _a_bas.a; leer_lsh.stk_zu4 += me_plus; leer_lsh.stk_ab4 += me_minus; leer_lsh.pr_vk4 = pr_vk; strncpy (leer_lsh.a_bez4, _a_bas.a_bz1, sizeof (leer_lsh.a_bez4) - 1); leer_lsh.a_bez4[sizeof (leer_lsh.a_bez4)] = 0; continue;} 
				if (leer_lsh.a5 == _a_bas.a || leer_lsh.a5 == 0) {leer_lsh.a5 = _a_bas.a; leer_lsh.stk_zu5 += me_plus; leer_lsh.stk_ab5 += me_minus; leer_lsh.pr_vk5 = pr_vk; strncpy (leer_lsh.a_bez5, _a_bas.a_bz1, sizeof (leer_lsh.a_bez5) - 1); leer_lsh.a_bez5[sizeof (leer_lsh.a_bez5)] = 0; continue;} 
				if (leer_lsh.a6 == _a_bas.a || leer_lsh.a6 == 0) {leer_lsh.a6 = _a_bas.a; leer_lsh.stk_zu6 += me_plus; leer_lsh.stk_ab6 += me_minus; leer_lsh.pr_vk6 = pr_vk; strncpy (leer_lsh.a_bez6, _a_bas.a_bz1, sizeof (leer_lsh.a_bez6) - 1); leer_lsh.a_bez6[sizeof (leer_lsh.a_bez6)] = 0; continue;} 
				if (leer_lsh.a7 == _a_bas.a || leer_lsh.a7 == 0) {leer_lsh.a7 = _a_bas.a; leer_lsh.stk_zu7 += me_plus; leer_lsh.stk_ab7 += me_minus; leer_lsh.pr_vk7 = pr_vk; strncpy (leer_lsh.a_bez7, _a_bas.a_bz1, sizeof (leer_lsh.a_bez7) - 1); leer_lsh.a_bez7[sizeof (leer_lsh.a_bez7)] = 0; continue;} 
				if (leer_lsh.a8 == _a_bas.a || leer_lsh.a8 == 0) {leer_lsh.a8 = _a_bas.a; leer_lsh.stk_zu8 += me_plus; leer_lsh.stk_ab8 += me_minus; leer_lsh.pr_vk8 = pr_vk; strncpy (leer_lsh.a_bez8, _a_bas.a_bz1, sizeof (leer_lsh.a_bez8) - 1); leer_lsh.a_bez8[sizeof (leer_lsh.a_bez8)] = 0; continue;} 
				if (leer_lsh.a9 == _a_bas.a || leer_lsh.a9 == 0) {leer_lsh.a9 = _a_bas.a; leer_lsh.stk_zu9 += me_plus; leer_lsh.stk_ab9 += me_minus; leer_lsh.pr_vk9 = pr_vk; strncpy (leer_lsh.a_bez9, _a_bas.a_bz1, sizeof (leer_lsh.a_bez9) - 1); leer_lsh.a_bez9[sizeof (leer_lsh.a_bez9)] = 0; continue;} 
				if (leer_lsh.a10 == _a_bas.a || leer_lsh.a10 == 0) {leer_lsh.a10 = _a_bas.a; leer_lsh.stk_zu10 += me_plus; leer_lsh.stk_ab10 += me_minus; leer_lsh.pr_vk10 = pr_vk; strncpy (leer_lsh.a_bez10, _a_bas.a_bz1, sizeof (leer_lsh.a_bez10) - 1); leer_lsh.a_bez10[sizeof (leer_lsh.a_bez10)] = 0; continue;} 
				if (leer_lsh.a11 == _a_bas.a || leer_lsh.a11 == 0) {leer_lsh.a11 = _a_bas.a; leer_lsh.stk_zu11 += me_plus; leer_lsh.stk_ab11 += me_minus; leer_lsh.pr_vk11 = pr_vk; strncpy (leer_lsh.a_bez11, _a_bas.a_bz1, sizeof (leer_lsh.a_bez11) - 1); leer_lsh.a_bez11[sizeof (leer_lsh.a_bez11)] = 0; continue;} 
				if (leer_lsh.a12 == _a_bas.a || leer_lsh.a12 == 0) {leer_lsh.a12 = _a_bas.a; leer_lsh.stk_zu12 += me_plus; leer_lsh.stk_ab12 += me_minus; leer_lsh.pr_vk12 = pr_vk; strncpy (leer_lsh.a_bez12, _a_bas.a_bz1, sizeof (leer_lsh.a_bez12) - 1); leer_lsh.a_bez12[sizeof (leer_lsh.a_bez12)] = 0; continue;} 
				if (leer_lsh.a13 == _a_bas.a || leer_lsh.a13 == 0) {leer_lsh.a13 = _a_bas.a; leer_lsh.stk_zu13 += me_plus; leer_lsh.stk_ab13 += me_minus; leer_lsh.pr_vk13 = pr_vk; strncpy (leer_lsh.a_bez13, _a_bas.a_bz1, sizeof (leer_lsh.a_bez13) - 1); leer_lsh.a_bez13[sizeof (leer_lsh.a_bez13)] = 0; continue;} 
				if (leer_lsh.a14 == _a_bas.a || leer_lsh.a14 == 0) {leer_lsh.a14 = _a_bas.a; leer_lsh.stk_zu14 += me_plus; leer_lsh.stk_ab14 += me_minus; leer_lsh.pr_vk14 = pr_vk; strncpy (leer_lsh.a_bez14, _a_bas.a_bz1, sizeof (leer_lsh.a_bez14) - 1); leer_lsh.a_bez14[sizeof (leer_lsh.a_bez14)] = 0; continue;} 
				if (leer_lsh.a15 == _a_bas.a || leer_lsh.a15 == 0) {leer_lsh.a15 = _a_bas.a; leer_lsh.stk_zu15 += me_plus; leer_lsh.stk_ab15 += me_minus; leer_lsh.pr_vk15 = pr_vk; strncpy (leer_lsh.a_bez15, _a_bas.a_bz1, sizeof (leer_lsh.a_bez15) - 1); leer_lsh.a_bez15[sizeof (leer_lsh.a_bez15)] = 0; continue;} 
				if (leer_lsh.a16 == _a_bas.a || leer_lsh.a16 == 0) {leer_lsh.a16 = _a_bas.a; leer_lsh.stk_zu16 += me_plus; leer_lsh.stk_ab16 += me_minus; leer_lsh.pr_vk16 = pr_vk; strncpy (leer_lsh.a_bez16, _a_bas.a_bz1, sizeof (leer_lsh.a_bez16) - 1); leer_lsh.a_bez16[sizeof (leer_lsh.a_bez16)] = 0; continue;} 
				if (leer_lsh.a17 == _a_bas.a || leer_lsh.a17 == 0) {leer_lsh.a17 = _a_bas.a; leer_lsh.stk_zu17 += me_plus; leer_lsh.stk_ab17 += me_minus; leer_lsh.pr_vk17 = pr_vk; strncpy (leer_lsh.a_bez17, _a_bas.a_bz1, sizeof (leer_lsh.a_bez17) - 1); leer_lsh.a_bez17[sizeof (leer_lsh.a_bez17)] = 0; continue;} 
				if (leer_lsh.a18 == _a_bas.a || leer_lsh.a18 == 0) {leer_lsh.a18 = _a_bas.a; leer_lsh.stk_zu18 += me_plus; leer_lsh.stk_ab18 += me_minus; leer_lsh.pr_vk18 = pr_vk; strncpy (leer_lsh.a_bez18, _a_bas.a_bz1, sizeof (leer_lsh.a_bez18) - 1); leer_lsh.a_bez18[sizeof (leer_lsh.a_bez18)] = 0; continue;} 
				if (leer_lsh.a19 == _a_bas.a || leer_lsh.a19 == 0) {leer_lsh.a19 = _a_bas.a; leer_lsh.stk_zu19 += me_plus; leer_lsh.stk_ab19 += me_minus; leer_lsh.pr_vk19 = pr_vk; strncpy (leer_lsh.a_bez19, _a_bas.a_bz1, sizeof (leer_lsh.a_bez19) - 1); leer_lsh.a_bez19[sizeof (leer_lsh.a_bez19)] = 0; continue;} 
				if (leer_lsh.a20 == _a_bas.a || leer_lsh.a20 == 0) {leer_lsh.a20 = _a_bas.a; leer_lsh.stk_zu20 += me_plus; leer_lsh.stk_ab20 += me_minus; leer_lsh.pr_vk20 = pr_vk; strncpy (leer_lsh.a_bez20, _a_bas.a_bz1, sizeof (leer_lsh.a_bez20) - 1); leer_lsh.a_bez20[sizeof (leer_lsh.a_bez20)] = 0; continue;} 
			}
			DbClass.sqlclose (cursorl);
		}
	} //directdruck == TRUE



    if (directdruck == FALSE)
	{
		if (!BeldrError (FileName, "gedruckt"))  
		{
			Mess.Message ("Druck OK");
		}
		else
		{
			Mess.Message ("");
		}
		IsGroup = FALSE;
	    WriteLogfile (" Drucken beendet\n");
		CloseLogfile ();
		DeleteFile (FileName.GetBuffer ());
	}

}



void KompAuf::FreeLsGroup (char *where)
{
	int cursor;
	char *tmp;
	Text Sql;
	Text FileName;
	char datum [11];
	char zeit [11];
    char Debug [256];
	extern HWND mamain1;
	CMessageDialog dlg;


    if (RosiSqlDebug)
	{
			 sprintf (Debug, "-q %s", DebugFile);
	}
	else
	{
			 strcpy (Debug, "");
	}


    strcpy (rosipath, getenv (RSDIR));

	CreateLogfile ();

    sysdate (datum);
	systime (zeit);
    WriteLogfile ("Bereichs�bergabe %s %s\n", datum, zeit);
	WriteLogfile ("%s\n", where);

	DWORD CurrentPid = GetCurrentProcessId ();
	tmp = getenv ("TMPPATH");
	if (tmp == NULL)
	{
		FileName.Format ("$$beldr%ld.dat", CurrentPid ); 
	}
	else
	{
		FileName.Format ("%s\\$$beldr%ld.dat", tmp, CurrentPid); 
	}

	FILE *fp = fopen (FileName.GetBuffer (), "w");
	if (fp == NULL) 
	{
	    WriteLogfile ("%s kann nicht ge�ffnet werden\n", FileName.GetBuffer ());
		return;
	}


    Mess.WaitWindow ("Die Lieferscheine werden freigegeben");

	Text PersName = sys_ben.pers_nam;
	PersName.Trim ();
	if (PersName == "")
	{
		PersName = "Fit";
	}
	DbClass.sqlout ((short *) &lsk.mdn, 1, 0);
	DbClass.sqlout ((short *) &lsk.fil, 1, 0);
	DbClass.sqlout ((long *)  &lsk.ls, 2, 0);

	Sql.Format ("select lsk.mdn, lsk.fil, lsk.ls from lsk,kun %s  and lsk.mdn = kun.mdn and lsk.fil = kun.fil and lsk.kun = kun.kun", where);
	cursor = DbClass.sqlcursor (Sql.GetBuffer ());
	while (DbClass.sqlfetch (cursor) == 0)
	{
		fprintf (fp, "L %hd %hd %ld %s F\n", lsk.mdn, lsk.fil, lsk.ls, PersName.GetBuffer ());
		WriteLogfile ("L %hd %hd %ld %s F\n", lsk.mdn, lsk.fil, lsk.ls, PersName.GetBuffer ());
		Sql.Format ("Lieferschein %ld", lsk.ls);
        Mess.Message (Sql.GetBuffer ());
	}
	fclose (fp);
	DbClass.sqlclose (cursor);
	CProcess process;
	Text Command;
	Command.Format ("%s\\BIN\\rswrun %s beldr S %s", rosipath, Debug, FileName.GetBuffer ());
    process.SetCommand (Command);

	HANDLE Pid = process.Start (SW_SHOWNORMAL);
	if (Pid == NULL)
	{
	    WriteLogfile ("Fehler beim Start von %s\n", CallBeldr.GetBuffer ());
	}
	else
	{
		DWORD ExitCode = process.WaitForEnd ();
		if (ExitCode != 0)
		{
			WriteLogfile ("Fehler %ld bei der Ausf�hrung von %s\n", ExitCode, CallBeldr.GetBuffer ());
		}
	}

//	SetWindowPos (mamain1, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

//	FreeLs ();

	Mess.CloseWaitWindow ();

	EnableWindow (hMainWindow, TRUE);
    SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
    SetForegroundWindow (hMainWindow);

/* Hier mu� noc die R�ckgabedatei gelesen werden  */

	if (!BeldrError (FileName, "freigegeben"))  
	{
		Mess.Message ("Freigabe OK");
	}
	else
	{
		Mess.Message ("");
	}

	IsGroup = FALSE;
    WriteLogfile (" Freigabe beendet\n");
	CloseLogfile ();
	DeleteFile (FileName.GetBuffer ());
}


BOOL KompAuf::BeldrError (Text& FileName, char *command)
{
	extern HWND hMainWindow;
	char buffer [512];
	int soll = 0;
	int ist = 0;
	CMessageDialog dlg;

	FILE *fp = fopen (FileName.GetBuffer (), "r");
	if (fp == NULL)
	{
		return FALSE;
	}

	while (fgets (buffer, 511, fp) != NULL)
	{
		strtok (buffer, "\n");
		Token t;
		t.SetSep ("=");
		t = buffer;
		if (t.GetAnzToken () < 2) continue;
		Text name = t.GetToken (0);
		name.Trim ();
		name.MakeUpper ();
		if (name == "SOLL")
		{
			soll = atoi (t.GetToken (1));
		}
		else if (name == "IST")
		{
			ist = atoi (t.GetToken (1));
		}
	}
	fclose (fp);

	if (ist != soll)
	{
		Text msg;
		msg.Format ("Es konnten nur %d von %d Lieferscheinen %s werden", ist, soll, 
			                                                             command);
//		MessageBox (hMainWindow, msg.GetBuffer (), "Fehler", MB_OK | MB_ICONERROR);
		dlg.SetText (msg.GetBuffer ());
		dlg.DoModal ();
		return TRUE;
	}
	else
	{
		Text msg;
		msg.Format ("Es wurden %d von %d Lieferscheinen %s", ist, soll, 
			                                                             command);
//		MessageBox (hMainWindow, msg.GetBuffer (), "Info", MB_OK | MB_ICONINFORMATION);
        
		dlg.SetText (msg.GetBuffer ());
		dlg.DoModal ();

		return TRUE;
	}
    return FALSE;
}



long KompAuf::GenLsNr (short mdn, short fil)
/**
Lieferschein-Nummer generieren.
**/
{
	    char buffer [256];
        int dsqlstatus;
        extern  short sql_mode;
        short sqlm;
        int i = 0;


  	    sprintf (buffer, "Eine Lieferscheinnummer wird generiert");
        Mess.Message (buffer);
		Sleep (20);
		commitwork ();
        sqlm = sql_mode;
        sql_mode = 1;
		beginwork ();
		if (lutz_mdn_par)
		{
			mdn = fil = 0;
		}

//        dsqlstatus = AutoClass.nvholid (mdn, fil, "ls");
        dsqlstatus = AutoClass.nvholid (mdn, 0, "ls");
		commitwork ();
		if (dsqlstatus == -1)
		{
 			                sprintf (buffer, "Fehler bei nvholid");
                            Mess.Message (buffer);
 		                    Sleep (20);
			                beginwork ();
/*
					        DbClass.sqlin ((short *) &mdn, 1, 0);
					        DbClass.sqlin ((short *) &fil, 1, 0);
							dsqlstatus = DbClass.sqlcomm
								    ("delete from auto_nr where nr_nam = \"ls\" "
								     "and mdn = ? and fil = ?");
*/
					        DbClass.sqlin ((short *) &mdn, 1, 0);
							dsqlstatus = DbClass.sqlcomm
								    ("delete from auto_nr where nr_nam = \"ls\" "
								     "and mdn = ? and fil = 0");
							commitwork ();
                            if (dsqlstatus < 0)
							{
                                     disp_mess
										 ("Fehler bei der Generierung der Lieferschein-Nummere",2);
                                     return 0l;
							}
							dsqlstatus = 100;
		}


        i = 0;
        while (dsqlstatus < 0)
        {
			  sprintf (buffer, "Fehler 1 : Zaehler %d Maxwait MAXWAIT", i);
              Mess.Message (buffer);
              if (i == MAXWAIT)
			  {
                        disp_mess ("Fehler bei der Generierung der Lieferschein-Nummere",2);
                        return 0l;
			  }
              Sleep (50);
			  beginwork ();
//              dsqlstatus = AutoClass.nvholid (mdn, fil, "ls");
              dsqlstatus = AutoClass.nvholid (mdn, 0, "ls");
			  commitwork ();
              i ++;
        }

        if (dsqlstatus == 100)
        {
			  beginwork ();
/*
              dsqlstatus = AutoClass.nvanmprf (mdn,
                                     fil,
                                     "ls",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");
*/
              dsqlstatus = AutoClass.nvanmprf (mdn,
                                     0,
                                     "ls",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");
			  commitwork ();

              i = 0;
              while (dsqlstatus < 0)
              {
 			          sprintf (buffer, "Fehler 2 : Zaehler %d Maxwait MAXWAIT", i);
                      Mess.Message (buffer);
                      if (i == MAXWAIT)
					  {
                        disp_mess ("Fehler bei der Generierung der Lieferschein-Nummere",2);
                        return 0l;
					  }

                      Sleep (50);
					  beginwork ();
//                    dsqlstatus = AutoClass.nvholid (mdn, fil, "ls");
/*
                      dsqlstatus = AutoClass.nvanmprf (mdn,
                                                       fil,
                                                       "ls",
                                                       (long) 1,
                                                       (long) 999999,
                                                       (long) 10,
                                                        "");
*/
                      dsqlstatus = AutoClass.nvanmprf (mdn,
                                                       0,
                                                       "ls",
                                                       (long) 1,
                                                       (long) 999999,
                                                       (long) 10,
                                                        "");
					  commitwork ();
                      i ++;
              }

              if (dsqlstatus == 0)
              {
 					  beginwork ();
/*
                      dsqlstatus = AutoClass.nvholid (mdn,
                                                fil,
                                                "ls");
*/
                      dsqlstatus = AutoClass.nvholid (mdn,
                                                0,
                                                "ls");
					  commitwork ();
              }
         }

         i = 0;
         while (dsqlstatus < 0)
         {
			  sprintf (buffer, "Fehler 3 : Zaehler %d Maxwait MAXWAIT", i);
              Mess.Message (buffer);
              if (i == MAXWAIT) break;
              Sleep (50);
		      beginwork ();
//              dsqlstatus = AutoClass.nvholid (mdn, fil, "ls");
              dsqlstatus = AutoClass.nvholid (mdn, 0, "ls");
		      commitwork ();
              i ++;
         }

		 beginwork ();
         if (dsqlstatus < 0)
         {
             disp_mess ("Fehler bei der Generierung der Lieferschein-Nummere",2);
             return 0l;
         }
         sql_mode = sqlm;
   	     sprintf (buffer, "Lieferscheinnummer %ld generiert", auto_nr.nr_nr);
         Mess.Message (buffer);
         Sleep (20);
         return auto_nr.nr_nr;
}

long KompAuf::GenLsNr0 (short mdn, short fil)
/**
Lieferschein-Nummer generieren und pruefen, ob die Nummer schon existiert.
**/
{
	     static long ls_nr;
		 static short mdn_nr;
		 static short fil_nr;
		 static int tcursor = -1;
		 int dsqlstatus;
		 int i;


		 if (tcursor == -1)
		 {
			 if (lutz_mdn_par == 0)
			 {
			     DbClass.sqlin ((short *) &mdn_nr, 1, 0);
			     DbClass.sqlin ((short *) &fil_nr, 1, 0);
			     DbClass.sqlin ((long *)  &ls_nr, 2, 0);
			     tcursor = DbClass.sqlcursor ("select ls from lsk where mdn = ? and fil = ? and "
				                          "ls = ?");
			 }
			 else
			 {
			     DbClass.sqlin ((long *)  &ls_nr, 2, 0);
			     tcursor = DbClass.sqlcursor ("select ls from lsk where ls = ?");
			 }
		 }

		 i = 0;
		 mdn_nr = mdn;
		 fil_nr = fil;
		 ls_nr = GenLsNr (mdn, fil);
		 while (ls_nr > 0l)
		 {
			 DbClass.sqlopen (tcursor);
			 dsqlstatus = DbClass.sqlfetch (tcursor);
			 if (dsqlstatus == 100) return ls_nr;
             Sleep (50);
			 i ++;
             if (i == MAXWAIT) return 0l;
  		     ls_nr = GenLsNr (mdn, fil);
		 }
         return ls_nr;
}

double KompAuf::GetAufMeVgl (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;

		 lese_a_bas (lsp.a);
         einh_class.AktAufEinh (lsk.mdn, lsk.fil,
                                lsk.kun, lsp.a, lsp.me_einh_kun);
         einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                                lsk.kun, lsp.a, &keinheit);

         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                      lsp.auf_me_vgl = lsp.auf_me;
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     lsp.auf_me_vgl = lsp.auf_me * keinheit.inh;
         }
         return lsp.auf_me_vgl;
}


double KompAuf::GetGew (void)
/**
Gewicht aller Positionen mit me_einh = 2 holen.
**/
{
	     int cursor;
	     int dsqlstatus;
		 double gew;

		 DbClass.sqlin  ((short *) &lsk.mdn, 1, 0);
		 DbClass.sqlin  ((short *) &lsk.fil, 1, 0);
		 DbClass.sqlin  ((long *)  &lsk.ls, 2, 0);

		 DbClass.sqlout ((short *)  &lsp.me_einh, 1, 0);
		 DbClass.sqlout ((double *) &lsp.lief_me, 3, 0);
		 DbClass.sqlout ((double *) &lsp.a, 3, 0);
         cursor = DbClass.sqlcursor ("select me_einh, lief_me, a from lsp "
			                         "where mdn = ? "
									 "and fil = ? "
									 "and ls = ?");
		 gew = 0.0;
         dsqlstatus = DbClass.sqlfetch (cursor);
		 while (dsqlstatus == 0)
		 {
			 if (lsp.me_einh == 2)
			 {
				 gew += GetAufMeVgl ();
			 }
             dsqlstatus = DbClass.sqlfetch (cursor);
		 }
		 DbClass.sqlclose (cursor);
		 return gew;
}



long KompAuf::GenLspTxt0 (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	int count;
	long nr;

	nr = 0l;
    nr = AutoNr.GetNrLs (nr);
	count = 0;
	while (nr == 0l)
	{
		         Sleep (10);
		         count ++;
				 if (count == 10) break;
                 nr = AutoNr.GetNrLs (nr);
	}
	return nr;
}

BOOL KompAuf::TxtNrExist (long nr)
{
	   int dsqlstatus;

	   if (wa_pos_txt)
	   {
               DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
               DbClass.sqlin ((short *) &lsk.fil, 1, 0);
			   DbClass.sqlin ((long *)  &nr, 2, 0);
			   dsqlstatus = DbClass.sqlcomm ("select posi from lsp_txt "
				                             "where mdn = ? "
											 "and fil = ? "
											 "and posi = ?");
	   }
	   else
	   {
			   DbClass.sqlin ((long *)  &nr, 2, 0);
			   dsqlstatus = DbClass.sqlcomm ("select nr from lspt where nr = ?");
	   }
	   if (dsqlstatus == 100) return FALSE;
	   return TRUE;
}


long KompAuf::GenLspTxt (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	 long nr;

	 while (TRUE)
	 {
		 nr = GenLspTxt0 ();
		 if (TxtNrExist (nr) == FALSE) break;
		 if (nr > 99999999)
		 {
			 return 0l;
		 }
	 }
	 return nr;
}


void KompAuf::SetBsdKz (int kz)
{
	      bsd_kz = kz;
}

/*
long KompAuf::GetLagerort (double a)
Lagerort aus a_lgr holen.
{
	     long lgr;

		 lgr = 0l;
		 DbClass.sqlin   ((short *)  &lsk.mdn, 1, 0);
		 DbClass.sqlin   ((double *) &a, 3, 0);
		 DbClass.sqlout  ((long *)  &lgr, 2, 0);
		 DbClass.sqlcomm ("select lgr from a_lgr where mdn = ? "
			              "and a = ?");
         return lgr;
}
*/


long KompAuf::GetLagerort (long akt_lager, double a)
/**
Lagerort aus a_lgr holen.
**/
{
	     long lgr;
         static long lgr_gr = 0l;
         static BOOL lgrOK = FALSE;
         char cfg_v [12];

		 lgr = 0l;

         if (akt_lager != 0)
         {
              lgr_gr = akt_lager;
              lgrOK = TRUE;
         }
         else if (! lgrOK)
         {
              if (ProgCfg != NULL &&
                  ProgCfg->GetGroupDefault ("lager", cfg_v) == TRUE)
              {
                     lgr_gr = atol (cfg_v);
              }
              lgrOK = TRUE;
         }

         if (lgr_gr > 0l)
         {
             DbClass.sqlin   ((short *)  &lsk.mdn, 1, 0);
		     DbClass.sqlin   ((double *) &a, 3, 0);
 		     DbClass.sqlin   ((long *)   &lgr_gr, 2, 0);
 		     DbClass.sqlout  ((long *)   &lgr, 2, 0);
   		     DbClass.sqlcomm ("select lgr.lgr from lgr, a_lgr "
                              "where lgr.mdn = ? "
                              "and a_lgr.mdn = lgr.mdn "
                              "and a_lgr.a = ? "
                              "and a_lgr.lgr = lgr.lgr "
			                  "and lgr.lgr_gr = ? ");
// Bei mehreren Eintr�gen in lgr hier Auswahl aufrufen 26.06.2002
         }

         if (lgr_gr == 0l)
         {
             DbClass.sqlin   ((short *)  &lsk.mdn, 1, 0);
		     DbClass.sqlin   ((double *) &a, 3, 0);
		     DbClass.sqlout  ((long *)  &lgr, 2, 0);
		     DbClass.sqlcomm ("select lgr from a_lgr where mdn = ? "
			                  "and a = ?");
// Bei mehreren Eintr�gen in a_lgr hier Auswahl aufrufen 26.06.2002
         }
         return lgr;
}

BOOL BsdArtikel (double a)
/**
Bestandskennzeichen in a_bas pruefen.
**/
{
	     char bsd_kz [2];

		 strcpy (bsd_kz, "N");
		 DbClass.sqlin ((double *) &a, 3, 0);
		 DbClass.sqlout ((char *) bsd_kz, 0, 2);
		 DbClass.sqlcomm ("select bsd_kz from a_bas where a = ?");
		 if (bsd_kz[0] == 'J') return TRUE;
		 return FALSE;
}


BOOL KompAuf::BsdArtikel (double a)
/**
Bestandskennzeichen in a_bas pruefen.
**/
{
	     char bsd_kz [2];

		 strcpy (bsd_kz, "N");
		 DbClass.sqlin ((double *) &a, 3, 0);
		 DbClass.sqlout ((char *) bsd_kz, 0, 2);
		 DbClass.sqlcomm ("select bsd_kz from a_bas where a = ?");
		 if (bsd_kz[0] == 'J') return TRUE;
		 return FALSE;
}

void KompAuf::BucheBsd (double a, double lief_me,  double pr_vk)
/**
Bestandsbuchung vorbereiten.
**/
{
	double buchme;
	char datum [12];
	long lgrort;

	if (bsd_kz == 0) return;
	if (BsdArtikel (a) == FALSE) return;

	buchme = lief_me;

	bsd_buch.nr  = lsk.ls;
	strcpy (bsd_buch.blg_typ, "WA");
	bsd_buch.mdn = lsk.mdn;
	bsd_buch.fil = lsk.fil;
	bsd_buch.kun_fil = lsk.kun_fil;
	bsd_buch.a   = a;
	sysdate (datum);
	bsd_buch.dat = dasc_to_long (datum);
	systime (bsd_buch.zeit);
	strcpy (bsd_buch.pers, sys_ben.pers);

/*
Lager holen
*/
	lgrort = GetLagerort (0l, a);

    sprintf (bsd_buch.bsd_lgr_ort, "%ld", lgrort);
	bsd_buch.qua_status = 0;
	bsd_buch.me = buchme * -1;
	bsd_buch.bsd_ek_vk = pr_vk;
    strcpy (bsd_buch.chargennr, "");
    strcpy (bsd_buch.ident_nr, "");
    strcpy (bsd_buch.herk_nachw, "");
    sprintf (bsd_buch.lief, "%ld", lsk.kun);
    bsd_buch.nr = lsk.ls;
    strcpy  (bsd_buch.verfall, "");
    bsd_buch.delstatus = 0;
    strcpy  (bsd_buch.err_txt, "531:17.09.2003");
	BsdBuch.dbinsert ();
}




int KompAuf::Geteinz_ausw (void)
/**
**/
{
	      int einz_ausw;
		  int dsqlstatus;

          DbClass.sqlin  ((short *) &lsk.mdn,  1, 0);
          DbClass.sqlin  ((long *)  &lsk.kun,  2, 0);
		  DbClass.sqlout ((short *) &einz_ausw, 1, 0);
		  einz_ausw = 0;
          dsqlstatus = DbClass.sqlcomm ("select einz_ausw from kun "
			                            "where mdn = ? "
	 						            "and   kun = ? ");
          if (einz_ausw > 6) return 1;
		  return einz_ausw;
}




void KompAuf::ShowLocks (HWND hWnd)
/**
Auftraege, die nicht freigeben oder gedruckt werden konnten, anzeigen.
**/
{
	int wlen;
	int i;
	char buffer [80];

    if (aufanzlock == 0) return;

    SetHLines (0);
    SetVLines (0);
	SetMenSelect (FALSE);
	wlen = 12;
	if (aufanzlock < 12) wlen = aufanzlock;

	EnableWindows (hWnd, FALSE);
	OpenListWindowBu (hWnd, wlen, 40, 6, 20, 1);
    InsertListUb (NULL);
	SetWindowText (GethListBox (), "Nicht bearbeitete Auftr�ge");
	for (i = 0; i < aufanzlock; i ++)
	{
		sprintf (buffer,"    Auftrag %ld", auftablock [i]);
        InsertListRow (buffer);
	}
    EnterQueryListBox ();
	EnableWindows (hWnd, TRUE);
    SetMenSelect (TRUE);
}

void KompAuf::ShowNoDrk (HWND hWnd)
/**
Auftraege, die beim Freigeben noch nicht gedruckt waren, anzeigen.
**/
{
	int wlen;
	int i;
	char buffer [80];

    if (drklsanz == 0) return;

    SetHLines (0);
    SetVLines (0);
	SetMenSelect (FALSE);
	wlen = 12;
	if (drklsanz < 12) wlen = drklsanz;

	EnableWindows (hWnd, FALSE);
	OpenListWindowBu (hWnd, wlen, 40, 6, 20, 1);
    InsertListUb (NULL);
	SetWindowText (GethListBox (), "Nicht gedruckte Auftr�ge");
	for (i = 0; i < drklsanz; i ++)
	{
		sprintf (buffer,"    Auftrag %ld", drklstab [i]);
        InsertListRow (buffer);
	}
    EnterQueryListBox ();
	EnableWindows (hWnd, TRUE);
    SetMenSelect (TRUE);
}


void KompAuf::InitAufanz (void)
/**
aufanz auf 0 setzen.
**/
{
	aufanz = 0;
	aufanzlock = 0;
}

void KompAuf::KompParams (void)
/**
Systemparameter lesen.
**/
{
	static BOOL params_ok = FALSE;

	if (params_ok) return;

	params_ok = TRUE;
    strcpy (sys_par.sys_par_nam,"auf_text_par");
    if (sys_par_class.dbreadfirst () == 0)
	{
                 auf_text_par = atoi (sys_par.sys_par_wrt);
	}
    strcpy (sys_par.sys_par_nam,"wa_pos_txt");
    if (sys_par_class.dbreadfirst () == 0)
	{
                 wa_pos_txt = atoi (sys_par.sys_par_wrt);
	}
    strcpy (sys_par.sys_par_nam,"llls1");
    if (sys_par_class.dbreadfirst () == 0)
	{
                 ll_ls_par = atoi (sys_par.sys_par_wrt);
	}
    strcpy (sys_par.sys_par_nam,"llrech1");
    if (sys_par_class.dbreadfirst () == 0)
	{
                 ll_rech_par = atoi (sys_par.sys_par_wrt);
	}
}


void KompAuf::LockLs (void)
/**
Auftraege sperren.
**/
{
	int i;

    for (i = 0; i < aufanz; i ++)
	{
                      lsk.mdn = mdntab[i];
                      lsk.fil = filtab[i];
                      lsk.ls = auftab[i];
                      DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
                      DbClass.sqlin ((short *) &lsk.fil, 1, 0);
                      DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
                      DbClass.sqlcomm ("update lsk set delstatus = -1 "
						               "where mdn = ? "
									   "and   fil = ? "
									   "and ls   = ?");
	}
}


void KompAuf::TestFree (void)
/**
Testen, ob freie Auftraege inzwischen gesperrt sind.
**/
{
	int i;
    short mdntab0[1000];
    short filtab0[1000];
    long auftab0[1000];
    int dsqlstatus;
	int j;

	if (aufanz == 0) return;
    for (i = 0, j = 0; i < aufanz; i ++)
	{
                      lsk.mdn = mdntab[i];
                      lsk.fil = filtab[i];
                      lsk.ls = auftab[i];
                      DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
                      DbClass.sqlin ((short *) &lsk.fil, 1, 0);
                      DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
                      dsqlstatus = DbClass.sqlcomm ("select ls from lsk "
						                            "where mdn = ? "
									                "and   fil = ? "
									                "and ls = ? "
									                "and delstatus = 0");
					  if (dsqlstatus == 0)
					  {
						  mdntab0[j]  = mdntab[i];
						  filtab0[j]  = filtab[i];
						  auftab0[j]  = auftab[i];
						  j ++;
					  }
					  else
					  {
		                  mdntablock[aufanzlock] = mdntab[i];
		                  filtablock[aufanzlock] = filtab[i];
		                  auftablock[aufanzlock] = auftab[i];
						  aufanzlock ++;
					  }
	}
	if (j == aufanz) return;
	aufanz = j;
	for (i = 0; i < aufanz; i ++)
	{
			  mdntab[i]  = mdntab0[i];
			  filtab[i]  = filtab0[i];
			  auftab[i]  = auftab0[i];
	}
}


void KompAuf::FreeLs (void)
/**
Gesperrte Auftraege freigeben.
**/
{
	int i;
	int dsqlstatus;
	extern short sql_mode;

    sql_mode = 1;
    for (i = 0; i < aufanz; i ++)
	{
                      lsk.mdn = mdntab[i];
                      lsk.fil = filtab[i];
                      lsk.ls = auftab[i];
                      DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
                      DbClass.sqlin ((short *) &lsk.fil, 1, 0);
                      DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
                      dsqlstatus = DbClass.sqlcomm ("update lsk set delstatus = 0 "
						                            "where mdn = ? "
									                "and   fil = ? "
									                "and ls = ?");
					if (dsqlstatus < 0)
					{
						print_mess (2, "Achtung !!\n"
							           "Lieferschein %ld kann nicht zur�ckgesetzt werden",
                                        lsk.ls);
					}
	}
	sql_mode = 0;
	aufanz = 0;
}

void KompAuf::Getauf_a_sort (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    auf_a_sort = 0;
    strcpy (sys_par.sys_par_nam,"auf_a_sort");
    if (sys_par_class.dbreadfirst () == 0)
    {
        auf_a_sort = atoi (sys_par.sys_par_wrt);
    }
}

void KompAuf::K_Liste_Direct (void)
/**
Kommissionierliste ohne Listgenerator drucken.
**/
{
     char command [512];

	 sprintf (command, "rswrun aufdr 1 %hd %hd %ld %ld 1 9 01.01.1990 31.12.2099",
                        lsk.mdn, lsk.fil, lsk.ls, lsk.auf);
	 ProcWaitExec (command, SW_SHOWNORMAL, -1, 0, -1, 0);
	CreateLogfile ();
    WriteLogfile ("Auftrag %ld wurde �ber Direktdruck gedruckt\n", lsk.ls);
	CloseLogfile ();
}


void KompAuf::K_Liste_Fax (void)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];
	HWND akthWnd;

	akthWnd = GetActiveWindow ();
	SetAusgabe (FAX_OUT);
    if (syskey == KEY5) return;
    if (IsGroup == FALSE) commitwork ();
    if (LeseFormat (drformat) == FALSE)
    {
		           return;
    }


    sprintf (valfrom, "%hd", lsk.mdn);
    sprintf (valto, "%hd", lsk.mdn);
    FillFormFeld ("lsk.mdn", valfrom, valto);

    sprintf (valfrom, "%hd", lsk.fil);
    sprintf (valto, "%hd", lsk.fil);
    FillFormFeld ("lsk.fil", valfrom, valto);

    sprintf (valfrom, "%ld", lsk.ls);
    sprintf (valto, "%ld", lsk.ls);
    FillFormFeld ("lsk.ls", valfrom, valto);
    StartPrint ();

	SetActiveWindow (akthWnd);
	CreateLogfile ();
    WriteLogfile ("Auftrag %ld wurde gefaxt\n", lsk.ls);
	CloseLogfile ();
//    InitPid ();
}


void KompAuf::K_Liste (void)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];

	SetAusgabe (PRINTER_OUT);
//    AuftoPid ();
    if (syskey == KEY5) return;
    if (IsGroup == FALSE) commitwork ();

	if (klst_dr_par)
	{
		K_Liste_Direct ();
	}
	else
	{
        if (LeseFormat (drformat) == FALSE)
		{
		           return;
		}

        sprintf (valfrom, "%hd", lsk.mdn);
        sprintf (valto, "%hd", lsk.mdn);
        FillFormFeld ("lsk.mdn", valfrom, valto);

        sprintf (valfrom, "%hd", lsk.fil);
        sprintf (valto, "%hd", lsk.fil);
        FillFormFeld ("lsk.fil", valfrom, valto);

        sprintf (valfrom, "%ld", lsk.ls);
        sprintf (valto, "%ld", lsk.ls);
        FillFormFeld ("lsk.ls", valfrom, valto);
        StartPrint ();
	}
	CreateLogfile ();
    WriteLogfile ("Lieferschein %ld wurde gedruckt\n", lsk.ls);
	CloseLogfile ();
//    InitPid ();
    DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
    DbClass.sqlin ((short *) &lsk.fil, 1, 0);
    DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
    DbClass.sqlcomm ("update lsk set ls_stat = 4 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and ls = ? ");
    lsk.ls_stat = 4;
    if (IsGroup == FALSE) Mess.Message ("Der Lieferschein wurde gedruckt");
}


void KompAuf::K_ListeGroup (short mdn_von, short mdn_bis,
							short fil_von, short fil_bis,
							long ls_von, long ls_bis,
							char *dat_von, char *dat_bis,
							short stat_von, short stat_bis,
							long tou_von, long tou_bis,
							short abt_von, short abt_bis,
							long kun_von,  long kun_bis)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];
	long ldat_von, ldat_bis;
	int cursor;
//	int cursor_pos;
	short mdn, fil;
	long ls;
	int i;
	int dsqlstatus;
	char datum [11];
	char zeit [11];
	extern short sql_mode;

	CreateLogfile ();

    sysdate (datum);
	systime (zeit);
    WriteLogfile ("Bereichsdruck Kommisionerliste %s %s\n", datum, zeit);

	Mess.WaitWindow ("Die Auftr�ge werden gedruckt");

	max_ls = ls_bis;
    ldat_von = dasc_to_long (dat_von);
    ldat_bis = dasc_to_long (dat_bis);

// Auftr�ge nach Teilsortimentsgruppen splitten

    DbClass.sqlin ((short *) &mdn_von, 1, 0);
    DbClass.sqlin ((short *) &mdn_bis, 1, 0);
    DbClass.sqlin ((short *) &fil_von, 1, 0);
    DbClass.sqlin ((short *) &fil_bis, 1, 0);
    DbClass.sqlin ((long *)  &ls_von, 2, 0);
    DbClass.sqlin ((long *)  &ls_bis, 2, 0);
    DbClass.sqlin ((long *)  &kun_von, 2, 0);
    DbClass.sqlin ((long *)  &kun_bis, 2, 0);
    DbClass.sqlin ((long *)  &ldat_von, 2, 0);
    DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
    DbClass.sqlin ((short *) &stat_von, 1, 0);
    DbClass.sqlin ((short *) &stat_bis, 1, 0);
    DbClass.sqlin ((long *) &tou_von, 2, 0);
    DbClass.sqlin ((long *) &tou_bis, 2, 0);

    DbClass.sqlout ((long *) &lsk.mdn, 1, 0);
    DbClass.sqlout ((long *) &lsk.fil, 1, 0);
    DbClass.sqlout ((long *) &lsk.ls, 2, 0);
    cursor = DbClass.sqlcursor ("select lsk.mdn, lsk.fil, lsk.ls from lsk "
                          "where lsk.mdn between ? and ?"
                          "and lsk.fil between ? and ? "
                          "and lsk.auf between  ? and ? "
                          "and lsk.kun between  ? and ? "
                          "and lsk.lieferdat between  ? and ? "
                          "and lsk.ls_stat between  ? and ? "
                          "and lsk.tou between ? and ? "
						  "and delstatus = 0");

//	AufToSmtDrk (cursor, &ls_bis);


// Gesperrte Auftraege merken

    DbClass.sqlin ((short *) &mdn_von, 1, 0);
    DbClass.sqlin ((short *) &mdn_bis, 1, 0);
    DbClass.sqlin ((short *) &fil_von, 1, 0);
    DbClass.sqlin ((short *) &fil_bis, 1, 0);
    DbClass.sqlin ((long *)  &ls_von, 2, 0);
    DbClass.sqlin ((long *)  &ls_bis, 2, 0);
    DbClass.sqlin ((long *)  &kun_von, 2, 0);
    DbClass.sqlin ((long *)  &kun_bis, 2, 0);
    DbClass.sqlin ((long *)  &ldat_von, 2, 0);
    DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
    DbClass.sqlin ((short *) &stat_von, 1, 0);
    DbClass.sqlin ((short *) &stat_bis, 1, 0);
    DbClass.sqlin ((long *) &tou_von, 2, 0);
    DbClass.sqlin ((long *) &tou_bis, 2, 0);

    DbClass.sqlout ((long *) &mdn, 1, 0);
    DbClass.sqlout ((long *) &fil, 1, 0);
    DbClass.sqlout ((long *) &ls, 2, 0);
    cursor = DbClass.sqlcursor ("select lsk.mdn, lsk.fil, lsk.auf from lsk "
                          "where lsk.mdn between ? and ?"
                          "and lsk.fil between ? and ? "
                          "and lsk.ls between  ? and ? "
                          "and lsk.kun between  ? and ? "
                          "and lsk.lieferdat between  ? and ? "
                          "and lsk.ls_stat between  ? and ? "
                          "and lsk.tou between ? and ? "
						  "and delstatus != 0");
	aufanzlock = 0;
	DbClass.sqlopen (cursor);
	dsqlstatus = DbClass.sqlfetch (cursor);
	if (dsqlstatus == 0)
	{
		    WriteLogfile  ("Gesperrte Lieferscheine\n");
	}
	while (dsqlstatus == 0)
	{
		    mdntablock[aufanzlock] = mdn;
		    filtablock[aufanzlock] = fil;
		    auftablock[aufanzlock] = ls;
			WriteLogfile ("   LS %ld\n", ls);
			if (aufanz == 999)
			{
				disp_mess ("Maximale Anzahl Lieferscheine �berschritten", 2);
				break;
			}
	        dsqlstatus = DbClass.sqlfetch (cursor);
			aufanzlock ++;
	}
	DbClass.sqlclose (cursor);


// Freie Auftraege merken

    DbClass.sqlin ((short *) &mdn_von, 1, 0);
    DbClass.sqlin ((short *) &mdn_bis, 1, 0);
    DbClass.sqlin ((short *) &fil_von, 1, 0);
    DbClass.sqlin ((short *) &fil_bis, 1, 0);
    DbClass.sqlin ((long *)  &ls_von, 2, 0);
    DbClass.sqlin ((long *)  &ls_bis, 2, 0);
    DbClass.sqlin ((long *)  &kun_von, 2, 0);
    DbClass.sqlin ((long *)  &kun_bis, 2, 0);
    DbClass.sqlin ((long *)  &ldat_von, 2, 0);
    DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
    DbClass.sqlin ((short *) &stat_von, 1, 0);
    DbClass.sqlin ((short *) &stat_bis, 1, 0);
    DbClass.sqlin ((long *) &tou_von, 2, 0);
    DbClass.sqlin ((long *) &tou_bis, 2, 0);

    DbClass.sqlout ((long *) &mdn, 1, 0);
    DbClass.sqlout ((long *) &fil, 1, 0);
    DbClass.sqlout ((long *) &ls, 2, 0);
    cursor = DbClass.sqlcursor ("select lsk.mdn, lsk.fil, lsk.ls from lsk "
                          "where lsk.mdn between ? and ?"
                          "and lsk.fil between ? and ? "
                          "and lsk.ls between  ? and ? "
                          "and lsk.kun between  ? and ? "
                          "and lsk.lieferdat between  ? and ? "
                          "and lsk.ls_stat between  ? and ? "
                          "and lsk.tou between ? and ? "
						  "and delstatus = 0");

	aufanz = 0;
	DbClass.sqlopen (cursor);
	dsqlstatus = DbClass.sqlfetch (cursor);
	if (dsqlstatus == 0)
	{
		    WriteLogfile  ("Nicht gesperrte Lieferscheine\n");
	}
	while (dsqlstatus == 0)
	{
		    mdntab[aufanz] = mdn;
		    filtab[aufanz] = fil;
		    auftab[aufanz] = ls;
			WriteLogfile ("   LS %ld\n", ls);
			if (aufanz == 999)
			{
 			    WriteLogfile ("Maximale Anzahl Lieferscheine �berschritten\n");
				disp_mess ("Maximale Anzahl Lieferscheine �berschritten", 2);
				break;
			}
	        dsqlstatus = DbClass.sqlfetch (cursor);
			aufanz ++;
	}
	DbClass.sqlclose (cursor);

	SetAusgabe (PRINTER_OUT);
//    AuftoPid ();
//    ChoisePrinter0 ();
    if (syskey == KEY5) return;
    commitwork ();

    if (LeseFormat (drformat) == FALSE)
	{
  	    Mess.CloseWaitWindow ();
		return;
	}

	WriteLogfile ("�bergabewerte an Listgenerator\n");

    sprintf (valfrom, "%hd", mdn_von);
    sprintf (valto, "%hd", mdn_bis);
    FillFormFeld ("lsk.mdn", valfrom, valto);
	WriteLogfile (" Mandant von %hd bis %hd\n", mdn_von, mdn_bis);

    sprintf (valfrom, "%hd", fil_von);
    sprintf (valto, "%hd", fil_bis);
    FillFormFeld ("lsk.fil", valfrom, valto);
	WriteLogfile (" Filiale von %hd bis %hd\n", fil_von, fil_bis);


    sprintf (valfrom, "%ld", ls_von);
    sprintf (valto, "%ld",   ls_bis);
    FillFormFeld ("lsk.ls", valfrom, valto);
	WriteLogfile (" Lieferschein von %ld bis %ld\n", ls_von, ls_bis);

    sprintf (valfrom, "%s", dat_von);
    sprintf (valto, "%s", dat_bis);
    FillFormFeld ("lsk.lieferdat", valfrom, valto);
	WriteLogfile (" Lieferdatum von %s bis %s\n", dat_von, dat_bis);

    sprintf (valfrom, "%hd", stat_von);
    sprintf (valto, "%hd", stat_bis);
    FillFormFeld ("lsk.ls_stat", valfrom, valto);
	WriteLogfile (" Status von %hd bis %hd\n", stat_von, stat_bis);

    sprintf (valfrom, "%ld", tou_von);
    sprintf (valto, "%ld",   tou_bis);
    FillFormFeld ("lsk.tou", valfrom, valto);
	WriteLogfile (" Tour von %ld bis %ld\n", tou_von, tou_bis);

	if (AbtRemoved == FALSE)
	{
             sprintf (valfrom, "%hd", abt_von);
             sprintf (valto, "%hd",   abt_bis);
             FillFormFeld ("a_bas.abt", valfrom, valto);
	         WriteLogfile (" Abt von %hd bis %hd\n", abt_von, abt_bis);
	}

	if (DrkKunRemoved == FALSE)
	{
             sprintf (valfrom, "%ld", kun_von);
             sprintf (valto, "%ld",   kun_bis);
             FillFormFeld ("lsk.kun", valfrom, valto);
	         WriteLogfile (" Kunde von %ld bis %ld\n", kun_von, kun_bis);
	}

    StartPrint ();
//    InitPid ();
	sql_mode = 1;
	for (i = 0; i < aufanz; i ++)
	{
		    mdn = mdntab[i];
			fil = filtab[i];
			ls = auftab[i];
/*
		    DbClass.sqlopen (cursor_pos);
		    if (DbClass.sqlfetch (cursor_pos) == 0)
*/
			{
 	                beginwork ();
                    DbClass.sqlin ((long *) &mdn, 1, 0);
                    DbClass.sqlin ((long *) &fil, 1, 0);
                    DbClass.sqlin ((long *) &ls, 2, 0);
                    dsqlstatus = DbClass.sqlcomm ("update lsk set ls_stat = 4 "
                             "where mdn = ? "
                             "and fil = ? "
                             "and ls = ? "
							 "and ls_stat < 4");
					if (dsqlstatus < 0)
					{
						print_mess (2, "Achtung !!\n"
							           "Auftrag %ld kann nicht auf Status\n"
									   "gedruckt gesetzt werden", ls);
					}
	                commitwork ();
			}
	}
	sql_mode = 0;
//	DbClass.sqlclose (cursor_pos);
	Mess.CloseWaitWindow ();
    Mess.Message ("Die Auftr�ge wurden gedruckt");
	CopyListe ();
	CloseLogfile ();
}


void KompAuf::K_ListeGroup0 (char *where)
/**
Mehrere Auftraege uebertragen.
**/
{
	int cursor;
	char sqls [1028];

    KompAuf::ChoisePrinter0 ();
	IsGroup = TRUE;
    sprintf (sqls, "select mdn,fil,ls, tou, lieferdat from lsk %s", where);

    DbClass.sqlout ((short *) &lsk.mdn, 1, 0);
    DbClass.sqlout ((short *) &lsk.fil, 1, 0);
    DbClass.sqlout ((long *) &lsk.ls, 2, 0);
    DbClass.sqlout ((long *) &lsk.tou, 2,0);
    DbClass.sqlout ((long *) &lsk.lieferdat, 2, 0);
	cursor = DbClass.sqlcursor (sqls);
	Mess.WaitWindow ("Die Auftr�ge werden gedruckt");
	beginwork ();
	while (DbClass.sqlfetch (cursor) == 0)
	{
            ls_class.lese_lsk (lsk.mdn, lsk.fil, lsk.ls);
            sprintf (sqls, "Auftrag %ld", lsk.ls);
	 	    Mess.Message (sqls);
			K_Liste ();
	}
	commitwork ();
	Mess.CloseWaitWindow ();
	IsGroup = FALSE;
	DbClass.sqlclose (cursor);
}


/**
Fen_par fak_typ waehlen.
**/

static int nix       = 0;
static int bar       = 0;
static int anonym    = 0;

static int *fakbuttons [] = {&nix, &bar, &anonym, NULL};

static int SetNix (void);
static int SetBar (void);
static int SetAnonym (void);
static int BreakBox (void);
static int BoxBreak (void);

static int IsFakChoise;

static ITEM iNix      ("",  "Normalverkauf",           "", 0);
static ITEM iBarEx    ("",  "Barverkauf mit Adresse",  "", 0);
static ITEM iAnonym   ("",  "Barverkauf ohne Adresse", "", 0);

static int SetFakDef (void);

static field _fakbox [] = {
&iNix,                   26, 2, 1,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetNix, 101,
&iBar,                   26, 2, 3,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                              SetBar, 102,
/*
&iAnonym,                26, 2, 4,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetAnonym, 103,
*/
&iOK,                    15, 0, 7,  6,  0, "", BUTTON, 0, BreakBox, 107,
&iCancel,                15, 0, 7, 23, 0, "",  BUTTON, 0, BoxBreak, KEY5};

form fakbox = {4, 0, 0, _fakbox, 0, SetFakDef, 0, 0, NULL};


int SetFakDef (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          nix = 1;
          SendMessage (fakbox.mask[0].feldid, BM_SETCHECK,
                       nix, 0l);
          SetFocus (fakbox.mask[0].feldid);
          SetCurrentField (0);
          UnsetBox (0);
          return 1;
}


int FakEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

void UnsetFakBox (int pos)
/**
Button auf selectierte setzen. (ausser pos);
**/
{
        int i;

        for (i = 0; fakbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (fakbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *fakbuttons [i] = 0;
        }
}


int SetNix (void)
/**
K-Liste setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (fakbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       nix = 0;
          }
          else
          {
                       nix = 1;
          }
          if (syskey == KEYCR)
          {
                       nix = 1;
                       SendMessage (fakbox.mask[0].feldid, BM_SETCHECK,
                       nix, 0l);
                       break_enter ();
          }
          UnsetFakBox (0);
          return 1;
}

int SetBarEx (void)
/**
Uebergabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (fakbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        bar = 0;
          }
          else
          {
                        bar  = 1;
          }
          if (syskey == KEYCR)
          {
                       bar  = 1;
                       SendMessage (fakbox.mask[1].feldid, BM_SETCHECK,
                       bar, 0l);
                       break_enter ();
          }
          UnsetFakBox (1);
          return 1;
}

int SetAnonym (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (fakbox.mask[2].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       anonym = 0;
          }
          else
          {
                       anonym = 1;
          }
          if (syskey == KEYCR)
          {
                       anonym = 1;
                       SendMessage (cbox.mask[2].feldid, BM_SETCHECK,
                       anonym, 0l);
                       break_enter ();
          }
          UnsetFakBox (2);
          return 1;
}


void SelectFakCtr (void)
/**
Aktiven Button in EnterButton selectieren.
**/
{
        syskey = KEYCR;
        if (fakbox.mask[currentfield].after)
        {
                       (*fakbox.mask[currentfield].after) ();
        }
        syskey = KEYCR;
}



int KompAuf::SetFak_typ (HWND hWnd)
/**
Komplett setzen.
**/
{

         save_fkt (5);
         save_fkt (7);
		 set_fkt (NULL, 7);
         set_fkt (KomplettEnd, 5);
		 int savecurrent;

		 savecurrent = currentfield;
         nix = bar = anonym = 0;
         nix = 1;
         EnableWindows (hWnd, FALSE);
         SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
         SetCaption ("Auswahl f�r Fakturier-Typ");
         if (EnterButtonWindow (hWnd, &fakbox, 14, 9, 9, 42) == -1)
         {
			            SetActiveWindow (hWnd);
                        SetAktivWindow (hWnd);
                        EnableWindows (hWnd, TRUE);
                        restore_fkt (5);
		                currentfield = savecurrent;
                        return -1;
         }

         SetActiveWindow (hWnd);
         SetAktivWindow (hWnd);
         EnableWindows (hWnd, TRUE);
         restore_fkt (5);
         restore_fkt (7);
		 currentfield = savecurrent;
		 if (nix)
		 {
                return 0;
		 }
		 else if (bar)
		 {
                return 1;
		 }
		 else if (anonym)
		 {
                return 2;
		 }
		 return 0;
}


long KompAuf::GenAufNr (short mdn, short fil)
/**
Auftrags-Nummer generieren.
**/
{
	    char buffer [256];
        int dsqlstatus;
        extern  short sql_mode;
        short sqlm;
        int i = 0;


  	    sprintf (buffer, "Eine Auftragsnummer wird generiert");
        Mess.Message (buffer);
		Sleep (20);
		commitwork ();
        sqlm = sql_mode;
        sql_mode = 1;
		beginwork ();
        dsqlstatus = AutoClass.nvholid (mdn, 0, "ls");
		commitwork ();
		if (dsqlstatus == -1)
		{
 			                sprintf (buffer, "Fehler bei nvholid");
                            Mess.Message (buffer);
 		                    Sleep (20);
			                beginwork ();
					        DbClass.sqlin ((short *) &mdn, 1, 0);
					        DbClass.sqlin ((short *) &fil, 1, 0);
							dsqlstatus = DbClass.sqlcomm
								    ("delete from auto_nr where nr_nam = \"ls\" "
								     "and mdn = ? and fil = ?");
							commitwork ();
                            if (dsqlstatus < 0)
							{
                                     disp_mess
										 ("Fehler bei der Generierung der Lieferschein-Nummer",2);
                                     return 0l;
							}
							dsqlstatus = 100;
		}


        i = 0;
        while (dsqlstatus < 0)
        {
			  sprintf (buffer, "Fehler 1 : Zaehler %d Maxwait MAXWAIT", i);
              Mess.Message (buffer);
              if (i == MAXWAIT)
			  {
                        disp_mess ("Fehler bei der Generierung der Lieferschein-Nummer",2);
                        return 0l;
			  }
              Sleep (50);
			  beginwork ();
              dsqlstatus = AutoClass.nvholid (mdn, 0, "ls");
			  commitwork ();
              i ++;
        }

        if (dsqlstatus == 100)
        {
			  beginwork ();
              dsqlstatus = AutoClass.nvanmprf (mdn,
                                     0,
                                     "ls",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");
			  commitwork ();

              i = 0;
              while (dsqlstatus < 0)
              {
 			          sprintf (buffer, "Fehler 2 : Zaehler %d Maxwait MAXWAIT", i);
                      Mess.Message (buffer);
                      if (i == MAXWAIT)
					  {
                        disp_mess ("Fehler bei der Generierung der Lieferschein-Nummer",2);
                        return 0l;
					  }

                      Sleep (50);
					  beginwork ();
                      dsqlstatus = AutoClass.nvanmprf (mdn,
                                                       0,
                                                       "ls",
                                                       (long) 1,
                                                       (long) 999999,
                                                       (long) 10,
                                                        "");
					  commitwork ();
                      i ++;
              }

              if (dsqlstatus == 0)
              {
 					  beginwork ();
                      dsqlstatus = AutoClass.nvholid (mdn,
                                                0,
                                                "ls");
					  commitwork ();
              }
         }

         i = 0;
         while (dsqlstatus < 0)
         {
			  sprintf (buffer, "Fehler 3 : Zaehler %d Maxwait MAXWAIT", i);
              Mess.Message (buffer);
              if (i == MAXWAIT) break;
              Sleep (50);
		      beginwork ();
              dsqlstatus = AutoClass.nvholid (mdn, 0, "ls");
		      commitwork ();
              i ++;
         }

		 beginwork ();
         if (dsqlstatus < 0)
         {
             disp_mess ("Fehler bei der Generierung der Lieferschein-Nummer",2);
             return 0l;
         }
         sql_mode = sqlm;
   	     sprintf (buffer, "Lieferscheinnummer %ld generiert", auto_nr.nr_nr);
         Mess.Message (buffer);
         Sleep (20);
         return auto_nr.nr_nr;
}

long KompAuf::GenAufNr0 (short mdn, short fil)
/**
Auftrags-Nummer generieren und pruefen, ob die Nummer schon existiert.
**/
{
	     static long ls_nr;
		 static short mdn_nr;
		 static short fil_nr;
		 static int tcursor = -1;
		 int dsqlstatus;
		 int i;


		 if (tcursor == -1)
		 {
			      DbClass.sqlin ((short *) &mdn_nr, 1, 0);
			      DbClass.sqlin ((short *) &fil_nr, 1, 0);
			      DbClass.sqlin ((long *)  &ls_nr, 2, 0);
                  tcursor = DbClass.sqlcursor ("select ls from lsk "
					                      "where mdn = ? "
										  "and fil = ? "
										  "and ls = ?");
		 }

		 i = 0;
		 if (lutz_mdn_par)
		 {
			 mdn = fil = 0;
		 }
		 mdn_nr = mdn;
		 fil_nr = fil;
		 ls_nr = GenAufNr (mdn, fil);
		 while (ls_nr > 0l)
		 {
			 DbClass.sqlopen (tcursor);
			 dsqlstatus = DbClass.sqlfetch (tcursor);
			 if (dsqlstatus == 100) return ls_nr;
             Sleep (50);
			 i ++;
             if (i == MAXWAIT) return 0l;
  		     ls_nr = GenAufNr (mdn, fil);
		 }
         return ls_nr;
}




void KompAuf::K_ListeTS (void)
/**
Komissionierliste drucken.
Wenn kun.einz_ausw > 3, Auftraege splitten.
**/
{
	     int i;
		 int dsqlstatus;

		 if (SplitLs)
		 {
			 K_Liste ();
			 return;
		 }

         if (Geteinz_ausw () < 4)
		 {
			 K_Liste ();
			 return;
		 }

//         TeilSmttoAuf (1);
         for (i = 0; i < aufanz; i ++)
		 {
             lsk.mdn = mdntab[i];
             lsk.fil = filtab[i];
             lsk.ls = auftab[i];
             dsqlstatus = ls_class.lese_lsk (mdntab[i], filtab[i], auftab[i]);
			 if (dsqlstatus) continue;
			 K_Liste ();
		 }
}


void KompAuf::DrkLsTS (void)
/**
Komissionierliste drucken.
Wenn kun.einz_ausw > 3, Auftraege splitten.
**/
{
	     int i;
		 int dsqlstatus;

         if (SplitLs)
		 {
			 K_Liste ();
             SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
 	         SetForegroundWindow (hMainWindow);
             UpdateWindow (hMainWindow);
//             AuftoLs();
			 return;
		 }
         if (Geteinz_ausw () < 4)
		 {
			 K_Liste ();
             SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
 	         SetForegroundWindow (hMainWindow);
             UpdateWindow (hMainWindow);
//             AuftoLs();
			 return;
		 }

//         TeilSmttoAuf (1);
         for (i = 0; i < aufanz; i ++)
		 {
             lsk.mdn = mdntab[i];
             lsk.fil = filtab[i];
             lsk.ls = auftab[i];
             dsqlstatus = ls_class.lese_lsk (mdntab[i], filtab[i], auftab[i]);
			 if (dsqlstatus) continue;
             K_Liste ();
             SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
 	         SetForegroundWindow (hMainWindow);
             UpdateWindow (hMainWindow);
//             AuftoLs();
		 }
}

void KompAuf::AuftoLsTS (void)
/**
Komissionierliste drucken.
Wenn kun.einz_ausw > 3, Auftraege splitten.
**/
{
	     int i;
		 int dsqlstatus;

         if (SplitLs)
		 {
//             AuftoLs();
			 return;
		 }
         if (Geteinz_ausw () < 4)
		 {
//             AuftoLs();
			 return;
		 }

//         TeilSmttoAuf (1);
         for (i = 0; i < aufanz; i ++)
		 {
             lsk.mdn = mdntab[i];
             lsk.fil = filtab[i];
             lsk.ls = auftab[i];
             dsqlstatus = ls_class.lese_lsk (mdntab[i], filtab[i], auftab[i]);
			 if (dsqlstatus) continue;
//             AuftoLs();
		 }
}





int KompAuf::FaxAuf (HWND hMainWindow, short mdn, short fil, long auf)
/**
Auftrag faxen
**/
{
		 {
			 K_Liste_Fax ();
			 return 0;

         }

}

void KompAuf::ChoisePrinter0 ()
{
	if (!ll_ls_par && !IsPartyService)
	{
		::ChoisePrinter0 ();
	}
	else
	{
		LLPrinter ();
	}
}


void KompAuf::LLPrinter ()
{
	char buffer [80];
	Text Wahl;

	EnableWindow (AktivDialog, FALSE);
	CPrinterDialog PrinterDialog;
	int ret = PrinterDialog.DoModal ();
	if (ret == IDOK)
	{
		Text PersName = sys_ben.pers_nam;
		PersName.Trim ();
		if (lllsfo <= 1)
		{
			Wahl = "wahl";
		}
		else 
		{
			Wahl.Format ("wahl%d", lllsfo);
		}

		if (PrinterDialog.LsPrint)
		{
			if (PersName.GetLength () > 0)
			{
//				sprintf (buffer, "drllls -nutzer %s -wahl", sys_ben.pers_nam);
				sprintf (buffer, "drllls -nutzer %s -%s", sys_ben.pers_nam, Wahl.GetBuffer ());
			}
			else
			{
//				sprintf (buffer, "drllls -wahl");
				sprintf (buffer, "drllls -%s", Wahl.GetBuffer ());
			}
			CProcess process (buffer);
//			CProcess process ("drllls -wahl");
			process.Start ();
			process.WaitForEnd ();
		}
		if (PrinterDialog.RechPrint)
		{
			if (PersName.GetLength () > 0)
			{
				sprintf (buffer, "dr561 -nutzer %s -wahl 1 - formtyp 56100", sys_ben.pers_nam);
			}
			else
			{
				sprintf (buffer, "dr561 -wahl 1 - formtyp 56100");
			}
			CProcess process (buffer);
//			CProcess process ("dr561 -wahl 1 - formtyp 56100");
			process.Start ();
			process.WaitForEnd ();
		}
	}
	EnableWindow (AktivDialog, TRUE);
}


void *CPrinterDialog::Instance = NULL;

CPrinterDialog::CPrinterDialog ()
{
	Instance = this;
	DlgReturn = FALSE;
	LsPrint = TRUE;
	RechPrint = FALSE;
}

BOOL CPrinterDialog::OnInitDialog (HWND hwndDlg)
{
	if (LsPrint)
	{ 
        HWND hWnd = GetDlgItem (hwndDlg, IDC_LS);
		if (hWnd != NULL)
		{
			SendMessage (hWnd, BM_SETCHECK, BST_CHECKED, 0l);
		}
	}
	else if (LsPrint)
	{ 
        HWND hWnd = GetDlgItem (hwndDlg, IDC_RECH);
		if (hWnd != NULL)
		{
			SendMessage (hWnd, BM_SETCHECK, BST_CHECKED, 0l);
		}
	}
	else
	{ 
        HWND hWnd = GetDlgItem (hwndDlg, IDC_LS);
		if (hWnd != NULL)
		{
			SendMessage (hWnd, BM_SETCHECK, BST_CHECKED, 0l);
		}
	}
	return TRUE;
}

int CPrinterDialog::DoModal ()
{

	DialogBox(hMainInst,
	          MAKEINTRESOURCE (IDD_LLPRINTER),
              NULL,
			  (int (__stdcall *) ()) DlgProcP);
	return DlgReturn;
}


BOOL CALLBACK CPrinterDialog::DlgProcP (HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{ 
	HWND hWnd;

    switch (message) 
    { 
        case WM_NOTIFY: 
			break;
        case WM_INITDIALOG:
			if (Instance != NULL)
			{
				((CPrinterDialog *) Instance)->OnInitDialog (hwndDlg);
			}
			break;
        case WM_COMMAND: 
            switch (LOWORD(wParam)) 
			{
			case IDOK :
                EndDialog(hwndDlg, wParam); 
				((CPrinterDialog *) Instance)->DlgReturn = IDOK;
				hWnd = GetDlgItem (hwndDlg, IDC_LS);
				if (hWnd != NULL)
				{
					int ret = SendMessage (hWnd, BM_GETCHECK, 0, 0l);
					if (ret == BST_CHECKED)
					{
						((CPrinterDialog *) Instance)->LsPrint = TRUE;
					}
					else
					{
						((CPrinterDialog *) Instance)->LsPrint = FALSE;
					}
				}
 
				hWnd = GetDlgItem (hwndDlg, IDC_RECH);
				if (hWnd != NULL)
				{
					int ret = SendMessage (hWnd, BM_GETCHECK, 0, 0l);
					if (ret == BST_CHECKED)
					{
						((CPrinterDialog *) Instance)->RechPrint = TRUE;
					}
					else
					{
						((CPrinterDialog *) Instance)->RechPrint = FALSE;
					}
				}
                return TRUE; 
			case IDCANCEL :
                EndDialog(hwndDlg, wParam); 
				((CPrinterDialog *) Instance)->DlgReturn = IDCANCEL;
                return TRUE; 
			}
			break;
        case WM_SYSCOMMAND: 
            switch (wParam)
			{
			case SC_CLOSE:
                EndDialog(hwndDlg, wParam); 
                return TRUE; 
			}
			break;
	}
	return FALSE;
}

