//010805 Lud auf_me
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "ls.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "ptab.h"
#include "mo_aufl.h"
#include "mo_qa.h"
#include "mdn.h"
#include "fil.h"
#include "kun.h"
#include "a_kun.h"
#include "aktion.h"
#include "akt_krz.h"
#include "a_pr.h"
#include "mo_a_pr.h"
#include "mo_preis.h"
#include "sys_par.h"
#include "mo_einh.h"
#include "kumebest.h"
#include "mo_atxtl.h"
#include "mo_nr.h"
#include "best_bdf.h"
#include "best_res.h"
#include "stnd_auf.h"
#include "mo_stda.h"
#include "mo_menu.h"
#include "bsd_buch.h"
#include "tou.h"
#include "mo_progcfg.h"
#include "mo_smtg.h"
#include "aufkun.h"
#include "prov_satz.h"
#include "mo_choise.h"
#include "enterfunc.h"
#include "mo_kompl.h"
#include "lspt.h"
#include "searchcharge.h"
#include "searcha.h"
#include "datum.h"
#include "Emb.h"
#include "lspt.h"
#include "lsp_txt.h"
#include "sounds.h"
#include "CMessage.h"
#include "Integer.h"
#include "mo_leergut.h"

#define MAXLEN 40
#define MAXPOS 5000
#define LPLUS 1

#define MAXME 99999.99
#define MAXPR 9999.99

#define HNDW 1
#define EIG 2
#define EIG_DIV 3
#define LEIHARTIKEL 12
#define LEERGUT 11			//270812

#define DIENSTLEISTUNG 13

#define BonOK 2
#define ArtikelGesperrt 1

#define LIEFERDATUM 1



static int lastpruef ;	// 310812


class CScanValues 
{
public:
	double a;
	double eangew;
	double gew;
	Text Charge;
	Text Mhd;
	double auf_me;
	Text ACharge;
	BOOL aflag;
	BOOL gewflag;
	BOOL ChargeFlag;
	BOOL MhdFlag;
	CScanValues ()
	{
		a = 0.0;
		eangew = 0.0;
		gew = 0.0;
        Charge = "";
		Mhd = "";
		auf_me = 0;
		ACharge = "";
		aflag = FALSE;
		gewflag = FALSE;
		ChargeFlag = FALSE;
		MhdFlag = FALSE;
	}

	void Init ()
	{
		eangew = 0.0;
		a = 0.0;
        Charge = "";
		Mhd = "";
		aflag = FALSE;
		gewflag = FALSE;
		ChargeFlag = FALSE;
		MhdFlag = FALSE;
	}

	void SetA (double a)
	{
		this->a = a;
		aflag = TRUE;
	}

	void SetGew (double gew)
	{
		this->eangew = gew;
		this->gew += gew;
		gewflag = TRUE;
	}

	void SetCharge (Text& Charge)
	{
		this->Charge = Charge;
		ChargeFlag = TRUE;
	}

	void SetMhd (Text& Mhd)
	{
		this->Mhd = Mhd;
		MhdFlag = TRUE;
	}

	void IncAufMe ()
	{
		auf_me ++;
	}

	BOOL SetACharge ()
	{
		if (ACharge != "")
		{
			if (ACharge != Charge) return FALSE;
// Sp�ter eventuell eine neue Position erzeugen
			ACharge = Charge;
			return TRUE;
		}
        ACharge = Charge;
		return TRUE;
	}

	BOOL ScanOK ()
	{
		if (!aflag) return FALSE;
		if (!gewflag) return FALSE;
		if (!ChargeFlag) return FALSE;
		return TRUE;
	}

	BOOL ScanValuesOK ()
	{
		if (a == 0.0) return FALSE;
		if (gew == 0.0) return FALSE;
		if (Charge == "") return FALSE;
		return TRUE;
	}
};

CScanValues *ScanValues = NULL;

void EnableFullTax ();
void SetLsToFullTax ();
void SetLsToStandardTax ();
void SetToFullTax ();
void SetToStandardTax ();
void DisableFullTax ();
BOOL isPartyDienstleistung (short , short);
int ScanEan128 (Text& Ean128);

extern HANDLE  hMainInst;
extern HWND hWndMain;

extern char LONGNULL[4];

static HWND hMainWin;
static unsigned char ausgabebuffer [0x1000];
static HWND mamain1;
static HWND eWindow;
static HWND AufMehWnd = NULL;
static HWND AufMehWnd0 = NULL;
static HWND AufGewhWnd = NULL;
static HWND AufGewhWnd0 = NULL;
static HWND BasishWnd;
static PAINTSTRUCT aktpaint;

static COLORREF MessBkCol = DKYELLOWCOL;
static COLORREF MessCol   = BLACKCOL;

static ITEM iOK ("", "     OK     ", "", 0);
static ITEM iCA ("", "  Abbrechen ", "", 0);
static ITEM ishow ("","..", "", 0);

static int ListFocus = 3;
static int cfg_LeergutErfassen = 0;

static long akt_lager;

static int rab_prov_kz = 0;
static int auf_wert_anz = 0;
static int auf_gew_anz = 0;
static int a_kun_smt = 0;
static double ls_vk_pr;
static double ls_lad_pr;

static int preistest;
static int ladpreistest;
static double prproz_diff = 50.0;
static int  art_un_tst = 0;
static BOOL  add_me = FALSE;
static BOOL a_kum_par = 0;
static BOOL sacreate = NULL;
static HWND SaWindow = NULL;
static HWND PlusWindow = NULL;
static BOOL add;
static double aufme_old;
static BOOL testmeOK = FALSE;;
static BOOL meoptimize = TRUE;
static BOOL vertr_abr_par = TRUE;
static int lutz_mdn_par = 0;
static int wa_pos_txt;
static BOOL RemoveKondArt = TRUE;
static BOOL RemoveAGrund  = TRUE;
static BOOL RemoveAGrund0 = TRUE;
static BOOL RemoveEnterCharge = FALSE;
static BOOL RemoveEnterMHD = TRUE;
static int ls_charge_par = 0;
static int ls_mhd_par = 0;
static int lsc2_par = 0;
static int boes_par = 0;
static int lsc3_par = 0;
static BOOL NoRecNr = FALSE;
static BOOL ld_pr_prim = TRUE;
static BOOL autopfand = TRUE;
static int PosSave = 0;
static BOOL PosSaveMess = FALSE;
static int InsCount = 0;
static BOOL LiefMeDirect = TRUE;
static int a_ersatz = 0;
static BOOL AufCharge = FALSE;
static BOOL EnableChargeChange = TRUE;
static int MeDefault = 1;
static BOOL MeZwang = FALSE;
static BOOL EanMeZwang = FALSE;
static BOOL LeergutAnnahme = FALSE;
static double LeergutVon = 90000;
static double LeergutBis = 99999;
static int nachkpreis = 2;
static int VkAttribut = EDIT;
static BOOL TestBranSmt = FALSE;
static BOOL akt_preis = TRUE;
static BOOL FilStandard = FALSE;
static long scangew = 0l;
static long scanpr = 0l;
static int MaxScanDays = 5;
static double emb_anz;
static BOOL NewPosition = TRUE;
static short ListeSortierung = 0;
static BOOL ScannMessage = TRUE;
static BOOL LiefMe0 = FALSE;
static int CalcMhd = 0;
static int HbkPlus = 0; 
static BOOL TestChargeMe = FALSE; 

struct LSPS
{
       char posi [80];
       char sa_kz_sint [80];
       char a [80];
       char a_kun [20];
       char a_bz1 [80];
       char a_bz2 [80];
	   char last_me[80];
	   char last_pr_vk[80];
       char auf_me [80];
       char lief_me [80];
       char me_bz [80];
       char ls_vk_pr [80];
       char ls_lad_pr [80];
       char basis_me_bz [80];
       char teil_smt [5];
       char me_einh_kun [5];
       char me_einh [5];
       char lsp_txt [9];
       char me_einh_kun1 [5];
       char auf_me1 [14];
       char inh1 [14];
       char me_einh_kun2 [5];
       char auf_me2 [14];
       char inh2 [14];
       char me_einh_kun3 [5];
       char auf_me3 [14];
       char inh3 [14];
	   char rab_satz [10];
	   char prov_satz [10];
	   char gruppe [9];
       char ls_vk_dm [80];
       char ls_lad_dm [80];
       char ls_vk_euro [80];
       char ls_lad_euro [80];
       char ls_vk_fremd [80];
       char ls_lad_fremd [80];
       char kond_art0 [20];
       char kond_art [20];
       char a_grund  [20];
       char posi_ext  [9];
	   char last_ldat [12];
       short ls_pos_kz;
       char  a_ers [16];
       short dr_folge;
       char  ls_charge [35];
       double charge_gew;
	   double emb_anz;
	   double emb_lief_me;
	   short pos_stat;
	   short me_einh_ist;
	   double inh_ist;
	   double me_ist;
	   char lief_me_bz_ist [10];
	   long hbk_date;
	   char mhd [12];
	   char	ls_ident [21];
	   char aufschlag [5];
	   char aufschlag_wert[15];
	   char nve_posi[15];
	   short a_typ;
	   double a_leih;
	   short pos_txt_kz;
	   double tara;
	   double auf_me_vgl;
	   double lief_me1;
	   double lief_me2;
	   double lief_me3;
	   char pos_sum [20];
	   short mwst;
	   char erf_kz [2];
};

struct LSPS lsps, lsptab [MAXPOS], lsps_null;
char RowCharge [sizeof (lsp.ls_charge)] = {""};

short wiedereinstieg = 1;

ITEM iposi        ("posi", lsps.posi,             "", 0);
ITEM isa_kz_sint  ("sa_kz_sint", lsps.sa_kz_sint, "", 0);
ITEM ia           ("a",          lsps.a,          "", 0);
ITEM ia_kun       ("a_kun",      lsps.a_kun,      "", 0);
ITEM ia_bz1       ("a_bz1",      lsps.a_bz1,      "", 0);
ITEM ia_bz2       ("a_bz2",      lsps.a_bz2,      "", 0);
ITEM ilast_me     ("last_me",    lsps.last_me,    "", 0);
ITEM ildat        ("lieferdat",  lsps.last_ldat,  "", 0);
ITEM iauf_me      ("auf_me",     lsps.auf_me,     "", 0);
ITEM ilief_me      ("lief_me",   lsps.lief_me,    "", 0);
ITEM ime_bz       ("auf_me_bz",  lsps.me_bz,      "", 0);
ITEM ipr_vk       ("pr_vk",      lsps.ls_vk_pr,   "", 0);
ITEM ilpr_vk      ("lpr_vk",     lsps.last_pr_vk,  "", 0);
ITEM ilad_pr      ("ld_pr",      lsps.ls_lad_pr,  "", 0);
ITEM ibasis_me_bz ("basis_me_bz",lsps.basis_me_bz,"", 0);
ITEM ikond_art    ("kond_art",   lsps.kond_art,   "", 0);
ITEM ia_grund     ("a_grund",    lsps.a_grund,    "", 0);
ITEM ia_grund0    ("a_grund0",   lsps.a_grund,    "", 0);
ITEM ils_charge   ("ls_charge",  lsps.ls_charge,    "", 0);
ITEM i_mhd        ("mhd",        lsps.mhd,    "", 0);
ITEM ipos_sum      ("pos_sum",   lsps.pos_sum,    "", 0);

static field  _dataform[30] = {
&iposi,        5, 0, 0, 6, 0, "%4d",     DISPLAYONLY, 0, 0, 0,
&isa_kz_sint,  2, 0, 0, 12, 0, "%1d",    DISPLAYONLY, 0, 0, 0,
&ia,          14, 0, 0, 15, 0, "",       DISPLAYONLY, 0, 0, 0,
&ia_kun,      15, 0, 1, 15, 0, "",       REMOVED, 0, 0, 0,
&ia_bz1,      25, 0, 0, 30, 0, "",       DISPLAYONLY, 0, 0, 0,
&ilast_me,    11, 0, 0, 57, 0, "%8.3f",  DISPLAYONLY, 0, 0, 0, 
&iauf_me,     11, 0, 0, 70, 0, "%8.3f",  DISPLAYONLY,        0, 0, 0, 
&ime_bz ,     11, 0, 0, 83, 0, "",       DISPLAYONLY, 0, 0, 0,
&ilief_me,    11, 0, 0, 96, 0, "%8.3f",  EDIT,        0, 0, 0, 
&ibasis_me_bz,11, 0, 0,109, 0, "",       DISPLAYONLY, 0, 0, 0,
&ipr_vk,       9, 0, 0,125, 0, "%6.2f",  EDIT,        0, 0, 0, 
&ilad_pr,      9, 0, 0,137, 0, "%6.2f",  DISPLAYONLY, 0, 0, 0, 
&ikond_art,    6, 0, 0,149, 0, "",       DISPLAYONLY, 0, 0, 0,
&ia_grund,     6, 0, 0,160, 0, "%4d",    DISPLAYONLY, 0, 0, 0,
&ils_charge,  31, 0, 0,168, 0, "",       EDIT, 0, 0, 0,
&i_mhd,       11, 0, 0,201, 0, "dd.mm.yyyy",       EDIT, 0, 0, 0,
&ipos_sum,     9, 0, 0,214, 0, "%8.2f",  DISPLAYONLY, 0, 0, 0,
&ia_bz2,      25, 0, 1, 30, 0, "",       DISPLAYONLY, 0, 0, 0,
&ia_grund0,    6, 0, 1, 57, 0, "%4d",    DISPLAYONLY, 0, 0, 0,
};

static form dataform = {19, 0, 0, _dataform, 0, 0, 0, 0, NULL};

static field _fldat = {&ildat, 13, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};
static field _flpr_vk = {&ilpr_vk, 9, 0, 0, 0, 0, "%6.2f", DISPLAYONLY, 0, 0, 0};


static BOOL DelLadVK  = TRUE;
static BOOL DelPrVK  = FALSE;
static BOOL DelLastMe = TRUE;
static BOOL AddLastLief = FALSE;
static BOOL LastFromAufKun = FALSE;
static BOOL AddLastVkPr = FALSE;
static BOOL DelAufMe = FALSE;

static BOOL FormOK = FALSE;


void addField (form * frm, field * feld, int pos, int len, int space)
{
	int i;
	int plus;

	frm->fieldanz ++;
	plus = len + space;

	feld->pos[1] = frm->mask[pos].pos[1];
	for (i = frm->fieldanz - 1; i > pos; i --)
	{
		memcpy (&frm->mask[i], &frm->mask[i - 1], sizeof (field));
		frm->mask[i].pos[1] += plus;
	}
	memcpy (&frm->mask[i], feld, sizeof (field));
}


int addFieldName (form * frm, field * feld, char *name, int space)
{
	int pos;
	int len;

	len = feld->length;
	pos = GetItemPos (frm, name);
	if (pos == -1)
	{
		return pos;
	}
	addField (frm, feld, pos, len, space);
	return pos;
}


void DelFormField (form *frm, int pos)
/**
Feld aus Dataform herausnehmen.
**/
{
	         int i;
			 int diff;
			 int pos1, pos2;

			 pos1 = frm->mask[pos].pos[1];
			 if (pos < frm->fieldanz - 1)
			 {
				 pos2 = frm->mask[pos + 1].pos[1];
				 diff = max (0, pos2 - pos1);
			 }
			 else
			 {
				 diff = frm->mask[pos].length;
				 if (frm != &dataform) diff ++;
			 }
			 if (diff == 0)
			 {
				 diff = frm->mask[pos].length;
				 if (frm != &dataform) diff ++;
			 }

			 frm->fieldanz --;

			 for (i = pos; i < frm->fieldanz; i ++)
			 {
				 memcpy ((char *) &frm->mask[i], (char *) &frm->mask[i + 1], sizeof (field));
			 }

			 for (i = 0; i < frm->fieldanz; i ++)
			 {
				 if (frm->mask[i].pos[1] > pos1)
				 {
					 frm->mask[i].pos[1] -= diff;
				 }
			 }
}


static int ubrows [] = {0,
                        1,
                        2,2,
                        3,4,5,6,7,8,9,10, 11, 12, 13};

/*
struct CHATTR ChAttr [] = {"a",     DISPLAYONLY, EDIT,
                            NULL,  0,           0};
*/


struct CHATTR ChAttra [] = {"a",     DISPLAYONLY, EDIT,
                             NULL,  0,           0};
struct CHATTR ChAttra_kun [] = {"a_kun", DISPLAYONLY, EDIT,
                                 NULL,   0,           0};

struct CHATTR *ChAttr = ChAttra;

ColButton Cuposi = {  "Pos.", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cusa_kz_sint = {
                     "S", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cua = {
                     "Artikel-Nr", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cua_bz1 = {
                     "Bezeichnung1", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cua_bz2 = {
                     "Bezeichnung2", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Culief_me = {
                     "Menge", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cume_bz = {
                     "Best.ME", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cupr_vk = {
                     "VK", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cubasis_me_bz = {
                     "Einheit", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};


static char *VK        = "  VK    ";
static char *VK_DM     = " VK DM  ";
static char *VK_EURO   = " VK EURO ";
static char *VK_FREMD  = " VK FREMD ";

static char *LD        = " Ld.VK  ";
static char *LD_DM     = " LD DM  ";
static char *LD_EURO   = " LD EURO ";
static char *LD_FREMD  = "LD FREMD";


ITEM iuposi        ("posi",       "Pos.",            "", 0);
ITEM iusa_kz_sint  ("sa_kz_sint", "S",               "", 0);
ITEM iua           ("a",          "Artikel-Nr   ",   "", 0);
ITEM iua_bz1       ("a_bz1",      "Bezeichnung 1",   "", 0);
ITEM iua_bz2       ("a_bz2",      "Bezeichnung 2",   "", 0);
ITEM iulast_me     ("last_me",    "Letz.Bst",        "", 0);
ITEM iuldat        ("ldat",       "Letz.LD.",        "", 0);
ITEM iulpr_vk      ("lpr_vk",     "Letz.VK",         "", 0);
ITEM iuauf_me      ("auf_me",     "A.Menge",         "", 0);
ITEM iulief_me      ("lief_me",   "L.Menge",         "", 0);
ITEM iume_bz       ("auf_me_bz",      "Best.ME",         "",  0);
ITEM iupr_vk       ("pr_vk",       VK,               "", 0);
ITEM iulad_pr      ("ld_pr",       LD,               "", 0);
ITEM iubasis_me_bz ("basis_me_bz","Einheit",         "",  0);
ITEM iukond_art    ("kond_art",   "Kond.Art",        "",  0);
ITEM iua_grund     ("a_grund",    "Gr.Art",          "",  0);
ITEM iuls_charge   ("ls_charge",  "Chargen-Nr",      "",  0);
ITEM iu_mhd        ("mhd",        "MHD",            "",  0);
ITEM iupos_sum     ("pos_sum",    "Pos.Summe",       "",  0);
ITEM iufiller      ("",                "",           "", 0);


static field  _ubform[30] = {
&iuposi,        5, 0, 0,  6, 0, "",  BUTTON, 0, 0, 0,
&iusa_kz_sint,  3, 0, 0, 11, 0, "",  BUTTON, 0, 0, 0,
&iua,          15, 0, 0, 14, 0, "",  BUTTON, 0, 0, 0,
&iua_bz1,      27, 0, 0, 29, 0, "",  BUTTON, 0, 0, 0,
&iulast_me,    13, 0, 0, 56, 0, "",  BUTTON, 0, 0, 0, 
&iuauf_me,     13, 0, 0, 69, 0, "",  BUTTON, 0, 0, 0, 
&iume_bz,      13, 0, 0, 82, 0, "",  BUTTON, 0, 0, 0,
&iulief_me,    13, 0, 0, 95, 0, "",  BUTTON, 0, 0, 0, 
&iubasis_me_bz,15, 0, 0,108, 0, "",  BUTTON, 0, 0, 0,
&iupr_vk,      12, 0, 0,123,0,  "",  BUTTON, 0, 0, 0, 
&iulad_pr,     12, 0, 0,135,0,  "",  BUTTON, 0, 0, 0, 
&iukond_art,   12, 0, 0,147, 0, "",  BUTTON, 0, 0, 0,
&iua_grund,     8, 0, 0,159, 0, "",  BUTTON, 0, 0, 0,
&iuls_charge,  33, 0, 0,167, 0, "",  BUTTON, 0, 0, 0,
&iu_mhd,       12, 0, 0,200, 0, "",  BUTTON, 0, 0, 0,
&iupos_sum,    11, 0, 0,212, 0, "",  BUTTON, 0, 0, 0,
&iufiller,    100, 0, 0,223, 0, "",  BUTTON, 0, 0, 0,
};


static form ubform = {17, 0, 0, _ubform, 0, 0, 0, 0, NULL};

ITEM iline ("", "1", "", 0);


static field  _lineform[30] = {
&iline,      1, 0, 0, 11, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 14, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 29, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 56, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 69, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 82, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 95, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,108, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,123, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,135, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,147, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,159, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,167, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,200, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,212, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,223, 0, "",  NORMAL, 0, 0, 0,
};

static form lineform = {16, 0, 0, _lineform, 0, 0, 0, 0, NULL};

static field _fuldat = {&iuldat,   14, 0, 0, 0, 0, "", BUTTON, 0, 0, 0};
static field _fline  = {&iline,     1, 0, 0, 0, 0, "", NORMAL, 0, 0, 0};
static field _fulpr_vk = {&iulpr_vk,   11, 0, 0, 0, 0, "", BUTTON, 0, 0, 0};

void addLastLdat (char *name)
{
	int pos;
	static BOOL LastLdatOk = FALSE;

	if (LastLdatOk)
	{
		return;
	}

    LastLdatOk = TRUE;
	pos = addFieldName (&ubform,   &_fuldat, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fuldat.length, 0);
	     addFieldName (&dataform, &_fldat,  name, 2);
	}
}


void addLastVkPr (char *name)
{
	int pos;
	static BOOL LastVkPrOk = FALSE;

	if (LastVkPrOk)
	{
		return;
	}

    LastVkPrOk = TRUE;
	pos = addFieldName (&ubform,  &_fulpr_vk, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fulpr_vk.length, 0);
	     addFieldName (&dataform, &_flpr_vk,  name, 2);
	}
}


static ListClassDB eListe;
PTAB_CLASS ptab_class;
WA_PREISE WaPreis;
HNDW_CLASS HndwClass;
SYS_PAR_CLASS sys_par_class;
static LeergClass LeergClass;
static QueryClass QClass;
static LS_CLASS ls_class;
static DB_CLASS DbClass;
static EINH_CLASS einh_class;
static AUFPTLIST TListe;
static AutoNrClass AutoNr;
static BSD_BUCH_CLASS BsdBuch;
static AUFKUN_CLASS AufKun;
static LSP_TCLASS lsp_tclass;
static LSPT_CLASS lspt_class;

static BEST_BDF_CLASS best_bdf_class;
static BEST_RES_CLASS best_res_class;
static PROG_CFG ProgCfg ("53100");
static SMTG_CLASS Smtg;
static PROV Prov;
static SCANEAN Scanean ((Text) "scanean.cfg");
static Emb emb;

static int plu_size = 4;
static int lief_me_default= 0;
static long aufkunanz = 5;
static int SmtTest = 0;
static BOOL cfg_TestLiefMe0 = TRUE;

static StndAuf StndAuf;
static BOOL searchadirect = TRUE;
static BOOL searchmodedirect = TRUE;
static double RowHeight = 1.5;
static int UbHeight = 0;
static int bsd_kz = 1;
static int matchcode = 0;

static BOOL NoArtMess = FALSE;

static BOOL ListColors = TRUE;
static BOOL ber_komplett = TRUE;
static COLORREF KompfColor = BLACKCOL;
static COLORREF KompbColor = GRAYCOL;
static COLORREF SafColor   = WHITECOL;
static COLORREF SabColor   = BLACKCOL;


static char KunItem[] = {"kuna"};
static char FilItem[] = {"fila"};
static double Akta;
static BOOL lief_me_pr_0 = TRUE;
static int preis0_mess = 1;
static double inh = 0.0;
static long dauertief = 90l;

static double akt_me;
static double akt_lief_me;
static int akt_me_einh;


void DelFormFieldEx (form *frm, int pos, int diff)
/**
Feld aus Dataform herausnehmen.
**/
{
	         int i;
			 int pos1;

			 pos1 = frm->mask[pos].pos[1];
			 frm->fieldanz --;

			 for (i = pos; i < frm->fieldanz; i ++)
			 {
				 memcpy ((char *) &frm->mask[i], (char *) &frm->mask[i + 1], sizeof (field));
			 }

			 for (i = 0; i < frm->fieldanz; i ++)
			 {
				 if (frm->mask[i].pos[1] > pos1)
				 {
					 frm->mask[i].pos[1] -= diff;
				 }
			 }
}

/*
void DelFrmDB (int pos)
{
	         int i;

			 for (i = 0; dbipr[i].frm; i ++)
			 {
				 if (dbipr[i].frmpos == pos) break;
			 }
			 if (dbipr[i].frm)
			 {
			     for (; dbipr[i].frm; i ++)
				 {
				            memcpy (&dbipr[i], &dbipr[i + 1], sizeof (FRMDB));
				 }
			 }
			 for (i = 0; dbipr[i].frm; i ++)
			 {
				 if (dbipr[i].frmpos > pos) dbipr[i].frmpos --;
			 }
}
*/

void DelListField (char *Item)
{
	         int pos, diff;

		     diff = 0;
		     pos = GetItemPos (&ubform, Item);
			 if (pos > -1 && pos < ubform.fieldanz - 1)
			 {
					 diff = ubform.mask[pos + 1].pos[1] -
						    ubform.mask[pos].pos[1];
			 }
	         if (pos > -1) DelFormFieldEx (&ubform, pos, diff);
	         if (pos > -1) DelFormFieldEx (&lineform, pos - 1, diff);
		     pos = GetItemPos (&dataform, Item);
	         if (pos > -1) DelFormFieldEx (&dataform, pos, diff);
//			 DelFrmDB (pos);
}


static TEXTMETRIC textm;

static int EnterBreak ()
{
	 break_enter ();
	 return (0);
}

static int EnterTest (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		}
        return 0;
}


void SetUbHeight (void)
/**
Hoehe der Listueberschrift setzen.
**/
{
	int i;

	for (i = 0; i < ubform.fieldanz; i ++)
	{
		ubform.mask[i].rows = UbHeight;
	}
}

static int InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System
**/
{

    clipped (*Item);
//    if (strcmp (*Item, "a") == 0)
    {
		if (lsk.kun_fil == 0)
		{
			*Item = KunItem;
			sprintf (where, "where kun = %ld and a = %.0lf",
                         lsk.kun, ratod (Value));
		}
		else
		{
			*Item = FilItem;
			sprintf (where, "where mdn = %hd and fil = %ld and a = %.0lf",
                         lsk.mdn, lsk.kun, ratod (Value));
		}
        return 1;
    }
    return 0;
}

//short AUFPLIST::auf_art = 0;
short AUFPLIST::auf_art = 1;
BOOL AUFPLIST::Muster = FALSE;
BOOL AUFPLIST::AErsInLs = TRUE;
int AUFPLIST::SortFlag = NoSort;

CDllPreise AUFPLIST::DllPreise;
CLeerGut AUFPLIST::LeerGut;
BOOL AUFPLIST::TxtByDefault = FALSE;
BOOL AUFPLIST::PosDeleteWarning = TRUE;
int AUFPLIST::PosTxtKz = CControlPlugins::LiefVisible;
CControlPlugins AUFPLIST::pControls;
CWaWiePro AUFPLIST::WaWiePro;
BOOL AUFPLIST::ChargeZwang;
CSmtHandler *AUFPLIST::smtHandler = NULL;

void AUFPLIST::SetMessColors (COLORREF color, COLORREF bkcolor)
/**
Farben fuer Melungen setzen.
**/
{
	 MessCol   = color;
	 MessBkCol = bkcolor;
}

static long mdnprod = 0;

AUFPLIST *AUFPLIST::Instance = NULL;

void AUFPLIST::SetCfgProgName (LPSTR ProgName)
{
    ProgCfg.SetProgName (ProgName);
}

void AUFPLIST::SetMdnProd (int mdnp)
{
	mdnprod = mdnp;
}

void AUFPLIST::SethMainWindow (HWND hMainWindow)
{
    this->hMainWindow = hMainWindow;
	hMainWin = hMainWindow;
}

AUFPLIST:: AUFPLIST ()
{
	Instance = this;
    mamainmax = 0;
    mamainmin = 0;
    eingabesatz = NULL;
    ausgabesatz = ausgabebuffer;
    zlen = 0;
    feld_anz = 0;
    banz = 0;
    PageView = 0;
    LstZiel = NULL;
    LstSatz   = NULL;
    Lstbanz = 0;
    SwRecs = 0;
    AktRow = 0;
    AktColumn = 0;
    scrollpos = 0;
    InfoCaption[0] = (char) 0;
    WithMenue   = 1;
    WithToolBar = 1;
	preistest = 0;
	ladpreistest = 0;
	add = FALSE;
    lutz_mdn_par = 0;
	liney = 0;
	aufme_old = (double) 0.0;
    dataform.after  = WriteRow;
    dataform.before = SetRowItem;
    dataform.mask[2].before = Savea;
    dataform.mask[2].after  = fetcha;
    dataform.mask[3].after  = fetcha_kun;
    dataform.mask[8].before = setkey9me;
    dataform.mask[8].after  = testme;
    dataform.mask[10].before  = setkey9basis;
    dataform.mask[10].after  = testprx;			// 310812
	dataform.mask[11].before  = setkey9basisl;	// 2700812
    dataform.mask[11].after  = testprx;	// 310812 / 270812

    dataform.mask[15].after  = testmhd;
//	dataform.mask[14].after = TestLsCharge;

    ListAktiv = 0;
    inh = (double) 0.0;
    this->hMainWindow = NULL;
	hMainWin = NULL;
    Muster = FALSE;
	EditAufMeEnabled = FALSE;

// Bei QuickSearch = TRUE keine verschachtelten Aktionen

    WA_PREISE::QuickSearch = FALSE;
	NoteIcon = NULL;
	ScanMode = FALSE;
	ScanMulti = FALSE;
	ScannMeZwang = 0;
	Ean128Eanlen = 12;
	ScannedCharge = "";
	ChargeZwang = TRUE;
	WithZerlData = FALSE;
    ChargeFromEan128 = 0;
    Ean128StartLen = 10;
	EnterAufMe = FALSE;

}

void AUFPLIST::SetLager (long lgr)
{
	akt_lager = lgr;
}

int AUFPLIST::ShowBasis (void)
/**
Basismenge der aktuellen Bestellmenge anzeigen.
**/
{
	HDC hdc;
	double lief_me;
	double a;
	short me_einh_kun;
	double lief_me_vgl;
    KEINHEIT keinheit;
	HWND hWnd;
	HWND hMainWindow;

	static char basis_me[15];
	static char basis_einh[11];


    static mfont anzfont    = {NULL, -1, -1, 1,
                               BLACKCOL,
                               WHITECOL,
                               0};

	static ITEM ibasis_me    ("basis_me", basis_me, "Menge in Basiseinheit :", 0);
	static ITEM ibasis_einh  ("me_einh",  basis_einh, "", 0);
	static ITEM iOK          ("OK",       "OK", "", 0);

	static field _fbasis_me[] = {
		&ibasis_me,      9, 0, 1, 2, 0, "%8.2f", READONLY, 0, 0, 0,
		&ibasis_einh,    8, 0, 1,36, 0, "",      DISPLAYONLY, 0, 0, 0,
		&iOK,           10, 0, 3,17, 0, "",      BUTTON,      0, StopEnter, KEY5,
	};

	static form fbasis_me = {3, 0, 0, _fbasis_me, 0, 0, 0, 0, NULL};

    return 0;
	anzfont.FontName = "                   ";
    GetStdFont (&anzfont);
//	anzfont.FontName = "ARIAL";
    anzfont.FontAttribute = 1;

    lief_me = ratod (lsps.lief_me);
    a  = ratod (lsps.a);
    me_einh_kun = atoi (lsps.me_einh_kun);

    strcpy (kumebest.kun_bran2, kun.kun_bran2);
    einh_class.AktAufEinh (lsk.mdn, lsk.fil,
                                lsk.kun, a, me_einh_kun);
    strcpy (kumebest.kun_bran2, kun.kun_bran2);
    einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                              lsk.kun, a, &keinheit);

    if (keinheit.me_einh_kun == keinheit.me_einh_bas)
    {
            lief_me_vgl = lief_me;
    }
    else
    {
            if (keinheit.inh <= (double) 0.0)
            {
                     keinheit.inh = (double) 1.0;
            }
            lief_me_vgl = lief_me * keinheit.inh;
    }

	sprintf (basis_me, "%8.2lf", lief_me_vgl);
	strcpy  (basis_einh, keinheit.me_einh_bas_bez);

    break_end ();
	SetButtonTab (TRUE);
	hMainWindow = GetActiveWindow ();
    DisablehWnd (hMainWindow);
    SetBorder (WS_POPUP | WS_VISIBLE| WS_DLGFRAME);
	SethStdWindow ("hListWindow");
    hWnd = OpenWindowChC (5, 46, 10, 14, hMainInst, "");
	SethStdWindow ("hStdWindow");
//	hWnd = OpenColWindow (5, 46, 10, 14, WHITECOL, NULL);
	BasishWnd = hWnd;
    hdc = GetDC (hWnd);
    ChoiseLines (hWnd, hdc);
    ReleaseDC (hWnd, hdc);

    SetStaticWhite (TRUE);
    enter_form (hWnd, &fbasis_me, 0, 0);
    SetStaticWhite (FALSE);

	CloseControls (&fbasis_me);
    DestroyWindow (hWnd);
	BasishWnd = NULL;
	AktivWindow = hMainWindow;
	SetButtonTab (FALSE);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
	return 0;
}

void AUFPLIST::SetChAttr (int ca)
{
    switch (ca)
    {
          case 0 :
              ChAttr = ChAttra;
              break;
          case 1:
              ChAttr = ChAttra_kun;
              break;
    }
}

void AUFPLIST::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return;

         dataform.mask[i].attribut = attr;
}

int AUFPLIST::GetFieldAttr (char *fname)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return -1;

         return (dataform.mask[i].attribut);
}

int AUFPLIST::Getib (void)
{
    static BOOL ParOK = 0;
    static int IB;

    if (ParOK) return IB;

    ParOK = 1;
    IB = 0;
    strcpy (sys_par.sys_par_nam,"ib");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt))
        {
                  IB = 1;
        }
    }
    return IB;
}

void AUFPLIST::Geta_kum_par (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    a_kum_par = FALSE;
    strcpy (sys_par.sys_par_nam,"a_kum_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt))
        {
                  a_kum_par = TRUE;
        }
    }
    strcpy (sys_par.sys_par_nam, "ls_mhd_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        ls_mhd_par = atoi (sys_par.sys_par_wrt);
    }


    strcpy (sys_par.sys_par_nam, "ls_charge_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        ls_charge_par = atoi (sys_par.sys_par_wrt);
    }
    strcpy (sys_par.sys_par_nam, "lsc2_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        lsc2_par = atoi (sys_par.sys_par_wrt);
    }
    strcpy (sys_par.sys_par_nam, "lsc3_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        lsc3_par = atoi (sys_par.sys_par_wrt);
    }
    return;
}


void AUFPLIST::Geta_bz2_par (void)
{
    static BOOL ParOK = 0;

	if (EnterAufMe)
	{
		SetFieldAttr ("auf_me", EDIT);
	}
	else
	{
		SetFieldAttr ("auf_me", DISPLAYONLY);
	}

    if (ParOK) return;

    ParOK = 1;

    SetFieldAttr ("a_bz2", REMOVED);
    return;
    strcpy (sys_par.sys_par_nam,"a_bz2_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt))
        {
                  SetFieldAttr ("a_bz2", DISPLAYONLY);
                  return;
        }
    }
    SetFieldAttr ("a_bz2", REMOVED);
//    eListe.DestroyField (eListe.GetFieldPos ("a_bz2"));
}

void AUFPLIST::Getlief_me_pr0 (void)
{
    BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    lief_me_pr_0 = 0;
    strcpy (sys_par.sys_par_nam,"auf_me_pr_0");
    if (sys_par_class.dbreadfirst () != 0)
    {
        return;
    }
    lief_me_pr_0 = atoi (sys_par.sys_par_wrt);
}


double AUFPLIST::GetAufMeVgl (double a, double lief_me, short me_einh_kun)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;
         double lief_me_vgl;

         strcpy (kumebest.kun_bran2, kun.kun_bran2);
         einh_class.AktAufEinh (lsk.mdn, lsk.fil,
                                lsk.kun, a, me_einh_kun);
         strcpy (kumebest.kun_bran2, kun.kun_bran2);
         einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                                lsk.kun, a, &keinheit);

         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                      lief_me_vgl = lief_me;
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     lief_me_vgl = lief_me * keinheit.inh;
         }
         return lief_me_vgl;
}



int AUFPLIST::SetRowItem (void)
{
	   static int cursor = -1;
	   static int charg_hand;
	   int pos;
	   double lief_me;

	   pos = eListe.GetAktRow ();
       eListe.SetRowItem ("a", lsptab[pos].a);
       lief_me = ratod (lsptab[pos].lief_me);
	   strcpy (RowCharge, lsptab[pos].ls_charge);
	   if (lief_me == (double) 0.0)
	   {
		   akt_me = (double) 0.0;
	   }
	   else
	   {

           akt_me = GetAufMeVgl (ratod (lsptab[pos].a), lief_me,
                                 atoi (lsptab[pos].me_einh_kun));
	   }
       akt_lief_me = lief_me;
       akt_me_einh = atoi (lsptab[pos].me_einh_kun);
       ls_vk_pr = ratod (lsps.ls_vk_pr);
       ls_lad_pr = ratod (lsps.ls_lad_pr);	// 270812


	   if (!RemoveEnterCharge)
	   {
				if (cursor == -1)
				{
					DbClass.sqlin ((double *) &_a_bas.a_grund, 3, 0);
					DbClass.sqlout ((long *) &charg_hand, 2, 0);
					cursor = DbClass.sqlcursor ("select charg_hand from a_bas where a = ?");
				}
				charg_hand = _a_bas.charg_hand;
				if (_a_bas.a_grund != 0.0)
				{
					DbClass.sqlopen (cursor);
					DbClass.sqlfetch (cursor);
				}

				if (charg_hand == 1 || charg_hand == 9)
				{
					if (!EnableChargeChange)
					{
						clipped (lsps.ls_charge);
						if (strcmp (lsps.ls_charge, " ") > 0)
						{
						   SetItemAttr (eListe.GetDataForm (), "ls_charge", DISPLAYONLY);
						   SetItemAttr (eListe.GetDataForm (), "mhd", DISPLAYONLY);
						}
						else
						{
						    SetItemAttr (eListe.GetDataForm (), "ls_charge", EDIT);
						    SetItemAttr (eListe.GetDataForm (), "mhd", EDIT);
						}
					}
					else
					{
						    SetItemAttr (eListe.GetDataForm (), "ls_charge", EDIT);
						    SetItemAttr (eListe.GetDataForm (), "mhd", EDIT);
					}
				}
				else if (charg_hand != 1 && charg_hand != 9)
				{
				   SetItemAttr (eListe.GetDataForm (), "ls_charge", DISPLAYONLY);
				   SetItemAttr (eListe.GetDataForm (), "mhd", DISPLAYONLY);
				}
	   }
       TestSaPr ();
       return 0;
}

void AUFPLIST::SetAufVkPr (double pr, double pr2)
{
	   ls_vk_pr = pr;
	   ls_lad_pr = pr2;
}

int AUFPLIST::TestAppend (void)
/**
Testen, ob ein Satz angehaengt werden darf.
**/
{
    if (eListe.GetRecanz () == 0);
    else if (ratod (lsps.a) == (double) 0.0)
    {
        return FALSE;
    }

    if (PosSave > 0)
    {
        if (TestInsCount () == FALSE) return 0;
    }
    testmeOK = FALSE;
	ls_vk_pr = (double) 0.0;
	ls_lad_pr = (double) 0.0;	// 270812
    memcpy (&lsps, &lsps_null, sizeof (struct LSPS));
    eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
    return TRUE;
}

int AUFPLIST::KunInfo (void)
{
	Text FreiTxt2 = kun.frei_txt2;
	FreiTxt2.TrimRight ();
	FreiTxt2.MakeLines (60);
	if (FreiTxt2.GetLen () == 0)
	{
		return 0;
	}

	Text wert;
    wert.Format ("%.0lf", kun.kred_lim);
    ptabn.ptbez[0] = 0;
    ptab_class.lese_ptab ("kred_lim", wert.GetBuffer ());
	FreiTxt2 = FreiTxt2 + "\n" + ptabn.ptbez;
    CMessage::Show (FreiTxt2.GetBuffer (), Text (" OK "), RETURN, FALSE);
	return 0;
}

int AUFPLIST::DeleteLine (void)
/**
Zeile aus Liste loeschen.
**/
{
		
	    if (PosDeleteWarning)
		{
		   if (abfragejn (eListe.Getmamain3 (),
					 "Position wirklich l�schen ?" , "N") == 0)
		   {
			   return 0;
		   }
		}
		   

	    BOOL HasService = ContainsService ();
		WaWiePro.erf_kz = "S";
        BucheBsd (ratod (lsps.a), (double) 0.0, atoi (lsps.me_einh_kun),
                  ratod (lsps.ls_vk_pr));
        eListe.DeleteLine ();
	    if (PosSave > 0)
		{
              int recs = eListe.GetRecanz ();
              ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
              if (Getib ())
			  {
                     DeleteBsd ();
			  }
              GenNewPosi ();
              for (int i = 0; i < recs; i ++)
			  {
                     WritePos (i);
			  }
		}
        testmeOK = FALSE;
 	    AnzAufWert ();
 	    AnzAufGew ();
	    if (IsPartyService)
		{
           if (HasService && !ContainsService ())
		   {
/*
				int ret = MessageBox (NULL, "Lieferschein auf Standardsteuersatz setzen ?", NULL,
						MB_YESNO | MB_ICONQUESTION);
				if (ret == IDYES)
				{		
					lsk.psteuer_kz = 1;
				}
*/
				SetLsToStandardTax ();
				EnableFullTax ();
		   }
		}
        return 0;
}

int AUFPLIST::InsertLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        if (PosSave > 0)
        {
            if (TestInsCount () == FALSE) return 0;
        }
        testmeOK = FALSE;
        eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
        eListe.InsertLine ();
        return 0;
}


int AUFPLIST::PosRab (void)
/**
Zeile aus Liste loeschen.
**/
{
/*
	    EnterPosRab ();
        return 0;
*/

	    
        if (GetKeyState (VK_SHIFT) < 0)
		{
			EnterLiefMe3 ();
		}
		else
		{
			wiedereinstieg = 1;
			while (wiedereinstieg)//dies ist nur ne Kr�cke, um den Wert in der Form anzuzeigen Lud 290506
			{
				wiedereinstieg = 0;
				EnterPosRab ();
			}
		}
        return 0;
}

int AUFPLIST::ProdMdn (void)
/**
Zeile aus Liste loeschen.
**/
{
	    long gruppe;

        gruppe = ChoiseProdLgr (atol (lsps.gruppe));
        sprintf (lsps.gruppe, "%ld", gruppe);
        memcpy (&lsptab[eListe.GetAktRow()], &lsps, sizeof (struct LSPS));
        return 0;
}

BOOL AUFPLIST::SavePosis (void)
{
       int recs;
       int i;

       if (PosSave > 1 && PosSaveMess &&
           abfragejn (eListe.Getmamain3 (),
		             "Positionen speichern ?" , "J") == 0)
       {
           DoBreak ();
           return FALSE;
       }
	   int pos = eListe.GetAktRow ();
       memcpy (&lsptab[pos], &lsps, sizeof (struct LSPS));
       recs = eListe.GetRecanz ();


       if (Getib ())
       {
            DeleteBsd ();
       }

	   if ((pos < recs - 1) && recs > 1)
// kein Anh�ngen an Liste sondern einf�gen  
	   {

            WritePos (pos);
		    return TRUE;
	   }

       GenNewPosi ();
       for (i = recs - PosSave; i < recs; i ++)
       {
           WritePos (i);
       }
       commitwork ();
       beginwork ();
       InsCount ++;
       return TRUE;
}


BOOL AUFPLIST::TestInsCount (void)
{
/*
        if (InsCount >= PosSave)
        {
            InsCount = 0;
            return SavePosis ();
        }
        InsCount ++;
*/
        return TRUE;
}


int AUFPLIST::AppendLine (void)
/**
Zeile aus Liste loeschen.
**/
{
/*
        if (PosSave > 0)
        {
            if (TestInsCount () == FALSE) return 0;
        }
*/
	    akt_me = 0.0; 
	    akt_lief_me = 0.0; 
	    if (PosSave > 0)
		{
              int recs = eListe.GetRecanz ();
              ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
              if (Getib ())
			  {
                     DeleteBsd ();
			  }
              GenNewPosi ();
              for (int i = 0; i < recs; i ++)
			  {
                     WritePos (i);
			  }
		}
        testmeOK = FALSE;
	    ls_vk_pr = (double) 0.0;
	    ls_lad_pr = (double) 0.0;	// 270812
        eListe.AppendLine ();
        return 0;
}

BOOL AUFPLIST::BsdArtikel (double a)
/**
Bestandskennzeichen in a_bas pruefen.
**/
{
	     char bsd_kz [2];

		 strcpy (bsd_kz, "N");
		 DbClass.sqlin ((double *) &a, 3, 0);
		 DbClass.sqlout ((char *) bsd_kz, 0, 2);
		 DbClass.sqlcomm ("select bsd_kz from a_bas where a = ?");
		 if (bsd_kz[0] == 'J') return TRUE;
		 return FALSE;
}


void AUFPLIST::BucheBsd (double a, double lief_me, short me_einh_kun, double pr_vk)
/**
Bestandsbuchung vorbereiten.
**/
{
	double lief_me_vgl;
	double buchme;
	char datum [12];
    long lgrort;
	BOOL IsEqual = TRUE;
	BOOL EqualCharge = TRUE;
	Text tLsCharge;
	Text tRowCharge;

	tLsCharge = lsps.ls_charge;
	tRowCharge = RowCharge;
	tLsCharge.TrimRight ();
	tRowCharge.TrimRight ();

//    lief_me_vgl = GetAufMeVgl (a, lief_me,me_einh_kun);
    lief_me_vgl = lief_me;

//	if (lief_me_vgl == akt_me) return;
	if (tRowCharge != tLsCharge)
	{
		IsEqual = FALSE;
		EqualCharge = FALSE;
	}
 
	if (lief_me_vgl != akt_lief_me) 
	{
		IsEqual = FALSE;
	}

	if (IsEqual)
	{
		return;
	}


//	buchme = lief_me_vgl - akt_me;
	if (EqualCharge)
	{
		buchme = lief_me_vgl - akt_lief_me;
		strcpy (bsd_buch.chargennr, lsps.ls_charge);
	}
	else
	{
		buchme = 0 - akt_lief_me;
		strcpy (bsd_buch.chargennr, RowCharge);
	}

	if (buchme != 0.0)
	{
		LeerGut.Update (a, buchme, pr_vk);
		WaWiePro.a = a;
		WaWiePro.auf_lad_pr = ratod (lsps.ls_lad_pr);
		WaWiePro.ls_charge  = lsps.ls_charge;
		WaWiePro.ls_ident   = lsps.ls_ident;
		double netto = buchme;
		if (netto != 0.0)
		{
			if (strcmp (WaWiePro.erf_kz,"S") == 0)
			{
				netto *= -1;
			}
			WaWiePro.Write (netto, netto, 0.0);
		}
	}

	if (bsd_kz == 0) return;
	if (BsdArtikel (a) == FALSE) return;

    bsd_buch.nr  = lsk.ls;
	strcpy (bsd_buch.blg_typ, "WA");
    bsd_buch.mdn = lsk.mdn;
    bsd_buch.fil = lsk.fil;
    bsd_buch.kun_fil = lsk.kun_fil;
	bsd_buch.a   = a;
	sysdate (datum);
	bsd_buch.dat = dasc_to_long (datum);
	systime (bsd_buch.zeit);
	strcpy (bsd_buch.pers, sys_ben.pers);
    if (lsk.tou && tou.lgr > 0)
	{
            sprintf (bsd_buch.bsd_lgr_ort, "%ld", tou.lgr);
	}
	else
	{
            lgrort = KompAuf::GetLagerort (akt_lager, a);
            sprintf (bsd_buch.bsd_lgr_ort, "%ld", lgrort);
	}
	bsd_buch.qua_status = 0;
	bsd_buch.me = buchme * -1;
	bsd_buch.bsd_ek_vk = pr_vk;
//    strcpy (bsd_buch.chargennr, lsps.ls_charge);
    strcpy (bsd_buch.ident_nr, "");
    strcpy (bsd_buch.herk_nachw, "");
    sprintf (bsd_buch.lief, "%ld", lsk.kun);
    bsd_buch.auf = lsk.auf;
    strcpy  (bsd_buch.verfall, "");
    bsd_buch.delstatus = 0;
    strcpy  (bsd_buch.err_txt, "");
	if (buchme != 0.0)
	{
		BsdBuch.dbinsert ();
	}
	if (!EqualCharge)
	{
		buchme = lief_me_vgl;
		if (buchme != 0.0)
		{
			LeerGut.Update (a, buchme, pr_vk);
			strcpy (bsd_buch.chargennr, lsps.ls_charge);
			bsd_buch.me = buchme * -1;
			BsdBuch.dbinsert ();
		}
	}
}

void AUFPLIST::TestPfand (double a)
/**
Bei Pfandverknuepfung Pfand einfuegen.
**/
{
    double lief_me;

    if (autopfand == FALSE) return;
    if (_a_bas.a_typ != 1) return;
    if (a_hndw.a_pfa == (double) 0.0) return;
    lief_me = ratod (lsps.lief_me);
    AppendLine ();
    sprintf (lsps.lief_me, "%.0lf", lief_me);
    sprintf (lsps.a, "%.0lf", a_hndw.a_pfa);
	int row = eListe.GetAktRow ();
    memcpy (&lsptab[eListe.GetAktRow ()], &lsps, sizeof (struct LSPS));
//    fetchaDirect (eListe.GetAktRow ());
    fetcha ();
    testme ();
    AnzAufWert ();
    AnzAufGew ();
}

void AUFPLIST::TestLeih (double a)
/**
Bei Pfandverknuepfung Pfand einfuegen.
**/
{

    if (!IsPartyService) return;
    if (lsps.a_leih == (double) 0.0) return;
	_a_bas.a = lsps.a_leih;
    AppendLine ();
    sprintf (lsps.a, "%.0lf", _a_bas.a);
	int row = eListe.GetAktRow ();
    memcpy (&lsptab[eListe.GetAktRow ()], &lsps, sizeof (struct LSPS));
    fetcha ();
	PostMessage (NULL, WM_KEYDOWN, VK_UP, 0l);
}


int AUFPLIST::WriteRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{

	if (!LiefMe0 && ratod (lsps.a) > 0.0 && ratod (lsps.lief_me) == 0.0)
	{
	       if (ScannMessage)
		   {
			    EnableWindow (hWndMain, FALSE);
		        Text t;
		        t.Format ("Achtung !!\nDie Liefermenge mu� gr��er als 0 sein");
                CMessage::Show (t.GetBuffer ());
		        EnableWindow (hWndMain, TRUE);
		   }
		   else
		   {

			   disp_mess ("Achtung !!\nDie Liefermenge mu� gr��er als 0 sein", 2);
		   }
		   return -1;
	}
    if (testme () == -1) return -1;

	if (testpr () == -1) return -1;
	lastpruef = 99 ;	// 310812 
	set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    if (ratod (lsps.a) == (double) 0.0)
    {
        eListe.DeleteLine ();
        return (1);
    }

	WaWiePro.erf_kz = "H";
    BucheBsd (ratod (lsps.a), ratod (lsps.lief_me), atoi (lsps.me_einh_kun),
              ratod (lsps.ls_vk_pr));

    if (eListe.IsAppend ())
    {
        TestPfand (ratod (lsps.a));
		TestLeih (ratod (lsps.a));
    }
	WriteAktPos ();
    return (0);
}

int AUFPLIST::TestRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{

    if (ratod (lsps.lief_me) == (double) 0.0) return 0;
    if (testme () == -1) return -1;

		if (testpr () == -1) return -1;

    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    return (0);
}

void AUFPLIST::GenNewPosi (void)
/**
Neue Positionsnummern in 10-er Schritten generieren.
**/
{
    int i;
    int row;
    int recs;
    long posi;

    row  = eListe.GetAktRow ();
    memcpy (&lsptab[row], &lsps, sizeof (struct LSPS));
    recs = eListe.GetRecanz ();
    posi = 10;
    for (i = 0; i < recs; i ++, posi += 10)
    {
        sprintf (lsptab[i].posi, "%ld", posi);
    }
    eListe.DisplayList ();
    memcpy (&lsps, &lsptab[row], sizeof (struct LSPS));
}

void AUFPLIST::GenNewPosi0 (void)
/**
Neue Positionsnummern in 10-er Schritten generieren.
**/
{
    int i;
    int recs;
    long posi;

    recs = eListe.GetRecanz ();
    posi = 10;
    for (i = 0; i < recs; i ++, posi += 10)
    {
        sprintf (lsptab[i].posi, "%ld", posi);
    }
}

int AUFPLIST::PosiEnd (long posi)
/**
Positionnummer testen.
**/
{
    int row;
    int recs;
    long nextposi;

    row  = eListe.GetAktRow ();
    recs = eListe.GetRecanz ();

    if (row >= recs - 1)
    {
            return FALSE;
    }

    nextposi = atol (lsptab [row + 1].posi);
    if (nextposi <= posi)
    {
        GenNewPosi ();
        return TRUE;
    }
    return FALSE;
}

void AUFPLIST::TestMessage (void)
{
	MSG msg;

    if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
    {
          TranslateMessage(&msg);
          DispatchMessage(&msg);
    }
}

int AUFPLIST::doStd (void)
/**
Standardauftrag.
**/
{
         double a;
         double me;
         double pr_vk;
         int ret;
         int row;
         int lrow;
		 int SaveRow, SaveColumn;
         int wo_tag;
         char wo_tags [5];
         char datum [12];


         save_fkt (5);
         save_fkt (6);
         save_fkt (7);
         save_fkt (8);
         save_fkt (9);
         save_fkt (10);
         save_fkt (11);
         save_fkt (12);
		 SaveRow     = eListe.GetAktRow ();
		 SaveColumn  = eListe.GetAktColumn ();

         if (FilStandard == FALSE || lsk.kun_fil == 0)
         {
                ret = StndAuf.StdAuftrag (eListe.Getmamain3 (),
                                          lsk.mdn, lsk.fil,
                                          lsk.kun, lsk.kun_fil);
         }
         else
         {
                sysdate (datum);
                wo_tag = get_wochentag (datum);
                sprintf (wo_tags, "%d", wo_tag);
                ret = StndAuf.StdFilAuftrag (mamain1, eListe.Getmamain3 (),
                                             lsk.mdn, (short) lsk.kun,
                                             wo_tags);

         }
         restore_fkt (5);
         restore_fkt (6);
         restore_fkt (7);
         restore_fkt (8);
         restore_fkt (9);
         restore_fkt (10);
         restore_fkt (11);
         restore_fkt (12);
         if (ret == 0)
         {
             return 0; 
         }
         if (ret == 1)
         {
                  a = StndAuf.GetStda ();
                  me = StndAuf.GetStdme ();
                  pr_vk = StndAuf.GetStdpr_vk ();
                  sprintf (lsps.a,  "%.0lf", a);
                  sprintf (lsps.lief_me, "%.3lf", me);
                  sprintf (lsps.ls_vk_pr, "%lf", pr_vk);
                  memcpy (&lsptab [eListe.GetAktRow ()], &lsps,
                          sizeof (struct LSPS));
//                  fetcha ();
                  fetchaDirect (eListe.GetAktRow ());
         }
         else if (ret == 2)
         {
				  eListe.DestroyFocusWindow ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
                  UpdateWindow (eListe.Getmamain3 ());
			      NoArtMess = TRUE;
                  row = 0;
                  lrow = eListe.GetAktRow ();
                  while (StndAuf.GetStdRow (row, &a, &me, &pr_vk))
                  {
// 					    TestMessage ();
                        eListe.SetVPos (lrow);
                        if (ratod (lsptab[lrow].a) ||
                            lrow >= eListe.GetRecanz ())
                        {
//                            AppendLine ();
							  eListe.SetRecanz (lrow);
                        }

                        sprintf (lsps.a,  "%.0lf", a);
                        sprintf (lsps.lief_me, "%.3lf", me);
                        row ++;
                        sprintf (lsps.ls_vk_pr, "%lf", pr_vk);
                        sprintf (lsps.sa_kz_sint, "%1d", 0);
                        memcpy (&lsptab [lrow], &lsps,
                          sizeof (struct LSPS));
                        fetchaDirect (lrow);
                        lrow ++;
                  }
                  if (ratod (lsptab[lrow].a) ||
                           lrow >= eListe.GetRecanz ())
                  {
//                            AppendLine ();
							  eListe.SetRecanz (lrow);
                  }
			      NoArtMess = FALSE;
                  AppendLine ();
                  syskey = KEYUP;
                  eListe.FocusUp ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
				  UpdateWindow (eListe.Getmamain3 ());
         }
         memcpy (&lsps, &lsptab [SaveRow], sizeof (struct LSPS));
		 eListe.SetPos (SaveRow, eListe.FirstColumn ());
         eListe.SetVPos (SaveRow);
         eListe.ShowAktRow ();
         return 0;
}

int AUFPLIST::Querya (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;

	   if (a_kun_smt == 2 || preis0_mess == 2)
	   {
		   SetQueryKun (kun.kun);
	   }

	   if (searchadirect)
	   {
             ret = QClass.searcha (eListe.Getmamain3 ());
	   }
       else
	   {
            ret = QClass.querya (eListe.Getmamain3 ());
	   }
       set_fkt (dokey5, 5);
//       set_fkt (InsertLine, 6);
//       set_fkt (DeleteLine, 7);
//       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (),
                                 eListe.GetAktColumn ());
           return 0;
       }
       UpdateWindow (mamain1);
       sprintf (lsptab[eListe.GetAktRow ()].a, "%.0lf", _a_bas.a);
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (),
                             eListe.GetAktColumn ());
       PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}



BOOL AUFPLIST::ShowA ()
{
        struct SA *Sa;
        SEARCHA SearchA;
        char matchorg [256];
        char matchupper [256];
        char matchlower [256];
        char matchuplow [256];
        char query [512];
        char as [20];

        strcpy (as, lsps.a);
        if (strcmp (clipped (as), " ") > 0 &&
            strcmp (as, "*") != 0)
        {
	        strcpy (matchorg, as);
            strupcpy (matchupper, as);
            strlowcpy (matchlower, as);
            struplowcpy (matchuplow, as);
            sprintf (query, " and (a_bz1 matches \"%s*\" "
                           "or a_bz1 matches \"%s*\" "
                           "or a_bz1 matches \"%s*\" "
                           "or a_bz1 matches \"%s*\")",matchorg, 
                                                          matchupper,
                                                          matchlower,
                                                          matchuplow);
        }
        else
        {
            sprintf (query, " and a_bz1 matches \"*\"");
        }

        SearchA.SetQuery (query);
        SearchA.SetParams (hMainInst, hMainWin);
        SearchA.Setawin (hMainWin);
		SearchA.TestAKun = Testa_kun;
        SearchA.SearchA ();
        if (syskey == KEY5)
        {
            eListe.SetFeldFocus0 (eListe.GetAktRow (),
                             eListe.GetAktColumn ());
            return TRUE;
        }
        Sa = SearchA.GetSa ();
        if (Sa == NULL)
        {
            eListe.SetFeldFocus0 (eListe.GetAktRow (),
                             eListe.GetAktColumn ());
            return TRUE;
        }
       sprintf (lsptab[eListe.GetAktRow ()].a, "%.0lf", ratod (Sa->a));
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (),
                             eListe.GetAktColumn ());
       PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
       return TRUE;
}


BOOL AUFPLIST::GetInstanceScanMode ()
	{
		if (Instance != NULL)
		{
			return Instance->GetScanMode ();
		}
		return FALSE;
}


int AUFPLIST::Savea (void)
/**
Artikelnummer sichern.
**/
{
	   if (GetInstanceScanMode ())
	   {
		   sprintf (lsps.a, "");
	   }
	   else
	   {
		   Akta = ratod (lsps.a);
		   if (! numeric (lsps.a))
		   {
			   sprintf (lsps.a, "%13.0lf", 0.0);
			   memcpy (&lsptab[eListe.GetAktRow()], &lsps, sizeof (struct LSPS));
			   eListe.ShowAktRow ();
		   }
	   }
       set_fkt (Querya, 9);
       SetFkt (9, auswahl, KEY9);
       set_fkt (doStd, 10);
       SetFkt (10, standardauf, KEY10);
       return 0;
}

int AUFPLIST::ChMeEinh (void)
/**
Auswahl Auftragsmengeneinheit.
**/
{

       if (GetKeyState (VK_SHIFT) < 0)
	   {
		   if (SortFlag == ArticleSort)
		   {
			SortByPrice ();
		   }
		   else if (SortFlag == PriceSort)
		   {
			SortByArt ();
		   }
		   else if (SortFlag == NoSort)
		   {
			SortByPrice ();
		   }
		   SetRowItem ();
		   return 0;
	   }
       kumebest.mdn = lsk.mdn;
       kumebest.fil = lsk.fil;
       kumebest.kun = lsk.kun;
       strcpy (kumebest.kun_bran2, kun.kun_bran2);
       kumebest.a = ratod (lsps.a);
       if (ratod (lsps.a))
       {
           strcpy (kumebest.kun_bran2, kun.kun_bran2);
           einh_class.AktAufEinh (lsk.mdn, lsk.fil, lsk.kun,
                                  ratod (lsps.a), atoi (lsps.me_einh_kun));
       }
       else
       {
           strcpy (kumebest.kun_bran2, kun.kun_bran2);
           einh_class.SetAufEinh (1);
       }
       EnableWindow (mamain1, FALSE);
       einh_class.ChoiseEinh ();
       EnableWindow (mamain1, TRUE);
       _a_bas.a = ratod (lsps.a);
       if (syskey == KEY5)
	   {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
		   return 0;
	   }
       ReadMeEinh ();

       memcpy (&lsptab[eListe.GetAktRow()], &lsps, sizeof (struct LSPS));
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
       return 0;
}

long AUFPLIST::GenLspTxt0 (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	int count;
	long nr;

	nr = 0l;
    nr = AutoNr.GetNr (nr);
	count = 0;
	while (nr == 0l)
	{
		         Sleep (10);
		         count ++;
				 if (count == 10) break;
                 nr = AutoNr.GetNr (nr);
	}
	return nr;
}

BOOL AUFPLIST::TxtNrExist (long nr)
{
	   int dsqlstatus;

	   if (wa_pos_txt)
	   {
               DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
               DbClass.sqlin ((short *) &lsk.fil, 1, 0);
			   DbClass.sqlin ((long *)  &nr, 2, 0);
               dsqlstatus = DbClass.sqlcomm ("select posi from lsp_txt "
				                             "where mdn = ? "
											 "and fil = ? "
											 "and posi = ?");
	   }
	   else
	   {
			   DbClass.sqlin ((long *)  &nr, 2, 0);
               dsqlstatus = DbClass.sqlcomm ("select nr from lspt where nr = ?");
	   }
	   if (dsqlstatus == 100) return FALSE;
	   return TRUE;
}


long AUFPLIST::GenLspTxt (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	 long nr;

	 while (TRUE)
	 {
         nr = GenLspTxt0 ();
		 if (TxtNrExist (nr) == FALSE) break;
		 if (nr > 99999999)
		 {
			 return 0l;
		 }
	 }
	 return nr;
}


void AUFPLIST::WriteTextPos (char *txt)
/**
Position schreiben.
**/
{
       long nr;

       nr = atol (lsps.lsp_txt);

       if (nr == 0l) nr = GenLspTxt ();
	   if (nr == 0)
	   {
		        return;
	   }

       sprintf (lsps.lsp_txt, "%ld", nr);
/*
       memcpy (&lsptab[eListe.GetAktRow()],
                &lsps, sizeof (struct LSPS));
*/
	   if (wa_pos_txt)
	   {
                lsp_txt.mdn = lsk.mdn;
                lsp_txt.fil = lsk.fil;
                lsp_txt.ls = lsk.ls;
                lsp_txt.posi = nr;
                lsp_txt.zei       = (long) 10;
                strcpy (lsp_txt.txt, txt);
                lsp_tclass.dbupdate ();
	   }
	   else
	   {
                lspt.nr        = nr;
                lspt.zei       = (long) 10;
                strcpy (lspt.txt, txt);
                lspt_class.dbupdate ();
	   }
}


int AUFPLIST::Texte (void)
/**
Freie Artikeltexte erfassen.
**/
{
    long lsp_txt;
//	int count;

    eListe.SetNoRecNr (FALSE);
    set_fkt (NULL, 9);
    SetFkt (9, leer, 0);
    set_fkt (NULL, 10);
    SetFkt (10, leer, 0);
    set_fkt (NULL, 11);
    SetFkt (11, leer, 0);
    TListe.SethMainWindow (mamain1);
    TListe.SetListLines (12);
    EnableWindow (mamain1, FALSE);
    lsp_txt = atol (lsps.lsp_txt);

    if (lsp_txt == 0l) lsp_txt = GenLspTxt ();

    if (lsp_txt)
	{
            lsp_txt = TListe.EnterAufp (lsk.mdn , lsk.fil, lsk.ls,
						   atol (lsps.posi), lsp_txt);
			sprintf (lsps.lsp_txt, "%ld", lsp_txt);
	}
	else
	{
		     syskey = KEY5;
    }

	if (NoRecNr)
	{
	   eListe.SetNoRecNr (TRUE);
    }
    EnableWindow (mamain1, TRUE);
    SetActiveWindow (mamain1);


    if (syskey != KEY5)
    {
		     if (!TxtByDefault && pControls.IsActive ())
			 {
				 PosTxtKz = pControls.GetPosTxtKz (lsps.pos_txt_kz, TRUE);
				 lsps.pos_txt_kz = (short) PosTxtKz;

			 }
			 else
			 {
				 lsps.pos_txt_kz = (short) pControls.LiefRechVisible;
			 }
             sprintf (lsps.lsp_txt, "%ld", lsp_txt);
             memcpy (&lsptab[eListe.GetAktRow()],
                     &lsps, sizeof (struct LSPS));
    }

	else
	{
			 lsps.pos_txt_kz = (short) pControls.LiefRechVisible;
             sprintf (lsps.lsp_txt, "%ld", lsp_txt);
             memcpy (&lsptab[eListe.GetAktRow()],
                     &lsps, sizeof (struct LSPS));
	}
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);

	if (lutz_mdn_par)
	{
		set_fkt (ProdMdn, 8);
	}
    else if (rab_prov_kz)
	{
		set_fkt (PosRab, 8);
	}


    set_fkt (WriteAllPos, 12);
    set_fkt (Schirm, 11);

    SetFkt (6, einfuegen, KEY6);
    SetFkt (7, loeschen, KEY7);

	if (lutz_mdn_par)
	{
		SetFkt (8, prodmdn, KEY8);
	}
    else if (rab_prov_kz)
	{
		SetFkt (8, posrab, KEY8);
	}

    SetFkt (11, vollbild, KEY11);
    set_fkt (ChMeEinh, 9);
    SetFkt (9, einhausw, KEY9);
    set_fkt (Texte, 10);
    SetFkt (10, texte, KEY10);
	eListe.ShowAktRow ();
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
    return 0;
}

int AUFPLIST::setkey9me (void)
/**
Artikelnummer sichern.
**/
{
/*
       if (ratod (lsps.a))
       {
           einh_class.AktAufEinh (lsk.mdn, lsk.fil, lsk.kun,
                                  ratod (lsps.a), atoi (lsps.me_einh_kun));
       }
       else
       {
           einh_class.SetAufEinh (1);
       }
*/
       testmeOK = FALSE;
       set_fkt (ChMeEinh, 9);
       SetFkt (9, einhausw, KEY9);
       set_fkt (Texte, 10);
       SetFkt (10, texte, KEY10);
       return 0;
}

int AUFPLIST::setkey9basis (void)
/**
Artikelnummer sichern.
**/
{
       ls_vk_pr = ratod (lsps.ls_vk_pr);
//       set_fkt (ShowBasis, 9);
//       SetFkt (9, basisme, KEY9);
       return 0;
}


int AUFPLIST::setkey9basisl (void)
{
       ls_lad_pr = ratod (lsps.ls_lad_pr);
       return 0;
}



char *SaText = "Sonderangebot";
char *DaText = "Dauertiefpreis";

static ITEM iSaTxt  ("",  DaText, "", 0);

static field _fSaTxt [] = {
&iSaTxt,        13,  0, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fSaTxt = {1, 0, 0, _fSaTxt, 0, 0, 0, 0, NULL};
static char *satxt = SaText;


void AUFPLIST::MoveSaW (void)
/**
Fenster Sonderangebot bewegen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;
        if (AufMehWnd == NULL) return;
        if (SaWindow == NULL) return;

	    memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  11 * tm.tmHeight);
        cx = 15 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;
		MoveWindow (SaWindow, x, y, cx, cy, TRUE);
}


HWND AUFPLIST::CreateSaW (void)
/**
Fenster fuer Sonderangebot erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
		long aktionlen;

        if (hMainWin == NULL) return NULL;

		aktionlen = WaPreis.GetAktionLen ();
		if (dauertief && (aktionlen > dauertief))
		{
			iSaTxt.SetFeld (DaText);
			satxt = DaText;
		}
		else
		{
			iSaTxt.SetFeld (SaText);
			satxt = SaText;
		}
		memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  11 * tm.tmHeight);
        cx = 15 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;

        SaWindow       = CreateWindow (
                                       "StaticMess",
                                       "",
                                       WS_BORDER |
                                       WS_POPUP | WS_VISIBLE,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);


        return SaWindow;
}

void AUFPLIST::PaintSa (HDC hdc)
/**
Text anzeigen.
**/
{
	  RECT rect;
	  SIZE size;
	  int x,y;
	  extern HFONT stdHfont;

	  if (SaWindow == NULL) return;

	  GetClientRect (SaWindow, &rect);
      SelectObject (hdc, stdHfont);
      GetTextExtentPoint32 (hdc, satxt, strlen (satxt), &size);
	  x = max (0, (rect.right  - rect.left - size.cx) / 2);
	  y = max (0, (rect.bottom - rect.top  - size.cy) / 2);
	  SetBkMode (hdc, TRANSPARENT);
      SetTextColor (hdc,MessCol);
      SetBkColor (hdc, MessBkCol);
	  TextOut (hdc, x, y, satxt, strlen (satxt));
}

void AUFPLIST::CreateSa (void)
/**
Fenster Sonderpreis anzeigen.
**/
{
	   if (SaWindow) return;

	   SaWindow = CreateSaW ();
/*
	   hdc = GetDC (SaWindow);
	   PaintSa (hdc);
	   ReleaseDC (SaWindow, hdc);


	   currentf = currentfield;
 	   SetStaticWhite (TRUE);
	   display_form (SaWindow, &fSaTxt, 0, 0);
	   SetStaticWhite (FALSE);
       currentfield = currentf;
*/
}

void AUFPLIST::DestroySa (void)
/**
Fenster mit Sonderpreis loeschen.
**/
{
	  if (SaWindow == NULL)
	  {
		  return;
	  }
	  CloseControls (&fSaTxt);
	  DestroyWindow (SaWindow);
	  SaWindow = NULL;
}

void AUFPLIST::TestSaPr (void)
{
/*
       char lieferdat [12];
       int dsqlstatus;
       double pr_ek;
       double pr_vk;
*/
       short sa;

       if (ratod (lsps.ls_vk_pr) == 0)
	   {
		   DestroySa ();
		   return;
	   }

       sa = atoi( lsps.sa_kz_sint);
	   if (sa)
	   {
		   CreateSa ();
	   }
	   else
	   {
		   DestroySa ();
	   }
}

void AUFPLIST::MovePlus (void)
/**
Fenster Sonderangebot bewegen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;
        if (AufMehWnd == NULL) return;
		if (PlusWindow == NULL) return;

	    memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  9 * tm.tmHeight);
        cx = 3 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;
		MoveWindow (PlusWindow, x, y, cx, cy, TRUE);
}

HWND AUFPLIST::CreatePlus (void)
/**
Fenster fuer Sonderangebot erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return NULL;

	    memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  9 * tm.tmHeight);
        cx = 3 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;

        PlusWindow       = CreateWindow (
                                       "StaticMess",
                                       "",
                                       WS_BORDER |
                                       WS_POPUP | WS_VISIBLE,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);


        return PlusWindow;
}

void AUFPLIST::PaintPlus (HDC hdc)
/**
+ anzeigen.
**/
{
	  RECT rect;
	  SIZE size;
	  int x,y;
	  extern HFONT stdHfont;
	  static char *txt = "+";

	  if (PlusWindow == NULL) return;

	  GetClientRect (PlusWindow, &rect);
      SelectObject (hdc, stdHfont);
      GetTextExtentPoint32 (hdc, txt, strlen (txt), &size);
	  x = max (0, (rect.right  - rect.left - size.cx) / 2);
	  y = max (0, (rect.bottom - rect.top  - size.cy) / 2);
	  SetBkMode (hdc, TRANSPARENT);
      SetTextColor (hdc,MessCol);
      SetBkColor (hdc, MessBkCol);
	  TextOut (hdc, x, y, txt, strlen (txt));
}


void AUFPLIST::DestroyPlus (void)
/**
Fenster mit + loeschen.
**/
{
	  if (PlusWindow == NULL)
	  {
		  return;
	  }
	  DestroyWindow (PlusWindow);
	  PlusWindow = NULL;
}

void AUFPLIST::FillDM (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kundenwaehrung ist DM
**/
{
	  double pr_ek_euro;
	  double pr_vk_euro;
	  double kurs;


	  kurs = _mdn.konversion;
	  if (kurs == 0.0) kurs = 1.0;

      if (atoi (_mdn.waehr_prim) == 1)
	  {
/* Basiswaehrung ist DM                  */
	           pr_ek_euro = pr_ek / kurs;
	           pr_vk_euro = pr_vk / kurs;
	  }
	  else if (atoi (_mdn.waehr_prim) == 2)
	  {
/* Basiswaehrung ist EURO                   */
	           pr_ek_euro = pr_ek * kurs;
	           pr_vk_euro = pr_vk * kurs;
	  }
	  else
	  {
	           pr_ek_euro = pr_ek;
	           pr_vk_euro = pr_vk;
	  }
      sprintf (lsps.ls_vk_pr,     "%lf",    pr_ek);
      sprintf (lsps.ls_lad_pr,    "%lf",    pr_vk);
      sprintf (lsps.ls_vk_dm,     "%lf", pr_ek);
      sprintf (lsps.ls_lad_dm,    "%lf", pr_vk);
      sprintf (lsps.ls_vk_euro,   "%lf",  pr_ek_euro);
      sprintf (lsps.ls_lad_euro,  "%lf",  pr_vk_euro);
}

void AUFPLIST::FillEURO (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kunden waehrung ist EURO
**/
{
	  double pr_ek_dm;
	  double pr_vk_dm;
	  double kurs;

	  kurs = _mdn.konversion;
	  if (kurs == 0.0) kurs = 1.0;

	  if (atoi (_mdn.waehr_prim) == 1)
/* Basiswaehrung ist DM                  */
	  {
	          pr_ek_dm = pr_ek * kurs;
	          pr_vk_dm = pr_vk * kurs;
	  }
	  else if (atoi (_mdn.waehr_prim) == 2)
	  {
/* Basiswaehrung ist EURO                   */
	           pr_ek_dm = pr_ek * kurs;
	           pr_vk_dm = pr_vk * kurs;
	  }
      sprintf (lsps.ls_vk_dm,    "%lf",    pr_ek_dm);
      sprintf (lsps.ls_lad_dm,   "%lf",    pr_vk_dm);
      sprintf (lsps.ls_vk_euro,  "%lf",  pr_ek);
      sprintf (lsps.ls_lad_euro, "%lf",  pr_vk);
      sprintf (lsps.ls_vk_pr,    "%lf",    pr_ek);
      sprintf (lsps.ls_lad_pr,   "%lf",    pr_vk);
	  if (ld_pr_prim && atoi (_mdn.waehr_prim) == 1)
	  {
                sprintf (lsps.ls_lad_dm,   "%lf",    pr_vk);
	  }
}

void AUFPLIST::FillFremd (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kunden waehrung ist DM
**/
{
	  double pr_ek_dm;
	  double pr_vk_dm;
	  double pr_ek_euro;
	  double pr_vk_euro;
	  double kurs1;
	  double kurs2;
	  short  faktor;
	  int dsqlstatus;

	  kurs1 = _mdn.konversion;
	  if (kurs1 == 0.0) kurs1 = 1.0;

      DbClass.sqlin ((short *)   &lsk.mdn, 1, 0);
      DbClass.sqlin ((short *)   &lsk.waehrung, 1, 0);
	  DbClass.sqlout ((double *) &kurs2, 3, 0);
	  DbClass.sqlout ((short *)  &faktor, 1, 0);
	  dsqlstatus = DbClass.sqlcomm ("select kurs, faktor from devise "
		                            "where mdn = ? "
									"and devise_nr = ?");
	  if (dsqlstatus != 0)
	  {
            if (atoi (_mdn.waehr_prim) == 1)
			{
				pr_ek_dm = pr_ek;
				pr_vk_dm = pr_vk;
				pr_ek_euro = pr_ek / kurs1;
				pr_vk_euro = pr_vk / kurs1;
			}
			else
			{
				pr_ek_dm = pr_ek * kurs1;
				pr_vk_dm = pr_vk * kurs1;
				pr_ek_euro = pr_ek;
				pr_vk_euro = pr_vk;
			}
	  }
	  else
	  {
		    if (atoi (_mdn.waehr_prim) == 1)
			{
	            pr_ek_dm = pr_ek * kurs2 / faktor;
	            pr_vk_dm = pr_vk * kurs2 / faktor;
				pr_ek_euro = pr_ek_dm / kurs1;
				pr_vk_euro = pr_vk_dm / kurs1;
			}
			else
			{
                pr_ek_euro = pr_ek * kurs2 / faktor;
                pr_vk_euro = pr_vk * kurs2 / faktor;
				pr_ek_dm   = pr_ek_euro / kurs1;
				pr_vk_dm   = pr_vk_euro / kurs1;
			}
	  }
      sprintf (lsps.ls_vk_dm,     "%lf", pr_ek_dm);
      sprintf (lsps.ls_lad_dm,    "%lf", pr_vk_dm);
      sprintf (lsps.ls_vk_euro,   "%lf", pr_ek_euro);
      sprintf (lsps.ls_lad_euro,  "%lf", pr_vk_euro);
//      sprintf (lsps.ls_vk_fremd,  "%lf", pr_ek);
//      sprintf (lsps.ls_lad_fremd, "%lf", pr_vk);
      sprintf (lsps.ls_vk_pr,     "%lf", pr_ek);
      sprintf (lsps.ls_lad_pr,    "%lf", pr_vk);
	  if (ld_pr_prim)
	  {
                sprintf (lsps.ls_lad_dm,   "%lf",    pr_vk);
	  }
}


void AUFPLIST::InitWaehrung (void)
{
       sprintf (lsps.ls_vk_euro,  "%lf", 0.0);
       sprintf (lsps.ls_lad_euro, "%lf", 0.0);
//       sprintf (lsps.ls_vk_fremd, "%lf", 0.0);
//       sprintf (lsps.ls_lad_fremd,"%lf", 0.0);
}

void AUFPLIST::FillWaehrung (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen.
**/
{
	   InitWaehrung ();
       if (lsk.waehrung == 0)
       {
             lsk.waehrung = 1;
       }
       switch (lsk.waehrung )
	   {
	         case 1 :
				 FillDM (pr_ek, pr_vk);
				 break;
	         case 2 :
				 FillEURO (pr_ek, pr_vk);
				 break;
            default :
				 FillFremd (pr_ek, pr_vk);
				 break;
	   }
}

void AUFPLIST::FillAktWaehrung ()
/**
Anzeigefelder fuer EK und VK fuellen.
**/
{
       if (lsk.waehrung == 1 || lsk.waehrung == 0)
	   {
           sprintf (lsps.ls_vk_pr,    "%lf",    lsp.ls_vk_pr);
           sprintf (lsps.ls_lad_pr,   "%lf",    lsp.ls_lad_pr);
	   }
       else if (lsk.waehrung == 2)
	   {
           sprintf (lsps.ls_vk_pr,    "%lf",    lsp.ls_vk_euro);
           sprintf (lsps.ls_lad_pr,   "%lf",    lsp.ls_lad_euro);
     	   if (ld_pr_prim)
		   {
                sprintf (lsps.ls_lad_dm,   "%lf",lsp.ls_lad_euro);
		   }
	   }
/*
       else if (lsk.waehrung == 2)
	   {
           sprintf (lsps.ls_vk_pr,    "%lf",    lsp.ls_vk_fremd);
           sprintf (lsps.ls_lad_pr,   "%lf",    lsp.ls_lad_fremd);
     	   if (ld_pr_prim)
		   {
                sprintf (lsps.ls_lad_dm,   "%lf",lsp.ls_lad_euro);
		   }
	   }
*/
	   else
	   {
           sprintf (lsps.ls_vk_pr,    "%lf",    lsp.ls_vk_pr);
           sprintf (lsps.ls_lad_pr,   "%lf",    lsp.ls_lad_pr);
	   }
}

void AUFPLIST::FillKondArt (char *kond_art)
{
	   int dsqlstatus;

       memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));

       strcpy (ptabn.ptitem,"sap_kond_art");
	   strcpy (ptabn.ptwer1, kond_art);

	   DbClass.sqlin ((char *) ptabn.ptitem, 0, 19);
	   DbClass.sqlin ((char *) ptabn.ptwer1, 0, 9);
	   DbClass.sqlout ((char *) ptabn.ptwer2, 0, 9);
	   dsqlstatus = DbClass.sqlcomm ("select ptwer2 from ptabn "
		                             "where ptitem = ? "
									 "and ptwer1 = ?");
       strcpy (lsps.kond_art0, ptabn.ptwer1);
       strcpy (lsps.kond_art,  ptabn.ptwer2);
}

double AUFPLIST::GetBasa_grund (double a)
{
	   double a_grund = 0;

	   DbClass.sqlin ((double *) &a, 3, 0);
	   DbClass.sqlout ((double *) &a_grund, 3, 0);
	   DbClass.sqlcomm ("select a_grund from a_bas where a = ?");
	   return a_grund;
}


void AUFPLIST::ReadExtraPr (void)
{
    char lieferdat [12];
    int dsqlstatus;
    short sa;
    double pr_ek;
    double pr_vk;
	short extra_mdn;
	long extra_kun;

	if (lsk.kun_fil == 0)
	{
		if (_mdn.tou == 0l || _mdn.kun == 0l)
		{
			return;
		}
		extra_mdn = (short) _mdn.tou;
		extra_kun = _mdn.kun;
	}
	else if (lsk.kun_fil == 1)
	{
		if (_fil.verr_mdn == 0l || _fil.kun == 0l)
		{
			return;
		}
		extra_mdn = (short) _fil.verr_mdn;
		extra_kun = _fil.kun;
	}


	dlong_to_asc (lsk.lieferdat, lieferdat);

// Preise immer mit Filiale 0 lesen.

	   WaPreis.SetAufArt (auf_art);

	   if (DllPreise.PriceLib != NULL && 
	    DllPreise.preise_holen != NULL)
	   {
				  dsqlstatus = (DllPreise.preise_holen) (extra_mdn, 
                                          0,
//                                          lsk.kun_fil,
						                  0,
                                          extra_kun,
                                           ratod (lsps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
				  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
	   }
  	   else
	   {
				dsqlstatus = WaPreis.preise_holen (extra_mdn,
                                          0,
//                                          lsk.kun_fil,
										  0,
                                          extra_kun,
                                           ratod (lsps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
	   }
	   sprintf (lsps.ls_vk_fremd, "%lf", pr_ek);
}


void AUFPLIST::ReadPr (void)
/**
Artikelpreis holen.
**/
{
       char lieferdat [12];
       int dsqlstatus;
       short sa, sa_werbung;
       double pr_ek;
       double pr_vk;
	   BOOL fix;
	   int dsqlstatusw = 100;

       if (Muster)
       {
                    return;
       }

       dlong_to_asc (lsk.lieferdat, lieferdat);

// Preise immer mit Filiale 0 lesen.

	   WaPreis.SetAufArt (auf_art);

// WAL-103  1. Priorit�t Werbung holen  aus der Preisliste   auf kun_erw.werbung
	   // Kennung kein fixer Preis, variabel , zus. zum Normalpreis :   ----> aufp.ksys = 2   und aufp.bestellvorschl =  WerbePreis
	   // Kennung fixer WerbePreis,                                 :   ----> aufp.ksys = 2   und aufp.bestellvorschl =  WerbePreis  aufp.auf_vk_pr = WerbePreis
	   fix = FALSE;
	   int werbung = 0;
	   double werbepreis = 0.0;
		 DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		 DbClass.sqlin ((long *) &lsk.kun, 2, 0);
		 DbClass.sqlout ((long *) &werbung, 2, 0);
		 DbClass.sqlcomm ("select werbung from kun_erw where mdn = ? and fil = 0 and kun = ?");
	   if (lsk.kun_fil == 0 && werbung > 0)
	   {
					dsqlstatusw = WaPreis.werbung_holen (lsk.mdn,
                                          0,
                                          werbung,
                                           ratod (lsps.a),
                                          lieferdat,
                                          &sa_werbung,
                                          &pr_ek,
										  &fix);

					if (sa_werbung == 1 && fix == TRUE) 
					{
		                werbepreis = pr_ek;

					}


	   }
	   
	   if (DllPreise.PriceLib != NULL && 
	    DllPreise.preise_holen != NULL)
	   {
				  dsqlstatus = (DllPreise.preise_holen) (lsk.mdn, 
                                          0,
                                          lsk.kun_fil,
                                          lsk.kun,
                                           ratod (lsps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
				  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
	   }
  	   else
	   {
				dsqlstatus = WaPreis.preise_holen (lsk.mdn,
                                          0,
                                          lsk.kun_fil,
                                          lsk.kun,
                                           ratod (lsps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
	   }
	   if (fix == TRUE && dsqlstatusw == 0) //WAL-103
	   {
			pr_ek = werbepreis;
			sa = sa_werbung;
	   }



       if ((NoArtMess == FALSE) && (pr_ek == (double) 0.0))
       {
               if (preis0_mess == 0 && lief_me_pr_0 && MeDefault)
			   {
			       if (ScannMessage)
				   {
					    EnableWindow (hWndMain, FALSE);
				        Text t;
				        t.Format ("Achtung !!\nPreis 0 gelesen");
				        CMessage::Show (t.GetBuffer ());
					    EnableWindow (hWndMain, TRUE);
				   }
				   else
				   {
                      disp_mess ("Achtung !!\nPreis 0 gelesen", 2);
				   }
			   }
       }

       if (dsqlstatus == 0)
       {
                 sprintf (lsps.ls_vk_pr, "%lf",  pr_ek);
                 sprintf (lsps.ls_lad_pr,"%lf", pr_vk);
                 sprintf (lsps.sa_kz_sint, "%1d", sa);
                 strcpy (lsps.kond_art, WaPreis.GetKondArt ());
                 strcpy (lsps.kond_art0, WaPreis.GetKondArt ());
                 sprintf (lsps.a_grund, "%4.0lf", WaPreis.GetAGrund ());
                 FillKondArt (lsps.kond_art);
       }
	   ls_vk_pr = pr_ek;
	   ls_lad_pr = pr_vk;	// 270812
	   FillWaehrung (pr_ek, pr_vk);
       sprintf (lsps.a_grund, "%4.0lf", WaPreis.GetAGrund ());
       if (ratod (lsps.a_grund) == 0.0)
	   {
            sprintf (lsps.a_grund, "%4.0lf", GetBasa_grund (_a_bas.a));
	   }
	   if (sa)
	   {
		   CreateSa ();
	   }
	   else
	   {
		   DestroySa ();
	   }
	   ReadExtraPr ();
}

void AUFPLIST::GetMeEinhIst (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;

		 memset (&keinheit, 0, sizeof (keinheit));
         lsps.me_einh_ist = 0;
         lsps.inh_ist = 0.0;
         strcpy (lsps.lief_me_bz_ist,  "");
         strcpy (lsps.lief_me_bz_ist,  clipped (keinheit.me_einh_kun_bez));

	 	 einh_class.SetAufEinh (1);
         einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                                lsk.kun, _a_bas.a, &keinheit);

         lsps.me_einh_ist = keinheit.me_einh_kun;
         lsps.inh_ist = keinheit.inh;
         strcpy (lsps.lief_me_bz_ist,  clipped (keinheit.me_einh_kun_bez));
         return;
}


void AUFPLIST::ReadMeEinh (void)
/**
Mengeneinheiten holen.
**/
{
        int dsqlstatus;
        char ptwert [5];

        KEINHEIT keinheit;

		memset (&keinheit, 0, sizeof (keinheit));
        lsps.me_einh_ist = 0;
        lsps.inh_ist = 0.0;
        strcpy (lsps.lief_me_bz_ist,  "");
        strcpy (lsps.lief_me_bz_ist,  clipped (keinheit.me_einh_kun_bez));
 		memcpy (&a_hndw, &a_hndw_null, sizeof (a_hndw));
        einh_class.GetKunEinh (lsk.mdn, lsk.fil, lsk.kun,
                               _a_bas.a, &keinheit);
        strcpy (lsps.basis_me_bz, keinheit.me_einh_bas_bez);
        strcpy (lsps.me_bz, keinheit.me_einh_kun_bez);
        sprintf (lsps.me_einh_kun, "%hd", keinheit.me_einh_kun);
        sprintf (lsps.me_einh,     "%hd", keinheit.me_einh_bas);
        inh = keinheit.inh;
        sprintf (lsps.me_einh_kun1, "%hd", keinheit.me_einh1);
        sprintf (lsps.me_einh_kun2, "%hd", keinheit.me_einh2);
        sprintf (lsps.me_einh_kun3, "%hd", keinheit.me_einh3);

        sprintf (lsps.inh1, "%.3lf", keinheit.inh1);
        sprintf (lsps.inh2, "%.3lf", keinheit.inh2);
        sprintf (lsps.inh3, "%.3lf", keinheit.inh3);

        return;


        switch (_a_bas.a_typ)
        {
             case HNDW :
                     dsqlstatus = HndwClass.lese_a_hndw (_a_bas.a);
                     break;
             case EIG :
                     dsqlstatus = HndwClass.lese_a_eig (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig.me_einh_ek;
                     break;
             case EIG_DIV :
                     dsqlstatus = HndwClass.lese_a_eig_div (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                     break;
             case DIENSTLEISTUNG :
             case LEIHARTIKEL :
                     a_hndw.me_einh_kun = _a_bas.me_einh;
                     break;
             default :
                     a_hndw.me_einh_kun = _a_bas.me_einh;
                     break;
        }

		lsps.a_typ = _a_bas.a_typ;
		lsps.mwst = _a_bas.mwst;

        sprintf (ptwert, "%hd", _a_bas.me_einh);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (lsps.basis_me_bz, ptabn.ptbezk);
        }

        if (dsqlstatus) return;

        sprintf (ptwert, "%hd", a_hndw.me_einh_kun);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (lsps.me_bz, ptabn.ptbezk);
        }
}

void AUFPLIST::rechne_aufme0 (void)
/**
Auftragsmengen berechnen.
**/
{
        double lief_me;
        double auf_me1;
        double auf_me2;
        double auf_me3;
        double inh1;
        double inh2;
        double inh3;

        lief_me = ratod (lsps.lief_me);
        inh1 = ratod (lsps.inh1);
        if (inh1 > (double) 0.0 && atoi (lsps.me_einh_kun1))
        {
               auf_me1 = lief_me / inh1;
               if (atoi (lsps.me_einh_kun1) == 2)
               {
                     sprintf
                         (lsps.auf_me1, "%.3lf", auf_me1);
               }
               else
               {
                     sprintf
                         (lsps.auf_me1, "%.0lf", auf_me1);
               }
        }
        else
        {
               sprintf
                         (lsps.auf_me1, "%.0lf", (double) 0.0);
               auf_me1 = lief_me;
        }
        inh2 = ratod (lsps.inh2);
        if (inh2 > (double) 0.0 && atoi (lsps.me_einh_kun2))
        {
               auf_me2 = auf_me1 / inh2;
               sprintf (lsps.auf_me2, "%.0lf", auf_me2);
        }
        else
        {
               sprintf
                         (lsps.auf_me2, "%.0lf", (double) 0.0);
               auf_me2 = auf_me1;
        }
        inh3 = ratod (lsps.inh3);
        if (inh3 > (double) 0.0 && atoi (lsps.me_einh_kun3))
        {
               auf_me3 = auf_me2 / inh3;
               sprintf (lsps.auf_me3, "%.0lf", auf_me3);
        }
        else
        {
               sprintf
                         (lsps.auf_me3, "%.0lf", (double) 0.0);
        }
}


void AUFPLIST::rechne_aufme1 (void)
/**
Auftragsmengen berechnen.
**/
{
        double lief_me;
        double auf_me1;
        double auf_me2;
        double auf_me3;
        double inh2;
        double inh3;

        lief_me = ratod (lsps.lief_me);
        auf_me1 = lief_me;
        sprintf (lsps.auf_me1, "%.3lf", auf_me1);
        inh2 = ratod (lsps.inh2);
        if (inh2 > (double) 0.0 && atoi (lsps.me_einh_kun2))
        {
               auf_me2 = auf_me1 / inh2;
               sprintf (lsps.auf_me2, "%.0lf", auf_me2 + 0.5);
        }
        else
        {
               sprintf (lsps.auf_me2, "%.0lf", (double) 0.0);
               auf_me2 = auf_me1;
        }
        inh3 = ratod (lsps.inh3);
        if (inh3 > (double) 0.0 && atoi (lsps.me_einh_kun3))
        {
               auf_me3 = auf_me2 / inh3;
               sprintf (lsps.auf_me3, "%.0lf", auf_me3 + 0.5);
        }
        else
        {
               sprintf (lsps.auf_me3, "%.0lf", (double) 0.0);
        }
}

void AUFPLIST::rechne_aufme2 (void)
/**
Auftragsmengen berechnen.
**/
{
        double lief_me;
        double auf_me1;
        double auf_me2;
        double auf_me3;
        double inh2;
        double inh3;

        lief_me = ratod (lsps.lief_me);
        auf_me2 = lief_me;
        sprintf (lsps.auf_me2, "%.3lf", auf_me2);
        inh2 = ratod (lsps.inh2);
        if (inh2 > (double) 0.0 && atoi (lsps.me_einh_kun1))
        {
               auf_me1 = auf_me2 * inh2;
               sprintf (lsps.auf_me1, "%.3lf", auf_me1);
        }
        else
        {
               sprintf (lsps.auf_me1, "%.3lf", (double) 0.0);
        }
        inh3 = ratod (lsps.inh3);
        if (inh3 > (double) 0.0 && atoi (lsps.me_einh_kun3))
        {
               auf_me3 = auf_me2 / inh3;
               sprintf (lsps.auf_me3, "%.0lf", auf_me3 + 0.5);
        }
        else
        {
               sprintf (lsps.auf_me3, "%.0lf", (double) 0.0);
        }
}

void AUFPLIST::rechne_aufme3 (void)
/**
Auftragsmengen berechnen.
**/
{
        double lief_me;
        double auf_me1;
        double auf_me2;
        double auf_me3;
        double inh2;
        double inh3;

        lief_me = ratod (lsps.lief_me);
        auf_me3 = lief_me;
        sprintf (lsps.auf_me3, "%.3lf", auf_me3);
        inh3 = ratod (lsps.inh3);
        if (inh3 > (double) 0.0 && atoi (lsps.me_einh_kun2))
        {
               auf_me2 = auf_me3 * inh3;
               sprintf (lsps.auf_me2, "%.3lf", auf_me2);
        }
        else
        {
               sprintf (lsps.auf_me2, "%.3lf", (double) 0.0);
               auf_me2 = auf_me3;
        }
        inh2 = ratod (lsps.inh2);
        if (inh2 > (double) 0.0 && atoi (lsps.me_einh_kun1))
        {
               auf_me1 = auf_me2 * inh2;
               sprintf (lsps.auf_me1, "%.3lf", auf_me1);
        }
        else
        {
               sprintf (lsps.auf_me1, "%.3lf", (double) 0.0);
        }
}

void AUFPLIST::rechne_liefme (void)
/**
Basismenge der aktuellen Bestellmenge anzeigen.
**/
{
	double lief_me;
	double a;
	short me_einh_kun;
	double lief_me_vgl;
    KEINHEIT keinheit;
    char lieferdat [12];
//    int dsqlstatus;
//    short sa;
//    double pr_ek;
//    double pr_vk;


	return;


    dlong_to_asc (lsk.lieferdat, lieferdat);


    lief_me = ratod (lsps.lief_me);
    a  = ratod (lsps.a);
    me_einh_kun = atoi (lsps.me_einh_kun);

    strcpy (kumebest.kun_bran2, kun.kun_bran2);
    einh_class.AktAufEinh (lsk.mdn, lsk.fil,
                                lsk.kun, a, me_einh_kun);
    strcpy (kumebest.kun_bran2, kun.kun_bran2);
    einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                              lsk.kun, a, &keinheit);

    if (keinheit.me_einh_kun == keinheit.me_einh_bas)
    {
            lief_me_vgl = lief_me;
    }
    else
    {
            if (keinheit.inh <= (double) 0.0)
            {
                     keinheit.inh = (double) 1.0;
            }
            lief_me_vgl = lief_me * keinheit.inh;
    }

    sprintf (lsps.lief_me, "%10.4lf", lief_me_vgl);

/** ????
	WaPreis.SetAufArt (auf_art);

	WaPreis.SetAktMe (lief_me_vgl);

    dsqlstatus = WaPreis.preise_holen (lsk.mdn,
                                          0,
                                          lsk.kun_fil,
                                          lsk.kun,
                                           ratod (lsps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
    if ((NoArtMess == FALSE) && (pr_ek == (double) 0.0))
    {
               if (preis0_mess == 0)
			   {
                      disp_mess ("Achtung !!\nPreis 0 gelesen", 2);
			   }
    }

    if (dsqlstatus == 0)
    {
                 sprintf (lsps.ls_vk_pr, "%6.2lf",  pr_ek);
                 sprintf (lsps.ls_lad_pr, "%6.2lf", pr_vk);
                 sprintf (lsps.sa_kz_sint, "%1d", sa);
                 strcpy (lsps.kond_art, WaPreis.GetKondArt ());
                 strcpy (lsps.kond_art0, WaPreis.GetKondArt ());
                 sprintf (lsps.a_grund, "%4.0lf", WaPreis.GetAGrund ());
    }
    ls_vk_pr = pr_ek;
	FillWaehrung (pr_ek, pr_vk);
    sprintf (lsps.a_grund, "%4.0lf", WaPreis.GetAGrund ());
    if (sa)
	{
		   CreateSa ();
	}
	else
	{
		   DestroySa ();
	}
*/
}


int AUFPLIST::testme (void)
/**
Auftragsmeneg pruefen.
**/
{
//        KEINHEIT keinheit;
        char lief_me [20];
        char charge_gew [20];

        if (testmeOK) return 0;
        if (ratod (lsps.a) == (double) 0.0) return 0;

        if (LeergutAnnahme == FALSE && (ratod (lsps.a) >= LeergutVon &&
                                        ratod (lsps.a) <= LeergutBis) &&
										lsps.pos_stat == 3)
		{
			sprintf (lsps.lief_me, "%.3lf", akt_lief_me);
            memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
			eListe.ShowAktRow ();
		}

		int pos = eListe.GetAktRow ();
		if (add);
        else if (akt_lief_me !=  ratod (lsptab[pos].lief_me));
        else if (akt_me_einh != ratod (lsptab[pos].me_einh_kun));
        else if (ls_vk_pr != ratod (lsps.ls_vk_pr));
        else
        {
             if (meoptimize && add == FALSE)
             {
                   testmeOK = TRUE;
             }
// Row 23.04.2009 Test, weil es bei 1 Bestelleinheit m�glich, war mit Menge 0 weiterzukommen.    
//			 return 0;
        }


		if (add && aufme_old != (double) 0.0)
		{
            lsp.lief_me = ratod (lsps.lief_me);
            sprintf (lsps.lief_me, "%.3lf", lsp.lief_me + aufme_old);
            memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
			eListe.ShowAktRow ();
		}


		if (lsps.emb_anz != 0.0)
		{
			if (ratod (lsps.lief_me) != (lsps.emb_anz * lsps.emb_lief_me))
			{
				lsps.emb_lief_me = ratod (lsps.lief_me);
			    sprintf (lsps.lief_me, "%.3lf", ratod (lsps.lief_me) * lsps.emb_anz);
				emb_anz = 0.0;
			}
		}
//        akt_lief_me  =  atoi (lsptab[pos].lief_me);
        akt_me_einh = atoi (lsptab[pos].me_einh_kun);
        ls_vk_pr   = ratod (lsps.ls_vk_pr);

        sprintf (lsps.auf_me1, "%.3lf", (double) 0.0);
        sprintf (lsps.auf_me2, "%.3lf", (double) 0.0);
        sprintf (lsps.auf_me3, "%.3lf", (double) 0.0);

/*
Row 27.07.09 Dieser Ablauf stammt aus 51100 und wird hier mit aller Wahrscheinlichkeit nicht ben�tigt

        if (ratod (lsps.lief_me) == (double) 0.0 && syskey == KEYCR)
        {
            einh_class.NextAufEinh (lsk.mdn, lsk.fil, lsk.kun,
                                 ratod (lsps.a),
                                 atoi (lsps.me_einh_kun), &keinheit);
            strcpy (lsps.basis_me_bz, keinheit.me_einh_bas_bez);
            strcpy (lsps.me_bz, keinheit.me_einh_kun_bez);
            sprintf (lsps.me_einh_kun, "%hd", keinheit.me_einh_kun);
            inh = keinheit.inh;
            memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());

  	        add = FALSE;
			DestroyPlus ();
            return -1;
        }
*/

        if (ratod (lsps.lief_me) > MAXME)
        {
            print_mess (2, "Die Auftragsmenge ist zu gross");
            sprintf (lsps.lief_me, "%.3lf", (double) 0.0);
            memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
  	        add = FALSE;
			DestroyPlus ();
            return -1;
        }


        if (atoi (lsps.me_einh_kun) ==
            atoi (lsps.me_einh_kun1))
        {
            rechne_aufme1 ();
        }
        else if (atoi (lsps.me_einh_kun) ==
                 atoi (lsps.me_einh_kun2))
        {
            rechne_aufme2 ();
        }
        else if (atoi (lsps.me_einh_kun) ==
                 atoi (lsps.me_einh_kun3))
        {
            rechne_aufme3 ();
        }
        else if (atoi (lsps.me_einh_kun) ==
                 atoi (lsps.me_einh))
        {
            rechne_aufme0 ();
        }
		if (LiefMeDirect)
        {
//            rechne_liefme ();
        }
        if (AufCharge && _a_bas.charg_hand == 1 &&
            (lsc2_par ==1 || lsc3_par == 1))
        {
            rechne_liefme ();
        }
        if (AufCharge && _a_bas.charg_hand == 1 &&
            (lsc2_par == 1 || lsc3_par == 1) && lsps.charge_gew > 0.0)
        {

            if (TestChargeMe && strcmp (lsps.ls_charge, " ") > 0 &&
                ratod (lsps.lief_me) > lsps.charge_gew)
            {
/*
                print_mess (2, "Die Auftragsmenge %.3lf %s ist zu gro�\n"
                               "Maximale Menge f�r Charge %s ist %.3lf %s",
                               ratod (lsps.lief_me), clipped (lsps.basis_me_bz),
                               lsps.ls_charge, lsps.charge_gew, lsps.basis_me_bz);
*/
                print_mess (2, "Die Auftragsmenge %s %s ist zu gro�\n"
                               "Maximale Menge f�r Charge %s ist %s %s",
                               KFormat (lief_me, "%.3lf", ratod (lsps.lief_me)),
                               clipped (lsps.basis_me_bz),
                               lsps.ls_charge,
                               KFormat (charge_gew, "%.3lf", lsps.charge_gew),
                               lsps.basis_me_bz);
                sprintf (lsps.lief_me, "%.3lf", (double) 0.0);
                memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
                eListe.ShowAktRow ();
                eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
  	            add = FALSE;
			    DestroyPlus ();
                return -1;
            }
        }
        memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
        AnzAufWert ();
        AnzAufGew ();
		aufme_old = (double) 0.0;
	    add = FALSE;
		DestroyPlus ();
        if (meoptimize)
        {
             testmeOK = TRUE;
        }
        return 0;
}


int AUFPLIST::TestPrproz_diff (void)
/**
Test, ob die Preisaenderung ueber prproz_diff % ist.
**/
{
	    double oldvk;
	    double diff;
		double diffproz;
		char buffer [256];

		if (ls_vk_pr == (double) 0.0) return 1;
        oldvk = ratod (lsps.ls_vk_pr);
		diff = oldvk - ls_vk_pr;
		if (diff < 0) diff *= -1;
        diffproz = 100 * diff / ls_vk_pr;
		if (diffproz > prproz_diff)
		{
//			print_mess (2, "Achtung !! Preis�nderung �ber %.2lf %c", prproz_diff, '%');
			sprintf (buffer, "Achtung !! Preis�nderung �ber %.2lf %c.\n"
				             "�nderung OK ?", prproz_diff, '%');
            if (abfragejn (eListe.Getmamain3 (),
					       buffer , "N") == 0)
			{
                sprintf (lsps.ls_vk_pr, "%lf", ls_vk_pr);
                memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
                eListe.ShowAktRow ();
                eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());

			    return 0;
			}
		}
		return 1;
}

int AUFPLIST::testmhd (void)
{
	char datum [12];
	sysdate (datum);
	long dlong = dasc_to_long (datum) - 365;
// 270812 
	if (dasc_to_long (lsps.mhd) < dlong)
	{
		    strcpy (lsps.mhd , "01.01.2000");
            memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
            return -1;
	}
	return 0;
}


int AUFPLIST::LadTestPrproz_diff (void)
/**
Test, ob die Preisaenderung ueber prproz_diff % ist.
**/
{
	    double oldlad;
	    double diff;
		double diffproz;
		char buffer [256];

		if (ls_lad_pr == (double) 0.0) return 1;
        oldlad = ratod (lsps.ls_lad_pr);
		diff = oldlad - ls_lad_pr;
		if (diff < 0) diff *= -1;
        diffproz = 100 * diff / ls_lad_pr;
		if (diffproz > prproz_diff)
		{
//			print_mess (2, "Achtung !! Preis�nderung �ber %.2lf %c", prproz_diff, '%');
			sprintf (buffer, "Achtung !! Preis�nderung �ber %.2lf %c.\n"
				             "�nderung OK ?", prproz_diff, '%');
            if (abfragejn (eListe.Getmamain3 (),
					       buffer , "N") == 0)
			{
                sprintf (lsps.ls_lad_pr, "%lf", ls_lad_pr);
                memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
                eListe.ShowAktRow ();
                eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());

			    return 0;
			}
		}
		return 1;
}

int AUFPLIST::testprx (void)
{
	lastpruef = 99 ;
	return ( testpr()) ;
}

int AUFPLIST::testpr (void)
/**
Artikel holen.
**/
{
	    char buffer [256];


// 270812 A

    int lfpos = GetItemPos (&dataform, "ld_pr");
	int aktposi = eListe.GetAktColumn ();
    if ((lfpos == aktposi) && (lfpos > -1 ) && ( lastpruef != aktposi ))
	{
		lastpruef = aktposi ;
		sprintf (buffer, dataform.mask[lfpos].picture, ls_lad_pr);
		ls_lad_pr = ratod (buffer);


		FillWaehrung (ratod (lsps.ls_vk_pr), ratod (lsps.ls_lad_pr));
		memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));

		if ( lsk.kun_fil == 1 || lsk.kun_fil ==  0 )
		{
			if (ladpreistest == 1 && ls_lad_pr)
			{
				if (ratod (lsps.ls_lad_pr) != ls_lad_pr)
				{
					disp_mess ("Achtung !! Der Preis wurde ge�ndert", 2);
				}
				return 0;
			}
			else if (ladpreistest == 4)
			{
				if (LadTestPrproz_diff () == 0)
				{
					eListe.SetPos (eListe.GetAktRow (),
					eListe.GetAktColumn ());
					return (-1);
				}
			}
			else if (ladpreistest == 2 && ls_lad_pr)
			{
				sprintf (lsps.ls_lad_pr, "%lf", ls_lad_pr);
			}

			if (ratod (lsps.a) == (double) 0.0) return 0;
 
			if ((Muster == FALSE) && ratod (lsps.ls_lad_pr) == (double) 0.0 &&
				lsps.a_typ != LEIHARTIKEL && lsps.a_typ != LEERGUT)
			{
				if (lief_me_pr_0)
				{
					if ((eListe.IsAppend ()) && (preis0_mess == 1))
					{
						if ( lsk.kun_fil == 1 )	// bei Kundenpreisen klappt es sonst fast immer ....
						{
							sprintf (buffer, "Achtung !! Preis ist 0\n"
			                         "            OK ?");
							if (abfragejn (eListe.Getmamain3 (),
									buffer , "N") == 0)
							{
								eListe.SetPos (eListe.GetAktRow (),
								eListe.GetAktColumn ());
								return -1;
							}
						}
//						else
							return 0 ;
					}
					else
					{

						sprintf (buffer, "Achtung !! Preis ist 0\n"
			                         "            OK ?");
						if (abfragejn (eListe.Getmamain3 (),
								buffer , "N") == 0)
						{
							eListe.SetPos (eListe.GetAktRow (),
							eListe.GetAktColumn ());
							return -1;
						}
						else
							return 0 ;

// 280812 : zu hart !!!
							disp_mess ("Der Preis darf nicht 0 sein", 2);
							eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
							return (-1);
					}
				}
			}
			if (ratod (lsps.ls_lad_pr) > MAXPR)
			{
				print_mess (2, "Der Preis ist zu gross");
				sprintf (lsps.ls_lad_pr, "%lf", (double) 0.0);
				memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
				eListe.ShowAktRow ();
				eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
				return -1;
			}
			memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
			AnzAufWert ();
			AnzAufGew ();

			return 0;
		
		}	// lsk.kun_fil == 1
	}	// ls_lad_pr

     int fpos = GetItemPos (&dataform, "pr_vk");
     if ( (fpos != -1 ) && (fpos == aktposi) && ( lastpruef != aktposi ))	// 280812
	 {

		 lastpruef = aktposi ;
         sprintf (buffer, dataform.mask[fpos].picture, ls_vk_pr);
	     ls_vk_pr = ratod (buffer);


        if (ratod (lsps.ls_vk_pr) != ls_vk_pr)
        {
            lsps.ls_pos_kz = 1;
//            memcpy (&lsptab[eListe.GetAktRow()],
//                            &lsps, sizeof (struct LSPS));
        }
        FillWaehrung (ratod (lsps.ls_vk_pr), ratod (lsps.ls_lad_pr));
        memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));

	    if (preistest == 1 && ls_vk_pr)
		{
            if (ratod (lsps.ls_vk_pr) != ls_vk_pr)
			{
			        disp_mess ("Achtung !! Der Preis wurde ge�ndert", 2);
			}
			return 0;
		}
		else if (preistest == 4)
		{
			if (TestPrproz_diff () == 0)
			{
               eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
               return (-1);
            }
		}
		else if (preistest == 2 && ls_vk_pr)
		{
            sprintf (lsps.ls_vk_pr, "%lf", ls_vk_pr);
		}


        if (ratod (lsps.a) == (double) 0.0) return 0;
        if ((Muster == FALSE) && ratod (lsps.ls_vk_pr) == (double) 0.0 &&
			lsps.a_typ != LEIHARTIKEL && lsps.a_typ != LEERGUT)
        {
            if (lief_me_pr_0)
            {
               if ((eListe.IsAppend ()) && (preis0_mess == 1))
			   {
			          sprintf (buffer, "Achtung !! Preis ist 0\n"
				                       "            OK ?");
                      if (abfragejn (eListe.Getmamain3 (),
					       buffer , "N") == 0)
					  {
                              eListe.SetPos (eListe.GetAktRow (),
                                             eListe.GetAktColumn ());
							  return -1;
					  }
			  }
              return 0;
            }
            else
            {
		       if (ScannMessage)
			   {
					    EnableWindow (hWndMain, FALSE);
				        Text t;
				        t.Format ("Der Preis darf nicht 0 sein");
				        CMessage::Show (t.GetBuffer ());
					    EnableWindow (hWndMain, TRUE);
			   }
			   else
			   {
                        disp_mess ("Der Preis darf nicht 0 sein", 2);
			   }
               eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
            }
            return (-1);
        }
        if (ratod (lsps.ls_vk_pr) > MAXPR)
        {
            print_mess (2, "Der Preis ist zu gross");
            sprintf (lsps.ls_vk_pr, "%lf", (double) 0.0);
            memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
            return -1;
        }
        memcpy (&lsptab[eListe.GetAktRow()],
                            &lsps, sizeof (struct LSPS));
        AnzAufWert ();
        AnzAufGew ();

	 }
        return 0;
}

int AUFPLIST::TestEnableCharge (void)
{
	    static long charg_hand;
		static int cursor = -1;
    
		if (cursor == -1)
		{
					DbClass.sqlin ((double *) &_a_bas.a_grund, 3, 0);
					DbClass.sqlout ((long *) &charg_hand, 2, 0);
					cursor = DbClass.sqlcursor ("select charg_hand from a_bas where a = ?");
		}
		charg_hand = _a_bas.charg_hand;
		if (_a_bas.a_grund != 0.0)
		{
			DbClass.sqlopen (cursor);
			DbClass.sqlfetch (cursor);
		}
	    if (charg_hand == 0)
		{

//			int apos = eListe.GetFieldPos ("ls_charge");
//			eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
			PostMessage (NULL, WM_KEYDOWN, VK_RETURN, 0l);
		}
	    return 0;
}

int AUFPLIST::TestLsCharge (void)
/**
Artikel holen.
**/
{
	   if (!EnableChargeChange)
	   {
		   clipped (RowCharge);
		   if (strcmp (RowCharge, " ") > 0 &&
			   strcmp (clipped (lsps.ls_charge), clipped (RowCharge)) != 0)
		   {
			   strcpy (lsps.ls_charge, RowCharge);
			   disp_mess ("Die Chargennummer darf nicht ge�ndert werden!!\n"
						  "F�r eine neue Chargenummer mu� die Zeile gel�scht werden.", 2);  
			   return 0;
		   }
	   }
	   if (ChargeZwang && _a_bas.charg_hand == 1)
	   {
		   if (strcmp (clipped (lsps.ls_charge), " ") <= 0)
		   {
			   disp_mess ("Eine Chargennummer mu� eingegeben werden!!\n", 2);
			   return -1;
		   }
	   }
 	   if (bsd_kz == 0) return 0;
       if (AufCharge)
       {
		   lese_a_bas (ratod (lsps.a));
		   clipped (lsps.ls_charge);
		   if (strcmp (lsps.ls_charge, " ") <= 0)
		   {
				BOOL ret = DispCharge ();
				if (ChargeZwang && (ret == FALSE))
				{
					return -1;
				}
				syskey = KEYCR;
		   }
       }
	   return 0;
}

void AUFPLIST::EanGew (char *eans, BOOL eangew)
/**
Gewicht aus ERAN-Nr holen oder Defaultwert.
**/
{
	   char gews [6];
	   double gew;

	   if (eangew)
	   {
		   memcpy (gews, &eans[7], 5);
		   gews [5] = (char) 0;
		   gew = ratod (gews) / 1000;
           sprintf (lsps.lief_me, "%.3lf", gew);
	   }
	   else if (lief_me_default)
	   {
           sprintf (lsps.lief_me, "%d", lief_me_default);
	   }
}


int AUFPLIST::ScanKunEan (double ean)
{
        double a;

        DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
        DbClass.sqlin ((long *)  &lsk.kun, 2, 0);
        DbClass.sqlin ((double *) &ean, 3, 0);
        DbClass.sqlout ((double *) &a, 3, 0);

        int dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                          "where mdn = ? "
                                          "and kun = ? "
                                          "and kun_bran2 <= \"0\" "
                                          "and ean = ?");
        if (dsqlstatus == 100)
        {
               DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
               DbClass.sqlin ((char *)  kun.kun_bran2, 0, 3);
               DbClass.sqlin ((double *) &ean, 3, 0);
               DbClass.sqlout ((double *) &a, 3, 0);

               int dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                          "where mdn = ? "
                                          "and kun = 0 "
                                          "and kun_bran2 = ? "
                                          "and ean = ?");
        }

        if (dsqlstatus == 0)
        {
                 dsqlstatus = lese_a_bas (a);
        }
        return dsqlstatus;
}

void AUFPLIST::InsertBonArtikel (long BonNr)
{
	    int cursor;
		double Lb_a;
        double Lb_me;
        double Lb_pr_vk;
		BOOL p0;
		BOOL ap;
		char txt [256];
		char datum [12];
		long dlong;

		sysdate (datum);
		dlong = dasc_to_long (datum);
        p0 = preis0_mess;
		ap = akt_preis;
        preis0_mess = TRUE;
		akt_preis = TRUE;
		int lrow = eListe.GetAktRow ();
		DbClass.sqlin ((long *) &BonNr, 2, 0);
		DbClass.sqlin ((long *) &dlong, 2, 0);
		DbClass.sqlout ((double *) &Lb_a, 3, 0);
		DbClass.sqlout ((double *) &Lb_me, 3, 0);
		DbClass.sqlout ((double *) &Lb_pr_vk, 3, 0);
		cursor = DbClass.sqlcursor ("select a, lief_me, pr_vk from lb where bon = ? "
			                        "and dat = ? "  
			                        "and (fitok != 1 or fitok is null)" );
        int daycount = 0;
		while (daycount < MaxScanDays)
		{
			    DbClass.sqlopen (cursor);
			    if (DbClass.sqlfetch (cursor) == 0)
				{
					break;
				}
				daycount ++;
		}

	    DbClass.sqlopen (cursor);
		int banz = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			            sprintf (lsps.a, "%.0lf", Lb_a);
			            sprintf (lsps.lief_me, "%.3lf", Lb_me);
                        sprintf (lsps.sa_kz_sint, "%1d", 0);
						sprintf (txt, "Bon-Nummer : %ld", BonNr);
						WriteTextPos (txt);
                        memcpy (&lsptab [lrow], &lsps, sizeof (struct LSPS));
                        fetchaDirect (lrow);
                        memcpy (&lsps, &lsptab [lrow], sizeof (struct LSPS));
						sprintf (lsps.ls_vk_pr, "%lf", Lb_pr_vk);
                        FillWaehrung (Lb_pr_vk, ratod (lsps.ls_lad_pr));
						sprintf (txt, "Bon-Nummer : %ld", BonNr);
						WriteTextPos (txt);
                        memcpy (&lsptab [lrow], &lsps, sizeof (struct LSPS));
						lrow ++;
						banz ++;
		}
		if (banz == 0)
		{
			print_mess (2, "Bon-Nummer %ld nicht gefunden", BonNr);
			return;
		}
        eListe.SetRecanz (lrow);
		if (lrow > 0)
		{
		      eListe.SetPos (lrow - 1, 0);
		}
		eListe.ShowAktRow ();
		AppendLine ();
        memcpy (&lsps, &lsps_null, sizeof (struct LSPS));
        memcpy (&lsptab [lrow], &lsps, sizeof (struct LSPS));
        preis0_mess = p0;
		akt_preis   = ap; 	   
		AnzAufWert ();
 	    AnzAufGew ();
		DbClass.sqlin ((long *) &BonNr, 2, 0);
		DbClass.sqlcomm ("update lb set fitok = 1 where bon = ?");
}
	     

int AUFPLIST::ScanArtikel (double a)
{
        Text Ean;
		Text EanBon;
        Text EanDb;
        Text EanA;
        STRUCTEAN *EanStruct;
        double ean;
        long BonNr;

        scangew = 0l;
        scanpr = 0l;
		EanBon.Format ("%.0lf", a);
        Ean.Format ("%13.0lf", a);
        Ean.Format ("%.0lf", a);
        EanStruct = Scanean.GetStructean (Ean);
        if (EanStruct != NULL)
        {
			EanMeZwang = EanStruct->GetEnterMe ();
            switch (Scanean.GetType (Ean))
            {
            case EANGEW :
                EanDb = Scanean.GetEanForDb (Ean);
                ean = ratod (EanDb.GetBuffer ());
                scangew = atol (Scanean.GetEanGew (Ean));
                break;
            case EANSTK :
                EanDb = Scanean.GetEanForDb (Ean);
                ean = ratod (EanDb.GetBuffer ());
                scangew = atol (Scanean.GetEanGew (Ean)) * 1000;
                break;
            case EANPR :
                EanDb = Scanean.GetEanForDb (Ean);
                ean = ratod (EanDb.GetBuffer ());
				if (EanStruct->GetPrFromEan ())
				{
					scanpr = atol (Scanean.GetEanGew (Ean));
				}
                break;
            case EANBON :
                BonNr = Scanean.GetBonNr (EanBon);
				if (BonNr > 0l)
				{
					InsertBonArtikel (BonNr);
					return BonOK;
				}
				else
				{
					return 100;
				}
                break;
            default :
                ean = ratod (Ean.GetBuffer ());
                if (ScanKunEan (ean) == 0)
                {
                    return 0;
                }
            }
        }
        else
        {
                ean = ratod (Ean.GetBuffer ());
                if (ScanKunEan (ean) == 0)
                {
                    return 0;
                }
        }
        DbClass.sqlout ((double *) &a, 3, 0);
        DbClass.sqlin  ((double *) &ean, 3, 0);
        int dsqlstatus = DbClass.sqlcomm ("select a from a_ean where ean = ?");
        if (dsqlstatus == 0)
        {
                 dsqlstatus = lese_a_bas (a);
        }
        if (dsqlstatus == 0)
        {
                 return dsqlstatus;
        }
        if (EanStruct != NULL && EanStruct->GetAFromEan () != 0)
        {
                 EanA = Ean.SubString (2, EanStruct->GetLen ());
				 if (EanStruct->GetAFromEan () == ADDPLZ)
				 {
					 Text Format;
					 Format.Format ("%c0%hd.0lf", '%', EanStruct->GetLen ());  
					 EanA.Format (Format.GetBuffer (), ratod (EanA.GetBuffer ()));
                     EanA = Scanean.AddPlz (EanA);
				 }
				 a = ratod (EanA.GetBuffer ());
                 dsqlstatus = lese_a_bas (a);
        }
        return dsqlstatus;
}


int AUFPLIST::ReadEan (double ean)
{
	   double a;
	   char eans [14];
	   int dsqlstatus;
	   char PLU [7];
	   long a_krz;
	   BOOL eangew;

	   return ScanArtikel (ean);
	   
       DbClass.sqlin ((double *) &ean,    3, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_ean "
                                     "where ean = ?");
	   if (dsqlstatus == 0)
	   {
           sprintf (lsps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (lsps.a));
		   return dsqlstatus;
	   }
	   sprintf (eans, "%.0lf", a);
	   if (strlen (eans) < 13)
	   {
		   return dsqlstatus;
	   }

	   eangew = FALSE;
	   if (memcmp (eans, "20", 2) == 0);
	   else if (memcmp (eans, "21", 2) == 0);
	   else if (memcmp (eans, "22", 2) == 0);
	   else if (memcmp (eans, "23", 2) == 0);
	   else if (memcmp (eans, "24", 2) == 0);
	   else if (memcmp (eans, "27", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else if (memcmp (eans, "28", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else if (memcmp (eans, "29", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else
	   {
		   return dsqlstatus;
	   }
	   memcpy (PLU, &eans[2], plu_size);
	   PLU[plu_size] = (char) 0;
	   a_krz = atol (PLU);
       DbClass.sqlin ((long *) &a_krz, 2, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_krz "
                                     "where a_krz = ?");
	   if (dsqlstatus == 0)
	   {
           sprintf (lsps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (lsps.a));
		   EanGew (eans, eangew);
		   return dsqlstatus;
	   }
	   a = (double) a_krz;

       DbClass.sqlin ((double *) &a,    3, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_bas "
                                     "where a = ?");
	   if (dsqlstatus == 0)
	   {
           sprintf (lsps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (lsps.a));
		   EanGew (eans, eangew);
	   }
	   return dsqlstatus;
}


int AUFPLIST::ReadEmb (double a)
{
	        
 	    int dsqlstatus =  emb.Read (a);
		if (dsqlstatus != 0)
		{
			return dsqlstatus;
		}
		a = emb.GetA ();
		_a_bas.a = a;
        return lese_a_bas (a);
}


int AUFPLIST::Testa_kun (void)
/**
Test, on fuer den Kunden ein Eintrag in a_kun existiert.
Wenn ja, wird gepr�ft, ob der aktuelle Artikel in a_kun existiert.
**/
{
	   int dsqlstatus;

       DbClass.sqlin ((short *) &lsk.mdn,    1, 0);
       DbClass.sqlin ((short *) &lsk.fil,    1, 0);
       DbClass.sqlin ((long *)  &lsk.kun,   2, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? ");

	   if (dsqlstatus == 100 & TestBranSmt)
       {
           DbClass.sqlin ((short *) &lsk.mdn,     1, 0);
           DbClass.sqlin ((short *) &lsk.fil,     1, 0);
           DbClass.sqlin ((char *)  kun.kun_bran2, 0, 3);
           dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                         "where mdn = ? "
                                         "and   fil = ? "
                                         "and   kun = 0 "
                                         "and   kun_bran2 = ? ");
       }

	   if (dsqlstatus == 100) return 0;

       DbClass.sqlin ((short *) &lsk.mdn,    1, 0);
       DbClass.sqlin ((short *) &lsk.fil,    1, 0);
       DbClass.sqlin ((long *)  &lsk.kun,   2, 0);
       DbClass.sqlin ((double*) &_a_bas.a, 3,0);
       DbClass.sqlout ((char *) lsps.a_kun, 0,17);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? "
                                     "and  a    = ?");
	   if (dsqlstatus == 0) return 0;


       if (TestBranSmt)
       {
           DbClass.sqlin ((short *) &lsk.mdn,    1, 0);
           DbClass.sqlin ((short *) &lsk.fil,    1, 0);
           DbClass.sqlin ((char *)  kun.kun_bran2, 0, 3);
           DbClass.sqlin ((double*) &_a_bas.a, 3,0);
           DbClass.sqlout ((char *) lsps.a_kun, 0,17);
           dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                         "where mdn = ? "
                                         "and   fil = ? "
                                         "and   kun = 0 "
                                         "and   kun_bran2 = ? "
                                         "and  a    = ?");
       }
	   if (dsqlstatus == 0) return 0;

	   if (NoArtMess == FALSE)
	   {
              	   print_mess (2, "Der Artikel ist nicht im Sortiment des Kunden");
	   }
	   if (a_kun_smt == 1)
	   {
		   return 0;
	   }
       return -1;
}


BOOL AUFPLIST::Testa_kun (double art)
/**
Test, ob fuer den Kunden ein Eintrag in a_kun existiert.
Wenn ja, wird gepr�ft, ob der aktuelle Artikel in a_kun existiert.
**/
{
	   int dsqlstatus;
	   char lieferdat[12];
	   short sa;
	   double pr_ek;
	   double pr_vk;
	   static double a;
	   static int a_kun_cursor = -1;
	   static int a_kun_bran_cursor = -1;
	   static int a_kuna_cursor = -1;
	   static int a_kuna_bran_cursor = -1;

	   if (preis0_mess == 2)
	   {
		   WaPreis.SetAufArt (auf_art);
	       dlong_to_asc (lsk.lieferdat, lieferdat);


		   if (DllPreise.PriceLib != NULL && 
			DllPreise.preise_holen != NULL)
		   {
					  dsqlstatus = (DllPreise.preise_holen) (lsk.mdn, 
											   0,
											   lsk.kun_fil,
											   lsk.kun,
											   art,
											   lieferdat,
											   &sa,
											   &pr_ek,
											   &pr_vk);
					  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
		   }
		   else
		   {
						dsqlstatus = WaPreis.preise_holen (lsk.mdn,
											  0,
											  lsk.kun_fil,
											  lsk.kun,
											  art,
											  lieferdat,
											  &sa,
											  &pr_ek,
											  &pr_vk);
		   }
		   if (pr_ek == 0.0)
		   {
			   return FALSE;
		   }
		   return TRUE;

	   }

	   a = art;
	   if (a_kun_smt == 1)
	   {
		   return TRUE;
	   }

	   if (a_kun_cursor == -1)
	   {
		   DbClass.sqlin ((short *) &aufk.mdn,    1, 0);
		   DbClass.sqlin ((short *) &aufk.fil,    1, 0);
		   DbClass.sqlin ((long *)  &aufk.kun,   2, 0);
		   a_kun_cursor = DbClass.sqlcursor ("select a from a_kun "
										 "where mdn = ? "
										 "and   fil = ? "
										 "and   kun = ? ");

	       DbClass.sqlin ((short *) &aufk.mdn,     1, 0);
           DbClass.sqlin ((short *) &aufk.fil,     1, 0);
           DbClass.sqlin ((char *)  kun.kun_bran2, 0, 3);
           a_kun_bran_cursor = DbClass.sqlcursor ("select a from a_kun "
                                         "where mdn = ? "
                                         "and   fil = ? "
                                         "and   kun = 0 "
                                         "and   kun_bran2 = ? ");
		   DbClass.sqlin ((short *) &aufk.mdn,    1, 0);
		   DbClass.sqlin ((short *) &aufk.fil,    1, 0);
		   DbClass.sqlin ((long *)  &aufk.kun,   2, 0);
		   DbClass.sqlin ((double*) &a, 3,0);
		   a_kuna_cursor = DbClass.sqlcursor ("select a from a_kun "
										 "where mdn = ? "
										 "and   fil = ? "
										 "and   kun = ? "
										 "and   a    = ?");
           DbClass.sqlin ((short *) &aufk.mdn,    1, 0);
           DbClass.sqlin ((short *) &aufk.fil,    1, 0);
           DbClass.sqlin ((char *)  kun.kun_bran2, 0, 3);
           DbClass.sqlin ((double*) &_a_bas.a, 3,0);
           a_kuna_bran_cursor = DbClass.sqlcursor ("select a from a_kun "
                                         "where mdn = ? "
                                         "and   fil = ? "
                                         "and   kun = 0 "
                                         "and   kun_bran2 = ? "
                                         "and  a    = ?");
	   }
	   DbClass.sqlopen (a_kun_cursor);
	   dsqlstatus = DbClass.sqlfetch (a_kun_cursor);

	   if (dsqlstatus == 100 & TestBranSmt)
       {
		   DbClass.sqlopen (a_kun_bran_cursor);
		   dsqlstatus = DbClass.sqlfetch (a_kun_bran_cursor);
       }

	   if (dsqlstatus == 100) return TRUE;

	   DbClass.sqlopen (a_kuna_cursor);
	   dsqlstatus = DbClass.sqlfetch (a_kuna_cursor);
	   if (dsqlstatus == 0) return TRUE;


       if (TestBranSmt)
       {
		   DbClass.sqlopen (a_kuna_bran_cursor);
		   dsqlstatus = DbClass.sqlfetch (a_kuna_bran_cursor);
       }
	   if (dsqlstatus == 0) return TRUE;

       return FALSE;
}



void AUFPLIST::GetLastMeAuf (double a)
/**
Letzte Bestellmenge des Kunden holen.
**/
{
	   int cursork;
	   int cursorp;
	   long ls;
	   double lief_me;
	   double pr_vk;
	   double lief_me_ges;
	   char ldat[12];

	   if (AddLastLief);
	   else if (AddLastVkPr);
	   else if (DelLastMe) return;

       DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
       DbClass.sqlin ((short *) &lsk.fil, 1, 0);
       DbClass.sqlin ((long *)  &lsk.kun, 2, 0);
       DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
	   DbClass.sqlout ((long *)  &ls, 2, 0);
	   DbClass.sqlout ((char *)  ldat, 0, 11);
       cursork = DbClass.sqlcursor ("select ls, lieferdat from lsk where mdn = ? "
		                                                  "and fil = ? "
														  "and kun = ? "
														  "and ls != ? "
														  "order by lieferdat desc, "
														  "ls desc");

       DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
       DbClass.sqlin ((short *) &lsk.fil, 1, 0);
	   DbClass.sqlin ((long *)  &ls, 2, 0);
	   DbClass.sqlin ((double *)  &a, 3, 0);
	   DbClass.sqlout ((double *) &lief_me, 3, 0);
	   DbClass.sqlout ((double *) &pr_vk, 3, 0);
       cursorp = DbClass.sqlcursor ("select lief_me, ls_vk_euro  from lsp "
		                            "where mdn = ? "
									"and fil = ? "
									"and ls = ? "
									"and a = ?");
	   lief_me_ges = (double) 0.0;

       while (DbClass.sqlfetch (cursork) == 0)
	   {
		   if (DbClass.sqlopen (cursorp)) break;
		   while (DbClass.sqlfetch (cursorp) == 0)
		   {
			   lief_me_ges += lief_me;
		   }
		   if (lief_me_ges != (double) 0.0) break;
	   }
	   DbClass.sqlclose (cursorp);
	   DbClass.sqlclose (cursork);
       sprintf (lsps.last_me, "%.3lf", lief_me_ges);
       sprintf (lsps.last_ldat, "%s"  , ldat);
	   sprintf (lsps.last_pr_vk,"%.3lf"  , pr_vk);
}


void AUFPLIST::GetLastMe (double a)
/**
Letzte Bestellmenge des Kunden holen.
**/
{
	   long auf;
	   double lief_me_ges;
	   double auf_vk_pr;

	   if (AddLastLief);
	   else if (AddLastVkPr);
	   else if (DelLastMe) return;

	   if (!LastFromAufKun)
	   {
			GetLastMeAuf (a);
			return;
	   }

	   lief_me_ges = 0;
       strcpy (lsps.last_ldat, "");
       aufkun.lief_me = (double) 0.0;
       aufkun.mdn     = lsk.mdn;
       aufkun.fil     = lsk.fil;
       aufkun.kun     = lsk.kun;
       aufkun.a       = a;
       aufkun.kun_fil = lsk.kun_fil;

	   AufKun.dbreadlast ();

       auf = aufkun.auf;
       lief_me_ges = aufkun.auf_me;
       dlong_to_asc (aufkun.lieferdat, lsps.last_ldat );
	   while (AufKun.dbreadnextlast () == 0)
	   {
           if (auf != aufkun.auf) break;
           lief_me_ges += aufkun.lief_me;
		   auf_vk_pr = aufkun.auf_vk_pr;
	   }
	   if (lief_me_ges == (double) 0.0)
	   {
		   GetLastMeAuf (a);
	   }
	   else
	   {
           sprintf (lsps.last_me, "%.3lf", lief_me_ges);
           sprintf (lsps.last_pr_vk, "%.4lf", auf_vk_pr);
	   }
}


int AUFPLIST::TestNewArt (double a)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
	     int i;
         int lspanz;
		 int akt_pos;

		 akt_pos = eListe.GetAktRow ();
         lspanz = eListe.GetRecanz ();
         for (i = 0; i < lspanz; i ++)
		 {
			 if (i == akt_pos) continue;
			 if (a == ratod (lsptab[i].a)) 
			 {
				 if (!GetInstanceScanMode ())
				 {
					break;
				 }
				 Text ActCharge  = lsps.ls_charge;
				 Text ListCharge = lsptab[i].ls_charge;
				 ActCharge.Trim ();
				 ListCharge.Trim ();
				 if (ListCharge == "")
				 {
					 break;
				 }
				 if (ListCharge == ActCharge)
				 {
					 break;
				 }
			 }
		 }
         if (i == lspanz) return -1;
		 return (i);
}


int AUFPLIST::fetchaDirect (int lrow)
/**
Artikel holen.
**/
{

       int dsqlstatus;
       char wert [5];
       long posi;
       int i;


       clipped (lsps.a);
       sprintf (lsps.a, "%13.0lf", ratod (lsps.a));

       einh_class.SetAufEinh (1);
       sprintf (lsps.lsp_txt, "%ld", 0l);
       SetFkt (9, leer, NULL);
       set_fkt (NULL, 10);
       SetFkt (10, leer, NULL);
       posi = atol (lsps.posi);
       if (posi == 0)
       {
             i = lrow;
			 if (PosSave == 0)
			 {
                if (i == 0)
				{
                    posi = 1;
				} 
			    else
				{
				    posi ++;
				}
                if ( PosiEnd (posi) == 0)
				{
                    sprintf (lsps.posi, "%ld", posi);
				}
			 }
			 else
			 {

                if (i == 0)
				{
                    posi = 10;
				} 
				else
				{
					int recs = eListe.GetRecanz ();
					if (lrow >= recs)
					{
						posi += 10;
					}
					else
					{
						posi ++;
					}
				}
			 }
       }

       dsqlstatus = lese_a_bas (ratod (lsps.a));
       if (dsqlstatus == 100 || dsqlstatus == ArtikelGesperrt) //FS-26
       {
           if (syskey != KEYCR &&syskey != KEYTAB
               && eListe.IsAppend ())
           {
               return (-1);
           }
           sprintf (lsptab[lrow].a, "%.0lf", Akta);
           memcpy (&lsps, &lsptab[lrow], sizeof (struct LSPS));
           return (-1);
       }
	   if (!TestSmt ())
	   {
           sprintf (lsptab[lrow].a, "%.0lf", Akta);
           memcpy (&lsps, &lsptab[lrow], sizeof (struct LSPS));
           return (-1);
	   }

       GetLastMe (ratod (lsps.a));

	   lsps.a_typ = _a_bas.a_typ;
	   lsps.mwst = _a_bas.mwst;
	   if (IsPartyService && isPartyDienstleistung(_a_bas.a_typ,_a_bas.mwst))
	   {
 	       SetToFullTax ();
		   EnableFullTax ();
//		   lsk.psteuer_kz = 2;
	   }
	   if (lutz_mdn_par)
	   {
                  sprintf (lsps.gruppe, "%ld", mdnprod);
	   }
       sprintf (lsps.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (lsps.a_bz2,       "%s",      _a_bas.a_bz2);
       lsps.dr_folge = _a_bas.dr_folge;
       sprintf (wert, "%hd", _a_bas.me_einh);
       sprintf (lsps.me_einh, "%hd", _a_bas.me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
       strcpy (lsps.basis_me_bz, ptabn.ptbezk);

       memcpy (&lsptab[lrow], &lsps, sizeof (struct LSPS));
       if (eListe.IsNewRec ())
       {
           if (ratod (lsps.ls_vk_pr) == (double) 0.0)
           {
                    ReadPr ();
           }
           else if (akt_preis)
           {
                    ReadPr ();
           }
           kumebest.mdn = lsk.mdn;
           kumebest.fil = lsk.fil;
           kumebest.kun = lsk.kun;
           strcpy (kumebest.kun_bran2, kun.kun_bran2);
           kumebest.a = ratod (lsps.a);
           ReadMeEinh ();
/*
           if (kun.einz_ausw > 3)
           {
               _a_bas.teil_smt = (short) Smtg.Get (lsk.mdn, lsk.kun, (long) _a_bas.teil_smt);
               sprintf (lsps.teil_smt, "%hd", _a_bas.teil_smt);
           }
           else
           {
               strcpy (lsps.teil_smt, "0");
          }
*/
       }
       else if (ratod (lsps.ls_vk_pr) == (double) 0.0)
	   {
		           ReadPr ();
	   }
       memcpy (&lsptab[lrow], &lsps, sizeof (struct LSPS));
       return 0;
}

BOOL AUFPLIST::TestSmt ()
{
	BOOL ret = TRUE;
	if (smtHandler != NULL && SmtTest > 0)
	{
		ret = smtHandler->TestSmt (&_a_bas);
		if (ret)
		{
            sprintf (lsps.teil_smt, "%hd", smtHandler->Smt ());
			lsk.teil_smt = smtHandler->Smt ();
		}
		else
		{
			MessageBox (eListe.Getmamain3 (), "Der Artikel ist nicht im Sortiment des Lieferscheins",
				                              "Sortminentspr�fung", MB_OK | MB_ICONERROR);
		}
	}
	return ret;
}


int AUFPLIST::fetchMatchCode (void)
{
	   double a;
	   int dsqlstatus;
	   char matchorg [25];
	   char matchupper[25];
	   char matchlower[25];
	   char matchuplow[25];

       strcpy (matchorg, lsps.a);
       strupcpy (matchupper, lsps.a);
       strlowcpy (matchlower, lsps.a);
       struplowcpy (matchuplow, lsps.a);

	   DbClass.sqlout ((double *) &a, 3, 0);
	   DbClass.sqlin ((char *) matchorg, 0, 13);
	   DbClass.sqlin ((char *) matchupper, 0, 13);
	   DbClass.sqlin ((char *) matchlower, 0, 13);
	   DbClass.sqlin ((char *) matchuplow, 0, 13);
	   dsqlstatus = DbClass.sqlcomm ("select a from a_bas where a_bz3 = ? "
		                                                    "or a_bz3 = ? "
														    "or a_bz3 = ? "
														    "or a_bz3 = ?");
	   if (dsqlstatus == 100)
	   {

           print_mess (2, "Artikel %s nicht gefunden", lsps.a);
           sprintf (lsps.a, "%13.0lf", 0.0);
	   }
	   else
	   {
           sprintf (lsps.a, "%13.0lf", a);
	   }
	   return 0;
}

BOOL AUFPLIST::AddPosTxt (char *txt)
/**
Text fuer Nicht- osder Nachliefern einfuegen.
**/
{
       long lsp_txt;

       lsp_txt = atol (lsps.lsp_txt);
       if (lsp_txt == 0l) lsp_txt = GenLspTxt ();
       if (lsp_txt == 0l)
       {
           disp_mess ("Fehler beim Generieren einer Textnummer", 2);
           return FALSE;
       }
       sprintf (lsps.lsp_txt, "%ld", lsp_txt);
       lspt.nr = lsp_txt;
       DbClass.sqlin ((long *) &lspt.nr, 2, 0);
       DbClass.sqlout ((long *) &lspt.zei, 2, 0);
       if (DbClass.sqlcomm ("select max (zei) from lspt where nr = ?") != 0)
       {
                    lspt.zei = 0;
       }
       else if (DbClass.IsLongnull (lspt.zei))
       {
                    lspt.zei = 0;
       }
       lspt.zei += 10;
       strcpy (lspt.txt, txt);
       lspt_class.dbupdate ();
       return TRUE;
}


BOOL AUFPLIST::DispErsatz (void)
{
        char ptwert [5];
        char ers_txt [80];
        char a_bz1 [25];
        char buffer [256];

        if (_a_bas.a_ers_kz == 0)
        {
            return TRUE;
        }

        sprintf (ptwert, "%d", _a_bas.a_ers_kz);
        if (ptab_class.lese_ptab ("a_ers_kz", ptwert) == 0)
        {
            strcpy (ers_txt, ptabn.ptbez);
        }

        if (_a_bas.a_ersatz > 0.0)
        {
            DbClass.sqlin ((double *) &_a_bas.a_ersatz, 3, 0);
            DbClass.sqlout ((char *) a_bz1, 0, 25);
            int dsqlstatus =  DbClass.sqlcomm ("select a_bz1 from a_bas where a = ?");
            if (dsqlstatus == 0)
            {
                 print_mess (1, "%.0lf %s %s\n"
                                "Wir liefern statt dessen %.0lf %s",
                                 _a_bas.a, clipped (_a_bas.a_bz1),
                                  ers_txt, _a_bas.a_ersatz, a_bz1);
                 sprintf (lsps.a,     "%.0lf", _a_bas.a_ersatz);
                 if (AErsInLs)
                 {
                           sprintf (lsps.a_ers, "%0.lf", _a_bas.a);
                 }
                 else
                 {
                           sprintf (lsps.a_ers, "%0.lf", _a_bas.a);
                 }
                 sprintf (buffer, "%.0lf %s %s\n",
                          _a_bas.a, clipped (_a_bas.a_bz1), ers_txt);
                 AddPosTxt (buffer);
                 sprintf (buffer, "Wir liefern statt dessen %.0lf %s",
                                   _a_bas.a_ersatz, a_bz1);
                 AddPosTxt (buffer);
            }
            return TRUE;
        }
        print_mess (2, "%.0lf %s %s\n"
                                "Wir liefern statt dessen %.0lf %s",
                                 _a_bas.a, ers_txt, clipped (_a_bas.a_bz1));
        return FALSE;
}


void AUFPLIST::ReadChargeGew (void)
{
       double a_bsd;
       char buffer [256];


       lsps.charge_gew = 0.0;
       if (lsc2_par == 0 &&
           lsc3_par == 0)
       {
           return;
       }

       if (strcmp (lsps.ls_charge, " ") <= 0)
       {
           return;
       }

       a_bsd = _a_bas.a;


       if (_a_bas.a_grund != 0.0)
       {
           DbClass.sqlin  ((double *) &_a_bas.a_grund, 3, 0);
           DbClass.sqlout ((short *)  &_a_bas.charg_hand, 1, 0);
           int dsqlstatus = DbClass.sqlcomm ("select charg_hand from a_bas where a = ?");
           if (dsqlstatus != 0)
           {
               return;
           }
           a_bsd = _a_bas.a_grund;
       }
       if (_a_bas.charg_hand != 1)
       {
           return;
       }

       sprintf (buffer, "select bsd.bsd_gew "
	 			       "from bsd "
                       "where mdn = ? "
                       "and fil = ? "
                       "and  a = ? "
                       "and chargennr = ?");
      DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
      DbClass.sqlin ((short *) &lsk.fil, 1, 0);
      DbClass.sqlin ((double *) &a_bsd, 3, 0);
      DbClass.sqlin ((char *) lsps.ls_charge, 0, sizeof (lsp.ls_charge));
      DbClass.sqlout ((double *) &lsps.charge_gew, 3, 0);
      int dsqlstatus = DbClass.sqlcomm (buffer);
      lsps.charge_gew += ratod (lsps.lief_me);
      return;
}


BOOL AUFPLIST::DispCharge (void)
{
       char keys [80];
       double a_bsd;

       if (lsc2_par == 0 &&
           lsc3_par == 0)
       {
           return TRUE;
       }

       a_bsd = _a_bas.a;


       if (_a_bas.a_grund != 0.0)
       {
           DbClass.sqlin  ((double *) &_a_bas.a_grund, 3, 0);
           DbClass.sqlout ((short *)  &_a_bas.charg_hand, 1, 0);
           int dsqlstatus = DbClass.sqlcomm ("select charg_hand from a_bas where a = ?");
           if (dsqlstatus != 0)
           {
               return TRUE;
           }
           a_bsd = _a_bas.a_grund;
       }
       if (_a_bas.charg_hand != 1)
       {
           return TRUE;
       }
       sprintf (keys, "%hd %hd %.0lf", lsk.mdn, lsk.fil, a_bsd);

	   if (!SEARCHCHARGE::TestLst (keys))
	   {
		   return TRUE;
	   }

       SEARCHCHARGE *SearchCharge = new SEARCHCHARGE ();
       SearchCharge->SetParams (hMainInst, hMainWin);
       SearchCharge->Setawin (hMainWin);
       EnableWindow (mamain1, FALSE);
       EnableWindow (hWndMain, FALSE);
       SearchCharge->Search (keys);
       EnableWindow (mamain1, TRUE);
       EnableWindow (hWndMain, TRUE);
	   BOOL ret = FALSE;
       if (syskey != KEY5)
       {
		   ret = TRUE;
           SearchCharge->GetSelectedRow (keys);
           int anz = wsplit (keys, " ", '|');
           strcpy (lsps.ls_charge, wort[0]);
           clipped (lsps.ls_charge);
		   strcpy (RowCharge, lsps.ls_charge);
           lsps.charge_gew = ratod (wort[1]);
       }
       delete SearchCharge;
       return ret;
}


BOOL AUFPLIST::BestandOk (void)
{
       double bsd_gew = 0.0;

       if (a_ersatz == 3)
       {
           return DispErsatz ();
       }
 	   if (bsd_kz == 0) return TRUE;
       if (AufCharge)
       {
           BOOL ret = DispCharge ();
		   return TRUE;
       }
	   if (BsdArtikel (_a_bas.a) == FALSE) return TRUE;
       DbClass.sqlout ((double *) &bsd_gew, 3, 0);
       DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
       DbClass.sqlin ((short *) &lsk.fil, 1, 0);
       DbClass.sqlin ((double *) &_a_bas.a, 3, 0);
       DbClass.sqlcomm ("select sum (bsd_gew) from bsd "
                        "where mdn = ? "
                        "and fil = ? "
                        "and a = ?");
       if (a_ersatz == 2 && bsd_gew <= 0.0 && _a_bas.a_grund > 0.0)
       {
           if (abfragejn (NULL, "Artikel nicht mehr vorr�tig\n"
                               "Ersatzartikel buntzen ?", "J"))
           {
//               sprintf (lsps.a,     "%.0lf", _a_bas.a_ersatz);
               sprintf (lsps.a,     "%.0lf", _a_bas.a_grund);
               if (AErsInLs)
               {
                     sprintf (lsps.a_ers, "%0.lf", 0.0);
               }
               else
               {
                     sprintf (lsps.a_ers, "%0.lf", _a_bas.a);
               }
               return TRUE;
           }
           else
           {
               return FALSE;
           }
       }
       else if (a_ersatz > 0 && bsd_gew <= 0.0)
       {
           if (abfragejn (NULL, "Artikel nicht mehr vorr�tig\n"
                               "Artikel erfassen ?", "J"))
           {
//               sprintf (lsps.a,     "%.0lf", _a_bas.a_ersatz);
               sprintf (lsps.a,     "%.0lf", _a_bas.a_grund);
               if (AErsInLs)
               {
                     sprintf (lsps.a_ers, "%0.lf", 0.0);
               }
               else
               {
                     sprintf (lsps.a_ers, "%0.lf", _a_bas.a);
               }
               return TRUE;
           }
           else
           {
               return FALSE;
           }
       }
       return TRUE;
}


void CatBra (char *cha)
/**
Wenn lsc3_par gesetzt ist, wert aud bws_defa.kubraext mit kun,kun_bran2 zusammenbauen
und mit '/' and charge anhaengen.
**/
{
	    char *wert;
		char kubraext [80];

		sprintf (kubraext, "kubraext%s", kun.kun_bran2);
		wert = getenv_default (kubraext);
		if (wert == NULL) return;
		strcat (cha, "/");
		strcat (cha, wert);
		strcat (cha, kun.kun_bran2);
}


char *GenCharge (void)
/**
Chargen-Nummer generieren.
**/
{
	    static char cha [21];
		char datum [12];
		long datl;
		int wk;
		char yy [5];
		char ww [3];

//		sysdate (datum);
//		datl = dasc_to_long (datum);
		datl = lsk.lieferdat;
		datl += 2;
		dlong_to_asc (datl, datum);
		strcpy (yy, &datum[8]);
		wk = get_woche (datum);
		sprintf (ww, "%02d", wk);
		sprintf (cha, "%s%s",yy,ww);
		if (lsc3_par)
		{
			CatBra (cha);
		}
		return cha;
}

void datplushbk (char *datum)
{
        long ldat;


        ldat = dasc_to_long (datum);

        switch (_a_bas.hbk_kz[0])
        {
        case 'T' :
                ldat += _a_bas.hbk_ztr;
                break;
        case 'W' :
                ldat += _a_bas.hbk_ztr * 7;
                break;
        case 'M' :
                ldat += _a_bas.hbk_ztr * 30;
                break;
        default :
			    strcpy (datum, "");
				return;
				
        }
        ldat += HbkPlus;
        dlong_to_asc (ldat, datum);
}

/*
void datplushbk_kun (char *datum)
{
        long ldat;


        ldat = dasc_to_long (datum);

        ldat += a_kun_gx.hbk_ztr;
        ldat += HbkPlus;
        dlong_to_asc (ldat, datum);
}
*/


void AUFPLIST::SetChargeAttr ()
{
	static int cursor = -1;
	static long charg_hand;
	char datum [12];

	if (RemoveEnterCharge) return;

	if (cursor == -1)
	{
		DbClass.sqlin ((double *) &_a_bas.a_grund, 3, 0);
		DbClass.sqlout ((long *) &charg_hand, 2, 0);
		cursor = DbClass.sqlcursor ("select charg_hand from a_bas where a = ?");
	}

	charg_hand = _a_bas.charg_hand;
	if (_a_bas.a_grund != 0.0)
	{
		DbClass.sqlopen (cursor);
		DbClass.sqlfetch (cursor);
	}

	if (charg_hand == 1)
	{
	    eListe.SetFieldAttr ("ls_charge", EDIT);
	    eListe.SetFieldAttr ("mhd", EDIT);
	}
	else if (charg_hand == 0)
	{
	    eListe.SetFieldAttr ("ls_charge", DISPLAYONLY);
	    eListe.SetFieldAttr ("mhd", DISPLAYONLY);
	}

	else if (_a_bas.charg_hand == 2)
	{
	    eListe.SetFieldAttr ("ls_charge", DISPLAYONLY);
	    eListe.SetFieldAttr ("mhd", DISPLAYONLY);
		strcpy (lsps.ls_charge, GenCharge ());
	}
	else if (_a_bas.charg_hand == 3)
	{
	    eListe.SetFieldAttr ("ls_charge", DISPLAYONLY);
	    eListe.SetFieldAttr ("mhd", DISPLAYONLY);
		strcpy (lsps.ls_charge, "");
	}
	else if (_a_bas.charg_hand == 4)
	{
	    eListe.SetFieldAttr ("ls_charge", DISPLAYONLY);
	    eListe.SetFieldAttr ("mhd", DISPLAYONLY);
        sysdate (datum);
        datplushbk (datum);
        memcpy (&lsps.ls_charge[4], &datum[0], 2);
        memcpy (&lsps.ls_charge[2], &datum[3], 2);
        memcpy (&lsps.ls_charge[0], &datum[8], 2);
        lsps.ls_charge[6] = 0;
    }
	else if (_a_bas.charg_hand == 5)
	{
	    eListe.SetFieldAttr ("ls_charge", DISPLAYONLY);
	    eListe.SetFieldAttr ("mhd", DISPLAYONLY);
        if (CalcMhd == LIEFERDATUM)
        {
              dlong_to_asc (lsk.lieferdat, datum);
        }
        else
        {
              sysdate (datum);
        }
        datplushbk (datum);
        memcpy (&lsps.ls_charge[0], &datum[0], 2);
        memcpy (&lsps.ls_charge[2], &datum[3], 2);
        memcpy (&lsps.ls_charge[4], &datum[8], 2);
        lsps.ls_charge[6] = 0;
    }
	else if (_a_bas.charg_hand == 9)
	{
	    eListe.SetFieldAttr ("ls_charge", DISPLAYONLY);
	    eListe.SetFieldAttr ("mhd", DISPLAYONLY);
    }
}


int AUFPLIST::fetcha (void)
/**
Artikel holen.
**/
{

       char buffer [256];
	   RECT rect;
       int dsqlstatus;
       char wert [5];
       long posi;
       int art_pos;
       int i;
	   long gruppe;
	   BOOL IsEan128 = FALSE;


       clipped (lsps.a);
       if (syskey != KEYCR && ratod (lsps.a) == (double) 0.0)
	   {
            return 0;
	   }

	   if (a_kun_smt == 2 || preis0_mess == 2)
	   {
		   SetQueryKun (kun.kun);
	   }

       if (GetInstanceScanMode () && strlen (lsps.a) >=16)
	   {
			  Text Ean = lsps.a;
			  int ret = Instance->ScanEan128 (Ean);
			  if (ret != 0)
			  {
			   sprintf (lsps.a, "%13.0lf", 0.0);
               memcpy (&lsptab[eListe.GetAktRow()],
                       &lsps, sizeof (struct LSPS));
			   UpdateWindow (mamain1);
               eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
               eListe.ShowAktRow ();
			   return (-1);
			  }
			  IsEan128 = TRUE;
	   }

       if (!numeric (lsps.a))
	   {
		   if (matchcode == 2)
		   {
			      fetchMatchCode ();
		   }
		   else if (searchmodedirect)
		   {
                  QClass.searcha_direct (eListe.Getmamain3 (),lsps.a);
		   }
		   else
		   {
//                  QClass.querya_direct (eListe.Getmamain3 (),lsps.a);
				  ShowA ();
		   }
           if (ratod (lsps.a) == (double) 0.0)
		   {
			   sprintf (lsps.a, "%13.0lf", ratod (lsps.a));
               memcpy (&lsptab[eListe.GetAktRow()],
                       &lsps, sizeof (struct LSPS));
			   UpdateWindow (mamain1);
               eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
               eListe.ShowAktRow ();
			   return (-1);
		   }
	   }


// Row 23.04.2009 Fehlermeldung von Schiller, bei Artikel 0 wird immer die Bezeichnung des letzten Artikels angezeigt
       if (ratod (lsps.a) == (double) 0.0)
	   {
		   if (ScannMessage)
		   {
				EnableWindow (hWndMain, FALSE);
				Text t;
				t.Format ("Artikel 0 ist nicht erlaubt");
				CMessage::Show (t.GetBuffer ());
				EnableWindow (hWndMain, TRUE);
		   }
		   else
		   {
                 print_mess (2, "Artikel 0 ist nicht erlaubt");
		   }

           sprintf (lsptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
	   }


       lsps.charge_gew = 0.0;
	   sprintf (lsps.a, "%13.0lf", ratod (lsps.a));

	   emb.Init ();
	   if (MeDefault == 2)
	   {
             MeZwang = FALSE;
	   }
	   else
	   {
             MeZwang = TRUE;
	   }
       dsqlstatus = lese_a_bas (ratod (lsps.a));
       if (dsqlstatus == 100 || dsqlstatus == ArtikelGesperrt) //FS-26
	   {
		   EanMeZwang = TRUE;
           MeZwang = FALSE;
           if (dsqlstatus == 100) dsqlstatus = ReadEan (ratod (lsps.a));
		   if (dsqlstatus == BonOK)
		   {
                 eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                 eListe.ShowAktRow ();
                 return (0);
		   }
		   if (dsqlstatus == 0 && EanMeZwang)
		   {
			     MeZwang = TRUE;
		   }
		   if (dsqlstatus == 100)
		   {
			     dsqlstatus = ReadEmb (ratod (lsps.a));
		   }
		   if (dsqlstatus == 100)
		   {
	             if (MeDefault == 2)
				 {
                     MeZwang = FALSE;
				 }
	             else
				 {
                      MeZwang = TRUE;
				 }
		   }
	   }
	   if (!TestSmt ())
	   {
           sprintf (lsptab[eListe.GetAktRow ()].a, "%.0lf", Akta);
           memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
	   }
	   sprintf (lsps.a, "%13.0lf", ratod (lsps.a));
	   lsps.a_typ = _a_bas.a_typ;
	   lsps.mwst = _a_bas.mwst;
	   lsps.a_leih = _a_bas.a_leih;
	   if (IsPartyService &&isPartyDienstleistung(_a_bas.a_typ,_a_bas.mwst))
	   {
		   if (lsk.psteuer_kz == 1)
		   {
//			   MessageBox (NULL, "Achtung !!\nLieferschein wird auf vollen Steuersatz gesetzt",
//				           NULL, MB_OK | MB_ICONWARNING);        
		   }
		   SetToFullTax ();
		   DisableFullTax ();
//		   lsk.psteuer_kz = 2;
	   }
       if (scangew != 0l)
       {
            sprintf (lsps.lief_me, "%.3lf", (double) scangew / 1000);
       }

	   if (emb.GetAnz () != 0)
	   {
		    if (MeDefault)
			{
                     sprintf (lsps.lief_me, "%.3lf", (double) emb.GetAnz ());
        			 lsps.emb_lief_me = 1.0;
			}
			lsps.emb_anz = (double) emb.GetAnz ();
	   }
	   else 
	   {
		    if (MeDefault)
			{
                     if (ratod (lsps.lief_me) == 0.0 && MeZwang == FALSE)
					 {
                               sprintf (lsps.lief_me, "%.3lf", 1.0);
					 }
        			 lsps.emb_lief_me = ratod (lsps.lief_me);
			}
			lsps.emb_anz = 0.0;
	   }

       art_pos = TestNewArt (ratod (lsps.a));
	   if (art_pos != -1 && a_kum_par == FALSE && art_un_tst)
	   {
		           if (abfragejn (eListe.Getmamain3 (),
					        "Artikel bereits im Auftrag, OK?", "J") == 0)
				   {
                        sprintf (lsptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
                        memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
                        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                        eListe.ShowAktRow ();
                        return (-1);
				   }
	   }

	   if (art_pos != -1 && a_kum_par && art_un_tst == 1)
	   {
		           if (abfragejn (eListe.Getmamain3 (),
					        "Der Artikel ist bereits erfasst.\n\n"
							"Artikel bearbeiten ?", "J") == 0)
				   {
                        sprintf (lsptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
                        memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
                        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                        eListe.ShowAktRow ();
                        return (-1);
				   }
	   }


	   if (art_pos != -1 && a_kum_par)
	   {
  		           GetClientRect (eListe.Getmamain3 (), &rect);
                   DeleteLine ();
				   if (art_pos > eListe.GetAktRow ()) art_pos --;
				   if (emb.GetAnz () != 0.0 &&
					   lsptab[art_pos].emb_anz == emb.GetAnz ())
				   {
//                          lsptab[art_pos].emb_anz  = emb.GetAnz ();
                          sprintf (lsptab[art_pos].lief_me, "%.3lf", 
							       lsptab[art_pos].emb_lief_me);
				   }
				   else
				   {
                          lsptab[art_pos].emb_anz     = 1.0;
				   }
                   eListe.SetNewRow (art_pos);
 		           eListe.SetFeldFocus0 (art_pos, eListe.FirstColumn ());
                   InvalidateRect (eListe.Getmamain3 (), &rect, TRUE);
                   UpdateWindow (eListe.Getmamain3 ());
                   memcpy (&lsps, &lsptab[art_pos], sizeof (struct LSPS));
				   if (add_me)
				   {
				            add = TRUE;
                            aufme_old = ratod (lsps.lief_me);
         		            if (MeDefault)
							{
								if (scangew != 0.0)
								{
                                 sprintf (lsptab[art_pos].lief_me, "%.3lf", (double) scangew / 1000);
								}
								else
								{
                                 sprintf (lsptab[art_pos].lief_me, "%.3lf", 1.0);
								}
							}
				            CreatePlus ();
				   }
				   SetRowItem ();
				   if (IsEan128 && ratod (lsps.lief_me) > 0.0 && strcmp (lsps.ls_charge, " ") > 0)
				   {
                       sprintf (lsptab[art_pos].lief_me, "%.3lf", ratod (lsptab[art_pos].lief_me) + aufme_old);
					   PostMessage (NULL, WM_KEYDOWN, VK_F6, 0l);
				   }
				   else if (IsEan128 && ratod (lsps.lief_me) > 0.0)
				   {
					   PostMessage (NULL, WM_KEYDOWN, VK_RETURN, 0l);
				   }
                   return (0);
	   }
	   scangew = 0l;
       memcpy (&lsptab[eListe.GetAktRow ()], &lsps, sizeof (struct LSPS));

       GetLastMe (ratod (lsps.a));
//       einh_class.SetAufEinh (0);
       einh_class.SetAufEinh (1);
       sprintf (lsps.lsp_txt, "%ld", 0l);
       SetFkt (9, leer, NULL);
       set_fkt (NULL, 10);
       SetFkt (10, leer, NULL);
       posi = atol (lsps.posi);
       if (posi == 0)
       {
             i = eListe.GetAktRow ();
			 if (PosSave == 0)
			 {
                if (i == 0)
				{
                    posi = 1;
				} 
			    else
				{
                    posi = atol (lsptab[i - 1].posi);
				    posi ++;
				}
                if ( PosiEnd (posi) == 0)
				{
                    sprintf (lsps.posi, "%ld", posi);
				}
			 }
			 else
			 {

                if (i == 0)
				{
                    posi = 10;
				} 
				else
				{
                    posi = atol (lsptab[i - 1].posi);
					int recs = eListe.GetRecanz ();
					if (i >= recs - 1)
					{
						posi += 10;
					}
					else
					{
						posi ++;
					}
				}
			 }
			 sprintf (lsps.posi, "%hd", posi);
       }

/*
	   emb.Init ();
       dsqlstatus = lese_a_bas (ratod (lsps.a));
	   if (dsqlstatus == 100)
	   {
           dsqlstatus = ReadEan (ratod (lsps.a));
		   if (dsqlstatus == BonOK)
		   {
                 eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                 eListe.ShowAktRow ();
                 return (0);
		   }
		   if (dsqlstatus == 100)
		   {
			     dsqlstatus = ReadEmb (ratod (lsps.a));
		   }
	   }
       sprintf (lsps.a, "%13.0lf", _a_bas.a);
       if (scangew != 0l)
       {
            sprintf (lsps.lief_me, "%.3lf", (double) scangew / 1000);
			scangew = 0l;
       }

	   if (emb.GetAnz () != 0)
	   {
            sprintf (lsps.lief_me, "%.3lf", (double) emb.GetAnz ());
			lsps.emb_anz = (double) emb.GetAnz ();
			lsps.emb_lief_me = 1.0;
	   }
*/


// Umvorhersehbare Ereignisse

	   if (dsqlstatus < 0)
	   {
		   print_mess (2, "Fehler %d beim Lesen von Artikel %.0lf", dsqlstatus,
                                                                    ratod (lsps.a));
           sprintf (lsptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
	   }


       if (dsqlstatus == 100 || dsqlstatus == ArtikelGesperrt) //FS-26
       {
           if (syskey != KEYCR &&syskey != KEYTAB
               && eListe.IsAppend ())
           {
               return (-1); 
           }
		   if (NoArtMess == FALSE)
		   {
			       if (ScannMessage)
				   {
					    EnableWindow (hWndMain, FALSE);
				        Text t;
				        t.Format ("Artikel %.0lf nicht gefunden"
						, ratod (lsps.a));
				        CMessage::Show (t.GetBuffer ());
					    EnableWindow (hWndMain, TRUE);
				   }
				   else
				   {
					   if (dsqlstatus == ArtikelGesperrt) //FS-26
					   {
							print_mess (2, "Artikel %.0lf ist deaktiviert",
							ratod (lsps.a));
						}
					   else
					   {

							 print_mess (2, "Artikel %.0lf nicht gefunden",
                                     ratod (lsps.a));
					   }
				   }

		   }
           sprintf (lsptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

       if (!BestandOk ())
       {
           sprintf (lsptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

	   if (a_kun_smt && Testa_kun () == -1)
	   {
           sprintf (lsptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
		   return -1;
	   }

	   if (lutz_mdn_par)
	   {

                  memcpy (&lsptab[eListe.GetAktRow()], &lsps, sizeof (struct LSPS));
                  eListe.ShowAktRow ();
				  gruppe = TestProdLgr (mdnprod);
				  if (gruppe == 0)
				  {
                      gruppe = lsk.mdn;
				  }
                  sprintf (lsps.gruppe, "%ld", gruppe);
	   }
       lsps.dr_folge = _a_bas.dr_folge;
       sprintf (lsps.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (lsps.a_bz2,       "%s",      _a_bas.a_bz2);
       sprintf (wert, "%hd", _a_bas.me_einh);
       sprintf (lsps.me_einh, "%hd", _a_bas.me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
       strcpy (lsps.basis_me_bz, ptabn.ptbezk);

       memcpy (&lsptab[eListe.GetAktRow()], &lsps, sizeof (struct LSPS));
       eListe.ShowAktRow ();
       if (eListe.IsNewRec ())
       {
           lsps.ls_pos_kz = 2;
//           if (ratod (lsps.ls_vk_pr) == (double) 0.0)
           {
                    ReadPr ();
					if (scanpr != 0.0)
					{
						sprintf (lsps.ls_vk_pr, "%lf", (double) scanpr / 100);
						scanpr = 0l;
					}
                    if ((Muster == FALSE) && (ratod (lsps.ls_vk_pr) == 0.0) && (preis0_mess == 2 ) &&
						lsps.a_typ != LEIHARTIKEL && lsps.a_typ != LEERGUT )
					{
						disp_mess ("Der Artikel ist nicht im Sortiment des Kunden", 2);
						return -1;
					}
                    if ((Muster == FALSE) && (ratod (lsps.ls_vk_pr) == 0.0) && (preis0_mess == 1) &&
						lsps.a_typ != LEIHARTIKEL && lsps.a_typ != LEERGUT)
					{
			          sprintf (buffer, "Achtung !! Preis 0 gelesen\n"
				                       "            OK ?");
                      if (abfragejn (eListe.Getmamain3 (),
					       buffer , "N") == 0)
					  {
                              sprintf (lsptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
                              memcpy (&lsps, &lsptab[eListe.GetAktRow ()],
								      sizeof (struct LSPS));
                              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                              eListe.ShowAktRow ();
		                      return -1;
					  }
			}
           }
           kumebest.mdn = lsk.mdn;
           kumebest.fil = lsk.fil;
           kumebest.kun = lsk.kun;
           strcpy (kumebest.kun_bran2, kun.kun_bran2);
           kumebest.a = ratod (lsps.a);
           ReadMeEinh ();
/*
           if (kun.einz_ausw > 3)
           {
               _a_bas.teil_smt = (short) Smtg.Get (lsk.mdn, lsk.kun, (long) _a_bas.teil_smt);
               sprintf (lsps.teil_smt, "%hd", _a_bas.teil_smt);
           }
           else
           {
               strcpy (lsps.teil_smt, "0");
           }
*/
           if (vertr_abr_par && lsk.vertr > 0)
		   {
               Prov.GetProvSatz (lsk.mdn,
                                 lsk.fil,
                                 lsk.vertr,
                                 lsk.kun,
								 kun.kun_bran2,
								 _a_bas.a,
								 _a_bas.ag,
								 _a_bas.wg,
								 _a_bas.hwg);
               if (atoi (lsps.sa_kz_sint) && prov_satz.prov_sa_satz != (double) 0.0)
			   {
                   sprintf (lsps.prov_satz, "%4.2lf", prov_satz.prov_sa_satz);
			   }
			   else
			   {
                   sprintf (lsps.prov_satz, "%4.2lf", prov_satz.prov_satz);
			   }


		   }
       }

	   if (atoi (lsps.me_einh) != 2 && lsps.emb_anz == 0.0)
	   {
 		    MeZwang = FALSE; 
		    if (MeDefault)
			{
                     if (ratod (lsps.lief_me) == 0.0)
					 {
                               sprintf (lsps.lief_me, "%.3lf", 1.0);
					 }
             		 lsps.emb_lief_me = ratod (lsps.lief_me);
			}
	   }

	   Instance->SetChargeAttr ();
       memcpy (&lsptab[eListe.GetAktRow()], &lsps, sizeof (struct LSPS));

       eListe.ShowAktRow ();
       eListe.SetRowItem ("a", lsptab[eListe.GetAktRow ()].a);
       set_fkt (NULL, 9);


	   if (!IsEan128 && MeDefault && ratod (lsps.lief_me) != 0 && MeZwang == FALSE)
	   {
		   PostMessage (NULL, WM_KEYDOWN, VK_DOWN, 0l);
	   }
	   else if (IsEan128 && ratod (lsps.lief_me) > 0.0 && strcmp (lsps.ls_charge, " ") > 0)
	   {
		   PostMessage (NULL, WM_KEYDOWN, VK_F6, 0l);
	   }
	   else if (IsEan128 && ratod (lsps.lief_me) > 0.0)
	   {
		   PostMessage (NULL, WM_KEYDOWN, VK_RETURN, 0l);
	   }
       return 0;
}


int AUFPLIST::fetchkun_bran2(void)
/**
Artikel ueber Kunden-Artikelnummer holen.
**/
{
       int dsqlstatus;
	   char kun_bran2 [3];

       DbClass.sqlin  ((short *) &lsk.mdn,   1, 0);
       DbClass.sqlin  ((short *) &lsk.fil,   1, 0);
       DbClass.sqlin  ((long *)  &lsk.kun,   2, 0);
	   DbClass.sqlout ((char *)  kun_bran2,   0, 3);
	   dsqlstatus = DbClass.sqlcomm ("select kun_bran2 from kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ?");

	   if (dsqlstatus == 100)
	   {
 		      print_mess (2, "Kundenartikel-Nummer %.0lf nicht gefunden",
                                                    ratod (lsps.a_kun));
              sprintf (lsptab[eListe.GetAktRow ()].a_kun, "%.0lf", (double) 0);
              memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
              eListe.ShowAktRow ();
			  return -1;
	   }

       DbClass.sqlin ((short *) &lsk.mdn,   1, 0);
       DbClass.sqlin ((short *) &lsk.fil,   1, 0);
	   DbClass.sqlin ((char *)  kun_bran2,   0, 2);
       DbClass.sqlin ((long *)  &lsps.a_kun, 0, 13);
       DbClass.sqlout ((char *)  lsps.a, 0,14);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun_bran2 = ? "
                                     "and   a_kun = ?");

       if (dsqlstatus == 100)
       {
 		      print_mess (2, "Kundenartikel-Nummer %.0lf nicht gefunden",
                                                    ratod (lsps.a_kun));
              sprintf (lsptab[eListe.GetAktRow ()].a_kun, "%.0lf", (double) 0);
              memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
              eListe.ShowAktRow ();
			  return -1;
       }
       return fetcha ();
}


int AUFPLIST::fetcha_kun (void)
/**
Artikel ueber Kunden-Artikelnummer holen.
**/
{
       int dsqlstatus;

       DbClass.sqlin ((short *) &lsk.mdn,    1, 0);
       DbClass.sqlin ((short *) &lsk.fil,    1, 0);
       DbClass.sqlin ((long *)  &lsk.kun,   2, 0);
       DbClass.sqlin ((char *)   lsps.a_kun, 0,13);
       DbClass.sqlout ((char *)  lsps.a, 0,14);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? "
                                     "and  a_kun = ?");

	   if (dsqlstatus == 100)
	   {
		       return (fetchkun_bran2 ());
	   }

       return fetcha ();
}

void AUFPLIST::DeleteBsd (void)
/**
Bestand updaten.
**/
{
	    return;
}


double AUFPLIST::GetAufMeVgl (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;
         double lief_me_vgl;

         strcpy (kumebest.kun_bran2, kun.kun_bran2);
         einh_class.AktAufEinh (lsp.mdn, lsp.fil,
                                lsk.kun, lsp.a, lsp.me_einh_kun);
         strcpy (kumebest.kun_bran2, kun.kun_bran2);
         einh_class.GetKunEinh (lsp.mdn, lsp.fil,
                                lsk.kun, lsp.a, &keinheit);

         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                      lief_me_vgl = lsp.lief_me;
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     lief_me_vgl = lsp.lief_me * keinheit.inh;
         }
         return lief_me_vgl;
}


int AUFPLIST::GetAufMeVglEx (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;

	     int row     = eListe.GetAktRow ();
         strcpy (kumebest.kun_bran2, kun.kun_bran2);
         einh_class.AktAufEinh (lsp.mdn, lsp.fil,
                                lsp.kun, ratod (lsps.a), atoi (lsps.me_einh_kun));
         strcpy (kumebest.kun_bran2, kun.kun_bran2);
         einh_class.GetKunEinh (lsp.mdn, lsp.fil,
                                lsk.kun, ratod (lsps.a), &keinheit);

         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
//                      lsps.auf_me_vgl = ratod (lsps.auf_me);
                      lsptab[row].auf_me_vgl = ratod (lsps.auf_me);
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
//                     lsps.auf_me_vgl = ratod (lsps.auf_me) * keinheit.inh;
                     lsptab[row].auf_me_vgl = ratod (lsps.auf_me) * keinheit.inh;
         }
         return 0;
}

void AUFPLIST::EnterLiefMe3 ()
{
	char cLiefMe3[15];
	sprintf (cLiefMe3, "%.3lf", lsps.lief_me3);
	EnterF Enter;
	Enter.SetColor (GetSysColor (COLOR_3DFACE));
	Enter.EnterText (eListe.Getmamain3 (), "Ist-Auftragsmenge", cLiefMe3, 12, "%.3lf"); 
    if (syskey == KEY5)
    {
            eListe.ShowAktRow ();
	        return;
    }

    lsps.lief_me3 = ratod (cLiefMe3);
    memcpy (&lsptab[eListe.GetAktRow()], &lsps, sizeof (struct LSPS));
    eListe.ShowAktRow ();
}

void AUFPLIST::UpdateBsd (void)
/**
Bestand updaten.
**/
{

		return;
}


void AUFPLIST::WriteAufkun (void)
/**
Tabelle aufkun updaten.
**/
{
	/*********
	   char datum [12];

	   sysdate (datum);
       aufkun.mdn    = lsk.mdn;
       aufkun.fil    = lsk.fil;
       aufkun.kun    = lsk.kun;
       aufkun.auf    = lsk.auf;
       aufkun.a      = lsp.a;
       aufkun.auf_me = lsp.lief_me;
//     aufkun.ls_vk_pr = lsp.ls_vk_pr;
       aufkun.ls_vk_pr = lsp.ls_vk_euro;
       aufkun.lieferdat = lsk.lieferdat;
       aufkun.bearb     = dasc_to_long (datum);
       aufkun.kun_fil   = lsk.kun_fil;
       AufKun.dbupdatelast (aufkunanz);
	   ***********/
}

void AUFPLIST::SetPreise (void)
{
       lsp.ls_vk_pr      = ratod (lsps.ls_vk_dm);
       lsp.ls_lad_pr     = ratod (lsps.ls_lad_dm);
       lsp.ls_vk_euro    = ratod (lsps.ls_vk_euro);
       lsp.ls_lad_euro   = ratod (lsps.ls_lad_euro);
       lsp.ls_vk_fremd   = ratod (lsps.ls_vk_fremd);
       lsp.ls_lad_fremd  = ratod (lsps.ls_lad_fremd);
}


BOOL AUFPLIST::testdecval (void)
/**
Groesse der Decimalwerte pruefen.
**/
{
    static double * vars[] = {&lsp.auf_me,
                              &lsp.lief_me,
                              &lsp.ls_vk_pr,
                              &lsp.ls_lad_pr,
                              &lsp.prov_satz,
                              &lsp.rab_satz,
                              &lsp.auf_me1,
                              &lsp.inh1,
                              &lsp.auf_me2,
                              &lsp.inh2,
                              &lsp.auf_me3,
                              &lsp.inh3,
							  &lsp.inh_ist,
							  &lsp.me_ist,
							  &lsp.lief_me1,
							  &lsp.lief_me2,
							  &lsp.lief_me3,
							  &lsp.auf_me_vgl,
							  NULL};

	static double values[] =  {99999.999,    // auf_me
		                       99999.999,    //lief_me
							   9999.9999,	 // ls_vk_pr	
							   9999.99,	     // ls_lad_pr
							   99.99,        // prov_satz
							   999.99,       // rab_satz
							   99999.999,    // auf_me1
							   99999.999,    // inh1
							   99999.999,    // auf_me2
							   99999.999,    // inh2
							   99999.999,    // auf_me3
							   99999.999,    // inh3
							   99999.999,    // inh_ist 
							   99999.999,    // me_ist 
							   99999.999,    // lief_me1 
							   99999.999,    // lief_me2 
							   99999.999,    // lief_me3 
							   99999.999,    // auf_me_vgl
	};

	static double mvalues[] =  {-99999.999,    // auf_me
		                        -99999.999,    //lief_me
					  		    -9999.9999,	 // ls_vk_pr	
							    -9999.99,	     // ls_lad_pr
							    -9999.99,        // prov_satz
							    -999.99,       // rab_satz
							   -99999.999,    // auf_me1
							   -99999.999,    // inh1
							   -99999.999,    // auf_me2
							   -99999.999,    // inh2
							   -99999.999,    // auf_me3
							   -99999.999,    // inh3
							   -99999.999,    // inh_ist 
							   -99999.999,    // me_ist 
							   -99999.999,    // lief_me1 
							   -99999.999,    // lief_me2 
							   -99999.999,    // lief_me3 
							   -99999.999,    // auf_me_vgl
	};

	static double nvalues[] =  {0.0,    // auf_me
							    0.0,    // lief_me
								0.0,    // ls_vk_pr	
							    0.0,    // ls_lad_pr
							    0.0,    // prov_satz
							    0.0,    // rab_satz
							    0.0,    // auf_me1
							    0.0,    // inh1
							    0.0,    // auf_me2
							    0.0,    // inh2
							    0.0,    // auf_me3
							    0.0,    // inh3
							    0.0,    // inh_ist 
							    0.0,    // me_ist 
							    0.0,    // lief_me1 
							    0.0,    // lief_me2 
							    0.0,    // lief_me3 
							    0.0,    // auf_me_vgl
	};

	int i;

	for (i = 0; vars [i] != NULL; i ++)
	{
		if (*vars[i] > values [i])
		{
			*vars[i] = values [i];
		}
		else if (*vars[i] < mvalues [i])
		{
			*vars[i] = nvalues [i];
		}
		else if (IsDoublenull (*vars[i]))
		{
			*vars[i] = 0.0;
		}
	}
	return 0;
}


void AUFPLIST::datplushbk (char *datum)
{
        long ldat;


        ldat = dasc_to_long (datum);

        switch (_a_bas.hbk_kz[0])
        {
        case 'T' :
                ldat += _a_bas.hbk_ztr;
                break;
        case 'W' :
                ldat += _a_bas.hbk_ztr * 7;
                break;
        case 'M' :
                ldat += _a_bas.hbk_ztr * 30;
                break;
        default :
			    strcpy (datum, "");
				return;
				
        }
        ldat += HbkPlus;
        dlong_to_asc (ldat, datum);
}

void AUFPLIST::WritePos (int pos)
/**
Position schreiben.
**/
{
	   KEINHEIT keinheit;
	   double pr_vk;
       char ptwert [10];


       lsp.posi       = atol  (lsptab[pos].posi);
       lsp.sa_kz_sint = atoi  (lsptab[pos].sa_kz_sint);
       lsp.a          = ratod (lsptab[pos].a);
       lsp.lief_me     = ratod (lsptab[pos].lief_me);
       lsp.auf_me     = ratod (lsptab[pos].auf_me); //010805

       lsp.rab_satz   = ratod (lsptab[pos].rab_satz);
       lsp.prov_satz  = ratod (lsptab[pos].prov_satz);
       sprintf (lsp.lief_me_bz,"%s",lsptab[pos].me_bz);
       sprintf (lsp.erf_kz,"%s",lsptab[pos].erf_kz);

       lsp.ls_vk_pr  = ratod (lsptab[pos].ls_vk_dm);
       lsp.ls_lad_pr  = ratod (lsptab[pos].ls_lad_dm);
       lsp.ls_vk_euro  = ratod (lsptab[pos].ls_vk_euro);
       lsp.ls_lad_euro  = ratod (lsptab[pos].ls_lad_euro);
       lsp.ls_vk_fremd  = ratod (lsptab[pos].ls_vk_fremd);
       lsp.ls_lad_fremd  = ratod (lsptab[pos].ls_lad_fremd);

       sprintf (lsp.lief_me_bz,"%s",lsptab[pos].basis_me_bz);
       if (lsp.a == (double) 0)
       {
           return;
       }
       lsp.me_einh_kun = atoi (lsptab[pos].me_einh_kun);
	   if (lsp.me_einh_kun < 0) lsp.me_einh_kun = 2;
       strcpy (lsp.auf_me_bz, lsptab[pos].me_bz);


       strcpy (kumebest.kun_bran2, kun.kun_bran2);
       lsp.lief_me = ratod (lsptab[pos].lief_me);

       einh_class.AktAufEinh (lsk.mdn, lsk.fil,
                                lsk.kun, lsp.a, lsp.me_einh_kun);
       strcpy (kumebest.kun_bran2, kun.kun_bran2);

       memcpy (&lsps, &lsptab[pos], sizeof (struct LSPS));

       einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                              lsk.kun, lsp.a, &keinheit);

/*
	   if (_a_bas.charg_hand == 0)
	   {
		   strcpy (lsptab[pos].ls_charge, "");
	   }
*/
/*
       if (keinheit.me_einh_kun == keinheit.me_einh_bas)
	   {
             lsp.lief_me = lsp.lief_me;
	   }
       else
	   {
            if (keinheit.inh <= (double) 0.0)
            {
                     keinheit.inh = (double) 1.0;
            }
            lsp.lief_me = lsp.lief_me * keinheit.inh;
	   }
*/

       lsp.me_einh     = atoi (lsptab[pos].me_einh);
	   if (lsp.me_einh < 0) lsp.me_einh = 2;
       sprintf (ptwert, "%hd", lsp.me_einh);
       if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
       {
            strcpy (lsp.lief_me_bz, ptabn.ptbezk);
       }
       lsp.lsp_txt    = atol (lsptab[pos].lsp_txt);

       lsp.me_einh_kun1 = atoi (lsptab[pos].me_einh_kun1);
	   if (lsp.me_einh_kun1 < 0) lsp.me_einh_kun1 = 2;
       lsp.auf_me1      = ratod (lsptab[pos].auf_me1);
       lsp.inh1         = ratod (lsptab[pos].inh1);

       lsp.me_einh_kun2 = atoi (lsptab[pos].me_einh_kun2);
	   if (lsp.me_einh_kun2 < 0) lsp.me_einh_kun2 = 2;
       lsp.auf_me2      = ratod (lsptab[pos].auf_me2);
       lsp.inh2         = ratod (lsptab[pos].inh2);

       lsp.me_einh_kun3 = atoi (lsptab[pos].me_einh_kun3);
	   if (lsp.me_einh_kun3 < 0) lsp.me_einh_kun3 = 2;
       lsp.auf_me3      = ratod (lsptab[pos].auf_me3);
       lsp.inh3         = ratod (lsptab[pos].inh3);

       strcpy (ptabn.ptitem,"sap_kond_art");
       lsp.a_grund = ratod (lsptab[pos].a_grund);
       strcpy (lsp.kond_art, lsptab[pos].kond_art0);
       lsp.posi_ext  = atol (lsptab[pos].posi_ext);
       strcpy (lsp.ls_charge, lsptab[pos].ls_charge);
       lsp.ls_pos_kz = lsptab[pos].ls_pos_kz;

		if (strcmp (clipped (lsptab[pos].lief_me_bz_ist), " ") <= 0)
		{
			memcpy (&lsps, &lsptab[pos], sizeof (lsps));
			_a_bas.a = ratod (lsps.a);
			GetMeEinhIst ();
			memcpy (&lsptab[pos], &lsps, sizeof (lsps));
		}

		strcpy (lsp.lief_me_bz_ist, lsptab[pos].lief_me_bz_ist);
        lsp.me_einh_ist = lsptab[pos].me_einh_ist;
		lsp.me_ist = lsptab[pos].me_ist;
		lsp.inh_ist = lsptab[pos].inh_ist;
        lsp.hbk_date = lsptab[pos].hbk_date;  
		lsp.lief_me1 = lsptab[pos].lief_me1;
		lsp.lief_me2 = lsptab[pos].lief_me2;
		lsp.lief_me3 = lsptab[pos].lief_me3;
		lsp.auf_me_vgl = lsptab[pos].auf_me_vgl;
		lsp.teil_smt   = atoi (lsptab[pos].teil_smt);

		if (lsp.hbk_date <= 0l)
		{
			lese_a_bas (lsp.a);
			char datum [12];
		    if (CalcMhd == LIEFERDATUM)
			{
				dlong_to_asc (lsk.lieferdat, datum);
				datplushbk (datum);
			}
			else
			{
				sysdate (datum);
				datplushbk (datum);
			}
			lsp.hbk_date  = dasc_to_long (datum);
		}
		if (lsp.hbk_date <= 0l)
		{
			memcpy (&lsp.hbk_date, LONGNULL, sizeof (LONGNULL));
		}


		if (ls_mhd_par) lsp.hbk_date = dasc_to_long (lsptab[pos].mhd);  //in diesem Fall wird nur manuell eingegeben

	   strcpy (lsp.ls_ident, lsptab[pos].ls_ident);
	   lsp.aufschlag = (short) atoi (lsptab[pos].aufschlag);
	   lsp.aufschlag_wert = ratod (lsptab[pos].aufschlag_wert);
	   lsp.nve_posi = atoi (lsptab[pos].nve_posi);

	   if ( _a_bas.a != lsp.a )	// extra nachlesen wegen Hinsemann : in der rosi war jede bearbeitete Position == 3  050912
					lese_a_bas (lsp.a);
	   if ( lsp.lief_me != 0 && _a_bas.a_typ == LEERGUT && lsp.pos_stat < 4)
		   lsp.pos_stat = 3 ;	// kompatibel wegen Hinsemann
	   // 050912 : btw.: ist total unsauber : pos_stat ist vorher NICHT initialisiert und alle nachfolgenden stehen dann irgendwie doof rum ....
 
	   
	   
	   if (LeergutAnnahme && (lsp.a >= LeergutVon &&
                              lsp.a <= LeergutBis))
	   {
	          if (lsp.pos_stat < 4) lsp.pos_stat = 3;
	   }
	   else if (lsp.pos_stat < 3)
	   {
	          lsp.pos_stat = 2;
	   }
	   lsp.pos_txt_kz = lsptab[pos].pos_txt_kz;
	   lsp.tara = lsptab[pos].tara;
       testdecval ();
	   if (LeergutAnnahme && 
		   lsp.a >= LeergutVon && lsp.a <= LeergutBis)
	   {
		   lsp.lief_me *= -1;
	   }
       ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls,
                             lsp.a, lsp.posi);
       pr_vk = ratod (lsptab[pos].ls_vk_pr);
//       StndAuf.UpdStdAuftrag (lsp.mdn, lsp.fil, lsk.kun, lsk.kun_fil, lsp.a, pr_vk);
       if (Getib ())
       {
           UpdateBsd ();
       }
	   if (lsp.pos_stat < 4) lsp.pos_stat = 2 ;	// 050912 : damit es wenigstens wieder von vorne losgeht
//       WriteAufkun ();

}

int AUFPLIST::WriteAktPos ()
{
	   if (PosSave == 0)
	   {
		   return 0;
	   }
	   int pos = eListe.GetAktRow ();
       memcpy (&lsptab[pos], &lsps, sizeof (struct LSPS));
       WritePos (pos);
	   return 1;
}


BOOL AUFPLIST::TestLiefMe0 ()
{
	if (cfg_TestLiefMe0 == FALSE) return FALSE;
    int recs = eListe.GetRecanz ();
    for (int i = 0; i < recs; i ++)
	{
       if (ratod (lsptab[i].lief_me) == 0.0)
	   {
		   return TRUE;
	   }
	}
	return FALSE;
}

int AUFPLIST::ErfasseLeergut (void)
{
	int i;
	BOOL save_warning;
	int save_ls_stat;
	double lief_me;
	double save_lief_me; 
     BOOL dspeichern =  LeergClass.EnterLeerg (AktivWindow); //testtesttest 
	 if (dspeichern == FALSE)  return 0;

	  save_warning = PosDeleteWarning;
	  save_ls_stat = lsk.ls_stat;
	  save_lief_me = akt_lief_me;
	  lsk.ls_stat = 4;
	  PosDeleteWarning = FALSE;

	  int recs = eListe.GetRecanz ();

	  for (i = 0; i < recs; i ++)   // zum reduzieren von kun_leerg
	  {
		    memcpy (&lsps, &lsptab[i], sizeof (struct LSPS));
			if (strcmp (lsps.erf_kz, "L") == 0)
			{
				akt_lief_me = (double) 0;
                lief_me = ratod(lsps.lief_me) * -1;
		        BucheBsd (ratod (lsps.a), lief_me, atoi (lsps.me_einh_kun),
                  ratod (lsps.ls_vk_pr));
			}

	  }

	  for (i = 0; i < recs; i ++)
	  {
		    memcpy (&lsps, &lsptab[i], sizeof (struct LSPS));
			if (strcmp (lsps.erf_kz, "L") == 0)
			{
				akt_lief_me = (double) 0;
		        eListe.SetPos (i,0);   //171214  AktRow setzten 

				DeleteLine ();
			}
	  }


	 for (i = 0; LeerPosArt[i].lanz; i ++)
	 {
	    AppendLine ();
	    sprintf (lsps.lief_me, "%d", LeerPosArt[i].lanz);
	    sprintf (lsps.a, "%.0lf", LeerPosArt[i].lart);
	    lsps.pos_stat = 3;
	    strcpy (lsps.erf_kz, "L");
		int row = eListe.GetAktRow ();
	    memcpy (&lsptab[eListe.GetAktRow ()], &lsps, sizeof (struct LSPS));
	    fetcha ();
	    testme ();
	    AnzAufWert ();
	    AnzAufGew ();
		akt_lief_me = (double) 0;
        lief_me = ratod(lsps.lief_me) ;
        BucheBsd (ratod (lsps.a), (double) lief_me , atoi (lsps.me_einh_kun),
                 ratod (lsps.ls_vk_pr));
	 }
	  akt_lief_me = save_lief_me;
	  PosDeleteWarning = save_warning;
	  lsk.ls_stat = save_ls_stat;
	 return 0;
}

int AUFPLIST::WriteAllPos (void)
/**
Alle Positionen schreiben.
**/
{
    int i;
    int recs;
    int row;

	if ((cfg_LeergutErfassen || boes_par) && syskey != KEY5 ) ErfasseLeergut ();

    row     = eListe.GetAktRow ();

    if (TestRow () == -1)
    {

            eListe.SetFeldFocus0 (eListe.GetAktRow (),
                                  eListe.GetAktColumn ());
            return -1;
    }

    if (ratod (lsps.a))
	{
		if (strcmp (lsps.erf_kz, "L") != 0)     //Leergut aus erfasseleerg wird extra behandelt
		{
		  WaWiePro.erf_kz = "H";
          BucheBsd (ratod (lsps.a), ratod (lsps.lief_me), atoi (lsps.me_einh_kun),
              ratod (lsps.ls_vk_pr));
		}
	}
    memcpy (&lsptab[row], &lsps, sizeof (struct LSPS));
    recs = eListe.GetRecanz ();
    ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
    if (Getib ())
    {
            DeleteBsd ();
    }
    GenNewPosi ();
/*
	if (IsPartyService)
	{
		if (ContainsService ())
		{
			lsk.psteuer_kz = Voll;
		}
		else
		{
			lsk.psteuer_kz = Standard;
		}
	}
*/
    for (i = 0; i < recs; i ++)
    {
        WritePos (i);
    }
	StndAuf.CleanUpdError ();
    eListe.BreakList ();
    return 0;
}


void AUFPLIST::SaveAuf (void)
{
    save_fkt (5);
    save_fkt (6);
    save_fkt (7);
    save_fkt (8);
    save_fkt (12);
}

void AUFPLIST::SetAuf (void)
{
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);
    set_fkt (AppendLine, 8);
    set_fkt (WriteAllPos, 12);
}

void AUFPLIST::RestoreAuf (void)
{
    restore_fkt (5);
    restore_fkt (6);
    restore_fkt (7);
    restore_fkt (8);
    restore_fkt (12);
}


int AUFPLIST::dokey5 (void)
/**
Listenerfassung abbrechen.
**/
{
	short sql_sav;
	extern short sql_mode;

//    if (abfragejn (mamain1, "Positionen speichern ?", "J"))
    {
           WriteAllPos ();
           syskey = KEY12;
		   return 0;
    }

	sql_sav = sql_mode;
	sql_mode = 1;
/*
    DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
    DbClass.sqlin ((short *) &lsk.fil, 1, 0);
    DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
    DbClass.sqlcomm ("delete from lsp where mdn = ? and fil = ? "
                     "and ls = ? and lief_me = 0");
*/

	sql_mode = sql_sav;
    syskey = KEY5;
    RestoreAuf ();

    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());

    eListe.BreakList ();
    return 1;
}

void AUFPLIST::DoBreak (void)
{
	short sql_sav;
	extern short sql_mode;

	sql_sav = sql_mode;
	sql_mode = 1;
    DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
    DbClass.sqlin ((short *) &lsk.fil, 1, 0);
    DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
    DbClass.sqlcomm ("delete from lsp where mdn = ? and fil = ? "
                     "and ls = ? and lief_me = 0");

	sql_mode = sql_sav;
    syskey = KEY5;
    RestoreAuf ();

    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());

    eListe.BreakList ();
}


void AUFPLIST::InitSwSaetze (void)
{
       int i;

       for (i = 0; i < MAXPOS; i ++)
       {
               SwSaetze [i] = (char *) &lsptab [i];
       }
}

void AUFPLIST::SetRecHeight (void)
/**
Anzahl Zeilen pro Listzeile ermitteln.
**/
{
       int height;
       int i;

       height = 0;
       for (i = 0; i < dataform.fieldanz; i ++)
       {
           if (dataform.mask[i].attribut & REMOVED) continue;
           if (dataform.mask[i].pos[0] > height)
           {
               height = dataform.mask[i].pos[0];
           }
       }
       height ++;
       eListe.SetRecHeight (height);
}

int AUFPLIST::ToMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       eListe.SetRecanz (pos + 1);
       return 0;
}


void AUFPLIST::SetStringEnd (char *feldname, int len)
/**
Stringende Setzen.
**/
{

       return;
}


void AUFPLIST::uebertragen (void)
/**
Eingabesatz in ASCII-Struktur uebertragen.
**/
{
	   static int cursor = -1;

	   if (cursor == -1 && (GetFieldAttr ("a_kun") & REMOVED) == 0)
	   {
                 DbClass.sqlin ((short *) &lsk.mdn,    1, 0);
                 DbClass.sqlin ((short *) &lsk.fil,    1, 0);
                 DbClass.sqlin ((long *)  &lsk.kun,   2, 0);
                 DbClass.sqlin ((char *)  lsps.a, 0,14);
                 DbClass.sqlout ((char *) lsps.a_kun, 0,13);
                 cursor = DbClass.sqlcursor ("select a_kun from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? "
                                     "and   a = ?");
	   }
       lese_a_bas (lsp.a);
	   lsps.a_typ = _a_bas.a_typ;
	   lsps.mwst = _a_bas.mwst;
	   lsps.a_leih = _a_bas.a_leih;

	   if (LeergutAnnahme && 
		   lsp.a >= LeergutVon && lsp.a <= LeergutBis)
	   {
		   lsp.lief_me *= -1;
	   }
       sprintf (lsps.posi,        "%4ld",    lsp.posi);
       sprintf (lsps.sa_kz_sint,  "%1hd",    lsp.sa_kz_sint);
       sprintf (lsps.a,           "%13.0lf", lsp.a);
       sprintf (lsps.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (lsps.a_bz2,       "%s",      _a_bas.a_bz2);
       sprintf (lsps.last_me,     "%8.3lf",  0,0);
       sprintf (lsps.auf_me,      "%8.3lf",  lsp.auf_me);
       sprintf (lsps.lief_me,     "%8.3lf",  lsp.lief_me);
       sprintf (lsps.rab_satz,    "%5.2lf",  lsp.rab_satz);
       sprintf (lsps.prov_satz,    "%5.2lf", lsp.prov_satz);
       sprintf (lsps.me_bz, "%s",            lsp.auf_me_bz);
       sprintf (lsps.ls_vk_dm,  "%lf",   lsp.ls_vk_pr);
       sprintf (lsps.ls_lad_dm, "%lf",   lsp.ls_lad_pr);
       sprintf (lsps.ls_vk_euro,  "%lf",   lsp.ls_vk_euro);
       sprintf (lsps.ls_lad_euro, "%lf",   lsp.ls_lad_euro);
       sprintf (lsps.ls_vk_fremd,  "%lf",   lsp.ls_vk_fremd);
       sprintf (lsps.ls_lad_fremd, "%lf",   lsp.ls_lad_fremd);
	   FillAktWaehrung ();
       sprintf (lsps.basis_me_bz, "%s",      lsp.lief_me_bz);
       sprintf (lsps.me_einh_kun, "%hd",     lsp.me_einh_kun);
       sprintf (lsps.me_einh,     "%hd",     lsp.me_einh);
       sprintf (lsps.lsp_txt,    "%ld",     lsp.lsp_txt);

       sprintf (lsps.me_einh_kun1,"%hd",     lsp.me_einh_kun1);
       sprintf (lsps.auf_me1,     "%8.3lf",  lsp.auf_me1);
       sprintf (lsps.inh1,        "%8.3lf",  lsp.inh1);

       sprintf (lsps.me_einh_kun2,"%hd",     lsp.me_einh_kun2);
       sprintf (lsps.auf_me2,     "%8.3lf",  lsp.auf_me2);
       sprintf (lsps.inh2,        "%8.3lf",  lsp.inh2);

       sprintf (lsps.me_einh_kun3,"%hd",     lsp.me_einh_kun3);
       sprintf (lsps.auf_me3,     "%8.3lf",  lsp.auf_me3);
       sprintf (lsps.inh3,        "%8.3lf",  lsp.inh3);
       sprintf (lsps.kond_art,    "%s",     lsp.kond_art);
       sprintf (lsps.kond_art0,   "%s",     lsp.kond_art);
       sprintf (lsps.posi_ext, "%ld", lsp.posi_ext);
       sprintf (lsps.ls_charge, "%s", lsp.ls_charge);

	   strcpy (lsps.lief_me_bz_ist, clipped (lsp.lief_me_bz_ist));
       lsps.me_einh_ist = lsp.me_einh_ist;
	   lsps.me_ist = lsp.me_ist;
	   lsps.inh_ist = lsp.inh_ist;
	   lsps.hbk_date = lsp.hbk_date;
	   dlong_to_asc (lsp.hbk_date, lsps.mhd);;
	   lsps.lief_me1 = lsp.lief_me1;
	   lsps.lief_me2 = lsp.lief_me2;
	   lsps.lief_me3 = lsp.lief_me3;
	   lsps.auf_me_vgl = lsp.auf_me_vgl;

	   strcpy (lsps.ls_ident, lsp.ls_ident);
	   sprintf (lsps.aufschlag, "%hd", lsp.aufschlag);
	   sprintf (lsps.aufschlag_wert, "%.3lf", lsp.aufschlag_wert);
	   sprintf (lsps.nve_posi, "%ld", lsp.nve_posi);

       lsps.charge_gew = 0.0;
       FillKondArt (lsps.kond_art);
       lsps.ls_pos_kz = lsp.ls_pos_kz;
       sprintf (lsps.a_grund, "%4.0lf", lsp.a_grund);
	   lsps.pos_stat = lsp.pos_stat;
	   strcpy (lsps.erf_kz, lsp.erf_kz);
	   lsps.pos_txt_kz = lsp.pos_txt_kz;
	   lsps.tara = lsp.tara;

       strcpy (lsps.a_kun, " ");
       GetLastMe (ratod (lsps.a));
	   if (cursor != -1 && (GetFieldAttr ("a_kun") & REMOVED) == 0)
	   {
	           DbClass.sqlopen (cursor);
	           DbClass.sqlfetch (cursor);
	   }
       ReadChargeGew ();
	   SetChargeAttr ();
}


void AUFPLIST::AfterPaint (HDC hdc, RECT *rect, int pos, int SubRow)
{
	if (SubRow != 0) return;

	if (NoteIcon != NULL && atol (lsptab[pos].lsp_txt) != 0l)
	{
		int plus = max (0, (rect->bottom - rect->top - 16) / 2);
		int y = rect->top + plus; 
		BitMap.DrawBitmap (hdc, NoteIcon, rect->left + 1, y);
	}
}

void AUFPLIST::ShowDB (short mdn, short fil, long ls)
/**
Auftragspositionen lesen und anzeigen.
**/
{
        int i;
        HCURSOR oldcursor;
        extern short do_exit;
        int dsqlstatus;


        InitSwSaetze ();
        eListe.SetRecanz (0);
        i = eListe.GetRecanz ();
        eListe.SetUbForm (&ubform);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));

        if (ListeSortierung == 0) 
		{
			dsqlstatus = ls_class.lese_lsp (mdn, fil, ls, "order by posi");
		}
		else
		{
			dsqlstatus = ls_class.lese_lsp (mdn, fil, ls, "order by lsp.a");
		}

        while (dsqlstatus == 0)
        {
                     uebertragen ();
                     if (ToMemory (i) != 0) break;
                     i = eListe.GetRecanz ();
                     if (i >= MAXPOS) break;
                     dsqlstatus = ls_class.lese_lsp ();
        }
        if (ListeSortierung) GenNewPosi0 ();

        SetRecHeight ();
        SetCursor (oldcursor);
        i = eListe.GetRecanz ();

        eListe.SetRecanz (i);
        SwRecs = eListe.GetRecanz ();

        SetFieldAttr ("a", DISPLAYONLY);
        eListe.SetDataForm0 (&dataform, &lineform);
        eListe.SetSaetze (SwSaetze);
        eListe.SetChAttr (ChAttr);
        eListe.SetUbRows (ubrows);
        if (i == 0)
        {
			     Posanz = 0;
				 memcpy (&lsps, &lsps_null, sizeof (struct LSPS));
                 eListe.AppendLine ();
				 AktRow = AktColumn = 0;
                 eListe.SetPos (0, 0);
                 i = eListe.GetRecanz ();
        }
		else
		{
			     Posanz = i;
		}
		if (Posanz > 0 && NewPosition)
		{
			     AktRow = Posanz - 1;
                 eListe.SetPos (AktRow, 0);
				 memcpy (&lsps, &lsps_null, sizeof (struct LSPS));
// WAL-158 Position war dadurch doppelt               eListe.AppendLine ();
		}
        SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
        InvalidateRect (eListe.Getmamain3 (), 0, FALSE);
        UpdateWindow (eListe.Getmamain3 ());
        memcpy (&lsps, &lsptab[0], sizeof (struct LSPS));
        if (! numeric (lsps.a))
		{
           sprintf (lsps.a, "%.0lf", 0.0);
           memcpy (&lsptab[eListe.GetAktRow()], &lsps, sizeof (struct LSPS));
		}

}


int AUFPLIST::SortByPrice0 (const void *elem1, const void *elem2)
{
	        struct LSPS *el1;
	        struct LSPS *el2;

			el1 = (struct LSPS *) elem1;
			el2 = (struct LSPS *) elem2;
			if (ratod (el1->ls_vk_euro) < ratod (el2->ls_vk_euro))
			{
				return -1;
			}
			if (ratod (el1->ls_vk_euro) > ratod (el2->ls_vk_euro))
			{
				return 1;
			}
			return 0;
}


void AUFPLIST::SortByPrice ()
{
	qsort (lsptab, eListe.GetRecanz (), sizeof (struct LSPS), SortByPrice0);
    InvalidateRect (eListe.Getmamain3 (), 0, FALSE);
    UpdateWindow (eListe.Getmamain3 ());
    memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
    eListe.ShowAktRow ();
	SortFlag = PriceSort;
}


int AUFPLIST::SortByArt0 (const void *elem1, const void *elem2)
{
	        struct LSPS *el1;
	        struct LSPS *el2;

			el1 = (struct LSPS *) elem1;
			el2 = (struct LSPS *) elem2;
			if (ratod (el1->a) < ratod (el2->a))
			{
				return -1;
			}
			if (ratod (el1->a) > ratod (el2->a))
			{
				return 1;
			}
			return 0;
}


void AUFPLIST::SortByArt ()
{
	qsort (lsptab, eListe.GetRecanz (), sizeof (struct LSPS), SortByArt0);
    InvalidateRect (eListe.Getmamain3 (), 0, FALSE);
    UpdateWindow (eListe.Getmamain3 ());
    memcpy (&lsps, &lsptab[eListe.GetAktRow ()], sizeof (struct LSPS));
    eListe.ShowAktRow ();
	SortFlag = ArticleSort;
}


void AUFPLIST::ReadDB (short mdn, short fil, long ls)
/**
Auftragspositionen lesen.
**/
{
        ausgabesatz = (unsigned char *) &lsps;
        zlen = sizeof (struct LSPS);

        PageView = 0;
        eListe.SetPageView (0);
        feld_anz = dataform.fieldanz;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;

        Lstbanz = feld_anz;

        eListe.SetPos (AktRow, AktColumn);
        eListe.Setbanz (feld_anz);
        eListe.Setzlen (zlen);
        eListe.Initscrollpos ();
        eListe.SetAusgabeSatz (ausgabesatz);
}

void AUFPLIST::DestroyWindows (void)
{
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
}

void AUFPLIST::SetSchirm (void)
{
       set_fkt (Schirm, 11);
       SetFkt (11, vollbild, KEY11);
}


void AUFPLIST::SetNoRecNr (void)
{
	   static BOOL SetOK = FALSE;
	   int i;

	   if (SetOK) return;

	   for (i = 0; i < dataform.fieldanz; i ++)
	   {
		   dataform.mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform.fieldanz; i ++)
	   {
		   ubform.mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform.fieldanz; i ++)
	   {
		   lineform.mask[i].pos[1] -= 6;
	   }
	   eListe.SetNoRecNr (TRUE);
       SetOK = TRUE;
}

void AUFPLIST::FillWaehrungBz (void)
{
       static BOOL WaeOK = FALSE;
       char ptwert [5];

       if (WaeOK) return;

       WaeOK = TRUE;
       sprintf (ptwert, "%hd", 1);
       if (ptab_class.lese_ptab ("waehrung", ptwert) == 0)
       {
           clipped (ptabn.ptbezk);
           VK_DM = new char [20];
           LD_DM = new char [20];
           if (VK_DM != NULL)
           {
                 sprintf (VK_DM, " VK %s", ptabn.ptbezk);
           }
           else
           {
                 VK_DM     = " VK EURO  ";
           }
           if (LD_DM != NULL)
           {
                 sprintf (LD_DM, " LD %s", ptabn.ptbezk);
           }
           else
           {
                  LD_DM     = " LD EURO  ";
           }
       }
       sprintf (ptwert, "%hd", 2);
       if (ptab_class.lese_ptab ("waehrung", ptwert) == 0)
       {
           clipped (ptabn.ptbezk);
           VK_EURO = new char [20];
           LD_EURO = new char [20];
           if (VK_EURO != NULL)
           {
                 sprintf (VK_EURO, " VK %s", ptabn.ptbezk);
           }
           else
           {
                 VK_EURO     = " VK DM  ";
           }
           if (LD_EURO != NULL)
           {
                 sprintf (LD_EURO, " LD %s", ptabn.ptbezk);
           }
           else
           {
                  LD_EURO     = " LD DM  ";
           }
       }

       sprintf (ptwert, "%hd", 3);
       if (ptab_class.lese_ptab ("waehrung", ptwert) == 0)
       {
           clipped (ptabn.ptbezk);
           VK_FREMD = new char [20];
           LD_FREMD = new char [20];
           if (VK_FREMD != NULL)
           {
                 sprintf (VK_FREMD, " VK %s", ptabn.ptbezk);
           }
           else
           {
                 VK_FREMD     = " VK FREMD  ";
           }
           if (LD_FREMD != NULL)
           {
                 sprintf (LD_FREMD, " LD %s", ptabn.ptbezk);
           }
           else
           {
                  LD_FREMD     = " LD FREMD  ";
           }
       }
}


void AUFPLIST::SetPrPicture (void)
{
       int fpos;

       fpos = GetItemPos (&dataform, "pr_vk");
       if (fpos == -1) return;

       if (nachkpreis == 0)
       {
           dataform.mask[fpos].picture = "%4.0f";
       }
       else if (nachkpreis == 1)
       {
           dataform.mask[fpos].picture = "%6.1f";
       }
       else if (nachkpreis == 3)
       {
           dataform.mask[fpos].picture = "%7.3f";
       }

       else if (nachkpreis == 4)
       {
           dataform.mask[fpos].picture = "%8.4f";
       }
}


void AUFPLIST::ShowLsp (short mdn, short fil, long ls,
                         short kunfil, long kun, char *ldat)
/**
Auftragsliste bearbeiten.
**/

{
	   int pos;

       if (ListAktiv) return;

	   eListe.RowEvent = this;
       FillWaehrungBz ();
       if (lsk.waehrung == 0)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD);
				 }
	   }
       else if (lsk.waehrung == 1)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_DM);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_DM);
				 }
	   }
       else if (lsk.waehrung == 2)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_EURO);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_EURO);
				 }
	   }
	   else
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_FREMD);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_FREMD);
				 }
	   }

       Geta_bz2_par ();
       Geta_kum_par ();
	   GetCfgValues ();
	   int AufMePos = GetItemPos (&dataform, "auf_me");
	   if (AufMePos == -1)
	   {
			dataform.mask[AufMePos].after = NULL;
	   }
	   if (EnterAufMe)
	   {
		   SetFieldAttr ("auf_me", EDIT);
		   if (AufMePos != -1)
		   {
				dataform.mask[AufMePos].after = GetAufMeVglEx;
		   }
	   }

       strcpy (sys_par.sys_par_nam,"boes_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
            boes_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam,"nachkpreis");
       if (sys_par_class.dbreadfirst () == 0)
       {
            nachkpreis = atoi (sys_par.sys_par_wrt);
            SetPrPicture ();
       }
       strcpy (sys_par.sys_par_nam,"lsc2_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
            lsc2_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam,"lsc3_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
            lsc3_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam,"pos_txt_kz_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
		     int value =  atoi (sys_par.sys_par_wrt);
             if (value == 0)
			 {
				 TxtByDefault = TRUE;
			 }
			 else
			 {
				 TxtByDefault = FALSE;
			 }
       }
	   if (AddLastLief)
	   {
	        addLastLdat ("lief_me");
	   }
	   if (AddLastVkPr)
	   {
	        addLastVkPr ("lief_me");
	   }
	   if (FormOK == FALSE)
	   {
           if (DelLadVK)
		   {
			     DelListField ("ld_pr");
		   }
           if (DelPrVK)
		   {
			     DelListField ("pr_vk");
		   }
           if (DelLastMe)
		   {
			     DelListField ("last_me");
		   }
           if (DelAufMe)
		   {
			     DelListField ("auf_me");
			     DelListField ("auf_me_bz");
		   }
		   FormOK = TRUE;
	   }
       lsk.mdn = mdn;
       lsk.fil = fil;
       lsk.ls = ls;
       lsk.kun = kun;
       lsk.lieferdat = dasc_to_long (ldat);

   	   add = FALSE;
	   aufme_old = (double) 0.0;
	   DestroyPlus ();
       SetSchirm ();
       eListe.SetInfoProc (InfoProc);
       sprintf (InfoCaption, "Lieferschein %ld", ls);
       mamain1 = CreateMainWindow ();
       eListe.InitListWindow (mamain1);
       ReadDB (mdn, fil, ls);

       eListe.SetListFocus (0);
       eListe.Initscrollpos ();
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       ShowDB (mdn, fil, ls);

       eListe.SetRowItem ("a", lsptab[0].a);

       ListAktiv = 1;
	   AnzAufWert ();

       return;
}

void AUFPLIST::GetListColor (COLORREF *color, char *cfg_v)
/**
Farbunterlegung fuer Listen uebertragen.
**/
{
	static char *ColTxt[] = {"BLACKCOL",
		                     "WHITECOL",
							 "BLUECOL",
							 "REDCOL",
							 "LTGRAYCOL",
							 "GREENCOL",
							 "YELLOWCOL",
							 "DKYELLOWCOL",
							 NULL};

	static COLORREF ColVal[] = {BLACKCOL,
		                        WHITECOL,
					  		    BLUECOL,
							    REDCOL,
							    LTGRAYCOL,
							    GREENCOL,
							    YELLOWCOL,
								DKYELLOWCOL};
	int i;
	int red, blue, green;
	char *ColR;
	int Cols = 0;

	for (i = 0; ColTxt[i]; i ++)
	{
		if (strcmp (ColTxt[i], cfg_v) == 0)
		{
			*color = ColVal[i];
			return;
		}
	}


	ColR = strstr (cfg_v, "red");
	if (ColR)
	{
		red = atoi (&ColR[3]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "blue");
	if (ColR)
	{
		blue = atoi (&ColR[4]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "green");
	if (ColR)
	{
		green = atoi (&ColR[5]);
		Cols ++;
	}
	if (Cols == 3)
	{
		*color = RGB (red, green, blue);
	}

}

void AUFPLIST::SetPreisTest (int mode)
{
	if (mode == 0) return;

	preistest = min (4, max (1, mode));
	if (preistest == 3)
	{
		SetItemAttr (&dataform, "pr_vk", DISPLAYONLY);
	}
}


void AUFPLIST::SetLadPreisTest (int mode)
{
	if (mode == 0) return;

	ladpreistest = min (4, max (1, mode));
	if (ladpreistest == 3)
	{
		SetItemAttr (&dataform, "lad_pr", DISPLAYONLY);
	}
}


void AUFPLIST::GetCfgValues (void)
/**
Werte aus 53100.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [512];

	   if (cfgOK) return;

	   cfgOK = TRUE;
       if (ProgCfg.GetCfgValue ("SmtTest", cfg_v) == TRUE)
	   {
					 SmtTest = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("TestLiefMe0", cfg_v) == TRUE)
	   {
					 cfg_TestLiefMe0 = atoi (cfg_v);
	   }

       if (ProgCfg.GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg.GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg.GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("LeergutErfassen", cfg_v) == TRUE)
       {
                    cfg_LeergutErfassen = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("plu_size", cfg_v) ==TRUE)
       {
                    plu_size = atoi (cfg_v);
       }
       else
        {
                    plu_size = 0;
        }
        if (ProgCfg.GetCfgValue ("lief_me_default", cfg_v) == TRUE)
        {
                    lief_me_default = atoi (cfg_v);
        }
        else
        {
                    lief_me_default = 0;
        }
        if (ProgCfg.GetCfgValue ("searchadirect", cfg_v) == TRUE)
		{
			        searchadirect = min (1, atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("searchmodedirect", cfg_v) == TRUE)
		{
			        searchmodedirect = min (1, atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg.GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
        if (ProgCfg.GetCfgValue ("EditAufMeEnabled", cfg_v) == TRUE)
		{
			        EditAufMeEnabled = atoi (cfg_v);
		}
		eListe.SetRowHeight (RowHeight);
/*
        if (ProgCfg.GetCfgValue ("listfocus", cfg_v) == TRUE)
		{
			        ListFocus = min (4, atoi (cfg_v));
					ListFocus = max (3, ListFocus);
		}
*/
        if (ProgCfg.GetCfgValue ("matchcode", cfg_v) == TRUE)
		{
			         matchcode = atoi (cfg_v);
			         SetMatchCode (atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("bsd_kz", cfg_v) == TRUE)
		{
			         bsd_kz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("rab_prov_kz", cfg_v) == TRUE)
		{
			         rab_prov_kz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("auf_wert_anz", cfg_v) == TRUE)
		{
			         auf_wert_anz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("auf_gew_anz", cfg_v) == TRUE)
		{
			         auf_gew_anz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("ChargeZwang", cfg_v) == TRUE)
		{
			         ChargeZwang = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("EnableChargeChange", cfg_v) == TRUE)
		{
			         EnableChargeChange = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("a_kun_smt", cfg_v) == TRUE)
		{
			         a_kun_smt = atoi (cfg_v);
					 if (a_kun_smt == 2)
					 {
						QClass.testa_kun = Testa_kun;		
					 }
		}

        if (ProgCfg.GetCfgValue ("LISTCOLORS", cfg_v) == TRUE)
        {
		             ListColors =  min (1, max (0, atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("KOMPFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompfColor, cfg_v);
        }

        if (ProgCfg.GetCfgValue ("KOMPBCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompbColor, cfg_v);
        }
        if (ProgCfg.GetCfgValue ("SAFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SafColor, cfg_v);
					 MessCol = SafColor;
        }
        if (ProgCfg.GetCfgValue ("SABCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SabColor, cfg_v);
					 MessBkCol = SafColor;
        }
        if (ProgCfg.GetCfgValue ("sortstnd", cfg_v) == TRUE)
        {
		             StndAuf.SetSortMode (atoi (cfg_v));
        }
        if (ProgCfg.GetCfgValue ("preistest", cfg_v) == TRUE)
        {
		             SetPreisTest (atoi (cfg_v));
        }
        if (ProgCfg.GetCfgValue ("ladpreistest", cfg_v) == TRUE)
        {
		             SetLadPreisTest (atoi (cfg_v));
        }
        if (ProgCfg.GetCfgValue ("sacreate", cfg_v) == TRUE)
        {
		             sacreate = min (1, max (0, (atoi (cfg_v))));
        }
        if (ProgCfg.GetCfgValue ("lad_vk", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v))
					 {
						 DelLadVK = FALSE;
					 }

                     if (atoi (cfg_v) == 2)
                     {
   		                  SetItemAttr (&dataform, "ld_pr", EDIT);
                     }

        }
        if (ProgCfg.GetCfgValue ("pr_vk", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v) == 0)
					 {
						 DelPrVK = TRUE;
					 }

                     if (atoi (cfg_v) == 1)
                     {
   		                  SetItemAttr (&dataform, "pr_vk", DISPLAYONLY);
                          VkAttribut = DISPLAYONLY;
                     }

        }
        if (ProgCfg.GetCfgValue ("last_me", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v))
					 {
						 DelLastMe = FALSE;
					 }
        }
        if (ProgCfg.GetCfgValue ("auf_me", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v) == 0)
					 {
						 DelAufMe = TRUE;
					 }
        }
        if (ProgCfg.GetCfgValue ("last_ldat", cfg_v) == TRUE)
        {
			         AddLastLief = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("LastFromAufKun", cfg_v) == TRUE)
        {
			         LastFromAufKun = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("last_pr_vk", cfg_v) == TRUE)
        {
			         AddLastVkPr = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("proptimize", cfg_v) == TRUE)
        {
			          WaPreis.SetOptimize (atoi (cfg_v));
        }
        if (ProgCfg.GetGroupDefault ("pr_alarm", cfg_v) == TRUE)
        {
                      prproz_diff = ratod (cfg_v);
        }
        if (ProgCfg.GetGroupDefault ("leergut_annahme", cfg_v) == TRUE)
        {
			LeergutAnnahme = (atoi (cfg_v) == 0) ? FALSE : TRUE;
        }
        if (ProgCfg.GetGroupDefault ("leergut_von", cfg_v) == TRUE)
        {
                      LeergutVon = ratod (cfg_v);
        }
        if (ProgCfg.GetGroupDefault ("leergut_bis", cfg_v) == TRUE)
        {
                      LeergutBis = ratod (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("art_un_tst", cfg_v) == TRUE)
        {
		             art_un_tst =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("add_me", cfg_v) == TRUE)
        {
		             add_me =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("a_kum", cfg_v) == TRUE)
        {
                     a_kum_par =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("preis0_mess", cfg_v) == TRUE)
        {
                     preis0_mess = atoi (cfg_v);
					 if (preis0_mess == 2)
					 {
						QClass.testa_kun = Testa_kun;		
					 }
        }
        if (ProgCfg.GetCfgValue ("dauertief", cfg_v) == TRUE)
        {
                     dauertief = atol (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr ();
					}
		}
        if (ProgCfg.GetCfgValue ("RemoveKondArt", cfg_v) == TRUE)
        {
                    RemoveKondArt = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("RemoveAGrund", cfg_v) == TRUE)
        {
                    RemoveAGrund = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("RemoveEnterCharge", cfg_v) == TRUE)
        {
                    RemoveEnterCharge = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("RemoveAGrund0", cfg_v) == TRUE)
        {
                    RemoveAGrund0 = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("len_a", cfg_v) == TRUE)
        {
		            SetNewLen ("a", atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("picture_a", cfg_v) == TRUE)
        {
		            SetNewPicture ("a", cfg_v);
		}
        if (ProgCfg.GetCfgValue ("len_a_bz1", cfg_v) == TRUE)
        {
		            SetNewLen ("a_bz1", atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("len_me_bz", cfg_v) == TRUE)
        {
		            SetNewLen ("me_bz", atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("len_basis_me_bz", cfg_v) == TRUE)
        {
		            SetNewLen ("basis_me_bz", atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("ld_pr_prim", cfg_v) == TRUE)
		{
			         ld_pr_prim = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("autopfand", cfg_v) == TRUE)
		{
			         autopfand = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("PosSave", cfg_v) == TRUE)
		{
			         PosSave = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("PosSaveMess", cfg_v) == TRUE)
		{
			         PosSaveMess = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("LiefMeDirect", cfg_v) == TRUE)
		{
			         LiefMeDirect = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("meoptimize", cfg_v) == TRUE)
		{
			         meoptimize = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("a_ersatz", cfg_v) == TRUE)
		{
			         a_ersatz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("AErsInLs", cfg_v) == TRUE)
		{
			         AErsInLs = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("AufCharge", cfg_v) == TRUE)
		{
			         AufCharge = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("MeDefault", cfg_v) == TRUE)
		{
			         MeDefault = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("FilStandard", cfg_v) == TRUE)
		{
			         FilStandard = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("MaxScanDays", cfg_v) == TRUE)
		{
			         MaxScanDays = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("NewPosition", cfg_v) == TRUE)
		{
			         NewPosition = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("ListeSortierung", cfg_v) == TRUE) //LOH-27
		{
			         ListeSortierung = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("ScannMessage", cfg_v) == TRUE)
		{
			         ScannMessage = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("LiefMe0", cfg_v) == TRUE)
		{
			         LiefMe0 = atoi (cfg_v);
		}
        if (RemoveKondArt)
		{
			     DelListField ("kond_art");
		}
        if (RemoveAGrund)
		{
			     DelListField ("a_grund");
		}
	    if (ls_charge_par == 0 || (lsc2_par == 0 && lsc3_par == 0))
		{
			     RemoveEnterCharge = TRUE;
		}
        if (RemoveEnterCharge)
		{
			     DelListField ("ls_charge");
		}
	    if (ls_mhd_par == 0)
		{
			     RemoveEnterMHD = TRUE;
		}
		else  RemoveEnterMHD = FALSE;
        if (RemoveEnterMHD)
		{
			     DelListField ("mhd");
		}
        if (RemoveAGrund0)
		{
			     DelListField ("a_grund0");
		}
        if (ProgCfg.GetCfgValue ("TestBranSmt", cfg_v) == TRUE)
		{
			         TestBranSmt = atoi (cfg_v);
		}
       if (ProgCfg.GetCfgValue ("akt_preis", cfg_v) == TRUE)
       {
                     akt_preis = atol (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("RemoveBestMe", cfg_v) == TRUE) //#080903
       {
			     DelListField ("me_bz");
	   }
       if (ProgCfg.GetCfgValue ("CalcMhd", cfg_v) == TRUE)
	   {
			         CalcMhd = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("HbkPlus", cfg_v) ==TRUE)
       {
                    HbkPlus = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("TestChargeMe", cfg_v) ==TRUE)
       {
                    TestChargeMe = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("TxtByDefault", cfg_v) == TRUE)
       {
                     TxtByDefault = atol (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("PosDeleteWarning", cfg_v) == TRUE)
       {
                     PosDeleteWarning = atol (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("ScanMode", cfg_v) == TRUE)
       {
                     ScanMode = atol (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("ChargeFromEan128", cfg_v) == TRUE)
       {
                     ChargeFromEan128 = atol (cfg_v);

       }
       if (ProgCfg.GetCfgValue ("Ean128StartLen", cfg_v) == TRUE)
       {
                     Ean128StartLen = atol (cfg_v);

       }
       Scanean.Read ();
	   if (ScanMode)
	   {
		   int count = 1;
	       int pos = GetItemPos (&dataform, "a");
		   if (pos == -1) pos = 2;
           SetNewPicture ("a", "");
		   ScanColumns[0] = pos;
	       pos = GetItemPos (&dataform, "ls_charge");
		   if (pos != -1)
		   {
				ScanColumns[1] = pos;
				count ++;
		   }
		   eListe.SetEditRenderer (this, ScanColumns, count);
		   eListe.SetChangeChar (ChangeChar);
	   }
}


/*
static char prabval [10];
static char pprovval [10];

static ITEM iprab  ("rab_satz",  prabval,   "Rabatt       ", 0);
static ITEM ipprov ("prov_satz", pprovval,  "Provision    ", 0);

static field _prab1 [] = {
&iprab,       8,  0, 1,10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab1 = {3, 0, 0, _prab1, 0, 0, 0, 0, NULL};

static field _prab2 [] = {
&ipprov,      8,  0, 1,10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab2 = {3, 0, 0, _prab2, 0, 0, 0, 0, NULL};

static field _prab3 [] = {
&iprab,       8,  0, 1, 10, 0, "%5.2f", EDIT, 0, 0, 0,
&ipprov,      8,  0, 2, 10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  4,  4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  4, 21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab3 = {4, 0, 0, _prab3, 0, 0, 0, 0, NULL};

static form *prab;

void AUFPLIST::ChoiseLines (HWND eWindow, HDC hdc)
{
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         TEXTMETRIC tm;
         RECT rect;
         int x, y;
         int cx, cy;

 		 if (eWindow == NULL) return;

		 memcpy (&tm, &textm, sizeof (tm));
         GetClientRect (eWindow, &rect);
         x = tm.tmAveCharWidth / 2;
         y = tm.tmHeight / 2;

         cx = rect.right - tm.tmAveCharWidth / 2;
         cy = rect.bottom - tm.tmHeight / 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         SelectObject (hdc, hPenG);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

         SelectObject (hdc, hPenW);


         y ++;
         x ++;
         cx --;
         cy --;
         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         cx += 2;
         cy += 2;
         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);
}


HWND AUFPLIST::CreateEnter (void)
/?*
Hauptfenster fuer Liste erzeugen.
*?/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        HDC hdc;

        if (hMainWin == NULL) return NULL;

        if (eWindow) return eWindow;

//        eListe.GetTextMetric (&tm);
	    memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.bottom - 16 * tm.tmHeight);
        cx = 40 * tm.tmAveCharWidth;
        x = wrect.left + 2 + (rect.right - cx) / 2;
		if (rab_prov_kz < 3)
		{
                  cy = 7 * tm.tmHeight;
		}
		else
		{
                  cy = 9 * tm.tmHeight;
		}

        eWindow       = CreateWindow (
                                       "ListMain",
//                                       "hListWindow",
                                       "",
                                       WS_DLGFRAME |
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);
        hdc = GetDC (eWindow);
        ChoiseLines (eWindow, hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}


long AUFPLIST::EnterPosRab (void)
/?*
Nummer Eingeben.
*?/
{
          int currentf;

		  save_fkt (5);
		  save_fkt (6);
		  save_fkt (7);
		  save_fkt (8);
		  save_fkt (9);
		  save_fkt (10);
		  save_fkt (11);
		  save_fkt (12);
          set_fkt (EnterBreak, 5);
		  CreateEnter ();
          currentf = currentfield;
		  switch (rab_prov_kz)
		  {
		        case 1 :
			        prab = &prab1;
					break;
		        case 2 :
			        prab = &prab2;
					break;
		        case 3 :
			        prab = &prab3;
					break;
		  }
          sprintf (prabval , "%.2lf", (double) ratod (lsps.rab_satz));
          sprintf (pprovval, "%.2lf", (double) ratod (lsps.prov_satz));
          break_end ();
		  EnableWindows (hMainWin, FALSE);
          enter_form (eWindow, prab, 0, 0);
		  EnableWindows (hMainWin, TRUE);
		  DestroyWindow (eWindow);
		  eWindow = NULL;
          no_break_end ();
          currentfield = currentf;
          eListe.SetFeldFocus0 (eListe.GetAktRow (),
                             eListe.GetAktColumn ());
		  if (syskey != KEY5)
		  {
                   sprintf (lsps.rab_satz, "%.2lf", (double) ratod (prabval));
                   sprintf (lsps.prov_satz, "%.2lf", (double) ratod (pprovval));
                   memcpy (&lsptab[eListe.GetAktRow ()], &lsps, sizeof (struct LSPS));
		  }
		  restore_fkt (5);
		  restore_fkt (6);
		  restore_fkt (7);
		  restore_fkt (8);
		  restore_fkt (9);
		  restore_fkt (10);
		  restore_fkt (11);
		  restore_fkt (12);
          return 0l;
}
*/

static char paufschlval [37];


static int testaufschlag (void);
static int showtestaufschlag (void);


static char prabval [10];
static char pprovval [10];
static char ptext [20];

static ITEM iprab  ("rab_satz",  prabval,              "Rabatt......:", 0);
static ITEM ipaufschlag  ("aufschlag",  paufschlval,   "Aufschlag...:", 0);
static ITEM itext  ("",  ptext,   "", 0);
static ITEM ipprov ("prov_satz", pprovval,             "Provision...:", 0);

static field _prab1 [] = {
&iprab,       8,  0, 1,10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab1 = {3, 0, 0, _prab1, 0, 0, 0, 0, NULL};

static field _prab2 [] = {
&ipprov,      8,  0, 1,10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab2 = {3, 0, 0, _prab2, 0, 0, 0, 0, NULL};

static field _prab3 [] = {
&iprab,       8,  0, 1, 10, 0, "%5.2f", EDIT, 0, 0, 0,
&ipprov,      8,  0, 2, 10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  4,  4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  4, 21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab3 = {4, 0, 0, _prab3, 0, 0, 0, 0, NULL};


char *aufschlagtab = NULL;

combofield prc4[] = {0, 37, 3, (char *) aufschlagtab};  
combofield prc5[] = {1, 37, 3, (char *) aufschlagtab};  
combofield prc6[] = {2, 37, 3, (char *) aufschlagtab};  

combofield *prc = NULL;

static field _prab4 [] = {
&ipaufschlag,  3,  0,  1, 4, 0, "%hd", EDIT, 0, testaufschlag, 0,
&ishow,        2,  0,  1, 21, 0, "", BUTTON, 0,showtestaufschlag ,KEY9,
&itext,        20,  0, 1, 24,0, "", DISPLAYONLY, 0,0 ,0,
&iOK,         15,  0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,         15,  0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static field _prab41 [] = {
&ipaufschlag, 20, 10,  1, 4, 0, "", COMBOBOX | DROPLIST, 0, 0, 0,
&iOK,         15,  0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,         15,  0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

// static form prab4 = {5, 0, 0, _prab4, 0, 0, 0, 0, NULL};
static form prab4 = {3, 0, 0, _prab41, 0, 0, 0, prc4, NULL};

static field _prab5 [] = {
&iprab,      8,  0, 1, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipaufschlag,3,  0, 2, 4, 0, "%hd", EDIT, 0, testaufschlag, 0,
&ishow,        2,  0,  2, 21, 0, "", BUTTON, 0,showtestaufschlag ,KEY9,
&itext,        20,  0, 2, 24,0, "", DISPLAYONLY, 0,0 ,0,
&iOK,        15, 0,  4, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  4,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};



static field _prab51 [] = {
&iprab,       8,  0, 1, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipaufschlag,20, 10, 2, 4, 0, "", COMBOBOX | DROPLIST, 0, 0, 0,
&iOK,        15, 0,  4, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  4,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

// static form prab5 = {6, 0, 0, _prab5, 0, 0, 0, 0, NULL};
static form prab5 = {4, 0, 0, _prab51, 0, 0, 0, prc5, NULL};

static field _prab6 [] = {
&iprab,       8,  0, 1, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipprov,      8,  0, 2, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipaufschlag, 3,  0, 3, 4, 0, "%hd", EDIT, 0, testaufschlag, 0,
&ishow,       2,  0, 3, 21, 0, "", BUTTON, 0,showtestaufschlag ,KEY9,
&itext,       20,  0,3, 24,0, "", DISPLAYONLY, 0,0 ,0,
&iOK,        15, 0,  5,  4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  5, 21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};


static field _prab61 [] = {
&iprab,       8,  0, 1, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipprov,      8,  0, 2, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipaufschlag,20, 10, 3, 4, 0, "", COMBOBOX | DROPLIST, 0, 0, 0,
&iOK,        15, 0,  5,  4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  5, 21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

// static form prab6 = {7, 0, 0, _prab6, 0, 0, 0, 0, NULL};
static form prab6 = {5, 0, 0, _prab61, 0, 0, 0, prc6, NULL};

static form *prab;

static void schreibe_postxt (void)
{
	extern struct LSP_TXT lsp_txt;
	static LSP_TCLASS lsp_tclass;
	extern struct LSPT lst;
	static LSPT_CLASS lspt_class;
       static int prep_ok = 0;
	   int dsqlstatus;
       int zeile;

	long lsptxt = atol (lsps.lsp_txt);


	if (lsptxt == 0l)
	{
		lsptxt = AUFPLIST::GenLspTxt ();
		sprintf (lsps.lsp_txt, "%ld", lsptxt);
		memcpy (&lsptab[eListe.GetAktRow()],
                   &lsps, sizeof (struct LSPS));
	}
   if (wa_pos_txt)
   {
		lsp_txt.mdn = lsk.mdn;
		lsp_txt.fil = lsk.fil;
		lsp_txt.ls = lsk.ls;
		lsp_txt.posi = lsptxt;
       DbClass.sqlin ((short *) &lsp_txt.mdn, 1, 0);
       DbClass.sqlin ((short *) &lsp_txt.fil, 1, 0);
       DbClass.sqlin ((long *) &lsp_txt.ls, 2, 0);
       DbClass.sqlin ((long *) &lsp_txt.posi, 2, 0);
       DbClass.sqlout ((long *) &lsp_txt.zei, 2, 0);
       if (DbClass.sqlcomm ("select max (zei) from lsp_txt where mdn = ? and fil = ? and ls = ? and posi = ?") != 0)
       {
                    lsp_txt.zei = 0;
       }
       else if (DbClass.IsLongnull (lsp_txt.zei))
       {
                    lsp_txt.zei = 0;
       }
	   else
	   {

		   lsp_txt.zei += 1;
	   }
	   strcpy (lsp_txt.txt,ptabn.ptbez);
 	   lsp_tclass.dbupdate();
	   if (lsp_txt.zei > 0)
	   {
		   AUFPLIST::Texte ();
	   }
   }
   else
   {

               if (prep_ok == 0)
			   {
                    lspt.nr = 0;
                    lspt_class.dbreadfirst ();
                    prep_ok = 1;
			   }
		       lspt.nr = lsptxt;
               zeile = 0;
			   lspt.zei = 0;
               dsqlstatus = lspt_class.dbreadfirst ();
               while (dsqlstatus == 0)
			   {
                     dsqlstatus = lspt_class.dbread ();
                     zeile += 1;
                     lspt.zei = zeile;
			   }
			   strcpy (lspt.txt,ptabn.ptbez);
               lspt_class.dbupdate ();
			   if (lspt.zei > 0)
			   {
					AUFPLIST::Texte ();
			   }
   }

}
static void rechne_preis_aufschlag (void)
{
	double vk_pr = 0.0;
	double aufschl = 0.0;
	double aufschl_euro = 0.0;
	double aufschl_proz = 0.0;
	if (ratod(lsps.a) < 1.0) return;
	vk_pr = ratod(lsps.ls_vk_pr);
	aufschl = ratod(lsps.aufschlag_wert);
	if (vk_pr == 0.0) return;
	vk_pr = vk_pr - aufschl;
	aufschl_euro = ratod(ptabn.ptwer1);
	aufschl_proz = ratod(ptabn.ptwer2);
	aufschl = 0.0;
	if (aufschl_proz != 0.0)
	{
		aufschl = vk_pr * aufschl_proz / 100;
		vk_pr = vk_pr + aufschl;
	}
	if (aufschl_euro != 0.0)
	{
		aufschl = aufschl + aufschl_euro;
		vk_pr = vk_pr + aufschl_euro;
	}
	sprintf(lsps.ls_vk_pr,"%.4lf",vk_pr);
	sprintf(lsps.aufschlag_wert,"%.4lf",aufschl);
}

static int testaufschlag (void)
{


        if (ptab_class.lese_ptab ("aufschlag", paufschlval) != 0 && atoi(paufschlval) != 0)
		{
			    wiedereinstieg = 1;
	            no_break_enter ();
				return 0;
		}
		strcpy (ptext, ptabn.ptbez);

        if (syskey == KEY5 || syskey == KEY12)
        {
            break_enter ();
			if (syskey == KEY12)
			{
				if (atoi(paufschlval) != atoi(lsps.aufschlag))
				{
					schreibe_postxt ();
					rechne_preis_aufschlag ();
					strcpy(lsps.aufschlag,paufschlval);
					memcpy (&lsptab[eListe.GetAktRow()],
	                   &lsps, sizeof (struct LSPS));
					eListe.ShowAktRow ();
			}
			}
            return 0;
        }
        if (syskey == KEY9)
        {
		    int dsqlstatus = ptab_class.Show ("aufschlag");
			if (syskey == KEY5)
			{
				wiedereinstieg = 0;
	            break_enter ();
		        return 0;
			}
			strcpy (paufschlval, ptabn.ptwert);
			ptab_class.lese_ptab ("aufschlag", paufschlval);
			strcpy (ptext, ptabn.ptbez);
			if (atoi(paufschlval) != atoi(lsps.aufschlag))
			{
				schreibe_postxt ();
				rechne_preis_aufschlag ();
				strcpy(lsps.aufschlag,paufschlval);
				memcpy (&lsptab[eListe.GetAktRow()],
	                   &lsps, sizeof (struct LSPS));
				eListe.ShowAktRow ();
			}
			wiedereinstieg = 1;
            break_enter ();
	        return 0;

		}

		if (atoi(paufschlval) != atoi(lsps.aufschlag))
		{
			schreibe_postxt ();
			rechne_preis_aufschlag ();
			strcpy(lsps.aufschlag,paufschlval);
			memcpy (&lsptab[eListe.GetAktRow()],
	                   &lsps, sizeof (struct LSPS));
			eListe.ShowAktRow ();
		}
        return 1;
}


static void TestAufschlag (void)
{

        if (syskey != KEY12 && syskey != KEYCR) return;
		
        if (ptab_class.lese_ptab ("aufschlag", paufschlval) != 0 && atoi(paufschlval) != 0)
		{
//			    wiedereinstieg = 1;
	            no_break_enter ();
				return;
		}
//		strcpy (ptext, ptabn.ptbez);

	    if (atoi(paufschlval) != atoi(lsps.aufschlag))
		{
				schreibe_postxt ();
				rechne_preis_aufschlag ();
				strcpy(lsps.aufschlag,paufschlval);
				memcpy (&lsptab[eListe.GetAktRow()],
	                   &lsps, sizeof (struct LSPS));
				eListe.ShowAktRow ();
		}
}


static int showtestaufschlag (void)
{
	syskey = KEY9;
	testaufschlag ();
	return (0);
}

void AUFPLIST::ChoiseLines (HWND eWindow, HDC hdc)
{
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         TEXTMETRIC tm;
         RECT rect;
         int x, y;
         int cx, cy;

 		 if (eWindow == NULL) return;

		 memcpy (&tm, &textm, sizeof (tm));
         GetClientRect (eWindow, &rect);
         x = tm.tmAveCharWidth / 2;
         y = tm.tmHeight / 2;

         cx = rect.right - tm.tmAveCharWidth / 2;
         cy = rect.bottom - tm.tmHeight / 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         SelectObject (hdc, hPenG);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

         SelectObject (hdc, hPenW);


         y ++;
         x ++;
         cx --;
         cy --;
         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         cx += 2;
         cy += 2;
         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);
}


HWND AUFPLIST::CreateEnter (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        HDC hdc;

        if (hMainWin == NULL) return NULL;    
           
        if (eWindow) return eWindow;

//        eListe.GetTextMetric (&tm);
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);
       
        y = (wrect.bottom - 16 * tm.tmHeight);
        cx = 40 * tm.tmAveCharWidth;
        x = wrect.left + 2 + (rect.right - cx) / 2;
		if (rab_prov_kz == 3)
		{
                  cy = 9 * tm.tmHeight;
		}
		else if (rab_prov_kz == 4)
		{
                  cy = 7 * tm.tmHeight;
			      cx = 50 * tm.tmAveCharWidth;
		}
		else if (rab_prov_kz == 5)
		{
                  cy = 9 * tm.tmHeight;
			      cx = 50 * tm.tmAveCharWidth;
		}
		else if (rab_prov_kz == 6)
		{
                  cy = 10 * tm.tmHeight;
			      cx = 50 * tm.tmAveCharWidth;
		}
		else
		{
                  cy = 7 * tm.tmHeight;
		}

        eWindow       = CreateWindow (
                                       "ListMain",
//                                       "hListWindow", 
                                       "",
                                       WS_DLGFRAME | 
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);
        hdc = GetDC (eWindow);
        ChoiseLines (eWindow, hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}


       
static void DisableWindows (HWND hWnd)
/**
Hauptfenster der Anwendung holen.
**/
{

         while (TRUE)
         {
                 EnableWindow (hWnd, FALSE);     
                 hWnd = GetParent (hWnd);
                 if (hWnd == NULL) break;
         }
}

static void EnableWindows (HWND hWnd)
/**
Hauptfenster der Anwendung holen.
**/
{

         while (TRUE)
         {
                 EnableWindow (hWnd, TRUE);     
                 hWnd = GetParent (hWnd);
                 if (hWnd == NULL) break;
         }
}

void AUFPLIST::ReadPtabAufschlag ()
{
	if (aufschlagtab != NULL) return;

	int i = 0;
	int dsqlstatus = ptab_class.lese_ptab_all ("aufschlag");
	while (dsqlstatus == 0)
	{
		i ++;
		dsqlstatus = ptab_class.lese_ptab_all ();
	}

	prc[0].cbanz = i;
	aufschlagtab = new char [(i + 3) * prc[0].cbdim];
	if (aufschlagtab == NULL) return;

	memset (aufschlagtab, 0, (i + 3) * prc[0].cbdim);
	prc[0].cbwerte = aufschlagtab;

	char *pos = aufschlagtab;
	dsqlstatus = ptab_class.lese_ptab_all ("aufschlag");
	while (dsqlstatus == NULL)
	{
		Text t;
		t.Format ("%d %s", atoi (ptabn.ptwert), ptabn.ptbez);
		strncpy (pos, t.GetBuffer (), prc[0].cbdim - 1);
		pos += prc[0].cbdim;
		dsqlstatus = ptab_class.lese_ptab_all ();
	}
}

long AUFPLIST::EnterPosRab (void)
/**
Nummer Eingeben.
**/
{
          int currentf;

		  save_fkt (5);
		  save_fkt (6);
		  save_fkt (7);
		  save_fkt (8);
		  save_fkt (9);
		  save_fkt (10);
		  save_fkt (11);
		  save_fkt (12);
		  set_fkt (NULL, 9);
		  SetFkt (9, leer, 0);
          set_fkt (EnterBreak, 5);
		  CreateEnter ();
          currentf = currentfield;
		  switch (rab_prov_kz)
		  {
		        case 1 :
			        prab = &prab1;
					break;
		        case 2 :
			        prab = &prab2;
					break;
		        case 3 :
			        prab = &prab3;
					break;
		        case 4 :
			        prab = &prab4;
					prc = prc4;
					break;
		        case 5 :
			        prab = &prab5;
					prc = prc5;
					break;
		        case 6 :
			        prab = &prab6;
					prc = prc6;
					break;
		  }
          sprintf (paufschlval , "%hd", (short) atoi (lsps.aufschlag));          
          sprintf (prabval , "%.2lf", (double) ratod (lsps.rab_satz));          
          sprintf (pprovval, "%.2lf", (double) ratod (lsps.prov_satz));          
          if (ptab_class.lese_ptab ("aufschlag", paufschlval) == 0)
		  {
  			strcpy (ptext, ptabn.ptbez);
		  }
		  else
		  {
  			strcpy (ptext, " ");
		  }

          break_end ();
/*
		  EnableWindows (hMainWin, FALSE);
		  EnableWindow (eListe.Getmamain2 (), FALSE);
*/
		  DisableWindows (eListe.Getmamain3 ());

		  if (rab_prov_kz > 3)
		  {
			ReadPtabAufschlag ();
			strcpy (ipaufschlag.feld, aufschlagtab);
			ComboBreak = FALSE;
		  }

          enter_form (eWindow, prab, 0, 0);
		  if (rab_prov_kz > 3)
		  {
			ComboBreak = TRUE;
			Token t;
			t.SetSep (" ");
			t = ipaufschlag.feld;
			if (t.GetAnzToken () > 0)
			{
				  strcpy (paufschlval, t.GetToken (0));
				TestAufschlag ();
			}
		  }

		  EnableWindows (eListe.Getmamain3 ());
		  EnableWindow (eListe.Getmamain2 (), TRUE);
		  EnableWindows (hMainWin, TRUE);
		  DestroyWindow (eWindow);
		  eWindow = NULL;
          no_break_end ();
          currentfield = currentf;
          eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
		  if (syskey != KEY5)
		  {
                   sprintf (lsps.rab_satz, "%.2lf", (double) ratod (prabval));          
                   sprintf (lsps.aufschlag, "%hd", (short) atoi (paufschlval));          
                   sprintf (lsps.prov_satz, "%.2lf", (double) ratod (pprovval));          
                   memcpy (&lsptab[eListe.GetAktRow ()], &lsps, sizeof (struct LSPS));
		  }
		  restore_fkt (5);
		  restore_fkt (6);
		  restore_fkt (7);
		  restore_fkt (8);
		  restore_fkt (9);
		  restore_fkt (10);
		  restore_fkt (11);
		  restore_fkt (12);
		  syskey = 0;
          return 0l;
}
      

void AUFPLIST::SetNewRow (char *item, int row)
{
        int lenu;
        int len;
        int diff;
        int i;
		int datapos;
		int ipos;


        ipos = GetItemPos (&ubform, item);
		if (ipos <= 0) return;

		for (i = ipos; i < dataform.fieldanz; i ++)
		{
		    datapos = dataform.mask[i].pos[1];
			if (datapos >= ubform.mask[ipos].pos[1]) break;
		}

        diff = row - ubform.mask[ipos].pos[1];

		if (i == dataform.fieldanz)
		{
	        datapos = dataform.mask[i].pos[1];
            len  = dataform.mask[i].length + diff;
		}
		else
		{
	        datapos = dataform.mask[i - 1].pos[1];
            len  = dataform.mask[i- 1].length + diff;
		}

        lenu = ubform.mask[ipos - 1].length + diff;

        if (ipos < ubform.fieldanz - 1)
        {
             ubform.mask[ipos].pos[1] = row;
             lineform.mask[ipos - 1].pos[1] = row;

             for ( i = ipos + 1; i < ubform.fieldanz; i ++)
             {
                  ubform.mask[i].pos[1]   += diff;
                  lineform.mask[i - 1].pos[1] += diff;
             }
        }

		for (i = 0; i < dataform.fieldanz; i ++)
		{

			   if (dataform.mask[i].pos[1] == datapos)
			   {
                         dataform.mask[i].length = len;
			   }

			   if (dataform.mask[i].pos[1] > datapos)
			   {
                  dataform.mask[i].pos[1] += diff;
			   }
		}
        ubform.mask[ipos - 1].length = lenu;
}

void AUFPLIST::SetNewPicture (char *item, char *picture)
{
		int ipos;
		char *pic;
        ipos = GetItemPos (&dataform, item);
        if (ipos == -1) return;

		pic = new char (strlen (picture + 2));
		if (pic == NULL) return;

		dataform.mask[ipos].picture = pic;
}


void AUFPLIST::SetNewLen (char *item, int len)
{
        int lenu;
        int diff;
        int i;
		int datapos;
		int ipos;


		if (len == 0) return;
        ipos = GetItemPos (&ubform, item);
        if (ipos == -1) return;

        diff = len - ubform.mask[ipos].length + 2;
		for (i = ipos; i < dataform.fieldanz; i ++)
		{
		    datapos = dataform.mask[i].pos[1];
			if (datapos >= ubform.mask[ipos].pos[1]) break;
		}

		if (i < dataform.fieldanz)
		{
                 datapos = dataform.mask[i].pos[1];
		}

        lenu = ubform.mask[ipos].length + diff;

        if (ipos < ubform.fieldanz - 1)
        {
             for ( i = ipos + 1; i < ubform.fieldanz; i ++)
             {
                  ubform.mask[i].pos[1]   += diff;
                  lineform.mask[i - 1].pos[1] += diff;
             }
        }

		for (i = 0; i < dataform.fieldanz; i ++)
		{

			   if (dataform.mask[i].pos[1] == datapos)
			   {
                         dataform.mask[i].length = len;
			   }

			   if (dataform.mask[i].pos[1] > datapos)
			   {
                  dataform.mask[i].pos[1] += diff;
			   }
		}
        ubform.mask[ipos].length = lenu;
}


void AUFPLIST::EnterLsp (short mdn, short fil, long ls,
                          short kunfil, long kun, char *ldat)
/**
Auftragsliste bearbeiten.
**/

{
       static int initlsp = 0;
	   int pos;
//	   form *savecurrent;

//	   savecurrent = current_form;

	   eListe.RowEvent = this;
       if ((DelPrVK == FALSE) && Muster)
       {
 	         SetItemAttr (&dataform, "pr_vk", DISPLAYONLY);
       }
       else
       {
 	         SetItemAttr (&dataform, "pr_vk", VkAttribut);
       }

       InsCount = 0;
       FillWaehrungBz ();
       strcpy (sys_par.sys_par_nam,"vertr_abr_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
                 vertr_abr_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam,"lutz_mdn_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
                 lutz_mdn_par = atoi (sys_par.sys_par_wrt);
       }

       strcpy (sys_par.sys_par_nam,"lsc2_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
            lsc2_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam,"lsc3_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
            lsc3_par = atoi (sys_par.sys_par_wrt);
       }
       if (lsk.waehrung == 0)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD);
				 }
	   }
       else if (lsk.waehrung == 1)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_DM);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_DM);
				 }
	   }
       else if (lsk.waehrung == 2)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_EURO);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_EURO);
				 }
	   }
	   else
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_FREMD);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_FREMD);
				 }
	   }


       Geta_bz2_par ();
       Geta_kum_par ();
       Getlief_me_pr0 ();
	   GetCfgValues ();
   	   add = FALSE;
	   aufme_old = (double) 0.0;
	   DestroyPlus ();
	   if (FormOK == FALSE)
	   {
           if (DelLadVK)
		   {
		   }
           if (DelLastMe)
		   {
			     DelListField ("last_me");
		   }
           if (DelAufMe)
		   {
			     DelListField ("auf_me");
			     DelListField ("auf_me_bz");
		   }
		   FormOK = TRUE;
	   }

	   akt_me = (double) 0.0;
       strcpy (lsps.lief_me, "0");
       strcpy (lsptab[0].lief_me, "0");
       lsk.fil = fil;
       lsk.ls = ls;
       lsk.kun = kun;
       lsk.kun_fil = kunfil;
       lsk.lieferdat = dasc_to_long (ldat);
	   int p = GetItemPos (&dataform, "ls_charge");
	   if (p != -1)
	   {
			dataform.mask[p].after  = TestLsCharge;
			dataform.mask[p].before = TestEnableCharge;
	   }

	   int AufMePos = GetItemPos (&dataform, "auf_me");
	   if (AufMePos == -1)
	   {
			dataform.mask[AufMePos].after = NULL;
	   }
	   if (EnterAufMe)
	   {
		   SetFieldAttr ("auf_me", EDIT);
		   if (AufMePos != -1)
		   {
				dataform.mask[AufMePos].after = GetAufMeVglEx;
		   }
	   }

	   set_fkt (NULL, 8);
	   SetFkt (8, leer, NULL);

       set_fkt (dokey5, 5);
       set_fkt (AppendLine, 6);
       set_fkt (DeleteLine, 7);

       if (lutz_mdn_par)
	   {
		   set_fkt (ProdMdn, 8);
	   }
       else if (rab_prov_kz)
	   {
		   set_fkt (PosRab, 8);
	   }
	   if (lutz_mdn_par)
	   {
	  	   SetFkt (8, prodmdn, KEY8);
	   }
       else if (rab_prov_kz)
	   {
		   SetFkt (8, posrab, KEY8);
	   }

       set_fkt (KunInfo, 3);
       set_fkt (WriteAllPos, 12);
       set_fkt (Schirm, 11);

       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (11, vollbild, KEY11);
       AktRow = 0;
       AktColumn = 0;
       if (initlsp == 0)
       {
                 eListe.SetInfoProc (InfoProc);
                 eListe.SetTestAppend (TestAppend);
                 sprintf (InfoCaption, "Lieferschein %ld", ls);
                 mamain1 = CreateMainWindow ();
                 eListe.InitListWindow (mamain1);
                 ReadDB (mdn, fil, ls);
                 initlsp = 1;
       }

       eListe.SetListFocus (ListFocus);
       eListe.Initscrollpos ();
	   eListe.SetListclipp (TRUE);
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       ShowDB (mdn, fil, ls);

       eListe.SetRowItem ("a", lsptab[0].a);
       SetRowItem ();

	   AnzAufWert ();
	   AnzAufGew ();
	   if (ScanMode)
	   {
		   int count = 1;
	       int pos = GetItemPos (&dataform, "a");
		   if (pos == -1) pos = 2;
           SetNewPicture ("a", "");
		   ScanColumns[0] = pos;
	       pos = GetItemPos (&dataform, "ls_charge");
		   if (pos != -1)
		   {
				ScanColumns[1] = pos;
				count ++;
		   }
		   eListe.SetEditRenderer (this, ScanColumns, count);
		   eListe.SetChangeChar (ChangeChar);
	   }

       ListAktiv = 1;
       eListe.ProcessMessages ();

       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 6);
       set_fkt (NULL, 7);
       set_fkt (NULL, 8);
       set_fkt (NULL, 11);
	   CloseAufw ();
	   CloseAufGew ();
	   DestroySa ();
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       initlsp = 0;
//	   current_form = savecurrent;
       return;
}

void AUFPLIST::WorkLsp ()
/**
Auftragsliste bearbeiten.
**/

{

	   eListe.RowEvent = this;
       beginwork ();

	   set_fkt (NULL, 8);
	   SetFkt (8, leer, NULL);

       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);

	   if (lutz_mdn_par)
	   {
		set_fkt (ProdMdn, 8);
	   }
       else if (rab_prov_kz)
	   {
		set_fkt (PosRab, 8);
	   }
	   if (lutz_mdn_par)
	   {
	  	   SetFkt (8, prodmdn, KEY8);
	   }
       else if (rab_prov_kz)
	   {
		   SetFkt (8, posrab, KEY8);
	   }

       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);


       eListe.SetDataForm0 (&dataform, &lineform);
       eListe.SetChAttr (ChAttr);
       eListe.SetUbRows (ubrows);
       eListe.SetListFocus (ListFocus);
       eListe.SetFeldFocus0 (eListe.GetAktRow (),
                             eListe.GetAktColumn ());
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;

       SetAktivWindow (eListe.Getmamain2 ());

       ListAktiv = 1;
	   AnzAufWert ();
	   AnzAufGew ();
       eListe.ProcessMessages ();

       commitwork ();
       if (syskey == KEY5)
       {
                  eListe.Initscrollpos ();
                  ShowDB (lsk.mdn, lsk.fil, lsk.ls);
       }
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
       return;
}


void AUFPLIST::DestroyMainWindow (void)
/**
Hauptfenster fuer Liste loeschen.
**/
{
        if (mamain1 == NULL) return;
        DestroyWindow (mamain1);
        mamain1 = NULL;
}


HWND AUFPLIST::CreateMainWindow (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWindow == NULL) return NULL;

        if (mamain1) return mamain1;

        eListe.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);


		if (liney > 0)
		{
			y = liney;
		}
		else
		{
            y = (wrect.bottom - 12 * tm.tmHeight);
		}
        x = wrect.left + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;

        mamain1 = CreateWindow ("ListMain",
                                    InfoCaption,
                                    WS_THICKFRAME |
//                                    WS_CAPTION |
//                                    WS_CHILD |
                                    WS_POPUP |
                                    WS_SYSMENU |
                                    WS_MINIMIZEBOX |
                                    WS_MAXIMIZEBOX,
                                    x, y,
                                    cx, cy,
                                    hMainWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (mamain1, SW_SHOWNORMAL);
        UpdateWindow (mamain1);
        return mamain1;
}

static int IsMax = 0;
static int IsMin = 0;

void AUFPLIST::SetMax0 (int val)
{
    IsMax = val;
    if (IsMax)
    {
             SetFkt (11, fenster, KEY11);
    }
    else
    {
             SetFkt (11, vollbild, KEY11);
    }
}

void AUFPLIST::SetMin0 (int val)
{
    IsMin = val;
}


int AUFPLIST::Schirm (void)
{
         if (IsMin) return 0;

         if (IsMax)
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_RESTORE, 0l);
             IsMax = 0;
         }
         else
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_MAXIMIZE, 0l);
             IsMax = 1;
         }
         return 0;
}


void AUFPLIST::MoveMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        eListe.GetTextMetric (&tm);
        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        if (mamainmax)
        {
                 x = wrect.left + 2;
                 y = wrect.top + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        else
        {
 		         if (liney)
				 {
			            y = liney;
				 }
				 else
				 {
                        y = (wrect.bottom - 12 * tm.tmHeight);
				 }
                 x = wrect.left + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}


void AUFPLIST::MaximizeMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        x = wrect.left + 2;
        y = wrect.top + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}



static char aufwert [20];
static char noworkpos [20];
static ITEM iaufwert  ("auf_wert",  aufwert,   "Betrag Netto   ", 0);
static ITEM inoworkpos  ("noworkpos",  noworkpos,   "unb.Positionen ", 0);

static field _faufwert [] = {
&iaufwert,        14,  0, 1, 1, 0, "%10.2f", READONLY, 0, 0, 0,
&inoworkpos,       8,  0, 2, 1, 0, "%5d",    READONLY, 0, 0, 0,
};

static form faufwert = {2, 0, 0, _faufwert, 0, 0, 0, 0, NULL};

static char aufgew [20];
static ITEM ibtr_bto ("btr_bto",    aufgew,    "Betrag Brutto   ", 0);
static ITEM iaufgrw  ("auf_gew",    aufgew,    "Auftragsgew.    ", 0);

static field _faufgew [] = {
&iaufgrw,        14,  0, 1, 1, 0, "%11.3f", READONLY, 0, 0, 0,
&ibtr_bto,       14,  0, 1, 1, 0, "%11.2f", READONLY, 0, 0, 0,
};

static form faufgew = {1, 0, 0, _faufgew, 0, 0, 0, 0, NULL};


void AUFPLIST::MoveAufw (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;
        if (AufMehWnd == NULL) return;

	    memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight + 8;
		MoveWindow (AufMehWnd, x, y, cx, cy, TRUE);
}

void AUFPLIST::MoveAufGew (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        RECT merect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;
        if (AufGewhWnd == NULL) return;

	    memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  2);
        if (AufMehWnd)
        {
             GetWindowRect (AufMehWnd, &merect);
             y = merect.bottom;
        }

        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight + 8;
		MoveWindow (AufGewhWnd, x, y, cx, cy, TRUE);
}


HWND AUFPLIST::CreateAufw (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        HDC hdc;
		HWND eWindow;

        if (hMainWin == NULL) return NULL;

	    memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

/*
        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight;
*/

        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 5 * tm.tmHeight + 8;

        eWindow       = CreateWindow (
//                                       "StaticWhite",
                                       "StaticGray",
                                       "",
                                       WS_DLGFRAME |
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);

        AufMehWnd0      = CreateWindowEx (
			                           WS_EX_CLIENTEDGE,
                                       "StaticWhite",
                                       "",
									   WS_CHILD | WS_VISIBLE,
                                       2, 4,
                                       cx - 10, cy - 14,
                                       eWindow,
                                       NULL,
                                       hMainInst,
                                       NULL);

        hdc = GetDC (eWindow);
//        ChoiseLines (eWindow, hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}

HWND AUFPLIST::CreateAufGew (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        RECT merect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        HDC hdc;
		HWND eWindow;

        if (hMainWin == NULL) return NULL;

	    memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  2);
        if (AufMehWnd)
        {
             GetWindowRect (AufMehWnd, &merect);
             y = merect.bottom;
        }
        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight + 8;

        eWindow       = CreateWindow (
//                                       "StaticWhite",
                                       "StaticGray",
                                       "",
                                       WS_DLGFRAME |
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);

        AufGewhWnd0      = CreateWindowEx (
			                           WS_EX_CLIENTEDGE,
                                       "StaticWhite",
                                       "",
									   WS_CHILD | WS_VISIBLE,
                                       2, 4,
                                       cx - 10, cy - 14,
                                       eWindow,
                                       NULL,
                                       hMainInst,
                                       NULL);

        hdc = GetDC (eWindow);
//        ChoiseLines (eWindow, hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}


void AUFPLIST::CloseAufw (void)
{
        if (auf_wert_anz == 0) return;

        if (AufMehWnd)
		{
			CloseControls (&faufwert);
            if (AufMehWnd0)
			{
			         DestroyWindow (AufMehWnd0);
 			         AufMehWnd0 = NULL;
			}
			DestroyWindow (AufMehWnd);
			AufMehWnd = NULL;
		}
}

void AUFPLIST::CloseAufGew (void)
{
        if (auf_gew_anz == 0) return;

        if (AufGewhWnd)
		{
			CloseControls (&faufgew);
            if (AufGewhWnd0)
			{
			         DestroyWindow (AufGewhWnd0);
 			         AufGewhWnd0 = NULL;
			}
			DestroyWindow (AufGewhWnd);
			AufGewhWnd = NULL;
		}
}


void AUFPLIST::AnzAufWert (void)
/**
Auftragswert anzeigen.
**/
{
          int currentf;
		  int i;
		  double wert;
		  double pos_wert;
		  int uwcount;
		  int recanz;


          if (auf_wert_anz == 0) return;

          currentf = currentfield;
		  recanz = eListe.GetRecanz ();
		  if (recanz == 0)
		  {
			  if (AufMehWnd)
			  {
				  DestroyWindow (AufMehWnd);
				  AufMehWnd = NULL;
			  }
			  return;
		  }
          if (AufMehWnd == NULL)
		  {
		            AufMehWnd = CreateAufw ();
					if (AufMehWnd == NULL) return;
		  }
		  wert = 0.0;
		  uwcount = 0;

		  pos_wert = ratod (lsps.lief_me) *
                        ratod (lsps.ls_vk_pr);
		  sprintf (lsps.pos_sum, "%.2lf", pos_wert);
          eListe.ShowAktRow ();

		  for (i = 0; i < recanz; i ++)
		  {
			      pos_wert = ratod (lsptab[i].lief_me) *
                                ratod (lsptab[i].ls_vk_pr);
				  sprintf (lsptab[i].pos_sum, "%.2lf", pos_wert);
                  wert = wert + ratod (lsptab[i].lief_me) *
                                ratod (lsptab[i].ls_vk_pr);
				  if (ratod (lsptab[i].lief_me) == 0.0)
				  {
					  uwcount ++;
				  }
          }

		  SetStaticWhite (TRUE);
//		  sprintf (aufwert, "%10.2lf", wert);
		  sprintf (aufwert, "%lf", wert);
		  sprintf (noworkpos, "%d", uwcount);
		  display_form (AufMehWnd0, &faufwert, 0, 0);
		  SetStaticWhite (FALSE);

          currentfield = currentf;
}

double AUFPLIST::GetArtGew (int i)
{
          double a_gew;
          char buffer [512];

          if (atoi (lsptab[i].me_einh) == 2)
          {
                    return ratod (lsptab[i].lief_me);
          }

          a_gew = 0.0;
          sprintf (buffer, "select a_gew from a_bas where a = %.0lf",
                            ratod (lsptab[i].a));
          DbClass.sqlout ((double *) &a_gew, 3, 0);
          DbClass.sqlcomm (buffer);
          if (a_gew == (double) 0.0)
          {
              a_gew = 1.0;
          }

          return a_gew * ratod (lsptab[i].lief_me);
}


double AUFPLIST::GetArtMwst (int i)
{
	      static cursor = -1; 
          static char cfaktor [9];

          double faktor;
//		  char roundwert [15];
          char buffer [512];


		  if (cursor == -1)
		  {
				sprintf (buffer, "select ptwer2 from a_bas,ptabn where a = ? and ptitem = \"mwst\" and ptlfnr = a_bas.mwst ",
					             ratod (lsptab[i].a));
				DbClass.sqlin ((double *) &_a_bas.a, 3, 0);
				DbClass.sqlout ((double *) cfaktor, 0, 8);
				cursor = DbClass.sqlcursor (buffer);
		  }
          faktor = 1.0;
          DbClass.sqlopen (cursor);
          int dsqlstatus = DbClass.sqlfetch (cursor);
		  faktor = ratod (cfaktor);
          if (faktor == (double) 0.0)
          {
              faktor = 1.0;
          }
		  if (atoi (kun.form_typ2) == FormTyp2 || atoi (kun.form_typ2) == FormTyp5)
		  {
			  faktor = 1.0;
		  }

          return faktor * ratod (lsptab[i].lief_me) * ratod (lsptab[i].ls_vk_pr);
//          sprintf (roundwert, "%.2lf", 
//			        faktor * ratod (lsptab[i].lief_me) * ratod (lsptab[i].ls_vk_pr));
//		  return ratod (roundwert);
}

void AUFPLIST::AnzAufGew (void)
/**
Auftragswert anzeigen.
**/
{
          int currentf;
		  int i;
		  double wert;
		  int recanz;


          if (auf_gew_anz == 0) return;

		  recanz = eListe.GetRecanz ();
		  if (recanz == 0)
		  {
			  if (AufGewhWnd)
			  {
				  DestroyWindow (AufGewhWnd);
				  AufGewhWnd = NULL;
			  }
			  return;
		  }
          if (AufGewhWnd == NULL)
		  {
		            AufGewhWnd = CreateAufGew ();
					if (AufGewhWnd == NULL) return;
		  }
		  wert = 0;
		  for (i = 0; i < recanz; i ++)
		  {
//                wert = wert + ratod (lsptab[i].lief_me);
		        wert = wert + GetArtGew (i);
// Hier wird jetzt wieder das Gewicht angezeigt, da das wichtiger zu schein seint. 
// Wenn sich eine Kunden beschwert, wird alles einstellbar gemacht.
// Row 21.01.2010
// 			      wert = wert + GetArtMwst (i);
          }

		  SetStaticWhite (TRUE);
		  sprintf (aufgew, "%10.3lf", wert);
		  display_form (AufGewhWnd0, &faufgew, 0, 0);
		  SetStaticWhite (FALSE);

          currentfield = currentf;
}

void AUFPLIST::EnterLdVk (void)
/**
Laden-VK editieren.
**/
{
       char ld_pr [11];
	   EnterF Enter;

       if (ratod (lsps.a) == (double) 0.0)
       {
           return;
       }
       sprintf (ld_pr, "%s", lsps.ls_lad_pr);
       Enter.SetColor (LTGRAYCOL);
	   Enter.EnterText (hMainWindow, "Laden-Preis ", ld_pr, 10, "%8.2lf");

	   if (syskey == KEY5)
	   {
                eListe.ShowAktRow ();
	            return;
	   }

       if (ratod (lsps.ls_lad_pr) != ratod (ld_pr))
       {
           lsps.ls_pos_kz = 1;
       }
       sprintf (lsps.ls_lad_pr, "%6.2lf", ratod (ld_pr));
       memcpy (&lsptab[eListe.GetAktRow()], &lsps, sizeof (struct LSPS));
       eListe.ShowAktRow ();
}


void AUFPLIST::EnterPrVk (void)
/**
Laden-VK editieren.
**/
{
       char vk_pr [11];
       int fpos;
       char *picture = "%8.2lf";
	   EnterF Enter;

       if (ratod (lsps.a) == (double) 0.0)
       {
           return;
       }
       fpos = GetItemPos (&dataform, "pr_vk");
       if (fpos != -1)
       {
           picture = dataform.mask[fpos].picture;
       }

       sprintf (vk_pr, "%s", lsps.ls_vk_pr);
       Enter.SetColor (LTGRAYCOL);
	   Enter.EnterText (hMainWindow, "   VK-Preis ", vk_pr, 10, picture);

	   if (syskey == KEY5)
	   {
                eListe.ShowAktRow ();
	            return;
	   }

       sprintf (lsps.ls_vk_pr, "%lf", ratod (vk_pr));
       memcpy (&lsptab[eListe.GetAktRow()], &lsps, sizeof (struct LSPS));
       eListe.ShowAktRow ();
}


long AUFPLIST::TestProdLgr (long gruppe)
/**
Test, ob der Artikel beim Aktuellen Mandanten vorhanden ist.
**/
{
	      int dsqlstatus;
          short mdn;
		  double a;

          a = ratod (lsps.a);
		  mdn = (short) gruppe;
		  DbClass.sqlin ((short *) &mdn, 1, 0);
		  DbClass.sqlin ((double *) &a, 3, 0);
		  dsqlstatus = DbClass.sqlcomm ("select a from a_lgr where mdn = ? and a = ?");
		  if (dsqlstatus == 0) return gruppe;
		  return ChoiseProdLgr (gruppe);
}


long AUFPLIST::ChoiseProdLgr (long gruppe)
/**
Produktionslager fuer Artikel auswaehlen.
**/
{
	   CHOISE *Choise;
	   HWND Pwindow;
	   char Mandant [80];
	   char Row [80];
	   char adr_krz [17];
	   short mdn;
	   char *Caption = "Produktions-Mandanten";
	   int cursor;
	   int mdn_cursor;
	   double a;
	   char pmdn[20];
	   char Ub [80];

       memset (pmdn, ' ', 19);
       a = ratod (lsps.a);
	   if (a == (double) 0.0) return gruppe;

	   sprintf (Mandant, "  %ld", gruppe);
	   Choise = new CHOISE (-1, -1, 70, 16, Caption, 120, "Zugeordneter Mandant",
		                                                   "Gew�hlter Mandant");
	   Pwindow = Choise->OpenWindow (hMainInst, hMainWin);
	   Choise->SetChoiseDefault (Mandant);

       sprintf (Ub, " Mandanten f�r Artikel %.0lf", ratod (lsps.a));
	   Choise->FillListUb (Ub);
	   DbClass.sqlin ((double *) &a, 3, 0);
	   DbClass.sqlout ((char *) &pmdn [2], 0, 9);
	   cursor = DbClass.sqlcursor ("select distinct mdn from a_lgr where a = ?");
	   DbClass.sqlin ((short *) &mdn, 1, 0);
	   DbClass.sqlout ((char *) adr_krz, 0, 17);
	   mdn_cursor = DbClass.sqlcursor ("select adr_krz from mdn,adr where mdn.mdn = ? "
		                               "and adr.adr = mdn.adr");
       while (DbClass.sqlfetch (cursor) == 0)
	   {
		           strcpy (adr_krz, "");
				   mdn = atoi (&pmdn[2]);
		           DbClass.sqlopen (mdn_cursor);
		           DbClass.sqlfetch (mdn_cursor);
				   sprintf (Row, "%s \"%s\"", pmdn, adr_krz);
	               Choise->FillListRow (Row);
	   }
       DbClass.sqlclose (cursor);
       DbClass.sqlclose (mdn_cursor);

	   Choise->ProcessMessages ();
	   Choise->DestroyWindow ();
       delete Choise;
       eListe.SetFeldFocus0 (eListe.GetAktRow (),
                             eListe.GetAktColumn ());
	   gruppe = atol (Mandant);
	   return gruppe;
}



HWND AUFPLIST::GetMamain1 (void)
{
       return (mamain1);
}

void AUFPLIST::SethwndTB (HWND hwndTB)
{
         this->hwndTB = hwndTB;
         eListe.SethwndTB (hwndTB);
}

void AUFPLIST::SetTextMetric (TEXTMETRIC *tm)
{
         memcpy (&textm, tm, sizeof (TEXTMETRIC));
         eListe.SetTextMetric (tm);
}


void AUFPLIST::SetLineRow (int LineRow)
{
         eListe.SetLineRow (0);
}

void AUFPLIST::SetListLines (int i)
{
//         TListe.SetListLines (i);
         eListe.SetListLines (i);
}

void AUFPLIST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	    HDC hdc;

        TListe.OnPaint (hWnd, msg, wParam, lParam);
        eListe.OnPaint (hWnd, msg, wParam, lParam);
        if (hWnd == eWindow)
        {
                    hdc = BeginPaint (eWindow, &aktpaint);
                    ChoiseLines (eWindow, hdc);
                    EndPaint (eWindow, &aktpaint);
        }
        else if (hWnd == AufMehWnd)
        {
                    hdc = BeginPaint (AufMehWnd, &aktpaint);
//                    ChoiseLines (AufMehWnd, hdc);
                    EndPaint (AufMehWnd, &aktpaint);
        }
        else if (hWnd == BasishWnd)
        {
                    hdc = BeginPaint (BasishWnd, &aktpaint);
                    ChoiseLines (BasishWnd, hdc);
                    EndPaint (BasishWnd, &aktpaint);
        }
        else if (hWnd == SaWindow)
        {
                    hdc = BeginPaint (hWnd, &aktpaint);
                    PaintSa (hdc);
                    EndPaint (hWnd, &aktpaint);
        }
        else if (hWnd == PlusWindow)
        {
                    hdc = BeginPaint (hWnd, &aktpaint);
                    PaintPlus (hdc);
                    EndPaint (hWnd, &aktpaint);
        }
}


void AUFPLIST::MoveListWindow (void)
{
        TListe.MoveListWindow ();
        eListe.MoveListWindow ();
}


void AUFPLIST::BreakList (void)
{
        eListe.BreakList ();
}


void AUFPLIST::OnHScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        TListe.OnHScroll (hWnd, msg,wParam, lParam);
        eListe.OnHScroll (hWnd, msg,wParam, lParam);
}

void AUFPLIST::OnVScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        TListe.OnVScroll (hWnd, msg,wParam, lParam);
        eListe.OnVScroll (hWnd, msg,wParam, lParam);
}

void AUFPLIST::OnSize (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        if (hWnd == GetMamain1 ())
        {
                    MoveListWindow ();
                    MoveAufw ();
                    MoveAufGew ();
                    MoveSaW ();
                    MovePlus ();
        }
        else if (hWnd == TListe.GetMamain1 ())
        {
                    TListe.MoveListWindow ();
        }
}


void AUFPLIST::StdAuftrag (void)
{
         if (eListe.Getmamain3 () == NULL) return;
         doStd ();
}

void AUFPLIST::FunkKeys (WPARAM wParam, LPARAM lParam)
{
		if (TListe.Getmamain3())
		{
				TListe.FunkKeys (wParam, lParam);
		}
		else
		{
				eListe.FunkKeys (wParam, lParam);
		}
}


int AUFPLIST::GetRecanz (void)
{
          return eListe.GetRecanz ();
}


void AUFPLIST::SwitchPage0 (int rows)
{
           eListe.SwitchPage0 (rows);
}


HWND AUFPLIST::Getmamain2 (void)
{
           return eListe.Getmamain2 ();
}


HWND AUFPLIST::Getmamain3 (void)
{
           return eListe.Getmamain3 ();
}


void AUFPLIST::ChoiseFont (mfont *lfont)
{
           eListe.ChoiseFont (lfont);
}

void AUFPLIST::SetFont (mfont *lfont)
{
           TListe.SetFont (lfont);
           eListe.SetFont (lfont);
}

void AUFPLIST::SetListFont (mfont *lfont)
{
           TListe.SetListFont (lfont);
           eListe.SetListFont (lfont);
}

void AUFPLIST::FindString (void)
{
           eListe.FindString ();
}


void AUFPLIST::SetLines (int Lines)
{
           TListe.SetLines (Lines);
           eListe.SetLines (Lines);
}


int AUFPLIST::GetAktRow (void)
{
                 return eListe.GetAktRow ();
}

int AUFPLIST::GetAktRowS (void)
{
                 return eListe.GetAktRowS ();
}

void AUFPLIST::SetColors (COLORREF Color, COLORREF BkColor)
{
                 TListe.SetColors (Color, BkColor);
                 eListe.SetColors (Color, BkColor);
}

void AUFPLIST::SetListFocus (void)
{
                 eListe.SetFeldFocus0 (eListe.GetAktRow (),
                                       eListe.GetAktColumn ());
}

void AUFPLIST::PaintUb (void)
{
                 eListe.PaintUb ();
}

BOOL AUFPLIST::ContainsService ()
{
    int recs = eListe.GetRecanz ();
	for (int i = 0; i < recs; i ++)
	{
		if (isPartyDienstleistung(lsptab[i].a_typ,lsptab[i].mwst))
		{
			return TRUE;
		}
	}
	return FALSE;
}

void AUFPLIST::NewPr ()
{
    if (Muster) return;
    int recs = eListe.GetRecanz ();
	if (recs <= 0) return;
	int AktRow = eListe.GetAktRow();

    memcpy (&lsptab[AktRow], &lsps, sizeof (struct LSPS));
	for (int i = 0; i < recs; i ++)
	{
		memcpy (&lsps, &lsptab[i], sizeof (struct LSPS));
		if (lsps.ls_pos_kz == 1) continue;
		ReadPrEx ();
	    memcpy (&lsptab[i], &lsps, sizeof (struct LSPS));
	}
    memcpy (&lsps, &lsptab[AktRow], sizeof (struct LSPS));
    InvalidateRect (eListe.Getmamain3 (), 0, FALSE);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
    AnzAufWert ();
    AnzAufGew ();
}

void AUFPLIST::ReadPrEx (void)
/**
Artikelpreis holen.
**/
{
       char lieferdat [12];
       int dsqlstatus;
       short sa, sa_werbung;
       double pr_ek;
       double pr_vk;
	   BOOL fix;
	   int dsqlstatusw = 100;

       dlong_to_asc (lsk.lieferdat, lieferdat);

// Preise immer mit Filiale 0 lesen.

	   WaPreis.SetAufArt (auf_art);
// WAL-103  1. Priorit�t Werbung holen  aus der Preisliste   auf kun_erw.werbung
	   // Kennung kein fixer Preis, variabel , zus. zum Normalpreis :   ----> aufp.ksys = 2   und aufp.bestellvorschl =  WerbePreis
	   // Kennung fixer WerbePreis,                                 :   ----> aufp.ksys = 2   und aufp.bestellvorschl =  WerbePreis  aufp.auf_vk_pr = WerbePreis
	   fix = FALSE;
	   int werbung = 0;
	   double werbepreis = 0.0;
		 DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		 DbClass.sqlin ((long *) &lsk.kun, 2, 0);
		 DbClass.sqlout ((long *) &werbung, 2, 0);
		 DbClass.sqlcomm ("select werbung from kun_erw where mdn = ? and fil = 0 and kun = ?");
	   if (lsk.kun_fil == 0 && werbung > 0)
	   {
					dsqlstatusw = WaPreis.werbung_holen (lsk.mdn,
                                          0,
                                          werbung,
                                           ratod (lsps.a),
                                          lieferdat,
                                          &sa_werbung,
                                          &pr_ek,
										  &fix);

					if (sa_werbung == 1 && fix == TRUE) 
					{
		                werbepreis = pr_ek;

					}


	   }

	   if (DllPreise.PriceLib != NULL && 
	    DllPreise.preise_holen != NULL)
	   {
				  dsqlstatus = (DllPreise.preise_holen) (lsk.mdn, 
                                          0,
                                          lsk.kun_fil,
                                          lsk.kun,
                                           ratod (lsps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
				  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
	   }
  	   else
	   {
				dsqlstatus = WaPreis.preise_holen (lsk.mdn,
                                          0,
                                          lsk.kun_fil,
                                          lsk.kun,
                                           ratod (lsps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
	   }
	   if (fix == TRUE && dsqlstatusw == 0) //WAL-103
	   {
			pr_ek = werbepreis;
			sa = sa_werbung;
	   }

       if (dsqlstatus == 0)
       {
                 sprintf (lsps.ls_vk_pr, "%lf",  pr_ek);
                 sprintf (lsps.ls_lad_pr,"%lf", pr_vk);
                 sprintf (lsps.sa_kz_sint, "%1d", sa);
                 strcpy (lsps.kond_art, WaPreis.GetKondArt ());
                 strcpy (lsps.kond_art0, WaPreis.GetKondArt ());
                 sprintf (lsps.a_grund, "%4.0lf", WaPreis.GetAGrund ());
                 FillKondArt (lsps.kond_art);
       }
	   ls_vk_pr = pr_ek;
	   FillWaehrung (pr_ek, pr_vk);
       sprintf (lsps.a_grund, "%4.0lf", WaPreis.GetAGrund ());
       if (ratod (lsps.a_grund) == 0.0)
	   {
            sprintf (lsps.a_grund, "%4.0lf", GetBasa_grund (_a_bas.a));
	   }
	   ReadExtraPr ();
}

BOOL AUFPLIST::artikelOK (double a)
{ 
       int dsqlstatus = lese_a_bas (a);
       if (dsqlstatus == 100 || dsqlstatus == ArtikelGesperrt) //FS-26
	   {
           if (dsqlstatus == 100) dsqlstatus = ReadEan (a);
		   if (dsqlstatus == 100)
		   {
			     dsqlstatus = ReadEmb (a);
		   }
		   if (dsqlstatus == 100)
		   {
			   disp_mess ("Artikel nicht gefunden", 2);
			   return false;
		   }
		   if (dsqlstatus == ArtikelGesperrt)
		   {
			   disp_mess ("Artikel ist deaktiviert", 2);
			   return false;
		   }

	   }
	   sprintf (lsps.a, "%13.0lf", ratod (lsps.a));
	   return true;
}


int AUFPLIST::ScanEan128Ai (Text& Ean128)
{


/* Teil f�r allgemeine Ean128verarbeitung */

        double ean1 = 0.0;
		double Gew;

		Text *ean = Scanean.GetEan128Ai (Text ("01"), Ean128);
		if (ean == NULL)
		{

		        disp_mess ("Fehler bei Ean128 : Eancode", 2);
				return 100;
		}

		Text Ean = ean->SubString (1, Ean128Eanlen);
		delete ean;
		double a = ratod (Ean.GetBuffer ());
		ean1 = a;
		if (artikelOK (a) == FALSE)
		{
			return 100;
		}

		sprintf (lsps.a, "%.0lf", _a_bas.a);
        memcpy (&lsptab[eListe.GetAktRow()],
                           &lsps, sizeof (struct LSPS));
		eListe.ShowAktRow ();

		Gew = 0.0;
		if (ScannMeZwang != 1)
		{
			if (_a_bas.hnd_gew[0] == 'G')
			{
				Text *gew = Scanean.GetEan128Ai (Text ("3103"), Ean128);
				if (gew == NULL)
				{
					if (ScannMeZwang == 2)
					{
						disp_mess ("Fehler bei Ean128 : keine Gewichtauszeichnung", 2);
						return 100;
					}
				}
				else
				{
					Gew = ratod (gew->GetBuffer ());
					delete gew;
				}
			}
			else
			{
				Text *gew = Scanean.GetEan128Ai (Text ("30"), Ean128);
				if (gew == NULL)
				{
					gew = Scanean.GetEan128Ai (Text ("37"), Ean128);
				}
				if (gew == NULL)
				{
					if (ScannMeZwang == 2)
					{
						disp_mess ("Fehler bei Ean128 : keine St�ckauszeichnung", 2);
						return 100;
					}
				}
				else
				{
					Gew = ratod (gew->GetBuffer ()) * 1000.0;
					delete gew;
				}
			}
		}
		Text Mhd = "";
		Text *mhd = Scanean.GetEan128Ai (Text ("15"), Ean128);
		if (mhd == NULL)
		if (mhd != NULL)
		{
				Text Mhd = *mhd;
				delete mhd;
		}
		Ean128Date = Mhd;
		lsps.hbk_date = dasc_to_long (Ean128Date.ToString ().GetBuffer ());
		Text Charge = "";
		ScannedCharge = "";
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

		if (ChargeZwang && _a_bas.charg_hand == 1 && charge == NULL)
		{
		        disp_mess ("Fehler bei Ean128 : Charge", 2);
				return 100;
		}

		if (charge != NULL)
		{
				Charge = *charge;
				ScannedCharge = *charge;
				strcpy (lsps.ls_charge, Charge.GetBuffer ());
		}
		else
		{
				strcpy (lsps.ls_charge, "");
		}
		sprintf (lsps.a, "%.0lf", _a_bas.a);
//		sprintf (lsps.s, "%hd", 2);
		scangew = (long) (Gew);
		sprintf (lsps.me_einh, "%hd", 2);
		memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
		if (ptab_class.lese_ptab ("me_einh", lsps.me_einh) == 0)
		{
                  strcpy (lsps.basis_me_bz, ptabn.ptbezk);
		}
        return 0;
}


int AUFPLIST::ScanEan128Charge (Text& Ean128)
{
		Text Charge = "";
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

/*
		if (ChargeZwang && _a_bas.charg_hand == 1 && charge == NULL)
		{
		        disp_messG ("Fehler bei Ean128 : Charge", 2);
				return 100;
		}
*/

		if (charge != NULL)
		{
				Charge = *charge;
				if (Charge.GetLength () > 20)
				{
					  Charge = Charge.SubString (0, 20);
				}
				if (WithZerlData && (_a_bas.charg_hand == 9 || _a_bas.zerl_eti == 1))
				{
					strcpy (lsps.ls_ident, Charge.GetBuffer ());
				}
				else
				{
					strcpy (lsps.ls_charge, Charge.GetBuffer ());
				}
		}
		else
		{
				if (WithZerlData && (_a_bas.charg_hand == 9 || _a_bas.zerl_eti == 1))
				{
					strcpy (lsps.ls_ident, "");
				}
				else
				{
					strcpy (lsps.ls_charge, "");
				}
		}
		sprintf (lsps.a, "%.0lf", _a_bas.a);
//		sprintf (lsps.s, "%hd", 2);
        return 0;
}


int AUFPLIST::ScanEan128 (Text& Ean128)
{
/*
	if (Text (Ean128.SubString (0, 2)) == "00")
	{
		return ScanEan128Nve (Ean128);
	}
*/
	return ScanEan128Ai (Ean128);
}


int AUFPLIST::TestEanCharge (char *buffer)
{
	if (ChargeFromEan128 == ChargeFromAi01)
	{
 		Text Ean128 = buffer;
		Text *Ean = Scanean.GetEan128Ai (Text ("01"), Ean128);
        if (Ean == NULL)
		{		
// Keine Ean-Nr vorhanden, buffer ist charge
			return 0;
		}
		Text *Charge = Scanean.GetEan128Ai (Text ("10"), Ean128);
		if (Charge == NULL)
		{
// Keine Chargen-Nr vorhanden, Fehler
			disp_mess ("Das Etikett hat keine Chargennummer", 2);
			strcpy (buffer, "");
			return 0;
		}
		strcpy (buffer, Charge->GetBuffer ());
	}
	else if (ChargeFromEan128 == ChargeFromLength)
// Stringl�nge entscheidet
	{
		Text Ean128 = buffer;
		Ean128.Trim ();
		if (Ean128.GetLength () < Ean128StartLen)
		{		
// Keine Ean-Nr vorhanden, buffer ist charge
			return 0;
		}
		Text *Charge = Scanean.GetEan128Ai (Text ("10"), Ean128);
		if (Charge == NULL)
		{
// Keine Chargen-Nr vorhanden, buffer ist charge
			return 0;
		}
		strcpy (buffer, Charge->GetBuffer ());
	}
	return 0;
}


int AUFPLIST::ChangeChar (int key)
{
	if (eListe.GetAktColumn () != 2)
	{
		return key;
	}
	CInteger Key (key);
	Scanean.AiInfos.FirstPosition ();
    AiInfo *ai;
	while ((ai = (AiInfo *) Scanean.AiInfos.GetNext ()) != NULL)
	{
		ai->EndChar.FirstPosition ();
		CInteger *ec;
		while ((ec = (CInteger *) ai->EndChar.GetNext ()) != NULL)
		{
			if (Key == *ec)
			{
				return ai->EndCharEdit;
			}
		}
	}
	return key;
}


HWND AUFPLIST::SetFeldEdit (int row, int column, int x, int y, int cx, int cy, HWND hParent, HINSTANCE hInstance)
{
      int apos = eListe.GetFieldPos ("a");
      int chargepos = eListe.GetFieldPos ("ls_charge");
	  if (column == apos || (column == chargepos && ChargeFromEan128 != 0))
	  {
				HWND FocusWindow = CreateWindow ("Edit",
									"",
										WS_CHILD | ES_MULTILINE | ES_AUTOHSCROLL | WS_BORDER, 
										x - 5, y - 5, cx * 3, cy + 10,
										hParent,
										NULL,
										hInstance,
										NULL);
				return FocusWindow;
	  }
	  return NULL;
}

char *AUFPLIST::SetDefault (int row, int column, char *buffer, int length)
{
    int apos = eListe.GetFieldPos ("a");
    int chargepos = eListe.GetFieldPos ("ls_charge");
	if (column == apos || (column == chargepos && ChargeFromEan128 != 0))
	{
		strcpy (buffer, "");
	}
	return buffer;
}

char *AUFPLIST::GetWindowText (HWND FocusWindow, int row, int column, char *buffer, int length)
{
    int apos = eListe.GetFieldPos ("a");
    int chargepos = eListe.GetFieldPos ("ls_charge");
	if (column == apos || (column == chargepos && ChargeFromEan128 != 0))
	{
		  ::GetWindowText (FocusWindow,
				           buffer,
					       length);
		  if (column == chargepos)
		  {
			  TestEanCharge (buffer);
		  }
	}
	  return buffer;
}



