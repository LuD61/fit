#ifndef _SEARCHERROR_DEF
#define _SEARCHERROR_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"
#include "Property.h"

class SEARCHERROR
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static int idx;
           static long anz;
           static CHQEX *Query;

           int SearchPos;
           int OKPos;
           int CAPos;
           char Key [512];
	       static CProperty Errors;

        public :
           SEARCHERROR ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
           }

           ~SEARCHERROR ()
           {
                   if (Query)
                   {
                       delete Query;
                       Query = NULL;
                   }
           }

/*
           SWG *GetLine (void)
           {
               if (idx == -1) return NULL;
               return &sline;
           }
*/

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           static int SearchLst (char *);
           static int ReadLst (char *);
           BOOL GetKey (char *);
           void SetParams (HINSTANCE, HWND);
           void CopyError (void);
           void Search (void);
};  
#endif