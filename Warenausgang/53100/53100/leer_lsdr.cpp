#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "mo_curso.h"
#include "dbclass.h"
#include "leer_lsdr.h"

struct LEER_LSDR leer_lsdr, leer_lsdr_null;

void LEER_LSDR_CLASS::prepare (void)
{
            TCHAR *sqltext;

            ins_quest ((char *)  &leer_lsdr.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsdr.a, SQLDOUBLE, 0);
            ins_quest ((char *)  &leer_lsdr.ls, SQLLONG, 0);
            ins_quest ((char *)   leer_lsdr.blg_typ, SQLCHAR, 2);
    out_quest ((char *) &leer_lsdr.mdn,1,0);
    out_quest ((char *) &leer_lsdr.fil,1,0);
    out_quest ((char *) &leer_lsdr.ls,2,0);
    out_quest ((char *) leer_lsdr.blg_typ,0,2);
    out_quest ((char *) &leer_lsdr.a,3,0);
    out_quest ((char *) &leer_lsdr.me_stk_zu,2,0);
    out_quest ((char *) &leer_lsdr.me_stk_abn,2,0);
    out_quest ((char *) &leer_lsdr.stk,2,0);
    out_quest ((char *) &leer_lsdr.stat,1,0);
            cursor = sqlcursor ("select leer_lsdr.mdn,  "
"leer_lsdr.fil,  leer_lsdr.ls,  leer_lsdr.blg_typ,  leer_lsdr.a,  "
"leer_lsdr.me_stk_zu,  leer_lsdr.me_stk_abn,  leer_lsdr.stk,  "
"leer_lsdr.stat from leer_lsdr "

#line 22 "leer_lsdr.rpp"
                                  "where mdn = ? "
				  "and a = ? "
 				  "and ls = ? "
				  "and blg_typ = ?");
    ins_quest ((char *) &leer_lsdr.mdn,1,0);
    ins_quest ((char *) &leer_lsdr.fil,1,0);
    ins_quest ((char *) &leer_lsdr.ls,2,0);
    ins_quest ((char *) leer_lsdr.blg_typ,0,2);
    ins_quest ((char *) &leer_lsdr.a,3,0);
    ins_quest ((char *) &leer_lsdr.me_stk_zu,2,0);
    ins_quest ((char *) &leer_lsdr.me_stk_abn,2,0);
    ins_quest ((char *) &leer_lsdr.stk,2,0);
    ins_quest ((char *) &leer_lsdr.stat,1,0);
            sqltext = "update leer_lsdr set "
"leer_lsdr.mdn = ?,  leer_lsdr.fil = ?,  leer_lsdr.ls = ?,  "
"leer_lsdr.blg_typ = ?,  leer_lsdr.a = ?,  leer_lsdr.me_stk_zu = ?,  "
"leer_lsdr.me_stk_abn = ?,  leer_lsdr.stk = ?,  leer_lsdr.stat = ? "

#line 27 "leer_lsdr.rpp"
                                  "where mdn = ? "
				  "and a = ? "
 				  "and ls = ? "
				  "and blg_typ = ?";
            ins_quest ((char *)  &leer_lsdr.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsdr.a, SQLDOUBLE, 0);
            ins_quest ((char *)  &leer_lsdr.ls, SQLLONG, 0);
            ins_quest ((char *)   leer_lsdr.blg_typ, SQLCHAR, 2);
            upd_cursor = sqlcursor (sqltext);

            ins_quest ((char *)  &leer_lsdr.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsdr.a, SQLDOUBLE, 0);
            ins_quest ((char *)  &leer_lsdr.ls, SQLLONG, 0);
            ins_quest ((char *)   leer_lsdr.blg_typ, SQLCHAR, 2);
            test_upd_cursor = sqlcursor ("select a from leer_lsdr "
                                  "where mdn = ? "
				  "and a = ? "
 				  "and ls = ? "
				  "and blg_typ = ?");
            ins_quest ((char *)  &leer_lsdr.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsdr.a, SQLDOUBLE, 0);
            ins_quest ((char *)  &leer_lsdr.ls, SQLLONG, 0);
            ins_quest ((char *)   leer_lsdr.blg_typ, SQLCHAR, 2);
            del_cursor = sqlcursor ("delete from leer_lsdr "
                                  "where mdn = ? "
				  "and a = ? "
 				  "and ls = ? "
				  "and blg_typ = ?");
    ins_quest ((char *) &leer_lsdr.mdn,1,0);
    ins_quest ((char *) &leer_lsdr.fil,1,0);
    ins_quest ((char *) &leer_lsdr.ls,2,0);
    ins_quest ((char *) leer_lsdr.blg_typ,0,2);
    ins_quest ((char *) &leer_lsdr.a,3,0);
    ins_quest ((char *) &leer_lsdr.me_stk_zu,2,0);
    ins_quest ((char *) &leer_lsdr.me_stk_abn,2,0);
    ins_quest ((char *) &leer_lsdr.stk,2,0);
    ins_quest ((char *) &leer_lsdr.stat,1,0);
            ins_cursor = sqlcursor ("insert into leer_lsdr ("
"mdn,  fil,  ls,  blg_typ,  a,  me_stk_zu,  me_stk_abn,  stk,  stat) "

#line 56 "leer_lsdr.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?)"); 

#line 58 "leer_lsdr.rpp"
}

BOOL LEER_LSDR_CLASS::operator== (LEER_LSDR& leer_lsdr)
{
            if (this->leer_lsdr.mdn        != leer_lsdr.mdn) return FALSE;  
            if (this->leer_lsdr.ls         != leer_lsdr.ls) return FALSE;  
            if (strcmp (this->leer_lsdr.blg_typ, leer_lsdr.blg_typ) != 0) return FALSE;  
            if (this->leer_lsdr.a          != leer_lsdr.a) return FALSE;  
            return TRUE;
} 
