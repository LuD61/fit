// WaWiePro.cpp: Implementierung der Klasse CWaWiePro.
//
//////////////////////////////////////////////////////////////////////

#include "WaWiePro.h"
#include "Ls.h"
#include "mo_menu.h"
#include "strfkt.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CWaWiePro::CWaWiePro()
{
	a = 0.0;
	auf_lad_pr = 0.0;
	erf_kz = "";
	ls_charge = "";
	ls_ident = "";
}

CWaWiePro::~CWaWiePro()
{

}

void CWaWiePro::Write (double brutto, double netto, double tara)
{
         char datum [12];
         char zeit  [12];

         sysdate (datum);
         systime (zeit);

		 memcpy (&wiepro, &wiepro_null, sizeof (WIEPRO));
         strcpy (wiepro.peri_nam, "");
		 wiepro.mdn = lsk.mdn;
         wiepro.fil = lsk.fil;
         wiepro.a   = a;
		 wiepro.pr  = auf_lad_pr;
         strcpy (wiepro.blg_typ, "L");
         wiepro.nr = lsk.ls;
         wiepro.kun = lsk.kun ;
		 wiepro.gew_bto = brutto;
		 wiepro.gew_nto = brutto - tara;
         wiepro.tara = tara;
         memcpy (wiepro.zeit, zeit, 2);
         memcpy (&wiepro.zeit[2], &zeit[3], 2);
         memcpy (&wiepro.zeit[4], &zeit[6], 2);
		 wiepro.zeit[6] = 0;	 ;
         wiepro.dat = dasc_to_long (datum);
         wiepro.sys = 0;
		 wiepro.kun_fil = lsk.kun_fil;
         strcpy (wiepro.pers, sys_ben.pers_nam);
         strcpy (wiepro.erf_kz, erf_kz);
		 clipped (ls_charge);
		 if (strlen (ls_charge) <= 20)
		 {
			strcpy (wiepro.ls_charge, ls_charge);
		 }
		 else
		 {
			strcpy (wiepro.ls_char30, ls_charge);
		 }
         strcpy (wiepro.ls_ident, ls_ident);
		 WiePro.dbinsert ();
}


void CWaWiePro::Write (double brutto, double netto, double tara, char *erf_kz)
{
         char datum [12];
         char zeit  [12];


         sysdate (datum);
         systime (zeit);

		 memcpy (&wiepro, &wiepro_null, sizeof (WIEPRO));
         strcpy (wiepro.peri_nam, "");
		 wiepro.mdn = lsk.mdn;
         wiepro.fil = lsk.fil;
         wiepro.a   = a;
		 wiepro.pr  = auf_lad_pr;
		 if (auf_lad_pr > (double) 9999) wiepro.pr = 0;   //verhindert den �berlauf da pr nur ein decimal(6,2) ist
         strcpy (wiepro.blg_typ, "L");
         wiepro.nr = lsk.ls;
         wiepro.kun = lsk.kun ;
		 wiepro.gew_bto = brutto;
		 wiepro.gew_nto = brutto - tara;
         wiepro.tara = tara;
         memcpy (wiepro.zeit, zeit, 2);
         memcpy (&wiepro.zeit[2], &zeit[3], 2);
         memcpy (&wiepro.zeit[4], &zeit[6], 2);
		 wiepro.zeit[6] = 0;	 ;
         wiepro.dat = dasc_to_long (datum);
         wiepro.sys = 0;
		 wiepro.kun_fil = lsk.kun_fil;
         strcpy (wiepro.pers, sys_ben.pers_nam);
         strcpy (wiepro.erf_kz, erf_kz);
		 clipped (ls_charge);
		 if (strlen (ls_charge) <= 20)
		 {
			strcpy (wiepro.ls_charge, ls_charge);
		 }
		 else
		 {
			strcpy (wiepro.ls_char30, ls_charge);
		 }
         strcpy (wiepro.ls_ident, ls_ident);
		 WiePro.dbinsert ();
}

