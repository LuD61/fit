#ifndef _LEER_LSDR_DEF
#define _LEER_LSDR_DEF
#include "dbclass.h"

struct LEER_LSDR {
   short     mdn;
   short     fil;
   long      ls;
   char      blg_typ[2];
   double    a;
   long      me_stk_zu;
   long      me_stk_abn;
   long      stk;
   short     stat;
};
extern struct LEER_LSDR leer_lsdr, leer_lsdr_null;

#line 6 "leer_lsdr.rh"

class LEER_LSDR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LEER_LSDR leer_lsdr;
               LEER_LSDR_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (LEER_LSDR&);  
};
#endif
