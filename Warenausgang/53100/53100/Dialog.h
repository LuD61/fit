// Dialog.h: Schnittstelle f�r die Klasse CDialog.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIALOG_H__252AA6E5_0F09_43BA_8BAD_CDE464515199__INCLUDED_)
#define AFX_DIALOG_H__252AA6E5_0F09_43BA_8BAD_CDE464515199__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>


class CDialog  
{
public:
	DWORD Id;
	HWND m_hWnd;
	HWND Parent;
	BOOL ret;
	WPARAM wParam;
	static CDialog *Instance;
	CDialog(DWORD Id, HWND parent = NULL);
	virtual ~CDialog();
protected:
	virtual void AttachControls ();
	virtual BOOL OnInitDialog ();
	virtual HBRUSH OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor);
	virtual void OnOK ();
	virtual void OnCancel ();
	static DWORD CALLBACK DlgProc (HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam); 

public:
	BOOL DoModal ();

};

#endif // !defined(AFX_DIALOG_H__252AA6E5_0F09_43BA_8BAD_CDE464515199__INCLUDED_)
