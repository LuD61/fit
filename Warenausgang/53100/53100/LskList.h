#ifndef _LSK_LIST_DEF
#define _LSK_LIST_DEF
#pragma once

class CLskList
{
public:
	short mdn;
	long ls;
	long auf;
	long kun;
	char lieferdat[12];
	short ls_stat;
	char kun_krz1[17];
	char kun_bran2[3];

	CLskList(void);
	~CLskList(void);
	void SetLieferdat (char * p);
	void SetKunKrz1 (char * kun_krz1);
	void SetKunBran2 (char * kun_bran2);
};
#endif
