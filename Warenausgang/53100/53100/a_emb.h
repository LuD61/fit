#ifndef _A_EMB_DEF
#define _A_EMB_DEF

#include "dbclass.h"

struct A_EMB {
   short     anz_emb;
   short     delstatus;
   double    emb;
   char      emb_bz[25];
   char      emb_vk_kz[2];
   double    tara;
   double    tara_proz;
   double    unt_emb;
   double    a_pfa;
};
extern struct A_EMB a_emb, a_emb_null;

#line 7 "a_emb.rh"

class A_EMB_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_EMB_CLASS () : DB_CLASS ()
               {
               }
};
#endif
