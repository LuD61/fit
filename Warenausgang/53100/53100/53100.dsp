# Microsoft Developer Studio Project File - Name="53100" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=53100 - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "53100.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "53100.mak" CFG="53100 - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "53100 - Win32 Release" (basierend auf  "Win32 (x86) Application")
!MESSAGE "53100 - Win32 Debug" (basierend auf  "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "53100 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "$(INFORMIXDIR)\INCL\ESQL723tc9" /I "$(INFORMIXDIR)\INCL\ESQL" /I "..\INCLUDE" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_PFONT" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib winmm.lib /nologo /subsystem:windows /machine:I386 /out:"d:\user\fit\bin/53100.exe" /libpath:"$(INFORMIXDIR)\LIB"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "53100 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /I "$(INFORMIXDIR)\INCL\ESQL723tc9" /I "$(INFORMIXDIR)\INCL\ESQL" /I "..\INCLUDE" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_PFONT" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib winmm.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"$(INFORMIXDIR)\LIB"
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "53100 - Win32 Release"
# Name "53100 - Win32 Debug"
# Begin Source File

SOURCE=.\53100.cpp
# End Source File
# Begin Source File

SOURCE=.\53100.rc

!IF  "$(CFG)" == "53100 - Win32 Release"

!ELSEIF  "$(CFG)" == "53100 - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\A_BAS.CPP
# End Source File
# Begin Source File

SOURCE=.\a_emb.cpp
# End Source File
# Begin Source File

SOURCE=.\A_HNDW.CPP
# End Source File
# Begin Source File

SOURCE=.\a_kun.cpp
# End Source File
# Begin Source File

SOURCE=.\A_PR.CPP
# End Source File
# Begin Source File

SOURCE=.\Achtung.bmp
# End Source File
# Begin Source File

SOURCE=.\akt_ipr.cpp
# End Source File
# Begin Source File

SOURCE=.\akt_iprz.cpp
# End Source File
# Begin Source File

SOURCE=.\AKT_KRZ.CPP
# End Source File
# Begin Source File

SOURCE=.\AKTION.CPP
# End Source File
# Begin Source File

SOURCE=.\angk.cpp
# End Source File
# Begin Source File

SOURCE=.\angp.cpp
# End Source File
# Begin Source File

SOURCE=.\Application.cpp
# End Source File
# Begin Source File

SOURCE=.\Application.h
# End Source File
# Begin Source File

SOURCE=.\aufkun.cpp
# End Source File
# Begin Source File

SOURCE=.\aufp_txt.cpp
# End Source File
# Begin Source File

SOURCE=.\aufpt.cpp
# End Source File
# Begin Source File

SOURCE=.\auto_nr.cpp
# End Source File
# Begin Source File

SOURCE=.\best_bdf.cpp
# End Source File
# Begin Source File

SOURCE=.\best_res.cpp
# End Source File
# Begin Source File

SOURCE=.\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=..\lib\bmlib.lib
# End Source File
# Begin Source File

SOURCE=.\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\bmp00006.bmp
# End Source File
# Begin Source File

SOURCE=.\bmp00007.bmp
# End Source File
# Begin Source File

SOURCE=.\bsd_buch.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\cmask.cpp
# End Source File
# Begin Source File

SOURCE=.\CMessage.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\colbut.cpp
# End Source File
# Begin Source File

SOURCE=.\ControlPlugins.cpp
# End Source File
# Begin Source File

SOURCE=.\ControlPlugins.h
# End Source File
# Begin Source File

SOURCE=.\DataCollection.cpp
# End Source File
# Begin Source File

SOURCE=.\DataCollection.h
# End Source File
# Begin Source File

SOURCE=.\DATUM.C
# End Source File
# Begin Source File

SOURCE=..\libsrc\dbclass.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\DBFUNC.CPP
# End Source File
# Begin Source File

SOURCE=.\DdeDial.cpp
# End Source File
# Begin Source File

SOURCE=.\Debug.cpp
# End Source File
# Begin Source File

SOURCE=.\Debug.h
# End Source File
# Begin Source File

SOURCE=.\Dialog.cpp
# End Source File
# Begin Source File

SOURCE=.\Dialog.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\dlg.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\dlglst.cpp
# End Source File
# Begin Source File

SOURCE=.\DllPreise.cpp
# End Source File
# Begin Source File

SOURCE=.\Emb.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\enterfunc.cpp
# End Source File
# Begin Source File

SOURCE=.\f5.bmp
# End Source File
# Begin Source File

SOURCE=.\f5.msk.bmp
# End Source File
# Begin Source File

SOURCE=.\fax.ico
# End Source File
# Begin Source File

SOURCE=..\libsrc\FIL.CPP
# End Source File
# Begin Source File

SOURCE=.\fit.ico
# End Source File
# Begin Source File

SOURCE=.\fit0.ico
# End Source File
# Begin Source File

SOURCE=.\fit01.ico
# End Source File
# Begin Source File

SOURCE=.\fit02.ico
# End Source File
# Begin Source File

SOURCE=..\libsrc\FORMFELD.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\help.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\image.cpp
# End Source File
# Begin Source File

SOURCE=..\lib\inflib.lib
# End Source File
# Begin Source File

SOURCE=.\Integer.cpp
# End Source File
# Begin Source File

SOURCE=.\Integer.h
# End Source File
# Begin Source File

SOURCE=.\ipr.cpp
# End Source File
# Begin Source File

SOURCE=.\iprz.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\ITEM.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\itemc.cpp
# End Source File
# Begin Source File

SOURCE=..\lib\kasseex.lib
# End Source File
# Begin Source File

SOURCE=.\kumebest.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\KUN.CPP
# End Source File
# Begin Source File

SOURCE=.\kun_leerg.cpp
# End Source File
# Begin Source File

SOURCE=.\kun_leerg.h
# End Source File
# Begin Source File

SOURCE=.\kunlief.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\lbox.cpp
# End Source File
# Begin Source File

SOURCE=.\leer_bel.cpp
# End Source File
# Begin Source File

SOURCE=.\leer_bel.h
# End Source File
# Begin Source File

SOURCE=.\leer_lsdr.cpp
# End Source File
# Begin Source File

SOURCE=.\leer_lsdr.h
# End Source File
# Begin Source File

SOURCE=.\leer_lsh.cpp
# End Source File
# Begin Source File

SOURCE=.\LeerGut.cpp
# End Source File
# Begin Source File

SOURCE=.\LeerGut.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\listcl.cpp
# End Source File
# Begin Source File

SOURCE=..\include\listcl.h
# End Source File
# Begin Source File

SOURCE=.\lizenz.cpp
# End Source File
# Begin Source File

SOURCE=.\lizenz.h
# End Source File
# Begin Source File

SOURCE=.\LS.CPP
# End Source File
# Begin Source File

SOURCE=.\ls_txt.cpp
# End Source File
# Begin Source File

SOURCE=.\LskList.cpp
# End Source File
# Begin Source File

SOURCE=.\LskList.h
# End Source File
# Begin Source File

SOURCE=.\lsp_txt.cpp
# End Source File
# Begin Source File

SOURCE=.\lspt.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MDN.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\message.cpp
# End Source File
# Begin Source File

SOURCE=.\MessageDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\MessageDialog.h
# End Source File
# Begin Source File

SOURCE=.\MO_A_PR.CPP
# End Source File
# Begin Source File

SOURCE=.\mo_abasis.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_arg.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_atxtl.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_aufl.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_auto.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_ch.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_choise.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_chq.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_chqex.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_CURSO.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_DRAW.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_EINH.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_enter.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_gdruck.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_inf.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_kf.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_kompl.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_last.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_leergut.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_leergut.h
# End Source File
# Begin Source File

SOURCE=.\mo_lgr.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_lsgen.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MELD.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MENU.CPP
# End Source File
# Begin Source File

SOURCE=.\mo_nr.cpp
# End Source File
# Begin Source File

SOURCE=.\MO_PREIS.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_progcfg.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_qa.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_qtou.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_qvertr.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_smtg.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_stda.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_vers.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_wmess.cpp
# End Source File
# Begin Source File

SOURCE=.\NewCalculate.cpp
# End Source File
# Begin Source File

SOURCE=.\NewCalculate.h
# End Source File
# Begin Source File

SOURCE=.\NoIcon.ico
# End Source File
# Begin Source File

SOURCE=.\oarrow.cur
# End Source File
# Begin Source File

SOURCE=..\libsrc\Process.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\Property.cpp
# End Source File
# Begin Source File

SOURCE=.\prov_satz.cpp
# End Source File
# Begin Source File

SOURCE=.\PTAB.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\RBrush.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\rdonly.cpp
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\RFont.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\RowEvent.cpp
# End Source File
# Begin Source File

SOURCE=..\include\RowEvent.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\RPen.cpp
# End Source File
# Begin Source File

SOURCE=.\scanean.cpp
# End Source File
# Begin Source File

SOURCE=.\searcha.cpp
# End Source File
# Begin Source File

SOURCE=.\searchaart.h
# End Source File
# Begin Source File

SOURCE=.\searchcharge.cpp
# End Source File
# Begin Source File

SOURCE=.\searcherror.cpp
# End Source File
# Begin Source File

SOURCE=.\searchkun.cpp
# End Source File
# Begin Source File

SOURCE=.\searchkuncc.cpp
# End Source File
# Begin Source File

SOURCE=.\searchkunlief.cpp
# End Source File
# Begin Source File

SOURCE=.\searchliefkun.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\searchptab.cpp
# End Source File
# Begin Source File

SOURCE=.\searchstdfilk.cpp
# End Source File
# Begin Source File

SOURCE=.\SmtHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\SmtHandler.h
# End Source File
# Begin Source File

SOURCE=.\Sounds.cpp
# End Source File
# Begin Source File

SOURCE=.\stdfilaufk.cpp
# End Source File
# Begin Source File

SOURCE=.\stdfilaufp.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\STDFKT.CPP
# End Source File
# Begin Source File

SOURCE=.\stnd_auf.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\STRFKT.CPP
# End Source File
# Begin Source File

SOURCE=.\sys_par.cpp
# End Source File
# Begin Source File

SOURCE=.\telefon.ico
# End Source File
# Begin Source File

SOURCE=..\libsrc\Text.cpp
# End Source File
# Begin Source File

SOURCE=.\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\tou.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\VBrush.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\Vector.cpp
# End Source File
# Begin Source File

SOURCE=.\VERTR.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\VFont.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\VPen.cpp
# End Source File
# Begin Source File

SOURCE=.\WaWiePro.cpp
# End Source File
# Begin Source File

SOURCE=.\WaWiePro.h
# End Source File
# Begin Source File

SOURCE=.\wiepro.cpp
# End Source File
# Begin Source File

SOURCE=.\wiepro.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\wmaskc.cpp
# End Source File
# Begin Source File

SOURCE=.\wo_tou.cpp
# End Source File
# End Target
# End Project
