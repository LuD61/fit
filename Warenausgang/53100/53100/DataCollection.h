// DataCollection.h: Schnittstelle f�r die Klasse CDataCollection.
//
//////////////////////////////////////////////////////////////////////
#ifndef DATA_COLLECTION_DEF
#define DATA_COLLECTION_DEF

#include <windows.h>

template <class T> 
class CDataCollection 
{
public:
	int bSize;
	T Arr[0x1000];
	int anz;
	int pos;

CDataCollection()
{
//	Arr = NULL;
	anz = 0;
	pos = 0;
	bSize = 0x1000;
}

~CDataCollection()
{
/*
	if (Arr != NULL)
	{
		delete Arr;
	}
*/
}

void Destroy()
{
/*
	if (Arr != NULL)
	{
		delete Arr;
	}
*/
}

void Clear()
{
	anz = 0;
	pos = 0;
}

void Start ()
{
	pos = 0;
}

void Add (T Element)
{
/*
	if (Arr == NULL)
	{
		Arr = new T [bSize];
	}
*/
	if (anz == bSize) return;
    memcpy (&Arr[anz], &Element, sizeof (T));
    anz ++;
}

void Add (T *Element)
{
/*
	if (Arr == NULL)
	{
		Arr = new T [bSize];
	}
*/
	if (anz == bSize) return;
    memcpy (&Arr[anz], Element, sizeof (T));
    anz ++;
}

BOOL Drop (int idx)
{
	int i;

    if (idx >= anz)
    {
        return FALSE;
    }

    anz --;
    for (i = idx; i < anz; i ++)
    {
        memcpy (&Arr[i], &Arr[i + 1], sizeof (T));
    }
    return TRUE;
}

T* GetNext (void)
{
    if (pos >= anz)
    {
        return NULL;
    }
    pos ++;
    return &Arr [pos - 1];
}

T* Get (int idx)
{
    if (idx >= anz)
    {
        return NULL;
    }
    return &Arr [idx];
}
};

#endif
