#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_emb.h"

struct A_EMB a_emb, a_emb_null;

void A_EMB_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &a_emb.emb, 3, 0);
    out_quest ((char *) &a_emb.anz_emb,1,0);
    out_quest ((char *) &a_emb.delstatus,1,0);
    out_quest ((char *) &a_emb.emb,3,0);
    out_quest ((char *) a_emb.emb_bz,0,25);
    out_quest ((char *) a_emb.emb_vk_kz,0,2);
    out_quest ((char *) &a_emb.tara,3,0);
    out_quest ((char *) &a_emb.tara_proz,3,0);
    out_quest ((char *) &a_emb.unt_emb,3,0);
    out_quest ((char *) &a_emb.a_pfa,3,0);
            cursor = prepare_sql ("select a_emb.anz_emb,  "
"a_emb.delstatus,  a_emb.emb,  a_emb.emb_bz,  a_emb.emb_vk_kz,  "
"a_emb.tara,  a_emb.tara_proz,  a_emb.unt_emb,  a_emb.a_pfa from a_emb "

#line 26 "a_emb.rpp"
                                  "where emb = ?");
    ins_quest ((char *) &a_emb.anz_emb,1,0);
    ins_quest ((char *) &a_emb.delstatus,1,0);
    ins_quest ((char *) &a_emb.emb,3,0);
    ins_quest ((char *) a_emb.emb_bz,0,25);
    ins_quest ((char *) a_emb.emb_vk_kz,0,2);
    ins_quest ((char *) &a_emb.tara,3,0);
    ins_quest ((char *) &a_emb.tara_proz,3,0);
    ins_quest ((char *) &a_emb.unt_emb,3,0);
    ins_quest ((char *) &a_emb.a_pfa,3,0);
            sqltext = "update a_emb set a_emb.anz_emb = ?,  "
"a_emb.delstatus = ?,  a_emb.emb = ?,  a_emb.emb_bz = ?,  "
"a_emb.emb_vk_kz = ?,  a_emb.tara = ?,  a_emb.tara_proz = ?,  "
"a_emb.unt_emb = ?,  a_emb.a_pfa = ? "

#line 28 "a_emb.rpp"
                                  "where emb = ?";
            ins_quest ((char *) &a_emb.emb, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &a_emb.emb, 3, 0);
            test_upd_cursor = prepare_sql ("select emb from a_emb "
                                  "where emb = ?");
            ins_quest ((char *) &a_emb.emb, 3, 0);
            del_cursor = prepare_sql ("delete from a_emb "
                                  "where emb = ?");
    ins_quest ((char *) &a_emb.anz_emb,1,0);
    ins_quest ((char *) &a_emb.delstatus,1,0);
    ins_quest ((char *) &a_emb.emb,3,0);
    ins_quest ((char *) a_emb.emb_bz,0,25);
    ins_quest ((char *) a_emb.emb_vk_kz,0,2);
    ins_quest ((char *) &a_emb.tara,3,0);
    ins_quest ((char *) &a_emb.tara_proz,3,0);
    ins_quest ((char *) &a_emb.unt_emb,3,0);
    ins_quest ((char *) &a_emb.a_pfa,3,0);
            ins_cursor = prepare_sql ("insert into a_emb ("
"anz_emb,  delstatus,  emb,  emb_bz,  emb_vk_kz,  tara,  tara_proz,  unt_emb,  a_pfa) "

#line 39 "a_emb.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?)"); 

#line 41 "a_emb.rpp"
}
