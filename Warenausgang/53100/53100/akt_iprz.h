#ifndef _AKT_IPRZ_DEF
#define _AKT_IPRZ_DEF

#include "dbclass.h"

struct AKT_IPRZ {
   long      mdn;
   long      pr_gr_stuf;
   long      kun_pr;
   double    a;
   double    aki_pr;
   long      aki_von;
   long      aki_bis;
   double    ld_pr;
   short     aktion_nr;
   char      a_akt_kz[2];
   char      modif[2];
   short     waehrung;
   long      kun;
   double    a_grund;
   char      kond_art[5];
};
extern struct AKT_IPRZ akt_iprz, akt_iprz_null;

#line 7 "akt_iprz.rh"

class AKT_IPRZ_CLASS : public DB_CLASS 
{
       private :
               int cursor_a;
               int cursor_gr;
               int del_a_curs;
               int del_gr_curs; 
               void prepare (void);
               void prepare_a (char *);
               void prepare_gr (char *);
       public :
               AKT_IPRZ_CLASS () : DB_CLASS (),
                                  cursor_a (-1) ,
                                  cursor_gr (-1) ,
                                  del_a_curs (-1) ,
                                  del_gr_curs (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_a (char *);
               int dbread_a (void);
               int dbreadfirst_gr (char *);
               int dbread_gr (void);
               int dbdelete_a (void);
               int dbdelete_gr (void);
               int dbclose_a (void);
               int dbclose_gr (void);
};
#endif
