#ifndef _EMB_DEF
#define _EMB_DEF
#include "a_emb.h"
#include "Text.h"
#include "Vector.h"

class EmbZweig 
{
  public :
   short     anz_emb;
   short     delstatus;
   double    emb;
   char      emb_bz[25];
   char      emb_vk_kz[2];
   double    tara;
   double    tara_proz;
   double    unt_emb;
   double    a_pfa;

   EmbZweig ();
   ~EmbZweig ();
   void SetEmb ();
};

class Emb : public A_EMB_CLASS
{
  private :
     int anz;
     double emb;
	 double a;
	 CVector Embs;

  public :
	Emb ();
	~Emb ();
	void Init ();
	int Read (double);
	int GetAnz ();
	double GetA ();
	double GetEmb ();
};


#endif
