#ifndef _DDEDIAL_DEF
#define _DDEDIAL_DEF
#include <ddeml.h>

#define DDE_TIMEOUT 3000

class DdeDial
{ 
      private :
          DWORD idInst;
          static char *szServer;
          static char szService[];
          static char szTopic [];
          static char szItem [];
          HSZ hszService;
          HSZ hszTopic;
          HSZ hszItem;
          HCONV hConv;
      public :
          DdeDial ();
          DdeDial (char *, char *, char *, char *);

          ~DdeDial ()
          {
              DdeUninitialize (idInst);
          }

          void SetServer (char *);
          void SetService (char *);
          void SetTopic (char *);
          void SetItem (char *);

          void Call (LPSTR);
          int Exec (LPSTR, WORD, int, int, int, int);
          int Exec (LPSTR, WORD);
          static HDDEDATA CALLBACK DdeCallback (UINT, UINT, HCONV,
                                                HSZ, HSZ, HDDEDATA,
                                                DWORD, DWORD);
};

#endif