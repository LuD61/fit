#ifndef _MO_AQDEF
#define _MO_AQDEF

class QueryClass
{
private :
	    BOOL lutz_mdn_par;
public :
	    BOOL (*testa_kun) (double a);
	    QueryClass () : lutz_mdn_par (0)
        {
			testa_kun = NULL;
        }
		void SetLutzMdnPar (BOOL par)
		{
			lutz_mdn_par = par;
		}
        int querya (HWND);
        int searcha (HWND hWnd);
        int queryauf (HWND);
        int queryaufex (HWND, BOOL);
        int queryauftou (HWND);
        int querykun (HWND);
        int querya_direct (HWND, char *);
        int searcha_direct (HWND, char *);
        int searchkun_direct (HWND, char *);
        int queryang (HWND);
        int queryangex (HWND, BOOL);
};
#endif
