#include <windows.h>
#include "searchkuncc.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "Text.h"


struct SKUNCC *SEARCHKUNR::skuntab = NULL;
struct SKUNCC SEARCHKUNR::skun;
int SEARCHKUNR::idx = -1;
long SEARCHKUNR::kunanz;
CHQEX *SEARCHKUNR::Query = NULL;
DB_CLASS SEARCHKUNR::DbClass;
HINSTANCE SEARCHKUNR::hMainInst;
HWND SEARCHKUNR::hMainWindow;
HWND SEARCHKUNR::awin;
int SEARCHKUNR::SearchField = 1;
int SEARCHKUNR::sokun = 1;
int SEARCHKUNR::soadr_nam1 = 1;
int SEARCHKUNR::soplz = 1;
int SEARCHKUNR::soort1 = 1;
short SEARCHKUNR::mdn = 0;
short SEARCHKUNR::ccmarkt = 0;
static BOOL ScannMode = TRUE;

int SEARCHKUNR::sortkun (const void *elem1, const void *elem2)
{
	      struct SKUNCC *el1;
	      struct SKUNCC *el2;

	      el1 = (struct SKUNCC *) elem1;
		  el2 = (struct SKUNCC *) elem2;
          if (atol (el1->kun) > atol (el2->kun))
          {
              return 1 * sokun;
          }
          if (atol (el1->kun) < atol (el2->kun))
          {
              return -1 * sokun;
          }
	      return 0 * sokun;
}

void SEARCHKUNR::SortKun (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUNCC),
				   sortkun);
       sokun *= -1;
}

int SEARCHKUNR::sortadr_nam1 (const void *elem1, const void *elem2)
{
	      struct SKUNCC *el1;
	      struct SKUNCC *el2;

		  el1 = (struct SKUNCC *) elem1;
		  el2 = (struct SKUNCC *) elem2;
          clipped (el1->adr_nam1);
          clipped (el2->adr_nam1);
          if (strlen (el1->adr_nam1) == 0 &&
              strlen (el2->adr_nam1) == 0)
          {
              return 0;
          }
          if (strlen (el1->adr_nam1) == 0)
          {
              return -1;
          }
          if (strlen (el2->adr_nam1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->adr_nam1,el2->adr_nam1) * soadr_nam1);
}

void SEARCHKUNR::SortAdr_nam1 (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUNCC),
				   sortadr_nam1);
       soadr_nam1 *= -1;
   	   ScannMode = FALSE;
 	   Query->CheckName ("check", ScannMode);

}

int SEARCHKUNR::sortplz (const void *elem1, const void *elem2)
{
	      struct SKUNCC *el1;
	      struct SKUNCC *el2;

		  el1 = (struct SKUNCC *) elem1;
		  el2 = (struct SKUNCC *) elem2;
          clipped (el1->plz);
          clipped (el2->plz);
          if (strlen (el1->plz) == 0 &&
              strlen (el2->plz) == 0)
          {
              return 0;
          }
          if (strlen (el1->plz) == 0)
          {
              return -1;
          }
          if (strlen (el2->plz) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->plz,el2->plz) * soplz);
}

void SEARCHKUNR::SortPlz (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUNCC),
				   sortplz);
       soplz *= -1;
   	   ScannMode = FALSE;
 	   Query->CheckName ("check", ScannMode);
}

int SEARCHKUNR::sortort1 (const void *elem1, const void *elem2)
{
	      struct SKUNCC *el1;
	      struct SKUNCC *el2;

		  el1 = (struct SKUNCC *) elem1;
		  el2 = (struct SKUNCC *) elem2;
          clipped (el1->ort1);
          clipped (el2->ort1);
          if (strlen (el1->ort1) == 0 &&
              strlen (el2->ort1) == 0)
          {
              return 0;
          }
          if (strlen (el1->ort1) == 0)
          {
              return -1;
          }
          if (strlen (el2->ort1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->ort1,el2->ort1) * soort1);
}

void SEARCHKUNR::SortOrt1 (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUNCC),
				   sortort1);
       soort1 *= -1;
   	   ScannMode = FALSE;
 	   Query->CheckName ("check", ScannMode);
}

void SEARCHKUNR::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortKun (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortAdr_nam1 (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortPlz (hWnd);
                  SearchField = 2;
                  break;
              case 3 :
                  SortOrt1 (hWnd);
                  SearchField = 3;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHKUNR::FillFormat (char *buffer)
{
         sprintf (buffer, "%s %s %s %s %s",
                          "%d",
						  "%s",
						  "%d",
						  "%s",
                          "%s");
}


void SEARCHKUNR::FillCaption (char *buffer)
{
	    sprintf (buffer, " %10s  %-27s  %-10s  %-21s  %-37s", " Kunde",
                          " Name1",
						  " Lieferschein",
                          " PLZ",
                          "  Ort");
}

void SEARCHKUNR::FillVlines (char *buffer)
{
	    sprintf (buffer, " %11s  %28s  %11s  %22s  %38s",
                         "1", "1", "1", "1");
}


void SEARCHKUNR::FillRec (char *buffer, int i)
{
 	      sprintf (buffer, " %8ld     |%-26.26s|    %8ld    |%-20s|    |%-36s|", 
                            atol (skuntab[i].kun), 
                            skuntab[i].adr_nam1,
							atol (skuntab[i].ls),
                            skuntab[i].plz,
                            skuntab[i].ort1);
}


void SEARCHKUNR::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < kunanz; i ++)
       {
          FillRec (buffer, i);
	      Query->UpdateRecord (buffer, i);
       }
}


int SEARCHKUNR::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (skuntab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < kunanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
                   strcpy (sabuff, skuntab[i].kun);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (atol (sebuff) == atol (sabuff)) break;
           }
           else if (SearchField == 1)
           {
		           if (strupcmp (sebuff, skuntab[i].adr_nam1, len) == 0) break;
           }
           else if (SearchField == 2)
           {
		           if (strupcmp (sebuff, skuntab[i].plz, len) == 0) break;
           }
           else if (SearchField == 3)
           {
		           if (strupcmp (sebuff, skuntab[i].ort1, len) == 0) break;
           }
	   }
	   if (i == kunanz) return 0;
	   Query->SetSel (i);
	   return 0;
}

int SEARCHKUNR::ReadKun (char *kun_krz1)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (skuntab) 
	  {
           delete skuntab;
           skuntab = NULL;
	  }


      idx = -1;
	  kunanz = 0;
      SearchField = 0;
      Query->SetSBuff ("");
      clipped (kun_krz1);
      if (strcmp (kun_krz1, " ") == 0) kun_krz1[0] = 0;

      sprintf (buffer, "select count (*) from kun where kun_krz1 matches \"%s*\"", kun_krz1);
      DbClass.sqlout ((int *) &kunanz, 2, 0);
	  DbClass.sqlcomm (buffer);

      if (kunanz == 0) kunanz = 0x10000;

	  skuntab = new struct SKUNCC [kunanz];
      clipped (kun_krz1);
/*
      if (strlen (kun_krz1) == 0 ||
          kun_krz1[0] <= ' ')
*/
      {
	          sprintf (buffer, "select lsk.kun, lsk.ls, adr.adr_nam1, adr.plz, adr.ort1 "
	 			       "from lsk, kun, adr "
                       "where lsk.lieferdat = today "
					   "and lsk.ls_stat = 2 "
					   "and lsk.ccmarkt = %hd "
					   "and kun.kun = lsk.kun "
					   "and kun.mdn = lsk.mdn "
					   "and kun.mdn = %hd "
                       "and adr.adr = kun.adr1", ccmarkt, mdn); 
      }
/*
      else
      {
	          sprintf (buffer, "select kun, adr.adr_nam1, adr.plz, adr.ort1 "
	 			       "from kun, adr "
                       "where kun.mdn = %hd "
                       "and adr.adr = kun.adr1 "
                       "and kun_krz1 matches \"%s*\"", mdn, kun_krz1);
      }
*/
      DbClass.sqlout ((char *) skun.kun, 0, 9);
      DbClass.sqlout ((char *) skun.ls, 0, 9);
      DbClass.sqlout ((char *) skun.adr_nam1, 0, 37);
      DbClass.sqlout ((char *) skun.plz, 0, 21);
      DbClass.sqlout ((char *) skun.ort1, 0, 37);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		  memcpy (&skuntab[i], &skun, sizeof (struct SKUNCC));
          FillRec (buffer, i);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      kunanz = i;
	  DbClass.sqlclose (cursor);
      sokun = 1;
      if (kunanz > 1)
      {
             SortKun (Query->GethWnd ());
      }
      UpdateList ();
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHKUNR::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < kunanz; i ++)
      {
          FillRec (buffer, i);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHKUNR::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHKUNR::SearchKun (short mdn, short ccmarkt)
{
	  char buffer [256];
      RECT rect;

      if (mdn == 0)
      {
          disp_mess ("Mandant 0 ist nicht erlaubt", 2);
          syskey = KEY5;
          return;
      }
      this->mdn = mdn;
      if (ccmarkt == 0)
      {
          disp_mess ("ccmarkt nicht aktiv", 2);
          syskey = KEY5;
          return;
      }
      this->ccmarkt = ccmarkt;

      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      SetSortProc (SortLst);
//      EnableWindow (hMainWindow, FALSE);
      if (Query == NULL)
      {
        idx = -1;
//        Query = new CHQEX (cx, cy, "Name", "");
//        Query = new CHQEX (cx, cy);
        Query = new CHQEX (cx, cy, "Scann-Modus");
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (ReadKun);
	    Query->SetSearchLst (SearchLst);
		ReadKun ("");
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (ReadKun);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
	  Query->SetSBuffSet (FALSE);
	  Query->CheckName ("check", ScannMode);
	  Query->ProcessMessages ();
//      EnableWindow (hMainWindow, TRUE);
      idx = Query->GetSel ();
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (skuntab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) 
	  {
 	      delete skuntab;
          skuntab = NULL;
          delete Query;
          Query = NULL;
		  return;
	  }
	  memcpy (&skun, &skuntab[idx], sizeof (skun));
      ScannMode = Query->GetCheckBoxValue ();
	  if (ScannMode)
	  {
	      char *s = Query->GetSBuff ();
		  Text Sb = s;
		  Text Kun = skun.kun;
		  Sb.TrimRight ();
		  Kun.TrimRight ();
		  if (Sb > " " && Sb != Kun)
		  {
			  print_mess (2, "Falsche Kundennummer %ld", atol (Sb.GetBuffer ()));
			  idx = -1;
		  }
	  }
      SetSortProc (NULL);
	  delete skuntab;
      skuntab = NULL;
      delete Query;
      Query = NULL;
/*
      if (skuntab != NULL)
      {
          Query->SavefWork ();
      }
*/
}


SKUNCC *SEARCHKUNR::GetNextSkun (void)
{
      if (idx == -1) return NULL;
      if (idx < kunanz - 1) 
      {
             idx ++;
      }
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}


SKUNCC *SEARCHKUNR::GetPriorSkun (void)
{
      if (idx == -1) return NULL;
      if (idx > 0) 
      {
             idx --;
      }
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}

SKUNCC *SEARCHKUNR::GetFirstSkun (void)
{
      if (idx == -1) return NULL;
      idx = 0;
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}

SKUNCC *SEARCHKUNR::GetLastSkun (void)
{
      if (idx == -1) return NULL;
      idx = kunanz - 1;
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}



