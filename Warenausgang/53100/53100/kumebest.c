#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "kumebest.h"
#include "dbclass.h"
#include "dbfunc.h"

struct KUMEBEST kumebest, kumebest_null;

static char *sqltext;

void KMB_CLASS::prepare (void)
{
         ins_quest ((char *) &kumebest.mdn, 1, 0);
         ins_quest ((char *) &kumebest.fil, 1, 0);  
         ins_quest ((char *) &kumebest.kun, 2, 0);
         ins_quest ((char *) &kumebest.a, 3, 0); 

    out_quest ((char *) &kumebest.mdn,1,0);
    out_quest ((char *) &kumebest.fil,1,0);
    out_quest ((char *) &kumebest.kun,2,0);
    out_quest ((char *) kumebest.kun_bran2,0,3);
    out_quest ((char *) &kumebest.a,3,0);
    out_quest ((char *) &kumebest.tara0,3,0);
    out_quest ((char *) &kumebest.me_einh0,1,0);
    out_quest ((char *) &kumebest.inh0,3,0);
    out_quest ((char *) &kumebest.me_einh1,1,0);
    out_quest ((char *) &kumebest.inh1,3,0);
    out_quest ((char *) &kumebest.tara1,3,0);
    out_quest ((char *) &kumebest.me_einh2,1,0);
    out_quest ((char *) &kumebest.inh2,3,0);
    out_quest ((char *) &kumebest.tara2,3,0);
    out_quest ((char *) &kumebest.me_einh3,1,0);
    out_quest ((char *) &kumebest.inh3,3,0);
    out_quest ((char *) &kumebest.tara3,3,0);
    out_quest ((char *) &kumebest.me_einh4,1,0);
    out_quest ((char *) &kumebest.inh4,3,0);
    out_quest ((char *) &kumebest.tara4,3,0);
    out_quest ((char *) &kumebest.me_einh5,1,0);
    out_quest ((char *) &kumebest.inh5,3,0);
    out_quest ((char *) &kumebest.tara5,3,0);
    out_quest ((char *) &kumebest.me_einh6,1,0);
    out_quest ((char *) &kumebest.inh6,3,0);
    out_quest ((char *) &kumebest.tara6,3,0);
    out_quest ((char *) &kumebest.knuepf1,1,0);
    out_quest ((char *) &kumebest.knuepf2,1,0);
    out_quest ((char *) &kumebest.knuepf3,1,0);
    out_quest ((char *) &kumebest.knuepf4,1,0);
    out_quest ((char *) &kumebest.knuepf5,1,0);
    out_quest ((char *) &kumebest.knuepf6,1,0);
    out_quest ((char *) &kumebest.ean_su1,3,0);
    out_quest ((char *) &kumebest.ean_su2,3,0);
         cursor = prepare_sql ("select kumebest.mdn,  "
"kumebest.fil,  kumebest.kun,  kumebest.kun_bran2,  kumebest.a,  "
"kumebest.tara0,  kumebest.me_einh0,  kumebest.inh0,  "
"kumebest.me_einh1,  kumebest.inh1,  kumebest.tara1,  "
"kumebest.me_einh2,  kumebest.inh2,  kumebest.tara2,  "
"kumebest.me_einh3,  kumebest.inh3,  kumebest.tara3,  "
"kumebest.me_einh4,  kumebest.inh4,  kumebest.tara4,  "
"kumebest.me_einh5,  kumebest.inh5,  kumebest.tara5,  "
"kumebest.me_einh6,  kumebest.inh6,  kumebest.tara6,  kumebest.knuepf1,  "
"kumebest.knuepf2,  kumebest.knuepf3,  kumebest.knuepf4,  "
"kumebest.knuepf5,  kumebest.knuepf6,  kumebest.ean_su1,  "
"kumebest.ean_su2 from kumebest "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   a   = ? ");

         ins_quest ((char *) &kumebest.mdn, 1, 0);
         ins_quest ((char *) &kumebest.fil, 1, 0);  
         ins_quest ((char *) kumebest.kun_bran2, 0, 2);
         ins_quest ((char *) &kumebest.a, 3, 0); 

    out_quest ((char *) &kumebest.mdn,1,0);
    out_quest ((char *) &kumebest.fil,1,0);
    out_quest ((char *) &kumebest.kun,2,0);
    out_quest ((char *) kumebest.kun_bran2,0,3);
    out_quest ((char *) &kumebest.a,3,0);
    out_quest ((char *) &kumebest.tara0,3,0);
    out_quest ((char *) &kumebest.me_einh0,1,0);
    out_quest ((char *) &kumebest.inh0,3,0);
    out_quest ((char *) &kumebest.me_einh1,1,0);
    out_quest ((char *) &kumebest.inh1,3,0);
    out_quest ((char *) &kumebest.tara1,3,0);
    out_quest ((char *) &kumebest.me_einh2,1,0);
    out_quest ((char *) &kumebest.inh2,3,0);
    out_quest ((char *) &kumebest.tara2,3,0);
    out_quest ((char *) &kumebest.me_einh3,1,0);
    out_quest ((char *) &kumebest.inh3,3,0);
    out_quest ((char *) &kumebest.tara3,3,0);
    out_quest ((char *) &kumebest.me_einh4,1,0);
    out_quest ((char *) &kumebest.inh4,3,0);
    out_quest ((char *) &kumebest.tara4,3,0);
    out_quest ((char *) &kumebest.me_einh5,1,0);
    out_quest ((char *) &kumebest.inh5,3,0);
    out_quest ((char *) &kumebest.tara5,3,0);
    out_quest ((char *) &kumebest.me_einh6,1,0);
    out_quest ((char *) &kumebest.inh6,3,0);
    out_quest ((char *) &kumebest.tara6,3,0);
    out_quest ((char *) &kumebest.knuepf1,1,0);
    out_quest ((char *) &kumebest.knuepf2,1,0);
    out_quest ((char *) &kumebest.knuepf3,1,0);
    out_quest ((char *) &kumebest.knuepf4,1,0);
    out_quest ((char *) &kumebest.knuepf5,1,0);
    out_quest ((char *) &kumebest.knuepf6,1,0);
    out_quest ((char *) &kumebest.ean_su1,3,0);
    out_quest ((char *) &kumebest.ean_su2,3,0);
         cursor_bra = prepare_sql ("select kumebest.mdn,  "
"kumebest.fil,  kumebest.kun,  kumebest.kun_bran2,  kumebest.a,  "
"kumebest.tara0,  kumebest.me_einh0,  kumebest.inh0,  "
"kumebest.me_einh1,  kumebest.inh1,  kumebest.tara1,  "
"kumebest.me_einh2,  kumebest.inh2,  kumebest.tara2,  "
"kumebest.me_einh3,  kumebest.inh3,  kumebest.tara3,  "
"kumebest.me_einh4,  kumebest.inh4,  kumebest.tara4,  "
"kumebest.me_einh5,  kumebest.inh5,  kumebest.tara5,  "
"kumebest.me_einh6,  kumebest.inh6,  kumebest.tara6,  kumebest.knuepf1,  "
"kumebest.knuepf2,  kumebest.knuepf3,  kumebest.knuepf4,  "
"kumebest.knuepf5,  kumebest.knuepf6,  kumebest.ean_su1,  "
"kumebest.ean_su2 from kumebest "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun_bran2 = ? "
							   "and kun = 0 "
                               "and   a   = ? ");
         ins_quest ((char *) &kumebest.mdn, 1, 0);
         ins_quest ((char *) &kumebest.fil, 1, 0);  
         ins_quest ((char *) &kumebest.a, 3, 0); 

    out_quest ((char *) &kumebest.mdn,1,0);
    out_quest ((char *) &kumebest.fil,1,0);
    out_quest ((char *) &kumebest.kun,2,0);
    out_quest ((char *) kumebest.kun_bran2,0,3);
    out_quest ((char *) &kumebest.a,3,0);
    out_quest ((char *) &kumebest.tara0,3,0);
    out_quest ((char *) &kumebest.me_einh0,1,0);
    out_quest ((char *) &kumebest.inh0,3,0);
    out_quest ((char *) &kumebest.me_einh1,1,0);
    out_quest ((char *) &kumebest.inh1,3,0);
    out_quest ((char *) &kumebest.tara1,3,0);
    out_quest ((char *) &kumebest.me_einh2,1,0);
    out_quest ((char *) &kumebest.inh2,3,0);
    out_quest ((char *) &kumebest.tara2,3,0);
    out_quest ((char *) &kumebest.me_einh3,1,0);
    out_quest ((char *) &kumebest.inh3,3,0);
    out_quest ((char *) &kumebest.tara3,3,0);
    out_quest ((char *) &kumebest.me_einh4,1,0);
    out_quest ((char *) &kumebest.inh4,3,0);
    out_quest ((char *) &kumebest.tara4,3,0);
    out_quest ((char *) &kumebest.me_einh5,1,0);
    out_quest ((char *) &kumebest.inh5,3,0);
    out_quest ((char *) &kumebest.tara5,3,0);
    out_quest ((char *) &kumebest.me_einh6,1,0);
    out_quest ((char *) &kumebest.inh6,3,0);
    out_quest ((char *) &kumebest.tara6,3,0);
    out_quest ((char *) &kumebest.knuepf1,1,0);
    out_quest ((char *) &kumebest.knuepf2,1,0);
    out_quest ((char *) &kumebest.knuepf3,1,0);
    out_quest ((char *) &kumebest.knuepf4,1,0);
    out_quest ((char *) &kumebest.knuepf5,1,0);
    out_quest ((char *) &kumebest.knuepf6,1,0);
    out_quest ((char *) &kumebest.ean_su1,3,0);
    out_quest ((char *) &kumebest.ean_su2,3,0);
         cursor_a = prepare_sql ("select kumebest.mdn,  "
"kumebest.fil,  kumebest.kun,  kumebest.kun_bran2,  kumebest.a,  "
"kumebest.tara0,  kumebest.me_einh0,  kumebest.inh0,  "
"kumebest.me_einh1,  kumebest.inh1,  kumebest.tara1,  "
"kumebest.me_einh2,  kumebest.inh2,  kumebest.tara2,  "
"kumebest.me_einh3,  kumebest.inh3,  kumebest.tara3,  "
"kumebest.me_einh4,  kumebest.inh4,  kumebest.tara4,  "
"kumebest.me_einh5,  kumebest.inh5,  kumebest.tara5,  "
"kumebest.me_einh6,  kumebest.inh6,  kumebest.tara6,  kumebest.knuepf1,  "
"kumebest.knuepf2,  kumebest.knuepf3,  kumebest.knuepf4,  "
"kumebest.knuepf5,  kumebest.knuepf6,  kumebest.ean_su1,  "
"kumebest.ean_su2 from kumebest "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   (kun_bran2 = \"0\" "
							   "or    kun_bran2 is NULL "
                                             "or    kun_bran2 <= \"  \") "
                               "and   kun = 0 " 
                               "and   a   = ? ");

    ins_quest ((char *) &kumebest.mdn,1,0);
    ins_quest ((char *) &kumebest.fil,1,0);
    ins_quest ((char *) &kumebest.kun,2,0);
    ins_quest ((char *) kumebest.kun_bran2,0,3);
    ins_quest ((char *) &kumebest.a,3,0);
    ins_quest ((char *) &kumebest.tara0,3,0);
    ins_quest ((char *) &kumebest.me_einh0,1,0);
    ins_quest ((char *) &kumebest.inh0,3,0);
    ins_quest ((char *) &kumebest.me_einh1,1,0);
    ins_quest ((char *) &kumebest.inh1,3,0);
    ins_quest ((char *) &kumebest.tara1,3,0);
    ins_quest ((char *) &kumebest.me_einh2,1,0);
    ins_quest ((char *) &kumebest.inh2,3,0);
    ins_quest ((char *) &kumebest.tara2,3,0);
    ins_quest ((char *) &kumebest.me_einh3,1,0);
    ins_quest ((char *) &kumebest.inh3,3,0);
    ins_quest ((char *) &kumebest.tara3,3,0);
    ins_quest ((char *) &kumebest.me_einh4,1,0);
    ins_quest ((char *) &kumebest.inh4,3,0);
    ins_quest ((char *) &kumebest.tara4,3,0);
    ins_quest ((char *) &kumebest.me_einh5,1,0);
    ins_quest ((char *) &kumebest.inh5,3,0);
    ins_quest ((char *) &kumebest.tara5,3,0);
    ins_quest ((char *) &kumebest.me_einh6,1,0);
    ins_quest ((char *) &kumebest.inh6,3,0);
    ins_quest ((char *) &kumebest.tara6,3,0);
    ins_quest ((char *) &kumebest.knuepf1,1,0);
    ins_quest ((char *) &kumebest.knuepf2,1,0);
    ins_quest ((char *) &kumebest.knuepf3,1,0);
    ins_quest ((char *) &kumebest.knuepf4,1,0);
    ins_quest ((char *) &kumebest.knuepf5,1,0);
    ins_quest ((char *) &kumebest.knuepf6,1,0);
    ins_quest ((char *) &kumebest.ean_su1,3,0);
    ins_quest ((char *) &kumebest.ean_su2,3,0);
         sqltext = "update kumebest set kumebest.mdn = ?,  "
"kumebest.fil = ?,  kumebest.kun = ?,  kumebest.kun_bran2 = ?,  "
"kumebest.a = ?,  kumebest.tara0 = ?,  kumebest.me_einh0 = ?,  "
"kumebest.inh0 = ?,  kumebest.me_einh1 = ?,  kumebest.inh1 = ?,  "
"kumebest.tara1 = ?,  kumebest.me_einh2 = ?,  kumebest.inh2 = ?,  "
"kumebest.tara2 = ?,  kumebest.me_einh3 = ?,  kumebest.inh3 = ?,  "
"kumebest.tara3 = ?,  kumebest.me_einh4 = ?,  kumebest.inh4 = ?,  "
"kumebest.tara4 = ?,  kumebest.me_einh5 = ?,  kumebest.inh5 = ?,  "
"kumebest.tara5 = ?,  kumebest.me_einh6 = ?,  kumebest.inh6 = ?,  "
"kumebest.tara6 = ?,  kumebest.knuepf1 = ?,  kumebest.knuepf2 = ?,  "
"kumebest.knuepf3 = ?,  kumebest.knuepf4 = ?,  kumebest.knuepf5 = ?,  "
"kumebest.knuepf6 = ?,  kumebest.ean_su1 = ?,  kumebest.ean_su2 = ? "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_bran2 = ? "
                               "and   a   = ? ";
  
         ins_quest ((char *) &kumebest.mdn, 1, 0);
         ins_quest ((char *) &kumebest.fil, 1, 0);  
         ins_quest ((char *) &kumebest.kun, 2, 0);
         ins_quest ((char *) kumebest.kun_bran2, 0, 2);
         ins_quest ((char *) &kumebest.a, 3, 0); 

         upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &kumebest.mdn,1,0);
    ins_quest ((char *) &kumebest.fil,1,0);
    ins_quest ((char *) &kumebest.kun,2,0);
    ins_quest ((char *) kumebest.kun_bran2,0,3);
    ins_quest ((char *) &kumebest.a,3,0);
    ins_quest ((char *) &kumebest.tara0,3,0);
    ins_quest ((char *) &kumebest.me_einh0,1,0);
    ins_quest ((char *) &kumebest.inh0,3,0);
    ins_quest ((char *) &kumebest.me_einh1,1,0);
    ins_quest ((char *) &kumebest.inh1,3,0);
    ins_quest ((char *) &kumebest.tara1,3,0);
    ins_quest ((char *) &kumebest.me_einh2,1,0);
    ins_quest ((char *) &kumebest.inh2,3,0);
    ins_quest ((char *) &kumebest.tara2,3,0);
    ins_quest ((char *) &kumebest.me_einh3,1,0);
    ins_quest ((char *) &kumebest.inh3,3,0);
    ins_quest ((char *) &kumebest.tara3,3,0);
    ins_quest ((char *) &kumebest.me_einh4,1,0);
    ins_quest ((char *) &kumebest.inh4,3,0);
    ins_quest ((char *) &kumebest.tara4,3,0);
    ins_quest ((char *) &kumebest.me_einh5,1,0);
    ins_quest ((char *) &kumebest.inh5,3,0);
    ins_quest ((char *) &kumebest.tara5,3,0);
    ins_quest ((char *) &kumebest.me_einh6,1,0);
    ins_quest ((char *) &kumebest.inh6,3,0);
    ins_quest ((char *) &kumebest.tara6,3,0);
    ins_quest ((char *) &kumebest.knuepf1,1,0);
    ins_quest ((char *) &kumebest.knuepf2,1,0);
    ins_quest ((char *) &kumebest.knuepf3,1,0);
    ins_quest ((char *) &kumebest.knuepf4,1,0);
    ins_quest ((char *) &kumebest.knuepf5,1,0);
    ins_quest ((char *) &kumebest.knuepf6,1,0);
    ins_quest ((char *) &kumebest.ean_su1,3,0);
    ins_quest ((char *) &kumebest.ean_su2,3,0);
         ins_cursor = prepare_sql ("insert into kumebest ("
"mdn,  fil,  kun,  kun_bran2,  a,  tara0,  me_einh0,  inh0,  me_einh1,  inh1,  tara1,  "
"me_einh2,  inh2,  tara2,  me_einh3,  inh3,  tara3,  me_einh4,  inh4,  tara4,  me_einh5,  "
"inh5,  tara5,  me_einh6,  inh6,  tara6,  knuepf1,  knuepf2,  knuepf3,  knuepf4,  "
"knuepf5,  knuepf6,  ean_su1,  ean_su2) "
                                   "values "
                                   "(?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

         ins_quest ((char *) &kumebest.mdn, 1, 0);
         ins_quest ((char *) &kumebest.fil, 1, 0);  
         ins_quest ((char *) &kumebest.kun, 2, 0);
         ins_quest ((char *) kumebest.kun_bran2, 0, 2);
         ins_quest ((char *) &kumebest.a, 3, 0); 
         del_cursor = prepare_sql ("delete from kumebest "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_bran2 = ? "
                               "and   a   = ? ");

         ins_quest ((char *) &kumebest.mdn, 1, 0);
         ins_quest ((char *) &kumebest.fil, 1, 0);  
         ins_quest ((char *) &kumebest.kun, 2, 0);
         ins_quest ((char *) kumebest.kun_bran2, 0, 3);
         ins_quest ((char *) &kumebest.a, 3, 0); 
         test_upd_cursor = prepare_sql ("select a from kumebest "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_bran2 = ? "
                               "and   a   = ? ");
}

int KMB_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int KMB_CLASS::dbreadfirst_bra (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         open_sql (cursor_bra);
         fetch_sql (cursor_bra);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int KMB_CLASS::dbread_bra (void)
/**
Naechsten Satz aus Tabelle lesen.
**/
{
         fetch_sql (cursor_bra);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}
int KMB_CLASS::dbreadfirst_a (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         open_sql (cursor_a);
         fetch_sql (cursor_a);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int KMB_CLASS::dbread_a (void)
/**
Naechsten Satz aus Tabelle lesen.
**/
{
         fetch_sql (cursor_a);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}
 
 
