// LeerGut.cpp: Implementierung der Klasse CLeerGut.
//
//////////////////////////////////////////////////////////////////////
#include <windows.h>
#include "mo_menu.h"
#include "LeerGut.h"
#include "strfkt.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CLeerGut::CLeerGut()
{
	init = FALSE;
    kun_leer = 0; 
	leer_konto = FALSE;
}

CLeerGut::~CLeerGut()
{
	if (Tou != -1)
	{
			KunLeerG.sqlclose (Tou);
	}
	KunLeerG.dbclose ();
	LeerBel.dbclose ();

}

void CLeerGut::Init ()
{
	strcpy (sys_par.sys_par_nam, "kun_leer");
	if (Syspar.dbreadfirst () == 0)
	{
		kun_leer = atoi (sys_par.sys_par_wrt);
	}
	strcpy (sys_par.sys_par_nam, "leer_konto");
	if (Syspar.dbreadfirst () == 0)
	{
		leer_konto = atoi (sys_par.sys_par_wrt);
	}
	tou = 0l;
	htou = 0l;
	Tou = -1;
	KunLeerG.sqlin ((long *) &tou, 2, 0);
	KunLeerG.sqlout ((long *) &htou, 2, 0);
	KunLeerG.sqlout ((short *) &eigentou, 2, 0);
	Tou = KunLeerG.sqlcursor ("select htou, eigentour from tou where tou = ?");
	init = TRUE;
}

void CLeerGut::Update (double a, double lief_me, double pr_vk)
{
	char datum [12];

	if (!init) Init ();
	if (kun_leer < 2) return;
	if (lsk.ls_stat < 4) return;

	if (lese_a_bas (a) != 0) return;
	if (_a_bas.a_typ != LeerGut &&
        _a_bas.a_typ2 != LeerGut)
	{
		return;
	}

	sysdate (datum);
	memcpy (&KunLeerG.kun_leerg, &kun_leerg_null, sizeof (KUN_LEERG));
	memcpy (&LeerBel.leer_bel, &leer_bel_null, sizeof (LEER_BEL));
	memcpy (&KunLeerG.kun_leerg.ret_dat, &DB_CLASS::LongNull, sizeof (DB_CLASS::LongNull));
	memcpy (&KunLeerG.kun_leerg.lief_dat, &DB_CLASS::LongNull, sizeof (DB_CLASS::LongNull));

	KunLeerG.kun_leerg.mdn = lsk.mdn;
	KunLeerG.kun_leerg.fil = lsk.fil;
	KunLeerG.kun_leerg.kun = lsk.kun;
	KunLeerG.kun_leerg.kun_fil = lsk.kun_fil;
	KunLeerG.kun_leerg.a   = a;
	KunLeerG.dbreadfirst ();
	LeerBel.leer_bel.bestand = KunLeerG.kun_leerg.bsd_stk;

	if (lief_me < 0)
	{
		KunLeerG.kun_leerg.ret_dat = dasc_to_long (datum);
		strcpy (KunLeerG.kun_leerg.ben_ret, sys_ben.pers_nam);
		strcpy (KunLeerG.kun_leerg.ben_lief, "");
	}
	else
	{
		KunLeerG.kun_leerg.lief_dat = dasc_to_long (datum);
		strcpy (KunLeerG.kun_leerg.ben_lief, sys_ben.pers_nam);
		strcpy (KunLeerG.kun_leerg.ben_ret, "");
	}

	LeerBel.leer_bel.anzahl = KunLeerG.kun_leerg.bsd_stk;
    KunLeerG.kun_leerg.bsd_stk += (long) lief_me;
    KunLeerG.kun_leerg.bsd_wrt = KunLeerG.kun_leerg.bsd_stk * pr_vk;
	int i = 0;
	while (KunLeerG.dbupdate () < 0)
	{
		if (i >= 200) break;
		Sleep (10);
		i ++;
	}

	if (!leer_konto) return;

    LeerBel.leer_bel.mdn = lsk.mdn;
    LeerBel.leer_bel.nr  = lsk.ls;
    LeerBel.leer_bel.kun_fil = lsk.kun_fil;
    LeerBel.leer_bel.kun   = lsk.kun;
    LeerBel.leer_bel.hkun  = GetHKun ();
	LeerBel.leer_bel.a = a;
    strcpy (LeerBel.leer_bel.pers_nam, sys_ben.pers_nam);
	LeerBel.leer_bel.datum = dasc_to_long (datum);
	systime (LeerBel.leer_bel.zeit);
    LeerBel.leer_bel.anzahl = (long) lief_me;
    strcpy (LeerBel.leer_bel.blg_typ, "L");
	i = 0;
	while (LeerBel.dbinsert () < 0)
	{
		if (i >= 200) break;
		Sleep (10);
		i ++;
	}
}


long CLeerGut::GetHKun ()
{
	int dsqlstatus = 0;
    if (Tou != -1)
	{
		tou = lsk.tou_nr;
		KunLeerG.sqlopen (Tou);
		dsqlstatus = KunLeerG.sqlfetch (Tou);
		if ((dsqlstatus == 0) && (eigentou == 0) && (htou != 0l))
		{
			return htou;
		}
	}
	return lsk.kun;
}