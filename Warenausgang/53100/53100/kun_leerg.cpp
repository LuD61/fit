#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "mo_curso.h"
#include "dbclass.h"
#include "kun_leerg.h"

struct KUN_LEERG kun_leerg, kun_leerg_null;

void KUN_LEERG_CLASS::prepare (void)
{
            TCHAR *sqltext;

            ins_quest ((char *)   &kun_leerg.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &kun_leerg.a, SQLDOUBLE, 0);
            ins_quest ((char *)    &kun_leerg.kun, SQLLONG, 0);
            ins_quest ((char *)   &kun_leerg.kun_fil, SQLSHORT, 0);
    out_quest ((char *) &kun_leerg.delstatus,1,0);
    out_quest ((char *) &kun_leerg.mdn,1,0);
    out_quest ((char *) &kun_leerg.fil,1,0);
    out_quest ((char *) &kun_leerg.kun_fil,1,0);
    out_quest ((char *) &kun_leerg.kun,2,0);
    out_quest ((char *) &kun_leerg.a,3,0);
    out_quest ((char *) &kun_leerg.bsd_stk,2,0);
    out_quest ((char *) &kun_leerg.bsd_wrt,3,0);
    out_quest ((char *) &kun_leerg.lief_dat,2,0);
    out_quest ((char *) &kun_leerg.ret_dat,2,0);
    out_quest ((char *) kun_leerg.ben_lief,0,17);
    out_quest ((char *) kun_leerg.ben_ret,0,17);
            cursor = sqlcursor ("select kun_leerg.delstatus,  "
"kun_leerg.mdn,  kun_leerg.fil,  kun_leerg.kun_fil,  kun_leerg.kun,  "
"kun_leerg.a,  kun_leerg.bsd_stk,  kun_leerg.bsd_wrt,  "
"kun_leerg.lief_dat,  kun_leerg.ret_dat,  kun_leerg.ben_lief,  "
"kun_leerg.ben_ret from kun_leerg "

#line 22 "kun_leerg.rpp"
                                  "where mdn = ? "
				  "and a = ? "
 				  "and kun = ? "
				  "and kun_fil = ?");
    ins_quest ((char *) &kun_leerg.delstatus,1,0);
    ins_quest ((char *) &kun_leerg.mdn,1,0);
    ins_quest ((char *) &kun_leerg.fil,1,0);
    ins_quest ((char *) &kun_leerg.kun_fil,1,0);
    ins_quest ((char *) &kun_leerg.kun,2,0);
    ins_quest ((char *) &kun_leerg.a,3,0);
    ins_quest ((char *) &kun_leerg.bsd_stk,2,0);
    ins_quest ((char *) &kun_leerg.bsd_wrt,3,0);
    ins_quest ((char *) &kun_leerg.lief_dat,2,0);
    ins_quest ((char *) &kun_leerg.ret_dat,2,0);
    ins_quest ((char *) kun_leerg.ben_lief,0,17);
    ins_quest ((char *) kun_leerg.ben_ret,0,17);
            sqltext = "update kun_leerg set "
"kun_leerg.delstatus = ?,  kun_leerg.mdn = ?,  kun_leerg.fil = ?,  "
"kun_leerg.kun_fil = ?,  kun_leerg.kun = ?,  kun_leerg.a = ?,  "
"kun_leerg.bsd_stk = ?,  kun_leerg.bsd_wrt = ?,  "
"kun_leerg.lief_dat = ?,  kun_leerg.ret_dat = ?,  "
"kun_leerg.ben_lief = ?,  kun_leerg.ben_ret = ? "

#line 27 "kun_leerg.rpp"
                                  "where mdn = ? "
				  "and a = ? "
 				  "and kun = ? "
				  "and kun_fil = ?";
            ins_quest ((char *)   &kun_leerg.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &kun_leerg.a, SQLDOUBLE, 0);
            ins_quest ((char *)    &kun_leerg.kun, SQLLONG, 0);
            ins_quest ((char *)   &kun_leerg.kun_fil, SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            ins_quest ((char *)   &kun_leerg.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &kun_leerg.a, SQLDOUBLE, 0);
            ins_quest ((char *)    &kun_leerg.kun, SQLLONG, 0);
            ins_quest ((char *)   &kun_leerg.kun_fil, SQLSHORT, 0);
            test_upd_cursor = sqlcursor ("select a from kun_leerg "
                                  "where mdn = ? "
				  "and a = ? "
 				  "and kun = ? "
				  "and kun_fil = ?");
            ins_quest ((char *)   &kun_leerg.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &kun_leerg.a, SQLDOUBLE, 0);
            ins_quest ((char *)    &kun_leerg.kun, SQLLONG, 0);
            ins_quest ((char *)   &kun_leerg.kun_fil, SQLSHORT, 0);
            del_cursor = sqlcursor ("delete from kun_leerg "
                                  "where mdn = ? "
				  "and a = ? "
 				  "and kun = ? "
				  "and kun_fil = ?");
    ins_quest ((char *) &kun_leerg.delstatus,1,0);
    ins_quest ((char *) &kun_leerg.mdn,1,0);
    ins_quest ((char *) &kun_leerg.fil,1,0);
    ins_quest ((char *) &kun_leerg.kun_fil,1,0);
    ins_quest ((char *) &kun_leerg.kun,2,0);
    ins_quest ((char *) &kun_leerg.a,3,0);
    ins_quest ((char *) &kun_leerg.bsd_stk,2,0);
    ins_quest ((char *) &kun_leerg.bsd_wrt,3,0);
    ins_quest ((char *) &kun_leerg.lief_dat,2,0);
    ins_quest ((char *) &kun_leerg.ret_dat,2,0);
    ins_quest ((char *) kun_leerg.ben_lief,0,17);
    ins_quest ((char *) kun_leerg.ben_ret,0,17);
            ins_cursor = sqlcursor ("insert into kun_leerg ("
"delstatus,  mdn,  fil,  kun_fil,  kun,  a,  bsd_stk,  bsd_wrt,  lief_dat,  ret_dat,  "
"ben_lief,  ben_ret) "

#line 56 "kun_leerg.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?)"); 

#line 58 "kun_leerg.rpp"
}

BOOL KUN_LEERG_CLASS::operator== (KUN_LEERG& kun_leerg)
{
            if (this->kun_leerg.mdn        != kun_leerg.mdn) return FALSE;  
            if (this->kun_leerg.kun_fil    != kun_leerg.kun_fil) return FALSE;  
            if (this->kun_leerg.kun        != kun_leerg.kun) return FALSE;  
            if (this->kun_leerg.a          != kun_leerg.a) return FALSE;  
            return TRUE;
} 
