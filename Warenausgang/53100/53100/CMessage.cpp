#include <windows.h>
#include "Text.h"
#include "dlg.h"
#include "CMessage.h"
#include "Sounds.h"


// CMessage::CMessage () : DLG (-1, -1, 40, 5, "Fehler", 105, FALSE)
CMessage::CMessage () : DLG (-1, -1, 40, 5, "", 105, FALSE)
{
    
	controls = NULL;
	form = NULL;
    ButtonText = "  F5 dr�cken  ";
	BreakKey = F5;
	BreakF5 = TRUE;
}

CMessage::~CMessage ()
{
	if (controls == NULL)
	{
		  return;
	}

	for (int i; i < form->GetFieldanz (); i ++)
	{
		delete controls[i];
	}
	delete controls;
	delete form;
}

void CMessage::SetButtonText (Text &ButtonText)
{
	this->ButtonText = ButtonText;
}

void CMessage::SetBreakKey (int BreakKey)
{
	this->BreakKey = BreakKey;
}

void CMessage::SetBreakF5 (BOOL BreakF5)
{
	this->BreakF5 = BreakF5;
}


BOOL CMessage::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
       
     if (LOWORD (wParam) == VK_F5)
	 {
		      syskey = KEY5;
              return OnKey5 ();
	 }

        return DLG::OnCommand (hWnd, msg, wParam, lParam);
}


BOOL CMessage::OnKeyReturn (void)
{
	if (BreakKey == RETURN)
	{
	    PostQuitMessage (0);
	    DestroyWindow ();
  	    return TRUE;
	}

//	MessageBeep (MB_ICONASTERISK);
    if (signal)
	{
	       CSounds sound ("error.wav");
	       sound.Play ();
	}
	else
	{
           MessageBeep (MB_ICONASTERISK);
	}
	return TRUE;
}

BOOL CMessage::OnKey5 (void)
{
	if (BreakKey == F5 || BreakF5)
	{
	    PostQuitMessage (0);
	    DestroyWindow ();
  	    return TRUE;
	}
	return FALSE;
}


void CMessage::Start (LPSTR text, BOOL signal)
{
	      Token t (text, "\n");

		  this->signal =signal;
		  controls = new CFIELD *[t.GetAnzToken () + 2];
          form = new CFORM (0, controls); 
		  form->SetFieldanz (t.GetAnzToken () + 1);
		  int i = 0;
		  LPSTR txt;
		  while ((txt = t.NextToken ()) != NULL)
		  {
                     controls [i] = new CFIELD ("Text",
						                         txt, 
						                         strlen (txt) + 2, 2, 2, i, 
												  NULL, "", CDISPLAYONLY,
										          NULL, Font, FALSE, TRANSPARENT); 		  
					 i ++;
		  }
          controls [i] = new CFIELD ("BuOK",
			                          ButtonText.GetBuffer (),
			                          16, 2, -1, i + 1,
									  NULL, "", CBUTTON,
									  VK_F5, Font, FALSE, TRANSPARENT);
		  controls[i + 1] = NULL;
          form->SetFieldanz ();
          SetDialog (form);

		  HANDLE hInstance = (HANDLE) GetWindowLong (GetActiveWindow (), GWL_HINSTANCE);
		  Pack (2, 0);
		  SetWinBackground (GetSysColor (COLOR_3DFACE));
          OpenWindow (hInstance, NULL);
		  SetWindowPos (hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
		  if (signal)
		  {
			       CSounds sound ("error.wav");
				   sound.Play ();
		  }

		  ProcessMessages ();
}

void CMessage::Show (LPSTR txt)
{
	HWND hWnd = GetActiveWindow ();
	EnableWindow (hWnd, FALSE);
	CMessage *m = new CMessage ();
	m->SetStyle (WS_BORDER | WS_VISIBLE | WS_CAPTION);
	m->Start (txt, TRUE);
	EnableWindow (hWnd, TRUE);
}


void CMessage::Show (LPSTR txt, Text& ButtonText, int BreakKey, BOOL signal)
{
	HWND hWnd = GetActiveWindow ();
	EnableWindow (hWnd, FALSE);
	CMessage *m = new CMessage ();
	m->SetButtonText (ButtonText);
	m->SetBreakKey (BreakKey);
	m->SetStyle (WS_BORDER | WS_VISIBLE | WS_CAPTION);
	m->Start (txt, signal);
	EnableWindow (hWnd, TRUE);
}