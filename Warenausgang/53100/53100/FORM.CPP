// Form.cpp : implementation file
//

#include "stdafx.h"
#include "test3.h"
#include "Form.h"
#include "tabsheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CForm

IMPLEMENT_DYNCREATE(CForm, CFormView)

CForm::CForm()
	: CFormView(CForm::IDD)
{
	//{{AFX_DATA_INIT(CForm)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_CAllControlsSheet = NULL;
}

CForm::~CForm()
{
	if (m_CAllControlsSheet)
		delete m_CAllControlsSheet;
}

void CForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CForm, CFormView)
	//{{AFX_MSG_MAP(CForm)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CForm diagnostics

#ifdef _DEBUG
void CForm::AssertValid() const
{
	CFormView::AssertValid();
}

void CForm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CForm message handlers

void CForm::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// create and asociated the property sheet with the "place holder" window
	CWnd* pwndPropSheetHolder = GetDlgItem(IDC_FORM);
	m_CAllControlsSheet = new CAllControlsSheet("Test",pwndPropSheetHolder);
	if (!m_CAllControlsSheet->Create(pwndPropSheetHolder, 
		WS_CHILD | WS_VISIBLE, 0))
	{
		delete m_CAllControlsSheet;
		m_CAllControlsSheet = NULL;
		return;
	}

	// fit the property sheet into the place holder window, and show it
	CRect rectPropSheet;
	pwndPropSheetHolder->GetWindowRect(rectPropSheet);
	m_CAllControlsSheet->SetWindowPos(NULL, 0, 0,
		rectPropSheet.Width(), rectPropSheet.Height(),
		SWP_NOZORDER | SWP_NOACTIVATE);
	CRect r,r2;
	GetWindowRect(&r); 
	GetParent()->GetWindowRect(&r2);
	ScreenToClient(&r2);
	r2.top += 25;
	r2.right = r2.left + r.Width();
	r2.bottom = r2.top + (r.Height()*10)/7;
	GetParent()->MoveWindow(&r2,TRUE);
	
	// TODO: Add your specialized code here and/or call the base class
	
}
