// LeerGut.h: Schnittstelle f�r die Klasse CLeerGut.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEERGUT_H__A9485C53_160C_435B_A0EE_FCA6864D3991__INCLUDED_)
#define AFX_LEERGUT_H__A9485C53_160C_435B_A0EE_FCA6864D3991__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "kun_leerg.h"
#include "leer_lsdr.h"
#include "leer_bel.h"
#include "sys_par.h"
#include "ls.h"
#include "a_bas.h"


#define LEERMAX 100

	struct LP
	{
		double lart;
		int    lanz;
	} ;
	extern struct LP LeerPosArt [LEERMAX];


class CLeerGut  
{
protected:
	BOOL init;
	long tou;
	long htou;
	short eigentou;  
	int Tou;
public:
	enum
	{
		LeerGut = 11,
	};
	KUN_LEERG_CLASS KunLeerG;
	LEER_LSDR_CLASS LeerLsdr;
	LEER_BEL_CLASS LeerBel;
	SYS_PAR_CLASS Syspar;
	int kun_leer;
	BOOL leer_konto;
	CLeerGut();
	virtual ~CLeerGut();
	void Init ();
	void Update (double a, double lief_me, double pr_vk);
	long GetHKun ();


};

#endif // !defined(AFX_LEERGUT_H__A9485C53_160C_435B_A0EE_FCA6864D3991__INCLUDED_)
