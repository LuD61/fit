// WaWiePro.h: Schnittstelle f�r die Klasse CWaWiePro.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WAWIEPRO_H__5CB7F74B_7D98_4AF9_8696_093B922CBAD4__INCLUDED_)
#define AFX_WAWIEPRO_H__5CB7F74B_7D98_4AF9_8696_093B922CBAD4__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "wiepro.h"

class CWaWiePro  
{
private:
	WIEPRO_CLASS WiePro;
public:
	double a;
	double auf_lad_pr;
	char *erf_kz;
	char *ls_charge;
	char *ls_ident;

	CWaWiePro();
	virtual ~CWaWiePro();
	void Write (double brutto, double netto, double tara);
	void Write (double brutto, double netto, double tara, char *erf_kz);

};

#endif // !defined(AFX_WAWIEPRO_H__5CB7F74B_7D98_4AF9_8696_093B922CBAD4__INCLUDED_)
