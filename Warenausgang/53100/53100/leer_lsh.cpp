#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "mo_curso.h"
#include "dbclass.h"
#include "leer_lsh.h"

struct LEER_LSH leer_lsh, leer_lsh_null;

void LEER_LSH_CLASS::prepare (void)
{
            TCHAR *sqltext;

            ins_quest ((char *)  &leer_lsh.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsh.fil, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsh.ls, SQLLONG, 0);
            ins_quest ((char *)   leer_lsh.blg_typ, SQLCHAR, 2);
    out_quest ((char *) &leer_lsh.mdn,1,0);
    out_quest ((char *) &leer_lsh.fil,1,0);
    out_quest ((char *) &leer_lsh.ls,2,0);
    out_quest ((char *) leer_lsh.blg_typ,0,2);
    out_quest ((char *) &leer_lsh.a1,3,0);
    out_quest ((char *) leer_lsh.a_bez1,0,25);
    out_quest ((char *) &leer_lsh.stk_zu1,3,0);
    out_quest ((char *) &leer_lsh.stk_ab1,3,0);
    out_quest ((char *) &leer_lsh.stk1,3,0);
    out_quest ((char *) &leer_lsh.pr_vk1,3,0);
    out_quest ((char *) &leer_lsh.a2,3,0);
    out_quest ((char *) leer_lsh.a_bez2,0,25);
    out_quest ((char *) &leer_lsh.stk_zu2,3,0);
    out_quest ((char *) &leer_lsh.stk_ab2,3,0);
    out_quest ((char *) &leer_lsh.stk2,3,0);
    out_quest ((char *) &leer_lsh.pr_vk2,3,0);
    out_quest ((char *) &leer_lsh.a3,3,0);
    out_quest ((char *) leer_lsh.a_bez3,0,25);
    out_quest ((char *) &leer_lsh.stk_zu3,3,0);
    out_quest ((char *) &leer_lsh.stk_ab3,3,0);
    out_quest ((char *) &leer_lsh.stk3,3,0);
    out_quest ((char *) &leer_lsh.pr_vk3,3,0);
    out_quest ((char *) &leer_lsh.a4,3,0);
    out_quest ((char *) leer_lsh.a_bez4,0,25);
    out_quest ((char *) &leer_lsh.stk_zu4,3,0);
    out_quest ((char *) &leer_lsh.stk_ab4,3,0);
    out_quest ((char *) &leer_lsh.stk4,3,0);
    out_quest ((char *) &leer_lsh.pr_vk4,3,0);
    out_quest ((char *) &leer_lsh.a5,3,0);
    out_quest ((char *) leer_lsh.a_bez5,0,25);
    out_quest ((char *) &leer_lsh.stk_zu5,3,0);
    out_quest ((char *) &leer_lsh.stk_ab5,3,0);
    out_quest ((char *) &leer_lsh.stk5,3,0);
    out_quest ((char *) &leer_lsh.pr_vk5,3,0);
    out_quest ((char *) &leer_lsh.a6,3,0);
    out_quest ((char *) leer_lsh.a_bez6,0,25);
    out_quest ((char *) &leer_lsh.stk_zu6,3,0);
    out_quest ((char *) &leer_lsh.stk_ab6,3,0);
    out_quest ((char *) &leer_lsh.stk6,3,0);
    out_quest ((char *) &leer_lsh.pr_vk6,3,0);
    out_quest ((char *) &leer_lsh.a7,3,0);
    out_quest ((char *) leer_lsh.a_bez7,0,25);
    out_quest ((char *) &leer_lsh.stk_zu7,3,0);
    out_quest ((char *) &leer_lsh.stk_ab7,3,0);
    out_quest ((char *) &leer_lsh.stk7,3,0);
    out_quest ((char *) &leer_lsh.pr_vk7,3,0);
    out_quest ((char *) &leer_lsh.a8,3,0);
    out_quest ((char *) leer_lsh.a_bez8,0,25);
    out_quest ((char *) &leer_lsh.stk_zu8,3,0);
    out_quest ((char *) &leer_lsh.stk_ab8,3,0);
    out_quest ((char *) &leer_lsh.stk8,3,0);
    out_quest ((char *) &leer_lsh.pr_vk8,3,0);
    out_quest ((char *) &leer_lsh.party_mdn,1,0);
    out_quest ((char *) &leer_lsh.a9,3,0);
    out_quest ((char *) leer_lsh.a_bez9,0,5);
    out_quest ((char *) &leer_lsh.stk_zu9,3,0);
    out_quest ((char *) &leer_lsh.stk_ab9,3,0);
    out_quest ((char *) &leer_lsh.stk9,3,0);
    out_quest ((char *) &leer_lsh.pr_vk9,3,0);
    out_quest ((char *) &leer_lsh.a10,3,0);
    out_quest ((char *) leer_lsh.a_bez10,0,5);
    out_quest ((char *) &leer_lsh.stk_zu10,3,0);
    out_quest ((char *) &leer_lsh.stk_ab10,3,0);
    out_quest ((char *) &leer_lsh.stk10,3,0);
    out_quest ((char *) &leer_lsh.pr_vk10,3,0);
    out_quest ((char *) &leer_lsh.a11,3,0);
    out_quest ((char *) leer_lsh.a_bez11,0,5);
    out_quest ((char *) &leer_lsh.stk_zu11,3,0);
    out_quest ((char *) &leer_lsh.stk_ab11,3,0);
    out_quest ((char *) &leer_lsh.stk11,3,0);
    out_quest ((char *) &leer_lsh.pr_vk11,3,0);
    out_quest ((char *) &leer_lsh.a12,3,0);
    out_quest ((char *) leer_lsh.a_bez12,0,5);
    out_quest ((char *) &leer_lsh.stk_zu12,3,0);
    out_quest ((char *) &leer_lsh.stk_ab12,3,0);
    out_quest ((char *) &leer_lsh.stk12,3,0);
    out_quest ((char *) &leer_lsh.pr_vk12,3,0);
    out_quest ((char *) &leer_lsh.a13,3,0);
    out_quest ((char *) leer_lsh.a_bez13,0,5);
    out_quest ((char *) &leer_lsh.stk_zu13,3,0);
    out_quest ((char *) &leer_lsh.stk_ab13,3,0);
    out_quest ((char *) &leer_lsh.stk13,3,0);
    out_quest ((char *) &leer_lsh.pr_vk13,3,0);
    out_quest ((char *) &leer_lsh.a14,3,0);
    out_quest ((char *) leer_lsh.a_bez14,0,5);
    out_quest ((char *) &leer_lsh.stk_zu14,3,0);
    out_quest ((char *) &leer_lsh.stk_ab14,3,0);
    out_quest ((char *) &leer_lsh.stk14,3,0);
    out_quest ((char *) &leer_lsh.pr_vk14,3,0);
    out_quest ((char *) &leer_lsh.a15,3,0);
    out_quest ((char *) leer_lsh.a_bez15,0,5);
    out_quest ((char *) &leer_lsh.stk_zu15,3,0);
    out_quest ((char *) &leer_lsh.stk_ab15,3,0);
    out_quest ((char *) &leer_lsh.stk15,3,0);
    out_quest ((char *) &leer_lsh.pr_vk15,3,0);
    out_quest ((char *) &leer_lsh.a16,3,0);
    out_quest ((char *) leer_lsh.a_bez16,0,5);
    out_quest ((char *) &leer_lsh.stk_zu16,3,0);
    out_quest ((char *) &leer_lsh.stk_ab16,3,0);
    out_quest ((char *) &leer_lsh.stk16,3,0);
    out_quest ((char *) &leer_lsh.pr_vk16,3,0);
    out_quest ((char *) &leer_lsh.a17,3,0);
    out_quest ((char *) leer_lsh.a_bez17,0,5);
    out_quest ((char *) &leer_lsh.stk_zu17,3,0);
    out_quest ((char *) &leer_lsh.stk_ab17,3,0);
    out_quest ((char *) &leer_lsh.stk17,3,0);
    out_quest ((char *) &leer_lsh.pr_vk17,3,0);
    out_quest ((char *) &leer_lsh.a18,3,0);
    out_quest ((char *) leer_lsh.a_bez18,0,5);
    out_quest ((char *) &leer_lsh.stk_zu18,3,0);
    out_quest ((char *) &leer_lsh.stk_ab18,3,0);
    out_quest ((char *) &leer_lsh.stk18,3,0);
    out_quest ((char *) &leer_lsh.pr_vk18,3,0);
    out_quest ((char *) &leer_lsh.a19,3,0);
    out_quest ((char *) leer_lsh.a_bez19,0,5);
    out_quest ((char *) &leer_lsh.stk_zu19,3,0);
    out_quest ((char *) &leer_lsh.stk_ab19,3,0);
    out_quest ((char *) &leer_lsh.stk19,3,0);
    out_quest ((char *) &leer_lsh.pr_vk19,3,0);
    out_quest ((char *) &leer_lsh.a20,3,0);
    out_quest ((char *) leer_lsh.a_bez20,0,5);
    out_quest ((char *) &leer_lsh.stk_zu20,3,0);
    out_quest ((char *) &leer_lsh.stk_ab20,3,0);
    out_quest ((char *) &leer_lsh.stk20,3,0);
    out_quest ((char *) &leer_lsh.pr_vk20,3,0);
    out_quest ((char *) &leer_lsh.a_gew1,3,0);
    out_quest ((char *) &leer_lsh.a_gew2,3,0);
    out_quest ((char *) &leer_lsh.a_gew3,3,0);
    out_quest ((char *) &leer_lsh.a_gew4,3,0);
    out_quest ((char *) &leer_lsh.a_gew5,3,0);
    out_quest ((char *) &leer_lsh.a_gew6,3,0);
    out_quest ((char *) &leer_lsh.a_gew7,3,0);
    out_quest ((char *) &leer_lsh.a_gew8,3,0);
    out_quest ((char *) &leer_lsh.a_gew9,3,0);
    out_quest ((char *) &leer_lsh.a_gew10,3,0);
    out_quest ((char *) &leer_lsh.a_gew11,3,0);
    out_quest ((char *) &leer_lsh.a_gew12,3,0);
    out_quest ((char *) &leer_lsh.a_gew13,3,0);
    out_quest ((char *) &leer_lsh.a_gew14,3,0);
    out_quest ((char *) &leer_lsh.a_gew15,3,0);
    out_quest ((char *) &leer_lsh.a_gew16,3,0);
    out_quest ((char *) &leer_lsh.a_gew17,3,0);
    out_quest ((char *) &leer_lsh.a_gew18,3,0);
    out_quest ((char *) &leer_lsh.a_gew19,3,0);
    out_quest ((char *) &leer_lsh.a_gew20,3,0);
            cursor = sqlcursor ("select leer_lsh.mdn,  "
"leer_lsh.fil,  leer_lsh.ls,  leer_lsh.blg_typ,  leer_lsh.a1,  "
"leer_lsh.a_bez1,  leer_lsh.stk_zu1,  leer_lsh.stk_ab1,  leer_lsh.stk1,  "
"leer_lsh.pr_vk1,  leer_lsh.a2,  leer_lsh.a_bez2,  leer_lsh.stk_zu2,  "
"leer_lsh.stk_ab2,  leer_lsh.stk2,  leer_lsh.pr_vk2,  leer_lsh.a3,  "
"leer_lsh.a_bez3,  leer_lsh.stk_zu3,  leer_lsh.stk_ab3,  leer_lsh.stk3,  "
"leer_lsh.pr_vk3,  leer_lsh.a4,  leer_lsh.a_bez4,  leer_lsh.stk_zu4,  "
"leer_lsh.stk_ab4,  leer_lsh.stk4,  leer_lsh.pr_vk4,  leer_lsh.a5,  "
"leer_lsh.a_bez5,  leer_lsh.stk_zu5,  leer_lsh.stk_ab5,  leer_lsh.stk5,  "
"leer_lsh.pr_vk5,  leer_lsh.a6,  leer_lsh.a_bez6,  leer_lsh.stk_zu6,  "
"leer_lsh.stk_ab6,  leer_lsh.stk6,  leer_lsh.pr_vk6,  leer_lsh.a7,  "
"leer_lsh.a_bez7,  leer_lsh.stk_zu7,  leer_lsh.stk_ab7,  leer_lsh.stk7,  "
"leer_lsh.pr_vk7,  leer_lsh.a8,  leer_lsh.a_bez8,  leer_lsh.stk_zu8,  "
"leer_lsh.stk_ab8,  leer_lsh.stk8,  leer_lsh.pr_vk8,  "
"leer_lsh.party_mdn,  leer_lsh.a9,  leer_lsh.a_bez9,  leer_lsh.stk_zu9,  "
"leer_lsh.stk_ab9,  leer_lsh.stk9,  leer_lsh.pr_vk9,  leer_lsh.a10,  "
"leer_lsh.a_bez10,  leer_lsh.stk_zu10,  leer_lsh.stk_ab10,  "
"leer_lsh.stk10,  leer_lsh.pr_vk10,  leer_lsh.a11,  leer_lsh.a_bez11,  "
"leer_lsh.stk_zu11,  leer_lsh.stk_ab11,  leer_lsh.stk11,  "
"leer_lsh.pr_vk11,  leer_lsh.a12,  leer_lsh.a_bez12,  "
"leer_lsh.stk_zu12,  leer_lsh.stk_ab12,  leer_lsh.stk12,  "
"leer_lsh.pr_vk12,  leer_lsh.a13,  leer_lsh.a_bez13,  "
"leer_lsh.stk_zu13,  leer_lsh.stk_ab13,  leer_lsh.stk13,  "
"leer_lsh.pr_vk13,  leer_lsh.a14,  leer_lsh.a_bez14,  "
"leer_lsh.stk_zu14,  leer_lsh.stk_ab14,  leer_lsh.stk14,  "
"leer_lsh.pr_vk14,  leer_lsh.a15,  leer_lsh.a_bez15,  "
"leer_lsh.stk_zu15,  leer_lsh.stk_ab15,  leer_lsh.stk15,  "
"leer_lsh.pr_vk15,  leer_lsh.a16,  leer_lsh.a_bez16,  "
"leer_lsh.stk_zu16,  leer_lsh.stk_ab16,  leer_lsh.stk16,  "
"leer_lsh.pr_vk16,  leer_lsh.a17,  leer_lsh.a_bez17,  "
"leer_lsh.stk_zu17,  leer_lsh.stk_ab17,  leer_lsh.stk17,  "
"leer_lsh.pr_vk17,  leer_lsh.a18,  leer_lsh.a_bez18,  "
"leer_lsh.stk_zu18,  leer_lsh.stk_ab18,  leer_lsh.stk18,  "
"leer_lsh.pr_vk18,  leer_lsh.a19,  leer_lsh.a_bez19,  "
"leer_lsh.stk_zu19,  leer_lsh.stk_ab19,  leer_lsh.stk19,  "
"leer_lsh.pr_vk19,  leer_lsh.a20,  leer_lsh.a_bez20,  "
"leer_lsh.stk_zu20,  leer_lsh.stk_ab20,  leer_lsh.stk20,  "
"leer_lsh.pr_vk20,  leer_lsh.a_gew1,  leer_lsh.a_gew2,  "
"leer_lsh.a_gew3,  leer_lsh.a_gew4,  leer_lsh.a_gew5,  leer_lsh.a_gew6,  "
"leer_lsh.a_gew7,  leer_lsh.a_gew8,  leer_lsh.a_gew9,  "
"leer_lsh.a_gew10,  leer_lsh.a_gew11,  leer_lsh.a_gew12,  "
"leer_lsh.a_gew13,  leer_lsh.a_gew14,  leer_lsh.a_gew15,  "
"leer_lsh.a_gew16,  leer_lsh.a_gew17,  leer_lsh.a_gew18,  "
"leer_lsh.a_gew19,  leer_lsh.a_gew20 from leer_lsh "

#line 22 "leer_lsh.rpp"
                                  "where mdn = ? "
				  "and fil = ? "
 				  "and ls = ? "
				  "and blg_typ = ?");
    ins_quest ((char *) &leer_lsh.mdn,1,0);
    ins_quest ((char *) &leer_lsh.fil,1,0);
    ins_quest ((char *) &leer_lsh.ls,2,0);
    ins_quest ((char *) leer_lsh.blg_typ,0,2);
    ins_quest ((char *) &leer_lsh.a1,3,0);
    ins_quest ((char *) leer_lsh.a_bez1,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu1,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab1,3,0);
    ins_quest ((char *) &leer_lsh.stk1,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk1,3,0);
    ins_quest ((char *) &leer_lsh.a2,3,0);
    ins_quest ((char *) leer_lsh.a_bez2,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu2,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab2,3,0);
    ins_quest ((char *) &leer_lsh.stk2,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk2,3,0);
    ins_quest ((char *) &leer_lsh.a3,3,0);
    ins_quest ((char *) leer_lsh.a_bez3,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu3,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab3,3,0);
    ins_quest ((char *) &leer_lsh.stk3,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk3,3,0);
    ins_quest ((char *) &leer_lsh.a4,3,0);
    ins_quest ((char *) leer_lsh.a_bez4,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu4,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab4,3,0);
    ins_quest ((char *) &leer_lsh.stk4,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk4,3,0);
    ins_quest ((char *) &leer_lsh.a5,3,0);
    ins_quest ((char *) leer_lsh.a_bez5,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu5,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab5,3,0);
    ins_quest ((char *) &leer_lsh.stk5,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk5,3,0);
    ins_quest ((char *) &leer_lsh.a6,3,0);
    ins_quest ((char *) leer_lsh.a_bez6,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu6,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab6,3,0);
    ins_quest ((char *) &leer_lsh.stk6,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk6,3,0);
    ins_quest ((char *) &leer_lsh.a7,3,0);
    ins_quest ((char *) leer_lsh.a_bez7,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu7,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab7,3,0);
    ins_quest ((char *) &leer_lsh.stk7,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk7,3,0);
    ins_quest ((char *) &leer_lsh.a8,3,0);
    ins_quest ((char *) leer_lsh.a_bez8,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu8,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab8,3,0);
    ins_quest ((char *) &leer_lsh.stk8,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk8,3,0);
    ins_quest ((char *) &leer_lsh.party_mdn,1,0);
    ins_quest ((char *) &leer_lsh.a9,3,0);
    ins_quest ((char *) leer_lsh.a_bez9,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu9,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab9,3,0);
    ins_quest ((char *) &leer_lsh.stk9,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk9,3,0);
    ins_quest ((char *) &leer_lsh.a10,3,0);
    ins_quest ((char *) leer_lsh.a_bez10,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu10,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab10,3,0);
    ins_quest ((char *) &leer_lsh.stk10,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk10,3,0);
    ins_quest ((char *) &leer_lsh.a11,3,0);
    ins_quest ((char *) leer_lsh.a_bez11,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu11,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab11,3,0);
    ins_quest ((char *) &leer_lsh.stk11,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk11,3,0);
    ins_quest ((char *) &leer_lsh.a12,3,0);
    ins_quest ((char *) leer_lsh.a_bez12,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu12,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab12,3,0);
    ins_quest ((char *) &leer_lsh.stk12,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk12,3,0);
    ins_quest ((char *) &leer_lsh.a13,3,0);
    ins_quest ((char *) leer_lsh.a_bez13,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu13,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab13,3,0);
    ins_quest ((char *) &leer_lsh.stk13,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk13,3,0);
    ins_quest ((char *) &leer_lsh.a14,3,0);
    ins_quest ((char *) leer_lsh.a_bez14,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu14,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab14,3,0);
    ins_quest ((char *) &leer_lsh.stk14,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk14,3,0);
    ins_quest ((char *) &leer_lsh.a15,3,0);
    ins_quest ((char *) leer_lsh.a_bez15,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu15,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab15,3,0);
    ins_quest ((char *) &leer_lsh.stk15,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk15,3,0);
    ins_quest ((char *) &leer_lsh.a16,3,0);
    ins_quest ((char *) leer_lsh.a_bez16,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu16,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab16,3,0);
    ins_quest ((char *) &leer_lsh.stk16,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk16,3,0);
    ins_quest ((char *) &leer_lsh.a17,3,0);
    ins_quest ((char *) leer_lsh.a_bez17,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu17,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab17,3,0);
    ins_quest ((char *) &leer_lsh.stk17,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk17,3,0);
    ins_quest ((char *) &leer_lsh.a18,3,0);
    ins_quest ((char *) leer_lsh.a_bez18,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu18,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab18,3,0);
    ins_quest ((char *) &leer_lsh.stk18,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk18,3,0);
    ins_quest ((char *) &leer_lsh.a19,3,0);
    ins_quest ((char *) leer_lsh.a_bez19,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu19,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab19,3,0);
    ins_quest ((char *) &leer_lsh.stk19,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk19,3,0);
    ins_quest ((char *) &leer_lsh.a20,3,0);
    ins_quest ((char *) leer_lsh.a_bez20,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu20,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab20,3,0);
    ins_quest ((char *) &leer_lsh.stk20,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk20,3,0);
    ins_quest ((char *) &leer_lsh.a_gew1,3,0);
    ins_quest ((char *) &leer_lsh.a_gew2,3,0);
    ins_quest ((char *) &leer_lsh.a_gew3,3,0);
    ins_quest ((char *) &leer_lsh.a_gew4,3,0);
    ins_quest ((char *) &leer_lsh.a_gew5,3,0);
    ins_quest ((char *) &leer_lsh.a_gew6,3,0);
    ins_quest ((char *) &leer_lsh.a_gew7,3,0);
    ins_quest ((char *) &leer_lsh.a_gew8,3,0);
    ins_quest ((char *) &leer_lsh.a_gew9,3,0);
    ins_quest ((char *) &leer_lsh.a_gew10,3,0);
    ins_quest ((char *) &leer_lsh.a_gew11,3,0);
    ins_quest ((char *) &leer_lsh.a_gew12,3,0);
    ins_quest ((char *) &leer_lsh.a_gew13,3,0);
    ins_quest ((char *) &leer_lsh.a_gew14,3,0);
    ins_quest ((char *) &leer_lsh.a_gew15,3,0);
    ins_quest ((char *) &leer_lsh.a_gew16,3,0);
    ins_quest ((char *) &leer_lsh.a_gew17,3,0);
    ins_quest ((char *) &leer_lsh.a_gew18,3,0);
    ins_quest ((char *) &leer_lsh.a_gew19,3,0);
    ins_quest ((char *) &leer_lsh.a_gew20,3,0);
            sqltext = "update leer_lsh set leer_lsh.mdn = ?,  "
"leer_lsh.fil = ?,  leer_lsh.ls = ?,  leer_lsh.blg_typ = ?,  "
"leer_lsh.a1 = ?,  leer_lsh.a_bez1 = ?,  leer_lsh.stk_zu1 = ?,  "
"leer_lsh.stk_ab1 = ?,  leer_lsh.stk1 = ?,  leer_lsh.pr_vk1 = ?,  "
"leer_lsh.a2 = ?,  leer_lsh.a_bez2 = ?,  leer_lsh.stk_zu2 = ?,  "
"leer_lsh.stk_ab2 = ?,  leer_lsh.stk2 = ?,  leer_lsh.pr_vk2 = ?,  "
"leer_lsh.a3 = ?,  leer_lsh.a_bez3 = ?,  leer_lsh.stk_zu3 = ?,  "
"leer_lsh.stk_ab3 = ?,  leer_lsh.stk3 = ?,  leer_lsh.pr_vk3 = ?,  "
"leer_lsh.a4 = ?,  leer_lsh.a_bez4 = ?,  leer_lsh.stk_zu4 = ?,  "
"leer_lsh.stk_ab4 = ?,  leer_lsh.stk4 = ?,  leer_lsh.pr_vk4 = ?,  "
"leer_lsh.a5 = ?,  leer_lsh.a_bez5 = ?,  leer_lsh.stk_zu5 = ?,  "
"leer_lsh.stk_ab5 = ?,  leer_lsh.stk5 = ?,  leer_lsh.pr_vk5 = ?,  "
"leer_lsh.a6 = ?,  leer_lsh.a_bez6 = ?,  leer_lsh.stk_zu6 = ?,  "
"leer_lsh.stk_ab6 = ?,  leer_lsh.stk6 = ?,  leer_lsh.pr_vk6 = ?,  "
"leer_lsh.a7 = ?,  leer_lsh.a_bez7 = ?,  leer_lsh.stk_zu7 = ?,  "
"leer_lsh.stk_ab7 = ?,  leer_lsh.stk7 = ?,  leer_lsh.pr_vk7 = ?,  "
"leer_lsh.a8 = ?,  leer_lsh.a_bez8 = ?,  leer_lsh.stk_zu8 = ?,  "
"leer_lsh.stk_ab8 = ?,  leer_lsh.stk8 = ?,  leer_lsh.pr_vk8 = ?,  "
"leer_lsh.party_mdn = ?,  leer_lsh.a9 = ?,  leer_lsh.a_bez9 = ?,  "
"leer_lsh.stk_zu9 = ?,  leer_lsh.stk_ab9 = ?,  leer_lsh.stk9 = ?,  "
"leer_lsh.pr_vk9 = ?,  leer_lsh.a10 = ?,  leer_lsh.a_bez10 = ?,  "
"leer_lsh.stk_zu10 = ?,  leer_lsh.stk_ab10 = ?,  leer_lsh.stk10 = ?,  "
"leer_lsh.pr_vk10 = ?,  leer_lsh.a11 = ?,  leer_lsh.a_bez11 = ?,  "
"leer_lsh.stk_zu11 = ?,  leer_lsh.stk_ab11 = ?,  leer_lsh.stk11 = ?,  "
"leer_lsh.pr_vk11 = ?,  leer_lsh.a12 = ?,  leer_lsh.a_bez12 = ?,  "
"leer_lsh.stk_zu12 = ?,  leer_lsh.stk_ab12 = ?,  leer_lsh.stk12 = ?,  "
"leer_lsh.pr_vk12 = ?,  leer_lsh.a13 = ?,  leer_lsh.a_bez13 = ?,  "
"leer_lsh.stk_zu13 = ?,  leer_lsh.stk_ab13 = ?,  leer_lsh.stk13 = ?,  "
"leer_lsh.pr_vk13 = ?,  leer_lsh.a14 = ?,  leer_lsh.a_bez14 = ?,  "
"leer_lsh.stk_zu14 = ?,  leer_lsh.stk_ab14 = ?,  leer_lsh.stk14 = ?,  "
"leer_lsh.pr_vk14 = ?,  leer_lsh.a15 = ?,  leer_lsh.a_bez15 = ?,  "
"leer_lsh.stk_zu15 = ?,  leer_lsh.stk_ab15 = ?,  leer_lsh.stk15 = ?,  "
"leer_lsh.pr_vk15 = ?,  leer_lsh.a16 = ?,  leer_lsh.a_bez16 = ?,  "
"leer_lsh.stk_zu16 = ?,  leer_lsh.stk_ab16 = ?,  leer_lsh.stk16 = ?,  "
"leer_lsh.pr_vk16 = ?,  leer_lsh.a17 = ?,  leer_lsh.a_bez17 = ?,  "
"leer_lsh.stk_zu17 = ?,  leer_lsh.stk_ab17 = ?,  leer_lsh.stk17 = ?,  "
"leer_lsh.pr_vk17 = ?,  leer_lsh.a18 = ?,  leer_lsh.a_bez18 = ?,  "
"leer_lsh.stk_zu18 = ?,  leer_lsh.stk_ab18 = ?,  leer_lsh.stk18 = ?,  "
"leer_lsh.pr_vk18 = ?,  leer_lsh.a19 = ?,  leer_lsh.a_bez19 = ?,  "
"leer_lsh.stk_zu19 = ?,  leer_lsh.stk_ab19 = ?,  leer_lsh.stk19 = ?,  "
"leer_lsh.pr_vk19 = ?,  leer_lsh.a20 = ?,  leer_lsh.a_bez20 = ?,  "
"leer_lsh.stk_zu20 = ?,  leer_lsh.stk_ab20 = ?,  leer_lsh.stk20 = ?,  "
"leer_lsh.pr_vk20 = ?,  leer_lsh.a_gew1 = ?,  leer_lsh.a_gew2 = ?,  "
"leer_lsh.a_gew3 = ?,  leer_lsh.a_gew4 = ?,  leer_lsh.a_gew5 = ?,  "
"leer_lsh.a_gew6 = ?,  leer_lsh.a_gew7 = ?,  leer_lsh.a_gew8 = ?,  "
"leer_lsh.a_gew9 = ?,  leer_lsh.a_gew10 = ?,  leer_lsh.a_gew11 = ?,  "
"leer_lsh.a_gew12 = ?,  leer_lsh.a_gew13 = ?,  leer_lsh.a_gew14 = ?,  "
"leer_lsh.a_gew15 = ?,  leer_lsh.a_gew16 = ?,  leer_lsh.a_gew17 = ?,  "
"leer_lsh.a_gew18 = ?,  leer_lsh.a_gew19 = ?,  leer_lsh.a_gew20 = ? "

#line 27 "leer_lsh.rpp"
                                  "where mdn = ? "
				  "and fil = ? "
 				  "and ls = ? "
				  "and blg_typ = ?";
            ins_quest ((char *)  &leer_lsh.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsh.fil, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsh.ls, SQLLONG, 0);
            ins_quest ((char *)   leer_lsh.blg_typ, SQLCHAR, 2);
            upd_cursor = sqlcursor (sqltext);

            ins_quest ((char *)  &leer_lsh.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsh.fil, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsh.ls, SQLLONG, 0);
            ins_quest ((char *)   leer_lsh.blg_typ, SQLCHAR, 2);
            ins_quest ((char *)  &leer_lsh.a_gew20, SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor ("select a1 from leer_lsh "
                                  "where mdn = ? "
				  "and fil = ? "
 				  "and ls = ? "
				  "and blg_typ = ? and a_gew20 = ? ");
            ins_quest ((char *)  &leer_lsh.mdn, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsh.fil, SQLSHORT, 0);
            ins_quest ((char *)  &leer_lsh.ls, SQLLONG, 0);
            ins_quest ((char *)   leer_lsh.blg_typ, SQLCHAR, 2);
            del_cursor = sqlcursor ("delete from leer_lsh "
                                  "where mdn = ? "
				  "and fil = ? "
 				  "and ls = ? "
				  "and blg_typ = ?");
    ins_quest ((char *) &leer_lsh.mdn,1,0);
    ins_quest ((char *) &leer_lsh.fil,1,0);
    ins_quest ((char *) &leer_lsh.ls,2,0);
    ins_quest ((char *) leer_lsh.blg_typ,0,2);
    ins_quest ((char *) &leer_lsh.a1,3,0);
    ins_quest ((char *) leer_lsh.a_bez1,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu1,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab1,3,0);
    ins_quest ((char *) &leer_lsh.stk1,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk1,3,0);
    ins_quest ((char *) &leer_lsh.a2,3,0);
    ins_quest ((char *) leer_lsh.a_bez2,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu2,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab2,3,0);
    ins_quest ((char *) &leer_lsh.stk2,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk2,3,0);
    ins_quest ((char *) &leer_lsh.a3,3,0);
    ins_quest ((char *) leer_lsh.a_bez3,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu3,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab3,3,0);
    ins_quest ((char *) &leer_lsh.stk3,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk3,3,0);
    ins_quest ((char *) &leer_lsh.a4,3,0);
    ins_quest ((char *) leer_lsh.a_bez4,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu4,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab4,3,0);
    ins_quest ((char *) &leer_lsh.stk4,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk4,3,0);
    ins_quest ((char *) &leer_lsh.a5,3,0);
    ins_quest ((char *) leer_lsh.a_bez5,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu5,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab5,3,0);
    ins_quest ((char *) &leer_lsh.stk5,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk5,3,0);
    ins_quest ((char *) &leer_lsh.a6,3,0);
    ins_quest ((char *) leer_lsh.a_bez6,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu6,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab6,3,0);
    ins_quest ((char *) &leer_lsh.stk6,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk6,3,0);
    ins_quest ((char *) &leer_lsh.a7,3,0);
    ins_quest ((char *) leer_lsh.a_bez7,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu7,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab7,3,0);
    ins_quest ((char *) &leer_lsh.stk7,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk7,3,0);
    ins_quest ((char *) &leer_lsh.a8,3,0);
    ins_quest ((char *) leer_lsh.a_bez8,0,25);
    ins_quest ((char *) &leer_lsh.stk_zu8,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab8,3,0);
    ins_quest ((char *) &leer_lsh.stk8,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk8,3,0);
    ins_quest ((char *) &leer_lsh.party_mdn,1,0);
    ins_quest ((char *) &leer_lsh.a9,3,0);
    ins_quest ((char *) leer_lsh.a_bez9,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu9,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab9,3,0);
    ins_quest ((char *) &leer_lsh.stk9,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk9,3,0);
    ins_quest ((char *) &leer_lsh.a10,3,0);
    ins_quest ((char *) leer_lsh.a_bez10,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu10,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab10,3,0);
    ins_quest ((char *) &leer_lsh.stk10,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk10,3,0);
    ins_quest ((char *) &leer_lsh.a11,3,0);
    ins_quest ((char *) leer_lsh.a_bez11,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu11,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab11,3,0);
    ins_quest ((char *) &leer_lsh.stk11,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk11,3,0);
    ins_quest ((char *) &leer_lsh.a12,3,0);
    ins_quest ((char *) leer_lsh.a_bez12,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu12,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab12,3,0);
    ins_quest ((char *) &leer_lsh.stk12,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk12,3,0);
    ins_quest ((char *) &leer_lsh.a13,3,0);
    ins_quest ((char *) leer_lsh.a_bez13,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu13,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab13,3,0);
    ins_quest ((char *) &leer_lsh.stk13,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk13,3,0);
    ins_quest ((char *) &leer_lsh.a14,3,0);
    ins_quest ((char *) leer_lsh.a_bez14,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu14,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab14,3,0);
    ins_quest ((char *) &leer_lsh.stk14,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk14,3,0);
    ins_quest ((char *) &leer_lsh.a15,3,0);
    ins_quest ((char *) leer_lsh.a_bez15,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu15,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab15,3,0);
    ins_quest ((char *) &leer_lsh.stk15,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk15,3,0);
    ins_quest ((char *) &leer_lsh.a16,3,0);
    ins_quest ((char *) leer_lsh.a_bez16,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu16,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab16,3,0);
    ins_quest ((char *) &leer_lsh.stk16,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk16,3,0);
    ins_quest ((char *) &leer_lsh.a17,3,0);
    ins_quest ((char *) leer_lsh.a_bez17,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu17,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab17,3,0);
    ins_quest ((char *) &leer_lsh.stk17,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk17,3,0);
    ins_quest ((char *) &leer_lsh.a18,3,0);
    ins_quest ((char *) leer_lsh.a_bez18,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu18,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab18,3,0);
    ins_quest ((char *) &leer_lsh.stk18,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk18,3,0);
    ins_quest ((char *) &leer_lsh.a19,3,0);
    ins_quest ((char *) leer_lsh.a_bez19,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu19,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab19,3,0);
    ins_quest ((char *) &leer_lsh.stk19,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk19,3,0);
    ins_quest ((char *) &leer_lsh.a20,3,0);
    ins_quest ((char *) leer_lsh.a_bez20,0,5);
    ins_quest ((char *) &leer_lsh.stk_zu20,3,0);
    ins_quest ((char *) &leer_lsh.stk_ab20,3,0);
    ins_quest ((char *) &leer_lsh.stk20,3,0);
    ins_quest ((char *) &leer_lsh.pr_vk20,3,0);
    ins_quest ((char *) &leer_lsh.a_gew1,3,0);
    ins_quest ((char *) &leer_lsh.a_gew2,3,0);
    ins_quest ((char *) &leer_lsh.a_gew3,3,0);
    ins_quest ((char *) &leer_lsh.a_gew4,3,0);
    ins_quest ((char *) &leer_lsh.a_gew5,3,0);
    ins_quest ((char *) &leer_lsh.a_gew6,3,0);
    ins_quest ((char *) &leer_lsh.a_gew7,3,0);
    ins_quest ((char *) &leer_lsh.a_gew8,3,0);
    ins_quest ((char *) &leer_lsh.a_gew9,3,0);
    ins_quest ((char *) &leer_lsh.a_gew10,3,0);
    ins_quest ((char *) &leer_lsh.a_gew11,3,0);
    ins_quest ((char *) &leer_lsh.a_gew12,3,0);
    ins_quest ((char *) &leer_lsh.a_gew13,3,0);
    ins_quest ((char *) &leer_lsh.a_gew14,3,0);
    ins_quest ((char *) &leer_lsh.a_gew15,3,0);
    ins_quest ((char *) &leer_lsh.a_gew16,3,0);
    ins_quest ((char *) &leer_lsh.a_gew17,3,0);
    ins_quest ((char *) &leer_lsh.a_gew18,3,0);
    ins_quest ((char *) &leer_lsh.a_gew19,3,0);
    ins_quest ((char *) &leer_lsh.a_gew20,3,0);
            ins_cursor = sqlcursor ("insert into leer_lsh ("
"mdn,  fil,  ls,  blg_typ,  a1,  a_bez1,  stk_zu1,  stk_ab1,  stk1,  pr_vk1,  a2,  a_bez2,  "
"stk_zu2,  stk_ab2,  stk2,  pr_vk2,  a3,  a_bez3,  stk_zu3,  stk_ab3,  stk3,  pr_vk3,  a4,  "
"a_bez4,  stk_zu4,  stk_ab4,  stk4,  pr_vk4,  a5,  a_bez5,  stk_zu5,  stk_ab5,  stk5,  "
"pr_vk5,  a6,  a_bez6,  stk_zu6,  stk_ab6,  stk6,  pr_vk6,  a7,  a_bez7,  stk_zu7,  stk_ab7,  "
"stk7,  pr_vk7,  a8,  a_bez8,  stk_zu8,  stk_ab8,  stk8,  pr_vk8,  party_mdn,  a9,  a_bez9,  "
"stk_zu9,  stk_ab9,  stk9,  pr_vk9,  a10,  a_bez10,  stk_zu10,  stk_ab10,  stk10,  "
"pr_vk10,  a11,  a_bez11,  stk_zu11,  stk_ab11,  stk11,  pr_vk11,  a12,  a_bez12,  "
"stk_zu12,  stk_ab12,  stk12,  pr_vk12,  a13,  a_bez13,  stk_zu13,  stk_ab13,  stk13,  "
"pr_vk13,  a14,  a_bez14,  stk_zu14,  stk_ab14,  stk14,  pr_vk14,  a15,  a_bez15,  "
"stk_zu15,  stk_ab15,  stk15,  pr_vk15,  a16,  a_bez16,  stk_zu16,  stk_ab16,  stk16,  "
"pr_vk16,  a17,  a_bez17,  stk_zu17,  stk_ab17,  stk17,  pr_vk17,  a18,  a_bez18,  "
"stk_zu18,  stk_ab18,  stk18,  pr_vk18,  a19,  a_bez19,  stk_zu19,  stk_ab19,  stk19,  "
"pr_vk19,  a20,  a_bez20,  stk_zu20,  stk_ab20,  stk20,  pr_vk20,  a_gew1,  a_gew2,  "
"a_gew3,  a_gew4,  a_gew5,  a_gew6,  a_gew7,  a_gew8,  a_gew9,  a_gew10,  a_gew11,  "
"a_gew12,  a_gew13,  a_gew14,  a_gew15,  a_gew16,  a_gew17,  a_gew18,  a_gew19,  "
"a_gew20) "

#line 57 "leer_lsh.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 59 "leer_lsh.rpp"
}

