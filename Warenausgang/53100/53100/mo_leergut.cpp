#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "itemc.h"
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "mo_meld.h"
#include "mo_leergut.h"
#include "ptab.h"
#include "strfkt.h"
#include "ls.h"
#include "ls_txt.h"
#include "mo_auto.h"
#include "auto_nr.h"
#include "mo_menu.h"
#include "a_bas.h"
#include "ls.h"
#include "LeerGut.h"
#include "searchptab.h"

static PTAB_CLASS ptab_class;
static LS_TXT_CLASS ls_txt_class;
static AUTO_CLASS AutoClass;
CLeerGut LeerGut;


//#define LEERMAX 100


/**
 struct LP
{
	double lart;
	int    lanz;
} LeerPosArt [LEERMAX];
**/
struct LP LeerPosArt [LEERMAX];

HWND hFocus = NULL;
static int AnzZeilen = 4;
static int LeerGutText = 1;
static char KopfText[61];
static char KopfTextTab[LEERMAX][61];
static char ZeileText[LEERMAX][9];
static char KopfTextFTab[10][61];
static char KAktiv[6][4];
static int kopftextfpos;
static int kopftextfscroll;

static int kopftextpos = 0;
static int kopftextanz = 0;
static int ktpos = 0;
static int ktend = 0;
static TEXTMETRIC tmLP;
static BOOL CrCaret = FALSE;
static char *ztab[] = {" = ", " + "};
static int  zpos = 0;
static BOOL NumIn = FALSE;

static char *meeinhLP[100];
static char *meeinhLPK[100];
static double leer_art [100];

static int meeinhpos  = 0;
static int palettepos = 0;
static int ptanz = 0;
static int ptscroll = 0;
static int ptstart  = 0;
static int ptend    = 0;





static ITEM iOK ("", "     OK     ", "", 0);
static ITEM iCA ("", "  Abbrechen ", "", 0);
static ITEM iZeileLoeschen ("", "Zeile L�schen (F7)", "", 0);

static DB_CLASS DbClass;

static char aZeile1 [61];
static char aZeile2 [61];
static char aZeile3 [61];
static char aZeile4 [61];
static char z1aktiv [3];
static char z2aktiv [3];
static char z3aktiv [3];
static char z4aktiv [3];
static char aAnzahl [10];
static char aLeergBez [16];




void FillEinhLP (void);
static void strzcpy (char *dest, char *source, int len);
void stringcopy(char * Ziel, const char * Quelle, int sizeZiel) ;
void stringcopy(char * Ziel, const char * Quelle) ;
static void ToKopfFtab (void);
static void EinhToStr (void);
static void DispWindowText (HWND hWnd, char *text);
int SetLeerText (char *);
int GetMeEinhPos (char *text);
void ReadKopftext (void);
void Zeige (int i);
int setAnzahl (void);
void SetThisCaret(void);
void ScrollKopfTextDown (int zeilen);
void ScrollKopfTextUp (int zeilen);
int TestKopfText (char *kText);
void TestKopfTextBef (char *kText);
void NextKopfZeile (void);
void PriorKopfZeile (void);
int beforeAnz (void);
int beforeBez (void);
void setKAktiv (void);
void WriteKopftext (void);
void WriteLeerArt (void);
static void InitLeerPosArt (void);
static void SetLeerPosArt (double art, int anz);
void WriteLeerZeileArt (char *LeerZeile);
static BOOL IsLeerGutZeile (char *Zeile);
static BOOL IsLeerGut (double a);
static BOOL GetNextEinh (char *LeerZeile, int *anz, char *LeerBez, BOOL mode);
static double GetLeerArt (char *LeerBez);
long DelLeerArt (void);
static BOOL IsinLeerPos (double art);
int ShowLeergEx (HWND hMainWindow, char *dest, char *item);


static int testquery ();
static int testleerg ();
static int testleergbez ();
static int setz1 ();
static int setz2 ();
static int setz3 ();
static int setz4 ();

/**
static ITEM iZeile1 ("lz1",KopfTextFTab[0],"Zeile1", 0);
static ITEM iZeile2 ("lz2",KopfTextFTab[1],"Zeile2", 0);
static ITEM iZeile3 ("lz3",KopfTextFTab[2],"Zeile3", 0);
static ITEM iZeile4 ("lz4",KopfTextFTab[3],"Zeile4", 0);
***/
int i = 0;
static ITEM itext  ("ltext", "                                        Leergut-Erfassung", "", 0);
static ITEM iZeile1 ("lz1",KopfTextFTab[0],"", 0);
static ITEM iZeile2 ("lz2",KopfTextFTab[1],"", 0);
static ITEM iZeile3 ("lz3",KopfTextFTab[2],"", 0);
static ITEM iZeile4 ("lz4",KopfTextFTab[3],"", 0);
static ITEM iz1 ("z1",ZeileText[0],"", 0);
static ITEM iz2 ("z2",ZeileText[1],"", 0);
static ITEM iz3 ("z3",ZeileText[2],"", 0);
static ITEM iz4 ("z4",ZeileText[3],"", 0);
static ITEM iz1aktiv ("z1a",z1aktiv,"", 0);
static ITEM iz2aktiv ("z2a",z2aktiv,"", 0);
static ITEM iz3aktiv ("z3a",z3aktiv,"", 0);
static ITEM iz4aktiv ("z4a",z4aktiv,"", 0);
static ITEM iAnzahl ("anzahl",aAnzahl,"", 0);
static ITEM iLeergBez ("lbez",aLeergBez,"", 0);
static ITEM itextanz  ("ltextanz", "Anzahl", "", 0);
static ITEM itexteerg  ("ltextleerg", "Leergut", "", 0);




static field _leergform[] = {
           &iAnzahl,     6, 0, 8, 5, 0, "", EDIT,     beforeAnz, setAnzahl,0,
           &itexteerg,  20, 0, 7, 12, 0, "", READONLY,     0, testleergbez,0,
           &iLeergBez,  20, 0, 8, 12, 0, "", EDIT,     beforeBez, testleergbez,0,
           &iZeile1,    60, 0, 2, 13, 0, "", EDIT,     setz1, testleerg,0,
           &iZeile2,    60, 0, 3, 13, 0, "", EDIT,     setz2, testleerg,0,
           &iZeile3,    60, 0, 4, 13, 0, "", EDIT,     setz3, testleerg,0,
           &iZeile4,    60, 0, 5, 13, 0, "", EDIT,     setz4, testleerg,0,
           &itext,      80, 0, 0, 0, 0,   "",  READONLY, 0, 0 ,0,
           &iz1aktiv,    3, 0, 2, 1, 0,   "",  DISPLAYONLY, 0, 0 ,0,
           &iz2aktiv,    3, 0, 3, 1, 0,   "",  DISPLAYONLY, 0, 0 ,0,
           &iz3aktiv,    3, 0, 4, 1, 0,   "",  DISPLAYONLY, 0, 0 ,0,
           &iz4aktiv,    3, 0, 5, 1, 0,   "",  DISPLAYONLY, 0, 0 ,0,
           &iz1,         8, 0, 2, 5, 0,   "",  DISPLAYONLY, 0, 0 ,0,
           &iz2,         8, 0, 3, 5, 0,   "",  DISPLAYONLY, 0, 0 ,0,
           &iz3,         8, 0, 4, 5, 0,   "",  DISPLAYONLY, 0, 0 ,0,
           &iz4,         8, 0, 5, 5, 0,   "",  DISPLAYONLY, 0, 0 ,0,
           &itextanz,    6, 0, 7, 5, 0, "", READONLY,     0, 0,0,
           &iOK,        15, 0, 11,15, 0,  "",  BUTTON, 0,testquery ,KEY12,
           &iCA,        15, 0, 11,32, 0,  "",  BUTTON, 0,testquery ,KEY5,
           &iZeileLoeschen,   17, 0, 8,32, 0,  "",  BUTTON, 0,testquery ,KEY7,
};

static form leergform = {20, 0, 0, _leergform, 
                       0, 0, 0, 0, NULL};
        

static int testquery (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY7)
				{
					 sprintf (KopfTextFTab[kopftextfpos], "");
					return 1;
				}
		}
        return 0;
}


   
int setz1 (void)
{
	strcpy (z1aktiv,""); strcpy (z2aktiv,""); strcpy (z3aktiv,""); strcpy (z4aktiv,"");
	strcpy (z1aktiv,"**");
	kopftextfpos = 0;
	setKAktiv ();

	return 0;
}
int setz2 (void)
{
	strcpy (z1aktiv,""); strcpy (z2aktiv,""); strcpy (z3aktiv,""); strcpy (z4aktiv,"");
	strcpy (z2aktiv,"**");
	kopftextfpos = 1;
	setKAktiv ();
	return 0;
}
int setz3 (void)
{
	strcpy (z1aktiv,""); strcpy (z2aktiv,""); strcpy (z3aktiv,""); strcpy (z4aktiv,"");
	strcpy (z3aktiv,"**");
	kopftextfpos = 2;
	setKAktiv ();
	return 0;
}
int setz4 (void)
{
	strcpy (z1aktiv,""); strcpy (z2aktiv,""); strcpy (z3aktiv,""); strcpy (z4aktiv,"");
	strcpy (z4aktiv,"**");
	kopftextfpos = 3;
	setKAktiv ();
                    SetCurrentField (GetItemPos (&leergform, "anzahl"));
	return 0;
}



void SetThisCaret(void)
{
	int pos;
	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfTextFTab[kopftextfpos], '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfTextFTab[kopftextfpos], '='))
		{
            pos = (int) strlen (KopfTextFTab[kopftextfpos]);
			if (KopfTextFTab[kopftextfpos][pos - 1] == '=' ||
				KopfTextFTab[kopftextfpos][pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
    NumIn = FALSE;
	ktend = strlen (KopfTextFTab[kopftextfpos]);
    ktpos = ktend;


}



int beforeAnz (void)
{
	return 0;
}
int beforeBez (void)
{
	strcpy (aLeergBez, "");
	Zeige (-1);
	return 0;
}

int setAnzahl (void)
{
        switch (syskey)
        {
                case KEYPGD :
                case KEYDOWN :
					NextKopfZeile ();
					setKAktiv ();
                    SetCurrentField (GetItemPos (&leergform, "anzahl"));
                       return 0;
                case KEYPGU :
                case KEYUP :
					PriorKopfZeile ();
				    setKAktiv ();
                    SetCurrentField (GetItemPos (&leergform, "anzahl"));
                       return 0;
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY12 :
                case KEY11 :
                       syskey = KEY12;
                       break_enter ();
					   testleerg ();
                       return 1;
                case KEY7 :
					 sprintf (KopfTextFTab[kopftextfpos], "");
					   Zeige (-1);
					 return 1;

		}

    if (strlen (KopfTextFTab [kopftextfpos]) == 0) NumIn = 0;
	if (NumIn) return 0;
	SetThisCaret ();
              	   if (NumIn == FALSE && zpos == 2)
				   {
	                      zpos = 1;
				   }
	               else if (NumIn == FALSE && zpos == 1)
				   {
	                      strcat (KopfTextFTab[kopftextfpos], ztab [zpos]);
						  ktpos = strlen (KopfTextFTab[kopftextfpos]);
				   }

				   clipped(aAnzahl);

			       if (ktpos < 58) sprintf (KopfTextFTab[kopftextfpos],"%s %s",KopfTextFTab[kopftextfpos],aAnzahl);
				   ktpos = strlen (KopfTextFTab[kopftextfpos]);
				   if (ktend < ktpos) ktend = ktpos;
			       KopfTextFTab[kopftextfpos][ktend] = (char) 0;
	               NumIn = TRUE;
				   Zeige (-1);
				   return 0;
}

int testleergbez (void)
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEYCR :
					if (SetLeerText (aLeergBez) == -1)
					{
				       int ret = ShowLeergEx (AktivWindow, aLeergBez, "me_einh_leer"); 
						SetLeerText (aLeergBez); //testtest
					}
                    SetCurrentField (GetItemPos (&leergform, "anzahl"));
                       return 0;
                case KEY12 :
                case KEY11 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
                case KEY7 :
					 sprintf (KopfTextFTab[kopftextfpos], "");
					   Zeige (-1);
					 return 1;
		}
	return 0;
}

int testleerg (void)
/**
Lagernummer testen.
**/
{
		hFocus = GetFocus ();


        TestKopfText (KopfTextFTab[kopftextfpos]);
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
		}

/***
	    if (readleerg () == 100)
		{
			    print_mess (2, "Lager %d nicht gefunden", atol (algr));
				SetCurrentField (currentfield);
				return 0;
		}

  ******/
//		display_field (AktivWindow, &leergform.mask[1], 0, 0);
//		display_form (AktivWindow, &leergform, 0, 0);
		Zeige (-1);
        switch (syskey)
        {
                case KEY12 :
                case KEY11 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
		}
        SetCurrentField (GetItemPos (&leergform, "anzahl"));

		return 0;
}




BOOL LeergClass::EnterLeerg (HWND hWnd)
/**
Default-Lager aendern.
**/
{

        HANDLE hMainInst;
		HWND fhWnd;


        HWND lhWnd;
		int savefield;
		form *savecurrent;

		FillEinhLP ();
		if (ptanz == 0)
		{
			disp_mess ("Keine Pr�ftabelleneintr�ge f�r Leergut", 2);
		    return FALSE;
		}

        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
		fhWnd = GetFocus ();
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (FALSE);
        //SetBorder (WS_POPUP | WS_DLGFRAME);
		SetBorder (WS_OVERLAPPEDWINDOW);
		//SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
		EnableWindows (hWnd, FALSE);
        lhWnd = OpenWindowChC (15, 77, 11, 12, hMainInst,"Leerguterfassung ");

//        lhWnd = OpenColWindow (13, 77, 9, 10, LTGRAYCOL, NULL);

        SetAktivWindow (lhWnd);
		currentfield = 0;
		kopftextfpos = 0;
		kopftextpos = 0;
        ReadKopftext ();
        setKAktiv ();
        enter_form (lhWnd, &leergform, 0, 0);

		EnableWindows (hWnd, TRUE);
		SetButtonTab (FALSE);
        CloseControls (&leergform);
        DestroyWindow (lhWnd);
		SetAktivWindow (hWnd);
//		SetActiveWindow (hWnd);
        SetFocus (fhWnd);
		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5) return FALSE;
        WriteKopftext ();

        return TRUE;
}




void FillEinhLP (void)
/**
Einheiten aus Prueftabelle lesen.
**/
{
	    static BOOL EinhOK = FALSE;
		int dsqlstatus;

		if (EinhOK) return;

		EinhOK = TRUE;

		ptanz = 0;
		palettepos = ptanz;
        dsqlstatus = ptab_class.lese_ptab_all ("me_einh_leer");
        while (dsqlstatus == 0)
        {
			if ((meeinhLP[ptanz] = (char *) malloc (40)) == 0)
			{
				break;
			}
			if ((meeinhLPK[ptanz] = (char *) malloc (10)) == 0)
			{
				break;
			}
            strzcpy (meeinhLP[ptanz],  clipped (ptabn.ptbez), 16);
            stringcopy  (meeinhLPK[ptanz], clipped (ptabn.ptbezk),sizeof(meeinhLPK[ptanz]));
			leer_art[ptanz] = ratod (ptabn.ptwer1);
			if (strupcmp (ptabn.ptbez, "PALETTE", 7) == 0)
			{
				palettepos = ptanz;
			}
            ptanz ++;
            if (ptanz == 99) break;
            dsqlstatus = ptab_class.lese_ptab_all ();
        }
		meeinhLP[ptanz]  = NULL;
 	    meeinhLPK[ptanz] = NULL;
		ptscroll = 0;
}




static void strzcpy (char *dest, char *source, int len)
/**
source in dest zentrieren.
**/
{
	    int zpos;

		memset (dest, ' ', len);
		dest[len] = (char) 0;
		if ((int) strlen (source) > len - 1)
		{
			source[len - 1] = (char) 0;
		}
		zpos = (int) (double) ((double) (len - strlen (source)) / 2 + 0.5);
		zpos = max (0, zpos);
		memcpy (&dest[zpos], source, strlen (source));
}
void stringcopy(char * Ziel, const char * Quelle, int sizeZiel) 
{
	 int lenQuelle = strlen (Quelle) ;
	 int lenZiel = strlen (Ziel) ;
	 if (sizeZiel == 1) {strcpy (Ziel,Quelle); return; }
	 if (sizeZiel == 4) {strcpy (Ziel,Quelle); return; }
	 if (lenQuelle > sizeZiel) 
	 {
		 lenQuelle = sizeZiel;
	 }
	 strncpy (Ziel, Quelle, lenQuelle);
	 Ziel[lenQuelle] = 0;
}
void stringcopy(char * Ziel, const char * Quelle) 
{
	 strcpy (Ziel,Quelle);
}
static void ToKopfFtab (void)
/**
Kopfdelder aus Array in FormFelder uebertragen.
**/
{
      int i, j;

	  while (kopftextfscroll < 0) kopftextfscroll ++;

      for (i = 0, j = kopftextfscroll; i < AnzZeilen; i ++, j ++)
	  {
		  stringcopy (KopfTextFTab[i], KopfTextTab[j],sizeof(KopfTextFTab[i]));
	  }
	  /**
	  stringcopy (aZeile1, KopfTextFTab[0],sizeof(aZeile1)-1);
	  stringcopy (aZeile2, KopfTextFTab[1],sizeof(aZeile2)-1);
	  stringcopy (aZeile3, KopfTextFTab[2],sizeof(aZeile3)-1);
	  stringcopy (aZeile4, KopfTextFTab[3],sizeof(aZeile4)-1);
	  **/
	//  display_form (AktivWindow, &leergform, 0, 0);

//      Zeige (-1);

}

static void EinhToStr (void)
/**
Einheit an String anhaengen.
**/
{
	char Einheit[20];
	char *MeH;

	if (NumIn == FALSE) return;


    stringcopy (Einheit, meeinhLPK[meeinhpos],sizeof(Einheit));
	MeH = strtok (Einheit, " ");
	strcat (KopfTextFTab[kopftextfpos], " ");
    strcat (KopfTextFTab[kopftextfpos], MeH);

	if (zpos == 0)
	{
	        strcat (KopfTextFTab[kopftextfpos], ztab [zpos]);
	        zpos = 2;
	}

	ktend = strlen (KopfTextFTab[kopftextfpos]);
	ktpos = ktend;
//    DispWindowText (fAuswahlEinh.mask[kopftextfpos].feldid, NULL);
//	SetLPCaret ();
	NumIn = FALSE;

	Zeige (-1);
//    SetFocus (fAuswahlEinh.mask[currentfield].feldid);
}

static void DispWindowText (HWND hWnd, char *text)
/**
Text in Fenster anzeigen.
**/
{
	 InvalidateRect (hWnd, NULL, TRUE);
	 UpdateWindow (hWnd);
}

int SetLeerText (char *bez)
{
		meeinhpos = GetMeEinhPos (bez);  
		if (meeinhpos == -1) return -1;
		EinhToStr ();
		return 0;
}
int GetMeEinhPos (char *text)
{
	    int i;

		for (i = 0; i < ptanz; i ++)
		{
			if (strcmp (clipped(text), clipped(meeinhLPK[i])) == 0)  //in LPK ge�ndert!
			{
				return i;
			}
		}
		return -1;
}

void ReadKopftext (void)
/**
Kopftext lesen.
**/
{
		int dsqlstatus;
		int zei;
		int i;


		for (i = 0; i < AnzZeilen; i ++)
		{
		          memset (KopfTextFTab[i], ' ', 60);
				  KopfTextFTab[i][60] = 0;
	 		      clipped (KopfTextFTab[i]);
		}
		kopftextfpos = kopftextfscroll = 0;


		for (i = 0; i < LEERMAX; i ++)
		{
		          memset (KopfTextTab[i], ' ', 60);
				  KopfTextTab[i][60] = 0;
	 		      clipped (KopfTextTab[i]);
		}
		for (i = 0; i < LEERMAX; i ++)
		{
			      sprintf (ZeileText[i], "Zeile%d",i+1);
		}

		kopftextanz = zei = 0;

		if (lsk.kopf_txt == 0l)
		{
		           ToKopfFtab ();
				   return;
		}

        if (LeerGutText)
        {
		   ls_txt.nr = lsk.kopf_txt;
	       dsqlstatus = ls_txt_class.dbreadfirst ();
		   while (dsqlstatus == 0)
           {
			      clipped (ls_txt.txt);
                  stringcopy (KopfTextTab[zei], ls_txt.txt,sizeof(KopfTextTab[zei]));
				  kopftextanz ++;
				  zei ++;
				  dsqlstatus = ls_txt_class.dbread ();
           }
		}
		ToKopfFtab ();
}



void Zeige (int i)
{
	int x = 0;

      for (x = 0; x < AnzZeilen ; x++)
	  {
		  sprintf (ZeileText[x], "Zeile%d",x+1 + kopftextfscroll);
	  }

	if (i == -1)
	{
		for (i = 0; i < leergform.fieldanz; i ++)
		{
			display_field (AktivWindow, &leergform.mask[i], 0, 0);
		}
	}
	else
	{
		display_field (AktivWindow, &leergform.mask[i], 0, 0);
	}
}

void ScrollKopfTextDown (int zeilen)
/**
Kopftext nach unten scrollen.
**/
{

	if (kopftextfscroll > 0)
	{
		kopftextfscroll --;
	}
	ToKopfFtab ();
      Zeige (-1);

}


void ScrollKopfTextUp (int zeilen)
/**
Kopftext nach oben scrollen.
**/
{
	if (kopftextfscroll < 95)
	{
		kopftextfscroll ++;
	}
	ToKopfFtab ();
      Zeige (-1);
}
int TestKopfText (char *kText)
/**
Beim Verlassen einer Zeile, den Kopfinhalt testen.
**/
{
	 int len;
	 char *gpos;


	 len = strlen (kText);
	 if (kText[len - 1] != '=' &&
		 kText[len - 2] != '=')
	 {
		 return len;
	 }

	 gpos = strchr (kText, '=');

	 for (gpos = gpos - 1;  gpos != kText; gpos -=1)
	 {
		 if (*gpos > ' ')
		 {
			 *(gpos + 1) = 0;
			 return len;
		 }
	 }
	 return len;
}


void TestKopfTextBef (char *kText)
/**
Vor dem Enter einer Zeile, den Kopfinhalt testen.
**/
{
	 if (strcmp (kText, " ") <= 0) return;

	 if (strchr (kText, '=') == NULL)
	 {
		 strcat (kText, " = ");
	 }
}
void NextKopfZeile (void)
{
	int pos;

	EinhToStr ();
	stringcopy (KopfTextTab[kopftextpos], KopfTextFTab [kopftextfpos],sizeof(KopfTextTab[kopftextpos]));
	TestKopfText (KopfTextTab[kopftextpos]);
	TestKopfText (KopfTextFTab[kopftextfpos]); //151214
	if (kopftextpos > 59) return;

//testtest	if (strcmp (KopfTextTab[kopftextpos], " ") <= 0) return;

	kopftextpos ++;
	if (kopftextfpos < AnzZeilen - 1)
	{
	          kopftextfpos ++;
	}
	else
	{
              ScrollKopfTextUp (1);

	}
	if (kopftextpos >= kopftextanz) kopftextanz = kopftextpos + 1;
    stringcopy (KopfTextFTab[kopftextfpos], KopfTextTab[kopftextpos],sizeof(KopfTextFTab[kopftextfpos]));
	clipped (KopfTextFTab[kopftextfpos]);
	TestKopfTextBef (KopfTextFTab[kopftextfpos]);
	ktend = strlen (KopfTextFTab[kopftextfpos]);
	stringcopy (KopfText,KopfTextFTab[kopftextfpos],sizeof(KopfText,KopfTextFTab[kopftextfpos]));
    ktpos = ktend;
	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
	stringcopy (KopfTextFTab[kopftextfpos], KopfText,sizeof(KopfTextFTab[kopftextfpos]));
    NumIn = FALSE;
}


void PriorKopfZeile (void)
{
	int pos;

	EinhToStr ();
	stringcopy (KopfTextTab[kopftextpos], KopfTextFTab[kopftextfpos],sizeof(KopfTextTab[kopftextpos]));
	TestKopfText (KopfTextTab[kopftextpos]);
	TestKopfText (KopfTextFTab[kopftextfpos]); //151214
	if (kopftextpos < 1) return;
	kopftextpos --;
	if (kopftextfpos > 0)
	{
	          kopftextfpos --;
	}
	else
	{
              ScrollKopfTextDown (1);
	}
    stringcopy (KopfTextFTab[kopftextfpos], KopfTextTab[kopftextpos],sizeof(KopfTextFTab[kopftextfpos]));
	stringcopy (KopfText,KopfTextFTab[kopftextfpos],sizeof(KopfText));
	clipped (KopfTextFTab[kopftextfpos]);
	TestKopfTextBef (KopfTextFTab[kopftextfpos]);
	ktend = strlen (KopfTextFTab[kopftextfpos]);
	stringcopy (KopfText,KopfTextFTab[kopftextfpos],sizeof(KopfText));
    ktpos = ktend;
	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
	stringcopy (KopfTextFTab[kopftextfpos], KopfText,sizeof(KopfTextFTab[kopftextfpos]));
    NumIn = FALSE;
}

void setKAktiv (void)
{
	/**
	int i;
		for (i = 0; i < AnzZeilen; i ++)
		{
		          memset (KAktiv[i], ' ', 3);
				  KAktiv[i][3] = 0;
	 		      clipped (KAktiv[i]);
		}
		if (kopftextfpos < 4) strcpy (KAktiv [kopftextfpos] , "**");
		**/
	strcpy (z1aktiv,""); strcpy (z2aktiv,""); strcpy (z3aktiv,""); strcpy (z4aktiv,"");
	switch (kopftextfpos)
	{
	case 0:
			strcpy (z1aktiv,"**");
			break;
	case 1:
			strcpy (z2aktiv,"**");
			break;
	case 2:
			strcpy (z3aktiv,"**");
			break;
	case 3:
			strcpy (z4aktiv,"**");
			break;
	}


		Zeige (-1);
}

void WriteKopftext (void)
/**
Kopftext schreiben.
**/
{
        extern short sql_mode;
		int dsqlstatus;
		int zei;

		if (kopftextanz == 0)
		{
			kopftextanz ++;
		}
	    stringcopy (KopfTextTab[kopftextpos], KopfTextFTab[kopftextfpos],sizeof(KopfTextTab[kopftextpos]));
  	    commitwork ();
		beginwork ();
        if (LeerGutText)
        {
          if (lsk.kopf_txt == 0)
          {
                sql_mode = 1;
                dsqlstatus = AutoClass.nvholid (0, 0, "ls_txt");
				if (dsqlstatus == -1)
				{
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"ls_txt\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
				}

                if (dsqlstatus == 100)
                {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  "ls_txt",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (0,
                                                        0,
                                                        "ls_txt");
                           }
                }
                sql_mode = 0;
                if (auto_nr.nr_nr == 0l)
                {
                    disp_mess ("Es konnte keine Text-Nr generiert werden", 2);
                    return;
                }

                lsk.kopf_txt = auto_nr.nr_nr;
                stringcopy (lsk.pers_nam, sys_ben.pers_nam,sizeof(lsk.pers_nam));

                auto_nr.nr_nr = 0;

                 commitwork ();

                 beginwork ();
          }
		  ls_txt.nr = lsk.kopf_txt;
		  ls_txt_class.delete_aufpposi ();
		  for (zei = 0; zei < kopftextanz; zei ++)
          {
			      if (KopfTextTab[zei][0] == 0) continue;
			      ls_txt.nr  = lsk.kopf_txt;
				  ls_txt.zei = zei + 1;
				  stringcopy (ls_txt.txt, KopfTextTab[zei],sizeof(ls_txt.txt));
				  ls_txt_class.dbupdate ();
          }
		}
		WriteLeerArt ();
//		WriteTrapos (KopfTextTab, kopftextanz);
}



static void InitLeerPosArt (void)
/**
Tabelle mit Leergutartikel initialisieren.
**/
{
	    int i;

		for (i = 0; i < LEERMAX; i ++)
		{
			LeerPosArt[i].lart = (double) 0.0;
			LeerPosArt[i].lanz = 0;
		}
}

static void SetLeerPosArt (double art, int anz)
/**
Tabelle mit Leergutartikel initialisieren.
**/
{
	    int i;

		if (anz == 0) return;
		if (art == (double) 0.0) return;

		for (i = 0; LeerPosArt[i].lanz; i ++)
		{
			if (i == LEERMAX) return;
			if (LeerPosArt[i].lart == art)
			{
				break;
			}
		}

		LeerPosArt[i].lart  = art;
		LeerPosArt[i].lanz += anz;
}



void WriteLeerArt (void)
/**
Artikel als Positionen fuer Leergut schreiben.
**/
{
	     int i = 0;
		 long max_posi = 0;
		 int pos = 0;
//	   KEINHEIT keinheit;
//	   double pr_vk;
//       char ptwert [10];


		 InitLeerPosArt ();
		 for (i = 0; i < kopftextanz; i ++)
		 {
			 WriteLeerZeileArt (KopfTextTab[i]);
		 }

		 max_posi = DelLeerArt ();
/**
         pos = aufpanz;
		 max_posi += 10;



	 for (i = 0; LeerPosArt[i].lanz; i ++)
	 {
       lsp.posi       = max_posi;
//       lsp.sa_kz_sint = atoi  (lsptab[pos].sa_kz_sint);
			     sprintf (aufps.a, "%.0lf", LeerPosArt[i].lart);
                 GetMeEinh ();
			     stringcopy (aufps.auf_me_bz, aufps.lief_me_bz,sizeof(aufps.auf_me_bz));
			     stringcopy (aufps.me_einh_kun, aufps.me_einh,sizeof(aufps.me_einh_kun));
			 sprintf (aufps.auf_me,  "%d", LeerPosArt[i].lanz);
				 sprintf (aufps.lief_me, "%d", LeerPosArt[i].lanz);
       lsp.a          = LeerPosArt[i].lart;
       lsp.lief_me     = LeerPosArt[i].lanz;
       lsp.auf_me     = LeerPosArt[i].lanz;
	   stringcopy (lsp.erf_kz, "H");

       lsp.rab_satz   = ratod (lsptab[pos].rab_satz);
       lsp.prov_satz  = ratod (lsptab[pos].prov_satz);
       sprintf (lsp.lief_me_bz,"%s",lsptab[pos].me_bz);

       lsp.ls_vk_pr  = ratod (lsptab[pos].ls_vk_dm);
       lsp.ls_lad_pr  = ratod (lsptab[pos].ls_lad_dm);
       lsp.ls_vk_euro  = ratod (lsptab[pos].ls_vk_euro);
       lsp.ls_lad_euro  = ratod (lsptab[pos].ls_lad_euro);
       lsp.ls_vk_fremd  = ratod (lsptab[pos].ls_vk_fremd);
       lsp.ls_lad_fremd  = ratod (lsptab[pos].ls_lad_fremd);

       sprintf (lsp.lief_me_bz,"%s",lsptab[pos].basis_me_bz);
       if (lsp.a == (double) 0)
       {
           return;
       }
       lsp.me_einh_kun = atoi (lsptab[pos].me_einh_kun);
	   if (lsp.me_einh_kun < 0) lsp.me_einh_kun = 2;
       strcpy (lsp.auf_me_bz, lsptab[pos].me_bz);


       strcpy (kumebest.kun_bran2, kun.kun_bran2);
       lsp.lief_me = ratod (lsptab[pos].lief_me);

       einh_class.AktAufEinh (lsk.mdn, lsk.fil,
                                lsk.kun, lsp.a, lsp.me_einh_kun);
       strcpy (kumebest.kun_bran2, kun.kun_bran2);

       memcpy (&lsps, &lsptab[pos], sizeof (struct LSPS));

       einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                              lsk.kun, lsp.a, &keinheit);


       lsp.me_einh     = atoi (lsptab[pos].me_einh);
	   if (lsp.me_einh < 0) lsp.me_einh = 2;
       sprintf (ptwert, "%hd", lsp.me_einh);
       if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
       {
            strcpy (lsp.lief_me_bz, ptabn.ptbezk);
       }
       lsp.lsp_txt    = atol (lsptab[pos].lsp_txt);

       lsp.me_einh_kun1 = atoi (lsptab[pos].me_einh_kun1);
	   if (lsp.me_einh_kun1 < 0) lsp.me_einh_kun1 = 2;
       lsp.auf_me1      = ratod (lsptab[pos].auf_me1);
       lsp.inh1         = ratod (lsptab[pos].inh1);

       lsp.me_einh_kun2 = atoi (lsptab[pos].me_einh_kun2);
	   if (lsp.me_einh_kun2 < 0) lsp.me_einh_kun2 = 2;
       lsp.auf_me2      = ratod (lsptab[pos].auf_me2);
       lsp.inh2         = ratod (lsptab[pos].inh2);

       lsp.me_einh_kun3 = atoi (lsptab[pos].me_einh_kun3);
	   if (lsp.me_einh_kun3 < 0) lsp.me_einh_kun3 = 2;
       lsp.auf_me3      = ratod (lsptab[pos].auf_me3);
       lsp.inh3         = ratod (lsptab[pos].inh3);

       strcpy (ptabn.ptitem,"sap_kond_art");
       lsp.a_grund = ratod (lsptab[pos].a_grund);
       strcpy (lsp.kond_art, lsptab[pos].kond_art0);
       lsp.posi_ext  = atol (lsptab[pos].posi_ext);
       strcpy (lsp.ls_charge, lsptab[pos].ls_charge);
       lsp.ls_pos_kz = lsptab[pos].ls_pos_kz;

		if (strcmp (clipped (lsptab[pos].lief_me_bz_ist), " ") <= 0)
		{
			memcpy (&lsps, &lsptab[pos], sizeof (lsps));
			_a_bas.a = ratod (lsps.a);
			GetMeEinhIst ();
			memcpy (&lsptab[pos], &lsps, sizeof (lsps));
		}

		strcpy (lsp.lief_me_bz_ist, lsptab[pos].lief_me_bz_ist);
        lsp.me_einh_ist = lsptab[pos].me_einh_ist;
		lsp.me_ist = lsptab[pos].me_ist;
		lsp.inh_ist = lsptab[pos].inh_ist;
        lsp.hbk_date = lsptab[pos].hbk_date;  
		lsp.lief_me1 = lsptab[pos].lief_me1;
		lsp.lief_me2 = lsptab[pos].lief_me2;
		lsp.lief_me3 = lsptab[pos].lief_me3;
		lsp.auf_me_vgl = lsptab[pos].auf_me_vgl;
		lsp.teil_smt   = atoi (lsptab[pos].teil_smt);

		if (lsp.hbk_date <= 0l)
		{
			lese_a_bas (lsp.a);
			char datum [12];
		    if (CalcMhd == LIEFERDATUM)
			{
				dlong_to_asc (lsk.lieferdat, datum);
				datplushbk (datum);
			}
			else
			{
				sysdate (datum);
				datplushbk (datum);
			}
			lsp.hbk_date  = dasc_to_long (datum);
		}
		if (lsp.hbk_date <= 0l)
		{
			memcpy (&lsp.hbk_date, LONGNULL, sizeof (LONGNULL));
		}


		if (ls_mhd_par) lsp.hbk_date = dasc_to_long (lsptab[pos].mhd);  //in diesem Fall wird nur manuell eingegeben

	   strcpy (lsp.ls_ident, lsptab[pos].ls_ident);
	   lsp.aufschlag = (short) atoi (lsptab[pos].aufschlag);
	   lsp.aufschlag_wert = ratod (lsptab[pos].aufschlag_wert);

	   if ( _a_bas.a != lsp.a )	// extra nachlesen wegen Hinsemann : in der rosi war jede bearbeitete Position == 3  050912
					lese_a_bas (lsp.a);
	   if ( lsp.lief_me != 0 && _a_bas.a_typ == LEERGUT )
		   lsp.pos_stat = 3 ;	// kompatibel wegen Hinsemann
 
	   
	   
	   if (LeergutAnnahme && (lsp.a >= LeergutVon &&
                              lsp.a <= LeergutBis))
	   {
	          lsp.pos_stat = 3;
	   }
	   else if (lsp.pos_stat < 3)
	   {
	          lsp.pos_stat = 2;
	   }
	   lsp.pos_txt_kz = lsptab[pos].pos_txt_kz;
	   lsp.tara = lsptab[pos].tara;
       testdecval ();
       ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls,
                             lsp.a, lsp.posi);
       pr_vk = ratod (lsptab[pos].ls_vk_pr);
       if (Getib ())
       {
           UpdateBsd ();
       }
	   lsp.pos_stat = 2 ;	
    	 max_posi += 10;
	    pos += 1;
	} //end for

/***
 		 for (i = 0; LeerPosArt[i].lanz; i ++)
		 {
                 if (! artikelOK (LeerPosArt[i].lart, FALSE)) continue;
			     sprintf (aufps.a, "%.0lf", LeerPosArt[i].lart);
                 GetMeEinh ();
			     stringcopy (aufps.auf_me_bz, aufps.lief_me_bz,sizeof(aufps.auf_me_bz));
			     stringcopy (aufps.me_einh_kun, aufps.me_einh,sizeof(aufps.me_einh_kun));
				 sprintf (aufps.auf_me,  "%d", LeerPosArt[i].lanz);
				 sprintf (aufps.lief_me, "%d", LeerPosArt[i].lanz);
				 stringcopy (aufps.erf_kz, "H");
                 write_wawiepro (ratod (aufps.lief_me), ratod (aufps.lief_me), (double) 0.0, 0 );
				 bsd_me = ratod (aufps.lief_me);
				 sprintf (aufps.ls_pos_kz, "%d", 1);
				 stringcopy (aufps.s, "3");
				 sprintf (aufps.pnr, "%ld", max_posi);
                 memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
                 is_lspeinzel = arrtolsp (pos);
                 if (lsp.lief_me > MAXME || lsp.lief_me3 > MAXME)
                 {
                            print_messG (2, "Wert %3lf ist zu gro�", lsp.lief_me);
                 }
                 else
                 {
                           ls_class.update_lsp (is_lspeinzel,lsp.mdn, lsp.fil, lsp.ls, lsp.a, lsp.posi);
                 }
	             if (bsd_me != 0.0)
				 {
						LeerGut.Update (ratod (aufparr[pos].a), bsd_me, ratod (aufparr[pos].auf_pr_vk));
				 }
				 max_posi += 10;
				 pos += 1;
		 }
***/
}


void WriteLeerZeileArt (char *LeerZeile)
/**
Eine Zeile fuer Leegutartikel bearbeiten.
**/
{
	     double art;
		 int anz;
		 char Bez[20];

		 if (IsLeerGutZeile(LeerZeile) == FALSE) return;

		 if (GetNextEinh (LeerZeile, &anz, Bez, 0) == FALSE) return;

		 art = GetLeerArt (Bez);
		 if (IsLeerGut (art))
		 {
			 SetLeerPosArt (art, anz);
		 }

		 while (GetNextEinh (LeerZeile, &anz, Bez, 1))
		 {
           		 art = GetLeerArt (Bez);
		         if (IsLeerGut (art))
				 {
				         SetLeerPosArt (art, anz);
				 }
		 }
}

static BOOL IsLeerGutZeile (char *Zeile)
/**
Test, ob die Zeile eine Leergutzeile ist.
**/
{
	     int i, j;
		 int anz;

	     for (i = 0; i < ptanz; i ++)
		 {
			 if (strstr (Zeile, meeinhLPK[i]))
			 {
				 break;
			 }
		 }
		 if (i == ptanz) return FALSE;

		 anz = wsplit (Zeile, " ");
		 if (anz == 0) return FALSE;

		 for (i = 0; i < anz; i ++)
		 {
			 if (strcmp (wort[i], "+") == 0) continue;
			 if (strcmp (wort[i], "=") == 0)continue;
			 if (numeric (wort[i])) continue;
  	         for (j = 0; j < ptanz; j ++)
			 {
			        if (strcmp (wort[i], meeinhLPK[j]) == 0)
					{
				              break;
					}
			 }
			 if (j == ptanz) return FALSE;
		 }
		 return TRUE;
}

static BOOL IsLeerGut (double a)
/**
Artikeltyp pruefen.
**/
{
	     int dsqlstatus;

		 if (a <= (double) 0.0) return FALSE;
		 dsqlstatus = lese_a_bas (a);
		 if (dsqlstatus == 100) return FALSE;
         if (_a_bas.a_typ != 11) return FALSE;
		 return TRUE;
}

static BOOL GetNextEinh (char *LeerZeile, int *anz, char *LeerBez, BOOL mode)
/**
Nexte Leergutbezeichnung und Leergutanzahl aus Zeile holen.
**/
{
	     static char *pos = NULL;
		 static char *LZ = NULL;
		 char anzstr [10];
		 int i;

		 if (LZ != LeerZeile)
		 {
			 pos = LeerZeile;
		 }
		 if (mode == FALSE)
		 {
			 pos = LeerZeile;
		 }

		 for (; *pos; pos += 1)
		 {
			 if (*pos == '+');
			 else if (*pos == '=');
			 else if (*pos > ' ')
			 {
				 break;
			 }
		 }

         if (*pos == 0)
		 {
			  *anz = 0;
              LeerBez[0] = 0;
			  return FALSE;
		 }

		 i = 0;
		 for (; *pos; pos += 1)
		 {
			 if (*pos < '0' || *pos > '9')
			 {
				 break;
			 }
			 anzstr[i] = *pos;
			 i ++;
		 }

         if (*pos == 0)
		 {
			  *anz = 0;
              LeerBez[0] = 0;
			  return FALSE;
		 }

		 LZ = LeerZeile;
         anzstr[i] = 0;
		 *anz = atoi (anzstr);
		 for (; *pos; pos += 1)
		 {
			 if (*pos == '+');
			 else if (*pos == '=');
			 else if (*pos > ' ')
			 {
				 break;
			 }
		 }

         if (*pos == 0)
		 {
              LeerBez[0] = 0;
			  return FALSE;
		 }


		 i = 0;
		 for (; *pos; pos += 1)
		 {
			 if (*pos <= ' ')
			 {
				 break;
			 }
             LeerBez[i] = *pos;
			 i ++;
		 }
		 LeerBez[i] = 0;
		 return TRUE;
}

static double GetLeerArt (char *LeerBez)
/**
Artikel-Nummer zu Leergutbezeichnung holen.
**/
{
	    int i;

		for (i = 0; i < ptanz; i ++)
		{
			if (strcmp (meeinhLPK[i], LeerBez) == 0)
			{
				return leer_art[i];
			}
		}
		return (double) 0.0;
}

static BOOL IsinLeerPos (double art)
/**
Test, ob Leergutartikel aud Positionen im Kopftext erfasst wurden.
**/
{
        int i;

		for (i = 0; LeerPosArt[i].lanz; i ++)
		{
			if (i == LEERMAX) return FALSE;
			if (LeerPosArt[i].lart == art)
			{
				return TRUE;
			}
		}
		return FALSE;
}

long DelLeerArt (void)
/**
Positionszeilen mit Leergutartikeln, die als KopfTexte erfass wurden aus den Positionen
loeschen.
**/
{
	     int i;
		 static double art;
		 static double lief_me;
		 static double ls_vk_pr;
		 double bsd_me;
		 long posi;
		 long max_posi;
		 static int cursor = -1;
		 static int such_cursor = -1;
         static int ls;
		struct LEERG
		{
			double a;
			int    posi;
			double lief_me;
			double ls_vk_pr;
		} leerg [LEERMAX];


		 if (cursor == -1)
		 {
			 DbClass.sqlin ((short *)  &lsk.mdn, 1, 0);
			 DbClass.sqlin ((short *)  &lsk.fil, 1, 0);
			 DbClass.sqlin ((long *)   &lsk.ls,  2, 0);
			 DbClass.sqlin ((double *) &art,     3, 0);
			 cursor = DbClass.sqlcursor ("delete from lsp where mdn = ? "
				                                          "and fil  = ? "
														  "and ls   = ? "
														  "and a    = ?");
		 }
		 if (such_cursor == -1)
		 {
			 DbClass.sqlin ((short *)  &lsk.mdn, 1, 0);
			 DbClass.sqlin ((short *)  &lsk.fil, 1, 0);
			 DbClass.sqlin ((long *)   &lsk.ls,  2, 0);
			 DbClass.sqlout ((double *) &art,     3, 0);
			 DbClass.sqlout ((long *) &posi,     2, 0);
			 DbClass.sqlout ((double *) &lief_me,     3, 0);
			 DbClass.sqlout ((double *) &ls_vk_pr,     3, 0);
			 such_cursor = DbClass.sqlcursor ("select a,posi,lief_me,ls_vk_pr from lsp where mdn = ? "
				                                          "and fil  = ? "
														  "and ls   = ? order by posi ");
		 }
		DbClass.sqlopen (such_cursor);
		int di = 0;
		while ( DbClass.sqlfetch (such_cursor) == 0)
		{
			leerg[di].a = art;
			leerg[di].posi = posi;
			leerg[di].lief_me = lief_me;
			leerg[di].ls_vk_pr = ls_vk_pr;
			di ++;
		}
		di --;
		
		 max_posi = 0;
		for (i = di; i >= 0; i--)
		{
			art = leerg[i].a;
			posi = leerg[i].posi;
			lief_me = leerg[i].lief_me;
			if (posi > max_posi) max_posi = posi;
			if (IsinLeerPos (art))
			{
				 bsd_me = 0 - lief_me;
				 DbClass.sqlexecute (cursor);
	             if (bsd_me != 0.0)
				 {
						LeerGut.Update (leerg[i].a, bsd_me, leerg[i].ls_vk_pr);
				 }
			 }

		}
		 return max_posi;
}

int ShowLeergEx (HWND hMainWindow, char *dest, char *item)
{
       char wert [80];
	   char ptbezk [9];
       SEARCHPTAB *SearchPtab;
	   HINSTANCE hMainInst;
	   int pcursor ;


       hMainInst = (HINSTANCE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
       SearchPtab = new SEARCHPTAB (item);
       SearchPtab->SetParams (hMainInst, hMainWindow); 
       SearchPtab->Setawin (hMainWindow);
                                                  
       SearchPtab->Search ();
       if (SearchPtab->GetKeyErw (wert,0) == TRUE)
       {
           delete SearchPtab;

		     int dlfnr = atoi (wert);
			 DbClass.sqlin ((long *)   &dlfnr,  2, 0);
			 DbClass.sqlout ((char *) ptbezk,     0, 8);
			 pcursor = DbClass.sqlcursor ("select ptbezk from ptabn where ptitem  = \"me_einh_leer\"  "
				                                          "and ptlfnr  = ? ");
			DbClass.sqlopen (pcursor);
			DbClass.sqlfetch (pcursor);
           strcpy (dest, ptbezk);
			DbClass.sqlclose (pcursor);
		   return 0;
       }
       delete SearchPtab;
       return -1;
}
