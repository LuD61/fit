#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "DdeDial.h"
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "dbclass.h"
#include "mo_menu.h"
#include "kun.h"
#include "sys_par.h"
#include "vertr.h"
#include "mdn.h"
#include "fil.h"
// #include "mo_numme.h"
#include "mo_auto.h"
#include "mdnfil.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "mo_qa.h"
#include "ls.h"
#include "a_bas.h"
#include "ptab.h"
#include "Text.h"
#include "mo_aufl.h"
#include "cmask.h"
#include "bmlib.h"
#include "tou.h"
#include "wo_tou.h"
#include "mo_qtou.h"
#include "mo_qvertr.h"
#include "message.h"
#include "mo_kompl.h"
#include "mo_last.h"
#include "mo_kf.h"
#include "auto_nr.h"
#include "mo_progcfg.h"
#include "mo_lgr.h"
#include "mo_leergut.h"
#include "mo_lsgen.h"
#include "mo_inf.h"
#include "mo_ch.h"
#include "mo_chq.h"
#include "mo_chqex.h"
#include "datum.h"
#include "mo_vers.h"
#include "ls_txt.h"
#include "aktion.h"
#include "akt_krz.h"
#include "a_pr.h"
#include "mo_a_pr.h"
#include "mo_preis.h"
#include "listcl.h"
#include "colbut.h"
#include "lbox.h"
#include "searchkunlief.h"
#include "searchliefkun.h"
#include "searchaart.h"
#include "searchkun.h"
#include "searchkuncc.h"
#include "kunlief.h"
#include "sounds.h"
#include "searcherror.h"
#include "kasse.h"
#include "lizenz.h"
#include "DataCollection.h"
#include "LskList.h"
#include "NewCalculate.h"
#include "Application.h"
#include "smthandler.h"
#include "Debug.h"

#define MAXLEN 40
#define MAXPOS 3000
#define LPLUS 1
 
#define IDM_FRAME     901
#define IDM_REVERSE   902
#define IDM_NOMARK    903
#define IDM_EDITMARK  904
#define IDM_PAGEVIEW  905
#define IDM_LIST      906
#define IDM_PAGE      907
#define IDM_FONT      908
#define IDM_FIND      909
#define IDM_ANZBEST   910
#define IDM_STD       911
#define IDM_BASIS     912
#define IDM_LGR       913
#define IDM_LGRDEF    914
#define IDM_POSTEXT   915
//#define IDM_VINFO     916
#define IDM_LDPR      917
#define IDM_VKPR      918
#define IDM_MUSTER    923 
#define IDM_KUNLIEF   924
#define IDM_KASSE     919

#define QUERYAUF      920 
#define QUERYKUN      921 
#define QUERYTOU      922 
#define IDM_ERROR     930 
#define IDM_PARTYSERVICE    931 
#define IDM_FULLTAX   932 
#define IDM_NEWPR   933 
#define IDM_SORTPR   934 
#define IDM_ALLNEWPR   935 
#define IDM_PRIOR_ROW  936 
#define IDM_NEXT_ROW   937 
#define IDM_FIRST_ROW   938 
#define IDM_LAST_ROW   939 
#define IDM_SORTART   940 
#define IDM_ENTER_AUF_ME   941 
#define IDM_ENTER_LIEF_ME3 942 

#define PARTY_POSITION 17
#define TAB 9

#define FDEBUG CDebug::GetInstance ()
#define DBLINE CDebug::GetInstance ()->GetLine ()

static BOOL NewStyle = FALSE;
static char FaxTrenner = TAB;

static char *Programm = "53100";

static BOOL PrintLM = FALSE;


static HANDLE NewPrPlugin = NULL;
CDataCollection<CLskList> * (*ChoiceLs)() = NULL; 
void (*ChoiceLsEx)(CDataCollection<CLskList> *) = NULL; 
long (*LskChoiceLs)(short, short, BOOL) = NULL; 
long (*LskNextRow)() = NULL; 
long (*LskPriorRow)() = NULL; 
long (*LskFirstRow)() = NULL; 
long (*LskLastRow)() = NULL; 
long (*LskCurrentRow)() = NULL; 
void (*LskDeleteRow)(short, short, long) = NULL; 
void (*Destroy)() = NULL; 

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL StaticWProc(HWND,UINT,WPARAM,LPARAM);
static int ProcessMessages(void);
void     InitFirstInstance(HANDLE);
BOOL     InitNewInstance(HANDLE, int);
void     DisplayLines ();
void     MoveButtons (void);
void     PrintButtons (void);
void     ShowDaten (void);
int      lesetou (void);
int      TouZTest (void);
static   void FreeLsk (void);
static   int testkun0 (void);
static   void EnableArrows (form *, BOOL);
static   void IncDatum (char *);
static   void TelefonList (void);  
static   int FaxAuf (void);
static   int RunClipDial (char *);
static   int RunDdeDial (char *);
static   void TestActiveDial (void);
static   void SetMuster (BOOL); 
static   void SetParty (BOOL); 
static   void SetKunLief (void); 
static   void SetMusterAuftrag (void);
static   void SetPartyservice (void);
static   int ChoiseKunLief0 (void);
static   int ChoiseKunLief (void);
static   BOOL ShowKun ();
static   BOOL ShowKuncc ();
static   void EnableKasse ();
static	 void RestartProcess ();
static	 void CheckPartyService (BOOL b);
static	 void SetPartyActive ();
static   void EnablePartyService (BOOL b);
void EnableFullTax ();
void SetLsToFullTax ();
void SetLsToStandardTax ();
void SetToFullTax ();
void SetToStandardTax ();
void TestLizenz ();
void LskChoicePriorRow ();
void LskChoiceNextRow ();
void LskChoiceFirstRow ();
void LskChoiceLastRow ();
void LskChoiceCurrentRow ();
void LskChoiceDeleteRow (short mdn, short fil, long ls);
void SetPlugins ();

static int dokey50 (void);
static int dokey5 (void);
static int dokey12 (void);
static int doliste (void);
static int KopfTexte (void);
static int FussTexte (void);

static char iformStapeldruck[30] ;

static char stat_defp_von [3] = {"1"};
static char stat_defp_bis [3] = {"3"};;
static char stat_deff_von [3] = {"1"};
static char stat_deff_bis [3] = {"4"};;
static char tou_def_von [10]  = {"1"};
static char tou_def_bis [10]  = {"9999"};
static char abt_def_von [6]   = {"0"};
static char abt_def_bis [6]   = {"9999"};
static char smt_def_von [6]  = {"0"};
static char smt_def_bis [6]  = {"99"};
static BOOL AbtRemoved = TRUE;
static BOOL SmtRemoved = TRUE;
static BOOL DrkKunRemoved = TRUE;
static BOOL LuKunRemoved = TRUE;
static BOOL KunDirect = FALSE;
static BOOL LsDirect = FALSE;
static long LsNr = 0l;
static BOOL MdnDirect = FALSE;
BOOL   NoMenue = FALSE;
short  Menuedirect = -1;
short  StartMdn = 0;

static BOOL NoClose = FALSE;

extern BOOL ColBorder;

static int  tou_def_zwang = 0;
static long  datum_plus = 0;
static BOOL telelist;
static long  ldatdiffminus = 0;
static long  ldatdiffplus = 365;
static BOOL  lsdirect = FALSE;
static BOOL  auftou = TRUE;
static BOOL gen_div_adr = FALSE;
static BOOL IsAng = FALSE;
static int  Startsize = 0;
static BOOL auf_art_active = FALSE;
static int bg_cc_par = 0;
static int zustell_par = 0;
static BOOL akt_preis = TRUE;
static BOOL KunLief = FALSE;
static BOOL KunLiefKz = FALSE;
static BOOL SearchKunDirect = FALSE;
static BOOL Color3D = TRUE;
static BOOL UpdatePersNam = TRUE;
static BOOL Wochentag = FALSE;
static BOOL KreditLimit = FALSE;
static BOOL UniqueKun = FALSE;
static BOOL TestUniqueKun = TRUE;
static BOOL UniqueKunError = FALSE;
static BOOL OpenPosMessage = FALSE;
static long UniqueKunLs = 0l;
static int TransactMax = 50;
static BOOL TransactMessage = FALSE;
static BOOL PartyKz_cfg = FALSE;	// 1301014
static int tcount = 0;
HACCEL MenAccelerators = NULL;
HANDLE  hMainInst;
HWND   hMainWindow;
HWND   mamain1;
BOOL ListeOK = FALSE;
int KompTestVk0 = 0;

HWND ForegroundWindow;

void GetForeground (void)
{
	ForegroundWindow = GetForegroundWindow ();
}

void SetForeground (void)
{
	SetForegroundWindow (ForegroundWindow);
}

extern HWND  ftasten;
extern HWND  ftasten2;

static int WithMenue   = 1;
static int WithToolBar = 1;

static   TEXTMETRIC tm;

static int PageView = 0;

static char *TXT_WORK   = "Bearbeiten";
static char *TXT_SHOW   = "Anzeigen";
static char *TXT_DEL    = "L�schen";
static char *TXT_PRINT  = "Drucken";

static char *TXT_ACTIVE = TXT_WORK;


static int IDM_ACTIVE = IDM_WORK;

static PAINTSTRUCT aktpaint;

HMENU hMenu;

// static int CallT1 =  850;
// static int CallT2 =  851;
// static int CallT3 =  852;

#define CallT1  850
#define CallT2  851
#define CallT3  852
#define KUNCC   853

struct PMENUE dateimen[] = {
	                        "&1 Bearbeiten", "G",  NULL, IDM_WORK, 
                            "&2 Anzeigen",   "G",  NULL, IDM_SHOW,
						    "&3 Drucken",    " ",  NULL, IDM_PRINT,
							"",              "S",  NULL, 0, 
	                        "B&eenden",      " ",  NULL, IDM_EXIT,

                             NULL, NULL, NULL, 0};


struct PMENUE bearbeiten[] = {
	                        "&Fax-Nummer      <SHIFT F8>",   "G", NULL, CallT1, 
	                        "&Telefon-Nummer  <SHIFT F9>",   "G", NULL, CallT2, 
	                        "&Preisgruppen    <SHIFT F10>",  "G", NULL, CallT3, 
							"Auftragsmenge editieren",     " ", NULL, IDM_ENTER_AUF_ME,
							"Ist-Auftragsmenge editieren <SHIFT F8>", "G", NULL, IDM_ENTER_LIEF_ME3,
							"",                            "S",   NULL, 0, 
	                        "A&bbruch F5",                 " ", NULL, KEY5, 
							"Spe&ichern F12",              " ", NULL, KEY12,
							"vorheriger Satz F2",          "G", NULL, IDM_PRIOR_ROW,
							"n�chster Satz F3",            "G", NULL, IDM_NEXT_ROW,
//							"",                            "S",   NULL, 0, 
//							"&Musterauftrag ",             " ", NULL, IDM_MUSTER,
							"",                            "S",   NULL, 0, 
//							"",                            "S",   NULL, 0, 
							"Neu bewerten ",               "G", NULL, IDM_NEWPR,
							"Nach Preis sortieren <Shift F9>", "G", NULL, IDM_SORTPR,
							"Nach Artikelnummer sortieren <Shift F9>", "G", NULL, IDM_SORTART,
							"Lieferscheine neu bewerten ", " ", NULL, IDM_ALLNEWPR,
							"",                            "S",   NULL, 0, 
							"Party&service ",              " ", NULL, IDM_PARTYSERVICE,
							"&voller Steuersatz", 
							                               " ", NULL, IDM_FULLTAX,
							"",                            "S", NULL, 0, 
	                        "&Anzahl Bestellartikel",      " ", NULL, IDM_ANZBEST, 
							"&Standortlager �ndern",       " ", NULL, IDM_LGR,
							"&Default Standortlager",      " ", NULL, IDM_LGRDEF,
							"",                            "S",   NULL, 0, 
	                        "&Suchen",                     "G", NULL, IDM_FIND, 
                            "&Positionstexte",             "G", NULL, IDM_POSTEXT, 
                             NULL,                         NULL, NULL, NULL, 
                             NULL,                         NULL, NULL, NULL, 
                             NULL,                         NULL, NULL, NULL, 
                             NULL,                         NULL, NULL, NULL, 
                             0};

struct PMENUE changeldpr = {"&Laden-Preis editieren  ALT L","G", NULL, IDM_LDPR}; 
struct PMENUE changevkpr = {"&Verkaufs-Preis editieren  ALT V","G", NULL, IDM_VKPR}; 
struct PMENUE kasse      = {"&Kasse ",  " ", NULL, IDM_KASSE}; 

struct PMENUE ansicht[] = {
//                            "&Lieferadresse",              " ", NULL, IDM_KUNLIEF,   
                            "&Basismenge",                 "G", NULL, IDM_BASIS, 
                            "&Fonteinstellung",            "G", NULL, IDM_FONT,
                            "Fehler&Meldungen",            " ",  NULL, IDM_ERROR,
                             NULL, NULL, NULL, 0};

struct PMENUE menuetab[] = {"&Datei",      "M", dateimen,   0, 
                            "&Bearbeiten", "M", bearbeiten, 0, 
                            "&Optionen",   "M", ansicht,    0, 
                            "&?",          " ", NULL,       IDM_VINFO, 
						     NULL, NULL, NULL, 0};

static int ActiveMark = IDM_FRAME;

static char InfoCaption [80] = {"\0"};


extern HWND hWndToolTip;
HWND hwndTB;
HWND hwndCombo1;
HWND hwndCombo2;

static TBBUTTON tbb[] =
{
 0,               IDM_WORK,   TBSTATE_ENABLED, 
                               TBSTYLE_BUTTON, 
 0, 0, 0, 0,
 1,               IDM_SHOW,   TBSTATE_ENABLED, 
	                          TBSTYLE_BUTTON,
 0, 0, 0, 0,
 2,               IDM_FIND,   TBSTATE_INDETERMINATE, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 3,               IDM_PRINT,  TBSTATE_ENABLED, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED,
                  TBSTYLE_SEP, 
 0, 0, 0, 0,
 /*
 4,               IDM_LIST,   TBSTATE_ENABLED | TBSTATE_CHECKED, 
                  TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 */

/*
 5,               IDM_STD,TBSTATE_INDETERMINATE, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,

 5,               IDM_PAGE, TBSTATE_INDETERMINATE, 
                  TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 */
 6,               IDM_INFO,   TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 
 7,               KEYTAB, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 8,               KEYSTAB, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 9,               KEYDOWN, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
10 ,               KEYUP, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
11 ,              IDM_LGR, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
12 ,              IDM_LGRDEF, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
};

static char *Version[] = {"  Lieferscheinbearbeitung  ",
                          "  Programm    53100", 
                          "  SETEC            ", 
						  "  Versions-Nr 2.02 31.08.2012",
						   NULL,
};

HWND    QuikInfo = NULL;

void     CreateQuikInfos (void);
static int CallTele1 ();
static int CallTele2 ();
static int CallTele3 ();


static int MoveMain = FALSE;
static int cfgccmarkt = 1;
static int kun_sperr = 1;
static int tour_zwang = 0;
static int tour_datum = 1;
static int abw_komm = -1;
static int fen_par = 0;
static int lutz_mdn_par = 0;
static BOOL fil_lief_par = FALSE;
static long tour_div = 100;
static long akt_lager = 0;
static BOOL vertr_abr_par = TRUE; 
static BOOL vertr_kun = TRUE;
static BOOL tou_wo_tou = FALSE;
static BOOL tou_par = TRUE;
static BOOL wo_tou_par = TRUE;
static BOOL use_kopf_txt = FALSE;
static int KomplettMode = 0;
static HWND DlgWindow = NULL;
static BOOL autosysdat = FALSE;
static BOOL autokunfil = FALSE;
static int listedirect = 0;
static BOOL PosOK = FALSE;
static BOOL LdVkEnabled = FALSE;
static BOOL PrVkEnabled = FALSE;
static char *ClipDial = NULL;
static char *DdeDialProg = NULL;
static BOOL DdeDialActive = TRUE;
static char *DdeDialService = NULL;
static char *DdeDialTopic = NULL;
static char *DdeDialItem = NULL;
static HANDLE DialPid;
static BOOL EnableKunDirect = TRUE;
static BOOL FixDat = FALSE;
static BOOL FitParams = FALSE;

BOOL StapelTour = FALSE; /* 26.03.12 GK */

BOOL IsPartyService = FALSE;
short PartyKz = 0;

HICON telefon1;
HICON fax;

HBITMAP btelefon;
HBITMAP btelefoni;

ColButton CTelefon = { 
//                      "Telefon", -1, -1,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
//                       btelefon, 6, 8,
                       btelefon, -1, -1,
                       NULL, 0, 0,
                       RGB (255, 255, 255),
//                       BLUECOL,
                       GRAYCOL,
                       -1};

ColButton CFax =    { "Fax", -1, -1,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (255, 255, 255),
//                     BLUECOL,
                       GRAYCOL,
                       -1};
ColButton CGruppen =    { "PG", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
//                        BLUECOL,
                          GRAYCOL,
                          -1};
/*
static ITEM itelefon1 ("", (char *) &telefon1, "", 0);
static ITEM ifax      ("", (char *) &fax,      "", 0);



static field _buform [] = {
&ifax,             4, 2, 2,73, 0, "", ICON| BORDERED, 0, 0,
                                                           CallT1,
&itelefon1,        4, 2, 4,73, 0, "", ICON | BORDERED, 0, 0,
                                                           CallT2,
};
*/



static mfont BuFont = {
                      "Courier New", 
                       120, 
                       100, 
                       0, 
                       RGB (-1, 0, 0), 
                       RGB (-1, 0, 0),
                       0, 0};

static BOOL temode = 0;
static ITEM itelefon1 ("", (char *) &CTelefon, "", 0);
static ITEM ifax      ("", (char *) &CFax,     "", 0);
static ITEM iGruppen  ("", (char *) &CGruppen, "", 0);

static ITEM iOK      ("", "    OK     ", "", 0);
static ITEM iCancel  ("", " Abbrechen ", "", 0);

static field _buform [] = {
&ifax,             4, 2, 2,73, 0, "", COLBUTTON, 0, 0,
                                                           CallT1,
&itelefon1,        4, 2, 4,73, 0, "", COLBUTTON, 0, 0,
                                                           CallT2,
&iGruppen,         4, 2, 6,73, 0, "", COLBUTTON, 0, 0,
                                                           CallT3,
};

static form buform = {3, 0, 0, _buform, 0, 0, 0, 0, &BuFont};



static char *qInfo [] = {"Neue Datei �ffnen",
                         "Tablle �ffnen",
                         "Query zu Tabelle",
                         "Drucken",
                         "Funktion f�r aktives Eingabefeld",
                         "S�tze in Listenform anzeigen",
                         "Standardauftrag",
                         "Lager f�r Standort �ndern",
						 "Default-Lager f�r Standort setzen",
                          0, 
                          0};

static UINT qIdfrom [] = {IDM_WORK, IDM_SHOW, IDM_DEL,
                         IDM_PRINT, IDM_INFO,
                         IDM_LIST, IDM_STD,IDM_LGR, IDM_LGRDEF, 0, 0};

static char *telenr =  "Telefon-Nummer";
static char *telelst = "Telefon-Liste";
static int telepos = 3;
static BOOL telekun = FALSE;

static char *qhWndInfo [] = {"Tabellen-Linien festlegen", 
                             "Farbe f�r Vordergrund und Hintergrund "
                             "festlegen",
                             "Fax-Nummer",
                             telenr,
                             "Preisgruppen",
                             "Abbrechen",
                             "Speichern",
                             0};

static HWND *qhWndFrom [] = {&hwndCombo1, 
                             &hwndCombo2,
                             &buform.mask[0].feldid,
                             &buform.mask[1].feldid,
                             &buform.mask[2].feldid,
                             NULL,
                             NULL,
                             NULL}; 

void SetTeleNr (void)
{
	      qhWndInfo[telepos] = telenr;
}

void SetTeleLst (void)
{
	      qhWndInfo[telepos] = telelst;
}


static char *Combo1 [] = {"wei�e Tabelle" ,
                          "hellgraue Tabelle",
                          "schwarze Tabelle",
                          "graue Tabelle",
                          "vertikal wei�",
                          "vertikal hellgrau",
                          "vertikal schwarz", 
                          "vertikal grau",
                          "horizontal wei�",
                          "horizontal hellgrau",
                          "horizontal schwarz", 
                          "horizontal grau",
                          "keine Linien",
                           NULL};

static char *Combo2 [] = {"wei�er Hintergrund" ,
                          "blauer Hintergrund",
                          "schwarzer Hintergrund",
                          "grauer Hintergrund",
						  "hellgrauer Hintergrund",
                           NULL};

static COLORREF Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL,
							BLACKCOL,
};

static COLORREF BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL,
							LTGRAYCOL,
};

static COLORREF MessBkCol = DKYELLOWCOL; 
static COLORREF MessCol   = BLACKCOL; 

static int combo1pos = 0;
static int combo2pos = 0;

mfont lFont = {
               "Courier New", 
               120, 
               100, 
               0, 
               RGB (-1, 0, 0), 
               RGB (-1, 0, 0),
               0, 0};

static int m3Size = 0;

extern LS_CLASS ls_class;
static WA_PREISE WaPreis;
static KUN_CLASS kun_class;
static AUFPLIST lspListe;
static MENUE_CLASS menue_class;
static FIL_CLASS fil_class;
static MDN_CLASS mdn_class;
static ADR_CLASS adr_class;
static VERTR_CLASS vertr_class;
static QueryClass QClass;
static TOU_CLASS Tour;
static QueryTou QTClass;
static QueryVertr QVtrClass;
static KompAuf AufKompl;
static LastAuf AufLast;
static LSTXTLIST lstxtlist;
static DB_CLASS DbClass;
static AUTO_CLASS AutoClass;
static PROG_CFG ProgCfg ("53100");
static LgrClass LgrClass;
static LeergClass LeergClass;
static PTAB_CLASS ptab_class;
static KINFO Kinfo;
static VINFO Vinfo;
static FITL Fitl;
static CSmtHandler smtHandler;

extern MELDUNG Mess;
extern SYS_PAR_CLASS sys_par_class;



static char mdn [10];
static char fil [10];

static BOOL FreeAufNr = TRUE;

/* ****************************************************************/
/* Auftragskopf    
                                              */
static int mench = 0;

static BOOL ldat_ok = FALSE;
static long akt_auf = 0l;
static long akt_tou = 0l;
static long akt_kun = 0l;
static long adr_nr = 0;
static long LskChoiceNr = 0l;

char akt_ldat[12];

long LiefAdr = 0l;
char lieferschein[9] = "0";
char auftrag[9];
char kunfil[2];
char kuns[9];
char kunliefs[9];
char ldat[12] = {" "};
char kdat[12];
char fix_dat[12];
char lzeit[7];
char ls_status[2];
char ls_stat_txt[40];
char kd_auftrag[31];
char k_kurzb[17];
char tele[26];
char vertrs [9];
char vertr_krz[17];
char tour[10];
char tour_krz[50];
char htext[51];
char auf_art [5];
char auf_art_txt [20];

static int dsqlstatus;
static int inDaten = 0;
static HBITMAP ArrDown;
static BOOL ScrollChoice = FALSE;


static int leseauf (void);
static int lese_auftr (void);
static int leseaufcc (long);
static int lesekun (void);
static int setkey9kun (void);
static int testtou (int);
static int testvertr (void);
static int saveldat (void);
static int testldat (void);
static int testtou0 (void);
static int testvertr0 (void);
static int setkey9tou (void);
static int setkey9vertr (void);
static int setkey9aufart (void);
static int setkey10_11 (void);
static int testhtext (void);
static int SwitchArt (void);
static int testkommdat (void);
static int testfixdat (void);
static int getaufart (void);
static int testaufart0 (void);
static int testaufart (void);
static int QueryAuf (void);
static int AufEnter (void);
static BOOL ldatdiffOK (void);
static BOOL KommdatOk (void);
static void LskChoice ();
static void DestroyChoiceLsk ();


static ColButton CMuster   = { "Muster",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 50, 3,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
                             NOCOLPRESS | NOCOLBORDER,
//                             0,
};
static ColButton CParty   = { "Partyservice   ",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 80, 3,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
//                             NOCOLPRESS | NOCOLBORDER,
                             NOCOLPRESS,
//                             0,
};


static ColButton CFirstRow= {NULL,    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
                             NOCOLPRESS | NOCOLBORDER,
};

static ColButton CPriorRow= {NULL,    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
                             NOCOLPRESS | NOCOLBORDER,
};

static ColButton CNextRow= {NULL,    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
                             NOCOLPRESS | NOCOLBORDER,
};

static ColButton CLastRow= {NULL,    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, -1, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
                             NOCOLPRESS | NOCOLBORDER,
};

HBITMAP SelBmp = NULL;

CFIELD *MusterButton =  new CFIELD ("muster", (ColButton *) &CMuster, 12, 0, 23, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_MUSTER, 0, 0, TRANSPARENT);

CFIELD *PartyButton =  new CFIELD ("party", (ColButton *) &CParty, 14, 0, 21, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_PARTYSERVICE, 0, 0, TRANSPARENT);

static mfont partyfont = {
                       FontName, 
                       120, 
                       180, 
                       FontAttribute, 
                       REDCOL, 
                       LTGRAYCOL,
                       0, 0};



static ITEM iParty ("party", " volle MWST ", "", 0);

static field _partyf [] = {
&iParty,          0, 0, 0, 53, 0,  "", DISPLAYONLY,       0, 0, 0,
};

static form partyf = {1, 0, 0, _partyf, 0, 0, 0, 0, &partyfont};



static mfont musterfont = {
                       FontName, 
                       FontHeight, 
                       FontWidth, 
                       FontAttribute, 
                       BLUECOL, 
                       WHITECOL,
                       0, 0};




static int Size = 100;

static mfont rowbuttonfont = {"MS SANS SERIF", Size, 0, 0,
                           RGB (0, 255, 255),
                           0,
                           NULL};

static CFIELD *_fRowButtons[20] = {
                     new CFIELD ("FirstRow", (ColButton *) &CFirstRow,  3, 0, 10, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_FIRST_ROW, &rowbuttonfont, 0, TRANSPARENT),
                     new CFIELD ("PriorRow", (ColButton *) &CPriorRow,  3, 0, 13, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F2, &rowbuttonfont, 0, TRANSPARENT),
                     new CFIELD ("NextRow", (ColButton *) &CNextRow,  3, 0, 16, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F3, &rowbuttonfont, 0, TRANSPARENT),
                     new CFIELD ("LastRow", (ColButton *) &CLastRow,  3, 0, 19, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_LAST_ROW, &rowbuttonfont, 0, TRANSPARENT),
                     NULL,
};

static CFORM fRowButtons (4, _fRowButtons);


static ITEM iMuster ("muster", " Musterauftrag ", "", 0);

static field _musterf [] = {
&iMuster,          0, 0, 0, 50, 0,  "", DISPLAYONLY,       0, 0, 0,
};

static form musterf = {1, 0, 0, _musterf, 0, 0, 0, 0, &musterfont};


static ITEM iauftrag ("auf", lieferschein,     "Lieferschein  ", 0); 
static ITEM ikunfil ("kun_fil",    kunfil,"Kunde/Filiale ", 0); 
static ITEM ikunfil_bz 
                    ("",           "Kunde", "", 0); 
static ITEM iarrdown ("arrdown",  " < ",     "", 0);
static ITEM iauftr
                    ("auftrag", auftrag, "Auftragsnr.", 0); 

static field _aufformk [] = {
&iauftrag,                 9, 0, 2, 1, 0,  "%8d", EDIT,       0, leseauf, 0,
&iarrdown,                 2, 0, 2,25, 0, "B",    BUTTON,     0, 0, QUERYAUF, 
&ikunfil,                  2, 0, 3, 1, 0, "%1d", READONLY,   0, 0,0,
&ikunfil_bz,              10, 0, 3,19, 0, "",    DISPLAYONLY,0, 0,0,
&iauftr,                   9, 0, 2,45, 0, "%8d",    EDIT,      0, lese_auftr,0,
};

static form aufformk = {5, 0, 0, _aufformk, 0, 0, 0, 0, NULL};

static ITEM ikunstxt   ("kun_txt",  "Kundennummer  ",  "", 0); 
static ITEM ikunlstxt  ("kun_txt",  "Lieferadresse ",  "", 0); 
// static ITEM ikuns      ("kun",      kuns,              "", 0); 
static ITEM ikunliefs  ("kun",      kunliefs,          "", 0); 


static ITEM ikuns      ("kun",      kuns,       "Kundennummer  ", 0); 
// static ITEM ikunliefs  ("kun",      kunliefs,   "Lieferadresse ", 0); 


static ITEM ildat   ("lieferdat",  ldat,  "Lieferdatum   ", 0);
static ITEM ilzeit  ("lieferzeit", lzeit, "Lieferzeit    ", 0);
static ITEM ils_status
                    ("ls_status", ls_status, "LS -Status ", 0);
static ITEM ils_stat_txt
                    ("ls_stat_txt", ls_stat_txt, "", 0);
// static ITEM ik_kurzb("kun_krz",    k_kurzb,    "K.-Kurzb.  ", 0);
static ITEM ik_kurzb("kun_krz",    k_kurzb,    "", 0);
static ITEM itele   ("tele",       _adr.tel,   "TelNr ", 0);
static ITEM iauf_art   ("auf_art", auf_art,    "A.Art ", 0);
static ITEM iauf_art_txt ("auf_art_txt", auf_art_txt,    "", 0);

static ITEM ivertrs ("vertr",      vertrs,     "Vertr-Nr ",   0);
static ITEM ivertr_krz
                    ("vertr_krz",  vertr_krz,  "",   0);   
static ITEM itour   ("tou",        tour,       "Tour-Nr ",    0);
static ITEM itour_krz
                    ("tou_bz",     tour_krz,   "     ",       0);
static ITEM ihtext  ("htext",      htext,  "Hinweistext   ", 0); 
static ITEM ikd_auftrag
                    ("kd_auftrag", kd_auftrag, "Ext.Auftrag", 0); 


static field _aufform [] = {
//&ikunfil,                    2, 0, 3,1, 0, "%1d", NORMAL,   0, testkeys,0,
// &ikuns,                      9, 0, 4,1, 0, "%8d", NORMAL,   setkey9kun, 
//&ikunstxt,                   0, 0, 4, 1, 0, "", DISPLAYONLY,   0, 0, 0, 
&ikuns,                      9, 0, 4,1, 0, "", NORMAL,   setkey9kun, 
                                                          lesekun, 0,

&iarrdown,                   2, 0, 4,25, 0, "B",    BUTTON,     0, 0, QUERYKUN, 
&ildat,                     11, 0, 6,1, 0, "dd.mm.yyyy", NORMAL,
                                                            saveldat, testldat,0,
&ilzeit,                     6, 0, 7,1, 0, "", NORMAL,      0, testkeys,0,
&ihtext,                    48, 0, 8,1, 0, "", NORMAL,      setkey10_11, testhtext,0,
&ils_status,                2, 0, 4,45, 0, "", READONLY,      0, testkeys,0,
&ils_stat_txt,             13, 0, 4,60, 0, "", READONLY, 0, 0, 0, 
&ikd_auftrag,               30, 0, 3,45, 0, "", EDIT, 0, testkeys,0,
//&ik_kurzb,                  16, 0, 4,40, 0, "", READONLY, 0, testkeys,0,
&ik_kurzb,                  16, 0, 4,27, 0, "", READONLY, 0, testkeys,0,
&itele,                     25, 0, 2,70, 0, "", READONLY, 0, 0,0,
&iauf_art,                   3, 0, 4,45, 0, "", REMOVED, setkey9aufart, testaufart,0,
&iauf_art_txt,              16, 0, 4,55, 0, "", REMOVED, 0,  0,0,
&ivertrs,                    5, 0, 6,32, 0, "%4d",NORMAL,    setkey9vertr, testvertr0,0,
&itour,                      9, 0, 6,49, 0, "%8d", NORMAL,   setkey9tou, testtou0,0,
&iarrdown,                   2, 0, 6,67, 0, "B",    BUTTON,     0, 0, QUERYTOU, 
&ivertr_krz,                16, 0, 7,32, 0, "", READONLY,    0, testkeys,0,
&itour_krz,                 16, 0, 7,50, 0, "", READONLY,    0, testkeys,0
};

static form aufform = {17, 0, 0, _aufform, 0, 0, 0, 0, NULL};


static int kunfield = 1;
static int toufield = 9;

static char *aktart = kunart;
static char *aktzei = a_bz2_on;
static int NewRec = 0;

static int  anzart = 150;
static char AnzBest [10];



static mfont buttonfont = {"MS SANS SERIF", 115, 0, 0,
                           RGB (0, 255, 255),
                           0,
                           NULL};


ColButton Cfrei_txt2 = { 
//	                  "Achtung !!",  -1,  1,
//                      "Kundeninfo",  -1,  2,
	                  "Info <F3>",  40,  -1,
//                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
//                       btelefon, -1, 10,
                       btelefon, 5, -1,
                       NULL, 0, 0,
                       REDCOL,
                       LTGRAYCOL,
                       0};

CFIELD *_ffrei_txt2 [] = {
                     new CFIELD ("frei_txt2", (ColButton *) &Cfrei_txt2,  
						         10, 0, 50, 8,  NULL, "", 
                                 CCOLBUTTON,
                                 VK_F11, &buttonfont, 0, TRANSPARENT),
                     NULL
};

CFORM ffrei_txt2 (1, _ffrei_txt2);


//static ITEM ifreitxt2 ("ifreitxt2",  " Kundeninfo ",     "", 0);
static ITEM ifreitxt2 ("ifreitxt2",  (char *) &Cfrei_txt2 ,  "", 0);

static field _Ffrei_txt2_bu [] = {
&ifreitxt2,              12, 2, 11, 73, 0, "", COLBUTTON,  0, 0, KEY3
};

static form Ffrei_txt2_bu = {1, 0, 0, _Ffrei_txt2_bu, 0, 0, 0, 0, NULL};

static int TestDivAdr = 1;

// Werte f�r Adressbereiche  

static long adr_von  = 90000000;
static long adr_bis  = 99999999;

// Werte f�r gesch�tzte Adressbereiche  

static long sadr_von = 90000000;
static long sadr_bis = 99999999;
static void fillsadr (void);

static int testanzb (void)
{
	if (syskey == KEY5 || syskey == KEY12)
	{
		break_enter ();
	}

	if (atoi (AnzBest) > 200)
	{
		strcpy (AnzBest, "200");
	}
	else if (atoi (AnzBest) < 5)
	{
		strcpy (AnzBest, "5");
	}
	return 0;
}

static ITEM iAnzBestT ("anzbest",  "Anzahl Bestellartikel", "", 0);
static ITEM iAnzBest ("anzbest",   AnzBest, "", 0);

// static ITEM iAnzBest ("anzbest",   AnzBest, "Anzahl Bestellartikel", 0);

static field _fAnzBest [] = {
    &iAnzBestT,  22, 0, 1, 1, 0, "", DISPLAYONLY, 0, 0, 0,
    &iAnzBest,    7, 0, 1,24, 0, "%4d", EDIT | UPDOWN, 0, testanzb, 0,
    &iOK,          12, 0, 3, 5, 0, "",  BUTTON,   0,0, KEY12,
    &iCancel,      12, 0, 3,19, 0, "",  BUTTON,   0,0, KEY5};

static form fAnzBest = {4,0, 0, _fAnzBest, 0, 0, 0, 0, NULL};

static struct UDTAB udtab[] = {100, 5, 10};
static int udanz = 1;

void SetAnzBest (void)
/**
String suchen.
**/
{
        HWND hAnzBest;
        int end_break;
        int currents;

        udtab[0].Pos = anzart;
        currents = currentfield;
        end_break = GetBreakEnd ();
        break_end ();
        sprintf (AnzBest, "%d", anzart);
        EnableWindows (mamain1, FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_DLGFRAME);
        hAnzBest = OpenWindowChC (5,36,10, 20, hMainInst,
                   "");
        SetUpDownTab (udtab , udanz);
        enter_form (hAnzBest, &fAnzBest, 0, 0);
		if (syskey != KEY5)
		{
                 anzart = atoi (AnzBest);
		}
        EnableWindows (mamain1, TRUE);
        DestroyWindow (hAnzBest);
        SetBreakEnd (end_break);
        SetAktivWindow (mamain1);
        SetActiveWindow (mamain1);
        currentfield = currents;
        SetCurrentFocus (currentfield);
}


int CallTele1 (void)
/**
Waehlen ausfuehren.
**/
{
/*
        clipped (_adr.fax);
        if (strcmp (_adr.fax, " ") <= 0)
        {
                  disp_mess ("Keine Fax-Nummer", 0);
        }
        else
        {
                  print_mess (0, "Fax Nummer %s", _adr.fax);
        }
*/
        if (atol (kuns) != 0l)
        {
                  FaxAuf ();
                  DestroyWindow (QuikInfo);
                  CreateQuikInfos ();
        }
        return 0;
}

BOOL IsC_C (void)
/**
Test, ob C+C gesetzt ist. Dann ist die Tour-Nr immer 0.
**/
{
	     static int C_C = 0;
		 static BOOL DefOK = FALSE;
         char cfg_v [20];

		 if (DefOK == FALSE)
		 {
                    if (ProgCfg.GetGroupDefault ("C+C", cfg_v))
					{
					       if (atoi (cfg_v))
						   {
						          C_C = atoi (cfg_v);
						   }
					}
                    if (ProgCfg.GetCfgValue ("C+C", cfg_v) == TRUE)
					{
				           C_C = atoi (cfg_v);
					}
					DefOK = TRUE;
		 }
         if (C_C)
		 {
			 tou_par = FALSE;
		 }
		 return C_C;
}


int CallTele2 (void)
/**
Waehlen ausfuehren.
**/
{
        clipped (_adr.tel);
		if (temode == 0)
		{
                  TelefonList ();
//                    ShowKuncc ();
		}
        else if (strcmp (_adr.tel, " ") <= 0)
        {
                  disp_mess ("Keine Telefon-Nummer", 0);
        }
        else
        {
                  if (ClipDial != NULL)
                  {
                      RunClipDial (_adr.tel);
                  }
                  else if (DdeDialProg != NULL)
                  {
                      RunDdeDial (_adr.tel);
                  }
                  else
                  {
                      print_mess (0, "Telefon Nummer %s", _adr.tel);
                  }
        }
        CreateQuikInfos ();
        return 0;
}

int CallTele3 (void)
/**
Waehlen ausfuehren.
**/
{
        if (atol (kuns) != 0l)
        {
	         Kinfo.InfoKunF (hMainInst, hMainWindow, lsk.mdn, atol (kuns), kun.kun_bran2);
             CreateQuikInfos ();
        }
        return 0;
}

int InfoVersion (void)
/**
Waehlen ausfuehren.
**/
{
	    Vinfo.VInfoF (hMainInst, hMainWindow, Version);
        return 0;
}

void CreateQuikInfos (void)
/**
QuikInfos generieren.
**/
{
     BOOL bSuccess ;
     TOOLINFO ti ;

     if (QuikInfo) DestroyWindow (QuikInfo);
     QuikInfo = CreateWindow (TOOLTIPS_CLASS,
                                "",
                                WS_POPUP,
                                0, 0,
                                0, 0,
                                hMainWindow,
                                NULL,
                                hMainInst,
                                NULL);


     ZeroMemory (&ti, sizeof (TOOLINFO)) ;
     ti.cbSize = sizeof (TOOLINFO) ;
     ti.uFlags = TTF_IDISHWND | TTF_CENTERTIP | TTF_SUBCLASS ;
     ti.hwnd   = mamain1 ;
     ti.uId    = (UINT) (HWND) buform.mask[0].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     ti.uId    = (UINT) (HWND) buform.mask[1].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     ti.uId    = (UINT) (HWND) buform.mask[2].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     SetFktQuikInfo (QuikInfo, &ti);
}


int InfoKun (void)
{
        char where [80];

        if (atoi (kunfil) == 0)
        {
              if (atol (kuns))
              {
                        sprintf (where, "where kun = %ld", atol (kuns));
              }
              else
              {
                        sprintf (where, "where kun > 0");
              }
              _CallInfoEx (hMainWindow, NULL,
							      "kun", 
								  where, 0l);
        }
        else
        {
              if (atol (kuns))
              {
                        sprintf (where, "where mdn = %hd and fil = %ld", lsk.mdn, atol (kuns));
              }
              else
              {
                        sprintf (where, "where mdn = %hd and fil > 0", lsk.mdn);
              }
              _CallInfoEx (hMainWindow, NULL,
							      "fil", 
								  where, 0l);
        }
        return 0;
}

int InfoTou (void)
{
        char where [80];
        long tou;

        if (atol (tour))
        {
            tou = atol (tour) / tour_div;
            sprintf (where, "where tou = %ld", tou);
        }
        else
        {
            sprintf (where, "where tou > 0");
        }
        _CallInfoEx (hMainWindow, NULL,
							      "tou", 
								  where, 0l);
        return 0;
}

int InfoVertr (void)
{
        char where [80];
        long vertr;

        if (atol (vertrs))
        {
            vertr= atol (vertrs);
            sprintf (where, "where vertr = %ld", vertr);
        }
        else
        {
            sprintf (where, "where vertr > 0");
        }
        _CallInfoEx (hMainWindow, NULL,
							      "vertr", 
								  where, 0l);
        return 0;
}


BOOL Lock_Lsk (void)
/**
Auftrag sperren.
**/
{
    short sqlm;
    extern short sql_mode;

    sqlm = sql_mode;
    sql_mode = 1;

	lsk.delstatus = -1;

	dsqlstatus = ls_class.update_lsk (lsk.mdn, lsk.fil, lsk.ls);

	lsk.delstatus = 0;
    if (dsqlstatus < 0)
    {
	    print_mess (2, "Lieferschein %ld wird im Moment bearbeitet", lsk.ls);
        sql_mode = sqlm;
        return FALSE;
    }
    sql_mode = sqlm;
    return TRUE;
}


void GetAufstatTxt (char *aufstxt)
/**
Klartext fuer Auftragsstatus holen.
**/
{
	  char wert [5];

	  sprintf (wert, "%hd", lsk.ls_stat);
	  ptabn.ptbez[0] = 0;
      ptab_class.lese_ptab ("ls_status", wert);
	  strcpy (aufstxt, ptabn.ptbez);
}

static void SetKunAttr (void)
/**
Atribut auf Feld Kunde setzen.
**/
{
	   int kunarrowpos;

	   kunarrowpos = GetItemPos (&aufform, "kun") + 1;

	   if (kun_sperr == FALSE) 
	   {
			if (atol (kuns) == 0l)
			{
	            SetItemAttr (&aufform, "kun", EDIT);
				aufform.mask[kunarrowpos].attribut = BUTTON;
			}
		    return;
	   }

       if (atol (kuns) == 0l)
	   {
	            SetItemAttr (&aufform, "kun", EDIT);
				aufform.mask[kunarrowpos].attribut = BUTTON;
	   }
	   else if (EnableKunDirect == FALSE && lspListe.GetPosanz () == 0)
	   {
	            SetItemAttr (&aufform, "kun", EDIT);
				aufform.mask[kunarrowpos].attribut = BUTTON;
	   }
	   else
	   {
	            SetItemAttr (&aufform, "kun", READONLY);
				aufform.mask[kunarrowpos].attribut = REMOVED;
	   }
}


int lese_kun_mdn (long kun)
/**
Kunde ohne Mandant lesen. Mandant wird vom Kunden geholt.
**/
{
	   int dsqlstatus;
	   DbClass.sqlin ((long *) &kun, 2, 0);
	   DbClass.sqlout ((char *) mdn, 0, 5);
	   DbClass.sqlout ((char *) fil, 0, 5);
       dsqlstatus = DbClass.sqlcomm ("select mdn,fil from kun where kun = ?");
	   if (dsqlstatus == 0)
	   {
		   lsk.mdn = atoi (mdn);
		   lsk.fil = atoi (fil);
 	       DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
	       DbClass.sqlin ((short *) &lsk.fil, 1, 0);
	       DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
	       DbClass.sqlcomm ("update lsk set mdn = ?, fil = ? where ls = ?");
		   mdn_class.lese_mdn (lsk.mdn);
	   }
	   return dsqlstatus;
}

int lese_fil_mdn (long kun)
/**
Kunde ohne Mandant lesen. Mandant wird vom Kunden geholt.
**/
{
	   int dsqlstatus;
	   short nfil;

	   DbClass.sqlin ((long *) &kun, 2, 0);
	   DbClass.sqlout ((char *) mdn, 0, 5);
	   DbClass.sqlout ((char *) kuns, 0, 9);
       dsqlstatus = DbClass.sqlcomm ("select mdn,fil from fil where kun = ?");

	   if (dsqlstatus == 100)
	   {
		        nfil = (short) kun;
 	            DbClass.sqlin ((long *) &nfil, 1, 0);
	            DbClass.sqlout ((char *) mdn, 0, 5);
	            DbClass.sqlout ((char *) kuns, 0, 9);
                dsqlstatus = DbClass.sqlcomm ("select mdn,fil from fil where fil = ?");
	   }


	   if (dsqlstatus == 0)
	   {
		   lsk.mdn = atoi (mdn);
		   lsk.fil = 0;
		   sprintf (fil, "%hd", 0);
		   lsk.kun = atol (kuns);
 		   sprintf (kuns, "%8ld", atol (kuns));
	       DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
	       DbClass.sqlin ((short *) &lsk.fil, 1, 0);
	       DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
	       DbClass.sqlcomm ("update lsk set mdn = ?, fil = ? where ls = ?");
	   }

	   return dsqlstatus;
}


static void EnableKun (BOOL b)
{

  	    int kunpos = GetItemPos (&aufform, "kun");

        CloseControl (&aufform, kunpos);
        CloseControl (&aufform, kunpos + 1);
        if (b == FALSE)
        {
	            SetItemAttr (&aufform, "kun", READONLY);
				aufform.mask[kunpos + 1].attribut = REMOVED;
                display_field (mamain1, &aufform.mask[kunpos], 0, 0);
        }
        else
        {
	            SetItemAttr (&aufform, "kun", EDIT);
				aufform.mask[kunpos + 1].attribut = BUTTON;
                display_field (mamain1, &aufform.mask[kunpos], 0, 0);
                display_field (mamain1, &aufform.mask[kunpos + 1], 0, 0);
        }
}
 
	       
static void AufToForm (void)
{
/**
Auftragsdaten in Form uebertragen.
**/
          sprintf (kunfil, "%1d", lsk.kun_fil);
          if (atoi (kunfil) == 0)
          {
                    ikunfil_bz.SetFeldPtr ("Kunde");
          }
          else
          {
                    ikunfil_bz.SetFeldPtr ("Filiale");
          }
          display_field (mamain1, &aufformk.mask[1], 0, 0);
          display_field (mamain1, &aufformk.mask[2], 0, 0);


          sprintf (kuns,   "%8ld", lsk.kun);
          if (lsk.kun_fil == 0)
          {
		          dsqlstatus = 0;
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_kun_mdn (atol (kuns));
				  }
				  if (dsqlstatus == 0)
				  {

// Kunde immer mit Filiale 0 lesen

                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                         0,
                                                         atol (kuns));
				  }
				  if (dsqlstatus == 100 && autokunfil)
				  {
					   lsk.kun_fil = 1;
					   strcpy (kunfil, "1");
                       ikunfil_bz.SetFeldPtr ("Filiale");
                       display_form (mamain1, &aufformk, 0, 0);
				  }
				  else
				  {
                       if (dsqlstatus == 0)
					   {
                         smtHandler.set_Kun (&kun);
                         if (KunLiefKz && LiefAdr > 0l)
                         {
                                 kun.adr2 = LiefAdr;
                         }
                         dsqlstatus = adr_class.lese_adr (kun.adr2);
						 lsk.waehrung = kun.waehrung;
					   }
                       strcpy (k_kurzb, kun.kun_krz2);
				       if ((lsk.kopf_txt == 0) && use_kopf_txt && kun.ls_kopf_txt)
					   {
					         lsk.kopf_txt = kun.ls_kopf_txt;
					   }
				   }

          }
          if (lsk.kun_fil == 1)
          {
		          dsqlstatus = 0;
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_fil_mdn (atol (kuns));
				  }
				  if (dsqlstatus == 0)
				  {
                       dsqlstatus = fil_class.lese_fil (atoi (mdn),
                                                    atoi (kuns));
                       if (dsqlstatus == 0)
					   {
                                  strcpy (k_kurzb, _adr.adr_krz);
					              lsk.waehrung = atoi (_mdn.waehr_prim);
					   }
				  }
          }
          clipped (_adr.tel);
//		  strcpy (tele, _adr.tel);

          if (strcmp (_adr.tel, " ") > 0)
          {
                   CTelefon.bmp = btelefon;
                   ActivateColButton (mamain1, 
                                      &buform, 1, 0, 1);
                   EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
          }

          clipped (_adr.fax);
          if (strcmp (_adr.fax, " ") > 0)
          {
                   ActivateColButton (mamain1, 
                                      &buform,      0, 0, 1);
                   EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
          }
          ActivateColButton (mamain1, &buform, 2, 0, 1);
          EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
          MoveButtons ();


          dlong_to_asc (lsk.lieferdat, ldat);
          dlong_to_asc (lsk.komm_dat, kdat);
          dlong_to_asc (lsk.fix_dat, fix_dat);
          strcpy (lzeit, lsk.lieferzeit);
          sprintf (ls_status, "%1d", lsk.ls_stat);
		  GetAufstatTxt (ls_stat_txt);
//		  itoa (lsk.auf , kd_auftrag, 16);
		  sprintf (auftrag, "%ld", lsk.auf);
		  sprintf (kd_auftrag, "%s", lsk.auf_ext);
          if (lsk.vertr < 0l)
          {
              lsk.vertr = 0l;
          }
          sprintf (vertrs, "%8ld", lsk.vertr);
          dsqlstatus = vertr_class.lese_vertr (atoi (mdn),
                                               atoi (fil),
                                               lsk.vertr);
          if (dsqlstatus == 0)
          {
                         strcpy (vertr_krz, _adr.adr_krz);
          }
          else
          {
                         strcpy (vertr_krz, " ");
          }
          if (lsk.tou < 0l)
          {
              lsk.tou = 0l;
          }
          sprintf (tour, "%8ld", lsk.tou);
          lesetou ();
          strcpy (htext, lsk.hinweis);
          sprintf (auf_art, "%hd", lsk.auf_art);
		  if (auf_art_active)
		  {
			  getaufart ();
		  }

/* das ist relativ boese ---- 240512  >>>>>>
    //testtest      if (lsk.auf_art == 99)
     //     {
              SetMuster (TRUE);
      //    }
< ----------- */
		if (lsk.auf_art == 99)	// nicht immer gnadenlos setzen !!?? 240512
		{
              SetMuster (TRUE);
        }

}


static double TimeToDec (LPSTR timestr, double max)
{
	double dec_time = 0.0;


	Text Time = timestr;
	int pos;
	if ((pos = Time.Find (":")) != -1)
	{
		Text Hour = Time.SubString (0, pos);
		Text Min  = Time.SubString (pos + 1, Time.GetLen () - (pos + 1));
		dec_time = ratod (Hour.GetBuffer ());
		double min_dec = ratod (Min.GetBuffer ());
//		min_dec /= 60;
		min_dec /= 100;
		dec_time += min_dec;
	}
	else
	{
		dec_time = ratod (timestr);
	}
	if ((dec_time > max) || (dec_time < (max * -1)))
	{
		dec_time = 0.0;
	}
	return dec_time;
}


static void FormToAuf (short ls_stat)
/**
Form in Auftragsdaten uebertragen.
**/
{
          lsk.mdn     = atoi (mdn);
          lsk.fil     = atoi (fil);
          lsk.ls    = atol (lieferschein);
          lsk.kun_fil = atoi (kunfil);
          lsk.kun = atol (kuns);
          strcpy (lsk.kun_krz1,k_kurzb);
          lsk.lieferdat = dasc_to_long (ldat); 
          lsk.komm_dat  = dasc_to_long (kdat); 
          lsk.fix_dat  = dasc_to_long (fix_dat); 
//          lsk.lieferdat  = dasc_to_long (datum); 
          strcpy (lsk.lieferzeit, lzeit);
//          lsk.zeit_dec = ratod (lzeit);
          lsk.zeit_dec = TimeToDec (lzeit, 999.99);
		  if (lsk.ls_stat < atoi (ls_status))
		  {
			lsk.ls_stat = atoi (ls_status);
		  }
          if (lsk.ls_stat < ls_stat) lsk.ls_stat = ls_stat;
//		  itoa (lsk.auf, kd_auftrag, 16);
          strcpy (lsk.auf_ext, kd_auftrag);
		  sprintf (auftrag, "%ld", lsk.auf);
		  sprintf (kd_auftrag, "%s", lsk.auf_ext);
          lsk.vertr = atol (vertrs);
          lsk.tou = atol (tour);
		  lsk.tou_nr = lsk.tou / (long) tour_div;
          strcpy (lsk.hinweis, htext);
/*
          if (lsk.ls_stat == 0)
          {
              lsk.ls_stat = 2;
          }
*/
          if (UpdatePersNam || strcmp (clipped (lsk.pers_nam), " ") <= 0)
          {
		         strcpy (lsk.pers_nam, sys_ben.pers_nam);
          }
		  if (strcmp (lsk.pers_nam, " ") <= 0)
		  {
			  strcpy (lsk.pers_nam, "leer");
		  }
		  if ( atoi ( auf_art ) > 0 )	// 240512 
				lsk.auf_art = atoi (auf_art);
          if (lspListe.GetMuster ())
          {
              lsk.auf_art = 99;
          }
}

void SetLieferdat (void)
/**
Defaultwert fuer Lieferdatum setzen.
**/
{
         char datum [12];

		 if (strcmp (ldat, "          ") <= 0 || autosysdat) 
		 {
                    sysdate (datum);
					IncDatum (datum);
                    strcpy (ldat, datum);
                    strcpy (kdat, datum);
                    strcpy (fix_dat, datum);
		 }
}


static void InitAuf (void)
/**
aufform Initialisieren.
**/
{
//          sprintf (kunfil, "%1d", 0);
	      if (telekun == FALSE)
		  {
                sprintf (kuns,"%8ld", 0l);
		  }
		  else
		  {
			    telekun = FALSE;
		  }
          strcpy (k_kurzb, " ");
          strcpy (_adr.tel, " ");
          strcpy (_adr.fax,  " ");
//          strcpy (ldat, " ");
          strcpy (kdat, " ");
          strcpy (fix_dat, " ");
          SetLieferdat ();
          strcpy (lzeit, " ");
          sprintf (ls_status, "%1d", 0);
          sprintf (auftrag, "%ld", 0l);
          strcpy (kd_auftrag, " ");
          sprintf (vertrs, "%8ld", 0l);
          strcpy (vertr_krz, " ");
          sprintf (tour, "%8ld", 0l);
          strcpy (tour_krz, " ");
          strcpy (htext, " ");
          lsk.kopf_txt = 0l;
          lsk.fuss_txt = 0l;
		  if (auf_art_active)
		  {
			  strcpy (auf_art, "1");
			  getaufart ();
		  }
}

void ReadPr (double a)
/**
Artikelpreis holen.
**/
{
       int dsqlstatus;
	   int dsqlstatusw = 100;
       short sa, sa_werbung;
       double pr_ek;
       double pr_vk;
	   BOOL fix;
       
       dlong_to_asc (lsk.lieferdat, ldat);

	   fix = FALSE;
	   int dmdn = atoi (mdn);
	   int dkun = atol (kuns);
	   int werbung = 0;
	   double werbepreis = 0.0;
		 DbClass.sqlin ((short *) &dmdn, 1, 0);
		 DbClass.sqlin ((long *) &dkun, 2, 0);
		 DbClass.sqlout ((long *) &werbung, 2, 0);
		 DbClass.sqlcomm ("select werbung from kun_erw where mdn = ? and fil = 0 and kun = ?");

	   if (atoi (kunfil) == 0 && werbung > 0)
	   {
					dsqlstatusw = WaPreis.werbung_holen (atoi (mdn),
                                          0,
                                          werbung,
                                           a,
                                          ldat,
                                          &sa_werbung,
                                          &pr_ek,
										  &fix);

					if (sa_werbung == 1 && fix == TRUE) 
					{
		                werbepreis = pr_ek;

					}


	   }


// Preise immer mit Filiale 0 lesen.

	   WaPreis.SetAufArt (atoi (auf_art));
	   if (lspListe.DllPreise.PriceLib != NULL && 
	       lspListe.DllPreise.preise_holen != NULL)
	   {
				  dsqlstatus = (lspListe.DllPreise.preise_holen) (atoi (mdn), 
                                          0,
                                          atoi (kunfil),
                                          atol (kuns),
                                          a,
                                          ldat,
                                          &sa, 
                                          &pr_ek,
                                          &pr_vk);
	   }
	   else
	   {
					dsqlstatus = WaPreis.preise_holen (atoi (mdn),
                                          0,
                                          atoi (kunfil),
                                          atol (kuns),
                                          a,
                                          ldat,
                                          &sa, 
                                          &pr_ek,
                                          &pr_vk);
	   }

	   if (fix == TRUE && dsqlstatusw == 0) //WAL-103
	   {
			pr_ek = werbepreis;
			sa = sa_werbung;
	   }

       if (dsqlstatus == 0)
       {
                 lsp.ls_vk_pr  = pr_ek;
                 lsp.ls_lad_pr = pr_vk;
				 lsp.sa_kz_sint = sa;
	             strcpy (lsp.kond_art,   WaPreis.GetKondArt ());
				 lsp.a_grund   =  WaPreis.GetAGrund ();
				 lspListe.FillKondArt (lsp.kond_art);
       }
	   lspListe.SetAufVkPr (pr_ek, pr_vk);	// 270812
	   lspListe.FillWaehrung (pr_ek, pr_vk);
	   lspListe.SetPreise ();
}

void InsLsp (long ls, double a, long posi)
/**
Satz in Auftragsposition einfuegen.
**/
{
           lsk.lieferdat = dasc_to_long (ldat);
           if (ls_class.lese_lsp_a (lsk.mdn, lsk.fil, ls, a) != 0)
           {
               return;
           }
           lsp.sa_kz_sint = 0;
           lsp.lief_me  = (double) 0.0;
           lsp.auf_me1 = (double) 0.0;
           lsp.auf_me2 = (double) 0.0;
           lsp.auf_me3 = (double) 0.0;
           lsp.lief_me = (double) 0.0;
           lsp.posi = posi;
		   if (akt_preis)
		   {
		           ReadPr (a);
		   }
           ls_class.update_lsp (lsp.mdn, lsp.fil, 
                                 lsk.ls, a, posi);
}


static int LastBest (void)
/**
Artikel der letzten Bestellungen holen.
**/
{
          int status; 
          long auf;
          double a;
          long posi;

          posi = 0;
          AufLast.AnzArt (anzart);
          AufLast.ReadLstAuf (lsk.mdn, atol (kuns));
          status = AufLast.GetFirst (&auf, &a);
          while (status)
          {
              posi += 10;
              InsLsp (auf, a, posi);
              status = AufLast.GetNext (&auf, &a);
          }
          break_enter ();
          return 0;
}

BOOL MultiMdnProd (void)
/**
Test, ob mehrere verschiedene Gruppen in lsp existieren.
**/
{
	      int i;
		  int cursor;
     
		  DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		  DbClass.sqlin ((short *) &lsk.fil, 1, 0);
		  DbClass.sqlin ((long *) &lsk.ls, 2, 0);
		  cursor = DbClass.sqlcursor ("select distinct gruppe from lsp "
			                          "where mdn = ? "
									  "and   fil = ? "
									  "and ls = ?");
		  i = 0;
		  while (DbClass.sqlfetch (cursor) == 0)
		  {
			  i ++;
		  }
		  DbClass.sqlclose (cursor);
		  if ( i > 1) return TRUE;
		  return FALSE;
}


static BOOL TestLiefMe0 ()
{
//	if (!lspListe.TestLiefMe0 ())
	if (!AufKompl.TestLiefMe0 ())
	{
		return FALSE;
	}

	return TRUE;
}

static BOOL TestPositions ()
{
	int PosMode = 0;

	while (AufKompl.TestLiefMe0 ())
	{
		if (abfragejn (mamain1, "Es sind unbearbeitete Positionen vorhanden\n"
								"Vorgang abbrechen ?", "J"))
		{
 					   return FALSE;
		}
		PosMode = AufKompl.GetLiefMe0Mode ();
		if (PosMode == AufKompl.PosDlgCancel)
		{
			return FALSE;
		}
		AufKompl.AnalysePosMode (PosMode);
		if (PosMode == AufKompl.PosAccept)
		{
			break;
		}
	}

	if (KompTestVk0 != 0)
	{
		BOOL TestLeergut = FALSE;
		if (KompTestVk0 == 2)
		{
			TestLeergut = TRUE;
		}
		if (AufKompl.TestPrVk0 (TestLeergut))
		{
			if (abfragejn (mamain1, "Es sind Positionen mit Preis0 vorhanden\n"
									"Vorgang abbrechen ?", "J"))
			{
 						   return FALSE;
			}
		}
	}
	return TRUE;

}

static int SetKomplett (void)
{


//          LeergClass.EnterLeerg (AktivWindow); //testtesttest 
	      NoClose = TRUE;
		  if (!TestPositions ())
		  {
			  SetCurrentField (currentfield);
			  SetCurrentFocus (currentfield);
	          NoClose = FALSE;
			  return 1;
		  }
          if (ldatdiffOK () == FALSE)
		  {
					 SetCurrentField (currentfield);
					 SetCurrentFocus (currentfield);
		             return 1;
		  }
          if (bg_cc_par && zustell_par && 
                    getaufart () == -1)
          {
                     print_mess (2, "Falsche Auftragsart");
					 SetCurrentField (currentfield);
					 SetCurrentFocus (currentfield);
		             return 1;
          }
		  if (abw_komm == 0) strcpy (kdat, ldat);

/*********/
          if (!IsC_C () && lspListe.GetPosanz () == 0)
		  {
					 disp_mess ("Es wurden kein Positionen erfasst", 2);
					 SetCurrentField (currentfield);
					 return (1);
		  }
/***********/
		  TestUniqueKun = FALSE;
 		  if (testkun0 () == FALSE)
		  {
        		     TestUniqueKun = TRUE;
			         NoClose = FALSE;
					 return 1;
		  }
	      TestUniqueKun = TRUE;
	      if (TouZTest () == FALSE) return 0;
          lspListe.DestroyWindows ();
		  if (lutz_mdn_par && lsk.ls_stat < 3)
		  {
			  if (MultiMdnProd ())
			  {
				  FormToAuf (2);
			  }
			  else
			  {
				  FormToAuf (3);
			  }
		  }
		  else
		  {
              FormToAuf (3);
		  }
          ls_class.update_lsk (atoi (mdn), atoi (fil), lsk.ls);
//          commitwork ();
          if (lsk.ls != auto_nr.nr_nr)
          {
//                       beginwork ();
					   if (lutz_mdn_par)
					   {
						   sprintf (mdn, "%hd", 0);
						   sprintf (fil, "%hd", 0);
					   }

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "ls",  auto_nr.nr_nr);
//                       commitwork ();
          }
		  CloseControls (&Ffrei_txt2_bu);
          Mess.Message ("Satz wurde geschrieben");
		  if (KomplettMode == 0)
		  {
                   AufKompl.SetKomplett (mamain1);
		  }
          syskey = KEY12;
          break_enter ();
          NoClose = FALSE;
          SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
//          SetActiveWindow (hMainWindow);
          SetForegroundWindow (hMainWindow);
          SetMuster (FALSE);
		  if (TransactMax > 0)
		  {
		        tcount ++;
		        if (tcount >= TransactMax)
				{
					 RestartProcess ();
				}
		  }
		  if (ScrollChoice && NewPrPlugin != NULL)
		  {
			  PostMessage (NULL, WM_KEYDOWN, VK_F3, 0l);
		  }
          return 0;
}


int lese_auf_mdn (long auf)
/**
Kunde ohne Mandant lesen. Mandant wird vom Kunden geholt.
**/
{
	   int dsqlstatus;
	   DbClass.sqlin ((long *) &auf, 2, 0);
	   DbClass.sqlout ((char *) mdn, 0, 5);
	   DbClass.sqlout ((char *) fil, 0, 5);
       dsqlstatus = DbClass.sqlcomm ("select mdn,fil from lsk where auf = ?");
	   return dsqlstatus;
}

static int leseauf ()
/**
Auftrag lesen.
**/
{
	     int dsqlstatus;

         if (testkeys ()) return 0;

		 TestLizenz ();
		 lsk.kun = 0l;
		 UniqueKunError = FALSE;
         LiefAdr = 0l;
		 adr_nr = 0l;
         temode = 1;
         SetTeleNr ();
		 lsk.ls = atol (lieferschein);
         if (mench > 0 && akt_auf && 
             (atol (lieferschein) == akt_auf)) 
         {
             SetCurrentField (0);
             return 0;
         }

         lspListe.DestroyWindows (); 
		 if (lutz_mdn_par)
		 {
			 dsqlstatus = lese_auf_mdn (atol (lieferschein));
		 }
		 else
		 {
			 dsqlstatus = 0;
		 }
		 if (dsqlstatus == 0)
		 {
             dsqlstatus = ls_class.lese_lsk (atoi (mdn),
                                          atoi (fil),
                                          atol (lieferschein));
		 }
         if (lsk.delstatus != 0) dsqlstatus = 100;
         if (dsqlstatus == 0 && mench == 0)
         {
                       if (lsk.ls_stat > 4)
                       {
                           disp_mess ("Der Lieferschein wurde schon abgeschlossen", 2);
						   SetCurrentField (0);
                           return 0;
                       }
                       if (Lock_Lsk () == FALSE) 
					   {
						   SetCurrentField (0);
						   return 0;
					   }
         }
//		 else if (mench == 0 || (mench == 2 && dsqlstatus == 0))
		 else if (mench == 0)
		 {
                       if (Lock_Lsk () == FALSE) 
					   {
						   SetCurrentField (0);
						   return 0;
					   }
		 }

         if (dsqlstatus == 0 && mench == 0)
         {
                       NewRec = 0;
					   PosOK = TRUE;
         }
         else if (mench == 0)
         {
                       NewRec = 1;
					   PosOK = FALSE;
         }

         if (dsqlstatus == 0)
         {
			           if (IsShortnull (lsk.psteuer_kz))
					   {
						   lsk.psteuer_kz = 0;
					   }
			           if (lsk.psteuer_kz == 0)
					   {
						   CheckPartyService (FALSE);
					   }
					   else
					   {
						   CheckPartyService (TRUE);	// Das ist aber dumm, man kann das nie wieder deaktivieren ?!
					   }
			             
			           adr_nr = lsk.adr; 
                       if (KunLiefKz)
                       {
                            LiefAdr = adr_nr;
                       }
                       AufToForm ();  
         }

         else if (mench != 0 || lsk.ls != auto_nr.nr_nr)
         {
                       print_mess (2, "Lieferschein %ld nicht gefunden",
                                   atol (lieferschein));
					   if (mench != 1)
					   {
						   rollbackwork ();
						   beginwork ();
					   }
                       InitAuf ();
					   if (mench == 0) 
					   {
						   sprintf (lieferschein, "%ld",auto_nr.nr_nr);
					   }
					   else
					   {
						    SetCurrentField (0);
                            strcpy (lieferschein, "0");
					   }
                       display_form (mamain1, &aufformk, 0, 0);
                       return 0;
         }
         else
         {
			           lsk.ls_stat = 0; 
                       if (Lock_Lsk () == FALSE) return 0;
                       memcpy (&lsk, &lsk_null, sizeof (struct LSK));
                       InitAuf ();
                       lsk.mdn = atoi (mdn);
                       lsk.fil = atoi (fil);
                       lsk.ls = atol (lieferschein);
					   if (IsPartyService)
					   {
						   lsk.psteuer_kz = 1;
						   SetToStandardTax ();
					   }
         }
//		 lsk.ccmarkt = IsC_C ();
		 if (cfgccmarkt)
		 {
		        lsk.ccmarkt = 1;
		 }

         if (mench != 0)
         {
             ShowDaten ();
             SetCurrentField (0);
         }
         else
         {
             break_enter ();
         }
         akt_auf = atol (lieferschein);
         return 0;
}

static int leseaufcc (long ls)
/**
Auftrag lesen f�r ccmarkt Einstieg �ber Kunde
**/
{
	     int dsqlstatus;

         if (testkeys ()) return 0;

         LiefAdr = 0l;
		 adr_nr = 0l;
         temode = 1;
         SetTeleNr ();
		 lsk.ls = ls;
         if (mench > 0 && akt_auf && 
             (ls == akt_auf)) 
         {
             return 0;
         }

         lspListe.DestroyWindows (); 
         dsqlstatus = ls_class.lese_lsk (atoi (mdn),
                                          atoi (fil),
                                          ls);
         if (lsk.delstatus != 0) dsqlstatus = 100;
         if (dsqlstatus == 0 && mench == 0)
         {
                       if (lsk.ls_stat > 4)
                       {
                           disp_mess ("Der Lieferschein wurde schon abgeschlossen", 2);
                           return 0;
                       }
//                       if (Lock_Lsk () == FALSE) return 0;
         }
		 else if (mench == 0 || (mench == 2 && dsqlstatus == 0))
		 {
//                       if (Lock_Lsk () == FALSE) return 0;
		 }

         if (dsqlstatus == 0 && mench == 0)
         {
                       NewRec = 0;
					   PosOK = TRUE;
         }
         else if (mench == 0)
         {
                       NewRec = 1;
					   PosOK = FALSE;
         }

         if (dsqlstatus == 0)
         {
			           adr_nr = lsk.adr; 
                       if (KunLiefKz)
                       {
                            LiefAdr = adr_nr;
                       }
                       AufToForm ();  
         }

         else if (mench != 0 || lsk.ls != auto_nr.nr_nr)
         {
                       print_mess (2, "Lieferschein %ld nicht gefunden",
                                   atol (lieferschein));
					   if (mench != 1)
					   {
						   rollbackwork ();
						   beginwork ();
					   }
                       InitAuf ();
					   if (mench == 0) 
					   {
						   sprintf (lieferschein, "%ld",auto_nr.nr_nr);
					   }
					   else
					   {
                            strcpy (lieferschein, "0");
					   }
                       display_form (mamain1, &aufformk, 0, 0);
                       return 0;
         }
         else
         {
			           lsk.ls_stat = 0; 
  //                     if (Lock_Lsk () == FALSE) return 0;
                       memcpy (&lsk, &lsk_null, sizeof (struct LSK));
                       InitAuf ();
                       lsk.mdn = atoi (mdn);
                       lsk.fil = atoi (fil);
                       lsk.ls = atol (lieferschein);
         }
		 lsk.ccmarkt = IsC_C ();

         if (mench != 0)
         {
             ShowDaten ();
         }
         else
         {
             break_enter ();
         }
         akt_auf = atol (lieferschein);
         return 0;
}

void IncDatum (char *datum)
/**
Systemdatum um dat_plsu erhoehen.
**/
{
	    long ldat;
		
        if (datum_plus == 0l) return;

        ldat = dasc_to_long (datum);
        ldat += datum_plus;
        dlong_to_asc (ldat, datum);
}

long GetKunLiefTour (void)
/**
Tour aus kunlief holen.
**/
{
         KUNLIEF_CLASS KunLief;

         kunlief.mdn = atoi (mdn);
         kunlief.fil = atoi (fil);
         kunlief.kun = kun.kun;
         kunlief.adr = LiefAdr;
         if (KunLief.dbreadfirst () == 0)
         {
             if (kunlief.tou != 0l)
             {
                 return kunlief.tou;
             }
         }
         return kun.tou;
}

void SetHtext (char ** DayTab, char *WeekDay)
{
    char text [256];

    clipped (htext);
    strcpy (text, htext);
    for (int i = 0; DayTab[i] != NULL; i ++)
    {
        if (strlen (htext) < strlen (DayTab[i]))
        {
            continue;
        }
        if (memcmp (DayTab[i], htext, strlen (DayTab[i])) == 0)
        {
            break;
        }
    }
    int pos = 0;
    if (DayTab[i] != NULL)
    {
        pos = strlen (DayTab[i]);
    }

    strcpy (htext, WeekDay);
    strcat (htext, &text[pos]);
}

void GetWochenTag ()
{
    static char *WeekDay[] = {"Sonntag",
                              "Montag",
                              "Dienstag",
                              "Mittwoch",
                              "Donnerstag",
                              "Freitag",
                              "Samstag",
                              "Sonntag",
                              NULL,
    };

    if (Wochentag == FALSE)
    {
        return;
    }

    short wk = get_wochentag (ldat);
    SetHtext (WeekDay, WeekDay[wk]);
    int htextpos = GetItemPos (&aufform, "htext");
    if (htextpos > -1)
    {
         display_field (mamain1, &aufform.mask[htextpos], 0, 0);
    }
}

void GetLieferdat ()
/**
Lieferdatum aus Tourenplan holen.
**/
{
         char datum [12];
         long tou;
         int ldatpos;

         GetWochenTag ();
		 if (tou_par == 0) return;
		 if (IsC_C ()) 
		 {
            sprintf (tour, "%ld", 0l);
			return;
		 }
		 if (wo_tou_par == 0)
		 {
			      if (atoi (kunfil) == 0 && atol (tour) == 0l)
				  {
                         if (KunLiefKz && LiefAdr != 0l)
                         {
                             kun.tou = GetKunLiefTour ();
                         }
                         sprintf (tour, "%ld", kun.tou);
				  }

				  else
				  {
                         sprintf (tour, "%ld", _fil.tou);
				  }

                  testtou (1);
				  return;
		 }

		 if (tour_datum == 0) 
		 {
// Tour zum Datum holen
                int toupos = GetItemPos (&aufform, "tou");
                if ((aufform.mask[toupos].attribut & REMOVED) == 0)
                {
				   sysdate (datum);
				   IncDatum (datum);
				   strcpy (ldat, datum);
				   strcpy (kdat, datum);
				   strcpy (fix_dat, datum);
                   tou = QTClass.DatetoTouDir (lsk.mdn, 0, atoi (kunfil), 
                                  atol (kuns), ldat);
                   current_form = &aufform;
                   sprintf (tour, "%ld", tou);
                   display_field (mamain1, &aufform.mask[toupos], 0, 0);
                   if (tou) 
                   {
			             testtou (0);
                   }
		           else
                   {
                         sprintf (tour_krz, " "); 
                         int toukrzpos = GetItemPos (&aufform, "tou_bz");
                         display_field (mamain1, &aufform.mask[toukrzpos], 0, 0);
                   }
				}
			    return;
		 }

         sysdate (datum);
		 IncDatum (datum);
         strcpy (ldat, datum);
         strcpy (kdat, datum);
         strcpy (fix_dat, datum);
/*
         tou = QTClass.DatetoTou (lsk.mdn, lsk.fil, atoi (kunfil), 
                                  atol (kuns), ldat);
*/

// Tour mit Filiale 0 bearbeiten

         tou = QTClass.DatetoTou (lsk.mdn, 0, atoi (kunfil), 
                                  atol (kuns), ldat);
         if (tou == 0l) return;
         ldatpos = GetItemPos (&aufform, "lieferdat");
         GetWochenTag ();
         current_form = &aufform;
         display_field (mamain1, &aufform.mask[ldatpos], 0, 0);
         sprintf (tour, "%ld", tou);
         testtou (1);
}


static void FillKredLim ()
{
	      char wert [6];
		  
	      if (KreditLimit == FALSE)
		  {
			  return;
		  }
  	      sprintf (wert, "%.0lf", kun.kred_lim);
	      ptabn.ptbez[0] = 0;
          ptab_class.lese_ptab ("kred_lim", wert);
	      strcpy (htext, ptabn.ptbez);
          int htextpos = GetItemPos (&aufform, "htext");
          if (htextpos > -1)
		  {
                     display_field (mamain1, &aufform.mask[htextpos], 0, 0);
		  }
}


static void TestFreiTxt2 ()
{
	Text FreiTxt2 = kun.frei_txt2;
	FreiTxt2.TrimRight ();
	if (FreiTxt2.GetLen () == 0)
	{
		  CloseControls (&Ffrei_txt2_bu);
		  return;
	}
/*
	ffrei_txt2.SethWndMain (mamain1);
	ffrei_txt2.display ();
*/
	display_form (mamain1, &Ffrei_txt2_bu, 0, 0);
    set_fkt (lspListe.KunInfo, 3);
}


static BOOL OpenPosition (long kun)
{
	static int kuncursor = -1;
	static short mdnnr;
	static long kunnr;
	static long ls = 0l;

	UniqueKunError = FALSE;
	if (UniqueKun == FALSE)
	{
		return FALSE;
	}

	if (atoi (kunfil) != 0)
	{
		return FALSE;
	}

	if (TestUniqueKun == FALSE)
	{
		return FALSE;
	}

	if (syskey == KEY6)
	{
		return FALSE;
	}

	if (kuncursor == -1)
	{
		DbClass.sqlin ((short *) &mdnnr, 1, 0);
		DbClass.sqlin ((long *) &kunnr, 2, 0);
		DbClass.sqlin ((long *) &lsk.ls, 2, 0);
		DbClass.sqlout ((long *) &ls, 2, 0);
		kuncursor = DbClass.sqlcursor ("select ls from lsk "
		   	                           "where mdn = ? "
									   "and kun = ? "
									   "and kun_fil = 0 "
									   "and ls != ? "
									   "and kun > 0 "
									   "and delstatus = 0 "
									   "and ls_stat < 4");
	}
	mdnnr = atoi (mdn);
	kunnr = kun;
	ls = 0l;
    UniqueKunLs = ls;
	DbClass.sqlopen (kuncursor);
	int ret = DbClass.sqlfetch (kuncursor);
	if (ret == 0)
	{
		if (OpenPosMessage)
		{
		   Text txt;
		   txt.Format ("Es existiert ein offener Lieferschein f�r den Kunden %ld\n"
			        "Lieferscheinnr %ld bearbeiten ?", kunnr, ls);
            if (abfragejn (mamain1, txt.GetBuffer (), "J"))
			{
 	               UniqueKunLs = ls;
			}
		}
		else
		{
            UniqueKunLs = ls;
		}
 	    UniqueKunError = TRUE;
		return TRUE;
	}
	return FALSE;
}


static int testkun0 ()
{
         int kun_krz_field; 
		 int kun_field;
         int tele_field;
         
         kun_field = GetItemPos (&aufform, "kun");
         kun_krz_field = GetItemPos (&aufform, "kun_krz");
         tele_field = GetItemPos (&aufform, "tele");
         if (atol (kuns) == 0)
         {
                  disp_mess ("Kunde 0 ist nicht erlaubt", 2);
                  SetCurrentField (kun_field);
                  return FALSE;
         }

	     if (autokunfil && akt_kun != atol (kuns))
		 {
	              lsk.kun_fil = 0;
		          strcpy (kunfil, "0");
                  ikunfil_bz.SetFeldPtr ("Kunde");
		 }
         if (atoi (kunfil) == 0)
         {


			      dsqlstatus = 0; 
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_kun_mdn (atol (kuns));
				  }
				  if (dsqlstatus == 0)
				  {
/*
                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                    atoi (fil),
                                                    atol (kuns));
*/

// Kunde immer mit Filiale 0 lesen 

                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                    0,
                                                    atol (kuns));
				  }
				  if (dsqlstatus == 100 && autokunfil)
				  {
					   lsk.kun_fil = 1;
					   strcpy (kunfil, "1");
                       ikunfil_bz.SetFeldPtr ("Filiale");
                       display_form (mamain1, &aufformk, 0, 0);
				  }
				  else if (dsqlstatus == 0 && kun.kun_gr2 == 9999)   //260613 Sonderablauf f. Werner Filialen sollen als Kunde drin bleiben, aber keine Kundenlieferscheine erzeugt werden
				  {
					   lsk.kun_fil = 1;
					   strcpy (kunfil, "1");
                       ikunfil_bz.SetFeldPtr ("Filiale");
                       display_form (mamain1, &aufformk, 0, 0);
				  }
				  else
				  {
                      if (dsqlstatus)
					  { 
                       disp_mess ("Kunde nicht angelegt", 0);
                       SetCurrentField (kun_field);
					   akt_kun = 0l;
                       return FALSE;
					  }
                      smtHandler.set_Kun (&kun);
					  TestFreiTxt2 ();
                      if (kun.kun_of_lf > 0)
                      {
                          disp_mess ("Der Kunde hat Lieferstop !!", 2);
                          SetCurrentField (kun_field);
	    			      akt_kun = 0l;
                          sprintf (kuns, "%8ld", 0l);
                          display_field (mamain1, &aufformk.mask[kun_field], 0, 0);
                          return FALSE;
                      }

			          if (OpenPosition (atol (kuns)))
					  {
					      if (UniqueKunLs > 0l)
						  {
					        dokey50 ();
						  }
					      else
						  {
                            SetCurrentField (kun_field);
						  }
					      return FALSE;
					  }
					  FillKredLim ();

//					  akt_kun = kun.kun;
                      if (KunLiefKz && LiefAdr > 0l)
                      {
                          kun.adr2 = LiefAdr;
                      }
                      dsqlstatus = adr_class.lese_adr (kun.adr2);


// KB Lieferadressnummer wird auch bei diversen Kunden �bernommen 29.04.2009
					  if (_adr.adr_typ != 25)
					  {
  		               lsk.adr = kun.adr2;
					  }
// Row �bernahme f�r diverse Kunden nur wenn lsk.adr != 0 17.06.2009
					  else if (lsk.adr == 0l)
					  {
  		               lsk.adr = kun.adr2;
					  }

//                      strcpy (k_kurzb, _adr.adr_krz);
                      strcpy (k_kurzb, kun.kun_krz2);
      		          lsk.waehrung = kun.waehrung;
			          if (vertr_abr_par && vertr_kun)
					  {
                           if (atol (vertrs) == 0l)
                           {
					             sprintf (vertrs, "%ld", kun.vertr1);
                           }
					       testvertr ();
 					  }
			          if ((lsk.kopf_txt == 0) && use_kopf_txt && kun.ls_kopf_txt)
					  {
					   lsk.kopf_txt = kun.ls_kopf_txt;
					  }
                      if (autokunfil)
					  {
 				          lsk.kun_fil = 0;
				 	      strcpy (kunfil, "0");
                          ikunfil_bz.SetFeldPtr ("Kunde");
                          display_form (mamain1, &aufformk, 0, 0);
					  }
				  }
 
         }
         if (atoi (kunfil) == 1)
         {
		          dsqlstatus = 0;
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_fil_mdn (atol (kuns));
				  }
//				  if (dsqlstatus == 0)
				  {
                       dsqlstatus = fil_class.lese_fil (atoi (mdn),
                                                    atoi (kuns));
                       if (dsqlstatus)
					   {
                                disp_mess ("Filiale nicht angelegt", 0);
                                SetCurrentField (kun_field);
								if (autokunfil)
								{
					                   lsk.kun_fil = 0;
					                   strcpy (kunfil, "0");
                                       ikunfil_bz.SetFeldPtr ("Kunde");
                                       display_form (mamain1, &aufformk, 0, 0);
								}
                                return FALSE;
					   }
                       else
					   {
                                dsqlstatus = adr_class.lese_adr (_fil.adr);
					            lsk.adr = _fil.adr;
                                strcpy (k_kurzb, _adr.adr_krz);
					            lsk.waehrung = atoi (_mdn.waehr_prim);
//					            akt_kun = _fil.fil;
					   }
				  }
         }
		 if (auf_art_active == FALSE)
		 {
                  display_field (mamain1, &aufform.mask[tele_field], 0, 0);
		 }
         clipped (_adr.tel);
         if (strcmp (_adr.tel, " ") > 0)
         {
                          CTelefon.bmp = btelefon;
                          ActivateColButton (mamain1, 
                                      &buform, 1, 0, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
         }
         else
         {
                          CTelefon.bmp = btelefoni;
                          ActivateColButton (mamain1, 
                                      &buform,      1, -1, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
         }
         clipped (_adr.fax);
         if (strcmp (_adr.fax, " ") > 0)
         {
                          ActivateColButton (mamain1, 
                                      &buform,      0, 0, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
         }
         else
         {
                          ActivateColButton (mamain1, 
                                      &buform,      0, -1, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
         }
         if (ls_class.lese_lsp (lsk.mdn, lsk.fil, 
                                 atol (lieferschein)) == 100)
         {
                   set_fkt (LastBest, 8);
                   SetFkt (8, bestellung, KEY8);
         }
         else
         {
                   set_fkt (NULL, 8);
                   SetFkt (8, leer, NULL);
         }
         ActivateColButton (mamain1, &buform, 2, 0, 1);
         EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
         MoveButtons ();
         current_form = &aufform;
         display_field (mamain1, &aufform.mask[kun_field], 0, 0);
         display_field (mamain1, &aufform.mask[kun_krz_field], 0, 0);
		 return TRUE;
}

static int lesekun0 ()
{
         int kun_krz_field;
         int kun_field;
 
		 clipped (kuns);
         kun_krz_field = GetItemPos (&aufform, "kun_krz");
         kun_field = GetItemPos (&aufform, "kun");
 	     if (!numeric (kuns))
		 {
                  if (SearchKunDirect)
                  {
                      return ShowKun ();
                  }
                  
			      kun.mdn = atoi (mdn);
			      kun.fil = atoi (fil);
                  QClass.searchkun_direct (mamain1, kuns);
		          sprintf (kuns, "%8ld", atol (kuns));
				  if (atol (kuns) == 0)
				  {
                            current_form = &aufform;
                            SetCurrentField (currentfield);
                            display_field (mamain1, &aufform.mask[kun_field], 0, 0);
							return 0;
				  }
		 }

         if (OpenPosition (atol (kuns)))
		 {

				  if (UniqueKunLs > 0l)
				  {
					        dokey50 ();
				  }
				  else

				  {
                           SetCurrentField (kun_field);
				  }
				  return 0;
		 }
		 sprintf (kuns, "%8ld", atol (kuns));

         if (akt_kun != atol (kuns))
         {
                  sprintf (tour, "%ld", 0l);
                  sprintf (vertrs, "%hd", 0);
                  strcpy (tour_krz, "");
                  strcpy (vertr_krz , "");
                  if (tou_par)
                  {
                     display_field(mamain1, &aufform.mask[GetItemPos (&aufform, "tou")], 0, 0);
                     display_field(mamain1, &aufform.mask[GetItemPos (&aufform, "tou_bz")], 0, 0);
                  }
                  if (vertr_abr_par)
                  {
                     display_field(mamain1, &aufform.mask[GetItemPos (&aufform, "vertr")], 0, 0);
                     display_field(mamain1, &aufform.mask[GetItemPos (&aufform, "vertr_krz")], 0, 0); 
                  }
         }

         SetFkt (9, leer, 0);
         set_fkt (NULL, 9);

         if (akt_kun == atol (kuns)) 
		 {
             GetLieferdat ();
             display_field (mamain1, &aufform.mask[kun_field], 0, 0);
             display_field (mamain1, &aufform.mask[kun_krz_field], 0, 0);
			 return 0;
		 }
         if (atol (kuns) == 0)
         {
                  disp_mess ("Kunde 0 ist nicht erlaubt", 2);
                  SetCurrentField (currentfield);
                  return 0;
         }

	     if (autokunfil && akt_kun != atol (kuns))
		 {
	              lsk.kun_fil = 0;
		          strcpy (kunfil, "0");
                  ikunfil_bz.SetFeldPtr ("Kunde");
		 }

         if (atoi (kunfil) == 0)
         {
			      dsqlstatus = 0; 
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_kun_mdn (atol (kuns));
				  }
				  if (dsqlstatus == 0)
				  {
/*
                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                    atoi (fil),
                                                    atol (kuns));
*/

// Kunde immer mir Filiale 0 lesen 

                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                         0,
                                                         atol (kuns));
				  }
			      if (dsqlstatus == 100 && autokunfil)
				  {
					   lsk.kun_fil = 1;
					   strcpy (kunfil, "1");
                       ikunfil_bz.SetFeldPtr ("Filiale");
                       display_form (mamain1, &aufformk, 0, 0);
				  }
				  else
				  {
                       if (dsqlstatus)
					   {
                           disp_mess ("Kunde nicht angelegt", 0);
                           SetCurrentField (currentfield);
                           return 0;
					   }
                       smtHandler.set_Kun (&kun);
				       if (kun.kun_of_lf > 0)
                       {
                          disp_mess ("Der Kunde hat Lieferstop !!", 2);
                          SetCurrentField (kun_field);
	    			      akt_kun = 0l;
                          sprintf (kuns, "%8ld", 0l);
                          display_field (mamain1, &aufformk.mask[kun_field], 0, 0);
                          return FALSE;
                       }
					   akt_kun = kun.kun;
                       if (KunLiefKz && LiefAdr > 0l)
                       {
                          kun.adr2 = LiefAdr;
                       }
       		           lsk.waehrung = kun.waehrung;
                       dsqlstatus = adr_class.lese_adr (kun.adr2);
// KB Lieferadressnummer wird auch bei diversen Kunden �bernommen 29.04.2009
					   if (_adr.adr_typ != 25)
					   {
				           lsk.adr = _adr.adr;
					   }
// Row �bernahme f�r diverse Kunden nur wenn lsk.adr != 0 17.06.2009
					   else if (lsk.adr == 0l)
					   {
  		                   lsk.adr = kun.adr2;
					   }
//                       strcpy (k_kurzb, _adr.adr_krz);
                       strcpy (k_kurzb, kun.kun_krz2);
				       if (vertr_abr_par && vertr_kun)
					   {
                           if (atol (vertrs) == 0l)
                           {
					                 sprintf (vertrs, "%ld", kun.vertr1);
                           }
 					       testvertr ();
					   }
			           if ((lsk.kopf_txt == 0) && use_kopf_txt && kun.ls_kopf_txt)
					   {
					       lsk.kopf_txt = kun.ls_kopf_txt;
					   }
					   if (autokunfil)
					   {
  				           lsk.kun_fil = 0;
					       strcpy (kunfil, "0");
                           ikunfil_bz.SetFeldPtr ("Kunde");
                           display_form (mamain1, &aufformk, 0, 0);
					   }
				  }
         }
         if (atoi (kunfil) == 1)
         {
		          dsqlstatus = 0;
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_fil_mdn (atol (kuns));
				  }
				  if (dsqlstatus == 0)
				  {
                       dsqlstatus = fil_class.lese_fil (atoi (mdn),
                                                    atoi (kuns));
                       if (dsqlstatus)
					   {
                                disp_mess ("Filiale nicht angelegt", 0);
								if (autokunfil)
								{
					                   lsk.kun_fil = 0;
					                   strcpy (kunfil, "0");
                                       ikunfil_bz.SetFeldPtr ("Kunde");
                                       display_form (mamain1, &aufformk, 0, 0);
								}
                                SetCurrentField (currentfield);
                                return 0;
					   }
                       else
					   {
                                dsqlstatus = adr_class.lese_adr (_fil.adr);
          			            lsk.adr = _adr.adr;
                                strcpy (k_kurzb, _adr.adr_krz);
					            akt_kun = kun.kun;
					   }
                       kun.einz_ausw = 0;
				  }
         }
         akt_kun = atol (kuns);
         GetLieferdat ();
         if (dsqlstatus == 0)
         {
                   clipped (_adr.tel);
                   if (strcmp (_adr.tel, " ") > 0)
                   {
                          CTelefon.bmp = btelefon;
                          ActivateColButton (mamain1, 
                                      &buform, 1, 0, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
                   }
                   else
                   {
                          CTelefon.bmp = btelefoni;
                          ActivateColButton (mamain1, 
                                      &buform,      1, -1, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
                   }

                   clipped (_adr.fax);
                   if (strcmp (_adr.fax, " ") > 0)
                   {
                          ActivateColButton (mamain1, 
                                      &buform,      0, 0, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
                   }
                   else
                   {
                          ActivateColButton (mamain1, 
                                      &buform,      0, -1, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
                   }
                   ActivateColButton (mamain1, &buform, 2, 0, 1);
                   EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
                   MoveButtons ();
         }
         if (ls_class.lese_lsp (lsk.mdn, lsk.fil, 
                                 atol (lieferschein)) == 100)
         {
                   set_fkt (LastBest, 8);
                   SetFkt (8, bestellung, KEY8);
         }
         else
         {
                   set_fkt (NULL, 8);
                   SetFkt (8, leer, NULL);
         }
         MoveButtons ();
         current_form = &aufform;
         display_field (mamain1, &aufform.mask[kun_field], 0, 0);
         display_field (mamain1, &aufform.mask[kun_krz_field], 0, 0);
         return 1;
}

static int lesekun ()
/**
Kunde fuer Kundennummer holen.
**/
{

         if (testkeys ()) return 0;

         SetFkt (8, leer, NULL);
         set_fkt (NULL, 8);
         if (akt_kun != atol (kuns))
         {
             sprintf (vertrs, "%ld", 0l);
         }
		 clipped (kuns);
		 if (numeric (kuns))
		 {
                 if (KunLiefKz)
                 {
					 ChoiseKunLief0 ();
                       if (atol (kuns) == 0l)
                       {
                           return 1;
                       }
                 }
		         if (testkun0 () == FALSE) 
                 {
                     return 1;
                 }
		 }
		 else
		 {
			int ret = lesekun0 ();
			if (ret == 0) 
			{
				return 1;
			}
			if (KunLiefKz)
			{
				ChoiseKunLief0 ();
				testkun0 ();
				return 1;
			}
		 }
		 if (listedirect == 1)
		 {
			 PostMessage (mamain1, WM_KEYDOWN, VK_F6, 0l);
		 }
//WAL-167         if (ret && EnableKunDirect)
         {
             EnableKun (FALSE);
         }
		 return 1;
}

long KopfTextGen (void)
/**
Kopftexte erfassen.
**/
{
           long text_nr;
           extern short sql_mode;


           sql_mode = 1;
           dsqlstatus = AutoClass.nvholid (0, 0, "ls_txt");
		   if (dsqlstatus == -1)
		   {
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"ls_txt\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
			}
					        
            if (dsqlstatus == 100)
            {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  "ls_txt",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (0,
                                                        0,
                                                        "ls_txt");
                           }
             }
             sql_mode = 0;
             if (auto_nr.nr_nr == 0l)
             {
                    disp_mess ("Es konnte keine text-Nr generiert werden", 2);
                    return 0l;
             }
			 text_nr = auto_nr.nr_nr;
			 return text_nr;
}


BOOL KopfTextExist (short mdn, short fil, long kun, long kopt_txt)
/**
Test, ob die Nummer schon benutzt wird.
**/
{
	       int dsqlstatus;

	       DbClass.sqlin ((short *) &mdn, 1, 0); 
	       DbClass.sqlin ((short *) &fil, 1, 0); 
	       DbClass.sqlin ((long *)  &kun, 2, 0); 
	       DbClass.sqlin ((long *)  &kopt_txt, 2, 0); 
		   dsqlstatus = DbClass.sqlcomm ("select ls from lsk "
			                             "where mdn = ? "
										 "and fil = ? "
										 "and kun != ? "
										 "and kopf_txt = ?");
		   if (dsqlstatus == 0) return TRUE;
		   return FALSE;
}


int KopfTexte (void)
/**
Kopftexte erfassen.
**/
{
	       long nr_nr;
           long text_nr;

           nr_nr = auto_nr.nr_nr;
		   text_nr = lsk.kopf_txt;
		   if (text_nr == 0l)
		   {
		      while (TRUE)
			  {
			     text_nr = KopfTextGen ();
				 if (text_nr == 0l) return 0l;
			     if (KopfTextExist (lsk.mdn, lsk.fil, lsk.kun, text_nr) == FALSE) 
				 {
				                   break;
				 }
			  }

              auto_nr.nr_nr = 0;
               
//        Die Transtion wird an dieser Stelle geschlossen.

              FormToAuf (0);
              lsk.delstatus = -1;
			  lsk.rech = 0;
              ls_class.update_lsk (atoi (mdn), atoi (fil), lsk.ls);
              FreeAufNr = FALSE; 
              commitwork ();

              if (lsk.ls != nr_nr)
			  {
                       beginwork ();
					   if (lutz_mdn_par)
					   {
							   sprintf (mdn, "%hd", 0);
 						       sprintf (fil, "%hd", 0);
					   }
// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "ls",  nr_nr);
                       commitwork ();
					   sprintf (mdn, "%hd", lsk.mdn);
					   sprintf (fil, "%hd", lsk.fil);
			  }
              beginwork ();
              Lock_Lsk ();
		   }
           lsk.kopf_txt = text_nr;

           set_fkt (NULL, 9);
           SetFkt (9, leer, 0);
           set_fkt (NULL, 10);
           SetFkt (10, leer, 0);
           set_fkt (NULL, 11);
           SetFkt (11, leer, 0);
           lstxtlist.SethMainWindow (mamain1);
           lstxtlist.SetListLines (12);
//           lstxtlist.SetTextMetric (&tm);
		   EnableWindow (lspListe.GetMamain1 (), FALSE);
           lstxtlist.EnterLsp ("Kopftext", text_nr);
           text_nr = ls_txt.nr;
		   lsk.kopf_txt = ls_txt.nr; 
		   EnableWindow (lspListe.GetMamain1 (), TRUE);
           SetActiveWindow (mamain1);
           SetCurrentFocus (currentfield);
    
           set_fkt (dokey5, 5);
           set_fkt (doliste, 6);
           set_fkt (dokey12, 12);
           set_fkt (NULL, 7);
           SetFkt (6, liste, KEY6);
           SetFkt (7, leer, 0);
           set_fkt (KopfTexte, 10);
           set_fkt (FussTexte, 11);
           SetFkt (10, kopftext, KEY10);
           SetFkt (11, fusstext, KEY11);

           if (lspListe.GetPosanz () > 0)
		   {
			   if (KomplettMode < 2)
			   {
		             set_fkt (SetKomplett, 8);
                     SetFkt (8, komplett, KEY8);
			   }
		   }
		   else
		   {
               set_fkt (LastBest, 8);
               SetFkt (8, bestellung, KEY8);
		   }
           return 0;
}


int FussTexte (void)
/**
Fusstexte erfassen.
**/
{
           long nr_nr;
           long text_nr;
           extern short sql_mode;


           nr_nr = auto_nr.nr_nr;

           if (lsk.fuss_txt == 0)
           {
                sql_mode = 1;
                dsqlstatus = AutoClass.nvholid (0, 0, "ls_txt");
				if (dsqlstatus == -1)
				{
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"ls_txt\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
				}
					        
                if (dsqlstatus == 100)
                {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  "ls_txt",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (0,
                                                        0,
                                                        "ls_txt");
                           }
                }
                sql_mode = 0;
                if (auto_nr.nr_nr == 0l)
                {
                    disp_mess ("Es konnte keine text-Nr generiert werden", 2);
                    return 0;
                }

                text_nr = auto_nr.nr_nr;

                auto_nr.nr_nr = 0;
               
//        Die Transtion wird an dieser Stelle geschlossen.

                 FormToAuf (0);
                 lsk.delstatus = -1;
			     lsk.rech = 0;
                 ls_class.update_lsk (atoi (mdn), atoi (fil), lsk.ls);
                 commitwork ();

                 if (lsk.ls != nr_nr)
                 {
                       beginwork ();
					   if (lutz_mdn_par)
					   {
							   sprintf (mdn, "%hd", 0);
 						       sprintf (fil, "%hd", 0);
					   }
// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "ls",  nr_nr);
                       commitwork ();
					   sprintf (mdn, "%hd", lsk.mdn);
					   sprintf (fil, "%hd", lsk.fil);
                 }
                 beginwork ();
                 Lock_Lsk ();
                 lsk.fuss_txt = text_nr;
           }
           else
           {
                  text_nr = lsk.fuss_txt;
           }

           set_fkt (NULL, 9);
           SetFkt (9, leer, 0);
           set_fkt (NULL, 10);
           SetFkt (10, leer, 0);
           set_fkt (NULL, 11);
           SetFkt (11, leer, 0);
           lstxtlist.SethMainWindow (mamain1);
           lstxtlist.SetListLines (12);
//         lstxtlist.SetTextMetric (&tm);
		   EnableWindow (lspListe.GetMamain1 (), FALSE);
           lstxtlist.EnterLsp ("Fu�text", text_nr);

           text_nr = ls_txt.nr;
		   lsk.fuss_txt = ls_txt.nr; 
		   EnableWindow (lspListe.GetMamain1 (), TRUE);
           SetActiveWindow (mamain1);
           SetCurrentFocus (currentfield);
    
           set_fkt (dokey5, 5);
           set_fkt (doliste, 6);
           set_fkt (dokey12, 12);
           set_fkt (NULL, 7);
           SetFkt (6, liste, KEY6);
           SetFkt (7, leer, 0);
           set_fkt (KopfTexte, 10);
           set_fkt (FussTexte, 11);
           SetFkt (10, kopftext, KEY10);
           SetFkt (11, fusstext, KEY11);
           if (lspListe.GetPosanz () > 0)
		   {
			   if (KomplettMode < 2)
			   {
		             set_fkt (SetKomplett, 8);
                     SetFkt (8, komplett, KEY8);
			   }
		   }
		   else
		   {
               set_fkt (LastBest, 8);
               SetFkt (8, bestellung, KEY8);
		   }
           return 0;
}


int setkey10_11 (void)
{
       set_fkt (KopfTexte, 10);
       set_fkt (FussTexte, 11);
       SetFkt (10, kopftext, KEY10);
       SetFkt (11, fusstext, KEY11);
       return 0;
}

static int BreakKomm (void)
{
       break_enter ();
       return 1;
}

static char kommdat [12];

static BOOL KommdatOk (void)
/**
Abweichung beim Kommisionierdatum pruefen.
**/
{       

	   char ldatum [12];
	   long dat_von, dat_bis, lidat;

	   if (strcmp (kommdat, "01.01.1900") <= 0)
	   {
		   strcpy (kommdat, ldat);
	   }
	   sysdate (ldatum);
	   dat_von = dasc_to_long (ldatum) - ldatdiffminus;
	   dat_bis = dasc_to_long (ldatum) + ldatdiffplus;
	   lidat = dasc_to_long (kommdat);

	   if ((lidat < dat_von) || (lidat > dat_bis))
	   {
			 disp_mess ("Abweichung beim Kommisionierdatum ist zu gro�", 2);
		     SetCurrentField (currentfield);
			 return FALSE;
	   }
	   return TRUE;
}


static int testkommwert (void)
{
        long dat1;
        long dat2;
        char datum [12];

        if (syskey == KEY5 || syskey == KEY12)
        {
            break_enter ();
            return 0;
        }
		if (KommdatOk () == FALSE)
        {
            SetCurrentField (currentfield);
            return 0;
        }
        sysdate (datum);
        dat1 = dasc_to_long (datum);
        dat2 = dasc_to_long (kommdat);
        if (dat2 < dat1)
        {
            print_mess (2,"Das Kommisionier-Datum ist zu klein");
            SetCurrentField (currentfield);
            return 0;
        }
        return 1;
}
       


void InputKommdat (void)
/**
Kommisionierdatum eingeben.
**/
{
       HWND KommDat;
       int end_break;
       int currents;

       static ITEM iOK     ("OK",     "     OK     ", "", 0);
       static ITEM ikommdat ("komm_dat", kommdat,
                             "Kommisionierdatum :", 0);

       static field _fkommdat [] = {
           &ikommdat,     11, 0, 1, 1, 0, "dd.mm.yyyy", 
                                           EDIT,
                                           0,
                                           testkommwert,
                                           0,
           &iOK,          12, 0, 3, 5, 0, "", 
		                                   BUTTON,
										   0,0, KEY12,
           &iCancel,      12, 0, 3,19, 0, "", 
		                                   BUTTON,
										   0,0, KEY5};

        static form fkommdat = {3, 0, 0, _fkommdat, 
               0, 0, 0, 0, NULL};

        save_fkt (5);
        save_fkt (6);
        save_fkt (7);
        save_fkt (8);
        save_fkt (9);
        save_fkt (10);
        save_fkt (11);
        save_fkt (12);

        set_fkt (BreakKomm, 5);
        set_fkt (BreakKomm, 8);


        currents = currentfield;
        end_break = GetBreakEnd ();
        break_end ();
        EnableWindows (mamain1, FALSE);
        EnableWindow (lspListe.GetMamain1 (), FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_DLGFRAME);
        KommDat = OpenWindowChC (5, 36, 10, 20, hMainInst,
                   "");
        strcpy (kommdat, kdat);
        enter_form (KommDat, &fkommdat, 0, 0);
        if (syskey != KEY5)
        {
            strcpy (kdat, kommdat);
        }

        EnableWindow (lspListe.Getmamain3 (), TRUE);
        EnableWindows (mamain1, TRUE);
        DestroyWindow (KommDat);
        SetBreakEnd (end_break);
        SetAktivWindow (mamain1);
        SetActiveWindow (mamain1);
        currentfield = currents;
        SetCurrentFocus (currentfield + 1);

        restore_fkt (5);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        restore_fkt (9);
        restore_fkt (10);
        restore_fkt (11);
        restore_fkt (12);
}


static char fixdat [12];

static int testfixwert (void)
{
        long dat1;
        long dat2;
        char datum [12];

        if (syskey == KEY5 || syskey == KEY12)
        {
            break_enter ();
            return 0;
        }
/*
		if (KommdatOk () == FALSE)
        {
            SetCurrentField (currentfield);
            return 0;
        }
*/
        sysdate (datum);
        dat1 = dasc_to_long (datum);
        dat2 = dasc_to_long (fixdat);
/*
        if (dat2 < dat1)
        {
            print_mess (2,"Das Fix-Datum ist zu klein");
            SetCurrentField (currentfield);
            return 0;
        }
*/
        return 1;
}
       

void InputFixdat (void)
/**
Kommisionierdatum eingeben.
**/
{
       HWND FixDat;
       int end_break;
       int currents;

       static ITEM iOK     ("OK",     "     OK     ", "", 0);
       static ITEM ifixdat ("fix_dat", fixdat,
                             "Fixes Datum :", 0);

       static field _ffixdat [] = {
           &ifixdat,     11, 0, 1, 1, 0, "dd.mm.yyyy", 
                                           EDIT,
                                           0,
                                           testfixwert,
                                           0,
           &iOK,          12, 0, 3, 5, 0, "", 
		                                   BUTTON,
										   0,0, KEY12,
           &iCancel,      12, 0, 3,19, 0, "", 
		                                   BUTTON,
										   0,0, KEY5};

        static form ffixdat = {3, 0, 0, _ffixdat, 
               0, 0, 0, 0, NULL};

        save_fkt (5);
        save_fkt (6);
        save_fkt (7);
        save_fkt (8);
        save_fkt (9);
        save_fkt (10);
        save_fkt (11);
        save_fkt (12);

        set_fkt (BreakKomm, 5);
        set_fkt (BreakKomm, 8);


        currents = currentfield;
        end_break = GetBreakEnd ();
        break_end ();
        EnableWindows (mamain1, FALSE);
        EnableWindow (lspListe.GetMamain1 (), FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_DLGFRAME);
        FixDat = OpenWindowChC (5, 36, 10, 20, hMainInst,
                   "");
        strcpy (fixdat, fix_dat);
        enter_form (FixDat, &ffixdat, 0, 0);
        if (syskey != KEY5)
        {
            strcpy (fix_dat, fixdat);
        }

        EnableWindow (lspListe.Getmamain3 (), TRUE);
        EnableWindows (mamain1, TRUE);
        DestroyWindow (FixDat);
        SetBreakEnd (end_break);
        SetAktivWindow (mamain1);
        SetActiveWindow (mamain1);
        currentfield = currents;
        SetCurrentFocus (currentfield + 1);

        restore_fkt (5);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        restore_fkt (9);
        restore_fkt (10);
        restore_fkt (11);
        restore_fkt (12);
}

static int SearchPos = 8;
static int OKPos = 9;
static int CAPos = 10;

struct SADR
{
	  long adr;
	  char adr_krz [17];
	  char adr_nam1 [37];
	  char adr_nam2 [37];
	  char pf [17];
	  char plz [9];
	  char str [37];
	  char ort1 [37];
	  char ort2 [37];
	  short adr_typ;
};

static struct SADR *sadrtab = NULL;
static struct SADR sadr;
static int idx;
static long adranz;
static CHQEX *Query;
// static CHQ *Query;
static HWND adrwin;
static short adr_typ;


int SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   int i;
	   int len;

	   if (sadrtab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < adranz; i ++)
	   {
		   len = min (16, strlen (sebuff));
		   if (strupcmp (sebuff, sadrtab[i].adr_krz, len) == 0) break;
	   }
	   if (i == adranz) return 0;
	   Query->SetSel (i);
	   return 0;
}

int ReadAdr (char *adr_name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;

	  if (sadrtab) 
	  {
		  delete sadrtab;
		  sadrtab = NULL;
	  }

      clipped (adr_name);
      if (strlen (adr_name) == 1 &&
          adr_name[0] == ' ')
      {
          adr_name[0] = 0;
      }
	  adranz = 0;
	  sprintf (buffer, "select count (*) from adr where adr_krz "
		            "matches \"%s*\" and adr_typ between 20 and 25", adr_name);
      DbClass.sqlout ((int *) &adranz, 2, 0);
	  DbClass.sqlcomm (buffer);
	  if (adranz == 0) return 0;

	  sadrtab = new struct SADR [adranz + 2];
	  sprintf (buffer, "select adr, adr_krz, "
		               "adr_nam1, adr_nam2, pf, plz, "
					   "str, ort1, ort2, adr_typ from adr where adr_krz "
		               "matches \"%s*\"  and adr_typ between 20 and 25 "
					   "order by adr_nam1", adr_name);
      DbClass.sqlout ((long *) &sadr.adr, 2, 0);
      DbClass.sqlout ((char *) sadr.adr_krz, 0, 37);
      DbClass.sqlout ((char *) sadr.adr_nam1, 0, 37);
      DbClass.sqlout ((char *) sadr.adr_nam2, 0, 37);
      DbClass.sqlout ((char *) sadr.pf, 0, 17);
      DbClass.sqlout ((char *) sadr.plz, 0, 9);
      DbClass.sqlout ((char *) sadr.str, 0, 37);
      DbClass.sqlout ((char *) sadr.ort1, 0, 37);
      DbClass.sqlout ((char *) sadr.ort2, 0, 37);
      DbClass.sqlout ((short *) &sadr.adr_typ, 1, 0);
	  cursor = DbClass.sqlcursor (buffer);
	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
	  {
          if (TestDivAdr)
          {
                  if (sadr.adr < sadr_von) continue;  
                  if (sadr.adr > sadr_bis) continue;  
          }
		  memcpy (&sadrtab[i], &sadr, sizeof (sadr));
 	      sprintf (buffer, " %-8ld \"%-36s\" \"%-36s\"", sadr.adr, sadr.adr_nam1, sadr.ort1); 
	      Query->InsertRecord (buffer);
		  i ++;
	  }
	  DbClass.sqlclose (cursor);
	  return 0;
}

static void SearchAdr (void)
{
  	  int cx, cy;
	  char buffer [256];
	  form *scurrent;

	  scurrent = current_form;
	  idx = -1;
      cx = 80;
      cy = 20;
      Query = new CHQEX (cx, cy, "Name", "");
//      Query = new CHQ (cx, cy, "Name", "");
      Query->OpenWindow (hMainInst, hMainWindow);
	  sprintf (buffer, " %9s %36s %36s", "1", "1", "1"); 
	  Query->VLines (buffer, 0);
	  EnableWindow (hMainWindow, FALSE);
	  EnableWindow (adrwin, FALSE);

	  sprintf (buffer, " %-8s %-36s %-36s", "AdrNr", "Name", "Ort"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadAdr);
	  Query->SetSearchLst (SearchLst);
	  Query->ProcessMessages ();
      idx = Query->GetSel ();
	  EnableWindow (hMainWindow, TRUE);
	  EnableWindow (adrwin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (adrwin);
	  if (idx == -1) return;
	  memcpy (&sadr, &sadrtab[idx], sizeof (sadr));
	  if (sadrtab) delete sadrtab;
	  sadrtab = NULL;
	  current_form = scurrent;
	  if (syskey == KEY5) return;
	  sprintf (current_form->mask[0].item->GetFeldPtr (), "%ld", sadr.adr); 
	  sprintf (current_form->mask[1].item->GetFeldPtr (), "%s", sadr.adr_nam1); 
	  sprintf (current_form->mask[2].item->GetFeldPtr (), "%s", sadr.adr_nam2); 
	  sprintf (current_form->mask[3].item->GetFeldPtr (), "%s", sadr.pf); 
	  sprintf (current_form->mask[4].item->GetFeldPtr (), "%s", sadr.str); 
	  sprintf (current_form->mask[5].item->GetFeldPtr (), "%s", sadr.plz); 
	  sprintf (current_form->mask[6].item->GetFeldPtr (), "%s", sadr.ort1); 
	  sprintf (current_form->mask[7].item->GetFeldPtr (), "%s", sadr.ort2); 
	  adr_typ = sadr.adr_typ;
	  display_form (adrwin, current_form);
}


static int testadr (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY9 :
                       if (TestDivAdr < 2)
                       {
 					        SearchAdr ();
                       }
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
                case KEYCR :
					   if (currentfield == GetItemPos (current_form, "OK"))
					   {
                             syskey = KEY12;
                             break_enter ();
                             return 1;
					   }
					   else if (currentfield == GetItemPos (current_form, "CA"))
					   {
                             syskey = KEY5;
                             break_enter ();
                             return 1;
					   }
					   else if (currentfield == GetItemPos (current_form, "Search"))
					   {
                             syskey = KEY9;
							 SearchAdr ();
                             return 1;
					   }
         }
        return 0;
}


void AdrInput (long adr_nr)
/**
Eingabemaske fuer Adresse diverser Kunde.
**/
{
        HANDLE hMainInst;
		HWND hWnd;

        static char adrval[41];
        static char adrz1val [41];
        static char adrz2val [41];
        static char pfval [41];
        static char strval [41];
        static char plzval [41];
        static char ort1val [41];
        static char ort2val [41];

        static ITEM iOK     ("OK",     "     OK     ", "", 0);
        static ITEM iCA     ("CA",     "  Abbrechen ", "", 0);
        static ITEM iSearch ("Search", "    Suchen  ", "", 0);


        static ITEM iadr ("adr", 
                           adrval, 
                             "Adress-Nr        ", 
                           0);

        static ITEM iadrz1 ("adr_nam1", 
                                adrz1val, 
                             "Adressz. 1       ", 
                                   0);
        static ITEM iadrz2 ("adr_nam1", 
                                adrz2val, 
                             "Adressz. 2       ", 
                                   0);
        static ITEM ipf ("pf", 
                             pfval, 
                             "Postfach         ", 
                             0);

        static ITEM istr ("str", 
                            strval, 
                             "Strasse          ", 
                            0);
        static ITEM iplz ("plz", 
                            plzval, 
                             "Postleitzahl     ", 
                             0);
        static ITEM iort1 ("ort1", 
                                ort1val, 
                             "Ort 1.Zei        ", 
                                   0);
        static ITEM iort2 ("ort2", 
                                ort2val, 
                             "Ort 2.Zei        ", 
                                   0);


        static field _adrform[] = {
           &iadr,     9, 0, 1,1, 0, "%8d", NORMAL, 0, 0,0,
           &iadrz1,  37, 0, 2,1, 0, "",    NORMAL, 0, testadr,0,
           &iadrz2,  37, 0, 3,1, 0, "",    NORMAL, 0, testadr,0,
           &ipf,     16, 0, 4,1, 0, "",    NORMAL, 0, testadr,0,
           &istr,    37, 0, 5,1, 0, "",    NORMAL, 0, testadr,0,
           &iplz,     7, 0, 6,1, 0, "",    NORMAL, 0, testadr,0,
           &iort1,   37, 0, 7,1, 0, "",    NORMAL, 0, testadr,0,
           &iort2,   37, 0, 8,1, 0, "",    NORMAL, 0, testadr,0,
           &iSearch, 15, 0,10, 5, 0, "", BUTTON, 0, testadr,KEY9,
           &iOK,     15, 0,10,22, 0, "", BUTTON, 0, testadr,KEY12,
           &iCA,     15, 0,10,39, 0, "", BUTTON, 0, testadr,KEY5,
		};

        static form adrform = {11, 0, 0, _adrform, 
			                    0, 0, 0, 0, NULL};
        

		int savefield;
		form *savecurrent;

        if (TestDivAdr == 2)
        {
            _adrform[8].attribut = REMOVED;
        }
		adr_typ = _adr.adr_typ;
        hWnd = AktivWindow;
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testadr, 5);
        set_fkt (testadr, 9);
        set_fkt (testadr, 11);
        set_fkt (testadr, 12);

 		sprintf (adrval, "%ld", adr_nr);
		strcpy (adrz1val,_adr.adr_nam1);
		strcpy (adrz2val,_adr.adr_nam2);
		strcpy (pfval,_adr.pf);
		strcpy (strval,_adr.str);
		strcpy (plzval,_adr.plz);
		strcpy (ort1val,_adr.ort1);
		strcpy (ort2val,_adr.ort2);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        adrwin = OpenWindowChC (13, 62, 9, 10, hMainInst,
                               "Anschrift : Diverser Kunde");
		EnableWindow (lspListe.GetMamain1 (), FALSE);
		EnableWindow (hMainWindow, FALSE);
        SetButtonTab (TRUE);
        enter_form (adrwin, &adrform, 0, 0);

        SetButtonTab (FALSE);
        CloseControls (&adrform);
		EnableWindow (lspListe.GetMamain1 (), TRUE);
		EnableWindow (hMainWindow, TRUE);
        DestroyWindow (adrwin);
		current_form = savecurrent;
		currentfield = savefield;
        restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (9);
		restore_fkt (11);
		restore_fkt (12);

		if (syskey == KEY5) return;

		adr_nr = atol (adrval);
		lsk.adr = adr_nr;
		_adr.adr = lsk.adr;
		kun.adr2 = adr_nr;
		strcpy (_adr.adr_nam1, adrz1val);
		strcpy (_adr.adr_nam2, adrz2val);
		strcpy (_adr.pf,       pfval);
		strcpy (_adr.str,      strval);
		strcpy (_adr.plz,      plzval);
		strcpy (_adr.ort1,     ort1val);
		strcpy (_adr.ort2,     ort2val);
		_adr.adr_typ = adr_typ;
		adr_class.dbupdate ();
		strcpy (lsk.adr_nam1, adrz1val);
		strcpy (lsk.adr_nam2, adrz2val);
		strcpy (lsk.pf,       pfval);
		strcpy (lsk.str,      strval);
		strcpy (lsk.plz,      plzval);
		strcpy (lsk.ort1,     ort1val);

}

void fillsadr (void)
/**
Gesch�tzte Adressbereiche f�llen.
**/
{
	  static BOOL adrber_ok = FALSE;
	  int dsqlstatus;
	  
	  if (adrber_ok) return;

	  adrber_ok = TRUE;
	  memcpy (&ptabn, &ptabn_null, sizeof (ptabn));
      dsqlstatus = ptab_class.lese_ptab ("adr", "1");
	  if (dsqlstatus) 
	  {
		  return;
	  }
	  adr_von = atol (ptabn.ptwer1);
	  adr_bis = atol (ptabn.ptwer2);
	  memcpy (&ptabn, &ptabn_null, sizeof (ptabn));
      dsqlstatus = ptab_class.lese_ptab ("adr", "2");
	  if (dsqlstatus) 
	  {
		  return;
	  }
	  sadr_von = atol (ptabn.ptwer2) + 1;
	  sadr_bis = adr_bis;
}

long GenAdr (void)
/**
Adresse fuer diverse Kunden generieren.
**/
{
           extern short sql_mode;

           sql_mode = 1;
//           dsqlstatus = AutoClass.nvholid (0, 0, "adr");
           dsqlstatus = AutoClass.nvholidk2 (0, 0, "adr");
		   if (dsqlstatus == -1)
		   {
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"adr\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
			}
					        
           if (dsqlstatus == 100)
           {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  "adr",
                                                  (long) sadr_von,
                                                  (long) sadr_bis,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
//                                  dsqlstatus = AutoClass.nvholid (0,
                                  dsqlstatus = AutoClass.nvholidk2 (0,
                                                        0,
                                                        "adr");
                           }
            }
            sql_mode = 0;
/*
            if (auto_nr.nr_nr == 0l)
            {
                    disp_mess ("Es konnte keine Adress-Nr generiert werden", 2);
                    return 0l;
            }
*/

            return auto_nr.nr_nr;

}

long AdrExist (long adr_nr)
/**
Testen, ob die Adress schon existiert.
**/
{
	         int dsqlstatus;

	         DbClass.sqlin ((long *) &adr_nr, 2, 0);
			 dsqlstatus = DbClass.sqlcomm ("select adr from adr where adr = ?");
			 if (dsqlstatus == 0) return TRUE;
			 return FALSE;
}

static BOOL AdrGen (long kun_nr)
/**
Test, ob eine ADressnummer generiert werden soll.
**/
{

	        if (adr_nr == 0l)
			{
                   return TRUE;
			}
            else if (adr_nr < sadr_von)
            {
                   return TRUE;
            }
            else if (adr_nr > sadr_bis)
            {
                   return TRUE;
            }
			return FALSE;
}
	        

int InputAdr (void)
/**
Adresse fuer diverse Kunden eingeben.
**/
{
            long nr_nr;
			int count;


		    fillsadr ();
			gen_div_adr = AdrGen (atol (kuns));
		    if (gen_div_adr == FALSE)
			{
                    dsqlstatus = adr_class.lese_adr (adr_nr);
					if (dsqlstatus == 100)
					{
                             dsqlstatus = adr_class.lese_adr (kun.adr2);
					}
			        AdrInput (adr_nr);
                    SetCurrentFocus (currentfield);
                    return 0;
			}
  		    adr_nr = 0l;
			count = 0;
            nr_nr = auto_nr.nr_nr;
			while (TRUE)
			{
                    adr_nr = GenAdr ();
					if (adr_nr == 0l)
					{
                        count ++;
						Sleep (5);
					}
                    if (adr_nr < sadr_von)
                    {
                        continue;
					}
                    if (adr_nr > sadr_bis)
                    {
                        continue;
					}
					else if (AdrExist (adr_nr) == FALSE)
					{
						break;
					}
			}

            if (auto_nr.nr_nr == 0l)
            {
                    disp_mess ("Es konnte keine Adress-Nr generiert werden", 2);
                    return 0;
            }

            adr_nr = auto_nr.nr_nr;

            auto_nr.nr_nr = 0;
               
//        Die Transakttion wird an dieser Stelle geschlossen.

            FormToAuf (0);
            ls_class.update_lsk (atoi (mdn), atoi (fil), lsk.ls);
            FreeAufNr = FALSE; 
            commitwork ();

            if (lsk.ls != nr_nr)
            {
                       beginwork ();

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "ls",  nr_nr);
                       commitwork ();
            }
            beginwork ();
//            lsk.adr = adr_nr;
			AdrInput (adr_nr);
            SetCurrentFocus (currentfield);
    	    gen_div_adr = FALSE;
            return 0;
}


int saveldat (void)
/**
Lieferdatum merken.
**/
{
      if (IsAng)
	  {
	        GetLieferdat ();
			IsAng = FALSE;
	  }
	  strcpy (akt_ldat, ldat);
	  if (_adr.adr_typ == 25)
	  {
		  gen_div_adr = TRUE;
          set_fkt (InputAdr, 9);
		  SetFkt (9, adresse, KEY9);
	  }
	  return 0;
}

int testkommdat (void)
/**
Test, ob ein abweichendes Kommisionierdatum eingegeben werden soll.
**/
{

	  if (syskey != KEYCR) return 0;
      set_fkt (NULL, 9);
	  SetFkt (9, leer, NULL);
	  if (abw_komm == -1)
      {
               abw_komm = 0;
               memcpy (&sys_par, &sys_par_null, 
                   sizeof (struct SYS_PAR));
               strcpy (sys_par.sys_par_nam,"abw_komm");
               if (sys_par_class.dbreadfirst () == 0)
               {
                       if (atoi (sys_par.sys_par_wrt)) 
                       {
                             abw_komm = 1;
                       }
               }
      }

      if (abw_komm)
      {
          InputKommdat ();
      }
	  else
	  {
		   strcpy (kdat, ldat);
	  }
	  testfixdat ();
      return 0;
}


int testfixdat (void)
/**
Test, ob ein abweichendes Fixdatum eingegeben werden soll.
**/
{

	  if (syskey != KEYCR) return 0;
      set_fkt (NULL, 9);
	  SetFkt (9, leer, NULL);
/*
	  if (abw_fic == -1)
      {
               abw_komm = 0;
               memcpy (&sys_par, &sys_par_null, 
                   sizeof (struct SYS_PAR));
               strcpy (sys_par.sys_par_nam,"abw_komm");
               if (sys_par_class.dbreadfirst () == 0)
               {
                       if (atoi (sys_par.sys_par_wrt)) 
                       {
                             abw_komm = 1;
                       }
               }
      }
*/

      if (FixDat)
      {
          InputFixdat ();
      }
	  else
	  {
		   strcpy (fix_dat, ldat);
	  }
      return 0;
}


int testhtext (void)
{
      if (testkeys ()) return 0;

      set_fkt (SwitchArt, 10);
      SetFkt (10, aktart, KEY10);
      lspListe.SetSchirm ();
      return 0;
}

int doliste ()
{
	       if (UniqueKunError)
		   {
			   return 0;
		   }
	       if (ldatdiffOK () == FALSE)
		   {
			   SetCurrentField (currentfield);
			   SetCurrentFocus (currentfield);
			   return 0;
		   }
           break_enter ();
           return 0;
}

int SwitchArt (void)
/**
Switch zwischen Kundenartikel-Nummer und eigener Atikelnummer.
**/
{
         if (aktart == kunart)
         {
             aktart = eigart;
             lspListe.SetFieldAttr ("a_kun", DISPLAYONLY);
             lspListe.SetChAttr (1);
             lspListe.SetFieldAttr ("a_bz2", DISPLAYONLY);
         }
         else
         {
             aktart = kunart;
             lspListe.SetFieldAttr ("a_kun", REMOVED);
             lspListe.SetChAttr (0);
             lspListe.SetFieldAttr ("a_bz2", REMOVED);
         }
         SetFkt (10, aktart, KEY10);
         lspListe.DestroyWindows ();
         lspListe.ShowLsp (atoi (mdn) , atoi (fil), 
                             atol (lieferschein), atoi (kunfil),
                             atol (kuns), ldat);
         return 0;
}


int SwitchZei (void)
/**
Switch zwischen Kundenartikel-Nummer und eigener Atikelnummer.
**/
{
         if (aktzei == a_bz2_on)
         {
             aktzei = a_bz2_off;
             lspListe.SetFieldAttr ("a_bz2", DISPLAYONLY);
         }
         else
         {
             aktzei = a_bz2_on;
             lspListe.SetFieldAttr ("a_bz2", REMOVED);
         }
         SetFkt (7, aktzei, KEY7);
         lspListe.DestroyWindows ();
         lspListe.ShowLsp (atoi (mdn) , atoi (fil), 
                             atol (lieferschein), atoi (kunfil),
                             atol (kuns), ldat);
         return 0;
}


void DelLs (void)
/**
Auftrag l�schen.
**/
{
           ls_class.delete_ls (atoi (mdn), atoi (fil), lsk.ls);
           ls_class.delete_lspls (atoi (mdn), atoi (fil), lsk.ls);
           Mess.Message ("Satz wurde gel�scht");
           return;
}
           
int TouZTest (void)
/**
Testen, ob eine Tour eingegeben werden muss.
**/
{
	   int tou_pos; 
       char *touval;
	   int dsqlstatus;

       if (tour_zwang == FALSE) return TRUE;

       tou_pos    = GetItemPos (&aufform, "tou");
	   GetWindowText (aufform.mask[tou_pos].feldid,
		              aufform.mask[tou_pos].item->GetFeldPtr (),
					  9);
       touval    = aufform.mask[tou_pos].item->GetFeldPtr ();
       tou.tou = atol (touval);
       if (tou.tou < (long) tour_div)
       {
           tou.tou *= tour_div;
       }
       sprintf (touval, "%ld", tou.tou);
       tou.tou /= tour_div;
       if (tou.tou == 0l)
       {
                 dsqlstatus = 100;;
       }
	   else
	   {
                 dsqlstatus = Tour.dbreadfirst ();
	   }
	   if (dsqlstatus == 0) return TRUE;

	   disp_mess ("Eine Tour-Nr muss eingegeben werden", 2);
       SetCurrentField (tou_pos);
       SetCurrentFocus (tou_pos);
	   return FALSE;
}


int dokey12 ()
/**
Taste Key12 behandeln.
**/
{
  		   akt_tou = 0l;
           if (mench == 0)
           {
	             if (ldatdiffOK () == FALSE)
				 {
					 SetCurrentField (currentfield);
					 SetCurrentFocus (currentfield);
		             return 1;
				 }
                 if (bg_cc_par && zustell_par && 
                     getaufart () == -1)
                 {
                     print_mess (2, "Falsche Auftragsart");
					 SetCurrentField (currentfield);
					 SetCurrentFocus (currentfield);
		             return 1;
                 }
			     if (abw_komm == 0) strcpy (kdat, ldat);

/**********
 	             if (lspListe.GetPosanz () == 0)
				 {
					 disp_mess ("Es wurden kein Positionen erfasst", 2);
					 SetCurrentField (currentfield);
					 SetCurrentFocus (currentfield);
					 return (1);
				 }
********/
				 if (testkun0 () == FALSE)
				 {
					 return 1;
				 }
                 FormToAuf (2);
				 if (TouZTest () == FALSE) return 0;
				 testtou (0);
                 FormToAuf (2);
				 if (lsk.ls_stat == 0)
				 {
					 lsk.ls_stat = 2;
				 }
                 if (lspListe.GetMuster ())
                 {
                     lsk.auf_art = 99;
                 }
                 ls_class.update_lsk (atoi (mdn), atoi (fil), lsk.ls);
                 commitwork ();
                 SetMuster (FALSE);

                 if (lsk.ls != auto_nr.nr_nr)
                 {
                       beginwork ();
					   if (lutz_mdn_par)
					   {
							   sprintf (mdn, "%hd", 0);
 						       sprintf (fil, "%hd", 0);
					   }

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "ls",  auto_nr.nr_nr);
                       commitwork ();
                 }
                 InitAuf ();
                 break_enter ();
                 set_fkt (NULL, 12);
	             CloseControls (&Ffrei_txt2_bu);
                 Mess.Message ("Satz wurde geschrieben");
		         if (TransactMax > 0)
				 {
		               tcount ++;
		               if (tcount >= TransactMax)
					   {
					         RestartProcess ();
					   }
				 }
           }
           else if (mench == 2)
           {
			     if (lsk.ls_stat > 4)
				 {
					 disp_mess ("Der Lieferschein wurde schon freigegeben", 2);
                     commitwork ();
                     InitAuf ();
//                        break_enter ();

//		                lspListe.DestroyWindows (); 
//						CloseControls (&aufform);
						SetCurrentField (0);
						SetCurrentFocus (0);

                     return 0;
				 }
				 if (Lock_Lsk ())
				 {
					 BOOL loesche = FALSE;
					 if (lsk.ls_stat < 3) loesche = TRUE;
					 if (lsk.ls_stat >= 3)  //LOH-8
					 {
 	  					if (abfragejn (mamain1, "Vorsicht:  der Lieferschein wurde schon komplett gesetzt trotzdem l�schen ?", "N")) loesche = TRUE;
					 }
					 if (lsk.ls_stat >= 4)  //LOH-8
					 {
 	  					if (loesche == TRUE) if (abfragejn (mamain1, "!!Vorsicht!! :  der Lieferschein wurde schon gedruckt trotzdem l�schen ?", "N")) loesche = TRUE;
 	  					if (loesche == TRUE) if (abfragejn (mamain1, "!!!!Vorsicht!!!! :  soll der schon gedruckte Lieferschein wirklich gel�scht werden  ?", "N")) loesche = TRUE;
					 }
 	  			     if (loesche == TRUE) if (!abfragejn (mamain1, "Lieferschein l�schen ?", "N")) loesche = FALSE;
                      if (loesche == TRUE)
					  {
						    sprintf (DBLINE, "Start DelLs");
							FDEBUG->Write ();
							DelLs ();
						    sprintf (DBLINE, "DelLs OK");
							FDEBUG->Write ();
						    sprintf (DBLINE, "Start commitwork");
							FDEBUG->Write ();
							commitwork ();
						    sprintf (DBLINE, "Start OK");
							FDEBUG->Write ();
						    sprintf (DBLINE, "Start InitAuf"); 
							FDEBUG->Write ();
							InitAuf ();
						    sprintf (DBLINE, "InitAuf OK");
							FDEBUG->Write ();
//							break_enter ();
						    sprintf (DBLINE, "Start DestroyWindows");
							FDEBUG->Write ();
							lspListe.DestroyWindows (); 
						    sprintf (DBLINE, "DestroyWindows");
							FDEBUG->Write ();
						    sprintf (DBLINE, "Start CloseControls");
							FDEBUG->Write ();
							CloseControls (&aufform);
						    sprintf (DBLINE, "CloseControls OK");
							FDEBUG->Write ();
							SetCurrentField (0);
							SetCurrentFocus (0);

							set_fkt (NULL, 12);
							strcpy (lieferschein, "0");
							strcpy (auftrag, "0");
							display_form (mamain1, &aufformk, 0, 0);
							Mess.Message ("Satz wurde gel�scht");
						    sprintf (DBLINE, "Start LskChoiceDeleteRow");
							FDEBUG->Write ();
							LskChoiceDeleteRow (lsk.mdn, lsk.fil, lsk.ls);
						    sprintf (DBLINE, "LskChoiceDeleteRow OK");
							FDEBUG->Write ();
						    sprintf (DBLINE, "Start LskChoiceCurrentRow");
							FDEBUG->Write ();
							LskChoiceCurrentRow ();
						    sprintf (DBLINE, "LskChoiceCurrentRow OK");
							FDEBUG->Write ();
					 }
				 }
           }
           
           else if (mench == 3)
           {
                 AufKompl.K_Liste ();                  
           }
		   lsk.mdn = lsk.fil = 0;
		   lsk.kun = 0;
		   sprintf (kuns, "%ld", 0l);
		   sprintf (auftrag, "%ld", 0l);
           return 0;
}


/* Ende Eingabe Kopfdaten                                         */
/******************************************************************/

void tst_arg (char *arg)
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                            case 'u' :
                                     strcpy (InfoCaption, arg + 1);
                                     return;
                 }
          }
          return;
}



void rollbacklsk (void)
/**
delstatus in lsk auf 0 setzen.
Bei neuem Auftrag Satz loeschen.
**/
{
	    int ls_stat = 0;
		int dsqlstatus;

		DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		DbClass.sqlin ((short *) &lsk.fil, 1, 0);
		DbClass.sqlin ((short *) &lsk.ls, 2, 0);
		DbClass.sqlout ((short *) &ls_stat, 1, 0);
		dsqlstatus = DbClass.sqlcomm ("select ls_stat from lsk "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and ls = ?");
		if (dsqlstatus != 0) return;

		if (ls_stat == 0)
		{
  		           DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		           DbClass.sqlin ((short *) &lsk.fil, 1, 0);
		           DbClass.sqlin ((short *) &lsk.ls, 2, 0);
		           dsqlstatus = DbClass.sqlcomm ("select ls from lsp "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and ls = ?");

				   if (dsqlstatus == 0)
				   {
                             DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		                     DbClass.sqlin ((short *) &lsk.fil, 1, 0);
		                     DbClass.sqlin ((short *) &lsk.ls, 2, 0);
		                     dsqlstatus = DbClass.sqlcomm ("delete from lsp "
			                                               "where mdn = ? "
									                       "and fil = ? "
									                       "and ls = ?");
				   }
  		           DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		           DbClass.sqlin ((short *) &lsk.fil, 1, 0);
		           DbClass.sqlin ((short *) &lsk.ls, 2, 0);
		           dsqlstatus = DbClass.sqlcomm ("delete from lsk "
			                                     "where mdn = ? "
					                             "and fil = ? "
									             "and ls = ?");


		}
		else
		{
  		           DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		           DbClass.sqlin ((short *) &lsk.fil, 1, 0);
		           DbClass.sqlin ((short *) &lsk.ls, 2, 0);
		           dsqlstatus = DbClass.sqlcomm ("update lsk set delstatus = 0 "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and ls = ?");
		}
}

int dokey50 ()
/**
Aktion bei Taste F5
**/
{
        int dsqlstatus;
        
	    CloseControls (&Ffrei_txt2_bu);
        SetMuster (FALSE);
		if (mench == 0 ||
			mench == 2) 
		{
			        if ((FreeAufNr == FALSE) && (mench == 0) && inDaten)
					{
			               rollbacklsk ();
					}
					else
					{
                           rollbackwork ();
					}
		}
        if (mench == 0 && auto_nr.nr_nr && FreeAufNr)
        {
			    if (lutz_mdn_par)
				{
					sprintf (mdn, "%hd", 0);
					sprintf (fil, "%hd", 0);
				}
                beginwork ();
// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "ls",  auto_nr.nr_nr);
				FreeAufNr = 0;
                commitwork ();
        }
        break_enter ();
        DisplayAfterEnter (0);
        set_fkt (NULL, 12);
 	    lsk.mdn = lsk.fil = 0;
		lsk.kun = 0;
		sprintf (kuns, "%ld", 0l);
		sprintf (auftrag, "%ld", 0l);
        return 1;
}


int dokey2 ()
{
	LskChoicePriorRow ();
	return 0;
}

int dokey3 ()
{
	LskChoiceNextRow ();
	return 0;
}

int dokey5 ()
/**
Aktion bei Taste F5
**/
{
        
		akt_tou = 0l;
		if (mench == 0 && inDaten && atol (kuns) != 0l)
		{
                if (abfragejn (mamain1, "Lieferschein speichern ?", "J"))
				{
					return dokey12 ();
				}
		}
		return dokey50 ();

}


BOOL IsComboMsg (MSG *msg)
/**
Test, ob die Meldung fuer die Combobox ist.
**/
{
      if (HIWORD (msg->lParam) == CBN_DROPDOWN)
      {
                 opencombobox = TRUE;
                 return TRUE;
      }
      if (HIWORD (msg->lParam) == CBN_CLOSEUP)
      {
                 opencombobox = FALSE;
                 return TRUE;
      }

      if (msg->hwnd == hwndCombo1)
      {
                 return TRUE;
      }
      if (msg->hwnd == hwndTB)
      {
                 return TRUE;
      }

      return FALSE;
}

void FillCombo1 (void)
{
      int i;

      for (i = 0; Combo1[i]; i ++)
      {
           SendMessage (hwndCombo1, CB_ADDSTRING, 0,
                                   (LPARAM) Combo1 [i]);
      }
      SendMessage (hwndCombo1, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo1 [combo1pos]);
      lspListe.SetLines (combo1pos);
}

void FillCombo2 (void)
{
      int i;

      for (i = 0; Combo2[i]; i ++)
      {
           SendMessage (hwndCombo2, CB_ADDSTRING, 0,
                                   (LPARAM) Combo2 [i]);
      }
      SendMessage (hwndCombo2, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo2 [combo2pos]);
      SetComboMess (IsComboMsg);
}

static int StopProc (void)
{
    PostQuitMessage (0);
    return 0;
}


void DisableTB (void)
{
     ToolBar_SetState(hwndTB,IDM_STD,  TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,IDM_LIST, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,KEYLEFT,  TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,KEYRIGHT, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,IDM_FIND, TBSTATE_INDETERMINATE);
}

void EnableTB (void)
{
     ToolBar_SetState(hwndTB,IDM_STD,  TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,IDM_LIST, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,KEYLEFT,  TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,KEYRIGHT, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,IDM_FIND, TBSTATE_ENABLED);

     ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
     ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
}


BOOL GroupOK (void)
/**
Test, ob komplett gesetzt werden darf.
**/
{
	    long tou;

	    if (tou_def_zwang == FALSE) return TRUE;
        tou = atol (tour) / tour_div;
		if (tou == 0l) return TRUE;

		if (tou < atol (tou_def_von)) return FALSE;
		if (tou > atol (tou_def_bis)) return FALSE;
		return TRUE;
}


int EnterVk ()
{
        lspListe.EnterLdVk ();
        return 0;
}

int EnterPrVk ()
{
        lspListe.EnterPrVk ();
        return 0;
}


int EnterListe ()
{
	int ftyp;

    
	PrintButtons ();
	TestUniqueKun = FALSE;
    if (testkun0 () == FALSE) return 0;
	TestUniqueKun = TRUE;
	if (fen_par)
	{
           ftyp = AufKompl.SetFak_typ (mamain1);
		   if (ftyp != -1)
		   {
			   lsk.fak_typ = ftyp;
		   }
	}
	DlgWindow = NULL;
    EnableTB ();
    set_fkt (NULL, 4);
	EnableArrows (&aufform, FALSE);
    EnableMenuItem (hMenu, IDM_FIND,      MF_ENABLED);
    EnableMenuItem (hMenu, IDM_FONT,      MF_ENABLED);
    EnableMenuItem (hMenu, IDM_BASIS,     MF_ENABLED);
    EnableMenuItem (hMenu, IDM_POSTEXT,   MF_ENABLED);
    EnableMenuItem (hMenu, IDM_LDPR,      MF_ENABLED);
    EnableMenuItem (hMenu, IDM_VKPR,      MF_ENABLED);
    EnableMenuItem (hMenu, IDM_NEWPR,     MF_ENABLED);
    EnableMenuItem (hMenu, IDM_SORTPR,    MF_ENABLED);
    EnableMenuItem (hMenu, IDM_SORTART,   MF_ENABLED);
    EnableMenuItem (hMenu, IDM_PRIOR_ROW, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_NEXT_ROW,  MF_GRAYED);
    EnableMenuItem (hMenu, IDM_ENTER_AUF_ME,  MF_GRAYED);
    EnableMenuItem (hMenu, IDM_ENTER_LIEF_ME3,  MF_ENABLED);

    set_fkt (NULL, 9);
    SetFkt (9, leer, 0);
    set_fkt (NULL, 10);
    SetFkt (10, leer, 0);
	if (lutz_mdn_par)
	{
		if (atol (kunfil) == 0) 
		{
			lspListe.SetMdnProd (kun.kst);
		}
		else
		{
			lspListe.SetMdnProd (_fil.mdn);
		}
	}

    if (LdVkEnabled)
    {
         set_men (EnterVk, (unsigned char) 'L');
    }

    if (PrVkEnabled)
    {
         set_men (EnterPrVk, (unsigned char) 'V');
    }

/* Vor dem Aufruf der Liste Kopf abspeichern */

    FormToAuf (2);
    if (lsk.ls_stat == 0)
    {
		 lsk.ls_stat = 2;
	}

	lsk.delstatus = -1;
    ls_class.update_lsk (atoi (mdn), atoi (fil), lsk.ls);
	lsk.delstatus = 0;
	set_fkt (NULL, 2);
	set_fkt (NULL, 3);

    lspListe.EnterLsp (atoi (mdn) , atoi (fil), atol (lieferschein), atoi (kunfil),
                         atol (kuns), ldat);

    set_men (NULL, (unsigned char) 'L');
    set_men (NULL, (unsigned char) 'V');

    EnableMenuItem (hMenu, IDM_FIND, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_FONT, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_BASIS, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_POSTEXT, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_LDPR,    MF_GRAYED);
    EnableMenuItem (hMenu, IDM_VKPR,    MF_GRAYED);
    EnableMenuItem (hMenu, IDM_NEWPR,   MF_GRAYED);
    EnableMenuItem (hMenu, IDM_SORTPR,  MF_GRAYED);
    EnableMenuItem (hMenu, IDM_SORTART,  MF_GRAYED);
    EnableMenuItem (hMenu, IDM_PRIOR_ROW,  MF_ENABLED);
    EnableMenuItem (hMenu, IDM_NEXT_ROW,  MF_ENABLED);
    EnableMenuItem (hMenu, IDM_ENTER_AUF_ME,  MF_ENABLED);
    EnableMenuItem (hMenu, IDM_ENTER_LIEF_ME3,  MF_GRAYED);
	EnableArrows (&aufform, TRUE);
    DisableTB ();
	if (GroupOK ())
	{
            if (KomplettMode < 2) 
			{
				set_fkt (SetKomplett, 8);
                SetFkt (8, komplett, KEY8);
			}
	}
	set_fkt (dokey2, 2);
	set_fkt (dokey3, 3);
    PosOK = TRUE;
    return 0;
}

void PrintButtons (void)
{
         clipped (_adr.tel);
         if (strcmp (_adr.tel, " ") > 0)
         {
                CTelefon.bmp = btelefon;
                ActivateColButton (mamain1, 
                                  &buform, 1, 0, 1);
                EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
         }
         else
         {
                CTelefon.bmp = btelefoni;
                ActivateColButton (mamain1, 
                &buform, 1, -1, 1);
                EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
         }
         clipped (_adr.fax);
         if (strcmp (_adr.fax, " ") > 0)
         {
                ActivateColButton (mamain1, 
                                   &buform,      0, 0, 1);
                EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
         }
         else
         {
                ActivateColButton (mamain1, 
                                      &buform,      0, -1, 1);
                EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
         }
         ActivateColButton (mamain1, &buform, 2, 0, 1);
         EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
         MoveButtons ();
}


void MoveButtons (void)
/**
Buttons in mamain1 rechtsbuendig.
**/
{
     RECT rect;
     RECT rectb;
     TEXTMETRIC tm;
     HDC hdc;
     int i;
     int x, y;

     GetClientRect (mamain1, &rect); 
     hdc = GetDC (mamain1);
     GetTextMetrics (hdc, &tm);
     ReleaseDC (mamain1, hdc);

     for (i = 0; i < buform.fieldanz; i ++)
     {
		 if (buform.mask[i].feldid == NULL) continue;

         GetClientRect (buform.mask[i].feldid, &rectb );
         x = rect.right - rectb.right - 1;
		 if (BuFont.PosMode == 0)
		 {
                y = buform.mask[i].pos[0] * tm.tmHeight;
		 }
		 else
		 {
                y = buform.mask[i].pos[0];
		 }
         MoveWindow (buform.mask[i].feldid,
                                    x, y,
                                    rectb.right,
                                    rectb.bottom,
                                    TRUE);

		 buform.mask[i].pos[0] = y;
		 buform.mask[i].pos[1] = x;
		 buform.mask[i].length = (short) rectb.right;
		 buform.mask[i].rows   = (short) rectb.bottom;
     }
     if (buform.mask[0].feldid)
	 {
	     BuFont.PosMode = 1;
	 }
     CreateQuikInfos ();
}


int CalcMinusPos (int cy)
{
        TEXTMETRIC tm;
        SIZE size;
        HDC hdc;


        hdc = GetDC (ftasten);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (ftasten, hdc);
        cy -= 3 * tm.tmHeight;
        return cy;
}

int CalcPlusPos (int y)
{
        TEXTMETRIC tm;
        SIZE size;
        HDC hdc;


        hdc = GetDC (ftasten);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (ftasten, hdc);
        y += 1 * tm.tmHeight + 5;
        return y;
}

void MoveMamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;

     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;
     cy = frect.top - mrect.top - y - 50;
     if (ftasten2 == NULL)
     {
         y  = CalcPlusPos (y);
         cy = CalcMinusPos (cy);
     }
     MoveWindow (mamain1, x, y, cx, cy, TRUE);
     MoveFktBitmap ();
     MoveButtons ();
}


void Createmamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;


     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;


     cy = frect.top - mrect.top - y - 50;

     if (ftasten2 == NULL)
     {
         y  = CalcPlusPos (y);
         cy = CalcMinusPos (cy);
     }

     mamain1 = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "hStdWindow",
                              InfoCaption,
                              WS_CHILD |  
                              WS_VISIBLE,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
}

void ShowDaten (void)
/**
Daten fuer Auftrag anzeigen.
**/
{
        Mess.CloseMessage ();
        display_form (mamain1, &aufform, 0, 0);

        lspListe.ShowLsp (atoi (mdn) , atoi (fil), 
                             atol (lieferschein), atoi (kunfil),
                             atol (kuns), ldat);
        current_form = &aufformk;
        set_fkt (dokey12, 12);
        display_form (mamain1, &aufformk, 0, 0);

}

void MSleep (int tsec)
{
        MSG msg;  
        int i;

		for (i = 0; i < tsec; i ++)
		{
                  if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
                  {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                  }
                  Sleep (1);
        }
}



void eingabedaten (void)
/**
Artikel bearbeiten.
**/
{

       inDaten = 1;
	   EnableFullTax ();
       Mess.CloseMessage ();
	   akt_kun = 0l;
       ListeOK = TRUE;
	   int AfterList = FALSE;
       while (TRUE)
       {
                set_fkt (InfoKun, 4);
                set_fkt (doliste, 6);
                SetFkt (6, liste, KEY6);
                set_fkt (SwitchZei, 7);
                SetFkt (7, aktzei, KEY7);
                set_fkt (SwitchArt, 10);
                SetFkt (10, aktart, KEY10);
                set_fkt (dokey12, 12);
                no_break_end ();

                lspListe.ShowLsp (atoi (mdn) , atoi (fil), 
                             atol (lieferschein), atoi (kunfil),
                             atol (kuns), ldat);
				SetKunAttr ();
                DisplayAfterEnter (FALSE);
				currentfield = 0;
	            if ((syskey != KEY5) && AfterList && (listedirect > 0))
				{
		                 int fpos = GetItemPos (current_form, "htext");
		                 if (fpos >= 0)
						 {
				              SetCurrentField (fpos);
						 }
				}
				if (lsk.psteuer_kz > 0 && lspListe.ContainsService ())
				{
					EnableFullTax ();
				}
                enter_form (mamain1, &aufform, 0, 0);

/*
Zum Dauertest der Masken enter_form asukommentieren.


create_enter_form (mamain1, &aufform, 0, 0);
MSleep (100);
dokey5 ();
*/
                DisplayLines ();
// break;
                if (syskey == KEYESC || syskey == KEY5 ||
                    syskey == KEY12 || UniqueKunError)
                {
                                 DisplayAfterEnter (TRUE);
                                 break;
                }
                current_form = &aufform;
                lspListe.DestroyWindows (); 
                display_form (mamain1, &aufformk);
                display_form (mamain1, &aufform, 0, 0);
                EnterListe ();
				AfterList = TRUE;
                set_fkt (dokey5, 5);
                set_fkt (doliste, 6);
                set_fkt (NULL, 7);
                SetFkt (6, liste, KEY6);
                SetFkt (7, leer, 0);
                set_fkt (NULL, 9);
        }
        lspListe.DestroyWindows (); 
        CloseControls (&aufform);
        ListeOK = FALSE;
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        SetFkt (6, leer, 0);
        SetFkt (7, leer, 0);
        inDaten = 0;
        set_fkt (NULL, 8);
        SetFkt (8, leer, NULL);
		akt_tou = 0l;
        return;
}

int ShowFil (void)
/**
Auswahl ueber Filialen.
**/
{
	   _fil.mdn = atoi (mdn);
	   if (lutz_mdn_par)
	   {
                 fil_class.ShowAllBu (mamain1, 0);
	   }
	   else
	   {
                 fil_class.ShowAllBu (mamain1, 0, lsk.mdn);
	   }
       if (syskey == KEYESC || syskey == KEY5)
       {
           return 0;
       }
       kunfield = GetItemPos (&aufform, "kun");
       sprintf (kuns, "%d", _fil.fil);
       display_field (mamain1, &aufform.mask[kunfield], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;

}
    

int ChoiseKunLief0 (void)
{
       struct SKUNLIEF *skunlief;
       struct SLIEFKUN *sliefkun;
       SEARCHKUNLIEF SearchKunLief;
       SEARCHLIEFKUN SearchLiefKun;
       char query [512];

       LiefAdr = 0l;
       int kunfield = GetItemPos (&aufform, "kun");
       SearchKunLief.SetParams (hMainInst, mamain1);
       SearchKunLief.Setawin (hMainWindow);
       if (atol (kuns) != 0)
       {
             sprintf (query, " and kun = %ld", atol (kuns)),
             SearchKunLief.SetQuery (query);
       }
       if (SearchKunLief.TestRec (atoi (mdn)) != 0)
       {
             return 0;
       }
       SearchKunLief.Search (atoi (mdn), atoi (kuns));   //WAL-167  mit kuns
       skunlief = SearchKunLief.GetSkun ();
	   /*** WAL-167
       if (syskey == KEY5 || skunlief == NULL)
       {
            SetCurrentFocus (currentfield);
            display_field (mamain1, &aufform.mask[kunfield], 0, 0);
            if (atol (kuns) == 0l)
            {
                  syskey = KEY5;
            }
            return 0;
       }
	   **/

       if (atol (kuns) == 0l)
       {
            SearchLiefKun.SetParams (hMainInst, mamain1);
            SearchLiefKun.Setawin (hMainWindow);
            sprintf (query, " and adr = %ld", atol (skunlief->adr)),
            SearchLiefKun.SetQuery (query);
            SearchLiefKun.Search (atoi (mdn));
            sliefkun = SearchLiefKun.GetSkun ();
			/*** WAL-167  soll in diesem Fall nicht auf Kundennummer bleiben, 

            if (syskey == KEY5 || sliefkun == NULL)
            {
                  SetCurrentFocus (currentfield);
                  display_field (mamain1, &aufform.mask[kunfield], 0, 0);
                  if (atol (kuns) == 0l)
                  {
                         syskey = KEY5;
                  }
                  return 0;
            }
			**/
            sprintf (kuns, "%ld", atol (sliefkun->kun));
       }
       LiefAdr = atol (skunlief->adr);
       display_field (mamain1, &aufform.mask[kunfield], 0, 0);

       return 0;
}


int ChoiseKunLief (void)
{
       
       ChoiseKunLief0 ();
       if (syskey != KEY5)
       {
              lesekun0 ();
              int ldatfield = GetItemPos (&aufform, "lieferdat");
              if (ldatfield != -1)
              {
 	                  SetCurrentField (ldatfield);
                      SetCurrentFocus (ldatfield);
              }
//              PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       }
       return 0;
}

BOOL ShowKun ()
{
        struct SKUN *skun;
        SEARCHKUN SearchKun;
        char matchorg [256];
        char matchupper [256];
        char matchlower [256];
        char matchuplow [256];
        char query [512];

        if (strcmp (clipped (kuns), " ") > 0 &&
            strcmp (kuns, "*") != 0)
        {
	        strcpy (matchorg, kuns);
            strupcpy (matchupper, kuns);
            strlowcpy (matchlower, kuns);
            struplowcpy (matchuplow, kuns);
            sprintf (query, " and (kun_krz1 matches \"%s*\" "
                           "or kun_krz1 matches \"%s*\" "
                           "or kun_krz1 matches \"%s*\" "
                           "or kun_krz1 matches \"%s*\")",matchorg, 
                                                          matchupper,
                                                          matchlower,
                                                          matchuplow);
        }
        else
        {
            sprintf (query, " and kun_krz1 matches \"*\"");
        }

        SearchKun.SetQuery (query);
        SearchKun.SetParams (hMainInst, mamain1);
        SearchKun.Setawin (hMainWindow);
        SearchKun.SearchKun (lsk.mdn, NULL, NULL);
        if (syskey == KEY5)
        {
            SetCurrentFocus (currentfield); 
            return 0;
        }
        skun = SearchKun.GetSkun ();
        if (skun == NULL)
        {
            SetCurrentFocus (currentfield); 
            return 0;
        }
        strcpy (kuns, skun->kun);
        return lesekun0 ();
}


int QueryKun (void)
/**
Auswahl ueber Kunden.
**/
{
       int ret;

       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain1);
	   kun.mdn = atoi (mdn);
//	   kun.fil = atoi (fil);
	   kun.fil = 0;
       ret = QClass.querykun (mamain1);
       set_fkt (dokey5, 5);
       set_fkt (doliste, 6);
       SetFkt (6, liste, KEY6);
       SetFkt (7, aktzei, KEY7);
       SetFkt (9, auswahl, KEY9);
	   SetFkt (8, leer, NULL);
	   set_fkt (NULL, 8);
	   set_fkt (NULL, 12);

       if (atoi (kunfil))
       {
             set_fkt (ShowFil, 9);
       }
       else
       {
             set_fkt (QueryKun, 9);
       }

       if (GroupOK ())
       {
            if (KomplettMode < 2) 
			{
				set_fkt (SetKomplett, 8);
                SetFkt (8, komplett, KEY8);
			}
       }
       set_fkt (dokey12, 12);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       kunfield = GetItemPos (&aufform, "kun");
       sprintf (kuns, "%ld", kun.kun);
       if (KunLiefKz)
       {
           ChoiseKunLief ();
       }
       else
       {
          display_field (mamain1, &aufform.mask[kunfield], 0, 0);
          PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       }
       return 0;
}


int setkey9kun ()
/**
Auswahl fuer Kunde setzen.
**/
{
       LiefAdr = 0l; 
	   if (IsAng)
	   {
	        GetLieferdat ();
			IsAng = FALSE;
	   }
       akt_kun = atol (kuns);
       SetFkt (9, auswahl, KEY9);
       if (atoi (kunfil))
       {
             set_fkt (ShowFil, 9);
       }
       else
       {
             set_fkt (QueryKun, 9);
             if (KunLiefKz)
             {
                  SetFkt (8, kunlieftxt, KEY8);
                  set_fkt (ChoiseKunLief, 8);
             }
       }
       return 0;
}

int TouQuery (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;
	   int tou_bz_pos;
	   char *tou_bz;


       ret = QTClass.querytou (mamain1);
       set_fkt (dokey5, 5);
       set_fkt (doliste, 6);
       SetFkt (6, liste, KEY6);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       toufield = GetItemPos (&aufform, "tou");

       tou_bz_pos = GetItemPos (&aufform, "tou_bz");
       tou_bz = aufform.mask[tou_bz_pos].item->GetFeldPtr ();

	   SetCurrentField (toufield);
       sprintf (tour, "%ld", tou.tou * tour_div);
	   strcpy (tou_bz, tou.tou_bz);

       display_field (mamain1, &aufform.mask[toufield], 0, 0);
       display_field (mamain1, &aufform.mask[tou_bz_pos], 0, 0);
       SetCurrentFocus (currentfield);

       return 0;
}

int VertrQuery (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;
       int vertrfield;

       vertr.mdn = lsk.mdn;
//       vertr.fil = lsk.fil;
       vertr.fil = 0;
       ret = QVtrClass.queryvertr (mamain1);
       set_fkt (dokey5, 5);
       set_fkt (doliste, 6);
       SetFkt (6, liste, KEY6);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       vertrfield = GetItemPos (&aufform, "vertr");
       sprintf (vertrs, "%ld", vertr.vertr);
	   lsk.vertr = vertr.vertr;
       display_field (mamain1, &aufform.mask[vertrfield], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

static void FillAufArt  (char *buffer)
/**
Werte fuer Auswahl fuellen.
**/
{
         if (atoi (ptabn.ptwert) == 2)
         {
             strcpy (buffer, "NULL");
         }
         else if (atoi (ptabn.ptwert) == 3 &&
                  kun.zahl_art == 2)
         {
             strcpy (buffer, "NULL");
         }
         else
         {
             sprintf (buffer, "%3hd %-3s %s",
                              ptabn.ptlfnr, ptabn.ptwert,
				    		  ptabn.ptbez);
         }
}

int ShowAufArtEx (HWND hMainWindow, char *dest, char *item)
{
       char wert [80];
       SEARCHAART *SearchPtab;
	   HINSTANCE hMainInst;


       hMainInst = (HINSTANCE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
       SearchPtab = new SEARCHAART (item);
       SearchPtab->SetParams (hMainInst, hMainWindow); 
       SearchPtab->Setawin (hMainWindow);
                                                  
       SearchPtab->Search ();
       if (SearchPtab->GetKey (wert) == TRUE)
       {
           delete SearchPtab;
           strcpy (dest, wert);
		   return 0;
       }
       delete SearchPtab;
       return -1;
}




int ShowAufArt (void)
{

       int ret = ShowAufArtEx (hMainWindow, auf_art, "auf_art"); 
       SetCurrentFocus (currentfield);
       if (ret == -1)
       {
              return 0;
       }
	   return testaufart0 ();

       ptab_class.SetValuesProc (FillAufArt);
	   ptab_class.Show ("auf_art");
       ptab_class.SetValuesProc (NULL);
	   if (syskey == KEY5 || syskey == KEYESC)
	   {
		   return 0;
	   }
	   strcpy (auf_art, ptabn.ptwert);
	   return testaufart0 ();
}

int setkey9aufart ()
/**
Auswahl fuer Touren setzen.
**/
{
       SetFkt (9, ansehen, KEY9);
       set_fkt (ShowAufArt, 9);
       return 0;
}


int setkey9tou ()
/**
Auswahl fuer Touren setzen.
**/
{
       akt_tou = atol (tour);
       SetFkt (9, auswahl, KEY9);
       set_fkt (TouQuery, 9);
       set_fkt (InfoTou, 4);
       return 0;
}


int setkey9vertr ()
/**
Auswahl fuer Touren setzen.
**/
{
       akt_tou = atol (tour);
       vertr.mdn = lsk.mdn;
       vertr.fil = lsk.fil;
       SetFkt (9, auswahl, KEY9);
       set_fkt (VertrQuery, 9);
       set_fkt (InfoVertr, 4);
       return 0;
}

int lesetou (void)
/**
Tour lesen
**/
{
       int dsqlstatus;
       char *tou_bz;
       char *touval;
       int tou_pos;
       int tou_bz_pos;


       if (akt_tou == atol (tour)) return 0;

       tou_pos    = GetItemPos (&aufform, "tou");
       tou_bz_pos = GetItemPos (&aufform, "tou_bz");
       touval    = aufform.mask[tou_pos].item->GetFeldPtr ();
       tou_bz = aufform.mask[tou_bz_pos].item->GetFeldPtr ();
       tou.tou = atol (touval);
       if (tou.tou < tour_div)
       {
           tou.tou *= tour_div;
       }
       sprintf (touval, "%ld", tou.tou);
       tou.tou /= tour_div;
       if (tou.tou == 0l)
       {
           return 0;
       }
       dsqlstatus = Tour.dbreadfirst ();
       if (dsqlstatus == 0)
       {
           strcpy (tou_bz, tou.tou_bz);
       }
       return 0;
}


BOOL ldatdiffOK (void)
/**
Lieferdatum testen.
**/
{
		 char ldatum [12];
		 int ldpos;
		 long dat_von, dat_bis, lidat;

		 ldpos = GetItemPos (&aufform, "lieferdat");
		 if (ldpos > -1)
		 {
                display_field (mamain1, &aufform.mask[ldpos], 0, 0);
		 }
		 sysdate (ldatum);
		 dat_von = dasc_to_long (ldatum) - ldatdiffminus;
		 dat_bis = dasc_to_long (ldatum) + ldatdiffplus;
		 lidat = dasc_to_long (ldat);

		 if ((lidat < dat_von) || (lidat > dat_bis))
		 {
			 disp_mess ("Abweichung beim Lieferdatum ist zu gro�", 2);
		     SetCurrentField (currentfield);
			 return 0;
		 }
		 return KommdatOk ();
}


int testldat0 (void)
/**
Lieferdatum testen.
**/
{
         long tou;
		 int toupos;
		 int toukrzpos;
		 char ldatum [12];
		 long dat_von, dat_bis, lidat;

         if (testkeys ()) return 0;

		 sysdate (ldatum);
		 dat_von = dasc_to_long (ldatum) - ldatdiffminus;
		 dat_bis = dasc_to_long (ldatum) + ldatdiffplus;
		 lidat = dasc_to_long (ldat);

		 if ((lidat < dat_von) || (lidat > dat_bis))
		 {
			 disp_mess ("Abweichung beim Lieferdatum ist zu gro�", 2);
		     SetCurrentField (currentfield);
			 return 0;
		 }

         GetWochenTag ();
		 if (strcmp (ldat, akt_ldat) == 0)
		 {
		              testkommdat ();
		              return 1;
		 }

		 if (IsC_C () == 0 && wo_tou_par)
		 {
/*
                tou = QTClass.DatetoTouDir (lsk.mdn, lsk.fil, atoi (kunfil), 
                                  atol (kuns), ldat);
*/

// Tour mit Filiale 0 bearbeiten

                toupos = GetItemPos (&aufform, "tou");
                if ((aufform.mask[toupos].attribut & REMOVED) == 0)
                {
                   tou = QTClass.DatetoTouDir (lsk.mdn, 0, atoi (kunfil), 
                                  atol (kuns), ldat);
                   current_form = &aufform;
                   sprintf (tour, "%ld", tou);
                   display_field (mamain1, &aufform.mask[toupos], 0, 0);
                   if (tou) 
                   {
			             testtou (0);
                   }
		           else
                   {
                         sprintf (tour_krz, " "); 
                         toukrzpos = GetItemPos (&aufform, "tou_bz");
                         display_field (mamain1, &aufform.mask[toukrzpos], 0, 0);
                   }
				}
		 }
		 testkommdat ();
		 return 1;
}

int testldat ()
{
         if (testldat0 () == 0) return 0;
		 if (PosOK == FALSE && listedirect == 2)
		 {
			 PostMessage (mamain1, WM_KEYDOWN, VK_F6, 0l);
		 }
		 return 0;
}


int testtou (int Dmode)
/**
Tour lesen
**/
{
       int dsqlstatus;
       char *tou_bz;
       char *touval;
       int tou_pos;
       int tou_bz_pos;
       int ldatpos;


       set_fkt (InfoKun, 4);
       if (akt_tou && akt_tou == atol (tour)) return 0;

       tou_pos    = GetItemPos (&aufform, "tou");
       tou_bz_pos = GetItemPos (&aufform, "tou_bz");
       touval    = aufform.mask[tou_pos].item->GetFeldPtr ();
       tou_bz = aufform.mask[tou_bz_pos].item->GetFeldPtr ();
       tou.tou = atol (touval);
       if (tou.tou < (long) tour_div)
       {
           tou.tou *= tour_div;
       }
       sprintf (touval, "%ld", tou.tou);
       tou.tou /= tour_div;
       if (tou.tou == 0l)
       {
	            if (tour_zwang)
				{
		           disp_mess ("Tour 0 ist nicht erlaubt", 2);
				   SetCurrentField (currentfield);
				}
                return 0;
       }
       dsqlstatus = Tour.dbreadfirst ();
	   if (dsqlstatus == 100 && tour_zwang)
	   {
		           disp_mess ("Tour nicht gefunden", 2);
				   SetCurrentField (currentfield);
				   return 0;
	   }
       if (dsqlstatus == 0)
       {
           strcpy (tou_bz, tou.tou_bz);
       }
       current_form = &aufform;
       tou.tou = atol (touval);
       current_form = &aufform;
	   if (Dmode)
	   {
/*
                  QTClass.ToutoDate (lsk.mdn, lsk.fil, atoi (kunfil), 
                           atol (kuns), ldat, tou.tou);
*/

// Touren zuordnung mit Filiale 0 lesen

                  QTClass.ToutoDate (lsk.mdn, 0, atoi (kunfil), 
                           atol (kuns), ldat, tou.tou);
                  ldatpos = GetItemPos (&aufform, "lieferdat");
                  strcpy (kdat, ldat);
                  display_field (mamain1, &aufform.mask[ldatpos], 0, 0);
	   }
       display_field (mamain1, &aufform.mask[tou_pos], 0, 0);
       display_field (mamain1, &aufform.mask[tou_bz_pos], 0, 0);
       return 0;
}

int testtou0 (void)
{
       if (testkeys ()) return 0;
       return testtou (0);
}

int testvertr (void)
/**
Tour lesen
**/
{
       int dsqlstatus;
       char *vertr_krz;
       char *vertrval;
       int vertr_pos;
       int vertr_bz_pos;

       set_fkt (InfoKun, 4);

       vertr_pos    = GetItemPos (&aufform, "vertr");
       vertr_bz_pos = GetItemPos (&aufform, "vertr_krz");
       vertrval    = aufform.mask[vertr_pos].item->GetFeldPtr ();
       vertr_krz   = aufform.mask[vertr_bz_pos].item->GetFeldPtr ();
       vertr.vertr = atol (vertrval);
       if (vertr.vertr == 0l)
       {
 	       lsk.vertr = vertr.vertr;
           strcpy (vertr_krz, "");
           display_field (mamain1, &aufform.mask[vertr_pos], 0, 0);
           display_field (mamain1, &aufform.mask[vertr_bz_pos], 0, 0);
           return 0;
       }
       dsqlstatus = vertr_class.lese_vertr (lsk.mdn, lsk.fil,
                                            vertr.vertr);
       if (dsqlstatus != 0)
       {
              dsqlstatus = vertr_class.lese_vertr (lsk.mdn, 0,
                                                   vertr.vertr);
       }
       if (dsqlstatus != 0)
       {
                   print_mess (2, "Vertreter %ld ist nicht angelegt",
                                   vertr.vertr);
                   SetCurrentField (currentfield);
                   return 0;
       }
	   lsk.vertr = vertr.vertr;
       strcpy (vertr_krz, _adr.adr_krz);
       display_field (mamain1, &aufform.mask[vertr_pos], 0, 0);
       display_field (mamain1, &aufform.mask[vertr_bz_pos], 0, 0);
       return 0;
}

int testvertr0 (void)
{
       if (testkeys ()) return 0;
       return testvertr ();
}

int getaufart (void)
{
      int dsqlstatus;
	   
	  char wert [5];

	  sprintf (wert, "%ld", atoi (auf_art));
      if (atoi (wert) == 2)
      {
          return -1;
      }
      else if (atoi (wert) == 3 && kun.zahl_art == 2)
      {
          return -1;
      }
	  ptabn.ptbez[0] = 0;
      dsqlstatus = ptab_class.lese_ptab ("auf_art", wert);
      if (dsqlstatus != 0)
      {
          return -1;
      }
	  strcpy (auf_art_txt, ptabn.ptbez);
	  lspListe.SetAufArt ((short) atoi (auf_art));
	  return 0;
}

int testaufart0 (void)
{
	  int apos;

	  apos = GetItemPos (&aufform, "auf_art");
	  if (getaufart () == -1)
      {
               print_mess (2, "Falsche Auftragsart");
	  	       SetCurrentField (apos);
			   SetCurrentFocus (apos);
               return 0;
      }
	  if (apos >= 0)
	  {
               display_field (mamain1, &aufform.mask[apos]);
	  }
	  apos = GetItemPos (&aufform, "auf_art_txt");
	  if (apos >= 0)
	  {
               display_field (mamain1, &aufform.mask[apos]);
	  }
	  return 0;
}

int testaufart (void)
{
       if (testkeys ()) return 0;

	   SetFkt (9, leer, NULL);
	   set_fkt (NULL, 9);
       return testaufart0 ();
}

int SwitchKunFil (void)
{
       if (atoi (kunfil) == 0)
       {
           sprintf (kunfil, "1");
           ikunfil_bz.SetFeldPtr ("Filiale");
       }
       else
       {
           sprintf (kunfil, "0");
           ikunfil_bz.SetFeldPtr ("Kunde");
       }
       lsk.kun_fil = atoi (kunfil);
       display_field (mamain1, &aufformk.mask[2], 0, 0);
       display_field (mamain1, &aufformk.mask[3], 0, 0);
       return 0;
}

int AufEnter (void)
{
	   int aufpos; 
	    
	   aufpos = GetItemPos (&aufformk, "auftrag");
	   if (aufpos == -1)
	   {
		   return 0;
	   }
	   SetCurrentField (aufpos);
       SetCurrentFocus (aufpos);
	   return 0;
}

int lese_auftr (void)
{
       if (testkeys ()) return 0;

	   lsk.auf = atol (auftrag);
	   if (lsk.auf == 0l)
	   {
		       return 0;
	   }
       if (ls_class.lese_lsk_auf (atoi (mdn), atoi (fil), lsk.auf) != 0)
	   {
               print_mess (2, "Auftrag %ld nicht gefunden", lsk.auf);
               SetCurrentFocus (currentfield);
			   return 0;
	   }
 
       sprintf (lieferschein, "%ld", lsk.ls);
       display_field (mamain1, &aufformk.mask[0], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

int QueryAuf (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;


	   lsk.mdn = atoi (mdn);
	   lsk.fil = atoi (fil);
	   if (ScrollChoice && NewPrPlugin != NULL)
	   {
		   LskChoice ();
		   return 0; 
	   }
       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain1);
	   if (IsC_C () && cfgccmarkt == 1)
	   {
                 ret = QClass.queryaufex (mamain1, 0);
	   }
	   else if (cfgccmarkt == 2)
	   {
                 ret = QClass.queryaufex (mamain1, 1);
	   }

/*
	   else if (auftou)
	   {
                 ret = QClass.queryauftou (mamain1);
	   }
	   else
	   {
                 ret = QClass.queryauf (mamain1);
	   }
*/
	   else
	   {
//                 ret = QClass.queryaufex (mamain1, 1);
                 ret = QClass.queryauftou (mamain1);
	   }
         set_fkt (dokey5, 5);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       sprintf (lieferschein, "%ld", lsk.ls);
       display_field (mamain1, &aufformk.mask[0], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

  
void ShowAuf (void)
/**
Autrag anzeigen.
**/
{
       extern short sql_mode; 

       Mess.CloseMessage ();
       akt_auf = 0l;
       sprintf (lieferschein, "0");
       no_break_end ();
       set_fkt (QueryAuf, 10);
       SetFkt (10, auswahl, KEY10);
	   if (mench == 2) beginwork ();
       enter_form (mamain1, &aufformk, 0, 0);
	   if (mench == 2) commitwork ();
       lspListe.DestroyWindows (); 
       CloseControls (&aufform);
       CloseControls (&aufformk);
       set_fkt (NULL, 12);
       CTelefon.bmp = btelefoni;
       ActivateColButton (mamain1, 
                                      &buform, 0, -1, 1);
       ActivateColButton (mamain1, 
                                      &buform, 1, -1, 1);
       ActivateColButton (mamain1, 
                                      &buform, 2, -1, 1);

       EnableMenuItem (hMenu, CallT1,    MF_GRAYED);
       EnableMenuItem (hMenu, CallT2,    MF_GRAYED);
       EnableMenuItem (hMenu, CallT3,    MF_GRAYED);

       MoveButtons ();
       return;
}

BOOL AufNrOK (void)
/**
generierte Auftragsnummer testen.
**/
{
       char buffer [256];

       if (auto_nr.nr_nr == 0l) return FALSE;

	   if (lutz_mdn_par)
	   {
                 sprintf (buffer, "select ls from lsk where ls = %ld",
                                   auto_nr.nr_nr);
	   }
	   else
	   {
                 sprintf (buffer, "select auf from lsk where mdn = %d "
                                              "and   fil = %d "
                                              "and ls =   %ld",
                                   atoi (mdn), atoi (fil), auto_nr.nr_nr);
	   }

       if (DbClass.sqlcomm (buffer) != 100) return FALSE;
       return TRUE;
}
      

void GenAufNr (void)
/**
Auftragsnummer generieren.
**/
{
       extern short sql_mode; 
	   int i;
	   static int MAXWAIT = 100;
       MSG msg;

       beginwork ();
       sql_mode = 1;
	   i = 0;
       while (TRUE)
       {
		        if (lutz_mdn_par)
				{
					sprintf (mdn, "%hd", 0);
					sprintf (fil, "%hd", 0);
					lsk.mdn = lsk.fil = 0;
				}

                dsqlstatus = AutoClass.nvholid (atoi (mdn), 0, "ls");

				if (dsqlstatus == -1)
				{

					        DbClass.sqlin ((short *) &lsk.mdn, 1, 0); 
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"ls\" "
								              "and mdn = ? and fil = 0");
							dsqlstatus = 100;
				} 		
					   
                if (dsqlstatus == 100)
                {
                           dsqlstatus = AutoClass.nvanmprf (atoi (mdn),
                                                  0,
                                                  "ls",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (atoi (mdn),
                                                         0,
                                                        "ls");
                           }
                }

                if (dsqlstatus == 0 && AufNrOK ()) break;
                Sleep (50);
                if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
                {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                }

				i ++;
				if (i > MAXWAIT) break;
       }
       sql_mode = 0;
       commitwork ();
       FreeAufNr = TRUE;
}


void EnableArrows (form *frm, BOOL mode)
/**
Pfeilbuttons aktivieren oder deaktivieren.
**/
{
	   int i;

	   for (i = 0; i < frm->fieldanz; i ++)
	   {
		   if (frm->mask[i].item->GetItemName () && 
			   strcmp (frm->mask[i].item->GetItemName (), "arrdown") == 0)
		   {
			   if (mode == FALSE)
			   {
			             frm->mask[i].picture = "";
						 frm->mask[i].attribut = REMOVED;
						 CloseControl (frm, i);
			   }
			   else
			   {
						 frm->mask[i].attribut = BUTTON;
			             frm->mask[i].picture = "B";
			   }
			   EnableWindow (frm->mask[i].feldid, mode);
		   }
	   }
       if (tou_par == 0)
       {
	       aufform.mask[GetItemPos (&aufform, "tou") + 1].attribut = REMOVED;
       }
}


void EnterAuf (void)
/**
Autrag bearbeiten.
**/
{
	 

       Mess.CloseMessage ();
       akt_auf = 0l;
       while (TRUE)
       {
                if (lutz_mdn_par)
				{
		            sprintf (mdn, "%hd", 0);
		            sprintf (fil, "%hd", 0);
					lsk.mdn = lsk.fil = 0;
				}

		        if (autokunfil) strcpy (kunfil, "0"); 
				if (atoi (fil) > 0) strcpy (kunfil, "0");
                if (atoi (kunfil) == 0)
				{
                    ikunfil_bz.SetFeldPtr ("Kunde");
				}
                else
				{
                    ikunfil_bz.SetFeldPtr ("Filiale");
				}
                break_end ();
				IsAng = FALSE;
//				if (UniqueKunLs == 0l)
				if (LskChoiceNr == 0l)
				{
                    GenAufNr ();
				    if (auto_nr.nr_nr == 0l)
					{
					     disp_mess ("Es konnte keine Lieferscheinnummer generiert werden", 2);
					     return;
					}
                    sprintf (lieferschein, "%ld", auto_nr.nr_nr);
				}
				else
				{
                    sprintf (lieferschein, "%ld", LskChoiceNr);
                    PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
					LskChoiceNr = 0l;
				}
//				else
				if (UniqueKunLs != 0l)
				{
                    sprintf (lieferschein, "%ld", UniqueKunLs);
                    PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
		            if (listedirect == 1)
					{
			                 PostMessage (mamain1, WM_KEYDOWN, VK_F6, 0l);
					}
				}
				else if (LsDirect && LsNr != 0l)
				{
                    sprintf (lieferschein, "%ld", LsNr);
                    PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
/*
		            if (listedirect == 1)
					{
			                 PostMessage (mamain1, WM_KEYDOWN, VK_F6, 0l);
					}
*/
				}
				else
				{
					LsDirect = FALSE;
				}
                beginwork ();
				if (atoi (fil) == 0)
				{
                        set_fkt (SwitchKunFil, 8);
                        SetFkt (8, kundefil, KEY8);
				}

				set_fkt (dokey2, 2);
				set_fkt (dokey3, 3);
                set_fkt (QueryAuf, 10);
                SetFkt (10, auswahl, KEY10);
                set_fkt (AufEnter, 11);
                SetFkt (11, mauftrag, KEY11);
                DisplayAfterEnter (TRUE);
                no_break_end ();
				if (telelist)
				{
                      CTelefon.bmp = btelefon;
                      ActivateColButton (mamain1,  &buform, 1, 0, 1);
                      MoveButtons ();
                      EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
				      temode = 0;
				      SetTeleLst ();
				}
				telekun = FALSE;
//                if (IsC_C()) ShowKuncc ();
                if (IsC_C() && UniqueKunLs == 0l) 
				{
					SendMessage (hMainWindow, WM_COMMAND, KUNCC, 0l);
				}
				else
				{
					if (KunDirect && (UniqueKunLs == 0l))
					{
                           PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
					}
				}
			    enter_form (mamain1, &aufformk, 0, 0);

/*
Zum Dauertest der Masken enter_form asukommentieren.

create_enter_form (mamain1, &aufformk, 0, 0);
MSleep (100);
CloseControls (&aufformk);
display_form (mamain1, &aufformk, 0, 0);
*/

		        if (UniqueKunLs != 0l && listedirect == 1)
				{
			                 PostMessage (mamain1, WM_KEYDOWN, VK_F6, 0l);
				}
		        else if (LsDirect && listedirect == 1)
				{
			                 PostMessage (mamain1, WM_KEYDOWN, VK_F6, 0l);
				}
                set_fkt (NULL, 8);
                SetFkt (8, leer, 0);
                set_fkt (NULL, 9);
                SetFkt (9, leer, 0);
                set_fkt (NULL, 10);
                SetFkt (10, leer, 0);
                if (syskey == KEY5 || syskey == KEYESC) break;

                if (NewRec == 0 && GroupOK ())
                {
                        if (KomplettMode < 2) 
						{
				                 set_fkt (SetKomplett, 8);
                                 SetFkt (8, komplett, KEY8);
						}
                }
				EnableArrows (&aufformk, FALSE);
				UniqueKunError = FALSE;
				UniqueKunLs = 0l;
				EnableFullTax ();
				EnableMenuItem (hMenu, IDM_ALLNEWPR,   MF_GRAYED);
                eingabedaten ();
				lspListe.CloseAufw ();
				EnableMenuItem (hMenu, IDM_ALLNEWPR,   MF_ENABLED);
                commitwork ();
				LsDirect = FALSE;
                 LsNr = 0l;
				EnableArrows (&aufformk,TRUE);
                CTelefon.bmp = btelefoni;
                ActivateColButton (mamain1, 
                                      &buform, 0, -1, 1);
                ActivateColButton (mamain1, 
                                      &buform, 1, -1, 1);
                ActivateColButton (mamain1, 
                                      &buform, 2, -1, 1);
                EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
                EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
                EnableMenuItem (hMenu, CallT3,   MF_GRAYED);

                MoveButtons ();
                set_fkt (NULL, 6);
                set_fkt (NULL, 7);
                set_fkt (NULL, 9);
                SetFkt (6, leer, 0);
                SetFkt (7, leer, 0);
                SetFkt (9, leer, 0);
                if (IsC_C() == FALSE && 
                    KunDirect && (syskey == KEY5 || syskey == KEYESC)) 
				{
					CloseControls (&aufformk);
					break;
				}
        }
		set_fkt (NULL, 2);
		set_fkt (NULL, 3);
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        SetFkt (6, leer, 0);
        SetFkt (7, leer, 0);
        Mess.CloseMessage ();
        return;
}

void DisableMe (void)
{
     ToolBar_SetState(hwndTB,IDM_WORK, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_WORK,  MF_GRAYED);

     ToolBar_SetState(hwndTB,IDM_SHOW, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_SHOW,  MF_GRAYED);

     ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_PRINT,  MF_GRAYED);

}

void EnableMe (void)
{

     ToolBar_SetState(hwndTB, IDM_WORK, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,      IDM_WORK, MF_GRAYED);

     ToolBar_SetState(hwndTB, IDM_SHOW, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,      IDM_SHOW, MF_GRAYED);

     ToolBar_SetState(hwndTB, IDM_PRINT, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,      IDM_PRINT, MF_GRAYED);
/*
     ToolBar_SetState(hwndTB, IDM_PRINT, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_PRINT, MF_ENABLED);
*/
}


void ShowBm (void)
{
    RECT rect;
    TEXTMETRIC tm;
    int x, y, cx, cy;
    static int abmp = 0;
    char buffer [80];

    abmp ++;
    if (abmp > 3) abmp = 1;
    GetWindowRect (hMainWindow, &rect);
    stdfont ();
    SetTmFont (hMainWindow, &tm);

    cx = 5;
    cy = 2;
    x = (rect.right - 2) / tm.tmAveCharWidth;
    x -= cx;
    y = rect.top;
    y = (int) (double) ((double) y / 
                                (double)(1 + (double) 1/3));
    y /= tm.tmHeight;
    y += 3;
    sprintf (buffer, "-nm2 -z%d -s%d -h%d -w%d "
             "c:\\user\\fit\\bilder\\artikel\\%d.bmp",
             y, x, cy, cx, abmp);
    _ShowBm (buffer);
}


void EingabeMdnFil (void)
{
       extern MDNFIL cmdnfil;

       Mess.CloseMessage ();
       DisableMe ();
	   if (fil_lief_par == FALSE)
	   {
                 cmdnfil.SetFilialAttr (REMOVED);
	   }
       cmdnfil.SetNoMdnNull (TRUE);
	   if (StartMdn != 0)
	   {
		   sprintf (mdn, "%hd", StartMdn);
           cmdnfil.SetMandantAttr (READONLY);
	   }
	   while (TRUE)
       {
           set_fkt (dokey5, 5);
		   if (lutz_mdn_par == 0 &&  
			         cmdnfil.eingabemdnfil (mamain1, mdn, fil) == -1)
           {
			         break;
           }
		   if (PartyKz) // jetzt pauschal f�r alle Mandanten 030512  == (short) atoi (mdn))
		   {
			   EnablePartyService (TRUE);
//			   CheckPartyService (TRUE); 
			   SetParty (TRUE);
		   }
		   else
		   {
			   EnablePartyService (FALSE);
		   }
// Nicht aktiv    ShowBm ();
           switch (mench)
           {
                case 0:
                      EnterAuf  ();
                      break;
                case 1 :
                case 2 :
                case 3 :
					  set_fkt (dokey2, 2);
					  set_fkt (dokey3, 3);
                      ShowAuf  ();
					  set_fkt (NULL, 2);
					  set_fkt (NULL, 3);
                      CTelefon.bmp = btelefoni;
                      break;
           }
		   if (lutz_mdn_par) break;
	   }
       cmdnfil.CloseForm ();
       EnableMe ();
}


static int testber (void)
/**
Abfrage in Query-Eingabe.
**/
{
	    int savecurrent;

		savecurrent = currentfield;
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
				case KEY7 :
					   AufKompl.ChoisePrinter0 ();
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
					   return 1;
        }

		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY7)
				{
					   syskey = KEY7; 
					   ChoisePrinter0 ();
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
					   return 1;
				}
		}
        return 0;
}

BOOL LockBereich (char *where)
/**
Druckbereich sperren.
Bereich fuer Lieferscheinuebergabe sperren. 
**/
{
	char sqls [1028];
	int cursor;
//	int cursor_pos;
	int cursorupd;
	int dsqlstatus;
	extern short sql_mode;

	DbClass.sqlout ((short *) &lsk.mdn, 1, 0);
	DbClass.sqlout ((short *) &lsk.fil, 1, 0);
	DbClass.sqlout ((long *) &lsk.ls, 2, 0);
	//WAL-182  vorher wurde hier lsk.auf selektiert und in lsk.ls geschrieben,   jetzt korrigiert so konnten LS freigegeben werden , die noch in bearbeitung waren !!! 
	sprintf (sqls, "select lsk.mdn, lsk.fil, lsk.ls from lsk,kun %s and lsk.delstatus = 0 and lsk.mdn = kun.mdn and lsk.fil = kun.fil and lsk.kun = kun.kun", where);   //WAL-182  mit kun
	cursor = DbClass.sqlcursor (sqls);

	DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
	DbClass.sqlin ((short *) &lsk.fil, 1, 0);
	DbClass.sqlin ((long *) &lsk.ls, 2, 0);
	cursorupd = DbClass.sqlcursor ("select ls from lsk where mdn = ? "
		                            "and fil = ? "
									"and ls = ? for update");

	beginwork ();
    sql_mode = 1;
	while (DbClass.sqlfetch (cursor) == 0)
	{
		 DbClass.sqlopen (cursorupd);
		 dsqlstatus = DbClass.sqlfetch (cursorupd);
		 if (dsqlstatus < 0)
		 {
			 print_mess (2, "Lieferschein %ld wird im Moment bearbeitet", lsk.ls);
			 break;
		 }
	}
	DbClass.sqlclose (cursor);
	DbClass.sqlclose (cursorupd);
	commitwork ();
    sql_mode = 0;
    if (dsqlstatus < 0)
	{
		  return FALSE;
	}
	return TRUE;
}

void RemoveAbt (form *berformdr)
{
	static BOOL OK = FALSE;

	if (OK) return;

    SetItemAttr (berformdr, "abtlabel", REMOVED); 			   
    SetItemAttr (berformdr, "abt_von", REMOVED); 			   
    SetItemAttr (berformdr,"abt_bis", REMOVED); 			   
	berformdr->mask[berformdr->fieldanz -1].pos[0] -= 2;
	berformdr->mask[berformdr->fieldanz -2].pos[0] -= 2;
	berformdr->mask[berformdr->fieldanz -3].pos[0] -= 2;
    OK = TRUE;
}

BOOL DropLabel (form *Frm, char *Item, int LabelAnz, int rowdiff)
{
         int lpos = GetItemPos (Frm, Item);

         if (lpos == -1)
         {
             return FALSE;
         }

         if (Frm->mask[lpos].attribut == REMOVED)
         {
             return FALSE;
         }

         Frm->mask[lpos].attribut = REMOVED;
         for (int i = lpos + 1; i < LabelAnz; i ++)
         {
             Frm->mask[i].pos[0] -= rowdiff;
         }
         return TRUE;
}


BOOL DropText (form *Frm, char *Item, int TextAnz, int rowdiff)
{
         int lpos = GetItemPos (Frm, Item);

         if (lpos == -1)
         {
             return FALSE;
         }

         if (Frm->mask[lpos].attribut == REMOVED)
         {
             return FALSE;
         }


         Frm->mask[lpos].attribut = REMOVED;
         Frm->mask[lpos + 1].attribut = REMOVED;

         for (int i = lpos + 2; i < TextAnz; i ++)
         {
             Frm->mask[i].pos[0] -= rowdiff;
         }

         return TRUE;
}


        static char mdn_von [5];
        static char mdn_bis [5];
        static char fil_von [5];
        static char fil_bis [5];
        static char ls_von [9];
        static char ls_bis [9];
        static char kunfil_von [5];
        static char kunfil_bis [5];
        static char kun_von [9];
        static char kun_bis [9];
        static char inka_nr_von [9];
        static char inka_nr_bis [9];
        static char stat_von [3];
        static char stat_bis [3];
        static char dat_von [12];
        static char dat_bis [12];
        static char tou_von [10];
        static char smt_bis [10];
        static char smt_von [10];
        static char tou_bis [10];
        static char abt_von [6];
        static char abt_bis [6];
        static char samrech_von [6];
        static char samrech_bis [6];

void LsStapelDruck (BOOL drkauswahl)
{
  FILE *lst ;
  char dateinam[22] ;
  char buffer [512] ;
  char zeilbuffer[128] ;
  char kommando[128] ;
  char zwi_von[25] ;
  char zwi_bis[25] ;
  
  if (strlen (clipped(iformStapeldruck)) == 0)  sprintf ( iformStapeldruck , "%s", "Timm53100" ) ;

  sprintf ( dateinam , "%s.llf", iformStapeldruck ) ;

  if (getenv ("TMPPATH"))
  {
    sprintf (buffer, "%s\\%s", getenv("TMPPATH"), dateinam);
  }
  else
  {
    strcpy (buffer, dateinam );
  }
  remove (buffer) ;
  if ( (lst = fopen (buffer, "wt" )) == NULL ) 
  {
                     print_mess (2, "%s kann nicht ge�ffnet werden", buffer);
                     return ;
  }
	//==== Druckerwahl  =====

	//==== Formatname=====
	    memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "NAME %s" , iformStapeldruck ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;
	//==== Druckerwahl  =====
        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "DRUCK %hd" , drkauswahl ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;
	//==== Liste , nicht Label =====
        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "LABEL %hd" , 0 ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;
	//==== RangeDialog aus =====
        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, "MITRANGE %hd" , 0 ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;

	//==== Text ==========
        memset ( zeilbuffer, '\0', 80 ) ;
        sprintf ( zeilbuffer, "%-16s %12s %12s","#Feldname", "von", "bis") ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;


	//==== Felder ==========
        sprintf ( zwi_von , "%d" , atoi(mdn_von) ) ;
        sprintf ( zwi_bis  ,"%d" , atoi(mdn_bis) ) ;
        memset ( zeilbuffer, '\0', 80 ) ;
        sprintf ( zeilbuffer, "%-16s %12s %12s","mdn", zwi_von, zwi_bis) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;
		
        sprintf ( zwi_von , "%d" , atoi(fil_von) ) ;
        sprintf ( zwi_bis  ,"%d" , atoi(fil_bis) ) ;
        memset ( zeilbuffer, '\0', 80 ) ;
        sprintf ( zeilbuffer, "%-16s %12s %12s","fil", zwi_von, zwi_bis) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;

        sprintf ( zwi_von , "%d" , atoi(ls_von) ) ;
        sprintf ( zwi_bis  ,"%d" , atoi(ls_bis) ) ;
        memset ( zeilbuffer, '\0', 80 ) ;
        sprintf ( zeilbuffer, "%-16s %12s %12s","ls", zwi_von, zwi_bis) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;


        memset ( zeilbuffer, '\0', 80 ) ;
        sprintf ( zeilbuffer, "%-16s %12s %12s","lieferdat", dat_von, dat_bis) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;

		
        sprintf ( zwi_von , "%d" , atoi(stat_von) ) ;
        sprintf ( zwi_bis  ,"%d" , atoi(stat_bis) ) ;
        memset ( zeilbuffer, '\0', 80 ) ;
        sprintf ( zeilbuffer, "%-16s %12s %12s","gruppe", zwi_von, zwi_bis) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;

    fclose ( lst ) ;


  if (getenv ("TMPPATH"))
  {
    sprintf (kommando, "%s -name %s -datei %s\\%s","dr70001",iformStapeldruck, getenv("TMPPATH"), dateinam);
  }
  else
  {
    sprintf (kommando, "%s -name %s -datei %s","dr70001",iformStapeldruck, dateinam);
  }

    HCURSOR oldcursor = SetCursor ( LoadCursor ( NULL , IDC_WAIT)) ;

    ProcWaitExec ( kommando, SW_SHOWNORMAL, 0, 40, -1, 0);

}




void BereichsAuswahl (void)
/**
Bereich fuer Druck oder Lieferscheinuebergabe eingeben.
**/
{
        HANDLE hMainInst;
		HWND hWnd;
		char where [1028];

        static int DrkRows = 22; 
        static int LuRows = 22; 
//		long max_ls;

        static ITEM iOK ("", "     OK     ", "", 0);
        static ITEM iCA ("", "  Abbrechen ", "", 0);
        static ITEM iDW ("", "Druckerwahl ",  "", 0);


		static ITEM ivon     ("", "von",             "", 0);
		static ITEM ibis     ("", "bis",             "", 0);
		static ITEM iMdn     ("mdnlabel", "Mandant",         "", 0);
		static ITEM iFil     ("fillabel", "Filiale",         "", 0);
		static ITEM iLs      ("lslabel",  "Lieferschein",    "", 0);
		static ITEM iKunFil  ("kunfillabel", "Kunde/Filiale",           "", 0);
		static ITEM iKun     ("kunlabel", "Kunde",           "", 0);
		static ITEM iInka    ("inkalabel","Inkasso",           "", 0);
		static ITEM iStat    ("statlabel", "Lieferschein-Status", "", 0);
		static ITEM iDat     ("datlabel", "Lieferdatum",     "", 0);
		static ITEM iTour    ("toulabel", "Tour",            "", 0);
		static ITEM iSmt    ("smtlabel", "Sortiment",            "", 0);
		static ITEM iAbt     ("abtlabel", "Abteilung", "", 0);
		static ITEM iSamRech ("samrechlabel", "Rechnungsart", "", 0);  //WAL-182 

        static ITEM imdn_von ("mdn_von", mdn_von,  "", 0);
        static ITEM imdn_bis ("mdn_bis", mdn_bis,  "", 0);
        static ITEM ifil_von ("fil_von", fil_von,  "", 0);
        static ITEM ifil_bis ("fil_bis", fil_bis,  "", 0);
        static ITEM ils_von  ("ls_von",  ls_von,  "", 0);
        static ITEM ils_bis  ("ls_bis",  ls_bis,  "", 0);
        static ITEM ikunfil_von ("kunfil_von", kunfil_von,  "", 0);
        static ITEM ikunfil_bis ("kunfil_bis", kunfil_bis,  "", 0);
        static ITEM ikun_von ("kun_von", kun_von,  "", 0);
        static ITEM ikun_bis ("kun_bis", kun_bis,  "", 0);
        static ITEM iinka_nr_von ("inka_nr_von", inka_nr_von,  "", 0);
        static ITEM iinka_nr_bis ("inka_nr_bis", inka_nr_bis,  "", 0);
        static ITEM istat_von ("stat_von", stat_von,  "", 0);
        static ITEM istat_bis ("stat_bis", stat_bis,  "", 0);
        static ITEM idat_von ("dat_von", dat_von,  "", 0);
        static ITEM idat_bis ("dat_bis", dat_bis,  "", 0);
        static ITEM itou_von ("tou_von", tou_von,  "", 0);
        static ITEM itou_bis ("tou_bis", tou_bis,  "", 0);
        static ITEM ismt_von ("smt_von", smt_von,  "", 0);  //LAC-202
        static ITEM ismt_bis ("smt_bis", smt_bis,  "", 0);
        static ITEM iabt_von ("abt_von", abt_von,  "", 0);
        static ITEM iabt_bis ("abt_bis", abt_bis,  "", 0);
        static ITEM isamrech_von ("samrech_von", samrech_von,  "", 0);
        static ITEM isamrech_bis ("samrech_bis", samrech_bis,  "", 0);


        static field _berform[] = {
           &ivon,     0, 0, 1,18,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &ibis,     0, 0, 1,40,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iMdn,     0, 0, 1, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iFil,     0, 0, 3, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iLs,      0, 0, 5, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iKunFil,  0, 0, 7, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iKun,     0, 0, 8, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iInka,    0, 0, 9, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iSamRech, 0, 0,10, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iStat,    0, 0,12, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iDat,     0, 0,14, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iTour,    0, 0,16, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 

           &imdn_von, 5, 0, 1,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &imdn_bis, 5, 0, 1,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_von, 5, 0, 3,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_bis, 5, 0, 3,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ils_von,  9, 0, 5,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ils_bis,  9, 0, 5,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ikunfil_von, 5, 0, 7,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ikunfil_bis, 5, 0, 7,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ikun_von, 9, 0, 8,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ikun_bis, 9, 0, 8,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &iinka_nr_von, 9, 0, 9,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &iinka_nr_bis, 9, 0, 9,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &isamrech_von, 2, 0, 10,18, 0, "%1d",    EDIT, 0 ,testber,0, 
           &isamrech_bis, 2, 0, 10,40, 0, "%1d",    EDIT, 0 ,testber,0, 
           &istat_von, 2, 0,12,18, 0, "%1d",    EDIT, 0 ,testber,0, 
           &istat_bis, 2, 0,12,40, 0, "%1d",    EDIT, 0 ,testber,0, 
           &idat_von, 11, 0,14,18, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 
           &idat_bis, 11, 0,14,40, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 
           &itou_von,  9, 0,16,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &itou_bis,  9, 0,16,40, 0, "%8d",    EDIT, 0 ,testber,0, 

           &iOK,     13, 0,19,15, 0, "", BUTTON, 0, testber,KEY12,
           &iCA,     13, 0,19,32, 0, "", BUTTON, 0, testber,KEY5,
		};

        static form berform = {34, 0, 0, _berform, 
			                    0, 0, 0, 0, NULL};
        static int lulabelanz = 10;
        
        static field _berformdr[] = {
           &ivon,     0, 0, 1,18,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &ibis,     0, 0, 1,40,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iMdn,     0, 0, 1, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iFil,     0, 0, 3, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iLs,      0, 0, 5, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iKun,     0, 0, 7, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iStat,    0, 0, 9, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iDat,     0, 0,11, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iTour,    0, 0,13, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iAbt,     0, 0,15, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iSmt,     0, 0,17, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 

           &imdn_von, 5, 0, 1,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &imdn_bis, 5, 0, 1,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_von, 5, 0, 3,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_bis, 5, 0, 3,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ils_von,  9, 0, 5,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ils_bis,  9, 0, 5,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ikun_von, 9, 0, 7,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ikun_bis, 9, 0, 7,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &istat_von, 2, 0, 9,18, 0, "%1d",    EDIT, 0 ,testber,0, 
           &istat_bis, 2, 0, 9,40, 0, "%1d",    EDIT, 0 ,testber,0, 
           &idat_von, 11, 0,11,18, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 
           &idat_bis, 11, 0,11,40, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 
           &itou_von,  9, 0,13,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &itou_bis,  9, 0,13,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &iabt_von,  5, 0,15,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &iabt_bis,  5, 0,15,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ismt_von,  5, 0,17,18, 0, "%4d",    EDIT, 0 ,testber,0,  //LAC-202
           &ismt_bis,  5, 0,17,40, 0, "%4d",    EDIT, 0 ,testber,0, 

           &iOK,     15, 0,19, 6, 0, "", BUTTON, 0, testber,KEY12,
           &iCA,     15, 0,19,23, 0, "", BUTTON, 0, testber,KEY5,
           &iDW,     15, 0,19,40, 0, "", BUTTON, 0, testber,KEY7,
		};

        static form berformdr = {32, 0, 0, _berformdr, 
			                    0, 0, 0, 0, NULL};

        static int drklabelanz = 11;


        HWND berwin;
		int savefield;
		form *savecurrent;

        NoClose = TRUE;
        hWnd = AktivWindow;
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testber, 5);
        set_fkt (testber, 11);
        set_fkt (testber, 12);

		sysdate (dat_von);
		sysdate (dat_bis);
		strcpy (mdn_von, "1");
		strcpy (mdn_bis, "9999");
		strcpy (fil_von, "0");
		strcpy (fil_bis, "9999");
		strcpy (ls_von, "1");
		strcpy (ls_bis, "99999999");
		strcpy (kunfil_von, "0");
		strcpy (kunfil_bis, "1");
		strcpy (kun_von, "1");
		strcpy (kun_bis, "99999999");
		strcpy (inka_nr_von, "0");
		strcpy (inka_nr_bis, "99999999");
		strcpy (samrech_von, "1");
		strcpy (samrech_bis, "7");
		if (mench == 4)
		{
		         strcpy (stat_von, stat_deff_von);
		         strcpy (stat_bis, stat_deff_bis);
		}
		else
		{
		         strcpy (stat_von, stat_defp_von);
		         strcpy (stat_bis, stat_defp_bis);
				 if (Menuedirect == 3)  //Direkteinstieg in die Druckmaske (F�r Touch) (Aufruf 53100 menuedirect=3)
				 {
			         strcpy (stat_von, "3");
			         strcpy (stat_bis, "3");
					 sysdate (dat_von); 
					 sysdate (dat_bis);
					 long datum  = dasc_to_long (dat_von) ;
					 dlong_to_asc (datum, dat_von);
					 dlong_to_asc (datum, dat_bis);
				 }
		}
		strcpy (tou_von, tou_def_von);
		strcpy (tou_bis, tou_def_bis);
		strcpy (smt_von, smt_def_von);
		strcpy (smt_bis, smt_def_bis);
		strcpy (abt_von, abt_def_von);
		strcpy (abt_bis, abt_def_bis);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);

        if (DrkKunRemoved)
        {
             BOOL ret = DropLabel (&berformdr, "kunlabel", drklabelanz, 2);
             if (ret)
             {
                   ret = DropText (&berformdr,  "kun_von" , berformdr.fieldanz, 2);
             }
             if (ret)
             {
                   DrkRows -= 2;
             }
        }

        if (AbtRemoved)
        {
             BOOL ret = DropLabel (&berformdr, "abtlabel", drklabelanz, 2);
             if (ret)
             {
                   ret = DropText (&berformdr,  "abt_von" , berformdr.fieldanz, 2);
             }
             if (ret)
             {
                   DrkRows -= 2;
             }
        }

        if (SmtRemoved) //LAC-202
        {
             BOOL ret = DropLabel (&berformdr, "smtlabel", drklabelanz, 2);
             if (ret)
             {
                   ret = DropText (&berformdr,  "smt_von" , berformdr.fieldanz, 2);
             }
             if (ret)
             {
                   DrkRows -= 2;
             }
        }
        if (LuKunRemoved)
        {
             BOOL ret = DropLabel (&berform, "kunlabel", lulabelanz, 1);
             if (ret)
             {
                   ret = DropText (&berform,  "kun_von" , berform.fieldanz, 1);
             }
             if (ret)
             {
                   LuRows -= 1;
             }
        }

		if (mench == 4)
		{
               berwin = OpenWindowChC (LuRows, 62, 6, 10, hMainInst,
                               "LS-Freigabe");
               MoveZeWindow (hMainWindow, berwin);
	           strcpy (stat_von, "4");
	           strcpy (stat_bis, "4");
			   SetItemAttr (&berform, "stat_von", READONLY);
			   SetItemAttr (&berform, "stat_bis", READONLY);
               enter_form (berwin, &berform, 0, 0);
		}
/*
		else if (mench == 3 && AbtRemoved)
		{
			   RemoveAbt (&berformdr);
			    
               berwin = OpenWindowChC (DrkRows, 62, 6, 10, hMainInst,
                               "Druck K-Liste");
               MoveZeWindow (hMainWindow, berwin);
               enter_form (berwin, &berformdr
				   , 0, 0);
		}
*/

//		else if (mench == 3 && AbtRemoved == FALSE)
		else if (mench == 3)
		{
			    
               berwin = OpenWindowChC (DrkRows, 62, 6, 10, hMainInst,
                               "LS Stapeldruck");
               MoveZeWindow (hMainWindow, berwin);
               enter_form (berwin, &berformdr
				   , 0, 0);
		}


		SetButtonTab (FALSE);
        CloseControls (&berform);
        DestroyWindow (berwin);
        SetAktivWindow (hWnd);
		current_form = savecurrent;
		currentfield = savefield;
        restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (9);
		restore_fkt (11);
		restore_fkt (12);

		if (syskey == KEY5) 
		{
            NoClose = FALSE;
			return;
		}

/* Bei gesetztem tour_def_par pruefen, ob die Defaultgrenzen eingehalten wurden.  */


		if (tou_def_zwang && (atoi (tou_von) || atoi (tou_bis)))
		{
                 if (atol (tou_von) < atol (tou_def_von))
				 {
		                     strcpy (tou_von, tou_def_von);
				 }
                 if (atol (tou_bis) > atol (tou_def_bis))
				 {
		                     strcpy (tou_bis, tou_def_bis);
				 }
		}
					     

		AufKompl.InitAufanz ();
		sprintf (where, "where lsk.mdn between %d and %d "
			            "and   lsk.fil between %d and %d "
						"and   lsk.ls between %ld and %ld "
						"and   lsk.kun_fil between %ld and %ld "
						"and   lsk.kun between %ld and %ld "
						"and   lsk.inka_nr between %ld and %ld "
						"and   lsk.lieferdat between \"%s\" and \"%s\" "
						"and   lsk.ls_stat  between %d and %d "
						"and   lsk.teil_smt between %d and %d "
						"and   lsk.tou  between %ld and %ld",
						atoi (mdn_von), atoi (mdn_bis),
						atoi (fil_von), atoi (fil_bis),
						atol (ls_von), atol (ls_bis),
						atol (kunfil_von), atol (kunfil_bis),
						atol (kun_von), atol (kun_bis),
						atol (inka_nr_von), atol (inka_nr_bis),
						clipped (dat_von), clipped (dat_bis),
						atoi (stat_von), atoi (stat_bis),
						atoi (smt_von), atoi (smt_bis),
						atol (tou_von) * tour_div, (atol (tou_bis) + 1) * tour_div - 1);
		EnableWindow (hMainWindow, FALSE);
		if (mench == 3)
		{
/*
			      if (LockBereich (where) == FALSE) return;
                  AufKompl.K_ListeGroup (atoi (mdn_von), atoi (mdn_bis),
						                 atoi (fil_von), atoi (fil_bis),
						                 atol (ls_von), atol (ls_bis),
						                 clipped (dat_von), clipped (dat_bis),
						                 atoi (stat_von), atoi (stat_bis),
						                 atol (tou_von) * tour_div, (atol (tou_bis) + 1) * tour_div - 1,
										 atoi (abt_von), atoi (abt_bis),
                                         atol (kun_von), atoi (kun_bis));
				   max_ls = AufKompl.Getmax_ls ();
				   if (max_ls > atol (ls_bis))
				   {
					   sprintf (ls_bis, "%8ld", max_ls);
				   }
 		           EnableWindow (hMainWindow, TRUE);
                   SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
				   SetForegroundWindow (hMainWindow);
                   UpdateWindow (hMainWindow);
										 

                  if (lsdirect)
				  {
		              sprintf (where, "where lsk.mdn between %d and %d "
			            "and   lsk.fil between %d and %d "
						"and   lsk.ls between %ld and %ld "
						"and   lsk.kun between %ld and %ld "
						"and   lsk.lieferdat between \"%s\" and \"%s\" "
						"and   ls_stat between 4 and 4 "
						"and   lsk.tou  between %ld and %ld",
						atoi (mdn_von), atoi (mdn_bis),
						atoi (fil_von), atoi (fil_bis),
						atol (ls_von), atol (ls_bis),
						atol (kun_von), atol (kun_bis),
						clipped (dat_von), clipped (dat_bis),
						atol (tou_von) * tour_div, (atol (tou_bis) + 1) * tour_div - 1);
					    mench = 4;
				  }
*/

			      if (LockBereich (where) == FALSE) return;
				  if (Menuedirect == 3)
				  {
					  AufKompl.PrintLsGroup (where, 1);
					  LsStapelDruck (TRUE);
//					  LsStapelDruck (FALSE);
					  AufKompl.SetLsGruppe ();
				  }
				  else
				  {
					  AufKompl.PrintLsGroup (where,0);
				  }
		}
		if (mench == 4)
		{
			  int s_von = atoi (samrech_von);
			  int s_bis = atoi (samrech_bis);
			    //WAL-182
				sprintf (where, "where lsk.mdn between %d and %d "
			            "and   lsk.fil between %d and %d "
						"and   lsk.ls between %ld and %ld "
						"and   lsk.kun_fil between %ld and %ld "
						"and   lsk.kun between %ld and %ld "
						"and   lsk.inka_nr between %ld and %ld "
						"and   lsk.lieferdat between \"%s\" and \"%s\" "
						"and   lsk.ls_stat  between %d and %d "
						"and   lsk.tou  between %ld and %ld "
						"and   kun.sam_rech between %d and %d",
						atoi (mdn_von), atoi (mdn_bis),
						atoi (fil_von), atoi (fil_bis),
						atol (ls_von), atol (ls_bis),
						atol (kunfil_von), atol (kunfil_bis),
						atol (kun_von), atol (kun_bis),
						atol (inka_nr_von), atol (inka_nr_bis),
						clipped (dat_von), clipped (dat_bis),
						atoi (stat_von), atoi (stat_bis),
						atol (tou_von) * tour_div, (atol (tou_bis) + 1) * tour_div - 1,
						atoi (samrech_von), atoi(samrech_bis));


			      if (LockBereich (where) == FALSE) return;
				  AufKompl.FreeLsGroup (where);
		}
        NoClose = FALSE;
		EnableWindow (hMainWindow, TRUE);
        SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
        SetForegroundWindow (hMainWindow);
}


static HWND chwnd;

static int lstcancel (void)
{

    syskey = KEY5;
    break_enter ();
    return 0;
}


static int lstok (void)
{
    syskey = KEY12;
    mench = currentfield;
    break_enter ();
    return 0;
}


static int testwork (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

//    mench = 0;
    break_enter ();
    return 0;
}

static int testshow (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

//    mench = 1;
    break_enter ();
    return 0;
}

static int testdel (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}


static int testprint (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}

static int testfree (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}

static int testcancel (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

	syskey = KEY5;
    break_enter ();
    return 0;
}

        static ColButton LstChoise = {
                              "Auswahl", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
//                               WHITECOL,
//                               RGB (0,0,120),
                               BLACKCOL,
                               GRAYCOL,
                               -2};
   
        static ColButton cWork = {
                              "Bearbeiten", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cShow = {
                              "Anzeigen", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cDel = {
                              "L�schen", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cPrint = {
                              "Drucken", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cFree = {
                              "Freigeben", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};
							  static ColButton *cButtons [] = {&cWork, &cShow, &cDel, &cPrint, &cFree, NULL}; 



        static ITEM iLstChoise ("", (char *) &LstChoise,  "", 0);

        static ITEM iWork      ("", (char *) &cWork,      "", 0);
        static ITEM iShow      ("", (char *) &cShow,      "", 0);
        static ITEM iDel       ("", (char *) &cDel,       "", 0);
        static ITEM iPrint     ("", (char *) &cPrint,     "", 0);
        static ITEM iFree      ("", (char *) &cFree,      "", 0);

        static ITEM iWorkB      ("", (char *) "Bearbeiten",  "", 0);
        static ITEM iShowB      ("", (char *) "Anzeigen",    "", 0);
        static ITEM iDelB       ("", (char *) "L�schen",     "", 0);
        static ITEM iPrintB     ("", (char *) "Drucken",     "", 0);
        static ITEM iFreeB      ("", (char *) "Freigeben",   "", 0);

        static ITEM vOK     ("",  "    OK     ",  "", 0);
        static ITEM vCancel ("",  " Abbrechen ", "", 0);

        static field _fLstChoise[] = {
          &iLstChoise, 36, 0,  0, 0, 0, "", COLBUTTON, 0, 0, 0};

        static form fLstChoise = {1, 0, 0, _fLstChoise, 
                                  0, 0, 0, 0, NULL};    


        static field _clist [] = {
&iWork,     34, 2, 2, 1, 0, "", COLBUTTON,            0, testwork, 
                                                           KEY12 , 
&iShow,     34, 2, 4, 1, 0, "", COLBUTTON,            0, testshow, 
                                                          KEY12,
&iDel,      34, 2, 6, 1, 0, "", COLBUTTON,            0, testdel, 
                                                           KEY12,

&iPrint,    34, 2, 8, 1, 0, "", COLBUTTON,            0, testprint, 
                                                           KEY12,
&iFree,     34, 2,10, 1, 0, "", COLBUTTON,            0, testfree, 
                                                           KEY12,
&vCancel,  12, 0, 10, 12, 0, "",  BUTTON,               0, testcancel, 

/*                                                           KEY5, 
&vCancel,  12, 0, 8, 12, 0, "",  BUTTON,               0, testcancel, 
*/
                                                           KEY5, 
};


        static field _clistB [] = {
&iWorkB,     30, 0, 2, 3, 0, "", BUTTON,            0, testwork, 
                                                           KEY12 , 
&iShowB,     30, 0, 3, 3, 0, "", BUTTON,            0, testshow, 
                                                          KEY12,
&iDelB,      30, 0, 4, 3, 0, "", BUTTON,            0, testdel, 
                                                           KEY12,
&iPrintB,    30, 0, 5, 3, 0, "", BUTTON,            0, testprint, 
                                                           KEY12,
&iFreeB,     30, 0, 6, 3, 0, "", BUTTON,            0, testfree, 
                                                           KEY12,

&vCancel,   12, 0,  8,12, 0, "",  BUTTON,               0, testcancel, 

/*                                                           KEY5, 
&vCancel,   12, 0,  6,12, 0, "",  BUTTON,               0, testcancel, 
*/
                                                           KEY5, 
};


static form clist = {6, 0, 0, _clistB, 0, 0, 0, 0, NULL}; 


void StartMenu (void)
/**
Liste auswaehlen und Listengenerator starten
**/
{
     form *sform;
     int  sfield;

	 if (Menuedirect != -1)
	 {
	      mench = Menuedirect;
	        switch (mench)
		    {
              case 0 :
              case 1 :
              case 2 :
                     EingabeMdnFil ();
					 break;
              case 3 :
              case 4:
                     BereichsAuswahl ();
			}
		  return;
	}
   
	 if (MdnDirect || NoMenue)
	 {
	      mench = 0;
          EingabeMdnFil ();
		  return;
	 }

     while (TRUE)
     {
        sform = current_form;
        sfield = currentfield;
        save_fkt (5);
        save_fkt (12);

        set_fkt (lstcancel, 5);
        set_fkt (lstok, 12);

        DisableMe ();
        no_break_end ();
        SetButtonTab (TRUE);
//        DisablehWnd (hMainWindow);
        SetBorder (WS_POPUP | WS_DLGFRAME);

        SetAktivWindow (mamain1);
        chwnd = OpenWindowCh (12, 36, 10, 22, hMainInst);
		MoveZeWindow (hMainWindow, chwnd);
        DlgWindow = chwnd;

        display_form (chwnd, &fLstChoise, 0, 0);
        currentfield = 0;
        enter_form (chwnd, &clist, 0, 0);
		mench = currentfield;
        CloseControls (&fLstChoise);
        CloseControls (&clist);
        SetAktivWindow (mamain1);
        DestroyWindow (chwnd);
        chwnd = NULL;
        SetButtonTab (FALSE);
        restore_fkt (5);
        restore_fkt (12);
        if (sform)
        {
                current_form = sform;
                currentfield = sfield;
                SetCurrentFocus (currentfield);
        }

        if (syskey == KEY5) return;
        if (syskey == KEYESC) return;

        switch (mench)
        {
              case 0 :
              case 1 :
              case 2 :
                     EingabeMdnFil ();
					 break;
              case 3 :
              case 4:
                     BereichsAuswahl ();
        }
     }
}


void SetListBkColor (char *coltxt)
/**
Hintergrund f�r Liste setzen.
**/
{
	   int i;
       static char *BkColorsTxt[] = {"WHITECOL",
                                     "BLUECOL",
                                     "BLACKCOL",
                                     "GRAYCOL",
					      		     "LTGRAYCOL",
	   };

       clipped (coltxt);
       for (i = 0; Combo2[i]; i ++)
       {
             if (strcmp (BkColorsTxt[i],coltxt) == 0)
             {
                   lspListe.SetColors (Colors[i], BkColors[i]);
                   lstxtlist.SetColors (Colors[i], BkColors[i]);
				   combo2pos = i;
                   break;
             }
	   }
}

int SetListLine (char *cfg_v)
{
       static char *lineflag [] = {"WT" ,
                                   "HT",
                                   "ST",
                                   "GT",
                                   "VW",
                                   "VH",
                                   "VS", 
                                   "VG",
                                   "HW",
                                   "HH",
                                   "HS", 
                                   "HG",
                                   "NO",
                                   NULL};
       int i;

       for (i = 0; lineflag[i] != NULL; i ++)
       {
           if (strcmp (lineflag[i], cfg_v) == 0) 
           {
               lspListe.SetListLines (i);
			   combo1pos = i;
               return i;
           }
       }
       lspListe.SetListLines (1);
       return 1;
}


void EnableAufArt (void)
{

       SetItemAttr (&aufform, "tele", REMOVED);
       SetItemAttr (&aufform, "auf_art", EDIT);
       SetItemAttr (&aufform, "auf_art_txt", READONLY);
       auf_art_active = TRUE;
}


void EnableLdVkChange ()
/**
Moeglichkeit zur Aenderung des VK's in Menue beaerbeiten einfuegen.
**/
{

       InsertMenueItem (bearbeiten, &changeldpr, 0);
       LdVkEnabled = TRUE;
}

void EnablePrVkChange ()
/**
Moeglichkeit zur Aenderung des VK's in Menue beaerbeiten einfuegen.
**/
{

       InsertMenueItem (bearbeiten, &changevkpr, 0);
       PrVkEnabled = TRUE;
}

void EnableKasse ()
/**
Moeglichkeit zur Aenderung des VK's in Menue beaerbeiten einfuegen.
**/
{

       InsertMenueItem (bearbeiten, &kasse, 0);
}


int HextoInt (char *Hex)
{
       int Hi;
       int Low;

       Hi = Hex[0];
       if (Hi >= (int) 'A' &&
           Hi <= (int) 'F')
       {
           Hi -= ((int) 'A' - 0x0A);
       }
       else if (Hi >= (int) 'a' &&
                Hi <= (int) 'f')
       {
           Hi -= ((int) 'a' - 0x0A);
       }
       else if (Hi >= (int) '0' &&
                Hi <= (int) '9')
       {
           Hi -= 0x30;
       }
       else
       {
           Hi = 0;
       }

       Low = Hex[1];
       if (Low >= (int) 'A' &&
           Low <= (int) 'F')
       {
           Low -= ((int) 'A' - 0x0A);
       }
       else if (Low >= (int) 'a' &&
                Low <= (int) 'f')
       {
           Low -= ((int) 'a' - 0x0A);
       }
       else if (Low >= (int) '0' &&
                Low <= (int) '9')
       {
           Low -= 0x30;
       }
       else
       {
           Low = 0;
       }
       return ((Hi << 4) & 0xF0) |  Low;
}


void GetCfgValues (void)
/**
Werte aus 53100.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [512];

	   if (cfgOK) return;

	   cfgOK = TRUE;
       if (ProgCfg.GetCfgValue ("FaxTrenner", cfg_v) == TRUE)
       {
                    if (strcmp (cfg_v, "NO") == 0)
                    {
                        FaxTrenner = 0;
                    }
                    else if (strcmp (cfg_v, "TAB") == 0)
                    {
                        FaxTrenner = TAB;
                    }
                    else if (memcmp (cfg_v, "0x", 2) == 0 ||
                             memcmp (cfg_v, "0X", 2) == 0)
                    {
                        FaxTrenner = HextoInt (&cfg_v[2]);
                    }
                    else
                    {
                        FaxTrenner = cfg_v[0];
                    }
	   }
       if (ProgCfg.GetCfgValue ("NewStyle", cfg_v) == TRUE)
       {
                    NewStyle = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("Startsize", cfg_v) == TRUE)
       {
                    Startsize = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("colbutton", cfg_v) == TRUE)
       {
		            if (atoi (cfg_v))
					{
					       clist.mask = _clist;
					}
       }
       if (ProgCfg.GetCfgValue ("TransactMax", cfg_v) == TRUE)
       {
                    TransactMax = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("TransactMessage", cfg_v) == TRUE)
       {
                    TransactMessage = atoi (cfg_v);
       }

       if (ProgCfg.GetCfgValue ("Party_Kz", cfg_v) == TRUE)
       {
                    PartyKz_cfg = atoi (cfg_v);
       }
     
	   
	   if (ProgCfg.GetCfgValue ("lad_vk_edit", cfg_v) == TRUE)
       {
                     if (atoi (cfg_v))
                     {
   		                  EnableLdVkChange ();
                     }
                         
       }
       if (ProgCfg.GetCfgValue ("pr_vk_edit", cfg_v) == TRUE)
       {
                     if (atoi (cfg_v))
                     {
   		                  EnablePrVkChange ();
                     }
                         
       }
       if (ProgCfg.GetCfgValue ("searchmodea_bas", cfg_v) == TRUE)
       {
		            SetBasSearchMode (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchfielda_bas", cfg_v) == TRUE)
       {
		            SetBasSearchField (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchmodekun", cfg_v) == TRUE)
       {
		            kun_class.SetSearchModeKun (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchfieldkun", cfg_v) == TRUE)
       {
		           kun_class.SetSearchFieldKun (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("wahlccmarkt", cfg_v) == TRUE)
       {
		            cfgccmarkt = atoi (cfg_v);
					if (cfgccmarkt)
					{
 			              EnableKasse ();
					}
       }
       if (ProgCfg.GetCfgValue ("tour_zwang", cfg_v) == TRUE)
       {
                     tour_zwang = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("lief_me_par", cfg_v) == TRUE)
       {
//                     AufKompl.SetLiefMePar (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("split_ls", cfg_v) == TRUE)
       {
//                     AufKompl.SetSplitLs (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("bsd_kz", cfg_v) == TRUE)
	   {
			         AufKompl.SetBsdKz (atoi (cfg_v));

	   }
       if (ProgCfg.GetCfgValue ("tour_datum", cfg_v) == TRUE)
       {
                     tour_datum =  (atoi (cfg_v));
       }

       if (ProgCfg.GetCfgValue ("stat_defp_von", cfg_v) == TRUE)
       {
                     strcpy (stat_defp_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("stat_defp_bis", cfg_v) == TRUE)
       {
                     strcpy (stat_defp_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("stat_deff_von", cfg_v) == TRUE)
       {
                     strcpy (stat_deff_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("stat_deff_bis", cfg_v) == TRUE)
       {
                     strcpy (stat_deff_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("abt_def_von", cfg_v) == TRUE)
       {
                     strcpy (abt_def_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("abt_def_bis", cfg_v) == TRUE)
       {
                     strcpy (abt_def_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("AbtRemoved", cfg_v) == TRUE)
       {
                     AbtRemoved = atol (cfg_v);
//                     AufKompl.SetAbt (AbtRemoved);      
       }
       if (ProgCfg.GetCfgValue ("SmtRemoved", cfg_v) == TRUE) //LAC-202
       {
                     SmtRemoved = atol (cfg_v);
       }

       if (ProgCfg.GetCfgValue ("LuKunRemoved", cfg_v) == TRUE)
       {
                     LuKunRemoved = atol (cfg_v);
//                     AufKompl.SetLuKun (LuKunRemoved);      
       }

       if (ProgCfg.GetCfgValue ("DrkKunRemoved", cfg_v) == TRUE)
       {
                     DrkKunRemoved = atol (cfg_v);
//                     AufKompl.SetDrkKun (DrkKunRemoved);      
       }

       if (ProgCfg.GetCfgValue ("tou_def_zwang", cfg_v) == TRUE)
       {
                     tou_def_zwang = min (max (0, atoi (cfg_v)), 1);
       }

       if (ProgCfg.GetCfgValue ("kun_sperr", cfg_v) == TRUE)
       {
                     kun_sperr = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("vertr_kun", cfg_v) == TRUE)
       {
                     vertr_kun = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("tou_wo_tou", cfg_v) == TRUE)
       {
                     tou_wo_tou = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("use_kopf_txt", cfg_v) == TRUE)
       {
                     use_kopf_txt = min (max (0, atoi (cfg_v)), 1);
       }

	   if (cfgccmarkt) tour_zwang = 0;
	   if (IsC_C ()) tour_zwang = 0;

       if (ProgCfg.GetGroupDefault ("lager", cfg_v) == TRUE)
       {
                     akt_lager = atol (cfg_v);
       }
       if (ProgCfg.GetGroupDefault ("tou_def_von", cfg_v) == TRUE)
       {
                     strcpy (tou_def_von, cfg_v);
       }
       if (ProgCfg.GetGroupDefault ("tou_def_bis", cfg_v) == TRUE)
       {
                     strcpy (tou_def_bis, cfg_v);
       }


/* smt_def_von und tou_def_bis aus 53100.cfg hat Vorrang vor fitgroup.def   */
       if (ProgCfg.GetGroupDefault ("smt_def_von", cfg_v) == TRUE)   //LAC-202
       {
                     strcpy (smt_def_von, cfg_v);
       }
       if (ProgCfg.GetGroupDefault ("smt_def_bis", cfg_v) == TRUE)
       {
                     strcpy (smt_def_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("smt_def_von", cfg_v) == TRUE)
       {
                     strcpy (smt_def_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("smt_def_bis", cfg_v) == TRUE)
       {
                     strcpy (smt_def_bis, cfg_v);
       }


       if (ProgCfg.GetCfgValue ("tou_def_von", cfg_v) == TRUE)
       {
                     strcpy (tou_def_von, cfg_v);
       }

       if (ProgCfg.GetCfgValue ("drk_ls_sperr", cfg_v) == TRUE)
       {
//                     AufKompl.SetDrkLsSperr (atoi(cfg_v));
       }

       if (ProgCfg.GetCfgValue ("datum_plus", cfg_v) == TRUE)
       {
                     datum_plus = atol (cfg_v);
       }

       if (ProgCfg.GetCfgValue ("wo_tag_plus", cfg_v) == TRUE)
       {
                     QTClass.SetWotagplus (atol (cfg_v));
       }

       if (ProgCfg.GetCfgValue ("satour", cfg_v) == TRUE)
       {
                     QTClass.SetSatour (atol (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("sotour", cfg_v) == TRUE)
       {
                     QTClass.SetSotour (atol (cfg_v));
       }
/* tou_def_von und tou_def_bis aus 53100.cfg hat Vorrang vor fitgroup.def   */

       if (ProgCfg.GetCfgValue ("tou_def_von", cfg_v) == TRUE)
       {
                     strcpy (tou_def_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("tou_def_bis", cfg_v) == TRUE)
       {
                     strcpy (tou_def_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("komplett_default", cfg_v) == TRUE)
       {
		             AufKompl.SetKompDefault (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("SaveKomplettDefault", cfg_v) == TRUE)
       {
                     AufKompl.SaveDefault = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("format", cfg_v) == TRUE)
       {
//		             AufKompl.SetDrFormat (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("SAFCOLOR", cfg_v) == TRUE)
       {
		             lspListe.GetListColor (&MessCol, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("SABCOLOR", cfg_v) == TRUE)
       {
		             lspListe.GetListColor (&MessBkCol, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("with_log", cfg_v) == TRUE)
       {
//		             AufKompl.SetLog (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("komplettmode", cfg_v) == TRUE)
       {
                     KomplettMode = min (max (0, atoi (cfg_v)), 2);
       }
       if (ProgCfg.GetCfgValue ("TestLiefMe0", cfg_v) == TRUE)
	   {
					 KompAuf::cfg_TestLiefMe0 = atoi (cfg_v);
	   }

       if (ProgCfg.GetCfgValue ("SqlDebug", cfg_v) == TRUE)
       {
                     int SqlDebug = atoi (cfg_v);
					 if ((SqlDebug == 1) || SqlDebug == 3)
					 {
						 KompAuf::RosiSqlDebug = TRUE;
						 KompAuf::CreateDebugFile ();
					 }
					 if ((SqlDebug == 2) || SqlDebug == 3)
					 {
						 SetCSqlDebug (1);
					 }
                         
       }
       if (ProgCfg.GetCfgValue ("autodatum", cfg_v) == TRUE)
       {
                     autosysdat = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("autokunfil", cfg_v) == TRUE)
       {
                     autokunfil = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("listedirect", cfg_v) == TRUE)
       {
                     listedirect = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("telelist", cfg_v) == TRUE)
       {
                     telelist = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ldatdiffminus", cfg_v) == TRUE)
       {
                     ldatdiffminus = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ldatdiffplus", cfg_v) == TRUE)
       {
                     ldatdiffplus = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ldatdiff", cfg_v) == TRUE)
       {
                     ldatdiffplus = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("lsdirect", cfg_v) == TRUE)
       {
                     lsdirect = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ListBkColor", cfg_v) == TRUE)
       {
		             SetListBkColor (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("ListBkColor", cfg_v) == TRUE)
       {
		             SetListBkColor (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("ListLine", cfg_v) == TRUE)
       {
		             SetListLine (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("auf_art", cfg_v) == TRUE)
       {
		   if (atoi (cfg_v))
		   {
		             EnableAufArt ();
		   }
	   }
       if (ProgCfg.GetCfgValue ("akt_preis", cfg_v) == TRUE)
       {
                     akt_preis = atol (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("auftou", cfg_v) == TRUE)
       {
                     auftou = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("TestDivAdr", cfg_v) == TRUE)
       {
                     TestDivAdr = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ClipDial", cfg_v) == TRUE)
       {
                      ClipDial = new char [strlen (cfg_v) + 1];
                      strcpy (ClipDial, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DdeDial", cfg_v) == TRUE)
       {
                      DdeDialProg = new char [strlen (cfg_v) + 1];
                      strcpy (DdeDialProg, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DdeDialActive", cfg_v) == TRUE)
       {
                      DdeDialActive = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DdeDialService", cfg_v) == TRUE)
       {
                      DdeDialService = new char [strlen (cfg_v) + 1];
                      strcpy (DdeDialService, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DdeDialTopic", cfg_v) == TRUE)
       {
                      DdeDialTopic = new char [strlen (cfg_v) + 1];
                      strcpy (DdeDialTopic, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DdeDialItem", cfg_v) == TRUE)
       {
                      DdeDialItem = new char [strlen (cfg_v) + 1];
                      strcpy (DdeDialItem, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("KunLiefKz", cfg_v) == TRUE)
       {
                      KunLiefKz = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("SearchKunDirect", cfg_v) == TRUE)
       {
                      SearchKunDirect = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("MdnDirect", cfg_v) == TRUE)
       {
                      MdnDirect = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("Color3D", cfg_v) == TRUE)
       {
                      Color3D = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("UpdatePersNam", cfg_v) == TRUE)
       {
                      UpdatePersNam = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("Wochentag", cfg_v) == TRUE)
       {
                      Wochentag = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("KreditLimit", cfg_v) == TRUE)
       {
                      KreditLimit = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("UniqueKun", cfg_v) == TRUE)
       {
                      UniqueKun = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("OpenPosMessage", cfg_v) == TRUE)
       {
                      OpenPosMessage = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("EnableKun", cfg_v) == TRUE)
       {
                     EnableKunDirect = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("KunDirect", cfg_v) == TRUE)
       {
                     KunDirect = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("KasseDirect", cfg_v) == TRUE)
	   {
		             KompAuf::KasseDirect = atol (cfg_v);
					 KompAuf::KasseDirect = (KompAuf::KasseDirect > 0) ? TRUE : FALSE;
	   }
       if (ProgCfg.GetCfgValue ("FixDat", cfg_v) == TRUE)
       {
                     FixDat = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("FitParams", cfg_v) == TRUE)
       {
                     FitParams = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("BarKunde", cfg_v) == TRUE)
       {
					KompAuf::BarKunde = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ScrollChoice", cfg_v) == TRUE)
       {
					ScrollChoice = atol (cfg_v);
       }
       if (ProgCfg.GetGroupDefault ("Kassennr", cfg_v) == TRUE)
	   {
                     ls_class.SetKassennr (atol (cfg_v));

	   }
       if (ProgCfg.GetCfgValue ("KompTestVk0", cfg_v) == TRUE)
	   {
                     KompTestVk0  = atol (cfg_v);

	   }

       if (ProgCfg.GetCfgValue ("KompOPDefault", cfg_v) == TRUE)
	   {
                     AufKompl.KompOPDefault  = atol (cfg_v);

	   }

       if (ProgCfg.GetCfgValue ("SmtTest", cfg_v) == TRUE)
	   {
					 smtHandler.set_SmtTest (atoi (cfg_v));

	   }

	   StapelTour = FALSE;
       if (ProgCfg.GetCfgValue ("StapelTour", cfg_v) == TRUE)
	   {
		             /* 26.03.2012 GK */
					 StapelTour = atoi (cfg_v);
	   }

       if (ProgCfg.GetCfgValue ("Stapeldruck", cfg_v) == TRUE) 
       {
                     strcpy (iformStapeldruck, cfg_v);
       }
/*
       if (ProgCfg.GetGlobDefault ("ll_ls_dr", cfg_v) == TRUE)
	   {
                     KompAuf::ll_ls_par = atol (cfg_v);

	   }
*/
       if (ProgCfg.GetGlobDefault ("lllsfo", cfg_v) == TRUE)
	   {
                     AufKompl.SetLllsfo (atol (cfg_v));

	   }
	   lspListe.SetLager (akt_lager);
       AufKompl.SetProgCfg (&ProgCfg);
}

void MoveMamain (void)
/**
Koordinaten in $BWSETC lesen.
**/
{
        char *etc;
        char buffer [256];
        FILE *fp;
        int anz;
		static BOOL scrfOK = FALSE;
		RECT rect;
	    int xfull, yfull;



	    if (MoveMain == FALSE) return;
	    if (Startsize > 0) return;
        xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
	    if (xfull < 900) return;

        etc = getenv ("BWSETC");
        if (etc == NULL)
        {
             etc = "\\user\\fit\\etc";
        }
        sprintf (buffer, "%s\\fit.rct", etc);

        fp = fopen (buffer, "r");
        if (fp == NULL) return;

        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        fclose (fp);

        anz = wsplit (buffer, " ");
        if (anz < 4) 
        {
            return;
        }

        rect.left   = atoi (wort[0]);
        rect.top    = atoi (wort[1]);
        rect.right  = atoi (wort[2]);
        rect.bottom = atoi (wort[3]);
		rect.left ++; 
		rect.top ++; 
		rect.right  = rect.right  - rect.left - 2;
		rect.bottom = rect.bottom - rect.top - 2;
        MoveWindow (hMainWindow, rect.left, rect.top, rect.right, rect.bottom, TRUE);
        return;
}

void SetcButtons (COLORREF Col, COLORREF BkCol)
/**
Farbe der Button setzen.
**/
{
	    int i;

		for (i = 0; cButtons[i]; i ++)
		{
                 cButtons[i]->Color   = Col;
				 cButtons[i]->BkColor = BkCol;
		}
}

void SaveRect (void)
/**
Aktuelle Windowgroesse sichern.
**/
{
	    char *etc;
		char rectname [512];
		RECT rect;
		FILE *fp;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		sprintf (rectname, "%s\\%s.rct", etc,Programm);
		GetWindowRect (hMainWindow, &rect);

		fp = fopen (rectname, "w");
		fprintf (fp, "left    %d\n", rect.left);
		fprintf (fp, "top     %d\n", rect.top);
		fprintf (fp, "right   %d\n", rect.right - rect.left);
		fprintf (fp, "bottom  %d\n", rect.bottom - rect.top);
		fclose (fp);
}


void MoveRect (void)
/**
Fenster auf gesicherte Daten anpassen.
**/
{
	    char *etc;
		char buffer [512];
		RECT rect;
		FILE *fp;
		int x, y, cx, cy;
		int anz;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		GetWindowRect (hMainWindow, &rect);
		x = rect.left;
		y = rect.top;
		cx = rect.right;
		cy = rect.bottom;
		sprintf (buffer, "%s\\%s.rct", etc,Programm);
		fp = fopen (buffer, "r");
		if (fp == NULL) return;
		while (fgets (buffer, 511, fp))
		{
			cr_weg (buffer);
			anz = wsplit (buffer, " ");
			if (anz < 2) continue;
			if (strcmp (wort[0], "left") == 0)
			{
				x = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "top") == 0)
			{
				y = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "right") == 0)
			{
				cx = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "bottom") == 0)
			{
				cy = atoi (wort[1]);
			}
		}
		fclose (fp);
		MoveWindow (hMainWindow, x, y, cx, cy, TRUE);
}

BOOL TestMenue (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == KEY5)
        {
            SendMessage (hMainWindow, WM_COMMAND, KEY5, 0l);
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_MUSTER)
        {
            SetMusterAuftrag ();
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_PARTYSERVICE)
        {
            SetPartyservice ();
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_FIRST_ROW)
        {
			LskChoiceFirstRow ();
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_LAST_ROW)
        {
			LskChoiceLastRow ();
            return TRUE;
        }
        return FALSE;
}

void TestLizenz ()
{
		if (FitParams == FALSE)
// M�glichkeit, die Lizenzabfrage abzuschalten.
		{
        switch (Fitl.TestLizenz ())
		{
		       case 0 :
				    break;
			   case 1:
				    print_mess (2, "Status-Datei nicht vorhanden oder ung�ltiges Format");
					ExitProcess (1);
			   case 2:
				    PrintLM = TRUE;
				    Fitl.LizenzMessage (hMainInst, hMainWindow);
					break;
			   case 3:
				    Fitl.DisableLizenz (hMainInst, hMainWindow);
					ExitProcess (1);
		}
		}

}


int    PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
       int anz; 
       int i;
       char argtab[20][80];
       char *varargs[20];
	   char *mv;
	   char *tr;
       char *cmdl;
       extern MDNFIL cmdnfil;
    
	   CApplication::GetInstance ()->SetAppInstance (hInstance);

       anz = wsplit (lpszCmdLine, " ");
	   for (i = 0; i < anz; i ++)
	   {
		   if (memcmp (wort[i], "menuedirect=", 12) == 0)
		   {
			   if (memcmp (&wort[i][12], "0", 1) == 0) Menuedirect = 0;
			   if (memcmp (&wort[i][12], "1", 1) == 0) Menuedirect = 1;
			   if (memcmp (&wort[i][12], "2", 1) == 0) Menuedirect = 2;
			   if (memcmp (&wort[i][12], "3", 1) == 0) Menuedirect = 3;
			   if (memcmp (&wort[i][12], "4", 1) == 0) Menuedirect = 4;

		   }

		   if (memcmp (wort[i], "nomenu=", 7) == 0)
		   {
			   if (memcmp (&wort[i][7], "true", 4) == 0)
			   {
				   NoMenue = TRUE;
			   }

			   else if (memcmp (&wort[i][7], "false", 5) == 0)
			   {
				   NoMenue = FALSE;
			   }
		   }
		   else if (memcmp (wort[i], "mdn=", 4) == 0)
		   {
			   StartMdn = atoi (&wort[i][4]);
		   }
	   }

       if (anz > 0 && strcmp (wort[0], "51200") == 0)
       {
           ProgCfg.SetProgName (wort[0]);
           lspListe.SetCfgProgName (wort[0]);
           cmdl = lpszCmdLine + 6;
           while (*cmdl <= ' ')
           {
               if (*cmdl == 0) break;
               cmdl += 1;
           }

       }
       else
       {
           cmdl = lpszCmdLine;
       }


	   Token cmd;
	   cmd.SetSep (" ");
	   cmd = lpszCmdLine;
	   char *lstoken;
	   while ((lstoken = cmd.NextToken ()) != NULL)
	   {
		   Text LsToken = lstoken;
		   LsToken.MakeUpper ();
		   Text Property = LsToken.SubString (0, 3);
		   if (Property != "LS=") continue;
		   Token Value;
		   Value.SetSep (";");
           Value = LsToken.SubString (3, 50);
		   int c = Value.GetAnzToken ();
		   if (c > 0)
		   {
			   sprintf (mdn, "%hd", atoi (Value.NextToken ()));
			   cmdnfil.FixMdn = atoi (mdn);
		   }
		   if (c > 1)
		   {
			   sprintf (fil, "%hd", atoi (Value.NextToken ()));
			   cmdnfil.FixFil = atoi (fil);
		   }
		   if (c > 2)
		   {
		       LsNr = atol (Value.NextToken ());
		   }
		   LsDirect = TRUE;
		   break;
	   }
        

	   SetPlugins ();
	   AufKompl.SetPlugins ();

       MenAccelerators = LoadAccelerators (hInstance, "MENACCELERATORS");
	   GetForeground ();   
	   SetcButtons (BLACKCOL, LTGRAYCOL);

	   ColBorder = TRUE;
       tr = getenv_default ("COLBORDER");
	   if (tr)
	   {
			ColBorder = max(0, min (1, atoi (tr)));
	   }
	   ArrDown = LoadBitmap (hInstance, "ARRDOWN");
	   iarrdown.SetFeldPtr ((char *) &ArrDown);

       Cfrei_txt2.bmp = LoadBitmap (hInstance, "Achtung");
//       Cfrei_txt2.bmp = BMAP::LoadBitmap (hInstance, "AchtungTF", "AchtungTB", LTGRAYCOL);

//       SelBmp = BMAP::LoadBitmap (hInstance, "SEL",   "MSK",  GetSysColor (COLOR_3DFACE));
       SelBmp = BMAP::LoadBitmap (hInstance, "SEL",   "MSK",  LTGRAYCOL);
	   GetCfgValues ();
       opendbase ("bws");
	   FreeLsk ();
//       menue_class.SetSysBen (cmdl);
       menue_class.SetSysBenPers ();
       SetListLine ("HT");
       if (KunLiefKz && DbClass.sqlcomm ("select * from kunlief") != 0)
       {
           KunLiefKz = FALSE;
       }
	   mv = getenv_default ("MOVEMAIN");
	   if (mv)
	   {
		   MoveMain = min (1, max (0, atoi (mv)));
	   }

	   AufKompl.KompParams ();
       memcpy (&sys_par, &sys_par_null,  sizeof (struct SYS_PAR));
       strcpy (sys_par.sys_par_nam,"fen_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             fen_par = atoi (sys_par.sys_par_wrt);
             }
       }
       strcpy (sys_par.sys_par_nam,"lutz_mdn_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             lutz_mdn_par = atoi (sys_par.sys_par_wrt);
							 QClass.SetLutzMdnPar (lutz_mdn_par);
//							 AufKompl.SetLutzMdnPar (lutz_mdn_par);
             }
       }
       strcpy (sys_par.sys_par_nam,"fil_lief_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             fil_lief_par = atoi (sys_par.sys_par_wrt);
             }
       }
	   if (fil_lief_par == FALSE && sys_ben.fil)
	   {
		     disp_mess ("Lieferscheinerfassung f�r Filialen ist nicht erlaubt", 2);
			 ExitProcess (1);
	   }
       strcpy (sys_par.sys_par_nam,"klst_dr_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             AufKompl.SetKlstPar (atoi (sys_par.sys_par_wrt));
       }
       strcpy (sys_par.sys_par_nam,"tourlang");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             switch (atoi (sys_par.sys_par_wrt))
							 {
							        case 4 :
                                           tour_div = (long) 100;
										   break;
									case 5 :
                                           tour_div = (long) 1000;
										   break;
							 }
             }
       }
	   if (tou_wo_tou)
	   {
                strcpy (sys_par.sys_par_nam,"tou_par");
                if (sys_par_class.dbreadfirst () == 0)
				{
                         tou_par = atoi (sys_par.sys_par_wrt);
				}
				if (tou_par == 0)
				{
		                 SetItemAttr (&aufform, "tou", REMOVED);
		                 SetItemAttr (&aufform, "tou_bz", REMOVED);
						 aufform.mask[GetItemPos (&aufform, "tou") + 1].attribut = REMOVED;
						 tour_zwang = 0;
				}
				else
				{
                         strcpy (sys_par.sys_par_nam,"wo_tou_par");
                         if (sys_par_class.dbreadfirst () == 0)
						 {
                                    wo_tou_par = atoi (sys_par.sys_par_wrt);
						 }
						 if (wo_tou_par == 0) tour_div = (long) 1;
				}
       }
       strcpy (sys_par.sys_par_nam,"vertr_abr_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
                 vertr_abr_par = atoi (sys_par.sys_par_wrt);
       }
	   if (vertr_abr_par == FALSE)
	   {
		   SetItemAttr (&aufform, "vertr", REMOVED);
		   SetItemAttr (&aufform, "vertr_krz", REMOVED);
	   }
       strcpy (sys_par.sys_par_nam,"bg_cc_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             bg_cc_par = atoi (sys_par.sys_par_wrt);
             }
       }
       strcpy (sys_par.sys_par_nam,"zustell_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             zustell_par = atoi (sys_par.sys_par_wrt);
             }
       }
	   if (bg_cc_par && zustell_par)
	   {
		     EnableAufArt ();
	   }
	   if (IsC_C ())
	   {
		   SetItemAttr (&aufform, "tou", REMOVED);
		   SetItemAttr (&aufform, "tou_bz", REMOVED);
		   aufform.mask[GetItemPos (&aufform, "tou") + 1].attribut = REMOVED;
	   }

       anz = wsplit (lpszCmdLine, " ");
       for (i = 0; i < anz; i ++)
       {
            strcpy (argtab[i], wort[i]);
            varargs[i] = (char *) argtab[i];
       }
       argtst (&anz, varargs, tst_arg);

       SetStdProc (WndProc);
       InitFirstInstance (hInstance);
       InitNewInstance (hInstance, nCmdShow);

       CFirstRow.bmp      = BMAP::LoadBitmap (hInstance, MAKEINTRESOURCE (IDB_FIRST_ROW), 
							  MAKEINTRESOURCE (IDB_FIRST_ROW_MASK), StdBackCol);
       CPriorRow.bmp      = BMAP::LoadBitmap (hInstance, MAKEINTRESOURCE (IDB_PRIOR_ROW), 
							  MAKEINTRESOURCE (IDB_PRIOR_ROW_MASK), StdBackCol);
       CNextRow.bmp      = BMAP::LoadBitmap (hInstance, MAKEINTRESOURCE (IDB_NEXT_ROW), 
							  MAKEINTRESOURCE (IDB_NEXT_ROW_MASK), StdBackCol);
       CLastRow.bmp      = BMAP::LoadBitmap (hInstance, MAKEINTRESOURCE (IDB_LAST_ROW), 
							  MAKEINTRESOURCE (IDB_LAST_ROW_MASK), StdBackCol);

       CFirstRow.BkColor = StdBackCol;
       CPriorRow.BkColor = StdBackCol;
       CNextRow.BkColor  = StdBackCol;
       CLastRow.BkColor  = StdBackCol;

	   TestLizenz ();

       if (WithToolBar)
       {
	               hwndTB = MakeToolBarEx (hInstance, 
		                       hMainWindow,tbb, 56 ,
                               qInfo, qIdfrom,
		                       qhWndInfo, qhWndFrom);

                   hwndCombo1 = MakeToolBarCombo (hInstance,
                                      hMainWindow,
                                      hwndTB, 
                                      10, 
                                      11, 
                                      32);

                   FillCombo1 ();
                   ComboToolTip (hwndTB, hwndCombo1);
 
                   hwndCombo2 = MakeToolBarCombo (hInstance,
                                                  hMainWindow,
                                                  hwndTB, 
                                                  11, 
                                                  33, 
                                                  52);

                   FillCombo2 ();
                   ComboToolTip (hwndTB, hwndCombo2);


                   lspListe.SethwndTB (hwndTB);
                   DisableTB ();
       }

       if (NewStyle)
       {
//             AddFktButton (MusterButton);
			 	SetPartyActive ();
			 if (ScrollChoice)
			 {
				AddFktButton (_fRowButtons[0]);
				AddFktButton (_fRowButtons[1]);
				AddFktButton (_fRowButtons[2]);
				AddFktButton (_fRowButtons[3]);
			 }
             ftasten = OpenFktEx (hInstance);
             ITEM::SetHelpName ("51400.cmd");
             CreateFktBitmap (hInstance, hMainWindow, hwndTB);
             SetFktMenue (TestMenue);
       }
       else
       {
             ftasten = OpenFktM (hInstance);
       }
       Mess.OpenMessage ();
       Createmamain1 ();
       MoveMamain ();
       if (WithMenue)
       {
	               hMenu = MakeMenue (menuetab);
	               SetMenu (hMainWindow, hMenu);
                   lspListe.SetMenu (hMenu);
       }
	   if (Startsize == 1)
	   {
	               ShowWindow (hMainWindow, SW_SHOWMAXIMIZED);
	   }
	   else if (Startsize == 2)
	   {
	               MoveRect ();
	   }

       SetEnvFont ();
	   GetStdFont (&BuFont);
       lstxtlist.SetListFont (&lFont);
       lspListe.SetListFont (&lFont);


       telefon1     = LoadIcon (hMainInst, "tele1");
       fax          = LoadIcon (hMainInst, "fax");

       btelefon       = LoadBitmap (hInstance, "btele");
       btelefoni      = LoadBitmap (hInstance, "btelei");
       CTelefon.bmp   = btelefoni;

       display_form (mamain1, &buform, 0, 0);
       MoveButtons ();
       CreateQuikInfos ();


       CheckMenuItem (hMenu, IDM_PAGEVIEW, MF_UNCHECKED); 
       ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
       ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);

	   lspListe.NoteIcon = LoadBitmap (hInstance, "NOTEICON");
       lspListe.SethMainWindow (mamain1);
//       EingabeMdnFil ();
 
	   lspListe.smtHandler = &smtHandler;
//	   SetPartyActive ();
       StartMenu ();

//       ProcessMessages ();
	   FreeLsk ();
	   SaveRect ();
       TestActiveDial ();
       SetForeground ();
	   DestroyChoiceLsk ();
	   closedbase ();
       return 0;
}


void InitFirstInstance(HANDLE hInstance)
{
        
        WNDCLASS wc;
		HDC hdc;
		int col;

		col = 0;
		hdc = GetDC (NULL);
		col = GetDeviceCaps (hdc, BITSPIXEL);
		ReleaseDC (NULL, hdc);
        if (col < 16) ColBorder = FALSE;

		SetEnvFont ();

		strcpy (musterfont.FontName, FontName);
        musterfont.FontHeight = FontHeight;
        musterfont.FontWidth  = FontWidth;       
        musterfont.FontAttribute  = FontAttribute;       

		/***
		strcpy (partyfont.FontName, FontName);
        partyfont.FontHeight = FontHeight;
        partyfont.FontWidth  = FontWidth;       
        partyfont.FontAttribute  = FontAttribute;       
		**/

		SetSWEnvFont ();

		strcpy (lFont.FontName, FontNameSW);
        lFont.FontHeight = FontHeightSW;
        lFont.FontWidth  = FontWidthSW;       
        lFont.FontAttribute  = FontAttributeSW;       


        if (Color3D)
        {
             StdBackCol = GetSysColor (COLOR_3DFACE);
             SetColor3D (TRUE);
             CMuster.BkColor = StdBackCol;
             CParty.BkColor = StdBackCol;
  	         SetcButtons (BLACKCOL, StdBackCol);
        }

		if (hbrBackground == NULL)
		{
			hbrBackground = CreateSolidBrush (StdBackCol);
		}


        wc.style         =  CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "FITICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
//        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
//        wc.hbrBackground =  CreateSolidBrush (StdBackCol);
        wc.hbrBackground =  hbrBackground;
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "hStdWindow";

        RegisterClass(&wc);

        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "ListMain";
        RegisterClass(&wc);


        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hListWindow";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  StaticWProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "StaticWhite";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  StaticWProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.hbrBackground =  GetStockObject (GRAY_BRUSH);
        wc.lpszClassName =  "StaticGray";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  WndProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.hbrBackground =  CreateSolidBrush (MessBkCol);
        wc.lpszClassName =  "StaticMess";
        RegisterClass(&wc);
        hMainInst = hInstance;
        return;
}


BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        HDC hdc;
        HFONT hFont, oldFont;
        char *Caption;
        SIZE size;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (0, hdc);
        lspListe.SetTextMetric (&tm);
        if (InfoCaption[0])
        {
            Caption = InfoCaption;
        }
        else
        {
            Caption = "Lieferscheinbearbeitung";
        }

        SetBorder (WS_THICKFRAME | WS_CAPTION | 
                   WS_SYSMENU | WS_MINIMIZEBOX |
                   WS_MAXIMIZEBOX);

        hMainWindow = OpenWindowChC (26, 80, 2, 0, hInstance, 
                      Caption);
        return 0; 
}


static int ProcessMessages(void)
{
        MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                  syskey = KEY5;
                                  PostQuitMessage (0);
                                  continue; ;
                              case VK_RETURN :
                                  syskey = KEYCR;
                                  EingabeMdnFil ();
                                  continue; ;
                       }
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
        }
        return msg.wParam;
}

void InvalidateLines (void)
{
         RECT rect;
         static TEXTMETRIC tm;
         HDC hdc;

         return;

         hdc = GetDC (hMainWindow);
         GetTextMetrics (hdc, &tm);
         ReleaseDC (hMainWindow, hdc);
         GetClientRect (hMainWindow, &rect);

         rect.top = 2 * tm.tmHeight;
         rect.top = rect.top + rect.top / 3;
         rect.top -= 14;
         rect.bottom = rect.top + 10;
         InvalidateRect (hMainWindow, &rect, TRUE);
}


void PrintLines (HDC hdc)
/**
Linien am Bildschirm anzeigen.
**/
{
         static TEXTMETRIC tm;
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         static HPEN hPenB = NULL;
         RECT rect;
		 RECT wrect;
         int x, y;
         HFONT hFont, oldfont;

         stdfont ();
         hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
         oldfont = SelectObject (hdc,hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (SelectObject (hdc, oldfont));
         y = 2 * tm.tmHeight;
         y = y + y / 3;
		 y -= 14;
         GetClientRect (mamain1, &rect);
         GetWindowRect (mamain1, &wrect);
         x = rect.right - 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
                   hPenB = CreatePen (PS_SOLID, 0, BLACKCOL);
         }

/* Linie oben                 */

         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

/* Linie mitte                */
         
		 y += 5 * tm.tmHeight;
		 y += tm.tmHeight / 2;
         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);
		 y -= tm.tmHeight / 2;


/* Linie unten                */
         
		 y += 6 * tm.tmHeight;
         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

		 y += (1 + wrect.top);
		 lspListe.SetLiney (y);
         lspListe.MoveMamain1 ();
}

void DisplayLines ()
{
         HDC hdc;

         hdc = GetDC (mamain1);
         PrintLines (hdc);
         ReleaseDC (mamain1, hdc);
}

void ChoiseCombo1 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo1,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo1[i]; i ++)
          {
              if (strcmp (Combo1[i],text) == 0)
              {
                   lspListe.SetListLines (i);
                   break;
              }
          }
}

void ChoiseCombo2 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo2,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo2[i]; i ++)
          {
              if (strcmp (Combo2[i],text) == 0)
              {
                   lspListe.SetColors (Colors[i], BkColors[i]);
                   break;
              }
          }
}

void disp_forms (void)
{
      if (aufformk.mask[0].feldid) display_form (mamain1, &aufformk, 0, 0);
      if (aufform.mask[0].feldid)  display_form (mamain1, &aufform, 0, 0);
}



int FaxAuf (void)
{
	      int ret;
		  FORM *scurrent;
		  int   sfield;
          char buffer [256];


		  scurrent = current_form;
		  sfield = currentfield;
	      NoClose = TRUE;
/***************
          if (lspListe.GetPosanz () == 0)
		  {
					 disp_mess ("Es wurden kein Positionen erfasst", 2);
					 SetCurrentField (currentfield);
					 return (1);
		  }
*************/
 		  if (testkun0 () == FALSE)
		  {
			         NoClose = FALSE;
					 return 1;
		  }
	      if (TouZTest () == FALSE) return 0;

	      if (abfragejn (hMainWindow, "Auftrag faxen ?", "J") == FALSE)
		  {
			         NoClose = FALSE;
		             current_form = scurrent;
		             currentfield = sfield;
					 return 1;
		  }

          lspListe.DestroyWindows ();
		  if (lutz_mdn_par && lsk.ls_stat < 3)
		  {
			  if (MultiMdnProd ())
			  {
				  FormToAuf (2);
			  }
			  else
			  {
				  FormToAuf (3);
			  }
		  }
		  else
		  {
              FormToAuf (3);
		  }
          ls_class.update_lsk (atoi (mdn), atoi (fil), lsk.ls);
          if (lsk.ls != auto_nr.nr_nr)
          {
					   if (lutz_mdn_par)
					   {
						   sprintf (mdn, "%hd", 0);
						   sprintf (fil, "%hd", 0);
					   }

// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "ls",  auto_nr.nr_nr);
          }


          clipped (_adr.fax);
          if (FaxTrenner != 0 && (wsplit (_adr.fax, "/")) > 1)
          {
              sprintf (buffer, "%s%c%s", wort[0], FaxTrenner, wort[1]);
              ToClipboard (buffer);
          }
          else
          {
              ToClipboard (_adr.fax);
          }
		  ret = AufKompl.FaxAuf (hMainWindow, lsk.mdn, lsk.fil, 
			                                      lsk.ls);
          DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
          DbClass.sqlin ((short *) &lsk.fil, 1, 0);
          DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
          DbClass.sqlcomm ("update lsk set ls_stat = 4 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and ls = ? ");
          lsk.ls_stat = 4;
          syskey = KEY12;
          break_enter ();
          NoClose = FALSE;
          current_form = scurrent;
          currentfield = sfield;
          return 0;
}

void TestActiveDial (void)
{ 
          DWORD ExitCode;

          if (DialPid != NULL)
          {
 		        GetExitCodeProcess (DialPid, &ExitCode);
		        if (ExitCode == STILL_ACTIVE)
                {
	                     TerminateProcess (DialPid, 0);
                }
          }
          DialPid = NULL;
}


int RunClipDial (char *tel)
{
          char buffer [256];

          if (FaxTrenner != 0 && (wsplit (tel, "/")) > 1)
          {
              sprintf (buffer, "%s%c%s", wort[0], FaxTrenner, wort[1]);
              ToClipboard (buffer);
          }
          else
          {
              ToClipboard (tel);
          }

          TestActiveDial ();

          DialPid = ProcExecPid (ClipDial, SW_SHOWNORMAL, -1, 0, -1, 0);
          if (DialPid == NULL)
          {
              print_mess (2, "%s konnte nicht gestartet werden", ClipDial);
          }
          return 0;
}

int RunDdeDial (char *tel)
{
          char buffer [256];

          if (FaxTrenner != 0 && (wsplit (tel, "/")) > 1)
          {
              sprintf (buffer, "%s%c%s", wort[0], FaxTrenner, wort[1]);
          }
          else
          {
              strcpy (buffer, tel);
          }

//          TestActiveDial ();

          if (DdeDialActive == FALSE)
          {
              DialPid = ProcExecPid (DdeDialProg, SW_SHOWMINNOACTIVE, -1, 0, -1, 0);
              if (DialPid == NULL)
              {
                  print_mess (2, "%s konnte nicht gestartet werden", DdeDialProg);
                  return 0;
              }
              Sleep (1000);
          }
          DdeDial *ddeDial = new DdeDial ();
          ddeDial->SetServer (DdeDialProg);
          if (DdeDialService != NULL)
          {
              ddeDial->SetService (DdeDialService);
          }
          if (DdeDialTopic != NULL)
          {
              ddeDial->SetTopic (DdeDialTopic);
          }
          if (DdeDialItem != NULL)
          {
              ddeDial->SetItem (DdeDialItem);
          }
          ddeDial->Call (buffer);
          delete ddeDial;
          return 0;
}

void SetMuster (BOOL b)
{
          if (b == FALSE)
          {
              lspListe.SetMuster (FALSE);
              CheckMenuItem (hMenu, IDM_MUSTER, MF_UNCHECKED);
              CMuster.bmp = NULL;
              DisplayFktBitmap ();
              if (fil_lief_par == FALSE)
              {
                      CloseControls (&musterf);
              }
          }
          else
          {
              lspListe.SetMuster (TRUE);
              CheckMenuItem (hMenu, IDM_MUSTER, MF_CHECKED);
              CMuster.bmp = SelBmp;
              DisplayFktBitmap ();
              if (fil_lief_par == FALSE)
              {
                      display_form (mamain1, &musterf, 0, 0);
              }
          }
}
void SetParty (BOOL b)
{
          if (b == FALSE)
          {
              CheckPartyService (FALSE);
              CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_UNCHECKED);
              CParty.bmp = NULL;
              DisplayFktBitmap (); 
              if (PartyKz == TRUE)
              {
                      CloseControls (&partyf);
              }
          }
          else
          {
              CheckPartyService (TRUE);
              CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_CHECKED);
              CParty.bmp = SelBmp;
              DisplayFktBitmap ();
          }
}

void SetPartyservice (void)
{
          if (IsPartyService )
          {
              CheckPartyService (FALSE);
              CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_UNCHECKED);
              CParty.bmp = NULL;
              DisplayFktBitmap (); 
              if (PartyKz == TRUE)
              {
                      CloseControls (&partyf);
              }
          }
          else
          {
              CheckPartyService (TRUE);
              CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_CHECKED);
              CParty.bmp = SelBmp;
              DisplayFktBitmap ();
          }
}

void SetMusterAuftrag (void)
{
          if (lspListe.GetMuster ())
          {
              lspListe.SetMuster (FALSE);
              CheckMenuItem (hMenu, IDM_MUSTER, MF_UNCHECKED);
              CMuster.bmp = NULL;
              DisplayFktBitmap ();
              if (fil_lief_par == FALSE)
              {
                      CloseControls (&musterf);
              }
          }
          else
          {
              lspListe.SetMuster (TRUE);
              CheckMenuItem (hMenu, IDM_MUSTER, MF_CHECKED);
              CMuster.bmp = SelBmp;
              DisplayFktBitmap ();
              if (fil_lief_par == FALSE)
              {
                      display_form (mamain1, &musterf, 0, 0);
              }
          }
}


void SetKunLief (void)
{
/*
          int KunPos;

          if (KunLief)
          {
               CheckMenuItem (hMenu, IDM_KUNLIEF, MF_UNCHECKED);
                              KunLief = FALSE;
 	           KunPos = GetItemPos (&aufform, "kun_txt");
               CloseControl (&aufform, KunPos);
               CloseControl (&aufform, KunPos + 1);
               aufform.mask[KunPos].item     = &ikunstxt;
               aufform.mask[KunPos + 1].item = &ikuns;
          }
          else
          {
               CheckMenuItem (hMenu, IDM_KUNLIEF, MF_CHECKED);
                              KunLief = TRUE;
 	           KunPos = GetItemPos (&aufform, "kun_txt");
               CloseControl (&aufform, KunPos);
               CloseControl (&aufform, KunPos + 1);
               aufform.mask[KunPos].item     = &ikunlstxt;
               aufform.mask[KunPos + 1].item = &ikunliefs;
               display_form (mamain1, &aufform);
          }
          display_field (mamain1, &aufform.mask[KunPos], 0, 0);
          display_field (mamain1, &aufform.mask[KunPos + 1], 0, 0);
*/
}

int ShowErrors ()
{
	SEARCHERROR errors;

    errors.SetParams (hMainInst, mamain1);
    errors.Setawin (hMainWindow);

     errors.Search ();
	 if (ListeOK)
	 {
	       lspListe.SetListFocus ();
		   }
     return 0;

}

void Kasse ()
{
	  Text Progname;
	  if (atol (kuns) != 0l)
	  {
	        Progname.Format ("-Kunde=%ld %s", atol (kuns), sys_ben.pers_nam);
	  }
	  else
	  {
	        Progname.Format ("%s", sys_ben.pers_nam);
	  }
      StartKasse (hMainInst, Progname.GetBuffer (), SW_SHOWNORMAL);
}


void CheckPartyService (BOOL b)
{
	if (b)
	{
		IsPartyService = TRUE;
        CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_CHECKED);
        EnableFullTax ();
		if (lspListe.ContainsService ())
		{
			SetToFullTax ();
			EnableFullTax ();
		}
		else 
		{
			SetToStandardTax ();
		}
	}
	else
	{
		IsPartyService = FALSE;
		lsk.psteuer_kz = 0;
        CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_UNCHECKED);
        EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
		SetToStandardTax ();
		lsk.psteuer_kz = 0;
	}
}


void SetPartyActive ()
{
     memcpy (&sys_par, &sys_par_null, 
                   sizeof (struct SYS_PAR));
     strcpy (sys_par.sys_par_nam,"party_kz");
     if (sys_par_class.dbreadfirst () == 0)
     {

// Nur auf speziellen Pl�tzen �berhaupt Party-Mechanik zulassen	// 130114
		 if ( PartyKz_cfg == TRUE )
		 {
			 // erst jetzt gilt der Systemparameter : Restproblem bleibt :
			 // einer bearbeitet diesen Beleg auf einem Party-Platz, danach macht jemand an einem Nicht-Party-Platz herum ...
			 // Also : Beleg muss immer komplett am Party-Platz bis zur Freigabe bearbeitet werden, sonst gibt es Stress ....

		 }
		 else
		 {
			strcpy ( sys_par.sys_par_wrt ,"0" ) ;
		 }


         if (atoi (sys_par.sys_par_wrt) == 0) 
         {
			 CheckPartyService (FALSE);
			 HMENU SubMenu = GetSubMenu (hMenu, 1);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DrawMenuBar (hMainWindow);
		 }
         else
		 {
			PartyKz = (short) atoi (sys_par.sys_par_wrt);
	        AddFktButton (PartyButton);

		 }
     }
	 else
	 {
			 CheckPartyService (FALSE);
			 HMENU SubMenu = GetSubMenu (hMenu, 1);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DrawMenuBar (hMainWindow);
	 }
}

void EnablePartyService (BOOL b)
{
	if (b)
	{
        EnableMenuItem (hMenu, IDM_PARTYSERVICE, MF_ENABLED);
        EnableFullTax ();
	}
	else
	{
		IsPartyService = FALSE;
        CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_UNCHECKED);
        EnableMenuItem (hMenu, IDM_PARTYSERVICE, MF_GRAYED);
        EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
	}
}

void EnableFullTax ()
{
	if (inDaten != 1)
	{
        EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
		return;
	}
    if (!IsPartyService) 
	{
        EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
		return;
	}

    if (lsk.psteuer_kz == 2) 
	{
        EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
		return;
	}
    EnableMenuItem (hMenu, IDM_FULLTAX, MF_ENABLED);
}

void DisableFullTax ()
{
    EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
}

void SetLsToFullTax ()
{
	int ret = MessageBox (NULL, "Lieferschein auf vollen Steuersatz setzen ?", NULL,
				          MB_YESNO | MB_ICONQUESTION);
	if (ret == IDYES)
	{
        CheckMenuItem (hMenu, IDM_FULLTAX, MF_CHECKED);
		lsk.psteuer_kz = 2;
	    if (PartyKz == TRUE)
		{
			  display_form (mamain1, &partyf, 0, 0);
		}	
	}
}

void SetToFullTax ()
{
    CheckMenuItem (hMenu, IDM_FULLTAX, MF_CHECKED);
	lsk.psteuer_kz = 2;
    if (PartyKz == TRUE)
    {
          display_form (mamain1, &partyf, 0, 0);
    }

}

void SetLsToStandardTax ()
{
    if (lspListe.ContainsService ()) return;

//	int ret = MessageBox (NULL, "Lieferschein auf Standardsteuersatz setzen ?", NULL,
//				          MB_YESNO | MB_ICONQUESTION);
//	if (ret == IDYES)
//	{
        CheckMenuItem (hMenu, IDM_FULLTAX, MF_UNCHECKED);
		lsk.psteuer_kz = 1;
        if (PartyKz == TRUE)
        {
               CloseControls (&partyf);
        }

//	}
}


void SetToStandardTax ()
{

    CheckMenuItem (hMenu, IDM_FULLTAX, MF_UNCHECKED);
	lsk.psteuer_kz = 1;
    if (PartyKz == TRUE)
    {
           CloseControls (&partyf);
    }
}


void SetLsTax ()
{
	if (lsk.psteuer_kz ==1) 
	{
		SetLsToFullTax ();
	}
	else
	{
		SetLsToStandardTax ();
	}
}

BOOL isPartyDienstleistung (short a_typ, short mwst)
{
	if (mwst == 1) return TRUE;  //Voller mwst-Satz
	return FALSE;
}

void SetPlugins ()
{
	if (NewPrPlugin == NULL)
	{
		NewPrPlugin = LoadLibrary ("ChoiceLs.dll");
		if (NewPrPlugin != NULL)
		{
			ChoiceLs = (CDataCollection<CLskList> *  (*) ())
					  GetProcAddress ((HMODULE) NewPrPlugin, "LskChoice");
			ChoiceLsEx = (void  (*) (CDataCollection<CLskList> *))
						GetProcAddress ((HMODULE) NewPrPlugin, "LskChoiceEx");
			LskChoiceLs = (long  (*) (short, short, BOOL))
						GetProcAddress ((HMODULE) NewPrPlugin, "LskChoiceLs");
			LskNextRow = (long  (*) ())
						GetProcAddress ((HMODULE) NewPrPlugin, "LskNextRow");
			LskPriorRow = (long  (*) ())
						GetProcAddress ((HMODULE) NewPrPlugin, "LskPriorRow");
			LskFirstRow = (long  (*) ())
						GetProcAddress ((HMODULE) NewPrPlugin, "LskFirstRow");
			LskLastRow = (long  (*) ())
						GetProcAddress ((HMODULE) NewPrPlugin, "LskLastRow");
			LskCurrentRow = (long  (*) ())
						GetProcAddress ((HMODULE) NewPrPlugin, "LskCurrentRow");
			LskDeleteRow = (void  (*) (short, short, long))
						GetProcAddress ((HMODULE) NewPrPlugin, "LskDeleteRow");
			Destroy = (void  (*) ())
						GetProcAddress ((HMODULE) NewPrPlugin, "Destroy");
		}
	}
}

void LsNewPr ()
{
	BOOL InWork = IsInWork ();
	CNewCalculate  NewCalculate;
	if (ChoiceLs != NULL)
	{
		HWND hWnd = GetFocus ();
		CDataCollection<CLskList> *LskArray = new CDataCollection<CLskList> ();
		(*ChoiceLsEx) (LskArray);
		NewCalculate.SetLskArray (LskArray);
		NewCalculate.Calculate ();
		SetFocus (hWnd);
	    delete LskArray;
	}
	if (InWork)
	{
		beginwork ();
	}
}


void LskChoice ()
{
	BOOL InWork = IsInWork ();
	if (LskChoiceLs != NULL)
	{
		HWND hWnd = GetFocus ();
		long ls = (*LskChoiceLs) (lsk.mdn, lsk.kun_fil,mench);
		if (ls != 0)
		{
                   EnableMenuItem (hMenu, IDM_PRIOR_ROW,   MF_ENABLED);
                   EnableMenuItem (hMenu, IDM_NEXT_ROW,   MF_ENABLED);
				   sprintf (lieferschein, "%ld", ls);
				   display_form (mamain1, &aufformk, 0, 0);
				   PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
				   return;
		}
		SetFocus (hWnd);
	    SetCurrentField (currentfield);
		SetCurrentFocus (currentfield);
	}
}


void LskChoiceNextRow ()
{
	BOOL InWork = IsInWork ();
	if (LskNextRow != NULL)
	{
		HWND hWnd = GetFocus ();
		long ls = (*LskNextRow) ();
		if (ls != 0)
		{
			       int ret = 0;
			       if (inDaten)
				   {
					   ret = dokey12 (); 
				       syskey = KEY12;
					   if (ret == 0)
					   {
						   LskChoiceNr = ls;
						   return;
					   }
				   }
				   else
				   {
						sprintf (lieferschein, "%ld", ls);
						display_form (mamain1, &aufformk, 0, 0);
						PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
						return;
				   }
		}
		SetFocus (hWnd);
	}
}

void LskChoicePriorRow ()
{
	BOOL InWork = IsInWork ();
	if (LskPriorRow != NULL)
	{
		HWND hWnd = GetFocus ();
		long ls = (*LskPriorRow) ();
		if (ls != 0)
		{
			       int ret = 0;
			       if (inDaten)
				   {
					   ret = dokey12 (); 
				       syskey = KEY12;
					   if (ret == 0)
					   {
						   LskChoiceNr = ls;
						   return;
					   }
				   }
				   else
				   {
						sprintf (lieferschein, "%ld", ls);
						display_form (mamain1, &aufformk, 0, 0);
						PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
						return;
				   }
		}
		SetFocus (hWnd);
	}
}

void LskChoiceFirstRow ()
{
	BOOL InWork = IsInWork ();
	if (LskFirstRow != NULL)
	{
		HWND hWnd = GetFocus ();
		long ls = (*LskFirstRow) ();
		if (ls != 0)
		{
			       int ret = 0;
			       if (inDaten)
				   {
					   ret = dokey12 (); 
				       syskey = KEY12;
					   if (ret == 0)
					   {
						   LskChoiceNr = ls;
						   return;
					   }
				   }
				   else
				   {
						sprintf (lieferschein, "%ld", ls);
						display_form (mamain1, &aufformk, 0, 0);
						PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
						return;
				   }
		}
		SetFocus (hWnd);
	}
}

void LskChoiceLastRow ()
{
	BOOL InWork = IsInWork ();
	if (LskLastRow != NULL)
	{
		HWND hWnd = GetFocus ();
		long ls = (*LskLastRow) ();
		if (ls != 0)
		{
			       int ret = 0;
			       if (inDaten)
				   {
					   ret = dokey12 (); 
				       syskey = KEY12;
					   if (ret == 0)
					   {
						   LskChoiceNr = ls;
						   return;
					   }
				   }
				   else
				   {
						sprintf (lieferschein, "%ld", ls);
						display_form (mamain1, &aufformk, 0, 0);
						PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
						return;
				   }
		}
		SetFocus (hWnd);
	}
}

void LskChoiceCurrentRow ()
{
	BOOL InWork = IsInWork ();
	if (LskCurrentRow != NULL)
	{
		HWND hWnd = GetFocus ();
		long ls = (*LskCurrentRow) ();
		if (ls != 0)
		{
			       int ret = 0;
			       if (inDaten)
				   {
					   ret = dokey12 (); 
				       syskey = KEY12;
					   if (ret == 0)
					   {
						   LskChoiceNr = ls;
						   return;
					   }
				   }
				   else
				   {
						sprintf (lieferschein, "%ld", ls);
						display_form (mamain1, &aufformk, 0, 0);
						PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
						return;
				   }
		}
		SetFocus (hWnd);
	}
}

void LskChoiceDeleteRow (short mdn, short fil, long ls)
{
	if (LskDeleteRow != NULL)
	{
		(*LskDeleteRow) (mdn, fil, ls);
	}
}


void DestroyChoiceLsk ()
{
	if (Destroy != NULL)
	{
		(*Destroy) ();
	}
}

LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
        char cfg_v [20];
//		char where [256];

        switch(msg)
        {
              case WM_PAINT :
                      if (hWnd == mamain1)
                      {
//                              disp_forms ();
                              hdc = BeginPaint (hWnd, &aktpaint);
                              PrintLines (hdc);
                              EndPaint (hWnd, &aktpaint);
                      }
                      else 
                      {
                              lspListe.OnPaint (hWnd, msg, wParam, lParam);
                              lstxtlist.OnPaint (hWnd, msg, wParam, lParam);
                      }
                      break;
              case WM_MOVE :
                      if (hWnd == hMainWindow)
                      {
                              lspListe.MoveMamain1 ();
                              lstxtlist.MoveMamain1 ();
                      }
                      else if (hWnd == mamain1)
                      {
                              lspListe.MoveMamain1 ();
                              lstxtlist.MoveMamain1 ();
                      }
                      else
                      {
                              lspListe.OnSize (hWnd, msg, wParam, lParam);
                              lstxtlist.OnSize (hWnd, msg, wParam, lParam);
                      }
                      break;
              case WM_SIZE :
                      if (hWnd == hMainWindow)
                      {
                              MoveFkt ();
                              Mess.MoveMess ();
                              MoveMamain1 ();  
                              hdc = GetDC (hMainWindow);
                              ReleaseDC (hMainWindow, hdc);
                      }
                      else if (hWnd == mamain1)
                      {
						      InvalidateLines ();
                              lspListe.MoveMamain1 ();
                              lstxtlist.MoveMamain1 ();
                      }
                      else
                      {
                              lspListe.OnSize (hWnd, msg, wParam, lParam);
                              lstxtlist.OnSize (hWnd, msg, wParam, lParam);
                      }
                      break;

              case WM_SYSCOMMAND:
                      if (hWnd == lspListe.GetMamain1 ()
                          && wParam == SC_MINIMIZE)
                      {
                                   lspListe.SetMin ();

                      }
                      else if (hWnd == lspListe.GetMamain1 ()
                               && wParam == SC_MAXIMIZE)
                      {
                                   lspListe.SetMax ();
                                   lspListe.MaximizeMamain1 ();
                                   return 0;

                      }
                      else if (hWnd == lspListe.GetMamain1 ()
                               && wParam == SC_RESTORE)
                      {
                                   lspListe.InitMax ();
                                   lspListe.InitMin ();
                                   ShowWindow (lspListe.GetMamain1 (), 
                                               SW_SHOWNORMAL);
                                   lspListe.MoveMamain1 ();
                                   return 0;
                      }
                      else if (hWnd == lspListe.GetMamain1 ()
                               && wParam == SC_CLOSE)
                      {
                                   if (lspListe.IsListAktiv ())
                                   {
                                           syskey = KEY5;
                                           SendKey (VK_F5);
                                   }
                                   return 0;
                      }
                      else if (hWnd == hMainWindow && wParam == SC_CLOSE)
                      {
						           if (NoClose)
								   {
						                    disp_mess (
												"Abbruch durch Windowmanager ist nicht m�glich", 
												2);
								            return 0;
								   }
                                   if (abfragejn (hMainWindow, 
                                                  "Bearbeitung abbrechen ?",
                                                  "N") == 0)
                                   {
                                           return 0;
                                   }
                                   rollbackwork ();
                                   beginwork ();
								   if (lutz_mdn_par)
								   {
									   sprintf (mdn, "%hd", 0);
									   sprintf (fil, "%hd", 0);
								   }

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "ls",  auto_nr.nr_nr);
                                   commitwork ();
	                               FreeLsk ();
	                               SetForeground ();
								   SaveRect ();
                                   TestActiveDial ();
								   DestroyChoiceLsk ();
	                               closedbase ();
                                   ExitProcess (0);
                                   break;
                                   
                      }
                      break;

              case WM_HSCROLL :
                       lspListe.OnHScroll (hWnd, msg,wParam, lParam);
                       lstxtlist.OnHScroll (hWnd, msg,wParam, lParam);
                       break;
                      
              case WM_VSCROLL :
                       lspListe.OnVScroll (hWnd, msg, wParam, lParam);
                       lstxtlist.OnVScroll (hWnd, msg, wParam, lParam);
                       break;

              case WM_NOTIFY :
                    {
                      LPNMHDR pnmh = (LPNMHDR) lParam;

                      if (pnmh->code == TTN_NEEDTEXT)
                      {
                           LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
                           if (QuickCpy (lpttt->szText, lpttt->hdr.idFrom)
                               == FALSE)
                           {
                                    QuickHwndCpy (lpttt);
                           }
                      }
                      break;
                    }
	
              case WM_DESTROY :
 				      OnDestroy (hWnd); 
                      if (hWnd == hMainWindow)
                      {
                             PostQuitMessage (0);
                             return 0;
                      }
                      break;
              case WM_KEYDOWN :
                      if (lspListe.GetMamain1 ())
                      {
                               lspListe.FunkKeys (wParam, lParam);
                      }
                      break;
                  
              case WM_COMMAND :
                    if (LOWORD (wParam) == IDM_EXIT)
                    {
					        if (NoClose)
                            {
						          disp_mess (
										"Abbruch durch Windowmanager ist nicht m�glich", 
										2);
							      return 0;
							 }
                             if (abfragejn (hMainWindow, 
                                            "Bearbeitung abbrechen ?",
                                            "N") == 0)
                            {
                                  return 0;
                            }
                            rollbackwork ();
                            beginwork ();
							if (lutz_mdn_par)
							{
							      sprintf (mdn, "%hd", 0);
							      sprintf (fil, "%hd", 0);
                            }
                            dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "ls",  auto_nr.nr_nr);
                            commitwork ();
	                        FreeLsk ();
	                        SetForeground ();
	 			            SaveRect ();
                            TestActiveDial ();
							DestroyChoiceLsk ();
	                        closedbase ();
                            ExitProcess (0);
                            break;
                    }

                    if (LOWORD (wParam) == KEYESC)
                    {
                            syskey = KEYESC;
                            SendKey (VK_ESCAPE);
                            break;
                    }
                    if (LOWORD (wParam) == KEY2)
                    {
                            syskey = KEY2;
                            SendKey (VK_F2);
                            break;
                    }
                    if (LOWORD (wParam) == KEY3)
                    {
                            syskey = KEY3;
                            SendKey (VK_F3);
                            break;
                    }
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYDOWN)
                    {
                            syskey = KEYDOWN;
                            SendKey (VK_DOWN);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYUP)
                    {
                            syskey = KEYUP;
                            SendKey (VK_UP);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYLEFT)
                    {
                            syskey = KEYLEFT;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYRIGHT)
                    {
                            syskey = KEYRIGHT;
                            SendKey (VK_RIGHT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYSTAB)
                    {
                            syskey = KEYSTAB;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYTAB)
                    {
                            syskey = KEYTAB;
                            SendKey (VK_TAB);
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_PRIOR_ROW)
					{
							LskChoicePriorRow ();
					}
                    else if (LOWORD (wParam) == IDM_NEXT_ROW)
					{
							LskChoiceNextRow ();
					}
                    else if (LOWORD (wParam) == IDM_ENTER_AUF_ME)
					{
						if (lspListe.EnterAufMe)
						{
							lspListe.EnterAufMe = FALSE;
						    CheckMenuItem (hMenu, IDM_ENTER_AUF_ME,
								           MF_UNCHECKED); 
						}
						else
						{
							lspListe.EnterAufMe = TRUE;
						    CheckMenuItem (hMenu, IDM_ENTER_AUF_ME,
								           MF_CHECKED); 
						}
					}
                    else if (LOWORD (wParam) == IDM_ENTER_LIEF_ME3)
					{
						lspListe.EnterLiefMe3 (); 
					}
                    else if (LOWORD (wParam) == IDM_VINFO)
                    {
						    InfoVersion (); 
                            break;
                    }
                   else if (LOWORD (wParam) == QUERYAUF)
				   {
					        QueryAuf ();
				   }
                   else if (LOWORD (wParam) == QUERYKUN)
				   {
					        if (atoi (kunfil))
							{
								ShowFil ();
							}
							else
							{
					            QueryKun ();
							}
				   }
                   else if (LOWORD (wParam) == QUERYTOU)
				   {
					        TouQuery ();
				   }
				   else if (LOWORD (wParam) == IDM_FRAME)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_FRAME,
								           MF_CHECKED); 
							ActiveMark = IDM_FRAME;
                            syskey = KEY3;
                            SendKey (VK_F3);
                   }
				   else if (LOWORD (wParam) == IDM_REVERSE)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_REVERSE,
								           MF_CHECKED); 
							ActiveMark = IDM_REVERSE;
                            syskey = KEY4;
                            SendKey (VK_F4);
                   }
				   else if (LOWORD (wParam) == IDM_NOMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_NOMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_NOMARK;
                            syskey = KEY6;
                            SendKey (VK_F6);
                   }
				   else if (LOWORD (wParam) == IDM_EDITMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_EDITMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_EDITMARK;
                            syskey = KEY7;
                            SendKey (VK_F7);
                   }
				   else if (LOWORD (wParam) == IDM_PAGEVIEW)
                   {
                            if (lspListe.GetRecanz () < 1) break;
                            if (PageView)
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                                    ToolBar_SetState(hwndTB, IDM_PAGE, TBSTATE_ENABLED);
                                    PageView = 0;
                                    lspListe.SwitchPage0 (lspListe.GetAktRowS ());
                            }
                            else
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                                    ToolBar_SetState(hwndTB, IDM_LIST, TBSTATE_ENABLED);
                                    PageView = 1;
                                    lspListe.SwitchPage0 (lspListe.GetAktRow ());
                            }
                            SendMessage (lspListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (lspListe.Getmamain3 (), 0, TRUE);
                   }
 				   else if (LOWORD (wParam) == IDM_PAGE)
                   {
                            PageView = 1; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                            lspListe.SwitchPage0 (lspListe.GetAktRow ());
                            SendMessage (lspListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (lspListe.Getmamain3 (), 0, TRUE);
                   }
 				   else if (LOWORD (wParam) == IDM_STD)
                   {
                             lspListe.StdAuftrag ();
                             lspListe.SetListFocus ();
                   }
 				   else if (LOWORD (wParam) == IDM_LIST)
                   {
                            PageView = 0; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                            lspListe.SwitchPage0 (lspListe.GetAktRowS ());
                            SendMessage (lspListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (lspListe.Getmamain3 (), 0, TRUE);
                   }
			       else if (LOWORD (wParam) == IDM_WORK)
                   {
                             EingabeMdnFil ();
                   }
			       else if (LOWORD (wParam) == IDM_PRINT)
                   {
                   }

                   if (HIWORD (wParam) == CBN_CLOSEUP)
                   {
                                if (lParam == (LPARAM) hwndCombo1)
                                {
                                   ChoiseCombo1 ();
                                }
                                if (lParam == (LPARAM) hwndCombo2)
                                {
                                   ChoiseCombo2 ();
                                }

                                if (IsDlgCombobox ((HWND) lParam))
                                {
                                      return 0;
                                }

                                if (lspListe.Getmamain3 ())
                                {
//                                    SetFocus (lspListe.Getmamain3 ());
                                      lspListe.SetListFocus ();
                                }
                                else
                                {
                                    SetCurrentFocus (currentfield);
                                }
                    }
					else if (LOWORD (wParam) == IDM_FONT)
                    {
                                  lspListe.ChoiseFont (&lFont);
                                  lstxtlist.SetListFont (&lFont);
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_ERROR)
                    {
						          ShowErrors ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_BASIS)
                    {
                                  lspListe.ShowBasis ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_BASIS)
                    {
                                  lspListe.ShowBasis ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_POSTEXT)
                    {
                                  lspListe.Texte ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_ANZBEST)
                    {
                                  SetAnzBest ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_LGRDEF)
					{
                                  if (ProgCfg.GetGroupDefault ("lager", cfg_v) == TRUE)
								  {
                                             akt_lager = atol (cfg_v);
											 lspListe.SetLager (akt_lager);
								  }
					}
					else if (LOWORD (wParam) == IDM_LGR)
					{
					          akt_lager = LgrClass.DefaultLgr (hWnd, akt_lager); 

						        
								  lspListe.SetLager (akt_lager);
					}
					else if (LOWORD (wParam) == IDM_PARTYSERVICE)
					{
								  if (IsPartyService)
								  {
									  CheckPartyService (FALSE);
							          CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_UNCHECKED);
							          EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
									  lsk.psteuer_kz = 0;
								  }
								  else
								  {
									  CheckPartyService (TRUE);
							          CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_CHECKED);
							          EnableFullTax ();
									  if (lspListe.ContainsService ())
									  {
										lsk.psteuer_kz = lspListe.Voll;
									  }
									  else
									  {

										lsk.psteuer_kz = lspListe.Standard;
									  }
								  }
								  	
					}
					else if (LOWORD (wParam) == IDM_FULLTAX)
					{
								  SetLsTax ();
					}
					else if (LOWORD (wParam) == IDM_NEWPR)
					{
								  lspListe.NewPr ();
					}
					else if (LOWORD (wParam) == IDM_ALLNEWPR)
					{
								  LsNewPr ();
					}
					else if (LOWORD (wParam) == IDM_SORTPR)
					{
								  lspListe.SortByPrice ();
					}
					else if (LOWORD (wParam) == IDM_SORTART)
					{
								  lspListe.SortByArt ();
					}
					else if (LOWORD (wParam) == IDM_MUSTER)
					{
                                  SetMusterAuftrag ();
					}
					else if (LOWORD (wParam) == IDM_PARTYSERVICE)
					{
                                  SetPartyservice ();
					}
					else if (LOWORD (wParam) == IDM_KUNLIEF)
					{
                                  SetKunLief (); 
					}
                    else if (LOWORD (wParam) == IDM_INFO)
                    {
                                  syskey = KEY4;
                                  SendKey (VK_F4);
                    }
                    else if (LOWORD (wParam) == IDM_TEXT)
                    {
                    }
                    else if (LOWORD (wParam) == IDM_ETI)
                    {
                    }
                    else if (LOWORD (wParam) == CallT1)
                    {
                            CallTele1 ();
                            break;
                    }
                    else if (LOWORD (wParam) == CallT2)
                    {
                            CallTele2 ();
                            break;
                    }
                    else if (LOWORD (wParam) == KUNCC)
                    {
						   ShowKuncc (); 
                           break;
                    }
                    else if (LOWORD (wParam) == CallT3)
                    {
                            CallTele3 ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_LDPR)
                    {
					        lspListe.EnterLdVk ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_VKPR)
                    {
					        lspListe.EnterPrVk ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_KASSE)
                    {
					        Kasse ();
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL StaticWProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void FreeLsk (void)
/**
Beim Verlassen des Programms wird versucht alle Saetze in lsk mit
delstatus -1 auf delstatus 0 zu setzen.
Bei Saetze, die von einem anderen Benutzer bearbeitet werden bleibt der 
delstatus auf -1;
**/
{
	    extern short sql_mode; 
		short sql_s;
		int cursor;
		int upd_cursor;

		sql_s = sql_mode;
		sql_mode = 1;

		DbClass.sqlout ((short *) &lsk.mdn, 1, 0);
		DbClass.sqlout ((short *) &lsk.fil, 1, 0);
		DbClass.sqlout ((long *)  &lsk.ls, 2, 0);
		cursor = DbClass.sqlcursor ("select mdn,fil,ls from lsk "
			                        "where delstatus = -1");
		if (cursor < 0) return;

		DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		DbClass.sqlin ((short *) &lsk.fil, 1, 0);
		DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
		upd_cursor = DbClass.sqlcursor ("update lsk set delstatus = 0"
			                        "where mdn = ? "
									"and fil   = ? "
									"and ls = ?");
		if (upd_cursor < 0) return;

		beginwork ();
        while (DbClass.sqlfetch (cursor) == 0)
		{
			DbClass.sqlexecute (upd_cursor);
		}
		commitwork ();
		DbClass.sqlclose (upd_cursor);
		DbClass.sqlclose (cursor);
		sql_mode = sql_s;
}

struct TL
{
	  short mdn;
	  short fil;
	  char kun [9];
	  char name [17];
	  char tele [21];
	  char uhrzeit [6];
	  char partner [37];
	  char status [2];
      char text [37];
      long adr;
};

static struct TL Teles, *Teletab;
static  CH *Choise1;
static  short wo_tag;
static	char datum [12];


int TeleOK (int pos)
/**
Kunde auf erledigt setzen.
**/
{
 	   char buffer [256];
	   long kun;

	   strcpy (Teletab[pos].status, "X");
	   memcpy (&Teles, &Teletab[pos], sizeof (TL));
 	   sprintf (buffer, " %-8s %-18s %-20s %-5s %s", Teles.kun, Teles.name, 
		                                             Teles.tele, Teles.uhrzeit,
													 Teles.partner);
	   Choise1->UpdateRecord (buffer, pos);
	   kun = atol (Teles.kun);
	   DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
	   DbClass.sqlin ((short *) &lsk.fil, 1, 0);
	   DbClass.sqlin ((long *)  &kun,      2, 0);
	   DbClass.sqlin ((short *) &wo_tag,   1, 0);
	   DbClass.sqlin ((char *)  Teles.uhrzeit, 0, 6);
	   DbClass.sqlcomm ("update kun_anr set tag = today "
		                "where mdn = ? "
						"and fil = ? "
						"and kun = ? "
						"and wo_tag = ? "
						"and uhrzeit = ?");
	   commitwork ();
	   beginwork ();
	   PostQuitMessage (0);
	   telekun = TRUE;
	   sprintf (kuns, "%ld", kun); 
	   return 0;
}

int TeleDial (int pos)
/**
Telefon-Nummer w�hlen.
**/
{
        char buffer [512]; 
        if (Choise1 == NULL)
        {
            return 0;
        }

        Choise1->GetText (buffer);
        int anz = wsplit (buffer, " ");
        if (anz < 3)
        {
  	        disp_mess ("Keine Telefonnummer gefunden", 2);
            return 0;
        }
        strcpy (buffer, wort[2]);
        if (ClipDial != NULL)
        {
            RunClipDial (buffer);         
        }
        else if (DdeDialProg != NULL)
        {
            RunDdeDial (buffer);
        }
		return 0;
}

int TeleInfo (int pos)
/**
Telefon-Nummer w�hlen.
**/
{
        char buffer [512];
        
        sprintf (buffer, "maske partner.scr");
        ProcExec (buffer, SW_SHOWNORMAL, -1, 0, -1, 0);
		return 0;
}

void TelefonList (void)
/**
Auswahl ueber Kunden, die an diesem Tag angerufen werden sollen.
**/
{
	  int cx, cy;
	  char buffer [256];
	  int cursor, cursor_kun, cursor_adr;
      int cursor_txt, cursor_adr2;
	  short teanz;
	  long adr2;
      long txt_nr;
      short zei;
	  int i;
	  int dsqlstatus;

	  sysdate (datum);
      wo_tag = get_wochentag (datum);

	  lsk.mdn = atoi (mdn);
	  lsk.fil = atoi (fil);

	  if (lutz_mdn_par == 0)
	  {
	           DbClass.sqlout ((short *) &teanz,    1, 0);
	           DbClass.sqlin ((short *)  &lsk.mdn, 1, 0);
	           DbClass.sqlin ((short *)  &lsk.fil, 1, 0);
	           DbClass.sqlin ((short *)  &wo_tag,   1, 0);
	           DbClass.sqlin ((char *)   sys_ben.pers_nam, 0, 9);
	           DbClass.sqlcomm ("select count (*) from kun_anr where "
		                          "mdn = ? "
								  "and fil = ? "
								  "and wo_tag = ? "
								  "and (pers_nam = ? or pers_nam = \" \" or pers_nam is null) "
								  "and tag <> today");
	  }
	  else
	  {
	           DbClass.sqlout ((short *) &teanz,    1, 0);
	           DbClass.sqlin ((short *)  &wo_tag,   1, 0);
	           DbClass.sqlin ((char *)   sys_ben.pers_nam, 0, 9);
	           DbClass.sqlcomm ("select count (*) from kun_anr where "
								  "wo_tag = ? "
								  "and (pers_nam = ? or pers_nam = \" \" or pers_nam is null) "
								  "and tag <> today");
	  }


	  if (teanz == 0)
	  {
		  disp_mess ("Keine Liste vorhanden",2);
		  return;
	  }

	  Teletab = new struct TL [teanz + 2];
	  if (Teletab == NULL)
	  {
		  disp_mess ("Fehler bei der Speicherzuornung", 2);
		  return;
	  }

	  if (lutz_mdn_par == 0)
	  {
	      DbClass.sqlout ((char *) Teles.kun,  0, 9); 
	      DbClass.sqlout ((char *) Teles.tele, 0, 21); 
	      DbClass.sqlout ((char *) Teles.uhrzeit, 0, 6); 
	      DbClass.sqlout ((char *) Teles.partner, 0, 37); 
	      DbClass.sqlout ((char *) &Teles.adr, 2, 0); 

	      DbClass.sqlin ((short *)  &lsk.mdn, 1, 0);
	      DbClass.sqlin ((short *)  &lsk.fil, 1, 0);
	      DbClass.sqlin ((short *)  &wo_tag,   1, 0);
	      DbClass.sqlin ((char *)   sys_ben.pers_nam, 0, 9);
	      cursor = DbClass.sqlcursor ("select kun, tel, uhrzeit, partner, adr "
		                          "from kun_anr "
								  "where mdn = ? "
								  "and fil = ? "
								  "and wo_tag = ? "
								  "and (pers_nam = ? or pers_nam = \" \" or pers_nam is null) "
								  "and kun_anr.tag <> today order by uhrzeit");

	  }
	  else
	  {
	      DbClass.sqlout ((short *)  &lsk.mdn, 1, 0);
	      DbClass.sqlout ((short *)  &lsk.fil, 1, 0);
	      DbClass.sqlout ((char *) Teles.kun,  0, 9); 
	      DbClass.sqlout ((char *) Teles.tele, 0, 21); 
	      DbClass.sqlout ((char *) Teles.uhrzeit, 0, 6); 
	      DbClass.sqlout ((char *) Teles.partner, 0, 37); 
	      DbClass.sqlout ((char *) &Teles.adr, 2, 0); 

	      DbClass.sqlin ((short *)  &wo_tag,   1, 0);
	      DbClass.sqlin ((char *)   sys_ben.pers_nam, 0, 9);
	      cursor = DbClass.sqlcursor ("select mdn, fil, kun, tel, uhrzeit, partner, adr "
		                          "from kun_anr "
								  "where wo_tag = ? "
								  "and (pers_nam = ? or pers_nam = \" \" or pers_nam is null) "
								  "and kun_anr.tag <> today");
	  }

	  DbClass.sqlin ((short *)  &lsk.mdn, 1, 0);
	  DbClass.sqlin ((short *)  &lsk.fil, 1, 0);
	  DbClass.sqlin ((char *)   Teles.kun, 0, 9);

	  DbClass.sqlout ((char *) Teles.name, 0, 17); 
	  DbClass.sqlout ((long *) &adr2, 2, 0); 

	  cursor_kun = DbClass.sqlcursor ("select kun_krz2, adr2 from kun "
		                              "where mdn = ? "
									  "and   fil = ? "
									  "and kun = ?");

	  DbClass.sqlin ((long *) &adr2, 2, 0);
	  DbClass.sqlout ((char *) Teles.partner, 0, 37);
	  cursor_adr = DbClass.sqlcursor ("select partner from adr where adr = ?");

	  DbClass.sqlin ((long *) &Teles.adr, 2, 0);
	  DbClass.sqlout ((long *) &txt_nr, 2, 0);
	  cursor_adr2 = DbClass.sqlcursor ("select txt_nr from adr where adr = ?");

	  DbClass.sqlin ((long *) &txt_nr, 2, 0);
      DbClass.sqlout ((char *) Teles.text, 0, 37);
      DbClass.sqlout ((short *) &zei, 1, 0);
      cursor_txt = prepare_sql ("select txt, zei from we_txt "
                                "where nr = ? "
                                "order by zei");

      Settchar ('\"');
	  cx = 80;
	  cy = 20;
	  Choise1 = new CH (cx, cy);
      Choise1->OpenWindow (hMainInst, mamain1);

//	  Choise1->VLines (" ", 0);
	  sprintf (buffer, " %9s %18s %20s %5s %20s %20s", "1", "1", "1", "1", "1", "1"); 
	  Choise1->VLines (buffer, 0);
	  EnableWindow (hMainWindow, FALSE);
	  sprintf (buffer, " %-8s %-18s %-20s %-5s %-20s %-20s", "K-Nr", "Name", "Telefon-Nr", 
		                                            "Zeit", "Partner", 
                                                    "Zust�ndigkeit");
	  Choise1->InsertCaption (buffer);

	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
	  {
		  Teles.mdn = lsk.mdn;
		  Teles.fil = lsk.fil;
          strcpy (Teles.name, " "); 
          strcpy (Teles.status, " "); 
		  DbClass.sqlopen (cursor_kun);
		  dsqlstatus = DbClass.sqlfetch (cursor_kun);

          clipped (Teles.partner);
          if (strcmp (Teles.partner, " ") <= 0)
          {
                  if (Teles.adr > 0)
                  {
                          adr2 = Teles.adr;
                  }
  		          DbClass.sqlopen (cursor_adr);
		          dsqlstatus = DbClass.sqlfetch (cursor_adr);
          }

          Teles.text[0] = 0;
          DbClass.sqlopen (cursor_adr2);
          dsqlstatus = DbClass.sqlfetch (cursor_adr2);
          if (txt_nr > 0)
          {
              DbClass.sqlopen (cursor_txt);
              DbClass.sqlfetch (cursor_txt);
          }

		  memcpy (&Teletab[i], &Teles, sizeof (TL));
  		  sprintf (buffer, " %-8s \"%-18s\" \"%-20s\" %-5s \"%-20s\" \"%-20s\"", Teles.kun, Teles.name, Teles.tele, 
			                                                Teles.uhrzeit,Teles.partner, Teles.text);
	      Choise1->InsertRecord (buffer);
		  i ++;
	  }

	  DbClass.sqlclose (cursor);
	  DbClass.sqlclose (cursor_kun);
	  DbClass.sqlclose (cursor_adr);
	  DbClass.sqlclose (cursor_adr2);
	  DbClass.sqlclose (cursor_txt);

	  Choise1->SetOkFunc (TeleOK);
	  Choise1->SetDialFunc (TeleDial);
	  Choise1->SetInfoFunc (TeleInfo);
	  Choise1->ProcessMessages ();
	  if (syskey != KEY5)
	  {
	            Choise1->GetText (buffer);
	  }
	  EnableWindow (hMainWindow, TRUE);
      Choise1->DestroyWindow ();
	  delete Choise1;
	  Choise1 = NULL;
	  delete Teletab;
	  currentfield = 0;
	  SetFocus (aufformk.mask[0].feldid);
	  if (telekun)
	  {
	   leseauf ();
	   PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
	  }
      return;
}

BOOL ShowKuncc ()
{
        struct SKUNCC *skun;
		SEARCHKUNR SearchKun; 

       SearchKun.SetParams (hMainInst, mamain1);
       SearchKun.Setawin (hMainWindow);

  	    lsk.mdn = atoi (mdn);
        SearchKun.SearchKun (lsk.mdn, IsC_C() );
      if (syskey == KEY5 || skun->kun == NULL)
        {
               SetCurrentFocus (currentfield);
/*
 			   if (KunDirect)
			   {
                      PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
			   }
*/
               return 0;
         }

        skun = SearchKun.GetSkun ();
        if (skun == NULL)
        {
            SetCurrentFocus (currentfield); 
/*
 		    if (KunDirect)
			{
                      PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
			}
*/
            return 0;
        }
        sprintf (kuns, "%ld", atol (skun->kun));

  	    EnableWindow (hMainWindow, TRUE);
	    currentfield = 0;
	    SetFocus (aufformk.mask[0].feldid);


        strcpy (kuns, skun->ls);
        strcpy (lieferschein, skun->ls);
	    if (atoi(kuns) > 0)
		{
         current_form = &aufform;
	     leseaufcc (atoi(kuns));

	     PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
		}
        return 0;
}


void RestartProcess ()
{
	   char command [512]; 
	   if (TransactMessage)
	   {
	        disp_mess ("Die Anzahl der Transaktionen pro Process ist �berschritten.\n" 
		               "Ein Restart des Programms wird durchgef�hrt", 2); 
	   }
	   sprintf (command, "%s nomenu=true mdn=%hd", GetCommandLine (), atoi (mdn));
	   ProcExec (command, SW_SHOW, -1, 0, -1, 0);
	   Sleep (1000);
	   ExitProcess (0);
}
	   