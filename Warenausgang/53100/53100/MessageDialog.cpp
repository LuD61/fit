// MessageDialog.cpp: Implementierung der Klasse MessageDialog.
//
//////////////////////////////////////////////////////////////////////

#include "MessageDialog.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CMessageDialog::CMessageDialog(HWND Parent) :
               CDialog (IDD, Parent) 
{
    m_Text = NULL;
    Text   = NULL;
	TextBrush = NULL;
}

CMessageDialog::~CMessageDialog()
{

}

void CMessageDialog::AttachControls ()
{
	m_Text = GetDlgItem (m_hWnd, IDC_MESSAGE);
	m_OK   = GetDlgItem (m_hWnd, IDOK);
	m_Cancel   = GetDlgItem (m_hWnd, IDCANCEL);
}

BOOL CMessageDialog::OnInitDialog ()
{
	CDialog::OnInitDialog ();
	if (Text != NULL)
	{
		SetWindowText (m_Text, Text);
	}
	ShowWindow (m_Cancel, SW_HIDE);
	return TRUE;
}

HBRUSH CMessageDialog::OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor)
{
	if (TextBrush == NULL)
	{
		TextBrush = CreateSolidBrush (GetSysColor (COLOR_3DFACE));
	}
	SetTextColor (hDC, RGB (0, 0, 255));
	SetBkColor (hDC, GetSysColor (COLOR_3DFACE));
	SetBkMode (hDC, TRANSPARENT);
	return TextBrush;
}
