#ifndef _MO_AUFL_DEF
#define _MO_AUFL_DEF
#include "wmaskc.h"
#include "listcl.h"
#include "mo_intp.h"
#include "DllPreise.h"
#include "LeerGut.h"
#include "ControlPlugins.h"
#include "WaWiePro.h"
#include "Text.h"
#include "scanean.h"
#include "smthandler.h"


#define MAXLEN 40

extern BOOL IsPartyService;

class AUFPLIST : public CRowEvent, CEditRenderer
{
            private :
                int mamainmax;
                int mamainmin;
                HWND    hwndTB;
                char *eingabesatz;
                unsigned char *ausgabesatz;
                int zlen;
                int feld_anz;
                int banz;
                PAINTSTRUCT aktpaint;
                char *SwSaetze[30000];
                int PageView;
                unsigned char DlgSatz [20 + MAXLEN + 2];
                FELDER *LstZiel;
                unsigned char *LstSatz;
                int Lstzlen;
                int Lstbanz;
                int SwRecs;
                int AktRow;
                int AktColumn;
                int scrollpos;
                TEXTMETRIC tm;
                char InfoCaption [80];
                int WithMenue;
                int WithToolBar;
                BOOL ListAktiv;
				HWND hMainWindow;
				int Posanz;
				int liney;
				BOOL ScanMode;
				BOOL ScanMulti;
				int ScannMeZwang;
				int Ean128Eanlen;
				Text ScannedCharge;
				static BOOL ChargeZwang;
				BOOL EditAufMeEnabled;
				int ChargeFromEan128;
				int Ean128StartLen;
				BOOL WithZerlData;
				int ScanColumns[5];
				CEan128Date Ean128Date;
				static short auf_art;
                HMENU hMenu;
				static CWaWiePro WaWiePro;
                static BOOL Muster;
				static CLeerGut LeerGut;
				static AUFPLIST *Instance;
				static int SortFlag;


             public :
				enum 
				{
					Standard = 1,
                    Voll = 2,
				};
                  
				enum 
				{
					FormTyp2 = 2,
                    FormTyp5 = 5,
				};

				enum
				{
					ChargeFromAi01 = 1,
                    ChargeFromLength = 2,  
				};

				enum
				{
					NoSort = 0,
					PriceSort = 1,
					ArticleSort = 2,
				};


				static CDllPreise DllPreise;
                static void SetMax0 (int);
                static void SetMin0 (int);
                static BOOL AErsInLs;
				static CControlPlugins pControls;
				static int PosTxtKz;
				static BOOL TxtByDefault;
				static BOOL PosDeleteWarning;
				static CSmtHandler *smtHandler;
				BOOL EnterAufMe;
				BMAP BitMap;
				HBITMAP NoteIcon;


                AUFPLIST ();

				BOOL GetScanMode ()
				{
					return ScanMode;
				}

                BOOL GetMuster (void)
                {
                    return Muster;
                }

                void SetMuster (BOOL b)
                {
                    Muster = b;
                }

				void SetAufArt (short auf_art)
				{
					this->auf_art = auf_art;
				}

				void SetLiney (int ly)
				{
					liney = ly;
				}

                void SetMenue (int with)
                {
                    WithMenue = with;
                }

                void SetToolMenue (int with)
                {
                    WithToolBar = with;
                }

                void SetMax (void)
                {
                    mamainmax = 1;
                    SetMax0 (mamainmax);
                }

                void SetMin (void)
                {
                    mamainmin = 1;
                    SetMin0 (mamainmin);
                }

                void InitMax (void)
                {
                    mamainmax = 0;
                    SetMax0 (mamainmax);
                }

                void InitMin (void)
                {
                    mamainmin = 0;
                    SetMin0 (mamainmin);
                }

				void SetLager (long);

                BOOL IsListAktiv (void)
                {
                    return ListAktiv;
                }

                void SetCfgProgName (LPSTR);

				void SetMdnProd (int);

				int GetPosanz (void)
				{
					return Posanz;
				}

                void SetMenu (HMENU hMenu)
                {
                    this->hMenu = hMenu;
                }

                void SetPrPicture (void);
                void SetNewPicture (char *, char *);
                void SetNewLen (char *, int);
                void SetNewRow (char *, int);
                void SethMainWindow (HWND);
                HWND GetMamain1 (void);
				static BOOL GetInstanceScanMode ();
                static int KunInfo (void);
                static int ShowBasis (void);
                static int TestAppend (void);
                static int DeleteLine (void);
                static void DoBreak (void);
                static BOOL SavePosis (void);
                static BOOL TestInsCount (void);
                static int InsertLine (void);
                static int AppendLine (void);
                static int PosRab (void);
                static int SearchA (void);
                static void PaintSa (HDC hdc);
                static HWND CreateSaW (void);
                static void CreateSa (void);
				static void DestroySa (void);
                static HWND CreatePlus (void);
				static void DestroyPlus (void);
                void   PaintPlus (HDC);
                static void TestSaPr (void);
                static void FillDM (double, double);
                static void FillEURO (double, double);
                static void FillFremd (double, double);
				static void InitWaehrung (void);
                static void FillAktWaehrung (void);
                static void FillWaehrung (double, double);
				static void FillKondArt (char *);
				static double GetBasa_grund (double);
                static void ReadPr (void);
                static void ReadExtraPr (void);
                static void ReadMeEinh (void);
                static void GetMeEinhIst (void);

                static void rechne_aufme0 (void);
                static void rechne_aufme1 (void);
                static void rechne_aufme2 (void);
                static void rechne_aufme3 (void);
                static void rechne_liefme (void);

                static int testme (void);
                static int testme0 (void);
                static int TestPrproz_diff (void);
                static int LadTestPrproz_diff (void);

                static int testprx (void);
                static int testpr (void);
                static int testmhd (void);
                static int TestLsCharge (void);
                static int TestEnableCharge (void);
                static void EanGew (char *, BOOL);
                static int ScanKunEan (double);
                static void InsertBonArtikel (long);
                static int ScanArtikel (double);
                static int ReadEan (double);
                static int ReadEmb (double);
                static int Testa_kun (void);
				static BOOL Testa_kun (double art);
                static void GetLastMeAuf (double);
                static void GetLastMe (double);
                static int TestNewArt (double);
                static int fetchMatchCode (void);
                static void ReadChargeGew (void);
                static BOOL DispCharge (void);
                static BOOL DispErsatz (void);
                static BOOL AddPosTxt (char *);
                static BOOL BestandOk (void);
                static int fetcha (void);
                static int fetchaDirect (int);
                static int fetcha_kun (void);
                static int fetchkun_bran2 (void);
                static int ChMeEinh (void);
                static long GenLspTxt0 (void);
			    static BOOL TxtNrExist (long);
                static long GenLspTxt (void);
                static void WriteTextPos (char *);
                static int Texte (void);
                static int Querya (void);
                static BOOL ShowA ();
                static int doStd (void);
                static void TestMessage (void);
                static int setkey9me (void);
                static int setkey9basisl (void);
                static int setkey9basis (void);
                static int Savea (void);
                static double GetAufMeVgl (void);
                static double GetAufMeVgl (double, double, short);
                static int GetAufMeVglEx (void);
                static BOOL BsdArtikel (double);
                static void BucheBsd (double, double, short, double);
                static void UpdateBsd (void);
                static void DeleteBsd (void);
                static void WriteAufkun (void);
                static BOOL testdecval (void);
                static void WritePos (int);
                static int WriteAllPos (void);
                static int ErfasseLeergut (void);
                static int dokey5 (void);
                static int WriteRow (void);
                static int WriteAktPos ();
                static int TestRow (void);
                static void GenNewPosi (void);
                static void GenNewPosi0 (void);
                static int PosiEnd (long);
                static void SaveAuf (void);
                static void SetAuf (void);
                static void RestoreAuf (void);
                static int Schirm (void);
                static int SetRowItem (void);
                static int  Getib (void);
                static void ChoiseLines (HWND, HDC);
                static HWND CreateEnter (void);
                static long EnterPosRab (void);
                static HWND CreateAufw (void);
                static void AnzAufWert (void);
                static HWND CreateAufGew (void);
                static double GetArtGew (int);
                static double GetArtMwst (int);
                static void AnzAufGew (void);
                static long ChoiseProdLgr (long);
                static long TestProdLgr (long);
                static int ProdMdn (void);
                static void TestPfand (double);
                static void TestLeih (double);
				static void datplushbk (char *datum);

                void EnterLdVk (void);
                void EnterPrVk (void);
                void SetAufVkPr (double, double);
				void SetPreise (void);
                void MoveSaW (void);
                void MovePlus (void);
                void MoveAufw (void);
                void MoveAufGew (void);
                void CloseAufw (void);
                void CloseAufGew (void);
                void InitSwSaetze (void);
                int ToMemory (int pos);
                void SetStringEnd (char *, int);
                void uebertragen (void);
                void ShowDB (short, short, long);
                void ReadDB (short, short, long);
                void SetSchirm (void);
                void GetListColor (COLORREF *, char *);
                void SetPreisTest (int);
                void SetLadPreisTest (int);

                void GetCfgValues (void);
                void SetNoRecNr (void);
                void FillWaehrungBz (void);
                void EnterLsp (short, short, long,
                                short, long,  char *);
                void ShowLsp (short, short, long,
                                short, long,  char *);
                void DestroyWindows (void);
                void WorkLsp (void);
                void DestroyMainWindow (void);
                HWND CreateMainWindow (void);
                void MoveMamain1 ();
                void MaximizeMamain1 ();
                void SetMessColors (COLORREF, COLORREF);

                void SetChAttr (int);
                static void SetFieldAttr (char *, int);
                static int  GetFieldAttr (char *);
                void Geta_bz2_par (void);
                void Geta_kum_par (void);
                void Getlief_me_pr0 (void);
                void Getwa_pos_txt (void);
                void SetRecHeight (void);

                void SethwndTB (HWND);
                void SetTextMetric (TEXTMETRIC *);
                void SetLineRow (int);
                void SetListLines (int);
                void OnPaint (HWND, UINT, WPARAM, LPARAM);
                void MoveListWindow (void);
                void BreakList (void);
                void OnHScroll (HWND, UINT, WPARAM, LPARAM);
                void OnVScroll (HWND, UINT, WPARAM, LPARAM);
                void OnSize (HWND, UINT, WPARAM, LPARAM);
                void StdAuftrag (void);
                void FunkKeys (WPARAM, LPARAM);
                int  GetRecanz (void);
                void SwitchPage0 (int);
                HWND Getmamain2 (void);
                HWND Getmamain3 (void);
                void SetFont (mfont *);
                void SetListFont (mfont *);
                void ChoiseFont (mfont *);
                void FindString (void);
                void SetLines (int);
                int GetAktRow (void);
                int GetAktRowS (void);
                void SetColors (COLORREF, COLORREF);
                void SetListFocus (void);
 			    void PaintUb (void);
				static BOOL ContainsService ();
				static void ReadPtabAufschlag ();
 			    virtual void AfterPaint (HDC hdc, RECT *rect, int pos, int SubRow);
				void NewPr ();
				void ReadPrEx ();
				void SetChargeAttr ();
				static int SortByPrice0 (const void *elem1, const void *elem2);
				static int SortByArt0 (const void *elem1, const void *elem2);
				static void SortByPrice ();
				static void SortByArt ();
				static int ChangeChar (int key);
				static BOOL  TestSmt ();

				BOOL artikelOK (double a);
				int ScanEan128Ai (Text& Ean128);
				int ScanEan128Charge (Text& Ean128);
				int ScanEan128 (Text& Ean128);
				int TestEanCharge (char *buffer);
				BOOL TestLiefMe0 ();
				static void EnterLiefMe3 ();

				virtual HWND SetFeldEdit (int row, int column, int x, int y, int cx, int cy, HWND hParent, HINSTANCE hInstance);
				virtual char *SetDefault (int row, int column, char *buffer, int length);
				virtual char *GetWindowText (HWND FocusWindow, int row, int column, char *buffer, int length);
};
#endif
