#ifndef _STDFILAUFK_DEF
#define _STDFILAUFK_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct STDFILAUFK {
   short     mdn;
   short     fil;
   char      wo_tag[3];
   long      lfd;
   char      bez[17];
   short     delstatus;
};
extern struct STDFILAUFK stdfilaufk, stdfilaufk_null;

#line 10 "stdfilaufk.rh"

class STDFILAUFK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               STDFILAUFK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
