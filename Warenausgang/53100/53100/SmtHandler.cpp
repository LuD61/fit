// SmtHandler.cpp: Implementierung der Klasse CSmtHandler.
//
//////////////////////////////////////////////////////////////////////

#include "SmtHandler.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CSmtHandler::CSmtHandler()
{
	m_SmtTest  = FALSE;
	m_Smt      = 0;
	m_Ls       = 0l;
}

CSmtHandler::~CSmtHandler()
{

}


void CSmtHandler::GetSmt (short teil_smt)
{
	if (m_SmtTest)
	{
		m_Smt = 0;
		if ( m_Kun != NULL )	// was passiert bei Wechsel zwischen Kun und fil ? 
		{
			if (m_Kun->einz_ausw > 3)
			{
				m_Smt = (short) Smtg.Get (m_Kun->mdn, m_Kun->kun, (long) teil_smt);
			}
		}
	}
}

BOOL CSmtHandler::TestSmt (A_BAS *a_bas)
{
	BOOL ret = TRUE;

	if (m_SmtTest)
	{
		if (m_Smt != 0)
		{
			if (a_bas->a_typ != 11)
			{
				short smt = (short) Smtg.Get (m_Kun->mdn, m_Kun->kun, (long) a_bas->teil_smt);
				if (smt != m_Smt)
				{
					ret = FALSE;
				}
			}
		}
		else
		{
			GetSmt (a_bas->teil_smt);
		}
	}
	return ret;
}

