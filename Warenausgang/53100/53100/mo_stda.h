#ifndef _MO_STDA_DEF
#define _MO_STDA_DEF
#include "stdfilaufk.h"
#include "stdfilaufp.h"
#include "searchstdfilk.h"

class StndAuf {
         private :
             HANDLE hMainInst;
			 int SortMode;
			 BOOL UpdError;
			 STDFILAUFP_CLASS Stdfilaufp;
         public :
			 StndAuf () : SortMode (0), UpdError (FALSE)
            {
            }
 
			void CleanUpdError (void)
			{
				UpdError = FALSE;
			}

            void SetSortMode (int mode)
			{
				SortMode = max (0, mode);
			}
            int StdAuftrag (HWND, short, short, long, short);
            void UpdStdAuftrag (short, short, long, short, double, double);
            double GetStda (void);
            double GetStdme (void);
            double GetStdpr_vk (void);
            BOOL GetStdRow (int, double *, double *, double *);
            int StdFilAuftrag (HWND, HWND, short, short, char *);
            int ReadStdFilAuftrag (HWND, short, short, SSTDAUFK *);
};
#endif