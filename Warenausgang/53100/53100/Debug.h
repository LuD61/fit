// Debug.h: Schnittstelle f�r die Klasse CDebug.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEBUG_H__115759A4_F817_4EC7_871D_AD993F579232__INCLUDED_)
#define AFX_DEBUG_H__115759A4_F817_4EC7_871D_AD993F579232__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <windows.h>

class CDebug  
{

protected:
	CDebug();
	virtual ~CDebug();
private :
	char Name[512];
	char Line [512];
	FILE *fp;
	static CDebug *Instance;
public :
	void SetName (char *Name)
	{
		memset (this->Name, 0, sizeof (this->Name));
        strncpy (this->Name, Name, sizeof (this->Name) - 1);
	}

	LPSTR GetLine ()
	{
		return Line;
	}

	void SetLine (char *Line)
	{
		memset (this->Line, 0, sizeof (this->Line));
        strncpy (this->Line, Line, sizeof (this->Line) - 1);
	}

	static CDebug *GetInstance ();
	void Open ();
	void Write ();
	void Close ();
};

#endif // !defined(AFX_DEBUG_H__115759A4_F817_4EC7_871D_AD993F579232__INCLUDED_)
