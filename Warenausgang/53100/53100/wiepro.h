#ifndef _WIEPRO_DEF
#define _WIEPRO_DEF

#include "dbclass.h"

struct WIEPRO {
   char      zeit[7];
   long      dat;
   long      sys;
   char      peri_nam[21];
   char      blg_typ[2];
   long      nr;
   short     kun_fil;
   short     fil;
   short     mdn;
   long      kun;
   double    a;
   double    gew_bto;
   double    gew_nto;
   double    tara;
   char      erf_kz[2];
   char      pers[13];
   double    pr;
   short     delstatus;
   char      ls_charge[21];
   long      anz;
   char      ls_ident[21];
   char      ls_char30[31];
   double    nett_alt;
};
extern struct WIEPRO wiepro, wiepro_null;

#line 7 "wiepro.rh"

class WIEPRO_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               WIEPRO_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
               int dbinsert (void);
               int dbupdate (void);
               int dbdelete (void);
               int dbclose (void);
};
#endif
