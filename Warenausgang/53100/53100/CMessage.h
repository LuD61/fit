#ifndef __CMESSAGE_DEF
#define __CMESSAGE_DEF
#define BuOk 1001
#include "dlg.h"

#define RETURN 0
#define F5 1

class CMessage : public DLG 
{
      private :
		   CFIELD **controls;
		   CFORM *form;
		   BOOL signal;
           Text ButtonText;
		   int BreakKey;
		   BOOL BreakF5;

      public :
		  CMessage ();
		  ~CMessage ();
		  void SetButtonText (Text&); 
		  void SetBreakKey (int); 
		  void SetBreakF5 (BOOL); 
		  void Start (LPSTR, BOOL);
          virtual BOOL OnCommand (HWND,UINT,WPARAM,LPARAM);
          virtual BOOL OnKeyReturn (void);
          virtual BOOL OnKey5 (void);
		  static void Show (LPSTR);
          static void Show (LPSTR, Text&, int, BOOL);
};
#endif
	
