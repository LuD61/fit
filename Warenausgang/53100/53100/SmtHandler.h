// SmtHandler.h: Schnittstelle f�r die Klasse CSmtHandler.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SMTHANDLER_H__19A5E75E_E66B_43AF_BFB0_78811539A017__INCLUDED_)
#define AFX_SMTHANDLER_H__19A5E75E_E66B_43AF_BFB0_78811539A017__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>
#include "kun.h"
#include "ls.h"
#include "a_bas.h"
#include "mo_smtg.h"

class CSmtHandler  
{
public:
	CSmtHandler();
	virtual ~CSmtHandler();

private:
	BOOL m_SmtTest;
	KUN *m_Kun;
	short m_Smt;
	long m_Ls;
	SMTG_CLASS Smtg;
public:

	void set_Kun (KUN *kun)
	{
		m_Kun = kun;
		m_Smt = 0;
	}

	short Smt ()
	{
		return m_Smt;
	}

	void set_Smt (short smt)
	{
		m_Smt = smt;
	}

	void set_SmtTest (BOOL smtTest)
	{
		m_SmtTest = smtTest;
	}

	void set_Ls (long ls)
	{
		m_Ls = ls;
	}

	void GetSmt (short teil_smt);
	BOOL TestSmt (A_BAS *a_bas);
};

#endif // !defined(AFX_SMTHANDLER_H__19A5E75E_E66B_43AF_BFB0_78811539A017__INCLUDED_)
