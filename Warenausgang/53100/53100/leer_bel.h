#ifndef _LEER_BEL_DEF
#define _LEER_BEL_DEF
#include "dbclass.h"

struct LEER_BEL {
   short     mdn;
   short     kun_fil;
   long      nr;
   char      blg_typ[2];
   double    a;
   long      anzahl;
   long      bestand;
   long      datum;
   char      zeit[9];
   char      pers_nam[17];
   long      kun;
   long      hkun;
   short     delstatus;
};
extern struct LEER_BEL leer_bel, leer_bel_null;

#line 6 "leer_bel.rh"

class LEER_BEL_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LEER_BEL leer_bel;
               LEER_BEL_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (LEER_BEL&);  
               int dbinsert ();  
};
#endif
