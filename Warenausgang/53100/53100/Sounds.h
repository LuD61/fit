#include "Text.h"

#ifndef __SOUND_DEF
#define __SOUND_DEF
class CSounds
{
   private :
      	int len;
        int wait;
        UINT type;
		Text SoundName;

   public :
	   CSounds ();
	   CSounds (LPSTR);
	   CSounds (int, int, UINT);
	   void Run ();
	   void Play ();
};
#endif

