// MessageDialog.h: Schnittstelle f�r die Klasse MessageDialog.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSAGEDIALOG_H__E8E2ED1D_C8D3_4019_8BA3_67A7A890295E__INCLUDED_)
#define AFX_MESSAGEDIALOG_H__E8E2ED1D_C8D3_4019_8BA3_67A7A890295E__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "Dialog.h"
#include "resrc1.h"

class CMessageDialog : public CDialog  
{
private:
	LPSTR Text;
	HWND m_Text;
	HWND m_OK;
	HWND m_Cancel;
	HBRUSH TextBrush;

protected:
	virtual void AttachControls ();
	virtual BOOL OnInitDialog ();
	virtual HBRUSH OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor);

public:
	enum {IDD = IDD_MSG_DIALOG};

	void SetText (LPSTR Text)
	{
		this->Text = Text;
	}

	CMessageDialog(HWND Parent=NULL);
	virtual ~CMessageDialog();
};

#endif // !defined(AFX_MESSAGEDIALOG_H__E8E2ED1D_C8D3_4019_8BA3_67A7A890295E__INCLUDED_)
