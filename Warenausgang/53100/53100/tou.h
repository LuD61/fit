#ifndef _TOUDEF
#define _TOUDEF
#include "dbclass.h"

struct TOU {
   long      tou;
   char      tou_bz[49];
   char      fz_kla[3];
   char      fz[13];
   char      srt_zeit[6];
   char      dau[6];
   long      lng;
   char      fah_1[13];
   char      fah_2[13];
   short     delstatus;
   long      lgr; 
};
extern struct TOU tou, tou_null;

#line 6 "tou.rh"

class TOU_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
               int prep_awcursor (void);
               int prep_awcursor (char *);
               void fill_aw (int);

       public :
               TOU_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
               int ShowAllBu (HWND hWnd, int ws_flag);
               int ShowBuQuery (HWND hWnd, int ws_flag, char *);
               int QueryBu (HWND hWnd, int ws_flag);
               int ShowBuQuery (HWND, int); 
               int PrepareQuery (form *, char *[]);
};
#endif
