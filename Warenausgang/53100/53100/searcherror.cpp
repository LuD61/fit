#include <windows.h>
#include "searcherror.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "Text.h"


int SEARCHERROR::idx;
long SEARCHERROR::anz;
CHQEX *SEARCHERROR::Query = NULL;
HINSTANCE SEARCHERROR::hMainInst;
HWND SEARCHERROR::hMainWindow;
HWND SEARCHERROR::awin;
CProperty SEARCHERROR::Errors;


int SEARCHERROR::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHERROR::ReadLst (char *Name)
/**
Query-Liste fuellen. 
**/
{
	  char *etc;

	  etc = getenv  ("BWSETC");
	  Text FileName;
	  if (etc != NULL)
	  {
		  FileName.Format ("%s\\error.txt", etc);
	  }
	  else
	  {
		  FileName = "error.txt";
	  }

      if (Errors.Load (FileName) == FALSE)
	  {
		  return -1;
	  }


	  anz = 0;
  	  int i = 0;
	  Text *Item;
	  while ((Item = Errors.GetItemAt (i)) != NULL)
      {
		  Text ListItem;
		  ListItem.Format ("| %s |", Item->GetBuffer ());
	      Query->InsertRecord (ListItem.GetBuffer ());
		  i ++;
      }
      anz = i;
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHERROR::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHERROR::GetKey (char * key)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 1) return FALSE;
      strcpy (key, wort[0]);
      return TRUE;
}

void SEARCHERROR::CopyError (void)
{
	  char *etc;

	  Text *ErrorName;
	  Text Item = Key;
	  Token t;
	  t.SetSep ("|");
	  t = Item;
	  Item = t.NextToken ();
      Item.TrimLeft (); 
      Item.TrimRight (); 
	  ErrorName = Errors.GetValue (Item);
	  if (ErrorName == NULL)
	  {
		  return;
	  }
	  etc = getenv  ("BWSETC");
	  Text Source;
	  Text Dest;
	  if (etc != NULL)
	  {
		  Source.Format ("%s\\%s", etc, ErrorName->GetBuffer ());
		  Dest.Format ("%s\\%s", etc, "error.wav");
	  }
	  else
	  {
		  Source = ErrorName->GetBuffer ();
		  Dest = "error.wav";
	  }
	  CopyFile (Source.GetBuffer (), Dest.GetBuffer (), FALSE);
}
	 

void SEARCHERROR::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  HWND Focus = GetFocus ();
	  idx = -1;
      cx = 40;
      cy = 10;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy, FALSE);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s");
      Query->RowAttr (buffer);
	  Query->VLines ("", 0);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, " %s ", "|Fehlersignal beim Scannen|"); 
//	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
//      Query->SetSortRow (0, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
					 CopyError ();
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
	  SetFocus (Focus);
}

