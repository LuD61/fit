// Dialog.cpp: Implementierung der Klasse CDialog.
//
//////////////////////////////////////////////////////////////////////

#include "Dialog.h"
#include "Application.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CDialog *CDialog:: Instance = NULL;

CDialog::CDialog(DWORD Id, HWND Parent)
{
	this->Id = Id;
	this->Parent = Parent;
	ret = FALSE;
	Instance = this;
}

CDialog::~CDialog()
{

}

void CDialog::AttachControls ()
{
}

BOOL CDialog::OnInitDialog ()
{
	AttachControls ();
	return TRUE;
}

HBRUSH CDialog::OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor)
{
	return NULL;
}

void CDialog::OnOK ()
{
	EndDialog(m_hWnd, wParam); 
}

void CDialog::OnCancel ()
{
	EndDialog(m_hWnd, wParam); 
}


BOOL CDialog::DoModal ()
{
	HINSTANCE hInstance = CApplication::GetInstance ()->GetAppInstance ();
	DialogBox(hInstance,
	          MAKEINTRESOURCE (Id),
              NULL,
			  ((DLGPROC) DlgProc));
	return ret;
}

DWORD CALLBACK CDialog::DlgProc (HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{
    switch (message) 
    { 
        case WM_INITDIALOG:
			Instance->m_hWnd = hwndDlg;
			return Instance->OnInitDialog ();
        case WM_COMMAND: 
            switch (LOWORD(wParam)) 
			{
				case IDOK :
					Instance->ret = TRUE; 
					Instance->wParam = wParam;
					Instance->OnOK ();
					return TRUE; 
				case IDCANCEL :
					Instance->wParam = wParam;
					Instance->OnCancel ();
					return TRUE; 
			}
			break;
		case WM_CTLCOLORSTATIC :
			return (DWORD) Instance->OnCtlColor ((HDC) wParam, (HWND) lParam, CTLCOLOR_STATIC);
			break;

        case WM_SYSCOMMAND: 
            switch (wParam)
			{
				case SC_CLOSE:
					EndDialog(hwndDlg, wParam); 
					return TRUE; 
			}
			break;
	}
	return NULL;
}
