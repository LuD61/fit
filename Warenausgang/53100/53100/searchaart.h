#ifndef _SEARCHAART_DEF
#define _SEARCHAART_DEF
#endif
#include "searchptab.h"

class SEARCHAART : public SEARCHPTAB
{
      public :
          SEARCHAART (char *ptitem) : 
             SEARCHPTAB (ptitem)
          {
          }
          ~SEARCHAART ()
          {
          }

          void InsertRow (char *ptlfnr, char *ptwert, char *ptbez)
          { 
	             char buffer [512];

                 if (atoi (ptwert) == 2)
                 {
                     return;
                 }
                 if (NumVal)
                 {
 	                      sprintf (buffer, "    %4d  |%-36s|", atoi (ptwert), ptbez);
                 }
                 else
                 {
 	                      sprintf (buffer, "    %-4s  |%-36s|", ptwert, ptbez);
                 }
                 Query->InsertRecord (buffer);
          }
};

         
