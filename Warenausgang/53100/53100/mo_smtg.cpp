/* ************************************************************************************* */
/*                                                                                       */
/*                                                                                       */
/*                        Ermitteln der Teilsortimentsgruppe                             */
/*                                                                                       */
/*                                                                                       */
/*                        30.03.2000 W.Roth                                              */
/*                                                                                       */
/*                                                                                       */
/*                                                                                       */
/*                                                                                       */
/* ************************************************************************************* */
#include "mo_smtg.h"
#include "tsmtg.h"

int SMTG_CLASS::GetKunBran2 (short mdn, long kun)
{
           DbClass.sqlin  ((short *)&mdn, 1, 0);
           DbClass.sqlin  ((long *) &kun, 2, 0);
           DbClass.sqlout ((long *) tsmtgg.kun_bran2, 0, 3);
           dsqlstatus = DbClass.sqlcomm ("select kun_bran2 from kun where mdn = ? and kun = ? "
                                         "and kun_bran2 > \"0\"");
           return dsqlstatus;
}
 
long SMTG_CLASS::GetKunSmt (short mdn, long kun)
{
           tsmtgg.k_tsmt_gr = 0l;
           DbClass.sqlin  ((short *)&mdn, 1, 0);
           DbClass.sqlin  ((long *) &kun, 2, 0);
           DbClass.sqlout ((long *) &tsmtgg.k_tsmt_gr, 2, 0);
           dsqlstatus = DbClass.sqlcomm ("select k_tsmt_gr from tsmtgg where mdn = ? and kun = ?");
           return tsmtgg.k_tsmt_gr;
} 
             
long SMTG_CLASS::GetBranSmt (short mdn, long kun)
{
           tsmtgg.k_tsmt_gr = 0l;
           DbClass.sqlin  ((short *)&mdn, 1, 0);
           DbClass.sqlin  ((long *) tsmtgg.kun_bran2, 0, 3);
           DbClass.sqlout ((long *) &tsmtgg.k_tsmt_gr, 2, 0);
           dsqlstatus = DbClass.sqlcomm ("select k_tsmt_gr from tsmtgg "
			   "where mdn = ? and kun_bran2 = ? and kun = 0");
           return tsmtgg.k_tsmt_gr;
} 

long SMTG_CLASS::Get (short mdn, long kun, long teil_smt)
{
           tsmtgg.k_tsmt_gr = GetKunSmt (mdn, kun);
           if (tsmtgg.k_tsmt_gr == 0l)
           {
                   if (GetKunBran2 (mdn, kun) != 0) return teil_smt;         
                   tsmtgg.k_tsmt_gr = GetBranSmt (mdn, kun);
           }
           if (tsmtgg.k_tsmt_gr == 0l)
           {
                   return teil_smt;
           } 
              
           DbClass.sqlin  ((short *) &mdn, 1, 0);
           DbClass.sqlin  ((long *) &tsmtgg.k_tsmt_gr, 2, 0);
           DbClass.sqlin  ((long *) &teil_smt, 2, 0);
           DbClass.sqlout ((long *) &tsmtg.tsmt_gr, 2, 0);
           dsqlstatus = DbClass.sqlcomm ("select tsmt_gr from tsmtg "
                                         "where mdn = ? "
                                         "and k_tsmt_gr = ? "
                                         "and teil_smt = ?");
           if (dsqlstatus != 0)
           {
                     return (long) 9999;
           }
           return tsmtg.tsmt_gr;
} 
  
