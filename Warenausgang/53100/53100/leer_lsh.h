#ifndef _LEER_LSH_DEF
#define _LEER_LSH_DEF
#include "dbclass.h"

struct LEER_LSH {
   short     mdn;
   short     fil;
   long      ls;
   char      blg_typ[2];
   short     party_mdn;
   double    a1;
   char      a_bez1[25];
   double    stk_zu1;
   double    stk_ab1;
   double    stk1;
   double    pr_vk1;
   double    a2;
   char      a_bez2[25];
   double    stk_zu2;
   double    stk_ab2;
   double    stk2;
   double    pr_vk2;
   double    a3;
   char      a_bez3[25];
   double    stk_zu3;
   double    stk_ab3;
   double    stk3;
   double    pr_vk3;
   double    a4;
   char      a_bez4[25];
   double    stk_zu4;
   double    stk_ab4;
   double    stk4;
   double    pr_vk4;
   double    a5;
   char      a_bez5[25];
   double    stk_zu5;
   double    stk_ab5;
   double    stk5;
   double    pr_vk5;
   double    a6;
   char      a_bez6[25];
   double    stk_zu6;
   double    stk_ab6;
   double    stk6;
   double    pr_vk6;
   double    a7;
   char      a_bez7[25];
   double    stk_zu7;
   double    stk_ab7;
   double    stk7;
   double    pr_vk7;
   double    a8;
   char      a_bez8[25];
   double    stk_zu8;
   double    stk_ab8;
   double    stk8;
   double    pr_vk8;
   double    a9;
   char      a_bez9[5];
   double    stk_zu9;
   double    stk_ab9;
   double    stk9;
   double    pr_vk9;
   double    a10;
   char      a_bez10[5];
   double    stk_zu10;
   double    stk_ab10;
   double    stk10;
   double    pr_vk10;
   double    a11;
   char      a_bez11[5];
   double    stk_zu11;
   double    stk_ab11;
   double    stk11;
   double    pr_vk11;
   double    a12;
   char      a_bez12[5];
   double    stk_zu12;
   double    stk_ab12;
   double    stk12;
   double    pr_vk12;
   double    a13;
   char      a_bez13[5];
   double    stk_zu13;
   double    stk_ab13;
   double    stk13;
   double    pr_vk13;
   double    a14;
   char      a_bez14[5];
   double    stk_zu14;
   double    stk_ab14;
   double    stk14;
   double    pr_vk14;
   double    a15;
   char      a_bez15[5];
   double    stk_zu15;
   double    stk_ab15;
   double    stk15;
   double    pr_vk15;
   double    a16;
   char      a_bez16[5];
   double    stk_zu16;
   double    stk_ab16;
   double    stk16;
   double    pr_vk16;
   double    a17;
   char      a_bez17[5];
   double    stk_zu17;
   double    stk_ab17;
   double    stk17;
   double    pr_vk17;
   double    a18;
   char      a_bez18[5];
   double    stk_zu18;
   double    stk_ab18;
   double    stk18;
   double    pr_vk18;
   double    a19;
   char      a_bez19[5];
   double    stk_zu19;
   double    stk_ab19;
   double    stk19;
   double    pr_vk19;
   double    a20;
   char      a_bez20[5];
   double    stk_zu20;
   double    stk_ab20;
   double    stk20;
   double    pr_vk20;
   double    a_gew1;
   double    a_gew2;
   double    a_gew3;
   double    a_gew4;
   double    a_gew5;
   double    a_gew6;
   double    a_gew7;
   double    a_gew8;
   double    a_gew9;
   double    a_gew10;
   double    a_gew11;
   double    a_gew12;
   double    a_gew13;
   double    a_gew14;
   double    a_gew15;
   double    a_gew16;
   double    a_gew17;
   double    a_gew18;
   double    a_gew19;
   double    a_gew20;
};
extern struct LEER_LSH leer_lsh, leer_lsh_null;

#line 6 "leer_lsh.rh"


class LEER_LSH_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LEER_LSH_CLASS () : DB_CLASS ()
               {
               }
};
#endif
