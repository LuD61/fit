#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "ls.h"
#include "a_bas.h"
#include "mdn.h"
#include "fil.h"
#include "ls_txt.h"
#include "mo_kf.h"

#define MAXLEN 40
#define MAXPOS 3000
#define LPLUS 1

#define HNDW 1
#define EIG 2
#define EIG_DIV 3

extern HANDLE  hMainInst;

static unsigned char ausgabebuffer [0x1000];
static HWND mamain1;
static HWND eWindow;

static int ListFocus = 4;

struct LS_TXTS 
{
       char zei [80];
       char txt [80];
};

struct LS_TXTS ls_txts, ls_txttab [MAXPOS];

static ITEM izei         ("zei",        ls_txts.zei,             "", 0);
static ITEM itxt         ("txt",        ls_txts.txt,             "", 0);

static field  _dataform[] = {
&itxt,        60, 0, 0, 6, 0, "",        EDIT,        0, 0, 0, 
};

static form dataform = {1, 0, 0, _dataform, 0, 0, 0, 0, NULL}; 

static int ubrows [] = {0}; 




static ITEM iutxt         ("txt",         "Text",            "", 0);

static field  _ubform[] = {
&iutxt,        60, 1, 0, 6, 0, "",  BUTTON, 0, 0, 0, 
};


static form ubform = {1, 0, 0, _ubform, 0, 0, 0, 0, NULL}; 

static ITEM iline ("", "1", "", 0);

static field  _lineform[] = {
&iline,      1, 0, 0,66, 0, "",  NORMAL, 0, 0, 0, 
};

static form lineform = {1, 0, 0, _lineform, 0, 0, 0, 0, NULL}; 

static char TextNr [10];

static ITEM iNummer ("txt_nr", TextNr,   "Text_nr...:", 0);

static field _ftext [] = {
&iNummer,     9,  0, 1, 1, 0, "%8d", EDIT, 0, 0, 0
};

static form ftext = {1, 0, 0, _ftext, 0, 0, 0, 0, NULL};    


static ListClassDB LS_TXTS;
static LS_TXT_CLASS ls_txt_class;


LSTXTLIST::LSTXTLIST ()
{
    mamainmax = 0;
    mamainmin = 0; 
    eingabesatz = NULL;
    ausgabesatz = ausgabebuffer;
    zlen = 0;
    feld_anz = 0;
    banz = 0;
    PageView = 0;
    LstZiel = NULL;
    LstSatz   = NULL;
    Lstbanz = 0;
    SwRecs = 0;
    AktRow = 0;
    AktColumn = 0;
    scrollpos = 0;
    InfoCaption[0] = (char) 0; 
    WithMenue   = 1;
    WithToolBar = 1;
    dataform.after  = WriteRow; 
    ListAktiv = 0;
    this->hMainWindow = NULL;
}

void LSTXTLIST::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return;

         dataform.mask[i].attribut = attr;
}

int LSTXTLIST::TestAppend (void)
/**
Testen, ob ein Satz angehaengt werden darf.
**/
{
    LS_TXTS.SetPos (LS_TXTS.GetAktRow (), LS_TXTS.FirstColumn ());
    return TRUE;
}

int LSTXTLIST::DeleteLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        LS_TXTS.DeleteLine ();
        return 0;
}

int LSTXTLIST::InsertLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        LS_TXTS.SetPos (LS_TXTS.GetAktRow (), LS_TXTS.FirstColumn ());
        LS_TXTS.InsertLine ();
        return 0;
}

int LSTXTLIST::AppendLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        LS_TXTS.AppendLine ();
        return 0;
}

int LSTXTLIST::WriteRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
    char str [61];
    
    memset (str, ' ', 60);
    str[60] = (char) 0;
    if (strcmp (ls_txts.txt, str) <= 0 &&
        syskey == KEYUP)
    {
        LS_TXTS.DeleteLine ();
        return (1);
    }
    return (0);
}

int LSTXTLIST::TestRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    return (0);
}

void LSTXTLIST::WritePos (int pos)
/**
Position schreiben.
**/
{
       ls_txt.zei       = (long) pos + 1;
       strcpy (ls_txt.txt, ls_txttab[pos].txt);

       ls_txt_class.dbupdate ();
}


int LSTXTLIST::WriteAllPos (void)
/**
Alle Positionen schreiben.
**/
{
    int i;
    int recs;
    int row;
    char str [61];
    
    memset (str, ' ', 60);
    str[60] = (char) 0;

    row     = LS_TXTS.GetAktRow ();

    if (TestRow () == -1)
    {
            return -1;
    }

    memcpy (&ls_txttab[row], &ls_txts, sizeof (struct LS_TXTS));
    recs = LS_TXTS.GetRecanz ();
    ls_txt_class.delete_aufpposi ();
    for (i = 0; i < recs - 1; i ++)
    {
        WritePos (i);
    }
    if (strcmp (ls_txttab[i].txt, str))
    {
        WritePos (i);
    }
    LS_TXTS.BreakList ();
    RestoreAuf ();
    return 0;
}


void LSTXTLIST::SaveAuf (void)
{
    save_fkt (5);
    save_fkt (6);
    save_fkt (7);
    save_fkt (8);
    save_fkt (12);
}

void LSTXTLIST::SetAuf (void)
{
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);
    set_fkt (AppendLine, 8);
    set_fkt (WriteAllPos, 12);
}

void LSTXTLIST::RestoreAuf (void)
{
    restore_fkt (5);
    restore_fkt (6);
    restore_fkt (7);
    restore_fkt (8);
    restore_fkt (12);
}

int LSTXTLIST::dokey5 (void)
/**
Listenerfassung abbrechen.
**/
{
    if (abfragejn (mamain1, "Texte speichern ?", "J"))
    {
           WriteAllPos ();
           syskey = KEY12;
    }
	else
	{
           syskey = KEY5;
           RestoreAuf ();
	}

    LS_TXTS.SetListFocus (0);
    LS_TXTS.SetFeldFocus0 (LS_TXTS.GetAktRow (), LS_TXTS.GetAktColumn ());

    LS_TXTS.BreakList ();
    return 1;
}


void LSTXTLIST::InitSwSaetze (void)
{
       int i;

       for (i = 0; i < MAXPOS; i ++)
       {
               SwSaetze [i] = (char *) &ls_txttab [i];
       }
}

void LSTXTLIST::SetRecHeight (void)
/**
Anzahl Zeilen pro Listzeile ermitteln.
**/
{
       int height;
       int i;

       height = 0;
       for (i = 0; i < dataform.fieldanz; i ++)
       {
           if (dataform.mask[i].attribut & REMOVED) continue;
           if (dataform.mask[i].pos[0] > height)
           {
               height = dataform.mask[i].pos[0];
           }
       }
       height ++; 
       LS_TXTS.SetRecHeight (height);
}

int LSTXTLIST::ToMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       LS_TXTS.SetRecanz (pos + 1);
       return 0;
}


void LSTXTLIST::SetStringEnd (char *feldname, int len)
/**
Stringende Setzen.
**/
{

       return;
}


void LSTXTLIST::uebertragen (void)
/**
Eingabesatz in ASCII-Striktur uebertragen.
**/
{
       sprintf (ls_txts.zei,     "%8ld",    ls_txt.zei);
       sprintf (ls_txts.txt,     "%s",      ls_txt.txt);
}

void LSTXTLIST::ShowDB (void)
/**
Auftragspositionen lesen und anzeigen.
**/
{
        int i;
        HCURSOR oldcursor;
        extern short do_exit;
        int dsqlstatus;


        InitSwSaetze ();
        LS_TXTS.SetRecanz (0);
        i = LS_TXTS.GetRecanz ();
        LS_TXTS.SetUbForm (&ubform);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        dsqlstatus = ls_txt_class.dbreadfirst ();
        while (dsqlstatus == 0)
        {
                     uebertragen ();
                     if (ToMemory (i) != 0) break;
                     i = LS_TXTS.GetRecanz ();
                     if (i >= MAXPOS) break; 
                     dsqlstatus = ls_txt_class.dbread ();
        }

        SetRecHeight ();
        SetCursor (oldcursor);
        i = LS_TXTS.GetRecanz ();

        LS_TXTS.SetRecanz (i);
        SwRecs = LS_TXTS.GetRecanz ();
 
        LS_TXTS.SetDataForm0 (&dataform, &lineform);
        LS_TXTS.SetSaetze (SwSaetze);
//        LS_TXTS.SetChAttr (ChAttr); 
        LS_TXTS.SetUbRows (ubrows); 
        if (i == 0)
        {
                 LS_TXTS.AppendLine ();
				 AktRow = AktColumn = 0;
                 LS_TXTS.SetPos (0, 0);
                 i = LS_TXTS.GetRecanz ();
        }
        SendMessage (LS_TXTS.Getmamain2 (), WM_SIZE, NULL, NULL);
        InvalidateRect (LS_TXTS.Getmamain3 (), 0, TRUE);
        memcpy (&ls_txts, &ls_txttab[0], sizeof (struct LS_TXTS));
}

void LSTXTLIST::ReadDB (void)
/**
Auftragspositionen lesen.
**/
{
        ausgabesatz = (unsigned char *) &ls_txts;
        zlen = sizeof (struct LS_TXTS);

        PageView = 0;
        LS_TXTS.SetPageView (0);
        feld_anz = dataform.fieldanz;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;

        Lstbanz = feld_anz;

        LS_TXTS.SetPos (AktRow, AktColumn);
        LS_TXTS.Setbanz (feld_anz);
        LS_TXTS.Setzlen (zlen);
        LS_TXTS.Initscrollpos ();
        LS_TXTS.SetAusgabeSatz (ausgabesatz);
}

void LSTXTLIST::DestroyWindows (void)
{
       LS_TXTS.DestroyListWindow ();
       DestroyMainWindow ();
       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
}

void LSTXTLIST::ChoiseLines (HDC hdc)
{
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         RECT rect;
         int x, y;
         int cx, cy;

         if (eWindow == NULL) return;

         GetClientRect (eWindow, &rect);
         x = tm.tmAveCharWidth / 2;
         y = tm.tmHeight / 2;

         cx = rect.right - tm.tmAveCharWidth / 2;
         cy = rect.bottom - tm.tmHeight / 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         SelectObject (hdc, hPenG);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

         SelectObject (hdc, hPenW);


         y ++;
         x ++;
         cx --;
         cy --;
         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         cx += 2;
         cy += 2;
         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);
}


long LSTXTLIST::EnterNr (long nr)
/**
Nummer Eingeben.
**/
{
          int currentf;
          currentf = currentfield;
          sprintf (TextNr, "%ld", nr);
          break_end ();
          enter_form (eWindow, &ftext, 0, 0);
          display_form (eWindow, &ftext, 0, 0);
          no_break_end ();
          currentfield = currentf;
          return atol (TextNr);
}
      

void LSTXTLIST::EnterLsp (char *ubtext, long nr)
/**
Auftragsliste bearbeiten.
**/

{
       static int initaufp = 0;

       if (ubtext)
       {
                 iutxt.SetFeldPtr (ubtext);
       }
       else
       {
                 iutxt.SetFeldPtr ("Text");
       }
       ls_txt.nr = nr;
       eWindow = CreateEnter ();

       set_fkt (NULL, 5);
       set_fkt (NULL, 12);
       set_fkt (NULL, 6);
       set_fkt (NULL, 7);
       set_fkt (NULL, 8);
       set_fkt (NULL, 9);
       set_fkt (NULL, 10);
       set_fkt (NULL, 11);

       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (9, leer, NULL);
       SetFkt (10, leer, NULL);
       SetFkt (11, leer, NULL);


       ls_txt.nr = EnterNr (ls_txt.nr);
       if (ls_txt.nr == 0l)
       {
                 SetFkt (6, leer, NULL);
                 SetFkt (7, leer, NULL);
                 SetFkt (8, leer, NULL);
                 SetFkt (11, leer, NULL);
                 set_fkt (NULL, 11);
                 CloseControls (&ftext);
                 DestroyWindow (eWindow);
                 eWindow = NULL;
                 disp_mess ("Text-Nummer 0 ist nicht erlaubt", 2); 
                 return;
       }
       if (syskey == KEY5)
       {
                 CloseControls (&ftext);
                 DestroyWindow (eWindow);
                 eWindow = NULL;
                 return;
       }
       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);
       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);

       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (8, anfuegen, KEY8);

       if (initaufp == 0)
       {
                 LS_TXTS.SetTestAppend (TestAppend);
//                 sprintf (InfoCaption, "Auftrag %ld", auf);
                 mamain1 = CreateMainWindow ();
                 LS_TXTS.InitListWindow (mamain1);
                 ReadDB ();
                 initaufp = 1;
       }

       LS_TXTS.SetListFocus (ListFocus);
       LS_TXTS.Initscrollpos ();
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;
       SetAktivWindow (LS_TXTS.Getmamain2 ());
       ShowDB ();
     
       ListAktiv = 1;
       LS_TXTS.ProcessMessages ();

       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
       LS_TXTS.DestroyListWindow ();
       DestroyMainWindow ();
       CloseControls (&ftext);
       initaufp = 0;
       return;
}


void LSTXTLIST::DestroyMainWindow (void)
/**
Hauptfenster fuer Liste loeschen.
**/
{
        if (mamain1 == NULL) return;
        DestroyWindow (mamain1);
        mamain1 = NULL;
        if (eWindow == NULL) return;
        DestroyWindow (eWindow);
        eWindow = NULL;
}


HWND LSTXTLIST::CreateEnter (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
//        TEXTMETRIC tm;
        HDC hdc;

        if (hMainWindow == NULL) return NULL;    
           
        if (eWindow) return eWindow;

        LS_TXTS.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);


        y = (wrect.bottom - 16 * tm.tmHeight);
        x = wrect.left + 2 + 4 * tm.tmAveCharWidth;
        cy = 4 * tm.tmHeight;
        cx = rect.right - 8 * tm.tmAveCharWidth;

        eWindow       = CreateWindow (
                                       "ListMain",
//                                       "hListWindow", 
                                       InfoCaption,
                                       WS_DLGFRAME | 
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWindow,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);
        hdc = GetDC (eWindow);
        ChoiseLines (hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}


HWND LSTXTLIST::CreateMainWindow (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWindow == NULL) return NULL;    
           
        if (mamain1) return mamain1;

        LS_TXTS.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);


        y = (wrect.bottom - 12 * tm.tmHeight);
        x = wrect.left + 2 + 4 * tm.tmAveCharWidth;
        cy = wrect.bottom - y - 2;
        cx = rect.right - 8 * tm.tmAveCharWidth;

        mamain1 = CreateWindow ("ListMain",
                                    InfoCaption,
                                    WS_THICKFRAME | 
//                                    WS_CAPTION | 
//                                    WS_CHILD |
                                    WS_POPUP,
//                                    WS_SYSMENU |
//                                    WS_MINIMIZEBOX |
//                                    WS_MAXIMIZEBOX,
                                    x, y,
                                    cx, cy,
                                    hMainWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (mamain1, SW_SHOWNORMAL);
        UpdateWindow (mamain1);
        return mamain1;
}

static int IsMax = 0;
static int IsMin = 0;

void LSTXTLIST::SetMax0 (int val)
{
    IsMax = val;
    if (IsMax)
    {
             SetFkt (11, fenster, KEY11);
    }
    else
    {
             SetFkt (11, vollbild, KEY11);
    }
}

void LSTXTLIST::SetMin0 (int val)
{
    IsMin = val;
}


int LSTXTLIST::Schirm (void)
{
         if (IsMin) return 0;
         
         if (IsMax)
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_RESTORE, 0l);
             IsMax = 0;
         }
         else
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_MAXIMIZE, 0l);
             IsMax = 1;
         }
         return 0;
}

void LSTXTLIST::MoveeWindow ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        LS_TXTS.GetTextMetric (&tm);
        GetClientRect (mamain1, &rect);
        GetWindowRect (mamain1, &wrect);
        y = (wrect.top - 4 * tm.tmHeight);
        x = wrect.left;
        cy = 4 * tm.tmHeight;
        cx = wrect.right - wrect.left;
        MoveWindow (eWindow, x,y, cx, cy, TRUE);
}


void LSTXTLIST::MoveMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        LS_TXTS.GetTextMetric (&tm);
        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        if (mamainmax)
        {
                 x = wrect.left + 2;
                 y = wrect.top + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        else
        {
/*
                 y = (wrect.bottom - 12 * tm.tmHeight);
                 x = wrect.left + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
*/
                 y = (wrect.bottom - 12 * tm.tmHeight);
                 x = wrect.left + 2 + 4 * tm.tmAveCharWidth;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right - 8 * tm.tmAveCharWidth;
        }
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
        MoveeWindow ();
}


void LSTXTLIST::MaximizeMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        x = wrect.left + 2;
        y = wrect.top + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}

HWND LSTXTLIST::GetMamain1 (void)
{
       return (mamain1);
}

void LSTXTLIST::SethwndTB (HWND hwndTB)
{
         this->hwndTB = hwndTB; 
         LS_TXTS.SethwndTB (hwndTB);
}

void LSTXTLIST::SetTextMetric (TEXTMETRIC *tm)
{
         LS_TXTS.SetTextMetric (tm);
}


void LSTXTLIST::SetLineRow (int LineRow)
{
         LS_TXTS.SetLineRow (0);
}

void LSTXTLIST::SetListLines (int i)
{ 
         LS_TXTS.SetListLines (i);
}

void LSTXTLIST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        HDC hdc;

        LS_TXTS.OnPaint (hWnd, msg, wParam, lParam);
        if (hWnd == eWindow)  
        {
                    hdc = BeginPaint (eWindow, &aktpaint);
                    ChoiseLines (hdc); 
                    EndPaint (eWindow, &aktpaint);
        }
}


void LSTXTLIST::MoveListWindow (void)
{
        LS_TXTS.MoveListWindow ();
}


void LSTXTLIST::OnSize (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        if (hWnd == GetMamain1 ())
        {
                    MoveListWindow ();
                    MoveeWindow ();
        }
}


void LSTXTLIST::BreakList (void)
{
        LS_TXTS.BreakList ();
}


void LSTXTLIST::OnHScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        LS_TXTS.OnHScroll (hWnd, msg,wParam, lParam);
}

void LSTXTLIST::OnVScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        LS_TXTS.OnVScroll (hWnd, msg,wParam, lParam);
}


void LSTXTLIST::FunkKeys (WPARAM wParam, LPARAM lParam)
{
                    LS_TXTS.FunkKeys (wParam, lParam);
}


int LSTXTLIST::GetRecanz (void)
{
          return LS_TXTS.GetRecanz ();
}


void LSTXTLIST::SwitchPage0 (int rows)
{
           LS_TXTS.SwitchPage0 (rows);
}


HWND LSTXTLIST::Getmamain2 (void)
{
           return LS_TXTS.Getmamain2 ();
}


HWND LSTXTLIST::Getmamain3 (void)
{
           return LS_TXTS.Getmamain3 ();
}


void LSTXTLIST::ChoiseFont (mfont *lfont)
{
           LS_TXTS.ChoiseFont (lfont);
}

void LSTXTLIST::SetFont (mfont *lfont)
{
           LS_TXTS.SetFont (lfont);
}

void LSTXTLIST::SetListFont (mfont *lfont)
{
           LS_TXTS.SetListFont (lfont);
}

void LSTXTLIST::FindString (void)
{
           LS_TXTS.FindString ();
}


void LSTXTLIST::SetLines (int Lines)
{
           LS_TXTS.SetLines (Lines);
}


int LSTXTLIST::GetAktRow (void)
{
                 return LS_TXTS.GetAktRow ();
}

int LSTXTLIST::GetAktRowS (void)
{
                 return LS_TXTS.GetAktRowS ();
}

void LSTXTLIST::SetColors (COLORREF Color, COLORREF BkColor)
{
                 LS_TXTS.SetColors (Color, BkColor); 
}

void LSTXTLIST::SetListFocus (void)
{
                 LS_TXTS.SetFeldFocus0 (LS_TXTS.GetAktRow (), 
                                       LS_TXTS.GetAktColumn ()); 
}

