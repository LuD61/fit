// NewCalculate.cpp: Implementierung der Klasse CNewCalculate.
//
//////////////////////////////////////////////////////////////////////

#include "NewCalculate.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CDllPreise CNewCalculate::DllPreise;

short dmdn;
long dls;


CNewCalculate::CNewCalculate()
{
	error = FALSE;
}

CNewCalculate::~CNewCalculate()
{

}

void CNewCalculate::Calculate ()
{
	beginwork ();
	memcpy (&lsks, &lsk, sizeof (LSK));
	memcpy (&lsps, &lsp, sizeof (LSP));
	Dbase.sqlin ((short *)    &lsp.mdn,  1, 0);
	Dbase.sqlin ((long *)     &lsp.ls,   2, 0);
	Dbase.sqlout ((long *)    &lsp.posi, 2, 0);
	Dbase.sqlout ((double *)  &lsp.a,    3, 0);
	cursor = Dbase.sqlcursor ("select posi, a from lsp where mdn = ? and ls = ?");

	Dbase.sqlout ((short *)    &dmdn,  1, 0);
	Dbase.sqlout ((long *)     &dls,   2, 0);
	cursorls = Dbase.sqlcursor ("select mdn,ls from ls where 1 = 1");
//	LskArray->pos = 0;
//	while ((lskl = LskArray->GetNext ()) != NULL)
//	{
//		CalculateLs ();
//	}

	Dbase.sqlopen (cursorls);
	while (Dbase.sqlfetch (cursorls) == 0)
	{
		CalculateLs ();
	}

	Dbase.sqlclose (cursor);
	Dbase.sqlclose (cursorls);
	commitwork ();
	memcpy (&lsk, &lsks, sizeof (LSK));
	memcpy (&lsp, &lsps, sizeof (LSP));
}

void CNewCalculate::CalculateLs ()
{
//	int dsqlstatus = Ls.lese_lsk (lskl->mdn, 0, lskl->ls);
	int dsqlstatus = Ls.lese_lsk (dmdn, 0, dls);
	if (dsqlstatus != 0)
	{
		return;
	}
//	lsp.mdn  = lskl->mdn;
//	lsp.ls   = lskl->ls;
	lsp.mdn  = dmdn;
	lsp.ls   = dls;
	lsp.fil = 0;
	Dbase.sqlopen (cursor);
	while (Dbase.sqlfetch (cursor) == 0)
	{
		Ls.lese_lsp_ap (lsp.mdn, lsp.fil, lsp.ls, lsp.a, lsp.posi);
		ReadPrEx ();
	}
}

void CNewCalculate::ReadPrEx (void)
/**
Artikelpreis holen.
**/
{
    char lieferdat [12];

       int dsqlstatus;
       short sa; 
       double pr_ek;
       double pr_vk;
	   BOOL fix;
	   int dsqlstatusw = 100;


// Preise immer mit Filiale 0 lesen.
// WAL-103  1. Priorität Werbung holen  aus der Preisliste   auf kun_erw.werbung
	   // Kennung kein fixer Preis, variabel , zus. zum Normalpreis :   ----> aufp.ksys = 2   und aufp.bestellvorschl =  WerbePreis
	   // Kennung fixer WerbePreis,                                 :   ----> aufp.ksys = 2   und aufp.bestellvorschl =  WerbePreis  aufp.auf_vk_pr = WerbePreis
	   fix = FALSE;
	   int werbung = 0;
	   double werbepreis = 0.0;
	   /***
		 Dbase.sqlin ((short *) &lsk.mdn, 1, 0);
		 Dbase.sqlin ((long *) &lsk.kun, 2, 0);
		 Dbase.sqlout ((long *) &werbung, 2, 0);
		 Dbase.sqlcomm ("select werbung from kun_erw where mdn = ? and fil = 0 and kun = ?");
	   if (lsk.kun_fil == 0 && werbung > 0)
	   {
					dsqlstatusw = WaPreis.werbung_holen (lsk.mdn,
                                          0,
                                          werbung,
                                           lsp.a,
                                          lskl->lieferdat,,
                                          &sa_werbung,
                                          &pr_ek,
										  &fix);

					if (sa_werbung == 1 && fix == TRUE) 
					{
		                werbepreis = pr_ek;

					}


	   }
	   *******/
	dlong_to_asc (lsk.lieferdat, lieferdat);

	   if (DllPreise.PriceLib != NULL && 
	    DllPreise.preise_holen != NULL)
	   {
				  dsqlstatus = (DllPreise.preise_holen) (lsk.mdn, 
                                          0,
                                          lsk.kun_fil,
                                          lsk.kun,
                                          lsp.a,
//                                          lskl->lieferdat,
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
				  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
	   }
  	   else
	   {
				  return;	
	   }
	   /**
	   if (fix == TRUE && dsqlstatusw == 0) //WAL-103
	   {
			pr_ek = werbepreis;
			sa = sa_werbung;
	   }
	   **/


       if (dsqlstatus == 0)
       {
		   /****  temporär!!   Hier nicht preis überschreiben, sondern danegen stellen ! 
		         lsp.ls_vk_pr = pr_ek;
		         lsp.ls_lad_pr = pr_vk;
				 lsp.sa_kz_sint = sa;
		         lsp.ls_vk_euro = pr_ek;
		         lsp.ls_lad_euro = pr_vk;
***/
//temporär : wg. Korr. Walser 29.05.2014

		         if (getenv ("PREISEFALSCH") != NULL)
				 {
			         lsp.ls_lad_fremd = pr_ek;
				 }
				 else
				 {
					lsp.ls_vk_fremd = pr_ek;
				 }

				 Ls.update_lsp (lsp.mdn, lsp.fil, lsp.ls, lsp.a, lsp.posi);
       }
}
