// Debug.cpp: Implementierung der Klasse CDebug.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "Debug.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

// #define DEBUG_ON

CDebug *CDebug::Instance = NULL;

CDebug::CDebug()
{
	fp = NULL;
	strcpy (Name, "C:\\temp\\53100.dbg");

}

CDebug::~CDebug()
{
	if (fp != NULL)
	{
		Close ();
	}

}

CDebug *CDebug::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CDebug ();
	}
	return Instance;
}


void CDebug::Open ()
{
	fp = fopen (Name, "w");
}

void CDebug::Write ()
{

#ifdef DEBUG_ON
	if (fp == NULL)
	{
		Open ();
	}
	if (fp != NULL)
	{
		fprintf (fp, "%s\n", Line);
		fflush (fp);
	}
#endif
}

void CDebug::Close ()
{
	if (fp != NULL)
	{
		fclose (fp);
		fp = NULL;
	}
}
