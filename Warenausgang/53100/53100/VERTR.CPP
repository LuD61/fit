#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "vertr.h"
#include "dbclass.h"
#include "dbfunc.h"

struct VERTR vertr, vertr_null;

static DB_CLASS DbClass;
#include "itemc.h"
#define MAXSORT 30000
static long MaxSort;

static char *sqltext;

static int dosort1 ();
static int dosort2 ();

struct SORT_AW
{
	     char sort1 [10];
	     char sort2 [37];
	     int  sortidx;
};

//static struct SORT_AW sort_aw, sort_awtab[MAXSORT];
static struct SORT_AW sort_aw, *sort_awtab;

static void SortMalloc (void) 
/**
Speicher fuer Listbereich zuordnen.
**/
{
	static BOOL SortOK = FALSE;
    long anz;

	if (SortOK) return;

	DbClass.sqlout ((long *) &anz, 2, 0);
	DbClass.sqlcomm ("select count (*) from vertr");

	if (anz == 0l) return;

    sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   ((anz + 1) * sizeof (struct SORT_AW))); 
	while (sort_awtab == NULL)
	{
		if (anz == 0l) break;
		anz --;
        sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   (anz * sizeof (struct SORT_AW))); 
	}
	MaxSort = anz + 1;
	if (anz == 0l)
	{
		print_mess (2, "Es konnte kein Speicher fuer die Artikelauswahl (a_hndw) zugewiesen werden");
		return;
	}
	SortOK = TRUE;
}


static int sort_awanz;
static int sort1 = -1;
static int sort2 = -1;
static int sort_idx = 0;

static ITEM isort1    ("", sort_aw.sort1,    "", 0);
static ITEM isort2    ("", sort_aw.sort2, "", 0);

static field _fsort_aw[] = {
&isort1,      9, 1, 0, 0, 0, "%8d", NORMAL, 0, 0, 0,
&isort2,     16, 1, 0,10, 0, "",    NORMAL, 0, 0, 0,
};

static form fsort_aw = {2, 0, 0, _fsort_aw, 0, 0, 0, 0, NULL};

static ITEM isort1ub    ("", "Vertreter",  "", 0);
static ITEM isort2ub    ("", "Name",     "", 0);
static ITEM ifillub     ("", " ",             "", 0); 

static field _fsort_awub[] = {
&isort1ub,       10, 1, 0, 0, 0, "", BUTTON, 0, dosort1,    102,
&isort2ub,       16, 1, 0,10, 0, "", BUTTON, 0, dosort2,    103,
&ifillub,        80, 1, 0,26, 0, "", BUTTON, 0, 0, 0};

static form fsort_awub = {3, 0, 0, _fsort_awub, 0, 0, 0, 0, NULL};

static ITEM isortl ("", "1", "", 0);

static field _fsort_awl[] = {
&isortl,          1, 1, 0, 10, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 26, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awl = {2, 0, 0, _fsort_awl, 0, 0, 0, 0, NULL};

static int dosort10 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

 	      el1 = (struct SORT_AW *) elem1;
		el2 = (struct SORT_AW *) elem2;
	      return ((int) (atol(el1->sort1) - atol (el2->sort1)) * 
				                                  sort1);
}


int dosort1 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort10);
	sort1 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static int dosort20 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return (strcmp (el1->sort2,el2->sort2) * sort2);
}


int dosort2 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort20);
	sort2 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}

static void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        sort_idx = idx;
        break_list ();
        return;
}

static int endsort (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}


void VERTR_CLASS::prepare_vertr (void)
/**
Cursor zum Vertreter-Lesen vorbereiten.
**/
{
    out_quest ((char *) &vertr.mdn,1,0);
    out_quest ((char *) &vertr.fil,1,0);
    out_quest ((char *) &vertr.vertr,2,0);
    out_quest ((char *) &vertr.adr,2,0);
    out_quest ((char *) vertr.bank_nam,0,37);
    out_quest ((char *) &vertr.blz,2,0);
    out_quest ((char *) vertr.kto_nr,0,17);
    out_quest ((char *) &vertr.hausbank,1,0);
    out_quest ((char *) vertr.frei_txt1,0,65);
    out_quest ((char *) vertr.vertr_krz,0,17);
    out_quest ((char *) vertr.vertr_gebiet,0,25);
    out_quest ((char *) &vertr.norm_ums_tdm,3,0);
    out_quest ((char *) &vertr.norm_ums_p,3,0);
    out_quest ((char *) &vertr.akt_ums_tdm,3,0);
    out_quest ((char *) &vertr.akt_ums_p,3,0);
    out_quest ((char *) &vertr.son_ums_tdm,3,0);
    out_quest ((char *) &vertr.son_ums_p,3,0);
    out_quest ((char *) &vertr.bon_grenz_tdm,3,0);
    out_quest ((char *) &vertr.bon_grenz_p,3,0);
    out_quest ((char *) &vertr.jr_plan_ums,3,0);
    out_quest ((char *) vertr.statk_period,0,2);
    out_quest ((char *) &vertr.delstatus,1,0);
    out_quest ((char *) _adr.adr_krz, 0, 17);

    ins_quest ((char *) &vertr.vertr,2,0);
    ins_quest ((char *) &vertr.mdn,1,0);
    ins_quest ((char *) &vertr.fil,1,0);
 
cursor_vertr = prepare_sql ("select vertr.mdn,  vertr.fil,  "
"vertr.vertr,  vertr.adr,  vertr.bank_nam,  vertr.blz,  vertr.kto_nr,  "
"vertr.hausbank,  vertr.frei_txt1,  vertr.vertr_krz,  "
"vertr.vertr_gebiet,  vertr.norm_ums_tdm,  vertr.norm_ums_p,  "
"vertr.akt_ums_tdm,  vertr.akt_ums_p,  vertr.son_ums_tdm,  "
"vertr.son_ums_p,  vertr.bon_grenz_tdm,  vertr.bon_grenz_p,  "
"vertr.jr_plan_ums,  vertr.statk_period,  vertr.delstatus, adr.adr_krz "
                            "from vertr, outer adr "
                            "where vertr.vertr = ? "
                            "and vertr.mdn = ? "
                            "and vertr.fil = ? "
                            "and vertr.adr = adr.adr");
}

int VERTR_CLASS::lese_vertr (short mdn, short fil, long vertr_nr)
/**
Tabelle vertr lesen.
**/
{
         if (cursor_vertr == -1)
         {
                      prepare_vertr ();
         }
         vertr.mdn = mdn;
         vertr.fil = fil;
         vertr.vertr = vertr_nr;
         open_sql (cursor_vertr);
         fetch_sql (cursor_vertr);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int VERTR_CLASS::lese_vertr (void)
/**
Naechsten Satz aus Tabelle vertr lesen.
**/
{
         fetch_sql (cursor_vertr);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

#ifndef CONSOLE

int VERTR_CLASS::ShowBuQuery (HWND hWnd, int ws_flag) 
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		HWND eWindow;
	    form *savecurrent;

		SortMalloc ();
	    savecurrent = current_form;
        if (qschange)
        {
   		    sort_awanz = 0;
		    while (fetch_scroll (cursor_ausw, NEXT) == 0)
            {
			         sprintf (sort_awtab[sort_awanz].sort1, "%ld",
						      vertr.vertr);
					 strcpy (sort_awtab[sort_awanz].sort2, 
						     _adr.adr_krz);
					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 sort_awanz ++;
					 if (sort_awanz == MAXSORT) break;
            }
        }

		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
        eWindow = OpenListWindowEnF (10, 40, 8, 20, 0);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
		Setlistenter (1);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
         }
		 sort_idx = sort_awtab[sort_idx].sortidx;
         fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
         return 0;
}

int VERTR_CLASS::prep_awcursor (char *sqlstring)
/**
Cursor fuer Auswahl vorbereiten.
**/
{
         int cursor_ausw;

         cursor_ausw = prepare_scroll (sqlstring);
         if (sqlstatus)
         {
                     return (-1);
         }
         return (cursor_ausw);
}


int VERTR_CLASS::PrepareQuery (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         short old_mode;
	     char sqlstring [1000]; 
         ReadQuery (qform, qnamen); 
         if (strcmp (qstring0, qstring) == 0)
         {
                   qschange = 0;
         }
         else
         {
                   qschange = 1;
                     
         }

		 if (cursor_ausw != -1)
		 {
                  close_sql (cursor_ausw);
		          cursor_ausw = -1;
		 }
         strcpy (qstring0, qstring);

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select vertr.vertr, "
                            "adr.adr_krz "
							"from vertr, adr "
                            "where vertr  > 0 "
                             "and vertr.mdn = ? "
                             "and vertr.fil = ? "
                             "and adr.adr = vertr.adr "
                             "and %s "
							 "order by vertr", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
					   "select vertr.vertr, "
                            "adr.adr_krz "
							"from vertr, adr "
                            "where vertr > 0 and adr.adr = vertr.adr "
                             "and vertr.mdn = ? "
                             "and vertr.fil = ? "
                            "order by vertr");
         }

         ins_quest ((char *) &vertr.mdn, 1, 0);
         ins_quest ((char *) &vertr.fil, 1, 0);

         out_quest ((char *) &vertr.vertr, 2, 0);
         out_quest ((char *) _adr.adr_krz, 0, 16);
         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prep_awcursor (sqlstring);
         sql_mode = old_mode;
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}
#endif
