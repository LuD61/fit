#ifndef _MO_SMTG_DEF
#define _MO_SMTG_DEF
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"

class SMTG_CLASS 
{
          private :
               DB_CLASS DbClass;
               int dsqlstatus;
          public :
              SMTG_CLASS ()
              {
              }     
              int GetKunBran2 (short, long);
              long GetKunSmt (short, long);
              long GetBranSmt (short, long);
              long Get(short, long, long);
};
#endif