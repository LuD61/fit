// NewCalculate.h: Schnittstelle f�r die Klasse CNewCalculate.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NEWCALCULATE_H__C3C52A86_A0DC_4013_8BEA_E25231E322AD__INCLUDED_)
#define AFX_NEWCALCULATE_H__C3C52A86_A0DC_4013_8BEA_E25231E322AD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "DataCollection.h"
#include "LskList.h"
#include "wmaskc.h"
#include "ls.h"
#include "DbClass.h"
#include "DllPreise.h"
#include "mdn.h"
#include "fil.h"
#include "kun.h"


class CNewCalculate  
{
private:
	CDataCollection<CLskList> *LskArray;
	LS_CLASS Ls;
	struct LSK lsks;
	struct LSP lsps;
	CLskList *lskl;
	BOOL error;
	int cursor;
	int cursorls;
	DB_CLASS Dbase;
	static CDllPreise DllPreise;

public:
	CNewCalculate();
	virtual ~CNewCalculate();
	void SetLskArray (CDataCollection<CLskList> *LskArray)
	{
		this->LskArray = LskArray;
	}

	BOOL GetError ()
	{
		return error;
	}

	void Calculate ();
	void CalculateLs ();
    void ReadPrEx ();
};

#endif // !defined(AFX_NEWCALCULATE_H__C3C52A86_A0DC_4013_8BEA_E25231E322AD__INCLUDED_)
