#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "mo_curso.h"
#include "dbclass.h"
#include "leer_bel.h"

struct LEER_BEL leer_bel, leer_bel_null;

void LEER_BEL_CLASS::prepare (void)
{
            TCHAR *sqltext;

            ins_quest ((char *)   &leer_bel.mdn, SQLSHORT, 0);
            ins_quest ((char *)   &leer_bel.a, SQLDOUBLE, 0);
            ins_quest ((char *)   &leer_bel.nr, SQLLONG, 0);
            ins_quest ((char *)   &leer_bel.kun, SQLLONG, 0);
            ins_quest ((char *)   &leer_bel.kun_fil, SQLSHORT, 0);
    out_quest ((char *) &leer_bel.mdn,1,0);
    out_quest ((char *) &leer_bel.kun_fil,1,0);
    out_quest ((char *) &leer_bel.nr,2,0);
    out_quest ((char *) leer_bel.blg_typ,0,2);
    out_quest ((char *) &leer_bel.a,3,0);
    out_quest ((char *) &leer_bel.anzahl,2,0);
    out_quest ((char *) &leer_bel.bestand,2,0);
    out_quest ((char *) &leer_bel.datum,2,0);
    out_quest ((char *) leer_bel.zeit,0,9);
    out_quest ((char *) leer_bel.pers_nam,0,17);
    out_quest ((char *) &leer_bel.kun,2,0);
    out_quest ((char *) &leer_bel.hkun,2,0);
    out_quest ((char *) &leer_bel.delstatus,1,0);
            cursor = sqlcursor ("select leer_bel.mdn,  "
"leer_bel.kun_fil,  leer_bel.nr,  leer_bel.blg_typ,  leer_bel.a,  "
"leer_bel.anzahl,  leer_bel.bestand,  leer_bel.datum,  leer_bel.zeit,  "
"leer_bel.pers_nam,  leer_bel.kun,  leer_bel.hkun,  leer_bel.delstatus from leer_bel "

#line 23 "leer_bel.rpp"
                                  "where mdn = ? "
				  "and a = ? "
 				  "and nr = ? "
 				  "and kun = ? "
				  "and kun_fil = ?");
    ins_quest ((char *) &leer_bel.mdn,1,0);
    ins_quest ((char *) &leer_bel.kun_fil,1,0);
    ins_quest ((char *) &leer_bel.nr,2,0);
    ins_quest ((char *) leer_bel.blg_typ,0,2);
    ins_quest ((char *) &leer_bel.a,3,0);
    ins_quest ((char *) &leer_bel.anzahl,2,0);
    ins_quest ((char *) &leer_bel.bestand,2,0);
    ins_quest ((char *) &leer_bel.datum,2,0);
    ins_quest ((char *) leer_bel.zeit,0,9);
    ins_quest ((char *) leer_bel.pers_nam,0,17);
    ins_quest ((char *) &leer_bel.kun,2,0);
    ins_quest ((char *) &leer_bel.hkun,2,0);
    ins_quest ((char *) &leer_bel.delstatus,1,0);
            sqltext = "update leer_bel set leer_bel.mdn = ?,  "
"leer_bel.kun_fil = ?,  leer_bel.nr = ?,  leer_bel.blg_typ = ?,  "
"leer_bel.a = ?,  leer_bel.anzahl = ?,  leer_bel.bestand = ?,  "
"leer_bel.datum = ?,  leer_bel.zeit = ?,  leer_bel.pers_nam = ?,  "
"leer_bel.kun = ?,  leer_bel.hkun = ?,  leer_bel.delstatus = ? "

#line 29 "leer_bel.rpp"
                                  "where mdn = ? "
				  "and datum = ? "
 				  "and zeit = ?";
            ins_quest ((char *)   &leer_bel.mdn,   SQLSHORT, 0);
            ins_quest ((char *)   &leer_bel.datum, SQLLONG, 0);
            ins_quest ((char *)   leer_bel.zeit,  SQLCHAR, sizeof (leer_bel.zeit));
            upd_cursor = sqlcursor (sqltext);

            ins_quest ((char *)   &leer_bel.mdn,   SQLSHORT, 0);
            ins_quest ((char *)   &leer_bel.datum, SQLLONG, 0);
            ins_quest ((char *)   leer_bel.zeit,  SQLCHAR, sizeof (leer_bel.zeit));
            test_upd_cursor = sqlcursor ("select a from leer_bel "
                                  "where mdn = ? "
				  "and datum = ? "
 				  "and zeit = ?");
            ins_quest ((char *)   &leer_bel.mdn,   SQLSHORT, 0);
            ins_quest ((char *)   &leer_bel.datum, SQLLONG, 0);
            ins_quest ((char *)   leer_bel.zeit,  SQLCHAR, sizeof (leer_bel.zeit));
            del_cursor = sqlcursor ("delete from leer_bel "
                                  "where mdn = ? "
				  "and datum = ? "
 				  "and zeit = ?");
    ins_quest ((char *) &leer_bel.mdn,1,0);
    ins_quest ((char *) &leer_bel.kun_fil,1,0);
    ins_quest ((char *) &leer_bel.nr,2,0);
    ins_quest ((char *) leer_bel.blg_typ,0,2);
    ins_quest ((char *) &leer_bel.a,3,0);
    ins_quest ((char *) &leer_bel.anzahl,2,0);
    ins_quest ((char *) &leer_bel.bestand,2,0);
    ins_quest ((char *) &leer_bel.datum,2,0);
    ins_quest ((char *) leer_bel.zeit,0,9);
    ins_quest ((char *) leer_bel.pers_nam,0,17);
    ins_quest ((char *) &leer_bel.kun,2,0);
    ins_quest ((char *) &leer_bel.hkun,2,0);
    ins_quest ((char *) &leer_bel.delstatus,1,0);
            ins_cursor = sqlcursor ("insert into leer_bel ("
"mdn,  kun_fil,  nr,  blg_typ,  a,  anzahl,  bestand,  datum,  zeit,  pers_nam,  kun,  hkun,  "
"delstatus) "

#line 52 "leer_bel.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?)"); 

#line 54 "leer_bel.rpp"
}

int LEER_BEL_CLASS::dbinsert ()
{
	if (ins_cursor == -1)
        {
               prepare ();
        }
        return sqlexecute (ins_cursor);
}

BOOL LEER_BEL_CLASS::operator== (LEER_BEL& leer_bel)
{
/*
            if (this->leer_bel.mdn        != leer_bel.mdn) return FALSE;  
            if (this->leer_bel.kun_fil    != leer_bel.kun_fil) return FALSE;  
            if (this->leer_bel.kun        != leer_bel.kun) return FALSE;  
            if (this->leer_bel.a          != leer_bel.a) return FALSE;  
            return TRUE;
*/
            return FALSE;
} 
