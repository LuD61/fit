//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by 53100.rc
//
#define IDD_LLPRINTER                   101
#define IDB_PRIOR_ROW                   102
#define IDB_NEXT_ROW                    103
#define IDB_FIRST_ROW                   104
#define IDB_LAST_ROW                    105
#define IDB_FIRST_ROW_MASK              106
#define IDB_PRIOR_ROW_MASK              107
#define IDB_NEXT_ROW_MASK               108
#define IDB_LAST_ROW_MASK               109
#define IDD_MSG_DIALOG                  110
#define CallT1                          850
#define CallT2                          851
#define CallT3                          852
#define IDC_LS                          1000
#define IDC_RECH                        1001
#define IDC_MESSAGE                     1002
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        111
#define _APS_NEXT_COMMAND_VALUE         40003
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
