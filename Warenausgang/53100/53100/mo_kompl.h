#ifndef _MO_KOMPL_DEF
#define _MO_KOMPL_DEF
#include "mo_progcfg.h"
#include "Text.h"
#include "resrc1.h"


extern BOOL StapelTour;

class KompAuf
{
        private :
            short mdntab[1000]; 
            short filtab[1000];
            long auftab[1000];
            int aufanz;
            short mdntablock[1000]; 
            short filtablock[1000];
            long auftablock[1000];
            int aufanzlock;
            long drklstab [1000];
            int drklsanz;
            int auf_a_sort; 
			char drformat [20];
			BOOL IsGroup;
			long max_ls;
			int def_start;
			int def_nr;
			static int lllsfo;
            static PROG_CFG *ProgCfg; 
            static char DebugFile [];
        public :
			enum
			{
				PosAccept,
				PosDelete,
				PosAuto,
				PosDlgCancel,
			};

			HANDLE DlgPlugin;
			int (*GetPosMode)(int PosMode); 
			static long BarKunde;
			static HWND mainWnd;
			static BOOL RosiSqlDebug;
			static BOOL cfg_TestLiefMe0;
            static BOOL KasseDirect;
			static BOOL ll_ls_par;
			static BOOL ll_rech_par;
			static BOOL SaveDefault;
			static BOOL KompOPDefault;
            KompAuf () : auf_a_sort (0), IsGroup (FALSE), aufanz (0), aufanzlock (0)
            {
				strcpy (drformat, "53100"); 
                ProgCfg = NULL;
				def_nr = 0;
				DlgPlugin = NULL;
				GetPosMode = NULL;
				lllsfo = 1;            
			}

            void SetProgCfg (PROG_CFG *ProgCfg)
            {
                this->ProgCfg = ProgCfg;
            }

			void SetDrFormat (char *format)
			{
				strcpy (drformat, format);
			}

			long Getmax_ls (void)
			{
				return max_ls;
			}

			void SetLllsfo (int lllsfo)
			{
				this->lllsfo = lllsfo;
			}

            void SetAbt (BOOL);
            void SetDrkKun (BOOL);
            void SetLuKun (BOOL);
            void SetKompDefault (int);
            void SetDrkLsSperr (BOOL);
            void SetLiefMePar (int);
            void SetLog (int);
            void SetKlstPar (int);
            void SetLutzMdnPar (int);
            void SetSplitLs (int);
            void BearbKomplett (void);
            int  SetKomplett (HWND);
            long GenLsNr (short, short);
            long GenLsNr0 (short, short);
            long GenAufNr (short, short);
            long GenAufNr0 (short, short);
            void AufktoLsk (void);
            void AufktoAufk (void);
            void aufpttolspt (void);
            long GenLspTxt0 (void);
			BOOL TxtNrExist (long);
            long GenLspTxt (void);
            void AufptoLsp (long);
            double GetGew (void);
            double GetAufMeVgl(void);
            void TeilSmttoLs (void);
//            void TeilSmttoAuf (int);
            void ProdMdntoAuf (int);
            int Geteinz_ausw (void);
//            void AuftoLs (void);
            void ShowLocks (HWND);
            void ShowNoDrk (HWND hWnd);
			void InitAufanz (void);
			void KompParams (void);
            void AuftoLsGroup (char *);
            void Getauf_a_sort (void);
//            void AuftoPid (void);
//            void InitPid (void);
            void K_Liste_Direct ();
            void K_Liste_Fax (void);
            void K_Liste ();
            void K_ListeGroup (short, short, short, short,long, long,
							   char *, char *, short, short, long, long,
							   short, short, long, long);
			int FaxAuf (HWND, short, short, long);

            void K_ListeGroup0 (char *);
            int  SetFak_typ (HWND hWnd);
            void FreeLs (void);
            void LockLs (void);
            void TestFree (void);
            void K_ListeTS (void);
            void DrkLsTS (void);
            void AuftoLsTS (void);
            void BeldrAufruf (void);
//            void AufToSmtDrk (int, long *);
			long GetGruppe (void);
            BOOL BsdArtikel (double);
            void BucheBsd (double, double,  double);
            void SetBsdKz (int);
            void AngktoAufk (void);
            void AngptoAufp (long);
			void PrintLsGroup (char *, BOOL );
			void PrintLsGroupEx (char *, BOOL);
			void InitLeerLsh (void);
			void SetLsGruppe (void);
			void FreeLsGroup (char *);
			BOOL BeldrError (Text&, char *);
			void SetPlugins ();
			BOOL GetLiefMe0Mode ();
			BOOL TestLiefMe0 ();
			BOOL TestPrVk0 (BOOL TestLeergut);
			BOOL AnalysePosMode (int PosMode);
            static long GetLagerort (long, double);
            static void CreateDebugFile ();
			static void ChoisePrinter0 ();
			static void LLPrinter ();
};


class CPrinterDialog
{
protected :
	  static void *Instance;
	  virtual BOOL OnInitDialog (HWND);
	  int DlgReturn; 
public :
	  BOOL LsPrint;
	  BOOL RechPrint;
	  CPrinterDialog ();
	  int DoModal ();
 	  static BOOL CALLBACK DlgProcP (HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam); 

};
#endif
