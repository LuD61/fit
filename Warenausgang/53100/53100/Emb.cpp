#include <windows.h>
#include "Emb.h"


EmbZweig::EmbZweig ()
{
}

EmbZweig::~EmbZweig ()
{
}

void EmbZweig::SetEmb ()
{
   anz_emb    = a_emb.anz_emb;
   delstatus  = a_emb.delstatus;
   emb        = a_emb.emb;
   strcpy (emb_bz, a_emb.emb_bz);
   strcpy (emb_vk_kz, a_emb.emb_vk_kz);
   tara       = a_emb.tara;
   tara_proz  = a_emb.tara_proz;
   unt_emb    = a_emb.unt_emb ;
   a_pfa      = a_emb.a_pfa;
}


Emb::Emb () : A_EMB_CLASS ()
{
}

Emb::~Emb ()
{
	Embs.DestroyAll ();
	Embs.Init ();
}

void Emb::Init ()
{
	anz = 0;
	emb = 0.0;
	a   = 0.0;
	Embs.DestroyAll ();
    Embs.Init ();
}

int Emb::GetAnz ()
{
	return anz;
}

double Emb::GetEmb ()
{
	return emb;
}

double Emb::GetA ()
{
	return a;
}

int Emb::Read (double emb)
{
	Init ();
	a_emb.emb = emb;
	int dsqlstatus = dbreadfirst ();
	if (dsqlstatus != 0)
	{
		return dsqlstatus;
	}

	anz = a_emb.anz_emb;
	emb = a_emb.emb;
	a   = a_emb.unt_emb;
	EmbZweig *ez = new EmbZweig ();
	ez->SetEmb ();
	Embs.Add (ez);
    a_emb.emb = a_emb.unt_emb;
	while (dbreadfirst () == 0)
	{
        anz *= a_emb.anz_emb;
 	    a   = a_emb.unt_emb;
  	    EmbZweig *ez = new EmbZweig ();
	    ez->SetEmb ();
	    Embs.Add (ez);
		a_emb.emb = a_emb.unt_emb;
	}
    return 0;
}


