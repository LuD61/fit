#if !defined(AFX_FORM_H__9F1689B8_5649_11D2_AE4F_0000C099E22F__INCLUDED_)
#define AFX_FORM_H__9F1689B8_5649_11D2_AE4F_0000C099E22F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// Form.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CForm form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CAllControlsSheet;

class CForm : public CFormView
{
protected:
	CForm();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CForm)

// Form Data
public:
	//{{AFX_DATA(CForm)
	enum { IDD = IDD_FORM };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	CAllControlsSheet *m_CAllControlsSheet;

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CForm)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CForm();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CForm)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORM_H__9F1689B8_5649_11D2_AE4F_0000C099E22F__INCLUDED_)
