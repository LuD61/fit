#include <windows.h>
#include "searchptab.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


int SEARCHPTAB::idx;
long SEARCHPTAB::anz;
CHQEX *SEARCHPTAB::Query = NULL;
DB_CLASS SEARCHPTAB::DbClass; 
HINSTANCE SEARCHPTAB::hMainInst;
HWND SEARCHPTAB::hMainWindow;
HWND SEARCHPTAB::awin;
char *SEARCHPTAB::Item = NULL;
BOOL SEARCHPTAB::NumVal = FALSE;

SEARCHPTAB::SEARCHPTAB (char *ptitem)
{
          SearchPos = 8;
          OKPos = 9;
          CAPos = 10;
          Item = ptitem;
}

void SEARCHPTAB::SetItem (char *ptitem)
{
          Item = ptitem;
}

int SEARCHPTAB::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHPTAB::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      char ptlfnr[5];
      char ptwert[5];
      char ptbez [37];


	  anz = 0;
      if (Item == NULL) return 0;
      clipped (Item);
      if (strcmp (Item, " ") == 0) Item[0] = 0;
      if (Item[0] == 0) return 0;

      sprintf (buffer, "select ptlfnr, ptwert, ptbez "
	 			       "from ptabn "
                       "where ptitem = \"%s\" "
                       "order by ptlfnr", Item);
      DbClass.sqlout ((char *) ptlfnr, 0, 4);
      DbClass.sqlout ((char *) ptwert, 0, 4);
      DbClass.sqlout ((char *) ptbez, 0, 37);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
//      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
//  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
          if (NumVal)
          {
 	             sprintf (buffer, "    %4d  |%-36s|", atoi (ptwert), ptbez);
          }
          else
          {
 	             sprintf (buffer, "    %-4s  |%-36s|", ptwert, ptbez);
          }
	      Query->InsertRecord (buffer);
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
// 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHPTAB::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHPTAB::GetKey (char * key)
{
      int anz;
      static char kvalue [512];
      char *kv = kvalue;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 1) return FALSE;
      strcpy (kv, wort[0]);
      while (*kv && (*kv <= ' ')) kv ++;
      clipped (kv);
      strcpy (key, kv);
      return TRUE;
}
BOOL SEARCHPTAB::GetKeyErw (char * key,short pos)
{
      int anz;
      static char kvalue [512];
      char *kv = kvalue;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 1) return FALSE;
      strcpy (kv, wort[pos]);
      while (*kv && (*kv <= ' ')) kv ++;
      clipped (kv);
      strcpy (key, kv);
      return TRUE;
}


void SEARCHPTAB::Search (void)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 48;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      if (NumVal)
      {
            sprintf (buffer, "%d %s",
                                "%s",
                               "%s");
      }
      else
      {
            sprintf (buffer, "%s %s",
                                "%s",
                               "%s");
      }
      Query->RowAttr (buffer);
	  sprintf (buffer, "    %6s", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "4;4 11;36");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, "    %4s  %-36s", "Wert", "Bezeichnung"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst ("");
      Query->SetSortRow (0, TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

