#ifndef _VINFO_DEF
#define _VINFO_DEF
#include <windows.h>
#include "cmask.h"


class VINFO
{
          private :
                  static DWORD BuOk;
                  static mfont TextFont1;
                  static mfont TextFont2;
                  static mfont TextFont;
                  static mfont BuFont;
                  static char *text[]; 
                  static mfont *textfont[]; 

                  static CFIELD Text1;
                  static CFIELD Text2;
                  static CFIELD Text3;
                  static CFIELD Text4;
                  static CFIELD Text5;
                  static CFIELD Text6;
                  static CFIELD *TextField[];

                  static CFORM TextForm;  		  

			MESSCLASS Msg;
			int lines;
          public :
			VINFO () :lines (5)
               {
               }
               void VInfo (HANDLE, HWND, char **);
               void VInfoF (HANDLE, HWND, char **);
};
#endif
