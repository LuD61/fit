struct DBITEM {
   char      name[19];
   char      bezgkurz[4];
   char      bezkurz[11];
   char      bezlang[19];
   char      itype[16];
   char      iformat[17];
   char      bereich[49];
   char      vorbeleg[16];
   short     hilfnr;
   short     commentnr;
   char      bemerkung1[51];
   char      bemerkung2[51];
   char      bemerkung3[51];
   char      bemerkung4[51];
   short     delstatus;
   long      akv;
   long      bearb;
   char      pers_nam[9];
};

extern struct DBITEM _item;

class ITEM_CLASS
{
       private :
            short cursor_item;

       public:
           ITEM_CLASS ()
           {
                     cursor_item = -1;
           }
           int lese_item (char *);
           int lese_item (void);
           void erase_item (void);
};
