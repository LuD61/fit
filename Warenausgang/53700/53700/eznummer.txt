		sprintf (buffer, "Command=S%cArtikel=%.0lf%c"
			                         "Kunde=%ld%c"
			                         "Branche=%s%c"
			                         "Preis=%.4lf%c"
							         "Artikeltyp=%hd%c"
									 "Tour= %ld%c"
									 "Charge=%s%c"
									 "Auftragsmenge=%.0lf%c"
									 "Auftragsmengeneinheit=%d%c"
									 "Datum1=%s%c"
//									 "Datum2=%s%c"
									 "Gewicht=%.3lf%c"
									 "Tara=%.3lf%c"
									 "Haltbarkeitstage=%d%c"
									 "EZNummer=%s%c"
									 ,
//									 "Kartonmenge=%hd%c"
//									 "Kartonmengeneinheit=1",
							DdeSep,
			                _a_bas.a, DdeSep, 
			                atol (aufps.kun), DdeSep, 
			                aufps.kun_bran2, DdeSep, 
							ratod (aufps.auf_lad_pr), DdeSep,
							_a_bas.a_typ, DdeSep,
							lsk.tou_nr,DdeSep,
							aufps.ls_charge,DdeSep,
//							ratod (aufps.auf_me),DdeSep,
							auf_me_kun,DdeSep,
							atoi (aufps.me_einh_kun),DdeSep,
							aufps.hbk_date, DdeSep,
//							aufps.hbk_date2, DdeSep,
							_a_bas.a_gew, DdeSep,
							a_hndw.tara, DdeSep,
							_a_bas.hbk_ztr, DdeSep,
							EznGes, DdeSep
 


        if (paz_direkt == 4)
        {
			          if (mfifo == NULL)
					  {
                          mfifo = new Mfifo (100, "bws_asc.pip");
						  if (mfifo == NULL)
						  {
							  disp_messG ("Fehler beim Generieren von mfifo", 2);
                              autochange = FALSE;
							  return (-1);
						  }
					  }

			          if (ba_active == 0)
					  {

	                      mfifo->InitMsg ();
						  sprintf (buffer, "bws_ascw -wp -we %s", mod4params);
						  ba_pid = ProcExecPid (buffer,  SW_SHOWNORMAL, -1, 0, -1, 0);
						  if (ba_pid == NULL)
						  {
                              autochange = FALSE;
							  disp_messG ("Fehler beim Start von bws_ascw", 2);
							  return (-1);
						  }

						  ba_active = TRUE;
					  }
					  else
					  {
						  if (TestPid (buffer) == -1)
						  {
                              autochange = FALSE;
							  disp_messG ("Fehler beim Start von bws_ascw", 2);
							  return (-1);
						  }
					  }


                      BuInactiv ();
                      sprintf (buffer, "pr_paz -dokt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, lieferdatum, auf_me_kun,
                          aufps.ls_lad_dm, kunme_einh);
                      mfifo->InitMsg ();
	                  mfifo->WriteMsg (buffer);

 	                  if (mfifo->WaitMsg (ba_pid) == FALSE)
					  {
                          print_messG (2,
                              "Fehler bei bws_ascw");
						  CloseHandle (ba_pid);
						  ba_active = 0;
						  ba_pid = NULL;
                          BuActiv ();
                          autochange = FALSE;
                          return (-1);
					  }


                      sprintf (buffer, "pr_paz -dokt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, atol (AktTour));
	                  mfifo->WriteMsg (buffer);


 	                  if (mfifo->WaitMsg (ba_pid) == FALSE)
					  {
                          print_messG (2,
                              "Fehler bei bws_ascw");
						  CloseHandle (ba_pid);
						  ba_active = 0;
						  ba_pid = NULL;
                          BuActiv ();
                          autochange = FALSE;
                          return (-1);
					  }


					  if (mfifo->TestMsgError ())
					  {
                          print_messG (2,
                              "Fehler beim Aufbau der Auszeichnerdaten");
                          BuActiv ();
                          autochange = FALSE;
                          return (-1);
					  }

		}
        else if (paz_direkt == 3)
        {
                      sprintf (buffer, "pr_paz -dokt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx3.if %ld %ld %s %.0lf %s %hd %ld %s %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, lieferdatum, auf_me_kun,
                          aufps.ls_lad_dm, kunme_einh , atol (AktTour), aufps.ls_charge,
						  EznGes);

                      BuInactiv ();
                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL,
                                                 -1, 0, -1, 0);
/*
                      if (ex != 0l)
                      {
                          print_messG (2,
                              "Fehler beim Aufbau der Auszeichnerdaten");
                          BuActiv ();
                          autochange = FALSE;
                          return (-1);
                      }
*/
        }
        else if (paz_direkt == 2)
        {


                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.ls_lad_dm);

                      sprintf (buffer, "pr_paz -dokt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, lieferdatum, auf_me_kun,
                          aufps.ls_lad_dm, kunme_einh);

                      BuInactiv ();
                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL,
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2,
                              "Fehler beim Aufbau der Basisdaten");
                          BuActiv ();
                          autochange = FALSE;
                          return (-1);
                      }

                      sprintf (buffer, "pr_paz -dokt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, atol (AktTour));

                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL,
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2,
                              "Fehler beim Aufbau der Texte");
                          BuActiv ();
                          autochange = FALSE;
                          return (-1);
                      }
       }
        else if (paz_direkt == 1)
        {
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, lieferdatum, auf_me_kun,
                          aufps.ls_lad_dm, kunme_einh);


                      BuInactiv ();
                      CreateBatch (buffer);

                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, atol (AktTour));

                     RunBatch (buffer);
        }
        else
        {
/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, lieferdatum, auf_me_kun,
                          aufps.ls_lad_dm, kunme_einh );

                      BuInactiv ();
                      CreateMessage (WiegWindow, "Die Daten werden vorbereitet");
                      ex = ProcWaitExec (buffer, SW_HIDE,
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage ();
                          AktivWindow = WiegWindow;
                          print_messG (2,
                              "Fehler beim Aufbau der Basisdaten");
                          BuActiv ();
                          autochange = FALSE;
                          return (-1);
                      }
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, atol (AktTour));

                      ex = ProcWaitExec (buffer, SW_HIDE,
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage ();
                          AktivWindow = WiegWindow;
                          print_messG (2,
                              "Fehler beim Aufbau der Texte");
                          BuActiv ();
                          autochange = FALSE;
                          return (-1);
                      }
                      DestroyMessage ();
                      AktivWindow = WiegWindow;
       }
   