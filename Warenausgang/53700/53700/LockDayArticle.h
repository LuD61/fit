// LockDayArticle.h: Schnittstelle f�r die Klasse LockDayArticle.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOCKDAYARTICLE_H__675A7044_544D_4AF5_9121_E5B0FB20102F__INCLUDED_)
#define AFX_LOCKDAYARTICLE_H__675A7044_544D_4AF5_9121_E5B0FB20102F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>
#include "wmask.h"
#include "dbclass.h"
#include "lockprot.h"

class LockDayArticle  
{
private:
	short m_Mdn;
	short m_Fil;
	double m_Article;
	long   m_KommDate;
	int m_TestCursor;
	int m_LockCursor;
	int m_UnlockCursor;
	short m_PosStat;
	LOCKPROT_CLASS Lockprot;
	DB_CLASS DbClass;
public:
	LockDayArticle();
	void SetKeys (short mdn, short fil, double article, long kommDate);
	BOOL IsLocked ();
	BOOL Lock ();
	BOOL Unlock ();
	BOOL UnlockManual (char *pers, char *prog);
	void CreateLog ();
	virtual ~LockDayArticle();

};

#endif // !defined(AFX_LOCKDAYARTICLE_H__675A7044_544D_4AF5_9121_E5B0FB20102F__INCLUDED_)
