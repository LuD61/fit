#include <stdio.h>
#include "Integer.h"


CInteger::CInteger ()
{
	value = 0;
	sValue = NULL;
}

CInteger::CInteger (int value)
{
	this->value = value;
	sValue = new Text ();
    sValue->Format ("%d", value);
}

CInteger::CInteger (LPSTR sValue)
{
	this->value = atoi (sValue);
	this->sValue = new Text ();
	this->sValue = new Text ();
    this->sValue->Format ("%d", value);
}

CInteger::CInteger (Text& sValue)
{
	this->value = atoi (sValue.GetBuffer ());
	this->sValue = new Text ();
    this->sValue->Format ("%d", value);
}


CInteger::~CInteger ()
{
	if (sValue != NULL)
	{
		delete sValue;
	}
}

void CInteger::FromHex (LPSTR hValue)
{
	value = FromHexString (hValue);
	if (sValue != NULL)
	{
		delete sValue;
		sValue = new Text ();
	}
    sValue->Format ("%d", value);
}

void CInteger::FromHex (Text& hValue)
{
	value = FromHexString (hValue.GetBuffer ());
	if (sValue != NULL)
	{
		delete sValue;
		sValue = new Text ();
	}
    sValue->Format ("%d", value);
}

int CInteger::IntValue ()
{
	return value;
}

LPSTR CInteger::ToString ()
{
	if (sValue == NULL)
	{
		sValue = new Text ();
	}
    sValue->Format ("%d", value);
	return sValue->GetBuffer ();
}


int CInteger::FromHexString (LPSTR hValue)
{
	int v;

	Text h = hValue;
    h.MakeUpper (); 
	sscanf (h.GetBuffer (), "%X", &v);
	return v;
}

int CInteger::FromHexString (Text& hValue)
{
	return FromHexString (hValue.GetBuffer ());
}

CInteger& CInteger::operator= (int v)
{
	value = v;
	return *this;
}

CInteger& CInteger::operator= (Text& v)
{
	value = atoi (v.GetBuffer ());
	return *this;
}

BOOL CInteger::operator== (CInteger& IntValue)
{
	return (value == IntValue.IntValue ());
}