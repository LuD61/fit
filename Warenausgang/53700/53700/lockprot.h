#ifndef _LOCKPROT_DEF
#define _LOCKPROT_DEF

#include "dbclass.h"

struct LOCKPROT {
   char      pers[13];
   char      computer[31];
   long      tag;
   char      zeit[9];
   char      prog[21];
   char      text[41];
};
extern struct LOCKPROT lockprot, lockprot_null;

#line 7 "lockprot.rh"

class LOCKPROT_CLASS : public DB_CLASS 
{
       private :
               virtual void prepare (void);
       public :
               virtual void dbupdate (); 
};
#endif
