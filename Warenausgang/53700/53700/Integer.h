#ifndef _INTEGER_DEF
#define _INTEGER_DEF
#include "Text.h"

class CInteger
{
	public:
		int value;
		Text *sValue;
		CInteger ();
		CInteger (int);
		CInteger (LPSTR);
		CInteger (Text&);
		~CInteger ();
		int IntValue ();
		LPSTR ToString ();
		LPSTR ToHexString ();
		void FromHex (LPSTR);
		void FromHex (Text&);
        CInteger& operator= (int);
        CInteger& operator= (Text&);
		BOOL operator== (CInteger&);
		static int FromHexString (LPSTR);
		static int FromHexString (Text&);
};

#endif