#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#ifndef CONSOLE
#include "wmask.h"
#include "mo_meld.h"
#include "dbfunc.h"
#endif
#include "mo_curso.h"
#include "strfkt.h"
#include "a_bas.h"

static short cursor_a = -1;
static short cursor = -1;
static short cursor_a_ausw = -1;
struct A_BAS _a_bas;

static char sqlstring [0x1000];

void FillArtUb  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%13s %-24s %s",
                          "Artikel", "Bezeichnung1", "Bezeichnung2");
}

static void FillArtValues  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%13.0lf %-24.24s %s", _a_bas.a,
                          _a_bas.a_bz1, _a_bas.a_bz2);
}

static void prepare_a (char *order)
{
    out_quest ((char *) &_a_bas.a,3,0);
    out_quest ((char *) &_a_bas.mdn,1,0);
    out_quest ((char *) &_a_bas.fil,1,0);
    out_quest ((char *) _a_bas.a_bz1,0,25);
    out_quest ((char *) _a_bas.a_bz2,0,25);
    out_quest ((char *) &_a_bas.a_gew,3,0);
    out_quest ((char *) &_a_bas.a_typ,1,0);
    out_quest ((char *) &_a_bas.a_typ2,1,0);
    out_quest ((char *) &_a_bas.abt,1,0);
    out_quest ((char *) &_a_bas.ag,1,0);
    out_quest ((char *) _a_bas.best_auto,0,2);
    out_quest ((char *) _a_bas.bsd_kz,0,2);
    out_quest ((char *) _a_bas.cp_aufschl,0,2);
    out_quest ((char *) &_a_bas.delstatus,1,0);
    out_quest ((char *) &_a_bas.dr_folge,1,0);
    out_quest ((char *) &_a_bas.erl_kto,2,0);
    out_quest ((char *) _a_bas.hbk_kz,0,2);
    out_quest ((char *) &_a_bas.hbk_ztr,1,0);
    out_quest ((char *) _a_bas.hnd_gew,0,2);
    out_quest ((char *) &_a_bas.hwg,1,0);
    out_quest ((char *) _a_bas.kost_kz,0,3);
    out_quest ((char *) &_a_bas.me_einh,1,0);
    out_quest ((char *) _a_bas.modif,0,2);
    out_quest ((char *) &_a_bas.mwst,1,0);
    out_quest ((char *) &_a_bas.plak_div,1,0);
    out_quest ((char *) _a_bas.stk_lst_kz,0,2);
    out_quest ((char *) &_a_bas.sw,3,0);
    out_quest ((char *) &_a_bas.teil_smt,1,0);
    out_quest ((char *) &_a_bas.we_kto,2,0);
    out_quest ((char *) &_a_bas.wg,1,0);
    out_quest ((char *) &_a_bas.zu_stoff,1,0);
    out_quest ((char *) &_a_bas.akv,2,0);
    out_quest ((char *) &_a_bas.bearb,2,0);
    out_quest ((char *) _a_bas.pers_nam,0,9);
    out_quest ((char *) &_a_bas.prod_zeit,3,0);
    out_quest ((char *) _a_bas.pers_rab_kz,0,2);
    out_quest ((char *) &_a_bas.gn_pkt_gbr,3,0);
    out_quest ((char *) &_a_bas.kost_st,2,0);
    out_quest ((char *) _a_bas.sw_pr_kz,0,2);
    out_quest ((char *) &_a_bas.kost_tr,2,0);
    out_quest ((char *) &_a_bas.a_grund,3,0);
    out_quest ((char *) &_a_bas.kost_st2,2,0);
    out_quest ((char *) &_a_bas.we_kto2,2,0);
    out_quest ((char *) &_a_bas.charg_hand,2,0);
    out_quest ((char *) &_a_bas.intra_stat,2,0);
    out_quest ((char *) _a_bas.qual_kng,0,5);
    if (order)
    {
                       cursor = prepare_sql
                          ("select a_bas.a,  a_bas.mdn,  a_bas.fil,  "
"a_bas.a_bz1,  a_bas.a_bz2,  a_bas.a_gew,  a_bas.a_typ,  a_bas.a_typ2,  "
"a_bas.abt,  a_bas.ag,  a_bas.best_auto,  a_bas.bsd_kz,  a_bas.cp_aufschl,  "
"a_bas.delstatus,  a_bas.dr_folge,  a_bas.erl_kto,  a_bas.hbk_kz,  "
"a_bas.hbk_ztr,  a_bas.hnd_gew,  a_bas.hwg,  a_bas.kost_kz,  "
"a_bas.me_einh,  a_bas.modif,  a_bas.mwst,  a_bas.plak_div,  "
"a_bas.stk_lst_kz,  a_bas.sw,  a_bas.teil_smt,  a_bas.we_kto,  a_bas.wg,  "
"a_bas.zu_stoff,  a_bas.akv,  a_bas.bearb,  a_bas.pers_nam,  "
"a_bas.prod_zeit,  a_bas.pers_rab_kz,  a_bas.gn_pkt_gbr,  "
"a_bas.kost_st,  a_bas.sw_pr_kz,  a_bas.kost_tr,  a_bas.a_grund,  "
"a_bas.kost_st2,  a_bas.we_kto2, a_bas.charg_hand, a_bas.intra_stat, "
"a_bas.qual_kng from a_bas "
                          "where a > 0 %s", order);
    }
    else
    {
                       cursor = prepare_sql
                          ("select a_bas.a,  a_bas.mdn,  a_bas.fil,  "
"a_bas.a_bz1,  a_bas.a_bz2,  a_bas.a_gew,  a_bas.a_typ,  a_bas.a_typ2,  "
"a_bas.abt,  a_bas.ag,  a_bas.best_auto,  a_bas.bsd_kz,  a_bas.cp_aufschl,  "
"a_bas.delstatus,  a_bas.dr_folge,  a_bas.erl_kto,  a_bas.hbk_kz,  "
"a_bas.hbk_ztr,  a_bas.hnd_gew,  a_bas.hwg,  a_bas.kost_kz,  "
"a_bas.me_einh,  a_bas.modif,  a_bas.mwst,  a_bas.plak_div,  "
"a_bas.stk_lst_kz,  a_bas.sw,  a_bas.teil_smt,  a_bas.we_kto,  a_bas.wg,  "
"a_bas.zu_stoff,  a_bas.akv,  a_bas.bearb,  a_bas.pers_nam,  "
"a_bas.prod_zeit,  a_bas.pers_rab_kz,  a_bas.gn_pkt_gbr,  "
"a_bas.kost_st,  a_bas.sw_pr_kz,  a_bas.kost_tr,  a_bas.a_grund,  "
"a_bas.kost_st2,  a_bas.we_kto2, a_bas.charg_hand, a_bas.intra_stat, " 
"a_bas.qual_kng from a_bas "

                          "where a > 0");
    }
}

static void prepare_a_bas (void)
{
    ins_quest ((char *) &_a_bas.a,3,0);
    out_quest ((char *) &_a_bas.a,3,0);
    out_quest ((char *) &_a_bas.mdn,1,0);
    out_quest ((char *) &_a_bas.fil,1,0);
    out_quest ((char *) _a_bas.a_bz1,0,25);
    out_quest ((char *) _a_bas.a_bz2,0,25);
    out_quest ((char *) &_a_bas.a_gew,3,0);
    out_quest ((char *) &_a_bas.a_typ,1,0);
    out_quest ((char *) &_a_bas.a_typ2,1,0);
    out_quest ((char *) &_a_bas.abt,1,0);
    out_quest ((char *) &_a_bas.ag,1,0);
    out_quest ((char *) _a_bas.best_auto,0,2);
    out_quest ((char *) _a_bas.bsd_kz,0,2);
    out_quest ((char *) _a_bas.cp_aufschl,0,2);
    out_quest ((char *) &_a_bas.delstatus,1,0);
    out_quest ((char *) &_a_bas.dr_folge,1,0);
    out_quest ((char *) &_a_bas.erl_kto,2,0);
    out_quest ((char *) _a_bas.hbk_kz,0,2);
    out_quest ((char *) &_a_bas.hbk_ztr,1,0);
    out_quest ((char *) _a_bas.hnd_gew,0,2);
    out_quest ((char *) &_a_bas.hwg,1,0);
    out_quest ((char *) _a_bas.kost_kz,0,3);
    out_quest ((char *) &_a_bas.me_einh,1,0);
    out_quest ((char *) _a_bas.modif,0,2);
    out_quest ((char *) &_a_bas.mwst,1,0);
    out_quest ((char *) &_a_bas.plak_div,1,0);
    out_quest ((char *) _a_bas.stk_lst_kz,0,2);
    out_quest ((char *) &_a_bas.sw,3,0);
    out_quest ((char *) &_a_bas.teil_smt,1,0);
    out_quest ((char *) &_a_bas.we_kto,2,0);
    out_quest ((char *) &_a_bas.wg,1,0);
    out_quest ((char *) &_a_bas.zu_stoff,1,0);
    out_quest ((char *) &_a_bas.akv,2,0);
    out_quest ((char *) &_a_bas.bearb,2,0);
    out_quest ((char *) _a_bas.pers_nam,0,9);
    out_quest ((char *) &_a_bas.prod_zeit,3,0);
    out_quest ((char *) _a_bas.pers_rab_kz,0,2);
    out_quest ((char *) &_a_bas.gn_pkt_gbr,3,0);
    out_quest ((char *) &_a_bas.kost_st,2,0);
    out_quest ((char *) _a_bas.sw_pr_kz,0,2);
    out_quest ((char *) &_a_bas.kost_tr,2,0);
    out_quest ((char *) &_a_bas.a_grund,3,0);
    out_quest ((char *) &_a_bas.kost_st2,2,0);
    out_quest ((char *) &_a_bas.we_kto2,2,0);
    out_quest ((char *) &_a_bas.charg_hand,2,0);
    out_quest ((char *) &_a_bas.intra_stat,2,0);
    out_quest ((char *) _a_bas.qual_kng,0,5);
    cursor_a = prepare_sql
                          ("select a_bas.a,  a_bas.mdn,  a_bas.fil,  "
"a_bas.a_bz1,  a_bas.a_bz2,  a_bas.a_gew,  a_bas.a_typ,  a_bas.a_typ2,  "
"a_bas.abt,  a_bas.ag,  a_bas.best_auto,  a_bas.bsd_kz,  a_bas.cp_aufschl,  "
"a_bas.delstatus,  a_bas.dr_folge,  a_bas.erl_kto,  a_bas.hbk_kz,  "
"a_bas.hbk_ztr,  a_bas.hnd_gew,  a_bas.hwg,  a_bas.kost_kz,  "
"a_bas.me_einh,  a_bas.modif,  a_bas.mwst,  a_bas.plak_div,  "
"a_bas.stk_lst_kz,  a_bas.sw,  a_bas.teil_smt,  a_bas.we_kto,  a_bas.wg,  "
"a_bas.zu_stoff,  a_bas.akv,  a_bas.bearb,  a_bas.pers_nam,  "
"a_bas.prod_zeit,  a_bas.pers_rab_kz,  a_bas.gn_pkt_gbr,  "
"a_bas.kost_st,  a_bas.sw_pr_kz,  a_bas.kost_tr,  a_bas.a_grund,  "
"a_bas.kost_st2,  a_bas.we_kto2, a_bas.charg_hand, a_bas.intra_stat, " 
"a_bas.qual_kng from a_bas "
                          "where a = ?");
   }

int lese_a (char *order)
/**
Tabelle a_bas lesen.
**/
{
         if (cursor == -1)
         {
                  prepare_a (order);
         }
         open_sql (cursor);
         fetch_sql (cursor);
         if (sqlstatus == 0)
         {
                return (0);
         }
         return (100);
}

int lese_a (void)
/**
Tabelle a_bas lesen.
**/
{
         if (cursor == -1)
         {
                return 100;
         }
         fetch_sql (cursor);
         if (sqlstatus == 0)
         {
                return (0);
         }
         return (100);
}



int lese_a_bas (double a)
/**
Tabelle a_bas lesen.
**/
{
         if (cursor_a == -1)
         {
                prepare_a_bas ();
/*
                ins_quest ((char *) &_a_bas.a, 3, 0);
                out_quest (_a_bas.a_bz1, 0, 24);
                out_quest (_a_bas.a_bz2, 0, 24);
                out_quest ((char *) &_a_bas.me_einh, 1, 0);
                out_quest ((char *) &_a_bas.a_typ, 1, 0);
                cursor_a = prepare_sql ("select a_bz1, a_bz2, me_einh, "
                                        "a_typ "
                                        "from a_bas "
                                        "where a = ?");
*/
         }
         _a_bas.a = a;
         open_sql (cursor_a);
         fetch_sql (cursor_a);
         if (sqlstatus == 0)
         {
                return (0);
         }
/*
#ifndef CONSOLE
         print_mess (0, "Artikel %.0lf nicht gefunden", _a_bas.a);
#endif
*/
         return (100);
}

#ifndef CONSOLE
int Auswahla_basQuery ()
/**
Auswahl ueber a_bas mit Qzery-String.
**/
{

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select a, a_bz1, a_bz2 from a_bas "
                            "where a > 0 and %s order by a", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                           "select a, a_bz1, a_bz2 from a_bas "
                           "where a > 0 order by a");
         }
         return Showa_bas (sqlstring);
}

int Auswahla_bas ()
/**
Auswahl ueber a_bas (alle Artikel.
**/
{
         strcpy (sqlstring ,
                 "select a, a_bz1, a_bz2 from a_bas "
                 "where a > 0 order by a");
         return Showa_bas (sqlstring);
}

int Showa_bas (char *sqlstring)
/**
Auswahl ueber Artikel anzeigen.
**/
{
         int i;

         out_quest ((char *) &_a_bas.a, 3, 0);
         out_quest (_a_bas.a_bz1, 0, 24);
         out_quest (_a_bas.a_bz2, 0, 24);

         cursor_a_ausw = prepare_scroll (sqlstring);
         if (sqlstatus)
         {
                     return (-1);
         }
         i = DbAuswahl (cursor_a_ausw, FillArtUb, FillArtValues);
         UpdateFkt ();
         SetFocus (current_form->mask[currentfield].feldid);

         SetCurrentField (currentfield);

         if (syskey == KEYESC || syskey == KEY5)
         {
                     close_sql (cursor_a_ausw);
                     return 0;
         }
         fetch_scroll (cursor_a_ausw, DBABSOLUTE, i + 1);
         close_sql (cursor_a_ausw);
         return 0;
}
#endif  // CONSOLE
