#ifndef _ABASIS_DEF
#define _ABASIS_DEF

#include <windows.h>
#include "wmask.h"
#include "dbclass.h"

struct BA
{
        short me_einh;
        double matob;
        double matmb;
        double sk;
        double sp;
        double rab;
        double rabwrt;
        double sprab;
};

extern struct BA BaValues, BaValues_null;

class ABASIS : public DB_CLASS
{
        public :
            ABASIS ()
            {
            }
 
            BOOL GetGrundPreis (short, short, double, double, long);
            BOOL GetHndw (short, short, double, double, long);
            BOOL GetEig (short, short, double, double, long);
			double GetKunRab (short, long);
			double GetKunRabSchl (short, char *);
			BOOL   GetPosRab (short, char *, double *);
			BOOL   GetSmtRab (short, char *, double *);
			BOOL   GetMeEinhRab (short, char *, double *);
			BOOL   GetSmtRabWrt (short, char *, double *);
			BOOL   GetMeEinhRabWrt (short, char *, double *);
};
#endif
