#include <stdio.h>
#include <string.h>
#include <windows.h>
#include "strfkt.h"
#include "mo_progcfg.h"

static char rec [1024];
static char dm  [512];

BOOL PROG_CFG::OpenCfg (void)
/**
CFG-Datei oeffnen.
**/
{
         char *etc;

         etc = getenv ("BWSETC");
         if (etc == NULL)
         {
                     etc = "c:\\user\\fit\\etc";
         }
 
         sprintf (dm, "%s\\%s.cfg", etc, progname);

         progfp = fopen (dm, "r");
         if (progfp == NULL) return FALSE;
         return TRUE;
}
           
void PROG_CFG::CloseCfg (void)
/**
CFG-Datei schliessen.
**/
{
         if (progfp)
         {
             fclose (progfp);
             progfp = NULL;
         }
}

BOOL PROG_CFG::GetCfgValue (char *progitem, char *progvalue)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         BOOL itOK; 

         if (progfp == NULL)
         {
                   if (OpenCfg () == FALSE) return FALSE;
         }

         itOK = FALSE;
         clipped (progitem);
         progvalue[0] = (char) 0;
		 fseek (progfp, 0l, 0);
         while (fgets (rec, 1023, progfp))
         {
                   if (rec[0] < (char) 0x30) continue;

				   cr_weg (rec);
                   anz = wsplit (rec, " ");
                   if (anz < 2) continue;
                   if (strcmp (wort[0], progitem) == 0)
                   {
                              strcpy (progvalue, wort[1]);
                              itOK = TRUE; 
                              break;
                   }
         }
         return itOK;
}
         
BOOL PROG_CFG::ReadCfgItem (char *itname, char *def, char *text,
                            char **values, char *help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         char *s;

         if (progfp == NULL)
         {
                   if (OpenCfg () == FALSE) return FALSE;
         }

         while (s = fgets (rec, 1023, progfp))
         {
                   if (rec[0] >= (char) 0x30) break;
         }

         if (s == NULL) return FALSE;

         anz = wsplit (rec, " ");
         if (anz < 3) return FALSE;

         itname = (char *) malloc (strlen (wort[0]) + 10);
         if (itname == NULL) return FALSE;
         strcpy (itname, wort[0]);
         def = (char *) malloc (strlen (wort[1]) + 10);
         if (def == NULL) return FALSE;
         strcpy (def, wort [1]);
         text = (char *) malloc (strlen (wort[2]) + 10);
         if (text == NULL) return FALSE;
         strcpy (text, wort [2]);
         return ReadCfgValues (values, help);
}
         
BOOL PROG_CFG::ReadCfgValues (char **values, char *help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         int i;
         char *s;
         BOOL InValue;

         if (progfp == NULL)
         {
                   return FALSE;
         }

         i = 0;
         InValue = FALSE;
         while (s = fgets (rec, 1023, progfp))
         {
                   cr_weg (rec);
                   if (rec[0] >= (char) 0x30) return FALSE;
                   anz = wsplit (rec, " ");
                   if (anz == 0) continue;
                   if (strcmp (wort[0], "$VALUES") == 0) 
                   {
                       InValue = TRUE;
                       continue;
                   }

                   if (strcmp (wort[0], "$HELP")   == 0) 
                   {
                        return ReadCfgHelp (help);
                   }
                   if (strcmp (wort[0], "$END")    == 0) return TRUE;
                   if (InValue)
                   {
                       values[i] = (char *) malloc (strlen (rec) + 10);
                       if (values[i] == NULL)
                       {
                           return FALSE;
                       }
                       strcpy (values[i], rec);
                       i ++;
                   }
         }
         return FALSE;
}
         
                          
BOOL PROG_CFG::ReadCfgHelp (char *help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         int i;
         char *s;
         BOOL InHelp;

         if (progfp == NULL)
         {
                   return FALSE;
         }

         i = 0;
         InHelp = FALSE;
         while (s = fgets (rec, 1023, progfp))
         {
                   cr_weg (rec);
                   if (rec[0] >= (char) 0x30) return FALSE;
                   anz = wsplit (rec, " ");
                   if (anz == 0) continue;
                   if (strcmp (wort[0], "$HELP") == 0) 
                   {
                       InHelp = TRUE;
                       continue;
                   }

                   if (strcmp (wort[0], "$END")    == 0) return TRUE;
                   if (InHelp)
                   {
                       help = (char *) malloc (strlen (rec) + 10);
                       if (help == NULL) return FALSE;
                       strcpy (help, rec);
                   }
         }
         return FALSE;
}
         
BOOL PROG_CFG::GetGlobDefault (char *env, char *wert)
/**
Wert aus bws_default holen.
**/
{
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\bws_defa", etc);
        fp = fopen (buffer, "r");
        if (fp == NULL) return FALSE;

        clipped (env);
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], env, strlen (env)) == 0)
                     {
                                 strcpy (wert, wort [2]);
                                 fclose (fp);
                                 return TRUE;
                     }
         }
         fclose (fp);
         return (FALSE);
}

BOOL PROG_CFG::GetGroupDefault (char *env, char *wert)
/**
Wert aus fitgroup.def holen.
**/
{
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\fitgroup.def", etc);
        fp = fopen (buffer, "r");
        if (fp == NULL) return FALSE;

        clipped (env);
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], env, strlen (env)) == 0)
                     {
                                 strcpy (wert, wort [2]);
                                 fclose (fp);
                                 return TRUE;
                     }
         }
         fclose (fp);
         return (FALSE);
}
                               

