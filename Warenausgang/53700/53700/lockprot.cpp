#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lockprot.h"

struct LOCKPROT lockprot, lockprot_null;


void LOCKPROT_CLASS::prepare ()
{

    out_quest ((char *) lockprot.pers,0,13);
    out_quest ((char *) lockprot.computer,0,31);
    out_quest ((char *) &lockprot.tag,2,0);
    out_quest ((char *) lockprot.zeit,0,9);
    out_quest ((char *) lockprot.prog,0,21);
    out_quest ((char *) lockprot.text,0,41);
            cursor = prepare_sql ("select lockprot.pers,  "
"lockprot.computer,  lockprot.tag,  lockprot.zeit,  lockprot.prog,  "
"lockprot.text from lockprot order by tag desc, zeit desc");

#line 24 "lockprot.rpp"



    ins_quest ((char *) lockprot.pers,0,13);
    ins_quest ((char *) lockprot.computer,0,31);
    ins_quest ((char *) &lockprot.tag,2,0);
    ins_quest ((char *) lockprot.zeit,0,9);
    ins_quest ((char *) lockprot.prog,0,21);
    ins_quest ((char *) lockprot.text,0,41);
            ins_cursor = prepare_sql ("insert into lockprot ("
"pers,  computer,  tag,  zeit,  prog,  text) "

#line 28 "lockprot.rpp"
                                      "values "
                                      "(?,?,?,?,?,?)"); 

#line 30 "lockprot.rpp"
}

void LOCKPROT_CLASS::dbupdate ()
{
	    if (ins_cursor == -1)
            {
                   prepare ();
            }
            execute_curs (ins_cursor); 
}

