#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include<winbase.h>
#include "fit.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_curs0.h"
#include "mo_menu.h"
#include "wmask.h"
#include "mo_meld.h"
#include "ls.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "kun.h"
#include "ptab.h"
#include "dbfunc.h"
#include "fnb.h"
#include "mdn.h"
#include "fil.h"
#include "a_pr.h"
#include "akt_krz.h"
#include "aktion.h"
#include "mo_a_pr.h"
#include "mo_preis.h"
#include "mo_einh.h"
#include "mo_geb.h"
#include "mo_numme.h"
#include "sys_peri.h"
#include "pht_wg.h"
#include "sys_par.h"
#include "mo_progcfg.h"
//#include "bws_asc.h"

#define AUFMAX 1000

#define ENTER 0
#define SUCHEN 1
#define AUSWAHL 2

#define BWS "BWS"
#define RSDIR "RSDIR"

#define sql_in ins_quest
#define sql_out out_quest

#define BAR 6
#define SOFORT 1

#define WIEGEN 9
#define AUSZEICHNEN 1
#define STOP 2
#define MINUS -1
#define PLUS 1

struct AUF_S
{
        char auf [10];
        char ls [10];
        char kun [10];
        char kun_krz1 [18];
        char lieferdat [12];
        char komm_dat [12];
        char ls_stat [3];
};

struct AUF_S aufs, aufs_null;
struct AUF_S aufsarr [AUFMAX];
int aufanz = 0;
int aufidx = 0;
static double akt_a = (double) 0.0;

struct AUFP_S
/** Auswahl ueber Auftraege                                 */
{
        char pnr [5];
        char s [2];
        char a [15];
        char a_kun [15];
        char a_bz1 [25];
        char a_bz2 [25];
        char a_me_einh [5];
        char auf_me [13];
        char lief_me [13];
        char auf_me_vgl [13];
        char tara [13];
        char auf_me_bz [10];
        char lief_me_bz [10];
        char auf_pr_vk [13];
        char auf_lad_pr [13];
        char basis_me_bz [7];
        char erf_kz [2];
        char rest [13];
        char ls_charge [21];
        char lsp_txt [10];
        char me_einh_kun [5]; 
        char me_einh [5]; 

        char sa_kz_sint [9]; 
        char prov_satz [11];
        char leer_pos [9];
        char hbk_date [11];
        char ls_vk_euro [13];
        char ls_vk_fremd [13];
        char ls_lad_euro [13];
        char ls_lad_fremd [13];
        char rab_satz [10];
        char me_einh_kun1 [5];
        char auf_me1 [14];
        char inh1 [14];
        char me_einh_kun2 [5];
        char auf_me2 [14];
        char inh2 [14];
        char me_einh_kun3 [5];
        char auf_me3 [14];
        char inh3 [14];


};

struct AUFP_S aufps, aufp_null;
struct AUFP_S aufparr [AUFMAX];
int aufpanz = 0;
int aufpidx = 0;
int aufpanz_upd = 0;
static char auf_me [12];
static int screenwidth  = 800;
static int screenheight = 580;

static int EnterTyp = ENTER;

static int mit_trans = 1;
static int paz_direkt = 0;
static long auszeichner;
static long StdWaage;
static double AktPrWieg[1000];
static double StornoWieg;
static int AktWiegPos = 0;

static char KomDatum [12];

static int testmode = 0;

static int menZeile = 0;
static int menSelect = 0;

static int menAufZeile = 0;
static int menAufSelect = 0;
static int cursor_lsk_auf_ausw;

//  Prototypen 

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL GebProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL FitLogoProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL UhrProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EntNumProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL PreisProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL ListKopfProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL PtFussProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL MsgProc(HWND,UINT, WPARAM,LPARAM);
void            fetch_std_geb (void);
void            update_kun_geb (void);
long            GetStdAuszeichner (void);
long            GetStdWaage (void);
int             AuswahlSysPeri (void);
int             AuswahlKopf (void);
void            LeseLsp (void) ;
void            IsDblClck (int);
BOOL            lsptoarr (int);
BOOL            lsptoarr0 (int);

int             ShowPtBox (HWND, char *, int, char *, int, form *,
                           form *, form *);
int IsWiegMessage (MSG *);
void InitAufKopf (void);

static     void SelectAktCtr (void);
void            InitFirstInstance(HANDLE);
BOOL            InitNewInstance(HANDLE, int);
int             ProcessMessages(void);
LONG            MenuJob(HWND, WPARAM, LPARAM);
void            AuftragKopf (int);
void            LiefKopf (void);
void            EnterNumBox (HWND, char *, char *, int, char *);
void            EnterPreisBox (HWND, int, int);
void            EnterListe (void);
void            ArtikelWiegen (HWND);
void            ArtikelHand (void);
void            WaageInit (void);
static int      DbAuswahl (short,void (*) (char *), void (*) (char *));
void            SetStorno (int (*) (void), int);
int             ShowFestTara (char *);
int             SetKomplett (void);
void            DisplayLiefKopf (void);
void            CloseLiefKopf (void);
void            LiefFocus (void);
int             print_messG (int, char *, ...);
void            disp_messG (char *, int);
int             abfragejnG (HWND, char *, char *);
int             AuswahlWieg (void);
void            fnbexec (int, char *[]);
BOOL            TestPosStatus (void);
BOOL            LsLeerGut (void);
void            AuftragListe (void);

BOOL            Lock_Lsp (void);
static int      la_zahl = 0;
static int      AktKopf = 0; 

//  Globale Variablen

HWND     hMainWindow;
HWND     hFitWindow = NULL;
HWND     BuDialog1;

HWND     BackWindow1;
HWND     BackWindow2;
HWND     BackWindow3;

HWND     LogoWindow;

HWND     FitWindow = NULL;
HWND     UhrWindow = NULL;
HWND     MessWindow = NULL;
HANDLE   hMainInst;
static   HWND eWindow = NULL; 
static   HWND InsWindow = NULL; 
static   HWND PtWindow = NULL; 
static   HWND eKopfWindow = NULL;
static   HWND eFussWindow = NULL;
static   HWND PtFussWindow = NULL;
static   HWND GebWindow = NULL;
static   HWND GebWindButton = NULL;

LPSTR    lpszMainTitle = "TOUCH Komissionierung ";
LPSTR    lpszClassName = "fit";
HBITMAP  fitbmp;
HBITMAP   pfeill;
HBITMAP   pfeilo;
HBITMAP   pfeilu;
HBITMAP   pfeilr;
HBITMAP   hbmOld;
HDC       hdc;
HDC       hdcMemory;
BITMAP    bm;
BITMAP    fitbm;
BITMAP    hfitbm;
HDC       fithdc;
int       procwait = 0;
int       IsFhs = 0;
BOOL      LockAuf = 1;
BOOL      AufLock = 0;

WA_PREISE WaPreis;

static char waadir [128] = {"C:\\waakt"};
static char ergebnis [128] = {"C:\\waakt\\ergebnis"};
static int WaaInit = 0;

static DWORD GebColor = RGB (0, 0, 255);

/* Globale Parameter fuer printlogo                   */

static long twait = 50;
static long twaitU = 500;
static int plus =  3;
static int xplus = 3;
static int ystretch = 0;
static int xstretch = 0;
static int logory = 0;
static int logorx = 1;
static int sign = 1;
static int signx = 1;

static short akt_mdn;
static short akt_fil;

static char fitpath [256];
static char rosipath [256];
static char progname [256];

struct MENUE scmenue;
struct MENUE leermenue;

static int ListeAktiv = 0;
static int PtListeAktiv = 0;
static int NumEnterAktiv = 0;
static int PreisAktiv = 0;
static int KopfEnterAktiv = 0;
static int WiegenAktiv = 0;
static int LiefEnterAktiv = 0;
static int KomplettAktiv = 0;
static int WorkModus = WIEGEN;
static HWND NumEnterWindow;
static HWND AufKopfWindow;
static HWND WiegWindow;


static int dsqlstatus;
class LS_CLASS ls_class;
class LSPT_CLASS lspt_class;
class KUN_CLASS kun_class;
class FIL_CLASS fil_class;
class ADR_CLASS adr_class;
class PTAB_CLASS ptab_class;
class MENUE_CLASS menue_class;
class EINH_CLASS einh_class;
class HNDW_CLASS hndw_class;
class GEB_CLASS geb_class;
class SYS_PERI_CLASS sys_peri_class;
class PHT_WG_CLASS pht_wg_class;
class SYS_PAR_CLASS sys_par_class;
static PROG_CFG ProgCfg ("53600");
static int tara_kz;
static int mhd_kz;

// HMENU hMenu;
static char *mentab[] = {scmenue.menue1,
                         scmenue.menue2,
                         scmenue.menue3,
                         scmenue.menue4,
                         scmenue.menue5,
                         scmenue.menue6,
                         scmenue.menue7,
                         scmenue.menue8,
                         scmenue.menue9,
                         scmenue.menue0};
static char *zeitab [] = {scmenue.zei1,
                          scmenue.zei2,
                          scmenue.zei3,
                          scmenue.zei4,
                          scmenue.zei5,
                          scmenue.zei6,
                          scmenue.zei7,
                          scmenue.zei8,
                          scmenue.zei9,
                          scmenue.zei0};

static char *mentab0[] = {menue.menue1,
                          menue.menue2,
                          menue.menue3,
                          menue.menue4,
                          menue.menue5,
                          menue.menue6,
                          menue.menue7,
                          menue.menue8,
                          menue.menue9,
                          menue.menue0};
static char *zeitab0[] = {menue.zei1,
                          menue.zei2,
                          menue.zei3,
                          menue.zei4,
                          menue.zei5,
                          menue.zei6,
                          menue.zei7,
                          menue.zei8,
                          menue.zei9,
                          menue.zei0};
                                  

static HMENU hMenu;

/* Masken fuer Logofenster                                 */

mfont fittextlogo = {"Arial", 1000, 0, 3, RGB (255, 0, 0),
                                               RGB (0, 255, 255),
                                               1,
                                               NULL};

mfont fittextlogo2 = {"Arial", 200, 0, 3, RGB (0, 0, 255),
                                      RGB (0, 255, 255),
                                      1,
                                      NULL};

mfont buttonfont = {"", 90, 0, 1,
                                       RGB (0, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont uhrfont     = {"Arial", 80, 0, 1,
                                       BLACKCOL,
                                       LTGRAYCOL,
                                       1,
                                       NULL};

field _fittext[] = {"     f i t", 0, 0, 50, -1, 0, "", DISPLAYONLY, 0, 0, 0};

form fittext = {1, 0, 0, _fittext, 0, 0, 0, 0, &fittextlogo};

field _fittext2[] = {"in Frische und Fleisch", 0, 0, 250, -1, 0, "",
                       DISPLAYONLY, 0, 0, 0};

form fittext2 = {1, 0, 0, _fittext2, 0, 0, 0, 0, &fittextlogo2};

static int menu_ebene = 0;
static int ohne_preis = 0;
static int immer_preis = 0;
static int ls_leer_zwang = 0;

struct FITMENU
{
           char menutext[80];
           char menuprog[256];
           int progtyp;
           int start_mode;
};

struct FITMENU fitmenu [11];

static char auftrag_nr [22];
static char lief_nr [22];
static char kun_nr [22];
static char kun_krz1 [17];
static char lieferdatum  [22]; 
static char komm_dat [22]; 
static char lieferzeit   [22];
static char ls_stat [22];
static char ls_stat_txt [27];
static char a [27];

static int menfunc1 (void);
static int menfunc2 (void);
static int menfunc3 (void);
static int menfunc4 (void);
static int menfunc5 (void);
static int menfunc6 (void);
static int menfunc7 (void);
static int menfunc8 (void);
static int menfunc9 (void);
static int menfunc0 (void);

static int func1 (void);
static int func2 (void);
static int func3 (void);
static int func4 (void);
static int func5 (void);
static int func6 (void);
static int func7 (void);
static int func8 (void);

ColButton Ende = {"Ende", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   TRUE};


ColButton Artikel  =   {"&Artikel", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton ArtChoise  =   {"&Artikelwahl", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               RGB (0, 255, 255),
                               BLUECOL,
                               TRUE};

ColButton Auftraege = {
                        "A&ftr�ge", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};

ColButton Suchen   =  {"&Suchen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                          TRUE};

ColButton Auswahl  =  {"Aus&wahl", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};


ColButton Geraet = {
                        "&Ger�t", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton KDatum    =  {"&Komm-Datum", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Warenausgang =  {
                        "Wa&renausgang", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton BestInv =  {
                        "&Bestand, Inventur", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Auswertungen =  {
                        "Auswer&tungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Anwendungen =  {
                        "Anwen&dungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Systemfunktionen =  {
                        "Systemf&unktionen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Benutzer =  {
                        "Ben&utzer", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};



ColButton Hilfe =  {
                        "Hilfe &?", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};


ColButton Hauptmenue =  {
                        "Hauptmen&�", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Back  =  {
                        "Zur�ck F5", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton BuOK = {"OK", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   TRUE};

ColButton BuKomplett =
                  {"komplett", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   REDCOL,
                   -1};


ColButton PfeilL       =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeill, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilO     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilo, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilU     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilu, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilR     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilr, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};


static int bsb  = 85;

static int bss0 = 73;
static int bss  = 77; 
static int bsz  = 40;
static int bsz0 = 36;
static int AktButton = 0;

field _MainButton[] = {
(char *) &Artikel,          128, bsz0, 115, -1, 0, "", COLBUTTON, 0,
                                                     menfunc1, 0,
(char *) &ArtChoise,        128, bsz0, 115 + 1 * bsz, -1, 0, "", COLBUTTON, 0,

                                                      menfunc2, 0,
/*
(char *) &Auftraege,        128, bsz0, 115 + 2 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc3, 0,
(char *) &Suchen,           128, bsz0, 115 + 2 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc4, 0,
(char *) &Auswahl,          128, bsz0, 115 + 3 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc5, 0,
(char *) &Liste,            128, bsz0, 115 + 4 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc2, 0,
*/
(char *) &KDatum,           128, bsz0, 115 + 2 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc7, 0,
(char *) &Geraet,           128, bsz0, 115 + 3 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc6, 0,
(char *) &Ende,             128, bsz0, 130 + 9 * bsz,  -1, 0, "", COLBUTTON, 0,
                                                          menfunc0, 0};

form MainButton = {5, 0, 0, _MainButton, 0, 0, 0, 0, &buttonfont};

field _MainButton2[] = {
(char *) &Hilfe,             bss0, bsz0, 5, 4+7*bss, 0, "", COLBUTTON, 0,
                                                              0, 0,
(char *) &Back,              bss0, bsz0, 5, 4+6*bss, 0, "", COLBUTTON, 0,
 *                                                              func5, 0,
(char *) &BuOK,              bss0, bsz0, 5, 4+5*bss, 0, "", COLBUTTON, 0,
 *                                                              func6, 0,
(char *) &BuKomplett,        bss0, bsz0, 5, 4+4*bss, 0, "", COLBUTTON, 0,
 *                                                              func8, 0,
(char *) &PfeilR,            bss0, bsz0, 5, 4+3*bss, 0, "", COLBUTTON, 0,
                                                          func4, 0,
(char *) &PfeilU,            bss0, bsz0, 5, 4+2*bss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,            bss0, bsz0, 5, 4+1*bss, 0, "", COLBUTTON, 0,
                                                          func2, 0,
(char *) &PfeilL,            bss0,  bsz0,5, 4,     0, "", COLBUTTON, 0,
                                                          func1, 0};

form MainButton2 = {8, 0, 0, _MainButton2, 0, 0, 0, 0, &buttonfont};

static char datum [12];
static char zeit [12];
static char datumzeit [24];

/*
static field _uhrform [] = {
datum,            0, 0,  5, -1, 0, "", DISPLAYONLY, 0, 0, 0, 
zeit,             0, 0, 20, -1, 0, "", DISPLAYONLY, 0, 0, 0};
*/

static field _uhrform [] = {
datumzeit,        0, 0,  -1, -1, 0, "", DISPLAYONLY, 0, 0, 0}; 

static form uhrform = {1, 0, 0, _uhrform, 0, 0, 0, 0, &uhrfont};

int GetAktButton (void)
/**
Aktiven Button holen.
**/
{
        HWND hWnd;
        int i;

        hWnd = GetFocus ();

        for (i = 0; i < MainButton.fieldanz; i ++)
        {
                    if (hWnd == MainButton.mask[i].feldid)
                    {
                               return (i);
                    }
        }

        for (i = 0; i < MainButton2.fieldanz; i ++)
        {
                    if (hWnd == MainButton2.mask[i].feldid)
                    {
                            return (MainButton.fieldanz + i);
                    }
        }
        return (AktButton);
}

field *GetAktField ()
/**
Field zu AktButton holen.
**/
{
        if (AktButton < MainButton.fieldanz)
        {
                      return (&MainButton.mask [AktButton]);
        }
        return (&MainButton2.mask[AktButton - MainButton.fieldanz]);
}

int menfunc1 (void)
/**
Function fuer Button 1
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         EnterNumBox (LogoWindow,
                                "  Artikel-Nr  ", aufps.a, 14,
                                "");


         memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
         AuftragListe ();
         SetColFocus (MainButton.mask[0].feldid); 
         AktButton = GetAktButton ();
         return 0;
}

int menfunc2 (void)
/**
Function fuer Button 2
**/
{
         HWND aktfocus;
         
         if (AufLock) return 0;

         aktfocus = GetFocus ();
         EnterListe ();
         while (syskey != KEY5)
         {
                   AuftragListe ();
                   EnterListe ();
         }
         AktButton = GetAktButton ();
         aufpidx = 0;
         menZeile = menSelect = 0;
         return 0;
}

int menfunc3 (void)
/**
Function fuer Button 3
**/
{
         HWND aktfocus;

//         InitAufKopf ();
         LiefKopf ();
         aktfocus = GetFocus ();
         SetColFocus (MainButton.mask[0].feldid); 
         AktButton = GetAktButton ();
         return 0;
}

int menfunc4 (void)
/**
Function fuer Button 4
**/
{
         HWND aktfocus;

         EnterTyp = SUCHEN;
         AuftragKopf (SUCHEN);
         EnterTyp = ENTER;
         if (syskey == KEY5)
         {
                       InitAufKopf ();
         }
         else
         {
                      AuftragKopf (ENTER);
         }
         aktfocus = GetFocus ();
         AktButton = GetAktButton ();
         return 0;
}



int menfunc5 (void)
/**
Function fuer Button 5
**/
{
         HWND aktfocus;

         AuftragKopf (AUSWAHL);
         if (syskey == KEY5)
         {
                       InitAufKopf ();
         }
         else
         {
                      AuftragKopf (ENTER);
         }
         aktfocus = GetFocus ();
         AktButton = GetAktButton ();
         return 0;
}

int menfunc6 (void)
/**
Function fuer Button 6
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         AuswahlSysPeri ();
         AktButton = GetAktButton ();
         return 0;
}

int menfunc7 (void)
/**
Function fuer Button 7
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         EnterNumBox (LogoWindow,
                                "  Komm-Datum  ", KomDatum, 11,
                                "");
         AktButton = GetAktButton ();
         return 0;
}

int menfunc8 (void)
/**
Function fuer Button 8
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         AktButton = GetAktButton ();
         return 0;
}

int menfunc9 (void)
/**
Function fuer Button 9
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         AktButton = GetAktButton ();
         return 0;
}

void menue_end (void)
/**
Menue beenden.
**/
{
         PostQuitMessage (0);
}

int menfunc0 (void)
/**
Function fuer Button 10
**/
{
         HWND aktfocus;
         field *feld;

         feld = &MainButton2.mask[3];
         aktfocus = GetFocus ();
         menue_end ();
         return 0;
}

int func1 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 // PostMessage (NULL, WM_CHAR,  (WPARAM) ' ', 0l);
                 SelectAktCtr ();
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_LEFT,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}
          
int func2 (void)
/**
Button fuer Pfeil oben bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_UP, 0l);
                 return 1;
         }
         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_UP,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}
          
int func3 (void)
/**
Button fuer Pfeil unten bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_DOWN,
                                             0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func4 (void)
/**
Button fuer Pfeil rechts bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) ' ', 0l);
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }

         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_RIGHT,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

void backmenue (char *men)
/**
Vorhergehendes Menue ermitteln.
**/
{
          char *pos;

          menue_end ();
          return;

          pos = men + strlen (men) - 1;

          for (; pos >= men; pos --)
          {
                    if (*pos != '0')
                     {
                            *pos = '0';
                            break;
                    }
          }
}
          

int func5 (void)
/**
Function fuer Zurueckbutton bearbeiten
**/
{
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 PostMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (KopfEnterAktiv)
         {
                 PostMessage (AufKopfWindow, WM_KEYDOWN,
                                     (WPARAM) VK_F5, 0l);
                 return 1;
         }


         backmenue (menue.prog);
         return 0;
}

int dokey5 (void)
/**
Aktion bei Taste F5 (zurueck) 
**/
{
         field *feld;

         menue_end ();
         return 0;

         backmenue (menue.prog);
         feld = &MainButton.mask[0];
         SetFocus (feld->feldid);
         AktButton = GetAktButton ();
         display_form (BackWindow2, &MainButton, 0, 0);
         UpdateWindow (BackWindow2);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}


int func6 (void)
/**
Function fuer OK setzen.
**/
{

         if (PtListeAktiv)
         {
                  PostMessage (PtWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
                 return 1;
         }


         if (KomplettAktiv)
         {
                 SelectAktCtr ();
                 return 1;
         }

         if (KopfEnterAktiv && ListeAktiv == 0)
         {
                 if (EnterTyp == SUCHEN)
                 {
                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
                 }
                 else
                 {
                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
                 }
                 return 1;
         }

         if (LiefEnterAktiv && ListeAktiv == 0)
         {
                 syskey = KEY12;
                 PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
                 return 1;
         }

         if (ListeAktiv == 0)
         {
                 return 0;
         }

         PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
         return 0;
}

int func8 (void)
/**
Function fuer Benutzer bearbeiten
**/
{
         if (ListeAktiv == 0) return 0;
         if (KomplettAktiv) return 0;

         EnableWindow (eKopfWindow, FALSE);
         EnableWindow (eFussWindow, FALSE);
         SetKomplett ();
         EnableWindow (eKopfWindow, TRUE);
         EnableWindow (eFussWindow, TRUE);
         return 0;
}


void printlogo (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
        HDC      hdc;

        GetObject (hbr, sizeof (BITMAP), &bm);
        bmy = bm.bmHeight / 2;
        bmx = bm.bmWidth / 2;
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bm.bmWidth - xstretch, bm.bmHeight - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);
        
        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bm.bmHeight * 2)
            {
                        ystretch = bm.bmHeight * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bm.bmWidth * 2)
            {
                        xstretch = bm.bmWidth * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}

void strechbmp (HBITMAP hbr, int x, int y, DWORD mode)
{

        HDC      hdc;

        GetObject (hbr, sizeof (BITMAP), &bm);
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        // SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, 0, 0,
                         bm.bmWidth, bm.bmHeight,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);
        
        SelectObject (hdcMemory, hbmOld);
        DeleteDC (hdcMemory);        
}


void printbmp (HBITMAP hbr, int x, int y, DWORD mode)
{
        RECT rect;

        GetClientRect (hMainWindow,  &rect);
        GetObject (hbr, sizeof (BITMAP), &bm);
        x = (rect.right - bm.bmWidth) / 2;
        y = (rect.bottom - bm.bmHeight) / 2;
        hdc = GetDC (hFitWindow);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        BitBlt (hdc, x, y, bm.bmWidth, bm.bmHeight,
                           hdcMemory,0, 0, mode);
        
        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);        
        ReleaseDC (hFitWindow, hdc);
}

int ScrollMenue ()
/**
Leere MenueZeilen entfernrn.
**/
{
        int i, j;

        memcpy (&scmenue, &menue, sizeof (MENUE));
        memcpy (&menue,   &leermenue, sizeof (MENUE));
        menue.mdn = scmenue.mdn;
        menue.fil = scmenue.fil;
        strcpy (menue.pers, scmenue.pers);
        strcpy (menue.prog, scmenue.prog);
        menue.anz_menue = scmenue.anz_menue;

        for (i = 0,j = 0; j < 10; j ++)
        {
                clipped (mentab[j]);
                if (strcmp (mentab[j], " ") > 0)
                {
                                    strcpy (mentab0[i], mentab[j]);
                                    strcpy (zeitab0[i], zeitab[j]);
                                    i ++;
                }
        }
        return (i);
}


//  Hauptprogramm

int      PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
        HCURSOR oldcursor;
        char cfg_v [5];


        if (ProgCfg.GetCfgValue ("tara", cfg_v) ==TRUE)
        {
                    tara_kz = atoi (cfg_v);
        }
        else
        {
                    tara_kz = 0;
        }
        if (ProgCfg.GetCfgValue ("mhd", cfg_v) ==TRUE)
        {
                    mhd_kz = atoi (cfg_v);
        }
        else
        {
                    mhd_kz = 0;
        }
        if (getenv ("testmode"))
        {
                     testmode = atoi (getenv ("testmode"));
        } 
        opendbase ("bws");
        sysdate (KomDatum);
        menue_class.SetSysBen ();
        if (sys_ben.berecht != 0)
        {
                   ohne_preis = 1;
        }

        if (getenv ("WAAKT"))
        {
                    strcpy (waadir, getenv ("WAAKT"));
                    sprintf (ergebnis, "%s\\ergebnis", waadir);
        }
 
        menue_class.Lesesys_inst ();


        auszeichner = GetStdAuszeichner ();
        sys_peri_class.lese_sys (auszeichner);

        StdWaage = GetStdWaage ();
        sys_peri_class.lese_sys (StdWaage);
        la_zahl = sys_peri.la_zahl;

        strcpy (sys_par.sys_par_nam, "immer_preis");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    immer_preis = atoi (sys_par.sys_par_wrt);
        }

        strcpy (sys_par.sys_par_nam, "ls_leer_zwang");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    ls_leer_zwang = atoi (sys_par.sys_par_wrt);
        }

        if (strcmp (clipped (sys_inst.projekt), "F H S"))
        { 
                    fitbmp   = LoadBitmap (hInstance, "fit1");
        }
        else
        {
                    fitbmp   = LoadBitmap (hInstance, "fhs");
                    IsFhs = 1;
        }
        PfeilL.bmp = LoadBitmap (hInstance, "pfeill");
        PfeilR.bmp = LoadBitmap (hInstance, "pfeilr");
        PfeilU.bmp = LoadBitmap (hInstance, "pfeilu");
        PfeilO.bmp = LoadBitmap (hInstance, "pfeilo");

        hMainInst = hInstance;
        if (getenv (BWS))
        {
                   strcpy (fitpath, getenv (BWS));
        }
        else
        {
                   strcpy (fitpath, "\\USER\\BWS8000");
        }


        strcpy (rosipath, getenv (RSDIR));

        GetObject (fitbmp, sizeof (BITMAP), &fitbm);

        InitFirstInstance (hInstance);
        if (InitNewInstance (hInstance, nCmdShow) == 0) return 0;

        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        WaageInit ();
        SetCursor (oldcursor);
        SetFocus (MainButton.mask[0].feldid);
        return ProcessMessages ();
}

void InitFirstInstance(HANDLE hInstance)
{
        WNDCLASS wc;

        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "FITICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  lpszClassName;

        RegisterClass(&wc);

        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_OWNDC;
        wc.lpfnWndProc   =  FitLogoProc;
        wc.lpszMenuName  =  "";
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "Fitlogo";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  UhrProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "Uhr";
        RegisterClass(&wc);


        wc.lpfnWndProc   =  WndProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "Backwind";

        RegisterClass(&wc);

        wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
        wc.lpszClassName =  "LogoWind";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  GebProc;
        wc.hbrBackground =  CreateSolidBrush (GebColor);
        wc.lpszClassName =  "GebWind";
        RegisterClass(&wc);

        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "GebWindB";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   MsgProc;
        wc.hbrBackground =   GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "AufWiegWhite";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   MsgProc;
        wc.hbrBackground =   CreateSolidBrush (RGB (0, 0, 255));
        wc.lpszClassName =  "AufWiegBlue";
        RegisterClass(&wc);

}


BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        RECT rect;
        RECT rect1;
        HCURSOR oldcursor;

        hMainWindow = CreateWindow (lpszClassName,
                                    lpszMainTitle,
                                    WS_DLGFRAME | WS_CAPTION | WS_SYSMENU |
                                    WS_MINIMIZEBOX,
                                    0, 0,
                                    800, 580,
                                    NULL,
                                    NULL,
                                    hInstance,
                                    NULL);
        if (hMainWindow == 0) return FALSE;
        ShowWindow(hMainWindow, nCmdShow);
        UpdateWindow(hMainWindow);

        hFitWindow = hMainWindow;

        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        strcpy (menue.prog, "00000");
        strcpy (menue.prog, "00000");
        SetCursor (oldcursor);

        GetClientRect (hMainWindow,  &rect);

        BackWindow1  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              10, 10,
                              rect.right - (fitbm.bmWidth + 40),
                              rect.bottom - 100,
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);

        GetClientRect (BackWindow1,  &rect1);

        BackWindow2  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              rect.right - (fitbm.bmWidth + 25),
                              10,
                              fitbm.bmWidth + 15,
                              rect.bottom - 20,
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);

        BackWindow3  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              10,
                              rect.bottom - 60,
                              rect.right - (fitbm.bmWidth + 40),
                              rect.bottom - 20 - (rect.bottom - 70),
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);


        LogoWindow  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "LogoWind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              5, 5,
                              rect1.right - 10,
                              rect1.bottom - 10,
                              BackWindow1,
                              NULL,
                              hInstance,
                              NULL);
        UpdateWindow (BackWindow1);
        UpdateWindow (BackWindow2);
        UpdateWindow (BackWindow3);
        UpdateWindow (LogoWindow);
        return TRUE;
}

int CreateFitWindow (void)
/**
FitWindow erzeugen.
**/
{

        FitWindow = CreateWindow   ("Fitlogo",
                                    "",
                                    WS_CHILD | WS_VISIBLE | WS_DLGFRAME,
                                    5, 5,
                                    fitbm.bmWidth, fitbm.bmHeight,
                                    BackWindow2,
                                    NULL,
                                    hMainInst,
                                    NULL);
        fithdc = GetDC (FitWindow);
        if (IsFhs == 0)
        {
                    SetTimer (FitWindow , 1, twait, 0);  
        }
        return TRUE;
}

int CreateUhrWindow (void)
/**
UhrWindow erzeugen.
**/
{
        RECT rect;
        int x,y, cx, cy;

        GetClientRect (LogoWindow,  &rect);
        cx = fitbm.bmWidth;
        cy = 13;
        x = 5;
        y = fitbm.bmHeight + 5;

        UhrWindow = CreateWindowEx (
                                    0,
                                    "Uhr",
                                    "",
                                    WS_CHILD | WS_VISIBLE,
                                    x, y,
                                    cx, cy,
                                    BackWindow2,
                                    NULL,
                                    hMainInst,
                                    NULL);
        SetTimer (UhrWindow , 1, twaitU, 0);  
        return TRUE;
}


int IsHotKey (MSG *msg)
/**
HotKey Testen.
**/
{
         UCHAR taste;
         int i;
         ColButton *ColBut;
         char *pos;

         if (msg->message != WM_CHAR)
         {
                      return FALSE;
         }

         taste = (char) msg->wParam;
         for (i = 0; i < MainButton.fieldanz; i ++)
         {
                      ColBut = (ColButton *) MainButton.mask[i].feld;
                      if (ColBut->text1)
                      {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                              ((UCHAR) toupper (*(pos + 1)) == toupper (taste)))
                           {
                                   SetFocus (MainButton.mask[i].feldid);
                                   AktButton = GetAktButton ();
                                   SendMessage (AktivWindow,WM_KEYDOWN,
                                                VK_RETURN, 0); 
                                   return TRUE;
                           }
                      }
         }

         for (i = 0; i < MainButton2.fieldanz; i ++)
         {
                      ColBut = (ColButton *) MainButton2.mask[i].feld;
                      if (ColBut->text1)
                      {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                              ((UCHAR) toupper (*(pos + 1)) == toupper (taste)))
                           {
                                   SetFocus (MainButton2.mask[i].feldid);
                                   AktButton = GetAktButton ();
                                   return TRUE;
                           }
                      }
         }
         return FALSE;
}

void PrevKey (void)
/**
Vorhergenden Button aktivieren.
**/
{

         if (AktButton >= MainButton.fieldanz) return;
         AktButton = GetAktButton (); 
         if (AktButton == 0)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}

void NextKey (void)
/**
Naechsten Button aktivieren.
**/
{

         if (AktButton >= MainButton.fieldanz) return;

         AktButton = GetAktButton (); 
         if (AktButton == MainButton.fieldanz - 1)
         {
                     AktButton = 0;
         }
         else
         {
                     AktButton ++;
         }
         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}

void RightKey (void)
/**
Button nach rechts.
**/
{

         AktButton = GetAktButton ();
         if (AktButton <  MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz +
                                 MainButton2.fieldanz - 5;
         }
         else if (AktButton == MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}

void LeftKey (void)
/**
Button nach rechts.
**/
{

         AktButton = GetAktButton (); 
         if (AktButton <  MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz;
         }
         else if (AktButton == MainButton.fieldanz +
                               MainButton2.fieldanz - 5)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton ++;
         }
                     
         if (AktButton < MainButton.fieldanz)
         {
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}


void ActionKey (void)
/**
Button-Aktion ausfuehren.
**/
{
         field *feld;

         AktButton = GetAktButton ();
         feld = GetAktField ();

         if (feld->after != (int (*) (void)) 0)
         {
                    (*feld->after) ();
         }
         else if (feld->BuId != 0)
         {
                    SendMessage (GetParent (feld->feldid), WM_COMMAND,
                                            MAKELONG (feld->BuId,
                                            feld->feldid),
                                            0l);
         }
}
         

int IsKeyPress (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         int taste;
         int i;
         ColButton *ColBut;
         char *pos;



         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     if (keypressed)
                     {
                                  return FALSE;
                     }
                     taste = (int) msg->wParam;
                     if (taste == VK_UP)
                     {
                               PrevKey ();
                               return TRUE;
                     }
                     if (taste == VK_DOWN)
                     {
                               NextKey ();
                               return TRUE;
                     }
                     if (taste == VK_RIGHT)
                     {
                               RightKey ();
                               return TRUE;
                     }
                     if (taste == VK_LEFT)
                     {
                               LeftKey ();
                               return TRUE;
                     }
                     if (taste == VK_TAB)
                     {
                         if (GetKeyState (VK_SHIFT) < 0)
                         {
                              PrevKey ();
                              return TRUE;
                         }
                         NextKey ();
                         return TRUE;
                     }
                     if (taste == VK_RETURN)
                     {
                               ActionKey ();
                               return TRUE;
                     }
                     for (i = 0; i < MainButton.fieldanz; i ++)
                     {
                         ColBut = (ColButton *) MainButton.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                        keypressed = i + 1;
                                        keyhwnd = MainButton.mask[i].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                              }
                         }
                    }

                    for (i = 0; i < MainButton2.fieldanz; i ++)
                    {
                         ColBut = (ColButton *) MainButton2.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                         keypressed = i + MainButton.fieldanz;
                                         keyhwnd = MainButton2.mask[i].feldid;
                                         SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                         return TRUE;
                              }
                         }
                      }
              }
              case WM_KEYUP :
              {
                     if (!keypressed)
                     {
                                  return FALSE;
                     }
                     taste = (int) msg->wParam;
                     for (i = 0; i < MainButton.fieldanz; i ++)
                     {
                         ColBut = (ColButton *) MainButton.mask[i].feld;
                         if (ColBut->text1)
                         {
                             if ((pos = strchr (ColBut->text1, '&')) &&
                                ((UCHAR) toupper (*(pos + 1)) == taste))
                             {
                                   if (keyhwnd != MainButton.mask[i].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = MainButton.mask[i].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                    }

                    for (i = 0; i < MainButton2.fieldanz; i ++)
                    {
                         ColBut = (ColButton *) MainButton2.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                   if (keyhwnd != MainButton2.mask[i].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = MainButton2.mask[i].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                      }
             }
         }
         return FALSE;
}


int     ProcessMessages(void)
{
       MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsMouseMessage (&msg));
              else if (IsHotKey (&msg));
              else if (IsKeyPress (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
        }
        DestroyFonts ();
        return msg.wParam;
}


LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == BackWindow2)
                    {
                             if (FitWindow == 0)
                             {
                                        CreateFitWindow ();
                             }
                             display_form (BackWindow2,&MainButton, 0, 0);
                    }
                    else if (hWnd == BackWindow3)
                    {
                             if (MainButton2.mask[0].feldid == 0)
                             {
                                  display_form (BackWindow3,&MainButton2, 0, 0);
                             }
                    }
                    break;
              case WM_KEYDOWN :
                         switch (wParam)
                         {
                              case VK_F5 :
                                      func5 ();
                                      return 0;

                         }
                         break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    return MenuJob(hWnd,wParam,lParam);
              case WM_DESTROY :
                    if (hWnd == hMainWindow)
                    {
                             ExitProcess (0);
                             return 0;
                    }
                    return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG MenuJob(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
        HMENU hMenu;

        hMenu = GetMenu (hWnd);
 
        switch (wParam)
        {
              case CM_HROT :
                     logorx = (logorx + 1) % 2;
                     if (logorx)
                     {
                               CheckMenuItem (hMenu, CM_HROT, MF_CHECKED);
                               CheckMenuItem (hMenu, CM_VROT, MF_UNCHECKED);
                               logory = 0;
                     }
                     else
                     {
                               CheckMenuItem (hMenu, CM_HROT, MF_UNCHECKED);
                     }
                     sign = signx = 1;
                     xstretch = ystretch = 0;
                     return 0;
              case CM_VROT :
                     logory = (logory + 1) % 2;
                     if (logory)
                     {
                               CheckMenuItem (hMenu, CM_VROT, MF_CHECKED);
                               CheckMenuItem (hMenu, CM_HROT, MF_UNCHECKED);
                               logorx = 0;
                     }
                     else
                     {
                               CheckMenuItem (hMenu, CM_VROT, MF_UNCHECKED);
                     }
                     sign = signx = 1;
                     xstretch = ystretch = 0;
                     return 0;
              case CM_PROGRAMMENDE :
                     DeleteObject (fitbmp);
                     DestroyWindow (hMainWindow);
                     KillTimer (FitWindow, 1);
                     ReleaseDC (FitWindow, fithdc);
                     PostQuitMessage(0);
                     return 0;
        }
        return (0);
}

LONG FAR PASCAL UhrProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {
              case WM_TIMER :
                    switch (wParam)
                    {
                        case 1 :
                           sysdate (datum);
                           systime (zeit);
                           sprintf (datumzeit, "%s %s", datum, zeit);
                           InvalidateRect (UhrWindow, NULL, TRUE);
                           UpdateWindow (UhrWindow);
                           return 0;
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL FitLogoProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;

        switch(msg)
        {
              case WM_TIMER :
                    switch (wParam)
                    {
                        case 1 :
                           printlogo (fitbmp, 0, 0, SRCCOPY);
                           return 0;
                    }
                    break;
              case WM_PAINT :
                    if (IsFhs)
                    {
                                fithdc = BeginPaint (hWnd, &ps);
                                strechbmp (fitbmp, 0, 0, SRCCOPY);
                                EndPaint (hWnd, &ps);
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


/** Ab hier Eingabe einer Nummer in einem extra Fenster mit
    numerischen Block fuer die Maus.
**/

static HWND eNumWindow;
static break_num_enter = 0;

mfont EditNumFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};

/*
mfont TextNumFont = {"Arial", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};
*/

mfont TextNumFont = {"Arial", 200, 0, 1, BLACKCOL,
                                         LTGRAYCOL,
                                         1,
                                         NULL};

mfont numfieldfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont ctrlfont = {"", 100, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont stornofont = {"", 150, 0, 1,
                                       RGB (255, 0, 0),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont OKfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};


ColButton Num1      =   {"1", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton Num2      =   {"2", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton Num3      =   {"3", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton Num4      =   {"4", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton Num5      =   {"5", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton Num6      =   {"6", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton Num7      =   {"7", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton Num8      =   {"8", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton Num9      =   {"9", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton Num0      =   {"0", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton NumP     =   {",", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                        0};

ColButton NumM     =   {"-", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};


ColButton NumLeft   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeill, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton NumRight   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilr, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         0};

ColButton NumDel     =   {"L�schen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         0};

ColButton NumAbbruch  =   {"Abbruch", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         0};

ColButton NumOK       =   {"OK", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         BLACKCOL,
                         GREENCOL,
                         0};

ColButton BuStorno =   {"&Storno", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         0};


static int nbsb  = 85;

static int nbsx   = 5;
int ny    = 10;
/*
static int nbss0 = 80;
static int nbss  = 64; 
static int nbsz  = 40;
static int nbsz0 = 64;
*/
static int nbss0 = 64;
static int nbss  = 64; 
static int nbsz  = 40;
static int nbsz0 = 64;
static int nAktButton = 0;

static int donum1 ();
static int donum2 ();
static int donum3 ();
static int donum4 ();
static int donum5 ();
static int donum6 ();
static int donum7 ();
static int donum8 ();
static int donum9 ();
static int donum0 ();
static int donump ();
static int donumm ();

static int doleft ();
static int doright ();
static int dodel ();
static int doabbruch ();
static int doOK ();

static field _NumField[] = {
(char *) &Num1,          nbss0, nbsz0, ny, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum1, 0,
(char *) &Num2,          nbss0, nbsz0, ny, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum2, 0,
(char *) &Num3,          nbss0, nbsz0, ny, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum3, 0,
(char *) &Num4,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum4, 0,
(char *) &Num5,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum5, 0,
(char *) &Num6,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum6, 0,
(char *) &Num7,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum7, 0,
(char *) &Num8,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum8, 0,
(char *) &Num9,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum9, 0,
(char *) &Num0,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donum0, 0,
(char *) &NumP,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donump, 0,
(char *) &NumM,          nbss0, nbsz0, ny, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donumm, 0};

static form NumField = {12, 0, 0, _NumField, 0, 0, 0, 0, &numfieldfont};

/*
static int cbss0 = 80;
static int cbss  = 64; 
static int cbsz  = 40;
static int cbsz0 = 64;
*/

static int cbss0 = 64;
static int cbss  = 64; 
static int cbsz  = 40;
static int cbsz0 = 64;
static int cbsx = nbsx + 5 * nbss0;

static field _NumControl[] = {
(char *) &NumLeft,       cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doleft, 0,
(char *) &NumRight,       cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doright, 0,
(char *) &NumDel,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           dodel, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl = {4, 0, 0, _NumControl, 0, 0, 0, 0, &ctrlfont};

static field _OKControl[] = {
(char *) &NumOK,         cbss0 * 2,
                                cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                             doOK, 0};
static form OKControl = {1, 0, 0, _OKControl, 0, 0, 0, 0, &OKfont};


static char editbuff [40];

static field _EditForm [] = {
editbuff,             0, 0, 0, 0, 0, "", EDIT, 0, 0, 0};

static form EditForm = {1, 0, 0, _EditForm, 0, 0, 0, 0, &EditNumFont};


static field _TextForm [] = {
editbuff,             0, 0, 0, 0, 0, "", READONLY, 0, 0, 0};

static form TextForm = {1, 0, 0, _TextForm, 0, 0, 0, 0, &TextNumFont};


static field _StornoForm [] = {
(char *) &BuStorno,       100, 64, 0, 0, 0, "", COLBUTTON, 0, 0, 0};

static form StornoForm = {1, 0, 0, _StornoForm, 0, 0, 0, 0, &stornofont};


void SetStorno (int (*sproc) (void), int BuId)
/**
Procedure fuer Storno Setzen.
**/
{
           StornoForm.mask[0].after = sproc;
           StornoForm.mask[0].BuId  = BuId;
}

int donum1 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '1', 0l);
        return 1;
}

int donum2 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '2', 0l);
        return 1;
}

int donum3 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '3', 0l);
        return 1;
}

int donum4 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '4', 0l);
        return 1;
}

int donum5 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '5', 0l);
        return 1;
}

int donum6 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '6', 0l);
        return 1;
}

int donum7 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '7', 0l);
        return 1;
}

int donum8 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '8', 0l);
        return 1;
}

int donum9 ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '9', 0l);
        return 1;
}

int donum0 ()
/**
Aktion bei Button ,
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '0', 0l);
        return 1;
}

int donump ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '.', 0l);
        return 1;
}

int donumm ()
/**
Aktion bei Button 1
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '-', 0l);
        return 1;
}


int doleft ()
/**
Aktion bei Button links
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_LEFT, 0l);
        return 1;
}

int doright ()
/**
Aktion bei Button rechts
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RIGHT, 0l);
        return 1;
}

int dodel ()
/**
Aktion bei Button rechts
**/
{
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_DELETE, 0l);
        return 1;
}

int doabbruch ()
/**
Aktion bei Button F5
**/
{
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_F5, 0l);
        return 1;
}

int doOK ()
/**
Aktion bei Button rechts
**/
{
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RETURN, 0l);
        return 1;
}

void NumEnterBreak ()
/**
Endeflag fuer Erfassung setzen.
**/
{
        break_num_enter = 1;
}

HWND IsNumChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < NumField.fieldanz; i ++)
        {
                   if (MouseinWindow (NumField.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumField.mask[i].feldid);
                   }
        }

        for (i = 0; i < NumControl.fieldanz; i ++)
        {
                   if (MouseinWindow (NumControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < OKControl.fieldanz; i ++)
        {
                   if (MouseinWindow (OKControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (OKControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < EditForm.fieldanz; i ++)
        {
                   if (MouseinWindow (EditForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (EditForm.mask[i].feldid);
                   }
        }
        if (StornoForm.mask[0].feldid == 0)
        {
                   return FALSE;
        }

        for (i = 0; i < StornoForm.fieldanz; i ++)
        {
                   if (MouseinWindow (StornoForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (StornoForm.mask[i].feldid);
                   }
        }
        return NULL;
}


LONG FAR PASCAL EntNumProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
        HWND enchild;
        static int SendChild = 0;
        int HasFocus = 1;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    display_form (eNumWindow, &NumField, 0, 0);
                    display_form (eNumWindow, &NumControl, 0, 0);
                    display_form (eNumWindow, &OKControl, 0, 0);
                    display_form (eNumWindow, &TextForm, 0, 0);
                    display_form (eNumWindow, &EditForm, 0, 0);

                    if (StornoForm.mask[0].after)
                    {
                              display_form (eNumWindow, &StornoForm, 0, 0);
                    }
                    else if (StornoForm.mask[0].BuId)
                    {
                              display_form (eNumWindow, &StornoForm, 0, 0);
                    }

                    SetFocus (EditForm.mask[0].feldid);
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    enchild = IsNumChild (&mousepos);
                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
                    return TRUE;
              case WM_ACTIVATE :
                    switch (wParam)
                    {
                         case WA_ACTIVE :
                         case WA_CLICKACTIVE :
//                              SetCapture (eNumWindow);
                              break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void RegisterNumEnter (void)
/**
Fenster fuer numerische Eingabe registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  EntNumProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 120));
                   wc.lpszClassName =  "EnterNum";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

int IsNumKey (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         ColButton *ColBut;
         int taste;
         char *pos;
         
         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    NumEnterBreak ();
                                    syskey = KEY5;
                                    return TRUE;
                            case VK_RETURN :
                                    NumEnterBreak ();
                                    GetWindowText (EditForm.mask[0].feldid,
                                                   EditForm.mask [0].feld,
                                                   EditForm.mask [0].length);
                                    syskey = KEYCR;
                                    return TRUE;
                     }
                     taste = (int) msg->wParam;
                     ColBut = (ColButton *) StornoForm.mask[0].feld;
                     if (ColBut->text1)
                     {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = StornoForm.mask[0].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                     }
                     return FALSE;
              }
              case WM_KEYUP :
              {
                         taste = (int) msg->wParam;
                         ColBut = (ColButton *) StornoForm.mask[0].feld;
                         if (ColBut->text1)
                         {
                             if ((pos = strchr (ColBut->text1, '&')) &&
                                ((UCHAR) toupper (*(pos + 1)) == taste))
                             {
                                   if (keyhwnd != StornoForm.mask[0].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = StornoForm.mask[0].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                         return FALSE;
              }
              case WM_CHAR :
              {
                     SendMessage (EditForm.mask[0].feldid,
                                   msg->message,
                                   msg->wParam,
                                   msg->lParam);
                                   return TRUE;
               }
          }

          return FALSE;
}


void EnterNumBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic)
/**
Fenster fuer numerische Eingabe oeffnen.
**/
{
        RECT rect;
        MSG msg;
        TEXTMETRIC tm;
        HFONT hFont;
        HDC hdc;
        int x, y, cx, cy;
        int i, j;
        int tlen;
        int elen, epos;
        int eheight;
        HCURSOR oldcursor;
        static int BitMapOK = 0;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();
        tlen = strlen (text);

        if (BitMapOK == 0)
        {
                 NumLeft.bmp = LoadBitmap (hMainInst, "pfeill");
                 NumRight.bmp = LoadBitmap (hMainInst, "pfeilr");
                 BitMapOK = 1;
        }

        RegisterNumEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = rect.bottom - (3 * nbsz0) - 46;
        cx = rect.right - rect.left; 
        cy = rect.bottom - y;

        ny = cy - (3 * nbsz0) - 30;
        for (i = 0,j = 0; j < NumField.fieldanz - 3; i ++,j += 3)
        {
                     NumField.mask[j].pos[0] = ny + i * nbsz0;  
                     NumField.mask[j + 1].pos[0] = ny + i * nbsz0;  
                     NumField.mask[j + 2].pos[0] = ny + i * nbsz0;
        }
        NumField.mask[j].pos[0] = ny + 2 * nbsz0;                 
        NumField.mask[j + 1].pos[0] = ny + 1 * nbsz0;
        NumField.mask[j + 2].pos[0] = ny;


        OKControl.mask[0].pos[0]  = ny;

        NumControl.mask[0].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[1].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[2].pos[0] = ny + 2 * nbsz0;
        NumControl.mask[3].pos[0] = ny + 2 * nbsz0;

        cbsx = cx - 2 * nbss0 - 5;
        OKControl.mask[0].pos[1]  = cbsx;

        NumControl.mask[0].pos[1] = cbsx;
        NumControl.mask[1].pos[1] = cbsx + nbss0;
        NumControl.mask[2].pos[1] = cbsx;
        NumControl.mask[3].pos[1] = cbsx + nbss0;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        EditNumFont.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&EditNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DeleteObject (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * nlen;
                 if (elen < epos) break;
                 EditNumFont.FontHeight -= 20;
                 if (EditNumFont.FontHeight <= 80) break;
        } 
                  
        TextNumFont.FontHeight = 200;
                  
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos; 
        eheight = tm.tmHeight;
        eheight += eheight / 3;
        EditForm.mask[0].length = elen; 
        EditForm.mask[0].rows   = eheight;
        EditForm.mask[0].pos[1] = epos;
        EditForm.mask[0].pos[0] = ny;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        elen = StornoForm.mask[0].length;
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos; 
        StornoForm.mask[0].pos[1] = epos;
        StornoForm.mask[0].pos[0] = cy - 30 - StornoForm.mask[0].rows;

        if (pic)
        {
                       EditForm.mask[0].picture = pic;
        }

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);

        elen = tm.tmAveCharWidth * strlen (text);
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos; 
        eheight = tm.tmHeight;
        TextForm.mask[0].length = elen; 
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;
        TextForm.mask[0].pos[0] = (short)
                            EditForm.mask[0].pos[0] + 2 * (short) tm.tmHeight;
        TextForm.mask[0].feld = text;

        while (TRUE)
        {
                 spezfont (&TextNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DeleteObject (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * tlen;
                 if (elen < epos) break;
                 TextNumFont.FontHeight -= 20;
                 if (TextNumFont.FontHeight <= 80) break;
        } 

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);
        elen = tm.tmAveCharWidth * strlen (text);

        TextNumFont.FontHeight -= 20;    

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (hMainWindow, hdc);
        eheight = tm.tmHeight + tm.tmHeight / 3;
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos; 
        TextForm.mask[0].length = elen; 
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;


        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
        memcpy (editbuff, nummer, strlen (nummer));

        eNumWindow  = CreateWindowEx (
                              0,
                              "EnterNum",
                              "",
                              WS_POPUP | WS_CAPTION,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

        create_enter_form (eNumWindow, &EditForm, 0, 0);
        ShowWindow (eNumWindow, SW_SHOW);
        UpdateWindow (eNumWindow);

        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));

        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));

        NumEnterWindow = EditForm.mask[0].feldid;
        EnableWindows (hWnd, FALSE);
//        SetCapture (eNumWindow);

        NumEnterAktiv = 1;
        break_num_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsNumKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_num_enter) break;
        }
        SetActiveWindow (hWnd);
        EnableWindows (hWnd, TRUE);
        CloseControls (&NumField);
        CloseControls (&NumControl);
        CloseControls (&OKControl);
        CloseControls (&EditForm);
        CloseControls (&TextForm);
        if (StornoForm.mask[0].after)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
        else if (StornoForm.mask[0].BuId)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
//        ReleaseCapture ();
        DestroyWindow (eNumWindow);
        NumEnterAktiv = 0;
        strcpy (nummer, editbuff);
        SetCursor (oldcursor);

        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
}


/**

  Listenerfassung

**/


// static int lfbsz0 = 36;
static int lfbsz0 = 42;
static int lfbss0 = 73;

static int doinsert (void);
static int Auszeichnen ();
static int Nachliefern ();
static int Nichtliefern ();
static int SysChoise (); 
static int SwitchwKopf (); 
void arrtolsp (int);
BOOL LeseLspUpd (void);
static int CheckAusz ();

static int sorta (void);
static int sortb (void);
static int sortame (void);
static int sortlme (void);
static int sorts (void);

mfont ListFont = {"Courier New", 200, 150, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      0,
                                      NULL};
mfont InsFontT = {"Courier New", 110, 0, 1, BLACKCOL,
                                            LTGRAYCOL,
                                            1,
                                            NULL};

mfont InsFont = {"Courier New", 150, 0, 1, BLACKCOL,
                                           LTGRAYCOL,
                                           1,
                                           NULL};

mfont lKopfFont = {"Courier New", 150, 0, 1, BLACKCOL,
                                             LTGRAYCOL,
                                             1,
                                             NULL};

mfont lFussFont = {"", 90, 0, 1,       RGB (0, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

ColButton BuInsert = {"Neu", -1, 5,
                       "F6", -1, 21,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (0, 255, 255),
                       BLUECOL,
                       FALSE};

ColButton BuNach = {"nachliefern", -1, 5,
                    "F7", -1, 21,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (0, 255, 255),
                       BLUECOL,
                       FALSE};

ColButton BuNicht = {"nicht vorh.", -1, 5,
                      "F8", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,

                      FALSE};
ColButton BuSys =   {"Ger�t", -1, 5,
                      "F11", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuKopf =   {"Wiegekopf", -1, 5,
                      "F10", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuAusZ = {"auszeich.", -1, 5,
                     "F9", -1, 21,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     YELLOWCOL,
                     -1};

static field _lKopfform0 [] =
{
        "Artikel mit Kommisionierdatum",      
                          0, 0, -1, 10,  0, "", DISPLAYONLY, 0, 0, 0,
         KomDatum,        0, 0, -1, 200, 0, "", DISPLAYONLY, 0, 0, 0
};

static field _lKopfform1 [] =
{
        "Artikel ",      
                          0, 0, -1, 10,  0, "", DISPLAYONLY, 0, 0, 0,
         aufps.a,         0, 0, -1, 200, 0, "", DISPLAYONLY, 0, 0, 0
};

static form lKopfform = {2, 0, 0, _lKopfform0, 0, 0, 0, 0, &lKopfFont};

field _lFussform[] = {
(char *) &BuNach,        lfbss0, lfbsz0,-1, 10 + 0 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           Nachliefern, 0,
(char *) &BuNicht,       lfbss0, lfbsz0,-1, 10 + 1 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           Nichtliefern, 0,
// (char *) &BuInsert,      lfbss0, lfbsz0,-1, 10 + 5 * (15 + lfbss0),   0, "", COLBUTTON, 0,
(char *) &BuInsert,      lfbss0, lfbsz0,-1, -1,                          0, "", COLBUTTON, 0,
                                                           doinsert,  0,
(char *) &BuAusZ,        lfbss0, lfbsz0,-1, 40 + 8 * (5 + lfbss0),    0, "", COLBUTTON, 0,
                                                           Auszeichnen, 0,
(char *) &BuSys,         lfbss0, lfbsz0,-1, 10 + 2 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           SysChoise, 0,
(char *) &BuKopf,        lfbss0, lfbsz0,-1, 10 + 6 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           SwitchwKopf, 0,
};

static form lFussform = {6, 0, 0, _lFussform, 0, 0, 0, 0, &lFussFont};

static field _insform [] =
{
        aufps.a,           12, 0, 1, 1, 0, "%11.0f", READONLY, 0, 0, 0,
        aufps.a_bz1,       20, 0, 1, 14,0, "",       READONLY, 0, 0, 0,      
        aufps.auf_pr_vk,   10, 0, 1, 36, 0, "%9.3f", READONLY, 0, 0, 0,      
        aufps.auf_lad_pr,  10, 0, 1, 47, 0, "%9.3f", READONLY , 0, 0, 0,
};

static form insform = {4, 0, 0, _insform, 0, 0, 0, 0, &InsFont};

static field _insub [] = {
"Artikel-Nr",            0, 0, 0,  1, 0, "", DISPLAYONLY, 0, 0, 0,
"Bezeichnung",           0, 0, 0, 14, 0, "", DISPLAYONLY, 0, 0, 0,
"VK-Preis",              0, 0, 0, 36, 0, "", DISPLAYONLY, 0, 0, 0,
"Ladenpreis",            0, 0, 0, 47, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form insub = { 4, 0, 0,_insub, 0, 0, 0, 0, &InsFontT};


static field _testform [] =
{
        aufps.a,           11, 1, 0, 0, 0, "%11.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       20, 1, 0, 13,0, "",       READONLY, 0, 0, 0,      
        aufps.a_bz2,       20, 1, 0, 35, 0, "",      READONLY, 0, 0, 0,      
        " ",                2, 1, 0, 57, 0, "%9.3f", READONLY , 0, 0, 0,
};


static form testform = { 4, 0, 4,_testform, 0, CheckAusz, 0, 0, &ListFont};

/*
static field _testub [] = {
"Artikel-Nr",           11, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0,
"Bezeichnung",          20, 1, 0,11, 0, "", DISPLAYONLY, 0, 0, 0,
"Auf-Me",                9, 1, 0,35, 0, "", DISPLAYONLY, 0, 0, 0,
"Lief-Me",               9, 1, 0,46, 0, "", DISPLAYONLY, 0, 0, 0,
"S",                     1, 1, 0,57, 0, "", DISPLAYONLY, 0, 0, 0
};
*/

static field _testub [] = {
"Artikel-Nr",           12, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung 1",        22, 1, 0,12, 0, "", BUTTON, 0, sortb,  102,
"Bezeichnung 2",        22, 1, 0,34, 0, "", BUTTON, 0, 0,      103,
"",                      4, 1, 0,56, 0, "", BUTTON, 0, 0,      104,
};

static form testub = { 4, 0, 0,_testub, 0, 0, 0, 0, &ListFont};

static field _testvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 34 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 56 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0
};

static form testvl = {4, 0, 0,_testvl, 0, 0, 0, 0, &ListFont};

static int asort = 1;
static int bsort = 1;
static int amesort = 1;
static int lmesort = 1;
static int ssort = 1;


static int sorta0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1; 
	        struct AUFP_S *el2; 

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((int) (ratod(el1->a) - ratod (el2->a)) * asort);
}


static int sorta (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sorta0);
			asort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sortb0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1; 
	        struct AUFP_S *el2; 

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((int) (strcmp (el1->a_bz1,el2->a_bz1)) * bsort);
}


static int sortb (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortb0);
			bsort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


static int sortame0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1; 
	        struct AUFP_S *el2; 

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
			if (ratod (el1->auf_me) > ratod (el2->auf_me))
			{
				          return (1 * amesort);
            }
			if (ratod (el1->auf_me) < ratod (el2->auf_me))
			{
				          return (-1 * amesort);
            }
			return 0;
}

static int sortame (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortame0);
			amesort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sortlme0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1; 
	        struct AUFP_S *el2; 

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
			if (ratod (el1->lief_me) > ratod (el2->lief_me))
			{
				          return (1 * lmesort);
            }
			if (ratod (el1->lief_me) < ratod (el2->lief_me))
			{
				          return (-1 * lmesort);
            }
			return 0;
}


static int sortlme (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortlme0);
			lmesort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sorts0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1; 
	        struct AUFP_S *el2; 

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((atoi (el1->s) - atoi (el2->s)) * ssort);
}


static int sorts (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sorts0);
			ssort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


BOOL IsAuszArtikel (void)
/**
Feld pr_ausz pruefen.
**/
{
            char pr_ausz [2];

            if (lese_a_bas (ratod (aufps.a)) != 0)
            {
                          return FALSE;
            }
            strcpy (pr_ausz, "N");
            switch (_a_bas.a_typ)
            {
                   case 1 :
                         strcpy (a_hndw.pr_ausz, "N");
                         hndw_class.lese_a_hndw (_a_bas.a);
                         strcpy (pr_ausz, a_hndw.pr_ausz); 
                         break;  
                   case 2 :
                         strcpy (a_eig.pr_ausz, "N");
                         hndw_class.lese_a_eig (_a_bas.a);
                         strcpy (pr_ausz, a_eig.pr_ausz); 
                         a_hndw.tara = a_eig.tara;
                         break;  
                   case 3 :
                         strcpy (a_eig_div.pr_ausz, "N");
                         hndw_class.lese_a_eig_div (_a_bas.a);
                         strcpy (pr_ausz, a_eig_div.pr_ausz); 
                         a_hndw.tara = a_eig_div.tara;
                         break;  
                   default :
                         return FALSE; 
            }
            if (pr_ausz[0] == 'N')
            {
                    return FALSE; 
            }
            return TRUE;
} 
     

int CheckAusz ()
/**
Pruefen, ob der Artikel ausgezeichnet werden kann.
**/
{
            IsAuszArtikel ();
/*             
            if (IsAuszArtikel ())
            {
                         ActivateColButton (eFussWindow, &lFussform, 3, 0, 1);
            }
            else
            {
                         ActivateColButton (eFussWindow, &lFussform, 3, -1, 1);
            }
*/
            return (0);
}

void GetMeEinh (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;
         double auf_me, auf_me_vgl;
        
         einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                                lsk.kun, _a_bas.a, &keinheit);
          
         strcpy (aufps.auf_me_bz,    keinheit.me_einh_kun_bez);
         strcpy (aufps.lief_me_bz,   keinheit.me_einh_bas_bez);
         sprintf (aufps.me_einh_kun, "%d", keinheit.me_einh_kun);
         sprintf (aufps.me_einh,     "%d", keinheit.me_einh_bas);
         clipped (aufps.auf_me_bz);
         clipped (aufps.lief_me_bz);
         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                     strcpy (aufps.auf_me_vgl, aufps.auf_me);
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     auf_me = (double) ratod (aufps.auf_me);
                     auf_me_vgl = auf_me * keinheit.inh;
                     sprintf (aufps.auf_me_vgl, "%.3lf", auf_me_vgl);
         }
         return;
}


int artikelOK (double a)
/**
Artikel-Nummer in Basisdaten suchen.
**/
{
        short sa;
        double pr_ek;
        double pr_vk;
        char lieferdat [12];

        dsqlstatus = lese_a_bas (a);
        if (dsqlstatus != 0)
        {
                     return FALSE;
        }
        sprintf (aufps.a, "%11.0lf", _a_bas.a);
        strcpy (aufps.a_bz1, _a_bas.a_bz1);
        strcpy (aufps.a_bz2, _a_bas.a_bz2);
        pr_ek = pr_vk = (double) 0.0;
        sa = 0;
        dlong_to_asc (lsk.lieferdat, lieferdat);
        dsqlstatus =   WaPreis.preise_holen (lsk.mdn,
                                             lsk.fil,
                                             lsk.kun_fil,
                                             lsk.kun,
                                             _a_bas.a,
                                             lieferdat,
                                             &sa, 
                                             &pr_ek,
                                             &pr_vk);
         if (dsqlstatus != 0)
         {
                      return TRUE;
         }
         sprintf (aufps.auf_pr_vk,  "%8.2lf", pr_ek); 
         sprintf (aufps.auf_lad_pr, "%8.2lf", pr_vk);
         display_form (InsWindow, &insform, 0, 0);
         CheckAusz ();
         return TRUE;
}

void KorrInsPos (TEXTMETRIC *tm)
/**
Positionen in insform korrigieren.
**/
{
        static int posOK = 0;
        int i;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < insform.fieldanz; i ++)
        {
                    insform.mask[i].length *= (short) tm->tmAveCharWidth;       
                    insform.mask[i].pos[1] *= (short) tm->tmAveCharWidth;       
                    insform.mask[i].pos[0] *= (short) tm->tmHeight;
        }

        for (i = 0; i < insub.fieldanz; i ++)
        {
                    insub.mask[i].pos[1] *= (short) tm->tmAveCharWidth;       
                    insub.mask[i].pos[0] *= (short) tm->tmHeight;
        }
}

void OpenInsert (void)
/**
Fenster fuer Insert-Row oeffnen.
**/
{
        RECT rect;
        RECT frmrect;
        HFONT hFont;
        TEXTMETRIC tm;
        int x, y, cx, cy;

        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&insform, insform.font, &frmrect);

        x = rect.left + 5;
        y = rect.top + 60;
        cx = frmrect.right + 26; 
        cy = rect.bottom - rect.top - 200;

        spezfont (&InsFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (hMainWindow, hdc);
        DeleteObject (hFont);

        KorrInsPos (&tm);

        InsWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x, y,
                                    cx + 24, 72,
                                    eWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
}

void DeleteInsert (void)
/**
Insertwindow schliessen.
**/
{
       CloseControls (&insform); 
       CloseControls (&insub); 
       DestroyWindow (InsWindow);
       InsWindow = NULL;
}

BOOL NewPosi (void)
/**
Positionsnummern neu vergeben und in Datenbank schreiben.
**/
{
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   sprintf (aufparr[i].pnr, "%d", (i + 1) * 10);
                   arrtolsp (i);
                   ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls,
                                        lsp.a, lsp.posi);
          }
          commitwork ();
          return TRUE;
}

BOOL DelLeerPos (void)
/**
Positionsnummern neu vergeben und in Datenbank schreiben.
**/
{
          int i;

          lsk.ls      = atol (aufsarr[aufidx].ls);
          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) > 2)
                   {
                         arrtolsp (i);
                         ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls,
                                              lsp.a, lsp.posi);
                   }
          }
          commitwork ();
/*
          if (eWindow)
          {
                   LeseLsp ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;
}

BOOL SollToIst (void)
/**
Bei nicht bearbeiteten Positionen Ist-Wert auf Soll-Wert setzen.
**/
{
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) < 3)
                   {
                       strcpy (aufparr[i].lief_me,
                               aufparr[i].auf_me_vgl);
                       strcpy (aufparr[i].s, "3");
                   }
                   arrtolsp (i);
                   ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls,
                                              lsp.a, lsp.posi);
          }
          commitwork ();
/*
          if (eWindow)
          {
                   LeseLsp ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;
}

BOOL SetAllPosStat (void)
/**
Bei nicht bearbeiteten Positionen Ist-Wert auf Soll-Wert setzen.
**/
{
          int i;

          beginwork ();
          if (LeseLspUpd () == FALSE) return FALSE;
          ls_class.delete_lspls (lsk.mdn, lsk.fil, lsk.ls);
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) < 3)
                   {
                       strcpy (aufparr[i].s, "3");
                   }
                   arrtolsp (i);
                   ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls,
                                              lsp.a, lsp.posi);
          }
          commitwork ();
/*
          if (eWindow)
          {
                   LeseLsp ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
*/
          return TRUE;
}

long GetNextPosi (int pos)
/**
Naechste gueltige Positionnummer ermitteln.
**/
{
          long ap;
          long posi;

          if (pos > 0)
          {
                strcpy (aufparr[pos].pnr, aufparr[pos - 1].pnr);
          }
          for (pos ++;pos <= aufpanz; pos ++)
          {
                 ap = atol (aufparr[pos].pnr);
                 if ((ap % 10) == 0) break;
          }
          posi = atol (aufparr[pos - 1].pnr) + 1;
          return (posi);
}

void ScrollAufp (int pos)
/**
aufparr ab pos zum Einfuegen scrollen.
**/
{
         int i;

         for (i = aufpanz; i > pos; i --)
         {
                       memcpy (&aufparr [i], &aufparr [i + 1],
                                sizeof (struct AUFP_S));
         }
         aufpanz ++;
}


int doinsert (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
         struct AUFP_S aufp;
         char buffer [20];
         int pos;
         long akt_posi;

		 if (BuInsert.aktivate == -1) return (0);
         OpenInsert ();   
         memcpy (&aufp, &aufps, sizeof (struct AUFP_S));
         memcpy (&aufps, &aufp_null, sizeof (struct AUFP_S));
         while (TRUE)
         {
                   strcpy (buffer, "0");
                   EnterNumBox (eWindow,
                                "  Artikel  ", buffer, 12,
                                   testform.mask[0].picture);
                   InvalidateRect (eWindow, NULL, TRUE);
                   UpdateWindow (eWindow);
                   if ((double) ratod (buffer) <= (double) 0.0)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
                              return 0;
                   }
                   if (artikelOK ((double) ratod (buffer)))
                   {
                              break;
                   }
                   disp_messG ("Falsche Artikelnummer", 2);
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
                              return 0;
                   }
         }

         strcpy (buffer, "0");
         if (_a_bas.a_typ == 11)
         {
                  EnterNumBox (eWindow, " Menge ", buffer, 10,
                       testform.mask[2].picture);
                 strcpy (aufps.lief_me, buffer);
                 if (ratod (aufps.lief_me) != (double) 0.0)
                 {
                     strcpy (aufps.s, "3");
                 }
         }
         else
         {
                 EnterNumBox (eWindow, "Auftrags-Menge", buffer, 10,
                       testform.mask[2].picture);
                 strcpy (aufps.auf_me, buffer);
                 GetMeEinh ();
         }


         if (ohne_preis == 0 && immer_preis)
         {
                   EnterPreisBox (eWindow, -1, -1);
         }    

         current_form = &testform;
         DeleteInsert ();
         InvalidateRect (eWindow, NULL, TRUE);
         UpdateWindow (eWindow);
         pos = GetListPos () + 1;
         akt_posi = GetNextPosi (pos);
         sprintf (aufps.pnr, "%ld", akt_posi);
         ScrollAufp (pos);
         memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
         arrtolsp (pos);
         beginwork ();
         ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls, lsp.a, lsp.posi);
         commitwork ();
         LeseLsp ();
         ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
         return 0;
}

int endlist (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

void arrtolsp (int idx)
/**
Satz aus lsp in aufarr uebertragen.
**/
{
        lsp.posi = atol (aufparr[idx].pnr);
        lsp.a = (double) ratod (aufparr[idx].a);
        lsp.auf_me = (double) ratod (aufparr[idx].auf_me);
        lsp.lief_me = (double) ratod (aufparr[idx].lief_me);
        lsp.auf_me_vgl = (double) ratod (aufparr[idx].auf_me_vgl);
        lsp.ls_vk_pr = (double) ratod (aufparr[idx].auf_pr_vk);
        lsp.ls_lad_pr = (double) ratod (aufparr[idx].auf_lad_pr);
        lsp.tara = (double) ratod (aufparr[idx].tara);
        strcpy (lsp.auf_me_bz,aufparr[idx].auf_me_bz);
        strcpy (lsp.lief_me_bz, aufparr[idx].lief_me_bz);
        lsp.pos_stat = atoi (aufparr[idx].s);
        strcpy  (lsp.erf_kz, aufparr[idx].erf_kz);
        strcpy  (lsp.ls_charge, aufparr[idx].ls_charge);
        lsp.lsp_txt = atol (aufparr[idx].lsp_txt);
        lsp.me_einh_kun = atoi (aufparr[idx].me_einh_kun);
        lsp.me_einh     = atoi (aufparr[idx].me_einh);

        lsp.sa_kz_sint   = atoi (aufparr[idx].sa_kz_sint); 
        lsp.prov_satz    = ratod (aufparr[idx].prov_satz);
        lsp.leer_pos     = atoi (aufparr[idx].leer_pos);
        lsp.hbk_date     = dasc_to_long (aufparr[idx].hbk_date);
        lsp.ls_vk_euro   = ratod (aufparr[idx].ls_vk_euro);
        lsp.ls_vk_fremd  = ratod (aufparr[idx].ls_vk_fremd);
        lsp.ls_lad_euro  = ratod (aufparr[idx].ls_lad_euro);
        lsp.ls_lad_fremd = ratod (aufparr[idx].ls_lad_fremd);
        lsp.rab_satz     = ratod (aufparr[idx].rab_satz);

        lsp.me_einh_kun1 = atoi (aufparr[idx].me_einh_kun1);
        lsp.auf_me1      = ratod (aufparr[idx].auf_me1);
        lsp.inh1         = ratod (aufparr[idx].inh1);

        lsp.me_einh_kun2 = atoi (aufparr[idx].me_einh_kun2);
        lsp.auf_me2      = ratod (aufparr[idx].auf_me2);
        lsp.inh2         = ratod (aufparr[idx].inh2);

        lsp.me_einh_kun3 = atoi (aufparr[idx].me_einh_kun3);
        lsp.auf_me3      = ratod (aufparr[idx].auf_me3);
        lsp.inh3         = ratod (aufparr[idx].inh3);
}

long GenLText (void)
/**
Text-Nr fuer lsp.lsp_txt genereieren.
**/
{
        int dsqlstatus;
        short mdn;
        short fil;
        long gen_nr;
        extern short sql_mode;

        mdn = 1;
        fil = 0;

        sql_mode = 1;
        dsqlstatus = nvholid (mdn, fil, "lspt_txt");
        if (dsqlstatus == 100)
        {
              dsqlstatus = nvanmprf (mdn,
                                     fil,
                                     "lspt_txt",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");

              if (dsqlstatus == 0)
              {
                          dsqlstatus = nvholid (mdn,
                                                fil,
                                                "lspt_txt");
              }
         }
         gen_nr = auto_nr.nr_nr;
         sql_mode = 0;
         return gen_nr;
}


void TextInsert (void)
/**
Text fuer Nicht- osder Nachliefern einfuegen.
**/
{
       int aufidx;
       long lsp_txt;
      
       beginwork (); 
       if (Lock_Lsp () == FALSE)
       {
            commitwork ();
            return;
       }
       aufidx = GetListPos ();
       memcpy (&aufparr [aufidx], &aufps, sizeof (struct AUFP_S));
       lsp_txt = atol (aufparr [aufidx].lsp_txt);
       if (lsp_txt == 0l)
       {
           lsp_txt = GenLText ();
       }
       if (lsp_txt == 0l) return;
       strcpy (aufparr[aufidx].s, "3"); 
       sprintf (aufparr[aufidx].lsp_txt, "%ld", lsp_txt);
       memcpy (&aufps, &aufparr [aufidx], sizeof (struct AUFP_S));
       lspt.nr = lsp_txt;
       lspt.zei = 10;
       strcpy (lspt.txt, ptabn.ptbez);
       lspt_class.dbupdate ();
       arrtolsp (aufidx);
       ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls,
                                        lsp.a, lsp.posi);
       commitwork ();
       ShowNewElist ((char *) aufparr,
                      aufpanz,
                      (int) sizeof (struct AUFP_S));
}

int Nachliefern ()
/**
Status NACHLIEFERN setzen.
**/
{
      ptab_class.lese_ptab ("na_lief_kz", "2");
      TextInsert ();
      return (0);
}


int Nichtliefern ()
/**
Status NICHTLIEFERN setzen.
**/
{
      ptab_class.lese_ptab ("na_lief_kz", "1");
      TextInsert ();
      return (0);
}


int SwitchwKopf ()
{
        static int arganz;
        static char *args[4];
        char kopf [4];

        if (la_zahl < 0) return 0;   

        if (la_zahl == 2)
        {
            AktKopf = (AktKopf  + 1) % la_zahl;
            sprintf (kopf, "%d", AktKopf + 1);
            arganz = 4;
            args[0] = "Leer";
            args[1] = "6";
            args[2] = waadir;
            args[3] = (char *) kopf;
            fnbexec (arganz, args);
            print_messG (1, "Wiegkopf %s gew�hlt", kopf);
            return 0;
        }
        SaveMenue ();
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
        AktKopf = AuswahlKopf ();
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetActiveWindow (eWindow);
        AktivWindow = eWindow;
        RestoreMenue ();
        SetDblClck (IsDblClck, 0);

        sprintf (kopf, "%d", AktKopf + 1);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "6";
        args[2] = waadir;
        args[3] = (char *) kopf;
        fnbexec (arganz, args);
        return 0;
}


int SysChoise ()
/**
Auswahl ueber Geraete.
**/
{
        SaveMenue ();
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
        AuswahlSysPeri ();
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetActiveWindow (eWindow);
        AktivWindow = eWindow;
        RestoreMenue ();
        SetDblClck (IsDblClck, 0);
        return (0);
}


int Auszeichnen ()
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        if (BuAusZ.aktivate == -1) return (0);
        WorkModus = AUSZEICHNEN;
        PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
        return (0);
}


void StartAusz (void)
/**
Auszeichnen starten.
**/
{
        struct AUFP_S aufps_save;
        char where [512];
        int idxsave;

        beginwork (); 
        if (Lock_Lsp () == FALSE)
        {
            commitwork ();
            return;
        }
        memcpy (&aufps_save, &aufps,sizeof (struct AUFP_S));
        idxsave = aufpidx;
        aufpidx = 0;
        WorkModus = AUSZEICHNEN;

        sprintf (where, " where lsp.mdn =  %hd and lsp.ls = %ld "
                        " and lsp.a = %0.lf",
                          lsk.mdn, lsk.ls, ratod (aufps.a));
        dsqlstatus = ls_class.lese_lsp_w (where);
        lsptoarr0 (aufpidx);

        memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
        strcpy (auf_me, aufps.rest);
        ArtikelWiegen (LogoWindow);
        SetDblClck (IsDblClck, 0);
        current_form = &testform;
        strcpy (aufps.rest, auf_me);
        memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtolsp (aufpidx);
        ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls, lsp.a, lsp.posi);

        commitwork ();
        aufpidx = idxsave;
        memcpy (&aufps, &aufps_save, sizeof (struct AUFP_S));
        memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
        return;

}


void AuftragListe (void)
/**
Liste ueber Auftraege zum gewaehlten Artikel.
Wird ueber menfunc2 gestartet.
**/
{
        menAufZeile = menAufSelect = 0;
        ActivateColButton (BackWindow3, &MainButton2, 3, 1, 1);
        AuftragKopf (AUSWAHL);
        while (syskey != KEY5)
        {
            if (syskey == KEYCR) StartAusz ();
            AuftragKopf (AUSWAHL);
        }
        ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
}

        
void IsDblClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{

        syskey = KEYCR;
        aufpidx = idx;
        break_list ();    
}


BOOL TestSysWg (void)
/**
Pruefen, ob die Warengruppe dem aaktiven Geraet zugeordnet ist.
**/
{
        pht_wg.sys = auszeichner;
        pht_wg.wg  = _a_bas.wg;
        if (pht_wg_class.dbreadfirst_wg () != 0)
        {
            return FALSE;
        }
        return TRUE;
}

        

BOOL lsptoarr0 (int idx)
/**
Satz aus lsp in aufarr uebertragen.
**/
{
        lese_a_bas (lsp.a);   
        /*
        if (TestSysWg () == FALSE)
        {
            return 1;
        }
        */
        sprintf (aufparr[idx].pnr, "%ld", lsp.posi);
        sprintf (aufparr[idx].a, "%.0lf", lsp.a);
        strcpy (aufparr[idx].a_bz1,_a_bas.a_bz1);
        strcpy (aufparr[idx].a_bz2,_a_bas.a_bz2);
        sprintf (aufparr[idx].a_me_einh, "%hd", _a_bas.me_einh);
        sprintf (aufparr[idx].auf_me, "%.3lf", lsp.auf_me);
        sprintf (aufparr[idx].lief_me, "%.3lf", lsp.lief_me);
        sprintf (aufparr[idx].auf_pr_vk, "%.2lf", lsp.ls_vk_pr);
        sprintf (aufparr[idx].auf_lad_pr, "%.2lf", lsp.ls_lad_pr);
        sprintf (aufparr[idx].auf_me_vgl, "%.3lf", lsp.auf_me_vgl);
        sprintf (aufparr[idx].tara, "%.3lf", lsp.tara);
        strcpy (aufparr[idx].basis_me_bz, lsp.lief_me_bz);
        strcpy (aufparr[idx].auf_me_bz, lsp.auf_me_bz);
        strcpy (aufparr[idx].lief_me_bz, lsp.lief_me_bz);
        sprintf (aufparr[idx].s, "%hd", lsp.pos_stat);
        strcpy  (aufparr[idx].erf_kz, lsp.erf_kz);
        sprintf (aufparr[idx].rest, "%.3lf", lsp.auf_me_vgl - lsp.lief_me);
        strcpy  (aufparr[idx].ls_charge, lsp.ls_charge);
        clipped (aufparr[idx].ls_charge);
        sprintf (aufparr[idx].lsp_txt, "%ld", lsp.lsp_txt);
        sprintf (aufparr[idx].me_einh_kun, "%hd", lsp.me_einh_kun); 
        sprintf (aufparr[idx].me_einh, "%hd", lsp.me_einh); 
        sprintf (aufparr[idx].sa_kz_sint, "%hd", lsp.sa_kz_sint); 
        sprintf (aufparr[idx].prov_satz, "%.3lf", lsp.prov_satz);
        sprintf (aufparr[idx].leer_pos, "%hd", lsp.leer_pos);
        dlong_to_asc (lsp.hbk_date, aufparr[idx].hbk_date);
        sprintf (aufparr[idx].ls_vk_euro ,"%.2lf", lsp.ls_vk_euro);
        sprintf (aufparr[idx].ls_vk_fremd,"%.2lf", lsp.ls_vk_fremd);
        sprintf (aufparr[idx].ls_lad_euro,"%.2lf", lsp.ls_lad_euro);
        sprintf (aufparr[idx].ls_lad_fremd,"%.2lf", lsp.ls_lad_fremd);
        sprintf (aufparr[idx].rab_satz,"%.3lf", lsp.rab_satz);

        sprintf (aufparr[idx].me_einh_kun1,"%.hd", lsp.me_einh_kun1);
        sprintf (aufparr[idx].auf_me1,     "%8.3lf", lsp.auf_me1);
        sprintf (aufparr[idx].inh1,        "%8.3lf", lsp.inh1);

        sprintf (aufparr[idx].me_einh_kun2,"%.hd", lsp.me_einh_kun2);
        sprintf (aufparr[idx].auf_me2,     "%8.3lf", lsp.auf_me2);
        sprintf (aufparr[idx].inh2,        "%8.3lf", lsp.inh2);

        sprintf (aufparr[idx].me_einh_kun3,"%.hd", lsp.me_einh_kun3);
        sprintf (aufparr[idx].auf_me3,     "%8.3lf", lsp.auf_me3);
        sprintf (aufparr[idx].inh3,        "%8.3lf", lsp.inh3);

        return 0;   
}
        
BOOL lsptoarr (int idx)
/**
Satz aus lsp in aufarr uebertragen.
**/
{

        if (akt_a == lsp.a)
        {
            return 1;
        }
        akt_a = lsp.a;
        return lsptoarr0 (idx);
}


BOOL LeseLspUpd (void)
/**
Daten aus lsp in aufarr einlesen.
**/
{
        extern short sql_mode;
        int idx_sav;

        idx_sav = aufpidx;

        aufpanz = 0;
        aufpidx = 0;

        sql_mode = 1;
        dsqlstatus = ls_class.lese_lsp (1, 0, lsk.ls, "order by posi, a");
        while (dsqlstatus == 0)
        {
                   if (ls_class.lese_lsp_upd () != 0) 
                   {
                       return FALSE;
                   }
                   if (lsptoarr0 (aufpanz) != 0)
                   {
                       dsqlstatus = ls_class.lese_lsp ();
                       continue;
                   }

                   aufpanz ++;
                   if (aufpanz == AUFMAX) break; 
                   dsqlstatus = ls_class.lese_lsp ();
        }
        aufpanz_upd = aufpanz;
        aufpidx = idx_sav;
        return TRUE;
}

void LeseLsp (void)
/**
Daten aus lsp in aufarr einlesen.
**/
{
        char where [512];

        akt_a = (double) 0.0;
        aufpanz = 0;
        sprintf (where, ", lsk where lsp.ls = lsk.ls "
                        "  and lsk.komm_dat = \"%s\" "
                        "  and lsk.ls_stat < 3 "
                        "  order by a", KomDatum);
        dsqlstatus = ls_class.lese_lsp_w (where);
        while (dsqlstatus == 0)
        {
                   if (lsptoarr (aufpanz) != 0)
                   {
                       dsqlstatus = ls_class.lese_lsp_w ();
                       continue;
                   }

                   aufpanz ++;
                   if (aufpanz == AUFMAX) break; 
                   dsqlstatus = ls_class.lese_lsp_w ();
        }
        if (aufpanz  - 1 < aufpidx)
        {
            aufpidx = aufpanz - 1;
        }
}
        
void RegisterListe (void)
/**
Kopf - und Fuss - Fenster fuer Liste registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  ListKopfProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "ListKopf";
                   RegisterClass(&wc);
                   registered = 1;
        }
}


LONG FAR PASCAL ListKopfProc(HWND hWnd,UINT msg,
                          WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == eKopfWindow)
                    {
                             SetListEWindow (0);
                             display_form (eKopfWindow, &lKopfform, 0, 0);
                    }
                    if (hWnd == eFussWindow)
                    {
//                             display_form (eFussWindow, &lFussform, 0, 0);
                    }
                    if (hWnd == InsWindow)
                    {
                             SetListEWindow (0);
                             display_form (InsWindow, &insub, 0, 0);
                             display_form (InsWindow, &insform, 0, 0);
                    }
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

int GetDatPos (void)
/**
Position des Datum fuer Kopffont ermitteln.
**/
{
        TEXTMETRIC tm;
        int len;
        
        SetTextMetrics (hMainWindow, &tm, &lKopfFont);
        len = strlen (lKopfform.mask[0].feld);
        return tm.tmAveCharWidth * (len + 2);
}

void EnterListe (void)
/**
Listenerfassung
**/
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HCURSOR oldcursor;
        static long ls_alt = 0;
        HWND phWnd;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        lKopfform.mask = _lKopfform0;
        lKopfform.mask[0].length = 0;
        lKopfform.mask[1].pos[1] = GetDatPos ();

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

//        ActivateColButton (eFussWindow, &lFussform, 2, 0, 0);

        phWnd = LogoWindow;
        SetActiveWindow (phWnd);   
        SetAktivWindow (phWnd);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        LeseLsp ();
        RegisterNumEnter ();
        RegisterListe ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&testform, testform.font, &frmrect);

        x = rect.left + 5;
        y = rect.top + 60;
        cx = frmrect.right + 15; 
        cy = rect.bottom - rect.top - 200;


        SetAktivWindow (phWnd);

        set_fkt (endlist, 5);
/*
        set_fkt (doinsert, 6);
        set_fkt (Nachliefern, 7);
        set_fkt (Nichtliefern, 8);
        set_fkt (Auszeichnen, 9);
        set_fkt (SysChoise, 11);
        set_fkt (SetKomplett, 12);
*/
        SetDblClck (IsDblClck, 0);
        ListeAktiv = 1;
        SetListFont (TRUE);
        spezfont (testform.font);


        CloseControls (&lKopfform);
        eKopfWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_DLGFRAME,
                                    x, y - 35,
                                    cx + 24, 36,
                                    phWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);


        eWindow = OpenListWindowEnEx (x, y, cx, cy);

        GetWindowRect (eWindow, &rect);

        eFussWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_DLGFRAME,
                                    x, rect.bottom - 2,
                                    cx + 24, 50,
                                    phWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);

        ShowWindow (eKopfWindow, SW_SHOWNORMAL);
        ShowWindow (eFussWindow, SW_SHOWNORMAL);

        ElistVl (&testvl);
        ElistUb (&testub);
//        ActivateColButton (BackWindow3, &MainButton2, 3, 1, 1);
        ShowElist ((char *) aufparr,
                    aufpanz,
                  (char *) &aufps,
                  (int) sizeof (struct AUFP_S),
                  &testform);
        SetCursor (oldcursor);
        SetElistPos (menZeile, menSelect);
        EnterElist (eWindow, (char *) aufparr,
                             aufpanz,
                             (char *) &aufps,
                             (int) sizeof (struct AUFP_S),
                              &testform);
         if (aufpanz == 0) syskey = KEY5;
         // NewPosi ();
         CloseEWindow (eWindow);
         CloseControls (&lKopfform);
//         CloseControls (&lFussform);
         if (testub.mask[0].attribut & BUTTON)
         {
                    CloseControls (&testub); 
         }
         DestroyWindow (eFussWindow);
         DestroyWindow (eKopfWindow);
         set_fkt (NULL, 5);
         set_fkt (NULL, 6);
         set_fkt (NULL, 7);
         set_fkt (NULL, 8);
         set_fkt (NULL, 9);
         set_fkt (NULL, 11);
         set_fkt (NULL, 12);
         SetListFont (FALSE);
         ListeAktiv = 0;
         ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
         SetDblClck (NULL, 1);
         GetElistPos (&menZeile, &menSelect);
         SetMenue (&smenue);
         SethListBox (shListBox);
         Setlbox (slbox);
         set_fkt (menfunc2, 6);
}


static int aufsort ();
static int kunsort ();
static int kunnsort ();
static int datsort ();
static int kdatsort ();
static int statsort ();

static field _aufform [] =
{
        aufs.auf,          9, 1, 0, 0, 0, "%8d", READONLY, 0, 0, 0,
        aufs.kun,          9, 1, 0, 10,0, "%8d", READONLY, 0, 0, 0,      
        aufs.kun_krz1,    17, 1, 0, 21,0, "",    READONLY, 0, 0, 0,      
        aufs.lieferdat,    9, 1, 0, 40,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.komm_dat,     9, 1, 0, 49,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.ls_stat,      2, 1, 0, 58, 0, "%1d", READONLY, 0, 0, 0      
};


static form aufform = { 6, 0, 0,_aufform, 0, 0, 0, 0, &ListFont};

/*
static field _aufub [] = {
"Auftrag",            9, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0,
"Kunden-Nr",         10, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
"Kunden-Name",       17, 1, 0,21, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferdatum",       11, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0, 
"S",                  1, 1, 0,57, 0, "", DISPLAYONLY, 0, 0, 0  
};
*/

static field _aufub [] = {
"Auftrag",            9, 1, 0, 0, 0, "", BUTTON, 0, aufsort,  102,
"Kunden-Nr",         11, 1, 0, 9, 0, "", BUTTON, 0, kunsort,  103,
"Kunden-Name",       20, 1, 0,20, 0, "", BUTTON, 0, kunnsort, 104,
"L-Datum",            9, 1, 0,40, 0, "", BUTTON, 0, datsort,  105, 
"K-Datum",            9, 1, 0,49, 0, "", BUTTON, 0, kdatsort,  105, 
"S",                  2, 1, 0,58, 0, "", BUTTON, 0, statsort, 106,
" ",                  4, 1, 0,60, 0, "", BUTTON, 0, 0, 0,  
};

static form aufub = { 6, 0, 0,_aufub, 0, 0, 0, 0, &ListFont};

static field _aufvl [] =
{      "1",                  1,  0, 0,  9, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 20 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 40 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 49 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 58 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0,
};
static form aufvl = {6, 0, 0,_aufvl, 0, 0, 0, 0, &ListFont};


static int faufsort = 1;
static int fkunsort = 1;
static int fkunnsort = 1;
static int fdatsort = 1;
static int fkdatsort = 1;
static int fstatsort = 1;

static int aufsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1; 
	        struct AUF_S *el2; 

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atol(el1->auf) - atol (el2->auf)) * 
				                                  faufsort);
}


static int aufsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   aufsort0);
		  faufsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}

static int kunsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1; 
	        struct AUF_S *el2; 

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atol(el1->kun) - atol (el2->kun)) * 
		                                  fkunsort);
}

static int kunsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	      qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				    kunsort0);
		fkunsort *= -1;
            ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		return 0;
}


static int kunnsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1; 
	        struct AUF_S *el2; 

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) strcmp(el1->kun_krz1, el2->kun_krz1) * 
				                                  fkunnsort);
}

static int kunnsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   kunnsort0);
 		  fkunnsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


static int datsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1; 
	        struct AUF_S *el2; 
              long dat1;
              long dat2;   

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
              dat1 = dasc_to_long (el1->lieferdat); 
              dat2 = dasc_to_long (el2->lieferdat); 
              if (dat1 > dat2)
              {
                            return  (1 * fdatsort);  
              }
              else
              if (dat1 < dat2)
              {
                            return  (-1 * fdatsort);  
              }
              return 0;   
}

static int datsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   datsort0);
		  fdatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}
static int kdatsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1; 
	        struct AUF_S *el2; 
              long dat1;
              long dat2;   

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
              dat1 = dasc_to_long (el1->komm_dat); 
              dat2 = dasc_to_long (el2->komm_dat); 
              if (dat1 > dat2)
              {
                            return  (1 * fkdatsort);  
              }
              else
              if (dat1 < dat2)
              {
                            return  (-1 * fkdatsort);  
              }
              return 0;   
}

static int kdatsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   datsort0);
		  fkdatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


static int statsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1; 
	        struct AUF_S *el2; 

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atoi(el1->ls_stat) - atoi (el2->ls_stat)) * 
		                                  fstatsort);
}

static int statsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   statsort0);
		  fstatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        aufidx = idx;
        break_list ();
        return;
}


static int DbAuswahl (short cursor,void (*fillub) (char *),
                      void (*fillvalues) (char *))
/**
Auswahl ueber Auftraege.
**/
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        static long ls_alt = 0;
        HCURSOR oldcursor;

        aufanz = 0;
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        while (fetch_scroll (cursor, NEXT) == 0)
        {
                kun_class.lese_kun (1, 0, lsk.kun);
                sprintf (aufsarr[aufanz].auf, "%8ld", lsk.auf);
                sprintf (aufsarr[aufanz].ls,  "%8ld", lsk.ls);
                sprintf (aufsarr[aufanz].kun, "%8ld",  lsk.kun);
                dlong_to_asc (lsk.lieferdat, aufsarr[aufanz].lieferdat);
                dlong_to_asc (lsk.komm_dat, aufsarr[aufanz].komm_dat);
                strcpy  (aufsarr[aufanz].kun_krz1, kun.kun_krz1);
                sprintf (aufsarr [aufanz].ls_stat, "%1hd", lsk.ls_stat);
                aufanz ++;
                if (aufanz == AUFMAX) break;
        }

        RegisterNumEnter ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&testform, testform.font, &frmrect);

/*
        x = rect.left + 5;
        y = rect.top + 20;
        cx = frmrect.right + 15; 
        cy = rect.bottom - rect.top - 120;
*/

        set_fkt (SetKomplett, 12);

        lKopfform.mask = _lKopfform1;
        lKopfform.mask[0].length = 0;
        lKopfform.mask[1].pos[1] = GetDatPos ();

        x = rect.left + 5;
        y = rect.top + 60;
        cx = frmrect.right + 15; 
        cy = rect.bottom - rect.top - 160;

        set_fkt (endlist, 5);
        syskey = 1;
        SetDblClck (IsAwClck, 0);
        ListeAktiv = 2;
        SetAktivWindow (hMainWindow);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (testform.font);

        eKopfWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_DLGFRAME,
                                    x, y - 35,
                                    cx + 24, 36,
                                    hMainWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);

        eWindow = OpenListWindowEnEx (x, y, cx, cy);
        ShowWindow (eKopfWindow, SW_SHOWNORMAL);

        ElistVl (&aufvl);
        ElistUb (&aufub);
        ShowElist ((char *) aufsarr,
                    aufanz,
                  (char *) &aufs,
                  (int) sizeof (struct AUF_S),
                  &aufform);
        SetCursor (oldcursor);
        SetElistPos (menAufZeile, menAufSelect);
        EnterElist (eWindow, (char *) aufsarr,
                             aufanz,
                             (char *) &aufs,
                             (int) sizeof (struct AUF_S),
                             &aufform);
        if (aufub.mask[0].attribut & BUTTON)
        {
                    CloseControls (&aufub); 
        }
        CloseEWindow (eWindow);
        CloseControls (&lKopfform);
        DestroyWindow (eKopfWindow);
        set_fkt (NULL, 5);
        SetListFont (FALSE);
        ListeAktiv = 0;
        SetDblClck (NULL, 1);
        GetElistPos (&menAufZeile, &menAufSelect);
        set_fkt (NULL, 12);
        return aufidx;
}

/**

  Auftragskopf anzeigen und Eingabefelder waehlen.

**/

mfont AufKopfFontT = {"Courier New", 200, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont AufKopfFontE = {"Courier New", 200, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


mfont AufKopfFontTD = {"Courier New", 150, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont AufKopfFontED = {"Courier New", 150, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


static char KundeFiliale [14] = {"Kunden-Nr   :"};
static int ReadAuftrag (void);
static int AufAuswahl (void);
static int BreakAuswahl (void);

static field _AufKopfT[] = {
"Auftrag-Nr  :",     12, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0};

static form AufKopfT = {1, 0, 0, _AufKopfT, 0, 0, 0, 0, &AufKopfFontT};


static field _AufKopfTD[] = {
KundeFiliale,        13, 0, 4, 2, 0, "", DISPLAYONLY, 0, 0, 0,
kun_krz1,            16, 0, 4,28, 0, "", DISPLAYONLY,        0, 0, 0,
"Lieferdatum :",     13, 0, 5, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Komm.-Datum :",     13, 0, 6, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferzeit  :",     13, 0, 7, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Status      :",     13, 0, 8, 2, 0, "", DISPLAYONLY, 0, 0, 0,
 ls_stat_txt,        16, 0, 8,28, 0, "", DISPLAYONLY,        0, 0, 0
};

static form AufKopfTD = {7,0, 0, _AufKopfTD, 0, 0, 0, 0, &AufKopfFontTD};


static field _AufKopfE[] = {
  auftrag_nr,           9, 0, 1,18, 0, "%8d", EDIT,        0, ReadAuftrag, 0};

static form AufKopfE = {1, 0, 0, _AufKopfE, 0, 0, 0, 0, &AufKopfFontE};


static field _AufKopfED[] = { 
  kun_nr,              20, 0, 4,18, 0, "%8d", EDIT,        0, 0, 0,
  lieferdatum,         20, 0, 5,18, 0, "dd.mm.yyyy",
                                              EDIT,        0, 0, 0,
  komm_dat,            20, 0, 6,18, 0, "dd.mm.yyyy",
                                              EDIT,        0, 0, 0,
  lieferzeit,          20, 0, 7,18, 0, "",
                                              EDIT,        0, 0, 0,
  ls_stat,             20, 0, 8,18, 0, "",
                                              EDIT,        0, BreakAuswahl, 0
};

static form AufKopfED = {5, 0, 0, _AufKopfED, 0, 0, 0, 0, &AufKopfFontED};

static field _AufKopfQuery[] = { 
  auftrag_nr,           9, 0, 1,18, 0, "", EDIT,        0, 0, 0,
  kun_nr,               9, 0, 4,18, 0, "", EDIT,        0, 0, 0,
  lieferdatum,         11, 0, 5,18, 0, "",
                                           EDIT,        0, 0, 0,
  komm_dat,            11, 0, 6,18, 0, "",
                                           EDIT,        0, 0, 0,
  lieferzeit,           6, 0, 7,18, 0, "",
                                              EDIT,        0, 0, 0,
  ls_stat,              2, 0, 8,18, 0, "",
                                              EDIT,        0, 0, 0,
  a,                   14, 0, 9,18, 0, "", 
                                              REMOVED,        0, 0, 0,
};

static form AufKopfQuery = {7, 0, 0, _AufKopfQuery, 0, 0, 0, 0, 0};
static char *AufKopfnamen[] = {"auf", "kun", "lieferdat", "komm_dat", 
                                     "lieferzeit",
                                     "ls_stat", 
                                     "lsp.a"};

static int break_kopf_enter = 0;
static int aspos = 0;
static int asend = 4;

int BreakAuswahl (void)
/**
Auswahl ueber Auftraege.
**/
{
        break_kopf_enter = 1;
        return 0;
}


int Showlsk (char *sqlstring)
/**
Auswahl ueber Auftraege anzeigen.
**/
{
         int i;

         out_quest ((char *) &lsk.auf, 2, 0);
         out_quest ((char *) &lsk.ls, 2, 0);
         out_quest ((char *) &lsk.kun, 2, 0);
         out_quest ((char *) &lsk.lieferdat, 2, 0);
         out_quest ((char *) &lsk.komm_dat, 2, 0);
         out_quest ((char *) &lsk.ls_stat, 1, 0);
         cursor_lsk_auf_ausw = prepare_scroll (sqlstring);

//         i = DbAuswahl (cursor_lsk_auf_ausw, FillAufUb, FillAufValues);
         i = DbAuswahl (cursor_lsk_auf_ausw, NULL, NULL);

         if (syskey == KEYESC || syskey == KEY5)
         {
                     close_sql (cursor_lsk_auf_ausw);
                     return 0;
         }
         fetch_scroll (cursor_lsk_auf_ausw, DBABSOLUTE, i + 1);
         close_sql (cursor_lsk_auf_ausw);
         return 0;
}

int Auswahl_lsk_aufQuery ()
/**
Auswahl ueber lsk mit Query-String.
**/
{
         char sqlstring [0x1000];

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select auf, lsk.ls, kun, lieferdat, komm_dat,ls_stat from lsk,lsp "
                            "where auf > 0 "
                            "and lsp.ls = lsk.ls "
                            "and %s order by auf", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                           "select auf, lsk.ls, kun, lieferdat, komm_dat,ls_stat from lsk "
                           "where auf > 0 order by auf");
         }
         return Showlsk (sqlstring);
}

int AufAuswahl (void)
/**
Auswahl ueber Auftraege.
**/
{
        SetDbAwSpez (DbAuswahl);
        break_kopf_enter = 1;
        ReadQuery (&AufKopfQuery, AufKopfnamen);
//        if (ls_class.Auswahl_lsk_aufQuery () == 0 &&
//            syskey != KEY5)
        if (Auswahl_lsk_aufQuery () == 0 &&
            syskey != KEY5)
        {
                     syskey = KEYCR;
                     sprintf (auftrag_nr, "%ld", lsk.auf);
                     if (aufanz)
                     {
                               LockAuf = 0;
                               ReadAuftrag ();
                               LockAuf = 1;
                     }
                     else
                     {
                               InitAufKopf ();
                     }
                     SetDbAwSpez (NULL);
                     return 0;
        }
        InitAufKopf ();
        SetDbAwSpez (NULL);
        return 0;
}

void FillAufForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
             kun_class.lese_kun (1, 0, lsk.kun);
             sprintf (kun_nr, "%ld", lsk.kun);
             dlong_to_asc (lsk.lieferdat, lieferdatum);
             dlong_to_asc (lsk.komm_dat, komm_dat);
             strcpy (lieferzeit, lsk.lieferzeit);
             sprintf (ls_stat, "%hd", lsk.ls_stat);
             strcpy (kun_krz1, kun.kun_krz1);
             ptab_class.lese_ptab ("ls_stat", ls_stat);
             strcpy (ls_stat_txt, ptabn.ptbezk);
//             display_form (AufKopfWindow, &AufKopfED, 0, 0);
}

BOOL Lock_Lsk (void)
/**
Lieferschein sperren.
**/
{
    short sqlm;
    extern short sql_mode;

    if (LockAuf == 0) 
        return TRUE;
    sqlm = sql_mode;
    sql_mode = 1;
    dsqlstatus = ls_class.lock_lsk (1, 0, lsk.ls);
    if (dsqlstatus < 0)
    {
        if (atol (auftrag_nr))
        {
               print_messG (2, "Auftrag %ld ist gesperrt",
                           atol (auftrag_nr));
        }
        else
        {
               print_messG (2, "Lieferschein %ld ist gesperrt",
                               lsk.ls);
        }
        sql_mode = sqlm;
        AufLock = 1;
        ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
        return FALSE;
    }
    AufLock = 0;
    ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
    sql_mode = sqlm;
    return TRUE;
}

BOOL Lock_Lsp (void)
/**
Lieferschein sperren.
**/
{
    short sqlm;
    extern short sql_mode;

    sqlm = sql_mode;
    sql_mode = 1;
    dsqlstatus = ls_class.lock_lsp (1, 0, lsp.ls, ratod (aufps.a), 
                                                  atol (aufps.pnr));
    if (dsqlstatus < 0)
    {
               print_messG (2, "Position wird von einem anderen "
                               "Benutzer bearbeitet");
               return FALSE;
    }
    sql_mode = sqlm;
    return TRUE;
}


int ReadAuftrag (void)
/**
Lieferschein nach Auftragsnummer lesen.
**/
{
            if (atol (auftrag_nr) <= 0l) return (-1);

            dsqlstatus = ls_class.lese_lsk_auf (1, 0, atol (auftrag_nr));
            if (dsqlstatus)
            {
                   ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
                   set_fkt (NULL, 6);
                   print_messG (0, "Auftrag %s nicht gefunden", auftrag_nr);
                   return -1;
            }

//            if (Lock_Lsk () == FALSE) return 0;

            ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
            set_fkt (menfunc2, 6);
            FillAufForm ();
            return 0;
}
         

void KonvTextPos (mfont *Font1,  mfont *Font2, int *z, int *s)
/**
Textposition von einer Schriftgroesse zur anderen konvertieren.
**/
{
         TEXTMETRIC tm1, tm2;
         HDC hdc;
         HFONT hFont, oldfont;
         int ax, ay;

         spezfont (Font1);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm1);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         spezfont (Font2);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm2);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         ax = *s;
         ay = *z;

         ax = ax * tm1.tmAveCharWidth / tm2.tmAveCharWidth;
         ay = ay * tm1.tmHeight / tm2.tmHeight;

         *s = ax;
         *z = ay;
}

void KonvTextAbsPos (mfont *Font, int *cy, int *cx)
/**
Absolute Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);
         *cx *= tm.tmAveCharWidth;
         *cy *= tm.tmHeight;
}

void GetTextPos (mfont *Font, int *cy, int *cx)
/**
Zeichenorientierte Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         *cx /= tm.tmAveCharWidth;
         *cy /= tm.tmHeight;
}



void FontTextLen (mfont *Font, char *text, int *cx, int *cy)
/**
Laenge und Hoehe eines Textes fuer den gewaehlten Font holen.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;
         SIZE size;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         GetTextExtentPoint (hdc, text, *cx, &size);
         DeleteObject (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
         DeleteObject (hFont);

         *cx = size.cx;
         *cy = tm.tmHeight;
}

void SetAufFocus (int pos)
/**
Focus auf Eingabefelder AuftragKopfsetzen.
**/
{
        switch (pos)
        {
               case 0 :
                      SetFocus (AufKopfE.mask[0].feldid);
                      SendMessage (AufKopfE.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               default :
                      SetFocus (AufKopfED.mask[pos - 1].feldid);
                      SendMessage (AufKopfED.mask[pos -1].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
        }
}

void PrintKLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
         RECT rect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         GetClientRect (AufKopfWindow, &rect);

         hdc = BeginPaint (AufKopfWindow, &ps);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 10;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);


         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 100;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 300;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}

static int NoDisplay;


LONG FAR PASCAL AufKopfProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (LiefEnterAktiv)
                    {
                                DisplayLiefKopf (); 
                                PrintKLines ();
                                if (MessWindow == 0)
                                {
                                        LiefFocus ();
                                }
                    }
                    else if (KopfEnterAktiv)
                    {
                                display_form (AufKopfWindow, &AufKopfT, 0, 0);
                                display_form (AufKopfWindow, &AufKopfTD, 0, 0);
                                if (AufKopfE.mask[0].feldid)
                                {
                                      display_form (AufKopfWindow, &AufKopfE, 0, 0);
                                }
                                if (AufKopfE.mask[0].feldid || asend == 0)
                                {
                                      display_form (AufKopfWindow, &AufKopfED, 0, 0);
                                }
                                PrintKLines ();
                                if (MessWindow == 0)
                                {
                                         SetAufFocus (aspos);
                                }
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterKopfEnter (void)
/**
Fenster fuer Auftragskopf registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  AufKopfProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
                   wc.lpszClassName =  "AufKopf";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

void GetEditText (void)
/**
Text aus Editierfeld holen.
**/
{
        switch (aspos)
        {
                 case 0 :
                    GetWindowText (AufKopfE.mask [0].feldid,
                                   AufKopfE.mask [0].feld,
                                   AufKopfE.mask[0].length);
                    break;
                 case 1 :
                    GetWindowText (AufKopfED.mask [0].feldid,
                                   AufKopfED.mask [0].feld,
                                   AufKopfED.mask[0].length);
                    break;
                 case 2 :
                    GetWindowText (AufKopfED.mask [1].feldid,
                                   AufKopfED.mask [1].feld,
                                   AufKopfED.mask[1].length);
                    break;
                 case 3 :
                    GetWindowText (AufKopfED.mask [2].feldid,
                                   AufKopfED.mask [2].feld,
                                   AufKopfED.mask[2].length);
                    break;
                 case 4 :
                    GetWindowText (AufKopfED.mask [3].feldid,
                                   AufKopfED.mask [3].feld,
                                   AufKopfED.mask[3].length);

                    break;
        }
}

int MenKey (int taste)
{
    int i;
    ColButton *ColBut;
    char *pos;

    for (i = 0; i < MainButton.fieldanz; i ++)
    {
           ColBut = (ColButton *) MainButton.mask[i].feld;
           if (ColBut->aktivate < 0) continue;
           if (ColBut->text1)
           {
                 if ((pos = strchr (ColBut->text1, '&')) &&
                    ((UCHAR) toupper (*(pos + 1)) == taste))
                 {
                             if (MainButton.mask[i].after)
                             {
                                 (*MainButton.mask[i].after) ();
                             }
                             return TRUE;
                  }
           }
    }
    return FALSE;
}

int IsKopfMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;
         
         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    break_kopf_enter = 1;
                                    return TRUE;
                            case VK_F6 :
                                    syskey = KEY6;
                                    testkeys ();
                                    return TRUE;
                            case VK_F12 :
                                    syskey = KEY12;
                                    testkeys ();
                                    return TRUE;
                            case VK_DOWN :
                            case VK_RETURN :
                                    GetEditText ();
                                    if (asend == 0 && AufKopfE.mask[0].after)
                                    {
                                         if ((*AufKopfE.mask[0].after) () == -1)
                                         {
                                                  return TRUE;
                                         }
                                    }
                                    if (aspos > 0 &&
                                        AufKopfED.mask [aspos - 1].after)
                                    {
                                      (*AufKopfED.mask [aspos - 1].after) ();
                                    }
                                    aspos ++;
                                    if (aspos > asend) aspos = 0;
                                    display_form (AufKopfWindow,
                                                  &AufKopfE, 0, 0);
                                    display_form (AufKopfWindow,
                                                  &AufKopfED, 0, 0);
                                    SetAufFocus (aspos);
                                    return TRUE;
                            case VK_UP :
                                    GetEditText ();
                                    aspos --;
                                    if (aspos < 0) aspos = asend;
                                    display_form (AufKopfWindow,
                                                  &AufKopfE, 0, 0);
                                    display_form (AufKopfWindow,
                                                  &AufKopfED, 0, 0);
                                    SetAufFocus (aspos);
                                    return TRUE;
                            case VK_TAB :
                                    GetEditText ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                            aspos --;
                                            if (aspos < 0) aspos = 4;
                                            SetAufFocus (aspos);
                                     }
                                     else
                                     {
                                            aspos ++;
                                            if (aspos > 4) aspos = 0;
                                            SetAufFocus (aspos);
                                     }
                                     display_form (AufKopfWindow,
                                                 &AufKopfE, 0, 0);
                                     display_form (AufKopfWindow,
                                                 &AufKopfED, 0, 0);
                                      return TRUE;
                            default :
                                    return MenKey (msg->wParam);   
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (AufKopfE.mask[0].feldid, &mousepos))
                    {
                          EnterNumBox (AufKopfWindow, "  Auftrags-Nr  ",
                                       auftrag_nr, 9, AufKopfE.mask[0].picture);
                          if (asend == 0 && AufKopfE.mask[0].after)
                          {
                                if ((*AufKopfE.mask[0].after) () == -1)
                                {
                                       return TRUE;
                                }
                          }
                          display_form (AufKopfWindow, &AufKopfE, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[0].feldid, &mousepos))
                    {
                          if (asend == 0) return TRUE;
                          EnterNumBox (AufKopfWindow,"  Kunden-Nr  ",
                                          kun_nr, 9, AufKopfED.mask[0].picture);
                          display_form (AufKopfWindow, &AufKopfE, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[1].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Lieferdatum  ",
                                      lieferdatum, 11,
//                                      "dd.mm.yyyy");
                                            AufKopfED.mask[1].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[2].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Komm.-Datum  ",
                                      komm_dat   , 11,
//                                      "dd.mm.yyyy");
                                          AufKopfED.mask[2].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[3].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Lieferzeit  ",
                                      lieferzeit, 6,
                                            AufKopfED.mask[3].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[4].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Status  ",
                                         ls_stat,  2,
                                            AufKopfED.mask[4].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
                         return TRUE;
                    }
             }
          }

          return FALSE;
}

void KonvAufKopf (void)
/**
Spaltenposition von AufKopfED anpassen.
**/
{
        static int KonvOK = 0;
        int z, s;
        int i;

        if (KonvOK) return;

        KonvOK = 1;
        for (i = 0; i < AufKopfED.fieldanz; i ++)
        {
                    s = AufKopfED.mask[i].pos [1];
                    KonvTextPos (AufKopfE.font,
                                 AufKopfED.font,
                                 &z, &s);
                    AufKopfED.mask[i].pos [1] = s;
        }

        s = AufKopfTD.mask[1].pos [1];
        KonvTextPos (AufKopfE.font,
                    AufKopfED.font,
                    &z, &s);
        AufKopfTD.mask[1].pos [1] = s;

        s = AufKopfTD.mask[5].pos [1];
        KonvTextPos (AufKopfE.font,
                    AufKopfED.font,
                    &z, &s);
        AufKopfTD.mask[5].pos [1] = s;
}

void InitAufKopf (void)
/**
Auftrag-Kopf initialisieren.
**/
{
        int i;

        AufKopfE.mask[0].feld[0] = (char) 0;

        for (i = 0; i < AufKopfED.fieldanz; i ++)
        {
                      AufKopfED.mask[i].feld[0] = (char) 0;
        }
        kun_krz1[0] = (char) 0;
        ls_stat_txt[0] = (char) 0;
}


void MainBuInactiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{         
          ActivateColButton (BackWindow2, &MainButton, 0, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 3, -1, 1);
/*
          ActivateColButton (BackWindow2, &MainButton, 5, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 6, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
*/
}

void MainBuActiv (void)
/**
Alle Button auf aktiv setzen.
**/
{         
          ActivateColButton (BackWindow2, &MainButton, 0, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 1, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 3, 1, 1);
/*
          ActivateColButton (BackWindow2, &MainButton, 5, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 6, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
          set_fkt (NULL, 6);
*/
}


void AuftragKopf (int eType)
/**
Auftragskopf bearbeiten.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;

        if (KopfEnterAktiv) return;
        if (LiefEnterAktiv) return;

        MainBuInactiv ();

        if (eType == ENTER)
        {
            beginwork ();
        }
        else 
        {
                   InitAufKopf ();
        }

        if (atoi (auftrag_nr))
        {
                  if (ReadAuftrag () == -1) return;;
        }
        CloseLiefKopf ();
        if (eType == AUSWAHL)
        {
                     KopfEnterAktiv = 2;
                     asend = 5;
//                     InitAufKopf ();
                     AufKopfE.mask[0].picture = "";
                     AufKopfED.mask[0].picture = "";
                     AufKopfED.mask[1].picture = "";
                     AufKopfED.mask[2].picture = "";

                     AufKopfED.mask[0].attribut = EDIT;
                     AufKopfED.mask[1].attribut = EDIT;
                     AufKopfED.mask[2].attribut = EDIT;
                     AufKopfED.mask[3].attribut = EDIT;
                     AufKopfED.mask[4].attribut = EDIT;
                     AufKopfTD.mask[1].attribut = REMOVED;
                     AufKopfTD.mask[6].attribut = REMOVED;

                     AufKopfE.mask[0].length = 20;
                     AufKopfED.mask[0].length = 20;
                     AufKopfED.mask[1].length = 20;
                     AufKopfED.mask[2].length = 20;
                     AufKopfED.mask[3].length = 20;
                     AufKopfED.mask[4].length = 20;
                     strcpy (ls_stat, "< 3");
                     strcpy (komm_dat, KomDatum);
                     strcpy (a, aufps.a);
                     AufAuswahl ();
                     KopfEnterAktiv = 0;
                     MainBuActiv ();
                     return;
        }

        if (eType == ENTER)
        {
                     asend = 0;
                     AufKopfE.mask[0].picture  = "%8d";
                     AufKopfED.mask[0].picture = "%8d";
                     AufKopfED.mask[1].picture = "dd.mm.yyyy";
                     AufKopfED.mask[2].picture = "dd.mm.yyyy";

                     AufKopfED.mask[0].attribut = READONLY;
                     AufKopfED.mask[1].attribut = READONLY;
                     AufKopfED.mask[2].attribut = READONLY;
                     AufKopfED.mask[3].attribut = READONLY;
                     AufKopfED.mask[4].attribut = READONLY;
                     AufKopfTD.mask[1].attribut = DISPLAYONLY;
                     AufKopfTD.mask[6].attribut = DISPLAYONLY;

                     AufKopfE.mask[0].length = 9;
                     AufKopfED.mask[0].length = 9;
                     AufKopfED.mask[1].length = 11;
                     AufKopfED.mask[2].length = 11;
                     AufKopfED.mask[3].length = 6;
                     AufKopfED.mask[4].length = 2;
        }
        else if (eType == SUCHEN)
        {
                     asend = 5;
                     set_fkt (BreakAuswahl, 12);
                     InitAufKopf ();
                     AufKopfE.mask[0].picture = "";
                     AufKopfED.mask[0].picture = "";
                     AufKopfED.mask[1].picture = "";
                     AufKopfED.mask[2].picture = "";

                     AufKopfED.mask[0].attribut = EDIT;
                     AufKopfED.mask[1].attribut = EDIT;
                     AufKopfED.mask[2].attribut = EDIT;
                     AufKopfED.mask[3].attribut = EDIT;
                     AufKopfED.mask[4].attribut = EDIT;
                     AufKopfTD.mask[1].attribut = REMOVED;
                     AufKopfTD.mask[6].attribut = REMOVED;

                     AufKopfE.mask[0].length = 20;
                     AufKopfED.mask[0].length = 20;
                     AufKopfED.mask[1].length = 20;
                     AufKopfED.mask[2].length = 20;
                     AufKopfED.mask[3].length = 20;
                     AufKopfED.mask[4].length = 20;
        }

        aspos = 0;
        RegisterKopfEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = rect.top;
        cx = rect.right - rect.left; 
        cy = rect.bottom - rect.top;

        KonvAufKopf ();

        KopfEnterAktiv = 1;
        if (AufKopfWindow == 0)
        {   
                  AufKopfWindow  = CreateWindowEx (
                              0,
                              "AufKopf",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
        }

        if (AufKopfWindow == NULL)
        {
                    KopfEnterAktiv = 0;
                    MainBuActiv ();
                    return;
        }
        ShowWindow (AufKopfWindow, SW_SHOW);
        UpdateWindow (AufKopfWindow);
        create_enter_form (AufKopfWindow, &AufKopfE, 0, 0);
        if (asend > 0)
        {
                  create_enter_form (AufKopfWindow, &AufKopfED, 0, 0);
        }
        if (eType == ENTER)
        {
               set_fkt (menfunc2, 6);
        }
        SetAufFocus (aspos);
        break_kopf_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsKopfMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_kopf_enter) break;
        }
        CloseControls (&AufKopfT);
        CloseControls (&AufKopfE);
        CloseControls (&AufKopfTD);
        CloseControls (&AufKopfED);
        if (eType == ENTER)
        {
               set_fkt (NULL, 6);
        }

        if (eType == ENTER)
        {
                     DestroyWindow (AufKopfWindow);
                     AufKopfWindow = NULL;
                     commitwork ();
//                     InitAufKopf ();
        }

        if (eType == SUCHEN)
        { 
                     set_fkt (NULL, 12);
                     DestroyWindow (AufKopfWindow);
                     AufKopfWindow = NULL;
                     AufAuswahl ();
        }
        KopfEnterAktiv = 0;
        MainBuActiv ();
}

/**

Wiegen

**/


#define BUTARA  502
static int break_wieg = 0;

HWND WiegKg;
HWND WiegCtr;

static char WiegText [30] = {"Kiloware"};
static char WiegMe [12] = {"14.150"};


mfont WiegKgFont  = {"Courier New", 300, 0, 1, BLACKCOL,
                                               WHITECOL,
                                               1,
                                               NULL};

mfont WiegKgFontMe = {"Courier New", 300, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                1,
                                                NULL};


mfont WiegKgFontT  = {"Courier New", 150, 0, 1, WHITECOL,
                                                BLUECOL,
                                                1,
                                                NULL};

mfont WiegBuFontE  = {"Courier New", 200, 0, 1, BLACKCOL,
                                                REDCOL,
                                                1,
                                                NULL};

mfont WiegBuFontW  = {"Courier New", 150, 0, 1, BLACKCOL,
                                                GREENCOL,
                                                1,
                                                NULL};

ColButton WieOK = { "OK", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     GREENCOL,
                     2};


ColButton WieEnde = {"Zur�ck", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     REDCOL,
                     2};

ColButton WieWie = {"Wiegen", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     GREENCOL,
                     2};

ColButton WieHand = {"Hand", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      BLACKCOL,
                      YELLOWCOL,
                      2};

ColButton WiePreis = {"Preise", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      BLACKCOL,
                      YELLOWCOL,
                      2};

ColButton WieTara = {"Tara", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      // BLACKCOL,
                      // RGB (0, 255, 255),
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton AutoTara = {"Auto-", -1,15,
                      "Tara ", -1,40,
                      "aus",   -1,65,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       // BLACKCOL,
                       // RGB (0, 255, 255),
                       WHITECOL,
                       RGB (0, 0, 255),
                       3};

ColButton DelTara = { "Tara   ", -1, 25,
                      "L�schen", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton FestTara = {"Fest", -1, 25,
                      "Tara", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};


ColButton SetTara = { "Tara  ", -1, 15,
                      "�ber- ", -1, 40,
                      "nehmen", -1, 65,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton HandTara = {"Hand", -1, 25,
                      "Tara", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};


static int current_wieg;
static int CtrMode;
static int wiegend = 6;
static int dowiegen (void);
static int doWOK (void);
static int dohand (void);
static int dotara (void);
static int dogebinde (void);
static int dopreis (void);
static int doauto (void);
static int deltara (void);
static int handtara (void);
static int festtara (void);
static int settara (void);
static int closetara (void);
static int autoan = 0;
static int autopos = 1;
static char *anaus[] = {"aus", "ein"};
static char chargen_nr [18];
static int WiegY;
static double akt_tara = (double) 0.0;

static int bsize = 100;
static int babs = 100;

static field _WieButton[] = {
(char *) &WieTara,          bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      dotara, BUTARA,
(char *) &WiePreis,         bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      dopreis, 0,
(char *) &WieHand,          bsize, bsize, -1, 15, 0, "", COLBUTTON, 0,
                                                      dohand, 0,
(char *) &WieWie,           bsize, bsize, -1, 145, 0, "", COLBUTTON, 0,
                                                      dowiegen, 0,
(char *) &WieOK,            bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      doWOK, 0,
(char *) &WieEnde,          bsize, bsize, -1, 405, 0, "", COLBUTTON, 0,
                                                      0, VK_F5};

static form WieButton = {6, 0, 0, _WieButton, 0, 0, 0, 0, &WiegBuFontW};

static int bsize2 = 100;
static int babs2 = 100;

static field _WieButtonT[] = {
(char *) &AutoTara,         bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      doauto, 0,
(char *) &DelTara,          bsize2, bsize2, -1, 145, 0, "", COLBUTTON, 0,
                                                      deltara, 0,
(char *) &FestTara,         bsize2, bsize2, -1, 15, 0, "", COLBUTTON, 0,
                                                      festtara, 0,
(char *) &HandTara,         bsize2, bsize2, -1, 405, 0, "", COLBUTTON, 0,
                                                      handtara, 0,
(char *) &SetTara,          bsize2, bsize2, -1, 15, 0, "", COLBUTTON, 0,
                                                      settara, 0,
(char *) &WieEnde,          bsize2, bsize2, -1, 405, 0, "", COLBUTTON, 0,
                                                      0, VK_F5};

static form WieButtonT = {6, 0, 0, _WieButtonT, 0, 0, 0, 0, &WiegBuFontW};

static field _WiegKgT [] = {
WiegText,           0, 0, 10, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form WiegKgT = {1, 0, 0, _WiegKgT, 0, 0, 0, 0, &WiegKgFontT};

static field _WiegKgE [] = {
WiegMe,             0, 0, 0, -1, 0, "%11.3f", READONLY, 0, 0, 0};

static form WiegKgE = {1, 0, 0, _WiegKgE, 0, 0, 0, 0, &WiegKgFont};

static field _WiegKgMe [] = {
"KG",                   10, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};

static form WiegKgMe = {1, 0, 0, _WiegKgMe, 0, 0, 0, 0, &WiegKgFontMe};


mfont ArtWFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      1,
                                      NULL};
mfont ArtWFontT = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                        RGB (0, 255, 255),
                                        1,
                                        NULL};

/*
mfont ArtWCFont = {"Courier New", 100, 0, 0, RGB (0, 0, 0),
                                      RGB (0, 255, 255),
                                      1,
                                      NULL};
                                      */

mfont ArtWCFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      1,
                                      NULL};

mfont ArtWCFontT = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                        RGB (0, 255, 255),
                                        1,
                                        NULL};

mfont ftarafont  = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                              RGB (255, 255, 255),
                                              1,
                                              NULL};

mfont ftaratfont  = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                               RGB (0, 255, 255),
                                               1,
                                               NULL};

static field _artwconstT [] =
{
"Artikel",                 9, 1, 3, 3,  0, "", DISPLAYONLY,    0, 0, 0,         
"Artikelbezeichnung",     18, 1, 3,14,  0, "", DISPLAYONLY,    0, 0, 0,         
"Auftrag",                 7, 1, 1, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Kunde",                   7, 1, 1,14,  0, "", DISPLAYONLY,    0, 0, 0};

static form artwconstT = {4, 0, 0,_artwconstT, 0, 0, 0, 0, &ArtWCFontT};


static char summe1 [40];
static char summe2 [40];
static char summe3 [40];
static char zsumme1 [40];
static char zsumme2 [40];
static char zsumme3 [40];

static char vorgabe1[40] = {"100"};
static char veinh1[10] = {"kg"};
static char vorgabe2[40] = {"100"};
static char veinh2[10] = {"kg"};

static field _artwconst [] =
{
        aufps.a,            9, 1,  4, 3,  0, "%8.0f",  READONLY, 0, 0, 0,
        aufps.a_bz1,       21, 1,  4, 14, 0, "",       READONLY, 0, 0, 0,
        auftrag_nr,         9, 1,  2,  3, 0, "%8d",    READONLY , 0, 0, 0,
        kun_krz1,          21, 1,  2, 14, 0, "",       READONLY , 0, 0, 0
};

/*
static field _artwconst [] =
{
        aufps.a,            9, 0, 4, 3,  0, "%8.0f",  DISPLAYONLY, 0, 0, 0,
        aufps.a_bz1,       21, 0, 4, 14, 0, "",       DISPLAYONLY, 0, 0, 0,
        auftrag_nr,        9, 0,  2,  3, 0, "%8f",    DISPLAYONLY, 0, 0, 0,
        kun_krz1,         17, 0,  2, 14, 0, "",       DISPLAYONLY, 0, 0, 0
};
*/

static form artwconst = {4, 0, 0,_artwconst, 0, 0, 0, 0, &ArtWCFont};


/*
static field _artwformT [] =
{
"Chargen-Nr",             10, 1, 5, 3,  0, "", DISPLAYONLY,    0, 0, 0,         
"Restmenge",              10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Liefermenge",            11, 1, 5,37,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,37,  0, "", DISPLAYONLY,    0, 0, 0, 
"Auftragsmenge",          13, 1, 5,23,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7,23,  0, "", DISPLAYONLY,    0, 0, 0, 
"Sollmenge",              11, 1, 3,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Summe1",                  6, 1, 3,49,  0, "", DISPLAYONLY,    0, 0, 0,              
"Summe2",                  6, 1, 5,49,  0, "", DISPLAYONLY,    0, 0, 0,              
};

static form artwformT = {9, 0, 0,_artwformT, 0, 0, 0, 0, &ArtWFontT};

static field _artwform [] =
{
        aufps.ls_charge,   19, 0, 6,  3, 0, "",       EDIT,     0, 0, 0,  
        auf_me,            10, 1, 2, 37, 0, "%9.3f",  READONLY , 0, 0, 0,
        aufps.lief_me,     10, 1, 6, 37, 0, "%9.3f",  READONLY, 0, 0, 0,      
        aufps.auf_me,      12, 1, 6, 23, 0, "%11.3f", READONLY, 0, 0, 0,      
        aufps.auf_me_vgl,  10, 1, 4, 37, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe1,            10, 1, 4, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe2,            10, 1, 6, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        zsumme1,            4, 1, 4, 61, 0, "%3d",    READONLY, 0, 0, 0,
        zsumme2,            4, 1, 6, 61, 0, "%3d",    READONLY, 0, 0, 0,
};

static form artwform = {9, 0, 0,_artwform, 0, 0, 0, 0, &ArtWFont};
*/

static form artwformT;
static form artwform;


// Masken fuer Wiegen

static field _artwformTW [] =
{
"Chargen-Nr",             10, 1, 5, 3,  0, "", DISPLAYONLY,    0, 0, 0,         
"Restmenge",              10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Liefermenge",            11, 1, 5,37,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,37,  0, "", DISPLAYONLY,    0, 0, 0, 
"Auftragsmenge",          13, 1, 5,23,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7,23,  0, "", DISPLAYONLY,    0, 0, 0, 
"Sollmenge",              11, 1, 3,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Summe1",                  6, 1, 3,49,  0, "", REMOVED,    0, 0, 0,              
"Summe2",                  6, 1, 5,49,  0, "", REMOVED,    0, 0, 0,              
};

static form artwformTW = {9, 0, 0,_artwformTW, 0, 0, 0, 0, &ArtWFontT};


static field _artwformW [] =
{
        aufps.ls_charge,   19, 0, 6,  3, 0, "",       EDIT,     0, 0, 0,  
        auf_me,            10, 1, 2, 37, 0, "%9.3f",  READONLY , 0, 0, 0,
        aufps.lief_me,     10, 1, 6, 37, 0, "%9.3f",  READONLY, 0, 0, 0,      
        aufps.auf_me,      12, 1, 6, 23, 0, "%11.3f", READONLY, 0, 0, 0,      
        aufps.auf_me_vgl,  10, 1, 4, 37, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe1,            10, 1, 4, 49, 0, "%9.3f",  REMOVED, 0, 0, 0,
        summe2,            10, 1, 6, 49, 0, "%9.3f",  REMOVED, 0, 0, 0,
        zsumme1,            4, 1, 4, 61, 0, "%3d",    REMOVED, 0, 0, 0,
        zsumme2,            4, 1, 6, 61, 0, "%3d",    REMOVED, 0, 0, 0,
};

static form artwformW = {9, 0, 0,_artwformW, 0, 0, 0, 0, &ArtWFont};


// Masken fuer Auszeichnung

static field _artwformTP [] =
{
"Chargen-Nr",             10, 1, 5, 3,  0, "", DISPLAYONLY,    0, 0, 0,         
"Vorgaben",                10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
veinh2,                    9, 1, 5,37,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,37,  0, "", REMOVED,        0, 0, 0, 
"Liefermenge",            13, 1, 5,23,  0, "", DISPLAYONLY,        0, 0, 0,
aufps.auf_me_bz,          10, 1, 7,37,  0, "", DISPLAYONLY,    0, 0, 0, 
veinh1,                    9, 1, 3,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Summen",                  6, 1, 1,49,  0, "", DISPLAYONLY,    0, 0, 0,              
"Summe2",                  6, 1, 3,49,  0, "", REMOVED,    0, 0, 0,              
"Summe3",                  6, 1, 5,49,  0, "", REMOVED,    0, 0, 0,              
"kg",                      6, 1, 7,49,  0, "", REMOVED,    0, 0, 0, 
"St�ck",                   7, 1, 3, 61, 0, "", DISPLAYONLY , 0, 0, 0,
"Karton",                  7, 1, 5, 61, 0, "", DISPLAYONLY , 0, 0, 0,
"Palette",                 7, 1, 7, 61, 0, "", DISPLAYONLY , 0, 0, 0,
veinh1,                    9, 1, 3, 49,  0, "", DISPLAYONLY,    0, 0, 0,
veinh2,                    9, 1, 5, 49,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7, 49,  0, "", DISPLAYONLY,    0, 0, 0, 
};

static form artwformTP = {17, 0, 0,_artwformTP, 0, 0, 0, 0, &ArtWFontT};


static field _artwformP [] =
{
        aufps.ls_charge,   19, 0, 6,  3, 0, "",       EDIT,     0, 0, 0,  
        vorgabe1,          12, 1, 2, 37, 0, "%11.3f", READONLY, 0, 0, 0,
        aufps.lief_me,     10, 1, 6, 23, 0, "%9.3f",  READONLY,  0, 0, 0,      
        aufps.auf_me,      12, 1, 6, 37, 0, "%11.3f", READONLY, 0, 0, 0,      
        vorgabe2,          12, 1, 4, 37, 0, "%11.3f",  READONLY, 0, 0, 0,
        summe1,            10, 1, 2, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe2,            10, 1, 4, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe3,            10, 1, 6, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        zsumme1,            4, 1, 2, 61, 0, "%3d",    READONLY, 0, 0, 0,
        zsumme2,            4, 1, 4, 61, 0, "%3d",    READONLY, 0, 0, 0,
        zsumme3,            4, 1, 6, 61, 0, "%3d",    READONLY, 0, 0, 0,
};

static form artwformP = {11, 0, 0,_artwformP, 0, 0, 0, 0, &ArtWFont};

// Ende Maske fuer Preisauszeichnung

static field _ftaratxt [] =
{
"Tara ",                   5, 1,  8, 13, 0, "",  DISPLAYONLY, 0, 0, 0
};

static form ftaratxt = {1, 0, 0, _ftaratxt, 0, 0, 0, 0, &ftaratfont};

static field _ftara [] =
{
        aufps.tara,        8, 1,  8, 20, 0, "%7.3f",  READONLY, 0, 0, 0
};

static form ftara = {1, 0, 0, _ftara, 0, 0, 0, 0, &ftarafont};


void SwitchLiefAuf (void)
/**
Text fuer Autrags-Nr oder Lieferschein belegen.
**/
{
         if (LiefEnterAktiv)
         {
                   artwconstT.mask[2].feld   = "Lieferschein";
                   artwconstT.mask[2].length = 0;
                   artwconst.mask[2].feld    = lief_nr;
         }
         else
         {
                   artwconstT.mask[2].feld   = "Auftrag";
                   artwconstT.mask[2].length = 0;
                   artwconst.mask[2].feld    = auftrag_nr;
         }
}



int GetGewicht (char *datei, double *brutto, double *netto, double *tara)
/**
Gewichtswerte aus der Datei ergebnis holen.
**/
{
         FILE *fp;
         char buffer [81];

         fp = fopen (datei, "r");
         if (fp == NULL) return (-1);

/*
         if (testmode)
         {
                    disp_messG ("ergebnis ge�ffnet", 1);
         } 
*/

         while (fgets (buffer, 80, fp))
         {
/*
               if (testmode)
               {
                     print_messG (1, "gelesen %s", buffer);
               } 
*/
               switch (buffer [0])
               {
                        case 'B' :
                                *brutto = (double) ratod (&buffer[1]);
                                break;
                        case 'N' :
                                *netto = (double) ratod (&buffer[1]);
                                break;
                        case 'T' :
                                *tara = (double) ratod (&buffer[1]);
                                break;
               }
         }
         fclose (fp);
                     
         if (*netto == (double) 0)
         {
                        *netto = *brutto - *tara;
         }
         else if (*brutto == (double) 0)
         {
                        *brutto = *netto + *tara;
         }
         if (autoan)
         {
                       akt_tara = *brutto;
         }
         else
         {
                       akt_tara = *tara;
         }
/*
         if (testmode)
         {
                    print_messG (1,"%.2lf  %.2lf %-2lf", *brutto, *netto, *tara);
         } 
*/
         return 0;
}

void fnbexec (int arganz, char *argv[])
/**
**/
{
        char aufruf [256];
        int i;

        if (getenv (BWS) == NULL) return;

        sprintf (aufruf, "%s\\fnb.exe", waadir);

        for (i = 1; i < arganz; i ++)
        {
                     strcat (aufruf, " ");
                     strcat (aufruf, argv[i]);
        }

        WaitConsoleExec (aufruf, CREATE_NO_WINDOW);
}

BOOL WaaInitOk (char *wa)
/**
Test, ob die Waage schon initialisiert wurde.
**/
{
        static char *waadirs [20];
        static int waaz = 0;
        int i;

        if (waaz == 20) return FALSE; 
        clipped (wa);
        for (i = 0; i < waaz; i ++)
        {
            if (strcmp (wa, waadirs [i]) == 0)
            {
                return TRUE;
            }
        }
        waadirs [i] = (char *) malloc (strlen (wa) + 3);
        if (waadirs [i] == NULL)
        {
            return FALSE;
        }
        strcpy (waadirs[i], wa);
        if (waaz < 20) waaz ++;
        return FALSE;
}



void WaageInit (void)
/**
Waage Initialisieren.
**/
{
        static int arganz;
        static char *args[4];


//        if (WaaInit) return;

        if (WaaInitOk (waadir)) return;

        WaaInit = 1;
        arganz = 3;
        args[0] = "Leer";
        args[1] = "0";
        args[2] = waadir;
        fnbexec (arganz, args);
}

int doWOK (void)
/**
Wiegen durchfuehren.
**/
{
        strcpy (aufps.s, "3");
        break_wieg = 1;
        return 0;
}


int doauto (void)
/**
Wiegen durchfuehren.
**/
{
        if (AutoTara.aktivate == 4)
        {
              autoan = 1;
        }
        else
        {
              autoan = 0;
        }
        AutoTara.text3 = anaus [autoan];
        current_wieg = autopos;
        return 0;
}

void PressAuto ()
/**
Autotara druecken.
**/
{
       SendMessage (WieButton.mask[autopos].feldid, WM_LBUTTONDOWN, 0l, 0l);
       SendMessage (WieButton.mask[autopos].feldid, WM_LBUTTONUP, 0l, 0l);
}

int festtara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char ftara [11];
        static char *args[5];
        char buffer [20];

        ShowFestTara (buffer);
        sprintf (ftara, "%.3lf", (double) ratod (buffer));
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        return 0;
}


int deltara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        akt_tara = (double) 0.0;
        return 0;
}

int settara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "4";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        return 0;
}

int handtara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static char ftara [11];
        static int arganz;
        static char *args[4];

        EnterNumBox (WiegWindow,"  Tara  ",  ftara, 8, "%7.3f");
        SetAktivWindow (WiegWindow);
        akt_tara = akt_tara + (double) ratod (ftara);
        sprintf (ftara, "%.3lf", akt_tara);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
        current_wieg = 2;
        return 0;
}

static int StornoSet = 0;

int dostorno (void)
/**
SornoFlag setzen.
**/
{
       StornoSet = 1;
       PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RETURN, 0l);
       return 0;
}

BOOL TestAllStorno (void)
/**
Abfragem, ob die ganze Position stormiert werden soll.
**/
{
         double rest;
         double lief_me;

         if (WorkModus == AUSZEICHNEN && AktWiegPos)
         {
                 if (abfragejnG (WiegWindow,
                         "Einzelne Wiegung stornieren", "J"))
                 {
                          AuswahlWieg ();
                          rest    = (double) ratod (auf_me);
                          lief_me = (double) ratod (aufps.lief_me);
                          rest += StornoWieg;
                          lief_me -= StornoWieg;
                          sprintf (auf_me, "%.3lf", rest);
                          sprintf (aufps.lief_me, "%3lf", lief_me);
                          return FALSE;
                 }
         }

         if (abfragejnG (WiegWindow,
                         "Die gesamte Position stornieren", "N"))
         {
             return TRUE;
         }
         return FALSE;
}


void stornoaction (void)
/**
Storno ausfuehren.
**/
{
        double lief_me;
        double rest;
        double wiegme;

        lief_me = (double) ratod (aufps.lief_me);
        rest    = (double) ratod (auf_me);
        wiegme = ratod (editbuff);
        if (wiegme != (double) 0.0)
        {
            rest += wiegme;
            lief_me -= wiegme;
            sprintf (auf_me, "%.3lf", rest);
        }
        else
        {
            if (TestAllStorno () == FALSE) return;
            rest += lief_me;
            lief_me = (double) 0;
            strcpy (auf_me, aufps.auf_me_vgl);
            strcpy (summe1, "0");
            strcpy (summe2, "0");
            strcpy (summe3, "0");
            strcpy (zsumme1, "0");
            strcpy (zsumme2, "0");
            strcpy (zsumme3, "0");
        }
        sprintf (aufps.lief_me, "%3lf", lief_me);
        strcpy (WiegMe, "0.000");
        StornoSet = 0;
        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtolsp (aufpidx);
        ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls,
                                        lsp.a, lsp.posi);
        commitwork ();
        beginwork ();
}
        
int dohand (void)
/**
Wiegen durchfuehren.
**/
{
        double alt;
        double netto;

        StornoSet = 0;
        SetStorno (dostorno, 0);
        sprintf (WiegMe, "%.3lf", (double) 0);
        EnterNumBox (WiegWindow,"  Gewicht  ",  WiegMe, 12, WiegKgE.mask[0].picture);
        if (syskey != KEY5)
        {
              strcpy (aufps.erf_kz, "H");
              if (StornoSet)
              {
                  stornoaction ();
              }
              else
              {
                  netto = (double) ratod (WiegMe);
                  alt = (double) ratod (auf_me);
                  alt -= netto;
                  sprintf (auf_me, "%3lf", alt);
                  alt = (double) ratod (aufps.lief_me);
                  alt += netto;
                  sprintf (aufps.lief_me, "%3lf", alt);
                  memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                  arrtolsp (aufpidx);
                  ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls,
                                        lsp.a, lsp.posi);
                  commitwork ();
                  beginwork ();
              }
              display_form (WiegKg, &WiegKgE, 0, 0);
              display_form (WiegWindow, &artwform, 0, 0);
              display_form (WiegWindow, &artwconst, 0, 0);
        }
        current_wieg = 2;
        return 0;
}

int GetDefault (char *env, char *wert)
/**
Wert aus bws_default holen.
**/
{
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\bws_defa", etc);
        fp = fopen (buffer, "r");
/*
        if (fp == NULL)
        {
                    sprintf (buffer, "%s\\bws_defa", etc);
                    fp = fopen (buffer, "r");
        }
*/
        if (fp == NULL) return -1;

        clipped (env);
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], env, strlen (env)) == 0)
                     {
                                 strcpy (wert, wort [2]);
                                 fclose (fp);
                                 return 0;
                     }
         }
         fclose (fp);
         return (-1);
}

long GetStdAuszeichner (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
        static long auszeichner = 0;
        char defwert [20];

        if (auszeichner) return auszeichner;

        if (GetDefault ("TOUCH-AUSZEICHNER", defwert) == -1)
        {
                             return (long) 1;
        }
        auszeichner = atol (defwert);
        if (GetDefault ("PAZ_DIREKT", defwert) != -1)
        {
                        paz_direkt = atoi (defwert);
        }
        return auszeichner;
}

long GetStdWaage (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
        static long waage = 0;
        char defwert [20];

        if (waage) return waage;

        if (GetDefault ("TOUCH-WAAGE", defwert) == -1)
        {
                             return (long) 1;
        }
        waage = atol (defwert);
        return waage;
}
 
static long start_aufme;
static long start_liefme;
static long alarm_me = 0;
static short alarm_ok = 0;
static HWND AlarmWindow = NULL;
static short a_me_einh;
static int me_faktor = 1;
static long akt_me = 0; 
static int break_ausz = 0;
int wiegx, wiegy, wiegcx, wiegcy;
int alarmy;
static int GxStop = 0;

static char amenge [80]; 

mfont AlarmFont  = {"Courier New", 150, 0, 1, BLACKCOL,
                                              LTGRAYCOL,
                                              1,
                                              NULL};

static field _alfield [] = 
                 {"A C H T U N G ! ! !", 0, 0, 40, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  amenge,                0, 0, 70, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form alform = {2, 0, 0, _alfield, 0, 0, 0, 0, &AlarmFont};

mfont MessFont  = {"Courier New", 150, 0, 1, BLACKCOL,
                                             WHITECOL,
                                             1,
                                             NULL};

mfont MessFontBlue  = {"Courier New", 180, 0, 1, WHITECOL,
                                                 BLUECOL,
                                                 1,
                                                 NULL};

static char msgstring  [80]; 
static char msgstring1 [80]; 
static char msgstring2 [80]; 

static int msgy = 1;
static int oky = 4;
static int okcy = 3;
static int okcx = 8;
static int BreakKey5 (void);
static int BreakKey12 (void);
static int BreakOK (void);

static field _msgfield [] = 
{
                  msgstring,  0, 0, 1, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring1, 0, 0, 2, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring2, 0, 0, 3, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  " OK ",     0, 2, 4, -1, 0, "", BUTTON, 0, 0, KEYCR,  
}; 


static form msgform = {1, 0, 0, _msgfield, 0, 0, 0, 0, &MessFont};

static field _msgfieldjn [] = 
{
                  msgstring,  0, 0, 1, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring1, 0, 0, 2, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring2, 0, 0, 3, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  "  Ja  ",   0, 2, 4, -1, 0, "", BUTTON, 0, 0,  KEY12,  
                  " Nein ",   0, 2, 4, -1, 0, "", BUTTON, 0, 0,  KEY5,  
}; 


static form msgformjn = {5, 0, 0, _msgfieldjn, 0, 0, 0, 0, &MessFont};

static form *aktmessform;


void TestMessage (void)
/**
Window-Meldungen testen.
**/
{
         MSG msg;

         if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE) == 0) return;

         if (msg.message == WM_KEYDOWN && msg.wParam != VK_RETURN) return;

         if (IsWiegMessage (&msg) == 0)
         {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
         }
}

void WritePrcomm (char *comm)
/**
Datei Prcomm schreiben.
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        int z;

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prcomm", tmp);
        z = 0;
        fp = fopen (buffer, "w");
        while (fp == NULL)
        {
                    if (z == 1000) break;
                    TestMessage (); 
                    if (break_ausz) break;
                    Sleep (10);
                    z ++;
                    fp = fopen (buffer, "w");
        }
        if (fp == NULL) return;
        fprintf (fp, "%s\n", comm);
        fclose (fp);
}  

void Alarm (void)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int i;

        if (AlarmWindow) return;         

        sprintf (amenge, "Sollmenge fast erreicht"); 

/*
        AlarmWindow  = CreateWindowEx (
                              0,
                              "AufWiegCtr",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              wiegx, wiegy - wiegcy - 5,
                              wiegcx, wiegcy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
*/
        AlarmWindow  = CreateWindowEx (
                              0,
                              "AufWiegCtr",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              wiegx, 
//                              wiegy,
                              alarmy,
                              wiegcx, wiegcy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        if (AlarmWindow == NULL) return;
        ShowWindow (AlarmWindow, SW_SHOWNORMAL);
        UpdateWindow (AlarmWindow); 
        for (i = 0; i < 3; i ++)
        { 
                    MessageBeep (0xFFFFFFFF);
        }
}


LONG FAR PASCAL MsgProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (hWnd == MessWindow)
                    {
                            display_form (hWnd, aktmessform, 0, 0);
                    }
                    break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                     {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

    
void CreateMessage (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        int i;
        RECT rect;
        TEXTMETRIC tm;

        if (MessWindow) return;         

        aktmessform = &msgform;
        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.fieldanz = 1;
        msgform.mask[0].pos[0] = -1;
        msgform.mask[0].pos[1] = -1;
        msgform.font = &MessFont;
        SetTextMetrics (hWnd, &tm, msgform.font);

        GetClientRect (hWnd, &rect); 
        strcpy (msgstring, message);

        cx = (strlen (msgstring) + 3) * tm.tmAveCharWidth;
        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 2;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegWhite",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
        UpdateWindow (MessWindow); 
}

void DestroyMessage (void)
{
        if (MessWindow == NULL) return;         
        CloseControls (&msgform);
        DestroyWindow (MessWindow); 
        MessWindow = 0;
}

int BreakKey5 ()
{
        syskey = KEY5;
        break_enter ();
        return 0;
}

int BreakKey12 ()
{
        syskey = KEY12;
        break_enter ();
        return 0;
}

int BreakOK ()
{
        break_enter ();
        return 0;
}
    

int CreateMessageJN (HWND hWnd, char *message, char *def)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        int okpos;
        unsigned int len, pos;
        HWND OldFocus;

        if (MessWindow) return 0;         

        aktmessform = &msgformjn;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return 0;

        for (i = 0; i < msgformjn.fieldanz; i ++)
        {
            msgformjn.mask[i].length = 0;
        }

        msgformjn.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgformjn.font);

        msgformjn.mask[1].attribut = REMOVED;
        msgformjn.mask[2].attribut = REMOVED;
        strcpy (msgstring, wort[0]);
        len = strlen (msgstring);
        ltext = msgstring;
        okpos = oky;
        if (anz > 1)
        {
            strcpy (msgstring1, wort[1]);
            if (strlen (msgstring1) > len) 
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgformjn.mask[1].attribut = DISPLAYONLY;
            okpos += 1;
        }
        if (anz > 2)
        {
            strcpy (msgstring2, wort[2]);
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgformjn.mask[2].attribut = DISPLAYONLY;
            okpos += 1;
        }

        msgform.fieldanz = 5;

        GetClientRect (hMainWindow, &rect); 

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 4;
        y = (rect.bottom - cy) / 2 ;

        msgformjn.mask[0].pos[0] = tm.tmHeight * msgy;
        msgformjn.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgformjn.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));
        msgformjn.mask[msgformjn.fieldanz - 2].pos[0] = 
                                 (int) (double) (tm.tmHeight * okpos);
        msgformjn.mask[msgformjn.fieldanz - 1].pos[0] = 
                                 (int) (double) (tm.tmHeight * okpos);

        msgformjn.mask[msgformjn.fieldanz - 2].rows   = tm.tmHeight * okcy;
        msgformjn.mask[msgformjn.fieldanz - 2].length = tm.tmAveCharWidth * okcx;
        msgformjn.mask[msgformjn.fieldanz - 1].rows   = tm.tmHeight * okcy;
        msgformjn.mask[msgformjn.fieldanz - 1].length = tm.tmAveCharWidth * okcx;
        strcpy (msgformjn.mask[msgformjn.fieldanz - 2].feld, "  Ja  ");
        strcpy (msgformjn.mask[msgformjn.fieldanz - 1].feld, " Nein ");

        pos = (len + 3 - 16) / 2;
        if (pos <= 0) pos = 0;

        msgformjn.mask[msgformjn.fieldanz - 2].pos[1] = pos * tm.tmAveCharWidth;
        msgformjn.mask[msgformjn.fieldanz - 1].pos[1] = (pos + 9)
                                                     * tm.tmAveCharWidth;

        cy =  msgformjn.mask[msgformjn.fieldanz - 1].pos[0] +
              msgformjn.mask[msgformjn.fieldanz - 1].rows +
              tm.tmHeight;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
//                              hWnd,
                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return 0;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
//        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
//                   SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        display_form (MessWindow, &msgformjn, 0, 0);
        save_fkt (5);
        save_fkt (12);
        set_fkt (BreakKey5, 5);
        set_fkt (BreakKey12, 12);
        no_break_end ();
        EnableWindows (hMainWindow, FALSE);
        EnableWindows (hWnd, FALSE);
        SetButtonTab (TRUE);
        enter_form (MessWindow, &msgformjn, 0, 0);
        SetButtonTab (FALSE);
        EnableWindows (hWnd, TRUE);
        EnableWindows (hMainWindow, TRUE);
        DestroyMessage ();
        SetFocus (OldFocus);
        restore_fkt (5);
        restore_fkt (12);
        if (syskey == KEY5) return 0;
        return 1;
}


void CreateMessageOK (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        int okpos;
        unsigned int len;
        HWND OldFocus;

        if (MessWindow) return;         

        aktmessform = &msgform;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return;

        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgform.font);

        msgform.mask[1].attribut = REMOVED;
        msgform.mask[2].attribut = REMOVED;
        strcpy (msgstring, wort[0]);
        len = strlen (msgstring);
        ltext = msgstring;
        okpos = oky;
        if (anz > 1)
        {
            strcpy (msgstring1, wort[1]);
            if (strlen (msgstring1) > len) 
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgform.mask[1].attribut = DISPLAYONLY;
            okpos += 1;
        }
        if (anz > 2)
        {
            strcpy (msgstring2, wort[2]);
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgform.mask[2].attribut = DISPLAYONLY;
            okpos += 1;
        }

        msgform.fieldanz = 4;

        GetClientRect (hMainWindow, &rect); 

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 4;
        y = (rect.bottom - cy) / 2 ;

        msgform.mask[0].pos[0] = tm.tmHeight * msgy;
        msgform.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgform.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));
        msgform.mask[msgform.fieldanz - 1].pos[0] = 
                                 (int) (double) (tm.tmHeight * okpos);

        msgform.mask[msgform.fieldanz - 1].rows   = tm.tmHeight * okcy;
        msgform.mask[msgform.fieldanz - 1].length = tm.tmAveCharWidth * okcx;
        strcpy (msgform.mask[msgform.fieldanz - 1].feld, " OK ");

        cy =  msgform.mask[msgform.fieldanz - 1].pos[0] +
              msgform.mask[msgform.fieldanz - 1].rows +
              tm.tmHeight;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
//                              WS_EX_TOPMOST,
                               0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
//        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
//                   SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        display_form (MessWindow, &msgform, 0, 0);
        set_fkt (BreakOK, 12);
        break_end ();
        EnableWindows (hMainWindow, FALSE);
        EnableWindows (hWnd, FALSE);
        SetButtonTab (TRUE);
        enter_form (MessWindow, &msgform, 0, 0);
        SetButtonTab (FALSE);
        EnableWindows (hWnd, TRUE);
        EnableWindows (hMainWindow, TRUE);
        DestroyMessage ();
        SetFocus (OldFocus);
}

void disp_messG (char *text, int modus)
{
       if (AktivWindow == NULL)
       {
                 AktivWindow = GetActiveWindow ();
       }
        if (modus == 2)
        {
                    MessageBeep (0xFFFFFFFF);
        }
        CreateMessageOK (AktivWindow, text);
}

static char messbuffer [1024];

int print_messG (int mode, char * format, ...)
/**
Ausgabe in dbfile.
**/
{
        va_list args;
 
        va_start (args, format);
        vsprintf (messbuffer, format, args);
        va_end (args);
        disp_messG (messbuffer, mode);
        return (0);
}

                  
int abfragejnG (HWND hWnd, char *text, char *def)
/**
Ausgabe in dbfile.
**/
{
        return CreateMessageJN (hWnd, text, def);
}


void WaitStat (char *stat)
/**
Ergebnis-Datei vom Preisauszeichner lesen. Auf stat warten
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        char satz [512];
        int z;

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);

        z = 0; 
        while (TRUE)
        {
                  if (z == 30000) break;
                  fp = fopen (buffer, "r");
                  if (fp == NULL)
                  { 
                              z ++;
                              Sleep (10);
                              continue;
                  }
                  if (fgets (satz, 511, fp) == 0) 
                  {
                              z ++;
                              fclose (fp); 
                              Sleep (10);
                              continue;
                  }
                  fclose (fp);
                  cr_weg (satz);
                  split (satz); 
                  if (strcmp (wort[1], stat) == 0) break;
                  z ++;
                  TestMessage (); 
                  if (break_ausz) break;
                  Sleep (10);
         }
}


static long sup;
static long sup0;
static long sup1;
static short kunme_einh = 0; 
static long  kunmez = 0l;
static long  kunme = 0l; 

void TestSu (double zsu1, double zsu2)
{ 
         static double akt_zsu1 = (double) 0;
         static double akt_zsu2 = (double) 0;

         if (zsu1 != akt_zsu1 && zsu1 <= (double) 1)
         {
                 sup ++;
                 sup1 ++;
         }

         if (zsu2 != akt_zsu2 && zsu2 <= (double) 1)
         {
                 sup = (long) zsu1;
                 sup0 ++;
         }
         akt_zsu1 = zsu1;
         akt_zsu2 = zsu2;
}


int AuszKomplett (void)  
/**
Test, ob die Auftragsmenge erreicht ist.
**/
{
        long auf_me_kun;
        
        auf_me_kun = atol (aufps.auf_me);     
        switch (kunme_einh)
        {
            case 2 :
                 if (ratod (auf_me) <= (double) 0.0)
                 {
                         return 1;
                 } 
                 return 0;
            case 1 :
                 if (kunme > auf_me_kun)
                 {
                         return 1;
                 } 
                 return 0;
            case 8 :
                 if (sup1 > auf_me_kun)
                 {
                         return 1;
                 } 
                 return 0;
            case 16 :
                 if (sup0 > auf_me_kun)
                 {
                         return 1;
                 } 
                 return 0;
        }
        return 0;
}


void GetKunMe ()
/**
Auftragsmenge in Kundenbestelleinheit 
**/
{
         int me_einh_kun;

         me_einh_kun = atoi (aufps.me_einh_kun);
         switch (me_einh_kun)
         {
             case 2 :
                   kunme_einh = 2;
                   break;
             case 6 :
             case 7 :
                   kunme_einh = 8;
                   break;
             case 10 :
                   kunme_einh = 16;
                   break;
             default :
                   kunme_einh = 1;
                   break;
         }
}

void FillSummen (double su1, double su2, 
                 double zsu1, double zsu2)
/**
Aktuellen Inhalte der einzelnen Summen in Abhaengigkeit
von der Einheit fuellen.
**/
{

         switch (a_kun_geb.geb_fill)
         {
               case 2 :
                    sprintf (summe1, "%.3lf", su1 / me_faktor);
                    break;
               case 1: 
                    sprintf (summe1, "%.0lf", zsu1);
                    break;
               default :
                    sprintf (summe1, "%.3lf", su1 / me_faktor);
                    break;
         }

         switch (a_kun_geb.pal_fill)
         {
               case 2 :
                    sprintf (summe2, "%.3lf", su2 / me_faktor);
                    break;
               case 1: 
                    sprintf (summe2, "%.0lf", zsu2);
                    break;
               case 5: 
                    sprintf (summe2, "%ld", sup);
                    break;
               default :
                    sprintf (summe2, "%.3lf", su2 / me_faktor);
                    break;
         }
}

void FillSumme3 (double su3, double zsu3)
/**
Aktuellen Inhalte von Summe3 in Abhaengigkeit
von der Einheit fuellen.
**/
{

         switch (kunme_einh)
         {
            case 2 :
                    sprintf (summe3, "%.3lf", su3 / me_faktor);
                    break;
            case 1 :
                    sprintf (summe3, "%.0lf", zsu3);
                    break;
            case 8 :
                    sprintf (summe3, "%.0lf", sup);
                    break;
            case 16 :
                    sprintf (summe3, "%ld", sup0);
                    break;
        }
}

int KorrMe (char *satz)
/**
Auftragsmenge korrigieren.
**/
{
         int anz;
         long me;
         long zielme;
         double me_vgl;
         double su1;
         double su2;
         double zsu1;
         double zsu2;
         double zsu3;

         if (atoi (aufps.a_me_einh) == 2)
         {  
                    me_vgl = ratod (aufps.auf_me_vgl) * 1000;
         }
         else
         {  
                    me_vgl = ratod (aufps.auf_me_vgl);
         }

         cr_weg (satz);
         anz = split (satz);
         if (anz < 9) 
         {
                   return 1;
         }

         if (wort[1][0] == 'E')
         {
                   return 0;
         }

         if (GxStop) return 1;

         zsu1 = ratod (wort[4]);
         su1  = ratod (wort[5]);
         zsu2 = ratod (wort[6]);
         su2  = ratod (wort[7]);
         zsu3 = ratod (wort[8]);
         me   = atol (wort[9]);


         TestSu (zsu1, zsu2);
         FillSummen (su1, su2, zsu1, zsu2);

         zsu2 = (double) sup;
//         zsu3 = (double) sup0;

/*         
         sprintf (summe1, "%.3lf", su1 / me_faktor);
         sprintf (summe2, "%.3lf", su2 / me_faktor);
*/

         sprintf (zsumme1, "%.0lf", zsu1);
         sprintf (zsumme2, "%.0lf", zsu2);

         if (!alarm_ok && me > 0l)
         {
                     alarm_me = (long) ((double) me_vgl - 3 * me);
                     alarm_ok = 1;
         }

         if (me == akt_me) return 1;

         kunme ++;
         zielme = start_aufme - me;
         sprintf (auf_me, "%.3lf", (double) ((double) zielme / me_faktor));

         zielme = start_liefme + me;
         sprintf (aufps.lief_me, "%.3lf", (double) ((double) zielme / me_faktor));
         FillSumme3 ((double) zielme, zsu3);
//         sprintf (summe3, "%.3lf", (double) ((double) zielme / me_faktor));
         zsu3 = (double) sup0;
         sprintf (zsumme3, "%.0lf", zsu3);
         AktPrWieg[AktWiegPos] = ((double) (me - akt_me)) / me_faktor;
         sprintf (WiegMe, "%.3lf", (double) ((double) (me - akt_me) / me_faktor));

         display_field (WiegWindow, &artwform.mask[1], 0, 0);
         display_field (WiegWindow, &artwform.mask[2], 0, 0);

         display_field (WiegWindow, &artwform.mask[3], 0, 0);
         display_field (WiegWindow, &artwform.mask[4], 0, 0);
         display_field (WiegWindow, &artwform.mask[5], 0, 0);
         display_field (WiegWindow, &artwform.mask[6], 0, 0);
         display_field (WiegWindow, &artwform.mask[7], 0, 0);
         display_field (WiegWindow, &artwform.mask[8], 0, 0);
         display_field (WiegWindow, &artwform.mask[9], 0, 0);
         display_field (WiegWindow, &artwform.mask[10], 0, 0);

         display_field (WiegKg,     &WiegKgE.mask[0], 0, 0);

         memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
         arrtolsp (aufpidx);
         ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls,
                                        lsp.a, lsp.posi);
         commitwork ();
         beginwork ();

         SetFocus (WieButton.mask[3].feldid);
         if (AktWiegPos < 998) AktWiegPos ++;
         akt_me = (long) me; 
//         if (ratod (auf_me) <= (double) 0.0)
         if (AuszKomplett ())
         {
                         return 0;
         }

         if (zielme >= alarm_me)
         {
                         Alarm ();
         } 

         return 1;
}

void PollAusz (void)
/**
Ergebnis-Datei vom Preisauszeichner lesen.
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        char satz [512];
        int pollanz;

        a_me_einh = atoi (aufps.a_me_einh);
        alarm_me = 0l;
        alarm_ok = 0;
        kunme = 0;
        kunmez = 0l;
        GetKunMe ();
        sup = 0l;
        sup0 = 0l;
        sup1 = 0l;

        if (a_me_einh == 2)
        {
                      me_faktor = 1000;
        }
        else
        {
                      me_faktor = 1;
        }

        start_aufme  =  (long) ((double) ratod (auf_me) * me_faktor);
        start_liefme =  (long) ((double) ratod (aufps.lief_me) * me_faktor);

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);

        GxStop = pollanz = 0;
        while (TRUE)
        {
                  if (GxStop) pollanz ++;
                  if (pollanz >= 400) break;
                  fp = fopen (buffer, "r");
                  if (fp == NULL)
                  { 
                              TestMessage (); 
                              Sleep (5);
                              continue;
                  }
                  if (fgets (satz, 511, fp) == 0) 
                  {
                              TestMessage (); 
                              fclose (fp); 
                              Sleep (5);
                              continue;
                  }
                  fclose (fp);
                  if (KorrMe (satz) == 0)
                  {
                              break;
                  }
                  TestMessage (); 
                  SetFocus (WieButton.mask[3].feldid);
                  Sleep (5);
         }
}

void starte_prakt (void)
/**
Prgramm prakt starten.
**/
{
        char *tmp;
        char *bws;
        char buffer [512];

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);
        unlink (buffer);

        bws = getenv ("BWS");
        if (bws == NULL)
        {
                  bws = "C:\\USER\\FIT";
        }
        sprintf (buffer, "%s\\bin\\gxakt", bws);

        if (ProcExec (buffer, SW_SHOWMINNOACTIVE, -1, 0, -1, 0) == 0)
        {
                       print_messG (2, "Fehler beim Start von %s", buffer);
//                       break_ausz = 1;
        }
}

void SetAuszText (int mode)
/**
Text fuer AuszecihnenButton setzen.
**/
{
         if (mode == 0)
         {
               WieWie.text1 = "Aus-";
               WieWie.text2 = "zeichnen";
               WieWie.tx1 = -1;
               WieWie.ty1 = 25;
               WieWie.tx2 = -1;
               WieWie.ty2 = 50;
         }
         else
         {
               WieWie.text1 = "Stop";
               WieWie.text2 = "";
               WieWie.tx1 = -1;
               WieWie.ty1 = -1;
               WieWie.tx2 = -1;
               WieWie.ty2 = -1;
          }
          WieWie.aktivate = 2;   
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 1);
          SetFocus (WieButton.mask[3].feldid);
}

void BuInactiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{         
          ActivateColButton (WiegCtr, &WieButton, 0, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 1, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 2, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 3, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 4, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 5, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 6, -1, 1);
}

void BuActiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{         
          ActivateColButton (WiegCtr, &WieButton, 0, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 1, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 2, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 4, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 5, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 6, 2, 1);
}

static void FileError (void)
{

          disp_messG ("Es wurden keine Daten zum Auszeichnen gefunden\n"
                     "Pr�fen Sie bitte den Verkaufspreis\n und die Hauptwarengruppenzuordnung",
                     2);
          return;
}


BOOL KunGxOk (void)
/**
Pruefen, ob die Dateien fuer gxakt leer sind.
**/
{
        char *tmp;
        char dname [256];
        HANDLE fp;
        DWORD fsize;

        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (dname, "%s\\kungx", tmp);
        fp = CreateFile (dname, GENERIC_READ, 0, NULL, 
                         OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (fp == INVALID_HANDLE_VALUE)
        {
                         FileError (); 
                         return FALSE;
        }

        fsize = GetFileSize (fp, NULL);
        if (fsize == (DWORD) 0 ||
            fsize == 0xFFFFFFFF)
        {     
                         CloseHandle (fp);
                         FileError (); 
                         return FALSE;
        }

        CloseHandle (fp);
        return TRUE;
}


/*
void RunBwsasc (long auszeichner, double auf_me_vgl)
{
        char buffer [256];
        char *tmp;
        char args [80] [20]; 
        char and_part [512];


        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }

         sprintf (and_part, 
                 "where a between %.0lf and %.0lf", 
                 ratod (aufps.a), ratod (aufps.a));
 
         sethochkomma ();
         setomodus ("w");
         settrenner (' ');
         setclipped (1);
         setand_part (and_part);
         sprintf (buffer, "%s\\kungx", tmp);
         setascii_name (buffer); 
         setinfo_name ("kungx.if");

         sprintf (args [0], "%ld", auszeichner);
         sprintf (args [1],"%ld", atol (kun_nr));
         strcpy (args [2], lieferdatum);
         sprintf (args [3], "%.0lf", auf_me_vgl);

         argsp [0] = args [0];
         argsp [1] = args [1];
         argsp [2] = args [2];
         argsp [3] = args [3];

         do_bwsasc ();
         
         sprintf (buffer, "%s\\kungtex", tmp);
         setascii_name (buffer); 
         setinfo_name ("kungtex.if");

         do_bwsasc ();
}
*/

int doauszeichnen (void)
/**
Auftragsposition auszeichnen.
**/
{
        char buffer [512];
        char *tmp;
        double auf_me_vgl;
        double auf_me_kun;
        DWORD ex;

        akt_me = 0;
        break_ausz = 0;
        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }

//        auszeichner = GetStdAuszeichner ();

        GetKunMe ();
        if (atoi (aufps.me_einh_kun) == 2)
        {  
                    auf_me_kun = ratod (aufps.auf_me) * 1000;
        }
        else
        {
                    auf_me_kun = ratod (aufps.auf_me);
        }
        if (auf_me_kun <= (double) 0.0) 
        {
                       
                       disp_messG ("Menge 0 kann nicht ausgezeichnet werden", 2);       
                       return (0);
        }
        if (atoi (aufps.a_me_einh) == 2)
        {  
                    auf_me_vgl = ratod (aufps.auf_me_vgl) * 1000;
        }
/*
        if (auf_me_vgl <= (double) 0.0) 
        {
                       
                       disp_messG ("Menge 0 kann nicht ausgezeichnet werden", 2);       
                       return (0);
        }
*/

        if (ratod (auf_me) <= (double) 0.0) 
        {
                       
                       disp_messG ("Die Sollmenge ist erreicht", 2);       
                       return (0);
        }
        if (paz_direkt == 3)
        {
/*
                      RunBwsasc (auszeichner, auf_me_vgl);
                      BuInactiv ();
*/
        }
        else if (paz_direkt == 2)
        {

/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -dokt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh);

                      BuInactiv ();
                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL, 
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2, 
                              "Fehler beim 1. Start von pr_paz");
                          BuActiv (); 
                          return (-1);
                      }

                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL, 
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2, 
                              "Fehler beim 2. Start von pr_paz");
                          BuActiv (); 
                          return (-1);
                      }
       }
        else if (paz_direkt == 1)
        {
/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh);


                      BuInactiv ();
                      CreateBatch (buffer);

                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                     RunBatch (buffer);
        }
        else
        {
/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh );

                      BuInactiv ();
                      CreateMessage (WiegWindow, "Die Daten werden vorbereitet"); 
                      ex = ProcWaitExec (buffer, SW_HIDE, 
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage (); 
                          AktivWindow = WiegWindow;
                          print_messG (2, 
                              "Fehler beim 1. Start von pr_paz");
                          BuActiv (); 
                          return (-1);
                      }
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr));

                      ex = ProcWaitExec (buffer, SW_HIDE, 
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage (); 
                          AktivWindow = WiegWindow;
                          print_messG (2, 
                              "Fehler beim 2. Start von pr_paz");
                          BuActiv (); 
                          return (-1);
                      }
                      DestroyMessage (); 
                      AktivWindow = WiegWindow;
       }
       if (! KunGxOk ())
       {
                    BuActiv (); 
                    WorkModus = AUSZEICHNEN;
                    return (-1);
        } 

        WorkModus = STOP;
        WritePrcomm ("G");
        if (! break_ausz)
        { 
                   SetAuszText (1);
                   starte_prakt ();
                   SetActiveWindow (WiegCtr);
        }
        if (! break_ausz)
        { 
                   PollAusz ();
        }
        WritePrcomm ("E"); 
        SetAuszText (0);
        BuActiv (); 
        WorkModus = AUSZEICHNEN;
        if (AlarmWindow)
        {
                     CloseControls (&alform);
                     DestroyWindow (AlarmWindow);
                     AlarmWindow = NULL;
        }
        return 0;
} 

int StopAuszeichnen (void)
{

        GxStop = 1;
        if (abfragejnG (WiegWindow, "Auszeichnen stoppen ?", "N") == 0) 
        {
            GxStop = 0;
            return 0;
        }
        WritePrcomm ("E"); 
        GxStop = 1;
        return 0;
}

int dowiegen (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        double alt;
        static int arganz;
        static char *args[4];

        if (WorkModus == AUSZEICHNEN)
        {
                       return (doauszeichnen ());
        }
        else if (WorkModus == STOP)
        {
                       return (StopAuszeichnen ());
        }
        arganz = 3;
        args[0] = "Leer";
        args[1] = "11";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);

        if (tara == (double) 0.0)
        {
                   strcpy (aufps.erf_kz, "W");
        }
        else
        {
                   strcpy (aufps.erf_kz, "E");
        }
        alt = (double) ratod (auf_me);
        alt -= netto;
        sprintf (WiegMe, "%11.3lf", netto);
        sprintf (auf_me, "%3lf", alt);
        alt = (double) ratod (aufps.lief_me);
        alt += netto;
        sprintf (aufps.lief_me, "%3lf", alt);
        sprintf (aufps.tara, "%.3lf", tara);
        display_form (WiegKg, &WiegKgE, 0, 0);
        display_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        if (autoan) settara ();
        current_wieg = 3;
        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                  arrtolsp (aufpidx);
        ls_class.update_lsp (lsk.mdn, lsk.fil, lsk.ls,
                             lsp.a, lsp.posi);
        commitwork ();
        beginwork ();
        return 0;
}

void PrintWLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
         RECT rect, frect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         GetClientRect (WiegWindow, &rect);
         GetFrmRect (&artwform, &frect);

         hdc = BeginPaint (WiegWindow, &ps);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 10;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);


         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = frect.bottom + 25;
         alarmy = y;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}


LONG FAR PASCAL WiegProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (hWnd == MessWindow)
                    {
                            display_form (hWnd, &msgform, 0, 0);
                    }


                    if (hWnd == WiegKg)
                    {
                            display_form (hWnd, &WiegKgT, 0, 0);
                            display_form (hWnd, &WiegKgE, 0, 0);
                    }
                    else if (hWnd == WiegCtr)
                    {       if (CtrMode == 0)
                            {
                                    display_form (hWnd, &WieButton, 0, 0);
                                    SetFocus (
                                           WieButton.mask[current_wieg].feldid);
                            }
                            else
                            {
                                    display_form (hWnd, &WieButtonT, 0, 0);
                                    SetFocus (
                                           WieButtonT.mask[current_wieg].feldid);
                            }
                    }
                    else if (hWnd == WiegWindow)
                    {
                            display_form (hWnd, &WiegKgMe, 0, 0);
                            display_form (hWnd, &artwformT, 0, 0);
                            display_form (hWnd, &artwconstT, 0, 0);
                            if (artwform.mask[0].feldid)
                            {
                                 display_form (hWnd, &artwform, 0, 0);
                                 display_form (hWnd, &artwconst, 0, 0);
                            }
                            display_form (hWnd, &ftara, 0, 0);
                            display_form (hWnd, &ftaratxt, 0, 0);
                            PrintWLines ();
                    }
                    else if (hWnd == AlarmWindow)
                    {
                            display_form (hWnd, &alform, 0, 0);
                    }
                    break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == BUTARA)
                    {
                            PostMessage (WiegWindow, WM_USER, wParam, 0l);
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterWieg (void)
/**
Fenster fuer Auftragskopf registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  WiegProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
                   wc.lpszClassName =  "AufWieg";
                   RegisterClass(&wc);

                   // wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 120));
                   wc.hbrBackground =   GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "AufWiegCtr";
                   RegisterClass(&wc);

                   wc.lpfnWndProc   =   WiegProc;
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 255));
                   wc.lpszClassName =  "AufWiegKg";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

void TestEnter (void)
/**
Eingabefeld testen.
**/
{
       GetWindowText (artwform.mask[0].feldid,
                      artwform.mask[0].feld,
                      artwform.mask[0].length);
}


void SetWiegFocus (void)
/**
Focus fuer Wiegmaske setzen.
**/
{
       form *wform;

       if (CtrMode == 1)
       {
                  wform = &WieButtonT;
       }
       else
       {
                  wform = &WieButton;
       }

       if (current_wieg < wiegend)
       {
                  SetFocus (wform->mask[current_wieg].feldid);
       }
       else
       {
                  SetFocus (artwform.mask[0].feldid);
                  SendMessage (artwform.mask[0].feldid, EM_SETSEL,
                               (WPARAM) 0, MAKELONG (-1, 0));
       }
}

int ReturnActionW (void)
/**
Returntaste behandeln.
**/
{
       form *wform;

       if (current_wieg == wiegend)
       {
                  PostMessage (WiegWindow, WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
       }

       if (CtrMode == 1)
       {
                  wform = &WieButtonT;
       }
       else
       {
                  wform = &WieButton;
       }

       if (CtrMode == 0 && current_wieg == autopos)
       {
                        PressAuto (); 
       }
       else if (wform->mask[current_wieg].after)
       {
                       (*wform->mask[current_wieg].after) ();
       }
       else if (wform->mask[current_wieg].BuId)
       {
                       SendKey
                                 (wform->mask[current_wieg].BuId);
       }
       return TRUE;
}

int TestAktivButton (int sign, int current_wieg)
/**
Status von Colbutton pruefen.
**/
{
       ColButton *Cub;
       int run;
       form *wform;

       if (CtrMode == 1)
       {
                  wform = &WieButtonT;
       }
       else
       {
                  wform = &WieButton;
       }

       run = 1;
       while (TRUE)
       {
              if (current_wieg >= wform->fieldanz) break;                               
              Cub = (ColButton *) wform->mask[current_wieg].feld;
              if (Cub->aktivate != -1) break;
              current_wieg += sign;
              if (sign == PLUS && current_wieg > wiegend)
              {
                                if (run == 2) break;
                                current_wieg = 0;
                                run ++;
              }
              else if (sign == MINUS && current_wieg < 0)
              {
                                if (run == 2) break;
                                current_wieg = wiegend;
                                run ++;
              }  
       }
       return (current_wieg);
}  
           

int IsWiegMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     TestEnter ();
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    if (CtrMode == 1)
                                    {
                                          closetara ();
                                    }
                                    else
                                    {
                                          break_wieg = 1;
                                    }
                                    return TRUE;
                            case VK_RETURN :
                                    return (ReturnActionW ());
                            case VK_DOWN :
                            case VK_RIGHT :
                                    current_wieg ++;
                                    if (current_wieg > wiegend)
                                                current_wieg = 0;
                                    current_wieg = TestAktivButton (PLUS, current_wieg);
                                    SetWiegFocus ();
                                    return TRUE;
                            case VK_UP :
                            case VK_LEFT :
                                   current_wieg --;
                                   if (current_wieg < 0)
                                                current_wieg = wiegend;
                                    current_wieg = TestAktivButton (MINUS, current_wieg);
                                    SetWiegFocus ();
                                    return TRUE;
                            case VK_TAB :
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                             current_wieg --;
                                             if (current_wieg < 0)
                                                  current_wieg = wiegend;
                                             current_wieg = 
                                                 TestAktivButton (MINUS, current_wieg);
                                     }
                                     else
                                     {
                                             current_wieg ++;
                                             if (current_wieg > wiegend)
                                                  current_wieg = 0;
                                             current_wieg = 
                                                 TestAktivButton (PLUS, current_wieg);
                                     }
                                     SetWiegFocus ();
                                     return TRUE;
                            default :
                                    return FALSE;   
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (artwform.mask[0].feldid, &mousepos))
                    {
                          EnterNumBox (WiegWindow,"  Chargen-Nr  ",
                                       aufps.ls_charge, 17,
                                       artwform.mask[0].picture);
                          display_form (WiegWindow, &artwform, 0, 0);
                          current_wieg = TestAktivButton (PLUS, current_wieg);
                          SetWiegFocus ();
                          return TRUE;
                    }
              }
              case WM_USER :
                    if (LOWORD (msg->wParam) == BUTARA)
                    {
                          dotara ();
                          return TRUE;
                    }
         }
         return FALSE;
}

void PosArtFormW (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;


        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwformT.fieldanz; i ++)
        {
                    s = artwformT.mask[i].pos [1];
                    z = artwformT.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].pos [1] = s;
                    artwformT.mask[i].pos [0] = z;
                    s = artwformT.mask[i].length;
                    z = artwformT.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].length = s;
                    artwformT.mask[i].rows = z;
        }
        for (i = 0; i < artwform.fieldanz; i ++)
        {
                    s = artwform.mask[i].pos [1];
                    z = artwform.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].pos [1] = s;
                    artwform.mask[i].pos [0] = z;
                    s = artwform.mask[i].length;
                    z = artwform.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].length = s;
                    artwform.mask[i].rows = z;
        }
}

void PosArtFormC (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwconstT.fieldanz; i ++)
        {
                    s = artwconstT.mask[i].pos [1];
                    z = artwconstT.mask[i].pos [0];
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconstT.mask[i].pos [1] = s;
                    artwconstT.mask[i].pos [0] = z;
                    s = artwconstT.mask[i].length;
                    z = artwconstT.mask[i].rows;
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconstT.mask[i].length = s;
                    artwconstT.mask[i].rows = z;
        }
        for (i = 0; i < artwconst.fieldanz; i ++)
        {
                    s = artwconst.mask[i].pos [1];
                    z = artwconst.mask[i].pos [0];
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconst.mask[i].pos [1] = s;
                    artwconst.mask[i].pos [0] = z;
                    s = artwconst.mask[i].length;
                    z = artwconst.mask[i].rows;
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconst.mask[i].length = s;
                    artwconst.mask[i].rows = z;
        }
}

void PosArtFormP (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwformT.fieldanz; i ++)
        {
                    s = artwformT.mask[i].pos [1];
                    z = artwformT.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].pos [1] = s;
                    artwformT.mask[i].pos [0] = z;
                    s = artwformT.mask[i].length;
                    z = artwformT.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].length = s;
                    artwformT.mask[i].rows = z;
        }
        for (i = 0; i < artwform.fieldanz; i ++)
        {
                    s = artwform.mask[i].pos [1];
                    z = artwform.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].pos [1] = s;
                    artwform.mask[i].pos [0] = z;
                    s = artwform.mask[i].length;
                    z = artwform.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].length = s;
                    artwform.mask[i].rows = z;
        }
}

void PosMeForm (int yw, int xw, int cyw, int cxw)
/**
Position fuer WiegKgMe festlegen.
**/
{
        static int posOK = 0;
        int s, z;

        if (posOK) return;

        posOK = 1;

        WiegKgMe.mask[0].pos[0] = yw + WiegKgE.mask[0].pos[0];
        WiegKgMe.mask[0].pos[1] = xw + cxw + 30;
        s = WiegKgMe.mask[0].length;
        z = WiegKgMe.mask[0].rows;
        KonvTextAbsPos (WiegKgMe.font, &z, &s);
        WiegKgMe.mask[0].length = s;
        WiegKgMe.mask[0].rows = z;
}

void GetZentrierWerte (int *buglen, int *buabs, int wlen, int anz,
                       int abs, int size)
/**
Werte fuer Zentrierung ermitteln.
**/
{
        int blen;
        int spos;

        while (TRUE)
        {
                 blen = anz * (size + abs) - abs;
                 spos = (wlen - blen) / 2;
                 if (spos > 9)
                 {
                              *buglen = blen;
                              *buabs  = abs + size;
                              return;
                 }
                 abs -= 10;
                 if (abs <= 0) return;
       }
}

int closetara (void)
/**
Tara-Leiste schliessen.
**/
{
        CtrMode = 0;
        CloseControls (&WieButtonT);
        current_wieg = 0;
        if (ohne_preis)
        {
                 wiegend = 5;
        }
        else
        {
                 wiegend = 6;
        }
        return 0;
}

int dotara (void)
/**
Wiegen durchfuehren.
**/
{
        if (WieTara.aktivate == -1) return 0;
 
        if (WorkModus == AUSZEICHNEN)
        {
                        dogebinde ();
                        return (0);
        }
        CtrMode = 1;
        CloseControls (&WieButton);
        current_wieg = 0;
        CtrMode = 1;
        wiegend = 6;
        return 0;
}

int dopreis (void)
/**
Wiegen durchfuehren.
**/
{
        HWND fhWnd;

        fhWnd = GetFocus ();
        EnterPreisBox (WiegWindow, -1, WiegY - 86);
        SetFocus (fhWnd);
        return 0;
}

void ArtikelHand (void)
/**
Artikel-Menge von Hand eingeben.
**/
{
        double alt;
        double netto;

        StornoSet = 0;
        SetStorno (dostorno, 0);
        sprintf (WiegMe, "%.3lf", (double) 0);
        EnterNumBox (eWindow,"  Anzahl  ",  WiegMe, 12, WiegKgE.mask[0].picture);
        if (syskey != KEY5)
        {
              strcpy (aufps.erf_kz, "H");
              if (StornoSet)
              {
                  stornoaction ();
              }
              else
              {
                  netto = (double) ratod (WiegMe);
                  alt = (double) ratod (auf_me);
                  alt -= netto;
                  sprintf (auf_me, "%3lf", alt);
                  alt = (double) ratod (aufps.lief_me);
                  alt += netto;
                  sprintf (aufps.lief_me, "%3lf", alt);
                  strcpy (aufps.s, "3");
              }
        }
}

void ArtikelWiegen (HWND hWnd)
/**
Artikel wiegen.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
        int xw, yw, cxw, cyw;
        int xc, yc, cxc, cyc;
        int wlen, wrows;
        int buglen, buabs;
        int z, s;
        static int first = 0;

        if (WiegenAktiv) return;

        SwitchLiefAuf ();
        if (WorkModus == AUSZEICHNEN)
        {
//               WieWie.text1 = "Auszeichn.";
               strcpy (summe1, "0");
               strcpy (summe2, "0");
               strcpy (summe3, "0");
               strcpy (zsumme1, "0");
               strcpy (zsumme2, "0");
               strcpy (zsumme3, "0");
               WieWie.text1 = "Aus-";
               WieWie.text2 = "zeichnen";
               WieWie.tx1 = -1;
               WieWie.ty1 = 25;
               WieWie.tx2 = -1;
               WieWie.ty2 = 50;
//               WieTara.aktivate = -1;   
               WieTara.text1 = "Gebinde";
               WieTara.text2 = "Tara";
               WieTara.tx1 = -1;
               WieTara.ty1 = 25;
               WieTara.tx2 = -1;
               WieTara.ty2 = 50;
               fetch_std_geb ();
               memcpy (&artwformT,&artwformTP, sizeof (form));
               memcpy (&artwform,&artwformP, sizeof (form));
               PosArtFormP ();
        }
        else
        {
               WieWie.text1 = "Wiegen";
               WieWie.text2 = NULL;
               WieWie.tx1 = -1;
               WieWie.ty1 = -1;
               WieWie.tx2 = 0;
               WieWie.ty2 = 0;
//               WieTara.aktivate = 2;   
               WieTara.text1 = "Tara";
               WieTara.text2 = NULL;
               WieTara.tx1 = -1;
               WieTara.ty1 = -1;
               WieTara.tx2 = 0;
               WieTara.ty2 = 0;
               memcpy (&artwformT,&artwformTW, sizeof (form));
               memcpy (&artwform,&artwformW, sizeof (form));
               PosArtFormW ();
        }
        autoan = 0;
        AutoTara.aktivate = 3;
        WieButton.mask[1].BuId = 0;
        CtrMode = 0;

        AutoTara.text3 = anaus [autoan];
        SetListEWindow (0);
        strcpy (WiegMe, "0.000");
        sprintf (summe1, "%.0lf", (double) 0);
        sprintf (summe2, "%.0lf", (double) 0);
        sprintf (zsumme1, "0");
        sprintf (zsumme2, "0");
        AktWiegPos = 0;
        RegisterWieg ();
        GetWindowRect (hMainWindow, &rect);
        x = rect.left;
        y = rect.top + 24;
        cx = rect.right - rect.left; 
        cy = rect.bottom - rect.top - 24;

        PosArtFormC ();
        wlen = strlen (WiegText);
        FontTextLen (WiegKgT.font, WiegText, &wlen,
                                             &wrows);
        WiegKgT.mask[0].length = wlen;
        WiegKgT.mask[0].rows   = wrows;

        WiegKgE.mask[0].pos[0]  = WiegKgT.mask[0].pos[0] + 2 * wrows;

        wlen = 11;
        FontTextLen (WiegKgE.font, "0000000,000", &wlen,
                                                  &wrows);

        WiegKgE.mask[0].length = wlen;
        WiegKgE.mask[0].rows   = wrows;

        if (wlen > WiegKgT.mask[0].length)
        {
                   cxw = wlen;
        }
        else
        {
                   cxw = WiegKgT.mask[0].length;
        }

        cxw += 20;

        cyw = WiegKgE.mask[0].pos[0] + wrows + wrows / 2 + 10;
        xw = (cx - cxw) / 2;
        yw = (cy - cxw) / 2 + 120;

        PosMeForm (yw, xw, cyw, cxw);

        if (first == 0)
        {
                  ftaratxt.mask[0].pos[1] = xw;
                  ftaratxt.mask[0].pos[0] = yw - 40;

                  z = yw - 40;
                  s = xw;
                  GetTextPos (ftaratxt.font, &z, &s);
                  s += 7;
                  KonvTextAbsPos (ftaratxt.font, &z, &s);
                  ftara.mask[0].pos[0] = z + 10;
                  ftara.mask[0].pos[1] = s;

                  s = strlen (ftaratxt.mask[0].feld); 
                  FontTextLen (ftaratxt.font, ftaratxt.mask[0].feld, &s, &z);
                  ftaratxt.mask[0].length = s;
                  ftaratxt.mask[0].rows   = z;

                  s = 8;
                  FontTextLen (ftara.font, "0000.000", &s, &z);
                  ftara.mask[0].length = s;
                  ftara.mask[0].rows   = z;
                  first = 1;
        }

        WiegWindow  = CreateWindowEx (
                              0,
                              "AufWieg",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
        if (WiegWindow == NULL) return;
        EnableWindow (hMainWindow, FALSE); 

        WiegY = yw + cyw;
        wiegx  = xw;
        wiegy  = yw;
        wiegcx = cxw;
        wiegcy = cyw;

        WiegKg  = CreateWindowEx (
                              0,
                              "AufWiegKg",
                              "",
                              WS_CHILD | WS_BORDER,
                              xw, yw,
                              cxw, cyw,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        cxc = cx - 20;
        cyc = 150;
        xc  = 10;
        yc  = cy - 160;

        if (ohne_preis)
        {
              WieButton.mask[1].attribut = REMOVED;
              GetZentrierWerte (&buglen, &buabs, cxc, 5, babs, bsize);
              WieButton.mask[0].pos[1] = (cxc - buglen) / 2;
              WieButton.mask[2].pos[1] = WieButton.mask[0].pos[1] + buabs;
              WieButton.mask[3].pos[1] = WieButton.mask[2].pos[1] + buabs;
              WieButton.mask[4].pos[1] = WieButton.mask[3].pos[1] + buabs;
              WieButton.mask[5].pos[1] = WieButton.mask[4].pos[1] + buabs;
        }
        else
        {
              WieButton.mask[1].attribut = COLBUTTON;
              GetZentrierWerte (&buglen, &buabs, cxc, 6, babs, bsize);
              WieButton.mask[0].pos[1] = (cxc - buglen) / 2;
              WieButton.mask[1].pos[1] = WieButton.mask[0].pos[1] + buabs;
              WieButton.mask[2].pos[1] = WieButton.mask[1].pos[1] + buabs;
              WieButton.mask[3].pos[1] = WieButton.mask[2].pos[1] + buabs;
              WieButton.mask[4].pos[1] = WieButton.mask[3].pos[1] + buabs;
              WieButton.mask[5].pos[1] = WieButton.mask[4].pos[1] + buabs;
        }

        GetZentrierWerte (&buglen, &buabs, cxc, 6, babs2, bsize2);
        WieButtonT.mask[0].pos[1] = (cxc - buglen) / 2;
        WieButtonT.mask[1].pos[1] = WieButtonT.mask[0].pos[1] + buabs;
        WieButtonT.mask[2].pos[1] = WieButtonT.mask[1].pos[1] + buabs;
        WieButtonT.mask[3].pos[1] = WieButtonT.mask[2].pos[1] + buabs;
        WieButtonT.mask[4].pos[1] = WieButtonT.mask[3].pos[1] + buabs;
        WieButtonT.mask[5].pos[1] = WieButtonT.mask[4].pos[1] + buabs;

        WiegCtr  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "AufWiegCtr",
                              "",
                              WS_CHILD,
                              xc, yc,
                              cxc, cyc,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);

        ShowWindow (WiegWindow, SW_SHOW);
        UpdateWindow (WiegWindow);
        ShowWindow (WiegKg, SW_SHOW);
        UpdateWindow (WiegKg);
        ShowWindow (WiegCtr, SW_SHOW);
        UpdateWindow (WiegCtr);
        create_enter_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        current_wieg = TestAktivButton (PLUS, 0);
        SetFocus (WieButton.mask[current_wieg].feldid);

        WiegenAktiv = 1;
        current_wieg = 0;
        break_wieg = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsWiegMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_wieg) break;
        }
        if (AlarmWindow)
        {
                     CloseControls (&alform);
                     DestroyWindow (AlarmWindow);
                     AlarmWindow = NULL;
        }
        EnableWindow (hMainWindow, TRUE); 
        CloseControls (&WiegKgT);
        CloseControls (&WiegKgE);
        CloseControls (&WieButton);
        CloseControls (&artwform);
        CloseControls (&artwformT);
        CloseControls (&artwconst);
        CloseControls (&artwconstT);
        CloseControls (&WiegKgMe);
        CloseControls (&ftara);
        CloseControls (&ftaratxt);
        DestroyWindow (WiegWindow);
//        SetListEWindow (1);
        WiegenAktiv = 0;
}

/** Ab hier Auswahl fuer Preiseingabe 
**/

static HWND ePreisWindow;
static break_preis_enter = 0;


mfont PreisTextFont = {"Arial", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};


mfont preisfont   = {"", 120, 0, 1,
                     RGB (255, 0, 0),
                     BLUECOL,
                     1,
                     NULL};


ColButton BuVkPreis =   {"&Rampen", -1, 25,
                         "Preis", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         2};

ColButton BuLadPreis =   {"&Laden", -1, 25,
                          "Preis", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         2};

ColButton BuPreisOK =   {"&Zur�ck", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
                          REDCOL,
                          2};

static int dovk ();
static int dolad ();
static int doprok ();

static HWND fhWnd;
static int pnbss0 = 80;
int pny    = 3;
int pnx    = 1;
static int pcbss0 = 100;
static int pcbss  = 64; 
static int pcbsz  = 64;
static int pcbsz0 = 100;

static field _PrWahl[] = {
(char *) &BuVkPreis,      pcbss0, pcbsz0, pny, pnx,        0, "",
                                                           COLBUTTON, 0,
                                                           dovk, 0,
(char *) &BuLadPreis,     pcbss0, pcbsz0, pny, pnx + 1 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dolad, 0,
(char *) &BuPreisOK,      pcbss0, pcbsz0, pny, pnx + 2 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           doprok, 0};

static form PrWahl = {3, 0, 0, _PrWahl, 0, 0, 0, 0, &preisfont};

static char preisbuff [256];

static field _PrTextForm [] = {
preisbuff,             0, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};

static form PrTextForm = {1, 0, 0, _PrTextForm, 0, 0, 0, 0, &PreisTextFont};

void PreisEnterBreak ()
/**
Endeflag fuer Erfassung setzen.
**/
{
        break_preis_enter = 1;
}

int dovk (void)
{
        char preis [20];

        strcpy (preis, aufps.auf_pr_vk);
        EnterNumBox (ePreisWindow,"  Rampen-Preis  ", preis, 8, "%6.2f");
        SetActiveWindow (ePreisWindow);
        if (strcmp (preis, "0") > 0)
        {
                   strcpy (aufps.auf_pr_vk, preis);
                   if (InsWindow)
                   {
                             InvalidateRect (InsWindow, NULL, TRUE);
                             UpdateWindow (InsWindow);
                   }
        }
        SetFocus (fhWnd);
        return 0;
}

int dolad (void)
{
        char preis [20];


        strcpy (preis, aufps.auf_lad_pr);
        EnterNumBox (ePreisWindow,"  Laden-Preis  ", preis, 8, "%6.2f");
        if (strcmp (preis, "0") > 0)
        {
                   strcpy (aufps.auf_lad_pr, preis);
                   if (InsWindow)
                   {
                             InvalidateRect (InsWindow, NULL, TRUE);
                             UpdateWindow (InsWindow);
                   }
        }
        SetActiveWindow (ePreisWindow);
        SetFocus (fhWnd);
        return 0;
}

int doprok (void)
{
        PreisEnterBreak ();
        return 0;
}

HWND IsPreisChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < PrWahl.fieldanz; i ++)
        {
                   if (MouseinWindow (PrWahl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (PrWahl.mask[i].feldid);
                   }
        }

        return NULL;
}


LONG FAR PASCAL PreisProc(HWND hWnd,UINT msg,
                          WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
        HWND enchild;
        static int SendChild = 0;
        int HasFocus = 1;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    display_form (ePreisWindow, &PrWahl, 0, 0);
                    SetFocus (fhWnd);
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    enchild = IsPreisChild (&mousepos);
                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
                    return TRUE;
              case WM_ACTIVATE :
                    switch (wParam)
                    {
                         case WA_ACTIVE :
                         case WA_CLICKACTIVE :
                              SetCapture (ePreisWindow);
                              break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void RegisterPreis (void)
/**
Fenster fuer Preis registrieren.
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  PreisProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (LTGRAYCOL);
                   wc.lpszClassName =  "EnterPreis";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

int PrGetAktButton (void)
/**
Aktiven Button holen.
**/
{
        HWND hWnd;
        int i;

        hWnd = GetFocus ();

        for (i = 0; i < PrWahl.fieldanz; i ++)
        {
                    if (hWnd == PrWahl.mask[i].feldid)
                    {
                               return (i);
                    }
        }
        return (0);
}


void PrPrevKey (void)
/**
Vorhergenden Button aktivieren.
**/
{
         int AktButton;

         AktButton = PrGetAktButton (); 
         if (AktButton == 0)
         {
                     AktButton = PrWahl.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         SetFocus (PrWahl.mask [AktButton].feldid);
         fhWnd = GetFocus ();
}

void PrNextKey (void)
/**
Naechsten Button aktivieren.
**/
{
         int AktButton;

         AktButton = PrGetAktButton (); 
         if (AktButton == PrWahl.fieldanz - 1)
         {
                     AktButton = 0;
         }
         else
         {
                     AktButton ++;
         }
         SetFocus (PrWahl.mask [AktButton].feldid);
         fhWnd = GetFocus ();
}

void PrActionKey (void)
/**
Button-Aktion ausfuehren.
**/
{
         int AktButton;
         field *feld;

         AktButton = PrGetAktButton ();
         feld =  &PrWahl.mask[AktButton];

         if (feld->after != (int (*) (void)) 0)
         {
                    (*feld->after) ();
         }
         else if (feld->BuId != 0)
         {
                    SendMessage (GetParent (feld->feldid), WM_COMMAND,
                                            MAKELONG (feld->BuId,
                                            feld->feldid),
                                            0l);
         }
}
         
int IsPreisKey (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         ColButton *ColBut;
         int taste;
         int i;
         char *pos;
         
         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     if (keypressed)
                     {
                                  return FALSE;
                     }
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                               PreisEnterBreak ();
                               syskey = KEY5;
                               return TRUE;
                            case  VK_UP :
                               PrPrevKey ();
                               return TRUE;
                            case VK_DOWN :
                               PrNextKey ();
                               return TRUE;
                            case VK_RIGHT :
                               PrNextKey ();
                               return TRUE;
                            case VK_LEFT :
                               PrPrevKey ();
                               return TRUE;
                            case VK_TAB :
                               if (GetKeyState (VK_SHIFT) < 0)
                               {
                                      PrPrevKey ();
                                      return TRUE;
                               }
                               PrNextKey ();
                               return TRUE;
                             case VK_RETURN :
                               PrActionKey ();
                               return TRUE;
                     }
                     taste = (int) msg->wParam;
                     for (i = 0; i < 3; i ++)
                     {
                        ColBut = (ColButton *) PrWahl.mask[i].feld;
                        if (ColBut->text1)
                        {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = PrWahl.mask[i].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                       }
                     }
                     return FALSE;
              }
              case WM_KEYUP :
              {
                     taste = (int) msg->wParam;
                     for (i = 0; i < 3; i ++)
                     {
                         if (keyhwnd == PrWahl.mask[i].feldid)
                         {
                                   keypressed = 0;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                         }
                     }
                     return FALSE;
              }
          }
          return FALSE;
}


void EnterPreisBox (HWND hWnd, int px, int py)
/**
Fenster fuer Preisauswahl oeffnen.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
        HCURSOR oldcursor;
        static int BitMapOK = 0;

        RegisterPreis ();
        GetWindowRect (hWnd, &rect);
        cx = 3 * pcbss0 + 7;
        cy = pcbsz0 + 10;
        if (px == -1)
        {
                  x = rect.left + (rect.right - rect.left - cx) / 2;
        }
        else
        {
                  x = px;
        }
        if (py == -1)
        {
                  y = (rect.bottom - cy) / 2;
        }
        else
        {
                  y = py;
        }

        ePreisWindow  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "EnterPreis",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

        ShowWindow (ePreisWindow, SW_SHOW);
        UpdateWindow (ePreisWindow);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));
        SetFocus (PrWahl.mask[0].feldid);
        fhWnd = GetFocus ();
        SetCapture (ePreisWindow);

        PreisAktiv = 1;
        break_preis_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsPreisKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_preis_enter) break;
        }
        CloseControls (&PrWahl);
        ReleaseCapture ();
        DestroyWindow (ePreisWindow);
        PreisAktiv = 0;
        SetCursor (oldcursor);
}

/*

Auswahl fuer Festtara.

*/

struct PT
{
        char ptbez [33];
        char ptwer1 [9];
};

struct PT ptab, ptabarr [20];
static int ptabanz = 0;


static int ptidx;

mfont PtFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};

static field _ftaraform [] =
{
        ptab.ptbez,     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptab.ptwer1,     9, 1, 0,13, 0, "%7.3f", DISPLAYONLY, 0, 0, 0
};

static form ftaraform = {2, 0, 0, _ftaraform, 0, 0, 0, 0, &PtFont};

static field _ftaraub [] =
{
        "Bezeichnung",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Tara",             9, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0
};

static form ftaraub = {2, 0, 0, _ftaraub, 0, 0, 0, 0, &PtFont};

static field _ftaravl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
};

static form ftaravl = {1, 0, 0, _ftaravl, 0, 0, 0, 0, &PtFont};

int ShowFestTara (char *pttara)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (ptabanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("tara_fest");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabarr[ptabanz].ptbez,  ptabn.ptbez);
                   strcpy (ptabarr[ptabanz].ptwer1, ptabn.ptwer1);
                   ptabanz ++;
                   if (ptabanz == 20) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        EnableWindow (WiegWindow, FALSE);
        ptidx = ShowPtBox (WiegWindow, (char *) ptabarr, ptabanz,
                     (char *) &ptab,(int) sizeof (struct PT),
                     &ftaraform, &ftaravl, &ftaraub);
        EnableWindow (WiegWindow, TRUE);
        strcpy (pttara, ptabarr [ptidx].ptwer1);
        return ptidx;
}

/*

Auswahl fuer Drucker.

*/

struct DR
{
        char drucker [33];
};

struct DR ldrucker, ldruckarr [20];
static int ldruckanz = 0;
static int ldidx;

mfont DrFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};
static field _ldruckform [] =
{
        ldrucker.drucker,    22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ldruckform = {1, 0, 0, _ldruckform, 0, 0, 0, 0, &DrFont};

static field _ldruckub [] =
{
        "Drucker",     22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ldruckub = {1, 0, 0, _ldruckub, 0, 0, 0, 0, &DrFont};

void BelegeDrucker (void)
/**
Drucker aus Formatverzeichnis holen.
**/
{
      char frmdir [256];
      char *frmenv;
      WIN32_FIND_DATA lpffd;
      HANDLE hFile;
      char *p;
      int i;
      static int druckerOK = 0;

      if (druckerOK) return;

      druckerOK = 1;
      frmenv = getenv ("FRMPATH");
      if (frmenv == NULL) return;

      sprintf (frmdir, "%s\\*.cfg", frmenv);


      hFile = FindFirstFile (frmdir, &lpffd);

      if (hFile == INVALID_HANDLE_VALUE)
      {
                      return;
      }

      i = 0;
      p = strchr (lpffd.cFileName, '.');
      if (p) *p = (char) 0;

      strcpy (ldruckarr[i].drucker, lpffd.cFileName);
      i ++;

      while (FindNextFile (hFile, &lpffd))
      {
               p = strchr (lpffd.cFileName, '.');
               if (p) *p = (char) 0;
               strcpy (ldruckarr[i].drucker, lpffd.cFileName);
               i ++;
      }
      FindClose (hFile);

      ldruckanz = i;
}

void drucker_akt (char *dname)
/**
Ausgewaehlten Drucker in drucker.akt_uebertragen.
**/
{
        char ziel [256];
        char quelle [256];

        sprintf (quelle, "%s\\%s.cfg", getenv ("FRMPATH"), dname);
        sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
        CopyFile (quelle, ziel, FALSE);
        sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 clipped (sys_ben.pers_nam),
                                 "akt");
        CopyFile (quelle, ziel, FALSE);
}

int DruckChoise ()
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        char aktdrucker [80];

        BelegeDrucker ();
        ldidx = ShowPtBox (eWindow, (char *) ldruckarr, ldruckanz,
                     (char *) &ldrucker,(int) sizeof (struct DR),
                     &ldruckform, NULL, &ldruckub);
        strcpy (aktdrucker, ldruckarr [ldidx].drucker);
        clipped (aktdrucker);
        drucker_akt (aktdrucker);
        return ldidx;
}


/** Ptabanzeige
**/

static int ptbss0 = 73;
static int ptbss  = 77; 
static int ptbsz  = 40;
static int ptbsz0 = 36;

field _PtButton[] = {
(char *) &Back,         ptbss0, ptbsz0, -1, 4+0*ptbss, 0, "", COLBUTTON, 0,
 *                                                        func5, 0,
(char *) &BuOK,         ptbss0, ptbsz0, -1, 4+1*ptbss, 0, "", COLBUTTON, 0,
 *                                                        func6, 0,
(char *) &PfeilU,       ptbss0, ptbsz0, -1, 4+2*ptbss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,       ptbss0, ptbsz0, -1, 4+3*ptbss, 0, "", COLBUTTON, 0,
                                                          func2, 0
};

form PtButton = {4, 0, 0, _PtButton, 0, 0, 0, 0, &buttonfont};


void IsPtClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        ptidx = idx;
        break_list ();
        return;
}

void RegisterPt (void)
/**
Kopf - und Fuss - Fenster fuer Liste registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  PtFussProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "PtFuss";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

HWND IsPtFussChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < PtButton.fieldanz; i ++)
        {
                   if (MouseinWindow (PtButton.mask[i].feldid,
                                      mousepos))
                   {
                                 return (PtButton.mask[i].feldid);
                   }
        }
        return NULL;
}

int MouseInPt (POINT *mpos)
{
        HWND hListBox;

        return TRUE;
        hListBox = GethListBox ();

        if (MouseinDlg (hListBox, mpos)) return TRUE;
        if (MouseinDlg (PtFussWindow, mpos)) return TRUE;
        return FALSE;
}


LONG FAR PASCAL PtFussProc(HWND hWnd,UINT msg,
                           WPARAM wParam,LPARAM lParam)
{
        HWND enchild;
        POINT mousepos;
        HWND lbox;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    display_form (PtFussWindow, &PtButton, 0, 0);
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    enchild = IsPtFussChild (&mousepos);
                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
                    lbox = Getlbox ();
                    if (MouseinWindow (lbox, &mousepos))
                    {
                              ScreenToClient (lbox, &mousepos);
                              lParam = MAKELONG (mousepos.x, mousepos.y);
                              SendMessage (lbox, msg, wParam, lParam);
                              return TRUE;
                    }
                    return TRUE;
              case WM_VSCROLL :
                    lbox = Getlbox ();
                    if (MouseinWindow (lbox, &mousepos))
                    {
                              SendMessage (lbox, msg, wParam, lParam);
                    }
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

int ShowPtBox (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
               int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15; 
        cy = 185;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x, 
//                                    y + cy + 23,
                                    y,
                                    cx + 24, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
//        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        init_break_list ();
//        SetMouseLock (FALSE);
//        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}

/**
Lieferschein auf komplett setzen.
**/

static int ldruck   = 0;
static int ldrfr    = 0;
static int lfrei    = 0;
static int lbar     = 0;
static int lsofort  = 0;
static int drwahl   = 0;

static int *lbuttons [] = {&ldruck, &ldrfr, &lfrei, &lbar, &lsofort,
                          &drwahl, NULL};

static int Setldruck (void);
static int Setldrfr (void);
static int Setlfrei (void);
static int Setlbar (void);
static int Setlsofort (void);
static int Setdrwahl (void);
static int BreakBox (void);
static int BoxBreak (void);

static int IsDrChoise;


static field _cbox [] = {
" LS-Drucken      ",     22, 2, 0, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setldruck, 101,
" Freig.+ Druck   ",     19, 2, 2, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setldrfr, 102,
" Freigeben       ",     19, 2, 4, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setlfrei, 103,
" Barverkauf      ",     19, 2, 6, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setlbar, 104,
" Sofortrechnung  ",     19, 2, 8, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setlsofort, 105,
" Druckerwahl     ",     19, 2,10, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setdrwahl, 106,
"   OK    ",             10, 1,13, 2,  0, "", BUTTON, 0, BreakBox, 107,
" Abbruch ",             10, 1,13, 15, 0, "", BUTTON, 0, BoxBreak, KEY5};

form cbox = {8, 0, 0, _cbox, 0,0,0,0,&ListFont};

int KomplettEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

void UnsetBox (int pos)
/**
Buttona auf selectierte setzen. (ausser pos);
**/
{
        int i;

        for (i = 0; lbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (cbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *lbuttons [i] = 0;
        }
}


int Setldruck (void)
/**
Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       ldruck = 0;
          }
          else
          {
                       ldruck = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       !ldruck, 0l);
                       ldruck = !ldruck;
          }
          UnsetBox (0);
          return 1;
}

int Setldrfr (void)
/**
Freigabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        ldrfr = 0;
          }
          else
          {
                        ldrfr = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       !ldrfr, 0l);
                       ldrfr = !ldrfr;
          }
          UnsetBox (1);
          return 1;
}

int Setlfrei (void)

/**
Freigabe setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[2].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lfrei = 0;
          }
          else
          {
                       lfrei = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[2].feldid, BM_SETCHECK,
                       !lfrei, 0l);
                       lfrei = !lfrei;
          }
          UnsetBox (2);
          return 1;
}

int Setlbar (void)

/**
Barverkauf setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[3].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lbar = 0;
          }
          else
          {
                       lbar = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[3].feldid, BM_SETCHECK,
                       !lbar, 0l);
                       lbar = !lbar;
          }
          UnsetBox (3);
          return 1;
}

int Setlsofort (void)

/**
Sofortrechnung setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[4].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lsofort = 0;
          }
          else
          {
                       lsofort = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[4].feldid, BM_SETCHECK,
                       !lsofort, 0l);
                       lsofort = !lsofort;
          }
          UnsetBox (4);
          return 1;
}

int Setdrwahl (void)

/**
Sofortrechnung setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          DruckChoise ();
          set_fkt (KomplettEnd, 5);
          SetFocus (cbox.mask[5].feldid);
          return 1;
          /*
          if (SendMessage (cbox.mask[5].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       drwahl = 0;
          }
          else
          {
                       drwahl = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[5].feldid, BM_SETCHECK,
                       !drwahl, 0l);
                       drwahl = !drwahl;
          }
          UnsetBox (5);
          return 1;
          */
}


int BreakBox (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEYUP || syskey == KEYDOWN) return 0;
           syskey = KEYCR;
           break_enter ();
           return 1;
}

int BoxBreak (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEYUP || syskey == KEYDOWN) return 0;
           syskey = KEY5;
           break_enter ();
           return 1;
}

int MouseInButton (POINT *mpos)
{
        int i;

        if (MouseinDlg (MainButton2.mask[2].feldid, mpos))
        {
                  return TRUE;
        }

        for (i = 5; i < 7; i ++)
        {
                if (MouseinDlg (MainButton2.mask[i].feldid, mpos))
                {
                                   return TRUE;
                }
        }
        return FALSE;
}

static void SelectAktCtr (void)
/**
Aktiven Button in EnterButton selectieren.
**/
{
        syskey = KEYCR;
        if (cbox.mask[currentfield].after)
        {
                       (*cbox.mask[currentfield].after) ();
        }
        syskey = KEYCR;
}

BOOL SetLskStatus (int ls_stat)
/**
ls_stat in lsk auf 3 setzen.
**/
{
//        if (lsk.ls_stat >= ls_stat) return FALSE;
        beginwork ();
        if (Lock_Lsk () == FALSE) 
        {
            commitwork ();
            return FALSE;
        }
        if (TestPosStatus () == FALSE) return FALSE;
        if (LsLeerGut () == FALSE) return FALSE;

        beginwork ();
        lsk.ls_stat = ls_stat;
        ls_class.update_lsk (lsk.mdn, lsk.fil, lsk.ls);
        commitwork ();
        return TRUE;
}

void BearbKomplett (void)
/**
Auswahl aus Komplett bearbeiten.
**/
{
         char progname [256];

         if (strcmp (sys_ben.pers_nam, " ") <= 0)
         {
             strcpy (sys_ben.pers_nam, "fit");
         }
         commitwork ();
         if (lsk.kun_fil == 0 && kun.kun_typ == BAR)
         {
                ldruck = 0;
                lbar = 1;
         }

         else if (lsk.kun == 0 && kun.sam_rech == SOFORT &&
                  kun.kun_typ != 4 && kun.kun_typ != 7 &&
                  kun.kun_typ != 10)
         {
                ldruck = 0;
                lsofort = 1;
         }

         if (ldruck)
         {
                sprintf (progname, "%s\\BIN\\rswrun beldr L "
                                     "%hd %hd %ld %s",
                                     rosipath,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     sys_ben.pers_nam);
                ProcExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
         }
         else if (ldrfr)
         {
                sprintf (progname, "%s\\BIN\\rswrun beldr L "
                                     "%hd %hd %ld %s",
                                     rosipath,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     sys_ben.pers_nam);
                ProcExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
         }
         else if (lfrei)
         {
                if (lsk.ls_stat < 4)
                {
                             disp_messG ("Der Lieferschein mu� zuerst gedruckt werden", 2);
                             return;
                }  
                sprintf (progname, "%s\\BIN\\rswrun beldr F "
                                     "%hd %hd %ld %s",
                                     rosipath,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     sys_ben.pers_nam);
                ProcExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
         }
         else if (lbar)
         {
                sprintf (progname, "%s\\BIN\\rswrun beldr R "
                                     "%hd %hd %ld %s B",
                                     rosipath,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     sys_ben.pers_nam);
                ProcExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
          }
         else if (lsofort)
         {
                sprintf (progname, "%s\\BIN\\rswrun beldr R "
                                     "%hd %hd %ld %s R",
                                     rosipath,
                                     lsk.mdn, lsk.fil, lsk.ls,
                                     sys_ben.pers_nam);
                 ProcExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
          }
          beginwork ();
}

void LskNew (void)
{
         int dir;
         int cursor;
                   
         aufanz = 0;
         cursor = cursor_lsk_auf_ausw;
         dir = FIRST; 
         open_sql (cursor);
         while (fetch_scroll (cursor, dir) == 0)
         {
                dir = NEXT;
                kun_class.lese_kun (1, 0, lsk.kun);
                sprintf (aufsarr[aufanz].auf, "%8ld", lsk.auf);
                sprintf (aufsarr[aufanz].ls,  "%8ld", lsk.ls);
                sprintf (aufsarr[aufanz].kun, "%8ld",  lsk.kun);
                dlong_to_asc (lsk.lieferdat, aufsarr[aufanz].lieferdat);
                dlong_to_asc (lsk.komm_dat, aufsarr[aufanz].komm_dat);
                strcpy  (aufsarr[aufanz].kun_krz1, kun.kun_krz1);
                sprintf (aufsarr [aufanz].ls_stat, "%1hd", lsk.ls_stat);
                aufanz ++;
                if (aufanz == AUFMAX) break;
         }

         ShowNewElist ((char *) aufsarr,
                        aufanz,
                        (int) sizeof (struct AUF_S));
         if (aufanz == 0) 
         {
             syskey = KEY5;
             break_list ();
         }
}


int SetKomplett ()
/**
Komplett setzen.
**/
{
         int x,y;
         int cx, cy;
         int mx;
         RECT rect;
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont; 
         form *currents;

         lsk.ls      = atol (aufsarr[aufidx].ls);
         lsk.ls_stat = atoi (aufsarr[aufidx].ls_stat);
         if (lsk.ls_stat > 4)
         {
                             disp_messG ("Der Lieferschein wurde schon freigegeben", 2);
                             return (0);
         }  


         if (SetLskStatus (3) == FALSE) 
         {
             return 0; 
         }

         sprintf (aufs.ls_stat, "%1d", lsk.ls_stat); 
         currents = current_form;
         set_fkt (KomplettEnd, 5);
         spezfont (&ListFont);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (hFont);
         ReleaseDC (hMainWindow, hdc);
         GetClientRect (eWindow, &rect);
         mx = rect.right / tm.tmAveCharWidth;
         GetFrmSize (&cbox, &cx, &cy);

         x = max (0, (mx - cx) / 2);

         y = 1;
         cy = 13;

         KomplettAktiv = 1;

         ldruck = ldrfr = lfrei = lbar = lsofort = drwahl = 0;
         SetMouseLockProc (MouseInButton);
         SetBorder (WS_VISIBLE | WS_DLGFRAME | WS_POPUP);
         if (EnterButtonWindow (eWindow, &cbox, y, x, cy, cx) == -1)
         {
                        KomplettAktiv = 0;
                        set_fkt (endlist, 5);
                        if (aufub.mask[0].attribut & BUTTON)
                        {
                                      CloseControls (&aufub); 
                        }
                        display_form (Getlbox (), &aufub, 0, 0);
                        LskNew ();
                        current_form = currents;
                        return 0;
         }

         if (aufub.mask[0].attribut & BUTTON)
         {
                    CloseControls (&aufub); 
         }
         display_form (Getlbox (), &aufub, 0, 0);
         KomplettAktiv = 0;
         SetMouseLockProc (NULL);
         BearbKomplett ();
         set_fkt (endlist, 5);

         LskNew ();
         current_form = currents;
         return 0;
}


/*   Gebinde eingeben          */

mfont GebUbFont = {"Courier New", 190, 0, 1, RGB (255, 255, 255),
                                             GebColor,
                                             0,
                                             NULL};
mfont GebFont   = {"Courier New", 190, 0, 1, RGB (255, 255, 255),
                                             GebColor,
                                             0,
                                             NULL};

static char gebinde [4] = {"N"};
static char palette [4] = {"N"};
static char geb_einh [4] = {"1"}; 
static char pal_einh [4] = {"1"}; 
static char geb_me [9];
static char pal_me [9];
static char gtara [9];
static char gtara_einh [3] = {"2"};

static int gebcx = 620;
static int gebcy = 400;

static int break_geb = 0;

static int buttony = gebcy - bsz0 - 20;
static int buttonpos = 5;

static int current_geb = 0;
static int gebend = 6;

static int gbanz = 0;

static int testgebinde ();
static int testpalette ();
static int testgeb_einh ();
static int testpal_einh ();

static int gebfunc1 (void);
static int gebfunc4 (void);

field _gebub[] = {
           "Etikett",  8, 0, 1,10, 0, "", DISPLAYONLY, 0, 0, 0, 
           "Einheit",  8, 0, 1,20, 0, "", DISPLAYONLY, 0, 0, 0, 
           "Menge  ",  0, 0, 1,30, 0, "", DISPLAYONLY, 0, 0, 0,
           "Gebinde",  10, 0, 3,  1, 0, "",      DISPLAYONLY, 0, 0, 0,           
           "Palette",  10, 0, 5,  1, 0, "",      DISPLAYONLY, 0, 0, 0,           
           gtara_einh,  3, 0, 7, 22, 0, "%2d",   READONLY, 0, 0, 0,
           "Einzel-Tara", 11, 0, 7,  1, 0, "",   DISPLAYONLY, 0, 0, 0,           
};

form gebub = {7, 0, 0, _gebub, 0, 0, 0, 0, &GebUbFont}; 


field _gebform[] = {

         gebinde,     2, 0, 3, 13, 0, "",      NORMAL, 0, testgebinde, 0,
         geb_einh,    3, 0, 3, 22, 0, "%2d",   NORMAL, 0, testgeb_einh, 0,
         geb_me,      8, 0, 3, 30, 0, "%3.3f", NORMAL, 0, 0, 0,

         palette,     2, 0, 5, 13, 0, "",      NORMAL, 0, testpalette, 0,
         pal_einh,    3, 0, 5, 22, 0, "%2d",   NORMAL, 0, testpal_einh, 0,
         pal_me,      8, 0, 5, 30, 0, "%3.3f", NORMAL, 0, 0, 0,
         gtara,       8, 0, 7, 30, 0, "%3.3f", NORMAL, 0, 0, 0,
};

form gebform = {7, 0, 0, _gebform, 0, 0, 0, 0, &GebFont};
                                              
field _GebButton[] = {
(char *) &Back,              bss0, bsz0, buttonpos, 4+5*bss, 0, "", COLBUTTON, 0,
 *                                                              func5, 0,
(char *) &BuOK,              bss0, bsz0, buttonpos, 4+4*bss, 0, "", COLBUTTON, 0,
 *                                                              func6, 0,
(char *) &PfeilR,            bss0, bsz0, buttonpos, 4+3*bss, 0, "", COLBUTTON, 0,
                                                          gebfunc4, 0,
(char *) &PfeilU,            bss0, bsz0, buttonpos, 4+2*bss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,            bss0, bsz0, buttonpos, 4+1*bss, 0, "", COLBUTTON, 0,
                                                          func2, 0,
(char *) &PfeilL,            bss0, bsz0, buttonpos, 4,     0, "", COLBUTTON, 0,
                                                          gebfunc1, 0};

form GebButton = {6, 0, 0, _GebButton, 0, 0, 0, 0, &buttonfont};

int gebfunc1 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{

         SetFocus (gebform.mask[current_geb].feldid);
         SendMessage (gebform.mask[current_geb].feldid, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
         return 1;
}
          
int gebfunc4 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{

         SetFocus (gebform.mask[current_geb].feldid);
         SendMessage (gebform.mask[current_geb].feldid, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
         return 1;
}

void PrintGLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
//         RECT rect, frect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;

         hdc = BeginPaint (GebWindow, &ps);

/*
         GetFrmRectEx (&gebform, &GebFont, &frect);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = gebcx - 4;
         y = frect.top - 30;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);
*/

         y = buttony - 10;
         x = gebcx - 4;

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}

void SetGebFocus (void)
/**
Focus fuer Wiegmaske setzen.
**/
{

          SetFocus (gebform.mask[current_geb].feldid);
          SendMessage (gebform.mask[current_geb].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
}


LONG FAR PASCAL GebProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == GebWindow)
                    {
                                 display_form (GebWindow, &gebub, 0, 0);
                                 display_form (GebWindow, &gebform, 0, 0);
                        //        PrintGLines ();
                                 SetGebFocus ();
                    }
                    else if (hWnd == GebWindButton)
                    {
                                 display_form (GebWindButton, &GebButton, 0, 0);
                    }
                    break;
              case WM_KEYDOWN :
                         switch (wParam)
                         {
                              case VK_F5 :
                                      func5 ();
                                      return 0;

                         }
                         break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    break;
              case WM_DESTROY :
                    if (hWnd == hMainWindow)
                    {
                             ExitProcess (0);
                             return 0;
                    }
                    return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

static int testgeb_einh (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
/*
	       if (ptab_class.lese_ptab ("me_einh_fill", 
                   geb_einh)
			!= 0)
		{
					strcpy (geb_einh, "0");
		}
*/
            display_form (GebWindow, &gebform, 0, 0);
		return 0;
}

static int testpal_einh (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
/*
	       if (ptab_class.lese_ptab ("me_einh_fill", 
                   pal_einh)
			!= 0)
		{
					strcpy (pal_einh, "0");
		}
*/
            display_form (GebWindow, &gebform, 0, 0);
		return 0;
}


int testgebinde ()
/**
Eingabe fuer Gebinde testen.
**/
{
         if (gebinde[0] == 'J' || gebinde[0] == 'j')
         {
                            gebinde[0] = 'J';
                            display_form (GebWindow, &gebform, 0, 0);
                            return 0;
         }

         if (atoi (gebinde))
         {
                            gebinde[0] = 'J';
                            display_form (GebWindow, &gebform, 0, 0);
                            return 0;
         }
         gebinde[0] = 'N';
         display_form (GebWindow, &gebform, 0, 0);
         return 0;
}      

int testpalette ()
/**
Eingabe fuer Gebinde testen.
**/
{
         if (palette[0] == 'J' || palette[0] == 'j')
         {
                            palette[0] = 'J';
                            return 0;
         }

         if (atoi (palette))
         {
                            palette[0] = 'J';
                            return 0;
         }
         palette[0] = 'N';
         return 0;
} 

struct PT ptabmh, ptabmharr [30];
static int gebanz = 0;
     
static field _fptabform [] =
{
        ptabmh.ptbez,     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptabmh.ptwer1,     9, 1, 0,13, 0, "%2d", DISPLAYONLY, 0, 0, 0
};

static form fptabform = {2, 0, 0, _fptabform, 0, 0, 0, 0, &PtFont};

static field _fptabub [] =
{
        "Bezeichnung",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Wert",             9, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0
};

static form fptabub = {2, 0, 0, _fptabub, 0, 0, 0, 0, &PtFont};

static field _fptabvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
};

static form fptabvl = {1, 0, 0, _fptabvl, 0, 0, 0, 0, &PtFont};



int ShowGebMeEinh (char *me_einh)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (gebanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("me_einh_fill");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabmharr[gebanz].ptbez,  ptabn.ptbezk);
                   strcpy (ptabmharr[gebanz].ptwer1, ptabn.ptwert);
                   gebanz ++;
                   if (gebanz == 30) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        syskey = 0; 
        ptidx = ShowPtBox (GebWindow, (char *) ptabmharr, gebanz,
                     (char *) &ptabmh,(int) sizeof (struct PT),
                     &fptabform, &fptabvl, &fptabub);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                     strcpy (me_einh, ptabmharr [ptidx].ptwer1);
        }
        return ptidx;
}

int testgebfields (void)
/**
Eingabefelder testen.
**/
{
        if (current_geb == 0)
        {
                   testgebinde ();
        }
        else if (current_geb == 3)
        {
                   testpalette ();
        }
        else if (current_geb == 1)
        {
                   testgeb_einh ();
        } 
        else if (current_geb == 4)
        {
                   testpal_einh ();
        } 
        return (0);
}

void gebaktion (void)
/**
Aktion auf aktivem Feld.
**/
{
        switch (current_geb)
        {
                  case 0 :
                          if (gebinde[0] == 'J')
                          {
                                       gebinde[0] = 'N';
                          }
                          else
                          {
                                       gebinde[0] = 'J';
                          }
                          testgebinde (); 
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 1 : 
                          ShowGebMeEinh (geb_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 2 :
                          EnterNumBox (GebWindow,"  Gebinde-Menge  ",
                                       geb_me, gebform.mask[2].length,    
                                       gebform.mask[2].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
   
                  case 3 :
                          if (palette[0] == 'J')
                          {
                                       palette[0] = 'N';
                          }
                          else
                          {
                                       palette[0] = 'J';
                          }
                          testpalette (); 
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 4 : 
                          ShowGebMeEinh (pal_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 5 :
                          EnterNumBox (GebWindow,"  Paletten-Menge  ",
                                       pal_me, gebform.mask[5].length,    
                                       gebform.mask[5].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 6 :
                          EnterNumBox (GebWindow,"       Tara       ",
                                       gtara, gebform.mask[6].length,    
                                       gebform.mask[6].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
             }
}

int IsGebMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_ESCAPE :
                            case VK_F5 :
                                    break_geb = 1;
                                    return TRUE;
                            case VK_RETURN :
                                    gebaktion ();
                                    return TRUE; 
                            case VK_DOWN :
                                    testgebfields ();
                                    current_geb ++;
                                    if (current_geb > gebend)
                                                current_geb = 0;
                                    SetGebFocus ();
                                    return TRUE;
                            case VK_UP :
                                   testgebfields ();
                                   current_geb --;
                                   if (current_geb < 0)
                                                current_geb = gebend;
                                    SetGebFocus ();
                                    return TRUE;
                            case VK_TAB :
                                    testgebfields ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                             current_geb --;
                                             if (current_geb < 0)
                                                  current_geb = gebend;
                                     }
                                     else
                                     {
                                             current_geb ++;
                                             if (current_geb > gebend)
                                                  current_geb = 0;
                                     }
                                     SetGebFocus ();
                                     return TRUE;

                             case VK_F9 :
                                     syskey = KEY9;
                                     if (current_geb == 1)
                                     {
                                               ShowGebMeEinh (geb_einh);
                                               display_form (GebWindow, &gebform, 0, 0);
                                     }
                                     else if (current_geb == 4)
                                     {
                                               ShowGebMeEinh (pal_einh);
                                               display_form (GebWindow, &gebform, 0, 0);
                                     }
                                     return TRUE;          

                            default :
                                    return FALSE;   
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (gebform.mask[0].feldid, &mousepos))
                    {
                          current_geb = 0;
/*
                          EnterNumBox (GebWindow,"  Gebinde-Etikett  ",
                                       gebinde, gebform.mask[0].length,
                                       "%1d");
*/
                          if (gebinde[0] == 'J')
                          {
                                       gebinde[0] = 'N';
                          }
                          else
                          {
                                       gebinde[0] = 'J';
                          }
                          testgebinde (); 
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[1].feldid, &mousepos))
                    {
/*
                          EnterNumBox (GebWindow,"  Gebinde-Einheit  ",
                                       geb_einh, gebform.mask[1].length,    
                                       gebform.mask[1].picture);
*/
                          current_geb = 1;
                          ShowGebMeEinh (geb_einh);
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[2].feldid, &mousepos))
                    {
                          current_geb = 2;
                          EnterNumBox (GebWindow,"  Gebinde-Menge  ",
                                       geb_me, gebform.mask[2].length,    
                                       gebform.mask[2].picture);
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }

                    else if (MouseinWindow (gebform.mask[3].feldid, &mousepos))
                    {
                          current_geb = 3;
/*
                          EnterNumBox (GebWindow,"  Paletten-Etikett  ",
                                       palette, gebform.mask[3].length,
                                       "%1d");
*/
                          if (palette[0] == 'J')
                          {
                                       palette[0] = 'N';
                          }
                          else
                          {
                                       palette[0] = 'J';
                          }
                          testpalette (); 
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[4].feldid, &mousepos))
                    {
/*
                          EnterNumBox (GebWindow,"  Paletten-Einheit  ",
                                       pal_einh, gebform.mask[4].length,    
                                       gebform.mask[4].picture);
*/
                          current_geb = 4;
                          ShowGebMeEinh (pal_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[5].feldid, &mousepos))
                    {
                          current_geb = 5;
                          EnterNumBox (GebWindow,"  Paletten-Menge  ",
                                       pal_me, gebform.mask[5].length,    
                                       gebform.mask[5].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[6].feldid, &mousepos))
                    {
                          current_geb = 6; 
                          EnterNumBox (GebWindow, "       Tara       ",
                                       gtara, gebform.mask[6].length,    
                                       gebform.mask[6].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
              }
              case WM_USER :
                    if (LOWORD (msg->wParam) == BUTARA)
                    {
                          dotara ();
                          return TRUE;
                    }
         }
         return FALSE;
}


int dogebinde ()
/**
Parameter fuer Gebinde - und Palettenetikett eingeben.
**/
{
        MSG msg; 
        int  x, y;
        int cx, cy;

        cx = gebcx;
        x = (screenwidth - cx) / 2;
        cy = gebcy;
        y = (screenheight - cy) / 2;

        GebWindow  = CreateWindowEx (
                              0,
                              "GebWind",
                              "",
                              WS_POPUP | WS_DLGFRAME | WS_VISIBLE,
                              x, y,
                              cx, cy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        if (GebWindow == NULL) return (0);
 
        x = 5;
        cx -= 15;
        y = buttony;
        cy = bsz0 + 10; 
        
        GebWindButton  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "GebWindB",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              x, y,
                              cx, cy,
                              GebWindow,
                              NULL,
                              hMainInst,
                              NULL);

        EnableWindow (WiegWindow, FALSE); 
        create_enter_form (GebWindow, &gebform, 0, 0);
        current_geb = 0;
        SetGebFocus ();
        break_geb = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsGebMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_geb) break;
        }
        CloseControls (&gebub);
        CloseControls (&gebform);
        CloseControls (&GebButton);
        DestroyWindow (GebWindButton);
        DestroyWindow (GebWindow);
        GebWindow = NULL;
        SetActiveWindow (WiegWindow);
        EnableWindow (WiegWindow, TRUE); 
        update_kun_geb ();
        return (0);
}

void ToVorgabe (char *veinh, short einh)
/**
Vorgabe-Einheit als Klartext.
**/
{
        switch (einh)
        {
           case 1 :
                strcpy (veinh, "St�ck");
                break;
           case 2:
                strcpy (veinh, "kg");
                break;
           case 4:
                strcpy (veinh, "Preis");
                break;
           case 5:
                strcpy (veinh, "Karton");
                break;
           case 6:
                strcpy (veinh, "Palette");
                break;
           default:
                strcpy (veinh, "kg");
                break;
        }
}


void gebindelsp (void)
/**
Gebindevorgaben in lsp testen.
Wenn brauchbare Gebindewerte in der lsp vorhanden sind,
Gebindewerte aus a_kun_gx ueberschreiben.
**/
{
        short   me_einh_kun1;
        double  auf_me1;
        double  inh1;
        short   me_einh_kun2;
        double  auf_me2;
        double  inh2;
        short   me_einh_kun3;
        double  auf_me3;
        double  inh3;

        me_einh_kun1 = atoi (aufps.me_einh_kun1);
        auf_me1      = ratod (aufps.auf_me1);
        inh1         = ratod (aufps.inh1);

        me_einh_kun2 = atoi (aufps.me_einh_kun2);
        auf_me2      = ratod (aufps.auf_me2);
        inh2         = ratod (aufps.inh2);

        me_einh_kun3 = atoi (aufps.me_einh_kun3);
        auf_me3      = ratod (aufps.auf_me3);
        inh3         = ratod (aufps.inh3);

        if (me_einh_kun1 <= 0 &&
            me_einh_kun2 <= 0 &&
            me_einh_kun3 <= 0)
        {
            return;
        }

        if (me_einh_kun1 == 2)
        {
              a_kun_geb.geb_fill = 2;
              sprintf (geb_einh, "%hd", a_kun_geb.geb_fill); 
              ToVorgabe (veinh1, a_kun_geb.geb_fill);
        }
        else
        {
              a_kun_geb.geb_fill = 1;
              sprintf (geb_einh, "%hd", a_kun_geb.geb_fill); 
              ToVorgabe (veinh1, a_kun_geb.geb_fill);
        }

        if (me_einh_kun2)
        {
              a_kun_geb.pal_fill = 5;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill); 
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }

        if (auf_me2 > (double) 0.0 && me_einh_kun2)
        {
              if (inh3)
              {
                  auf_me2 = inh3;
              }
              sprintf (pal_me,   "%.0lf",  auf_me2); 
              strcpy (vorgabe2, pal_me);
        }

        if (auf_me1 > (double) 0.0 && me_einh_kun1)
        {
              if (inh2)
              {
                  auf_me1 = inh2;
              }
              sprintf (geb_me,   "%.3lf", auf_me1); 
              strcpy (vorgabe1, geb_me);
        }
}       


void fetch_std_geb (void)
/**
Standard-Gebindeeinstellungen aus a_kun lesen und in kun_geb uebertragen.
Der Eintrag wird nur temporaer benutzt        .
**/
{
        short sqm;
        extern short sql_mode;
        

        strcpy (a_kun_geb.geb_eti, "N"); 
        strcpy (a_kun_geb.pal_eti, "N"); 
        a_kun_geb.geb_fill = 0; 
        a_kun_geb.pal_fill = 0; 
        a_kun_geb.geb_anz = 0; 
        a_kun_geb.pal_anz = 0; 
        
        a_kun_geb.mdn = 1;
        a_kun_geb.fil = 0;
        a_kun_geb.kun = lsk.kun;   
        a_kun_geb.a   = ratod (aufps.a);
        a_kun_geb.tara   = 0;
        a_kun_geb.mhd   = 0;
        strcpy (gebinde, a_kun_geb.geb_eti);    

        sprintf (geb_einh, "%hd", a_kun_geb.geb_fill); 
        ToVorgabe (veinh1, a_kun_geb.geb_fill);

        sprintf (geb_me,   "%.3lf", 
            (double) ((double) a_kun_geb.geb_anz / 1000)); 
        strcpy (vorgabe1, geb_me);

        strcpy (palette, a_kun_geb.pal_eti);    

        sprintf (pal_einh, "%hd", a_kun_geb.pal_fill); 
        ToVorgabe (veinh2, a_kun_geb.pal_fill);

        sprintf (pal_me,   "%.3lf", 
            (double) ((double) a_kun_geb.pal_anz / 1000)); 
        strcpy (vorgabe2, pal_me);

        gebindelsp ();

        sprintf (gtara,     "%.3lf", a_kun_geb.tara); 

        if (tara_kz == 1)
        {
                  sprintf (gtara,     "%.3lf", a_hndw.tara); 
                  a_kun_geb.tara =  a_hndw.tara; 
        }
        if (mhd_kz == 1)
        {
              if (_a_bas.hbk_kz[0] == 'T' ||
                  _a_bas.hbk_kz[0] <= ' ')
              {
                  a_kun_geb.mhd =  _a_bas.hbk_ztr; 
              }
        }
        sprintf (aufps.tara,     "%.3lf", a_kun_geb.tara); 
        sqm = sql_mode;
        sql_mode = 1;
        geb_class.update_kun_geb ();
        sql_mode = sqm;
}

void updategeblsp (void)
/**
Gebinde in lsp aktialisieren.
**/
{
        short   me_einh_kun1;
        double  auf_me1;
        double  inh1;
        double  auf_me2;
        double  inh2;
        double  auf_me3;
        double  inh3;

        inh1         = ratod (aufps.inh1);
        inh2         = ratod (aufps.inh2);
        inh3         = ratod (aufps.inh3);

        auf_me3      = ratod (aufps.auf_me3);
        if (a_kun_geb.pal_fill == 5)
        {
               auf_me2      = a_kun_geb.pal_anz / 1000;
               sprintf (aufps.inh2, "%.3lf", auf_me2);
               if (inh3) auf_me2 *= auf_me3;
               sprintf (aufps.auf_me2, "%.3lf", auf_me2);
        }
        if (a_kun_geb.geb_fill == 1 ||
            a_kun_geb.geb_fill == 2)
        {
               if (atoi (aufps.me_einh_kun1) == 1 ||
                   atoi (aufps.me_einh_kun1) == 2)
               {
                       me_einh_kun1 = a_kun_geb.geb_fill;
                       sprintf (aufps.me_einh_kun1, "%hd", me_einh_kun1);
               }
               auf_me1      = a_kun_geb.geb_anz / 1000;
               sprintf (aufps.inh1, "%.3lf", auf_me1);
               if (inh2)    
               {
                   auf_me1 *= auf_me2;
               }
               else if (inh3)
               {
                   auf_me1 *= auf_me3;
               }
               sprintf (aufps.auf_me1, "%.3lf", auf_me1);
        }
}
         

void update_kun_geb (void)
/**
Tabelle kun_geb aktualisieren.
**/
{
        short sqm;
        extern short sql_mode;

        sqm = sql_mode;
        strcpy (a_kun_geb.geb_eti, gebinde);
        a_kun_geb.geb_fill = atoi (geb_einh);
        ToVorgabe (veinh1, a_kun_geb.geb_fill);
        strcpy (vorgabe1, geb_me);
        a_kun_geb.geb_anz =  (long) (double) (ratod (geb_me) * 1000);

        strcpy (a_kun_geb.pal_eti, palette);
        a_kun_geb.pal_fill = atoi (pal_einh);
        ToVorgabe (veinh2, a_kun_geb.pal_fill);
        strcpy (vorgabe2, pal_me);
        a_kun_geb.pal_anz =  (long) (double) ratod (pal_me) * 1000;
        a_kun_geb.tara    =  ratod (gtara);
        sprintf (aufps.tara,     "%.3lf", a_kun_geb.tara); 
        sql_mode = 1;
        geb_class.update_kun_geb ();
        sql_mode = sqm;
        updategeblsp ();
}


/**

Neuen Lieferschein eingeben.
  
**/

mfont LiefKopfFontT = {"Courier New", 200, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont LiefKopfFontE = {"Courier New", 200, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


mfont LiefKopfFontTD = {"Courier New", 150, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont LiefKopfFontED = {"Courier New", 150, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


static char KundeFil0 [14] = {"Kunden-Nr   :"};
static char KundeFil1 [14] = {"Filial-Nr   :"};
static char kunfil[3];
static long gen_lief_nr;

static field _LiefKopfT[] = {
"Lieferschein-Nr:",     16, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0};

static form LiefKopfT = {1, 0, 0, _LiefKopfT, 0, 0, 0, 0, &LiefKopfFontT};


static field _LiefKopfTD[] = {
"Kunde/Fil   :",     13, 0, 4, 2, 0, "", DISPLAYONLY, 0, 0, 0,
KundeFil0,           13, 0, 5, 2, 0, "", DISPLAYONLY, 0, 0, 0,
kun_krz1,            16, 0, 5,28, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferdatum :",     13, 0, 6, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferzeit  :",     13, 0, 7, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Status      :",     13, 0,10, 2, 0, "", DISPLAYONLY, 0, 0, 0,
ls_stat_txt,         16, 0,10,28, 0, "", DISPLAYONLY, 0, 0, 0
};

static form LiefKopfTD = {7, 0, 0, _LiefKopfTD, 0, 0, 0, 0, &LiefKopfFontTD};

static int ls_stat_pos = 6;
static int SetKunFil (void);
static int ReadKun (void);
static int ReadLS (void);


static field _LiefKopfE[] = {
  lief_nr,                9, 0, 1,18, 0, "%8d", EDIT,        0, ReadLS, 0};

static form LiefKopfE = {1, 0, 0, _LiefKopfE, 0, 0, 0, 0, &LiefKopfFontE};


static field _LiefKopfED[] = { 
  kunfil,               2, 0, 4,18, 0, "%1", EDIT,      0, SetKunFil, 0, 
  kun_nr,               9, 0, 5,18, 0, "%8d", EDIT,     0, ReadKun, 0,
  lieferdatum,         11, 0, 6,18, 0, "dd.mm.yyyy",
                                              EDIT,     0, 0, 0,
  lieferzeit,           6, 0, 7,18, 0, "",
                                              EDIT,     0, 0, 0,
  ls_stat,              2, 0,10,18, 0, "",
                                              READONLY, 0, 0, 0
};

static form LiefKopfED = {5, 0, 0, _LiefKopfED, 0, 0, 0, 0, &LiefKopfFontED};


static int break_lief_enter = 0;
static int lspos = 0;
static int lsend = 5;

void FillLSForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
             kun_class.lese_kun (1, 0, lsk.kun);
             sprintf (kun_nr, "%ld", lsk.kun);
             dlong_to_asc (lsk.lieferdat, lieferdatum);
             strcpy (lieferzeit, lsk.lieferzeit);
             sprintf (ls_stat, "%hd", lsk.ls_stat);
             strcpy (kun_krz1, kun.kun_krz1);
             ptab_class.lese_ptab ("ls_stat", ls_stat);
             strcpy (ls_stat_txt, ptabn.ptbezk);
}

void FromLSForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
             short mdn;
             short fil;

             mdn = 1;
             fil = 0;

             lsk.mdn = mdn;
             lsk.fil = fil;
             lsk.ls  = atol (lief_nr);
             lsk.kun = atol (kun_nr);  
             lsk.lieferdat = dasc_to_long (lieferdatum);
             strcpy (lsk.lieferzeit, lieferzeit);
             lsk.ls_stat = atoi (ls_stat);
             if (lsk.ls_stat < 2)
             {
                 lsk.ls_stat = 2;
             }
}

static int readstatus = 0;
static int freestat = 0;

void FreeLS (void)
/**
Lieferschein-Nummer freigeben.
**/
{
     short mdn;
     short fil;

     mdn = 1;
     fil = 0;

     if (freestat == 1) return;

     beginwork ();
     dsqlstatus = nveinid (mdn, fil,
                          "ls",  gen_lief_nr);
     commitwork ();
     freestat = 1;
}


int WriteLS (void)
/**
Lieferscheinkopf schreiben.
**/
{
    char datum [12];
    long dat1, dat2;

    if (readstatus > 1) return 0;

    if (atol (kun_nr) == 0l) return 0;

    sysdate (datum); 
    dat1 = dasc_to_long (datum); 
    dat2 = dasc_to_long (lieferdatum); 
    if (dat1 > dat2)
    {
        disp_messG ("Lieferdatum nicht korrekt", 2);
        return (0);
    }

    FromLSForm ();
    if (Lock_Lsk () == FALSE) 
    {
        PostQuitMessage (0);
        break_lief_enter = 1;
        commitwork ();
        return 0;
    }
    ls_class.update_lsk (lsk.mdn, lsk.fil, lsk.ls);
    ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
    set_fkt (menfunc2, 6);
    if (readstatus == 1 || lsk.ls == gen_lief_nr)
    {
        freestat = 1;
    }
    commitwork ();
    return 0;
}


int ReadLS (void)
/**
Lieferschein nach Auftragsnummer lesen.
**/
{
            readstatus = 0; 
            if (atol (lief_nr) <= 0l) return (-1);

            freestat = 0;
            dsqlstatus = ls_class.lese_lsk (1, 0, atol (lief_nr));
            if (dsqlstatus == 100 && atol (lief_nr) != gen_lief_nr)
            {
                   ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
                   set_fkt (NULL, 6);
                   print_messG (0, "Lieferschein %s nicht gefunden", lief_nr);
                   readstatus = 2;
                   break_lief_enter = 1;
                   return -1;
            }
            else if (dsqlstatus < 0)
            {
                   ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
                   set_fkt (NULL, 6);
                   print_messG (0, "Datenbankfehler %d", dsqlstatus);
                   readstatus = 2;
                   return -1;
            }

            if (dsqlstatus == 0)
            {
                   if (lsk.ls != gen_lief_nr)
                   {
                           commitwork ();
                           FreeLS ();
                   }
                   beginwork ();
                   if (Lock_Lsk () == FALSE) 
                   {
                          break_lief_enter = 1;
//                          PostMessage (AufKopfWindow, WM_USER, 0, 0);
                          return -1;
                   }
                   ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
                   set_fkt (menfunc2, 6);
                   FillLSForm ();
            }
            else
            {
                   sprintf (ls_stat,"%hd", 2);
                   ptab_class.lese_ptab ("ls_stat", ls_stat);
                   readstatus = 1;
            }
            return 0;
}
         

int SetKunFil (void)
{
          if (atoi (kunfil) == 0)
          {
                      sprintf (kunfil, "0");
                      LiefKopfTD.mask[1].feld = KundeFil0;
          }
          else
          {
                      sprintf (kunfil, "1");
                      LiefKopfTD.mask[1].feld = KundeFil1;
          }

          display_form (AufKopfWindow, &LiefKopfTD, 0, 0);
          display_form (AufKopfWindow, &LiefKopfED, 0, 0);
          return (0);
}


void FillLskAdr (void)
/**
Adressfelder in Lsk fuellen.
**/
{
          lsk.adr = kun.adr2;
          strcpy (lsk.adr_nam1, _adr.adr_nam1);
          strcpy (lsk.adr_nam2,  _adr.adr_nam2);
          strcpy (lsk.str,       _adr.str);
          strcpy (lsk.plz,       _adr.plz);
          strcpy (lsk.ort1,      _adr.ort1);
          strcpy (lsk.pf,        _adr.pf);
}

int ReadKun (void)
{
          short mdn;
          short fil;

          mdn = 1;
          fil = 0;
          kun_krz1[0] = (char) 0;
          if (atoi (kunfil) == 0)
          {
                  dsqlstatus = kun_class.lese_kun (mdn, fil, atol (kun_nr)); 
                  if (dsqlstatus == 0)
                  {
                         strcpy (kun_krz1, kun.kun_krz1);
                         strcpy (lsk.kun_krz1,kun.kun_krz1);
                         adr_class.lese_adr (kun.adr2);
                         FillLskAdr ();
                  }
          }
          else
          {
                  dsqlstatus = fil_class.lese_fil (mdn, (short) atol (kun_nr)); 
                  if (dsqlstatus == 0)
                  {
                      adr_class.lese_adr (_fil.adr);
                      strcpy (kun_krz1, _adr.adr_krz);
                      strcpy (lsk.kun_krz1, _adr.adr_krz);
                      FillLskAdr ();
                  }
          }
          if (dsqlstatus)
          {
                  printf (kun_nr, "%ld", 0l);
          }
                  
          display_field (AufKopfWindow, &LiefKopfTD.mask[2], 0, 0);
          return (0);
}
                       

void DisplayLiefKopf (void)
/**
Masken fuer LiefKopf anzeigen.
**/
{
          display_form (AufKopfWindow, &LiefKopfT, 0, 0);
          display_form (AufKopfWindow, &LiefKopfTD, 0, 0);
          if (LiefKopfE.mask[0].feldid)
          {
              display_form (AufKopfWindow, &LiefKopfE, 0, 0);
              display_form (AufKopfWindow, &LiefKopfED, 0, 0);
          }
}

void CloseLiefKopf (void)
/**
Masken fuer LiefKopf anzeigen.
**/
{
        CloseControls (&LiefKopfT);
        CloseControls (&LiefKopfE);
        CloseControls (&LiefKopfTD);
        CloseControls (&LiefKopfED);
}

void GetEditTextL (void)
/**
Text aus Editierfeld holen.
**/
{
        switch (lspos)
        {
                 case 0 :
                    GetWindowText (LiefKopfE.mask [0].feldid,
                                   LiefKopfE.mask [0].feld,
                                   LiefKopfE.mask[0].length);
                    break;
                 case 1 :
                    GetWindowText (LiefKopfED.mask [0].feldid,
                                   LiefKopfED.mask [0].feld,
                                   LiefKopfED.mask[0].length);
                    break;
                 case 2 :
                    GetWindowText (LiefKopfED.mask [1].feldid,
                                   LiefKopfED.mask [1].feld,
                                   LiefKopfED.mask[1].length);
                    break;
                 case 3 :
                    GetWindowText (LiefKopfED.mask [2].feldid,
                                   LiefKopfED.mask [2].feld,
                                   LiefKopfED.mask[2].length);
                    break;
                 case 4 :
                    GetWindowText (LiefKopfED.mask [3].feldid,
                                   LiefKopfED.mask [3].feld,
                                   LiefKopfED.mask[3].length);

                    break;
        }
        if (lspos == 0 && LiefKopfE.mask[0].after)
        {
                    (*LiefKopfE.mask[0].after) ();
        }
        else if (lspos &&  LiefKopfED.mask[lspos - 1].after)
        {
                    (*LiefKopfED.mask[lspos - 1].after) ();
        }
}

void LiefAfter (int pos)
{
        lspos = pos;
        if (lspos == 0 && LiefKopfE.mask[0].after)
        {
                    (*LiefKopfE.mask[0].after) ();
        }
        else if (lspos &&  LiefKopfED.mask[lspos - 1].after)
        {
                    (*LiefKopfED.mask[lspos - 1].after) ();
        }
}

void SetLiefFocus (int pos)
/**
Focus auf Eingabefelder AuftragKopfsetzen.
**/
{
        switch (pos)
        {
               case 0 :
                      SetFocus (LiefKopfE.mask[0].feldid);
                      SendMessage (LiefKopfE.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               default :
                      SetFocus (LiefKopfED.mask[pos - 1].feldid);
                      SendMessage (LiefKopfED.mask[pos -1].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
        }
}

void LiefFocus (void)
{
        SetLiefFocus (lspos);
}


int IsLiefMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;
         
         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    break_lief_enter = 1;
                                    return TRUE;
                            case VK_F6 :
                                    testkeys ();
                                    return TRUE;
                            case VK_F12 :
                                    WriteLS ();
                                    return TRUE;
                            case VK_DOWN :
                            case VK_RETURN :
                                    GetEditTextL ();
                                    lspos ++;
                                    if (lspos > lsend - 1) lspos = 0;
                                    SetLiefFocus (lspos);
                                    return TRUE;
                            case VK_UP :
                                    GetEditTextL ();
                                    lspos --;
                                    if (lspos < 0) lspos = lsend - 1;
                                    SetLiefFocus (lspos);
                                    return TRUE;
                            case VK_TAB :
                                    GetEditTextL ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                            lspos --;
                                            if (lspos < 0) lspos = 4;
                                            SetLiefFocus (lspos);
                                     }
                                     else
                                     {
                                            lspos ++;
                                            if (lspos > 4) lspos = 0;
                                            SetLiefFocus (lspos);
                                     }
                                      return TRUE;
                            default :
                                    return FALSE;   
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (LiefKopfE.mask[0].feldid, &mousepos))
                    {
                          EnterNumBox (AufKopfWindow, "  Lieferschein-Nr  ",
                                       lief_nr, 9, LiefKopfE.mask[0].picture);
                          display_field (AufKopfWindow, &LiefKopfE.mask[0], 0, 0);
                          LiefAfter (0);
                          return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfED.mask[0].feldid, &mousepos))
                    {
                          EnterNumBox (AufKopfWindow,"  Kunden/Fil  ",
                                          kunfil, 2, LiefKopfED.mask[0].picture);
                          display_field (AufKopfWindow, &LiefKopfED.mask[0], 0, 0);
                          LiefAfter (1);
                          return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfED.mask[1].feldid, &mousepos))
                    {
                          EnterNumBox (AufKopfWindow,"  Kunden-Nr  ",
                                          kun_nr, 9, LiefKopfED.mask[0].picture);
                          display_field (AufKopfWindow, &LiefKopfE.mask[1], 0, 0);
                          LiefAfter (2);
                          return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfED.mask[2].feldid, &mousepos))
                    {
                         EnterNumBox (AufKopfWindow,"  Lieferdatum  ",
                                      lieferdatum, 11,
                                            LiefKopfED.mask[1].picture);
                         display_field (AufKopfWindow, &LiefKopfE.mask[2], 0, 0);
                         LiefAfter (3);
                         return TRUE;
                    }
                    else if (MouseinWindow (LiefKopfED.mask[3].feldid, &mousepos))
                    {
                         EnterNumBox (AufKopfWindow,"  Lieferzeit  ",
                                      lieferzeit, 6,
                                            LiefKopfED.mask[2].picture);
                         display_field (AufKopfWindow, &LiefKopfE.mask[3], 0, 0);
                         LiefAfter (4);
                         return TRUE;
                    }
             }
          }

          return FALSE;
}

void KonvLiefKopf (void)
/**
Spaltenposition von LiefKopfED anpassen.
**/
{
        static int KonvOK = 0;
        int z, s;
        int i;

        if (KonvOK) return;

        KonvOK = 1;
        for (i = 0; i < LiefKopfED.fieldanz; i ++)
        {
                    s = LiefKopfED.mask[i].pos [1];
                    KonvTextPos (LiefKopfE.font,
                                 LiefKopfED.font,
                                 &z, &s);
                    LiefKopfED.mask[i].pos [1] = s;
        }

        s = LiefKopfTD.mask[1].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[1].pos [1] = s;

        s = LiefKopfTD.mask[2].pos [1];
        KonvTextPos (LiefKopfE.font,
                    LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[2].pos [1] = s;

        s = LiefKopfTD.mask[6].pos [1];
        KonvTextPos (LiefKopfE.font,
                     LiefKopfED.font,
                    &z, &s);
        LiefKopfTD.mask[6].pos [1] = s;
}

void LiefInit (field *feld)
/**
Feld Initialisieren.
**/
{
        memset (feld->feld, ' ', feld->length); 
        feld->feld[feld->length - 1] = (char) 0;
}

void InitLiefKopf (void)
/**
Auftrag-Kopf initialisieren.
**/
{
        int i;

        LiefKopfE.mask[0].feld[0] = (char) 0;

        for (i = 0; i < LiefKopfED.fieldanz; i ++)
        {
                      LiefInit (&LiefKopfED.mask[i]);
        }
        LiefInit (&LiefKopfTD.mask[2]);
        LiefInit (&LiefKopfTD.mask[ls_stat_pos]);
}

void GenLS (void)
/**
Lieferscheinnummer generieren.
**/
{
        int dsqlstatus;
        short mdn;
        short fil;

        mdn = 1;
        fil = 0;

        beginwork ();
        dsqlstatus = nvholid (mdn, fil, "ls");
        if (dsqlstatus == 100)
        {
              dsqlstatus = nvanmprf (mdn,
                                     fil,
                                     "ls",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");

              if (dsqlstatus == 0)
              {
                          dsqlstatus = nvholid (mdn,
                                                fil,
                                                "ls");
              }
         }
         commitwork ();
         sprintf (lief_nr, "%ld", auto_nr.nr_nr);
         gen_lief_nr = auto_nr.nr_nr;
         sysdate (lieferdatum);
}
         


void LiefKopf (void)
/**
Auftragskopf bearbeiten.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;

        if (KopfEnterAktiv) return;
        if (LiefEnterAktiv) return;

        MainBuInactiv ();
        freestat = 0;
        CloseControls (&AufKopfT);
        CloseControls (&AufKopfE);
        CloseControls (&AufKopfTD);
        CloseControls (&AufKopfED);

        lspos = 0;
        RegisterKopfEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = rect.top;
        cx = rect.right - rect.left; 
        cy = rect.bottom - rect.top;

        KonvLiefKopf ();
        InitLiefKopf ();
        GenLS ();

        beginwork ();
        LiefEnterAktiv = 1;
        if (AufKopfWindow == 0)
        {   
                  AufKopfWindow  = CreateWindowEx (
                              0,
                              "AufKopf",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
        }

        if (AufKopfWindow == NULL) 
        {
                 LiefEnterAktiv = 0;
                 return;
        }
        ShowWindow (AufKopfWindow, SW_SHOW);
        UpdateWindow (AufKopfWindow);
        create_enter_form (AufKopfWindow, &LiefKopfE, 0, 0);
        create_enter_form (AufKopfWindow, &LiefKopfED, 0, 0);
        SetLiefFocus (lspos);
        break_lief_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsLiefMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_lief_enter) break;
        }
        CloseControls (&LiefKopfT);
        CloseControls (&LiefKopfE);
        CloseControls (&LiefKopfTD);
        CloseControls (&LiefKopfED);

        DestroyWindow (AufKopfWindow);
        AufKopfWindow = NULL;
        LiefEnterAktiv = 0;
        MainBuActiv ();
        commitwork ();
        FreeLS ();
}


/* Auswahl ueber Geraete (sys_peri    */

#define MCI 6
#define GX  16

struct SYSPS 
{
        char sys [9];
        char peri_typ [3];
        char txt [61];
        char peri_nam [21];
};


struct SYSPS sysarr [50], sysps;


static int sysidx;
static int sysanz = 0;

mfont SysFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _sysform [] =
{
        sysps.sys,      12, 1, 0, 1, 0, "%8d", DISPLAYONLY, 0, 0, 0,
        sysps.peri_typ,  4, 1, 0,13, 0, "",    DISPLAYONLY, 0, 0, 0,
        sysps.txt,      20, 1, 0,17, 0, "",    DISPLAYONLY, 0, 0, 0
};

static form sysform = {3, 0, 0, _sysform, 0, 0, 0, 0, &SysFont};

static field _sysub [] =
{
        "Ger�te-Nr",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Typ",            4, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0,
        "Ger�t",         20, 1, 0,17, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form sysub = {3, 0, 0, _sysub, 0, 0, 0, 0, &SysFont};

static field _sysvl [] =
{      
    "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 16, 0, "", NORMAL, 0, 0, 0,
};

static form sysvl = {2, 0, 0, _sysvl, 0, 0, 0, 0, &SysFont};

int ShowSysPeri (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15; 
        cy = 185;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x, 
//                                    y + cy + 23,
                                    y,
                                    cx + 24, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}

int AuswahlSysPeri (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        if (sysanz == 0)
        {
               dsqlstatus = sys_peri_class.lese_sys_peri ("order by sys");
               while (dsqlstatus == 0)
               {
                   if (sys_peri.peri_typ == MCI || 
                       sys_peri.peri_typ == GX)
                   {
                       sprintf (sysarr[sysanz].sys, "%ld", 
                                     sys_peri.sys);
                       sprintf (sysarr[sysanz].peri_typ,"%hd",
                                     sys_peri.peri_typ);
                       strcpy (sysarr[sysanz].txt, sys_peri.txt);
                       strcpy (sysarr[sysanz].peri_nam, 
                                      sys_peri.peri_nam);
                       sysanz ++;
                       if (sysanz == 50) break;
                   }
                   dsqlstatus = sys_peri_class.lese_sys_peri ();
               }
        }
        if (sysanz == 0)
        {
               disp_messG ("Keine Ger�te gefunden", 1);
               return 0;
        }
        syskey = 0;
        sysidx = ShowSysPeri (LogoWindow, (char *) sysarr, sysanz,
                     (char *) &sysps,(int) sizeof (struct SYSPS),
                     &sysform, &sysvl, &sysub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        if (atoi (sysarr[sysidx].peri_typ) == 6)
        {
                strcpy (waadir, sysarr[sysidx].peri_nam);
                clipped (waadir); 
                sprintf (ergebnis, "%s\\ergebnis", waadir);
                WaaInit = 0;
                WaageInit ();
                sys_peri_class.lese_sys (atol (sysarr[sysidx].sys));
                la_zahl = sys_peri.la_zahl;
                AktKopf = 0;
        }
        else 
        {
              auszeichner = atol (sysarr[sysidx].sys);
//            F�r Preisauszeichner noch nicht aktiv.
        }

        return sysidx;
}


/* Auswahl ueber Wiegungen                    */


struct WIEGT
{
        char wieg [5];
        char menge [12];
};


struct WIEGT wiegarr [1000], wiegt;


static int wiegidx;
static int wieganz = 0;

mfont WiegFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wiegform [] =
{
        wiegt.wieg,      9, 1, 0, 1, 0, "%7d",   DISPLAYONLY, 0, 0, 0,
        wiegt.menge,    13, 1, 0,10, 0, ":9.3f", DISPLAYONLY, 0, 0, 0,
};

static form wiegform = {2, 0, 0, _wiegform, 0, 0, 0, 0, &WiegFont};

static field _wiegub [] =
{
        "Wiegung",        9, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Menge",         13, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wiegub = {2, 0, 0, _wiegub, 0, 0, 0, 0, &WiegFont};

static field _wiegvl [] =
{      
    "1",                  1,  0, 0,  9, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 22, 0, "", NORMAL, 0, 0, 0,
};

static form wiegvl = {2, 0, 0, _wiegvl, 0, 0, 0, 0, &WiegFont};

int ShowWieg (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15; 
        cy = 185;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx + 24, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}

int AuswahlWieg (void)
/**
Ausahlfenster ueber Wiegungen anzeigen
**/
{
        int i;
        int j;
       
        for (i = AktWiegPos - 1, j = 0; i >= 0; i --, j ++)
        {
            sprintf (wiegarr[j].wieg, "%d", i + 1);
            sprintf (wiegarr[j].menge, "%.3lf", AktPrWieg[i]);
        }
        wieganz = j;
                 
        if (wieganz == 0)
        {
               return 0;
        }
        syskey = 0;
        wiegidx = ShowWieg (WiegWindow, (char *) wiegarr, wieganz,
                     (char *) &wiegt,(int) sizeof (struct WIEGT),
                     &wiegform, &wiegvl, &wiegub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        StornoWieg = ratod (wiegarr[wiegidx].menge);
        return TRUE;
}


/* Auswahl ueber Wiegekoepfe                    */


struct WKOPF
{
        char nr [10];
        char akt [10];
};


struct WKOPF wkopfarr [20], wkopf;


static int kopfidx;
static int kopfanz = 0;

mfont KopfFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wkopfform [] =
{
        wkopf.nr,     11, 1, 0, 1, 0, "%4d",   DISPLAYONLY, 0, 0, 0,
        wkopf.akt,    13, 1, 0,12, 0, "",      DISPLAYONLY, 0, 0, 0,
};

static form wkopfform = {2, 0, 0, _wkopfform, 0, 0, 0, 0, &KopfFont};

static field _wkopfub [] =
{
        "Wiegekopf",       11, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "  aktiv",         13, 1, 0,12, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wkopfub = {2, 0, 0, _wkopfub, 0, 0, 0, 0, &KopfFont};

static field _wkopfvl [] =
{      
    "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 24, 0, "", NORMAL, 0, 0, 0,
};

static form wkopfvl = {2, 0, 0, _wkopfvl, 0, 0, 0, 0, &KopfFont};

int ShowWKopf (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15; 
        cy = 185;

        set_fkt (endlist, 5);
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
                                    y,
                                    cx + 24, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
//        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
//        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
        return ptidx;
}


int AuswahlKopf (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        int i;

        for (i = 0; i < la_zahl; i ++)
        {
                       sprintf (wkopfarr[i].nr, "%d", i + 1);  
                       if (i == AktKopf)
                       {
                               sprintf (wkopfarr[i].akt, "   * ");  
                       }
                       else
                       {
                               sprintf (wkopfarr[i].akt, "     ");  
                       }
        }
        kopfanz = i;
        if (kopfanz == 0)
        {
               return 0;
        }
        syskey = 0;
        kopfidx = ShowWKopf (LogoWindow, (char *) wkopfarr, kopfanz,
                     (char *) &wkopf,(int) sizeof (struct WKOPF),
                     &wkopfform, &wkopfvl, &wkopfub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        return kopfidx;
}


/* Auswahl bei leeren Positionen   */

mfont KtFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static char ktext [61];
static char ktextarr [10][61];

static int ktidx;

static field _ktform [] = 
{
        ktext,     50, 1, 0, 1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form ktform = {1, 0, 0, _ktform, 0, 0, 0, 0, &KtFont};

static field _ktub [] = 
{
        "Auswahl",     50, 1, 0, 1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form ktub = {1, 0, 0, _ktub, 0, 0, 0, 0, &KtFont};


BOOL LeerPosAuswahl ()
{
        static BOOL CpOK = FALSE;

        if (! CpOK)
        {
            strcpy (ktextarr[0], 
                "Unbearbeitete Positionen ignorieren");
            strcpy (ktextarr[1], 
                "Unbearbeitete Positionen l�schen");
            strcpy (ktextarr[2], 
                "Ist-Menge = Soll-Menge");
        }
        SetHoLines (0);
        syskey = 0;
        if (eWindow)
        {
                EnableWindow (eWindow, FALSE);
                EnableWindow (eFussWindow, FALSE);
                EnableWindow (eKopfWindow, FALSE);
        }
        ktidx = ShowWKopf (eWindow, (char *) ktextarr, 3,
                     (char *) &ktext, 61,
                     &ktform, NULL, &ktub);
        if (eWindow)
        {
                EnableWindow (eWindow, TRUE);
                EnableWindow (eFussWindow, TRUE);
                EnableWindow (eKopfWindow, TRUE);
        }

        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return FALSE;
        }
        switch (ktidx)
        {
                case 0 :
                    SetAllPosStat ();
                    return TRUE;
                case 1 :
                    DelLeerPos ();
                    return TRUE;
                case 2:
                    SollToIst ();
                    return TRUE;
        }
        return TRUE;
}



BOOL PosLeer (void)
/**
Reaktion auf nicht abgeschlossene Positionen.
**/
{
          EnableWindow (hMainWindow, FALSE);
          EnableWindow (eWindow, FALSE);
          EnableWindow (eFussWindow, FALSE);
          EnableWindow (eKopfWindow, FALSE);
          if (abfragejnG (eWindow,
                          "Es sind nicht bearbeitete Positionen vorhanden.\n"
                          "Abbrechen ?", "J"))
          {
              EnableWindow (hMainWindow, TRUE);
              EnableWindow (eWindow, TRUE);
              EnableWindow (eFussWindow, TRUE);
              EnableWindow (eKopfWindow, TRUE);
              return FALSE;
          }
          EnableWindow (hMainWindow, TRUE);
          EnableWindow (eWindow, TRUE);
          EnableWindow (eFussWindow, TRUE);
          EnableWindow (eKopfWindow, TRUE);
          return LeerPosAuswahl ();
}


BOOL TestPosis (void)
/**
Einzelne Positionen testen.
**/
{
          int i;

          for (i = 0; i < aufpanz_upd; i ++)
          {
              if (atoi (aufparr[i].s) < 3)
              {
                  return PosLeer ();
              }
          }
          return TRUE;
}


BOOL TestPosStatus (void)
/**
Test, ob der Lieferschein auf komplett gesetzt werden darf.
**/
{
          if (NewPosi () == FALSE) 
          {
              disp_messG ("Der Lieferschein wird noch von einem\n"
                          "anderen Benutzer bearbeitet", 2);
              return FALSE;
          }

          if (TestPosis () == FALSE)
          {
              return FALSE;
          }
          return TRUE;
}

int LeerGutExist (void)
/**
Test, ob Leergutartikel erfasst wurden.
**/
{
          int i;
          int lganz;

          lganz = 0;
          for (i = 0; i < aufpanz_upd; i ++)
          {
              if (lese_a_bas (ratod (aufparr[i].a)) != 0)
              {
                  continue;
              }
              if (_a_bas.a_typ == 11) 
              {
                  lganz += atoi (aufparr[i].lief_me);
              }
          }
          return lganz;
}

BOOL PrintAufkleber (int lganz)
/**
Druck fuer Aufkleber starten.
**/
{
          char buffer [256];
          DWORD ret;

          sprintf (buffer, "%d", lganz);

          EnterNumBox (eWindow," Anzahl Aufkleber", buffer, 9,
                                   "%4d");

/*
          EnterNumBox (eWindow,
              " Wieviele Aufkleber sollen ausgedruckt werden", buffer, 9,
                                   "%4d");
*/
          lganz = atoi (buffer); 
          if (lganz == 0) return TRUE;

          sprintf (buffer, "rswrun 53800 0 %s %hd %hd %ld %d",
                                 clipped (sys_ben.pers_nam),
                                 lsk.mdn, lsk.fil, lsk.kun, lganz);
          SetAktivWindow (eWindow);

//          disp_messG (buffer, 1);

/*
          sprintf (buffer, "rswrun 53800 0 %s %hd",
                                 clipped (sys_ben.pers_nam));
*/
          ret = ProcExec (buffer, SW_SHOWMINIMIZED, -1, 0, -1, 0);
          SetAktivWindow (eWindow);
          if (ret == 0)
          {
              disp_messG ("Fehler beim Start des Etikettendrucks",2);
          }
          else
          {
              disp_messG ("Etikettendruck gestartet", 2); 
          }
          return TRUE;
}


BOOL LsLeerGut (void)
/**
Leergut testen.
**/
{
         int lganz;

         if (ls_leer_zwang == 0) return TRUE;
         if (lganz = LeerGutExist ())
         {
             return PrintAufkleber (lganz);
         }
         if (abfragejnG (eWindow, "Es wurde kein Leergutartikel erfasst\n"
                                  "Abbrechen ?", "J"))
         {
// Durch PostMessage wuerde das Fenster zum Erfassen von Artikeln
// geoeffnet.
//             PostMessage (eWindow, WM_KEYDOWN, VK_F6, 0l);
             return FALSE;
         }
         return TRUE;
}

              
