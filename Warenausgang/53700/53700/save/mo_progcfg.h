#ifndef _PROG_CFG
#define _PROG_CGF


class PROG_CFG 
         {
          private :
                char progname [80];
                FILE *progfp;
          public :
              PROG_CFG (char *progname) : progfp (NULL)
              {
                       strcpy (this->progname, progname);
              }
                        
              BOOL OpenCfg (void);
              void CloseCfg (void);
              BOOL GetCfgValue (char *, char *);
         };
#endif         
                          
                               

