// LockDayArticle.cpp: Implementierung der Klasse LockDayArticle.
//
//////////////////////////////////////////////////////////////////////

#include "LockDayArticle.h"
#include "strfkt.h"
#include "mo_curs0.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////


static char *CreateString = "create table lockprot "
							"("
							"pers char (12),"
							"computer char (30),"
          					"tag date,"
                            "zeit char (8),"
                            "prog char (20),"
							"text char (40)"
                            ") in fit_dat extent size 32 next size 256 lock mode row"; 

LockDayArticle::LockDayArticle()
{
	m_Mdn = 0;
	m_Fil = 0;
	m_Article = 0.0;
	m_KommDate = 0l;
	m_TestCursor = -1;
	m_LockCursor = -1;
	m_UnlockCursor = -1;
}

LockDayArticle::~LockDayArticle()
{

}

void LockDayArticle::SetKeys (short mdn, short fil, double article, long kommDate)
{
	m_Mdn = mdn;
	m_Fil = fil;
	m_Article = article;
	m_KommDate = kommDate;
}

BOOL LockDayArticle::IsLocked ()
{
	BOOL ret = FALSE;
	int dsqlstatus;
	if (m_TestCursor == -1)
	{
		DbClass.sqlin ((short *)  &m_Mdn,      SQLSHORT, 0);
		DbClass.sqlin ((short *)  &m_Fil,      SQLSHORT, 0);
		DbClass.sqlin ((long *)   &m_KommDate, SQLLONG, 0);
		DbClass.sqlin ((double *) &m_Article,  SQLDOUBLE, 0);
		m_TestCursor = DbClass.sqlcursor ("select lsp.delstatus from lsk, lsp "
			                              "where lsk.mdn = ? "
			                              "and lsk.fil = ? "
			                              "and lsk.komm_dat = ? "
			                              "and lsp.mdn = lsk.mdn "
			                              "and lsp.fil = lsk.fil "
			                              "and lsp.ls = lsk.ls "
			                              "and lsp.a = ? "
			                              "and lsp.delstatus != 0");
	}
    dsqlstatus = DbClass.sqlopen (m_TestCursor);
	if (dsqlstatus == 0)
	{
		dsqlstatus = DbClass.sqlfetch (m_TestCursor);
		if (dsqlstatus == 0)
		{
			ret = TRUE;
		}
	}
	return ret;
}

BOOL LockDayArticle::Lock ()
{
	BOOL ret = TRUE;
	extern short sql_mode;
	short sql_s;
	int dsqlstatus;
	sql_s = sql_mode;
	sql_mode = 1;
	if (m_LockCursor == -1)
	{
		DbClass.sqlin ((short *)  &m_Mdn,      SQLSHORT, 0);
		DbClass.sqlin ((short *)  &m_Fil,      SQLSHORT, 0);
		DbClass.sqlin ((short *)  &m_Mdn,      SQLSHORT, 0);
		DbClass.sqlin ((short *)  &m_Fil,      SQLSHORT, 0);
		DbClass.sqlin ((long *)   &m_KommDate, SQLLONG, 0);
		DbClass.sqlin ((double *) &m_Article,  SQLDOUBLE, 0);
		m_LockCursor = DbClass.sqlcursor ("update lsp set delstatus = -1 "
			                              "where lsp.mdn = ? "
										  "and lsp.fil = ? "
			                              "and lsp.ls in (select ls from lsk "
											"where lsk.mdn = ? "
											"and lsk.fil = ? "
											"and lsk.komm_dat = ?) "
			                              "and lsp.a = ? ");
	}
	dsqlstatus = DbClass.sqlexecute (m_LockCursor);
	if (dsqlstatus != 0)
	{
		ret = FALSE;
	}
	sql_mode = sql_s;
	return ret;
}


BOOL LockDayArticle::Unlock ()
{
	BOOL ret = TRUE;
	extern short sql_mode;
	short sql_s;
	int dsqlstatus;
	sql_s = sql_mode;
	sql_mode = 1;
	if (m_UnlockCursor == -1)
	{
		DbClass.sqlin ((short *)  &m_Mdn,      SQLSHORT, 0);
		DbClass.sqlin ((short *)  &m_Fil,      SQLSHORT, 0);
		DbClass.sqlin ((short *)  &m_Mdn,      SQLSHORT, 0);
		DbClass.sqlin ((short *)  &m_Fil,      SQLSHORT, 0);
		DbClass.sqlin ((long *)   &m_KommDate, SQLLONG, 0);
		DbClass.sqlin ((double *) &m_Article,  SQLDOUBLE, 0);
		m_UnlockCursor = DbClass.sqlcursor ("update lsp set delstatus = 0 "
			                              "where lsp.mdn = ? "
										  "and lsp.fil = ? "
			                              "and lsp.ls in (select ls from lsk "
											"where lsk.mdn = ? "
											"and lsk.fil = ? "
											"and lsk.komm_dat = ?) "
			                              "and lsp.a = ? "
										  "and delstatus != 0");
	}
	dsqlstatus = DbClass.sqlexecute (m_UnlockCursor);
	if (dsqlstatus != 0)
	{
		ret = FALSE;
	}
	sql_mode = sql_s;
	return ret;
}

BOOL LockDayArticle::UnlockManual (char *pers, char *prog)
{
	BOOL ret = TRUE;
	extern short sql_mode;
	short sqls;
	int dsqlstatus;
	char tag [12];
	char zeit [12];
	char computer [21];
	unsigned long length;

	beginwork ();
	ret = Unlock ();
	sqls = sql_mode;
	sql_mode = 1;
	dsqlstatus = DbClass.sqlcomm ("select * from lockprot");
	if (dsqlstatus < 0)
	{
		CreateLog ();
	}

	memset (lockprot.pers, 0, sizeof (lockprot.pers));
	memset (lockprot.prog, 0, sizeof (lockprot.prog));
	memset (computer, 0, sizeof (computer));
	strncpy (lockprot.pers, pers, sizeof (lockprot.pers) - 1);
	strncpy (lockprot.prog, prog, sizeof (lockprot.prog) - 1);
	strcpy (lockprot.text, "entsperrt");

	sysdate (tag);
	systime (zeit);
	lockprot.tag = dasc_to_long (tag);
	strcpy (lockprot.zeit, zeit);


	length = sizeof (computer) - 1;
	if (GetComputerName (computer, &length))
	{
		strncpy (lockprot.computer, computer, sizeof (lockprot.computer) - 1);
	}
	Lockprot.dbupdate ();
	commitwork ();

    sql_mode = sqls;
	return ret;
}

void LockDayArticle::CreateLog ()
{
	DbClass.sqlcomm (CreateString);
}
