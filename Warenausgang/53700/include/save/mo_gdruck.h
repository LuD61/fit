#ifndef _mo_gdruck_def
#define mo_gdruck_def
void SetPrnBreak (BOOL);
HDC GetPrinterDC (void);
int Gdruck (char *);
BOOL SetPage (void);
int GetPageRows (char *);
int GetPageStdCols (char *);
int GetRowWidthStz (int);
BOOL SelectPrinter (void);
char *GetNextPrinter (void);
void GetAllPrinters (void);
HDC GetPrinterbyName (char *);
char *GetPrinter (char *);
void SetPrinter (char *);
int IsGdiPrint (void);
void SetPersName (char *);
void ShowGdiPrinters (void);
void SetGdiPrinter (char *);
void SetpDevMode (DEVMODE *);
void SetQuerDefault (BOOL);
void SetDrkDeviceFile (char *);
void SetRowPrintFont (int);
void SetDevFile (char *);
void SetSelectOnly (BOOL);
BOOL GetSelectOnly (void);
#endif	   
       

