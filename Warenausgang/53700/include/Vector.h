#ifndef _VECTOR_DEF
#define _VECTOR_DEF
#include <windows.h>

class CVector
{
    private :

       void **Arr;
       int anz;
       int pos;
    public :
       int GetAnz ()
       {
           return anz;
       }

       int GetCount ()
       {
           return anz;
       }

       CVector ();
       CVector (void **Object);
       ~CVector ();
       void Init (void);
       void SetObject (void **Object);
       void Add (void *);
       void SetPosition (int);
       void FirstPosition (void);
       void *Get (int);
       void *GetNext (int);
       void *GetNext (void);
       void DestroyAll ();
       BOOL Drop (int);
       int Get (void *);
       void Drop (void *);
};
#endif
