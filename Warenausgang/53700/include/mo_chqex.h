#ifndef _MO_CHQEX_DEF
#define _MO_CHQEX_DEF
#include "cmask.h"

#define MAXIMAGE 50

class CHQEX
{
          private :
			  HWND hMainWindow;
			  HINSTANCE hInstance;
              mfont *Font;
			  DWORD currentfield;

              CFIELD **_SQuery;
              CFORM *SQuery;

              CFIELD **_SSearch;
              CFORM *SSearch;

              CFIELD **_SfListe;
              CFORM *SfListe;

              CFIELD **_SfButton;
              CFORM *SfButton;

              CFIELD **_SfWork;
              CFORM *SfWork;

			  static  HWND hWnd;
              static int listpos;
			  static int (*OkFunc) (int); 
			  static int (*OkFuncE) (char *); 
			  static int (*DialFunc) (int); 
              static int (*FillDb) (char *); 
              static int (*SearchLst) (char *); 
			  static char EBuff [256];
			  static char SBuff [256];
  		      static BOOL registered;
              int cx, cy;
              int cxorg, cyorg;
  			  static BOOL WithTabStops;
			  static HWND *TabStops;
			  static int TabStoplen;
			  static int TabStopPos;
              int    SortRow;
              DWORD  WindowStyle;
			  IMG *Images [MAXIMAGE];
			  int imgidx;
              char CheckBoxValue[5];
              BOOL SbuffSet;
              char *WinCaption;
			  BOOL DeleteWinCaption;
              static CHQEX *ActiveList;

         public :
              static BOOL FillOK;
              static char *Caption;
			  CHQEX (int, int, char *, char *);
			  CHQEX (int, int);
			  CHQEX (int, int, char *);
		      CHQEX (int, int, BOOL);
			  ~CHQEX ();
              void SetSBuff (char *b)
              {
                  if (b != NULL)
                  {
                        strcpy (SBuff, b);
                        SbuffSet = TRUE;
                  }
                  else
                  {
                      strcpy (SBuff, "");
                      SbuffSet = FALSE; 
                  }
              }

              char *GetSBuff ()
              {
                      return SBuff;
              }

              void SetSBuffSet (BOOL b)
              {
                      SbuffSet = b;
              }


              BOOL GetCheckBoxValue (void)
              {
                  if (CheckBoxValue[0] == 'J')
                  {
                      return TRUE;
                  }
                  return FALSE;
              }

              void SetWindowStyle (DWORD);
              void SetWindowCaption (char *);
              void SavefWork (void);
              void RestorefWork (void);
              BOOL IsFocusGet (int, int);
 		      void ProcessMessages (void);
              static void ShowDlg (HDC, form *);
              static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
			  static void SetOkFunc (int (*) (int));
			  static void SetOkFuncE (int (*) (char *));
			  static void SetDialFunc (int (*) (int));
			  static void SetFillDb (int (*) (char *));
			  static void SetSearchLst (int (*) (char *));
              static int  GetSel (void);
              static int  GetCount (void);
              static void  SetSel (int);
              static void MoveWindow (void);
              static void SetFontSize (int);
              static void ResetFontSize (void);
              void OpenWindow (HANDLE, HWND);
              void OpenWindow (HANDLE, HWND, BOOL);
              void GetDockParams (RECT *);
              void DestroyWindow (void);
			  void SetCurrentID (DWORD);
			  void SetCurrentName (char *);
			  void EnableID (DWORD, BOOL);
			  void EnableName (char *, BOOL);
			  void CheckID (DWORD, BOOL);
			  void CheckName (char *, BOOL);
              void VLines (char *, int);
              void RowAttr (char *);
              void RowPos (char *);
			  void InsertCaption (char *);
			  void InsertRecord  (char *);
			  void UpdateRecord  (char *, int);
              void GetText (char *);
              HWND GetSearchHwnd (void);
              HWND GethWnd (void);
              BOOL TestButtons (HWND);
              BOOL TestEdit (HWND);
              static void SethWndTabStop (HWND);
              void UpdateTabstop (HWND);
	  		  int  GetTabStoplen (CFORM *, int);
			  void AddTabStops (CFORM *);
			  void SetTabStops (CFORM *);
			  void DestroyTabStops (void);
			  void NewTabStops (CFORM *);
              void SetTabFocus (void);
              void EnableSort (BOOL b);
              void SearchList (void);
              void SearchList (char *);
              void SetSortRow (int, BOOL);
			  void AddImage (HBITMAP, HBITMAP);
};
#endif

