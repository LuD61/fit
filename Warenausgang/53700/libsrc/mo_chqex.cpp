#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "wmaskc.h"
#include "colbut.h"
#include "dbclass.h"
#include "cmask.h"
//#include "searchmdn.h"
#include "mo_chqex.h"
#include "lbox.h"
#include "dlg.h"

#define BuOk       1001
#define BuCancel   1002
#define LIBOX      1003
#define BuDial     1004
#define ID_LABEL   1005
#define ID_EDIT    1006 
#define ID_LABEL1  1007
#define ID_SEARCH  1008 
#define BuCheck    1009

BOOL *pDeleteWinCaption = FALSE;

static int StdSize = 100;

static mfont mFont = {
                     "MS Sans Serif", 
                      100, 
                      0, 
                      0, 
                      BLACKCOL, 
                      LTGRAYCOL,
                      0, 0};



static CFIELD **_Query;
static CFORM *Query;

static CFIELD **_Search;
static CFORM *Search;

static CFIELD **_fListe;
static CFORM *fListe;

static CFIELD **_fButton;
static CFORM *fButton;

static CFIELD **_fWork;
static CFORM *fWork;


HWND CHQEX::hWnd;
int CHQEX::listpos = 2;
char CHQEX::EBuff [256];
char CHQEX::SBuff [256];
int (*CHQEX::OkFunc) (int) = NULL;
int (*CHQEX::OkFuncE) (char *) = NULL;
int (*CHQEX::DialFunc) (int)  = NULL;
int (*CHQEX::FillDb) (char *) = NULL;
int (*CHQEX::SearchLst) (char *) = NULL;
BOOL CHQEX::registered = FALSE;
CHQEX *CHQEX::ActiveList = NULL;

BOOL CHQEX::WithTabStops = FALSE;
HWND *CHQEX::TabStops = NULL;
int CHQEX::TabStoplen = 0;
int CHQEX::TabStopPos = 0;
BOOL CHQEX::FillOK = FALSE;
char *CHQEX::Caption = NULL;

void CHQEX::SetFontSize (int size)
{
	     StdSize = size;
		 mFont.FontHeight = size;
}

void CHQEX::ResetFontSize (void)
{
	     StdSize = 100;
		 mFont.FontHeight = 100;
}

void CHQEX::SavefWork (void)
{
         _SQuery   = _Query;
         SQuery    = Query;

         _SSearch  = _Search;
         SSearch   = Search;

         _SfListe  = _fListe;
         SfListe   = fListe;

         _SfButton = _fButton;
         SfButton  = fButton;

         _SfWork   = _fWork;
         SfWork    = fWork;
}

void CHQEX::RestorefWork (void)
{
         _Query   = _SQuery;
         Query    = SQuery;

         _Search  = _SSearch;
         Search   = SSearch;

         _fListe  = _SfListe;
         fListe   = SfListe;

         _fButton = _SfButton;
         fButton  = SfButton;

         _fWork   = _SfWork;
         fWork    = SfWork;
}

CHQEX::CHQEX (int cx, int cy, char *Label, char *Text)
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;
            int xfull, yfull;
//			int cyplus;

            ActiveList = this;
            WinCaption = NULL;
            DeleteWinCaption = FALSE;
            Query = Search = NULL;
            SortRow = -1;
            WindowStyle = WS_POPUP | WS_THICKFRAME;
            WithTabStops = TRUE;
            FillOK = FALSE;
            mFont.FontHeight = StdSize;
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
             if (xfull < 1000) 
             {
                 mFont.FontHeight -= 30;
             }
 	        this->Font = &mFont;  

			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;
            cxorg = this->cx;
            cyorg = this->cy;
            x = strlen (Label) + 3; 
			if (x < 9) x = 9;
			strcpy (EBuff, Text);

			_Query     = new CFIELD * [2];
			_Query[0]  = new CFIELD ("Label", Label,  
				                      0, 0, 1, 1, NULL, "", CDISPLAYONLY,
												 ID_LABEL, Font, 0, 0);
			_Query[1]  = new CFIELD ("Query", EBuff,  
				                     37, 0, x, 1, NULL, "", CEDIT,
												 ID_EDIT, Font, 0, 0);
			 Query = new CFORM (2, _Query);

			_Search     = new CFIELD * [2];
			_Search[0]  = new CFIELD ("Label1", "Suchen",  
				                      0, 0, 1, 2, NULL, "", CDISPLAYONLY,
												 ID_LABEL1, Font, 0, 0);
			_Search[1]  = new CFIELD ("Search", SBuff,  
				                     37, 0, x, 2, NULL, "", CEDIT,
												 ID_SEARCH, Font, 0, 0);
			 Search = new CFORM (2, _Search);

            _fListe    = new CFIELD * [1];
			_fListe[0] = new CFIELD ("Liste", "Liste",
				                     cx - 2, cy - 8, 1, 4, NULL, "", CLISTBOX,
												 LIBOX, Font, 0, 0);

			fListe     = new CFORM (1, _fListe);
			fListe->Setchsize (2, 2);

			x = 0;
			y = (cy - 2) * tm.tmHeight;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);
			
			_fButton    = new CFIELD * [2];
			_fButton[0] = new CFIELD ("OK", "OK", cx, cy, x * size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
			_fButton[1] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x + 12 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (2, _fButton);
			fButton->Setchpos (0, 2);

            _fWork    = new CFIELD * [4];
			_fWork[0] = new CFIELD ("fQuery", Query, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 982, Font, 0, 0);
			_fWork[1] = new CFIELD ("fSearch", Search, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 983, Font, 0, 0);
			_fWork[2] = new CFIELD ("fListe", fListe, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 980, Font, 0, 0);
			_fWork[3] = new CFIELD ("Button", fButton, 0, 0, -1, 0, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (4, _fWork);
            SbuffSet = FALSE;
}


CHQEX::CHQEX (int cx, int cy)
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;
            int xfull, yfull;
//			int cyplus;

            ActiveList = this;
            WinCaption = NULL;
            DeleteWinCaption = FALSE;
			pDeleteWinCaption = &DeleteWinCaption;
			imgidx = 0;
			Images[0] = NULL;
            Query = Search = NULL;
            SortRow = -1;
            WindowStyle = WS_POPUP | WS_THICKFRAME;
            WithTabStops = TRUE;
            FillOK = FALSE;
            mFont.FontHeight = StdSize;
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
             if (xfull < 1000) 
             {
                 mFont.FontHeight -= 30;
             }
 	        this->Font = &mFont;  

			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;


			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;
            cxorg = this->cx;
            cyorg = this->cy;
            x = strlen ("Suchen") + 3; 
			if (x < 9) x = 9;
			strcpy (EBuff, "");

			_Search     = new CFIELD * [2];
			_Search[0]  = new CFIELD ("Label1", "Suchen",  
				                      0, 0, 1, 1, NULL, "", CDISPLAYONLY,
												 ID_LABEL1, Font, 0, 0);
			_Search[1]  = new CFIELD ("Search", SBuff,  
				                     37, 0, x, 1, NULL, "", CEDIT,
												 ID_SEARCH, Font, 0, 0);
			 Search = new CFORM (2, _Search);

            _fListe    = new CFIELD * [1];
			_fListe[0] = new CFIELD ("Liste", "Liste",
				                     cx - 2, cy - 8, 1, 3, NULL, "", CLISTBOX,
												 LIBOX, Font, 0, 0);

			fListe     = new CFORM (1, _fListe);
			fListe->Setchsize (2, 2);

			x = 0;
			y = (cy - 2) * tm.tmHeight;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);
			
			_fButton    = new CFIELD * [2];
			_fButton[0] = new CFIELD ("OK", "OK", cx, cy, x * size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
			_fButton[1] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x + 12 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (2, _fButton);
			fButton->Setchpos (0, 2);

            _fWork    = new CFIELD * [3];
			_fWork[0] = new CFIELD ("fSearch", Search, 0, 0, 0, 0, NULL, "", CFFORM,
  		                                 983, Font, 0, 0);
			_fWork[1] = new CFIELD ("fListe", fListe, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 980, Font, 0, 0);
			_fWork[2] = new CFIELD ("Button", fButton, 0, 0, -1, 0, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (3, _fWork);
            SbuffSet = FALSE;
}

CHQEX::CHQEX (int cx, int cy, char *CheckBoxText)
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;
            int xfull, yfull;
            int checkcx;
//			int cyplus;

            ActiveList = this;
            WinCaption = NULL;
            DeleteWinCaption = FALSE;
			imgidx = 0;
			Images[0] = NULL;
            Query = Search = NULL;
            SortRow = -1;
            WindowStyle = WS_POPUP | WS_THICKFRAME;
            WithTabStops = TRUE;
            FillOK = FALSE;
            mFont.FontHeight = StdSize;
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
             if (xfull < 1000) 
             {
                 mFont.FontHeight -= 30;
             }
 	        this->Font = &mFont;  

			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;


			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;
            cxorg = this->cx;
            cyorg = this->cy;
            x = strlen ("Suchen") + 3; 
			if (x < 9) x = 9;
			strcpy (EBuff, "");

			_Search     = new CFIELD * [2];
			_Search[0]  = new CFIELD ("Label1", "Suchen",  
				                      0, 0, 1, 1, NULL, "", CDISPLAYONLY,
												 ID_LABEL1, Font, 0, 0);
			_Search[1]  = new CFIELD ("Search", EBuff,  
				                     37, 0, x, 1, NULL, "", CEDIT,
												 ID_SEARCH, Font, 0, 0);
			 Search = new CFORM (2, _Search);

            _fListe    = new CFIELD * [1];
			_fListe[0] = new CFIELD ("Liste", "Liste",
				                     cx - 2, cy - 8, 1, 3, NULL, "", CLISTBOX,
												 LIBOX, Font, 0, 0);

			fListe     = new CFORM (1, _fListe);
			fListe->Setchsize (2, 2);

			x = 0;
			y = (cy - 2) * tm.tmHeight;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);
            checkcx = strlen (CheckBoxText) * size.cx + 3 * size.cx;
			
            strcpy (CheckBoxValue, "N");
            
			_fButton    = new CFIELD * [3];

            _fButton [0] = new CFIELD ("check", CheckBoxText, CheckBoxValue,
                                                 checkcx, 
                                                 cy, x * size.cx, y,  
                                                 NULL, "", CBUTTON,
                                                 BuCheck, Font, 1, BS_AUTOCHECKBOX);

			_fButton[1] = new CFIELD ("OK", "OK", cx, cy, x * size.cx + checkcx + size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
			_fButton[2] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x * size.cx + 
                                                  checkcx + 12 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (3, _fButton);
			fButton->Setchpos (0, 2);

            _fWork    = new CFIELD * [3];
			_fWork[0] = new CFIELD ("fSearch", Search, 0, 0, 0, 0, NULL, "", CFFORM,
  		                                 983, Font, 0, 0);
			_fWork[1] = new CFIELD ("fListe", fListe, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 980, Font, 0, 0);
			_fWork[2] = new CFIELD ("Button", fButton, 0, 0, -1, 0, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (3, _fWork);
            SbuffSet = FALSE;
}


CHQEX::CHQEX (int cx, int cy, BOOL search)
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;
            int xfull, yfull;
//            int cyplus;


            ActiveList = this;
            WinCaption = NULL;
            DeleteWinCaption = FALSE;
			imgidx = 0;
			Images[0] = NULL;
            Query = Search = NULL;
            SortRow = -1;
            WindowStyle = WS_POPUP | WS_THICKFRAME;
            WithTabStops = TRUE;
            FillOK = FALSE;
            mFont.FontHeight = StdSize;
             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
             if (xfull < 1000) 
             {
                 mFont.FontHeight -= 30;
             }
 	        this->Font = &mFont;  

			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;


			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;
            cxorg = this->cx;
            cyorg = this->cy;
            x = strlen ("Suchen") + 3; 
			if (x < 9) x = 9;
			strcpy (EBuff, "");

            _fListe    = new CFIELD * [1];
			_fListe[0] = new CFIELD ("Liste", "Liste",
				                     cx - 2, cy - 4, 1, 1, NULL, "", CLISTBOX,
												 LIBOX, Font, 0, 0);

			fListe     = new CFORM (1, _fListe);
			fListe->Setchsize (2, 2);

			x = 0;
			y = (cy - 2) * tm.tmHeight;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);
			
			_fButton    = new CFIELD * [2];
			_fButton[0] = new CFIELD ("OK", "OK", cx, cy, x * size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
			_fButton[1] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x + 12 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (2, _fButton);
			fButton->Setchpos (0, 2);

            _fWork    = new CFIELD * [2];
			_fWork[0] = new CFIELD ("fListe", fListe, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 980, Font, 0, 0);
			_fWork[1] = new CFIELD ("Button", fButton, 0, 0, -1, 0, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (2, _fWork);
            SbuffSet = FALSE;
}

CHQEX::~CHQEX ()
{
	        int i;

            if (DeleteWinCaption)
            {
                delete WinCaption;
                WinCaption = NULL;
            }

  	        if (TabStops)
            {
			    delete TabStops;
			    TabStops = NULL;
            }
            if (fWork != NULL)
            {
	             fWork->destroy (); 
            }
            if (Query)
            {
                 delete _Query[0];   
                 delete _Query[1];   
                 delete _Query;   
                 delete Query;   
                 Query = NULL;
            }

            if (Search)
            {
                 delete _Search[0];   
                 delete _Search[1];   
                 delete _Search;   
                 delete Search;   
                 Search = NULL;
            }

            if (fListe)
            {
                 delete _fListe[0];   
                 delete _fListe;   
                 delete fListe;   
                 fListe = NULL;
            }

            if (fButton)
            {
                 for (i = 0; i < fButton->GetFieldanz (); i ++)
                 {
			      delete _fButton[i];
                 }
			     delete _fButton;
			     delete fButton;
                 fButton = NULL;
            }

            if (fWork != NULL)
            {
                  for (i = 0; i < fWork->GetFieldanz (); i ++)
                  {
			       delete _fWork[i];
                  }
			      delete _fWork;
			      delete fWork;
                  fWork = NULL;
            }
			for (i = 0; i < imgidx; i ++)
			{
				if (Images[i]) delete Images[i];
				Images[i] = NULL;
			}
}


HWND CHQEX::GetSearchHwnd ()
{
           if (fWork == NULL)
           {
               return NULL;
           }
           return fWork->GetCfield ("Search")->GethWnd (); 
}


void CHQEX::SetWindowStyle (DWORD style)
{
           WindowStyle = style;
}

void CHQEX::SetWindowCaption (char *Caption)
{
           this->WinCaption = new char [strlen (Caption) + 1];
           if (this->WinCaption != NULL)
           {
                    strcpy (this->WinCaption, Caption);
					DeleteWinCaption = TRUE;
           }
}

void CHQEX::EnableSort (BOOL b)
{
           fWork->GetCfield ("Liste")->EnableSort (b);
}

void CHQEX::SetSortRow (int Row, BOOL DoSort)
{
           this->SortRow = Row;
           if (DoSort)
           {
               SendMessage (fListe->GetCfield () [0]->GethWnd (), LB_SORT, Row, 0L);
           }
}

void CHQEX::SetOkFunc (int (*OkF) (int))
{
	       OkFunc = OkF;
}

void CHQEX::SetOkFuncE (int (*OkF) (char *))
{
	       OkFuncE = OkF;
}

void CHQEX::SetDialFunc (int (*DialF) (int))
{
	       DialFunc = DialF;
}

void CHQEX::SetFillDb (int (*Fi) (char *))
{
	       FillDb = Fi;
}

void CHQEX::SetSearchLst (int (*Se) (char *))
{
	       SearchLst = Se;
}

void CHQEX::AddImage (HBITMAP ibmp, HBITMAP imsk)
{
	        if (imgidx == MAXIMAGE - 1) return;
			Images[imgidx] = new IMG (hWnd, ibmp, imsk);
			imgidx ++;
			Images[imgidx] = NULL;
		    fWork->SetImage (Images, LIBOX);
}

void CHQEX::SetCurrentID (DWORD ID)
{
	       if (fWork->SetCurrentID (ID))
		   {
			   currentfield = ID;
		   }
}

void CHQEX::SetCurrentName (char *name)
{
	       if (fWork->SetCurrentName (name))
		   {
			   currentfield = fWork->GetID (name);
		   }
}

void CHQEX::EnableID (DWORD ID, BOOL flag)
{
	       fWork->EnableID (ID, flag);
}

void CHQEX::EnableName (char *name, BOOL flag)
{
	       fWork->EnableName (name, flag);
}

void CHQEX::CheckID (DWORD ID, BOOL flag)
{
	       fWork->CheckID (ID, flag);
}

void CHQEX::CheckName (char *name, BOOL flag)
{
	       fWork->CheckName (name, flag);
}

BOOL CHQEX::TestButtons (HWND hWndBu)
{
	       if (hWndBu == fWork->GethWndID (BuOk))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuOk, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
	       if (hWndBu == fWork->GethWndID (BuCancel))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuCancel, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
		   return FALSE;
}
	       

BOOL CHQEX::TestEdit (HWND hWndEd)
{
	       DWORD attr;
		   
		   attr = fWork->GetAttribut ();
		   if (attr == CEDIT)
		   {
			  if (hWndEd == fWork->GethWndID (ID_EDIT))
			  {
				         GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, 255);
	                     SendMessage (fWork->GethWndID (LIBOX), 
								            LB_RESETCONTENT, (WPARAM) 0l, 
				                            (LPARAM) 0l); 
                         if (FillDb)
                         {
						        (*FillDb) (EBuff);
                                 SendMessage (fWork->GethWndID (LIBOX), LB_SETCURSEL, 0, 0L);
                         }
                         FillOK = TRUE;
                         if (WithTabStops)
                         {
                              SethWndTabStop (fWork->GethWndID (LIBOX));
/*
                              TabStopPos ++;
*/
                              SetTabFocus ();

                         }
                         else
                         {
                              fWork->NextFormField (); 
                         }
			  }
              else if (hWndEd == fWork->GethWndID (ID_SEARCH))
			  {
 			              SendMessage (hWnd, WM_COMMAND, MAKELONG (BuOk, VK_RETURN),
				                                    (LPARAM) hWnd); 
                          return TRUE;

                          if (WithTabStops)
                          {
                              SethWndTabStop (fWork->GethWndID (LIBOX));
/*
                              TabStopPos ++;
*/
                              SetTabFocus ();

                          }
                          else
                          {
			                  fWork->NextFormField (); 
                          }
			              GetWindowText (fWork->GethWndID (ID_SEARCH), SBuff, 255);
			  }
			  else
			  {
     	                  if (WithTabStops)
                          {
			                    TabStopPos ++;
			                    SetTabFocus ();
                          }
                          else
                          {
                                fWork->NextField (); 
                          }
			  }
			  return TRUE;
		   }
		   return FALSE;
}


BOOL CHQEX::IsFocusGet (int xpos, int ypos)
{
           POINT p;
           int i;
           HWND hWnd;
           
           GetCursorPos (&p);

           if (Query)
           {
             for (i = 0; i < Query->GetFieldanz (); i ++)
             {
               hWnd = Query->GetCfield () [i]-> GethWnd ();
               if (hWnd != NULL && MouseinWindow (hWnd, &p))
               {
                   Query->GetCfield () [i]->SetFocus ();
           	       Query->SetCurrentFieldID (ID_EDIT);
           	       fWork->SetCurrentFieldID (ID_EDIT);
                   SethWndTabStop (hWnd);
                   SetTabFocus ();
                   return TRUE;
               }
             }
           }
           if (Search)
           {
             for (i = 0; i < Search->GetFieldanz (); i ++)
             {
               hWnd = Search->GetCfield () [i]-> GethWnd ();
               if (hWnd != NULL && MouseinWindow (hWnd, &p))
               {
                   Search->GetCfield () [i]->SetFocus ();
           	       Search->SetCurrentFieldID (ID_SEARCH);
           	       fWork->SetCurrentFieldID (ID_SEARCH);
                   SethWndTabStop (hWnd);
                   SetTabFocus ();
                   return TRUE;
               }
             }
           }
           hWnd = fListe->GetCfield ("Liste")->GethWnd ();
           if (hWnd != NULL && MouseinWindow (hWnd, &p))
           {
                  SethWndTabStop (hWnd);
                  SetTabFocus ();
           }
           return FALSE;
}


void CHQEX::ProcessMessages (void)
{
	      MSG msg;
		  HWND hWnd;

          if (SbuffSet == FALSE)
          {
            strcpy (SBuff, "");
          }
          fWork->SetText ();
  		  if (fWork)
          {
			SetTabStops (fWork);
          }

          hWnd = fWork->GethWndID (LIBOX);
		  fWork->SetCurrent (0);
          if (Query && SbuffSet == FALSE)
          {
		       Query->GetCfield () [1]->SetFocus ();
          }
          else if (Search)
          {
		       Search->GetCfield () [1]->SetFocus ();
          }
          else
          {
		       fListe->GetCfield () [0]->SetFocus ();
               SendMessage (fListe->GetCfield () [0]->GethWnd (), LB_SETCURSEL, 0, 0L);
          }
          if (FillOK || SbuffSet)
          {
                   SethWndTabStop (fWork->GethWndID (LIBOX));
          }
          else
          {
                   TabStopPos = 0;
          }
          SetTabFocus ();

          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
			      if (msg.wParam == VK_TAB)
				  {

                      if (GetKeyState (VK_SHIFT) < 0)
                      {
                              syskey = KEYSTAB;
							  if (msg.hwnd == fWork->GethWndID (ID_EDIT))
							  {
	                               if (WithTabStops)
                                   {
			                              TabStopPos --;
			                              SetTabFocus ();
                                   }
                                   else
                                   {
                                          fWork->PriorFormField (); 
                                   }
							  }
							  else if (msg.hwnd == fWork->GethWndID (ID_SEARCH))
							  {
	                               if (WithTabStops)
                                   {
			                              TabStopPos --;
			                              SetTabFocus ();
                                   }
                                   else
                                   {
                                          fWork->PriorFormField (); 
                                   }
							  }
							  else if (msg.hwnd == fWork->GethWndID (LIBOX))
							  {
                                   Search->GetCfield () [1]->SetFocus ();
           	                       Search->SetCurrentFieldID (ID_SEARCH);
           	                       fWork->SetCurrentFieldID (ID_SEARCH);
                              }
							  else
							  {
	                               if (WithTabStops)
                                   {
			                              TabStopPos --;
			                              SetTabFocus ();
                                   }
                                   else
                                   {
	                                      if (WithTabStops)
                                          {
			                                     TabStopPos --;
			                                     SetTabFocus ();
                                          }
                                          else
                                          {
                                                 fWork->PriorField (); 
                                          }
                                   }
							  }
					  }
					  else
					  {
					          syskey = KEYTAB;
							  if (msg.hwnd == fWork->GethWndID (ID_EDIT))
							  {
       	                          if (WithTabStops)
                                  {
			                              TabStopPos ++;
			                              SetTabFocus ();
                                  }
                                  else
                                  {
                                          fWork->NextFormField (); 
                                  }
 						          GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, 255);
							  }
							  else if (msg.hwnd == fWork->GethWndID (ID_SEARCH))
							  {
       	                          if (WithTabStops)
                                  {
			                              TabStopPos ++;
			                              SetTabFocus ();
                                  }
                                  else
                                  {
                                          fWork->NextFormField (); 
                                  }
 						          GetWindowText (fWork->GethWndID (ID_SEARCH), SBuff, 255);
							  }
							  else
							  {
       	                          if (WithTabStops)
                                  {
			                              TabStopPos ++;
			                              SetTabFocus ();
                                  }
                                  else
                                  {
                                          fWork->NextField (); 
                                  }
                              }
					  }
					  continue;
				  }
				  else if (msg.wParam == VK_F5)
				  {
					  syskey = KEY5;
					  break;
				  }
				  else if (msg.hwnd == fWork->GethWndName ("Liste"));
				  else if (msg.wParam == VK_DOWN)
				  {

					  syskey = KEYDOWN;
					  if (msg.hwnd == fWork->GethWndID (ID_EDIT))
					  {
       	                        if (WithTabStops)
                                {
			                              TabStopPos ++;
			                              SetTabFocus ();
                                }
                                else
                                {
                                          fWork->NextFormField (); 
                                }
 						        GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, 255);
					  }
					  else if (msg.hwnd == fWork->GethWndID (ID_SEARCH))
					  {
   	                            if (WithTabStops)
                                {
			                              TabStopPos ++;
			                              SetTabFocus ();
                                }
                                else
                                {
                                          fWork->NextFormField (); 
                                }
 						        GetWindowText (fWork->GethWndID (ID_SEARCH), SBuff, 255);
					  }
					  else
					  {
  	                          if (WithTabStops)
                              {
			                              TabStopPos ++;
			                              SetTabFocus ();
                              }
                              else
                              {
                                          fWork->NextField (); 
                              }
					  }
					  continue;
				  }
				  else if (msg.wParam == VK_UP)
				  {

					  syskey = KEYUP;
					  if (msg.hwnd == fWork->GethWndID (ID_EDIT))
					  {
                                if (WithTabStops)
                                {
	                              TabStopPos --;
	                              SetTabFocus ();
                                }
                                else
                                {
                                   fWork->PriorFormField (); 
                                }
					  }
					  else if (msg.hwnd == fWork->GethWndID (ID_SEARCH))
					  {
                                if (WithTabStops)
                                {
	                              TabStopPos --;
	                              SetTabFocus ();
                                }
                                else
                                {
                                   fWork->PriorFormField (); 
                                }
					  }
					  else
					  {
 	                            if (WithTabStops)
                                {
			                              TabStopPos --;
			                              SetTabFocus ();
                                }
                                else
                                {
	                                      if (WithTabStops)
                                          {
			                                       TabStopPos --;
			                                       SetTabFocus ();
                                          }
                                          else
                                          {
                                                   fWork->PriorField (); 
                                          }
                                }
					  }
					  continue;
				  }
				  else if (msg.wParam == VK_RETURN)
				  {
					  syskey = KEYCR;
					  if (TestButtons (msg.hwnd))
					  {
					           continue;
					  }
					  if (TestEdit (msg.hwnd))
					  {
					           continue;
					  }
				  }
			  }
              else if (msg.message == WM_LBUTTONDOWN)
              {
                  if (IsFocusGet (LOWORD (msg.lParam), HIWORD (msg.lParam)))
                  {
                       continue;
                  }
              }
              TranslateMessage(&msg);
			  DispatchMessage(&msg);
		  }
          fButton->GetText ();
}


CALLBACK CHQEX::CProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
                    if (fWork)
                    {
					     fWork->display (hWnd, hdc);
                    }
                    EndPaint (hWnd, &ps);
					break;
              case WM_SIZE :
				    MoveWindow ();
					break;

              case WM_SYSCOMMAND :
                    if (wParam == SC_CLOSE)
                    {
						   syskey = KEY5;
                           PostQuitMessage (0);
                           return 0;
                    }
              case WM_COMMAND :
                    if (LOWORD (wParam) == BuOk)
                    {
						   syskey = KEYCR;
						   if (OkFuncE)
						   {
			                   SendMessage (fWork->GethWndID (LIBOX), 
								            LB_RESETCONTENT, (WPARAM) 0l, 
				                            (LPARAM) 0l); 
							   (*OkFuncE) (EBuff);
                               FillOK = TRUE;
							   SetFocus (fWork->GethWndID (LIBOX));
                               SendMessage (fWork->GethWndID (LIBOX), LB_SETCURSEL, 0, 0L);
						   }
						   else
						   {
                                PostQuitMessage (0);
						   }
                    }
                    if (LOWORD (wParam) == BuDial)
                    {
						   syskey = KEYCR;
						   if (DialFunc)
						   {
							   (*DialFunc) (GetSel ());
						   }
                    }
                    else if (LOWORD (wParam) == BuCancel)
                    {
						   syskey = KEY5;
                           PostQuitMessage (0);
                    }
                    else if (LOWORD (wParam) == LIBOX)
                    {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
						          syskey = KEYCR;
					 	          if (OkFunc)
								  {
							              (*OkFunc) (GetSel ());
								  }
						          else
								  {
                                           PostQuitMessage (0);
								  }
						   }
                           else if (HIWORD (wParam) == VK_RETURN)
                           {
						          syskey = KEYCR;
					 	          if (OkFunc)
								  {
							              (*OkFunc) (GetSel ());
								  }
						          else
								  {
                                           PostQuitMessage (0);
								  }
						   }
                           else if (HIWORD (wParam) == LBN_SELCHANGE)
                           {
		                          fWork->SetCurrent (listpos);
						   }
                           else if (HIWORD (wParam) == LBN_SETFOCUS)
                           {
							       SetOkFuncE (NULL);
						   }
                    }
                    else if (LOWORD (wParam) == ID_EDIT)
                    {
                           if (HIWORD (wParam) == EN_SETFOCUS)
                           {
							       SetWindowText (fWork->GethWndID (ID_EDIT), EBuff);
								   fWork->SetCurrentFieldID (ID_EDIT);
                                   SendMessage (fWork->GethWndID (ID_EDIT), 
									   EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
                                   SethWndTabStop (fWork->GethWndID (ID_EDIT));
							       SetOkFuncE (FillDb);
						   }
						   else if (HIWORD (wParam) == EN_KILLFOCUS)
						   {
							       GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, 255);
						   }

						   else if (HIWORD (wParam) == EN_CHANGE)
						   {
							       GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, 255);
						   }
					}
                    else if (LOWORD (wParam) == ID_SEARCH)
                    {
                           if (HIWORD (wParam) == EN_SETFOCUS)
                           {
                                   clipped (SBuff);
                                   if (strcmp (SBuff, " ") == 0) SBuff[0] = 0;
							       SetWindowText (fWork->GethWndID (ID_SEARCH), SBuff);
								   fWork->SetCurrentFieldID (ID_SEARCH);
                                   SendMessage (fWork->GethWndID (ID_SEARCH), 
									   EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
                                   SethWndTabStop (fWork->GethWndID (ID_SEARCH));
						   }
						   else if (HIWORD (wParam) == EN_KILLFOCUS)
						   {
							       GetWindowText (fWork->GethWndID (ID_SEARCH), SBuff, 255);
						   }
						   else if (HIWORD (wParam) == EN_CHANGE)
						   {
							       GetWindowText (fWork->GethWndID (ID_SEARCH), SBuff, 255);
                                   clipped (SBuff);
                                   if (strcmp (SBuff, " ") == 0) SBuff[0] = 0;
								   if (SearchLst)
								   {
									   (*SearchLst) (SBuff);
								   }
                                   else
                                   {
                                        ActiveList->SearchList ();
                                   }
						   }
					}
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void CHQEX::InsertCaption (char *buffer)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);
		 
		 SendMessage (hWnd, LB_TITLE, -1,  
                                 (LPARAM) (char *) buffer);
}



void CHQEX::InsertRecord (char *buffer)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

         SendMessage (hWnd, LB_INSERTSTRING, -1,  
                                 (LPARAM) (char *) buffer);
}

void CHQEX::UpdateRecord (char *buffer, int pos)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

         SendMessage (hWnd, LB_INSERTSTRING, pos,  
                                 (LPARAM) (char *) buffer);
}

void CHQEX::GetText (char *buffer)
{
	     HWND hWnd;
		 int idx;

         hWnd = fWork->GethWndID (LIBOX);

	     idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
		 GetLboxText (hWnd, idx, buffer);
//	     SendMessage (hWnd, LB_GETTEXT, idx, (LPARAM) buffer);
}

int CHQEX::GetSel (void)
{
	     HWND hWnd;
		 int idx;

         hWnd = fWork->GethWndID (LIBOX);

	     idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
		 return idx;
}

int CHQEX::GetCount (void)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

	     return SendMessage (hWnd, LB_GETCOUNT, 0, 0l);
}

void CHQEX::SetSel (int idx)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

	     SendMessage (hWnd, LB_SETCURSEL, (WPARAM) idx, (LPARAM) 0l);
		 return;
}

void CHQEX::SearchList (void)
{
        Search->GetText (); 
        ::SearchList (fWork->GetCfield ("Liste")->GethWnd (), clipped (SBuff));
		return;
}

void CHQEX::SearchList (char *Text)
{
	    strcpy (EBuff, Text);
        Search->SetText (); 
        ::SearchList (fWork->GetCfield ("Liste")->GethWnd (), clipped (SBuff));
		return;
}

HWND CHQEX::GethWnd (void)
{
         return hWnd;
}

void CHQEX::VLines (char *vpos, int hlines)
{
	       HWND hWnd;

		   hWnd = fWork->GethWndName ("Liste");
           SendMessage (hWnd, LB_CAPTSIZE, 0,  
                                 (LPARAM) 130);
           SendMessage (hWnd, LB_VPOS, hlines,  
                                 (LPARAM) (char *) vpos);
}


void CHQEX::RowAttr (char *RowAttr)
{
	       HWND hWnd;

		   hWnd = fWork->GethWndName ("Liste");
           SendMessage (hWnd, LB_ROWATTR, 0,  
                                 (LPARAM) (char *) RowAttr);
}

void CHQEX::RowPos (char *RowPos)
{
      HWND hWnd;
      hWnd = fWork->GethWndName ("Liste");
      SendMessage (hWnd, LB_RPOS, 0, (LPARAM) (char *) RowPos);
}

void CHQEX::DestroyWindow (void)
{
	       fWork->destroy ();
		   ::DestroyWindow (hWnd);
}
						
void CHQEX::OpenWindow (HANDLE hInstance, HWND hMainWindow)
{
		  int x, y;
          WNDCLASS wc;
		  RECT rect, rect1;
 		  COLORREF StdBackCol;

		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;
//		  SetColBorderM (TRUE);

		  if (registered == FALSE)
		  {
                  StdBackCol = GetSysColor (COLOR_3DFACE);
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
//                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.hbrBackground =  CreateSolidBrush (StdBackCol);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CQueryWindEx";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }

		  GetClientRect (hMainWindow, &rect);
		  GetWindowRect (hMainWindow, &rect1);

		  if (WindowStyle & WS_CAPTION) 
		  {
			  cy = cyorg + GetSystemMetrics (SM_CYSMCAPTION); 
		  }


          if (hMainWindow != NULL)
          {
		        GetClientRect (hMainWindow, &rect);
		        GetWindowRect (hMainWindow, &rect1);

		        if (WindowStyle & WS_CAPTION) 
                {
			           cy = cyorg + GetSystemMetrics (SM_CYSMCAPTION); 
                }
          }
          else
          {
                rect.left = 0;
                rect.top = 0;
                rect.right = GetSystemMetrics (SM_CXFULLSCREEN);
                rect.bottom = GetSystemMetrics (SM_CYFULLSCREEN);
                DLG::GetDockParams (&rect);
                memcpy (&rect1, &rect, sizeof (rect));
          }

		  x  = max (0, (rect.right - cx) / 2)  + rect1.left;
		  y  = max (0, (rect.bottom - cy) / 2) + rect1.top;

          hWnd = CreateWindowEx (0, 
                                 "CQueryWindEx",
                                 "",
                                 WindowStyle,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
		  if (WindowStyle & WS_CAPTION && WinCaption != NULL)
          {
              SetWindowText (hWnd, WinCaption);
          }
		  ShowWindow (hWnd, SW_SHOWNORMAL);
		  UpdateWindow (hWnd);
          fWork->MoveWindow ();
}

void CHQEX::OpenWindow (HANDLE hInstance, HWND hMainWindow, BOOL mSize)
{
		  int x, y;
          WNDCLASS wc;
		  RECT rect, rect1;
 		  COLORREF StdBackCol;


          if (mSize == FALSE)
          {
              OpenWindow (hInstance, hMainWindow);
              return;
          }
		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;

		  if (registered == FALSE)
		  {
                  StdBackCol = GetSysColor (COLOR_3DFACE);
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
//                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.hbrBackground =  CreateSolidBrush (StdBackCol);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CQueryWindEx";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }

          if (hMainWindow != NULL)
          {
		        GetClientRect (hMainWindow, &rect);
		        GetWindowRect (hMainWindow, &rect1);
                DWORD Style = GetWindowLong (hMainWindow, GWL_STYLE); 
                DWORD ExStyle = GetWindowLong (hMainWindow, GWL_EXSTYLE); 
 
                if ((ExStyle & WS_EX_CLIENTEDGE) ||
                    (Style & WS_DLGFRAME))
                {
 				       rect1.left += GetSystemMetrics (SM_CXEDGE);
				       rect1.top   += GetSystemMetrics (SM_CYEDGE);
                }

		        if (WindowStyle & WS_CAPTION) 
                {
			           cy = cyorg + GetSystemMetrics (SM_CYSMCAPTION); 
                }
		        x  = max (0, (rect.right - this->cx) / 2)  + rect1.left;
		        y  = max (0, (rect.bottom - this->cy) / 2) + rect1.top;
          }
          else
          {
                rect.left = 0;
                rect.top = 0;
                rect.right = GetSystemMetrics (SM_CXFULLSCREEN);
                rect.bottom = GetSystemMetrics (SM_CYFULLSCREEN);
                rect1.left = 0;
                rect1.top = 0;
                rect1.right = GetSystemMetrics (SM_CXFULLSCREEN);
                rect1.bottom = GetSystemMetrics (SM_CYFULLSCREEN);
		        if (WindowStyle & WS_CAPTION) 
                {
			           cy = cyorg + GetSystemMetrics (SM_CYSMCAPTION); 
                }
		        x  = max (0, (rect.right - this->cx) / 2)  + rect1.left;
		        y  = max (0, (rect.bottom - this->cy) / 2) + rect1.top;
          }


          hWnd = CreateWindowEx (0, 
                                 "CQueryWindEx",
                                 "",
                                 WindowStyle,
                                 x, y,
                                 this->cx, this->cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
		  if (WindowStyle & WS_CAPTION && WinCaption != NULL)
          {
              SetWindowText (hWnd, WinCaption);
          }

		  ShowWindow (hWnd, SW_SHOWNORMAL);
  	      UpdateWindow (hWnd);
          if (hMainWindow == NULL)
          {
               DLG::GetDockParams (&rect);
               ::MoveWindow (hWnd, rect.left, rect.top, rect.right, rect.bottom, TRUE);
          }
}



void CHQEX::MoveWindow (void)
{
          if (hWnd != NULL)
          {
	            fWork->MoveWindow ();
                InvalidateRect (hWnd, NULL, TRUE);
          }
}


int CHQEX::GetTabStoplen (CFORM *fWork, int len)
{
	    int i;
		CFORM *Cform;

		for (i = 0; i < fWork->GetFieldanz (); i ++)
		{
          	if (fWork->GetCfield ()[i]->GetAttribut () == CFFORM)
			{
		           Cform = (CFORM *) fWork->GetCfield ()[i]->GetFeld ();
				   len = GetTabStoplen (Cform, len);
				   continue;
			}
			if (fWork->GetCfield ()[i]->GetTabstop ())
			{
				    len ++;
			}
		}
		return len;
}


void CHQEX::AddTabStops (CFORM *fWork)
{
	    int i;
		CFORM *Cform;
		HWND hWnd;

		for (i = 0; i < fWork->GetFieldanz (); i ++)
		{
          	if (fWork->GetCfield ()[i]->GetAttribut () == CFFORM)
			{
		           Cform = (CFORM *) fWork->GetCfield ()[i]->GetFeld ();
				   AddTabStops (Cform);
				   continue;
			}
			if (fWork->GetCfield ()[i]->GetTabstop ())
			{
  			       hWnd = fWork->GetCfield ()[i]->GethWnd ();
				   if (hWnd)
				   {
					   TabStops [TabStoplen] = hWnd;
					   TabStoplen ++;
				   }
			}
		}
}


void CHQEX::UpdateTabstop (HWND hWnd)
{
        int i;

        if (TabStops == NULL) return;

        for (i = 0; i < TabStoplen; i ++)
        {
            if (TabStops[i] == hWnd)
            {
                TabStopPos = i;
                return;
            }
        }
}

void CHQEX::SetTabStops (CFORM *fWork)
{
		int len;
		len = GetTabStoplen (fWork, 0);
		if (len <= 0) return;
        TabStops = new HWND [len];
		if (TabStops == NULL) return;
		TabStoplen = 0;
		AddTabStops (fWork);
}

void CHQEX::DestroyTabStops (void)
{

		if (TabStops)
		{
			delete TabStops;
			TabStops = NULL;
			TabStoplen = 0;
		}
}

void CHQEX::NewTabStops (CFORM *fWork)
{
	    DestroyTabStops ();
		SetTabStops (fWork);
}

void CHQEX::SethWndTabStop (HWND hWnd)
{
        int i;

	    if (WithTabStops == FALSE) return;

        for (i = 0; i < TabStoplen; i ++)
        {
            if (hWnd == TabStops[i])
            {
                TabStopPos = i;
                return;
            }
        }
}
void CHQEX::SetTabFocus (void)
{
       
        if (TabStoplen == 0) return;
	    if (TabStopPos >= TabStoplen)
		{
			TabStopPos = 0;
		}
		else if (TabStopPos < 0)
		{
			TabStopPos = TabStoplen -1;
		}
		SetFocus (TabStops[TabStopPos]);
}
