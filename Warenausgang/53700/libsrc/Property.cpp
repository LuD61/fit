#include <windows.h>
#include <stdio.h>
#include "strfkt.h"
#include "Property.h"


Prop::Prop (char * Property)
{
	      Token t;
		  t.SetSep ("=");
		  t = Property;
		  if (t.GetAnzToken () < 2)
		  {
			  return;
		  }

		  Item = t.NextToken ();
		  Value = t.NextToken ();
		  Item.TrimRight ();
		  Item.TrimLeft ();
		  Value.TrimRight ();
		  Value.TrimLeft ();
}

Prop::Prop (Text& Item, Text& Value)
{
	     this->Item  = Item;
		 this->Value = Value;
}

Text* Prop::GetItem ()
{
	     return &Item;
}
void Prop::SetItem (Text& Item)
{
	     this->Item = Item;
}

Text* Prop::GetValue ()
{
	     return &Value;
}
void Prop::SetValue (Text& Value)
{
	     this->Value = Value;
}


CProperty::CProperty ()
{
}

CProperty::~CProperty ()
{
	      properties.DestroyAll ();
		  properties.Init ();
}


BOOL CProperty::Load (Text& FileName)
{
	       FILE *fp;
		   char buffer [256];

 	       properties.DestroyAll ();
		   properties.Init ();
		   fp = fopen (FileName.GetBuffer (), "r");
		   if (fp == NULL)
		   {
			   return FALSE;
		   }

		   while (fgets (buffer, 255, fp))
		   {
			   cr_weg (buffer);
			   Text txt = buffer;
			   txt.TrimRight ();
			   Prop *p = new Prop (txt.GetBuffer ());
			   properties.Add (p);
		   }
		   fclose (fp);
		   return TRUE;
}

Text* CProperty::GetValue (Text& Item)
{
	       Prop *prop;
 		   Item.TrimRight ();
	       properties.FirstPosition ();
		   while ((prop = (Prop *) properties.GetNext ()) != NULL)
		   {
			   if (prop->Item == Item)
			   {
				   return &prop->Value;
			   }
		   }
		   return NULL;
}

Text* CProperty::GetItem (Text& Value)
{
	       Prop *prop;
  		   Value.TrimRight ();
	       properties.FirstPosition ();
		   while ((prop = (Prop *) properties.GetNext ()) != NULL)
		   {
			   if (prop->Value == Value)
			   {
				   return &prop->Item;
			   }
		   }
		   return NULL;
}

Text* CProperty::GetItem (int value)
{
	       Prop *prop;
		   Text Value;
		   Value = value;
  		   Value.TrimRight ();
	       properties.FirstPosition ();
		   while ((prop = (Prop *) properties.GetNext ()) != NULL)
		   {
			   if (prop->Value == Value)
			   {
				   return &prop->Item;
			   }
		   }
		   return NULL;
}

Text *CProperty::GetItemAt (int idx)
{
	      Prop *prop;
		  prop = (Prop *) properties.Get (idx);
		  if (prop == NULL)
		  {
			  return NULL;
		  }
		  return &prop->Item;
}

Text *CProperty::GetValueAt (int idx)
{
	      Prop *prop;
		  prop = (Prop *) properties.Get (idx);
		  if (prop == NULL)
		  {
			  return NULL;
		  }
		  return &prop->Value;
}

void CProperty::Add (Text *txt)
{
		   txt->TrimRight ();
 		   Prop *p = new Prop (txt->GetBuffer ());
		   properties.Add (p);
}


	      
