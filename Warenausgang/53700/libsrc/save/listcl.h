#ifndef _eListDef
#define _eListDef

#define MAXFIELD 200

struct CHATTR
{
    char *feldname;
    int  UpdAttr;
    int  InsAttr;
};

void SetComboMess (BOOL (*) (MSG *));

class ListClass
{
      private :
             int ListBreak; 
             char *eingabesatz;
             int NoScroll;
             int lPagelen;
             int LineRow;
             HWND    hwndTB;
             HWND    hTrack;
             HWND    vTrack;

             char frmrow [0x1000];
             TEXTMETRIC tm;
             PAINTSTRUCT aktpaint;

             BOOL IsArrow;
             HCURSOR oldcursor;
             HCURSOR arrow;
             HCURSOR aktcursor;
             int movfield; 
             int InMove;
			 RECT FocusRect;
             HFONT EditFont;
             mfont UbFont;
             mfont UbSFont;
             mfont SFont;
             mfont ListFont;
             int LineColor;
             int InAppend;
             int NewRec;
             int paintallways;
             COLORREF Color;
             COLORREF BkColor;
             struct CHATTR *ChAttr;
             int UbHeight;

      public :
             unsigned char *ausgabesatz;
             int SwRecs;
             char **SwSaetze;
             char **LstSaetze;
             unsigned char *LstSatz;
             FELDER *LstZiel;
             int zlenS;
             int scrollpos;
             int scrollposS;
             int recanz;
             int recHeight ;
             int PageView;
             HANDLE  hMainInst;
             HWND    hMainWindow; 
             HWND    mamain2; 
             HWND    mamain3; 
             field _UbForm [MAXFIELD];
             form UbForm;
             field _DataForm [MAXFIELD];
             form DataForm;
             field _LineForm [MAXFIELD];
             form LineForm;
             form *UbRow;
             form *DataRow;
             form *lineRow;
             int banz;
             int zlen;
             int banzS;
			 int WithFocus;
			 int  AktRow;
			 int  SelRow;
			 int AktColumn;
			 int  AktRowS;
             int AktPage;
			 int AktColumnS;
             int FindRow;
             int FindColumn;
			 char AktItem [21];
			 char AktValue [80];
			 char RowItem [21];
			 char RowValue [80];
             BOOL UseRowItem;
			 int  ListFocus;
			 int  AktFocus;
			 HWND FocusWindow;
             int RowMode;
             int UseZiel;
             int FrmRow;
             int (*InfoProc) (char **, char *, char *);
             int (*TestAppend) (void);

             void SetListFont (mfont *lFont)
             {
                 memcpy (&UbFont,  lFont, sizeof (mfont));   
                 memcpy (&UbSFont, lFont, sizeof (mfont));   
                 memcpy (&SFont,   lFont, sizeof (mfont));   
                 memcpy (&ListFont,lFont, sizeof (mfont));
             }

             BOOL IsNewRec (void)
             {
                 return NewRec;
             }

             BOOL IsAppend (void)
             {
                 return InAppend;
             }

             void SetInfoProc (int (*Proc) (char **, char *, char *))
             {
                 InfoProc = Proc;
             }

             void SetTestAppend (int (*Proc) (void))
             {
                 TestAppend = Proc;
             }

			 char *GetAktItem (void)
			 {
                 if (UseRowItem)
                 {
                     return RowItem;

                 }
				 return AktItem;
			 }

			 char *GetAktValue (void)
			 {
                 if (UseRowItem)
                 {
                     return RowValue;

                 }
				 return AktValue;
			 }

             void SetChAttr (struct CHATTR *cha)
             {
                 ChAttr = cha;
             }

             ListClass ()
             {
                    IsArrow = FALSE;
                    oldcursor = NULL;;
                    arrow = NULL;
                    aktcursor = LoadCursor (NULL, IDC_ARROW);
                    movfield = 0; 
                    InMove = 0;
					AktRow = 0;
					SelRow = 0;
					AktColumn = 0;
					AktRowS = 0;
					AktColumnS = 0;
					WithFocus = 1;
					recanz = 0;
					recHeight = 1;
					banz = 0;
					ListFocus = 1;
					FocusWindow = NULL;
                    RowMode = 0;
                    LstZiel = NULL;
                    PageView = 0;
                    EditFont = NULL;
                    LineColor = 3;
                    UseZiel = 0;
                    InfoProc = NULL;
                    TestAppend = NULL;
                    TestAppend = 0;
                    paintallways = 0;
                    ausgabesatz = NULL;
                    Color = RGB (0, 0, 0);;
                    BkColor = RGB (255, 255, 255); 
                    NewRec = 0;
                    InAppend = 0;
                    ChAttr = NULL;
                    UseRowItem = FALSE;
                    FrmRow = 0;
             }

             void SetRowItem (char *name, char *value)
             {
                     strcpy (RowItem, name);
                     strcpy (RowValue, value);
                     UseRowItem = TRUE;
             }

             void InitRowItem (void)
             {
                     UseRowItem = FALSE;
             }

             void SetAusgabeSatz (unsigned char *satz)
             {
                    ausgabesatz = satz;
             }

             void BreakList (void)
             {
                    ListBreak = 1;
                    PostQuitMessage (0);
             }

             void SetListFocus (int focus)
             {
                    ListFocus = focus;
             }

             void SetLines (int LineNr)
             {
                     LineColor = LineNr;
             }

             void SetPageView (int pg)
             {
                     PageView = pg;
             }

             void SetLstZiel (FELDER *zl)
             {
                     LstZiel = zl;
             }

             void SetRowMode (int mode)
             {
                    RowMode = mode;
             }

             void SetPos (int zeile, int spalte)
             {
					AktRow = zeile;
					AktColumn = spalte;
             }

             void Initscrollpos (void)
             {
                    scrollpos = 0;
             }

             void Setscrollpos (int pos)
             {
                 scrollpos = pos;
             }

             int Getscrollpos (void)
             {
                 return scrollpos;
             }


             void InitRecanz (void)
             {
                 recanz = 0;
             }

             int  GetRecanz (void)
             {
                 return recanz;
             }

             int GetRecHeight (void)
             {
                 return recHeight;
             }

             int  GetAktRow (void)
             {
                 return AktRow;
             }

             int  GetAktRowS (void)
             {
                 return AktRowS;
             }

             int  GetAktColumn (void)
             {
                 return AktColumn;
             }

             void SethwndTB (HWND hwndTB)
             {
                 this->hwndTB = hwndTB;
             }

             void SetSaetze (char **Saetze)
             {
                 SwSaetze = Saetze;
             }

             void Setbanz (int banz)
             {
                 this->banz = banz;
             }

             void Setzlen (int zlen)
             {
                 this->zlen = zlen;
             }

             void SetRecanz (int anz)
             {
                 recanz = anz;
             }

             void SetRecHeight (int height)
             {
                 recHeight = height;
             }

             void SetPagelen (int Pagelen)
             {
                 lPagelen = Pagelen;
             }

             void SetLineRow (int LineRow)
             {
                 this->LineRow = LineRow;
             }

             void SetTextMetric (TEXTMETRIC *ttm)
             {
                 memcpy (&tm, ttm, sizeof (tm));
             }

             void GetTextMetric (TEXTMETRIC *ttm)
             {
                 memcpy (ttm, &tm, sizeof (tm));
             }
             void SetAktPaint (PAINTSTRUCT *pm)
             {
                 memcpy (&aktpaint, pm, sizeof (aktpaint));
             }

             HWND Getmamain2 (void)
             {
                  return mamain2;
             }

             HWND Getmamain3 (void)
             {
                  return mamain3;
             }

             HWND GethTrack (void)
             {
                  return  hTrack;
             }

             HWND GetvTrack (void)
             {
                  return  vTrack;
             }

             BOOL IsRowMode (void)
             {
                 return RowMode;
             }

             HWND     InitListWindow (HWND);
             void     GetPageLen (void);
             BOOL     TrackNeeded (void);
             void     CreateTrack (void);
             void     DestroyTrack (void); 

             BOOL     VTrackNeeded (void);
             void     CreateVTrack (void);
             void     DestroyVTrack (void);
             void     TestTrack (void);
             void     TestVTrack (void);
             void     TestMamain3 (void);
             void     MoveListWindow (void);
             void     SetDataStart (void);
             BOOL     MustPaint (int);
             void     PrintUSlinesSatzNr (HDC);
             void     PrintSlinesSatzNr (HDC);
             void     PrintVlineSatzNr (HDC);
             void     PrintVlines (HDC);
             void     PrintVline (HDC, int);
             void     PrintHlinesSatzNr (HDC);
             void     PrintHlineSatzNr (HDC, int);
             void     PrintHlines (HDC);
             void     PrintHline (HDC, int);
			 void     ShowFocusText (HDC, int, int);
			 void     GetFocusText (void);
			 void     ShowWindowText (int, int);
			 void     DestroyFocusWindow (void);
             void     SetFeldEdit (int, int);
             void     SetFocusFrame (HDC, int, int);
             void     SetFeldEdit0 (int, int);
			 void     SetFeldFrame (HDC, int, int);
			 void     SetFeldFocus (HDC, int, int);
			 void     SetFeldFocus0 (int, int);
             int      DelRec (void); 
             void     FillFrmRow (char *, form *, int);
             void     ShowFrmRow (HDC, int, int);
             void     ShowDataForms (HDC);
             void     ScrollLeft (void);
             void     ScrollRight (void);
             void     ScrollPageDown (void);
             void     ScrollPageUp (void);
             void     SetPos (int);
             void     SetTop (void);
             void     SetBottom (void);
             void     HScroll (WPARAM, LPARAM);
             void     SetVPos (int);
             void     ScrollDown (void);
             void     ScrollUp (void);
             void     ScrollVPageDown (void);
             void     ScrollVPageUp (void);
             void     VScroll (WPARAM, LPARAM);
			 void     FocusLeft (void);
			 void     FocusRight (void);
			 void     FocusUp (void);
			 void     FocusDown (void);
             void     ZielFormat (char *, FELDER *);
             BOOL     IsInListArea (MSG *);
             void     OnPaint (HWND, UINT, WPARAM, LPARAM);
             void     OnSize (HWND, UINT, WPARAM, LPARAM);
             void     OnHScroll (HWND, UINT, WPARAM, LPARAM);
             void     OnVScroll (HWND, UINT, WPARAM, LPARAM);
             void     FunkKeys (WPARAM, LPARAM);
             void     FillUbForm (field *, char *, int, int,int);
             void     FillDataForm (field *, char *, int, int);
             void     FillLineForm (field *, int);
             void     MakeLineForm (form *);
             void     MakeDataForm (form *);
             void     MakeUbForm (void);
             void     MakeDataForm0 (void);

             void     SetLineForm (form *);
             void     SetDataForm (form *);
             void     SetUbForm (form *);
             void     SetDataForm0 (form *, form *);

             void     FreeLineForm (void);
             void     FreeDataForm (void);
             void     FreeUbForm (void);
             BOOL     IsMouseMessage (MSG *);
             void     StopMove (void);
             void     StopMove0 (void);
             void     StartMove (void);
             void     MoveLine (void);
             void     TestNextLine (void);
             int      GetFieldPos (char *);
             void     DestroyField (int);
             void     ShowArrow (BOOL);
             int      IsUbEnd (MSG *);
             int      IsUbRow (MSG *);
			 void     EditScroll (void);
             void     ToDlgRec (char *);
             void     FromDlgRec (char *);
             void     ToDlgRec0 (char *);
             void     FromDlgRec0 (char *);
             void     FreeDlgSaetze (void);
             void     SwitchPage (int);
             void     SetPage (int);
             void     SetPage0 (int);
             void     PriorPageRow (void);
             void     NextPageRow (void);
             void     SetFont (mfont *);
             void     ChoiseFont (mfont *);
             void     ChoiseLines (HDC);
             BOOL     InRec (char *, int *);
             void     find (char *);
             void     FindString (void);
             void     FindNext (void);
             BOOL     NewSearchString ();
             void     SetListLines (int); 
             int      ProcessMessages(void);
             void     SetColors (COLORREF, COLORREF);

             BOOL    IsEditColumn (int);
             int     FirstColumn (void);
             int     PriorColumn (void);
             int     NextColumn (void);
             int     FirstColumnR (void);
             int     PriorColumnR (void);
             int     NextColumnR (void);
             int     TestAfter (void);
             int     TestBefore (void);
             int     TestAfterR (void);
             int     TestBeforeR (void);
             int     TestAfterRow (void);
             int     TestBeforeRow (void);
             void    InsertLine (void);
             void    AppendLine (void);
             void    DeleteLine (void);
             void    ShowAktRow (void);
             void    ShowAktColumn (HDC, int, int, int);
             void    DisplayList (void);
             void    KillListFocus (void);
             void    SetListFocus (void);
             void    SetInsAttr (void);
             void    SetFieldAttr (CHATTR *);
             void    DestroyRmFields (void);
             void    DestroyListWindow (void);
             void    ScrollUbForm (void);
};

class ListClassDB : public ListClass
{
     public :
            ListClassDB () : ListClass ()
            {
            }
            void     FreeBezTab (void);
            void     FillUbForm (field *, char *, int, int,int);
            void     MakeUbForm (void);
            void     SwitchPage (int);
            void     SwitchPage0 (int);
            char *   GetItemName (char *); 
};

#endif 
