#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "dlg.h" 
#include "mo_gdruck.h" 
#include "colbut.h"
#ifdef BIWAK 
#include "conf_env.h" 
#endif

extern BOOL ColBorder;


static int StdSize = STDSIZE;

static mfont dlgfont = {"Arial", StdSize, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};



HINSTANCE DLG::hInstance;
BOOL DLG::MousePressed = FALSE;
double DLG::scrfx = 1.0;
double DLG::scrfy = 1.0;
DLG *DLG::ActiveDlg = NULL;
DLG *DLG::Instances [MAXINSTANCES];
int DLG::instanz = 0;
struct COLW DLG::ColWindows [MAXCOLS];
int DLG::colwanz = 0;
BOOL DLG::ScChar = FALSE;

void DLG::ScreenParam (double scrx, double scry)
{
	    scrfx = scrx;
	    scrfy = scry;
}

void DLG::SetStyle (DWORD style)
{
         this->style = style;
}

void DLG::SetStyleEx (DWORD StyleEx)
{
         this->StyleEx = StyleEx;
}

void DLG::SetMenue (struct PMENUE *pm, BOOL (*tm) (HWND,UINT,WPARAM,LPARAM))
{
         if (pm)
         {
                 menuetab = pm;
                 hMenu = MakeMenue (menuetab);
                 if (hWnd)
                 {
                         SetMenu (hWnd, hMenu);
                 }
         }
         TestMenue = tm;
}

void DLG::SetTimerProc (void (*tm) (void))
{
         TimerProc = tm;
}

void DLG::SetWinBackground (COLORREF Col)
{
          WinBackground = Col;
          if (hWnd != NULL && WinBackground != ClassBackground)
          {
              SetClassLong (hWnd, GCL_HBRBACKGROUND, 
                                  (long) CreateSolidBrush (WinBackground));
          }
}

void DLG::SetToolbar (TBBUTTON *tbb0, int tblen0, char **qInfo0, UINT *qIdfrom0,
                      char **qhWndInfo0, HWND **qhWndFrom0) 
 
{
         tbb       = tbb0;
         tblen     = tblen0;
         qInfo     = qInfo0;
         qIdfrom   = qIdfrom0;
         qhWndInfo = qhWndInfo0;
         qhWndFrom = qhWndFrom0;

         if (hWnd)
         {
                  hwndTB = MakeToolBarEx (hInstance, 
		                       hMainWindow,tbb, tblen,
                               qInfo, qIdfrom,
		                       qhWndInfo, qhWndFrom);
         }

}


void DLG::SetSize (int Size)
{
		int xfull, yfull;

		if (Size > 0) 
		{
				StdSize = Size;
        }
        if (StdSize > 150) StdSize = 150;
        xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

        if (xfull < 1000) StdSize -= 20;
        dlgfont.FontHeight = StdSize;
}


void DLG::SetLocation (int x, int y)
{
		TEXTMETRIC tm;
		HFONT hFont, oldfont;
 	    SIZE size;
		HDC hdc;


		hdc = GetDC (NULL);
        hFont = SetDeviceFont (hdc, Font, &tm);
        oldfont = SelectObject (hdc, hFont);
	    GetTextMetrics (hdc, &tm);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
		DeleteObject (SelectObject (hdc, oldfont));
        xorg = x;
        yorg = y;

		this->x = size.cx * x;
		this->y = (int) (double) ((double) tm.tmHeight * y * 1.3);
}


void DLG::SetDimension (int cx, int cy)
{
		TEXTMETRIC tm;
		HFONT hFont, oldfont;
 	    SIZE size;
		HDC hdc;


		hdc = GetDC (NULL);
        hFont = SetDeviceFont (hdc, Font, &tm);
        oldfont = SelectObject (hdc, hFont);
	    GetTextMetrics (hdc, &tm);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
		DeleteObject (SelectObject (hdc, oldfont));
        cxorg = cx;
        cyorg = cy;
        
        this->cx = cx;
        this->cy = cy;
        if (this->cx > 0)
        {
		        this->cx = size.cx * cx;
        }
        if (this->cy > 0)
        {
		        this->cy = (int) (double) ((double) tm.tmHeight * cy * 1.3);
        }
}

void DLG::AddInstance (void)
{
        int i;

        for (i = 0; i < instanz; i ++)
        {
            if (Instances [i] == NULL)
            {
                Instances[i] = this;
                return;
            }
        }
        if (instanz < MAXINSTANCES)
        {
                Instances[i] = this;
                instanz ++;
        }
}


void DLG::DelInstance (void)
{
        int i;

        for (i = 0; i < instanz; i ++)
        {
            if (Instances [i] == this)
            {
                Instances[i] = NULL;
                return;
            }
        }
}

DLG::DLG (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
		TEXTMETRIC tm;
		HFONT hFont, oldfont;
 	    SIZE size;
		HDC hdc;
		int xfull, yfull;

        xorg = x;
        yorg = y;
        cxorg = cx;
        cyorg = cy;
 	    this->Font = &dlgfont;  
        TestMenue = NULL;
        hMenu = NULL;
        tbMain = NULL;
        tbb = NULL;
        hWnd = NULL;
        style = NULL;
        StyleEx = NULL;
        currentfield = -1;
        FieldSet = FALSE;
        this->Pixel = Pixel;

        ClassBackground = GRAYCOL;
        WinBackground = GRAYCOL;
        OldDlg = ActiveDlg;
        ActiveDlg = this;
        AddInstance ();
        fWork = NULL;

		if (Size > 0) 
		{
				StdSize = Size;
        }
//        if (StdSize > 150) StdSize = 150;
        xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

//        if (xfull < 1000) StdSize -= 20;

        dlgfont.FontHeight = StdSize;

  	    this->Caption = NULL;
		if (Caption && Caption[0])
		{
		        this->Caption = Caption; 
		}
		hdc = GetDC (NULL);
        hFont = SetDeviceFont (hdc, Font, &tm);
        oldfont = SelectObject (hdc, hFont);
	    GetTextMetrics (hdc, &tm);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
		DeleteObject (SelectObject (hdc, oldfont));

        CharHeight = tm.tmHeight;
        CharWidth = size.cx;
        this->cx = cx;
        this->cy = cy;
        if (this->cx > 0)
        {
		     if (Pixel == FALSE) this->cx = size.cx * cx;
             cxorg = this->cx;
        }
        else if (Pixel == FALSE)
        {
		     this->cx = size.cx * cx;
             cxorg = this->cx;
        }

        if (this->cy > 0)
        {
		     if (Pixel == FALSE) this->cy = (int) (double) ((double) tm.tmHeight * cy * 1.3);
             cyorg = this->cy;
        }
        else if (Pixel == FALSE)
        {
		     this->cy = size.cy * cy;
             cyorg = this->cy;
        }

        this->x = x;
        if (Pixel == FALSE)
        {
		     this->x = size.cx * x;
        }
        this->y = y;
        if (Pixel == FALSE)
        {
		     this->y = (int) (double) ((double) tm.tmHeight * y * 1.3);
        }
        hWndMain = NULL;
        hWnd = NULL;
        hTrack = NULL;
        vTrack = NULL;
        hScroll = 0;
        vScroll = 0;
        hScrollRange = 0;
        vScrollRange = 0;
        hScrollPage = 0;
        vScrollPage = 0;
        hScWidth = CharWidth;
        vScWidth = CharHeight;
        ScSize = GetSystemMetrics (SM_CYHSCROLL);
        ScVSize = GetSystemMetrics (SM_CXVSCROLL);
        TrackAllways = FALSE;
        TmFont = NULL;
        ShowModus = SW_SHOWNORMAL;
        hbrBackground = NULL;
        Bitmapmode = 1;
        DockProg = NULL;
		TabStops = NULL;
		WithTabStops = TRUE;
		TabStopPos = 0;
		TabStoplen = 0;
        TimerProc = NULL;
}

DLG::~DLG ()
{
	    if (TabStops)
		{
			delete TabStops;
			TabStops = NULL;
		}
        DelInstance ();
        ActiveDlg = OldDlg; 
}


void DLG::SetTmFont (mfont *TmFont)
{
        this->TmFont = TmFont;
}

void DLG::SetCurrentID (DWORD ID)
{
        if (fWork == NULL) return; 
	    if (fWork->SetCurrentID (ID))
        {
		       currentfield = ID;
		}
        FieldSet = TRUE;
}

void DLG::SetCurrentName (char *name)
{
        if (fWork == NULL) return; 
	    if (fWork->SetCurrentName (name))
        {
			   currentfield = fWork->GetID (name);
        }
        FieldSet = TRUE;
}


void DLG::TestScroll (void)
{
         RECT rect;
         int y0, y;

         GetClientRect (hWnd, &rect);
         y0 = CurrentCfield->GetY ();
         y = y0 + fWork->GetStart () + 2 * CharHeight;
         while (y >= rect.bottom)
         {
             SendMessage (hWnd, WM_VSCROLL, SB_LINEDOWN, 0l);
             y = y0 + fWork->GetStart () + 2 * CharHeight;
         }

         y -= 3 * CharHeight;
         while (y < 0)
         {
             SendMessage (hWnd, WM_VSCROLL, SB_LINEUP, 0l);
             y = y0 + fWork->GetStart ();
         }
}

void DLG::SethWndTabStop (HWND hWnd)
{
        int i;

	    if (WithTabStops == FALSE) return;

        for (i = 0; i < TabStoplen; i ++)
        {
            if (hWnd == TabStops[i])
            {
                TabStopPos = i;
                return;
            }
        }
}

void DLG::SetTabFocus (void)
{
       
        if (TabStoplen == 0) return;
	    if (TabStopPos >= TabStoplen)
		{
			TabStopPos = 0;
		}
		else if (TabStopPos < 0)
		{
			TabStopPos = TabStoplen -1;
		}
		SetFocus (TabStops[TabStopPos]);
}
         
void DLG::FocusDown ()
{

	    if (WithTabStops)
		{
			TabStopPos ++;
			SetTabFocus ();
		}
        else
        {
            fWork->NextField (); 
        }
        if (vTrack)
        {
            TestScroll ();
        }
}

void DLG::FocusUp ()
{
	    if (WithTabStops)
		{
			TabStopPos --;
			SetTabFocus ();
		}
        else
        {
            fWork->PriorField (); 
        }
        if (vTrack)
        {
            TestScroll ();
        }
}

int DLG::GetTabStoplen (CFORM *fWork, int len)
{
	    int i;
		CFORM *Cform;

		for (i = 0; i < fWork->GetFieldanz (); i ++)
		{
          	if (fWork->GetCfield ()[i]->GetAttribut () == CFFORM)
			{
		           Cform = (CFORM *) fWork->GetCfield ()[i]->GetFeld ();
				   len = GetTabStoplen (Cform, len);
				   continue;
			}
			if (fWork->GetCfield ()[i]->GetTabstop ())
			{
				    len ++;
			}
		}
		return len;
}

void DLG::AddTabStops (CFORM *fWork)
{
	    int i;
		CFORM *Cform;
		HWND hWnd;

		for (i = 0; i < fWork->GetFieldanz (); i ++)
		{
          	if (fWork->GetCfield ()[i]->GetAttribut () == CFFORM)
			{
		           Cform = (CFORM *) fWork->GetCfield ()[i]->GetFeld ();
				   AddTabStops (Cform);
				   continue;
			}
			if (fWork->GetCfield ()[i]->GetTabstop ())
			{
  			       hWnd = fWork->GetCfield ()[i]->GethWnd ();
				   if (hWnd)
				   {
					   TabStops [TabStoplen] = hWnd;
					   TabStoplen ++;
				   }
			}
		}
}

void DLG::UpdateTabstop (HWND hWnd)
{
        int i;

        if (TabStops == NULL) return;

        for (i = 0; i < TabStoplen; i ++)
        {
            if (TabStops[i] == hWnd)
            {
                TabStopPos = i;
                return;
            }
        }
}

void DLG::SetTabStops (CFORM *fWork)
{
		int len;
		len = GetTabStoplen (fWork, 0);
		if (len <= 0) return;
        TabStops = new HWND [len];
		if (TabStops == NULL) return;
		TabStoplen = 0;
		AddTabStops (fWork);
}

void DLG::DestroyTabStops (void)
{

		if (TabStops)
		{
			delete TabStops;
			TabStops = NULL;
			TabStoplen = 0;
		}
}

void DLG::NewTabStops (CFORM *fWork)
{
	    DestroyTabStops ();
		SetTabStops (fWork);
}


void DLG::ProcessMessages (void)
{
	    MSG msg;

		if (fWork)
		{
			SetTabStops (fWork);
		}

		if (WithTabStops && TabStops != NULL)
		{
			SetTabFocus ();
		}
        else if (fWork && FieldSet == FALSE)
        {
            fWork->SetFirstFocus ();
        }
		else if (fWork && currentfield != -1)
        {
              fWork->SetCurrentID (currentfield);
        }
        while (GetMessage (&msg, NULL, 0, 0))
  	    {
			  if (msg.message == WM_KEYDOWN)
			  {
                   
                  MousePressed = FALSE; 
		 	      if (msg.wParam == VK_TAB)
                  {

                          if (OnKeyTab ()) continue;
                          if (GetKeyState (VK_SHIFT) < 0)
                          {
                                 syskey = KEYSTAB;
                                 FocusUp ();
	       	              }
			              else
			              {
			                     syskey = KEYTAB;
                                 FocusDown ();
                          }
    			          continue;
                  }
			      if (msg.wParam == KEYSTAB)
                  {

                          syskey = KEYSTAB;
                          FocusUp ();
    			          continue;
				 }
				 if (msg.wParam == VK_F1)
				 {
					  if (OnKey (VK_F1)) continue;
				 }
				 else if (msg.wParam == VK_F2)
				 {
					  if (OnKey (VK_F2)) continue;
				 }
				 else if (msg.wParam == VK_F3)
				 {
					  if (OnKey (VK_F3)) continue;
				 }
				 else if (msg.wParam == VK_F4)
				 {
					  if (OnKey (VK_F4)) continue;
				 }
				 else if (msg.wParam == VK_F5)
				 {
					  if (OnKey (VK_F5)) continue;
				 }
				 else if (msg.wParam == VK_F6)
				 {
					  if (OnKey (VK_F6)) continue;
				 }
				 else if (msg.wParam == VK_F7)
				 {
					  if (OnKey (VK_F7)) continue;
				 }
				 else if (msg.wParam == VK_F8)
				 {
					  if (OnKey (VK_F8)) continue;
				 }
				 else if (msg.wParam == VK_F9)
				 {
					  if (OnKey (VK_F9)) continue;
				 }
				 else if (msg.wParam == VK_F10)
				 {
					  if (OnKey (VK_F10)) continue;
				 }
				 else if (msg.wParam == VK_F11)
				 {
					  if (OnKey (VK_F11)) continue;
				 }
				 else if (msg.wParam == VK_F12)
				 {
					  if (OnKey (VK_F12)) continue;
				 }
				 else if (msg.wParam == VK_DELETE)
				 {
					  if (OnKey (VK_DELETE)) continue;
				 }
				 else if (msg.wParam == VK_INSERT)
				 {
					  if (OnKey (VK_INSERT)) continue;
				 }
				 else if (msg.wParam == VK_ESCAPE)
				 {
					  if (OnKey (VK_ESCAPE)) continue;
				 }
				 else if (msg.wParam == VK_DOWN)
				 {
                      if (OnKeyDown ());
                      else if (fWork->GetAttribut () == CCOMBOBOX);
                      else if (fWork->GetAttribut () == CLISTBOX);
                      else
                      {
      				       syskey = KEYDOWN;
                           FocusDown ();
                           continue;
                      }

				 }
				 else if (msg.wParam == VK_UP)
				 {

                      if (OnKeyUp ());
                      else if (fWork->GetAttribut () == CCOMBOBOX);
                      else if (fWork->GetAttribut () == CLISTBOX);
                      else
                      {
					      syskey = KEYUP;
                          FocusUp ();
					      continue;
                      }
				 }
				 else if (msg.wParam == VK_NEXT)
				 {

					  OnKey (VK_NEXT);
					  continue;
				 }
				 else if (msg.wParam == VK_PRIOR)
				 {

					  OnKey (VK_PRIOR);
					  continue;
                 }
				 else if (msg.wParam == VK_RETURN)
				 {

                      if (OnKey (VK_RETURN)) continue;
                      syskey = KEYCR;
                      FocusDown ();
					  continue;
				  }
                  else
                  {
                      if (OnKey (NULL)) continue;
                  }
			  }
			  else if (msg.message == WM_SYSKEYDOWN)
			  {
                 MousePressed = FALSE; 
				 if (msg.wParam == VK_F10)
				 {
					  if (OnKey (VK_F10)) continue;
				 }
              }
			  else if (msg.message == WM_COMMAND)
			  {
			  }
              else if (msg.message == WM_LBUTTONDOWN)
              {
                       MousePressed = TRUE; 
                       if (OnLButtonDown (msg.hwnd, msg.message, msg.wParam, msg.lParam))
                       {
                           continue;
                       }
              }  
              else if (msg.message == WM_RBUTTONDOWN)
              {
                       MousePressed = TRUE; 
                       if (OnRButtonDown (msg.hwnd, msg.message, msg.wParam, msg.lParam))
                       {
                           continue;
                       }
              }  
              else if (msg.message == WM_LBUTTONUP)
              {
                       MousePressed = TRUE; 
                       if (OnLButtonUp (msg.hwnd, msg.message, msg.wParam, msg.lParam))
                       {
                           continue;
                       }
              }  
              else if (msg.message == WM_RBUTTONUP)
              {
                       MousePressed = TRUE; 
                       if (OnRButtonUp (msg.hwnd, msg.message, msg.wParam, msg.lParam))
                       {
                           continue;
                       }
              }  
              else if (msg.message == WM_MOUSEMOVE)
              {
                       MousePressed = TRUE; 
                       if (OnMouseMove (msg.hwnd, msg.message, msg.wParam, msg.lParam))
                       {
                           continue;
                       }
              }  
              TranslateMessage(&msg);
 	   	      DispatchMessage(&msg);
 	  }
}

BOOL DLG::OnKey1 (void)
{
        return FALSE;
}

BOOL DLG::OnKey2 (void)
{
        return FALSE;
}

BOOL DLG::OnKey3 (void)
{
        return FALSE;
}

BOOL DLG::OnKey4 (void)
{
        return FALSE;
}

BOOL DLG::OnKey5 (void)
{
        return FALSE;
}

BOOL DLG::OnKey6 (void)
{
        return FALSE;
}

BOOL DLG::OnKey7 (void)
{
        return FALSE;
}

BOOL DLG::OnKey8 (void)
{
        return FALSE;
}

BOOL DLG::OnKey9 (void)
{
        return FALSE;
}

BOOL DLG::OnKey10 (void)
{
        return FALSE;
}

BOOL DLG::OnKey11 (void)
{
        return FALSE;
}

BOOL DLG::OnKey12 (void)
{
        return FALSE;
}

BOOL DLG::OnKeyDown (void)
{
        return FALSE;
}

BOOL DLG::OnKeyUp (void)
{
        return FALSE;
}

BOOL DLG::OnKeyPrior (void)
{
        return FALSE;
}

BOOL DLG::OnKeyNext (void)
{
        return FALSE;
}

BOOL DLG::OnKeyHome (void)
{
        return FALSE;
}

BOOL DLG::OnKeyEnd (void)
{
        return FALSE;
}

BOOL DLG::OnKeyReturn (void)
{
        return FALSE;
}

BOOL DLG::OnKeyTab (void)
{
        return FALSE;
}

BOOL DLG::OnKeyEscape (void)
{
        return FALSE;
}


BOOL DLG::OnKeyDelete (void)
{
        return FALSE;
}

BOOL DLG::OnKeyInsert (void)
{
        return FALSE;
}


BOOL DLG::OnKey (int Key)
{
        switch (Key)
        {
            case VK_F1 :
                if (OnKey1 ()) return TRUE;
                break;
            case VK_F2 :
                if (OnKey2 ()) return TRUE;
                break;
            case VK_F3:
                if (OnKey3 ()) return TRUE;
                break;
            case VK_F4 :
                if (OnKey4 ()) return TRUE;
                break;
            case VK_F5 :
                if (OnKey5 ()) return TRUE;
                break;
            case VK_F6 :
                if (OnKey6 ()) return TRUE;
                break;
            case VK_F7 :
                if (OnKey7 ()) return TRUE;
                break;
            case VK_F8 :
                if (OnKey8 ()) return TRUE;
                break;
            case VK_F9 :
                if (OnKey9 ()) return TRUE;
                break;
            case VK_F10 :
                if (OnKey10 ()) return TRUE;
                break;
            case VK_F11 :
                if (OnKey11 ()) return TRUE;
                break;
            case VK_F12 :
                if (OnKey12 ()) return TRUE;
                break;
            case VK_PRIOR :
                if (OnKeyPrior ()) return TRUE;
                break;
            case VK_NEXT :
                if (OnKeyNext ()) return TRUE;
                break;
            case VK_HOME :
                if (OnKeyHome ()) return TRUE;
                break;
            case VK_END :
                if (OnKeyEnd ()) return TRUE;
                break;
            case VK_RETURN :
                if (OnKeyReturn ()) return TRUE;
                break;
            case VK_ESCAPE :
                if (OnKeyEscape ()) return TRUE;
                break;
            case VK_DELETE :
                if (OnKeyDelete ()) return TRUE;
                break;
            case VK_INSERT :
                if (OnKeyInsert ()) return TRUE;
                break;
        }
        return FALSE;
//        return (BOOL) SendMessage (hWnd, WM_COMMAND, (WPARAM) Key, 0l); 
}

BOOL DLG::OnLButtonDown (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        return FALSE;
}
        
BOOL DLG::OnRButtonDown (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        return FALSE;
}
        
BOOL DLG::OnLButtonUp (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        return FALSE;
}
        
BOOL DLG::OnRButtonUp (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        return FALSE;
}
        
BOOL DLG::OnMouseMove (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        return FALSE;
}
        
BOOL DLG::OnSetFocus (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        CFIELD *aktfield;
        static BOOL InSetFocus = FALSE;

        if (InSetFocus) return FALSE;
        if (fWork == NULL) return FALSE;

        aktfield = fWork->GetCfield ((DWORD) LOWORD (wParam));
        SethWndTabStop ((HWND) lParam);
        if (aktfield != NULL)
        {
            InSetFocus = TRUE;
            fWork->SetCurrentFieldID (aktfield->GetID ());
            aktfield->SetText ();
            CurrentCfield = aktfield;
            aktfield->before ();
            InSetFocus = FALSE;
  	        if (aktfield->GetAttribut () == CEDIT && 
                (aktfield->GetBkMode () & ES_MULTILINE) == 0)
            {
                 PostMessage (aktfield->GethWnd (), EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
            }
            return TRUE;
        }
        return FALSE;
}

BOOL DLG::OnKillFocus (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        CFIELD *aktfield;
        static BOOL InKillFocus = FALSE;

        if (InKillFocus) return FALSE;
        if (fWork == NULL) return FALSE;

        aktfield = fWork->GetCfield ((DWORD) LOWORD (wParam));
        if (aktfield != NULL)
        {
            InKillFocus = TRUE;
            aktfield->GetText ();
            CurrentCfield = aktfield;
            aktfield->after ();
            InKillFocus = FALSE;
            return FALSE;
        }
        return FALSE;
}


BOOL DLG::OnChildSetFocus (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        return FALSE;
}

BOOL DLG::OnChildKillFocus (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        return FALSE;
}

BOOL DLG::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             fWork->display (hWnd, hdc);
             if (OldDlg != NULL)
             {
		            OldDlg->OnPaint (hWnd, hdc, msg, wParam, lParam);
             }
             return TRUE;
        }
        if (OldDlg != NULL)
        {
             OldDlg->OnPaint (hWnd, hdc, msg, wParam, lParam);
        }
        return FALSE;
}

BOOL DLG::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (TestMenue != NULL)
        {
            if ((*TestMenue) (hWnd, msg, wParam,lParam))
            {
                return TRUE;
            }
        }

        if (LOWORD (wParam) == VK_F1)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_F1, 0l);
             return TRUE;
        }

        if (LOWORD (wParam) == VK_F2)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_F2, 0l);
             return TRUE;
        }

        if (LOWORD (wParam) == VK_F3)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_F3, 0l);
             return TRUE;
        }

        else if (LOWORD (wParam) == VK_F4)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_F4, 0l);
             return FALSE;
        }

        else if (LOWORD (wParam) == VK_F5)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_F5, 0l);
             return TRUE;
        }
        else if (LOWORD (wParam) == VK_F6)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_F6, 0l);
             return TRUE;
        }
        else if (LOWORD (wParam) == VK_F7)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_F7, 0l);
             return TRUE;
        }
        else if (LOWORD (wParam) == VK_F8)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_F8, 0l);
             return TRUE;
        }
        else if (LOWORD (wParam) == VK_F9)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_F9, 0l);
             return TRUE;
        }
        else if (LOWORD (wParam) == VK_F10)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_F10, 0l);
             return TRUE;
        }
        else if (LOWORD (wParam) == VK_PRIOR)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_PRIOR, 0l);
             return TRUE;
        }
        else if (LOWORD (wParam) == VK_NEXT)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_NEXT, 0l);
             return TRUE;
        }
        else if (LOWORD (wParam) == VK_HOME)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_HOME, 0l);
             return TRUE;
        }
        else if (LOWORD (wParam) == VK_END)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_END, 0l);
             return TRUE;
        }
        else if (LOWORD (wParam) == VK_RETURN)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_RETURN, 0l);
             return TRUE;
        }
        else if (LOWORD (wParam) == KEYDOWN)
        {
            PostMessage (NULL, WM_KEYDOWN, VK_DOWN, 0l);
        }
        else if (LOWORD (wParam) == KEYUP)
        {
            PostMessage (NULL, WM_KEYDOWN, VK_UP, 0l);
        }
        else if (LOWORD (wParam) == KEYTAB)
        {
            PostMessage (NULL, WM_KEYDOWN, VK_TAB, 0l);
        }
        else if (LOWORD (wParam) == KEYSTAB)
        {
            PostMessage (NULL, WM_KEYDOWN, KEYSTAB, 0l);
        }
        else if (LOWORD (wParam) == VK_DELETE)
        {
             PostMessage (NULL, WM_KEYDOWN, VK_DELETE, 0l);
             return TRUE;
        }
        return FALSE;
}

BOOL DLG::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        return FALSE;
}

BOOL DLG::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        PostQuitMessage (0);
        return TRUE;
}


BOOL DLG::OnNotify (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (tbb)
        {
             LPNMHDR pnmh = (LPNMHDR) lParam;

             if (pnmh->code == TTN_NEEDTEXT)
             {
                      LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
                      if (QuickCpy (lpttt->szText, lpttt->hdr.idFrom)
                               == FALSE)
                      {
                                QuickHwndCpy (lpttt);
                      }
              }
              return TRUE;
        }
        return FALSE;
}

void DLG::OnHScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
	     RECT rect;
		 int PageScroll;
		 int ScrollPos;

         fWork->SetNewPos (FALSE);
         switch (LOWORD (wParam))
         {

             case SB_LINEDOWN :
             { 
				     if (hScroll < hScrollRange)
					 {

				             hScroll += 1;
                             fWork->xStartMinus (hScWidth);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, -hScWidth, 0, NULL, NULL);
                             SetScrollPos (hTrack, SB_CTL, hScroll,
                                           TRUE);
//					         fWork->Move ();

					 }
                     break;
             }

             case SB_LINEUP :
             {
				     if (hScroll > 0) 
					 {
						     hScroll -= 1;
                             fWork->xStartPlus (hScWidth);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, hScWidth, 0, NULL, NULL);
                             SetScrollPos (hTrack, SB_CTL, hScroll,
                                            TRUE);
//					         fWork->Move ();
					 }
                     break;
             }

             case SB_PAGEDOWN :
             {    
				     if (hScroll < hScrollRange)
					 {
				             hScroll += hScrollPage;
							 PageScroll = hScrollPage * hScWidth;
							 if (hScroll > hScrollRange)
							 {
								 PageScroll -= ((hScroll - hScrollRange) * hScWidth); 
								 hScroll     = hScrollRange; 
							 }
                             fWork->xStartMinus (PageScroll);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, -PageScroll, 0, NULL, NULL);
                             SetScrollPos (hTrack, SB_CTL, hScroll,
                                           TRUE);
//					         fWork->Move ();
					 }
                     break;
             }

             case SB_PAGEUP :
             {    
				     if (hScroll > 0)
					 {
				             hScroll -= hScrollPage;
							 PageScroll = hScrollPage * hScWidth;
							 if (hScroll < 0)
							 {
								 PageScroll += (hScroll * hScWidth); 
								 hScroll = 0;
							 }
                             fWork->xStartPlus (PageScroll);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, PageScroll, 0, NULL, NULL);
                             SetScrollPos (hTrack, SB_CTL, hScroll,
                                           TRUE);
//					         fWork->Move ();
					 }
                     break;
             }

             case SB_THUMBTRACK :
             case SB_THUMBPOSITION :
                    ScrollPos = HIWORD (wParam);
                    if (ScrollPos == hScroll) break;
					PageScroll = (ScrollPos - hScroll) * hScWidth;
					hScroll = ScrollPos;
					if (PageScroll < 0)
					{
                             fWork->xStartPlus (PageScroll * -1);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, -PageScroll, 0, NULL, NULL);
					}
					else
					{
                             fWork->xStartMinus (PageScroll);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, -PageScroll, 0, NULL, NULL);
					}
                    SetScrollPos (hTrack, SB_CTL, hScroll,
                                  TRUE);
//		            fWork->Move ();
                    break;

             case SB_TOP :
				     if (hScroll)
					 {
		                     ScrollWindow (hWnd, -hScroll, 0, NULL, NULL);
                             hScroll = 0;
					         fWork->xSetStart (0);
                             SetScrollPos (hTrack, SB_CTL, hScroll,
                                           TRUE);
//		                     fWork->Move ();
					 }
                     break;
             case SB_BOTTOM :
				     if (hScroll < hScrollRange)
					 {
		                     ScrollWindow (hWnd,hScrollRange - hScroll, 0, NULL, NULL);
							 hScroll = hScrollRange;
					         fWork->xSetStart (hScrollRange * hScWidth);
                             SetScrollPos (hTrack, SB_CTL, hScroll,
                                           TRUE);
//		                     fWork->Move ();
					 }
                     break;

         }
         UpdateWindow (hWnd);
         fWork->SetNewPos (TRUE);
}

void DLG::OnVScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
	     RECT rect;
		 int PageScroll;
		 int ScrollPos;

         fWork->SetNewPos (FALSE);
         switch (LOWORD (wParam))
         {

             case SB_LINEDOWN :
             { 
				     if (vScroll < vScrollRange)
					 {

				             vScroll += 1;
                             fWork->StartMinus (vScWidth);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, 0, -vScWidth, NULL, NULL);
                             rect.top -= vScWidth;
                             SetScrollPos (vTrack, SB_CTL, vScroll,
                                           TRUE);

//					         fWork->Move ();

					 }
                     break;
             }

             case SB_LINEUP :
             {
				     if (vScroll > 0) 
					 {
						     vScroll -= 1;
                             fWork->StartPlus (vScWidth);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, 0, vScWidth, NULL, NULL);
                             SetScrollPos (vTrack, SB_CTL, vScroll,
                                            TRUE);
//					         fWork->Move ();
					 }
                     break;
             }

             case SB_PAGEDOWN :
             {    
				     if (vScroll < vScrollRange)
					 {
				             vScroll += vScrollPage;
							 PageScroll = vScrollPage * vScWidth;
							 if (vScroll > vScrollRange)
							 {
								 PageScroll -= ((vScroll - vScrollRange) * vScWidth); 
								 vScroll     = vScrollRange; 
							 }
                             fWork->StartMinus (PageScroll);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, 0, -PageScroll, NULL, NULL);
                             SetScrollPos (vTrack, SB_CTL, vScroll,
                                           TRUE);
//					         fWork->Move ();
					 }
                     break;
             }

             case SB_PAGEUP :
             {    
				     if (vScroll > 0)
					 {
				             vScroll -= vScrollPage;
							 PageScroll = vScrollPage * vScWidth;
							 if (vScroll < 0)
							 {
								 PageScroll += (vScroll * vScWidth); 
								 vScroll = 0;
							 }
                             fWork->StartPlus (PageScroll);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, 0, PageScroll, NULL, NULL);
                             SetScrollPos (vTrack, SB_CTL, vScroll,
                                           TRUE);
//					         fWork->Move ();
					 }
                     break;
             }

             case SB_THUMBTRACK :
             case SB_THUMBPOSITION :
                    ScrollPos = HIWORD (wParam);
                    if (ScrollPos == vScroll) break;
					PageScroll = (ScrollPos - vScroll) * vScWidth;
					vScroll = ScrollPos;
					if (PageScroll < 0)
					{
                             fWork->StartPlus (PageScroll * -1);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, 0, -PageScroll, NULL, NULL);
					}
					else
					{
                             fWork->StartMinus (PageScroll);
		                     GetClientRect (hWnd, &rect);
		                     ScrollWindow (hWnd, 0, -PageScroll, NULL, NULL);
					}
                    SetScrollPos (vTrack, SB_CTL, vScroll,
                                  TRUE);
//		            fWork->Move ();
                    break;

             case SB_TOP :
				     if (vScroll)
					 {
		                     ScrollWindow (hWnd, 0, -vScroll, NULL, NULL);
                             vScroll = 0;
					         fWork->SetStart (0);
                             SetScrollPos (vTrack, SB_CTL, vScroll,
                                           TRUE);
//		                     fWork->Move ();
					 }
                     break;
             case SB_BOTTOM :
				     if (vScroll < vScrollRange)
					 {
		                     ScrollWindow (hWnd,0, vScrollRange - vScroll, NULL, NULL);
							 vScroll = vScrollRange;
					         fWork->SetStart (vScrollRange * vScWidth);
                             SetScrollPos (vTrack, SB_CTL, vScroll,
                                           TRUE);
//		                     fWork->Move ();
					 }
                     break;

         }
         UpdateWindow (hWnd);

}

BOOL DLG::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (this->hWnd == hWnd)
        {
            if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
            {
	          if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
            }
            return FALSE;
        }
        if (OldDlg != NULL)
        {
            return OldDlg->OnActivate (hWnd, msg, wParam, lParam);
        }
        return FALSE;
}

BOOL DLG::OnUser (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (OldDlg != NULL)
        {
		            OldDlg->OnUser (hWnd, msg, wParam, lParam);
        }
        return FALSE;
}


void DLG::Enable (CFORM *cForm, char **Fields, BOOL mode)
{
        int i;
        
        for (i = 0; Fields[i]; i ++)
        {
            cForm->GetCfield (Fields[i])->Enable (mode);
        }
        if (ActiveDlg->GetWithTabStops ())
        {
            ActiveDlg->NewTabStops (cForm);
        }


}

BOOL DLG::OnTimer (HWND hWnd, UINT msg,WPARAM wParam,LPARAM lParam)
{
        return FALSE;
}

void DLG::ToForm (FORMFIELD **Fields)
{
        int i;
        
        for (i = 0; Fields[i]; i ++)
        {
            Fields[i]->ToForm ();
        }
}

void DLG::FromForm (FORMFIELD **Fields)
{
        int i;
        
        for (i = 0; Fields[i]; i ++)
        {
            Fields[i]->FromForm ();
        }
}



CALLBACK DLG::CProc (HWND hWnd,UINT msg,
                          WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
  	    HDC hdc;

        if (ActiveDlg)
        {
            switch(msg)
            {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
		            ActiveDlg->OnPaint (hWnd, hdc, msg, wParam, lParam);
                    EndPaint (hWnd, &ps);
		 	        break;
              case WM_MOVE :
                    if (ActiveDlg->OnMove (hWnd, msg, wParam, lParam)) return TRUE;
                    break;
              case WM_SIZE :
                    if (ActiveDlg->OnMove (hWnd, msg, wParam, lParam)) return TRUE;
                    break;
              case WM_COMMAND :
				    if (ActiveDlg->OnCommand (hWnd, msg, wParam, lParam)) return TRUE;
                    break; 
              case WM_SYSCOMMAND :
                    if (ActiveDlg->OnSysCommand (hWnd, msg, wParam, lParam)) return TRUE;
                    break; 
              case WM_SETFOCUS :
                    if (ActiveDlg->OnSetFocus (hWnd, msg, wParam, lParam)) return TRUE;
                    break; 
              case WM_KILLFOCUS :
                    if (ActiveDlg->OnKillFocus (hWnd, msg, wParam, lParam)) return TRUE;
                    break; 
              case WM_NOTIFY :
                    if (ActiveDlg->OnNotify (hWnd, msg, wParam, lParam)) return TRUE;
                    break; 
              case WM_HSCROLL :
                         ActiveDlg->OnHScroll (wParam, lParam);
						 return TRUE;
              case WM_VSCROLL :
                         ActiveDlg->OnVScroll (wParam, lParam);
						 return TRUE;
              case WM_LBUTTONDOWN :
                         if (ActiveDlg->OnLButtonDown (hWnd, msg, wParam, lParam)) return TRUE;
                         break;
              case WM_ACTIVATE :
                         if (ActiveDlg->OnActivate (hWnd, msg, wParam, lParam)) return TRUE;
                         break;
              case WM_DESTROY :
                    ActiveDlg->OnDestroy (hWnd, msg, wParam, lParam);
                    break; 
              case WM_TIMER :
                         if (ActiveDlg->OnTimer (hWnd, msg, wParam, lParam)) return TRUE;
						 return TRUE;
              case WM_USER :
                    if (ActiveDlg->OnUser (hWnd, msg, wParam, lParam)) return TRUE;
                    break; 
            }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void DLG::DestroyWindow (void)
{
	       fWork->destroy ();
           if (hWndMain)
           {
 		        ::DestroyWindow (hWndMain);
           }
		   ::DestroyWindow (hWnd);
}

BOOL DLG::GetWindowClass (COLORREF col, char **colname)
{
    int i;

    for (i = 0; i < colwanz; i ++)
    {
        if (col == ColWindows[i].col)
        {
            *colname = ColWindows[i].colname; 
            return TRUE;
        }
    }
    if (colwanz < MAXCOLS)
    {
        sprintf (ColWindows[i].colname, "DlgWin%d", i);
        *colname = ColWindows[i].colname; 
        colwanz ++;
        return FALSE;
    }
    *colname = ColWindows[0].colname; 
    return TRUE;
}


void DLG::MoveWindow (int mode, int x, int y, int cx, int cy)
{
		  RECT rect, rect1;
		  int xfull, yfull;


          SetLocation (x, y);
          SetDimension (cx, cy);
          xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
          yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

          if (mode == 0)
		  {
			      if (this->x < 0)
                  {
		                 this->x  = max (0, (xfull - cx) / 2);
                  }
			      if (this->y < 0)
                  {
		                 this->y  = max (0, (yfull - cy) / 2);
                  }
		  }
          else if (hMainWindow && ((style & WS_CHILD) == 0))
          {
          	      GetClientRect (hMainWindow, &rect);
		          GetWindowRect (hMainWindow, &rect1);
                  if (this->x < 0)
                  {
 		              this->x  = max (0, (rect.right - cx) / 2)  + rect1.left;
                  }
			      if (this->y < 0)
                  {
 		               this->y  = max (0, (rect.bottom - cy) / 2) + rect1.top;
                  }
                  if (cx <= 0)
                  {
                      cx = rect.right + cx + rect1.left - x;
                  }
                  if (cy <= 0)
                  {
                      cy = rect.bottom + cy + rect1.top - y;
                  }
		  }
          else if (hMainWindow)
          {
          	      GetClientRect (hMainWindow, &rect);
                  if (this->x < 0)
                  {
		                  this->x  = max (0, (rect.right - cx) / 2);
                  }

			      if (this->y < 0)
                  {
         		          this->y  = max (0, (rect.bottom - cy) / 2);
                  }
                  if (cx <= 0)
                  {
                      cx = rect.right + cx - x;
                  }
                  if (cy <= 0)
                  {
                      cy = rect.bottom + cy - y;
                  }
		  }
		  else
		  {
			      if (this->x < 0)
                  {
		                 this->x  = max (0, (xfull - this->cx) / 2);
                  }
			      if (this->y < 0)
                  {
		                 this->y  = max (0, (yfull - this->cy) / 2);
                  }
		  }
          switch (mode)
          {
          case 0 :
                  ::MoveWindow (hMainWindow, this->x, this->y, this->cx, this->cy, TRUE);
                  UpdateWindow (hMainWindow);
                  break;
          case 1 :
                  ::MoveWindow (hWnd, this->x, this->y, this->cx, this->cy, TRUE);
                  UpdateWindow (hWnd);
                  break;
          }
}
						
HWND DLG::OpenWindow (HANDLE hInstance, HWND hMainWindow)
{
		  static BOOL registered = FALSE;
          HDC hdc;
          WNDCLASS wc;
		  RECT rect, rect1;
		  int xfull, yfull;
          char *regname;
          HFONT hFont, oldfont;
          TEXTMETRIC tm;
          SIZE size;


		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;
 		  SetColBorderM (TRUE);
          xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
          yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

		  if (GetWindowClass (ClassBackground, &regname) == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC |
                              CS_VREDRAW | CS_HREDRAW;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "MAINICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  if (hbrBackground)
                  {
 
                           wc.hbrBackground =  hbrBackground;
                  }
                  else
                  {
                           wc.hbrBackground =   CreateSolidBrush (ClassBackground);
                  }
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  regname;
                  RegisterClass(&wc);

  		          registered = TRUE;
		  }

          if (hMainWindow && ((style & WS_CHILD) == 0))
          {
          	      GetClientRect (hMainWindow, &rect);
		          GetWindowRect (hMainWindow, &rect1);
                  if (this->x < 0)
                  {
 		              this->x  = max (0, (rect.right - cx) / 2)  + rect1.left;
                  }
			      if (this->y < 0)
                  {
 		               this->y  = max (0, (rect.bottom - cy) / 2) + rect1.top;
                  }
                  if (cx <= 0)
                  {
                      cx = rect.right + cx + rect1.left - x;
                  }
                  if (cy <= 0)
                  {
                      cy = rect.bottom + cy + rect1.top - y;
                  }
		  }
          else if (hMainWindow)
          {
          	      GetClientRect (hMainWindow, &rect);
                  if (this->x < 0)
                  {
		                  this->x  = max (0, (rect.right - cx) / 2);
                  }

			      if (this->y < 0)
                  {
         		          this->y  = max (0, (rect.bottom - cy) / 2);
                  }
                  if (cx <= 0)
                  {
                      cx = rect.right + cx - x;
                  }
                  if (cy <= 0)
                  {
                      cy = rect.bottom + cy - y;
                  }
		  }
		  else
		  {
			      if (this->x < 0)
                  {
		                 this->x  = max (0, (xfull - cx) / 2);
                  }
			      if (this->y < 0)
                  {
		                 this->y  = max (0, (yfull - cy) / 2);
                  }
		  }

          if (style == 0l)
          {
		      if (Caption)
              {
			       style = WS_VISIBLE | WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU
				      | WS_MINIMIZEBOX;
              }
		      else
              {
			       style = WS_VISIBLE | WS_POPUP | WS_DLGFRAME;
              }
          }

          hWnd = CreateWindowEx (StyleEx, 
                                  regname,
                                  Caption,
                                  style,
                                  x, y,
                                  cx, cy,
                                  hMainWindow,
                                  NULL,
                                  hInstance,
                                  NULL);

          if ((WinBackground != ClassBackground) && hbrBackground == NULL)
          {
              SetClassLong (hWnd, GCL_HBRBACKGROUND, 
                                  (long) CreateSolidBrush (WinBackground));
			  InvalidateRect (hWnd, NULL, TRUE);
          }

          if (hbrBackground && Bitmapmode == 2)
          {
              GetClientRect (hWnd, &rect);
              hdc = GetDC (hWnd);
              StrechBitmapMem (hdc, rect.right, rect.bottom);
              ReleaseDC (hWnd, hdc);
              hbrBackground =  CreatePatternBrush (hBitmap);
              SetClassLong (hWnd, GCL_HBRBACKGROUND, 
                                  (long) hbrBackground);
			  InvalidateRect (hWnd, NULL, TRUE);
          }               


          if (hMenu)
          {
                 SetMenu (hWnd, hMenu);
          }
          if (tbb)
          {
                  if (tbMain == NULL)
                  {
                      tbMain = hWnd;
                  }
                  hwndTB = MakeToolBarEx (hInstance, 
		                       tbMain,tbb, tblen,
                               qInfo, qIdfrom,
		                       qhWndInfo, qhWndFrom);
          }

		  ShowWindow (hWnd, ShowModus);
		  UpdateWindow (hWnd);
          hdc = GetDC (hWnd);
          if (TmFont)
          {
                   hFont = SetDeviceFont (hdc, TmFont, &tm);
                   oldfont = SelectObject (hdc, hFont);
	               GetTextMetrics (hdc, &tm);
		           GetTextExtentPoint32 (hdc, "X", 1, &size);
		           DeleteObject (SelectObject (hdc, oldfont));
                   CharHeight = tm.tmHeight;
                   CharWidth  = size.cx;
          }
          ReleaseDC (hWnd, hdc);
		  return hWnd;
}

HWND DLG::OpenScrollWindow (HANDLE hInstance, HWND hMainWindow)
{
		  static BOOL registered = FALSE;
          HDC hdc;
          WNDCLASS wc;
		  RECT rect, rect1;
		  int xfull, yfull;
          char *regname;
          HFONT hFont, oldfont;
          TEXTMETRIC tm;
          SIZE size;


		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;
 		  SetColBorderM (TRUE);
          xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
          yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

		  if (GetWindowClass (ClassBackground, &regname) == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC |
                              CS_VREDRAW | CS_HREDRAW;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "BIWAKICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  if (hbrBackground)
                  {
 
                           wc.hbrBackground =  hbrBackground;
                  }
                  else
                  {
                           wc.hbrBackground =   CreateSolidBrush (ClassBackground);
                  }
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  regname;
                  RegisterClass(&wc);

  		          registered = TRUE;
		  }

          if (hMainWindow && ((style & WS_CHILD) == 0))
          {
          	      GetClientRect (hMainWindow, &rect);
		          GetWindowRect (hMainWindow, &rect1);
                  if (this->x < 0)
                  {
 		              this->x  = max (0, (rect.right - cx) / 2)  + rect1.left;
                  }
			      if (this->y < 0)
                  {
 		               this->y  = max (0, (rect.bottom - cy) / 2) + rect1.top;
                  }
                  if (cx <= 0)
                  {
                      cx = rect.right + cx + rect1.left - x;
                  }
                  if (cy <= 0)
                  {
                      cy = rect.bottom + cy + rect1.top - y;
                  }
		  }
          else if (hMainWindow)
          {
          	      GetClientRect (hMainWindow, &rect);
                  if (this->x < 0)
                  {
		                  this->x  = max (0, (rect.right - cx) / 2);
                  }

			      if (this->y < 0)
                  {
         		          this->y  = max (0, (rect.bottom - cy) / 2);
                  }
                  if (cx <= 0)
                  {
                      cx = rect.right + cx - x;
                  }
                  if (cy <= 0)
                  {
                      cy = rect.bottom + cy - y;
                  }
		  }
		  else
		  {
			      if (this->x < 0)
                  {
		                 this->x  = max (0, (xfull - cx) / 2);
                  }
			      if (this->y < 0)
                  {
		                 this->y  = max (0, (yfull - cy) / 2);
                  }
		  }

          if (style == 0l)
          {
		      if (Caption)
              {
			       style = WS_VISIBLE | WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU
				      | WS_MINIMIZEBOX;
              }
		      else
              {
			       style = WS_VISIBLE | WS_POPUP | WS_DLGFRAME;
              }
          }

          hWndMain = CreateWindowEx (StyleEx, 
                                  regname,
                                  Caption,
                                  style,
                                  x, y,
                                  cx, cy,
                                  hMainWindow,
                                  NULL,
                                  hInstance,
                                  NULL);

          if (WinBackground != ClassBackground)
          {
              SetClassLong (hWndMain, GCL_HBRBACKGROUND, 
                                  (long) CreateSolidBrush (WinBackground));
          }

		  ShowWindow (hWndMain, ShowModus);

          GetClientRect (hWndMain, &rect);
          cx = rect.right;
          cy = rect.bottom;
          hWnd = CreateWindowEx (0, 
                                  regname,
                                  Caption,
                                  WS_CHILD,
                                  0, 0,
                                  cx, cy,
                                  hWndMain,
                                  NULL,
                                  hInstance,
                                  NULL);

          if ((WinBackground != ClassBackground) && hbrBackground == NULL)
          {
              SetClassLong (hWnd, GCL_HBRBACKGROUND, 
                                  (long) CreateSolidBrush (WinBackground));
          }

          if (hbrBackground && Bitmapmode == 1)
          {
              hbrBackground =  CreatePatternBrush (hBitmap);
              SetClassLong (hWnd, GCL_HBRBACKGROUND, 
                                  (long) hbrBackground);
          }               
          else if (hbrBackground && Bitmapmode == 2)
          {
              GetClientRect (hWnd, &rect);
              hdc = GetDC (hWnd);
              StrechBitmapMem (hdc, rect.right, rect.bottom);
              ReleaseDC (hWnd, hdc);
              hbrBackground =  CreatePatternBrush (hBitmap);
              SetClassLong (hWnd, GCL_HBRBACKGROUND, 
                                  (long) hbrBackground);
          }               
          if (hMenu)
          {
                 SetMenu (hWnd, hMenu);
          }
          if (tbb)
          {
                  if (tbMain == NULL)
                  {
                      tbMain = hWnd;
                  }
                  hwndTB = MakeToolBarEx (hInstance, 
		                       tbMain,tbb, tblen,
                               qInfo, qIdfrom,
		                       qhWndInfo, qhWndFrom);
          }

		  ShowWindow (hWndMain, ShowModus);
		  UpdateWindow (hWndMain);
		  TestTrack ();
		  TestVTrack ();
		  ShowWindow (hWnd, SW_SHOWNORMAL);
		  UpdateWindow (hWnd);
          hdc = GetDC (hWnd);
//          fWork->display (hWnd, hdc);
          if (TmFont)
          {
                   hFont = SetDeviceFont (hdc, TmFont, &tm);
                   oldfont = SelectObject (hdc, hFont);
	               GetTextMetrics (hdc, &tm);
		           GetTextExtentPoint32 (hdc, "X", 1, &size);
		           DeleteObject (SelectObject (hdc, oldfont));
                   CharHeight = tm.tmHeight;
                   CharWidth  = size.cx;
          }
          ReleaseDC (hWnd, hdc);
		  return hWnd;
}

BOOL DLG::OnMove (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
		  RECT rect;
		  RECT rect1;

          if (hWnd != hMainWindow) return FALSE;
          if (DockProg)
          {
              (*DockProg) ();
          }
          if (fWork == NULL) return FALSE;

          fWork->SetNewPos (TRUE);
          fWork->display ();
   	      GetClientRect (hMainWindow, &rect);
          GetWindowRect (hMainWindow, &rect1);
          if (hMainWindow && ((style & WS_CHILD) == 0))
          {
          	      GetClientRect (hMainWindow, &rect);
		          GetWindowRect (hMainWindow, &rect1);
                  if (this->xorg < 0)
                  {
		                  this->x  = max (0, (rect.right - cx) / 2)  + rect1.left;
                  }
			      if (this->yorg < 0)
                  {
		                  this->y  = max (0, (rect.bottom - cy) / 2) + rect1.top;
                  }
                  if (cxorg <= 0)
                  {
                      cx = rect.right + cxorg + rect1.left - x;
                  }
                  if (cyorg <= 0)
                  {
                      cy = rect.bottom + cyorg + rect1.top - y;
                  }
		  }
          else if (hMainWindow)
          {
          	      GetClientRect (hMainWindow, &rect);
                  if (this->xorg < 0)
                  {
		                  this->x  = max (0, (rect.right - cx) / 2);
                  }

			      if (this->yorg < 0)
                  {
         		          this->y  = max (0, (rect.bottom - cy) / 2);
                  }
                  if (cxorg <= 0)
                  {
                      this->cx = rect.right + cxorg - x;
                  }
                  if (cyorg <= 0)
                  {
                      this->cy = rect.bottom + cyorg - y;
                  }
		  }
          if (hWndMain)
          {
                 ::MoveWindow (this->hWndMain, x, y, cx, cy, TRUE);
                 GetClientRect (hWndMain, &rect);
                 cx = rect.right;
                 cy = rect.bottom;
                 ::MoveWindow (this->hWnd, 0, 0, cx, cy, TRUE);
                 DestroyTrack ();
                 DestroyVTrack ();
		         TestTrack ();
		         TestVTrack ();

          }
          else
          {
                 ::MoveWindow (this->hWnd, x, y, cx, cy, TRUE);
          }
          fWork->SetNewPos (FALSE);
/*
          if (vTrack)
          {
             SendMessage (hWnd, WM_VSCROLL, SB_TOP, 0l);
          }
          if (hTrack)
          {
             SendMessage (hWnd, WM_HSCROLL, SB_TOP, 0l);
          }
*/
          return FALSE;
}


BOOL DLG::TrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT winrect;
		int cx, cy;


        if (TrackAllways)
        {
                    return TRUE;
        }

        if (fWork == NULL)
        {
            return FALSE;
        }
        fWork->GetRect (&cx, &cy);
		if (cx == 0) return FALSE;
        GetClientRect (hWnd, &winrect);
        if (cx > winrect.right)
        {
                    return TRUE;
        }
        return FALSE;
}

void DLG::TestTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
//	   SCROLLINFO scinfo;

       if (TrackNeeded ())
       {
           CreateTrack ();
       }
       else
       {
           DestroyTrack ();
       }
       if (hTrack == NULL) return;

       GetClientRect (hWnd, &rect);
       x = 0;
       y = rect.bottom;
       cy = ScSize;
       cx = rect.right;
/*
       ::MoveWindow (hTrack, x, y, cx, cy, TRUE);
	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE;
       scinfo.nPage  = 1;
       SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
*/
}

						
void DLG::CreateTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
		int fcx, fcy;
        RECT rect;
        RECT crect;
		SCROLLINFO scinfo;

        if (hTrack) return;

        GetClientRect (hWndMain, &rect);

        x = 0;
        y = rect.bottom - ScSize;
        cy = ScSize;
        cx = rect.right;

		hScroll = 0;
        hTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_HORZ,
                               x, y, cx, cy,
                               hWndMain,
                               (HMENU) 701,
                               this->hInstance,
                               0);
         fWork->GetRect (&fcx, &fcy); 
		 if (ScChar == FALSE)
		 {
		         hScrollRange = (fcx - cx + ScSize) / hScWidth + 2;
		         hScrollPage = cx / hScWidth;
		 }
		 else
		 {
                 hScWidth = fWork->xGetTm (NULL, hWnd); 
		         hScrollRange = (fcx - cx + ScSize) / hScWidth + 2;
		         hScrollPage = cx / hScWidth;
		 }
		 scinfo.cbSize = sizeof (SCROLLINFO);
		 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		 scinfo.nMin   = 0;
		 scinfo.nMax   = (fcx  + ScSize) / hScWidth + 2;
		 scinfo.nPage  = cx / hScWidth;
         SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
		 GetClientRect (hWnd, &rect);
		 GetClientRect (hWndMain, &crect);
		 x = rect.left;
		 y = rect.top;
		 cx = rect.right;
		 cy = crect.bottom - ScSize;
		 ::MoveWindow (hWnd, 0, 0, cx, cy, TRUE);
}


void DLG::DestroyTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (hTrack)
          {
              ::DestroyWindow (hTrack);
              hTrack = NULL;
          }
}

		    
BOOL DLG::VTrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT winrect;
		int cx, cy;

        if (TrackAllways)
        {
                    return TRUE;
        }

        if (fWork == NULL)
        {
            return FALSE;
        }

        fWork->GetRect (&cx, &cy);
		if (cy == 0) return FALSE;
        GetClientRect (hWnd, &winrect);
        if (cy + 3 > winrect.bottom)
        {
                    return TRUE;
        }
        return FALSE;
}


void DLG::TestVTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
//	   SCROLLINFO scinfo;

       if (VTrackNeeded ())
       {
           CreateVTrack ();
       }
       else
       {
           DestroyVTrack ();
       }
       if (vTrack == NULL) return;

       GetClientRect (hWnd, &rect);
       x = rect.right - ScSize;
       y = 0;
       cy = rect.bottom;
       cx = ScSize;
/*
       ::MoveWindow (hTrack, x, y, cx, cy, TRUE);
	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE;
       scinfo.nPage  = 1;
       SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
*/
}

						
void DLG::CreateVTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
		int fcx, fcy;
        RECT rect;
        RECT crect;
		SCROLLINFO scinfo;

        if (vTrack) return;

        GetClientRect (hWndMain, &rect);
        GetClientRect (hWnd, &crect);


        x = rect.right - ScVSize;
        cx = ScVSize;
        y = 0;
        cy = crect.bottom;

		vScroll = 0;
        vTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_VERT,
                               x, y, cx, cy,
                               hWndMain,
                               (HMENU) 701,
                               this->hInstance,
                               0);
         fWork->GetRect (&fcx, &fcy); 
		 if (ScChar == FALSE)
		 {
		         vScrollRange = (fcy - cy + ScSize) / vScWidth + 3;
		         vScrollPage = cy / vScWidth;
		 }
		 else
		 {
                 vScWidth = fWork->GetTm (NULL, hWnd); 
		         vScrollRange = (fcy - cy + ScSize) / vScWidth + 2;
		         vScrollPage = cy / vScWidth;
		 }
		 scinfo.cbSize = sizeof (SCROLLINFO);
		 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		 scinfo.nMin   = 0;
		 scinfo.nMax   = (fcy + ScSize) / vScWidth + 3;
		 scinfo.nPage  = cy / vScWidth;
         SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
		 GetClientRect (hWnd, &rect);
		 GetClientRect (hWndMain, &crect);
		 x = rect.left;
		 y = rect.top;
		 cx = crect.right  - ScVSize;
		 cy = rect.bottom;
		 ::MoveWindow (hWnd, 0, 0, cx, cy, TRUE);
}


void DLG::DestroyVTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (vTrack)
          {
              ::DestroyWindow (vTrack);
              vTrack = NULL;
          }
}

void DLG::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void DLG::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}


void DLG::ReadBmp (HWND hMainWindow, char *bmpname, int cx, int cy)
/**
Bitmap fuer Hauptfenster lesen.
**/
{
	    char *etc;
		char buffer [256];
		HDC hdc;

		etc = getenv ("ETC");
		if (etc == NULL) return;
		
        sprintf (buffer, "%s\\%s", etc, bmpname);
        hdc = GetDC (hMainWindow);
		hBitmap = bMap.ReadBitmap (hdc, buffer);
		if (cx > 0 && cy > 0)
		{
		        StrechBitmapMem (hdc, cx, cy);
		}
		ReleaseDC (hMainWindow, hdc);
        hbrBackground =  CreatePatternBrush (hBitmap);
}

void DLG::StrechBitmapMem (HDC hdc, int cx, int cy)
{

        HDC hdcMemory;
        HDC hdcMemoryZ;
        HDC hbmOld;
        BITMAP bm;
		HBITMAP hBitmapZoom;

		hBitmapOrg = hBitmap;
        GetObject (hBitmapOrg, sizeof (BITMAP), &bm);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hBitmapOrg);
        hBitmapZoom = CreateCompatibleBitmap (hdc, cx, cy);
        hdcMemoryZ  = CreateCompatibleDC(hdc);
        SelectObject (hdcMemoryZ, hBitmapZoom);

        SetViewportOrgEx (hdc, 0, 0, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdcMemoryZ, 0, 0,
                         cx, cy,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, SRCCOPY);
        DeleteDC (hdcMemory);        
        DeleteDC (hdcMemoryZ);        
        hBitmap = hBitmapZoom;
}
       
void DLG::SetTimer (UINT nIDEvent, UINT uElapse, TIMERPROC TimerProc)
{
        ::SetTimer (hWnd, nIDEvent, uElapse, TimerProc);
}
						


