#include <stdio.h>
#include <string.h>
#include <windows.h>
#include "strfkt.h"
#include "cfg.h"
#include "cfg_doc.h"
#include "mo_progcfg.h"
#include "mo_menu.h"
#include "text.h"

static char rec [1024];
static char dm  [512];

static CFG_CLASS CfgClass;
static CFG_DOC_CLASS CfgDocClass;

BOOL PROG_CFG::OpenCfg (void)
/**
CFG-Datei oeffnen.
**/
{
         char *etc;

         etc = getenv ("BWSETC");
         if (etc == NULL)
         {
                     etc = "c:\\user\\fit\\etc";
         }
 
         sprintf (dm, "%s\\%s.cfg", etc, progname);

         progfp = fopen (dm, "r");
         if (progfp == NULL) return FALSE;
         return TRUE;
}
           
void PROG_CFG::CloseCfg (void)
/**
CFG-Datei schliessen.
**/
{
         if (progfp)
         {
             fclose (progfp);
             progfp = NULL;
         }
}

BOOL PROG_CFG::GetCfgValue (char *progitem, char *progvalue)
/**
Wert fuer progitem holen.
**/
{
           extern short sql_mode;



         int anz;
         BOOL itOK; 
         int len;
         char text [512];
         char def [512];
		 char *values [30];
         char help [512];
		 char cr_lf [2];
//		 BOOL PROG_CFG::ReadCfgItem (char *itname, char *def, char *text,
//                            char **values, char *help)

           sql_mode = 1;

		 strcpy (help,"");
		 strcpy (text,"");
         itOK = FALSE;
         if (progfp == NULL)
         {
                   if (OpenCfg () == FALSE) return FALSE;
         }
		 strcpy (cr_lf , " ");
		 cr_lf[0] = char (10);
//		 cr_lf[1] = char (13);

         clipped (progitem);
         progvalue[0] = (char) 0;
		 fseek (progfp, 0l, 0);
         while (fgets (rec, 1023, progfp))
         {
                   if (rec[0] < (char) 0x30) continue;

                   anz = wsplit (rec, " ");
                   if (anz < 2) continue;
                   len = max (strlen (progitem), strlen (wort[0]));
                   if (strupcmp (wort[0], progitem, len) == 0)
                   {
                              strcpy (progvalue, wort[1]);
							  clipped (progvalue);
                              itOK = TRUE; 
							  ReadCfgItem (progitem,def, text, values, help);   //testtest
							  clipped(def);
							  strcpy(progvalue,def);
                              break;
                   }
         }
		 /**** testtest
		 strcpy (cfg.programm, "51100");
		 strcpy (cfg.kunde, " ");
		 strcpy (cfg.pers_nam, sys_ben.pers_nam);
	 	 DWORD nSize = sizeof(cfg.computername);
		 GetComputerName(cfg.computername, &nSize);   
         char datum [11];
         sysdate (datum);
         cfg.datum = dasc_to_long (datum);
	     strcpy (cfg.item, progitem);
		 CfgClass.dbreadfirst ();
	
		 strcpy (cfg_doc.programm, "51100");
	     strcpy (cfg_doc.item, progitem);
		 CfgDocClass.dbreadfirst ();

		 strcpy(cfg.wert," ");

		 strcpy(cfg_doc.text," ");
		 strcpy(cfg_doc.values," ");
		 strcpy(cfg_doc.help," ");
		 if (itOK == TRUE) 
		 {
			 strncpy(cfg.wert,progvalue,sizeof(cfg.wert));
			 strncpy(cfg_doc.text,clipped(text),strlen(clipped(text)));
			 cfg_doc.text[strlen(text)] = 0;
			 strncpy(cfg_doc.help,clipped(help),strlen(clipped(help)));
			 cfg_doc.help[strlen(help)] = 0;
			 int i = 0;
			 strcpy(cfg_doc.values,"");
			 while (strlen (values[i]) > 1)
			 {
				 if (strlen(cfg_doc.values) > 1)
				 {
					 strcat (cfg_doc.values,cr_lf);
				 }

				 if (strlen (cfg_doc.values) + strlen (clipped(values[i])) < sizeof(cfg_doc.values) )
				 {
					strncat(cfg_doc.values,clipped(values[i]),strlen(clipped(values[i])));
					i++;
				 }
			 }
			cfg_doc.values[strlen(cfg_doc.values)] = 0;

		 }
		 else
		 {
			 strncpy(cfg.wert,"???",sizeof(cfg.wert));
		 }
		 CfgClass.dbupdate();
		 CfgDocClass.dbupdate();
           sql_mode = 0;
 ***********/


         return itOK;
}
         
BOOL PROG_CFG::GetCfgValue (char *progitem, char *progvalue, char *csplit)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         BOOL itOK; 
         int len;

         itOK = FALSE;
         if (progfp == NULL)
         {
                   if (OpenCfg () == FALSE) return FALSE;
         }


		char com_name[255];
		DWORD nSize = sizeof(com_name);
		GetComputerName(com_name, &nSize);   //testtest
		char user_name[255];
		nSize = sizeof(user_name);
		GetUserName(user_name, &nSize);

         clipped (progitem);
         progvalue[0] = (char) 0;
		 fseek (progfp, 0l, 0);
         while (fgets (rec, 1023, progfp))
         {
                   if (rec[0] < (char) 0x30) continue;

                   anz = wsplit (rec, csplit);
                   if (anz < 2) continue;
                   len = max (strlen (progitem), strlen (wort[0]));
                   if (strupcmp (wort[0], progitem, len) == 0)
                   {
                              strcpy (progvalue, wort[1]);
							  clipped (progvalue);
                              itOK = TRUE; 
                              break;
                   }
         }
         return itOK;
}

BOOL PROG_CFG::ReadCfgItem (char *itname, char *def, char *text,
                            char **values, char *help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         char *s;


		 Text srec = rec;
		 Text stext;
         if (progfp == NULL)
         {
                   if (OpenCfg () == FALSE) return FALSE;
         }

		 /**
         while (s = fgets (rec, 1023, progfp))
         {
                   if (rec[0] >= (char) 0x30) break;
         }
		 **/

         if (s == NULL) return FALSE;

         anz = wsplit (rec, " ");
         if (anz < 2) return FALSE;

         itname = (char *) malloc (strlen (wort[0]) + 10);
         if (itname == NULL) return FALSE;
         strcpy (itname, wort[0]);
//         def = (char *) malloc (strlen (wort[1]) + 10);
//         if (def == NULL) return FALSE;
         strcpy (def, wort [1]);
//         text = (char *) malloc (strlen (wort[2]) + 10);
//         if (text == NULL) return FALSE;
		 if (anz > 2)
		 {
			  int lwort0 = strlen(wort[0]);
			  int lwort1 = strlen(wort[1]);
			  int lrec = strlen(rec);
			  int von = lwort0 + lwort1 + 1;
			  int anz = lrec - lwort0 - lwort1;
//			  sprintf(cmess,"von: %ld, anz: %ld wort0: %ld,  wort1 %ld",von,anz,lwort0,lwort1);
//			  disp_mess(cmess,2);

			  stext = srec.SubString (von, anz);  
			  strcpy (text, stext.GetBuffer ());
			  text[strlen(text) -1] = 0;
		 }
         return ReadCfgValues (values, help);
}

BOOL PROG_CFG::ReadCfgValues (char **values, char *help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         int i;
         char *s;
         BOOL InValue;

         if (progfp == NULL)
         {
                   return FALSE;
         }

         i = 0;
         InValue = FALSE;
         while (s = fgets (rec, 1023, progfp))
         {
                   cr_weg (rec);
                   if (rec[0] >= (char) 0x30) 
				   {
				         values[i] = (char *) malloc (strlen (rec) + 10);
						strcpy (values[i], " ");
					   return FALSE;
				   }
                   anz = wsplit (rec, " ");
                   if (anz == 0) continue;
                   if (strcmp (wort[0], "$VALUES") == 0) 
                   {
                       InValue = TRUE;
                       continue;
                   }

                   if (strcmp (wort[0], "$HELP")   == 0) 
                   {
					   if (InValue)
					   {
							values[i] = (char *) malloc (strlen (rec) + 10);
							strcpy (values[i], " ");
					   }

                        return ReadCfgHelp (help);
                   }
                   if (strcmp (wort[0], "$END")    == 0) return TRUE;
                   if (InValue)
                   {
                       values[i] = (char *) malloc (strlen (rec) + 10);
                       if (values[i] == NULL)
                       {
                           return FALSE;
                       }
                       strcpy (values[i], rec);
                       i ++;
					   if (i > 28) break;
                   }
         }
         values[i] = (char *) malloc (strlen (rec) + 10);
         strcpy (values[i], " ");

         return FALSE;
}
         
                          
BOOL PROG_CFG::ReadCfgHelp (char *help)
/**
Wert fuer progitem holen.
**/
{
         int anz;
         int i;
         char *s;
         BOOL InHelp;

         if (progfp == NULL)
         {
                   return FALSE;
         }

         i = 0;
         InHelp = FALSE;
         if (strcmp (wort[0], "$HELP") == 0) 
         {
              InHelp = TRUE;
         }

         while (s = fgets (rec, 1023, progfp))
         {
                   cr_weg (rec);
//                   if (rec[0] >= (char) 0x30) return FALSE;
                   anz = wsplit (rec, " ");
                   if (anz == 0) continue;
                   if (strcmp (wort[0], "$HELP") == 0) 
                   {
                       InHelp = TRUE;
                       continue;
                   }

                   if (strcmp (wort[0], "$END")    == 0) return TRUE;
                   if (InHelp)
                   {
//                       help = (char *) malloc (strlen (rec) + 10);
//                       if (help == NULL) return FALSE;
                       strcpy (help, rec);
                   }
         }
         return FALSE;
}
         
                               

BOOL PROG_CFG::GetGlobDefault (char *env, char *wert)
/**
Wert aus bws_default holen.
**/
{
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\bws_defa", etc);
        fp = fopen (buffer, "r");
        if (fp == NULL) return FALSE;

        clipped (env);
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], env, strlen (env)) == 0)
                     {
                                 strcpy (wert, wort [2]);
                                 fclose (fp);
                                 return TRUE;
                     }
         }
         fclose (fp);
         return (FALSE);
}

BOOL PROG_CFG::GetGroupDefault (char *env, char *wert)
/**
Wert aus fitgroup.def holen.
**/
{
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\fitgroup.def", etc);
        fp = fopen (buffer, "r");
        if (fp == NULL) return FALSE;

        clipped (env);
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = wsplit (buffer, " ");
                     if (anz < 2) continue;
                     if (strupcmp (wort[0], env, strlen (env)) == 0)
                     {
                                 strcpy (wert, wort [1]);
                                 fclose (fp);
                                 return TRUE;
                     }
         }
         fclose (fp);
         return (FALSE);
}

