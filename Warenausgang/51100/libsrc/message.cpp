#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "wmaskc.h"
#include "stdfkt.h"
#include "message.h"

static HWND hMainWind;
static char wtext [120];

static ITEM iwtext ("", wtext, "", 0);
static field _fwait [] = {&iwtext,   100, 0, 1, 1, 0, "", DISPLAYONLY, 0, 0, 0};
static form fwait = {1, 0, 0, _fwait, 0 ,0, 0, 0, NULL};

void MELDUNG::Message (HWND hWnd, char *text, int zeile)
/**
Meldung im Hauptfenster.
**/
{

	   static ITEM imess ("", "", "", 0);
	   static field _fmess [] = 
	   {
		   &imess,     60, 0, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0
	   };
	   
	   static form fmess = {1, 0, 0, _fmess, 0, 0, 0, 0, NULL};

	   HWND oldactive, oldaktiv;
	   form *scurrent;

       oldactive = GetActiveWindow ();
       oldaktiv  = AktivWindow;
	   scurrent = current_form;
	   imess.SetFeldPtr (text);
	   display_form (hWnd, &fmess, zeile, 0);
	   messform = &fmess;
	   SetActiveWindow (oldactive);
	   AktivWindow = oldaktiv;
	   current_form = scurrent;
}


void MELDUNG::SaveMessPos (void)
{
       RECT frect;
       RECT frect2;
       RECT mrect;

       GetWindowRect (MesshWnd,  &frect);
       GetWindowRect (hMainWind, &mrect);

       Fktx = frect.left - mrect.left;
       Fkty = mrect.bottom - frect.top;
       Fktcy = frect.bottom - frect.top;

       Fktx2 = frect2.left - mrect.left;
       Fkty2 = mrect.bottom - frect2.top;
       Fktcy2 = frect2.bottom - frect2.top;
}

void MELDUNG::MoveMess (void)
{
       RECT mrect;
       int x,y,cx, cy;

       GetWindowRect (hMainWind, &mrect);

       x = 0;
       y = (mrect.bottom - Fkty - mrect.top - 50);
       cx = mrect.right - mrect.left;
       cy = Fktcy;

       MoveWindow (MesshWnd, x,y,cx,cy, TRUE);
}


HWND MELDUNG::OpenMessage (void)
/**
Funktionstasten-Fenster oeffnen.
**/
{
       HANDLE hInstance;
       HWND hWnd;
       HWND aWindow;

       aWindow = AktivWindow;
       hMainWind = GetActiveWindow ();
       hWnd = GetParent (hMainWind);
       while (hWnd)
       {
           hMainWind = hWnd;
           hWnd = GetParent (hMainWind);
       }

       hInstance = (HANDLE) GetWindowLong (hMainWind, 
                                               GWL_HINSTANCE);
       SetEnvFont ();
       SetBorder (NULL);
       SetAktivWindow (hMainWind);

       MesshWnd = OpenColWindow (1, 80, 22, 0, WS_CHILD,
                                  StdBackCol, NULL);

       SaveMessPos ();
       AktivWindow = aWindow;
       return MesshWnd;
}




void MELDUNG::Message (char *text)
/**
Meldung im Meldungsfenster.
**/
{
       static mfont msgfont    = {NULL, -1, -1, 1,
                                       BLACKCOL,
                                       StdBackCol,
                                       0};

	   static ITEM imess ("", "", "", 0);
	   static field _fmess [] = 
	   {
		   &imess,     60, 0, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0
	   };
	   
	   static form fmess = {1, 0, 0, _fmess, 0, 0, 0, 0, &msgfont};

	   HWND oldactive, oldaktiv;
	   form *scurrent;

       if (MesshWnd == NULL)
       {
           OpenMessage ();
       }
       oldactive = GetActiveWindow ();
       oldaktiv  = AktivWindow;
	   scurrent = current_form;
	   imess.SetFeldPtr (text);
	   display_form (MesshWnd, &fmess, 0, 0);
	   messform = &fmess;
	   SetActiveWindow (oldactive);
	   AktivWindow = oldaktiv;
	   current_form = scurrent;
}


void MELDUNG::CloseMessage (void)
/**
Meldung schliessen.
**/
{
	   if (messform)
	   {
		   CloseControls (messform);
		   messform = NULL;
	   }
}


void MELDUNG::CloseMesshWnd (void)
/**
Meldungsfenster schliessen.
**/
{
        CloseMessage ();
        DestroyWindow (MesshWnd);
        MesshWnd = NULL;
}

void MELDUNG::WaitWindow (char *text)
/**
Wartefenster anzeigen.
**/
{
       HANDLE hInstance;
       HWND hWnd;
       HWND aWindow;
	   int Width;
	   int left;

       aWindow = AktivWindow;
       hMainWind = GetActiveWindow ();
       hWnd = GetParent (hMainWind);
       while (hWnd)
       {
           hMainWind = hWnd;
           hWnd = GetParent (hMainWind);
       }

       hInstance = (HANDLE) GetWindowLong (hMainWind, 
                                               GWL_HINSTANCE);
       SetEnvFont ();
       SetAktivWindow (hMainWind);

	   strcpy (wtext, text);
	   Width = strlen (wtext) + 2;
	   left = max (0, (80 - Width) / 2);
       WaithWnd = OpenColWindow (3, Width, 12, left, WS_CHILD | WS_DLGFRAME,
                                  StdBackCol, NULL);
	   if (WaithWnd == NULL) return;
	   display_form (WaithWnd, &fwait, 0, 0);
}   
	     
void MELDUNG::CloseWaitWindow (void)
/**
WaitWindow schliessen.
**/
{
	    if (WaithWnd)
		{
			CloseControls (&fwait);
			DestroyWindow (WaithWnd);
			WaithWnd = NULL;
		}
}
