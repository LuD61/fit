#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#ifdef MO_CURSC
#include "mo_cursc.h"
#else
#include "mo_curso.h"
#endif
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lief.h"

struct LIEF lief, lief_null;

void LIEF_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lief.mdn, 1, 0);
            ins_quest ((char *) &lief.lief, 0, 17);
    out_quest ((char *) lief.abr,0,2);
    out_quest ((char *) &lief.adr,2,0);
    out_quest ((char *) lief.bank_nam,0,37);
    out_quest ((char *) &lief.bbn,2,0);
    out_quest ((char *) &lief.best_sort,1,0);
    out_quest ((char *) lief.best_trans_kz,0,2);
    out_quest ((char *) &lief.blz,2,0);
    out_quest ((char *) lief.bonus_kz,0,2);
    out_quest ((char *) &lief.fil,1,0);
    out_quest ((char *) lief.fracht_kz,0,2);
    out_quest ((char *) lief.ink,0,17);
    out_quest ((char *) &lief.kreditor,2,0);
    out_quest ((char *) lief.kto,0,17);
    out_quest ((char *) &lief.letzt_lief,2,0);
    out_quest ((char *) lief.lief,0,17);
    out_quest ((char *) lief.lief_kun,0,17);
    out_quest ((char *) lief.lief_rht,0,2);
    out_quest ((char *) &lief.lief_s,2,0);
    out_quest ((char *) &lief.lief_typ,1,0);
    out_quest ((char *) &lief.lief_zeit,1,0);
    out_quest ((char *) &lief.lief_zusch,3,0);
    out_quest ((char *) &lief.lst_nr,2,0);
    out_quest ((char *) &lief.mahn_stu,1,0);
    out_quest ((char *) &lief.mdn,1,0);
    out_quest ((char *) lief.me_kz,0,2);
    out_quest ((char *) lief.min_me_kz,0,2);
    out_quest ((char *) &lief.min_zusch,3,0);
    out_quest ((char *) &lief.min_zusch_proz,3,0);
    out_quest ((char *) lief.nach_lief,0,2);
    out_quest ((char *) lief.ordr_kz,0,2);
    out_quest ((char *) lief.rab_abl,0,2);
    out_quest ((char *) lief.rab_kz,0,2);
    out_quest ((char *) lief.rech_kz,0,2);
    out_quest ((char *) &lief.rech_toler,3,0);
    out_quest ((char *) &lief.sdr_nr,1,0);
    out_quest ((char *) &lief.son_kond,2,0);
    out_quest ((char *) &lief.sprache,1,0);
    out_quest ((char *) lief.steuer_kz,0,2);
    out_quest ((char *) lief.unt_lief_kz,0,2);
    out_quest ((char *) lief.vieh_bas,0,2);
    out_quest ((char *) lief.we_erf_kz,0,2);
    out_quest ((char *) lief.we_kontr,0,2);
    out_quest ((char *) lief.we_pr_kz,0,2);
    out_quest ((char *) &lief.we_toler,3,0);
    out_quest ((char *) &lief.zahl_kond,1,0);
    out_quest ((char *) lief.zahlw,0,2);
    out_quest ((char *) &lief.hdklpargr,1,0);
    out_quest ((char *) &lief.vorkgr,2,0);
    out_quest ((char *) &lief.delstatus,1,0);
    out_quest ((char *) &lief.mwst,1,0);
    out_quest ((char *) &lief.liefart,1,0);
    out_quest ((char *) lief.modif,0,2);
    out_quest ((char *) lief.ust_id,0,17);
    out_quest ((char *) lief.eg_betriebsnr,0,21);
    out_quest ((char *) &lief.eg_kz,1,0);
    out_quest ((char *) &lief.waehrung,1,0);
    out_quest ((char *) lief.iln,0,17);
    out_quest ((char *) &lief.zertifikat1,1,0);
    out_quest ((char *) &lief.zertifikat2,1,0);
            cursor = prepare_sql ("select lief.abr,  lief.adr,  "
"lief.bank_nam,  lief.bbn,  lief.best_sort,  lief.best_trans_kz,  "
"lief.blz,  lief.bonus_kz,  lief.fil,  lief.fracht_kz,  lief.ink,  "
"lief.kreditor,  lief.kto,  lief.letzt_lief,  lief.lief,  lief.lief_kun,  "
"lief.lief_rht,  lief.lief_s,  lief.lief_typ,  lief.lief_zeit,  "
"lief.lief_zusch,  lief.lst_nr,  lief.mahn_stu,  lief.mdn,  lief.me_kz,  "
"lief.min_me_kz,  lief.min_zusch,  lief.min_zusch_proz,  "
"lief.nach_lief,  lief.ordr_kz,  lief.rab_abl,  lief.rab_kz,  "
"lief.rech_kz,  lief.rech_toler,  lief.sdr_nr,  lief.son_kond,  "
"lief.sprache,  lief.steuer_kz,  lief.unt_lief_kz,  lief.vieh_bas,  "
"lief.we_erf_kz,  lief.we_kontr,  lief.we_pr_kz,  lief.we_toler,  "
"lief.zahl_kond,  lief.zahlw,  lief.hdklpargr,  lief.vorkgr,  "
"lief.delstatus,  lief.mwst,  lief.liefart,  lief.modif,  lief.ust_id,  "
"lief.eg_betriebsnr,  lief.eg_kz,  lief.waehrung,  lief.iln,  "
"lief.zertifikat1,  lief.zertifikat2 from lief "

#line 27 "lief.rpp"
                                  "where mdn = ? "
                                  "and   lief = ?");

    ins_quest ((char *) lief.abr,0,2);
    ins_quest ((char *) &lief.adr,2,0);
    ins_quest ((char *) lief.bank_nam,0,37);
    ins_quest ((char *) &lief.bbn,2,0);
    ins_quest ((char *) &lief.best_sort,1,0);
    ins_quest ((char *) lief.best_trans_kz,0,2);
    ins_quest ((char *) &lief.blz,2,0);
    ins_quest ((char *) lief.bonus_kz,0,2);
    ins_quest ((char *) &lief.fil,1,0);
    ins_quest ((char *) lief.fracht_kz,0,2);
    ins_quest ((char *) lief.ink,0,17);
    ins_quest ((char *) &lief.kreditor,2,0);
    ins_quest ((char *) lief.kto,0,17);
    ins_quest ((char *) &lief.letzt_lief,2,0);
    ins_quest ((char *) lief.lief,0,17);
    ins_quest ((char *) lief.lief_kun,0,17);
    ins_quest ((char *) lief.lief_rht,0,2);
    ins_quest ((char *) &lief.lief_s,2,0);
    ins_quest ((char *) &lief.lief_typ,1,0);
    ins_quest ((char *) &lief.lief_zeit,1,0);
    ins_quest ((char *) &lief.lief_zusch,3,0);
    ins_quest ((char *) &lief.lst_nr,2,0);
    ins_quest ((char *) &lief.mahn_stu,1,0);
    ins_quest ((char *) &lief.mdn,1,0);
    ins_quest ((char *) lief.me_kz,0,2);
    ins_quest ((char *) lief.min_me_kz,0,2);
    ins_quest ((char *) &lief.min_zusch,3,0);
    ins_quest ((char *) &lief.min_zusch_proz,3,0);
    ins_quest ((char *) lief.nach_lief,0,2);
    ins_quest ((char *) lief.ordr_kz,0,2);
    ins_quest ((char *) lief.rab_abl,0,2);
    ins_quest ((char *) lief.rab_kz,0,2);
    ins_quest ((char *) lief.rech_kz,0,2);
    ins_quest ((char *) &lief.rech_toler,3,0);
    ins_quest ((char *) &lief.sdr_nr,1,0);
    ins_quest ((char *) &lief.son_kond,2,0);
    ins_quest ((char *) &lief.sprache,1,0);
    ins_quest ((char *) lief.steuer_kz,0,2);
    ins_quest ((char *) lief.unt_lief_kz,0,2);
    ins_quest ((char *) lief.vieh_bas,0,2);
    ins_quest ((char *) lief.we_erf_kz,0,2);
    ins_quest ((char *) lief.we_kontr,0,2);
    ins_quest ((char *) lief.we_pr_kz,0,2);
    ins_quest ((char *) &lief.we_toler,3,0);
    ins_quest ((char *) &lief.zahl_kond,1,0);
    ins_quest ((char *) lief.zahlw,0,2);
    ins_quest ((char *) &lief.hdklpargr,1,0);
    ins_quest ((char *) &lief.vorkgr,2,0);
    ins_quest ((char *) &lief.delstatus,1,0);
    ins_quest ((char *) &lief.mwst,1,0);
    ins_quest ((char *) &lief.liefart,1,0);
    ins_quest ((char *) lief.modif,0,2);
    ins_quest ((char *) lief.ust_id,0,17);
    ins_quest ((char *) lief.eg_betriebsnr,0,21);
    ins_quest ((char *) &lief.eg_kz,1,0);
    ins_quest ((char *) &lief.waehrung,1,0);
    ins_quest ((char *) lief.iln,0,17);
    ins_quest ((char *) &lief.zertifikat1,1,0);
    ins_quest ((char *) &lief.zertifikat2,1,0);
            sqltext = "update lief set lief.abr = ?,  "
"lief.adr = ?,  lief.bank_nam = ?,  lief.bbn = ?,  lief.best_sort = ?,  "
"lief.best_trans_kz = ?,  lief.blz = ?,  lief.bonus_kz = ?,  "
"lief.fil = ?,  lief.fracht_kz = ?,  lief.ink = ?,  lief.kreditor = ?,  "
"lief.kto = ?,  lief.letzt_lief = ?,  lief.lief = ?,  "
"lief.lief_kun = ?,  lief.lief_rht = ?,  lief.lief_s = ?,  "
"lief.lief_typ = ?,  lief.lief_zeit = ?,  lief.lief_zusch = ?,  "
"lief.lst_nr = ?,  lief.mahn_stu = ?,  lief.mdn = ?,  lief.me_kz = ?,  "
"lief.min_me_kz = ?,  lief.min_zusch = ?,  lief.min_zusch_proz = ?,  "
"lief.nach_lief = ?,  lief.ordr_kz = ?,  lief.rab_abl = ?,  "
"lief.rab_kz = ?,  lief.rech_kz = ?,  lief.rech_toler = ?,  "
"lief.sdr_nr = ?,  lief.son_kond = ?,  lief.sprache = ?,  "
"lief.steuer_kz = ?,  lief.unt_lief_kz = ?,  lief.vieh_bas = ?,  "
"lief.we_erf_kz = ?,  lief.we_kontr = ?,  lief.we_pr_kz = ?,  "
"lief.we_toler = ?,  lief.zahl_kond = ?,  lief.zahlw = ?,  "
"lief.hdklpargr = ?,  lief.vorkgr = ?,  lief.delstatus = ?,  "
"lief.mwst = ?,  lief.liefart = ?,  lief.modif = ?,  lief.ust_id = ?,  "
"lief.eg_betriebsnr = ?,  lief.eg_kz = ?,  lief.waehrung = ?,  "
"lief.iln = ?,  lief.zertifikat1 = ?,  lief.zertifikat2 = ? "

#line 31 "lief.rpp"
                                  "where mdn = ? "
                                  "and   lief = ?";


            ins_quest ((char *) &lief.mdn, 1, 0);
            ins_quest ((char *) &lief.lief, 0, 17);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lief.mdn, 1, 0);
            ins_quest ((char *) &lief.lief, 0, 17);
            test_upd_cursor = prepare_sql ("select lief from lief "
                                  "where mdn = ? "
                                  "and   lief = ?");
            ins_quest ((char *) &lief.mdn, 1, 0);
            ins_quest ((char *) &lief.lief, 0, 17);
            del_cursor = prepare_sql ("delete from lief "
                                  "where mdn = ? "
                                  "and   lief = ?");
    ins_quest ((char *) lief.abr,0,2);
    ins_quest ((char *) &lief.adr,2,0);
    ins_quest ((char *) lief.bank_nam,0,37);
    ins_quest ((char *) &lief.bbn,2,0);
    ins_quest ((char *) &lief.best_sort,1,0);
    ins_quest ((char *) lief.best_trans_kz,0,2);
    ins_quest ((char *) &lief.blz,2,0);
    ins_quest ((char *) lief.bonus_kz,0,2);
    ins_quest ((char *) &lief.fil,1,0);
    ins_quest ((char *) lief.fracht_kz,0,2);
    ins_quest ((char *) lief.ink,0,17);
    ins_quest ((char *) &lief.kreditor,2,0);
    ins_quest ((char *) lief.kto,0,17);
    ins_quest ((char *) &lief.letzt_lief,2,0);
    ins_quest ((char *) lief.lief,0,17);
    ins_quest ((char *) lief.lief_kun,0,17);
    ins_quest ((char *) lief.lief_rht,0,2);
    ins_quest ((char *) &lief.lief_s,2,0);
    ins_quest ((char *) &lief.lief_typ,1,0);
    ins_quest ((char *) &lief.lief_zeit,1,0);
    ins_quest ((char *) &lief.lief_zusch,3,0);
    ins_quest ((char *) &lief.lst_nr,2,0);
    ins_quest ((char *) &lief.mahn_stu,1,0);
    ins_quest ((char *) &lief.mdn,1,0);
    ins_quest ((char *) lief.me_kz,0,2);
    ins_quest ((char *) lief.min_me_kz,0,2);
    ins_quest ((char *) &lief.min_zusch,3,0);
    ins_quest ((char *) &lief.min_zusch_proz,3,0);
    ins_quest ((char *) lief.nach_lief,0,2);
    ins_quest ((char *) lief.ordr_kz,0,2);
    ins_quest ((char *) lief.rab_abl,0,2);
    ins_quest ((char *) lief.rab_kz,0,2);
    ins_quest ((char *) lief.rech_kz,0,2);
    ins_quest ((char *) &lief.rech_toler,3,0);
    ins_quest ((char *) &lief.sdr_nr,1,0);
    ins_quest ((char *) &lief.son_kond,2,0);
    ins_quest ((char *) &lief.sprache,1,0);
    ins_quest ((char *) lief.steuer_kz,0,2);
    ins_quest ((char *) lief.unt_lief_kz,0,2);
    ins_quest ((char *) lief.vieh_bas,0,2);
    ins_quest ((char *) lief.we_erf_kz,0,2);
    ins_quest ((char *) lief.we_kontr,0,2);
    ins_quest ((char *) lief.we_pr_kz,0,2);
    ins_quest ((char *) &lief.we_toler,3,0);
    ins_quest ((char *) &lief.zahl_kond,1,0);
    ins_quest ((char *) lief.zahlw,0,2);
    ins_quest ((char *) &lief.hdklpargr,1,0);
    ins_quest ((char *) &lief.vorkgr,2,0);
    ins_quest ((char *) &lief.delstatus,1,0);
    ins_quest ((char *) &lief.mwst,1,0);
    ins_quest ((char *) &lief.liefart,1,0);
    ins_quest ((char *) lief.modif,0,2);
    ins_quest ((char *) lief.ust_id,0,17);
    ins_quest ((char *) lief.eg_betriebsnr,0,21);
    ins_quest ((char *) &lief.eg_kz,1,0);
    ins_quest ((char *) &lief.waehrung,1,0);
    ins_quest ((char *) lief.iln,0,17);
    ins_quest ((char *) &lief.zertifikat1,1,0);
    ins_quest ((char *) &lief.zertifikat2,1,0);
            ins_cursor = prepare_sql ("insert into lief (abr,  "
"adr,  bank_nam,  bbn,  best_sort,  best_trans_kz,  blz,  bonus_kz,  fil,  fracht_kz,  "
"ink,  kreditor,  kto,  letzt_lief,  lief,  lief_kun,  lief_rht,  lief_s,  lief_typ,  "
"lief_zeit,  lief_zusch,  lst_nr,  mahn_stu,  mdn,  me_kz,  min_me_kz,  min_zusch,  "
"min_zusch_proz,  nach_lief,  ordr_kz,  rab_abl,  rab_kz,  rech_kz,  rech_toler,  "
"sdr_nr,  son_kond,  sprache,  steuer_kz,  unt_lief_kz,  vieh_bas,  we_erf_kz,  "
"we_kontr,  we_pr_kz,  we_toler,  zahl_kond,  zahlw,  hdklpargr,  vorkgr,  "
"delstatus,  mwst,  liefart,  modif,  ust_id,  eg_betriebsnr,  eg_kz,  waehrung,  iln,  "
"zertifikat1,  zertifikat2) "

#line 50 "lief.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 52 "lief.rpp"
}
int LIEF_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

