#ifndef MDNFILDEF
#define MDNFILDEF
#include "mdn.h"
#include "fil.h"

class MDNFIL : public MDN_CLASS, FIL_CLASS 
{
       private :
             BOOL NoMdnNull;
             BOOL NoFilNull;
//            MDN_CLASS mdn_class;
//            FIL_CLASS fil_class;

       public :
		    short FixMdn;
		    short FixFil;

            MDNFIL () : NoMdnNull (0), NoFilNull (0),
                        MDN_CLASS (),FIL_CLASS () 
            {
				FixMdn = FixFil = 0;
            } 
            void SetNoMdnNull (BOOL mnull)
            {
                this->NoMdnNull = mnull;
            }

            void SetNoFilNull (BOOL mnull)
            {
                this->NoFilNull = mnull;
            }

            BOOL GetNoMdnNull (void)
            {
                return this->NoMdnNull;
            }

            BOOL GetNoFilNull (void)
            {
                return this->NoFilNull;
            }

            void SetMandantAttr (int);
            void SetFilialAttr (int);
            int showmdn (void);
            int showfil (void);
            int GetMdn (void);
            int GetFil (void);
            void CloseForm (void);
            int eingabemdnfil (HWND, char *, char *);
};
#endif
