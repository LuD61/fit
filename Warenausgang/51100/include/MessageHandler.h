// MessageHandler.h: Schnittstelle f�r die Klasse MessageHandler.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSAGEHANDLER_H__A300BEF1_7D5B_4E34_90B4_5E2F536D2130__INCLUDED_)
#define AFX_MESSAGEHANDLER_H__A300BEF1_7D5B_4E34_90B4_5E2F536D2130__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>

class CMessageHandler  
{
private:
	DWORD msg;
public:
	void SetMsg (DWORD msg)
	{
		this->msg = msg;
	}
	DWORD GetMsg ()
	{
		return msg;
	}
public:
	CMessageHandler();
	CMessageHandler(DWORD msg);
	virtual ~CMessageHandler();
	virtual BOOL Run () = 0;
};

#endif // !defined(AFX_MESSAGEHANDLER_H__A300BEF1_7D5B_4E34_90B4_5E2F536D2130__INCLUDED_)
