#ifndef _CFG_DEF
#define _CFG_DEF
#include "dbclass.h"

struct CFG {
   char      programm[21];
   char      kunde[17];
   char      pers_nam[9];
   char      item[21];
   char      wert[21];
   long      datum;
   char      computername[65];
};
extern struct CFG cfg, cfg_null;

#line 6 "cfg.rh"

class CFG_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               CFG_CLASS () : DB_CLASS ()
               {
               }
};
#endif
