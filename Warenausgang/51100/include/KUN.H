/*   Datenbankfunktionen                                  */
#ifndef _KUN_OK
#define _KUN_OK
#include "itemc.h"
#include "dbclass.h"

struct KUN {
   short     mdn;
   short     fil;
   long      kun;
   long      adr1;
   long      adr2;
   long      adr3;
   long      kun_seit;
   long      txt_nr1;
   char      frei_txt1[65];
   char      kun_krz1[17];
   char      kun_bran[2];
   char      kun_krz2[17];
   char      kun_krz3[17];
   short     kun_typ;
   long      bbn;
   short     pr_stu;
   long      pr_lst;
   char      vereinb[6];
   long      inka_nr;
   long      vertr1;
   long      vertr2;
   char      statk_period[2];
   char      a_period[2];
   short     sprache;
   long      txt_nr2;
   char      frei_txt2[65];
   char      freifeld1[9];
   char      freifeld2[9];
   long      tou;
   char      vers_art[3];
   short     lief_art;
   char      fra_ko_ber[3];
   short     rue_schei;
   char      form_typ1[3];
   short     auflage1;
   char      freifeld3[9];
   char      freifeld4[9];
   short     zahl_art;
   short     zahl_ziel;
   char      form_typ2[3];
   short     auflage2;
   long      txt_nr3;
   char      frei_txt3[65];
   char      nr_bei_rech[17];
   char      rech_st[3];
   short     sam_rech;
   short     einz_ausw;
   short     gut;
   char      rab_schl[5];
   double    bonus1;
   double    bonus2;
   double    tdm_grenz1;
   double    tdm_grenz2;
   double    jr_plan_ums;
   char      deb_kto[9];
   double    kred_lim;
   short     inka_zaehl;
   char      bank_kun[37];
   long      blz;
   char      kto_nr[17];
   short     hausbank;
   short     kun_of_po;
   short     kun_of_lf;
   short     kun_of_best;
   short     delstatus;
   char      kun_bran2[3];
   long      rech_fuss_txt;
   long      ls_fuss_txt;
   char      ust_id[12];
   long      rech_kopf_txt;
   long      ls_kopf_txt;
   char      gn_pkt_kz[2];
   char      sw_rab[2];
   char      bbs[9];
   long      inka_nr2;
   short     sw_fil_gr;
   short     sw_fil;
   char      ueb_kz[2];
   char      modif[2];
   short     kun_leer_kz;
   char      ust_id16[17];
   char      iln[17];
   short     waehrung;
   short     pr_ausw;
   char      pr_hier[2];
   short     pr_ausw_ls;
   short     pr_ausw_re;
   long      kst;
   long      kond_kun;
   short     kun_schema;
   char      plattform [17];
   char      be_log [4];
   short     kun_gr1;
   short     kun_gr2;
   short	 min_best;

};

extern struct KUN kun, kun_null;

class KUN_CLASS : public DB_CLASS 
{
       private :
            char qstring0[0x1000];
            char qstring0Ex[0x1000];
            int qschange;
            int qschangeEx;
            int cursor_kun;
            int cursor_a_kun;
            int cursor_a_kun_bran;
			int cursor_ausw;
            void preparedb (void);
            int prep_awcursor (char *);
       public:
           KUN_CLASS () : DB_CLASS ()
           {
                    strcpy (qstring0, "Start");
                    strcpy (qstring0Ex, "Start");
                    cursor_kun = -1;
                    cursor_a_kun = -1;
                    cursor_a_kun_bran = -1;
					cursor_ausw = -1;
           }
           void prepare (void);
           void prepare_a_kun (void);
           void prepare_a_kun_bran (void);
           int lese_kun (short, short, long);
           int lese_kun (void);
           int lese_a_kun (short, short, long, double);
           int lese_a_kun (void);
           int lese_a_kun_bran (short, short, char *, double);
           int lese_a_kun_bran (void);
           int dbreadfirst (void);
           int ShowAllBu (HWND, int, short, short);
		   void SetNew (void);
           int PrepareQueryMdn (form *, char *[]);
           int PrepareQuery (form *, char *[]);
           int PrepareQuery (void);
           int PrepareQueryMdn (void);
           int ShowBuQuery (HWND, int); 
           void SetTestKunProc (int (*)(long));
		   void SetNewEx (void);
           int PrepareQueryEx (form *, char *[]);
           int ShowBuQueryEx (HWND, int); 
           void SetSearchModeKun (int mode);
           void SetSearchFieldKun (int);
           void SetCharBuffKun (char *);
		   void ListToMamain1 (HWND);
};

class KUNITEM : virtual public ITEM
{
         private :
   	         int GetDBItem (char *item);
         public :
			 KUNITEM (char *itname, char *value, 
				      char *ittext, int itnum) : 
                  ITEM (itname, value, ittext, itnum)
				  {
				  }	   
             void CallInfo ();
};

#endif /* _KUN_OK  */
