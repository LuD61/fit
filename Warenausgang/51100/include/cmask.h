#ifndef _CMASK_DEF
#define _CMASK_DEF
#include <windows.h>
#include "wmaskc.h"
#include "image.h"

#define CEDIT         0
#define CDISPLAYONLY  1
#define CREADONLY     2
#define CBUTTON       3
#define CCOLBUTTON    4
#define CLISTBOX      5
#define COWNLISTBOX   6
#define CCOMBOBOX     7
#define CCAPTBORDER   8 
#define CFFORM        9
#define CREMOVED     10
#define CSTATIC      11 
#define CBORDER      12 
#define CRIEDIT     100 
#define NOATTRIBUT  999

#define LINE 0
#define RAISED 1
#define HIGH 2
#define LOW 3
#define VLINE 4
#define RAISEDVLINE 5
#define HLINE 6
#define RAISEDHLINE 7

#define POS 0
#define XFLOW 1

#define CCHECKBUTTON  1

extern HFONT EzCreateFontT (HDC hdc, char * szFaceN, int iDeciPtH,
                    int iDeciPtW, int iAttrib, BOOL fLogRes);
extern BOOL DeleteObjectT (HGDIOBJ hObject);


#define EzCreateFont EzCreateFontT
#define DeleteObject DeleteObjectT


class BORDER
{
        private : 
            int Width;
            COLORREF Col;
            COLORREF HiCol;
            int Type;
        public :
            BORDER ()
            {
                Width = 0;
                Col   = GRAYCOL;
                HiCol = WHITECOL;
                Type  = RAISED;
            }

            BORDER (int Type)
            {
                Width = 0;
                Col   = GRAYCOL;
                HiCol = WHITECOL;
                this->Type  = Type;
            }

            BORDER (COLORREF Col, COLORREF HiCol)
            {
                Width = 0;
                this->Col   = Col;
                this->HiCol = HiCol;
                Type  = RAISED;
            }

            BORDER (COLORREF Col, COLORREF HiCol, int Type)
            {
                Width = 0;
                this->Col   = Col;
                this->HiCol = HiCol;
                this->Type  = Type;
            }

            BORDER (COLORREF Col, COLORREF HiCol, int Type, int Width)
            {
                this->Width = Width;
                this->Col   = Col;
                this->HiCol = HiCol;
                this->Type  = Type;
            }

            void  SetColors (COLORREF Col, COLORREF HiCol)
            {
                this->Col   = Col;
                this->HiCol = HiCol;
            }

            void  SetCol (COLORREF Col)
            {
                this->Col   = Col;
            }

            void  SetHiCol (COLORREF HiCol)
            {
                this->HiCol   = HiCol;
            }

            void SetType (int Type)
            {
                this->Type = Type;
            }

            void SetWidth (int Width)
            {
                this->Width = Width;
            }

            COLORREF GetCol (void)
            {
                return Col;
            }

            COLORREF GetHiCol (void)
            {
                return HiCol;
            }

            int GetType (void)
            {
                return Type;
            }

            int GetWidth (void)
            {
                return Width;
            }

            void  PrintVLine (HWND, HDC, int, int, int);
            void  PrintRaisedVLine (HWND, HDC, int, int, int);
            void  PrintHLine (HWND, HDC, int, int, int);
            void  PrintRaisedHLine (HWND, HDC, int, int, int);
            void  PrintLine (HWND, HDC, int, int, int, int);
            void  PrintRaised (HWND, HDC, int, int, int, int);
            void  PrintHigh (HWND, HDC, int, int, int, int);
            void  PrintLow (HWND, HDC, int, int, int, int);
            virtual void  Print (HWND, HDC, int, int, int, int);
};

class FILLEDBORDER : virtual public BORDER
{
    private :
        COLORREF FillCol;
    public :
/*
        FilledBorder () : Border ()
        {
                FillCol = LTGRAYCOL;
        }
*/
        FILLEDBORDER (COLORREF Col, COLORREF HiCol, int Type, int Width, COLORREF FillCol) :
                      BORDER (Col, HiCol, Type, Width)
        {
                this->FillCol   = FillCol;
        }
        void  FillRect (HDC, int, int, int, int);
        void  Print (HWND, HDC, int, int, int, int);
};



class CHS
{
         private :
			    int chsizex;
			    int chsizey;
				int chposx;
				int chposy;
			    int x;
				int y;
				int cx;
				int cy;
				int cxp;
				int cyp;
				double xfaktor;
				double yfaktor;
				double cxfaktor;
				double cyfaktor;
				int xspace;
				int yspace;

         public :
			    CHS ()
				{
					chsizex = 0;
					chsizey = 0;
					chposx  = 0;
					chposy  = 0;
				}

				void Setchsize (int chsizex, int chsizey)
				{
					this->chsizex = chsizex;
					this->chsizey = chsizey;
				}

				int Getchsizex ()
				{
					return chsizex;
				}

				int Getchsizey ()
				{
					return chsizey;
				}

				void Setchpos (int chposx, int chposy)
				{
					this->chposx = chposx;
					this->chposy = chposy;
				}

				int Getchposx ()
				{
					return chposx;
				}
				int Getchposy ()
				{
					return chposy;
				}

				void SetValues (int x, int y, int cx, int cy)
				{
					this->x  = x;
					this->y  = y;
					this->cx = cx;
					this->cy = cy;
				}

				void SetPValues (int cxp, int cyp)
				{
					this->cxp = cxp;
					this->cyp = cyp;
				}

				void SetSize (HWND hWndMain)
				{
					RECT rect;

					cxfaktor = cyfaktor = 0.0;
					GetClientRect (hWndMain, &rect);
					cxp = rect. right;
					cyp = rect.bottom;

					if (cxp)
					{
						  xfaktor  = (double) x / cxp;
					      cxfaktor = (double) cx / cxp;
						  xspace = cxp - x;
					}

					if (cyp)
					{
						  yfaktor  = (double) y / cyp;
					      cyfaktor = (double) cy / cyp;
						  yspace = cyp - y;
					}

				}

				void NewSize (HWND hWndMain)
				{
					RECT rect;
					int cxp0;
					int cyp0;

					cxp0 = cxp;
					cyp0 = cyp;
					GetClientRect (hWndMain, &rect);
					cxp = rect. right;
					cyp = rect.bottom;
 
					if (chsizex == 1)
					{
					          cx = (int) (double) ((double) cxp * cxfaktor);
					}
					else if (chsizex == 2)
					{
						      cx = cx + cxp - cxp0;
					}
					if (chsizey == 1)
					{
					          cy = (int) (double) ((double) cyp * cyfaktor);
					}
					else if (chsizey == 2)
					{
						      cy = cy + cyp - cyp0;
					}
				}


				void NewPos (HWND hWndMain)
				{
					RECT rect;
					int cxp0;
					int cyp0;

					cxp0 = cxp;
					cyp0 = cyp;

					GetClientRect (hWndMain, &rect);
					cxp = rect. right;
					cyp = rect.bottom;

//					GetWindowRect (hWndMain, &rect);
//					cxp = rect. right - rect.left;
//					cyp = rect.bottom - rect.top;

					if (chposx == 1)
					{
					         x = (int) (double) ((double) cxp  * xfaktor);
					}
					else if (chposx == 2)
					{
						     x = x + cxp - cxp0;
						     x = cxp - xspace;
					}
					if (chposy == 1)
					{
					         y = (int) (double) ((double) cyp  * yfaktor);
					}
					else if (chposy == 2)
					{
						     y = cyp - yspace;
					}
				}

				int GetX (void)
				{
					return x;
				}

				int GetY (void)
				{
					return y;
				}

				int GetCX (void)
				{
					return cx;
				}

				int GetCY (void)
				{
					return cy;
				}
};


class CFIELD
{
          private :
                void *feld;
                void *checkboxfeld;
				int start;
				int maxstart;
				int minstart;
				int xstart;
				int xmaxstart;
				int xminstart;
				char *name;
				int length;
				int cx;
                int cy;
				int x;
				int y;
				int xorg;
				int yorg;
				int cxp;
				int cxorg;
				int cyorg;
                int cyp;
				int xp;
				int yp;
                int xpplus;
                int ypplus;
                BOOL xset;
                BOOL yset;
				HWND hWndcl;
				HWND hWnd;
				HWND hWndMain;
				char *picture;
				DWORD attribut;
				DWORD ID;
				MFONT *mFont;
				HFONT hFont;
				HFONT oldfont;
				MFONT *PosmFont;
				HFONT PoshFont;
				int   UpMax;
				int   UpMin;
				int   UpValue;
				BOOL  UpDown;
				BOOL  Pixel;
				BOOL  Pixelorg;
				BOOL  AbsPos;
				int BkMode;
				int BkModeEx;
				int (*beforeprog) (void);
				int (*afterprog) (void);
                int (*beforecreate) (void);
                void (*sortproc) (int, HWND);
				CHS  Chs;
				IMG **Image;
                BOOL PosSet;
                BORDER *Border;
                BOOL Disabled;
                HWND QuikInfo;
                BOOL NewPosNeeded;
                HBITMAP bitmap;
                BOOL tabstop;
                BOOL tabstopex;
                char **Combobox;
                BOOL Shadow;
                COLORREF ShadowColor;
                int shx;
                int shy;
                BOOL SortEnable;
                char *RowAttr;
          public :
                static int fspace;
                static int fspaceh;
                static BOOL DecimalIsKomma;
			    CFIELD (char *name,
					    void *feld, int cx, int cy, int x, int y, HWND hWnd, 
					    char *picture,
						DWORD attribut,
					    DWORD ID, 
					    MFONT *mFont,
						BOOL Pixel,
						int BkMode)
				{
					beforeprog = NULL;
					afterprog = NULL;
					beforecreate = NULL;
					sortproc = NULL;
					SortEnable = FALSE;
                    checkboxfeld = "N";
                    RowAttr = NULL;
                    BkModeEx = WS_EX_CLIENTEDGE;
			        Init (name, feld, cx, cy, x, y, hWnd, picture, attribut,ID, 
                          mFont,Pixel,BkMode);
				}

			    CFIELD (char *name,
					    void *feld, int cx, int cy, int x, int y, HWND hWnd, 
					    char *picture,
						DWORD attribut,
					    DWORD ID, 
					    MFONT *mFont,
						BOOL Pixel,
						int BkMode,
                        int BkModeEx)
				{
					beforeprog = NULL;
					afterprog = NULL;
					beforecreate = NULL;
					sortproc = NULL;
					SortEnable = FALSE;
                    checkboxfeld = "N";
                    RowAttr = NULL;
                    this->BkModeEx = BkModeEx;
			        Init (name, feld, cx, cy, x, y, hWnd, picture, attribut,ID, 
                          mFont,Pixel,BkMode);
				}

			    CFIELD (char *name,
					    void *feld, char *checkboxfeld,
                        int cx, int cy, int x, int y, HWND hWnd, 
					    char *picture,
						DWORD attribut,
					    DWORD ID, 
					    MFONT *mFont,
						BOOL Pixel,
						int BkMode)
				{
					beforeprog = NULL;
					afterprog = NULL;
					beforecreate = NULL;
					sortproc = NULL;
                    RowAttr = NULL;
                    BkModeEx = WS_EX_CLIENTEDGE;
                    this->checkboxfeld = checkboxfeld;
			        Init (name, feld, cx, cy, x, y, hWnd, picture, attribut,ID, 
                          mFont,Pixel,BkMode);
				}

			    CFIELD (char *name,
					    void *feld, int cx, int cy, int x, int y, HWND hWnd, 
					    char *picture,
						DWORD attribut,
					    DWORD ID, 
					    MFONT *mFont,
						BOOL Pixel,
						int BkMode,
						int (*beforeprog) (void),
						int (*afterprog) (void))
				{
					this->beforeprog = beforeprog;
					this->afterprog  = afterprog;
					beforecreate = NULL;
					sortproc = NULL;
					SortEnable = FALSE;
                    checkboxfeld = "N";
                    RowAttr = NULL;
                    BkModeEx = WS_EX_CLIENTEDGE;
			        Init (name, feld, cx, cy, x, y, hWnd, picture, attribut,ID, 
                          mFont,Pixel,BkMode);
                }

			    void Init (char *name,
					    void *feld, int cx, int cy, int x, int y, HWND hWnd, 
					    char *picture,
						DWORD attribut,
					    DWORD ID, 
					    MFONT *mFont,
						BOOL Pixel,
						int BkMode)
                {
					Image = NULL;
					this->name     = name;
					this->feld     = feld;
					this->cx       = cx;
					this->cy       = cy;
					this->x        = x;
					this->y        = y;
					xorg           = x;
                    yorg           = y;
					cxorg          = cx;
                    cyorg          = cy;
					this->hWnd     = hWnd;
					this->picture  = picture;
					this->attribut = attribut;
                    this->ID       = ID;
					this->mFont    = mFont;
					this->hFont    = NULL;
					this->Pixel    = Pixel;
					this->Pixelorg = Pixel;
					if (Pixel == 0)
					{
						length = cx;
					}
                    this->BkMode   = BkMode;
					this->AbsPos = FALSE;
					start = 0;
					maxstart = 9999;
					minstart = -9999;
					xstart = 0;
					xmaxstart = 9999;
					xminstart = -9999;
					hWndMain = NULL;
					UpDown = FALSE;
                    PosmFont = mFont;
                    PosSet = FALSE;
                    Border = new BORDER ();
                    if (BkMode & DISABLED)
                    {
                        Disabled = TRUE;
                    }
                    else
                    {
                        Disabled = FALSE;
                    }
                    QuikInfo = NULL;
                    NewPosNeeded = TRUE;
                    bitmap = NULL;
					switch (this->attribut)
					{
					     case CEDIT :
                         case CRIEDIT :
						 case CBUTTON :
						 case CCOLBUTTON :
						 case CLISTBOX :
						 case CCOMBOBOX :
                                tabstop = TRUE;
                                tabstopex = TRUE;
								break;
						 default :
							    tabstop = FALSE;
							    tabstopex = FALSE;
								break;
					}
                    Combobox = NULL;
                    xpplus = 0;
                    ypplus = 0;
                    Shadow = FALSE;
                    ShadowColor = BLACKCOL;
                    shy = shx = -10;
                    xset = yset = FALSE;
                    hFont = NULL;
                    PoshFont = NULL;
                    oldfont = NULL;
                }

                ~CFIELD ()
                {

                    delete Border;
                    if (hWnd != NULL)
                    {
                         HDC hdc = GetDC (hWnd);
                         SelectObject (hdc, oldfont);
                         ReleaseDC (hWnd, hdc);
                         if (PoshFont != hFont)
                         {
                              if (PoshFont != NULL)
                              {
                                  DeleteObject (PoshFont);
                              }
                         }
                         if (hFont != NULL)
                         {
                             DeleteObject (hFont);
                         }
                    }
                }

                void SetRowAttr (char *RowAttr)
                {
                    this->RowAttr = RowAttr;
                }

                char *GetRowAttr (void)
                {
                    return RowAttr;
                }

                void SetSortProc (void (*sortproc) (int, HWND))
                {
                    this->sortproc = sortproc;
                }

                void EnableSort (BOOL b)
                {
                    SortEnable = b;
                }

                void SetShadow (BOOL b)
                {
                    Shadow = b;
                }

                void SetShadowColor (COLORREF Col)
                {
                    ShadowColor = Col;
                }
                void SetShadowPos (int shx, int shy)
                {
                    this->shx = shy;
                    this->shy = shy;
                }


                void SetXOK (BOOL b)
                {
                    xset = b;
                }

                void SetYOK (BOOL b)
                {
                    yset = b;
                }

                void SetCombobox (char **Combobox)
                {
                    if (this == NULL) return;
                    this->Combobox = Combobox;
                }

                void SetBitmap (HBITMAP bitmap)
                {
                    if (this == NULL) return;
                    this->bitmap = bitmap;
                }

                void SetTabstop (BOOL b)
                {
                    tabstop = b;
                    tabstopex = b;
                }

                BOOL GetTabstop (void)
                {
                    return tabstop;
                }

                void SetTabstopEx (BOOL b)
                {
                    tabstopex = b;
                }

                BOOL GetTabstopEx (void)
                {
                    return tabstopex;
                }

                BOOL IsDisabled (void)
                {
                    return Disabled;
                }

                void SetBorder (BORDER * Border)
                {
                    if (this == NULL) return;
                    this->Border = Border;
                }

                void SetBorder (COLORREF Col, COLORREF HiCol, int Type)
                {
                    if (this == NULL) return;
                    if (Border != NULL)
                    {
                        delete Border;
                    }
                    Border = new BORDER (Col, HiCol, Type);
                }

                void SetBorder (COLORREF Col, COLORREF HiCol, int Type, int Width)
                {
                    if (this == NULL) return;
                    if (Border != NULL)
                    {
                        delete Border;
                    }
                    Border = new BORDER (Col, HiCol, Type, Width);
                }

                BORDER * GetBorder (void)
                {
                    return Border;
                }

                void EnablePosSet (void)
                {
                    if (this == NULL) return;
                    PosSet = FALSE;
                }

				void SethWndMain (HWND MainWindow)
				{
                     if (this == NULL) return;
					 hWndMain = MainWindow;
				}

				HWND GethWndMain (void)
				{
				    return hWndMain;
				}

				int GetStart (void)
				{
					return this->start;
				}

				int xGetStart (void)
				{
					return this->xstart;
				}

				void SetLength (int length)
				{
                    if (this == NULL) return;
					this->length = length;
				}

				void SetName (char *name)
				{
                    if (this == NULL) return;
					this->name = name;
				}

				void SetUpDown (BOOL mode, int max, int min, int value)
				{
                    if (this == NULL) return;
					UpDown = mode;
					UpMax  = max;
					UpMin  = min;
                    UpValue = value;
				}

				void SetCX (int cx)
				{
                    if (this == NULL) return;
					this->cx = cx;
					this->cxorg = cx;
				}

				void SetCY (int cy)
				{
					this->cy = cy;
					this->cyorg = cy;
				}


                int GetXorg (void)
                {
                    return xorg;
                }

                int GetYorg (void)
                {
                    return yorg;
                }

                int GetCXorg (void)
                {
                    return cxorg;
                }

                int GetCYorg (void)
                {
                    return cyorg;
                }

				void SetPX (int x)
				{
                    if (this == NULL) return;
					this->x = x;
				}

				void SetX (int x)
				{
                    if (this == NULL) return;
					this->x = x;
					this->xorg = x;
				}

				void SetY (int y)
				{
                    if (this == NULL) return;
					this->y = y;
					this->yorg = y;
				}

				void SethWnd (HWND hWnd)
				{
					this->hWnd = hWnd;
				}

				void SetAttribut (DWORD attribut)
				{
					this->attribut = attribut;
                    if (attribut == CDISPLAYONLY ||
                        attribut == CREADONLY ||
                        attribut == CREMOVED)
                    {
                        SetTabstop (FALSE);
                    }
				}

				void SetPicture (char *picture)
				{
					this->picture = picture;
				}

				void SetBkMode (int BkMode)
				{
                    this->BkMode   = BkMode;
				}

				void SetBkModeEx (int BkModeEx)
				{
                    this->BkModeEx   = BkModeEx;
				}

				void SetID (DWORD ID)
				{
					this->ID = ID;
				}

				void SetFont (MFONT *mFont)
				{
                    if (this == NULL) return;
					this->mFont = mFont;
				}

				void SetPosFont (MFONT *PosmFont)
				{
					this->PosmFont = PosmFont;
				}

				char *GetName (void)
				{
					return name;
				}

				BOOL GetUpDown (void)
				{
					return UpDown;
				}

				char *GetFeld (void)
				{
					return (char *) feld;
				}

                void SetCheckboxFeld (char *checkboxfeld)
                {
                       this->checkboxfeld = checkboxfeld;
                }

                void *GetCheckboxFeld (void)
                {
                       return checkboxfeld;
                }

 			    int GetLength (void)
				{
					return this->length;
				}

				void SetImage (IMG **img)
				{
					Image = img;
				}

				IMG **GetImage (void)
				{
					return Image;
				}

                void SetNewPos (BOOL b);
				char *GetFeld (DWORD);
				BOOL SetImage (IMG **, DWORD);
				IMG **GetImage (DWORD);
				void TimeFormat (void);
				void DatFormat (void);
				void ToFormat (void);
				int GetCX (void);
				int GetCY (void);
				int GetX (void);
				int GetY (void);
				int GetXP (void);
				int GetYP (void);
				int GetCXP (void);
				int GetCYP (void);

				void Setchsize (int, int);
				void Setchpos (int, int);
				int GetWidthText (void);
				int GetHeightText (void);
				int GetWidth (void);
				int GetHeight (void);
                void StartPlus (int plus);
                void StartMinus (int minus);
                void xStartPlus (int);
                void xStartMinus (int);
				void SetMaxStart (int);
				void SetMinStart (int);
				void SetStart (int);
				void xSetMaxStart (int);
				void xSetMinStart (int);
				void xSetStart (int);
                void MoveToFoot (HWND);
				void Move (void);
				void MoveWindow (void);

				DWORD GetAttribut (void)
				{
					return attribut;
				}

				DWORD GetBkMode (void)
				{
					return BkMode;
				}

				DWORD GetBkModeEx (void)
				{
					return BkModeEx;
				}

				char *GetPicture (void)
				{
					return picture;
				}

				HWND GethWnd (void)
				{
					return hWnd;
				}

				DWORD GetID (void)
				{
					return ID;
				}

				MFONT *GetFont (void)
				{
					return mFont;
				}

				void SetBefore (int (*beforeprog) (void))
				{
					this->beforeprog = beforeprog;
				}

				void SetAfter (int (*afterprog) (void))
				{
					this->afterprog = afterprog;
				}

				void SetBeforeCreate (int (*beforecreate) (void))
				{
					this->beforecreate = beforecreate;
				}

				virtual int before (void)
				{
					if (beforeprog)
					{
					         return (*beforeprog) ();
					}
					return 0;
				}

				virtual int after (void)
				{
					if (afterprog)
					{
					         return (*afterprog) ();
					}
					return 0;
				}

                int GetLine (void);
				int dobefore (DWORD);
				int dobefore (char *);
				int doafter (DWORD);
				int doafter (char *);
                int GetNextPos (HWND);
			    void SetAbsPos (void);
			    void SetAbsPosX (int);
			    void SetAbsPosY (int);
			    void SetAbsPos (int, int);
				DWORD GetFirstID (void);
				int NextField (int);
			    int PriorField (int);
                int GetComboPos (void);
                void SetPosCombo (int);
                void FillComboBox (void);
                void SetLimitText (int);
                void SetText ();
                void GetText ();
		        void Invalidate (BOOL);
   			    void display (HWND,HDC);
   			    void display (HWND);
   			    void display (void);
				void enter (void);
				void displayborder (HWND, HDC);
				void displaytxt (HWND, HDC);
				void displaybu (HWND, HDC);
				void displaystatic (HWND, HDC);
				void displayedit (HWND, HDC);
				void displayredit (HWND, HDC);
				void displaycombobox (HWND, HDC);
				void displaycolbu (HWND, HDC);
				void displayrdonly (HWND, HDC);
				void displaycp (HWND, HDC);
				void displaylbox (HWND, HDC);
				void destroy ();
                void CreateQuikInfos (HWND);
 		        BOOL IsID (DWORD);
 		        CFIELD *IsCfield (DWORD);
 		        CFIELD *IsCfield (char *);
 		        char *IsCurrentID (DWORD);
 			    DWORD IsCurrentName (char *);
 			    DWORD IsCurrenthWnd (HWND);
 		        BOOL SetFocusID (DWORD);
 			    BOOL SetFocusName (char *);
				void SetFocus (void);
				void SetSel (void);
		        BOOL EnableID (DWORD, BOOL);
 			    BOOL EnableName (char *, BOOL);
				void Enable (BOOL);
		        BOOL CheckID (DWORD, BOOL);
 			    BOOL CheckName (char *, BOOL);
				void Check (BOOL); 
			    HWND GethWndID (DWORD);
				HWND GethWndName (char *);
                DWORD GetAttrhWnd (HWND hWnd);
				void SetFeld (char *feld);
				BOOL SetFeld (char *feld, DWORD ID);
				BOOL SetUpDown (BOOL, int, int, int, DWORD);
				void Update (void);
                void SetFieldPicture (char *, char *);
                void SetFieldBkMode (char *, int);
                void Setxpplus (int);
                void Setypplus (int);
                void CallInfo (void);
                LRESULT SendMessage( UINT, WPARAM, LPARAM);
                BOOL PostMessage( UINT, WPARAM, LPARAM);
};


class CFORM
{
			   private :
				   int fieldanz;
				   CFIELD **cfield;
				   int start;
				   int maxstart;
				   int minstart;
				   int xstart;
				   int xmaxstart;
				   int xminstart;
				   int currentfield;
				   MFONT *mFont;
				   HFONT hFont;
				   HWND hWndMain;
				   int layout;
                   BOOL NewPosNeeded;
			   public :
                   static int fspace;
                   static int fspaceh;
                   static int bottomspace;

				   CFORM (int fieldanz, CFIELD **cfield) : fieldanz (fieldanz),
														  cfield (cfield),
														  currentfield (0)
				   {
					start = 0;
					maxstart = 9999;
					minstart = -9999;
					xstart = 0;
					xmaxstart = 9999;
					xminstart = -9999;
					mFont = NULL;
					hFont = NULL;
					layout = POS;
                    NewPosNeeded = TRUE;
                    CFIELD::fspace  = fspace;
                    CFIELD::fspaceh = fspaceh;

				   }

                   int before (DWORD);
                   int before (char *);
				   int after (DWORD);
				   int after (char *);
 
				   void Invalidate (BOOL mode)
				   {
					   RECT rect;
					   RECT rect0;

                       if (hWndMain == NULL) return;
					   GetClientRect (hWndMain, &rect0);
					   GetRectP (&rect);
					   rect.left  = rect0.left;
					   rect.right = rect0.right;
					   InvalidateRect (hWndMain, &rect, mode);
//					   InvalidateRect (hWndMain, NULL, mode);
                      
				   }
                     
				   void SethWndMain (HWND MainWindow)
				   {
					 hWndMain = MainWindow;
				   }

				   HWND GethWndMain (void)
				   {
					  return hWndMain;
				   }
				
				   void SetFont (MFONT *mFont)
				   {
				 	   this->mFont = mFont;
				   }

				   MFONT *GetFont (void)
				   {
                       if (mFont)
					   {
				 	         return mFont;
					   }
					   return cfield[0]->GetFont ();
				   }

				   void SetLayout (int layout)
				   {
					   this->layout = layout;
				   }


				   char  * GetFeld (int current)
				   {
					   return cfield[current]->GetFeld ();
				   }

				   void SetCurrent (int current)
				   {
					   currentfield = current;
				   }
				   int GetCurrent (void)
				   {
					   return currentfield;
				   }

				   void SetFieldanz (int fieldanz)
				   {
					   this->fieldanz = fieldanz;
				   }

				   void SetCfield (CFIELD **cfield)
				   {
					   this->cfield = cfield;
				   }

				   int GetFieldanz (void)
				   {
					   return fieldanz;
				   }

				   CFIELD **GetCfield (void)
				   {
					   return cfield;
				   }

                   void SetNewPos (BOOL b);
				   char *GetFeld (DWORD);
				   void Setchsize (DWORD, int, int);
				   void Setchpos (DWORD, int, int);

				   void Setchsize (char *, int, int);
				   void Setchpos (char *, int, int);

				   void Setchsize (int, int);
				   void Setchpos (int, int);

				   void SetMaxStart (int);
				   void SetMinStart (int);
 				   void SetStart (int);
				   int GetStart (void);
                   void StartPlus (int);
                   void StartMinus (int);
				   void xSetMaxStart (int);
				   void xSetMinStart (int);
				   void xSetStart (int);
				   int xGetStart (void);
                   void xStartPlus (int);
                   void xStartMinus (int);
				   int GetTm (MFONT *, HWND hWnd);
				   int xGetTm (MFONT *, HWND hWnd);
				   void TmStartPlus (int, MFONT *, HWND);
				   void TmStartMinus (int, MFONT *, HWND);
				   void xTmStartPlus (int, MFONT *, HWND);
				   void xTmStartMinus (int, MFONT *, HWND);
                   void MoveToFoot (HWND);
				   void Move (void);
				   void MoveWindow (void);

				   DWORD GetFirstID (void);
				   void SetFocus (void);
				   void SetSel (void);
				   int SetFirstFocus (void);
				   int NextField (void);
				   int PriorField (void);
				   int NextFormField (void);
				   int PriorFormField (void);
   			       void SetAbsPosX (int);
 			       void SetAbsPosY (int);
 			       void SetAbsPos (int, int);
				   void Centx (HWND);
				   void Centy (HWND);
				   void CentX (HWND);
				   void CentY (HWND);
				   void Rightx (HWND);
				   void Bottomy (HWND);
				   void Centx (HWND, int);
				   void Centy (HWND, int);
				   void Rightx (HWND, int);
				   void Bottomy (HWND, int);
				   void display (HWND, HDC);
				   void display (void);
				   void enter ();
				   void displaytxt (HWND, HDC);
  				   void destroy ();
				   void GetRectText (int *, int *);
				   void GetRect (int *, int *);
				   void GetRect (RECT *);
				   void GetRectP (RECT *);
				   BOOL SetImage (IMG **, DWORD);
				   IMG **GetImage (DWORD);
 		           BOOL IsID (DWORD);
 			       BOOL SetCurrentID (DWORD);
 			       BOOL SetCurrentName (char *);
 			       BOOL SetCurrentFieldID (DWORD);
 			       BOOL EnableID (DWORD, BOOL);
 			       BOOL EnableName (char *, BOOL);
 			       BOOL CheckID (DWORD, BOOL);
 			       BOOL CheckName (char *, BOOL);
				   DWORD GetID (char *);
				   DWORD GetID (HWND);
				   char *GetName (DWORD);
				   HWND GethWndID (DWORD);
				   HWND GethWndName (char *);
				   void Update (int);
				   void Update (void);
                   void SetText (void);
                   void GetText (void);
				   void SetFeld (char * feld, int current);
				   BOOL SetFeld (char * feld, DWORD ID);
				   BOOL SetUpDown (BOOL mode,  int, int, int, DWORD ID);
				   DWORD GetAttribut (void);
				   CFIELD *GetCfield (DWORD);
				   CFIELD *GetCfield (char *);
                   void SetFieldPicture (char *, char *);
                   void SetFieldBkMode (char *, int);
                   void SetFieldanz (void);
                   void Setxpplus (int);
                   void Setypplus (int);
                   void ScrollInsert (int);
                   BOOL InsertCfield (CFIELD *, char *);
                   BOOL InsertCfieldAt (CFIELD *, char *);
                   void ScrollRemove (int);
                   BOOL RemoveCfield (char *);
};


class WFORM
{
          private :
               form *wFrm;
			   HWND whWnd;
               CFORM *cwFrm;               
          public :     
			   WFORM ()
			   {
			   }
			   void SetwForm (form *frm)
			   {
				   wFrm = frm;
			   }
			   form * GetwForm (void)
			   {
				   return wFrm;
			   }
			   void SetcwForm (CFORM *cfrm)
			   {
				   cwFrm = cfrm;
			   }
			   CFORM *GetcwForm (void)
			   {
				   return cwFrm;
			   }
			   void ProcessMessages (void);
               static void ShowDlg (HDC, form *);
               static CALLBACK Proc(HWND,UINT, WPARAM,LPARAM);
               static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
};
	     
class MESSCLASS : public WFORM
{
          private :
			   int tlines;
			   HWND hMainWindow;
			   HANDLE hMainInst;
          public :
			  MESSCLASS () : WFORM ()
               {
               }
               HWND OpenWindow (HANDLE, HWND, char **, mfont *);
               void MessageText (HANDLE, HWND, char **, mfont *);
               void DestroyText (void);
               HWND COpenWindow (HANDLE, HWND, char **, mfont **);
               HWND COpenWindowF (HANDLE, HWND, CFORM *);
               void CMessageText (HANDLE, HWND, char **, mfont **, mfont *);
               void CMessageTextOK (HANDLE, HWND, char **, mfont **, BOOL);
//               void CMessageTextOK (HANDLE, HWND, char **, mfont **, mfont *, mfont *);
               void CMessageTextF (HANDLE, HWND, CFORM *, BOOL);
               void CDestroyText (void);
               void CDestroyTextF (void);
};
#endif // _CMASK_DEF