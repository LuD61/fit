#ifndef _FORM_FELD_DEF
#define _FORM_FELD_DEF

struct FORM_FELD {
   char      agg_funk[2];
   char      feld_id[2];
   char      feld_nam[19];
   char      feld_typ[2];
   short     delstatus;
   char      form_nr[6];
   long      lfd;
   char      tab_nam[19];
};

struct FORM_HEAD {
   short     delstatus;
   char      form_nr[6];
   char      form_ube[129];
   char      form_vers[4];
   char      form_distinct[2];
   char      lst_par[21];
   char      sort[25];
   short     zei_bis;
   short     zei_von;
   short     zei_von1;
   short     zei_bis1;
};

extern struct FORM_FELD _form_feld;

class FORMFELD_CLASS
{
       private :
            short cursor_formfeld;
            short cursor_feld;

       public:
           FORMFELD_CLASS ()
           {
                     cursor_formfeld = -1;
                     cursor_feld = -1;
           }
           int lese_formfeld (char *);
           int lese_formfeld (void);
           void erase_formfeld (void);
           int lese_feld (char *, char *, char *);
           void erase_feld (void);
};
// jg: braucht Kurt auch noch
struct FORM_FRM {
   char      form_nr[6];
};

extern struct FORM_FRM _form_frm;

class FORMFRM_CLASS
{
       private :
            short cursor_formfrm;

       public:
           FORMFRM_CLASS ()
           {
                     cursor_formfrm = -1;
           }
           int lese_formfrm (char *);
           int lese_formfrm (void);
           void erase_formfrm (void);
};

extern struct FORM_HEAD form_head, form_head_null;

class FORMHEAD_CLASS
{
       private :
            short cursor;
            void prepare (void);

       public:
           FORMHEAD_CLASS ();
           int dbreadfirst ();
           int dbread (void);
           int dbclose (void);
};

#endif