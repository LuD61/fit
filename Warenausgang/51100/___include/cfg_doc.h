#ifndef _CFG_DOC_DEF
#define _CFG_DOC_DEF
#include "dbclass.h"

struct CFG_DOC {
   char      programm[21];
   char      item[21];
   char      text[257];
   char      values[513];
   char      help[513];
};
extern struct CFG_DOC cfg_doc, cfg_doc_null;

#line 6 "cfg_doc.rh"

class CFG_DOC_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               CFG_DOC_CLASS () : DB_CLASS ()
               {
               }
};
#endif
