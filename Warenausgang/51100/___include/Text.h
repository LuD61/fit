#ifndef TEXT_DEF
#define TEXT_DEF
#include <string.h> 
#include <stdarg.h> 
#include <windows.h> 

class Text
{
     char *Buffer;
     int Len;
     char *Sub;
  public :
     int GetLen (void)
     {
         return Len;
     }

     int GetLength (void)
     {
         return Len;
     }

     Text ();
     Text (char *);
     Text (Text&);
     ~Text ();
     char *GetBuffer (void);
     void SetBuffer (char *);
     const Text& operator=(char *);
     const Text& operator=(int);
     const Text& operator=(Text&);

     Text& operator+ (char *);
     Text& operator+= (char *);
     Text& operator+ (Text&);
     Text& operator+= (Text&);
     BOOL operator== (Text&);
     BOOL operator== (char *);
     BOOL operator!= (Text&);
     BOOL operator!= (char *);
     BOOL operator> (Text&);
     BOOL operator> (char *);
     BOOL operator< (Text&);
     BOOL operator< (char *);
     char operator[] (int);
     void TrimLeft (void);
     void TrimRight (void);
     void Trim (void);
     char *SubString (int, int);
     void Format (char *format, ...);
     static BOOL matchcomp (LPSTR, LPSTR);
     BOOL CompareMatch (Text &);
     void MakeLines (int);
     void MakeUpper ();
     int Find (LPCSTR);
     static char *TrimRight (char *);
};

class Token
{
  private :
     Text Buffer;
     Text **Tokens;
     Text sep;
     int AnzToken;
     int AktToken;
  public :
     Token ();
     Token (char *, char *);
     Token (Text&, char *);
     ~Token ();
     const Token& operator=(char *);
     const Token& operator=(Text&);
     void GetTokens (char *);
     void SetSep (char *);
     char * NextToken (void);
     char * GetToken (int);
     int GetAnzToken (void);
};
#endif
