#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "dbclass.h"
#include "hwg.h"

struct HWG hwg, hwg_null;

void HWG_CL::prepare (void)
{
            char *sqltext;

            sqlin ((long *)   &hwg.hwg,  SQLSHORT, 0);
    out_quest ((char *) &hwg.hwg,1,0);
    out_quest ((char *) hwg.hwg_bz1,0,25);
    out_quest ((char *) hwg.hwg_bz2,0,25);
    out_quest ((char *) hwg.sp_kz,0,2);
    out_quest ((char *) &hwg.we_kto,2,0);
    out_quest ((char *) &hwg.erl_kto,2,0);
    out_quest ((char *) hwg.durch_vk,0,2);
    out_quest ((char *) hwg.durch_sk,0,2);
    out_quest ((char *) hwg.durch_fil,0,2);
    out_quest ((char *) hwg.durch_sw,0,2);
    out_quest ((char *) hwg.best_auto,0,2);
    out_quest ((char *) &hwg.sp_vk,3,0);
    out_quest ((char *) &hwg.sp_sk,3,0);
    out_quest ((char *) &hwg.sp_fil,3,0);
    out_quest ((char *) &hwg.sw,3,0);
    out_quest ((char *) &hwg.me_einh,1,0);
    out_quest ((char *) &hwg.mwst,1,0);
    out_quest ((char *) &hwg.teil_smt,1,0);
    out_quest ((char *) &hwg.bearb_lad,1,0);
    out_quest ((char *) &hwg.bearb_fil,1,0);
    out_quest ((char *) &hwg.bearb_sk,1,0);
    out_quest ((char *) hwg.sam_ean,0,2);
    out_quest ((char *) hwg.smt,0,2);
    out_quest ((char *) hwg.kost_kz,0,3);
    out_quest ((char *) &hwg.pers_rab,3,0);
    out_quest ((char *) &hwg.akv,2,0);
    out_quest ((char *) &hwg.bearb,2,0);
    out_quest ((char *) hwg.pers_nam,0,9);
    out_quest ((char *) &hwg.delstatus,1,0);
    out_quest ((char *) &hwg.erl_kto_1,2,0);
    out_quest ((char *) &hwg.erl_kto_2,2,0);
    out_quest ((char *) &hwg.erl_kto_3,2,0);
    out_quest ((char *) &hwg.we_kto_1,2,0);
    out_quest ((char *) &hwg.we_kto_2,2,0);
    out_quest ((char *) &hwg.we_kto_3,2,0);
            cursor = sqlcursor ("select hwg.hwg,  hwg.hwg_bz1,  "
"hwg.hwg_bz2,  hwg.sp_kz,  hwg.we_kto,  hwg.erl_kto,  hwg.durch_vk,  "
"hwg.durch_sk,  hwg.durch_fil,  hwg.durch_sw,  hwg.best_auto,  hwg.sp_vk,  "
"hwg.sp_sk,  hwg.sp_fil,  hwg.sw,  hwg.me_einh,  hwg.mwst,  hwg.teil_smt,  "
"hwg.bearb_lad,  hwg.bearb_fil,  hwg.bearb_sk,  hwg.sam_ean,  hwg.smt,  "
"hwg.kost_kz,  hwg.pers_rab,  hwg.akv,  hwg.bearb,  hwg.pers_nam,  "
"hwg.delstatus,  hwg.erl_kto_1,  hwg.erl_kto_2,  hwg.erl_kto_3,  "
"hwg.we_kto_1,  hwg.we_kto_2,  hwg.we_kto_3 from hwg "

#line 18 "hwg.rpp"
                                  "where hwg = ?");
    ins_quest ((char *) &hwg.hwg,1,0);
    ins_quest ((char *) hwg.hwg_bz1,0,25);
    ins_quest ((char *) hwg.hwg_bz2,0,25);
    ins_quest ((char *) hwg.sp_kz,0,2);
    ins_quest ((char *) &hwg.we_kto,2,0);
    ins_quest ((char *) &hwg.erl_kto,2,0);
    ins_quest ((char *) hwg.durch_vk,0,2);
    ins_quest ((char *) hwg.durch_sk,0,2);
    ins_quest ((char *) hwg.durch_fil,0,2);
    ins_quest ((char *) hwg.durch_sw,0,2);
    ins_quest ((char *) hwg.best_auto,0,2);
    ins_quest ((char *) &hwg.sp_vk,3,0);
    ins_quest ((char *) &hwg.sp_sk,3,0);
    ins_quest ((char *) &hwg.sp_fil,3,0);
    ins_quest ((char *) &hwg.sw,3,0);
    ins_quest ((char *) &hwg.me_einh,1,0);
    ins_quest ((char *) &hwg.mwst,1,0);
    ins_quest ((char *) &hwg.teil_smt,1,0);
    ins_quest ((char *) &hwg.bearb_lad,1,0);
    ins_quest ((char *) &hwg.bearb_fil,1,0);
    ins_quest ((char *) &hwg.bearb_sk,1,0);
    ins_quest ((char *) hwg.sam_ean,0,2);
    ins_quest ((char *) hwg.smt,0,2);
    ins_quest ((char *) hwg.kost_kz,0,3);
    ins_quest ((char *) &hwg.pers_rab,3,0);
    ins_quest ((char *) &hwg.akv,2,0);
    ins_quest ((char *) &hwg.bearb,2,0);
    ins_quest ((char *) hwg.pers_nam,0,9);
    ins_quest ((char *) &hwg.delstatus,1,0);
    ins_quest ((char *) &hwg.erl_kto_1,2,0);
    ins_quest ((char *) &hwg.erl_kto_2,2,0);
    ins_quest ((char *) &hwg.erl_kto_3,2,0);
    ins_quest ((char *) &hwg.we_kto_1,2,0);
    ins_quest ((char *) &hwg.we_kto_2,2,0);
    ins_quest ((char *) &hwg.we_kto_3,2,0);
            sqltext = "update hwg set hwg.hwg = ?,  "
"hwg.hwg_bz1 = ?,  hwg.hwg_bz2 = ?,  hwg.sp_kz = ?,  hwg.we_kto = ?,  "
"hwg.erl_kto = ?,  hwg.durch_vk = ?,  hwg.durch_sk = ?,  "
"hwg.durch_fil = ?,  hwg.durch_sw = ?,  hwg.best_auto = ?,  "
"hwg.sp_vk = ?,  hwg.sp_sk = ?,  hwg.sp_fil = ?,  hwg.sw = ?,  "
"hwg.me_einh = ?,  hwg.mwst = ?,  hwg.teil_smt = ?,  hwg.bearb_lad = ?,  "
"hwg.bearb_fil = ?,  hwg.bearb_sk = ?,  hwg.sam_ean = ?,  hwg.smt = ?,  "
"hwg.kost_kz = ?,  hwg.pers_rab = ?,  hwg.akv = ?,  hwg.bearb = ?,  "
"hwg.pers_nam = ?,  hwg.delstatus = ?,  hwg.erl_kto_1 = ?,  "
"hwg.erl_kto_2 = ?,  hwg.erl_kto_3 = ?,  hwg.we_kto_1 = ?,  "
"hwg.we_kto_2 = ?,  hwg.we_kto_3 = ? "

#line 20 "hwg.rpp"
                                  "where hwg = ?";
            sqlin ((long *)   &hwg.hwg,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &hwg.hwg,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor ("select hwg from hwg "
                                  "where hwg = ?");
            sqlin ((long *)   &hwg.hwg,  SQLSHORT, 0);
            del_cursor = sqlcursor ("delete from hwg "
                                  "where hwg = ?");
    ins_quest ((char *) &hwg.hwg,1,0);
    ins_quest ((char *) hwg.hwg_bz1,0,25);
    ins_quest ((char *) hwg.hwg_bz2,0,25);
    ins_quest ((char *) hwg.sp_kz,0,2);
    ins_quest ((char *) &hwg.we_kto,2,0);
    ins_quest ((char *) &hwg.erl_kto,2,0);
    ins_quest ((char *) hwg.durch_vk,0,2);
    ins_quest ((char *) hwg.durch_sk,0,2);
    ins_quest ((char *) hwg.durch_fil,0,2);
    ins_quest ((char *) hwg.durch_sw,0,2);
    ins_quest ((char *) hwg.best_auto,0,2);
    ins_quest ((char *) &hwg.sp_vk,3,0);
    ins_quest ((char *) &hwg.sp_sk,3,0);
    ins_quest ((char *) &hwg.sp_fil,3,0);
    ins_quest ((char *) &hwg.sw,3,0);
    ins_quest ((char *) &hwg.me_einh,1,0);
    ins_quest ((char *) &hwg.mwst,1,0);
    ins_quest ((char *) &hwg.teil_smt,1,0);
    ins_quest ((char *) &hwg.bearb_lad,1,0);
    ins_quest ((char *) &hwg.bearb_fil,1,0);
    ins_quest ((char *) &hwg.bearb_sk,1,0);
    ins_quest ((char *) hwg.sam_ean,0,2);
    ins_quest ((char *) hwg.smt,0,2);
    ins_quest ((char *) hwg.kost_kz,0,3);
    ins_quest ((char *) &hwg.pers_rab,3,0);
    ins_quest ((char *) &hwg.akv,2,0);
    ins_quest ((char *) &hwg.bearb,2,0);
    ins_quest ((char *) hwg.pers_nam,0,9);
    ins_quest ((char *) &hwg.delstatus,1,0);
    ins_quest ((char *) &hwg.erl_kto_1,2,0);
    ins_quest ((char *) &hwg.erl_kto_2,2,0);
    ins_quest ((char *) &hwg.erl_kto_3,2,0);
    ins_quest ((char *) &hwg.we_kto_1,2,0);
    ins_quest ((char *) &hwg.we_kto_2,2,0);
    ins_quest ((char *) &hwg.we_kto_3,2,0);
            ins_cursor = sqlcursor ("insert into hwg (hwg,  "
"hwg_bz1,  hwg_bz2,  sp_kz,  we_kto,  erl_kto,  durch_vk,  durch_sk,  durch_fil,  "
"durch_sw,  best_auto,  sp_vk,  sp_sk,  sp_fil,  sw,  me_einh,  mwst,  teil_smt,  "
"bearb_lad,  bearb_fil,  bearb_sk,  sam_ean,  smt,  kost_kz,  pers_rab,  akv,  bearb,  "
"pers_nam,  delstatus,  erl_kto_1,  erl_kto_2,  erl_kto_3,  we_kto_1,  we_kto_2,  "
"we_kto_3) "

#line 31 "hwg.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 33 "hwg.rpp"
}
