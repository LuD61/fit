#include "sg.h"
#include "ptab.h"
#include "strfkt.h"

struct SG sg, sg_null;

void SG_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &sg.sg, 1, 0);
    out_quest ((char *) &sg.sg,1,0);
    out_quest ((char *) sg.sg_bez,0,32);
    out_quest ((char *) sg.sg_bezk,0,8);
    out_quest ((char *) &ptabn.ptwer1,0,8);
    out_quest ((char *) &ptabn.ptwer2,0,8);
            cursor = prepare_sql ("select ptabn.ptlfnr,  ptabn.ptbez, ptabn.ptbezk,  "
"ptabn.ptwer1,  ptabn.ptwer2 from ptabn "

                                  "where ptlfnr = ? and ptitem = \"minstaffgr\" and ptlfnr > 0 ");

    ins_quest ((char *) &sg.sg,1,0);
    ins_quest ((char *) sg.sg_bez,0,32);
    out_quest ((char *) sg.sg_bezk,0,8);
    ins_quest ((char *) &ptabn.ptwer1,0,8);
    ins_quest ((char *) &ptabn.ptwer2,0,8);
            sqltext = "update ptabn set ptabn.ptitem = \"minstaffgr\",  ptabn.ptbez = ?, ptabn.ptbezk = ?, "
"ptabn.ptwer1 = ?,  ptabn.ptwer2 = ? "

                                  "where ptitem = \"minstaffgr\" and ptlfnr = ? ";

            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &sg.sg, 1, 0);
            test_upd_cursor = prepare_sql ("select ptlfnr from ptabn "
                                  "where ptitem = \"minstaffgr\"  and ptlfnr = ?");

            ins_quest ((char *) &sg.sg, 1, 0);
            del_cursor = prepare_sql ("delete from ptabn "
                                  "where ptitem = \"minstaffgr\" and ptlfnr = ? ");

    ins_quest ((char *) &sg.sg,1,0);
    ins_quest ((char *) sg.sg_bez,0,32);
    ins_quest ((char *) sg.sg_bezk,0,8);
    ins_quest ((char *) &ptabn.ptwer1,0,8);
    ins_quest ((char *) &ptabn.ptwer2,0,8);
            ins_cursor = prepare_sql ("insert into ptabn (ptlfnr,  "
"ptbez, ptbezk, ptwer1,  ptwer2) "

                                      "values "
                                      "(?,?,?,?,?)"); 

}

int SG_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	int dsqlstatus = 0;
         if (cursor == -1)
         {
                this->prepare ();
         }
         dsqlstatus = this->DB_CLASS::dbreadfirst ();
		 if (dsqlstatus == 0)
		 {
			sg.min_bestellmenge = ratod(ptabn.ptwer1);
			sg.staffelung = ratod(ptabn.ptwer2);
		 }
		 else
		 {
			sg.min_bestellmenge = 0.0;
			sg.staffelung = 0.0;
		 }
         return dsqlstatus;
}
