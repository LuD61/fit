	// Font.h: Schnittstelle f�r die Klasse CFont.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FONT_H__8A076F81_F841_44EA_A169_69C7AC19862E__INCLUDED_)
#define AFX_FONT_H__8A076F81_F841_44EA_A169_69C7AC19862E__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CFont  
{
public:
	CFont();
	virtual ~CFont();
private:
	HFONT m_hFont;
public:
	int nHeight; 
 // logical height of font 
 	int nWidth; 
 // logical average character width 
	int nEscapement; 
 // angle of escapement 
	int nOrientation; 
 // base-line orientation angle 
	int fnWeight; 
 // font weight 
	DWORD fdwItalic; 
 // italic attribute flag 
	DWORD fdwUnderline; 
 // underline attribute flag 
	DWORD fdwStrikeOut; 
 // strikeout attribute flag 
	DWORD fdwCharSet; 
 // character set identifier 
	DWORD fdwOutputPrecision; 
 // output precision 
	DWORD fdwClipPrecision; 
 // clipping precision 
	DWORD fdwQuality; 
 // output quality 
	DWORD fdwPitchAndFamily; 
 // pitch and family 
	LPCTSTR lpszFace; 
 // pointer to typeface name string 

public:
	void Create (LPSTR name, int size, BOOL bold=FALSE, BOOL italic=FALSE, BOOL underline=FALSE);
	operator HFONT () const;
	void Attach (HFONT hFont);

};

#endif // !defined(AFX_FONT_H__8A076F81_F841_44EA_A169_69C7AC19862E__INCLUDED_)
