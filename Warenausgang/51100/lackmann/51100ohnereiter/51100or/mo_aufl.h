#ifndef _MO_AUFL_DEF
#define _MO_AUFL_DEF
#include "wmaskc.h"
#include "listcl.h"
#include "mo_intp.h"
#include "DllPreise.h"
#include "bsd.h"
#include "RowEvent.h"
#include "ControlPlugins.h"
#include "GrpPrice.h"
#include "ClientOrderThread.h"
#include "ThreadAction.h"
#include "MessageHandler.h"
#include "Ampel.h"
#include "cron_best.h"
#include "calcualtesmt.h"
#include "smtreader.h"
#include "waithandler.h"

#define MAXLEN 40

#define WM_STND_COMPLETE WM_USER+20
#define WM_STND_COM_FAILED WM_USER+21
#define WM_STND_RELOAD WM_USER+22

extern BOOL IsPartyService;

class CColBkName
{
private:
	char name[80];
	COLORREF Color;

public:
	LPSTR GetName ()
	{
		return name;
	}


	COLORREF GetColor ()
	{
		return Color;
	}

	CColBkName (LPSTR name, COLORREF Color)
	{
		memset (this->name, 0, sizeof (this->name));
		strncpy (this->name, name, sizeof (this->name) - 1);
		this->Color = Color;
	}
};

class AUFPLIST : public CRowEvent, CThreadAction, CSmtReader
{

            private :

// Interne Klassen
				class CStndHandler : public CMessageHandler
				{
				protected :
					AUFPLIST *Instance;
				public :
					void SetInstance (AUFPLIST *Instance)
					{
						this->Instance = Instance;
					}
				public:
					CStndHandler (DWORD msg) : CMessageHandler (msg)
					{
						Instance = NULL;
					}

					virtual BOOL Run (); 
				};
				CStndHandler *StndHandler;


				class CStndFailed : public CStndHandler
				{

				public:
					CStndFailed (DWORD msg) : CStndHandler (msg)
					{
						Instance = NULL;
					}
					virtual BOOL Run (); 
				};
				CStndFailed *StndFailed;


				class CStndReload : public CMessageHandler
				{
				protected :
					AUFPLIST *Instance;
				public :
					void SetInstance (AUFPLIST *Instance)
					{
						this->Instance = Instance;
					}
				public:
					CStndReload (DWORD msg) : CMessageHandler (msg)
					{
						Instance = NULL;
					}

					virtual BOOL Run (); 
				};
				CStndReload *StndReload;
				static CCalcualteSmt m_CalculateSmt;

                int mamainmax;
                int mamainmin;
                HWND    hwndTB;
                char *eingabesatz;
                unsigned char *ausgabesatz;
                int zlen;
                int feld_anz;
                int banz;
                PAINTSTRUCT aktpaint;
                char *SwSaetze[30000];
                int PageView;
                unsigned char DlgSatz [20 + MAXLEN + 2];
                FELDER *LstZiel;
                unsigned char *LstSatz;
                int Lstzlen;
                int Lstbanz;
                int SwRecs;
                int AktRow;
                int AktColumn;
                int scrollpos;
				static int splitls;
                TEXTMETRIC tm;
                char InfoCaption [80];
                int WithMenue;
                int WithToolBar;
                BOOL ListAktiv;
				static HWND hMainWindow;
				int Posanz;
				int liney;
				static short auf_art;
                HMENU hMenu;
				CDataCollection<CColBkName *> ColBkNames;
				static CWaitHandler *waitHandler;
				static int posRecs;
				static CRON_BEST_CLASS CronBest;
				static CClientOrderThread ClientOrder;
				static CControlPlugins pControls;
				static CGrpPrice GrpPrice;
				static int PosTxtKz;
                static BOOL Muster;
				static CBsd Bsd;
				static BOOL TxtByDefault;
				static BOOL PosTxtNrPar;
				static AUFPLIST *instance;

             public :
				enum 
				{
					Standard = 1,
                    Voll = 2,
				};

				CAmpel Ampel;
				static CClientOrder::STD_RDOPTIMIZE StndRdOptimize;
				static BOOL RefreshStnd;
				static CDllPreise DllPreise;
                static void SetMax0 (int);
                static void SetMin0 (int);
                static BOOL AErsInLs;
				static int StndDirect;
				BMAP BitMap;
				HBITMAP NoteIcon;

                AUFPLIST ();
                ~AUFPLIST ();

				static BOOL IgnoriereMinBest; //LA-59

                BOOL GetMuster (void)
                {
                    return Muster;
                }

                void SetMuster (BOOL b)
                {
                    Muster = b;
                }

				void SetAufArt (short auf_art)
				{
					this->auf_art = auf_art;
				}

				void SetLiney (int ly)
				{
					liney = ly;
				}

                void SetMenue (int with)
                {
                    WithMenue = with;
                }

                void SetToolMenue (int with)
                {
                    WithToolBar = with;
                }

                void SetMax (void)
                {
                    mamainmax = 1;
                    SetMax0 (mamainmax);
                }

                void SetMin (void)
                {
                    mamainmin = 1;
                    SetMin0 (mamainmin);
                }

                void InitMax (void)
                {
                    mamainmax = 0;
                    SetMax0 (mamainmax);
                }

                void InitMin (void)
                {
                    mamainmin = 0;
                    SetMin0 (mamainmin);
                }

				void SetLager (long);

                BOOL IsListAktiv (void)
                {
                    return ListAktiv;
                }

                void SetCfgProgName (LPSTR);

				void SetMdnProd (int);

				int GetPosanz (void)
				{
					return Posanz;
				}

                void SetMenu (HMENU hMenu)
                {
                    this->hMenu = hMenu;
                }

                void SetPrPicture (void);
                void SetNewPicture (char *, char *);
                void SetNewLen (char *, int);
                void SetNewRow (char *, int);
                void SethMainWindow (HWND);
                HWND GetMamain1 (void);
                static int ShowBasis (void);
                static int TestAppend (void);
                static int DeleteLine (void);
                static void DoBreak (void);
                static BOOL SavePosis (void);
                static BOOL TestInsCount (void);
                static int InsertLine (void);
                static int AppendLine (void);
                static int IgnoreMinBest (void); //LAC-59
                static int PosRab (void);
                static int SearchA (void);
                static void PaintSa (HDC hdc);
                static HWND CreateSaW (void);
                static void CreateSa (void);
				static void DestroySa (void);
                static HWND CreatePlus (void);
				static void DestroyPlus (void);
                void   PaintPlus (HDC);
                static void TestSaPr (void);
                static void FillDM (double, double);
                static void FillEURO (double, double);
                static void FillFremd (double, double);
				static void InitWaehrung (void);
                static void FillAktWaehrung (void);
                static void FillWaehrung (double, double);
				static void FillKondArt (char *);
				static double GetBasa_grund (double);
                static void ReadPr (void);
                static void ReadExtraPr (void);
                static void ReadMeEinh (void);

                static void rechne_aufme0 (void);
                static void rechne_aufme1 (void);
                static void rechne_aufme2 (void);
                static void rechne_aufme3 (void);
                static void rechne_liefme (void);

                static int testme0 (void); //LAC-104-1
                static int testme (void);
                static int TestPrproz_diff (void);
                static int testpr (void);
                static int AfterPrEnter (void);
                static void EanGew (char *, BOOL);
                static int ReadEan (double);
                static int Testa_kun (void);
				static BOOL Testa_kun (double art);
                static void GetLastMeAuf (double);
                static void GetLastMe (double);
                static int TestNewArt (double);
                static int fetchMatchCode (void);
                static void ReadChargeGew (void);
                static BOOL DispCharge (void);
                static BOOL DispErsatz (void);
                static BOOL AddPosTxt (char *);
                static BOOL BestandOk (void);
                static int fetcha (void);
                static int fetchaDirect (int);
                static int fetcha_kun (void);
                static int fetchkun_bran2 (void);
                static int ChMeEinh (void);
                static long GenAufpTxt0 (void);
			    static BOOL TxtNrExist (long);
                static long GenAufpTxt (void);
                static int Texte (void);
                static int Querya (void);
                static HANDLE StarteProcess (void); //testtest
                static int KillProcess (void); //testtest
                static int doStd (void);
                static int doActivate ();
				static void ActivateStd ();
				static int SwitchStd ();
                static void TestMessage (void);
                static int setkey9me (void);
                static int setkey9basis (void);
                static int Savea (void);
                static double GetAufMeVgl (void);
                static double GetAufMeVgl (double, double, short);
                static BOOL BsdArtikel (double);
                static void BucheBsd (double, double, short, double);
                static void UpdateBsd (void);
                static void DeleteBsd (void);
                static void WriteAufkun (void);
                static BOOL testdecval (void);
                static void WritePos (int);
                static int WriteAllPos (void);
                static int dokey5 (void);
                static int WriteRow (void);
                static int TestRow (void);
                static void GenNewPosi (void);
                static int PosiEnd (long);
                static void SaveAuf (void);
                static void SetAuf (void);
                static void RestoreAuf (void);
                static int Schirm (void);
                static int SetRowItem (void);
                static int  Getib (void);
                static void ChoiseLines (HWND, HDC);
                static HWND CreateEnter (void);
                static long EnterPosRab (void);
                static HWND CreateAufw (void);
                static void AnzAufWert (void);
                static HWND CreateAufGew (void);
                static double GetArtGew (int);
                static void AnzAufGew (void);
                static long ChoiseProdLgr (long);
                static long TestProdLgr (long);
                static int ProdMdn (void);
                static void TestPfand (double);
                static void TestLeih (double);
//                static int testaufschlag (void);

                void EnterLdVk (void);
                void EnterPrVk (void);
                void SetAufVkPr (double);
                void PrVkFromList ();
                void UpdatePrVk (int);
				void SetPreise (void);
                void MoveSaW (void);
                void MovePlus (void);
                void MoveAufw (void);
                void MoveAufGew (void);
                void CloseAufw (void);
                void CloseAufGew (void);
                void InitSwSaetze (void);
                int ToMemory (int pos);
                void SetStringEnd (char *, int);
                void uebertragen (void);
                void ShowDB (short, short, long);
                void ReadDB (short, short, long);
                void SetSchirm (void);
                void GetListColor (COLORREF *, char *);
                void SetPreisTest (int);
                void GetCfgValues (void);
                void SetNoRecNr (void);
                void FillWaehrungBz (void);
                void EnterAufp (short, short, long,
                                short, long,  char *);
                void ShowAufp (short, short, long,
                                short, long,  char *);
                void RollbackBsd ();
                void DestroyWindows (void);
                void WorkAufp (void);
                void DestroyMainWindow (void);
                HWND CreateMainWindow (void);
                void MoveMamain1 ();
                void MaximizeMamain1 ();
                void SetMessColors (COLORREF, COLORREF);

                void SetChAttr (int);
                void SetFieldAttr (char *, int);
                int  GetFieldAttr (char *);
                void Geta_bz2_par (void);
                void Geta_kum_par (void);
                void Getauf_me_pr0 (void);
                void Getwa_pos_txt (void);
                void SetRecHeight (void);

                void SethwndTB (HWND);
                void SetTextMetric (TEXTMETRIC *);
                void SetLineRow (int);
                void SetListLines (int); 
                void OnPaint (HWND, UINT, WPARAM, LPARAM);
                void MoveListWindow (void);
                void BreakList (void);
                void OnHScroll (HWND, UINT, WPARAM, LPARAM);
                void OnVScroll (HWND, UINT, WPARAM, LPARAM);
                void OnSize (HWND, UINT, WPARAM, LPARAM);
                void StdAuftrag (void);
                void FunkKeys (WPARAM, LPARAM);
                int  GetRecanz (void);
                void SwitchPage0 (int);
                HWND Getmamain2 (void);
                HWND Getmamain3 (void);
                void SetFont (mfont *); 
                void SetListFont (mfont *);
                void ChoiseFont (mfont *);
                void FindString (void);
                void SetLines (int);
                int GetAktRow (void);
                int GetAktRowS (void);
                void SetColors (COLORREF, COLORREF);
                void SetListFocus (void);
 			    void PaintUb (void);
				static BOOL ContainsService ();
				static void ReadPtabAufschlag ();
 			    virtual void AfterPaint (HDC hdc, RECT *rect, int pos, int SubRow);
				void BsdCalculate ();
				static void CalcLdPrPrc (double vk_pr, double ld_pr);
				void StndComplete ();
				void StndComFailed ();
				void PrintAmpel (HDC hDC);
				BOOL TestStndActive (); 
				BOOL SetRowBkColor (HDC hdc, int einzel, BOOL AktZeile); //LAC-8
			    virtual BOOL FillColRect (HDC hdc, RECT *rect, int einzel, BOOL AktZeile); //LAC-8
                static BOOL sg_menge_ok (short sg, double sg_mindestmenge, double sg_staffel); //LAC-104
                static void sg_menge_ok (void); //LAC-104

// CSmtReader
				short Get (double a);
				double Get_inh_abverk ();
				short Get_hwg ();
				double Get_a_gew ();

// CThreadAction
				virtual int Execute (void *Param);
};
#endif