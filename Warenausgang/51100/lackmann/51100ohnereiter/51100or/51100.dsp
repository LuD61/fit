# Microsoft Developer Studio Project File - Name="51100" - Package Owner=<4>
# Von Microsoft Developer Studio generierte Erstellungsdatei, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=51100 - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "51100.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "51100.mak" CFG="51100 - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "51100 - Win32 Release" (basierend auf  "Win32 (x86) Application")
!MESSAGE "51100 - Win32 Debug" (basierend auf  "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "51100 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "D:\INFORMIX\INCL\ESQL" /I "..\INCLUDE" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_PFONT" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib KunTour.lib /nologo /subsystem:windows /machine:I386 /out:"d:\user\fit\bin\51100or.exe" /libpath:"D:\INFORMIX\LIB"

!ELSEIF  "$(CFG)" == "51100 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /I "D:\INFORMIX\INCL\ESQL" /I "..\INCLUDE" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_PFONT" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib KunTour.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"D:\INFORMIX\LIB"
# SUBTRACT LINK32 /map

!ENDIF 

# Begin Target

# Name "51100 - Win32 Release"
# Name "51100 - Win32 Debug"
# Begin Source File

SOURCE=.\A_BAS.CPP
# End Source File
# Begin Source File

SOURCE=.\A_HNDW.CPP
# End Source File
# Begin Source File

SOURCE=.\a_kun.cpp
# End Source File
# Begin Source File

SOURCE=.\A_PR.CPP
# End Source File
# Begin Source File

SOURCE=.\adr.cpp
# End Source File
# Begin Source File

SOURCE=.\adr.h
# End Source File
# Begin Source File

SOURCE=.\AdrDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\AdrDialog.h
# End Source File
# Begin Source File

SOURCE=.\akt_ipr.cpp
# End Source File
# Begin Source File

SOURCE=.\akt_iprz.cpp
# End Source File
# Begin Source File

SOURCE=.\AKT_KRZ.CPP
# End Source File
# Begin Source File

SOURCE=.\AKTION.CPP
# End Source File
# Begin Source File

SOURCE=.\Ampel.cpp
# End Source File
# Begin Source File

SOURCE=.\Ampel.h
# End Source File
# Begin Source File

SOURCE=.\res\ampelgruen.ico
# End Source File
# Begin Source File

SOURCE=.\res\ampelgruens.ico
# End Source File
# Begin Source File

SOURCE=.\res\ampelrot.ico
# End Source File
# Begin Source File

SOURCE=.\res\ampelrots.ico
# End Source File
# Begin Source File

SOURCE=.\angk.cpp
# End Source File
# Begin Source File

SOURCE=.\angp.cpp
# End Source File
# Begin Source File

SOURCE=.\angpt.cpp
# End Source File
# Begin Source File

SOURCE=.\angpt.h
# End Source File
# Begin Source File

SOURCE=.\Application.cpp
# End Source File
# Begin Source File

SOURCE=.\Application.h
# End Source File
# Begin Source File

SOURCE=.\aufkun.cpp
# End Source File
# Begin Source File

SOURCE=.\aufp_txt.cpp
# End Source File
# Begin Source File

SOURCE=.\Aufps.h
# End Source File
# Begin Source File

SOURCE=.\aufpt.cpp
# End Source File
# Begin Source File

SOURCE=.\auftest.cpp
# End Source File
# Begin Source File

SOURCE=.\auftest.rc

!IF  "$(CFG)" == "51100 - Win32 Release"

!ELSEIF  "$(CFG)" == "51100 - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\auto_nr.cpp
# End Source File
# Begin Source File

SOURCE=.\best_bdf.cpp
# End Source File
# Begin Source File

SOURCE=.\best_res.cpp
# End Source File
# Begin Source File

SOURCE=..\lib\bmlib.lib
# End Source File
# Begin Source File

SOURCE=.\Bsd.cpp
# End Source File
# Begin Source File

SOURCE=.\Bsd.h
# End Source File
# Begin Source File

SOURCE=.\bsd_buch.cpp
# End Source File
# Begin Source File

SOURCE=.\CalcualteSmt.cpp
# End Source File
# Begin Source File

SOURCE=.\CalcualteSmt.h
# End Source File
# Begin Source File

SOURCE=.\CAufps.cpp
# End Source File
# Begin Source File

SOURCE=.\CAufps.h
# End Source File
# Begin Source File

SOURCE=.\ClientOrder.cpp
# End Source File
# Begin Source File

SOURCE=.\ClientOrder.h
# End Source File
# Begin Source File

SOURCE=.\ClientOrderCom.cpp
# End Source File
# Begin Source File

SOURCE=.\ClientOrderCom.h
# End Source File
# Begin Source File

SOURCE=.\ClientOrderThread.cpp
# End Source File
# Begin Source File

SOURCE=.\ClientOrderThread.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\cmask.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\colbut.cpp
# End Source File
# Begin Source File

SOURCE=.\ComObject.cpp
# End Source File
# Begin Source File

SOURCE=.\ComObject.h
# End Source File
# Begin Source File

SOURCE=.\ControlPlugins.cpp
# End Source File
# Begin Source File

SOURCE=.\ControlPlugins.h
# End Source File
# Begin Source File

SOURCE=.\cron_best.cpp
# End Source File
# Begin Source File

SOURCE=.\cron_best.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\DataCollection.cpp
# End Source File
# Begin Source File

SOURCE=..\include\DataCollection.h
# End Source File
# Begin Source File

SOURCE=.\DATUM.C
# End Source File
# Begin Source File

SOURCE=..\libsrc\dbclass.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\DBFUNC.CPP
# End Source File
# Begin Source File

SOURCE=.\DdeDial.cpp
# End Source File
# Begin Source File

SOURCE=.\Debug.cpp
# End Source File
# Begin Source File

SOURCE=.\Debug.h
# End Source File
# Begin Source File

SOURCE=.\Dialog.cpp
# End Source File
# Begin Source File

SOURCE=.\Dialog.h
# End Source File
# Begin Source File

SOURCE=.\DialogItem.cpp
# End Source File
# Begin Source File

SOURCE=.\DialogItem.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\dlg.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\dlglst.cpp
# End Source File
# Begin Source File

SOURCE=.\DllPreise.cpp
# End Source File
# Begin Source File

SOURCE=.\Edit.cpp
# End Source File
# Begin Source File

SOURCE=.\Edit.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\enterfunc.cpp
# End Source File
# Begin Source File

SOURCE=.\fax.ico
# End Source File
# Begin Source File

SOURCE=..\libsrc\FIL.CPP
# End Source File
# Begin Source File

SOURCE=.\fit.ico
# End Source File
# Begin Source File

SOURCE=.\fit0.ico
# End Source File
# Begin Source File

SOURCE=.\fit01.ico
# End Source File
# Begin Source File

SOURCE=.\fit02.ico
# End Source File
# Begin Source File

SOURCE=.\Font.cpp
# End Source File
# Begin Source File

SOURCE=.\Font.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\FORMFELD.CPP
# End Source File
# Begin Source File

SOURCE=.\GrpPrice.cpp
# End Source File
# Begin Source File

SOURCE=.\GrpPrice.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\help.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\hwg.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\image.cpp
# End Source File
# Begin Source File

SOURCE=..\lib\inflib.lib
# End Source File
# Begin Source File

SOURCE=.\ipr.cpp
# End Source File
# Begin Source File

SOURCE=.\iprz.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\ITEM.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\itemc.cpp
# End Source File
# Begin Source File

SOURCE=..\lib\kasseex.lib
# End Source File
# Begin Source File

SOURCE=.\kumebest.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\KUN.CPP
# End Source File
# Begin Source File

SOURCE=.\kunlief.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\lbox.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\listcl.cpp
# End Source File
# Begin Source File

SOURCE=..\include\listcl.h
# End Source File
# Begin Source File

SOURCE=.\ListHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\ListHandler.h
# End Source File
# Begin Source File

SOURCE=.\lizenz.cpp
# End Source File
# Begin Source File

SOURCE=.\lizenz.h
# End Source File
# Begin Source File

SOURCE=.\LS.CPP
# End Source File
# Begin Source File

SOURCE=.\ls_txt.cpp
# End Source File
# Begin Source File

SOURCE=.\lsp_txt.cpp
# End Source File
# Begin Source File

SOURCE=.\lspt.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MDN.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\message.cpp
# End Source File
# Begin Source File

SOURCE=.\MessageHandler.cpp
# End Source File
# Begin Source File

SOURCE=..\include\MessageHandler.h
# End Source File
# Begin Source File

SOURCE=.\MO_A_PR.CPP
# End Source File
# Begin Source File

SOURCE=.\mo_abasis.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_arg.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_atxtl.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_aufl.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_auto.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_ch.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_choise.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_chq.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_chqex.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_CURSO.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_DRAW.CPP
# End Source File
# Begin Source File

SOURCE=.\MO_EINH.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_enter.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_gdruck.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_inf.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_kf.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_kompl.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_last.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_lgr.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_llgen.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_lsgen.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MELD.CPP
# End Source File
# Begin Source File

SOURCE=..\include\MO_MELD.H
# End Source File
# Begin Source File

SOURCE=..\libsrc\MO_MENU.CPP
# End Source File
# Begin Source File

SOURCE=.\mo_nr.cpp
# End Source File
# Begin Source File

SOURCE=.\MO_PREIS.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_progcfg.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_qa.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_qtou.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_qvertr.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_smtg.cpp
# End Source File
# Begin Source File

SOURCE=.\mo_stda.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_vers.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\mo_wmess.cpp
# End Source File
# Begin Source File

SOURCE=.\Mutex.cpp
# End Source File
# Begin Source File

SOURCE=.\Mutex.h
# End Source File
# Begin Source File

SOURCE=.\NoIcon.ico
# End Source File
# Begin Source File

SOURCE=.\NOTE.ICO
# End Source File
# Begin Source File

SOURCE=.\NOTES.ICO
# End Source File
# Begin Source File

SOURCE=.\oarrow.cur
# End Source File
# Begin Source File

SOURCE=..\libsrc\Process.cpp
# End Source File
# Begin Source File

SOURCE=.\ProgressCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\ProgressCtrl.h
# End Source File
# Begin Source File

SOURCE=.\prov_satz.cpp
# End Source File
# Begin Source File

SOURCE=.\PTAB.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\RBrush.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\rdonly.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\RFont.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\RowEvent.cpp
# End Source File
# Begin Source File

SOURCE=..\include\RowEvent.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\RPen.cpp
# End Source File
# Begin Source File

SOURCE=.\searchaart.h
# End Source File
# Begin Source File

SOURCE=.\searchcharge.cpp
# End Source File
# Begin Source File

SOURCE=.\searchkun.cpp
# End Source File
# Begin Source File

SOURCE=.\searchkunlief.cpp
# End Source File
# Begin Source File

SOURCE=.\searchliefkun.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\searchptab.cpp
# End Source File
# Begin Source File

SOURCE=.\searchstdfilk.cpp
# End Source File
# Begin Source File

SOURCE=.\sg.cpp
# End Source File
# Begin Source File

SOURCE=.\sg.h
# End Source File
# Begin Source File

SOURCE=.\SmtReader.cpp
# End Source File
# Begin Source File

SOURCE=.\SmtReader.h
# End Source File
# Begin Source File

SOURCE=.\SmtTable.cpp
# End Source File
# Begin Source File

SOURCE=.\SmtTable.h
# End Source File
# Begin Source File

SOURCE=.\SmtValues.cpp
# End Source File
# Begin Source File

SOURCE=.\SmtValues.h
# End Source File
# Begin Source File

SOURCE=.\SortList.cpp
# End Source File
# Begin Source File

SOURCE=.\SortList.h
# End Source File
# Begin Source File

SOURCE=.\Sounds.cpp
# End Source File
# Begin Source File

SOURCE=.\Sounds.h
# End Source File
# Begin Source File

SOURCE=.\stdfilaufk.cpp
# End Source File
# Begin Source File

SOURCE=.\stdfilaufp.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\STDFKT.CPP
# End Source File
# Begin Source File

SOURCE=.\stnd_auf.cpp
# End Source File
# Begin Source File

SOURCE=.\StndOrder.cpp
# End Source File
# Begin Source File

SOURCE=.\StndOrder.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\STRFKT.CPP
# End Source File
# Begin Source File

SOURCE=.\sys_par.cpp
# End Source File
# Begin Source File

SOURCE=.\telefon.ico
# End Source File
# Begin Source File

SOURCE=..\libsrc\Text.cpp
# End Source File
# Begin Source File

SOURCE=.\Thread.cpp
# End Source File
# Begin Source File

SOURCE=.\Thread.h
# End Source File
# Begin Source File

SOURCE=.\ThreadAction.cpp
# End Source File
# Begin Source File

SOURCE=.\ThreadAction.h
# End Source File
# Begin Source File

SOURCE=.\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\tou.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\VBrush.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\Vector.cpp
# End Source File
# Begin Source File

SOURCE=.\VERTR.CPP
# End Source File
# Begin Source File

SOURCE=..\libsrc\VFont.cpp
# End Source File
# Begin Source File

SOURCE=..\libsrc\VPen.cpp
# End Source File
# Begin Source File

SOURCE=.\WaitHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\WaitHandler.h
# End Source File
# Begin Source File

SOURCE=..\libsrc\wmaskc.cpp
# End Source File
# Begin Source File

SOURCE=.\Wnd.cpp
# End Source File
# Begin Source File

SOURCE=.\Wnd.h
# End Source File
# Begin Source File

SOURCE=.\wo_tou.cpp
# End Source File
# End Target
# End Project
