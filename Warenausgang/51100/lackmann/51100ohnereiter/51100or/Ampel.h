// Ampel.h: Schnittstelle f�r die Klasse CAmpel.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMPEL_H__68FC9FA5_BC7D_417A_A23F_DA1E3CF7AB38__INCLUDED_)
#define AFX_AMPEL_H__68FC9FA5_BC7D_417A_A23F_DA1E3CF7AB38__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <windows.h>

class CAmpel  
{
public:
	enum AMPEL_STATE
	{
		No, Red, Green,
	};
private:
	HICON iRed;
	HICON iGreen;
	HWND hWnd;
	HINSTANCE hInstance;
	POINT p;
	BOOL Play;
	BOOL OwnWindow;
	HWND aWnd;
	BOOL registered;
	HBRUSH hbrBackground;
	AMPEL_STATE state;
	static CAmpel *instance;
	static PAINTSTRUCT aktpaint;

public:

	void SetState (AMPEL_STATE state)
	{
		this->state = state;
		if (state != No && aWnd != NULL)
		{
			ShowWindow (aWnd, SW_SHOWNORMAL);
		}
	}

	AMPEL_STATE GetState ()
	{
		return state;
	}

	void SetHbrBackground (HBRUSH hbrBackground)
	{
		this->hbrBackground = hbrBackground;
	}

	void SetHWnd (HWND hWnd)
	{
		this->hWnd = hWnd;
	}

	HWND GetAWnd ()
	{
		return aWnd;
	}

	void SetOwnWindow (BOOL OwnWindow=TRUE);

	void SetHInstance (HINSTANCE hInstance)
	{
		this->hInstance = hInstance;
	}


	void SetP (POINT p)
	{
		this->p = p;
	}

	void SetP (int x, int y)
	{
		p.x = x;
		p.y = y;
	}

	void SetPlay (BOOL Play)
	{
		this->Play = Play;
	}

	CAmpel();
	CAmpel (HINSTANCE hInstance, HWND hWnd, POINT p, BOOL OwnWindow=FALSE); 
	virtual ~CAmpel();
	void PrintAmpel (HDC hdc);
	void Load ();
	void Invalidate ();
	void ShowRed (HDC hDC);
	void ShowGreen (HDC hDC);
	void SetUnvisible ();
	void SetAbsPos (HWND hWnd, int x, int y);

	void Register ();
	static LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                                  WPARAM wParam,LPARAM lParam);
};

#endif // !defined(AFX_AMPEL_H__68FC9FA5_BC7D_417A_A23F_DA1E3CF7AB38__INCLUDED_)
