#include "bsd.h"

void CBsd::Prepare ()
{
	if (BsdCursor != -1)
	{
		Close ();
	}

	DbClass.sqlin ((short *)  &mdn, 1, 0);
	DbClass.sqlin ((double *) &a, 3, 0);
	DbClass.sqlout ((double *) &bsd, 3, 0);
	BsdCursor = DbClass.sqlcursor ("select sum (bsd_gew) from bsd "
		                            "where mdn = ? "
									"and a = ?");
	if (BsdCursor == -1) return;

	DbClass.sqlin ((short *)  &mdn, 1, 0);
	DbClass.sqlin ((double *) &a, 3, 0);
	DbClass.sqlout ((double *) &bsd_res, 3, 0);
	BestResCursor = DbClass.sqlcursor ("select sum (me) from best_res "
				                       "where mdn = ? "
									   "and a = ?");

	DbClass.sqlin ((short *)  &mdn, 1, 0);
	DbClass.sqlin ((double *) &a, 3, 0);
	DbClass.sqlout ((double *) &bsd_buchwa, 3, 0);
	BsdBuchWaCursor = DbClass.sqlcursor ("select sum (me) from bsd_buch "
				                       "where mdn = ? "
									   "and a = ? "
									   "and (blg_typ = \"A\" " 
									   "or blg_typ = \"WA\")");
	DbClass.sqlin ((short *)  &mdn, 1, 0);
	DbClass.sqlin ((double *) &a, 3, 0);
	DbClass.sqlout ((double *) &bsd_buchwe, 3, 0);
	if (UseOrders)
	{
		BsdBuchWeCursor = DbClass.sqlcursor ("select sum (me) from bsd_buch "
				                       "where mdn = ? "
									   "and a = ? "
									   "and (blg_typ = \"B\" " 
									   "or blg_typ = \"WE\")");
	}
	else
	{
		BsdBuchWeCursor = DbClass.sqlcursor ("select sum (me) from bsd_buch "
				                       "where mdn = ? "
									   "and a = ? "
									   "and blg_typ = \"WE\"");
	}
}

void CBsd::Close ()
{
	if (BsdCursor != -1)
	{
		DbClass.sqlclose (BsdCursor);
		BsdCursor = -1;
	}
	if (BestResCursor != -1)
	{
		DbClass.sqlclose (BestResCursor);
		BestResCursor = -1;
	}
	if (BsdBuchWaCursor != -1)
	{
		DbClass.sqlclose (BsdBuchWaCursor);
		BsdBuchWaCursor = -1;
	}
	if (BsdBuchWeCursor != -1)
	{
		DbClass.sqlclose (BsdBuchWeCursor);
		BsdBuchWeCursor = -1;
	}
}

double CBsd::Calculate ()
{
	if (a == 0.0) return 0.0;
	if (BsdCursor == -1)
	{
		Prepare ();
	}
	if (BsdCursor == -1)
	{
		return 0.0;
	}

	bsd = bsd_res = 0.0; 
	bsd_buchwe = bsd_buchwa = 0.0; 
	DbClass.sqlopen (BsdCursor);
	DbClass.sqlfetch (BsdCursor);

	DbClass.sqlopen (BestResCursor);
	DbClass.sqlfetch (BestResCursor);

	DbClass.sqlopen (BsdBuchWaCursor);
	DbClass.sqlfetch (BsdBuchWaCursor);

	DbClass.sqlopen (BsdBuchWeCursor);
	DbClass.sqlfetch (BsdBuchWeCursor);

	return bsd - bsd_res + bsd_buchwe - bsd_buchwa;

}

double CBsd::Calculate (short mdn, double a)
{
	this->mdn = mdn;
	this->a = a;
	return Calculate ();
}
