#ifndef _SG_DEF
#define _SG_DEF
#include "dbclass.h"

struct SG {
   short     sg;
   char      sg_bez[32];
   char      sg_bezk[8];
   double min_bestellmenge; 
   double  staffelung;
};
extern struct SG sg, sg_null;

class SG_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               SG_CLASS () : DB_CLASS ()
               {
               }
			   int dbreadfirst (void);
};

#endif
