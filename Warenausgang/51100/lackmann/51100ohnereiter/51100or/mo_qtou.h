#ifndef _MO_TQDEF
#define _MO_TQDEF

class QueryTou
{
private :
        long wo_tag_plus; 
		BOOL satour;
		BOOL sotour;
public :
	QueryTou () : wo_tag_plus (1), satour (TRUE), sotour (FALSE)
        {
        }

	    void SetWotagplus (long plus)
		{
			wo_tag_plus = plus;
		}

	    void SetSatour (BOOL tour)
		{
			satour = tour;
		}
	    void SetSotour (BOOL tour)
		{
			sotour = tour;
		}

        int querytou (HWND, int);
        void infotou (HWND);
        long DatetoTou (short, short, short, long, char *);
        long ReadWoTou (short, short, short, long, int); //LAC-10
        long ReadWoTouDatum (char *); //LAC-10
        void ToutoDate (short, short, short, long, char *, long);
        long DatetoTouDir (short, short, short, long, char *);
        int  WoTagPlus (int);
};
#endif
