#ifndef __STND_ORDER
#define __STND_ORDER
#include <comdef.h>
#include "CAufps.h"
#include "ClientOrder.h"

class CStndOrder
{
public:
	IDispatch *iStndOrder;
	CCAufps CAufps;
	BOOL IdsOK;
	short client;

	OLECHAR FAR *NPutSortMode;
	OLECHAR FAR *NPutRdOptimize;
	OLECHAR FAR *NPutMdn;
	OLECHAR FAR *NPutFil;
	OLECHAR FAR *NPutKunFil;
	OLECHAR FAR *NPutKun;
	OLECHAR FAR *NPutAktAuf;
	OLECHAR FAR *NPutVisible;
	OLECHAR FAR *NSetWindow;
	OLECHAR FAR *NSetKeys;
	OLECHAR FAR *NLoad;
	OLECHAR FAR *NGetFirstRecord;
	OLECHAR FAR *NGetNextRecord;
	OLECHAR FAR *NRegisterClient;
	OLECHAR FAR *NUnregisterClient;

    DISPID IdPutSortMode;
    DISPID IdPutRdOptimize;
    DISPID IdPutMdn;
    DISPID IdPutFil;
    DISPID IdPutKunFil;
    DISPID IdPutKun;
    DISPID IdPutAktAuf;
    DISPID IdPutVisible;
    DISPID IdSetWindow;
    DISPID IdSetKeys;
    DISPID IdLoad;
    DISPID IdGetFirstRecord;
    DISPID IdGetNextRecord;
	DISPID IdRegisterClient;
	DISPID IdUnregisterClient;

	CStndOrder::CStndOrder ();
	CStndOrder::~CStndOrder ();
	CStndOrder& operator= (IDispatch *idpt);
	BOOL Init (IDispatch *idp);
	BOOL Failed ();

	HRESULT PutSortMode (CClientOrder::STD_SORT SortMode);
	HRESULT PutRdOptimize (CClientOrder::STD_RDOPTIMIZE SortMode);
	HRESULT PutMdn (short Mdn);
	HRESULT PutFil (short Fil);
	HRESULT PutKunFil (short KunFil);
	HRESULT PutKun (long Kun);
	HRESULT PutAktAuf (long AktAuf);
	HRESULT PutVisible (BOOL visible);
	HRESULT SetWindow ();
	HRESULT SetKeys (short mdn, short fil, short kun_fil, long kun, long akt_auf, short client);
	HRESULT Load (short client);
	CCAufps *GetFirstRecord (short client);
	CCAufps *GetNextRecord (short client);
	short RegisterClient ();
	HRESULT UnregisterClient (short client);
};
#endif
