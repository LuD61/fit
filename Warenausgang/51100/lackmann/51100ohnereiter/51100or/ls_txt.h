#ifndef _LS_TXT_DEF
#define _LS_TXT_DEF

#include "dbclass.h"

struct LS_TXT {
   long      nr;
   long      zei;
   char      txt[61];
};
extern struct LS_TXT ls_txt, ls_txt_null;

#line 7 "ls_txt.rh"

class LS_TXT_CLASS : public DB_CLASS 
{
       private :
               int del_cursor_posi;
               void prepare (void);
       public :
               LS_TXT_CLASS () : DB_CLASS ()
               {
                         del_cursor_posi = -1;
               }
               int dbreadfirst (void);
               int delete_aufpposi (void);
};
#endif
