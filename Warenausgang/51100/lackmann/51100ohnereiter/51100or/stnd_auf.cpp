#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "stnd_auf.h"
#include "dbclass.h"
#include "dbfunc.h"

struct STND_AUFK stnd_aufk, stnd_aufk_null;
struct STND_AUFP stnd_aufp, stnd_aufp_null;

static char *sqltext;


void SAUFK_CLASS::prepare (void)
{
         ins_quest ((char *) &stnd_aufk.mdn, 1, 0);
         ins_quest ((char *) &stnd_aufk.fil, 1, 0);  
         ins_quest ((char *) &stnd_aufk.kun, 2, 0);
         ins_quest ((char *) &stnd_aufk.kun_fil, 1, 0);

    out_quest ((char *) &stnd_aufk.mdn,1,0);
    out_quest ((char *) &stnd_aufk.fil,1,0);
    out_quest ((char *) &stnd_aufk.kun_fil,1,0);
    out_quest ((char *) &stnd_aufk.kun,2,0);
    out_quest ((char *) stnd_aufk.kun_krz1,0,17);
    out_quest ((char *) stnd_aufk.stnd_auf_kz,0,2);
    out_quest ((char *) &stnd_aufk.delstatus,1,0);
         cursor = prepare_sql ("select stnd_aufk.mdn,  "
"stnd_aufk.fil,  stnd_aufk.kun_fil,  stnd_aufk.kun,  "
"stnd_aufk.kun_krz1,  stnd_aufk.stnd_auf_kz,  stnd_aufk.delstatus from stnd_aufk "

#line 29 "stnd_auf.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_fil   = ? ");

    ins_quest ((char *) &stnd_aufk.mdn,1,0);
    ins_quest ((char *) &stnd_aufk.fil,1,0);
    ins_quest ((char *) &stnd_aufk.kun_fil,1,0);
    ins_quest ((char *) &stnd_aufk.kun,2,0);
    ins_quest ((char *) stnd_aufk.kun_krz1,0,17);
    ins_quest ((char *) stnd_aufk.stnd_auf_kz,0,2);
    ins_quest ((char *) &stnd_aufk.delstatus,1,0);
         sqltext = "update stnd_aufk set stnd_aufk.mdn = ?,  "
"stnd_aufk.fil = ?,  stnd_aufk.kun_fil = ?,  stnd_aufk.kun = ?,  "
"stnd_aufk.kun_krz1 = ?,  stnd_aufk.stnd_auf_kz = ?,  "
"stnd_aufk.delstatus = ? "

#line 35 "stnd_auf.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_fil = ? ";
  
         ins_quest ((char *) &stnd_aufk.mdn, 1, 0);
         ins_quest ((char *) &stnd_aufk.fil, 1, 0);  
         ins_quest ((char *) &stnd_aufk.kun, 2, 0);
         ins_quest ((char *) &stnd_aufk.kun_fil, 1, 0);

         upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &stnd_aufk.mdn,1,0);
    ins_quest ((char *) &stnd_aufk.fil,1,0);
    ins_quest ((char *) &stnd_aufk.kun_fil,1,0);
    ins_quest ((char *) &stnd_aufk.kun,2,0);
    ins_quest ((char *) stnd_aufk.kun_krz1,0,17);
    ins_quest ((char *) stnd_aufk.stnd_auf_kz,0,2);
    ins_quest ((char *) &stnd_aufk.delstatus,1,0);
         ins_cursor = prepare_sql ("insert into stnd_aufk ("
"mdn,  fil,  kun_fil,  kun,  kun_krz1,  stnd_auf_kz,  delstatus) "

#line 48 "stnd_auf.rpp"
                                   "values "
                                   "(?,?,?,?,?,?,?)");

#line 50 "stnd_auf.rpp"

         ins_quest ((char *) &stnd_aufk.mdn, 1, 0);
         ins_quest ((char *) &stnd_aufk.fil, 1, 0);  
         ins_quest ((char *) &stnd_aufk.kun, 2, 0);
         ins_quest ((char *) &stnd_aufk.kun_fil, 1, 0);
         del_cursor = prepare_sql ("delete from stnd_aufk "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_fil = ? ");

         ins_quest ((char *) &stnd_aufk.mdn, 1, 0);
         ins_quest ((char *) &stnd_aufk.fil, 1, 0);  
         ins_quest ((char *) &stnd_aufk.kun, 2, 0);
         ins_quest ((char *) &stnd_aufk.kun_fil, 1, 0);
         test_upd_cursor = prepare_sql ("select kun from stnd_aufk "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_fil = ?");
}

int SAUFK_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	     int dsqlstatus;
		 char kun_bran2[5];
         if (cursor == -1)
         {
                this->prepare ();
         }
         dsqlstatus =  this->DB_CLASS::dbreadfirst ();
		 if (dsqlstatus == 100)
		 {
			    if (stnd_aufk.kun_fil != 0) return dsqlstatus;

                sqlin  ((short *) &stnd_aufk.mdn, 1, 0);
                sqlin  ((short *) &stnd_aufk.fil, 1, 0);  
                sqlin  ((long *) &stnd_aufk.kun, 2, 0);
                sqlout ((char *) kun_bran2, 0, 4);
				dsqlstatus = sqlcomm ("select kun_bran2 from kun "
					                  "where mdn = ? "
									  "and fil = ? "
									  "and kun = ?");
				if (dsqlstatus == 100) return dsqlstatus;
				stnd_aufk.kun = atol (kun_bran2);
                dsqlstatus =  this->DB_CLASS::dbreadfirst ();
		 }
		 return dsqlstatus;
}

 
void SAUFP_CLASS::prepare (void)
{
         ins_quest ((char *) &stnd_aufp.mdn, 1, 0);
         ins_quest ((char *) &stnd_aufp.fil, 1, 0);  
         ins_quest ((char *) &stnd_aufp.kun, 2, 0);
         ins_quest ((char *) &stnd_aufp.kun_fil, 1, 0);

    out_quest ((char *) &stnd_aufp.mdn,1,0);
    out_quest ((char *) &stnd_aufp.fil,1,0);
    out_quest ((char *) &stnd_aufp.kun_fil,1,0);
    out_quest ((char *) &stnd_aufp.kun,2,0);
    out_quest ((char *) &stnd_aufp.a,3,0);
    out_quest ((char *) stnd_aufp.me_einh_bz,0,6);
    out_quest ((char *) &stnd_aufp.dat,2,0);
    out_quest ((char *) &stnd_aufp.pr_vk,3,0);
    out_quest ((char *) &stnd_aufp.me,3,0);
    out_quest ((char *) &stnd_aufp.posi,2,0);
         cursor = prepare_sql ("select stnd_aufp.mdn,  "
"stnd_aufp.fil,  stnd_aufp.kun_fil,  stnd_aufp.kun,  stnd_aufp.a,  "
"stnd_aufp.me_einh_bz,  stnd_aufp.dat,  stnd_aufp.pr_vk,  stnd_aufp.me,  "
"stnd_aufp.posi from stnd_aufp "

#line 112 "stnd_auf.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_fil   = ? ");

    ins_quest ((char *) &stnd_aufp.mdn,1,0);
    ins_quest ((char *) &stnd_aufp.fil,1,0);
    ins_quest ((char *) &stnd_aufp.kun_fil,1,0);
    ins_quest ((char *) &stnd_aufp.kun,2,0);
    ins_quest ((char *) &stnd_aufp.a,3,0);
    ins_quest ((char *) stnd_aufp.me_einh_bz,0,6);
    ins_quest ((char *) &stnd_aufp.dat,2,0);
    ins_quest ((char *) &stnd_aufp.pr_vk,3,0);
    ins_quest ((char *) &stnd_aufp.me,3,0);
    ins_quest ((char *) &stnd_aufp.posi,2,0);
         sqltext = "update stnd_aufp set stnd_aufp.mdn = ?,  "
"stnd_aufp.fil = ?,  stnd_aufp.kun_fil = ?,  stnd_aufp.kun = ?,  "
"stnd_aufp.a = ?,  stnd_aufp.me_einh_bz = ?,  stnd_aufp.dat = ?,  "
"stnd_aufp.pr_vk = ?,  stnd_aufp.me = ?,  stnd_aufp.posi = ? "

#line 118 "stnd_auf.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_fil = ? "
                               "and   posi = ? "
                               "and   a = ? ";
  
         ins_quest ((char *) &stnd_aufp.mdn, 1, 0);
         ins_quest ((char *) &stnd_aufp.fil, 1, 0);  
         ins_quest ((char *) &stnd_aufp.kun, 2, 0);
         ins_quest ((char *) &stnd_aufp.kun_fil, 1, 0);
         ins_quest ((char *) &stnd_aufp.posi, 2, 0);
         ins_quest ((char *) &stnd_aufp.a, 3, 0);

         upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &stnd_aufp.mdn,1,0);
    ins_quest ((char *) &stnd_aufp.fil,1,0);
    ins_quest ((char *) &stnd_aufp.kun_fil,1,0);
    ins_quest ((char *) &stnd_aufp.kun,2,0);
    ins_quest ((char *) &stnd_aufp.a,3,0);
    ins_quest ((char *) stnd_aufp.me_einh_bz,0,6);
    ins_quest ((char *) &stnd_aufp.dat,2,0);
    ins_quest ((char *) &stnd_aufp.pr_vk,3,0);
    ins_quest ((char *) &stnd_aufp.me,3,0);
    ins_quest ((char *) &stnd_aufp.posi,2,0);
         ins_cursor = prepare_sql ("insert into stnd_aufp ("
"mdn,  fil,  kun_fil,  kun,  a,  me_einh_bz,  dat,  pr_vk,  me,  posi) "

#line 135 "stnd_auf.rpp"
                                   "values "
                                   "(?,?,?,?,?,?,?,?,?,?)");

#line 137 "stnd_auf.rpp"

         ins_quest ((char *) &stnd_aufp.mdn, 1, 0);
         ins_quest ((char *) &stnd_aufp.fil, 1, 0);  
         ins_quest ((char *) &stnd_aufp.kun, 2, 0);
         ins_quest ((char *) &stnd_aufp.kun_fil, 1, 0);
         ins_quest ((char *) &stnd_aufp.posi, 2, 0);
         ins_quest ((char *) &stnd_aufp.a, 3, 0);
         del_cursor = prepare_sql ("delete from stnd_aufp "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_fil = ? "
                               "and   posi = ? "
                               "and   a = ? ");

         ins_quest ((char *) &stnd_aufp.mdn, 1, 0);
         ins_quest ((char *) &stnd_aufp.fil, 1, 0);  
         ins_quest ((char *) &stnd_aufp.kun, 2, 0);
         ins_quest ((char *) &stnd_aufp.kun_fil, 1, 0);
         ins_quest ((char *) &stnd_aufp.posi, 2, 0);
         ins_quest ((char *) &stnd_aufp.a, 3, 0);
         test_upd_cursor = prepare_sql ("select a from stnd_aufp "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_fil = ? "
                               "and   posi = ? "
                               "and   a = ? ");
}

void SAUFP_CLASS::preparea (void)
{
         ins_quest ((char *) &stnd_aufp.mdn, 1, 0);
         ins_quest ((char *) &stnd_aufp.fil, 1, 0);  
         ins_quest ((char *) &stnd_aufp.kun, 2, 0);
         ins_quest ((char *) &stnd_aufp.kun_fil, 1, 0);
         ins_quest ((char *) &stnd_aufp.a, 3, 0);
         ins_quest ((char *) &stnd_aufp.posi, 2, 0);
  

    out_quest ((char *) &stnd_aufp.mdn,1,0);
    out_quest ((char *) &stnd_aufp.fil,1,0);
    out_quest ((char *) &stnd_aufp.kun_fil,1,0);
    out_quest ((char *) &stnd_aufp.kun,2,0);
    out_quest ((char *) &stnd_aufp.a,3,0);
    out_quest ((char *) stnd_aufp.me_einh_bz,0,6);
    out_quest ((char *) &stnd_aufp.dat,2,0);
    out_quest ((char *) &stnd_aufp.pr_vk,3,0);
    out_quest ((char *) &stnd_aufp.me,3,0);
    out_quest ((char *) &stnd_aufp.posi,2,0);
         cursora = prepare_sql ("select stnd_aufp.mdn,  "
"stnd_aufp.fil,  stnd_aufp.kun_fil,  stnd_aufp.kun,  stnd_aufp.a,  "
"stnd_aufp.me_einh_bz,  stnd_aufp.dat,  stnd_aufp.pr_vk,  stnd_aufp.me,  "
"stnd_aufp.posi from stnd_aufp "

#line 178 "stnd_auf.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_fil   = ? "
                               "and   a = ? "
                               "and posi = ?");
         ins_quest ((char *) &stnd_aufp.mdn, 1, 0);
         ins_quest ((char *) &stnd_aufp.fil, 1, 0);  
         ins_quest ((char *) &stnd_aufp.kun, 2, 0);
         ins_quest ((char *) &stnd_aufp.kun_fil, 1, 0);
         ins_quest ((char *) &stnd_aufp.a, 3, 0);
  

    out_quest ((char *) &stnd_aufp.mdn,1,0);
    out_quest ((char *) &stnd_aufp.fil,1,0);
    out_quest ((char *) &stnd_aufp.kun_fil,1,0);
    out_quest ((char *) &stnd_aufp.kun,2,0);
    out_quest ((char *) &stnd_aufp.a,3,0);
    out_quest ((char *) stnd_aufp.me_einh_bz,0,6);
    out_quest ((char *) &stnd_aufp.dat,2,0);
    out_quest ((char *) &stnd_aufp.pr_vk,3,0);
    out_quest ((char *) &stnd_aufp.me,3,0);
    out_quest ((char *) &stnd_aufp.posi,2,0);
         cursora0 = prepare_sql ("select stnd_aufp.mdn,  "
"stnd_aufp.fil,  stnd_aufp.kun_fil,  stnd_aufp.kun,  stnd_aufp.a,  "
"stnd_aufp.me_einh_bz,  stnd_aufp.dat,  stnd_aufp.pr_vk,  stnd_aufp.me,  "
"stnd_aufp.posi from stnd_aufp "

#line 192 "stnd_auf.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_fil   = ? "
                               "and   a = ?");
}


int SAUFP_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	     int dsqlstatus;
		 char kun_bran2[5];
         if (cursor == -1)
         {
                this->prepare ();
         }
         dsqlstatus =  this->DB_CLASS::dbreadfirst ();
		 if (dsqlstatus == 100)
		 {
			    if (stnd_aufk.kun_fil != 0) return dsqlstatus;
                sqlin  ((short *) &stnd_aufp.mdn, 1, 0);
                sqlin  ((short *) &stnd_aufp.fil, 1, 0);  
                sqlin  ((long *) &stnd_aufp.kun, 2, 0);
                sqlout ((char *) kun_bran2, 0, 4);
				dsqlstatus = sqlcomm ("select kun_bran2 from kun "
					                  "where mdn = ? "
									  "and fil = ? "
									  "and kun = ?");
				if (dsqlstatus == 100) return dsqlstatus;
				stnd_aufp.kun = atol (kun_bran2);
				stnd_aufp.kun_fil = 2;
                dsqlstatus =  this->DB_CLASS::dbreadfirst ();
		 }
		 return dsqlstatus;
}


int SAUFP_CLASS::dbreadfirsta (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         int dsqlstatus;

         if (cursora == -1)
         {
                this->prepare ();
                this->preparea ();
         }
         dsqlstatus = open_sql (cursora);
         dsqlstatus = fetch_sql (cursora);
	   return dsqlstatus;
}


int SAUFP_CLASS::dbreada (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	   int dsqlstatus;

         fetch_sql (cursora);
	   return dsqlstatus;
}

int SAUFP_CLASS::dbreadfirsta0 (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         int dsqlstatus;

         if (cursora0 == -1)
         {
                this->prepare ();
                this->preparea ();
         }
         dsqlstatus = open_sql (cursora0);
         dsqlstatus = fetch_sql (cursora0);
	   return dsqlstatus;
}


int SAUFP_CLASS::dbreada0 (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	   int dsqlstatus;

         fetch_sql (cursora0);
	   return dsqlstatus;
}

int SAUFP_CLASS::dbclosea (void)
{
         if (cursora > -1)
         {
                  close_sql (cursora);
                  cursora = -1;
         }
         if (cursora0 > -1)
         {
                  close_sql (cursora0);
                  cursora0 = -1;
         }
         return 0;
}

