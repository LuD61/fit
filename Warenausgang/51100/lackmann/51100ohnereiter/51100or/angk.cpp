#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "angk.h"
#include "dbclass.h"
#include "dbfunc.h"

struct ANGK angk, angk_null;

extern HWND mamain1;

static DB_CLASS DbClass;
static char *sqltext;
#define MAXSORT 30000
static long MaxSort;

static int dosort1 ();
static int dosort2 ();
static int dosort3 ();
static int dosort4 ();
static int dosort5 ();
static int dosort6 ();
static int dosort7 ();

struct SORT_AW
{
	     char sort1 [10];
		 char sort2 [2];
		 char sort3 [9];
		 char sort4 [11];
		 char sort5 [2];
		 char sort6 [38];
		 char sort7 [38];
		 int  sortidx;
};

static struct SORT_AW sort_aw, *sort_awtab;


static void SortMalloc (void) 
/**
Speicher fuer Listbereich zuordnen.
**/
{
	static BOOL SortOK = FALSE;
    long anz;

	if (SortOK) return;

	DbClass.sqlout ((long *) &anz, 2, 0);
	DbClass.sqlcomm ("select count (*) from angk");

	if (anz == 0l) return;

    sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   ((anz + 1) * sizeof (struct SORT_AW))); 
	while (sort_awtab == NULL)
	{
		if (anz == 0l) break;
		anz --;
        sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   (anz * sizeof (struct SORT_AW))); 
	}
	MaxSort = anz + 1;
	if (anz == 0l)
	{
		print_mess (2, "Es konnte kein Speicher fuer die Angebotsauswahl zugewiesen werden");
		return;
	}
	SortOK = TRUE;
}


static int sort_awanz;
static int sort1 = -1;
static int sort2 = -1;
static int sort3 = -1;
static int sort4 = -1;
static int sort5 = -1;
static int sort6 = -1;
static int sort7 = -1;
static int sort_idx = 0;

static ITEM isort1    ("", sort_aw.sort1,    "", 0);
static ITEM isort2    ("", sort_aw.sort2, "", 0);
static ITEM isort3    ("", sort_aw.sort3, "", 0);
static ITEM isort4    ("", sort_aw.sort4, "", 0);
static ITEM isort5    ("", sort_aw.sort5, "", 0);
static ITEM isort6    ("", sort_aw.sort6, "", 0);
static ITEM isort7    ("", sort_aw.sort7, "", 0);

static field _fsort_aw[] = {
&isort1,     10, 1, 0, 1, 0, "%8d", NORMAL, 0, 0, 0,
&isort2,      3, 1, 0,12, 0, "%2d", NORMAL, 0, 0, 0,
&isort3,      9, 1, 0,16, 0, "%8d", NORMAL, 0, 0, 0,
&isort4,     10, 1, 0,26, 0, "dd.mm.yyy", 
                                    NORMAL, 0, 0, 0,
&isort5,      3, 1, 0,37, 0, "%2d", NORMAL, 0, 0, 0,
}
;
static form fsort_aw = {5, 0, 0, _fsort_aw, 0, 0, 0, 0, NULL};

static field _fsort_awex[] = {
&isort1,     10, 1, 0, 1, 0, "%8d", NORMAL, 0, 0, 0,
&isort3,      9, 1, 0,12, 0, "%8d", NORMAL, 0, 0, 0,
&isort6,     20, 1, 0,22, 0, "", 
                                    NORMAL, 0, 0, 0,
&isort7,     20, 1, 0,43, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awex = {4, 0, 0, _fsort_awex, 0, 0, 0, 0, NULL};

static ITEM isort1ub    ("", "Angebots-Nr",  "", 0);
static ITEM isort2ub    ("", "K/F",         "", 0);
static ITEM isort3ub    ("", "Kunde",       "", 0);
static ITEM isort4ub    ("", "Lieferdat",   "", 0);
static ITEM isort5ub    ("", "S",           "", 0);

static ITEM isort6ub    ("", "Name",   "", 0);
static ITEM isort7ub    ("", "Ort",           "", 0);
static ITEM ifillub     ("", " ",           "", 0); 

static field _fsort_awub[] = {
&isort1ub,       11, 1, 0, 0, 0, "", BUTTON, 0, dosort1,    102,
&isort2ub,        4, 1, 0,11, 0, "", BUTTON, 0, dosort2,    103,
&isort3ub,       10, 1, 0,15, 0, "", BUTTON, 0, dosort3,    104,
&isort4ub,       11, 1, 0,25, 0, "", BUTTON, 0, dosort4,    105,
&isort5ub,        4, 1, 0,36, 0, "", BUTTON, 0, dosort5,    106,
&ifillub,        80, 1, 0,40, 0, "", BUTTON, 0, 0, 0};

static form fsort_awub = {6, 0, 0, _fsort_awub, 0, 0, 0, 0, NULL};

static field _fsort_awubex[] = {
&isort1ub,       11, 1, 0, 0, 0, "", BUTTON, 0, dosort1,    102,
&isort3ub,       10, 1, 0,11, 0, "", BUTTON, 0, dosort3,    104,
&isort6ub,       21, 1, 0,21, 0, "", BUTTON, 0, dosort6,    107,
&isort7ub,       21, 1, 0,42, 0, "", BUTTON, 0, dosort7,    108,
&ifillub,        80, 1, 0,63, 0, "", BUTTON, 0, 0, 0};

static form fsort_awubex = {5, 0, 0, _fsort_awubex, 0, 0, 0, 0, NULL};

static ITEM isortl ("", "1", "", 0);

static field _fsort_awl[] = {
&isortl,          1, 1, 0, 11, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 15, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 25, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 36, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 40, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awl = {5, 0, 0, _fsort_awl, 0, 0, 0, 0, NULL};

static field _fsort_awlex[] = {
&isortl,          1, 1, 0, 11, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 21, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 42, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 63, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awlex = {4, 0, 0, _fsort_awlex, 0, 0, 0, 0, NULL};

static int dosort10 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return ((int) (atol(el1->sort1) - atol (el2->sort1)) * 
				                                  sort1);
}


int dosort1 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort10);
	sort1 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static int dosort20 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return ((int) (atoi(el1->sort2) - atoi (el2->sort2)) * 
				                                  sort2);
}


int dosort2 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort20);
	sort2 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}

static int dosort30 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return ((int) (atol(el1->sort3) - atol (el2->sort3)) * 
				                                  sort3);
}


int dosort3 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort30);
	sort3 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}

static int dosort40 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 
          long dat1, dat2;

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
          dat1 = dasc_to_long (el1->sort4);
          dat2 = dasc_to_long (el2->sort4);
	      return ((int) (dat1 - dat2) *  sort4);
}


int dosort4 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort40);
	sort4 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}

static int dosort50 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return ((int) (atoi(el1->sort5) - atoi (el2->sort5)) * 
				                                  sort5);
}


int dosort5 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort50);
	sort5 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}

static int dosort60 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return (strcmp (el1->sort6, el2->sort6) * sort6);
}


int dosort6 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort60);
	sort6 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}

static int dosort70 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return (strcmp (el1->sort7, el2->sort7) * sort7);
}


int dosort7 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort70);
	sort7 *= -1;
    ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}

static void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        sort_idx = idx;
        break_list ();
        return;
}

static int endsort (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

void ANGK_CLASS::ListToMamain1 (HWND eWindow)
/**
Fenster an mamain1 anpassen.
**/
{
	    RECT rect;

		GetWindowRect (mamain1, &rect);

		rect.left ++;
		rect.top ++;
		rect.right = rect.right - rect.left - 1;
		rect.bottom = rect.bottom - rect.top - 1;
		MoveWindow (eWindow, rect.left, rect.top, rect.right, rect.bottom, TRUE);
}


int ANGK_CLASS::ShowBuAngQuery (HWND hWnd, int ws_flag) 
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		HWND eWindow;
	    form *savecurrent;

		SortMalloc ();
	    savecurrent = current_form;
        if (aufchange)
        {
  		    sort_awanz = 0;
		    while (fetch_scroll (cursor_ausw, NEXT) == 0)
            {
			         sprintf (sort_awtab[sort_awanz].sort1, "%ld",
						      angk.ang);
			         sprintf (sort_awtab[sort_awanz].sort2, "%hd",
						      angk.kun_fil);
			         sprintf (sort_awtab[sort_awanz].sort3, "%ld",
						      angk.kun);
			         dlong_to_asc (angk.lieferdat, 
                                   sort_awtab[sort_awanz].sort4); 
			         sprintf (sort_awtab[sort_awanz].sort5, "%hd",
						      angk.ang_stat);
					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 sort_awanz ++;
					 if (sort_awanz == MAXSORT) break;
            }
        }

		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
        eWindow = OpenListWindowEnF (10, 40, 8, 20, 0);
		ListToMamain1 (eWindow);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
		Setlistenter (1);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
         }
		 sort_idx = sort_awtab[sort_idx].sortidx;
         fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
         close_sql (cursor_ausw);
		 cursor_ausw = -1;
         return 0;
}

int ANGK_CLASS::PrepareAngQuery (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         short old_mode;
	     char sqlstring [1000]; 

         ReadQuery (qform, qnamen); 
         if (strcmp (aufstring0, qstring) == 0)
         {
                   aufchange = 0;
         }
         else
         {
                   aufchange = 1;
         }
		 aufchange = 1;
         close_sql (cursor_ausw);
		 cursor_ausw = -1;
         strcpy (aufstring0, qstring);

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select angk.ang, angk.kun_fil, angk.kun, "
                            "angk.lieferdat, angk.ang_stat "
							"from angk "
                            "where ang  > 0 "
                            "and %s "
						    "order by ang", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select angk.ang, angk.kun_fil, angk.kun, "
                            "angk.lieferdat, angk.ang_stat "
							"from angk "
                            "where ang  > 0 "
                            "order by ang");
         }
         out_quest ((char *) &angk.ang,      2, 0);
         out_quest ((char *) &angk.kun_fil,  1, 0);
         out_quest ((char *) &angk.kun,      2, 0);
         out_quest ((char *) &angk.lieferdat,2, 0);
         out_quest ((char *) &angk.ang_stat, 1, 0);
         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prepare_scroll (sqlstring);
         sql_mode = old_mode;
         if (sqlstatus < 0)
         {
                       return (sqlstatus);
         }
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}

int ANGK_CLASS::ShowBuAngQueryEx (HWND hWnd, int ws_flag) 
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		HWND eWindow;
	    form *savecurrent;

		SortMalloc ();
	    savecurrent = current_form;
        if (aufchange)
        {
  		    sort_awanz = 0;
		    while (fetch_scroll (cursor_ausw, NEXT) == 0)
            {
			         sprintf (sort_awtab[sort_awanz].sort1, "%ld",
						      angk.ang);
			         sprintf (sort_awtab[sort_awanz].sort3, "%ld",
						      angk.kun);
					 strcpy (sort_awtab[sort_awanz].sort6, _adr.adr_nam1);
					 strcpy (sort_awtab[sort_awanz].sort7, _adr.ort1);
					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 sort_awanz ++;
					 if (sort_awanz == MAXSORT) break;
            }
        }

		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
//        eWindow = OpenListWindowEnF (10, 40, 8, 20, 0);
        eWindow = OpenListWindowEnF (17, 73, 6, 2, 0);
		ListToMamain1 (eWindow);
        ElistVl (&fsort_awlex);
        ElistUb (&fsort_awubex);
		Setlistenter (1);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_awex);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_awex);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
         }
		 sort_idx = sort_awtab[sort_idx].sortidx;
         fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
         close_sql (cursor_ausw);
		 cursor_ausw = -1;
         return 0;
}

int ANGK_CLASS::PrepareAngQueryEx (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         short old_mode;
	     char sqlstring [1000]; 

         ReadQuery (qform, qnamen); 
         if (strcmp (aufstring0, qstring) == 0)
         {
                   aufchange = 0;
         }
         else
         {
                   aufchange = 1;
         }
		 aufchange = 1;
         close_sql (cursor_ausw);
		 cursor_ausw = -1;
         strcpy (aufstring0, qstring);

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select angk.ang, angk.kun_fil, angk.kun, "
                            "adr.adr_nam1, adr.ort1 "
							"from angk,adr "
                            "where ang  > 0 "
							"and angk.adr = adr.adr "
                            "and %s "
						    "order by ang", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select angk.ang, angk.kun_fil, angk.kun, "
                            "adr.adr_nam1, adr.ort1 "
							"from angk,adr "
                            "where ang  > 0 "
							"and angk.adr = adr.adr "
                            "order by ang");
         }
         out_quest ((char *) &angk.ang,      2, 0);
         out_quest ((char *) &angk.kun_fil,  1, 0);
         out_quest ((char *) &angk.kun,      2, 0);
         out_quest ((char *) _adr.adr_nam1,0, 37);
         out_quest ((char *) _adr.ort1, 0, 37);
         old_mode = sql_mode;
         sql_mode = 2;
         cursor_ausw = prepare_scroll (sqlstring);
         sql_mode = old_mode;
         if (sqlstatus < 0)
         {
                       return (sqlstatus);
         }
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}




void ANGK_CLASS::prepare (void)
{
         ins_quest ((char *) &angk.mdn, 1, 0);
         ins_quest ((char *) &angk.fil, 1, 0);  
         ins_quest ((char *) &angk.ang, 2, 0);

    out_quest ((char *) &angk.mdn,1,0);
    out_quest ((char *) &angk.fil,1,0);
    out_quest ((char *) &angk.ang,2,0);
    out_quest ((char *) &angk.adr,2,0);
    out_quest ((char *) &angk.kun_fil,1,0);
    out_quest ((char *) &angk.kun,2,0);
    out_quest ((char *) &angk.lieferdat,2,0);
    out_quest ((char *) angk.lieferzeit,0,6);
    out_quest ((char *) angk.hinweis,0,49);
    out_quest ((char *) &angk.ang_stat,1,0);
    out_quest ((char *) angk.kun_krz1,0,17);
    out_quest ((char *) angk.feld_bz1,0,20);
    out_quest ((char *) angk.feld_bz2,0,12);
    out_quest ((char *) angk.feld_bz3,0,8);
    out_quest ((char *) &angk.delstatus,1,0);
    out_quest ((char *) &angk.zeit_dec,3,0);
    out_quest ((char *) &angk.kopf_txt,2,0);
    out_quest ((char *) &angk.fuss_txt,2,0);
    out_quest ((char *) &angk.vertr,2,0);
    out_quest ((char *) angk.ang_ext,0,17);
    out_quest ((char *) &angk.tou,2,0);
    out_quest ((char *) angk.pers_nam,0,9);
    out_quest ((char *) &angk.komm_dat,2,0);
    out_quest ((char *) &angk.best_dat,2,0);
    out_quest ((char *) &angk.waehrung,1,0);
    out_quest ((char *) &angk.psteuer_kz,1,0);
         cursor = prepare_sql ("select angk.mdn,  angk.fil,  "
"angk.ang,  angk.adr,  angk.kun_fil,  angk.kun,  angk.lieferdat,  "
"angk.lieferzeit,  angk.hinweis,  angk.ang_stat,  angk.kun_krz1,  "
"angk.feld_bz1,  angk.feld_bz2,  angk.feld_bz3,  angk.delstatus,  "
"angk.zeit_dec,  angk.kopf_txt,  angk.fuss_txt,  angk.vertr,  "
"angk.ang_ext,  angk.tou,  angk.pers_nam,  angk.komm_dat,  angk.best_dat,  "
"angk.waehrung,  angk.psteuer_kz from angk "

#line 655 "angk.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   ang = ?");

    ins_quest ((char *) &angk.mdn,1,0);
    ins_quest ((char *) &angk.fil,1,0);
    ins_quest ((char *) &angk.ang,2,0);
    ins_quest ((char *) &angk.adr,2,0);
    ins_quest ((char *) &angk.kun_fil,1,0);
    ins_quest ((char *) &angk.kun,2,0);
    ins_quest ((char *) &angk.lieferdat,2,0);
    ins_quest ((char *) angk.lieferzeit,0,6);
    ins_quest ((char *) angk.hinweis,0,49);
    ins_quest ((char *) &angk.ang_stat,1,0);
    ins_quest ((char *) angk.kun_krz1,0,17);
    ins_quest ((char *) angk.feld_bz1,0,20);
    ins_quest ((char *) angk.feld_bz2,0,12);
    ins_quest ((char *) angk.feld_bz3,0,8);
    ins_quest ((char *) &angk.delstatus,1,0);
    ins_quest ((char *) &angk.zeit_dec,3,0);
    ins_quest ((char *) &angk.kopf_txt,2,0);
    ins_quest ((char *) &angk.fuss_txt,2,0);
    ins_quest ((char *) &angk.vertr,2,0);
    ins_quest ((char *) angk.ang_ext,0,17);
    ins_quest ((char *) &angk.tou,2,0);
    ins_quest ((char *) angk.pers_nam,0,9);
    ins_quest ((char *) &angk.komm_dat,2,0);
    ins_quest ((char *) &angk.best_dat,2,0);
    ins_quest ((char *) &angk.waehrung,1,0);
    ins_quest ((char *) &angk.psteuer_kz,1,0);
         sqltext = "update angk set angk.mdn = ?,  angk.fil = ?,  "
"angk.ang = ?,  angk.adr = ?,  angk.kun_fil = ?,  angk.kun = ?,  "
"angk.lieferdat = ?,  angk.lieferzeit = ?,  angk.hinweis = ?,  "
"angk.ang_stat = ?,  angk.kun_krz1 = ?,  angk.feld_bz1 = ?,  "
"angk.feld_bz2 = ?,  angk.feld_bz3 = ?,  angk.delstatus = ?,  "
"angk.zeit_dec = ?,  angk.kopf_txt = ?,  angk.fuss_txt = ?,  "
"angk.vertr = ?,  angk.ang_ext = ?,  angk.tou = ?,  angk.pers_nam = ?,  "
"angk.komm_dat = ?,  angk.best_dat = ?,  angk.waehrung = ?,  "
"angk.psteuer_kz = ? "

#line 660 "angk.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   ang = ?";
  
         ins_quest ((char *) &angk.mdn, 1, 0);
         ins_quest ((char *) &angk.fil, 1, 0);  
         ins_quest ((char *) &angk.ang, 2, 0);

         upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &angk.mdn,1,0);
    ins_quest ((char *) &angk.fil,1,0);
    ins_quest ((char *) &angk.ang,2,0);
    ins_quest ((char *) &angk.adr,2,0);
    ins_quest ((char *) &angk.kun_fil,1,0);
    ins_quest ((char *) &angk.kun,2,0);
    ins_quest ((char *) &angk.lieferdat,2,0);
    ins_quest ((char *) angk.lieferzeit,0,6);
    ins_quest ((char *) angk.hinweis,0,49);
    ins_quest ((char *) &angk.ang_stat,1,0);
    ins_quest ((char *) angk.kun_krz1,0,17);
    ins_quest ((char *) angk.feld_bz1,0,20);
    ins_quest ((char *) angk.feld_bz2,0,12);
    ins_quest ((char *) angk.feld_bz3,0,8);
    ins_quest ((char *) &angk.delstatus,1,0);
    ins_quest ((char *) &angk.zeit_dec,3,0);
    ins_quest ((char *) &angk.kopf_txt,2,0);
    ins_quest ((char *) &angk.fuss_txt,2,0);
    ins_quest ((char *) &angk.vertr,2,0);
    ins_quest ((char *) angk.ang_ext,0,17);
    ins_quest ((char *) &angk.tou,2,0);
    ins_quest ((char *) angk.pers_nam,0,9);
    ins_quest ((char *) &angk.komm_dat,2,0);
    ins_quest ((char *) &angk.best_dat,2,0);
    ins_quest ((char *) &angk.waehrung,1,0);
    ins_quest ((char *) &angk.psteuer_kz,1,0);
         ins_cursor = prepare_sql ("insert into angk (mdn,  fil,  "
"ang,  adr,  kun_fil,  kun,  lieferdat,  lieferzeit,  hinweis,  ang_stat,  kun_krz1,  "
"feld_bz1,  feld_bz2,  feld_bz3,  delstatus,  zeit_dec,  kopf_txt,  fuss_txt,  "
"vertr,  ang_ext,  tou,  pers_nam,  komm_dat,  best_dat,  waehrung,  psteuer_kz) "

#line 671 "angk.rpp"
                                   "values "
                                   "(?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

#line 673 "angk.rpp"

         ins_quest ((char *) &angk.mdn, 1, 0);
         ins_quest ((char *) &angk.fil, 1, 0);  
         ins_quest ((char *) &angk.ang, 2, 0);
         del_cursor = prepare_sql ("delete from angk "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   ang = ?");

         ins_quest ((char *) &angk.mdn, 1, 0);
         ins_quest ((char *) &angk.fil, 1, 0);  
         ins_quest ((char *) &angk.ang, 2, 0);
         test_upd_cursor = prepare_sql ("select ang from angk "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   ang = ?");
}

int ANGK_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}


 
