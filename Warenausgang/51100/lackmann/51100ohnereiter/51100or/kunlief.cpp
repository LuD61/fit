#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "kunlief.h"

struct KUNLIEF kunlief, kunlief_null;

void KUNLIEF_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &kunlief.mdn, 1, 0);
            ins_quest ((char *) &kunlief.fil, 1, 0);
            ins_quest ((char *) &kunlief.kun, 2, 0);
            ins_quest ((char *) &kunlief.adr, 2, 0);
    out_quest ((char *) &kunlief.mdn,1,0);
    out_quest ((char *) &kunlief.fil,1,0);
    out_quest ((char *) &kunlief.kun,2,0);
    out_quest ((char *) &kunlief.adr,2,0);
    out_quest ((char *) kunlief.kun_krz2,0,17);
    out_quest ((char *) &kunlief.sprache,1,0);
    out_quest ((char *) &kunlief.tou,2,0);
    out_quest ((char *) kunlief.vers_art,0,3);
    out_quest ((char *) &kunlief.lief_art,1,0);
    out_quest ((char *) kunlief.fra_ko_ber,0,3);
    out_quest ((char *) kunlief.form_typ1,0,3);
    out_quest ((char *) &kunlief.auflage1,1,0);
    out_quest ((char *) &kunlief.ls_fuss_txt,2,0);
    out_quest ((char *) &kunlief.ls_kopf_txt,2,0);
    out_quest ((char *) kunlief.gn_pkt_kz,0,2);
    out_quest ((char *) kunlief.sw_rab,0,2);
    out_quest ((char *) &kunlief.kun_leer_kz,1,0);
    out_quest ((char *) kunlief.iln,0,17);
    out_quest ((char *) kunlief.plattform,0,17);
    out_quest ((char *) kunlief.be_log,0,4);
    out_quest ((char *) &kunlief.stat_kun,2,0);
            cursor = prepare_sql ("select kunlief.mdn,  "
"kunlief.fil,  kunlief.kun,  kunlief.adr,  kunlief.kun_krz2,  "
"kunlief.sprache,  kunlief.tou,  kunlief.vers_art,  kunlief.lief_art,  "
"kunlief.fra_ko_ber,  kunlief.form_typ1,  kunlief.auflage1,  "
"kunlief.ls_fuss_txt,  kunlief.ls_kopf_txt,  kunlief.gn_pkt_kz,  "
"kunlief.sw_rab,  kunlief.kun_leer_kz,  kunlief.iln,  "
"kunlief.plattform,  kunlief.be_log,  kunlief.stat_kun from kunlief "

#line 23 "kunlief.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun = ? " 
                                  "and   adr = ?" );

    ins_quest ((char *) &kunlief.mdn,1,0);
    ins_quest ((char *) &kunlief.fil,1,0);
    ins_quest ((char *) &kunlief.kun,2,0);
    ins_quest ((char *) &kunlief.adr,2,0);
    ins_quest ((char *) kunlief.kun_krz2,0,17);
    ins_quest ((char *) &kunlief.sprache,1,0);
    ins_quest ((char *) &kunlief.tou,2,0);
    ins_quest ((char *) kunlief.vers_art,0,3);
    ins_quest ((char *) &kunlief.lief_art,1,0);
    ins_quest ((char *) kunlief.fra_ko_ber,0,3);
    ins_quest ((char *) kunlief.form_typ1,0,3);
    ins_quest ((char *) &kunlief.auflage1,1,0);
    ins_quest ((char *) &kunlief.ls_fuss_txt,2,0);
    ins_quest ((char *) &kunlief.ls_kopf_txt,2,0);
    ins_quest ((char *) kunlief.gn_pkt_kz,0,2);
    ins_quest ((char *) kunlief.sw_rab,0,2);
    ins_quest ((char *) &kunlief.kun_leer_kz,1,0);
    ins_quest ((char *) kunlief.iln,0,17);
    ins_quest ((char *) kunlief.plattform,0,17);
    ins_quest ((char *) kunlief.be_log,0,4);
    ins_quest ((char *) &kunlief.stat_kun,2,0);
            sqltext = "update kunlief set kunlief.mdn = ?,  "
"kunlief.fil = ?,  kunlief.kun = ?,  kunlief.adr = ?,  "
"kunlief.kun_krz2 = ?,  kunlief.sprache = ?,  kunlief.tou = ?,  "
"kunlief.vers_art = ?,  kunlief.lief_art = ?,  "
"kunlief.fra_ko_ber = ?,  kunlief.form_typ1 = ?,  "
"kunlief.auflage1 = ?,  kunlief.ls_fuss_txt = ?,  "
"kunlief.ls_kopf_txt = ?,  kunlief.gn_pkt_kz = ?,  "
"kunlief.sw_rab = ?,  kunlief.kun_leer_kz = ?,  kunlief.iln = ?,  "
"kunlief.plattform = ?,  kunlief.be_log = ?,  kunlief.stat_kun = ? "

#line 29 "kunlief.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun = ? "
                                  "and   adr = ?";

            ins_quest ((char *) &kunlief.mdn, 1, 0);
            ins_quest ((char *) &kunlief.fil, 1, 0);
            ins_quest ((char *) &kunlief.kun, 2, 0);
            ins_quest ((char *) &kunlief.adr, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &kunlief.mdn, 1, 0);
            ins_quest ((char *) &kunlief.fil, 1, 0);
            ins_quest ((char *) &kunlief.kun, 2, 0);
            ins_quest ((char *) &kunlief.adr, 2, 0);
            test_upd_cursor = prepare_sql ("select adr from kunlief "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun = ? "
                                  "and   adr = ? "
                                  "for update");

            ins_quest ((char *) &kunlief.mdn, 1, 0);
            ins_quest ((char *) &kunlief.fil, 1, 0);
            ins_quest ((char *) &kunlief.kun, 2, 0);
            ins_quest ((char *) &kunlief.adr, 2, 0);
            del_cursor = prepare_sql ("delete from kunlief "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun = ? "
                                  "and   adr = ?");

    ins_quest ((char *) &kunlief.mdn,1,0);
    ins_quest ((char *) &kunlief.fil,1,0);
    ins_quest ((char *) &kunlief.kun,2,0);
    ins_quest ((char *) &kunlief.adr,2,0);
    ins_quest ((char *) kunlief.kun_krz2,0,17);
    ins_quest ((char *) &kunlief.sprache,1,0);
    ins_quest ((char *) &kunlief.tou,2,0);
    ins_quest ((char *) kunlief.vers_art,0,3);
    ins_quest ((char *) &kunlief.lief_art,1,0);
    ins_quest ((char *) kunlief.fra_ko_ber,0,3);
    ins_quest ((char *) kunlief.form_typ1,0,3);
    ins_quest ((char *) &kunlief.auflage1,1,0);
    ins_quest ((char *) &kunlief.ls_fuss_txt,2,0);
    ins_quest ((char *) &kunlief.ls_kopf_txt,2,0);
    ins_quest ((char *) kunlief.gn_pkt_kz,0,2);
    ins_quest ((char *) kunlief.sw_rab,0,2);
    ins_quest ((char *) &kunlief.kun_leer_kz,1,0);
    ins_quest ((char *) kunlief.iln,0,17);
    ins_quest ((char *) kunlief.plattform,0,17);
    ins_quest ((char *) kunlief.be_log,0,4);
    ins_quest ((char *) &kunlief.stat_kun,2,0);
            ins_cursor = prepare_sql ("insert into kunlief ("
"mdn,  fil,  kun,  adr,  kun_krz2,  sprache,  tou,  vers_art,  lief_art,  fra_ko_ber,  "
"form_typ1,  auflage1,  ls_fuss_txt,  ls_kopf_txt,  gn_pkt_kz,  sw_rab,  "
"kun_leer_kz,  iln,  plattform,  be_log,  stat_kun) "

#line 62 "kunlief.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?)"); 

#line 64 "kunlief.rpp"
}
int KUNLIEF_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

