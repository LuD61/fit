#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "itemc.h"
#include "wmaskc.h"
#include "dbfunc.h"
#include "a_bas.h"
#include "ls.h"
#include "kun.h"
#include "mo_meld.h"
#include "mo_qa.h"
#include "angk.h"

static ITEM iOK ("", "     OK     ", "", 0);
static ITEM iCA ("", "  Abbrechen ", "", 0);

static LS_CLASS  ls_class;
static KUN_CLASS kun_class;
static ANGK_CLASS  angk_class;

static int testquery (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		}
        return 0;
}


int QueryClass::querya (HWND hWnd)
/**
Query ueber Artikel.
**/
{

        HANDLE hMainInst;

        static char aval[41];
        static char a_bz1val[41];
        static char hwgval[41];
        static char wgval[41];
        static char agval[41];
        static char a_typval[41];

        static ITEM iaval ("a", 
                           aval, 
                             "Artikel-Nr        ", 
                           0);

        static ITEM ia_bz1val ("a_bz1", 
                                a_bz1val, 
                             "Bezeichnung       ", 
                                   0);
        static ITEM ihwgval ("hwg", 
                             hwgval, 
                             "Hauptwarengruppe  ", 
                             0);

        static ITEM iwgval ("wg", 
                            wgval, 
                             "Warengruppe       ", 

                            0);
        static ITEM iagval ("ag", 
                            agval, 
                             "Artikelgruppe     ", 
                             0);


        static ITEM ia_typval ("a_typ", 
                                a_typval, 
                             "Artikel-Typ       ", 
                                   0);

        static field _qtxtform[] = {
           &iaval,      40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ia_bz1val,  40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ihwgval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &iwgval,     40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iagval,     40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ia_typval,  40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 8,15, 0, "", BUTTON, 0,testquery ,KEY12,
           &iCA,        15, 0, 8,32, 0, "", BUTTON, 0,testquery ,KEY5,
		};

        static form qtxtform = {8, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"a", "a_bz1", "hwg", "wg", "ag",
                                 "a_typ",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (8);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 8);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (11, 62, 9, 10, hMainInst,
                               "Suchkriterien f�r Artikel");
        enter_form (query, &qtxtform, 0, 0);

		::TestAKun = testa_kun;
		SetButtonTab (FALSE);
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEY10) 
        {
                  Preparea_basQuery (&qtxtform, qnamen);

                  Showa_basQuery (hWnd, 1);
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}


int QueryClass::querya_direct (HWND hWnd, char *a_bz1)
/**
Query ueber Artikel.
**/
{

        HANDLE hMainInst;

        static char a_bz1val[41];

        static ITEM ia_bz1val ("a_bz1", 
                                a_bz1val, 
                             "Bezeichnung......:", 
                                   0);

        static field _qtxtform[] = {
           &ia_bz1val,  40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
		};

        static form qtxtform = {1, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"a_bz1",
                                  NULL};

 	    int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
		save_fkt (6);
		save_fkt (7);
		save_fkt (8);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 8);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);


		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);

		::TestAKun = testa_kun;
		SetAktivWindow (hWnd);
		strcpy (a_bz1val, a_bz1);
        Preparea_basQuery_Bez (&qtxtform, qnamen);

        Showa_basQuery_Bez (hWnd, 1);
		currentfield = savefield;
		strcpy (a_bz1val, "");
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        if (syskey == KEY5)
		{
			strcpy (a_bz1, "0");
			return FALSE;
		}
		sprintf (a_bz1, "%13.0lf", _a_bas.a);
        return TRUE;
}

int QueryClass::searcha_direct (HWND hWnd, char *a_bz1)
/**
Query ueber Artikel.
**/
{
 	    int savefield;
		form *savecurrent;

        HANDLE hMainInst;

        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
		save_fkt (8);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 8);
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetAktivWindow (hWnd);

		::TestAKun = testa_kun;
		SetCharBuffa_bas (a_bz1);
        Preparea_basQuery ();
        Showa_basQuery (hWnd, 1);

		currentfield = savefield;
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        if (syskey == KEY5)
		{
			strcpy (a_bz1, "0");
			return FALSE;
		}
		sprintf (a_bz1, "%13.0lf", _a_bas.a);
        return TRUE;
}


int QueryClass::searcha (HWND hWnd)
/**
Auswahl ueber Artikel.
**/
{

        HANDLE hMainInst;

		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
		save_fkt (8);
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 8);
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetAktivWindow (hWnd);

		::TestAKun = testa_kun;
        Preparea_basQuery ();
        Showa_basQuery (hWnd, 1);

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}

int QueryClass::queryauf (HWND hWnd)
/**
Query ueber Auftr�ge.
**/
{

        HANDLE hMainInst;

        static char mdnval[41];
        static char filval[41];
        static char aufval[41];
        static char kunfilval[41];
        static char kunval[41];
        static char kun_krzval [41];
        static char ortval [41];
        static char ldatval[41];
        static char statval[41];

        static ITEM imdnval ("mdn", 
                             mdnval, 
                             "Mandanten-Nr      ", 

                             0);
        static ITEM ifilval ("fil", 
                             filval, 
                             "Filiale-Nr        ", 
                             0);


        static ITEM iaufval ("auf", 
                             aufval, 
                             "Auftrag-Nr        ", 
                             0);

        static ITEM ikunfilval ("kun_fil", 
                                 kunfilval, 
                                "Kunde/Filiale     ", 
                                 0);

        static ITEM ikunval    ("kun", 
                                 kunval, 
                                "Kunde             ", 
                                 0);

        static ITEM ikun_krzval    
			                   ("kun", 
                                 kun_krzval, 
                                "Name              ", 
                                 0);

        static ITEM iortval    
			                   ("ort1", 
                                 ortval, 
                                "Ort               ", 
                                 0);

        static ITEM ildatval ("lieferdat", 
                             ldatval, 
                             "Lieferdatum       ", 
                             0);

        static ITEM istatval("auf_stat", 
                             statval, 
                             "Status            ",
                             0);

        static field _qtxtform[] = {
           &iaufval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krzval,40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iortval,    40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 7,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 9,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0, 9,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform = {9, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"auf", "kun_fil", "kun", "kun_krz1", "adr.ort1",
                                 "lieferdat", "auf_stat",
                                  NULL};

        static field _qtxtform0[] = {
           &imdnval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ifilval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &iaufval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krzval,40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iortval,    40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 7,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 9,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0, 9,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform0 = {11, 0, 0, _qtxtform0, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen0[] = {"aufk.mdn", "aufk.fil","auf", 
			                      "kun_fil", "kun", "kun_krz1", "adr.ort1",
                                 "lieferdat", "auf_stat",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		strcpy (statval, "<5");
		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (12,62, 9, 10, hMainInst,
                               "Suchkriterien f�r Auftr�ge");
        enter_form (query, &qtxtform, 0, 0);

		SetButtonTab (FALSE);
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEYESC)
        {
			if (lutz_mdn_par)
			{
                  if (ls_class.PrepareAufQueryEx (&qtxtform, qnamen) == 0)
                  {
                             ls_class.ShowBuAufQueryEx (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
			}
			else
			{
                  sprintf (mdnval, "%hd", aufk.mdn); 
                  sprintf (filval, "%hd", aufk.fil); 
                  if (ls_class.PrepareAufQueryEx (&qtxtform0, qnamen0) == 0)
                  {
                             ls_class.ShowBuAufQueryEx (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
			}

        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (9);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}

int QueryClass::queryaufnd (HWND hWnd)
/**
Query ueber Auftr�ge f�r Nachdruck.
**/
{

        HANDLE hMainInst;

        static char mdnval[41];
        static char filval[41];
        static char aufval[41];
        static char kunfilval[41];
        static char kunval[41];
        static char kun_krzval [41];
        static char ortval [41];
        static char ldatval[41];
        static char statval[41];

        static ITEM imdnval ("mdn", 
                             mdnval, 
                             "Mandanten-Nr      ", 

                             0);
        static ITEM ifilval ("fil", 
                             filval, 
                             "Filiale-Nr        ", 
                             0);


        static ITEM iaufval ("auf", 
                             aufval, 
                             "Auftrag-Nr        ", 
                             0);

        static ITEM ikunfilval ("kun_fil", 
                                 kunfilval, 
                                "Kunde/Filiale     ", 
                                 0);

        static ITEM ikunval    ("kun", 
                                 kunval, 
                                "Kunde             ", 
                                 0);

        static ITEM ikun_krzval    
			                   ("kun", 
                                 kun_krzval, 
                                "Name              ", 
                                 0);

        static ITEM iortval    
			                   ("ort1", 
                                 ortval, 
                                "Ort               ", 
                                 0);

        static ITEM ildatval ("lieferdat", 
                             ldatval, 
                             "Lieferdatum       ", 
                             0);

        static ITEM istatval("auf_stat", 
                             statval, 
                             "Status            ",
                             0);

        static field _qtxtform[] = {
           &iaufval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krzval,40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 7,1, 0, "", REMOVED, 0, testquery,0,
           &iOK,        15, 0, 7,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0, 7,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform = {8, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"auf", "kun_fil", "kun", "kun_krz1",
                                 "lieferdat", "auf_stat",
                                  NULL};

        static field _qtxtform0[] = {
           &imdnval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ifilval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &iaufval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krzval,40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iortval,    40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 7,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 9,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0, 9,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform0 = {11, 0, 0, _qtxtform0, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen0[] = {"aufk.mdn", "aufk.fil","auf", 
			                      "kun_fil", "kun", "kun_krz1", "adr.ort1",
                                 "lieferdat", "auf_stat",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		strcpy (statval, ">2");
		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (10,62, 9, 10, hMainInst,
                               "Suchkriterien f�r Auftr�ge");
        enter_form (query, &qtxtform, 0, 0);

		SetButtonTab (FALSE);
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEYESC)
        {
			if (lutz_mdn_par)
			{
                  if (ls_class.PrepareAufQueryEx (&qtxtform, qnamen) == 0)
                  {
                             ls_class.ShowBuAufQueryEx (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
			}
			else
			{
				  if (aufk.mdn > 0)
				  {
					sprintf (mdnval, "%hd", aufk.mdn); 
				  }
				  else
				  {
					sprintf (mdnval, "%hd", 1); 
					aufk.mdn = 1;
				  }
                  sprintf (filval, "%hd", aufk.fil); 
                  if (ls_class.PrepareAufQuery (&qtxtform0, qnamen0) == 0)
                  {
                             ls_class.ShowBuAufQuery (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
			}

        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (9);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}


int QueryClass::querykun (HWND hWnd)
/**
Query ueber Kunden.
**/
{

        HANDLE hMainInst;

        static char lmdn [41];
        static char lfil [41];
        static char lkun [41];
        static char lkun_krz1 [41];
        static char lort1 [41];

        static ITEM imdnval ("mdn", 
                              lmdn, 
                              "Mandant      ", 
                              0);

        static ITEM ifilval ("fil", 
                              lfil, 
                              "Filiale      ", 
                              0);

        static ITEM ikunval ("kun", 
                              lkun, 
                              "Kunden-Nr      ", 
                              0);

        static ITEM ikun_krz1val ("kun_krz1", 
                                   lkun_krz1, 
                                   "Kurz-Name      ", 
                                   0);

        static ITEM ikun_ort1val   ("ort1", 
                                   lort1, 
                                   "Ort            ", 
                                   0);


        static field _qtxtform[] = {
           &ikunval,      40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krz1val, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_ort1val, 40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,          15, 0, 5,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,          15, 0, 5,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform = {5, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};

        static char *qnamen[] = {"kun.kun", "kun.kun_krz1", 
                                 "adr.ort1",  NULL};

// Form fuer uebergabe an preparequery mit Mandant 
        
        static field _qtxtform0[] = {
           &imdnval,      40, 0, 1,0, 0, "", NORMAL, 0, testquery,0,
           &ifilval,      40, 0, 1,0, 0, "", NORMAL, 0, testquery,0,
           &ikunval,      40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krz1val, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_ort1val, 40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,          15, 0, 5,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,          15, 0, 5,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform0 = {7, 0, 0, _qtxtform0, 
			                    0, 0, 0, 0, NULL};
        static char *qnamen0[] = {"kun.mdn", "kun.fil", 
			                      "kun.kun", "kun.kun_krz1", 
                                  "adr.ort1",  NULL};

        HWND query;
		int savefield;
		form *savecurrent;

        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (8);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        SetFkt (6, leer, NULL);
        set_fkt (NULL, 7);
        SetFkt (7, leer, NULL);
        set_fkt (NULL, 9);
        SetFkt (9, leer, NULL);
        set_fkt (NULL, 8);
        SetFkt (8, leer, NULL);
		set_fkt (NULL, 11);
		set_fkt (NULL, 12);
 
		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (9, 60, 11, 10, hMainInst,
                               "Suchkriterien f�r Kunden");
        enter_form (query, &qtxtform, 0, 0);

		SetButtonTab (FALSE);
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetAktivWindow (hWnd);
        if (syskey == KEY12)
        {
			      if (lutz_mdn_par)
				  {
                    if (kun_class.PrepareQueryMdn (&qtxtform, qnamen) == 0)
					{
                              kun_class.ShowBuQuery (hWnd, 0);
					}
                    else
					{
                      syskey = KEY5;
					}
				  }
				  else
				  {
                    sprintf (lmdn, "%hd", kun.mdn); 
                    sprintf (lfil, "%hd", kun.fil); 
                    if (kun_class.PrepareQuery (&qtxtform0, qnamen0) == 0)
					{
                              kun_class.ShowBuQuery (hWnd, 0);
					}
                    else
					{
                      syskey = KEY5;
					}
				  }
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (9);
        restore_fkt (8);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}


int QueryClass::searchkun_direct (HWND hWnd, char *kun_krz1)
/**
Query ueber Kunden.
**/
{
 	    int savefield;
		form *savecurrent;

        HANDLE hMainInst;

        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetAktivWindow (hWnd);

		kun_class.SetCharBuffKun (kun_krz1);
		if (lutz_mdn_par)
		{
                kun_class.PrepareQuery ();
		}
		else
		{
                kun_class.PrepareQueryMdn ();
		}
        kun_class.ShowBuQuery (hWnd, 1);

		currentfield = savefield;
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5)
		{
			strcpy (kun_krz1, "0");
			return FALSE;
		}
		sprintf (kun_krz1, "%8ld", kun.kun);
        return TRUE;
}


int QueryClass::queryaufex (HWND hWnd, BOOL que)
/**
Query ueber Auftr�ge.
**/
{

        HANDLE hMainInst;

        static char mdnval[41];
        static char filval[41];
        static char aufval[41];
        static char kunfilval[41];
        static char kunval[41];
        static char ldatval[41];
        static char statval[41];
        static char ccmarktval[41];

        static ITEM imdnval ("mdn", 
                             mdnval, 
                             "Mandanten-Nr      ", 

                             0);
        static ITEM ifilval ("fil", 
                             filval, 
                             "Filiale-Nr        ", 
                             0);


        static ITEM iaufval ("auf", 
                             aufval, 
                             "Auftrag-Nr        ", 
                             0);

        static ITEM ikunfilval ("kun_fil", 
                                 kunfilval, 
                                "Kunde/Filiale     ", 
                                 0);

        static ITEM ikunval    ("kun", 
                                 kunval, 
                                "Kunde             ", 
                                 0);

        static ITEM ildatval ("lieferdat", 
                             ldatval, 
                             "Lieferdatum       ", 
                             0);

        static ITEM istatval("auf_stat", 
                             statval, 
                             "Status            ",
                             0);
        static ITEM iccmarktval("ccmarkt", 
                              ccmarktval, 
                             "C+C-Markt         ",
                             0);

        static field _qtxtform[] = {
           &iaufval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &iccmarktval, 
		                40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 8,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0, 8,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform = {8, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"auf", "kun_fil", "kun", 
                                 "lieferdat", "auf_stat", "ccmarkt",
                                  NULL};
        static field _qtxtform0[] = {
           &imdnval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ifilval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &iaufval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &iccmarktval, 
		                40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 8,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0, 8,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform0 = {10, 0, 0, _qtxtform0, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen0[] = {"aufk.mdn" "aufk.fil", 
			                      "auf", "kun_fil", "kun", 
                                 "lieferdat", "auf_stat", "ccmarkt",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		strcpy (statval, "<5");
		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        
		if (que)
		{
                   query = OpenWindowChC (11, 62, 9, 10, hMainInst,
                               "Suchkriterien f�r Auftr�ge");
                   enter_form (query, &qtxtform, 0, 0);

                   CloseControls (&qtxtform);
                   DestroyWindow (query);
		}
		else
		{
   		           strcpy (statval, "1");
		           strcpy (ccmarktval, "1");
                   syskey = 0;
		}

		SetButtonTab (FALSE);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                  if (lutz_mdn_par)
				  {
                       if (ls_class.PrepareAufQueryEx (&qtxtform, qnamen) == 0)
					   {
                             ls_class.ShowBuAufQueryEx (hWnd, 0);
					   }
                       else
					   {
                             syskey = KEY5;
					   }
				  }
				  else
				  {
                       sprintf (mdnval, "%hd", aufk.mdn); 
                       sprintf (filval, "%hd", aufk.fil); 
                       if (ls_class.PrepareAufQueryEx (&qtxtform0, qnamen0) == 0)
					   {
                             ls_class.ShowBuAufQueryEx (hWnd, 0);
					   }
                       else
					   {
                             syskey = KEY5;
					   }
				  }
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (9);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}

int QueryClass::queryang (HWND hWnd)
/**
Query ueber Angebote.
**/
{

        HANDLE hMainInst;

        static char mdnval[41];
        static char filval[41];
        static char angval[41];
        static char kunfilval[41];
        static char kunval[41];
        static char kun_krzval [41];
        static char ortval [41];
        static char ldatval[41];
        static char statval[41];

        static ITEM imdnval ("mdn", 
                              mdnval, 
                             "Mandanten-Nr      ", 

                             0);
        static ITEM ifilval ("fil", 
                             filval, 
                             "Filiale-Nr        ", 
                             0);

        static ITEM iangval ("ang", 
                             angval, 
                                "Angebots-Nr       ", 
                             0);

        static ITEM ikunfilval ("kun_fil", 
                                 kunfilval, 
                                "Kunde/Filiale     ", 
                                 0);

        static ITEM ikunval    ("kun", 
                                 kunval, 
                                "Kunde             ", 
                                 0);

        static ITEM ikun_krzval    
			                   ("kun", 
                                 kun_krzval, 
                                "Name              ", 
                                 0);

        static ITEM iortval    
			                   ("ort1", 
                                 ortval, 
                                "Ort               ", 
                                 0);

        static ITEM ildatval ("lieferdat", 
                             ldatval, 
                             "Lieferdatum       ", 
                             0);

        static ITEM istatval("ang_stat", 
                             statval, 
                             "Status            ",
                             0);

        static field _qtxtform[] = {
           &iangval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krzval,40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iortval,    40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 7,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 9,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0, 9,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform = {9, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"ang", "kun_fil", "kun", "kun_krz1", "adr.ort1",
                                 "lieferdat", "ang_stat",
                                  NULL};

        static field _qtxtform0[] = {
           &imdnval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ifilval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &iangval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krzval,40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iortval,    40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 7,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 9,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0, 9,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform0 = {11, 0, 0, _qtxtform0, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen0[] = {"angk.mdn", "angk.fil", "ang", "kun_fil", "kun", "kun_krz1", "adr.ort1",
                                 "lieferdat", "ang_stat",
                                  NULL};
        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		strcpy (statval, "<5");
		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (12,62, 9, 10, hMainInst,
                               "Suchkriterien f�r Angebote");
        enter_form (query, &qtxtform, 0, 0);

		SetButtonTab (FALSE);
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                  if (lutz_mdn_par)
				  {
                       if (angk_class.PrepareAngQueryEx (&qtxtform, qnamen) == 0)
					   {
                             angk_class.ShowBuAngQueryEx (hWnd, 0);
					   }
                       else
					   {
                             syskey = KEY5;
					   }
				  }
				  else
				  {
                       sprintf (mdnval, "%hd", aufk.mdn); 
                       sprintf (filval, "%hd", aufk.fil); 
                       if (angk_class.PrepareAngQueryEx (&qtxtform0, qnamen0) == 0)
					   {
                             angk_class.ShowBuAngQueryEx (hWnd, 0);
					   }
                       else
					   {
                             syskey = KEY5;
					   }
				  }
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (9);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}


int QueryClass::queryangex (HWND hWnd, BOOL que)
/**
Query ueber Angebote.
**/
{

        HANDLE hMainInst;

        static char mdnval[41];
        static char filval[41];
        static char angval[41];
        static char kunfilval[41];
        static char kunval[41];
        static char ldatval[41];
        static char statval[41];
        static char ccmarktval[41];

        static ITEM iangval ("ang", 
                             angval, 
                                "Angebots-Nr       ", 
                             0);

        static ITEM ikunfilval ("kun_fil", 
                                 kunfilval, 
                                "Kunde/Filiale     ", 
                                 0);

        static ITEM ikunval    ("kun", 
                                 kunval, 
                                "Kunde             ", 
                                 0);

        static ITEM ildatval ("lieferdat", 
                             ldatval, 
                             "Lieferdatum       ", 
                             0);

        static ITEM istatval("ang_stat", 
                             statval, 
                             "Status            ",
                             0);
        static ITEM iccmarktval("ccmarkt", 
                              ccmarktval, 
                             "C+C-Markt         ",
                             0);

        static field _qtxtform[] = {
           &iangval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &iccmarktval, 
		                40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0, 8,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0, 8,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform = {8, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"ang", "kun_fil", "kun", 
                                 "lieferdat", "ang_stat", "ccmarkt",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		strcpy (statval, "<5");
		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        
		if (que)
		{
                   query = OpenWindowChC (11, 62, 9, 10, hMainInst,
                               "Suchkriterien f�r Angebote");
                   enter_form (query, &qtxtform, 0, 0);

                   CloseControls (&qtxtform);
                   DestroyWindow (query);
		}
		else
		{
   		           strcpy (statval, "1");
		           strcpy (ccmarktval, "1");
                   syskey = 0;
		}

		SetButtonTab (FALSE);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                  if (angk_class.PrepareAngQueryEx (&qtxtform, qnamen) == 0)
                  {
                             angk_class.ShowBuAngQueryEx (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}

int QueryClass::queryauftou (HWND hWnd)
/**
Query ueber Auftr�ge.
**/
{

        HANDLE hMainInst;

        static char mdnval[41];
        static char filval[41];
        static char aufval[41];
        static char kunfilval[41];
        static char kunval[41];
        static char kun_krzval [41];
        static char ortval [41];
        static char ldatval[41];
        static char ltouval[41];
        static char statval[41];

        static ITEM imdnval ("mdn", 
                             mdnval, 
                             "Mandanten-Nr      ", 

                             0);
        static ITEM ifilval ("fil", 
                             filval, 
                             "Filiale-Nr        ", 
                             0);


        static ITEM iaufval ("auf", 
                             aufval, 
                             "Auftrag-Nr        ", 
                             0);

        static ITEM ikunfilval ("kun_fil", 
                                 kunfilval, 
                                "Kunde/Filiale     ", 
                                 0);

        static ITEM ikunval    ("kun", 
                                 kunval, 
                                "Kunde             ", 
                                 0);

        static ITEM ikun_krzval    
			                   ("kun", 
                                 kun_krzval, 
                                "Name              ", 
                                 0);

        static ITEM iortval    
			                   ("ort1", 
                                 ortval, 
                                "Ort               ", 
                                 0);

        static ITEM ildatval ("lieferdat", 
                             ldatval, 
                             "Lieferdatum       ", 
                             0);

        static ITEM itouval  ("tou_nr", 
                              ltouval, 
                             "Tour              ", 
                             0);

        static ITEM istatval("auf_stat", 
                             statval, 
                             "Status            ",
                             0);

        static field _qtxtform[] = {
           &iaufval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krzval,40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iortval,    40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &itouval,    40, 0, 7,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 8,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0,10,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0,10,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform = {10, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"auf", "kun_fil", "kun", "kun_krz1", "adr.ort1",
                                 "lieferdat", "tou_nr", "auf_stat",
                                  NULL};

        static field _qtxtform0[] = {
           &imdnval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ifilval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &iaufval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krzval,40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iortval,    40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &itouval,    40, 0, 7,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 8,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0,10,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0,10,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform0 = {12, 0, 0, _qtxtform0, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen0[] = {"aufk.mdn", "aufk.fil","auf", 
			                      "kun_fil", "kun", "kun_krz1", "adr.ort1",
                                 "lieferdat", "tou_nr", "auf_stat",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		strcpy (statval, "<5");
		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (13,62, 9, 10, hMainInst,
                               "Suchkriterien f�r Auftr�ge");
        enter_form (query, &qtxtform, 0, 0);

		SetButtonTab (FALSE);
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEYESC)
        {
			if (lutz_mdn_par)
			{
                  if (ls_class.PrepareAufQueryEx (&qtxtform, qnamen) == 0)
                  {
                             ls_class.ShowBuAufQueryEx (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
			}
			else
			{
                  sprintf (mdnval, "%hd", aufk.mdn); 
                  sprintf (filval, "%hd", aufk.fil); 
                  if (ls_class.PrepareAufQueryEx (&qtxtform0, qnamen0) == 0)
                  {
                             ls_class.ShowBuAufQueryEx (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
			}

        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (9);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}


int QueryClass::queryauflietou (HWND hWnd)
/**
Query ueber Auftr�ge.
**/
{

        HANDLE hMainInst;

        static char mdnval[41];
        static char filval[41];
        static char aufval[41];
        static char kunfilval[41];
        static char kunval[41];
        static char kun_krzval [41];
        static char ortval [41];
        static char ldatval[41];
        static char ltouval[41];
        static char statval[41];

        static ITEM imdnval ("mdn", 
                             mdnval, 
                             "Mandanten-Nr      ", 

                             0);
        static ITEM ifilval ("fil", 
                             filval, 
                             "Filiale-Nr        ", 
                             0);


        static ITEM iaufval ("auf", 
                             aufval, 
                             "Auftrag-Nr        ", 
                             0);

        static ITEM ikunfilval ("kun_fil", 
                                 kunfilval, 
                                "Kunde/Filiale     ", 
                                 0);

        static ITEM ikunval    ("kun", 
                                 kunval, 
                                "Kunde             ", 
                                 0);

        static ITEM ikun_krzval    
			                   ("kun", 
                                 kun_krzval, 
                                "Name              ", 
                                 0);

        static ITEM iortval    
			                   ("ort1", 
                                 ortval, 
                                "Ort               ", 
                                 0);

        static ITEM ildatval ("lieferdat", 
                             ldatval, 
                             "Lieferdatum       ", 
                             0);

        static ITEM itouval  ("tou_nr", 
                              ltouval, 
                             "Tour              ", 
                             0);

        static ITEM istatval("auf_stat", 
                             statval, 
                             "Status            ",
                             0);

        static field _qtxtform[] = {
           &iaufval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krzval,40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iortval,    40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &itouval,    40, 0, 7,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 8,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0,10,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0,10,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform = {10, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"auf", "kun_fil", "kun", "kun_krz1", "adr.ort1",
                                 "lieferdat", "tou_nr", "auf_stat",
                                  NULL};

        static field _qtxtform0[] = {
           &imdnval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ifilval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &iaufval,    40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ikunfilval, 40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ikunval,    40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ikun_krzval,40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &iortval,    40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ildatval,   40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &itouval,    40, 0, 7,1, 0, "", NORMAL, 0, testquery,0,
           &istatval,   40, 0, 8,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,        15, 0,10,15, 0, "", BUTTON, 0,testquery,KEY12,
           &iCA,        15, 0,10,32, 0, "", BUTTON, 0,testquery,KEY5,
		};

        static form qtxtform0 = {12, 0, 0, _qtxtform0, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen0[] = {"aufk.mdn", "aufk.fil","auf", 
			                      "kun_fil", "kun", "kun_krz1", "adr.ort1",
                                 "lieferdat", "tou_nr", "auf_stat",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;
        
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		strcpy (statval, "<5");
		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (13,62, 9, 10, hMainInst,
                               "Suchkriterien f�r Auftr�ge");
        enter_form (query, &qtxtform, 0, 0);

		SetButtonTab (FALSE);
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                  sprintf (mdnval, "%hd", aufk.mdn); 
                  sprintf (filval, "%hd", aufk.fil); 
                  if (ls_class.PrepareAufQueryLieTou (&qtxtform0, qnamen0) == 0)
                  {
                             ls_class.ShowBuAufQueryLieTou (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }

        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (9);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}

