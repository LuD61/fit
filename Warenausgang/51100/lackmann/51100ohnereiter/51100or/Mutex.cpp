// Mutex.cpp: Implementierung der Klasse CMutex.
//
//////////////////////////////////////////////////////////////////////

#include "Mutex.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CMutex::CMutex()
{
	m_hMutex = ::CreateMutexW(NULL, false, NULL);
}

CMutex::CMutex(LPCSTR Name)
{
	m_hMutex = ::CreateMutex(NULL, false, Name);
}

CMutex::~CMutex()
{
	if (m_hMutex != NULL) 
	{
		CloseHandle(m_hMutex);
		m_hMutex = NULL;
	}

}

int CMutex::WaitForSingleObject(long Timeout)
{
	int RetCode = MUTEX_ERROR;
	unsigned long Result = ::WaitForSingleObject(m_hMutex, Timeout);
	switch (Result)
	{
	case WAIT_TIMEOUT:
		{
			RetCode = MUTEX_TIMEOUT;
		}break;
	case WAIT_OBJECT_0:
		{
			RetCode = MUTEX_SUCCESS;
		}break;
	case WAIT_ABANDONED:
		{
			RetCode = MUTEX_ERROR;
		}break;
	default:
	case WAIT_FAILED:
		{
			RetCode = MUTEX_ERROR;
		}break;
	}
	return (RetCode);
}


bool CMutex::Release()
{
	bool RetCode = false;
	if (::ReleaseMutex(m_hMutex))
	{
		RetCode = true;
	}
	return (RetCode);
}