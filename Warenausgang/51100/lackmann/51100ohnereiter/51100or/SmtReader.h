// SmtReader.h: Schnittstelle f�r die Klasse CSmtReader.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SMTREADER_H__E501616C_02DD_4BF3_982C_84C19FA7ED96__INCLUDED_)
#define AFX_SMTREADER_H__E501616C_02DD_4BF3_982C_84C19FA7ED96__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CSmtReader  
{
public:
	CSmtReader();
	virtual ~CSmtReader();
	virtual short Get (double a)=0;
	virtual double Get_inh_abverk ()=0;
	virtual short Get_hwg ()=0;
	virtual double Get_a_gew ()=0;

};

#endif // !defined(AFX_SMTREADER_H__E501616C_02DD_4BF3_982C_84C19FA7ED96__INCLUDED_)
