// KUnden2.h : header file
//

// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1997 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#ifndef _KUnden2_H
#define _KUnden2_H
/////////////////////////////////////////////////////////////////////////////
// CKunden2Page dialog

class CKunden2Page : public CPropertyPage
{
// Construction
public:
	CKunden2Page();   // standard constructor

// Dialog Data
	//{{AFX_DATA(CKunden2Page)
	enum { IDD = IDD_KUNDEN1 };
	//}}AFX_DATA
/*	CKunden2 m_AnimateCtrl;
	CRect m_rectAnimateCtrl;
    DWORD m_dwStyle;   // control styles*/
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKunden2Page)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation
protected:


	// Generated message map functions
	//{{AFX_MSG(CKunden2Page)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif
