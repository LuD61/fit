// ClientOrderThread.cpp: Implementierung der Klasse CClientOrderThread.
//
//////////////////////////////////////////////////////////////////////

#include "ClientOrderThread.h"
#include "DbClass.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CClientOrderThread::CClientOrderThread() : 
			        CClientOrderCom (), CThread ()
{
	ThreadAction = NULL;
}

CClientOrderThread::~CClientOrderThread()
{

}

unsigned int CClientOrderThread::Run()
{
	unsigned int RetCode = 0;


	if (StartServer ())
	{
		CClientOrderCom::Load ();
		CClientOrderCom::UnregisterServer ();
		if (ThreadAction != NULL)
		{
			ThreadAction->Execute (NULL);
		}
		RetCode = 1;
	}
	else
	{
		if (ThreadAction != NULL)
		{
			ThreadAction->Execute ("FAILED");
		}
	}
	return RetCode;
}


void CClientOrderThread::Terminate()
{
	if (m_Mutex != NULL) m_Mutex->WaitForSingleObject(0xFFFFFFFF);
	SetStopWorking (TRUE);
	if (m_Mutex != NULL) m_Mutex->Release();
	Kill ();
	CClientOrderCom::UnregisterServer ();
}
