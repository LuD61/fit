#include <windows.h>
#include "adr.h"

struct ADREX adr, adr_null;

void CAdr::prepare (void)
{
            char *sqltext;

            sqlin ((long *)   &adr.adr,  SQLLONG, 0);
    out_quest ((char *) &adr.adr,2,0);
    out_quest ((char *) adr.adr_krz,0,17);
    out_quest ((char *) adr.adr_nam1,0,37);
    out_quest ((char *) adr.adr_nam2,0,37);
    out_quest ((char *) &adr.adr_typ,1,0);
    out_quest ((char *) adr.adr_verkt,0,17);
    out_quest ((char *) &adr.anr,1,0);
    out_quest ((char *) &adr.delstatus,1,0);
    out_quest ((char *) adr.fax,0,21);
    out_quest ((char *) &adr.fil,1,0);
    out_quest ((char *) &adr.geb_dat,2,0);
    out_quest ((char *) &adr.land,1,0);
    out_quest ((char *) &adr.mdn,1,0);
    out_quest ((char *) adr.merkm_1,0,3);
    out_quest ((char *) adr.merkm_2,0,3);
    out_quest ((char *) adr.merkm_3,0,3);
    out_quest ((char *) adr.merkm_4,0,3);
    out_quest ((char *) adr.merkm_5,0,3);
    out_quest ((char *) adr.modem,0,21);
    out_quest ((char *) adr.ort1,0,37);
    out_quest ((char *) adr.ort2,0,37);
    out_quest ((char *) adr.partner,0,37);
    out_quest ((char *) adr.pf,0,17);
    out_quest ((char *) adr.plz,0,9);
    out_quest ((char *) &adr.staat,1,0);
    out_quest ((char *) adr.str,0,37);
    out_quest ((char *) adr.tel,0,21);
    out_quest ((char *) adr.telex,0,21);
    out_quest ((char *) &adr.txt_nr,2,0);
    out_quest ((char *) adr.plz_postf,0,9);
    out_quest ((char *) adr.plz_pf,0,9);
    out_quest ((char *) adr.iln,0,33);
    out_quest ((char *) adr.email,0,37);
    out_quest ((char *) adr.swift,0,25);
    out_quest ((char *) adr.adr_nam3,0,37);
            cursor = sqlcursor ("select adr.adr,  adr.adr_krz,  "
"adr.adr_nam1,  adr.adr_nam2,  adr.adr_typ,  adr.adr_verkt,  adr.anr,  "
"adr.delstatus,  adr.fax,  adr.fil,  adr.geb_dat,  adr.land,  adr.mdn,  "
"adr.merkm_1,  adr.merkm_2,  adr.merkm_3,  adr.merkm_4,  adr.merkm_5,  "
"adr.modem,  adr.ort1,  adr.ort2,  adr.partner,  adr.pf,  adr.plz,  adr.staat,  "
"adr.str,  adr.tel,  adr.telex,  adr.txt_nr,  adr.plz_postf,  adr.plz_pf,  "
"adr.iln,  adr.email,  adr.swift,  adr.adr_nam3 from adr "

#line 12 "adr.rpp"
                                  "where adr = ?");
    ins_quest ((char *) &adr.adr,2,0);
    ins_quest ((char *) adr.adr_krz,0,17);
    ins_quest ((char *) adr.adr_nam1,0,37);
    ins_quest ((char *) adr.adr_nam2,0,37);
    ins_quest ((char *) &adr.adr_typ,1,0);
    ins_quest ((char *) adr.adr_verkt,0,17);
    ins_quest ((char *) &adr.anr,1,0);
    ins_quest ((char *) &adr.delstatus,1,0);
    ins_quest ((char *) adr.fax,0,21);
    ins_quest ((char *) &adr.fil,1,0);
    ins_quest ((char *) &adr.geb_dat,2,0);
    ins_quest ((char *) &adr.land,1,0);
    ins_quest ((char *) &adr.mdn,1,0);
    ins_quest ((char *) adr.merkm_1,0,3);
    ins_quest ((char *) adr.merkm_2,0,3);
    ins_quest ((char *) adr.merkm_3,0,3);
    ins_quest ((char *) adr.merkm_4,0,3);
    ins_quest ((char *) adr.merkm_5,0,3);
    ins_quest ((char *) adr.modem,0,21);
    ins_quest ((char *) adr.ort1,0,37);
    ins_quest ((char *) adr.ort2,0,37);
    ins_quest ((char *) adr.partner,0,37);
    ins_quest ((char *) adr.pf,0,17);
    ins_quest ((char *) adr.plz,0,9);
    ins_quest ((char *) &adr.staat,1,0);
    ins_quest ((char *) adr.str,0,37);
    ins_quest ((char *) adr.tel,0,21);
    ins_quest ((char *) adr.telex,0,21);
    ins_quest ((char *) &adr.txt_nr,2,0);
    ins_quest ((char *) adr.plz_postf,0,9);
    ins_quest ((char *) adr.plz_pf,0,9);
    ins_quest ((char *) adr.iln,0,33);
    ins_quest ((char *) adr.email,0,37);
    ins_quest ((char *) adr.swift,0,25);
    ins_quest ((char *) adr.adr_nam3,0,37);
            sqltext = "update adr set adr.adr = ?,  "
"adr.adr_krz = ?,  adr.adr_nam1 = ?,  adr.adr_nam2 = ?,  "
"adr.adr_typ = ?,  adr.adr_verkt = ?,  adr.anr = ?,  adr.delstatus = ?,  "
"adr.fax = ?,  adr.fil = ?,  adr.geb_dat = ?,  adr.land = ?,  adr.mdn = ?,  "
"adr.merkm_1 = ?,  adr.merkm_2 = ?,  adr.merkm_3 = ?,  adr.merkm_4 = ?,  "
"adr.merkm_5 = ?,  adr.modem = ?,  adr.ort1 = ?,  adr.ort2 = ?,  "
"adr.partner = ?,  adr.pf = ?,  adr.plz = ?,  adr.staat = ?,  adr.str = ?,  "
"adr.tel = ?,  adr.telex = ?,  adr.txt_nr = ?,  adr.plz_postf = ?,  "
"adr.plz_pf = ?,  adr.iln = ?,  adr.email = ?,  adr.swift = ?,  "
"adr.adr_nam3 = ? "

#line 14 "adr.rpp"
                                  "where adr = ?";
            sqlin ((long *)   &adr.adr,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &adr.adr,  SQLLONG, 0);
            test_upd_cursor = sqlcursor ("select adr from adr "
                                  "where adr = ?");
            sqlin ((long *)   &adr.adr,  SQLLONG, 0);
            del_cursor = sqlcursor ("delete from adr "
                                  "where adr = ?");
    ins_quest ((char *) &adr.adr,2,0);
    ins_quest ((char *) adr.adr_krz,0,17);
    ins_quest ((char *) adr.adr_nam1,0,37);
    ins_quest ((char *) adr.adr_nam2,0,37);
    ins_quest ((char *) &adr.adr_typ,1,0);
    ins_quest ((char *) adr.adr_verkt,0,17);
    ins_quest ((char *) &adr.anr,1,0);
    ins_quest ((char *) &adr.delstatus,1,0);
    ins_quest ((char *) adr.fax,0,21);
    ins_quest ((char *) &adr.fil,1,0);
    ins_quest ((char *) &adr.geb_dat,2,0);
    ins_quest ((char *) &adr.land,1,0);
    ins_quest ((char *) &adr.mdn,1,0);
    ins_quest ((char *) adr.merkm_1,0,3);
    ins_quest ((char *) adr.merkm_2,0,3);
    ins_quest ((char *) adr.merkm_3,0,3);
    ins_quest ((char *) adr.merkm_4,0,3);
    ins_quest ((char *) adr.merkm_5,0,3);
    ins_quest ((char *) adr.modem,0,21);
    ins_quest ((char *) adr.ort1,0,37);
    ins_quest ((char *) adr.ort2,0,37);
    ins_quest ((char *) adr.partner,0,37);
    ins_quest ((char *) adr.pf,0,17);
    ins_quest ((char *) adr.plz,0,9);
    ins_quest ((char *) &adr.staat,1,0);
    ins_quest ((char *) adr.str,0,37);
    ins_quest ((char *) adr.tel,0,21);
    ins_quest ((char *) adr.telex,0,21);
    ins_quest ((char *) &adr.txt_nr,2,0);
    ins_quest ((char *) adr.plz_postf,0,9);
    ins_quest ((char *) adr.plz_pf,0,9);
    ins_quest ((char *) adr.iln,0,33);
    ins_quest ((char *) adr.email,0,37);
    ins_quest ((char *) adr.swift,0,25);
    ins_quest ((char *) adr.adr_nam3,0,37);
            ins_cursor = sqlcursor ("insert into adr (adr,  "
"adr_krz,  adr_nam1,  adr_nam2,  adr_typ,  adr_verkt,  anr,  delstatus,  fax,  fil,  "
"geb_dat,  land,  mdn,  merkm_1,  merkm_2,  merkm_3,  merkm_4,  merkm_5,  modem,  ort1,  "
"ort2,  partner,  pf,  plz,  staat,  str,  tel,  telex,  txt_nr,  plz_postf,  plz_pf,  iln,  "
"email,  swift,  adr_nam3) "

#line 25 "adr.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 27 "adr.rpp"
}
