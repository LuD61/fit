#ifndef _eDlgDef
#define _eDlgDef

class DlgClass
{
      private :
             char **SwSaetze;
             char *eingabesatz;
             int recanz;
             int lPagelen;
             int scrollpos;
             int LineRow;
             HWND    hwndTB;
             HWND    hTrack;
             HWND    vTrack;

             char frmrow [0x1000];
             TEXTMETRIC tm;
             PAINTSTRUCT aktpaint;

             BOOL IsArrow;
             HCURSOR oldcursor;
             HCURSOR arrow;
             HCURSOR aktcursor;
             int movfield; 
             int InMove;
			 RECT FocusRect;

      public :
             HANDLE  hMainInst;
             HWND    hMainWindow; 
             HWND    mamain2; 
             HWND    mamain3; 
             form UbForm;
             form DataForm;
             form LineForm;
             int banz;
			 int WithFocus;
			 int  AktRow;
			 int AktColumn;
			 char AktItem [21];
			 char AktValue [80];
			 int  ListFocus;
			 HWND FocusWindow;

			 char *GetAktItem (void)
			 {
				 return AktItem;
			 }

			 char *GetAktValue (void)
			 {
				 return AktValue;
			 }

             DlgClass ()
             {
                    IsArrow = FALSE;
                    oldcursor = NULL;;
                    arrow = NULL;
                    aktcursor = NULL;
                    movfield = 0; 
                    InMove = 0;
					AktRow = 0;
					AktColumn = 0;
					WithFocus = 1;
					recanz = 0;
					banz = 0;
					ListFocus = 1;
					FocusWindow = NULL;
             }

             void SetPos (int zeile, int spalte)
             {
					AktRow = zeile;
					AktColumn = spalte;
             }

             void Initscrollpos (void)
             {
                 scrollpos = 0;
             }

             void InitRecanz (void)
             {
                 recanz = 0;
             }

             int  GetRecanz (void)
             {
                 return recanz;
             }

             void SethwndTB (HWND hwndTB)
             {
                 this->hwndTB = hwndTB;
             }

             void SetSaetze (char **Saetze)
             {
                 SwSaetze = Saetze;
             }

             void Setbanz (int banz)
             {
                 this->banz = banz;
             }

             void SetRecanz (int anz)
             {
                 recanz = anz;
             }

             void SetPagelen (int Pagelen)
             {
                 lPagelen = Pagelen;
             }

             void SetLineRow (int LineRow)
             {
                 this->LineRow = LineRow;
             }

             void SetTextMetric (TEXTMETRIC *ttm)
             {
                 memcpy (&tm, ttm, sizeof (tm));
             }

             void SetAktPaint (PAINTSTRUCT *pm)
             {
                 memcpy (&aktpaint, pm, sizeof (aktpaint));
             }

             HWND Getmamain2 (void)
             {
                  return mamain2;
             }

             HWND Getmamain3 (void)
             {
                  return mamain3;
             }

             HWND     InitListWindow (HWND);
             void     GetPageLen (void);
             BOOL     TrackNeeded (void);
             void     CreateTrack (void);
             void     DestroyTrack (void); 

             BOOL     VTrackNeeded (void);
             void     CreateVTrack (void);
             void     DestroyVTrack (void);
             void     TestTrack (void);
             void     TestVTrack (void);
             void     TestMamain3 (void);
             void     MoveListWindow (void);
             void     SetDataStart (void);
             BOOL     MustPaint (int);
             void     PrintVlineSatzNr (HDC);
             void     PrintVlines (HDC);
             void     PrintHlinesSatzNr (HDC);
             void     PrintHlines (HDC);
			 void     ShowFocusText (HDC, int, int);
			 void     GetFocusText (void);
			 void     ShowWindowText (int, int);
			 void     DestroyFocusWindow (void);
             void     SetFeldEdit (int, int);
			 void     SetFeldFrame (HDC, int, int);
			 void     SetFeldFocus (HDC, int, int);
			 void     SetFeldFocus0 (int, int);
             int      DelRec (void); 
             void     FillFrmRow (char *, form *);
             void     ShowFrmRow (HDC, int);
             void     ShowDataForms (HDC);
             void     ScrollLeft (void);
             void     ScrollRight (void);
             void     ScrollPageDown (void);
             void     ScrollPageUp (void);
             void     SetPos (int);
             void     SetTop (void);
             void     SetBottom (void);
             void     HScroll (WPARAM, LPARAM);
             void     SetVPos (int);
             void     ScrollDown (void);
             void     ScrollUp (void);
             void     ScrollVPageDown (void);
             void     ScrollVPageUp (void);
             void     VScroll (WPARAM, LPARAM);
			 void     FocusLeft (void);
			 void     FocusRight (void);
			 void     FocusUp (void);
			 void     FocusDown (void);
			 BOOL     IsInListArea (void);
             void     OnPaint (HWND, UINT, WPARAM, LPARAM);
             void     OnSize (HWND, UINT, WPARAM, LPARAM);
             void     FunkKeys (WPARAM, LPARAM);
             void     FillUbForm (field *, char *, int, int,int);
             void     FillDataForm (field *, char *, int, int);
             void     FillLineForm (field *, int);
             void     MakeLineForm (form *);
             void     MakeDataForm (form *);
             void     MakeUbForm (void);
             void     MakeDataForm0 (void);
             void     FreeLineForm (void);
             void     FreeDataForm (void);
             void     FreeUbForm (void);
             BOOL     IsMouseMessage (MSG *);
             void     StopMove (void);
             void     StartMove (void);
             void     DestroyField (int);
             void     ShowArrow (BOOL);
             int      IsUbEnd (void);
             int      IsUbRow (void);
			 void      EditScroll (void);
             int      ProcessMessages(void);
};

class DlgClassDB : public DlgClass
{
     public :
            DlgClassDB () : DlgClass ()
            {
            }
            void     FreeBezTab (void);
            void     FillUbForm (field *, char *, int, int,int);
            void     MakeUbForm (void);
};

#endif 
