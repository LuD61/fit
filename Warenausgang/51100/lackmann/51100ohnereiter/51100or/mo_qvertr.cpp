#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "itemc.h"
#include "wmaskc.h"
#include "dbfunc.h"
#include "vertr.h"
#include "mo_meld.h"
#include "mo_qvertr.h"
#include "datum.h"
#include "strfkt.h"
#include "mo_curso.h"

VERTR_CLASS vertr_class;

static int testquery (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
        return 0;
}


int QueryVertr::queryvertr (HWND hWnd)
/**
Query ueber Touren.
**/
{

        HANDLE hMainInst;

        static char vertrval[41];
        static char vertr_krzval[41];
         
        static ITEM ivertrval ("vertr", 
                               vertrval, 
                              "Vertreter-Nr.:", 
                              0);
        static ITEM ivertr_krzval ("adr_krz",
                                  vertr_krzval,
                                 "Vertr.-Name..:",
                                 0);
        static ITEM iOK ("", "     OK     ", "", 0);
        static ITEM iCA ("", "  Abbrechen ", "", 0);

        static field _qtxtform[] = {
           &ivertrval,        40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &ivertr_krzval,    40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,              15, 0, 4,15, 0, "", BUTTON, 0, 0,KEY12,
           &iCA,              15, 0, 4,32, 0, "", BUTTON, 0, 0,KEY5,
		};

        static form qtxtform = {4, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"vertr.vertr", "adr.adr_krz", 
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;

        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);


		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (7, 62,9, 10, hMainInst,
                               "Suchkriterien f�r Vertreter");
        syskey = 0;
        EnableWindows (hWnd, FALSE); 
        enter_form (query, &qtxtform, 0, 0);

        EnableWindows (hWnd, TRUE); 
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetActiveWindow (hWnd);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                  if (vertr_class.PrepareQuery (&qtxtform, qnamen) == 0)
                  {
                             vertr_class.ShowBuQuery (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}

