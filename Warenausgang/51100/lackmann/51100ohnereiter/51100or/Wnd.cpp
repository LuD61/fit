// Wnd.cpp: Implementierung der Klasse CWnd.
//
//////////////////////////////////////////////////////////////////////

#include "Wnd.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CWnd::CWnd()
{
	m_hWnd = NULL;
}

CWnd::~CWnd()
{
	Destroy ();
}


void CWnd::Destroy ()
{
	if (m_hWnd != NULL)
	{
		DestroyWindow (m_hWnd);
		m_hWnd = NULL;
	}
}

CWnd CWnd::operator= (HWND hWnd)
{
	m_hWnd = hWnd;
	return *this;
}

CWnd::operator HWND () const
{
	return m_hWnd;
}

void CWnd::Attach (HWND hWnd)
{
	m_hWnd = hWnd;
}

void CWnd::SetParent (CWnd& parent)
{
	m_hParent = (HWND) parent;
}

void CWnd::SetParent (HWND parent)
{
	m_hParent = parent;
}

void CWnd::SetWindowText (LPSTR windowtext)
{
	if (m_hWnd != NULL)
	{
		::SetWindowText (m_hWnd, windowtext);
	}
}

void CWnd::SetFont (CFont& font)
{
	SendMessage (m_hWnd, WM_SETFONT, (WPARAM) (HFONT) font, 0l);
}
