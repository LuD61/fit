#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <windows.h>
#include "strfkt.h"
#include "mo_inf.h"
#include "dbclass.h"
#include "cmask.h"

static DWORD BuOk = 900;

mfont TextFont1 = {
//                  "Comic Sans MS", 
//                  "Times New Roman", 
                  "Impact", 
                   150, 
                   0, 
                   0, 
                   BLUECOL, 
                   LTGRAYCOL,
                   0, 0};


mfont TextFont = {
//                  "Times New Roman", 
                  "Courier New", 
                   120, 
                   0, 
                   1, 
                   BLACKCOL, 
                   LTGRAYCOL,
                   0, 0};

mfont TextFontRed = {
                  "Times New Roman", 
//                  "Impact", 
                   120, 
                   0, 
                   1, 
                   YELLOWCOL, 
                   LTGRAYCOL,
                   0, 0};

mfont BuFont = {
                  "Times New Roman", 
                   120, 
                   0, 
                   1, 
                   BLACKCOL, 
                   LTGRAYCOL,
                   0, 0};


static char *text[10]; 

static mfont *textfont[] = {&TextFont1, 
                            &TextFont,
							&TextFont,
							&TextFont,
							&TextFont,
							&BuFont,
							&TextFont,
							&TextFont,
							&TextFont,
							&TextFont,
							&TextFont,
							NULL,
};


CFIELD Text1  ("Text1",text[0], 0, 0, 1, 0,  NULL, "", DISPLAYONLY,
			                NULL, textfont[0], FALSE, TRANSPARENT); 		  
CFIELD Text2 ("Text2", text[1], 0, 0, 1, 2,  NULL, "", DISPLAYONLY,
			                NULL, textfont[1], FALSE, TRANSPARENT); 		  
CFIELD Text3 ("Text3", text[2], 0, 0, 1, 3,  NULL, "", DISPLAYONLY,
			                NULL, textfont[2], FALSE, TRANSPARENT); 		  
CFIELD Text4 ("Text4", text[3], 0, 0, 1, 4,  NULL, "", DISPLAYONLY,
			                NULL, textfont[3], FALSE, TRANSPARENT);
 		  
CFIELD Text5 ("Text5", text[4], 0, 0, 1, 5,  NULL, "", DISPLAYONLY,
			                NULL, textfont[4], FALSE, TRANSPARENT);

CFIELD Text6 ("Text6", text[5], 0, 0, 1, 6,  NULL, "", DISPLAYONLY,
			                NULL, textfont[4], FALSE, TRANSPARENT);

CFIELD Text7 ("OK",    "  OK ", 10, 0, -1, 7,  NULL, "", CBUTTON,
			                BuOk, textfont[5], FALSE, TRANSPARENT); 

CFIELD *TextField[] = {&Text1, &Text2, &Text3, &Text4, &Text5, &Text6, &Text7};

CFORM TextForm (7, TextField);  		  


CFIELD Text8 ("Text6", text[5], 0, 0, 1, 7,  NULL, "", DISPLAYONLY,
			                NULL, &TextFontRed, FALSE, TRANSPARENT);

CFIELD Text9 ("OK",    "  OK ", 10, 0, -1, 8,  NULL, "", CBUTTON,
			                BuOk, textfont[5], FALSE, TRANSPARENT); 

CFIELD *TextFieldLong[] = {&Text1, &Text2, &Text3, &Text4, &Text5, &Text6, &Text8, &Text9};

CFORM TextFormLong (8, TextFieldLong);  		  



void KINFO::InfoKun (HANDLE hInstance, HWND hMainWindow, short mdn, long kun)
{

	      int dsqlstatus;
		  short pr_stu;
		  long pr_lst;
		  int i;
		  char zus_bez[25];
		  char zus_bez_kun[25];


		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin  ((long *)  &kun, 2, 0);
		  sqlout ((short *) &pr_stu,  1, 0);
		  sqlout ((long *)  &pr_lst, 2, 0);
		  dsqlstatus = sqlcomm ("select pr_stu, pr_lst from kun where mdn = ? and kun = ?");
		  if (dsqlstatus != 0) return;

		  strcpy (zus_bez, " ");
		  strcpy (zus_bez_kun, " ");
		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin ((short *) &pr_stu,  1, 0);
		  sqlout ((char *) zus_bez, 0, 25);
		  dsqlstatus = sqlcomm ("select zus_bz from iprgrstufk where mdn = ? "
			                                                    "and pr_gr_stuf = ?");

		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin ((short *) &pr_lst,  1, 0);
		  sqlout ((char *) zus_bez_kun, 0, 25);
		  dsqlstatus = sqlcomm ("select zus_bz from i_kun_prk where mdn = ? "
			                                                  "and kun_pr = ?");
		  for (i = 0; i < lines; i ++)
		  {
		             text[i] = (char *) GlobalAlloc (GMEM_FIXED, 80);
		  }

		  for (i = 0; i < lines; i ++)
		  {
			  if (text[i] == NULL) break;
		  }

		  if (i < lines)
		  {
			  for (i = 0; i < lines; i ++)
			  {
				  if (text[i]) GlobalFree (text[i]);
			  }
			  return;
		  }

		  sprintf (text[0], "  Preisgruppen Kunde %ld  ", kun);
		  sprintf (text[1], " ");
		  sprintf (text[2], "   Preisgruppenstufe %hd %s", pr_stu, zus_bez);
		  sprintf (text[3], " ");
		  sprintf (text[4], "   Kundenpreisliste   %hd %s", pr_lst, zus_bez_kun);
		  text[5] = NULL;


		  Msg.CMessageTextOK (hInstance, hMainWindow, text, textfont, FALSE);
		  Msg.ProcessMessages ();
		  Msg.CDestroyText ();

		  for (i = 0; i < lines; i ++)
		  {
				  if (text[i]) GlobalFree (text[i]);
		  }
		  return;

}


void KINFO::InfoKunF (HANDLE hInstance, HWND hMainWindow, short mdn, long kun, char *kun_bran2)
{

	      int dsqlstatus;
		  short pr_stu;
		  long pr_lst;
		  short zahl_art;
		  int i;
		  char zus_bez[25];
		  char zus_bez_kun[25];
		  char branbez [37];
          char kun_krz1 [17];
          char zahl_art_bez [17];


		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin  ((long *)  &kun, 2, 0);
		  sqlout ((short *) &pr_stu,  1, 0);
		  sqlout ((long *)  &pr_lst,  2, 0);
		  sqlout ((char *)  kun_krz1, 0, 17);
		  sqlout ((short *) &zahl_art,  1, 0);
		  dsqlstatus = sqlcomm ("select pr_stu, pr_lst, kun_krz1, zahl_art from kun "
                                "where mdn = ? and kun = ?");
		  if (dsqlstatus != 0) return;

		  strcpy (zus_bez, " ");
		  strcpy (zus_bez_kun, " ");
		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin ((short *) &pr_stu,  1, 0);
		  sqlout ((char *) zus_bez, 0, 25);
		  dsqlstatus = sqlcomm ("select zus_bz from iprgrstufk where mdn = ? "
			                                                    "and pr_gr_stuf = ?");

		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin ((short *) &pr_lst,  1, 0);
		  sqlout ((char *) zus_bez_kun, 0, 25);
		  dsqlstatus = sqlcomm ("select zus_bz from i_kun_prk where mdn = ? "
			                                                  "and kun_pr = ?");
          branbez[0] = 0;
		  sqlin  ((char *) kun_bran2, 0, 3);
		  sqlout ((char *) branbez, 0, 33);
		  dsqlstatus = sqlcomm ("select ptbez from ptabn where ptitem = \"kun_bran2\" "
			                                            "and ptwert = ?");
          zahl_art_bez[0] = 0;
		  sqlin  ((short *) &zahl_art, 1, 0);
		  sqlout ((char *) zahl_art_bez, 0, sizeof (zahl_art_bez));
		  dsqlstatus = sqlcomm ("select ptbez from ptabn where ptitem = \"zahl_art\" "
			                                            "and ptwert = ?");
		  for (i = 0; i < lines; i ++)
		  {
		             text[i] = (char *) GlobalAlloc (GMEM_FIXED, 80);
		  }

		  for (i = 0; i < lines; i ++)
		  {
			  if (text[i] == NULL) break;
		  }

		  if (i < lines)
		  {
			  for (i = 0; i < lines; i ++)
			  {
				  if (text[i]) GlobalFree (text[i]);
			  }
			  return;
		  }

		  sprintf (text[0], "  Preisgruppen Kunde %ld  %s ", kun, kun_krz1);
		  sprintf (text[1], " ");
		  sprintf (text[2], "   Preisgruppenstufe %hd   %s", pr_stu, zus_bez);
		  sprintf (text[3], "   Kundenpreisliste  %hd   %s", pr_lst, zus_bez_kun);
		  sprintf (text[4], "   Kundenbranche     %s  %s", kun_bran2, branbez);
		  sprintf (text[5], "   Zahlungsart       %hd   %s", zahl_art, zahl_art_bez);
		  text[6] = NULL;

		  Text1.SetFeld (text[0]);
		  Text2.SetFeld (text[1]);
		  Text3.SetFeld (text[2]);
		  Text4.SetFeld (text[3]);
		  Text5.SetFeld (text[4]);
		  Text6.SetFeld (text[5]);


		  Msg.CMessageTextF (hInstance, hMainWindow, &TextForm, FALSE);
//		  Msg.CMessageTextOK (hInstance, hMainWindow, text, textfont, FALSE);
		  Msg.ProcessMessages ();
		  Msg.CDestroyTextF ();

		  for (i = 0; i < lines; i ++)
		  {
				  if (text[i]) 
				  {
					  GlobalFree (text[i]);
					  text[i] = NULL;
				  }
		  }
		  return;

}


          
void KINFO::InfoKunFText (HANDLE hInstance, HWND hMainWindow, short mdn, long kun, char *kun_bran2)
{

	      int dsqlstatus;
		  short pr_stu;
		  long pr_lst;
		  short zahl_art;
		  int i;
		  char zus_bez[25];
		  char zus_bez_kun[25];
		  char branbez [37];
          char kun_krz1 [17];
          char zahl_art_bez [17];
          char frei_txt2 [65];


		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin  ((long *)  &kun, 2, 0);
		  sqlout ((short *) &pr_stu,  1, 0);
		  sqlout ((long *)  &pr_lst,  2, 0);
		  sqlout ((char *)  kun_krz1, 0, 17);
		  sqlout ((short *) &zahl_art,  1, 0);
		  sqlout ((char *)  frei_txt2,  0, sizeof (frei_txt2));
		  dsqlstatus = sqlcomm ("select pr_stu, pr_lst, kun_krz1, zahl_art, frei_txt2 from kun "
                                "where mdn = ? and kun = ?");
		  if (dsqlstatus != 0) return;

		  strcpy (zus_bez, " ");
		  strcpy (zus_bez_kun, " ");
		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin ((short *) &pr_stu,  1, 0);
		  sqlout ((char *) zus_bez, 0, 25);
		  dsqlstatus = sqlcomm ("select zus_bz from iprgrstufk where mdn = ? "
			                                                    "and pr_gr_stuf = ?");

		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin ((short *) &pr_lst,  1, 0);
		  sqlout ((char *) zus_bez_kun, 0, 25);
		  dsqlstatus = sqlcomm ("select zus_bz from i_kun_prk where mdn = ? "
			                                                  "and kun_pr = ?");
          branbez[0] = 0;
		  sqlin  ((char *) kun_bran2, 0, 3);
		  sqlout ((char *) branbez, 0, 33);
		  dsqlstatus = sqlcomm ("select ptbez from ptabn where ptitem = \"kun_bran2\" "
			                                            "and ptwert = ?");
          zahl_art_bez[0] = 0;
		  sqlin  ((short *) &zahl_art, 1, 0);
		  sqlout ((char *) zahl_art_bez, 0, sizeof (zahl_art_bez));
		  dsqlstatus = sqlcomm ("select ptbez from ptabn where ptitem = \"zahl_art\" "
			                                            "and ptwert = ?");
		  for (i = 0; i < tlines; i ++)
		  {
		             text[i] = (char *) GlobalAlloc (GMEM_FIXED, 80);
		  }

		  for (i = 0; i < tlines; i ++)
		  {
			  if (text[i] == NULL) break;
		  }

		  if (i < tlines)
		  {
			  for (i = 0; i < tlines; i ++)
			  {
				  if (text[i]) GlobalFree (text[i]);
			  }
			  return;
		  }

		  sprintf (text[0], "  Preisgruppen Kunde %ld  %s ", kun, kun_krz1);
		  sprintf (text[1], " ");
		  sprintf (text[2], "   Preisgruppenstufe %hd   %s", pr_stu, zus_bez);
		  sprintf (text[3], "   Kundenpreisliste  %hd   %s", pr_lst, zus_bez_kun);
		  sprintf (text[4], "   Kundenbranche     %s  %s", kun_bran2, branbez);
		  sprintf (text[5], "   Zahlungsart       %hd   %s", zahl_art, zahl_art_bez);
		  sprintf (text[6], "   %s", frei_txt2);
		  text[7] = NULL;

		  Text1.SetFeld (text[0]);
		  Text2.SetFeld (text[1]);
		  Text3.SetFeld (text[2]);
		  Text4.SetFeld (text[3]);
		  Text5.SetFeld (text[4]);
		  Text6.SetFeld (text[5]);
		  Text8.SetFeld (text[6]);


		  Msg.CMessageTextF (hInstance, hMainWindow, &TextFormLong, FALSE);
		  Msg.ProcessMessages ();
		  Msg.CDestroyTextF ();

		  for (i = 0; i < tlines; i ++)
		  {
				  if (text[i]) 
				  {
					  GlobalFree (text[i]);
					  text[i] = NULL;
				  }
		  }
		  return;

}

 
 

          

          
                    
          