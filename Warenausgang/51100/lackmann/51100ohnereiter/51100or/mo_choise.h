#ifndef _MO_CHOISE_DEF
#define _MO_CHOISE_DEF
#include "wmaskc.h"

struct DR
{
        char drucker [33];
};


class CHOISE
{
          private :
			  HWND hMainWindow;
			  HINSTANCE hInstance;
			  HWND hWnd;
			  HWND hWnd1;
			  HWND hWnd2;
              mfont *Font;
			  DWORD currentfield;
			  char **Texte;
			  int textstart;
			  int textanz;
              int x,y, cx, cy;
              struct DR ldrucker, *ldruckarr;
              int ldruckanz;
              int ldidx;
			  char *Caption;
			  char *ChoiseText;

          public :
			  CHOISE (int, int, int, int, char *, int, char *, char *);
			  ~CHOISE ();
 		      void ProcessMessages (void);
			  static void SetCurrentTast (DWORD);
              static void ShowDlg (HDC, form *);
              static void SetPrChoise (HWND); 
              static void SetChoise (HWND); 
              static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
              static CALLBACK CProc1(HWND,UINT, WPARAM,LPARAM);
              static CALLBACK CProc2(HWND,UINT, WPARAM,LPARAM);
              HWND OpenWindow (HANDLE, HWND);
              void SetChoiseDefault (char *); 
              void DestroyWindow (void);
              void MoveWindow (void);
			  void SetTexte (char **, char **);
			  void Scroll (BOOL);
			  void SetCurrentID (DWORD);
			  void SetCurrentName (char *);
			  void EnableID (DWORD, BOOL);
			  void EnableName (char *, BOOL);
			  void CheckID (DWORD, BOOL);
			  void CheckName (char *, BOOL);
              BOOL TestButtons (HWND);
              static DWORD IsTast (DWORD);
              void BelegeGdiDrucker (void);
              void BelegeDrucker (void);
			  void FillPrinterBox (void);
			  void FillPrinterNam (void);
              void FillListUb (char *);
              void FillListRow (char *);
              void SetCursel (int);
              void VLines (void);
};
#endif

