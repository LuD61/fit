// CalcualteSmt.cpp: Implementierung der Klasse CCalcualteSmt.
//
//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "CalcualteSmt.h"
#include "strfkt.h"
#include "Text.h"
#include "ListHandler.h"
#include  "hwg.h"

#define CString Text
#define LISTHANDLER CListHandler::GetInstance ()

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

// List-Klasse f�r Sortimentwerte pro Position sortiert

HWG_CL Hwg ; 
CSmtOrderItem::CSmtOrderItem ()
{
	m_A     = 0.0;
	m_Smt   = 0;
	m_Me    = 0.0;
	m_Gew   = 0.0;
	m_Vk    = 0.0;
	m_A_gew = 0.0;
}

CSmtOrderList::CSmtOrderList ()
{
}
	
CSmtOrderList::~CSmtOrderList ()
{
	DestroyElements ();
	Destroy ();
}

void CSmtOrderList::DestroyElements ()
{
	CSmtOrderItem *element;

	if (Arr != NULL)
	{
		for (int i = 0; i < anz ; i ++)
		{
			element = *Get (i);
			delete element;
			Arr[i] = NULL;
		}
	}
	anz = 0;
	pos = 0;
}


double CSmtOrderList::GetOrderAmount (short smt)
{
	double value = 0;
	int i = 0;
	CSmtOrderItem * Item;

	for (i = 0; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () == smt)
		{
			break;
		}
	}

	for (; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () != smt)
		{
			break;
		}
		value += Item->Vk () * Item->Me ();
	}

	return value;
}

double CSmtOrderList::GetOrderWeight (short smt)
{
	double value = 0.0;
	int i = 0;
	CSmtOrderItem * Item;

	for (i = 0; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () == smt)
		{
			break;
		}
	}

	for (; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () != smt)
		{
			break;
		}
		value += Item->Gew ();
	}

	return value;
}

double CSmtOrderList::GetLiq (short smt)
{
	double value = 0;
    double amount = GetOrderAmount (smt); 
    double weight = GetOrderWeight (smt);   
	if (weight != 0.0)
	{
		value = amount / weight;
	}
	return value;
}

int CSmtOrderList::GetStndAllElements (short smt)
{
	int i;
	CDataCollection<AUFPS *> *StndAll = LISTHANDLER->GetStndAll ();
	AUFPS *aufps;
	int SmtAllItems = 0;

	for (i = 0; i < StndAll->anz; i ++)
	{
		aufps = *StndAll->Get (i);
		if (atoi (aufps->teil_smt) == (int) smt)
		{
			break;
		}
	}

	for (; i < StndAll->anz; i ++)
	{
		aufps = *StndAll->Get (i);
		if (atoi (aufps->teil_smt) == (int) smt)
		{
			SmtAllItems ++;
		}
	}
	return SmtAllItems;
}


double CSmtOrderList::GetSmtQuot (short smt)
{
	double value = 0;
	int SmtAllItems = 0;
	int SmtValueItems = 0;

	int i = 0;
	CSmtOrderItem * Item;

	/** 260312
	for (i = 0; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () == smt)
		{
			break;
		}
	}
	**/

	for (; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () != smt)
		{
	//		break;
			continue ; //260312
		}
		SmtAllItems ++;
		if (Item->Me () != 0.0)
		{
			SmtValueItems ++;
		}
	}

	if (SmtAllItems != 0)
	{
		value = (double) SmtValueItems * 100 / SmtAllItems;
	}

	int allElements = GetStndAllElements (smt);
	if (allElements  != 0)
	{
		value = (double) SmtValueItems * 100 / allElements;
	}

	return value;
}


double CSmtOrderList::GetSmtQuot ()
{
	double value = 0;
	int SmtAllItems = 0;
	int SmtValueItems = 0;

	int i = 0;
	CSmtOrderItem * Item;


	for (; i < anz; i ++)
	{
		Item = *Get (i);
		SmtAllItems ++;
		if (Item->Me () != 0.0)
		{
			SmtValueItems ++;
		}
	}

	if (SmtAllItems != 0.0)
	{
		value = (double) SmtValueItems * 100 / SmtAllItems;
	}

	if (LISTHANDLER->GetStndAll ()->anz > 0)
	{
		value = (double) SmtValueItems * 100 / LISTHANDLER->GetStndAll ()->anz;
	}

	return value;
}


// Vergleichs-Klasse f�r Sortimentwerte.  

int CCompareOrderInterface::Compare (CSmtOrderItem *Item1, CSmtOrderItem *Item2)
{
	return Item1->Smt () - Item2->Smt ();
}

// List-Klasse f�r Sortimente eindeutig, sortiert

CSmtList::CSmtList ()
{
	set_Unique (TRUE);
}
	
CSmtList::~CSmtList ()
{
	Destroy ();
}

// Vergleichs-Klasse f�r Sortimente.  

int CCompareSmtInterface::Compare (short Item1, short Item2)
{
	return Item1 - Item2;
}

CCalcualteSmt::CCalcualteSmt()
{
	m_SmtOrderList.set_CompareInterface (&Compare);
	m_SmtList.set_CompareInterface (&SmtCompare);
	m_Controls = NULL;
	m_hWnd = NULL;
}

CCalcualteSmt::~CCalcualteSmt()
{
	m_SmtOrderList.DestroyElements ();
	m_SmtOrderList.Destroy ();
}

void CCalcualteSmt::Calculate (AUFPS *aufps, int size)
{
	int i = 0;
	double sumAmount = 0.0;
	double sumWeight = 0.0;
	double sumLiq = 0.0;
	double sumSmtPart = 0.0;

	m_SmtOrderList.DestroyElements ();
	m_SmtList.Destroy ();
	for (i = 0; i < size; i ++)
	{
	  if (ratod (aufps[i].lief_me) != 0.0)
	  { 
		if (!IsDiscountArticle (ratod (aufps[i].a), aufps[i].smt))
		{
			CSmtOrderItem * Item = new CSmtOrderItem ();
			Item->set_A (ratod (aufps[i].a));
//			Item->set_Smt ((short) atoi (aufps[i].teil_smt));  //todo hier statt teil_smt hwg //140312 
			Item->set_Smt (aufps[i].hwg);  //140312 
			Item->set_Me (ratod (aufps[i].lief_me));
			Item->set_Vk (ratod (aufps[i].auf_vk_pr));
			Item->set_A_gew (aufps[i].a_gew);
			if (atoi (aufps[i].me_einh) != 2) 
			{
				Item->set_Gew (Item->Me () * Item->A_gew ());  
			}
			else
			{
				Item->set_Gew (Item->Me ());
			}
			m_SmtOrderList.Add (Item);
			m_SmtList.Add (Item->Smt ());
		}
	  }
	}

	if (m_Controls != NULL)
	{
		CSmtTable * smtTable = m_Controls->SmtTable ();
		smtTable->Clear ();
		for (int i = 0; i < m_SmtList.anz; i ++)
		{
			short smt      = *m_SmtList.Get (i);
			double amount  = m_SmtOrderList.GetOrderAmount (smt); 
			double weight  = m_SmtOrderList.GetOrderWeight (smt); 
			double liq     = m_SmtOrderList.GetLiq (smt); 
			double smtquot = m_SmtOrderList.GetSmtQuot (smt);
			if (amount != 0.0 || weight != 0.0)
			{
				sumAmount += amount;
				sumWeight += weight;
				sumLiq += liq;
				sumSmtPart += smtquot;
				CString cSmt;
				cSmt.Format ("%d", smt);
				CSmtValues *smtValues = new CSmtValues ();
				memcpy (&ptabn, &ptabn_null, sizeof (PTABN));
//140312				Ptabn.lese_ptab ("teil_smt", cSmt.GetBuffer ());              //todo
//140312				smtValues->set_Name (CString::TrimRight (ptabn.ptbez));
				hwg.hwg = atoi(cSmt.GetBuffer()); //140312
				if (Hwg.dbreadfirst() == 0) //140312
				{
					smtValues->set_Name (CString::TrimRight (hwg.hwg_bz1));
				}
				else
				{
					smtValues->set_Name ("");
				}
				smtValues->set_Amount (amount);
				smtValues->set_Weight (weight);
				smtValues->set_Liq (liq);
				smtValues->set_SmtPart (smtquot);
				smtTable->Add (smtValues);
				delete smtValues;
			}
		}
		sumSmtPart = m_SmtOrderList.GetSmtQuot ();
		if (sumWeight != 0.0)
        { 
			sumLiq = sumAmount / sumWeight;
		}
		CSmtValues *smtValues = new CSmtValues ();
		smtValues->set_Name ("Gesamt");
		smtValues->set_Amount (sumAmount);
		smtValues->set_Weight (sumWeight);
		smtValues->set_Liq (sumLiq);
		smtValues->set_SmtPart (sumSmtPart);
		smtTable->Add (smtValues);
	}
}

BOOL CCalcualteSmt::IsDiscountArticle (double a, LPSTR smt)
{
    CString Smt = smt;
	Smt.Trim ();
	if (Smt.GetLength () == 0)
	{
		lese_a_bas (a);
		strcpy (smt, _a_bas.smt);
		Smt = _a_bas.smt;
	}
	return (Smt == "R");
}

