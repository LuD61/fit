// Dialog.h: Schnittstelle f�r die Klasse CDialog.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIALOG_H__252AA6E5_0F09_43BA_8BAD_CDE464515199__INCLUDED_)
#define AFX_DIALOG_H__252AA6E5_0F09_43BA_8BAD_CDE464515199__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>
#include <vector>
#include "DialogItem.h"
#include "font.h"

using namespace std;

class CDialogInstance
{
public:
	HWND m_hWnd;
	void *m_Instance;
	CDialogInstance (HWND hWnd, void *Instance)
	{
		m_hWnd = hWnd;
		m_Instance = Instance;
	}
};

class CDialog  
{
public:
	enum DIALOG_TYPE
	{
		BoxMode = 0,
        CreateMode = 1,
	};
	DIALOG_TYPE DialogType;
	DWORD Id;
	HWND m_hWnd;
	HWND Parent;
	BOOL ret;
	BOOL m_SetPos;
	int m_X, m_Y;
	WPARAM wParam;
	static CDialog *Instance;
	static vector<CDialogInstance *> m_Instances;
	CDialog(DWORD Id, HWND parent = NULL);
	virtual ~CDialog();
protected:
	BOOL (*m_CloseHandler) (CDialog *dlg);
	BOOL m_Modal;
    vector<CDialogItem *> m_Controls;
	void AddInstance ();
	void DropInstance ();
	virtual void AttachControls ();
	virtual BOOL OnInitDialog ();
	virtual BOOL PreTranslateMessage (MSG *msg);
	virtual void Display ();
	virtual void Get ();
	virtual HBRUSH OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor);
	virtual void OnOK ();
	virtual void OnCancel ();
	void GetFont (DWORD controlId, CFont& font);
	void SetFont (DWORD controlId, CFont& font);
	HWND GethWnd (DWORD controlId);
	void EndDialog(HWND m_hWnd, WPARAM wParam); 
	static CDialog *GetInstance (HWND hWnd, CDialog *DefInst);
	static DWORD CALLBACK DlgProc (HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam); 

public:
	void set_CloseHandler (BOOL (*closeHandler) (CDialog *dlg) )
	{
		m_CloseHandler = closeHandler;
	}

	void SetDlgPos (HWND hWnd);
	BOOL DoModal ();
	BOOL Create ();
	void Hide ();
	void Show ();

};

#endif // !defined(AFX_DIALOG_H__252AA6E5_0F09_43BA_8BAD_CDE464515199__INCLUDED_)
