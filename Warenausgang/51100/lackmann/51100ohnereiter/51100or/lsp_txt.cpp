#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lsp_txt.h"

struct LSP_TXT lsp_txt, lsp_txt_null;

void LSP_TCLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lsp_txt.mdn, 1, 0);
            ins_quest ((char *) &lsp_txt.fil, 1, 0);
            ins_quest ((char *) &lsp_txt.ls, 2, 0);
            ins_quest ((char *) &lsp_txt.posi, 2, 0);
    out_quest ((char *) &lsp_txt.mdn,1,0);
    out_quest ((char *) &lsp_txt.fil,1,0);
    out_quest ((char *) &lsp_txt.ls,2,0);
    out_quest ((char *) &lsp_txt.posi,2,0);
    out_quest ((char *) &lsp_txt.zei,2,0);
    out_quest ((char *) lsp_txt.txt,0,61);
            cursor = prepare_sql ("select lsp_txt.mdn,  "
"lsp_txt.fil,  lsp_txt.ls,  lsp_txt.posi,  lsp_txt.zei,  lsp_txt.txt from lsp_txt "

#line 29 "lsp_txt.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ls = ? "
                                  "and   posi = ? "
                                  "order by zei");

    ins_quest ((char *) &lsp_txt.mdn,1,0);
    ins_quest ((char *) &lsp_txt.fil,1,0);
    ins_quest ((char *) &lsp_txt.ls,2,0);
    ins_quest ((char *) &lsp_txt.posi,2,0);
    ins_quest ((char *) &lsp_txt.zei,2,0);
    ins_quest ((char *) lsp_txt.txt,0,61);
            sqltext = "update lsp_txt set lsp_txt.mdn = ?,  "
"lsp_txt.fil = ?,  lsp_txt.ls = ?,  lsp_txt.posi = ?,  lsp_txt.zei = ?,  "
"lsp_txt.txt = ? "

#line 36 "lsp_txt.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ls = ? "
                                  "and   posi = ? "
                                  "and   zei  = ?";

            ins_quest ((char *) &lsp_txt.mdn, 1, 0);
            ins_quest ((char *) &lsp_txt.fil, 1, 0);
            ins_quest ((char *) &lsp_txt.ls, 2, 0);
            ins_quest ((char *) &lsp_txt.posi, 2, 0);
            ins_quest ((char *) &lsp_txt.zei, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lsp_txt.mdn, 1, 0);
            ins_quest ((char *) &lsp_txt.fil, 1, 0);
            ins_quest ((char *) &lsp_txt.ls, 2, 0);
            ins_quest ((char *) &lsp_txt.posi, 2, 0);
            ins_quest ((char *) &lsp_txt.zei, 2, 0);
            test_upd_cursor = prepare_sql ("select ls from lsp_txt "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ls = ? "
                                  "and   posi = ? "
                                  "and   zei  = ?");
            ins_quest ((char *) &lsp_txt.mdn, 1, 0);
            ins_quest ((char *) &lsp_txt.fil, 1, 0);
            ins_quest ((char *) &lsp_txt.ls, 2, 0);
            ins_quest ((char *) &lsp_txt.posi, 2, 0);
            ins_quest ((char *) &lsp_txt.zei, 2, 0);
            del_cursor = prepare_sql ("delete from lsp_txt "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ls = ? "
                                  "and   posi = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &lsp_txt.mdn, 1, 0);
            ins_quest ((char *) &lsp_txt.fil, 1, 0);
            ins_quest ((char *) &lsp_txt.ls, 2, 0);
            ins_quest ((char *) &lsp_txt.posi, 2, 0);
            del_cursor_posi = prepare_sql ("delete from lsp_txt "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ls = ? "
                                  "and   posi = ?");

            ins_quest ((char *) &lsp_txt.mdn, 1, 0);
            ins_quest ((char *) &lsp_txt.fil, 1, 0);
            ins_quest ((char *) &lsp_txt.ls, 2, 0);
            del_cursor_ls = prepare_sql ("delete from lsp_txt "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ls = ? ");

    ins_quest ((char *) &lsp_txt.mdn,1,0);
    ins_quest ((char *) &lsp_txt.fil,1,0);
    ins_quest ((char *) &lsp_txt.ls,2,0);
    ins_quest ((char *) &lsp_txt.posi,2,0);
    ins_quest ((char *) &lsp_txt.zei,2,0);
    ins_quest ((char *) lsp_txt.txt,0,61);
            ins_cursor = prepare_sql ("insert into lsp_txt ("
"mdn,  fil,  ls,  posi,  zei,  txt) "

#line 91 "lsp_txt.rpp"
                                      "values "
                                      "(?,?,?,?,?,?)"); 

#line 93 "lsp_txt.rpp"
}
int LSP_TCLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int LSP_TCLASS::delete_lspposi (void)
/**
Alle text fuer eine Position in lsp_txt loeschen
**/
{
         if (del_cursor_posi == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_posi);
         return (sqlstatus);
}

int LSP_TCLASS::delete_lspls (void)
/**
Alle text fuer eine Lieferschein in lsp_txt loeschen
**/
{
         if (del_cursor_ls == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_ls);
         return (sqlstatus);
}

