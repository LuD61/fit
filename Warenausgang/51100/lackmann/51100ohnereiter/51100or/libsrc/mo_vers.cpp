#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <windows.h>
#include "strfkt.h"
#include "mo_vers.h"
#include "cmask.h"

DWORD VINFO::BuOk = 900;

mfont VINFO::TextFont1 = {
                  "MS SAN SERIF", 
//                  "Impact", 
                   150, 
                   0, 
                   1, 
                   BLUECOL, 
                   LTGRAYCOL,
                   0, 0};


mfont VINFO::TextFont2 = {
                  "MS SAN SERIF", 
//                  "Impact", 
                   120, 
                   0, 
                   1, 
                   BLUECOL, 
                   LTGRAYCOL,
                   0, 0};

mfont VINFO::TextFont = {
                  "MS SAN SERIF", 
                   120, 
                   0, 
                   1, 
                   REDCOL, 
                   LTGRAYCOL,
                   0, 0};

mfont VINFO::BuFont = {
                  "MS SAN SERIF", 
                   120, 
                   0, 
                   1, 
                   BLACKCOL, 
                   LTGRAYCOL,
                   0, 0};


char *VINFO::text[10]; 

mfont *VINFO::textfont[] = {&TextFont1, 
                            &TextFont2,
				    &TextFont,
				    &TextFont,
				    &TextFont,
				    &BuFont,
				    &TextFont,
				    &TextFont,
				    &TextFont,
				    &TextFont,
				    &TextFont,
				    NULL,
};


CFIELD VINFO::Text1  ("Text1",text[0], 0, 0, 1, 0,  NULL, "", DISPLAYONLY,
			                NULL, textfont[0], FALSE, TRANSPARENT); 		  
CFIELD VINFO::Text2 ("Text2", text[1], 0, 0, 1, 2,  NULL, "", DISPLAYONLY,
			                NULL, textfont[1], FALSE, TRANSPARENT); 		  
CFIELD VINFO::Text3 ("Text3", text[2], 0, 0, 1, 3,  NULL, "", DISPLAYONLY,
			                NULL, textfont[2], FALSE, TRANSPARENT); 		  
CFIELD VINFO::Text4 ("Text4", text[3], 0, 0, 1, 4,  NULL, "", DISPLAYONLY,
			                NULL, textfont[3], FALSE, TRANSPARENT);
 		  
CFIELD VINFO::Text5 ("Text5", text[3], 0, 0, 1, 5,  NULL, "", DISPLAYONLY,
			                NULL, textfont[4], FALSE, TRANSPARENT);

CFIELD VINFO::Text6 ("OK",    "  OK ", 10, 0, -1, 7,  NULL, "", CBUTTON,
			                BuOk, textfont[5], FALSE, TRANSPARENT); 

CFIELD *VINFO::TextField[] = {&Text1, &Text2, &Text3, &Text4, &Text5, &Text6};

CFORM VINFO::TextForm (6, TextField);  		  

void VINFO::VInfo (HANDLE hInstance, HWND hMainWindow, char **txt)
{
          int i;

		  for (i = 0; i < lines; i ++)
		  {
		             text[i] = (char *) GlobalAlloc (GMEM_FIXED, 80);
		  }

		  for (i = 0; i < lines; i ++)
		  {
			  if (text[i] == NULL) break;
		  }

		  if (i < lines)
		  {
			  for (i = 0; i < lines; i ++)
			  {
				  if (text[i]) GlobalFree (text[i]);
			  }
			  return;
		  }

		  sprintf (text[0], txt[0]);
		  sprintf (text[1], " ");
		  sprintf (text[2], txt[1]);
		  sprintf (text[3], " ");
		  sprintf (text[4], txt[2]);
		  text[5] = NULL;


		  Msg.CMessageTextOK (hInstance, hMainWindow, text, textfont, FALSE);
		  Msg.ProcessMessages ();
		  Msg.CDestroyText ();

		  for (i = 0; i < lines; i ++)
		  {
				  if (text[i]) GlobalFree (text[i]);
		  }
		  return;

}


void VINFO::VInfoF (HANDLE hInstance, HWND hMainWindow, char **txt)
{
          int i;

		  for (i = 0; i < lines; i ++)
		  {
		             text[i] = (char *) GlobalAlloc (GMEM_FIXED, 80);
		  }

		  for (i = 0; i < lines; i ++)
		  {
			  if (text[i] == NULL) break;
		  }

		  if (i < lines)
		  {
			  for (i = 0; i < lines; i ++)
			  {
				  if (text[i]) GlobalFree (text[i]);
			  }
			  return;
		  }

		  sprintf (text[0], txt [0]);
		  sprintf (text[1], txt[1]);
		  sprintf (text[2], "");
		  sprintf (text[3], txt [2]);
		  sprintf (text[4], txt [3]);
		  text[5] = NULL;

		  Text1.SetFeld (text[0]);
		  Text2.SetFeld (text[1]);
		  Text3.SetFeld (text[2]);
		  Text4.SetFeld (text[3]);
		  Text5.SetFeld (text[4]);


		  Msg.CMessageTextF (hInstance, hMainWindow, &TextForm, FALSE);
		  Msg.ProcessMessages ();
		  Msg.CDestroyTextF ();

		  for (i = 0; i < lines; i ++)
		  {
				  if (text[i]) GlobalFree (text[i]);
		  }
		  return;

}


           
 

          

          
                    
          