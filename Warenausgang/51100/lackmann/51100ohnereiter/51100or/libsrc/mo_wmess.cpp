#include "strfkt.h"
#include "cmask.h"
#include "mo_wmess.h"

MESSCLASS Msg;

void WMESS::Message (HANDLE hInstance, HWND hMainWindow, char *text)
{
	      int anz; 
		  int i;

		  Free ();
		  anz = wsplit (text, "\n");
		  warten      = (char **) GlobalAlloc (GMEM_FIXED, (anz + 1) * sizeof (char *));
		  if (warten == NULL)
		  {
			  return;
		  }

		  wartenfont  = (mfont **) GlobalAlloc (GMEM_FIXED, (anz + 1) * sizeof (mfont *));
		  if (wartenfont == NULL)
		  {
			  GlobalFree (warten);
			  return;
		  }

          for (i = 0; i < anz; i ++)
		  {
			  warten[i] = (char *) GlobalAlloc (GMEM_FIXED, 512);
			  if (warten[i] == NULL) break;
			  strcpy (warten[i], wort[i]);
			  wartenfont[i] = &stdfont;
		  }
		  warten[i] = NULL;

		  Msg.CMessageText (hInstance, hMainWindow, warten, wartenfont, wartenfont[0]);
}

void WMESS::Free (void)
{
		  int i;

		  if (warten)
		  {
			  for (i = 0; warten[i]; i ++)
			  {
				  GlobalFree (warten[i]);
			  }
			  GlobalFree (warten);
			  GlobalFree (wartenfont);
			  warten = NULL;
			  wartenfont = NULL;
		  }
}

void WMESS::Destroy (void)
{
	      Free ();
		  Msg.CDestroyText ();
}

