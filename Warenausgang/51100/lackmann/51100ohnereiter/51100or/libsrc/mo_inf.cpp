#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <windows.h>
#include "strfkt.h"
#include "mo_inf.h"
#include "dbclass.h"
#include "cmask.h"

static DWORD BuOk = 900;

mfont TextFont1 = {
//                  "Comic Sans MS", 
//                  "Times New Roman", 
                  "Impact", 
                   150, 
                   0, 
                   0, 
                   BLUECOL, 
                   LTGRAYCOL,
                   0, 0};


mfont TextFont = {
//                  "Times New Roman", 
                  "Courier New", 
                   120, 
                   0, 
                   1, 
                   BLACKCOL, 
                   LTGRAYCOL,
                   0, 0};

mfont BuFont = {
                  "Times New Roman", 
                   120, 
                   0, 
                   1, 
                   BLACKCOL, 
                   LTGRAYCOL,
                   0, 0};


static char *text[10]; 

static mfont *textfont[] = {&TextFont1, 
                            &TextFont,
							&TextFont,
							&TextFont,
							&TextFont,
							&BuFont,
							&TextFont,
							&TextFont,
							&TextFont,
							&TextFont,
							&TextFont,
							NULL,
};


CFIELD Text1  ("Text1",text[0], 0, 0, 1, 0,  NULL, "", DISPLAYONLY,
			                NULL, textfont[0], FALSE, TRANSPARENT); 		  
CFIELD Text2 ("Text2", text[1], 0, 0, 1, 2,  NULL, "", DISPLAYONLY,
			                NULL, textfont[1], FALSE, TRANSPARENT); 		  
CFIELD Text3 ("Text3", text[2], 0, 0, 1, 3,  NULL, "", DISPLAYONLY,
			                NULL, textfont[2], FALSE, TRANSPARENT); 		  
CFIELD Text4 ("Text4", text[3], 0, 0, 1, 4,  NULL, "", DISPLAYONLY,
			                NULL, textfont[3], FALSE, TRANSPARENT);
 		  
CFIELD Text5 ("Text5", text[3], 0, 0, 1, 5,  NULL, "", DISPLAYONLY,
			                NULL, textfont[4], FALSE, TRANSPARENT);

CFIELD Text6 ("OK",    "  OK ", 10, 0, -1, 7,  NULL, "", CBUTTON,
			                BuOk, textfont[5], FALSE, TRANSPARENT); 

CFIELD *TextField[] = {&Text1, &Text2, &Text3, &Text4, &Text5, &Text6};

CFORM TextForm (6, TextField);  		  




void KINFO::InfoKun (HANDLE hInstance, HWND hMainWindow, short mdn, long kun)
{

	      int dsqlstatus;
		  short pr_stu;
		  long pr_lst;
		  int i;
		  char zus_bez[25];
		  char zus_bez_kun[25];


		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin  ((long *)  &kun, 2, 0);
		  sqlout ((short *) &pr_stu,  1, 0);
		  sqlout ((long *)  &pr_lst, 2, 0);
		  dsqlstatus = sqlcomm ("select pr_stu, pr_lst from kun where mdn = ? and kun = ?");
		  if (dsqlstatus != 0) return;

		  strcpy (zus_bez, " ");
		  strcpy (zus_bez_kun, " ");
		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin ((short *) &pr_stu,  1, 0);
		  sqlout ((char *) zus_bez, 0, 25);
		  dsqlstatus = sqlcomm ("select zus_bz from iprgrstufk where mdn = ? "
			                                                    "and pr_gr_stuf = ?");

		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin ((short *) &pr_lst,  1, 0);
		  sqlout ((char *) zus_bez_kun, 0, 25);
		  dsqlstatus = sqlcomm ("select zus_bz from i_kun_prk where mdn = ? "
			                                                  "and kun_pr = ?");
		  for (i = 0; i < lines; i ++)
		  {
		             text[i] = (char *) GlobalAlloc (GMEM_FIXED, 80);
		  }

		  for (i = 0; i < lines; i ++)
		  {
			  if (text[i] == NULL) break;
		  }

		  if (i < lines)
		  {
			  for (i = 0; i < lines; i ++)
			  {
				  if (text[i]) GlobalFree (text[i]);
			  }
			  return;
		  }

		  sprintf (text[0], "  Preisgruppen Kunde %ld  ", kun);
		  sprintf (text[1], " ");
		  sprintf (text[2], "   Preisgruppenstufe %hd %s", pr_stu, zus_bez);
		  sprintf (text[3], " ");
		  sprintf (text[4], "   Kundenpreisliste   %hd %s", pr_lst, zus_bez_kun);
		  text[5] = NULL;


		  Msg.CMessageTextOK (hInstance, hMainWindow, text, textfont, FALSE);
		  Msg.ProcessMessages ();
		  Msg.CDestroyText ();

		  for (i = 0; i < lines; i ++)
		  {
				  if (text[i]) GlobalFree (text[i]);
		  }
		  return;

}


void KINFO::InfoKunF (HANDLE hInstance, HWND hMainWindow, short mdn, long kun, char *kun_bran2)
{

	      int dsqlstatus;
		  short pr_stu;
		  long pr_lst;
		  int i;
		  char zus_bez[25];
		  char zus_bez_kun[25];
		  char branbez [37];


		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin  ((long *)  &kun, 2, 0);
		  sqlout ((short *) &pr_stu,  1, 0);
		  sqlout ((long *)  &pr_lst, 2, 0);
		  dsqlstatus = sqlcomm ("select pr_stu, pr_lst from kun where mdn = ? and kun = ?");
		  if (dsqlstatus != 0) return;

		  strcpy (zus_bez, " ");
		  strcpy (zus_bez_kun, " ");
		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin ((short *) &pr_stu,  1, 0);
		  sqlout ((char *) zus_bez, 0, 25);
		  dsqlstatus = sqlcomm ("select zus_bz from iprgrstufk where mdn = ? "
			                                                    "and pr_gr_stuf = ?");

		  sqlin  ((short *) &mdn, 1, 0);
		  sqlin ((short *) &pr_lst,  1, 0);
		  sqlout ((char *) zus_bez_kun, 0, 25);
		  dsqlstatus = sqlcomm ("select zus_bz from i_kun_prk where mdn = ? "
			                                                  "and kun_pr = ?");
          branbez[0] = 0;
		  sqlin  ((char *) kun_bran2, 0, 3);
		  sqlout ((char *) branbez, 0, 33);
		  dsqlstatus = sqlcomm ("select ptbez from ptabn where ptitem = \"kun_bran2\" "
			                                            "and ptwert = ?");
		  for (i = 0; i < lines; i ++)
		  {
		             text[i] = (char *) GlobalAlloc (GMEM_FIXED, 80);
		  }

		  for (i = 0; i < lines; i ++)
		  {
			  if (text[i] == NULL) break;
		  }

		  if (i < lines)
		  {
			  for (i = 0; i < lines; i ++)
			  {
				  if (text[i]) GlobalFree (text[i]);
			  }
			  return;
		  }

		  sprintf (text[0], "  Preisgruppen Kunde %ld  ", kun);
		  sprintf (text[1], " ");
		  sprintf (text[2], "   Preisgruppenstufe %hd   %s", pr_stu, zus_bez);
		  sprintf (text[3], "   Kundenpreisliste  %hd   %s", pr_lst, zus_bez_kun);
		  sprintf (text[4], "   Kundenbranche     %s  %s", kun_bran2, branbez);
		  text[5] = NULL;

		  Text1.SetFeld (text[0]);
		  Text2.SetFeld (text[1]);
		  Text3.SetFeld (text[2]);
		  Text4.SetFeld (text[3]);
		  Text5.SetFeld (text[4]);


		  Msg.CMessageTextF (hInstance, hMainWindow, &TextForm, FALSE);
//		  Msg.CMessageTextOK (hInstance, hMainWindow, text, textfont, FALSE);
		  Msg.ProcessMessages ();
		  Msg.CDestroyTextF ();

		  for (i = 0; i < lines; i ++)
		  {
				  if (text[i]) GlobalFree (text[i]);
		  }
		  return;

}


           
 

          

          
                    
          