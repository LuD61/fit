// DataCollection.h: Schnittstelle f�r die Klasse CDataCollection.
//
//////////////////////////////////////////////////////////////////////
#ifndef DATA_COLLECTION_DEF
#define DATA_COLLECTION_DEF

#include <windows.h>

template <class T> 

class CCollection 
{
public:
	int bSize;
	T *Arr;
	int anz;
	int pos;

CCollection()
{
	Arr = NULL;
	anz = 0;
	pos = 0;
	bSize = 0x100;
}

~CCollection()
{
}

void Destroy()
{
	if (Arr != NULL)
	{
		delete Arr;
		Arr = NULL;
	}
	anz = 0;
	pos = 0;
}


void Clear()
{
	anz = 0;
	pos = 0;
}

void Start ()
{
	pos = 0;
}

void Expand ()
{
	bSize *= 2;
	T *NewArr = new T [bSize];
	memset (NewArr, 0, bSize * sizeof (T));
	for (int i = 0; i < anz; i ++)
	{
		memcpy (&NewArr[i], &Arr[i], sizeof (T));
	}
	delete Arr;
	Arr = NewArr;
}


void Insert (T Element, int idx)
{
	if (Arr == NULL)
	{
		Arr = new T [bSize];
		memset (Arr, 0, bSize * sizeof (T));
	}
	if (anz == bSize) Expand ();;
	if (idx > anz)
	{
		idx = anz;
	}
	if (idx == anz)
	{
		Add (Element);
		return;
	}
	for (int i = anz; i > idx; i --)
	{
	    memcpy (&Arr[i], &Arr[i - 1], sizeof (T));
	}
    memcpy (&Arr[idx], &Element, sizeof (T));
    anz ++;
}

void Insert (T *Element, int idx)
{
	if (Arr == NULL)
	{
		Arr = new T [bSize];
		memset (Arr, 0, bSize * sizeof (T));
	}
	if (anz == bSize) Expand ();;
	if (idx > anz)
	{
		idx = anz;
	}
	if (idx == anz)
	{
		Add (Element);
		return;
	}
	for (int i = anz; i > idx; i --)
	{
	    memcpy (&Arr[i], &Arr[i - 1], sizeof (T));
	}
    memcpy (&Arr[idx], Element, sizeof (T));
    anz ++;
}

void Add (T Element)
{
	if (Arr == NULL)
	{
		Arr = new T [bSize];
		memset (Arr, 0, bSize * sizeof (T));
	}
	if (anz == bSize) Expand ();
    memcpy (&Arr[anz], &Element, sizeof (T));
    anz ++;
}

void Add (T *Element)
{
	if (Arr == NULL)
	{
		Arr = new T [bSize];
		memset (Arr, 0, bSize * sizeof (T));
	}
	if (anz == bSize) Expand ();
    memcpy (&Arr[anz], Element, sizeof (T));
    anz ++;
}


BOOL Drop (int idx)
{
	int i;

    if (idx >= anz)
    {
        return FALSE;
    }

    anz --;
    for (i = idx; i < anz; i ++)
    {
        memcpy (&Arr[i], &Arr[i + 1], sizeof (T));
    }
    return TRUE;
}

T* GetNext (void)
{
    if (pos >= anz)
    {
        return NULL;
    }
    pos ++;
    return &Arr [pos - 1];
}

T* Get (int idx)
{
    if (idx >= anz)
    {
        return NULL;
    }
    return &Arr [idx];
}
};

template <class T> 

class CDataCollection : public CCollection<T> 
{
public:

~CDataCollection()
{
	if (Arr != NULL)
	{
		delete Arr;
		Arr = NULL;
	}
}

BOOL Drop (int idx)
{
	int i;

    if (idx >= anz)
    {
        return FALSE;
    }

    anz --;
    for (i = idx; i < anz; i ++)
    {
        memcpy (&Arr[i], &Arr[i + 1], sizeof (T));
    }
    return TRUE;
}

virtual BOOL Drop (T Element)
{

	for (int i = 0; i < anz; i ++)
	{
		T *t = Get (i);
		if (*t == Element)
		{
			return CCollection<T>::Drop (i);
		}
	}
	return FALSE;
}

void DestroyElements ()
{
	T element;
	if (Arr != NULL)
	{
		for (int i = 0; i < anz ; i ++)
		{
			element = *Get (i);
			delete element;
			Arr[i] = NULL;
		}
	}
	anz = 0;
	pos = 0;
}
};

template <class T> 

class CDataCollectionEx : public CCollection<T> 
{
};

#endif
