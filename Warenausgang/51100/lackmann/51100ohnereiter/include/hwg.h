#ifndef _HWG_DEF
#define _HWG_DEF

#include "dbclass.h"

struct HWG {
   short     hwg;
   char      hwg_bz1[25];
   char      hwg_bz2[25];
   char      sp_kz[2];
   long      we_kto;
   long      erl_kto;
   char      durch_vk[2];
   char      durch_sk[2];
   char      durch_fil[2];
   char      durch_sw[2];
   char      best_auto[2];
   double    sp_vk;
   double    sp_sk;
   double    sp_fil;
   double    sw;
   short     me_einh;
   short     mwst;
   short     teil_smt;
   short     bearb_lad;
   short     bearb_fil;
   short     bearb_sk;
   char      sam_ean[2];
   char      smt[2];
   char      kost_kz[3];
   double    pers_rab;
   long      akv;
   long      bearb;
   char      pers_nam[9];
   short     delstatus;
   long      erl_kto_1;
   long      erl_kto_2;
   long      erl_kto_3;
   long      we_kto_1;
   long      we_kto_2;
   long      we_kto_3;
};
extern struct HWG hwg, hwg_null;

#line 7 "hwg.rh"

class HWG_CL : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               HWG_CL () : DB_CLASS ()
               {
               }
};
#endif
