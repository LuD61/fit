#ifndef _A_BAS_DEF
#define _A_BAS_DEF
/*   Datenbankfunktionen                                  */
#include "wmaskc.h"

#define HNDW 1
#define EIG 2
#define EIG_DIV 3

struct A_BAS {
   double    a;
   short     mdn;
   short     fil;
   char      a_bz1[25];
   char      a_bz2[25];
   double    a_gew;
   short     a_typ;
   short     a_typ2;
   short     abt;
   long      ag;
   char      best_auto[2];
   char      bsd_kz[2];
   char      cp_aufschl[2];
   short     delstatus;
   short     dr_folge;
   long      erl_kto;
   char      hbk_kz[2];
   short     hbk_ztr;
   char      hnd_gew[2];
   short     hwg;
   char      kost_kz[3];
   short     me_einh;
   char      modif[2];
   short     mwst;
   short     plak_div;
   char      stk_lst_kz[2];
   double    sw;
   short     teil_smt;
   long      we_kto;
   short     wg;
   short     zu_stoff;
   long      akv;
   long      bearb;
   char      pers_nam[9];
   double    prod_zeit;
   char      pers_rab_kz[2];
   double    gn_pkt_gbr;
   long      kost_st;
   char      sw_pr_kz[2];
   long      kost_tr;
   double    a_grund;
   long      kost_st2;
   long      we_kto2;
   char      a_bz3[25];
   long      charg_hand;
   long      intra_stat;
   char      qual_kng[5];
   double    a_ersatz;
   short     a_ers_kz; 
   long      inh_ek; 
   double    a_leih;
   short     zerl_eti;
   char      smt[2];
   short     min_bestellmenge;
   short     staffelung;
   short     filialsperre;
   short     ghsperre;
   char      bild [128];

};
extern struct A_BAS _a_bas, _a_bas_null;

struct A_BAS_ERW {
   double    a;
   char      pp_a_bz1[100];
   char      pp_a_bz2[100];
   char      lgr_tmpr[100];
   char      lupine[2];
   char      schutzgas[2];
   short     huelle;
   short     shop_wg1;
   short     shop_wg2;
   short     shop_wg3;
   short     shop_wg4;
   short     shop_wg5;
   double    tara2;
   short     a_tara2;
   double    tara3;
   short     a_tara3;
   double    tara4;
   short     a_tara4;
   double    salz;
   double    davonfett;
   double    davonzucker;
   double    ballaststoffe;
   char      a_bez[63];
   char      shop_aktion[2];
   char      shop_neu[2];
   char      shop_tv[2];
   double    shop_agew;
   char      zutat[2001];
   short     userdef1;
   short     userdef2;
   short     userdef3;
   short     minstaffgr;
};
extern struct A_BAS_ERW _a_bas_erw, _a_bas_erw_null;


extern BOOL (*TestAKun) (double a);

void SetQueryKun (long kun);
int lese_a (char *);
int lese_a (void);
int lese_a_bas (double);
int Auswahla_basQuery ();
int Auswahla_bas ();
int Auswahla_basBu (HWND, int);
int Showa_bas (char *);
int Showa_basBu (HWND, int, char *);
int Showa_basQuery (HWND, int);
int Preparea_basQuery (form *, char **);
int Showa_basQuery_Bez (HWND, int);
int Preparea_basQuery_Bez (form *, char **);
int Preparea_basQuery (void);
void SetBasSearchMode (int);
void SetBasSearchField (int);
void SetCharBuffa_bas (char *);
void SetMatchCode (BOOL);
#endif