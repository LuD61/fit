#ifndef _LSK_LIST_DEF
#define _LSK_LIST_DEF
#pragma once

#include <odbcinst.h>
#include <sqlext.h>
#include <atlstr.h>

class CLskList
{
public:
	long ls;
	long auf;
	long kun;
	DATE_STRUCT lieferdat;
	short ls_stat;
	TCHAR kun_krz1[17];
	TCHAR kun_bran2[3];

	CLskList(void);
	CLskList(long ls, long auf, long kun, DATE_STRUCT& lieferdat, short ls_stat,
		     LPTSTR kun_krz1, LPTSTR kun_bran2);
	~CLskList(void);
	void SetLieferdat (LPTSTR p);
	void SetLieferdat (CString& ld);
	void SetKunKrz1 (LPTSTR kun_krz1);
	void SetKunBran2 (LPTSTR kun_bran2);
	void SetKunKrz1 (CString kun_krz1);
	void SetKunBran2 (CString kun_bran2);
};
#endif
