#include "StdAfx.h"
#include "PlzListS.h"

CPlzListS::CPlzListS(void)
{
	ort        = "";
	zusatz     = "";
	plz        = "";
	vorwahl    = "";
	bundesland = "";
}

CPlzListS::~CPlzListS(void)
{
}

CPlzListS& CPlzListS::operator= (CPlzListS& PlzList)
{
	ort        = PlzList.ort;
	zusatz     = PlzList.zusatz;
	plz        = PlzList.plz;
	vorwahl    = PlzList.vorwahl;
	bundesland = PlzList.bundesland;
	return *this;
}

