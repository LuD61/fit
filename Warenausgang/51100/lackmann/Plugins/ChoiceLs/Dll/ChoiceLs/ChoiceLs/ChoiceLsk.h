#ifndef _CHOICELSK_DEF
#define _CHOICELSK_DEF

#include "ChoiceX.h"
#include "LskList.h"
#include <vector>

class CChoiceLsk : public CChoiceX
{
    protected :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
        static int Sort7;
        static int Sort8;
        static int Sort9;
        static int Sort10;
        static int Sort11;
        static int Sort12;
		int ptcursor;
		TCHAR ptitem [19];
		TCHAR ptwert [4];
		TCHAR ptbez [37];
		TCHAR ptbezk [17];
		short KunFil;
      
    public :
		short GetKunFil ()
		{
			return KunFil;
		}

		void SetKunFil (short KunFil)
		{
			this->KunFil = KunFil;
		}
		int NewKunFil;
		CString Where;
		CString Types;
		BOOL EnterMdn;
	    std::vector<CLskList *> LskList;
	    std::vector<CLskList *> SelectList;
      	CChoiceLsk(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceLsk(); 
/*
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
*/
        virtual void FillList (void);
        void SearchCol (CListCtrl *, LPTSTR, int);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
	    virtual void SaveSelection (CListCtrl *);
		CLskList *GetSelectedText ();
		CLskList *GetNextSelectedText ();
		CLskList *GetPriorSelectedText ();
		CLskList *GetFirstSelectedText ();
		CLskList *GetLastSelectedText ();
		CLskList *GetCurrentSelectedText ();
		void DeleteChoiceLs (short mdn, short fil, long ls);
        int GetPtBez (LPTSTR, LPTSTR, LPSTR);
		void DestroyList ();
		virtual void SetDefault ();
		virtual void OnEnter ();
	    virtual void OnFilter ();
		virtual void OnF5 ();
		void TestKunFil (short kun_fil);
};
#endif
