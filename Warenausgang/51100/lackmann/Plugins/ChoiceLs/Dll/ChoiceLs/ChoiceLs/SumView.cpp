// SumView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "ChoiceLs.h"
#include "SumView.h"
#include "smtcore.h"


// CSumView

IMPLEMENT_DYNCREATE(CSumView, CFormView)

CSumView::CSumView()
	: CFormView(CSumView::IDD)
{
}

CSumView::~CSumView()
{
}

void CSumView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SMT_GRID, m_SmtGrid);
}

BEGIN_MESSAGE_MAP(CSumView, CFormView)
	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CSumView-Diagnose

#ifdef _DEBUG
void CSumView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSumView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSumView-Meldungshandler

void CSumView::OnInitialUpdate ()
{
	CFormView::OnInitialUpdate ();
	m_SmtGrid.set_SmtTable (SMT_CORE->SmtTable ());
    
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand
	m_SmtGrid.FillList ();

	CCtrlInfo *c_SmtGrid = new CCtrlInfo (&m_SmtGrid, 0, 0, DOCKRIGHT, DOCKBOTTOM);
	c_SmtGrid->rightspace = 5;
	CtrlGrid.Add (c_SmtGrid);
	CtrlGrid.Display ();
}

void CSumView::OnSize (UINT nType, int cx, int cy)
{
	CRect trect;
	GetClientRect (&trect);
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
}

