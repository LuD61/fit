// ChoicePos.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "ChoiceLs.h"
#include "ChoicePos.h"


// CChoicePos-Dialogfeld

IMPLEMENT_DYNAMIC(CChoicePos, CDialog)

CChoicePos::CChoicePos(CWnd* pParent /*=NULL*/)
	: CDialog(CChoicePos::IDD, pParent)
{
	PosMode = PosAuto;
}

CChoicePos::~CChoicePos()
{
}

void CChoicePos::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_POS_DELETE, m_PosDelete);
	DDX_Control(pDX, IDC_POS_AUTO, m_PosAuto);
	DDX_Control(pDX, IDC_POS_ACCEPT, m_PosAccept);
}


BEGIN_MESSAGE_MAP(CChoicePos, CDialog)
	ON_BN_CLICKED(IDC_POS_DELETE, &CChoicePos::OnBnClickedPosDelete)
	ON_BN_CLICKED(IDOK, &CChoicePos::OnBnClickedOk)
	ON_BN_CLICKED(IDC_POS_AUTO, &CChoicePos::OnBnClickedPosAuto)
	ON_BN_CLICKED(IDC_POS_ACCEPT, &CChoicePos::OnBnClickedPosAccept)
END_MESSAGE_MAP()


BOOL CChoicePos::OnInitDialog ()
{
	BOOL ret = CDialog::OnInitDialog ();
	if (PosMode == PosAccept)
	{
		m_PosAccept.SetCheck (BST_CHECKED);
	}
	else if (PosMode == PosAuto)
	{
		m_PosAuto.SetCheck (BST_CHECKED);
	}
	else if (PosMode == PosDelete)
	{
		m_PosDelete.SetCheck (BST_CHECKED);
	}
	else 
	{
		m_PosAuto.SetCheck (BST_CHECKED);
	}
	return ret;
}


void CChoicePos::SetPosMode (int PosMode)
{
	this->PosMode = PosMode;
}

int CChoicePos::GetPosMode ()
{
	return PosMode;
}

// CChoicePos-Meldungshandler

void CChoicePos::OnBnClickedPosDelete()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (m_PosDelete.GetCheck () == BST_CHECKED)
	{
		PosMode = PosDelete;
	}
}

void CChoicePos::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	OnOK();
}

void CChoicePos::OnBnClickedPosAuto()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (m_PosAuto.GetCheck () == BST_CHECKED)
	{
		PosMode = PosAuto;
	}
}

void CChoicePos::OnBnClickedPosAccept()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (m_PosAccept.GetCheck () == BST_CHECKED)
	{
		PosMode = PosAccept;
	}
}
