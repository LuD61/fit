// ChoiceLs.h : Hauptheaderdatei f�r die ChoiceLs-DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "'stdafx.h' vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"		// Hauptsymbole
#include "DataCollection.h"
#include "LskList.h"


// CChoiceLsApp
// Siehe ChoiceLs.cpp f�r die Implementierung dieser Klasse
//

#undef EXPORT
#define EXPORT extern "C" _declspec (dllexport)

class CChoiceLsApp : public CWinApp
{
public:
	CChoiceLsApp();

// �berschreibungen
public:

	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};

EXPORT CDataCollection<CLskList>* LskChoice ();
