#pragma once
#include "afxwin.h"


// CChoicePos-Dialogfeld

class CChoicePos : public CDialog
{
	DECLARE_DYNAMIC(CChoicePos)

public:
	enum
	{
		PosAccept,
		PosDelete,
		PosAuto,
		PosDlgCancel,
	};

	CChoicePos(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CChoicePos();

// Dialogfelddaten
	enum { IDD = IDD_CHOICE_POS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog(); 

	DECLARE_MESSAGE_MAP()

private:
	int PosMode;

public:
	CButton m_OK;
	CButton m_PosDelete;
	CButton m_PosAuto;
	CButton m_PosAccept;

	void SetPosMode (int PosMode);
	int GetPosMode ();
public:
	afx_msg void OnBnClickedPosDelete();
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedPosAuto();
public:
	afx_msg void OnBnClickedPosAccept();
};
