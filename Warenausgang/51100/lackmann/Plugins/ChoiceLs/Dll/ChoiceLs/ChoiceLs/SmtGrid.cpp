#include "StdAfx.h"
#include "SmtGrid.h"

CSmtGrid::CSmtGrid(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_SmtTable = NULL;
}

CSmtGrid::~CSmtGrid(void)
{
}

void CSmtGrid::FillList () 
{
	int i = 0;
	CSmtValues *smtValues;
	CSmtValues **it;

	CListCtrl *listView = GetListView ();
	ClearList ();
    if (m_SmtTable == NULL)
	{
		return;
	}

    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
	m_SmtTable->Start ();
	i = 2;
    SetCol (_T(""),  1, 200, LVCFMT_LEFT);
	while ((it = m_SmtTable->GetNext ()) != NULL)
	{
		smtValues = *it;
		if (smtValues != NULL)
		{
		    SetCol (smtValues->Name ().GetBuffer (), i, 80, LVCFMT_RIGHT);
		}
	}
}
