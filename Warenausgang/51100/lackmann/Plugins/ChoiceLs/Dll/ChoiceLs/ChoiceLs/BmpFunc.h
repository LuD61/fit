#pragma once

class CBmpFunc
{
public:
	CBmpFunc(void);
	~CBmpFunc(void);

public:
    BITMAPINFO FAR *bmap;
    HBITMAP hBitmap;
    HBITMAP hBitmapOrg;
    HBITMAP hBitmapZoom;
    HWND bmphWnd;
    static HGLOBAL hglbDlg;
         
    void SetBitmap (HBITMAP hBitmap);
	HBITMAP GetBitmap (void);

    void TestEnvironment (LPTSTR, LPTSTR);
    HBITMAP ReadBitmap (HDC, LPTSTR);
    void DestroyBitmap (void);
    void BitmapSize (HDC,POINT *);
    void BitmapSize (HDC, HBITMAP, POINT *);
    void BitmapSize (CDC *, HBITMAP, POINT *);
    void DrawBitmap (HDC, int, int);
    void DrawBitmapExt (HDC, int, int);
    void DrawBitmap (HDC, HBITMAP, int, int);
    void DrawCompatibleBitmap (HDC, HBITMAP, int, int);
    void DrawCompatibleBitmap (HDC, int, int);
    void DrawBitmapEx (HDC, int , int, DWORD);
    void StrechBitmap (HDC, int, int);
    void StrechBitmap (HDC, HDC, int, int);
    void StrechBitmap (HDC, int, int, double, double);
    void DrawBitmapPart (HDC, int, int, int, int, int, int);
    void DrawBitmapPart (HDC, HBITMAP, int, int, int, int, int, int);
    void ZoomBitmap (HDC, int, int, int, int, int, int, double);
    void DrawImage (HDC, HBRUSH, CRect *, HBITMAP, HBITMAP, int, int);
    void DrawBackground (HDC, HBRUSH, CRect *);
    void StrechBitmapMem (HWND, int, int);
    static HBITMAP PrintBitmapMem (HBITMAP, HBITMAP, COLORREF);
    static HBITMAP PrintBitmapMem (HBITMAP, HBITMAP, HBRUSH);
    static HBITMAP PrintBitmapMem (HBITMAP, HBITMAP, CRect *,COLORREF);
    static HBITMAP PrintBitmapBottom (HBITMAP, HBITMAP, CRect *,COLORREF, int);
    static HBITMAP PrintBitmapBottom (HBITMAP, HBITMAP, CRect *,COLORREF);
    static HBITMAP PrintBitmapMem (HBITMAP, HBITMAP, CRect *,HBRUSH);
    static HBITMAP PrintBitmapBottom (HBITMAP, HBITMAP, CRect *,HBRUSH, int);
    static HBITMAP PrintBitmapBottom (HBITMAP, HBITMAP, CRect *,HBRUSH);
    static HBITMAP LoadBitmap (HINSTANCE, LPTSTR, LPTSTR);
    static HBITMAP LoadBitmap (HINSTANCE, LPTSTR, LPTSTR, COLORREF BkColor);
    static HBITMAP LoadBitmap (HINSTANCE, DWORD,  DWORD, COLORREF BkColor);
};
