#pragma once


// CSumFrame-Rahmen

class CSumFrame : public CFrameWnd
{
	DECLARE_DYNCREATE(CSumFrame)
public:
	CSumFrame();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CSumFrame();
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnSize (UINT, int, int);
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg BOOL OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext);
	DECLARE_MESSAGE_MAP()
};


