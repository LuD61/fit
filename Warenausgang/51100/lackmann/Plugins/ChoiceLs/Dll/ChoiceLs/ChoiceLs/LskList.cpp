#include "StdAfx.h"
#include "LskList.h"

CLskList::CLskList(void)
{
	mdn = 0;
	ls = 0l;
	auf = 0l;
	kun = 0l;
	ls_stat = 0;
	inka_nr = 0l;
	tou_nr = 0l;
	hinweis[0] = 0;
	kun_krz1[0] = 0;
	kun_bran2[0] = 0;
}

CLskList::CLskList(short mdn, long ls, long auf, long kun, DATE_STRUCT lieferdat, short ls_stat,
				   LPTSTR kun_krz1, LPTSTR kun_bran2)
{
	this->mdn = mdn;
	this->ls = ls;
	this->auf = auf;
	this->kun = kun;
	_stprintf (this->lieferdat, "%02hd.%02hd.%04hd", lieferdat.day, lieferdat.month, lieferdat.year);
	this->ls_stat = ls_stat;
	_tcsncpy (this->kun_krz1, kun_krz1, 16);
    this->kun_krz1[16] = 0;   
	_tcsncpy (this->kun_bran2, kun_bran2, 2);
	this->kun_bran2[2] = 0;
	inka_nr = 0l;
	tou_nr = 0l;
	hinweis[0] = 0;
}

CLskList::CLskList(short mdn, long ls, long auf, long kun, TCHAR lieferdat[], short ls_stat,
				   LPTSTR kun_krz1, LPTSTR kun_bran2)
{
    this->mdn = mdn;
	this->ls = ls;
	this->auf = auf;
	this->kun = kun;
	_tcscpy (this->lieferdat, lieferdat);
	this->ls_stat = ls_stat;
	_tcsncpy (this->kun_krz1, kun_krz1, 16);
    this->kun_krz1[16] = 0;   
	_tcsncpy (this->kun_bran2, kun_bran2, 2);
	this->kun_bran2[2] = 0;
	inka_nr = 0l;
	tou_nr = 0l;
	hinweis[0] = 0;
}

CLskList::~CLskList(void)
{
}

void CLskList::SetLieferdat (LPTSTR p)
{
	strcpy (this->lieferdat, lieferdat);
}

void CLskList::SetLieferdat (CString& ld)
{
	SetLieferdat (ld.GetBuffer ());
}

void CLskList::SetKunKrz1 (LPTSTR kun_krz1)
{
	_tcsncpy (this->kun_krz1, kun_krz1, 16);
    this->kun_krz1[16] = 0;   
}

void CLskList::SetKunBran2 (LPTSTR kun_bran2)
{
	_tcsncpy (this->kun_bran2, kun_bran2, 2);
	this->kun_bran2[2] = 0;
}

void CLskList::SetHinweis (LPTSTR hinweis)
{
	_tcsncpy (this->hinweis, hinweis, 48);
	this->hinweis[48] = 0;
}

void CLskList::SetKunKrz1 (CString kun_krz1)
{
	_tcsncpy (this->kun_krz1, kun_krz1.GetBuffer (), 16);
    this->kun_krz1[16] = 0;   
}

void CLskList::SetKunBran2 (CString kun_bran2)
{
	_tcsncpy (this->kun_bran2, kun_bran2.GetBuffer (), 2);
	this->kun_bran2[2] = 0;
}

void CLskList::SetHinweis (CString hinweis)
{
	_tcsncpy (this->hinweis, hinweis.GetBuffer (), 48);
	this->hinweis[48] = 0;
}
