#ifndef _PLZ_LISTS_DEF
#define _PLZ_LISTS_DEF
#include <string>

using namespace std;

struct SPlzListS
{
	char          ort[37];
	char          zusatz[37];
	char          plz[21];
	char          vorwahl[21];
	char          bundesland[37];
};


class CPlzListS
{
public:
	string          ort;
	string          zusatz;
	string          plz;
	string          vorwahl;
	string          bundesland;
	CPlzListS(void);
	~CPlzListS(void);
	CPlzListS& operator= (CPlzListS& PlzList);
};
#endif
