#pragma once
#include "smttable.h"


// CSmtGridEx-Dialogfeld

class CSmtGridEx : public CListCtrl
{
	DECLARE_DYNAMIC(CSmtGridEx)

public:
	CSmtGridEx();   // Standardkonstruktor
	virtual ~CSmtGridEx();

// Dialogfelddaten

private:
	CSmtTable * m_SmtTable;
protected:
	BOOL SetCol (LPTSTR Txt, int idx, int Width, int Align=LVCFMT_LEFT);
public:
	void set_SmtTable (CSmtTable *table)
	{
		m_SmtTable = table;
	}
    virtual void FillList (void);
private:
    HIMAGELIST hLarge;   // image list for icon view 
    HIMAGELIST hSmall;   // image list for other views 
	DWORD SetStyle (DWORD st);
	DWORD SetListStyle (DWORD st);
	DWORD SetExtendedStyle (DWORD st);
};
