#include "StdAfx.h"
#include "PlzList.h"

CPlzList::CPlzList(void)
{
	ort        = _T("");
	zusatz     = _T("");
	plz        = _T("");
	vorwahl    = _T("");
	bundesland = _T("");
}

CPlzList::~CPlzList(void)
{
}

CPlzList& CPlzList::operator= (CPlzList& PlzList)
{
	ort        = PlzList.ort;
	zusatz     = PlzList.zusatz;
	plz        = PlzList.plz;
	vorwahl    = PlzList.vorwahl;
	bundesland = PlzList.bundesland;
	return *this;
}

CPlzList& CPlzList::operator= (PLZ_CLASS& Plz)
{
	ort        = Plz.plz.ort;
	zusatz     = Plz.plz.zusatz;
	plz        = Plz.plz.plz;
	vorwahl    = Plz.plz.vorwahl;
	bundesland = Plz.plz.bundesland;
	return *this;
}
