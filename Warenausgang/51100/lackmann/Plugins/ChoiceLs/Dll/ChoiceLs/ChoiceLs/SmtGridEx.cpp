// SmtGridEx.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "ChoiceLs.h"
#include "SmtGridEx.h"


// CSmtGridEx-Dialogfeld

IMPLEMENT_DYNAMIC(CSmtGridEx, CListCtrl)

CSmtGridEx::CSmtGridEx()
	: CListCtrl ()
{
	m_SmtTable = NULL;
}

CSmtGridEx::~CSmtGridEx()
{
}

void CSmtGridEx::FillList () 
{
    HICON hiconItem;     // icon for list-view items 
	int i = 0;
	CSmtValues *smtValues;
	CSmtValues **it;

    if (m_SmtTable == NULL)
	{
		return;
	}

/*
    hLarge = ImageList_Create(GetSystemMetrics(SM_CXICON), 
        GetSystemMetrics(SM_CYICON), ILC_MASK, 1, 1); 
    hSmall = ImageList_Create(GetSystemMetrics(SM_CXSMICON), 
        GetSystemMetrics(SM_CYSMICON), ILC_MASK, 1, 1); 
 
    hiconItem = LoadIcon(AfxGetApp()->m_hInstance, 
		                 MAKEINTRESOURCE(IDI_ICON2)); 
    ImageList_AddIcon(hLarge, hiconItem); 
    ImageList_AddIcon(hSmall, hiconItem); 
    DestroyIcon(hiconItem); 


    HWND hWndListView = m_hWnd;

    ListView_SetImageList(hWndListView, hLarge, LVSIL_NORMAL); 
    ListView_SetImageList(hWndListView, hSmall, LVSIL_SMALL); 
*/


    DWORD Style = SetStyle (LVS_REPORT);

    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
	m_SmtTable->Start ();
	i = 2;
    SetCol (_T(""),  1, 200, LVCFMT_LEFT);
	while ((it = m_SmtTable->GetNext ()) != NULL)
	{
		smtValues = *it;
		if (smtValues != NULL)
		{
		    SetCol (smtValues->Name ().GetBuffer (), i, 80, LVCFMT_RIGHT);
		}
		i ++;
	}
}

BOOL CSmtGridEx::SetCol (LPTSTR Txt, int idx, int Width, int Align)
{  
   LV_COLUMN Col;

   HWND hWndListView = this->m_hWnd;

   Col.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;   
   Col.fmt = Align;
   Col.cx  = Width;
   Col.pszText = Txt;
   Col.iSubItem = idx;
   return ListView_InsertColumn(hWndListView, idx, &Col); 
}

DWORD CSmtGridEx::SetStyle (DWORD st)
{
    DWORD Style = GetWindowLong (m_hWnd, GWL_STYLE);

    Style &= ~(LVS_ICON | LVS_SMALLICON | LVS_LIST | LVS_REPORT);
    SetWindowLong (m_hWnd, GWL_STYLE, Style |= LVS_REPORT); 
    return Style;
}

DWORD CSmtGridEx::SetListStyle (DWORD st)
{
    DWORD Style = GetWindowLong (m_hWnd, GWL_STYLE);
    Style |= st;
    SetWindowLong (m_hWnd, GWL_STYLE, Style); 
    return Style;
}


DWORD CSmtGridEx::SetExtendedStyle (DWORD st)
{

    DWORD style = GetExtendedStyle ();
    style |= st;
	SetExtendedStyle (style);
    return style;
}

// CSmtGridEx-Meldungshandler
