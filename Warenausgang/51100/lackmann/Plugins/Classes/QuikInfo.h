#pragma once
#include "afxwin.h"

class CQuikInfo :
	public CStatic
{

protected:
	CString Text;
	BOOL NoUpdate;
	virtual void DrawItem (LPDRAWITEMSTRUCT  lpDrawItemStruct);
	DECLARE_MESSAGE_MAP()

public:
	CQuikInfo(void);
	~CQuikInfo(void);

	enum
	{
		Center = 0,
		Left = 1,
		Right = 2,
	};
	int Orientation;
	int WindowOrientation;
	COLORREF TextColor;
	long Delay;
	CWnd* Tool;

	virtual BOOL Create(LPCTSTR lpszText, CWnd *Tool,  
		                DWORD dwStyle, RECT& rect,   
						CWnd* pParentWnd); 

	void Draw (CDC&);

	void Show ();
	void Hide ();
	void SetSize ();
	void SetText (LPTSTR Text);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
