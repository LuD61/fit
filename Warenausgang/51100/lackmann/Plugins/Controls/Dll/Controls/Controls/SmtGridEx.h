#pragma once
#include "smttable.h"


// CSmtGridEx-Dialogfeld

class CSmtGridEx : public CListCtrl
{
	DECLARE_DYNAMIC(CSmtGridEx)

public:
	enum GRID_ROW
	{
		Amount = 0,
		Weight = 1,
		Liq = 2,
		SmtPart = 3,
	};

public:
	CSmtGridEx();   // Standardkonstruktor
	virtual ~CSmtGridEx();

// Dialogfelddaten

private:
	CSmtTable * m_SmtTable;
protected:
	BOOL SetCol (LPTSTR Txt, int idx, int Width, int Align=LVCFMT_LEFT);
public:
	void set_SmtTable (CSmtTable *table)
	{
		m_SmtTable = table;
	}
    virtual short FillList (void);
private:
	CDataCollection<LPSTR> RowText;
    HIMAGELIST hLarge;   // image list for icon view 
    HIMAGELIST hSmall;   // image list for other views 
	DWORD SetStyle (DWORD st);
	DWORD SetListStyle (DWORD st);
	DWORD SetExtendedStyle (DWORD st);
	BOOL InsertItem (int idx);
	BOOL InsertItem (int idx, int image);
	BOOL SetItemText (LPTSTR Txt, int idx, int pos);
};
