#include "StdAfx.h"
#include "VHLabel.h"

CVHLabel::CVHLabel(void)
{
}

CVHLabel::~CVHLabel(void)
{
}

void CVHLabel::FillVRectParts (CDC &cDC, COLORREF BkColor, CRect rect)
{
	int blue = (BkColor >> 16) & 0xFF;
	int green = (BkColor >> 8) & 0xFF;
	int red = (BkColor) & 0xFF;
	int height = rect.bottom - rect.top;
    int parts = 10;
	if (height > 50)
	{
		parts = 25;
	}
	else
	{
	    parts = 15;
	}
	int colordiff = 5;
	int part = (rect.bottom) / parts;
	CRect rect1 (rect.left, rect.top, rect.right, part); 
	int redplus = 240 - red;
	int greenplus = 240 - green;
	int blueplus = 240 - blue;
	int redstep = redplus / parts;
	int greenstep = greenplus / parts;
	int bluestep = blueplus / parts;
	red = min (red + redplus, 240);
	green = min (green + greenplus, 240);
	blue = min (blue + blueplus, 240);
	if (part == 1) 
	{
		part = 2;
	}

	for (int i = 0; i < parts; i ++)
	{
		cDC.FillRect (&rect1, &CBrush (RGB (red, green, blue)));
		rect1.top += part - 1;
		rect1.bottom += part;
		if (rect1.top >= rect.bottom - part) return;
		red -= redstep;
		green -= greenstep;
		blue -= bluestep;
	}
	rect1.bottom = rect.bottom; 
	cDC.FillRect (&rect1, &CBrush (RGB (red, green, blue)));
}

void CVHLabel::FillRectParts (CDC &cDC, COLORREF BkColor, CRect rect)
{
	int blue = (BkColor >> 16) & 0xFF;
	int green = (BkColor >> 8) & 0xFF;
	int red = (BkColor) & 0xFF;
	int parts = 120;
	int part = (rect.right) / parts;
	if (part < 1)
	{
		FillVRectParts (cDC, BkColor, rect);
		return;
	}

	int colordiff = 2;
	part ++;

	CRect rect1 (rect.left, rect.top, part, rect.bottom); 

	for (int i = 0; i < parts / 3; i ++)
	{
		FillVRectParts (cDC, RGB (red, green, blue), rect1);
		rect1.left += part - 1;
		rect1.right += part;
		if (red <= 255 - colordiff) red += colordiff;

		FillVRectParts (cDC, RGB (red, green, blue), rect1);

		rect1.left += part - 1;
		rect1.right += part;
		if (green <= 255 - colordiff) green += colordiff;
		FillVRectParts (cDC, RGB (red, green, blue), rect1);

		rect1.left += part - 1;
		rect1.right += part;
		if (blue <= 255 - colordiff) blue += colordiff;
	}
	rect1.left += part - 2;
	rect1.right = rect.right; 
	FillVRectParts (cDC, RGB (red, green, blue), rect1);

}
