#pragma once
#include "label.h"

class CVHLabel :
	public CLabel
{
public:
	CVHLabel(void);
public:
	~CVHLabel(void);
	virtual void FillVRectParts (CDC &cDC, COLORREF color, CRect rect);
	virtual void FillRectParts (CDC &cDC, COLORREF color, CRect rect);
};
