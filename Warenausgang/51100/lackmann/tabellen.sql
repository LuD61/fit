alter table aufk add storno_grund1 integer;
alter table aufk add storno_grund2 integer;
alter table aufk add storno_grund3 integer;
alter table aufk add storno_text char (128);
update aufk set storno_grund1 = 0 where storno_grund1 is null;
update aufk set storno_grund2 = 0 where storno_grund2 is null;
update aufk set storno_grund3 = 0 where storno_grund3 is null;
update aufk set storno_text = "" where storno_text is null;

