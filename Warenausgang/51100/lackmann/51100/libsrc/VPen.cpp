#include "VPen.h"

VPen::VPen () : CVector ()
{
}

VPen::~VPen ()
{
}


BOOL VPen::Exist (RPen* rPen)
{
	RPen *p;
	FirstPosition ();
	while ((p = (RPen *) GetNext ()) != NULL)
	{
		if (*p == *rPen && p->GetPen () != NULL)
		{
			p->inc ();
			rPen->SetPen (p->GetPen ());
			return TRUE;
		}
	}
	return FALSE;
}

BOOL VPen::CanDestroy (HPEN hPen)
{
	RPen *p;
	FirstPosition ();
	while ((p = (RPen *) GetNext ()) != NULL)
	{
		if (p->GetPen () == hPen)
		{
			break;
		}
	}
	if (p == NULL)
	{
		return TRUE;
	}

	if (p->GetInstances () == 0)
	{
		return TRUE;
	}
	p->dec ();
	if (p->GetInstances () == 0)
	{
		Drop (p);
		return TRUE;
	}
	return FALSE;
}
