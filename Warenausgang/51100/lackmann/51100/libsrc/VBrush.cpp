#include "VBrush.h"

VBrush::VBrush () : CVector ()
{
}

VBrush::~VBrush ()
{
}

/*
void VBrush::SetStdFont (HFONT Font)
{
	StdFont = Font;
}
*/

BOOL VBrush::Exist (RBrush* rBrush)
{
	RBrush *b;
	FirstPosition ();
	while ((b = (RBrush *) GetNext ()) != NULL)
	{
		if (*b == *rBrush && b->GetBrush () != NULL)
		{
			b->inc ();
			rBrush->SetBrush (b->GetBrush ());
			return TRUE;
		}
	}
	return FALSE;
}

BOOL VBrush::CanDestroy (HBRUSH hBrush)
{
	RBrush *b;
	FirstPosition ();
	while ((b = (RBrush *) GetNext ()) != NULL)
	{
		if (b->GetBrush () == hBrush)
		{
			break;
		}
	}
	if (b == NULL)
	{
		return TRUE;
	}

	if (b->GetInstances () == 0)
	{
		return TRUE;
	}
	b->dec ();
	if (b->GetInstances () == 0)
	{
		Drop (b);
		return TRUE;
	}
	return FALSE;
}
