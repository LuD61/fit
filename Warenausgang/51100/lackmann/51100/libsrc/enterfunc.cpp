#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_enter.h"
#include "enterfunc.h"
#include "text.h"


int EnterF::BreakOk (void)
{
	   syskey = KEYCR;
       return 1;
}

int EnterF::BreakCancel (void)
{
	   syskey = KEY5;
       return 1;
}

int EnterF::BreakEnter (void)
{
	   if (syskey == KEYCR)
	   {
	            return 1;
	   }
	   return 0;
}


void EnterF::EnterText (HWND hMainWindow, char *label, char *text)
/**
Dialog mit einem Eingabefeld und 2 Buttons
**/
{
       HANDLE hMainInst;
       CFIELD C1 ("text1", "", 
 			                           0, 0, 0, 1, NULL, "",
                                       CDISPLAYONLY,
						               600, &textfont, 0, TRANSPARENT,
									   NULL,NULL);
       CFIELD C2 ("edit1", "",
						               20, 0, 12, 1, NULL, "",  
                                       CEDIT,
						               601, &textfont, 0, 0,
									   NULL,BreakEnter);
						
       CFIELD *_fWork0[] = {&C1,&C2};
       CFORM fWork0 (2, _fWork0);

	   CFIELD B1 ("BU1", "     OK    ", 
 			                           11, 0, 0, 3, NULL, "",
                                       CBUTTON,
						               603, &textfont, 0, 0,
									   NULL,BreakOk);
	   CFIELD B2 ("BU2", " Abbrechen ", 
 			                           11, 0, 12, 3, NULL, "",
                                       CBUTTON,
						               604, &textfont, 0, 0,
									   NULL,BreakCancel);
       CFIELD *_fWork1[] = {&B1,&B2};
       CFORM fWork1 (2, _fWork1);


       CFIELD C3 ("form0", &fWork0, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   602, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD C4 ("form1", &fWork1, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   605, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD *_fWork[] = {&C3, &C4};
       CFORM fWork (2, _fWork);

       DWORD style = WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU;
 			         
       hMainInst = (HANDLE) GetClassLong (hMainWindow, GCL_HMODULE); 

       C1.SetFeld (label);
       C2.SetFeld (text);
       fWork1.Setchpos (0, 2);

       Enter = new fEnter (-1, -1, 46, 5, NULL);
       Enter->SetMask (&fWork);

       textfont.FontBkColor = Color;
       Enter->SetBkColor (Color);

       Enter->OpenWindow (hMainInst, hMainWindow, style );
       Enter->ProcessMessages ();
       Enter->DestroyWindow ();
       delete Enter;
}

void EnterF::EnterText (HWND hMainWindow, char *label, char *text, int len, char *picture)
/**
Dialog mit einem Eingabefeld und 2 Buttons
**/
{
       HANDLE hMainInst;
       CFIELD C1 ("text1", "", 
 			                           0, 0, 0, 1, NULL, "",
                                       CDISPLAYONLY,
						               600, &textfont, 0, TRANSPARENT,
									   NULL,NULL);
       CFIELD C2 ("edit1", "",
						               len, 0, 12, 1, NULL, "",  
                                       CEDIT,
						               601, &textfont, 0, 0,
									   NULL,BreakEnter);
						
       CFIELD *_fWork0[] = {&C1,&C2};
       CFORM fWork0 (2, _fWork0);

	   CFIELD B1 ("BU1", "     OK    ", 
 			                           11, 0, 0, 3, NULL, "",
                                       CBUTTON,
						               603, &bufont, 0, 0,
									   NULL,BreakOk);
	   CFIELD B2 ("BU2", " Abbrechen ", 
 			                           11, 0, 12, 3, NULL, "",
                                       CBUTTON,
						               604, &bufont, 0, 0,
									   NULL,BreakCancel);
       CFIELD *_fWork1[] = {&B1,&B2};
       CFORM fWork1 (2, _fWork1);


       CFIELD C3 ("form0", &fWork0, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   602, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD C4 ("form1", &fWork1, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   605, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD *_fWork[] = {&C3, &C4};
       CFORM fWork (2, _fWork);

       fWork.GetCfield ("edit1")->SetX (strlen (label));

       if (picture && strlen (picture))
       {
           fWork.SetFieldPicture ("edit1", picture);
           if (strchr (picture, 'f'))
           {
               fWork.SetFieldBkMode ("edit1", ES_RIGHT);
           }
           else if (strchr (picture, 'd'))
           {
               fWork.SetFieldBkMode ("edit1", ES_RIGHT);
           }
       }

       DWORD style = WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU;
 			         
       hMainInst = (HANDLE) GetClassLong (hMainWindow, GCL_HMODULE); 

       C1.SetFeld (label);
	   Text::TrimRight (text);
       C2.SetFeld (text);
       fWork1.Setchpos (0, 2);
       len = len + strlen (label) + 4;
       Enter = new fEnter (-1, -1, len, 5, NULL);
       Enter->SetMask (&fWork);

       textfont.FontBkColor = Color;
       Enter->SetBkColor (Color);

       Enter->OpenWindow (hMainInst, hMainWindow, style );
       Enter->ProcessMessages ();
       Enter->DestroyWindow ();
       delete Enter;
}


void EnterF::EnterText (HWND hMainWindow, char *label, char *text, int len, char *picture, 
						char *ComboTxt [])
/**
Dialog mit einem Eingabefeld und 2 Buttons
**/
{
       HANDLE hMainInst;
       CFIELD C1 ("text1", "", 
 			                           0, 0, 0, 1, NULL, "",
                                       CDISPLAYONLY,
						               600, &textfont, 0, TRANSPARENT,
									   NULL,NULL);
/*
                     new CFIELD ("default_me_kz", bzgf.default_me_kz,
                                 17, 5, 66, 2,  NULL, "", CCOMBOBOX,
                                 DEFAULT_ME_KZ_CTL, Font, 0,    CBS_DROPDOWNLIST |
                                                        WS_VSCROLL),
*/

       CFIELD C2 ("edit1", "",
						               len + 3, 10, 12, 1, NULL, "",  
                                       CCOMBOBOX,
						               601, &textfont, 0, CBS_DROPDOWN | WS_VSCROLL,
									   NULL,BreakEnter);
	   if (ComboList)
	   {
		   C2.SetBkMode (CBS_DROPDOWNLIST | WS_VSCROLL);
	   }
						
       CFIELD *_fWork0[] = {&C1,&C2};
       CFORM fWork0 (2, _fWork0);

	   CFIELD B1 ("BU1", "     OK    ", 
 			                           11, 0, 0, 3, NULL, "",
                                       CBUTTON,
						               603, &bufont, 0, 0,
									   NULL,BreakOk);
	   CFIELD B2 ("BU2", " Abbrechen ", 
 			                           11, 0, 12, 3, NULL, "",
                                       CBUTTON,
						               604, &bufont, 0, 0,
									   NULL,BreakCancel);
       CFIELD *_fWork1[] = {&B1,&B2};
       CFORM fWork1 (2, _fWork1);


       CFIELD C3 ("form0", &fWork0, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   602, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD C4 ("form1", &fWork1, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   605, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD *_fWork[] = {&C3, &C4};
       CFORM fWork (2, _fWork);

       fWork.GetCfield ("edit1")->SetX (strlen (label));

       if (picture && strlen (picture))
       {
           fWork.SetFieldPicture ("edit1", picture);
           if (strchr (picture, 'f'))
           {
               fWork.SetFieldBkMode ("edit1", ES_RIGHT);
           }
           else if ((strchr (picture, 'd') != NULL) && (strchr (picture, '.') == NULL))
           {
               fWork.SetFieldBkMode ("edit1", ES_RIGHT);
           }
       }

       DWORD style = WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU;
 			         
       hMainInst = (HANDLE) GetClassLong (hMainWindow, GCL_HMODULE); 

       C1.SetFeld (label);
	   Text::TrimRight (text);
       C2.SetFeld (text);
       C2.SetCombobox (ComboTxt); 
       fWork1.Setchpos (0, 2);
       len = len + strlen (label) + 4;
       Enter = new fEnter (-1, -1, len, 5, NULL);
       Enter->SetMask (&fWork);

       textfont.FontBkColor = Color;
       Enter->SetBkColor (Color);

       Enter->OpenWindow (hMainInst, hMainWindow, style );
       Enter->ProcessMessages ();
       Enter->DestroyWindow ();
       delete Enter;
}


void EnterF::EnterText (HINSTANCE hMainInst, HWND hMainWindow, 
                        char *label, char *text, int len, char *picture,
                        char *Caption)
/**
Dialog mit einem Eingabefeld und 2 Buttons
**/
{
       CFIELD C1 ("text1", "", 
 			                           0, 0, 0, 1, NULL, "",
                                       CDISPLAYONLY,
						               600, &textfont, 0, TRANSPARENT,
									   NULL,NULL);
       CFIELD C2 ("edit1", "",
						               len, 0, 12, 1, NULL, "",  
                                       CEDIT,
						               601, &textfont, 0, 0,
									   NULL,BreakEnter);
						
       CFIELD *_fWork0[] = {&C1,&C2};
       CFORM fWork0 (2, _fWork0);

	   CFIELD B1 ("BU1", "     OK    ", 
 			                           11, 0, 0, 3, NULL, "",
                                       CBUTTON,
						               603, &bufont, 0, 0,
									   NULL,BreakOk);
	   CFIELD B2 ("BU2", " Abbrechen ", 
 			                           11, 0, 12, 3, NULL, "",
                                       CBUTTON,
						               604, &bufont, 0, 0,
									   NULL,BreakCancel);
       CFIELD *_fWork1[] = {&B1,&B2};
       CFORM fWork1 (2, _fWork1);


       CFIELD C3 ("form0", &fWork0, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   602, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD C4 ("form1", &fWork1, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   605, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD *_fWork[] = {&C3, &C4};
       CFORM fWork (2, _fWork);

       fWork.GetCfield ("edit1")->SetX (strlen (label));

       if (picture && strlen (picture))
       {
           fWork.SetFieldPicture ("edit1", picture);
           if (strchr (picture, 'f'))
           {
               fWork.SetFieldBkMode ("edit1", ES_RIGHT);
           }
           else if (strchr (picture, 'd'))
           {
               fWork.SetFieldBkMode ("edit1", ES_RIGHT);
           }
       }

       DWORD style = WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU;
 			         
       C1.SetFeld (label);
       C2.SetFeld (text);
       fWork1.Setchpos (0, 2);
       len = len + strlen (label) + 4;
       Enter = new fEnter (-1, -1, len, 5, Caption);
       Enter->SetMask (&fWork);

       textfont.FontBkColor = Color;
       Enter->SetBkColor (Color);

       Enter->OpenWindow (hMainInst, hMainWindow, style );
       Enter->ProcessMessages ();
       Enter->DestroyWindow ();
       delete Enter;
}

void EnterF::EnterUpDown (HWND hMainWindow, char *label, char *text)
/**
Dialog mit einem Eingabefeld und 2 Buttons
**/
{
       HANDLE hMainInst;
       CFIELD C1 ("text1", "", 
 			                           0, 0, 0, 1, NULL, "",
                                       CDISPLAYONLY,
						               600, &textfont, 0, TRANSPARENT,
									   NULL,NULL);
       CFIELD C2 ("edit1", "",
						               4, 0, 12, 1, NULL, "%3hd",  
                                       CEDIT,
						               601, &textfont, 0, 0,
									   NULL,BreakEnter);
						
       CFIELD *_fWork0[] = {&C1,&C2};
       CFORM fWork0 (2, _fWork0);

	   CFIELD B1 ("BU1", "     OK    ", 
 			                           11, 2, 0, 4, NULL, "",
                                       CBUTTON,
						               603, &bufont, 0, 0,
									   NULL,BreakOk);
	   CFIELD B2 ("BU2", " Abbrechen ", 
 			                           11, 2, 12, 4, NULL, "",
                                       CBUTTON,
						               604, &bufont, 0, 0,
									   NULL,BreakCancel);
       CFIELD *_fWork1[] = {&B1,&B2};
       CFORM fWork1 (2, _fWork1);


       CFIELD C3 ("form0", &fWork0, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   602, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD C4 ("form1", &fWork1, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   605, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD *_fWork[] = {&C3, &C4};
       CFORM fWork (2, _fWork);

       DWORD style = WS_BORDER | WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU;
 			         
       hMainInst = (HANDLE) GetClassLong (hMainWindow, GCL_HMODULE); 

       C1.SetFeld (label);
       C2.SetFeld (text);
       C2.SetX (strlen (label) + 1);
	   C2.SetUpDown (TRUE, 200, 0, atoi (text));
	   fWork0.SetLayout (XFLOW);
       Enter = new fEnter (-1, -1, 30, 5, "Numerische Eingabe");
       Enter->SetMask (&fWork);

       textfont.FontBkColor = Color;
       Enter->SetBkColor (Color);

       Enter->OpenWindow (hMainInst, hMainWindow, style );
	   EnableWindow (hMainWindow, FALSE);
       Enter->ProcessMessages ();
	   EnableWindow (hMainWindow, TRUE);
       Enter->DestroyWindow ();
       delete Enter;
}


void EnterF::EnterTextMehrf (HWND hMainWindow, char *Caption,
							 char *label1, char *text1, int len1, char *picture1,char *ComboTxt1 [],
							 char *label2, char *text2, int len2, char *picture2,char *ComboTxt2 [],
							 char *label3, char *text3, int len3, char *picture3,char *ComboTxt3 [],
							 char *label4, char *text4, int len4, char *picture4,char *ComboTxt4 []
							 )
/**
Dialog mit einem TextEingabefeld und 3 Comboboxen
**/
{
	int len; int lenlabel;
       HANDLE hMainInst;
      CFIELD C1_1 ("text1", "", 0,         0,  0, 1, NULL, "", CDISPLAYONLY, 600, &textfont, 0, TRANSPARENT, NULL,NULL);
      CFIELD C2_1 ("edit1", "", len1 + 3,  0, 12, 1, NULL, "", CEDIT,    601, &textfont, 0,  WS_VSCROLL, NULL,NULL);
      CFIELD C1_2 ("text2", "", 0,         0,  0, 3, NULL, "", CDISPLAYONLY, 600, &textfont, 0, TRANSPARENT, NULL,NULL);
      CFIELD C2_2 ("edit2", "", len2 + 3,  0, 12, 3, NULL, "", CEDIT,    621, &textfont, 0,  WS_VSCROLL, NULL,NULL);
      CFIELD C1_3 ("text3", "", 0,         0,  0, 5, NULL, "", CDISPLAYONLY, 600, &textfont, 0, TRANSPARENT, NULL,NULL);
      CFIELD C2_3 ("edit3", "", len3 + 3,  0, 12, 5, NULL, "", CEDIT,    631, &textfont, 0, WS_VSCROLL, NULL,NULL);
      CFIELD C1_4 ("text4", "", 0,         0,  0, 7, NULL, "", CDISPLAYONLY, 600, &textfont, 0, TRANSPARENT, NULL,NULL);
      CFIELD C2_4 ("edit4", "", len4 + 3,  0, 12, 7, NULL, "", EDIT,    641, &textfont, 0, 0, NULL,NULL);
	   if (ComboList)
	   {
		   if (ComboTxt1 != NULL) { C2_1.SetBkMode (CBS_DROPDOWNLIST | WS_VSCROLL);C2_1.SetAttribut (CCOMBOBOX); C2_1.SetCY (10); }
		   if (ComboTxt2 != NULL) { C2_2.SetBkMode (CBS_DROPDOWNLIST | WS_VSCROLL);C2_2.SetAttribut (CCOMBOBOX); C2_2.SetCY (10); } 
		   if (ComboTxt3 != NULL) { C2_3.SetBkMode (CBS_DROPDOWNLIST | WS_VSCROLL);C2_3.SetAttribut (CCOMBOBOX); C2_3.SetCY (10); } 
		   if (ComboTxt4 != NULL) { C2_4.SetBkMode (CBS_DROPDOWNLIST | WS_VSCROLL);C2_4.SetAttribut (CCOMBOBOX); C2_4.SetCY (10); }
		   
	   }
						
       CFIELD *_fWork0[] = {&C1_1,&C2_1,&C1_2,&C2_2,&C1_3,&C2_3,&C1_4,&C2_4};
       CFORM fWork0 (8, _fWork0);

	   CFIELD B1 ("BU1", "     OK    ", 
 			                           11, 0, 0, 10, NULL, "",
                                       CBUTTON,
						               603, &bufont, 0, 0,
									   NULL,BreakOk);
	   CFIELD B2 ("BU2", " Abbrechen ", 
 			                           11, 0, 12, 10, NULL, "",
                                       CBUTTON,
						               604, &bufont, 0, 0,
									   NULL,BreakCancel);
       CFIELD *_fWork1[] = {&B1,&B2};
       CFORM fWork1 (2, _fWork1);


       CFIELD C3 ("form0", &fWork0, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   602, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD C4 ("form1", &fWork1, 0, 0, -1, 0, NULL, "",
		                   CFFORM,
						   605, &textfont, 0, 0,
						   NULL, NULL);
       CFIELD *_fWork[] = {&C3, &C4};
       CFORM fWork (2, _fWork);

	   fWork.GetCfield ("edit1")->SetX (strlen (label1));
	   fWork.GetCfield ("edit2")->SetX (strlen (label2));
	   fWork.GetCfield ("edit3")->SetX (strlen (label3));
	   fWork.GetCfield ("edit4")->SetX (strlen (label4));

       if (picture1 && strlen (picture1))
       {
           fWork.SetFieldPicture ("edit1", picture1);
           if (strchr (picture1, 'f'))
           {
               fWork.SetFieldBkMode ("edit1", ES_RIGHT);
           }
           else if ((strchr (picture1, 'd') != NULL) && (strchr (picture1, '.') == NULL))
           {
               fWork.SetFieldBkMode ("edit1", ES_RIGHT);
           }
       }

       DWORD style = WS_POPUP | WS_DLGFRAME | WS_CAPTION | WS_SYSMENU;
 			         
       hMainInst = (HANDLE) GetClassLong (hMainWindow, GCL_HMODULE); 

       C1_1.SetFeld (label1);
       C1_2.SetFeld (label2);
       C1_3.SetFeld (label3);
       C1_4.SetFeld (label4);
	   Text::TrimRight (text1);
       C2_1.SetFeld (text1);
       C2_1.SetCombobox (ComboTxt1); 
	   Text::TrimRight (text2);
       C2_2.SetFeld (text2);
       C2_2.SetCombobox (ComboTxt2); 
	   Text::TrimRight (text3);
       C2_3.SetFeld (text3);
       C2_3.SetCombobox (ComboTxt3); 
	   Text::TrimRight (text4);
       C2_4.SetFeld (text4);
       C2_4.SetCombobox (ComboTxt4); 
       fWork1.Setchpos (0, 2);
	   len = len1;
	   if (len < len2) len = len2;
	   if (len < len3) len = len3;
	   if (len < len4) len = len4;
	   lenlabel = strlen(label1);
	   if (lenlabel < (int) strlen(label2)) lenlabel = strlen(label2);
	   if (lenlabel < (int) strlen(label3)) lenlabel = strlen(label3);
	   if (lenlabel < (int) strlen(label4)) lenlabel = strlen(label4);
       len = len + lenlabel + 4;
       Enter = new fEnter (-1, -1, len, 12, Caption);
       Enter->SetMask (&fWork);

       textfont.FontBkColor = Color;
       Enter->SetBkColor (Color);

       Enter->OpenWindow (hMainInst, hMainWindow, style );
       Enter->ProcessMessages ();
       Enter->DestroyWindow ();
       delete Enter;
}
