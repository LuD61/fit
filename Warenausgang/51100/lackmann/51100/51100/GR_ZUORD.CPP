#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "gr_zuord.h"
#include "dbfunc.h"

struct GR_ZUORD _gr_zuord, _gr_zuord_null;

static void FillUb  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%7s %-16s",
                          "Gruppe ", "Name");
}

static void FillValues  (char *buffer)
/**
Werte fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%7hd %-16.16s",
                          _gr_zuord.gr, _gr_zuord.gr_bz1);
}


int GR_CLASS::lese_gr (short mdn, short gr)
/**
Tabelle gr_zuord fuer Mandant mdn lesen.
**/
{
         if (cursor_gr == -1)
         {
                ins_quest ((char *) &_gr_zuord.mdn, 1, 0);
                ins_quest ((char *) &_gr_zuord.gr, 1, 0);
                out_quest ((char *) _gr_zuord.gr_bz1, 0, 16);
                cursor_gr =
                    prepare_sql ("select gr_bz1 "
                                 "from gr_zuord "
                                 "where mdn = ? "
                                 "and gr = ? ");
         }
         _gr_zuord.mdn = mdn;
         _gr_zuord.gr =  gr;
         open_sql (cursor_gr);
         fetch_sql (cursor_gr);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int GR_CLASS::lese_gr (void)
/**
Naechsten Satz aus Tabelle gr_zuord lesen.
**/
{
         fetch_sql (cursor_gr);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int GR_CLASS::ShowAllGr (short mdn)
/**
Auswahl ueber Gruppen anzeigen.
**/
{
         int cursor_ausw;
         int i;

         ins_quest ((char *) &_gr_zuord.mdn, 1, 0);
         out_quest ((char *) &_gr_zuord.gr, 1, 0);
         out_quest (_gr_zuord.gr_bz1, 0, 24);

         cursor_ausw = prepare_scroll ("select gr, gr_bz1 from "
                                       "gr_zuord "
                                       "where gr > 0 "
                                       "and mdn = ? "
                                       "order by gr");
         if (sqlstatus)
         {
                     return (-1);
         }
         _gr_zuord.mdn = mdn;
         i = DbAuswahl (cursor_ausw, FillUb,
                                     FillValues);
         UpdateFkt ();
         SetFocus (current_form->mask[currentfield].feldid);

         SetCurrentField (currentfield);

         if (syskey == KEYESC || syskey == KEY5)
         {
                     close_sql (cursor_ausw);
                     return 0;
         }
         fetch_scroll (cursor_ausw, DBABSOLUTE, i + 1);
         close_sql (cursor_ausw);
         return 0;
}
