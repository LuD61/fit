// GrpPrice.h: Schnittstelle f�r die Klasse GrpPrice.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRPPRICE_H__F16513EA_60FA_4FAA_A9E3_BBC1BCD1703A__INCLUDED_)
#define AFX_GRPPRICE_H__F16513EA_60FA_4FAA_A9E3_BBC1BCD1703A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Ipr.h"

class CGrpPrice  
{
private:
	IPR_CLASS Ipr;
	int AGewCursor;
	double a_gew;
	short me_einh;
public:
	void SetKeys (short mdn, long pr_gr_stuf, long kun_pr, long kun, double a)
	{
		ipr.mdn = mdn;
		ipr.pr_gr_stuf = pr_gr_stuf;
		ipr.kun_pr = kun_pr;
        ipr.a = a;
	}

	void SetAKey (double a);
	void SetAKey (double a, short me_einh, double a_gew);

	CGrpPrice();
	virtual ~CGrpPrice();
	BOOL Read (double *pPrVk, double *pLsPr);
	double CalculateVkPr (double pr);
	double CalculateLdPr (double pr);
	void CalculateVkPr (double pr, LPSTR sMarge, int size);
	void CalculateLdPr (double pr, LPSTR sMarge, int size);
	void CalculateLdPr (double pr, LPSTR sMarge, int size, LPSTR sLdPr, int sizeLdPr);
};

#endif // !defined(AFX_GRPPRICE_H__F16513EA_60FA_4FAA_A9E3_BBC1BCD1703A__INCLUDED_)
