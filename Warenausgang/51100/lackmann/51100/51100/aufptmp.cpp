#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "aufptmp.h"

struct AUFPTMP aufptmp, aufptmp_null;

void AUFPTMP_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &aufptmp.auf, 2, 0);
    out_quest ((char *) &aufptmp.auf,2,0);
    out_quest ((char *) &aufptmp.a,3,0);
    out_quest ((char *) &aufptmp.auf_me,3,0);
    out_quest ((char *) aufptmp.auf_me_bz,0,7);
    out_quest ((char *) &aufptmp.lief_me,3,0);
    out_quest ((char *) aufptmp.lief_me_bz,0,7);
    out_quest ((char *) &aufptmp.auf_vk_pr,3,0);
    out_quest ((char *) &aufptmp.auf_lad_pr,3,0);
    out_quest ((char *) &aufptmp.rab_satz,3,0);
    out_quest ((char *) &aufptmp.me_einh_kun,1,0);
    out_quest ((char *) &aufptmp.me_einh,1,0);
    out_quest ((char *) &aufptmp.me_einh_kun1,1,0);
    out_quest ((char *) &aufptmp.auf_me1,3,0);
    out_quest ((char *) &aufptmp.inh1,3,0);
    out_quest ((char *) &aufptmp.me_einh_kun2,1,0);
    out_quest ((char *) &aufptmp.auf_me2,3,0);
    out_quest ((char *) &aufptmp.inh2,3,0);
    out_quest ((char *) &aufptmp.me_einh_kun3,1,0);
    out_quest ((char *) &aufptmp.auf_me3,3,0);
    out_quest ((char *) &aufptmp.inh3,3,0);
    out_quest ((char *) &aufptmp.kun,2,0);
    out_quest ((char *) &aufptmp.teil_smt,1,0);
    out_quest ((char *) &aufptmp.a_gew,3,0);
    out_quest ((char *) &aufptmp.bsd_mitpr,3,0);
    out_quest ((char *) &aufptmp.bsd_ohnepr,3,0);
    out_quest ((char *) &aufptmp.stk_karton,3,0);
    out_quest ((char *) &aufptmp.gew_karton,3,0);
    out_quest ((char *) &aufptmp.hwg,1,0);
    out_quest ((char *) &aufptmp.wg,1,0);
    out_quest ((char *) &aufptmp.lieferdat,2,0);
            cursor = prepare_sql ("select aufptmp.auf,  "
"aufptmp.a,  aufptmp.auf_me,  aufptmp.auf_me_bz,  aufptmp.lief_me,  "
"aufptmp.lief_me_bz,  aufptmp.auf_vk_pr,  aufptmp.auf_lad_pr,  "
"aufptmp.rab_satz,  aufptmp.me_einh_kun,  aufptmp.me_einh,  "
"aufptmp.me_einh_kun1,  aufptmp.auf_me1,  aufptmp.inh1,  "
"aufptmp.me_einh_kun2,  aufptmp.auf_me2,  aufptmp.inh2,  "
"aufptmp.me_einh_kun3,  aufptmp.auf_me3,  aufptmp.inh3,  aufptmp.kun,  "
"aufptmp.teil_smt,  aufptmp.a_gew,  aufptmp.bsd_mitpr,  "
"aufptmp.bsd_ohnepr,  aufptmp.stk_karton,  aufptmp.gew_karton,  "
"aufptmp.hwg,  aufptmp.wg,  aufptmp.lieferdat from aufptmp "

#line 26 "aufptmp.rpp"
                                  "where auf = ? "
                                  "order by a");

    ins_quest ((char *) &aufptmp.auf,2,0);
    ins_quest ((char *) &aufptmp.a,3,0);
    ins_quest ((char *) &aufptmp.auf_me,3,0);
    ins_quest ((char *) aufptmp.auf_me_bz,0,7);
    ins_quest ((char *) &aufptmp.lief_me,3,0);
    ins_quest ((char *) aufptmp.lief_me_bz,0,7);
    ins_quest ((char *) &aufptmp.auf_vk_pr,3,0);
    ins_quest ((char *) &aufptmp.auf_lad_pr,3,0);
    ins_quest ((char *) &aufptmp.rab_satz,3,0);
    ins_quest ((char *) &aufptmp.me_einh_kun,1,0);
    ins_quest ((char *) &aufptmp.me_einh,1,0);
    ins_quest ((char *) &aufptmp.me_einh_kun1,1,0);
    ins_quest ((char *) &aufptmp.auf_me1,3,0);
    ins_quest ((char *) &aufptmp.inh1,3,0);
    ins_quest ((char *) &aufptmp.me_einh_kun2,1,0);
    ins_quest ((char *) &aufptmp.auf_me2,3,0);
    ins_quest ((char *) &aufptmp.inh2,3,0);
    ins_quest ((char *) &aufptmp.me_einh_kun3,1,0);
    ins_quest ((char *) &aufptmp.auf_me3,3,0);
    ins_quest ((char *) &aufptmp.inh3,3,0);
    ins_quest ((char *) &aufptmp.kun,2,0);
    ins_quest ((char *) &aufptmp.teil_smt,1,0);
    ins_quest ((char *) &aufptmp.a_gew,3,0);
    ins_quest ((char *) &aufptmp.bsd_mitpr,3,0);
    ins_quest ((char *) &aufptmp.bsd_ohnepr,3,0);
    ins_quest ((char *) &aufptmp.stk_karton,3,0);
    ins_quest ((char *) &aufptmp.gew_karton,3,0);
    ins_quest ((char *) &aufptmp.hwg,1,0);
    ins_quest ((char *) &aufptmp.wg,1,0);
    ins_quest ((char *) &aufptmp.lieferdat,2,0);
            sqltext = "update aufptmp set aufptmp.auf = ?,  "
"aufptmp.a = ?,  aufptmp.auf_me = ?,  aufptmp.auf_me_bz = ?,  "
"aufptmp.lief_me = ?,  aufptmp.lief_me_bz = ?,  "
"aufptmp.auf_vk_pr = ?,  aufptmp.auf_lad_pr = ?,  "
"aufptmp.rab_satz = ?,  aufptmp.me_einh_kun = ?,  "
"aufptmp.me_einh = ?,  aufptmp.me_einh_kun1 = ?,  "
"aufptmp.auf_me1 = ?,  aufptmp.inh1 = ?,  aufptmp.me_einh_kun2 = ?,  "
"aufptmp.auf_me2 = ?,  aufptmp.inh2 = ?,  aufptmp.me_einh_kun3 = ?,  "
"aufptmp.auf_me3 = ?,  aufptmp.inh3 = ?,  aufptmp.kun = ?,  "
"aufptmp.teil_smt = ?,  aufptmp.a_gew = ?,  aufptmp.bsd_mitpr = ?,  "
"aufptmp.bsd_ohnepr = ?,  aufptmp.stk_karton = ?,  "
"aufptmp.gew_karton = ?,  aufptmp.hwg = ?,  aufptmp.wg = ?,  "
"aufptmp.lieferdat = ? "

#line 30 "aufptmp.rpp"
                                  "where auf = ? "
                                  "and a  = ?";

            ins_quest ((char *) &aufptmp.auf, 2, 0);
            ins_quest ((char *) &aufptmp.a, 3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &aufptmp.auf, 2, 0);
            ins_quest ((char *) &aufptmp.a, 3, 0);
            test_upd_cursor = prepare_sql ("select auf from aufptmp "
                                  "where auf = ? "
                                  "and   a  = ?");

            ins_quest ((char *) &aufptmp.auf, 2, 0);
            ins_quest ((char *) &aufptmp.a, 3, 0);
            del_cursor = prepare_sql ("delete from aufptmp "
                                  "where auf = ? "
                                  "and   a  = ?");

            ins_quest ((char *) &aufptmp.auf, 2, 0);
            del_cursor_aufptmp = prepare_sql ("delete from aufptmp "
                                  "where  auf = ? ");

    ins_quest ((char *) &aufptmp.auf,2,0);
    ins_quest ((char *) &aufptmp.a,3,0);
    ins_quest ((char *) &aufptmp.auf_me,3,0);
    ins_quest ((char *) aufptmp.auf_me_bz,0,7);
    ins_quest ((char *) &aufptmp.lief_me,3,0);
    ins_quest ((char *) aufptmp.lief_me_bz,0,7);
    ins_quest ((char *) &aufptmp.auf_vk_pr,3,0);
    ins_quest ((char *) &aufptmp.auf_lad_pr,3,0);
    ins_quest ((char *) &aufptmp.rab_satz,3,0);
    ins_quest ((char *) &aufptmp.me_einh_kun,1,0);
    ins_quest ((char *) &aufptmp.me_einh,1,0);
    ins_quest ((char *) &aufptmp.me_einh_kun1,1,0);
    ins_quest ((char *) &aufptmp.auf_me1,3,0);
    ins_quest ((char *) &aufptmp.inh1,3,0);
    ins_quest ((char *) &aufptmp.me_einh_kun2,1,0);
    ins_quest ((char *) &aufptmp.auf_me2,3,0);
    ins_quest ((char *) &aufptmp.inh2,3,0);
    ins_quest ((char *) &aufptmp.me_einh_kun3,1,0);
    ins_quest ((char *) &aufptmp.auf_me3,3,0);
    ins_quest ((char *) &aufptmp.inh3,3,0);
    ins_quest ((char *) &aufptmp.kun,2,0);
    ins_quest ((char *) &aufptmp.teil_smt,1,0);
    ins_quest ((char *) &aufptmp.a_gew,3,0);
    ins_quest ((char *) &aufptmp.bsd_mitpr,3,0);
    ins_quest ((char *) &aufptmp.bsd_ohnepr,3,0);
    ins_quest ((char *) &aufptmp.stk_karton,3,0);
    ins_quest ((char *) &aufptmp.gew_karton,3,0);
    ins_quest ((char *) &aufptmp.hwg,1,0);
    ins_quest ((char *) &aufptmp.wg,1,0);
    ins_quest ((char *) &aufptmp.lieferdat,2,0);
            ins_cursor = prepare_sql ("insert into aufptmp ("
"auf,  a,  auf_me,  auf_me_bz,  lief_me,  lief_me_bz,  auf_vk_pr,  auf_lad_pr,  "
"rab_satz,  me_einh_kun,  me_einh,  me_einh_kun1,  auf_me1,  inh1,  "
"me_einh_kun2,  auf_me2,  inh2,  me_einh_kun3,  auf_me3,  inh3,  kun,  teil_smt,  "
"a_gew,  bsd_mitpr,  bsd_ohnepr,  stk_karton,  gew_karton,  hwg,  wg,  lieferdat) "

#line 54 "aufptmp.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 56 "aufptmp.rpp"
}
int AUFPTMP_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int AUFPTMP_CLASS::delete_aufptmp (int auf)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         extern short sql_mode;

	int sav_sql_mode = sql_mode;
	sql_mode = 2;
         if (del_cursor_aufptmp == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_aufptmp);
		 sql_mode = sav_sql_mode;

         return (sqlstatus);
}

