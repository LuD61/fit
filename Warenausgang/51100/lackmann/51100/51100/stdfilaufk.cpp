#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "stdfilaufk.h"

struct STDFILAUFK stdfilaufk, stdfilaufk_null;

/*
create index "fit".i01stdfilaufk on "fit".stdfilaufk (lfd,wo_tag,
    fil,mdn);
*/

void STDFILAUFK_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &stdfilaufk.mdn, 1, 0);
            ins_quest ((char *)   &stdfilaufk.fil, 1, 0);
            ins_quest ((char *)   &stdfilaufk.wo_tag,  0, 3);
            ins_quest ((char *)   &stdfilaufk.lfd,  2, 0);
    out_quest ((char *) &stdfilaufk.mdn,1,0);
    out_quest ((char *) &stdfilaufk.fil,1,0);
    out_quest ((char *) stdfilaufk.wo_tag,0,3);
    out_quest ((char *) &stdfilaufk.lfd,2,0);
    out_quest ((char *) stdfilaufk.bez,0,17);
    out_quest ((char *) &stdfilaufk.delstatus,1,0);
            cursor = prepare_sql ("select stdfilaufk.mdn,  "
"stdfilaufk.fil,  stdfilaufk.wo_tag,  stdfilaufk.lfd,  stdfilaufk.bez,  "
"stdfilaufk.delstatus from stdfilaufk "

#line 28 "stdfilaufk.rpp"
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   wo_tag = ? "
                                  "and lfd = ?");
    ins_quest ((char *) &stdfilaufk.mdn,1,0);
    ins_quest ((char *) &stdfilaufk.fil,1,0);
    ins_quest ((char *) stdfilaufk.wo_tag,0,3);
    ins_quest ((char *) &stdfilaufk.lfd,2,0);
    ins_quest ((char *) stdfilaufk.bez,0,17);
    ins_quest ((char *) &stdfilaufk.delstatus,1,0);
            sqltext = "update stdfilaufk set "
"stdfilaufk.mdn = ?,  stdfilaufk.fil = ?,  stdfilaufk.wo_tag = ?,  "
"stdfilaufk.lfd = ?,  stdfilaufk.bez = ?,  stdfilaufk.delstatus = ? "

#line 33 "stdfilaufk.rpp"
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   wo_tag = ? "
                                  "and lfd = ?";
            ins_quest ((char *)   &stdfilaufk.mdn, 1, 0);
            ins_quest ((char *)   &stdfilaufk.fil, 1, 0);
            ins_quest ((char *)   &stdfilaufk.wo_tag,  0, 3);
            ins_quest ((char *)   &stdfilaufk.lfd,  2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *)   &stdfilaufk.mdn, 1, 0);
            ins_quest ((char *)   &stdfilaufk.fil, 1, 0);
            ins_quest ((char *)   &stdfilaufk.wo_tag,  0, 3);
            ins_quest ((char *)   &stdfilaufk.lfd,  2, 0);
            test_upd_cursor = prepare_sql ("select wo_tag from stdfilaufk "
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   wo_tag = ? "
                                  "and lfd = ? "
                                  "for update");

            ins_quest ((char *)   &stdfilaufk.mdn, 1, 0);
            ins_quest ((char *)   &stdfilaufk.fil, 1, 0);
            ins_quest ((char *)   &stdfilaufk.wo_tag,  0, 3);
            ins_quest ((char *)   &stdfilaufk.lfd,  2, 0);
            del_cursor = prepare_sql ("delete from stdfilaufk "
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   wo_tag = ? "
                                  "and lfd = ?");
    ins_quest ((char *) &stdfilaufk.mdn,1,0);
    ins_quest ((char *) &stdfilaufk.fil,1,0);
    ins_quest ((char *) stdfilaufk.wo_tag,0,3);
    ins_quest ((char *) &stdfilaufk.lfd,2,0);
    ins_quest ((char *) stdfilaufk.bez,0,17);
    ins_quest ((char *) &stdfilaufk.delstatus,1,0);
            ins_cursor = prepare_sql ("insert into stdfilaufk ("
"mdn,  fil,  wo_tag,  lfd,  bez,  delstatus) "

#line 64 "stdfilaufk.rpp"
                                      "values "
                                      "(?,?,?,?,?,?)"); 

#line 66 "stdfilaufk.rpp"
}
