#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "hwg.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "hwg_best_wk.h"

struct HWG_BEST_WK hwg_best_wk, hwg_best_wk_null;

void HWG_BEST_WK_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &hwg_best_wk.hwg, 1, 0);
            ins_quest ((char *) &hwg_best_wk.profil, 1, 0);
    out_quest ((char *) &hwg_best_wk.hwg,1,0);
    out_quest ((char *) &hwg_best_wk.prozent,3,0);
    out_quest ((char *) &hwg_best_wk.qm,3,0);
    out_quest ((char *) &hwg_best_wk.profil,1,0);
    out_quest ((char *) &hwg_best_wk.gueltig_von,2,0);
    out_quest ((char *) &hwg_best_wk.gueltig_bis,2,0);
    out_quest ((char *) &hwg_best_wk.preis,3,0);
    out_quest ((char *) &hwg_best_wk.liqui,3,0);
    out_quest ((char *) &hwg_best_wk.ges_sortiment,3,0);
            cursor = prepare_sql ("select hwg_best_wk.hwg,  "
"hwg_best_wk.prozent,  hwg_best_wk.qm,  hwg_best_wk.profil,  "
"hwg_best_wk.gueltig_von,  hwg_best_wk.gueltig_bis,  "
"hwg_best_wk.preis,  hwg_best_wk.liqui,  hwg_best_wk.ges_sortiment from hwg_best_wk "

#line 27 "hwg_best_wk.rpp"
                                  "where hwg = ? and profil = ? ");

            ins_quest ((char *) &hwg_best_wk.profil, 1, 0);
			// LAC-194  mit Tab hwg ohne Einschränkung qm und prozent
    out_quest ((char *) &hwg.hwg,1,0);
    out_quest ((char *) hwg.hwg_bz1,0,25);
    out_quest ((char *) hwg.hwg_bz2,0,25);
    out_quest ((char *) hwg.sp_kz,0,2);
    out_quest ((char *) &hwg.we_kto,2,0);
    out_quest ((char *) &hwg.erl_kto,2,0);
    out_quest ((char *) hwg.durch_vk,0,2);
    out_quest ((char *) hwg.durch_sk,0,2);
    out_quest ((char *) hwg.durch_fil,0,2);
    out_quest ((char *) hwg.durch_sw,0,2);
    out_quest ((char *) hwg.best_auto,0,2);
    out_quest ((char *) &hwg.sp_vk,3,0);
    out_quest ((char *) &hwg.sp_sk,3,0);
    out_quest ((char *) &hwg.sp_fil,3,0);
    out_quest ((char *) &hwg.sw,3,0);
    out_quest ((char *) &hwg.me_einh,1,0);
    out_quest ((char *) &hwg.mwst,1,0);
    out_quest ((char *) &hwg.teil_smt,1,0);
    out_quest ((char *) &hwg.bearb_lad,1,0);
    out_quest ((char *) &hwg.bearb_fil,1,0);
    out_quest ((char *) &hwg.bearb_sk,1,0);
    out_quest ((char *) hwg.sam_ean,0,2);
    out_quest ((char *) hwg.smt,0,2);
    out_quest ((char *) hwg.kost_kz,0,3);
    out_quest ((char *) &hwg.pers_rab,3,0);
    out_quest ((char *) &hwg.akv,2,0);
    out_quest ((char *) &hwg.bearb,2,0);
    out_quest ((char *) hwg.pers_nam,0,9);
    out_quest ((char *) &hwg.delstatus,1,0);
    out_quest ((char *) &hwg.erl_kto_1,2,0);
    out_quest ((char *) &hwg.erl_kto_2,2,0);
    out_quest ((char *) &hwg.erl_kto_3,2,0);
    out_quest ((char *) &hwg.we_kto_1,2,0);
    out_quest ((char *) &hwg.we_kto_2,2,0);
    out_quest ((char *) &hwg.we_kto_3,2,0);
            cursor_all = prepare_sql ("select hwg.hwg,  "
"hwg.hwg_bz1,  hwg.hwg_bz2,  hwg.sp_kz,  hwg.we_kto,  hwg.erl_kto,  "
"hwg.durch_vk,  hwg.durch_sk,  hwg.durch_fil,  hwg.durch_sw,  "
"hwg.best_auto,  hwg.sp_vk,  hwg.sp_sk,  hwg.sp_fil,  hwg.sw,  hwg.me_einh,  "
"hwg.mwst,  hwg.teil_smt,  hwg.bearb_lad,  hwg.bearb_fil,  hwg.bearb_sk,  "
"hwg.sam_ean,  hwg.smt,  hwg.kost_kz,  hwg.pers_rab,  hwg.akv,  hwg.bearb,  "
"hwg.pers_nam,  hwg.delstatus,  hwg.erl_kto_1,  hwg.erl_kto_2,  "
"hwg.erl_kto_3,  hwg.we_kto_1,  hwg.we_kto_2,  hwg.we_kto_3 "

#line 32 "hwg_best_wk.rpp"
				", hwg_best_wk.hwg,  hwg_best_wk.prozent,  hwg_best_wk.qm,  "
				"hwg_best_wk.profil,  hwg_best_wk.gueltig_von,  "
				"hwg_best_wk.gueltig_bis,  hwg_best_wk.preis,  hwg_best_wk.liqui,  "
				"hwg_best_wk.ges_sortiment "
				 "from hwg_best_wk,hwg  "
                                  "where hwg_best_wk.profil = ? and hwg.hwg = hwg_best_wk.hwg  order by hwg.hwg ");
                                  
    ins_quest ((char *) &hwg_best_wk.hwg,1,0);
    ins_quest ((char *) &hwg_best_wk.prozent,3,0);
    ins_quest ((char *) &hwg_best_wk.qm,3,0);
    ins_quest ((char *) &hwg_best_wk.profil,1,0);
    ins_quest ((char *) &hwg_best_wk.gueltig_von,2,0);
    ins_quest ((char *) &hwg_best_wk.gueltig_bis,2,0);
    ins_quest ((char *) &hwg_best_wk.preis,3,0);
    ins_quest ((char *) &hwg_best_wk.liqui,3,0);
    ins_quest ((char *) &hwg_best_wk.ges_sortiment,3,0);
            sqltext = "update hwg_best_wk set "
"hwg_best_wk.hwg = ?,  hwg_best_wk.prozent = ?,  hwg_best_wk.qm = ?,  "
"hwg_best_wk.profil = ?,  hwg_best_wk.gueltig_von = ?,  "
"hwg_best_wk.gueltig_bis = ?,  hwg_best_wk.preis = ?,  "
"hwg_best_wk.liqui = ?,  hwg_best_wk.ges_sortiment = ? "

#line 40 "hwg_best_wk.rpp"
                                  "where hwg = ? and profil = ?  ";

            ins_quest ((char *) &hwg_best_wk.hwg, 1, 0);
            ins_quest ((char *) &hwg_best_wk.profil, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &hwg_best_wk.hwg, 1, 0);
            test_upd_cursor = prepare_sql ("select hwg from hwg_best_wk "
                                  "where hwg = ? and profil = ?  ");

            ins_quest ((char *) &hwg_best_wk.hwg, 1, 0);
            ins_quest ((char *) &hwg_best_wk.profil, 1, 0);
            del_cursor = prepare_sql ("delete from hwg_best_wk "
                                  "where hwg = ? and profil = ?  ");


    ins_quest ((char *) &hwg_best_wk.hwg,1,0);
    ins_quest ((char *) &hwg_best_wk.prozent,3,0);
    ins_quest ((char *) &hwg_best_wk.qm,3,0);
    ins_quest ((char *) &hwg_best_wk.profil,1,0);
    ins_quest ((char *) &hwg_best_wk.gueltig_von,2,0);
    ins_quest ((char *) &hwg_best_wk.gueltig_bis,2,0);
    ins_quest ((char *) &hwg_best_wk.preis,3,0);
    ins_quest ((char *) &hwg_best_wk.liqui,3,0);
    ins_quest ((char *) &hwg_best_wk.ges_sortiment,3,0);
            ins_cursor = prepare_sql ("insert into hwg_best_wk ("
"hwg,  prozent,  qm,  profil,  gueltig_von,  gueltig_bis,  preis,  liqui,  "
"ges_sortiment) "

#line 57 "hwg_best_wk.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?)"); 

#line 59 "hwg_best_wk.rpp"
}
int HWG_BEST_WK_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int HWG_BEST_WK_CLASS::dbreadfirst_all (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_all == -1)
         {
                this->prepare ();
         }
         open_sql (cursor_all);
         fetch_sql (cursor_all);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int HWG_BEST_WK_CLASS::dbread_all (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         fetch_sql (cursor_all);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}
