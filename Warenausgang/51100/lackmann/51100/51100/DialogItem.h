// DialogItem.h: Schnittstelle f�r die Klasse CDialogItem.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIALOGITEM_H__AC0C54CC_20F5_4240_A9C3_F74DD50169BC__INCLUDED_)
#define AFX_DIALOGITEM_H__AC0C54CC_20F5_4240_A9C3_F74DD50169BC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "windows.h"

class CDialogItem  
{
public:
	enum REF_TYPE
	{
		String ,
        Short,
		Long,
		Double,
		Date,
		Char,
	};

	HWND m_Parent;
	HWND m_hWnd;
	DWORD m_Id;
	void *m_Ref;
	REF_TYPE m_RefType; 
	int m_Length;
	char m_Format[10];
	
    operator HWND ();
	CDialogItem(HWND Parent);
	CDialogItem(HWND Parent, DWORD Id, void *m_Ref, REF_TYPE RefType=String, int m_Length=0, char *format=NULL);
	virtual ~CDialogItem();
	void PutValue ();
	void GetValue ();
	BOOL IsControl (HWND hWnd);
	virtual void SetDateValue (char *buffer);
	virtual void GetDateValue (char *buffer);
};

#endif // !defined(AFX_DIALOGITEM_H__AC0C54CC_20F5_4240_A9C3_F74DD50169BC__INCLUDED_)
