#ifndef _KRITMHD
#define _KRITMHD
#include "dbclass.h"
#include "mo_aufl.h"
#include "cron_best.h"


class KRITMHD_CLASS : public DB_CLASS 
{
       private :
               int prep_cursor (void);
			   int KritischeMHD;
			   BOOL ModusBestand;
			   BOOL ModusKritMHD;

       public :
               KRITMHD_CLASS () : DB_CLASS ()
               {
               }
               int ShowKritMHD (HWND, int, double a, short wg, int KritischeMHD, BOOL Zeige, BOOL ZeigeBestand); 
			   int CloseWindow (HWND);

};
#endif
