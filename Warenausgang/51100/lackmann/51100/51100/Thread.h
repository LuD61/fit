// Thread.h: Schnittstelle f�r die Klasse CThread.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_THREAD_H__203F7CED_ADA2_4639_855D_A1C60360801A__INCLUDED_)
#define AFX_THREAD_H__203F7CED_ADA2_4639_855D_A1C60360801A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <Windows.h>
#include "Mutex.h"

#define THREADEX_MAX_RETRY					30L
#define THREADEX_DELAY_MSEC					10000L
#define THREAD_TERMINATE_AND_WAIT_TIMEOUT	30000L

class CThread  
{
public:
	enum Status { Created, Running, Suspended, Finished, Invalid, Starting, Started };

private:
	LPSTR Name;
	HANDLE ThreadHandle;
	
	unsigned long ThreadId; // thread address -> return value of _beginthreadex
	int TerminationRequested;	
	Status	enumCheckStatus();	
	Status  enumThreadStatus;
	

	virtual unsigned int Run() = 0;
	static unsigned long _stdcall Execute( void *thread );

	unsigned long	Suspend();
	unsigned long	Resume();
	
	int				SetPriority( int pri );
	void SetTerminationRequest(int TerminationRequest);

protected:
	CMutex* m_Mutex;

public:
	CThread();
	virtual ~CThread();

	HANDLE GetThreadHandle ()
	{
		return ThreadHandle;
	}

	void			Kill();
	bool SetName (LPSTR Name);
	bool Start(LPSTR Name);
	Status enumGetStatus();
	void enumSetStatus (Status status);
	unsigned int GetThreadID();
};

#endif // !defined(AFX_THREAD_H__203F7CED_ADA2_4639_855D_A1C60360801A__INCLUDED_)

