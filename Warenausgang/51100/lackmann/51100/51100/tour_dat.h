#ifndef _TOUR_DAT_DEF
#define _TOUR_DAT_DEF

#include "dbclass.h"

struct TOUR_DAT {
   long      tou;
   long      lieferdat;
   short     posi;
   double    palette;
   double    soll_volumen;
   double    ist_volumen;
};
extern struct TOUR_DAT tour_dat, tour_dat_null;

#line 7 "tour_dat.rh"

class TOUR_DAT_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               TOUR_DAT_CLASS () : DB_CLASS ()
               {
               }
};
#endif
