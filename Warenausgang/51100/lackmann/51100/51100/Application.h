// Application.h: Schnittstelle f�r die Klasse CApplication.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_APPLICATION_H__5A7C9020_B746_4AF0_98ED_540E4F9A1ED4__INCLUDED_)
#define AFX_APPLICATION_H__5A7C9020_B746_4AF0_98ED_540E4F9A1ED4__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CApplication  
{
private:
	static CApplication *Instance;
	HINSTANCE appInstance;
protected:
	CApplication();
public:
	virtual ~CApplication();

	void SetAppInstance (HINSTANCE appInstance)
	{
		this->appInstance = appInstance;
	}

	HINSTANCE GetAppInstance ()
	{
		return appInstance;
	}

	static CApplication *GetInstance ();
	static void Destroy ();



};

#endif // !defined(AFX_APPLICATION_H__5A7C9020_B746_4AF0_98ED_540E4F9A1ED4__INCLUDED_)
