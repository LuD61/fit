// CalcualteSmt.cpp: Implementierung der Klasse CCalcualteSmt.
//
//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "CalcualteSmt.h"
#include "strfkt.h"
#include "Text.h"
#include "ListHandler.h"
#include  "hwg.h"
#include  "hwgchart.h" //LAC-96
#include  "DbClass.h" //LAC-109
#include  "mo_curso.h" //LAC-109
#include  "aufchart.h" //LAC-109
#include  "datum.h" //LAC-109
#include  "hwg_best_wk.h" //LAC-96
#include  "kun.h" //LAC-96

#define CString Text
#define LISTHANDLER CListHandler::GetInstance ()

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

// List-Klasse f�r Sortimentwerte pro Position sortiert

HWG_CL Hwg ; 
extern HWGCHART_CLASS HwgChart ;  //LAC-96
HWG_BEST_WK_CLASS HwgBestWk ;  //LAC-96
extern AUFCHART_CLASS AufChart ;  //LAC-96

static DB_CLASS DbClass;

CSmtOrderItem::CSmtOrderItem ()
{
	m_A     = 0.0;
	m_Smt   = 0;
	m_Me    = 0.0;
	m_Gew   = 0.0;
	m_Vk    = 0.0;
	m_A_gew = 0.0;
}

CSmtOrderList::CSmtOrderList ()
{
}
	
CSmtOrderList::~CSmtOrderList ()
{
	DestroyElements ();
	Destroy ();
}

void CSmtOrderList::DestroyElements ()
{
	CSmtOrderItem *element;

	if (Arr != NULL)
	{
		for (int i = 0; i < anz ; i ++)
		{
			element = *Get (i);
			delete element;
			Arr[i] = NULL;
		}
	}
	anz = 0;
	pos = 0;
}


double CSmtOrderList::GetOrderAmount (short smt)
{
	double value = 0;
	int i = 0;
	CSmtOrderItem * Item;

	for (i = 0; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () == smt)
		{
			break;
		}
	}

	for (; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () != smt)
		{
			break;
		}
		value += Item->Vk () * Item->Me ();
	}

	return value;
}



double CSmtOrderList::GetOrderWeight (short smt)
{
	double value = 0.0;
	int i = 0;
	CSmtOrderItem * Item;

	for (i = 0; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () == smt)
		{
			break;
		}
	}

	for (; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () != smt)
		{
			break;
		}
		value += Item->Gew ();
	}

	return value;
}

double CSmtOrderList::GetLiq (short smt)
{
	double value = 0;
    double amount = GetOrderAmount (smt); 
    double weight = GetOrderWeight (smt);   
	if (weight != 0.0)
	{
		value = amount / weight;
	}
	return value;
}

int CSmtOrderList::GetStndAllElements (short smt)
{
	int i;
	CDataCollection<AUFPS *> *StndAll = LISTHANDLER->GetStndAll ();
	AUFPS *aufps;
	int SmtAllItems = 0;

	/** LAC-111
	for (i = 0; i < StndAll->anz; i ++)
	{
		aufps = *StndAll->Get (i);
//		if (atoi (aufps->teil_smt) == (int) smt) 
		if (aufps->hwg == (int) smt)   //LAC-111 teil_smt -> hwg
		{
			break;
		}
	}
	**/

	for (i = 0; i < StndAll->anz; i ++)
	{
		aufps = *StndAll->Get (i);
//		if (atoi (aufps->teil_smt) == (int) smt) 
		if (aufps->hwg == (int) smt) //LAC-111 teil_smt -> hwg
		{
			SmtAllItems ++;
		}
	}
	return SmtAllItems;
}


int CSmtOrderList::GetSmtAnzahl (short smt)
{
	int SmtValueItems = 0;

	int i = 0;
	CSmtOrderItem * Item;

	for (i = 0; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () == smt)
		{
			if (Item->Me () != 0.0)
			{
    			SmtValueItems ++;
			}
		}
	}
	return SmtValueItems;
}



double CSmtOrderList::GetSmtQuot (short smt)
{
	double value = 0;
	int SmtAllItems = 0;
	int SmtValueItems = 0;

	int i = 0;
	CSmtOrderItem * Item;

	/** 260312
	for (i = 0; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () == smt)
		{
			break;
		}
	}
	**/

	for (; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () != smt)
		{
	//		break;
    		SmtAllItems ++; //LAC-111 Anteil am Sortiment
			continue ; //260312
		}
		SmtAllItems ++;
		if (Item->Me () != 0.0)
		{
			SmtValueItems ++;
		}
	}

	if (SmtAllItems != 0)
	{
		value = (double) SmtValueItems * 100 / SmtAllItems;
	}

	/** LAC-111 
	int allElements = GetStndAllElements (smt);
	if (allElements  != 0)
	{
		value = (double) SmtValueItems * 100 / allElements;
	}
	**/

	return value;
}


double CSmtOrderList::GetSmtQuot ()
{
	double value = 0;
	int SmtAllItems = 0;
	int SmtValueItems = 0;

	int i = 0;
	CSmtOrderItem * Item;


	for (; i < anz; i ++)
	{
		Item = *Get (i);
		SmtAllItems ++;
		if (Item->Me () != 0.0)
		{
			SmtValueItems ++;
		}
	}

	if (SmtAllItems != 0.0)
	{
		value = (double) SmtValueItems * 100 / SmtAllItems;
	}

	/** LAC-111
	if (LISTHANDLER->GetStndAll ()->anz > 0)
	{
		value = (double) SmtValueItems * 100 / LISTHANDLER->GetStndAll ()->anz;
	}
	**/

	return value;
}


// Vergleichs-Klasse f�r Sortimentwerte.  

int CCompareOrderInterface::Compare (CSmtOrderItem *Item1, CSmtOrderItem *Item2)
{
	return Item1->Smt () - Item2->Smt ();
}

// List-Klasse f�r Sortimente eindeutig, sortiert

CSmtList::CSmtList ()
{
	set_Unique (TRUE);
}
	
CSmtList::~CSmtList ()
{
	Destroy ();
}

// Vergleichs-Klasse f�r Sortimente.  

int CCompareSmtInterface::Compare (short Item1, short Item2)
{
	return Item1 - Item2;
}

CCalcualteSmt::CCalcualteSmt()
{
	m_SmtOrderList.set_CompareInterface (&Compare);
	m_SmtList.set_CompareInterface (&SmtCompare);
	m_Controls = NULL;
	m_hWnd = NULL;
}

CCalcualteSmt::~CCalcualteSmt()
{
	m_SmtOrderList.DestroyElements ();
	m_SmtOrderList.Destroy ();
}

short CCalcualteSmt::Calculate (AUFPS *aufps, int size, int cfg_AnzTageSollwerte, int mench )
{
	int i = 0;
	double sumAmount = 0.0;
	double sumWeight = 0.0;
	double sumLiq = 0.0;
	double sumSmtPart = 0.0;
    int dsqlstat_WK = 0;
	int anz_cursor = -1;
	short smt      = 0;
	double amount  = 0.0;
	double weight  = 0.0;
	double liq     = 0.0;
	double smtquot = 0.0;
	short anzhwg = 0;

        if (getenv ("testmode")) //LAC-194test
		{
			if (atoi (getenv ("testmode")) == 1)  
			{
						disp_mess ("Calculate Anfang", 2);
			}
		}
	m_SmtOrderList.DestroyElements ();
	m_SmtList.Destroy ();
	for (i = 0; i < size; i ++)
	{
	  if (ratod (aufps[i].lief_me) != 0.0)
	  { 
		if (!IsDiscountArticle (ratod (aufps[i].a), aufps[i].smt))
		{
			CSmtOrderItem * Item = new CSmtOrderItem ();
			Item->set_A (ratod (aufps[i].a));
//			Item->set_Smt ((short) atoi (aufps[i].teil_smt));  //todo hier statt teil_smt hwg //140312 
			Item->set_Smt (aufps[i].hwg);  //140312 
			Item->set_Me (ratod (aufps[i].lief_me));
			Item->set_Vk (ratod (aufps[i].auf_vk_pr));
			Item->set_A_gew (aufps[i].a_gew);
			if (atoi (aufps[i].me_einh) != 2) 
			{
				Item->set_Gew (Item->Me () * Item->A_gew ());  
			}
			else
			{
				Item->set_Gew (Item->Me ());
			}
			m_SmtOrderList.Add (Item);
			m_SmtList.Add (Item->Smt ());
		}
	  }
	}

	hwgchart.auf = aufk.auf; //LAC-96
	if (mench == 0) HwgChart.del_istwerte(); //LAC-96
	if (m_Controls != NULL)
	{
	  CSmtTable * smtTable = m_Controls->SmtTable ();
	  smtTable->Clear ();
	  hwg_best_wk.profil = kun_erw.best_profil;  //testtest
	  hwg.hwg = 0;
	  int dsqlstat_WK = HwgBestWk.dbreadfirst_all ();
	  int i;
	  while (dsqlstat_WK == 0)
	  {
			if (hwg_best_wk.hwg == 0 || hwg_best_wk.prozent == 0.0 || hwg_best_wk.qm == 0.0) 
			{
				for (i = 0; i < m_SmtList.anz; i ++)              //  Schleife �ber die einzelnen HWGs 
				{
					smt      = *m_SmtList.Get (i);
					if (smt == hwg.hwg)
					{
	  					amount  = m_SmtOrderList.GetOrderAmount (smt); 
						weight  = m_SmtOrderList.GetOrderWeight (smt); 
						liq     = m_SmtOrderList.GetLiq (smt); 
						smtquot = m_SmtOrderList.GetSmtQuot (smt);
			
						if (amount != 0.0 || weight != 0.0)
						{
							sumAmount += amount;
							sumWeight += weight;
							sumLiq += liq;
							sumSmtPart += smtquot;
							CString cSmt;
							cSmt.Format ("%d", smt);
							CSmtValues *smtValues = new CSmtValues ();
							hwg.hwg = atoi(cSmt.GetBuffer()); //140312
							//LAC-194 ist schon gelesen				if (Hwg.dbreadfirst() == 0) //140312
							if (hwg.hwg > 0)
							{
								smtValues->set_Name (CString::TrimRight (hwg.hwg_bz1));
							}
							else
							{
								smtValues->set_Name ("");
							}
							smtValues->set_Amount (amount);
							smtValues->set_Weight (weight);
							smtValues->set_Liq (liq);
							smtValues->set_SmtPart (smtquot);
							smtTable->Add (smtValues);
							delete smtValues;
						}
					}
				}

				dsqlstat_WK = HwgBestWk.dbread_all ();
				continue;
			}




      //LAC-96 A Hier Tabelle HWGChart f�llen 
		memcpy (&hwgchart, &hwgchart_null, sizeof (struct HWGCHART));
		hwgchart.auf = aufk.auf;
		hwgchart.hwg = hwg_best_wk.hwg;
		if ( HwgChart.dbreadfirst () == 100)
		{
			// hwgchart.hwg_anz  wird jetzt in der HWG_CLASS gef�llt

			  /**
			  anz_cursor = DbClass.sqlcursor  ("select count(*) from stnd_aufp,a_bas where a_bas.hwg = ? and a_bas.delstatus = 0  and a_bas.ghsperre = 0 "
						" and stnd_aufp.kun = 0 and stnd_aufp.kun_fil = 2 and stnd_aufp.fil = 0 and  stnd_aufp.mdn = 1 "
						" and stnd_aufp.a = a_bas.a ");
              DbClass.sqlopen (anz_cursor);
              DbClass.sqlfetch (anz_cursor); 
              DbClass.sqlclose (anz_cursor); 
			  **/

		}
		for (i = 0; i < m_SmtList.anz; i ++)              //  Schleife �ber die einzelnen HWGs 
		{
			smt      = *m_SmtList.Get (i);
			if (smt == hwg_best_wk.hwg)
			{
			  amount  = m_SmtOrderList.GetOrderAmount (smt); 
			  weight  = m_SmtOrderList.GetOrderWeight (smt); 
			  liq     = m_SmtOrderList.GetLiq (smt); 
			  smtquot = m_SmtOrderList.GetSmtQuot (smt);
	
				hwgchart.ist_kg = weight;
				hwgchart.ist_eur = amount; 
				int SmtAnz = m_SmtOrderList.GetSmtAnzahl (smt);
				hwgchart.ist_anz = SmtAnz; 
				if (hwgchart.hwg_anz > 0)
				{
					hwgchart.ist_proz = (double) SmtAnz * 100 / hwgchart.hwg_anz;          //Anteil am HWG-Sortiment (z.b. in der HWG sind 100 Artikel, bestellt wurden 65   proz = 65 )
				} else hwgchart.ist_proz = 0.0;

				if (weight != 0.0)
				{
					hwgchart.ist_eurkg = amount / weight;
				} else hwgchart.ist_eurkg = 0.0;



//				HwgChart.dbupdate ();
			//LAC-96 E Hier Tabelle HWGChart f�llen 
			if (amount != 0.0 || weight != 0.0)
			{
				sumAmount += amount;
				sumWeight += weight;
				sumLiq += liq;
				sumSmtPart += smtquot;
				CString cSmt;
				cSmt.Format ("%d", smt);
				CSmtValues *smtValues = new CSmtValues ();
				memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));

				hwg.hwg = atoi(cSmt.GetBuffer()); 
				//LAC-194 ist schon gelesen				if (Hwg.dbreadfirst() == 0) //140312
				if (hwg.hwg > 0) 
				{
					smtValues->set_Name (CString::TrimRight (hwg.hwg_bz1));
				}
				else
				{
					smtValues->set_Name ("");
				}
				smtValues->set_Amount (amount);
				smtValues->set_Weight (weight);
				smtValues->set_Liq (liq);
				smtValues->set_SmtPart (smtquot);
				smtTable->Add (smtValues);


				delete smtValues;
			}
			}  //end of if smt == hwg_best_wk.hwg
		}
		if (aufchart.auf != aufk.auf)
		{
					memcpy (&aufchart, &aufchart_null, sizeof (struct AUFCHART));
					aufchart.auf = aufk.auf;
					AufChart.dbreadfirst ();
		}
		// =================================  IST_GES_Werte =====================
		HwgChart.hole_istgeswerte(aufk.auf); 
		hwgchart.ist_ges_kg += hwgchart.ist_kg;
		hwgchart.ist_ges_eur += hwgchart.ist_eur;
		if (hwgchart.hwg_anz > 0)
		{
			hwgchart.ist_ges_proz = hwgchart.ist_ges_anz * 100 / hwgchart.hwg_anz;          //Anteil am HWG-Sortiment (z.b. in der HWG sind 100 Artikel, bestellt wurden 65   proz = 65 )
		} else hwgchart.ist_ges_proz = 0.0;
		if (hwgchart.ist_ges_kg != 0.0)
		{
			hwgchart.ist_ges_eurkg = hwgchart.ist_ges_eur / hwgchart.ist_ges_kg;
		} else hwgchart.ist_ges_eurkg = 0.0;
		// =================================  ZIEL_Werte =====================
		if (kun_erw.m2laden > 0)
		{
			hwgchart.ziel_ges_kg = kun_erw.m2laden * hwg_best_wk.qm  * kun_erw.faktor;
			hwgchart.ziel_ges_eurkg = hwg_best_wk.preis;
			hwgchart.ziel_ges_eur = hwgchart.ziel_ges_kg * hwgchart.ziel_ges_eurkg;
			hwgchart.ziel_ges_proz = hwg_best_wk.ges_sortiment;
		}
		// =================================  SOLL_Werte =====================
		HwgChart.hole_sollwerte(aufk.auf,cfg_AnzTageSollwerte); 
    	if (hwgchart.hwg_anz > 0) hwgchart.soll_proz = hwgchart.soll_anz * 100 / hwgchart.hwg_anz;          //Anteil am HWG-Sortiment (z.b. in der HWG sind 100 Artikel, bestellt wurden 65   proz = 65 )
		if (hwgchart.soll_kg != 0.0)
		{
			hwgchart.soll_eurkg = hwgchart.soll_eur / hwgchart.soll_kg;
		} else hwgchart.soll_eurkg = 0.0;

		if (mench == 0) HwgChart.dbupdate (); 
		anzhwg ++;
	    dsqlstat_WK = HwgBestWk.dbread_all ();
	  } // end of while (dsqlstat_WK == 0)  //LAC-96

		sumSmtPart = m_SmtOrderList.GetSmtQuot ();
		if (sumWeight != 0.0)
        { 
			sumLiq = sumAmount / sumWeight;
		}
		CSmtValues *smtValues = new CSmtValues ();
		smtValues->set_Name ("Gesamt");
		smtValues->set_Amount (sumAmount);
		smtValues->set_Weight (sumWeight);
		smtValues->set_Liq (sumLiq);
		smtValues->set_SmtPart (sumSmtPart);
		smtTable->Add (smtValues);
	}
        if (getenv ("testmode")) //LAC-194test
		{
			if (atoi (getenv ("testmode")) == 1)  
			{
						disp_mess ("Calculate Ende", 2);
			}
		}
	return anzhwg;
}


void CCalcualteSmt::Calculate (int anz_tage,int anz_tage_bis, int cfg_AnzTageSollwerte)  //LAC-109  initial best�cken  von sysdate - anz_tage bis sysdate
{
//	   DB_CLASS DbClass;
	   extern LS_CLASS ls_class ;  
		extern short sql_mode;

	   int aufk_cursor; 
	   int dsql_aufp = 0;
	int size = 0;
	int i = 0;
	double sumAmount = 0.0;
	double sumWeight = 0.0;
	double sumLiq = 0.0;
	double sumSmtPart = 0.0;
         char datum [12];
         long sysdatum;
         long sysdatum_bis;


    sql_mode = 1;

	beginwork ();
     sysdate (datum);
     sysdatum = dasc_to_long (datum);
	 sysdatum = sysdatum - anz_tage;
     sysdatum_bis = dasc_to_long (datum);
	 sysdatum_bis = sysdatum_bis - anz_tage_bis;

//	 aufk.auf = 315774;
		DbClass.sqlout ((int *) &aufk.lieferdat, 2, 0);
		DbClass.sqlout ((int *) &aufk.kun, 2, 0);
		DbClass.sqlout ((int *) &aufk.auf, 2, 0);
		DbClass.sqlout ((int *) &kun_erw.m2laden, 2, 0);
		DbClass.sqlout ((int *) &aufchart.jr, 1, 0);
		DbClass.sqlout ((int *) &aufchart.kw, 1, 0);
		DbClass.sqlout ((int *) &aufchart.lieferdat, 2, 0);
//		DbClass.sqlin ((int *) &aufk.auf, 2, 0);

		DbClass.sqlin ((int *) &sysdatum, 2, 0);
		DbClass.sqlin ((int *) &sysdatum_bis, 2, 0);
	 aufk_cursor = DbClass.sqlcursor (" select aufk.lieferdat, aufk.kun,aufk.auf, kun_erw.m2laden, "
										" aufchart.jr, aufchart.kw, aufchart.lieferdat from aufk,outer kun_erw,outer aufchart where "
										" aufk.lieferdat between ? and ? and aufk.auf_stat > 0 and aufk.kun = kun_erw.kun and aufk.mdn = kun_erw.mdn and aufk.fil = kun_erw.fil and "
										" aufk.auf = aufchart.auf ");
//	 aufk_cursor = DbClass.sqlcursor (" select aufk.lieferdat, aufk.kun,aufk.auf from aufk where auf = ? and auf_stat > 0 ");
     DbClass.sqlopen (aufk_cursor);
  while (DbClass.sqlfetch (aufk_cursor) == 0)
 {
	m_SmtOrderList.DestroyElements ();
	m_SmtList.Destroy ();
	if (aufk.auf == 315666)
		{
			int di = 0;
		}
	dsql_aufp =  ls_class.lese_aufp (1,0,aufk.auf);
	while (dsql_aufp == 0)
	{
	  if (aufp.lief_me != 0.0)
	  { 
		  if (lese_a_bas (aufp.a) == 0)
		  {
			if (!IsDiscountArticle (aufp.a, _a_bas.smt))
			{
				CSmtOrderItem * Item = new CSmtOrderItem ();
				Item->set_A (aufp.a);
				Item->set_Smt (_a_bas.hwg);  //140312 
				Item->set_Me (aufp.lief_me);
				Item->set_Vk (aufp.auf_vk_pr);
				Item->set_A_gew (_a_bas.a_gew);
				if (aufp.me_einh != 2) 
				{
					Item->set_Gew (Item->Me () * Item->A_gew ());  
				}
				else
				{
					Item->set_Gew (Item->Me ());
				}
				m_SmtOrderList.Add (Item);
				m_SmtList.Add (Item->Smt ());
			}
		  }
	  }

 	  dsql_aufp =  ls_class.lese_aufp ();
	}

	hwgchart.auf = aufk.auf; 
	HwgChart.del_istwerte(); 
  hwg.hwg = 0;
  int dsqlstat_WK = HwgBestWk.dbreadfirst_all ();
  while (dsqlstat_WK == 0)
  {
	  if (hwg.hwg > 0)  //LAC-194
	  {
		  if (hwg_best_wk.prozent == 0 || hwg_best_wk.qm == 0) 
		  {
				dsqlstat_WK = HwgBestWk.dbread_all ();
				continue;
		  }

	  }
	  
	//LAC-96 A Hier Tabelle HWGChart f�llen 
	memcpy (&hwgchart, &hwgchart_null, sizeof (struct HWGCHART));
	hwgchart.auf = aufk.auf;
	hwgchart.hwg = hwg_best_wk.hwg;
	if ( HwgChart.dbreadfirst () == 100)
	{
	// hwgchart.hwg_anz  wird jetzt in der HWG_CLASS gef�llt
	/***
             DbClass.sqlin  ((short *)  &hwgchart.hwg, 1, 0);
  	          DbClass.sqlout ((long *) &hwgchart.hwg_anz, 2, 0);
			  DbClass.sqlcomm ("select count(*) from stnd_aufp,a_bas where a_bas.hwg = ? and a_bas.delstatus = 0  and a_bas.ghsperre = 0 "
					" and stnd_aufp.kun = 0 and stnd_aufp.kun_fil = 2 and stnd_aufp.fil = 0 and  stnd_aufp.mdn = 1 "
					" and stnd_aufp.a = a_bas.a ");
					**/
	}
	// =================================  ZIEL_Werte =====================
	if (kun_erw.m2laden > 0)
	{
			hwgchart.ziel_ges_kg = kun_erw.m2laden * hwg_best_wk.qm ;
			hwgchart.ziel_ges_eurkg = hwg_best_wk.preis;
			hwgchart.ziel_ges_eur = hwgchart.ziel_ges_kg * hwgchart.ziel_ges_eurkg;
			hwgchart.ziel_ges_proz = hwg_best_wk.ges_sortiment;
	}
	for (int i = 0; i < m_SmtList.anz; i ++)              //  Schleife �ber die einzelnen HWGs 
	{
		short smt      = *m_SmtList.Get (i);
	   if (smt == hwg_best_wk.hwg)
	   {
		double amount  = m_SmtOrderList.GetOrderAmount (smt); 
		double weight  = m_SmtOrderList.GetOrderWeight (smt); 
		double liq     = m_SmtOrderList.GetLiq (smt); 
		double smtquot = m_SmtOrderList.GetSmtQuot (smt);
		//LAC-96 A Hier Tabelle HWGChart f�llen 
		memcpy (&hwgchart, &hwgchart_null, sizeof (struct HWGCHART));
		hwgchart.auf = aufk.auf;
		hwgchart.hwg = smt;
		if ( HwgChart.dbreadfirst () == 100)
		{
			// hwgchart.hwg_anz  wird jetzt in der HWG_CLASS gef�llt
		}
			hwgchart.ist_kg = weight;
		hwgchart.ist_eur = amount; 
		int SmtAnz = m_SmtOrderList.GetSmtAnzahl (smt);
		hwgchart.ist_anz = SmtAnz; 
		if (hwgchart.hwg_anz > 0)
		{
			hwgchart.ist_proz = (double) SmtAnz * 100 / hwgchart.hwg_anz;          //Anteil am HWG-Sortiment (z.b. in der HWG sind 100 Artikel, bestellt wurden 65   proz = 65 )
		} else hwgchart.ist_proz = 0.0;
			if (weight != 0.0)
		{
			hwgchart.ist_eurkg = amount / weight;
		} else hwgchart.ist_eurkg = 0.0;
		if (aufchart.auf != aufk.auf)
		{
					memcpy (&aufchart, &aufchart_null, sizeof (struct AUFCHART));
					aufchart.auf = aufk.auf;
					AufChart.dbreadfirst ();
		}
		HwgChart.hole_istgeswerte(aufk.auf); 
		hwgchart.ist_ges_kg += hwgchart.ist_kg;
		hwgchart.ist_ges_eur += hwgchart.ist_eur;
		if (hwgchart.hwg_anz > 0)
		{
			hwgchart.ist_ges_proz = hwgchart.ist_ges_anz * 100 / hwgchart.hwg_anz;          //Anteil am HWG-Sortiment (z.b. in der HWG sind 100 Artikel, bestellt wurden 65   proz = 65 )
		} else hwgchart.ist_ges_proz = 0.0;
		if (hwgchart.ist_ges_kg != 0.0)
		{
			hwgchart.ist_ges_eurkg = hwgchart.ist_ges_eur / hwgchart.ist_ges_kg;
		} else hwgchart.ist_ges_eurkg = 0.0;
	   } // end of if smt
	}
	HwgChart.hole_sollwerte(aufk.auf,cfg_AnzTageSollwerte); 
	if (hwgchart.hwg_anz > 0) hwgchart.soll_proz = hwgchart.soll_anz * 100 / hwgchart.hwg_anz;          //Anteil am HWG-Sortiment (z.b. in der HWG sind 100 Artikel, bestellt wurden 65   proz = 65 )
	if (hwgchart.soll_kg != 0.0)
	{
		hwgchart.soll_eurkg = hwgchart.soll_eur / hwgchart.soll_kg;
	} else hwgchart.soll_eurkg = 0.0;

	HwgChart.dbupdate ();
	if (sqlstatus < 0) 
	{
		char cmess [80];
		sprintf (cmess,"sqlstatus %ld bei Auftrag %ld",sqlstatus,aufk.auf);
		disp_mess (cmess,2);
	}
	dsqlstat_WK = HwgBestWk.dbread_all ();
  }// end of while (dsqlstat_WK == 0)

//jetzt aufchart
	char clieferdat [16];
  					   dlong_to_asc (aufk.lieferdat, clieferdat);
					   aufchart.jr = get_jahr (clieferdat);
					   if (aufchart.jr > 0)
					   {
						memcpy (&aufchart, &aufchart_null, sizeof (struct AUFCHART));
						aufchart.auf = aufk.auf;
						AufChart.dbreadfirst ();
						aufchart.kun = aufk.kun;
						aufchart.lieferdat = aufk.lieferdat;
						dlong_to_asc (aufk.lieferdat, clieferdat);
						aufchart.jr = get_jahr (clieferdat);
						if (aufchart.jr <99)  aufchart.jr = 2000 + aufchart.jr;
						aufchart.mo = get_monat (clieferdat);
						aufchart.kw = get_woche (clieferdat);
						aufchart.wochentag = get_wochentag (clieferdat);
						if (aufchart.woche_folge == 0)
						{
							int von_lieferdat = aufk.lieferdat - aufchart.wochentag + 1;
							int bis_lieferdat = aufk.lieferdat - 1;
							int dfolge = 0;
				            ins_quest ((char *) &aufk.kun, 2, 0);
				            ins_quest ((char *) &von_lieferdat, 2, 0);
				            ins_quest ((char *) &bis_lieferdat, 2, 0);
				            out_quest ((char *) &dfolge, 2, 0);
							execute_sql ("select count(unique lieferdat ) from aufk  "
						     "where kun = ? and lieferdat between ? and ? ");   
							   if (sqlstatus == 0)
							   {
									aufchart.woche_folge = dfolge + 1;
							   } else aufchart.woche_folge = 1;
						}
						AufChart.dbupdate ();
					   }

  }

commitwork ();
}


BOOL CCalcualteSmt::IsDiscountArticle (double a, LPSTR smt)
{
    CString Smt = smt;
	Smt.Trim ();
	if (Smt.GetLength () == 0)
	{
		lese_a_bas (a);
		strcpy (smt, _a_bas.smt);
		Smt = _a_bas.smt;
	}
	return (Smt == "R");
}

