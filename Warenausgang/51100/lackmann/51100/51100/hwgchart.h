#ifndef _HWGCHART_DEF
#define _HWGCHART_DEF

#include "dbclass.h"

struct HWGCHART {
   long      auf;
   short     hwg;
   long      hwg_anz;
   long      ist_ges_anz;
   double    ist_ges_eur;
   double    ist_ges_kg;
   double    ist_ges_proz;
   double    ist_ges_eurkg;
   double    ziel_ges_eur;
   double    ziel_ges_kg;
   double    ziel_ges_proz;
   double    ziel_ges_eurkg;
   double    soll_eur;
   double    soll_kg;
   double    soll_proz;
   double    soll_eurkg;
   long      ist_anz;
   double    ist_eur;
   double    ist_kg;
   double    ist_proz;
   double    ist_eurkg;
   long      soll_anz;
   short     aktiv;
};
extern struct HWGCHART hwgchart, hwgchart_null;

#line 7 "hwgchart.rh"

class HWGCHART_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               HWGCHART_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
               int del_istwerte (void);
               int hole_istgeswerte (int dauf);
               int hole_sollwerte (int dauf, int danz_tage);
               
};
#endif
