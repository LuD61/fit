#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "wmaskc.h"
#include "colbut.h"
#include "dbclass.h"
#include "cmask.h"
#include "mo_chq.h"
#include "lbox.h"

#define BuOk     1001
#define BuCancel 1002
#define LIBOX    1003
#define BuDial   1004
#define ID_LABEL 1005
#define ID_EDIT  1006 


static mfont mFont = {
                     "MS Sans Serif", 
                      100, 
                      0, 
                      0, 
                      BLACKCOL, 
                      LTGRAYCOL,
                      0, 0};



static CFIELD **_Query;
static CFORM *Query;

static CFIELD **_fListe;
static CFORM *fListe;

static CFIELD **_fButton;
static CFORM *fButton;

static CFIELD **_fWork;
static CFORM *fWork;


int CHQ::listpos = 1;
char CHQ::EBuff [80];
int (*CHQ::OkFunc) (int) = NULL;
int (*CHQ::OkFuncE) (char *) = NULL;
int (*CHQ::DialFunc) (int)  = NULL;
int (*CHQ::FillDb) (char *) = NULL;

CHQ::CHQ (int cx, int cy, char *Label, char *Text)
{
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			SIZE size;
			HDC hdc;
			int x,y;

 	        this->Font = &mFont;  

			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			GetTextExtentPoint32 (hdc, "X", 1, &size);
			DeleteObject (SelectObject (hdc, oldfont));
	

			this->cx = size.cx * cx;
			this->cy = tm.tmHeight * cy;
            x = strlen (Label) + 3; 
			strcpy (EBuff, Text);

			_Query     = new CFIELD * [2];
			_Query[0]  = new CFIELD ("Label", Label,  
				                      0, 0, 1, 1, NULL, "", CDISPLAYONLY,
												 ID_LABEL, Font, 0, 0);
			_Query[1]  = new CFIELD ("Query", EBuff,  
				                     37, 0, x, 1, NULL, "", CEDIT,
												 ID_EDIT, Font, 0, 0);
			 Query = new CFORM (2, _Query);


            _fListe    = new CFIELD * [1];
			_fListe[0] = new CFIELD ("Liste", "Liste",
				                     cx - 2, cy - 7, 1, 3, NULL, "", CLISTBOX,
												 LIBOX, Font, 0, 0);

			fListe     = new CFORM (1, _fListe);
			fListe->Setchsize (2, 2);

			x = 0;
			y = (cy - 2) * tm.tmHeight;
			cx = 10 * size.cx;
			cy = (int) (double) ((double) tm.tmHeight * 1.3);
			
			_fButton    = new CFIELD * [2];
			_fButton[0] = new CFIELD ("OK", "OK", cx, cy, x * size.cx, y, 
				                                  NULL, "", CBUTTON,
                                                  BuOk, Font, 1, BS_PUSHBUTTON);				                                   
 
			_fButton[1] = new CFIELD ("Cancel", "Abbruch", 
				                                  cx , cy, x + 12 * size.cx, y,
				                                  NULL, "", CBUTTON,
                                                  BuCancel, Font, 1, BS_PUSHBUTTON);				    
			fButton     = new CFORM (2, _fButton);
			fButton->Setchpos (0, 2);

            _fWork    = new CFIELD * [3];
			_fWork[0] = new CFIELD ("fQuery", Query, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 982, Font, 0, 0);
			_fWork[1] = new CFIELD ("fListe", fListe, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 980, Font, 0, 0);
			_fWork[2] = new CFIELD ("Button", fButton, 0, 0, -1, 0, NULL, "", CFFORM,
				                                 981, Font, 0, 0);

			fWork   = new CFORM (3, _fWork);
}

CHQ::~CHQ ()
{
	        fWork->destroy (); 
            delete _Query[0];   
            delete _Query[1];   
            delete _Query;   

            delete _fListe[0];   
            delete _fListe;   

			delete _fButton[0];
			delete _fButton[1];
			delete _fButton[2];
			delete fButton;

			delete _fWork[0];
			delete _fWork[1];
			delete _fWork[2];
			delete fWork;
}

void CHQ::SetOkFunc (int (*OkF) (int))
{
	       OkFunc = OkF;
}

void CHQ::SetOkFuncE (int (*OkF) (char *))
{
	       OkFuncE = OkF;
}

void CHQ::SetDialFunc (int (*DialF) (int))
{
	       DialFunc = DialF;
}

void CHQ::SetFillDb (int (*Fi) (char *))
{
	       FillDb = Fi;
}

void CHQ::SetCurrentID (DWORD ID)
{
	       if (fWork->SetCurrentID (ID))
		   {
			   currentfield = ID;
		   }
}

void CHQ::SetCurrentName (char *name)
{
	       if (fWork->SetCurrentName (name))
		   {
			   currentfield = fWork->GetID (name);
		   }
}

void CHQ::EnableID (DWORD ID, BOOL flag)
{
	       fWork->EnableID (ID, flag);
}

void CHQ::EnableName (char *name, BOOL flag)
{
	       fWork->EnableName (name, flag);
}

void CHQ::CheckID (DWORD ID, BOOL flag)
{
	       fWork->CheckID (ID, flag);
}

void CHQ::CheckName (char *name, BOOL flag)
{
	       fWork->CheckName (name, flag);
}

BOOL CHQ::TestButtons (HWND hWndBu)
{
	       if (hWndBu == fWork->GethWndID (BuOk))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuOk, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
	       if (hWndBu == fWork->GethWndID (BuCancel))
		   {
			   SendMessage (hWnd, WM_COMMAND, MAKELONG (BuCancel, VK_RETURN),
				                                    (LPARAM) hWnd); 
			   return TRUE;
		   }
		   return FALSE;
}
	       

BOOL CHQ::TestEdit (HWND hWndEd)
{
	       DWORD attr;
		   
		   attr = fWork->GetAttribut ();
		   if (attr == CEDIT)
		   {
			  if (hWndEd == fWork->GethWndID (ID_EDIT))
			  {
                         fWork->NextFormField (); 
				         GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, 255);
	                     SendMessage (fWork->GethWndID (LIBOX), 
								            LB_RESETCONTENT, (WPARAM) 0l, 
				                            (LPARAM) 0l); 
						 (*FillDb) (EBuff);
						  SetFocus (fWork->GethWndID (LIBOX));
                          SendMessage (fWork->GethWndID (LIBOX), LB_SETCURSEL, 0, 0L);
			  }
			  else
			  {
                         fWork->NextField (); 
			  }
			  return TRUE;
		   }
		   return FALSE;
}



void CHQ::ProcessMessages (void)
{
	      MSG msg;
		  HWND hWnd;

          hWnd = fWork->GethWndID (LIBOX);
		  fWork->SetCurrent (0);
		  Query->GetCfield () [1]->SetFocus ();
          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
			      if (msg.wParam == VK_TAB)
				  {

                      if (GetKeyState (VK_SHIFT) < 0)
                      {
                              syskey = KEYSTAB;
							  if (msg.hwnd == fWork->GethWndID (ID_EDIT))
							  {
                                   fWork->PriorFormField (); 
							  }
							  else
							  {
                                   fWork->PriorField (); 
							  }
					  }
					  else
					  {
					          syskey = KEYTAB;
							  if (msg.hwnd == fWork->GethWndID (ID_EDIT))
							  {
                                   fWork->NextFormField (); 
 						           GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, 255);
							  }
							  else
							  {
                                   fWork->NextField (); 
							  }
					  }
					  continue;
				  }
				  else if (msg.wParam == VK_F5)
				  {
					  syskey = KEY5;
					  break;
				  }
				  else if (msg.hwnd == fWork->GethWndName ("Liste"));
				  else if (msg.wParam == VK_DOWN)
				  {

					  syskey = KEYDOWN;
					  if (msg.hwnd == fWork->GethWndID (ID_EDIT))
					  {
                                fWork->NextFormField (); 
 						        GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, 255);
					  }
					  else
					  {
                                fWork->NextField (); 
					  }
					  continue;
				  }
				  else if (msg.wParam == VK_UP)
				  {

					  syskey = KEYUP;
					  if (msg.hwnd == fWork->GethWndID (ID_EDIT))
					  {
                                fWork->PriorFormField (); 
					  }
					  else
					  {
                                fWork->PriorField (); 
					  }
					  continue;
				  }
				  else if (msg.wParam == VK_RETURN)
				  {
					  syskey = KEYCR;
					  if (TestButtons (msg.hwnd))
					  {
					           continue;
					  }
					  if (TestEdit (msg.hwnd))
					  {
					           continue;
					  }
				  }
			  }
              TranslateMessage(&msg);
			  DispatchMessage(&msg);
		  }
}


CALLBACK CHQ::CProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					fWork->display (hWnd, hdc);
                    EndPaint (hWnd, &ps);
					break;
              case WM_SIZE :
				    MoveWindow ();
					break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == BuOk)
                    {
						   syskey = KEYCR;
						   if (OkFuncE)
						   {
			                   SendMessage (fWork->GethWndID (LIBOX), 
								            LB_RESETCONTENT, (WPARAM) 0l, 
				                            (LPARAM) 0l); 
							   (*OkFuncE) (EBuff);
							   SetFocus (fWork->GethWndID (LIBOX));
                               SendMessage (fWork->GethWndID (LIBOX), LB_SETCURSEL, 0, 0L);
						   }
						   else
						   {
                                PostQuitMessage (0);
						   }
                    }
                    if (LOWORD (wParam) == BuDial)
                    {
						   syskey = KEYCR;
						   if (DialFunc)
						   {
							   (*DialFunc) (GetSel ());
						   }
                    }
                    else if (LOWORD (wParam) == BuCancel)
                    {
						   syskey = KEY5;
                           PostQuitMessage (0);
                    }
                    else if (LOWORD (wParam) == LIBOX)
                    {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
						          syskey = KEYCR;
					 	          if (OkFunc)
								  {
							              (*OkFunc) (GetSel ());
								  }
						          else
								  {
                                           PostQuitMessage (0);
								  }
						   }
                           else if (HIWORD (wParam) == VK_RETURN)
                           {
						          syskey = KEYCR;
					 	          if (OkFunc)
								  {
							              (*OkFunc) (GetSel ());
								  }
						          else
								  {
                                           PostQuitMessage (0);
								  }
						   }
                           else if (HIWORD (wParam) == LBN_SELCHANGE)
                           {
		                          fWork->SetCurrent (listpos);
						   }
                           else if (HIWORD (wParam) == LBN_SETFOCUS)
                           {
							       SetOkFuncE (NULL);
						   }
                    }
                    else if (LOWORD (wParam) == ID_EDIT)
                    {
                           if (HIWORD (wParam) == EN_SETFOCUS)
                           {
							       SetWindowText (fWork->GethWndID (ID_EDIT), EBuff);
								   fWork->SetCurrentFieldID (ID_EDIT);
                                   SendMessage (fWork->GethWndID (ID_EDIT), 
									   EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
							       SetOkFuncE (FillDb);
						   }
						   else if (HIWORD (wParam) == EN_KILLFOCUS)
						   {
							       GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, 255);
						   }

						   else if (HIWORD (wParam) == EN_CHANGE)
						   {
							       GetWindowText (fWork->GethWndID (ID_EDIT), EBuff, 255);
						   }
					}
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void CHQ::InsertCaption (char *buffer)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);
		 
		 SendMessage (hWnd, LB_TITLE, -1,  
                                 (LPARAM) (char *) buffer);
}



void CHQ::InsertRecord (char *buffer)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

         SendMessage (hWnd, LB_INSERTSTRING, -1,  
                                 (LPARAM) (char *) buffer);
}

void CHQ::UpdateRecord (char *buffer, int pos)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

         SendMessage (hWnd, LB_INSERTSTRING, pos,  
                                 (LPARAM) (char *) buffer);
}

void CHQ::GetText (char *buffer)
{
	     HWND hWnd;
		 int idx;

         hWnd = fWork->GethWndID (LIBOX);

	     idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
	     SendMessage (hWnd, LB_GETTEXT, idx, (LPARAM) buffer);
}

int CHQ::GetSel (void)
{
	     HWND hWnd;
		 int idx;

         hWnd = fWork->GethWndID (LIBOX);

	     idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
		 return idx;
}

void CHQ::SetSel (int idx)
{
	     HWND hWnd;

         hWnd = fWork->GethWndID (LIBOX);

	     SendMessage (hWnd, LB_SETCURSEL, (WPARAM) idx, (LPARAM) 0l);
		 return;
}

void CHQ::VLines (char *vpos, int hlines)
{
	       HWND hWnd;

		   hWnd = fWork->GethWndName ("Liste");
           SendMessage (hWnd, LB_CAPTSIZE, 0,  
                                 (LPARAM) 130);
           SendMessage (hWnd, LB_VPOS, hlines,  
                                 (LPARAM) (char *) vpos);
}

void CHQ::DestroyWindow (void)
{
	       fWork->destroy ();
		   ::DestroyWindow (hWnd);
}
						
void CHQ::OpenWindow (HANDLE hInstance, HWND hMainWindow)
{
		  int x, y;
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  RECT rect, rect1;

		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;
//		  SetColBorderM (TRUE);

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CQueryWind";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }

		  GetClientRect (hMainWindow, &rect);
		  GetWindowRect (hMainWindow, &rect1);
/*
		  cx = rect.right;
		  cy = rect.bottom;
*/

		  x  = max (0, (rect.right - cx) / 2)  + rect1.left;
		  y  = max (0, (rect.bottom - cy) / 2) + rect1.top;

          hWnd = CreateWindowEx (0, 
                                 "CQueryWind",
                                 "",
                                 WS_VISIBLE | WS_POPUP | WS_THICKFRAME,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
		  ShowWindow (hWnd, SW_SHOWNORMAL);
		  UpdateWindow (hWnd);
}

void CHQ::MoveWindow (void)
{
/*
		  RECT rect;

		  GetClientRect (hMainWindow, &rect);
          ::MoveWindow (hWnd, rect.left, rect.top, rect.right, rect.bottom, TRUE);
*/

	      fWork->MoveWindow ();
}

