// Font.cpp: Implementierung der Klasse CFont.
//
//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "Font.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CFont::CFont()
{
	m_hFont = NULL;
	nHeight = 95; 
 	nWidth  = 0; 
	nEscapement = 0; 
	nOrientation = 0; 
	fnWeight = FW_NORMAL; 
	fdwItalic = FALSE; 
	fdwUnderline = FALSE; 
	fdwStrikeOut = FALSE; 
	fdwCharSet = ANSI_CHARSET; 
	fdwOutputPrecision = OUT_DEFAULT_PRECIS; 
	fdwClipPrecision = CLIP_DEFAULT_PRECIS; 
	fdwQuality = DEFAULT_QUALITY; 
	fdwPitchAndFamily = DEFAULT_PITCH; 
	lpszFace = ""; 
}

CFont::~CFont()
{
	if (m_hFont != NULL)
	{
		DeleteObject (m_hFont);
	}
}

void CFont::Create (LPSTR name, int size, BOOL bold, BOOL italic, BOOL underline)
{
	lpszFace = name;
	nHeight = size;
	if (bold)
	{
		fnWeight = FW_BOLD;
	}
	fdwItalic = italic;
	fdwUnderline = underline;
	m_hFont = CreateFont (	nHeight,
 							nWidth,
							nEscapement,
							nOrientation,
							fnWeight,
							fdwItalic,
							fdwUnderline,
							fdwStrikeOut,
							fdwCharSet,
							fdwOutputPrecision,
							fdwClipPrecision,
							fdwQuality,
							fdwPitchAndFamily,
							lpszFace);
}

CFont::operator HFONT () const
{
	return m_hFont;
}

void CFont::Attach (HFONT hFont)
{
	m_hFont = hFont;
}

