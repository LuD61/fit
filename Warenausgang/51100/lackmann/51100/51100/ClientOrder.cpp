// ClientOrder.cpp: Implementierung der Klasse CClientOrder.
//
//////////////////////////////////////////////////////////////////////

#include <fstream>
#include "ClientOrder.h"
#include <stdlib.h>
#include "strfkt.h"
#include "A_bas.h"
#include "a_hndw.h"
#include "sg.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

#define RD_OPTIMIZE RdOptimize


using namespace std;


extern HNDW_CLASS HndwClass;


CClientOrder::CClientOrder()
{
	ListHandler = NULL;
	StndAll = NULL;
	lBestAll = NULL; //LAC-9
	KritMhdAll = NULL; //LAC-84
	KommldatAll = NULL; //LAC-116
	HWG1All = NULL; //LAC-9
	HWG2All = NULL; //LAC-9
	HWG3All = NULL; //LAC-9
	HWG4All = NULL; //LAC-9
	HWG5All = NULL; //LAC-9
	HWG6All = NULL; //LAC-9
	HWG7All = NULL; //LAC-9
	HWG8All = NULL; //LAC-9
	HWG9All = NULL; //LAC-9
	HWG10All = NULL; //LAC-9
	HWG11All = NULL; 
	HWG12All = NULL; 
	HWG13All = NULL; 
	HWG14All = NULL; 
	HWG15All = NULL; 
	HWG16All = NULL; 
	MustReadLetztBest = TRUE; //LAC-9
	dlieferdat = 0;
	akt_auf = 0;
	mdn = 0;
	fil = 0;
	kun_fil = 0;
	kun_nr = 0l;
	dletzteBestellung = 0l;
	dbestVorschlag = 0l;
	akt_hwg = 0;
	akt_wg = 0;
	SortMode = PosA;
	RdOptimize = No;
	RefreshRdOptimize = FALSE;
	cursor = -1;
	cursor_lbestk = -1; //LAC-9
	cursor_lbestp = -1; //LAC-9
	cursor_kritmhdk = -1; 
	cursor_bestellvorschlag = -1; 
	cursor_bestellvorschlagges = -1; 
	cursor_kritmhdp = -1; 
	cursor_kommldatk = -1; 
	cursor_suche_kw = -1; //LAC-131 
	cursor_kommldatp = -1; 
	cursor_lieferung = -1; //LAC-54
	cursor_hwg = -1; //LAC-9
	cursor_wg = -1; //LAC-9
	Bestellvorschlag = FALSE; //LAC-9
	letzteBestellung = FALSE; //LAC-9
	KritischeMHD = FALSE; //LAC-84
	Kommldat = FALSE; //LAC-116 kommissioniert am gleichem Lieferdatum
	HWG1 = FALSE; //LAC-9
	HWG2 = FALSE; //LAC-9
	HWG3 = FALSE; //LAC-9
	HWG4 = FALSE; //LAC-9
	HWG5 = FALSE; //LAC-9
	HWG6 = FALSE; //LAC-9
	HWG7 = FALSE; //LAC-9
	HWG8 = FALSE; //LAC-9
	HWG9 = FALSE; //LAC-9
	HWG10 = FALSE; //LAC-9
	HWG11 = FALSE; 
	HWG12 = FALSE; 
	HWG13 = FALSE; 
	HWG14 = FALSE; 
	HWG15 = FALSE; 
	HWG16 = FALSE; 
    LastFromAufKun = FALSE;
	last_kun = 0l;
	inh = 0.0;
}

CClientOrder::~CClientOrder()
{

}

void CClientOrder::SetKeys (short mdn, short fil, short kun_fil, long kun, BOOL MustReadLetztBest, long dlieferdat, long akt_auf, long dletztBest, long dBestVorschlag)
{
	stnd_aufp.mdn = mdn;
	stnd_aufp.fil = fil;
	stnd_aufp.kun_fil = kun_fil;
	stnd_aufp.kun = kun;
	this->mdn = mdn;
	this->fil = fil;
	this->kun_fil = kun_fil;
	this->kun_nr = kun;
	this->MustReadLetztBest = MustReadLetztBest; //LAC-9
	this->dlieferdat = dlieferdat; //LAC-117
	this->akt_auf = akt_auf; //LAC-117
	this->dletzteBestellung = dletztBest; 
	this->dbestVorschlag = dBestVorschlag; 

}

void CClientOrder::Load ()   //nicht mehr aktiv !!! 
{
	BOOL MustLoad = FALSE;

	if (ListHandler == NULL)
	{
		ListHandler = CListHandler::GetInstance ();
		StndAll = ListHandler->GetStndAll ();
		lBestAll = ListHandler->GetlBestAll (); //LAC-9
		KritMhdAll = ListHandler->GetKritMhdAll (); //LAC-84
		KommldatAll = ListHandler->GetKommldatAll (); //LAC-116
		HWG1All = ListHandler->GetHWG1All ();
		HWG2All = ListHandler->GetHWG2All ();
		HWG3All = ListHandler->GetHWG3All ();
		HWG4All = ListHandler->GetHWG4All ();
		HWG5All = ListHandler->GetHWG5All ();
		HWG6All = ListHandler->GetHWG6All ();
		HWG7All = ListHandler->GetHWG7All ();
		HWG8All = ListHandler->GetHWG8All ();
		HWG9All = ListHandler->GetHWG9All ();
		HWG10All = ListHandler->GetHWG10All ();
		HWG11All = ListHandler->GetHWG11All ();
		HWG12All = ListHandler->GetHWG12All ();
		HWG13All = ListHandler->GetHWG13All ();
		HWG14All = ListHandler->GetHWG14All ();
		HWG15All = ListHandler->GetHWG15All ();
		HWG16All = ListHandler->GetHWG16All ();
//		ListHandler->Init (0);
		MustLoad = TRUE;
		mdn = stnd_aufp.mdn;
		fil = stnd_aufp.fil;
		kun_fil = stnd_aufp.kun_fil;
		kun_nr = stnd_aufp.kun;
	}
	else
	{
		if (mdn != stnd_aufp.mdn ||
			fil != stnd_aufp.fil ||
			kun_fil != stnd_aufp.kun_fil ||
			Bestellvorschlag == FALSE ||
			kun_nr != stnd_aufp.kun)
		{
			ListHandler->Init (REITERBESTELLVORSCHLAG); //LAC-9
			MustLoad = TRUE;
		    mdn = stnd_aufp.mdn;
			fil = stnd_aufp.fil;
			kun_fil = stnd_aufp.kun_fil;
			kun_nr = stnd_aufp.kun;
		}
		else
		{
//LAC-9			ListHandler->GetStndValue ()->DestroyElements ();
//LAC-9			ListHandler->GetStndValue ()->Destroy ();
//LAC-9			ListHandler->InitAufMe ();
		}
	}
	if (MustLoad)
	{
		if (cursor == -1)
		{
			Prepare ();
		}
		Read ();
		Bestellvorschlag = TRUE; //LAC-9

	}
}


void CClientOrder::Load_Bestellvorschlag () //LAC-156
{
	BOOL MustLoad = FALSE;

	if (ListHandler == NULL)
	{
		ListHandler = CListHandler::GetInstance ();
		StndAll = ListHandler->GetStndAll ();
		lBestAll = ListHandler->GetlBestAll (); 
		KritMhdAll = ListHandler->GetKritMhdAll (); 
		KommldatAll = ListHandler->GetKommldatAll (); 
		HWG1All = ListHandler->GetHWG1All ();
		HWG2All = ListHandler->GetHWG2All ();
		HWG3All = ListHandler->GetHWG3All ();
		HWG4All = ListHandler->GetHWG4All ();
		HWG5All = ListHandler->GetHWG5All ();
		HWG6All = ListHandler->GetHWG6All ();
		HWG7All = ListHandler->GetHWG7All ();
		HWG8All = ListHandler->GetHWG8All ();
		HWG9All = ListHandler->GetHWG9All ();
		HWG10All = ListHandler->GetHWG10All ();
		HWG11All = ListHandler->GetHWG11All ();
		HWG12All = ListHandler->GetHWG12All ();
		HWG13All = ListHandler->GetHWG13All ();
		HWG14All = ListHandler->GetHWG14All ();
		HWG15All = ListHandler->GetHWG15All ();
		HWG16All = ListHandler->GetHWG16All ();
		MustLoad = TRUE;
		mdn = stnd_aufp.mdn;
		fil = stnd_aufp.fil;
		kun_fil = stnd_aufp.kun_fil;
		kun_nr = stnd_aufp.kun;
	}
	else
	{
		if (mdn != stnd_aufp.mdn ||
			fil != stnd_aufp.fil ||
			kun_fil != stnd_aufp.kun_fil ||
			Bestellvorschlag == FALSE ||
			TRUE )   //LAC-132 immer neu laden 
//			kun_nr != stnd_aufp.kun)
		{
			ListHandler->Init (REITERBESTELLVORSCHLAG); 
			MustLoad = TRUE;
		    mdn = stnd_aufp.mdn;
			fil = stnd_aufp.fil;
			kun_fil = stnd_aufp.kun_fil;
			kun_nr = stnd_aufp.kun;
		}
	}
	if (MustLoad)
	{
		if (cursor_bestellvorschlag == -1)
		{
			Prepare_Bestellvorschlag ();
		}
		Read_Bestellvorschlag ();
		Bestellvorschlag = TRUE; 

	}
}


void CClientOrder::Prepare ()
{
	StndAufp.sqlin ((short *) &mdn, 1, 0);
	StndAufp.sqlin ((short *) &fil, 1, 0);
	StndAufp.sqlin ((long *) &kun_nr, 2, 0);
	StndAufp.sqlin ((short *) &kun_fil, 1, 0);

	StndAufp.sqlout ((short *) &stnd_aufp.mdn, 1, 0);
	StndAufp.sqlout ((short *) &stnd_aufp.fil, 1, 0);
	StndAufp.sqlout ((short *) &stnd_aufp.kun, 2, 0);
	StndAufp.sqlout ((short *) &stnd_aufp.kun_fil, 1, 0);
	StndAufp.sqlout ((long *) &stnd_aufp.posi, 2, 0);
	StndAufp.sqlout ((double *) &stnd_aufp.a, 3, 0);
	StndAufp.sqlout ((double *) &stnd_aufp.me, 3, 0);
	StndAufp.sqlout ((short *) &ag, 1, 0);
	StndAufp.sqlout ((short *) &wg, 1, 0);
	StndAufp.sqlout ((char *)  &a_bz1, 0, 25);

	if (SortMode == PosA)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, stnd_aufp.me, a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a and a_bas.delstatus = 0 "
								"order by 5,6");  
	}
	else if (SortMode == AgA)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, stnd_aufp.me,  a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a  and a_bas.delstatus = 0 "
								"order by 7,6");  
	}
	else if (SortMode == WgA)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, stnd_aufp.me,  a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a  and a_bas.delstatus = 0 "
								"order by 8,6");  
	}
	else if (SortMode == AgBz)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, stnd_aufp.me,  a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a  and a_bas.delstatus = 0 "
								"order by 7,9");  
	}
	else if (SortMode == WgBz)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, stnd_aufp.me,  a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a "
								"order by 8,9");  
	}
}


int CClientOrder::Read ()
{
	int i = 0;
	int dsqlstatus = 100;
	int posi;
	AUFPS *Aufps;

	KunBran.Format (_T("kun%ld"), kun_nr);
    if ((dsqlstatus = StndAufp.sqlopen (cursor)) != 0)
	{
		return dsqlstatus;
	}

	if (ReadAufps ())
	{
		StndAufp.sqlclose (cursor); 
		return 0;
	}
    dsqlstatus = StndAufp.sqlfetch (cursor);

	if (dsqlstatus == 100)
	{
			if (stnd_aufp.kun_fil != 0) return dsqlstatus;


            StndAufp.sqlin  ((short *) &mdn, 1, 0);
            StndAufp.sqlin  ((short *) &fil, 1, 0);  
            StndAufp.sqlin  ((long *) &kun_nr, 2, 0);
            StndAufp.sqlout ((char *) kun_bran2, 0, 4);
			dsqlstatus = StndAufp.sqlcomm ("select kun_bran2 from kun "
					              "where mdn = ? "
								  "and fil = ? "
								  "and kun = ?");
			if (dsqlstatus == 100) return dsqlstatus;

			KunBran.Format (_T("bran%s"), kun_bran2);
			KunBran.Trim ();
			if (ReadAufps ())
			{
				StndAufp.sqlclose (cursor); 
				return 0;
			}

			kun_nr = atol (kun_bran2);
			kun_fil = 2;
            stnd_aufp.kun = kun_nr;        
            stnd_aufp.kun_fil = kun_fil;
            dsqlstatus = StndAufp.sqlopen (cursor); 
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
            dsqlstatus = StndAufp.sqlfetch (cursor); 
	}

	posi = 10;
    while (dsqlstatus == 0)
    {
			 Aufps = new AUFPS;   
			 memset (Aufps, 0, sizeof (AUFPS));
            // StndAufp.dbreadfirsta ();
             sprintf (Aufps->a, "%13.0lf", stnd_aufp.a); 
             dsqlstatus = lese_a_bas (stnd_aufp.a);
             if (dsqlstatus == 0)
             {
                         strcpy (Aufps->a_bz1, _a_bas.a_bz1); 
             }
             else
             {
                         strcpy (Aufps->a_bz1, " "); 
             }
			 sprintf (Aufps->posi, "%d", posi);
			 Aufps->hwg = _a_bas.hwg; //260312
		     sprintf (Aufps->soll_me, "%.3lf", stnd_aufp.me); //LAC-9
 			if (kun.min_best == 1)  
			{
				if (_a_bas.min_bestellmenge > 0 || _a_bas.staffelung > 0)
				{
					sprintf (Aufps->min_best,    "%.1lf/%.1lf",  _a_bas.min_bestellmenge, _a_bas.staffelung); //LAC-44
					Aufps->min_bestellmenge = _a_bas.min_bestellmenge; //LAC-104
					Aufps->sg_min_bestellmenge = sg.min_bestellmenge; //LAC-104
					Aufps->sg_staffel = sg.staffelung; //LAC-104
					Aufps->sg = sg.sg; //LAC-104
				}
			    if (sg.min_bestellmenge > 0.0)
				{
				    sprintf (Aufps->grp_min_best,    "%.1lf/%.1lf/%s",  sg.min_bestellmenge, sg.staffelung, clipped(sg.sg_bezk)); //LAC-104
				} else  strcpy (Aufps->grp_min_best,"");

			}

			 FillRow (Aufps);
			 StndAll->Add (Aufps);
             dsqlstatus = StndAufp.sqlfetch (cursor); 
             i ++;
			 posi += 10;
    }
	return 0;
}

void CClientOrder::initReiterv () //LAC-9
{ 
	Bestellvorschlag = FALSE; 
	letzteBestellung = FALSE; 
	KritischeMHD = FALSE; 
	Kommldat = FALSE; 
	HWG1 = FALSE; 
	HWG2 = FALSE; 
	HWG3 = FALSE; 
	HWG4 = FALSE; 
	HWG5 = FALSE; 
	HWG6 = FALSE; 
	HWG7 = FALSE; 
	HWG8 = FALSE; 
	HWG9 = FALSE; 
	HWG10 = FALSE;
	HWG11 = FALSE;
	HWG12 = FALSE;
	HWG13 = FALSE;
	HWG14 = FALSE;
	HWG15 = FALSE;
	HWG16 = FALSE;
}

void CClientOrder::Load_letztBest (int dauf) //LAC-9
{ 
	BOOL MustLoad = FALSE;

	if (ListHandler == NULL)
	{
		ListHandler = CListHandler::GetInstance ();
		StndAll = ListHandler->GetStndAll (); //LAC-9
		lBestAll = ListHandler->GetlBestAll ();
		KritMhdAll = ListHandler->GetKritMhdAll (); //LAC-84
		KommldatAll = ListHandler->GetKommldatAll (); //LAC-116
		HWG1All = ListHandler->GetHWG1All ();
		HWG2All = ListHandler->GetHWG2All ();
		HWG3All = ListHandler->GetHWG3All ();
		HWG4All = ListHandler->GetHWG4All ();
		HWG5All = ListHandler->GetHWG5All ();
		HWG6All = ListHandler->GetHWG6All ();
		HWG7All = ListHandler->GetHWG7All ();
		HWG8All = ListHandler->GetHWG8All ();
		HWG9All = ListHandler->GetHWG9All ();
		HWG10All = ListHandler->GetHWG10All ();
		HWG11All = ListHandler->GetHWG11All ();
		HWG12All = ListHandler->GetHWG12All ();
		HWG13All = ListHandler->GetHWG13All ();
		HWG14All = ListHandler->GetHWG14All ();
		HWG15All = ListHandler->GetHWG15All ();
		HWG16All = ListHandler->GetHWG16All ();
//		ListHandler->Init (0);
		MustLoad = TRUE;
		mdn = stnd_aufp.mdn;
		fil = stnd_aufp.fil;
		kun_fil = stnd_aufp.kun_fil;
		kun_nr = stnd_aufp.kun;
	}
	else
	{
		if (mdn != stnd_aufp.mdn ||
			fil != stnd_aufp.fil ||
			kun_fil != stnd_aufp.kun_fil ||
			letzteBestellung == FALSE ||
			kun_nr != stnd_aufp.kun || 
			TRUE )   //LAC-132 immer neu laden 
		{
			ListHandler->Init (REITERLETZTEBESTELLUNG);
			MustLoad = TRUE;
		    mdn = stnd_aufp.mdn;
			fil = stnd_aufp.fil;
			kun_fil = stnd_aufp.kun_fil;
			kun_nr = stnd_aufp.kun;
		}
		else
		{
//LAC-9			ListHandler->GetStndValue ()->DestroyElements ();
//LAC-9			ListHandler->GetStndValue ()->Destroy ();
//LAC-9			ListHandler->InitAufMe ();
		}
	}
	if (MustLoad)
	{
		if (cursor_lbestk == -1)
		{
			Prepare_letztBest ();
		}
		Read_letztBest (dauf); //LAC-132
		letzteBestellung = TRUE;

	}
}


void CClientOrder::Load_KritMhd () //LAC-84
{ 
	BOOL MustLoad = FALSE;

	if (ListHandler == NULL)
	{
		ListHandler = CListHandler::GetInstance ();
		StndAll = ListHandler->GetStndAll (); 
		lBestAll = ListHandler->GetlBestAll ();
		KritMhdAll = ListHandler->GetKritMhdAll (); //LAC-84
		KommldatAll = ListHandler->GetKommldatAll (); //LAC-116
		HWG1All = ListHandler->GetHWG1All ();
		HWG2All = ListHandler->GetHWG2All ();
		HWG3All = ListHandler->GetHWG3All ();
		HWG4All = ListHandler->GetHWG4All ();
		HWG5All = ListHandler->GetHWG5All ();
		HWG6All = ListHandler->GetHWG6All ();
		HWG7All = ListHandler->GetHWG7All ();
		HWG8All = ListHandler->GetHWG8All ();
		HWG9All = ListHandler->GetHWG9All ();
		HWG10All = ListHandler->GetHWG10All ();
		HWG11All = ListHandler->GetHWG11All ();
		HWG12All = ListHandler->GetHWG12All ();
		HWG13All = ListHandler->GetHWG13All ();
		HWG14All = ListHandler->GetHWG14All ();
		HWG15All = ListHandler->GetHWG15All ();
		HWG16All = ListHandler->GetHWG16All ();
		MustLoad = TRUE;
		mdn = stnd_aufp.mdn;
		fil = stnd_aufp.fil;
		kun_fil = stnd_aufp.kun_fil;
		kun_nr = stnd_aufp.kun;
	}
	else
	{
		if (mdn != stnd_aufp.mdn ||
			fil != stnd_aufp.fil ||
			kun_fil != stnd_aufp.kun_fil ||
			KritischeMHD == FALSE ||
			kun_nr != stnd_aufp.kun)
		{
			ListHandler->Init (REITERKRITISCHEMHD);
			MustLoad = TRUE;
		    mdn = stnd_aufp.mdn;
			fil = stnd_aufp.fil;
			kun_fil = stnd_aufp.kun_fil;
			kun_nr = stnd_aufp.kun;
		}
	}
	if (MustLoad)
	{
		if (cursor_kritmhdk == -1)
		{
			Prepare_KritMhd ();
		}
		Read_KritMhd ();
		KritischeMHD = TRUE;

	}
}

void CClientOrder::Load_Kommldat () //LAC-116
{ 
	BOOL MustLoad = FALSE;

	if (ListHandler == NULL)
	{
		ListHandler = CListHandler::GetInstance ();
		StndAll = ListHandler->GetStndAll (); 
		lBestAll = ListHandler->GetlBestAll ();
		KritMhdAll = ListHandler->GetKritMhdAll (); //LAC-84
		KommldatAll = ListHandler->GetKommldatAll (); //LAC-116
		HWG1All = ListHandler->GetHWG1All ();
		HWG2All = ListHandler->GetHWG2All ();
		HWG3All = ListHandler->GetHWG3All ();
		HWG4All = ListHandler->GetHWG4All ();
		HWG5All = ListHandler->GetHWG5All ();
		HWG6All = ListHandler->GetHWG6All ();
		HWG7All = ListHandler->GetHWG7All ();
		HWG8All = ListHandler->GetHWG8All ();
		HWG9All = ListHandler->GetHWG9All ();
		HWG10All = ListHandler->GetHWG10All ();
		HWG11All = ListHandler->GetHWG11All ();
		HWG12All = ListHandler->GetHWG12All ();
		HWG13All = ListHandler->GetHWG13All ();
		HWG14All = ListHandler->GetHWG14All ();
		HWG15All = ListHandler->GetHWG15All ();
		HWG16All = ListHandler->GetHWG16All ();
		MustLoad = TRUE;
		mdn = stnd_aufp.mdn;
		fil = stnd_aufp.fil;
		kun_fil = stnd_aufp.kun_fil;
		kun_nr = stnd_aufp.kun;
	}
	else
	{
		if (mdn != stnd_aufp.mdn ||
			fil != stnd_aufp.fil ||
			kun_fil != stnd_aufp.kun_fil ||
			Kommldat == FALSE ||
			kun_nr != stnd_aufp.kun)
		{
			ListHandler->Init (REITERKOMMLDAT);
			MustLoad = TRUE;
		    mdn = stnd_aufp.mdn;
			fil = stnd_aufp.fil;
			kun_fil = stnd_aufp.kun_fil;
			kun_nr = stnd_aufp.kun;
		}
	}
	if (MustLoad)
	{
		/** LAC-131
		if (cursor_kommldatk == -1)
		{
			Prepare_Kommldat ();
		}
		Read_Kommldat ();
		**/
		if (cursor_suche_kw == -1)
		{
			Prepare_KommldatKW ();
		}
		Read_KommldatKW ();
		Kommldat = TRUE;

	}
}

void CClientOrder::Load_HWG (int dhwg, int dwg, int idx) //LAC-9
{ 
	BOOL MustLoad = FALSE;

	if (ListHandler == NULL)
	{
		ListHandler = CListHandler::GetInstance ();
		StndAll = ListHandler->GetStndAll (); //LAC-9
		lBestAll = ListHandler->GetlBestAll ();
		KritMhdAll = ListHandler->GetKritMhdAll (); //LAC-84
		KommldatAll = ListHandler->GetKommldatAll (); //LAC-116
		HWG1All = ListHandler->GetHWG1All ();
		HWG2All = ListHandler->GetHWG2All ();
		HWG3All = ListHandler->GetHWG3All ();
		HWG4All = ListHandler->GetHWG4All ();
		HWG5All = ListHandler->GetHWG5All ();
		HWG6All = ListHandler->GetHWG6All ();
		HWG7All = ListHandler->GetHWG7All ();
		HWG8All = ListHandler->GetHWG8All ();
		HWG9All = ListHandler->GetHWG9All ();
		HWG10All = ListHandler->GetHWG10All ();
		HWG11All = ListHandler->GetHWG11All ();
		HWG12All = ListHandler->GetHWG12All ();
		HWG13All = ListHandler->GetHWG13All ();
		HWG14All = ListHandler->GetHWG14All ();
		HWG15All = ListHandler->GetHWG15All ();
		HWG16All = ListHandler->GetHWG16All ();
//		ListHandler->Init (0);
		MustLoad = TRUE;
		mdn = stnd_aufp.mdn;
		fil = stnd_aufp.fil;
		kun_fil = stnd_aufp.kun_fil;
		kun_nr = stnd_aufp.kun;
	}
	else
	{
		if (idx == 1)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG1 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG1);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 2)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG2 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG2);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 3)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG3 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG3);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 4)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG4 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG4);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 5)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG5 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG5);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 6)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG6 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG6);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 7)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG7 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG7);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 8)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG8 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG8);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 9)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG9 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG9);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 10)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG10 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG10);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 11)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG11 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG11);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 12)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG12 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG12);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 13)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG13 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG13);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 14)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG14 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG14);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 15)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG15 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG15);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
		else if (idx == 16)
		{
			if (mdn != stnd_aufp.mdn ||
				fil != stnd_aufp.fil ||
				kun_fil != stnd_aufp.kun_fil ||
				HWG16 == FALSE ||
				kun_nr != stnd_aufp.kun)
			{
				ListHandler->Init (REITERHWG16);
				MustLoad = TRUE;
				mdn = stnd_aufp.mdn;
				fil = stnd_aufp.fil;
				kun_fil = stnd_aufp.kun_fil;
				kun_nr = stnd_aufp.kun;
			}
		}
	}
	if (MustLoad)
	{
		if (cursor_hwg == -1)
		{
			Prepare_HWG ();
		}
		Read_HWG (dhwg, dwg, idx);
		if (idx == 1) HWG1 = TRUE;
		if (idx == 2) HWG2 = TRUE;
		if (idx == 3) HWG3 = TRUE;
		if (idx == 4) HWG4 = TRUE;
		if (idx == 5) HWG5 = TRUE;
		if (idx == 6) HWG6 = TRUE;
		if (idx == 7) HWG7 = TRUE;
		if (idx == 8) HWG8 = TRUE;
		if (idx == 9) HWG9 = TRUE;
		if (idx == 10) HWG10 = TRUE;
		if (idx == 11) HWG11 = TRUE;
		if (idx == 12) HWG12 = TRUE;
		if (idx == 13) HWG13 = TRUE;
		if (idx == 14) HWG14 = TRUE;
		if (idx == 15) HWG15 = TRUE;
		if (idx == 16) HWG16 = TRUE;

	}
}


void CClientOrder::Prepare_letztBest ()
{
	//LAC-117  nicht �ber today abfragen
	/**
		StndAufp.sqlin ((short *) &mdn, 1, 0);
		StndAufp.sqlin ((short *) &fil, 1, 0);
		StndAufp.sqlin ((long *) &kun_nr, 2, 0);
		StndAufp.sqlin ((short *) &kun_fil, 1, 0);

		StndAufp.sqlout ((short *) &stnd_aufp.mdn, 1, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.fil, 1, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.kun, 2, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.kun_fil, 1, 0);
		StndAufp.sqlout ((long *) &dauf, 2, 0);
		StndAufp.sqlout ((long *) &lieferdat, 2, 0);

           		cursor_lbestk = StndAufp.sqlcursor ("select aufk.mdn, aufk.fil, "
			                        "aufk.kun, aufk.kun_fil, aufk.auf, aufk.lieferdat "
			                        "from aufk "
									"where mdn = ? "
									"and   fil = ? "  
									"and   kun = ? "  
									"and   kun_fil = ? and lieferdat < today "
									"order by lieferdat desc");  
			                      
**/
		StndAufp.sqlin ((short *) &mdn, 1, 0);
		StndAufp.sqlin ((short *) &fil, 1, 0);
		StndAufp.sqlin ((long *) &kun_nr, 2, 0);
		StndAufp.sqlin ((short *) &kun_fil, 1, 0);
		StndAufp.sqlin ((long *) &akt_auf, 2, 0);
		StndAufp.sqlin ((long *) &dletzteBestellung, 2, 0); //LAC-132

		StndAufp.sqlout ((short *) &stnd_aufp.mdn, 1, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.fil, 1, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.kun, 2, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.kun_fil, 1, 0);
		StndAufp.sqlout ((long *) &dauf, 2, 0);
		StndAufp.sqlout ((long *) &lieferdat, 2, 0);

           		cursor_lbestk = StndAufp.sqlcursor ("select aufk.mdn, aufk.fil, "
			                        "aufk.kun, aufk.kun_fil, aufk.auf, aufk.lieferdat "
			                        "from aufk "
									"where aufk.mdn = ? "
									"and   aufk.fil = ? "  
									"and   aufk.kun = ? "  
									"and   aufk.kun_fil = ? "
									"and aufk.auf != ? and aufk.auf = ?"
									"order by aufk.lieferdat desc");  


		StndAufp.sqlin ((short *) &mdn, 1, 0);
		StndAufp.sqlin ((short *) &fil, 1, 0);
		StndAufp.sqlin ((long *) &dauf, 2, 0);

		StndAufp.sqlout ((long *) &stnd_aufp.posi, 2, 0);
		StndAufp.sqlout ((double *) &stnd_aufp.a, 3, 0);
		StndAufp.sqlout ((char *)  &auf_me_bz, 0, 6);
		StndAufp.sqlout ((double *) &vk_pr, 3, 0);
		StndAufp.sqlout ((double *) &auf_me, 3, 0);
		StndAufp.sqlout ((double *) &lief_me, 3, 0);
           		cursor_lbestp = StndAufp.sqlcursor ("select aufp.posi, aufp.a, "
									"aufp.auf_me_bz ,aufp.auf_vk_euro, aufp.auf_me , aufp.lief_me "
			                        "from aufp "
									"where aufp.mdn = ? "
									"and   aufp.fil = ? "  
									"and   aufp.auf = ? "  
									"order by aufp.posi");  

		StndAufp.sqlin ((short *) &mdn, 1, 0);
		StndAufp.sqlin ((short *) &fil, 1, 0);
		StndAufp.sqlin ((long *) &dauf, 2, 0);
		StndAufp.sqlin ((double *) &stnd_aufp.a, 3, 0);

		StndAufp.sqlout ((double *) &lieferung, 3, 0);
           		cursor_lieferung = StndAufp.sqlcursor ("select sum(lsp.lief_me) from lsk,lsp "
									"where lsk.mdn = ? "
									"and   lsk.fil = ? "  
									"and   lsk.auf = ? "
									"and lsk.mdn = lsp.mdn and lsk.fil = lsp.fil and lsk.ls = lsp.ls "
									"and lsp.a = ? ");

}


int CClientOrder::Read_letztBest (int dauf)  //Hier: letzte Bestellung holen 
{
	int i = 0;
	int dsqlstatusk = 0;
	int dsqlstatus = 100;
	int posi;
	BOOL posgefunden  = FALSE;
	BOOL flg_uebernahme = FALSE;
	AUFPS *Aufps;

        i = 0;

		if (dletzteBestellung < 0)  //LAC-132 �bernahme der ist-Werte
		{
			dletzteBestellung *= -1;
			flg_uebernahme = TRUE;
		}
        dsqlstatusk = StndAufp.sqlopen (cursor_lbestk); 
		while (dsqlstatusk == 0 && posgefunden == FALSE)
		{
	        dsqlstatusk = StndAufp.sqlfetch (cursor_lbestk);
			if (dsqlstatusk != 0) return dsqlstatusk;

	        dsqlstatus = StndAufp.sqlopen (cursor_lbestp); 
			dsqlstatus = StndAufp.sqlfetch (cursor_lbestp);
		
			posi = 10;
			while (dsqlstatus == 0)
			{
				 Aufps = new AUFPS;   
				memset (Aufps, 0, sizeof (AUFPS));
				sprintf (Aufps->a, "%13.0lf", stnd_aufp.a); 
//LAC-117				dsqlstatus = lese_a_bas (stnd_aufp.a);
//LAC-117				if (dsqlstatus == 0)
				if (lese_a_bas (stnd_aufp.a) == 0) //LAC-117
				{
		 			if (kun.min_best == 1)  
					{
						if (_a_bas.min_bestellmenge > 0 || _a_bas.staffelung > 0)
						{
							sprintf (Aufps->min_best,    "%.1lf/%.1lf",  _a_bas.min_bestellmenge, _a_bas.staffelung); //LAC-44
							Aufps->min_bestellmenge = _a_bas.min_bestellmenge; //LAC-104
							Aufps->sg_min_bestellmenge = sg.min_bestellmenge; //LAC-104
							Aufps->sg_staffel = sg.staffelung; //LAC-104
							Aufps->sg = sg.sg; //LAC-104
						}
						 if (sg.min_bestellmenge > 0.0)
							{
								sprintf (Aufps->grp_min_best,    "%.1lf/%.1lf/%s",  sg.min_bestellmenge, sg.staffelung, clipped(sg.sg_bezk)); //LAC-104
							} else  strcpy (Aufps->grp_min_best,"");
					}
					strcpy (Aufps->a_bz1, _a_bas.a_bz1); 
					sprintf (Aufps->posi, "%d", posi);
					Aufps->hwg = _a_bas.hwg; //260312
					//LAC-54 A
					sprintf (Aufps->last_lief_me, "%.2lf", lief_me); 
					if (flg_uebernahme) //LAC-132
					{
						sprintf (Aufps->auf_me, "%.2lf", auf_me);    
						sprintf (Aufps->lief_me, "%.2lf", lief_me);  
					}
			        StndAufp.sqlopen (cursor_lieferung); 
					if ( StndAufp.sqlfetch (cursor_lieferung) == 0)
					{
						sprintf (Aufps->last_lieferung, "%.2lf", lieferung); 
					}
					//LAC-54 E

					FillRow (Aufps);
					lBestAll->Add (Aufps);
					if (flg_uebernahme) //LAC-132
					{
						ListHandler->Update (0, Aufps);
					}

					i ++;
					posi += 10;
					posgefunden = TRUE;
				}
				else
				{
							strcpy (Aufps->a_bz1, " "); 
				}
 				dsqlstatus = StndAufp.sqlfetch (cursor_lbestp); 
			}
		}
		return 0;

}

void CClientOrder::Prepare_KritMhd () //LAC-84
{

 		 StndAufp.sqlin ((long *) &dlieferdat, 2, 0);
 		 StndAufp.sqlout ((double *) &stnd_aufp.a, 3, 0);
         DbClass.sqlout ((long *) &CronBest.cron_best.mhd, 2, 0);
         DbClass.sqlout ((long *) &CronBest.cron_best.me, 3, 0);

         cursor_kritmhdk = DbClass.sqlcursor ("select a_bas.a,cron_best.mhd, sum(cron_best.me) " 
			            " from a_bas,cron_best where cron_best.a = a_bas.a " 
						" and cron_best.mhd < ( ? + a_bas.restlaufzeit) and cron_best.mhd <> 0 "
						" group by a_bas.a, cron_best.mhd order by  cron_best.mhd,a_bas.a ");

}

int CClientOrder::Read_KritMhd ()  //Hier: kritische MHS-s holen 
{
	int i = 0;
	int dsqlstatusk = 0;
	int dsqlstatus = 100;
	int posi;
	int save_mhd;
	BOOL posgefunden  = FALSE;
	AUFPS *Aufps;

        i = 0;

		posi = 10;

        dsqlstatusk = StndAufp.sqlopen (cursor_kritmhdk); 
        dsqlstatusk = StndAufp.sqlfetch (cursor_kritmhdk); 
		while (dsqlstatusk == 0)
        {
				 save_mhd = CronBest.cron_best.mhd;
				 Aufps = new AUFPS;   
				memset (Aufps, 0, sizeof (AUFPS));
				sprintf (Aufps->a, "%13.0lf", stnd_aufp.a); 
				dsqlstatus = lese_a_bas (stnd_aufp.a);
				if (dsqlstatus == 0) 
				{
		 			if (kun.min_best == 1)  
					{
						if (_a_bas.min_bestellmenge > 0 || _a_bas.staffelung > 0)
						{
							sprintf (Aufps->min_best,    "%.1lf/%.1lf",  _a_bas.min_bestellmenge, _a_bas.staffelung);
							Aufps->min_bestellmenge = _a_bas.min_bestellmenge; 
							Aufps->sg_min_bestellmenge = sg.min_bestellmenge; 
							Aufps->sg_staffel = sg.staffelung; 
							Aufps->sg = sg.sg; 
						}
						if (sg.min_bestellmenge > 0.0)
						{
							sprintf (Aufps->grp_min_best,   "%.1lf/%.1lf/%s",  sg.min_bestellmenge, sg.staffelung, clipped(sg.sg_bezk)); 
						} else  strcpy (Aufps->grp_min_best,"");
					}
					strcpy (Aufps->a_bz1, _a_bas.a_bz1); 
					sprintf (Aufps->posi, "%d", posi);
					Aufps->hwg = _a_bas.hwg; 
					strcpy (Aufps->a_bz1, _a_bas.a_bz1); 
					sprintf (Aufps->posi, "%d", posi);
					Aufps->hwg = _a_bas.hwg; 
					dlong_to_asc (CronBest.cron_best.mhd, Aufps->mhd );
					Aufps->KritMHD = TRUE;
					FillRow (Aufps);
					KritMhdAll->Add (Aufps);
					i ++;
					posi += 10;
					posgefunden = TRUE;
				} 
				else
				{
							strcpy (Aufps->a_bz1, " "); 
				}
		        dsqlstatusk = StndAufp.sqlfetch (cursor_kritmhdk); 
        }

		return 0;

}


void CClientOrder::Prepare_Bestellvorschlag () 
{

		 DbClass.sqlin ((long *) &kun_nr, 2, 0);
         DbClass.sqlout ((double *) &best_vorschlag.a, 3, 0);
         DbClass.sqlout ((double *) &best_vorschlag.me, 3, 0);
         DbClass.sqlout ((char *) best_vorschlag.kz, 0, 2);

         cursor_bestellvorschlag = DbClass.sqlcursor ("select a,me,kz  " 
			            " from best_vorschlag where kun = ? order by kz,a "); 


		 DbClass.sqlin ((long *) &kun_nr, 2, 0);
         DbClass.sqlout ((double *) &best_vorschlag.a, 3, 0);
         DbClass.sqlout ((double *) &best_vorschlag.me, 3, 0);

         cursor_bestellvorschlagges = DbClass.sqlcursor ("select a,avg(me)  " 
			            " from best_vorschlag where kun = ? group by a order by a "); 

}


int CClientOrder::Read_Bestellvorschlag ()  
{
	int i = 0;
	int dsqlstatusk = 0;
	int dsqlstatus = 100;
	int posi;
	BOOL posgefunden  = FALSE;
	BOOL flg_uebernahme = FALSE;
	AUFPS *Aufps;

        i = 0;

		posi = 10;

		if (dbestVorschlag == 'G')
		{
			dsqlstatusk = StndAufp.sqlopen (cursor_bestellvorschlagges); 
			dsqlstatusk = StndAufp.sqlfetch (cursor_bestellvorschlagges); 
		}
		else
		{
			dsqlstatusk = StndAufp.sqlopen (cursor_bestellvorschlag); 
			dsqlstatusk = StndAufp.sqlfetch (cursor_bestellvorschlag); 
		}

		while (dsqlstatusk == 0)
        {
			if (dbestVorschlag == 'G' || dbestVorschlag == best_vorschlag.kz[0])
			{
				 Aufps = new AUFPS;   
				memset (Aufps, 0, sizeof (AUFPS));
				sprintf (Aufps->a, "%13.0lf", best_vorschlag.a); 
				stnd_aufp.a = best_vorschlag.a ; //LAC-180
				dsqlstatus = lese_a_bas (best_vorschlag.a);
				if (dsqlstatus == 0) 
				{
		 			if (kun.min_best == 1)  
					{
						if (_a_bas.min_bestellmenge > 0 || _a_bas.staffelung > 0)
						{
							sprintf (Aufps->min_best,    "%.1lf/%.1lf",  _a_bas.min_bestellmenge, _a_bas.staffelung);
							Aufps->min_bestellmenge = _a_bas.min_bestellmenge; 
							Aufps->sg_min_bestellmenge = sg.min_bestellmenge; 
							Aufps->sg_staffel = sg.staffelung; 
							Aufps->sg = sg.sg; 
						}
						if (sg.min_bestellmenge > 0.0)
						{
							sprintf (Aufps->grp_min_best,   "%.1lf/%.1lf/%s",  sg.min_bestellmenge, sg.staffelung, clipped(sg.sg_bezk)); 
						} else  strcpy (Aufps->grp_min_best,"");
					}
					strcpy (Aufps->a_bz1, _a_bas.a_bz1); 
					sprintf (Aufps->posi, "%d", posi);
					Aufps->hwg = _a_bas.hwg; 
					strcpy (Aufps->a_bz1, _a_bas.a_bz1); 
					sprintf (Aufps->posi, "%d", posi);
					Aufps->hwg = _a_bas.hwg; 
					if (flg_uebernahme) //LAC-132
					{
						sprintf (Aufps->auf_me, "%.2lf", best_vorschlag.me);    
						sprintf (Aufps->lief_me, "%.2lf", best_vorschlag.me);  
					}
					sprintf (Aufps->soll_me, "%.2lf", best_vorschlag.me); 
					FillRow (Aufps);
				    StndAll->Add (Aufps);

					if (flg_uebernahme) 
					{
						ListHandler->Update (0, Aufps);
					}
					i ++;
					posi += 10;
					posgefunden = TRUE;
				} 
				else
				{
							strcpy (Aufps->a_bz1, " "); 
				}
			}
			if (dbestVorschlag == 'G')
			{
			   dsqlstatusk = StndAufp.sqlfetch (cursor_bestellvorschlagges); 
			}
			else
			{
			   dsqlstatusk = StndAufp.sqlfetch (cursor_bestellvorschlag); 
			}
		}

		return 0;

}




void CClientOrder::Prepare_Kommldat () //LAC-116 nicht mehr aktiv
{
		StndAufp.sqlin ((short *) &mdn, 1, 0);
		StndAufp.sqlin ((short *) &fil, 1, 0);
		StndAufp.sqlin ((long *) &kun_nr, 2, 0);
		StndAufp.sqlin ((short *) &kun_fil, 1, 0);
		StndAufp.sqlin ((long *) &dlieferdat, 2, 0);
		StndAufp.sqlin ((long *) &akt_auf, 2, 0);

		StndAufp.sqlout ((short *) &stnd_aufp.mdn, 1, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.fil, 1, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.kun, 2, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.kun_fil, 1, 0);
		StndAufp.sqlout ((long *) &dls, 2, 0);
		StndAufp.sqlout ((long *) &lieferdat, 2, 0);

           		cursor_kommldatk = StndAufp.sqlcursor ("select lsk.mdn, lsk.fil, "
			                        "lsk.kun, lsk.kun_fil, lsk.ls, lsk.lieferdat "
			                        "from lsk "
									"where mdn = ? "
									"and   fil = ? "  
									"and   kun = ? "  
									"and   kun_fil = ? and lieferdat = ? and auf <> ? "
									"order by ls desc");  

				

		StndAufp.sqlin ((short *) &mdn, 1, 0);
		StndAufp.sqlin ((short *) &fil, 1, 0);
		StndAufp.sqlin ((long *) &dls, 2, 0);

		StndAufp.sqlout ((long *) &stnd_aufp.posi, 2, 0);
		StndAufp.sqlout ((double *) &stnd_aufp.a, 3, 0);
		StndAufp.sqlout ((char *)  &auf_me_bz, 0, 6);
		StndAufp.sqlout ((double *) &vk_pr, 3, 0);
		StndAufp.sqlout ((double *) &auf_me, 3, 0);
		StndAufp.sqlout ((double *) &lief_me, 3, 0);
		StndAufp.sqlout ((double *) &ddiff, 3, 0);
           		cursor_kommldatp = StndAufp.sqlcursor ("select lsp.posi, lsp.a, "
									"lsp.auf_me_bz ,lsp.ls_vk_euro, lsp.auf_me , lsp.lief_me , "
									"(auf_me - lief_me) * 100 / auf_me diff "
			                        "from lsp "
									"where lsp.mdn = ? "
									"and   lsp.fil = ? "  
									"and   lsp.ls = ? and lsp.auf_me > 0"  
									"order by diff desc, lsp.a");  

}
void CClientOrder::Prepare_KommldatKW () //LAC-131
{
		StndAufp.sqlin ((long *) &kun_nr, 2, 0);
		StndAufp.sqlin ((short *) &mdn, 1, 0);
		StndAufp.sqlin ((short *) &fil, 1, 0);
		StndAufp.sqlin ((long *) &kun_nr, 2, 0);
		StndAufp.sqlin ((short *) &kun_fil, 1, 0);
		StndAufp.sqlout ((short *) &djr, 1, 0);
		StndAufp.sqlout ((short *) &dkw, 1, 0);
           		cursor_suche_kw = StndAufp.sqlcursor ("select jr,kw "
			                        "from aufchart  "
									"where   kun = ? "  
									"and   auf in (select auf from lsk where mdn = ? and fil = ? and kun = ? and kun_fil = ? and ls_stat > 2 and lieferdat > today -60) "
									"order by jr desc, kw desc");  


		StndAufp.sqlin ((short *) &mdn, 1, 0);
		StndAufp.sqlin ((short *) &fil, 1, 0);
		StndAufp.sqlin ((long *) &kun_nr, 2, 0);
		StndAufp.sqlin ((short *) &kun_fil, 1, 0);
		StndAufp.sqlin ((long *) &kun_nr, 2, 0);
		StndAufp.sqlin ((short *) &djr, 1, 0);
		StndAufp.sqlin ((short *) &dkw, 1, 0);

		StndAufp.sqlout ((double *) &stnd_aufp.a, 3, 0);
		StndAufp.sqlout ((char *)  &auf_me_bz, 0, 6);
		StndAufp.sqlout ((double *) &auf_me, 3, 0);
		StndAufp.sqlout ((double *) &lief_me, 3, 0);
		StndAufp.sqlout ((double *) &ddiff, 3, 0);
           		cursor_kommldatp = StndAufp.sqlcursor ("select lsp.a, "
									"lsp.auf_me_bz , sum (lsp.auf_me) , sum(lsp.lief_me) , "
									"(sum (auf_me) - sum(lief_me)) * 100 / sum(auf_me) diff "
			                        "from lsp, lsk "
									"where lsk.mdn = ? "
									"and   lsk.fil = ? "  
									"and   lsk.kun = ? "  
									"and   lsk.kun_fil = ? "  
									"and   lsk.auf in (select auf from aufchart where kun = ? and jr = ? and kw = ?) "
									"and lsp.auf_me > 0"  
									"and lsk.mdn = lsp.mdn and lsk.fil = lsp.fil and lsk.ls = lsp.ls "
									" group by 1,2 "
									"order by diff desc, lsp.a");  

}

int CClientOrder::Read_Kommldat ()  //LAC-116 Hier: Lieferung (zum akt. Datum ) holen   nicht mehr aktiv !!! 
{
	int i = 0;
	int dsqlstatusk = 0;
	int dsqlstatus = 100;
	int posi;
	BOOL posgefunden  = FALSE;
	AUFPS *Aufps;

        i = 0;

        dsqlstatusk = StndAufp.sqlopen (cursor_kommldatk); 
		while (dsqlstatusk == 0 && posgefunden == FALSE)
		{
	        dsqlstatusk = StndAufp.sqlfetch (cursor_kommldatk);
			if (dsqlstatusk != 0) return dsqlstatusk;

	        dsqlstatus = StndAufp.sqlopen (cursor_kommldatp); 
			dsqlstatus = StndAufp.sqlfetch (cursor_kommldatp);
		
			posi = 10;
			while (dsqlstatus == 0)
			{
				 Aufps = new AUFPS;   
				memset (Aufps, 0, sizeof (AUFPS));
				sprintf (Aufps->a, "%13.0lf", stnd_aufp.a); 
				dsqlstatus = lese_a_bas (stnd_aufp.a);
				if (dsqlstatus == 0)
				{
		 			if (kun.min_best == 1)  
					{
						if (_a_bas.min_bestellmenge > 0 || _a_bas.staffelung > 0)
						{
							sprintf (Aufps->min_best,    "%.1lf/%.1lf",  _a_bas.min_bestellmenge, _a_bas.staffelung); //LAC-44
							Aufps->min_bestellmenge = _a_bas.min_bestellmenge; //LAC-104
							Aufps->sg_min_bestellmenge = sg.min_bestellmenge; //LAC-104
							Aufps->sg_staffel = sg.staffelung; //LAC-104
							Aufps->sg = sg.sg; //LAC-104
						}
					    if (sg.min_bestellmenge > 0.0)
						{
							sprintf (Aufps->grp_min_best,    "%.1lf/%.1lf/%s",  sg.min_bestellmenge, sg.staffelung, clipped(sg.sg_bezk)); //LAC-104
						} else  strcpy (Aufps->grp_min_best,"");
					}
					strcpy (Aufps->a_bz1, _a_bas.a_bz1); 
					sprintf (Aufps->posi, "%d", posi);
					Aufps->hwg = _a_bas.hwg; 
					sprintf (Aufps->last_lief_me, "%.2lf", auf_me); 
					sprintf (Aufps->last_lieferung, "%.2lf", lief_me); 
					FillRow (Aufps);
					KommldatAll->Add (Aufps);
					i ++;
					posi += 10;
					posgefunden = TRUE;
				}
				else
				{
							strcpy (Aufps->a_bz1, " "); 
				}
 				dsqlstatus = StndAufp.sqlfetch (cursor_kommldatp); 
			}
		}
		return 0;

}

int CClientOrder::Read_KommldatKW ()  //LAC-131 Hier: Es sollen die kompletten Lieferungen der KW angezeigt werden, in der KW in der die letzte Lieferung liegt
{
	int i = 0;
	int dsqlstatusk = 0;
	int dsqlstatus = 100;
	int posi;
	BOOL posgefunden  = FALSE;
	AUFPS *Aufps;

        i = 0;

        dsqlstatusk = StndAufp.sqlopen (cursor_suche_kw); 
		while (dsqlstatusk == 0 && posgefunden == FALSE)
		{
	        dsqlstatusk = StndAufp.sqlfetch (cursor_suche_kw);
			if (dsqlstatusk != 0) return dsqlstatusk;

	        dsqlstatus = StndAufp.sqlopen (cursor_kommldatp); 
			dsqlstatus = StndAufp.sqlfetch (cursor_kommldatp);
		
			posi = 10;
			while (dsqlstatus == 0)
			{
				 Aufps = new AUFPS;   
				memset (Aufps, 0, sizeof (AUFPS));
				sprintf (Aufps->a, "%13.0lf", stnd_aufp.a); 
				dsqlstatus = lese_a_bas (stnd_aufp.a);
				if (dsqlstatus == 0)
				{
		 			if (kun.min_best == 1)  
					{
						if (_a_bas.min_bestellmenge > 0 || _a_bas.staffelung > 0)
						{
							sprintf (Aufps->min_best,    "%.1lf/%.1lf",  _a_bas.min_bestellmenge, _a_bas.staffelung); //LAC-44
							Aufps->min_bestellmenge = _a_bas.min_bestellmenge; //LAC-104
							Aufps->sg_min_bestellmenge = sg.min_bestellmenge; //LAC-104
							Aufps->sg_staffel = sg.staffelung; //LAC-104
							Aufps->sg = sg.sg; //LAC-104
						}
					    if (sg.min_bestellmenge > 0.0)
						{
							sprintf (Aufps->grp_min_best,    "%.1lf/%.1lf/%s",  sg.min_bestellmenge, sg.staffelung, clipped(sg.sg_bezk)); //LAC-104
						} else  strcpy (Aufps->grp_min_best,"");
					}
					strcpy (Aufps->a_bz1, _a_bas.a_bz1); 
					sprintf (Aufps->posi, "%d", posi);
					Aufps->hwg = _a_bas.hwg; 
					sprintf (Aufps->last_lief_me, "%.2lf", auf_me); 
					sprintf (Aufps->last_lieferung, "%.2lf", lief_me); 
					FillRow (Aufps);
					KommldatAll->Add (Aufps);
					i ++;
					posi += 10;
					posgefunden = TRUE;
				}
				else
				{
							strcpy (Aufps->a_bz1, " "); 
				}
 				dsqlstatus = StndAufp.sqlfetch (cursor_kommldatp); 
			}
		}
		return 0;

}
void CClientOrder::Prepare_HWG ()
{


 		 StndAufp.sqlin ((long *) &dlieferdat, 2, 0);
 		 StndAufp.sqlin ((long *) &dlieferdat, 2, 0);
		 StndAufp.sqlin ((long *) &akt_hwg, 2, 0);
		 StndAufp.sqlin ((long *) &akt_hwg, 2, 0);
		 StndAufp.sqlin ((long *) &akt_hwg, 2, 0);
		 StndAufp.sqlin ((long *) &akt_hwg, 2, 0);
		 StndAufp.sqlin ((long *) &akt_hwg, 2, 0);
		 StndAufp.sqlin ((long *) &akt_hwg, 2, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.mdn, 1, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.fil, 1, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.kun, 2, 0);
		StndAufp.sqlout ((short *) &stnd_aufp.kun_fil, 1, 0);
		StndAufp.sqlout ((long *) &stnd_aufp.posi, 2, 0);
		StndAufp.sqlout ((double *) &stnd_aufp.a, 3, 0);
		StndAufp.sqlout ((short *) &ag, 1, 0);
		StndAufp.sqlout ((short *) &wg, 1, 0);
		StndAufp.sqlout ((char *)  &a_bz1, 0, 25);
         DbClass.sqlout ((long *) &CronBest.cron_best.mhd, 2, 0);
         DbClass.sqlout ((long *) &CronBest.cron_best.dat, 2, 0);


 // LAC-191 and a_bas.vk_gr > -1 herausgenommen

           	cursor_hwg = StndAufp.sqlcursor ("select 1 , 0, 0, 0, 0, a_bas.a,a_bas.ag, a_bas.wg,a_bas.a_bz1,avg(cron_best.mhd), "
											" case when avg(cron_best.mhd) < ? + avg(a_bas.restlaufzeit) then "
											"            ? + avg(a_bas.restlaufzeit) - avg(cron_best.mhd) "
											"  end diff "

											" from a_bas,cron_best where cron_best.a = a_bas.a " 
											" and a_bas.delstatus = 0 and "
											" (a_bas.hwg = ? " 
											"   or "
											"   ( "
											"   a_bas.a in "
											"     ( select a_bas.a from a_bas_erw,a_bas where a_bas.a = a_bas_erw.a and "
											"       ( "
											"		a_bas_erw.shop_wg1 in (select wg from wg where hwg = ?) or "
											"	a_bas_erw.shop_wg2 in (select wg from wg where hwg = ?) or "
											"   a_bas_erw.shop_wg3 in (select wg from wg where hwg = ?) or "
											"	a_bas_erw.shop_wg4 in (select wg from wg where hwg = ?) or "
											"	a_bas_erw.shop_wg5 in (select wg from wg where hwg = ?) "
											"		) "
											"     ) "
											"   ) "
											" ) "
											" and a_bas.ghsperre <> 1 "

											" group by 1,2,3,4,5,6,7,8,9 order by diff desc, a_bas.a ");


/***
	StndAufp.sqlin ((long *) &akt_hwg, 2, 0);

	StndAufp.sqlout ((short *) &stnd_aufp.mdn, 1, 0);
	StndAufp.sqlout ((short *) &stnd_aufp.fil, 1, 0);
	StndAufp.sqlout ((short *) &stnd_aufp.kun, 2, 0);
	StndAufp.sqlout ((short *) &stnd_aufp.kun_fil, 1, 0);
	StndAufp.sqlout ((long *) &stnd_aufp.posi, 2, 0);
	StndAufp.sqlout ((double *) &stnd_aufp.a, 3, 0);
	StndAufp.sqlout ((short *) &ag, 1, 0);
	StndAufp.sqlout ((short *) &wg, 1, 0);
	StndAufp.sqlout ((char *)  &a_bz1, 0, 25);


           	cursor_hwg = StndAufp.sqlcursor ("select 1 , 0, 0, 0, 0, a_bas.a,  "
								" a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from a_bas "
								"where a_bas.delstatus = 0 and a_bas.hwg = ? and a_bas.ghsperre <> 1  and a_bas.vk_gr > -1 "
								"order by a_bas.a");  

********/
	
}


int CClientOrder::Read_HWG (int dhwg, int dwg, int idx)  // Hier: HWG holen 
{
	int i = 0;
	int dsqlstatus = 100;
	int posi;
	AUFPS *Aufps;

        i = 0;
		akt_hwg = dhwg;
		akt_wg = dwg;


      if (akt_wg > 0)
	  {
        dsqlstatus = StndAufp.sqlopen (cursor_hwg); 
        dsqlstatus = StndAufp.sqlfetch (cursor_hwg);
	  }
	  else
	  {
        dsqlstatus = StndAufp.sqlopen (cursor_hwg); 
        dsqlstatus = StndAufp.sqlfetch (cursor_hwg);
	  }


	posi = 10;
        while (dsqlstatus == 0)
        {
			 Aufps = new AUFPS;   
			 memset (Aufps, 0, sizeof (AUFPS));
             sprintf (Aufps->a, "%13.0lf", stnd_aufp.a); 
             dsqlstatus = lese_a_bas (stnd_aufp.a);
             if (dsqlstatus == 0)
             {
                 strcpy (Aufps->a_bz1, _a_bas.a_bz1); 
				 sprintf (Aufps->posi, "%d", posi);
	           //  StndAufp.dbreadfirsta ();
				 Aufps->hwg = _a_bas.hwg; //260312
	 			if (kun.min_best == 1)  
				{
					if (_a_bas.min_bestellmenge > 0 || _a_bas.staffelung > 0)
					{
						sprintf (Aufps->min_best,    "%.1lf/%.1lf",  _a_bas.min_bestellmenge, _a_bas.staffelung); //LAC-44
						Aufps->min_bestellmenge = _a_bas.min_bestellmenge; //LAC-104
						Aufps->sg_min_bestellmenge = sg.min_bestellmenge; //LAC-104
						Aufps->sg_staffel = sg.staffelung; //LAC-104
						Aufps->sg = sg.sg; //LAC-104
					}
				    if (sg.min_bestellmenge > 0.0)
					{
					    sprintf (Aufps->grp_min_best,    "%.1lf/%.1lf/%s",  sg.min_bestellmenge, sg.staffelung, clipped(sg.sg_bezk)); //LAC-104
					} else  strcpy (Aufps->grp_min_best,"");
				}
				dlong_to_asc (CronBest.cron_best.mhd, Aufps->mhd );
				if (CronBest.cron_best.dat > 0) Aufps->KritMHD = TRUE;  
				 FillRow (Aufps);
				if (idx == 1) HWG1All->Add (Aufps);
				if (idx == 2) HWG2All->Add (Aufps);
				if (idx == 3) HWG3All->Add (Aufps);
				if (idx == 4) HWG4All->Add (Aufps);
				if (idx == 5) HWG5All->Add (Aufps);
				if (idx == 6) HWG6All->Add (Aufps);
				if (idx == 7) HWG7All->Add (Aufps);
				if (idx == 8) HWG8All->Add (Aufps);
				if (idx == 9) HWG9All->Add (Aufps);
				if (idx == 10) HWG10All->Add (Aufps);
				if (idx == 11) HWG11All->Add (Aufps);
				if (idx == 12) HWG12All->Add (Aufps);
				if (idx == 13) HWG13All->Add (Aufps);
				if (idx == 14) HWG14All->Add (Aufps);
				if (idx == 15) HWG15All->Add (Aufps);
				if (idx == 16) HWG16All->Add (Aufps);
				i ++;
				posi += 10;
             }
             else
             {
                         strcpy (Aufps->a_bz1, " "); 
             }
			if (akt_wg > 0)
			{
				dsqlstatus = StndAufp.sqlfetch (cursor_hwg); 
			}
			else
			{
				dsqlstatus = StndAufp.sqlfetch (cursor_hwg); 
			}
        }
        return 0;

}



void CClientOrder::FillRow (AUFPS *Aufps)
{
   int dsqlstatus;
   char wert[sizeof (ptabn.ptwert)];
   short sa;
   double pr_ek;
   double pr_vk;
	char clieferdat [12];

   //LAC-9 statt stnd_aufp.kun und stnd_aufp.kun_fil  aufk.kun und aufk.kun_fil als Eingabeparameter beim Preiseholen

	//LAC-180
    if (strlen ((LPSTR) lieferdat.c_str ()) == 0)
	{
			dlong_to_asc (dlieferdat, clieferdat);
	}
	else
	{
		sprintf (clieferdat, "%s%",(LPSTR) lieferdat.c_str ()); 
	}


   if (DllPreise.PriceLib != NULL && 
	DllPreise.preise_holen != NULL)
   {
			  dsqlstatus = (DllPreise.preise_holen) (stnd_aufp.mdn, 
			                           0,
				                       aufk.kun_fil,
					                   aufk.kun,
						               stnd_aufp.a,
							       //    (LPSTR) lieferdat.c_str (),
										clieferdat,   //LAC-180
								       &sa,
									   &pr_ek,
									   &pr_vk);
			  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
   }
   else
   {
				dsqlstatus = WaPreis.preise_holen (stnd_aufp.mdn,
                                      0,
                                      aufk.kun_fil,
                                      aufk.kun,
                                      stnd_aufp.a,
							       //    (LPSTR) lieferdat.c_str (),
										clieferdat,   //LAC-180
                                      &sa,
                                      &pr_ek,
                                      &pr_vk);
   }
   if (pr_ek != 0.0)
   {
	  sprintf (Aufps->auf_vk_pr, "%lf",  pr_ek);
	  sprintf (Aufps->auf_vk_dm, "%lf",  pr_ek);  //LAC-110 auf FillWaehrung()  wird hier erst mal verzichtet 
	  sprintf (Aufps->auf_vk_euro, "%lf",  pr_ek);  //LAC-110 auf FillWaehrung()  wird hier erst mal verzichtet 
	  sprintf (Aufps->sa_kz_sint, "%1d", sa);
   }
   if (pr_vk != 0.0)
   {
      sprintf (Aufps->auf_lad_pr,"%lf", pr_vk);
      sprintf (Aufps->auf_lad_dm,"%lf", pr_vk);   //LAC-110 auf FillWaehrung()  wird hier erst mal verzichtet 
      sprintf (Aufps->auf_lad_euro,"%lf", pr_vk);   //LAC-110 auf FillWaehrung()  wird hier erst mal verzichtet 
	  sprintf (Aufps->sa_kz_sint, "%1d", sa);
   }
   CalcLdPrPrc (Aufps, pr_ek, pr_vk);
   strcpy (Aufps->kond_art, WaPreis.GetKondArt ());
   strcpy (Aufps->kond_art0, WaPreis.GetKondArt ());
   sprintf (Aufps->a_grund, "%4.0lf", WaPreis.GetAGrund ());
   // bei Lackmann nicht n�tig : FillKondArt (Aufps);
   Einh.SetAufEinh (1);
   sprintf (Aufps->aufp_txt, "%ld", 0l);
   if (MustReadLetztBest) GetLastMe (Aufps);  //LAC-9 nur wenn MustReadLetztbest
   memcpy (&CronBest.cron_best, &cron_best_null, sizeof (CRON_BEST));
/*
   CronBest.cron_best.a = stnd_aufp.a;
   dsqlstatus = CronBest.dbreadfirst_a ();
   if (dsqlstatus == 0)
   {
	   sprintf (Aufps->bsd, "%.3lf", CronBest.cron_best.me);
   }
*/

   CronBest.cron_best.a = stnd_aufp.a;
   CronBest.cron_best.pr_kz = (int) BsdPrice;
   dsqlstatus = CronBest.dbreadfirst ();
   if (dsqlstatus == 0)
   {
	   sprintf (Aufps->bsd, "%.3lf", CronBest.cron_best.me);
   }

   CronBest.cron_best.pr_kz = (int) BsdNoPrice;
   dsqlstatus = CronBest.dbreadfirst ();
   if (dsqlstatus == 0)
   {
	   sprintf (Aufps->bsd2, "%.3lf", CronBest.cron_best.me);
   }

   Aufps->a_gew = _a_bas.a_gew;
   Aufps->dr_folge = _a_bas.dr_folge;
   sprintf (wert, "%hd", _a_bas.me_einh);
   sprintf (Aufps->me_einh, "%hd", _a_bas.me_einh);
   dsqlstatus = Ptab.lese_ptab ("me_einh", wert);
   strcpy (Aufps->basis_me_bz, ptabn.ptbezk);

   kumebest.mdn = aufk.mdn;
   kumebest.fil = aufk.fil;
   kumebest.kun = aufk.kun;
   strcpy (kumebest.kun_bran2, kun.kun_bran2);
   kumebest.a = ratod (Aufps->a);
   ReadMeEinh (Aufps);
//060312 A
        switch (_a_bas.a_typ)
        {
             case HNDW :
                     dsqlstatus = HndwClass.lese_a_hndw (_a_bas.a);
                     break;
             case EIG :
                     dsqlstatus = HndwClass.lese_a_eig (_a_bas.a);
                     a_hndw.inh_abverk = a_eig.inh_abverk;
                     break;
             default :
                     break;
        }
	   if (dsqlstatus == 0)
	   {
		   sprintf (Aufps->stk_karton, "%.0lf", a_hndw.inh_abverk);
		   sprintf (Aufps->gew_karton, "%.3lf", a_hndw.inh_abverk * _a_bas.a_gew);
	   }
//060312 E
}

void CClientOrder::FillKondArt (AUFPS *Aufps)
{
	   int dsqlstatus;

       memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));

       strcpy (ptabn.ptitem,"sap_kond_art");
	   strcpy (ptabn.ptwer1, Aufps->kond_art);

	   StndAufp.sqlin ((char *) ptabn.ptitem, 0, 19);
	   StndAufp.sqlin ((char *) ptabn.ptwer1, 0, 9);
	   StndAufp.sqlout ((char *) ptabn.ptwer2, 0, 9);
	   dsqlstatus = StndAufp.sqlcomm ("select ptwer2 from ptabn "
		                             "where ptitem = ? "
									 "and ptwer1 = ?");
       strcpy (Aufps->kond_art0, ptabn.ptwer1);
       strcpy (Aufps->kond_art,  ptabn.ptwer2);
}

void CClientOrder::GetLastMeAuf (AUFPS *Aufps)   //Hier: Tuningbedarf  (warum nicht direkt Join aufk,aufp ?)
/**
Letzte Bestellmenge des Kunden holen.
**/
{
	   double a; 
	   int cursork = -1;
	   int cursorp = -1;
	   long dauf;
	   static double auf_me;
	   static double pr_vk;
	   static double auf_me_ges;
	   static char ldat[12];

	   a = ratod (Aufps->a);
	   auf_me = 0.0;
	   pr_vk = 0.0;
	   auf_me_ges = 0.0;
	   dauf = 0;
	   memset (ldat, 0, sizeof (ldat));


	   //LAC-188   nur ein cursor
	   if (cursork == -1)
	   {
		   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
			DbClass.sqlin ((short *) &aufk.fil, 1, 0);
			DbClass.sqlin ((long *)  &aufk.kun, 2, 0);
			DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
			DbClass.sqlin ((double *)  &a, 3, 0);
			DbClass.sqlout ((long *)  &dauf, 2, 0);
			DbClass.sqlout ((char *)  ldat, 0, 11);
			DbClass.sqlout ((double *) &auf_me_ges, 3, 0);
			DbClass.sqlout ((double *) &pr_vk, 3, 0); 
			cursork = DbClass.sqlcursor ("select aufk.auf, aufk.lieferdat, sum(aufp.auf_me), avg(aufp.auf_vk_euro) from aufk,aufp where aufk.mdn = ? "
		                                                  "and aufk.fil = ? "
														  "and aufk.kun = ? "
														  "and aufk.auf != ? "
														  "and aufp.a = ? "
														  "and aufk.auf = aufp.auf "
														  "and aufk.mdn = aufp.mdn "
														  "and aufk.fil = aufp.fil "

														  "group by 1,2 "
														  "order by aufk.lieferdat desc, "
														  "auf desc");

			DbClass.sqlfetch (cursork) ;
	   }


/* LAC-188 

	   if (cursork == -1)
	   {
		   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		   DbClass.sqlin ((long *)  &aufk.kun, 2, 0);
		   DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
		   DbClass.sqlout ((long *)  &dauf, 2, 0);
		   DbClass.sqlout ((char *)  ldat, 0, 11);
		   cursork = DbClass.sqlcursor ("select auf, lieferdat from aufk where mdn = ? "
															  "and fil = ? "
															  "and kun = ? "
															  "and auf != ? "
															  "order by lieferdat desc, "
															  "auf desc");

		   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		   DbClass.sqlin ((long *)  &dauf, 2, 0);
		   DbClass.sqlin ((double *)  &a, 3, 0);
		   DbClass.sqlout ((double *) &auf_me, 3, 0);
		   DbClass.sqlout ((double *) &pr_vk, 3, 0);
		   cursorp = DbClass.sqlcursor ("select auf_me, auf_vk_euro from aufp "
										"where mdn = ? "
										"and fil = ? "
										"and auf = ? "
										"and a = ?");
	   }
	   auf_me_ges = (double) 0.0;

	   if (DbClass.sqlopen (cursork) != 0)
	   {
		   return;
	   }
       while (DbClass.sqlfetch (cursork) == 0)
	   {
		   if (dauf == 0) break;   //LAC-9
		   if (DbClass.sqlopen (cursorp) != 0) break;
		   while (DbClass.sqlfetch (cursorp) == 0)
		   {
			   auf_me_ges += auf_me;
		   }
		   if (auf_me_ges != (double) 0.0) break;
	   }
	   ************/
	   sprintf (Aufps->last_me, "%.3lf", auf_me_ges);
	   sprintf (Aufps->last_ldat, "%s"  , ldat);
	   sprintf (Aufps->last_pr_vk,"%.3lf"  , pr_vk);
	   DbClass.sqlclose (cursorp);
	   DbClass.sqlclose (cursork);

}


void CClientOrder::GetLastMe (AUFPS *Aufps)
/**
Letzte Bestellmenge des Kunden holen.
**/
{
	   double a; 
	   long auf;
	   double auf_me_ges;
	   double auf_vk_pr;

	   if (!LastFromAufKun)
	   {
			GetLastMeAuf (Aufps);
			return;
	   }

	   a = ratod (Aufps->a);

	   auf_me_ges = 0;
	   strcpy (Aufps->last_ldat, "");
	   aufkun.auf_me = (double) 0.0;
	   aufkun.mdn     = aufk.mdn;
	   aufkun.fil     = aufk.fil;
	   aufkun.kun     = aufk.kun;
	   aufkun.a       = a;
	   aufkun.kun_fil = aufk.kun_fil;

	   AufKun.dbreadlast ();

	   auf = aufkun.auf;
	   auf_me_ges = aufkun.auf_me;
	   auf_vk_pr = aufkun.auf_vk_pr;
	   dlong_to_asc (aufkun.lieferdat, Aufps->last_ldat );
	   while (AufKun.dbreadnextlast () == 0)
	   {
		   if (auf != aufkun.auf) break;
		   auf_me_ges += aufkun.auf_me;
		   auf_vk_pr = aufkun.auf_vk_pr;
	   }
	   if (auf_me_ges == (double) 0.0)
	   {
		   GetLastMeAuf (Aufps);
	   }
	   else
	   {
           sprintf (Aufps->last_me,     "%.3lf", auf_me_ges);
           sprintf (Aufps->last_pr_vk, "%.4lf", auf_vk_pr);
	   }
}

void CClientOrder::ReadMeEinh (AUFPS *Aufps)
/**
Mengeneinheiten holen.
**/
{
        int dsqlstatus;
        char ptwert [5];

        KEINHEIT keinheit;

        Einh.GetKunEinh (aufk.mdn, aufk.fil, aufk.kun,
                         _a_bas.a, &keinheit);
        strcpy (Aufps->basis_me_bz, keinheit.me_einh_bas_bez);
        strcpy (Aufps->me_bz, keinheit.me_einh_kun_bez);
        sprintf (Aufps->me_einh_kun, "%hd", keinheit.me_einh_kun);
        sprintf (Aufps->me_einh,     "%hd", keinheit.me_einh_bas);
        inh = keinheit.inh;
        sprintf (Aufps->me_einh_kun1, "%hd", keinheit.me_einh1);
        sprintf (Aufps->me_einh_kun2, "%hd", keinheit.me_einh2);
        sprintf (Aufps->me_einh_kun3, "%hd", keinheit.me_einh3);

        sprintf (Aufps->inh1, "%.3lf", keinheit.inh1);
        sprintf (Aufps->inh2, "%.3lf", keinheit.inh2);
        sprintf (Aufps->inh3, "%.3lf", keinheit.inh3);

        return;


        switch (_a_bas.a_typ)
        {
             case enumHndw :
                     dsqlstatus = Hndw.lese_a_hndw (_a_bas.a);
                     break;
             case enumEig :
                     dsqlstatus = Hndw.lese_a_eig (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig.me_einh_ek;
                     break;
             case enumEigDiv :
                     dsqlstatus = Hndw.lese_a_eig_div (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                     break;
             case enumDienst :
             case enumLeih :
                     a_hndw.me_einh_kun = _a_bas.me_einh;
                     break;
             default :
                     a_hndw.me_einh_kun = _a_bas.me_einh;
                     break;
        }

		Aufps->a_typ = _a_bas.a_typ;

        sprintf (ptwert, "%hd", _a_bas.me_einh);
        if (Ptab.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (Aufps->basis_me_bz, ptabn.ptbezk);
        }

        if (dsqlstatus) return;

        sprintf (ptwert, "%hd", a_hndw.me_einh_kun);
        if (Ptab.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (Aufps->me_bz, ptabn.ptbezk);
        }
}


void CClientOrder::CalcLdPrPrc (AUFPS *aufps, double vk_pr, double ld_pr)
{
	    double auf_lad_pr_prc = 0.0;
	    if (vk_pr != 0.0 && ld_pr != 0.0)
		{
			auf_lad_pr_prc = 100 - (vk_pr * 100 / ld_pr);	
		}
		sprintf (aufps->auf_lad_pr_prc,"%lf", auf_lad_pr_prc);
		GrpPrice.SetAKey (ratod (aufps->a));
//		GrpPrice.CalculateLdPr (vk_pr, aufps.marge, sizeof (aufps.marge), aufps->auf_lad_pr, sizeof (aufps.auf_lad_pr));
		GrpPrice.CalculateLdPr (vk_pr, aufps->marge, sizeof (aufps->marge), aufps->auf_lad_pr0, sizeof (aufps->auf_lad_pr0));
}


void CClientOrder::WriteAufps ()
{
	string msOutFileName;
	string msLine;
	fstream mfspOut;
	AUFPS **it;
	AUFPS *Aufps;
	CString Etc;
	CString FileName;

	Etc = getenv ("BWSETC");
	FileName.Format ("%s\\stnd", Etc.GetBuffer ());
	CreateDirectory (FileName.GetBuffer (), NULL);
	FileName += "\\";
	FileName += KunBran;
	msOutFileName = FileName.GetBuffer ();
	mfspOut.open (msOutFileName.c_str (), ios_base::out | ios_base::binary);
	if (!mfspOut.is_open ())
	{
		return;
	}
    StndAll->Start ();
	while ((it = StndAll->GetNext ()) != NULL)
	{
		Aufps = *it;
		mfspOut.write ((const char *) Aufps, sizeof (AUFPS));
	}
	mfspOut.close ();
}

BOOL CClientOrder::ReadAufps ()
{
	BOOL ret = FALSE;
	string msInFileName;
	string msLine;
	fstream mfspIn;
	AUFPS *Aufps;
	CString Etc;
	CString FileName;

	if (RD_OPTIMIZE == FromFile)
	{
		Etc = getenv ("BWSETC");
		FileName.Format ("%s\\stnd", Etc.GetBuffer ());
		CreateDirectory (FileName.GetBuffer (), NULL);
		FileName += "\\";
		FileName += KunBran;
		msInFileName = FileName.GetBuffer ();
		mfspIn.open (msInFileName.c_str (), ios_base::in | ios_base::binary);
		if (mfspIn.is_open ())
		{
			StndAll->DestroyElements ();
			StndAll->Destroy ();
			Aufps = new AUFPS;
			mfspIn.read ((char *) Aufps, sizeof (AUFPS));
			while (!mfspIn.eof ())
			{
				StndAll->Add (Aufps);
				Aufps = new AUFPS;
				mfspIn.read ((char *) Aufps, sizeof (AUFPS));
			}
			delete Aufps;
			mfspIn.close ();
			ret = TRUE;
		}
	}
	return ret;
}
