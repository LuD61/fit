#include <windows.h>
#include "wmaskc.h"
#include "searchstdfilk.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"
#include "ptab.h"


struct SSTDAUFK *SEARCHSTDFILK::skuntab = NULL;
struct SSTDAUFK SEARCHSTDFILK::skun;
int SEARCHSTDFILK::idx = -1;
long SEARCHSTDFILK::kunanz;
CHQEX *SEARCHSTDFILK::Query = NULL;
DB_CLASS SEARCHSTDFILK::DbClass; 
HINSTANCE SEARCHSTDFILK::hMainInst;
HWND SEARCHSTDFILK::hMainWindow;
HWND SEARCHSTDFILK::awin;
int SEARCHSTDFILK::SearchField = 1;
int SEARCHSTDFILK::sowo_tag = 1; 
int SEARCHSTDFILK::solfd = 1; 
int SEARCHSTDFILK::sobez = 1; 
short SEARCHSTDFILK::mdn = 0; 
short SEARCHSTDFILK::fil = 0; 


ITEM SEARCHSTDFILK::uwo_tag  ("wo_tag",    "Wochentag",     "", 0);  
ITEM SEARCHSTDFILK::ulfd     ("lfd" ,      "Nummer",        "", 0);  
ITEM SEARCHSTDFILK::ubez     ("bez",       "Bezeichnung",   "", 0);  


field SEARCHSTDFILK::_UbForm[] = {
&uwo_tag,     20, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&ulfd,        10, 0, 0, 20 , NULL, "", BUTTON, 0, 0, 0,     
&ubez,        20, 0, 0, 30 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHSTDFILK::UbForm = {3, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

ITEM SEARCHSTDFILK::iwo_tag  ("wo_tag",           skun.wo_tag_bez,  "", 0);  
ITEM SEARCHSTDFILK::ilfd     ("lfd",              skun.lfd,         "", 0);  
ITEM SEARCHSTDFILK::ibez     ("bez",              skun.bez,         "", 0);  


field SEARCHSTDFILK::_DataForm[] = {
&iwo_tag,     18, 0, 0,  1,  NULL, "",    DISPLAYONLY, 0, 0, 0,     
&ilfd,         4, 0, 0, 23 , NULL, "%4d", DISPLAYONLY, 0, 0, 0,     
&ibez,        18, 0, 0, 31 , NULL, "",    DISPLAYONLY, 0, 0, 0,     
};

form SEARCHSTDFILK::DataForm = {3, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHSTDFILK::iline ("", "1", "", 0);

field SEARCHSTDFILK::_LineForm[] = {
&iline,       1, 0, 0, 20 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 30 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHSTDFILK::LineForm = {2, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHSTDFILK::query = NULL;

int SEARCHSTDFILK::sortwo_tag (const void *elem1, const void *elem2)
{
	      struct SSTDAUFK *el1; 
	      struct SSTDAUFK *el2; 

		  el1 = (struct SSTDAUFK *) elem1;
		  el2 = (struct SSTDAUFK *) elem2;
          clipped (el1->wo_tag);
          clipped (el2->wo_tag);
          if (strlen (el1->wo_tag) == 0 &&
              strlen (el2->wo_tag) == 0)
          {
              return 0;
          }
          if (strlen (el1->wo_tag) == 0)
          {
              return -1;
          }
          if (strlen (el2->wo_tag) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->wo_tag,el2->wo_tag) * sowo_tag);
}

void SEARCHSTDFILK::SortWo_tag (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SSTDAUFK),
				   sortwo_tag);
       sowo_tag *= -1;
}

int SEARCHSTDFILK::sortlfd (const void *elem1, const void *elem2)
{
	      struct SSTDAUFK *el1; 
	      struct SSTDAUFK *el2; 

		  el1 = (struct SSTDAUFK *) elem1;
		  el2 = (struct SSTDAUFK *) elem2;
          return (atoi (el1->lfd) - atoi (el2->lfd)) * solfd;
}

void SEARCHSTDFILK::SortLfd (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SSTDAUFK),
				   sortlfd);
       solfd *= -1;
}

int SEARCHSTDFILK::sortbez (const void *elem1, const void *elem2)
{
	      struct SSTDAUFK *el1; 
	      struct SSTDAUFK *el2; 

		  el1 = (struct SSTDAUFK *) elem1;
		  el2 = (struct SSTDAUFK *) elem2;
          clipped (el1->bez);
          clipped (el2->bez);
          if (strlen (el1->bez) == 0 &&
              strlen (el2->bez) == 0)
          {
              return 0;
          }
          if (strlen (el1->bez) == 0)
          {
              return -1;
          }
          if (strlen (el2->bez) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->bez,el2->bez) * sobez);
}

void SEARCHSTDFILK::SortBez (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SSTDAUFK),
				   sortbez);
       sobez *= -1;
}

void SEARCHSTDFILK::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortWo_tag (hWnd);
                  SearchField = 1;
                  break;
              case 1 :
                  SortLfd (hWnd);
                  SearchField = 2;
                  break;
              case 2 :
                  SortBez (hWnd);
                  SearchField = 3;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHSTDFILK::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHSTDFILK::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHSTDFILK::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHSTDFILK::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHSTDFILK::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < kunanz; i ++)
       {
		  memcpy (&skun, &skuntab[i],  sizeof (struct SSTDAUFK));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHSTDFILK::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHSTDFILK::TestRec (short mdn, short fil)
{
 	   char buffer [512];

       sprintf (buffer, "select * "
 	 			        "from stdfilaufk "
                        "where mdn = %hd and fil = %hd", mdn, fil); 
       if (query != NULL && strlen (query) > 0)
       {
                  strcat (buffer, " ");
                  strcat (buffer, query);
       }
       return DbClass.sqlcomm (buffer);  
}

int SEARCHSTDFILK::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
 	   char buffer [512];
	  int cursor;
  	  int i;
      WMESS Wmess;
	  PTAB_CLASS *Ptab = new PTAB_CLASS ();


	  if (skuntab) 
	  {
           delete skuntab;
           skuntab = NULL;
	  }


      idx = -1;
	  kunanz = 0;
      SearchField = 1;
//      Query->SetSBuff ("");
      Query->SetSBuff (NULL);
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (kunanz == 0) kunanz = 0x10000;

	  skuntab = new struct SSTDAUFK [kunanz];
      if (squery == NULL || 
          strlen (squery) == 0)
      {
             sprintf (buffer, "select wo_tag, lfd, bez "
	 			       "from stdfilaufk "
                       "where mdn = %hd and fil = %hd", 
                       mdn, fil); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
              strcat (buffer, "  order by wo_tag, lfd");
              DbClass.sqlout ((char *) skun.wo_tag, 0, 3);
              DbClass.sqlout ((char *) skun.lfd, 0, 9);
              DbClass.sqlout ((char *) skun.bez, 0, 17);

              cursor = DbClass.sqlcursor (buffer);
      }
      else
      {
             sprintf (buffer, "select wo_tag, lfd, bez "
	 			       "from stdfilaufk "
                       "where mdn = %hd and fil = %hd"
                       "and wo_tag matches \"%s*\"", mdn, fil, squery); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
              DbClass.sqlout ((char *) skun.wo_tag, 0, 3);
              DbClass.sqlout ((char *) skun.lfd, 0, 9);
              DbClass.sqlout ((char *) skun.bez, 0, 17);

              cursor = DbClass.sqlcursor (buffer);
      }
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
          if (Ptab->lese_ptab ("wo_tag", skun.wo_tag) == 0)
		  {
			  strcpy (skun.wo_tag_bez, ptabn.ptbez);
		  }
		  else
		  {
			  strcpy (skun.wo_tag_bez, skun.wo_tag);
		  }
		  memcpy (&skuntab[i], &skun, sizeof (struct SSTDAUFK));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      kunanz = i;
	  DbClass.sqlclose (cursor);
	  delete Ptab;

      sowo_tag = 1;
/*
      if (kunanz > 1)
      {
             SortAdr_nam1 (Query->GethWnd ());
      }

      UpdateList ();
*/
/*
      if (kunanz > 1)
      {
             Query->SetSortRow (0, TRUE);
      }
*/

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHSTDFILK::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < kunanz; i ++)
      {
		  memcpy (&skun, &skuntab[i],  sizeof (struct SSTDAUFK));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHSTDFILK::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHSTDFILK::Search (short mdn, short fil, char *squery)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;
      BOOL doret = FALSE; 

      if (mdn == 0)
      {
          disp_mess ("Mandant 0 ist nicht erlaubt", 2);
          syskey = KEY5;
          return;
      }
      this->mdn = mdn;
      this->fil = fil;
      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
//      EnableWindow (hMainWindow, FALSE);
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
//               Query = new CHQEX (cx, cy);
               Query = new CHQEX (cx, cy, "Wochentag", squery);
        }
        else
        {
//               Query = new CHQEX (cx, cy);
               Query = new CHQEX (cx, cy, "Wochentag", squery);
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
  //      if (query != NULL && strlen (query) > 0)
        {
              Read (squery);
              if (kunanz == 0)
              {
                  doret = TRUE;
              }
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
      if (doret == FALSE)
      {
          Query->ProcessMessages ();
          idx = Query->GetSel ();
      }
      else
      {
          idx = -1;
      }
      query = NULL;
      EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (skuntab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&skun, &skuntab[idx], sizeof (skun));
      SetSortProc (NULL);
      if (skuntab != NULL)
      {
          Query->SavefWork ();
      }
}


SSTDAUFK *SEARCHSTDFILK::GetNextSkun (void)
{
      if (idx == -1) return NULL;
      if (idx < kunanz - 1) 
      {
             idx ++;
      }
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}


SSTDAUFK *SEARCHSTDFILK::GetPriorSkun (void)
{
      if (idx == -1) return NULL;
      if (idx > 0) 
      {
             idx --;
      }
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}

SSTDAUFK *SEARCHSTDFILK::GetFirstSkun (void)
{
      if (idx == -1) return NULL;
      idx = 0;
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}

SSTDAUFK *SEARCHSTDFILK::GetLastSkun (void)
{
      if (idx == -1) return NULL;
      idx = kunanz - 1;
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}



