// ClientOrderCom.cpp: Implementierung der Klasse CClientOrderCom.
//
//////////////////////////////////////////////////////////////////////

#include <objbase.h>
#include "ClientOrderCom.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CClientOrderCom::CClientOrderCom() : CClientOrder ()
{
	Window = FALSE;
	StopWorking = FALSE;
	iCount = 0;
}

CClientOrderCom::~CClientOrderCom()
{
	CoUninitialize ();
}

BOOL CClientOrderCom::StartServer ()
{
//	MSG msg;

//	HRESULT hr = CoInitializeEx (NULL, COINIT_APARTMENTTHREADED); 
	HRESULT hr = CoInitialize (NULL); 
	if (FAILED (hr))
	if (hr != S_OK)
	{
//		cout << "ConinitializeEx failed." << endl;
	}

	com.Name = (_T("FitObjects.StndOrder"));
	StndOrder = com.CreateObject ();

	if (StndOrder.Failed ())
	{
        return FALSE;
	}

	iCount ++;
	try
	{
		StopWorking = FALSE;
        client = StndOrder.RegisterClient ();
		while (client == 0)
		{
			  Sleep (1000);
			  client = StndOrder.RegisterClient ();
		}
		StndOrder.PutSortMode (SortMode);
		StndOrder.PutRdOptimize (RdOptimize);
		StndOrder.SetKeys (mdn, fil, kun_fil, kun_nr, 0, client);
		if (Window)
		{
			StndOrder.SetWindow ();
		}

	}
	catch (_com_error &e)
	{
		CString Error = (char *) e.ErrorMessage ();
        return FALSE;
	}
	return TRUE;
}


int CClientOrderCom::UnregisterServer ()
{
		StndOrder.UnregisterClient (client);
		if (iCount > 0)
		{
			iCount --;
			if (iCount == 0)
			{
				com.Release ();
			}
		}
		return 0;
}


void CClientOrderCom::Load ()
{
	BOOL MustLoad = FALSE;

	if (StopWorking)
	{
		return;
	}
	if (StndOrder.Failed ())
	{
		return;
	}
	if (ListHandler == NULL)
	{
		ListHandler = CListHandler::GetInstance ();
		StndAll = ListHandler->GetStndAll ();
		ListHandler->Init (0);
		MustLoad = TRUE;
		mdn = stnd_aufp.mdn;
		fil = stnd_aufp.fil;
		kun_fil = stnd_aufp.kun_fil;
		kun_nr = stnd_aufp.kun;
		last_kun = stnd_aufp.kun;
	}
	else
	{
		if (mdn != stnd_aufp.mdn ||
			fil != stnd_aufp.fil ||
			kun_fil != stnd_aufp.kun_fil ||
			last_kun != stnd_aufp.kun)
		{
			ListHandler->Init (0);
			MustLoad = TRUE;
		    mdn = stnd_aufp.mdn;
			fil = stnd_aufp.fil;
			kun_fil = stnd_aufp.kun_fil;
			kun_nr = stnd_aufp.kun;
			last_kun = stnd_aufp.kun;
		}
		else
		{
//LAC-9			ListHandler->GetStndValue ()->DestroyElements ();
//LAC-9			ListHandler->GetStndValue ()->Destroy ();
//LAC-9			ListHandler->InitAufMe ();
		}
	}
	if (RefreshRdOptimize)
	{
		MustLoad = TRUE;
	}
	if (MustLoad)
	{
		ListHandler->Init (0);
		StndOrder.Load (client);
		int i = 0;
		if (!StopWorking)
		{
			CAufps = StndOrder.GetFirstRecord (client);
		}
		while (CAufps != NULL && !StopWorking)
		{
			if (CAufps->GetEndOfRecords ())
			{
				break;
			}
			AUFPS *Aufps = new AUFPS;
			memset (Aufps, 0, sizeof (AUFPS));
			sprintf (Aufps->posi, "%ld", CAufps->GetPosi ());
			sprintf (Aufps->a, "%13.0lf", CAufps->GetA ());
			CAufps->GetABz1 (Aufps->a_bz1, sizeof (Aufps->a_bz1));
			sprintf (Aufps->auf_vk_pr, "%.4lf", CAufps->GetAufVkPr ());
			sprintf (Aufps->auf_lad_pr, "%.2lf", CAufps->GetAufLadPr ());
			sprintf (Aufps->auf_lad_pr0, "%.2lf", CAufps->GetAufLadPr0 ());
			sprintf (Aufps->auf_lad_pr_prc, "%.3lf", CAufps->GetAufLadPrPrc ());
			sprintf (Aufps->marge, "%.3lf", CAufps->GetMarge ());
			sprintf (Aufps->sa_kz_sint, "%d", CAufps->GetSaKzSint ());
			Aufps->a_gew = CAufps->GetAGew ();
			Aufps->dr_folge = CAufps->GetDrFolge ();
			sprintf (Aufps->me_einh, "%d", CAufps->GetMeEinh ());
			CAufps->GetSmt (Aufps->smt, sizeof (Aufps->smt));
			CAufps->GetBasisMeBz (Aufps->basis_me_bz, sizeof (Aufps->basis_me_bz));
			sprintf (Aufps->me_einh_kun, "%d", CAufps->GetMeEinhKun ());
			sprintf (Aufps->me_einh_kun1, "%d", CAufps->GetMeEinhKun1 ());
			sprintf (Aufps->me_einh_kun2, "%d", CAufps->GetMeEinhKun2 ());
			sprintf (Aufps->me_einh_kun3, "%d", CAufps->GetMeEinhKun3 ());
			sprintf (Aufps->inh1, "%.3lf", CAufps->GetInh1 ());
			sprintf (Aufps->inh2, "%.3lf", CAufps->GetInh2 ());
			sprintf (Aufps->inh3, "%.3lf", CAufps->GetInh3 ());
			CAufps->GetMeBz (Aufps->me_bz, sizeof (Aufps->me_bz));
			sprintf (Aufps->last_me, "%.3lf", CAufps->GetLastMe ());
			CAufps->GetLastLDat (Aufps->last_ldat, sizeof (Aufps->last_ldat));
			sprintf (Aufps->last_pr_vk, "%.4lf", CAufps->GetLastPrVk ());
			sprintf (Aufps->bsd, "%.3lf",  CAufps->GetBsd ());
			sprintf (Aufps->bsd2, "%.3lf", CAufps->GetBsd2 ());
			StndAll->Add (Aufps);
			CAufps = StndOrder.GetNextRecord (client);
			i ++;
		}
	}
}
