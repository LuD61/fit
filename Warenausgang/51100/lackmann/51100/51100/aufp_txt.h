#ifndef _AUFP_TXT_DEF
#define _AUFP_TXT_DEF

#include "dbclass.h"

struct AUFP_TXT {
   short     mdn;
   short     fil;
   long      auf;
   long      posi;
   long      zei;
   char      txt[61];
};
extern struct AUFP_TXT aufp_txt, aufp_txt_null;

#line 7 "aufp_txt.rh"

class AUFP_TCLASS : public DB_CLASS 
{
       private :
               int del_cursor_posi;
               int del_cursor_auf;
               void prepare (void);
       public :
               AUFP_TCLASS () : DB_CLASS ()
               {
                         del_cursor_posi = -1;
               }
               int dbreadfirst (void);
               int delete_aufpposi (void); 
               int delete_aufpauf (void); 
};
#endif
