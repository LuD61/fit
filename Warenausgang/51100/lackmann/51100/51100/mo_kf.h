#ifndef _MO_LSTXTL_DEF
#define _MO_LSTXTL_DEF

#define MAXLEN 40

class LSTXTLIST
{
            private :
                int mamainmax;
                int mamainmin;
                HWND    hwndTB;
                char *eingabesatz;
                unsigned char *ausgabesatz;
                int zlen;
                int feld_anz;
                int banz;
                PAINTSTRUCT aktpaint;
                char *SwSaetze[30000];
                int PageView;
                unsigned char DlgSatz [20 + MAXLEN + 2];
                FELDER *LstZiel;
                unsigned char *LstSatz;
                int Lstzlen;
                int Lstbanz;
                int SwRecs;
                int AktRow;
                int AktColumn;
                int scrollpos;
                TEXTMETRIC tm;
                char InfoCaption [80];
                int WithMenue;
                int WithToolBar;
                BOOL ListAktiv;
                HWND hMainWindow;

             public :
                static void SetMax0 (int);
                static void SetMin0 (int);
                LSTXTLIST ();
                void SetMenue (int with)
                {
                    WithMenue = with;
                }

                void SetToolMenue (int with)
                {
                    WithToolBar = with;
                }

                void SetMax (void)
                {
                    mamainmax = 1;
                    SetMax0 (mamainmax);
                }

                void SetMin (void)
                {
                    mamainmin = 1;
                    SetMin0 (mamainmin);
                }

                void InitMax (void)
                {
                    mamainmax = 0;
                    SetMax0 (mamainmax);
                }

                void InitMin (void)
                {
                    mamainmin = 0;
                    SetMin0 (mamainmin);
                }

                BOOL IsListAktiv (void)
                {
                    return ListAktiv;
                }

                void SethMainWindow (HWND hMainWindow)
                {
                    this->hMainWindow = hMainWindow;
                }

                HWND GetMamain1 (void);
                static int TestAppend (void);
                static int DeleteLine (void);
                static int InsertLine (void);
                static int AppendLine (void);
                static void WritePos (int);
                static int WriteAllPos (void);
                static int dokey5 (void);
                static int WriteRow (void);
                static int TestRow (void);
                static void SaveAuf (void);
                static void SetAuf (void);
                static void RestoreAuf (void);
                static int Schirm (void);
                static int SetRowItem (void);

                void InitSwSaetze (void);
                int ToMemory (int pos);
                void SetStringEnd (char *, int);
                void uebertragen (void);
                void ShowDB (void);
                void ReadDB (void);
                long EnterNr (long);
                void EnterAufp (char *, long);
                void DestroyWindows (void);
                void DestroyMainWindow (void);
                HWND CreateMainWindow (void);
                HWND CreateEnter (void);
                void MoveMamain1 (void);
                void MoveeWindow (void);
                void MaximizeMamain1 ();

                void SetChAttr (int);
                void SetFieldAttr (char *, int);
                void SetRecHeight (void);

                void SethwndTB (HWND);
                void SetTextMetric (TEXTMETRIC *);
                void SetLineRow (int);
                void SetListLines (int); 
                void ChoiseLines (HDC);
                void OnPaint (HWND, UINT, WPARAM, LPARAM);
                void OnSize (HWND, UINT, WPARAM, LPARAM);
                void MoveListWindow (void);
                void BreakList (void);
                void OnHScroll (HWND, UINT, WPARAM, LPARAM);
                void OnVScroll (HWND, UINT, WPARAM, LPARAM);
                void FunkKeys (WPARAM, LPARAM);
                int  GetRecanz (void);
                void SwitchPage0 (int);
                HWND Getmamain2 (void);
                HWND Getmamain3 (void);
                void SetFont (mfont *); 
                void SetListFont (mfont *);
                void ChoiseFont (mfont *);
                void FindString (void);
                void SetLines (int);
                int GetAktRow (void);
                int GetAktRowS (void);
                void SetColors (COLORREF, COLORREF);
                void SetListFocus (void);
 
};
#endif