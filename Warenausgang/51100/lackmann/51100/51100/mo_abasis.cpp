#include "mo_abasis.h"
#include "a_bas.h"

struct BA BaValues, BaValues_null;

BOOL ABASIS::GetGrundPreis (short mdn, short fil, double a, double vk_pr_i, long kun)
/**
Grundpreise holen.
**/
{
        int dsqlstatus;

        dsqlstatus = lese_a_bas (a);
        if (dsqlstatus != 0)
        {
                 return FALSE;
        }

        switch (_a_bas.a_typ)
        {
                case HNDW :
                       return GetHndw (mdn, fil, a, vk_pr_i, kun);
                case EIG :
                       return GetEig (mdn, fil, a, vk_pr_i, kun);
                default :
                       return FALSE;
        }
        return FALSE;
}


BOOL ABASIS::GetHndw (short mdn, short fil, double a, double vk_pr_i, long kun)
/**
Grundpreise aus a_hndw holen.
**/
{
        int dsqlstatus;
        int cursor;

        memcpy (&BaValues, &BaValues_null, sizeof (struct BA));
        BaValues.me_einh = _a_bas.me_einh;
        sqlin ((short *) &mdn, 1, 0);
        sqlin ((short *) &fil, 1, 0);
        sqlin ((short *) &a,   3, 0);
        sqlout ((double *) &BaValues.matob, 3, 0);
        sqlout ((double *) &BaValues.matmb, 3, 0);
        sqlout ((double *) &BaValues.sk, 3, 0);
        sqlout ((double *) &BaValues.sp, 3, 0);
        cursor = sqlcursor ("select pr_ek1, pr_ek1, sk_vollk, sp_vk "
                            "from a_kalkhndw "
                            "where mdn = ? "
                            "and fil = ? "
                            "and a = ?");

        sqlopen (cursor);
        while (dsqlstatus = sqlfetch (cursor) == 100)
        {
               if (fil > 0)
               {
                         fil = 0;
               }
               else if (mdn > 0)
               {
                         mdn = 0;
               }
               else
               {
                         break;
               }
               sqlopen (cursor);
        }
        if (dsqlstatus) return FALSE;
		BaValues.sp = (vk_pr_i / BaValues.sk - 1) * 100;
		if (kun)
		{
			BaValues.rab = GetKunRab (mdn, kun);
		}
		BaValues.sprab = ((vk_pr_i + BaValues.rabwrt) * 
			              (1 - BaValues.rab / 100) / BaValues.sk - 1) * 100;
        return TRUE;
}

       
BOOL ABASIS::GetEig (short imdn, short ifil, double a, double vk_pr_i, long kun)
/**
Grundpreise aus a_hndw holen.
**/
{
        int dsqlstatus;
        int cursor;
		short mdn, fil;

		mdn = imdn;
		fil = ifil;
        memcpy (&BaValues, &BaValues_null, sizeof (struct BA));
        BaValues.me_einh = _a_bas.me_einh;
        sqlin ((short *) &mdn, 1, 0);
        sqlin ((short *) &fil, 1, 0);
        sqlin ((short *) &a,   3, 0);
        sqlout ((double *) &BaValues.matob, 3, 0);
        sqlout ((double *) &BaValues.matmb, 3, 0);
        sqlout ((double *) &BaValues.sk, 3, 0);
        sqlout ((double *) &BaValues.sp, 3, 0);
        cursor = sqlcursor ("select mat_o_b, hk_vollk, sk_vollk, sp_vk "
                            "from a_kalk_eig "
                            "where mdn = ? "
                            "and fil = ? "
                            "and a = ?");

        sqlopen (cursor);
        while (dsqlstatus = sqlfetch (cursor) == 100)
        {
               if (fil > 0)
               {
                         fil = 0;
               }
               else if (mdn > 0)
               {
                         mdn = 0;
               }
               else
               {
                         break;
               }
               sqlopen (cursor);
        }
        if (dsqlstatus) return FALSE;
		BaValues.sp = (vk_pr_i / BaValues.sk - 1) * 100;
		if (kun)
		{
			BaValues.rab = GetKunRab (imdn, kun);
		}

		BaValues.sprab = ((vk_pr_i - BaValues.rabwrt) * 
			             (1 - BaValues.rab / 100) / BaValues.sk - 1) * 100;
        return TRUE;
}


double ABASIS::GetKunRab (short mdn, long kun)
{
	    double smt_rab;
        double rech_rab;
		double rab;
		char rab_schl [5];
		char rab_teil_smt [3];
		char rab_me_einh [3];

		sqlin ((short *) &mdn, 1, 0);
		sqlin ((long *)  &kun, 2, 0);
		sqlout ((char *) rab_schl, 0, 5);
        if (sqlcomm ("select rab_schl "
			         "from kun where mdn = ? and kun = ?") == 100)
		{
			return 0.0;
		}

		if (strcmp (rab_schl, "0") <= 0) 
		{
			return 0.0;
		}

		sqlin ((short *) &mdn, 1, 0);
        sqlin ((char *) rab_schl, 0, 5);
		sqlout ((double *) &smt_rab, 3, 0); 
		sqlout ((double *) &rech_rab, 3, 0); 
		sqlout ((char *) rab_teil_smt, 0, 3); 
		sqlout ((char *) rab_me_einh, 0, 3); 
		rab = smt_rab = rech_rab = 0.0;
        if (sqlcomm 
			("select smt_rab, rech_rab, rab_teil_smt, rab_me_einh "
			 "from rab_stufek where mdn = ? and rab_nr = ?") == 100)
		{
			return 0.0;
		}

		rab = rech_rab;
		if (GetPosRab (mdn, rab_schl, &rab) == FALSE)
		{
		           rab = smt_rab + rab - smt_rab*rab / 100;
		}
		GetSmtRab (mdn, rab_schl, &rab);
		GetMeEinhRab (mdn, rab_schl, &rab);
		BaValues.rabwrt = 0.0;
        GetSmtRabWrt (mdn, rab_schl, &BaValues.rabwrt);
        GetMeEinhRabWrt (mdn, rab_schl, &BaValues.rabwrt);
		return rab;
}
        
double ABASIS::GetKunRabSchl (short mdn, char *rab_schl)
{
	    double smt_rab;
        double rech_rab;
		double rab;
		char rab_teil_smt [3];
		char rab_me_einh [3];

		if (strcmp (rab_schl, "0") <= 0) 
		{
			return 0.0;
		}

		sqlin ((short *) &mdn, 1, 0);
        sqlin ((char *) rab_schl, 0, 5);
		sqlout ((double *) &smt_rab, 3, 0); 
		sqlout ((double *) &rech_rab, 3, 0); 
		sqlout ((char *) rab_teil_smt, 0, 3); 
		sqlout ((char *) rab_me_einh, 0, 3); 
		rab = smt_rab = rech_rab = 0.0;
        if (sqlcomm 
			("select smt_rab, rech_rab, rab_teil_smt, rab_me_einh "
			 "from rab_stufek where mdn = ? and rab_nr = ?") == 100)
		{
			return 0.0;
		}

		rab = rech_rab;
		if (GetPosRab (mdn, rab_schl, &rab) == FALSE)
		{
		           rab = smt_rab + rab - smt_rab*rab / 100;
		}
		GetSmtRab (mdn, rab_schl, &rab);
		GetMeEinhRab (mdn, rab_schl, &rab);
		BaValues.rabwrt = 0.0;
        GetSmtRabWrt (mdn, rab_schl, &BaValues.rabwrt);
        GetMeEinhRabWrt (mdn, rab_schl, &BaValues.rabwrt);
		return rab;
}
        

BOOL ABASIS::GetSmtRab (short mdn, char *rab_schl, double *rab)
{

	    double rab_wrt;
		char rab_kz_wa[2];
		int dsqlstatus;

		strcpy (rab_kz_wa, "%");
		sqlin ((short *) &mdn, 1, 0);
        sqlin ((char *) rab_schl, 0, 5);
        sqlin ((double *) &_a_bas.teil_smt, 1, 0);
        sqlin ((char *) rab_kz_wa, 0, 2);
		sqlout ((double *) &rab_wrt, 3, 0); 
        dsqlstatus = sqlcomm 
			("select rab_wrt from rab_tsmt "
			 "where mdn = ? "
			 "and rab_nr = ? "
			 "and teil_smt = ? "
			 "and rab_kz_wa = ?");
		if (dsqlstatus) return FALSE;

		*rab = *rab + rab_wrt - *rab * rab_wrt / 100;
		return TRUE;
}

BOOL ABASIS::GetMeEinhRab (short mdn, char *rab_schl, double *rab)
{

	    double rab_wrt;
		char rab_kz_wa[3];
		int dsqlstatus;

		strcpy (rab_kz_wa, "%");
		sqlin ((short *) &mdn, 1, 0);
        sqlin ((char *) rab_schl, 0, 5);
        sqlin ((double *) &_a_bas.me_einh, 1, 0);
        sqlin ((char *) rab_kz_wa, 0, 2);
		sqlout ((double *) &rab_wrt, 3, 0); 
        dsqlstatus = sqlcomm 
			("select rab_wrt from rab_meeinh "
			 "where mdn = ? "
			 "and rab_nr = ? "
			 "and me_einh = ? "
			 "and rab_kz_wa = ?");
		if (dsqlstatus) return FALSE;

		*rab = *rab + rab_wrt - *rab * rab_wrt / 100;
		return TRUE;
}

BOOL ABASIS::GetSmtRabWrt (short mdn, char *rab_schl, double *rab)
{

	    double rab_wrt;
		char rab_kz_wa[2];
		int dsqlstatus;

		strcpy (rab_kz_wa, "W");
		sqlin ((short *) &mdn, 1, 0);
        sqlin ((char *) rab_schl, 0, 5);
        sqlin ((double *) &_a_bas.teil_smt, 1, 0);
        sqlin ((char *) rab_kz_wa, 0, 2);
		sqlout ((double *) &rab_wrt, 3, 0); 
        dsqlstatus = sqlcomm 
			("select rab_wrt from rab_tsmt "
			 "where mdn = ? "
			 "and rab_nr = ? "
			 "and teil_smt = ? "
			 "and rab_kz_wa = ?");
		if (dsqlstatus) return FALSE;

		*rab = *rab + rab_wrt;
		return TRUE;
}

BOOL ABASIS::GetMeEinhRabWrt (short mdn, char *rab_schl, double *rab)
{

	    double rab_wrt;
		char rab_kz_wa[3];
		int dsqlstatus;

		strcpy (rab_kz_wa, "W");
		sqlin ((short *) &mdn, 1, 0);
        sqlin ((char *) rab_schl, 0, 5);
        sqlin ((double *) &_a_bas.me_einh, 1, 0);
        sqlin ((char *) rab_kz_wa, 0, 2);
		sqlout ((double *) &rab_wrt, 3, 0); 
        dsqlstatus = sqlcomm 
			("select rab_wrt from rab_meeinh "
			 "where mdn = ? "
			 "and rab_nr = ? "
			 "and me_einh = ? "
			 "and rab_kz_wa = ?");
		if (dsqlstatus) return FALSE;

		*rab = *rab + rab_wrt;
		return TRUE;
}

BOOL ABASIS::GetPosRab (short mdn, char *rab_schl, double *rab)
{

	    double rab_satz;
		double rab_me_kond;
		int dsqlstatus;

		sqlin ((short *) &mdn, 1, 0);
        sqlin ((char *) rab_schl, 0, 5);
        sqlin ((double *) &_a_bas.a, 3, 0);
		sqlout ((double *) &rab_satz, 3, 0); 
		sqlout ((double *) &rab_me_kond, 3, 0); 
        dsqlstatus = sqlcomm 
			("select rab_satz, rab_me_kond from rab_stufep "
			 "where mdn = ? "
			 "and rab_nr = ? "
			 "and a = ? order by rab_me_kond desc");
		if (dsqlstatus == 100)
		{
		     sqlin ((short *) &mdn, 1, 0);
             sqlin ((char *) rab_schl, 0, 5);
             sqlin ((double *) &_a_bas.ag, 1, 0);
		     sqlout ((double *) &rab_satz, 3, 0); 
		     sqlout ((double *) &rab_me_kond, 3, 0); 
             dsqlstatus = sqlcomm 
			         ("select rab_satz, rab_me_kond from rab_stufep "
			          "where mdn = ? "
			          "and rab_nr = ? "
			          "and ag = ? order by rab_me_kond desc");
		}
		if (dsqlstatus == 100)
		{
		     sqlin ((short *) &mdn, 1, 0);
             sqlin ((char *) rab_schl, 0, 5);
             sqlin ((double *) &_a_bas.wg, 1, 0);
		     sqlout ((double *) &rab_satz, 3, 0); 
		     sqlout ((double *) &rab_me_kond, 3, 0); 
             dsqlstatus = sqlcomm 
			         ("select rab_satz, rab_me_kond from rab_stufep "
			          "where mdn = ? "
			          "and rab_nr = ? "
			          "and wg = ? order by rab_me_kond desc");
		}

		if (dsqlstatus == 100)
		{
		     sqlin ((short *) &mdn, 1, 0);
             sqlin ((char *) rab_schl, 0, 5);
             sqlin ((double *) &_a_bas.hwg, 1, 0);
		     sqlout ((double *) &rab_satz, 3, 0); 
		     sqlout ((double *) &rab_me_kond, 3, 0); 
             dsqlstatus = sqlcomm 
			         ("select rab_satz, rab_me_kond from rab_stufep "
			          "where mdn = ? "
			          "and rab_nr = ? "
			          "and hwg = ? order by rab_me_kond desc");
		}
		if (dsqlstatus) return FALSE;

		*rab = *rab + rab_satz - *rab * rab_satz / 100;
		return TRUE;
}



       

        
         