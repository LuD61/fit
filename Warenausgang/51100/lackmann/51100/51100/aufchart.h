#ifndef _AUFCHART_DEF
#define _AUFCHART_DEF

#include "dbclass.h"

struct AUFCHART {
   long      auf;
   long      kun;
   long      lieferdat;
   short     jr;
   short     kw;
   short     mo;
   short     wochentag;
   short     woche_folge;
};
extern struct AUFCHART aufchart, aufchart_null;

#line 7 "aufchart.rh"

class AUFCHART_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               AUFCHART_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
};
#endif
