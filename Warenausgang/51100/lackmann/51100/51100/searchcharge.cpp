#include <windows.h>
#include "searchcharge.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "sys_par.h"


int SEARCHCHARGE::idx;
long SEARCHCHARGE::anz;
CHQEX *SEARCHCHARGE::Query = NULL;
DB_CLASS SEARCHCHARGE::DbClass; 
HINSTANCE SEARCHCHARGE::hMainInst;
HWND SEARCHCHARGE::hMainWindow;
HWND SEARCHCHARGE::awin;


int SEARCHCHARGE::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

double SEARCHCHARGE::ResBsd (short mdn, short fil, char *chargennr, double a, double bsd_gew)
{
      double me = 0;

      DbClass.sqlin ((short *) &mdn, 1, 0);
      DbClass.sqlin ((short *) &fil, 1, 0);
      DbClass.sqlin ((double *) &a, 3, 0);
      DbClass.sqlin ((char *) chargennr, 0, 20);
      DbClass.sqlout ((double *) &me, 3, 0);
      DbClass.sqlcomm ("select me from best_res "
                       "where mdn = ? "
                       "and fil = ? "
                       "and a = ?"
                       "and chargennr = ?");
       
      return bsd_gew - me;
}

int SEARCHCHARGE::ReadLst (char *name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;
      char chargennr [31];
      double bsd_gew;
      char lief [17];
      char verf_dat [12] = {""};
      long verf_datl;
      short mdn;
      short fil;
      double a;
	  int zentral_lager = 1;
	  SYS_PAR_CLASS sys_par_class;

      strcpy (sys_par.sys_par_nam,"zentral_lager");
      if (sys_par_class.dbreadfirst () == 0)
      {
            zentral_lager = atoi (sys_par.sys_par_wrt);
      }

      anz = wsplit (name, " ");
      if (anz < 3) return -1;

      mdn = atoi (wort[0]);
      fil = atoi (wort[1]);
      a   = ratod (wort[2]);

	  anz = 0;
    
	  if (zentral_lager)
	  {
		  sprintf (buffer, "select bsds.chargennr, bsds.bsd_gew, bsds.lief, bsds.verf_dat, bsd_update "
	 					   "from bsds "
						   "where  a = ? "
						   "and chargennr > \" \" order by bsd_update");

		  DbClass.sqlin ((double *) &a, 3, 0);
		  DbClass.sqlout ((char *) chargennr, 0, 31);
		  DbClass.sqlout ((double *) &bsd_gew, 3, 0);
		  DbClass.sqlout ((char *) lief, 0, 17);
		  DbClass.sqlout ((long *) &verf_datl, 2, 0);
	  }
	  else
	  {
		  sprintf (buffer, "select bsds.chargennr, bsds.bsd_gew, bsds.lief, bsds.verf_dat, bsd_update "
	 					   "from bsds "
						   "where mdn = ? "
						   "and fil = ? "
						   "and  a = ? "
						   "and chargennr > \" \" order by bsd_update");

		  DbClass.sqlin ((short *) &mdn, 1, 0);
		  DbClass.sqlin ((short *) &fil, 1, 0);
		  DbClass.sqlin ((double *) &a, 3, 0);
		  DbClass.sqlout ((char *) chargennr, 0, 31);
		  DbClass.sqlout ((double *) &bsd_gew, 3, 0);
		  DbClass.sqlout ((char *) lief, 0, 17);
		  DbClass.sqlout ((long *) &verf_datl, 2, 0);
	  }
	  cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
//      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
//  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		  if (verf_datl > 0l)
		  {
			dlong_to_asc (verf_datl, verf_dat);
		  }
		  else
		  {
			  strcpy (verf_dat, "");
		  }
		  if (strcmp (verf_dat, "01.01.1970") == 0)
		  {
			  strcpy (verf_dat, "");
		  }

          bsd_gew = ResBsd (mdn, fil, chargennr, a, bsd_gew);
          if (bsd_gew > 0)
          {
              sprintf (buffer, "  |%-31s|   %12.3lf   |%-16s|    |%-10s|", 
                                 chargennr, bsd_gew, lief, verf_dat);
	          Query->InsertRecord (buffer);
          }
		  i ++;
      }
      anz = i;
	  DbClass.sqlclose (cursor);
// 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHCHARGE::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

BOOL SEARCHCHARGE::GetKey (char * key)
{
      int anz;
     
      if (idx == -1) return FALSE;
      anz = wsplit (Key, "|");
      if (anz < 1) return FALSE;
      strcpy (key, wort[0]);
      return TRUE;
}

BOOL SEARCHCHARGE::GetSelectedRow (char * key)
{
    
      if (idx == -1) return FALSE;
      strcpy (key, Key);
      return TRUE;
}


void SEARCHCHARGE::Search (char *keys)
{
  	  int cx, cy;
	  char buffer [256];

	  idx = -1;
      cx = 100;
      cy = 25;
      Settchar ('|');
      EnableWindow (hMainWindow, FALSE);
      Query = new CHQEX (cx, cy);
      Query->EnableSort (TRUE);
      Query->OpenWindow (hMainInst, hMainWindow);
      sprintf (buffer, "%s %s %s %s",
                          "%s",
                          "%lf" 
                          "%s",
                          "%s");
      Query->RowAttr (buffer);
	  sprintf (buffer, " %32s  %15s   %18s", "1", "1", "1"); 
	  Query->VLines (buffer, 3);
      sprintf (buffer, "1;31 35;13 51;16 71;10");
      Query->RowPos (buffer);
	  EnableWindow (awin, FALSE);

	  sprintf (buffer, " %-32s  %14s   %-18s %-12s", " Chargennr", " Menge ", " Lieferant",
                                                     " Verfalldatum"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadLst);
	  Query->SetSearchLst (SearchLst);
      ReadLst (keys);
//      Query->SetSortRow (1, TRUE);
      Query->SetSBuffSet (TRUE);
	  Query->ProcessMessages ();
      EnableWindow (hMainWindow, TRUE);
      if (syskey != KEY5 && Query->GetCount () > 0)
      {
              idx = Query->GetSel ();
              if (idx != -1)
              {
                     Query->GetText (Key);
              }
      }
      else
      {
              syskey = KEY5;
      }
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
}

