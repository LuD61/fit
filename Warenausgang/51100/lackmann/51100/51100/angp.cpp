#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "angp.h"
#include "dbclass.h"
#include "dbfunc.h"

struct ANGP angp, angp_null;

static char *sqltext;

void ANGP_CLASS::prepare (void)
{
         ins_quest ((char *) &angp.mdn, 1, 0);
         ins_quest ((char *) &angp.fil, 1, 0);  
         ins_quest ((char *) &angp.ang, 2, 0);
         ins_quest ((char *) &angp.posi, 2, 0);
         ins_quest ((char *) &angp.a, 3, 0);

    out_quest ((char *) &angp.mdn,1,0);
    out_quest ((char *) &angp.fil,1,0);
    out_quest ((char *) &angp.ang,2,0);
    out_quest ((char *) &angp.posi,2,0);
    out_quest ((char *) &angp.angp_txt,2,0);
    out_quest ((char *) &angp.a,3,0);
    out_quest ((char *) &angp.auf_me,3,0);
    out_quest ((char *) angp.auf_me_bz,0,7);
    out_quest ((char *) &angp.lief_me,3,0);
    out_quest ((char *) angp.lief_me_bz,0,7);
    out_quest ((char *) &angp.auf_vk_pr,3,0);
    out_quest ((char *) &angp.auf_lad_pr,3,0);
    out_quest ((char *) &angp.delstatus,1,0);
    out_quest ((char *) &angp.sa_kz_sint,1,0);
    out_quest ((char *) &angp.prov_satz,3,0);
    out_quest ((char *) &angp.ksys,2,0);
    out_quest ((char *) &angp.pid,2,0);
    out_quest ((char *) &angp.auf_klst,2,0);
    out_quest ((char *) &angp.teil_smt,1,0);
    out_quest ((char *) &angp.dr_folge,1,0);
    out_quest ((char *) &angp.inh,3,0);
    out_quest ((char *) &angp.auf_vk_euro,3,0);
    out_quest ((char *) &angp.auf_vk_fremd,3,0);
    out_quest ((char *) &angp.auf_lad_euro,3,0);
    out_quest ((char *) &angp.auf_lad_fremd,3,0);
    out_quest ((char *) &angp.rab_satz,3,0);
    out_quest ((char *) &angp.me_einh_kun,1,0);
    out_quest ((char *) &angp.me_einh,1,0);
         cursor = prepare_sql ("select angp.mdn,  angp.fil,  "
"angp.ang,  angp.posi,  angp.angp_txt,  angp.a,  angp.auf_me,  "
"angp.auf_me_bz,  angp.lief_me,  angp.lief_me_bz,  angp.auf_vk_pr,  "
"angp.auf_lad_pr,  angp.delstatus,  angp.sa_kz_sint,  angp.prov_satz,  "
"angp.ksys,  angp.pid,  angp.auf_klst,  angp.teil_smt,  angp.dr_folge,  "
"angp.inh,  angp.auf_vk_euro,  angp.auf_vk_fremd,  angp.auf_lad_euro,  "
"angp.auf_lad_fremd,  angp.rab_satz,  angp.me_einh_kun,  angp.me_einh from angp "

#line 29 "angp.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   ang = ? "
                               "and   posi = ? "
                               "and   a    = ?");

    ins_quest ((char *) &angp.mdn,1,0);
    ins_quest ((char *) &angp.fil,1,0);
    ins_quest ((char *) &angp.ang,2,0);
    ins_quest ((char *) &angp.posi,2,0);
    ins_quest ((char *) &angp.angp_txt,2,0);
    ins_quest ((char *) &angp.a,3,0);
    ins_quest ((char *) &angp.auf_me,3,0);
    ins_quest ((char *) angp.auf_me_bz,0,7);
    ins_quest ((char *) &angp.lief_me,3,0);
    ins_quest ((char *) angp.lief_me_bz,0,7);
    ins_quest ((char *) &angp.auf_vk_pr,3,0);
    ins_quest ((char *) &angp.auf_lad_pr,3,0);
    ins_quest ((char *) &angp.delstatus,1,0);
    ins_quest ((char *) &angp.sa_kz_sint,1,0);
    ins_quest ((char *) &angp.prov_satz,3,0);
    ins_quest ((char *) &angp.ksys,2,0);
    ins_quest ((char *) &angp.pid,2,0);
    ins_quest ((char *) &angp.auf_klst,2,0);
    ins_quest ((char *) &angp.teil_smt,1,0);
    ins_quest ((char *) &angp.dr_folge,1,0);
    ins_quest ((char *) &angp.inh,3,0);
    ins_quest ((char *) &angp.auf_vk_euro,3,0);
    ins_quest ((char *) &angp.auf_vk_fremd,3,0);
    ins_quest ((char *) &angp.auf_lad_euro,3,0);
    ins_quest ((char *) &angp.auf_lad_fremd,3,0);
    ins_quest ((char *) &angp.rab_satz,3,0);
    ins_quest ((char *) &angp.me_einh_kun,1,0);
    ins_quest ((char *) &angp.me_einh,1,0);
         sqltext = "update angp set angp.mdn = ?,  angp.fil = ?,  "
"angp.ang = ?,  angp.posi = ?,  angp.angp_txt = ?,  angp.a = ?,  "
"angp.auf_me = ?,  angp.auf_me_bz = ?,  angp.lief_me = ?,  "
"angp.lief_me_bz = ?,  angp.auf_vk_pr = ?,  angp.auf_lad_pr = ?,  "
"angp.delstatus = ?,  angp.sa_kz_sint = ?,  angp.prov_satz = ?,  "
"angp.ksys = ?,  angp.pid = ?,  angp.auf_klst = ?,  angp.teil_smt = ?,  "
"angp.dr_folge = ?,  angp.inh = ?,  angp.auf_vk_euro = ?,  "
"angp.auf_vk_fremd = ?,  angp.auf_lad_euro = ?,  "
"angp.auf_lad_fremd = ?,  angp.rab_satz = ?,  angp.me_einh_kun = ?,  "
"angp.me_einh = ? "

#line 36 "angp.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   ang = ? "
                               "and   posi = ? "
                               "and   a    = ?";
  
         ins_quest ((char *) &angp.mdn, 1, 0);
         ins_quest ((char *) &angp.fil, 1, 0);  
         ins_quest ((char *) &angp.ang, 2, 0);
         ins_quest ((char *) &angp.posi, 2, 0);
         ins_quest ((char *) &angp.a, 3, 0);

         upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &angp.mdn,1,0);
    ins_quest ((char *) &angp.fil,1,0);
    ins_quest ((char *) &angp.ang,2,0);
    ins_quest ((char *) &angp.posi,2,0);
    ins_quest ((char *) &angp.angp_txt,2,0);
    ins_quest ((char *) &angp.a,3,0);
    ins_quest ((char *) &angp.auf_me,3,0);
    ins_quest ((char *) angp.auf_me_bz,0,7);
    ins_quest ((char *) &angp.lief_me,3,0);
    ins_quest ((char *) angp.lief_me_bz,0,7);
    ins_quest ((char *) &angp.auf_vk_pr,3,0);
    ins_quest ((char *) &angp.auf_lad_pr,3,0);
    ins_quest ((char *) &angp.delstatus,1,0);
    ins_quest ((char *) &angp.sa_kz_sint,1,0);
    ins_quest ((char *) &angp.prov_satz,3,0);
    ins_quest ((char *) &angp.ksys,2,0);
    ins_quest ((char *) &angp.pid,2,0);
    ins_quest ((char *) &angp.auf_klst,2,0);
    ins_quest ((char *) &angp.teil_smt,1,0);
    ins_quest ((char *) &angp.dr_folge,1,0);
    ins_quest ((char *) &angp.inh,3,0);
    ins_quest ((char *) &angp.auf_vk_euro,3,0);
    ins_quest ((char *) &angp.auf_vk_fremd,3,0);
    ins_quest ((char *) &angp.auf_lad_euro,3,0);
    ins_quest ((char *) &angp.auf_lad_fremd,3,0);
    ins_quest ((char *) &angp.rab_satz,3,0);
    ins_quest ((char *) &angp.me_einh_kun,1,0);
    ins_quest ((char *) &angp.me_einh,1,0);
         ins_cursor = prepare_sql ("insert into angp (mdn,  fil,  "
"ang,  posi,  angp_txt,  a,  auf_me,  auf_me_bz,  lief_me,  lief_me_bz,  auf_vk_pr,  "
"auf_lad_pr,  delstatus,  sa_kz_sint,  prov_satz,  ksys,  pid,  auf_klst,  "
"teil_smt,  dr_folge,  inh,  auf_vk_euro,  auf_vk_fremd,  auf_lad_euro,  "
"auf_lad_fremd,  rab_satz,  me_einh_kun,  me_einh) "

#line 51 "angp.rpp"
                                   "values "
                                   "(?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

#line 53 "angp.rpp"

         ins_quest ((char *) &angp.mdn, 1, 0);
         ins_quest ((char *) &angp.fil, 1, 0);  
         ins_quest ((char *) &angp.ang, 2, 0);
         ins_quest ((char *) &angp.posi, 2, 0);
         ins_quest ((char *) &angp.a, 3, 0);
         del_cursor = prepare_sql ("delete from angp "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   ang = ? "
                               "and   posi = ? "
                               "and   a    = ?");

         ins_quest ((char *) &angp.mdn, 1, 0);
         ins_quest ((char *) &angp.fil, 1, 0);  
         ins_quest ((char *) &angp.ang, 2, 0);
         ins_quest ((char *) &angp.posi, 2, 0);
         ins_quest ((char *) &angp.a, 3, 0);
         test_upd_cursor = prepare_sql ("select a from angp "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   ang = ? "
                               "and   posi = ? "
                               "and   a    = ?");
         ins_quest ((char *) &angp.mdn, 1, 0);
         ins_quest ((char *) &angp.fil, 1, 0);  
         ins_quest ((char *) &angp.ang, 2, 0);
         del_ang_curs = prepare_sql ("delete from angp "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   ang = ?");
}

void ANGP_CLASS::prepare_ang (char *order)
{
         ins_quest ((char *) &angp.mdn, 1, 0);
         ins_quest ((char *) &angp.fil, 1, 0);  
         ins_quest ((char *) &angp.ang, 2, 0);
         if (order)
         {
    out_quest ((char *) &angp.mdn,1,0);
    out_quest ((char *) &angp.fil,1,0);
    out_quest ((char *) &angp.ang,2,0);
    out_quest ((char *) &angp.posi,2,0);
    out_quest ((char *) &angp.angp_txt,2,0);
    out_quest ((char *) &angp.a,3,0);
    out_quest ((char *) &angp.auf_me,3,0);
    out_quest ((char *) angp.auf_me_bz,0,7);
    out_quest ((char *) &angp.lief_me,3,0);
    out_quest ((char *) angp.lief_me_bz,0,7);
    out_quest ((char *) &angp.auf_vk_pr,3,0);
    out_quest ((char *) &angp.auf_lad_pr,3,0);
    out_quest ((char *) &angp.delstatus,1,0);
    out_quest ((char *) &angp.sa_kz_sint,1,0);
    out_quest ((char *) &angp.prov_satz,3,0);
    out_quest ((char *) &angp.ksys,2,0);
    out_quest ((char *) &angp.pid,2,0);
    out_quest ((char *) &angp.auf_klst,2,0);
    out_quest ((char *) &angp.teil_smt,1,0);
    out_quest ((char *) &angp.dr_folge,1,0);
    out_quest ((char *) &angp.inh,3,0);
    out_quest ((char *) &angp.auf_vk_euro,3,0);
    out_quest ((char *) &angp.auf_vk_fremd,3,0);
    out_quest ((char *) &angp.auf_lad_euro,3,0);
    out_quest ((char *) &angp.auf_lad_fremd,3,0);
    out_quest ((char *) &angp.rab_satz,3,0);
    out_quest ((char *) &angp.me_einh_kun,1,0);
    out_quest ((char *) &angp.me_einh,1,0);
                      sel_ang_curs = prepare_sql ("select "
"angp.mdn,  angp.fil,  angp.ang,  angp.posi,  angp.angp_txt,  angp.a,  "
"angp.auf_me,  angp.auf_me_bz,  angp.lief_me,  angp.lief_me_bz,  "
"angp.auf_vk_pr,  angp.auf_lad_pr,  angp.delstatus,  angp.sa_kz_sint,  "
"angp.prov_satz,  angp.ksys,  angp.pid,  angp.auf_klst,  angp.teil_smt,  "
"angp.dr_folge,  angp.inh,  angp.auf_vk_euro,  angp.auf_vk_fremd,  "
"angp.auf_lad_euro,  angp.auf_lad_fremd,  angp.rab_satz,  "
"angp.me_einh_kun,  angp.me_einh from angp "

#line 94 "angp.rpp"
                                                  "where mdn = ? "
                                                  "and   fil = ? "
                                                  "and   ang = ? "
                                                  "%s", order);
         }
         else
         {
    out_quest ((char *) &angp.mdn,1,0);
    out_quest ((char *) &angp.fil,1,0);
    out_quest ((char *) &angp.ang,2,0);
    out_quest ((char *) &angp.posi,2,0);
    out_quest ((char *) &angp.angp_txt,2,0);
    out_quest ((char *) &angp.a,3,0);
    out_quest ((char *) &angp.auf_me,3,0);
    out_quest ((char *) angp.auf_me_bz,0,7);
    out_quest ((char *) &angp.lief_me,3,0);
    out_quest ((char *) angp.lief_me_bz,0,7);
    out_quest ((char *) &angp.auf_vk_pr,3,0);
    out_quest ((char *) &angp.auf_lad_pr,3,0);
    out_quest ((char *) &angp.delstatus,1,0);
    out_quest ((char *) &angp.sa_kz_sint,1,0);
    out_quest ((char *) &angp.prov_satz,3,0);
    out_quest ((char *) &angp.ksys,2,0);
    out_quest ((char *) &angp.pid,2,0);
    out_quest ((char *) &angp.auf_klst,2,0);
    out_quest ((char *) &angp.teil_smt,1,0);
    out_quest ((char *) &angp.dr_folge,1,0);
    out_quest ((char *) &angp.inh,3,0);
    out_quest ((char *) &angp.auf_vk_euro,3,0);
    out_quest ((char *) &angp.auf_vk_fremd,3,0);
    out_quest ((char *) &angp.auf_lad_euro,3,0);
    out_quest ((char *) &angp.auf_lad_fremd,3,0);
    out_quest ((char *) &angp.rab_satz,3,0);
    out_quest ((char *) &angp.me_einh_kun,1,0);
    out_quest ((char *) &angp.me_einh,1,0);
                      sel_ang_curs = prepare_sql ("select "
"angp.mdn,  angp.fil,  angp.ang,  angp.posi,  angp.angp_txt,  angp.a,  "
"angp.auf_me,  angp.auf_me_bz,  angp.lief_me,  angp.lief_me_bz,  "
"angp.auf_vk_pr,  angp.auf_lad_pr,  angp.delstatus,  angp.sa_kz_sint,  "
"angp.prov_satz,  angp.ksys,  angp.pid,  angp.auf_klst,  angp.teil_smt,  "
"angp.dr_folge,  angp.inh,  angp.auf_vk_euro,  angp.auf_vk_fremd,  "
"angp.auf_lad_euro,  angp.auf_lad_fremd,  angp.rab_satz,  "
"angp.me_einh_kun,  angp.me_einh from angp "

#line 102 "angp.rpp"
                                                  "where mdn = ? "
                                                  "and   fil = ? "
                                                  "and   ang = ?");
         }
}

int ANGP_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int ANGP_CLASS::dbreadfirstang (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (sel_ang_curs == -1)
         {
                this->prepare_ang (order);
         }
         open_sql (sel_ang_curs);
         return fetch_sql (sel_ang_curs);
}

int ANGP_CLASS::dbreadang (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         return fetch_sql (sel_ang_curs);
}
 
int ANGP_CLASS::dbdeleteang (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (del_ang_curs == -1)
         {
                this->prepare ();
         }
         return execute_curs (del_ang_curs);
}

int ANGP_CLASS::dbcloseang (void)
{
         if (sel_ang_curs != -1)
         {
                 close_sql (sel_ang_curs);
                 sel_ang_curs = -1;
         }
         return 0;
}


int ANGP_CLASS::dbclose (void)
{
         if (del_ang_curs != -1)
         {
                 close_sql (del_ang_curs);
                 del_ang_curs = -1;
         }
         this->DB_CLASS::dbclose ();
         return 0;
}
