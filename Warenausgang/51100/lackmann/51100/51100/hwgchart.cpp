#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "hwgchart.h"
#include "aufchart.h"

struct HWGCHART hwgchart, hwgchart_null;
static DB_CLASS DbClass;

void HWGCHART_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &hwgchart.auf, 2, 0);
            ins_quest ((char *) &hwgchart.hwg, 1, 0);
    out_quest ((char *) &hwgchart.auf,2,0);
    out_quest ((char *) &hwgchart.hwg,1,0);
    out_quest ((char *) &hwgchart.hwg_anz,2,0);
    out_quest ((char *) &hwgchart.ist_ges_anz,2,0);
    out_quest ((char *) &hwgchart.ist_ges_eur,3,0);
    out_quest ((char *) &hwgchart.ist_ges_kg,3,0);
    out_quest ((char *) &hwgchart.ist_ges_proz,3,0);
    out_quest ((char *) &hwgchart.ist_ges_eurkg,3,0);
    out_quest ((char *) &hwgchart.ziel_ges_eur,3,0);
    out_quest ((char *) &hwgchart.ziel_ges_kg,3,0);
    out_quest ((char *) &hwgchart.ziel_ges_proz,3,0);
    out_quest ((char *) &hwgchart.ziel_ges_eurkg,3,0);
    out_quest ((char *) &hwgchart.soll_eur,3,0);
    out_quest ((char *) &hwgchart.soll_kg,3,0);
    out_quest ((char *) &hwgchart.soll_proz,3,0);
    out_quest ((char *) &hwgchart.soll_eurkg,3,0);
    out_quest ((char *) &hwgchart.ist_anz,2,0);
    out_quest ((char *) &hwgchart.ist_eur,3,0);
    out_quest ((char *) &hwgchart.ist_kg,3,0);
    out_quest ((char *) &hwgchart.ist_proz,3,0);
    out_quest ((char *) &hwgchart.ist_eurkg,3,0);
    out_quest ((char *) &hwgchart.soll_anz,2,0);
    out_quest ((char *) &hwgchart.aktiv,1,0);
            cursor = prepare_sql ("select hwgchart.auf,  "
"hwgchart.hwg,  hwgchart.hwg_anz,  hwgchart.ist_ges_anz,  "
"hwgchart.ist_ges_eur,  hwgchart.ist_ges_kg,  hwgchart.ist_ges_proz,  "
"hwgchart.ist_ges_eurkg,  hwgchart.ziel_ges_eur,  "
"hwgchart.ziel_ges_kg,  hwgchart.ziel_ges_proz,  "
"hwgchart.ziel_ges_eurkg,  hwgchart.soll_eur,  hwgchart.soll_kg,  "
"hwgchart.soll_proz,  hwgchart.soll_eurkg,  hwgchart.ist_anz,  "
"hwgchart.ist_eur,  hwgchart.ist_kg,  hwgchart.ist_proz,  "
"hwgchart.ist_eurkg,  hwgchart.soll_anz,  hwgchart.aktiv from hwgchart "

#line 29 "hwgchart.rpp"
                                  "where auf = ? and hwg = ? "
                                  "order by auf,hwg");

    ins_quest ((char *) &hwgchart.auf,2,0);
    ins_quest ((char *) &hwgchart.hwg,1,0);
    ins_quest ((char *) &hwgchart.hwg_anz,2,0);
    ins_quest ((char *) &hwgchart.ist_ges_anz,2,0);
    ins_quest ((char *) &hwgchart.ist_ges_eur,3,0);
    ins_quest ((char *) &hwgchart.ist_ges_kg,3,0);
    ins_quest ((char *) &hwgchart.ist_ges_proz,3,0);
    ins_quest ((char *) &hwgchart.ist_ges_eurkg,3,0);
    ins_quest ((char *) &hwgchart.ziel_ges_eur,3,0);
    ins_quest ((char *) &hwgchart.ziel_ges_kg,3,0);
    ins_quest ((char *) &hwgchart.ziel_ges_proz,3,0);
    ins_quest ((char *) &hwgchart.ziel_ges_eurkg,3,0);
    ins_quest ((char *) &hwgchart.soll_eur,3,0);
    ins_quest ((char *) &hwgchart.soll_kg,3,0);
    ins_quest ((char *) &hwgchart.soll_proz,3,0);
    ins_quest ((char *) &hwgchart.soll_eurkg,3,0);
    ins_quest ((char *) &hwgchart.ist_anz,2,0);
    ins_quest ((char *) &hwgchart.ist_eur,3,0);
    ins_quest ((char *) &hwgchart.ist_kg,3,0);
    ins_quest ((char *) &hwgchart.ist_proz,3,0);
    ins_quest ((char *) &hwgchart.ist_eurkg,3,0);
    ins_quest ((char *) &hwgchart.soll_anz,2,0);
    ins_quest ((char *) &hwgchart.aktiv,1,0);
            sqltext = "update hwgchart set hwgchart.auf = ?,  "
"hwgchart.hwg = ?,  hwgchart.hwg_anz = ?,  hwgchart.ist_ges_anz = ?,  "
"hwgchart.ist_ges_eur = ?,  hwgchart.ist_ges_kg = ?,  "
"hwgchart.ist_ges_proz = ?,  hwgchart.ist_ges_eurkg = ?,  "
"hwgchart.ziel_ges_eur = ?,  hwgchart.ziel_ges_kg = ?,  "
"hwgchart.ziel_ges_proz = ?,  hwgchart.ziel_ges_eurkg = ?,  "
"hwgchart.soll_eur = ?,  hwgchart.soll_kg = ?,  "
"hwgchart.soll_proz = ?,  hwgchart.soll_eurkg = ?,  "
"hwgchart.ist_anz = ?,  hwgchart.ist_eur = ?,  hwgchart.ist_kg = ?,  "
"hwgchart.ist_proz = ?,  hwgchart.ist_eurkg = ?,  "
"hwgchart.soll_anz = ?,  hwgchart.aktiv = ? "

#line 33 "hwgchart.rpp"
                                  "where auf = ? "
                                  "and hwg  = ?";

            ins_quest ((char *) &hwgchart.auf, 2, 0);
            ins_quest ((char *) &hwgchart.hwg, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &hwgchart.auf, 2, 0);
            ins_quest ((char *) &hwgchart.hwg, 1, 0);
            test_upd_cursor = prepare_sql ("select auf from hwgchart "
                                  "where auf = ? "
                                  "and   hwg  = ?");

            ins_quest ((char *) &hwgchart.auf, 2, 0);
            ins_quest ((char *) &hwgchart.hwg, 1, 0);
            del_cursor = prepare_sql ("delete from hwgchart "
                                  "where auf = ? "
                                  "and   hwg  = ?");


    ins_quest ((char *) &hwgchart.auf,2,0);
    ins_quest ((char *) &hwgchart.hwg,1,0);
    ins_quest ((char *) &hwgchart.hwg_anz,2,0);
    ins_quest ((char *) &hwgchart.ist_ges_anz,2,0);
    ins_quest ((char *) &hwgchart.ist_ges_eur,3,0);
    ins_quest ((char *) &hwgchart.ist_ges_kg,3,0);
    ins_quest ((char *) &hwgchart.ist_ges_proz,3,0);
    ins_quest ((char *) &hwgchart.ist_ges_eurkg,3,0);
    ins_quest ((char *) &hwgchart.ziel_ges_eur,3,0);
    ins_quest ((char *) &hwgchart.ziel_ges_kg,3,0);
    ins_quest ((char *) &hwgchart.ziel_ges_proz,3,0);
    ins_quest ((char *) &hwgchart.ziel_ges_eurkg,3,0);
    ins_quest ((char *) &hwgchart.soll_eur,3,0);
    ins_quest ((char *) &hwgchart.soll_kg,3,0);
    ins_quest ((char *) &hwgchart.soll_proz,3,0);
    ins_quest ((char *) &hwgchart.soll_eurkg,3,0);
    ins_quest ((char *) &hwgchart.ist_anz,2,0);
    ins_quest ((char *) &hwgchart.ist_eur,3,0);
    ins_quest ((char *) &hwgchart.ist_kg,3,0);
    ins_quest ((char *) &hwgchart.ist_proz,3,0);
    ins_quest ((char *) &hwgchart.ist_eurkg,3,0);
    ins_quest ((char *) &hwgchart.soll_anz,2,0);
    ins_quest ((char *) &hwgchart.aktiv,1,0);
            ins_cursor = prepare_sql ("insert into hwgchart ("
"auf,  hwg,  hwg_anz,  ist_ges_anz,  ist_ges_eur,  ist_ges_kg,  ist_ges_proz,  "
"ist_ges_eurkg,  ziel_ges_eur,  ziel_ges_kg,  ziel_ges_proz,  "
"ziel_ges_eurkg,  soll_eur,  soll_kg,  soll_proz,  soll_eurkg,  ist_anz,  "
"ist_eur,  ist_kg,  ist_proz,  ist_eurkg,  soll_anz,  aktiv) "

#line 54 "hwgchart.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 56 "hwgchart.rpp"
}
int HWGCHART_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	int dsqlstatus = 0;
	int anz_cursor ;
         if (cursor == -1)
         {
                this->prepare ();
         }
         dsqlstatus = this->DB_CLASS::dbreadfirst ();
		 if (dsqlstatus == 100)
		 {
			  DbClass.sqlin ((short *) &hwgchart.hwg, 1, 0);
			  DbClass.sqlout ((int *) &hwgchart.hwg_anz, 2, 0);
			  anz_cursor = DbClass.sqlcursor  ("select count(*) from stnd_aufp,a_bas where a_bas.hwg = ? and a_bas.delstatus = 0  and a_bas.ghsperre = 0 "
						" and stnd_aufp.kun = 0 and stnd_aufp.kun_fil = 2 and stnd_aufp.fil = 0 and  stnd_aufp.mdn = 1 "
						" and stnd_aufp.a = a_bas.a ");
              DbClass.sqlopen (anz_cursor);
              DbClass.sqlfetch (anz_cursor); 
              DbClass.sqlclose (anz_cursor); 
		 }
         return dsqlstatus;
}

int HWGCHART_CLASS::del_istwerte (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	int dret ;
	int upd_curs;
         extern short sql_mode;
	int sav_sql_mode = sql_mode;
	sql_mode = 2;
    DbClass.sqlin ((long *) &hwgchart.auf, 2, 0);
    upd_curs = DbClass.sqlcursor ("update hwgchart set ist_kg = 0, ist_eur = 0, ist_proz = 0 where auf = ? ");

    DbClass.sqlexecute (upd_curs);
   sql_mode = sav_sql_mode;
   return dret;
   
}

int HWGCHART_CLASS::hole_istgeswerte (int dauf)
/**
Ersten Satz aus Tabelle lesen.
**/
{
int anz_curs ; 
int sum_curs;
int dreturn;

    DbClass.sqlin ((short *) &aufchart.jr, 1, 0);
    DbClass.sqlin ((short *) &aufchart.kw, 1, 0);
    DbClass.sqlin ((long *) &aufchart.kun, 2, 0);
    DbClass.sqlin ((short *) &hwgchart.hwg, 1, 0);
    DbClass.sqlout ((long *) &hwgchart.ist_ges_anz, 2, 0);
    anz_curs = DbClass.sqlcursor ("select count(unique aufp.a)  from aufp,aufchart,a_bas "
                   " where aufp.a = a_bas.a and "
                   " aufp.auf = aufchart.auf and "
                   " aufp.mdn = 1 and aufp.fil = 0 and "
                   " aufchart.jr = ? and aufchart.kw = ? and aufchart.kun = ? "
                   " and a_bas.hwg = ? and a_bas.smt <> \"R\" ");

              DbClass.sqlopen (anz_curs);
              DbClass.sqlfetch (anz_curs); 
              DbClass.sqlclose (anz_curs); 


  DbClass.sqlin ((short *) &hwgchart.hwg,1,0);
  DbClass.sqlin ((long *) &dauf,2,0);
  DbClass.sqlin ((short *) &aufchart.jr,1,0);
  DbClass.sqlin ((short *) &aufchart.kw,1,0);
  DbClass.sqlin ((long *) &aufchart.kun,2,0);
  DbClass.sqlout ((double *) &hwgchart.ist_ges_kg,3,0);
  DbClass.sqlout ((double *) &hwgchart.ist_ges_eur,3,0);
   sum_curs = DbClass.sqlcursor ("select sum(ist_kg), sum(ist_eur)  from hwgchart,aufchart "
                   " where hwgchart.hwg = ? and hwgchart.auf <> ? and "
                   " hwgchart.auf = aufchart.auf and "
                   " aufchart.jr = ? and aufchart.kw = ? and aufchart.kun = ? "); 

              DbClass.sqlopen (sum_curs);
              dreturn = DbClass.sqlfetch (sum_curs); 
              DbClass.sqlclose (sum_curs); 

return dreturn;
}

int HWGCHART_CLASS::hole_sollwerte (int dauf, int danz_tage)
/**
Ersten Satz aus Tabelle lesen.
**/
{
 int von_lieferdat = aufchart.lieferdat - danz_tage;
 int bis_lieferdat = aufchart.lieferdat - 1;
 double soll_anz = 0.0;
 int sum_curs;
 int dsqlstatus;

  DbClass.sqlin ((short *) &hwgchart.hwg,1,0);
  DbClass.sqlin ((short *) &aufchart.jr,1,0);
  DbClass.sqlin ((long *) &von_lieferdat,2,0);
  DbClass.sqlin ((long *) &bis_lieferdat,2,0);
  DbClass.sqlin ((long *) &aufchart.kun,2,0);
  DbClass.sqlin ((short *) &aufchart.woche_folge,1,0);
  DbClass.sqlout ((double *) &soll_anz,3,0);
  DbClass.sqlout ((double *) &hwgchart.soll_kg,3,0);
  DbClass.sqlout ((double *) &hwgchart.soll_eur,3,0);
  sum_curs = DbClass.sqlcursor ("select sum(ist_anz) / count(*), sum(ist_kg) / count(*), sum(ist_eur) / count(*)  from hwgchart,aufchart "
                   " where hwgchart.hwg = ? and "
                   " hwgchart.auf = aufchart.auf and "
                   " aufchart.jr = ? and aufchart.lieferdat between ? and ? and aufchart.kun = ? and aufchart.woche_folge = ? "); 
                   
              DbClass.sqlopen (sum_curs);
              dsqlstatus = DbClass.sqlfetch (sum_curs); 
              DbClass.sqlclose (sum_curs); 
 
 hwgchart.soll_anz = (int) (soll_anz + 0.5);
 return dsqlstatus;                  

}
