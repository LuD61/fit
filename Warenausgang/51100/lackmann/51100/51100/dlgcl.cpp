#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "dlgcl.h"
#ifndef _NINFO
#include "inflib.h"
#endif

#define MAXLEN 40
#define MAXBEZ 500

static ITEM iUbSatzNr ("", "S-Nr", "", 0);

static field _fUbSatzNr[] = {
&iUbSatzNr,          6, 1, 0, 0, 0, "", BUTTON, 0, 0, 0};

static form fUbSatzNr = {1, 0, 0, _fUbSatzNr, 0, 0, 0, 0, NULL};


static char SatzNr [10];
static ITEM iSatzNr ("", SatzNr, "", 0);

static field _fSatzNr[] = {
&iSatzNr,          6, 1, 0, 0, 0, "", EDIT, 0, 0, 0};

static form fSatzNr = {1, 0, 0, _fSatzNr, 0, 0, 0, 0, NULL};


static field iButton = {NULL,  0, 1, 0, 0, 0, "", BUTTON, 0, 0, 0}; 
static field iEdit =   {NULL,  0, 0, 0, 0, 0, "", EDIT, 0, 0, 0}; 

static form IForm = {0, 0, 0, NULL, 0, 0, 0, 0, NULL};

static char *feldbztab [MAXBEZ];
static int fbanz = 0;


HWND DlgClass::InitListWindow (HWND hMainWindow)
/**
Listfenster oeffnen.
**/
{
       int x,y;
       int cx,cy;
       RECT rect;

       this->hMainWindow = hMainWindow;
       this->hMainInst = (HANDLE) GetWindowLong (hMainWindow, 
                                               GWL_HINSTANCE);
       y = LineRow + 5;
       x = 1;
       GetClientRect (hMainWindow, &rect);
       cy = rect.bottom - y - 5;
       cx = rect.right - 2;

       mamain2 = CreateWindowEx (
                                WS_EX_CLIENTEDGE, 
                                "hListWindow",
                                "",       
                                WS_CHILD | WS_VISIBLE,
                                x, y,
                                cx, cy,
                                hMainWindow,
                                NULL,
                                hMainInst,
                                NULL);

        ShowWindow (mamain2, SW_SHOWNORMAL);
        UpdateWindow (mamain2);
        mamain3 = CreateWindowEx (
                                WS_EX_CLIENTEDGE, 
                                "hListWindow",
                                "",       
                                WS_CHILD | WS_VISIBLE,
                                0, 0,
                                0, 0,
                                mamain2,
                                NULL,
                                hMainInst,
                                NULL);
        return mamain2;
}


void DlgClass::TestMamain3 (void)
{
       RECT rect;
//       HDC hdc;
       
       GetClientRect (mamain2, &rect);

       if (TrackNeeded ())
       {
           rect.bottom -= 16;
       }
       if (VTrackNeeded ())
       {
           rect.right -= 16;
       }

       MoveWindow (mamain3, 0, 0, rect.right, rect.bottom, TRUE);


       CloseControls (&UbForm);
       if (UbForm.mask)
       {
               display_form (mamain3, &UbForm, 0, 0);
       }
/*
       hdc = GetDC (mamain3);
       ShowDataForms (hdc);
       ReleaseDC (mamain3, hdc);
*/
}

void DlgClass::TestTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
	   SCROLLINFO scinfo;

       if (TrackNeeded ())
       {
           CreateTrack ();
       }
       else
       {
           DestroyTrack ();
       }
       if (hTrack == NULL) return;

       GetClientRect (mamain2, &rect);
       x = 0;
       y = rect.bottom - 16;
       cy = 16;
       cx = rect.right;
       MoveWindow (hTrack, x, y, cx, cy, TRUE);
//       MoveWindow (hTrack, x, y, cx, cy, FALSE);
	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE;
       scinfo.nPage  = 1;
       SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
}

void DlgClass::TestVTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
	   SCROLLINFO scinfo;

       if (VTrackNeeded ())
       {
           CreateVTrack ();
       }
       else
       {
           DestroyVTrack ();
       }
       if (vTrack == NULL) return;

       GetClientRect (mamain2, &rect);

       x = rect.right - 16;
       cx = 16;
       y = 0;
       cy = rect.bottom;
       if (hTrack)
       {
                 cy -= 16;
       }
       MoveWindow (vTrack, x, y, cx, cy, TRUE);
//       MoveWindow (vTrack, x, y, cx, cy, FALSE);
	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE;
       scinfo.nPage  = 1;
       SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
}


void DlgClass::MoveListWindow (void)
/**
Listfenster oeffnen.
**/
{
       int x,y;
       int cx,cy;
       RECT rect;

       if (mamain2 == NULL) return;

       GetPageLen ();
       y = LineRow + 5;
       x = 1;
       GetClientRect (hMainWindow, &rect);
       cy = rect.bottom - y - 5;
       cx = rect.right - 2;

       MoveWindow (mamain2, x, y, cx, cy, TRUE);

//       MoveWindow (mamain2, x, y, cx, cy, FALSE);

       if (hwndTB == NULL) return;
       GetClientRect (hwndTB, &rect);
       MoveWindow (hwndTB, 0, 0, cx + 2, rect.bottom, TRUE);

       TestMamain3 ();
       TestTrack ();
       TestVTrack ();
}


BOOL DlgClass::TrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT frmrect;
        RECT winrect;

		if (banz == 0) return FALSE;
        GetFrmRect (&UbForm, &frmrect);
        GetClientRect (mamain2, &winrect);
        if (frmrect.right > winrect.right)
        {
                    return TRUE;
        }
        return TRUE;
}

void DlgClass::CreateTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
        RECT rect;
		SCROLLINFO scinfo;

        if (hTrack) return;

        GetClientRect (mamain2, &rect);

        x = 0;
        y = rect.bottom - 16;
        cy = 16;
        cx = rect.right;

        hTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_HORZ,
                               x, y, cx, cy,
                               mamain2,
                               (HMENU) 701,
                               hMainInst,
                               0);


		 scinfo.cbSize = sizeof (SCROLLINFO);
		 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		 scinfo.nMin   = 0;
		 scinfo.nMax   = banz - 1;
		 scinfo.nPage  = 0;
         SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
}

void DlgClass::DestroyTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (hTrack)
          {
              DestroyWindow (hTrack);
              hTrack = NULL;
          }
}

BOOL DlgClass::VTrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT winrect;
        int cy;

        GetClientRect (mamain2, &winrect);

// recanz + 1 wegen Ueberschrift 

        cy = (recanz + 1) * tm.tmHeight;
        if (hTrack) 
        {

// untere Bildlaufleiste abziehen

            cy += 16; 
        }
        if (cy > winrect.bottom)
        {
                    return TRUE;
        }
        return FALSE;
}

void DlgClass::CreateVTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
        RECT rect;
		SCROLLINFO scinfo;

        if (vTrack) return;
        GetClientRect (mamain2, &rect);

        x = rect.right - 16;
        cx = 16;
        y = 0;
        cy = rect.bottom;
        if (hTrack)
        {
                 cy -= 16;
        }

        vTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_VERT,
                               x, y, cx, cy,
                               mamain2,
                               (HMENU) 702,
                               hMainInst,
                               0);
		 scinfo.cbSize = sizeof (SCROLLINFO);
		 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		 scinfo.nMin   = 0;
		 scinfo.nMax   = recanz - 1;
		 if (lPagelen)
		 {
		     scinfo.nPage  = 1;
		 }
		 else
		 {
			 scinfo.nPage = 0;
		 }
         SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
}

void DlgClass::DestroyVTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (vTrack)
          {
              DestroyWindow (vTrack);
              vTrack = NULL;
          }
}

BOOL DlgClass::IsInListArea (void)
/**
MousePosition Pruefen unf Focus setzen.
**/
{
	    POINT mousepos;
		RECT rect;
		int x1, y1;
		int x2, y2;
		int i, j;
		int diff;

        diff = 0;
        if (LineForm.frmstart)
		{
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
		}
        GetCursorPos (&mousepos);
		GetWindowRect (mamain3, &rect);
		rect.top += tm.tmHeight;

		if (mousepos.x < rect.left ||
			mousepos.x > rect.right)
		{
			        return FALSE;
		}
		if (mousepos.y < rect.top ||
			mousepos.y > rect.bottom)
		{
			        return FALSE;
		}

        ScreenToClient (mamain3, &mousepos);

		for (i = 0; i < recanz; i ++)
		{
			    if (i > lPagelen) break;
			    y1 = (i + 1) * tm.tmHeight;
				y2 = y1 + tm.tmHeight;
				if (mousepos.y < y1 ||
					mousepos.y > y2)
				{
					continue;
				}
				for (j = 0; j < banz; j ++)
				{
					x1 = (DataForm.mask[j].pos[1] - diff) *
						 tm.tmAveCharWidth;
					x2 = x1 + (DataForm.mask[j].length * tm.tmAveCharWidth);
 		    		if (mousepos.x > x1 &&
			    		mousepos.x < x2)
					{
					        break;
					}
				}
	    		if (mousepos.x > x1 &&
		    		mousepos.x < x2)
				{
					break;
				}
		}
   		if (mousepos.x < x1 ||
    		mousepos.x > x2)
		{
				return FALSE;
		}

		if (i + scrollpos >= recanz) return FALSE;
		if (j >= banz) return FALSE;
		AktRow = i + scrollpos;
		AktColumn = j;
        InvalidateRect (mamain3, &FocusRect, TRUE);
	    UpdateWindow (mamain3);
		SetFeldFocus0 (AktRow, AktColumn);
		return TRUE;
}

void DlgClass::DestroyFocusWindow ()
/**
Editfenster loeschen.
**/
{
	       if (FocusWindow)
		   {
			   DestroyWindow (FocusWindow);
			   FocusWindow = NULL;
		   }
}

void DlgClass::SetFeldEdit (int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  int x,y ,cx, cy;
		  int diff;
		  HANDLE hInstance;
		  static HFONT hFont = NULL;

          if (banz == 0) return;

          hInstance = (HANDLE) GetWindowLong (mamain3, 
                                               GWL_HINSTANCE);
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
		  }
          strcpy (AktItem, ziel[spalte].feldname);
		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);

		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());


		  rect.top = (zeile - scrollpos + 1) * tm.tmHeight - 2;
		  rect.bottom = rect.top + tm.tmHeight;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
		  rect.right = rect.left + DataForm.mask[spalte].length *
			          tm.tmAveCharWidth;  
 		  memcpy (&FocusRect, &rect, sizeof (RECT));

		  y  = (zeile - scrollpos + 1) * tm.tmHeight;
		  if (y < tm.tmHeight) return;
/*
		  cy = tm.tmHeight + 6;
		  x  = (DataForm.mask[spalte].pos[1] - diff) *
			   tm.tmAveCharWidth;
		  cx = DataForm.mask[spalte].length *
			           tm.tmAveCharWidth;  

          FocusWindow = CreateWindow ("Edit",
                                    "",
                                    WS_DLGFRAME | WS_CHILD, 
									x, y, cx, cy,
                                    mamain3,
                                    NULL,
                                    hInstance,
                                    NULL);
*/
		  cy = tm.tmHeight + 2;
		  x  = (DataForm.mask[spalte].pos[1] - diff) *
			   tm.tmAveCharWidth;
		  cx = DataForm.mask[spalte].length *
			           tm.tmAveCharWidth;  

          FocusWindow = CreateWindow ("Edit",
                                    "",
                                    WS_BORDER | WS_CHILD, 
									x, y, cx, cy,
                                    mamain3,
                                    NULL,
                                    hInstance,
                                    NULL);
  		  if (hFont == NULL)
		  {
	           	stdfont ();
		        hFont = SetWindowFont (mamain3);
		  }
          SendMessage (FocusWindow,  WM_SETFONT, (WPARAM) hFont, 0);
		  ShowWindow (FocusWindow, SW_SHOWNORMAL);
		  UpdateWindow (FocusWindow);
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowWindowText (rect.left, rect.top);
          SendMessage (FocusWindow, EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
}


void DlgClass::SetFeldFrame (HDC hdc, int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  HBRUSH hBrush;
		  int diff;
		  
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
		  }
          strcpy (AktItem, ziel[spalte].feldname);
		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);
		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());
		  rect.top = (zeile - scrollpos + 1) * tm.tmHeight;
		  rect.bottom = rect.top + tm.tmHeight;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
		  rect.right = rect.left + DataForm.mask[spalte].length *
			          tm.tmAveCharWidth;  
		  memcpy (&FocusRect, &rect, sizeof (RECT));


          hBrush = CreateSolidBrush (BLUECOL);

          SelectObject (hdc, hBrush);
          SelectObject (hdc, GetStockObject (NULL_PEN));
          Rectangle (hdc,rect.left, rect.top, rect.right, rect.bottom);
          DeleteObject (hBrush); 
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowFocusText (hdc, rect.left, rect.top);
}


void DlgClass::SetFeldFocus (HDC hdc, int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  HBRUSH hBrush;
		  int diff;


		  DestroyFocusWindow ();
		  if (ListFocus == 0) return;

		  if (ListFocus == 2)
		  {
		           SetFeldFrame (hdc, zeile, spalte);
		           return;
		  }

		  if (ListFocus == 3)
		  {
		           SetFeldEdit (zeile, spalte);
		           return;
		  }

          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
		  }
          strcpy (AktItem, ziel[spalte].feldname);
		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);
		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());
		  rect.top = (zeile - scrollpos + 1) * tm.tmHeight;
		  rect.bottom = rect.top + tm.tmHeight;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
		  rect.right = rect.left + DataForm.mask[spalte].length *
			          tm.tmAveCharWidth;  
		  memcpy (&FocusRect, &rect, sizeof (RECT));


          hBrush = CreateSolidBrush (BLUECOL);
          SelectObject (hdc, GetStockObject (PS_DASH));
          FrameRect (hdc, &rect, hBrush);
          DeleteObject (hBrush);
		  AktRow    = zeile;
          AktColumn = spalte;
}

int DlgClass::DelRec (void)
/**
Satz aus Liste loeschen.
**/
{
          int i;
 		  SCROLLINFO scinfo;
          
          if (ListFocus == 0) return recanz;

          recanz --;
          for (i = AktRow; i < recanz; i ++)
          {
              SwSaetze[i] = SwSaetze[ i + 1];
          }
          
          if (mamain3)
          {
             SendMessage (mamain2, WM_SIZE, NULL, NULL);
             InvalidateRect (mamain3, 0, TRUE);
             TestVTrack ();
             if (vTrack)
             {
 		          scinfo.cbSize = sizeof (SCROLLINFO);
		          scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		          scinfo.nMin   = 0;
		          scinfo.nMax   = recanz - 1;
		          if (lPagelen)
                  {
		             scinfo.nPage  = 1;
                  }
		          else
                  {
			         scinfo.nPage = 0;
                  }
                  SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
             }
          }
          
          return recanz;
}
          

void DlgClass::FocusLeft (void)
/**
Focus nachh rechts setzen.
**/
{
		  if (AktColumn <= 0) return;
	      AktColumn --;
		  if (AktColumn < DataForm.frmstart) 
          {
              ScrollLeft ();
          }
          else
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
		      UpdateWindow (mamain3);
          }
		  SetFeldFocus0 (AktRow, AktColumn);
}
		  

void DlgClass::FocusRight (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;
          int x;
          int diff;
          int Scrolled;

		  if (AktColumn >= banz - 1) return;

		  AktColumn ++;
          GetWindowRect (mamain3, &rect);


          diff = Scrolled = 0;
          if (LineForm.frmstart)
          {
                   diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                          fSatzNr.mask[0].length;
          }
          x = (DataForm.mask[AktColumn].pos[1] - diff + 
               DataForm.mask[AktColumn].length) *
                   tm.tmAveCharWidth;
          if (x <= rect.right)
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
	  	      UpdateWindow (mamain3);
 		      SetFeldFocus0 (AktRow, AktColumn);
              return;
          }
          ScrollRight ();
          diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                 fSatzNr.mask[0].length;
          x = (DataForm.mask[AktColumn].pos[1] - diff + 
               DataForm.mask[AktColumn].length) *
                   tm.tmAveCharWidth;
          while (x > rect.right)
          {
              ScrollRight ();
              diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                          fSatzNr.mask[0].length;
               x = (DataForm.mask[AktColumn].pos[1] - diff + 
                       DataForm.mask[AktColumn].length) *
                       tm.tmAveCharWidth;
          }
	      SetFeldFocus0 (AktRow, AktColumn);
}
		  
	        
void DlgClass::FocusUp (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;
          
		  if (AktRow <= 0) return;

          GetClientRect (mamain3, &rect);
          rect.top += tm.tmHeight;
          rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		  AktRow --;
		  if (AktRow < scrollpos)
          {
                    InvalidateRect (mamain3, &rect, TRUE);
                    ScrollUp (); 
          }
          else
          {
                    InvalidateRect (mamain3, &FocusRect, TRUE);
                    UpdateWindow (mamain3);
          }
		  SetFeldFocus0 (AktRow, AktColumn);
}

void DlgClass::FocusDown (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;

		  if (AktRow >= recanz - 1) return;

          GetClientRect (mamain3, &rect);
          rect.top += tm.tmHeight;
          rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		  AktRow ++;

          if (lPagelen < recanz && 
			  AktRow - scrollpos >= lPagelen - 1)
          {
              InvalidateRect (mamain3, &rect, TRUE);
              ScrollDown ();
          }
          else
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
	   	      UpdateWindow (mamain3);
          }
	      SetFeldFocus0 (AktRow, AktColumn);
}
  
    

void DlgClass::SetFeldFocus0 (int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	    HDC hdc;

		hdc = GetDC (mamain3);
        SetFeldFocus (hdc, zeile, spalte);
        ReleaseDC (mamain3, hdc);
}


void DlgClass::SetDataStart (void)
/**
frmstart bei DataForms erhoehen.
**/
{

        DataForm.frmstart = UbForm.frmstart;
        LineForm.frmstart = UbForm.frmstart;
}

BOOL DlgClass::MustPaint (int zeile)
/**
Feststellen, ob der Bereich neu gezeichnet werden muss.
**/
{
         int py;
         PAINTSTRUCT *ps;
         int Painttop;
         int Paintbottom;

         ps = &aktpaint;

/* Update-Region ermitteln.                              */

         py =  zeile;

         Painttop    = ps->rcPaint.top    -  tm.tmHeight;
         Paintbottom = ps->rcPaint.bottom +  tm.tmHeight;

         if (py < Painttop) return FALSE;
         if (py > Paintbottom) return FALSE;
         return TRUE;
}

void DlgClass::PrintVlineSatzNr (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int x, y;


        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;
        y = (lPagelen + 1) * tm.tmHeight;
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, GRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 2;
        y = (lPagelen + 1) * tm.tmHeight;
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}

void DlgClass::PrintVlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        if (LineForm.mask == NULL) return;

        PrintVlineSatzNr (hdc);

        diff = 0;
        if (LineForm.frmstart)
        {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
        }
        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (lPagelen + 1) * tm.tmHeight;
              MoveToEx (hdc, x, tm.tmHeight, NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}

void DlgClass::PrintHlinesSatzNr (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int i;
        int x, y;

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;

        hPen  = CreatePen (PS_SOLID, 0, BLACKCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i < lPagelen; i ++)
        {
              SelectObject (hdc, hPenW);
              y = (i + 1) * tm.tmHeight;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }

              SelectObject (hdc, hPen);
              y = (i + 2) * tm.tmHeight;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }

              SelectObject (hdc, hPenG);
              y --;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}

void DlgClass::PrintHlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int i;
        int x, y;

        GetClientRect (mamain3, &rect);
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i <= lPagelen; i ++)
        {

              y = (i + 1) * tm.tmHeight;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        PrintHlinesSatzNr (hdc);
}

void DlgClass::FillFrmRow (char *dest, form *frm)
/**
String fuellen.
**/
{
        int i;
        char buffer [1024];

        memset (dest, 0, 0x1000);

        strcat (dest, " ");
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
                   ToFormat (buffer, &frm->mask[i]);
                   strcat (dest, buffer);           
        }
}


void DlgClass::ShowFrmRow (HDC hdc, int zeile)
/**
Zeile anzeigen.
**/
{
         int y;
         int x;

         y = (zeile + 1) * tm.tmHeight;
         x = fSatzNr.mask[0].length * tm.tmAveCharWidth;

         if (MustPaint (y))
         {
                 SetTextColor (hdc,BLACKCOL);
                 SetBkColor (hdc,LTGRAYCOL);
                 TextOut (hdc, 0, y, SatzNr, strlen (SatzNr));

                 SetTextColor (hdc,BLACKCOL);
                 SetBkColor (hdc,WHITECOL);
                 TextOut (hdc, x, y, frmrow, strlen (frmrow));
         }
}


void DlgClass::ShowDataForms (HDC hdc)
/**
Seite anzeigen
**/
{
        int i;
        int pos;

        if (DataForm.mask == NULL) return;

        for (i = 0, pos = scrollpos; i < lPagelen; i ++, pos ++)
        {
                  if (pos == recanz) break;
                  memcpy (ausgabesatz, SwSaetze [pos], zlen); 
                  sprintf (SatzNr, "%5ld ", pos + 1);
                  FillFrmRow (frmrow, &DataForm);
                  ShowFrmRow (hdc, i);
        }
}

void DlgClass::GetFocusText (void)
/**
Focus-Text aus Fenster holen.
**/
{
	    char buffer [1024];
		int i;
		char *adr;

		i = AktColumn;
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
        GetWindowText (FocusWindow, buffer, 1024);
		adr = feldadr (ziel[i].feldname);

		mov (ziel[i].feldname, "%s", buffer);
	    strcpy (AktValue, 
			      DataForm.mask[AktColumn].item->GetFeldPtr ());

        memcpy (SwSaetze [AktRow], ausgabesatz, zlen); 
}
	 

void DlgClass::ShowWindowText (int x, int y)
/**
Focus-Text anzeigen.
**/
{
	    char buffer [1024];
		char dest[1024];

		strcpy (dest, " "); 
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
//        strcpy (buffer, feldadr (ziel[AktColumn].feldname));
        ToFormat (buffer, &DataForm.mask[AktColumn]);
		strcat (dest, buffer);
        SetWindowText (FocusWindow, buffer);
		SetFocus (FocusWindow);
}

void DlgClass::ShowFocusText (HDC hdc, int x, int y)
/**
Focus-Text anzeigen.
**/
{
	    char buffer [1024];
		char dest[1024];

		strcpy (dest, " "); 
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
        ToFormat (buffer, &DataForm.mask[AktColumn]);
		strcat (dest, buffer);
        SetTextColor (hdc,WHITECOL);
        SetBkColor (hdc,BLUECOL);
        TextOut (hdc, x, y, dest, strlen (dest) - 1);
}



void DlgClass::ScrollLeft (void)
/**
Ein Feld nach rechts scrollen.
**/
{


         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;

         if(UbForm.frmstart > 0)
         {
                     UbForm.frmstart --;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}



void DlgClass::ScrollRight (void)
/**
Ein Feld nach links scrollen.
**/
{


         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;

         if(UbForm.frmstart < banz - 1)
         {
                     UbForm.frmstart ++;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}

void DlgClass::ScrollPageUp (void)
/**
Eine Seite nach links scrollen.
**/
{

         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;

         UbForm.frmstart -= 5;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         if(UbForm.frmstart < 0) 
         {
                     UbForm.frmstart = 0;
         }
         SetDataStart ();

         CloseControls (&UbForm);
         display_form (mamain3, &UbForm, 0, 0);
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                       TRUE);
}


void DlgClass::ScrollPageDown (void)
/**
Eine Seite nach links scrollen.
**/
{

         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;

         UbForm.frmstart += 5;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         if(UbForm.frmstart > banz - 1) 
         {
                     UbForm.frmstart = banz - 1;
         }
         SetDataStart ();

         CloseControls (&UbForm);
         display_form (mamain3, &UbForm, 0, 0);
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                       TRUE);
}

void DlgClass::SetPos (int pos)
/**
Position setzen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;

         if (pos >= 0 && pos <= banz - 1)
         {
                     UbForm.frmstart = pos;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
                     InvalidateRect (mamain3, &rect, TRUE);
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}
                      
void DlgClass::SetTop ()
/**
Position setzen.
**/
{
          
          RECT rect;

          GetClientRect (mamain3, &rect);
          rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;

          UbForm.frmstart = 0;
          SetDataStart ();
          CloseControls (&UbForm);
          display_form (mamain3, &UbForm, 0, 0);
		  if (FocusWindow)
		  {
					 DestroyFocusWindow ();
		  }
          InvalidateRect (mamain3, &rect, TRUE);
          SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
}

                      
void DlgClass::SetBottom ()
/**
Position setzen.
**/
{
           
          RECT rect;

          GetClientRect (mamain3, &rect);

          UbForm.frmstart = banz - 1;;
          SetDataStart ();
          CloseControls (&UbForm);
          display_form (mamain3, &UbForm, 0, 0);
    	  if (FocusWindow)
		  {
					 DestroyFocusWindow ();
		  }
          InvalidateRect (mamain3, &rect, TRUE);
          SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
}
                      

void DlgClass::HScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
         switch (LOWORD (wParam))
         {
             case SB_LINEDOWN :
             {    
                     ScrollRight ();
                     break;
             }
             case SB_LINEUP :
             {    
                     ScrollLeft ();
                     break;
             }
             case SB_PAGEDOWN :
             {    
                     ScrollPageDown ();
                     break;
             }
             case SB_PAGEUP :
             {    
                     ScrollPageUp ();
                     break;
             }
             case SB_THUMBPOSITION :
                     SetPos (HIWORD (wParam));
                     break;
             case SB_TOP :
                     SetTop ();
                     break;
             case SB_BOTTOM :
                     SetBottom ();
                     break;
         }
}


void DlgClass::SetVPos (int pos)
/**
Position setzen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.top = tm.tmHeight;

         if (pos >= 0 && pos <= recanz - 1)
         {
                     scrollpos = pos;
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}
                      
void DlgClass::EditScroll (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;

          DestroyFocusWindow (); 
          GetClientRect (mamain3, &rect);
          rect.top += tm.tmHeight;
          rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;

          InvalidateRect (mamain3, &rect, TRUE);
}
  

void DlgClass::ScrollDown (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;
         RECT newrows;

         GetClientRect (mamain3, &rect);
         GetClientRect (mamain3, &newrows);
         rect.top = tm.tmHeight;

         newrows.top = newrows.bottom - tm.tmHeight;

         if(scrollpos < recanz - 1)
         {
                     scrollpos ++;
					 if (FocusWindow)
					 {
						 DestroyFocusWindow ();
					 }

                     ScrollWindow (mamain3, 0, -tm.tmHeight,
                                            &rect, &rect);

                     InvalidateRect (mamain3, &newrows, TRUE);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}
         
void DlgClass::ScrollUp (void)
/**
Ein Feld nach unten scrollen.
**/
{

         RECT rect;
         RECT newrows;

         GetClientRect (mamain3, &rect);
         GetClientRect (mamain3, &newrows);

         rect.top = tm.tmHeight;
         newrows.top = tm.tmHeight;
         newrows.bottom = newrows.top + tm.tmHeight;

         if(scrollpos > 0)
         {
                     scrollpos --;
					 if (FocusWindow)
					 {
						 DestroyFocusWindow ();
					 }

                     ScrollWindow (mamain3, 0, tm.tmHeight,
                                            &rect, &rect);

                     InvalidateRect (mamain3, &newrows, TRUE);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}

void DlgClass::ScrollVPageDown (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.top = tm.tmHeight;

         scrollpos += lPagelen - 1;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         if(scrollpos > recanz - 1)
         {
                     scrollpos = recanz - 1;
         }

         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
}

void DlgClass::ScrollVPageUp (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.top = tm.tmHeight;

         scrollpos -= lPagelen - 1;

         if(scrollpos < 0)
         {
                     scrollpos = 0;
         }

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
}

void DlgClass::VScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
         switch (LOWORD (wParam))
         {
             case SB_LINEDOWN :
             {    
                     ScrollDown ();
                     break;
             }
             case SB_LINEUP :
             {    
                     ScrollUp ();
                     break;
             }

             case SB_PAGEDOWN :
             {    
                     ScrollVPageDown ();
                     break;
             }
             case SB_PAGEUP :
             {    
                     ScrollVPageUp ();
                     break;
             }
             case SB_THUMBPOSITION :
                     SetVPos (HIWORD (wParam));
                     break;

             case SB_TOP :
                     SetTop ();
                     break;
             case SB_BOTTOM :
                     SetBottom ();
                     break;
         }
}

void DlgClass::FunkKeys (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
#ifndef _NINFO
 		 char where [256];
#endif

		 if (FocusWindow)
		 {
			 GetFocusText ();
		 }

         switch (LOWORD (wParam))
         {
             case VK_F2 :
#ifndef _NINFO
  		            sprintf (where, "where %s = %s",
							  GetAktItem (), 
							  GetAktValue ()); 
					_CallInfoEx (hMainWindow, NULL,
							     GetAktItem (), 
								 where, 0l);
#endif
					 break; 
             case VK_F3 :
				     ListFocus = 1;
                     InvalidateRect (mamain3, &FocusRect, TRUE);
	                 UpdateWindow (mamain3);
					 break;
             case VK_F4 :
				     ListFocus = 2;
                     InvalidateRect (mamain3, &FocusRect, TRUE);
	                 UpdateWindow (mamain3);
					 break;
             case VK_F6 :
				     ListFocus = 0;
                     InvalidateRect (mamain3, &FocusRect, TRUE);
	                 UpdateWindow (mamain3);
					 break;
             case VK_F7 :
				     ListFocus = 3;
                     InvalidateRect (mamain3, &FocusRect, TRUE);
	                 UpdateWindow (mamain3);
					 break;
             case VK_F11 :
                      syskey = KEY11; 
                      testkeys ();
                      break;
             case VK_F12 :
                      syskey = KEY12; 
                      testkeys ();
                      break;
             case VK_RIGHT :
             {    
				     if (ListFocus == 0)
					 {
                            ScrollRight ();
					 }
					 else
					 {
				            FocusRight ();
					 }
                     break;
             }
             case VK_LEFT :
             {    
				     if (ListFocus == 0)
					 {
                            ScrollLeft ();
					 }
					 else
					 {
				            FocusLeft ();
					 }
                     break;
             }
             case VK_DOWN :
             case VK_RETURN :
             {    
				     if (ListFocus == 0)
					 {
                            ScrollDown ();
					 }
					 else
					 {
				            FocusDown ();
					 }
                     break;
             }
             case VK_UP :
             {    
				     if (ListFocus == 0)
					 {
                            ScrollUp ();
					 }
					 else
					 {
				            FocusUp ();
					 }
                     break;
             }

             case VK_NEXT :
             {    
                     ScrollVPageDown ();
                     break;
             }
             case VK_PRIOR :
             {    
                     ScrollVPageUp ();
                     break;
             }

             case VK_HOME :
                     SetTop ();
                     break;
             case VK_END :
                     SetBottom ();
                     break;
             case VK_TAB :
                     if (GetKeyState (VK_SHIFT) < 0)
                     {
						      FocusLeft ();
                     }
					 else
					 {
 			                  FocusRight ();
					 }
					 break;

         }
}

void DlgClass::FillUbForm (field *feld, char *feldname, int len, int spalte,
                 int BuId  )
/**
Maskenfeld fuer Ueberschruft fuellen.
**/
{
        memcpy (feld, &iButton, sizeof (field)); 
        clipped (feldname);
        feld->item       = new ITEM ("", feldname, "", 0);
        feld->length     = len;
        feld->pos[0]     = 0;
        feld->pos[1]     = spalte;
        feld->BuId       = BuId;
}


void DlgClass::FillDataForm (field *feld, char *feldname, int len, int spalte)
/**
Maskenfeld fuer Daten fuellen.
**/
{
        char *adr;

        memcpy (feld, &iEdit, sizeof (field)); 
        clipped (feldname);
        adr = feldadr (feldname);
        if (adr == NULL) return;
        feld->item       = new ITEM ("", adr, "", 0);
        feld->length     = len;
        feld->pos[0]     = 0;
        feld->pos[1]     = spalte;
}

void DlgClass::FillLineForm (field *feld, int spalte)
/**
Maskenfeld fuer Daten fuellen.
**/
{
        memcpy (feld, &iEdit, sizeof (field)); 
        feld->pos[1]     = spalte;
}

void DlgClass::GetPageLen (void)
/**
Seitenlaenge ermitteln.
**/
{
        RECT rect;
        HDC hdc;
        HFONT hFont;
        int y;

        stdfont ();
        hFont = SetWindowFont (mamain2);
        hdc = GetDC (mamain2);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DeleteObject (hFont);
        ReleaseDC (mamain2, hdc);

        GetClientRect (mamain2, &rect);
        y = rect.bottom - 19;
        lPagelen = y / tm.tmHeight; 
        if (lPagelen > recanz)
        {
                     lPagelen = recanz;
        }
}

void DlgClass::MakeLineForm (form *frm)
/**
Form fuer daten generieren.
**/
{
        int i;
        int spalte;
        int len;
        
        memcpy (frm, &IForm, sizeof (form));
        frm->fieldanz = banz;
        frm->mask = (field *) GlobalAlloc (0, (banz + 2) * sizeof (field));
        if (frm->mask == NULL)
        {
                 disp_mess ("Kein Speicherplatz", 2);
                 ExitProcess (1);
        }

        for (i = 0, spalte = 6; i < banz; i ++)
        {
                 len = ziel[i].feldlen + 3;
                 if (len > MAXLEN) len = MAXLEN;
                 spalte += len;
                 FillLineForm (&frm->mask[i], spalte);
        }
}

void DlgClass::MakeDataForm (form *frm, int i)
/**
Form fuer daten generieren.
**/
{
        int spalte;
        int len;
        
        memcpy (frm, &IForm, sizeof (form));
        frm->fieldanz = banz;
        frm->mask = (field *) GlobalAlloc (0, (banz + 2) * sizeof (field));
        if (frm->mask == NULL)
        {
                 disp_mess ("Kein Speicherplatz", 2);
                 ExitProcess (1);
        }
        spalte = 6;  
        len = 20;
        this->FillUbForm (&frm.mask[0], ziel[i].feldname, len, spalte);
        spalte += len;
        len = MAXLEN;
        FillDataForm (&frm->mask[1], ziel[i].feldname, len, spalte);
}

void DlgClass::MakeDataForm0 (void)
/**
Forms f�r Daten generieren.
**/
{

        GetPageLen ();
        MakeDataForm (&DataForm);
        MakeLineForm (&LineForm);
}


void DlgClass::MakeUbForm (void)
/**
Form fuer Ueberschrift generieren.
**/
{
        int i;
        int spalte;
        int BuId;
        int len;

        memcpy (&UbForm, &IForm, sizeof (form));
        UbForm.fieldanz = 2;
        UbForm.mask = (field *) GlobalAlloc (0, (2 + 2) * sizeof (field));
        if (UbForm.mask == NULL)
        {
                 disp_mess ("Kein Speicherplatz", 2);
                 ExitProcess (1);
        }
        BuId = 600;
        spalte = 6;
        len = 20;
        this->FillUbForm (&UbForm.mask[0], "Bezeichnung,len, spalte,BuId);
        spalte += len;
        len = MAXLEN;
        BuId ++;
        this->FillUbForm (&UbForm.mask[1], "Wert",len, spalte,
                              BuId);
        display_form (mamain3, &fUbSatzNr, 0, 0);
        display_form (mamain3, &UbForm, 0, 0);
        if (TrackNeeded ())
        {
                 CreateTrack ();
        }
}


void DlgClass::FreeDataForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&DataForm);
        if (DataForm.mask)
        {   
             for (i = 0; i < banz; i ++)
             {
                  if (DataForm.mask[i].item)
                  {
                            delete DataForm.mask[i].item;                 
                            DataForm.mask[i].item = NULL;
                  }
             }
             GlobalFree (DataForm.mask);
        }
        memcpy (&DataForm, &IForm, sizeof (form));
}

void DlgClass::FreeLineForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&LineForm);
        if (LineForm.mask)
        {
               for (i = 0; i < banz; i ++)
               {
                  if (LineForm.mask[i].item)
                  {
                            delete LineForm.mask[i].item;                 
                            LineForm.mask[i].item = NULL;
                  }
               }
               GlobalFree (LineForm.mask);
        }
        memcpy (&LineForm, &IForm, sizeof (form));
}

void DlgClass::FreeUbForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&UbForm);
        if (UbForm.mask)
        {
               for (i = 0; i < banz; i ++)
               {
                  if (UbForm.mask[i].item)
                  {
                            delete UbForm.mask[i].item;                 
                            UbForm.mask[i].item = NULL;
                  }
               }
               GlobalFree (UbForm.mask);
               
        }
        memcpy (&UbForm, &IForm, sizeof (form));
}

int DlgClass::IsUbRow (void)
/**
Test, ob der Mousecursor ueber der Feldueberschrift steht.
**/
{
        POINT mousepos;
        RECT rect;

        if (UbForm.mask == NULL) return FALSE;
        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);

        if (mousepos.y < 0 ||
            mousepos.y > tm.tmHeight)
        {
                   return FALSE;
        }
        if (mousepos.x > rect.left &&
            mousepos.x < rect.right)
        {
                   return TRUE;
        }
        return FALSE;
}


int DlgClass::IsUbEnd (void)
/**
Test, ob der Mousecursor ueber dem Ende einer Feldueberschrift steht.
**/
{
        POINT mousepos;
        RECT rect;
        int i;
        int diff;
        int x;

        if (LineForm.mask == NULL) return FALSE;

        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);

        diff = 0;
        if (LineForm.frmstart)
        {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
        }
        if (mousepos.y < 0 ||
            mousepos.y > tm.tmHeight)
        {
                   return FALSE;
        }

        if (mousepos.x <= rect.left ||
            mousepos.x >= rect.right)
        {
                   return FALSE;
        }

        for (i = UbForm.frmstart; i < UbForm.fieldanz; i ++)
        {
                   x = (UbForm.mask[i].pos[1] - diff) * tm.tmAveCharWidth;
                   if ((x - mousepos.x) < 4 && 
                       (x - mousepos.x) >= 0) 
                   {
                       if (i == 0)
                       {
                            continue;
                       }
                       else
                       {
                            movfield = i - 1;
                            return TRUE;
                       }
                   }
                   else if ((x - mousepos.x) < 0 && 
                            (x - mousepos.x) > -4) 
                   {
                            movfield = i - 1;
                            return TRUE;
                   }
        }
        x = (UbForm.mask[i - 1].pos[1] + UbForm.mask[i - 1].length 
             - diff) * tm.tmAveCharWidth;
        if ((x - mousepos.x) < 4 && 
            (x - mousepos.x) >= 0) 
        {
                    movfield = i - 1;
                    return TRUE;
        }
        else if ((x - mousepos.x) < 0 && 
                 (x - mousepos.x) > -4) 
        {
                    movfield = i - 1;
                    return TRUE;
        }
        return FALSE;
}


void DlgClass::ShowArrow (BOOL mode)
/**
Mousecursor als anzeigen.
**/
{

        if (mode == FALSE && IsArrow == FALSE)
        {
            return;
        }

        if (mode)
        {
                IsArrow = 1;
                if (arrow == 0)
                {
//                            arrow =  LoadCursor (NULL, IDC_SIZEWE);
                            arrow =  LoadCursor (hMainInst, 
                                                 "oarrow");
                }
                oldcursor = SetCursor (arrow);
                aktcursor = arrow;
        }
        else
        {
                IsArrow = 0;
                SetCursor (oldcursor);
                aktcursor = oldcursor;
        }
}


void DlgClass::StartMove (void)
{
        InMove = 1; 
}

void DlgClass::DestroyField (int movfield)
/**
Ein Feld aus der Maske loeschen.
**/
{
        int i;
        int pos;
        RECT rect;

        if (UbForm.fieldanz <= 1) return;


        CloseControls (&UbForm);
        GetClientRect (mamain3, &rect);
        rect.top += tm.tmHeight;

        if (movfield > 0 && UbForm.mask[movfield].item)
        {
                  delete UbForm.mask[movfield].item;
                  UbForm.mask[movfield].item = NULL;
        }
        if (movfield > 0 && DataForm.mask[movfield].item)
        {
                  delete DataForm.mask[movfield].item;
                  DataForm.mask[movfield].item = NULL;
        }

        if (movfield > 1)
        {
                  if (LineForm.mask[movfield - 1].item)
                  {
                           delete LineForm.mask[movfield - 1].item;
                           LineForm.mask[movfield - 1].item = NULL;
                  }
        }

        pos = UbForm.mask[movfield].pos[1];
        for (i = movfield; i < UbForm.fieldanz - 1; i ++)
        {

                   memcpy (&ziel[i], &ziel[i + 1],
                           sizeof (FELDER));
                   memcpy (&UbForm.mask[i], &UbForm.mask[i + 1],
                           sizeof (field));
                   memcpy (&DataForm.mask[i], &DataForm.mask[i + 1],
                           sizeof (field));

                   if (i > 0)
                   {
                       memcpy (&LineForm.mask[i - 1], &LineForm.mask[i],
                               sizeof (field));
                   }

                   UbForm.mask[i].pos[1]   = pos;
                   DataForm.mask[i].pos[1] = pos;

                   if (i > 0)
                   {
                         LineForm.mask[i - 1].pos[1] = pos;
                   }

                   pos += UbForm.mask[i].length;
        }
        
        UbForm.fieldanz --;
        DataForm.fieldanz --;
        LineForm.fieldanz --;
        banz --;

        if (TrackNeeded ())
        {
           CreateTrack ();
           SetScrollRange (hTrack, SB_CTL, 0,
                                   banz - 1, TRUE);
        }
        else
        {
           DestroyTrack ();
        }
        if (hTrack == NULL) return;

        GetClientRect (mamain2, &rect);
        display_form (mamain3, &UbForm, 0, 0);
        InvalidateRect (mamain3, &rect, TRUE);
}

void DlgClass::StopMove (void)
{
        POINT mousepos;
        int x;
        int len;
        int diff;
        RECT rect;
        int i;

        InMove = 0;
        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);
        rect.top += tm.tmHeight;


        diff = 0;
        if (LineForm.frmstart)
        {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
        }

        if (movfield < UbForm.fieldanz - 1)
        {
                 x = (UbForm.mask[movfield + 1].pos[1] - diff) * 
                                 tm.tmAveCharWidth;
        }
        else
        {
                 x = (UbForm.mask[movfield].pos[1] + 
                      UbForm.mask[movfield].length - diff) *
                                 tm.tmAveCharWidth;
        }

        if ((x - mousepos.x) < 4 && 
            (x - mousepos.x) > -4) 
        {
                  return;
        }

        x = mousepos.x / tm.tmAveCharWidth + diff;

        if (movfield < UbForm.fieldanz - 1)
        {
                 diff = x - UbForm.mask[movfield + 1].pos[1]; 
        }
        else
        {
                 diff = x - (UbForm.mask[movfield].pos[1] + 
                             UbForm.mask[movfield].length); 
        }

        
        len = UbForm.mask[movfield].length + diff;

        if (len <= 0)
        {
    		if (FocusWindow)
			{
			     DestroyFocusWindow ();
			}
            DestroyField (movfield);
            return;
        }


        if (movfield < UbForm.fieldanz - 1)
        {
             UbForm.mask[movfield + 1].pos[1] = x;
             DataForm.mask[movfield + 1].pos[1] = x;
             LineForm.mask[movfield].pos[1] = x;

             for ( i = movfield + 2; i < UbForm.fieldanz; i ++)
             {
                  UbForm.mask[i].pos[1]   += diff;
                  DataForm.mask[i].pos[1] += diff;
                  LineForm.mask[i - 1].pos[1] += diff;
             }
        }
        LineForm.mask[LineForm.fieldanz - 1].pos[1] += diff;

        UbForm.mask[movfield].length = len;
        DataForm.mask[movfield].length = len;

		if (FocusWindow)
		{
			DestroyFocusWindow ();
		}

        CloseControls (&UbForm);
        display_form (mamain3, &UbForm, 0, 0);
        InvalidateRect (mamain3, &rect, TRUE);
}
        

BOOL DlgClass::IsMouseMessage (MSG *msg)
/**
Mouse-Meldungen pruefen.
**/
{
        switch (msg->message)
        {
        
              case WM_MOUSEMOVE :
                      if (InMove)
                      {
                            SetCursor (aktcursor);
                            return TRUE;
                      }
                      if (IsUbEnd ())
                      {
                              ShowArrow (TRUE);
                      }
                      else
                      {
                              ShowArrow (FALSE);
                      }
                      return FALSE;
              case WM_LBUTTONDOWN :
                      if (IsUbRow () == FALSE)
                      {
                              return IsInListArea ();
                      }
                      if (aktcursor == arrow)
                      {
                                SetCursor (aktcursor);
                                StartMove ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      return TRUE;
              case WM_LBUTTONDBLCLK :
                      if (IsUbRow () == FALSE)
                      {
                              return FALSE;
                      }
                      if (aktcursor == arrow)
                      {
                                SetCursor (aktcursor);
                                StartMove ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      return TRUE;
              case WM_LBUTTONUP :
                      if (InMove)
                      {
                                StopMove ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      if (IsUbRow ())
                      {
                              return TRUE;
                      }
                      else
                      {
                              return FALSE;
                      }
                      if (IsUbRow ())
                      {
                              return TRUE;
                      }
                      else
                      {
                              return FALSE;
                      }
        }
        return FALSE;
}

void DlgClass::OnPaint (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         HDC hdc;

         if (hWnd == mamain2)
         {
                    MoveListWindow ();
         }
         else if (hWnd == mamain3)
         {
                    hdc = BeginPaint (mamain3, &aktpaint);
                    ShowDataForms (hdc);
                    PrintVlines (hdc);
                    PrintHlines (hdc);
					if (! FocusWindow)
					{
                	     SetFeldFocus (hdc, AktRow, AktColumn);
					}
                    EndPaint (mamain3,&aktpaint);
         }
}

void DlgClass::OnSize (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         return;
}


int DlgClass::ProcessMessages(void)
{
        MSG msg;
        char KeyState [256];
        PBYTE lpKeyState; 

		AktRow = 0;
		AktColumn = 0;
		SetFeldFocus0 (AktRow, AktColumn);
        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                       return msg.wParam;
                              case VK_F3 :
                              case VK_F4 :
                              case VK_F6 :
                              case VK_F7 :
                              case VK_F8 :
                              case VK_F9 :
                              case VK_F10 :
                              case VK_F11 :
                              case VK_F12 :
                              case VK_DOWN :
                              case VK_RETURN:
                              case VK_UP :
                              case VK_NEXT :
                              case VK_PRIOR :
                              case VK_HOME :
                              case VK_END :
                              case VK_TAB :
								       FunkKeys (msg.wParam,
										         msg.lParam);
									   continue;
                              case 'A' :
                                 if (GetKeyState (VK_CONTROL) < 0)
                                 {
                                       lpKeyState = (PBYTE) KeyState; 
                                       GetKeyboardState (lpKeyState);
                                       lpKeyState [VK_CONTROL] = (char) 0;
                                       SetKeyboardState (lpKeyState);
                                       msg.wParam = '0';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       msg.wParam = 'X';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       msg.wParam = 'A';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       continue;
                                 }
                       }
             }
             if (IsMouseMessage (&msg))
             {
                       continue;
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
        }
        return msg.wParam;
}


// Ende DlgClass


void DlgClassDB::FreeBezTab (void)
/**
Speicher fuer feldbztab freigeben.
**/
{
        int i;

        for (i = 0; i < fbanz; i ++)
        {
            if (feldbztab[i])
            {
                GlobalFree (feldbztab[i]);
                feldbztab[i] = NULL;
            }
        }
        fbanz = 0;
}

void DlgClassDB::FillUbForm (field *feld, char *feldname, int len, int spalte,
                 int BuId  )
/**
Maskenfeld fuer Ueberschruft fuellen.
**/
{
#ifndef _NINFO
        char *bez;
#endif
        char *name;

        name = feldname;
        memcpy (feld, &iButton, sizeof (field)); 
#ifndef _NINFO
        bez = GetItemName (feldname);
        if (bez)
        {
                   feldbztab[fbanz] = (char *) GlobalAlloc (0, 21);
                   if (feldbztab[fbanz])
                   {
                       strcpy (feldbztab[fbanz], bez);
                       name = feldbztab [fbanz];
                       if (fbanz < MAXBEZ - 1)
                       {
                           fbanz ++;
                       }
                   }
        }
#endif
        clipped (name);
        feld->item       = new ITEM ("", name, "", 0);
        feld->length     = len;
        feld->pos[0]     = 0;
        feld->pos[1]     = spalte;
        feld->BuId       = BuId;
}

void DlgClassDB::MakeUbForm (void)
/**
Form fuer Ueberschrift generieren.
**/
{
        int i;
        int spalte;
        int BuId;
        int len;

        memcpy (&UbForm, &IForm, sizeof (form));
        UbForm.fieldanz = banz;
        UbForm.mask = (field *) GlobalAlloc (0, (banz + 2) * sizeof (field));
        if (UbForm.mask == NULL)
        {
                 disp_mess ("Kein Speicherplatz", 2);
                 ExitProcess (1);
        }
        BuId = 600;
        for (i = 0, spalte = 6; i < banz; i ++)
        {
                 len = ziel[i].feldlen + 3;
                 if (len > MAXLEN) len = MAXLEN;
                 this->FillUbForm (&UbForm.mask[i], ziel[i].feldname,
                              len, spalte,
                              BuId);
                 spalte += len;
                 BuId ++;
        }
        display_form (mamain3, &fUbSatzNr, 0, 0);
        display_form (mamain3, &UbForm, 0, 0);
        if (TrackNeeded ())
        {
                 CreateTrack ();
        }
}

