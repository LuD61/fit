#ifndef _CONTROLSPLUGIN_DEF
#define _CONTROLSPLUGIN_DEF
#include <windows.h>

class CControlsPlugin
{
public:
	enum
	{
		KommOnly = 0,
		LiefVisible = 1,
		LiefRechVisible = 2,
	};
	static HANDLE ControlsPlugin;
	int (*pGetPosTxtKz) (int);


	CControlsPlugin ();
	int GetPosTxtKz (int PosTxtKz);
	BOOL IsActive ();
};

#endif
