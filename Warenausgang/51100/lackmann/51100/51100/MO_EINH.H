#ifndef __EINH_DEF
#define __EINH_DEF

struct KE
{
          short me_einh_kun;
          char  me_einh_kun_bez [20];
          double inh;
          short me_einh_bas;
          char  me_einh_bas_bez [20];
          short me_einh1;
          double inh1;
          short me_einh2;
          double inh2;         
          short me_einh3;
          double inh3;
};

typedef struct KE KEINHEIT;


class EINH_CLASS
{
           private :
                int dsqlstatus;
                int AufEinh;
           public :
                EINH_CLASS () : AufEinh (1)
                {
                }
                int GetAufEinh (void)
                {
                    return AufEinh;
                }
                void SetAufEinh (int einh)
                {
                    AufEinh = einh;
                }
                void AktAufEinh (short, short, long, double, int);
                void NextAufEinh (short,short,long,double,int, 
                                  KEINHEIT *);
                double GetInh (int, double);
                void FillGeb0 (KEINHEIT *, int);
                void FillGeb1P (KEINHEIT *, int);
                void FillGeb1M (KEINHEIT *, int);
                void FillGeb2 (KEINHEIT *, int);
                void FillGeb (KEINHEIT *);
                void FillKmb (KEINHEIT *);
                int  ReadKmb (void);
                void GetKunEinh (short, short, long, double, KEINHEIT *);
                void GetBasEinh (double, KEINHEIT *);
                void GetKunEinhBas (double, KEINHEIT *);
                void FillOtab (int);
                void InitCombo (void);
                void ChoiseCombo (void);
                void ChoiseEinh (void);
};
#endif