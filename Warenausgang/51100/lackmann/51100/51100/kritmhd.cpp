#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "dbclass.h"
#include "listcl.h"
#include "kritmhd.h"

/* Variablen fuer Auswahl mit Buttonueberschrift    */

#include "itemc.h"

static DB_CLASS DbClass;
static AUFPLIST aufpListe;

#define MAXSORT 100
static int MaxSort = 100;

static char *sqltext;

static int dosort1 ();
static int dosort2 ();
static int dosort3 ();
static int dosort4 ();
static int dosort5 ();

CRON_BEST_CLASS CronBest;

struct ABAS {
   double    a;
   double    a_aufruf;
   short     mdn;
   short     fil;
   char      a_bz1[25];
   char      a_bz2[25];
   double    a_gew;
   short     a_typ;
   short     a_typ2;
   short     abt;
   long      ag;
   char      best_auto[2];
   char      bsd_kz[2];
   char      cp_aufschl[2];
   short     delstatus;
   short     dr_folge;
   long      erl_kto;
   char      hbk_kz[2];
   short     hbk_ztr;
   char      hnd_gew[2];
   short     hwg;
   char      kost_kz[3];
   short     me_einh;
   char      modif[2];
   short     mwst;
   short     plak_div;
   char      stk_lst_kz[2];
   double    sw;
   short     teil_smt;
   long      we_kto;
   short     wg;
   short     zu_stoff;
   long      akv;
   long      bearb;
   char      pers_nam[9];
   double    prod_zeit;
   char      pers_rab_kz[2];
   double    gn_pkt_gbr;
   long      kost_st;
   char      sw_pr_kz[2];
   long      kost_tr;
   double    a_grund;
   long      kost_st2;
   long      we_kto2;
   char      a_bz3[25];
   long      charg_hand;
   long      intra_stat;
   char      qual_kng[5];
   double    a_ersatz;
   short     a_ers_kz; 
   long      inh_ek; 
   double    a_leih;
   short     zerl_eti;
   char      smt[2];
   short     min_bestellmenge;
   short     staffelung;
   short     filialsperre;
   short     ghsperre;

};

static struct ABAS a_bas, a_bas_null;

HWND eWindow;
form *savecurrent;



struct SORT_AW
{
	     char sort1 [13];
	     char sort2 [49];
	     char sort3 [49];
	     char sort4 [49];
	     char sort5 [49];
	     int  sortidx;
};

 static struct SORT_AW sort_aw, sort_awtab[MAXSORT];
//static struct SORT_AW sort_aw, *sort_awtab;
static int sort_awanz;

/**
static void SortMalloc () 
//Speicher fuer Listbereich zuordnen.

{
	static BOOL SortOK = FALSE;
    long anz;

	if (SortOK) return;


	DbClass.sqlin ((short *) &a_bas.wg, 1, 0);
	DbClass.sqlout ((long *) &anz, 2, 0);
	DbClass.sqlcomm ("select count (*) from a_bas where wg = ? ");


	if (anz == 0l) return;

    sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   ((anz + 1) * sizeof (struct SORT_AW))); 
	while (sort_awtab == NULL)
	{
		if (anz == 0l) break;
		anz --;
        sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   (anz * sizeof (struct SORT_AW))); 
	}
	MaxSort = anz + 1;
	if (anz == 0l)
	{
		print_mess (2, "Es konnte kein Speicher fuer die Artikelauswahl zugewiesen werden");
		return;
	}
	SortOK = TRUE;

}
**/

static int sort1 = -1;
static int sort2 = -1;
static int sort3 = -1;
static int sort4 = -1;
static int sort5 = -1;
static int sort_idx = 0;

static ITEM isort1    ("", sort_aw.sort1, "", 0);
static ITEM isort2    ("", sort_aw.sort2, "", 0);
static ITEM isort3    ("", sort_aw.sort3, "", 0);
static ITEM isort4    ("", sort_aw.sort4, "", 0);
static ITEM isort5    ("", sort_aw.sort5, "", 0);
static ITEM ifillub   ("", " ",             "", 0); 

static field _fsort_aw[] = {
&isort1,      7, 1, 0,  0+1, 0, "",       NORMAL, 0, 0, 0,
&isort2,     28, 1, 0,  8+1, 0, "",          NORMAL, 0, 0, 0,
&isort3,     10, 1, 0, 37+1, 0, "",          NORMAL, 0, 0, 0,
&isort4,      9, 1, 0, 48+1, 0, "",          NORMAL, 0, 0, 0,
&isort5,      9, 1, 0, 58+1, 0, "",          NORMAL, 0, 0, 0,
&ifillub,    1, 1, 0, 68+1, 0, "",         NORMAL, 0, 0, 0,
};

static form fsort_aw = {6, 0, 0, _fsort_aw, 0, 0, 0, 0, NULL};

static ITEM isort1ub    ("", "Artikel",         "", 0);
static ITEM isort2ub    ("", "Bezeichnung",   "", 0);
static ITEM isort3ub    ("", "MHD",   "", 0);
static ITEM isort4ub    ("", "mit Preis",   "", 0);
static ITEM isort5ub    ("", "Ohne Preis",   "", 0);

static field _fsort_awub[] = {
&isort1ub,        9, 1, 0,  0, 0, "", BUTTON, 0, 0,    0,
&isort2ub,       29, 1, 0,  8, 0, "", BUTTON, 0, 0,    0,
&isort3ub,       11, 1, 0, 37, 0, "", BUTTON, 0, 0,    0,
&isort4ub,       10, 1, 0, 48, 0, "", BUTTON, 0, 0,    0,
&isort5ub,       10, 1, 0, 58, 0, "", BUTTON, 0, 0,    0,
&ifillub,        1, 1, 0, 68, 0, "", BUTTON, 0, 0, 0};

static form fsort_awub = {6, 0, 0, _fsort_awub, 0, 0, 0, 0, NULL};

static ITEM isortl ("", "1", "", 0);

static field _fsort_awl[] = {
&isortl,          1, 1, 0,  8, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 37, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 48, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 58, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 68, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awl = {5, 0, 0, _fsort_awl, 0, 0, 0, 0, NULL};


void SaveRect_kritmhd (void)
{
	    char *etc;
		char rectname [512];
		RECT rect;
		FILE *fp;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		sprintf (rectname, "%s\\%s.rct", etc,"51100Lackkritmhd");
		if (GetWindowRect (eWindow, &rect) ) 
		{
			fp = fopen (rectname, "w");
			fprintf (fp, "left    %d\n", rect.left);
			fprintf (fp, "top     %d\n", rect.top);
			fprintf (fp, "right   %d\n", rect.right - rect.left);
			fprintf (fp, "bottom  %d\n", rect.bottom - rect.top);
			fclose (fp);
		}
}

int KRITMHD_CLASS::CloseWindow (HWND hWnd)
{
	if (eWindow  !=  NULL)
	{
		/**
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
							 **/

//		SetMouseLock (FALSE);
        CloseUbControls (); 
	    CloseEWindow (eWindow);
		eWindow = NULL;
//        SetListFont (FALSE);
        SetDblClck (NULL, 1);
  //      SetAktivWindow (hWnd);
//        SetListEWindow (1);
//		SetHLines (0);
//		SetVLines (TRUE);
//		SetMouseLock (TRUE);
	}
	return 0;
}


static void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
		if (strcmp (sort_awtab[idx].sort1, "") == 0) return ;
		SaveRect_kritmhd ();
        sort_idx = idx;
	    current_form = savecurrent;
		SetMouseLock (FALSE);
//        CloseUbControls (); 
//        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
	    a_bas.a = atoi(sort_awtab[sort_idx].sort1); 
        if (syskey == KEYESC || syskey == KEY5)
        {
				    a_bas.a = (double) 0;  
        }
		else
		{
			aufpListe.AppendLine(a_bas.a); 
		}
		restore_fkt (5); 
		/***
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
		***/




//        break_list ();
        return; 
}

static int endsort (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}


 
int KRITMHD_CLASS::prep_cursor ()
/**
Cursor fuer Auswahl vorbereiten.
**/
{
	int cursor_mhd = -1;

	if (ModusBestand == FALSE)
	{ 
         DbClass.sqlin ((short *) &a_bas.wg, 1, 0);
         DbClass.sqlin ((long *) &KritischeMHD, 2, 0);
         DbClass.sqlin ((long *) &aufk.auf, 2, 0);
         DbClass.sqlout ((double *) &a_bas.a, 3, 0);
         DbClass.sqlout ((char *) a_bas.a_bz1, 0, 25);
         DbClass.sqlout ((long *) &CronBest.cron_best.mhd, 2, 0);
         DbClass.sqlout ((long *) &CronBest.cron_best.pr_kz, 1, 0);
         DbClass.sqlout ((long *) &CronBest.cron_best.me, 3, 0);

         cursor_mhd = DbClass.sqlcursor ("select a_bas.a,a_bas.a_bz1,cron_best.mhd, cron_best.pr_kz, cron_best.me  from a_bas,cron_best where a_bas.wg = ? and cron_best.a = a_bas.a " 
						" and cron_best.mhd < today + ?  and cron_best.mhd <> 0 "
						" and a_bas.a not in (select a from aufptmp where auf = ?  ) order by cron_best.mhd, a_bas.a ");
//         cursor_mhd = DbClass.sqlcursor ("select a,a_bz1 from a_bas where wg = ? and a <> ?   order by a_bas.a ");
//         cursor_mhd = DbClass.sqlcursor ("select a,a_bz1 from a_bas where   a = ? or a < 20   order by a_bas.a ");
	}
	else
	{
         DbClass.sqlin ((double *) &a_bas.a_aufruf, 3, 0);
         DbClass.sqlout ((double *) &a_bas.a, 3, 0);
         DbClass.sqlout ((char *) a_bas.a_bz1, 0, 25);
         DbClass.sqlout ((long *) &CronBest.cron_best.mhd, 2, 0);
         DbClass.sqlout ((long *) &CronBest.cron_best.pr_kz, 1, 0);
         DbClass.sqlout ((long *) &CronBest.cron_best.me, 3, 0);

         cursor_mhd = DbClass.sqlcursor ("select a_bas.a,a_bas.a_bz1,cron_best.mhd, cron_best.pr_kz, cron_best.me  from a_bas,cron_best "
						" where a_bas.a = ? and cron_best.a = a_bas.a " 
						" order by a_bas.a, cron_best.mhd ");
	}
         if (sqlstatus)
         {
                     return (-1);
         }
    return (cursor_mhd);
}





int KRITMHD_CLASS::ShowKritMHD (HWND hWnd, int ws_flag, double a, short wg, int kmhd, BOOL ZeigeKritMHD, BOOL ZeigeBestand) 
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		char buffer [512];
	    char *etc;
		FILE *fp;
		int x, y, cx, cy;
		int rect_x, rect_y, rect_cx, rect_cy;
		int anz, save_mhd;
		int cursor_mhd = -1;
		static BOOL savZeigeKritMHD = FALSE;
		static BOOL savZeigeBestand = FALSE;
		BOOL WindowAktiv = FALSE;
		BOOL Zeige = FALSE;
		char ca [14];
		ModusBestand = ZeigeBestand;
		ModusKritMHD = ZeigeKritMHD;
		if (kmhd == 0 && ModusBestand == FALSE) return 0;
        if (ZeigeKritMHD == TRUE) ModusBestand = FALSE;  
        if (ModusBestand == TRUE) ZeigeKritMHD = FALSE;  
		if (ZeigeKritMHD == TRUE || ZeigeBestand) Zeige = TRUE; 
		KritischeMHD = kmhd;
		BOOL BKritMHD = FALSE;
		short dx = 0;
		SetMehrfachauswahl (TRUE);  //LAC-166  bei OK bleibt Fenster offen
        
		a_bas.wg = wg;
        if (a_bas.a_aufruf == a && eWindow != NULL && Zeige == TRUE)  
		{
			if (savZeigeBestand == ZeigeBestand && savZeigeKritMHD == ZeigeKritMHD)
			{
				return 0;  //gleicher Artikel, hier ist nirchts zu tun ! 
			}
		}
		savZeigeBestand = ZeigeBestand;
		savZeigeKritMHD = ZeigeKritMHD;

		a_bas.a_aufruf = a;
		if (eWindow  !=  NULL)
		{
			WindowAktiv = TRUE;
		    current_form = savecurrent;
			SetMouseLock (FALSE);
//			CloseUbControls (); 
//			CloseEWindow (eWindow);
			SetListFont (FALSE);
			SetDblClck (NULL, 1);
			close_sql (cursor_mhd);
			cursor_mhd = -1;
		}



//        SortMalloc ();
		cursor_mhd= prep_cursor ();
	    savecurrent = current_form;
		char text [100];
		char wg_bz1 [25];
		char cmhd [20];
		if (ModusBestand == FALSE)
		{
			DbClass.sqlin ((short *) &a_bas.wg, 1, 0);
			DbClass.sqlout ((char *) &wg_bz1, 0, 24);
			DbClass.sqlcomm ("select wg_bz1 from wg where wg = ? ");
		}


		if (ModusBestand == FALSE) sprintf (text, "Kritsche MHD in der Warengruppe %s", clipped(a_bas.a_bz1));
		if (ModusBestand == TRUE) sprintf (text, "Artikelbestand");
		SetCaption (text);
		if (sort_awanz > 0 && Zeige == TRUE) 
		{
			//Inhalt des Fensers initialisieren
			int i;
			for (i = sort_awanz; i >= 0; i --)
			{
				strcpy (sort_awtab[i].sort1, "");
				strcpy (sort_awtab[i].sort2, "");
				strcpy (sort_awtab[i].sort3, "");
				strcpy (sort_awtab[i].sort4, "");
				strcpy (sort_awtab[i].sort5, "");
			}

			ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
		}

 		sort_awanz = 0;
		BKritMHD = FALSE;  //LAC-8
		int dsqlstatus = DbClass.sqlfetch (cursor_mhd); 
		while (dsqlstatus == 0)
        {
			if (a_bas.a == a_bas.a_aufruf && ZeigeKritMHD)
			{
				BKritMHD = TRUE;  //LAC-8
				dsqlstatus = DbClass.sqlfetch (cursor_mhd);
			}
			else
			{

			         sprintf (ca, "%.0lf", a_bas.a);
			         sprintf (sort_awtab[sort_awanz].sort1, "%s", ca);
			         sprintf (sort_awtab[sort_awanz].sort2, "%s", a_bas.a_bz1);
					 dlong_to_asc (CronBest.cron_best.mhd, cmhd);
					 save_mhd = CronBest.cron_best.mhd;
			         sprintf (sort_awtab[sort_awanz].sort3, "%s", cmhd);
					 if (CronBest.cron_best.pr_kz == 1)
					 {
						sprintf (sort_awtab[sort_awanz].sort4, "%.2lf", CronBest.cron_best.me);
						sprintf (sort_awtab[sort_awanz].sort5, "%s", "");
					 }
					 else
					 {
						sprintf (sort_awtab[sort_awanz].sort4, "%s", "");
						sprintf (sort_awtab[sort_awanz].sort5, "%.2lf", CronBest.cron_best.me);
					 }
					 dsqlstatus = DbClass.sqlfetch (cursor_mhd);
					 if (dsqlstatus == 0)
					 {
					 	 if (a_bas.a == ratod (ca) && save_mhd == CronBest.cron_best.mhd)
						 {
							 if (CronBest.cron_best.pr_kz == 1)
							 {
								sprintf (sort_awtab[sort_awanz].sort4, "%.2lf", CronBest.cron_best.me);
							 }
							 else
							 {
								sprintf (sort_awtab[sort_awanz].sort5, "%.2lf", CronBest.cron_best.me);
							 }
						 } else dx = 1;
					 }


					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 sort_awanz ++;
					if (sort_awanz == MaxSort) 
					{
						      print_mess (2, "Es konnten nicht alle Artikel eingelesen werden");       
						      break;
					}
					 if (sort_awanz == MAXSORT) break;
					 if (dx == 0) 
					 {
						 dsqlstatus = DbClass.sqlfetch (cursor_mhd);
					 }
					 dx = 0;
			}
        }

		if (Zeige == FALSE)
		{
			CloseWindow (hWnd); 
			a_bas.a_aufruf = 0.0;
			Setlistenter (1);
	        SetDblClck (IsAwClck, 1);
			 if (BKritMHD == TRUE) return 1; 
			 if (BKritMHD == FALSE) return -1; 
			 return 0;
		}
		save_fkt (5); 
//        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
//		SetMouseLock (TRUE);
		etc = getenv ("BWSETC");
		if (etc == NULL) return 0;
		sprintf (buffer, "%s\\%s.rct", etc,"51100Lackkritmhd");
		fp = fopen (buffer, "r");
		if (fp == NULL) 
		{
			x = 130;
			y = 0;
			cx = 60;
			cy = 10;
			rect_x = 0;
			rect_y = 0;
			rect_cx = 0;
			rect_cy = 0;
		}
		else
		{
			while (fgets (buffer, 511, fp))
			{
				cr_weg (buffer);
				anz = wsplit (buffer, " ");
				if (anz < 2) continue;
				if (strcmp (wort[0], "left") == 0)
				{
					rect_x = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "top") == 0)
				{
					rect_y = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "right") == 0)
				{
					rect_cx = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "bottom") == 0)
				{
					rect_cy = atoi (wort[1]);
				}
			}
			fclose (fp);
		}
		if (WindowAktiv == FALSE)
		{
			eWindow = OpenListWindowEnF (cy, cx, y, x, 0);
			if (rect_x != 0 || rect_y != 0 || rect_cx != 0 || rect_cy != 0)
			{
				MoveWindow (eWindow, rect_x, rect_y, rect_cx, rect_cy, TRUE);
			}

	        ElistVl (&fsort_awl);
		    ElistUb (&fsort_awub);
		}
		else
		{
			SetWindowText (eWindow, text);
		}
		Setlistenter (1);
/***
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
		CloseWindow (hWnd);

			eWindow = OpenListWindowEnF (cy, cx, y, x, 0);
			if (rect_x != 0 || rect_y != 0 || rect_cx != 0 || rect_cy != 0)
			{
				MoveWindow (eWindow, rect_x, rect_y, rect_cx, rect_cy, TRUE);
			}

	        ElistVl (&fsort_awl);
		    ElistUb (&fsort_awub);

		Setlistenter (1);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
**/





        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);

/** 
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 0;
         }
	     //tou.tou = atoi(sort_awtab[sort_idx].sort1); 
		 close_sql(cursor);
		 fetch_sql(cursor);
**/
		 if (BKritMHD == TRUE) return 1; 
		 if (BKritMHD == FALSE) return -1; 
         return 0;
}

