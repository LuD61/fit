int SetUeandDrk (void)
/**
Uebergabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;
		  int plus = 0;
		  if (KompAuf::WithAufb) plus = 1;

          if (SendMessage (cbox.mask[1 + plus].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        aufUeandDrk = 0;
          }
          else
          {
                        aufUeandDrk  = 1;
          }
          if (syskey == KEYCR)
          {
                       aufUeandDrk  = 1;
                       SendMessage (cbox.mask[1 + plus].feldid, BM_SETCHECK,
                       aufUeandDrk, 0l);
                       break_enter ();
          }
		  BOOL stat = aufUeandDrk;
          UnsetBox (1 + plus);
		  aufUeandDrk = stat;
          aufBest = 0;
          return 1;
}

int SetLsUeb (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;
		  int plus = 0;
		  if (KompAuf::WithAufb) plus = 1;

          if (SendMessage (cbox.mask[2 + plus].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       aufLsUeb = 0;
          }
          else
          {
                       aufLsUeb = 1;
          }
          if (syskey == KEYCR)
          {
                       aufLsUeb = 1;
                       SendMessage (cbox.mask[2 + plus].feldid, BM_SETCHECK,
                       aufLsUeb, 0l);
                       break_enter ();
          }
		  BOOL stat = aufLsUeb;
          UnsetBox (2 + plus);
		  aufLsUeb = stat;
          aufBest = 0;
          return 1;
}

int SetLsKomp (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;
		  int plus = 0;
		  if (KompAuf::WithAufb) plus = 1;

          if (SendMessage (cbox.mask[3 + plus].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       aufLsKomp = 0;
          }
          else
          {
                       aufLsKomp = 1;
          }
          if (syskey == KEYCR)
          {
                       aufLsKomp = 1;
                       SendMessage (cbox.mask[3 + plus].feldid, BM_SETCHECK,
                       aufLsUeb, 0l);
                       break_enter ();
          }
		  BOOL stat = aufLsKomp;
          UnsetBox (3 + plus);
		  aufLsKomp = stat;
          aufBest = 0;
          return 1;
}
