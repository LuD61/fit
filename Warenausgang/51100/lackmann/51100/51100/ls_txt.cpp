#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "ls_txt.h"

struct LS_TXT ls_txt, ls_txt_null;

void LS_TXT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &ls_txt.nr, 2, 0);
    out_quest ((char *) &ls_txt.nr,2,0);
    out_quest ((char *) &ls_txt.zei,2,0);
    out_quest ((char *) ls_txt.txt,0,61);
            cursor = prepare_sql ("select ls_txt.nr,  "
"ls_txt.zei,  ls_txt.txt from ls_txt "

#line 26 "ls_txt.rpp"
                                  "where nr = ? "
                                  "order by zei");

    ins_quest ((char *) &ls_txt.nr,2,0);
    ins_quest ((char *) &ls_txt.zei,2,0);
    ins_quest ((char *) ls_txt.txt,0,61);
            sqltext = "update ls_txt set ls_txt.nr = ?,  "
"ls_txt.zei = ?,  ls_txt.txt = ? "

#line 30 "ls_txt.rpp"
                                  "where nr = ? "
                                  "and zei  = ?";

            ins_quest ((char *) &ls_txt.nr, 2, 0);
            ins_quest ((char *) &ls_txt.zei, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &ls_txt.nr, 2, 0);
            ins_quest ((char *) &ls_txt.zei, 2, 0);
            test_upd_cursor = prepare_sql ("select nr from ls_txt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &ls_txt.nr, 2, 0);
            ins_quest ((char *) &ls_txt.zei, 2, 0);
            del_cursor = prepare_sql ("delete from ls_txt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &ls_txt.nr, 2, 0);
            del_cursor_posi = prepare_sql ("delete from ls_txt "
                                  "where  nr = ?");

    ins_quest ((char *) &ls_txt.nr,2,0);
    ins_quest ((char *) &ls_txt.zei,2,0);
    ins_quest ((char *) ls_txt.txt,0,61);
            ins_cursor = prepare_sql ("insert into ls_txt ("
"nr,  zei,  txt) "

#line 54 "ls_txt.rpp"
                                      "values "
                                      "(?,?,?)"); 

#line 56 "ls_txt.rpp"
}
int LS_TXT_CLASS::dbreadfirst (void)
/**

Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int LS_TXT_CLASS::delete_aufpposi (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (del_cursor_posi == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_posi);
         return (sqlstatus);
}

