// AdrDialog.h: Schnittstelle f�r die Klasse CAdrDialog.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ADRDIALOG_H__136685C9_5B09_4EEF_B695_D46ADAE488FE__INCLUDED_)
#define AFX_ADRDIALOG_H__136685C9_5B09_4EEF_B695_D46ADAE488FE__INCLUDED_
#include "Dialog.h"
#include "resrc1.h"
#include "kun.h"
#include "mdn.h"
#include "adr.h"
#include "Text.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define CString Text

using namespace std;

class CAdrDialog : 
	  public CDialog  
{
private:
	COLORREF m_DlgColor;
	HBRUSH   m_DlgBrush;
	COLORREF m_EditColor;
	COLORREF m_ColorFreiTxt;
	COLORREF m_ColorPrStu;
	COLORREF m_ColorZahlArt;
	HBRUSH   m_EditBrush;
	long     m_AdrNr;
	CAdr     m_Adr;
	CString  m_FreiTxt2;
	CString  m_PrStu;
	CString  m_ZahlArt;
//	CString  m_Pfand_ber; geht seltsamerweise nicht , Layout der F�lder �ndert sich !!!
	char  m_Pfand_ber[30];


	CFont m_FatFont;

	vector<CDialogItem *> m_EditItems;

	BOOL IsEditItem (HWND hWnd);
public:
	enum {Id = IDD_ADR_DIALOG};
	void SetAdrNr (long AdrNr)
	{
		m_AdrNr = AdrNr;
	}

	void set_FreiTxt2 (LPSTR frei_txt2)
	{
		m_FreiTxt2 = frei_txt2;
	}

	LPSTR FreiTxt2 ()
	{
		return (LPSTR) m_FreiTxt2;
	}

	void set_PrStu (LPSTR pr_stu)
	{
		m_PrStu = pr_stu;
	}

	LPSTR PrStu ()
	{
		return (LPSTR) m_PrStu;
	}

	void set_ZahlArt (LPSTR zahl_art)
	{
		m_ZahlArt = zahl_art;
	}

	void set_Pfand_ber (short pfand_ber)
	{
		if (pfand_ber == 0)
		{
			sprintf (m_Pfand_ber, "Keine Pfandberechnung");
		}
		else
		{
			sprintf (m_Pfand_ber, "== Pfandberechnung == ");
		}
	}

	LPSTR ZahlArt ()
	{
		return (LPSTR) m_ZahlArt;
	}

	CAdrDialog(HWND Parent=NULL);
	virtual ~CAdrDialog();
protected:
    void    ReadAdr ();
	virtual void AttachControls ();
	virtual BOOL OnF5 ();
	virtual BOOL OnInitDialog ();
	virtual BOOL PreTranslateMessage (MSG *msg);
	virtual HBRUSH OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor);
};

#endif // !defined(AFX_ADRDIALOG_H__136685C9_5B09_4EEF_B695_D46ADAE488FE__INCLUDED_)
