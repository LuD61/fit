#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "wo_tou.h"
#include "tou.h"

struct WO_TOU wo_tou, wo_tou_null;

/* Variablen fuer Auswahl mit Buttonueberschrift    */

#include "itemc.h"


void WO_TOU_CLASS::prepare (void)
{
         char *sqltext;

         ins_quest ((char *) &wo_tou.mdn,   1, 0);
         ins_quest ((char *) &wo_tou.fil,   1, 0);  
         ins_quest ((char *) &wo_tou.kun_fil,  1, 0);  

    out_quest ((char *) &wo_tou.mdn,1,0);
    out_quest ((char *) &wo_tou.fil,1,0);
    out_quest ((char *) &wo_tou.kun,2,0);
    out_quest ((char *) &wo_tou.ta_nr,1,0);
    out_quest ((char *) wo_tou.kun_krz1,0,17);
    out_quest ((char *) &wo_tou.kun_fil,1,0);
    out_quest ((char *) &wo_tou.posi,2,0);
    out_quest ((char *) &wo_tou.mo_tour,2,0);
    out_quest ((char *) &wo_tou.di_tour,2,0);
    out_quest ((char *) &wo_tou.mi_tour,2,0);
    out_quest ((char *) &wo_tou.do_tour,2,0);
    out_quest ((char *) &wo_tou.fr_tour,2,0);
    out_quest ((char *) &wo_tou.sa_tour,2,0);
    out_quest ((char *) &wo_tou.so_tour,2,0);
         cursor = prepare_sql ("select wo_tou.mdn,  wo_tou.fil,  "
"wo_tou.kun,  wo_tou.ta_nr,  wo_tou.kun_krz1,  wo_tou.kun_fil,  "
"wo_tou.posi,  wo_tou.mo_tour,  wo_tou.di_tour,  wo_tou.mi_tour,  "
"wo_tou.do_tour,  wo_tou.fr_tour,  wo_tou.sa_tour,  wo_tou.so_tour from wo_tou "

#line 31 "wo_tou.rpp"
                               "where mdn = ? "
                               "and   fil = ?" 
                               "and   kun_fil = ? "
                               "order by posi");

         ins_quest ((char *) &wo_tou.mdn,   1, 0);
         ins_quest ((char *) &wo_tou.fil,   1, 0);  
         ins_quest ((char *) &wo_tou.kun_fil,  1, 0);  
         ins_quest ((char *) &wo_tou.kun, 2, 0);

    out_quest ((char *) &wo_tou.mdn,1,0);
    out_quest ((char *) &wo_tou.fil,1,0);
    out_quest ((char *) &wo_tou.kun,2,0);
    out_quest ((char *) &wo_tou.ta_nr,1,0);
    out_quest ((char *) wo_tou.kun_krz1,0,17);
    out_quest ((char *) &wo_tou.kun_fil,1,0);
    out_quest ((char *) &wo_tou.posi,2,0);
    out_quest ((char *) &wo_tou.mo_tour,2,0);
    out_quest ((char *) &wo_tou.di_tour,2,0);
    out_quest ((char *) &wo_tou.mi_tour,2,0);
    out_quest ((char *) &wo_tou.do_tour,2,0);
    out_quest ((char *) &wo_tou.fr_tour,2,0);
    out_quest ((char *) &wo_tou.sa_tour,2,0);
    out_quest ((char *) &wo_tou.so_tour,2,0);
         cursor_kun = prepare_sql ("select wo_tou.mdn,  "
"wo_tou.fil,  wo_tou.kun,  wo_tou.ta_nr,  wo_tou.kun_krz1,  "
"wo_tou.kun_fil,  wo_tou.posi,  wo_tou.mo_tour,  wo_tou.di_tour,  "
"wo_tou.mi_tour,  wo_tou.do_tour,  wo_tou.fr_tour,  wo_tou.sa_tour,  "
"wo_tou.so_tour from wo_tou "

#line 42 "wo_tou.rpp"
                               "where mdn = ? "
                               "and   fil = ?" 
                               "and   kun_fil = ? "
                               "and   kun = ?");

    ins_quest ((char *) &wo_tou.mdn,1,0);
    ins_quest ((char *) &wo_tou.fil,1,0);
    ins_quest ((char *) &wo_tou.kun,2,0);
    ins_quest ((char *) &wo_tou.ta_nr,1,0);
    ins_quest ((char *) wo_tou.kun_krz1,0,17);
    ins_quest ((char *) &wo_tou.kun_fil,1,0);
    ins_quest ((char *) &wo_tou.posi,2,0);
    ins_quest ((char *) &wo_tou.mo_tour,2,0);
    ins_quest ((char *) &wo_tou.di_tour,2,0);
    ins_quest ((char *) &wo_tou.mi_tour,2,0);
    ins_quest ((char *) &wo_tou.do_tour,2,0);
    ins_quest ((char *) &wo_tou.fr_tour,2,0);
    ins_quest ((char *) &wo_tou.sa_tour,2,0);
    ins_quest ((char *) &wo_tou.so_tour,2,0);
         sqltext = "update wo_tou set wo_tou.mdn = ?,  "
"wo_tou.fil = ?,  wo_tou.kun = ?,  wo_tou.ta_nr = ?,  "
"wo_tou.kun_krz1 = ?,  wo_tou.kun_fil = ?,  wo_tou.posi = ?,  "
"wo_tou.mo_tour = ?,  wo_tou.di_tour = ?,  wo_tou.mi_tour = ?,  "
"wo_tou.do_tour = ?,  wo_tou.fr_tour = ?,  wo_tou.sa_tour = ?,  "
"wo_tou.so_tour = ? "

#line 48 "wo_tou.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   ta_nr = ? "
                               "and kun_fil = ?";
  
         ins_quest ((char *) &wo_tou.mdn, 1, 0);
         ins_quest ((char *) &wo_tou.fil, 1, 0);  
         ins_quest ((char *) &wo_tou.kun, 2, 0);
         ins_quest ((char *) &wo_tou.ta_nr, 1, 0);
         ins_quest ((char *) &wo_tou.kun_fil,  1, 0);  

         upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &wo_tou.mdn,1,0);
    ins_quest ((char *) &wo_tou.fil,1,0);
    ins_quest ((char *) &wo_tou.kun,2,0);
    ins_quest ((char *) &wo_tou.ta_nr,1,0);
    ins_quest ((char *) wo_tou.kun_krz1,0,17);
    ins_quest ((char *) &wo_tou.kun_fil,1,0);
    ins_quest ((char *) &wo_tou.posi,2,0);
    ins_quest ((char *) &wo_tou.mo_tour,2,0);
    ins_quest ((char *) &wo_tou.di_tour,2,0);
    ins_quest ((char *) &wo_tou.mi_tour,2,0);
    ins_quest ((char *) &wo_tou.do_tour,2,0);
    ins_quest ((char *) &wo_tou.fr_tour,2,0);
    ins_quest ((char *) &wo_tou.sa_tour,2,0);
    ins_quest ((char *) &wo_tou.so_tour,2,0);
         ins_cursor = prepare_sql ("insert into wo_tou (mdn,  "
"fil,  kun,  ta_nr,  kun_krz1,  kun_fil,  posi,  mo_tour,  di_tour,  mi_tour,  do_tour,  "
"fr_tour,  sa_tour,  so_tour) "

#line 63 "wo_tou.rpp"
                                   "values "
                                   "(?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?)");

#line 65 "wo_tou.rpp"

         ins_quest ((char *) &wo_tou.mdn, 1, 0);
         ins_quest ((char *) &wo_tou.fil, 1, 0);  
         ins_quest ((char *) &wo_tou.kun, 2, 0);
         ins_quest ((char *) &wo_tou.ta_nr, 1, 0);
         ins_quest ((char *) &wo_tou.kun_fil,  1, 0);  

         del_cursor = prepare_sql ("delete from wo_tou "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   ta_nr = ? "
                               "and   kun_fil = ?");

         ins_quest ((char *) &wo_tou.mdn, 1, 0);
         ins_quest ((char *) &wo_tou.fil, 1, 0);  
         ins_quest ((char *) &wo_tou.kun, 2, 0);
         ins_quest ((char *) &wo_tou.ta_nr, 1, 0);
         ins_quest ((char *) &wo_tou.kun_fil,  1, 0);  

         test_upd_cursor = prepare_sql ("select kun from wo_tou "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   ta_nr = ? "
                               "and   kun_fil = ?");
}

int WO_TOU_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int WO_TOU_CLASS::dbreadkunfirst (void)
/**
Ersten Satz mit Kunden-Nummer aus Tabelle lesen.
**/
{
         if (cursor_kun == -1)
         {
                this->prepare ();
         }
         open_sql (cursor_kun);
         fetch_sql (cursor_kun);
         return sqlstatus;
}

int WO_TOU_CLASS::dbreadkun (void)
/**
Naechsten Satz mit Kunden-Nummer aus Tabelle lesen.
**/
{
         fetch_sql (cursor_kun);
         return sqlstatus;
}

