#ifndef _HWG_BEST_WK_DEF
#define _HWG_BEST_WK_DEF

#include "dbclass.h"

struct HWG_BEST_WK {
   short     hwg;
   double    prozent;
   double    qm;
   short     profil;
   long      gueltig_von;
   long      gueltig_bis;
   double    preis;
   double    liqui;
   double    ges_sortiment;
};
extern struct HWG_BEST_WK hwg_best_wk, hwg_best_wk_null;

#line 7 "hwg_best_wk.rh"

class HWG_BEST_WK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
               int cursor_all;
               
       public :
               HWG_BEST_WK_CLASS () : DB_CLASS (),
                                     cursor_all (-1)

               {
               }
               int dbreadfirst (void);
               int dbreadfirst_all (void);
               int dbread_all (void);
};
#endif
