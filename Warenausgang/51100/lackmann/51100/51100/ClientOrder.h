// ClientOrder.h: Schnittstelle f�r die Klasse CClientOrder.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIENTORDER_H__211724AB_0CF5_4C96_B9F7_78B8C0CEEE2F__INCLUDED_)
#define AFX_CLIENTORDER_H__211724AB_0CF5_4C96_B9F7_78B8C0CEEE2F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "stnd_auf.h"
#include "ListHandler.h"
#include "DataCollection.h"
#include "Aufps.h"
#include "a_hndw.h"
#include "mdn.h"
#include "fil.h"
#include "kun.h"
#include "aktion.h"
#include "akt_krz.h"
#include "a_pr.h"
#include "mo_a_pr.h"
#include "mo_preis.h"
#include "ptab.h"
#include "mo_einh.h"
#include "kun.h"
#include "aufkun.h"
#include "kumebest.h"
#include "ls.h"
#include "DllPreise.h"
#include <string>
#include "GrpPrice.h"
#include "ComObject.h"
#include "cron_best.h"
#include "best_vorschlag.h"

using namespace std;

class CClientOrder  
{
public:
	enum STD_SORT
	{
		PosA = 0,
        AgA = 1,
		WgA = 2,
		AgBz = 3,
		WgBz = 4,
	};

	enum STD_RDOPTIMIZE
	{
		No = 0,
        FromFile = 1,
	};

	enum PR_KZ
	{
		BsdNoPrice = 0,
		BsdPrice = 1,
	};
	static CClientOrder *GetInstance (); //LAC-9
	static CClientOrder *instance; //LAC-9


protected:
	short mdn;
	short fil;
	short kun_fil;
	long kun_nr;
	long dletzteBestellung;
	long dbestVorschlag;
	BOOL MustReadLetztBest;
	long dlieferdat ;
	long akt_auf;
	long akt_hwg; //LAC-9
	long akt_wg; //LAC-9
	BOOL Bestellvorschlag; //LAC-9
	BOOL letzteBestellung; //LAC-9
	BOOL KritischeMHD; //LAC-84
	BOOL Kommldat; //LAC-116
	BOOL HWG1; //LAC-9
	BOOL HWG2; //LAC-9
	BOOL HWG3; //LAC-9
	BOOL HWG4; //LAC-9
	BOOL HWG5; //LAC-9
	BOOL HWG6; //LAC-9
	BOOL HWG7; //LAC-9
	BOOL HWG8; //LAC-9
	BOOL HWG9; //LAC-9
	BOOL HWG10; //LAC-9
	BOOL HWG11; 
	BOOL HWG12; 
	BOOL HWG13; 
	BOOL HWG14; 
	BOOL HWG15; 
	BOOL HWG16; 
	long last_kun;
    string lieferdat;
    BOOL LastFromAufKun;
	double inh;
	Text KunBran;

	int cursor;
	int cursor_lbestk; //LAC-9
	int cursor_lbestp; //LAC-9
	int cursor_kritmhdk; 
	int cursor_bestellvorschlag; 
	int cursor_bestellvorschlagges; 
	int cursor_kritmhdp;  
	int cursor_kommldatk; 
	int cursor_kommldatp; 
	int cursor_suche_kw; //LAC-131 
	int cursor_lieferung; //LAC-54
	int cursor_hwg; //LAC-9
	int cursor_wg; //LAC-9
	short ag;
	short wg;
	char a_bz1 [25];
	char kun_bran2 [4];
	int dauf; //LAC-9
	int dls ;
	int dkw ; //LAC-131
	int djr ; //LAC-131
	double ddiff;
		char auf_me_bz [7]; //LAC-9
		double vk_pr, auf_me, lief_me, lieferung ; //LAC-9
		long lieferdatum;  //LAC-9


	STD_SORT SortMode;
	STD_RDOPTIMIZE RdOptimize;
	BOOL RefreshRdOptimize;
	SAUFP_CLASS StndAufp;
	DB_CLASS DbClass;
	EINH_CLASS Einh;
	AUFKUN_CLASS AufKun;
	CRON_BEST_CLASS CronBest;
	BEST_VORSCHLAG_CLASS Best_vorschlag;
	LS_CLASS ls_class;
	PTAB_CLASS Ptab;
	HNDW_CLASS Hndw;
	CGrpPrice GrpPrice;
	CListHandler *ListHandler;
	CDataCollection<AUFPS *> *StndAll;
	CDataCollection<AUFPS *> *lBestAll; //LAC-9
	CDataCollection<AUFPS *> *KritMhdAll; //LAC-84
	CDataCollection<AUFPS *> *KommldatAll; //LAC-116
	CDataCollection<AUFPS *> *HWG1All; //LAC-9
	CDataCollection<AUFPS *> *HWG2All; //LAC-9
	CDataCollection<AUFPS *> *HWG3All; //LAC-9
	CDataCollection<AUFPS *> *HWG4All; //LAC-9
	CDataCollection<AUFPS *> *HWG5All; //LAC-9
	CDataCollection<AUFPS *> *HWG6All; //LAC-9
	CDataCollection<AUFPS *> *HWG7All; //LAC-9
	CDataCollection<AUFPS *> *HWG8All; //LAC-9
	CDataCollection<AUFPS *> *HWG9All; //LAC-9
	CDataCollection<AUFPS *> *HWG10All; //LAC-9
	CDataCollection<AUFPS *> *HWG11All; 
	CDataCollection<AUFPS *> *HWG12All; 
	CDataCollection<AUFPS *> *HWG13All; 
	CDataCollection<AUFPS *> *HWG14All; 
	CDataCollection<AUFPS *> *HWG15All; 
	CDataCollection<AUFPS *> *HWG16All; 
	WA_PREISE WaPreis;
	CDllPreise DllPreise;
	ComObject Com;
public:
	enum A_TYP
	{
		enumHndw = 1,
        enumEig = 2,
		enumEigDiv = 3,
		enumLeih = 12,
		enumDienst = 13,
	};


	void SetLastFromAufKun (BOOL LastFromAufKun)
	{
		this->LastFromAufKun = LastFromAufKun;

	}

	BOOL GetLastFromAufKun ()
	{
		return LastFromAufKun;
	}

	void SetletzteBestellung (BOOL b) //LAC-9
	{
		letzteBestellung = b;
	}
	void SetKritischeMHD (BOOL b) //LAC-84
	{
		KritischeMHD = b;
	}
	void SetKommldat (BOOL b) //LAC-116
	{
		Kommldat = b;
	}
	void SetLieferdat (LPSTR lieferdat)
	{
		this->lieferdat = lieferdat;
	}
	void SetSortMode (STD_SORT SortMode)
	{
		if (cursor != -1)
		{
			 StndAufp.sqlclose (cursor);
			 cursor = -1;
		}
		this->SortMode = SortMode;
	}

	STD_SORT GetSortMode ()
	{
		return SortMode;
	}

	void SetRdOptimize (STD_RDOPTIMIZE RdOptimize)
	{
		this->RdOptimize = RdOptimize;
	}

	STD_RDOPTIMIZE GetRdOptimize ()
	{
		return RdOptimize;
	}

	void SetRefreshRdOptimize (BOOL RefreshRdOptimize)
	{
		this->RefreshRdOptimize = RefreshRdOptimize;
	}


	CClientOrder();
	virtual ~CClientOrder();
	void SetKeys (short mdn, short fil, short kun_fil, long kun, BOOL MustReadLetztBest, long dlieferdat, long akt_auf, long dletztBest, long dBestVoreschlag);
	void Load ();
	void Load_Bestellvorschlag ();
	void Prepare ();
	void Prepare_Bestellvorschlag ();
	int Read ();
	int Read_Bestellvorschlag ();
	void Load_letztBest (int dauf); //LAC-9 //LAC-132
	void Load_KritMhd (); //LAC-84
	void Load_Kommldat (); //LAC-116
	void initReiterv (); //LAC-9
	void Load_HWG (int dhwg, int dwg, int idx); //LAC-9
	void Prepare_letztBest (); //LAC-9
	void Prepare_KritMhd (); //LAC-84
	void Prepare_Kommldat (); //LAC-116
	void Prepare_KommldatKW (); //LAC-131
	void Prepare_HWG (); //LAC-9
	int Read_letztBest (int dauf); //LAC-9
	int Read_KritMhd (); //LAC-84
	int Read_Kommldat (); //LAC-116
	int Read_KommldatKW (); //LAC-131
	int Read_HWG (int dhwg, int dwg, int idx); //LAC-9
	void FillRow (AUFPS *Aufps);
	void FillKondArt (AUFPS *Aufps);
	void GetLastMe (AUFPS *Aufps);
	void GetLastMeAuf (AUFPS *Aufps);
	void ReadMeEinh (AUFPS *Aufps);
	void CalcLdPrPrc (AUFPS *aufps, double vk_pr, double ld_pr);
	void WriteAufps ();
	BOOL ReadAufps ();
};

#endif // !defined(AFX_CLIENTORDER_H__211724AB_0CF5_4C96_B9F7_78B8C0CEEE2F__INCLUDED_)
