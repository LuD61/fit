#ifndef _MO_AUFL_DEF
#define _MO_AUFL_DEF
#include "wmaskc.h"
#include "listcl.h"
#include "mo_intp.h"
#include "DllPreise.h"
#include "bsd.h"
#include "RowEvent.h"
#include "ControlPlugins.h"
#include "GrpPrice.h"
#include "ClientOrderThread.h"
#include "ThreadAction.h"
#include "MessageHandler.h"
#include "Ampel.h"
#include "cron_best.h"
#include "calcualtesmt.h"
#include "smtreader.h"
#include "waithandler.h"

#define MAXLEN 40

#define WM_STND_COMPLETE WM_USER+20
#define WM_STND_COM_FAILED WM_USER+21
#define WM_STND_RELOAD WM_USER+22

extern BOOL IsPartyService;
extern  int mench ; //LAC-112
class CColBkName
{
private:
	char name[80];
	COLORREF Color;

public:
	LPSTR GetName ()
	{
		return name;
	}


	COLORREF GetColor ()
	{
		return Color;
	}

	CColBkName (LPSTR name, COLORREF Color)
	{
		memset (this->name, 0, sizeof (this->name));
		strncpy (this->name, name, sizeof (this->name) - 1);
		this->Color = Color;
	}
};

class AUFPLIST : public CRowEvent, CThreadAction, CSmtReader
{

            private :

// Interne Klassen
				class CStndHandler : public CMessageHandler
				{
				protected :
					AUFPLIST *Instance;
				public :
					void SetInstance (AUFPLIST *Instance)
					{
						this->Instance = Instance;
					}
				public:
					CStndHandler (DWORD msg) : CMessageHandler (msg)
					{
						Instance = NULL;
					}

					virtual BOOL Run (); 
				};
				CStndHandler *StndHandler;


				class CStndFailed : public CStndHandler
				{

				public:
					CStndFailed (DWORD msg) : CStndHandler (msg)
					{
						Instance = NULL;
					}
					virtual BOOL Run (); 
				};
				CStndFailed *StndFailed;


				class CStndReload : public CMessageHandler
				{
				protected :
					AUFPLIST *Instance;
				public :
					void SetInstance (AUFPLIST *Instance)
					{
						this->Instance = Instance;
					}
				public:
					CStndReload (DWORD msg) : CMessageHandler (msg)
					{
						Instance = NULL;
					}

					virtual BOOL Run (); 
				};
				CStndReload *StndReload;
				static CCalcualteSmt m_CalculateSmt;
                int mamainmax;
                int mamainmin;
                HWND    hwndTB;
                char *eingabesatz;
                unsigned char *ausgabesatz;
                int zlen;
                int feld_anz;
                int banz;
                PAINTSTRUCT aktpaint;
                char *SwSaetze[30000];
                int PageView;
                unsigned char DlgSatz [20 + MAXLEN + 2];
                FELDER *LstZiel;
                unsigned char *LstSatz;
                int Lstzlen;
                int Lstbanz;
                int SwRecs;
                int AktRow;
                int AktColumn;
                int scrollpos;
				static int splitls;
                TEXTMETRIC tm;
                char InfoCaption [80];
                int WithMenue;
                int WithToolBar;
                BOOL ListAktiv;
				static HWND hMainWindow;
				int Posanz;
				int liney;
				static short auf_art;
                HMENU hMenu;
				CDataCollection<CColBkName *> ColBkNames;
				static CWaitHandler *waitHandler;
				static int posRecs;
				static CRON_BEST_CLASS CronBest;
				static CClientOrderThread ClientOrder;
				static CClientOrder RClientOrder; //LAC-9
				static CControlPlugins pControls;
				static CGrpPrice GrpPrice;
				static int PosTxtKz;
                static BOOL Muster;
                static BOOL Storno;
                static BOOL FlgInEnterListe; //LAC-9
                static int dhwg; //LAC-9
                static int dwg; //LAC-9
                static int didx; //LAC-9
				static CBsd Bsd;
				static BOOL TxtByDefault;
				static BOOL PosTxtNrPar;
				static AUFPLIST *instance;
				static BOOL MustReadLetztBest;

             public :
				enum 
				{
					Standard = 1,
                    Voll = 2,
				};

				CAmpel Ampel;
				static CClientOrder::STD_RDOPTIMIZE StndRdOptimize;
				static BOOL RefreshStnd;
				static CDllPreise DllPreise;
                static void SetMax0 (int);
                static void SetMin0 (int);
                static BOOL AErsInLs;
				static int StndDirect;
				int LeisteWg; //LAC-9
				int LeisteHwg;//LAC-9
				static BOOL ZeigeBmp;
				static BOOL ZeigeOP; //LAC-94
				static BOOL ZeigePalette;
				static BOOL ZeigeKritMhd;
				static BOOL ZeigeBestand;
				static BOOL ZeigeWarenkorb;


			// LAC-9 A
				static BOOL AktAuf_aktiv;
				static BOOL Bestellvorschlag_aktiv;
				static BOOL letzteBestellung_aktiv;
				static BOOL KritischeMhd_aktiv; //LAC-84
				static BOOL kommldat_aktiv; //LAC-116
				static BOOL HWG1_aktiv;
				static BOOL HWG2_aktiv;
				static BOOL HWG3_aktiv;
				static BOOL HWG4_aktiv;
				static BOOL HWG5_aktiv;
				static BOOL HWG6_aktiv;
				static BOOL HWG7_aktiv;
				static BOOL HWG8_aktiv;
				static BOOL HWG9_aktiv;
				static BOOL HWG10_aktiv;
				static BOOL HWG11_aktiv;
				static BOOL HWG12_aktiv;
				static BOOL HWG13_aktiv;
				static BOOL HWG14_aktiv;
				static BOOL HWG15_aktiv;
				static BOOL HWG16_aktiv;
			// LAC-9 E


				BMAP BitMap;
				HBITMAP NoteIcon;
				HBITMAP SelBmp ; //LAC-9

				static BOOL RemoveBsd ;
				static BOOL Spalte_last_lieferung_Ok; //LA-54
				static BOOL Spalte_last_lief_me_Ok; //LA-54
				static BOOL Spalte_last_me_Ok;
				static BOOL Spalte_lieferdat_Ok;
				static BOOL Spalte_lpr_vk_Ok;
				static BOOL Spalte_soll_me_Ok;
				static BOOL Spalte_diff_me_Ok;
				static BOOL Spalte_bestellvorschlag_Ok; //LA-56
				static BOOL Spalte_min_best_Ok; //LA-56
				static BOOL Spalte_grp_min_best_Ok; //LA-104

				static BOOL Spalte_stk_karton_Ok ;  //LAC-118
				static BOOL Spalte_gew_karton_Ok ; 
				static BOOL Spalte_bsd_Ok ; 
				static BOOL Spalte_lad_pr_Ok ; 
				static BOOL Spalte_lad_pr_prc_Ok ; 
				static BOOL Spalte_lad_pr0_Ok ; 
				static BOOL Spalte_marge_Ok ; 
				static BOOL Spalte_mhd_Ok ; 


				static BOOL Spalte_last_lieferung_gewaehlt [6]; 
				static BOOL Spalte_last_lief_me_gewaehlt [6]; 
				static BOOL Spalte_last_me_gewaehlt [6];
				static BOOL Spalte_lieferdat_gewaehlt [6];
				static BOOL Spalte_lpr_vk_gewaehlt [6];
				static BOOL Spalte_soll_me_gewaehlt [6];
				static BOOL Spalte_diff_me_gewaehlt [6];
				static BOOL Spalte_bestellvorschlag_gewaehlt [6]; 
				static BOOL Spalte_grp_min_best_gewaehlt [6];  //LAC-104

				static BOOL Spalte_stk_karton_gewaehlt[6] ;  //LAC-118
				static BOOL Spalte_gew_karton_gewaehlt[6] ; 
				static BOOL Spalte_bsd_gewaehlt[6] ; 
				static BOOL Spalte_lad_pr_gewaehlt[6] ; 
				static BOOL Spalte_lad_pr_prc_gewaehlt[6] ; 
				static BOOL Spalte_lad_pr0_gewaehlt[6] ; 
				static BOOL Spalte_marge_gewaehlt[6] ; 
				static BOOL Spalte_mhd_gewaehlt[6] ; 


				static BOOL IgnoriereMinBest; //LA-59



                AUFPLIST ();
                ~AUFPLIST ();


				void SetWgLeiste (int hwg, int wg) //LAC-9
				{
					LeisteHwg = hwg;
					LeisteWg = wg;
				}

                BOOL GetMuster (void)
                {
                    return Muster;
                }

                void SetMuster (BOOL b)
                {
                    Muster = b;
                }
                BOOL GetStorno (void)
                {
                    return Storno;
                }

                void SetStorno (BOOL b)
                {
                    Storno = b;
                }
                static BOOL GetFlgInEnterListe (void) //LAC-9
                {
                    return FlgInEnterListe;
                }
                static void SetFlgInEnterListe (BOOL b) //LAC-9
                {
                    FlgInEnterListe  = b;
                }
                static BOOL Gethwg (void) //LAC-9
                {
                    return dhwg;
                }
                static BOOL Getwg (void) //LAC-9
                {
                    return dwg;
                }
                static BOOL GetReiterIdx (void) //LAC-9
                {
                    return didx;
                }
                static void Sethwg (int hwg) //LAC-9
                {
                    dhwg  = hwg;
                }
                static void Setwg (int wg) //LAC-9
                {
                    dwg  = wg;
                }
                static void SetReiterIdx (int idx) //LAC-9
                {
                    didx  = idx;
                }

				void SetAufArt (short auf_art)
				{
					this->auf_art = auf_art;
				}

				void SetLiney (int ly)
				{
					liney = ly;
				}

                void SetMenue (int with)
                {
                    WithMenue = with;
                }
                void SetZeigeBmp (BOOL b)
                {
					if (b == TRUE) ZeigeBmp = b;
					if (b == FALSE)
					{
						if (ZeigeBmp == FALSE) ZeigeBmp = TRUE;
						if (ZeigeBmp == TRUE) ZeigeBmp = FALSE;
					}
                }
                void SetZeigeOP (BOOL b)
                {
					if (b == TRUE) ZeigeOP = b;
					if (b == FALSE)
					{
						if (ZeigeOP == FALSE) ZeigeOP = TRUE;
						if (ZeigeOP == TRUE) ZeigeOP = FALSE;
					}
                }
                void SetZeigeKritMhd (BOOL b)
                {
					if (b == TRUE) ZeigeKritMhd = b;
					if (b == FALSE)
					{
						if (ZeigeKritMhd == FALSE) ZeigeKritMhd = TRUE;
						if (ZeigeKritMhd == TRUE) ZeigeKritMhd = FALSE;
					}
                }
                void SetZeigeBestand (BOOL b)
                {
					if (b == TRUE) ZeigeBestand = b;
					if (b == FALSE)
					{
						if (ZeigeBestand == FALSE) ZeigeBestand = TRUE;
						if (ZeigeBestand == TRUE) ZeigeBestand = FALSE;
					}
                }
                void SetZeigePalette (BOOL b)
                {
					if (b == TRUE) ZeigePalette = b;
					if (b == FALSE)
					{
						if (ZeigePalette == FALSE) ZeigePalette = TRUE;
						if (ZeigePalette == TRUE) ZeigePalette = FALSE;
					}
                }
                static void SetZeigeWarenkorb (BOOL b)
                {
					if (b == TRUE) ZeigeWarenkorb = b;
					if (b == FALSE)
					{
						if (ZeigeWarenkorb == FALSE) ZeigeWarenkorb = TRUE;
						if (ZeigeWarenkorb == TRUE) ZeigeWarenkorb = FALSE;
					}
                }

                static BOOL GetZeigeWarenkorb (void)
                {
                    return ZeigeWarenkorb;
                }

                void SetToolMenue (int with)
                {
                    WithToolBar = with;
                }

                void SetMax (void)
                {
                    mamainmax = 1;
                    SetMax0 (mamainmax);
                }

                void SetMin (void)
                {
                    mamainmin = 1;
                    SetMin0 (mamainmin);
                }

                void InitMax (void)
                {
                    mamainmax = 0;
                    SetMax0 (mamainmax);
                }

                void InitMin (void)
                {
                    mamainmin = 0;
                    SetMin0 (mamainmin);
                }

				void SetLager (long);

                BOOL IsListAktiv (void)
                {
                    return ListAktiv;
                }

                void SetCfgProgName (LPSTR);

				void SetMdnProd (int);

				int GetPosanz (void)
				{
					return Posanz;
				}
				BOOL GetLetzteBestellung_aktiv (void)
				{
					return letzteBestellung_aktiv;
				}

                void SetMenu (HMENU hMenu)
                {
                    this->hMenu = hMenu;
                }

                void SetPrPicture (void);
                void SetNewPicture (char *, char *);
                void SetNewLen (char *, int);
                void SetNewRow (char *, int);
                void SethMainWindow (HWND);
                HWND GetMamain1 (void);
                static int ShowBasis (void);
                static int TestAppend (void);
                static int DeleteLine (void);
                static void DoBreak (void);
                static BOOL SavePosis (void);
                static BOOL TestInsCount (void);
                static int InsertLine (void);
                static int AppendLine (void);
                static int GoToBestellvorschlag (void); //LAC-56
                static int IgnoreMinBest (void); //LAC-59
                static int AppendLine (double a); //LAC-51
                static void SchreibeAufptmp (int pos); //LAC-52
                static void SchreibeAufptmp (void); //LAC-52
                static int PosRab (void);
                static int SearchA (void);
                static void PaintSa (HDC hdc);
                static HWND CreateSaW (void);
                static void CreateSa (void);
				static void DestroySa (void);
                static HWND CreatePlus (void);
				static void DestroyPlus (void);
                void   PaintPlus (HDC);
                static void TestSaPr (void);
                static void FillDM (double, double);
                static void FillEURO (double, double);
                static void FillFremd (double, double);
				static void InitWaehrung (void);
                static void FillAktWaehrung (void);
                static void FillWaehrung (double, double);
				static void FillKondArt (char *);
				static double GetBasa_grund (double);
                static void ReadPr (void);
                static void ReadExtraPr (void);
                static void ReadMeEinh (void);

                static void rechne_aufme0 (void);
                static void rechne_aufme1 (void);
                static void rechne_aufme2 (void);
                static void rechne_aufme3 (void);
                static void rechne_liefme (void);

                static int testme (void);
                static int testme0 (void);
                static int testsg (void); //LAC-104
                static int TestPrproz_diff (void);
                static int testpr (void);
                static int AfterPrEnter (void);
                static int AfterBestellvorschl (void);
                static int SpalteAbmelden (char*, int dReiter);
                static int SpalteHinzufuegen (char*, int dReiter);
                static int ShowKritMHD (void);
                static int TerminateKritMHD (void);
                static void HoleCronBestand (double a);
                static void EanGew (char *, BOOL);
                static int ReadEan (double);
                static int Testa_kun (void);
				static BOOL Testa_kun (double art);
                static void GetLastMeAuf (double);
                static void GetLastMe (double);
                static int TestNewArt (double);
                static int fetchMatchCode (void);
                static void ReadChargeGew (void);
                static BOOL DispCharge (void);
                static BOOL DispErsatz (void);
                static BOOL AddPosTxt (char *);
                static BOOL BestandOk (void);
                static int fetcha (void);
                static int fetchaDirect (int);
                static int fetcha_kun (void);
                static int fetchkun_bran2 (void);
                static int ChMeEinh (void);
                static long GenAufpTxt0 (void);
			    static BOOL TxtNrExist (long);
                static long GenAufpTxt (void);
                static int Texte (void);
                static int Querya (void);
                static HANDLE StarteProcessWgChart (int dAuftrag); 
                static HANDLE StarteProcessShowOP (int kun); 
                static HANDLE StarteProcessShowBm (int Modus); 
                static HANDLE StarteProcessTourDat (void); 
                static int KillProcessWgChart (void); 
                static int KillProcessShowOP (void); 
                static int KillProcessShowBm (void); 
                static int KillProcessShowBm1 (void); 
                static int KillProcessTourDat (void); 
                static int doStd (void);
				//LAC-9 A
                static int doAktAuf (void);
                static int doBestellvorschlag (int);
                static int doletzteBestellung (int);
                static int doKritischeMhd (void); //LAC-84
                static int dokommldat (void); //LAC-116
                static void initReiterv (void);
                static int doHWG (int dhwg, int dwg, int idx);
				static void ActivatelBest (int dauf);
				static void ActivateKritMhd (); //LAC-84
				static void ActivateKommldat (); //LAC-116
				static void ActivateHWG (int dhwg,int dwg,int idx);
				//LAC-9 E
                static int doActivate ();
				static void ActivateStd ();
				static void hwgchart_fuellen (int anz_tage_ab,int anz_tage); //LAC-109
				static int SwitchStd ();
                static void TestMessage (void);
                static int setkey9me (void);
                static int setkey9basis (void);
                static int Savea (void);
                static double GetAufMeVgl (void);
                static double GetAufMeVgl (double, double, short);
                static BOOL BsdArtikel (double);
                static void BucheBsd (double, double, short, double);
                static void UpdateBsd (void);
                static void DeleteBsd (void);
                static void WriteAufkun (void);
                static BOOL testdecval (void);
                static void WritePos (int);
                static void WritePos (void); //LAC-124
                static int WriteAllPos (void);
                static int dokey5 (void);
                static int doreload (int dhwg, int dwg, int idx); //LAC-9
                static int WriteRow (void);
                static int TestRow (void);
                static void GenNewPosi (void);
                static int PosiEnd (long);
                static void SaveAuf (void);
                static void SetAuf (void);
                static void RestoreAuf (void);
                static int Schirm (void);
                static int SetRowItem (void);
                static void UpdHwgChartAktiv (int dhwg);
                static int  Getib (void);
                static void ChoiseLines (HWND, HDC);
                static HWND CreateEnter (void);
                static long EnterPosRab (void);
                static HWND CreateAufw (void);
                static void AnzAufWert (void);
                static BOOL sg_menge_ok (short sg, double sg_mindestmenge, double sg_staffel); //LAC-104
                static void sg_menge_ok (void); //LAC-104
                static HWND CreateAufGew (void);
//                static HWND CreateWindowLeiste (void); //LAC-9
                static void ZeigeLeiste (void); //LAC-9
				static void LadeBestellvorschlag (void); //LAC-9
				int GetZeileLeiste (void); //LAC-9

                static double GetArtGew (int);
                static void AnzAufGew (void);
                static long ChoiseProdLgr (long);
                static long TestProdLgr (long);
                static int ProdMdn (void);
                static void TestPfand (double);
                static void TestLeih (double);
//                static int testaufschlag (void);

                void EnterLdVk (void);
                void EnterPrVk (void);
                void SetAufVkPr (double);
                void PrVkFromList ();
                void UpdatePrVk (int);
				void SetPreise (void);
                void MoveSaW (void);
                void MovePlus (void);
                void MoveAufw (void);
                void MoveAufGew (void);
                void MoveLeiste0 (void); //LAC-9
                void CloseAufw (void);
                void CloseAufGew (void);
                void InitSwSaetze (void);
                int ToMemory (int pos);
                void SetStringEnd (char *, int);
                void uebertragen (void);
                void ShowDB (short, short, long, long);
                void ReadDB (short, short, long);
                void SetSchirm (void);
                void GetListColor (COLORREF *, char *);
                void SetPreisTest (int);
                void GetCfgValues (void);
                void SetNoRecNr (void);
                void FillWaehrungBz (void);
                void EnterAufp (short, short, long,
                                short, long,  char *);
                void ShowAufp (short, short, long,
                                short, long,  char *);
                void RollbackBsd ();
                void DestroyWindows (void);
//                void WorkAufp (void);
                void DestroyMainWindow (void);
                HWND CreateMainWindow (void);
                void MoveMamain1 ();
                void MaximizeMamain1 ();
                void SetMessColors (COLORREF, COLORREF);

                void SetChAttr (int);
                static void SetFieldAttr (char *, int); //LAC-56  static
                void DelField (char *); //LAC-9
                void DelFieldLetztDaten (void); //LAC-9
                void SetFieldLetztDaten (void); //LAC-9
                void SetSpaltenAktAuf (void); //LAC-9
                void SetSpaltenBestellvorschlag (void); //LAC-9
                void SetSpaltenletzteBestellung (void); //LAC-9
                void SetSpaltenKritischeMhd (void); //LAC-84
                void SetSpaltenkommldat (void); //LAC-116
                void SetSpaltenHWG (void); //LAC-9
                static int  GetFieldAttr (char *);
                void Geta_bz2_par (void);
                void Geta_kum_par (void);
                void Getauf_me_pr0 (void);
                void Getwa_pos_txt (void);
                void SetRecHeight (void);

                void SethwndTB (HWND);
                void SetTextMetric (TEXTMETRIC *);
                void SetLineRow (int);
                void SetListLines (int); 
                void OnPaint (HWND, UINT, WPARAM, LPARAM);
                void MoveListWindow (void);
                void BreakList (void);
                void OnHScroll (HWND, UINT, WPARAM, LPARAM);
                void OnVScroll (HWND, UINT, WPARAM, LPARAM);
                void OnSize (HWND, UINT, WPARAM, LPARAM);
                void StdAuftrag (void);
                void FunkKeys (WPARAM, LPARAM);
                int  GetRecanz (void);
                void SwitchPage0 (int);
                HWND Getmamain2 (void);
                HWND Getmamain3 (void);
                void SetFont (mfont *); 
                void SetListFont (mfont *);
                void ChoiseFont (mfont *);
                void FindString (void);
                void SetLines (int);
                int GetAktRow (void);
                int GetAktRowS (void);
                void SetColors (COLORREF, COLORREF);
                void SetListFocus (void);
 			    void PaintUb (void);
				static BOOL ContainsService ();
				static void ReadPtabAufschlag ();
 			    virtual void AfterPaint (HDC hdc, RECT *rect, int pos, int SubRow);
				void BsdCalculate ();
				static void CalcLdPrPrc (double vk_pr, double ld_pr);
				void StndComplete ();
				void StndComFailed ();
				void PrintAmpel (HDC hDC);
				BOOL TestStndActive (); 
				BOOL SetRowBkColor (HDC hdc, int einzel, BOOL AktZeile); //LAC-8
			    virtual BOOL FillColRect (HDC hdc, RECT *rect, int einzel, BOOL AktZeile); //LAC-8

// CSmtReader
				short Get (double a);
				double Get_inh_abverk ();
				short Get_hwg ();
				short Get_wg (); //LAC-9
				short Get_wg1 (); //LAC-191
				short Get_wg2 (); //LAC-191
				short Get_wg3 (); //LAC-191
				short Get_wg4 (); //LAC-191
				short Get_wg5 (); //LAC-191
				double Get_a_gew ();
				double Get_bsd ();  //LAC-163
				double Get_bsd2 (); //LAC-163

// CThreadAction
				virtual int Execute (void *Param);
};
#endif