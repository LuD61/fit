// SmtReader.h: Schnittstelle f�r die Klasse CSmtReader.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SMTREADER_H__E501616C_02DD_4BF3_982C_84C19FA7ED96__INCLUDED_)
#define AFX_SMTREADER_H__E501616C_02DD_4BF3_982C_84C19FA7ED96__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CSmtReader  
{
public:
	CSmtReader();
	virtual ~CSmtReader();
	virtual short Get (double a)=0;
	virtual double Get_inh_abverk ()=0;
	virtual short Get_hwg ()=0;
	virtual short Get_wg ()=0; //LAC-9
	virtual short Get_wg1 ()=0; //LAC-191
	virtual short Get_wg2 ()=0; //LAC-191
	virtual short Get_wg3 ()=0; //LAC-191
	virtual short Get_wg4 ()=0; //LAC-191
	virtual short Get_wg5 ()=0; //LAC-191
	virtual double Get_a_gew ()=0;
	virtual double Get_bsd ()=0; //LAC-163
	virtual double Get_bsd2 ()=0; //LAC-163

};

#endif // !defined(AFX_SMTREADER_H__E501616C_02DD_4BF3_982C_84C19FA7ED96__INCLUDED_)
