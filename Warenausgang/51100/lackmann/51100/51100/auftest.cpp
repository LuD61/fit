
//=================================================================
//======================== Version ================================
//=================================================================
static char Version1[] = {"  SETEC Sondererweiterung Fa. Lackmann "};
static char Version2[] = {"  Version 28.11.2014 01:08"};
static char VersAufk[18] = {"Vers: 28.11.2014"};
//=================================================================
//======================== Version ================================
//=================================================================


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "DdeDial.h"
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "dbclass.h"
#include "mo_menu.h"
#include "kun.h"
#include "sys_par.h"
#include "vertr.h"
#include "mdn.h"
#include "fil.h"
// #include "mo_numme.h"
#include "mo_auto.h"
#include "mdnfil.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "mo_qa.h"
#include "ls.h"
#include "a_bas.h"
#include "ptab.h"
#include "mo_aufl.h"
#include "bmlib.h"
#include "tou.h"
#include "wo_tou.h"
#include "mo_qtou.h"
#include "mo_qvertr.h"
#include "message.h"
#include "mo_kompl.h"
#include "mo_last.h"
#include "mo_kf.h"
#include "auto_nr.h"
#include "mo_progcfg.h"
#include "mo_lgr.h"
#include "mo_lsgen.h"
#include "mo_inf.h"
#include "mo_ch.h"
#include "mo_chq.h"
#include "mo_chqex.h"
#include "datum.h"
#include "mo_vers.h"
#include "ls_txt.h"
#include "aktion.h"
#include "akt_krz.h"
#include "a_pr.h"
#include "mo_a_pr.h"
#include "mo_preis.h"
#include "listcl.h"
#include "colbut.h"
#include "lbox.h"
#include "searchkunlief.h"
#include "searchliefkun.h"
#include "searchaart.h"
#include "searchkun.h"
#include "kunlief.h"
#include "kasse.h"
#include "Process.h"
#include "KunTour.h"
#include "enterfunc.h"
#include "lizenz.h"
#include "ClientOrder.h"
#include "Application.h"
#include <vector>
#include "AdrDialog.h"
#include "aufptmp.h"
#include "kritmhd.h"
#include "best_vorschlag.h"

#define CString Text

#define MAXLEN 40
// #define MAXPOS 3000
#define LPLUS 1
 
#define IDM_FRAME     901
#define IDM_REVERSE   902
#define IDM_NOMARK    903
#define IDM_EDITMARK  904
#define IDM_PAGEVIEW  905
#define IDM_LIST      906
#define IDM_PAGE      907
#define IDM_FONT      908
#define IDM_FIND      909
#define IDM_ANZBEST   910
#define IDM_STD       911
#define IDM_BASIS     912
#define IDM_LGR       913
#define IDM_LGRDEF    914
#define IDM_POSTEXT   915
//#define IDM_VINFO     916
#define IDM_LDPR      917
#define IDM_VKPR      918
#define IDM_MUSTER    923 
#define IDM_KUNLIEF   924
#define IDM_LS        925 
#define IDM_KOMMIS    926
#define QUERYAUF      920 
#define QUERYKUN      921 
#define QUERYTOU      922 
#define IDM_NACHDRUCK 924 
#define IDM_PRICEFROMLIST 927
#define IDM_PARTYSERVICE    931 
#define IDM_FULLTAX   932 
#define IDM_REFRESH_STND      933
#define IDM_BESTELLVORSCHL    934
#define IDM_IGNOREMINBEST    935
#define IDM_FIXWG    936
#define IDM_FIXPAL    937
#define IDM_FIXBMP    938
#define IDM_SHOWBMP   939
#define IDM_SHOWBMPP  940
#define IDM_KRITMHD   941
#define IDM_PALETTE   942
#define IDM_WARENKORB  943
#define IDM_BESTAND   944
#define QUERYSHOP     945
#define IDM_STORNO    946    //LAC-210
#define IDM_TOURHINWEIS 947 
#define PARTY_POSITION 14

#define TAB 9

static BOOL NewStyle = FALSE;
static BOOL SpaltenWaehlen = FALSE; //LAC-??
static int hwgchart_fuellen = 0; //LAC-109
static int hwgchart_fuellen_bis = 0; //LAC-109
static int cfg_anz_wo_letzteBestellung = 4; //LAC-132
static int cfg_anz_wo_Bestellvorschlag = 4; //LAC-156
static char FaxTrenner = TAB;

static char *Programm = "51100Lack";

static BOOL PrintLM = FALSE;

int dVorbestellung_lieferdat;

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL StaticWProc(HWND,UINT,WPARAM,LPARAM);
static int ProcessMessages(void);
void     InitFirstInstance(HANDLE);
BOOL     InitNewInstance(HANDLE, int);
void DestroyStorno ();
void EnterStorno (void);
void     DisplayLines ();
void     MoveButtons (void);
void     PrintButtons (void);
void     ShowDaten (void);
int      lesetou (void);
int      TouZTest (void);
static   void FreeAufk (void);
static   void FreeAufk (int dauf);
static   int testkun0 (void);
static   int lesekun0 ();
static   void EnableArrows (form *, BOOL);
static   void IncDatum (char *);
static   void TelefonList (void);  
static   int FaxAuf (void);
static   int RunClipDial (char *);
static   int RunDdeDial (char *);
static   void TestActiveDial (void);
static   void SetMuster (BOOL); 
static   void SetStorno (BOOL);  //LAC-220
static   void SetAnsehen (void); 
static   void SetKunLief (void); 
static   void SetMusterAuftrag (void);
static   void StartTourhinweis (void);
static   void SetStornoAuftrag (void);
static   void StartLsProc (void);
static    int QueryNachDruck (void);
static   int ChoiseKunLief0 (void);
static   int ChoiseKunLief (void);
static   BOOL ShowKun ();
static void RestartProcess (int kun);
static void EnterKommissionierer (BOOL);
static void DestroyKommissionierer ();
static BOOL OfPoOver (short pmdn, long pkun);
static void CloseOfPo ();
static	 void CheckPartyService (BOOL b);
static	 void SetPartyActive ();
static   void EnablePartyService (BOOL b);
void EnableFullTax ();
void SetLsToFullTax ();
void SetLsToStandardTax ();
void SetToFullTax ();
void SetToStandardTax ();
void TestLizenz ();
void DeleteRefreshStnd ();
static BOOL TestVerifyKun ();
static BOOL TestAufKun (long lieferdat); //LAC-21
int doreload (void); //LAC-??
void FillCombo3 (int dReiter) ; //LAC-??
void FillCombo4 (int dReiter); //LAC-??
BOOL LskVorhanden (void); //LAC-84
int Write_Bestellvorschlag (void); //LAC-157
void HoleHWG (void); 

static int of_pocursor = -1;
static int auf_cursor = -1; //LAC-184


static int dokey5 (void);
static int dokey12 (void);
static int doliste (void);
static int KopfTexte (void);
static int FussTexte (void);


static char stat_defp_von [3] = {"1"};
static char stat_defp_bis [3] = {"3"};;
static char stat_deff_von [3] = {"1"};
static char stat_deff_bis [3] = {"4"};;
static char tou_def_von [10]  = {"1"};
static char tou_def_bis [10]  = {"9999"};
static char abt_def_von [6]   = {"0"};
static char abt_def_bis [6]   = {"9999"};
static BOOL AbtRemoved = TRUE;
static BOOL DrkKunRemoved = TRUE;
static BOOL LuKunRemoved = TRUE;
static BOOL FixDat = FALSE;
static BOOL EnterKomm = FALSE;

static BOOL NoClose = FALSE;

extern BOOL ColBorder;

static int  tou_def_zwang = 0;
static long  datum_plus = 0;
static BOOL telelist;
static long  ldatdiffminus = 0;
static long  ldatdiffplus = 365;
static BOOL  lsdirect = FALSE;
static BOOL  auftou = TRUE;
static BOOL gen_div_adr = FALSE;
static BOOL IsAng = FALSE;
static int  Startsize = 0;
static BOOL auf_art_active = FALSE;
static int bg_cc_par = 0;
static int zustell_par = 0;
static BOOL akt_preis = TRUE;
static BOOL KunLief = FALSE;
static BOOL KunLiefKz = FALSE;
static BOOL SearchKunDirect = FALSE;
static BOOL Color3D = TRUE;
static BOOL UpdatePersNam = TRUE;
static BOOL Wochentag = FALSE;
static BOOL VerifyKun = FALSE;
HACCEL MenAccelerators = NULL;
HANDLE  hMainInst;
HWND   hMainWindow;
HWND   mamain1;
BOOL   NoMenue = FALSE;
short  StartMdn = 0;
int  StartKun = 0;  //LAC-184
char StartPers [13];
BOOL KunFromTeleList = FALSE;
BOOL TestOfPo = FALSE;
BOOL DisplayFreeText2 = FALSE;
static BOOL FitParams = FALSE;

BOOL IsPartyService = FALSE;
short PartyMdn = 0;
CClientOrder::STD_RDOPTIMIZE StndRdOptimize = CClientOrder::No;

HWND ForegroundWindow;

HANDLE KunTour = NULL;
BOOL TeleTour = FALSE;
static DB_CLASS DbClass;
extern AUFPTMP_CLASS Aufptmp;
extern KRITMHD_CLASS kritmhd;
BEST_VORSCHLAG_CLASS Best_vorschlag;
void GetForeground (void)
{
	ForegroundWindow = GetForegroundWindow ();
}

void SetForeground (void)
{
	SetForegroundWindow (ForegroundWindow);
}


void CleanTagAnr ()
{
	if (TeleTour)
	{	
		beginwork ();
		DbClass.sqlcomm ("delete from taganr where dat < today-3");
		commitwork ();
	}
}


extern HWND  ftasten;
extern HWND  ftasten2;

static int WithMenue   = 1;
static int WithToolBar = 1;

static   TEXTMETRIC tm;

static int PageView = 0;

static char *TXT_WORK   = "Bearbeiten";
static char *TXT_SHOW   = "Anzeigen";
static char *TXT_DEL    = "L�schen";
static char *TXT_PRINT  = "Drucken";

static char *TXT_ACTIVE = TXT_WORK;


static int IDM_ACTIVE = IDM_WORK;

static PAINTSTRUCT aktpaint;

HMENU hMenu;

// static int CallT1 =  850;
// static int CallT2 =  851;
// static int CallT3 =  852;

#define CallT1  850
#define CallT2  851
#define CallT3  852


struct PMENUE dateimen[] = {
	                        "&1 Bearbeiten", " ",  NULL, IDM_WORK, 
                            "&2 Anzeigen",   " ",  NULL, IDM_SHOW,
						    "&3 Drucken",    " ",  NULL, IDM_PRINT,
							"",              "S",  NULL, 0, 
						    "Position f�r WgChart fixieren",    " ",  NULL, IDM_FIXWG,
						    "Position f�r PalettenChart fixieren",    " ",  NULL, IDM_FIXPAL,
						    "Position f�r Artikelbild fixieren",    " ",  NULL, IDM_FIXBMP,
							"",              "S",  NULL, 0, 
	                        "B&eenden",      " ",  NULL, IDM_EXIT,

                             NULL, NULL, NULL, 0};


struct PMENUE bearbeiten[] = {
	                        "&Fax-Nummer      <SHIFT F8>",   "G", NULL, CallT1, 
	                        "&Telefon-Nummer  <SHIFT F9>",   "G", NULL, CallT2, 
	                        "&Preisgruppen    <SHIFT F10>",  "G", NULL, CallT3, 
							"",                            "S",   NULL, 0, 
	                        "Artikelbild originalgr.      <ALT B>",    "G", NULL, IDM_SHOWBMP, 
	                        "Artikelbild permanent.   <ALT A>",    "G", NULL, IDM_SHOWBMPP, 
						    "Kritische MHD                <ALT K>",    " ",  NULL, IDM_KRITMHD,
						    "Cron Bestand                 <ALT C>",    " ",  NULL, IDM_BESTAND,
						    "Tour Palettenbelegung  <ALT P>",    " ",  NULL, IDM_PALETTE,
						    "Warenkorb                    <ALT W>",    " ",  NULL, IDM_WARENKORB,
							"",                            "S",   NULL, 0, 
	                        "A&bbruch F5",                 " ", NULL, KEY5, 
							"Spe&ichern F12",              " ", NULL, KEY12,
							"",                            "S",   NULL, 0, 
							"&Musterauftrag ",             " ", NULL, IDM_MUSTER,
							"&Stornieren ",                " ", NULL, IDM_STORNO,
							"&Tourenhinweis ",               " ", NULL, IDM_TOURHINWEIS,
							"&Lieferscheine ",             " ", NULL, IDM_LS,
							"&Nachdruck ",                 " ", NULL, IDM_NACHDRUCK,
							"&Kommissionierer ",           " ", NULL, IDM_KOMMIS,
							"",                            "S", NULL, 0, 
							"&Preise aus Liste ",          "G", NULL, IDM_PRICEFROMLIST,
							"",                            "S",   NULL, 0, 
							"Party&service ",              " ", NULL, IDM_PARTYSERVICE,
							"&voller Steuersatz", 
							                               " ", NULL, IDM_FULLTAX,
							"",                            "S", NULL, 0, 
	                        "&Anzahl Bestellartikel",      " ", NULL, IDM_ANZBEST, 
							"&Standortlager �ndern",       " ", NULL, IDM_LGR,
							"&Default Standortlager",      " ", NULL, IDM_LGRDEF,
							"",                            "S",   NULL, 0, 
							"Bestellvorschlag editieren <F2>",  "G", NULL, IDM_BESTELLVORSCHL,
							"Mind.BestMe. ignorieren bei Preis 0 <F3>",  "G", NULL, IDM_IGNOREMINBEST,
							"",                            "S",   NULL, 0, 
	                        "&Suchen",                     "G", NULL, IDM_FIND, 
                            "&Positionstexte",             "G", NULL, IDM_POSTEXT, 
                             NULL,                         NULL, NULL, NULL, 
                             NULL,                         NULL, NULL, NULL, 
                             NULL,                         NULL, NULL, NULL, 
                             NULL,                         NULL, NULL, NULL, 
                             0};

struct PMENUE changeldpr = {"&Laden-Preis editieren  ALT L","G", NULL, IDM_LDPR}; 
struct PMENUE changevkpr = {"&Verkaufspreis editieren  ALT V","G", NULL, IDM_VKPR}; 

struct PMENUE ansicht[] = {
//                            "&Lieferadresse",              " ", NULL, IDM_KUNLIEF,   
                            "&Basismenge",                 "G", NULL, IDM_BASIS, 
                            "&Fonteinstellung",            "G", NULL, IDM_FONT,
							"",                            "S",   NULL, 0, 
							"&Standardauftrag neu einlesen", 
							                               " ", NULL, IDM_REFRESH_STND,
                             NULL, NULL, NULL, 0};

struct PMENUE menuetab[] = {"&Datei",      "M", dateimen,   0, 
                            "&Bearbeiten", "M", bearbeiten, 0, 
                            "&Optionen",   "M", ansicht,    0, 
                            "&?",          " ", NULL,       IDM_VINFO, 
						     NULL, NULL, NULL, 0};

static int ActiveMark = IDM_FRAME;

static char InfoCaption [80] = {"\0"};


extern HWND hWndToolTip;
HWND hwndTB;
HWND hwndCombo1;
HWND hwndCombo2;
HWND hwndCombo3;
HWND hwndCombo4;

static TBBUTTON tbb[] =
{
 0,               IDM_WORK,   TBSTATE_ENABLED, 
                               TBSTYLE_BUTTON, 
 0, 0, 0, 0,
 1,               IDM_SHOW,   TBSTATE_ENABLED, 
	                          TBSTYLE_BUTTON,
 0, 0, 0, 0,
 2,               IDM_FIND,   TBSTATE_INDETERMINATE, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 3,               IDM_PRINT,  TBSTATE_ENABLED, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED,
                  TBSTYLE_SEP, 
 0, 0, 0, 0,
 /*
 4,               IDM_LIST,   TBSTATE_ENABLED | TBSTATE_CHECKED, 
                  TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 */

/*
 5,               IDM_STD,TBSTATE_INDETERMINATE, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,

 5,               IDM_PAGE, TBSTATE_INDETERMINATE, 
                  TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 */
 6,               IDM_INFO,   TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 
 7,               KEYTAB, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 8,               KEYSTAB, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 9,               KEYDOWN, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
10 ,               KEYUP, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,


 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,

11 ,              IDM_LGR, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
12 ,              IDM_LGRDEF, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,

  0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,

};

static char *Version[] = {"  Auftragsbearbeitung  ",
                          "  Programm    51100Lack", 
                          Version1, 
						  Version2,
						   NULL,
};

HWND    QuikInfo = NULL;

void     CreateQuikInfos (void);
static int CallTele1 ();
static int CallTele2 ();
static int CallTele3 ();


static int MoveMain = FALSE;
static int cfgccmarkt = 0;
static int kun_sperr = 1;
static int tour_zwang = 0;
static int tour_datum = 1;
static int abw_komm = -1;
static int fen_par = 0;
static int lutz_mdn_par = 0;
static BOOL fil_lief_par = FALSE;
static long tour_div = 100;
static long akt_lager = 0;
static BOOL vertr_abr_par = TRUE; 
static BOOL vertr_kun = TRUE;
static BOOL tou_wo_tou = FALSE;
static BOOL tou_par = TRUE;
static BOOL wo_tou_par = TRUE;
static BOOL use_kopf_txt = FALSE;
static int KomplettMode = 0;
static HWND DlgWindow = NULL;
static BOOL autosysdat = FALSE;
static BOOL autokunfil = FALSE;
static int listedirect = 0;
static BOOL PosOK = FALSE;
static BOOL LdVkEnabled = FALSE;
static BOOL PrVkEnabled = FALSE;
static BOOL ShowBmEnabled = TRUE; //erst mal immer TRUE
static char *ClipDial = NULL;
static char *DdeDialProg = NULL;
static BOOL DdeDialActive = TRUE;
static char *DdeDialService = NULL;
static char *DdeDialTopic = NULL;
static char *DdeDialItem = NULL;
static HANDLE DialPid;
static BOOL EnableKunDirect = TRUE;
static int TransactMax = 1;   //LAC-184   generell immer raus nach Komplett, F12, F5, nach QueryKun
static BOOL TransactMessage = FALSE;
static int tcount = 0;
static int x_Ampel = 650;
static int y_Ampel = 48;
static BOOL inRestart = FALSE;

HICON telefon1;
HICON fax;

HBITMAP btelefon;
HBITMAP btelefoni;

ColButton CTelefon = { 
//                      "Telefon", -1, -1,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
//                       btelefon, 6, 8,
                       btelefon, -1, -1,
                       NULL, 0, 0,
                       RGB (255, 255, 255),
//                       BLUECOL,
                       GRAYCOL,
                       -1};

ColButton CFax =    { "Fax", -1, -1,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (255, 255, 255),
//                     BLUECOL,
                       GRAYCOL,
                       -1};
ColButton CGruppen =    { "PG", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
//                        BLUECOL,
                          GRAYCOL,
                          -1};
/*
static ITEM itelefon1 ("", (char *) &telefon1, "", 0);
static ITEM ifax      ("", (char *) &fax,      "", 0);



static field _buform [] = {
&ifax,             4, 2, 2,73, 0, "", ICON| BORDERED, 0, 0,
                                                           CallT1,
&itelefon1,        4, 2, 4,73, 0, "", ICON | BORDERED, 0, 0,
                                                           CallT2,
};
*/



static mfont BuFont = {
                      "Courier New", 
                       120, 
                       100, 
                       0, 
                       RGB (-1, 0, 0), 
                       RGB (-1, 0, 0),
                       0, 0};

static BOOL temode = 0;
static ITEM itelefon1 ("", (char *) &CTelefon, "", 0);
static ITEM ifax      ("", (char *) &CFax,     "", 0);
static ITEM iGruppen  ("", (char *) &CGruppen, "", 0);

static ITEM iOK      ("", "    OK     ", "", 0);
static ITEM iCancel  ("", " Abbrechen ", "", 0);

static field _buform [] = {
&ifax,             4, 2, 2,73, 0, "", COLBUTTON, 0, 0,
                                                           CallT1,
&itelefon1,        4, 2, 4,73, 0, "", COLBUTTON, 0, 0,
                                                           CallT2,
&iGruppen,         4, 2, 6,73, 0, "", COLBUTTON, 0, 0,
                                                           CallT3,
};

static form buform = {3, 0, 0, _buform, 0, 0, 0, 0, &BuFont};



static char *qInfo [] = {"Neue Datei �ffnen",
                         "Tablle �ffnen",
                         "Query zu Tabelle",
                         "Drucken",
                         "Funktion f�r aktives Eingabefeld",
                         "S�tze in Listenform anzeigen",
                         "Standardauftrag",
                         "Lager f�r Standort �ndern",
						 "Default-Lager f�r Standort setzen",
						 "Bestellvorschlag editieren / nur Lesen",
                          0, 
                          0};

static UINT qIdfrom [] = {IDM_WORK, IDM_SHOW, IDM_DEL,
                         IDM_PRINT, IDM_INFO,
                         IDM_LIST, IDM_STD,IDM_LGR, IDM_LGRDEF, 0, 0, 0};

static char *telenr =  "Telefon-Nummer";
static char *telelst = "Telefon-Liste";
static int telepos = 3;
static BOOL telekun = FALSE;

static char *qhWndInfo [] = {"Tabellen-Linien festlegen", 
                             "Farbe f�r Vordergrund und Hintergrund "
                             "festlegen",
                             "Fax-Nummer",
                             telenr,
                             "Preisgruppen",
                             "Spalten hinzuf�gen ",
                             "Spalten abw�hlen ",
                             "Abbrechen",
                             "Speichern",
                             0};

static HWND *qhWndFrom [] = {&hwndCombo1, 
                             &hwndCombo2,
                             &buform.mask[0].feldid,
                             &buform.mask[1].feldid,
                             &buform.mask[2].feldid,
                             &hwndCombo3,
                             &hwndCombo4,
							 NULL,
							 NULL,
                             NULL}; 

void SetTeleNr (void)
{
	      qhWndInfo[telepos] = telenr;
}

void SetTeleLst (void)
{
	      qhWndInfo[telepos] = telelst;
}


static char *Combo1 [] = {"wei�e Tabelle" ,
                          "hellgraue Tabelle",
                          "schwarze Tabelle",
                          "graue Tabelle",
                          "vertikal wei�",
                          "vertikal hellgrau",
                          "vertikal schwarz", 
                          "vertikal grau",
                          "horizontal wei�",
                          "horizontal hellgrau",
                          "horizontal schwarz", 
                          "horizontal grau",
                          "keine Linien",
                           NULL};
/*
static char *Combo2 [] = {"wei�er Hintergrund" ,
                          "blauer Hintergrund",
                          "schwarzer Hintergrund",
                          "grauer Hintergrund",
						  "hellgrauer Hintergrund",
                          NULL};

  */
static char *Combo2 [] = {"wei�er Hintergrund" ,
                          NULL};


//LAC-118
static char *Combo3 [] = {"Spalte hinzuf�gen             " ,
                          "Bestellvorschlag              ",
                          "letzter Verkaufspreis         ",
                          "letztes Lieferdatum           ",
                          "letzte Bestellung             ",
                          "letzte Lieferung              ",
                          "letzte SollLieferung          ",
                          "St�ck / Karton                ",
                          "Gew.d.Kartons                 ",
                          "Bestand                       ",
                          "LD.EURO                       ",
                          "Rabatt %                      ",
                          "LD Marge                      ",
                          "Marge %                       ",
                          "MHD Datum                     ",
                           NULL};

//LAC-118
static char *Combo4 [] = {"Spalte abw�hlen               " ,
                          "Bestellvorschlag              ",
                          "letzter Verkaufspreis         ",
                          "letztes Lieferdatum           ",
                          "letzte Bestellung             ",
                          "letzte Lieferung              ",
                          "letzte SollLieferung          ",
                          "St�ck / Karton                ",
                          "Gew.d.Kartons                 ",
                          "Bestand                       ",
                          "LD.EURO                       ",
                          "Rabatt %                      ",
                          "LD Marge                      ",
                          "Marge %                       ",
                          "MHD Datum                     ",
                           NULL};


static COLORREF Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL,
							BLACKCOL,
};

static COLORREF BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL,
							LTGRAYCOL,
};

static COLORREF MessBkCol = DKYELLOWCOL; 
static COLORREF MessCol   = BLACKCOL; 

static int combo1pos = 0;
static int combo2pos = 0;
static int combo3pos = 0;
static int combo4pos = 0;

mfont lFont = {
               "Courier New", 
               120, 
               100, 
               0, 
               RGB (-1, 0, 0), 
               RGB (-1, 0, 0),
               0, 0};

static int m3Size = 0;

extern LS_CLASS ls_class;
static WA_PREISE WaPreis;
static KUN_CLASS kun_class;
static AUFPLIST aufpListe;
static MENUE_CLASS menue_class;
static FIL_CLASS fil_class;
static MDN_CLASS mdn_class;
static ADR_CLASS adr_class;
static VERTR_CLASS vertr_class;
static QueryClass QClass;
static TOU_CLASS Tour;
static QueryTou QTClass;
static QueryVertr QVtrClass;
static KompAuf AufKompl;
static LastAuf AufLast;
static LSTXTLIST lstxtlist;
static AUTO_CLASS AutoClass;
static PROG_CFG ProgCfg ("51100Lack");
static LgrClass LgrClass;
static PTAB_CLASS ptab_class;
static KINFO Kinfo;
static VINFO Vinfo;
static FITL Fitl;

extern MELDUNG Mess;
extern SYS_PAR_CLASS sys_par_class;



static char mdn [10];
static char fil [10];

static BOOL FreeAufNr = TRUE;

/* ****************************************************************/
/* Auftragskopf    
                                              */
 int mench = 0;

static BOOL ldat_ok = FALSE;
static long akt_auf = 0l;
static long akt_tou = 0l;
static long akt_kun = 0l;
static long adr_nr = 0;

char akt_ldat[12];


long LiefAdr = 0l;
char orders_id[31];
char auftrag[9] = "0";
char kunfil[2];
char kuns[9];
char kunliefs[9];
char ldat[12] = {" "};
char kdat[12];
char fix_dat[12];
char lzeit[7];
char auf_status[2];
char auf_stat_txt[40];
char kd_auftrag[31];
char k_kurzb[17];
char tele[26];
char vertrs [9];
char vertr_krz[17];
char tour[10];
char tour_krz[50];
char htext[51];
char auf_art [5];
char auf_art_txt [20];

static int dsqlstatus;
static int inDaten = 0;
static HBITMAP ArrDown;

char StornoText[sizeof aufk.hinweis] = {" "}; 

static int lese_shop (void);
static int leseauf (void);
static int setzeStartKun (void); //LAC-184
static int lesekun (void);
static int setkey9kun (void);
static int testtou (int);
static int testvertr (void);
static int saveldat (void);
static int testldat (void);
static int testtou0 (void);
static int testvertr0 (void);
static int setkey9tou (void);
static int setkey9vertr (void);
static int setkey9aufart (void);
static int setkey10_11 (void);
static int testhtext (void);
static int SwitchArt (void);
static int testkommdat (void);
static int testfixdat (void);
static int getaufart (void);
static int testaufart0 (void);
static int testaufart (void);
static int QueryAuf (void);
static int QueryAng (void);
static BOOL ldatdiffOK (void);
static BOOL KommdatOk (void);
// void EnterKommissionierer ();


static ColButton CMuster   = { "Muster",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 50, 3,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
//                             NOCOLPRESS | NOCOLBORDER,
                             NOCOLPRESS,
//                             0,
};

static ColButton CTourhinweis   = { "Tourenhinweis",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 50, 3,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
                             NOCOLPRESS,
};

char ButtonStornoText [sizeof aufk.hinweis + 10] = {"         Storno"} ;  //LAC-210
static ColButton CStorno   = { ButtonStornoText,    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 10, 3,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
                             NOCOLPRESS,
};


/****
struct COLBUT
{
     char *text1;
     int tx1;
     int ty1;
     char *text2;
     int tx2;
     int ty2;
     char *text3;
     int tx3;
     int ty3;
     HBITMAP bmp;
     int bmpx;
     int bmpy;
     HICON icon;
     int icox;
     int icoy;
     COLORREF Color;
     COLORREF BkColor;
     BOOL aktivate;
     COLORREF BorderColor;
     COLORREF BorderLtColor;
     BOOL NoFocus;
};
************/


static ColButton CLs   = { "Lieferscheine",    0, 0, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 50, 3,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
                             NOCOLPRESS,
//                             0,
};

static ColButton CNachDruck   = { "Nachdruck",    0, 0, 
								   NULL,  0, 0,
								   NULL,  0, 0,
								   NULL, 50, 3,
								   NULL,  0, 0,
								   BLUECOL,
								   LTGRAYCOL,
								   NOCOLPRESS,
//                             0,
};


HBITMAP SelBmp = NULL;

CFIELD *MusterButton =  new CFIELD ("muster", (ColButton *) &CMuster, 10, 2, 21, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_MUSTER, 0, 0, TRANSPARENT);

CFIELD *TourhinweisButton =  new CFIELD ("tourhinweis", (ColButton *) &CTourhinweis, 15, 2, 41, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_TOURHINWEIS, 0, 0, TRANSPARENT);

CFIELD *StornoButton =  new CFIELD ("storno", (ColButton *) &CStorno, 80, 2, 57, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_STORNO, 0, 0, TRANSPARENT);
CFIELD *LsButton =  new CFIELD ("lsproc", (ColButton *) &CLs, 12, 0, 9, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 IDM_LS, 0, 0, TRANSPARENT);

CFIELD *NachDrButton =  new CFIELD ("nachdruck", (ColButton *) &CNachDruck, 9, 0, 31, 0,  NULL, "", 
									CCOLBUTTON,
									IDM_NACHDRUCK, 0, 0, TRANSPARENT);


static mfont musterfont = {
                       FontName, 
                       FontHeight, 
                       FontWidth, 
                       FontAttribute, 
                       BLUECOL, 
                       WHITECOL,
                       0, 0};


static ITEM iMuster ("muster", " Musterauftrag ", "", 0);

static field _musterf [] = {
&iMuster,          0, 0, 0, 50, 0,  "", DISPLAYONLY,       0, 0, 0,
};

static form musterf = {1, 0, 0, _musterf, 0, 0, 0, 0, &musterfont};



static mfont stornofont = {
                       FontName, 
                       FontHeight , 
                       FontWidth , 
                       FontAttribute, 
                       RGB (255, 0, 0), 
                       WHITECOL,
                       0, 0};

static ITEM iStorno ("storno", StornoText, "", 0);

static field _stornof [] = {
&iStorno,          0, 0, 0, 77, 0,  "", DISPLAYONLY,       0, 0, 0,
};

static form stornof = {1, 0, 0, _stornof, 0, 0, 0, 0, &stornofont};



//LAC-150
static mfont ansehenfont = {
                       FontName, 
                       FontHeight * 1.4, 
                       FontWidth * 1.4, 
                       FontAttribute, 
                       RGB (255, 0, 0), 
                       WHITECOL,
                       0, 0};
static ITEM iAnsehen ("ansehen", " nur Ansehen ", "", 0);
static field _ansehenf [] = {
&iAnsehen,          0, 0, 0, 50, 0,  "", DISPLAYONLY,       0, 0, 0,
};
static form ansehenf = {1, 0, 0, _ansehenf, 0, 0, 0, 0, &ansehenfont};




static ITEM iauftrag ("auf", auftrag,     "Auftragsnummer", 0); 
static ITEM ikunfil ("kun_fil",    kunfil,"Kunde/Filiale ", 0); 
static ITEM ikunfil_bz 
                    ("",           "Kunde", "", 0); 
static ITEM iarrdown ("arrdown",  " < ",     "", 0);
static ITEM ishopliste ("shopliste",  "Liste der Shopauftr�ge",     "", 0);
static ITEM ishop
                    ("orders_id", orders_id, "Shop-Bestellnummer.", 0); 

static field _aufformk [] = {
&iauftrag,                 9, 0, 2, 1, 0,  "%8d", EDIT,       0, leseauf, 0,
&iarrdown,                 2, 0, 2,25, 0, "B",    BUTTON,     0, 0, QUERYAUF, 
&ikunfil,                  2, 0, 3, 1, 0, "%1d", READONLY,   0, 0,0,
&ikunfil_bz,              10, 0, 3,19, 0, "",    DISPLAYONLY,0, 0,0,
&ishop,                   40, 0, 2,69, 0, "",    EDIT,      0, lese_shop,0,
&ishopliste,              25, 0, 3,89, 0, "",    BUTTON,     0, 0, QUERYSHOP,   //LAC-74
};

static form aufformk = {6, 0, 0, _aufformk, 0, 0, 0, 0, NULL};

static ITEM ikunstxt   ("kun_txt",  "Kundennummer  ",  "", 0); 
static ITEM ikunlstxt  ("kun_txt",  "Lieferadresse ",  "", 0); 
// static ITEM ikuns      ("kun",      kuns,              "", 0); 
static ITEM ikunliefs  ("kun",      kunliefs,          "", 0); 


static ITEM ikuns      ("kun",      kuns,       "Kundennummer  ", 0); 
// static ITEM ikunliefs  ("kun",      kunliefs,   "Lieferadresse ", 0); 


static ITEM ildat   ("lieferdat",  ldat,  "Lieferdatum   ", 0);
static ITEM ilzeit  ("lieferzeit", lzeit, "Lieferzeit    ", 0);
static ITEM iauf_status
                    ("auf_status", auf_status, "Auf-Status ", 0);
static ITEM iauf_stat_txt
                    ("auf_stat_txt", auf_stat_txt, "", 0);
static ITEM ikd_auftrag
                    ("kd_auftrag", kd_auftrag, "Kd-Auftrag ", 0); 
// static ITEM ik_kurzb("kun_krz",    k_kurzb,    "K.-Kurzb.  ", 0);
static ITEM ik_kurzb("kun_krz",    k_kurzb,    "", 0);
static ITEM itele   ("tele",       _adr.tel,   "TelNr ", 0);
static ITEM iauf_art   ("auf_art", auf_art,    "A.Art ", 0);
static ITEM iauf_art_txt ("auf_art_txt", auf_art_txt,    "", 0);

static ITEM ivertrs ("vertr",      vertrs,     "Vertr-Nr ",   0);
static ITEM ivertr_krz
                    ("vertr_krz",  vertr_krz,  "",   0);   
static ITEM itour   ("tou",        tour,       "Tour-Nr ",    0);
static ITEM itour_krz
                    ("tou_bz",     tour_krz,   "     ",       0);
static ITEM ihtext  ("htext",      htext,  "Hinweistext   ", 0); 



static field _aufform [] = {
//&ikunfil,                    2, 0, 3,1, 0, "%1d", NORMAL,   0, testkeys,0,
// &ikuns,                      9, 0, 4,1, 0, "%8d", NORMAL,   setkey9kun, 
//&ikunstxt,                   0, 0, 4, 1, 0, "", DISPLAYONLY,   0, 0, 0, 
&ikuns,                      9, 0, 4,1, 0, "", NORMAL,   setkey9kun, 
                                                          lesekun, 0,

&iarrdown,                   2, 0, 4,25, 0, "B",    BUTTON,     0, 0, QUERYKUN, 
&ildat,                     11, 0, 6,1, 0, "dd.mm.yyyy", NORMAL,
                                                            saveldat, testldat,0,
&ilzeit,                     6, 0, 7,1, 0, "hh:mm", NORMAL,      0, testkeys,0,
&ihtext,                    49, 0, 8,1, 0, "", NORMAL,      setkey10_11, testhtext,0,
&iauf_status,                2, 0, 2,40, 0, "", READONLY,      0, testkeys,0,
&iauf_stat_txt,             13, 0, 2,55, 0, "", READONLY, 0, 0, 0, 
&ikd_auftrag,               16, 0, 3,40, 0, "", NORMAL,      0, testkeys,0,
//&ik_kurzb,                  16, 0, 4,40, 0, "", READONLY, 0, testkeys,0,
&ik_kurzb,                  16, 0, 4,27, 0, "", READONLY, 0, testkeys,0,
&itele,                     25, 0, 4,45, 0, "", READONLY, 0, 0,0,
&iauf_art,                   3, 0, 4,45, 0, "", REMOVED, setkey9aufart, testaufart,0,
&iauf_art_txt,              16, 0, 4,55, 0, "", REMOVED, 0,  0,0,
&ivertrs,                    5, 0, 6,32, 0, "%4d",NORMAL,    setkey9vertr, testvertr0,0,
&itour,                      9, 0, 6,49, 0, "%8d", NORMAL,   setkey9tou, testtou0,0,
&iarrdown,                   2, 0, 6,67, 0, "B",    BUTTON,     0, 0, QUERYTOU, 
&ivertr_krz,                16, 0, 7,32, 0, "", READONLY,    0, testkeys,0,
&itour_krz,                 16, 0, 7,50, 0, "", READONLY,    0, testkeys,0,


};

static form aufform = {17, 0, 0, _aufform, 0, 0, 0, 0, NULL}; 

void SetItemPicture (form *Form, LPSTR Item, LPCSTR picture)
{
	int pos = GetItemPos (Form, Item);
	if (pos == -1) return;
	Form->mask[pos].picture = (LPSTR) picture;
}


static int kunfield = 1;
static int toufield = 9;

static char *aktart = kunart;
static char *aktzei = a_bz2_on;
static int NewRec = 0;

static int  anzart = 20;
static char AnzBest [10];

static int TestDivAdr = 1;

// Werte f�r Adressbereiche  

static long adr_von  = 90000000;
static long adr_bis  = 99999999;

// Werte f�r gesch�tzte Adressbereiche  

static long sadr_von = 90000000;
static long sadr_bis = 99999999;
static void fillsadr (void);

static int testanzb (void)
{
	if (syskey == KEY5 || syskey == KEY12)
	{
		break_enter ();
	}

	if (atoi (AnzBest) > 100)
	{
		strcpy (AnzBest, "100");
	}
	else if (atoi (AnzBest) < 5)
	{
		strcpy (AnzBest, "5");
	}
	return 0;
}

static ITEM iAnzBestT ("anzbest",  "Anzahl Bestellartikel", "", 0);
static ITEM iAnzBest ("anzbest",   AnzBest, "", 0);

// static ITEM iAnzBest ("anzbest",   AnzBest, "Anzahl Bestellartikel", 0);

static field _fAnzBest [] = {
    &iAnzBestT,  22, 0, 1, 1, 0, "", DISPLAYONLY, 0, 0, 0,
    &iAnzBest,    7, 0, 1,24, 0, "%4d", EDIT | UPDOWN, 0, testanzb, 0,
    &iOK,          12, 0, 3, 5, 0, "",  BUTTON,   0,0, KEY12,
    &iCancel,      12, 0, 3,19, 0, "",  BUTTON,   0,0, KEY5};

static form fAnzBest = {4,0, 0, _fAnzBest, 0, 0, 0, 0, NULL};

static struct UDTAB udtab[] = {100, 5, 10};
static int udanz = 1;

void SetAnzBest (void)
/**
String suchen.
**/
{
        HWND hAnzBest;
        int end_break;
        int currents;

        udtab[0].Pos = anzart;
        currents = currentfield;
        end_break = GetBreakEnd ();
        break_end ();
        sprintf (AnzBest, "%d", anzart);
        EnableWindows (mamain1, FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_DLGFRAME);
        hAnzBest = OpenWindowChC (5,36,10, 20, hMainInst,
                   "");
        SetUpDownTab (udtab , udanz);
        enter_form (hAnzBest, &fAnzBest, 0, 0);
		if (syskey != KEY5)
		{
                 anzart = atoi (AnzBest);
		}
        EnableWindows (mamain1, TRUE);
        DestroyWindow (hAnzBest);
        SetBreakEnd (end_break);
        SetAktivWindow (mamain1);
        SetActiveWindow (mamain1);
        currentfield = currents;
        SetCurrentFocus (currentfield);
}


int CallTele1 (void)
/**
Waehlen ausfuehren.
**/
{
/*
        clipped (_adr.fax);
        if (strcmp (_adr.fax, " ") <= 0)
        {
                  disp_mess ("Keine Fax-Nummer", 0);
        }
        else
        {
                  print_mess (0, "Fax Nummer %s", _adr.fax);
        }
*/
        if (atol (kuns) != 0l)
        {
                  FaxAuf ();
                  DestroyWindow (QuikInfo);
                  CreateQuikInfos ();
        }
        return 0;
}

int CallTele2 (void)
/**
Waehlen ausfuehren.
**/
{
        clipped (_adr.tel);


		if (temode == 0)
		{
			if (KunTour != NULL && TeleTour)
			{
				  LPSTR lpCmdLine = "";
                  int (WINAPI *StartDlg )(LPSTR params);
                  StartDlg = (int (WINAPI *) (char *)) GetProcAddress (KunTour, "StartDlg");
                  long (WINAPI *GetTeleKun )();
                  GetTeleKun = (long (WINAPI *) ()) GetProcAddress (KunTour, "GetTeleKun");
			      sprintf (lpCmdLine, "Transaction=ON");
 				  int ret = (*StartDlg )(lpCmdLine);
				  kun.kun = 0l;
				  if (ret != 0)
				  {
					  kun.kun = (*GetTeleKun) ();
					  currentfield = 0;
					  SetFocus (aufformk.mask[0].feldid);
					  if (kun.kun != 0l)
					  {
						    telekun = TRUE;
//							KunFromTeleList = TRUE;
							sprintf (kuns, "%ld", kun.kun);
// Achtung !! die n�chsten beiden Zeilen zum Test 

							leseauf ();
							lesekun0 ();

						    EnterKommissionierer (TRUE);
//							PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
					  }
					  else
					  {
						 int pos = GetItemPos (&aufformk, "auf");
						 SetCurrentField (pos);
						 SetFocus (aufformk.mask[pos].feldid);
					  }
				  }
				  else
				  {
					  int pos = GetItemPos (&aufformk, "auf");
					  SetCurrentField (pos);
					  SetFocus (aufformk.mask[pos].feldid);
				  }
				  commitwork ();
				  beginwork ();
				  return ret;
			}
			else
			{
                  TelefonList ();
			}
		}
        else if (strcmp (_adr.tel, " ") <= 0)
        {
                  disp_mess ("Keine Telefon-Nummer", 0);
        }
        else
        {
                  if (ClipDial != NULL)
                  {
                      RunClipDial (_adr.tel);
                  }
                  else if (DdeDialProg != NULL)
                  {
                      RunDdeDial (_adr.tel);
                  }
                  else
                  {
                      print_mess (0, "Telefon Nummer %s", _adr.tel);
                  }
        }
        CreateQuikInfos ();
        return 0;
}

int CallTele3 (void)
/**
Waehlen ausfuehren.
**/
{
        if (atol (kuns) != 0l)
        {
			 if (DisplayFreeText2 && clipped (kun.frei_txt2) > " ")
			 {
				Kinfo.InfoKunFText (hMainInst, hMainWindow, aufk.mdn, atol (kuns), kun.kun_bran2);
			 }
			 else
			 {
		        Kinfo.InfoKunF (hMainInst, hMainWindow, aufk.mdn, atol (kuns), kun.kun_bran2);
			 }
             CreateQuikInfos ();
        }
        return 0;
}

void SaveRect_ext (void) //LAC-52
{
	    char *etc;
		char rectname [512];
		RECT rect;
		FILE *fp;
		int xfull, yfull;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

          xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
          yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
		sprintf (rectname, "%s\\%s.rct", etc,Programm);
		GetWindowRect (hMainWindow, &rect);


		sprintf (rectname, "%s\\%s.rct", etc,"51100Lack_ext");
		GetWindowRect (hMainWindow, &rect);
		//testtest   Einstellung bei maximalgr��e soll nicht �bernommen werden 
		if (rect.right < xfull)  
		{
				fp = fopen (rectname, "w");
				fprintf (fp, "left    %d\n", rect.left);
				fprintf (fp, "top     %d\n", rect.top);
				fprintf (fp, "right   %d\n", rect.right - rect.left);
				fprintf (fp, "bottom  %d\n", rect.bottom - rect.top);
				fclose (fp);
				 disp_mess ("Position wurde f�r WGChart fixiert", 2);

		}
}


void SchreibeDateiShopauf (void) //LAC-74
{
	    char *etc;
		char rectname [512];
		char lieferdat [13];
		FILE *fp;

		if (mench > 0)  return ; //LAC-150
		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		dlong_to_asc (aufk.lieferdat, lieferdat);
		sprintf (rectname, "%s\\%s.ll", etc,"shopauf");

		fp = fopen (rectname, "w");
		fprintf (fp, "NAME SHOPAUF\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "MITRANGE 1\n");
		fprintf (fp, "auf  %ld    %ld\n", aufk.auf, aufk.auf);
		fprintf (fp, "kun  %ld    %ld\n", aufk.kun, aufk.kun);
		fprintf (fp, "lieferdat  %s    %s\n", lieferdat, lieferdat);
		fclose (fp);
}


void ListeShop (void) //LAC-74
{
    char buffer [512];
    char *etc;
	char rectname [512];
	SchreibeDateiShopauf ();
        
	etc = getenv ("BWSETC");
	if (etc == NULL) return;
	sprintf (rectname, "%s\\%s.ll", etc,"shopauf");
    sprintf (buffer, "dr70001 -name shopauf -DATEI %s",rectname);
    ProcExec (buffer, SW_SHOWNORMAL, -1, 0, -1, 0);


}



void SaveRect_ext_paletten (void) //C-52
{
	    char *etc;
		char rectname [512];
		RECT rect;
		FILE *fp;
		int xfull, yfull;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

          xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
          yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
		sprintf (rectname, "%s\\%s.rct", etc,Programm);
		GetWindowRect (hMainWindow, &rect);


		sprintf (rectname, "%s\\%s.rct", etc,"51100Lack_ext2");
		GetWindowRect (hMainWindow, &rect);
		//testtest   Einstellung bei maximalgr��e soll nicht �bernommen werden 
		if (rect.right < xfull)  
		{
				fp = fopen (rectname, "w");
				fprintf (fp, "left    %d\n", rect.left);
				fprintf (fp, "top     %d\n", rect.top);
				fprintf (fp, "right   %d\n", rect.right - rect.left);
				fprintf (fp, "bottom  %d\n", rect.bottom - rect.top);
				fclose (fp);
				 disp_mess ("Position wurde f�r PalettenChart fixiert", 2);
		}
}

void SaveRect_bmp (void) //C-52
{
	    char *etc;
		char rectname [512];
		RECT rect;
		FILE *fp;
		int xfull, yfull;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

          xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
          yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
		GetWindowRect (hMainWindow, &rect);


		sprintf (rectname, "%s\\%s.rct", etc,"51100Lack_bmp");
		GetWindowRect (hMainWindow, &rect);
		if (rect.right < xfull)  
		{
				fp = fopen (rectname, "w");
				fprintf (fp, "left    %d\n", rect.left);
				fprintf (fp, "top     %d\n", rect.top);
				fprintf (fp, "right   %d\n", rect.right - rect.left);
				fprintf (fp, "bottom  %d\n", rect.bottom - rect.top);
				fclose (fp);
				 disp_mess ("Position wurde f�r Artikelbild fixiert", 2);

		}
}


void SaveSpalten (void) //LAC-??
{
	    char *etc;
		char rectname [512];
		FILE *fp;
 
		etc = getenv ("BWSETC"); 
		if (etc == NULL) return;

		sprintf (rectname, "%s\\%s.rct", etc,"51100Lack_Spalten");
		fp = fopen (rectname, "w");

		fprintf (fp, "AktAuf  Spalte_grp_min_best_gewaehlt  %d\n", aufpListe.Spalte_grp_min_best_gewaehlt[0]);    //LAC-104
		fprintf (fp, "AktAuf  Spalte_bestellvorschlag_gewaehlt  %d\n", aufpListe.Spalte_bestellvorschlag_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_last_lieferung_gewaehlt    %d\n", aufpListe.Spalte_last_lieferung_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_last_lief_me_gewaehlt      %d\n", aufpListe.Spalte_last_lief_me_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_last_me_gewaehlt           %d\n", aufpListe.Spalte_last_me_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_lieferdat_gewaehlt         %d\n", aufpListe.Spalte_lieferdat_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_lpr_vk_gewaehlt            %d\n", aufpListe.Spalte_lpr_vk_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_stk_karton_gewaehlt    %d\n", aufpListe.Spalte_stk_karton_gewaehlt[0]); //LAC-118
		fprintf (fp, "AktAuf  Spalte_gew_karton_gewaehlt    %d\n", aufpListe.Spalte_gew_karton_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_bsd_gewaehlt    %d\n", aufpListe.Spalte_bsd_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_lad_pr_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_lad_pr_prc_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_prc_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_lad_pr0_gewaehlt    %d\n", aufpListe.Spalte_lad_pr0_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_marge_gewaehlt    %d\n", aufpListe.Spalte_marge_gewaehlt[0]);
		fprintf (fp, "AktAuf  Spalte_mhd_gewaehlt    %d\n", aufpListe.Spalte_mhd_gewaehlt[0]);

		fprintf (fp, "Bestellvorschlag  Spalte_grp_min_best_gewaehlt  %d\n", aufpListe.Spalte_grp_min_best_gewaehlt[1]); //LAC-104
		fprintf (fp, "Bestellvorschlag  Spalte_bestellvorschlag_gewaehlt  %d\n", aufpListe.Spalte_bestellvorschlag_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_last_lieferung_gewaehlt    %d\n", aufpListe.Spalte_last_lieferung_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_last_lief_me_gewaehlt      %d\n", aufpListe.Spalte_last_lief_me_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_last_me_gewaehlt           %d\n", aufpListe.Spalte_last_me_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_lieferdat_gewaehlt         %d\n", aufpListe.Spalte_lieferdat_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_lpr_vk_gewaehlt            %d\n", aufpListe.Spalte_lpr_vk_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_stk_karton_gewaehlt    %d\n", aufpListe.Spalte_stk_karton_gewaehlt[1]); //LAC-118
		fprintf (fp, "Bestellvorschlag  Spalte_gew_karton_gewaehlt    %d\n", aufpListe.Spalte_gew_karton_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_bsd_gewaehlt    %d\n", aufpListe.Spalte_bsd_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_lad_pr_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_lad_pr_prc_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_prc_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_lad_pr0_gewaehlt    %d\n", aufpListe.Spalte_lad_pr0_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_marge_gewaehlt    %d\n", aufpListe.Spalte_marge_gewaehlt[1]);
		fprintf (fp, "Bestellvorschlag  Spalte_mhd_gewaehlt    %d\n", aufpListe.Spalte_mhd_gewaehlt[1]);

		fprintf (fp, "LetzteBestellung  Spalte_grp_min_best_gewaehlt  %d\n", aufpListe.Spalte_grp_min_best_gewaehlt[2]); //LAC-104
		fprintf (fp, "LetzteBestellung  Spalte_bestellvorschlag_gewaehlt  %d\n", aufpListe.Spalte_bestellvorschlag_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_last_lieferung_gewaehlt    %d\n", aufpListe.Spalte_last_lieferung_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_last_lief_me_gewaehlt      %d\n", aufpListe.Spalte_last_lief_me_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_last_me_gewaehlt           %d\n", aufpListe.Spalte_last_me_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_lieferdat_gewaehlt         %d\n", aufpListe.Spalte_lieferdat_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_lpr_vk_gewaehlt            %d\n", aufpListe.Spalte_lpr_vk_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_stk_karton_gewaehlt    %d\n", aufpListe.Spalte_stk_karton_gewaehlt[2]); //LAC-118
		fprintf (fp, "LetzteBestellung  Spalte_gew_karton_gewaehlt    %d\n", aufpListe.Spalte_gew_karton_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_bsd_gewaehlt    %d\n", aufpListe.Spalte_bsd_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_lad_pr_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_lad_pr_prc_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_prc_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_lad_pr0_gewaehlt    %d\n", aufpListe.Spalte_lad_pr0_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_marge_gewaehlt    %d\n", aufpListe.Spalte_marge_gewaehlt[2]);
		fprintf (fp, "LetzteBestellung  Spalte_mhd_gewaehlt    %d\n", aufpListe.Spalte_mhd_gewaehlt[2]);

		fprintf (fp, "HWG  Spalte_grp_min_best_gewaehlt  %d\n", aufpListe.Spalte_grp_min_best_gewaehlt[3]); //LAC-104
		fprintf (fp, "HWG  Spalte_bestellvorschlag_gewaehlt  %d\n", aufpListe.Spalte_bestellvorschlag_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_last_lieferung_gewaehlt    %d\n", aufpListe.Spalte_last_lieferung_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_last_lief_me_gewaehlt      %d\n", aufpListe.Spalte_last_lief_me_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_last_me_gewaehlt           %d\n", aufpListe.Spalte_last_me_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_lieferdat_gewaehlt         %d\n", aufpListe.Spalte_lieferdat_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_lpr_vk_gewaehlt            %d\n", aufpListe.Spalte_lpr_vk_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_stk_karton_gewaehlt    %d\n", aufpListe.Spalte_stk_karton_gewaehlt[3]); //LAC-118
		fprintf (fp, "HWG  Spalte_gew_karton_gewaehlt    %d\n", aufpListe.Spalte_gew_karton_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_bsd_gewaehlt    %d\n", aufpListe.Spalte_bsd_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_lad_pr_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_lad_pr_prc_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_prc_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_lad_pr0_gewaehlt    %d\n", aufpListe.Spalte_lad_pr0_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_marge_gewaehlt    %d\n", aufpListe.Spalte_marge_gewaehlt[3]);
		fprintf (fp, "HWG  Spalte_mhd_gewaehlt    %d\n", aufpListe.Spalte_mhd_gewaehlt[3]);

		fprintf (fp, "KritischeMHD  Spalte_grp_min_best_gewaehlt  %d\n", aufpListe.Spalte_grp_min_best_gewaehlt[4]); //LAC-84
		fprintf (fp, "KritischeMHD   Spalte_bestellvorschlag_gewaehlt  %d\n", aufpListe.Spalte_bestellvorschlag_gewaehlt[4]);
		fprintf (fp, "KritischeMHD   Spalte_last_lieferung_gewaehlt    %d\n", aufpListe.Spalte_last_lieferung_gewaehlt[4]);
		fprintf (fp, "KritischeMHD   Spalte_last_lief_me_gewaehlt      %d\n", aufpListe.Spalte_last_lief_me_gewaehlt[4]);
		fprintf (fp, "KritischeMHD   Spalte_last_me_gewaehlt           %d\n", aufpListe.Spalte_last_me_gewaehlt[4]);
		fprintf (fp, "KritischeMHD   Spalte_lieferdat_gewaehlt         %d\n", aufpListe.Spalte_lieferdat_gewaehlt[4]);
		fprintf (fp, "KritischeMHD   Spalte_lpr_vk_gewaehlt            %d\n", aufpListe.Spalte_lpr_vk_gewaehlt[4]);
		fprintf (fp, "KritischeMHD  Spalte_stk_karton_gewaehlt    %d\n", aufpListe.Spalte_stk_karton_gewaehlt[4]); //LAC-118
		fprintf (fp, "KritischeMHD  Spalte_gew_karton_gewaehlt    %d\n", aufpListe.Spalte_gew_karton_gewaehlt[4]);
		fprintf (fp, "KritischeMHD  Spalte_bsd_gewaehlt    %d\n", aufpListe.Spalte_bsd_gewaehlt[4]);
		fprintf (fp, "KritischeMHD  Spalte_lad_pr_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_gewaehlt[4]);
		fprintf (fp, "KritischeMHD  Spalte_lad_pr_prc_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_prc_gewaehlt[4]);
		fprintf (fp, "KritischeMHD  Spalte_lad_pr0_gewaehlt    %d\n", aufpListe.Spalte_lad_pr0_gewaehlt[4]);
		fprintf (fp, "KritischeMHD  Spalte_marge_gewaehlt    %d\n", aufpListe.Spalte_marge_gewaehlt[4]);
		fprintf (fp, "KritischeMHD  Spalte_mhd_gewaehlt    %d\n", aufpListe.Spalte_mhd_gewaehlt[4]);

		fprintf (fp, "Lieferung   Spalte_grp_min_best_gewaehlt  %d\n", aufpListe.Spalte_grp_min_best_gewaehlt[5]); //LAC-84
		fprintf (fp, "Lieferung    Spalte_bestellvorschlag_gewaehlt  %d\n", aufpListe.Spalte_bestellvorschlag_gewaehlt[5]);
		fprintf (fp, "Lieferung    Spalte_last_lieferung_gewaehlt    %d\n", aufpListe.Spalte_last_lieferung_gewaehlt[5]);
		fprintf (fp, "Lieferung    Spalte_last_lief_me_gewaehlt      %d\n", aufpListe.Spalte_last_lief_me_gewaehlt[5]);
		fprintf (fp, "Lieferung    Spalte_last_me_gewaehlt           %d\n", aufpListe.Spalte_last_me_gewaehlt[5]);
		fprintf (fp, "Lieferung    Spalte_lieferdat_gewaehlt         %d\n", aufpListe.Spalte_lieferdat_gewaehlt[5]);
		fprintf (fp, "Lieferung    Spalte_lpr_vk_gewaehlt            %d\n", aufpListe.Spalte_lpr_vk_gewaehlt[5]);
		fprintf (fp, "Lieferung  Spalte_stk_karton_gewaehlt    %d\n", aufpListe.Spalte_stk_karton_gewaehlt[5]); //LAC-118
		fprintf (fp, "Lieferung  Spalte_gew_karton_gewaehlt    %d\n", aufpListe.Spalte_gew_karton_gewaehlt[5]);
		fprintf (fp, "Lieferung  Spalte_bsd_gewaehlt    %d\n", aufpListe.Spalte_bsd_gewaehlt[5]);
		fprintf (fp, "Lieferung  Spalte_lad_pr_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_gewaehlt[5]);
		fprintf (fp, "Lieferung  Spalte_lad_pr_prc_gewaehlt    %d\n", aufpListe.Spalte_lad_pr_prc_gewaehlt[5]);
		fprintf (fp, "Lieferung  Spalte_lad_pr0_gewaehlt    %d\n", aufpListe.Spalte_lad_pr0_gewaehlt[5]);
		fprintf (fp, "Lieferung  Spalte_marge_gewaehlt    %d\n", aufpListe.Spalte_marge_gewaehlt[5]);
		fprintf (fp, "Lieferung  Spalte_mhd_gewaehlt    %d\n", aufpListe.Spalte_mhd_gewaehlt[5]);

		fclose (fp);
}


int InfoVersion (void)
/**
Waehlen ausfuehren.
**/
{
	    Vinfo.VInfoF (hMainInst, hMainWindow, Version);

        return 0;
}

void CreateQuikInfos (void)
/**
QuikInfos generieren.
**/
{
     BOOL bSuccess ;
     TOOLINFO ti ;

     if (QuikInfo) DestroyWindow (QuikInfo);
     QuikInfo = CreateWindow (TOOLTIPS_CLASS,
                                "",
                                WS_POPUP,
                                0, 0,
                                0, 0,
                                hMainWindow,
                                NULL,
                                hMainInst,
                                NULL);


     ZeroMemory (&ti, sizeof (TOOLINFO)) ;
     ti.cbSize = sizeof (TOOLINFO) ;
     ti.uFlags = TTF_IDISHWND | TTF_CENTERTIP | TTF_SUBCLASS ;
     ti.hwnd   = mamain1 ;
     ti.uId    = (UINT) (HWND) buform.mask[0].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     ti.uId    = (UINT) (HWND) buform.mask[1].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     ti.uId    = (UINT) (HWND) buform.mask[2].feldid;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
     SetFktQuikInfo (QuikInfo, &ti);
}


int InfoKun (void)
{
        char where [80];

        if (atoi (kunfil) == 0)
        {
              if (atol (kuns))
              {
                        sprintf (where, "where kun = %ld", atol (kuns));
              }
              else
              {
                        sprintf (where, "where kun > 0");
              }
              _CallInfoEx (hMainWindow, NULL,
							      "kun", 
								  where, 0l);
        }
        else
        {
              if (atol (kuns))
              {
                        sprintf (where, "where mdn = %hd and fil = %ld", aufk.mdn, atol (kuns));
              }
              else
              {
                        sprintf (where, "where mdn = %hd and fil > 0", aufk.mdn);
              }
              _CallInfoEx (hMainWindow, NULL,
							      "fil", 
								  where, 0l);
        }
        return 0;
}

int InfoTou (void)
{
        char where [80];
        long tou;

        if (atol (tour))
        {
            tou = atol (tour) / tour_div;
            sprintf (where, "where tou = %ld", tou);
        }
        else
        {
            sprintf (where, "where tou > 0");
        }
        _CallInfoEx (hMainWindow, NULL,
							      "tou", 
								  where, 0l);
        return 0;
}

int InfoVertr (void)
{
        char where [80];
        long vertr;

        if (atol (vertrs))
        {
            vertr= atol (vertrs);
            sprintf (where, "where vertr = %ld", vertr);
        }
        else
        {
            sprintf (where, "where vertr > 0");
        }
        _CallInfoEx (hMainWindow, NULL,
							      "vertr", 
								  where, 0l);
        return 0;
}


BOOL Lock_Aufk (void)
/**
Auftrag sperren.
**/
{
    short sqlm;
    extern short sql_mode;

    sqlm = sql_mode;
    sql_mode = 1;
	
	if (mench == 1) return TRUE; //LAC-150

	aufk.delstatus = -1;

/*
	DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
	DbClass.sqlin ((short *) &aufk.fil, 1, 0);
	DbClass.sqlin ((long *) &aufk.auf, 2, 0);
	dsqlstatus = DbClass.sqlcomm ("update aufk set delstatus = -1 where mdn = ? "
		                          "and fil = ? and auf = ?");
*/
	dsqlstatus = ls_class.update_aufk (aufk.mdn, aufk.fil, aufk.auf);
/*
	if (dsqlstatus == 0)
	{
        dsqlstatus = ls_class.lock_aufk (aufk.mdn, aufk.fil, aufk.auf);
	}
	if (dsqlstatus == 100)
	{
		ls_class.update_aufk (aufk.mdn, aufk.fil, aufk.auf);
        dsqlstatus = ls_class.lock_aufk (aufk.mdn, aufk.fil, aufk.auf);
	}
*/

	aufk.delstatus = 0;
    if (dsqlstatus < 0)
    {
	    print_mess (2, "Auftrag %ld wird im Moment bearbeitet", aufk.auf);
        sql_mode = sqlm;
        return FALSE;
    }
    sql_mode = sqlm;
    return TRUE;
}


void GetAufstatTxt (char *aufstxt)
/**
Klartext fuer Auftragsstatus holen.
**/
{
	  char wert [5];

	  sprintf (wert, "%hd", aufk.auf_stat);
	  ptabn.ptbez[0] = 0;
      ptab_class.lese_ptab ("auf_status", wert);
	  strcpy (aufstxt, ptabn.ptbez);
}

static void SetKunAttr (void)
/**
Atribut auf Feld Kunde setzen.
**/
{
	   int kunarrowpos;

	   if (kun_sperr == FALSE) return;

	   kunarrowpos = GetItemPos (&aufform, "kun") + 1;
       if (atol (kuns) == 0l)
	   {
	            SetItemAttr (&aufform, "kun", EDIT);
				aufform.mask[kunarrowpos].attribut = BUTTON;
	   }
	   else if (EnableKunDirect == FALSE && aufpListe.GetPosanz () == 0)
	   {
	            SetItemAttr (&aufform, "kun", EDIT);
				aufform.mask[kunarrowpos].attribut = BUTTON;
	   }
	   else if (KunFromTeleList)
	   {
	            SetItemAttr (&aufform, "kun", EDIT);
				aufform.mask[kunarrowpos].attribut = BUTTON;
	   }
	   else
	   {
	            SetItemAttr (&aufform, "kun", READONLY);
				aufform.mask[kunarrowpos].attribut = REMOVED;
	   }
       KunFromTeleList = FALSE;
}


int lese_kun_mdn (long kun)
/**
Kunde ohne Mandant lesen. Mandant wird vom Kunden geholt.
**/
{
	   int dsqlstatus;
	   DbClass.sqlin ((long *) &kun, 2, 0);
	   DbClass.sqlout ((char *) mdn, 0, 5);
	   DbClass.sqlout ((char *) fil, 0, 5);
       dsqlstatus = DbClass.sqlcomm ("select mdn,fil from kun where kun = ?");
	   if (dsqlstatus == 0)
	   {
		   aufk.mdn = atoi (mdn);
		   aufk.fil = atoi (fil);
 	       DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
	       DbClass.sqlin ((short *) &aufk.fil, 1, 0);
	       DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
	       DbClass.sqlcomm ("update aufk set mdn = ?, fil = ? where auf = ?");
		   mdn_class.lese_mdn (aufk.mdn);
	   }
	   return dsqlstatus;
}

int lese_fil_mdn (long kun)
/**
Kunde ohne Mandant lesen. Mandant wird vom Kunden geholt.
**/
{
	   int dsqlstatus;
	   short nfil;

	   DbClass.sqlin ((long *) &kun, 2, 0);
	   DbClass.sqlout ((char *) mdn, 0, 5);
	   DbClass.sqlout ((char *) kuns, 0, 9);
       dsqlstatus = DbClass.sqlcomm ("select mdn,fil from fil where kun = ?");

	   if (dsqlstatus == 100)
	   {
		        nfil = (short) kun;
 	            DbClass.sqlin ((long *) &nfil, 1, 0);
	            DbClass.sqlout ((char *) mdn, 0, 5);
	            DbClass.sqlout ((char *) kuns, 0, 9);
                dsqlstatus = DbClass.sqlcomm ("select mdn,fil from fil where fil = ?");
	   }


	   if (dsqlstatus == 0)
	   {
		   aufk.mdn = atoi (mdn);
		   aufk.fil = 0;
		   sprintf (fil, "%hd", 0);
		   aufk.kun = atol (kuns);
 		   sprintf (kuns, "%8ld", atol (kuns));
	       DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
	       DbClass.sqlin ((short *) &aufk.fil, 1, 0);
	       DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
	       DbClass.sqlcomm ("update aufk set mdn = ?, fil = ? where auf = ?");
	   }

	   return dsqlstatus;
}


static void EnableKun (BOOL b)
{

  	    int kunpos = GetItemPos (&aufform, "kun");

        CloseControl (&aufform, kunpos);
        CloseControl (&aufform, kunpos + 1);
        if (b == FALSE)
        {
	            SetItemAttr (&aufform, "kun", READONLY);
				aufform.mask[kunpos + 1].attribut = REMOVED;
                display_field (mamain1, &aufform.mask[kunpos], 0, 0);
        }
        else
        {
	            SetItemAttr (&aufform, "kun", EDIT);
				aufform.mask[kunpos + 1].attribut = BUTTON;
                display_field (mamain1, &aufform.mask[kunpos], 0, 0);
                display_field (mamain1, &aufform.mask[kunpos + 1], 0, 0);
        }
}
 
	       
static void AufToForm (void)
/**
Auftragsdaten in Form uebertragen.
**/
{
          sprintf (kunfil, "%1d", aufk.kun_fil);
          if (atoi (kunfil) == 0)
          {
                    ikunfil_bz.SetFeldPtr ("Kunde");
          }
          else
          {
                    ikunfil_bz.SetFeldPtr ("Filiale");
          }
          display_field (mamain1, &aufformk.mask[1], 0, 0);
          display_field (mamain1, &aufformk.mask[2], 0, 0);


          sprintf (kuns,   "%8ld", aufk.kun);
          if (aufk.kun_fil == 0)
          {
		          dsqlstatus = 0;
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_kun_mdn (atol (kuns));
				  }
				  if (dsqlstatus == 0)
				  {
/*
                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                    atoi (fil),
                                                    atol (kuns));
*/

// Kunde immer mit Filiale 0 lesen

                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                         0,
                                                         atol (kuns));
				  }
				  if (dsqlstatus == 100 && autokunfil)
				  {
					   aufk.kun_fil = 1;
					   strcpy (kunfil, "1");
                       ikunfil_bz.SetFeldPtr ("Filiale");
                       display_form (mamain1, &aufformk, 0, 0);
				  }
				  else
				  {
                       if (dsqlstatus == 0)
					   {
                         if (KunLiefKz && LiefAdr > 0l)
                         {
                                 kun.adr2 = LiefAdr;
                         }
                         dsqlstatus = adr_class.lese_adr (kun.adr2);
						 aufk.waehrung = kun.waehrung;
					   }
                       strcpy (k_kurzb, kun.kun_krz2);
				       if ((aufk.kopf_txt == 0) && use_kopf_txt && kun.ls_kopf_txt)
					   {
					         aufk.kopf_txt = kun.ls_kopf_txt;
					   }
				   }

          }
          if (aufk.kun_fil == 1)
          {
		          dsqlstatus = 0;
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_fil_mdn (atol (kuns));
				  }
				  if (dsqlstatus == 0)
				  {
                       dsqlstatus = fil_class.lese_fil (atoi (mdn),
                                                    atoi (kuns));
                       if (dsqlstatus == 0)
					   {
                                  strcpy (k_kurzb, _adr.adr_krz);
					              aufk.waehrung = atoi (_mdn.waehr_prim);
					   }
				  }
          }
          clipped (_adr.tel);
//		  strcpy (tele, _adr.tel);

          if (strcmp (_adr.tel, " ") > 0)
          {
                   CTelefon.bmp = btelefon;
                   ActivateColButton (mamain1, 
                                      &buform, 1, 0, 1);
                   EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
          }

          clipped (_adr.fax);
          if (strcmp (_adr.fax, " ") > 0)
          {
                   ActivateColButton (mamain1, 
                                      &buform,      0, 0, 1);
                   EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
          }
          ActivateColButton (mamain1, &buform, 2, 0, 1);
          EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
          MoveButtons ();


          dlong_to_asc (aufk.lieferdat, ldat);
          dlong_to_asc (aufk.komm_dat, kdat);
          dlong_to_asc (aufk.fix_dat, fix_dat);
          strcpy (lzeit, aufk.lieferzeit);
          sprintf (auf_status, "%1d", aufk.auf_stat);
		  GetAufstatTxt (auf_stat_txt);
          strcpy (kd_auftrag, aufk.auf_ext);
          if (aufk.vertr < 0l)
          {
              aufk.vertr = 0l;
          }
          sprintf (vertrs, "%8ld", aufk.vertr);
          dsqlstatus = vertr_class.lese_vertr (atoi (mdn),
                                               atoi (fil),
                                               aufk.vertr);
          if (dsqlstatus == 0)
          {
                         strcpy (vertr_krz, _adr.adr_krz);
          }
          else
          {
                         strcpy (vertr_krz, " ");
          }
          if (aufk.tou < 0l)
          {
              aufk.tou = 0l;
          }
          sprintf (tour, "%8ld", aufk.tou);
          lesetou ();
          strcpy (htext, aufk.hinweis);
          sprintf (auf_art, "%hd", aufk.auf_art);
		  if (auf_art_active)
		  {
			  getaufart ();
		  }
          if (aufk.auf_art == 99)
          {
              SetMuster (TRUE);
          }
          if (aufk.auf_stat < 0) //LAC-210
          {
              SetStorno (TRUE);
          }
}

static long GetSysDateLong ()
{
	      char datum [12];
        
           sysdate (datum);
		   return dasc_to_long (datum);
}

static double TimeToDec (LPSTR timestr, double max)
{
	double dec_time = 0.0;


	Text Time = timestr;
	int pos;
	if ((pos = Time.Find (":")) != -1)
	{
		Text Hour = Time.SubString (0, pos);
		Text Min  = Time.SubString (pos + 1, Time.GetLen () - (pos + 1));
		dec_time = ratod (Hour.GetBuffer ());
		double min_dec = ratod (Min.GetBuffer ());
//		min_dec /= 60;
		min_dec /= 100;
		dec_time += min_dec;
	}
	else
	{
		dec_time = ratod (timestr);
	}
	if ((dec_time > max) || (dec_time < (max * -1)))
	{
		dec_time = 0.0;
	}
	return dec_time;
}


static void FormToAuf (short auf_stat)
/**
Form in Auftragsdaten uebertragen.
**/
{
	      char datum [12];
		  long sysldat;
 
          sysldat = GetSysDateLong ();
		  sysdate (datum);

          aufk.mdn     = atoi (mdn);
          aufk.fil     = atoi (fil);
          aufk. auf    = atol (auftrag);
          aufk.kun_fil = atoi (kunfil);
          aufk.kun = atol (kuns);
          strcpy (aufk.kun_krz1,k_kurzb);
          aufk.lieferdat = dasc_to_long (ldat); 
          aufk.komm_dat  = dasc_to_long (kdat); 
          aufk.fix_dat  = dasc_to_long (fix_dat); 
		  if (aufk.fix_dat < aufk.lieferdat)
		  {
			  aufk.fix_dat = aufk.lieferdat;
		  }
//          aufk.best_dat  = dasc_to_long (datum); 
//          aufk.best_dat  = sysldat; 
          strcpy (aufk.lieferzeit, lzeit);
//          aufk.zeit_dec = ratod (lzeit);
          aufk.zeit_dec = TimeToDec (lzeit, 999.99);
		  if (aufk.zeit_dec > 999.99 ||
			  aufk.zeit_dec < -999.99)
		  {
			  aufk.zeit_dec = 0.0;
		  }
          aufk.auf_stat = atoi (auf_status);
          if (aufk.auf_stat < auf_stat) aufk.auf_stat = auf_stat;
          strcpy (aufk.auf_ext, kd_auftrag);
          aufk.vertr = atol (vertrs);
          aufk.tou = atol (tour);
		  aufk.tou_nr = aufk.tou / (long) tour_div;
          strcpy (aufk.hinweis, htext);
/*
          if (aufk.auf_stat == 0)
          {
              aufk.auf_stat = 1;
          }
*/
          if (UpdatePersNam || strcmp (clipped (aufk.pers_nam), " ") <= 0)
          {
		         strcpy (aufk.pers_nam, sys_ben.pers_nam);
          }
		  if (strcmp (aufk.pers_nam, " ") <= 0)
		  {
//			  strcpy (aufk.pers_nam, "leer");
		  }
          aufk.auf_art = atoi (auf_art);
          if (aufpListe.GetMuster ())
          {
              aufk.auf_art = 99;
          }
          if (aufpListe.GetStorno ())
          {
              if (aufk.auf_stat > 0) aufk.auf_stat *= -1;  //LAC-210
          }
}

void SetLieferdat (void)
/**
Defaultwert fuer Lieferdatum setzen.
**/
{
         char datum [12];

		 if (strcmp (ldat, "          ") <= 0 || autosysdat) 
		 {
                    sysdate (datum);
					IncDatum (datum);
                    strcpy (ldat, datum);
                    strcpy (kdat, datum);
                    strcpy (fix_dat, datum);
		 }
}


static void InitAuf (void)
/**
aufform Initialisieren.
**/
{
//          sprintf (kunfil, "%1d", 0);
	      if (telekun == FALSE)
		  {
			  if (StartKun == 0)  //LAC-184
			  {
                sprintf (kuns,"%8ld", 0l);
			  }
		  }
		  else
		  {
			    telekun = FALSE;
		  }
          if (StartKun == 0) strcpy (k_kurzb, " ");
          strcpy (_adr.tel, " ");
          strcpy (_adr.fax,  " ");
//          strcpy (ldat, " ");
          strcpy (kdat, " ");
          strcpy (fix_dat, " ");
          SetLieferdat ();
          strcpy (lzeit, " ");
          sprintf (auf_status, "%1d", 0);
          strcpy (kd_auftrag, " ");
          sprintf (vertrs, "%8ld", 0l);
          strcpy (vertr_krz, " ");
          sprintf (tour, "%8ld", 0l);
          strcpy (tour_krz, " ");
          strcpy (htext, " ");
          aufk.kopf_txt = 0l;
          aufk.fuss_txt = 0l;
		  if (auf_art_active)
		  {
			  strcpy (auf_art, "1");
			  getaufart ();
		  }
}

void ReadPr (double a)
/**
Artikelpreis holen.
**/
{
       int dsqlstatus;
       short sa;
       double pr_ek;
       double pr_vk;
       
       dlong_to_asc (aufk.lieferdat, ldat);
/*
       dsqlstatus = WaPreis.preise_holen (aufk.mdn,
                                          aufk.fil,
                                          aufk.kun_fil,
                                          aufk.kun,
                                           ratod (aufps.a),
                                          lieferdat,
                                          &sa, 
                                          &pr_ek,
                                          &pr_vk);
*/

// Preise immer mit Filiale 0 lesen.

	   WaPreis.SetAufArt (atoi (auf_art));

       if (aufpListe.DllPreise.PriceLib != NULL && 
		   aufpListe.DllPreise.preise_holen != NULL)
	   {
				  dsqlstatus = (aufpListe.DllPreise.preise_holen) (atoi (mdn), 
                                          0,
                                          atoi (kunfil),
                                          atol (kuns),
                                          a,
                                          ldat,
                                          &sa, 
                                          &pr_ek,
                                          &pr_vk);
	   }
	   else
	   {
				dsqlstatus = WaPreis.preise_holen (atoi (mdn),
                                          0,
                                          atoi (kunfil),
                                          atol (kuns),
                                          a,
                                          ldat,
                                          &sa, 
                                          &pr_ek,
                                          &pr_vk);
	   }

       if (dsqlstatus == 0)
       {
                 aufp.auf_vk_pr  = pr_ek;
                 aufp.auf_lad_pr = pr_vk;
				 aufp.sa_kz_sint = sa;
	             strcpy (aufp.kond_art,   WaPreis.GetKondArt ());
				 aufp.a_grund   =  WaPreis.GetAGrund ();
// bei lackmann nicht n�tig 				 aufpListe.FillKondArt (aufp.kond_art);
       }
	   aufpListe.SetAufVkPr (pr_ek);
	   aufpListe.FillWaehrung (pr_ek, pr_vk);
	   aufpListe.SetPreise ();
}

void InsAufp (long auf, double a, long posi)
/**
Satz in Auftragsposition einfuegen.
**/
{
           aufk.lieferdat = dasc_to_long (ldat);
           if (ls_class.lese_aufp_a (aufk.mdn, aufk.fil, auf, a) != 0)
           {
               return;
           }
           aufp.sa_kz_sint = 0;
           aufp.auf_me  = (double) 0.0;
           aufp.auf_me1 = (double) 0.0;
           aufp.auf_me2 = (double) 0.0;
           aufp.auf_me3 = (double) 0.0;
           aufp.lief_me = (double) 0.0;
           aufp.posi = posi;
		   if (akt_preis)
		   {
		           ReadPr (a);
		   }
           ls_class.update_aufp (aufp.mdn, aufp.fil, 
                                 aufk.auf, a); //LAC-152 jetzt ohne posi
}


static int LastBest (void)
/**
Artikel der letzten Bestellungen holen.
**/
{
          int status; 
          long auf;
          double a;
          long posi;

          posi = 0;
          AufLast.AnzArt (anzart);
          AufLast.ReadLstAuf (aufk.mdn, atol (kuns));
          status = AufLast.GetFirst (&auf, &a);
          while (status)
          {
              posi += 10;
			  if (lese_a_bas (a) == 0)
			  {
				InsAufp (auf, a, posi);
			  }
              status = AufLast.GetNext (&auf, &a);
          }
          break_enter ();
          return 0;
}

BOOL MultiMdnProd (void)
/**
Test, ob mehrere verschiedene Gruppen in aufp existieren.
**/
{
	      int i;
		  int cursor;
     
		  DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		  DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		  DbClass.sqlin ((long *) &aufk.auf, 2, 0);
		  cursor = DbClass.sqlcursor ("select distinct gruppe from aufp "
			                          "where mdn = ? "
									  "and   fil = ? "
									  "and auf = ?");
		  i = 0;
		  while (DbClass.sqlfetch (cursor) == 0)
		  {
			  i ++;
		  }
		  DbClass.sqlclose (cursor);
		  if ( i > 1) return TRUE;
		  return FALSE;
}
           

static int SetKomplett (void)
{
	      if (mench > 0 ) return 0; //LAC-150
	      NoClose = TRUE;
          if (aufpListe.GetPosanz () == 0)
		  {
					 disp_mess ("Es wurden keine Positionen erfasst", 2);
					 SetCurrentField (currentfield);
					 return (1);
		  }
          if (ldatdiffOK () == FALSE)
		  {
					 SetCurrentField (currentfield);
					 SetCurrentFocus (currentfield);
		             return 1;
		  }
 		  if (testkun0 () == FALSE)
		  {
			         NoClose = FALSE;
					 return 1;
		  }
	      if (TouZTest () == FALSE) return 0;
          aufpListe.DestroyWindows ();
  	      aufpListe.KillProcessWgChart (); //LAC-52
  	      aufpListe.KillProcessShowBm (); //LAC-62
  	      aufpListe.KillProcessTourDat (); 




		  //LAC-180  aufptmp wird nicht mehr ben�tigt  Aufptmp.delete_aufptmp (aufk.auf); //LAC-52

		  if (lutz_mdn_par && aufk.auf_stat < 3)
		  {
			  if (MultiMdnProd ())
			  {
				  FormToAuf (1);
			  }
			  else
			  {
				  FormToAuf (3);
			  }
		  }
		  else
		  {
              FormToAuf (3);
		  }
		  ls_class.Commit_Work (); //LAC-188   hier wird mdn = -1 S�tze gel�scht


//*************** Aufruf Tourhinweis *********/
//java -jar TourHinweis.jar <Kundennummer> <Lieferdatum> <Tourennummer>     <Auftrag> <Mandant> //LAC-213     
	char *bws = getenv ("BWS");
	char text [80];

//	 if (aufk.tou_nr == 0) { disp_mess ("Es ist keine Tour hinterlegt", 0);  }

//	 sprintf (text,"javaw -jar %s\\bin\\TourHinweis.jar %ld %s %ld %ld %ld", bws,aufk.kun, aufk.lieferdat, aufk.tou_nr, aufk.auf,aufk.mdn);
	 sprintf (text,"javaw -jar %s\\bin\\TourHinweis.jar %ld %s %ld %ld %ld", bws,atoi(kuns), ldat, aufk.tou_nr, aufk.auf,aufk.mdn);

	if (getenv ("testmode")) 
	{
		if (atoi (getenv ("testmode")) == 1)  
		{
				disp_mess (text, 0);
		}
	}
	ProcWaitExecEx (text, SW_SHOW, 0, 40, -1, 0);
  //  int Ecode = WaitPid (Pid); 




		  //LAC-188   auch beim komplettsetzen akv .... setzen
				 char date[12];
				 char time [12];
				 sysdate (date);
				 systime (time);
				 time[5] = 0;
				 aufk.bearb = dasc_to_long (date);
				 strcpy (aufk.bearb_zeit, time);
				 if (aufk.akv == 0)
				 {
					aufk.akv = dasc_to_long (date);
					strcpy (aufk.akv_zeit, time);
				 }
				 if (aufk.best_dat == 0)
				 {
					aufk.best_dat  = dasc_to_long (date); 
				 }


		   strcpy (aufk.feld_bz1,VersAufk);  //LAC-188  Versionskennung
          ls_class.update_aufk (atoi (mdn), atoi (fil), aufk.auf);
//          commitwork ();
          if (aufk.auf != auto_nr.nr_nr)
          {
//                       beginwork ();
					   if (lutz_mdn_par)
					   {
						   sprintf (mdn, "%hd", 0);
						   sprintf (fil, "%hd", 0);
					   }
/*
                       dsqlstatus = AutoClass.nveinid (atoi (mdn), atoi (fil),
                                            "auf",  auto_nr.nr_nr);
*/

// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "auf",  auto_nr.nr_nr);
//                       commitwork ();
          }
          Mess.Message ("Satz wurde geschrieben");
		  if (lutz_mdn_par && aufk.auf_stat < 3)
		  {
			       AufKompl.ProdMdntoAuf (1);
		  } 
		  else if (KomplettMode == 0)
		  {
                   AufKompl.SetKomplett (mamain1);
		  }
          syskey = KEY12;
          break_enter ();
          NoClose = FALSE;
          SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
//          SetActiveWindow (hMainWindow);
          SetForegroundWindow (hMainWindow);
          SetMuster (FALSE);
          SetStorno (FALSE);
		  if (TransactMax > 0)
		  {
		        tcount ++;
		        if (tcount >= TransactMax)
				{
					 RestartProcess (0);
				}
		  }
//		  if (EnterKomm)
		  {
			   EnableMenuItem (hMenu, IDM_KOMMIS, MF_BYCOMMAND | MF_GRAYED);

		  }
	      EnableMenuItem (hMenu, IDM_NACHDRUCK, MF_BYCOMMAND | MF_ENABLED);
	      NachDrButton->Enable (TRUE);
	      CNachDruck.Color = BLUECOL;
		  DisplayFktBitmap ();

          return 0;
}

BOOL IsC_C (void)
/**
Test, ob C+C gesetzt ist. Dann ist die Tour-Nr immer 0.
**/
{
	     static int C_C = 0;
		 static BOOL DefOK = FALSE;
         char cfg_v [20];

		 if (DefOK == FALSE)
		 {
                    if (ProgCfg.GetGroupDefault ("C+C", cfg_v))
					{
					       if (atoi (cfg_v))
						   {
						          C_C = atoi (cfg_v);
						   }
					}
                    if (ProgCfg.GetCfgValue ("C+C", cfg_v) == TRUE)
					{
				           C_C = atoi (cfg_v);
					}
					DefOK = TRUE;
		 }
		 return C_C;
}


int lese_auf_mdn (long auf)
/**
Kunde ohne Mandant lesen. Mandant wird vom Kunden geholt.
**/
{
	   int dsqlstatus;
	   DbClass.sqlin ((long *) &auf, 2, 0);
	   DbClass.sqlout ((char *) mdn, 0, 5);
	   DbClass.sqlout ((char *) fil, 0, 5);
       dsqlstatus = DbClass.sqlcomm ("select mdn,fil from aufk where auf = ?");
	   return dsqlstatus;
}

static int leseauf ()
/**
Auftrag lesen.
**/
{
	     int dsqlstatus;
          
         if (testkeys ()) return 0;
         akt_kun = 0;  //LAC-181 
		 TestLizenz ();

         if (StartKun == 0) LiefAdr = 0l;
// Zus�tzliche Initialisierung : Problem bei K�bler mit Adresse from vorhergehenden Kunden.
// Kann aber nicht nachvollzogen werden. 07.10.2005 RoW
		 
		 memcpy (&_adr, &_adr_null, sizeof (_adr));
		 adr_nr = 0l;
//         temode = 1;
         SetTeleNr ();
		 aufk.auf = atol (auftrag);
/** LAC-150
         if (mench > 0 && akt_auf && 
             (atol (auftrag) == akt_auf)) 
         {
             return 0;
         }
**/


         aufpListe.DestroyWindows (); 
		 if (lutz_mdn_par)
		 {
			 dsqlstatus = lese_auf_mdn (atol (auftrag));
		 }
		 else
		 {
			 dsqlstatus = 0;
		 }
		 if (dsqlstatus == 0)
		 {
             dsqlstatus = ls_class.lese_aufk (atoi (mdn),
                                          atoi (fil),
                                          atol (auftrag));
			 strcpy (orders_id,aufk.auf_ext);
		 }
         if (aufk.delstatus != 0) dsqlstatus = 100;
         if (dsqlstatus == 0 && (mench == 0 || mench == 2))  // mench == 2 LACK-125
         {
                       if (aufk.auf_stat > 4)
                       {
                           disp_mess ("Der Auftrag wurde schon �bergeben", 2);
                           return 0;
                       }
                       if (Lock_Aufk () == FALSE) return 0;
         }
		 else if (mench == 0 || (mench == 2 && dsqlstatus == 0))
		 {
                       if (Lock_Aufk () == FALSE) return 0;
		 }

         if (dsqlstatus == 0 && mench == 0)
         {
                       NewRec = 0;
					   PosOK = TRUE;
					   ls_class.Begin_Work (); //LAC-167
         }
         else if (mench == 0)
         {
                       NewRec = 1;
					   PosOK = FALSE;
         }

         if (dsqlstatus == 0)
         {
			           if (aufk.psteuer_kz == 0)
					   {
						   CheckPartyService (FALSE);
					   }
					   else
					   {
						   CheckPartyService (TRUE);
					   }
			           adr_nr = aufk.adr; 
                       if (KunLiefKz)
                       {
                            LiefAdr = adr_nr;
                       }
                       AufToForm ();  
					   if (aufk.lieferdat == 0l)
					   {
						   SetLieferdat ();
					   }
         }

//LAC-150         else if (mench != 0 || aufk.auf != auto_nr.nr_nr)
         else if ((mench == 0 || mench == 2) && aufk.auf != auto_nr.nr_nr)  //LAC-150
         {
                       print_mess (2, "Auftrag %ld nicht gefunden",
                                   atol (auftrag));
					   if (mench != 1)
					   {
						   rollbackwork ();
						   beginwork ();
					   }
                       InitAuf ();
					   if (mench == 0) 
					   {
						   sprintf (auftrag, "%ld",auto_nr.nr_nr);
					   }
					   else
					   {
                            strcpy (auftrag, "0");
					   }
                       display_form (mamain1, &aufformk, 0, 0);
                       return 0;
         }
         else
         {
			           aufk.auf_stat = 0; 
                       if (Lock_Aufk () == FALSE) return 0;
                       memcpy (&aufk, &aufk_null, sizeof (struct AUFK));
                       InitAuf ();
                       aufk.mdn = atoi (mdn);
                       aufk.fil = atoi (fil);
                       aufk.auf = atol (auftrag);
					   if (IsPartyService)
					   {
						   aufk.psteuer_kz = 1;
						   SetToStandardTax ();
					   }
         }
		 aufk.ccmarkt = IsC_C ();

         if (mench == 1)
         {
             break_enter (); //LAC-150
         }
         else if (mench == 2)
         {
             ShowDaten ();
         }
         else 
         {
			 if (aufk.auf_stat == 0) lesekun0 (); //LAC-162
             break_enter ();
         }
         akt_auf = atol (auftrag);
//		 if (EnterKomm)
		 {
			   EnableMenuItem (hMenu, IDM_KOMMIS, MF_BYCOMMAND | MF_ENABLED);
		 }
	     EnableMenuItem (hMenu, IDM_NACHDRUCK, MF_BYCOMMAND | MF_GRAYED);
         EnableMenuItem (hMenu, IDM_TOURHINWEIS, MF_BYCOMMAND | MF_ENABLED);
		 TourhinweisButton->Enable (TRUE);


		 NachDrButton->Enable (FALSE);
//		 CNachDruck.Color = WHITECOL;
		 CNachDruck.Color = RGB (150, 150, 150);
		 DisplayFktBitmap ();
		 SchreibeDateiShopauf (); //LAC-74
         return 0;
}

void IncDatum (char *datum)
/**
Systemdatum um dat_plsu erhoehen.
**/
{
	    long ldat;
		
        if (datum_plus == 0l) return;

        ldat = dasc_to_long (datum);
        ldat += datum_plus;
        dlong_to_asc (ldat, datum);
}

long GetKunLiefTour (void)
/**
Tour aus kunlief holen.
**/
{
         KUNLIEF_CLASS KunLief;

         kunlief.mdn = atoi (mdn);
         kunlief.fil = atoi (fil);
         kunlief.kun = kun.kun;
         kunlief.adr = LiefAdr;
         if (KunLief.dbreadfirst () == 0)
         {
             if (kunlief.tou != 0l)
             {
                 return kunlief.tou;
             }
         }
         return kun.tou;
}

void SetHtext (char ** DayTab, char *WeekDay)
{
    char text [256];

    clipped (htext);
    strcpy (text, htext);
    for (int i = 0; DayTab[i] != NULL; i ++)
    {
        if (strlen (htext) < strlen (DayTab[i]))
        {
            continue;
        }
        if (memcmp (DayTab[i], htext, strlen (DayTab[i])) == 0)
        {
            break;
        }
    }
    int pos = 0;
    if (DayTab[i] != NULL)
    {
        pos = strlen (DayTab[i]);
    }

    strcpy (htext, WeekDay);
    strcat (htext, &text[pos]);
}

void GetWochenTag ()
{
    static char *WeekDay[] = {"Sonntag",
                              "Montag",
                              "Dienstag",
                              "Mittwoch",
                              "Donnerstag",
                              "Freitag",
                              "Samstag",
                              "Sonntag",
                              NULL,
    };

    if (Wochentag == FALSE)
    {
        return;
    }

    short wk = get_wochentag (ldat);
    SetHtext (WeekDay, WeekDay[wk]);
    int htextpos = GetItemPos (&aufform, "htext");
    if (htextpos > -1)
    {
         display_field (mamain1, &aufform.mask[htextpos], 0, 0);
    }
}

void GetLieferdat ()
/**
Lieferdatum aus Tourenplan holen.
**/
{
         char datum [12];
         long tou;
         int ldatpos;

         GetWochenTag ();
		 if (tou_par == 0) return;
		 if (IsC_C ()) 
		 {
            sprintf (tour, "%ld", 0l);
			return;
		 }
		 if (wo_tou_par == 0)
		 {
			      if (atoi (kunfil) == 0 && atol (tour) == 0l)
				  {
                         if (KunLiefKz && LiefAdr != 0l)
                         {
                             kun.tou = GetKunLiefTour ();
                         }
                         sprintf (tour, "%ld", kun.tou);
				  }

				  else
				  {
                         sprintf (tour, "%ld", _fil.tou);
				  }

                  testtou (1);
				  return;
		 }

		 if (tour_datum == 0) 
		 {
// Tour zum Datum holen
                int toupos = GetItemPos (&aufform, "tou");
                if ((aufform.mask[toupos].attribut & REMOVED) == 0)
                {
				   sysdate (datum);
				   IncDatum (datum);
				   strcpy (ldat, datum);
				   strcpy (kdat, datum);
				   strcpy (fix_dat, datum);
                   tou = QTClass.DatetoTouDir (aufk.mdn, 0, atoi (kunfil), 
                                  atol (kuns), ldat);
                   current_form = &aufform;
                   sprintf (tour, "%ld", tou);
                   display_field (mamain1, &aufform.mask[toupos], 0, 0);
                   if (tou) 
                   {
			             testtou (0);
                   }
		           else
                   {
                         sprintf (tour_krz, " "); 
                         int toukrzpos = GetItemPos (&aufform, "tou_bz");
                         display_field (mamain1, &aufform.mask[toukrzpos], 0, 0);
                   }
				}
			    return;
		 }

         sysdate (datum);
		 IncDatum (datum);
         strcpy (ldat, datum);
         strcpy (kdat, datum);
         strcpy (fix_dat, datum);
/*
         tou = QTClass.DatetoTou (aufk.mdn, aufk.fil, atoi (kunfil), 
                                  atol (kuns), ldat);
*/

// Tour mit Filiale 0 bearbeiten

         tou = QTClass.DatetoTou (aufk.mdn, 0, atoi (kunfil), 
                                  atol (kuns), ldat);
         if (tou == 0l) return;
         ldatpos = GetItemPos (&aufform, "lieferdat");
         GetWochenTag ();
         current_form = &aufform;
         display_field (mamain1, &aufform.mask[ldatpos], 0, 0);
         sprintf (tour, "%ld", tou);
         testtou (1);
}

int testkun0 ()
{
         int kun_krz_field; 
		 int kun_field;
         int tele_field;
         
         kun_field = GetItemPos (&aufform, "kun");
         kun_krz_field = GetItemPos (&aufform, "kun_krz");
         tele_field = GetItemPos (&aufform, "tele");
         if (atol (kuns) == 0)
         {
                  disp_mess ("Kunde 0 ist nicht erlaubt", 2);
                  SetCurrentField (kun_field);
                  return FALSE;
         }

	     if (autokunfil && akt_kun != atol (kuns))
		 {
	              aufk.kun_fil = 0;
		          strcpy (kunfil, "0");
                  ikunfil_bz.SetFeldPtr ("Kunde");
		 }
         if (atoi (kunfil) == 0)
         {
			      dsqlstatus = 0; 
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_kun_mdn (atol (kuns));
				  }
				  if (dsqlstatus == 0)
				  {
/*
                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                    atoi (fil),
                                                    atol (kuns));
*/

// Kunde immer mit Filiale 0 lesen 

                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                    0,
                                                    atol (kuns));
				  }
				  if (dsqlstatus == 100 && autokunfil)
				  {
					   aufk.kun_fil = 1;
					   strcpy (kunfil, "1");
                       ikunfil_bz.SetFeldPtr ("Filiale");
                       display_form (mamain1, &aufformk, 0, 0);
				  }
				  else
				  {
                      if (dsqlstatus)
					  { 
                       disp_mess ("Kunde nicht angelegt", 0);
                       SetCurrentField (kun_field);
					   akt_kun = 0l;
                       return FALSE;
					  }
                      if (kun.kun_of_lf > 0 && mench == 0) //LAC-150
                      {
                          disp_mess ("Der Kunde hat Lieferstop !!", 2);
                          SetCurrentField (kun_field);
	    			      akt_kun = 0l;
                          sprintf (kuns, "%8ld", 0l);
                          display_field (mamain1, &aufformk.mask[kun_field], 0, 0);
                          return FALSE;
                      }

                      if (OfPoOver (kun.mdn, kun.kun))
                      {
						  if (!abfragejn (mamain1, "Der Kunde hat mehr als 2 offene Rechnungen.\n"
												"Weiter erfassen ?", "N")) 
						  {
								SetCurrentField (kun_field);
	    						akt_kun = 0l;
								sprintf (kuns, "%8ld", 0l);
								display_field (mamain1, &aufformk.mask[kun_field], 0, 0);
								return FALSE;
						  }
					  }
//					  akt_kun = kun.kun;
                      if (KunLiefKz && LiefAdr > 0l)
                      {
                          kun.adr2 = LiefAdr;
                      }
                      dsqlstatus = adr_class.lese_adr (kun.adr2);
// KB Ardressnummer auch bei diversen Kunden �bernehmen
				      if (_adr.adr_typ != 25)
					  {
  		               aufk.adr = kun.adr2;
					  }
// Row �bernahme f�r diverse Kunden nur wenn lsk.adr != 0 17.06.2009
					  else if (aufk.adr == 0l)
					  {
  		               aufk.adr = kun.adr2;
					  }
                      strcpy (k_kurzb, _adr.adr_krz);
      		          aufk.waehrung = kun.waehrung;
			          if (vertr_abr_par && vertr_kun)
					  {
                           if (atol (vertrs) == 0l)
                           {
					             sprintf (vertrs, "%ld", kun.vertr1);
                           }
					       testvertr ();
 					  }
			          if ((aufk.kopf_txt == 0) && use_kopf_txt && kun.ls_kopf_txt)
					  {
					   aufk.kopf_txt = kun.ls_kopf_txt;
					  }
                      if (autokunfil)
					  {
 				          aufk.kun_fil = 0;
				 	      strcpy (kunfil, "0");
                          ikunfil_bz.SetFeldPtr ("Kunde");
                          display_form (mamain1, &aufformk, 0, 0);
					  }
				  }
         }
         if (atoi (kunfil) == 1)
         {
		          dsqlstatus = 0;
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_fil_mdn (atol (kuns));
				  }
//				  if (dsqlstatus == 0)
				  {
                       dsqlstatus = fil_class.lese_fil (atoi (mdn),
                                                    atoi (kuns));
                       if (dsqlstatus)
					   {
                                disp_mess ("Filiale nicht angelegt", 0);
                                SetCurrentField (kun_field);
								if (autokunfil)
								{
					                   aufk.kun_fil = 0;
					                   strcpy (kunfil, "0");
                                       ikunfil_bz.SetFeldPtr ("Kunde");
                                       display_form (mamain1, &aufformk, 0, 0);
								}
                                return FALSE;
					   }
                       else
					   {
                                dsqlstatus = adr_class.lese_adr (_fil.adr);
					            aufk.adr = _fil.adr;
                                strcpy (k_kurzb, _adr.adr_krz);
					            aufk.waehrung = atoi (_mdn.waehr_prim);
//					            akt_kun = _fil.fil;
					   }
				  }
         }
		 if (auf_art_active == FALSE)
		 {
                  display_field (mamain1, &aufform.mask[tele_field], 0, 0);
		 }
         clipped (_adr.tel);

//		 AktiviereReiter (TRUE);

         if (strcmp (_adr.tel, " ") > 0)
         {
                          CTelefon.bmp = btelefon;
                          ActivateColButton (mamain1, 
                                      &buform, 1, 0, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
         }
         else
         {
                          CTelefon.bmp = btelefoni;
                          ActivateColButton (mamain1, 
                                      &buform,      1, -1, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
         }
         clipped (_adr.fax);
         if (strcmp (_adr.fax, " ") > 0)
         {
                          ActivateColButton (mamain1, 
                                      &buform,      0, 0, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
         }
         else
         {
                          ActivateColButton (mamain1, 
                                      &buform,      0, -1, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
         }
         if (ls_class.lese_aufp (aufk.mdn, aufk.fil, 
                                 atol (auftrag)) == 100)
         {
                   set_fkt (LastBest, 8);
                   SetFkt (8, bestellung, KEY8);
         }
         else
         {
                   set_fkt (NULL, 8);
                   SetFkt (8, leer, NULL);
         }
         ActivateColButton (mamain1, &buform, 2, 0, 1);
         EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
         MoveButtons ();
         current_form = &aufform;
         display_field (mamain1, &aufform.mask[kun_field], 0, 0);
         display_field (mamain1, &aufform.mask[kun_krz_field], 0, 0);
		 return TRUE;
}

int lesekun0 ()
{
         int kun_krz_field;
         int kun_field;

		 clipped (kuns);
         kun_krz_field = GetItemPos (&aufform, "kun_krz");
         kun_field = GetItemPos (&aufform, "kun");
 	     if (!numeric (kuns))
		 {
                  if (SearchKunDirect)
                  {
                      return ShowKun ();
                  }
                  
			      kun.mdn = atoi (mdn);
			      kun.fil = atoi (fil);
                  QClass.searchkun_direct (mamain1, kuns);
		          sprintf (kuns, "%8ld", atol (kuns));
				  if (atol (kuns) == 0)
				  {
                            current_form = &aufform;
                            SetCurrentField (currentfield);
                            display_field (mamain1, &aufform.mask[kun_field], 0, 0);
							return 0;
				  }
		 }

		 sprintf (kuns, "%8ld", atol (kuns));

         if (akt_kun != atol (kuns))
         {
                  sprintf (tour, "%ld", 0l);
                  sprintf (vertrs, "%hd", 0);
                  strcpy (tour_krz, "");
                  strcpy (vertr_krz , "");

/*
                  if (tou_par)
                  {
                     display_field(mamain1, &aufform.mask[GetItemPos (&aufform, "tou")], 0, 0);
                     display_field(mamain1, &aufform.mask[GetItemPos (&aufform, "tou_bz")], 0, 0);
                  }
                  if (vertr_abr_par)
                  {
                     display_field(mamain1, &aufform.mask[GetItemPos (&aufform, "vertr")], 0, 0);
                     display_field(mamain1, &aufform.mask[GetItemPos (&aufform, "vertr_krz")], 0, 0); 
                  }
*/

                  if (tou_par || vertr_abr_par)
                  {
                     display_form (mamain1, &aufform, 0, 0);
				  }
         }

         SetFkt (9, leer, 0);
         set_fkt (NULL, 9);

         if (akt_kun == atol (kuns)) 
		 {
             GetLieferdat ();
             display_field (mamain1, &aufform.mask[kun_field], 0, 0);
             display_field (mamain1, &aufform.mask[kun_krz_field], 0, 0);
			 return 0;
		 }
         if (atol (kuns) == 0)
         {
                  disp_mess ("Kunde 0 ist nicht erlaubt", 2);
                  SetCurrentField (currentfield);
                  return 0;
         }

	     if (autokunfil && akt_kun != atol (kuns))
		 {
	              aufk.kun_fil = 0;
		          strcpy (kunfil, "0");
                  ikunfil_bz.SetFeldPtr ("Kunde");
		 }

         if (atoi (kunfil) == 0)
         {
			      dsqlstatus = 0; 
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_kun_mdn (atol (kuns));
				  }
				  if (dsqlstatus == 0)
				  {
/*
                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                    atoi (fil),
                                                    atol (kuns));
*/

// Kunde immer mir Filiale 0 lesen 

                        dsqlstatus = kun_class.lese_kun (atoi (mdn),
                                                         0,
                                                         atol (kuns));
				  }
			      if (dsqlstatus == 100 && autokunfil)
				  {
					   aufk.kun_fil = 1;
					   strcpy (kunfil, "1");
                       ikunfil_bz.SetFeldPtr ("Filiale");
                       display_form (mamain1, &aufformk, 0, 0);
				  }
				  else
				  {
                       if (dsqlstatus)
					   {
                           disp_mess ("Kunde nicht angelegt", 0);
                           SetCurrentField (currentfield);
                           return 0;
					   }
                       if (kun.kun_of_lf > 0 && mench == 0) //LAC-150
                       {
                          disp_mess ("Der Kunde hat Lieferstop !!", 2);
                          SetCurrentField (kun_field);
	    			      akt_kun = 0l;
                          sprintf (kuns, "%8ld", 0l);
                          display_field (mamain1, &aufformk.mask[kun_field], 0, 0);
                          return FALSE;
                       }
					   if (OfPoOver (kun.mdn, kun.kun))
                       {
						  if (!abfragejn (mamain1, "Der Kunde hat mehr als 2 offene Rechnungen.\n"
												"Weiter erfassen ?", "N")) 
						  {
								SetCurrentField (kun_field);
	    						akt_kun = 0l;
								sprintf (kuns, "%8ld", 0l);
								display_field (mamain1, &aufformk.mask[kun_field], 0, 0);
								return FALSE;
						  }
					   }
					   akt_kun = kun.kun;
                       if (KunLiefKz && LiefAdr > 0l)
                       {
                          kun.adr2 = LiefAdr;
                       }
       		           aufk.waehrung = kun.waehrung;
                       dsqlstatus = adr_class.lese_adr (kun.adr2);
					   if (!TestVerifyKun ())
					   {
						        kun.kun = 0l;
								sprintf (kuns, "%ld", kun.kun);
								current_form = &aufform;
								SetCurrentField (currentfield);
								display_field (mamain1, &aufform.mask[kun_field], 0, 0);
								return 0;
					   }
// KB Ardressnummer auch bei diversen Kunden �bernehmen
				      if (_adr.adr_typ != 25)
					   {
				           aufk.adr = _adr.adr;
					   }
// Row �bernahme f�r diverse Kunden nur wenn lsk.adr != 0 17.06.2009
					  else if (aufk.adr == 0l)
					  {
  		               aufk.adr = kun.adr2;
					  }
                       strcpy (k_kurzb, _adr.adr_krz);
				       if (vertr_abr_par && vertr_kun)
					   {
                           if (atol (vertrs) == 0l)
                           {
					                 sprintf (vertrs, "%ld", kun.vertr1);
                           }
 					       testvertr ();
					   }
			           if ((aufk.kopf_txt == 0) && use_kopf_txt && kun.ls_kopf_txt)
					   {
					       aufk.kopf_txt = kun.ls_kopf_txt;
					   }
					   if (autokunfil)
					   {
  				           aufk.kun_fil = 0;
					       strcpy (kunfil, "0");
                           ikunfil_bz.SetFeldPtr ("Kunde");
                           display_form (mamain1, &aufformk, 0, 0);
					   }
				  }
         }
         if (atoi (kunfil) == 1)
         {
		          dsqlstatus = 0;
		          if (lutz_mdn_par)
				  {
					   dsqlstatus = lese_fil_mdn (atol (kuns));
				  }
				  if (dsqlstatus == 0)
				  {
                       dsqlstatus = fil_class.lese_fil (atoi (mdn),
                                                    atoi (kuns));
                       if (dsqlstatus)
					   {
                                disp_mess ("Filiale nicht angelegt", 0);
								if (autokunfil)
								{
					                   aufk.kun_fil = 0;
					                   strcpy (kunfil, "0");
                                       ikunfil_bz.SetFeldPtr ("Kunde");
                                       display_form (mamain1, &aufformk, 0, 0);
								}
                                SetCurrentField (currentfield);
                                return 0;
					   }
                       else
					   {
                                dsqlstatus = adr_class.lese_adr (_fil.adr);
          			            aufk.adr = _adr.adr;
                                strcpy (k_kurzb, _adr.adr_krz);
					            akt_kun = kun.kun;
					   }
                       kun.einz_ausw = 0;
				  }
         }
         akt_kun = atol (kuns);
         GetLieferdat ();
         if (dsqlstatus == 0)
         {
                   clipped (_adr.tel);
                   if (strcmp (_adr.tel, " ") > 0)
                   {
                          CTelefon.bmp = btelefon;
                          ActivateColButton (mamain1, 
                                      &buform, 1, 0, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
                   }
                   else
                   {
                          CTelefon.bmp = btelefoni;
                          ActivateColButton (mamain1, 
                                      &buform,      1, -1, 1);
                         EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
                   }

                   clipped (_adr.fax);
                   if (strcmp (_adr.fax, " ") > 0)
                   {
                          ActivateColButton (mamain1, 
                                      &buform,      0, 0, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
                   }
                   else
                   {
                          ActivateColButton (mamain1, 
                                      &buform,      0, -1, 1);
                         EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
                   }
                   ActivateColButton (mamain1, &buform, 2, 0, 1);
                   EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
                   MoveButtons ();

         }
	   //LAC-21 A
       if (mench == 0) //LAC-150
	   {
		int dauf = TestAufKun (dasc_to_long (ldat)); 
		if (dauf > 0)
		{
			  if (abfragejn (mamain1, "Der Kunde hat noch einen offenen Auftrag zu diesem Datum .\n"
									"Diesen Auftrag weiter erfassen ?", "J")) 
			{
						dsqlstatus = AutoClass.nveinid (aufk.mdn, 0,
											"auf",  aufk.auf);
				commitwork ();
				beginwork ();
				sprintf (auftrag,"%ld", dauf);
				aufk.auf = dauf;
                ls_class.lese_aufk (aufk.mdn,aufk.fil,aufk.auf);  //LAC-167a
                AufToForm ();   //LAC-167a
				ls_class.Begin_Work (); //LAC-167a

				ShowDaten ();
				doliste ();
				break_enter ();
				return 0;
			}
		}
	   }
					   //LAC-21 E

         if (ls_class.lese_aufp (aufk.mdn, aufk.fil, 
                                 atol (auftrag)) == 100)
         {
                   set_fkt (LastBest, 8);
                   SetFkt (8, bestellung, KEY8);
         }
         else
         {
                   set_fkt (NULL, 8);
                   SetFkt (8, leer, NULL);
         }
         MoveButtons ();
         current_form = &aufform;
         display_field (mamain1, &aufform.mask[kun_field], 0, 0);
         display_field (mamain1, &aufform.mask[kun_krz_field], 0, 0);
         EnterKommissionierer (TRUE);
         return 1;
}

static int lesekun ()
/**
Kunde fuer Kundennummer holen.
**/
{
		 int ret;

         if (testkeys ()) return 0;

         SetFkt (8, leer, NULL);
         set_fkt (NULL, 8);
         if (akt_kun != atol (kuns))
         {
             sprintf (vertrs, "%ld", 0l);
         }
		 clipped (kuns);
		 if (numeric (kuns))
		 {
                 if (KunLiefKz)
                 {
                       ChoiseKunLief0 ();
                       if (atol (kuns) == 0l)
                       {
                           return 0;  //LAC-184  von 1 auf 0 bringt nix
					   }
                 }
		         if (lesekun0 () == FALSE) 
                 {
                     return 0; //LAC-184  von 1 auf 0   bringt nix
                 }
		 }
		 else
		 {
			ret = lesekun0 ();
			if (ret == 0) 
			{
				 return 1;
			}
			if (KunLiefKz)
			{
				 ChoiseKunLief0 ();
				testkun0 ();
				return 1;
			}
		 }
		 display_form (mamain1, &aufform, 0, 0);
		 if (listedirect == 1)
		 {
			 PostMessage (mamain1, WM_KEYDOWN, VK_F6, 0l);
		 }
         if (ret && EnableKunDirect)
         {
             EnableKun (FALSE);
         }
		 return 1;
}

long KopfTextGen (void)
/**
Kopftexte erfassen.
**/
{
           long text_nr;
           extern short sql_mode;


           sql_mode = 1;
           dsqlstatus = AutoClass.nvholid (0, 0, "ls_txt");
		   if (dsqlstatus == -1)
		   {
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"ls_txt\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
			}
					        
            if (dsqlstatus == 100)
            {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  "ls_txt",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (0,
                                                        0,
                                                        "ls_txt");
                           }
             }
             sql_mode = 0;
             if (auto_nr.nr_nr == 0l)
             {
                    disp_mess ("Es konnte keine text-Nr generiert werden", 2);
                    return 0l;
             }
			 text_nr = auto_nr.nr_nr;
			 return text_nr;
}


BOOL KopfTextExist (short mdn, short fil, long kun, long kopt_txt)
/**
Test, ob die Nummer schon benutzt wird.
**/
{
	       int dsqlstatus;

	       DbClass.sqlin ((short *) &mdn, 1, 0); 
	       DbClass.sqlin ((short *) &fil, 1, 0); 
	       DbClass.sqlin ((long *)  &kun, 2, 0); 
	       DbClass.sqlin ((long *)  &kopt_txt, 2, 0); 
		   dsqlstatus = DbClass.sqlcomm ("select auf from aufk "
			                             "where mdn = ? "
										 "and fil = ? "
										 "and kun != ? "
										 "and kopf_txt = ?");
		   if (dsqlstatus == 0) return TRUE;
		   return FALSE;
}


int KopfTexte (void)
/**
Kopftexte erfassen.
**/
{
	       long nr_nr;
           long text_nr;

		   if (mench > 0 ) return 0; //LAC-150
           nr_nr = auto_nr.nr_nr;
		   text_nr = aufk.kopf_txt;
		   if (text_nr == 0l)
		   {
		      while (TRUE)
			  {
			     text_nr = KopfTextGen ();
				 if (text_nr == 0l) return 0l;
			     if (KopfTextExist (aufk.mdn, aufk.fil, aufk.kun, text_nr) == FALSE) 
				 {
				                   break;
				 }
			  }

              auto_nr.nr_nr = 0;
               
//        Die Transtion wird an dieser Stelle geschlossen.

              FormToAuf (0);
              aufk.delstatus = -1;
              ls_class.update_aufk (atoi (mdn), atoi (fil), aufk.auf);
              FreeAufNr = FALSE; 
              commitwork ();

              if (aufk.auf != nr_nr)
			  {
                       beginwork ();
					   if (lutz_mdn_par)
					   {
							   sprintf (mdn, "%hd", 0);
 						       sprintf (fil, "%hd", 0);
					   }
/*
                       dsqlstatus = AutoClass.nveinid (atoi (mdn), atoi (fil),
                                            "auf",  nr_nr);
*/
// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "auf",  nr_nr);
                       commitwork ();
					   sprintf (mdn, "%hd", aufk.mdn);
					   sprintf (fil, "%hd", aufk.fil);
			  }
              beginwork ();
              Lock_Aufk ();
		   }
           aufk.kopf_txt = text_nr;

           set_fkt (NULL, 9);
           SetFkt (9, leer, 0);
           set_fkt (NULL, 10);
           SetFkt (10, leer, 0);
           set_fkt (NULL, 11);
           SetFkt (11, leer, 0);
           lstxtlist.SethMainWindow (mamain1);
           lstxtlist.SetListLines (12);
//           lstxtlist.SetTextMetric (&tm);
		   EnableWindow (aufpListe.GetMamain1 (), FALSE);
           lstxtlist.EnterAufp ("Kopftext", text_nr);
           text_nr = ls_txt.nr;
		   aufk.kopf_txt = ls_txt.nr; 
		   EnableWindow (aufpListe.GetMamain1 (), TRUE);
           SetActiveWindow (mamain1);
           SetCurrentFocus (currentfield);
    
           set_fkt (dokey5, 5);
           set_fkt (doliste, 6);
           set_fkt (dokey12, 12);
           set_fkt (NULL, 7);
           SetFkt (6, liste, KEY6);
           SetFkt (7, leer, 0);
           set_fkt (KopfTexte, 10);
           set_fkt (FussTexte, 11);
           SetFkt (10, kopftext, KEY10);
           SetFkt (11, fusstext, KEY11);

           if (aufpListe.GetPosanz () > 0)
		   {
			   if (KomplettMode < 2)
			   {
		             set_fkt (SetKomplett, 8);
                     SetFkt (8, komplett, KEY8);
			   }
		   }
		   else
		   {
               set_fkt (LastBest, 8);
               SetFkt (8, bestellung, KEY8);
		   }

           return 0;
}


int FussTexte (void)
/**
Fusstexte erfassen.
**/
{
           long nr_nr;
           long text_nr;
           extern short sql_mode;
		   if (mench > 0 ) return 0; //LAC-150


           nr_nr = auto_nr.nr_nr;

           if (aufk.fuss_txt == 0)
           {
                sql_mode = 1;
                dsqlstatus = AutoClass.nvholid (0, 0, "ls_txt");
				if (dsqlstatus == -1)
				{
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"ls_txt\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
				}
					        
                if (dsqlstatus == 100)
                {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  "ls_txt",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (0,
                                                        0,
                                                        "ls_txt");
                           }
                }
                sql_mode = 0;
                if (auto_nr.nr_nr == 0l)
                {
                    disp_mess ("Es konnte keine text-Nr generiert werden", 2);
                    return 0;
                }

                text_nr = auto_nr.nr_nr;

                auto_nr.nr_nr = 0;
               
//        Die Transtion wird an dieser Stelle geschlossen.

                 FormToAuf (0);
                 aufk.delstatus = -1;
                 ls_class.update_aufk (atoi (mdn), atoi (fil), aufk.auf);
                 commitwork ();

                 if (aufk.auf != nr_nr)
                 {
                       beginwork ();
					   if (lutz_mdn_par)
					   {
							   sprintf (mdn, "%hd", 0);
 						       sprintf (fil, "%hd", 0);
					   }
/*
                       dsqlstatus = AutoClass.nveinid (atoi (mdn), atoi (fil),
                                            "auf",  nr_nr);
*/
// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "auf",  nr_nr);
                       commitwork ();
					   sprintf (mdn, "%hd", aufk.mdn);
					   sprintf (fil, "%hd", aufk.fil);
                 }
                 beginwork ();
                 Lock_Aufk ();
                 aufk.fuss_txt = text_nr;
           }
           else
           {
                  text_nr = aufk.fuss_txt;
           }

           set_fkt (NULL, 9);
           SetFkt (9, leer, 0);
           set_fkt (NULL, 10);
           SetFkt (10, leer, 0);
           set_fkt (NULL, 11);
           SetFkt (11, leer, 0);
           lstxtlist.SethMainWindow (mamain1);
           lstxtlist.SetListLines (12);
//         lstxtlist.SetTextMetric (&tm);
		   EnableWindow (aufpListe.GetMamain1 (), FALSE);
           lstxtlist.EnterAufp ("Fu�text", text_nr);
           text_nr = ls_txt.nr;
		   aufk.fuss_txt = ls_txt.nr; 
		   EnableWindow (aufpListe.GetMamain1 (), TRUE);
           SetActiveWindow (mamain1);
           SetCurrentFocus (currentfield);
    
           set_fkt (dokey5, 5);
           set_fkt (doliste, 6);
           set_fkt (dokey12, 12);
           set_fkt (NULL, 7);
           SetFkt (6, liste, KEY6);
           SetFkt (7, leer, 0);
           set_fkt (KopfTexte, 10);
           set_fkt (FussTexte, 11);
           SetFkt (10, kopftext, KEY10);
           SetFkt (11, fusstext, KEY11);
           if (aufpListe.GetPosanz () > 0)
		   {
			   if (KomplettMode < 2)
			   {
		             set_fkt (SetKomplett, 8);
                     SetFkt (8, komplett, KEY8);
			   }
		   }
		   else
		   {
               set_fkt (LastBest, 8);
               SetFkt (8, bestellung, KEY8);
		   }
           return 0;
}


int setkey10_11 (void)
{
       set_fkt (KopfTexte, 10);
       set_fkt (FussTexte, 11);
       SetFkt (10, kopftext, KEY10);
       SetFkt (11, fusstext, KEY11);
       return 0;
}

static int BreakKomm (void)
{
       break_enter ();
       return 1;
}

static char kommdat [12];

static BOOL KommdatOk (void)
/**
Abweichung beim Kommisionierdatum pruefen.
**/
{       

	   char ldatum [12];
	   long dat_von, dat_bis, lidat;

	   if (strcmp (kommdat, "01.01.1900") <= 0)
	   {
		   strcpy (kommdat, ldat);
	   }
	   sysdate (ldatum);
	   dat_von = dasc_to_long (ldatum) - ldatdiffminus;
	   dat_bis = dasc_to_long (ldatum) + ldatdiffplus;
	   lidat = dasc_to_long (kommdat);

	   if ((lidat < dat_von) || (lidat > dat_bis))
	   {
		    if (mench == 0) //LAC-150
			{
				 disp_mess ("Abweichung beim Kommisionierdatum ist zu gro�", 2);
				SetCurrentField (currentfield);
				return FALSE;
			}
	   }
	   return TRUE;
}


static int testkommwert (void)
{
/*
        long dat1;
        long dat2;
        char datum [12];
*/

        if (syskey == KEY5 || syskey == KEY12)
        {
            break_enter ();
            return 0;
        }
		if (KommdatOk () == FALSE)
        {
            SetCurrentField (currentfield);
            return 0;
        }
/* Achtung Tagesdatum wird nicht mehr gepr�ft, da in KommdatOk  
   Tagesdatum mit ldatdiffminus und ldatdiffplus gepr�ft wird.

        sysdate (datum);
        dat1 = dasc_to_long (datum);
        dat2 = dasc_to_long (kommdat);
        if (dat2 < dat1)
        {
            print_mess (2,"Das Kommisionier-Datum ist zu klein");
            SetCurrentField (currentfield);
            return 0;
        }
*/
        return 1;
}
       


void InputKommdat (void)
/**
Kommisionierdatum eingeben.
**/
{
       HWND KommDat;
       int end_break;
       int currents;

       static ITEM ikommdat ("komm_dat", kommdat,
                             "Kommisionierdatum :", 0);

       static field _fkommdat [] = {
           &ikommdat,     11, 0, 1, 1, 0, "dd.mm.yyyy", 
                                           EDIT,
                                           0,
                                           testkommwert,
                                           0,
           &iOK,          12, 0, 3, 5, 0, "", 
		                                   BUTTON,
										   0,0, KEY12,
           &iCancel,      12, 0, 3,19, 0, "", 
		                                   BUTTON,
										   0,0, KEY5};

        static form fkommdat = {3, 0, 0, _fkommdat, 
               0, 0, 0, 0, NULL};

        save_fkt (5);
        save_fkt (6);
        save_fkt (7);
        save_fkt (8);
        save_fkt (9);
        save_fkt (10);
        save_fkt (11);
        save_fkt (12);

        set_fkt (BreakKomm, 5);
        set_fkt (BreakKomm, 8);


        currents = currentfield;
        end_break = GetBreakEnd ();
        break_end ();
        EnableWindows (mamain1, FALSE);
        EnableWindow (aufpListe.GetMamain1 (), FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_DLGFRAME);
        KommDat = OpenWindowChC (5, 36, 10, 20, hMainInst,
                   "");
        strcpy (kommdat, kdat);
        enter_form (KommDat, &fkommdat, 0, 0);
        if (syskey != KEY5)
        {
            strcpy (kdat, kommdat);
        }

        EnableWindow (aufpListe.Getmamain3 (), TRUE);
        EnableWindows (mamain1, TRUE);
        DestroyWindow (KommDat);
        SetBreakEnd (end_break);
        SetAktivWindow (mamain1);
        SetActiveWindow (mamain1);
        currentfield = currents;
        SetCurrentFocus (currentfield + 1);

        restore_fkt (5);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        restore_fkt (9);
        restore_fkt (10);
        restore_fkt (11);
        restore_fkt (12);
}


static char fixdat [12];

static int testfixwert (void)
{
        long dat1;
        long dat2;
        char datum [12];

        if (syskey == KEY5 || syskey == KEY12)
        {
            break_enter ();
            return 0;
        }
/*
		if (KommdatOk () == FALSE)
        {
            SetCurrentField (currentfield);
            return 0;
        }
*/
        sysdate (datum);
//        dat1 = dasc_to_long (datum);
        dat1 = dasc_to_long (ldat);
        dat2 = dasc_to_long (fixdat);

        if (dat2 < dat1)
        {
            print_mess (2,"Das Fix-Datum ist zu klein");
            SetCurrentField (currentfield);
            return 0;
        }

        return 1;
}
       

void InputFixdat (void)
/**
Kommisionierdatum eingeben.
**/
{
       HWND FixDat;
       int end_break;
       int currents;

       static ITEM iOK     ("OK",     "     OK     ", "", 0);
       static ITEM ifixdat ("fix_dat", fixdat,
                             "Fixes Datum :", 0);

       static field _ffixdat [] = {
           &ifixdat,     11, 0, 1, 1, 0, "dd.mm.yyyy", 
                                           EDIT,
                                           0,
                                           testfixwert,
                                           0,
           &iOK,          12, 0, 3, 5, 0, "", 
		                                   BUTTON,
										   0,0, KEY12,
           &iCancel,      12, 0, 3,19, 0, "", 
		                                   BUTTON,
										   0,0, KEY5};

        static form ffixdat = {3, 0, 0, _ffixdat, 
               0, 0, 0, 0, NULL};

        save_fkt (5);
        save_fkt (6);
        save_fkt (7);
        save_fkt (8);
        save_fkt (9);
        save_fkt (10);
        save_fkt (11);
        save_fkt (12);

        set_fkt (BreakKomm, 5);
        set_fkt (BreakKomm, 8);


        currents = currentfield;
        end_break = GetBreakEnd ();
        break_end ();
        EnableWindows (mamain1, FALSE);
        EnableWindow (aufpListe.GetMamain1 (), FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_DLGFRAME);
        FixDat = OpenWindowChC (5, 36, 10, 20, hMainInst,
                   "");
        strcpy (fixdat, fix_dat);
        enter_form (FixDat, &ffixdat, 0, 0);
        if (syskey != KEY5)
        {
            strcpy (fix_dat, fixdat);
        }

        EnableWindow (aufpListe.Getmamain3 (), TRUE);
        EnableWindows (mamain1, TRUE);
        DestroyWindow (FixDat);
        SetBreakEnd (end_break);
        SetAktivWindow (mamain1);
        SetActiveWindow (mamain1);
        currentfield = currents;
        SetCurrentFocus (currentfield + 1);

        restore_fkt (5);
        restore_fkt (6);
        restore_fkt (7);
        restore_fkt (8);
        restore_fkt (9);
        restore_fkt (10);
        restore_fkt (11);
        restore_fkt (12);
}

static int SearchPos = 8;
static int OKPos = 9;
static int CAPos = 10;

struct SADR
{
	  long adr;
	  char adr_krz [17];
	  char adr_nam1 [37];
	  char adr_nam2 [37];
	  char pf [17];
	  char plz [9];
	  char str [37];
	  char ort1 [37];
	  char ort2 [37];
	  short adr_typ;
};

static struct SADR *sadrtab = NULL;
static struct SADR sadr;
static int idx;
static long adranz;
static CHQEX *Query;
// static CHQ *Query;
static HWND adrwin;
static short adr_typ;


int SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   int i;
	   int len;

	   if (sadrtab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < adranz; i ++)
	   {
		   len = min (16, strlen (sebuff));
		   if (strupcmp (sebuff, sadrtab[i].adr_krz, len) == 0) break;
	   }
	   if (i == adranz) return 0;
	   Query->SetSel (i);
	   return 0;
}

int ReadAdr (char *adr_name)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;

	  if (sadrtab) 
	  {
		  delete sadrtab;
		  sadrtab = NULL;
	  }

      clipped (adr_name);
      if (strlen (adr_name) == 1 &&
          adr_name[0] == ' ')
      {
          adr_name[0] = 0;
      }
	  adranz = 0;
	  sprintf (buffer, "select count (*) from adr where adr_krz "
		            "matches \"%s*\" and adr_typ between 20 and 25", adr_name);
      DbClass.sqlout ((int *) &adranz, 2, 0);
	  DbClass.sqlcomm (buffer);
	  if (adranz == 0) return 0;

	  sadrtab = new struct SADR [adranz + 2];
	  sprintf (buffer, "select adr, adr_krz, "
		               "adr_nam1, adr_nam2, pf, plz, "
					   "str, ort1, ort2, adr_typ from adr where adr_krz "
		               "matches \"%s*\"  and adr_typ between 20 and 25 "
					   "order by adr_nam1", adr_name);
      DbClass.sqlout ((long *) &sadr.adr, 2, 0);
      DbClass.sqlout ((char *) sadr.adr_krz, 0, 37);
      DbClass.sqlout ((char *) sadr.adr_nam1, 0, 37);
      DbClass.sqlout ((char *) sadr.adr_nam2, 0, 37);
      DbClass.sqlout ((char *) sadr.pf, 0, 17);
      DbClass.sqlout ((char *) sadr.plz, 0, 9);
      DbClass.sqlout ((char *) sadr.str, 0, 37);
      DbClass.sqlout ((char *) sadr.ort1, 0, 37);
      DbClass.sqlout ((char *) sadr.ort2, 0, 37);
      DbClass.sqlout ((short *) &sadr.adr_typ, 1, 0);
	  cursor = DbClass.sqlcursor (buffer);
	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
	  {
          if (TestDivAdr)
          {
                  if (sadr.adr < sadr_von) continue;  
                  if (sadr.adr > sadr_bis) continue;  
          }
		  memcpy (&sadrtab[i], &sadr, sizeof (sadr));
 	      sprintf (buffer, " %-8ld \"%-36s\" \"%-36s\"", sadr.adr, sadr.adr_nam1, sadr.ort1); 
	      Query->InsertRecord (buffer);
		  i ++;
	  }
	  DbClass.sqlclose (cursor);
	  return 0;
}

static void SearchAdr (void)
{
  	  int cx, cy;
	  char buffer [256];
	  form *scurrent;

	  scurrent = current_form;
	  idx = -1;
      cx = 80;
      cy = 20;
      Query = new CHQEX (cx, cy, "Name", "");
//      Query = new CHQ (cx, cy, "Name", "");
      Query->OpenWindow (hMainInst, hMainWindow);
	  sprintf (buffer, " %9s %36s %36s", "1", "1", "1"); 
	  Query->VLines (buffer, 0);
	  EnableWindow (hMainWindow, FALSE);
	  EnableWindow (adrwin, FALSE);

	  sprintf (buffer, " %-8s %-36s %-36s", "AdrNr", "Name", "Ort"); 
	  Query->InsertCaption (buffer);

	  Query->SetFillDb (ReadAdr);
	  Query->SetSearchLst (SearchLst);
	  Query->ProcessMessages ();
      idx = Query->GetSel ();
	  EnableWindow (hMainWindow, TRUE);
	  EnableWindow (adrwin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (adrwin);
	  if (idx == -1) return;
	  memcpy (&sadr, &sadrtab[idx], sizeof (sadr));
	  if (sadrtab) delete sadrtab;
	  sadrtab = NULL;
	  current_form = scurrent;
	  if (syskey == KEY5) return;
	  sprintf (current_form->mask[0].item->GetFeldPtr (), "%ld", sadr.adr); 
	  sprintf (current_form->mask[1].item->GetFeldPtr (), "%s", sadr.adr_nam1); 
	  sprintf (current_form->mask[2].item->GetFeldPtr (), "%s", sadr.adr_nam2); 
	  sprintf (current_form->mask[3].item->GetFeldPtr (), "%s", sadr.pf); 
	  sprintf (current_form->mask[4].item->GetFeldPtr (), "%s", sadr.str); 
	  sprintf (current_form->mask[5].item->GetFeldPtr (), "%s", sadr.plz); 
	  sprintf (current_form->mask[6].item->GetFeldPtr (), "%s", sadr.ort1); 
	  sprintf (current_form->mask[7].item->GetFeldPtr (), "%s", sadr.ort2); 
	  adr_typ = sadr.adr_typ;
	  display_form (adrwin, current_form);
}


static int testadr (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY9 :
                       if (TestDivAdr < 2)
                       {
 					        SearchAdr ();
                       }
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
                case KEYCR :
					   if (currentfield == GetItemPos (current_form, "OK"))
					   {
                             syskey = KEY12;
                             break_enter ();
                             return 1;
					   }
					   else if (currentfield == GetItemPos (current_form, "CA"))
					   {
                             syskey = KEY5;
                             break_enter ();
                             return 1;
					   }
					   else if (currentfield == GetItemPos (current_form, "Search"))
					   {
                             syskey = KEY9;
							 SearchAdr ();
                             return 1;
					   }
         }
        return 0;
}


void AdrInput (long adr_nr)
/**
Eingabemaske fuer Adresse diverser Kunde.
**/
{
        HANDLE hMainInst;
		HWND hWnd;

        static char adrval[41];
        static char adrz1val [41];
        static char adrz2val [41];
        static char pfval [41];
        static char strval [41];
        static char plzval [41];
        static char ort1val [41];
        static char ort2val [41];

        static ITEM iOK     ("OK",     "     OK     ", "", 0);
        static ITEM iCA     ("CA",     "  Abbrechen ", "", 0);
        static ITEM iSearch ("Search", "    Suchen  ", "", 0);


        static ITEM iadr ("adr", 
                           adrval, 
                             "Adress-Nr        ", 
                           0);

        static ITEM iadrz1 ("adr_nam1", 
                                adrz1val, 
                             "Adressz. 1       ", 
                                   0);
        static ITEM iadrz2 ("adr_nam1", 
                                adrz2val, 
                             "Adressz. 2       ", 
                                   0);
        static ITEM ipf ("pf", 
                             pfval, 
                             "Postfach         ", 
                             0);

        static ITEM istr ("str", 
                            strval, 
                             "Strasse          ", 
                            0);
        static ITEM iplz ("plz", 
                            plzval, 
                             "Postleitzahl     ", 
                             0);
        static ITEM iort1 ("ort1", 
                                ort1val, 
                             "Ort 1.Zei        ", 
                                   0);
        static ITEM iort2 ("ort2", 
                                ort2val, 
                             "Ort 2.Zei        ", 
                                   0);


        static field _adrform[] = {
           &iadr,     9, 0, 1,1, 0, "%8d", NORMAL, 0, 0,0,
           &iadrz1,  37, 0, 2,1, 0, "",    NORMAL, 0, testadr,0,
           &iadrz2,  37, 0, 3,1, 0, "",    NORMAL, 0, testadr,0,
           &ipf,     16, 0, 4,1, 0, "",    NORMAL, 0, testadr,0,
           &istr,    37, 0, 5,1, 0, "",    NORMAL, 0, testadr,0,
           &iplz,     7, 0, 6,1, 0, "",    NORMAL, 0, testadr,0,
           &iort1,   37, 0, 7,1, 0, "",    NORMAL, 0, testadr,0,
           &iort2,   37, 0, 8,1, 0, "",    NORMAL, 0, testadr,0,
           &iSearch, 15, 0,10, 5, 0, "", BUTTON, 0, testadr,KEY9,
           &iOK,     15, 0,10,22, 0, "", BUTTON, 0, testadr,KEY12,
           &iCA,     15, 0,10,39, 0, "", BUTTON, 0, testadr,KEY5,
		};

        static form adrform = {11, 0, 0, _adrform, 
			                    0, 0, 0, 0, NULL};
        

		int savefield;
		form *savecurrent;

        if (TestDivAdr == 2)
        {
            _adrform[8].attribut = REMOVED;
        }
		adr_typ = _adr.adr_typ;
        hWnd = AktivWindow;
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testadr, 5);
        set_fkt (testadr, 9);
        set_fkt (testadr, 11);
        set_fkt (testadr, 12);

 		sprintf (adrval, "%ld", adr_nr);
		strcpy (adrz1val,_adr.adr_nam1);
		strcpy (adrz2val,_adr.adr_nam2);
		strcpy (pfval,_adr.pf);
		strcpy (strval,_adr.str);
		strcpy (plzval,_adr.plz);
		strcpy (ort1val,_adr.ort1);
		strcpy (ort2val,_adr.ort2);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        adrwin = OpenWindowChC (13, 62, 9, 10, hMainInst,
                               "Anschrift : Diverser Kunde");
		EnableWindow (aufpListe.GetMamain1 (), FALSE);
		EnableWindow (hMainWindow, FALSE);
        SetButtonTab (TRUE);
        enter_form (adrwin, &adrform, 0, 0);

        SetButtonTab (FALSE);
        CloseControls (&adrform);
		EnableWindow (aufpListe.GetMamain1 (), TRUE);
		EnableWindow (hMainWindow, TRUE);
        DestroyWindow (adrwin);
		current_form = savecurrent;
		currentfield = savefield;
        restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (9);
		restore_fkt (11);
		restore_fkt (12);

		if (syskey == KEY5) return;

		adr_nr = atol (adrval);
		aufk.adr = adr_nr;
		_adr.adr = aufk.adr;
		strcpy (_adr.adr_nam1, adrz1val);
		strcpy (_adr.adr_nam2, adrz2val);
		strcpy (_adr.pf,       pfval);
		strcpy (_adr.str,      strval);
		strcpy (_adr.plz,      plzval);
		strcpy (_adr.ort1,     ort1val);
		strcpy (_adr.ort2,     ort2val);
		_adr.adr_typ = adr_typ;
		adr_class.dbupdate ();

}

void fillsadr (void)
/**
Gesch�tzte Adressbereiche f�llen.
**/
{
	  static BOOL adrber_ok = FALSE;
	  int dsqlstatus;
	  
	  if (adrber_ok) return;

	  adrber_ok = TRUE;
	  memcpy (&ptabn, &ptabn_null, sizeof (ptabn));
      dsqlstatus = ptab_class.lese_ptab ("adr", "1");
	  if (dsqlstatus) 
	  {
		  return;
	  }
	  adr_von = atol (ptabn.ptwer1);
	  adr_bis = atol (ptabn.ptwer2);
	  memcpy (&ptabn, &ptabn_null, sizeof (ptabn));
      dsqlstatus = ptab_class.lese_ptab ("adr", "2");
	  if (dsqlstatus) 
	  {
		  return;
	  }
	  sadr_von = atol (ptabn.ptwer2) + 1;
	  sadr_bis = adr_bis;
}

long GenAdr (void)
/**
Adresse fuer diverse Kunden generieren.
**/
{
           extern short sql_mode;

           sql_mode = 1;
//           dsqlstatus = AutoClass.nvholid (0, 0, "adr");
           dsqlstatus = AutoClass.nvholidk2 (0, 0, "adr");
		   if (dsqlstatus == -1)
		   {
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"adr\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
			}
					        
           if (dsqlstatus == 100)
           {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  "adr",
                                                  (long) sadr_von,
                                                  (long) sadr_bis,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
//                                  dsqlstatus = AutoClass.nvholid (0,
                                  dsqlstatus = AutoClass.nvholidk2 (0,
                                                        0,
                                                        "adr");
                           }
            }
            sql_mode = 0;
/*
            if (auto_nr.nr_nr == 0l)
            {
                    disp_mess ("Es konnte keine Adress-Nr generiert werden", 2);
                    return 0l;
            }
*/

            return auto_nr.nr_nr;

}

long AdrExist (long adr_nr)
/**
Testen, ob die Adress schon existiert.
**/
{
	         int dsqlstatus;

	         DbClass.sqlin ((long *) &adr_nr, 2, 0);
			 dsqlstatus = DbClass.sqlcomm ("select adr from adr where adr = ?");
			 if (dsqlstatus == 0) return TRUE;
			 return FALSE;
}

static BOOL AdrGen (long kun_nr)
/**
Test, ob eine ADressnummer generiert werden soll.
**/
{

	        if (adr_nr == 0l)
			{
                   return TRUE;
			}
            else if (adr_nr < sadr_von)
            {
                   return TRUE;
            }
            else if (adr_nr > sadr_bis)
            {
                   return TRUE;
            }
			return FALSE;
}
	        

int InputAdr (void)
/**
Adresse fuer diverse Kunden eingeben.
**/
{
            long nr_nr;
			int count;

		   if (mench > 0 ) return 0; //LAC-150

		    fillsadr ();
			gen_div_adr = AdrGen (atol (kuns));
		    if (gen_div_adr == FALSE)
			{
                    dsqlstatus = adr_class.lese_adr (adr_nr);
					if (dsqlstatus == 100)
					{
                             dsqlstatus = adr_class.lese_adr (kun.adr2);
					}
			        AdrInput (adr_nr);
                    SetCurrentFocus (currentfield);
                    return 0;
			}
  		    adr_nr = 0l;
			count = 0;
            nr_nr = auto_nr.nr_nr;
			while (TRUE)
			{
                    adr_nr = GenAdr ();
					if (adr_nr == 0l)
					{
                        count ++;
						Sleep (5);
					}
                    if (adr_nr < sadr_von)
                    {
                        continue;
					}
                    if (adr_nr > sadr_bis)
                    {
                        continue;
					}
					else if (AdrExist (adr_nr) == FALSE)
					{
						break;
					}
			}

            if (auto_nr.nr_nr == 0l)
            {
                    disp_mess ("Es konnte keine Adress-Nr generiert werden", 2);
                    return 0;
            }

            adr_nr = auto_nr.nr_nr;

            auto_nr.nr_nr = 0;
               
//        Die Transakttion wird an dieser Stelle geschlossen.

            FormToAuf (0);
            ls_class.update_aufk (atoi (mdn), atoi (fil), aufk.auf);
            FreeAufNr = FALSE; 
            commitwork ();

            if (aufk.auf != nr_nr)
            {
                       beginwork ();
/*
                       dsqlstatus = AutoClass.nveinid (atoi (mdn), atoi (fil),
                                            "auf",  nr_nr);
*/
// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "auf",  nr_nr);
                       commitwork ();
            }
            beginwork ();
//            aufk.adr = adr_nr;
			AdrInput (adr_nr);
            SetCurrentFocus (currentfield);
    	    gen_div_adr = FALSE;
            return 0;
}


int saveldat (void)
/**
Lieferdatum merken.
**/
{
      if (IsAng)
	  {
	        GetLieferdat ();
			IsAng = FALSE;
	  }
	  strcpy (akt_ldat, ldat);
	  if (_adr.adr_typ == 25)
	  {
		  gen_div_adr = TRUE;
          set_fkt (InputAdr, 9);
		  SetFkt (9, adresse, KEY9);
	  }
	  return 0;
}

int testkommdat (void)
/**
Test, ob ein abweichendes Kommisionierdatum eingegeben werden soll.
**/
{

	  if (syskey != KEYCR) return 0;
      set_fkt (NULL, 9);
	  SetFkt (9, leer, NULL);
	  if (abw_komm == -1)
      {
               abw_komm = 0;
               memcpy (&sys_par, &sys_par_null, 
                   sizeof (struct SYS_PAR));
               strcpy (sys_par.sys_par_nam,"abw_komm");
               if (sys_par_class.dbreadfirst () == 0)
               {
                       if (atoi (sys_par.sys_par_wrt)) 
                       {
                             abw_komm = 1;
                       }
               }
      }

      if (abw_komm)
      {
		  Text Kdat = kdat;
		  Kdat.Trim ();
		  if (Kdat == "")
		  {
			strcpy (kdat, ldat);
		  }
          InputKommdat ();
      }
	  else
	  {
		   strcpy (kdat, ldat);
	  }
	  testfixdat ();
//	  EnterKommissionierer ();
      return 0;
}

int testfixdat (void)
/**
Test, ob ein abweichendes Fixdatum eingegeben werden soll.
**/
{

	  if (syskey != KEYCR) return 0;
      set_fkt (NULL, 9);
	  SetFkt (9, leer, NULL);
/*
	  if (abw_fic == -1)
      {
               abw_komm = 0;
               memcpy (&sys_par, &sys_par_null, 
                   sizeof (struct SYS_PAR));
               strcpy (sys_par.sys_par_nam,"abw_komm");
               if (sys_par_class.dbreadfirst () == 0)
               {
                       if (atoi (sys_par.sys_par_wrt)) 
                       {
                             abw_komm = 1;
                       }
               }
      }
*/

      if (FixDat)
      {
		  Text Fixdat = fix_dat;
		  Fixdat.Trim ();
		  if (Fixdat == "")
		  {
			strcpy (fix_dat, ldat);
		  }
          InputFixdat ();
      }
	  else
	  {
		   strcpy (fix_dat, ldat);
	  }
      return 0;
}



int testhtext (void)
{
      if (testkeys ()) return 0;

      set_fkt (SwitchArt, 10);
      SetFkt (10, aktart, KEY10);
      aufpListe.SetSchirm ();
      return 0;
}




//LAC-9 A
static BOOL MustReload = FALSE;
static BOOL ReiterAktAuf = FALSE;

static BOOL ReiterBestellvorschlag = FALSE;

static BOOL ReiterletzteBestellung = FALSE;

static BOOL ReiterKritischeMhd = FALSE;
static BOOL Reiterkommldat = FALSE;

static BOOL ReiterHWG1 = FALSE;

static BOOL ReiterHWG2 = FALSE;

static BOOL ReiterHWG3 = FALSE;

static BOOL ReiterHWG4 = FALSE;


static BOOL ReiterHWG5 = FALSE;

static BOOL ReiterHWG6 = FALSE;

static BOOL ReiterHWG7 = FALSE;

static BOOL ReiterHWG8 = FALSE;

static BOOL ReiterHWG9 = FALSE;

static BOOL ReiterHWG10 = FALSE;
static BOOL ReiterHWG11 = FALSE;
static BOOL ReiterHWG12 = FALSE;
static BOOL ReiterHWG13 = FALSE;
static BOOL ReiterHWG14 = FALSE;
static BOOL ReiterHWG15 = FALSE;
static BOOL ReiterHWG16 = FALSE;

static BOOL SchonDaGewesen = FALSE;

int dhwg1 = 0;
int dhwg2 = 0;
int dhwg3 = 0;
int dhwg4 = 0;
int dhwg5 = 0;
int dhwg6 = 0;
int dhwg7 = 0;
int dhwg8 = 0;
int dhwg9 = 0;
int dhwg10 = 0;
int dhwg11 = 0;
int dhwg12 = 0;
int dhwg13 = 0;
int dhwg14 = 0;
int dhwg15 = 0;
int dhwg16 = 0;

int dwg1 = 0;
int dwg2 = 0;
int dwg3 = 0;
int dwg4 = 0;
int dwg5 = 0;
int dwg6 = 0;
int dwg7 = 0;
int dwg8 = 0;
int dwg9 = 0;
int dwg10 = 0;
int dwg11 = 0;
int dwg12 = 0;
int dwg13 = 0;
int dwg14 = 0;
int dwg15 = 0;
int dwg16 = 0;
int dWahlLetzteBestellung = 0;
int dWahlBestellvorschlag = 0;

static int LaengeAktAuf = 17;
static int LaengeBestellvorschlag = 17;
static int LaengeletzteBestellung = 17;
static int LaengeKritischeMhd = 17; //LAC-84
static int Laengekommldat = 17; //LAC-116
static int LaengeHWG1 = 17;
static int LaengeHWG2 = 17;
static int LaengeHWG3 = 17;
static int LaengeHWG4 = 17;
static int LaengeHWG5 = 17;
static int LaengeHWG6 = 17;
static int LaengeHWG7 = 17;
static int LaengeHWG8 = 17;
static int LaengeHWG9 = 17;
static int LaengeHWG10 = 17;
static int LaengeHWG11 = 17;
static int LaengeHWG12 = 17;
static int LaengeHWG13 = 17;
static int LaengeHWG14 = 17;
static int LaengeHWG15 = 17;
static int LaengeHWG16 = 17;

static int PosAktAuf = 0;
static int PosBestellvorschlag = LaengeAktAuf + PosAktAuf;
static int PosletzteBestellung = LaengeBestellvorschlag+ PosBestellvorschlag;

static int PosKritischeMhd = PosBestellvorschlag;  //LAC-84
static int Poskommldat = PosletzteBestellung;  //LAC-116

static int PosHWG1 = LaengeletzteBestellung + PosletzteBestellung;
static int PosHWG2 = LaengeHWG1+ PosHWG1; 
static int PosHWG3 = LaengeHWG2+ PosHWG2; 
static int PosHWG4 = LaengeHWG3+ PosHWG3; 
static int PosHWG5 = LaengeHWG4+ PosHWG4; 
static int PosHWG6 = LaengeHWG5+ PosHWG5; 
static int PosHWG7 = LaengeHWG6+ PosHWG6; 
static int PosHWG8 = LaengeHWG7+ PosHWG7; 
static int PosHWG9 =  PosHWG1; 
static int PosHWG10 = PosHWG2;   //LAC-87
static int PosHWG11 = PosHWG3;
static int PosHWG12 = PosHWG4;
static int PosHWG13 = PosHWG5;
static int PosHWG14 = PosHWG6;
static int PosHWG15 = PosHWG7;
static int PosHWG16 = PosHWG8;


static char txtAktiverAuftrag [21] =   " aktiver Auftrag" ;
static char txtBestellvorschlag [21] = " Bestellvorschlag" ;
static char txtletzteBestellung [21] = " letzte Bestellung" ;
static char txtletzteBestellung0 [21] = " letzte Bestellung" ;
static char txtKritischeMhd [19] = " kritische MHD" ; //LAC-84
static char txtkommldat [19] = " Lieferung" ; //LAC-116
static char txtHWG1 [25] =             " HWG1              " ;
static char txtHWG2 [20] =             " HWG2              " ;
static char txtHWG3 [20] =             " HWG3              " ;
static char txtHWG4 [20] =             " HWG4              " ;
static char txtHWG5 [20] =             " HWG5              " ;
static char txtHWG6 [20] =             " HWG6              " ;
static char txtHWG7 [20] =             " HWG7              " ;
static char txtHWG8 [20] =             " HWG8              " ;
static char txtHWG9 [20] =             " HWG9              " ;
static char txtHWG10[20] =             " HWG10             " ;
static char txtHWG11[20] =             " HWG11             " ;
static char txtHWG12[20] =             " HWG12             " ;
static char txtHWG13[20] =             " HWG13             " ;
static char txtHWG14[20] =             " HWG14             " ;
static char txtHWG15[20] =             " HWG15             " ;
static char txtHWG16[20] =             " HWG16             " ;



static ColButton CReiterAktAuf   = { txtAktiverAuftrag,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterBestellvorschlag   = { txtBestellvorschlag,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};

static ColButton CReiterletzteBestellung   = { txtletzteBestellung,    1, -1,   
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};//LAC-84

static ColButton CReiterKritischeMhd   = { txtKritischeMhd,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};//LAC-84


static ColButton CReiterkommldat   = { txtkommldat,    1, -1,    //LAC-116
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};

static ColButton CReiterHWG1   = { txtHWG1,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG2   = { txtHWG2,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG3   = { txtHWG3,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG4   = { txtHWG4,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG5   = { txtHWG5,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG6   = { txtHWG6,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG7   = { txtHWG7,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG8   = { txtHWG8,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG9   = { txtHWG9,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG10   = { txtHWG10,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG11   = { txtHWG11,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG12   = { txtHWG12,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG13   = { txtHWG13,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG14   = { txtHWG14,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG15   = { txtHWG15,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};
static ColButton CReiterHWG16   = { txtHWG16,    1, -1, 
                             NULL,  0, 0,
                             NULL,  0, 0,
                             NULL, 110, 0,
                             NULL,  0, 0,
                             BLUECOL,
                             LTGRAYCOL,
							 1,
};


CFIELD *iReiterAktauf =  new CFIELD ("ReiterAktauf", (ColButton *) &CReiterAktAuf, LaengeAktAuf, 0, PosAktAuf, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERAKTAUF, 0, 0, TRANSPARENT);
CFIELD *iReiterBestellvorschlag =  new CFIELD ("ReiterBestellvorschlag", (ColButton *) &CReiterBestellvorschlag, LaengeBestellvorschlag, 0, PosBestellvorschlag, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERBESTELLVORSCHLAG, 0, 0, TRANSPARENT);

CFIELD *iReiterletzteBestellung =  new CFIELD ("ReiterletzteBestellung", (ColButton *) &CReiterletzteBestellung, LaengeletzteBestellung, 0, PosletzteBestellung, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERLETZTEBESTELLUNG, 0, 0, TRANSPARENT);

CFIELD *iReiterKritischeMhd =  new CFIELD ("ReiterkritischeMhd", (ColButton *) &CReiterKritischeMhd, LaengeKritischeMhd, 0, PosKritischeMhd, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERKRITISCHEMHD, 0, 0, TRANSPARENT);   //LAC-84


CFIELD *iReiterkommldat =  new CFIELD ("Reiterkommldat", (ColButton *) &CReiterkommldat, Laengekommldat, 0, Poskommldat, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERKOMMLDAT, 0, 0, TRANSPARENT); //LAC-116

CFIELD *iReiterHWG1 =  new CFIELD ("ReiterHWG1", (ColButton *) &CReiterHWG1, LaengeHWG1, 0, PosHWG1, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG1, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG2 =  new CFIELD ("ReiterHWG2", (ColButton *) &CReiterHWG2, LaengeHWG2, 0, PosHWG2, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG2, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG3 =  new CFIELD ("ReiterHWG3", (ColButton *) &CReiterHWG3, LaengeHWG3, 0, PosHWG3, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG3, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG4 =  new CFIELD ("ReiterHWG4", (ColButton *) &CReiterHWG4, LaengeHWG4, 0, PosHWG4, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG4, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG5 =  new CFIELD ("ReiterHWG5", (ColButton *) &CReiterHWG5, LaengeHWG5, 0, PosHWG5, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG5, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG6 =  new CFIELD ("ReiterHWG6", (ColButton *) &CReiterHWG6, LaengeHWG6, 0, PosHWG6, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG6, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG7 =  new CFIELD ("ReiterHWG7", (ColButton *) &CReiterHWG7, LaengeHWG7, 0, PosHWG7, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG7, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG8 =  new CFIELD ("ReiterHWG8", (ColButton *) &CReiterHWG8, LaengeHWG8, 0, PosHWG8, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG8, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG9 =  new CFIELD ("ReiterHWG9", (ColButton *) &CReiterHWG9, LaengeHWG9, 0, PosHWG9, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG9, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG10 =  new CFIELD ("ReiterHWG10", (ColButton *) &CReiterHWG10, LaengeHWG10, 0, PosHWG10, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG10, 0, 0, TRANSPARENT);

CFIELD *iReiterHWG11 =  new CFIELD ("ReiterHWG11", (ColButton *) &CReiterHWG11, LaengeHWG11, 0, PosHWG11, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG11, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG12 =  new CFIELD ("ReiterHWG12", (ColButton *) &CReiterHWG12, LaengeHWG12, 0, PosHWG12, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG12, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG13 =  new CFIELD ("ReiterHWG13", (ColButton *) &CReiterHWG13, LaengeHWG13, 0, PosHWG13, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG13, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG14 =  new CFIELD ("ReiterHWG14", (ColButton *) &CReiterHWG14, LaengeHWG14, 0, PosHWG14, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG14, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG15 =  new CFIELD ("ReiterHWG15", (ColButton *) &CReiterHWG15, LaengeHWG15, 0, PosHWG15, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG15, 0, 0, TRANSPARENT);
CFIELD *iReiterHWG16 =  new CFIELD ("ReiterHWG16", (ColButton *) &CReiterHWG16, LaengeHWG16, 0, PosHWG16, 0,  NULL, "", 
                                 CCOLBUTTON,
                                 REITERHWG16, 0, 0, TRANSPARENT);
// LAC-9 E



//LAC-9 A
void DeaktiviereReiterVariablen (void) //LAC-9
{
		 ReiterAktAuf = FALSE;
		 ReiterBestellvorschlag = FALSE;
		 ReiterletzteBestellung = FALSE;
		 ReiterKritischeMhd = FALSE; //LAC-84
		 Reiterkommldat = FALSE;//LAC-116
		 ReiterHWG1 = FALSE;
		 ReiterHWG2 = FALSE;
		 ReiterHWG3 = FALSE;
		 ReiterHWG4 = FALSE;
		 ReiterHWG5 = FALSE;
		 ReiterHWG6 = FALSE;
		 ReiterHWG7 = FALSE;
		 ReiterHWG8 = FALSE;
		 ReiterHWG9 = FALSE;
		 ReiterHWG10 = FALSE;
		 ReiterHWG11 = FALSE;
		 ReiterHWG12 = FALSE;
		 ReiterHWG13 = FALSE;
		 ReiterHWG14 = FALSE;
		 ReiterHWG15 = FALSE;
		 ReiterHWG16 = FALSE;

         CReiterAktAuf.bmp = NULL;
         CReiterBestellvorschlag.bmp = NULL;
         CReiterletzteBestellung.bmp = NULL;
         CReiterKritischeMhd.bmp = NULL; //LAC-84
         CReiterkommldat.bmp = NULL;//LAC-116
         CReiterHWG1.bmp = NULL;
         CReiterHWG2.bmp = NULL;
         CReiterHWG3.bmp = NULL;
         CReiterHWG4.bmp = NULL;
         CReiterHWG5.bmp = NULL;
         CReiterHWG6.bmp = NULL;
         CReiterHWG7.bmp = NULL;
         CReiterHWG8.bmp = NULL;
         CReiterHWG9.bmp = NULL;
         CReiterHWG10.bmp = NULL;
         CReiterHWG11.bmp = NULL;
         CReiterHWG12.bmp = NULL;
         CReiterHWG13.bmp = NULL;
         CReiterHWG14.bmp = NULL;
         CReiterHWG15.bmp = NULL;
         CReiterHWG16.bmp = NULL;

         CReiterAktAuf.Color = BLUECOL;
         CReiterBestellvorschlag.Color = BLUECOL;
         CReiterletzteBestellung.Color = BLUECOL;
         CReiterKritischeMhd.Color = BLUECOL; //LAC-84
         CReiterkommldat.Color = BLUECOL;//LAC-116
         CReiterHWG1.Color = BLUECOL;
         CReiterHWG2.Color = BLUECOL;
         CReiterHWG3.Color = BLUECOL;
         CReiterHWG4.Color = BLUECOL;
         CReiterHWG5.Color = BLUECOL;
         CReiterHWG6.Color = BLUECOL;
         CReiterHWG7.Color = BLUECOL;
         CReiterHWG8.Color = BLUECOL;
         CReiterHWG9.Color = BLUECOL;
         CReiterHWG10.Color = BLUECOL;
         CReiterHWG11.Color = BLUECOL;
         CReiterHWG12.Color = BLUECOL;
         CReiterHWG13.Color = BLUECOL;
         CReiterHWG14.Color = BLUECOL;
         CReiterHWG15.Color = BLUECOL;
         CReiterHWG16.Color = BLUECOL;
}



int SetReiterAktAuf (void) 
{
     strcpy(CReiterAktAuf.text1,txtAktiverAuftrag);
     CReiterAktAuf.tx1 = 1;
     CReiterAktAuf.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterAktAuf) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterAktAuf = TRUE;
	    CReiterAktAuf.Color = REITERCOL;
        CReiterAktAuf.bmp = SelBmp;
		DisplayReiter ();
		FillCombo3 (1);
		FillCombo4 (1);
return TRUE;
}
int SetReiterBestellvorschlag (void)
{
     strcpy(CReiterBestellvorschlag.text1,txtBestellvorschlag);
     CReiterBestellvorschlag.tx1 = 1;
     CReiterBestellvorschlag.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterBestellvorschlag) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterBestellvorschlag = TRUE;
	    CReiterBestellvorschlag.Color = REITERCOL;
        CReiterBestellvorschlag.bmp = SelBmp;
//LAC-156 A
		Write_Bestellvorschlag ();
		int danz = cfg_anz_wo_Bestellvorschlag * -1;
		hwg_class.SetAktLieferdat (aufk.lieferdat);
        dWahlBestellvorschlag = hwg_class.ChoiseWg (aufk.kun, PosBestellvorschlag, aufpListe.GetZeileLeiste(),danz, 1);
        strncpy(CReiterBestellvorschlag.text1,hwg_class.GettxtHWG (20), 20);
	    CReiterBestellvorschlag.text1[20] = 0;
//LAC-156 E

		DisplayReiter ();
		FillCombo3 (2);
		FillCombo4 (2);
return TRUE;
}

int SetReiterletzteBestellung (void)
{
     strcpy(CReiterletzteBestellung.text1,txtletzteBestellung);
     CReiterletzteBestellung.tx1 = 1;
     CReiterletzteBestellung.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterletzteBestellung ) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterletzteBestellung = TRUE;
	    CReiterletzteBestellung.Color = REITERCOL;
        CReiterletzteBestellung.bmp = SelBmp;
//LAC-132 A
		int danz = cfg_anz_wo_letzteBestellung * -1;
		hwg_class.SetAktLieferdat (aufk.lieferdat);
        dWahlLetzteBestellung = hwg_class.ChoiseWg (aufk.kun, PosletzteBestellung, aufpListe.GetZeileLeiste(),danz,2);
		aufpListe.KillProcessWgChart ();
		aufpListe.StarteProcessWgChart (dWahlLetzteBestellung);
        strncpy(CReiterletzteBestellung.text1,hwg_class.GettxtHWG (0), 20);
	    CReiterletzteBestellung.text1[20] = 0;
//LAC-132 E

		DisplayReiter ();
		FillCombo3 (3);
		FillCombo4 (3);
return TRUE;
}


int SetReiterKritischeMhd (void) //LAC-84
{
     strcpy(CReiterKritischeMhd.text1,txtKritischeMhd);
     CReiterKritischeMhd.tx1 = 1;
     CReiterKritischeMhd.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterKritischeMhd ) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterKritischeMhd = TRUE;
	    CReiterKritischeMhd.Color = REITERCOL;
        CReiterKritischeMhd.bmp = SelBmp;
		DisplayReiter ();
		FillCombo3 (5);
		FillCombo4 (5);
return TRUE;
}

int SetReiterkommldat (void)//LAC-116
{
     strcpy(CReiterkommldat.text1,txtkommldat);
     CReiterkommldat.tx1 = 1;
     CReiterkommldat.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (Reiterkommldat ) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    Reiterkommldat = TRUE;
	    CReiterkommldat.Color = REITERCOL;
        CReiterkommldat.bmp = SelBmp;
		DisplayReiter ();
		FillCombo3 (6);
		FillCombo4 (6);
return TRUE;
}

int SetReiterHWG1 (void) //LAC-9
{
     strcpy(CReiterHWG1.text1,txtHWG1);
     CReiterHWG1.tx1 = 1;
     CReiterHWG1.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG1 = TRUE;
	    CReiterHWG1.Color = REITERCOL;
        CReiterHWG1.bmp = SelBmp;
        dwg1 = hwg_class.ChoiseWg (dhwg1, PosHWG1, aufpListe.GetZeileLeiste(),1,0);
		aufpListe.SetWgLeiste (dhwg1,dwg1);
        strncpy(CReiterHWG1.text1,hwg_class.GettxtHWG (1), 20);
	    CReiterHWG1.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}

int SetReiterHWG2 (void) //LAC-9
{
     strcpy(CReiterHWG2.text1,txtHWG2);
     CReiterHWG2.tx1 = 1;
     CReiterHWG2.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG2 = TRUE;
	    CReiterHWG2.Color = REITERCOL;
        CReiterHWG2.bmp = SelBmp;
        dwg2 = hwg_class.ChoiseWg (dhwg2,PosHWG2, aufpListe.GetZeileLeiste(),2,0);
		aufpListe.SetWgLeiste (dhwg2,dwg2);
        strncpy(CReiterHWG2.text1,hwg_class.GettxtHWG (2), 20);
	    CReiterHWG2.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}

int SetReiterHWG3 (void) //LAC-9
{
     strcpy(CReiterHWG3.text1,txtHWG3);
     CReiterHWG3.tx1 = 1;
     CReiterHWG3.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG3 = TRUE;
	    CReiterHWG3.Color = REITERCOL;
        CReiterHWG3.bmp = SelBmp;
		dwg3 = hwg_class.ChoiseWg (dhwg3,PosHWG3, aufpListe.GetZeileLeiste(),3,0);
		aufpListe.SetWgLeiste (dhwg3,dwg3);
        strncpy(CReiterHWG3.text1,hwg_class.GettxtHWG (3), 20);
	    CReiterHWG3.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}

int SetReiterHWG4 (void) //LAC-9
{

     strcpy(CReiterHWG4.text1,txtHWG4);
     CReiterHWG4.tx1 = 1;
     CReiterHWG4.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG4 = TRUE;
	    CReiterHWG4.Color = REITERCOL;
        CReiterHWG4.bmp = SelBmp;
        dwg4 = hwg_class.ChoiseWg (dhwg4,PosHWG4, aufpListe.GetZeileLeiste(),4,0);
		aufpListe.SetWgLeiste (dhwg4,dwg4);
        strncpy(CReiterHWG4.text1,hwg_class.GettxtHWG (4), 20);
	    CReiterHWG4.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}
int SetReiterHWG5 (void) //LAC-9
{

     strcpy(CReiterHWG5.text1,txtHWG5);
     CReiterHWG5.tx1 = 1;
     CReiterHWG5.BkColor = LTGRAYCOL;
     CReiterHWG5.aktivate = FALSE;


        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG5 = TRUE;
	    CReiterHWG5.Color = REITERCOL;
        CReiterHWG5.bmp = SelBmp;
        dwg5 = hwg_class.ChoiseWg (dhwg5,PosHWG5, aufpListe.GetZeileLeiste(),5,0);
		aufpListe.SetWgLeiste (dhwg5,dwg5);
        strncpy(CReiterHWG5.text1,hwg_class.GettxtHWG (5), 20);
	    CReiterHWG5.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}
int SetReiterHWG6 (void) //LAC-9
{
     strcpy(CReiterHWG6.text1,txtHWG6);
     CReiterHWG6.tx1 = 1;
     CReiterHWG6.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG6 = TRUE;
	    CReiterHWG6.Color = REITERCOL;
        CReiterHWG6.bmp = SelBmp;
        dwg6 = hwg_class.ChoiseWg (dhwg6,PosHWG6, aufpListe.GetZeileLeiste(),6,0);
		aufpListe.SetWgLeiste (dhwg6,dwg6);
        strncpy(CReiterHWG6.text1,hwg_class.GettxtHWG (6), 20);
	    CReiterHWG6.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}
int SetReiterHWG7 (void) //LAC-9
{
     strcpy(CReiterHWG7.text1,txtHWG7);
     CReiterHWG7.tx1 = 1;
     CReiterHWG7.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG7 = TRUE;
	    CReiterHWG7.Color = REITERCOL;
        CReiterHWG7.bmp = SelBmp;
        dwg7 = hwg_class.ChoiseWg (dhwg7,PosHWG7, aufpListe.GetZeileLeiste(),7,0);
		aufpListe.SetWgLeiste (dhwg7,dwg7);
        strncpy(CReiterHWG7.text1,hwg_class.GettxtHWG (7), 20);
	    CReiterHWG7.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}
int SetReiterHWG8 (void) //LAC-9
{
     strcpy(CReiterHWG8.text1,txtHWG8);
     CReiterHWG8.tx1 = 1;
     CReiterHWG8.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG8 = TRUE;
	    CReiterHWG8.Color = REITERCOL;
        CReiterHWG8.bmp = SelBmp;
        dwg8 = hwg_class.ChoiseWg (dhwg8,PosHWG8, aufpListe.GetZeileLeiste(),8,0);
		aufpListe.SetWgLeiste (dhwg8,dwg8);
        strncpy(CReiterHWG8.text1,hwg_class.GettxtHWG (8), 20);
	    CReiterHWG8.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}
int SetReiterHWG9 (void) //LAC-9
{
     strcpy(CReiterHWG9.text1,txtHWG9);
     CReiterHWG9.tx1 = 1;
     CReiterHWG9.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG9 = TRUE;
	    CReiterHWG9.Color = REITERCOL;
        CReiterHWG9.bmp = SelBmp;
        dwg9 = hwg_class.ChoiseWg (dhwg9,PosHWG9, aufpListe.GetZeileLeiste(),9,0);
		aufpListe.SetWgLeiste (dhwg9,dwg9);
        strncpy(CReiterHWG9.text1,hwg_class.GettxtHWG (9), 20);
	    CReiterHWG9.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}
int SetReiterHWG10 (void) //LAC-9
{
     strcpy(CReiterHWG10.text1,txtHWG10);
     CReiterHWG10.tx1 = 1;
     CReiterHWG10.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG10 = TRUE;
	    CReiterHWG10.Color = REITERCOL;
        CReiterHWG10.bmp = SelBmp;
        dwg10 = hwg_class.ChoiseWg (dhwg10,PosHWG10, aufpListe.GetZeileLeiste(),10,0);
		aufpListe.SetWgLeiste (dhwg10,dwg10);
        strncpy(CReiterHWG10.text1,hwg_class.GettxtHWG (10), 20);
	    CReiterHWG10.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}

int SetReiterHWG11 (void) 
{
     strcpy(CReiterHWG11.text1,txtHWG11);
     CReiterHWG11.tx1 = 1;
     CReiterHWG11.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG11 = TRUE;
	    CReiterHWG11.Color = REITERCOL;
        CReiterHWG11.bmp = SelBmp;
        dwg11 = hwg_class.ChoiseWg (dhwg11,PosHWG11, aufpListe.GetZeileLeiste(),11,0);
		aufpListe.SetWgLeiste (dhwg11,dwg11);
        strncpy(CReiterHWG11.text1,hwg_class.GettxtHWG (11), 20);
	    CReiterHWG11.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}

int SetReiterHWG12 (void) 
{
     strcpy(CReiterHWG12.text1,txtHWG12);
     CReiterHWG12.tx1 = 1;
     CReiterHWG12.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG12 = TRUE;
	    CReiterHWG12.Color = REITERCOL;
        CReiterHWG12.bmp = SelBmp;
        dwg12 = hwg_class.ChoiseWg (dhwg12,PosHWG12, aufpListe.GetZeileLeiste(),12,0);
		aufpListe.SetWgLeiste (dhwg12,dwg12);
        strncpy(CReiterHWG12.text1,hwg_class.GettxtHWG (12), 20);
	    CReiterHWG12.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}

int SetReiterHWG13 (void) 
{
     strcpy(CReiterHWG13.text1,txtHWG13);
     CReiterHWG13.tx1 = 1;
     CReiterHWG13.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG13 = TRUE;
	    CReiterHWG13.Color = REITERCOL;
        CReiterHWG13.bmp = SelBmp;
        dwg13 = hwg_class.ChoiseWg (dhwg13,PosHWG13, aufpListe.GetZeileLeiste(),13,0);
		aufpListe.SetWgLeiste (dhwg13,dwg13);
        strncpy(CReiterHWG13.text1,hwg_class.GettxtHWG (13), 20);
	    CReiterHWG13.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}

int SetReiterHWG14 (void) 
{
     strcpy(CReiterHWG14.text1,txtHWG14);
     CReiterHWG14.tx1 = 1;
     CReiterHWG14.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG14 = TRUE;
	    CReiterHWG14.Color = REITERCOL;
        CReiterHWG14.bmp = SelBmp;
        dwg14 = hwg_class.ChoiseWg (dhwg14,PosHWG14, aufpListe.GetZeileLeiste(),14,0);
		aufpListe.SetWgLeiste (dhwg14,dwg14);
        strncpy(CReiterHWG14.text1,hwg_class.GettxtHWG (14), 20);
	    CReiterHWG14.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}

int SetReiterHWG15 (void) 
{
     strcpy(CReiterHWG15.text1,txtHWG15);
     CReiterHWG15.tx1 = 1;
     CReiterHWG15.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG15 = TRUE;
	    CReiterHWG15.Color = REITERCOL;
        CReiterHWG15.bmp = SelBmp;
        dwg15 = hwg_class.ChoiseWg (dhwg15,PosHWG15, aufpListe.GetZeileLeiste(),15,0);
		aufpListe.SetWgLeiste (dhwg15,dwg15);
        strncpy(CReiterHWG10.text1,hwg_class.GettxtHWG (15), 20);
	    CReiterHWG15.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}

int SetReiterHWG16 (void) 
{
     strcpy(CReiterHWG16.text1,txtHWG16);
     CReiterHWG16.tx1 = 1;
     CReiterHWG16.BkColor = LTGRAYCOL;

        MustReload = TRUE;
		if (ReiterHWG1 || ReiterHWG2 || ReiterHWG3 || ReiterHWG4 || ReiterHWG5 || ReiterHWG6 || ReiterHWG7 || ReiterHWG8 || ReiterHWG9 || ReiterHWG10 || ReiterHWG11 || ReiterHWG12 || ReiterHWG13 || ReiterHWG14 || ReiterHWG15 || ReiterHWG16) MustReload = FALSE;
        DeaktiviereReiterVariablen ();
	    ReiterHWG16 = TRUE;
	    CReiterHWG16.Color = REITERCOL;
        CReiterHWG16.bmp = SelBmp;
        dwg16 = hwg_class.ChoiseWg (dhwg16,PosHWG16, aufpListe.GetZeileLeiste(),16,0);
		aufpListe.SetWgLeiste (dhwg16,dwg16);
        strncpy(CReiterHWG16.text1,hwg_class.GettxtHWG (16), 20);
	    CReiterHWG16.text1[20] = 0;
		DisplayReiter ();
		FillCombo3 (4);
		FillCombo4 (4);

return TRUE;
}


//LAC-9 E



BOOL ReiterMenue (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam) //LAC-9
{
	if (SchonDaGewesen == TRUE)  //nur aus Verzweifelung, weil der gleiche KEY 2 * reinkommt
	{
		SchonDaGewesen = FALSE;
		return TRUE;
	}

        if (LOWORD (wParam) == KEY5)
        {
            SendMessage (hMainWindow, WM_COMMAND, KEY5, 0l);
            return TRUE;
        }

        if (LOWORD (wParam) == IDM_MUSTER)
        {
            SetMusterAuftrag ();
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_TOURHINWEIS)
        {
            StartTourhinweis ();
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_STORNO)
        {
            SetStornoAuftrag ();
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_LS)
        {
            StartLsProc ();
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_NACHDRUCK)
        {
            QueryNachDruck ();
            return TRUE;
        }

        if (LOWORD (wParam) != REITERLETZTEBESTELLUNG) //LAC-132
        {
			if (aufpListe.GetLetzteBestellung_aktiv() == TRUE)
			{
				aufpListe.KillProcessWgChart ();
				aufpListe.StarteProcessWgChart (aufk.auf);
			}
		}
        if (LOWORD (wParam) == REITERAKTAUF)
        {
							aufpListe.SetSpaltenAktAuf ();
							SetReiterAktAuf ();
							if (MustReload)
							{
								aufpListe.doreload (0,0,1);
							}
							else
							{
								aufpListe.doAktAuf ();
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERBESTELLVORSCHLAG)
        {
							aufpListe.SetSpaltenBestellvorschlag ();
							SetReiterBestellvorschlag ();
							if (MustReload)
							{
								aufpListe.doreload (0,dWahlBestellvorschlag,2);
							}
							else
							{
									aufpListe.doBestellvorschlag (dWahlBestellvorschlag);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERLETZTEBESTELLUNG)
        {
							aufpListe.SetSpaltenletzteBestellung ();
							SetReiterletzteBestellung ();
							if (MustReload)
							{
								aufpListe.doreload (0,dWahlLetzteBestellung,3);
							}
							else
							{
								aufpListe.doletzteBestellung (dWahlLetzteBestellung); //LAC-132
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERKRITISCHEMHD) //LAC-84
        {
							aufpListe.SetSpaltenKritischeMhd ();
							SetReiterKritischeMhd ();
							if (MustReload)
							{
								aufpListe.doreload (0,0,4);
							}
							else
							{
								aufpListe.doKritischeMhd ();
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERKOMMLDAT) //LAC-116
        {
							aufpListe.SetSpaltenkommldat ();
							SetReiterkommldat ();
							if (MustReload)
							{
								aufpListe.doreload (0,0,5);
							}
							else
							{
								aufpListe.dokommldat ();
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG1)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG1 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg1,dwg1,1);
							}
							else
							{
								aufpListe.doHWG (dhwg1,dwg1,1);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG2)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG2 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg2,dwg2,2);
							}
							else
							{
								 aufpListe.doHWG (dhwg2,dwg2,2);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG3)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG3 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg3,dwg3,3);
							}
							else
							{
								aufpListe.doHWG (dhwg3,dwg3,3);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG4)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG4 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg4,dwg4,4);
							}
							else
							{
								aufpListe.doHWG (dhwg4,dwg4,4);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG5)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG5 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg5,dwg5,5);
							}
							else
							{
								aufpListe.doHWG (dhwg5,dwg5,5);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG6)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG6 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg6,dwg6,6);
							}
							else
							{
								aufpListe.doHWG (dhwg6,dwg6,6);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG7)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG7 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg7,dwg7,7);
							}
							else
							{
								aufpListe.doHWG (dhwg7,dwg7,7);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG8)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG8 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg8,dwg8,8);
							}
							else
							{
								aufpListe.doHWG (dhwg8,dwg8,8);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG9)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG9 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg9,dwg9,9);
							}
							else
							{
								aufpListe.doHWG (dhwg9,dwg9,9);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG10)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG10 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg10,dwg10,10);
							}
							else
							{
								aufpListe.doHWG (dhwg10,dwg10,10);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }

        if (LOWORD (wParam) == REITERHWG11)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG11 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg11,dwg11,11);
							}
							else
							{
								aufpListe.doHWG (dhwg11,dwg11,11);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG12)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG12 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg12,dwg12,12);
							}
							else
							{
								aufpListe.doHWG (dhwg12,dwg12,12);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG13)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG13 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg13,dwg13,13);
							}
							else
							{
								aufpListe.doHWG (dhwg13,dwg13,13);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG14)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG14 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg14,dwg14,14);
							}
							else
							{
								aufpListe.doHWG (dhwg14,dwg14,14);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG15)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG15 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg15,dwg15,15);
							}
							else
							{
								aufpListe.doHWG (dhwg15,dwg15,15);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }
        if (LOWORD (wParam) == REITERHWG16)
        {
							aufpListe.SetSpaltenHWG ();
							SetReiterHWG16 ();
							if (MustReload)
							{
								aufpListe.doreload (dhwg16,dwg16,16);
							}
							else
							{
								aufpListe.doHWG (dhwg16,dwg16,16);
							}
							SchonDaGewesen = TRUE; 
            return TRUE;
        }

        return FALSE;
}

void HoleHWG (void) //LAC-9
{
		   int cursor;
		   int i;
		   short hwg;    //lac-119  aus int -> short   kleine Ursache , gro�e Wirkung
		   char hwg_bz1 [25];
			DB_CLASS DbClass;


		   DbClass.sqlout ((short *) &hwg, 1, 0);
		   DbClass.sqlout ((char *)   hwg_bz1, 0, 25); 
		   cursor = DbClass.sqlcursor ("select hwg, hwg_bz1 from hwg "
//			                   "where hwg > 0  order by hwg");
			                   "where hwg > 0 and hwg in (select hwg from hwg_best_wk where prozent > 0 and qm > 0 ) order by hwg");

		   i = 1;
		   while (DbClass.sqlfetch (cursor) == 0)
		   {
			   if (i == 1) 
			   {
				   dhwg1 = hwg;
				   strncpy (txtHWG1,hwg_bz1, 20);
				   LaengeHWG1 = strlen(txtHWG1);
				   if (LaengeHWG1 > 20) LaengeHWG1 = 20;
			       AddReiterButton (iReiterHWG1,1);

			   }
			   if (i == 2) 
			   {
				   dhwg2 = hwg;
				   strncpy (txtHWG2,hwg_bz1, 20);
				   LaengeHWG2 = strlen(hwg_bz1);
				   if (LaengeHWG2 > 20) LaengeHWG2 = 20;
			       AddReiterButton (iReiterHWG2,1);
			   }
			   if (i == 3) 
			   {
				   dhwg3 = hwg;
				   strncpy (txtHWG3,hwg_bz1, 20);
				   LaengeHWG3 = strlen(hwg_bz1);
				   if (LaengeHWG3 > 20) LaengeHWG3 = 20;
			       AddReiterButton (iReiterHWG3,1);
			   }
			   if (i == 4) 
			   {
				   dhwg4 = hwg;
				   strncpy (txtHWG4,hwg_bz1, 20);
				   LaengeHWG4 = strlen(hwg_bz1);
				   if (LaengeHWG4 > 20) LaengeHWG4 = 20;
			       AddReiterButton (iReiterHWG4,1);
			   }
			   if (i == 5) 
			   {
				   dhwg5 = hwg;
				   strncpy (txtHWG5,hwg_bz1, 20);
				   LaengeHWG5 = strlen(hwg_bz1);
				   if (LaengeHWG5 > 20) LaengeHWG5 = 20;
			       AddReiterButton (iReiterHWG5,1);
			   }
			   if (i == 6) 
			   {
				   dhwg6 = hwg;
				   strncpy (txtHWG6,hwg_bz1, 20);
				   LaengeHWG6 = strlen(hwg_bz1);
				   if (LaengeHWG6 > 20) LaengeHWG6 = 20;
			       AddReiterButton (iReiterHWG6,1);
			   }
			   if (i == 7) 
			   {
				   dhwg7 = hwg;
				   strncpy (txtHWG7,hwg_bz1, 20);
				   LaengeHWG7 = strlen(hwg_bz1);
				   if (LaengeHWG7 > 20) LaengeHWG7 = 20;
			       AddReiterButton (iReiterHWG7,1);
			   }
			   if (i == 8) 
			   {
				   dhwg8 = hwg;
				   strncpy (txtHWG8,hwg_bz1, 20);
				   LaengeHWG8 = strlen(hwg_bz1);
				   if (LaengeHWG8 > 20) LaengeHWG8 = 20;
			       AddReiterButton (iReiterHWG8,1);
			   }
			   if (i == 9) 
			   {
				   dhwg9 = hwg;
				   strncpy (txtHWG9,hwg_bz1, 20);
				   LaengeHWG9 = strlen(hwg_bz1);
				   if (LaengeHWG9 > 20) LaengeHWG9 = 20;
			       AddReiterButton (iReiterHWG9,2);
			   }
			   if (i == 10) 
			   {
				   dhwg10 = hwg;
				   strncpy (txtHWG10,hwg_bz1, 20);
				   LaengeHWG10 = strlen(hwg_bz1);
				   if (LaengeHWG10 > 20) LaengeHWG10 = 20;
			       AddReiterButton (iReiterHWG10,2);
			   }
			   if (i == 11) 
			   {
				   dhwg11 = hwg;
				   strncpy (txtHWG11,hwg_bz1, 20);
				   LaengeHWG11 = strlen(hwg_bz1);
				   if (LaengeHWG11 > 20) LaengeHWG11 = 20;
			       AddReiterButton (iReiterHWG11,2);
			   }
			   if (i == 12) 
			   {
				   dhwg12 = hwg;
				   strncpy (txtHWG12,hwg_bz1, 20);
				   LaengeHWG12 = strlen(hwg_bz1);
				   if (LaengeHWG12 > 20) LaengeHWG12 = 20;
			       AddReiterButton (iReiterHWG12,2);
			   }
			   if (i == 13) 
			   {
				   dhwg13 = hwg;
				   strncpy (txtHWG13,hwg_bz1, 20);
				   LaengeHWG13 = strlen(hwg_bz1);
				   if (LaengeHWG13 > 20) LaengeHWG13 = 20;
			       AddReiterButton (iReiterHWG13,2);
			   }
			   if (i == 14) 
			   {
				   dhwg14 = hwg;
				   strncpy (txtHWG14,hwg_bz1, 20);
				   LaengeHWG14 = strlen(hwg_bz1);
				   if (LaengeHWG14 > 20) LaengeHWG14 = 20;
			       AddReiterButton (iReiterHWG14,2);
			   }
			   if (i == 15) 
			   {
				   dhwg15 = hwg;
				   strncpy (txtHWG15,hwg_bz1, 20);
				   LaengeHWG15 = strlen(hwg_bz1);
				   if (LaengeHWG15 > 20) LaengeHWG15 = 20;
			       AddReiterButton (iReiterHWG15,2);
			   }
			   if (i == 16) 
			   {
				   dhwg16 = hwg;
				   strncpy (txtHWG16,hwg_bz1, 20);
				   LaengeHWG16 = strlen(hwg_bz1);
				   if (LaengeHWG16 > 20) LaengeHWG16 = 20;
			       AddReiterButton (iReiterHWG16,2);
			   }
			   i++;
			   if (i > 16) break;
		   }

			PosHWG2 = LaengeHWG1+ PosHWG1; 
			PosHWG3 = LaengeHWG2+ PosHWG2; 
			PosHWG4 = LaengeHWG3+ PosHWG3; 
			PosHWG5 = LaengeHWG4+ PosHWG4; 
			PosHWG6 = LaengeHWG5+ PosHWG5; 
			PosHWG7 = LaengeHWG6+ PosHWG6; 
			PosHWG8 = LaengeHWG7+ PosHWG7; 
			PosHWG9 =  PosHWG1; 
			PosHWG10 = PosHWG2;   //LAC-87
			PosHWG11 = PosHWG3;
			PosHWG12 = PosHWG4;
			PosHWG13 = PosHWG5;
			PosHWG14 = PosHWG6;
			PosHWG15 = PosHWG7;
			PosHWG16 = PosHWG8;
//			fButtonstest.mask[i].length
			DbClass.sqlclose (cursor);
		   for (; i < 17; i ++)
		   {
			   if (i == 1) strcpy (txtHWG1,"");
			   if (i == 2) strcpy (txtHWG2,"");
			   if (i == 3) strcpy (txtHWG3,"");
			   if (i == 4) strcpy (txtHWG4,"");
			   if (i == 5) strcpy (txtHWG5,"");
			   if (i == 6) strcpy (txtHWG6,"");
			   if (i == 7) strcpy (txtHWG7,"");
			   if (i == 8) strcpy (txtHWG8,"");
			   if (i == 9) strcpy (txtHWG9,"");
			   if (i == 10) strcpy (txtHWG10,"");
			   if (i == 11) strcpy (txtHWG11,"");
			   if (i == 12) strcpy (txtHWG12,"");
			   if (i == 13) strcpy (txtHWG13,"");
			   if (i == 14) strcpy (txtHWG14,"");
			   if (i == 15) strcpy (txtHWG15,"");
			   if (i == 16) strcpy (txtHWG16,"");

//			   fleiste.mask[i+2].pos[0] = -1;
//			   fleiste.mask[i+2].pos[1] = 999; 

		   }
		   DbClass.sqlclose (cursor);
}


int doliste ()
{

	       if (ldatdiffOK () == FALSE)
		   {
			   SetCurrentField (currentfield);
			   SetCurrentFocus (currentfield);
			   return 0;
		   }

        if (hMainWindow == NULL) return 0;    

		
	
		//LAC-9 A
        AddReiterButton (iReiterAktauf,1);
        AddReiterButton (iReiterBestellvorschlag,1);
        AddReiterButton (iReiterletzteBestellung,1);
        AddReiterButton (iReiterKritischeMhd,2); //LAC-84
        if (LskVorhanden ()) 
		{
			AddReiterButton (iReiterkommldat,2); //LAC-116
		}



		HoleHWG(); 
		aufpListe.ZeigeLeiste ();
//        AnzLeiste (hMainWindow, hMainInst,15,277,2254,22); //LAC-9test
        SetReiterMenue (ReiterMenue);
		//LAC-9 E
           break_enter ();
           return 0;
}



int SwitchArt (void)
/**
Switch zwischen Kundenartikel-Nummer und eigener Atikelnummer.
**/
{
         if (aktart == kunart)
         {
             aktart = eigart;
             aufpListe.SetFieldAttr ("a_kun", DISPLAYONLY);
             aufpListe.SetChAttr (1);
             aufpListe.SetFieldAttr ("a_bz2", DISPLAYONLY);
         }
         else
         {
             aktart = kunart;
             aufpListe.SetFieldAttr ("a_kun", REMOVED);
             aufpListe.SetChAttr (0);
             aufpListe.SetFieldAttr ("a_bz2", REMOVED);
         }
         SetFkt (10, aktart, KEY10);
         aufpListe.DestroyWindows ();
         aufpListe.ShowAufp (atoi (mdn) , atoi (fil), 
                             atol (auftrag), atoi (kunfil),
                             atol (kuns), ldat);
         return 0;
}


int SwitchZei (void)
/**
Switch zwischen Kundenartikel-Nummer und eigener Atikelnummer.
**/
{
         if (aktzei == a_bz2_on)
         {
             aktzei = a_bz2_off;
             aufpListe.SetFieldAttr ("a_bz2", DISPLAYONLY);
         }
         else
         {
             aktzei = a_bz2_on;
             aufpListe.SetFieldAttr ("a_bz2", REMOVED);
         }
         SetFkt (7, aktzei, KEY7);
         aufpListe.DestroyWindows ();
         aufpListe.ShowAufp (atoi (mdn) , atoi (fil), 
                             atol (auftrag), atoi (kunfil),
                             atol (kuns), ldat);
         return 0;
}


void DelAuf (void)
/**
Auftrag l�schen.
**/
{
           ls_class.delete_auf (atoi (mdn), atoi (fil), aufk.auf);
		   aufpListe.RollbackBsd ();
           ls_class.delete_aufpauf (atoi (mdn), atoi (fil), aufk.auf);
           Mess.Message ("Satz wurde gel�scht");
           return;
}
           
int TouZTest (void)
/**
Testen, ob eine Tour eingegeben werden muss.
**/
{
	   int tou_pos; 
       char *touval;
	   int dsqlstatus;

       if (tour_zwang == FALSE) return TRUE;

       tou_pos    = GetItemPos (&aufform, "tou");
	   GetWindowText (aufform.mask[tou_pos].feldid,
		              aufform.mask[tou_pos].item->GetFeldPtr (),
					  9);
       touval    = aufform.mask[tou_pos].item->GetFeldPtr ();
       tou.tou = atol (touval);
       if (tou.tou < (long) tour_div)
       {
           tou.tou *= tour_div;
       }
       sprintf (touval, "%ld", tou.tou);
       tou.tou /= tour_div;
       if (tou.tou == 0l)
       {
                 dsqlstatus = 100;;
       }
	   else
	   {
                 dsqlstatus = Tour.dbreadfirst ();
	   }
	   if (dsqlstatus == 0) return TRUE;

	   disp_mess ("Eine Tour-Nr muss eingegeben werden", 2);
       SetCurrentField (tou_pos);
       SetCurrentFocus (tou_pos);
	   return FALSE;
}


int dokey12 ()
/**
Taste Key12 behandeln.
**/
{
  		   akt_tou = 0l;
           if (mench == 0)
           {
					//LAC-190 beginwork (); //LAC-167
					//LAC-190 ls_class.Commit_Work (); //LAC-167
	             if (ldatdiffOK () == FALSE)
				 {
					 SetCurrentField (currentfield);
					 SetCurrentFocus (currentfield);
		             return 1;
				 }
                 if (bg_cc_par && zustell_par && 
                     getaufart () == -1)
                 {
                     print_mess (2, "Falsche Auftragsart");
					 SetCurrentField (currentfield);
					 SetCurrentFocus (currentfield);
		             return 1;
                 }
			     if (abw_komm == 0) strcpy (kdat, ldat);

 	             if (aufpListe.GetPosanz () == 0)
				 {
					 disp_mess ("Es wurden keine Positionen erfasst", 2);
					 SetCurrentField (currentfield);
					 SetCurrentFocus (currentfield);
					 return (1);
				 }
				 if (testkun0 () == FALSE)
				 {
					 return 1;
				 }
					beginwork (); //LAC-190
					ls_class.Commit_Work (); //LAC-190
                 FormToAuf (1);
				 if (TouZTest () == FALSE) return 0;
				 testtou (0);
                 FormToAuf (1);
				 if (aufk.auf_stat == 0)
				 {
					 aufk.auf_stat = 1;
				 }
                 if (aufpListe.GetMuster ())
                 {
                     aufk.auf_art = 99;
                 }
                 if (aufpListe.GetStorno ())
                 {
                     if (aufk.auf_stat > 0) aufk.auf_stat *= -1; 
                 }
	  	         aufpListe.KillProcessWgChart (); //LAC-52
		  	     aufpListe.KillProcessShowBm (); //LAC-62
		  	     aufpListe.KillProcessTourDat (); 
			     //LAC-180  aufptmp wird nicht mehr ben�tigt Aufptmp.delete_aufptmp (aufk.auf); //LAC-52

				 char date[12];
				 char time [12];

				 sysdate (date);
				 systime (time);
				 time[5] = 0;
				 aufk.bearb = dasc_to_long (date);
				 strcpy (aufk.bearb_zeit, time);
				 if (aufk.akv == 0)
				 {
					aufk.akv = dasc_to_long (date);
					strcpy (aufk.akv_zeit, time);
				 }
				 if (aufk.best_dat == 0)
				 {
					aufk.best_dat  = dasc_to_long (date); 
				 }
				 strcpy (aufk.feld_bz1,VersAufk);  //LAC-188  Versionskennung
                 ls_class.update_aufk (atoi (mdn), atoi (fil), aufk.auf);
                 commitwork ();
                 SetMuster (FALSE);
                 SetStorno (FALSE);

                 if (aufk.auf != auto_nr.nr_nr)
                 {
                       beginwork ();
					   if (lutz_mdn_par)
					   {
							   sprintf (mdn, "%hd", 0);
 						       sprintf (fil, "%hd", 0);
					   }
/*
                       dsqlstatus = AutoClass.nveinid (atoi (mdn), atoi (fil),
                                            "auf",  auto_nr.nr_nr);
*/
// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "auf",  auto_nr.nr_nr);
                       commitwork ();
                 }
                 InitAuf ();
                 break_enter ();
                 set_fkt (NULL, 12);
		         if (TransactMax > 0)
				 {
		                 tcount ++;
		                 if (tcount >= TransactMax)
						 { 
					           RestartProcess (0);
						 }
				 }
                 Mess.Message ("Satz wurde geschrieben");
           }
           else if (mench == 2)
           {
                 if (abfragejn (mamain1, "Auftrag l�schen ?", "J"))
                 {
                        DelAuf ();
                        commitwork ();
                        InitAuf ();
                        break_enter ();
                        set_fkt (NULL, 12);
                        Mess.Message ("Satz wurde gel�scht");
                 }
           }
           
           else if (mench == 3)
           {
                 AufKompl.K_Liste ();                  
           }
		   aufk.mdn = aufk.fil = 0;
//			if (EnterKomm)
			{	
			   EnableMenuItem (hMenu, IDM_KOMMIS, MF_BYCOMMAND | MF_GRAYED);
			}
	       EnableMenuItem (hMenu, IDM_NACHDRUCK, MF_BYCOMMAND | MF_ENABLED);
           EnableMenuItem (hMenu, IDM_TOURHINWEIS, MF_BYCOMMAND | MF_GRAYED);
	       TourhinweisButton->Enable (FALSE);

		   NachDrButton->Enable (TRUE);
		   CNachDruck.Color = BLUECOL;
		   DisplayFktBitmap ();

           return 0;
}


/* Ende Eingabe Kopfdaten                                         */
/******************************************************************/

void tst_arg (char *arg)
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                            case 'u' :
                                     strcpy (InfoCaption, arg + 1);
                                     return;
                 }
          }
          return;
}



void rollbackaufk (void)
/**
delstatus in aufk auf 0 setzen.
Bei neuem Auftrag Satz loeschen.
**/
{
	    int auf_stat = 0;
		int dsqlstatus;

		DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		DbClass.sqlin ((short *) &aufk.auf, 2, 0);
		DbClass.sqlout ((short *) &auf_stat, 1, 0);
		dsqlstatus = DbClass.sqlcomm ("select auf_stat from aufk "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and auf = ?");
		if (dsqlstatus != 0) return;

		if (auf_stat == 0)
		{
  		           DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		           DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		           DbClass.sqlin ((short *) &aufk.auf, 2, 0);
		           dsqlstatus = DbClass.sqlcomm ("select auf from aufp "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and auf = ?");

				   if (dsqlstatus == 0)
				   {
                             DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		                     DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		                     DbClass.sqlin ((short *) &aufk.auf, 2, 0);
		                     dsqlstatus = DbClass.sqlcomm ("delete from aufp "
			                                               "where mdn = ? "
									                       "and fil = ? "
									                       "and auf = ?");
				   }
  		           DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		           DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		           DbClass.sqlin ((short *) &aufk.auf, 2, 0);
		           dsqlstatus = DbClass.sqlcomm ("delete from aufk "
			                                     "where mdn = ? "
					                             "and fil = ? "
									             "and auf = ?");

/*
				   if (dsqlstatus == 100)
				   {
  		                     DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		                     DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		                     DbClass.sqlin ((short *) &aufk.auf, 2, 0);
		                     dsqlstatus = DbClass.sqlcomm ("delete from aufk "
			                                               "where mdn = ? "
									                       "and fil = ? "
									                       "and auf = ?");
				   }
				   else
				   {
  		                     DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		                     DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		                     DbClass.sqlin ((short *) &aufk.auf, 2, 0);
		                     dsqlstatus = DbClass.sqlcomm ("update aufk set delstatus = 0 "
			                                               "where mdn = ? "
									                       "and fil = ? "
									                       "and auf = ?");
				   }
*/

		}
		else
		{
  		           DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		           DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		           DbClass.sqlin ((short *) &aufk.auf, 2, 0);
		           dsqlstatus = DbClass.sqlcomm ("update aufk set delstatus = 0 "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and auf = ?");
		}
}


int dokey5 ()
/**
Aktion bei Taste F5
**/
{
        int dsqlstatus;
        
		akt_tou = 0l;
		if (mench == 1) 
		{
   		   	       aufpListe.KillProcessWgChart (); //LAC-52
		  	       aufpListe.KillProcessShowBm (); //LAC-62
			  	   aufpListe.KillProcessTourDat (); 
		}
		else if (mench == 0 && inDaten)
		{
			 if (inRestart == FALSE) //LAC-184
			 {
                if (abfragejn (mamain1, "Auftrag speichern ?", "J"))
				{
					return dokey12 ();
				}
				else
				{
					beginwork (); //LAC-167
					BOOL auf_vorh = ls_class.AufVorh (aufk.mdn,aufk.fil,aufk.auf);  //LAC-190
					ls_class.Rollback_Work (); //LAC-167
					commitwork (); //LAC-192

/** LAC-192
					if (auf_vorh == TRUE)
					{
						auf_vorh = ls_class.AufVorh (aufk.mdn,aufk.fil,aufk.auf);  //LAC-190
						if (auf_vorh == FALSE) //LAC-190
						{
							rollbackwork ();
						}
						else
						{
							commitwork ();
						}
					}
					else
					{
						commitwork (); //LAC-167
					}
					***/

   		   	       aufpListe.KillProcessWgChart (); //LAC-52
		  	       aufpListe.KillProcessShowBm (); //LAC-62
			  	   aufpListe.KillProcessTourDat (); 
				   //LAC-180  aufptmp wird nicht mehr ben�tigt Aufptmp.delete_aufptmp (aufk.auf); //LAC-52
				}
			 }
			 else
			 {
					beginwork (); //LAC-167
					BOOL auf_vorh = ls_class.AufVorh (aufk.mdn,aufk.fil,aufk.auf);  //LAC-190
					ls_class.Rollback_Work (); //LAC-167
					if (auf_vorh == TRUE)
					{
						auf_vorh = ls_class.AufVorh (aufk.mdn,aufk.fil,aufk.auf);  //LAC-190
						if (auf_vorh == FALSE) //LAC-190
						{
							rollbackwork ();
						}
						else
						{
							commitwork ();
						}
					}
					else
					{
						commitwork (); //LAC-167
					}

   		   	       aufpListe.KillProcessWgChart (); //LAC-52
		  	       aufpListe.KillProcessShowBm (); //LAC-62
			  	   aufpListe.KillProcessTourDat (); 
				   //LAC-180  aufptmp wird nicht mehr ben�tigt Aufptmp.delete_aufptmp (aufk.auf); //LAC-52
			 }

		}
        SetMuster (FALSE);
        SetStorno (FALSE);
		if (mench == 0 ||
			mench == 2) 
		{
			        if ((FreeAufNr == FALSE) && (mench == 0) && inDaten)
					{
			               rollbackaufk ();
					}
					else
					{
                           rollbackwork ();
					}
		}
        if (mench == 0 && auto_nr.nr_nr && FreeAufNr)
        {
			    if (lutz_mdn_par)
				{
					sprintf (mdn, "%hd", 0);
					sprintf (fil, "%hd", 0);
				}
                beginwork ();
// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "auf",  auto_nr.nr_nr);
				FreeAufNr = 0;
				FreeAufk (aufk.auf); //LAC-124d
				commitwork ();
        }
        break_enter ();
        DisplayAfterEnter (0);
        set_fkt (NULL, 12);
 	    aufk.mdn = aufk.fil = 0;
//		if (EnterKomm)
		{
			   EnableMenuItem (hMenu, IDM_KOMMIS, MF_BYCOMMAND | MF_GRAYED);
		}
	    EnableMenuItem (hMenu, IDM_NACHDRUCK, MF_BYCOMMAND | MF_ENABLED);
	    EnableMenuItem (hMenu, IDM_TOURHINWEIS, MF_BYCOMMAND | MF_GRAYED);
        TourhinweisButton->Enable (FALSE);

	    NachDrButton->Enable (TRUE);
	    CNachDruck.Color = BLUECOL;
		DisplayFktBitmap ();

		//LAC-184  auch hier noch mal ganz raus 
		  if (TransactMax > 0 && mench == 0 && inDaten && inRestart == FALSE)
		  {
		        tcount ++;
		        if (tcount >= TransactMax)
				{
					 RestartProcess (0);
				}
		  }

        return 1;
}


BOOL IsComboMsg (MSG *msg)
/**
Test, ob die Meldung fuer die Combobox ist.
**/
{
      if (HIWORD (msg->lParam) == CBN_DROPDOWN)
      {
                 opencombobox = TRUE;
                 return TRUE;
      }
      if (HIWORD (msg->lParam) == CBN_CLOSEUP)
      {
                 opencombobox = FALSE;
                 return TRUE;
      }

      if (msg->hwnd == hwndCombo1)
      {
                 return TRUE;
      }
      if (msg->hwnd == hwndCombo2)
      {
                 return TRUE;
      }
      if (msg->hwnd == hwndCombo3)
      {
                 return TRUE;
      }
      if (msg->hwnd == hwndCombo4)
      {
                 return TRUE;
      }
      if (msg->hwnd == hwndTB)
      {
                 return TRUE;
      }

      return FALSE;
}

void FillCombo1 (void)
{
      int i;

      for (i = 0; Combo1[i]; i ++)
      {
           SendMessage (hwndCombo1, CB_ADDSTRING, 0,
                                   (LPARAM) Combo1 [i]);
      }
      SendMessage (hwndCombo1, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo1 [combo1pos]);
      aufpListe.SetLines (combo1pos);
}

void FillCombo2 (void)
{
      int i;

      for (i = 0; Combo2[i]; i ++)
      {
           SendMessage (hwndCombo2, CB_ADDSTRING, 0,
                                   (LPARAM) Combo2 [i]);
      }
      SendMessage (hwndCombo2, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo2 [combo2pos]);
      SetComboMess (IsComboMsg);
}

BOOL IsInSpaltenAddCombo ( char *text, int dReiter)
{
	if (dReiter == 0) return FALSE;
	clipped(text);
    if (strcmp ("letzte Lieferung",text) == 0 && aufpListe.Spalte_last_lieferung_Ok == FALSE  && aufpListe.Spalte_last_lieferung_gewaehlt[dReiter -1] == FALSE) 
	{
		return TRUE;
	}
    if (strcmp ("letzte SollLieferung",text) == 0 && aufpListe.Spalte_last_lief_me_Ok == FALSE  && aufpListe.Spalte_last_lief_me_gewaehlt[dReiter -1] == FALSE) 
	{
		return TRUE;
	}
    if (strcmp ("letzte Bestellung",text) == 0 && aufpListe.Spalte_last_me_Ok == FALSE   && aufpListe.Spalte_last_me_gewaehlt[dReiter -1] == FALSE)
	{
		return TRUE;
	}
    if (strcmp ("letztes Lieferdatum",text) == 0 && aufpListe.Spalte_lieferdat_Ok == FALSE   && aufpListe.Spalte_lieferdat_gewaehlt[dReiter -1] == FALSE)
	{
		return TRUE;
	}
    if (strcmp ("letzter Verkaufspreis",text) == 0 && aufpListe.Spalte_lpr_vk_Ok == FALSE   && aufpListe.Spalte_lpr_vk_gewaehlt[dReiter -1] == FALSE) 
	{
		return TRUE;
	}
    if (strcmp ("Bestellvorschlag",text) == 0 && aufpListe.Spalte_bestellvorschlag_Ok == FALSE  && aufpListe.Spalte_bestellvorschlag_gewaehlt[dReiter -1] == FALSE) 
	{
		return TRUE;
	}
    if (strcmp ("grp_min_best",text) == 0 && aufpListe.Spalte_grp_min_best_Ok == FALSE  && aufpListe.Spalte_grp_min_best_gewaehlt[dReiter -1] == FALSE)  //LAC-104
	{
		return TRUE;
	}
    if (strcmp ("St�ck / Karton",text) == 0 && aufpListe.Spalte_stk_karton_Ok == FALSE  && aufpListe.Spalte_stk_karton_gewaehlt[dReiter -1] == FALSE) //LAC-118
	{
		return TRUE;
	}
    if (strcmp ("Gew.d.Kartons",text) == 0 && aufpListe.Spalte_gew_karton_Ok == FALSE  && aufpListe.Spalte_gew_karton_gewaehlt[dReiter -1] == FALSE) 
	{
		return TRUE;
	}
    if (strcmp ("Bestand",text) == 0 && aufpListe.Spalte_bsd_Ok == FALSE  && aufpListe.Spalte_bsd_gewaehlt[dReiter -1] == FALSE) 
	{
		return TRUE;
	}
    if (strcmp ("LD.EURO",text) == 0 && aufpListe.Spalte_lad_pr_Ok == FALSE  && aufpListe.Spalte_lad_pr_gewaehlt[dReiter -1] == FALSE) 
	{
		return TRUE;
	}
    if (strcmp ("Rabatt %",text) == 0 && aufpListe.Spalte_lad_pr_prc_Ok == FALSE  && aufpListe.Spalte_lad_pr_prc_gewaehlt[dReiter -1] == FALSE) 
	{
		return TRUE;
	}
    if (strcmp ("LD Marge",text) == 0 && aufpListe.Spalte_lad_pr0_Ok == FALSE  && aufpListe.Spalte_lad_pr0_gewaehlt[dReiter -1] == FALSE) 
	{
		return TRUE;
	}
    if (strcmp ("Marge %",text) == 0 && aufpListe.Spalte_marge_Ok == FALSE  && aufpListe.Spalte_marge_gewaehlt[dReiter -1] == FALSE) 
	{
		return TRUE;
	}
    if (strcmp ("MHD Datum",text) == 0 && aufpListe.Spalte_mhd_Ok == FALSE  && aufpListe.Spalte_mhd_gewaehlt[dReiter -1] == FALSE) 
	{
		return TRUE;
	}

	return FALSE;
}

BOOL IsInSpaltenDelCombo ( char *text, int dReiter)
{
	if (dReiter == 0) return FALSE;
	clipped(text);
    if (strcmp ("letzte Lieferung",text) == 0 && aufpListe.Spalte_last_lieferung_Ok == TRUE && aufpListe.Spalte_last_lieferung_gewaehlt[dReiter -1] == TRUE )
	{
		return TRUE;
	}
    if (strcmp ("letzte SollLieferung",text) == 0 && aufpListe.Spalte_last_lief_me_Ok == TRUE && aufpListe.Spalte_last_lief_me_gewaehlt[dReiter -1] == TRUE )
	{
		return TRUE;
	}
    if (strcmp ("letzte Bestellung",text) == 0 && aufpListe.Spalte_last_me_Ok == TRUE   && aufpListe.Spalte_last_me_gewaehlt[dReiter -1] == TRUE) 
	{
		return TRUE;
	}
    if (strcmp ("letztes Lieferdatum",text) == 0 && aufpListe.Spalte_lieferdat_Ok == TRUE   && aufpListe.Spalte_lieferdat_gewaehlt[dReiter -1] == TRUE)
	{
		return TRUE;
	}
    if (strcmp ("letzter Verkaufspreis",text) == 0 && aufpListe.Spalte_lpr_vk_Ok == TRUE   && aufpListe.Spalte_lpr_vk_gewaehlt[dReiter -1] == TRUE) 
	{
		return TRUE;
	}
    if (strcmp ("Bestellvorschlag",text) == 0 && aufpListe.Spalte_bestellvorschlag_Ok == TRUE && aufpListe.Spalte_bestellvorschlag_gewaehlt[dReiter -1] == TRUE) 
	{
		return TRUE;
	}
    if (strcmp ("grp_min_best",text) == 0 && aufpListe.Spalte_grp_min_best_Ok == TRUE && aufpListe.Spalte_grp_min_best_gewaehlt[dReiter -1] == TRUE)  //LAC-104
	{
		return TRUE;
	}
    if (strcmp ("St�ck / Karton",text) == 0 && aufpListe.Spalte_stk_karton_Ok == TRUE  && aufpListe.Spalte_stk_karton_gewaehlt[dReiter -1] == TRUE) //LAC-118
	{
		return TRUE;
	}
    if (strcmp ("Gew.d.Kartons",text) == 0 && aufpListe.Spalte_gew_karton_Ok == TRUE  && aufpListe.Spalte_gew_karton_gewaehlt[dReiter -1] == TRUE) 
	{
		return TRUE;
	}
    if (strcmp ("Bestand",text) == 0 && aufpListe.Spalte_bsd_Ok == TRUE  && aufpListe.Spalte_bsd_gewaehlt[dReiter -1] == TRUE) 
	{
		return TRUE;
	}
    if (strcmp ("LD.EURO",text) == 0 && aufpListe.Spalte_lad_pr_Ok == TRUE  && aufpListe.Spalte_lad_pr_gewaehlt[dReiter -1] == TRUE) 
	{
		return TRUE;
	}
    if (strcmp ("Rabatt %",text) == 0 && aufpListe.Spalte_lad_pr_prc_Ok == TRUE  && aufpListe.Spalte_lad_pr_prc_gewaehlt[dReiter -1] == TRUE) 
	{
		return TRUE;
	}
    if (strcmp ("LD Marge",text) == 0 && aufpListe.Spalte_lad_pr0_Ok == TRUE  && aufpListe.Spalte_lad_pr0_gewaehlt[dReiter -1] == TRUE) 
	{
		return TRUE;
	}
    if (strcmp ("Marge %",text) == 0 && aufpListe.Spalte_marge_Ok == TRUE  && aufpListe.Spalte_marge_gewaehlt[dReiter -1] == TRUE) 
	{
		return TRUE;
	}
    if (strcmp ("MHD Datum",text) == 0 && aufpListe.Spalte_mhd_Ok == TRUE  && aufpListe.Spalte_mhd_gewaehlt[dReiter -1] == TRUE) 
	{
		return TRUE;
	}

	return FALSE;
}

void FillCombo3 (int dReiter)
{
      int i = 0;

	  while(SendMessage(hwndCombo3, CB_DELETESTRING, 0, NULL) > 0) ;
      SendMessage (hwndCombo3, CB_ADDSTRING, 0,
                             (LPARAM) Combo3 [i]);

      for (i = 1; Combo3[i]; i ++)
      {
		  if (IsInSpaltenAddCombo (Combo3[i], dReiter))
		  {
	          SendMessage (hwndCombo3, CB_ADDSTRING, 0,
                                   (LPARAM) Combo3 [i]);
		  }
      }
      SendMessage (hwndCombo3, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo3 [combo3pos]);
      SetComboMess (IsComboMsg);
}


void FillCombo4 (int dReiter)
{
      int i = 0;

	  while(SendMessage(hwndCombo4, CB_DELETESTRING, 0, NULL) > 0) ;
      SendMessage (hwndCombo4, CB_ADDSTRING, 0,
                              (LPARAM) Combo4 [i]);
      for (i = 1; Combo4[i]; i ++)
      {
		  if (IsInSpaltenDelCombo (Combo4[i], dReiter))
		  {
	           SendMessage (hwndCombo4, CB_ADDSTRING, 0,
                                   (LPARAM) Combo4 [i]);
		  }
      }
      SendMessage (hwndCombo4, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo4 [combo4pos]);
      SetComboMess (IsComboMsg);
}

static int StopProc (void)
{
    PostQuitMessage (0);
    return 0;
}


void DisableTB (void)
{
     ToolBar_SetState(hwndTB,IDM_STD,  TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,IDM_LIST, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,KEYLEFT,  TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,KEYRIGHT, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,IDM_FIND, TBSTATE_INDETERMINATE);
}

void EnableTB (void)
{
     ToolBar_SetState(hwndTB,IDM_STD,  TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,IDM_LIST, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,KEYLEFT,  TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,KEYRIGHT, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,IDM_FIND, TBSTATE_ENABLED);

     ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
     ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
}


BOOL GroupOK (void)
/**
Test, ob komplett gesetzt werden darf.
**/
{
	    long tou;

	    if (tou_def_zwang == FALSE) return TRUE;
        tou = atol (tour) / tour_div;
		if (tou == 0l) return TRUE;

		if (tou < atol (tou_def_von)) return FALSE;
		if (tou > atol (tou_def_bis)) return FALSE;
		return TRUE;
}


int EnterVk ()
{
        aufpListe.EnterLdVk ();
        return 0;
}

int EnterPrVk ()
{
        aufpListe.EnterPrVk ();
        return 0;
}

int StarteShowBm ()
{
	aufpListe.StarteProcessShowBm(1);
	return 0;
}

int ZeigeBmp ()
{
	if (aufpListe.ZeigeBmp  == TRUE)
	{
		aufpListe.KillProcessShowBm ();
		aufpListe.SetZeigeBmp (0);
	}
	else
	{
		aufpListe.StarteProcessShowBm (0);
		aufpListe.SetZeigeBmp (1);
	}
	return 0;
}
int ZeigePalette ()
{
	if (aufpListe.ZeigePalette  == TRUE)
	{
		aufpListe.KillProcessTourDat ();
		aufpListe.SetZeigePalette (0);
	}
	else
	{
		aufpListe.StarteProcessTourDat ();
		aufpListe.SetZeigePalette (1);
	}
	return 0;
}
int ZeigeWarenkorb ()
{
	if (aufpListe.ZeigeWarenkorb  == TRUE)
	{
		aufpListe.SetZeigeWarenkorb (0);
		aufpListe.KillProcessWgChart ();
	}
	else
	{
		aufpListe.SetZeigeWarenkorb (1);
		aufpListe.StarteProcessWgChart (aufk.auf);
	}
	return 0;
}

int ZeigeKritMhd ()
{
	if (aufpListe.ZeigeKritMhd  == TRUE)
	{
	    aufpListe.SetZeigeKritMhd (0);
	}
	else
	{
	    aufpListe.SetZeigeBestand (0);
	    aufpListe.SetZeigeKritMhd (1);
	}
    aufpListe.ShowKritMHD ();
	return 0;
}

int ZeigeBestand ()
{
	if (aufpListe.ZeigeBestand  == TRUE)
	{
	    aufpListe.SetZeigeBestand (0);
	}
	else
	{
	    aufpListe.SetZeigeKritMhd (0);
	    aufpListe.SetZeigeBestand (1);
	}
    aufpListe.ShowKritMHD ();
	return 0;
}


int EnterListe ()
{
	int ftyp;

    
	if (aufpListe.GetFlgInEnterListe() == FALSE) //LAC-9
    {
	     strcpy(CReiterletzteBestellung.text1,txtletzteBestellung0); //LAC-132
		SetReiterAktAuf (); //LAC-9   zu Anfang soll "aktiver Auftrag" aktiv sein 
		aufpListe.SetSpaltenAktAuf (); //LAC-50
		PrintButtons ();
		if (testkun0 () == FALSE) return 0;
		if (fen_par)
		{
			ftyp = AufKompl.SetFak_typ (mamain1);
			if (ftyp != -1)
			{
				aufk.fak_typ = ftyp;
			}
		}
		DlgWindow = NULL;
		EnableTB ();
		set_fkt (NULL, 4);
		EnableArrows (&aufform, FALSE);
		EnableMenuItem (hMenu, IDM_FIND,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_FONT,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_BASIS,   MF_ENABLED);
		EnableMenuItem (hMenu, IDM_POSTEXT, MF_ENABLED);
		EnableMenuItem (hMenu, IDM_BESTELLVORSCHL, MF_ENABLED);
		EnableMenuItem (hMenu, IDM_IGNOREMINBEST, MF_ENABLED);
		EnableMenuItem (hMenu, IDM_LDPR,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_VKPR,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_SHOWBMP,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_SHOWBMPP,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_KRITMHD,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_BESTAND,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_PALETTE,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_WARENKORB,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_PRICEFROMLIST, MF_BYCOMMAND | MF_ENABLED);
		EnableMenuItem (hMenu, IDM_FIXWG,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_FIXPAL,    MF_ENABLED);
		EnableMenuItem (hMenu, IDM_FIXBMP,    MF_ENABLED);
	
	    set_fkt (NULL, 9);
	    SetFkt (9, leer, 0);
	    set_fkt (NULL, 10);
	    SetFkt (10, leer, 0);
		if (lutz_mdn_par)
		{
			if (atol (kunfil) == 0) 
			{
				aufpListe.SetMdnProd (kun.kst);
			}
			else
			{
				aufpListe.SetMdnProd (_fil.mdn);
			}
		}
	
	    if (LdVkEnabled)
	    {
			set_men (EnterVk, (unsigned char) 'L');
		}
	
	    if (PrVkEnabled)
	    {
			set_men (EnterPrVk, (unsigned char) 'V');
		}
	    if (ShowBmEnabled)
	    {
			set_men (StarteShowBm, (unsigned char) 'B');
		}
			set_men (ZeigeBmp, (unsigned char) 'A');
			set_men (ZeigePalette, (unsigned char) 'P');
			set_men (ZeigeWarenkorb, (unsigned char) 'W');
			set_men (ZeigeKritMhd, (unsigned char) 'K');
			set_men (ZeigeBestand, (unsigned char) 'C');
	
	/* Vor dem Aufruf der Liste Kopf abspeichern */
	
	    FormToAuf (1);
	    if (aufk.auf_stat == 0)
	    {
			aufk.auf_stat = 1;
		}

		if (mench == 0) //LAC-150
		{
			aufk.delstatus = -1;
			ls_class.update_aufk (atoi (mdn), atoi (fil), aufk.auf);
			aufk.delstatus = 0;
		}
	}

    aufpListe.EnterAufp (atoi (mdn) , atoi (fil), atol (auftrag), atoi (kunfil),
                         atol (kuns), ldat);

		        if (getenv ("testmode")) //LAC-194test
				{
					if (atoi (getenv ("testmode")) == 1)  
					{
								disp_mess ("EnterListe nach EnterAufp", 2);
					}
				}
	if (aufpListe.GetFlgInEnterListe() == FALSE) //LAC-9
    {
		aufpListe.TerminateKritMHD (); //LAC-166
		        if (getenv ("testmode")) //LAC-194test
				{
					if (atoi (getenv ("testmode")) == 1)  
					{
								disp_mess ("EnterListe nach TerminateKritMHD", 2);
					}
				}
	    set_men (NULL, (unsigned char) 'L');
		set_men (NULL, (unsigned char) 'V');

		EnableMenuItem (hMenu, IDM_FIND, MF_GRAYED);
		EnableMenuItem (hMenu, IDM_FONT, MF_GRAYED);
		EnableMenuItem (hMenu, IDM_BASIS, MF_GRAYED);
		EnableMenuItem (hMenu, IDM_POSTEXT, MF_GRAYED);
		EnableMenuItem (hMenu, IDM_BESTELLVORSCHL, MF_GRAYED);
		EnableMenuItem (hMenu, IDM_IGNOREMINBEST, MF_GRAYED);
		EnableMenuItem (hMenu, IDM_LDPR,    MF_GRAYED);
		EnableMenuItem (hMenu, IDM_VKPR,    MF_GRAYED);
		EnableMenuItem (hMenu, IDM_SHOWBMP,    MF_GRAYED);
		EnableMenuItem (hMenu, IDM_SHOWBMPP,    MF_GRAYED);
		EnableMenuItem (hMenu, IDM_KRITMHD,    MF_GRAYED);
		EnableMenuItem (hMenu, IDM_BESTAND,    MF_GRAYED);
		EnableArrows (&aufform, TRUE);
		DisableTB ();
		if (GroupOK ())
		{
				if (KomplettMode < 2) 
				{
					set_fkt (SetKomplett, 8);
					SetFkt (8, komplett, KEY8);
				}
		}
	}
		        if (getenv ("testmode")) //LAC-194test
				{
					if (atoi (getenv ("testmode")) == 1)  
					{
								disp_mess ("EnterListe Ende", 2);
					}
				}
    PosOK = TRUE;
    return 0;
}

void PrintButtons (void)
{
         clipped (_adr.tel);
         if (strcmp (_adr.tel, " ") > 0)
         {
                CTelefon.bmp = btelefon;
                ActivateColButton (mamain1, 
                                  &buform, 1, 0, 1);
                EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
         }
         else
         {
                CTelefon.bmp = btelefoni;
                ActivateColButton (mamain1, 
                &buform, 1, -1, 1);
                EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
         }
         clipped (_adr.fax);
         if (strcmp (_adr.fax, " ") > 0)
         {
                ActivateColButton (mamain1, 
                                   &buform,      0, 0, 1);
                EnableMenuItem (hMenu, CallT1,   MF_ENABLED);
         }
         else
         {
                ActivateColButton (mamain1, 
                                      &buform,      0, -1, 1);
                EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
         }
         ActivateColButton (mamain1, &buform, 2, 0, 1);
         EnableMenuItem (hMenu, CallT3,   MF_ENABLED);
         MoveButtons ();
}

int EnterListe0 ()
{
	    EnterListe ();
// Testversion
		Sleep (10);
		MSG msg;
        if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
        {
                  TranslateMessage(&msg);
                  DispatchMessage(&msg);
        }
		PostMessage (mamain1, WM_KEYDOWN, VK_F6, 0l);
// Ende Testversion
		return 0;

}


void MoveButtons (void)
/**
Buttons in mamain1 rechtsbuendig.
**/
{
     RECT rect;
     RECT rectb;
     TEXTMETRIC tm;
     HDC hdc;
     int i;
     int x, y;

     GetClientRect (mamain1, &rect); 
     hdc = GetDC (mamain1);
     GetTextMetrics (hdc, &tm);
     ReleaseDC (mamain1, hdc);

     for (i = 0; i < buform.fieldanz; i ++)
     {
		 if (buform.mask[i].feldid == NULL) continue;

         GetClientRect (buform.mask[i].feldid, &rectb );
         x = rect.right - rectb.right - 1;
		 if (BuFont.PosMode == 0)
		 {
                y = buform.mask[i].pos[0] * tm.tmHeight;
		 }
		 else
		 {
                y = buform.mask[i].pos[0];
		 }
         MoveWindow (buform.mask[i].feldid,
                                    x, y,
                                    rectb.right,
                                    rectb.bottom,
                                    TRUE);

		 buform.mask[i].pos[0] = y;
		 buform.mask[i].pos[1] = x;
		 buform.mask[i].length = (short) rectb.right;
		 buform.mask[i].rows   = (short) rectb.bottom;
     }
     if (buform.mask[0].feldid)
	 {
	     BuFont.PosMode = 1;
	 }
     CreateQuikInfos ();
}


int CalcMinusPos (int cy)
{
        TEXTMETRIC tm;
        SIZE size;
        HDC hdc;


        hdc = GetDC (ftasten);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (ftasten, hdc);
        cy -= 3 * tm.tmHeight;
        return cy;
}

int CalcPlusPos (int y)
{
        TEXTMETRIC tm;
        SIZE size;
        HDC hdc;


        hdc = GetDC (ftasten);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (ftasten, hdc);
        y += 1 * tm.tmHeight + 5;
        return y;
}

void MoveMamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;

     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;
     cy = frect.top - mrect.top - y - 50;
     if (ftasten2 == NULL)
     {
         y  = CalcPlusPos (y);
         cy = CalcMinusPos (cy);
     }
     MoveWindow (mamain1, x, y, cx, cy, TRUE);
     MoveFktBitmap ();
     MoveButtons ();
}


void Createmamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;


     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;


     cy = frect.top - mrect.top - y - 50;

     if (ftasten2 == NULL)
     {
         y  = CalcPlusPos (y);
         cy = CalcMinusPos (cy);
     }

     mamain1 = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "hStdWindow",
                              InfoCaption,
                              WS_CHILD |  
                              WS_VISIBLE,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
}

void ShowDaten (void)
/**
Daten fuer Auftrag anzeigen.
**/
{
        Mess.CloseMessage ();
        display_form (mamain1, &aufform, 0, 0);

        aufpListe.ShowAufp (atoi (mdn) , atoi (fil), 
                             atol (auftrag), atoi (kunfil),
                             atol (kuns), ldat);
        current_form = &aufformk;
        set_fkt (dokey12, 12);
        display_form (mamain1, &aufformk, 0, 0);

}

void MSleep (int tsec)
{
        MSG msg;  
        int i;

		for (i = 0; i < tsec; i ++)
		{
                  if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
                  {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                  }
                  Sleep (1);
        }
}



void eingabedaten (void)
/**
Artikel bearbeiten.
**/
{

        if (getenv ("testmode")) //LAC-194test
		{
			if (atoi (getenv ("testmode")) == 1)  
			{
						disp_mess ("Eingabedaten Anfang", 2);
			}
		}
       inDaten = 1;
	   EnableFullTax ();
       Mess.CloseMessage ();
	   akt_kun = 0l;
	   aufpListe.SetFlgInEnterListe (FALSE);
       while (TRUE)
       {
				if (getenv ("testmode")) //LAC-194test
				{
					if (atoi (getenv ("testmode")) == 1)  
					{
								disp_mess ("Eingabedaten Anfang Schleife", 2);
					}
				}

                set_fkt (InfoKun, 4);
                set_fkt (doliste, 6);
                SetFkt (6, liste, KEY6);
                set_fkt (SwitchZei, 7);
                SetFkt (7, aktzei, KEY7);
                set_fkt (SwitchArt, 10);
                SetFkt (10, aktart, KEY10);
                set_fkt (dokey12, 12);
                no_break_end ();

				if (getenv ("testmode")) //LAC-194test
				{
					if (atoi (getenv ("testmode")) == 1)  
					{
							disp_mess ("Eingabedaten vor ShowAufp", 2);
					}
				}
                aufpListe.ShowAufp (atoi (mdn) , atoi (fil), 
                             atol (auftrag), atoi (kunfil),
                             atol (kuns), ldat);

				if (getenv ("testmode")) //LAC-194test
				{
					if (atoi (getenv ("testmode")) == 1)  
					{
								disp_mess ("Eingabedaten nach ShowAufp", 2);
					}
				}
 	            if (aufpListe.GetPosanz () > 0)
				{
					EnableMenuItem (hMenu, IDM_PRICEFROMLIST, MF_BYCOMMAND | MF_ENABLED);
				}
				if (aufk.psteuer_kz > 0 && aufpListe.ContainsService ())
				{
					EnableFullTax ();
				}
				SetKunAttr ();
                DisplayAfterEnter (FALSE);
				currentfield = 0;
				 // LAC-9 enter_form (mamain1, &aufform, 0, 0);
				//LAC-9
                if (aufpListe.GetFlgInEnterListe() == FALSE)  //LAC-9
				{
					aufpListe.SetSpaltenAktAuf (); //LAC-56
					enter_form (mamain1, &aufform, 0, 0);
				}
				else
				{
					AddReiterButton (iReiterAktauf,1);
					AddReiterButton (iReiterBestellvorschlag,1);
					AddReiterButton (iReiterletzteBestellung,1);
			        AddReiterButton (iReiterKritischeMhd,2); //LAC-84
					if (LskVorhanden ()) 
					{
						AddReiterButton (iReiterkommldat,2); //LAC-116
					}
					HoleHWG();  //LAC-84 	
					aufpListe.ZeigeLeiste ();
					SetReiterMenue (ReiterMenue);
				}
				//LAC-9 E

/*
Zum Dauertest der Masken enter_form asukommentieren.


create_enter_form (mamain1, &aufform, 0, 0);
MSleep (100);
dokey5 ();
*/
				if (getenv ("testmode")) //LAC-194test
				{
					if (atoi (getenv ("testmode")) == 1)  
					{
								disp_mess ("Eingabedaten 2", 2);
					}
				}

                EnableMenuItem (hMenu, IDM_PRICEFROMLIST, MF_GRAYED);
                DisplayLines ();
// break;
				if (aufpListe.GetFlgInEnterListe () == FALSE) //LAC-9
				{
					if (syskey == KEYESC || syskey == KEY5 ||
                    syskey == KEY12)
					{
                                 DisplayAfterEnter (TRUE);
                                 break;
					}
				}
				if (getenv ("testmode")) //LAC-194test
				{
					if (atoi (getenv ("testmode")) == 1)  
					{
								disp_mess ("Eingabedaten 3", 2);
					}
				}
                current_form = &aufform;
                aufpListe.DestroyWindows (); 
                display_form (mamain1, &aufformk);
                display_form (mamain1, &aufform, 0, 0);
				EnterListe ();
		        if (getenv ("testmode")) //LAC-194test
				{
					if (atoi (getenv ("testmode")) == 1)  
					{
								disp_mess ("Eingabedaten nach EnterListe", 2);
					}
				}
				if (aufpListe.GetFlgInEnterListe () == FALSE) //LAC-9
				{
	                aufpListe.initReiterv (); 
				}
                set_fkt (dokey5, 5);
                set_fkt (doliste, 6);
                set_fkt (NULL, 7);
                SetFkt (6, liste, KEY6);
                SetFkt (7, leer, 0);
		        if (getenv ("testmode")) //LAC-194test
				{
					if (atoi (getenv ("testmode")) == 1)  
					{
								disp_mess ("Eingabedaten nach EnterListe2", 2);
					}
				}
        }
        aufpListe.DestroyWindows (); 
        CloseControls (&aufform);
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        SetFkt (6, leer, 0);
        SetFkt (7, leer, 0);
        inDaten = 0;
        set_fkt (NULL, 8);
        SetFkt (8, leer, NULL);
		akt_tou = 0l;
        return;
}

int ShowFil (void)
/**
Auswahl ueber Filialen.
**/
{
	   _fil.mdn = atoi (mdn);
	   if (lutz_mdn_par)
	   {
                 fil_class.ShowAllBu (mamain1, 0);
	   }
	   else
	   {
                 fil_class.ShowAllBu (mamain1, 0, aufk.mdn);
	   }
       if (syskey == KEYESC || syskey == KEY5)
       {
           return 0;
       }
       kunfield = GetItemPos (&aufform, "kun");
       sprintf (kuns, "%d", _fil.fil);
       display_field (mamain1, &aufform.mask[kunfield], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;

}
    

int ChoiseKunLief0 (void)
{
       struct SKUNLIEF *skunlief;
       struct SLIEFKUN *sliefkun;
       SEARCHKUNLIEF SearchKunLief;
       SEARCHLIEFKUN SearchLiefKun;
       char query [512];

       LiefAdr = 0l;
       int kunfield = GetItemPos (&aufform, "kun");
       SearchKunLief.SetParams (hMainInst, mamain1);
       SearchKunLief.Setawin (hMainWindow);
      //LAC-184 if (atol (kuns) != 0)
       //LAC-184{
             sprintf (query, " and kun = %ld", atol (kuns)),
             SearchKunLief.SetQuery (query);
       //LAC-184}


       if (SearchKunLief.TestRec (atoi (mdn)) != 0)
       {
             return 0;
       }
       SearchKunLief.Search (atoi (mdn));
       skunlief = SearchKunLief.GetSkun ();
       if (syskey == KEY5 || skunlief == NULL)
       {
            SetCurrentFocus (currentfield);
            display_field (mamain1, &aufform.mask[kunfield], 0, 0);
            if (atol (kuns) == 0l)
            {
                  syskey = KEY5;
            }
            return 0;
       }
       if (atol (kuns) == 0l)
       {
            SearchLiefKun.SetParams (hMainInst, mamain1);
            SearchLiefKun.Setawin (hMainWindow);
            sprintf (query, " and adr = %ld", atol (skunlief->adr)),
            SearchLiefKun.SetQuery (query);
            SearchLiefKun.Search (atoi (mdn));
            sliefkun = SearchLiefKun.GetSkun ();
            if (syskey == KEY5 || sliefkun == NULL)
            {
                  SetCurrentFocus (currentfield);
                  display_field (mamain1, &aufform.mask[kunfield], 0, 0);
                  if (atol (kuns) == 0l)
                  {
                         syskey = KEY5;
                  }
                  return 0;
            }
            sprintf (kuns, "%ld", atol (sliefkun->kun));
       }
       LiefAdr = atol (skunlief->adr);
       display_field (mamain1, &aufform.mask[kunfield], 0, 0);

       return 0;
}


int ChoiseKunLief (void)
{
       
       ChoiseKunLief0 ();
       if (syskey != KEY5)
       {
              lesekun0 ();
              int ldatfield = GetItemPos (&aufform, "lieferdat");
              if (ldatfield != -1)
              {
 	                  SetCurrentField (ldatfield);
                      SetCurrentFocus (ldatfield);
              }
//              PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       }
       return 0;
}

BOOL ShowKun ()
{
        struct SKUN *skun;
        SEARCHKUN SearchKun;
        char matchorg [256];
        char matchupper [256];
        char matchlower [256];
        char matchuplow [256];
        char query [512];

        if (strcmp (clipped (kuns), " ") > 0 &&
            strcmp (kuns, "*") != 0)
        {
	        strcpy (matchorg, kuns);
            strupcpy (matchupper, kuns);
            strlowcpy (matchlower, kuns);
            struplowcpy (matchuplow, kuns);
            sprintf (query, " and (kun_krz1 matches \"%s*\" "
                           "or kun_krz1 matches \"%s*\" "
                           "or kun_krz1 matches \"%s*\" "
                           "or kun_krz1 matches \"%s*\")",matchorg, 
                                                          matchupper,
                                                          matchlower,
                                                          matchuplow);
        }
        else
        {
            sprintf (query, " and kun_krz1 matches \"*\"");
        }

        SearchKun.SetQuery (query);
        SearchKun.SetParams (hMainInst, mamain1);
        SearchKun.Setawin (hMainWindow);
        SearchKun.SearchKun (aufk.mdn, NULL, NULL);
        if (syskey == KEY5)
        {
            SetCurrentFocus (currentfield); 
            return 0;
        }
        skun = SearchKun.GetSkun ();
        if (skun == NULL)
        {
            SetCurrentFocus (currentfield); 
            return 0;
        }
        strcpy (kuns, skun->kun);
        return lesekun0 ();
}


int QueryKun (void)
/**
Auswahl ueber Kunden.
**/
{
       int ret;

       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain1);
	   kun.mdn = atoi (mdn);
//	   kun.fil = atoi (fil);
	   kun.fil = 0;
       ret = QClass.querykun (mamain1);
       set_fkt (dokey5, 5);
       set_fkt (doliste, 6);
       SetFkt (6, liste, KEY6);
       SetFkt (7, aktzei, KEY7);
       SetFkt (9, auswahl, KEY9);
	   SetFkt (8, leer, NULL);
	   set_fkt (NULL, 8);
	   set_fkt (NULL, 12);

       if (atoi (kunfil))
       {
             set_fkt (ShowFil, 9);
       }
       else
       {
             set_fkt (QueryKun, 9);
       }

       if (GroupOK ())
       {
            if (KomplettMode < 2) 
			{
				set_fkt (SetKomplett, 8);
                SetFkt (8, komplett, KEY8);
			}
       }
       set_fkt (dokey12, 12);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       kunfield = GetItemPos (&aufform, "kun");
       sprintf (kuns, "%ld", kun.kun);
		  if (TransactMax > 0)  //LAC-184
		  {
		        tcount ++;
		        if (tcount >= TransactMax)
				{
					int dkun = atoi (kuns);
					inRestart = TRUE;
					dokey5 (); 
					 RestartProcess (dkun);
				}
		  }
       if (KunLiefKz)
       {
           ChoiseKunLief ();
       }
       else
       {
          display_field (mamain1, &aufform.mask[kunfield], 0, 0);
          PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       }
		  return 0;
}


int setkey9kun ()
/**
Auswahl fuer Kunde setzen.
**/
{
// FS-547 setkey9kun wird aufgerufen in Position bei F9 Mengeneinheiten  !!!
      // FS-547 LiefAdr = 0l; 	  
	   if (IsAng)
	   {
	        GetLieferdat ();
			IsAng = FALSE;
	   }
       akt_kun = atol (kuns);
       SetFkt (9, auswahl, KEY9);
       if (atoi (kunfil))
       {
             set_fkt (ShowFil, 9);
       }
       else
       {
             set_fkt (QueryKun, 9);
             if (KunLiefKz)
             {
                  SetFkt (8, kunlieftxt, KEY8);
                  set_fkt (ChoiseKunLief, 8);
             }
       }
       return 0;
}

int TouQuery (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;
	   int tou_bz_pos;
	   char *tou_bz;

//       if (wo_tou_par)  QTClass.ReadWoTou (aufk.mdn, 0, atoi (kunfil),atol (kuns), tour_div); //LAC-10
       if (wo_tou_par)  QTClass.ReadWoTouDatum (ldat); //LAC-10
       ret = QTClass.querytou (mamain1, tour_div);
       set_fkt (dokey5, 5);
       set_fkt (doliste, 6);
       SetFkt (6, liste, KEY6);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       toufield = GetItemPos (&aufform, "tou");

       tou_bz_pos = GetItemPos (&aufform, "tou_bz");
       tou_bz = aufform.mask[tou_bz_pos].item->GetFeldPtr ();

	   SetCurrentField (toufield);
       sprintf (tour, "%ld", tou.tou * tour_div);
	   strcpy (tou_bz, tou.tou_bz);

       display_field (mamain1, &aufform.mask[toufield], 0, 0);
       display_field (mamain1, &aufform.mask[tou_bz_pos], 0, 0);
       SetCurrentFocus (currentfield);

       return 0;
}

int VertrQuery (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;
       int vertrfield;

       vertr.mdn = aufk.mdn;
//       vertr.fil = aufk.fil;
       vertr.fil = 0;
       ret = QVtrClass.queryvertr (mamain1);
       set_fkt (dokey5, 5);
       set_fkt (doliste, 6);
       SetFkt (6, liste, KEY6);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       vertrfield = GetItemPos (&aufform, "vertr");
       sprintf (vertrs, "%ld", vertr.vertr);
	   aufk.vertr = vertr.vertr;
       display_field (mamain1, &aufform.mask[vertrfield], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

static void FillAufArt  (char *buffer)
/**
Werte fuer Auswahl fuellen.
**/
{
         if (atoi (ptabn.ptwert) == 2)
         {
             strcpy (buffer, "NULL");
         }
         else if (atoi (ptabn.ptwert) == 3 &&
                  kun.zahl_art == 2)
         {
             strcpy (buffer, "NULL");
         }
         else
         {
             sprintf (buffer, "%3hd %-3s %s",
                              ptabn.ptlfnr, ptabn.ptwert,
				    		  ptabn.ptbez);
         }
}

int ShowAufArtEx (HWND hMainWindow, char *dest, char *item)
{
       char wert [80];
       SEARCHAART *SearchPtab;
	   HINSTANCE hMainInst;


       hMainInst = (HINSTANCE) GetWindowLong (hMainWindow, GWL_HINSTANCE);
       SearchPtab = new SEARCHAART (item);
       SearchPtab->SetParams (hMainInst, hMainWindow); 
       SearchPtab->Setawin (hMainWindow);
                                                  
       SearchPtab->Search ();
       if (SearchPtab->GetKey (wert) == TRUE)
       {
           delete SearchPtab;
           strcpy (dest, wert);
		   return 0;
       }
       delete SearchPtab;
       return -1;
}




int ShowAufArt (void)
{

       int ret = ShowAufArtEx (hMainWindow, auf_art, "auf_art"); 
       SetCurrentFocus (currentfield);
       if (ret == -1)
       {
              return 0;
       }
	   return testaufart0 ();

       ptab_class.SetValuesProc (FillAufArt);
	   ptab_class.Show ("auf_art");
       ptab_class.SetValuesProc (NULL);
	   if (syskey == KEY5 || syskey == KEYESC)
	   {
		   return 0;
	   }
	   strcpy (auf_art, ptabn.ptwert);
	   return testaufart0 ();
}

int setkey9aufart ()
/**
Auswahl fuer Touren setzen.
**/
{
       SetFkt (9, ansehen, KEY9);
       set_fkt (ShowAufArt, 9);
       return 0;
}


int setkey9tou ()
/**
Auswahl fuer Touren setzen.
**/
{
       akt_tou = atol (tour);
       SetFkt (9, auswahl, KEY9);
       set_fkt (TouQuery, 9);
       set_fkt (InfoTou, 4);
       return 0;
}


int setkey9vertr ()
/**
Auswahl fuer Touren setzen.
**/
{
       akt_tou = atol (tour);
       vertr.mdn = aufk.mdn;
       vertr.fil = aufk.fil;
       SetFkt (9, auswahl, KEY9);
       set_fkt (VertrQuery, 9);
       set_fkt (InfoVertr, 4);
       return 0;
}

int lesetou (void)
/**
Tour lesen
**/
{
       int dsqlstatus;
       char *tou_bz;
       char *touval;
       int tou_pos;
       int tou_bz_pos;


       if (akt_tou == atol (tour)) return 0;

       tou_pos    = GetItemPos (&aufform, "tou");
       tou_bz_pos = GetItemPos (&aufform, "tou_bz");
       touval    = aufform.mask[tou_pos].item->GetFeldPtr ();
       tou_bz = aufform.mask[tou_bz_pos].item->GetFeldPtr ();
       tou.tou = atol (touval);
       if (tou.tou < tour_div)
       {
           tou.tou *= tour_div;
       }
       sprintf (touval, "%ld", tou.tou);
       tou.tou /= tour_div;
       if (tou.tou == 0l)
       {
           return 0;
       }
       dsqlstatus = Tour.dbreadfirst ();
       if (dsqlstatus == 0)
       {
           strcpy (tou_bz, tou.tou_bz);
       }
       return 0;
}


BOOL ldatdiffOK (void)
/**
Lieferdatum testen.
**/
{
		 char ldatum [12];
		 int ldpos;
		 long dat_von, dat_bis, lidat;

		 ldpos = GetItemPos (&aufform, "lieferdat");
		 if (ldpos > -1)
		 {
                display_field (mamain1, &aufform.mask[ldpos], 0, 0);
		 }
		 sysdate (ldatum);
		 dat_von = dasc_to_long (ldatum) - ldatdiffminus;
		 dat_bis = dasc_to_long (ldatum) + ldatdiffplus;
		 lidat = dasc_to_long (ldat);
    
		 if ((lidat < dat_von) || (lidat > dat_bis))
		 {
			 if (mench == 0) //LAC-150
			 {
				disp_mess ("Abweichung beim Lieferdatum ist zu gro�", 2);
				SetCurrentField (currentfield);
				return 0;
			 }
		 }
		 return KommdatOk ();
}


int testldat0 (void)
/**
Lieferdatum testen.
**/
{
         long tou;
		 int toupos;
		 int toukrzpos;
		 char ldatum [12];
		 long dat_von, dat_bis, lidat;

         if (testkeys ()) return 0;

		 sysdate (ldatum);
		 dat_von = dasc_to_long (ldatum) - ldatdiffminus;
		 dat_bis = dasc_to_long (ldatum) + ldatdiffplus;
		 lidat = dasc_to_long (ldat);

		 if ((lidat < dat_von) || (lidat > dat_bis))
		 {
			 disp_mess ("Abweichung beim Lieferdatum ist zu gro�", 2);
		     SetCurrentField (currentfield);
			 return 0;
		 }

         GetWochenTag ();
		 if (strcmp (ldat, akt_ldat) == 0)
		 {
		              testkommdat ();
		              return 1;
		 }

		 if (IsC_C () == 0 && wo_tou_par)
		 {
/*
                tou = QTClass.DatetoTouDir (aufk.mdn, aufk.fil, atoi (kunfil), 
                                  atol (kuns), ldat);
*/

// Tour mit Filiale 0 bearbeiten

                toupos = GetItemPos (&aufform, "tou");
                if ((aufform.mask[toupos].attribut & REMOVED) == 0)
                {
                   tou = QTClass.DatetoTouDir (aufk.mdn, 0, atoi (kunfil), 
                                  atol (kuns), ldat);
                   current_form = &aufform;
                   sprintf (tour, "%ld", tou);
                   display_field (mamain1, &aufform.mask[toupos], 0, 0);
				   testtou (0); 
                   if (tou || tour_zwang) //LAC-10 || tour_zwang mit rein immer pr�fen bei tour_zwang wird dann gemeckert
                   {
			             testtou (0);
                   }
		           else
                   {
                         sprintf (tour_krz, " "); 
                         toukrzpos = GetItemPos (&aufform, "tou_bz");
                         display_field (mamain1, &aufform.mask[toukrzpos], 0, 0);
                   }
				}
		 }
		 testkommdat ();
		 return 1;
}

int testldat ()
{

         if (testldat0 () == 0) return 0;
		 if (PosOK == FALSE && listedirect == 2)
		 {
			 PostMessage (mamain1, WM_KEYDOWN, VK_F6, 0l);
		 }
		 return 0;
}


int testtou (int Dmode)
/**
Tour lesen
**/
{
       int dsqlstatus;
       char *tou_bz;
       char *touval;
       int tou_pos;
       int tou_bz_pos;
       int ldatpos;


       set_fkt (InfoKun, 4);
       if (akt_tou && akt_tou == atol (tour)) return 0;

       tou_pos    = GetItemPos (&aufform, "tou");
       tou_bz_pos = GetItemPos (&aufform, "tou_bz");
       touval    = aufform.mask[tou_pos].item->GetFeldPtr ();
       tou_bz = aufform.mask[tou_bz_pos].item->GetFeldPtr ();
       tou.tou = atol (touval);
       if (tou.tou < (long) tour_div)
       {
           tou.tou *= tour_div;
       }
       sprintf (touval, "%ld", tou.tou);
       tou.tou /= tour_div;
       if (tou.tou == 0l)
       {
	            if (tour_zwang)
				{
		           disp_mess ("Tour 0 ist nicht erlaubt", 2);
				   SetCurrentField (currentfield);
				}
                return 0;
       }
       dsqlstatus = Tour.dbreadfirst ();
	   if (dsqlstatus == 100 && tour_zwang)
	   {
		           disp_mess ("Tour nicht gefunden", 2);
				   SetCurrentField (currentfield);
				   return 0;
	   }
       if (dsqlstatus == 0)
       {
           strcpy (tou_bz, tou.tou_bz);
       }
       current_form = &aufform;
       tou.tou = atol (touval);
       current_form = &aufform;
	   if (Dmode)
	   {
/*
                  QTClass.ToutoDate (aufk.mdn, aufk.fil, atoi (kunfil), 
                           atol (kuns), ldat, tou.tou);
*/

// Touren zuordnung mit Filiale 0 lesen

                  QTClass.ToutoDate (aufk.mdn, 0, atoi (kunfil), 
                           atol (kuns), ldat, tou.tou);
                  ldatpos = GetItemPos (&aufform, "lieferdat");
                  strcpy (kdat, ldat);
                  display_field (mamain1, &aufform.mask[ldatpos], 0, 0);
	   }
       display_field (mamain1, &aufform.mask[tou_pos], 0, 0);
       display_field (mamain1, &aufform.mask[tou_bz_pos], 0, 0);
       return 0;
}

int testtou0 (void)
{
       if (testkeys ()) return 0;
       return testtou (0);
}

int testvertr (void)
/**
Tour lesen
**/
{
       int dsqlstatus;
       char *vertr_krz;
       char *vertrval;
       int vertr_pos;
       int vertr_bz_pos;

       set_fkt (InfoKun, 4);

       vertr_pos    = GetItemPos (&aufform, "vertr");
       vertr_bz_pos = GetItemPos (&aufform, "vertr_krz");
       vertrval    = aufform.mask[vertr_pos].item->GetFeldPtr ();
       vertr_krz   = aufform.mask[vertr_bz_pos].item->GetFeldPtr ();
       vertr.vertr = atol (vertrval);
       if (vertr.vertr == 0l)
       {
 	       aufk.vertr = vertr.vertr;
           strcpy (vertr_krz, "");
           display_field (mamain1, &aufform.mask[vertr_pos], 0, 0);
           display_field (mamain1, &aufform.mask[vertr_bz_pos], 0, 0);
           return 0;
       }
       dsqlstatus = vertr_class.lese_vertr (aufk.mdn, aufk.fil,
                                            vertr.vertr);
       if (dsqlstatus != 0)
       {
              dsqlstatus = vertr_class.lese_vertr (aufk.mdn, 0,
                                                   vertr.vertr);
       }
       if (dsqlstatus != 0)
       {
                   print_mess (2, "Vertreter %ld ist nicht angelegt",
                                   vertr.vertr);
                   SetCurrentField (currentfield);
                   return 0;
       }
	   aufk.vertr = vertr.vertr;
       strcpy (vertr_krz, _adr.adr_krz);
//       display_field (mamain1, &aufform.mask[vertr_pos], 0, 0);
//       display_field (mamain1, &aufform.mask[vertr_bz_pos], 0, 0);
       display_form (mamain1, &aufform, 0, 0);
       return 0;
}

int testvertr0 (void)
{
       if (testkeys ()) return 0;
       return testvertr ();
}

int getaufart (void)
{
      int dsqlstatus;
	   
	  char wert [5];

	  sprintf (wert, "%ld", atoi (auf_art));
      if (atoi (wert) == 2)
      {
          return -1;
      }
      else if (atoi (wert) == 3 && kun.zahl_art == 2)
      {
          return -1;
      }
	  ptabn.ptbez[0] = 0;
      dsqlstatus = ptab_class.lese_ptab ("auf_art", wert);
      if (dsqlstatus != 0)
      {
          return -1;
      }
	  strcpy (auf_art_txt, ptabn.ptbez);
	  aufpListe.SetAufArt ((short) atoi (auf_art));
	  return 0;
}

int testaufart0 (void)
{
	  int apos;

	  apos = GetItemPos (&aufform, "auf_art");
	  if (getaufart () == -1)
      {
               print_mess (2, "Falsche Auftragsart");
	  	       SetCurrentField (apos);
			   SetCurrentFocus (apos);
               return 0;
      }
	  if (apos >= 0)
	  {
               display_field (mamain1, &aufform.mask[apos]);
	  }
	  apos = GetItemPos (&aufform, "auf_art_txt");
	  if (apos >= 0)
	  {
               display_field (mamain1, &aufform.mask[apos]);
	  }
	  return 0;
}

int testaufart (void)
{
       if (testkeys ()) return 0;

	   SetFkt (9, leer, NULL);
	   set_fkt (NULL, 9);
       return testaufart0 ();
}

int ShopEnter (void)
{
	   int aufpos; 
	    
	   aufpos = GetItemPos (&aufformk, "orders_id");
	   if (aufpos == -1)
	   {
		   return 0;
	   }
	   SetCurrentField (aufpos);
       SetCurrentFocus (aufpos);
	   return 0;
}

int SwitchKunFil (void)
{
       if (atoi (kunfil) == 0)
       {
           sprintf (kunfil, "1");
           ikunfil_bz.SetFeldPtr ("Filiale");
       }
       else
       {
           sprintf (kunfil, "0");
           ikunfil_bz.SetFeldPtr ("Kunde");
       }
       aufk.kun_fil = atoi (kunfil);
       display_field (mamain1, &aufformk.mask[2], 0, 0);
       display_field (mamain1, &aufformk.mask[3], 0, 0);
       return 0;
}

int lese_shop (void)
{
       int ret;
       if (testkeys ()) return 0;
	   if (atoi(orders_id) == 0) return 0;
	   aufk.mdn = atoi (mdn);
	   aufk.fil = atoi (fil);
       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain1);
	   ls_class.PrepareAufQueryShop (atoi(orders_id));
       if (ls_class.ShowBuAufQueryEx (mamain1, 0) == 100)
	   {
               print_mess (2, "Es wurde kein Auftrag zu dieser Bestellnummer %ld gefunden", atoi(orders_id));
               SetCurrentFocus (currentfield);
			   return 0;
	   }


       set_fkt (dokey5, 5);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       sprintf (auftrag, "%ld", aufk.auf);
       display_field (mamain1, &aufformk.mask[0], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
	return 0;
}

int QueryAng (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;

	   if (GetKeyState (VK_SHIFT) < 0)
	   {
		   CallTele2 ();
		   return 0;
	   }
	   aufk.mdn = atoi (mdn);
	   aufk.fil = atoi (fil);
       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain1);
       ret = QClass.queryang (mamain1);
       set_fkt (dokey5, 5);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }

	   aufk.mdn = atoi (mdn);
	   aufk.fil = atoi (fil);
	   aufk.auf = atol (auftrag);
	   AufKompl.AngtoAuf ();
	   AufToForm ();
	   IsAng = TRUE;
       display_field (mamain1, &aufformk.mask[0], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}


int QueryAuf (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;

	   aufk.mdn = atoi (mdn);
	   aufk.fil = atoi (fil);
       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain1);
	   if (IsC_C () && cfgccmarkt == 1)
	   {
                 ret = QClass.queryaufex (mamain1, 0);
	   }
	   else if (cfgccmarkt == 2)
	   {
                 ret = QClass.queryaufex (mamain1, 1);
	   }
	   else if (auftou == 1)
	   {
                 ret = QClass.queryauftou (mamain1);
	   }
	   else if (auftou == 2)
	   {
                 ret = QClass.queryauflietou (mamain1);
	   }
	   else
	   {
                 ret = QClass.queryauf (mamain1);
	   }
       set_fkt (dokey5, 5);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }
       sprintf (auftrag, "%ld", aufk.auf);
       display_field (mamain1, &aufformk.mask[0], 0, 0);
       PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}

  
void ShowAuf (void)
/**
Autrag anzeigen.
**/
{
       extern short sql_mode; 

       Mess.CloseMessage ();
       akt_auf = 0l;
       sprintf (auftrag, "0");
       no_break_end ();
       set_fkt (QueryAuf, 10);
       SetFkt (10, auswahl, KEY10);
	   if (mench == 2) beginwork ();
       enter_form (mamain1, &aufformk, 0, 0);
	   if (mench == 2) commitwork ();
       aufpListe.DestroyWindows (); 
       CloseControls (&aufform);
       CloseControls (&aufformk);
       set_fkt (NULL, 12);
       CTelefon.bmp = btelefoni;
       ActivateColButton (mamain1, 
                                      &buform, 0, -1, 1);
       ActivateColButton (mamain1, 
                                      &buform, 1, -1, 1);
       ActivateColButton (mamain1, 
                                      &buform, 2, -1, 1);

       EnableMenuItem (hMenu, CallT1,    MF_GRAYED);
       EnableMenuItem (hMenu, CallT2,    MF_GRAYED);
       EnableMenuItem (hMenu, CallT3,    MF_GRAYED);

       MoveButtons ();
       return;
}

BOOL AufNrOK (void)
/**
generierte Auftragsnummer testen.
**/
{
       char buffer [256];

       if (auto_nr.nr_nr == 0l) return FALSE;

	   if (lutz_mdn_par)
	   {
                 sprintf (buffer, "select auf from aufk where auf = %ld",
                                   auto_nr.nr_nr);
	   }
	   else
	   {
                 sprintf (buffer, "select auf from aufk where mdn = %d "
                                              "and   fil = %d "
                                              "and auf =   %ld",
                                   atoi (mdn), atoi (fil), auto_nr.nr_nr);
	   }

       if (DbClass.sqlcomm (buffer) != 100) return FALSE;
       return TRUE;
}
      

void GenAufNr (void)
/**
Auftragsnummer generieren.
**/
{
       extern short sql_mode; 
	   int i;
	   static int MAXWAIT = 100;
       MSG msg;

	   if (mench > 0 ) return ; //LAC-150
       beginwork ();
       sql_mode = 1;
	   i = 0;
       while (TRUE)
       {
		        if (lutz_mdn_par)
				{
					sprintf (mdn, "%hd", 0);
					sprintf (fil, "%hd", 0);
					aufk.mdn = aufk.fil = 0;
				}
//                dsqlstatus = AutoClass.nvholid (atoi (mdn), atoi (fil), "auf");

// Auftrags-Nummer vorerst immer mit Filiale 0 holen

                dsqlstatus = AutoClass.nvholid (atoi (mdn), 0, "auf");

				if (dsqlstatus == -1)
				{
/*
					        DbClass.sqlin ((short *) &aufk.mdn, 1, 0); 
					        DbClass.sqlin ((short *) &aufk.fil, 1, 0); 
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"auf\" "
								              "and mdn = ? and fil = ?");
*/

					        DbClass.sqlin ((short *) &aufk.mdn, 1, 0); 
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"auf\" "
								              "and mdn = ? and fil = 0");
							dsqlstatus = 100;
				} 		
					   
                if (dsqlstatus == 100)
                {
/*
                           dsqlstatus = AutoClass.nvanmprf (atoi (mdn),
                                                  atoi (fil),
                                                  "auf",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");
*/
                           dsqlstatus = AutoClass.nvanmprf (atoi (mdn),
                                                  0,
                                                  "auf",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
/*
                                  dsqlstatus = AutoClass.nvholid (atoi (mdn),
                                                        atoi (fil),
                                                        "auf");
*/
                                  dsqlstatus = AutoClass.nvholid (atoi (mdn),
                                                         0,
                                                        "auf");
                           }
                }

                if (dsqlstatus == 0 && AufNrOK ()) break;
                Sleep (50);
                if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
                {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                }

				i ++;
				if (i > MAXWAIT) break;
       }
       sql_mode = 0;
       commitwork ();
       FreeAufNr = TRUE;
}


void EnableArrows (form *frm, BOOL mode)
/**
Pfeilbuttons aktivieren oder deaktivieren.
**/
{
	   int i;

	   for (i = 0; i < frm->fieldanz; i ++)
	   {
		   if (frm->mask[i].item->GetItemName () && 
			   strcmp (frm->mask[i].item->GetItemName (), "arrdown") == 0)
		   {
			   if (mode == FALSE)
			   {
			             frm->mask[i].picture = "";
						 frm->mask[i].attribut = REMOVED;
						 CloseControl (frm, i);
			   }
			   else
			   {
						 frm->mask[i].attribut = BUTTON;
			             frm->mask[i].picture = "B";
			   }
			   EnableWindow (frm->mask[i].feldid, mode);
		   }
	   }
       if (tou_par == 0)
       {
	       aufform.mask[GetItemPos (&aufform, "tou") + 1].attribut = REMOVED;
       }
}


void EnterAuf (void)
/**
Autrag bearbeiten.
**/
{
	 

       Mess.CloseMessage ();
       akt_auf = 0l;
       while (TRUE)
       {
                if (lutz_mdn_par)
				{
		            sprintf (mdn, "%hd", 0);
		            sprintf (fil, "%hd", 0);
					aufk.mdn = aufk.fil = 0;
				}

		        if (autokunfil) strcpy (kunfil, "0"); 
				if (atoi (fil) > 0) strcpy (kunfil, "0");
                if (atoi (kunfil) == 0)
				{
                    ikunfil_bz.SetFeldPtr ("Kunde");
				}
                else
				{
                    ikunfil_bz.SetFeldPtr ("Filiale");
				}
                break_end ();
				IsAng = FALSE;
                GenAufNr ();
				if (auto_nr.nr_nr == 0l && mench == 0)  //LAC-150
				{
					disp_mess ("Es konnte keine Auftragsnummer generiert werden", 2);
					return;
				}
                sprintf (auftrag, "%ld", auto_nr.nr_nr);
                if (mench != 1) beginwork ();
				if (atoi (fil) == 0)
				{
                        set_fkt (SwitchKunFil, 8);
                        SetFkt (8, kundefil, KEY8);
				}
                set_fkt (QueryAng, 9);
                SetFkt (9, angebote, KEY9);

                set_fkt (QueryAuf, 10);
                SetFkt (10, auswahl, KEY10);
                set_fkt (ShopEnter, 11);
                SetFkt (11, mshop, KEY11);

                DisplayAfterEnter (TRUE);
                no_break_end ();
				if (telelist || TeleTour)
				{
                      CTelefon.bmp = btelefon;
                      ActivateColButton (mamain1,  &buform, 1, 0, 1);
                      MoveButtons ();
                      EnableMenuItem (hMenu, CallT2,   MF_ENABLED);
				      temode = 0;
				      SetTeleLst ();
				}
				telekun = FALSE;
			    aufformk.before = setzeStartKun;  //LAC-184

                enter_form (mamain1, &aufformk, 0, 0);

/*
Zum Dauertest der Masken enter_form asukommentieren.

create_enter_form (mamain1, &aufformk, 0, 0);
MSleep (100);
CloseControls (&aufformk);
display_form (mamain1, &aufformk, 0, 0);
*/

                set_fkt (NULL, 8);
                SetFkt (8, leer, 0);
                set_fkt (NULL, 9);
                SetFkt (9, leer, 0);
                set_fkt (NULL, 10);
                SetFkt (10, leer, 0);
                if (syskey == KEY5 || syskey == KEYESC) break;

                if (NewRec == 0 && GroupOK ())
                {
                        if (KomplettMode < 2) 
						{
				                 set_fkt (SetKomplett, 8);
                                 SetFkt (8, komplett, KEY8);
						}
                }
				EnableArrows (&aufformk, FALSE);
                eingabedaten ();
                commitwork ();
				EnableArrows (&aufformk,TRUE);
                CTelefon.bmp = btelefoni;
                ActivateColButton (mamain1, 
                                      &buform, 0, -1, 1);
                ActivateColButton (mamain1, 
                                      &buform, 1, -1, 1);
                ActivateColButton (mamain1, 
                                      &buform, 2, -1, 1);
                EnableMenuItem (hMenu, CallT1,   MF_GRAYED);
                EnableMenuItem (hMenu, CallT2,   MF_GRAYED);
                EnableMenuItem (hMenu, CallT3,   MF_GRAYED);

                MoveButtons ();
                set_fkt (NULL, 6);
                set_fkt (NULL, 7);
                set_fkt (NULL, 9);
                SetFkt (6, leer, 0);
                SetFkt (7, leer, 0);
                SetFkt (9, leer, 0);
        }
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        SetFkt (6, leer, 0);
        SetFkt (7, leer, 0);
        Mess.CloseMessage ();
        return;
}

void DisableMe (void)
{
     ToolBar_SetState(hwndTB,IDM_WORK, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_WORK,  MF_GRAYED);

     ToolBar_SetState(hwndTB,IDM_SHOW, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_SHOW,  MF_GRAYED);

     ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_PRINT,  MF_GRAYED);

}

void EnableMe (void)
{

     ToolBar_SetState(hwndTB, IDM_WORK, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_WORK, MF_ENABLED);

     ToolBar_SetState(hwndTB, IDM_SHOW, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_SHOW, MF_ENABLED);

     ToolBar_SetState(hwndTB, IDM_PRINT, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_PRINT, MF_ENABLED);

}


void ShowBm (void)
{
    RECT rect;
    TEXTMETRIC tm;
    int x, y, cx, cy;
    static int abmp = 0;
    char buffer [80];

    abmp ++;
    if (abmp > 3) abmp = 1;
    GetWindowRect (hMainWindow, &rect);
    stdfont ();
    SetTmFont (hMainWindow, &tm);

    cx = 5;
    cy = 2;
    x = (rect.right - 2) / tm.tmAveCharWidth;
    x -= cx;
    y = rect.top;
    y = (int) (double) ((double) y / 
                                (double)(1 + (double) 1/3));
    y /= tm.tmHeight;
    y += 3;
    sprintf (buffer, "-nm2 -z%d -s%d -h%d -w%d "
             "c:\\user\\fit\\bilder\\artikel\\%d.bmp",
             y, x, cy, cx, abmp);
    _ShowBm (buffer);
}


void EingabeMdnFil (void)
{
       extern MDNFIL cmdnfil;

       Mess.CloseMessage ();
       DisableMe ();
	   if (fil_lief_par == FALSE)
	   {
                 cmdnfil.SetFilialAttr (REMOVED);
	   }
       cmdnfil.SetNoMdnNull (TRUE);
	   if (StartMdn != 0)
	   {
		   sprintf (mdn, "%hd", StartMdn);
           cmdnfil.SetMandantAttr (READONLY);
	   }
	   while (TRUE)
       {
           set_fkt (dokey5, 5);
		   if (lutz_mdn_par == 0 &&  
			         cmdnfil.eingabemdnfil (mamain1, mdn, fil) == -1)
           {
			         break;
           }
		   if (sys_ben.mdn == 0)
		   {
		              cmdnfil.SetMandantAttr (EDIT);
		   }

		   if (PartyMdn == (short) atoi (mdn))
		   {
			   EnablePartyService (TRUE);
			   CheckPartyService (TRUE);
		   }
		   else
		   {
			   EnablePartyService (FALSE);
		   }
// Nicht aktiv    ShowBm ();
		   SetAnsehen ();
            switch (mench)
           {
                case 0:
                case 1 :   //LAC-150
                      EnterAuf  ();
                      break;
//LAC-150                case 1 :
                case 2 :
                case 3 :
                      ShowAuf  ();
                      CTelefon.bmp = btelefoni;
                      break;
           }
		   if (lutz_mdn_par) break;
	   }
       cmdnfil.CloseForm ();
       EnableMe ();
}


static int testber (void)
/**
Abfrage in Query-Eingabe.
**/
{
	    int savecurrent;

		savecurrent = currentfield;
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
				case KEY7 :
					   ChoisePrinter0 ();
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
					   return 1;
        }

		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY7)
				{
					   syskey = KEY7; 
					   ChoisePrinter0 ();
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
					   return 1;
				}
		}
        return 0;
}

BOOL LockBereich (char *where)
/**
Druckbereich sperren.
Bereich fuer Lieferscheinuebergabe sperren. 
**/
{
	char sqls [1028];
	int cursor;
//	int cursor_pos;
	int cursorupd;
	int dsqlstatus;
	extern short sql_mode;

	DbClass.sqlout ((short *) &aufk.mdn, 1, 0);
	DbClass.sqlout ((short *) &aufk.fil, 1, 0);
	DbClass.sqlout ((long *) &aufk.auf, 2, 0);
	sprintf (sqls, "select mdn, fil, auf from aufk %s and delstatus = 0", where);
	cursor = DbClass.sqlcursor (sqls);

	DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
	DbClass.sqlin ((short *) &aufk.fil, 1, 0);
	DbClass.sqlin ((long *) &aufk.auf, 2, 0);
	cursorupd = DbClass.sqlcursor ("select auf from aufk where mdn = ? "
		                            "and fil = ? "
									"and auf = ? for update");

	beginwork ();
    sql_mode = 1;
	while (DbClass.sqlfetch (cursor) == 0)
	{
		 DbClass.sqlopen (cursorupd);
		 dsqlstatus = DbClass.sqlfetch (cursorupd);
		 if (dsqlstatus < 0)
		 {
			 print_mess (2, "Auftrag %ld wird im Moment bearbeitet", aufk.auf);
			 break;
		 }
	}
	DbClass.sqlclose (cursor);
	DbClass.sqlclose (cursorupd);
	commitwork ();
    sql_mode = 0;
    if (dsqlstatus < 0)
	{
		  return FALSE;
	}
	return TRUE;
}

void RemoveAbt (form *berformdr)
{
	static BOOL OK = FALSE;

	if (OK) return;

    SetItemAttr (berformdr, "abtlabel", REMOVED); 			   
    SetItemAttr (berformdr, "abt_von", REMOVED); 			   
    SetItemAttr (berformdr,"abt_bis", REMOVED); 			   
	berformdr->mask[berformdr->fieldanz -1].pos[0] -= 2;
	berformdr->mask[berformdr->fieldanz -2].pos[0] -= 2;
	berformdr->mask[berformdr->fieldanz -3].pos[0] -= 2;
    OK = TRUE;
}

BOOL DropLabel (form *Frm, char *Item, int LabelAnz)
{
         int lpos = GetItemPos (Frm, Item);

         if (lpos == -1)
         {
             return FALSE;
         }

         if (Frm->mask[lpos].attribut == REMOVED)
         {
             return FALSE;
         }

         Frm->mask[lpos].attribut = REMOVED;
         for (int i = lpos + 1; i < LabelAnz; i ++)
         {
             Frm->mask[i].pos[0] -= 2;
         }
         return TRUE;
}


BOOL DropText (form *Frm, char *Item, int TextAnz)
{
         int lpos = GetItemPos (Frm, Item);

         if (lpos == -1)
         {
             return FALSE;
         }

         if (Frm->mask[lpos].attribut == REMOVED)
         {
             return FALSE;
         }


         Frm->mask[lpos].attribut = REMOVED;
         Frm->mask[lpos + 1].attribut = REMOVED;
         for (int i = lpos + 2; i < TextAnz; i ++)
         {
             Frm->mask[i].pos[0] -= 2;
         }
         return TRUE;
}


void BereichsAuswahl (void)
/**
Bereich fuer Druck oder Lieferscheinuebergabe eingeben.
**/
{
        HANDLE hMainInst;
		HWND hWnd;
		char where [1028];

        static char mdn_von [5];
        static char mdn_bis [5];
        static char fil_von [5];
        static char fil_bis [5];
        static char auf_von [9];
        static char auf_bis [9];
        static char kun_von [9];
        static char kun_bis [9];
        static char stat_von [3];
        static char stat_bis [3];
        static char dat_von [12];
        static char dat_bis [12];
        static char tou_von [10];
        static char tou_bis [10];
        static char abt_von [6];
        static char abt_bis [6];
        static int DrkRows = 19; 
        static int LuRows = 19; 
		long max_auf;

        static ITEM iOK ("", "     OK     ", "", 0);
        static ITEM iCA ("", "  Abbrechen ", "", 0);
        static ITEM iDW ("", "Druckerwahl ",  "", 0);


		static ITEM ivon     ("", "von",             "", 0);
		static ITEM ibis     ("", "bis",             "", 0);
		static ITEM iMdn     ("mdnlabel", "Mandant",         "", 0);
		static ITEM iFil     ("fillabel", "Filiale",         "", 0);
		static ITEM iAuf     ("auflabel", "Auftrag",         "", 0);
		static ITEM iKun     ("kunlabel", "Kunde",           "", 0);
		static ITEM iStat    ("statlabel", "Auftrags-Status", "", 0);
		static ITEM iDat     ("datlabel", "Lieferdatum",     "", 0);
		static ITEM iTour    ("toulabel", "Tour",            "", 0);
		static ITEM iAbt     ("abtlabel", "Abteilung", "", 0);

        static ITEM imdn_von ("mdn_von", mdn_von,  "", 0);
        static ITEM imdn_bis ("mdn_bis", mdn_bis,  "", 0);
        static ITEM ifil_von ("fil_von", fil_von,  "", 0);
        static ITEM ifil_bis ("fil_bis", fil_bis,  "", 0);
        static ITEM iauf_von ("auf_von", auf_von,  "", 0);
        static ITEM iauf_bis ("auf_bis", auf_bis,  "", 0);
        static ITEM ikun_von ("kun_von", kun_von,  "", 0);
        static ITEM ikun_bis ("kun_bis", kun_bis,  "", 0);
        static ITEM istat_von ("stat_von", stat_von,  "", 0);
        static ITEM istat_bis ("stat_bis", stat_bis,  "", 0);
        static ITEM idat_von ("dat_von", dat_von,  "", 0);
        static ITEM idat_bis ("dat_bis", dat_bis,  "", 0);
        static ITEM itou_von ("tou_von", tou_von,  "", 0);
        static ITEM itou_bis ("tou_bis", tou_bis,  "", 0);
        static ITEM iabt_von ("abt_von", abt_von,  "", 0);
        static ITEM iabt_bis ("abt_bis", abt_bis,  "", 0);


        static field _berform[] = {
           &ivon,     0, 0, 1,18,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &ibis,     0, 0, 1,40,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iMdn,     0, 0, 1, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iFil,     0, 0, 3, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iAuf,     0, 0, 5, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iKun,     0, 0, 7, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iStat,    0, 0, 9, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iDat,     0, 0,11, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iTour,    0, 0,13, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 

           &imdn_von, 5, 0, 1,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &imdn_bis, 5, 0, 1,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_von, 5, 0, 3,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_bis, 5, 0, 3,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &iauf_von, 9, 0, 5,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &iauf_bis, 9, 0, 5,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ikun_von, 9, 0, 7,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ikun_bis, 9, 0, 7,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &istat_von, 2, 0, 9,18, 0, "%1d",    EDIT, 0 ,testber,0, 
           &istat_bis, 2, 0, 9,40, 0, "%1d",    EDIT, 0 ,testber,0, 
           &idat_von, 11, 0,11,18, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 
           &idat_bis, 11, 0,11,40, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 
           &itou_von,  9, 0,13,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &itou_bis,  9, 0,13,40, 0, "%8d",    EDIT, 0 ,testber,0, 

           &iOK,     13, 0,16,15, 0, "", BUTTON, 0, testber,KEY12,
           &iCA,     13, 0,16,32, 0, "", BUTTON, 0, testber,KEY5,
		};

        static form berform = {25, 0, 0, _berform, 
			                    0, 0, 0, 0, NULL};
        static int lulabelanz = 9;
        
        static field _berformdr[] = {
           &ivon,     0, 0, 1,18,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &ibis,     0, 0, 1,40,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iMdn,     0, 0, 1, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iFil,     0, 0, 3, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iAuf,     0, 0, 5, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iKun,     0, 0, 7, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iStat,    0, 0, 9, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iDat,     0, 0,11, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iTour,    0, 0,13, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iAbt,     0, 0,15, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 

           &imdn_von, 5, 0, 1,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &imdn_bis, 5, 0, 1,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_von, 5, 0, 3,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &ifil_bis, 5, 0, 3,40, 0, "%4d",    EDIT, 0 ,testber,0, 
           &iauf_von, 9, 0, 5,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &iauf_bis, 9, 0, 5,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ikun_von, 9, 0, 7,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ikun_bis, 9, 0, 7,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &istat_von, 2, 0, 9,18, 0, "%1d",    EDIT, 0 ,testber,0, 
           &istat_bis, 2, 0, 9,40, 0, "%1d",    EDIT, 0 ,testber,0, 
           &idat_von, 11, 0,11,18, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 
           &idat_bis, 11, 0,11,40, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 
           &itou_von,  9, 0,13,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &itou_bis,  9, 0,13,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &iabt_von,  5, 0,15,18, 0, "%4d",    EDIT, 0 ,testber,0, 
           &iabt_bis,  5, 0,15,40, 0, "%4d",    EDIT, 0 ,testber,0, 

           &iOK,     15, 0,17, 6, 0, "", BUTTON, 0, testber,KEY12,
           &iCA,     15, 0,17,23, 0, "", BUTTON, 0, testber,KEY5,
           &iDW,     15, 0,17,40, 0, "", BUTTON, 0, testber,KEY7,
		};

        static form berformdr = {29, 0, 0, _berformdr, 
			                    0, 0, 0, 0, NULL};

        static int drklabelanz = 10;


        HWND berwin;
		int savefield;
		form *savecurrent;

        NoClose = TRUE;
        hWnd = AktivWindow;
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testber, 5);
        set_fkt (testber, 11);
        set_fkt (testber, 12);

		sysdate (dat_von);
		sysdate (dat_bis);
		strcpy (mdn_von, "1");
		strcpy (mdn_bis, "9999");
		strcpy (fil_von, "0");
		strcpy (fil_bis, "9999");
		strcpy (auf_von, "1");
		strcpy (auf_bis, "99999999");
		strcpy (kun_von, "1");
		strcpy (kun_bis, "99999999");
		if (mench == 4)
		{
		         strcpy (stat_von, stat_deff_von);
		         strcpy (stat_bis, stat_deff_bis);
		}
		else
		{
		         strcpy (stat_von, stat_defp_von);
		         strcpy (stat_bis, stat_defp_bis);
		}
		strcpy (tou_von, tou_def_von);
		strcpy (tou_bis, tou_def_bis);
		strcpy (abt_von, abt_def_von);
		strcpy (abt_bis, abt_def_bis);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);

        if (DrkKunRemoved)
        {
             BOOL ret = DropLabel (&berformdr, "kunlabel", drklabelanz);
             if (ret)
             {
                   ret = DropText (&berformdr,  "kun_von" , berformdr.fieldanz);
             }
             if (ret)
             {
                   DrkRows -= 2;
             }
        }

        if (AbtRemoved)
        {
             BOOL ret = DropLabel (&berformdr, "abtlabel", drklabelanz);
             if (ret)
             {
                   ret = DropText (&berformdr,  "abt_von" , berformdr.fieldanz);
             }
             if (ret)
             {
                   DrkRows -= 2;
             }
        }

        if (LuKunRemoved)
        {
             BOOL ret = DropLabel (&berform, "kunlabel", lulabelanz);
             if (ret)
             {
                   ret = DropText (&berform,  "kun_von" , berform.fieldanz);
             }
             if (ret)
             {
                   LuRows -= 2;
             }
        }

		if (mench == 4)
		{
               berwin = OpenWindowChC (LuRows, 62, 6, 10, hMainInst,
                               "LS-�bergabe");
               MoveZeWindow (hMainWindow, berwin);
               enter_form (berwin, &berform, 0, 0);
		}
/*
		else if (mench == 3 && AbtRemoved)
		{
			   RemoveAbt (&berformdr);
			    
               berwin = OpenWindowChC (DrkRows, 62, 6, 10, hMainInst,
                               "Druck K-Liste");
               MoveZeWindow (hMainWindow, berwin);
               enter_form (berwin, &berformdr
				   , 0, 0);
		}
*/

//		else if (mench == 3 && AbtRemoved == FALSE)
		else if (mench == 3)
		{
			    
               berwin = OpenWindowChC (DrkRows, 62, 6, 10, hMainInst,
                               "Druck K-Liste");
               MoveZeWindow (hMainWindow, berwin);
               enter_form (berwin, &berformdr
				   , 0, 0);
		}


		SetButtonTab (FALSE);
        CloseControls (&berform);
        DestroyWindow (berwin);
        SetAktivWindow (hWnd);
		current_form = savecurrent;
		currentfield = savefield;
        restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (9);
		restore_fkt (11);
		restore_fkt (12);

		if (syskey == KEY5) 
		{
            NoClose = FALSE;
			return;
		}

/* Bei gesetztem tour_def_par pruefen, ob die Defaultgrenzen eingehalten wurden.  */


		if (tou_def_zwang && (atoi (tou_von) || atoi (tou_bis)))
		{
                 if (atol (tou_von) < atol (tou_def_von))
				 {
		                     strcpy (tou_von, tou_def_von);
				 }
                 if (atol (tou_bis) > atol (tou_def_bis))
				 {
		                     strcpy (tou_bis, tou_def_bis);
				 }
		}
					     

		AufKompl.InitAufanz ();
		sprintf (where, "where aufk.mdn between %d and %d "
			            "and   aufk.fil between %d and %d "
						"and   aufk.auf between %ld and %ld "
						"and   aufk.kun between %ld and %ld "
						"and   aufk.lieferdat between \"%s\" and \"%s\" "
						"and   aufk.auf_stat  between %d and %d "
						"and   aufk.tou  between %ld and %ld",
						atoi (mdn_von), atoi (mdn_bis),
						atoi (fil_von), atoi (fil_bis),
						atol (auf_von), atol (auf_bis),
						atol (kun_von), atol (kun_bis),
						clipped (dat_von), clipped (dat_bis),
						atoi (stat_von), atoi (stat_bis),
						atol (tou_von) * tour_div, (atol (tou_bis) + 1) * tour_div - 1);
		EnableWindow (hMainWindow, FALSE);
		if (mench == 3)
		{
			      if (LockBereich (where) == FALSE) return;
                  AufKompl.K_ListeGroup (atoi (mdn_von), atoi (mdn_bis),
						                 atoi (fil_von), atoi (fil_bis),
						                 atol (auf_von), atol (auf_bis),
						                 clipped (dat_von), clipped (dat_bis),
						                 atoi (stat_von), atoi (stat_bis),
						                 atol (tou_von) * tour_div, (atol (tou_bis) + 1) * tour_div - 1,
										 atoi (abt_von), atoi (abt_bis),
                                         atol (kun_von), atoi (kun_bis));
				   max_auf = AufKompl.Getmax_auf ();
				   if (max_auf > atol (auf_bis))
				   {
					   sprintf (auf_bis, "%8ld", max_auf);
				   }
 		           EnableWindow (hMainWindow, TRUE);
                   SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
				   SetForegroundWindow (hMainWindow);
                   UpdateWindow (hMainWindow);
										 

                  if (lsdirect)
				  {
		              sprintf (where, "where aufk.mdn between %d and %d "
			            "and   aufk.fil between %d and %d "
						"and   aufk.auf between %ld and %ld "
						"and   aufk.kun between %ld and %ld "
						"and   aufk.lieferdat between \"%s\" and \"%s\" "
						"and   auf_stat between 4 and 4 "
						"and   aufk.tou  between %ld and %ld",
						atoi (mdn_von), atoi (mdn_bis),
						atoi (fil_von), atoi (fil_bis),
						atol (auf_von), atol (auf_bis),
						atol (kun_von), atol (kun_bis),
						clipped (dat_von), clipped (dat_bis),
						atol (tou_von) * tour_div, (atol (tou_bis) + 1) * tour_div - 1);
					    mench = 4;
				  }
				  else if (abfragejn (hWnd, "Autr�ge als Lieferscheine �bergeben", "N"))
				  {
		              sprintf (where, "where aufk.mdn between %d and %d "
			            "and   aufk.fil between %d and %d "
						"and   aufk.auf between %ld and %ld "
						"and   aufk.kun between %ld and %ld "
						"and   aufk.lieferdat between \"%s\" and \"%s\" "
						"and   auf_stat between 4 and 4 "
						"and   aufk.tou  between %ld and %ld",
						atoi (mdn_von), atoi (mdn_bis),
						atoi (fil_von), atoi (fil_bis),
						atol (auf_von), atol (auf_bis),
						atol (kun_von), atol (kun_bis),
						clipped (dat_von), clipped (dat_bis),
						atol (tou_von) * tour_div, (atol (tou_bis) + 1) * tour_div - 1);
					    mench = 4;
				  }
				  else
				  {
					    AufKompl.FreeAuf ();
					    AufKompl.ShowLocks (hWnd);
				  }

		}
		if (mench == 4)
		{
			      if (LockBereich (where) == FALSE) return;
                  AufKompl.AuftoLsGroup (where);
			      AufKompl.ShowNoDrk (hWnd);
			      AufKompl.ShowLocks (hWnd);
		}
        NoClose = FALSE;
		EnableWindow (hMainWindow, TRUE);
        SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
        SetForegroundWindow (hMainWindow);
}


static HWND chwnd;

static int lstcancel (void)
{

    syskey = KEY5;
    break_enter ();
    return 0;
}


static int lstok (void)
{
    syskey = KEY12;
    mench = currentfield;
    break_enter ();
    return 0;
}


static int testwork (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

//    mench = 0;
    break_enter ();
    return 0;
}

static int testshow (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

//    mench = 1;
    break_enter ();
    return 0;
}

static int testdel (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}


static int testprint (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}

static int testfree (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}

static int testcancel (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

	syskey = KEY5;
    break_enter ();
    return 0;
}

        static ColButton LstChoise = {
                              "Auswahl", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
//                               WHITECOL,
//                               RGB (0,0,120),
                               BLACKCOL,
                               GRAYCOL,
                               -2};
   
        static ColButton cWork = {
                              "Bearbeiten", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cShow = {
                              "Anzeigen", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cDel = {
                              "L�schen", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cPrint = {
                              "Drucken", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cFree = {
                              "Freigeben", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};
							  static ColButton *cButtons [] = {&cWork, &cShow, &cDel, &cPrint, &cFree, NULL}; 



        static ITEM iLstChoise ("", (char *) &LstChoise,  "", 0);

        static ITEM iWork      ("", (char *) &cWork,      "", 0);
        static ITEM iShow      ("", (char *) &cShow,      "", 0);
        static ITEM iDel       ("", (char *) &cDel,       "", 0);
        static ITEM iPrint     ("", (char *) &cPrint,     "", 0);
        static ITEM iFree      ("", (char *) &cFree,      "", 0);

        static ITEM iWorkB      ("", (char *) "Bearbeiten",  "", 0);
        static ITEM iShowB      ("", (char *) "Anzeigen",    "", 0);
        static ITEM iDelB       ("", (char *) "L�schen",     "", 0);
        static ITEM iPrintB     ("", (char *) "Drucken",     "", 0);
        static ITEM iFreeB      ("", (char *) "Freigeben",   "", 0);

        static ITEM vOK     ("",  "    OK     ",  "", 0);
        static ITEM vCancel ("",  " Abbrechen ", "", 0);

        static field _fLstChoise[] = {
          &iLstChoise, 36, 0,  0, 0, 0, "", COLBUTTON, 0, 0, 0};

        static form fLstChoise = {1, 0, 0, _fLstChoise, 
                                  0, 0, 0, 0, NULL};    


        static field _clist [] = {
&iWork,     34, 2, 2, 1, 0, "", COLBUTTON,            0, testwork, 
                                                           KEY12 , 
&iShow,     34, 2, 4, 1, 0, "", COLBUTTON,            0, testshow, 
                                                          KEY12,
&iDel,      34, 2, 6, 1, 0, "", COLBUTTON,            0, testdel, 
                                                           KEY12,
&iPrint,    34, 2, 8, 1, 0, "", COLBUTTON,            0, testprint, 
                                                           KEY12,
&iFree,     34, 2,10, 1, 0, "", COLBUTTON,            0, testfree, 
                                                           KEY12,
&vCancel,  12, 0, 10, 12, 0, "",  BUTTON,               0, testcancel, 
                                                           KEY5, 
};


        static field _clistB [] = {
&iWorkB,     30, 0, 2, 3, 0, "", BUTTON,            0, testwork, 
                                                           KEY12 , 
&iShowB,     30, 0, 3, 3, 0, "", BUTTON,            0, testshow, 
                                                          KEY12,
&iDelB,      30, 0, 4, 3, 0, "", BUTTON,            0, testdel, 
                                                           KEY12,
&iPrintB,    30, 0, 5, 3, 0, "", BUTTON,            0, testprint, 
                                                           KEY12,
&iFreeB,     30, 0, 6, 3, 0, "", BUTTON,            0, testfree, 
                                                           KEY12,
&vCancel,   12, 0,  8,12, 0, "",  BUTTON,               0, testcancel, 
                                                           KEY5, 
};


static form clist = {6, 0, 0, _clistB, 0, 0, 0, 0, NULL}; 


void StartMenu (void)
/**
Liste auswaehlen und Listengenerator starten
**/
{
     form *sform;
     int  sfield;


     if (NoMenue)
	 {
		      mench = 0;
              EingabeMdnFil ();
     } 			  
			  

     while (TRUE)
     {
        sform = current_form;
        sfield = currentfield;
        save_fkt (5);
        save_fkt (12);

        set_fkt (lstcancel, 5);
        set_fkt (lstok, 12);

        no_break_end ();
        SetButtonTab (TRUE);
//        DisablehWnd (hMainWindow);
        SetBorder (WS_POPUP | WS_DLGFRAME);

        SetAktivWindow (mamain1);
        chwnd = OpenWindowCh (12, 36, 10, 22, hMainInst);
		MoveZeWindow (hMainWindow, chwnd);
        DlgWindow = chwnd;

        display_form (chwnd, &fLstChoise, 0, 0);
        currentfield = 0;
        enter_form (chwnd, &clist, 0, 0);
		mench = currentfield;
	    SetAnsehen ();  //LAC-153
        CloseControls (&fLstChoise);
        CloseControls (&clist);
        SetAktivWindow (mamain1);
        DestroyWindow (chwnd);
        chwnd = NULL;
        SetButtonTab (FALSE);
        restore_fkt (5);
        restore_fkt (12);
        if (sform)
        {
                current_form = sform;
                currentfield = sfield;
                SetCurrentFocus (currentfield);
        }

        if (syskey == KEY5) return;
        if (syskey == KEYESC) return;

        switch (mench)
        {
              case 0 :
                     EingabeMdnFil ();
					 break;
              case 1 :
                     EingabeMdnFil ();
//				     StartKasse (hMainInst, "", SW_SHOWNORMAL);
					 break;
              case 2 :
                     EingabeMdnFil ();
					 break;
              case 3 :
              case 4:
                     BereichsAuswahl ();
        }
     }
}


void SetListBkColor (char *coltxt)
/**
Hintergrund f�r Liste setzen.
**/
{
	   int i;
       static char *BkColorsTxt[] = {"WHITECOL",
                                     "BLUECOL",
                                     "BLACKCOL",
                                     "GRAYCOL",
					      		     "LTGRAYCOL",
	   };

       clipped (coltxt);
       for (i = 0; Combo2[i]; i ++)
       {
             if (strcmp (BkColorsTxt[i],coltxt) == 0)
             {
                   aufpListe.SetColors (Colors[i], BkColors[i]);
				   combo2pos = i;
                   break;
             }
	   }
}

void SetSpaltenAbmelden (char *sptxt) //LAC-??
/**
Hintergrund f�r Liste setzen.
**/
{
	   int i;
       static char *SpaltenTxt[] = {"bestellvorschlag",
                                     "test2",
                                     "test3",
                                     "test4",
					      		     "test5",
	   };

       clipped (sptxt);
       for (i = 0; Combo2[i]; i ++)
       {
             if (strcmp (SpaltenTxt[i],sptxt) == 0)
             {
				   aufpListe.SpalteAbmelden (SpaltenTxt[i], 0);
                   break;
             }
	   }
}

int SetListLine (char *cfg_v)
{
       static char *lineflag [] = {"WT" ,
                                   "HT",
                                   "ST",
                                   "GT",
                                   "VW",
                                   "VH",
                                   "VS", 
                                   "VG",
                                   "HW",
                                   "HH",
                                   "HS", 
                                   "HG",
                                   "NO",
                                   NULL};
       int i;

       for (i = 0; lineflag[i] != NULL; i ++)
       {
           if (strcmp (lineflag[i], cfg_v) == 0) 
           {
               aufpListe.SetListLines (i);
			   combo1pos = i;
               return i;
           }
       }
       aufpListe.SetListLines (1);
       return 1;
}


void EnableAufArt (void)
{

       SetItemAttr (&aufform, "tele", REMOVED);
       SetItemAttr (&aufform, "auf_art", EDIT);
       SetItemAttr (&aufform, "auf_art_txt", READONLY);
       auf_art_active = TRUE;
}


void EnableLdVkChange ()
/**
Moeglichkeit zur Aenderung des VK's in Menue beaerbeiten einfuegen.
**/
{

       InsertMenueItem (bearbeiten, &changeldpr, 0);
       LdVkEnabled = TRUE;
}

void EnablePrVkChange ()
/**
Moeglichkeit zur Aenderung des VK's in Menue beaerbeiten einfuegen.
**/
{

       InsertMenueItem (bearbeiten, &changevkpr, 0);
       PrVkEnabled = TRUE;
}

int HextoInt (char *Hex)
{
       int Hi;
       int Low;

       Hi = Hex[0];
       if (Hi >= (int) 'A' &&
           Hi <= (int) 'F')
       {
           Hi -= ((int) 'A' - 0x0A);
       }
       else if (Hi >= (int) 'a' &&
                Hi <= (int) 'f')
       {
           Hi -= ((int) 'a' - 0x0A);
       }
       else if (Hi >= (int) '0' &&
                Hi <= (int) '9')
       {
           Hi -= 0x30;
       }
       else
       {
           Hi = 0;
       }

       Low = Hex[1];
       if (Low >= (int) 'A' &&
           Low <= (int) 'F')
       {
           Low -= ((int) 'A' - 0x0A);
       }
       else if (Low >= (int) 'a' &&
                Low <= (int) 'f')
       {
           Low -= ((int) 'a' - 0x0A);
       }
       else if (Low >= (int) '0' &&
                Low <= (int) '9')
       {
           Low -= 0x30;
       }
       else
       {
           Low = 0;
       }
       return ((Hi << 4) & 0xF0) |  Low;
}


void GetCfgValues (void)
/**
Werte aus 51100Lack.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [512];

	   if (cfgOK) return;

	   cfgOK = TRUE;
       if (ProgCfg.GetCfgValue ("hwgchart_fuellen", cfg_v) == TRUE)   //LAC-109
       {
                    hwgchart_fuellen = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("hwgchart_fuellen_bis", cfg_v) == TRUE)   //LAC-109
       {
                    hwgchart_fuellen_bis = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("anz_wo_letzteBestellung", cfg_v) == TRUE)   //LAC-132
       {
                    cfg_anz_wo_letzteBestellung = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("anz_wo_Bestellvorschlag", cfg_v) == TRUE)   //LAC-156
       {
                    cfg_anz_wo_Bestellvorschlag = atoi (cfg_v);
	   }

       if (ProgCfg.GetCfgValue ("FaxTrenner", cfg_v) == TRUE)
       {
                    if (strcmp (cfg_v, "NO") == 0)
                    {
                        FaxTrenner = 0;
                    }
                    else if (strcmp (cfg_v, "TAB") == 0)
                    {
                        FaxTrenner = TAB;
                    }
                    else if (memcmp (cfg_v, "0x", 2) == 0 ||
                             memcmp (cfg_v, "0X", 2) == 0)
                    {
                        FaxTrenner = HextoInt (&cfg_v[2]);
                    }
                    else
                    {
                        FaxTrenner = cfg_v[0];
                    }
	   }
       if (ProgCfg.GetCfgValue ("SpaltenWaehlen", cfg_v) == TRUE)   //LAC-??
       {
                    SpaltenWaehlen = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("NewStyle", cfg_v) == TRUE)
       {
                    NewStyle = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("Startsize", cfg_v) == TRUE)
       {
                    Startsize = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("colbutton", cfg_v) == TRUE)
       {
		            if (atoi (cfg_v))
					{
					       clist.mask = _clist;
					}
       }
       if (ProgCfg.GetCfgValue ("lad_vk_edit", cfg_v) == TRUE)
       {
                     if (atoi (cfg_v))
                     {
   		                  EnableLdVkChange ();
                     }
                         
       }
       if (ProgCfg.GetCfgValue ("pr_vk_edit", cfg_v) == TRUE)
       {
                     if (atoi (cfg_v))
                     {
   		                  EnablePrVkChange ();
                     }
                         
       }
       if (ProgCfg.GetCfgValue ("searchmodea_bas", cfg_v) == TRUE)
       {
		            SetBasSearchMode (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchfielda_bas", cfg_v) == TRUE)
       {
		            SetBasSearchField (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchmodekun", cfg_v) == TRUE)
       {
		            kun_class.SetSearchModeKun (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("searchfieldkun", cfg_v) == TRUE)
       {
		           kun_class.SetSearchFieldKun (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("wahlccmarkt", cfg_v) == TRUE)
       {
		            cfgccmarkt = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("tour_zwang", cfg_v) == TRUE)
       {
                     tour_zwang = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("lief_me_par", cfg_v) == TRUE)
       {
                     AufKompl.SetLiefMePar (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("split_ls", cfg_v) == TRUE)
       {
                     AufKompl.SetSplitLs (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("LieferdatToKommdat", cfg_v) == TRUE)
       {
                     AufKompl.SetLieferdatToKommdat (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("bsd_kz", cfg_v) == TRUE)
	   {
			         AufKompl.SetBsdKz (atoi (cfg_v));

	   }
       if (ProgCfg.GetCfgValue ("AngText", cfg_v) == TRUE)
	   {
			         AufKompl.AngText = atoi (cfg_v);

	   }
       if (ProgCfg.GetCfgValue ("tour_datum", cfg_v) == TRUE)
       {
                     tour_datum =  (atoi (cfg_v));
       }

       if (ProgCfg.GetCfgValue ("stat_defp_von", cfg_v) == TRUE)
       {
                     strcpy (stat_defp_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("stat_defp_bis", cfg_v) == TRUE)
       {
                     strcpy (stat_defp_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("stat_deff_von", cfg_v) == TRUE)
       {
                     strcpy (stat_deff_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("stat_deff_bis", cfg_v) == TRUE)
       {
                     strcpy (stat_deff_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("abt_def_von", cfg_v) == TRUE)
       {
                     strcpy (abt_def_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("abt_def_bis", cfg_v) == TRUE)
       {
                     strcpy (abt_def_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("AbtRemoved", cfg_v) == TRUE)
       {
                     AbtRemoved = atol (cfg_v);
                     AufKompl.SetAbt (AbtRemoved);      
       }

       if (ProgCfg.GetCfgValue ("LuKunRemoved", cfg_v) == TRUE)
       {
                     LuKunRemoved = atol (cfg_v);
                     AufKompl.SetLuKun (LuKunRemoved);      
       }

       if (ProgCfg.GetCfgValue ("DrkKunRemoved", cfg_v) == TRUE)
       {
                     DrkKunRemoved = atol (cfg_v);
                     AufKompl.SetDrkKun (DrkKunRemoved);      
       }

       if (ProgCfg.GetCfgValue ("TxtByDefault", cfg_v) == TRUE)
       {
                     AufKompl.TxtByDefault = atol (cfg_v);
	   }

       if (ProgCfg.GetCfgValue ("tou_def_zwang", cfg_v) == TRUE)
       {
                     tou_def_zwang = min (max (0, atoi (cfg_v)), 1);
       }

       if (ProgCfg.GetCfgValue ("FitParams", cfg_v) == TRUE)
       {
                     FitParams = atol (cfg_v);
       }

       if (ProgCfg.GetCfgValue ("kun_sperr", cfg_v) == TRUE)
       {
                     kun_sperr = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("vertr_kun", cfg_v) == TRUE)
       {
                     vertr_kun = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("tou_wo_tou", cfg_v) == TRUE)
       {
                     tou_wo_tou = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("use_kopf_txt", cfg_v) == TRUE)
       {
                     use_kopf_txt = min (max (0, atoi (cfg_v)), 1);
       }

	   if (cfgccmarkt) tour_zwang = 0;
	   if (IsC_C ()) tour_zwang = 0;

       if (ProgCfg.GetGroupDefault ("lager", cfg_v) == TRUE)
       {
                     akt_lager = atol (cfg_v);
       }
       if (ProgCfg.GetGroupDefault ("tou_def_von", cfg_v) == TRUE)
       {
                     strcpy (tou_def_von, cfg_v);
       }
       if (ProgCfg.GetGroupDefault ("tou_def_bis", cfg_v) == TRUE)
       {
                     strcpy (tou_def_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("tou_def_von", cfg_v) == TRUE)
       {
                     strcpy (tou_def_von, cfg_v);
       }

       if (ProgCfg.GetCfgValue ("drk_ls_sperr", cfg_v) == TRUE)
       {
                     AufKompl.SetDrkLsSperr (atoi(cfg_v));
       }

       if (ProgCfg.GetCfgValue ("datum_plus", cfg_v) == TRUE)
       {
                     datum_plus = atol (cfg_v);
       }

       if (ProgCfg.GetCfgValue ("wo_tag_plus", cfg_v) == TRUE)
       {
                     QTClass.SetWotagplus (atol (cfg_v));
       }

       if (ProgCfg.GetCfgValue ("satour", cfg_v) == TRUE)
       {
                     QTClass.SetSatour (atol (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("sotour", cfg_v) == TRUE)
       {
                     QTClass.SetSotour (atol (cfg_v));
       }
/* tou_def_von und tou_def_bis aus 51100Lack.cfg hat Vorrang vor fitgroup.def   */

       if (ProgCfg.GetCfgValue ("tou_def_von", cfg_v) == TRUE)
       {
                     strcpy (tou_def_von, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("tou_def_bis", cfg_v) == TRUE)
       {
                     strcpy (tou_def_bis, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("WithAufb", cfg_v) == TRUE)
       {
                     AufKompl.WithAufb = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("komplett_default", cfg_v) == TRUE)
       {
		             AufKompl.SetKompDefault (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("format", cfg_v) == TRUE)
       {
		             AufKompl.SetDrFormat (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("SAFCOLOR", cfg_v) == TRUE)
       {
		             aufpListe.GetListColor (&MessCol, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("SABCOLOR", cfg_v) == TRUE)
       {
		             aufpListe.GetListColor (&MessBkCol, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("with_log", cfg_v) == TRUE)
       {
		             AufKompl.SetLog (atoi (cfg_v));
       }
       if (ProgCfg.GetCfgValue ("komplettmode", cfg_v) == TRUE)
       {
                     KomplettMode = min (max (0, atoi (cfg_v)), 2);
       }
       if (ProgCfg.GetCfgValue ("autodatum", cfg_v) == TRUE)
       {
                     autosysdat = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("autokunfil", cfg_v) == TRUE)
       {
                     autokunfil = min (max (0, atoi (cfg_v)), 1);
       }
       if (ProgCfg.GetCfgValue ("listedirect", cfg_v) == TRUE)
       {
                     listedirect = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("telelist", cfg_v) == TRUE)
       {
                     telelist = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ldatdiffminus", cfg_v) == TRUE)
       {
                     ldatdiffminus = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ldatdiffplus", cfg_v) == TRUE)
       {
                     ldatdiffplus = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ldatdiff", cfg_v) == TRUE)
       {
                     ldatdiffplus = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("lsdirect", cfg_v) == TRUE)
       {
                     lsdirect = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ListBkColor", cfg_v) == TRUE)
       {
// erst mal immer auf WHITECOL lassen, sonst siehts bescheiden aus !!! 		             SetListBkColor (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("ListLine", cfg_v) == TRUE)
       {
		             SetListLine (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("auf_art", cfg_v) == TRUE)
       {
		   if (atoi (cfg_v))
		   {
		             EnableAufArt ();
		   }
	   }
       if (ProgCfg.GetCfgValue ("akt_preis", cfg_v) == TRUE)
       {
                     akt_preis = atol (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("auftou", cfg_v) == TRUE)
       {
                     auftou = atol (cfg_v);
       }

	   //  LAC-184 erst mal nicht mehr auswerden,  bleibt generell auf 1! 
       if (ProgCfg.GetCfgValue ("TransactMax", cfg_v) == TRUE)
       {
//                    TransactMax = atoi (cfg_v);
					if (atoi (cfg_v) == -1)  TransactMax = 0;
       }
       if (ProgCfg.GetCfgValue ("TransactMessage", cfg_v) == TRUE)
       {
                    TransactMessage = atoi (cfg_v);
       }


       if (ProgCfg.GetCfgValue ("TestDivAdr", cfg_v) == TRUE)
       {
                     TestDivAdr = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ClipDial", cfg_v) == TRUE)
       {
                      ClipDial = new char [strlen (cfg_v) + 1];
                      strcpy (ClipDial, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DdeDial", cfg_v) == TRUE)
       {
                      DdeDialProg = new char [strlen (cfg_v) + 1];
                      strcpy (DdeDialProg, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DdeDialActive", cfg_v) == TRUE)
       {
                      DdeDialActive = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DdeDialService", cfg_v) == TRUE)
       {
                      DdeDialService = new char [strlen (cfg_v) + 1];
                      strcpy (DdeDialService, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DdeDialTopic", cfg_v) == TRUE)
       {
                      DdeDialTopic = new char [strlen (cfg_v) + 1];
                      strcpy (DdeDialTopic, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DdeDialItem", cfg_v) == TRUE)
       {
                      DdeDialItem = new char [strlen (cfg_v) + 1];
                      strcpy (DdeDialItem, cfg_v);
       }
       if (ProgCfg.GetCfgValue ("KunLiefKz", cfg_v) == TRUE)
       {
                      KunLiefKz = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("SearchKunDirect", cfg_v) == TRUE)
       {
                      SearchKunDirect = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("Color3D", cfg_v) == TRUE)
       {
                      Color3D = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("UpdatePersNam", cfg_v) == TRUE)
       {
                      UpdatePersNam = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("VerifyKun", cfg_v) == TRUE)
       {
                      VerifyKun = atoi (cfg_v);
       }
      if (ProgCfg.GetCfgValue ("Wochentag", cfg_v) == TRUE)
       {
                      Wochentag = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("EnableKun", cfg_v) == TRUE)
       {
                     EnableKunDirect = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("FixDat", cfg_v) == TRUE)
       {
                     FixDat = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("EnterKomm", cfg_v) == TRUE)
       {
                     EnterKomm = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("RunLs", cfg_v) == TRUE)
       {
                     AufKompl.RunLs = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("SaveKomplettDefault", cfg_v) == TRUE)
       {
                     AufKompl.SaveDefault = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("TeleTour", cfg_v) == TRUE)
       {
                     TeleTour = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("TestOfPo", cfg_v) == TRUE)
       {
                     TestOfPo = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("ChoicePrinter", cfg_v) == TRUE)
       {
		             AufKompl.ChoicePrinter = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("DisplayFreeText2", cfg_v) == TRUE)
       {
		             DisplayFreeText2 = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("x_Ampel", cfg_v) == TRUE)
       {
		             x_Ampel = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("y_Ampel", cfg_v) == TRUE)
       {
		             y_Ampel = atoi (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("PictureLieferzeit", cfg_v) == TRUE)
       {
		   if (strcmp (cfg_v, "hh:mm") == 0)
		   {
			   SetItemPicture (&aufform, "lieferzeit", "hh:mm");
		   }
		   else
		   {

			   SetItemPicture (&aufform, "lieferzeit", "");
		   }
       }
       if (ProgCfg.GetCfgValue ("StndRdOptimize", cfg_v) == TRUE)
	   {
		   StndRdOptimize = (CClientOrder::STD_RDOPTIMIZE) atoi (cfg_v);
	   }
	   aufpListe.SetLager (akt_lager);
       AufKompl.SetProgCfg (&ProgCfg);
       ProgCfg.CloseCfg ();
}

void MoveMamain (void)
/**
Koordinaten in $BWSETC lesen.
**/
{
        char *etc;
        char buffer [256];
        FILE *fp;
        int anz;
		static BOOL scrfOK = FALSE;
		RECT rect;
	    int xfull, yfull;



	    if (MoveMain == FALSE) return;
	    if (Startsize > 0) return;
        xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        yfull = GetSystemMetrics (SM_CYFULLSCREEN);        
	    if (xfull < 900) return;

        etc = getenv ("BWSETC");
        if (etc == NULL)
        {
             etc = "\\user\\fit\\etc";
        }
        sprintf (buffer, "%s\\fit.rct", etc);

        fp = fopen (buffer, "r");
        if (fp == NULL) return;

        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        if (fgets (buffer, 255, fp) == 0) 
        {
                 fclose (fp);          
                 return;
        }
        fclose (fp);

        anz = wsplit (buffer, " ");
        if (anz < 4) 
        {
            return;
        }

        rect.left   = atoi (wort[0]);
        rect.top    = atoi (wort[1]);
        rect.right  = atoi (wort[2]);
        rect.bottom = atoi (wort[3]);
		rect.left ++; 
		rect.top ++; 
		rect.right  = rect.right  - rect.left - 2;
		rect.bottom = rect.bottom - rect.top - 2;
        MoveWindow (hMainWindow, rect.left, rect.top, rect.right, rect.bottom, TRUE);
        return;
}

void SetcButtons (COLORREF Col, COLORREF BkCol)
/**
Farbe der Button setzen.
**/
{
	    int i;

		for (i = 0; cButtons[i]; i ++)
		{
                 cButtons[i]->Color   = Col;
				 cButtons[i]->BkColor = BkCol;
		}
}

void SaveRect (void)
/**
Aktuelle Windowgroesse sichern.
**/
{
	    char *etc;
		char rectname [512];
		RECT rect;
		FILE *fp;


		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		sprintf (rectname, "%s\\%s.rct", etc,Programm);
		GetWindowRect (hMainWindow, &rect);

		fp = fopen (rectname, "w");
		fprintf (fp, "left    %d\n", rect.left);
		fprintf (fp, "top     %d\n", rect.top);
		fprintf (fp, "right   %d\n", rect.right - rect.left);
		fprintf (fp, "bottom  %d\n", rect.bottom - rect.top);
		fclose (fp);

}


void MoveRect (void)
/**
Fenster auf gesicherte Daten anpassen.
**/
{
	    char *etc;
		char buffer [512];
		RECT rect;
		FILE *fp;
		int x, y, cx, cy;
		int anz;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		GetWindowRect (hMainWindow, &rect);
		x = rect.left;
		y = rect.top;
		cx = rect.right;
		cy = rect.bottom;
		sprintf (buffer, "%s\\%s.rct", etc,Programm);
		fp = fopen (buffer, "r");
		if (fp == NULL) return;
		while (fgets (buffer, 511, fp))
		{
			cr_weg (buffer);
			anz = wsplit (buffer, " ");
			if (anz < 2) continue;
			if (strcmp (wort[0], "left") == 0)
			{
				x = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "top") == 0)
			{
				y = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "right") == 0)
			{
				cx = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "bottom") == 0)
			{
				cy = atoi (wort[1]);
			}
		}
		fclose (fp);
		MoveWindow (hMainWindow, x, y, cx, cy, TRUE);
}



void TestLizenz ()
{
		if (FitParams == FALSE)
// M�glichkeit, die Lizenzabfrage abzuschalten.
		{
        switch (Fitl.TestLizenz ())
		{
		       case 0 :
				    break;
			   case 1:
				    print_mess (2, "Status-Datei nicht vorhanden oder ung�ltiges Format");
					ExitProcess (1);
			   case 2:
				    PrintLM = TRUE;
				    Fitl.LizenzMessage (hMainInst, hMainWindow);
					break;
			   case 3:
				    Fitl.DisableLizenz (hMainInst, hMainWindow);
					ExitProcess (1);
		}
		}

}



BOOL TestMenue (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == KEY5)
        {
            SendMessage (hMainWindow, WM_COMMAND, KEY5, 0l);
            return TRUE;
        }

        if (LOWORD (wParam) == IDM_MUSTER)
        {
            SetMusterAuftrag ();
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_TOURHINWEIS)
        {
            StartTourhinweis ();
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_STORNO)
        {
            SetStornoAuftrag ();
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_LS)
        {
            StartLsProc ();
            return TRUE;
        }
        if (LOWORD (wParam) == IDM_NACHDRUCK)
        {
            QueryNachDruck ();
            return TRUE;
        }

        return FALSE;
}


int    PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
       int anz; 
       int i;
       char argtab[20][80];
       char *varargs[20];
	   char *mv;
	   char *tr;
       char *cmdl;

	   CApplication::GetInstance ()->SetAppInstance (hInstance);
	   OleInitialize (0);
       anz = wsplit (lpszCmdLine, " ");
	   for (i = 0; i < anz; i ++)
	   {
		   if (memcmp (wort[i], "nomenu=", 7) == 0)
		   {
			   if (memcmp (&wort[i][7], "true", 4) == 0)
			   {
				   NoMenue = TRUE;
			   }

			   else if (memcmp (&wort[i][7], "false", 5) == 0)
			   {
				   NoMenue = FALSE;
			   }
		   }
		   else if (memcmp (wort[i], "mdn=", 4) == 0)
		   {
			   StartMdn = atoi (&wort[i][4]);
		   }
		   else if (memcmp (wort[i], "kun=", 4) == 0) //LAC-184
		   {
			   StartKun = atoi (&wort[i][4]);
		   }
		   else if (memcmp (wort[i], "pers=", 5) == 0) 
		   {
			   strncpy (StartPers ,&wort[i][5] , 12);
			   StartPers [12] = 0; 
		   }

	   }


       if (anz > 0 && strcmp (wort[0], "51200") == 0)
       {
           ProgCfg.SetProgName (wort[0]);
           aufpListe.SetCfgProgName (wort[0]);
           cmdl = lpszCmdLine + 6;
           while (*cmdl <= ' ')
           {
               if (*cmdl == 0) break;
               cmdl += 1;
           }

       }
       else
       {
           cmdl = lpszCmdLine;
       }


       MenAccelerators = LoadAccelerators (hInstance, "MENACCELERATORS");
	   GetForeground ();   
	   SetcButtons (BLACKCOL, LTGRAYCOL);

	   ColBorder = TRUE;
       tr = getenv_default ("COLBORDER");
	   if (tr)
	   {
			ColBorder = max(0, min (1, atoi (tr)));
	   }
	   ArrDown = LoadBitmap (hInstance, "ARRDOWN");
	   iarrdown.SetFeldPtr ((char *) &ArrDown);
       SelBmp = BMAP::LoadBitmap (hInstance, "SEL",   "MSK",  GetSysColor (COLOR_3DFACE));
       aufpListe.SelBmp = BMAP::LoadBitmap (hInstance, "SEL",   "MSK",  GetSysColor (COLOR_3DFACE)); //LAC-9
//       SelBmp = BMAP::LoadBitmap (hInstance, "SEL",   "MSK",  LTGRAYCOL);
       opendbase ("bws");
       menue_class.SetSysBen (cmdl);
//       menue_class.SetSysBenPers ();
       SetListLine ("HT");
	   GetCfgValues ();
	   if (hwgchart_fuellen == 0)   FreeAufk ();

	   if (TeleTour)
	   {
			KunTour = LoadLibrary ("KunTour.dll");
			if (KunTour == NULL)
			{
					disp_mess ("KunTour.dll kann nicht geladen werden", 2);
					TeleTour = FALSE;
			}
	   }

       if (KunLiefKz && DbClass.sqlcomm ("select * from kunlief") != 0)
       {
           KunLiefKz = FALSE;
       }
	   mv = getenv_default ("MOVEMAIN");
	   if (mv)
	   {
		   MoveMain = min (1, max (0, atoi (mv)));
	   }

       memcpy (&sys_par, &sys_par_null,  sizeof (struct SYS_PAR));
       strcpy (sys_par.sys_par_nam,"fen_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             fen_par = atoi (sys_par.sys_par_wrt);
             }
       }
       strcpy (sys_par.sys_par_nam,"lutz_mdn_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             lutz_mdn_par = atoi (sys_par.sys_par_wrt);
							 QClass.SetLutzMdnPar (lutz_mdn_par);
							 AufKompl.SetLutzMdnPar (lutz_mdn_par);
             }
       }
       strcpy (sys_par.sys_par_nam,"fil_lief_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             fil_lief_par = atoi (sys_par.sys_par_wrt);
             }
       }
	   if (fil_lief_par == FALSE && sys_ben.fil)
	   {
		     disp_mess ("Auftragserfassung f�r Filialen ist nicht erlaubt", 2);
			 CloseOfPo ();
		     closedbase ();
			 DestroyKommissionierer ();
			 DestroyStorno ();
			 ExitProcess (1);
	   }
       strcpy (sys_par.sys_par_nam,"klst_dr_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             AufKompl.SetKlstPar (atoi (sys_par.sys_par_wrt));
       }
       strcpy (sys_par.sys_par_nam,"tourlang");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             switch (atoi (sys_par.sys_par_wrt))
							 {
							        case 4 :
                                           tour_div = (long) 100;
										   break;
									case 5 :
                                           tour_div = (long) 1000;
										   break;
							 }
             }
       }
       strcpy (sys_par.sys_par_nam,"pos_txt_kz_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
		     int value =  atoi (sys_par.sys_par_wrt);
             if (value == 0)
			 {
				 AufKompl.TxtByDefault = TRUE;
			 }
			 else
			 {
				 AufKompl.TxtByDefault = FALSE;
			 }
       }
	   if (tou_wo_tou)
	   {
                strcpy (sys_par.sys_par_nam,"tou_par");
                if (sys_par_class.dbreadfirst () == 0)
				{
                         tou_par = atoi (sys_par.sys_par_wrt);
				}
				if (tou_par == 0)
				{
		                 SetItemAttr (&aufform, "tou", REMOVED);
		                 SetItemAttr (&aufform, "tou_bz", REMOVED);
						 aufform.mask[GetItemPos (&aufform, "tou") + 1].attribut = REMOVED;
						 tour_zwang = 0;
				}
				else
				{
                         strcpy (sys_par.sys_par_nam,"wo_tou_par");
                         if (sys_par_class.dbreadfirst () == 0)
						 {
                                    wo_tou_par = atoi (sys_par.sys_par_wrt);
						 }
						 if (wo_tou_par == 0) tour_div = (long) 1;
				}
       }
       strcpy (sys_par.sys_par_nam,"vertr_abr_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
                 vertr_abr_par = atoi (sys_par.sys_par_wrt);
       }
	   if (vertr_abr_par == FALSE)
	   {
		   SetItemAttr (&aufform, "vertr", REMOVED);
		   SetItemAttr (&aufform, "vertr_krz", REMOVED);
	   }
       strcpy (sys_par.sys_par_nam,"bg_cc_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             bg_cc_par = atoi (sys_par.sys_par_wrt);
             }
       }
       strcpy (sys_par.sys_par_nam,"zustell_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             zustell_par = atoi (sys_par.sys_par_wrt);
             }
       }
	   if (bg_cc_par && zustell_par)
	   {
		     EnableAufArt ();
	   }
	   if (IsC_C ())
	   {
		   SetItemAttr (&aufform, "tou", REMOVED);
		   SetItemAttr (&aufform, "tou_bz", REMOVED);
		   aufform.mask[GetItemPos (&aufform, "tou") + 1].attribut = REMOVED;
	   }

       anz = wsplit (lpszCmdLine, " ");
       for (i = 0; i < anz; i ++)
       {
            strcpy (argtab[i], wort[i]);
            varargs[i] = (char *) argtab[i];
       }
       argtst (&anz, varargs, tst_arg);

       SetStdProc (WndProc);
       InitFirstInstance (hInstance);
       InitNewInstance (hInstance, nCmdShow);
        

	   TestLizenz ();

       if (WithToolBar)
       {
	               hwndTB = MakeToolBarEx (hInstance, 
		                       hMainWindow,tbb, 116 ,
                               qInfo, qIdfrom,
		                       qhWndInfo, qhWndFrom);

                   hwndCombo1 = MakeToolBarCombo (hInstance,
                                      hMainWindow,
                                      hwndTB, 
                                      10, 
                                      11, 
                                      32);

                   FillCombo1 ();
                   ComboToolTip (hwndTB, hwndCombo1);
 
                   hwndCombo2 = MakeToolBarCombo (hInstance,
                                                  hMainWindow,
                                                  hwndTB, 
                                                  11, 
                                                  33, 
                                                  52);

                   FillCombo2 ();
                   ComboToolTip (hwndTB, hwndCombo2);


				   if (SpaltenWaehlen)
				   {
						hwndCombo3 = MakeToolBarCombo (hInstance,
                                                  hMainWindow,
                                                  hwndTB, 
                                                  12, 
                                                  70, 
                                                  91);

						FillCombo3 (0);
						ComboToolTip (hwndTB, hwndCombo3);

						hwndCombo4 = MakeToolBarCombo (hInstance,
                                                  hMainWindow,
                                                  hwndTB, 
                                                  13, 
                                                  92, 
                                                  113);

						FillCombo4 (0);
						ComboToolTip (hwndTB, hwndCombo4);
				   }


                   aufpListe.SethwndTB (hwndTB);
                   DisableTB ();
       }

       if (NewStyle)
       {
             AddFktButton (MusterButton);
             AddFktButton (LsButton);
             AddFktButton (NachDrButton);
             AddFktButton (StornoButton);
             AddFktButton (TourhinweisButton);
             ftasten = OpenFktEx (hInstance);
             ITEM::SetHelpName ("51400.cmd");
             CreateFktBitmap (hInstance, hMainWindow, hwndTB);
             SetFktMenue (TestMenue);
       }
       else
       {
             ftasten = OpenFktM (hInstance);
       }
       Mess.OpenMessage ();
       Createmamain1 ();
       MoveMamain ();
       if (WithMenue)
       {
	               hMenu = MakeMenue (menuetab);
	               SetMenu (hMainWindow, hMenu);
                   aufpListe.SetMenu (hMenu);
/*
				   if (!EnterKomm)
				   {
					   RemoveMenu (hMenu, IDM_KOMMIS, MF_BYCOMMAND);
				   }
				   else
				   {
				   }
*/
				   EnableMenuItem (hMenu, IDM_KOMMIS, MF_BYCOMMAND | MF_GRAYED);
				   EnableMenuItem (hMenu, IDM_TOURHINWEIS, MF_BYCOMMAND | MF_GRAYED);
			       TourhinweisButton->Enable (FALSE);


 
       }
	   if (Startsize == 1)
	   {
	               ShowWindow (hMainWindow, SW_SHOWMAXIMIZED);
	   }
	   else if (Startsize == 2)
	   {
	               MoveRect ();
	   }

	   if (StndRdOptimize == CClientOrder::No)
	   {
		   DeleteRefreshStnd ();
	   }

       SetEnvFont ();
	   GetStdFont (&BuFont);
       lstxtlist.SetListFont (&lFont);
       aufpListe.SetListFont (&lFont);

       aufpListe.Ampel.SetHInstance (hMainInst); 
       aufpListe.Ampel.SetHWnd (mamain1);
	   int pos = GetItemPos (&aufform, "tou_bz");
	   pos = -1;
       if (pos != -1)
	   {
			int tx = aufform.mask[pos].pos[1] + aufform.mask[pos].length;
			int ty = aufform.mask[pos].pos[0];
			aufpListe.Ampel.SetAbsPos (mamain1, tx, ty);
	   }
	   else
	   {
			int sx = GetSystemMetrics (SM_CXFULLSCREEN);
			if (sx > 1400 && x_Ampel <= 650)
			{
				x_Ampel = 850;
			}
			aufpListe.Ampel.SetP (x_Ampel, y_Ampel);
	   }
	   aufpListe.Ampel.SetHbrBackground (hbrBackground);
	   aufpListe.Ampel.SetOwnWindow ();
	   aufpListe.Ampel.SetPlay (TRUE);
	   aufpListe.Ampel.Load ();

       telefon1     = LoadIcon (hMainInst, "tele1");
       fax          = LoadIcon (hMainInst, "fax");



       btelefon       = LoadBitmap (hInstance, "btele");
       btelefoni      = LoadBitmap (hInstance, "btelei");
       CTelefon.bmp   = btelefoni;

       display_form (mamain1, &buform, 0, 0);
       MoveButtons ();
       CreateQuikInfos ();


       CheckMenuItem (hMenu, IDM_PAGEVIEW, MF_UNCHECKED); 
       ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
       ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);

//	    aufpListe.NoteIcon = LoadIcon (hInstance, "NOTESICON");
	    aufpListe.NoteIcon = LoadBitmap (hInstance, "NOTEICON");
        aufpListe.SethMainWindow (mamain1);
//       EingabeMdnFil ();
 
	   SetPartyActive ();
	   //LAC-109 A
	   if (hwgchart_fuellen > 0)
	   {
			aufpListe.hwgchart_fuellen (hwgchart_fuellen,hwgchart_fuellen_bis);
	       closedbase ();
		   return 0;
	   }
	   //LAC-109 E
       StartMenu ();

//       ProcessMessages ();
	   SaveSpalten ();
	   FreeAufk ();
	   SaveRect (); 
       TestActiveDial ();
       SetForeground ();
	   CleanTagAnr ();
	   CloseOfPo ();
       closedbase ();
       return 0;
}


void InitFirstInstance(HANDLE hInstance)
{
        
        WNDCLASS wc;
		HDC hdc;
		int col;

		col = 0;
		hdc = GetDC (NULL);
		col = GetDeviceCaps (hdc, BITSPIXEL);
		ReleaseDC (NULL, hdc);
        if (col < 16) ColBorder = FALSE;

		SetEnvFont ();

		strcpy (musterfont.FontName, FontName);
        musterfont.FontHeight = FontHeight;
        musterfont.FontWidth  = FontWidth;       
        musterfont.FontAttribute  = FontAttribute;       

		strcpy (ansehenfont.FontName, FontName);
        ansehenfont.FontHeight = FontHeight * 1.4;
        ansehenfont.FontWidth  = FontWidth * 1.4;       
        ansehenfont.FontAttribute  = FontAttribute;       

		strcpy (stornofont.FontName, FontName);
        stornofont.FontHeight = FontHeight * 1.3;     //LAC-210
        stornofont.FontWidth  = FontWidth * 1.3;       
        stornofont.FontAttribute  = FontAttribute;       

		SetSWEnvFont ();

		strcpy (lFont.FontName, FontNameSW);
        lFont.FontHeight = FontHeightSW;
        lFont.FontWidth  = FontWidthSW;       
        lFont.FontAttribute  = FontAttributeSW;       


        if (Color3D)
        {
             StdBackCol = GetSysColor (COLOR_3DFACE);
             SetColor3D (TRUE);
             CMuster.BkColor = StdBackCol;
             CStorno.BkColor = StdBackCol;
             CTourhinweis.BkColor = StdBackCol;
             CLs.BkColor = StdBackCol;
             CNachDruck.BkColor = StdBackCol;
  	         SetcButtons (BLACKCOL, StdBackCol);
        }

		if (hbrBackground == NULL)
		{
			hbrBackground = CreateSolidBrush (StdBackCol);
		}


        wc.style         =  CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "FITICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
//        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
//        wc.hbrBackground =  CreateSolidBrush (StdBackCol);
        wc.hbrBackground =  hbrBackground;
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "hStdWindow";

        RegisterClass(&wc);

        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "ListMain";
        RegisterClass(&wc);


        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hListWindow";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  StaticWProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "StaticWhite";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  StaticWProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.hbrBackground =  GetStockObject (GRAY_BRUSH);
        wc.lpszClassName =  "StaticGray";
        RegisterClass(&wc);

//LAC-9 A
        wc.lpfnWndProc   =  WndProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.hbrBackground =  hbrBackground;
        wc.lpszClassName =  "StaticHBR";
        RegisterClass(&wc);
//LAC-9 E


        wc.lpfnWndProc   =  WndProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.hbrBackground =  CreateSolidBrush (MessBkCol);
        wc.lpszClassName =  "StaticMess";
        RegisterClass(&wc);
        hMainInst = hInstance;
        return;
}


BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        HDC hdc;
        HFONT hFont, oldFont;
        char Caption [80];
        SIZE size;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (0, hdc);
        aufpListe.SetTextMetric (&tm);
		extern int session_id;  //FS-431

        if (InfoCaption[0])
        {
			strcpy (Caption,InfoCaption);
        }
        else
        {
			clipped(sys_ben.pers_nam);

//			sprintf (Caption,"%s  %s  <%ld>","Auftragsbearbeitung",Version2, session_id);  //FS-431 session_id
			sprintf (Caption,"<%s>   %s  %s  <%ld>",sys_ben.pers_nam,"Auftragsbearbeitung",Version2,session_id);  //FS-431 session_id

        }

        SetBorder (WS_THICKFRAME | WS_CAPTION | 
                   WS_SYSMENU | WS_MINIMIZEBOX |
                   WS_MAXIMIZEBOX);

        hMainWindow = OpenWindowChC (26, 80, 2, 0, hInstance, 
                      Caption);
        return 0; 
}


static int ProcessMessages(void)
{
        MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                  syskey = KEY5;
                                  PostQuitMessage (0);
                                  continue; ;
                              case VK_RETURN :
                                  syskey = KEYCR;
                                  EingabeMdnFil ();
                                  continue; ;
                       }
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
        }
  	    CloseOfPo ();
		closedbase ();
        return msg.wParam;
}

void InvalidateLines (void)
{
         RECT rect;
         static TEXTMETRIC tm;
         HDC hdc;

         return;

         hdc = GetDC (hMainWindow);
         GetTextMetrics (hdc, &tm);
         ReleaseDC (hMainWindow, hdc);
         GetClientRect (hMainWindow, &rect);

         rect.top = 2 * tm.tmHeight;
         rect.top = rect.top + rect.top / 3;
         rect.top -= 14;
         rect.bottom = rect.top + 10;
         InvalidateRect (hMainWindow, &rect, TRUE);
}


void PrintLines (HDC hdc)
/**
Linien am Bildschirm anzeigen.
**/
{
         static TEXTMETRIC tm;
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         static HPEN hPenB = NULL;
         RECT rect;
		 RECT wrect;
         int x, y;
         HFONT hFont, oldfont;

         stdfont ();
         hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
         oldfont = SelectObject (hdc,hFont);
         GetTextMetrics (hdc, &tm);
         DeleteObject (SelectObject (hdc, oldfont));
         y = 2 * tm.tmHeight;
         y = y + y / 3;
		 y -= 14;
         GetClientRect (mamain1, &rect);
         GetWindowRect (mamain1, &wrect);
         x = rect.right - 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
                   hPenB = CreatePen (PS_SOLID, 0, BLACKCOL);
         }

/* Linie oben                 */

         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

/* Linie mitte                */
         
		 y += 5 * tm.tmHeight;
		 y += tm.tmHeight / 2;
         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);
		 y -= tm.tmHeight / 2;


/* Linie unten                */
         
		 y += 6 * tm.tmHeight;
		 y += 1 * tm.tmHeight; //LAC-9 Liste muss etwas weiter unten beginnen, um Platz f�r die Reiter zu schaffen
		 y += 1 * tm.tmHeight; //LAC-87 Liste muss noch etwas weiter unten beginnen, um Platz f�r die 2-zeilige Reiter zu schaffen
         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

		 y += (1 + wrect.top);
		 aufpListe.SetLiney (y);
         aufpListe.MoveMamain1 ();
}

void DisplayLines ()
{
         HDC hdc;

         hdc = GetDC (mamain1);
         PrintLines (hdc);
         ReleaseDC (mamain1, hdc);
}

void ChoiseCombo1 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo1,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo1[i]; i ++)
          {
              if (strcmp (Combo1[i],text) == 0)
              {
                   aufpListe.SetListLines (i);
                   break;
              }
          }
}

void ChoiseCombo2 (void)
/**
Aktuell gewaehlten eintrag in Combobox 2 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo2,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo2[i]; i ++)
          {
              if (strcmp (Combo2[i],text) == 0)
              {
                   aufpListe.SetColors (Colors[i], BkColors[i]);
                   break;
              }
          }
}


void ChoiseCombo3 (void) //LAC-??
/**
Aktuell gewaehlten eintrag in Combobox 3 ermitteln.
**/
{
          int i, dReiter;
          char text [80];


		  dReiter = 0;
		  if (ReiterAktAuf) dReiter = 1;
		  if (ReiterBestellvorschlag) dReiter = 2;
		  if (ReiterletzteBestellung) dReiter = 3;
		  if (ReiterKritischeMhd) dReiter = 5; //LAC-84
		  if (Reiterkommldat) dReiter = 6; //LAC-116
		  if (ReiterHWG1) dReiter = 4;
		  if (ReiterHWG2) dReiter = 4;
		  if (ReiterHWG3) dReiter = 4;
		  if (ReiterHWG4) dReiter = 4;
		  if (ReiterHWG5) dReiter = 4;
		  if (ReiterHWG6) dReiter = 4;
		  if (ReiterHWG7) dReiter = 4;
		  if (ReiterHWG8) dReiter = 4;
		  if (ReiterHWG9) dReiter = 4;
		  if (ReiterHWG10) dReiter = 4;
		  if (ReiterHWG11) dReiter = 4;
		  if (ReiterHWG12) dReiter = 4;
		  if (ReiterHWG13) dReiter = 4;
		  if (ReiterHWG14) dReiter = 4;
		  if (ReiterHWG15) dReiter = 4;
		  if (ReiterHWG16) dReiter = 4;
          GetWindowText (hwndCombo3,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo3[i]; i ++)
          {
              if (strcmp (clipped(Combo3[i]),text) == 0)
              {
                  // aufpListe.SetListLines (i);
				   aufpListe.SpalteHinzufuegen (text, dReiter); 
 				   if (i > 0) doreload ();
                   break;
              }
          }
		  combo3pos = 0;
      SendMessage (hwndCombo3, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo3 [combo3pos]);

}

void ChoiseCombo4 (void) //LAC-??
/**
Aktuell gewaehlten eintrag in Combobox 4 ermitteln.
**/
{
          int i, dReiter;
          char text [80];

		  dReiter = 0;
		  if (ReiterAktAuf) dReiter = 1;
		  if (ReiterBestellvorschlag) dReiter = 2;
		  if (ReiterletzteBestellung) dReiter = 3;
		  if (ReiterKritischeMhd) dReiter = 5; //LAC-84
		  if (Reiterkommldat) dReiter = 6; //LAC-116
		  if (ReiterHWG1) dReiter = 4;
		  if (ReiterHWG2) dReiter = 4;
		  if (ReiterHWG3) dReiter = 4;
		  if (ReiterHWG4) dReiter = 4;
		  if (ReiterHWG5) dReiter = 4;
		  if (ReiterHWG6) dReiter = 4;
		  if (ReiterHWG7) dReiter = 4;
		  if (ReiterHWG8) dReiter = 4;
		  if (ReiterHWG9) dReiter = 4;
		  if (ReiterHWG10) dReiter = 4;
		  if (ReiterHWG11) dReiter = 4;
		  if (ReiterHWG12) dReiter = 4;
		  if (ReiterHWG13) dReiter = 4;
		  if (ReiterHWG14) dReiter = 4;
		  if (ReiterHWG15) dReiter = 4;
		  if (ReiterHWG16) dReiter = 4;
          GetWindowText (hwndCombo4,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo4[i]; i ++)
          {
              if (strcmp (clipped(Combo4[i]),text) == 0)
              {
                   aufpListe.SpalteAbmelden (text, dReiter);
 				   if (i > 0) doreload ();
                   break;
              }
          }
	  combo4pos = 0;
      SendMessage (hwndCombo4, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo4 [combo4pos]);
}



void disp_forms (void)
{
      if (aufformk.mask[0].feldid) display_form (mamain1, &aufformk, 0, 0);
      if (aufform.mask[0].feldid)  display_form (mamain1, &aufform, 0, 0);
}



int FaxAuf (void)
{
	      int ret;
		  FORM *scurrent;
		  int   sfield;
          char buffer [256];


		  if (mench > 0) return 0; //LAC-150

		  scurrent = current_form;
		  sfield = currentfield;
	      NoClose = TRUE;
          if (aufpListe.GetPosanz () == 0)
		  {
					 disp_mess ("Es wurden keine Positionen erfasst", 2);
					 SetCurrentField (currentfield);
					 return (1);
		  }
 		  if (testkun0 () == FALSE)
		  {
			         NoClose = FALSE;
					 return 1;
		  }
	      if (TouZTest () == FALSE) return 0;

	      if (abfragejn (hMainWindow, "Auftrag faxen ?", "J") == FALSE)
		  {
			         NoClose = FALSE;
		             current_form = scurrent;
		             currentfield = sfield;
					 return 1;
		  }

          aufpListe.DestroyWindows ();
		  if (lutz_mdn_par && aufk.auf_stat < 3)
		  {
			  if (MultiMdnProd ())
			  {
				  FormToAuf (1);
			  }
			  else
			  {
				  FormToAuf (3);
			  }
		  }
		  else
		  {
              FormToAuf (3);
		  }
          ls_class.update_aufk (atoi (mdn), atoi (fil), aufk.auf);
          if (aufk.auf != auto_nr.nr_nr)
          {
					   if (lutz_mdn_par)
					   {
						   sprintf (mdn, "%hd", 0);
						   sprintf (fil, "%hd", 0);
					   }

// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "auf",  auto_nr.nr_nr);
          }


          clipped (_adr.fax);
          if (FaxTrenner != 0 && (wsplit (_adr.fax, "/")) > 1)
          {
              sprintf (buffer, "%s%c%s", wort[0], FaxTrenner, wort[1]);
              ToClipboard (buffer);
          }
          else
          {
              ToClipboard (_adr.fax);
          }
		  ret = AufKompl.FaxAuf (hMainWindow, aufk.mdn, aufk.fil, 
			                                      aufk.auf);
          DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
          DbClass.sqlin ((short *) &aufk.fil, 1, 0);
          DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
          DbClass.sqlcomm ("update aufk set auf_stat = 4 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
          aufk.auf_stat = 4;
          syskey = KEY12;
          break_enter ();
          NoClose = FALSE;
          current_form = scurrent;
          currentfield = sfield;
          return 0;
}

void TestActiveDial (void)
{ 
          DWORD ExitCode;

          if (DialPid != NULL)
          {
 		        GetExitCodeProcess (DialPid, &ExitCode);
		        if (ExitCode == STILL_ACTIVE)
                {
	                     TerminateProcess (DialPid, 0);
                }
          }
          DialPid = NULL;
}


int RunClipDial (char *tel)
{
          char buffer [256];

          if (FaxTrenner != 0 && (wsplit (tel, "/")) > 1)
          {
              sprintf (buffer, "%s%c%s", wort[0], FaxTrenner, wort[1]);
              ToClipboard (buffer);
          }
          else
          {
              ToClipboard (tel);
          }

          TestActiveDial ();

          DialPid = ProcExecPid (ClipDial, SW_SHOWNORMAL, -1, 0, -1, 0);
          if (DialPid == NULL)
          {
              print_mess (2, "%s konnte nicht gestartet werden", ClipDial);
          }
          return 0;
}

int RunDdeDial (char *tel)
{
          char buffer [256];

          if (FaxTrenner != 0 && (wsplit (tel, "/")) > 1)
          {
              sprintf (buffer, "%s%c%s", wort[0], FaxTrenner, wort[1]);
          }
          else
          {
              strcpy (buffer, tel);
          }

//          TestActiveDial ();

          if (DdeDialActive == FALSE)
          {
              DialPid = ProcExecPid (DdeDialProg, SW_SHOWMINNOACTIVE, -1, 0, -1, 0);
              if (DialPid == NULL)
              {
                  print_mess (2, "%s konnte nicht gestartet werden", DdeDialProg);
                  return 0;
              }
              Sleep (1000);
          }
          DdeDial *ddeDial = new DdeDial ();
          ddeDial->SetServer (DdeDialProg);
          if (DdeDialService != NULL)
          {
              ddeDial->SetService (DdeDialService);
          }
          if (DdeDialTopic != NULL)
          {
              ddeDial->SetTopic (DdeDialTopic);
          }
          if (DdeDialItem != NULL)
          {
              ddeDial->SetItem (DdeDialItem);
          }
          ddeDial->Call (buffer);
          delete ddeDial;
          return 0;
}

void SetMuster (BOOL b)
{
          if (b == FALSE)
          {
              aufpListe.SetMuster (FALSE);
              CheckMenuItem (hMenu, IDM_MUSTER, MF_UNCHECKED);
              CMuster.bmp = NULL;
              DisplayFktBitmap ();
              if (fil_lief_par == FALSE)
              {
                      CloseControls (&musterf);
              }
          }
          else
          {
              aufpListe.SetMuster (TRUE);
              CheckMenuItem (hMenu, IDM_MUSTER, MF_CHECKED);
              CMuster.bmp = SelBmp;
              DisplayFktBitmap ();
              if (fil_lief_par == FALSE)
              {
                      display_form (mamain1, &musterf, 0, 0);
              }
          }
}
void SetStorno (BOOL b) //LAC-220
{
          if (b == FALSE)
          {
              aufpListe.SetStorno (FALSE);
			  sprintf (ButtonStornoText, "         %s", "Storno");  
              CheckMenuItem (hMenu, IDM_STORNO, MF_UNCHECKED);
		      CStorno.Color = BLUECOL;
              CStorno.bmp = NULL;
              DisplayFktBitmap ();
              CloseControls (&stornof);
          }
          else
          {
			 char date[12];
			 char time [12];
			 sysdate (date);
			 systime (time);
			 time[5] = 0;
			 sprintf (StornoText, "%s", aufk.hinweis);  
			 sprintf (ButtonStornoText, "         %s", aufk.hinweis);  

		      CStorno.Color = REDCOL;
              aufpListe.SetStorno (TRUE);
              CheckMenuItem (hMenu, IDM_STORNO, MF_CHECKED);
              CStorno.bmp = SelBmp;
              DisplayFktBitmap ();
              display_form (mamain1, &stornof, 0, 0);
          }
}

void SetAnsehen (void)
{
          if (mench == 1)
          {
                display_form (mamain1, &ansehenf, 0, 0);
          }
          else
          {
                CloseControls (&ansehenf);
          }
}

void StartTourhinweis (void)
{
	char text [80];
	char *bws = getenv ("BWS");
    long tou = 0;

//java -jar TourHinweis.jar <Kundennummer> <Lieferdatum> <Tourennummer>     <Auftrag> <Mandant> //LAC-213     
     if (tour_div > 0) tou = atol (tour) / tour_div;
	 if (atoi(kuns) == 0) return;
	 if (tou == 0) { disp_mess ("Es ist keine Tour hinterlegt", 0); return; }

	 sprintf (text,"javaw -jar %s\\bin\\TourHinweis.jar %ld %s %ld %ld %ld", bws,atoi(kuns), ldat, tou, aufk.auf,aufk.mdn);

	if (getenv ("testmode")) 
	{
		if (atoi (getenv ("testmode")) == 1)  
		{
				disp_mess (text, 0);
		}
	}


	ProcExecPid (text, SW_SHOW, 0, 40, -1, 0);
	return ;
}

void SetMusterAuftrag (void)
{
          if (aufpListe.GetMuster ())
          {
              aufpListe.SetMuster (FALSE);
              CheckMenuItem (hMenu, IDM_MUSTER, MF_UNCHECKED);
              CMuster.bmp = NULL;
              DisplayFktBitmap ();
              if (fil_lief_par == FALSE)
              {
                      CloseControls (&musterf);
              }
          }
          else
          {
              aufpListe.SetMuster (TRUE);
              CheckMenuItem (hMenu, IDM_MUSTER, MF_CHECKED);
              CMuster.bmp = SelBmp;
              DisplayFktBitmap ();
              if (fil_lief_par == FALSE)
              {
                      display_form (mamain1, &musterf, 0, 0);
              }
          }
}
void SetStornoAuftrag (void)
{
          if (aufpListe.GetStorno ())
          {
				EnterStorno ();
             if (! abfragejn (hMainWindow, 
                           "Stornierung entfernen ?",
                            "N") == 0)
			 {
              if (aufk.auf_stat < 0) aufk.auf_stat *= -1;
			  sprintf (auf_status, "%hd",aufk.auf_stat);

				strcpy (htext, "");
				strcpy (aufk.hinweis, "");
              aufpListe.SetStorno (FALSE);
		      CStorno.Color = BLUECOL;
			  sprintf (ButtonStornoText, "         %s", "Storno");  
              CheckMenuItem (hMenu, IDM_STORNO, MF_UNCHECKED);
              CStorno.bmp = NULL;
              DisplayFktBitmap ();
				display_form (mamain1, &stornof, 0, 0);
				display_form (mamain1, &aufform, 0, 0);
              CloseControls (&stornof);
				beginwork () ;
	    //        ls_class.update_aufk (atoi (mdn), atoi (fil), aufk.auf);
				aufk.mdn = atoi (mdn);
				aufk.fil = atoi (fil);
				DbClass.sqlin ((short *)  &aufk.auf_stat, 1, 0);
				DbClass.sqlin ((char *)  &aufk.hinweis, 0, sizeof (aufk.hinweis));
				DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
				DbClass.sqlin ((short *) &aufk.fil, 1, 0);
				DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
				DbClass.sqlcomm  ("update aufk set auf_stat = ?, hinweis = ? "
			                        "where mdn = ? "
									"and fil   = ? "
									"and auf = ?");

				commitwork () ;
				beginwork () ;
			 }
          }
          else
          {
			 char date[12];
			 char time [12];
			 sysdate (date);
			 systime (time);
			 time[5] = 0;
             if (! abfragejn (hMainWindow, 
                           "Auftrag Stornieren ?",
                            "N") == 0)
			 {

				EnterStorno ();


                if (aufk.auf_stat > 0) aufk.auf_stat *= -1;
				  sprintf (auf_status, "%hd",aufk.auf_stat);

				sprintf (StornoText, "Storniert von %s am %s um %s", clipped(sys_ben.pers_nam),date,time); 
				sprintf (aufk.hinweis, "%s", StornoText);  
				sprintf (ButtonStornoText, "         %s", aufk.hinweis);  
				CStorno.Color = REDCOL;
				strcpy (htext, aufk.hinweis);
				aufpListe.SetStorno (TRUE);
				CheckMenuItem (hMenu, IDM_STORNO, MF_CHECKED);
				CStorno.bmp = SelBmp;
				DisplayFktBitmap ();
				display_form (mamain1, &stornof, 0, 0);
				display_form (mamain1, &aufform, 0, 0);
				beginwork () ;
	    //        ls_class.update_aufk (atoi (mdn), atoi (fil), aufk.auf);
				aufk.mdn = atoi (mdn);
				aufk.fil = atoi (fil);
				DbClass.sqlin ((short *)  &aufk.auf_stat, 1, 0);
				DbClass.sqlin ((char *)  &aufk.hinweis, 0, sizeof (aufk.hinweis));
				DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
				DbClass.sqlin ((short *) &aufk.fil, 1, 0);
				DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
				DbClass.sqlcomm  ("update aufk set auf_stat = ?, hinweis = ? "
			                        "where mdn = ? "
									"and fil   = ? "
									"and auf = ?");
				commitwork () ;
				beginwork () ;
			 }
          }
}


void SetKunLief (void)
{
/*
          int KunPos;

          if (KunLief)
          {
               CheckMenuItem (hMenu, IDM_KUNLIEF, MF_UNCHECKED);
                              KunLief = FALSE;
 	           KunPos = GetItemPos (&aufform, "kun_txt");
               CloseControl (&aufform, KunPos);
               CloseControl (&aufform, KunPos + 1);
               aufform.mask[KunPos].item     = &ikunstxt;
               aufform.mask[KunPos + 1].item = &ikuns;
          }
          else
          {
               CheckMenuItem (hMenu, IDM_KUNLIEF, MF_CHECKED);
                              KunLief = TRUE;
 	           KunPos = GetItemPos (&aufform, "kun_txt");
               CloseControl (&aufform, KunPos);
               CloseControl (&aufform, KunPos + 1);
               aufform.mask[KunPos].item     = &ikunlstxt;
               aufform.mask[KunPos + 1].item = &ikunliefs;
               display_form (mamain1, &aufform);
          }
          display_field (mamain1, &aufform.mask[KunPos], 0, 0);
          display_field (mamain1, &aufform.mask[KunPos + 1], 0, 0);
*/
}


void StartLsProc ()
{
	Text Command;

	EnableWindow (hMainWindow, FALSE);
	Command.Format ("53100 %s LS=%hd;%hd", sys_ben.pers_nam, atoi (mdn), atoi (fil));
	CProcess LsProc (Command);
	if (LsProc.Start () == NULL)
	{
		Command.Format ("rswrun 53100");
		LsProc.SetCommand (Command);
        LsProc.Start ();
	}
	LsProc.WaitForEnd ();
	EnableWindow (hMainWindow, TRUE);
	SetCurrentFocus (currentfield);
	Mess.Message ("");
}



int QueryNachDruck (void)
/**
Auswahl ueber Aauftr�ge.
**/
{
       int ret;

	   aufk.mdn = atoi (mdn);
	   aufk.fil = atoi (fil);
       DisablehWnd (hMainWindow);
       DisableListhWnd (mamain1);
       ret = QClass.queryaufnd (mamain1);
       set_fkt (dokey5, 5);
       SetAktivWindow (mamain1);
       if (ret == FALSE)
       {
           SetCurrentFocus (currentfield);
           return 0;
       }

       AufKompl.Print_Liste ();               
       SetCurrentFocus (currentfield);
	   aufk.auf = atoi (auftrag);
       return 0;
}


void CheckPartyService (BOOL b)
{
	if (b)
	{
		IsPartyService = TRUE;
        CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_CHECKED);
        EnableFullTax ();
		if (aufk.psteuer_kz == 1)
		{
			SetToStandardTax ();
		}
		else if (aufk.psteuer_kz == 2)
		{
			SetToFullTax ();
			EnableFullTax ();
		}
	}
	else
	{
		IsPartyService = FALSE;
        CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_UNCHECKED);
        EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
	}
}


void CheckRefreshStnd (BOOL b)
{
	if (b)
	{
        CheckMenuItem (hMenu, IDM_REFRESH_STND, MF_CHECKED);
	}
	else
	{
        CheckMenuItem (hMenu, IDM_REFRESH_STND, MF_UNCHECKED);
	}
}

void DeleteRefreshStnd ()
{
	 HMENU SubMenu = GetSubMenu (hMenu, 2);
	 DeleteMenu (SubMenu,  2, MF_BYPOSITION);
	 DeleteMenu (SubMenu,  IDM_REFRESH_STND, MF_BYCOMMAND);
}

void SetPartyActive ()
{
     memcpy (&sys_par, &sys_par_null, 
                   sizeof (struct SYS_PAR));
     strcpy (sys_par.sys_par_nam,"party_mdn");
     if (sys_par_class.dbreadfirst () == 0)
     {
         if (atoi (sys_par.sys_par_wrt) == 0) 
         {
			 CheckPartyService (FALSE);
			 HMENU SubMenu = GetSubMenu (hMenu, 1);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DrawMenuBar (hMainWindow);
        }
        else
		{
			PartyMdn = (short) atoi (sys_par.sys_par_wrt);
		}
     }
	 else
	 {
			 CheckPartyService (FALSE);
			 HMENU SubMenu = GetSubMenu (hMenu, 1);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DeleteMenu (SubMenu,    PARTY_POSITION, MF_BYPOSITION);
			 DrawMenuBar (hMainWindow);
	 }
}


void EnablePartyService (BOOL b)
{
	if (b)
	{
        EnableMenuItem (hMenu, IDM_PARTYSERVICE, MF_ENABLED);
        EnableFullTax ();
	}
	else
	{
		IsPartyService = FALSE;
        CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_UNCHECKED);
        EnableMenuItem (hMenu, IDM_PARTYSERVICE, MF_GRAYED);
        EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
	}
}

void EnableFullTax ()
{
	if (inDaten != 1)
	{
        EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
		return;
	}
    if (!IsPartyService) 
	{
        EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
		return;
	}

    if (aufk.psteuer_kz == 2 && aufpListe.ContainsService ())
	{
        EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
		return;
	}
    EnableMenuItem (hMenu, IDM_FULLTAX, MF_ENABLED);
}


void DisableFullTax ()
{
    EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
}


void SetLsToFullTax ()
{
	int ret = MessageBox (NULL, "Lieferschein auf vollen Steuersatz setzen ?", NULL,
				          MB_YESNO | MB_ICONQUESTION);
	if (ret == IDYES)
	{
        CheckMenuItem (hMenu, IDM_FULLTAX, MF_CHECKED);
		aufk.psteuer_kz = 2;
	}
}

void SetToFullTax ()
{
    CheckMenuItem (hMenu, IDM_FULLTAX, MF_CHECKED);
	aufk.psteuer_kz = 2;
}

void SetLsToStandardTax ()
{
    if (aufpListe.ContainsService ()) return;

	int ret = MessageBox (NULL, "Lieferschein auf Standardsteuersatz setzen ?", NULL,
				          MB_YESNO | MB_ICONQUESTION);
	if (ret == IDYES)
	{
        CheckMenuItem (hMenu, IDM_FULLTAX, MF_UNCHECKED);
		aufk.psteuer_kz = 1;
	}
}


void SetToStandardTax ()
{

    CheckMenuItem (hMenu, IDM_FULLTAX, MF_UNCHECKED);
	aufk.psteuer_kz = 1;
}


void SetLsTax ()
{
	if (aufk.psteuer_kz ==1)
	{
		SetLsToFullTax ();
	}
	else
	{
		SetLsToStandardTax ();
	}
}



LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
        char cfg_v [20];
//		char where [256];
		static form *SForm = NULL;
		static int SField = 0;


        switch(msg)
        {
              case WM_PAINT :
                      if (hWnd == mamain1)
                      {
//                              disp_forms ();
                              hdc = BeginPaint (hWnd, &aktpaint);
                              PrintLines (hdc);
							  if (aufpListe.Ampel.GetAWnd () == NULL)
							  {
								aufpListe.PrintAmpel (hdc);
							  }
                              EndPaint (hWnd, &aktpaint);
                      }
/*
					  else if (hWnd == aufpListe.Ampel.GetAWnd ())
					  {
							  aufpListe.PrintAmpel (hdc);
					  }
*/
                      else 
                      {
                              aufpListe.OnPaint (hWnd, msg, wParam, lParam);
                              lstxtlist.OnPaint (hWnd, msg, wParam, lParam);
                      }
                      break;
              case WM_MOVE :
                      if (hWnd == hMainWindow)
                      {
                              aufpListe.MoveMamain1 ();
                              lstxtlist.MoveMamain1 ();
                      }
                      else if (hWnd == mamain1)
                      {
                              aufpListe.MoveMamain1 ();
                              lstxtlist.MoveMamain1 ();
                      }
                      else
                      {
                              aufpListe.OnSize (hWnd, msg, wParam, lParam);
                              lstxtlist.OnSize (hWnd, msg, wParam, lParam);
                      }
                      break;
              case WM_SIZE :
                      if (hWnd == hMainWindow)
                      {
                              MoveFkt ();
                              Mess.MoveMess ();
                              MoveMamain1 ();  
							  InvalidateRect (mamain1, NULL, TRUE);
                              hdc = GetDC (hMainWindow);
                              ReleaseDC (hMainWindow, hdc);
                      }
                      else if (hWnd == mamain1)
                      {
						      InvalidateLines ();
                              aufpListe.MoveMamain1 ();
                              lstxtlist.MoveMamain1 ();
                      }
                      else
                      {
                              aufpListe.OnSize (hWnd, msg, wParam, lParam);
                              lstxtlist.OnSize (hWnd, msg, wParam, lParam);
                      }
                      break;

              case WM_SYSCOMMAND:
                      if (hWnd == aufpListe.GetMamain1 ()
                          && wParam == SC_MINIMIZE)
                      {
                                   aufpListe.SetMin ();

                      }
                      else if (hWnd == aufpListe.GetMamain1 ()
                               && wParam == SC_MAXIMIZE)
                      {
                                   aufpListe.SetMax ();
                                   aufpListe.MaximizeMamain1 ();
                                   return 0;

                      }
                      else if (hWnd == aufpListe.GetMamain1 ()
                               && wParam == SC_RESTORE)
                      {
                                   aufpListe.InitMax ();
                                   aufpListe.InitMin ();
                                   ShowWindow (aufpListe.GetMamain1 (), 
                                               SW_SHOWNORMAL);
                                   aufpListe.MoveMamain1 ();
                                   return 0;
                      }
                      else if (hWnd == aufpListe.GetMamain1 ()
                               && wParam == SC_CLOSE)
                      {
                                   if (aufpListe.IsListAktiv ())
                                   {
                                           syskey = KEY5;
                                           SendKey (VK_F5);
                                   }
                                   return 0;
                      }
                      else if (hWnd == hMainWindow && wParam == SC_CLOSE)
                      {
						           if (NoClose)
								   {
						                    disp_mess (
												"Abbruch durch Windowmanager ist nicht m�glich", 
												2);
								            return 0;
								   }
                                   if (aufpListe.TestStndActive ())
								   {
								            return 0;
								   }
                                   if (abfragejn (hMainWindow, 
                                                  "Bearbeitung abbrechen ?",
                                                  "N") == 0)
                                   {
                                           return 0;
                                   }
//LAC-188                                   rollbackwork ();
//LAC-188                                   beginwork ();

								 //LAC-188 A
				                if (abfragejn (mamain1, "Auftrag speichern ?", "J"))
								{
									return dokey12 ();
								}
								else
								{
									BOOL auf_vorh = ls_class.AufVorh (aufk.mdn,aufk.fil,aufk.auf);  //LAC-190
									ls_class.Rollback_Work (); //LAC-167
									if (auf_vorh == TRUE)
									{
										auf_vorh = ls_class.AufVorh (aufk.mdn,aufk.fil,aufk.auf);  //LAC-190
										if (auf_vorh == FALSE) //LAC-190
										{
											rollbackwork ();
										}
										else
										{
											commitwork ();
										}
									}
									else
									{
										commitwork (); //LAC-167
									}
									beginwork ();
   		   							aufpListe.KillProcessWgChart (); 
		  							aufpListe.KillProcessShowBm (); 
			  						aufpListe.KillProcessTourDat (); 
								}
								 //LAC-188 E

								   if (lutz_mdn_par)
								   {
									   sprintf (mdn, "%hd", 0);
									   sprintf (fil, "%hd", 0);
								   }
/*
                                   dsqlstatus = AutoClass.nveinid (atoi (mdn), atoi (fil),
                                                "auf",  auto_nr.nr_nr);
*/
// Verwaltung der Auftragsnummer mit Filiale 0

                       dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "auf",  auto_nr.nr_nr);
                                   commitwork ();
	                               FreeAufk ();
	                               SetForeground ();
								   CleanTagAnr ();
								   SaveRect (); 
                                   TestActiveDial ();
							       CloseOfPo ();
		                           closedbase ();
								   DestroyKommissionierer ();
								   DestroyStorno ();
                                   ExitProcess (0);
                                   break;
                                   
                      }
                      break;

              case WM_HSCROLL :
                       aufpListe.OnHScroll (hWnd, msg,wParam, lParam);
                       lstxtlist.OnHScroll (hWnd, msg,wParam, lParam);
                       break;
                      
              case WM_VSCROLL :
                       aufpListe.OnVScroll (hWnd, msg, wParam, lParam);
                       lstxtlist.OnVScroll (hWnd, msg, wParam, lParam);
                       break;

              case WM_NOTIFY :
                    {
                      LPNMHDR pnmh = (LPNMHDR) lParam;

                      if (pnmh->code == TTN_NEEDTEXT)
                      {
                           LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
                           if (QuickCpy (lpttt->szText, lpttt->hdr.idFrom)
                               == FALSE)
                           {
                                    QuickHwndCpy (lpttt);
                           }
                      }
                      break;
                    }
	
              case WM_CTLCOLOREDIT:
					if (BsdInfo.BsdInfo == (HWND) lParam)
					{
						HDC hdc = (HDC) wParam;
						COLORREF color = RGB (255, 255, 255);
						SetBkColor (hdc, color);
                        BsdInfo.InfoBrush = CreateSolidBrush (color);
 						return (long) BsdInfo.InfoBrush;
					}
					break;
              case WM_CTLCOLORSTATIC:
					if (BsdInfo.BsdInfo == (HWND) lParam)
					{
						HDC hdc = (HDC) wParam;
						COLORREF color = RGB (255, 255, 255);
						SetTextColor (hdc, RGB (0, 0, 120));
						SetBkColor (hdc, color);
                        BsdInfo.InfoBrush = CreateSolidBrush (color);
 						return (long) BsdInfo.InfoBrush;
					}
					break;
              case WM_DESTROY :
 				      OnDestroy (hWnd); 
                      if (hWnd == hMainWindow)
                      {
				             CloseOfPo ();
						     closedbase (); 
                             PostQuitMessage (0);
                             return 0;
                      }
                      break;
              case WM_KEYDOWN :
                      if (aufpListe.GetMamain1 ())
                      {
                               aufpListe.FunkKeys (wParam, lParam);
                      }
                      break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == IDM_EXIT)
                    {
					       if (NoClose)
						   {
						      disp_mess (
									"Abbruch durch Windowmanager ist nicht m�glich", 
									2);
							  return 0;
						   }
                           if (aufpListe.TestStndActive ())
						   {
								return 0;
						   }
                            if (abfragejn (hMainWindow, 
                                            "Bearbeitung abbrechen ?",
                                            "N") == 0)
                            {
                                  return 0;
                            }
//LAC-188                                   rollbackwork ();
//LAC-188                                   beginwork ();

								//LAC-188 A
				                if (abfragejn (mamain1, "Auftrag speichern ?", "J"))
								{
									return dokey12 ();
								}
								else
								{
									BOOL auf_vorh = ls_class.AufVorh (aufk.mdn,aufk.fil,aufk.auf);  //LAC-190
									ls_class.Rollback_Work (); //LAC-167
									if (auf_vorh == TRUE)
									{
										auf_vorh = ls_class.AufVorh (aufk.mdn,aufk.fil,aufk.auf);  //LAC-190
										if (auf_vorh == FALSE) //LAC-190
										{
											rollbackwork ();
										}
										else
										{
											commitwork ();
										}
									}
									else
									{
										commitwork (); //LAC-167
									}
                                    beginwork ();
   		   							aufpListe.KillProcessWgChart (); 
		  							aufpListe.KillProcessShowBm (); 
			  						aufpListe.KillProcessTourDat (); 
								}
								//LAC-188 E

							if (lutz_mdn_par)
							{
							      sprintf (mdn, "%hd", 0);
							      sprintf (fil, "%hd", 0);
                            }
                            dsqlstatus = AutoClass.nveinid (atoi (mdn), 0,
                                            "auf",  auto_nr.nr_nr);
                            commitwork ();
	                        FreeAufk ();
	                        SetForeground ();
							CleanTagAnr ();
	 			            SaveRect (); 
                            TestActiveDial ();
						    CloseOfPo ();
                     		closedbase ();
 						    DestroyKommissionierer ();
						   DestroyStorno ();
                            ExitProcess (0);
                            break;
                    }

                    if (LOWORD (wParam) == KEYESC)
                    {
                            syskey = KEYESC;
                            SendKey (VK_ESCAPE);
                            break;
                    }
                    if (LOWORD (wParam) == KEY2)
                    {
                            syskey = KEY2;
                            SendKey (VK_F2);
                            break;
                    }
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYDOWN)
                    {
                            syskey = KEYDOWN;
                            SendKey (VK_DOWN);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYUP)
                    {
                            syskey = KEYUP;
                            SendKey (VK_UP);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYLEFT)
                    {
                            syskey = KEYLEFT;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYRIGHT)
                    {
                            syskey = KEYRIGHT;
                            SendKey (VK_RIGHT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYSTAB)
                    {
                            syskey = KEYSTAB;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYTAB)
                    {
                            syskey = KEYTAB;
                            SendKey (VK_TAB);
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_VINFO)
                    {
						    InfoVersion (); 
                            break;
                    }
                   else if (LOWORD (wParam) == QUERYAUF)
				   {
					        QueryAuf ();
				   }
                   else if (LOWORD (wParam) == QUERYSHOP)
				   {
					        ListeShop (); //LAC-74
				   }
                   else if (LOWORD (wParam) == QUERYKUN)
				   {
					        if (atoi (kunfil))
							{
								ShowFil ();
							}
							else
							{
					            QueryKun ();
							}
				   }
                   else if (LOWORD (wParam) == QUERYTOU)
				   {
					        TouQuery ();
				   }
				   else if (LOWORD (wParam) == IDM_FRAME)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_FRAME,
								           MF_CHECKED); 
							ActiveMark = IDM_FRAME;
                            syskey = KEY3;
                            SendKey (VK_F3);
                   }
				   else if (LOWORD (wParam) == IDM_REVERSE)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_REVERSE,
								           MF_CHECKED); 
							ActiveMark = IDM_REVERSE;
                            syskey = KEY4;
                            SendKey (VK_F4);
                   }
				   else if (LOWORD (wParam) == IDM_NOMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_NOMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_NOMARK;
                            syskey = KEY6;
                            SendKey (VK_F6);
                   }
				   else if (LOWORD (wParam) == IDM_EDITMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_EDITMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_EDITMARK;
                            syskey = KEY7;
                            SendKey (VK_F7);
                   }
				   else if (LOWORD (wParam) == IDM_PAGEVIEW)
                   {
                            if (aufpListe.GetRecanz () < 1) break;
                            if (PageView)
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                                    ToolBar_SetState(hwndTB, IDM_PAGE, TBSTATE_ENABLED);
                                    PageView = 0;
                                    aufpListe.SwitchPage0 (aufpListe.GetAktRowS ());
                            }
                            else
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                                    ToolBar_SetState(hwndTB, IDM_LIST, TBSTATE_ENABLED);
                                    PageView = 1;
                                    aufpListe.SwitchPage0 (aufpListe.GetAktRow ());
                            }
                            SendMessage (aufpListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (aufpListe.Getmamain3 (), 0, TRUE);
                   }
 				   else if (LOWORD (wParam) == IDM_PAGE)
                   {
                            PageView = 1; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                            aufpListe.SwitchPage0 (aufpListe.GetAktRow ());
                            SendMessage (aufpListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (aufpListe.Getmamain3 (), 0, TRUE);
                   }
 				   else if (LOWORD (wParam) == IDM_STD)
                   {
                             aufpListe.StdAuftrag ();
                             aufpListe.SetListFocus ();
                   }
 				   else if (LOWORD (wParam) == IDM_LIST)
                   {
                            PageView = 0; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                            aufpListe.SwitchPage0 (aufpListe.GetAktRowS ());
                            SendMessage (aufpListe.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (aufpListe.Getmamain3 (), 0, TRUE);
                   }
			       else if (LOWORD (wParam) == IDM_WORK)
                   {
                             EingabeMdnFil ();
                   }
			       else if (LOWORD (wParam) == IDM_PRINT)
                   {
                   }

				   if ((HWND) lParam == BsdInfo.BsdCalculate)
				   {
							 aufpListe.BsdCalculate ();
				   }

                   if (HIWORD (wParam) == CBN_CLOSEUP)
                   {
                                if (lParam == (LPARAM) hwndCombo1)
                                {
                                   ChoiseCombo1 ();
                                }
                                if (lParam == (LPARAM) hwndCombo2)
                                {
                                   ChoiseCombo2 ();
                                }
                                if (lParam == (LPARAM) hwndCombo3)
                                {
                                   ChoiseCombo3 ();
                                }
                                if (lParam == (LPARAM) hwndCombo4)
                                {
                                   ChoiseCombo4 ();
                                }

                                if (IsDlgCombobox ((HWND) lParam))
                                {
                                      return 0;
                                }

                                if (aufpListe.Getmamain3 ())
                                {
//                                    SetFocus (aufpListe.Getmamain3 ());
                                      aufpListe.SetListFocus ();
                                }
                                else
                                {
                                    SetCurrentFocus (currentfield);
                                }
                    }
					else if (LOWORD (wParam) == IDM_FONT)
                    {
                                  aufpListe.ChoiseFont (&lFont);
                                  lstxtlist.SetListFont (&lFont);
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_FIXWG)
                    {
					    SaveRect_ext ();  //LAC-52 
						if (aufpListe.ZeigeWarenkorb  == TRUE)
						{
							aufpListe.KillProcessWgChart ();
							aufpListe.SetZeigeWarenkorb (0);
						}

						ZeigeWarenkorb ();
                                  return 0;
					}
					else if (LOWORD (wParam) == IDM_FIXPAL)
                    {
					    SaveRect_ext_paletten ();  
						if (aufpListe.ZeigePalette  == TRUE)
						{
							aufpListe.KillProcessTourDat ();
							aufpListe.SetZeigePalette (0);
						}

						ZeigeWarenkorb ();
                                  return 0;
					}
					else if (LOWORD (wParam) == IDM_FIXBMP)
                    {
					    SaveRect_bmp ();  //LAC-62
						if (aufpListe.ZeigeBmp  == TRUE)
						{
							aufpListe.KillProcessShowBm ();
							aufpListe.SetZeigeBmp (0);
						}

						ZeigeBmp ();
                                  return 0;
					}
					else if (LOWORD (wParam) == IDM_BASIS)
                    {
                                  aufpListe.ShowBasis ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_BASIS)
                    {
                                  aufpListe.ShowBasis ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_POSTEXT)
                    {
                                  aufpListe.Texte ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_ANZBEST)
                    {
                                  SetAnzBest ();
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_BESTELLVORSCHL)
					{
						aufpListe.GoToBestellvorschlag ();
					}
					else if (LOWORD (wParam) == IDM_IGNOREMINBEST)
					{
						aufpListe.IgnoreMinBest ();
					}
					else if (LOWORD (wParam) == IDM_LGRDEF)
					{
                                  if (ProgCfg.GetGroupDefault ("lager", cfg_v) == TRUE)
								  {
                                             akt_lager = atol (cfg_v);
											 aufpListe.SetLager (akt_lager);
								  }
					}
					else if (LOWORD (wParam) == IDM_LGR)
					{
						          akt_lager = LgrClass.DefaultLgr (hWnd, akt_lager); 
								  aufpListe.SetLager (akt_lager);
					}
					else if (LOWORD (wParam) == IDM_PARTYSERVICE)
					{
								  if (IsPartyService)
								  {
									  IsPartyService = FALSE;
							          CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_UNCHECKED);
							          EnableMenuItem (hMenu, IDM_FULLTAX, MF_GRAYED);
									  aufk.psteuer_kz = 0;
								  }
								  else
								  {
									  IsPartyService = TRUE;
							          CheckMenuItem (hMenu, IDM_PARTYSERVICE, MF_CHECKED);
							          EnableFullTax ();
									  if (aufpListe.ContainsService ())
									  {
										aufk.psteuer_kz = aufpListe.Voll;
									  }
									  else
									  {

										aufk.psteuer_kz = aufpListe.Standard;
									  }
								  }
								  	
					}
					else if (LOWORD (wParam) == IDM_REFRESH_STND)
					{
                                  
								  if (GetMenuState (hMenu, IDM_REFRESH_STND, MF_BYCOMMAND) == MF_CHECKED)
								  {
							          CheckMenuItem (hMenu, IDM_REFRESH_STND, MF_UNCHECKED);
	  								  aufpListe.RefreshStnd = FALSE;

								  }
								  else
								  {
							          CheckMenuItem (hMenu, IDM_REFRESH_STND, MF_CHECKED);
	  								  aufpListe.RefreshStnd = TRUE;
								  }
								  	
					}
					else if (LOWORD (wParam) == IDM_FULLTAX)
					{
								  SetLsTax ();
					}
					else if (LOWORD (wParam) == IDM_MUSTER)
					{
                                  SetMusterAuftrag ();
					}
					else if (LOWORD (wParam) == IDM_TOURHINWEIS)
					{
                                  StartTourhinweis ();
					}
					else if (LOWORD (wParam) == IDM_STORNO)
					{
                                  SetStornoAuftrag ();
					}
					else if (LOWORD (wParam) == IDM_LS)
					{
                                  StartLsProc ();
					}
					else if (LOWORD (wParam) == IDM_NACHDRUCK)
					{
                                  QueryNachDruck ();
					}
					else if (LOWORD (wParam) == IDM_KOMMIS)
					{
                                  EnterKommissionierer (FALSE);
 								  SetCurrentFocus (currentfield);
					}
					else if (LOWORD (wParam) == IDM_KUNLIEF)
					{
                                  SetKunLief (); 
					}
                    else if (LOWORD (wParam) == IDM_INFO)
                    {
                                  syskey = KEY4;
                                  SendKey (VK_F4);
                    }
                    else if (LOWORD (wParam) == IDM_TEXT)
                    {
                    }
                    else if (LOWORD (wParam) == IDM_ETI)
                    {
                    }
                    else if (LOWORD (wParam) == CallT1)
                    {
                            CallTele1 ();
                            break;
                    }
                    else if (LOWORD (wParam) == CallT2)
                    {
                            CallTele2 ();
                            break;
                    }
                    else if (LOWORD (wParam) == CallT3)
                    {
                            CallTele3 ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_LDPR)
                    {
					        aufpListe.EnterLdVk ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_VKPR)
                    {
					        aufpListe.EnterPrVk ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_SHOWBMP)
                    {
					        aufpListe.StarteProcessShowBm (1);
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_SHOWBMPP)
                    {
						    ZeigeBmp ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_KRITMHD)
                    {
						    ZeigeKritMhd ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_BESTAND)
                    {
						    ZeigeBestand ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_PALETTE)
                    {
						    ZeigePalette ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_WARENKORB)
                    {
						    ZeigeWarenkorb ();
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_PRICEFROMLIST)
                    {
					        aufpListe.PrVkFromList ();
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL StaticWProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void FreeAufk (void)
/**
Beim Verlassen des Programms wird versucht alle Saetze in aufk mit
delstatus -1 auf delstatus 0 zu setzen.
Bei Saetze, die von einem anderen Benutzer bearbeitet werden bleibt der 
delstatus auf -1;
**/
{
	    extern short sql_mode; 
		short sql_s;
		int cursor;
		int upd_cursor;

		sql_s = sql_mode;
		sql_mode = 1;

		DbClass.sqlout ((short *) &aufk.mdn, 1, 0);
		DbClass.sqlout ((short *) &aufk.fil, 1, 0);
		DbClass.sqlout ((long *)  &aufk.auf, 2, 0);
		cursor = DbClass.sqlcursor ("select mdn,fil,auf from aufk "
			                        "where delstatus = -1");
		if (cursor < 0) return;

		DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
		upd_cursor = DbClass.sqlcursor ("update aufk set delstatus = 0 "
			                        "where mdn = ? "
									"and fil   = ? "
									"and auf = ?");
		if (upd_cursor < 0) return;

		beginwork ();
        while (DbClass.sqlfetch (cursor) == 0)
		{
			DbClass.sqlexecute (upd_cursor);
		}
		commitwork ();
		DbClass.sqlclose (upd_cursor);
		DbClass.sqlclose (cursor);
		sql_mode = sql_s;
}
void FreeAufk (int dauf)
/**
Beim Verlassen des Programms wird versucht aufk mit
delstatus -1 auf delstatus 0 zu setzen.
**/
{
	    extern short sql_mode; 
		short sql_s;

		sql_s = sql_mode;
		sql_mode = 1;

		int upd_cursor;

		DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		DbClass.sqlin ((long *)  &dauf, 2, 0);
		upd_cursor = DbClass.sqlcursor ("update aufk set delstatus = 0 "
			                        "where mdn = ? "
									"and fil   = ? "
									"and auf = ?");

		DbClass.sqlexecute (upd_cursor);
		DbClass.sqlclose (upd_cursor);
		sql_mode = sql_s;
}

struct TL
{
	  short mdn;
	  short fil;
	  char kun [9];
	  char name [17];
	  char tele [21];
	  char uhrzeit [6];
	  char partner [37];
	  char status [2];
      char text [37];
      long adr;
};

static struct TL Teles, *Teletab;
static  CH *Choise1;
static  short wo_tag;
static	char datum [12];


int TeleOK (int pos)
/**
Kunde auf erledigt setzen.
**/
{
 	   char buffer [256];
	   long kun;

	   strcpy (Teletab[pos].status, "X");
	   memcpy (&Teles, &Teletab[pos], sizeof (TL));
 	   sprintf (buffer, " %-8s %-18s %-20s %-5s %s", Teles.kun, Teles.name, 
		                                             Teles.tele, Teles.uhrzeit,
													 Teles.partner);
	   Choise1->UpdateRecord (buffer, pos);
	   kun = atol (Teles.kun);
	   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
	   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
	   DbClass.sqlin ((long *)  &kun,      2, 0);
	   DbClass.sqlin ((short *) &wo_tag,   1, 0);
	   DbClass.sqlin ((char *)  Teles.uhrzeit, 0, 6);
	   DbClass.sqlcomm ("update kun_anr set tag = today "
		                "where mdn = ? "
						"and fil = ? "
						"and kun = ? "
						"and wo_tag = ? "
						"and uhrzeit = ?");
	   commitwork ();
	   beginwork ();
	   PostQuitMessage (0);
	   telekun = TRUE;
	   sprintf (kuns, "%ld", kun); 
	   return 0;
}

int TeleDial (int pos)
/**
Telefon-Nummer w�hlen.
**/
{
        char buffer [512]; 
        if (Choise1 == NULL)
        {
            return 0;
        }

        Choise1->GetText (buffer);
        int anz = wsplit (buffer, " ");
        if (anz < 3)
        {
  	        disp_mess ("Keine Telefonnummer gefunden", 2);
            return 0;
        }
        strcpy (buffer, wort[2]);
        if (ClipDial != NULL)
        {
            RunClipDial (buffer);         
        }
        else if (DdeDialProg != NULL)
        {
            RunDdeDial (buffer);
        }
		return 0;
}

int TeleInfo (int pos)
/**
Telefon-Nummer w�hlen.
**/
{
        char buffer [512];
        
        sprintf (buffer, "maske partner.scr");
        ProcExec (buffer, SW_SHOWNORMAL, -1, 0, -1, 0);
		return 0;
}

void TelefonList (void)
/**
Auswahl ueber Kunden, die an diesem Tag angerufen werden sollen.
**/
{
	  int cx, cy;
	  char buffer [256];
	  int cursor, cursor_kun, cursor_adr;
      int cursor_txt, cursor_adr2;
	  short teanz;
	  long adr2;
      long txt_nr;
      short zei;
	  int i;
	  int dsqlstatus;

	  sysdate (datum);
      wo_tag = get_wochentag (datum);

	  aufk.mdn = atoi (mdn);
	  aufk.fil = atoi (fil);

	  if (lutz_mdn_par == 0)
	  {
	           DbClass.sqlout ((short *) &teanz,    1, 0);
	           DbClass.sqlin ((short *)  &aufk.mdn, 1, 0);
	           DbClass.sqlin ((short *)  &aufk.fil, 1, 0);
	           DbClass.sqlin ((short *)  &wo_tag,   1, 0);
	           DbClass.sqlin ((char *)   sys_ben.pers_nam, 0, 9);
	           DbClass.sqlcomm ("select count (*) from kun_anr where "
		                          "mdn = ? "
								  "and fil = ? "
								  "and wo_tag = ? "
								  "and (pers_nam = ? or pers_nam = \" \" or pers_nam is null) "
								  "and tag <> today");
	  }
	  else
	  {
	           DbClass.sqlout ((short *) &teanz,    1, 0);
	           DbClass.sqlin ((short *)  &wo_tag,   1, 0);
	           DbClass.sqlin ((char *)   sys_ben.pers_nam, 0, 9);
	           DbClass.sqlcomm ("select count (*) from kun_anr where "
								  "wo_tag = ? "
								  "and (pers_nam = ? or pers_nam = \" \" or pers_nam is null) "
								  "and tag <> today");
	  }


	  if (teanz == 0)
	  {
		  disp_mess ("Keine Liste vorhanden",2);
		  return;
	  }

	  Teletab = new struct TL [teanz + 2];
	  if (Teletab == NULL)
	  {
		  disp_mess ("Fehler bei der Speicherzuornung", 2);
		  return;
	  }

	  if (lutz_mdn_par == 0)
	  {
	      DbClass.sqlout ((char *) Teles.kun,  0, 9); 
	      DbClass.sqlout ((char *) Teles.tele, 0, 21); 
	      DbClass.sqlout ((char *) Teles.uhrzeit, 0, 6); 
	      DbClass.sqlout ((char *) Teles.partner, 0, 37); 
	      DbClass.sqlout ((char *) &Teles.adr, 2, 0); 

	      DbClass.sqlin ((short *)  &aufk.mdn, 1, 0);
	      DbClass.sqlin ((short *)  &aufk.fil, 1, 0);
	      DbClass.sqlin ((short *)  &wo_tag,   1, 0);
	      DbClass.sqlin ((char *)   sys_ben.pers_nam, 0, 9);
	      cursor = DbClass.sqlcursor ("select kun, tel, uhrzeit, partner, adr "
		                          "from kun_anr "
								  "where mdn = ? "
								  "and fil = ? "
								  "and wo_tag = ? "
								  "and (pers_nam = ? or pers_nam = \" \" or pers_nam is null) "
								  "and kun_anr.tag <> today order by uhrzeit");

	  }
	  else
	  {
	      DbClass.sqlout ((short *)  &aufk.mdn, 1, 0);
	      DbClass.sqlout ((short *)  &aufk.fil, 1, 0);
	      DbClass.sqlout ((char *) Teles.kun,  0, 9); 
	      DbClass.sqlout ((char *) Teles.tele, 0, 21); 
	      DbClass.sqlout ((char *) Teles.uhrzeit, 0, 6); 
	      DbClass.sqlout ((char *) Teles.partner, 0, 37); 
	      DbClass.sqlout ((char *) &Teles.adr, 2, 0); 

	      DbClass.sqlin ((short *)  &wo_tag,   1, 0);
	      DbClass.sqlin ((char *)   sys_ben.pers_nam, 0, 9);
	      cursor = DbClass.sqlcursor ("select mdn, fil, kun, tel, uhrzeit, partner, adr "
		                          "from kun_anr "
								  "where wo_tag = ? "
								  "and (pers_nam = ? or pers_nam = \" \" or pers_nam is null) "
								  "and kun_anr.tag <> today");
	  }

	  DbClass.sqlin ((short *)  &aufk.mdn, 1, 0);
	  DbClass.sqlin ((short *)  &aufk.fil, 1, 0);
	  DbClass.sqlin ((char *)   Teles.kun, 0, 9);

	  DbClass.sqlout ((char *) Teles.name, 0, 17); 
	  DbClass.sqlout ((long *) &adr2, 2, 0); 

	  cursor_kun = DbClass.sqlcursor ("select kun_krz2, adr2 from kun "
		                              "where mdn = ? "
									  "and   fil = ? "
									  "and kun = ?");

	  DbClass.sqlin ((long *) &adr2, 2, 0);
	  DbClass.sqlout ((char *) Teles.partner, 0, 37);
	  cursor_adr = DbClass.sqlcursor ("select partner from adr where adr = ?");

	  DbClass.sqlin ((long *) &Teles.adr, 2, 0);
	  DbClass.sqlout ((long *) &txt_nr, 2, 0);
	  cursor_adr2 = DbClass.sqlcursor ("select txt_nr from adr where adr = ?");

	  DbClass.sqlin ((long *) &txt_nr, 2, 0);
      DbClass.sqlout ((char *) Teles.text, 0, 37);
      DbClass.sqlout ((short *) &zei, 1, 0);
      cursor_txt = prepare_sql ("select txt, zei from we_txt "
                                "where nr = ? "
                                "order by zei");

      Settchar ('\"');
	  cx = 80;
	  cy = 20;
	  Choise1 = new CH (cx, cy);
      Choise1->OpenWindow (hMainInst, mamain1);

//	  Choise1->VLines (" ", 0);
	  sprintf (buffer, " %9s %18s %20s %5s %20s %20s", "1", "1", "1", "1", "1", "1"); 
	  Choise1->VLines (buffer, 0);
	  EnableWindow (hMainWindow, FALSE);
	  sprintf (buffer, " %-8s %-18s %-20s %-5s %-20s %-20s", "K-Nr", "Name", "Telefon-Nr", 
		                                            "Zeit", "Partner", 
                                                    "Zust�ndigkeit");
	  Choise1->InsertCaption (buffer);

	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
	  {
		  Teles.mdn = aufk.mdn;
		  Teles.fil = aufk.fil;
          strcpy (Teles.name, " "); 
          strcpy (Teles.status, " "); 
		  DbClass.sqlopen (cursor_kun);
		  dsqlstatus = DbClass.sqlfetch (cursor_kun);

          clipped (Teles.partner);
          if (strcmp (Teles.partner, " ") <= 0)
          {
                  if (Teles.adr > 0)
                  {
                          adr2 = Teles.adr;
                  }
  		          DbClass.sqlopen (cursor_adr);
		          dsqlstatus = DbClass.sqlfetch (cursor_adr);
          }

          Teles.text[0] = 0;
          DbClass.sqlopen (cursor_adr2);
          dsqlstatus = DbClass.sqlfetch (cursor_adr2);
          if (txt_nr > 0)
          {
              DbClass.sqlopen (cursor_txt);
              DbClass.sqlfetch (cursor_txt);
          }

		  memcpy (&Teletab[i], &Teles, sizeof (TL));
  		  sprintf (buffer, " %-8s \"%-18s\" \"%-20s\" %-5s \"%-20s\" \"%-20s\"", Teles.kun, Teles.name, Teles.tele, 
			                                                Teles.uhrzeit,Teles.partner, Teles.text);
	      Choise1->InsertRecord (buffer);
		  i ++;
	  }

	  DbClass.sqlclose (cursor);
	  DbClass.sqlclose (cursor_kun);
	  DbClass.sqlclose (cursor_adr);
	  DbClass.sqlclose (cursor_adr2);
	  DbClass.sqlclose (cursor_txt);

	  Choise1->SetOkFunc (TeleOK);
	  Choise1->SetDialFunc (TeleDial);
	  Choise1->SetInfoFunc (TeleInfo);
	  Choise1->ProcessMessages ();
	  if (syskey != KEY5)
	  {
	            Choise1->GetText (buffer);
	  }
	  EnableWindow (hMainWindow, TRUE);
      Choise1->DestroyWindow ();
	  delete Choise1;
	  Choise1 = NULL;
	  delete Teletab;
	  currentfield = 0;
	  SetFocus (aufformk.mask[0].feldid);
	  if (telekun)
	  {
	   leseauf ();
	   PostMessage (mamain1, WM_KEYDOWN, VK_RETURN, 0l);
	  }
      return;
}

void RestartProcess (int kun)
{
	   char command [512]; 
	   if (TransactMessage)
	   {
	          disp_mess ("Die Anzahl der Transaktionen pro Process ist �berschritten.\n" 
		                 "Ein Restart des Programms wird durchgef�hrt", 2); 
	   }

//  sprintf (command, "%s nomenu=true mdn=%hd", GetCommandLine (), atoi (mdn));
//    sprintf (command, "%s nomenu=true mdn=%hd kun=%ld", GetCommandLine (), atoi (mdn), kun); //LAC-184  nicht GetCommandLine , command w�chst und w�chst nach jedem Restart
//    sprintf (command, "51100Lack %s nomenu=true mdn=%hd kun=%ld", clipped(sys_ben.pers_nam) , atoi (mdn), kun); //LAC-184
    sprintf (command, "51100Lack %s nomenu=true mdn=%hd kun=%ld pers=%s", clipped(sys_ben.pers_nam) , atoi (mdn), kun, clipped(sys_ben.pers_nam)); //LACK-184

//	disp_mess (command,0);
	   ProcExec (command, SW_SHOW, -1, 0, -1, 0);
	   Sleep (1000);
	   DestroyKommissionierer ();
	   ExitProcess (0);
}


class CPersKommis
{
	public :
		Text pers;
		Text name;
		Text taetigkeit;
		Text StrOut;
		CPersKommis ();
		CPersKommis (Text&, Text&, Text&);
		CPersKommis (LPSTR, LPSTR, LPSTR);
		~CPersKommis ();
		LPSTR ToString ();
};

CPersKommis::CPersKommis ()
{
	pers = "";
	name = "";
	taetigkeit = "";
}

CPersKommis::CPersKommis (Text& pers, Text& name, Text& taetigkeit)
{
	this->pers = pers;
	this->name = name;
	this->taetigkeit = taetigkeit;
}

CPersKommis::CPersKommis (LPSTR pers, LPSTR name, LPSTR taetigkeit)
{
	this->pers = pers;
	this->name = name;
	this->taetigkeit = taetigkeit;
}

CPersKommis::~CPersKommis ()
{
}

LPSTR CPersKommis::ToString ()
{
	pers.Trim ();
	name.Trim ();
	taetigkeit.Trim ();
	StrOut.Format ("%s; %s; %s", pers.GetBuffer (), name.GetBuffer (), taetigkeit.GetBuffer ());
	return StrOut.GetBuffer ();
}


static std::vector<CPersKommis *> KommTab;
static int PersCursor = -1;
LPSTR *ComboTab = NULL;

int GetKommPos (char *komm_name)
{
   Text pers = komm_name;
   pers.Trim ();
   int i = 0;
   for (std::vector<CPersKommis *>::iterator pk = KommTab.begin ();
	          pk != KommTab.end ();
	          pk ++, i ++)
			  {
					CPersKommis *PersKommis = *pk;
					PersKommis->pers.Trim ();
					if (PersKommis->pers == pers) 
					{
						return i;
					}
			  }
    return 0;
}
			  


void EnterKommissionierer (BOOL EnterAuto)
{
	   char pers[13];
	   char name [17];
	   char taet [25];

	   if (!EnterKomm && EnterAuto) return;
	   aufk.mdn = atoi (mdn);
	   aufk.fil = atoi (fil);
	   if (PersCursor == -1)
	   {
		   DbClass.sqlout ((char *) pers, 0, sizeof (pers));
		   DbClass.sqlout ((char *) name, 0, sizeof (name));
		   DbClass.sqlout ((char *) taet, 0, sizeof (taet));
		   DbClass.sqlin  ((short *) &aufk.mdn, 1, 0);
		   DbClass.sqlin  ((short *) &aufk.fil, 1, 0);
		   PersCursor = DbClass.sqlcursor ("select pers, adr.adr_krz, taet from pers, adr "
			                               "where adr.adr = pers.adr "
										   "and pers.mdn = ? "
										   "and pers.fil = ? "
										   "and pers.taet_kz = \"K\"");
	   }
	   if (KommTab.size () == 0)
	   {
			DbClass.sqlopen (PersCursor);
			while (DbClass.sqlfetch (PersCursor) == 0)
			{
				CPersKommis *PersKommis = new CPersKommis (pers, name, taet);
				KommTab.push_back (PersKommis);
			}
			ComboTab = new LPSTR [KommTab.size () + 1];

			int i = 0;
			for (std::vector<CPersKommis *>::iterator pk = KommTab.begin ();
			          pk != KommTab.end ();
 			          pk ++, i ++)
					  {
						  CPersKommis *PersKommis = *pk;
						  ComboTab[i] = PersKommis->ToString ();
					  }
					  ComboTab[i] = NULL;
	   }				  
													   
													  
	   EnterF Enter;
	   Enter.ComboList = TRUE;
       Enter.SetColor (GetSysColor (COLOR_3DFACE));

       char KommName [80];
	   strcpy (KommName, "");  

	   if (KommTab.size () > 0)
	   {
		   int idx = GetKommPos (aufk.komm_name);
		   strcpy (KommName, ComboTab[idx]);  
	   }
	   else
	   {
		   disp_mess ("Es sind keine Kommissionierer angelegt", 2);
		   return;
	   }


	   Enter.EnterText (mamain1, "Kommisionierer", KommName, 30, "", ComboTab);
	   if (syskey != KEY5)
	   {
		   Token t (KommName, ";");
		   strncpy (aufk.komm_name, t.GetToken (0), 12);
		   aufk.komm_name[12] = 0;
	   }
		   
}

void DestroyKommissionierer ()
{
	if (PersCursor != -1)
	{
		DbClass.sqlclose (PersCursor);
		PersCursor = -1;
	}

	for (std::vector<CPersKommis *>::iterator pk = KommTab.begin ();
			          pk != KommTab.end ();
 			          pk ++)
					  {
						  CPersKommis *PersKommis = *pk;
						  delete PersKommis;
					  }
	KommTab.clear ();
	if (ComboTab != NULL)
	{
		delete ComboTab;
		ComboTab = NULL;
	}

}

BOOL OfPoOver (short pmdn, long pkun)
{
	static short mdn;
	static long kun;

	static long anz;

	if (mench > 0 ) return FALSE ;//LAC-150
	if (!TestOfPo) return FALSE;

	if (of_pocursor == -1)
	{
		DbClass.sqlin ((short *) &mdn, 1, 0);
		DbClass.sqlin ((long *) &kun, 2, 0);
		DbClass.sqlout ((long *) &anz, 2, 0);
		of_pocursor = DbClass.sqlcursor ("select count (*) from of_po "
			                        "where mdn = ? "
									"and kun = ?");
	}

	mdn = pmdn;
	kun = pkun;

	anz = 0;
	DbClass.sqlopen (of_pocursor);
	DbClass.sqlfetch (of_pocursor);
	if (anz >= 3)
	{
		return TRUE;
	}
	return FALSE;
}

void CloseOfPo ()
{
	if (of_pocursor != -1)
	{
		DbClass.sqlclose (of_pocursor);
		of_pocursor = -1;
	}
	if (auf_cursor != -1) //LAC-184
	{
		DbClass.sqlclose (auf_cursor);
		auf_cursor = -1;
	}
}

int TestAufKun (long lieferdat) //LAC-21  Test , ob f�r dieses Datum schon ein Auftrag vorhanden ist
{
	int ret = 0 ; 
	int dauf = 0;
//LAC-184	static int auf_cursor = -1;
	
	if (auf_cursor == -1)  
	{
	   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
	   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
	   DbClass.sqlin ((long *) &aufk.auf, 2, 0);
	   DbClass.sqlin ((long *) &akt_kun, 2, 0);
	   DbClass.sqlin ((long *) &lieferdat, 2, 0);
	   DbClass.sqlout ((short *) &dauf, 2, 0);
       auf_cursor = DbClass.sqlcursor ("select aufk.auf from aufk,aufp where aufk.mdn = ? and aufk.fil = ? and aufk.auf <> ? and aufk.kun = ? "
								"and aufk.lieferdat = ? and aufk.auf_stat <= 2 and aufk.delstatus = 0 "
								"and aufk.mdn = aufp.mdn and aufk.fil = aufp.fil and aufk.auf = aufp.auf ");
	}

	DbClass.sqlopen (auf_cursor);
	ret = DbClass.sqlfetch (auf_cursor);
	if (ret != 0) dauf = 0;
	return dauf;
		
}


BOOL TestVerifyKun ()
{
	char zus_bez [25];
	char zus_bez_kun [25];
	CString ZusBez;
	CString ZusBezKun;
	CString ZahlArt;
	
	static int cursor = -1;
	BOOL ret = TRUE;
    if (VerifyKun)
	{
		if (cursor == -1)
		{
		  strcpy (zus_bez, " ");
		  strcpy (zus_bez_kun, " ");
		  DbClass.sqlin  ((short *) &kun.mdn, 1, 0);
		  DbClass.sqlin ((short *) &kun.pr_stu,  1, 0);
		  DbClass.sqlout ((char *) zus_bez, 0, 25);
		  cursor  = DbClass.sqlcursor ("select zus_bz from iprgrstufk where mdn = ? "
			                   "and pr_gr_stuf = ?");
		}

		CAdrDialog dlg (mamain1);

        if (cursor != -1)
		{
			memset (zus_bez, 0, sizeof (zus_bez));
			memset (zus_bez_kun, 0, sizeof (zus_bez_kun));
			DbClass.sqlopen (cursor);
			DbClass.sqlfetch (cursor);
		}
		sprintf (ptabn.ptwert, "%hd", kun.zahl_art);
		ptab_class.lese_ptab ("zahl_art", ptabn.ptwert);

		ZusBez.Format ("%hd %s", kun.pr_stu, zus_bez);
		ZahlArt.Format ("%hd %s", kun.zahl_art, ptabn.ptbez);
		dlg.SetAdrNr (kun.adr1);
		dlg.set_FreiTxt2 (kun.frei_txt2);
		dlg.set_PrStu ((LPSTR) ZusBez);
		dlg.set_ZahlArt ((LPSTR) ZahlArt);
		dlg.set_Pfand_ber (kun.pfand_ber);

		int pos = GetItemPos (&aufform, "kun");
		if (pos != -1)
		{
			dlg.SetDlgPos (aufform.mask[pos].feldid);
		}
		if (aufpListe.ZeigeOP) aufpListe.StarteProcessShowOP (kun.kun);
		if (dlg.DoModal () == IDCANCEL)
		{
			ret = FALSE;
		}
		aufpListe.KillProcessShowOP ();
	}
	return ret;
}

int doreload (void) //LAC-??
{
    if (ReiterAktAuf) 
	{
		aufpListe.doreload (0,0,1);
		FillCombo3 (1);
		FillCombo4 (1);
	}
    if (ReiterBestellvorschlag) 
	{
		aufpListe.doreload (0,0,2);
		FillCombo3 (2);
		FillCombo4 (2);
	}
    if (ReiterletzteBestellung) 
	{
		aufpListe.doreload (0,0,3);
		FillCombo3 (3);
		FillCombo4 (3);
	}
    if (ReiterKritischeMhd)  //LAC-84
	{
		aufpListe.doreload (0,0,4);
		FillCombo3 (5);
		FillCombo4 (5);
	}
    if (Reiterkommldat)  //LAC-116
	{
		aufpListe.doreload (0,0,5);
		FillCombo3 (6);
		FillCombo4 (6);
	}
    if (ReiterHWG1) 
	{
		aufpListe.doreload (dhwg1,dwg1,1);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG2) 
	{
		aufpListe.doreload (dhwg2,dwg2,2);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG3) 
	{
		aufpListe.doreload (dhwg3,dwg3,3);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG4) 
	{
		aufpListe.doreload (dhwg4,dwg4,4);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG5) 
	{
		aufpListe.doreload (dhwg5,dwg5,5);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG6) 
	{
		aufpListe.doreload (dhwg6,dwg6,6);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG7) 
	{
		aufpListe.doreload (dhwg7,dwg7,7);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG8) 
	{
		aufpListe.doreload (dhwg8,dwg8,8);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG9) 
	{
		aufpListe.doreload (dhwg9,dwg9,9);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG10) 
	{
		aufpListe.doreload (dhwg10,dwg10,10);
		FillCombo3 (4);
		FillCombo4 (4);
	}

    if (ReiterHWG11) 
	{
		aufpListe.doreload (dhwg11,dwg11,11);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG12) 
	{
		aufpListe.doreload (dhwg12,dwg12,12);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG13) 
	{
		aufpListe.doreload (dhwg13,dwg13,13);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG14) 
	{
		aufpListe.doreload (dhwg14,dwg14,14);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG15) 
	{
		aufpListe.doreload (dhwg15,dwg15,15);
		FillCombo3 (4);
		FillCombo4 (4);
	}
    if (ReiterHWG16) 
	{
		aufpListe.doreload (dhwg16,dwg16,16);
		FillCombo3 (4);
		FillCombo4 (4);
	}
	return 0;
}

BOOL LskVorhanden (void) //LAC-84
{
	static int dkun = 0;
	static int dlieferdat = 0;
	static short djr = 0;
	static short dkw = 0;
	static BOOL vorh = FALSE;
	int danz = 0;
	int dsqlstatus = -1;


	if (dkun == kun.kun && dlieferdat == dasc_to_long (ldat)) return vorh;



	/*** LAC-131
	dkun = kun.kun;
	dlieferdat = dasc_to_long (ldat);
	DbClass.sqlin ((long *) &aufk.kun_fil, 1, 0);
	DbClass.sqlin ((long *) &dkun, 2, 0);
	DbClass.sqlin ((long *) &dlieferdat, 2, 0);
	DbClass.sqlout ((long *) &danz, 2, 0);
	DbClass.sqlcomm ("select count(*) from lsk where  kun_fil = ? and kun = ? and lieferdat = ? ");		
	***/
	//LAC-131
		DbClass.sqlin ((long *) &kun.kun, 2, 0);
		DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		DbClass.sqlin ((long *) &kun.kun, 2, 0);
		DbClass.sqlin ((short *) &aufk.kun_fil, 1, 0);
		DbClass.sqlout ((short *) &djr, 1, 0);
		DbClass.sqlout ((short *) &dkw, 1, 0);
     	dsqlstatus = DbClass.sqlcomm ("select jr,kw "
			                        "from aufchart  "
									"where   kun = ? "  
									"and   auf in (select auf from lsk where mdn = ? and fil = ? and kun = ? and kun_fil = ? and ls_stat > 2 and lieferdat > today -60) "
									"order by jr desc, kw desc");  
		if (dsqlstatus == 0)
		{
			sprintf (txtkommldat, " Lieferung KW%hd", dkw);
			vorh =  TRUE;
		}

//	if (danz > 0) vorh = TRUE;
//	if (danz == 0) vorh = FALSE;
	return vorh;
}


int Write_Bestellvorschlag ()  //LAC-157
{
	int dsqlstatus = 100;
	int cursor_aktion; 
	int cursor_bestk; 
	int cursor_bestp; 
	int dauf = 0;
	int danz = 0;
	char shop_tv [2];


//===================================================
//============== Aktionen ===========================
//===================================================
   best_vorschlag.kun = aufk.kun;

		 DbClass.sqlin ((long *) &aufk.kun, 2, 0);
		 DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		 DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		 DbClass.sqlin ((long *) &aufk.lieferdat, 2, 0);
		 DbClass.sqlin ((long *) &aufk.lieferdat, 2, 0);
         DbClass.sqlout ((double *) &best_vorschlag.a, 3, 0);
         DbClass.sqlout ((char *) shop_tv, 0, 2);
         cursor_aktion = DbClass.sqlcursor ("select akiprgrstp.a, a_bas_erw.shop_tv from akiprgrstk, akiprgrstp, a_bas_erw,kun where akiprgrstk.aki_nr = akiprgrstp.aki_nr and " 
						" akiprgrstk.pr_gr_stuf = kun.pr_stu and akiprgrstk.mdn = kun.mdn and kun.kun = ? and kun.mdn = ? and kun.fil = ? and a_bas_erw.a = akiprgrstp.a "
						" and akiprgrstk.aki_von <= ? and akiprgrstk.aki_bis >= ? order by 1 ");


    DbClass.sqlin ((long *) &best_vorschlag.kun, 2, 0);
	DbClass.sqlcomm ("delete from best_vorschlag where kun = ? and kz in (\"T\", \"A\")");
	dsqlstatus = DbClass.sqlopen (cursor_aktion); 
	dsqlstatus = DbClass.sqlfetch (cursor_aktion);
	while (dsqlstatus == 0)
    {
 		best_vorschlag.me = 0.0;
		 DbClass.sqlin ((long *) &best_vorschlag.kun, 2, 0);
         DbClass.sqlin ((double *) &best_vorschlag.a, 3, 0);
         DbClass.sqlout ((double *) &best_vorschlag.me, 3, 0);
		DbClass.sqlcomm ("select me from best_vorschlag where kun = ? and a = ? and kz = \"S\"");

		if (strcmp(shop_tv,"J") == 0) 
		{
			strcpy (best_vorschlag.kz, "T");
		}
		else
		{
			strcpy (best_vorschlag.kz, "A");
		}
		best_vorschlag.bearb = aufk.lieferdat;
		
         DbClass.sqlin ((double *) &best_vorschlag.a, 3, 0);
		 DbClass.sqlin ((long *) &best_vorschlag.kun, 2, 0);
         DbClass.sqlin ((double *) &best_vorschlag.me, 3, 0);
         DbClass.sqlin ((char *) best_vorschlag.kz, 0, 2);
		 DbClass.sqlin ((long *) &best_vorschlag.bearb, 2, 0);
		DbClass.sqlcomm ("insert into best_vorschlag (a,kun,me,kz,bearb) values (?,?,?,?,?)");
		dsqlstatus = DbClass.sqlfetch (cursor_aktion);
	}

//===================================================
//============== Vorbestellung ======================
//===================================================

   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
   DbClass.sqlin ((long *) &aufk.kun, 2, 0);
   DbClass.sqlin ((long *) &aufk.lieferdat, 2, 0);
   DbClass.sqlout ((long *) &dVorbestellung_lieferdat, 2, 0);
   DbClass.sqlout ((long *) &dauf, 2, 0);
   DbClass.sqlout ((long *) &danz, 2, 0);
   cursor_bestk = DbClass.sqlcursor ("select aufk.lieferdat, aufk.auf,count(*) from aufk,aufp where aufk.mdn = ? and aufk.fil = ? "
							" and aufk.kun = ? and aufk.mdn = aufp.mdn and aufk.fil = aufp.fil and aufk.auf = aufp.auf "
							" and aufk.lieferdat < ? and aufk.lieferdat >= today -60 and aufk.auf_stat > 0 group by 1,2 order by 1 desc, 3 desc ");


   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
   DbClass.sqlin ((long *) &dauf, 2, 0);
   DbClass.sqlout ((double *) &best_vorschlag.a, 3, 0);
   DbClass.sqlout ((double *) &best_vorschlag.me, 3, 0);
   cursor_bestp = DbClass.sqlcursor ("select a, bestellvorschl from aufp where mdn = ? and fil = ? and auf = ? and bestellvorschl > 0 order by a ");


    DbClass.sqlin ((long *) &best_vorschlag.kun, 2, 0);
	DbClass.sqlcomm ("delete from best_vorschlag where kun = ? and kz = \"B\"");
	dsqlstatus = DbClass.sqlopen (cursor_bestk); 
	dsqlstatus = DbClass.sqlfetch (cursor_bestk);
	if (dsqlstatus == 0)
    {
		dsqlstatus = DbClass.sqlopen (cursor_bestp); 
		dsqlstatus = DbClass.sqlfetch (cursor_bestp);
		while (dsqlstatus == 0)
		{
			strcpy (best_vorschlag.kz, "B");
			best_vorschlag.bearb = aufk.lieferdat;
			
			DbClass.sqlin ((double *) &best_vorschlag.a, 3, 0);
			DbClass.sqlin ((long *) &best_vorschlag.kun, 2, 0);
			DbClass.sqlin ((double *) &best_vorschlag.me, 3, 0);
			DbClass.sqlin ((char *) best_vorschlag.kz, 0, 2);
			DbClass.sqlin ((long *) &best_vorschlag.bearb, 2, 0);
			DbClass.sqlcomm ("insert into best_vorschlag (a,kun,me,kz,bearb) values (?,?,?,?,?)");
			dsqlstatus = DbClass.sqlfetch (cursor_bestp);
		}
	}

	return 0;
}

static int setzeStartKun () //LAC-184
{
	if (StartKun > 0) 
	{
		sprintf (kuns, "%ld", StartKun); //LAC-184
       // ChoiseKunLief0 ();
		aufk.mdn = StartMdn;
		aufk.fil = 0;
		lesekun ();
		leseauf ();
		testkun0 ();
		int i = 0;
	}
	return 0;
}




class CStornoAuf
{
	public :
		Text ptwert;
		Text ptbez;
		Text ptbezk;
		Text StrOut;
		CStornoAuf ();
		CStornoAuf (Text&, Text&, Text&);
		CStornoAuf (LPSTR, LPSTR, LPSTR);
		~CStornoAuf ();
		LPSTR ToString ();
};

CStornoAuf::CStornoAuf ()
{
	ptwert = "";
	ptbez = "";
	ptbezk = "";
}

CStornoAuf::CStornoAuf (Text& ptwert, Text& ptbez, Text& ptbezk)
{
	this->ptwert = ptwert;
	this->ptbez = ptbez;
	this->ptbezk = ptbezk;
}

CStornoAuf::CStornoAuf (LPSTR ptwert, LPSTR ptbez, LPSTR ptbezk)
{
	this->ptwert = ptwert;
	this->ptbez = ptbez;
	this->ptbezk = ptbezk;
}

CStornoAuf::~CStornoAuf ()
{
}

LPSTR CStornoAuf::ToString ()
{
	ptwert.Trim ();
	ptbez.Trim ();
	ptbezk.Trim ();
	StrOut.Format ("%s; %s", ptwert.GetBuffer (), ptbez.GetBuffer ());
	return StrOut.GetBuffer ();
}


static std::vector<CStornoAuf *> StornoTab;
static int StornoCursor = -1;

int GetStornoAuf (char *wert)
{

   wsplit (wert, ";");

   Text ptwert = wort[0];
   ptwert.Trim ();
   int i = 0;
   for (std::vector<CStornoAuf *>::iterator pk = StornoTab.begin ();pk != StornoTab.end ();pk ++, i ++)
			  {
					CStornoAuf *StornoAuf = *pk;
					StornoAuf->ptwert.Trim ();
					if (StornoAuf->ptwert == ptwert) 
					{
						return atoi (ptwert.GetBuffer());
					}
			  }


    return 0;
}
int GetStornoidx (long wert)
{
	char cwert[4];
   sprintf (cwert,"%ld",wert);
   Text ptwert = cwert;
   ptwert.Trim ();
   int i = 0;
   for (std::vector<CStornoAuf *>::iterator pk = StornoTab.begin ();pk != StornoTab.end ();pk ++, i ++)
			  {
					CStornoAuf *StornoAuf = *pk;
					StornoAuf->ptwert.Trim ();
					if (StornoAuf->ptwert == ptwert) 
					{
						return i + 1;
					}
			  }
    return 0;
}

void EnterStorno (void)
{
	   char ptitem[19];
	   char ptwert[4];
	   char ptbez [33];
	   char ptbezk [9];

	   strcpy (ptitem, "storno_grund");

	   aufk.mdn = atoi (mdn);
	   aufk.fil = atoi (fil);
	   if (StornoCursor == -1)
	   {
		   DbClass.sqlin  ((char *) ptitem, 0, sizeof (ptitem));
		   DbClass.sqlout ((char *) ptwert, 0, sizeof (ptwert));
		   DbClass.sqlout ((char *) ptbez, 0, sizeof (ptbez));
		   DbClass.sqlout ((char *) ptbezk, 0, sizeof (ptbezk));
		   StornoCursor = DbClass.sqlcursor ("select ptwert,ptbez,ptbezk from ptabn "
			                               "where ptitem = ?  order by ptlfnr ");

	   }
	   if (StornoTab.size () == 0)
	   {
			DbClass.sqlopen (StornoCursor);
			if (DbClass.sqlfetch (StornoCursor) == 0)
			{
				CStornoAuf *StornoAuf = new CStornoAuf (ptwert, ptbez, ptbezk);
				StornoTab.push_back (StornoAuf);
			}
			else 
			{
			   disp_mess ("Es sind keine StornoGr�nde (ptitem storno_grund) angelegt", 2);
			   return;
			}
			while (DbClass.sqlfetch (StornoCursor) == 0)
			{
				CStornoAuf *StornoAuf = new CStornoAuf (ptwert, ptbez, ptbezk);
				StornoTab.push_back (StornoAuf);
			}
			ComboTab = new LPSTR [StornoTab.size () + 2];

			int i = 0;
			for (std::vector<CStornoAuf *>::iterator pk = StornoTab.begin ();pk != StornoTab.end (); pk ++, i ++)
			{
				  CStornoAuf *StornoAuf = *pk;
			      ComboTab[i] = StornoAuf->ToString ();
			}
			ComboTab[i] = " ";
			ComboTab[i+1] = NULL;
	   }				  
													   
													  
	   EnterF Enter;
	   Enter.ComboList = TRUE;
       Enter.SetColor (GetSysColor (COLOR_3DFACE));

       char StornoGrund1 [80];
	   strcpy (StornoGrund1, "");  
       char StornoGrund2 [80];
	   strcpy (StornoGrund2, "");  
       char StornoGrund3 [80];
	   strcpy (StornoGrund3, "");  
       char StornoGrund4 [129];
	   strcpy (StornoGrund4, "");  

	   if (StornoTab.size () > 0)
	   {
		   int idx = GetStornoidx (aufk.storno_grund1);
		   if (idx > 0) strcpy (StornoGrund1, ComboTab[idx-1]);  
		   idx = GetStornoidx (aufk.storno_grund2);
		   if (idx > 0) strcpy (StornoGrund2, ComboTab[idx-1]);  
		   idx = GetStornoidx (aufk.storno_grund3);
		   if (idx > 0) strcpy (StornoGrund3, ComboTab[idx-1]);  
		   strcpy (StornoGrund4,aufk.storno_text);
	   }
	   else
	   {
		   disp_mess ("Es sind keine StornoGr�nde (ptitem storno_grund) angelegt", 2);
		   return;
	   }


	   Enter.EnterTextMehrf (mamain1, "Erfassen der Stornogr�nde",
		   "Stornogrund1", StornoGrund1, 30, "", ComboTab,
		   "Stornogrund2", StornoGrund2, 30, "", ComboTab,
		   "Stornogrund3", StornoGrund3, 30, "", ComboTab,
		   "Text",         StornoGrund4, 90, "", NULL
		   );
	   if (syskey != KEY5)
	   {
		   aufk.storno_grund1 = GetStornoAuf (StornoGrund1);
		   aufk.storno_grund2 = GetStornoAuf (StornoGrund2);
		   aufk.storno_grund3 = GetStornoAuf (StornoGrund3);
		   strcpy (aufk.storno_text,StornoGrund4);
	   }
		   
}

void DestroyStorno ()
{
	if (PersCursor != -1)
	{
		DbClass.sqlclose (PersCursor);
		PersCursor = -1;
	}

	for (std::vector<CStornoAuf *>::iterator pk = StornoTab.begin ();
			          pk != StornoTab.end ();
 			          pk ++)
					  {
						  CStornoAuf *StornoAuf = *pk;
						  delete StornoAuf;
					  }
	StornoTab.clear ();
	if (ComboTab != NULL)
	{
		delete ComboTab;
		ComboTab = NULL;
	}

}
