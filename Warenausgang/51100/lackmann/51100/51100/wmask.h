#ifndef _WMASK_DEF
#define _WMASK_DEF
#define NORMAL 0
#define EDIT 0
#define REVERSE 0
#define DISPLAYONLY 1
#define DEFBUTTON 1
#define BUTTON 2
#define CHECKBUTTON 8
#define LISTBOX 4
#define READONLY 8
#define COMBOBOX 16
#define UPSHIFT 32
#define DISABLED 64
#define ICON 128
#define COLBUTTON 256
#define BORDERED 512
#define REMOVED 1024
#define PASSWORD 2048
#define HEXAGON 0x1000
#define SCROLL 0x2000
#define UPDOWN 0x4000

#define LBOX 101
#define BT_OK 102
#define BT_CANCEL 103
#include "itemc.h"
#include "mo_draw.h"

#define WM_BEFORE 901

#define LB_TITLE WM_USER + 100
#define LB_VPOS  WM_USER + 101
#define LBN_INSERTROW WM_USER + 102
#define LBN_DELETEROW WM_USER + 103
#define WM_SETCOLFOCUS WM_USER + 104
#define WM_KILLCOLFOCUS WM_USER + 105
#define WM_LISTINSERT WM_USER + 106

#define KEYESC 27
#define KEY1  801
#define KEY2  802
#define KEY3  803
#define KEY4  804
#define KEY5  805
#define KEY6  806
#define KEY7  807
#define KEY8  808
#define KEY9  809
#define KEY10 810
#define KEYDOWN 811
#define KEYUP 812
#define KEYTAB 813
#define KEYSTAB 814
#define KEYLEFT 815
#define KEYRIGHT 816
#define KEYPGD 817
#define KEYPGU 818
#define KEYCR 13
#define KEY11 819
#define KEY12 820

#define WHITECOL  RGB (255,255,255)
#define BLACKCOL  RGB (0,0,0)
#define LTGRAYCOL RGB (192,192,192)
#define GRAYCOL RGB (128,128,128)
#define BLUECOL RGB (0, 0, 255)
#define GREENCOL RGB (0, 255, 0)
#define REDCOL RGB (255, 0, 0)
#define YELLOWCOL RGB (255, 255, 0)
#define DKYELLOWCOL RGB (175, 175, 0)

#define EZ_ATTR_BOLD 1
#define EZ_ATTR_ITALIC 2
#define EZ_ATTR_UNDERLINE 4
#define EZ_ATTR_STRIKEOUT 8

#define FMMAX 200

extern int _WMDEBUG;
extern FIGURES fig_class;

struct MFONT
{
     char *FontName;
     int  FontHeight;
     int  FontWidth;
     int  FontAttribute;
     COLORREF  FontColor;
     COLORREF  FontBkColor;
     int PosMode;
     HFONT hFont;
};

struct COLBUT
{
     char *text1;
     int tx1;
     int ty1;
     char *text2;
     int tx2;
     int ty2;
     char *text3;
     int tx3;
     int ty3;
     HBITMAP bmp;
     int bmpx;
     int bmpy;
     HICON icon;
     int icox;
     int icoy;
     COLORREF Color;
     COLORREF BkColor;
     BOOL aktivate;
};

struct HEXA
{
     char *text1;
     int tx1;
     int ty1;
     char *text2;
     int tx2;
     int ty2;
     char *text3;
     int tx3;
     int ty3;
     HBITMAP bmp;
     int bmpx;
     int bmpy;
     HICON icon;
     int icox;
     int icoy;
     COLORREF Color;
     COLORREF BkColor;
     BOOL aktivate;
};


typedef struct COLBUT ColButton;

typedef struct MFONT mfont;

struct FELD
{
            ITEM *item;
            short length;
            short rows;
            short pos [2];
            HWND feldid;
            char *picture;
            unsigned int attribut;
            int (*before) ();
            int (*after) ();
            int BuId;
};

typedef struct FELD field;

struct COMBOFIELD
{
           int fieldid;
           int cbdim;
           int cbanz;
           char *cbwerte;
};

typedef struct COMBOFIELD combofield;

/*  Maske                                      */

struct FORM
{
           short fieldanz;
           short frmstart;
           short frmscroll;
           field *mask;
           char *caption;
           int (*before) ();
           int (*after) ();
           combofield *cbfield;
           mfont *font;
};

typedef struct FORM form;

struct UDTAB
{
           int Upper;
           int Lower;
           int Pos;
};

struct LMENUE
  { char *menArr [0x1000];
    char *menArr1 [0x1000];
    char *menArr2 [0x1000];
    char *menTitle[20];
    int menTAnz;
    char *menVlpos;
    int menAnz;
    int menSAnz;
    int menWzeilen;
    int menWspalten;
    int menZeile;
    int menSpalte;
    int menSelect;
    int menFpos;
    HFONT menhFont;
    RECT rect;
    RECT trect;
    RECT srect;
  };


struct DlgFont
               {char *fontname;
                int fHeight;
                int fWidth;
                int fAttr;
               };

typedef struct {int zeile;
                int spalte;
                int zeilen;
                int spalten;
                struct DlgFont *font;
               } WINDOW;

extern form *current_form;
extern int currentfield;
extern int currentbutton;
extern int syskey;
extern int opencombobox;
extern HWND AktivDialog;
extern HWND AktivWindow;
extern HWND SendWindow;
extern HWND AktivMenue;
extern int (*fkt_aktiv[])();

int writelog (char *, ...);
void disp_mess (char *, int modus);
int print_mess (int , char * , ...);
void EnterLBox (int, int, int, int, char *);
void EnterLBoxBu (int, int, int, int, char *);
void SetVeLines (int);
void SetHoLines (int);
void SetMenStart (int);
void SetButtonTab (BOOL);
HWND OpenWindow (int, int, int, int, int, HANDLE);
HWND OpenWindowCh (int, int, int, int,HANDLE);
HWND OpenWindowChC (int, int, int, int,HANDLE, char *);
HWND OpenWindowEx (int, int, int, int, long style, HANDLE);
HWND OpenColWindow (int, int, int, int,long,COLORREF, WNDPROC);
HWND OpenColWindowEx (int, int, int, int,long,COLORREF, WNDPROC);
HWND OpenQuery (HWND,int, int, int, int, HANDLE);
HWND OpenListWindow (HWND,int, int, int, int, BOOL);
HWND OpenSListWindow (int, int, int, int, BOOL);
HWND OpenListWindow (int, int, int, int, BOOL);
HWND OpenListWindowBu (int, int, int, int, BOOL);
HWND OpenListWindowBu (HWND, int, int, int, int, BOOL);
HWND OpenListWindowEn (int, int, int, int);
HWND OpenListWindowEnF (int, int, int, int, int);
HWND OpenListWindowEnEx (int, int, int, int);
int InsertListUb (char *);
int InsertListRow (char *);
int EnterQueryListBox (void);
void ShowElist (char *, int, char *, int, form* );
void ShowNewElist (char *, int, int);
void ElistVl (form*);
void ElistUb (form*);
void CloseUbControls (void);
void EnterElist (HWND, char *, int, char *, int, form* );
void CloseEWindow (HWND);
void break_list (void);
void init_break_list (void);
void break_enter (void);
void break_end (void);
void no_break_enter (void);
void no_break_end (void);
void break_enter (void);
void break_end (void);
int testkeys (void);
int set_fkt (int (*fkt_proc) (), short);
int set_keypgd (int (*fkt_proc) ());
int set_keypgu (int (*fkt_proc) ());
void save_fkt (short);
void restore_fkt (short);
void SetNoDlgProc (int (*NoDlgProc) (MSG *));
void SetBeforeList (void (*) ());
void SetAfterList (void (*) ());
void SetFillEmpty (void (*) ());
void SetDblClck (void (*) (int), BOOL);
HFONT EzCreateFont (HDC, char *, int, int, int, BOOL);
int GetListAnz (void);
int GetListPos (void);
void Setlistenter (int);
void SetListEWindow (int);
void SetListBorder (DWORD);
void SetAktivWindow (HWND);
void SetMouseLock (BOOL);
void SetMouseLockProc (int (*) (POINT *));
void  SetLMenue (char *, int, int, char *, char *);
void SetBorder (DWORD);
int IsFirstListPos (void);
int IsLastListPos (void);
int IsLastEnterField (int);
void SetCurrentField (int);
void SetCurrentFocus (int);
void display_form (HWND, form *, int, int);
void display_field (HWND, field *, int, int);
void CloseControls (form *);
void InitControls (form *);
void CloseControl (form *, int);
void move_field (form *, field *, int, int, int, int,int, int); 
void enter_form (HWND, form *, int, int);
void create_enter_form (HWND, form *, int, int);
void create_list_enter (HWND, form *, int, int);
void EnterListZeile (HWND, form *, int, int);
int EnterButtonWindow (HWND, form *, int, int);
int EnterButtonWindow (HWND, form *, int, int, int, int);
void SaveMenue (void);
void RestoreMenue (void);
void InitMenue (void);
int InsertListRow (char *);
void ToFormat (char *, field *);
void SetLbox (void);
void RefreshLbox (void);
void DisplayLZeile (void);
void MouseTest (HWND, UINT);
int IsMouseMessage (MSG *);
void SetFont (HFONT);
void SetNewStdFont (mfont *);
void DestroyFonts (void);
void SetHwFont (HWND, HFONT);
HFONT GetHwFont (HWND);
void SetColFocus (HWND);
void KillColFocus (HWND);
void KillFieldCol (field *, mfont *);
void stdfont (void);
void spezfont (mfont *);
HFONT SetWindowFont (HWND);
void SetModuleName (char *);
void SetStdProc (WNDPROC);
HFONT CreateStdFont (HWND);
void GetFrmSize (form *, int *, int *);
void GetFrmRect (form *, RECT *);
void GetFrmRectEx (form *, mfont *, RECT *);
void SetEnvFont (void);
void SetDiffPixel (int, int, int, int);
void GetDiffPixel (HWND, HWND, int *, int *, int *, int *);
int MouseinWindow (HWND, POINT *);
int MouseinClient (HWND, POINT *);
int MouseinDlg (HWND, POINT *);
void SetListFont (BOOL);
void SetSscroll (int, int);
HWND GetEWindow (void);
void UnsetAktCol (form *, HWND);
void InitAktiveCol (form *);
field *GetFormFeld (HWND);
void HexagonPressed (HWND, POINT *);
void AktivateHexagon (field *);
void UnAktivateHexa (form *);
HWND GethListBox (void);
void SethListBox (HWND);
HWND Getlbox (void);
HWND GetlboxBar (void);
void Setlbox (HWND);
void GetMenue (struct LMENUE *);
void SetMenue (struct LMENUE *);
void MenTextSave (struct LMENUE *);
HWND IsDlgChild (POINT *);
BOOL IsModalDlg (void);
void SetModalDlg (int);
int ActivateColButton (HWND, form *, int, int, int);
void DisplayAfterEnter (int);
void SetVLines (int);
void SetHLines (int);
int GetFormZeile (form *, int , int);
int GetFormSpalte (form *, int);
void DisablehWnd (HWND);
void OnDestroy (HWND);
int fillcombobox (form *, int);
void DestroyFrmWindow (void);
void SetUpDownTab (UDTAB *, int);
void InitForm (form *);
void InitField (field *);
#endif