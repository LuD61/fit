#ifndef AKTION_OK
#define AKTION_OK 1
struct AKT_KOPF {
   short     akt;
   char      akt_bz[25];
   short     akt_kte;
   long      bel_akt_bis;
   long      bel_akt_von;
   short     fil;
   short     fil_gr;
   char      gue[2];
   long      lad_akt_bis;
   long      lad_akt_von;
   char      lad_akv_sa[2];
   char      lief_akv_sa[2];
   short     mdn;
   short     mdn_gr;
   char      zeit_von[7];
   char      zeit_bis[7];
   short     zeit_von_n;
   short     zeit_bis_n;
};

struct AKT_POS {
   double    a;
   short     akt;
   short     key_typ_sint;
   double    pr_ek_sa;
   double    pr_ek_sa_euro;
   double    pr_vk_sa;
   double    pr_vk_sa_euro;
};

extern struct AKT_KOPF akt_kopf, akt_kopf_null;
extern struct AKT_POS akt_pos, akt_pos_null;

class AKTION_CLASS
{
       private :
            short cursor_akt_kopf;
            short cursor_akt_kopf_d;
            short cursor_gue;
            short cursor_akt_pos;
            short cursor_akt_pos_a;
            long  aktdat;

            void prepare (void);       
            void prepare_d (void);       
            void prepare_gue (void);       
            void prepare_pos (void);       
            void prepare_pos_a (void);       
       public:
           AKTION_CLASS ()
           {
                    cursor_akt_kopf  = -1;
                    cursor_akt_kopf_d = -1;
                    cursor_gue       = -1;
                    cursor_akt_pos   = -1;
                    cursor_akt_pos_a = -1;
           }
           int lese_akt_kopf (short);
           int lese_akt_kopf (void);
           int lese_akt_kopf_d (short, short, short, short, char *);
           int lese_akt_kopf_d (void);
           int lese_gue (short, short, short);
           int lese_gue ();
           int lese_akt_pos (short);
           int lese_akt_pos (void);
           int lese_akt_pos_a (short, double);
           int lese_akt_pos_a (void);
           void close_akt_kopf (void);
           void close_akt_kopf_d (void);
           void close_gue (void);
           void close_akt_pos (void);
           void close_akt_pos_a (void);
};
#endif
