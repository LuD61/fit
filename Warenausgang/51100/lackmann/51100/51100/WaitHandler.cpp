// WaitHandler.cpp: Implementierung der Klasse CWaitHandler.
//
//////////////////////////////////////////////////////////////////////
#include "WaitHandler.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CWaitHandler::CWaitHandler(HWND Parent) : 
			CDialog (Id, Parent)
{
	m_DlgColor  = RGB (255, 255, 204);
	m_DlgBrush  = NULL;
	m_TextColor  = RGB (0, 0, 160);
	m_TextBrush  = NULL;
	m_Min = 0;
	m_Max = 100;
}

CWaitHandler::~CWaitHandler()
{

}

void CWaitHandler::AttachControls ()
{
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_TEXT,   m_InfoText,  CDialogItem::String)); 
}

BOOL CWaitHandler::OnInitDialog ()
{
	BOOL ret = CDialog::OnInitDialog ();
	m_TextEdit.Attach (GethWnd (IDC_TEXT));
	m_Progress.Attach (GethWnd (IDC_PROGRESS));
	SetWindowText (m_hWnd, m_Title);
	SetWindowPos (m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	CFont font;
	m_FatFont.Create ("Arial", 20, TRUE);
	m_TextEdit.SetFont (m_FatFont);

	m_Progress.SetRange (0, 100);

	Display ();
	return TRUE;
}

BOOL CWaitHandler::PreTranslateMessage (MSG *msg)
{
/*
	switch (msg->message)
	{
	case WM_KEYDOWN:
		switch (msg->wParam)
		{

		case VK_F5 :
			return OnF5 ();
		}
	}
*/
	return FALSE;
}

void CWaitHandler::SetRange (int min, int max)
{
	m_Min = min;
	m_Max = max;
	if ((HWND) m_Progress != NULL && IsWindow ((HWND) m_Progress))
	{
		m_Progress.SetRange (min, max);
	}
}

void CWaitHandler::SetStep (int step)
{
	if ((HWND) m_Progress != NULL && IsWindow ((HWND) m_Progress))
	{
		m_Progress.SetStep (step);
	}
}

void CWaitHandler::StepIt ()
{
	if ((HWND) m_Progress != NULL && IsWindow ((HWND) m_Progress))
	{
		m_Progress.StepIt ();
	}
}

HBRUSH CWaitHandler::OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor)
{
	if (m_DlgBrush == NULL)
	{
		m_DlgBrush = CreateSolidBrush (m_DlgColor);
	}

	if (nCtlColor == CTLCOLOR_DLG)
	{
			SetBkColor (hDC, m_DlgColor);
			return m_DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC)
	{
			SetTextColor (hDC, m_TextColor);
			SetBkColor (hDC, m_DlgColor);
			SetBkMode (hDC, TRANSPARENT);
			return m_DlgBrush;
	}
	return NULL;
}

void CWaitHandler::Show ()
{
	CDialog::Show ();
	SetWindowText (m_hWnd, m_Title);
	m_TextEdit.SetWindowText ((LPSTR) m_InfoText); 
}


void CWaitHandler::ProcessEvent ()
{
  MSG msg;
  
  if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE) != NULL)
  {
	  TranslateMessage (&msg);
	  DispatchMessage (&msg);
	  Sleep (10);
  }
}