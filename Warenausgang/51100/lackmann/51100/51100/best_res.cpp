#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "best_res.h"

struct BEST_RES best_res, best_res_null;

void BEST_RES_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &best_res.mdn, 1, 0);
            ins_quest ((char *) &best_res.fil, 1, 0);
            ins_quest ((char *) &best_res.a, 3, 0);
            ins_quest ((char *) &best_res.auf, 2, 0);
            ins_quest ((char *) &best_res.lief_term, 2, 0);
    out_quest ((char *) &best_res.a,3,0);
    out_quest ((char *) &best_res.auf,2,0);
    out_quest ((char *) &best_res.fil,1,0);
    out_quest ((char *) &best_res.kun,2,0);
    out_quest ((char *) &best_res.lief_term,2,0);
    out_quest ((char *) &best_res.mdn,1,0);
    out_quest ((char *) &best_res.me,3,0);
    out_quest ((char *) &best_res.melde_term,2,0);
    out_quest ((char *) &best_res.stat,1,0);
    out_quest ((char *) &best_res.kun_fil,1,0);
    out_quest ((char *) best_res.chargennr,0,31);
            cursor = prepare_sql ("select best_res.a,  "
"best_res.auf,  best_res.fil,  best_res.kun,  best_res.lief_term,  "
"best_res.mdn,  best_res.me,  best_res.melde_term,  best_res.stat,  "
"best_res.kun_fil,  best_res.chargennr from best_res "

#line 30 "best_res.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ? "
                                  "and   auf = ? "
                                  "and   lief_term = ?");

    ins_quest ((char *) &best_res.a,3,0);
    ins_quest ((char *) &best_res.auf,2,0);
    ins_quest ((char *) &best_res.fil,1,0);
    ins_quest ((char *) &best_res.kun,2,0);
    ins_quest ((char *) &best_res.lief_term,2,0);
    ins_quest ((char *) &best_res.mdn,1,0);
    ins_quest ((char *) &best_res.me,3,0);
    ins_quest ((char *) &best_res.melde_term,2,0);
    ins_quest ((char *) &best_res.stat,1,0);
    ins_quest ((char *) &best_res.kun_fil,1,0);
    ins_quest ((char *) best_res.chargennr,0,31);
            sqltext = "update best_res set best_res.a = ?,  "
"best_res.auf = ?,  best_res.fil = ?,  best_res.kun = ?,  "
"best_res.lief_term = ?,  best_res.mdn = ?,  best_res.me = ?,  "
"best_res.melde_term = ?,  best_res.stat = ?,  best_res.kun_fil = ?,  "
"best_res.chargennr = ? "

#line 37 "best_res.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ? "
                                  "and   auf = ? "
                                  "and   lief_term = ?";

            ins_quest ((char *) &best_res.mdn, 1, 0);
            ins_quest ((char *) &best_res.fil, 1, 0);
            ins_quest ((char *) &best_res.a, 3, 0);
            ins_quest ((char *) &best_res.auf, 2, 0);
            ins_quest ((char *) &best_res.lief_term, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &best_res.mdn, 1, 0);
            ins_quest ((char *) &best_res.fil, 1, 0);
            ins_quest ((char *) &best_res.a, 3, 0);
            ins_quest ((char *) &best_res.auf, 2, 0);
            ins_quest ((char *) &best_res.lief_term, 2, 0);
            test_upd_cursor = prepare_sql ("select auf from best_res "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ? "
                                  "and   auf = ? "
                                  "and   lief_term = ?");
            ins_quest ((char *) &best_res.mdn, 1, 0);
            ins_quest ((char *) &best_res.fil, 1, 0);
            ins_quest ((char *) &best_res.a, 3, 0);
            ins_quest ((char *) &best_res.auf, 2, 0);
            ins_quest ((char *) &best_res.lief_term, 2, 0);
            del_cursor = prepare_sql ("delete from best_res "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ? "
                                  "and   auf = ? "
                                  "and   lief_term = ?");

            ins_quest ((char *) &best_res.mdn, 1, 0);
            ins_quest ((char *) &best_res.fil, 1, 0);
            ins_quest ((char *) &best_res.auf, 2, 0);
            ins_quest ((char *) &best_res.a, 3, 0);
            del_cursor_a = prepare_sql ("delete from best_res "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   auf = ? "
                                  "and   a = ?");
            ins_quest ((char *) &best_res.mdn, 1, 0);
            ins_quest ((char *) &best_res.fil, 1, 0);
            ins_quest ((char *) &best_res.auf, 2, 0);
            del_cursor_auf = prepare_sql ("delete from best_res "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   auf = ?");
    ins_quest ((char *) &best_res.a,3,0);
    ins_quest ((char *) &best_res.auf,2,0);
    ins_quest ((char *) &best_res.fil,1,0);
    ins_quest ((char *) &best_res.kun,2,0);
    ins_quest ((char *) &best_res.lief_term,2,0);
    ins_quest ((char *) &best_res.mdn,1,0);
    ins_quest ((char *) &best_res.me,3,0);
    ins_quest ((char *) &best_res.melde_term,2,0);
    ins_quest ((char *) &best_res.stat,1,0);
    ins_quest ((char *) &best_res.kun_fil,1,0);
    ins_quest ((char *) best_res.chargennr,0,31);
            ins_cursor = prepare_sql ("insert into best_res ("
"a,  auf,  fil,  kun,  lief_term,  mdn,  me,  melde_term,  stat,  kun_fil,  chargennr) "

#line 90 "best_res.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?)"); 

#line 92 "best_res.rpp"
}
int BEST_RES_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int BEST_RES_CLASS::delete_a (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (del_cursor_a == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_a);
         return (sqlstatus);
}

int BEST_RES_CLASS::delete_auf (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (del_cursor_auf == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_auf);
         return (sqlstatus);
}

