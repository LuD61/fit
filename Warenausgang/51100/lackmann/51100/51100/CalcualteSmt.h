// CalcualteSmt.h: Schnittstelle f�r die Klasse CCalcualteSmt.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CALCUALTESMT_H__1F7D85C8_47A3_4CF5_B415_D7F4BEFC11B7__INCLUDED_)
#define AFX_CALCUALTESMT_H__1F7D85C8_47A3_4CF5_B415_D7F4BEFC11B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "aufps.h"
#include "SortList.h"
#include "ControlPlugins.h"
#include "ptab.h"
#include "a_bas.h"
#include "ls.h"

class CSmtOrderItem
{
private :
	double m_A;
	short m_Smt;
	double m_Me;
	double m_Gew;
	double m_Vk;
	double m_A_gew;
public:

	void set_Smt (short smt)
	{
		m_Smt = smt;
	}

	short Smt()
	{
		return m_Smt;
	}

	void set_A (double a)
	{
		m_A = a;
	}

	double A()
	{
		return m_A;
	}

	void set_Me (double me)
	{
		m_Me = me;
	}

	double Me()
	{
		return m_Me;
	}

	void set_Gew (double gew)
	{
		m_Gew = gew;
	}

	double Gew()
	{
		return m_Gew;
	}

	void set_Vk (double vk)
	{
		m_Vk = vk;
	}

	double Vk()
	{
		return m_Vk;
	}

	void set_A_gew (double a_gew)
	{
		if (a_gew == 0.0)
		{
			a_gew = 1.0;
		}
		m_A_gew = a_gew;
	}

	double A_gew()
	{
		return m_A_gew;
	}

	CSmtOrderItem ();
};

class CSmtOrderList : public CSortList<CSmtOrderItem *>
{
public:
	CSmtOrderList ();	
	~CSmtOrderList ();
	void DestroyElements (); 
	double GetOrderAmount (short smt);
	double GetOrderWeight (short smt);
	double GetLiq (short smt);
	int GetStndAllElements (short smt);
	double GetSmtQuot (short smt);
	double GetSmtQuot ();
	int GetSmtAnzahl (short smt);
};

class CCompareOrderInterface : public CCompareInterface<CSmtOrderItem *>
{
public:
	virtual int Compare (CSmtOrderItem *Item1, CSmtOrderItem *Item2);
};

class CSmtList : public CSortList<short>
{
public:
	CSmtList ();	
	~CSmtList ();
};

class CCompareSmtInterface : public CCompareInterface<short>
{
public:
	virtual int Compare (short Item1, short Item2);
};


class CCalcualteSmt  
{
private:
	PTAB_CLASS Ptabn;
	CCompareOrderInterface Compare;
	CSmtOrderList m_SmtOrderList;
	CCompareSmtInterface SmtCompare;
	CSmtList m_SmtList;
	CControlPlugins *m_Controls;
	HWND m_hWnd;
public:
	void set_Controls (CControlPlugins *Controls)
	{
		m_Controls = Controls;;
	}

	void set_HWnd (HWND hWnd)
	{
		m_hWnd = hWnd;
	}

	CCalcualteSmt();
	virtual ~CCalcualteSmt();
	short Calculate (AUFPS *aufps, int size, int cfg_AnzTageSollwerte, int mench);
	void Calculate (int, int, int );
	BOOL IsDiscountArticle (double a, LPSTR smt);
};

#endif // !defined(AFX_CALCUALTESMT_H__1F7D85C8_47A3_4CF5_B415_D7F4BEFC11B7__INCLUDED_)
