// CAufps.h: Schnittstelle f�r die Klasse CCAufps.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CAUFPS_H__CF46E84F_4BC8_4D40_BD37_E01421C3665B__INCLUDED_)
#define AFX_CAUFPS_H__CF46E84F_4BC8_4D40_BD37_E01421C3665B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <comdef.h>

class CCAufps  
{
public:
	CCAufps();
	virtual ~CCAufps();

	IDispatch *iCAufps;
	BOOL IdsOK;

	OLECHAR FAR *NGetPosi;
	OLECHAR FAR *NGetA;
	OLECHAR FAR *NGetABz1;
	OLECHAR FAR *NGetAufVkPr;
	OLECHAR FAR *NGetAufLadPr;
	OLECHAR FAR *NGetAufLadPr0;
	OLECHAR FAR *NGetAufLadPrPrc;
	OLECHAR FAR *NGetMarge;
	OLECHAR FAR *NGetSaKzSint;
	OLECHAR FAR *NGetAGew;
	OLECHAR FAR *NGetDrFolge;
	OLECHAR FAR *NGetMeEinh;
	OLECHAR FAR *NGetSmt;
	OLECHAR FAR *NGetBasisMeBz;
	OLECHAR FAR *NGetMeEinhKun;
	OLECHAR FAR *NGetMeEinhKun1;
	OLECHAR FAR *NGetMeEinhKun2;
	OLECHAR FAR *NGetMeEinhKun3;
	OLECHAR FAR *NGetInh1;
	OLECHAR FAR *NGetInh2;
	OLECHAR FAR *NGetInh3;
	OLECHAR FAR *NGetMeBz;
	OLECHAR FAR *NGetLastMe;
	OLECHAR FAR *NGetLastLDat;
	OLECHAR FAR *NGetLastPrVk;
	OLECHAR FAR *NGetEndOfRecords;
	OLECHAR FAR *NGetBsd;
	OLECHAR FAR *NGetBsd2;

    DISPID IdGetPosi;
    DISPID IdGetA;
    DISPID IdGetABz1;
	DISPID IdGetAufVkPr;
	DISPID IdGetAufLadPr;
	DISPID IdGetAufLadPr0;
	DISPID IdGetAufLadPrPrc;
	DISPID IdGetMarge;
	DISPID IdGetSaKzSint;
	DISPID IdGetAGew;
	DISPID IdGetDrFolge;
	DISPID IdGetMeEinh;
	DISPID IdGetSmt;
	DISPID IdGetBasisMeBz;
	DISPID IdGetMeEinhKun;
	DISPID IdGetMeEinhKun1;
	DISPID IdGetMeEinhKun2;
	DISPID IdGetMeEinhKun3;
	DISPID IdGetInh1;
	DISPID IdGetInh2;
	DISPID IdGetInh3;
	DISPID IdGetMeBz;
	DISPID IdGetLastMe;
	DISPID IdGetLastLDat;
	DISPID IdGetLastPrVk;
	DISPID IdGetEndOfRecords;
	DISPID IdGetBsd;
	DISPID IdGetBsd2;

	CCAufps& operator= (IDispatch *idpt);
	BOOL Init (IDispatch *idp);
	BOOL Failed ();

	long GetPosi ();
	double GetA ();
	HRESULT GetABz1 (char *a_bz1, int len);
	double GetAufVkPr ();
	double GetAufLadPr ();
	double GetAufLadPr0 ();
	double GetAufLadPrPrc ();
	double GetMarge ();
	short GetSaKzSint ();
	double GetAGew ();
	short GetDrFolge ();
	short GetMeEinh ();
	HRESULT GetSmt (char *smt, int len);
	HRESULT GetBasisMeBz (char *basis_me_bz, int len);
	short GetMeEinhKun ();
	short GetMeEinhKun1();
	short GetMeEinhKun2();
	short GetMeEinhKun3();
	double GetInh1 ();
	double GetInh2 ();
	double GetInh3 ();
	HRESULT GetMeBz (char *me_bz, int len);
	double GetLastMe ();
	HRESULT GetLastLDat (char *last_ldat, int len);
	double GetLastPrVk ();
	double GetBsd ();
	double GetBsd2 ();
	BOOL GetEndOfRecords ();
};

#endif // !defined(AFX_CAUFPS_H__CF46E84F_4BC8_4D40_BD37_E01421C3665B__INCLUDED_)
