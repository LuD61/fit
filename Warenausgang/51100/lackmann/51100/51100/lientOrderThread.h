// lientOrderThread.h: Schnittstelle f�r die Klasse ClientOrderThread.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LIENTORDERTHREAD_H__ECB90980_25B2_4871_B1AF_DFDB843D8A3D__INCLUDED_)
#define AFX_LIENTORDERTHREAD_H__ECB90980_25B2_4871_B1AF_DFDB843D8A3D__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Thread.h"

class CClientOrderThread : public CThread  
{
public:
	CClientOrderThread();
	virtual ~CClientOrderThread();

};

#endif // !defined(AFX_LIENTORDERTHREAD_H__ECB90980_25B2_4871_B1AF_DFDB843D8A3D__INCLUDED_)
