// DialogItem.cpp: Implementierung der Klasse CDialogItem.
//
//////////////////////////////////////////////////////////////////////

#include "DialogItem.h"
#include "dbclass.h"
#include "strfkt.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CDialogItem::CDialogItem(HWND Parent)
{
	m_Parent = Parent;
	m_Id      = 0;
	m_Ref     = NULL;
	m_RefType = String; 
	m_Length  = 0;
	strcpy (m_Format, "%.2lf");
}

CDialogItem::CDialogItem(HWND Parent, DWORD Id, void *ref, REF_TYPE RefType, int length, char *format)
{
	m_Parent  = Parent;
	m_Id      = Id;
	m_Ref     = ref;
	m_RefType = RefType; 
	m_Length  = length;
	if (format != NULL)
	{
		strcpy (m_Format, format);
	}
	else
	{
		strcpy (m_Format, "%.2lf");
	}
}

CDialogItem::~CDialogItem()
{

}

CDialogItem::operator HWND ()
{
	m_hWnd = GetDlgItem (m_Parent, m_Id);
	return m_hWnd;
}

void CDialogItem::PutValue ()
{
	char buffer[256];
	char *stringvalue;
	short shortvalue;
	long longvalue;
	double doublevalue;

	m_hWnd = GetDlgItem (m_Parent, m_Id);
	if (m_hWnd == NULL)
	{
		return;
	}
	switch (m_RefType)
	{
	case String:
		stringvalue = (char *) m_Ref;
		SetWindowText (m_hWnd, stringvalue);
		break;
	case Short:
		shortvalue = *((short *) m_Ref);
		sprintf (buffer, "%hd", shortvalue);
		SetWindowText (m_hWnd, buffer);
		break;
	case Long:
		longvalue = *((long *) m_Ref);
		sprintf (buffer, "%ld", longvalue);
		SetWindowText (m_hWnd, buffer);
		break;
	case Double:
		doublevalue = *((double *) m_Ref);
		sprintf (buffer, m_Format, doublevalue);
		SetWindowText (m_hWnd, buffer);
		break;
	case Date:
		SetDateValue (buffer);
		SetWindowText (m_hWnd, buffer);
		break;
	}

}

void CDialogItem::SetDateValue (char *buffer)
{
	long longvalue = *((long *) m_Ref);
	DB_CLASS::DLongToAsc (buffer, &longvalue);
}

void CDialogItem::GetValue ()
{
	char buffer[256];
	char *stringvalue;
	short shortvalue;
	long longvalue;
	double doublevalue;

	m_hWnd = GetDlgItem (m_Parent, m_Id);
	if (m_hWnd == NULL)
	{
		return;
	}
	GetWindowText (m_hWnd, buffer, sizeof (buffer));
	switch (m_RefType)
	{
	case String:
		if (m_Length != 0)
		{
			stringvalue = (char *) m_Ref;
			memset (stringvalue, 0, m_Length);
            strncpy (stringvalue, buffer, m_Length-1);
		}
		break;
	case Short:
		shortvalue = (short) atoi (buffer);
		*((short *) m_Ref) = shortvalue;
		break;
	case Long:
		longvalue = atol(buffer);
		*((long *) m_Ref) = longvalue;
		break;
	case Double:
		doublevalue = ratod (buffer);
		*((double *) m_Ref) = doublevalue;
		break;
	case Date:
		GetDateValue (buffer);
		break;
	}
}


void CDialogItem::GetDateValue (char *buffer)
{
	long longvalue = 0;
	DB_CLASS::DAscToLong (buffer, &longvalue);
	*((long *) m_Ref) = longvalue;
}

BOOL CDialogItem::IsControl (HWND hWnd)
{
	BOOL ret = FALSE;

	m_hWnd = GetDlgItem (m_Parent, m_Id);
	if (m_hWnd == hWnd)
	{
		ret = TRUE;
	}
	return ret;
}
