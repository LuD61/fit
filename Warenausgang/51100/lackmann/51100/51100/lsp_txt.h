#ifndef _LSP_TXT_DEF
#define _LSP_TXT_DEF

#include "dbclass.h"

struct LSP_TXT {
   short     mdn;
   short     fil;
   long      ls;
   long      posi;
   long      zei;
   char      txt[61];
};
extern struct LSP_TXT lsp_txt, lsp_txt_null;

#line 7 "lsp_txt.rh"

class LSP_TCLASS : public DB_CLASS 
{
       private :
               int del_cursor_posi;
               int del_cursor_ls;
               void prepare (void);
       public :
               LSP_TCLASS () : DB_CLASS ()
               {
                         del_cursor_posi = -1;
               }
               int dbreadfirst (void);
               int delete_lspposi (void); 
               int delete_lspls (void); 
};
#endif
