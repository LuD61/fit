#ifndef _AUFPTMP_DEF
#define _AUFPTMP_DEF

#include "dbclass.h"

struct AUFPTMP {
   long      auf;
   double    a;
   double    auf_me;
   char      auf_me_bz[7];
   double    lief_me;
   char      lief_me_bz[7];
   double    auf_vk_pr;
   double    auf_lad_pr;
   double    rab_satz;
   short     me_einh_kun;
   short     me_einh;
   short     me_einh_kun1;
   double    auf_me1;
   double    inh1;
   short     me_einh_kun2;
   double    auf_me2;
   double    inh2;
   short     me_einh_kun3;
   double    auf_me3;
   double    inh3;
   long      kun;
   short     teil_smt;
   double    a_gew;
   double    bsd_mitpr;
   double    bsd_ohnepr;
   double    stk_karton;
   double    gew_karton;
   short     hwg;
   short     wg;
   long      lieferdat;
};
extern struct AUFPTMP aufptmp, aufptmp_null;

#line 7 "aufptmp.rh"

class AUFPTMP_CLASS : public DB_CLASS 
{
       private :
               int del_cursor_aufptmp;
               void prepare (void);
       public :
               AUFPTMP_CLASS () : DB_CLASS ()
               {
                         del_cursor_aufptmp = -1;
               }
               int dbreadfirst (void);
               int delete_aufptmp (int auf);
};
#endif
