#ifndef _A_BAS_DEF
#define _A_BAS_DEF

#include "dbclass.h"

struct A_BAS {
   double    a;
   short     mdn;
   short     fil;
   char      a_bz1[25];
   char      a_bz2[25];
   double    a_gew;
   short     a_typ;
   short     a_typ2;
   short     abt;
   long      ag;
   char      best_auto[2];
   char      bsd_kz[2];
   char      cp_aufschl[2];
   short     delstatus;
   short     dr_folge;
   long      erl_kto;
   char      hbk_kz[2];
   short     hbk_ztr;
   char      hnd_gew[2];
   short     hwg;
   char      kost_kz[3];
   short     me_einh;
   char      modif[2];
   short     mwst;
   short     plak_div;
   char      stk_lst_kz[2];
   double    sw;
   short     teil_smt;
   long      we_kto;
   short     wg;
   short     zu_stoff;
   long      akv;
   long      bearb;
   char      pers_nam[9];
   double    prod_zeit;
   char      pers_rab_kz[2];
   double    gn_pkt_gbr;
   long      kost_st;
   char      sw_pr_kz[2];
   long      kost_tr;
   double    a_grund;
   long      kost_st2;
   long      we_kto2;
   long      charg_hand;
   long      intra_stat;
   char      qual_kng[5];
   char      a_bz3[25];
   short     lief_einh;
   double    inh_lief;
   long      erl_kto_1;
   long      erl_kto_2;
   long      erl_kto_3;
   long      we_kto_1;
   long      we_kto_2;
   long      we_kto_3;
   char      skto_f[2];
   double    sk_vollk;
   double    a_ersatz;
   short     a_ers_kz;
   short     me_einh_abverk;
   double    inh_abverk;
   char      hnd_gew_abverk[2];
};
extern struct A_BAS _a_bas, _a_bas_null;

#line 7 "a_basc.rh"

class A_BAS_CL : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_BAS_CL () : DB_CLASS ()
               {
               }
};
#endif
