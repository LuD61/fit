#ifndef _HWG_DEF
#define _HWG_DEF

#include "dbclass.h"

struct HWG {
   short     hwg;
   char      hwg_bz1[25];
   char      hwg_bz2[25];
   char      sp_kz[2];
   long      we_kto;
   long      erl_kto;
   char      durch_vk[2];
   char      durch_sk[2];
   char      durch_fil[2];
   char      durch_sw[2];
   char      best_auto[2];
   double    sp_vk;
   double    sp_sk;
   double    sp_fil;
   double    sw;
   short     me_einh;
   short     mwst;
   short     teil_smt;
   short     bearb_lad;
   short     bearb_fil;
   short     bearb_sk;
   char      sam_ean[2];
   char      smt[2];
   char      kost_kz[3];
   double    pers_rab;
   long      akv;
   long      bearb;
   char      pers_nam[9];
   short     delstatus;
   long      erl_kto_1;
   long      erl_kto_2;
   long      erl_kto_3;
   long      we_kto_1;
   long      we_kto_2;
   long      we_kto_3;
};
extern struct HWG hwg, hwg_null;


class HWG_CL : public DB_CLASS 
{
       private :
               void prepare (void);
                int AufEinh [21];     //LAC-9 (als Array)
				int dAktLieferdat; 
       public :
		   HWG_CL ()  : DB_CLASS ()  
               {
					AufEinh[0] = 1;
					AufEinh[1] = 1;
					AufEinh[2] = 1;
					AufEinh[3] = 1;
					AufEinh[4] = 1;
					AufEinh[5] = 1;
					AufEinh[6] = 1;
					AufEinh[7] = 1;
					AufEinh[8] = 1;
					AufEinh[9] = 1;
					AufEinh[10] = 1;
					AufEinh[11] = 1;
					AufEinh[12] = 1;
					AufEinh[13] = 1;
					AufEinh[14] = 1;
					AufEinh[15] = 1;
					AufEinh[16] = 1;
					AufEinh[17] = 1;
					AufEinh[18] = 1;
					AufEinh[19] = 1;
					AufEinh[20] = 1;
					dAktLieferdat = 0;
               }
                int GetAufEinh (void)
                {
                    return AufEinh[0];
                }
                void SetAufEinh (int einh)
                {
                    AufEinh[0] = einh;
                }
                void SetAktLieferdat (int lieferdat)
                {
                    dAktLieferdat = lieferdat;
                }

                void InitCombo (int dhwg, int idx, int typ );   //LAC-9 mit idx //LAC-156 mit typ
                void ChoiseCombo (int idx);//LAC-9 mit idx
				char*  GettxtHWG (int i);		
                int ChoiseWg (int dhwg, int dPos, int dZeile, int idx, int typ );//LAC-9 mit idx //LAC-156 mit typ  
};
#endif
