// ThreadAction.h: Schnittstelle f�r die Klasse CThreadAction.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_THREADACTION_H__CA12FB94_DFDD_4558_B8FA_BA10326E1AC3__INCLUDED_)
#define AFX_THREADACTION_H__CA12FB94_DFDD_4558_B8FA_BA10326E1AC3__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CThreadAction  
{
public:
	CThreadAction();
	virtual ~CThreadAction();

	virtual int Execute (void *param) = 0;
};

#endif // !defined(AFX_THREADACTION_H__CA12FB94_DFDD_4558_B8FA_BA10326E1AC3__INCLUDED_)
