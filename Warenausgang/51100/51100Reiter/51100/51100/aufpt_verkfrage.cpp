#include <windows.h>
#include "aufpt_verkfrage.h"

struct AUFPT_VERKFRAGE aufpt_verkfrage, aufpt_verkfrage_null;

void CAufpt_verkfrage::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &aufpt_verkfrage.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &aufpt_verkfrage.auf,  SQLLONG, 0);
    out_quest ((char *) &aufpt_verkfrage.mdn,1,0);
    out_quest ((char *) &aufpt_verkfrage.auf,2,0);
    out_quest ((char *) &aufpt_verkfrage.pos_id,2,0);
    out_quest ((char *) aufpt_verkfrage.frage1,0,257);
    out_quest ((char *) aufpt_verkfrage.antwort1,0,257);
    out_quest ((char *) aufpt_verkfrage.frage2,0,257);
    out_quest ((char *) aufpt_verkfrage.antwort2,0,257);
    out_quest ((char *) aufpt_verkfrage.frage3,0,257);
    out_quest ((char *) aufpt_verkfrage.antwort3,0,257);
    out_quest ((char *) aufpt_verkfrage.frage4,0,257);
    out_quest ((char *) aufpt_verkfrage.antwort4,0,257);
    out_quest ((char *) aufpt_verkfrage.frage5,0,257);
    out_quest ((char *) aufpt_verkfrage.antwort5,0,257);
    out_quest ((char *) aufpt_verkfrage.frage6,0,257);
    out_quest ((char *) aufpt_verkfrage.antwort6,0,257);
    out_quest ((char *) aufpt_verkfrage.frage7,0,257);
    out_quest ((char *) aufpt_verkfrage.antwort7,0,257);
    out_quest ((char *) aufpt_verkfrage.frage8,0,257);
    out_quest ((char *) aufpt_verkfrage.antwort8,0,257);
    out_quest ((char *) aufpt_verkfrage.frage9,0,257);
    out_quest ((char *) aufpt_verkfrage.antwort9,0,257);
    out_quest ((char *) aufpt_verkfrage.frage10,0,257);
    out_quest ((char *) aufpt_verkfrage.antwort10,0,257);
            cursor = sqlcursor ("select aufpt_verkfrage.mdn,  "
"aufpt_verkfrage.auf,  aufpt_verkfrage.pos_id,  "
"aufpt_verkfrage.frage1,  aufpt_verkfrage.antwort1,  "
"aufpt_verkfrage.frage2,  aufpt_verkfrage.antwort2,  "
"aufpt_verkfrage.frage3,  aufpt_verkfrage.antwort3,  "
"aufpt_verkfrage.frage4,  aufpt_verkfrage.antwort4,  "
"aufpt_verkfrage.frage5,  aufpt_verkfrage.antwort5,  "
"aufpt_verkfrage.frage6,  aufpt_verkfrage.antwort6,  "
"aufpt_verkfrage.frage7,  aufpt_verkfrage.antwort7,  "
"aufpt_verkfrage.frage8,  aufpt_verkfrage.antwort8,  "
"aufpt_verkfrage.frage9,  aufpt_verkfrage.antwort9,  "
"aufpt_verkfrage.frage10,  aufpt_verkfrage.antwort10 from aufpt_verkfrage "

#line 13 "aufpt_verkfrage.rpp"
                                  "where mdn = ? and auf = ?");
    ins_quest ((char *) &aufpt_verkfrage.mdn,1,0);
    ins_quest ((char *) &aufpt_verkfrage.auf,2,0);
    ins_quest ((char *) &aufpt_verkfrage.pos_id,2,0);
    ins_quest ((char *) aufpt_verkfrage.frage1,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort1,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage2,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort2,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage3,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort3,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage4,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort4,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage5,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort5,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage6,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort6,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage7,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort7,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage8,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort8,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage9,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort9,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage10,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort10,0,257);
            sqltext = "update aufpt_verkfrage set "
"aufpt_verkfrage.mdn = ?,  aufpt_verkfrage.auf = ?,  "
"aufpt_verkfrage.pos_id = ?,  aufpt_verkfrage.frage1 = ?,  "
"aufpt_verkfrage.antwort1 = ?,  aufpt_verkfrage.frage2 = ?,  "
"aufpt_verkfrage.antwort2 = ?,  aufpt_verkfrage.frage3 = ?,  "
"aufpt_verkfrage.antwort3 = ?,  aufpt_verkfrage.frage4 = ?,  "
"aufpt_verkfrage.antwort4 = ?,  aufpt_verkfrage.frage5 = ?,  "
"aufpt_verkfrage.antwort5 = ?,  aufpt_verkfrage.frage6 = ?,  "
"aufpt_verkfrage.antwort6 = ?,  aufpt_verkfrage.frage7 = ?,  "
"aufpt_verkfrage.antwort7 = ?,  aufpt_verkfrage.frage8 = ?,  "
"aufpt_verkfrage.antwort8 = ?,  aufpt_verkfrage.frage9 = ?,  "
"aufpt_verkfrage.antwort9 = ?,  aufpt_verkfrage.frage10 = ?,  "
"aufpt_verkfrage.antwort10 = ? "

#line 15 "aufpt_verkfrage.rpp"
                                  "where mdn = ? and auf = ? and pos_id = ?";
            sqlin ((short *)   &aufpt_verkfrage.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &aufpt_verkfrage.auf,  SQLLONG, 0);
            sqlin ((long *)   &aufpt_verkfrage.pos_id,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &aufpt_verkfrage.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &aufpt_verkfrage.auf,  SQLLONG, 0);
            sqlin ((long *)   &aufpt_verkfrage.pos_id,  SQLLONG, 0);
            test_upd_cursor = sqlcursor ("select mdn from aufpt_verkfrage "
                                  "where mdn = ? and auf = ? and pos_id = ?");
            sqlin ((short *)   &aufpt_verkfrage.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &aufpt_verkfrage.auf,  SQLLONG, 0);
            sqlin ((long *)   &aufpt_verkfrage.pos_id,  SQLLONG, 0);
            del_cursor = sqlcursor ("delete from aufpt_verkfrage "
                                  "where mdn = ? and auf = ? and pos_id = ?");
    ins_quest ((char *) &aufpt_verkfrage.mdn,1,0);
    ins_quest ((char *) &aufpt_verkfrage.auf,2,0);
    ins_quest ((char *) &aufpt_verkfrage.pos_id,2,0);
    ins_quest ((char *) aufpt_verkfrage.frage1,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort1,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage2,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort2,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage3,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort3,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage4,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort4,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage5,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort5,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage6,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort6,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage7,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort7,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage8,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort8,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage9,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort9,0,257);
    ins_quest ((char *) aufpt_verkfrage.frage10,0,257);
    ins_quest ((char *) aufpt_verkfrage.antwort10,0,257);
            ins_cursor = sqlcursor ("insert into aufpt_verkfrage ("
"mdn,  auf,  pos_id,  frage1,  antwort1,  frage2,  antwort2,  frage3,  antwort3,  frage4,  "
"antwort4,  frage5,  antwort5,  frage6,  antwort6,  frage7,  antwort7,  frage8,  "
"antwort8,  frage9,  antwort9,  frage10,  antwort10) "

#line 32 "aufpt_verkfrage.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 34 "aufpt_verkfrage.rpp"
}
