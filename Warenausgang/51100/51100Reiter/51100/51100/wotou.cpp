#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_intp.h"
#include "mo_menu.h"
#include "kun.h"
#include "mdn.h"
#include "fil.h"
#include "mdnfil.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "mo_wol.h"
#include "bmlib.h"

#define MAXLEN 40
#define MAXPOS 3000
#define LPLUS 1
 
#define IDM_FRAME     901
#define IDM_REVERSE   902
#define IDM_NOMARK    903
#define IDM_EDITMARK  904
#define IDM_PAGEVIEW  905
#define IDM_LIST      906
#define IDM_PAGE      907
#define IDM_FONT      908
#define IDM_FIND      909


int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
static int ProcessMessages(void);
void     InitFirstInstance(HANDLE);
BOOL     InitNewInstance(HANDLE, int);
void     DisplayLines ();
void     MoveButtons (void);

HANDLE  hMainInst;
HWND   hMainWindow;
HWND   mamain1;
extern HWND  ftasten;
extern HWND  ftasten2;

static int WithMenue   = 1;
static int WithToolBar = 1;

static int PageView = 0;

static char *TXT_WORK   = "Bearbeiten";
static char *TXT_SHOW   = "Anzeigen";
static char *TXT_DEL    = "L�schen";
static char *TXT_PRINT  = "Drucken";

static char *TXT_ACTIVE = TXT_WORK;


static int IDM_ACTIVE = IDM_WORK;

static PAINTSTRUCT aktpaint;

HMENU hMenu;

struct PMENUE dateimen[] = {
	                        "&1 Bearbeiten", " ",  NULL, IDM_WORK, 
                            "&2 Anzeigen",   " ",  NULL, IDM_SHOW,
						    "&3 Drucken",    " ", NULL, IDM_PRINT,
							"",              "S",   NULL, 0, 
	                        "B&eenden",      " ",      NULL, KEY5,

                             NULL, NULL, NULL, 0};
struct PMENUE bearbeiten[] = {
	                        "&Suchen",                     "G", NULL, IDM_FIND, 
                             NULL, NULL, NULL, 0};


struct PMENUE ansicht[] = {
                            "&Seitenansicht",              "G", NULL, IDM_PAGEVIEW, 
                            "&Fonteinstellung",            "G", NULL, IDM_FONT,
                             NULL, NULL, NULL, 0};

struct PMENUE menuetab[] = {"&Datei",      "M", dateimen,   0, 
                            "&Bearbeiten", "M", bearbeiten, 0, 
                            "&Ansicht",    "M", ansicht,    0, 
						     NULL, NULL, NULL, 0};

static int ActiveMark = IDM_FRAME;

static char InfoCaption [80] = {"\0"};


extern HWND hWndToolTip;
HWND hwndTB;
HWND hwndCombo1;
HWND hwndCombo2;

static TBBUTTON tbb[] =
{
 0,               IDM_WORK,   TBSTATE_ENABLED, 
                               TBSTYLE_BUTTON, 
 0, 0, 0, 0,
 1,               IDM_SHOW,   TBSTATE_ENABLED, 
	                          TBSTYLE_BUTTON,
 0, 0, 0, 0,
 2,               IDM_FIND,   TBSTATE_INDETERMINATE, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 3,               IDM_PRINT,  TBSTATE_ENABLED, 
                              TBSTYLE_BUTTON,
 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED,
                  TBSTYLE_SEP, 
 0, 0, 0, 0,
 4,               IDM_LIST,   TBSTATE_ENABLED | TBSTATE_CHECKED, 
                  TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 5,               IDM_PAGE, TBSTATE_ENABLED, 
                  TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 /*
 5,               IDM_PAGE, TBSTATE_INDETERMINATE, 
                  TBSTYLE_CHECKGROUP,
 0, 0, 0, 0,
 */
 6,               IDM_INFO,   TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 
 7,               KEYRIGHT, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 8,               KEYLEFT, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
 9,               KEYDOWN, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,
10 ,               KEYUP, TBSTATE_ENABLED, 
                  TBSTYLE_BUTTON,
 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,

 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
 0, 0,            TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0, 0, 0,
};

HWND    QuikInfo = NULL;

void     CreateQuikInfos (void);

static char *qInfo [] = {"Neue Datei �ffnen",
                         "Tablle �ffnen",
                         "Query zu Tabelle",
                         "Drucken",
                         "Funktion f�r aktives Eingabefeld",
                         "S�tze in Listenform anzeigen",
                         "Satz auf einer Seite anzeigen ",
                          0,
                          0, 
                          0};

static UINT qIdfrom [] = {IDM_WORK, IDM_SHOW, IDM_DEL,
                         IDM_PRINT, IDM_INFO,
                         IDM_LIST, IDM_PAGE,0, 0, 0};

static char *qhWndInfo [] = {"Tabellen-Linien festlegen", 
                             "Farbe f�r Vordergrund und Hintergrund "
                             "festlegen",
                             "faxen",
                             "telefonieren",
                             0};

static HWND *qhWndFrom [] = {&hwndCombo1, 
                             &hwndCombo2,
//                             &buform.mask[0].feldid,
//                             &buform.mask[1].feldid,
                             NULL}; 


static char *Combo1 [] = {"wei�e Tabelle" ,
                          "hellgraue Tabelle",
                          "schwarze Tabelle",
                          "graue Tabelle",
                          "vertikal wei�",
                          "vertikal hellgrau",
                          "vertikal schwarz", 
                          "vertikal grau",
                          "horizontal wei�",
                          "horizontal hellgrau",
                          "horizontal schwarz", 
                          "horizontal grau",
                          "keine Linien",
                           NULL};

static char *Combo2 [] = {"wei�er Hintergrund" ,
                          "blauer Hintergrund",
                          "schwarzer Hintergrund",
                          "grauer Hintergrund",
                           NULL};

static COLORREF Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL};

static COLORREF BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL};


mfont lFont = {
               NULL, 
               90, 
               100, 
               0, 
               RGB (-1, 0, 0), 
               RGB (-1, 0, 0),
               0, 0};

static int m3Size = 0;

static TEXTMETRIC tm;

static WOTLIST Liste;
static MENUE_CLASS menue_class;

static char mdn [10];
static char fil [10];

/* ****************************************************************/
/* Kopf    
                                              */
char kun_fil[3];

static int dsqlstatus;

static int testkunfil (void);

static ITEM ikunfil ("kun_fil", kun_fil, "Kunde/Filiale    :", 0); 

static field _kfform [] = {
&ikunfil,                 2, 0, 2, 1, 0, "%1d", EDIT,  0, testkunfil, 0};

static form kfform = {1, 0, 0, _kfform, 0, 0, 0, 0, NULL};



void CreateQuikInfos (void)
/**
QuikInfos generieren.
**/
{
//     BOOL bSuccess ;
     TOOLINFO ti ;

     if (QuikInfo) DestroyWindow (QuikInfo);
     QuikInfo = CreateWindow (TOOLTIPS_CLASS,
                                "",
                                WS_POPUP,
                                0, 0,
                                0, 0,
                                hMainWindow,
                                NULL,
                                hMainInst,
                                NULL);


     ZeroMemory (&ti, sizeof (TOOLINFO)) ;
     ti.cbSize = sizeof (TOOLINFO) ;
     ti.uFlags = TTF_IDISHWND | TTF_CENTERTIP | TTF_SUBCLASS ;
     ti.hwnd   = mamain1 ;
//     ti.uId    = (UINT) (HWND) buform.mask[0].feldid;
//     ti.lpszText = LPSTR_TEXTCALLBACK ;
//     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
//     ti.uId    = (UINT) (HWND) buform.mask[1].feldid;
//     ti.lpszText = LPSTR_TEXTCALLBACK ;
//     bSuccess = ToolTip_AddTool (QuikInfo, &ti) ;
}

int testkunfil (void)
{
    if (testkeys ()) return 0;
    if (atoi (kun_fil))
    {
        sprintf (kun_fil, "1");
        display_field (mamain1, &kfform.mask[0], 0, 0);
    }
    return 0;
}


int InfoKun (void)
{
//        char where [80];

/*
        if (atol (kuns))
        {
            sprintf (where, "where kun = %ld", atol (kuns));
        }
        else
        {
            sprintf (where, "where kun > 0");
        }
        _CallInfoEx (hMainWindow, NULL,
							      "kun", 
								  where, 0l);
*/
        return 0;
}


int doliste ()
{
           break_enter ();
           return 0;
}


void tst_arg (char *arg)
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                            case 'u' :
                                     strcpy (InfoCaption, arg + 1);
                                     return;
                 }
          }
          return;
}


int dokey5 ()
/**
Aktion bei Taste F5
**/
{
//        int dsqlstatus;
        
		if (IDM_ACTIVE == IDM_WORK ||
			IDM_ACTIVE == IDM_DEL)
		{
                    rollbackwork ();
		}
        break_enter ();
        DisplayAfterEnter (0);
        return 1;
}

int dokey10 (void)
{
        disp_mess ("KEY10 gedr�ckt", 2);
        return 0;
}


BOOL IsComboMsg (MSG *msg)
/**
Test, ob die Meldung fuer die Combobox ist.
**/
{
      if (HIWORD (msg->lParam) == CBN_DROPDOWN)
      {
                 opencombobox = TRUE;
                 return TRUE;
      }
      if (HIWORD (msg->lParam) == CBN_CLOSEUP)
      {
                 opencombobox = FALSE;
                 return TRUE;
      }

      if (msg->hwnd == hwndCombo1)
      {
                 return TRUE;
      }
      if (msg->hwnd == hwndTB)
      {
                 return TRUE;
      }

      return FALSE;
}

void FillCombo1 (void)
{
      int i;

      for (i = 0; Combo1[i]; i ++)
      {
           SendMessage (hwndCombo1, CB_ADDSTRING, 0,
                                   (LPARAM) Combo1 [i]);
      }
      SendMessage (hwndCombo1, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo1 [3]);
      Liste.SetLines (3);
}

void FillCombo2 (void)
{
      int i;

      for (i = 0; Combo2[i]; i ++)
      {
           SendMessage (hwndCombo2, CB_ADDSTRING, 0,
                                   (LPARAM) Combo2 [i]);
      }
      SendMessage (hwndCombo2, CB_SELECTSTRING, 0,
                                   (LPARAM) Combo2 [0]);
      SetComboMess (IsComboMsg);
}

static int StopProc (void)
{
    PostQuitMessage (0);
    return 0;
}


void DisableTB (void)
{
     ToolBar_SetState(hwndTB,IDM_PAGE, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,IDM_LIST, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,KEYLEFT,  TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,KEYRIGHT, TBSTATE_INDETERMINATE);
     ToolBar_SetState(hwndTB,IDM_FIND, TBSTATE_INDETERMINATE);
}

void EnableTB (void)
{
//     ToolBar_SetState(hwndTB,IDM_PAGE, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,IDM_LIST, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,KEYLEFT,  TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,KEYRIGHT, TBSTATE_ENABLED);
     ToolBar_SetState(hwndTB,IDM_FIND, TBSTATE_ENABLED);

     ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
     ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
}


int EnterListe ()
{
    
    EnableTB ();
    set_fkt (NULL, 4);
    EnableMenuItem (hMenu, IDM_FIND, MF_ENABLED);
    EnableMenuItem (hMenu, IDM_FONT, MF_ENABLED);

    Liste.Enter (atoi (mdn) , atoi (fil), atoi (kun_fil));

    EnableMenuItem (hMenu, IDM_FIND, MF_GRAYED);
    EnableMenuItem (hMenu, IDM_FONT, MF_GRAYED);
    DisableTB ();
    return 0;
}

/*
void MoveButtons (void)
{
     RECT rect;
     RECT rectb;
     TEXTMETRIC tm;
     HDC hdc;
     int i;
     int x, y;

     GetClientRect (mamain1, &rect); 
     hdc = GetDC (mamain1);
     GetTextMetrics (hdc, &tm);
     ReleaseDC (mamain1, hdc);

     for (i = 0; i < buform.fieldanz; i ++)
     {
         GetClientRect (buform.mask[i].feldid, &rectb );
         x = rect.right - rectb.right - 1;
         y = buform.mask[i].pos[0] * tm.tmHeight;
         MoveWindow (buform.mask[i].feldid,
                                    x, y,
                                    rectb.right,
                                    rectb.bottom,
                                    TRUE);
     }
}
*/

void MoveMamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;


     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;
     cy = frect.top - mrect.top - y - 50;
     MoveWindow (mamain1, x, y, cx, cy, TRUE);
//     MoveButtons ();
}


void Createmamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;


     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;
     cy = frect.top - mrect.top - y - 50;

     mamain1 = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "hStdWindow",
                              InfoCaption,
                              WS_CHILD |  
                              WS_VISIBLE,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
}


void Enter (void)
/**
Auftrag bearbeiten.
**/
{
       extern short sql_mode; 

//       display_form (mamain1, &aufformkt, 0, 0);


       while (TRUE)
       {
                set_fkt (doliste, 6);
                SetFkt (6, liste, KEY6);
                break_end ();
                DisplayAfterEnter (TRUE);
                enter_form (mamain1, &kfform, 0, 0);
                if (syskey == KEY5 || syskey == KEYESC) break;

                EnterListe ();
                set_fkt (dokey5, 5);
                set_fkt (NULL, 6);
                set_fkt (NULL, 7);
                SetFkt (6, leer, 0);
                SetFkt (7, leer, 0);
        }
        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        SetFkt (6, leer, 0);
        SetFkt (7, leer, 0);
        return;
}


void DisableMe (void)
{
     ToolBar_SetState(hwndTB,IDM_WORK, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_WORK,  MF_GRAYED);

     ToolBar_SetState(hwndTB,IDM_SHOW, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_SHOW,  MF_GRAYED);

     ToolBar_SetState(hwndTB,IDM_PRINT, TBSTATE_INDETERMINATE);
     EnableMenuItem (hMenu,  IDM_PRINT,  MF_GRAYED);

}

void EnableMe (void)
{

     ToolBar_SetState(hwndTB, IDM_WORK, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_WORK, MF_ENABLED);

     ToolBar_SetState(hwndTB, IDM_SHOW, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_SHOW, MF_ENABLED);

     ToolBar_SetState(hwndTB, IDM_PRINT, TBSTATE_ENABLED);
     EnableMenuItem (hMenu,      IDM_PRINT, MF_ENABLED);

}


void ShowBm (void)
{
    RECT rect;
    TEXTMETRIC tm;
    int x, y, cx, cy;
    static int abmp = 0;
    char buffer [80];

    abmp ++;
    if (abmp > 3) abmp = 1;
    GetWindowRect (hMainWindow, &rect);
    stdfont ();
    SetTmFont (hMainWindow, &tm);

    cx = 5;
    cy = 2;
    x = (rect.right - 2) / tm.tmAveCharWidth;
    x -= cx;
    y = rect.top;
    y = (int) (double) ((double) y / 
                                (double)(1 + (double) 1/3));
    y /= tm.tmHeight;
    y += 3;
    sprintf (buffer, "-nm2 -z%d -s%d -h%d -w%d "
             "c:\\user\\fit\\bilder\\artikel\\%d.bmp",
             y, x, cy, cx, abmp);
    _ShowBm (buffer);
}


void EingabeMdnFil (void)
{
       extern MDNFIL cmdnfil;

       DisableMe ();
       cmdnfil.SetFilialAttr (REMOVED);
       cmdnfil.SetNoMdnNull (TRUE);
	   while (TRUE)
       {
           set_fkt (dokey5, 5);
		   if (cmdnfil.eingabemdnfil (mamain1, mdn, fil) == -1)
           {
			         break;
           }
           Enter  ();
	   }
       cmdnfil.CloseForm ();
       EnableMe ();
}

static HWND chwnd;
static int mench = 0;

static int lstcancel (void)
{
   
    syskey = KEY5;
    break_enter ();
    return 0;
}


static int lstok (void)
{
    syskey = KEY12;
    mench = currentfield;
    break_enter ();
    return 0;
}


static int testwork (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    mench = 0;
    break_enter ();
    return 0;
}

static int testshow (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    mench = 1;
    break_enter ();
    return 0;
}

static int testprint (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    mench = 2;
    break_enter ();
    return 0;
}

static int testcancel (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    mench = 3;
    break_enter ();
    return 0;
}

void StartMenu (void)
/**
Liste auswaehlen und Listengenerator starten
**/
{
        form *sform;
        int  sfield;

        static ColButton LstChoise = {
                              "Auswahl", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               RGB (0,0,120),
                               -2};
   
        static ColButton cWork = {
                              "Bearbeiten", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cShow = {
                              "Anzeigen", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cPrint = {
                              "Drucken", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ITEM iLstChoise ("", (char *) &LstChoise,  "", 0);
        static ITEM iWork      ("", (char *) &cWork,      "", 0);
        static ITEM iShow       ("", (char *)&cShow,      "", 0);
        static ITEM iPrint      ("", (char *)&cPrint,     "", 0);
        static ITEM vOK     ("",  "    OK     ",  "", 0);
        static ITEM vCancel ("",  " Abbrechen ", "", 0);

        static field _fLstChoise[] = {
          &iLstChoise, 35, 0,  0, 0, 0, "", COLBUTTON, 0, 0, 0};

        static form fLstChoise = {1, 0, 0, _fLstChoise, 
                                  0, 0, 0, 0, NULL};    


        static field _clist [] = {
&iWork,     34, 2, 2, 1, 0, "", COLBUTTON,            0, testwork, 
                                                           KEY12 , 
&iShow,     34, 2, 4, 1, 0, "", COLBUTTON,            0, testshow, 
                                                           KEY12,
&iPrint,    34, 2, 6, 1, 0, "", COLBUTTON,            0, testprint, 
                                                           KEY12,
&vCancel,  12, 0, 8,12, 0, "",  BUTTON,               0, testcancel, 
                                                           KEY5, 
};

        static form clist = {4, 0, 0, _clist, 0, 0, 0, 0, NULL}; 
      

     while (TRUE)
     {
        sform = current_form;
        sfield = currentfield;
        save_fkt (5);
        save_fkt (12);

        set_fkt (lstcancel, 5);
        set_fkt (lstok, 12);

        no_break_end ();
        SetButtonTab (TRUE);
        DisablehWnd (hMainWindow);
        SetBorder (WS_POPUP | WS_DLGFRAME);

        SetAktivWindow (mamain1);
        chwnd = OpenWindowCh (10, 36, 10, 22, hMainInst);

        display_form (chwnd, &fLstChoise, 0, 0);
        currentfield = 0;
        enter_form (chwnd, &clist, 0, 0);
        CloseControls (&fLstChoise);
        CloseControls (&clist);
        SetAktivWindow (mamain1);
        DestroyWindow (chwnd);
        chwnd = NULL;
        SetButtonTab (FALSE);
        restore_fkt (5);
        restore_fkt (12);
        current_form = sform;
        currentfield = sfield;
        SetCurrentFocus (currentfield);

        if (syskey == KEY5) return;
        if (syskey == KEYESC) return;

        switch (mench)
        {
              case 0 :
                     EingabeMdnFil ();
                     break;
              case 3:
                     return;
        }
     }
}


int    PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
       int anz; 
       int i;
       char argtab[20][80];
       char *varargs[20];

       opendbase ("bws");
       menue_class.SetSysBen ();

       anz = wsplit (lpszCmdLine, " ");
       for (i = 0; i < anz; i ++)
       {
            strcpy (argtab[i], wort[i]);
            varargs[i] = (char *) argtab[i];
       }
       argtst (&anz, varargs, tst_arg);

       SetStdProc (WndProc);
       InitFirstInstance (hInstance);
       InitNewInstance (hInstance, nCmdShow);


       if (WithMenue)
       {
	               hMenu = MakeMenue (menuetab);
	               SetMenu (hMainWindow, hMenu);
       }

       if (WithToolBar)
       {
	               hwndTB = MakeToolBarEx (hInstance, 
		                       hMainWindow,tbb, 53,
                               qInfo, qIdfrom,
		                       qhWndInfo, qhWndFrom);
                   hwndCombo1 = MakeToolBarCombo (hInstance,
                                      hMainWindow,
                                      hwndTB, 
                                      11, 
                                      13, 
                                      31);

                   FillCombo1 ();
                   ComboToolTip (hwndTB, hwndCombo1);
 
                   hwndCombo2 = MakeToolBarCombo (hInstance,
                                                  hMainWindow,
                                                  hwndTB, 
                                                  12, 
                                                  32, 
                                                  52);

                   FillCombo2 ();
                   ComboToolTip (hwndTB, hwndCombo2);


                   Liste.SethwndTB (hwndTB);
                   DisableTB ();
       }

       ftasten = OpenFktM (hInstance);
       Createmamain1 ();


       SetEnvFont ();
       Liste.SetListFont (&lFont);

//       CreateQuikInfos ();


       CheckMenuItem (hMenu, IDM_PAGEVIEW, MF_UNCHECKED); 
       ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
       ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);

       Liste.SethMainWindow (mamain1);
       EingabeMdnFil ();
 
//       StartMenu ();

       return 0;
}


void InitFirstInstance(HANDLE hInstance)
{
        
        WNDCLASS wc;
    

        wc.style         =  CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "FITICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "hStdWindow";

        RegisterClass(&wc);

        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "ListMain";
        RegisterClass(&wc);

        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hListWindow";
        RegisterClass(&wc);

        hMainInst = hInstance;
        return;
}


BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        TEXTMETRIC tm;
        HDC hdc;
        HFONT hFont, oldFont;
        char *Caption;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (0, hdc);
        Liste.SetTextMetric (&tm);
        if (InfoCaption[0])
        {
            Caption = InfoCaption;
        }
        else
        {
            Caption = "Tourenplanung";
        }

        SetBorder (WS_THICKFRAME | WS_CAPTION | 
                   WS_SYSMENU | WS_MINIMIZEBOX |
                   WS_MAXIMIZEBOX);

        hMainWindow = OpenWindowChC (26, 80, 2, 0, hInstance, 
                      Caption);

        return 0; 
}


static int ProcessMessages(void)
{
        MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                  syskey = KEY5;
                                  PostQuitMessage (0);
                                  continue; ;
                              case VK_RETURN :
                                  syskey = KEYCR;
                                  EingabeMdnFil ();
                                  continue; ;
                       }
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
        }
        return msg.wParam;
}

void InvalidateLines (void)
{
         RECT rect;
         static TEXTMETRIC tm;
         HDC hdc;

         return;

         hdc = GetDC (hMainWindow);
         GetTextMetrics (hdc, &tm);
         ReleaseDC (hMainWindow, hdc);
         GetClientRect (hMainWindow, &rect);

         rect.top = 2 * tm.tmHeight;
         rect.top = rect.top + rect.top / 3;
         rect.top -= 14;
         rect.bottom = rect.top + 10;
         InvalidateRect (hMainWindow, &rect, TRUE);
}


void PrintLines (HDC hdc)
/**
Linien am Bildschirm anzeigen.
**/
{
         static TEXTMETRIC tm;
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         static HPEN hPenB = NULL;
         RECT rect;
         int x, y;

         GetTextMetrics (hdc, &tm);
         y = 2 * tm.tmHeight;
         y = y + y / 3;
		 y -= 14;
         GetClientRect (mamain1, &rect);
         x = rect.right - 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
                   hPenB = CreatePen (PS_SOLID, 0, BLACKCOL);
         }

/* Linie oben                 */

         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

/* Linie unten                */
         
		 y += 3 * tm.tmHeight;
         SelectObject (hdc, hPenG);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);

         y ++;
         SelectObject (hdc, hPenW);
         MoveToEx (hdc, 0, y, NULL);
         LineTo (hdc, x + 2, y);
}

void DisplayLines ()
{
         HDC hdc;

         hdc = GetDC (mamain1);
         PrintLines (hdc);
         ReleaseDC (mamain1, hdc);
}

void ChoiseCombo1 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo1,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo1[i]; i ++)
          {
              if (strcmp (Combo1[i],text) == 0)
              {
                   Liste.SetListLines (i);
                   break;
              }
          }
}

void ChoiseCombo2 (void)
/**
Aktuell gewaehlten eintrag in Combobox 1 ermitteln.
**/
{
          int i;
          char text [80];

          GetWindowText (hwndCombo2,
                         text,
                         79);
          clipped (text);
          for (i = 0; Combo2[i]; i ++)
          {
              if (strcmp (Combo2[i],text) == 0)
              {
                   Liste.SetColors (Colors[i], BkColors[i]);
                   break;
              }
          }
}


LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
//		char where [256];

        switch(msg)
        {
              case WM_PAINT :
                      if (hWnd == mamain1)
                      {
                              hdc = BeginPaint (hWnd, &aktpaint);
                              PrintLines (hdc);
                              EndPaint (hWnd, &aktpaint);
                      }
                      else 
                      {
                              Liste.OnPaint (hWnd, msg, wParam, lParam);
                      }
                      break;
              case WM_MOVE :
                      if (hWnd == hMainWindow)
                      {
                              Liste.MoveMamain1 ();
                      }
                      break;
              case WM_SIZE :
                      if (hWnd == hMainWindow)
                      {
                              MoveFkt ();
                              MoveMamain1 ();  
                              hdc = GetDC (hMainWindow);
                              ReleaseDC (hMainWindow, hdc);
                      }
                      else if (hWnd == mamain1)
                      {
                              Liste.MoveMamain1 ();
                      }
                      else if (hWnd == Liste.GetMamain1 ())
                      {
                              Liste.MoveListWindow ();
                              hdc = GetDC (hMainWindow);
                              ReleaseDC (hMainWindow, hdc);
                      }
                      break;

              case WM_SYSCOMMAND:
                      if (hWnd == Liste.GetMamain1 ()
                          && wParam == SC_MINIMIZE)
                      {
                                   Liste.SetMin ();

                      }
                      else if (hWnd == Liste.GetMamain1 ()
                               && wParam == SC_MAXIMIZE)
                      {
                                   Liste.SetMax ();
/*
                                   ShowWindow (Liste.GetMamain1 (),
                                       SW_SHOWMAXIMIZED);
*/
                                   Liste.MaximizeMamain1 ();

                                   return 0;

                      }
                      else if (hWnd == Liste.GetMamain1 ()
                               && wParam == SC_RESTORE)
                      {
                                   Liste.InitMax ();
                                   Liste.InitMin ();
                                   ShowWindow (Liste.GetMamain1 (), 
                                               SW_SHOWNORMAL);
                                   Liste.MoveMamain1 ();
                                   return 0;
                      }
                      else if (hWnd == Liste.GetMamain1 ()
                               && wParam == SC_CLOSE)
                      {
                                   if (Liste.IsListAktiv ())
                                   {
                                           syskey = KEY5;
                                           SendKey (VK_F5);
                                   }
                                   return 0;
                      }
                      else if (hWnd == hMainWindow && wParam == SC_CLOSE)
                      {
                                   if (abfragejn (hMainWindow, 
                                                  "Bearbeitung abbrechen ?",
                                                  "N") == 0)
                                   {
                                           return 0;
                                   }
                                   rollbackwork ();
                                   ExitProcess (0);
                                   break;
                                   
                      }
                      break;

              case WM_HSCROLL :
                       Liste.OnHScroll (hWnd, msg,wParam, lParam);
                       break;
                      
              case WM_VSCROLL :
                       Liste.OnVScroll (hWnd, msg, wParam, lParam);
                       break;

              case WM_NOTIFY :
                    {
                      LPNMHDR pnmh = (LPNMHDR) lParam;

                      if (pnmh->code == TTN_NEEDTEXT)
                      {
                           LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
                           if (QuickCpy (lpttt->szText, lpttt->hdr.idFrom)
                               == FALSE)
                           {
                                    QuickHwndCpy (lpttt);
                           }
                      }
                      break;
                    }

              case WM_DESTROY :
				      OnDestroy (hWnd); 
                      if (hWnd == hMainWindow)
                      {
                             PostQuitMessage (0);
                             return 0;
                      }
                      break;
              case WM_KEYDOWN :
                      if (Liste.GetMamain1 ())
                      {
                               Liste.FunkKeys (wParam, lParam);
                      }
                      break;
                  
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEYESC)
                    {
                            syskey = KEYESC;
                            SendKey (VK_ESCAPE);
                            break;
                    }
                    if (LOWORD (wParam) == KEY2)
                    {
                            syskey = KEY2;
                            SendKey (VK_F2);
                            break;
                    }
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYDOWN)
                    {
                            syskey = KEYDOWN;
                            SendKey (VK_DOWN);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYUP)
                    {
                            syskey = KEYUP;
                            SendKey (VK_UP);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYLEFT)
                    {
                            syskey = KEYLEFT;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYRIGHT)
                    {
                            syskey = KEYRIGHT;
                            SendKey (VK_RIGHT);
                            break;
                    }
				   else if (LOWORD (wParam) == IDM_FRAME)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_FRAME,
								           MF_CHECKED); 
							ActiveMark = IDM_FRAME;
                            syskey = KEY3;
                            SendKey (VK_F3);
                   }
				   else if (LOWORD (wParam) == IDM_REVERSE)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_REVERSE,
								           MF_CHECKED); 
							ActiveMark = IDM_REVERSE;
                            syskey = KEY4;
                            SendKey (VK_F4);
                   }
				   else if (LOWORD (wParam) == IDM_NOMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_NOMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_NOMARK;
                            syskey = KEY6;
                            SendKey (VK_F6);
                   }
				   else if (LOWORD (wParam) == IDM_EDITMARK)
                   {
						    CheckMenuItem (hMenu, ActiveMark,
								           MF_UNCHECKED); 
						    CheckMenuItem (hMenu, IDM_EDITMARK,
								           MF_CHECKED); 
							ActiveMark = IDM_EDITMARK;
                            syskey = KEY7;
                            SendKey (VK_F7);
                   }
				   else if (LOWORD (wParam) == IDM_PAGEVIEW)
                   {
                            if (Liste.GetRecanz () < 1) break;
                            if (PageView)
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                                    ToolBar_SetState(hwndTB, IDM_PAGE, TBSTATE_ENABLED);
                                    PageView = 0;
                                    Liste.SwitchPage0 (Liste.GetAktRowS ());
                            }
                            else
                            {
						            CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                                    ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                                    ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                                    ToolBar_SetState(hwndTB, IDM_LIST, TBSTATE_ENABLED);
                                    PageView = 1;
                                    Liste.SwitchPage0 (Liste.GetAktRow ());
                            }
                            SendMessage (Liste.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (Liste.Getmamain3 (), 0, TRUE);
                   }
 				   else if (LOWORD (wParam) == IDM_PAGE)
                   {
                            PageView = 1; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_CHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_PAGE, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_LIST, FALSE);
                            Liste.SwitchPage0 (Liste.GetAktRow ());
                            SendMessage (Liste.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (Liste.Getmamain3 (), 0, TRUE);
                   }
 				   else if (LOWORD (wParam) == IDM_LIST)
                   {
                            PageView = 0; 
			                CheckMenuItem (hMenu, IDM_PAGEVIEW,
								                   MF_UNCHECKED); 
                            ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);
                            ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
                            Liste.SwitchPage0 (Liste.GetAktRowS ());
                            SendMessage (Liste.Getmamain2 (), WM_SIZE, NULL, NULL);
                            InvalidateRect (Liste.Getmamain3 (), 0, TRUE);
                   }
			       else if (LOWORD (wParam) == IDM_WORK)
                   {
                             EingabeMdnFil ();
                   }
			       else if (LOWORD (wParam) == IDM_PRINT)
                   {
                   }

                   if (HIWORD (wParam) == CBN_CLOSEUP)
                   {
                                if (lParam == (LPARAM) hwndCombo1)
                                {
                                   ChoiseCombo1 ();
                                }
                                if (lParam == (LPARAM) hwndCombo2)
                                {
                                   ChoiseCombo2 ();
                                }
                                if (Liste.Getmamain3 ())
                                {
//                                    SetFocus (Liste.Getmamain3 ());
                                      Liste.SetListFocus ();
                                }
                                else
                                {
                                    SetCurrentFocus (currentfield);
                                }
                    }
					else if (LOWORD (wParam) == IDM_FONT)
                    {
                                  Liste.ChoiseFont (&lFont);
//                                  Liste.SetFont (&lFont);
                                  return 0;
                                   
                    }
					else if (LOWORD (wParam) == IDM_FIND)
                    {
                                  Liste.FindString ();
                                  return 0;
                                   
                    }
                    else if (LOWORD (wParam) == IDM_INFO)
                    {
                                  syskey = KEY4;
                                  SendKey (VK_F4);
                    }
                    else if (LOWORD (wParam) == IDM_TEXT)
                    {
                    }
                    else if (LOWORD (wParam) == IDM_ETI)
                    {
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

