#ifndef _SEARCHSTDFILK_DEF
#define _SEARCHSTDFILK_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

struct SSTDAUFK
{
      char wo_tag [5];
      char wo_tag_bez [40];
	  char lfd [9];
	  char bez [40];
};

class SEARCHSTDFILK
{
       private :
           static ITEM uwo_tag;
           static ITEM ulfd;
           static ITEM ubez;
           static ITEM iwo_tag;
           static ITEM ilfd;
           static ITEM ibez;
           static ITEM iline;

           static field _UbForm[];
           static form UbForm;

           static field _DataForm[];
           static form DataForm;

           static field _LineForm[];
           static form LineForm;

           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SSTDAUFK *skuntab;
           static struct SSTDAUFK skun;
           static int idx;
           static long kunanz;
           static CHQEX *Query;
           static int SearchField;
           static char *query;
           static int sowo_tag; 
           static int solfd; 
           static int sobez; 
           static short mdn;
           static short fil;


           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHSTDFILK ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  mdn = 0;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHSTDFILK ()
           {
                  if (skuntab != NULL)
                  {
                      delete skuntab;
                      skuntab = NULL;
                  }
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void SetQuery (char *q)
           {
               if (query == NULL || (strcmp (query, q) != 0))
               {
                      query = q;
                      if (Query != NULL)
                      {
                             delete Query;
                      }
                      if (skuntab != NULL)
                      {
                             delete skuntab;
                      }
               }   
           }

           char *GetQuery (void)
           {
                return query;
           }

           SSTDAUFK *GetSkun (void)
           {
               if (idx == -1) return NULL;
               return &skun;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           SSTDAUFK *GetNextSkun (void);
           SSTDAUFK *GetPriorSkun (void);
           SSTDAUFK *GetFirstSkun (void);
           SSTDAUFK *GetLastSkun (void);
           static void FillFormat (char *, int);
           static void FillVlines (char *, int);
           static void FillCaption (char *, int);
           static void FillRec (char *, int, int);
           static int sortwo_tag (const void *, const void *);
           static void SortWo_tag (HWND);
           static int sortlfd (const void *, const void *);
           static void SortLfd (HWND);
           static int sortbez (const void *, const void *);
           static void SortBez (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int Read (char *);
           int TestRec (short, short);
           void SetParams (HINSTANCE, HWND);
           void Search (short, short, char *);
};  
#endif