// Mutex.h: Schnittstelle f�r die Klasse CMutex.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUTEX_H__B01A79A1_9E1E_48A2_B1D2_8ABA3301B7F3__INCLUDED_)
#define AFX_MUTEX_H__B01A79A1_9E1E_48A2_B1D2_8ABA3301B7F3__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <Windows.h>

#define MUTEX_ERROR		(int)-1
#define MUTEX_SUCCESS	(int) 0
#define MUTEX_TIMEOUT	(int) 1

class CMutex  
{
private:
	HANDLE m_hMutex;                        // object name

public:
	CMutex();
	CMutex(LPCSTR Name);
	virtual ~CMutex();
	int WaitForSingleObject(long Timeout);
	bool Release();
};

#endif // !defined(AFX_MUTEX_H__B01A79A1_9E1E_48A2_B1D2_8ABA3301B7F3__INCLUDED_)
