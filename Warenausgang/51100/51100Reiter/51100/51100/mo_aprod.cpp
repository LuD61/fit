#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "wmaskc.h"
#include "colbut.h"
#include "dbclass.h"
#include "cmask.h"
#include "mo_tast.h"
#include "lbox.h"

#define BuOk 1001
#define ABTBOX 991
#define ARTIKELBOX 992

extern BOOL ColBorder;

char *vpos = "              1";


static mfont mFont = {
                     "MS Sans Serif", 
                      100, 
                      0, 
                      0, 
                      BLACKCOL, 
                      LTGRAYCOL,
                      0, 0};
char Tast[50][7];

ColButton CTast   = {"0",  -1, -1, 
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
//                     BLACKCOL,
//                     GRAYCOL,
                     RGB (0, 255, 255),
                     BLUECOL,
                     2};
ColButton **CTasten;


static CFIELD **_fGeraet;
static CFORM *fGeraet; 

static CFIELD **_fTast;
static CFORM *fTast; 

static CFIELD **_fTasten;
static CFORM *fTasten; 

static CFIELD **_fWork;
static CFORM *fWork;

static CFIELD **_fListAbt;
static CFORM *fListAbt;

static CFIELD **_fListArt;
static CFORM *fListArt;

static char art [14] = {"1000"};

EINGABE::EINGABE () : DB_CLASS ()
{
	        int i, j;
			int x, y, cx, cy;
			TEXTMETRIC tm;
			HFONT hFont, oldfont;
			HDC hdc;

 	        this->Font = &mFont;  

			hdc = GetDC (NULL);
            hFont = SetDeviceFont (hdc, Font, &tm);
            oldfont = SelectObject (hdc, hFont);
	        GetTextMetrics (hdc, &tm);
			DeleteObject (SelectObject (hdc, oldfont));
	
            _fGeraet   = new CFIELD * [3];
 		    _fGeraet[0] = new CFIELD ("Capt", "Abteilung oder Ger�t",
				                     0, 4, 2, 1, NULL, "", CCAPTBORDER,
												 900, Font, 0, TRANSPARENT);
			_fGeraet[1] = new CFIELD ("Abt", "Abteilung",
							         12, 0, 3, 2, NULL, "", CBUTTON,
									 901, Font, 0, BS_AUTORADIOBUTTON);
			_fGeraet[2] = new CFIELD ("Ger", "Ger�t",
				                     12, 0, 3, 3, NULL, "", CBUTTON,
												 902, Font, 0, BS_AUTORADIOBUTTON);
			fGeraet = new CFORM (3, _fGeraet);


            _fTast      = new CFIELD * [3];
 		    _fTast[0] = new CFIELD ("Capt", "Tastenansicht",
				                     0, 4, 25, 1, NULL, "", CCAPTBORDER,
												 903, Font, 0, TRANSPARENT);
			_fTast[1] = new CFIELD ("Abt", "Normal",
							         12, 0, 26, 2, NULL, "", CBUTTON,
									 904, Font, 0, BS_AUTORADIOBUTTON);
			_fTast[2] = new CFIELD ("Ger", "Shift",
				                     12, 0, 26, 3, NULL, "", CBUTTON,
									 905, Font, 0, BS_AUTORADIOBUTTON);
			fTast = new CFORM (3, _fTast);

            _fTasten  = new CFIELD * [50];
			CTasten   = new ColButton * [50];

			x = 45 * tm.tmAveCharWidth;
			y = (int) (double) ((double) 1.3 * tm.tmHeight);
			cx = tm.tmAveCharWidth * 5 + 5;
			cy = 2 * tm.tmHeight;
/*
			j = 0;
			for (i = 0; i < 46; i ++)
			{
				_fTasten[i] = new CFIELD ("Bu", "0",
					                     cx, 0, x, y, NULL, "", CBUTTON,
										 910 + i, Font, TRUE, 0);
				x += cx + 5;
				j ++;
				if (j == 10)
				{
					j = 0;
					x = 45 * tm.tmAveCharWidth;
					y = (int) (double) ((double) y + tm.tmHeight * 1.3 + 5);
				}
			}
*/
			j = 0;
			for (i = 0; i < 46; i ++)
			{

				CTasten[i]  = new ColButton; 
				memcpy (CTasten[i], &CTast, sizeof (ColButton));
				_fTasten[i] = new CFIELD ("Bu", (ColButton *) CTasten[i],
					                     cx, cy, x, y, NULL, "", CCOLBUTTON,
										 910 + i, Font, TRUE, 0);
				x += cx + 5;
				j ++;
				if (j == 10)
				{
					j = 0;
					x = 45 * tm.tmAveCharWidth;
					y = (int) (double) ((double) y + tm.tmHeight * 2 + 5);
				}
			}
			fTasten = new CFORM (46, _fTasten);


            _fListAbt    = new CFIELD * [1];
			_fListAbt[0] = new CFIELD ("Abteilung", "Abteilung",
				                     30, 6, 2, 6, NULL, "", CLISTBOX,
												 ABTBOX, Font, 0, 0);

			fListAbt     = new CFORM (1, _fListAbt);

            _fListArt    = new CFIELD * [1];
			_fListArt[0] = new CFIELD ("Artikel", "Artikel",
				                     40, 10, 2, 12, NULL, "", CLISTBOX,
												 ARTIKELBOX, Font, 0, 0);

			fListArt     = new CFORM (1, _fListArt);


            _fWork    = new CFIELD * [3];
			_fWork[0] = new CFIELD ("Geraet", fGeraet, 0, 0, 0, 0, NULL, "",   CFFORM,
				                                 906, Font, 0, 0);
			_fWork[1] = new CFIELD ("Tast", fTast, 0, 0, 0, 0, NULL, "",       CFFORM,
				                                 907, Font, 0, 0);
			_fWork[2] = new CFIELD ("Tasten", fTasten, 0, 0, 0, 0, NULL, "",   CFFORM,
				                                 908, Font, 0, 0);
			_fWork[3] = new CFIELD ("AbtForm", fListAbt, 0, 0, 0, 0, NULL, "", CFFORM,
				                                 980, Font, 0, 0);
			_fWork[4] = new CFIELD ("art_text", "Ausgew�hlter Artikel", 
				                                         0, 0, 2, 11, NULL, "", CDISPLAYONLY,
                                                  981, Font, 0, 0);
			_fWork[5] = new CFIELD ("art_a",  art, 
				                                         14, 0, 24, 11, NULL, "%13.0f", CREADONLY,
                                                  981, Font, 0, 0);
			_fWork[6] = new CFIELD ("ArtForm", fListArt, 0, 0, 0, 0, NULL,  "", CFFORM,
				                                 982, Font, 0, 0);

			fWork   = new CFORM (7, _fWork);
			ColBorder = TRUE;

}

void EINGABE::SetCurrentID (DWORD ID)
{
	       if (fWork->SetCurrentID (ID))
		   {
			   currentfield = ID;
		   }
}

void EINGABE::SetCurrentName (char *name)
{
	       if (fWork->SetCurrentName (name))
		   {
			   currentfield = fWork->GetID (name);
		   }
}

void EINGABE::EnableID (DWORD ID, BOOL flag)
{
	       fWork->EnableID (ID, flag);
}

void EINGABE::EnableName (char *name, BOOL flag)
{
	       fWork->EnableName (name, flag);
}

void EINGABE::CheckID (DWORD ID, BOOL flag)
{
	       fWork->CheckID (ID, flag);
}

void EINGABE::CheckName (char *name, BOOL flag)
{
	       fWork->CheckName (name, flag);
}

void EINGABE::SetArtikel (HWND hWnd) 
{
	       int idx;
		   int current;
		   char text [512];
		   int anz;

		   idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
		   current = fTasten->GetCurrent ();
		   SendMessage (hWnd, LB_GETTEXT, idx, (LPARAM) text);
		   anz = wsplit (text, " ");
		   if (anz == 0) return;
		   strcpy (Tast[current], wort[0]);
		   fTasten->SetFeld (Tast[current], current);
		   fTasten->Update (current);
		   current ++;
		   if (current == fTasten->GetFieldanz ()) current = 0;
//		   fWork->SetCurrent (2);
		   fTasten->SetCurrent (current);
//		   fTasten->GetCfield () [current]->SetFocus ();
		   SendMessage (fTasten->GetCfield () [current]->GethWnd (),
			            WM_DEF, 0, 0l);
}
	       

void EINGABE::SetAChoise (HWND hWnd) 
{
	       int idx;
		   char text [512];
		   int anz;

		   idx = SendMessage (hWnd, LB_GETCURSEL, 0, 0l);
		   SendMessage (hWnd, LB_GETTEXT, idx, (LPARAM) text);
		   anz = wsplit (text, " ");
		   if (anz == 0) return;
		   strcpy (art, wort[0]);
		   fWork->Update (5);
}
	       

void EINGABE::ProcessMessages (void)
{
	      MSG msg;

          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
			      if (msg.wParam == VK_TAB)
				  {

                      if (GetKeyState (VK_SHIFT) < 0)
                      {
                              syskey = KEYSTAB;
                              fWork->PriorFormField (); 
					  }
					  else
					  {
					          syskey = KEYTAB;
                              fWork->NextFormField (); 
					  }
					  continue;
				  }
				  else if (msg.wParam == VK_F5)
				  {
					  break;
				  }
				  else if (msg.hwnd == fWork->GethWndName ("Abteilung"));
				  else if (msg.hwnd == fWork->GethWndName ("Artikel"));
				  else if (msg.wParam == VK_DOWN)
				  {

					  syskey = KEYDOWN;
                      fWork->NextField (); 
					  continue;
				  }
				  else if (msg.wParam == VK_UP)
				  {

					  syskey = KEYUP;
                      fWork->PriorField (); 
					  continue;
				  }
			  }
              TranslateMessage(&msg);
			  DispatchMessage(&msg);
		  }
}


DWORD EINGABE::IsTast (DWORD ID)
{

	      if (fTasten->GethWndID (ID))
		  {
			  return ID;
		  }
		  return NULL;
}

void EINGABE::SetCurrentTast (DWORD ID)
{
	    fTasten->SetCurrentFieldID (ID);
	    fWork->SetCurrent (2);
	    return;
}


CALLBACK EINGABE::CProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;
		DWORD Id;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					fWork->display (hWnd, hdc);
                    EndPaint (hWnd, &ps);
					break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == BuOk)
                    {
                           PostQuitMessage (0);
                    }
                    else if (LOWORD (wParam) == ABTBOX)
                    {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
						          disp_mess ("Meldung von ABTBOX mit DoppelKlick", 2); 
						   }
                           if (HIWORD (wParam) == VK_RETURN)
                           {
						          disp_mess ("Meldung von ABTBOX mit Enter", 2); 
						   }
                           if (HIWORD (wParam) == LBN_SELCHANGE)
                           {
		                          fWork->SetCurrent (3);
						   }
                    }
                    else if (LOWORD (wParam) == ARTIKELBOX)
                    {
                           if (HIWORD (wParam) == LBN_DBLCLK)
                           {
						          SetArtikel ((HWND) lParam); 
						   }
                           if (HIWORD (wParam) == VK_RETURN)
                           {
						          SetArtikel ((HWND) lParam); 
						   }
                           if (HIWORD (wParam) == LBN_SELCHANGE)
                           {
							      SetAChoise ((HWND) lParam);
		                          fWork->SetCurrent (4);
						   }
                    }
					else if (Id = IsTast (LOWORD (wParam)))
					{
						   if (HIWORD (wParam) == WM_SETFOCUS)
						   {
                                  SetCurrentTast (Id);
						   }
						   else if (HIWORD (wParam) == WM_KILLFOCUS)
						   {       
	                              SendMessage ((HWND) lParam, WM_DEF, 0, 0l);
						   }
					}
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void EINGABE::FillAbtBox (void)
{
	       HWND hWnd;
		   char buffer [80];

		   hWnd = fWork->GethWndName ("Abteilung");
		   sprintf (buffer, " %-13s %s", "Abteilung", "Bezeichnung");
           SendMessage (hWnd, LB_TITLE, -1,  
                                 (LPARAM) (char *) buffer);

		   sprintf (buffer, " %-13.0lf %s", (double) 1, "Wurst");
           SendMessage (hWnd, LB_INSERTSTRING, -1,  
                                 (LPARAM) (char *) buffer);
		   sprintf (buffer, " %-13.0lf %s", (double) 2, "Fleisch");
           SendMessage (hWnd, LB_INSERTSTRING, -1,  
                                 (LPARAM) (char *) buffer);
           SendMessage (hWnd, LB_SETCURSEL, 0, 0L);
}


void EINGABE::FillArtikelBox (void)
{
	       HWND hWnd;
		   char buffer [80];
		   int cursor;
		   double a;
		   char a_bz1 [25];

		   sqlout ((double *) &a, 3, 0);
		   sqlout ((char *)   a_bz1, 0, 25); 
		   cursor = sqlcursor ("select a, a_bz1 from a_bas "
			                   "where a > 0 order by a");

		   hWnd = fWork->GethWndName ("Artikel");
		   sprintf (buffer, " %-13s %s", "Artikel", "Bezeichnung");
           SendMessage (hWnd, LB_TITLE, -1,  
                                 (LPARAM) (char *) buffer);
		   while (sqlfetch (cursor) == 0)
		   {

		              sprintf (buffer, " %-13.0lf %s", (double) a, a_bz1);
                      SendMessage (hWnd, LB_INSERTSTRING, -1,  
                                 (LPARAM) (char *) buffer);
		   }
		   sqlclose (cursor);
           SendMessage (hWnd, LB_SETCURSEL, 0, 0L);
}

void EINGABE::VLines (void)
{
	       HWND hWnd;

		   hWnd = fWork->GethWndName ("Artikel");
           SendMessage (hWnd, LB_CAPTSIZE, 0,  
                                 (LPARAM) 130);
//           SendMessage (hWnd, LB_ROWSIZE, 0,  
//                                 (LPARAM) 130);
           SendMessage (hWnd, LB_VPOS, 0,  
                                 (LPARAM) (char *) vpos);
}
						
void EINGABE::OpenWindow (HANDLE hInstance, HWND hMainWindow)
{
		  int cx, cy, x, y;
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  RECT rect;

		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;
		  SetColBorderM (TRUE);

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CEinWind";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }

		  GetClientRect (hMainWindow, &rect);
		  cx = rect.right;
		  cy = rect.bottom;
		  x  = rect.left;
		  y  = rect.top;

          hWnd = CreateWindowEx (0, 
                                 "CEinWind",
                                 "",
                                 WS_VISIBLE | WS_CHILD,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
		  ShowWindow (hWnd, SW_SHOWNORMAL);
		  UpdateWindow (hWnd);
		  VLines ();
		  FillAbtBox ();
		  FillArtikelBox ();
}

void EINGABE::MoveWindow (void)
{
		  RECT rect;

		  GetClientRect (hMainWindow, &rect);
          ::MoveWindow (hWnd, rect.left, rect.top, rect.right, rect.bottom, TRUE);

}

