// KUnden2.cpp : implementation file
//

// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1997 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#include "stdafx.h"
#include "test3.h"
#include "kdpage2.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKunden2Page property page

CKunden2Page::CKunden2Page()
	: CPropertyPage(CKunden2Page::IDD)
{
	//{{AFX_DATA_INIT(CKunden2Page)
/*	m_cstrFileName = _T("");
	m_bCentered = TRUE;
	m_bTransparent = FALSE;
	m_bAutoplay = FALSE;*/
	//}}AFX_DATA_INIT
//	m_dwStyle = WS_CHILD|WS_VISIBLE|WS_BORDER|ACS_CENTER;
}

void CKunden2Page::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CKunden2Page)
/*	DDX_Check(pDX, IDC_CENTER, m_bCentered);
	DDX_Check(pDX, IDC_TRANSPARENT, m_bTransparent);
	DDX_Check(pDX, IDC_AUTOPLAY, m_bAutoplay);
	DDX_Text(pDX, IDC_EDIT1, m_cstrFileName);*/
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CKunden2Page, CPropertyPage)
	//{{AFX_MSG_MAP(CKunden2Page)
/*	ON_EN_KILLFOCUS(IDC_EDIT1, OnFileChange)
	ON_BN_CLICKED(IDC_BROWSE, OnBrowse)
	ON_BN_CLICKED(IDC_CENTER, OnCenter)
	ON_BN_CLICKED(IDC_TRANSPARENT, OnTransparent)
	ON_BN_CLICKED(IDC_AUTOPLAY, OnAutoplay)
	ON_BN_CLICKED(IDC_PLAY, OnPlay)
	ON_BN_CLICKED(IDC_STOP, OnStop)*/
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKunden2Page message handlers
BOOL CKunden2Page::OnInitDialog()
{
	if(!CPropertyPage::OnInitDialog())
		return FALSE;
	return TRUE;
}
