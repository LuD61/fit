#ifndef __DEFINES_DEF
#define __DEFINES_DEF


#define CString Text

#define MAXLEN 40
// #define MAXPOS 3000
#define LPLUS 1
 
#define IDM_FRAME     901
#define IDM_REVERSE   902
#define IDM_NOMARK    903
#define IDM_EDITMARK  904
#define IDM_PAGEVIEW  905
#define IDM_LIST      906
#define IDM_PAGE      907
#define IDM_FONT      908
#define IDM_FIND      909
#define IDM_ANZBEST   910
#define IDM_STD       911
#define IDM_BASIS     912
#define IDM_LGR       913
#define IDM_LGRDEF    914
#define IDM_POSTEXT   915
//#define IDM_VINFO     916
#define IDM_LDPR      917
#define IDM_VKPR      918
#define IDM_MUSTER    923 
#define IDM_KUNLIEF   924
#define IDM_LS        925 
#define IDM_KOMMIS    926
#define QUERYAUF      920 
#define QUERYKUN      921 
#define QUERYTOU      922 
#define IDM_NACHDRUCK 924 
#define IDM_PRICEFROMLIST 927
#define IDM_PARTYSERVICE    931 
#define IDM_FULLTAX   932 
#define IDM_REFRESH_STND      933
#define IDM_BESTELLVORSCHL    934
#define IDM_IGNOREMINBEST    935
#define IDM_FIXWG    936
#define IDM_FIXPAL    937
#define IDM_FIXBMP    938
#define IDM_SHOWBMP   939
#define IDM_SHOWBMPP  940
#define IDM_KRITMHD   941
#define IDM_PALETTE   942
#define IDM_WARENKORB  943
#define IDM_BESTAND   944
#define QUERYSHOP     945
#define IDM_KUNDENINFO    946 
#define IDM_SONDERETIKETT    947 
#define IDM_A_KUN_TXT 948
#define PARTY_POSITION 14

#define TAB 9



#define WALSER 23




#endif