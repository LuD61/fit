#ifndef _CONTROL_PLUGINS_DEF
#define _CONTROL_PLUGINS_DEF
#include <windows.h>
#include "SmtTable.h"

class CControlPlugins
{
private:
	CSmtTable m_SmtTable;
	void (*m_ShowOrderSum) (HWND,CSmtTable*);
	void (*m_HideOrderSum) ();
	void (*m_CloseOrderSum) ();
public:
	enum
	{
		KommOnly = 0,
		LiefVisible = 1,
		LiefRechVisible = 2,
	};
	CSmtTable *SmtTable ()
	{
		return &m_SmtTable;
	}
	static HANDLE Controls;
    int (*pGetPosTxtKz) (int PosTxtKz);
	CControlPlugins ();
	int GetPosTxtKz (int PosTxtKz);
	BOOL IsActive ();
	void HideOrderSum ();
	void CloseOrderSum ();
	void ShowOrderSum (HWND);
};

#endif
