#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "iprz.h"

struct IPRZ iprz, iprz_null;

void IPRZ_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &iprz.mdn, 2, 0);
            ins_quest ((char *) &iprz.a, 3, 0);
            ins_quest ((char *) &iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &iprz.kun_pr, 2, 0);
            ins_quest ((char *) &iprz.kun, 2, 0);
    out_quest ((char *) &iprz.mdn,2,0);
    out_quest ((char *) &iprz.pr_gr_stuf,2,0);
    out_quest ((char *) &iprz.kun_pr,2,0);
    out_quest ((char *) &iprz.a,3,0);
    out_quest ((char *) &iprz.vk_pr_i,3,0);
    out_quest ((char *) &iprz.ld_pr,3,0);
    out_quest ((char *) &iprz.aktion_nr,1,0);
    out_quest ((char *) iprz.a_akt_kz,0,2);
    out_quest ((char *) iprz.modif,0,2);
    out_quest ((char *) &iprz.waehrung,1,0);
    out_quest ((char *) &iprz.kun,2,0);
    out_quest ((char *) &iprz.a_grund,3,0);
    out_quest ((char *) iprz.kond_art,0,5);
            cursor = prepare_sql ("select iprz.mdn,  "
"iprz.pr_gr_stuf,  iprz.kun_pr,  iprz.a,  iprz.vk_pr_i,  iprz.ld_pr,  "
"iprz.aktion_nr,  iprz.a_akt_kz,  iprz.modif,  iprz.waehrung,  iprz.kun,  "
"iprz.a_grund,  iprz.kond_art from iprz "

#line 29 "iprz.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ?"); 



    ins_quest ((char *) &iprz.mdn,2,0);
    ins_quest ((char *) &iprz.pr_gr_stuf,2,0);
    ins_quest ((char *) &iprz.kun_pr,2,0);
    ins_quest ((char *) &iprz.a,3,0);
    ins_quest ((char *) &iprz.vk_pr_i,3,0);
    ins_quest ((char *) &iprz.ld_pr,3,0);
    ins_quest ((char *) &iprz.aktion_nr,1,0);
    ins_quest ((char *) iprz.a_akt_kz,0,2);
    ins_quest ((char *) iprz.modif,0,2);
    ins_quest ((char *) &iprz.waehrung,1,0);
    ins_quest ((char *) &iprz.kun,2,0);
    ins_quest ((char *) &iprz.a_grund,3,0);
    ins_quest ((char *) iprz.kond_art,0,5);
            sqltext = "update iprz set iprz.mdn = ?,  "
"iprz.pr_gr_stuf = ?,  iprz.kun_pr = ?,  iprz.a = ?,  iprz.vk_pr_i = ?,  "
"iprz.ld_pr = ?,  iprz.aktion_nr = ?,  iprz.a_akt_kz = ?,  "
"iprz.modif = ?,  iprz.waehrung = ?,  iprz.kun = ?,  iprz.a_grund = ?,  "
"iprz.kond_art = ? "

#line 38 "iprz.rpp"
                               "where mdn = ? "
                               "and   a = ? "
                               "and   pr_gr_stuf = ? "
                               "and   kun_pr = ? "
                               "and   kun = ?";
  
            ins_quest ((char *) &iprz.mdn, 2, 0);
            ins_quest ((char *) &iprz.a, 3, 0);
            ins_quest ((char *) &iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &iprz.kun_pr,  2, 0);  
            ins_quest ((char *) &iprz.kun,  2, 0);  

            upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &iprz.mdn,2,0);
    ins_quest ((char *) &iprz.pr_gr_stuf,2,0);
    ins_quest ((char *) &iprz.kun_pr,2,0);
    ins_quest ((char *) &iprz.a,3,0);
    ins_quest ((char *) &iprz.vk_pr_i,3,0);
    ins_quest ((char *) &iprz.ld_pr,3,0);
    ins_quest ((char *) &iprz.aktion_nr,1,0);
    ins_quest ((char *) iprz.a_akt_kz,0,2);
    ins_quest ((char *) iprz.modif,0,2);
    ins_quest ((char *) &iprz.waehrung,1,0);
    ins_quest ((char *) &iprz.kun,2,0);
    ins_quest ((char *) &iprz.a_grund,3,0);
    ins_quest ((char *) iprz.kond_art,0,5);
            ins_cursor = prepare_sql ("insert into iprz (mdn,  "
"pr_gr_stuf,  kun_pr,  a,  vk_pr_i,  ld_pr,  aktion_nr,  a_akt_kz,  modif,  waehrung,  "
"kun,  a_grund,  kond_art) "

#line 53 "iprz.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?)"); 

#line 55 "iprz.rpp"
            ins_quest ((char *) &iprz.mdn, 2, 0);
            ins_quest ((char *) &iprz.a, 3, 0);
            ins_quest ((char *) &iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &iprz.kun_pr,  2, 0);  
            ins_quest ((char *) &iprz.kun,  2, 0);  
            test_upd_cursor = prepare_sql ("select a from iprz "
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ?"); 
              
            ins_quest ((char *) &iprz.mdn, 2, 0);
            ins_quest ((char *) &iprz.a, 3, 0);
            ins_quest ((char *) &iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &iprz.kun_pr,  2, 0);  
            ins_quest ((char *) &iprz.kun,  2, 0);  
            del_cursor = prepare_sql ("delete from iprz "
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ?"); 
}

void IPRZ_CLASS::prepare_a (char *order)
{
            ins_quest ((char *) &iprz.mdn, 2, 0);
            ins_quest ((char *) &iprz.a, 3, 0);
            if (order)
            {
    out_quest ((char *) &iprz.mdn,2,0);
    out_quest ((char *) &iprz.pr_gr_stuf,2,0);
    out_quest ((char *) &iprz.kun_pr,2,0);
    out_quest ((char *) &iprz.a,3,0);
    out_quest ((char *) &iprz.vk_pr_i,3,0);
    out_quest ((char *) &iprz.ld_pr,3,0);
    out_quest ((char *) &iprz.aktion_nr,1,0);
    out_quest ((char *) iprz.a_akt_kz,0,2);
    out_quest ((char *) iprz.modif,0,2);
    out_quest ((char *) &iprz.waehrung,1,0);
    out_quest ((char *) &iprz.kun,2,0);
    out_quest ((char *) &iprz.a_grund,3,0);
    out_quest ((char *) iprz.kond_art,0,5);
                        cursor_a = prepare_sql ("select "
"iprz.mdn,  iprz.pr_gr_stuf,  iprz.kun_pr,  iprz.a,  iprz.vk_pr_i,  "
"iprz.ld_pr,  iprz.aktion_nr,  iprz.a_akt_kz,  iprz.modif,  iprz.waehrung,  "
"iprz.kun,  iprz.a_grund,  iprz.kond_art from iprz "

#line 87 "iprz.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "%s", order);
            }
            else
            {
    out_quest ((char *) &iprz.mdn,2,0);
    out_quest ((char *) &iprz.pr_gr_stuf,2,0);
    out_quest ((char *) &iprz.kun_pr,2,0);
    out_quest ((char *) &iprz.a,3,0);
    out_quest ((char *) &iprz.vk_pr_i,3,0);
    out_quest ((char *) &iprz.ld_pr,3,0);
    out_quest ((char *) &iprz.aktion_nr,1,0);
    out_quest ((char *) iprz.a_akt_kz,0,2);
    out_quest ((char *) iprz.modif,0,2);
    out_quest ((char *) &iprz.waehrung,1,0);
    out_quest ((char *) &iprz.kun,2,0);
    out_quest ((char *) &iprz.a_grund,3,0);
    out_quest ((char *) iprz.kond_art,0,5);
                        cursor_a = prepare_sql ("select "
"iprz.mdn,  iprz.pr_gr_stuf,  iprz.kun_pr,  iprz.a,  iprz.vk_pr_i,  "
"iprz.ld_pr,  iprz.aktion_nr,  iprz.a_akt_kz,  iprz.modif,  iprz.waehrung,  "
"iprz.kun,  iprz.a_grund,  iprz.kond_art from iprz "

#line 94 "iprz.rpp"
                                  "where mdn = ? "
                                  "and   a   = ?");
            }
            ins_quest ((char *) &iprz.mdn, 1, 0);
            ins_quest ((char *) &iprz.a, 3, 0);
            del_a_curs = prepare_sql ("delete from iprz "
                                  "where mdn = ? "
                                  "and   a   = ?");
}

void IPRZ_CLASS::prepare_gr (char *order)
{
            ins_quest ((char *) &iprz.mdn, 2, 0);
            ins_quest ((char *) &iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &iprz.kun_pr, 2, 0);
            if (order)
            {
    out_quest ((char *) &iprz.mdn,2,0);
    out_quest ((char *) &iprz.pr_gr_stuf,2,0);
    out_quest ((char *) &iprz.kun_pr,2,0);
    out_quest ((char *) &iprz.a,3,0);
    out_quest ((char *) &iprz.vk_pr_i,3,0);
    out_quest ((char *) &iprz.ld_pr,3,0);
    out_quest ((char *) &iprz.aktion_nr,1,0);
    out_quest ((char *) iprz.a_akt_kz,0,2);
    out_quest ((char *) iprz.modif,0,2);
    out_quest ((char *) &iprz.waehrung,1,0);
    out_quest ((char *) &iprz.kun,2,0);
    out_quest ((char *) &iprz.a_grund,3,0);
    out_quest ((char *) iprz.kond_art,0,5);
                        cursor_gr = prepare_sql ("select "
"iprz.mdn,  iprz.pr_gr_stuf,  iprz.kun_pr,  iprz.a,  iprz.vk_pr_i,  "
"iprz.ld_pr,  iprz.aktion_nr,  iprz.a_akt_kz,  iprz.modif,  iprz.waehrung,  "
"iprz.kun,  iprz.a_grund,  iprz.kond_art from iprz "

#line 112 "iprz.rpp"
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ? "
                                  "%s", order);
            }
            else
            {
    out_quest ((char *) &iprz.mdn,2,0);
    out_quest ((char *) &iprz.pr_gr_stuf,2,0);
    out_quest ((char *) &iprz.kun_pr,2,0);
    out_quest ((char *) &iprz.a,3,0);
    out_quest ((char *) &iprz.vk_pr_i,3,0);
    out_quest ((char *) &iprz.ld_pr,3,0);
    out_quest ((char *) &iprz.aktion_nr,1,0);
    out_quest ((char *) iprz.a_akt_kz,0,2);
    out_quest ((char *) iprz.modif,0,2);
    out_quest ((char *) &iprz.waehrung,1,0);
    out_quest ((char *) &iprz.kun,2,0);
    out_quest ((char *) &iprz.a_grund,3,0);
    out_quest ((char *) iprz.kond_art,0,5);
                        cursor_gr = prepare_sql ("select "
"iprz.mdn,  iprz.pr_gr_stuf,  iprz.kun_pr,  iprz.a,  iprz.vk_pr_i,  "
"iprz.ld_pr,  iprz.aktion_nr,  iprz.a_akt_kz,  iprz.modif,  iprz.waehrung,  "
"iprz.kun,  iprz.a_grund,  iprz.kond_art from iprz "

#line 120 "iprz.rpp"
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ?");
            }
            ins_quest ((char *) &iprz.mdn, 1, 0);
            ins_quest ((char *) &iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &iprz.kun_pr, 2, 0);
            del_gr_curs = prepare_sql ("delete from iprz "
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ?");
}

int IPRZ_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int IPRZ_CLASS::dbreadfirst_a (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_a == -1)
         {
                this->prepare_a (order);
         }
         open_sql (cursor_a);
         return fetch_sql (cursor_a);
}

int IPRZ_CLASS::dbread_a (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         return fetch_sql (cursor_a);
}

int IPRZ_CLASS::dbreadfirst_gr (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_gr == -1)
         {
                this->prepare_gr (order);
         }
         open_sql (cursor_gr);
         return fetch_sql (cursor_gr);
}

int IPRZ_CLASS::dbread_gr (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         return fetch_sql (cursor_gr);
}

int IPRZ_CLASS::dbdelete_gr (void)
{
         if (del_gr_curs == -1)
         {
                this->prepare_a (NULL);
         }
         return execute_curs (del_gr_curs);
}
       

int IPRZ_CLASS::dbdelete_a (void)
{
         if (del_a_curs == -1)
         {
                this->prepare_a (NULL);
         }
         return execute_curs (del_a_curs);
}
       


int IPRZ_CLASS::dbclose_a (void)
{
        if (cursor_a != -1)
        { 
                 close_sql (cursor_a);
                 cursor_a = -1;
        }
        return 0;
}


int IPRZ_CLASS::dbclose_gr (void)
{
        if (cursor_gr != -1)
        { 
                 close_sql (cursor_gr);
                 cursor_gr = -1;
        }
        return 0;
}

