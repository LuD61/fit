#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "message.h"
#include "ls.h"
#include "kun.h"
#include "mdn.h"
#include "fil.h"
#include "mo_curso.h"
#include "sys_par.h"
#include "dbclass.h"
#include "mo_paket.h"

#define MAXWAIT 60

static LS_CLASS ls_class;
static SYS_PAR_CLASS sys_par_class;
static DB_CLASS DbClass;
extern MELDUNG Mess;

static int dsqlstatus;
static long auf;
static double a;
static double PaketArtikel = 0.0;

void PAKET_CLASS::arttotab (double a, double auf_me, int me_einh_kun, double rab_satz, char *auf_me_bz)
/**
Artikel und Auftrag in Matrix einfuegen.
**/
{
         int i;

         for (i = 0; i < paket_anz; i ++)
         {
             if (a == paket[i].a) return;
         }
         paket[i].a = a;
         paket[i].auf_me = auf_me;
         paket[i].me_einh_kun = me_einh_kun;
         paket[i].rab_satz = rab_satz;
		 sprintf(paket[i].auf_me_bz, "%s",auf_me_bz); 
         paket_anz ++;
}

BOOL PAKET_CLASS::ReadPaket (short mdn,short fil, long kun, double a)
/**
Artikel auf letzten Auftraegen holen.
**/
{
         int paket_cursor;
	int auf_ext = int (a);
	BOOL MustCalculate = TRUE;
	double p_a = 0.0;
	double p_auf_me = 0.0;
	int p_me_einh_kun = 0;
	char p_auf_me_bz [7]; 
	double p_rab_satz = 0.0;
	BOOL ArtikelGefunden = FALSE;

	PaketArtikel = a;
          
         paket_anz = 0;
      
	   DbClass.sqlin ((short *) &mdn,    1, 0);
	   DbClass.sqlin ((short *) &fil,    1, 0);
	   DbClass.sqlin ((long *)  &kun,  2, 0);
	   DbClass.sqlin ((long *)  &auf_ext,     2, 0);
	   DbClass.sqlout ((double *)  &p_a,     3, 0);
	   DbClass.sqlout ((double *)  &p_auf_me,     3, 0);
	   DbClass.sqlout ((long *)  &p_me_einh_kun,     2, 0);
	   DbClass.sqlout ((char *)  p_auf_me_bz,     0, 6);
	   DbClass.sqlout ((double *)  &p_rab_satz,     3, 0);
	   paket_cursor = DbClass.sqlcursor ("select aufp.a, aufp.auf_me,aufp.me_einh_kun,aufp.auf_me_bz,aufp.rab_satz from aufk,aufp "
									 "where aufk.mdn = ? "
									 "and   aufk.fil = ? "
									 "and   aufk.kun = ? "
									 "and   aufk.auf_art = 99 "
									 "and   aufk.auf_ext = ? "
									 "and   aufk.mdn = aufp.mdn and aufk.fil = aufp.fil and aufk.auf = aufp.auf ");



     DbClass.sqlopen (paket_cursor);
	   dsqlstatus = DbClass.sqlfetch (paket_cursor);
	   while (dsqlstatus == 0)
	   {
		   ArtikelGefunden = TRUE;
           arttotab (p_a, p_auf_me, p_me_einh_kun, p_rab_satz, p_auf_me_bz);
		   dsqlstatus = DbClass.sqlfetch (paket_cursor);
	   }

         sqlclose (paket_cursor);
		 return ArtikelGefunden;
}

double PAKET_CLASS::GetPaketArtikel (void)
{
	return PaketArtikel;
}

BOOL PAKET_CLASS::GetFirst (double *a, double *auf_me, int *me_einh_kun, double *rab_satz, char *auf_me_bz)
/**
1. Satz aus Matrix holen.
**/
{
         if (paket_anz == 0)
         {
             return FALSE;
         }
         *a           = paket[0].a;
         *auf_me      = paket[0].auf_me;
         *me_einh_kun = paket[0].me_einh_kun;
         *rab_satz    = paket[0].rab_satz;
		 sprintf(auf_me_bz, "%s",paket[0].auf_me_bz); 
         paket_pos = 0;
         return TRUE;
}
         
BOOL PAKET_CLASS::GetNext (double *a, double *auf_me, int *me_einh_kun, double *rab_satz, char *auf_me_bz)
/**
Naechsten Satz aus Matrix holen.
**/
{
         paket_pos ++;
         if (paket_pos == paket_anz)
         {
             return FALSE;
         }
         *a           = paket[paket_pos].a;
         *auf_me      = paket[paket_pos].auf_me;
         *me_einh_kun = paket[paket_pos].me_einh_kun;
         *rab_satz    = paket[paket_pos].rab_satz;
		 sprintf(auf_me_bz, "%s",paket[paket_pos].auf_me_bz); 
         return TRUE;
}

BOOL PAKET_CLASS::GetPrior (double *a, double *auf_me, int *me_einh_kun, double *rab_satz, char *auf_me_bz)
/**
Naechsten Satz aus Matrix holen.
**/
{
         if (paket_pos == 0) return FALSE;
         paket_pos --;
         *a           = paket[paket_pos].a;
         *auf_me      = paket[paket_pos].auf_me;
         *me_einh_kun = paket[paket_pos].me_einh_kun;
         *rab_satz    = paket[paket_pos].rab_satz;
		 sprintf(auf_me_bz, "%s",paket[paket_pos].auf_me_bz); 
         return TRUE;
}

BOOL PAKET_CLASS::GetCurrent (double *a, double *auf_me, int *me_einh_kun, double *rab_satz, char *auf_me_bz)
/**
Naechsten Satz aus Matrix holen.
**/
{
         *a           = paket[paket_pos].a;
         *auf_me      = paket[paket_pos].auf_me;
         *me_einh_kun = paket[paket_pos].me_einh_kun;
         *rab_satz    = paket[paket_pos].rab_satz;
		 sprintf(auf_me_bz, "%s",paket[paket_pos].auf_me_bz); 
         return TRUE;
}

BOOL PAKET_CLASS::GetAbsolute (double *a, double *auf_me, int *me_einh_kun, double *rab_satz, char *auf_me_bz, int pos)
/**
Naechsten Satz aus Matrix holen.
**/
{
         if (pos >= paket_anz) return FALSE;
         if (pos < 0) return FALSE;
         paket_pos = pos;
         *a           = paket[paket_pos].a;
         *auf_me      = paket[paket_pos].auf_me;
         *me_einh_kun = paket[paket_pos].me_einh_kun;
         *rab_satz    = paket[paket_pos].rab_satz;
		 sprintf(auf_me_bz, "%s",paket[paket_pos].auf_me_bz); 
         return TRUE;
}
