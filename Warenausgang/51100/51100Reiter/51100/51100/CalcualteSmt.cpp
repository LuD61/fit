// CalcualteSmt.cpp: Implementierung der Klasse CCalcualteSmt.
//
//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "CalcualteSmt.h"
#include "strfkt.h"
#include "Text.h"
#include "ListHandler.h"
#include  "hwg.h"
#include  "DbClass.h" //LAC-109
#include  "mo_curso.h" //LAC-109
#include  "datum.h" //LAC-109
#include  "kun.h" //LAC-96
#include  "aufchart.h" //LAC-109


#define CString Text
#define LISTHANDLER CListHandler::GetInstance ()

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

// List-Klasse f�r Sortimentwerte pro Position sortiert

HWG_CL Hwg ; 
extern AUFCHART_CLASS AufChart ;  //LAC-96

static DB_CLASS DbClass;

CSmtOrderItem::CSmtOrderItem ()
{
	m_A     = 0.0;
	m_Smt   = 0;
	m_Me    = 0.0;
	m_Gew   = 0.0;
	m_Vk    = 0.0;
	m_A_gew = 0.0;
}

CSmtOrderList::CSmtOrderList ()
{
}
	
CSmtOrderList::~CSmtOrderList ()
{
	DestroyElements ();
	Destroy ();
}

void CSmtOrderList::DestroyElements ()
{
	CSmtOrderItem *element;

	if (Arr != NULL)
	{
		for (int i = 0; i < anz ; i ++)
		{
			element = *Get (i);
			delete element;
			Arr[i] = NULL;
		}
	}
	anz = 0;
	pos = 0;
}


double CSmtOrderList::GetOrderAmount (short smt)
{
	double value = 0;
	int i = 0;
	CSmtOrderItem * Item;

	for (i = 0; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () == smt)
		{
			break;
		}
	}

	for (; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () != smt)
		{
			break;
		}
		value += Item->Vk () * Item->Me ();
	}

	return value;
}



double CSmtOrderList::GetOrderWeight (short smt)
{
	double value = 0.0;
	int i = 0;
	CSmtOrderItem * Item;

	for (i = 0; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () == smt)
		{
			break;
		}
	}

	for (; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () != smt)
		{
			break;
		}
		value += Item->Gew ();
	}

	return value;
}

double CSmtOrderList::GetLiq (short smt)
{
	double value = 0;
    double amount = GetOrderAmount (smt); 
    double weight = GetOrderWeight (smt);   
	if (weight != 0.0)
	{
		value = amount / weight;
	}
	return value;
}

int CSmtOrderList::GetStndAllElements (short smt)
{
	int i;
	CDataCollection<AUFPS *> *StndAll = LISTHANDLER->GetStndAll ();
	AUFPS *aufps;
	int SmtAllItems = 0;

	/** LAC-111
	for (i = 0; i < StndAll->anz; i ++)
	{
		aufps = *StndAll->Get (i);
//		if (atoi (aufps->teil_smt) == (int) smt) 
		if (aufps->hwg == (int) smt)   //LAC-111 teil_smt -> hwg
		{
			break;
		}
	}
	**/

	for (i = 0; i < StndAll->anz; i ++)
	{
		aufps = *StndAll->Get (i);
//		if (atoi (aufps->teil_smt) == (int) smt) 
		if (aufps->hwg == (int) smt) //LAC-111 teil_smt -> hwg
		{
			SmtAllItems ++;
		}
	}
	return SmtAllItems;
}


int CSmtOrderList::GetSmtAnzahl (short smt)
{
	int SmtValueItems = 0;

	int i = 0;
	CSmtOrderItem * Item;

	for (i = 0; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () == smt)
		{
			if (Item->Me () != 0.0)
			{
    			SmtValueItems ++;
			}
		}
	}
	return SmtValueItems;
}



double CSmtOrderList::GetSmtQuot (short smt)
{
	double value = 0;
	int SmtAllItems = 0;
	int SmtValueItems = 0;

	int i = 0;
	CSmtOrderItem * Item;

	/** 260312
	for (i = 0; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () == smt)
		{
			break;
		}
	}
	**/

	for (; i < anz; i ++)
	{
		Item = *Get (i);
		if (Item->Smt () != smt)
		{
	//		break;
    		SmtAllItems ++; //LAC-111 Anteil am Sortiment
			continue ; //260312
		}
		SmtAllItems ++;
		if (Item->Me () != 0.0)
		{
			SmtValueItems ++;
		}
	}

	if (SmtAllItems != 0)
	{
		value = (double) SmtValueItems * 100 / SmtAllItems;
	}

	/** LAC-111 
	int allElements = GetStndAllElements (smt);
	if (allElements  != 0)
	{
		value = (double) SmtValueItems * 100 / allElements;
	}
	**/

	return value;
}


double CSmtOrderList::GetSmtQuot ()
{
	double value = 0;
	int SmtAllItems = 0;
	int SmtValueItems = 0;

	int i = 0;
	CSmtOrderItem * Item;


	for (; i < anz; i ++)
	{
		Item = *Get (i);
		SmtAllItems ++;
		if (Item->Me () != 0.0)
		{
			SmtValueItems ++;
		}
	}

	if (SmtAllItems != 0.0)
	{
		value = (double) SmtValueItems * 100 / SmtAllItems;
	}

	/** LAC-111
	if (LISTHANDLER->GetStndAll ()->anz > 0)
	{
		value = (double) SmtValueItems * 100 / LISTHANDLER->GetStndAll ()->anz;
	}
	**/

	return value;
}


// Vergleichs-Klasse f�r Sortimentwerte.  

int CCompareOrderInterface::Compare (CSmtOrderItem *Item1, CSmtOrderItem *Item2)
{
	return Item1->Smt () - Item2->Smt ();
}

// List-Klasse f�r Sortimente eindeutig, sortiert

CSmtList::CSmtList ()
{
	set_Unique (TRUE);
}
	
CSmtList::~CSmtList ()
{
	Destroy ();
}

// Vergleichs-Klasse f�r Sortimente.  

int CCompareSmtInterface::Compare (short Item1, short Item2)
{
	return Item1 - Item2;
}

CCalcualteSmt::CCalcualteSmt()
{
	m_SmtOrderList.set_CompareInterface (&Compare);
	m_SmtList.set_CompareInterface (&SmtCompare);
	m_Controls = NULL;
	m_hWnd = NULL;
}

CCalcualteSmt::~CCalcualteSmt()
{
	m_SmtOrderList.DestroyElements ();
	m_SmtOrderList.Destroy ();
}

void CCalcualteSmt::Calculate (AUFPS *aufps, int size)
{
	int i = 0;
	double sumAmount = 0.0;
	double sumWeight = 0.0;
	double sumLiq = 0.0;
	double sumSmtPart = 0.0;

	m_SmtOrderList.DestroyElements ();
	m_SmtList.Destroy ();
	for (i = 0; i < size; i ++)
	{
	  if (ratod (aufps[i].lief_me) != 0.0)
	  { 
		if (!IsDiscountArticle (ratod (aufps[i].a), aufps[i].smt))
		{
			CSmtOrderItem * Item = new CSmtOrderItem ();
			Item->set_A (ratod (aufps[i].a));
//			Item->set_Smt ((short) atoi (aufps[i].teil_smt));  //todo hier statt teil_smt hwg //140312 
			Item->set_Smt (aufps[i].hwg);  //140312 
			Item->set_Me (ratod (aufps[i].lief_me));
			Item->set_Vk (ratod (aufps[i].auf_vk_pr));
			Item->set_A_gew (aufps[i].a_gew);
			if (atoi (aufps[i].me_einh) != 2) 
			{
				Item->set_Gew (Item->Me () * Item->A_gew ());  
			}
			else
			{
				Item->set_Gew (Item->Me ());
			}
			m_SmtOrderList.Add (Item);
			m_SmtList.Add (Item->Smt ());
		}
	  }
	}

	if (m_Controls != NULL)
	{
		CSmtTable * smtTable = m_Controls->SmtTable ();
		smtTable->Clear ();
		for (int i = 0; i < m_SmtList.anz; i ++)
		{
			short smt      = *m_SmtList.Get (i);
			double amount  = m_SmtOrderList.GetOrderAmount (smt); 
			double weight  = m_SmtOrderList.GetOrderWeight (smt); 
			double liq     = m_SmtOrderList.GetLiq (smt); 
			double smtquot = m_SmtOrderList.GetSmtQuot (smt);
			if (amount != 0.0 || weight != 0.0)
			{
				sumAmount += amount;
				sumWeight += weight;
				sumLiq += liq;
				sumSmtPart += smtquot;
				CString cSmt;
				cSmt.Format ("%d", smt);
				CSmtValues *smtValues = new CSmtValues ();
				memcpy (&ptabn, &ptabn_null, sizeof (PTABN));
//140312				Ptabn.lese_ptab ("teil_smt", cSmt.GetBuffer ());              //todo
//140312				smtValues->set_Name (CString::TrimRight (ptabn.ptbez));
				hwg.hwg = atoi(cSmt.GetBuffer()); //140312
				if (Hwg.dbreadfirst() == 0) //140312
				{
					smtValues->set_Name (CString::TrimRight (hwg.hwg_bz1));
				}
				else
				{
					smtValues->set_Name ("");
				}
				smtValues->set_Amount (amount);
				smtValues->set_Weight (weight);
				smtValues->set_Liq (liq);
				smtValues->set_SmtPart (smtquot);
				smtTable->Add (smtValues);
				delete smtValues;
			}
		}
		sumSmtPart = m_SmtOrderList.GetSmtQuot ();
		if (sumWeight != 0.0)
        { 
			sumLiq = sumAmount / sumWeight;
		}
		CSmtValues *smtValues = new CSmtValues ();
		smtValues->set_Name ("Gesamt");
		smtValues->set_Amount (sumAmount);
		smtValues->set_Weight (sumWeight);
		smtValues->set_Liq (sumLiq);
		smtValues->set_SmtPart (sumSmtPart);
		smtTable->Add (smtValues);
	}
}

void CCalcualteSmt::Calculate (int anz_tage,int anz_tage_bis, int cfg_AnzTageSollwerte)  //LAC-109  initial best�cken  von sysdate - anz_tage bis sysdate
{
//	   DB_CLASS DbClass;
	   extern LS_CLASS ls_class ;  
		extern short sql_mode;

	   int aufk_cursor; 
	   int dsql_aufp = 0;
	int size = 0;
	int i = 0;
	double sumAmount = 0.0;
	double sumWeight = 0.0;
	double sumLiq = 0.0;
	double sumSmtPart = 0.0;
         char datum [12];
         long sysdatum;
         long sysdatum_bis;


    sql_mode = 1;

	beginwork ();
     sysdate (datum);
     sysdatum = dasc_to_long (datum);
	 sysdatum = sysdatum - anz_tage;
     sysdatum_bis = dasc_to_long (datum);
	 sysdatum_bis = sysdatum_bis - anz_tage_bis;

//	 aufk.auf = 315774;
		DbClass.sqlout ((int *) &aufk.lieferdat, 2, 0);
		DbClass.sqlout ((int *) &aufk.kun, 2, 0);
		DbClass.sqlout ((int *) &aufk.auf, 2, 0);
		DbClass.sqlout ((int *) &kun_erw.m2laden, 2, 0);
		DbClass.sqlout ((int *) &aufchart.jr, 1, 0);
		DbClass.sqlout ((int *) &aufchart.kw, 1, 0);
		DbClass.sqlout ((int *) &aufchart.lieferdat, 2, 0);
//		DbClass.sqlin ((int *) &aufk.auf, 2, 0);

		DbClass.sqlin ((int *) &sysdatum, 2, 0);
		DbClass.sqlin ((int *) &sysdatum_bis, 2, 0);
	 aufk_cursor = DbClass.sqlcursor (" select aufk.lieferdat, aufk.kun,aufk.auf, kun_erw.m2laden, "
										" aufchart.jr, aufchart.kw, aufchart.lieferdat from aufk,outer kun_erw,outer aufchart where "
										" aufk.lieferdat between ? and ? and aufk.auf_stat > 0 and aufk.kun = kun_erw.kun and aufk.mdn = kun_erw.mdn and aufk.fil = kun_erw.fil and "
										" aufk.auf = aufchart.auf ");
//	 aufk_cursor = DbClass.sqlcursor (" select aufk.lieferdat, aufk.kun,aufk.auf from aufk where auf = ? and auf_stat > 0 ");
     DbClass.sqlopen (aufk_cursor);
  while (DbClass.sqlfetch (aufk_cursor) == 0)
 {
	m_SmtOrderList.DestroyElements ();
	m_SmtList.Destroy ();
	if (aufk.auf == 315666)
		{
			int di = 0;
		}
	dsql_aufp =  ls_class.lese_aufp (1,0,aufk.auf);
	while (dsql_aufp == 0)
	{
	  if (aufp.lief_me != 0.0)
	  { 
		  if (lese_a_bas (aufp.a) == 0)
		  {
			if (!IsDiscountArticle (aufp.a, _a_bas.smt))
			{
				CSmtOrderItem * Item = new CSmtOrderItem ();
				Item->set_A (aufp.a);
				Item->set_Smt (_a_bas.hwg);  //140312 
				Item->set_Me (aufp.lief_me);
				Item->set_Vk (aufp.auf_vk_pr);
				Item->set_A_gew (_a_bas.a_gew);
				if (aufp.me_einh != 2) 
				{
					Item->set_Gew (Item->Me () * Item->A_gew ());  
				}
				else
				{
					Item->set_Gew (Item->Me ());
				}
				m_SmtOrderList.Add (Item);
				m_SmtList.Add (Item->Smt ());
			}
		  }
	  }

 	  dsql_aufp =  ls_class.lese_aufp ();
	}


	char clieferdat [16];
  					   dlong_to_asc (aufk.lieferdat, clieferdat);
					   aufchart.jr = get_jahr (clieferdat);
					   if (aufchart.jr > 0)
					   {
						memcpy (&aufchart, &aufchart_null, sizeof (struct AUFCHART));
						aufchart.auf = aufk.auf;
						AufChart.dbreadfirst ();
						aufchart.kun = aufk.kun;
						aufchart.lieferdat = aufk.lieferdat;
						dlong_to_asc (aufk.lieferdat, clieferdat);
						aufchart.jr = get_jahr (clieferdat);
						if (aufchart.jr <99)  aufchart.jr = 2000 + aufchart.jr;
						aufchart.mo = get_monat (clieferdat);
						aufchart.kw = get_woche (clieferdat);
						aufchart.wochentag = get_wochentag (clieferdat);
						if (aufchart.woche_folge == 0)
						{
							int von_lieferdat = aufk.lieferdat - aufchart.wochentag + 1;
							int bis_lieferdat = aufk.lieferdat - 1;
							int dfolge = 0;
				            ins_quest ((char *) &aufk.kun, 2, 0);
				            ins_quest ((char *) &von_lieferdat, 2, 0);
				            ins_quest ((char *) &bis_lieferdat, 2, 0);
				            out_quest ((char *) &dfolge, 2, 0);
							execute_sql ("select count(unique lieferdat ) from aufk  "
						     "where kun = ? and lieferdat between ? and ? ");   
							   if (sqlstatus == 0)
							   {
									aufchart.woche_folge = dfolge + 1;
							   } else aufchart.woche_folge = 1;
						}
						AufChart.dbupdate ();
					   }

  }

commitwork ();
}


BOOL CCalcualteSmt::IsDiscountArticle (double a, LPSTR smt)
{
    CString Smt = smt;
	Smt.Trim ();
	if (Smt.GetLength () == 0)
	{
		lese_a_bas (a);
		strcpy (smt, _a_bas.smt);
		Smt = _a_bas.smt;
	}
	return (Smt == "R");
}

