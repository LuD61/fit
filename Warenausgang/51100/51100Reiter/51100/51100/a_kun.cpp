#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"

struct A_KUN a_kun, a_kun_null;

/* Variablen fuer Auswahl mit Buttonueberschrift    */

static DB_CLASS DbClass;
#include "itemc.h"
#define MAXSORT 30000
static long MaxSort;

static char *sqltext;

static int dosort1 ();
static int dosort2 ();
static int dosort3 ();
static int dosort4 ();

struct SORT_AW
{
	     char sort1 [10];
	     char sort2 [17];
	     char sort3 [17];
	     char sort4 [25];
	     char sort5 [25];
	     int  sortidx;
};

// static struct SORT_AW sort_aw, sort_awtab[MAXSORT];
static struct SORT_AW sort_aw, *sort_awtab;

static void SortMalloc (void) 
/**
Speicher fuer Listbereich zuordnen.
**/
{
	static BOOL SortOK = FALSE;
    long anz;

	if (SortOK) return;

	DbClass.sqlout ((long *) &anz, 2, 0);
	DbClass.sqlcomm ("select count (*) from a_hndw");

	if (anz == 0l) return;

    sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   ((anz + 1) * sizeof (struct SORT_AW))); 
	while (sort_awtab == NULL)
	{
		if (anz == 0l) break;
		anz --;
        sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   (anz * sizeof (struct SORT_AW))); 
	}
	MaxSort = anz + 1;
	if (anz == 0l)
	{
		print_mess (2, "Es konnte kein Speicher fuer die Artikelauswahl (a_hndw) zugewiesen werden");
		return;
	}
	SortOK = TRUE;
}


static int sort_awanz;
static int sort1 = -1;
static int sort2 = -1;
static int sort3 = -1;
static int sort4 = -1;
static int sort5 = -1;
static int sort_idx = 0;

static ITEM isort1    ("", sort_aw.sort1, "", 0);
static ITEM isort2    ("", sort_aw.sort2, "", 0);
static ITEM isort3    ("", sort_aw.sort3, "", 0);
static ITEM isort4    ("", sort_aw.sort4, "", 0);
static ITEM isort5    ("", sort_aw.sort5, "", 0);
static ITEM ifillub   ("", " ",             "", 0); 

static field _fsort_aw[] = {
&isort1,      9, 1, 0, 0, 0, "%8d",       NORMAL, 0, 0, 0,
&isort2,      8, 1, 0, 9, 0, "",          NORMAL, 0, 0, 0,
&isort3,     16, 1, 0,17, 0, "",          NORMAL, 0, 0, 0,
&isort4,     14, 1, 0,31, 0, "%13.0f",    NORMAL, 0, 0, 0,
&isort5,     24, 1, 0,55,0, "",           NORMAL, 0, 0, 0,
&ifillub,    80, 1, 0, 79, 0, "",         NORMAL, 0, 0, 0,
};

static form fsort_aw = {5, 0, 0, _fsort_aw, 0, 0, 0, 0, NULL};

static ITEM isort1ub    ("", "Kunde",         "", 0);
static ITEM isort2ub    ("", "Branche",       "", 0);
static ITEM isort3ub    ("", "Name",          "", 0);
static ITEM isort4ub    ("", "Artikel",       "", 0);
static ITEM isort5ub    ("", "Bezeichnung",   "", 0);

static field _fsort_awub[] = {
&isort1ub,        9, 1, 0,  0, 0, "", BUTTON, 0, dosort1,    102,
&isort2ub,        8, 1, 0,  9, 0, "", BUTTON, 0, dosort2,    103,
&isort3ub,       14, 1, 0, 17, 0, "", BUTTON, 0, dosort3,    104,
&isort4ub,       24, 1, 0, 31, 0, "", BUTTON, 0, dosort4,    105,
&isort5ub,       24, 1, 0, 55, 0, "", BUTTON, 0, dosort4,    106,
&ifillub,        80, 1, 0, 79, 0, "", BUTTON, 0, 0, 0};

static form fsort_awub = {6, 0, 0, _fsort_awub, 0, 0, 0, 0, NULL};

static ITEM isortl ("", "1", "", 0);

static field _fsort_awl[] = {
&isortl,          1, 1, 0,  9, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 17, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 31, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 55, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 79, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awl = {5, 0, 0, _fsort_awl, 0, 0, 0, 0, NULL};

static int dosort10 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return ((int) (atol(el1->sort1) - atol (el2->sort1)) * 
				                                  sort1);
}


static int dosort1 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort10);
	sort1 *= -1;
      ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static int dosort20 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return (strcmp(el1->sort2, el2->sort2) * sort2);
}


static int dosort2 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort20);
	sort2 *= -1;
      ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}

static int dosort30 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return (strcmp(el1->sort3, el2->sort3) * sort3);
}


static int dosort3 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort30);
	sort3 *= -1;
      ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static int dosort40 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
              if (ratod (el1->sort4) > ratod (el2->sort4))
              {
                                 return sort4;
              }
              else if (ratod (el1->sort4) > ratod (el2->sort4))
              {
                                 return (sort4 * -1);
              }
 
	        return (0);
}


static int dosort4 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort40);
	sort4 *= -1;
      ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}

static int dosort50 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	        return ( strcmp(el1->sort5, el2->sort5) * sort5);
}


static int dosort5 ()
{
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort50);
	sort5 *= -1;
      ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	return 0;
}


static void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        sort_idx = idx;
        break_list ();
        return;
}

static int endsort (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

/* Ende Variablen fuer Auswahl mit Buttonueberschrift    */

static void FillUb  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%8s %-13s",
                          "Kunde ", "Artikel");
}


static void FillValues  (char *buffer)
{
         sprintf (buffer, "%8ld %13.0lf",
                           a_kun.kun, a_kun.a);
}

void A_KUN_CLASS::prepare (void)
{
         ins_quest ((char *) &a_kun.mdn, 1, 0);
         ins_quest ((char *) &a_kun.fil, 1, 0);  
         ins_quest ((char *) &a_kun.kun, 2, 0);
         ins_quest ((char *) &a_kun.a, 3, 0); 

    out_quest ((char *) &a_kun.mdn,1,0);
    out_quest ((char *) &a_kun.fil,1,0);
    out_quest ((char *) &a_kun.kun,2,0);
    out_quest ((char *) &a_kun.a,3,0);
    out_quest ((char *) a_kun.a_kun,0,13);
    out_quest ((char *) a_kun.a_bz1,0,25);
    out_quest ((char *) &a_kun.me_einh_kun,1,0);
    out_quest ((char *) &a_kun.inh,3,0);
    out_quest ((char *) a_kun.kun_bran2,0,3);
    out_quest ((char *) &a_kun.tara,3,0);
    out_quest ((char *) &a_kun.ean,3,0);
    out_quest ((char *) a_kun.a_bz2,0,25);
    out_quest ((char *) &a_kun.hbk_ztr,1,0);
    out_quest ((char *) &a_kun.kopf_text,2,0);
    out_quest ((char *) a_kun.pr_rech_kz,0,2);
    out_quest ((char *) a_kun.modif,0,2);
    out_quest ((char *) &a_kun.text_nr,2,0);
    out_quest ((char *) &a_kun.devise,1,0);
    out_quest ((char *) a_kun.geb_eti,0,2);
    out_quest ((char *) &a_kun.geb_fill,1,0);
    out_quest ((char *) &a_kun.geb_anz,2,0);
    out_quest ((char *) a_kun.pal_eti,0,2);
    out_quest ((char *) &a_kun.pal_fill,1,0);
    out_quest ((char *) &a_kun.pal_anz,2,0);
    out_quest ((char *) a_kun.pos_eti,0,2);
    out_quest ((char *) &a_kun.sg1,1,0);
    out_quest ((char *) &a_kun.sg2,1,0);
    out_quest ((char *) &a_kun.pos_fill,1,0);
    out_quest ((char *) &a_kun.ausz_art,1,0);
    out_quest ((char *) &a_kun.text_nr2,2,0);
    out_quest ((char *) &a_kun.cab,1,0);
    out_quest ((char *) a_kun.a_bz3,0,25);
    out_quest ((char *) a_kun.a_bz4,0,25);
         cursor = prepare_sql ("select a_kun.mdn,  a_kun.fil,  "
"a_kun.kun,  a_kun.a,  a_kun.a_kun,  a_kun.a_bz1,  a_kun.me_einh_kun,  "
"a_kun.inh,  a_kun.kun_bran2,  a_kun.tara,  a_kun.ean,  a_kun.a_bz2,  "
"a_kun.hbk_ztr,  a_kun.kopf_text,  a_kun.pr_rech_kz,  a_kun.modif,  "
"a_kun.text_nr,  a_kun.devise,  a_kun.geb_eti,  a_kun.geb_fill,  "
"a_kun.geb_anz,  a_kun.pal_eti,  a_kun.pal_fill,  a_kun.pal_anz,  "
"a_kun.pos_eti,  a_kun.sg1,  a_kun.sg2,  a_kun.pos_fill,  a_kun.ausz_art,  "
"a_kun.text_nr2,  a_kun.cab,  a_kun.a_bz3,  a_kun.a_bz4 from a_kun "

#line 269 "a_kun.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   a   = ? ");

         ins_quest ((char *) &a_kun.mdn, 1, 0);
         ins_quest ((char *) &a_kun.fil, 1, 0);  
         ins_quest ((char *) a_kun.kun_bran2, 0, 3);
         ins_quest ((char *) &a_kun.a, 3, 0); 

    out_quest ((char *) &a_kun.mdn,1,0);
    out_quest ((char *) &a_kun.fil,1,0);
    out_quest ((char *) &a_kun.kun,2,0);
    out_quest ((char *) &a_kun.a,3,0);
    out_quest ((char *) a_kun.a_kun,0,13);
    out_quest ((char *) a_kun.a_bz1,0,25);
    out_quest ((char *) &a_kun.me_einh_kun,1,0);
    out_quest ((char *) &a_kun.inh,3,0);
    out_quest ((char *) a_kun.kun_bran2,0,3);
    out_quest ((char *) &a_kun.tara,3,0);
    out_quest ((char *) &a_kun.ean,3,0);
    out_quest ((char *) a_kun.a_bz2,0,25);
    out_quest ((char *) &a_kun.hbk_ztr,1,0);
    out_quest ((char *) &a_kun.kopf_text,2,0);
    out_quest ((char *) a_kun.pr_rech_kz,0,2);
    out_quest ((char *) a_kun.modif,0,2);
    out_quest ((char *) &a_kun.text_nr,2,0);
    out_quest ((char *) &a_kun.devise,1,0);
    out_quest ((char *) a_kun.geb_eti,0,2);
    out_quest ((char *) &a_kun.geb_fill,1,0);
    out_quest ((char *) &a_kun.geb_anz,2,0);
    out_quest ((char *) a_kun.pal_eti,0,2);
    out_quest ((char *) &a_kun.pal_fill,1,0);
    out_quest ((char *) &a_kun.pal_anz,2,0);
    out_quest ((char *) a_kun.pos_eti,0,2);
    out_quest ((char *) &a_kun.sg1,1,0);
    out_quest ((char *) &a_kun.sg2,1,0);
    out_quest ((char *) &a_kun.pos_fill,1,0);
    out_quest ((char *) &a_kun.ausz_art,1,0);
    out_quest ((char *) &a_kun.text_nr2,2,0);
    out_quest ((char *) &a_kun.cab,1,0);
    out_quest ((char *) a_kun.a_bz3,0,25);
    out_quest ((char *) a_kun.a_bz4,0,25);
         cursor_bra = prepare_sql ("select a_kun.mdn,  "
"a_kun.fil,  a_kun.kun,  a_kun.a,  a_kun.a_kun,  a_kun.a_bz1,  "
"a_kun.me_einh_kun,  a_kun.inh,  a_kun.kun_bran2,  a_kun.tara,  a_kun.ean,  "
"a_kun.a_bz2,  a_kun.hbk_ztr,  a_kun.kopf_text,  a_kun.pr_rech_kz,  "
"a_kun.modif,  a_kun.text_nr,  a_kun.devise,  a_kun.geb_eti,  "
"a_kun.geb_fill,  a_kun.geb_anz,  a_kun.pal_eti,  a_kun.pal_fill,  "
"a_kun.pal_anz,  a_kun.pos_eti,  a_kun.sg1,  a_kun.sg2,  a_kun.pos_fill,  "
"a_kun.ausz_art,  a_kun.text_nr2,  a_kun.cab,  a_kun.a_bz3,  a_kun.a_bz4 from a_kun "

#line 280 "a_kun.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
							   "and   kun = 0 "
                               "and   kun_bran2 = ? "
                               "and   a   = ? ");

    ins_quest ((char *) &a_kun.mdn,1,0);
    ins_quest ((char *) &a_kun.fil,1,0);
    ins_quest ((char *) &a_kun.kun,2,0);
    ins_quest ((char *) &a_kun.a,3,0);
    ins_quest ((char *) a_kun.a_kun,0,13);
    ins_quest ((char *) a_kun.a_bz1,0,25);
    ins_quest ((char *) &a_kun.me_einh_kun,1,0);
    ins_quest ((char *) &a_kun.inh,3,0);
    ins_quest ((char *) a_kun.kun_bran2,0,3);
    ins_quest ((char *) &a_kun.tara,3,0);
    ins_quest ((char *) &a_kun.ean,3,0);
    ins_quest ((char *) a_kun.a_bz2,0,25);
    ins_quest ((char *) &a_kun.hbk_ztr,1,0);
    ins_quest ((char *) &a_kun.kopf_text,2,0);
    ins_quest ((char *) a_kun.pr_rech_kz,0,2);
    ins_quest ((char *) a_kun.modif,0,2);
    ins_quest ((char *) &a_kun.text_nr,2,0);
    ins_quest ((char *) &a_kun.devise,1,0);
    ins_quest ((char *) a_kun.geb_eti,0,2);
    ins_quest ((char *) &a_kun.geb_fill,1,0);
    ins_quest ((char *) &a_kun.geb_anz,2,0);
    ins_quest ((char *) a_kun.pal_eti,0,2);
    ins_quest ((char *) &a_kun.pal_fill,1,0);
    ins_quest ((char *) &a_kun.pal_anz,2,0);
    ins_quest ((char *) a_kun.pos_eti,0,2);
    ins_quest ((char *) &a_kun.sg1,1,0);
    ins_quest ((char *) &a_kun.sg2,1,0);
    ins_quest ((char *) &a_kun.pos_fill,1,0);
    ins_quest ((char *) &a_kun.ausz_art,1,0);
    ins_quest ((char *) &a_kun.text_nr2,2,0);
    ins_quest ((char *) &a_kun.cab,1,0);
    ins_quest ((char *) a_kun.a_bz3,0,25);
    ins_quest ((char *) a_kun.a_bz4,0,25);
         sqltext = "update a_kun set a_kun.mdn = ?,  "
"a_kun.fil = ?,  a_kun.kun = ?,  a_kun.a = ?,  a_kun.a_kun = ?,  "
"a_kun.a_bz1 = ?,  a_kun.me_einh_kun = ?,  a_kun.inh = ?,  "
"a_kun.kun_bran2 = ?,  a_kun.tara = ?,  a_kun.ean = ?,  "
"a_kun.a_bz2 = ?,  a_kun.hbk_ztr = ?,  a_kun.kopf_text = ?,  "
"a_kun.pr_rech_kz = ?,  a_kun.modif = ?,  a_kun.text_nr = ?,  "
"a_kun.devise = ?,  a_kun.geb_eti = ?,  a_kun.geb_fill = ?,  "
"a_kun.geb_anz = ?,  a_kun.pal_eti = ?,  a_kun.pal_fill = ?,  "
"a_kun.pal_anz = ?,  a_kun.pos_eti = ?,  a_kun.sg1 = ?,  a_kun.sg2 = ?,  "
"a_kun.pos_fill = ?,  a_kun.ausz_art = ?,  a_kun.text_nr2 = ?,  "
"a_kun.cab = ?,  a_kun.a_bz3 = ?,  a_kun.a_bz4 = ? "

#line 286 "a_kun.rpp"
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_bran2 = ? "
                               "and   a   = ? ";
  
         ins_quest ((char *) &a_kun.mdn, 1, 0);
         ins_quest ((char *) &a_kun.fil, 1, 0);  
         ins_quest ((char *) &a_kun.kun, 2, 0);
         ins_quest ((char *) a_kun.kun_bran2, 0, 2);
         ins_quest ((char *) &a_kun.a, 3, 0); 

         upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &a_kun.mdn,1,0);
    ins_quest ((char *) &a_kun.fil,1,0);
    ins_quest ((char *) &a_kun.kun,2,0);
    ins_quest ((char *) &a_kun.a,3,0);
    ins_quest ((char *) a_kun.a_kun,0,13);
    ins_quest ((char *) a_kun.a_bz1,0,25);
    ins_quest ((char *) &a_kun.me_einh_kun,1,0);
    ins_quest ((char *) &a_kun.inh,3,0);
    ins_quest ((char *) a_kun.kun_bran2,0,3);
    ins_quest ((char *) &a_kun.tara,3,0);
    ins_quest ((char *) &a_kun.ean,3,0);
    ins_quest ((char *) a_kun.a_bz2,0,25);
    ins_quest ((char *) &a_kun.hbk_ztr,1,0);
    ins_quest ((char *) &a_kun.kopf_text,2,0);
    ins_quest ((char *) a_kun.pr_rech_kz,0,2);
    ins_quest ((char *) a_kun.modif,0,2);
    ins_quest ((char *) &a_kun.text_nr,2,0);
    ins_quest ((char *) &a_kun.devise,1,0);
    ins_quest ((char *) a_kun.geb_eti,0,2);
    ins_quest ((char *) &a_kun.geb_fill,1,0);
    ins_quest ((char *) &a_kun.geb_anz,2,0);
    ins_quest ((char *) a_kun.pal_eti,0,2);
    ins_quest ((char *) &a_kun.pal_fill,1,0);
    ins_quest ((char *) &a_kun.pal_anz,2,0);
    ins_quest ((char *) a_kun.pos_eti,0,2);
    ins_quest ((char *) &a_kun.sg1,1,0);
    ins_quest ((char *) &a_kun.sg2,1,0);
    ins_quest ((char *) &a_kun.pos_fill,1,0);
    ins_quest ((char *) &a_kun.ausz_art,1,0);
    ins_quest ((char *) &a_kun.text_nr2,2,0);
    ins_quest ((char *) &a_kun.cab,1,0);
    ins_quest ((char *) a_kun.a_bz3,0,25);
    ins_quest ((char *) a_kun.a_bz4,0,25);
         ins_cursor = prepare_sql ("insert into a_kun (mdn,  "
"fil,  kun,  a,  a_kun,  a_bz1,  me_einh_kun,  inh,  kun_bran2,  tara,  ean,  a_bz2,  hbk_ztr,  "
"kopf_text,  pr_rech_kz,  modif,  text_nr,  devise,  geb_eti,  geb_fill,  geb_anz,  "
"pal_eti,  pal_fill,  pal_anz,  pos_eti,  sg1,  sg2,  pos_fill,  ausz_art,  text_nr2,  "
"cab,  a_bz3,  a_bz4) "

#line 301 "a_kun.rpp"
                                   "values "
                                   "(?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

#line 303 "a_kun.rpp"

         ins_quest ((char *) &a_kun.mdn, 1, 0);
         ins_quest ((char *) &a_kun.fil, 1, 0);  
         ins_quest ((char *) &a_kun.kun, 2, 0);
         ins_quest ((char *) a_kun.kun_bran2, 0, 2);
         ins_quest ((char *) &a_kun.a, 3, 0); 
         del_cursor = prepare_sql ("delete from a_kun "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_bran2 = ? "
                               "and   a   = ? ");

         ins_quest ((char *) &a_kun.mdn, 1, 0);
         ins_quest ((char *) &a_kun.fil, 1, 0);  
         ins_quest ((char *) &a_kun.kun, 2, 0);
         ins_quest ((char *) a_kun.kun_bran2, 0, 3);
         ins_quest ((char *) &a_kun.a, 3, 0); 
         test_upd_cursor = prepare_sql ("select a from a_kun "
                               "where mdn = ? "
                               "and   fil = ? "
                               "and   kun = ? "
                               "and   kun_bran2 = ? "
                               "and   a   = ? ");
}


 
int A_KUN_CLASS::prep_awcursor (void)
/**
Cursor fuer Auswahl vorbereiten.
**/
{
         int cursor_ausw;

         out_quest ((char *) &a_kun.mdn, 1, 0);
         out_quest ((char *) &a_kun.fil, 1, 0);
         out_quest ((char *) &a_kun.kun, 2, 0);
         out_quest ((char *) &a_kun.a, 3, 0);
         out_quest ((char *) a_kun.kun_bran2, 3, 0);

         cursor_ausw = prepare_scroll ("select mdn, fil, kun, a, kun_bran2 from a_kun "
                                       "where kun > 0 "
                                       "order by kun,a");
         if (sqlstatus)
         {
                     return (-1);
         }
         return (cursor_ausw);
}
int A_KUN_CLASS::prep_awcursor (char *sqlstring)
/**
Cursor fuer Auswahl vorbereiten.
**/
{
         int cursor_ausw;


         out_quest ((char *) &a_kun.mdn, 1, 0);
         out_quest ((char *) &a_kun.fil, 1, 0);
         out_quest ((char *) &a_kun.kun, 2, 0);
         out_quest ((char *) &a_kun.a, 3, 0);
         out_quest ((char *) a_kun.kun_bran2, 0, 2);

         cursor_ausw = prepare_scroll (sqlstring);
         if (sqlstatus)
         {
                     return (-1);
         }
         return (cursor_ausw);
}


int A_KUN_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int A_KUN_CLASS::dbreadfirst_bra (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         open_sql (cursor_bra);
         fetch_sql (cursor_bra);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int A_KUN_CLASS::dbread_bra (void)
/**
Naechsten Satz aus Tabelle lesen.
**/
{
         fetch_sql (cursor_bra);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}
 
void A_KUN_CLASS::fill_aw (int cursor_ausw)
/**
Struktur fuer Auswahl fuellen.
**/
{
 
		SortMalloc ();
        sort_awanz = 0; 
	  while (fetch_scroll (cursor_ausw, NEXT) == 0)
        {
			       sprintf (sort_awtab[sort_awanz].sort1, "%ld",
						      a_kun.kun);
			       sprintf (sort_awtab[sort_awanz].sort2, "%s",
						      a_kun.kun_bran2);
                         if (a_kun.kun > 0)
                         {  
                                 this->KUN_CLASS::lese_kun (a_kun.mdn, a_kun.fil, a_kun.kun);
 			               sprintf (sort_awtab[sort_awanz].sort3, "%s",
						      kun.kun_krz1);
                                 if (sqlstatus != 0)
                                 { 
                                            strcpy (sort_awtab[sort_awanz].sort3, "");
                                 } 
                         }
                         else
                         {
                                 strcpy (sort_awtab[sort_awanz].sort3, "");
                         }

			        sprintf (sort_awtab[sort_awanz].sort4, "%13.0lf",
						     a_kun.a);
                          lese_a_bas (a_kun.a);
				  strcpy (sort_awtab[sort_awanz].sort5, 
						     _a_bas.a_bz1);
				  sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
				  sort_awanz ++;
                          if (sort_awanz == MAXSORT) break;
        }
        return;
}


int A_KUN_CLASS::ShowAllBu (HWND hWnd, int ws_flag)
/**
Auswahl ueber Gruppen anzeigen.
**/
{
         int cursor_ausw;

         cursor_ausw = prep_awcursor ();
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         fill_aw (cursor_ausw);

         this->DB_CLASS::ShowAllBu (hWnd,
                                    ws_flag,
                                    cursor_ausw, 
                                    endsort,  
                                    IsAwClck,
                                    (char *) sort_awtab,
                                    sort_awanz,
                                    (char *) &sort_aw,
                                    (int) sizeof (struct SORT_AW),
                                    &fsort_aw,  
                                    &fsort_awl,
                                    &fsort_awub);
	  sort_idx = sort_awtab[sort_idx].sortidx;
        fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
        close_sql (cursor_ausw);
        return (0);
}

int A_KUN_CLASS::ShowBuQuery (HWND hWnd, int ws_flag, 
								           char *sqlstring )
/**
Auswahl ueber Gruppen anzeigen.
**/
{
         int cursor_ausw;

         cursor_ausw = prep_awcursor (sqlstring);
         if (cursor_ausw == -1)
         {
                       return (-1);
         }
         fill_aw (cursor_ausw);

		 if (fsort_aw.mask[0].item == NULL)
		 {
		            disp_mess ("Fehler", 2);
         }

         this->DB_CLASS::ShowAllBu (hWnd,
                                    ws_flag,
                                    cursor_ausw, 
                                    endsort,  
                                    IsAwClck,
                                    (char *) sort_awtab,
                                    sort_awanz,
                                    (char *) &sort_aw,
                                    (int) sizeof (struct SORT_AW),
                                    &fsort_aw,  
                                    &fsort_awl,
                                    &fsort_awub);
        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_ausw);
                     return 1;
        }
	    sort_idx = sort_awtab[sort_idx].sortidx;
        fetch_scroll (cursor_ausw, DBABSOLUTE, sort_idx);
        close_sql (cursor_ausw);
        return (0);
}


int A_KUN_CLASS::QueryBu (HWND hWnd, int ws_flag)
/**
Auswahl mit Query-String.
**/
{
	     char sqlstring [1000]; 

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select mdn, fil, kun, a, kun_bran2 "
							"from a_kun "
                            "where kun  > 0 and %s "
							"order by kun,a", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                            "select mdn, fil, kun, a, kun_bran2 "
							"from a_kun "
                            "where kun  > 0 order by kun");
         }
         return this->ShowBuQuery (hWnd, ws_flag, sqlstring);
}

