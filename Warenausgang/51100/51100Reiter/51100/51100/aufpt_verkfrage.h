#ifndef _AUFPT_VERKFRAGE_DEF
#define _AUFPT_VERKFRAGE_DEF
#include "dbclass.h"

struct AUFPT_VERKFRAGE {
   short     mdn;
   long      auf;
   long      pos_id;
   char      frage1[257];
   char      antwort1[257];
   char      frage2[257];
   char      antwort2[257];
   char      frage3[257];
   char      antwort3[257];
   char      frage4[257];
   char      antwort4[257];
   char      frage5[257];
   char      antwort5[257];
   char      frage6[257];
   char      antwort6[257];
   char      frage7[257];
   char      antwort7[257];
   char      frage8[257];
   char      antwort8[257];
   char      frage9[257];
   char      antwort9[257];
   char      frage10[257];
   char      antwort10[257];
};
extern struct AUFPT_VERKFRAGE aufpt_verkfrage, aufpt_verkfrage_null;

#line 6 "aufpt_verkfrage.rh"

class CAufpt_verkfrage : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               CAufpt_verkfrage () : DB_CLASS ()
               {
               }
};
#endif
