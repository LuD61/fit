// AdrDialog.cpp: Implementierung der Klasse CAdrDialog.
//
//////////////////////////////////////////////////////////////////////

#include "AdrDialog.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

static KUN_CLASS Kun;


CAdrDialog::CAdrDialog(HWND Parent) : 
			CDialog (Id, Parent)
{
	m_DlgColor  = RGB (255, 255, 204);
	m_DlgBrush  = NULL;
	m_EditColor = RGB (255, 255, 255);
	m_ColorFreiTxt = RGB (120, 120, 120);
	m_ColorPrStu = RGB (0, 0, 255);
	m_ColorZahlArt = RGB (0, 0, 255);
	m_AdrNr = 0l;
	
}

CAdrDialog::~CAdrDialog()
{

}

void CAdrDialog::ReadAdr ()
{
	memcpy (&m_Adr.adr, &_adr, sizeof (ADR));
	if (m_AdrNr != 0l)
	{
		m_Adr.adr.adr = m_AdrNr;
		m_Adr.dbreadfirst ();
	}
}

void CAdrDialog::AttachControls ()
{
    ReadAdr ();
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_NAME,      m_Adr.adr.adr_nam1,  CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_FIRSTNAME, m_Adr.adr.adr_nam2,  CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_STREET,    m_Adr.adr.adr_nam2,  CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_PLZ,       m_Adr.adr.plz,       CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_VILLAGE,   m_Adr.adr.ort1,      CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_PHONE,     m_Adr.adr.tel,       CDialogItem::String, 20)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_FAX,       m_Adr.adr.fax,       CDialogItem::String, 20)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_MAIL,      m_Adr.adr.email,     CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_FREI_TXT2, (LPSTR) m_FreiTxt2,  CDialogItem::String, 65)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_PR_STU,    (LPSTR) m_PrStu,     CDialogItem::String, 37)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_ZAHL_ART,  (LPSTR) m_ZahlArt,   CDialogItem::String, 37)); 
//	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_PFAND,     (LPSTR) m_Pfand_ber, CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_PFAND,     m_Pfand_ber,         CDialogItem::String, 36)); 
}

BOOL CAdrDialog::OnInitDialog ()
{
	BOOL ret = CDialog::OnInitDialog ();

	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_NAME,      m_Adr.adr.adr_nam1,  CDialogItem::String, 36)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_FIRSTNAME, m_Adr.adr.adr_nam2,  CDialogItem::String, 36)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_STREET,    m_Adr.adr.adr_nam2,  CDialogItem::String, 36)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_PLZ,       m_Adr.adr.plz,       CDialogItem::String, 36)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_VILLAGE,   m_Adr.adr.ort1,      CDialogItem::String, 36)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_PHONE,     m_Adr.adr.tel,       CDialogItem::String, 20)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_FAX,       m_Adr.adr.fax,       CDialogItem::String, 20)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_MAIL,      m_Adr.adr.email,     CDialogItem::String, 36)); 
//	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_PFAND,     (LPSTR) m_Pfand_ber, CDialogItem::String, 36)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_PFAND,     m_Pfand_ber,         CDialogItem::String, 36)); 


	CFont font;
	m_FatFont.Create ("Arial", 16, TRUE);
	SetFont (IDC_FREI_TXT2, m_FatFont);
	SetFont (IDC_PR_STU, m_FatFont);
	SetFont (IDC_ZAHL_ART, m_FatFont);


	Display ();
	return TRUE;
}

BOOL CAdrDialog::OnF5 ()
{
	OnCancel ();
	return TRUE;
}

BOOL CAdrDialog::PreTranslateMessage (MSG *msg)
{
	switch (msg->message)
	{
	case WM_KEYDOWN:
		switch (msg->wParam)
		{
		case VK_F5 :
			return OnF5 ();
		case VK_RETURN :
			OnOK ();
			return TRUE; 
		}
	}
	return FALSE;
}

HBRUSH CAdrDialog::OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor)
{
	if (m_DlgBrush == NULL)
	{
		m_DlgBrush = CreateSolidBrush (m_DlgColor);
	}
	if (m_EditBrush == NULL)
	{
		m_EditBrush = CreateSolidBrush (m_EditColor);
	}
	if (nCtlColor == CTLCOLOR_DLG)
	{
			SetBkColor (hDC, m_DlgColor);
			return m_DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (hWnd == GethWnd (IDC_FREI_TXT2))
		{
			SetTextColor (hDC, m_ColorFreiTxt);
		}
		else if (hWnd == GethWnd (IDC_PR_STU))
		{
			SetTextColor (hDC, m_ColorPrStu);
		}
		else if (hWnd == GethWnd (IDC_ZAHL_ART))
		{
			SetTextColor (hDC, m_ColorZahlArt);
		}
		else if (hWnd == GethWnd (IDC_PFAND))
		{
			if (kun.pfand_ber == 0)
			{
				SetTextColor (hDC, m_ColorFreiTxt);
			}
			else
			{
				SetTextColor (hDC, m_ColorPrStu);
			}
		}
		if (IsEditItem (hWnd))
		{
			SetBkColor (hDC, m_EditColor);
			SetBkMode (hDC, OPAQUE);
			return m_EditBrush;
		}
		else
		{
			SetBkColor (hDC, m_DlgColor);
			SetBkMode (hDC, TRANSPARENT);
			return m_DlgBrush;
		}
	}
	return NULL;
}

BOOL CAdrDialog::IsEditItem (HWND hWnd)
{
	BOOL ret = FALSE;

	CDialogItem *item;
	for (vector<CDialogItem *>::iterator it = m_EditItems.begin (); it != m_EditItems.end (); ++it)
	{
		item = *it;
		if (item != NULL)
		{
			if (item->IsControl (hWnd))
			{
				ret = TRUE;
			}
		}
	}
	return ret;
}
