// ListHandler.cpp: Implementierung der Klasse CListHandler.
//
//////////////////////////////////////////////////////////////////////
#include <string>
#include "strfkt.h"
#include "ListHandler.h"


//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

struct AUFP_POSI aufp_posi;

int CListHandler::CIdxCollection::GetIdxAll (int IdxValue)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxValue == IdxValue)
		{
			return Item->IdxAll;
		}
	}
	return -1;
}

int CListHandler::CIdxCollection::GetIdxValue (int IdxAll)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxAll == IdxAll)
		{
			return Item->IdxValue;
		}
	}
	return -1;
}


CListHandler::CIdxItem *CListHandler::CIdxCollection::GetItemByAll (int IdxAll)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxAll == IdxAll)
		{
			return Item;
		}
	}
	return NULL;
}

CListHandler::CIdxItem *CListHandler::CIdxCollection::GetItemByValue (int IdxValue)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxValue == IdxValue)
		{
			return Item;
		}
	}
	return NULL;
}


CListHandler *CListHandler::instance = NULL;

CListHandler *CListHandler::GetInstance ()
{
	if (instance == NULL)
	{
		instance = new CListHandler ();
	}
	return instance;
}

void CListHandler::DestroyInstance ()
{
	if (instance != NULL)
	{
		delete instance;
	}
}

CListHandler::CListHandler()
{
	ActiveMode = AllEntries;
	LoadComplete = FALSE;
	enableActivate = FALSE;
	activated = FALSE;
	activated_lBest = FALSE; //LAC-9
	activated_KritMhd = FALSE; //LAC-84
	activated_Kommldat = FALSE; //LAC-116
	activated_BestVorschlag = FALSE; 
	activated_HWG1 = FALSE; //LAC-9
	activated_HWG2 = FALSE; //LAC-9
	activated_HWG3 = FALSE; //LAC-9
	activated_HWG4 = FALSE; //LAC-9
	activated_HWG5 = FALSE; //LAC-9
	activated_HWG6 = FALSE; //LAC-9
	activated_HWG7 = FALSE; //LAC-9
	activated_HWG8 = FALSE; //LAC-9
	activated_HWG9 = FALSE; //LAC-9
	activated_HWG10 = FALSE; //LAC-9
	activated_HWG11 = FALSE; 
	activated_HWG12 = FALSE; 
	activated_HWG13 = FALSE; 
	activated_HWG14 = FALSE; 
	activated_HWG15 = FALSE; 
	activated_HWG16 = FALSE; 
	m_SmtReader = NULL;
}

CListHandler::~CListHandler()
{
	Init (0);
}

void CListHandler::Init (int dx)
{
	if (dx == 0)
	{
		StndAll.DestroyElements ();
		StndAll.Destroy ();
		lBestAll.DestroyElements (); //LAC-9
		lBestAll.Destroy (); //LAC-9
		KritMhdAll.DestroyElements (); //LAC-84
		KritMhdAll.Destroy (); //LAC-84
		KommldatAll.DestroyElements (); //LAC-116
		KommldatAll.Destroy (); //LAC-116
		BestVorschlagAll.DestroyElements (); //LAC-116
		BestVorschlagAll.Destroy (); //LAC-116
		HWG1All.DestroyElements (); //LAC-9
		HWG1All.Destroy (); //LAC-9
		HWG2All.DestroyElements (); //LAC-9
		HWG2All.Destroy (); //LAC-9
		HWG3All.DestroyElements (); //LAC-9
		HWG3All.Destroy (); //LAC-9
		HWG4All.DestroyElements (); //LAC-9
		HWG4All.Destroy (); //LAC-9
		HWG5All.DestroyElements (); //LAC-9
		HWG5All.Destroy (); //LAC-9
		HWG6All.DestroyElements (); //LAC-9
		HWG6All.Destroy (); //LAC-9
		HWG7All.DestroyElements (); //LAC-9
		HWG7All.Destroy (); //LAC-9
		HWG8All.DestroyElements (); //LAC-9
		HWG8All.Destroy (); //LAC-9
		HWG9All.DestroyElements (); //LAC-9
		HWG9All.Destroy (); //LAC-9
		HWG10All.DestroyElements (); //LAC-9
		HWG10All.Destroy (); 
		HWG11All.DestroyElements (); 
		HWG11All.Destroy (); 
		HWG12All.DestroyElements (); 
		HWG12All.Destroy (); 
		HWG13All.DestroyElements (); 
		HWG13All.Destroy (); 
		HWG14All.DestroyElements (); 
		HWG14All.Destroy (); 
		HWG15All.DestroyElements (); 
		HWG15All.Destroy (); 
		HWG16All.DestroyElements (); 
		HWG16All.Destroy (); 
		StndValue.DestroyElements ();
		StndValue.Destroy ();
		IdxCollection.DestroyElements ();
		IdxCollection.Destroy ();
		LoadComplete = FALSE;
		enableActivate = FALSE;
		activated = FALSE;
		activated_lBest = FALSE; //LAC-9
		activated_KritMhd = FALSE; //LAC-84
		activated_Kommldat = FALSE; //LAC-116
		activated_BestVorschlag = FALSE; 
		activated_HWG1 = FALSE; //LAC-9
		activated_HWG2 = FALSE; //LAC-9
		activated_HWG3 = FALSE; //LAC-9
		activated_HWG4 = FALSE; //LAC-9
		activated_HWG5 = FALSE; //LAC-9
		activated_HWG6 = FALSE; //LAC-9
		activated_HWG7 = FALSE; //LAC-9
		activated_HWG8 = FALSE; //LAC-9
		activated_HWG9 = FALSE; //LAC-9
		activated_HWG10 = FALSE; //LAC-9
		activated_HWG11 = FALSE; 
		activated_HWG12 = FALSE; 
		activated_HWG13 = FALSE; 
		activated_HWG14 = FALSE; 
		activated_HWG15 = FALSE; 
		activated_HWG16 = FALSE; 
	}
	if (dx == REITERSTANDARDBESTELLUNG)
	{
		StndAll.DestroyElements ();
		StndAll.Destroy ();
		activated = FALSE;
	}
	if (dx == REITERBESTELLVORSCHLAG)
	{
		BestVorschlagAll.DestroyElements ();
		BestVorschlagAll.Destroy ();
		activated_BestVorschlag = FALSE;
	}
	if (dx == REITERLETZTEBESTELLUNG)
	{
		lBestAll.DestroyElements (); //LAC-9
		lBestAll.Destroy (); //LAC-9
		activated_lBest = FALSE; //LAC-9
	}
	if (dx == REITERKRITISCHEMHD) //LAC-84
	{
		KritMhdAll.DestroyElements (); 
		KritMhdAll.Destroy ();
		activated_KritMhd = FALSE; 
	}
	if (dx == REITERKOMMLDAT) //LAC-116
	{
		KommldatAll.DestroyElements (); 
		KommldatAll.Destroy ();
		activated_Kommldat = FALSE; 
	}
	if (dx == REITERHWG1)
	{
		HWG1All.DestroyElements (); //LAC-9
		HWG1All.Destroy (); //LAC-9
		activated_HWG1 = FALSE; //LAC-9
	}
	if (dx == REITERHWG2)
	{
		HWG2All.DestroyElements (); //LAC-9
		HWG2All.Destroy (); //LAC-9
		activated_HWG2 = FALSE; //LAC-9
	}
	if (dx == REITERHWG3)
	{
		HWG3All.DestroyElements (); //LAC-9
		HWG3All.Destroy (); //LAC-9
		activated_HWG3 = FALSE; //LAC-9
	}
	if (dx == REITERHWG4)
	{
		HWG4All.DestroyElements (); //LAC-9
		HWG4All.Destroy (); //LAC-9
		activated_HWG4 = FALSE; //LAC-9
	}
	if (dx == REITERHWG5)
	{
		HWG5All.DestroyElements (); //LAC-9
		HWG5All.Destroy (); //LAC-9
		activated_HWG5 = FALSE; //LAC-9
	}
	if (dx == REITERHWG6)
	{
		HWG6All.DestroyElements (); //LAC-9
		HWG6All.Destroy (); //LAC-9
		activated_HWG6 = FALSE; //LAC-9
	}
	if (dx == REITERHWG7)
	{
		HWG7All.DestroyElements (); //LAC-9
		HWG7All.Destroy (); //LAC-9
		activated_HWG7 = FALSE; //LAC-9
	}
	if (dx == REITERHWG8)
	{
		HWG8All.DestroyElements (); //LAC-9
		HWG8All.Destroy (); //LAC-9
		activated_HWG8 = FALSE; //LAC-9
	}
	if (dx == REITERHWG9)
	{
		HWG9All.DestroyElements (); //LAC-9
		HWG9All.Destroy (); //LAC-9
		activated_HWG9 = FALSE; //LAC-9
	}
	if (dx == REITERHWG10)
	{
		HWG10All.DestroyElements (); //LAC-9
		HWG10All.Destroy (); //LAC-9
		activated_HWG10 = FALSE; //LAC-9
	}
	if (dx == REITERHWG11)
	{
		HWG10All.DestroyElements (); 
		HWG10All.Destroy (); 
		activated_HWG11 = FALSE;
	}
	if (dx == REITERHWG12)
	{
		HWG10All.DestroyElements (); 
		HWG10All.Destroy (); 
		activated_HWG12 = FALSE;
	}
	if (dx == REITERHWG13)
	{
		HWG10All.DestroyElements (); 
		HWG10All.Destroy (); 
		activated_HWG13 = FALSE;
	}
	if (dx == REITERHWG14)
	{
		HWG10All.DestroyElements (); 
		HWG10All.Destroy (); 
		activated_HWG14 = FALSE;
	}
	if (dx == REITERHWG15)
	{
		HWG10All.DestroyElements (); 
		HWG10All.Destroy (); 
		activated_HWG15 = FALSE;
	}
	if (dx == REITERHWG16)
	{
		HWG10All.DestroyElements (); 
		HWG10All.Destroy (); 
		activated_HWG16 = FALSE;
	}
}

void CListHandler::InitAufMe ()
{
	AUFPS **it;
	AUFPS *Item;

	StndAll.Start ();
	while ((it = StndAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			strcpy (Item->auf_me, "0.000");
		}
	}
	activated = FALSE;
}

void CListHandler::SetNewPosi () //LAC-9
{
	AUFPS **it;
	AUFPS *Item;

	int dposi = 10;
	StndValue.Start ();
	while ((it = StndValue.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			Item->posi_old = atoi(Item->posi); //LAC-124b 
			sprintf (Item->posi, "%ld", dposi);
			dposi += 10;
		}
	}
}


BOOL CListHandler::WriteNewPosi (short dmdn, short dfil ,int dauf) //LAC-124b
{
	AUFPS **it;
	AUFPS *Item;

	int dposi = 10;
	StndValue.Start ();
	while ((it = StndValue.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (Item->posi_old != atoi(Item->posi))
			{
				aufp_posi.a = ratod(Item->a);
				aufp_posi.posi = atoi (Item->posi);
				aufp_posi.posi_old = Item->posi_old;
				Item->posi_old = atoi(Item->posi); 
				return TRUE;
			}
		}
	}
	return FALSE;
}



int  CListHandler::Activate (AUFPS aufparr[MAXPOS], int aufpanz, int *idx)
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int i = 0;
	ListCount = StndAll.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA (Aufps);
		}
	}
	/****
	for (i = 0; i < aufpanz; i ++) 
	{
		SetStndAllA (&aufparr[i]);
	}
	***/
	ListCount = StndAll.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = StndAll.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); //260312
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}


			}
			memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
		}
	}
    activated = TRUE;
	return ListCount;
}


void CListHandler::InitElementslBestAll (void) //LAC-132
{
	lBestAll.DestroyElements ();
}

int  CListHandler::Activate_lBest (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dauf) //LAC-9 //LAC-132 dauf
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int i = 0;
	ListCount = lBestAll.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}
	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_lBest (Aufps);
		}
	}


	ListCount = lBestAll.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = lBestAll.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_lBest == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); //260312
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			memcpy (&aufparr[i], Aufps, sizeof (AUFPS));

		}
	}
    activated_lBest = TRUE;
	return ListCount;
}


int  CListHandler::Activate_KritMhd (AUFPS aufparr[MAXPOS], int aufpanz, int *idx) //LAC-84
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int i = 0;
	ListCount = KritMhdAll.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}
	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_KritMhd (Aufps);
		}
	}


	ListCount = KritMhdAll.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = KritMhdAll.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_KritMhd == FALSE) 
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); //260312
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
				}

			}
			memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
		}
	}
    activated_KritMhd = TRUE;
	return ListCount;
}

int  CListHandler::Activate_Kommldat (AUFPS aufparr[MAXPOS], int aufpanz, int *idx) //LAC-116
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int i = 0;
	ListCount = KommldatAll.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}
	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_Kommldat (Aufps);
		}
	}


	ListCount = KommldatAll.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = KommldatAll.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_Kommldat == FALSE) 
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); //260312
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
				}

			}
			memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
		}
	}
    activated_Kommldat = TRUE;
	return ListCount;
}


int  CListHandler::Activate_BestVorschlag (AUFPS aufparr[MAXPOS], int aufpanz, int *idx) 
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int i = 0;
	ListCount = BestVorschlagAll.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}
	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_BestVorschlag (Aufps);
		}
	}


	ListCount = BestVorschlagAll.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = BestVorschlagAll.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_BestVorschlag == FALSE) 
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); //260312
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
				}

			}
			memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
		}
	}
    activated_BestVorschlag = TRUE;
	return ListCount;
}



int  CListHandler::Activate_HWG1 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) //LAC-9
{
	AUFPS **it;
	AUFPS *Aufps;
	int dlistidx = 0;
	int ListCount = 0;
	int i = 0 ;
	ListCount = HWG1All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}
	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG1 (Aufps);
		}
	}


//	ListCount = HWG1All.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG1All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG1 == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG1 = TRUE;
	return dlistidx;
}

int  CListHandler::Activate_HWG2 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) //LAC-9
{
	AUFPS **it;
	AUFPS *Aufps;
	int dlistidx = 0;
	int ListCount = 0;
	int i = 0 ;
	ListCount = HWG2All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG2 (Aufps);
		}
	}


	ListCount = HWG2All.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG2All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG2 == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG2 = TRUE;
	return dlistidx;
}

int  CListHandler::Activate_HWG3 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) //LAC-9
{
	AUFPS **it;
	AUFPS *Aufps;
	int dlistidx = 0;
	int ListCount = 0;
	int i = 0 ;
	ListCount = HWG3All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG3 (Aufps);
		}
	}

//	ListCount = HWG3All.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG3All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG3 == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG3 = TRUE;
	return dlistidx;
}

int  CListHandler::Activate_HWG4 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) //LAC-9
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG4All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG4 (Aufps);
		}
	}

//	ListCount = HWG4All.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG4All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG4 == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG4 = TRUE;
	return dlistidx;
}

int  CListHandler::Activate_HWG5 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) //LAC-9
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG5All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG5 (Aufps);
		}
	}

//	ListCount = HWG5All.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG5All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG5 == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG5 = TRUE;
	return dlistidx;
}

int  CListHandler::Activate_HWG6 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) //LAC-9
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG6All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG6 (Aufps);
		}
	}

//	ListCount = HWG6All.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG6All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG6 == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG6 = TRUE;
	return dlistidx;
}

int  CListHandler::Activate_HWG7 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) //LAC-9
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG7All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG7 (Aufps);
		}
	}

//	ListCount = HWG7All.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG7All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG7 == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG7 = TRUE;
	return dlistidx;
}

int  CListHandler::Activate_HWG8 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) //LAC-9
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG8All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG8 (Aufps);
		}
	}

//	ListCount = HWG8All.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG8All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG8 == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG8 = TRUE;
	return dlistidx;
}

int  CListHandler::Activate_HWG9 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) //LAC-9
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG9All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG9 (Aufps);
		}
	}

//	ListCount = HWG9All.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG9All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG9 == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG9 = TRUE;
	return dlistidx;
}

int  CListHandler::Activate_HWG10 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) //LAC-9
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG10All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG10 (Aufps);
		}
	}

//	ListCount = HWG10All.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG10All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG10 == FALSE) //LAC-9 (VORSICHT! muss getestet werden) wenns einmal drin ist, muss es nicht nochmal neu best�ckt werden !!??  
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG10 = TRUE;
	return dlistidx;
}


int  CListHandler::Activate_HWG11 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) 
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG11All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG11 (Aufps);
		}
	}

	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG11All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG11 == FALSE) 
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG11 = TRUE;
	return dlistidx;
}

int  CListHandler::Activate_HWG12 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) 
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG12All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG12 (Aufps);
		}
	}

	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG12All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG12 == FALSE) 
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG12 = TRUE;
	return dlistidx;
}
int  CListHandler::Activate_HWG13 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) 
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG13All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG13 (Aufps);
		}
	}

	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG13All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG13 == FALSE) 
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG13 = TRUE;
	return dlistidx;
}
int  CListHandler::Activate_HWG14 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) 
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG14All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG14 (Aufps);
		}
	}

	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG14All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG14 == FALSE) 
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG14 = TRUE;
	return dlistidx;
}
int  CListHandler::Activate_HWG15 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) 
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG15All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG15 (Aufps);
		}
	}

	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG15All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG15 == FALSE) 
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG15 = TRUE;
	return dlistidx;
}
int  CListHandler::Activate_HWG16 (AUFPS aufparr[MAXPOS], int aufpanz, int *idx, int dhwg, int dwg) 
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int dlistidx = 0;
	int i = 0 ;
	ListCount = HWG16All.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	aufpanz = GetStndValue ()->anz;
	for (i = 0; i < aufpanz; i ++) 
	{
		it = StndValue.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
  		    SetStndAllA_HWG16 (Aufps);
		}
	}

	for (i = 0; i < ListCount; i ++) 
	{
		it = HWG16All.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			if (activated_HWG16 == FALSE) 
			{
				if (m_SmtReader != NULL) 
				{
					short smt = m_SmtReader->Get (ratod (Aufps->a));
					if (smt == 0)
					{
						smt = 99;
					}
					sprintf (Aufps->teil_smt, "%hd", smt);
					double inh_abverk = m_SmtReader->Get_inh_abverk ();
					double a_gew = m_SmtReader->Get_a_gew ();
					Aufps->hwg = m_SmtReader->Get_hwg (); 
					Aufps->wg = m_SmtReader->Get_wg (); 
					sprintf (Aufps->stk_karton, "%.0lf", inh_abverk);
					sprintf (Aufps->gew_karton, "%.3lf", inh_abverk * a_gew);
					//260312 TODO Hier hwg best�cken
				}

			}
			if (dwg == 0 ||  dwg == Aufps->wg)
			{
				memcpy (&aufparr[dlistidx], Aufps, sizeof (AUFPS));
				dlistidx ++;
			}
		}
	}
    activated_HWG16 = TRUE;
	return dlistidx;
}


void CListHandler::SetStndAllA (AUFPS *Aufps) //Hier StndValue best�cken
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	char soll_me [80]; 
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 )  //|| ratod (Aufps->auf_me) == 0.0) //LAC-9 Menge 0 muss auch behandelt werden !!!
	{
		return;
	}

	StndAll.Start ();
	while ((it = StndAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert) 
			{
				posi = atoi (Item->posi);
				 sprintf(soll_me, Item->soll_me);
				memcpy (Item, Aufps, sizeof (AUFPS));
				 sprintf(Item->soll_me, soll_me);
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
//LAC-124		posi = atoi (Aufps->posi);             
		posi = GetPosiStndValueA (Aufps); //LAC-124
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}



void CListHandler::SetStndAllA_lBest (AUFPS *Aufps) //LAC-9
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	double last_lief_me;
	double last_lieferung;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 )// || ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	lBestAll.Start ();
	while ((it = lBestAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			//if (ratod (Item->a) == ratod (Aufps->a)  && Item->pos_id == Aufps->pos_id)  //150514 erweitert
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				last_lief_me = ratod (Item->last_lief_me);  //LAC-117 muss gesichert wrden, da es sonst vom Aufps vom aktiven Reiter �berschrieben wird
				last_lieferung = ratod (Item->last_lieferung); //LAC-117

				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				sprintf (Item->last_lief_me, "%.2lf", last_lief_me); //LAC-117
				sprintf (Item->last_lieferung, "%.2lf", last_lieferung); //LAC-117
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_KritMhd (AUFPS *Aufps) //LAC-84
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 )
	{
		return;
	}

	KritMhdAll.Start ();
	while ((it = KritMhdAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));
		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_Kommldat (AUFPS *Aufps) //LAC-116
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	double last_lief_me;
	double last_lieferung;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 )
	{
		return;
	}

	KommldatAll.Start ();
	while ((it = KommldatAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);

				last_lief_me = ratod (Item->last_lief_me);  //Hier: LAC-116 muss gesichert wrden, da es sonst vom Aufps vom aktiven Reiter �berschrieben wird
				last_lieferung = ratod (Item->last_lieferung); //LAC-116

				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				sprintf (Item->last_lief_me, "%.2lf", last_lief_me); //LAC-116
				sprintf (Item->last_lieferung, "%.2lf", last_lieferung); //LAC-116

				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));
		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_BestVorschlag (AUFPS *Aufps) 
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	double last_lief_me;
	double last_lieferung;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 )
	{
		return;
	}

	BestVorschlagAll.Start ();
	while ((it = BestVorschlagAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);

				last_lief_me = ratod (Item->last_lief_me);  //Hier: LAC-116 muss gesichert wrden, da es sonst vom Aufps vom aktiven Reiter �berschrieben wird
				last_lieferung = ratod (Item->last_lieferung); //LAC-116

				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				sprintf (Item->last_lief_me, "%.2lf", last_lief_me); //LAC-116
				sprintf (Item->last_lieferung, "%.2lf", last_lieferung); //LAC-116

				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));
		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}


void CListHandler::SetStndAllA_HWG1 (AUFPS *Aufps) //LAC-9
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 ) //|| ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	HWG1All.Start ();
	while ((it = HWG1All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_HWG2 (AUFPS *Aufps) //LAC-9
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 ) //|| ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	HWG2All.Start ();
	while ((it = HWG2All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}


void CListHandler::SetStndAllA_HWG3 (AUFPS *Aufps) //LAC-9
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 ) //|| ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	HWG3All.Start ();
	while ((it = HWG3All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_HWG4 (AUFPS *Aufps) //LAC-9
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 ) //|| ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	HWG4All.Start ();
	while ((it = HWG4All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_HWG5 (AUFPS *Aufps) //LAC-9
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 ) //|| ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	HWG5All.Start ();
	while ((it = HWG5All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_HWG6 (AUFPS *Aufps) //LAC-9
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 ) //|| ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	HWG6All.Start ();
	while ((it = HWG6All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_HWG7 (AUFPS *Aufps) //LAC-9
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 ) //|| ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	HWG7All.Start ();
	while ((it = HWG7All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_HWG8 (AUFPS *Aufps) //LAC-9
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 ) //|| ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	HWG8All.Start ();
	while ((it = HWG8All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_HWG9 (AUFPS *Aufps) //LAC-9
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 ) //|| ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	HWG9All.Start ();
	while ((it = HWG9All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_HWG10 (AUFPS *Aufps) //LAC-9
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 ) //|| ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	HWG10All.Start ();
	while ((it = HWG10All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}

void CListHandler::SetStndAllA_HWG11 (AUFPS *Aufps) 
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 )
	{
		return;
	}

	HWG11All.Start ();
	while ((it = HWG11All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}


void CListHandler::SetStndAllA_HWG12 (AUFPS *Aufps) 
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 )
	{
		return;
	}

	HWG12All.Start ();
	while ((it = HWG12All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}


void CListHandler::SetStndAllA_HWG13 (AUFPS *Aufps) 
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 )
	{
		return;
	}

	HWG13All.Start ();
	while ((it = HWG13All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}


void CListHandler::SetStndAllA_HWG14 (AUFPS *Aufps) 
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 )
	{
		return;
	}

	HWG14All.Start ();
	while ((it = HWG14All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}


void CListHandler::SetStndAllA_HWG15 (AUFPS *Aufps) 
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 )
	{
		return;
	}

	HWG15All.Start ();
	while ((it = HWG15All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}


void CListHandler::SetStndAllA_HWG16 (AUFPS *Aufps) 
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 )
	{
		return;
	}

	HWG16All.Start ();
	while ((it = HWG16All.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}



void CListHandler::SetStndValueA (AUFPS *Aufps, int posi, int idxAll) 
{
	AUFPS **it;
	AUFPS *Item;
	int idxValue;

	BOOL found = FALSE;
	if (ratod (Aufps->auf_me) != 0.0)
	{
		StndValue.Start ();
		while ((it = StndValue.GetNext ()) != NULL)
		{
			Item = *it;
			if (Item != NULL)
			{
//FS-349				if (ratod (Item->a) == ratod (Aufps->a))
				if (ratod (Item->a) == ratod (Aufps->a) && Item->pos_id == Aufps->pos_id) //FS-349   WAL-150 Problem war :  pos_id noch nicht belegt jetzt ok
				{
					memcpy (Item, Aufps, sizeof (AUFPS));  //LAC-152
                	sprintf (Item->posi, "%d", posi); //LAC-152

					found = TRUE;
					break;
				}
			}
		}
		if (!found)
		{
			Item = new AUFPS;
			memcpy (Item, Aufps, sizeof (AUFPS));
			sprintf (Item->posi, "%d", posi);
			idxValue = StndValue.anz;
			StndValue.Add (Item);
			CIdxItem *item = new CIdxItem (idxAll, idxValue);
			IdxCollection.Add (item);
		}
	}
}

int CListHandler::GetPosiStndValueA (AUFPS *Aufps) //LAC-124
{
	AUFPS **it;
	AUFPS *Item;
	int dposi = 0;

	BOOL found = FALSE;
//LAC-124b	if (ratod (Aufps->auf_me) != 0.0)
//LAC-124b	{
		StndValue.Start ();
		while ((it = StndValue.GetNext ()) != NULL)
		{
			Item = *it;
			if (Item != NULL)
			{
				if (dposi < atoi (Item->posi)) dposi = atoi (Item->posi);
				if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
				{
					dposi = atoi (Item->posi);
					found = TRUE;
					break;
				}
			}
		}
		if (!found)
		{
			dposi += 10;
		}
//LAC-124b	}
	return dposi;
}



void CListHandler::DropStndValueA (AUFPS *Aufps, int idxAll)
{
	AUFPS **it;
	AUFPS *Item;
//	int idxValue;
	int i = 0;

	BOOL found = FALSE;
	StndValue.Start ();
	while ((it = StndValue.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				StndValue.Drop (i);
				CIdxItem *IdxItem = IdxCollection.GetItemByAll (idxAll);
				if (IdxItem != NULL)
				{
					IdxCollection.Drop (IdxItem);
				}
				found = TRUE;
				break;
			}
		}
		i ++;
	}
}

int CListHandler::GetAllPosiA (AUFPS *Aufps)
{
	AUFPS **it;
	AUFPS *Item;
	int posi;

	StndAll.Start ();
	while ((it = StndAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a)  && ratod(Item->me_einh_kun) == ratod(Aufps->me_einh_kun))  //150514 erweitert			{
			{
				posi = atoi (Item->posi);
				return posi;
			}
		}
	}
	return 10;
}

void CListHandler::Update (int idx, AUFPS *aufps)
{
	if (ratod (aufps->a) == 0.0)
	{
		return;
	}
//LAC-9 A
		int anz = GetStndValue ()->anz;
//	if (activated)  StndValue muss generell beschrieben werden
//	{
		SetStndAllA (aufps);
	//	SortByPosi (&StndAll); 
	//	SortByPosi (&StndValue); 
//	}
	if (activated_lBest)
	{
		SetStndAllA_lBest (aufps);
	}
	if (activated_KritMhd) //LAC-84
	{
		SetStndAllA_KritMhd (aufps);
	}
	if (activated_Kommldat) //LAC-116
	{
		SetStndAllA_Kommldat (aufps);
	}
	if (activated_BestVorschlag) 
	{
		SetStndAllA_BestVorschlag (aufps);
	}
	if (activated_HWG1)
	{
		SetStndAllA_HWG1 (aufps);
	}
	if (activated_HWG2)
	{
		SetStndAllA_HWG2 (aufps);
	}
	if (activated_HWG3)
	{
		SetStndAllA_HWG3 (aufps);
	}
	if (activated_HWG4)
	{
		SetStndAllA_HWG4 (aufps);
	}
	if (activated_HWG5)
	{
		SetStndAllA_HWG5 (aufps);
	}
	if (activated_HWG6)
	{
		SetStndAllA_HWG6 (aufps);
	}
	if (activated_HWG7)
	{
		SetStndAllA_HWG7 (aufps);
	}
	if (activated_HWG8)
	{
		SetStndAllA_HWG8 (aufps);
	}
	if (activated_HWG9)
	{
		SetStndAllA_HWG9 (aufps);
	}
	if (activated_HWG10)
	{
		SetStndAllA_HWG10 (aufps);
	}
	if (activated_HWG11)
	{
		SetStndAllA_HWG11 (aufps);
	}
	if (activated_HWG12)
	{
		SetStndAllA_HWG12 (aufps);
	}
	if (activated_HWG13)
	{
		SetStndAllA_HWG13 (aufps);
	}
	if (activated_HWG14)
	{
		SetStndAllA_HWG14 (aufps);
	}
	if (activated_HWG15)
	{
		SetStndAllA_HWG15 (aufps);
	}
	if (activated_HWG16)
	{
		SetStndAllA_HWG16 (aufps);
	}
//LAC-9 E
} 

int CListHandler::Switch (AUFPS aufparr[MAXPOS], int *idx)
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	if (ActiveMode == AllEntries)
	{
		ListCount = StndValue.anz;
		if (ListCount == 0)
		{
			return StndAll.anz;
		}
		for (int i = 0; i < ListCount; i ++) 
		{
			it = StndValue.Get (i);
			if (it != NULL)
			{
				Aufps = *it;
				memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
			}
		}
		*idx = IdxCollection.GetIdxValue (*idx);
		ActiveMode = ValueEntries;
	}
	else
	{
		ListCount = StndAll.anz;
		for (int i = 0; i < ListCount; i ++) 
		{
			it = StndAll.Get (i);
			if (it != NULL)
			{
				Aufps = *it;
				memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
			}
		}
		*idx = IdxCollection.GetIdxAll (*idx);
		ActiveMode = AllEntries;
	}
	return ListCount;
}

int CListHandler::HoleStndValue (AUFPS aufparr[MAXPOS], int *idx) //Hier: StndValue zur�ckschreiben in aufparr
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
		ListCount = StndValue.anz;
		if (ListCount == 0)
		{
//			return StndAll.anz;
			return 0;
		}
		for (int i = 0; i < ListCount; i ++) 
		{
			it = StndValue.Get (i);
			if (it != NULL)
			{
				Aufps = *it;
				memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
			}
		}
		*idx = IdxCollection.GetIdxValue (*idx);
	return ListCount;
}


double CListHandler::GetSmtPart ()
{
	double part = 0.0;
	if (StndAll.anz > 0.0 && StndValue.anz > 0.0)
	{
		part = (double) StndValue.anz * 100 / StndAll.anz;
	}
	return part;
}

void CListHandler::SortByPosi (CDataCollection<AUFPS *> *StndCollection)
{
   qsort (StndCollection->Arr, StndCollection->anz, sizeof (AUFPS *),
			   ComparePosi);

}

int CListHandler::ComparePosi (const void *el1, const void *el2)
{
	int ret = 0;
	AUFPS *Aufps1;
	AUFPS *Aufps2;

	Aufps1 = *((AUFPS **) el1);
	Aufps2 = *((AUFPS **) el2);

	ret = (int) (atol (Aufps1->posi) -  atol (Aufps2->posi));

	return ret;
}
