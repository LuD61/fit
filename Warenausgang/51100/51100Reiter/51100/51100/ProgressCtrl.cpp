// ProgressCtrl.cpp: Implementierung der Klasse CProgressCtrl.
//
//////////////////////////////////////////////////////////////////////

#include "ProgressCtrl.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CProgressCtrl::CProgressCtrl()
{
	m_StepPos = 1;
	m_Step = 10;

}

CProgressCtrl::~CProgressCtrl()
{

}

void CProgressCtrl::SetRange (int min, int max)
{
        SendMessage (m_hWnd, PBM_SETSTEP, (WPARAM) min, (LPARAM) max);
		int size = max - min;
		if (size < 5)
		{
			m_Step = 100;
		}
		else if (size < 10)
		{
			m_Step = 50;
		}
		else if (size < 50)
		{
			m_Step = 5;
			m_StepPos = size / 10;
		}
		else if (size < 100)
		{
			m_Step = 1;
			m_StepPos = size / 50;
		}
		else
		{
			m_Step = 1;
			m_StepPos = size / 100;
		}
		SetStep (m_Step);
		m_Pos = 0;
		m_Count = 0;
}


void CProgressCtrl::SetStep (int step)
{
        SendMessage (m_hWnd, PBM_SETSTEP, (WPARAM) step, (LPARAM) 0);
}


void CProgressCtrl::StepIt (void)
{
	    m_Pos ++;
		if (m_Pos >= m_StepPos)
		{
			m_Count ++;
			if (m_Count < 100)
			{
				SendMessage (m_hWnd, PBM_STEPIT, (WPARAM) 0, (LPARAM) 0);
				InvalidateRect (m_hWnd, NULL, FALSE);
				UpdateWindow (m_hWnd);
				m_Pos = 0;
			}
		}
}

void CProgressCtrl::SetPos (int pos)
{
        SendMessage (m_hWnd, PBM_SETPOS, (WPARAM) pos, (LPARAM) 0);
}

