#ifndef _MO_PAKET_DEF
#define _MO_PAKET_DEF

#define MAXPAKET 120

struct PAKET
{
    double a;
	double auf_me;
	int me_einh_kun;
	double rab_satz;
	char auf_me_bz[7];
};

extern struct PAKET paket [MAXPAKET];

class PAKET_CLASS : DB_CLASS
{
        private :
            int anzart;
            struct PAKET paket [MAXPAKET];
            int paket_anz;
            int paket_pos;
        public :
            PAKET_CLASS () : DB_CLASS (), anzart (10), paket_anz (0), paket_pos (0)
            {
            }
           void AnzArt (int anz)
           {
                 anzart = anz;
           }
           void arttotab (double a, double auf_me, int me_einh_kun, double rab_satz, char *auf_me_bz);
           BOOL ReadPaket (short,short, long, double);
           double GetPaketArtikel (void);
           BOOL GetFirst (double *a, double *auf_me, int *me_einh_kun, double *rab_satz, char *auf_me_bz);
           BOOL GetNext (double *a, double *auf_me, int *me_einh_kun, double *rab_satz, char *auf_me_bz);
           BOOL GetPrior (double *a, double *auf_me, int *me_einh_kun, double *rab_satz, char *auf_me_bz);
           BOOL GetCurrent (double *a, double *auf_me, int *me_einh_kun, double *rab_satz, char *auf_me_bz);
           BOOL GetAbsolute (double *a, double *auf_me, int *me_einh_kun, double *rab_satz, char *auf_me_bz, int pos);
};
#endif
