#ifndef _LLGEN_DEF
#define LLGEN
#include "Text.h"

class LLGEN_CLASS
{
       private :

static int textdatei;
static int windatei ;
static int exeldatei ;

static int SetWin (void);
static int SetText (void);
static int SetExel (void);
static int BreakBox (void);


//FORMFELD_CLASS formfeld_class;
//FORMFRM_CLASS formfrm_class;
//FORMHEAD_CLASS formhead_class;


       public :

        Text AlternateType;
		Text AlternateDir;
		Text AlternateName;

        LLGEN_CLASS (); 
        void DruckeFile (int label);
        void WaitListEnd (void);
        int IsError ();
        void print_file (void);
        void ShowFile (void);
        void printerfile (void);
        BOOL LeseFormat (char *form_nr);
        BOOL FillFormFeld (char *feldname, char *valfrom, char *valto);
        void BelegeDrucker (void);
        void BelegeGdiDrucker (void);
        void SetFormular (BOOL frml);
        void drucker_akt (char *);
        void StartPrint  (int label);
        void ChoisePrinter (void);
        void ChoisePrinter1 (void);
        void SetAusgabe (int);
        void DrkWahl (void);
        void SetDrkWahl (int);
};
#endif
