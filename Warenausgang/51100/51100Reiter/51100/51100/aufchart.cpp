#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "aufchart.h"

struct AUFCHART aufchart, aufchart_null;

void AUFCHART_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &aufchart.auf, 2, 0);
    out_quest ((char *) &aufchart.auf,2,0);
    out_quest ((char *) &aufchart.kun,2,0);
    out_quest ((char *) &aufchart.lieferdat,2,0);
    out_quest ((char *) &aufchart.jr,1,0);
    out_quest ((char *) &aufchart.kw,1,0);
    out_quest ((char *) &aufchart.mo,1,0);
    out_quest ((char *) &aufchart.wochentag,1,0);
    out_quest ((char *) &aufchart.woche_folge,1,0);
            cursor = prepare_sql ("select aufchart.auf,  "
"aufchart.kun,  aufchart.lieferdat,  aufchart.jr,  aufchart.kw,  "
"aufchart.mo,  aufchart.wochentag,  aufchart.woche_folge from aufchart "

#line 26 "aufchart.rpp"
                                  "where auf = ? ");

    ins_quest ((char *) &aufchart.auf,2,0);
    ins_quest ((char *) &aufchart.kun,2,0);
    ins_quest ((char *) &aufchart.lieferdat,2,0);
    ins_quest ((char *) &aufchart.jr,1,0);
    ins_quest ((char *) &aufchart.kw,1,0);
    ins_quest ((char *) &aufchart.mo,1,0);
    ins_quest ((char *) &aufchart.wochentag,1,0);
    ins_quest ((char *) &aufchart.woche_folge,1,0);
            sqltext = "update aufchart set aufchart.auf = ?,  "
"aufchart.kun = ?,  aufchart.lieferdat = ?,  aufchart.jr = ?,  "
"aufchart.kw = ?,  aufchart.mo = ?,  aufchart.wochentag = ?,  "
"aufchart.woche_folge = ? "

#line 29 "aufchart.rpp"
                                  "where auf = ? ";

            ins_quest ((char *) &aufchart.auf, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &aufchart.auf, 2, 0);
            test_upd_cursor = prepare_sql ("select auf from aufchart "
                                  "where auf = ? ");

            ins_quest ((char *) &aufchart.auf, 2, 0);
            del_cursor = prepare_sql ("delete from aufchart "
                                  "where auf = ? ");


    ins_quest ((char *) &aufchart.auf,2,0);
    ins_quest ((char *) &aufchart.kun,2,0);
    ins_quest ((char *) &aufchart.lieferdat,2,0);
    ins_quest ((char *) &aufchart.jr,1,0);
    ins_quest ((char *) &aufchart.kw,1,0);
    ins_quest ((char *) &aufchart.mo,1,0);
    ins_quest ((char *) &aufchart.wochentag,1,0);
    ins_quest ((char *) &aufchart.woche_folge,1,0);
            ins_cursor = prepare_sql ("insert into aufchart ("
"auf,  kun,  lieferdat,  jr,  kw,  mo,  wochentag,  woche_folge) "

#line 44 "aufchart.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?)"); 

#line 46 "aufchart.rpp"
}
int AUFCHART_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}


