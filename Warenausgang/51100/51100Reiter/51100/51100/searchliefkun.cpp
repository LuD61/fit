#include <windows.h>
#include "wmaskc.h"
#include "searchliefkun.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"
#include "dlglst.h"


struct SLIEFKUN *SEARCHLIEFKUN::skuntab = NULL;
struct SLIEFKUN SEARCHLIEFKUN::skun;
int SEARCHLIEFKUN::idx = -1;
long SEARCHLIEFKUN::kunanz;
CHQEX *SEARCHLIEFKUN::Query = NULL;
DB_CLASS SEARCHLIEFKUN::DbClass; 
HINSTANCE SEARCHLIEFKUN::hMainInst;
HWND SEARCHLIEFKUN::hMainWindow;
HWND SEARCHLIEFKUN::awin;
int SEARCHLIEFKUN::SearchField = 1;
int SEARCHLIEFKUN::soadr_nam1 = 1; 
int SEARCHLIEFKUN::soplz = 1; 
int SEARCHLIEFKUN::soort1 = 1; 
short SEARCHLIEFKUN::mdn = 0; 


ITEM SEARCHLIEFKUN::uname1   ("adr_nam1",  "Kunde",   "", 0);  
ITEM SEARCHLIEFKUN::uort1    ("ort1" ,     "Ort",     "", 0);  
ITEM SEARCHLIEFKUN::uplz     ("plz",       "PLZ",     "", 0);  
ITEM SEARCHLIEFKUN::ustrasse ("strasse",   "Strasse", "", 0);  


field SEARCHLIEFKUN::_UbForm[] = {
&uname1,      26, 0, 0,  0,  NULL, "", BUTTON, 0, 0, 0,     
&uort1,       26, 0, 0, 26 , NULL, "", BUTTON, 0, 0, 0,     
&uplz,        11, 0, 0, 52 , NULL, "", BUTTON, 0, 0, 0,     
&ustrasse,    26, 0, 0, 63 , NULL, "", BUTTON, 0, 0, 0,     
};

form SEARCHLIEFKUN::UbForm = {4, 0, 0, _UbForm, 0, 0, 0, 0, NULL};

ITEM SEARCHLIEFKUN::iname1   ("adr_nam1",   skun.adr_nam1, "", 0);  
ITEM SEARCHLIEFKUN::iort1    ("ort1",       skun.ort1,     "", 0);  
ITEM SEARCHLIEFKUN::iplz     ("plz",        skun.plz,      "", 0);  
ITEM SEARCHLIEFKUN::istrasse ("strasse",    skun.strasse,  "", 0);  


field SEARCHLIEFKUN::_DataForm[] = {
&iname1,      24, 0, 0,  1,  NULL, "", DISPLAYONLY, 0, 0, 0,     
&iort1,       24, 0, 0, 27 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iplz,         9, 0, 0, 54 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&istrasse,    24, 0, 0, 65 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHLIEFKUN::DataForm = {4, 0, 0, _DataForm, 0, 0, 0, 0, NULL};

ITEM SEARCHLIEFKUN::iline ("", "1", "", 0);

field SEARCHLIEFKUN::_LineForm[] = {
&iline,       1, 0, 0, 26 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 52 , NULL, "", DISPLAYONLY, 0, 0, 0,     
&iline,       1, 0, 0, 63 , NULL, "", DISPLAYONLY, 0, 0, 0,     
};

form SEARCHLIEFKUN::LineForm = {3, 0, 0, _LineForm, 0, 0, 0, 0, NULL};

char *SEARCHLIEFKUN::query = NULL;

int SEARCHLIEFKUN::sortadr_nam1 (const void *elem1, const void *elem2)
{
	      struct SLIEFKUN *el1; 
	      struct SLIEFKUN *el2; 

		  el1 = (struct SLIEFKUN *) elem1;
		  el2 = (struct SLIEFKUN *) elem2;
          clipped (el1->adr_nam1);
          clipped (el2->adr_nam1);
          if (strlen (el1->adr_nam1) == 0 &&
              strlen (el2->adr_nam1) == 0)
          {
              return 0;
          }
          if (strlen (el1->adr_nam1) == 0)
          {
              return -1;
          }
          if (strlen (el2->adr_nam1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->adr_nam1,el2->adr_nam1) * soadr_nam1);
}

void SEARCHLIEFKUN::SortAdr_nam1 (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SLIEFKUN),
				   sortadr_nam1);
       soadr_nam1 *= -1;
}

int SEARCHLIEFKUN::sortplz (const void *elem1, const void *elem2)
{
	      struct SLIEFKUN *el1; 
	      struct SLIEFKUN *el2; 

		  el1 = (struct SLIEFKUN *) elem1;
		  el2 = (struct SLIEFKUN *) elem2;
          clipped (el1->plz);
          clipped (el2->plz);
          if (strlen (el1->plz) == 0 &&
              strlen (el2->plz) == 0)
          {
              return 0;
          }
          if (strlen (el1->plz) == 0)
          {
              return -1;
          }
          if (strlen (el2->plz) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->plz,el2->plz) * soplz);
}

void SEARCHLIEFKUN::SortPlz (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SLIEFKUN),
				   sortplz);
       soplz *= -1;
}

int SEARCHLIEFKUN::sortort1 (const void *elem1, const void *elem2)
{
	      struct SLIEFKUN *el1; 
	      struct SLIEFKUN *el2; 

		  el1 = (struct SLIEFKUN *) elem1;
		  el2 = (struct SLIEFKUN *) elem2;
          clipped (el1->ort1);
          clipped (el2->ort1);
          if (strlen (el1->ort1) == 0 &&
              strlen (el2->ort1) == 0)
          {
              return 0;
          }
          if (strlen (el1->ort1) == 0)
          {
              return -1;
          }
          if (strlen (el2->ort1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->ort1,el2->ort1) * soort1);
}

void SEARCHLIEFKUN::SortOrt1 (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SLIEFKUN),
				   sortort1);
       soort1 *= -1;
}

void SEARCHLIEFKUN::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortAdr_nam1 (hWnd);
                  SearchField = 1;
                  break;
              case 1 :
                  SortPlz (hWnd);
                  SearchField = 2;
                  break;
              case 2 :
                  SortOrt1 (hWnd);
                  SearchField = 3;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHLIEFKUN::FillFormat (char *buffer, int size)
{
        DlgLst::FillFrmAttr (&DataForm, buffer, size);
}


void SEARCHLIEFKUN::FillCaption (char *buffer, int size)
{
        DlgLst::FillFrmTitle (&UbForm, buffer, 0, size);
}

void SEARCHLIEFKUN::FillVlines (char *buffer, int size) 
{
        DlgLst::FillFrmLines (&LineForm, buffer, 0, size);
}


void SEARCHLIEFKUN::FillRec (char *buffer, int i, int size)
{
          DlgLst::FillFrmData (&DataForm, buffer, 0, size);
}


void SEARCHLIEFKUN::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < kunanz; i ++)
       {
		  memcpy (&skun, &skuntab[i],  sizeof (struct SLIEFKUN));
          FillRec (buffer, i, 512);
	      Query->UpdateRecord (buffer, i);
       }
}

int SEARCHLIEFKUN::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   Query->SearchList ();
	   return 0;
}

int SEARCHLIEFKUN::Read (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
      int cursor1;
	  int i;
      WMESS Wmess;


	  if (skuntab) 
	  {
           delete skuntab;
           skuntab = NULL;
	  }


      idx = -1;
	  kunanz = 0;
//      SearchField = 1;
      Query->SetSBuff ("");
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      if (kunanz == 0) kunanz = 0x10000;

	  skuntab = new struct SLIEFKUN [kunanz];
      if (squery == NULL ||
          strlen (squery) == 0)
      {
              sprintf (buffer, "select kunlief.kun "
	 			       "from kunlief "
                       "where mdn = %hd ", mdn); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
              DbClass.sqlout ((char *) skun.kun, 0, 9);
              cursor = DbClass.sqlcursor (buffer);
      }
      else
      {
              sprintf (buffer, "select kunlief.kun "
	 			       "from kunlief, adr "
                       "where kunlief.mdn = %hd " 
                       "and kunlief.adr = adr.adr "
                       "and adr.adr_nam1 machtes \"%s*\"", mdn, squery); 
              if (query != NULL && strlen (query) > 0)
              {
                  strcat (buffer, " ");
                  strcat (buffer, query);
              }
             DbClass.sqlout ((char *) skun.kun, 0, 9);
              cursor = DbClass.sqlcursor (buffer);
      }
      if (cursor < 0) 
      {
          return -1;
      }
      sprintf (buffer, "select adr.adr, adr.adr_nam1, adr.plz, adr.ort1, adr.str "
	 			       "from kun, adr "
                       "where kun.mdn = %hd "
                       "and kun.kun = ? "
                       "and kun.adr1 = adr.adr", mdn); 
      DbClass.sqlin  ((char *) skun.kun, 0, 9);
      DbClass.sqlout ((char *) skun.adr, 0, 9);
      DbClass.sqlout ((char *) skun.adr_nam1, 0, 37);
      DbClass.sqlout ((char *) skun.plz,      0, 10);
      DbClass.sqlout ((char *) skun.ort1,     0, 37);
      DbClass.sqlout ((char *) skun.strasse,  0, 37);
      cursor1 = DbClass.sqlcursor (buffer);

      if (cursor1 < 0) 
      {
          return -1;
      }
  	  i = 0;
	  while (DbClass.sqlfetch (cursor) == 0)
      {
          DbClass.sqlopen (cursor1);
          DbClass.sqlfetch (cursor1);
		  memcpy (&skuntab[i], &skun, sizeof (struct SLIEFKUN));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      kunanz = i;
	  DbClass.sqlclose (cursor);
	  DbClass.sqlclose (cursor1);

      soadr_nam1 = 1;
      if (kunanz > 1)
      {
             SortAdr_nam1 (Query->GethWnd ());
      }
      UpdateList ();

	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHLIEFKUN::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < kunanz; i ++)
      {
		  memcpy (&skun, &skuntab[i],  sizeof (struct SLIEFKUN));
          FillRec (buffer, i, 512);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHLIEFKUN::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}


void SEARCHLIEFKUN::Search (short mdn)
{
  	  int cx, cy;
	  char buffer [512];
      RECT rect;

      if (mdn == 0)
      {
          disp_mess ("Mandant 0 ist nicht erlaubt", 2);
          syskey = KEY5;
          return;
      }
      this->mdn = mdn;
      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
//      EnableWindow (hMainWindow, FALSE);
      SetSortProc (SortLst);
      if (Query == NULL)
      {
        idx = -1;
        if (query != NULL && strlen (query) > 0)
        {
               Query = new CHQEX (cx, cy);
        }
        else
        {
               Query = new CHQEX (cx, cy, "Name", "");
        }
        Query->EnableSort (TRUE);
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        if (query != NULL && strlen (query) > 0)
        {
              Read ("");
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer, 512);
        Query->RowAttr (buffer);
        FillVlines (buffer, 512);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer, 512);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (Read);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
      }
	  Query->ProcessMessages ();
//      EnableWindow (hMainWindow, TRUE);
      idx = Query->GetSel ();
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
      if (skuntab == NULL)
      {
          delete Query;
          Query = NULL;
      }
	  if (idx == -1) return;
	  memcpy (&skun, &skuntab[idx], sizeof (skun));
      SetSortProc (NULL);
      if (skuntab != NULL)
      {
          Query->SavefWork ();
      }
}


SLIEFKUN *SEARCHLIEFKUN::GetNextSkun (void)
{
      if (idx == -1) return NULL;
      if (idx < kunanz - 1) 
      {
             idx ++;
      }
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}


SLIEFKUN *SEARCHLIEFKUN::GetPriorSkun (void)
{
      if (idx == -1) return NULL;
      if (idx > 0) 
      {
             idx --;
      }
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}

SLIEFKUN *SEARCHLIEFKUN::GetFirstSkun (void)
{
      if (idx == -1) return NULL;
      idx = 0;
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}

SLIEFKUN *SEARCHLIEFKUN::GetLastSkun (void)
{
      if (idx == -1) return NULL;
      idx = kunanz - 1;
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}



