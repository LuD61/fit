#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "aufkun.h"

struct AUFKUN aufkun, aufkun_null;

void AUFKUN_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)  &aufkun.mdn, 1, 0);
            ins_quest ((char *)  &aufkun.fil, 1, 0);
            ins_quest ((char *)  &aufkun.kun_fil, 1, 0);
            ins_quest ((char *)   &aufkun.kun, 2, 0);
            ins_quest ((char *) &aufkun.a, 3, 0);
            ins_quest ((char *)  &aufkun.lfd, 2, 0);
    out_quest ((char *) &aufkun.mdn,1,0);
    out_quest ((char *) &aufkun.fil,1,0);
    out_quest ((char *) &aufkun.kun,2,0);
    out_quest ((char *) &aufkun.auf,2,0);
    out_quest ((char *) &aufkun.a,3,0);
    out_quest ((char *) &aufkun.kun_fil,1,0);
    out_quest ((char *) &aufkun.lfd,2,0);
    out_quest ((char *) &aufkun.bearb,2,0);
    out_quest ((char *) &aufkun.lieferdat,2,0);
    out_quest ((char *) &aufkun.auf_me,3,0);
    out_quest ((char *) &aufkun.lief_me,3,0);
    out_quest ((char *) &aufkun.auf_vk_pr,3,0);
            cursor = prepare_sql ("select aufkun.mdn,  "
"aufkun.fil,  aufkun.kun,  aufkun.auf,  aufkun.a,  aufkun.kun_fil,  "
"aufkun.lfd,  aufkun.bearb,  aufkun.lieferdat,  aufkun.auf_me,  "
"aufkun.lief_me,  aufkun.auf_vk_pr from aufkun "

#line 31 "aufkun.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and kun_fil = ? "
                                  "and kun = ? "
                                  "and a = ? "
                                  "and lfd = ?");

            ins_quest ((char *) &aufkun.mdn, 1, 0);
            ins_quest ((char *) &aufkun.fil, 1, 0);
            ins_quest ((char *) &aufkun.kun_fil, 1, 0);
            ins_quest ((char *) &aufkun.kun, 2, 0);
            ins_quest ((char *) &aufkun.a, 3, 0);
 
    out_quest ((char *) &aufkun.mdn,1,0);
    out_quest ((char *) &aufkun.fil,1,0);
    out_quest ((char *) &aufkun.kun,2,0);
    out_quest ((char *) &aufkun.auf,2,0);
    out_quest ((char *) &aufkun.a,3,0);
    out_quest ((char *) &aufkun.kun_fil,1,0);
    out_quest ((char *) &aufkun.lfd,2,0);
    out_quest ((char *) &aufkun.bearb,2,0);
    out_quest ((char *) &aufkun.lieferdat,2,0);
    out_quest ((char *) &aufkun.auf_me,3,0);
    out_quest ((char *) &aufkun.lief_me,3,0);
    out_quest ((char *) &aufkun.auf_vk_pr,3,0);
            cursorlast = prepare_sql ("select aufkun.mdn,  "
"aufkun.fil,  aufkun.kun,  aufkun.auf,  aufkun.a,  aufkun.kun_fil,  "
"aufkun.lfd,  aufkun.bearb,  aufkun.lieferdat,  aufkun.auf_me,  "
"aufkun.lief_me,  aufkun.auf_vk_pr from aufkun "

#line 45 "aufkun.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and kun_fil = ? "
                                  "and kun = ? "
                                  "and a = ? "
                                  "order by bearb desc,lfd desc,auf");

    ins_quest ((char *) &aufkun.mdn,1,0);
    ins_quest ((char *) &aufkun.fil,1,0);
    ins_quest ((char *) &aufkun.kun,2,0);
    ins_quest ((char *) &aufkun.auf,2,0);
    ins_quest ((char *) &aufkun.a,3,0);
    ins_quest ((char *) &aufkun.kun_fil,1,0);
    ins_quest ((char *) &aufkun.lfd,2,0);
    ins_quest ((char *) &aufkun.bearb,2,0);
    ins_quest ((char *) &aufkun.lieferdat,2,0);
    ins_quest ((char *) &aufkun.auf_me,3,0);
    ins_quest ((char *) &aufkun.lief_me,3,0);
    ins_quest ((char *) &aufkun.auf_vk_pr,3,0);
            sqltext = "update aufkun set aufkun.mdn = ?,  "
"aufkun.fil = ?,  aufkun.kun = ?,  aufkun.auf = ?,  aufkun.a = ?,  "
"aufkun.kun_fil = ?,  aufkun.lfd = ?,  aufkun.bearb = ?,  "
"aufkun.lieferdat = ?,  aufkun.auf_me = ?,  aufkun.lief_me = ?,  "
"aufkun.auf_vk_pr = ? "

#line 53 "aufkun.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and kun_fil = ? "
                                  "and kun = ? "
                                  "and a = ? "
                                  "and lfd = ?";

            ins_quest ((char *)  &aufkun.mdn, 1, 0);
            ins_quest ((char *)  &aufkun.fil, 1, 0);
            ins_quest ((char *)  &aufkun.kun_fil, 1, 0);
            ins_quest ((char *)  &aufkun.kun, 2, 0);
            ins_quest ((char *)  &aufkun.a, 3, 0);
            ins_quest ((char *)  &aufkun.lfd, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *)  &aufkun.mdn, 1, 0);
            ins_quest ((char *)  &aufkun.fil, 1, 0);
            ins_quest ((char *)  &aufkun.kun_fil, 1, 0);
            ins_quest ((char *)  &aufkun.kun, 2, 0);
            ins_quest ((char *)  &aufkun.a, 3, 0);

            test_upd_cursor = prepare_sql ("select a from aufkun "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and kun_fil = ? "
                                  "and kun = ? "
                                  "and a = ? for update");



            ins_quest ((char *)  &aufkun.mdn, 1, 0);
            ins_quest ((char *)  &aufkun.fil, 1, 0);
            ins_quest ((char *)  &aufkun.kun_fil, 1, 0);
            ins_quest ((char *)   &aufkun.kun, 2, 0);
            ins_quest ((char *) &aufkun.a, 3, 0);
            ins_quest ((char *)  &aufkun.lfd, 2, 0);
            del_cursor = prepare_sql ("delete from aufkun "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and kun_fil = ? "
                                  "and kun = ? "
                                  "and a = ? "
                                  "and lfd = ?");

    ins_quest ((char *) &aufkun.mdn,1,0);
    ins_quest ((char *) &aufkun.fil,1,0);
    ins_quest ((char *) &aufkun.kun,2,0);
    ins_quest ((char *) &aufkun.auf,2,0);
    ins_quest ((char *) &aufkun.a,3,0);
    ins_quest ((char *) &aufkun.kun_fil,1,0);
    ins_quest ((char *) &aufkun.lfd,2,0);
    ins_quest ((char *) &aufkun.bearb,2,0);
    ins_quest ((char *) &aufkun.lieferdat,2,0);
    ins_quest ((char *) &aufkun.auf_me,3,0);
    ins_quest ((char *) &aufkun.lief_me,3,0);
    ins_quest ((char *) &aufkun.auf_vk_pr,3,0);
            ins_cursor = prepare_sql ("insert into aufkun ("
"mdn,  fil,  kun,  auf,  a,  kun_fil,  lfd,  bearb,  lieferdat,  auf_me,  lief_me,  auf_vk_pr) "

#line 98 "aufkun.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?)"); 

#line 100 "aufkun.rpp"
}
int AUFKUN_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int AUFKUN_CLASS::dbreadlast (void)
/**
Ersten Satz aus Tabelle lesen, absteigend sortiert nach bearb.
**/
{
         int dsqlstatus;

         if (cursorlast == -1)
         {
                this->prepare ();
         }
         dsqlstatus = open_sql (cursorlast);
         if (dsqlstatus == 0)
         {
                 dsqlstatus = fetch_sql (cursorlast);
         }
         return dsqlstatus;
}

int AUFKUN_CLASS::dbreadnextlast (void)
/**
Naechsten Satz aus Tabelle lesen, absteigend sortiert nach bearb.
**/
{
         int dsqlstatus;

         dsqlstatus = fetch_sql (cursorlast);
         return dsqlstatus;
}
         
int AUFKUN_CLASS::dbupdatelast0 (long saveanz)
/**
Update in aufkun. Nur saveanz Eintraege pro Artikel und Kunde werden gespeichert.
**/
{
         int dsqlstatus;
         long anz; 
         long auf;

         if (cursorlast == -1)
         {
                this->prepare ();
         }
         anz = 0;
         sqlin ((short *) &aufkun.mdn, 1, 0); 
         sqlin ((short *) &aufkun.fil, 1, 0); 
         sqlin ((long *)  &aufkun.kun, 2, 0); 
         sqlin ((short *)  &aufkun.kun_fil, 1, 0); 
         sqlin ((double *)  &aufkun.a,   3, 0); 
         sqlout ((long *) &anz, 2, 0);
         dsqlstatus = sqlcomm ("select count (*) from aufkun "
                               "where mdn = ? "
                               "and fil   = ? "
                               "and kun   = ? "
                               "and kun_fil = ? "
                               "and a     = ?");
         if (dsqlstatus == 100 || anz < saveanz)
         {
                      aufkun.lfd = anz + 1;  
                      return sqlexecute (ins_cursor);
         }
         saveanz = anz; 
         auf = aufkun.auf;
         sqlin ((short *) &aufkun.mdn, 1, 0); 
         sqlin ((short *) &aufkun.fil, 1, 0); 
         sqlin ((long *)  &aufkun.kun, 2, 0); 
         sqlin ((short *)  &aufkun.kun_fil, 1, 0); 
         sqlin ((double *)  &aufkun.a,   3, 0); 
         sqlout ((long *) &aufkun.lfd, 2, 0);
         dsqlstatus = sqlcomm ("select lfd,bearb from aufkun "
                               "where mdn = ? "
                               "and fil   = ? "
                               "and kun   = ? "
                               "and kun_fil   = ? "
                               "and a     = ? order by lfd");
         anz ++;


         sqlin ((long *)   &anz, 2, 0);
         sqlin ((long *)   &auf, 2, 0);
         sqlin ((double *) &aufkun.auf_me, 3, 0);
         sqlin ((double *) &aufkun.lief_me, 3, 0);
         sqlin ((double *) &aufkun.auf_vk_pr, 3, 0);
         sqlin ((long *)   &aufkun.bearb, 2, 0);
         sqlin ((long *)   &aufkun.lieferdat, 2, 0);

         sqlin ((short *)  &aufkun.mdn, 1, 0);
         sqlin ((short *)  &aufkun.fil, 1, 0);
         sqlin ((short *)  &aufkun.kun_fil, 1, 0);
         sqlin ((long *)   &aufkun.kun, 2, 0);
         sqlin ((double *) &aufkun.a, 3, 0);
         sqlin ((long *)   &aufkun.lfd, 2, 0);

         sqlcomm ("update aufkun set lfd = ?,auf = ?,auf_me = ?,lief_me = ?,"
                                  "auf_vk_pr = ?,"
                                  "bearb = ?,lieferdat = ? "  
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and kun_fil = ? "
                                  "and kun = ? "
                                  "and a = ? "
                                  "and lfd = ?");

         sqlin ((short *) &aufkun.mdn, 1, 0); 
         sqlin ((short *) &aufkun.fil, 1, 0); 
         sqlin ((long *)  &aufkun.kun, 2, 0); 
         sqlin ((short *)  &aufkun.kun_fil, 1, 0); 
         sqlin ((double *)  &aufkun.a,   3, 0); 
         sqlcomm ("update aufkun set lfd = lfd - 1 "
                               "where mdn = ? "
                               "and fil   = ? "
                               "and kun   = ? "
                               "and kun_fil   = ? "
                               "and a     = ?");
         return 0;
}

int AUFKUN_CLASS::dbupdatelast (long saveanz)
/**
Update in aufkun. Nur saveanz Eintraege pro Artikel und Kunde werden gespeichert.
**/
{
         int dsqlstatus;
		 extern short sql_mode;
		 short sqls;
		 int i = 0;

		 sqls = sql_mode;
		 sql_mode = 1;
		 dsqlstatus = dbupdatelast0 (saveanz);
		 while (sqlstatus != 0)
		 {
			 i ++;
		     if (i >= 50) break;
			 Sleep (10);
		     dsqlstatus = dbupdatelast0 (saveanz);
		 }
		 sql_mode = sqls;
		 return dsqlstatus;
}
		 

int AUFKUN_CLASS::dbclose (void)          
/**
Alle Cursor schliessen.
**/
{
         if (cursorlast != -1)
         {
                 close_sql (cursorlast);
                 cursorlast = -1;
         }
         this->DB_CLASS::dbclose ();  
         return 0;
}



