// Thread.cpp: Implementierung der Klasse CThread.
//
//////////////////////////////////////////////////////////////////////

#include <process.h> 
#include "Thread.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CThread::CThread()
{
	Name = NULL;
	enumThreadStatus = Created;
	m_Mutex = (CMutex*) new CMutex("SeThread");
	SetTerminationRequest(0);
}

CThread::~CThread()
{
	if (Name != NULL)
	{
		// DEBUGLOGGER->mf_vPrintf(L"BizBaseThread::~BizBaseThread()(%s) : Destructor\n", m_pwcName);
		delete [] Name;
		Name = NULL;
	}
	else
	{
		// DEBUGLOGGER->mf_vPrintf(L"BizBaseThread::~BizBaseThread()(nameless thread) : Destructor\n");
	}


	enumThreadStatus = Invalid;
	if (m_Mutex != NULL) 
	{
		delete m_Mutex; 
		m_Mutex = NULL;
	}
}

unsigned long __stdcall CThread::Execute( void *thread )
{
    unsigned int RetCode = 0;

    RetCode =((CThread*) thread)->Run();
    return (RetCode);

}

unsigned long CThread::Suspend()
{
	unsigned long RetCode = 0;
    switch( enumGetStatus() )
        {
        case Created:
			break;
        case Finished:
			break;
        default:
            enumThreadStatus = Suspended;
            RetCode = SuspendThread(ThreadHandle);
			break;
        }
	return (RetCode);
}

unsigned long CThread::Resume()
{
	unsigned long RetCode = 0;
    switch( enumGetStatus() )
        {
        case Created:
			break;
        case Running:
			break;
        case Finished:
			break;
        default:
            RetCode = ResumeThread(ThreadHandle);
            if( RetCode == 1 )
                enumThreadStatus = Running;
            
			break;
        }   
	return (RetCode);
}
	

void CThread::Kill()
{
	TerminateThread(ThreadHandle, -1);
}

int CThread::SetPriority( int pri )
{
    return ::SetThreadPriority(ThreadHandle, pri);
}


bool CThread::SetName (LPSTR Name)
{
	bool RetCode = false;

	if (this->Name != NULL) 
	{
		delete [] this->Name;
		this->Name = NULL;
	}
	if (Name != NULL)
	{
		long l = strlen(Name);
		this->Name = (LPSTR) new char[l+1];
		if (this->Name != NULL)
		{
			strcpy (this->Name, Name);
			RetCode = true;
		}
	}
	return RetCode;
}

bool CThread::Start(LPSTR Name)
{
	bool RetCode = false;
	long ExecuteCount = 0L;
	enumThreadStatus = Starting;
	SetTerminationRequest(0); // ReStart
	RetCode = SetName (Name);
	if (RetCode)
	{
		while (ExecuteCount < THREADEX_MAX_RETRY ) {
			errno = 0;
			ThreadHandle = (HANDLE) CreateThread (0, 0, &CThread::Execute, this, 0, &ThreadId);
			if( ThreadHandle != 0 )
			{
				enumThreadStatus = Running;
				break;
			}        
			else
			{
				Sleep((DWORD) THREADEX_DELAY_MSEC);
				ExecuteCount++;
				RetCode = false;
				enumThreadStatus = Invalid;
			}

		}
	}
	return (RetCode);
}


void CThread::SetTerminationRequest(int TerminationRequest)
{
	if (m_Mutex != NULL) m_Mutex->WaitForSingleObject(0xFFFFFFFF);
	this->TerminationRequested = TerminationRequest;
	if (m_Mutex != NULL) m_Mutex->Release();
}


CThread::Status CThread::enumCheckStatus() 
{
    DWORD ExitCode;
    ::GetExitCodeThread( ThreadHandle, &ExitCode );
    if( ExitCode == STILL_ACTIVE )
        return Running;
    else
        return Finished;
}

CThread::Status CThread::enumGetStatus()
{
	if (m_Mutex != NULL) m_Mutex->WaitForSingleObject(0xFFFFFFFF);
 	if	(	( enumThreadStatus == Running ) 
		||	( enumThreadStatus == Starting ) 
		)
	{
		enumThreadStatus = enumCheckStatus();
	}
	if (m_Mutex != NULL) m_Mutex->Release();
    return enumThreadStatus;
}


void CThread::enumSetStatus(CThread::Status status)
{
	if (m_Mutex != NULL) m_Mutex->WaitForSingleObject(0xFFFFFFFF);
	enumThreadStatus = status;
	if (m_Mutex != NULL) m_Mutex->Release();
}
