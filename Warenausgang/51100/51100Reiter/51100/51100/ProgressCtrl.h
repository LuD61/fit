// ProgressCtrl.h: Schnittstelle f�r die Klasse CProgressCtrl.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROGRESSCTRL_H__2704CD6F_D770_4C06_9D8E_5C147968FC24__INCLUDED_)
#define AFX_PROGRESSCTRL_H__2704CD6F_D770_4C06_9D8E_5C147968FC24__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Wnd.h"
#include <commctrl.h>

class CProgressCtrl : public CWnd  
{
private:
	int m_Pos;
	int m_Count;
	int m_Step;
	int m_Steps;
	int m_StepPos;
public:
	CProgressCtrl();
	virtual ~CProgressCtrl();
    void SetRange (int, int);
    void SetStep (int);
    void StepIt (void);
    void SetPos (int);

};

#endif // !defined(AFX_PROGRESSCTRL_H__2704CD6F_D770_4C06_9D8E_5C147968FC24__INCLUDED_)
