#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "dbclass.h"
#include "tour_dat.h"

struct TOUR_DAT tour_dat, tour_dat_null;

void TOUR_DAT_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((long *)   &tour_dat.tou,  SQLLONG, 0);
            sqlin ((long *)   &tour_dat.lieferdat,  SQLLONG, 0);
            sqlin ((short *)   &tour_dat.posi,  SQLSHORT, 0);
    out_quest ((char *) &tour_dat.tou,2,0);
    out_quest ((char *) &tour_dat.lieferdat,2,0);
    out_quest ((char *) &tour_dat.posi,1,0);
    out_quest ((char *) &tour_dat.palette,3,0);
    out_quest ((char *) &tour_dat.soll_volumen,3,0);
    out_quest ((char *) &tour_dat.ist_volumen,3,0);
            cursor = sqlcursor ("select tour_dat.tou,  "
"tour_dat.lieferdat,  tour_dat.posi,  tour_dat.palette,  "
"tour_dat.soll_volumen,  tour_dat.ist_volumen from tour_dat "

#line 20 "tour_dat.rpp"
                                  "where tou = ? and lieferdat = ? and posi = ?");
    ins_quest ((char *) &tour_dat.tou,2,0);
    ins_quest ((char *) &tour_dat.lieferdat,2,0);
    ins_quest ((char *) &tour_dat.posi,1,0);
    ins_quest ((char *) &tour_dat.palette,3,0);
    ins_quest ((char *) &tour_dat.soll_volumen,3,0);
    ins_quest ((char *) &tour_dat.ist_volumen,3,0);
            sqltext = "update hwg set tour_dat.tou = ?,  "
"tour_dat.lieferdat = ?,  tour_dat.posi = ?,  tour_dat.palette = ?,  "
"tour_dat.soll_volumen = ?,  tour_dat.ist_volumen = ? "

#line 22 "tour_dat.rpp"
                                  "where tou = ? and lieferdat = ? and posi = ?";
            sqlin ((long *)   &tour_dat.tou,  SQLLONG, 0);
            sqlin ((long *)   &tour_dat.lieferdat,  SQLLONG, 0);
            sqlin ((short *)   &tour_dat.posi,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &tour_dat.tou,  SQLLONG, 0);
            sqlin ((long *)   &tour_dat.lieferdat,  SQLLONG, 0);
            sqlin ((short *)   &tour_dat.posi,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor ("select tou from tour_dat "
                                  "where tou = ? and lieferdat = ? and posi = ?");
            sqlin ((long *)   &tour_dat.tou,  SQLLONG, 0);
            sqlin ((long *)   &tour_dat.lieferdat,  SQLLONG, 0);
            sqlin ((short *)   &tour_dat.posi,  SQLSHORT, 0);
            del_cursor = sqlcursor ("delete from tour_dat "
                                  "where tou = ? and lieferdat = ? and posi = ?");
    ins_quest ((char *) &tour_dat.tou,2,0);
    ins_quest ((char *) &tour_dat.lieferdat,2,0);
    ins_quest ((char *) &tour_dat.posi,1,0);
    ins_quest ((char *) &tour_dat.palette,3,0);
    ins_quest ((char *) &tour_dat.soll_volumen,3,0);
    ins_quest ((char *) &tour_dat.ist_volumen,3,0);
            ins_cursor = sqlcursor ("insert into hwg (tou,  "
"lieferdat,  posi,  palette,  soll_volumen,  ist_volumen) "

#line 39 "tour_dat.rpp"
                                      "values "
                                      "(?,?,?,?,?,?)"); 

#line 41 "tour_dat.rpp"
}
