#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "best_bdf.h"

struct BEST_BDF best_bdf, best_bdf_null;

void BEST_BDF_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &best_bdf.mdn, 1, 0);
            ins_quest ((char *) &best_bdf.fil, 1, 0);
            ins_quest ((char *) &best_bdf.a, 3, 0);
            ins_quest ((char *) &best_bdf.bdf_term, 2, 0);
    out_quest ((char *) &best_bdf.a,3,0);
    out_quest ((char *) &best_bdf.bdf_status,1,0);
    out_quest ((char *) &best_bdf.bdf_term,2,0);
    out_quest ((char *) &best_bdf.fil,1,0);
    out_quest ((char *) &best_bdf.mdn,1,0);
    out_quest ((char *) &best_bdf.me,3,0);
    out_quest ((char *) &best_bdf.me_einh,1,0);
    out_quest ((char *) &best_bdf.melde_term,2,0);
            cursor = prepare_sql ("select best_bdf.a,  "
"best_bdf.bdf_status,  best_bdf.bdf_term,  best_bdf.fil,  best_bdf.mdn,  "
"best_bdf.me,  best_bdf.me_einh,  best_bdf.melde_term from best_bdf "

#line 29 "best_bdf.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ? "
                                  "and   bdf_term = ?");

    ins_quest ((char *) &best_bdf.a,3,0);
    ins_quest ((char *) &best_bdf.bdf_status,1,0);
    ins_quest ((char *) &best_bdf.bdf_term,2,0);
    ins_quest ((char *) &best_bdf.fil,1,0);
    ins_quest ((char *) &best_bdf.mdn,1,0);
    ins_quest ((char *) &best_bdf.me,3,0);
    ins_quest ((char *) &best_bdf.me_einh,1,0);
    ins_quest ((char *) &best_bdf.melde_term,2,0);
            sqltext = "update best_bdf set best_bdf.a = ?,  "
"best_bdf.bdf_status = ?,  best_bdf.bdf_term = ?,  best_bdf.fil = ?,  "
"best_bdf.mdn = ?,  best_bdf.me = ?,  best_bdf.me_einh = ?,  "
"best_bdf.melde_term = ? "

#line 35 "best_bdf.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ? "
                                  "and   bdf_term = ?";

            ins_quest ((char *) &best_bdf.mdn, 1, 0);
            ins_quest ((char *) &best_bdf.fil, 1, 0);
            ins_quest ((char *) &best_bdf.a, 3, 0);
            ins_quest ((char *) &best_bdf.bdf_term, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &best_bdf.mdn, 1, 0);
            ins_quest ((char *) &best_bdf.fil, 1, 0);
            ins_quest ((char *) &best_bdf.a, 3, 0);
            ins_quest ((char *) &best_bdf.bdf_term, 2, 0);
            test_upd_cursor = prepare_sql ("select auf from best_bdf "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ? "
                                  "and   bdf_term = ?");
            ins_quest ((char *) &best_bdf.mdn, 1, 0);
            ins_quest ((char *) &best_bdf.fil, 1, 0);
            ins_quest ((char *) &best_bdf.a, 3, 0);
            ins_quest ((char *) &best_bdf.bdf_term, 2, 0);
            del_cursor = prepare_sql ("delete from best_bdf "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ? "
                                  "and   bdf_term = ?");

            ins_quest ((char *) &best_bdf.mdn, 1, 0);
            ins_quest ((char *) &best_bdf.fil, 1, 0);
            ins_quest ((char *) &best_bdf.a, 3, 0);
            del_cursor_a = prepare_sql ("delete from best_bdf "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   a = ?");
    ins_quest ((char *) &best_bdf.a,3,0);
    ins_quest ((char *) &best_bdf.bdf_status,1,0);
    ins_quest ((char *) &best_bdf.bdf_term,2,0);
    ins_quest ((char *) &best_bdf.fil,1,0);
    ins_quest ((char *) &best_bdf.mdn,1,0);
    ins_quest ((char *) &best_bdf.me,3,0);
    ins_quest ((char *) &best_bdf.me_einh,1,0);
    ins_quest ((char *) &best_bdf.melde_term,2,0);
            ins_cursor = prepare_sql ("insert into best_bdf ("
"a,  bdf_status,  bdf_term,  fil,  mdn,  me,  me_einh,  melde_term) "

#line 73 "best_bdf.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?)"); 

#line 75 "best_bdf.rpp"
}
int BEST_BDF_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int BEST_BDF_CLASS::delete_a (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (del_cursor_a == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_a);
         return (sqlstatus);
}
