// WaitHandler.h: Schnittstelle f�r die Klasse CWaitHandler.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WAITHANDLER_H__2920C901_23ED_4C09_8067_B9DE087589EF__INCLUDED_)
#define AFX_WAITHANDLER_H__2920C901_23ED_4C09_8067_B9DE087589EF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "resrc1.h"
#include "dialog.h"
#include "Text.h"
#include "Edit.h"
#include "ProgressCtrl.h"

#define CString Text

class CWaitHandler :  
	  public CDialog  
{
private:
	CString			m_InfoText;
	CString			m_Title;
    int             m_Min;
    int             m_Max;
	int				m_Elements;
	COLORREF		m_DlgColor;
	COLORREF		m_TextColor;
	HBRUSH			m_DlgBrush;
	HBRUSH			m_TextBrush;
	CEdit			m_TextEdit;
	CProgressCtrl	m_Progress;

	CFont m_FatFont;
public:
	enum {Id = IDD_WAIT};

	void set_InfoText (LPSTR infoText)
	{
		m_InfoText = infoText;
	}

	LPSTR InfoText ()
	{
		return (LPSTR) m_InfoText;
	}

	void set_Title (LPSTR title)
	{
		m_Title = title;
	}

	LPSTR Title ()
	{
		return (LPSTR) m_Title;
	}

	void set_Elelemts (int elements)
	{
		m_Elements = elements;
	}

	CWaitHandler(HWND Parent=NULL);
	virtual ~CWaitHandler();
protected:
	virtual void AttachControls ();
	virtual BOOL OnInitDialog ();
	virtual BOOL PreTranslateMessage (MSG *msg);
	virtual HBRUSH OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor);

public:
	void SetRange (int min, int max);
	void StepIt ();
	void SetStep (int step);
	void ProcessEvent ();
	void Show ();
};

#endif // !defined(AFX_WAITHANDLER_H__2920C901_23ED_4C09_8067_B9DE087589EF__INCLUDED_)
