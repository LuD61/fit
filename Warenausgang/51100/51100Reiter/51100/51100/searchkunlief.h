#ifndef _SEARCHKUNLIEF_DEF
#define _SEARCHKUNLIEF_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

struct SKUNLIEF
{
      char adr [15];
	  char adr_nam1 [38];
	  char plz [22];
	  char ort1 [38];
	  char strasse [38];
};

class SEARCHKUNLIEF
{
       private :
           static ITEM uname1;
           static ITEM uort1;
           static ITEM uplz;
           static ITEM ustrasse;

           static ITEM iname1;
           static ITEM iort1;
           static ITEM iplz;
           static ITEM istrasse;

           static ITEM iline;

           static field _UbForm[];
           static form UbForm;

           static field _DataForm[];
           static form DataForm;

           static field _LineForm[];
           static form LineForm;

           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SKUNLIEF *skuntab;
           static struct SKUNLIEF skun;
           static int idx;
           static long kunanz;
           static CHQEX *Query;
           static int SearchField;
           static char *query;
           static int soadr_nam1; 
           static int soplz; 
           static int soort1; 
           static short mdn;
           static int kun;

           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHKUNLIEF ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  mdn = 0;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHKUNLIEF ()
           {
                  if (skuntab != NULL)
                  {
                      delete skuntab;
                      skuntab = NULL;
                  }
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           void SetQuery (char *q)
           {
               if (query == NULL || (strcmp (query, q) != 0))
               {
                      query = q;
                      if (Query != NULL)
                      {
                             delete Query;
                      }
                      if (skuntab != NULL)
                      {
                             delete skuntab;
                      }
               }   
           }

           char *GetQuery (void)
           {
                return query;
           }

           SKUNLIEF *GetSkun (void)
           {
               if (idx == -1) return NULL;
               return &skun;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           SKUNLIEF *GetNextSkun (void);
           SKUNLIEF *GetPriorSkun (void);
           SKUNLIEF *GetFirstSkun (void);
           SKUNLIEF *GetLastSkun (void);
           static void FillFormat (char *, int);
           static void FillVlines (char *, int);
           static void FillCaption (char *, int);
           static void FillRec (char *, int, int);
           static int sortadr_nam1 (const void *, const void *);
           static void SortAdr_nam1 (HWND);
           static int sortplz (const void *, const void *);
           static void SortPlz (HWND);
           static int sortort1 (const void *, const void *);
           static void SortOrt1 (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int Read (char *);
           int TestRec (short);
           void SetParams (HINSTANCE, HWND);
           void Search (short, int kun);
};  
#endif