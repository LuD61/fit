// Debug.cpp: Implementierung der Klasse CDebug.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "Debug.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CDebug *CDebug::Instance = NULL;

CDebug::CDebug()
{
	fp = NULL;
	strcpy (Name, "C:\\temp\\51100Reiter.dbg");

}

CDebug::~CDebug()
{
	if (fp != NULL)
	{
		Close ();
	}

}

CDebug *CDebug::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CDebug ();
	}
	return Instance;
}


void CDebug::Open ()
{
	fp = fopen (Name, "w");
}

void CDebug::Write ()
{
/*
	if (fp == NULL)
	{
		Open ();
	}
	if (fp != NULL)
	{
		fprintf (fp, "%s\n", Line);
		fflush (fp);
	}
*/
}

void CDebug::Close ()
{
	if (fp != NULL)
	{
		fclose (fp);
		fp = NULL;
	}
}
