#ifndef __AUFPS_DEF
#define __AUFPS_DEF

#define MAXLEN 40
#define MAXPOS 2000   // FS-379   von 5000 auf 2000,  Dimension von aufparr, reicht vollständig 
#define LPLUS 1
//LAC-9 soll_me und diff_me  mit reingenommen f+r Reiter Bestellvorschlag
//LAC-44 min_best dazu
struct AUFPS
{
       char posi [80];
	   long pos_id;
       char sa_kz_sint [80];
       char a [80];
       char a_kun [20];
       char lang_bez [100];
       char a_bz1 [80];
       char a_bz2 [80];
	   char soll_me[80]; 
	   char diff_me[80]; 
	   char last_me[80];
	   char last_lief_me[40]; //LAC-54
	   char last_lieferung[40]; //LAC-54
	   char bestellvorschlag[40]; //LAC-54
	   char grp_min_best[40]; //LAC-104
	   char last_pr_vk[80];
	   char min_best[80];
       char auf_me [80];
       char me_bz [80];
       char auf_vk_pr [80];
       char auf_lad_pr [80];
       char auf_lad_pr_prc [80];
       char marge [80];
       char basis_me_bz [80];
       char teil_smt [5];
       char me_einh_kun [5];
       char me_einh [5];
       char aufp_txt [9];
       char me_einh_kun1 [5];
       char auf_me1 [14];
       char inh1 [14];
       char me_einh_kun2 [5];
       char auf_me2 [14];
       char inh2 [14];
       char me_einh_kun3 [5];
       char auf_me3 [14];
       char inh3 [14];
	   char rab_satz [10];
	   char prov_satz [10];
       char lief_me [80];
	   char gruppe [9];
       char auf_vk_dm [80];
       char auf_lad_dm [80];
       char auf_vk_euro [80];
       char auf_lad_euro [80];
       char auf_vk_fremd [80];
       char auf_lad_fremd [80];
       char kond_art0 [20];
       char kond_art [20];
       char a_grund  [20];
       char posi_ext  [9];
	   char last_ldat [12];
       short ls_pos_kz;
       char  a_ers [16];
       short dr_folge;
       char  ls_charge [35];
       double charge_gew;
	   char aufschlag [5];
	   char aufschlag_wert [20];
	   short a_typ;
	   double a_leih;
	   short pos_txt_kz;
	   short userdef1; //WAL-70
	   double a_gew;
	   char bsd[80];
	   char bsd2[80];
	   char auf_lad_pr0 [80];
	   char smt[2];
	   char stk_karton[60];
	   char gew_karton[80];
	   short hwg; //140312
	   short wg; //LAC-9
	   BOOL KritMHD; //LAC-8
	   short sg;  //LAC-104  Staffelgruppe
	   double min_bestellmenge; //LAC-104 
	   double sg_min_bestellmenge; //LAC-104 
	   double sg_staffel; //LAC-104 
	   BOOL sg_ok; //LAC-104
	   char mhd[11]; //LAC-84
	   int posi_old; //LAC-124b
	   int ksys; 
	   BOOL a_kun_txt; //WAL-170
	   BOOL verkfrage; //WAL-177
	   int txt_ls;  //WAL-210
	   int txt_rech; //WAL-210
	   int auf_klst;
};
#endif