// ListHandler.h: Schnittstelle f�r die Klasse CListHandler.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LISTHANDLER_H__939C9EA0_6AFE_44AC_A366_675E08A3C6E5__INCLUDED_)
#define AFX_LISTHANDLER_H__939C9EA0_6AFE_44AC_A366_675E08A3C6E5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>
#include "Aufps.h"
#include "DataCollection.h"
#include "smtreader.h"


//LAC-9 A
#define REITERAKTAUF  853
#define REITERBESTELLVORSCHLAG  854
#define REITERLETZTEBESTELLUNG  855
#define REITERHWG1  861
#define REITERHWG2  862
#define REITERHWG3  863
#define REITERHWG4  864
#define REITERHWG5  865
#define REITERHWG6  866
#define REITERHWG7  867
#define REITERHWG8  868
#define REITERHWG9  869
#define REITERHWG10  870
#define REITERHWG11  871
#define REITERHWG12  872
#define REITERHWG13  873
#define REITERHWG14  874
#define REITERHWG15  875
#define REITERHWG16  876
#define REITERKRITISCHEMHD  877
#define REITERKOMMLDAT  878
#define REITERSTANDARDBESTELLUNG  879
//LAC-9 E


	struct AUFP_POSI 
	{
       int posi;
       int posi_old;
       double a ;
	};


class CListHandler  
{
public:
	enum LIST_MODE
	{
		AllEntries = 0,
		ValueEntries = 1,
	};
public:
	class CIdxItem
	{
		public:
			int IdxAll;
			int IdxValue;
			CIdxItem () 
			{
				IdxAll = 0;
				IdxValue = 0;
			}
			CIdxItem (int IdxAll, int IdxValue) 
			{
				this->IdxAll   = IdxAll;
				this->IdxValue = IdxValue;
			}
		
	};



	class CIdxCollection :public CDataCollection<CIdxItem *>
	{
	public:
		int GetIdxAll   (int IdxValue);
		int GetIdxValue (int IdxAll);
		CIdxItem *GetItemByAll (int IdxAll);
		CIdxItem *GetItemByValue (int IdxValue);
	};

private:
	LIST_MODE ActiveMode;
	BOOL LoadComplete;
	BOOL enableActivate;
	BOOL activated;
	CDataCollection<AUFPS *> StndAll;
	CDataCollection<AUFPS *> lBestAll; //LAC-9
	CDataCollection<AUFPS *> KritMhdAll; //LAC-84
	CDataCollection<AUFPS *> KommldatAll; //LAC-116
	CDataCollection<AUFPS *> BestVorschlagAll;
	CDataCollection<AUFPS *> HWG1All; //LAC-9
	CDataCollection<AUFPS *> HWG2All; //LAC-9
	CDataCollection<AUFPS *> HWG3All; //LAC-9
	CDataCollection<AUFPS *> HWG4All; //LAC-9
	CDataCollection<AUFPS *> HWG5All; //LAC-9
	CDataCollection<AUFPS *> HWG6All; //LAC-9
	CDataCollection<AUFPS *> HWG7All; //LAC-9
	CDataCollection<AUFPS *> HWG8All; //LAC-9
	CDataCollection<AUFPS *> HWG9All; //LAC-9
	CDataCollection<AUFPS *> HWG10All; //LAC-9
	CDataCollection<AUFPS *> HWG11All; 
	CDataCollection<AUFPS *> HWG12All; 
	CDataCollection<AUFPS *> HWG13All; 
	CDataCollection<AUFPS *> HWG14All; 
	CDataCollection<AUFPS *> HWG15All; 
	CDataCollection<AUFPS *> HWG16All; 
	BOOL activated_lBest; //LAC-9
	BOOL activated_KritMhd; //LAC-84
	BOOL activated_Kommldat; //LAC-116
	BOOL activated_BestVorschlag; 
	BOOL activated_HWG1; //LAC-9
	BOOL activated_HWG2; //LAC-9
	BOOL activated_HWG3; //LAC-9
	BOOL activated_HWG4; //LAC-9
	BOOL activated_HWG5; //LAC-9
	BOOL activated_HWG6; //LAC-9
	BOOL activated_HWG7; //LAC-9
	BOOL activated_HWG8; //LAC-9
	BOOL activated_HWG9; //LAC-9
	BOOL activated_HWG10; //LAC-9
	BOOL activated_HWG11;
	BOOL activated_HWG12;
	BOOL activated_HWG13;
	BOOL activated_HWG14;
	BOOL activated_HWG15;
	BOOL activated_HWG16;
	CDataCollection<AUFPS *> StndValue;
	CIdxCollection IdxCollection;
	CSmtReader *m_SmtReader;
	static CListHandler *instance;
protected:
	CListHandler();
	virtual ~CListHandler();
public:

	void InitElementslBestAll (void); //LAC-132
	void set_SmtReader (CSmtReader *smtReader)
	{
		m_SmtReader = smtReader;
	}
	void EnableActivate (BOOL enableActivate=TRUE)
	{
		this->enableActivate = enableActivate;
	}

	BOOL CanActivate ()
	{
		return enableActivate;
	}

	void SetLoadComplete (BOOL LoadComplete=TRUE)
	{
		this->LoadComplete = LoadComplete;
	}


	BOOL GetLoadComplete ()
	{
		return LoadComplete;
	}

	CDataCollection<AUFPS *> *GetStndAll ()
	{
		return &StndAll;
	}
	CDataCollection<AUFPS *> *GetlBestAll () //LAC-9
	{
		return &lBestAll;
	}
	CDataCollection<AUFPS *> *GetKritMhdAll () //LAC-84
	{
		return &KritMhdAll;
	}
	CDataCollection<AUFPS *> *GetKommldatAll () //LAC-116
	{
		return &KommldatAll;
	}
	CDataCollection<AUFPS *> *GetBestVorschlagAll ()
	{
		return &BestVorschlagAll;
	}
	CDataCollection<AUFPS *> *GetHWG1All () //LAC-9
	{
		return &HWG1All;
	}
	CDataCollection<AUFPS *> *GetHWG2All () //LAC-9
	{
		return &HWG2All;
	}
	CDataCollection<AUFPS *> *GetHWG3All () //LAC-9
	{
		return &HWG3All;
	}
	CDataCollection<AUFPS *> *GetHWG4All () //LAC-9
	{
		return &HWG4All;
	}
	CDataCollection<AUFPS *> *GetHWG5All () //LAC-9
	{
		return &HWG5All;
	}
	CDataCollection<AUFPS *> *GetHWG6All () //LAC-9
	{
		return &HWG6All;
	}
	CDataCollection<AUFPS *> *GetHWG7All () //LAC-9
	{
		return &HWG7All;
	}
	CDataCollection<AUFPS *> *GetHWG8All () //LAC-9
	{
		return &HWG8All;
	}
	CDataCollection<AUFPS *> *GetHWG9All () //LAC-9
	{
		return &HWG9All;
	}
	CDataCollection<AUFPS *> *GetHWG10All () //LAC-9
	{
		return &HWG10All;
	}
	CDataCollection<AUFPS *> *GetHWG11All () 
	{
		return &HWG11All;
	}
	CDataCollection<AUFPS *> *GetHWG12All () 
	{
		return &HWG12All;
	}
	CDataCollection<AUFPS *> *GetHWG13All () 
	{
		return &HWG13All;
	}
	CDataCollection<AUFPS *> *GetHWG14All () 
	{
		return &HWG14All;
	}
	CDataCollection<AUFPS *> *GetHWG15All () 
	{
		return &HWG15All;
	}
	CDataCollection<AUFPS *> *GetHWG16All () 
	{
		return &HWG16All;
	}

	CDataCollection<AUFPS *> *GetStndValue ()
	{
		return &StndValue;
	}

	static CListHandler *GetInstance ();
	void DestroyInstance ();
	void Init (int );
	void InitAufMe ();
	void SetNewPosi (); //LAC-9
	BOOL WriteNewPosi (short dmdn, short dfil, int dauf); //LAC-124b
	int Activate (AUFPS aufparr[MAXPOS],int aufpanz, int *idx);
	void SetStndAllA (AUFPS *Aufps);
	void SetStndValueA (AUFPS *Aufps, int posi, int idxAll);
	int GetPosiStndValueA (AUFPS *Aufps); //LAC-124
	void DropStndValueA (AUFPS *Aufps, int idxAll);
	int GetAllPosiA (AUFPS *Aufps);
	int Switch (AUFPS aufparr[MAXPOS], int *idx);
	int HoleStndValue (AUFPS aufparr[MAXPOS], int *idx); //LAC-9
	int Activate_lBest (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dauf); //LAC-9
	int Activate_KritMhd (AUFPS aufparr[MAXPOS],int aufpanz, int *idx); //LAC-84
	int Activate_Kommldat (AUFPS aufparr[MAXPOS],int aufpanz, int *idx); //LAC-116
	int Activate_BestVorschlag (AUFPS aufparr[MAXPOS],int aufpanz, int *idx); 
	int Activate_HWG1 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); //LAC-9
	int Activate_HWG2 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); //LAC-9
	int Activate_HWG3 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); //LAC-9
	int Activate_HWG4 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); //LAC-9
	int Activate_HWG5 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); //LAC-9
	int Activate_HWG6 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); //LAC-9
	int Activate_HWG7 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); //LAC-9
	int Activate_HWG8 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); //LAC-9
	int Activate_HWG9 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); //LAC-9
	int Activate_HWG10 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); //LAC-9
	int Activate_HWG11 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); 
	int Activate_HWG12 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); 
	int Activate_HWG13 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); 
	int Activate_HWG14 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); 
	int Activate_HWG15 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); 
	int Activate_HWG16 (AUFPS aufparr[MAXPOS],int aufpanz, int *idx, int dhwg, int dwg); 
	void SetStndAllA_lBest (AUFPS *Aufps); //LAC-9
	void SetStndAllA_KritMhd (AUFPS *Aufps); //LAC-84
	void SetStndAllA_Kommldat (AUFPS *Aufps); //LAC-116
	void SetStndAllA_BestVorschlag (AUFPS *Aufps); 
	void SetStndAllA_HWG1 (AUFPS *Aufps); //LAC-9
	void SetStndAllA_HWG2 (AUFPS *Aufps); //LAC-9
	void SetStndAllA_HWG3 (AUFPS *Aufps); //LAC-9
	void SetStndAllA_HWG4 (AUFPS *Aufps); //LAC-9
	void SetStndAllA_HWG5 (AUFPS *Aufps); //LAC-9
	void SetStndAllA_HWG6 (AUFPS *Aufps); //LAC-9
	void SetStndAllA_HWG7 (AUFPS *Aufps); //LAC-9
	void SetStndAllA_HWG8 (AUFPS *Aufps); //LAC-9
	void SetStndAllA_HWG9 (AUFPS *Aufps); //LAC-9
	void SetStndAllA_HWG10 (AUFPS *Aufps); //LAC-9
	void SetStndAllA_HWG11 (AUFPS *Aufps); 
	void SetStndAllA_HWG12 (AUFPS *Aufps); 
	void SetStndAllA_HWG13 (AUFPS *Aufps); 
	void SetStndAllA_HWG14 (AUFPS *Aufps); 
	void SetStndAllA_HWG15 (AUFPS *Aufps); 
	void SetStndAllA_HWG16 (AUFPS *Aufps); 
	void Update (int idx, AUFPS *aufps);
	double GetSmtPart ();
	void SortByPosi (CDataCollection<AUFPS *> *StndCollection);
	static int ComparePosi (const void *el1, const void *el2);

};

#endif // !defined(AFX_LISTHANDLER_H__939C9EA0_6AFE_44AC_A366_675E08A3C6E5__INCLUDED_)
