#ifndef _MO_KOMPL_DEF
#define _MO_KOMPL_DEF
#include "mo_progcfg.h"



class KompAuf
{
        private :
            short mdntab[1000]; 
            short filtab[1000];
            long auftab[1000];
            int aufanz;
            short mdntablock[1000]; 
            short filtablock[1000];
            long auftablock[1000];
            int aufanzlock;
            long drklstab [1000];
            int drklsanz;
            int auf_a_sort; 
			BOOL IsGroup;
			long max_auf;
            static PROG_CFG *ProgCfg; 
        public :
			static BOOL TxtByDefault;
			static BOOL RunLs;
			static BOOL SaveDefault;
			static BOOL WithAufb;
			static char drformat [20];
			static char dformat [20];
			static char aufbformat [20];

			static char drformatf [20];
			static char drEpAuftrag [20];
			static char drEpAuftragf [20];
			static char drEpAuftrag2 [20];
			static char drEpAuftrag2f [20];
			static char drTkListen [20];
			static char drTkListenf [20];
			static char drWAHListe [20];
			static char drWAHListef [20];


			BOOL ChoicePrinter;
			BOOL AngText;
            KompAuf () : auf_a_sort (0), IsGroup (FALSE), aufanz (0), aufanzlock (0)
            {
				strcpy (drformat, "51100"); 
				strcpy (dformat, "51100"); 
                ProgCfg = NULL;
				ChoicePrinter = TRUE;
				AngText = TRUE;
            }

            void SetProgCfg (PROG_CFG *ProgCfg)
            {
                this->ProgCfg = ProgCfg;
            }

			void SetDrFormat (char *format)
			{
				strcpy (drformat, format);
			}

			long Getmax_auf (void)
			{
				return max_auf;
			}

            void SetAbt (BOOL);
            void SetDrkKun (BOOL);
            void SetLuKun (BOOL);
            void SetKompDefault (int);
            void SetDrkLsSperr (BOOL);
            void SetLiefMePar (int);
            void SetLog (int);
            void SetKlstPar (int);
            void SetLutzMdnPar (int);
            void SetSplitLs (int);
            void SetLieferdatToKommdat (BOOL);
            void BearbKomplett (void);
            int  SetKomplett (HWND);
            long GenLsNr (short, short);
            long GenLsNr0 (short, short);
            long GenAufNr (short, short);
            long GenAufNr0 (short, short);
            void AufktoLsk (void);
            void AufktoAufk (void);
            void aufpttolspt (void);
            long GenLspTxt0 (void);
			BOOL TxtNrExist (long);
            long GenLspTxt (void);
            long GenAufpTxt0 (void);
			BOOL AufpTxtNrExist (long);
            long GenAufpTxt (void);
            void AufptoLsp (long);
            void angpttoaufpt ();
            double GetGew (void);
            double GetAufMeVgl(void);
            void TeilSmttoLs (void);
            void TeilSmttoAuf (int);
            void ProdMdntoAuf (int);
            int Geteinz_ausw (void);
            void AuftoLs (void);
            void ShowLocks (HWND);
            void ShowNoDrk (HWND hWnd);
			void InitAufanz (void);
			void KompParams (void);
            void AuftoLsGroup (char *);
            void Getauf_a_sort (void);
            void AuftoPid (void);
            void InitPid (void);
            void K_Liste_Direct ();
            void K_Liste_Fax (void);
            void Print_Liste ();
            void K_Liste ();
			void Kom_Liste_Rab ();
			static BOOL IsLlFormat ();
            void K_LlListe ();
			void K_LlListeGroup (void);
            void K_ListeGroup (short, short, short, short,long, long,
							   char *, char *, short, short, long, long,
							   short, short, long, long, short);
			int FaxAuf (HWND, short, short, long);

            void K_ListeGroup0 (char *);
            int  SetFak_typ (HWND hWnd);
            void FreeAuf (void);
            void LockAuf (void);
            void TestFree (void);
            void K_ListeTS (void);
            void PrintAufbest (void);
            void DrkLsTS (void);
            void AuftoLsTS (void);
            void AufToSmtDrk (int, long *);
			long GetGruppe (void);
            BOOL BsdArtikel (double);
            void BucheBsd (double, double,  double);
            void SetBsdKz (int);
            void AngtoAuf (void);
            void AngktoAufk (void);
            void AngptoAufp (long);
            static long GetLagerort (long, double);
		    void GetEinhIst (void);
};
#endif
