#include "ControlPlugins.h"
#include "Text.h"

#define CString Text

HANDLE CControlPlugins::Controls = NULL;

CControlPlugins::CControlPlugins ()
{
    pGetPosTxtKz =NULL;
	char *bws;

	bws = getenv ("BWS");

	CString DllName;
	if (bws != NULL)
	{
		DllName.Format ("%s\\bin\\Controls.dll", bws);
	}
	else
	{
		DllName = "preisewa.dll";
	}
	Controls = LoadLibrary (DllName.GetBuffer ());
	if (Controls != NULL)
	{
		pGetPosTxtKz = (int (*) (int)) 
					GetProcAddress ((HMODULE) Controls, "GetPosTxtKz");
		m_ShowOrderSum = (void  (*) (HWND, CSmtTable*))
		 			     GetProcAddress ((HMODULE) Controls, "ShowOrderSum");
		m_HideOrderSum = (void  (*) ())
		 			     GetProcAddress ((HMODULE) Controls, "HideOrderSum");
		m_CloseOrderSum = (void  (*) ())
		 			     GetProcAddress ((HMODULE) Controls, "CloseOrderSum");
	}
}

int CControlPlugins::GetPosTxtKz (int PosTxtKz)
{
	if (pGetPosTxtKz != NULL)
	{
		return (*pGetPosTxtKz) (PosTxtKz);
	}
	return PosTxtKz;
}

BOOL CControlPlugins::IsActive ()
{
	if (pGetPosTxtKz != NULL)
	{
		return TRUE;
	}
	return FALSE;
}

void CControlPlugins::ShowOrderSum (HWND parent)
{
	if (m_ShowOrderSum != NULL)
	{
		(*m_ShowOrderSum) (parent, &m_SmtTable);
	}
}

void CControlPlugins::HideOrderSum ()
{
	if (m_HideOrderSum != NULL)
	{
		(*m_HideOrderSum) ();
	}
}

void CControlPlugins::CloseOrderSum ()
{
	if (m_CloseOrderSum != NULL)
	{
		(*m_CloseOrderSum) ();
	}
}
