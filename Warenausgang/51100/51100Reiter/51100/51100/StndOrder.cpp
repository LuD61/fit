#include <comdef.h>
#include "Text.h"
#include "StndOrder.h"

#define CString Text

typedef struct
{
		OLECHAR FAR **Name;
		DISPID *Id;
} NAID;


CStndOrder::CStndOrder ()
{
	iStndOrder = NULL;
	IdsOK = FALSE;
	NPutSortMode =  L"SortMode";
	NPutRdOptimize =  L"RdOptimize";
	NPutMdn =  L"Mdn";
	NPutFil = L"Fil";;
	NPutKunFil = L"KunFil";;
	NPutKun = L"Kun";;
	NPutAktAuf = L"AktAuf";;
	NPutVisible = L"Visible";;
	NSetWindow = L"SetWindow";;
	NSetKeys = L"SetKeys";;
	NLoad = L"Load";;
	NGetFirstRecord = L"GetFirstRecord";
	NGetNextRecord = L"GetNextRecord";
	NRegisterClient = L"RegisterClient";
	NUnregisterClient = L"UnregisterClient";
}

CStndOrder::~CStndOrder ()
{
}

CStndOrder& CStndOrder::operator= (IDispatch *idpt)
{
	Init (idpt);
	return *this;
}


BOOL CStndOrder::Init (IDispatch *idp)
{
	HRESULT hr;
	iStndOrder = idp;

	if (IdsOK) return TRUE;
	NAID mTab[] = {
			       &NPutSortMode, &IdPutSortMode,
			       &NPutRdOptimize, &IdPutRdOptimize,
			       &NPutMdn, &IdPutMdn,
			       &NPutFil, &IdPutFil,
			       &NPutKunFil, &IdPutKunFil,
			       &NPutKun, &IdPutKun,
			       &NPutAktAuf, &IdPutAktAuf,
//			       &NPutVisible, &IdPutVisible,
			       &NSetWindow, &IdSetWindow,
			       &NSetKeys, &IdSetKeys,
			       &NLoad, &IdLoad,
				   &NGetFirstRecord, &IdGetFirstRecord,
				   &NGetNextRecord, &IdGetNextRecord,
				   &NRegisterClient, &IdRegisterClient,
				   &NUnregisterClient, &IdUnregisterClient,
				   NULL, NULL
	};

	try
	{
		for (int i = 0; mTab[i].Name != NULL; i ++)
		{
			hr = iStndOrder->GetIDsOfNames (IID_NULL, mTab[i].Name, 
				  					  1, LOCALE_USER_DEFAULT, mTab[i].Id);
			if (FAILED (hr))
			{
				return FALSE;
			}
		}
	}
	catch (...)
	{
		return FALSE;
	}
	IdsOK = TRUE;
    return TRUE;
}

BOOL CStndOrder::Failed ()
{
	return !IdsOK;
}

HRESULT CStndOrder::PutSortMode (CClientOrder::STD_SORT SortMode)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_I2;
	dp.rgvarg[0].iVal = SortMode;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;
	hr = iStndOrder->Invoke (IdPutSortMode, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_PROPERTYPUT, &dp,
					   &pResult, NULL, NULL);      ;
	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	delete dp.rgvarg;
	return hr;
}


HRESULT CStndOrder::PutRdOptimize (CClientOrder::STD_RDOPTIMIZE RdOptimize)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_I2;
	dp.rgvarg[0].iVal = RdOptimize;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;
	hr = iStndOrder->Invoke (IdPutRdOptimize, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_PROPERTYPUT, &dp,
					   &pResult, NULL, NULL);      ;
	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	delete dp.rgvarg;
	return hr;
}

HRESULT CStndOrder::PutMdn (short Mdn)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_I2;
	dp.rgvarg[0].iVal = Mdn;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;
	hr = iStndOrder->Invoke (IdPutMdn, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_PROPERTYPUT, &dp,
					   &pResult, NULL, NULL);      ;
	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	delete dp.rgvarg;
	return hr;
}

HRESULT CStndOrder::PutFil (short Fil)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_I2;
	dp.rgvarg[0].iVal = Fil;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;
	hr = iStndOrder->Invoke (IdPutFil, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_PROPERTYPUT, &dp,
					   &pResult, NULL, NULL);      ;
	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	delete dp.rgvarg;
	return hr;
}

HRESULT CStndOrder::PutKunFil (short KunFil)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_I2;
	dp.rgvarg[0].iVal = KunFil;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;
	hr = iStndOrder->Invoke (IdPutKunFil, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_PROPERTYPUT, &dp,
					   &pResult, NULL, NULL);      ;
	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	delete dp.rgvarg;
	return hr;
}

HRESULT CStndOrder::PutKun (long Kun)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_I4;
	dp.rgvarg[0].lVal = Kun;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;
	hr = iStndOrder->Invoke (IdPutKun, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_PROPERTYPUT, &dp,
					   &pResult, NULL, NULL);      ;
	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	delete dp.rgvarg;
	return hr;
}

HRESULT CStndOrder::PutAktAuf (long AktAuf)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_I4;
	dp.rgvarg[0].lVal = AktAuf;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;
	hr = iStndOrder->Invoke (IdPutAktAuf, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_PROPERTYPUT, &dp,
					   &pResult, NULL, NULL);      ;
	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	delete dp.rgvarg;
	return hr;
}


HRESULT CStndOrder::PutVisible (BOOL Visible)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_BOOL;
	dp.rgvarg[0].boolVal = Visible;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;
	hr = iStndOrder->Invoke (IdPutVisible, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_PROPERTYPUT, &dp,
					   &pResult, NULL, NULL);      ;
	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	delete dp.rgvarg;
	return hr;
}



HRESULT CStndOrder::SetWindow ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;

    VARIANT FAR  pResult;  
	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;
	hr = iStndOrder->Invoke (IdSetWindow, IID_NULL, LOCALE_USER_DEFAULT,
					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);
	return hr;
}

HRESULT CStndOrder::SetKeys (short mdn, short fil, short kun_fil, long kun, long akt_auf, short client)
{
	HRESULT hr;
    DISPPARAMS FAR dp;

    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[6];
	dp.rgvarg[5].vt = VT_I2;
	dp.rgvarg[5].iVal = mdn;
	dp.rgvarg[4].vt = VT_I2;
	dp.rgvarg[4].iVal = fil;
	dp.rgvarg[3].vt = VT_I2;
	dp.rgvarg[3].iVal = kun_fil;
	dp.rgvarg[2].vt = VT_I4;
	dp.rgvarg[2].lVal = kun;
	dp.rgvarg[1].vt = VT_I4;
	dp.rgvarg[1].lVal = akt_auf;
	dp.rgvarg[0].vt = VT_I2;
	dp.rgvarg[0].iVal = client;
	dp.cArgs = 6;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;

	hr = iStndOrder->Invoke (IdSetKeys, IID_NULL, LOCALE_USER_DEFAULT,
					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);
	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	return hr;
}

HRESULT CStndOrder::Load (short client)
{
	HRESULT hr;
    DISPPARAMS FAR dp;

    VARIANT FAR  pResult;  
	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_I2;
	dp.rgvarg[0].iVal = client;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;
	hr = iStndOrder->Invoke (IdLoad, IID_NULL, LOCALE_USER_DEFAULT,
					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);
	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	return hr;
}

/*
template <>
CArt* CStndOrder::GetStammArtikel (LPCTSTR Key, int client)
{
	_bstr_t bKey = Key;
	return GetStammArtikel (bKey, client);
}

template <>
CArt* CStndOrder::GetStammArtikel (LPTSTR Key, int client)
{
	_bstr_t bKey = Key;
	return GetStammArtikel (bKey, client);
}
*/


CCAufps* CStndOrder::GetFirstRecord (short client)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_I2;
	dp.rgvarg[0].iVal = client;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;

	hr = iStndOrder->Invoke (IdGetFirstRecord, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	CAufps = pResult.pdispVal;
	return &CAufps;
}

CCAufps* CStndOrder::GetNextRecord (short client)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_I2;
	dp.rgvarg[0].iVal = client;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;

	hr = iStndOrder->Invoke (IdGetNextRecord, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	CAufps = pResult.pdispVal;
	return &CAufps;
}


short CStndOrder::RegisterClient ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iStndOrder->Invoke (IdRegisterClient, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.iVal;
}

HRESULT CStndOrder::UnregisterClient (short client)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = new VARIANT[1];
	dp.rgvarg[0].vt = VT_I2;
	dp.rgvarg[0].iVal = client;
	dp.cArgs = 1;
	dp.rgdispidNamedArgs = NULL;
	dp.cNamedArgs = 0;
	hr = iStndOrder->Invoke (IdUnregisterClient, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_PROPERTYPUT, &dp,
					   &pResult, NULL, NULL);      ;
	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	delete dp.rgvarg;
	return hr;
}
