#ifndef _MO_STDA_DEF
#define _MO_STDA_DEF
#include "stdfilaufk.h"
#include "stdfilaufp.h"
#include "searchstdfilk.h"

class StndAuf {
         private :
             HANDLE hMainInst;
			 int SortMode;
			 BOOL UpdError;
			 STDFILAUFP_CLASS Stdfilaufp;
         public :
			 BOOL StndDirect;
			 StndAuf () : SortMode (0), UpdError (FALSE), StndDirect (FALSE)
            {
            }
 
			void CleanUpdError (void)
			{
				UpdError = FALSE;
			}

            void SetSortMode (int mode)
			{
				SortMode = max (0, mode);
			}
            int StdAuftrag (HWND, short, short, long, short);
            void UpdStdAuftrag (short, short, long, short, double, double, double, int, char *);
            double GetStda (void);
            double GetStdme (void);
            double GetStdpr_vk (void);
            BOOL GetStdRow (int, double *, double *, double *);

            int StdFilAuftrag (HWND, HWND, short, short, char *);
            int ReadStdFilAuftrag (HWND, short, short, SSTDAUFK *);
			int letzteBestellungLaden (HWND hWnd, short mdn, short fil, long kun, short kun_fil); //LAC-9
			BOOL GetletztBestRow (int row, double *a, double *me, double *pr_vk); //LAC-9
};
#endif