struct GR_ZUORD {
   short     gr;
   char      gr_bz1[25];
   char      gr_bz2[25];
   short     mdn;
   char      smt_kz[2];
};

extern struct GR_ZUORD _gr_zuord, _gr_zuord_null;

class GR_CLASS 
{
       private :
               struct GR_ZUORD gr_zuord;
               short cursor_gr;
               short test_upd_cursor;
               short upd_cursor;
               short ins_cursor;
               short del_cursor;

       public :
               GR_CLASS ()
               {
                         cursor_gr       = -1;
                         test_upd_cursor = -1;
                         upd_cursor      = -1;
                         ins_cursor      = -1;
                         del_cursor      = -1;
               }

               int lese_gr (short, short);
               int lese_gr (void);
               int ShowAllGr (short);
};
