#ifndef _MO_TAST_DEF
#define _MO_TAST_DEF

class EINGABE : public DB_CLASS
{
          private :
			  HWND hMainWindow;
			  HINSTANCE hInstance;
			  HWND hWnd;
              mfont *Font;
			  DWORD currentfield;
          public :
			  EINGABE ();
 		      void ProcessMessages (void);
              static void ShowDlg (HDC, form *);
              static void SetArtikel (HWND); 
              static void SetAChoise (HWND); 
              static DWORD IsTast (DWORD);
              static void SetCurrentTast (DWORD);
              static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
              void OpenWindow (HANDLE, HWND);
              void MoveWindow (void);
			  void SetCurrentID (DWORD);
			  void SetCurrentName (char *);
			  void EnableID (DWORD, BOOL);
			  void EnableName (char *, BOOL);
			  void CheckID (DWORD, BOOL);
			  void CheckName (char *, BOOL);
              void VLines (void);
              void FillAbtBox (void);
              void FillArtikelBox (void);
};
#endif

