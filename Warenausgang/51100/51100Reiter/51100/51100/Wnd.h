// Wnd.h: Schnittstelle f�r die Klasse CWnd.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WND_H__06191CC6_A969_475C_9667_E06A79B61A76__INCLUDED_)
#define AFX_WND_H__06191CC6_A969_475C_9667_E06A79B61A76__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>
#include "font.h"

class CWnd  
{
public:
	CWnd();
	virtual ~CWnd();

protected:
	HWND m_hWnd;
	HWND m_hParent;

public:
	HWND Parent ()
	{
		return m_hParent;
	}

	CWnd operator= (HWND hwnd);
	operator HWND () const;
	void SetParent (CWnd& parent);
	void SetParent (HWND parent);
	void Attach (HWND hWnd);
	void SetWindowText (LPSTR windowtext);
	void Destroy ();
	void SetFont (CFont& Font);
};

#endif // !defined(AFX_WND_H__06191CC6_A969_475C_9667_E06A79B61A76__INCLUDED_)
