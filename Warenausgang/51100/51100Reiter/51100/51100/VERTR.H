struct VERTR {
   short     mdn;
   short     fil;
   long      vertr;
   long      adr;
   char      bank_nam[37];
   long      blz;
   char      kto_nr[17];
   short     hausbank;
   char      frei_txt1[65];
   char      vertr_krz[17];
   char      vertr_gebiet[25];
   double    norm_ums_tdm;
   double    norm_ums_p;
   double    akt_ums_tdm;
   double    akt_ums_p;
   double    son_ums_tdm;
   double    son_ums_p;
   double    bon_grenz_tdm;
   double    bon_grenz_p;
   double    jr_plan_ums;
   char      statk_period[2];
   short     delstatus;
};

extern struct VERTR vertr, vertr_null;

class VERTR_CLASS 
{
       private :
               int qschange;
               char qstring0[0x1000];
               short cursor_vertr;
               short cursor_ausw;
               void prepare_vertr (void);

       public :
               VERTR_CLASS ()
               {
                         cursor_vertr      = -1;
						 cursor_ausw       = -1;
                         strcpy (qstring0, "Start");
               }

               int lese_vertr (short, short, long);
               int lese_vertr (void);
               int prep_awcursor (char *);
               int PrepareQuery (form *, char *[]);
               int ShowBuQuery (HWND, int); 
};
