#include <windows.h>
#include "searchkun.h"
#include "strfkt.h"
#include "mo_wmess.h"
#include "lbox.h"


struct SKUN *SEARCHKUN::skuntab = NULL;
struct SKUN SEARCHKUN::skun;
int SEARCHKUN::idx = -1;
long SEARCHKUN::kunanz;
CHQEX *SEARCHKUN::Query = NULL;
DB_CLASS SEARCHKUN::DbClass; 
HINSTANCE SEARCHKUN::hMainInst;
HWND SEARCHKUN::hMainWindow;
HWND SEARCHKUN::awin;
int SEARCHKUN::SearchField = 1;
int SEARCHKUN::sokun = 1; 
int SEARCHKUN::soadr_nam1 = 1; 
int SEARCHKUN::soplz = 1; 
int SEARCHKUN::soort1 = 1; 
int SEARCHKUN::sokun_krz1 = 1; 
short SEARCHKUN::mdn = 0; 
char *SEARCHKUN::query = NULL;

int SEARCHKUN::sortkun (const void *elem1, const void *elem2)
{
	      struct SKUN *el1; 
	      struct SKUN *el2; 

	      el1 = (struct SKUN *) elem1;
		  el2 = (struct SKUN *) elem2;
          if (atol (el1->kun) > atol (el2->kun))
          {
              return 1 * sokun;
          }
          if (atol (el1->kun) < atol (el2->kun))
          {
              return -1 * sokun;
          }
	      return 0 * sokun;
}

void SEARCHKUN::SortKun (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUN),
				   sortkun);
       sokun *= -1;
}

int SEARCHKUN::sortadr_nam1 (const void *elem1, const void *elem2)
{
	      struct SKUN *el1; 
	      struct SKUN *el2; 

		  el1 = (struct SKUN *) elem1;
		  el2 = (struct SKUN *) elem2;
          clipped (el1->adr_nam1);
          clipped (el2->adr_nam1);
          if (strlen (el1->adr_nam1) == 0 &&
              strlen (el2->adr_nam1) == 0)
          {
              return 0;
          }
          if (strlen (el1->adr_nam1) == 0)
          {
              return -1;
          }
          if (strlen (el2->adr_nam1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->adr_nam1,el2->adr_nam1) * soadr_nam1);
}

void SEARCHKUN::SortAdr_nam1 (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUN),
				   sortadr_nam1);
       soadr_nam1 *= -1;
}

int SEARCHKUN::sortplz (const void *elem1, const void *elem2)
{
	      struct SKUN *el1; 
	      struct SKUN *el2; 

		  el1 = (struct SKUN *) elem1;
		  el2 = (struct SKUN *) elem2;
          clipped (el1->plz);
          clipped (el2->plz);
          if (strlen (el1->plz) == 0 &&
              strlen (el2->plz) == 0)
          {
              return 0;
          }
          if (strlen (el1->plz) == 0)
          {
              return -1;
          }
          if (strlen (el2->plz) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->plz,el2->plz) * soplz);
}

void SEARCHKUN::SortPlz (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUN),
				   sortplz);
       soplz *= -1;
}

int SEARCHKUN::sortort1 (const void *elem1, const void *elem2)
{
	      struct SKUN *el1; 
	      struct SKUN *el2; 

		  el1 = (struct SKUN *) elem1;
		  el2 = (struct SKUN *) elem2;
          clipped (el1->ort1);
          clipped (el2->ort1);
          if (strlen (el1->ort1) == 0 &&
              strlen (el2->ort1) == 0)
          {
              return 0;
          }
          if (strlen (el1->ort1) == 0)
          {
              return -1;
          }
          if (strlen (el2->ort1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->ort1,el2->ort1) * soort1);
}

void SEARCHKUN::SortOrt1 (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUN),
				   sortort1);
       soort1 *= -1;
}

int SEARCHKUN::sortkun_krz1 (const void *elem1, const void *elem2)
{
	      struct SKUN *el1; 
	      struct SKUN *el2; 

		  el1 = (struct SKUN *) elem1;
		  el2 = (struct SKUN *) elem2;
          clipped (el1->kun_krz1);
          clipped (el2->kun_krz1);
          if (strlen (el1->kun_krz1) == 0 &&
              strlen (el2->kun_krz1) == 0)
          {
              return 0;
          }
          if (strlen (el1->kun_krz1) == 0)
          {
              return -1;
          }
          if (strlen (el2->kun_krz1) == 0)
          {
              return 1;
          }
	      return (strcmp (el1->kun_krz1,el2->kun_krz1) * sokun_krz1);
}

void SEARCHKUN::SortKunKrz1 (HWND hWnd)
{
   	   qsort (skuntab, kunanz, sizeof (struct SKUN),
				   sortkun_krz1);
       sokun_krz1 *= -1;
}

void SEARCHKUN::SortLst (int Col, HWND hWnd)
/**
Procedure zum Sortiern der Liste.
**/
{
       switch (Col)
       {
              case 0 :
                  SortKun (hWnd);
                  SearchField = 0;
                  break;
              case 1 :
                  SortAdr_nam1 (hWnd);
                  SearchField = 1;
                  break;
              case 2 :
                  SortPlz (hWnd);
                  SearchField = 2;
                  break;
              case 3 :
                  SortOrt1 (hWnd);
                  SearchField = 3;
                  break;
              case 4 :
                  SortKunKrz1 (hWnd);
                  SearchField = 4;
                  break;
              default :
                  return;
       }
       UpdateList ();
}

void SEARCHKUN::FillFormat (char *buffer)
{
         sprintf (buffer, "%s %s %s",
                          "%d", 
                          "%s",
                          "%s");
}


void SEARCHKUN::FillCaption (char *buffer)
{
	    sprintf (buffer, " %10s  %-37s  %-21s  %-37s  %-17s", " Kunde", 
                          "                Name1", 
                          "         PLZ", 
                          "                 Ort",
                          "       Kurzname"); 
}

void SEARCHKUN::FillVlines (char *buffer) 
{
	    sprintf (buffer, " %11s  %38s  %22s  %38s  %18s", 
                         "1", "1", "1", "1", "1"); 
}


void SEARCHKUN::FillRec (char *buffer, int i)
{
 	      sprintf (buffer, " %8ld     |%-36s|    |%-20s|    |%36s|    |%16s|", 
                            atol (skuntab[i].kun), 
                            skuntab[i].adr_nam1,
                            skuntab[i].plz,
                            skuntab[i].ort1,
                            skuntab[i].kun_krz1);
}


void SEARCHKUN::UpdateList (void)
{
       int i;
 	   char buffer [512];

       for (i = 0; i < kunanz; i ++)
       {
          FillRec (buffer, i);
	      Query->UpdateRecord (buffer, i);
       }
}


int SEARCHKUN::SearchLst (char *sebuff)
/**
Nach Kurzname in Liste suchen.
**/
{
	   int i;
	   int len;
       char sabuff [80];
       char *pos;

	   if (skuntab == NULL) return 0;
	   if (strlen (sebuff) == 0) 
	   {
	       Query->SetSel (0);
		   return 0;
	   }

	   for (i = 0; i < kunanz; i ++)
	   {
		   len = min (16, strlen (sebuff));
           if (SearchField == 0)
           {
                   strcpy (sabuff, skuntab[i].kun);
                   for (pos = sabuff; *pos <= ' ' && *pos != 0 ; pos ++);
                   pos[len] = 0;
                   strcpy (sabuff, pos);
                   if (atol (sebuff) == atol (sabuff)) break;
           }
           else if (SearchField == 1)
           {
		           if (strupcmp (sebuff, skuntab[i].adr_nam1, len) == 0) break;
           }
           else if (SearchField == 2)
           {
		           if (strupcmp (sebuff, skuntab[i].plz, len) == 0) break;
           }
           else if (SearchField == 3)
           {
		           if (strupcmp (sebuff, skuntab[i].ort1, len) == 0) break;
           }
           else if (SearchField == 4)
           {
		           if (strupcmp (sebuff, skuntab[i].kun_krz1, len) == 0) break;
           }
	   }
	   if (i == kunanz) return 0;
	   Query->SetSel (i);
	   return 0;
}

int SEARCHKUN::ReadKun (char *squery)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int cursor;
	  int i;
      WMESS Wmess;


	  if (skuntab) 
	  {
           delete skuntab;
           skuntab = NULL;
	  }


      idx = -1;
	  kunanz = 0;
      SearchField = 1;
      Query->SetSBuff ("");
      clipped (squery);
      if (strcmp (squery, " ") == 0) squery[0] = 0;

      kunanz = 0;
      if (kunanz == 0) kunanz = 0x10000;

	  skuntab = new struct SKUN [kunanz];
      clipped (squery);
      if (strlen (squery) == 0 ||
          squery[0] <= ' ')
      {
	          sprintf (buffer, "select kun, adr.adr_nam1, adr.plz, adr.ort1, kun_krz1 "
	 			       "from kun, adr "
                       "where kun.mdn = %hd "
                       "and adr.adr = kun.adr1", mdn);
      }
      else
      {
	          sprintf (buffer, "select kun, adr.adr_nam1, adr.plz, adr.ort1, kun_krz1 "
	 			       "from kun, adr "
                       "where kun.mdn = %hd "
                       "and adr.adr = kun.adr1 "
                       "and upper (kun_krz1) matches upper (\"%s*\")", mdn, squery);         //WAL-180
      }
      if (query != NULL)
      {
             strcat (buffer, " "); 
             strcat (buffer, query); 
      }
      DbClass.sqlout ((char *) skun.kun, 0, 9);
      DbClass.sqlout ((char *) skun.adr_nam1, 0, 37);
      DbClass.sqlout ((char *) skun.plz, 0, 21);
      DbClass.sqlout ((char *) skun.ort1, 0, 37);
      DbClass.sqlout ((char *) skun.kun_krz1, 0, 17);
      cursor = DbClass.sqlcursor (buffer);
      if (cursor < 0) 
      {
          return -1;
      }
  	  i = 0;
      Wmess.Message (hMainInst, hMainWindow, " \nBitte warten.....\n"
  	                                         "Die Daten werden selektiert.\n ");  
	  while (DbClass.sqlfetch (cursor) == 0)
      {
		  memcpy (&skuntab[i], &skun, sizeof (struct SKUN));
          FillRec (buffer, i);
	      Query->InsertRecord (buffer);
		  i ++;
	  }
      kunanz = i;
	  DbClass.sqlclose (cursor);
//      soadr_nam1 = 1;
      sokun_krz1 = 1;
      if (kunanz > 1)
      {
//             SortAdr_nam1 (Query->GethWnd ());
             SortKunKrz1 (Query->GethWnd ());
      }
      UpdateList ();
 	  Wmess.Destroy ();
	  EnableWindow (awin, FALSE);
	  return 0;
}

void SEARCHKUN::FillBox (void)
/**
Query-Liste fuellen. 
**/
{
	  char buffer [512];
	  int i;

      for (i = 0; i < kunanz; i ++)
      {
          FillRec (buffer, i);
	      Query->InsertRecord (buffer);
      }
      if (idx > -1)
      {
	      Query->SetSel (idx);
      }
}


void SEARCHKUN::SetParams (HINSTANCE hMainInst, HWND hMainWindow)
{
      this->hMainInst   = hMainInst;
      this->hMainWindow = hMainWindow;
      awin = hMainWindow;
}

void SEARCHKUN::SetSearchTxt (char *stext)
{
       if (Query == NULL) return;
       
       HWND ShWnd = Query->GetSearchHwnd ();
       if (ShWnd == NULL) return;

       for (int i; stext[i] != 0; i ++)
       {
           PostMessage (NULL, WM_KEYDOWN, (WPARAM) stext[i], 0l);
       }
}


void SEARCHKUN::SearchKun (short mdn, char *qtext, char *stext)
{
  	  int cx, cy;
	  char buffer [256];
      RECT rect;

      if (mdn == 0)
      {
          disp_mess ("Mandant 0 ist nicht erlaubt", 2);
          syskey = KEY5;
          return;
      }
      this->mdn = mdn;
      Settchar ('|');
      GetClientRect (hMainWindow, &rect);
      cx = 80;
      cy = 25;
      SetSortProc (SortLst);
//      EnableWindow (hMainWindow, FALSE);
      if (Query == NULL)
      {
        idx = -1;
        if (qtext != NULL)
        {
            Query = new CHQEX (cx, cy);
        }
        else if (query != NULL)
        {
            Query = new CHQEX (cx, cy);
        }
        else
        {
            Query = new CHQEX (cx, cy, "Name", "");
        }
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (ReadKun);
	    Query->SetSearchLst (SearchLst);
        Query->SetSBuff (NULL);
        if (qtext != NULL)
        {
               ReadKun (qtext);
               if (stext != NULL && stext[0] != 0)
               {
                       Query->SetSBuff (stext);
               }
        }
        else if (query != NULL)
        {
               ReadKun ("");
               if (stext != NULL && stext[0] != 0)
               {
                       Query->SetSBuff (stext);
               }
        }
        else if (stext != NULL && stext[0] != 0)
        {
               ReadKun ("");
               Query->SetSBuff (stext);
        }
      }
      else
      {
        Query->RestorefWork ();
        if (SetStyle)
        {
            Query->SetWindowStyle (WindowStyle);
        }
        Query->OpenWindow (hMainInst, hMainWindow, TRUE);
        FillFormat (buffer);
        Query->RowAttr (buffer);
        FillVlines (buffer);
	    Query->VLines (buffer, 3);
	    EnableWindow (awin, FALSE);

        FillCaption (buffer);
	    Query->InsertCaption (buffer);

	    Query->SetFillDb (ReadKun);
	    Query->SetSearchLst (SearchLst);
        FillBox ();
        Query->SetSBuff (NULL);
        if (stext != NULL && stext[0] != 0)
        {
               Query->SetSBuff (stext);
        }
      }
	  Query->ProcessMessages ();
//      EnableWindow (hMainWindow, TRUE);
      idx = Query->GetSel ();
	  EnableWindow (awin, TRUE);
      Query->DestroyWindow ();
      SetActiveWindow (awin);
	  if (idx != -1)
      {
	      memcpy (&skun, &skuntab[idx], sizeof (skun));
      }
      SetSortProc (NULL);
      if (skuntab != NULL)
      {
          Query->SavefWork ();
      }
      else
      {
          delete Query;
          Query = NULL;
      }
}


SKUN *SEARCHKUN::GetNextSkun (void)
{
      if (idx == -1) return NULL;
      if (idx < kunanz - 1) 
      {
             idx ++;
      }
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}


SKUN *SEARCHKUN::GetPriorSkun (void)
{
      if (idx == -1) return NULL;
      if (idx > 0) 
      {
             idx --;
      }
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}

SKUN *SEARCHKUN::GetFirstSkun (void)
{
      if (idx == -1) return NULL;
      idx = 0;
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}

SKUN *SEARCHKUN::GetLastSkun (void)
{
      if (idx == -1) return NULL;
      idx = kunanz - 1;
      memcpy (&skun, &skuntab[idx], sizeof (skun));
      return &skun;
}



