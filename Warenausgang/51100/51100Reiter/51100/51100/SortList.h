// SortList.h: Schnittstelle f�r die Klasse CSortList.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SORTLIST_H__45E39B94_CEE9_4BEC_BF65_FB5338B64C8F__INCLUDED_)
#define AFX_SORTLIST_H__45E39B94_CEE9_4BEC_BF65_FB5338B64C8F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "DataCollection.h"

template <class T> 

class CCompareInterface
{
public:
	virtual int Compare (T t1, T t2)= 0;
};

template <class T> 

class CSortList : public CDataCollectionEx<T>  
{
private:
	CCompareInterface<T> *m_CompareInterface;
	BOOL m_Unique;
public:
	void set_CompareInterface (CCompareInterface<T> *compareInterface)
	{
		m_CompareInterface = compareInterface;
	}

	void set_Unique (BOOL unique)
	{
		m_Unique = unique;
	}

	CSortList() : CDataCollectionEx<T> ()
	{
		m_CompareInterface = NULL;
		m_Unique = FALSE;
	}

	void Add (T item)
	{
		T element;
   
		if (m_CompareInterface == NULL)
		{
			CDataCollectionEx<T>::Add (item);
		}
		else
		{
			for (int i = 0; i < anz ; i ++)
			{
				element = *Get (i);
				if (m_Unique && m_CompareInterface->Compare (item, element) == 0)
				{
					break;
				}
				if (m_CompareInterface->Compare (item, element) < 0)
				{
					CDataCollectionEx<T>::Insert (item, i);
					break;
				}
			}
			if (i == anz)
			{
				CDataCollectionEx<T>::Add (item);
			}
		}
	}
};

#endif // !defined(AFX_SORTLIST_H__45E39B94_CEE9_4BEC_BF65_FB5338B64C8F__INCLUDED_)
