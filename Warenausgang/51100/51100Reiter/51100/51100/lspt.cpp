#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "lspt.h"

struct LSPT lspt, lspt_null;

void LSPT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lspt.nr, 2, 0);
    out_quest ((char *) &lspt.nr,2,0);
    out_quest ((char *) &lspt.zei,2,0);
    out_quest ((char *) lspt.txt,0,61);
            cursor = prepare_sql ("select lspt.nr,  lspt.zei,  "
"lspt.txt from lspt "

#line 26 "lspt.rpp"
                                  "where nr = ? "
                                  "order by zei");

    ins_quest ((char *) &lspt.nr,2,0);
    ins_quest ((char *) &lspt.zei,2,0);
    ins_quest ((char *) lspt.txt,0,61);
            sqltext = "update lspt set lspt.nr = ?,  "
"lspt.zei = ?,  lspt.txt = ? "

#line 30 "lspt.rpp"
                                  "where nr = ? "
                                  "and zei  = ?";

            ins_quest ((char *) &lspt.nr, 2, 0);
            ins_quest ((char *) &lspt.zei, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lspt.nr, 2, 0);
            ins_quest ((char *) &lspt.zei, 2, 0);
            test_upd_cursor = prepare_sql ("select nr from lspt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &lspt.nr, 2, 0);
            ins_quest ((char *) &lspt.zei, 2, 0);
            del_cursor = prepare_sql ("delete from lspt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &lspt.nr, 2, 0);
            del_cursor_posi = prepare_sql ("delete from lspt "
                                  "where  nr = ?");

    ins_quest ((char *) &lspt.nr,2,0);
    ins_quest ((char *) &lspt.zei,2,0);
    ins_quest ((char *) lspt.txt,0,61);
            ins_cursor = prepare_sql ("insert into lspt (nr,  "
"zei,  txt) "

#line 54 "lspt.rpp"
                                      "values "
                                      "(?,?,?)"); 

#line 56 "lspt.rpp"
}
int LSPT_CLASS::dbreadfirst (void)
/**

Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int LSPT_CLASS::delete_lspposi (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (del_cursor_posi == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_posi);
         return (sqlstatus);
}

