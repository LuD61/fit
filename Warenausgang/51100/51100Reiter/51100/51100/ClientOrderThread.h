// ClientOrderThread.h: Schnittstelle f�r die Klasse CClientOrderThread.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIENTORDERTHREAD_H__F005E58E_0FBC_4EC5_87A9_4C6463BD5BC6__INCLUDED_)
#define AFX_CLIENTORDERTHREAD_H__F005E58E_0FBC_4EC5_87A9_4C6463BD5BC6__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Thread.h"
#include "ClientOrderCom.h"
#include "ThreadAction.h"

class CClientOrderThread : 
			public CThread, 
			public CClientOrderCom  
{
protected:
    CThreadAction *ThreadAction; 
public:
	void SetThreadAction (CThreadAction *ThreadAction)
	{
		this->ThreadAction = ThreadAction;
	}
	CClientOrderThread();
	virtual ~CClientOrderThread();
	virtual unsigned int Run();
	void Terminate ();

};

#endif // !defined(AFX_CLIENTORDERTHREAD_H__F005E58E_0FBC_4EC5_87A9_4C6463BD5BC6__INCLUDED_)
