#ifndef _MO_PROV_DEF
#define _MO_PROV_DEV
#include "dbclass.h"
struct PROV_SATZ {
   short     mdn;
   short     fil;
   long      vertr;
   long      kun;
   double    a;
   short     hwg;
   short     wg;
   long      ag;
   double    prov_satz;
   double    prov_sa_satz;
   char      a_bz1[25];
   char      kun_bran2[3];
};
extern struct PROV_SATZ prov_satz, prov_satz_null;

#line 5 "prov_satz.rh"

class PROV : public DB_CLASS
{
        private :
         long kun;
         char kun_bran2[3];
         double a;
         long  ag;
         short wg;
         short hwg;

         GetProvSatzKunA (short, 
                          short, 
                          long, 
                          long,
                          double);
         GetProvSatzKunAg (short, 
                           short, 
                           long, 
                           long,
                           long);
         GetProvSatzKunWg (short, 
                           short, 
                           long, 
                           long,
                           short);
         GetProvSatzKunHwg (short, 
                           short, 
                           long, 
                           long,
                           short);
         GetProvSatzKunAll (short, 
                            short, 
                            long, 
                            long);

         GetProvSatzBranA (short, 
                           short, 
                           long, 
                           char *,
                           double);
         GetProvSatzBranAg (short, 
                            short, 
                            long, 
                            char *,
                            long);
         GetProvSatzBranWg (short, 
                            short, 
                            long, 
                            char *,
                            short);
         GetProvSatzBranHwg (short, 
                            short, 
                            long, 
                            char *,
                            short);
         GetProvSatzBranAll (short, 
                             short, 
                             long, 
                             char *);
                             

         GetProvSatzAllA (short, 
                           short, 
                           long, 
                           double);
         GetProvSatzAllAg (short, 
                            short, 
                            long, 
                            long);
         GetProvSatzAllWg (short, 
                            short, 
                            long, 
                            short);
         GetProvSatzAllHwg (short, 
                            short, 
                            long, 
                            short);

         GetProvSatzAllAll (short, 
                            short, 
                            long); 
      public :
         PROV () : DB_CLASS ()
         {
         }
         GetProvSatz (short, 
                      short, 
                      long, 
                      long,
                      char *,
                      double,
                      long,
                      short,
                      short);

};   

#endif
