#include "ControlsPlugin.h"
#include "Text.h"

#define CString Text

HANDLE CControlsPlugin::ControlsPlugin = NULL;

CControlsPlugin::CControlsPlugin ()
{
    pGetPosTxtKz =NULL;
	char *bws;

	bws = getenv ("BWS");

	CString Controls;
	if (bws != NULL)
	{
		Controls.Format ("%s\\bin\\Controls.dll", bws);
	}
	else
	{
		Controls = "Controls.dll";
	}
	ControlsPlugin = LoadLibrary (Controls.GetBuffer ());
	if (ControlsPlugin != NULL)
	{
		pGetPosTxtKz = (int (*) (int)) 
					GetProcAddress ((HMODULE) ControlsPlugin, "GetPosTxtKz");
	}

}

int CControlsPlugin::GetPosTxtKz (int PosTxtKz)
{
	if (pGetPosTxtKz == NULL)
	{
		return PosTxtKz;
	}
	return (*pGetPosTxtKz) (PosTxtKz);
}

BOOL CControlsPlugin::IsActive ()
{
	if (pGetPosTxtKz == NULL)
	{
		return FALSE;
	}
	return TRUE;
}
