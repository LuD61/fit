// Ampel.cpp: Implementierung der Klasse CAmpel.
//
//////////////////////////////////////////////////////////////////////
#include <windows.h>
#include "wmaskc.h"
#include "resrc1.h"
#include "Ampel.h"
#include "sounds.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

PAINTSTRUCT CAmpel::aktpaint;

CAmpel *CAmpel::instance = NULL;

CAmpel::CAmpel()
{
	iRed = iGreen = NULL;
	this->hWnd = NULL;
	this->hInstance = NULL;
	p.x = p.y = 0;
	Play = FALSE;
	OwnWindow = FALSE;
	aWnd = NULL;
	registered = FALSE;
	state = No;
	hbrBackground = NULL;
	instance = this;
}

CAmpel::CAmpel(HINSTANCE hInstance, HWND hWnd, POINT p, BOOL OwnWindow)
{
	iRed = iGreen = NULL;
	this->hWnd = hWnd;
	this->hInstance = hInstance;
	this->p = p;
	Play = FALSE;
	this->OwnWindow = FALSE;
	aWnd = NULL;
	registered = FALSE;
	state = No;
	Load ();
	hbrBackground = NULL;
	instance = this;
}

void CAmpel::SetOwnWindow (BOOL OwnWindow)
{
	RECT rect;
	int x, y;
	this->OwnWindow = OwnWindow;
	if (!OwnWindow )
	{
		if (aWnd != NULL)
		{
			DestroyWindow (aWnd);
			aWnd = NULL;
		}
	}
	else if (hWnd != NULL && aWnd == NULL)
	{
		if (!registered)
		{
			Register ();
		}
		GetWindowRect (hWnd, &rect);
//		x = rect.left + p.x;
//		y = rect.top  + p.y;
		x = p.x;
		y = p.y;
        aWnd = CreateWindow ("Ampel",
                                "",
//                             WS_POPUP | WS_BORDER,
                             WS_POPUP,
                             x, y,
                             36, 36,
                             hWnd,
                             NULL,
                             hInstance,
                             NULL);
	
	}
}

CAmpel::~CAmpel()
{

}

void CAmpel::Load ()
{
	iRed   = LoadIcon (hInstance, MAKEINTRESOURCE (IDI_AMPEL_RED));
	iGreen = LoadIcon (hInstance, MAKEINTRESOURCE (IDI_AMPEL_GREEN));
}

void CAmpel::Invalidate ()
{
	if (aWnd != NULL)
	{
		InvalidateRect (aWnd, NULL, TRUE);
	}
	else
	{
		RECT Rect;
		Rect.left   = p.x - 20;
		Rect.top    = p.y - 20;
		Rect.right  = p.x + 36;
		Rect.bottom = p.y + 36;
		InvalidateRect (hWnd, &Rect, TRUE);
	}
}

void CAmpel::ShowGreen (HDC hDC)
{
	if (Green != NULL)
	{
		if (aWnd != NULL)
		{
			if (!IsWindowVisible (aWnd))
			{
				ShowWindow (aWnd, SW_SHOWNORMAL);
			}
			DrawIcon (hDC, 1, 3, iGreen);
		}
		else
		{
			DrawIcon (hDC, p.x, p.y, iGreen);
		}
	}
	if (Play)
	{
	    CSounds sound ("StndLoad.wav");
        sound.Play ();
	}
}

void CAmpel::ShowRed (HDC hDC)
{
	if (Red != NULL)
	{
		if (aWnd != NULL)
		{
			if (!IsWindowVisible (aWnd))
			{
				ShowWindow (aWnd, SW_SHOWNORMAL);
			}
			DrawIcon (hDC, 1, 3, iRed);
		}
		else
		{
			DrawIcon (hDC, p.x, p.y, iRed);
		}
	}
}

void CAmpel::SetUnvisible ()
{
	if (aWnd != NULL)
	{
		ShowWindow (aWnd, SW_HIDE);
	}
}

void CAmpel::SetAbsPos (HWND hWnd, int x, int y)
{
	HFONT hFont, oldfont;
	TEXTMETRIC tm;
	HDC hdc;
	SIZE size;

	hFont = EzCreateFont (hdc, "Courier New",
                            100,
                            0,
                            0,
                            TRUE);
	hdc = GetDC (hWnd);
	oldfont = SelectObject (hdc,hFont);
	GetTextMetrics (hdc, &tm);
    GetTextExtentPoint32 (hdc, "X", 1, &size);
	SelectObject (hdc, oldfont);
	DeleteObject (hFont);
	ReleaseDC (hWnd, hdc);
    p.x = x * size.cx + 5;
    p.y = y * size.cy;
	p.y += (int) ((double) tm.tmHeight * 1.5) + 5;	
}

void CAmpel::Register ()
{
        WNDCLASS wc;

        wc.style         =  CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "FITICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
//        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
//        wc.hbrBackground =  CreateSolidBrush (RGB (255, 255, 204));
        wc.hbrBackground =  hbrBackground;
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "Ampel";

        RegisterClass(&wc);
}

void CAmpel::PrintAmpel (HDC hDC)
{
	if (state == Red)
	{
		ShowRed (hDC);
	}
	else if (state == Green)
	{
		ShowGreen (hDC);
	    SetPlay (FALSE);
	}
	else
	{
		SetUnvisible ();
	}
}

LONG FAR PASCAL CAmpel::WndProc(HWND hWnd,UINT msg,
								WPARAM wParam,LPARAM lParam)
{
	HDC hdc;
    switch(msg)
    {
          case WM_PAINT :
                  hdc = BeginPaint (hWnd, &aktpaint);
				  instance->PrintAmpel (hdc);
                  EndPaint (hWnd, &aktpaint);
				  break;
	}
    return DefWindowProc(hWnd, msg, wParam, lParam);
}
