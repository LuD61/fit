#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#ifndef NINFO
#include "item.h"
#endif
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#ifndef NINFO
#include "inflib.h"
#endif

#define MAXLEN 40
#define MAXBEZ 500
#define MAXFIND 30

#define _NEWLST

#define LPLUS 1

extern HFONT EzCreateFontT (HDC, char *, int,
                            int, int, BOOL);
extern HBRUSH CreateSolidBrushT (COLORREF);
extern HBRUSH CreatePenT (int, int, COLORREF);
extern BOOL DeleteObjectT (HGDIOBJ); 

class ITEM_CLASS item_class;


#define EzCreateFont EzCreateFontT
#define CreateSolidBrush CreateSolidBrushT
#define CreatePen CreatePenT
#define DeleteObject DeleteObjectT


BOOL (*IsComboMessage) (MSG *) = NULL;

void SetComboMess (BOOL (*CbProg) (MSG *))
{
       IsComboMessage = CbProg;
}


mfont sfont = {
	           "Courier New", 
//	           "MS SAN SERIF", 
                90, 100, 0,
                BLACKCOL,
                LTGRAYCOL,
                0,
                NULL};



ITEM iUbSatzNr ("", "S-Nr", "", 0);

static field _FUbSatzNr[] = {
&iUbSatzNr,          6, 1, 0, 0, 0, "", BUTTON, 0, 0, 0};

static form FUbSatzNr = {1, 0, 0, _FUbSatzNr, 0, 0, 0, 0, NULL};

static char SatzNr [10];
static ITEM iSatzNr ("", SatzNr, "", 0);

static field _fSatzNr[] = {
&iSatzNr,          6, 1, 0, 0, 0, "", EDIT, 0, 0, 0};

static form fSatzNr = {1, 0, 0, _fSatzNr, 0, 0, 0, 0, NULL};


static field iButton = {NULL,  0, 1, 0, 0, 0, "", BUTTON, 0, 0, 0}; 
static field iEdit =   {NULL,  0, 0, 0, 0, 0, "", EDIT, 0, 0, 0}; 

static form IForm = {0, 0, 0, NULL, 0, 0, 0, 0, NULL};

static char *feldbztab [MAXBEZ];
static int fbanz = 0;

static BOOL NoMove = FALSE;
static BOOL FieldDestroy = FALSE;

/* Daten fuer Switch  */

static char *DlgSaetze [1000];
static int DlgAnz = 0;

static FELDER DlgZiel [] = {
"Bezeichnung",     20, "C", 0, 1,
"Wert",        MAXLEN, "C", 0, 1,
};

static form RowUb;
static form RowData;
static form RowLine;

struct DLGS
{
        char bez  [80];
        char wert [80];
};

struct DLGS dlgs, dlgstab [MAXBEZ]; 

static ITEM iDlgBezU  ("", "Bezeichnung", "", 0);
static ITEM iDlgWertU ("", "Wert",        "", 0);

static field _UbDlg[] = {
&iDlgBezU,         20, 1, 0, 6, 0, "", BUTTON, 0, 0, 0,
&iDlgWertU,    MAXLEN, 1, 0,26, 0, "", BUTTON, 0, 0, 0,
};

static form UbDlg = {2, 0, 0, _UbDlg, 0 ,0, 0, 0, NULL};  

static ITEM iDlgBez  ("", dlgs.bez,   "", 0);
static ITEM iDlgWert ("", dlgs.wert,  "", 0);

static field _DataDlg[] = {
&iDlgBez,          20, 1, 0, 6, 0, "", DISPLAYONLY, 0, 0, 0,
&iDlgWert,     MAXLEN, 1, 0,26, 0, "", EDIT, 0, 0, 0,
};

static form DataDlg = {2, 0, 0, _DataDlg, 0 ,0, 0, 0, NULL};  

static ITEM iline ("", "1", "", 0);

static field _LineDlg[] = {
&iline,             1, 1, 0,26, 0, "", DISPLAYONLY, 0, 0, 0,
&iline,             1, 1, 0,66, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form LineDlg = {2, 0, 0, _LineDlg, 0 ,0, 0, 0, NULL};  

static int Dlgzlen = 20 + MAXLEN;
static unsigned char DlgSatzB [20 + MAXLEN + 2];
static unsigned char *DlgSatz = DlgSatzB;

/* Daten fuer Foneingabe           */

HWND choise = NULL;

static char zh[] = {"Zeichenh�he  "};
static char zw[] = {"Zeichenbreite"};

static int FAttr;
static ITEM vHeightT ("" , zh, "",  0);
static ITEM vWidthT  ("" , zw, "",  0);
static ITEM vHeight ("" , "      ", "",   0);
static ITEM vWidth  ("" , "      ", "", 0);
static ITEM vAttrC  ("",  "Fett",  "", 0);
static ITEM vOK     ("",  "OK",  "", 0);
static ITEM vCancel ("",  "Abbrechen", "", 0);


static int testcr (void);

static field _ffont [] = {
&vHeightT, 13, 0, 1, 1, 0, "", DISPLAYONLY, 0, 0, 0, 
&vWidthT,  13, 0, 2, 1, 0, "", DISPLAYONLY, 0, 0, 0, 
&vHeight,   6, 0, 1,15, 0, "%3d", EDIT | UPDOWN, 0, testcr, 0, 
&vWidth,    6, 0, 2,15, 0, "%3d", EDIT | UPDOWN, 0, testcr, 0, 
&vAttrC,    7, 0, 3,15, 0, "", BUTTON | CHECKBUTTON, 0, testcr, KEY6,
&vOK,       4, 0, 5, 4, 0, "", BUTTON | DEFBUTTON, 0, testcr, KEY12,
&vCancel,  11, 0, 5, 9, 0, "", BUTTON, 0, testcr, KEY5, 
};

static form ffont = {7, 0, 0, _ffont, 0, 0, 0, 0, &sfont}; 
// static form ffont = {7, 0, 0, _ffont, 0, 0, 0, 0, NULL}; 

static struct UDTAB udtab[] = {500, 50, 90,
                               200,  0,100};
static int udanz = 2;

static char SearchBuff [256];

static ITEM vSearchBez ("","Suchen",         "", 0);   
static ITEM vSearch    ("", SearchBuff,       "", 0);

static field _ffind [] = {
&vSearchBez,   6, 0,  1, 2,  0, "", DISPLAYONLY, 0, 0, 0,
&vSearch,     20,10,  1, 9,  0, "255", COMBOBOX | SCROLL, 0, 0, 0,     
&vOK,          4, 0,  3, 8,  0, "", BUTTON, 0, 0, KEY12,
&vCancel,     11, 0,  3,13,  0, "", BUTTON, 0, 0, KEY5, 
};

static char findauswahl [MAXFIND][256];
static int findpos = 0;

static combofield  cbfield[] = {1, 256, 0, (char *) findauswahl,
                                 0,  0, 0, NULL};


static form ffind = {4, 0, 0, _ffind, 0, 0, 0, cbfield, NULL}; 

int eBreak (void)
{
       syskey = KEY5;
       break_enter ();
       return 0;
}

int eOk (void)
{
       syskey = KEY12;

       GetWindowText (ffont.mask [2].feldid,
                      ffont.mask [2].item->GetFeldPtr (),
                      ffont.mask [2].length);
       GetWindowText (ffont.mask [3].feldid,
                      ffont.mask [3].item->GetFeldPtr (),
                      ffont.mask [3].length);
       break_enter ();
       return 0;
}

int eAttr (void)
{
       syskey = KEY6;
       if (FAttr == 0)
       {
           FAttr = 1;
       }
       else
       {
           FAttr = 0;
       }
       return 0;
}

static HWND GethMainWindow (HWND hWnd)
/**
Hauptfenster der Anwendung holen.
**/
{
         HWND aktMain;

         while (TRUE)
         {
                 aktMain = GetParent (hWnd);
                 if (aktMain == NULL) break;
                 hWnd = aktMain;
         }
         return hWnd;
}

static void DisableWindows (HWND hWnd)
/**
Hauptfenster der Anwendung holen.
**/
{

         while (TRUE)
         {
                 EnableWindow (hWnd, FALSE);     
                 hWnd = GetParent (hWnd);
                 if (hWnd == NULL) break;
         }
}

static void EnableWindows (HWND hWnd)
/**
Hauptfenster der Anwendung holen.
**/
{

         while (TRUE)
         {
                 EnableWindow (hWnd, TRUE);     
                 hWnd = GetParent (hWnd);
                 if (hWnd == NULL) break;
         }
}


// BOOL ListClass::NoRecNr = FALSE;


void ListClass::SetNoRecNr (BOOL flag)
/**
FLag NoReNr setzen.
**/
{
       NoRecNr = flag;
}

void ListClass::ChoiseLines (HDC hdc)
{
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         RECT rect;
         int x, y;
         int cx, cy;

         GetClientRect (choise, &rect);
         x = tm.tmAveCharWidth / 2;
         y = tm.tmHeight / 2;

         cx = rect.right - tm.tmAveCharWidth / 2;
         cy = rect.bottom - tm.tmHeight / 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         SelectObject (hdc, hPenG);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

         SelectObject (hdc, hPenW);


         y ++;
         x ++;
         cx --;
         cy --;
         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         cx += 2;
         cy += 2;
         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

}

int testcr (void)
{
      HWND hWnd;
      
      if (syskey == KEY6)
      {
           eAttr ();
           return 1;
      }
      if (syskey == KEYCR)
      {
           hWnd = GetFocus ();
           if (hWnd == ffont.mask[4].feldid)
           {
               syskey = KEY6; 
               eAttr ();
               return 1;
           }
           else if (hWnd == ffont.mask[6].feldid)
           {
               syskey = KEY5;
           }
           break_enter ();
       }
       else if (syskey == KEY12)
       {
           break_enter ();
       }
       else if (syskey == KEY5)
       {
           break_enter ();
       }
       else if (syskey == KEYESC)
       {
           break_enter ();
       }
       return 0;
}

void ListClass::ChoiseFont (mfont *lFont)
/**
Font eingeben.
**/
{
        HDC hdc; 

        spezfont (&sfont);

        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);
        set_fkt (eAttr, 6);

        udtab[0].Pos = lFont->FontHeight;
        udtab[1].Pos = lFont->FontWidth;
        sprintf (ffont.mask[2].item->GetFeldPtr (), "%3d", 
                 lFont->FontHeight);       

        sprintf (ffont.mask[3].item->GetFeldPtr (), "%3d", 
                 lFont->FontWidth);
        strcpy (ffont.mask[0].item->GetFeldPtr (), zh);
        strcpy (ffont.mask[1].item->GetFeldPtr (), zw);

        if (lFont->FontAttribute)
        {
                 ffont.mask[4].picture = "1";
        }
        else
        {
                 ffont.mask[4].picture = "";
        }

        no_break_end ();
        SetButtonTab (TRUE);
        DisablehWnd (hMainWindow);

        choise  = CreateWindow (
                                 "hStdWindow",
                                 "Font-Daten",
                                 WS_POPUP | 
                                 WS_VISIBLE | 
                                 WS_CAPTION | 
                                 WS_DLGFRAME, 
                                 32, 40,
                                 200, 180,
                                 GetActiveWindow (),
                                 NULL,
                                 hMainInst,
                                 NULL);

        ShowWindow (choise, SW_SHOW);
        UpdateWindow (choise);

        hdc = GetDC (choise);
        ChoiseLines (hdc);
        ReleaseDC (choise, hdc);
        SetUpDownTab (udtab , udanz);
        DisplayAfterEnter (FALSE);
        enter_form (choise, &ffont, 0, 0);
		SetAktivWindow (mamain3);
        DestroyWindow (choise);
        DisplayAfterEnter (TRUE);
        choise = NULL;
        SetButtonTab (FALSE);
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);

        if (syskey == KEY5) return;
        if (syskey == KEYESC) return;

        lFont->FontHeight = atoi (ffont.mask[2].item->GetFeldPtr ());       
        lFont->FontWidth  = atoi (ffont.mask[3].item->GetFeldPtr ());       
        lFont->FontAttribute  = FAttr;       
        SetFont (lFont);
        UbHeight = UbForm.mask[0].rows * tm.tmHeight;
}

void ListClass::SetColors (COLORREF Color, COLORREF BkColor)
{
        this->Color   = Color;
        this->BkColor = BkColor;
		delete ColBkColor;
		ColBkColor = new CColBkColor (0, BkColor);
        if (mamain2 == NULL) return;
        SetClassLong (mamain2,GCL_HBRBACKGROUND, 
                                      (long) CreateSolidBrush (BkColor));
        InvalidateRect (mamain3, NULL, TRUE);
}

void ListClass::SetActBkColor (COLORREF ActBkColor)
{
        this->ActBkColor = ActBkColor;
		delete ActColBkColor;
		ActColBkColor = new CColBkColor (0, ActBkColor);
}

void ListClass::FindNext (void)
/**
Naechste Uebereinstimmung suchen.
**/
{
        char leer [256];

        memset (leer, ' ', 256);
        leer[255] = (char) 0;
        if (strcmp (SearchBuff,leer) <= 0)
        {
            FindString ();
        }
        else
        {
            this->find (SearchBuff);
        }
}

BOOL ListClass::NewSearchString ()
{
        int i;

        for (i = 0; i < findpos; i ++)
        {
            if (strcmp (SearchBuff, findauswahl[i]) == 0)
            {
                return FALSE;
            }
        }
        return TRUE;
}

void ListClass::FindString (void)
/**
String suchen.
**/
{
        HWND hFind;

        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        break_end ();
        DisablehWnd (hMainWindow);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
        hFind = OpenWindowChC (6, 35, 10, 20, hMainInst,
                "Suchen");
        enter_form (hFind, &ffind, 0, 0);
        DestroyFrmWindow ();
        hFind = NULL;
        no_break_end ();
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);
        if (syskey == KEY5) return;

        if (NewSearchString ())
        {
                 strcpy (findauswahl[findpos], SearchBuff);
                 if (findpos < MAXFIND -1) findpos ++;
                 cbfield[0].cbanz = findpos; 
                 fillcombobox (&ffind, 0);
        }
        this->find (SearchBuff);
}

BOOL ListClass::InRec (char *value, int *pos)
{
         int i;

         for (i = *pos; i < DataForm.fieldanz; i ++)
         {
//             if (strstr (DataForm.mask[i].item->GetFeldPtr (),
             if (upstrstr (DataForm.mask[i].item->GetFeldPtr (),
                 value))
             {
                 *pos = i;
                 return TRUE;
             }
         }
         return FALSE;
}


void ListClass::find (char *value)
/**
Begriff suchen.
**/
{
         int i;
         int row;
		 RECT rect;

         if (RowMode) return;

		 GetClientRect (mamain3, &rect);
		 rect.top += UbHeight;
         clipped (value);
         row = AktColumn + 1; 
         for (i = AktRow; i < recanz; i ++)
         {
             memcpy (ausgabesatz, SwSaetze[i], zlen);
             if (InRec (value, &row)) 
             {
                 break;
             }
             row = 0;
         }
         if (i == recanz) 
         {
             disp_mess ("Keine �bereinstimmung gefunden", 2);
             return;
         }

         AktRow = i;
         AktColumn = row;
/*
         if (DataForm.mask[row].attribut != EDIT)
         {
                   row = NextColumn ();
                   AktColumn = row;
         }
*/
		 row = FirstColumn ();
		 AktColumn = row;
         FindRow = i;
         FindColumn = row;
//         SetPos (AktColumn);
         SetVPos (AktRow);
 		 SetFeldFocus0 (AktRow, AktColumn);
         InvalidateRect (mamain3, &rect, TRUE);
         UpdateWindow (mamain3);
}

void ListClass::DestroyListWindow (void)
{
         
         DestroyFocusWindow ();
         if (mamain3)
         {
             DestroyWindow (mamain3);
         }

         if (vTrack)
         {
             DestroyWindow (vTrack);
         }

         if (hTrack)
         {
             DestroyWindow (hTrack);
         }
         if (mamain2)
         {
             DestroyWindow (mamain2);
         }

         mamain3 = NULL;
         vTrack  = NULL;
         hTrack  = NULL;
         mamain2 = NULL;
}



HWND ListClass::InitListWindow (HWND hMainWindow)
/**
Listfenster oeffnen.
**/
{
       int x,y;
       int cx,cy;
       RECT rect;


       memcpy (_fUbSatzNr, _FUbSatzNr, sizeof (field));
       memcpy (&fUbSatzNr, &FUbSatzNr, sizeof (form));
       fUbSatzNr.mask[0].feldid = NULL;
       InAppend = NewRec = 0;
       if (mamain2)
       {
           DestroyWindow (mamain2);
       }

       this->hMainWindow = hMainWindow;
       this->hMainInst = (HANDLE) GetWindowLong (hMainWindow, 
                                               GWL_HINSTANCE);
       y = LineRow + 5;
       x = 1;
       GetClientRect (hMainWindow, &rect);
       cy = rect.bottom - y - 5;
       cx = rect.right - 2;

       mamain2 = CreateWindowEx (
                                WS_EX_CLIENTEDGE, 
                                "hListWindow",
                                "",       
                                WS_CHILD | WS_VISIBLE,
                                x, y,
                                cx, cy,
                                hMainWindow,
                                NULL,
                                hMainInst,
                                NULL);

        SetClassLong (mamain2,GCL_HBRBACKGROUND, 
                         (long) CreateSolidBrush (BkColor));
        if (mamain3)
        {
           DestroyWindow (mamain3);
        }
        mamain3 = CreateWindowEx (
                                WS_EX_CLIENTEDGE, 
                                "hListWindow",
                                "",       
                                WS_CHILD | WS_VISIBLE,
                                0, 0,
                                0, 0,
                                mamain2,
                                NULL,
                                hMainInst,
                                NULL);
        return mamain2;
}

void ListClass::CopyFonts (mfont *lFont)
/**
Neuen Font fuer Liste setzen.
**/
{
        HFONT hFont, oldFont;
        HDC hdc;
        SIZE size;

        spezfont (lFont);
        memcpy (&UbFont,  lFont, sizeof (mfont));   
        memcpy (&UbSFont, lFont, sizeof (mfont));   
        memcpy (&SFont,   lFont, sizeof (mfont));   
        memcpy (&ListFont,lFont, sizeof (mfont));
        EditFont = NULL;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
		ListhFont = hFont;
		if (oldFont == NULL)
		{
		      oldhFont = oldFont;
		}
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (0, hdc);

        SetTextMetric (&tm);
}



void ListClass::SetFont (mfont *lFont)
/**
Neuen Font fuer Liste setzen.
**/
{
        HFONT hFont, oldFont;
        HDC hdc;
        SIZE size;

        spezfont (lFont);
        CloseControls (&fUbSatzNr);
        CloseControls (&UbForm);
        memcpy (&UbFont,  lFont, sizeof (mfont));   
        memcpy (&UbSFont, lFont, sizeof (mfont));   
        memcpy (&SFont,   lFont, sizeof (mfont));   
        memcpy (&ListFont,lFont, sizeof (mfont));
        EditFont = NULL;
        DestroyFocusWindow ();
        CloseControls (&fUbSatzNr);
        CloseControls (&UbForm);

        hdc = GetDC (NULL);
		if (ListhFont != NULL)
		{
			SelectObject (hdc, oldhFont);
			DeleteObject (ListhFont);
		}
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (0, hdc);

		ListhFont = hFont;
		if (oldFont == NULL)
		{
		      oldhFont = oldFont;
		}
        SetTextMetric (&tm);
        if (NoRecNr == FALSE)
        {
                  display_form (mamain3, &fUbSatzNr, 0, 0);
        }

        if (UbForm.mask[0].rows == 0)
        {
            fUbSatzNr.mask[0].rows = 0;
            UbHeight = tm.tmHeight + tm.tmHeight / 3; 
        }
        else 
        {
            fUbSatzNr.mask[0].rows = UbForm.mask[0].rows;
            UbHeight = tm.tmHeight * UbForm.mask[0].rows; 
        }
        display_form (mamain3, &UbForm, 0, 0);
        SendMessage (mamain2, WM_SIZE, NULL, NULL);
        stdfont ();
        InvalidateRect (mamain2, 0, TRUE);
        InvalidateRect (mamain3, 0, TRUE);
}


void ListClass::TestMamain3 (void)
{
       RECT rect;
//       HDC hdc;
       
       GetClientRect (mamain2, &rect);

       if (TrackNeeded ())
       {
           rect.bottom -= 16;
       }
       if (VTrackNeeded ())
       {
           rect.right -= 16;
       }

       MoveWindow (mamain3, 0, 0, rect.right, rect.bottom, TRUE);


       int current = currentfield;
       CloseControls (&UbForm);
       if (UbForm.mask)
       {
               display_form (mamain3, &UbForm, 0, 0);
       }
       currentfield = current;
}

void ListClass::TestTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
	   SCROLLINFO scinfo;

       if (TrackNeeded ())
       {
           CreateTrack ();
       }
       else
       {
           DestroyTrack ();
       }
       if (hTrack == NULL) return;

       GetClientRect (mamain2, &rect);
       x = 0;
       y = rect.bottom - 16;
       cy = 16;
       cx = rect.right;
       MoveWindow (hTrack, x, y, cx, cy, TRUE);
	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE;
       scinfo.nPage  = 1;
       SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
}

void ListClass::TestVTrack (void)
{
       RECT rect;
       int x,y,cx,cy;
	   SCROLLINFO scinfo;

       if (VTrackNeeded ())
       {
           CreateVTrack ();
       }
       else
       {
           DestroyVTrack ();
       }
       if (vTrack == NULL) return;

       GetClientRect (mamain2, &rect);

       x = rect.right - 16;
       cx = 16;
       y = 0;
       cy = rect.bottom;
       if (hTrack)
       {
                 cy -= 16;
       }
       MoveWindow (vTrack, x, y, cx, cy, TRUE);
	   scinfo.cbSize = sizeof (SCROLLINFO);
	   scinfo.fMask  = SIF_PAGE;
       scinfo.nPage  = 1;
       SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
       SetScrollPos (vTrack, SB_CTL, scrollpos, TRUE);
}


void ListClass::MoveListWindow (void)
/**
Listfenster oeffnen.
**/
{
       int x,y;
       int cx,cy;
       RECT rect;

       if (mamain2 == NULL) return;

       GetPageLen ();
       y = LineRow + 5;
       x = 1;
       GetClientRect (hMainWindow, &rect);
       cy = rect.bottom - y - 5;
       cx = rect.right - 2;

       MoveWindow (mamain2, x, y, cx, cy, TRUE);


       if (hwndTB != NULL) 
       {
              GetClientRect (hwndTB, &rect);
              MoveWindow (hwndTB, 0, 0, cx + 2, rect.bottom, TRUE);
       }

       TestMamain3 ();
       TestTrack ();
       TestVTrack ();
}


BOOL ListClass::TrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT frmrect;
        RECT winrect;

		if (banz == 0) return FALSE;
        GetFrmRect (&UbForm, &frmrect);
        GetClientRect (mamain2, &winrect);
        if (frmrect.right > winrect.right)
        {
                    return TRUE;
        }
        return TRUE;
}

void ListClass::CreateTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
        RECT rect;
		SCROLLINFO scinfo;

        if (hTrack) return;

        GetClientRect (mamain2, &rect);

        x = 0;
        y = rect.bottom - 16;
        cy = 16;
        cx = rect.right;

        hTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_HORZ,
                               x, y, cx, cy,
                               mamain2,
                               (HMENU) 701,
                               hMainInst,
                               0);


		 scinfo.cbSize = sizeof (SCROLLINFO);
		 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		 scinfo.nMin   = 0;
		 scinfo.nMax   = banz - 1;
		 scinfo.nPage  = 0;
         SetScrollInfo  (hTrack, SB_CTL, &scinfo, TRUE);
}

void ListClass::DestroyTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (hTrack)
          {
              DestroyWindow (hTrack);
              hTrack = NULL;
          }
}

BOOL ListClass::VTrackNeeded (void)
/**
Test, ob ein Schieberegler benoetigt wird.
**/
{
        RECT winrect;
        int cy;

        GetClientRect (mamain2, &winrect);

// recanz + 1 wegen Ueberschrift 

//      cy = (recanz + 1) * tm.tmHeight * recHeight;

        cy = (int) (double)((double) UbHeight + recanz * 
			         tm.tmHeight * recHeight * RowHeight);
        if (hTrack) 
        {

// untere Bildlaufleiste abziehen

            cy += 16; 
        }
        if (cy > winrect.bottom)
        {
                    return TRUE;
        }
        return FALSE;
}

void ListClass::CreateVTrack (void)
/**
Bildlaufleiste generieren.
**/
{
        int x,y;
        int cx, cy;
        RECT rect;
		SCROLLINFO scinfo;

        if (vTrack) return;
        GetClientRect (mamain2, &rect);

        x = rect.right - 16;
        cx = 16;
        y = 0;
        cy = rect.bottom;
        if (hTrack)
        {
                 cy -= 16;
        }

        vTrack = CreateWindow ("scrollbar",
                               NULL,
                               WS_CHILD | WS_VISIBLE | SBS_VERT,
                               x, y, cx, cy,
                               mamain2,
                               (HMENU) 702,
                               hMainInst,
                               0);
		 scinfo.cbSize = sizeof (SCROLLINFO);
		 scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		 scinfo.nMin   = 0;
		 scinfo.nMax   = recanz - 1;
		 if (lPagelen)
		 {
		     scinfo.nPage  = 1;
		 }
		 else
		 {
			 scinfo.nPage = 0;
		 }
         SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
}

void ListClass::DestroyVTrack (void)
/**
Bildlaufleiste loeschen.
**/
{
          if (vTrack)
          {
              DestroyWindow (vTrack);
              vTrack = NULL;
          }
}

BOOL ListClass::IsInListArea (MSG *msg)
/**
MousePosition Pruefen unf Focus setzen.
**/
{
	    POINT mousepos;
		RECT rect;
		int x1, y1;
		int x2, y2;
		int i, j;
		int diff;
        int breakstat, h;

//        if (msg->hwnd != mamain3) return FALSE;

        diff = 0;
        if (LineForm.frmstart)
		{
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
			if (NoRecNr == FALSE)
			{
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                           fSatzNr.mask[0].length;
			}
			else
			{
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
		}
        GetCursorPos (&mousepos);
		GetWindowRect (mamain3, &rect);
//		rect.top += tm.tmHeight;
		rect.top += UbHeight;

		if (mousepos.x < rect.left ||
			mousepos.x > rect.right)
		{
			        return FALSE;
		}
		if (mousepos.y < rect.top ||
			mousepos.y > rect.bottom)
		{
			        return FALSE;
		}

        ScreenToClient (mamain3, &mousepos);

        breakstat = 0;
        for (h = 0; h < recHeight; h ++)
        {
		  for (i = 0; i < recanz; i ++)
          {
			    if (i > lPagelen) break;
//			    y1 = UbHeight + i * tm.tmHeight * recHeight + h * tm.tmHeight;
			    y1 = (int) (double) ((double) UbHeight + 
					        i * tm.tmHeight * recHeight * 
							RowHeight + h * tm.tmHeight);
				y2 = y1 + tm.tmHeight;
				if (mousepos.y < y1 ||
					mousepos.y > y2)
				{
					continue;
				}
				for (j = 0; j < banz; j ++)
				{
                    if (DataForm.mask[j].pos[0] != h) continue;
					x1 = (DataForm.mask[j].pos[1] - diff) *
						 tm.tmAveCharWidth;
					x2 = x1 + (DataForm.mask[j].length * tm.tmAveCharWidth);
 		    		if (mousepos.x > x1 &&
			    		mousepos.x < x2)
					{
                            breakstat = 1;
					        break;
					}
				}
	    		if (mousepos.x > x1 &&
		    		mousepos.x < x2)
				{
					break;
				}
          }
          if (breakstat) break;
		}
   		if (mousepos.x < x1 ||
    		mousepos.x > x2)
		{
				return FALSE;
		}

		if (i + scrollpos >= recanz) return FALSE;
		if (j >= banz) return FALSE;
        if (IsEditColumn (j) == FALSE)
        {
                return TRUE;
        }

		syskey = 0;
		if (AktColumn != j)
		{
			    if (TestAfter () == -1) return FALSE;
				if (posset) return TRUE;
		}
		COLORREF SaveAktBkColor = ActBkColor;
/*
		ActBkColor = BkColor;
		ShowAktRow ();
		ActBkColor = SaveAktBkColor;
*/
        if (AktRow != i + scrollpos)
        {
                TestAfterRow ();
  		        AktRow = i + scrollpos;
                memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
                TestBeforeRow ();
				if (posset) return TRUE;
        }
        else
        {
 		        AktRow = i + scrollpos;
        }
        if (RowMode &&  j == 0)
        {
		        return TRUE;
        }
        AktColumn = j;
        TestBefore ();
		if (ActBkColor != BkColor)
		{
			ShowAktRow ();
		}
        InvalidateRect (mamain3, &FocusRect, TRUE);
	    UpdateWindow (mamain3);
		SetFeldFocus0 (AktRow, AktColumn);
		return TRUE;
}

void ListClass::DestroyFocusWindow ()
/**
Editfenster loeschen.
**/
{
	       if (FocusWindow)
		   {
			   DestroyWindow (FocusWindow);
			   FocusWindow = NULL;
		       if (ListFocus == 4)
               {
 			         InvalidateRect (mamain3, &FocusRect, TRUE);
               }
		   }

}

void ListClass::KillListFocus ()
/**
Editfenster loeschen.
**/
{
           AktFocus = ListFocus;
           DestroyFocusWindow ();
           ListFocus = 0;
}

void ListClass::SetListFocus ()
/**
Editfenster loeschen.
**/
{
           ListFocus = AktFocus;
           SetFeldFocus0 (AktRow, AktColumn);
}

void ListClass::SetFeldEdit (int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  int x,y ,cx, cy;
		  int diff;
		  HANDLE hInstance;
          int ubspalte;
          DWORD style = 0;
          SIZE size;

          if (EditFont != NULL)
          {
                 HDC hdc = GetDC (mamain3);
                 SelectObject (hdc, EditFont);
 		         GetTextExtentPoint32 (hdc, "X", 1, &size);
                 tm.tmAveCharWidth = size.cx;
                 ReleaseDC (mamain3, hdc);
          }

          if (banz == 0) return;

          ubspalte = GetUbSpalte (spalte);

          hInstance = (HANDLE) GetWindowLong (mamain3, 
                                               GWL_HINSTANCE);
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (ubspalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }

          if ((UseZiel == 0) && (RowMode == 0))
          {
                   strcpy (AktItem, UbForm.mask[ubspalte].item->GetItemName ());
          }

          else if ((UseZiel == 0) && (RowMode == 1))
          {
                   strcpy (AktItem, DataRow->mask[zeile].item->GetItemName ());
          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);

		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());

		  rect.top = (int) (double) ((double) UbHeight + (
			                zeile - scrollpos) * 
                             tm.tmHeight * recHeight * RowHeight - 2);
          rect.top += (int) (double) ((double) 
			             DataForm.mask[spalte].pos[0] * tm.tmHeight 
						       * RowHeight);
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
		  rect.right = rect.left + (DataForm.mask[spalte].length) 
                       * tm.tmAveCharWidth;  
 		  memcpy (&FocusRect, &rect, sizeof (RECT));

		  y  = (int) (double) ((double) UbHeight + 
		  (zeile - scrollpos) * tm.tmHeight * recHeight * RowHeight);
          y += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
		  y = GetMidPos (y);
		  if (y < UbHeight) return;


          cy = tm.tmHeight + tm.tmHeight / 3;
		  x  = (DataForm.mask[spalte].pos[1] - diff) *
			   tm.tmAveCharWidth;

          if (RowMode == 0)
          {
		        cx = (DataForm.mask[spalte].length - 1)
                      * tm.tmAveCharWidth;  
               if (strchr (DataForm.mask[spalte].picture, 'f') ||
                   (strchr (DataForm.mask[spalte].picture, 'd') &&
                        strchr (DataForm.mask[spalte].picture, 'y') == NULL))
               {
                        style |= ES_RIGHT;
               }
          }

		  HWND UserWindow = NULL;
		  if (EditRenderer != 0 && EditColumns != 0 && EditColumnsCount > 0)
		  {
				for (int i = 0; i < EditColumnsCount; i ++)
				{
					if (EditColumns[i] == spalte)
					{
						UserWindow = EditRenderer->SetFeldEdit (zeile, spalte, 
							                                    x, y, cx, cy, 
																mamain3, 
																hInstance);
						break;
					}
				}
		  }

		  if (UserWindow != NULL)
		  {
			  FocusWindow = UserWindow;
			  IsUserRenderer = TRUE;
		  }
		  else
		  {

	          FocusWindow = CreateWindowEx (WS_EX_CLIENTEDGE,
		                                "Edit",
			                            "",
				                        WS_CHILD | ES_MULTILINE | style, 
										x, y, cx, cy,
						                mamain3,
							            NULL,
								        hInstance,
									    NULL);
			  SendMessage (FocusWindow, EM_LIMITTEXT, DataForm.mask[spalte].length, 0l);
			  IsUserRenderer = FALSE;
		  }
  		  if (EditFont == NULL)
		  {
	           	spezfont (&ListFont);
		        EditFont = SetWindowFont (mamain3);
                stdfont ();
		  }
          SendMessage (FocusWindow,  WM_SETFONT, (WPARAM) EditFont, 0);

          SendMessage (FocusWindow,  EM_SETMARGINS, 
                                            (WPARAM) EC_LEFTMARGIN,
                                            MAKELONG (0,0));
          SendMessage (FocusWindow,  EM_SETMARGINS, 
                                            (WPARAM) EC_RIGHTMARGIN,
                                            MAKELONG (0,0));

		  ShowWindow (FocusWindow, SW_SHOWNORMAL);
		  UpdateWindow (FocusWindow);
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowWindowText (rect.left, rect.top);
//          SendMessage (FocusWindow, EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
          TestBefore ();
}


void ListClass::SetFeldEdit0 (int zeile, int spalte, DWORD style)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  int x,y ,cx, cy;
		  int diff;
		  HANDLE hInstance;
          int ubspalte;
          SIZE size;

          if (EditFont != NULL)
          {
                 HDC hdc = GetDC (mamain3);
                 SelectObject (hdc, EditFont);
 		         GetTextExtentPoint32 (hdc, "X", 1, &size);
                 tm.tmAveCharWidth = size.cx;
                 ReleaseDC (mamain3, hdc);
          }

          if (banz == 0) return;
          ubspalte = GetUbSpalte (spalte);

          hInstance = (HANDLE) GetWindowLong (mamain3, 
                                               GWL_HINSTANCE);
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (ubspalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }

          if ((UseZiel == 0) && (RowMode == 0))
          {
                   strcpy (AktItem, UbForm.mask[ubspalte].item->GetItemName ());
          }

          else if ((UseZiel == 0) && (RowMode == 1))
          {
                   strcpy (AktItem, DataRow->mask[zeile].item->GetItemName ());
          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);

		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());
		  rect.top = (int) (double) ((double) UbHeight + (zeile - scrollpos) * 
                     tm.tmHeight * recHeight * RowHeight - 2);
          rect.top += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
		  rect.right = rect.left + (DataForm.mask[spalte].length) 
                       * tm.tmAveCharWidth;  
 		  memcpy (&FocusRect, &rect, sizeof (RECT));

		  y  = (int) (double) ((double) UbHeight + 
			          (zeile - scrollpos) * tm.tmHeight * 
                       recHeight * RowHeight);
          y += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
		  y = GetMidPos (y);
		  if (y < UbHeight) return;


		  cy = tm.tmHeight;
		  x  = (DataForm.mask[spalte].pos[1] - diff) *
			   tm.tmAveCharWidth;
          if (RowMode == 0)
          {
		        cx = (DataForm.mask[spalte].length - 1)
                      * tm.tmAveCharWidth;  
               if (strchr (DataForm.mask[spalte].picture, 'f') ||
                   (strchr (DataForm.mask[spalte].picture, 'd') &&
                        strchr (DataForm.mask[spalte].picture, 'y') == NULL))
               {
                        style |= ES_RIGHT;
               }
          }
          if (style & WS_BORDER)
          {
               x -= tm.tmAveCharWidth;
               cx += tm.tmAveCharWidth;
          }

		  HWND UserWindow = NULL;
		  if (EditRenderer != 0 && EditColumns != 0 && EditColumnsCount > 0)
		  {
				for (int i = 0; i < EditColumnsCount; i ++)
				{
					if (EditColumns[i] == spalte)
					{
						UserWindow = EditRenderer->SetFeldEdit (zeile, spalte, 
							                                    x, y, cx, cy, 
																mamain3, 
																hInstance);
						break;
					}
				}
		  }

		  if (UserWindow != NULL)
		  {
			  FocusWindow = UserWindow;
			  IsUserRenderer = TRUE;
		  }
		  else
		  {

			  FocusWindow = CreateWindow ("Edit",
										"",
										WS_CHILD | ES_MULTILINE | style, 
										x, y, cx, cy,
										mamain3,
										NULL,
										hInstance,
										NULL);
			  SendMessage (FocusWindow, EM_LIMITTEXT, DataForm.mask[spalte].length, 0l);
			  IsUserRenderer = FALSE;
		  }
  		  if (EditFont == NULL)
		  {
	           	spezfont (&ListFont);
		        EditFont = SetWindowFont (mamain3);
                stdfont ();
		  }
          SendMessage (FocusWindow,  WM_SETFONT, (WPARAM) EditFont, 0);

          SendMessage (FocusWindow,  EM_SETMARGINS, 
                                            (WPARAM) EC_LEFTMARGIN,
                                            MAKELONG (0,0));
          SendMessage (FocusWindow,  EM_SETMARGINS, 
                                            (WPARAM) EC_RIGHTMARGIN,
                                            MAKELONG (0,0));


		  ShowWindow (FocusWindow, SW_SHOWNORMAL);
		  UpdateWindow (FocusWindow);
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowWindowText (rect.left, rect.top);
}

void ListClass::SetFocusFrame (HDC hdc, int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  HBRUSH hBrush;
		  int diff;
          int ubspalte;
		  int plus;


		  if (ListFocus != 4) return;
		  if (focusframe == FALSE) return;

          ubspalte = GetUbSpalte (spalte);

          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (ubspalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }
          if (UseZiel == 0)
          {
                   strcpy (AktItem, UbForm.mask[ubspalte].item->GetItemName ());
          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);
		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());
		  rect.top = (int) (double) ((double) UbHeight + 
			         (zeile - scrollpos) * tm.tmHeight * recHeight * 
					  RowHeight);
          rect.top += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
//		  rect.top = GetMidPos (rect.top);
//		  rect.bottom = rect.top + tm.tmHeight;
		  rect.bottom = (int) (double) ((double) rect.top + tm.tmHeight * RowHeight);
		  rect.left = (UbForm.mask[ubspalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
          if (RowMode == 0)
          {
		        rect.right = rect.left + (UbForm.mask[ubspalte].length) *
			                 tm.tmAveCharWidth;  
          }
/*
          else
          {
                rect.right = rect.left + LstZiel[zeile].feldlen *
                             tm.tmAveCharWidth;
          }
*/
		  if (RowHeight == (double) 1.0)
		  {
               rect.left -= 1;
               rect.right += 1;
               rect.top -= 1;
               rect.bottom += 1;
			   plus = 1;
		  }
		  else
		  {
               rect.left -= 1;
               rect.right += 1;
               rect.top -= 1;
               rect.bottom += 1;
  			   plus = 2;
		  }


//		  memcpy (&FocusRect, &rect, sizeof (RECT));
          

          if (BkColor == BLUECOL)
          {
                 hBrush = CreateSolidBrush (WHITECOL);
          }
          else if (BkColor == BLACKCOL)
          {
                 hBrush = CreateSolidBrush (WHITECOL);
          }
          else
          {
                 hBrush = CreateSolidBrush (BLACKCOL);
          }
          SelectObject (hdc, GetStockObject (PS_DASH));
          FrameRect (hdc, &rect, hBrush);

          HBRUSH oldBrush = SelectObject (hdc, hBrush);
          rect.top -= plus;
          rect.left -= plus;
          rect.bottom += plus;
          rect.right += plus;
          FrameRect (hdc, &rect, hBrush);
		  memcpy (&FocusRect, &rect, sizeof (RECT));

          SelectObject (hdc, oldBrush);
          DeleteObject (hBrush);
		  AktRow    = zeile;
          AktColumn = spalte;
}


void ListClass::SetFeldFrame (HDC hdc, int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  HBRUSH hBrush;
		  int diff;
		  
          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }
          if (UseZiel == 0)
          {
                   strcpy (AktItem, UbForm.mask[spalte].item->GetItemName ());
          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);
		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());

//		  rect.top = (zeile - scrollpos + 1) * tm.tmHeight;
		  rect.top = (int) (double) ((double) UbHeight + 
			          (zeile - scrollpos) * tm.tmHeight * recHeight * RowHeight);
          rect.top += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
          if (RowMode == 0)
          {
		        rect.right = rect.left + DataForm.mask[spalte].length *
		                    tm.tmAveCharWidth;  
          }
/*
          else
          {
                rect.right = rect.left + LstZiel[zeile].feldlen *
                             tm.tmAveCharWidth;
          }
*/
		  memcpy (&FocusRect, &rect, sizeof (RECT));


          hBrush = CreateSolidBrush (BLUECOL);
          HBRUSH oldBrush = SelectObject (hdc, hBrush);
          SelectObject (hdc, GetStockObject (NULL_PEN));
          Rectangle (hdc,rect.left, rect.top, rect.right, rect.bottom);
          SelectObject (hdc, oldBrush);
          DeleteObject (hBrush); 
		  AktRow    = zeile;
          AktColumn = spalte;

		  ShowFocusText (hdc, rect.left, rect.top);
}


void ListClass::SetFeldFocus (HDC hdc, int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	      RECT rect;
		  HBRUSH hBrush;
		  int diff;


		  if (InMove) return;

		  DestroyFocusWindow ();
		  if (ListFocus == 0) return;

          PrintUSlinesSatzNr (hdc);


          DataForm.mask[spalte].item->PrintComment ();
		  if (ListFocus == 9)
		  {
		           SetFeldFrame (hdc, zeile, spalte);
                   PrintSlinesSatzNr (hdc);
		           return;
		  }

		  if (ListFocus == 2)
		  {
		           SetFeldEdit0 (zeile, spalte, NULL);
                   PrintSlinesSatzNr (hdc);
		           return;
		  }
		  else if (ListFocus == 3)
		  {
		           SetFeldEdit (zeile, spalte);
                   PrintSlinesSatzNr (hdc);
		           return;
		  }
		  else if (ListFocus == 4)
		  {

			       if (InvalidateFocusFrame)
				   {
					   InvalidateRect (mamain3, &FocusRect, TRUE);
				   }

		           SetFeldEdit0 (zeile, spalte, NULL);
				   if (!IsUserRenderer)
				   {
						SetFocusFrame (hdc, zeile, spalte);
				   }
                   PrintSlinesSatzNr (hdc);
		           return;
		  }
		  else if (ListFocus == 5)
		  {
		           SetFeldEdit0 (zeile, spalte, WS_BORDER);
                   PrintSlinesSatzNr (hdc);
		           return;
		  }

          if (zeile >= recanz) return;
		  if (spalte >= banz) return;
          if (spalte < LineForm.frmstart) return; 

          diff = 0;
          if (LineForm.frmstart)
		  {
//            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                   fSatzNr.mask[0].length;
            if (NoRecNr == FALSE)
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
            }
            else
            {
                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
            }
		  }
          if (UseZiel == 0)
          {
                   strcpy (AktItem, UbForm.mask[spalte].item->GetItemName ());
          }

		  clipped (AktItem);
          memcpy (ausgabesatz, SwSaetze [zeile], zlen);
		  strcpy (AktValue, 
			      DataForm.mask[spalte].item->GetFeldPtr ());
//		  rect.top = (zeile - scrollpos + 1) * tm.tmHeight;
		  rect.top = (int) (double) ((double) UbHeight + 
			                          (zeile - scrollpos) * 
									  tm.tmHeight * recHeight * RowHeight);
          rect.top += (int) (double) ((double) 
			  DataForm.mask[spalte].pos[0] * tm.tmHeight * RowHeight);
		  rect.top = GetMidPos (rect.top);
		  rect.bottom = rect.top + tm.tmHeight;
		  rect.left = (DataForm.mask[spalte].pos[1] - diff) *
			          tm.tmAveCharWidth;
          if (RowMode == 0)
          {
		        rect.right = rect.left + DataForm.mask[spalte].length *
			                 tm.tmAveCharWidth;  
          }
/*
          else
          {
                rect.right = rect.left + LstZiel[zeile].feldlen *
                             tm.tmAveCharWidth;
          }
*/
		  memcpy (&FocusRect, &rect, sizeof (RECT));


          hBrush = CreateSolidBrush (BLUECOL);
          HBRUSH oldBrush = SelectObject (hdc, hBrush);
          SelectObject (hdc, GetStockObject (PS_DASH));
          FrameRect (hdc, &rect, hBrush);
          SelectObject (hdc, oldBrush);
          DeleteObject (hBrush);
		  AktRow    = zeile;
          AktColumn = spalte;
          PrintSlinesSatzNr (hdc);
}

int ListClass::DelRec (void)
/**
Satz aus Liste loeschen.
**/
{
          int i;
 		  SCROLLINFO scinfo;
          
          if (ListFocus == 0) return recanz;

          recanz --;
          for (i = AktRow; i < recanz; i ++)
          {
              SwSaetze[i] = SwSaetze[ i + 1];
          }
          
          if (mamain3)
          {
             SendMessage (mamain2, WM_SIZE, NULL, NULL);
             InvalidateRect (mamain3, 0, TRUE);
             TestVTrack ();
             if (vTrack)
             {
 		          scinfo.cbSize = sizeof (SCROLLINFO);
		          scinfo.fMask  = SIF_PAGE | SIF_RANGE;
		          scinfo.nMin   = 0;
		          scinfo.nMax   = recanz - 1;
		          if (lPagelen)
                  {
		             scinfo.nPage  = 1;
                  }
		          else
                  {
			         scinfo.nPage = 0;
                  }
                  SetScrollInfo  (vTrack, SB_CTL, &scinfo, TRUE);
             }
          }
          
          return recanz;
}

BOOL ListClass::IsEditColumn (int col)
/**
Test, ob die mit der Maus aktivierte Spalte ein Edit-Feld ist.
**/
{
          if (DataForm.mask[col].attribut == EDIT)
          {
              return TRUE;
          }
          return FALSE;
}

int ListClass::FirstColumn (void)
/**
N�chste Spalte suchen.
**/
{

          int i;
//          int h;

          i = 0;

/*
          for (h = 0; h < recHeight; h ++)
          {
            while (TRUE)
            {

              if (DataForm.mask[i].pos[0] != h) 
              {
                  i ++;
                  continue;
              }
              if (DataForm.mask[i].attribut == EDIT)
              {
                  return i;
              }
              i ++;
              if (i == DataForm.fieldanz)
              {
                  return 0;
              }
            }
          }
*/
          while (TRUE)
          {

              if (DataForm.mask[i].attribut == EDIT)
              {
                  return i;
              }
              i ++;
              if (i == DataForm.fieldanz)
              {
                  return 0;
              }
          }
          return i;
}

int ListClass::FirstColumnR (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = 0;

          while (TRUE)
          {
              if (DataRow->mask[i].attribut == EDIT)
              {
                  return i;
              }
              i ++;
              if (i == DataRow->fieldanz)
              {
                  return 0;
              }
          }
          return i;
}

          
int ListClass::PriorColumn (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktColumn;

          while (TRUE)
          {
              i --;
              if (i < 0)
              {
                  i = DataForm.fieldanz - 1;
              }
              if (DataForm.mask[i].attribut == EDIT)
              {
                  return i;
              }
          }
          return i;
}

int ListClass::PriorColumnR (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktRow;

          while (TRUE)
          {
              i --;
              if (i < 0)
              {
                  i = DataRow->fieldanz - 1;
              }
              if (DataRow->mask[i].attribut == EDIT)
              {
                  return i;
              }
          }
          return i;
}


void ListClass::FocusLeft (void)
/**
Focus nachh rechts setzen.
**/
{
          if (RowMode) 
          {
              PriorPageRow ();
              return;
          }

	      AktColumn = PriorColumn ();
		  while (AktColumn < DataForm.frmstart) 
          {
              ScrollLeft ();
          }
          InvalidateRect (mamain3, &FocusRect, TRUE);
          UpdateWindow (mamain3);
		  SetFeldFocus0 (AktRow, AktColumn);
}

		  
int ListClass::NextColumn (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktColumn;

          while (TRUE)
          {
              i ++;
              if (i == DataForm.fieldanz)
              {
                  i = 0;

              }
              if (DataForm.mask[i].attribut == EDIT)
              {
                  return i;
              }
          }
          return i;
}

int ListClass::NextColumnR (void)
/**
N�chste Spalte suchen.
**/
{

          int i;

          i = AktRow;

          while (TRUE)
          {
              i ++;
              if (i == DataRow->fieldanz)
              {
                  i = 0;

              }
              if (DataRow->mask[i].attribut == EDIT)
              {
                  return i;
              }
          }
          return i;
}


void ListClass::FocusFirst (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;
          int x;
		  int col;
          int diff;
          int Scrolled;

          if (RowMode)
          {
              return;
          }

		  AktColumn = -1;

		  col = NextColumn ();

		  AktColumn = col;

		  while (AktColumn < DataForm.frmstart) 
          {
              ScrollLeft ();
          }

          GetClientRect (mamain2, &rect);

          diff = Scrolled = 0;
          if (LineForm.frmstart)
          {
//                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                        fSatzNr.mask[0].length;
			       if (NoRecNr == FALSE)
				   {
                            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                                              fSatzNr.mask[0].length;
				   }
				   else
				   {
                            diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
				   }
          }
          x = (DataForm.mask[AktColumn].pos[1] - diff + 
               DataForm.mask[AktColumn].length) *
                   tm.tmAveCharWidth;
          if (x <= rect.right)
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
	  	      UpdateWindow (mamain3);
 		      SetFeldFocus0 (AktRow, AktColumn);
              return;
          }
          ScrollRight ();
  		  if (NoRecNr == FALSE)
		  {
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                             fSatzNr.mask[0].length;
		  }
		  else
		  {
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
		  }
          x = (DataForm.mask[AktColumn].pos[1] - diff + 
               DataForm.mask[AktColumn].length) *
                   tm.tmAveCharWidth;
          while (x > rect.right)
          {
              ScrollRight ();
			  if (NoRecNr == FALSE)
			  {
                       diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                                  fSatzNr.mask[0].length;
			  }
			  else
			  {
                       diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			  }
              x = (DataForm.mask[AktColumn].pos[1] - diff + 
                       DataForm.mask[AktColumn].length) *
                       tm.tmAveCharWidth;
          }
	      SetFeldFocus0 (AktRow, AktColumn);
}
		  


void ListClass::FocusRight (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;
          int x;
          int diff;
          int col;
          int Scrolled;

          if (RowMode)
          {
              NextPageRow ();
              return;
          }

		  if (syskey != KEYCR && AktColumn >= banz - 1) return;

		  col = NextColumn ();
		  if (FokusDownbeiKEYCR) col = AktColumn ;   //LAC-185a    FocusDown nach Menge erzwingen
          if (syskey == KEYCR && col <= AktColumn)
          {
/*
                    AktColumn = FirstColumn ();
 		            while (DataForm.frmstart > 0) 
                    {
                              ScrollLeft ();
                    }
*/

                    FocusDown ();
                    return;
          }
             

          AktColumn = col;
		  while (AktColumn < DataForm.frmstart) 
          {
              ScrollLeft ();
          }

          GetClientRect (mamain3, &rect);

          diff = Scrolled = 0;
          if (LineForm.frmstart)
          {
//                 diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                        fSatzNr.mask[0].length;
			       if (NoRecNr == FALSE)
				   {
                            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                                              fSatzNr.mask[0].length;
				   }
				   else
				   {
                            diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
				   }
          }
          x = (DataForm.mask[AktColumn].pos[1] - diff + 
               DataForm.mask[AktColumn].length) *
                   tm.tmAveCharWidth;
          if (x <= rect.right)
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
	  	      UpdateWindow (mamain3);
 		      SetFeldFocus0 (AktRow, AktColumn);
              return;
          }
          ScrollRight ();
//        diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//               fSatzNr.mask[0].length;
  		  if (NoRecNr == FALSE)
		  {
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                             fSatzNr.mask[0].length;
		  }
		  else
		  {
                    diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
		  }
          x = (DataForm.mask[AktColumn].pos[1] - diff + 
               DataForm.mask[AktColumn].length) *
                   tm.tmAveCharWidth;
          while (x > rect.right)
          {
              ScrollRight ();
//              diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
//                          fSatzNr.mask[0].length;
			  if (NoRecNr == FALSE)
			  {
                       diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                                  fSatzNr.mask[0].length;
			  }
			  else
			  {
                       diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			  }
              x = (DataForm.mask[AktColumn].pos[1] - diff + 
                       DataForm.mask[AktColumn].length) *
                       tm.tmAveCharWidth;
          }
	      SetFeldFocus0 (AktRow, AktColumn);
}
		  
	        
void ListClass::FocusUp (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;
          int Col;
          
		  if (RowMode == 0 && AktRow <= 0) return;

          InAppend = 0;
          NewRec = 0;
          SetInsAttr ();
          Col = FirstColumn ();
          if (AktColumn < Col)
          {
                  SetPos (AktRow, Col);
          }

          GetClientRect (mamain3, &rect);
          rect.top += UbHeight;
		  if (NoRecNr == FALSE)
		  {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		  }
          if (RowMode)
          {
                 AktRow = PriorColumnR ();
          }
          else
          {
                 if (TestAfterRow () != -1)
                 {
		                  AktRow --;
                 }
          }
		  if (AktRow < scrollpos)
          {
                    InvalidateRect (mamain3, &rect, TRUE);
                    ScrollUp (); 
          }
          else
          {
                    InvalidateRect (mamain3, &FocusRect, TRUE);
                    UpdateWindow (mamain3);
          }
          if (RowMode)
          {
                 TestBeforeR ();
          }
          else
          {
                 memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
                 TestBeforeRow ();
          }
		  if (ActBkColor != BkColor)
		  {
				ShowAktRow ();
		  }
		  SetFeldFocus0 (AktRow, AktColumn);
}


int ListClass::TestBeforeRow (void)
/**
Pruefung beim Verlassen der Zeile.
**/
{
         if (DataForm.before)
         {
             return (*DataForm.before) ();
         }
         return 0;
}
          

int ListClass::TestAfterRow (void)
/**
Pruefung beim Verlassen der Zeile.
**/
{

         if (TestAfter () == -1)
         {
             return (-1);
         }

         if (DataForm.after)
         {
             return (*DataForm.after) ();
         }
         return 0;
}

void ListClass::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < DataForm.fieldanz; i ++)
         {
             feldname = DataForm.mask[i].item->GetItemName ();
             if (feldname && strcmp (fname, feldname) == 0)
             {
                   break;
             }
         }
         if (i == DataForm.fieldanz) return;

         DataForm.mask[i].attribut = attr;
}

void ListClass::SetFieldAttr (CHATTR *cha)
{
         int i;
         char *feldname;

         for (i = 0; i < DataForm.fieldanz; i ++)
         {
             feldname = DataForm.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, cha->feldname) == 0)
             {
                   break;
             }
         }
         if (i == DataForm.fieldanz) return;

         if (NewRec || InAppend)
         {
                DataForm.mask[i].attribut = cha->InsAttr;
         }
         else
         {
                DataForm.mask[i].attribut = cha->UpdAttr;
         }
}
             
void ListClass::SetInsAttr (void)
/**
Feldattribute wechseln.
**/
{
         int i;

         if (ChAttr == NULL) return;

         for (i = 0; ChAttr[i].feldname; i ++)
         {
             SetFieldAttr (&ChAttr[i]);
         }
}


          

void ListClass::FocusDown (void)
/**
Focus nachh unten setzen.
**/
{
          RECT rect;
          int append = 0;


		  if (AktRow >= recanz - 1)
          {

              if (RowMode)
              {
                  if (syskey == KEYCR)
                  {
             	      SetFeldFocus0 (AktRow, AktColumn);
                  }
                  return;
              }
              append = 1;
          }
          else if (InAppend && RowMode == 0)
          {
              append = 1;
          }

          SetInsAttr ();
          GetClientRect (mamain3, &rect);
          rect.top += UbHeight;
		  if (NoRecNr == FALSE)
		  {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		  }
          if (RowMode)
          {
                 AktRow = NextColumnR ();
          }
          else
          {
                 if (append)
                 {
                      if (TestAfterRow () != -1)
                      {
                                 AktColumn = FirstColumn ();
   		                         while (DataForm.frmstart > 0) 
                                 {
                                        ScrollLeft ();
                                 }
                                 AppendLine ();
                      }
                 }
                 else
                 {
                      if (TestAfterRow () != -1)
                      {
		                        AktRow ++;
                      }
                      if (syskey == KEYCR)
                      {
                                AktColumn = FirstColumn ();
  		                        while (DataForm.frmstart > 0) 
                                {
                                          ScrollLeft ();
                                }
                      }
                 }
          }

          if (lPagelen < recanz && 
			  AktRow - scrollpos >= lPagelen)
          {
//              InvalidateRect (mamain3, &rect, TRUE);
//              InvalidateRect (mamain3, &FocusRect, TRUE);
//              InvalidateRect (mamain3, NULL, TRUE);
//	   	      UpdateWindow (mamain3);
              ScrollDown ();
              InvalidateRect (mamain3, NULL, TRUE);
	   	      UpdateWindow (mamain3);
          }
          else
          {
              InvalidateRect (mamain3, &FocusRect, TRUE);
	   	      UpdateWindow (mamain3);
          }
          if (RowMode)
          {
                 TestBeforeR ();
          }
          else
          {
                 memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
                 TestBeforeRow ();
          }
		  if (ActBkColor != BkColor)
		  {
				ShowAktRow ();
		  }
	      SetFeldFocus0 (AktRow, AktColumn);
}
  
    

void ListClass::SetFeldFocus0 (int zeile, int spalte)
/**
Focus in ein Feldsetzen.
**/
{
	    HDC hdc;

		hdc = GetDC (mamain3);
        SetFeldFocus (hdc, zeile, spalte);
        ReleaseDC (mamain3, hdc);
        SelRow = AktRow;
}


void ListClass::SetDataStart (void)
/**
frmstart bei DataForms erhoehen.
**/
{

        DataForm.frmstart = UbForm.frmstart;
        LineForm.frmstart = UbForm.frmstart;
}

BOOL ListClass::MustPaint (int zeile)
/**
Feststellen, ob der Bereich neu gezeichnet werden muss.
**/
{
         int py;
         PAINTSTRUCT *ps;
         int Painttop;
         int Paintbottom;

         if (paintallways) return TRUE;

         ps = &aktpaint;

/* Update-Region ermitteln.                              */

         py =  zeile;

         Painttop    = ps->rcPaint.top    -  tm.tmHeight;
         Paintbottom = ps->rcPaint.bottom +  tm.tmHeight;

         if (py < Painttop) return FALSE;
         if (py > Paintbottom) return FALSE;
         return TRUE;
}

void ListClass::PrintUSlinesSatzNr (HDC hdc)
/**
Horizontale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int i;
        int x, y, y1;
		int RHeight;

        if (NoRecNr) return;

		RHeight = (int) (double) ((double) tm.tmHeight * recHeight * RowHeight + 0.5);

        if (recanz == 0) return;
        if (SelRow >= recanz) return;
        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;

        hPen  = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        i = SelRow - scrollpos;

        SelectObject (hdc, hPenW);

        y = (int) (double) ((double) UbHeight + i * 
			       tm.tmHeight * recHeight * RowHeight);

//        y = UbHeight + i * RHeight;
        y1 = y;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPen);
        y ++;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);
 
        SelectObject (hdc, hPenG);

        y = (int) (double) ((double) 
			UbHeight + (i + 1) * tm.tmHeight * recHeight * RowHeight 
			       - 1);

//		y += (RHeight - 1);
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPen);
        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPen);
        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);


        SelectObject (hdc, hPenG);
        MoveToEx (hdc, x - 1, y1, NULL);
        LineTo (hdc,   x - 1, y + 1);

        SelectObject (hdc, hPen);
        MoveToEx (hdc, x - 2, y1, NULL);
        LineTo (hdc,   x - 2, y + 1);

        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}

void ListClass::PrintSlinesSatzNr (HDC hdc)
/**
Vertikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int i;
        int x, y, y1;
		int RHeight;

        if (NoRecNr) return;
		RHeight = (int) (double) ((double) tm.tmHeight * recHeight * RowHeight + 0.5);

        if (recanz == 0) return;
        if (AktRow >= recanz) return;
        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;

        hPen  = CreatePen (PS_SOLID, 0, BLACKCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        i = AktRow - scrollpos;

        SelectObject (hdc, hPenW);

        y = (int) (double) ((double) UbHeight + 
			       i * tm.tmHeight * recHeight * RowHeight);

//        y = UbHeight + i * RHeight;
        y1 = y;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        y ++;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);
 
        SelectObject (hdc, hPen);

        y = (int) (double) ((double) UbHeight + 
			 (i + 1) * tm.tmHeight * recHeight * RowHeight - 1);

//		y += RHeight;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPenG);
        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);

        SelectObject (hdc, hPen);
        MoveToEx (hdc, x, y1, NULL);
        LineTo (hdc,   x, y + 1);

        SelectObject (hdc, hPen);
        MoveToEx (hdc, x - 1, y1, NULL);
        LineTo (hdc,   x - 1, y + 1);

        SelectObject (hdc, hPenG);
        MoveToEx (hdc, x - 2, y1, NULL);
        LineTo (hdc,   x - 2, y + 1);

         
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}

/*
void ListClass::PrintVlineSatzNr (HDC hdc)
/?*
Verikale Linien zeichnen.
*?/
{
        static HPEN hPen;
        HPEN oldPen;
        int x, y;
        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        if (NoRecNr) return;

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;
        y = (int) (double) ((double) UbHeight + 
			recanz * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, GRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 2;
        y = (int) (double) ((double) UbHeight + 
			recanz * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}
*/

void ListClass::PrintVlineSatzNr (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int x, y;


        if (NoRecNr) return;
        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 1;
        y = (int) (double) ((double) UbHeight + 
			recanz * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, GRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth - 2;
        y = (int) (double) ((double) UbHeight + 
			recanz * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, x, tm.tmHeight, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}


void ListClass::SetListLines (int cnr)
{
        LineColor = cnr;
        if (mamain3 == NULL) return;
        InvalidateRect (mamain3, NULL, TRUE);
        UpdateWindow (mamain3);
}

void ListClass::Print3DVlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintVlineSatzNr (hdc);
        if (LineForm.mask == NULL) return;

        diff = 0;
        if (LineForm.frmstart)
        {
			if (NoRecNr == 0)
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                               fSatzNr.mask[0].length;
			}
			else
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
        }
        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x -= Plus3D;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{
          for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x -= 1;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
		  }
        }
        for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
        }
		if (Plus3D > 1)
		{
          for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += 1;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
		  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += Plus3D;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        Print3DHlinesEnd (hdc);
        PrintVlineSatzNr (hdc);

        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}

     
void ListClass::PrintVlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};

		if (print3D)
		{
                Print3DVlines (hdc);
				return;
		}

        PrintVlineSatzNr (hdc);
        if (LineColor > 11) return;
        if (LineColor > 7) return;
        if (LineForm.mask == NULL) return;

        diff = 0;
        if (LineForm.frmstart)
        {
			if (NoRecNr == 0)
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                               fSatzNr.mask[0].length;
			}
			else
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
        }
        hPen = CreatePen (PS_SOLID, 0, lcol[LineColor]);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				        recanz * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, UbHeight,    NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        PrintVlineSatzNr (hdc);
}


void ListClass::Print3DVline (HDC hdc, int z)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintVlineSatzNr (hdc);
        if (LineForm.mask == NULL) return;

        diff = 0;
        if (LineForm.frmstart)
        {
			if (NoRecNr == 0)
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                               fSatzNr.mask[0].length;
			}
			else
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
        }
        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
	          x -= Plus3D;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{
          for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x -= 1;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
		  }
        }
        for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
              LineTo (hdc, x, y);
        }
		if (Plus3D > 1)
		{
          for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
		  {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += 1;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
              LineTo (hdc, x, y);
		  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
			  x += Plus3D;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

        Print3DHlineEnd (hdc, z);
        PrintVlineSatzNr (hdc);

        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}


void ListClass::PrintVline (HDC hdc, int z)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        int i;
        int x, y;
        int diff;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL, WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


		if (print3D)
		{
                Print3DVline (hdc, z);
				return;
		}

        PrintVlineSatzNr (hdc);
        if (LineColor > 11) return;
        if (LineColor > 7) return;
        if (LineForm.mask == NULL) return;

        diff = 0;
        if (LineForm.frmstart)
        {
			if (NoRecNr == 0)
			{
               diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                              fSatzNr.mask[0].length;
			}
			else
			{
               diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
        }
        hPen = CreatePen (PS_SOLID, 0, lcol[LineColor]);
        oldPen = SelectObject (hdc, hPen);

        for (i = LineForm.frmstart; i < LineForm.fieldanz; i ++)
        {
              x = (LineForm.mask[i].pos[1] - diff) * 
                   tm.tmAveCharWidth;
              y = (int) (double) ((double) UbHeight + 
				  z * tm.tmHeight * recHeight * RowHeight);
              MoveToEx (hdc, x, y, NULL);
              y = (int) (double) ((double) UbHeight + 
				  (z  + 1) * tm.tmHeight * recHeight * RowHeight);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        PrintVlineSatzNr (hdc);
}


void ListClass::PrintHlinesSatzNr (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int i;
        int x, y;
		int RHeight;

        if (NoRecNr) return;

		RHeight = (int) (double) ((double) tm.tmHeight * recHeight * RowHeight + 0.5);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth;

        hPen  = CreatePen (PS_SOLID, 0, BLACKCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i < recanz; i ++)
        {
              SelectObject (hdc, hPenW);

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);

              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x - 1, y);
              }

              SelectObject (hdc, hPen);

              y = (int) (double) ((double) UbHeight + (i + 1) * 
				  tm.tmHeight * recHeight * RowHeight);

              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }

              SelectObject (hdc, hPenG);
              y --;

              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x - 1, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}


void ListClass::PrintHlineSatzNr (HDC hdc, int i)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        static HPEN hPenG;
        static HPEN hPenW;
        HPEN oldPen;
        int x, y;
		int RHeight;

		RHeight = (int) (double) ((double) tm.tmHeight * recHeight * RowHeight + 0.5);

        x = fSatzNr.mask[0].length * tm.tmAveCharWidth;

        hPen  = CreatePen (PS_SOLID, 0, BLACKCOL);
        hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
        hPenW = CreatePen (PS_SOLID, 0, WHITECOL);

        oldPen = SelectObject (hdc, hPen);
        SelectObject (hdc, hPenW);

        y = (int) (double) ((double) UbHeight + 
			i * tm.tmHeight * recHeight * RowHeight);

        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);


        SelectObject (hdc, hPen);
        y = (int) (double) ((double) UbHeight + 
			(i + 1) * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);

        SelectObject (hdc, hPenG);
        y --;
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x - 1, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        DeleteObject (hPenG);
        DeleteObject (hPenW);
}


void ListClass::Print3DHlinesEnd (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int i;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintHlinesSatzNr (hdc);
        GetClientRect (mamain3, &rect);
        x = rect.right;


        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{
          for (i = 0; i <= recanz; i ++)
		  {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y -= 1;
			  if (y < UbHeight) continue;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
		  }
		}
        for (i = 0; i <= recanz; i ++)
        {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
		if (Plus3D > 1)
		{
          for (i = 0; i <= recanz; i ++)
		  {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y += 1;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
		  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        PrintHlinesSatzNr (hdc);
}


void ListClass::Print3DHlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int i;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintHlinesSatzNr (hdc);
        GetClientRect (mamain3, &rect);
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i <= recanz; i ++)
        {
              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y -= Plus3D;
			  if (y < UbHeight) continue;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{
          for (i = 0; i <= recanz; i ++)
		  {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y -= 1;
			  if (y < UbHeight) continue;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
		  }
		}
        for (i = 0; i <= recanz; i ++)
        {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
		if (Plus3D > 1)
		{
          for (i = 0; i <= recanz; i ++)
		  {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y += 1;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
		  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i <= recanz; i ++)
        {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y += Plus3D;
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        PrintHlinesSatzNr (hdc);
}


void ListClass::PrintHlines (HDC hdc)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int i;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


		if (print3D)
		{
		         Print3DHlines (hdc);
		         return;
		}

        PrintHlinesSatzNr (hdc);
        if (LineColor > 11) return;
        if (LineColor > 3 && LineColor < 8) return;
        GetClientRect (mamain3, &rect);
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, lcol [LineColor]);
        oldPen = SelectObject (hdc, hPen);
        for (i = 0; i <= recanz; i ++)
        {

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
              if (MustPaint (y))
              {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
              }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
        PrintHlinesSatzNr (hdc);
}

void ListClass::Print3DHline (HDC hdc, int i)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
		RECT rect;
        HPEN oldPen;
        int x, y;
		int p = 0;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


        PrintHlinesSatzNr (hdc);
        GetClientRect (mamain3, &rect);
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, WHITECOL);
        oldPen = SelectObject (hdc, hPen);

        y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
  	    y -= Plus3D;
	    if (y > UbHeight + p)
		{
               MoveToEx (hdc, 0, y, NULL);
               LineTo (hdc, x, y);
		}
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y -= 1;
			  if (y > UbHeight + p)
			  {
                    MoveToEx (hdc, 0, y, NULL);
                    LineTo (hdc, x, y);
			  }
		}

        y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
 	    if (y > UbHeight + 2)
		{
			  if (y > UbHeight + p)
			  {
                    MoveToEx (hdc, 0, y, NULL);
                    LineTo (hdc, x, y);
			  }
		}
		if (Plus3D > 1)
		{

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y += 1;
 	          if (y > UbHeight + p)
			  {
                     MoveToEx (hdc, 0, y, NULL);
                     LineTo (hdc, x, y);
			  }
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        hPen = CreatePen (PS_SOLID, 0, BLACKCOL);
        oldPen = SelectObject (hdc, hPen);

        y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
		y += Plus3D;
        if (y > UbHeight + p)
		{
               MoveToEx (hdc, 0, y, NULL);
               LineTo (hdc, x, y);
		}
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        PrintHlinesSatzNr (hdc);
}

void ListClass::Print3DHlineEnd (HDC hdc, int i)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};

        PrintHlinesSatzNr (hdc);
        GetClientRect (mamain3, &rect);
        x = rect.right;


        hPen = CreatePen (PS_SOLID, 0, LTGRAYCOL);
        oldPen = SelectObject (hdc, hPen);

		if (Plus3D > 1)
		{

              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y -= 1;
              MoveToEx (hdc, 0, y, NULL);
              LineTo (hdc, x, y);
		}

        y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);
 		if (Plus3D > 1)
		{
 
              y = (int) (double) ((double) UbHeight + 
				  i * tm.tmHeight * recHeight * RowHeight);
			  y += 1;
              MoveToEx (hdc, 0, y, NULL);
              LineTo (hdc, x, y);
        }
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);

        PrintHlinesSatzNr (hdc);
}



void ListClass::PrintHline (HDC hdc, int i)
/**
Verikale Linien zeichnen.
**/
{
        static HPEN hPen;
        HPEN oldPen;
        RECT rect;
        int x, y;

        static COLORREF lcol[] = {WHITECOL, LTGRAYCOL, BLACKCOL, 
            GRAYCOL,0,0,0,0, 
            WHITECOL, LTGRAYCOL, BLACKCOL, GRAYCOL};


		if (print3D)
		{
		         Print3DHline (hdc, i);
		         return;
		}
        if (LineColor > 11) return;
        if (LineColor > 3 && LineColor < 8) return;
        GetClientRect (mamain3, &rect);
        x = rect.right;

        hPen = CreatePen (PS_SOLID, 0, lcol [LineColor]);
        oldPen = SelectObject (hdc, hPen);

        y = (int) (double) ((double) UbHeight + 
			i * tm.tmHeight * recHeight * RowHeight);
        MoveToEx (hdc, 0, y, NULL);
        LineTo (hdc, x, y);
        SelectObject (hdc, oldPen);
        DeleteObject (hPen);
}


#ifndef _PFONT

void ListClass::FillFrmRow (char *dest, form *frm, int h)
{
        int i;
        char buffer [1024];
        int ldiff;
        int pos;
        unsigned int frmlen;

        memset (dest, ' ', 0x1000);

        ldiff = 0;

        ldiff = LineForm.mask[LineForm.frmstart - 1].pos[1];


		if (NoRecNr == 0)
		{
			if (LineForm.frmstart)
			{
               ldiff = LineForm.mask[LineForm.frmstart - 1].pos[1];
				                       + fSatzNr.mask[0].length;
			}
			else
			{
				ldiff = fSatzNr.mask[0].length;
			}
		}


        frmlen = 0;
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
            if (frm->mask[i].pos[0] == h)
            {
                   ToFormat (buffer, &frm->mask[i]);

                   pos = frm->mask[i].pos[1];
                   pos = pos - ldiff;
                   if (pos < 0) continue;
                   if (frmlen < pos + strlen (buffer))
                   {
                       frmlen = pos + strlen (buffer);
                   }
                   memcpy (&dest[pos], buffer,
                           strlen (buffer));
            }
        }
        dest[frmlen] = (char) 0;
}

#else

void ListClass::FillFrmRow (HDC hdc, char *dest, form *frm, int zeile, int h)
{
        int i;
        char buffer [1024];
        int ldiff;
        int szdiff;
        int pos;
		int ubpos;
        unsigned int frmlen;
		int x, y;
		SIZE size;
		SIZE sizes;
		RECT rect;
		CColBkColor *ColBkColor;

/*

		if (zeile == AktRow - scrollpos)
		{
			RECT rect;
			GetClientRect (mamain3, &rect);
			rect.top = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
			  recHeight * RowHeight 
			  + tm.tmHeight * h * RowHeight);
			rect.bottom = (int) ((double) rect.top + tm.tmHeight * RowHeight);
			HBRUSH hBrush = CreateSolidBrush (ActBkColor);
			FillRect (hdc, &rect, hBrush);
			DeleteObject (hBrush);
		}
*/

//        PrintColBkColors (hdc, zeile, h);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
         y = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
			  recHeight * RowHeight 
			 + tm.tmHeight * h * RowHeight);
		 y = GetMidPos (y);
        ldiff = 0;

        if (LineForm.frmstart > 0)
        {
              ldiff = LineForm.mask[LineForm.frmstart - 1].pos[1];
 		      if (NoRecNr == 0) ldiff -= fSatzNr.mask[0].length;
        }

		if (NoRecNr == 0)
		{
                SetTextColor (hdc,BLACKCOL);
                SetBkColor (hdc,LTGRAYCOL);
                TextOut (hdc, 0, y, SatzNr, strlen (SatzNr));
		}

        frmlen = 0;
        szdiff = 0;
        for (i = frm->frmstart; i < frm->fieldanz; i ++)
        {
            if (frm->mask[i].pos[0] == h)
            {
                   ToFormat (buffer, &frm->mask[i]);
                   pos = frm->mask[i].pos[1];

                   if (strchr (frm->mask[i].picture, 'f') ||
                       (strchr (frm->mask[i].picture, 'd') &&
                        strchr (frm->mask[i].picture, 'y') == NULL))
                   {
                        clipped (buffer);
                   		GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &sizes);
                        pos = (pos + frm->mask[i].length - 1) * size.cx;
                        pos -= sizes.cx; 
                        pos -= ldiff * size.cx;
                        if (pos < 0) continue;
				        x = pos;
                   } 
                   else

                   {
                        pos = pos - ldiff;
                        if (pos < 0) continue;
				        x = pos * size.cx;
                   }

				   ColBkColor = NULL;
	 			   ubpos = GetUbPos (frm, i);
				   if (ubpos != -1)
				   {
				 	   ColBkColor = GetColBkColor (ubpos - 1);
				   }
                   pos -= (szdiff - size.cx);
                   SetTextColor (hdc,Color);

				   if (ColBkColor != NULL)
				   {
						pos = UbForm.mask[ubpos].pos[1];
						pos = pos - ldiff;
						if (pos >= 0)
						{
							rect.left = pos * size.cx;
							rect.right = rect.left + (UbForm.mask[ubpos].length * size.cx);
							rect.top = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
									recHeight * RowHeight 
									+ tm.tmHeight * h * RowHeight);
							rect.bottom = (int) ((double) rect.top +  tm.tmHeight * RowHeight * recHeight);
							FillRect (hdc, &rect, ColBkColor->GetHBrush ());
						}
					    int darkness = 0;
					    int blue = GetBValue (ColBkColor->GetColor ());
				        int red  = GetRValue (ColBkColor->GetColor ());
						int green = GetGValue (ColBkColor->GetColor ());
						if (blue <= 128)
						{
							darkness ++;
						}
						if (red <= 128)
						{
							darkness ++;
						}
						if (green <= 128)
						{
							darkness ++;
						}
						if (darkness > 1)
						{
							SetTextColor (hdc, RGB (255, 255, 255));
						}
						SetBkColor (hdc,ColBkColor->GetColor ());
				   }

				   if (zeile == AktRow - scrollpos && h == 0)
				   {
					    int darkness = 0;
					    int blue = GetBValue (ActBkColor);
				        int red  = GetRValue (ActBkColor);
						int green = GetGValue (ActBkColor);
						if (blue < 128)
						{
							darkness ++;
						}
						if (red < 128)
						{
							darkness ++; 
						}
						if (green < 128)
						{
							darkness ++;
						}
						if (darkness > 1)
						{
							SetTextColor (hdc, RGB (255, 255, 255));
						}


					    ubpos = GetUbPos (frm, i);
						pos = UbForm.mask[ubpos].pos[1];
						pos = pos - ldiff;
						if (pos >= 0)
						{
							rect.left = pos * size.cx;
							rect.right = rect.left + (UbForm.mask[ubpos].length * size.cx);

                            rect.top = y;
							rect.bottom = rect.top + tm.tmHeight;
//LAC-104 nicht mehr ganze Zeile							FillRect (hdc, &rect, ActColBkColor->GetHBrush ());
							BOOL ColBkSet = FALSE;
							if (RowEvent != NULL)
							{	 
								ColBkSet = RowEvent->FillColRect (hdc, &rect,frm->mask[i].rows,TRUE); //LAC-104
							}
						}

//LAC-104 nicht mehr ganze Zeile						SetBkColor (hdc,ActBkColor);
						BOOL BkSet = FALSE;
						if (RowEvent != NULL)
						{ 
								BkSet = RowEvent->SetRowBkColor (hdc,frm->mask[i].rows,TRUE); //LAC-104
						}

				   }
				   else if (ColBkColor == NULL)
				   {
						ubpos = GetUbPos (frm, i);
						pos = UbForm.mask[ubpos].pos[1];
						pos = pos - ldiff;
						if (pos >= 0)
						{
							rect.left = pos * size.cx;
							rect.right = rect.left + (UbForm.mask[ubpos].length * size.cx);

							rect.top = y;
							rect.bottom = rect.top + tm.tmHeight;
							BOOL ColBkSet = FALSE;
							if (RowEvent != NULL)
							{	 
								ColBkSet = RowEvent->FillColRect (hdc, &rect,frm->mask[i].rows,FALSE); //LAC-7
							}
							if (!ColBkSet)
							{
								FillRect (hdc, &rect, this->ColBkColor->GetHBrush ());
							}
						}
						BOOL BkSet = FALSE;
						if (RowEvent != NULL)
						{ 
								BkSet = RowEvent->SetRowBkColor (hdc,frm->mask[i].rows,FALSE); //LAC-7
						}
						if (!BkSet)
						{
							SetBkColor (hdc,BkColor);
						}
				   }


                   ToFormat (buffer, &frm->mask[i]);
                   TextOut (hdc, x, y, buffer, strlen (buffer));
            }
        }
		if (RowEvent != NULL)
		{ 
			RECT rect;
			rect.left  = 0;
       		GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &sizes);
			rect.right = x + size.cx + 1;
			rect.top   = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
						  recHeight * RowHeight 
						  + tm.tmHeight * h * RowHeight);
			rect.bottom   = (int) (double) ((double) UbHeight + (zeile + 1) * tm.tmHeight * 
						  recHeight * RowHeight 
						  + tm.tmHeight * h * RowHeight);

			RowEvent->AfterPaint (hdc, &rect, zeile + scrollpos, h);
		}
}

#endif


void ListClass::ShowSatzNr (HDC hdc, int zeile)
{
          RECT rect; 
          HBRUSH hBrush;
	    

          if (NoRecNr) return;

          rect.top = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
			  recHeight * RowHeight); 
		  rect.bottom = (int) (double) ((double) 
			  rect.top + tm.tmHeight * recHeight * RowHeight + 1); // + 1 zum Test 12.09.2000
		  rect.left = 0;
          rect.right = fSatzNr.mask[0].length * tm.tmAveCharWidth;

          hBrush = CreateSolidBrush (LTGRAYCOL);

          HBRUSH oldBrush = SelectObject (hdc, hBrush);
          SelectObject (hdc, GetStockObject (NULL_PEN));
          Rectangle (hdc,rect.left, rect.top, rect.right, rect.bottom);
          SelectObject (hdc, oldBrush);
          DeleteObject (hBrush); 
}

int ListClass::GetMidPos (int y)
/**
Mittelposition in Ratserzeile ermitteln.
**/
{
	     int h;

	     if (RowHeight <= 1) return y;

		 h = (int) (double) ((double) tm.tmHeight * RowHeight);
		 y = y + (h - tm.tmHeight) / 2;
		 return y;
}

void ListClass::ShowRowRect (COLORREF bcolor, int zeile)
/**
Zeile farbig hinterlegen.
**/
{
         int x, y, cx, cy;
         HBRUSH hBrush;
		 RECT rect;
		 HDC hdc;


         y = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
			  recHeight * RowHeight); 
         x = fSatzNr.mask[0].length * tm.tmAveCharWidth;
		 GetWindowRect (mamain3, &rect);
		 cx = rect.right - rect.left;
		 cy = (int) (double) ((double) tm.tmHeight * recHeight * RowHeight); 
         hBrush = CreateSolidBrush (bcolor);

         HBRUSH oldBrush = SelectObject (hdc, hBrush);
         SelectObject (hdc, GetStockObject (NULL_PEN));
         Rectangle (hdc,x, y, x + cx, y + cy);
         SelectObject (hdc, oldBrush);
         DeleteObject (hBrush); 
}
	

void ListClass::ShowFrmRow (HDC hdc, int zeile, int h)
/**
Zeile anzeigen.
**/
{
         int y;
         int x;

         y = (int) (double) ((double) UbHeight + zeile * tm.tmHeight * 
			  recHeight * RowHeight 
			 + tm.tmHeight * h * RowHeight);
		 y = GetMidPos (y);
          if (NoRecNr == 0)
         {
                  x = fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }
         else
         {
                  x = 0;
         }

         if (MustPaint (y))
         {
                 if (NoRecNr == 0)
                 {
                         SetTextColor (hdc,BLACKCOL);
                         SetBkColor (hdc,LTGRAYCOL);
                         TextOut (hdc, 0, y, SatzNr, strlen (SatzNr));
                 }

                 SetTextColor (hdc,Color);
                 SetBkColor (hdc,BkColor);
                 TextOut (hdc, x, y, frmrow, strlen (frmrow));
         }
}

#ifndef _NEWLST

void ListClass::ShowAktColumn (HDC hdc, int y, int i)
/**
Ein Maskenfeld anzeigen.
**/
{
        int x;
        char buffer [1024];
        int diff;

         diff = 0;
         if (LineForm.frmstart)
         {
			if (NoRecNr)
			{
               diff = 1 + LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
			else
			{
               diff = 1 + LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
			}
         }
         ToFormat (buffer, &DataForm.mask[i]);
         x = (DataForm.mask[i].pos[1] - diff) * tm.tmAveCharWidth;

         SetTextColor (hdc,Color);
         SetBkColor (hdc,BkColor);
         TextOut (hdc, x, y, buffer, strlen (buffer));
}

#else

void ListClass::ShowAktColumn (HDC hdc, int y, int i, int h)
/**
Ein Maskenfeld anzeigen.
**/
{
        int x;
        char buffer [1024];
        int diff;
        int pos;
        SIZE size;
        SIZE sizes;

        if (DataForm.mask[i].pos[0] != h) return;

   		GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        diff = 0;
        if (LineForm.frmstart)
        {
			if (NoRecNr)
			{
               diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
                      
			}
			else
			{
               diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
			}

        }

        if (NoRecNr); 
        else if ((DataForm.mask[i].pos[1] - diff) <
             fSatzNr.mask[0].length)
        {
            return;
        }

        ToFormat (buffer, &DataForm.mask[i]);
#ifndef _PFONT
        x = (DataForm.mask[i].pos[1] - diff) * tm.tmAveCharWidth;
#else

         if (strchr (DataForm.mask[i].picture, 'f') ||
                   (strchr (DataForm.mask[i].picture, 'd') &&
                        strchr (DataForm.mask[i].picture, 'y') == NULL))
         {
                        pos = DataForm.mask[i].pos[1];
                        clipped (buffer);
                   		GetTextExtentPoint32 (hdc, buffer, strlen (buffer), &sizes);
                        pos = (pos + DataForm.mask[i].length - 1) * tm.tmAveCharWidth;
                        pos -= sizes.cx; 
                        pos -= diff * tm.tmAveCharWidth;
                        x = pos;
         } 
         else

         {
                        x = (DataForm.mask[i].pos[1] - diff) * tm.tmAveCharWidth;
         }
#endif


         SetTextColor (hdc,Color);
		 if (y == AktRow)
		 {
			SetBkColor (hdc,ActBkColor);
		 }
		 else
		 {
			SetBkColor (hdc,BkColor);
		 }
	     memset (buffer, ' ', 80);
		 buffer [79] = 0;
         TextOut (hdc, x, y, buffer, strlen (buffer));
         ToFormat (buffer, &DataForm.mask[i]);
         TextOut (hdc, x, y, buffer, strlen (buffer));
}
#endif

void ListClass::ShowAktRow (void)
/**
Aktuelle Zeile anzeigen.
**/
{
	     RECT rect;
         HDC hdc;
         int y;
         int i;
         int z;
         int h;
		 BOOL scrolled = FALSE;

         if (RowMode) return;

          if (lPagelen < recanz && 
			  AktRow - scrollpos >= lPagelen)
          {
//              InvalidateRect (mamain3, &FocusRect, TRUE);
//	   	      UpdateWindow (mamain3);
//              ScrollDown ();
			  scrollpos ++;
			  scrolled = TRUE;
          }

         for (h = 0; h < recHeight; h ++)
         {
            z = AktRow - scrollpos;
            y = (int) (double) ((double) UbHeight + 
				z * tm.tmHeight * recHeight * RowHeight +
                h * tm.tmHeight * RowHeight);
 		    y = GetMidPos (y);
            hdc = GetDC (mamain3);
            for (i = UbForm.frmstart; i < DataForm.fieldanz; i ++)
            {
               ShowAktColumn (hdc, y, i, h);
            }
         }

		 if (RowEvent != NULL)
		 { 
			RECT rect;
			rect.left  = 0;
			rect.right = 400;
            rect.top = (int) (double) ((double) UbHeight + 
				z * tm.tmHeight * recHeight * RowHeight);

            rect.bottom = (int) (double) ((double) UbHeight + 
				(z + 1) * tm.tmHeight * recHeight * RowHeight);

			RowEvent->AfterPaint (hdc, &rect, AktRow, 0);
		 } 

         PrintHline (hdc, z);
         PrintVline (hdc, z);

         ReleaseDC (mamain3, hdc);
         SetFeldFocus0 (AktRow, AktColumn);
         GetClientRect (mamain3, &rect);
		 rect.bottom = UbHeight;
//         InvalidateRect (mamain3, &rect, FALSE);
		 rect.top = (int) ((double) y - 2 * tm.tmHeight * recHeight * RowHeight);
		 rect.bottom =  (int) ((double) y + recHeight * tm.tmHeight + 2 * tm.tmHeight * recHeight * RowHeight);
		 if (rect.top < 0) rect.top = 0;
		 if (scrolled)
		 {
                   InvalidateRect (mamain3, NULL, TRUE);
		 }
		 else
		 {
//		           InvalidateRect (mamain3, &rect, FALSE);
		           InvalidateRect (mamain3, NULL, FALSE);
		 }
		 UpdateWindow (mamain3);
}
          
void ListClass::ShowDataForms (HDC hdc)
/**
Seite anzeigen
**/
{
        int i, h;
        int pos;

        if (DataForm.mask == NULL) return;

        if (ListhFont)
        {
               SelectObject (hdc, ListhFont);
        }
        for (i = 0, pos = scrollpos; i < lPagelen + 1; i ++, pos ++)
        {
                  if (pos == recanz) break;
                  memcpy (ausgabesatz, SwSaetze [pos], zlen); 
                  sprintf (SatzNr, "%5ld ", pos + 1);

				  ShowSatzNr (hdc, i);
                  for (h = 0; h < recHeight;h ++)
                  {
                         if (h == 0)
                         {
                              sprintf (SatzNr, "%5ld ", pos + 1);
                         }
                         else
                         {
                              sprintf (SatzNr, "      ");
                         }
#ifndef _PFONT
                         FillFrmRow (frmrow, &DataForm, h);
                         ShowFrmRow (hdc, i, h);
#else

                         FillFrmRow (hdc, frmrow, &DataForm, i, h);
#endif
                  }
        }
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
}

void ListClass::ReadFocusText (void)
{
	if (FocusWindow != NULL)
	{
		GetFocusText ();
	}
}


void ListClass::GetFocusText (void)
/**
Focus-Text aus Fenster holen.
**/
{
	    char buffer [1024];
		int i;

        int len;
        char *feld;

		i = AktColumn;
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 

        GetWindowText (FocusWindow,
                       DataForm.mask [AktColumn].item->GetFeldPtr (),
                       DataForm.mask [AktColumn].length);

        if (UseZiel == 0 && RowMode)
        {
                 len = DataRow->mask[AktRow].length;
                 feld = DataRow->mask[AktRow].item->GetFeldPtr();
                 if (len > DataForm.mask[AktColumn].length)
                 {
                          len = DataForm.mask[AktColumn].length;
                 }
                 memcpy (feld,
                         DataForm.mask[AktColumn].item->GetFeldPtr (),
                         len);
                 len = DataRow->mask[AktRow].length;
                 feld[len] = (char) 0;
                 ToFormat (buffer, &DataRow->mask[AktRow]);
        }
        else
        {
                 ToFormat (buffer, &DataForm.mask[AktColumn]);
        }

		if (EditRenderer != NULL)
		{
			EditRenderer->GetWindowText (FocusWindow, AktRow, AktColumn, buffer, 1024);
		}

        strcpy (DataForm.mask[AktColumn].item->GetFeldPtr (), buffer);
        strcpy (AktValue, 
                DataForm.mask[AktColumn].item->GetFeldPtr ());
        memcpy (SwSaetze [AktRow], ausgabesatz, zlen); 
}
	 

void ListClass::ShowWindowText (int x, int y)
/**
Focus-Text anzeigen.
**/
{
	    char buffer [1024];
		char dest[1024];

		strcpy (dest, " "); 
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
        ToFormat (buffer, &DataForm.mask[AktColumn]);
        strcat (dest, buffer);

		if (listclipp)
		{
		      clipped (buffer);
		}
        else if (strchr (DataForm.mask[AktColumn].picture, 'f') ||
                   (strchr (DataForm.mask[AktColumn].picture, 'd') &&
                        strchr (DataForm.mask[AktColumn].picture, 'y') == NULL))
        {
              clipped (buffer);
        }
		if (!IsUserRenderer)
		{
			SetWindowText (FocusWindow, buffer);
			SetFocus (FocusWindow);
			if (setsel == FALSE)
			{
			  if (strcmp (buffer, " "))
			  {
					  SendMessage (FocusWindow, EM_SETSEL, (WPARAM) MAKELONG (-1, 0), 
												   (LPARAM) strlen (buffer));
			  }
			  return;
			}
			if (strlen (buffer))
			{
			  SendMessage (FocusWindow, EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
			}
		}
		else
		{
			EditRenderer->SetDefault (AktRow, AktColumn, buffer, 1024);
			SetWindowText (FocusWindow, buffer);
			SetFocus (FocusWindow);
		    SendMessage (FocusWindow, EM_SETSEL, (WPARAM) 0, MAKELONG (-1, 0));
		}
}

/*
void ListClass::ZielFormat (char *buffer, FELDER *zfeld)
{
        int len;

        len = strlen (DataForm.mask[AktColumn].item->GetFeldPtr ());
        if (len > zfeld->feldlen)
        {
            len = zfeld->feldlen;
        }
        memcpy (buffer,DataForm.mask[AktColumn].item->GetFeldPtr (), 
                len);
        buffer [zfeld->feldlen] = (char) 0;
}
*/

        

void ListClass::ShowFocusText (HDC hdc, int x, int y)
/**
Focus-Text anzeigen.
**/
{
	    char buffer [1024];
		char dest[1024];

		strcpy (dest, " "); 
        memcpy (ausgabesatz, SwSaetze [AktRow], zlen); 
        if (RowMode == 0)
        {
                  ToFormat (buffer, &DataForm.mask[AktColumn]);
        }
/*
        else
        {
                  ZielFormat (buffer, &LstZiel[AktRow]);
        }
*/
		strcat (dest, buffer);
        SetTextColor (hdc,Color);
        SetBkColor (hdc,BkColor);
        TextOut (hdc, x, y, dest, strlen (dest) - 1);
}



void ListClass::ScrollLeft (void)
/**
Ein Feld nach rechts scrollen.
**/
{

         RECT rect;

         GetClientRect (mamain3, &rect);
//         rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         if (NoRecNr == 0)
         {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }
         else
         {
                    rect.left = 0;
         }
		 rect.top += UbHeight;

         if (UbForm.frmstart > 0)
         {
                     UbForm.frmstart --;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
                     if (FocusWindow)
                     {
		                       DestroyFocusWindow ();
                     } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}

void ListClass::ScrollUbForm (void)
{
        int i;
        HWND Ctab [500];
        
        for (i = 0; i < UbForm.fieldanz; i ++)
        {
            Ctab[i] = UbForm.mask[i].feldid;
            UbForm.mask[i].feldid = NULL;
        }
        display_form (mamain3, &UbForm, 0, 0);

        for (i = 0; i < UbForm.fieldanz; i ++)
        {
            DestroyWindow (Ctab[i]);
        }

}


void ListClass::ScrollRight (void)
/**
Ein Feld nach links scrollen.
**/
{


         RECT rect;

         GetClientRect (mamain3, &rect);
//         rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         if (NoRecNr == 0)
         {
                    rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
         }
         else
         {
                    rect.left = 0;
         }
		 rect.top += UbHeight;

         if(UbForm.frmstart < banz - 1)
         {
                     UbForm.frmstart ++;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}

void ListClass::ScrollPageUp (void)
/**
Eine Seite nach links scrollen.
**/
{

         RECT rect;

         GetClientRect (mamain3, &rect);
		 if (NoRecNr == 0)
		 {
                     rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		 }
		 rect.top += UbHeight;

         UbForm.frmstart -= 5;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         if(UbForm.frmstart < 0) 
         {
                     UbForm.frmstart = 0;
         }
         SetDataStart ();

         CloseControls (&UbForm);
         display_form (mamain3, &UbForm, 0, 0);
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                       TRUE);
}


void ListClass::ScrollPageDown (void)
/**
Eine Seite nach links scrollen.
**/
{

         RECT rect;

         GetClientRect (mamain3, &rect);
		 if (NoRecNr == 0)
		 {
                     rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		 }
		 rect.top += UbHeight;

         UbForm.frmstart += 5;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         if(UbForm.frmstart > banz - 1) 
         {
                     UbForm.frmstart = banz - 1;
         }
         SetDataStart ();

         CloseControls (&UbForm);
         display_form (mamain3, &UbForm, 0, 0);
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                       TRUE);
}

void ListClass::SetPos (int pos)
/**
Position setzen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		 rect.top += UbHeight;
 
         if (pos >= 0 && pos <= banz - 1)
         {
                     UbForm.frmstart = pos;
                     SetDataStart ();
                     CloseControls (&UbForm);
                     display_form (mamain3, &UbForm, 0, 0);
                     InvalidateRect (mamain3, &rect, TRUE);
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
         }
}
                      
void ListClass::SetTop ()
/**
Position setzen.
**/
{
          
          RECT rect;

          GetClientRect (mamain3, &rect);
		  if (NoRecNr == 0)
		  {
                  rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;
		  }
 		  rect.top += UbHeight;

          UbForm.frmstart = 0;
          SetDataStart ();
          CloseControls (&UbForm);
          display_form (mamain3, &UbForm, 0, 0);
		  if (FocusWindow)
		  {
					 DestroyFocusWindow ();
		  }
          InvalidateRect (mamain3, &rect, TRUE);
          SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
}

                      
void ListClass::SetBottom ()
/**
Position setzen.
**/
{
           
          RECT rect;

          GetClientRect (mamain3, &rect);

          UbForm.frmstart = banz - 1;;
          SetDataStart ();
          CloseControls (&UbForm);
          display_form (mamain3, &UbForm, 0, 0);
    	  if (FocusWindow)
		  {
					 DestroyFocusWindow ();
		  }
          InvalidateRect (mamain3, &rect, TRUE);
          SetScrollPos (hTrack, SB_CTL, UbForm.frmstart,
                                   TRUE);
}
                      

void ListClass::HScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
         switch (LOWORD (wParam))
         {
             case SB_LINEDOWN :
             {    
                     ScrollRight ();
                     break;
             }
             case SB_LINEUP :
             {    
                     ScrollLeft ();
                     break;
             }
             case SB_PAGEDOWN :
             {    
                     ScrollPageDown ();
                     break;
             }
             case SB_PAGEUP :
             {    
                     ScrollPageUp ();
                     break;
             }
             case SB_THUMBPOSITION :
                     SetPos (HIWORD (wParam));
                     break;
             case SB_TOP :
                     SetTop ();
                     break;
             case SB_BOTTOM :
                     SetBottom ();
                     break;
         }
}


void ListClass::SetNewRow (int row)
/**
Position setzen.
**/
{
         RECT rect;

		 if (row < scrollpos)
		 {
			 SetVPos (row);
		 }
		 GetPageLen ();
		 if (row > (scrollpos + lPagelen) - 1)
		 {
			 SetVPos (row);
		 }
         GetClientRect (mamain3, &rect);
         rect.top = UbHeight;
		 SetPos (row, 0);
/*
         if (FocusWindow)
		 {
					           DestroyFocusWindow ();
		 } 
         InvalidateRect (mamain3, &rect, TRUE);
         SetScrollPos (vTrack, SB_CTL, scrollpos, TRUE);
*/
}
                      

void ListClass::SetVPos (int pos)
/**
Position setzen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.top = UbHeight;

         if (pos >= 0 && pos <= recanz - 1)
         {
                     scrollpos = pos;
  		             if (FocusWindow)
					 {
					           DestroyFocusWindow ();
					 } 
                     InvalidateRect (mamain3, &rect, TRUE);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}
                      
void ListClass::EditScroll (void)
/**
Focus nachh rechts setzen.
**/
{
          RECT rect;

          DestroyFocusWindow (); 
          GetClientRect (mamain3, &rect);
          rect.top += UbHeight;
          rect.left += fSatzNr.mask[0].length * tm.tmAveCharWidth;

          InvalidateRect (mamain3, &rect, TRUE);
}
  
void ListClass::ScrollWindow0 (HWND mamain3, int start, int height, RECT *rectv, RECT *rectb)
{
	     int i;
		 int plus;

		 if (height >= 0) 
		 {
			 plus = 1;
		 }
		 else
		 {
			 plus = -1;
			 height *= -1;
		 }

        
		 for (i = 0; i < height; i += 1)
		 {
                     ScrollWindow (mamain3, start, 
											plus,
                                            rectv, rectb);
//					 Sleep (1);
		 }
}
  

void ListClass::ScrollDown (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;
         RECT newrows;

         GetClientRect (mamain3, &rect);
         GetClientRect (mamain3, &newrows);
         rect.top = (int) (double) ((double) UbHeight + 1);;

         newrows.top = (int) (double) ((double) 
			 newrows.bottom - 3 * UbHeight * recHeight * RowHeight);
		 GetPageLen ();

         if(scrollpos < recanz - 1)
         {
                     scrollpos ++;
					 if (FocusWindow)
					 {
						 DestroyFocusWindow ();
					 }

                     ScrollWindow (mamain3, 0, 
                                            (int) (double) ((double) 
											-tm.tmHeight * recHeight 
											* RowHeight),
                                            &rect, &rect);


                     InvalidateRect (mamain3, &newrows, FALSE);
//                     InvalidateRect (mamain3, NULL, FALSE);
                     InvalidateRect (mamain3, &FocusRect, FALSE);
                     UpdateWindow (mamain3);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}
         
void ListClass::ScrollUp (void)
/**
Ein Feld nach unten scrollen.
**/
{

         RECT rect;
         RECT newrows;

         GetClientRect (mamain3, &rect);
         GetClientRect (mamain3, &newrows);

         rect.top = (int) (double) ((double) UbHeight + 1);
         newrows.top = UbHeight;
         if (recanz - scrollpos >= lPagelen)
         {
                  newrows.bottom = (int) (double) ((double) 
					  newrows.top + UbHeight * recHeight * RowHeight);
         }

         if(scrollpos > 0)
         {
                     scrollpos --;
					 if (FocusWindow)
					 {
						 DestroyFocusWindow ();
					 }

                     ScrollWindow (mamain3, 0, 
						 (int) (double) ((double) 
						 tm.tmHeight * recHeight * RowHeight),
                                            &rect, &rect);

                     InvalidateRect (mamain3, &newrows, FALSE);
                     InvalidateRect (mamain3, &FocusRect, FALSE);
                     UpdateWindow (mamain3);
                     SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
         }
}

void ListClass::ScrollVPageDown (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.top = tm.tmHeight;
		 int scrollalt;

		 scrollalt = scrollpos;
         scrollpos += lPagelen - 1;

         if(scrollpos > recanz - 1)
         {
                     scrollpos = recanz - 1;
         }
		 AktRow += scrollpos - scrollalt;
		 if (AktRow > recanz - 1) AktRow = recanz - 1;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         InvalidateRect (mamain3, &rect, TRUE);
         InvalidateRect (mamain3, &FocusRect, FALSE);
         UpdateWindow (mamain3);
         SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
}


void ListClass::MouseWheel (WPARAM wParam, LPARAM lParam)
{
         RECT rect;

         GetClientRect (mamain3, &rect);
         rect.top = tm.tmHeight;
		 int scrollalt;
		 short zdelta = (short) HIWORD(wParam);
		 scrollalt = scrollpos;
         scrollpos -= zdelta / 15;
         if(scrollpos > recanz - 1)
         {
                     scrollpos = recanz - 1;
         }
         if(scrollpos < 0)
         {
                     scrollpos = 0;
         }
		 AktRow += scrollpos - scrollalt;
		 if (AktRow > recanz - 1) AktRow = recanz - 1;

		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         InvalidateRect (mamain3, &rect, TRUE);
         InvalidateRect (mamain3, &FocusRect, FALSE);
         UpdateWindow (mamain3);
         SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
}


void ListClass::ScrollVPageUp (void)
/**
Ein Feld nach unten scrollen.
**/
{
         RECT rect;
		 int scrollalt;

		 scrollalt = scrollpos;
         GetClientRect (mamain3, &rect);
         rect.top = tm.tmHeight;

         scrollpos -= lPagelen - 1;

         if(scrollpos < 0)
         {
                     scrollpos = 0;
         }

		 AktRow += scrollpos - scrollalt;
		 if (AktRow < 0) AktRow = 0;
		 if (FocusWindow)
		 {
					 DestroyFocusWindow ();
		 }
         InvalidateRect (mamain3, &rect, TRUE);
         InvalidateRect (mamain3, &FocusRect, FALSE);
         UpdateWindow (mamain3);
         SetScrollPos (vTrack, SB_CTL, scrollpos,
                                   TRUE);
}


void ListClass::VScroll (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_VSCROLL erhalten.
**/
{
         switch (LOWORD (wParam))
         {
             case SB_LINEDOWN :
             {    
                     ScrollDown ();
                     break;
             }
             case SB_LINEUP :
             {    
                     ScrollUp ();
                     break;
             }

             case SB_PAGEDOWN :
             {    
                     ScrollVPageDown ();
                     break;
             }
             case SB_PAGEUP :
             {    
                     ScrollVPageUp ();
                     break;
             }
             case SB_THUMBPOSITION :
                     SetVPos (HIWORD (wParam));
                     break;
             case SB_TOP :
                     SetTop ();
                     break;
             case SB_BOTTOM :
                     SetBottom ();
                     break;
         }
}


int ListClass::TestAfterR (void)
/**
Funktion After fuer aktives Feld testen.
**/
{
         if (DataRow->mask[AktRow].after)
         {
             return (*DataRow->mask[AktRow].after) ();
         }
         return 0;
}

int ListClass::TestAfter (void)
/**
Funktion After fuer aktives Feld testen.
**/
{

	     posset = FALSE; 
         if (DataForm.mask[AktColumn].after)
         {
             return (*DataForm.mask[AktColumn].after) ();
         }
         return 0;
}

int ListClass::TestBeforeR (void)
/**
Funktion After fuer aktives Feld testen.
**/
{
         if (DataRow->mask[AktRow].before)
         {
             return (*DataRow->mask[AktRow].before) ();
         }
         return 0;
}

int ListClass::TestBefore (void)
/**
Funktion After fuer aktives Feld testen.
**/
{
	     posset = FALSE; 
         if (DataForm.mask[AktColumn].before)
         {
             return (*DataForm.mask[AktColumn].before) ();
         }
         return 0;
}

void ListClass::InsertLine (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
         int i;

         if (RowMode) return;

         InAppend = 1; 
         SetInsAttr ();
         SetPos (AktRow, FirstColumn ());

         InvalidateRect (mamain3, &FocusRect, TRUE);
         DestroyFocusWindow ();
         for (i = recanz; i > AktRow; i --)
         {
                   memcpy (SwSaetze[i], SwSaetze[i - 1], zlen);
         }

         recanz ++;
         SwRecs = recanz;
         InitForm (&DataForm);
         if (Lkeys != NULL)
         {
             Lkeys->InsertLine (AktRow);
         }
         memcpy (SwSaetze[i], ausgabesatz, zlen);
         GetPageLen ();
         MoveListWindow ();
		 DisplayList ();
/*
         InvalidateRect (mamain3, NULL, TRUE);
         UpdateWindow (mamain3);
*/
         NewRec = 1;
}


void ListClass::AppendLine (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
         int i;
		 RECT rect;

         if (RowMode) return;

         if (TestAppend)
         {
            if ((*TestAppend) () == 0)
            {
                return;
            }
         }

         InvalidateRect (mamain3, &FocusRect, TRUE);
         InAppend = 1; 
         NewRec = 1; 
         SetInsAttr ();
         SetPos (AktRow, FirstColumn ());

         DestroyFocusWindow ();
         AktRow ++;
         SelRow = AktRow;
         for (i = recanz; i > AktRow; i --)
         {
                   memcpy (SwSaetze[i], SwSaetze[i - 1], zlen);
         }

         recanz ++;
         SwRecs = recanz;
         InitForm (&DataForm);
         if (Lkeys != NULL)
         {
             Lkeys->AppendLine (AktRow);
         }
         memcpy (SwSaetze[i], ausgabesatz, zlen);
         GetPageLen ();
         MoveListWindow ();

		 GetClientRect (mamain3, &rect);
		 rect.top += UbHeight * tm.tmHeight;
		 DisplayList ();
/*
         InvalidateRect (mamain3, NULL, TRUE);
         UpdateWindow (mamain3);
*/
}

void ListClass::DeleteLine (void)
/**
Eine Zeile in Liste loeschen.
**/
{
         int i;
		 RECT rect;

         if (RowMode) return;

         if (recanz <= 1)
         {
	              InvalidateFocusFrame = TRUE;
                    InitForm (&DataForm);
                    memcpy (SwSaetze[0], ausgabesatz, zlen);
                    InAppend = 1;
                    NewRec = 1;
                    SetInsAttr ();
                    SetPos (AktRow, FirstColumn ());
                    ShowAktRow ();
                    SetFeldFocus0 (AktRow, AktColumn);
					InvalidateFocusFrame = FALSE;
                    return;
         }

         InAppend = 0; 
         NewRec = 0;
         SetInsAttr ();
         SetPos (AktRow, FirstColumn ());
         DestroyFocusWindow ();
         for (i = AktRow; i < recanz; i ++)
         {
                   memcpy (SwSaetze[i], SwSaetze[i + 1], zlen);
         }

         recanz --;
         if (AktRow > recanz) AktRow --;
         GetPageLen ();
         MoveListWindow ();
		 GetClientRect (mamain3, &rect);
		 rect.top += UbHeight;
//		 DisplayList ();

         InvalidateRect (mamain3, &rect, TRUE);
         UpdateWindow (mamain3);


         if (AktRow >= recanz && syskey != KEYUP)
         {
             AktRow --;
			 if (AktRow < scrollpos)
			 {
                    InvalidateRect (mamain3, &rect, TRUE);
                    ScrollUp (); 
			 }
             SetFeldFocus0 (AktRow, AktColumn); 
         }
	  	 InvalidateFocusFrame = FALSE;
         memcpy (ausgabesatz, SwSaetze[AktRow], zlen);
		 TestBeforeRow ();
}

void ListClass::FunkKeys (WPARAM wParam, LPARAM lParam)
/**
Meldung WM_HSCROLL erhalten.
**/
{
#ifndef NINFO
 		 char where [1024];
         char *Item;
#endif

		 if (mamain3 == NULL) return;

		 if (FocusWindow)
		 {
			 GetFocusText ();
		 }

         switch (LOWORD (wParam))
         {
             case VK_F2 :
                     syskey = KEY2; //LAC-56
                     testkeys (); //LAC-56
					 break; 
             case VK_F3 :
                     syskey = KEY3;
                     testkeys ();
					 break;
             case VK_F4 :
#ifndef NINFO
                    if (InfoProc)
                    {
                         Item = GetAktItem ();
                         (*InfoProc) (&Item, GetAktValue (), where);
                    }
                    else
                    {
                         Item = GetAktItem ();
  		                 sprintf (where, "where %s = %s",
							      Item,GetAktValue ()); 
                    }
                    DisableWindows (mamain2);
				    _CallInfoEx (hMainWindow, NULL,
							     Item, 
								 where, 0l);
                    EnableWindows (mamain2);
#endif
					break;
             case VK_F5 :
                     syskey = KEY5;
                     testkeys ();
					 break;
             case VK_F6 :
                     syskey = KEY6;
                     testkeys ();
					 break;
             case VK_F7 :
                     syskey = KEY7;
                     testkeys ();
					 break;
             case VK_F8 :
                     syskey = KEY8;
                     testkeys ();
					 break;
             case VK_F9 :
                     syskey = KEY9;
                     testkeys ();
					 break;
             case VK_F10 :
                      syskey = KEY10; 
                      testkeys ();
                      break;
             case VK_F11 :
                      syskey = KEY11; 
                      testkeys ();
                      break;
             case VK_F12 :
                      syskey = KEY12; 
                      testkeys ();
                      break;
             case VK_RETURN :
                     syskey = KEYCR;
				     if (ListFocus == 0)
					 {
                            ScrollRight ();
					 }
                     else if (RowMode)
                     {
                            TestAfterR ();
                            FocusDown (); 
                            TestBeforeR ();
                     }
					 else
					 {
						   int ret = 0;
						   int aCol = AktColumn; 
 						   int col = NextColumn ();
					  	   AktColumn = aCol;
						   if (col > aCol && FokusDownbeiKEYCR == FALSE)  //LAC-188  bei FokusDownbeiKEYCR nicht, da TestAfter sonst doppelt ausgef�hrt wird
						   {
								 ret = TestAfter ();
						   }
                           if (ret == 0 && posset == FALSE)
                           {
				                     FocusRight ();
                            }
                            else
                            {
                          	         SetFeldFocus0 (AktRow, AktColumn);
                            }
/*
                            TestBefore ();
							if (posset)
							{
                          	         SetFeldFocus0 (AktRow, AktColumn);
							}
*/

					 }
                     break;
             case VK_RIGHT :
                     syskey = KEYRIGHT;
				     if (ListFocus == 0)
					 {
                            ScrollRight ();
					 }
					 else
					 {
				            FocusRight ();
					 }
                     break;
             case VK_LEFT :
                     syskey = KEYLEFT;
				     if (ListFocus == 0)
					 {
                            ScrollLeft ();
					 }
					 else
					 {
				            FocusLeft ();
					 }
                     break;
             case VK_DOWN :
                     syskey = KEYDOWN;
				     if (ListFocus == 0)
					 {
                            ScrollDown ();
					 }
					 else
					 {
				            FocusDown ();
					 }
                     break;
             case VK_UP :
                     syskey = KEYUP;
				     if (ListFocus == 0)
					 {
                            ScrollUp ();
					 }
					 else
					 {
				            FocusUp ();
					 }
                     break;
             case VK_NEXT :
                     syskey = KEYPGD;
                     ScrollVPageDown ();
                     break;
             case VK_PRIOR :
                     syskey = KEYPGU;
                     ScrollVPageUp ();
                     break;
             case VK_HOME :
                     SetTop ();
                     break;
             case VK_END :
                     SetBottom ();
                     break;
             case VK_TAB :
				     syskey = KEYTAB;
                     if (GetKeyState (VK_SHIFT) < 0)
                     {
                              syskey = KEYLEFT; 
						      FocusLeft ();
                     }
					 else if (syskey == KEYSTAB)
                     {
                              syskey = KEYLEFT; 
						      FocusLeft ();
                     }
					 else
					 {
                            if (TestAfter () == 0 && posset == FALSE)
                            {
                                  syskey = KEYRIGHT; 
 			                      FocusRight ();
                            }
                            else
                            {
                                  syskey = KEYRIGHT; 
                          	      SetFeldFocus0 (AktRow, AktColumn);
                            }
					 }
					 break;


             default :
                     if (GetKeyState (VK_MENU) < 0)
                     {
                         testmenu ((int) wParam);
                     }
                     break; 
         }
         TestBefore ();
 		 if (posset)
		 {
           	         SetFeldFocus0 (AktRow, AktColumn);
		 }
}

void ListClass::FillUbForm (field *feld, char *feldname, int len, int spalte,
                 int BuId  )
/**
Maskenfeld fuer Ueberschruft fuellen.
**/
{
        memcpy (feld, &iButton, sizeof (field)); 
        clipped (feldname);
        feld->item       = new ITEM ("", feldname, "", 0);
        feld->length     = len;
        feld->pos[0]     = 0;
        feld->pos[1]     = spalte;
        feld->BuId       = BuId;
}


void ListClass::FreeDlgSaetze (void)
{
      int i;

      for (i = 0; i < DlgAnz; i ++)
      {
          if (DlgSaetze[i])
          {
              free (DlgSaetze[i]);
          }
      }
      DlgAnz = 0;
}
       
void ListClass::FromDlgRec (char *rec)
/**
Satz f�r Seitenansicht �bertragen.
**/
{

       return;
}

void ListClass::ToDlgRec (char *rec)
/**
Satz f�r Seitenansicht �bertragen.
**/
{

       return;
}

void ListClass::FromDlgRec0 (char *rec)
/**
Satz f�r Seitenansicht �bertragen.
**/
{
       int i;
       int len, len1;
       char *adr;

       memcpy (ausgabesatz, rec, zlen);
       for (i = 0; i < banz; i ++)
       {
           adr = DataRow->mask[i].item->GetFeldPtr ();
           len1 = DataRow->mask[i].length;
           len = strlen (adr);
           if (len > len1)
           {
               len = len1;
           }
           memcpy (adr, dlgstab[i].wert, len);
           adr [len] = (char) 0;
       }
       memcpy (rec, ausgabesatz, zlen);
}


void ListClass::ToDlgRec0 (char *rec)
/**
Satz f�r Seitenansicht �bertragen.
**/
{
       int i;
       int len;
//       char *adr;
//       char ibez [21];
//       char *bez;
       char *name;

       memcpy (ausgabesatz, rec, zlen);
       for (i = 0; i < banz; i ++)
       {
           DlgSaetze [i] = (char *) &dlgstab[i];

           name = UbForm.mask[i].item->GetFeldPtr ();
        
           len = strlen (name);
           if (len > 19) len = 19;

           strcpy (dlgstab[i].bez, name);

           name = DataForm.mask[i].item->GetFeldPtr ();
        
           len = strlen (name);
           if (len > MAXLEN) len = MAXLEN;

           strcpy (dlgstab[i].wert, name);
       }
       DlgAnz = i;
}


void ListClass::SetPage0 (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       if (PageView)
       {
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                banzS = banz;
                zlenS = zlen;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec0 (SwSaetze [pos]);
                AktRow = FirstColumnR ();
                SetPos (AktRow, 1);
                SelRow = 0;
                ausgabesatz = DlgSatz;
                zlen = sizeof (struct DLGS);
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);
                SetSaetze (DlgSaetze);
                memcpy (&UbForm, &UbDlg, sizeof (form));
                memcpy (&DataForm, &DataDlg, sizeof (form));
       }
       else
       {
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec0 (SwSaetze [pos]);
                memcpy (&UbForm, UbRow, sizeof (form));
                memcpy (&DataForm, DataRow, sizeof (form));
       }
}

void ListClass::SetPage (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       if (PageView)
       {
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                banzS = banz;
                zlenS = zlen;
//                LstZiel = ziel;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec (SwSaetze [pos]);
                SetPos (0, 1);
                SelRow = 0;
//                ziel = DlgZiel;
                ausgabesatz = DlgSatz;
                zlen = Dlgzlen;
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);
                SetSaetze (DlgSaetze);
       }
       else
       {
//                ziel = LstZiel;
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec (SwSaetze [pos]);
       }
}

void ListClass::SwitchPage (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       DestroyTrack ();
       DestroyVTrack ();
	   DestroyFocusWindow ();
       FreeUbForm ();
       FreeDataForm ();
       FreeLineForm ();
       if (PageView)
       {
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                AktPage     = AktRow;
                scrollposS  = scrollpos;
                banzS = banz;
                zlenS = zlen;
//                LstZiel = ziel;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec (SwSaetze [pos]);
                SetPos (0, 1);
                SelRow = 0;
//                ziel = DlgZiel;
                ausgabesatz = DlgSatz;
                zlen = Dlgzlen;
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);
                this->MakeUbForm ();
                SetSaetze (DlgSaetze);
                Initscrollpos ();
                MakeDataForm0 ();
       }
       else
       {
//                ziel = LstZiel;
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                this->MakeUbForm ();
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec (SwSaetze [pos]);
                Initscrollpos ();
                MakeDataForm0 ();
                Setscrollpos (scrollposS);
       }
}

void ListClass::NextPageRow (void)
/**
Naechsten Satz im PageView-Mode anzeigen.
**/
{
       if (PageView == 0) return; 
       DestroyFocusWindow ();
       if (UseZiel)
       {
           SetPage (AktRowS);
       }
       else
       {
           SetPage0 (AktRowS);
       }
       if (AktRowS < SwRecs - 1)
       {
           AktRowS = AktRowS + 1;
           AktRow  = AktRowS;
       }
       if (UseZiel)
       {
           SetPage (AktRowS);
       }
       else
       {
           SetPage0 (AktRowS);
       }
       SendMessage (mamain2, WM_SIZE, NULL, NULL);
       InvalidateRect (mamain3, 0, TRUE);
}

void ListClass::PriorPageRow (void)
/**
Naechsten Satz im PageView-Mode anzeigen.
**/
{
       if (PageView == 0) return; 
       DestroyFocusWindow ();
       if (UseZiel)
       {
           SetPage (AktRowS);
       }
       else
       {
           SetPage0 (AktRowS);
       }
       if (AktRowS > 0)
       {
           AktRow = AktRowS - 1;
           AktRowS = AktRow;
       }
       if (UseZiel)
       {
           SetPage (AktRowS);
       }
       else
       {
           SetPage0 (AktRowS);
       }
       SendMessage (mamain2, WM_SIZE, NULL, NULL);
       InvalidateRect (mamain3, 0, TRUE);
}

void ListClass::FillDataForm (field *feld, char *feldname, int len, int spalte)
/**
Maskenfeld fuer Daten fuellen.
**/
{
        return;
}

void ListClass::FillLineForm (field *feld, int spalte)
/**
Maskenfeld fuer Daten fuellen.
**/
{
        memcpy (feld, &iEdit, sizeof (field)); 
        feld->pos[1]     = spalte;
}

void ListClass::GetPageLen (void)
/**
Seitenlaenge ermitteln.
**/
{
        RECT rect;
        int y;

        GetClientRect (mamain2, &rect);
        y = rect.bottom - 16;
        lPagelen = (int) ((double) (y - UbHeight) / tm.tmHeight); 
        lPagelen = (int) (double) ((double) lPagelen / (recHeight * RowHeight));
        if (lPagelen > recanz)
        {
                     lPagelen = recanz;
        }
}

void ListClass::MakeLineForm (form *frm)
/**
Form fuer daten generieren.
**/
{
        
        return;
}

form * ListClass::GetDataForm ()
{
	    return &DataForm;
}


void ListClass::SetDataForm (form *frm)
/**
Form fuer Daten setzen.
**/
{
        int i;
        memcpy (&DataForm, frm, sizeof (form));
        if (DataForm.fieldanz > MAXFIELD)
        {
                  DataForm.fieldanz = MAXFIELD;
        }

        for (i = 0; i < DataForm.fieldanz; i ++)
        {
            memcpy (&_DataForm[i], &DataForm.mask[i], sizeof (field));
        }
        DataForm.mask = _DataForm;
        DataForm.font = &ListFont;
}

void ListClass::SetLineForm (form *frm)
/**
Form fuer Daten setzen.
**/
{
        int i;

        memcpy (&LineForm, frm, sizeof (form));
        if (LineForm.fieldanz > MAXFIELD)
        {
                  LineForm.fieldanz = MAXFIELD;
        }
        for (i = 0; i < LineForm.fieldanz; i ++)
        {
            memcpy (&_LineForm[i], &LineForm.mask[i], sizeof (field));
        }
        LineForm.mask = _LineForm;
        LineForm.font = &ListFont;
}

void ListClass::SetUbForm (form *frm)
/**
Form fuer Daten setzen.
**/
{
        int i, j;

        memcpy (&UbForm, frm, sizeof (form));
        if (UbForm.fieldanz > MAXFIELD)
        {
                  UbForm.fieldanz = MAXFIELD;
        }
        for (i = 0, j = 0; i < UbForm.fieldanz;i ++)
        {
            if (UbForm.mask[i].pos[0] == 0)
            {
                  memcpy (&_UbForm[j], &UbForm.mask[i], sizeof (field));
                  j ++;
            }
        }
        UbForm.fieldanz = j;
        UbForm.mask = _UbForm;
        fUbSatzNr.font = &UbSFont;
//        CloseControls (&fUbSatzNr);
        fSatzNr.font   = &SFont;
        UbForm.font    = &UbFont;
        if (UbForm.mask[0].rows == 0)
        {
            fUbSatzNr.mask[0].rows = 0;
            UbHeight = tm.tmHeight + tm.tmHeight / 3; 
        }
        else 
        {
            fUbSatzNr.mask[0].rows = UbForm.mask[0].rows;
            UbHeight = tm.tmHeight * UbForm.mask[0].rows; 
        }
		if (NoRecNr == FALSE)
		{
                 display_form (mamain3, &fUbSatzNr, 0, 0);
		}
        display_form (mamain3, &UbForm, 0, 0);
        if (TrackNeeded ())
        {
                 CreateTrack ();
        }
        UseZiel = 1;
}


void ListClass::MakeDataForm (form *frm)
/**
Form fuer daten generieren.
**/
{
        
        return;
}

void ListClass::DestroyRmFields (void)
/**
Removefelder aus Maske loeschen.
**/
{
        int i;

        return;
        for (i = 0; i < DataForm.fieldanz;)
        {
            if (DataForm.mask[i].attribut == REMOVED)
            {
                    DestroyField (i);
            }
            else
            {
                    i ++;
            }
        }
}


void ListClass::SetDataForm0 (form *dform, form *lform)
/**
Forms f�r Daten generieren.
**/
{

        GetPageLen ();
        SetDataForm (dform);
        SetLineForm (lform);
        UseZiel = 0;
        DestroyRmFields ();
}



void ListClass::MakeDataForm0 (void)
/**
Forms f�r Daten generieren.
**/
{

        GetPageLen ();
        MakeDataForm (&DataForm);
        MakeLineForm (&LineForm);
        UseZiel = 1;
}


void ListClass::MakeUbForm (void)
/**
Form fuer Ueberschrift generieren.
**/
{

        return;
}



void ListClass::FreeDataForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&DataForm);
        if (DataForm.mask)
        {   
             for (i = 0; i < banz; i ++)
             {
                  if (DataForm.mask[i].item)
                  {
                            delete DataForm.mask[i].item;                 
                            DataForm.mask[i].item = NULL;
                  }
             }
             GlobalFree (DataForm.mask);
        }
        memcpy (&DataForm, &IForm, sizeof (form));
}

void ListClass::FreeLineForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&LineForm);
        if (LineForm.mask)
        {
               for (i = 0; i < banz; i ++)
               {
                  if (LineForm.mask[i].item)
                  {
                            delete LineForm.mask[i].item;                 
                            LineForm.mask[i].item = NULL;
                  }
               }
               GlobalFree (LineForm.mask);
        }
        memcpy (&LineForm, &IForm, sizeof (form));
}

void ListClass::FreeUbForm (void)
/**
Item fuer UbForm freigeben.
**/
{
        int i;

        CloseControls (&UbForm);
        if (UbForm.mask)
        {
               for (i = 0; i < banz; i ++)
               {
                  if (UbForm.mask[i].item)
                  {
                            delete UbForm.mask[i].item;                 
                            UbForm.mask[i].item = NULL;
                  }
               }
               GlobalFree (UbForm.mask);
               
        }
        memcpy (&UbForm, &IForm, sizeof (form));
}

int ListClass::IsUbRow (MSG *msg)
/**
Test, ob der Mousecursor ueber der Feldueberschrift steht.
**/
{
        POINT mousepos;
        RECT rect;
        int i;

        if (UbForm.mask == NULL) return FALSE;


        for (i = 0; i < UbForm.fieldanz; i ++)
        {
            if (msg->hwnd == mamain3)
            {
                break;
            }
            else if (msg->hwnd == UbForm.mask[i].feldid)
            {
                break;
            }
        }
        if (i == UbForm.fieldanz) return FALSE;

//        return TRUE;


        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);

        if (mousepos.y < 0 ||
//            mousepos.y > tm.tmHeight)
            mousepos.y > UbHeight)
        {
                   return FALSE;
        }
        if (mousepos.x > rect.left &&
            mousepos.x < rect.right)
        {
                   return TRUE;
        }
        return FALSE;
}


int ListClass::IsUbEnd (MSG *msg)
/**
Test, ob der Mousecursor ueber dem Ende einer Feldueberschrift steht.
**/
{
        POINT mousepos;
        RECT rect;
        int i;
        int diff;
        int x;

        if (LineForm.mask == NULL) return FALSE;

        for (i = 0; i < UbForm.fieldanz; i ++)
        {

            if (msg->hwnd == mamain3)
            {
                break;
            }
            else if (msg->hwnd == UbForm.mask[i].feldid)
            {
                break;
            }
        }
        if (i == UbForm.fieldanz) return FALSE;

        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);

        diff = 0;
        if (LineForm.frmstart)
        {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
        }
        if (mousepos.y < 0 ||
//            mousepos.y > tm.tmHeight)
            mousepos.y > UbHeight)
        {
                   return FALSE;
        }

        if (mousepos.x <= rect.left ||
            mousepos.x >= rect.right)
        {
                   return FALSE;
        }

        for (i = UbForm.frmstart; i < UbForm.fieldanz; i ++)
        {
                   x = (UbForm.mask[i].pos[1] - diff) * tm.tmAveCharWidth;
                   if ((x - mousepos.x) < 4 && 
                       (x - mousepos.x) >= 0) 
                   {
                       if (i == 0)
                       {
                            continue;
                       }
                       else
                       {
                            movfield = i - 1;
                            return TRUE;
                       }
                   }
                   else if ((x - mousepos.x) < 0 && 
                            (x - mousepos.x) > -4) 
                   {
                            movfield = i - 1;
                            return TRUE;
                   }
        }
        x = (UbForm.mask[i - 1].pos[1] + UbForm.mask[i - 1].length 
             - diff) * tm.tmAveCharWidth;
        if ((x - mousepos.x) < 4 && 
            (x - mousepos.x) >= 0) 
        {
                    movfield = i - 1;
                    return TRUE;
        }
        else if ((x - mousepos.x) < 0 && 
                 (x - mousepos.x) > -4) 
        {
                    movfield = i - 1;
                    return TRUE;
        }
        return FALSE;
}


void ListClass::ShowArrow (BOOL mode)
/**
Mousecursor als anzeigen.
**/
{

        if (mode == FALSE && IsArrow == FALSE)
        {
            return;
        }

        if (mode)
        {
                IsArrow = 1;
                if (arrow == 0)
                {
//                            arrow =  LoadCursor (NULL, IDC_SIZEWE);
                            arrow =  LoadCursor (hMainInst, 
                                                 "oarrow");
                }
                oldcursor = SetCursor (arrow);
                aktcursor = arrow;
        }
        else
        {
                IsArrow = 0;
                SetCursor (oldcursor);
                aktcursor = oldcursor;
        }
}

HWND mLine = 0;


void ListClass::StartMove (void)
{
        RECT rect;
        POINT mousepos;

        
		if (FocusWindow)
		{
			DestroyFocusWindow ();
		}
        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        InMove = 1; 
        GetClientRect (mamain3, &rect);
        mLine = CreateWindow ("static",
                                 "",
                                WS_CHILD | 
                                WS_VISIBLE |
                                SS_BLACKRECT, 
                                mousepos.x, 0,
                                2, rect.bottom,
                                mamain3,
                                NULL,
                                hMainInst,
                                NULL);
}

void ListClass::MoveLine (void)
/**
Line bewegen.
**/
{

        RECT rect;
        POINT mousepos;
       
        if (mLine == NULL) return;

        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);
        MoveWindow (mLine, mousepos.x, 0, 
                           2, rect.bottom, TRUE);
		UpdateWindow (mamain3);
}

int ListClass::GetFieldPos (char*fieldname)
/**
Position fuer Feld holen.
**/
{
        int i;

        clipped (fieldname);
        for (i = 0; i < DataForm.fieldanz; i ++)
        {
            if (strcmp (fieldname,
                        DataForm.mask[i].item->GetItemName ()) == 0)
            {
                return i;
            }
        }
        return -1;
}


void ListClass::DestroyField (int movfield)
/**
Ein Feld aus der Maske loeschen.
**/
{
        int i;
        int upos;
        int dpos;
        RECT rect;

		return;
        if (recHeight > 1) return;
        if (UbForm.fieldanz <= 1) return;
		if (NoMove) return;
		if (FieldDestroy == FALSE) return;

        CloseControls (&UbForm);
        GetClientRect (mamain3, &rect);
        rect.top += tm.tmHeight;

        if (UseZiel)
        {
          if (movfield > 0 && UbForm.mask[movfield].item)
          {
                  delete UbForm.mask[movfield].item;
                  UbForm.mask[movfield].item = NULL;
          }
          if (movfield > 0 && DataForm.mask[movfield].item)
          {
                  delete DataForm.mask[movfield].item;
                  DataForm.mask[movfield].item = NULL;
          }


          if (movfield > 1)
          {
                  if (LineForm.mask[movfield - 1].item)
                  {
                           delete LineForm.mask[movfield - 1].item;
                           LineForm.mask[movfield - 1].item = NULL;
                  }
          }
        }


        upos = UbForm.mask[movfield].pos[1];
        dpos = DataForm.mask[movfield].pos[1];
        for (i = movfield; i < UbForm.fieldanz - 1; i ++)
        {
                   if (DataForm.mask[i].pos[0] != FrmRow) continue;
                   memcpy (&UbForm.mask[i], &UbForm.mask[i + 1],
                           sizeof (field));
                   memcpy (&DataForm.mask[i], &DataForm.mask[i + 1],
                           sizeof (field));
                   if (i > 0)
                   {
                       memcpy (&LineForm.mask[i - 1], &LineForm.mask[i],
                               sizeof (field));
                   }

                   UbForm.mask[i].pos[1]   = upos;
                   DataForm.mask[i].pos[1] = dpos;

/*
                   if (i > 0)
                   {
                         LineForm.mask[i - 1].pos[1] = pos;
                   }
*/
                   LineForm.mask[i - 1].pos[1] = upos;

                   upos += UbForm.mask[i].length;
                   dpos += DataForm.mask[i].length;
        }
        LineForm.mask[i - 1].pos[1] = upos;

        if (movfield < UbForm.fieldanz)
        {
                   UbForm.fieldanz --;
        }
        if (movfield < DataForm.fieldanz)
        {
                   DataForm.fieldanz --;
        }
        if (movfield < LineForm.fieldanz)
        {
                   LineForm.fieldanz --;
        }
        if (movfield < banz)
        {
                   banz --;
        }

        if (TrackNeeded ())
        {
           CreateTrack ();
           SetScrollRange (hTrack, SB_CTL, 0,
                                   banz - 1, TRUE);
        }
        else
        {
           DestroyTrack ();
        }
        if (hTrack == NULL) return;

        GetClientRect (mamain2, &rect);
        display_form (mamain3, &UbForm, 0, 0);
        InvalidateRect (mamain3, &rect, TRUE);
}

void ListClass::StopMove (void)
{
        POINT mousepos;
        int x;
        int lenu;
        int len;
        int diff;
        RECT rect;
        int i;
		int datapos;
        static int diff0;
        static int movfield0;



		if (NoMove) return;

        GetCursorPos (&mousepos);
        ScreenToClient (mamain3, &mousepos);
        GetClientRect (mamain3, &rect);
        rect.top += tm.tmHeight;

        diff = 0;
        if (LineForm.frmstart)
        {
            diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                   fSatzNr.mask[0].length;
        }

        if (movfield < UbForm.fieldanz - 1)
        {
                 x = (UbForm.mask[movfield + 1].pos[1] - diff) * 
                                 tm.tmAveCharWidth;
        }
        else
        {
                 x = (UbForm.mask[movfield].pos[1] + 
                       UbForm.mask[movfield].length - diff) *
                                 tm.tmAveCharWidth;
        }

        if ((x - mousepos.x) < 4 && 
            (x - mousepos.x) > -4) 
        {
                  return;
        }

        x = mousepos.x / tm.tmAveCharWidth + diff;
		for (i = movfield; i < DataForm.fieldanz; i ++)
		{
		    datapos = DataForm.mask[i].pos[1];
			if (datapos >= UbForm.mask[movfield].pos[1]) break;
		}

        if (movfield < UbForm.fieldanz - 1)
        {
                 diff = x - UbForm.mask[movfield + 1].pos[1]; 
        }
        else
        {
                 diff = x - (UbForm.mask[movfield].pos[1] + 
                               UbForm.mask[movfield].length); 
        }

		if (i == DataForm.fieldanz)
		{
	        datapos = DataForm.mask[i - 1].pos[1];
            len  = DataForm.mask[i - 1].length + diff;
		}
		else
		{
            len  = DataForm.mask[i].length + diff;
		}


        if (FrmRow == 0)
        {
            diff0 = diff;
            movfield0 = movfield;
            lenu = UbForm.mask[movfield].length + diff;
        }
        else
        {
            diff = diff0;
            lenu = UbForm.mask[movfield0].length;
        }


        if (len <= 0)
        {
            if (PageView) return;
    		if (FocusWindow)
			{
			     DestroyFocusWindow ();
			}
            DestroyField (movfield);
            return;
        }


        if (movfield < UbForm.fieldanz - 1)
        {
             if (UbForm.mask[movfield + 1].pos[0] == FrmRow)
             {
                   if (FrmRow == 0)
                   {
                         UbForm.mask[movfield + 1].pos[1] = x;
                         LineForm.mask[movfield].pos[1] = x;
                   }
             }

             for ( i = movfield + 2; i < UbForm.fieldanz; i ++)
             {
                  if (UbForm.mask[i].pos[0] != FrmRow) continue;
                  if (FrmRow == 0)
                  {
                         UbForm.mask[i].pos[1]   += diff;
                         LineForm.mask[i - 1].pos[1] += diff;
                  }
             }
        }

		for (i = 0; i < DataForm.fieldanz; i ++)
		{

			   if (DataForm.mask[i].pos[1] == datapos)
			   {
                         DataForm.mask[i].length = len;
			   }

			   if (DataForm.mask[i].pos[1] > datapos)
			   {
                  DataForm.mask[i].pos[1] += diff;
			   }
		}
/*
        if (FrmRow == 0)
        {
               LineForm.mask[LineForm.fieldanz - 1].pos[1] += diff;
               UbForm.mask[UbForm.fieldanz - 1].pos[1] += diff;
        }
*/
        if (FrmRow == 0)
        {
                UbForm.mask[movfield].length = lenu;
        }
//        DataForm.mask[movfield].length = len;

		if (FocusWindow)
		{
			DestroyFocusWindow ();
		}

        CloseControls (&UbForm);
        display_form (mamain3, &UbForm, 0, 0);
        InvalidateRect (mamain3, NULL, TRUE);
        UpdateWindow (mamain3);
}

void ListClass::TestNextLine (void)
{
        int i;

		return;
        for (i = 0 ; i < DataForm.fieldanz; i ++)
        {
            if (DataForm.mask[i].pos[0] != FrmRow) continue;
            if (DataForm.mask[i].pos[1] == 
                DataForm.mask[movfield].pos[1])
            {
                movfield = i;
                StopMove ();
                return;
            }
        }
}


void ListClass::StopMove0 (void)
{
        int h;

        InMove = 0;


        if (mLine)
        {
            DestroyWindow (mLine);
            mLine = NULL;
        }

        for (h = 0; h < recHeight; h ++)
        {
            FrmRow = h;
            if (h == 0) 
            {
                StopMove ();
            }
            else
            {
//                TestNextLine ();
            }
        }
}


void ListClass::DisplayList (void)
/**
Liste anzeigen.
**/
{
        HDC hdc;
        
        paintallways = 1;
        hdc = GetDC (mamain3);
        ShowDataForms (hdc);
        PrintHlines (hdc);
        PrintVlines (hdc);
	    if (! FocusWindow)
        {
          	     SetFeldFocus (hdc, AktRow, AktColumn);
		}
        else
        {
			     if (!IsUserRenderer)
				 {
					SetFocusFrame (hdc, AktRow, AktColumn );
				 }
                 PrintSlinesSatzNr (hdc);
        }
        ReleaseDC (mamain3, hdc);
        paintallways = 0;
}

BOOL ListClass::IsMouseMessage (MSG *msg)
/**
Mouse-Meldungen pruefen.
**/
{
    /*
        if (IsComboMessage && (*IsComboMessage) (msg))
        {
              return FALSE;
        }

        if (opencombobox)
        {
              return FALSE;
        }
    */

        switch (msg->message)
        {
        
              case WM_MOUSEWHEEL :
				      MouseWheel (msg->wParam, msg->lParam);
				      return FALSE;
              case WM_MOUSEMOVE :
                      if (InMove)
                      {
                            SetCursor (aktcursor);
                            MoveLine ();
                            return TRUE;
                      }
                      if (IsUbEnd (msg))
                      {
                              ShowArrow (TRUE);
                      }
                      else
                      {
                              ShowArrow (FALSE);
                      }
                      return FALSE;
              case WM_LBUTTONDOWN :
                      if (IsUbRow (msg) == FALSE)
                      {
                              return IsInListArea (msg);
                      }
                      if (aktcursor == arrow)
                      {
                                SetCursor (aktcursor);
                                StartMove ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      return TRUE;
              case WM_LBUTTONDBLCLK :
                      if (IsUbRow (msg) == FALSE)
                      {
                              return FALSE;
                      }
                      if (aktcursor == arrow)
                      {
                                SetCursor (aktcursor);
                                StartMove ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      return TRUE;
              case WM_LBUTTONUP :
                      if (InMove)
                      {
                                StopMove0 ();
                                return TRUE;
                      }
                      if (aktcursor)
                      {
                                SetCursor (aktcursor);
                      }
                      if (IsUbRow (msg))
                      {
                              return TRUE;
                      }
                      else
                      {
                              return FALSE;
                      }
        }
        return FALSE;
}
void ListClass::PaintUb (void)
{
	     CloseControls (&UbForm);
		 display_form (mamain3, &UbForm, 0, 0);
}

void ListClass::OnPaint (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         HDC hdc;

         if (hWnd == mamain2)
         {
                    MoveListWindow ();
         }
         else if (hWnd == mamain3)
         {
                    hdc = BeginPaint (mamain3, &aktpaint);
                    ShowDataForms (hdc);
                    PrintHlines (hdc);
                    PrintVlines (hdc);
					if (! FocusWindow)
					{
                	     SetFeldFocus (hdc, AktRow, AktColumn);
					}
                    else
                    {
						 if (!IsUserRenderer)
						 {
 							 SetFocusFrame (hdc, AktRow, AktColumn);
						 }
                         PrintSlinesSatzNr (hdc);
                    }
                    EndPaint (mamain3,&aktpaint);
         }
         else if (hWnd == choise)
         {
                    hdc = BeginPaint (choise, &aktpaint);
                    ChoiseLines (hdc); 
                    EndPaint (choise, &aktpaint);
         }
}

void ListClass::OnHScroll (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         if ((HWND) lParam == hTrack)
         {
             HScroll (wParam, lParam);
         }
}


void ListClass::OnVScroll (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         if ((HWND) lParam == vTrack)
         {
             VScroll (wParam, lParam);
         }
}


void ListClass::OnSize (HWND hWnd,UINT msg, WPARAM wParam,LPARAM lParam)
{
         return;
}


void ListClass::TestScroll ()
{
	      BOOL MustScroll = FALSE;
          while (lPagelen < recanz && 
			  AktRow - scrollpos >= lPagelen)
          {
			  MustScroll = TRUE;
              ScrollDown ();
          }
		  if (MustScroll)
		  {
              InvalidateRect (mamain3, &FocusRect, TRUE);
	   	      UpdateWindow (mamain3);
		  }
}


void ListClass::AddMessage (CMessageHandler *MessageHandler)
{
	MessageTab.Add (MessageHandler);
}

void ListClass::DropMessage (CMessageHandler *MessageHandler)
{
	MessageTab.Drop (MessageHandler);
}

BOOL ListClass::RunMessage (DWORD msg)
{
	BOOL ret = FALSE;
	CMessageHandler **it;
	CMessageHandler *MessageHandler;

	MessageTab.Start ();
	while ((it = MessageTab.GetNext ()) != NULL)
	{
		MessageHandler = *it;
		if (MessageHandler != NULL && MessageHandler->GetMsg () == msg)
		{
			ret = MessageHandler->Run ();
		}
	}
	return ret;
}

void ListClass::InitColBkColor ()
{
	ColBkColorTab.DestroyElements ();
	ColBkColorTab.Destroy ();
}

int ListClass::GetUbPos (form *dataform, int pos)
{
	int col = -1;
	LPSTR name = dataform->mask[pos].item->GetItemName ();
	if (name != NULL)
	{
		col = GetItemPos (&UbForm, name);
	}
	return col;
}

void ListClass::AddColBkColor (LPSTR ColName, COLORREF ColBkColor)
{
	int col = GetItemPos (&UbForm, ColName);
	if (col > 0)
	{
		ColBkColorTab.Add (new CColBkColor (col - 1, ColBkColor));
	}
}

CColBkColor *ListClass::GetColBkColor (int col)
{
	CColBkColor **it;
	CColBkColor *ColBkColor;
	ColBkColorTab.Start ();
	while ((it = ColBkColorTab.GetNext ()) != NULL)
	{
		ColBkColor = *it;
		if (ColBkColor != NULL && (ColBkColor->GetCol () == col))
		{
			return ColBkColor;
		}
	}
	return NULL;
}

void ListClass::PrintColBkColors (HDC hdc, int row, int height)
{
	    int i;
        int x;
        int diff = 0;
		RECT rect;
		CColBkColor *ColBkColor;

        if (LineForm.frmstart)
        {
			if (NoRecNr == 0)
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                               fSatzNr.mask[0].length;
			}
			else
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
        }
        for (i = LineForm.frmstart; i < LineForm.fieldanz - 1; i ++)
        {
			  ColBkColor = GetColBkColor (i);
			  if (ColBkColor != NULL)
			  {
				  x = (LineForm.mask[i].pos[1] - diff) * 
					   tm.tmAveCharWidth;
				  rect.left = x;
 				  rect.top = (int) (double) ((double) UbHeight + row * tm.tmHeight * 
						recHeight * RowHeight 
						+ tm.tmHeight * height * RowHeight);
				  rect.bottom = (int) ((double) rect.top + tm.tmHeight * RowHeight);
				  rect.right = (LineForm.mask[i + 1].pos[1] - diff) * 
					   tm.tmAveCharWidth;
				  ColBkColor->PaintBackground (hdc, &rect);
			  }
		}
}


void ListClass::PrintColBkColors (HDC hdc)
{
	    int i;
        int x, y;
        int diff = 0;
		RECT rect;
		CColBkColor *ColBkColor;

        if (LineForm.frmstart)
        {
			if (NoRecNr == 0)
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1] -
                               fSatzNr.mask[0].length;
			}
			else
			{
                diff = LineForm.mask[LineForm.frmstart - 1].pos[1];
			}
        }
        for (i = LineForm.frmstart; i < LineForm.fieldanz - 1; i ++)
        {
			  ColBkColor = GetColBkColor (i);
			  if (ColBkColor != NULL)
			  {
				  x = (LineForm.mask[i].pos[1] - diff) * 
					   tm.tmAveCharWidth;
				  y = (int) (double) ((double) UbHeight + 
							recanz * tm.tmHeight * recHeight * RowHeight);
				  rect.left = x;
				  rect.top = UbHeight;
				  rect.bottom = y;
				  rect.right = (LineForm.mask[i + 1].pos[1] - diff) * 
					   tm.tmAveCharWidth;
				  ColBkColor->PaintBackground (hdc, &rect);
			  }
		}
}

int ListClass::ProcessMessages(void)
{
        MSG msg;
        char KeyState [256];
        PBYTE lpKeyState; 

        if (RowMode == 0)
        {
			    if (posset == FALSE)
				{
		                 AktRow = 0;
				}
				else
				{
						 TestScroll ();
				}
		        AktColumn = FirstColumn ();
        }
			    
        ListBreak = 0;
		SetFeldFocus0 (AktRow, AktColumn);
        TestBefore ();
  	    if (posset)
		{
            SetFeldFocus0 (AktRow, AktColumn);
		}
		else
		{
			FocusFirst ();
		}
        while (GetMessage (&msg, NULL, 0, 0))
        {
			 if (RunMessage (msg.message))
			 {
				 continue;
			 }

             if (msg.message == WM_KEYDOWN)
             {
				       if (ChangeChar != NULL)
					   {
						   msg.wParam = (*ChangeChar) (msg.wParam);
					   }
					  if (LOWORD (msg.wParam) >= 33 && LOWORD (msg.wParam) <= 255)   //WAL-173    druckbare zeichen
					  {
						 if (getFocusTimerFlag () == TRUE)
						 {
							 KillTimer (GetActiveWindow (), 6); //WAL-173
							 setFocusTimerFlag (FALSE);
						 }
					  }
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                              case VK_F2 :    //LAC-56
                              case VK_F3 :
                              case VK_F4 :
                              case VK_F6 :
                              case VK_F7 :
                              case VK_F8 :
                              case VK_F9 :
                              case VK_F10 :
                              case VK_F11 :
                              case VK_F12 :
                              case VK_DOWN :
                              case VK_RETURN:
                              case VK_UP :
                              case VK_NEXT :
                              case VK_PRIOR :
                              case VK_HOME :
                              case VK_END :
                              case VK_TAB :
                              case REITERAKTAUF :
                              case REITERBESTELLVORSCHLAG :
                              case REITERLETZTEBESTELLUNG :
                              case REITERKRITISCHEMHD :
                              case REITERKOMMLDAT :
                              case REITERHWG1 :
                              case REITERHWG2 :
                              case REITERHWG3 :
                              case REITERHWG4 :
                              case REITERHWG5 :
                              case REITERHWG6 :
                              case REITERHWG7 :
                              case REITERHWG8 :
                              case REITERHWG9 :
                              case REITERHWG10 :
                              case REITERHWG11 :
                              case REITERHWG12 :
                              case REITERHWG13 :
                              case REITERHWG14 :
                              case REITERHWG15 :
                              case REITERHWG16 :
								       FunkKeys (msg.wParam,
										         msg.lParam);
									   continue;
                              case 'A' :
                                 if (GetKeyState (VK_CONTROL) < 0)
                                 {
                                       lpKeyState = (PBYTE) KeyState; 
                                       GetKeyboardState (lpKeyState);
                                       lpKeyState [VK_CONTROL] = (char) 0;
                                       SetKeyboardState (lpKeyState);
                                       msg.wParam = '0';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       msg.wParam = 'X';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       msg.wParam = 'A';
                                       TranslateMessage(&msg);
                                       DispatchMessage(&msg);
                                       continue;
                                 }
                       }
                       if (GetKeyState (VK_MENU) < 0)
                       {
                                       if (testmenu ((int) msg.wParam))
                                       {
									          continue;
                                       }
                       }
                 }
                 else if (msg.message == WM_SYSKEYDOWN)
                 {
                      switch (msg.wParam)
                      {
                              case VK_F10 :
								       FunkKeys (msg.wParam,
										         msg.lParam);
									   continue;
                      }
                      if (GetKeyState (VK_MENU) < 0)
                      {
							 if (getFocusTimerFlag () == TRUE)
							 {
								 KillTimer (GetActiveWindow (), 6); //WAL-173
								 setFocusTimerFlag (FALSE);
							 }
                                       if (testmenu ((int) msg.wParam))   
                                       {
									          continue;
                                       }
                      }
                 }
                 if (IsMouseMessage (&msg))
                 {
					   KillTimer (GetActiveWindow (), 6); //WAL-173
					   setFocusTimerFlag (FALSE);
                       continue;
                 }
                 TranslateMessage(&msg);
                 DispatchMessage(&msg);
        }

        if (recanz > 1)
        {
             InAppend = 0;
             NewRec = 0;
             SetInsAttr ();
        }
        CloseControls (&fUbSatzNr);
        return msg.wParam;
}


// Ende ListClass


void ListClassDB::FreeBezTab (void)
/**
Speicher fuer feldbztab freigeben.
**/
{
        int i;

        for (i = 0; i < fbanz; i ++)
        {
            if (feldbztab[i])
            {
                GlobalFree (feldbztab[i]);
                feldbztab[i] = NULL;
            }
        }
        fbanz = 0;
}

void ListClassDB::FillUbForm (field *feld, char *feldname, int len, int spalte,
                 int BuId  )
/**
Maskenfeld fuer Ueberschruft fuellen.
**/
{
        char *bez;
        char *name;
//        char ibez [21];

        name = feldname;
        memcpy (feld, &iButton, sizeof (field)); 
        bez = NULL;
//        bez = GetIfItem (name, ibez);
#ifndef NINFO
        if (bez == NULL)
        {
            bez = GetItemName (feldname);
        }
#endif
        if (bez)
        {
                   feldbztab[fbanz] = (char *) GlobalAlloc (0, 21);
                   if (feldbztab[fbanz])
                   {
                       strcpy (feldbztab[fbanz], bez);
                       name = feldbztab [fbanz];
                       if (fbanz < MAXBEZ - 1)
                       {
                           fbanz ++;
                       }
                   }
        }
        clipped (name);
        feld->item       = new ITEM ("", name, "", 0);
        feld->length     = len;
        feld->pos[0]     = 0;
        feld->pos[1]     = spalte;
        feld->BuId       = BuId;
}


void ListClassDB::MakeUbForm (void)
/**
Form fuer Ueberschrift generieren.
**/
{
        return;
}

void ListClassDB::SwitchPage (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       DestroyTrack ();
       DestroyVTrack ();
	   DestroyFocusWindow ();
       FreeUbForm ();
       FreeDataForm ();
       FreeLineForm ();
       if (PageView)
       {
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                AktPage     = AktRow;
                scrollposS  = scrollpos;
                banzS = banz;
                zlenS = zlen;
//                LstZiel = ziel;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec (SwSaetze [pos]);
                SetPos (0, 1);
                SelRow = 0;
//                ziel = DlgZiel;
                ausgabesatz = DlgSatz;
                zlen = Dlgzlen;
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);
                this->MakeUbForm ();
                SetSaetze (DlgSaetze);
                Initscrollpos ();
                MakeDataForm0 ();
       }
       else
       {
//                ziel = LstZiel;
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                this->MakeUbForm ();
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec (SwSaetze [pos]);
                Initscrollpos ();
                MakeDataForm0 ();
                Setscrollpos (scrollposS);
       }
}


void ListClassDB::SwitchPage0 (int pos)
/**
Satz in Seitenansicht.
**/
{
       if (recanz == 0) return;
       if (banz == 0) return;

       if (PageView)
       {
           PageView = 0;
       }
       else
       {
           PageView = 1;
       }

       DestroyTrack ();
       DestroyVTrack ();
	   DestroyFocusWindow ();
       if (PageView)
       {
                DlgSatz = (unsigned char *) &dlgs;
                AktRowS     = AktRow;
                AktColumnS  = AktColumn;
                AktPage     = AktRow;
                scrollposS  = scrollpos;
                banzS = banz;
                zlenS = zlen;
                LstSatz = ausgabesatz;
                LstSaetze = SwSaetze;
                SwRecs = recanz;
                ToDlgRec0 (SwSaetze [pos]);
                SetPos (0, 1);
                SelRow = 0;
                ausgabesatz = DlgSatz;
                zlen = sizeof (struct DLGS);
                SetRecanz (banz);
                Setbanz (2);
                SetRowMode (1);
                memcpy (&RowUb,  &UbForm,   sizeof (form));
                memcpy (&RowData,&DataForm, sizeof (form));
                memcpy (&RowLine,&LineForm, sizeof (form));
                UbRow   = &RowUb;
                DataRow = &RowData;
                lineRow = &RowLine ;
		        AktRow = FirstColumnR ();
                CloseControls (&UbForm);
                this->SetUbForm (&UbDlg);
                SetSaetze (DlgSaetze);
                Initscrollpos ();
                SetDataForm0 (&DataDlg, &LineDlg);
       }
       else
       {
                ausgabesatz = LstSatz;
                zlen = zlenS;
                SetPos (AktRowS, AktColumnS);
                SelRow = AktRow;
                Setbanz (banzS);
                SetRowMode (0);
                CloseControls (&UbForm);
                this->SetUbForm (UbRow);
                SetRecanz (SwRecs);
                SetSaetze (LstSaetze);
                FromDlgRec0 (SwSaetze [pos]);
                Initscrollpos ();
                SetDataForm0 (DataRow, lineRow);
                Setscrollpos (scrollposS);
       }
}


char *ListClassDB::GetItemName (char *feldname)
/**
Bezeichnung aus Tabelle Item zu Feldname holen.
**/
{
#ifndef NINFO
           int dsqlstatus;  
           dsqlstatus = item_class.lese_item (feldname);
           if (dsqlstatus == 0)
           {
               return (_item.bezkurz);
           }
#endif
           return (NULL);
}


CColBkColor::CColBkColor (int col, COLORREF Color)
	{
		this->col = col;
		this->Color = Color;
		hBrush = CreateSolidBrush (Color);
	}

CColBkColor::~CColBkColor ()
	{
		DeleteObject (hBrush);
	}

void CColBkColor::PaintBackground (HDC hdc, RECT *rect)
	{
		FillRect (hdc, rect, hBrush);
	}
