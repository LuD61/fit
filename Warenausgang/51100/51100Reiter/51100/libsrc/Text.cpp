#include <stdio.h>
#include "Text.h"
#include "Vector.h"

Text::Text ()
{
         Buffer = new (char [256]);
         strcpy (Buffer, "");
         Len = 0;
         Sub = NULL;
}

Text::Text (char *Txt)
{
         Sub = NULL;
         Buffer = new char [(Len = strlen (Txt)) + 1];
         if (Buffer == NULL)
         {
                Buffer = new (char [1]);
                strcpy (Buffer, "");
                Len = 0;
         }
         else
         {
                strcpy (Buffer, Txt);
         }
}

Text::Text (Text& Txt)
{
         Sub = NULL;
         Buffer = new char [(Len = Txt.GetLen ()) + 1];
         if (Buffer == NULL)
         {
                Buffer = new (char [1]);
                strcpy (Buffer, "");
                Len = 0;
         }
         else
         {
                strcpy (Buffer, Txt.GetBuffer ());
         }
}

Text::~Text ()
{
         if (Buffer != NULL)
         {
                delete Buffer;
         }
}

char *Text::GetBuffer (void)
{
         return Buffer;
}

void Text::SetBuffer (char *Txt)
{
         delete Buffer;
         Buffer = new char [(Len = strlen (Txt)) + 1];
         if (Buffer == NULL)
         {
                Buffer = new (char [1]);
                strcpy (Buffer, "");
                Len = 0;
         }
         else
         {
                strcpy (Buffer, Txt);
         }
}

const Text& Text::operator=(char *Str)
{
	            if (Str == NULL)
				{
					Str = "";
				}
                SetBuffer (Str);  
                return *this;
}

const Text& Text::operator=(int value)
{
	            Format ("%d", value);
				return *this;
}

const Text& Text::operator=(short value)
{
	            Format ("%hd", value);
				return *this;
}

const Text& Text::operator=(long value)
{
	            Format ("%ld", value);
				return *this;
}

const Text& Text::operator=(char value)
{
	            Format ("%c", value);
				return *this;
}

const Text& Text::operator=(Text& Txt)
{
                char *p = new char [Len + Txt.GetLen () + 1];
                strcpy (p, Txt.GetBuffer ());
                SetBuffer (p);  
                delete p;
                return *this;
}

Text& Text::operator+ (char *Str)
{
                char *p = new char [Len + strlen (Str) + 1];
                strcpy (p, Buffer);
                strcat (p, Str);
                SetBuffer (p);
                delete p;
                return *this;
}

Text& Text::operator+= (char *Str)
{
                char *p = new char [Len + strlen (Str) + 1];
                strcpy (p, Buffer);
                strcat (p, Str);
                SetBuffer (p);
                delete p;
                return *this;
}

Text& Text::operator+ (Text& t)
{
                char *p = new char [Len + t.GetLen () + 1];
                strcpy (p, Buffer);
                strcat (p, t.GetBuffer ());
                SetBuffer (p);
                delete p;
                return *this;
}

Text& Text::operator+= (Text& t)
{
                char *p = new char [Len + t.GetLen () + 1];
                strcpy (p, Buffer);
                strcat (p, t.GetBuffer ());
                SetBuffer (p);
                delete p;
                return *this;
}

BOOL Text::operator== (Text &t)
{
                if (strcmp (Buffer, t.GetBuffer ()) == 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator== (char *c)
{
	            if (c == NULL)
				{
					c = "";
				}
                if (strcmp (Buffer, c) == 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator!= (Text &t)
{
                if (strcmp (Buffer, t.GetBuffer ()) != 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator!= (char *c)
{
                if (strcmp (Buffer, c) != 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator> (Text &t)
{
                if (strcmp (Buffer, t.GetBuffer ()) > 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator> (char *c)
{
                if (strcmp (Buffer, c) > 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator< (Text &t)
{
                if (strcmp (Buffer, t.GetBuffer ()) < 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator< (char *c)
{
                if (strcmp (Buffer, c) < 0)
                {
                    return TRUE;
                }
                return FALSE;
}


char Text::operator[] (int idx)
{
                if (idx >= Len)
                {
                    return 0;
                }
                return Buffer[idx];
}


Text::operator LPSTR () const
{
	return Buffer;
}


void Text::ToCharArray (LPSTR array, int len)
{
	if (len > 0)
	{
		strncpy (array, Buffer, len);
        array[len] = 0;
	}
	else
	{
		strcpy (array, Buffer);
	}
}

Text& Text::TrimLeft (void)
{
         int i;
         unsigned char *p;

         p = new unsigned char [Len + 1];
         if (p == NULL)
         {
             return *this;
         }

         strcpy ((char *) p, Buffer);
         for (i = 0; i < Len; i ++)
         {
             if (p[i] > (unsigned char ) ' ')
             {
                 break;
             }
         }
         SetBuffer ((char *) &p[i]);
         delete p;
		 return *this;
}

Text& Text::TrimRight (void)
{
         int i;
         unsigned char *p;

         p = new unsigned char [Len + 1];
         if (p == NULL)
         {
             return *this;
         }

         strcpy ((char *) p, Buffer);
         for (i = Len; i >= 0; i --)
         {
             if (p[i] > (unsigned char ) ' ')
             {
                 break;
             }
         }
         p [i + 1] = 0;
         SetBuffer ((char *) p);
         delete p;
		 return *this;
}

Text& Text::Trim (void)
{
	      TrimLeft ();
		  TrimRight ();
		  return *this;
}


char *Text::TrimRight (char *str)
{
         Text String = str;
         String.TrimRight ();
         strcpy (str, String.GetBuffer ());
         return str;
}


char *Text::SubString (int start, int len)
{
         if (Sub != NULL)
         {
             delete Sub;
         }
		 if (len == 0)
		 {
			 len = GetLength () - start;
		 }
         Sub = new char [len + 1];
         if (Sub == NULL)
         {
             return NULL;
         }
         if (start + len > (int) strlen (Buffer))
         {
             len = strlen (Buffer) - start;
         }
         memcpy (Sub, &Buffer[start], len);  
         Sub[len] = 0;
         return Sub;
}

void Text::Format (char *format, ...)
{
         va_list args;
         char b [0x1000];

         va_start (args,format);
         vsprintf (b,format,args);
         va_end (args);
         SetBuffer ((char *) b);
}


int  Text::CompareNoCase (Text& Name)
{
	Text Str1 = Name;
	Text Str2 = *this;

	Str1.MakeUpper ();
	Str2.MakeUpper ();
	return strcmp (Str1.GetBuffer (), Str2.GetBuffer ());
}

void Text::MakeUpper ()
{
          for (int i = 0; i < Len; i ++)
          {
               Buffer[i] = (char) toupper ((int) Buffer [i]);
          }
}

BOOL Text::matchcomp (LPSTR m, LPSTR s)
{
		  if (*m == 0 && *s == 0)
		  {
			  return TRUE;
		  }

		  if (*m == 0)
		  {
			  return FALSE;
		  }

		  if (*s == 0)
		  {
			  return FALSE;
		  }

		  if (*m == '*')
		  {
		      while (*m == '*' && *m != 0) m += 1;
		      if (*m == 0)
			  {
			         return TRUE;
			  }

		      while (*s != 0)
			  {
			        if (*m == '?')
					{
				          m += 1;
				          continue;
					}
			        if (*m == *s)
					{
				           break;
					}
			        s += 1;
			  }
		      if (*s == 0)
			  {
			       return FALSE;
			  }
		  }
		  else
		  {
			  if (*m != *s && *m != '?')
			  {
				  return FALSE;
			  }
		  }

		  for (int i = 1; ; i ++)
		  {
			  if (m[i] == 0)
			  {
				  if (s[i] == 0)
				  {
				        return TRUE;
				  }
				  else
				  {
				        return FALSE;
				  }
			  }

			  if (m[i] == '*')
			  {
			      return matchcomp (&m[i], &s[i]);
			  }

			  if (s[i] == 0)
			  {
				  return FALSE;
			  }

			  if (m[i] == '?')
			  {
				  continue;
			  }
			  if (m[i] == s[i])
			  {
				  continue;
			  }
			  return matchcomp (m, s + 1);
		  }
		  return TRUE;
}

BOOL Text::CompareMatch (Text &txt)
{
	      char *p1 = txt.GetBuffer ();
		  char *p2 = GetBuffer ();
		  return matchcomp (p1, p2);
}


void Text::MakeLines (int MaxRowLen)
{
	       char *b;
		   int i, j, ib;
		   CVector Lines;

		   b = new char [MaxRowLen + 2];

		   ib = -1;
		   for (i = 0, j = 0; i < Len; i ++)
		   {
			   if (Buffer[i] == ' ')
			   {
				   ib = i;
			   }

			   if (Buffer[i] == '\n')
			   {
				   b[j] = 0;
				   Lines.Add (b);
				   j = 0;
 		           b = new char [MaxRowLen + 2];
				   ib = -1;
			   }

			   if (j == MaxRowLen)
			   {
				   if (ib != -1)
				   {
				       j -= (i - ib);
				       i = ib + 1;
				       ib = -1;
				   }
				   b[j] = 0;
				   Lines.Add (b);
				   j = 0;
 		           b = new char [MaxRowLen + 2];
			   }

               b[j] = Buffer[i];
			   j ++;
		   }
		   if (j > 0)
		   {
			       b[j] = 0;
				   Lines.Add (b);
		   }

           Text Tlines;
		   char *t;
		   if ((t = (char*) Lines.GetNext ()) != NULL)
		   {
			   Tlines = t;
		   }
		   while ((t = (char*) Lines.GetNext ()) != NULL)
		   {
			   Tlines = Tlines + "\n" + t;
		   }
		   Lines.DestroyAll ();
           SetBuffer ((char *) Tlines.GetBuffer ());
}

	       

int Text::Find (LPCSTR Substr)
{
          char *p;
          
          p = strstr (Buffer, Substr);
          if (p == NULL)
          {
              return -1;
          }
          return  (int) (p - Buffer);
}

int Text::Find (Text& Substr)
{
          char *p;
          
          p = strstr (Buffer, Substr.GetBuffer ());
          if (p == NULL)
          {
              return -1;
          }
          return  (int) (p - Buffer);
}

Text Text::Right (int length)
{
	Text Sub = "";
	int start = GetLength () - length;
	Sub = &Buffer[start];
	return Sub;
}

Text Text::Left (int length)
{
	Text Sub = "";
	Sub =  SubString (0, length);
	return Sub;
}

Text Text::Mid (int start, int length)
{
	Text Sub = "";
	if (length == 0)
	{
		length = GetLength ();
	}
	Sub = SubString (start, length);
	return Sub;
}

BOOL Text::EndsWith (LPSTR endstr)
{
	int len = strlen (endstr);
	Text end = SubString (GetLength () - len);
	return (end == endstr);
}


Token::Token ()
{
     Tokens   = NULL;
     AnzToken = NULL;
     AktToken = NULL;
}

Token::Token (char *Txt, char *sep)
{
     Tokens = NULL;
     Buffer = Txt;
     Buffer.TrimRight ();
     this->sep = sep;
     GetTokens (Txt);
}

Token::Token (Text& Txt, char *sep)
{
     Tokens = NULL;
     Buffer = Txt;
     Buffer.TrimRight ();
     this->sep = sep;
     GetTokens (Txt.GetBuffer ());
}


Token::~Token ()
{
     if (Tokens != NULL)
     {
         for (int i = 0; i < AnzToken; i ++)
         {
               delete Tokens[i];
         }
         delete Tokens;
     }
}

const Token& Token::operator=(char *Txt)
{
     if (Tokens == NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt);
     return *this;
}

const Token& Token::operator=(Text& Txt)
{
     if (Tokens == NULL)
     {
           for (int i = 0; i < AnzToken; i ++)
           {
               delete Tokens[i];
           }
           delete Tokens;
           Tokens = NULL;
     }
     Buffer = Txt;
     GetTokens (Txt.GetBuffer ());
     return *this;
}

void Token::GetTokens (char *Txt)
{
     AnzToken = 0;
     char *b = Buffer.GetBuffer ();
     char *p = strtok (b, sep.GetBuffer ());
     while (p != NULL)
     {
             AnzToken ++;
             p = strtok (NULL, sep.GetBuffer ());
     }
     Tokens = new Text *[AnzToken];
     if (Tokens == NULL)
     {
             return;
     }

     Buffer = Txt;
     Buffer.TrimRight ();
     b = Buffer.GetBuffer ();
     p = strtok (b, sep.GetBuffer ());
     int i = 0;
     while (p != NULL)
     {
             Tokens[i] = new Text (p);
             p = strtok (NULL, sep.GetBuffer ());
             i ++;
     }
     AktToken = 0;
}

void Token::SetSep (char *sep)
{
     this->sep = sep;
}

char * Token::NextToken (void)
{
     if (AktToken == AnzToken)
     {
          AktToken = 0;
          return NULL;
     }
     AktToken ++;
     return Tokens[AktToken - 1]->GetBuffer ();
}

char * Token::GetToken (int idx)
{
     if (idx < 0 || idx >= AnzToken)
     {
           return NULL;
     }

     return Tokens [idx]->GetBuffer ();
}

int Token::GetAnzToken (void)
{
     return AnzToken;
}

