#ifndef _ITPROG_DEF
#define _ITPROG_DEF
#include "cmask.h"

class ItProg
{
  private :
      char *Item;
      int (*Prog) (void);
  public :
      ItProg (char *Item, int (*Prog) (void))
      {
          this->Item = Item;
          this->Prog = Prog;
      }

      void SetItem (char *Item)
      {
          this->Item = Item;
      }

      void SetProg (int (*Prog) (void))
      {
          this->Prog = Prog;
      }

      char *GetItem (void)
      {
          return Item;
      }

      void *GetProg (void)
      {
          return Prog;
      }


      void SetBefore (CFORM *cForm)
      {
          if (Prog != NULL && Item != NULL)
          {
                if (cForm->GetCfield (Item) != NULL)
                {
                         cForm->GetCfield (Item)->SetBefore (Prog);
                }
          }
      }

      void SetAfter (CFORM *cForm)
      {
          if (Prog != NULL && Item != NULL)
          {
                if (cForm->GetCfield (Item) != NULL)
                {
                        cForm->GetCfield (Item)->SetAfter (Prog);
                }
          }
      }
};
#endif
