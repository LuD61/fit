#include "StdAfx.h"
#include "LskList.h"

CLskList::CLskList(void)
{
	ls = 0l;
	auf = 0l;
	kun = 0l;
	ls_stat = 0;
	kun_krz1[0] = 0;
	kun_bran2[0] = 0;
}

CLskList::CLskList(long ls, long auf, long kun, DATE_STRUCT& lieferdat, short ls_stat,
				   LPTSTR kun_krz1, LPTSTR kun_bran2)
{
	this->ls = ls;
	this->auf = auf;
	this->kun = kun;
	this->lieferdat.year  = lieferdat.year;
	this->lieferdat.month = lieferdat.month;
	this->lieferdat.day   = lieferdat.day;
	this->ls_stat = ls_stat;
	_tcsncpy (this->kun_krz1, kun_krz1, 16);
    this->kun_krz1[16] = 0;   
	_tcsncpy (this->kun_bran2, kun_bran2, 2);
	this->kun_bran2[2] = 0;
}

CLskList::~CLskList(void)
{
}

void CLskList::SetLieferdat (LPTSTR p)
{
	lieferdat.day   = _ttoi (p);
	lieferdat.month = _ttoi (&p[3]);
	lieferdat.year  = _ttoi (&p[5]);
}

void CLskList::SetLieferdat (CString& ld)
{
	SetLieferdat (ld.GetBuffer ());
}

void CLskList::SetKunKrz1 (LPTSTR kun_krz1)
{
	_tcsncpy (this->kun_krz1, kun_krz1, 16);
    this->kun_krz1[16] = 0;   
}

void CLskList::SetKunBran2 (LPTSTR kun_bran2)
{
	_tcsncpy (this->kun_bran2, kun_bran2, 2);
	this->kun_bran2[2] = 0;
}

void CLskList::SetKunKrz1 (CString kun_krz1)
{
	_tcsncpy (this->kun_krz1, kun_krz1.GetBuffer (), 16);
    this->kun_krz1[16] = 0;   
}

void CLskList::SetKunBran2 (CString kun_bran2)
{
	_tcsncpy (this->kun_bran2, kun_bran2.GetBuffer (), 2);
	this->kun_bran2[2] = 0;
}
