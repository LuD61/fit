#pragma once
#include "label.h"

class CVLabel :
	public CLabel
{
	DECLARE_DYNCREATE(CVLabel)
public:
	CVLabel(void);
public:
	~CVLabel(void);
	virtual void FillRectParts (CDC &cDC, COLORREF color, CRect rect);
};
