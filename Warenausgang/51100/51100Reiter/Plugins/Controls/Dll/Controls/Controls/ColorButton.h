#pragma once
#include "afxwin.h"
#include "QuikInfo.h"
#include "ControlContainer.h"

class CColorButton :
	public CStatic
//	public CButton
{
	DECLARE_DYNCREATE(CColorButton)
protected:
	DWORD Style;
	DECLARE_MESSAGE_MAP()
	virtual void OnSize (UINT, int, int);
	virtual void DrawItem (LPDRAWITEMSTRUCT  lpDrawItemStruct);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct); 
 
//	virtual BOOL PreTranslateMessage(MSG* pMsg ); 
public:
	enum
	{
		Center = 0,
		Left = 1,
		Right = 2,
	};
	enum
	{
		Standard = 0,
		Underline = 1,
		Bold = 2,
		UnderlineBold = 3,
	};

	enum
	{
		Solide = 0,
		Dot = 1,
		NoBorder = 2,
		Rounded = 3,
		LeftLine = 4,
		RightLine = 5,
		TopLine = 6,
		BottomLine = 7,
		LeftRect = 8,
		RightRect = 9,
	};

	enum ACTIVE_REG
	{
		AllActive = 0,
		LeftActive = 1,
		RightActive = 2,
	};

	ACTIVE_REG m_ActiveReg;
	CSize *m_ActiveSize;
	int Orientation;
	int TextStyle;
	int BorderStyle;
	BOOL Transparent;
	BOOL DynamicColor;
	BOOL IsActive;
	BOOL IsSelected;
	static COLORREF DynColorGray;
	static COLORREF DynColorBlue;
	COLORREF DynColor;
	COLORREF RolloverColor;
	BOOL EnableRollover;
	COLORREF ActiveColor;
	CQuikInfo Tooltip;
	COLORREF TextColor;
	HCURSOR Hand;
	HCURSOR Arrow;
    BOOL ButtonCursor;
	CBitmap Bitmap;
	CBitmap TransBitmap;
	HBITMAP hTransBitmap;
	CBitmap Mask;
	HBITMAP hMask;
	UINT nID;
	COLORREF BkColor;
	COLORREF TransparentColor;
	BOOL UseTransparentColor;
	int xSpace;
	CControlContainer *Container;

	BOOL ColorSet;
	BOOL NoUpdate;
	CFont Font;
	BOOL FontSet;
	CColorButton(void);
	~CColorButton (void);

	void SetTansParentColor ()
	{
		UseTransparentColor = TRUE;
	}

	void SetTansParentColor (COLORREF color)
	{
		TransparentColor = color;
		UseTransparentColor = TRUE;
	}
    virtual BOOL Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect,
                        CWnd* pParentWnd,   UINT nID = 0xffff);
	void SetToolTip (LPTSTR Text);
	void LoadBitmap (UINT ID);
	void LoadMask (UINT ID);
	void SetBkColor (COLORREF color);
	void Draw (CDC&);
    void SetButtonCursor (BOOL b);
    void SetActive (BOOL b = TRUE);
    void SetSelected (BOOL b = TRUE);
    BOOL InClient (CPoint p);
    void SetButtonCursor (CPoint p);
	void SetFont ();
	virtual void SetFont (CFont *f, BOOL Redrae=TRUE);
    afx_msg void OnMouseMove(UINT nFlags,  CPoint p);
    afx_msg void OnLButtonDown(UINT nFlags,  CPoint p);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest,  UINT message );
	afx_msg void OnKeyDown(UINT nChar,  UINT nRepCnt,  UINT nFlags);
	afx_msg void OnSetFocus(CWnd *cWnd);
	afx_msg void OnKillFocus(CWnd *cWnd);
	afx_msg void OnShowWindow (BOOL bShow, UINT nStatus);
	void FillRectParts (CDC &cDC, COLORREF color, CRect rect);
	void FillRoundedRect (CDC& cDC, CRect *rect, COLORREF Color);
};
