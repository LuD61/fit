#pragma once
#include "vector.h"
#include "CtrlInfo.h"
#include "ColorButton.h"

#define DOCKSIZE -1
#define DOCKLEFT -2
#define DOCKRIGHT -3
#define DOCKTOP -4
#define DOCKBOTTOM -5

class CCtrlGrid :
	public CVector
{
public:
	CWnd *Dlg;
	CRect *DlgSize;
	int rows;
	int columns;
	int RowSpace;
	int ColSpace;
	int xBorder;
	int yBorder;
	int cxBorder;
	int cyBorder;
	int CellHeight;
	int pcx;
    int pcy;
	CFont *Font;

	CVector ColStart;
	CCtrlGrid(void);
	~CCtrlGrid(void);
	void DestroyGrid ();
	void Create (CWnd *, int, int);
//	void Add (CCtrlInfo *);
	void Display ();
	int Calculate (int);
	void SetGridX (int, int, TEXTMETRIC*, int);
	void Move (int, int);
	void SetBorder (int, int);
	void SetBorder (int, int, int, int);
	void SetGridSpace (int, int);
	void SetCellHeight (int);
	void SetFontCellHeight (CWnd *, CFont *);
	void SetFontCellHeight (CWnd *);
	void SetFontCellHeight (CWnd *, int);
	void SetParentMetrics (int , int);
    void TestRightDockControl ();
	virtual void CreateChoiceButton (CButton& Button, int Id, CWnd *Parent);
	virtual void CreateColorChoiceButton (CColorButton&, int, CWnd*);
	void CreateCheckBox (CButton&, int, CWnd*);
	void SetFont (CFont *);
	void GetGridRect (CRect *rect);
	void MoveGrid (CRect *rect);
	void MoveGridControls (int, int);

    void SetItemCharHeight (CWnd *Item, int Height);
    void SetItemLogHeight (CWnd *Item, int Height);
    void SetItemPixelHeight (CWnd *Item, int Height);

    void SetItemCharWidth (CWnd *Item, int Width);
    void SetItemLogWidth (CWnd *Item, int Width);
    void SetItemPixelWidth (CWnd *Item, int Width);
	void SetNewStyle (BOOL style);

    static void DockControlWidth (CWnd *, CWnd *, CWnd *);
    static void DockControlHeight (CWnd *, CWnd *, CWnd *);
	void Enable (BOOL enable);
	void SetVisible (BOOL enable);
};
