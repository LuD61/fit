#include "StdAfx.h"
#include "SmtCore.h"

CSmtCore *CSmtCore::m_Instance = NULL;

CSmtCore::CSmtCore(void)
{
	m_SmtTable = NULL;
}

CSmtCore::~CSmtCore(void)
{
}

CSmtCore *CSmtCore::GetInstance ()
{
	if (m_Instance == NULL)
	{
		m_Instance = new CSmtCore ();
	}
	return m_Instance;
}

void CSmtCore ::DestroyInstance ()
{
	if (m_Instance != NULL)
	{
		delete m_Instance;
		m_Instance = NULL;
	}
}
