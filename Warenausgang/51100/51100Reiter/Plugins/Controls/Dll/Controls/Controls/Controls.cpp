// Controls.cpp : Definiert die Initialisierungsroutinen f�r die DLL.
//

#include "stdafx.h"
#include "Controls.h"
#include "sumframe.h"
#include "sumview.h"
#include "smttable.h"
#include <vector>
#include "smtcore.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: Wenn diese DLL dynamisch mit MFC-DLLs verkn�pft ist,
//		muss f�r alle aus dieser DLL exportierten Funktionen, die in
//		MFC aufgerufen werden, das AFX_MANAGE_STATE-Makro
//		am Anfang der Funktion hinzugef�gt werden.
//
//		Beispiel:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// Hier normaler Funktionsrumpf
//		}
//
//		Es ist sehr wichtig, dass dieses Makro in jeder Funktion
//		vor allen MFC-Aufrufen angezeigt wird. Dies bedeutet,
//		dass es als erste Anweisung innerhalb der 
//		Funktion angezeigt werden muss, sogar vor jeglichen Deklarationen von Objektvariablen,
//		da ihre Konstruktoren Aufrufe in die MFC-DLL generieren
//		k�nnten.
//
//		Siehe Technische Hinweise f�r MFC 33 und 58 f�r weitere
//		Details.
//


// CControlsApp

BEGIN_MESSAGE_MAP(CControlsApp, CWinApp)
END_MESSAGE_MAP()


// CControlsApp-Erstellung

CControlsApp::CControlsApp()
{
	// TODO: Hier Code zur Konstruktion einf�gen.
	// Alle wichtigen Initialisierungen in InitInstance positionieren.
}


// Das einzige CControlsApp-Objekt

CControlsApp theApp;


// CControlsApp-Initialisierung

BOOL CControlsApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

EXPORT int GetPosTxtKz (int PosTxtKz)
{
	CPosTxtKzDlg dlg;
	dlg.PosTxtMode = PosTxtKz;
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
		PosTxtKz = dlg.PosTxtMode;
	}
	return PosTxtKz;
}

EXPORT int GetPosTxtKzEx (int PosTxtKz, BOOL FromLs)
{
	CPosTxtKzDlg dlg;
	dlg.PosTxtMode = PosTxtKz;
	dlg.FromLs = FromLs;
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
		PosTxtKz = dlg.PosTxtMode;
	}
	return PosTxtKz;
}

CWnd *cWndParent = NULL;
CSumFrame *m_SumFrame = NULL;


EXPORT void CloseOrderSum ()
{
	if (m_SumFrame != NULL)
	{
		delete m_SumFrame;
		m_SumFrame = NULL;
		cWndParent = NULL;
	}
}

EXPORT void ShowOrderSum (HWND parent, CSmtTable* smtTable)
{
	struct CCreateContext cc;
	int cxscreen;
	int AnzSpalten = 0; 
	static int AnzSpalten_old = 0; 
	int x;
	int y;
	CRect rect;
	HWND focus = GetFocus ();
	if (m_SumFrame != NULL)
	{
		if (!IsWindow (m_SumFrame->m_hWnd))
		{
			m_SumFrame = NULL;
			cWndParent = NULL;
		}
	}

	if (m_SumFrame != NULL)
	{
		smtTable->Start ();
		while (smtTable->GetNext () != NULL)
		{
			AnzSpalten +=1;
		}
		if (AnzSpalten != AnzSpalten_old)
		{
	
			CloseOrderSum ();
	
			if (parent != NULL)
			{
				cWndParent = new CWnd ();
				cWndParent->Attach (parent);
			}
			memset (&cc, 0, sizeof (CCreateContext));
			cc.m_pNewViewClass = RUNTIME_CLASS (CSumView);
	
			cxscreen = GetSystemMetrics (SM_CXSCREEN);
	
			x = cxscreen - 220 - (100 * AnzSpalten) ;
			y = GetSystemMetrics (SM_CYCAPTION);
			rect.left = x; 
			rect.top = y;
			rect.right = cxscreen;
//LAC-87			rect.bottom = y + 270;
			rect.bottom = y + 248;  //LAC-87 etwas k�rzer, wg. 2-Zeilige Reiter
	
	
			m_SumFrame = new CSumFrame ();
			cc.m_pCurrentFrame = m_SumFrame;
			m_SumFrame->Create (NULL, _T("Kalkulationsfenster "), WS_THICKFRAME | WS_CAPTION, rect, cWndParent, NULL, NULL, &cc);
			m_SumFrame->InitialUpdateFrame (NULL, TRUE);
			m_SumFrame->ShowWindow(SW_SHOWNORMAL);
			m_SumFrame->UpdateWindow();
			AnzSpalten_old = AnzSpalten;
		}



		SMT_CORE->set_SmtTable (smtTable);
		CSumView *View = dynamic_cast<CSumView *> (m_SumFrame->GetActiveView ());
		if (View != NULL)
		{
			View->RefreshGrid ();
		}
		m_SumFrame->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (parent != NULL)
	{
		cWndParent = new CWnd ();
		cWndParent->Attach (parent);
	}
    memset (&cc, 0, sizeof (CCreateContext));
	cc.m_pNewViewClass = RUNTIME_CLASS (CSumView);

	cxscreen = GetSystemMetrics (SM_CXSCREEN);
//	int x = cxscreen - 600;
	smtTable->Start ();
	while (smtTable->GetNext () != NULL)
	{
		AnzSpalten +=1;
	}

	x = cxscreen - 220 - (100 * AnzSpalten) ;
	y = GetSystemMetrics (SM_CYCAPTION);
	rect.left = x; 
	rect.top = y;
	rect.right = cxscreen;
//LAC-87	rect.bottom = y + 270;
	rect.bottom = y + 248;  //LAC-87  etwas k�rzer wg. 2-zeilige Reiter


    if (smtTable == NULL)
	{
		smtTable = new CSmtTable ();
		CSmtValues *values = new CSmtValues ();
		values->set_Name (_T("Wurst und Fleisch"));
		smtTable->Add (values);

		values = new CSmtValues ();
		values->set_Name (_T("TK-Waren"));
		smtTable->Add (values);

		values = new CSmtValues ();
		values->set_Name (_T("S��waren"));
		smtTable->Add (values);

		values = new CSmtValues ();
		values->set_Name (_T("Gesamt"));
		smtTable->Add (values);
	}

	SMT_CORE->set_SmtTable (smtTable);

	m_SumFrame = new CSumFrame ();
	cc.m_pCurrentFrame = m_SumFrame;
//	cc.m_pCurrentDoc = AngebotCore->GetDocument ();
	m_SumFrame->Create (NULL, _T("Kalkulationsfenster "), WS_THICKFRAME | WS_CAPTION, rect, cWndParent, NULL, NULL, &cc);
//    CalcFrame->InitialUpdateFrame (AngebotCore->GetDocument (), TRUE);
    m_SumFrame->InitialUpdateFrame (NULL, TRUE);
	m_SumFrame->ShowWindow(SW_SHOWNORMAL);
	m_SumFrame->UpdateWindow();

}

EXPORT void HideOrderSum ()
{
	if (m_SumFrame != NULL)
	{
		m_SumFrame->ShowWindow (SW_HIDE);
	}
}


