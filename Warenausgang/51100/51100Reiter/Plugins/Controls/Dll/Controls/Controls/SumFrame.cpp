// SumFrame.cpp: Implementierungsdatei
//

#include "stdafx.h"
//#include "ChoiceLs.h"
#include "SumFrame.h"


// CSumFrame

IMPLEMENT_DYNCREATE(CSumFrame, CFrameWnd)

CSumFrame::CSumFrame()
{

}

CSumFrame::~CSumFrame()
{
}

BEGIN_MESSAGE_MAP(CSumFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CSumFrame-Meldungshandler
int CSumFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
	{
		return -1;
	}
	return 0;
}

BOOL CSumFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	cs.style = WS_POPUP | WS_THICKFRAME | WS_CAPTION | WS_SYSMENU;
	return TRUE;
}

BOOL CSumFrame::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	CFrameWnd::OnCreateClient (lpcs, pContext);
	return TRUE;
}

void CSumFrame::OnSize (UINT nType, int cx, int cy)
{
	CView *View = GetActiveView ();
	if (View != NULL)
	{
		View->MoveWindow (0, 0, cx, cy);
	}
}

void CSumFrame::OnClose ()
{
	ShowWindow (SW_HIDE);
}