// SumView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Resource.h"
#include "SumView.h"
#include "smtcore.h"


// CSumView

IMPLEMENT_DYNCREATE(CSumView, CFormView)

CSumView::CSumView()
	: CFormView(CSumView::IDD)
{
	BkColorSet = FALSE;
	dlgBrush = NULL;
	tableBrush = NULL;
	DlgBkColor = GetSysColor (COLOR_3DFACE);
    m_TableBkColor = RGB ( 0, 0, 255);
}

CSumView::~CSumView()
{
}

void CSumView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SMT_GRID, m_SmtGrid);
	DDX_Control(pDX, IDC_TITLE, m_Title);
}

BEGIN_MESSAGE_MAP(CSumView, CFormView)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()


// CSumView-Diagnose

#ifdef _DEBUG
void CSumView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSumView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSumView-Meldungshandler

void CSumView::OnInitialUpdate ()
{
	CFormView::OnInitialUpdate ();
	m_SmtGrid.set_SmtTable (SMT_CORE->SmtTable ());
    
	m_Title.SetParts (60);
	m_Title.SetWindowText (_T(" Sortimentsbezogene Summen (HWG)"));
	m_Title.SetBoderStyle (m_Title.Solide);
	m_Title.SetOrientation (m_Title.Left);
	m_Title.SetDynamicColor (TRUE);
//	m_Title.SetTextColor (RGB (255, 255, 153));
//	m_Title.SetTextColor (RGB (175, 175, 255));
	m_Title.SetTextColor (RGB (255, 255, 255));
	m_Title.SetBkColor (RGB (192, 192, 192));

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand
	m_SmtGrid.FillList ();

	CCtrlInfo *c_Title = new CCtrlInfo (&m_Title, 0, 0, DOCKRIGHT, 1);
	c_Title->BottomControl = &m_SmtGrid;
	CtrlGrid.Add (c_Title);
	CCtrlInfo *c_SmtGrid = new CCtrlInfo (&m_SmtGrid, 0, 2, DOCKRIGHT, DOCKBOTTOM);
	CtrlGrid.Add (c_SmtGrid);

	CFont *Font = GetFont ();
	LOGFONT l; 
	Font->GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	l.lfHeight = 25;
	m_TitleFont.CreateFontIndirect (&l);
	m_Title.SetFont (&m_TitleFont);

	CtrlGrid.Display ();
}

void CSumView::OnSize (UINT nType, int cx, int cy)
{
	CRect trect;
	GetClientRect (&trect);
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
}

HBRUSH CSumView::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && !BkColorSet)
	{
		    if (dlgBrush == NULL)
			{
				dlgBrush = CreateSolidBrush (DlgBkColor);
			}
		    if (dlgBrush != NULL)
			{
				BkColorSet = TRUE;
				pDC->SetBkMode (TRANSPARENT);
				return dlgBrush;
			}
	}
	return CFormView::OnCtlColor (pDC, pWnd, nCtlColor);
}

short CSumView::RefreshGrid ()
{
	m_SmtGrid.set_SmtTable (SMT_CORE->SmtTable ());
	return m_SmtGrid.FillList ();
}

