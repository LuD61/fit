// SmtGridEx.cpp: Implementierungsdatei
//

#include "stdafx.h"
//#include "ChoiceLs.h"
#include "SmtGridEx.h"


// CSmtGridEx-Dialogfeld

IMPLEMENT_DYNAMIC(CSmtGridEx, CListCtrl)

CSmtGridEx::CSmtGridEx()
	: CListCtrl ()
{
	m_SmtTable = NULL;
	RowText.Add (_T("  Auftragswert in €"));
	RowText.Add (_T("  Auftragsgew. in kg"));
	RowText.Add (_T("  Liquidität in €"));
	RowText.Add (_T("  Anteil SMT in %"));
}

CSmtGridEx::~CSmtGridEx()
{
}

short CSmtGridEx::FillList () 
{
    HICON hiconItem;     // icon for list-view items 
	int i = 0;
	short dreturn = 0;
	CSmtValues smtValues;
	CSmtValues *it;
	CString GridValue;

    if (m_SmtTable == NULL)
	{
		return dreturn;
	}


    hLarge = ImageList_Create(GetSystemMetrics(SM_CXICON), 
        GetSystemMetrics(SM_CYICON), ILC_MASK, 1, 1); 
//    hLarge = ImageList_Create(GetSystemMetrics(SM_CXICON), 
//        GetSystemMetrics(SM_CYICON), ILC_MASK, 1, 1); 
    hSmall = ImageList_Create(20, 30, ILC_MASK, 1, 1); 
 
/*
	hiconItem = LoadIcon(AfxGetApp()->m_hInstance, 
		                 MAKEINTRESOURCE(IDI_ICON2)); 
    ImageList_AddIcon(hLarge, hiconItem); 
    ImageList_AddIcon(hSmall, hiconItem); 
    DestroyIcon(hiconItem); 
*/


    HWND hWndListView = m_hWnd;

    ListView_SetImageList(hWndListView, hLarge, LVSIL_NORMAL); 
    ListView_SetImageList(hWndListView, hSmall, LVSIL_SMALL); 

	SetExtendedStyle (LVS_EX_GRIDLINES);
	SetTextBkColor (RGB (255,255,204));
	SetBkColor (RGB (255,255,204));

	DeleteAllItems ();
	int nColumnCount = GetHeaderCtrl()->GetItemCount();

	for (int i=0;i < nColumnCount;i++)
	{
	   DeleteColumn(0);
	}	
    DWORD Style = SetStyle (LVS_REPORT);

    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
	m_SmtTable->Start ();
	i = 2;
    SetCol (_T("Beschreibung"),  1, 150, LVCFMT_LEFT);
	while ((it = m_SmtTable->GetNext ()) != NULL)
	{
		smtValues = *it;
		CString Name = smtValues.Name ();
		if (Name != _T(""))
		{
			SetCol (smtValues.Name (), i, 100, LVCFMT_RIGHT);
			i ++;
		}
	}
	dreturn = i;

	int row;
	for (row = 0; row < 4; row ++)
	{
        int ret = InsertItem (row, -1);
		m_SmtTable->Start ();
		ret = SetItemText (*RowText.Get(row), row, 1);
		int col = 2;
		while ((it = m_SmtTable->GetNext ()) != NULL)
		{
			smtValues = *it;
			CString Name = smtValues.Name ();
			if (Name == _T(""))
			{
				continue;
			}
			if (row == Amount)
			{
				 GridValue.Format (_T("%.2lf"), smtValues.Amount ());
			}
			else if (row == Weight)
			{
				GridValue.Format (_T("%.3lf"), smtValues.Weight ());
			}
			else if (row == Liq)
			{
				GridValue.Format (_T("%.2lf"), smtValues.Liq ());
			}
			else if (row == SmtPart)
			{
				GridValue.Format (_T("%.2lf"), smtValues.SmtPart ());
			}
			ret = SetItemText (GridValue.GetBuffer (), row, col);
			col ++;
		}
	}

	return dreturn;
}

BOOL CSmtGridEx::SetCol (LPTSTR Txt, int idx, int Width, int Align)
{  
   LV_COLUMN Col;

   HWND hWndListView = this->m_hWnd;

   Col.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;   
   Col.fmt = Align;
   Col.cx  = Width;
   Col.pszText = Txt;
   Col.iSubItem = idx;
   return ListView_InsertColumn(hWndListView, idx, &Col); 
}

DWORD CSmtGridEx::SetStyle (DWORD st)
{
    DWORD Style = GetWindowLong (m_hWnd, GWL_STYLE);

    Style &= ~(LVS_ICON | LVS_SMALLICON | LVS_LIST | LVS_REPORT);
    SetWindowLong (m_hWnd, GWL_STYLE, Style |= LVS_REPORT); 
    return Style;
}

DWORD CSmtGridEx::SetListStyle (DWORD st)
{
    DWORD Style = GetWindowLong (m_hWnd, GWL_STYLE);
    Style |= st;
    SetWindowLong (m_hWnd, GWL_STYLE, Style); 
    return Style;
}


DWORD CSmtGridEx::SetExtendedStyle (DWORD st)
{

    DWORD style = GetExtendedStyle ();
    style |= st;
	CListCtrl::SetExtendedStyle (style);
    return style;
}

BOOL CSmtGridEx::InsertItem (int idx)
{
   LV_ITEM Item;

   Item.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
   Item.state      = 0; 
   Item.stateMask  = 0; 
   Item.iImage     = 0;
   Item.iItem      = idx;
   Item.iSubItem   = 0;
   Item.pszText    = NULL;
   Item.lParam     = (LPARAM) idx;
   return CListCtrl::InsertItem (&Item);
}

BOOL CSmtGridEx::InsertItem (int idx, int image)
{
   LV_ITEM Item;


   Item.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
   Item.state      = 0; 
   Item.stateMask  = 0; 
   Item.iImage     = image;
   Item.iItem      = idx;
   Item.iSubItem   = 0;
   Item.pszText    = NULL;
   Item.lParam     = (LPARAM) idx;
   return CListCtrl::InsertItem (&Item);
}

BOOL CSmtGridEx::SetItemText (LPTSTR Txt, int idx, int pos)
{
   return CListCtrl::SetItemText (idx, pos, Txt);
}


// CSmtGridEx-Meldungshandler
