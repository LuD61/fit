#pragma once
#include "smtgridex.h"
#include "afxcmn.h"
#include "CtrlGridColor.h"
#include "VHLabel.h"


// CSumView-Formularansicht

class CSumView : public CFormView
{
	DECLARE_DYNCREATE(CSumView)

protected:
	CSumView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CSumView();

public:
	enum { IDD = IDD_SUM_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate ();
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()
private:
	CLabel m_Title;
	CSmtGridEx m_SmtGrid;
	CCtrlGridColor CtrlGrid;
	HBRUSH dlgBrush;
	HBRUSH tableBrush;
	COLORREF DlgBkColor;
	COLORREF m_TableBkColor;
	BOOL BkColorSet;
	CFont m_TitleFont;
public:
	short RefreshGrid ();
};


