#pragma once

class CBkBitmap
{
public:
	enum BK_TYPE
	{
		DynamicMetal,
		DynamicBlue,
		DynamicDarkBlue,
		DynamicGrey,
	};

	enum DIRECTION
	{
		Horizontal,
		Vertical,
	};

private:
	BK_TYPE m_BkType;
	DIRECTION m_Direction;
	CBitmap m_Bitmap;
	int m_Width;
	int m_Height;
	UINT m_Planes;
	int parts;

public:
	void SetBkType (BK_TYPE BkType)
	{
		m_BkType = BkType;
	}

	void SetDirection (DIRECTION Direction)
	{
		m_Direction = Direction;
	}

	void SetWidth (int Width)
	{
		m_Width = Width;
	}

	void SetHeight (int Height)
	{
		m_Height = Height;
	}

	void SetPlanes (int Planes)
	{
		m_Planes = Planes;
	}

	CBkBitmap(void);
	~CBkBitmap(void);

	CBitmap* Create ();
	virtual void FillVRectParts (CDC &cDC, COLORREF color, CRect rect);
	virtual void FillRectParts (CDC &cDC, COLORREF color, CRect rect);
};
