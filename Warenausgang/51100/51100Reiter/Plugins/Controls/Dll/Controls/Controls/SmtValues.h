#pragma once

#define NAME_LENGTH 80

class CSmtValues
{
private:
	char m_Name[NAME_LENGTH];
	double m_Amount;
	double m_Weight;
	double m_Liq;
	double m_SmtPart;
public:
	LPSTR Name ()
	{
		return m_Name;
	}
	void set_Name (LPSTR name)
	{
		strcpy (m_Name,  name);
	}

	double Amount ()
	{
		return m_Amount;
	}
	void set_Amount (double amount)
	{
		m_Amount = amount;
	}

	double Weight ()
	{
		return m_Weight;
	}
	void set_Weight (double weight)
	{
		m_Weight = weight;
	}

	double Liq ()
	{
		return m_Liq;
	}
	void set_Liq (double liq)
	{
		m_Liq = liq;
	}

	double SmtPart ()
	{
		return m_SmtPart;
	}
	void set_SmtPart (double smtPart)
	{
		m_SmtPart = smtPart;
	}

	CSmtValues(void);
	~CSmtValues(void);
};
