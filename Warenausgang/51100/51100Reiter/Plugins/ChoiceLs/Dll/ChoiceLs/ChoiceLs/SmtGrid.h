#pragma once
#include "choicex.h"
#include "smttable.h"

class CSmtGrid :
	public CChoiceX
{
private:
	CSmtTable * m_SmtTable;
public:
	void set_SmtTable (CSmtTable *table)
	{
		m_SmtTable = table;
	}

	CSmtGrid(CWnd* pParent = NULL);
public:
	~CSmtGrid(void);
    virtual void FillList (void);
};
