#include "StdAfx.h"
#include "ctrlgrid.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CCtrlGrid::CCtrlGrid(void)
{
	Dlg = NULL;
	rows = 1;
	columns = 1;
	RowSpace = 5;
	ColSpace = 5;
	xBorder = 0;
	yBorder = 0;
	cxBorder = 0;
	cyBorder = 0;
	CellHeight = 0;
	pcx = 0;
	pcy = 0;
	Font = NULL;
	ColStart.Init ();
	DlgSize = NULL;
}

CCtrlGrid::~CCtrlGrid(void)
{
	DestroyAll ();
}

void CCtrlGrid::DestroyGrid ()
{
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->Grid != NULL)
		{
			CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
			delete Grid;
		}
		delete control;
	}
}

void CCtrlGrid::Create (CWnd *Dlg, int rows, int columns)
{
	this->Dlg = Dlg;
	this->rows = rows;
	this->columns = columns;
}

void CCtrlGrid::Display ()
{
	Move (0, 0);
}

void CCtrlGrid::Move (int x, int y)
{
	if (Dlg == NULL)
	{
		return;
	}
	MoveGridControls (x, y);
	if (Font == NULL)
	{
		Font = Dlg->GetFont ();
	}
	CDC *cDc = Dlg->GetDC ();
    cDc->SelectObject (Font);
	TEXTMETRIC tm;
	cDc->GetTextMetrics (&tm);
	Dlg->ReleaseDC (cDc);
	if (CellHeight == 0) CellHeight = tm.tmHeight;
	ColStart.Init ();
	for (int i = 0; i < columns; i ++)
	{
		int pos = Calculate (i) + x;
		ColStart.Add ((void *) (UINT_PTR) pos);
		SetGridX (i, pos, &tm, y);
	}

	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->gridx == DOCKRIGHT)
		{
			if (pcx == 0 && pcy == 0)
			{
				CRect pRect;
				if (DlgSize == NULL)
				{
					Dlg->GetClientRect (&pRect);
				}
				else
				{
					pRect = *DlgSize;
				}
				pcx = pRect.right;
				pcy = pRect.bottom;
			}
		    CRect cRect;
		    CRect wRect;
			if (control->Grid != NULL)
			{
				CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
				Grid->GetGridRect (&wRect);
			}
			else
			{
//				control->cWnd->GetWindowRect (&wRect);
				control->GetWindowRect (wRect);
			}
		    cRect.right = wRect.right - wRect.left;
		    cRect.bottom = wRect.bottom - wRect.top;
		    Dlg->ScreenToClient (&wRect);
			wRect.left = pcx - (wRect.right - wRect.left);
			wRect.left -= xBorder;
		    wRect.top = control->gridy * (CellHeight + RowSpace) 
			            + yBorder + control->yplus + y;
	        wRect.right = wRect.left + cRect.right;
		    if (control->gridcy == DOCKBOTTOM)
		    {
                     cRect.bottom = pcy - wRect.top - CellHeight;
		    }
		    wRect.bottom = wRect.top + cRect.bottom;
			if (control->Grid != NULL)
			{
				CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
				Grid->MoveGrid (&wRect);
			}
			else
			{
				control->cWnd->MoveWindow (&wRect, TRUE);
			}

		}
		if (control->BottomControl != NULL)
		{
				control->SetBottom ();
		}
	}
	TestRightDockControl ();
}

int CCtrlGrid::Calculate (int col)
{
	if (col == 0)
	{
		return xBorder;
	}

	int pos = ((int) (UINT_PTR) ColStart.Get (col - 1)) + ColSpace;
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
//		if (control->gridx >= col) continue;
//		for (int i = col - 1; i >= 0; i --)
		{
			if (control->gridcx == DOCKRIGHT) continue;
			if ((control->gridx + control->gridcx) != col)
			{
				continue;
			}
            CRect cRect;
			if (control->Grid != NULL)
			{
				CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
				Grid->GetGridRect (&cRect);
			}
			else
			{
//				control->cWnd->GetWindowRect (&cRect);
				control->GetWindowRect (cRect);
			}
			Dlg->ScreenToClient (&cRect);
//			if ((cRect.left + cRect.right) > pos)
			if (cRect.right > pos)
			{
				pos = cRect.right + ColSpace;
		}
		}
	}
	return pos;
}

void CCtrlGrid::SetGridX (int col, int pos, TEXTMETRIC *tm, int y)
{
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->gridx != col) continue;
		CRect cRect;
		CRect wRect;
		if (control->Grid != NULL)
		{
			CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
			Grid->GetGridRect (&wRect);
		}
		else
		{
//			control->cWnd->GetWindowRect (&wRect);
			control->GetWindowRect (wRect);
		}
		cRect.right = wRect.right - wRect.left;
		cRect.bottom = wRect.bottom - wRect.top;
		Dlg->ScreenToClient (&wRect);
//		wRect.left = pos + xBorder + control->xplus;
		wRect.left = pos + control->xplus;


		if (control->gridy == DOCKBOTTOM)
		{
				CRect pRect;
				if (DlgSize == NULL)
				{
					Dlg->GetClientRect (&pRect);
				}
				else
				{
					pRect = *DlgSize;
				}
				wRect.top = pRect.bottom - cRect.bottom - yBorder - 
					        control->cyplus;
		}
		else
		{
				wRect.top = control->gridy * (CellHeight + RowSpace) 
					   + yBorder + control->yplus + y;
		}

		if (control->gridcx == DOCKSIZE)
		{
			if (pcx == 0 && pcy == 0)
			{
				CRect pRect;
				if (DlgSize == NULL)
				{
					Dlg->GetClientRect (&pRect);
				}
				else
				{
					pRect = *DlgSize;
				}
				pcx = pRect.right;
				pcy = pRect.bottom;
			}
			int space = wRect.left;
			cRect.right = pcx - 2 * space;
			cRect.right -= control->rightspace;
		}
		else if (control->gridcx == DOCKRIGHT)
		{
//			if (pcx == 0 && pcy == 0)
			{
				CRect pRect;
				if (DlgSize == NULL)
				{
					Dlg->GetClientRect (&pRect);
				}
				else
				{
					pRect = *DlgSize;
				}
				pcx = pRect.right - cxBorder;
				pcy = pRect.bottom;
			}
			int space = wRect.left;
			cRect.right = pcx;
			cRect.right -= control->rightspace;
			cRect.right -= wRect.left;
		}
		if (control->gridcy == DOCKBOTTOM)
		{
			if (pcx == 0 && pcy == 0)
			{
				CRect pRect;
				if (DlgSize == NULL)
				{
					Dlg->GetClientRect (&pRect);
				}
				else
				{
					pRect = *DlgSize;
				}
				pcx = pRect.right;
				pcy = pRect.bottom;
			}
//            cRect.bottom = pcy - wRect.top - CellHeight;
			cRect.bottom = pcy - wRect.top - cyBorder - control->BottomSpace;
		}

        wRect.right = wRect.left + cRect.right;
		wRect.bottom = wRect.top + cRect.bottom;
		if (control != NULL && control->cWnd != NULL &&
			control->cWnd->IsKindOf (RUNTIME_CLASS (CComboBox)))
		{
			wRect.bottom = wRect.top + 100;
		}
		if (control->Grid != NULL)
		{
			CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
			Grid->MoveGrid (&wRect);
		}
		else
		{
				control->cWnd->MoveWindow (&wRect, TRUE);
		}
	}
}

void CCtrlGrid::SetBorder (int xBorder, int yBorder)
{
	this->xBorder = xBorder;
	this->yBorder = yBorder;
	cxBorder = xBorder;
	cyBorder = yBorder;
}

void CCtrlGrid::SetBorder (int xBorder, int yBorder, int cxBorder, int cyBorder)
{
	this->xBorder = xBorder;
	this->yBorder = yBorder;
	this->cxBorder = cxBorder;
	this->cyBorder = cyBorder;
}

void CCtrlGrid::SetGridSpace (int ColSpace, int RowSpace)
{
	this->ColSpace = ColSpace;
	this->RowSpace = RowSpace;
}

void CCtrlGrid::SetCellHeight (int CellHeight)
{
	this->CellHeight = CellHeight;
}

void CCtrlGrid::SetFontCellHeight (CWnd *cWnd)
{
	CFont *font = cWnd->GetFont ();
	CDC *cDc = cWnd->GetDC ();
	CFont *oldFont = cDc->SelectObject (font);
	TEXTMETRIC tm;
	cDc->GetTextMetrics (&tm);
	cDc->SelectObject (oldFont);
	cWnd->ReleaseDC (cDc);
	int Height = tm.tmHeight + tm.tmHeight / 3;
	SetCellHeight (Height);
}

void CCtrlGrid::SetFontCellHeight (CWnd *cWnd, CFont *font)
{
	LOGFONT lf;
	CFont cFont;
	font->GetLogFont (&lf);
	cFont.CreateFontIndirect (&lf);
	CDC *cDc = cWnd->GetDC ();
	CFont *oldFont = cDc->SelectObject (&cFont);
	TEXTMETRIC tm;
	cDc->GetTextMetrics (&tm);
	cDc->SelectObject (oldFont);
	cWnd->ReleaseDC (cDc);
	int Height = tm.tmHeight;
//	int Height = tm.tmHeight + tm.tmHeight / 3;
	SetCellHeight (Height);
}

void CCtrlGrid::SetFontCellHeight (CWnd *cWnd, int plus)
{
	CFont *font = cWnd->GetFont ();
	CDC *cDc = cWnd->GetDC ();
	CFont *oldFont = cDc->SelectObject (font);
	TEXTMETRIC tm;
	cDc->GetTextMetrics (&tm);
	cDc->SelectObject (oldFont);
	cWnd->ReleaseDC (cDc);
	SetCellHeight (tm.tmHeight + plus);
}

void CCtrlGrid::SetParentMetrics (int pcx, int pcy)
{
	this->pcx = pcx;
	this->pcy = pcy;
}


void CCtrlGrid::TestRightDockControl ()
{
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->gridcx == DOCKSIZE && 
			control->RightDockControl != NULL)
		{
			CRect dRect;
			CRect cRect;
			CRect wRect;
			control->RightDockControl->GetWindowRect (&dRect);
			Dlg->ScreenToClient (&dRect);
			control->cWnd->GetWindowRect (&wRect);
			cRect.right = dRect.left;
			if (cRect.right < wRect.left) continue;
			cRect.bottom = wRect.bottom - wRect.top;
			Dlg->ScreenToClient (&wRect);
			int space = wRect.left;
			wRect.right = dRect.left;
		    wRect.right -= (ColSpace + control->rightspace);
			control->cWnd->MoveWindow (&wRect, TRUE);
		}
	}
}

void CCtrlGrid::CreateChoiceButton (CButton& Button, int Id, CWnd *Parent)
{
	TEXTMETRIC tm;
    CDC *cDC = Parent->GetDC ();
	cDC->SelectObject (Parent->GetFont ());
	cDC->GetTextMetrics (&tm);
	Parent->ReleaseDC (cDC);

	int bsize = GetSystemMetrics (SM_CYBORDER);
    int cWidth = tm.tmAveCharWidth;
	int cHeight = tm.tmHeight;
    int hPoint = (int) (double) ((double) cHeight / 8 + 0.9); 
    cHeight += 4 * bsize;
    cHeight = hPoint * 8 + 4 * bsize;
	int wPoint = (int) (double) ((double) cWidth / 4 + 0.9); 
	cWidth = wPoint * 4 + 2;

	CRect Rect;
	Rect.left = 0;
	Rect.top = 0;
	Rect.right = 2 * cWidth;
	Rect.bottom = cHeight;
	Button.Create (_T(".."),
		           BS_PUSHBUTTON | BS_CENTER | 
				   BS_TEXT | BS_BOTTOM | WS_VISIBLE | WS_CHILD,
		           Rect, Parent, Id);
	Button.ModifyStyle (WS_TABSTOP, 0);
}


void CCtrlGrid::CreateColorChoiceButton (CColorButton& Button, int Id, CWnd *Parent)
{
	TEXTMETRIC tm;
    CDC *cDC = Parent->GetDC ();
	cDC->SelectObject (Parent->GetFont ());
	cDC->GetTextMetrics (&tm);
	Parent->ReleaseDC (cDC);

	int bsize = GetSystemMetrics (SM_CYBORDER);
    int cWidth = tm.tmAveCharWidth;
	int cHeight = tm.tmHeight;
    int hPoint = (int) (double) ((double) cHeight / 8 + 0.9); 
    cHeight += 4 * bsize;
    cHeight = hPoint * 8 + 4 * bsize;
	int wPoint = (int) (double) ((double) cWidth / 4 + 0.9); 
	cWidth = wPoint * 4 + 2;

	CRect Rect;
	Rect.left = 0;
	Rect.top = 0;
	Rect.right = 2 * cWidth;
	Rect.bottom = cHeight;
	Button.Create (_T(".."), WS_CHILD | WS_VISIBLE, Rect,
							   Parent, Id);

	Button.ModifyStyle (WS_TABSTOP, 0);

	COLORREF DynButtonColor = RGB (192, 192, 192);

	Button.nID = Id;
	Button.Orientation = Button.Center;
	Button.TextStyle = Button.Standard;
	Button.BorderStyle = Button.Solide;
	Button.DynamicColor = TRUE;
	Button.TextColor =RGB (0, 0, 0);
	Button.SetBkColor (DynButtonColor);
	Button.DynColor = DynButtonColor;
	Button.RolloverColor = RGB (192, 192, 192);
	Button.SetWindowText (_T(".."));
//	Button.LoadBitmap (IDB_POS_TEXT);
//	Button.LoadMask (IDB_POS_TEXT_MASK);
//	Button.SetFont (&Font);
}

void CCtrlGrid::CreateCheckBox (CButton& Button, int Id, CWnd *Parent)
{
	TEXTMETRIC tm;
    CDC *cDC = Parent->GetDC ();
	cDC->SelectObject (Parent->GetFont ());
	cDC->GetTextMetrics (&tm);
	Parent->ReleaseDC (cDC);

	int bsize = GetSystemMetrics (SM_CYBORDER);
    int cWidth = tm.tmAveCharWidth;
	int cHeight = tm.tmHeight;
    int hPoint = (int) (double) ((double) cHeight / 8 + 0.9); 
    cHeight += 4 * bsize;
    cHeight = hPoint * 8 + 4 * bsize;
	int wPoint = (int) (double) ((double) cWidth / 4 + 0.9); 
	cWidth = wPoint * 4 + 2;

	CRect Rect;
	Rect.left = 0;
	Rect.top = 0;
	Rect.right = 2 * cWidth;
	Rect.bottom = cHeight;
	Button.Create (_T(".."),
		           BS_PUSHBUTTON | BS_CENTER | 
				   BS_TEXT | BS_CHECKBOX | WS_VISIBLE | WS_CHILD,
		           Rect, Parent, Id);
	Button.ModifyStyle (WS_TABSTOP, 0);
}

void CCtrlGrid::SetFont (CFont *font)
{
	int i = 0;
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->Grid != NULL)
		{
			CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
			Grid->SetFont (font);
		}
		else
		{
			control->SetFont (font);
		}
		i ++;
	}
	this->Font = font;
}

void CCtrlGrid::GetGridRect (CRect *rect)
{
    BOOL First = TRUE;
	CRect crect;
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->Grid != NULL)
		{
			CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
            Grid->GetGridRect (&crect);
		}
		else
		{
			control->cWnd->GetWindowRect (&crect);
		}

		if (First)
		{
			rect->top    = crect.top;
			rect->left   = crect.left;
			rect->bottom = crect.bottom;
			rect->right  = crect.right;
			First = FALSE;
		}
		else
		{
			if (crect.top < rect->top)
			{
				rect->top = crect.top;
			}
			if (crect.left < rect->left)
			{
				rect->left = crect.left;
			}
			if (crect.bottom > rect->bottom)
			{
				rect->bottom = crect.bottom;
			}
			if (crect.right > rect->right)
			{
				rect->right = crect.right;
			}
		}
	}
}
			
void CCtrlGrid::MoveGrid (CRect *rect)
{
    BOOL First = TRUE;
	CRect crect;
	CRect wrect;
    GetGridRect (&crect);
	Dlg->ScreenToClient (&crect);
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->Grid != NULL)
		{
			CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
			Grid->GetGridRect (&wrect);
			Dlg->ScreenToClient (&wrect);
			wrect.left += rect->left - crect.left;
			wrect.top  += rect->top - crect.top;
			wrect.right += rect->right - crect.right;
			wrect.bottom  += rect->bottom - crect.bottom;
            Grid->MoveGrid (&wrect);
		}
		else
		{
			control->cWnd->GetWindowRect (&wrect);
			Dlg->ScreenToClient (&wrect);
			wrect.left += rect->left - crect.left;
			wrect.top  += rect->top - crect.top;
			wrect.right += rect->right - crect.right;
			wrect.bottom  += rect->bottom - crect.bottom;
            control->cWnd->MoveWindow (&wrect, TRUE);
		}
	}
}

void CCtrlGrid::MoveGridControls (int x, int y)
{
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->Grid != NULL)
		{
			CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
			Grid->DlgSize = DlgSize;
			Grid->Move (x,y);
		}
	}
}

void CCtrlGrid::DockControlWidth (CWnd *Parent, CWnd *Source, CWnd *Target)
{
	CRect sRect;
	CRect tRect;

	Source->GetWindowRect (&sRect);
	Target->GetWindowRect (&tRect);
	tRect.right = sRect.right;
	Parent->ScreenToClient (&tRect);
	Target->MoveWindow (&tRect); 
}

void CCtrlGrid::DockControlHeight (CWnd *Parent, CWnd *Source, CWnd *Target)
{
	CRect sRect;
	CRect tRect;

	Source->GetWindowRect (&sRect);
	Target->GetWindowRect (&tRect);
	tRect.bottom = tRect.top + sRect.bottom - sRect.top;
	Parent->ScreenToClient (&tRect);
	Target->MoveWindow (&tRect); 
}

void CCtrlGrid::SetItemCharHeight (CWnd *Item, int Height)
{
	CRect crect;
	CRect wrect;
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->cWnd == Item)
		{
			control->cWnd->GetWindowRect (&wrect);
	        Dlg->ScreenToClient (&wrect);
			wrect.bottom  = wrect.top + Height * (CellHeight + RowSpace);
            control->cWnd->MoveWindow (&wrect);
		}
	}
}

void CCtrlGrid::SetItemLogHeight (CWnd *Item, int Height)
{
	CFont *font = Item->GetFont ();
	CDC *cDc = Item->GetDC ();
	CFont *oldFont = cDc->SelectObject (font);
	TEXTMETRIC tm;
	cDc->GetTextMetrics (&tm);
	cDc->SelectObject (oldFont);
	Item->ReleaseDC (cDc);
	int unit = tm.tmHeight / 4;
	Height *= unit;
	CRect wrect;
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->cWnd == Item)
		{
			control->cWnd->GetWindowRect (&wrect);
	        Dlg->ScreenToClient (&wrect);
			wrect.bottom  = wrect.top + Height;
            control->cWnd->MoveWindow (&wrect);
		}
	}
}

void CCtrlGrid::SetItemPixelHeight (CWnd *Item, int Height)
{
	CRect wrect;
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->cWnd == Item)
		{
			control->cWnd->GetWindowRect (&wrect);
	        Dlg->ScreenToClient (&wrect);
			wrect.bottom  = wrect.top + Height;
            control->cWnd->MoveWindow (&wrect);
		}
	}
}

void CCtrlGrid::SetItemCharWidth (CWnd *Item, int Width)
{
	CFont *font = Item->GetFont ();
	CDC *cDc = Item->GetDC ();
	CFont *oldFont = cDc->SelectObject (font);
	TEXTMETRIC tm;
	cDc->GetTextMetrics (&tm);
	CSize Size = cDc->GetTextExtent (_T("X"), 1);
	cDc->SelectObject (oldFont);
	Item->ReleaseDC (cDc);
    Width *= Size.cx;
	CRect crect;
	CRect wrect;
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->cWnd == Item)
		{
			control->cWnd->GetWindowRect (&wrect);
	        Dlg->ScreenToClient (&wrect);
			wrect.right  = wrect.left + Width;
            control->cWnd->MoveWindow (&wrect);
		}
	}
}

void CCtrlGrid::SetItemLogWidth (CWnd *Item, int Width)
{
	CFont *font = Item->GetFont ();
	CDC *cDc = Item->GetDC ();
	CFont *oldFont = cDc->SelectObject (font);
	TEXTMETRIC tm;
	cDc->GetTextMetrics (&tm);
	CSize Size = cDc->GetTextExtent (_T("X"), 1);
	cDc->SelectObject (oldFont);
	Item->ReleaseDC (cDc);
	Size.cx /= 8;
	Size.cx = (Size.cx > 0) ? Size.cx : 1;
    Width *= Size.cx;
	CRect crect;
	CRect wrect;
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->cWnd == Item)
		{
			control->cWnd->GetWindowRect (&wrect);
	        Dlg->ScreenToClient (&wrect);
			wrect.right  = wrect.left + Width;
            control->cWnd->MoveWindow (&wrect);
		}
	}
}

void CCtrlGrid::SetItemPixelWidth (CWnd *Item, int Width)
{
	CRect crect;
	CRect wrect;
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->cWnd == Item)
		{
			control->cWnd->GetWindowRect (&wrect);
	        Dlg->ScreenToClient (&wrect);
			wrect.right  = wrect.left + Width;
            control->cWnd->MoveWindow (&wrect);
		}
	}
}

void CCtrlGrid::SetNewStyle (BOOL style)
{
	if (CCtrlInfo::NewStyle != style)
	{
		CCtrlInfo::NewStyle = style;
	}
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->Grid != NULL)
		{
			CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
			Grid->SetNewStyle (style);
		}
		else
		{
			control->SetNewStyle ();
		}
	}
}

void CCtrlGrid::Enable (BOOL enable)
{
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->Grid != NULL)
		{
			CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
			Grid->Enable (enable);
		}
		else if (control->cWnd != NULL)
		{
			control->cWnd->EnableWindow (enable);
		}
	}
}

void CCtrlGrid::SetVisible (BOOL v)
{
	WORD visible = SW_SHOWNORMAL;
	if (!v)
	{
		visible = SW_HIDE;
	}
	FirstPosition ();
	CCtrlInfo *control;
	while ((control = (CCtrlInfo *) GetNext ()) != NULL)
	{
		if (control->Grid != NULL)
		{
			CCtrlGrid *Grid = (CCtrlGrid *) control->Grid;
			Grid->SetVisible (v);
		}
		else if (control->cWnd != NULL)
		{
			control->cWnd->ShowWindow (visible);
		}
	}
}
