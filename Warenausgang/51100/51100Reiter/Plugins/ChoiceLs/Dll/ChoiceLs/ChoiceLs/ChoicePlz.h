#ifndef _CHOICEPLZ_DEF
#define _CHOICEPLZ_DEF

#include "ChoiceX.h"
#include "PlzList.h"
#include <vector>

class CChoicePlz : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
        CString query; 
      
    public :
		long m_Plz;
	    std::vector<CPlzList *> PlzList;
        void SetQuery (LPTSTR query)
        {
             this->query = query;
        }
      	CChoicePlz(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoicePlz(); 
//        virtual void BezLabel (CListCtrl *);
//        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
//        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchCol (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CPlzList *GetSelectedText ();
		void DestroyList ();
	    virtual void OnFilter ();
};
#endif
