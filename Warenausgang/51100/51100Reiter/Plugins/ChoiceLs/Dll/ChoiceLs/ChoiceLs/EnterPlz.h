#pragma once

#include "plz.h"
#include "ChoicePlz.h"
#include <vector>

class CEnterPlz
{
private:
	int cursor_ort;
	int cursor_vorwahl;
	BOOL found;
	CString LastQuery;
	CChoicePlz *ChoicePlz;
public:
	PLZ_CLASS Plz;
	BOOL Found ()
	{
		return found;
	}
	CEnterPlz(void);
	~CEnterPlz(void);
	CPlzList* Choice (PLZ_CLASS& Plz);
	PLZ_CLASS& GetPlzFromPlz (LPTSTR plz);
	PLZ_CLASS& GetPlzFromOrt (LPTSTR ort);
	PLZ_CLASS& GetPlzFromVorwahl (LPTSTR vorwahl);
	static int GetComboIdx (LPTSTR value, std::vector<CString *> Values);
};
