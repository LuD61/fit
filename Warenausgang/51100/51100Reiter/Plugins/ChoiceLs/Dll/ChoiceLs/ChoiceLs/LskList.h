#ifndef _LSK_LIST_DEF
#define _LSK_LIST_DEF
#pragma once

class CLskList
{
public:
	short mdn;
	long ls;
	long auf;
	long kun;
	TCHAR lieferdat[12];
	short ls_stat;
	long inka_nr;
	long tou_nr;
	short ls_tsat;
	TCHAR hinweis[49];
	TCHAR kun_krz1[17];
	TCHAR kun_bran2[3];

	CLskList(void);
	CLskList(short mdn, long ls, long auf, long kun, DATE_STRUCT lieferdat, short ls_stat,
		     LPTSTR kun_krz1, LPTSTR kun_bran2);
	CLskList(short mdn, long ls, long auf, long kun, TCHAR lieferdat[], short ls_stat,
		     LPTSTR kun_krz1, LPTSTR kun_bran2);
	~CLskList(void);
	void SetLieferdat (LPTSTR p);
	void SetLieferdat (CString& ld);
	void SetKunKrz1 (LPTSTR kun_krz1);
	void SetKunBran2 (LPTSTR kun_bran2);
	void SetKunKrz1 (CString kun_krz1);
	void SetKunBran2 (CString kun_bran2);
	void SetHinweis (LPTSTR hinweis);
	void SetHinweis (CString hinweis);
};
#endif
