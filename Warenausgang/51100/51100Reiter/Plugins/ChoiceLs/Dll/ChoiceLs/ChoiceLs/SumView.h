#pragma once
#include "smtgridex.h"
#include "afxcmn.h"
#include "CtrlGridColor.h"



// CSumView-Formularansicht

class CSumView : public CFormView
{
	DECLARE_DYNCREATE(CSumView)

protected:
	CSumView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CSumView();

public:
	enum { IDD = IDD_SUM_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate ();
	virtual void OnSize (UINT, int, int);

	DECLARE_MESSAGE_MAP()
private:
	CSmtGridEx m_SmtGrid;
	CCtrlGridColor CtrlGrid;
};


