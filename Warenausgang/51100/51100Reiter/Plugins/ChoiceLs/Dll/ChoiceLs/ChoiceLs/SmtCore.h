#pragma once
#include "smttable.h"

class CSmtCore
{
private:
	static CSmtCore *m_Instance;
    CSmtTable *m_SmtTable;
protected:
	CSmtCore(void);
	~CSmtCore(void);
public:
	static CSmtCore *GetInstance ();
	static void DestroyInstance ();
	void set_SmtTable (CSmtTable *smtTable)
	{
		m_SmtTable = smtTable;
	}

    CSmtTable *SmtTable ()
	{
		return m_SmtTable;
	}
};

#define SMT_CORE CSmtCore::GetInstance ()
