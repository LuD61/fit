#include "StdAfx.h"
#include "ctrlline.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNAMIC(CCtrlLine, CCtrlInfo)

CCtrlLine::CCtrlLine(CWnd *pParent, int Direction, int width, int pWidth, int gridx, int gridy, 
 		             int gridcx, int gridcy)
{
	if (Direction == HORIZONTAL)
	{
		Line.Create (NULL, WS_CHILD | WS_VISIBLE | SS_OWNERDRAW, 
					CRect (0, 0, width, pWidth),
					pParent);
	}
	else
	{
		Line.Create (NULL, WS_CHILD | WS_VISIBLE | SS_OWNERDRAW, 
					CRect (0, 0, pWidth, width),
					pParent);
	}
	this->cWnd = &Line;
	this->Direction = Direction;
	Line.Direction = Direction;
	xplus = 0;
	yplus = 0;
	cxplus = 0;
	cyplus = 0;
	SetPos (gridx, gridy, gridcx, gridcy);
	rightspace = 0;
	RightDockControl = NULL;
	Grid = NULL;
	SetWidth ();
}

CCtrlLine::~CCtrlLine(void)
{
}

