create table "fit".cfg_doc 
  (
    programm char (20),
    item char (20),
    text char(256),
    values char (512),
    help char(512)
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".cfg_doc from "public";

create unique index "fit".i01cfg_doc on "fit".cfg_doc (programm,item);




