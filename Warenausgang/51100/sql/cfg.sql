create table "fit".cfg 
  (
    programm char (20),
    kunde char (16), 
    pers_nam char(8),
    item char (20),
    wert char (20),
    datum date,
    computername char(64)
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".cfg from "public";

create unique index "fit".i01cfg on "fit".cfg (programm,kunde,pers_nam,computername,item);




