#ifndef _TMP_CATERING_DEF
#define __TMP_CATERING_DEF
#include "dbclass.h"

struct TMP_CATERING {
   short     mdn;
   short     fil;
   long      ls;
   long      auf;
   double    a;
   long      posi;
   double    auf_me;
   char      auf_me_bz[7];
   long      lsp_txt;
   long      kun;
   short     teil_smt;
   double    a_lsp;
   short     prdk_typ;
   short     extra;
   short     sort;
};
extern struct TMP_CATERING tmp_catering, tmp_catering_null;

#line 6 "tmp_catering.rh"

class CTMP_CATERING : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               CTMP_CATERING () : DB_CLASS ()
               {
               }
};
#endif
