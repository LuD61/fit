#include <windows.h>
#include "tmp_catering.h"

struct TMP_CATERING tmp_catering, tmp_catering_null;

void CTMP_CATERING::prepare (void)
{
            char *sqltext;

            sqlin ((long *)   &tmp_catering.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_catering.fil,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_catering.ls,   SQLLONG, 0);
            sqlin ((long *)   &tmp_catering.auf,  SQLLONG, 0);
            sqlin ((long *)   &tmp_catering.a,  SQLDOUBLE, 0);
            sqlin ((long *)   &tmp_catering.posi,  SQLSHORT, 0);
    out_quest ((char *) &tmp_catering.mdn,1,0);
    out_quest ((char *) &tmp_catering.fil,1,0);
    out_quest ((char *) &tmp_catering.ls,2,0);
    out_quest ((char *) &tmp_catering.auf,2,0);
    out_quest ((char *) &tmp_catering.a,3,0);
    out_quest ((char *) &tmp_catering.posi,2,0);
    out_quest ((char *) &tmp_catering.auf_me,3,0);
    out_quest ((char *) tmp_catering.auf_me_bz,0,7);
    out_quest ((char *) &tmp_catering.lsp_txt,2,0);
    out_quest ((char *) &tmp_catering.kun,2,0);
    out_quest ((char *) &tmp_catering.teil_smt,1,0);
    out_quest ((char *) &tmp_catering.a_lsp,3,0);
    out_quest ((char *) &tmp_catering.prdk_typ,1,0);
    out_quest ((char *) &tmp_catering.extra,1,0);
    out_quest ((char *) &tmp_catering.sort,1,0);
            cursor = sqlcursor ("select tmp_catering.mdn,  "
"tmp_catering.fil,  tmp_catering.ls,  tmp_catering.auf,  "
"tmp_catering.a,  tmp_catering.posi,  tmp_catering.auf_me,  "
"tmp_catering.auf_me_bz,  tmp_catering.lsp_txt,  tmp_catering.kun,  "
"tmp_catering.teil_smt,  tmp_catering.a_lsp,  tmp_catering.prdk_typ,  "
"tmp_catering.extra,  tmp_catering.sort from tmp_catering "

#line 17 "tmp_catering.rpp"
                                  "where mdn = ? and fil = ? and ls = ? and auf = ? and a = ? and posi = ? ");
    ins_quest ((char *) &tmp_catering.mdn,1,0);
    ins_quest ((char *) &tmp_catering.fil,1,0);
    ins_quest ((char *) &tmp_catering.ls,2,0);
    ins_quest ((char *) &tmp_catering.auf,2,0);
    ins_quest ((char *) &tmp_catering.a,3,0);
    ins_quest ((char *) &tmp_catering.posi,2,0);
    ins_quest ((char *) &tmp_catering.auf_me,3,0);
    ins_quest ((char *) tmp_catering.auf_me_bz,0,7);
    ins_quest ((char *) &tmp_catering.lsp_txt,2,0);
    ins_quest ((char *) &tmp_catering.kun,2,0);
    ins_quest ((char *) &tmp_catering.teil_smt,1,0);
    ins_quest ((char *) &tmp_catering.a_lsp,3,0);
    ins_quest ((char *) &tmp_catering.prdk_typ,1,0);
    ins_quest ((char *) &tmp_catering.extra,1,0);
    ins_quest ((char *) &tmp_catering.sort,1,0);
            sqltext = "update tmp_catering set "
"tmp_catering.mdn = ?,  tmp_catering.fil = ?,  tmp_catering.ls = ?,  "
"tmp_catering.auf = ?,  tmp_catering.a = ?,  tmp_catering.posi = ?,  "
"tmp_catering.auf_me = ?,  tmp_catering.auf_me_bz = ?,  "
"tmp_catering.lsp_txt = ?,  tmp_catering.kun = ?,  "
"tmp_catering.teil_smt = ?,  tmp_catering.a_lsp = ?,  "
"tmp_catering.prdk_typ = ?,  tmp_catering.extra = ?,  "
"tmp_catering.sort = ? "

#line 19 "tmp_catering.rpp"
                                  "where mdn = ? and fil = ? and ls = ? and auf = ? and a = ? and posi = ? ";
            sqlin ((long *)   &tmp_catering.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_catering.fil,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_catering.ls,   SQLLONG, 0);
            sqlin ((long *)   &tmp_catering.auf,  SQLLONG, 0);
            sqlin ((long *)   &tmp_catering.a,  SQLDOUBLE, 0);
            sqlin ((long *)   &tmp_catering.posi,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &tmp_catering.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_catering.fil,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_catering.ls,   SQLLONG, 0);
            sqlin ((long *)   &tmp_catering.auf,  SQLLONG, 0);
            sqlin ((long *)   &tmp_catering.a,  SQLDOUBLE, 0);
            sqlin ((long *)   &tmp_catering.posi,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor ("select mdn from tmp_catering "
                                  "where mdn = ? and fil = ? and ls = ? and auf = ? and a = ? and posi = ? ");
            sqlin ((long *)   &tmp_catering.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_catering.fil,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_catering.ls,   SQLLONG, 0);
            sqlin ((long *)   &tmp_catering.auf,  SQLLONG, 0);
            sqlin ((long *)   &tmp_catering.a,  SQLDOUBLE, 0);
            sqlin ((long *)   &tmp_catering.posi,  SQLSHORT, 0);
            del_cursor = sqlcursor ("delete from tmp_catering "
                                  "where mdn = ? and fil = ? and ls = ? and auf = ? and a = ? and posi = ? ");
    ins_quest ((char *) &tmp_catering.mdn,1,0);
    ins_quest ((char *) &tmp_catering.fil,1,0);
    ins_quest ((char *) &tmp_catering.ls,2,0);
    ins_quest ((char *) &tmp_catering.auf,2,0);
    ins_quest ((char *) &tmp_catering.a,3,0);
    ins_quest ((char *) &tmp_catering.posi,2,0);
    ins_quest ((char *) &tmp_catering.auf_me,3,0);
    ins_quest ((char *) tmp_catering.auf_me_bz,0,7);
    ins_quest ((char *) &tmp_catering.lsp_txt,2,0);
    ins_quest ((char *) &tmp_catering.kun,2,0);
    ins_quest ((char *) &tmp_catering.teil_smt,1,0);
    ins_quest ((char *) &tmp_catering.a_lsp,3,0);
    ins_quest ((char *) &tmp_catering.prdk_typ,1,0);
    ins_quest ((char *) &tmp_catering.extra,1,0);
    ins_quest ((char *) &tmp_catering.sort,1,0);
            ins_cursor = sqlcursor ("insert into tmp_catering ("
"mdn,  fil,  ls,  auf,  a,  posi,  auf_me,  auf_me_bz,  lsp_txt,  kun,  teil_smt,  a_lsp,  "
"prdk_typ,  extra,  sort) "

#line 45 "tmp_catering.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?)"); 

#line 47 "tmp_catering.rpp"
}
