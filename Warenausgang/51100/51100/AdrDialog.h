// AdrDialog.h: Schnittstelle f�r die Klasse CAdrDialog.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ADRDIALOG_H__136685C9_5B09_4EEF_B695_D46ADAE488FE__INCLUDED_)
#define AFX_ADRDIALOG_H__136685C9_5B09_4EEF_B695_D46ADAE488FE__INCLUDED_
#include "Dialog.h"
#include "resrc1.h"
#include "kun.h"
#include "mdn.h"
#include "adr.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

using namespace std;

class CAdrDialog : 
	  public CDialog  
{
private:
	COLORREF m_DlgColor;
	HBRUSH   m_DlgBrush;
	COLORREF m_EditColor;
	HBRUSH   m_EditBrush;
	long     m_AdrNr;
	CAdr     m_Adr;
	vector<CDialogItem *> m_EditItems;

	BOOL IsEditItem (HWND hWnd);
public:
	enum {Id = IDD_ADR_DIALOG};
	void SetAdrNr (long AdrNr)
	{
		m_AdrNr = AdrNr;
	}

	CAdrDialog(HWND Parent=NULL);
	virtual ~CAdrDialog();
protected:
    void    ReadAdr ();
	virtual void AttachControls ();
	virtual BOOL OnF5 ();
	virtual BOOL OnInitDialog ();
	virtual BOOL PreTranslateMessage (MSG *msg);
	virtual HBRUSH OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor);
};

#endif // !defined(AFX_ADRDIALOG_H__136685C9_5B09_4EEF_B695_D46ADAE488FE__INCLUDED_)
