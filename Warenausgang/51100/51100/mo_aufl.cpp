//031012  wenn InfoProcess aktiv ist (Wgchart.jar)  , dann nach jeder �nderung (Menge, Preis in aufp zur�ckschreiben
//030611  Fehler -239 eleminiert trat auf, wenn im cfg PosSave > 0 , da beim L�schen einer Zeile nicht in der DB gel�scht wurde
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include "comcthlp.h"
#include "itemc.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_intp.h"
#include "listcl.h"
#include "inflib.h"
#include "mo_arg.h"
#include "mo_curso.h"
#include "ls.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "ptab.h"
#include "mo_aufl.h"
#include "mo_qa.h"
#include "mdn.h"
#include "fil.h"
#include "kun.h"
#include "a_kun.h"
#include "aktion.h"
#include "akt_krz.h"
#include "a_pr.h"
#include "mo_a_pr.h"
#include "mo_preis.h"
#include "sys_par.h"
#include "mo_einh.h"
#include "kumebest.h"
#include "mo_atxtl.h"
#include "mo_nr.h"
#include "best_bdf.h"
#include "best_res.h"
#include "stnd_auf.h"
#include "mo_stda.h"
#include "mo_menu.h"
#include "bsd_buch.h"
#include "tou.h"
#include "mo_progcfg.h"
#include "mo_smtg.h"
#include "aufkun.h"
#include "prov_satz.h"
#include "mo_choise.h"
#include "enterfunc.h"
#include "mo_kompl.h"
#include "aufpt.h"
#include "searchcharge.h"
#include "datum.h"
#include "aufp_txt.h"
#include "Text.h"
#include "Aufps.h"
#include "ag.h"
// #pragma comment(lib,"winmm.lib")

/*
#define MAXLEN 40
#define MAXPOS 5000
#define LPLUS 1
*/

#define MAXME 99999.99
#define MAXPR 9999.99

#define ESSIG 1

#define HNDW 1
#define EIG 2
#define EIG_DIV 3
#define KEYAUFSCHLAG 830

#define LEIHARTIKEL 12
#define DIENSTLEISTUNG 13

#define LISTHANDLER CListHandler::GetInstance ()

extern void WriteLogfile (char * format, ...);

char ProcessVerkFragen [100];
char InfoProcess [100];
char InfoSQL [1024];
int  InfoProcessRefresh = 0;
int F2Userdef1 = 0;

void EnableFullTax ();
void SetLsToFullTax ();
void SetLsToStandardTax ();
void SetToFullTax ();
void SetToStandardTax ();
void DisableFullTax ();

extern HANDLE  hMainInst;
extern HWND hWndMain;

static HANDLE ProcessID;
static HWND hMainWin;
static unsigned char ausgabebuffer [0x1000];
static HWND mamain1;
static HWND eWindow;
static HWND AufMehWnd = NULL;
static HWND AufMehWnd0 = NULL;
static HWND AufGewhWnd = NULL;
static HWND AufGewhWnd0 = NULL;
static HWND BasishWnd;
static PAINTSTRUCT aktpaint;

static COLORREF MessBkCol = DKYELLOWCOL;
static COLORREF MessCol   = BLACKCOL;
static COLORREF AktZeile_bColor   =    RGB (255, 255, 153); //WAL-70

static ITEM iOK ("", "     OK     ", "", 0);
static ITEM iCA ("", "  Abbrechen ", "", 0);
static ITEM ishow ("","..", "", 0);

static int ListFocus = 3;

static long akt_lager;
static int Sonderablauf = 0;

static int rab_prov_kz = 0;
static int auf_wert_anz = 0;
static int auf_gew_anz = 0;
static int a_kun_smt = 0;
static double auf_vk_pr;
static int preistest;
static double prproz_diff = 50.0;
static int  art_un_tst = 0;
static BOOL  add_me = FALSE;
static BOOL a_kum_par = 0;
static BOOL sacreate = NULL;
static HWND SaWindow = NULL;
static HWND PlusWindow = NULL;
static BOOL add;
static double aufme_old;
static BOOL testmeOK = FALSE;
static BOOL meoptimize = TRUE;
static BOOL vertr_abr_par = TRUE;
static int lutz_mdn_par = 0;
static int wa_pos_txt;
static BOOL RemoveKondArt = TRUE;
static BOOL RemoveAGrund  = TRUE;
static BOOL RemoveAGrund0 = TRUE;
static BOOL NoRecNr = FALSE;
static BOOL ld_pr_prim = TRUE;
static BOOL autopfand = TRUE;
static int PosSave = 0;
static BOOL PosSaveMess = FALSE;
static int InsCount = 0;
static BOOL LiefMeDirect = TRUE;
static int a_ersatz = 0;
static BOOL AufCharge = FALSE;
static int nachkpreis = 2;
static int lsc2_par = 0;
static int lsc3_par = 0;
static int VkAttribut = EDIT;
static BOOL TestBranSmt = FALSE;
static BOOL akt_preis = TRUE;
static BOOL FilStandard = FALSE;
static BOOL LdVkAsPercent = FALSE;
static BOOL StndWithWindow = FALSE;
static BOOL StndWithSound = TRUE;
static BOOL ReadLastMe = TRUE;
static BOOL WriteLastMe = TRUE;

BOOL lad_vk_edit = FALSE;


BOOL AUFPLIST::Spalte_last_lieferung_Ok = FALSE; 
BOOL AUFPLIST::Spalte_last_lief_me_Ok = FALSE; 
BOOL AUFPLIST::Spalte_last_me_Ok = TRUE;
BOOL AUFPLIST::Spalte_lieferdat_Ok = FALSE;
BOOL AUFPLIST::Spalte_lpr_vk_Ok = FALSE;
BOOL AUFPLIST::Spalte_langbez_Ok = FALSE;

BOOL AUFPLIST::Spalte_lad_pr_Ok = TRUE;
BOOL AUFPLIST::Spalte_lad_pr0_Ok = FALSE;
BOOL AUFPLIST::Spalte_pr_vk_Ok = FALSE;
BOOL AUFPLIST::Spalte_lad_pr_prc_Ok = FALSE;
BOOL AUFPLIST::Spalte_basis_me_bz_Ok = TRUE;



BOOL AUFPLIST::Spalte_last_lieferung_gewaehlt = FALSE; 
BOOL AUFPLIST::Spalte_last_lief_me_gewaehlt = FALSE; 
BOOL AUFPLIST::Spalte_last_me_gewaehlt = TRUE;
BOOL AUFPLIST::Spalte_lieferdat_gewaehlt = FALSE;
BOOL AUFPLIST::Spalte_lpr_vk_gewaehlt = FALSE;
BOOL AUFPLIST::Spalte_langbez_gewaehlt = FALSE;

BOOL AUFPLIST::Spalte_lad_pr_gewaehlt = FALSE;
BOOL AUFPLIST::Spalte_lad_pr0_gewaehlt = FALSE;
BOOL AUFPLIST::Spalte_pr_vk_gewaehlt = FALSE;
BOOL AUFPLIST::Spalte_lad_pr_prc_gewaehlt = FALSE;
BOOL AUFPLIST::Spalte_basis_me_bz_gewaehlt = TRUE;



#define Userdef1 9  //WAL-70




/*
struct AUFPS
{
       char posi [80];
       char sa_kz_sint [80];
       char a [80];
       char a_kun [20];
       char a_bz1 [80];
       char a_bz2 [80];
	   char last_me[80];
	   char last_pr_vk[80];
       char auf_me [80];
       char me_bz [80];
       char auf_vk_pr [80];
       char auf_lad_pr [80];
       char auf_lad_pr_prc [80];
       char marge [80];
       char basis_me_bz [80];
       char teil_smt [5];
       char me_einh_kun [5];
       char me_einh [5];
       char aufp_txt [9];
       char me_einh_kun1 [5];
       char auf_me1 [14];
       char inh1 [14];
       char me_einh_kun2 [5];
       char auf_me2 [14];
       char inh2 [14];
       char me_einh_kun3 [5];
       char auf_me3 [14];
       char inh3 [14];
	   char rab_satz [10];
	   char prov_satz [10];
       char lief_me [80];
	   char gruppe [9];
       char auf_vk_dm [80];
       char auf_lad_dm [80];
       char auf_vk_euro [80];
       char auf_lad_euro [80];
       char auf_vk_fremd [80];
       char auf_lad_fremd [80];
       char kond_art0 [20];
       char kond_art [20];
       char a_grund  [20];
       char posi_ext  [9];
	   char last_ldat [12];
       short ls_pos_kz;
       char  a_ers [16];
       short dr_folge;
       char  ls_charge [35];
       double charge_gew;
	   char aufschlag [5];
	   char aufschlag_wert [20];
	   short a_typ;
	   double a_leih;
	   short pos_txt_kz;
	   double a_gew;
	   char bsd[80];
};
*/

short wiedereinstieg = 1;
struct AUFPS aufps, aufptab [MAXPOS], aufps_null;

ITEM iposi        ("posi",		 aufps.posi,       "", 0);
ITEM isa_kz_sint  ("sa_kz_sint", aufps.sa_kz_sint, "", 0);
ITEM ia           ("a",          aufps.a,          "", 0);
ITEM ia_kun       ("a_kun",      aufps.a_kun,      "", 0);
ITEM ia_bz1       ("a_bz1",      aufps.a_bz1,      "", 0);
ITEM ia_bz2       ("a_bz2",      aufps.a_bz2,      "", 0);
ITEM ilast_me     ("last_me",    aufps.last_me,    "", 0);
ITEM ildat        ("lieferdat",  aufps.last_ldat,  "", 0);
ITEM ilangbez     ("lang_bez",   aufps.lang_bez,   "", 0); //WAL-11
ITEM iauf_me      ("auf_me",     aufps.auf_me,     "", 0);
ITEM ime_bz       ("me_bz",      aufps.me_bz,      "", 0);
ITEM ipr_vk       ("pr_vk",      aufps.auf_vk_pr,  "", 0);
ITEM ilpr_vk      ("lpr_vk",     aufps.last_pr_vk,  "", 0);
ITEM ilad_pr      ("ld_pr",      aufps.auf_lad_pr, "", 0);
ITEM ilad_pr_prc  ("ld_pr_prc",  aufps.auf_lad_pr_prc, "", 0);
ITEM ilad_pr0     ("ld_pr0",     aufps.auf_lad_pr0, "", 0);
ITEM imarge       ("marge",      aufps.marge, "", 0);
ITEM ibasis_me_bz ("basis_me_bz",aufps.basis_me_bz,"", 0);
ITEM ikond_art    ("kond_art",   aufps.kond_art,   "", 0);
ITEM ia_grund     ("a_grund",    aufps.a_grund,    "", 0);
ITEM ia_grund0    ("a_grund0",   aufps.a_grund,    "", 0);
ITEM ibsd         ("bsd",        aufps.bsd,    "", 0);
ITEM ibsd2        ("bsd2",       aufps.bsd2,   "", 0);
ITEM idummy       ("dummy",      "",    "", 0);

static field  _dataform[30] = {
&iposi,        5, Userdef1, 0, 6, 0, "%4d",     DISPLAYONLY, 0, 0, 0,
&isa_kz_sint,  2, Userdef1, 0, 12, 0, "%1d",    DISPLAYONLY, 0, 0, 0,
&ia,          14, 0, 0, 15, 0, "",       DISPLAYONLY, 0, 0, 0,
&ia_kun,      15, 0, 1, 15, 0, "",       REMOVED, 0, 0, 0,
&ia_bz1,      25, 0, 0, 30, 0, "",       DISPLAYONLY, 0, 0, 0,
&ia_bz2,      25, 0, 1, 30, 0, "",       DISPLAYONLY, 0, 0, 0,
&ilast_me,    11, 0, 0, 57, 0, "%8.3f",  DISPLAYONLY, 0, 0, 0,
&iauf_me,     11, 0, 0, 70, 0, "%8.3f",  EDIT,        0, 0, 0,
&ime_bz ,     11, 0, 0, 83, 0, "",       DISPLAYONLY, 0, 0, 0,
&ipr_vk,       9, 0, 0, 96,0, "%6.2f",   EDIT,        0, 0, 0,
&ilad_pr,      9, 0, 0,107,0, "%6.2f",   DISPLAYONLY, 0, 0, 0,
&ilad_pr_prc,  9, 0, 0,118,0, "%6.2f",   DISPLAYONLY, 0, 0, 0,
&ilad_pr0,     9, 0, 0,129,0, "%6.2f",   DISPLAYONLY, 0, 0, 0,
&imarge,       9, 0, 0,140,0, "%6.2f",   DISPLAYONLY, 0, 0, 0,
&ibasis_me_bz,11, 0, 0,151, 0, "",       DISPLAYONLY, 0, 0, 0,
&ikond_art,    6, 0, 0,166, 0, "",       DISPLAYONLY, 0, 0, 0,
&ia_grund,     6, 0, 0,177, 0, "%4d",    DISPLAYONLY, 0, 0, 0,
&ibsd,        13, 0, 0,186, 0, "%10.3f", DISPLAYONLY, 0, 0, 0,
&ibsd2,       13, 0, 0,201, 0, "%10.3f", DISPLAYONLY, 0, 0, 0,
&idummy,       3, 0, 0,201, 0, "%10.3f", DISPLAYONLY, 0, 0, 0,
&ia_grund0,    4, 0, 1, 57, 0, "%4d",    DISPLAYONLY, 0, 0, 0,
}; 

static form dataform = {21, 0, 0, _dataform, 0, 0, 0, 0, NULL};

static field _fldat   = {&ildat, 12, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};
static field _flpr_vk = {&ilpr_vk, 9, 0, 0, 0, 0, "%6.2f", DISPLAYONLY, 0, 0, 0};
static field _flast_me = {&ilast_me, 9, 0, 0, 0, 0, "%6.2f", DISPLAYONLY, 0, 0, 0}; //FS-126
static field _flad_pr = {&ilad_pr, 9, 0, 0, 0, 0, "%6.2f", DISPLAYONLY, 0, 0, 0}; //FS-126
static field _flad_pr0 = {&ilad_pr0, 9, 0, 0, 0, 0, "%6.2f", DISPLAYONLY, 0, 0, 0}; //FS-126
static field _fpr_vk = {&ipr_vk, 9, 0, 0, 0, 0, "%6.2f", DISPLAYONLY, 0, 0, 0}; //FS-126
static field _fbasis_me_bz = {&ibasis_me_bz, 11, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0}; //FS-126
static field _flad_pr_prc = {&ilad_pr_prc, 9, 0, 0, 0, 0, "%6.2f", DISPLAYONLY, 0, 0, 0}; //FS-126
static field _flangbez   = {&ilangbez, 50, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0}; //WAL-11
static field _fa_bz1   = {&ia_bz1, 25, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0}; //WAL-11

static BOOL DelLadVK0  = TRUE;
static BOOL DelLadVK  = FALSE;
static BOOL DelPrVK  = FALSE;
static BOOL DelLastMe = TRUE;
static BOOL AddLastLief = FALSE;
static BOOL LastFromAufKun = FALSE;
static BOOL AddLastVkPr = FALSE;
static BOOL AddLangBez = FALSE; //WAL-11
static BOOL DelLadVKPrc = TRUE;
static BOOL RemoveBasisMe = FALSE;
static BOOL RemoveMarge = TRUE;
static BOOL RemoveBsd = TRUE;
static BOOL ShowLqd = FALSE;

static BOOL FormOK = FALSE;


void addField (form * frm, field * feld, int pos, int len, int space)
{
	int i;
	int plus;

	frm->fieldanz ++;
	plus = len + space;

	feld->pos[1] = frm->mask[pos].pos[1];
	for (i = frm->fieldanz - 1; i > pos; i --)
	{
		memcpy (&frm->mask[i], &frm->mask[i - 1], sizeof (field));
		frm->mask[i].pos[1] += plus;
	}
	memcpy (&frm->mask[i], feld, sizeof (field));
}


int addFieldName (form * frm, field * feld, char *name, int space)
{
	int pos;
	int len;

	len = feld->length;
	pos = GetItemPos (frm, name);
	if (pos == -1)
	{
		return pos;
	}
	addField (frm, feld, pos, len, space);
	return pos;
}


void DelFormField (form *frm, int pos)
/**
Feld aus Dataform herausnehmen.
**/
{
	         int i;
			 int diff;
			 int pos1, pos2;

			 pos1 = frm->mask[pos].pos[1];
			 if (pos < frm->fieldanz - 1)
			 {
				 pos2 = frm->mask[pos + 1].pos[1];
				 diff = max (0, pos2 - pos1);
			 }
			 else
			 {
				 diff = frm->mask[pos].length;
				 if (frm != &dataform) diff ++;
			 }
			 if (diff == 0)
			 {
				 diff = frm->mask[pos].length;
				 if (frm != &dataform) diff ++;
			 }

			 frm->fieldanz --;

			 for (i = pos; i < frm->fieldanz; i ++)
			 {
				 memcpy ((char *) &frm->mask[i], (char *) &frm->mask[i + 1], sizeof (field));
			 }

			 for (i = 0; i < frm->fieldanz; i ++)
			 {
				 if (frm->mask[i].pos[1] > pos1)
				 {
					 frm->mask[i].pos[1] -= diff;
				 }
			 }
}


static int ubrows [] = {0,
                        1,
                        2,2,
                        3,3,
						4,5,6,7,8,9, 10, 11, 12, 13, 14, 15, 17, 18};

/*
struct CHATTR ChAttr [] = {"a",     DISPLAYONLY, EDIT,
                            NULL,  0,           0};
*/


struct CHATTR ChAttra [] = {"a",     DISPLAYONLY, EDIT,
                             NULL,  0,           0};
struct CHATTR ChAttra_kun [] = {"a_kun", DISPLAYONLY, EDIT,
                                 NULL,   0,           0};

struct CHATTR *ChAttr = ChAttra;

ColButton Cuposi = {  "Pos.", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cusa_kz_sint = {
                     "S", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cua = {
                     "Artikel-Nr", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};
ColButton Cua_bz1 = {
                     "Bezeichnung1", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cua_bz2 = {
                     "Bezeichnung2", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cuauf_me = {
                     "A.-Menge", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cume_bz = {
                     "Best.ME", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cupr_vk = {
                     "VK", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};

ColButton Cubasis_me_bz = {
                     "Basis-ME", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      -2};


static char *VK        = "  VK    ";
static char *VK_DM     = " VK DM  ";
static char *VK_EURO   = " VK EURO ";
static char *VK_FREMD  = " VK FREMD ";

static char *LD        = " Ld.VK  ";
static char *LDPRC     = " Rabatt % ";
static char *Marge     = " Marge % ";
static char *LD_DM     = " LD DM  ";
static char *LD_EURO   = " LD EURO ";
static char *LD_FREMD  = "LD FREMD";


ITEM iuposi        ("posi",       "Pos.",            "", 0);
ITEM iusa_kz_sint  ("sa_kz_sint", "S",               "", 0);
ITEM iua           ("a",          "Artikel-Nr   ",   "", 0);
ITEM iua_bz1       ("a_bz1",      "Bezeichnung 1",   "", 0);
ITEM iulangbez     ("lang_bez",    "Bezeichnungen",   "", 0); //WAL-11
ITEM iua_bz2       ("a_bz2",      "Bezeichnung 2",   "", 0);
ITEM iulast_me     ("last_me",    "Letz.Bst",        "", 0);
ITEM iuldat        ("lieferdat", "Letz.LD.",        "", 0);
ITEM iulpr_vk      ("lpr_vk",     "Letz.VK",         "", 0);
ITEM iuauf_me      ("auf_me",     "A.-Menge",        "", 0);
ITEM iume_bz       ("me_bz",      "Best.ME",         "",  0);
ITEM iupr_vk       ("pr_vk",       VK,               "", 0);
ITEM iulad_pr      ("ld_pr",       LD,               "", 0);
ITEM iulad_pr_prc  ("ld_pr_prc",   LDPRC,            "", 0);
ITEM iulad_pr0     ("ld_pr0",      "LD Marg.",       "", 0);
ITEM iumarge       ("marge",       Marge,            "", 0);
ITEM iubasis_me_bz ("basis_me_bz","Basis-ME",        "",  0);
ITEM iukond_art    ("kond_art",   "Kond.Art",        "",  0);
ITEM iua_grund     ("a_grund",    "Gr.Art",          "",  0);
ITEM iubsd         ("bsd",        "Bestand m.Preis", "",  0);
ITEM iubsd2        ("bsd2",       "Bestand o.Preis", "",  0);
ITEM iudummy       ("dummy",                "",           "", 0);
ITEM iufiller      ("",                "",           "", 0);


static field  _ubform[30] = {
&iuposi,        5, 0, 0,  6, 0, "",  BUTTON, 0, 0, 0,
&iusa_kz_sint,  3, 0, 0, 11, 0, "",  BUTTON, 0, 0, 0,
&iua,          15, 0, 0, 14, 0, "",  BUTTON, 0, 0, 0,
&iua_bz1,      27, 0, 0, 29, 0, "",  BUTTON, 0, 0, 0,
&iulast_me,    13, 0, 0, 56, 0, "",  BUTTON, 0, 0, 0,
&iuauf_me,     13, 0, 0, 69, 0, "",  BUTTON, 0, 0, 0,
&iume_bz,      13, 0, 0, 82, 0, "",  BUTTON, 0, 0, 0,
&iupr_vk,      11, 0, 0, 95,0,  "",  BUTTON, 0, 0, 0,
&iulad_pr,     11, 0, 0,106,0,  "",  BUTTON, 0, 0, 0,
&iulad_pr_prc, 11, 0, 0,117,0,  "",  BUTTON, 0, 0, 0,
&iulad_pr0,    11, 0, 0,128,0,  "",  BUTTON, 0, 0, 0,
&iumarge,      11, 0, 0,139,0,  "",  BUTTON, 0, 0, 0,
&iubasis_me_bz,15, 0, 0,150, 0, "",  BUTTON, 0, 0, 0,
&iukond_art,   12, 0, 0,165, 0, "",  BUTTON, 0, 0, 0,
&iua_grund,     8, 0, 0,177, 0, "",  BUTTON, 0, 0, 0,
&iubsd,        15, 0, 0,185, 0, "",  BUTTON, 0, 0, 0,
&iubsd2,       15, 0, 0,200, 0, "",  BUTTON, 0, 0, 0,
&iudummy,       1, 0, 0,215, 0, "",  BUTTON, 0, 0, 0,
&iufiller,    100, 0, 0,215, 0, "",  BUTTON, 0, 0, 0,
};


static form ubform = {19, 0, 0, _ubform, 0, 0, 0, 0, NULL};

ITEM iline ("", "1", "", 0);


static field  _lineform[30] = {
&iline,      1, 0, 0, 11, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 14, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 29, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 56, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 69, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 82, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0, 95, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,106, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,117, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,128, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,139, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,150, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,165, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,177, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,185, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,200, 0, "",  NORMAL, 0, 0, 0,
&iline,      1, 0, 0,215, 0, "",  NORMAL, 0, 0, 0,
};


static form lineform = {17, 0, 0, _lineform, 0, 0, 0, 0, NULL};

static field _fuldat   = {&iuldat,     14, 0, 0, 0, 0, "", BUTTON, 0, 0, 0};
static field _fline    = {&iline,       1, 0, 0, 0, 0, "", NORMAL, 0, 0, 0};
static field _fulpr_vk = {&iulpr_vk,   11, 0, 0, 0, 0, "", BUTTON, 0, 0, 0};
static field _fulast_me = {&iulast_me,   11, 0, 0, 0, 0, "", BUTTON, 0, 0, 0}; //FS-126
static field _fulad_pr = {&iulad_pr, 11, 0, 0, 0, 0, "", BUTTON, 0, 0, 0}; //FS-126
static field _fulad_pr0 = {&iulad_pr0, 11, 0, 0, 0, 0, "", BUTTON, 0, 0, 0}; //FS-126
static field _fupr_vk = {&iupr_vk, 11, 0, 0, 0, 0, "", BUTTON, 0, 0, 0}; //FS-126
static field _fubasis_me_bz = {&iubasis_me_bz, 13, 0, 0, 0, 0, "", BUTTON, 0, 0, 0}; //FS-126
static field _fulad_pr_prc = {&iulad_pr_prc, 11, 0, 0, 0, 0, "", BUTTON, 0, 0, 0}; //FS-126
static field _fulangbez = {&iulangbez, 52, 0, 0, 0, 0, "", BUTTON, 0, 0, 0}; //WAL-11
static field _fua_bz1 = {&iua_bz1, 27, 0, 0, 0, 0, "", BUTTON, 0, 0, 0}; //WAL-11


void addLangBez (char *name)  //WAL-11
{
	int pos;
	if (AUFPLIST::Spalte_langbez_Ok) 
	{
		return;
	}

    AUFPLIST::Spalte_langbez_Ok = TRUE;
	pos = addFieldName (&ubform,   &_fulangbez, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fulangbez.length, 0);
	     addFieldName (&dataform, &_flangbez,  name, 2);
	}
}
void adda_bz1 (char *name)  //WAL-11
{
	int pos;
	pos = addFieldName (&ubform,   &_fua_bz1, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fua_bz1.length, 0);
	     addFieldName (&dataform, &_fa_bz1,  name, 2);
	}
}

void addLastLdat (char *name)
{
	int pos;
	if (AUFPLIST::Spalte_lieferdat_Ok)
	{
		return;
	}

    AUFPLIST::Spalte_lieferdat_Ok = TRUE;
	pos = addFieldName (&ubform,   &_fuldat, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fuldat.length, 0);
	     addFieldName (&dataform, &_fldat,  name, 2);
	}
}

void addLastVkPr (char *name)
{
	int pos;
	if (AUFPLIST::Spalte_lpr_vk_Ok)
	{
		return;
	}

    AUFPLIST::Spalte_lpr_vk_Ok = TRUE;

	pos = addFieldName (&ubform,  &_fulpr_vk, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fulpr_vk.length, 0);
	     addFieldName (&dataform, &_flpr_vk,  name, 2);
	}
}

void addPrVk (char *name) //FS-126
{
	int pos;
	if (AUFPLIST::Spalte_pr_vk_Ok)
	{
		return;
	}

    AUFPLIST::Spalte_pr_vk_Ok = TRUE;
	pos = addFieldName (&ubform,  &_fupr_vk, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fupr_vk.length, 0);
	     addFieldName (&dataform, &_fpr_vk,  name, 2);
	}
}


void addLastMe (char *name) //FS-126
{
	int pos;
	if (AUFPLIST::Spalte_last_me_Ok)
	{
		return;
	}

    AUFPLIST::Spalte_last_me_Ok = TRUE;
	pos = addFieldName (&ubform,  &_fulast_me, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fulast_me.length, 0);
	     addFieldName (&dataform, &_flast_me,  name, 2);
	}
}

void addLadPr (char *name) //FS-126
{
	int pos;
	if (AUFPLIST::Spalte_lad_pr_Ok)
	{
		return;
	}

    AUFPLIST::Spalte_lad_pr_Ok = TRUE;
	pos = addFieldName (&ubform,  &_fulad_pr, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fulad_pr.length, 0);
	     addFieldName (&dataform, &_flad_pr,  name, 2);
		 if (lad_vk_edit) SetItemAttr (&dataform, "ld_pr", EDIT);  //FS-375

	}
}

void addLadPr0 (char *name) //FS-126
{
	int pos;
	if (AUFPLIST::Spalte_lad_pr0_Ok)
	{
		return;
	}

    AUFPLIST::Spalte_lad_pr0_Ok = TRUE;
	pos = addFieldName (&ubform,  &_fulad_pr0, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fulad_pr0.length, 0);
	     addFieldName (&dataform, &_flad_pr0,  name, 2);
	}
}


void addLadPrPrc (char *name) //FS-126
{
	int pos;
	if (AUFPLIST::Spalte_lad_pr_prc_Ok)
	{
		return;
	}

    AUFPLIST::Spalte_lad_pr_prc_Ok = TRUE;
	pos = addFieldName (&ubform,  &_fulad_pr_prc, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fulad_pr_prc.length, 0);
	     addFieldName (&dataform, &_flad_pr_prc,  name, 2);
	}
}

void addBasisMeBz (char *name) //FS-126
{
	int pos;
	if (AUFPLIST::Spalte_basis_me_bz_Ok)
	{
		return;
	}

    AUFPLIST::Spalte_basis_me_bz_Ok = TRUE;
	pos = addFieldName (&ubform,  &_fubasis_me_bz, name, 0);
	if (pos != -1)
	{
		 if (pos) pos --;
	     addField (&lineform,  &_fline,  pos, _fubasis_me_bz.length, 0);
	     addFieldName (&dataform, &_fbasis_me_bz,  name, 2);
	}
}

static ListClassDB eListe;
PTAB_CLASS ptab_class;
WA_PREISE WaPreis;
HNDW_CLASS HndwClass;
AG_CL Ag;
SYS_PAR_CLASS sys_par_class;
static QueryClass QClass;
static LS_CLASS ls_class;
static DB_CLASS DbClass;
static EINH_CLASS einh_class;
static AUFPTLIST TListe;
static AutoNrClass AutoNr;
static BSD_BUCH_CLASS BsdBuch;
static AUFKUN_CLASS AufKun;
static AUFPT_CLASS aufpt_class;

static BEST_BDF_CLASS best_bdf_class;
static BEST_RES_CLASS best_res_class;
static PROG_CFG ProgCfg ("51100");
static SMTG_CLASS Smtg;
static PROV Prov;
static int plu_size = 4;
static int auf_me_default= 0;
static long aufkunanz = 5;

static StndAuf StndAuf;
static BOOL searchadirect = TRUE;
static BOOL searchmodedirect = TRUE;
static double RowHeight = 1.5;
static int UbHeight = 0;
static int bsd_kz = 1;
static int ShowBsd = 0;
static int matchcode = 0;

static BOOL NoArtMess = FALSE;

static BOOL ListColors = TRUE;
static BOOL ber_komplett = TRUE;
static COLORREF KompfColor = BLACKCOL;
static COLORREF KompbColor = GRAYCOL;
static COLORREF SafColor   = WHITECOL;
static COLORREF SabColor   = BLACKCOL;
static COLORREF userdef1_bColor   =    RGB (255, 100, 100); //WAL-70


static char KunItem[] = {"kuna"};
static char FilItem[] = {"fila"};
static double Akta = 0.0;
static BOOL auf_me_pr_0 = 1;
static int preis0_mess = 1;
static double inh = 0.0;
static long dauertief = 90l;

static double akt_me;
static double akt_auf_me;
static int akt_me_einh;


void DelFormFieldEx (form *frm, int pos, int diff)
/**
Feld aus Dataform herausnehmen.
**/
{
	         int i;
			 int pos1;

			 pos1 = frm->mask[pos].pos[1];
			 frm->fieldanz --;

			 for (i = pos; i < frm->fieldanz; i ++)
			 {
				 memcpy ((char *) &frm->mask[i], (char *) &frm->mask[i + 1], sizeof (field));
			 }

			 for (i = 0; i < frm->fieldanz; i ++)
			 {
				 if (frm->mask[i].pos[1] > pos1)
				 {
					 frm->mask[i].pos[1] -= diff;
				 }
			 }
}

/*
void DelFrmDB (int pos)
{
	         int i;

			 for (i = 0; dbipr[i].frm; i ++)
			 {
				 if (dbipr[i].frmpos == pos) break;
			 }
			 if (dbipr[i].frm)
			 {
			     for (; dbipr[i].frm; i ++)
				 {
				            memcpy (&dbipr[i], &dbipr[i + 1], sizeof (FRMDB));
				 }
			 }
			 for (i = 0; dbipr[i].frm; i ++)
			 {
				 if (dbipr[i].frmpos > pos) dbipr[i].frmpos --;
			 }
}
*/

void DelListField (char *Item)
{
	         int pos, diff;

		     diff = 0;
		     pos = GetItemPos (&ubform, Item);
			 if (pos > -1 && pos < ubform.fieldanz - 1)
			 {
					 diff = ubform.mask[pos + 1].pos[1] -
						    ubform.mask[pos].pos[1];
			 }
	         if (pos > -1) DelFormFieldEx (&ubform, pos, diff);
	         if (pos > -1) DelFormFieldEx (&lineform, pos - 1, diff);
		     pos = GetItemPos (&dataform, Item);
	         if (pos > -1) DelFormFieldEx (&dataform, pos, diff);
//			 DelFrmDB (pos);
}


static TEXTMETRIC textm;

static int EnterBreak ()
{
	 break_enter ();
	 return (0);
}

static int EnterTest (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		}
        return 0;
}


void SetUbHeight (void)
/**
Hoehe der Listueberschrift setzen.
**/
{
	int i;

	for (i = 0; i < ubform.fieldanz; i ++)
	{
		ubform.mask[i].rows = UbHeight;
	}
}

static int InfoProc (char **Item, char *Value, char *where)
/**
Spezielle Procedure fuer Info-System
**/
{

    clipped (*Item);
//    if (strcmp (*Item, "a") == 0)
    {
		if (aufk.kun_fil == 0)
		{
			*Item = KunItem;
			sprintf (where, "where kun = %ld and a = %.0lf",
                         aufk.kun, ratod (Value));
		}
		else
		{
			*Item = FilItem;
			sprintf (where, "where mdn = %hd and fil = %ld and a = %.0lf",
                         aufk.mdn, aufk.kun, ratod (Value));
		}
        return 1;
    }
    return 0;
}

//short AUFPLIST::auf_art = 0;
short AUFPLIST::auf_art = 1;
BOOL AUFPLIST::FlgInEnterListe = FALSE; //FS-126

BOOL AUFPLIST::Muster = FALSE;
BOOL AUFPLIST::AErsInLs = TRUE;
int AUFPLIST::StndDirect = 0;
int AUFPLIST::splitls = 0;
int AUFPLIST::LetztLiefHolen = 0;
HANDLE AUFPLIST::ProcessIDVerkFragen = 0;
int AUFPLIST::LetztLiefAnzTage = 30;
CDllPreise AUFPLIST::DllPreise;
CBsd AUFPLIST::Bsd;
BOOL AUFPLIST::TxtByDefault = FALSE;
int AUFPLIST::PosTxtKz = CControlPlugins::KommOnly;
BOOL AUFPLIST::PosTxtNrPar = FALSE;
CGrpPrice AUFPLIST::GrpPrice;
CControlPlugins AUFPLIST::pControls;
CClientOrderThread AUFPLIST::ClientOrder;
CClientOrder AUFPLIST::RClientOrder; //FS-126  f�r die Reiter  ohne Thread
AUFPLIST *AUFPLIST::instance = NULL;
CClientOrder::STD_RDOPTIMIZE AUFPLIST::StndRdOptimize;
BOOL AUFPLIST::RefreshStnd = FALSE;
CRON_BEST_CLASS AUFPLIST::CronBest;


void AUFPLIST::SetMessColors (COLORREF color, COLORREF bkcolor)
/**
Farben fuer Melungen setzen.
**/
{
	 MessCol   = color;
	 MessBkCol = bkcolor;
}

static long mdnprod = 0;

void AUFPLIST::SetCfgProgName (LPSTR ProgName)
{
    ProgCfg.SetProgName (ProgName);
}

void AUFPLIST::SetMdnProd (int mdnp)
{
	mdnprod = mdnp;
}

void AUFPLIST::SethMainWindow (HWND hMainWindow)
{
    this->hMainWindow = hMainWindow;
	hMainWin = hMainWindow;
}

AUFPLIST:: AUFPLIST ()
{
	instance = this;
	RefreshStnd = FALSE;
    mamainmax = 0;
    mamainmin = 0;
    eingabesatz = NULL;
    ausgabesatz = ausgabebuffer;
    zlen = 0;
    feld_anz = 0;
    splitls = 0;
    banz = 0;
    PageView = 0;
    LstZiel = NULL;
    LstSatz   = NULL;
    Lstbanz = 0;
    SwRecs = 0;
    AktRow = 0;
    AktColumn = 0;
    scrollpos = 0;
    InfoCaption[0] = (char) 0;
    WithMenue   = 1;
    WithToolBar = 1;
	preistest = 0;
	add = FALSE;
    lutz_mdn_par = 0;
	liney = 0;
	aufme_old = (double) 0.0;
    dataform.after  = WriteRow;
    dataform.before = SetRowItem;
    dataform.mask[2].before = Savea;
    dataform.mask[2].after  = fetcha;
    dataform.mask[3].after  = fetcha_kun;
    dataform.mask[7].before = setkey9me;
    dataform.mask[7].after  = testme;
    dataform.mask[9].before  = setkey9basis;
	dataform.mask[9].after  = AfterPrEnter;


    ListAktiv = 0;
    inh = (double) 0.0;
    this->hMainWindow = NULL;
	hMainWin = NULL;
    Muster = FALSE;

// Bei QuickSearch = TRUE keine verschachtelten Aktionen

    WA_PREISE::QuickSearch = FALSE;
	NoteIcon = NULL;
	StndHandler = new CStndHandler (WM_STND_COMPLETE);
	StndHandler->SetInstance (this);
	StndFailed = new CStndFailed (WM_STND_COM_FAILED);
	StndFailed->SetInstance (this);
	StndReload = new CStndReload (WM_STND_RELOAD);
	StndReload->SetInstance (this);
	Ampel.SetState (CAmpel::No);
}

AUFPLIST::~AUFPLIST ()
{
	ColBkNames.DestroyElements ();
	ColBkNames.Destroy ();
}

void AUFPLIST::SetLager (long lgr)
{
	akt_lager = lgr;
}

int AUFPLIST::ShowBasis (void)
/**
Basismenge der aktuellen Bestellmenge anzeigen.
**/
{
	HDC hdc;
	double auf_me;
	double a;
	short me_einh_kun;
	double auf_me_vgl;
    KEINHEIT keinheit;
	HWND hWnd;
	HWND hMainWindow;

	static char basis_me[15];
	static char basis_einh[11];


    static mfont anzfont    = {NULL, -1, -1, 1,
                               BLACKCOL,
                               WHITECOL,
                               0};

	static ITEM ibasis_me    ("basis_me", basis_me, "Menge in Basiseinheit :", 0);
	static ITEM ibasis_einh  ("me_einh",  basis_einh, "", 0);
	static ITEM iOK          ("OK",       "OK", "", 0);

	static field _fbasis_me[] = {
		&ibasis_me,      9, 0, 1, 2, 0, "%8.2f", READONLY, 0, 0, 0,
		&ibasis_einh,    8, 0, 1,36, 0, "",      DISPLAYONLY, 0, 0, 0,
		&iOK,           10, 0, 3,17, 0, "",      BUTTON,      0, StopEnter, KEY5,
	};

	static form fbasis_me = {3, 0, 0, _fbasis_me, 0, 0, 0, 0, NULL};

	anzfont.FontName = "                   ";
    GetStdFont (&anzfont);
//	anzfont.FontName = "ARIAL";
    anzfont.FontAttribute = 1;

	auf_me = ratod (aufps.auf_me);
	a  = ratod (aufps.a);
	me_einh_kun = atoi (aufps.me_einh_kun);

    strcpy (kumebest.kun_bran2, kun.kun_bran2);
    einh_class.AktAufEinh (aufk.mdn, aufk.fil,
                                aufk.kun, a, me_einh_kun);
    strcpy (kumebest.kun_bran2, kun.kun_bran2);
    einh_class.GetKunEinh (aufk.mdn, aufk.fil,
                              aufk.kun, a, &keinheit);

    if (keinheit.me_einh_kun == keinheit.me_einh_bas)
    {
            auf_me_vgl = auf_me;
    }
    else
    {
            if (keinheit.inh <= (double) 0.0)
            {
                     keinheit.inh = (double) 1.0;
            }
            auf_me_vgl = auf_me * keinheit.inh;
    }

	sprintf (basis_me, "%8.2lf", auf_me_vgl);
	strcpy  (basis_einh, keinheit.me_einh_bas_bez);

    break_end ();
	SetButtonTab (TRUE);
	hMainWindow = GetActiveWindow ();
    DisablehWnd (hMainWindow);
    SetBorder (WS_POPUP | WS_VISIBLE| WS_DLGFRAME);
	SethStdWindow ("hListWindow");
    hWnd = OpenWindowChC (5, 46, 10, 14, hMainInst, "");
	SethStdWindow ("hStdWindow");
//	hWnd = OpenColWindow (5, 46, 10, 14, WHITECOL, NULL);
	BasishWnd = hWnd;
    hdc = GetDC (hWnd);
    ChoiseLines (hWnd, hdc);
    ReleaseDC (hWnd, hdc);

    SetStaticWhite (TRUE);
    enter_form (hWnd, &fbasis_me, 0, 0);
    SetStaticWhite (FALSE);

	CloseControls (&fbasis_me);
    DestroyWindow (hWnd);
	BasishWnd = NULL;
	AktivWindow = hMainWindow;
	SetButtonTab (FALSE);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
	return 0;
}

void AUFPLIST::SetChAttr (int ca)
{
    switch (ca)
    {
          case 0 :
              ChAttr = ChAttra;
              break;
          case 1:
              ChAttr = ChAttra_kun;
              break;
    }
}

void AUFPLIST::SetFieldAttr (char *fname, int attr)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return;

         dataform.mask[i].attribut = attr;
}

int AUFPLIST::GetFieldAttr (char *fname)
{
         int i;
         char *feldname;

         for (i = 0; i < dataform.fieldanz; i ++)
         {
             feldname = dataform.mask[i].item->GetItemName ();
             if (feldname && strcmp (feldname, fname) == 0)
             {
                   break;
             }
         }
         if (i == dataform.fieldanz) return -1;

         return (dataform.mask[i].attribut);
}

int AUFPLIST::Getib (void)
{
    static BOOL ParOK = 0;
    static int IB;

    if (ParOK) return IB;

    ParOK = 1;
    IB = 0;
    strcpy (sys_par.sys_par_nam,"ib");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt))
        {
                  IB = 1;
        }
    }
    return IB;
}

void AUFPLIST::Geta_kum_par (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    a_kum_par = FALSE;
    strcpy (sys_par.sys_par_nam,"a_kum_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt))
        {
                  a_kum_par = TRUE;
        }
    }
    return;
}


void AUFPLIST::Geta_bz2_par (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    SetFieldAttr ("a_bz2", REMOVED);
    return;
    strcpy (sys_par.sys_par_nam,"a_bz2_par");
    if (sys_par_class.dbreadfirst () == 0)
    {
        if (atoi (sys_par.sys_par_wrt))
        {
                  SetFieldAttr ("a_bz2", DISPLAYONLY);
                  return;
        }
    }
    SetFieldAttr ("a_bz2", REMOVED);
//    eListe.DestroyField (eListe.GetFieldPos ("a_bz2"));
}

void AUFPLIST::Getauf_me_pr0 (void)
{
    BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    auf_me_pr_0 = 0;
    strcpy (sys_par.sys_par_nam,"auf_me_pr_0");
    if (sys_par_class.dbreadfirst () != 0)
    {
        return;
    }
    auf_me_pr_0 = atoi (sys_par.sys_par_wrt);
}


double AUFPLIST::GetAufMeVgl (double a, double auf_me, short me_einh_kun)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;
         double auf_me_vgl;

         strcpy (kumebest.kun_bran2, kun.kun_bran2);
         einh_class.AktAufEinh (aufk.mdn, aufk.fil,
                                aufk.kun, a, me_einh_kun);
         strcpy (kumebest.kun_bran2, kun.kun_bran2);
         einh_class.GetKunEinh (aufk.mdn, aufk.fil,
                                aufk.kun, a, &keinheit);

         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                      auf_me_vgl = auf_me;
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     auf_me_vgl = auf_me * keinheit.inh;
         }
         return auf_me_vgl;
}



int AUFPLIST::SetRowItem (void)
{
	   int pos;
	   double auf_me;


	   pos = eListe.GetAktRow ();
       eListe.SetRowItem ("a", aufptab[pos].a);
	   auf_me = ratod (aufptab[pos].auf_me);
	   if (auf_me == (double) 0.0)
	   {
		   akt_me = (double) 0.0;
	   }
	   else
	   {

           akt_me = GetAufMeVgl (ratod (aufptab[pos].a), auf_me,
			                     atoi (aufptab[pos].me_einh_kun));
	   }
       akt_auf_me = auf_me;
       akt_me_einh = atoi (aufptab[pos].me_einh_kun);
 
	   auf_vk_pr = ratod (aufps.auf_vk_pr);
       TestSaPr ();
	   if (bsd_kz && ShowBsd != 0) 
	   {
			BsdInfo.Create ();
			if (ShowBsd == 1)
			{
				double bsd = Bsd.Calculate (aufk.mdn, ratod (aufps.a));
				BsdInfo.SetBsd (bsd);
			}
	   }
	   if (strlen(clipped(ProcessVerkFragen)) >2 ) //WAL-15
	   {
		  SetzeEventVerkFragen(aufps.pos_id, ratod(aufps.a));
	   }
       return 0;
}

void AUFPLIST::BsdCalculate ()
{
//		BsdInfo.Create ();
		double bsd = Bsd.Calculate (aufk.mdn, ratod (aufps.a));
		BsdInfo.SetBsd (bsd);
}

void AUFPLIST::SetAufVkPr (double pr)
{
	   auf_vk_pr = pr;
}

int AUFPLIST::TestAppend (void)
/**
Testen, ob ein Satz angehaengt werden darf.
**/
{
/*
	if (LISTHANDLER->GetLoadComplete ())
	{
		return FALSE;
	}
*/
    if (eListe.GetRecanz () == 0);
    else if (ratod (aufps.a) == (double) 0.0)
    {
        return FALSE;
    }


    if (PosSave > 0)
    {
        if (TestInsCount () == FALSE) return 0;
    }
    testmeOK = FALSE;
	auf_vk_pr = (double) 0.0;
	memcpy (&aufps, &aufps_null, sizeof (struct AUFPS));
    eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
    return TRUE;
}

int AUFPLIST::DeleteLine (void)
/**
Zeile aus Liste loeschen.
**/
{
	    BOOL HasService = ContainsService ();
   	    BucheBsd (ratod (aufps.a), (double) 0.0, atoi (aufps.me_einh_kun),
			      ratod (aufps.auf_vk_pr));
        eListe.DeleteLine ();
		LISTHANDLER->Update (atoi (aufps.posi), &aufps);//FS-126

		//030611 A
	    if (PosSave > 0) 
		{
			_WriteAllPos (); 
		    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ()); 
		}

		//030611 E
		

        testmeOK = FALSE;
 	    AnzAufWert ();
 	    AnzAufGew ();
	    if (IsPartyService)
		{
           if (HasService && !ContainsService ())
		   {
/*
				int ret = MessageBox (NULL, "Auftrag auf Standardsteuersatz setzen ?", NULL,
						MB_YESNO | MB_ICONQUESTION);
				if (ret == IDYES)
				{		
					aufk.psteuer_kz = 1;
				}
*/ 
			    SetLsToStandardTax ();
			    EnableFullTax ();

		   }
		}
        return 0;
}

int AUFPLIST::InsertLine (void)
/**
Zeile aus Liste loeschen.
**/
{

        if (PosSave > 0)
        {
            if (TestInsCount () == FALSE) return 0;
        }

        testmeOK = FALSE;
        eListe.SetPos (eListe.GetAktRow (), eListe.FirstColumn ());
        eListe.InsertLine ();
        return 0;
}


int AUFPLIST::PosRab (void)
/**
Zeile aus Liste loeschen.
**/
{
	    wiedereinstieg = 1;
	    while (wiedereinstieg)//dies ist nur ne Kr�cke, um den Wert in der Form anzuzeigen Lud 290506
		{
		    wiedereinstieg = 0;
			EnterPosRab ();
		}
        return 0;
}

int AUFPLIST::ProdMdn (void)
/**
Zeile aus Liste loeschen.
**/
{
	    long gruppe;

	    gruppe = ChoiseProdLgr (atol (aufps.gruppe));
		sprintf (aufps.gruppe, "%ld", gruppe);
        memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
        return 0;
}

BOOL AUFPLIST::SavePosis (void)
{
       int recs;
       int i;

       if (PosSave > 1 && PosSaveMess &&
           abfragejn (eListe.Getmamain3 (),
		             "Positionen speichern ?" , "J") == 0)
       {
           DoBreak ();
           return FALSE;
       }
       memcpy (&aufptab[eListe.GetAktRow ()], &aufps, sizeof (struct AUFPS));
       recs = eListe.GetRecanz ();
//       ls_class.delete_aufpauf (aufk.mdn, aufk.fil, aufk.auf);

       if (Getib ())
       {
            DeleteBsd ();
       }

       GenNewPosi ();
       for (i = recs - PosSave; i < recs; i ++)
       {
           WritePos (i);
       }
//	   StndAuf.CleanUpdError ();
       commitwork ();
       beginwork ();

	   //FS-35 A
       DbClass.sqlin ((short *) &aufk.mdn,   1, 0);
       DbClass.sqlin ((short *) &aufk.mdn,   1, 0);
       DbClass.sqlin ((short *) &aufk.fil,   1, 0);
       DbClass.sqlin ((long *)  &aufk.auf,   2, 0);
	   DbClass.sqlcomm ("update aufk set mdn = ? where mdn = ? and fil = ? and auf = ?  ");
	   //FS-35 E
       InsCount ++;
       return TRUE;
}


BOOL AUFPLIST::TestInsCount (void)
{
        if (InsCount >= PosSave)
        {
            InsCount = 0;
            return SavePosis ();
        }
        InsCount ++;
        return TRUE;
}

int AUFPLIST::AppendLine (void)
/**
Zeile aus Liste loeschen.
**/
{
        if (PosSave > 0)
        {
            if (TestInsCount () == FALSE) return 0;
        }
        testmeOK = FALSE;
	    auf_vk_pr = (double) 0.0;
        eListe.AppendLine ();
        return 0;
}

BOOL AUFPLIST::BsdArtikel (double a)
/**
Bestandskennzeichen in a_bas pruefen.
**/
{
	     static double bsd_a; 
	     static char bsd_kz [2];
		 static int cursor = -1;

		 if (cursor == -1)
		 {
			DbClass.sqlin ((double *) &bsd_a, 3, 0);
			DbClass.sqlout ((char *) bsd_kz, 0, sizeof (bsd_kz));
			cursor = DbClass.sqlcursor ("select bsd_kz from a_bas where a = ?");
		 }
		 strcpy (bsd_kz, "N");
		 bsd_a = a;
		 DbClass.sqlopen (cursor);
		 DbClass.sqlfetch (cursor);
		 if (bsd_kz[0] == 'J') return TRUE;
		 return FALSE;
}


void AUFPLIST::BucheBsd (double a, double auf_me, short me_einh_kun, double pr_vk)
/**
Bestandsbuchung vorbereiten.
**/
{
	double auf_me_vgl;
	double buchme;
	char datum [12];
    long lgrort;

	if (bsd_kz == 0) return;
	if (BsdArtikel (a) == FALSE) return;
    auf_me_vgl = GetAufMeVgl (a, auf_me,me_einh_kun);
	if (auf_me_vgl == akt_me) return;
	buchme = auf_me_vgl - akt_me;

	bsd_buch.nr  = aufk.auf;
	strcpy (bsd_buch.blg_typ, "A");
	bsd_buch.mdn = aufk.mdn;
	bsd_buch.fil = aufk.fil;
	bsd_buch.kun_fil = aufk.kun_fil;
	bsd_buch.a   = a;
	sysdate (datum);
	bsd_buch.dat = dasc_to_long (datum);
	bsd_buch.lieferdat = aufk.lieferdat;
	systime (bsd_buch.zeit);
	strcpy (bsd_buch.pers, sys_ben.pers);
	if (aufk.tou && tou.lgr > 0)
	{
            sprintf (bsd_buch.bsd_lgr_ort, "%ld", tou.lgr);
	}
	else
	{
            lgrort = KompAuf::GetLagerort (akt_lager, a);
            sprintf (bsd_buch.bsd_lgr_ort, "%ld", lgrort);
	}
	bsd_buch.qua_status = 0;
	bsd_buch.me = buchme;
	bsd_buch.bsd_ek_vk = pr_vk;
    strcpy (bsd_buch.chargennr, aufps.ls_charge);
    strcpy (bsd_buch.ident_nr, "");
    strcpy (bsd_buch.herk_nachw, "");
    sprintf (bsd_buch.lief, "%ld", aufk.kun);
    bsd_buch.auf = aufk.auf;
    strcpy  (bsd_buch.verfall, "");
    bsd_buch.delstatus = 0;
    strcpy  (bsd_buch.err_txt, "");
	BsdBuch.dbinsert ();
}

void AUFPLIST::TestPfand (double a)
/**
Bei Pfandverknuepfung Pfand einfuegen.
**/
{
    double auf_me;

    if (autopfand == FALSE) return;
    if (_a_bas.a_typ != 1) return;
    if (a_hndw.a_pfa == (double) 0.0) return;
    auf_me = ratod (aufps.auf_me);
    AppendLine ();
    sprintf (aufps.auf_me, "%.0lf", auf_me);
    sprintf (aufps.a, "%.0lf", a_hndw.a_pfa);
//    fetchaDirect (eListe.GetAktRow ());
    fetcha ();
    testme ();
    AnzAufWert ();
    AnzAufGew ();
}

void AUFPLIST::TestLeih (double a)
/**
Bei Pfandverknuepfung Pfand einfuegen.
**/
{
	double a_leih;

    if (!IsPartyService) return;
    if (aufps.a_leih == (double) 0.0) return;
	a_leih = aufps.a_leih;
    AppendLine ();
	_a_bas.a = a_leih;
    sprintf (aufps.a, "%.0lf", _a_bas.a);
	int row = eListe.GetAktRow ();
    memcpy (&aufptab[eListe.GetAktRow ()], &aufps, sizeof (struct AUFPS));
    fetcha ();
	PostMessage (NULL, WM_KEYDOWN, VK_UP, 0l);
}

int AUFPLIST::AfterPrEnter ()
{
    testpr ();
    eListe.ShowAktRow ();
        if (InfoProcessRefresh > 0)  //031012
        {
	       memcpy (&aufptab[eListe.GetAktRow ()], &aufps, sizeof (struct AUFPS));
           WritePos (eListe.GetAktRow ());
	       commitwork ();
		   beginwork ();
        }

	return 0;
}


int AUFPLIST::WriteRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{
    if (testme () == -1) return -1;
    if (testpr () == -1) return -1;
    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    if (ratod (aufps.a) == (double) 0.0)
    {
        eListe.DeleteLine ();
        return (1);
    }

	BucheBsd (ratod (aufps.a), ratod (aufps.auf_me), atoi (aufps.me_einh_kun),
		      ratod (aufps.auf_vk_pr));

    if (eListe.IsAppend ())
    {
        TestPfand (ratod (aufps.a));
		TestLeih (ratod (aufps.a));
    }
    return (0);
}

int AUFPLIST::TestRow (void)
/**
Zeile beim Verlassen pruefen.
**/
{

    if (ratod (aufps.auf_me) == (double) 0.0) return 0;
    if (testme () == -1) return -1;
    if (testpr () == -1) return -1;
    set_fkt (NULL, 9);
    SetFkt (9, leer, NULL);
    return (0);
}

void AUFPLIST::GenNewPosi (void)
/**
Neue Positionsnummern in 10-er Schritten generieren.
**/
{
    int i;
    int row;
    int recs;
    long posi;

    row  = eListe.GetAktRow ();
    memcpy (&aufptab[row], &aufps, sizeof (struct AUFPS));
    recs = eListe.GetRecanz ();
    posi = 10;
    for (i = 0; i < recs; i ++, posi += 10)
    {
        sprintf (aufptab[i].posi, "%ld", posi);
    }
    eListe.DisplayList ();
    memcpy (&aufps, &aufptab[row], sizeof (struct AUFPS));
}

long AUFPLIST::GetNextPos_id (void)
/**
Neue Pos-Id holen
**/
{
    int i;
    int recs;
    long pos_id = 0;

    recs = eListe.GetRecanz ();
    for (i = 0; i < recs; i ++)
    {
        if (aufptab[i].pos_id > pos_id) pos_id = aufptab[i].pos_id;
    }
	pos_id ++;
	return pos_id;
}


int AUFPLIST::PosiEnd (long posi)
/**
Positionnummer testen.
**/
{
    int row;
    int recs;
    long nextposi;

    row  = eListe.GetAktRow ();
    recs = eListe.GetRecanz ();

    if (row >= recs - 1)
    {
            return FALSE;
    }

    nextposi = atol (aufptab [row + 1].posi);
    if (nextposi <= posi)
    {
        GenNewPosi ();
        return TRUE;
    }
    return FALSE;
}

void AUFPLIST::TestMessage (void)
{
	MSG msg;

    if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
    {
          TranslateMessage(&msg);
          DispatchMessage(&msg);
    }
}

void AUFPLIST::ActivateStd ()
{
	 int akt_pos;
     int aufpanz;

	 if (LISTHANDLER->GetStndAll ()->anz == 0)
	 {
		 return;
	 }
	 akt_pos = eListe.GetAktRow ();
     aufpanz = eListe.GetRecanz ();
     aufpanz = LISTHANDLER->Activate (aufptab, aufpanz, &akt_pos);
	 eListe.DestroyFocusWindow ();

//     instance->InitSwSaetze ();
//     eListe.SetRecanz (0);

     InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
     UpdateWindow (eListe.Getmamain3 ());
	 eListe.SetRecanz (aufpanz);
     eListe.DestroyVTrack ();
	 eListe.MoveListWindow ();
//     eListe.AppendLine ();
     AppendLine ();
     syskey = KEYUP;
     eListe.FocusUp ();
     InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
	 UpdateWindow (eListe.Getmamain3 ());
	 akt_pos = 0;
     memcpy (&aufps, &aufptab [akt_pos], sizeof (struct AUFPS));
	 eListe.SetPos (akt_pos, eListe.FirstColumn ());
	 eListe.SetVPos (akt_pos);
     eListe.ShowAktRow ();
	 AnzAufWert ();
	 AnzAufGew ();
     SetFkt (7, "umschalten", KEY7);
	 set_fkt (SwitchStd, 7);
//     SetFkt (7, leer, KEY7);
//	 set_fkt (NULL, 7);
	 LISTHANDLER->SetLoadComplete ();
	 auf_vk_pr = 0.0;
}

int AUFPLIST::SwitchStd ()
{
	 int akt_pos;
     int aufpanz;

//	 eListe.ReadFocusText ();
	 auf_vk_pr = 0.0;
	 SendMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0);
	 akt_pos = eListe.GetAktRow ();
     aufpanz = eListe.GetRecanz ();
     aufpanz = LISTHANDLER->Switch (aufptab, &akt_pos);
	 eListe.DestroyFocusWindow ();
     InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
     UpdateWindow (eListe.Getmamain3 ());
	 eListe.SetRecanz (aufpanz);
     eListe.DestroyVTrack ();
	 eListe.MoveListWindow ();
     syskey = KEYUP;
     eListe.FocusUp ();
     InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
	 UpdateWindow (eListe.Getmamain3 ());
	 akt_pos = 0;
	 auf_vk_pr = 0.0;
     memcpy (&aufps, &aufptab [akt_pos], sizeof (struct AUFPS));
	 eListe.SetAppend (0);
	 eListe.SetInsAttr ();
	 eListe.SetPos (akt_pos, eListe.FirstColumn ());
	 eListe.SetVPos (akt_pos);
     eListe.ShowAktRow ();
	 AnzAufWert ();
	 AnzAufGew ();
	 return 0;
}

int AUFPLIST::doActivate (void)
{
	ActivateStd ();
	instance->Ampel.SetState (CAmpel::No);
	instance->Ampel.Invalidate ();
	return 0;
}


int AUFPLIST::doStd (void)
/**
Standardauftrag.
**/
{
         double a;
         double me;
         double pr_vk;
		 int me_einh_kun;
         int ret;
         int row;
         int lrow;
		 int SaveRow, SaveColumn;
         int wo_tag;
         char wo_tags [5];
         char datum [12];


         save_fkt (5);
         save_fkt (6);
         save_fkt (7);
         save_fkt (8);
         save_fkt (9);
         save_fkt (10);
         save_fkt (11);
         save_fkt (12);
		 SaveRow     = eListe.GetAktRow ();
		 SaveColumn  = eListe.GetAktColumn ();


		 if (StndDirect == 3)
		 {
			 instance->Ampel.SetState (CAmpel::Red);
			 instance->Ampel.Invalidate ();
			 instance->Ampel.SetPlay (StndWithSound);

			 ClientOrder.SetWindow (StndWithWindow);
			 ClientOrder.SetThreadAction (instance);
			 ClientOrder.SetKeys (aufk.mdn, aufk.fil, aufk.kun_fil, aufk.kun);
			 ClientOrder.SetRdOptimize (StndRdOptimize);
			 if (RefreshStnd)
			 {
					ClientOrder.SetRdOptimize (CClientOrder::No);
					ClientOrder.SetRefreshRdOptimize (TRUE);
			 }

			ClientOrder.Start ("ClientOrders");


//			 ClientOrder.StartServer ();
//             ClientOrder.Load ();
//			 ClientOrder.StartServer ();
//			 SetFkt (6, "Laden", KEY6);
//			 set_fkt (doActivate, 6);
			 return 0;
		 }

	     StndAuf.StndDirect = FALSE;
         if (StndDirect == 2)
		 {
			     StndAuf.StndDirect = TRUE;
		 }

         if (FilStandard == FALSE || aufk.kun_fil == 0)
         {
                ret = StndAuf.StdAuftrag (eListe.Getmamain3 (),
                                          aufk.mdn, aufk.fil,
                                          aufk.kun, aufk.kun_fil);
         }
         else
         {
                sysdate (datum);
                wo_tag = get_wochentag (datum);
                sprintf (wo_tags, "%d", wo_tag);
                ret = StndAuf.StdFilAuftrag (mamain1, eListe.Getmamain3 (),
                                             aufk.mdn, (short) aufk.kun,
                                             wo_tags);

         }
         restore_fkt (5);
         restore_fkt (6);
         restore_fkt (7);
         restore_fkt (8);
         restore_fkt (9);
         restore_fkt (10);
         restore_fkt (11);
         restore_fkt (12);
         if (ret == 0)
         {
             return 0;
         }
         if (ret == 1)
         {
                  a = StndAuf.GetStda ();
                  me = StndAuf.GetStdme ();
                  me_einh_kun = StndAuf.GetMeEinhKun ();
                  pr_vk = StndAuf.GetStdpr_vk ();
                  sprintf (aufps.a,  "%.0lf", a);
                  sprintf (aufps.auf_me, "%.3lf", me);
                  sprintf (aufps.auf_vk_pr, "%lf", pr_vk);
                  sprintf (aufps.me_einh_kun, "%ld", me_einh_kun);
                  memcpy (&aufptab [eListe.GetAktRow ()], &aufps,
                          sizeof (struct AUFPS));
//                  fetcha ();
                  fetchaDirect (eListe.GetAktRow (), FALSE);
         }
         else if (ret == 2)
         {
				  eListe.DestroyFocusWindow ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
                  UpdateWindow (eListe.Getmamain3 ());
			      NoArtMess = TRUE;
                  row = 0;
                  lrow = eListe.GetAktRow ();
                  while (StndAuf.GetStdRow (row, &a, &me, &pr_vk, &me_einh_kun))
                  {
// 					    TestMessage ();
                        eListe.SetVPos (lrow);
	                    if (ratod (aufptab[lrow].a) ||
                            lrow >= eListe.GetRecanz ())
                        {
//                            AppendLine ();
							  eListe.SetRecanz (lrow);
                        }

                        sprintf (aufps.a,  "%.0lf", a);
                        sprintf (aufps.auf_me, "%.3lf", me);
                        sprintf (aufps.me_einh_kun, "%ld", me_einh_kun);
                        rechne_liefme ();
                        row ++;
                        sprintf (aufps.auf_vk_pr, "%lf", pr_vk);
 				        sprintf (aufps.sa_kz_sint, "%1d", 0);
                        memcpy (&aufptab [lrow], &aufps,
                          sizeof (struct AUFPS));
                        fetchaDirect (lrow, TRUE);
                        lrow ++;
                  }
                  if (ratod (aufptab[lrow].a) ||
                           lrow >= eListe.GetRecanz ())
                  {
//                            AppendLine ();
							  eListe.SetRecanz (lrow);
                  }
			      NoArtMess = FALSE;
                  AppendLine ();
                  syskey = KEYUP;
                  eListe.FocusUp ();
                  InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
				  UpdateWindow (eListe.Getmamain3 ());
         }
         memcpy (&aufps, &aufptab [SaveRow], sizeof (struct AUFPS));
		 eListe.SetPos (SaveRow, eListe.FirstColumn ());
         eListe.SetVPos (SaveRow);
         eListe.ShowAktRow ();
	     AnzAufWert ();
	     AnzAufGew ();
         return 0;
}
HANDLE AUFPLIST::StarteProcess (void) //testtest
{
			HANDLE Pid;
		   

	    char *etc;
		char buffer [512];
		char sqlstr [512];
		FILE *fp;
		int x, y, cx, cy;
		int anz;
		BOOL AllwaysOnTop = TRUE;

		etc = getenv ("BWSETC");
		if (etc == NULL) return NULL;
		if (strlen(clipped(InfoProcess)) <2 ) return NULL;

		x = -4;
		y = 120;
		cx = 800;
		cy = 400;
		sprintf (buffer, "%s\\%s.rct", etc,"51100_ext");
		fp = fopen (buffer, "r");
		if (fp == NULL) return NULL;
		while (fgets (buffer, 511, fp))
		{
			cr_weg (buffer);
			anz = wsplit (buffer, " ");
			if (anz < 2) continue;
			if (strcmp (wort[0], "left") == 0)
			{
				x = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "top") == 0)
			{
				y = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "right") == 0)
			{
				cx = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "bottom") == 0)
			{
				cy = atoi (wort[1]);
			}
		}
		fclose (fp);
		//false sthet f�r den Focus das Aufgerufene Java-Teil schnappt sich dann den Focus nicht
//		sprintf (buffer, "%s %ld %ld false %ld %ld %hd %hd %ld", InfoProcess, x,y,cx,cy, aufk.mdn,aufk.fil,aufk.auf);
//		sprintf (sqlstr, "\"select a_bas.wg,sum(aufp.auf_me * aufp.auf_vk_pr) from aufp,a_bas where aufp.a = a_bas.a and aufp.mdn = %hd and aufp.fil = %hd and aufp.auf = %ld group by a_bas.wg order by a_bas.wg\"", aufp.mdn,aufp.fil,aufp.auf);
		sprintf (sqlstr, InfoSQL, aufk.mdn,aufk.fil,aufk.auf,aufk.mdn,aufk.fil,aufk.auf);
//		sprintf (buffer, "%s %ld %ld false %ld %ld \"%s\" %ld %hd", InfoProcess, x,y,cx,cy, sqlstr, InfoProcessRefresh,AllwaysOnTop);
		sprintf (buffer, "%s %ld %ld false %ld %ld \"%s\" %ld %s", InfoProcess, x,y,cx,cy, sqlstr, InfoProcessRefresh, "true");

        Pid = ProcExecPid (buffer, SW_SHOWNOACTIVATE, x, y, cx, cy);
//        Pid = ProcExecPid (buffer, SW_HIDE, x, y, cx, cy);


	return Pid ;
}

int AUFPLIST::KillProcess (void) //testtest
{
		   
	if (ProcessID != NULL) TerminateProcess (ProcessID, 0);
	ProcessID = NULL;

	return 0 ;
}


int AUFPLIST::Querya (void)
/**
Auswahl ueber Artikel.
**/
{
       int ret;

	   if (a_kun_smt == 2 || preis0_mess == 2)
	   {
		   SetQueryKun (kun.kun);
	   }

	   if (searchadirect)
	   {
            ret = QClass.searcha (eListe.Getmamain3 ());
	   }
       else
	   {
            ret = QClass.querya (eListe.Getmamain3 ());
	   }
       set_fkt (dokey5, 5);
//       set_fkt (InsertLine, 6);
//       set_fkt (DeleteLine, 7);
//       set_fkt (AppendLine, 8);
       set_fkt (WriteAllPos, 12);
       if (ret == FALSE)
       {
           eListe.SetFeldFocus0 (eListe.GetAktRow (),
                                 eListe.GetAktColumn ());
           return 0;
       }
       UpdateWindow (mamain1);
       sprintf (aufptab[eListe.GetAktRow ()].a, "%.0lf", _a_bas.a);
       eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (),
                             eListe.GetAktColumn ());
       PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0l);
       return 0;
}


int AUFPLIST::Savea (void)
/**
Artikelnummer sichern.
**/
{
       Akta = ratod (aufps.a);
	   if (! numeric (aufps.a))
	   {
		   sprintf (aufps.a, "%13.0lf", 0.0);
           memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
           eListe.ShowAktRow ();
	   }
       set_fkt (Querya, 9);
       SetFkt (9, auswahl, KEY9);
       set_fkt (doStd, 10);
       SetFkt (10, standardauf, KEY10);
       return 0;
}

int AUFPLIST::ChMeEinh (void)
/**
Auswahl Auftragsmengeneinheit.
**/
{

       kumebest.mdn = aufk.mdn;
       kumebest.fil = aufk.fil;
       kumebest.kun = aufk.kun;
       strcpy (kumebest.kun_bran2, kun.kun_bran2);
       kumebest.a = ratod (aufps.a);
       if (ratod (aufps.a))
       {
           strcpy (kumebest.kun_bran2, kun.kun_bran2);
           einh_class.AktAufEinh (aufk.mdn, aufk.fil, aufk.kun,
                                  ratod (aufps.a), atoi (aufps.me_einh_kun));
       }
       else
       {
           strcpy (kumebest.kun_bran2, kun.kun_bran2);
           einh_class.SetAufEinh (1);
       }
       EnableWindow (mamain1, FALSE);
       einh_class.ChoiseEinh ();
       EnableWindow (mamain1, TRUE);
       _a_bas.a = ratod (aufps.a);
       if (syskey == KEY5)
	   {
           eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
		   return 0;
	   }
       ReadMeEinh ();

       memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
       eListe.ShowAktRow ();
       eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
       return 0;
}

long AUFPLIST::GenAufpTxt0 (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	int count;
	long nr;

	nr = 0l;
    nr = AutoNr.GetNr (nr);
	count = 0;
	while (nr == 0l)
	{
		         Sleep (10);
		         count ++;
				 if (count == 10) break;
                 nr = AutoNr.GetNr (nr);
	}
	return nr;
}

BOOL AUFPLIST::TxtNrExist (long nr)
{
	   int dsqlstatus;

	   if (wa_pos_txt)
	   {
			   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
			   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
			   DbClass.sqlin ((long *)  &nr, 2, 0);
			   dsqlstatus = DbClass.sqlcomm ("select posi from aufp_txt "
				                             "where mdn = ? "
											 "and fil = ? "
											 "and posi = ?");
	   }
	   else
	   {
			   DbClass.sqlin ((long *)  &nr, 2, 0);
			   dsqlstatus = DbClass.sqlcomm ("select nr from aufpt where nr = ?");
	   }
	   if (dsqlstatus == 100) return FALSE;
	   return TRUE;
}


long AUFPLIST::GenAufpTxt (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	 long nr;

	 while (TRUE)
	 {
		 nr = GenAufpTxt0 ();
		 if (TxtNrExist (nr) == FALSE) break;
		 if (nr > 99999999)
		 {
			 return 0l;
		 }
	 }
	 return nr;
}



int AUFPLIST::Texte (void)
/**
Freie Artikeltexte erfassen.
**/
{
    long aufp_txt;
//	int count;

    eListe.SetNoRecNr (FALSE);
    set_fkt (NULL, 9);
    SetFkt (9, leer, 0);
    set_fkt (NULL, 10);
    SetFkt (10, leer, 0);
    set_fkt (NULL, 11);
    SetFkt (11, leer, 0);
    TListe.SethMainWindow (mamain1);
    TListe.SetListLines (12);
    EnableWindow (mamain1, FALSE);
    aufp_txt = atol (aufps.aufp_txt);
	TListe.PosTxtNrPar = PosTxtNrPar;

	if (aufp_txt == 0l) aufp_txt = GenAufpTxt ();

	if (aufp_txt != 0l)
	{
            aufp_txt = TListe.EnterAufp (aufk.mdn , aufk.fil, aufk.auf,
                         atol (aufps.posi), aufp_txt);
			sprintf (aufps.aufp_txt, "%ld", aufp_txt);
            if (aufp_txt == 0l)
			{
				syskey = KEY5;
			}
	}
	else
	{
		     syskey = KEY5;
    }

	if (NoRecNr)
	{
	   eListe.SetNoRecNr (TRUE);
    }
    EnableWindow (mamain1, TRUE);
    SetActiveWindow (mamain1);

    if (syskey != KEY5)
    {
		     if (!TxtByDefault && pControls.IsActive ())
			 {
				 PosTxtKz = pControls.GetPosTxtKz (aufps.pos_txt_kz);
				 aufps.pos_txt_kz = (short) PosTxtKz;

			 }
			 else
			 {
				 aufps.pos_txt_kz = (short) pControls.LiefRechVisible;
			 }
             sprintf (aufps.aufp_txt, "%ld", aufp_txt);
             memcpy (&aufptab[eListe.GetAktRow()],
                     &aufps, sizeof (struct AUFPS));
    }
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);

	if (lutz_mdn_par)
	{
		set_fkt (ProdMdn, 8);
	}
    else if (rab_prov_kz)
	{
		set_fkt (PosRab, 8);
	}


    set_fkt (WriteAllPos, 12);
    set_fkt (Schirm, 11);

    SetFkt (6, einfuegen, KEY6);
    SetFkt (7, loeschen, KEY7);

	if (lutz_mdn_par)
	{
		SetFkt (8, prodmdn, KEY8);
	}
    else if (rab_prov_kz)
	{
		SetFkt (8, posrab, KEY8);
	}

    SetFkt (11, vollbild, KEY11);
    set_fkt (ChMeEinh, 9);
    SetFkt (9, einhausw, KEY9);
    set_fkt (Texte, 10);
    SetFkt (10, texte, KEY10);
	eListe.ShowAktRow ();
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
    return 0;
}

int AUFPLIST::setkey9me (void)
/**
Artikelnummer sichern.
**/
{
/*
       if (ratod (aufps.a))
       {
           einh_class.AktAufEinh (aufk.mdn, aufk.fil, aufk.kun,
                                  ratod (aufps.a), atoi (aufps.me_einh_kun));
       }
       else
       {
           einh_class.SetAufEinh (1);
       }
*/
//FS-109 A
		if (kun.min_best == 1)
		{
    	    int pos = eListe.GetAktRow ();
			double dauf_me = ratod (aufptab[pos].auf_me);
			if (dauf_me == (double) 0)
			{
		        if (ratod(aufps.a) != _a_bas.a) lese_a_bas (ratod(aufps.a));
				dauf_me = (double) _a_bas.min_bestellmenge;
				if (dauf_me > (double) 0 && dauf_me < (double) 32000) //NULL-Value abfangen
				{
					sprintf (aufptab[pos].auf_me, "%.3lf", dauf_me);
					sprintf (aufps.auf_me, "%.3lf", dauf_me); 
				}
				eListe.ShowAktRow ();
			}
		}
//FS-109 E

       testmeOK = FALSE;
       set_fkt (ChMeEinh, 9);
       SetFkt (9, einhausw, KEY9);
       set_fkt (Texte, 10);
       SetFkt (10, texte, KEY10);
       return 0;
}

int AUFPLIST::setkey9basis (void)
/**
Artikelnummer sichern.
**/
{
//	   auf_vk_pr = ratod (aufps.auf_vk_pr);
       set_fkt (ShowBasis, 9);
       SetFkt (9, basisme, KEY9);
       return 0;
}

char *SaText = "Sonderangebot";
char *DaText = "Dauertiefpreis";

static ITEM iSaTxt  ("",  DaText, "", 0);

static field _fSaTxt [] = {
&iSaTxt,        13,  0, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fSaTxt = {1, 0, 0, _fSaTxt, 0, 0, 0, 0, NULL};
static char *satxt = SaText;


void AUFPLIST::MoveSaW (void)
/**
Fenster Sonderangebot bewegen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;
        if (AufMehWnd == NULL) return;
        if (SaWindow == NULL) return;

	    memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  11 * tm.tmHeight);
        cx = 15 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;
		MoveWindow (SaWindow, x, y, cx, cy, TRUE);
}


HWND AUFPLIST::CreateSaW (void)
/**
Fenster fuer Sonderangebot erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
		long aktionlen;

        if (hMainWin == NULL) return NULL;

		aktionlen = WaPreis.GetAktionLen ();
		if (dauertief && (aktionlen > dauertief))
		{
			iSaTxt.SetFeld (DaText);
			satxt = DaText;
		}
		else
		{
			iSaTxt.SetFeld (SaText);
			satxt = SaText;
		}
		memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  11 * tm.tmHeight);
        cx = 15 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;

        SaWindow       = CreateWindow (
                                       "StaticMess",
                                       "",
                                       WS_BORDER |
                                       WS_POPUP | WS_VISIBLE,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);


        return SaWindow;
}

void AUFPLIST::PaintSa (HDC hdc)
/**
Text anzeigen.
**/
{
	  RECT rect;
	  SIZE size;
	  int x,y;
	  extern HFONT stdHfont;

	  if (SaWindow == NULL) return;

	  GetClientRect (SaWindow, &rect);
      SelectObject (hdc, stdHfont);
      GetTextExtentPoint32 (hdc, satxt, strlen (satxt), &size);
	  x = max (0, (rect.right  - rect.left - size.cx) / 2);
	  y = max (0, (rect.bottom - rect.top  - size.cy) / 2);
	  SetBkMode (hdc, TRANSPARENT);
      SetTextColor (hdc,MessCol);
      SetBkColor (hdc, MessBkCol);
	  TextOut (hdc, x, y, satxt, strlen (satxt));
}


BOOL AUFPLIST::FillColRect (HDC hdc, RECT *rect, int einzel, BOOL AktZeile) //WAL-70
{
	BOOL ret = FALSE;
	if (ListColors)
	{

		if (AktZeile) //WAL-70
		{
			HBRUSH hBrush = CreateSolidBrush (AktZeile_bColor);
			FillRect (hdc, rect, hBrush);
			DeleteObject (hBrush);
            ret = TRUE;
		}
		else
		{
			HBRUSH hBrush = CreateSolidBrush (RGB(255,255,255));
			FillRect (hdc, rect, hBrush);
			DeleteObject (hBrush);
			ret = TRUE;
		}


		if (aufps.userdef1 == TRUE && einzel == Userdef1 && F2Userdef1)  //WAL-70
		{
			HBRUSH hBrush = CreateSolidBrush (userdef1_bColor);
			FillRect (hdc, rect, hBrush);
			DeleteObject (hBrush);
			ret = TRUE;
		}
	}
	return ret;
}


BOOL AUFPLIST::SetRowBkColor (HDC hdc, int einzel,BOOL AktZeile) //WAL-70
{
	BOOL ret = FALSE;
	if (ListColors)
	{
		if (AktZeile) //WAL-70
		{
			SetBkColor (hdc, AktZeile_bColor);
		}
		else
		{
				SetBkColor (hdc, RGB(255,255,255));
		}

		if (aufps.userdef1 == TRUE && einzel == Userdef1 && F2Userdef1)  //WAL-70
		{
			SetBkColor (hdc, userdef1_bColor);
			ret = TRUE;
		}

	}
	return ret;
}


void AUFPLIST::CreateSa (void)
/**
Fenster Sonderpreis anzeigen.
**/
{
	   if (SaWindow) return;

	   SaWindow = CreateSaW ();
/*
	   hdc = GetDC (SaWindow);
	   PaintSa (hdc);
	   ReleaseDC (SaWindow, hdc);


	   currentf = currentfield;
 	   SetStaticWhite (TRUE);
	   display_form (SaWindow, &fSaTxt, 0, 0);
	   SetStaticWhite (FALSE);
       currentfield = currentf;
*/
}

void AUFPLIST::DestroySa (void)
/**
Fenster mit Sonderpreis loeschen.
**/
{
	  if (SaWindow == NULL)
	  {
		  return;
	  }
	  CloseControls (&fSaTxt);
	  DestroyWindow (SaWindow);
	  SaWindow = NULL;
}

void AUFPLIST::TestSaPr (void)
{
/*
       char lieferdat [12];
       int dsqlstatus;
       double pr_ek;
       double pr_vk;
*/
       short sa;

       if (ratod (aufps.auf_vk_pr) == 0)
	   {
		   DestroySa ();
		   return;
	   }

	   sa = atoi( aufps.sa_kz_sint);
/*
       dlong_to_asc (aufk.lieferdat, lieferdat);
       dsqlstatus = WaPreis.preise_holen (aufk.mdn,
                                          aufk.fil,
                                          aufk.kun_fil,
                                          aufk.kun,
                                           ratod (aufps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
*/
	   if (sa)
	   {
		   CreateSa ();
	   }
	   else
	   {
		   DestroySa ();
	   }
}

void AUFPLIST::MovePlus (void)
/**
Fenster Sonderangebot bewegen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;
        if (AufMehWnd == NULL) return;
		if (PlusWindow == NULL) return;

	    memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  9 * tm.tmHeight);
        cx = 3 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;
		MoveWindow (PlusWindow, x, y, cx, cy, TRUE);
}

HWND AUFPLIST::CreatePlus (void)
/**
Fenster fuer Sonderangebot erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return NULL;

	    memcpy (&tm, &textm, sizeof (tm));

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  9 * tm.tmHeight);
        cx = 3 * tm.tmAveCharWidth + 2;
        x = wrect.left + rect.right - cx + 1;
        cy = 2 * tm.tmHeight;

        PlusWindow       = CreateWindow (
                                       "StaticMess",
                                       "",
                                       WS_BORDER |
                                       WS_POPUP | WS_VISIBLE,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);


        return PlusWindow;
}

void AUFPLIST::PaintPlus (HDC hdc)
/**
+ anzeigen.
**/
{
	  RECT rect;
	  SIZE size;
	  int x,y;
	  extern HFONT stdHfont;
	  static char *txt = "+";

	  if (PlusWindow == NULL) return;

	  GetClientRect (PlusWindow, &rect);
      SelectObject (hdc, stdHfont);
      GetTextExtentPoint32 (hdc, txt, strlen (txt), &size);
	  x = max (0, (rect.right  - rect.left - size.cx) / 2);
	  y = max (0, (rect.bottom - rect.top  - size.cy) / 2);
	  SetBkMode (hdc, TRANSPARENT);
      SetTextColor (hdc,MessCol);
      SetBkColor (hdc, MessBkCol);
	  TextOut (hdc, x, y, txt, strlen (txt));
}


void AUFPLIST::DestroyPlus (void)
/**
Fenster mit + loeschen.
**/
{
	  if (PlusWindow == NULL)
	  {
		  return;
	  }
	  DestroyWindow (PlusWindow);
	  PlusWindow = NULL;
}

void AUFPLIST::FillDM (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kundenwaehrung ist DM
**/
{
	  double pr_ek_euro;
	  double pr_vk_euro;
	  double kurs;


	  kurs = _mdn.konversion;
	  if (kurs == 0.0) kurs = 1.0;

      if (atoi (_mdn.waehr_prim) == 1)
	  {
/* Basiswaehrung ist DM                  */
	           pr_ek_euro = pr_ek / kurs;
	           pr_vk_euro = pr_vk / kurs;
	  }
	  else if (atoi (_mdn.waehr_prim) == 2)
	  {
/* Basiswaehrung ist EURO                   */
	           pr_ek_euro = pr_ek * kurs;
	           pr_vk_euro = pr_vk * kurs;
	  }
	  else
	  {
	           pr_ek_euro = pr_ek;
	           pr_vk_euro = pr_vk;
	  }
      sprintf (aufps.auf_vk_pr,     "%lf",    pr_ek);
      sprintf (aufps.auf_lad_pr,    "%lf",    pr_vk);
      sprintf (aufps.auf_vk_dm,     "%lf", pr_ek);
      sprintf (aufps.auf_lad_dm,    "%lf", pr_vk);
      sprintf (aufps.auf_vk_euro,   "%lf",  pr_ek_euro);
      sprintf (aufps.auf_lad_euro,  "%lf",  pr_vk_euro);
}

void AUFPLIST::FillEURO (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kunden waehrung ist EURO
**/
{
	  double pr_ek_dm;
	  double pr_vk_dm;
	  double kurs;

	  kurs = _mdn.konversion;
	  if (kurs == 0.0) kurs = 1.0;

	  if (atoi (_mdn.waehr_prim) == 1)
/* Basiswaehrung ist DM                  */
	  {
	          pr_ek_dm = pr_ek * kurs;
	          pr_vk_dm = pr_vk * kurs;
	  }
	  else if (atoi (_mdn.waehr_prim) == 2)
	  {
/* Basiswaehrung ist EURO                   */
	           pr_ek_dm = pr_ek * kurs;
	           pr_vk_dm = pr_vk * kurs;
	  }
      sprintf (aufps.auf_vk_dm,    "%lf",    pr_ek_dm);
      sprintf (aufps.auf_lad_dm,   "%lf",    pr_vk_dm);
      sprintf (aufps.auf_vk_euro,  "%lf",  pr_ek);
      sprintf (aufps.auf_lad_euro, "%lf",  pr_vk);
      sprintf (aufps.auf_vk_pr,    "%lf",    pr_ek);
      sprintf (aufps.auf_lad_pr,   "%lf",    pr_vk);
	  if (ld_pr_prim && atoi (_mdn.waehr_prim) == 1)
	  {
                sprintf (aufps.auf_lad_dm,   "%8.4lf",    pr_vk);
	  }
}

void AUFPLIST::FillFremd (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kunden waehrung ist DM
**/
{
	  double pr_ek_dm;
	  double pr_vk_dm;
	  double pr_ek_euro;
	  double pr_vk_euro;
	  double kurs1;
	  double kurs2;
	  short  faktor;
	  int dsqlstatus;

	  kurs1 = _mdn.konversion;
	  if (kurs1 == 0.0) kurs1 = 1.0;

      DbClass.sqlin ((short *)   &aufk.mdn, 1, 0);
      DbClass.sqlin ((short *)   &aufk.waehrung, 1, 0);
	  DbClass.sqlout ((double *) &kurs2, 3, 0);
	  DbClass.sqlout ((short *)  &faktor, 1, 0);
	  dsqlstatus = DbClass.sqlcomm ("select kurs, faktor from devise "
		                            "where mdn = ? "
									"and devise_nr = ?");
	  if (dsqlstatus != 0)
	  {
            if (atoi (_mdn.waehr_prim) == 1)
			{
				pr_ek_dm = pr_ek;
				pr_vk_dm = pr_vk;
				pr_ek_euro = pr_ek / kurs1;
				pr_vk_euro = pr_vk / kurs1;
			}
			else
			{
				pr_ek_dm = pr_ek * kurs1;
				pr_vk_dm = pr_vk * kurs1;
				pr_ek_euro = pr_ek;
				pr_vk_euro = pr_vk;
			}
	  }
	  else
	  {
		    if (atoi (_mdn.waehr_prim) == 1)
			{
	            pr_ek_dm = pr_ek * kurs2 / faktor;
	            pr_vk_dm = pr_vk * kurs2 / faktor;
				pr_ek_euro = pr_ek_dm / kurs1;
				pr_vk_euro = pr_vk_dm / kurs1;
			}
			else
			{
                pr_ek_euro = pr_ek * kurs2 / faktor;
                pr_vk_euro = pr_vk * kurs2 / faktor;
				pr_ek_dm   = pr_ek_euro / kurs1;
				pr_vk_dm   = pr_vk_euro / kurs1;
			}
	  }
      sprintf (aufps.auf_vk_dm,     "%lf", pr_ek_dm);
      sprintf (aufps.auf_lad_dm,    "%lf", pr_vk_dm);
      sprintf (aufps.auf_vk_euro,   "%lf", pr_ek_euro);
      sprintf (aufps.auf_lad_euro,  "%lf", pr_vk_euro);
//      sprintf (aufps.auf_vk_fremd,  "%lf", pr_ek);
//      sprintf (aufps.auf_lad_fremd, "%lf", pr_vk);
      sprintf (aufps.auf_vk_pr,     "%lf", pr_ek);
      sprintf (aufps.auf_lad_pr,    "%lf", pr_vk);
	  if (ld_pr_prim)
	  {
                sprintf (aufps.auf_lad_dm,   "%8.4lf",    pr_vk);
	  }
}


void AUFPLIST::InitWaehrung (void)
{
	   sprintf (aufps.auf_vk_euro,  "%lf", 0.0);
	   sprintf (aufps.auf_lad_euro, "%lf", 0.0);
//	   sprintf (aufps.auf_vk_fremd, "%lf", 0.0);
//	   sprintf (aufps.auf_lad_fremd,"%lf", 0.0);
}

void AUFPLIST::FillWaehrung (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen.
**/
{
	   InitWaehrung ();
       if (aufk.waehrung == 0)
       {
             aufk.waehrung = 1;
       }
	   switch (aufk.waehrung )
	   {
	         case 1 :
				 FillDM (pr_ek, pr_vk);
				 break;
	         case 2 :
				 FillEURO (pr_ek, pr_vk);
				 break;
            default :
				 FillFremd (pr_ek, pr_vk);
				 break;
	   }
}

void AUFPLIST::FillAktWaehrung ()
/**
Anzeigefelder fuer EK und VK fuellen.
**/
{
	   if (aufk.waehrung == 1 || aufk.waehrung == 0)
	   {
           sprintf (aufps.auf_vk_pr,    "%lf",    aufp.auf_vk_pr);
           sprintf (aufps.auf_lad_pr,   "%lf",    aufp.auf_lad_pr);
	   }
	   else if (aufk.waehrung == 2)
	   {
           sprintf (aufps.auf_vk_pr,    "%lf",    aufp.auf_vk_euro);
           sprintf (aufps.auf_lad_pr,   "%lf",    aufp.auf_lad_euro);
     	   if (ld_pr_prim)
		   {
                sprintf (aufps.auf_lad_dm,   "%lf",lsp.ls_lad_euro);
		   }
	   }
/*
	   else if (aufk.waehrung == 2)
	   {
           sprintf (aufps.auf_vk_pr,    "%lf",    aufp.auf_vk_fremd);
           sprintf (aufps.auf_lad_pr,   "%lf",    aufp.auf_lad_fremd);
     	   if (ld_pr_prim)
		   {
                sprintf (aufps.auf_lad_dm,   "%lf",lsp.ls_lad_euro);
		   }
	   }
*/
	   else
	   {
           sprintf (aufps.auf_vk_pr,    "%lf",    aufp.auf_vk_pr);
           sprintf (aufps.auf_lad_pr,   "%lf",    aufp.auf_lad_pr);
	   }
}

void AUFPLIST::FillKondArt (char *kond_art)
{
	   int dsqlstatus;

       memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));

       strcpy (ptabn.ptitem,"sap_kond_art");
	   strcpy (ptabn.ptwer1, kond_art);

	   DbClass.sqlin ((char *) ptabn.ptitem, 0, 19);
	   DbClass.sqlin ((char *) ptabn.ptwer1, 0, 9);
	   DbClass.sqlout ((char *) ptabn.ptwer2, 0, 9);
	   dsqlstatus = DbClass.sqlcomm ("select ptwer2 from ptabn "
		                             "where ptitem = ? "
									 "and ptwer1 = ?");
       strcpy (aufps.kond_art0, ptabn.ptwer1);
       strcpy (aufps.kond_art,  ptabn.ptwer2);
}

double AUFPLIST::GetBasa_grund (double a)
{
	   double a_grund = 0;

	   DbClass.sqlin ((double *) &a, 3, 0);
	   DbClass.sqlout ((double *) &a_grund, 3, 0);
	   DbClass.sqlcomm ("select a_grund from a_bas where a = ?");
	   return a_grund;
}


void AUFPLIST::ReadExtraPr (void)
{
    char lieferdat [12];
    int dsqlstatus;
    short sa;
    double pr_ek;
    double pr_vk;
	short extra_mdn;
	long extra_kun;

	if (aufk.kun_fil == 0)
	{
		if (_mdn.tou == 0l || _mdn.kun == 0l)
		{
			return;
		}
		extra_mdn = (short) _mdn.tou;
		extra_kun = _mdn.kun;
	}
	else if (aufk.kun_fil == 1)
	{
		if (_fil.verr_mdn == 0l || _fil.kun == 0l)
		{
			return;
		}
		extra_mdn = (short) _fil.verr_mdn;
		extra_kun = _fil.kun;
	}


	dlong_to_asc (aufk.lieferdat, lieferdat);

// Preise immer mit Filiale 0 lesen.

	   WaPreis.SetAufArt (auf_art);

	   if (DllPreise.PriceLib != NULL && 
	    DllPreise.preise_holen != NULL)
	   {
				  dsqlstatus = (DllPreise.preise_holen) (extra_mdn, 
                                          0,
//                                          aufk.kun_fil,
										  0,
                                          extra_kun,
                                           ratod (aufps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
				  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
	   }
  	   else
	   {
				dsqlstatus = WaPreis.preise_holen (extra_mdn,
                                          0,
//                                          aufk.kun_fil,
										  0,
                                          extra_kun,
                                           ratod (aufps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
	   }
	   sprintf (aufps.auf_vk_fremd, "%lf", pr_ek);
}



void AUFPLIST::ReadPr (void)
/**
Artikelpreis holen.
**/
{
       char lieferdat [12];
       int dsqlstatus;
       short sa;
       double pr_ek;
       double pr_vk;

       if (Muster)
       {
                    return;
       }

       dlong_to_asc (aufk.lieferdat, lieferdat);

// Preise immer mit Filiale 0 lesen.

	   WaPreis.SetAufArt (auf_art);


	   if (DllPreise.PriceLib != NULL && 
	    DllPreise.preise_holen != NULL)
	   {
				  dsqlstatus = (DllPreise.preise_holen) (aufk.mdn, 
			                               0,
				                           aufk.kun_fil,
					                       aufk.kun,
						                   ratod (aufps.a),
							               lieferdat,
								           &sa,
									       &pr_ek,
										   &pr_vk);
				  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
	   }
	   else
	   {
					dsqlstatus = WaPreis.preise_holen (aufk.mdn,
                                          0,
                                          aufk.kun_fil,
                                          aufk.kun,
                                           ratod (aufps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
	   }
	   if ((NoArtMess == FALSE) && (pr_ek == (double) 0.0))
	   {
/*
               if (preis0_mess == 1)
			   {
                      disp_mess ("Achtung !!\nPreis 0 gelesen", 2);
			   }
*/
       }

       if (dsqlstatus == 0)
       {
                 sprintf (aufps.auf_vk_pr, "%lf",  pr_ek);
                 sprintf (aufps.auf_lad_pr,"%lf", pr_vk);
				 sprintf (aufps.sa_kz_sint, "%1d", sa);
	             strcpy (aufps.kond_art, WaPreis.GetKondArt ());
	             strcpy (aufps.kond_art0, WaPreis.GetKondArt ());
				 sprintf (aufps.a_grund, "%4.0lf", WaPreis.GetAGrund ());
				 FillKondArt (aufps.kond_art);
       }
	   auf_vk_pr = pr_ek;
	   FillWaehrung (pr_ek, pr_vk);
	   sprintf (aufps.a_grund, "%4.0lf", WaPreis.GetAGrund ());
	   if (ratod (aufps.a_grund) == 0.0)
	   {
	        sprintf (aufps.a_grund, "%4.0lf", GetBasa_grund (_a_bas.a));
	   }
	   if (sa)
	   {
		   CreateSa ();
	   }
	   else
	   {
		   DestroySa ();
	   }
	   if (!RemoveMarge)
	   {
			CalcLdPrPrc (pr_ek, pr_vk);
	   }
	   ReadExtraPr ();
}

void AUFPLIST::ReadMeEinh (void)
/**
Mengeneinheiten holen.
**/
{
        int dsqlstatus;
        char ptwert [5];

        KEINHEIT keinheit;

        einh_class.GetKunEinh (aufk.mdn, aufk.fil, aufk.kun,
                               _a_bas.a, &keinheit);
        strcpy (aufps.basis_me_bz, keinheit.me_einh_bas_bez);
        strcpy (aufps.me_bz, keinheit.me_einh_kun_bez);
        sprintf (aufps.me_einh_kun, "%hd", keinheit.me_einh_kun);
        sprintf (aufps.me_einh,     "%hd", keinheit.me_einh_bas);
        inh = keinheit.inh;
        sprintf (aufps.me_einh_kun1, "%hd", keinheit.me_einh1);
        sprintf (aufps.me_einh_kun2, "%hd", keinheit.me_einh2);
        sprintf (aufps.me_einh_kun3, "%hd", keinheit.me_einh3);

        sprintf (aufps.inh1, "%.3lf", keinheit.inh1);
        sprintf (aufps.inh2, "%.3lf", keinheit.inh2);
        sprintf (aufps.inh3, "%.3lf", keinheit.inh3);

        return;


        switch (_a_bas.a_typ)
        {
             case HNDW :
                     dsqlstatus = HndwClass.lese_a_hndw (_a_bas.a);
                     break;
             case EIG :
                     dsqlstatus = HndwClass.lese_a_eig (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig.me_einh_ek;
                     break;
             case EIG_DIV :
                     dsqlstatus = HndwClass.lese_a_eig_div (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                     break;
             case DIENSTLEISTUNG :
             case LEIHARTIKEL :
                     a_hndw.me_einh_kun = _a_bas.me_einh;
                     break;
             default :
                     a_hndw.me_einh_kun = _a_bas.me_einh;
                     break;
        }

		aufps.a_typ = _a_bas.a_typ;

        sprintf (ptwert, "%hd", _a_bas.me_einh);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (aufps.basis_me_bz, ptabn.ptbezk);
        }

        if (dsqlstatus) return;

        sprintf (ptwert, "%hd", a_hndw.me_einh_kun);
        if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (aufps.me_bz, ptabn.ptbezk);
        }
}

void AUFPLIST::rechne_aufme0 (void)
/**
Auftragsmengen berechnen.
**/
{
        double auf_me;
        double auf_me1;
        double auf_me2;
        double auf_me3;
        double inh1;
        double inh2;
        double inh3;

        auf_me = ratod (aufps.auf_me);
        inh1 = ratod (aufps.inh1);
        if (inh1 > (double) 0.0 && atoi (aufps.me_einh_kun1))
        {
               auf_me1 = auf_me / inh1;
               if (atoi (aufps.me_einh_kun1) == 2)
               {
                     sprintf
                         (aufps.auf_me1, "%.3lf", auf_me1);
               }
               else
               {
                     sprintf
                         (aufps.auf_me1, "%.0lf", auf_me1);
               }
        }
        else
        {
               sprintf
                         (aufps.auf_me1, "%.0lf", (double) 0.0);
               auf_me1 = auf_me;
        }
        inh2 = ratod (aufps.inh2);
        if (inh2 > (double) 0.0 && atoi (aufps.me_einh_kun2))
        {
               auf_me2 = auf_me1 / inh2;
               sprintf (aufps.auf_me2, "%.0lf", auf_me2);
        }
        else
        {
               sprintf
                         (aufps.auf_me2, "%.0lf", (double) 0.0);
               auf_me2 = auf_me1;
        }
        inh3 = ratod (aufps.inh3);
        if (inh3 > (double) 0.0 && atoi (aufps.me_einh_kun3))
        {
               auf_me3 = auf_me2 / inh3;
               sprintf (aufps.auf_me3, "%.0lf", auf_me3);
        }
        else
        {
               sprintf
                         (aufps.auf_me3, "%.0lf", (double) 0.0);
        }
}


void AUFPLIST::rechne_aufme1 (void)
/**
Auftragsmengen berechnen.
**/
{
        double auf_me;
        double auf_me1;
        double auf_me2;
        double auf_me3;
        double inh2;
        double inh3;

        auf_me = ratod (aufps.auf_me);
        auf_me1 = auf_me;
        sprintf (aufps.auf_me1, "%.3lf", auf_me1);
        inh2 = ratod (aufps.inh2);
        if (inh2 > (double) 0.0 && atoi (aufps.me_einh_kun2))
        {
               auf_me2 = auf_me1 / inh2;
               sprintf (aufps.auf_me2, "%.0lf", auf_me2 + 0.5);
        }
        else
        {
               sprintf (aufps.auf_me2, "%.0lf", (double) 0.0);
               auf_me2 = auf_me1;
        }
        inh3 = ratod (aufps.inh3);
        if (inh3 > (double) 0.0 && atoi (aufps.me_einh_kun3))
        {
               auf_me3 = auf_me2 / inh3;
               sprintf (aufps.auf_me3, "%.0lf", auf_me3 + 0.5);
        }
        else
        {
               sprintf (aufps.auf_me3, "%.0lf", (double) 0.0);
        }
}

void AUFPLIST::rechne_aufme2 (void)
/**
Auftragsmengen berechnen.
**/
{
        double auf_me;
        double auf_me1;
        double auf_me2;
        double auf_me3;
        double inh2;
        double inh3;

        auf_me = ratod (aufps.auf_me);
        auf_me2 = auf_me;
        sprintf (aufps.auf_me2, "%.3lf", auf_me2);
        inh2 = ratod (aufps.inh2);
        if (inh2 > (double) 0.0 && atoi (aufps.me_einh_kun1))
        {
               auf_me1 = auf_me2 * inh2;
               sprintf (aufps.auf_me1, "%.3lf", auf_me1);
        }
        else
        {
               sprintf (aufps.auf_me1, "%.3lf", (double) 0.0);
        }
        inh3 = ratod (aufps.inh3);
        if (inh3 > (double) 0.0 && atoi (aufps.me_einh_kun3))
        {
               auf_me3 = auf_me2 / inh3;
               sprintf (aufps.auf_me3, "%.0lf", auf_me3 + 0.5);
        }
        else
        {
               sprintf (aufps.auf_me3, "%.0lf", (double) 0.0);
        }
}

void AUFPLIST::rechne_aufme3 (void)
/**
Auftragsmengen berechnen.
**/
{
        double auf_me;
        double auf_me1;
        double auf_me2;
        double auf_me3;
        double inh2;
        double inh3;

        auf_me = ratod (aufps.auf_me);
        auf_me3 = auf_me;
        sprintf (aufps.auf_me3, "%.3lf", auf_me3);
        inh3 = ratod (aufps.inh3);
        if (inh3 > (double) 0.0 && atoi (aufps.me_einh_kun2))
        {
               auf_me2 = auf_me3 * inh3;
               sprintf (aufps.auf_me2, "%.3lf", auf_me2);
        }
        else
        {
               sprintf (aufps.auf_me2, "%.3lf", (double) 0.0);
               auf_me2 = auf_me3;
        }
        inh2 = ratod (aufps.inh2);
        if (inh2 > (double) 0.0 && atoi (aufps.me_einh_kun1))
        {
               auf_me1 = auf_me2 * inh2;
               sprintf (aufps.auf_me1, "%.3lf", auf_me1);
        }
        else
        {
               sprintf (aufps.auf_me1, "%.3lf", (double) 0.0);
        }
}

void AUFPLIST::rechne_liefme (void)
/**
Basismenge der aktuellen Bestellmenge anzeigen.
**/
{
	double auf_me;
	double a;
	short me_einh_kun;
	double auf_me_vgl;
    KEINHEIT keinheit;
    char lieferdat [12];
//    int dsqlstatus;
//    short sa;
//    double pr_ek;
//    double pr_vk;

    dlong_to_asc (aufk.lieferdat, lieferdat);


	auf_me = ratod (aufps.auf_me);
	a  = ratod (aufps.a);
	me_einh_kun = atoi (aufps.me_einh_kun);

    strcpy (kumebest.kun_bran2, kun.kun_bran2);
    einh_class.AktAufEinh (aufk.mdn, aufk.fil,
                                aufk.kun, a, me_einh_kun);
    strcpy (kumebest.kun_bran2, kun.kun_bran2);
    einh_class.GetKunEinh (aufk.mdn, aufk.fil,
                              aufk.kun, a, &keinheit);

    if (keinheit.me_einh_kun == keinheit.me_einh_bas)
    {
            auf_me_vgl = auf_me;
    }
    else
    {
            if (keinheit.inh <= (double) 0.0)
            {
                     keinheit.inh = (double) 1.0;
            }
            auf_me_vgl = auf_me * keinheit.inh;
    }

	sprintf (aufps.lief_me, "%8.2lf", auf_me_vgl);

/** ????
	WaPreis.SetAufArt (auf_art);

	WaPreis.SetAktMe (auf_me_vgl);

    dsqlstatus = WaPreis.preise_holen (aufk.mdn,
                                          0,
                                          aufk.kun_fil,
                                          aufk.kun,
                                           ratod (aufps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
    if ((NoArtMess == FALSE) && (pr_ek == (double) 0.0))
    {
               if (preis0_mess == 0)
			   {
                      disp_mess ("Achtung !!\nPreis 0 gelesen", 2);
			   }
    }

    if (dsqlstatus == 0)
    {
                 sprintf (aufps.auf_vk_pr, "%6.2lf",  pr_ek);
                 sprintf (aufps.auf_lad_pr, "%6.2lf", pr_vk);
				 sprintf (aufps.sa_kz_sint, "%1d", sa);
	             strcpy (aufps.kond_art, WaPreis.GetKondArt ());
	             strcpy (aufps.kond_art0, WaPreis.GetKondArt ());
				 sprintf (aufps.a_grund, "%4.0lf", WaPreis.GetAGrund ());
    }
    auf_vk_pr = pr_ek;
	FillWaehrung (pr_ek, pr_vk);
	sprintf (aufps.a_grund, "%4.0lf", WaPreis.GetAGrund ());
    if (sa)
	{
		   CreateSa ();
	}
	else
	{
		   DestroySa ();
	}
*/
}


int AUFPLIST::testme (void)
/**
Auftragsmeneg pruefen.
**/
{
        KEINHEIT keinheit;
        char lief_me [20];
        char charge_gew [20];
		HWND activeWindow ;

	    activeWindow = GetActiveWindow();		



	  if (strlen(clipped(InfoProcess)) >2 ) 
	  {

	   if (ProcessID == NULL) 
	   {
//testtest		  eListe.KillListFocus ();
		   ProcessID = StarteProcess (); //testtest
          if (ProcessID == NULL)
          {
              print_mess (2, "%s konnte nicht gestartet werden", InfoProcess);
          }
//testtest		  ShowWindow (activeWindow,SW_SHOWNORMAL);
//testtest		  eListe.SetListFocus ();
//testtest		  Sleep (8000);
//testtest		KillProcess () ; //testtest

//	      SendMessage(GetParent(hwndDlg), WM_NCACTIVATE, (WPARAM)FALSE, NULL)
//	      SendMessage(GetActiveWindow (), WM_KILLFOCUS, (WPARAM)FALSE, NULL);

		  /**

		SetFocus (eListe.Getmamain2 ());
       SetAktivWindow (eListe.Getmamain2 ());
	   UpdateWindow (eListe.Getmamain2 ());

           eListe.SetFeldFocus0 (eListe.GetAktRow (),
                                 eListe.GetAktColumn ());

  **/
 
	   }
	  }
        if (testmeOK) return 0;
        if (ratod (aufps.a) == (double) 0.0) return 0;

  	    int pos = eListe.GetAktRow ();

	    if (strlen(clipped(ProcessVerkFragen)) >2 ) //WAL-15
		{
		  SetzeEventVerkFragen(aufps.pos_id, ratod (aufps.a));
		}



//FS-109 A
     if (ratod (aufps.auf_me) != (double (0)))
	 {
		if (kun.min_best == 1)
		{
			double dauf_me = ratod (aufptab[pos].auf_me);
			double dstaffel_me = (double) _a_bas.min_bestellmenge;
			if (dstaffel_me < (double) _a_bas.staffelung) dstaffel_me = (double) _a_bas.staffelung;
			double dstaffel_rest = (double) 0;
			if (dauf_me < (double) _a_bas.min_bestellmenge)
			{
				dauf_me = (double) _a_bas.min_bestellmenge;
			}
			else 
			{
				if (_a_bas.staffelung > 0)
				{
					while (dauf_me > dstaffel_me)
					{
						dstaffel_me += (double) _a_bas.staffelung;
					}
					dstaffel_rest = (dauf_me - dstaffel_me) * -1;
					if (dstaffel_rest == (double) 0)
					{
						dauf_me = dstaffel_me;

					}
					else
					{
						if ((dstaffel_rest * 100 / _a_bas.staffelung) > 49.99 ) 
						{
							dauf_me = dstaffel_me - (double) _a_bas.staffelung;
						}
						else
						{
							dauf_me = dstaffel_me;
						}
					}
				}
				
			}
			sprintf (aufptab[pos].auf_me, "%.3lf", dauf_me);
			sprintf (aufps.auf_me, "%.3lf", dauf_me);

		}
	 }
//FS-109 E


		sprintf (aufptab[pos].auf_me, "%.3lf", ratod (aufptab[pos].auf_me));
		if (StndDirect == 3)
		{
			LISTHANDLER->Update (pos, &aufptab[pos]);
		}
		else 			LISTHANDLER->Update (pos, &aufptab[pos]); //FS-126


        if (InfoProcessRefresh > 0)  //031012
        {
	       memcpy (&aufptab[eListe.GetAktRow ()], &aufps, sizeof (struct AUFPS));
           WritePos (eListe.GetAktRow ());
	       commitwork ();
		   beginwork ();
        }

		if (add);
        else if (akt_auf_me !=  ratod (aufptab[pos].auf_me));
        else if (akt_me_einh != ratod (aufptab[pos].me_einh_kun));
        else if (auf_vk_pr != ratod (aufps.auf_vk_pr));
        else if (ratod (aufps.auf_me) == (double) 0.0 && syskey == KEYCR);
        else
        {
             if (meoptimize)
             {
                   testmeOK = TRUE;
             }
            return 0;
        }


		if (add && aufme_old != (double) 0.0)
		{
			aufp.auf_me = ratod (aufps.auf_me);
			sprintf (aufps.auf_me, "%.3lf", aufp.auf_me + aufme_old);
            memcpy (&aufptab[eListe.GetAktRow()],
                            &aufps, sizeof (struct AUFPS));
			eListe.ShowAktRow ();
		}


        akt_auf_me  =  atoi (aufptab[pos].auf_me);
        akt_me_einh = atoi (aufptab[pos].me_einh_kun);
//        auf_vk_pr   = ratod (aufps.auf_vk_pr);

        sprintf (aufps.auf_me1, "%.3lf", (double) 0.0);
        sprintf (aufps.auf_me2, "%.3lf", (double) 0.0);
        sprintf (aufps.auf_me3, "%.3lf", (double) 0.0);

        if (ratod (aufps.auf_me) == (double) 0.0 && syskey == KEYCR)
        {
            einh_class.NextAufEinh (aufk.mdn, aufk.fil, aufk.kun,
                                 ratod (aufps.a),
                                 atoi (aufps.me_einh_kun), &keinheit);
            strcpy (aufps.basis_me_bz, keinheit.me_einh_bas_bez);
            strcpy (aufps.me_bz, keinheit.me_einh_kun_bez);
            sprintf (aufps.me_einh_kun, "%hd", keinheit.me_einh_kun);
            inh = keinheit.inh;
            memcpy (&aufptab[eListe.GetAktRow()],
                            &aufps, sizeof (struct AUFPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());

  	        add = FALSE;
			DestroyPlus ();
            return -1;
        }

        if (ratod (aufps.auf_me) > MAXME)
        {
            print_mess (2, "Die Auftragsmenge ist zu gross");
            sprintf (aufps.auf_me, "%.3lf", (double) 0.0);
            memcpy (&aufptab[eListe.GetAktRow()],
                            &aufps, sizeof (struct AUFPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
  	        add = FALSE;
			DestroyPlus ();
            return -1;
        }


        if (atoi (aufps.me_einh_kun) ==
            atoi (aufps.me_einh_kun1))
        {
            rechne_aufme1 ();
        }
        else if (atoi (aufps.me_einh_kun) ==
                 atoi (aufps.me_einh_kun2))
        {
            rechne_aufme2 ();
        }
        else if (atoi (aufps.me_einh_kun) ==
                 atoi (aufps.me_einh_kun3))
        {
            rechne_aufme3 ();
        }
        else if (atoi (aufps.me_einh_kun) ==
                 atoi (aufps.me_einh))
        {
            rechne_aufme0 ();
        }
		if (LiefMeDirect)
        {
            rechne_liefme ();
        }
        if (AufCharge && _a_bas.charg_hand == 1 &&
            (lsc2_par ==1 || lsc3_par == 1))
        {
            rechne_liefme ();
        }
        if (AufCharge && _a_bas.charg_hand == 1 &&
            (lsc2_par == 1 || lsc3_par == 1))
        {

            if (strcmp (aufps.ls_charge, " ") > 0 &&
                ratod (aufps.lief_me) > aufps.charge_gew)
            {
/*
                print_mess (2, "Die Auftragsmenge %.3lf %s ist zu gro�\n"
                               "Maximale Menge f�r Charge %s ist %.3lf %s",
                               ratod (aufps.lief_me), clipped (aufps.basis_me_bz),
                               aufps.ls_charge, aufps.charge_gew, aufps.basis_me_bz);
*/
                print_mess (2, "Die Auftragsmenge %s %s ist zu gro�\n"
                               "Maximale Menge f�r Charge %s ist %s %s",
                               KFormat (lief_me, "%.3lf", ratod (aufps.lief_me)),
                               clipped (aufps.basis_me_bz),
                               aufps.ls_charge,
                               KFormat (charge_gew, "%.3lf", aufps.charge_gew),
                               aufps.basis_me_bz);
                sprintf (aufps.auf_me, "%.3lf", (double) 0.0);
                memcpy (&aufptab[eListe.GetAktRow()],
                            &aufps, sizeof (struct AUFPS));
                eListe.ShowAktRow ();
                eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
  	            add = FALSE;
			    DestroyPlus ();
                return -1;
            }
        }
        memcpy (&aufptab[eListe.GetAktRow()],
                            &aufps, sizeof (struct AUFPS));
        AnzAufWert ();
        AnzAufGew ();
		aufme_old = (double) 0.0;
	    add = FALSE;
		DestroyPlus ();
        if (meoptimize)
        {
             testmeOK = TRUE;
        }

        return 0;
}

int AUFPLIST::TestPrproz_diff (void)
/**
Test, ob die Preisaenderung ueber prproz_diff % ist.
**/
{
	    double oldvk;
	    double diff;
		double diffproz;
		char buffer [256];

		if (auf_vk_pr == (double) 0.0) return 1;
		oldvk = ratod (aufps.auf_vk_pr);
		diff = oldvk - auf_vk_pr;
		if (diff < 0) diff *= -1;
        diffproz = 100 * diff / auf_vk_pr;
		if (diffproz > prproz_diff)
		{
//			print_mess (2, "Achtung !! Preis�nderung �ber %.2lf %c", prproz_diff, '%');
			sprintf (buffer, "Achtung !! Preis�nderung �ber %.2lf %c.\n"
				             "�nderung OK ?", prproz_diff, '%');
            if (abfragejn (eListe.Getmamain3 (),
					       buffer , "N") == 0)
			{
				sprintf (aufps.auf_vk_pr, "%lf", auf_vk_pr);
                memcpy (&aufptab[eListe.GetAktRow()],
                            &aufps, sizeof (struct AUFPS));
                eListe.ShowAktRow ();
                eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());

			    return 0;
			}
		}
		return 1;
}


int AUFPLIST::testpr (void)
/**
Artikel holen.
**/
{
	    char buffer [256];


        int fpos = GetItemPos (&dataform, "pr_vk");
        if (fpos != -1)
		{
           sprintf (buffer, dataform.mask[fpos].picture, auf_vk_pr);
		   auf_vk_pr = ratod (buffer);
		}

		if (ratod (aufps.auf_vk_pr) != auf_vk_pr)
        {
            aufps.ls_pos_kz = 1;
        }
		FillWaehrung (ratod (aufps.auf_vk_pr), ratod (aufps.auf_lad_pr));
//		CalcLdPrPrc (ratod (aufps.auf_vk_pr), ratod (aufps.auf_lad_pr));
        memcpy (&aufptab[eListe.GetAktRow()],
                            &aufps, sizeof (struct AUFPS));
	    if (preistest == 1 && auf_vk_pr)
		{
			if (ratod (aufps.auf_vk_pr) != auf_vk_pr)
			{
			        disp_mess ("Achtung !! Der Preis wurde ge�ndert", 2);
			}
			return 0;
		}
		else if (preistest == 4)
		{
			if (TestPrproz_diff () == 0)
			{
               eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
               return (-1);
            }
		}
		else if (preistest == 2 && auf_vk_pr)
		{
			sprintf (aufps.auf_vk_pr, "%lf", auf_vk_pr);
		}

        if (ratod (aufps.a) == (double) 0.0) return 0;

        if ((Muster == FALSE) && ratod (aufps.auf_vk_pr) == (double) 0.0 &&
			aufps.a_typ != LEIHARTIKEL)
        {
            if (auf_me_pr_0)
            {
               if ((eListe.IsAppend ()) && (preis0_mess == 1))
			   {
			          sprintf (buffer, "Achtung !! Preis ist 0\n"
				                       "            OK ?");
                      if (abfragejn (eListe.Getmamain3 (),
					       buffer , "N") == 0)
					  {
                              eListe.SetPos (eListe.GetAktRow (),
                                             eListe.GetAktColumn ());
							  return -1;
					  }
			  }
              return 0;
            }
            else
            {
               disp_mess ("Der Preis darf nicht 0 sein", 2);
               eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
            }
            return (-1);
        }
        if (ratod (aufps.auf_vk_pr) > MAXPR)
        {
            print_mess (2, "Der Preis ist zu gross");
            sprintf (aufps.auf_vk_pr, "%lf", (double) 0.0);
            memcpy (&aufptab[eListe.GetAktRow()],
                            &aufps, sizeof (struct AUFPS));
            eListe.ShowAktRow ();
            eListe.SetPos (eListe.GetAktRow (),
                              eListe.GetAktColumn ());
            return -1;
        }
	    if (!RemoveMarge)
		{
			CalcLdPrPrc (ratod (aufps.auf_vk_pr), ratod (aufps.auf_lad_pr));
		}
        memcpy (&aufptab[eListe.GetAktRow()],
                            &aufps, sizeof (struct AUFPS));
        AnzAufWert ();
        AnzAufGew ();
        return 0;
}

void AUFPLIST::CalcLdPrPrc (double vk_pr, double ld_pr)
{
	    double auf_lad_pr_prc = 0.0;
	    if (vk_pr != 0.0 && ld_pr != 0.0)
		{
			auf_lad_pr_prc = 100 - (vk_pr * 100 / ld_pr);	
		}
		sprintf (aufps.auf_lad_pr_prc,"%lf", auf_lad_pr_prc);
 		GrpPrice.SetAKey (ratod (aufps.a));
//		GrpPrice.SetAKey (ratod (aufps.a), atoi (aufps.me_einh), aufps.a_gew);
 //		GrpPrice.CalculateLdPr (vk_pr, aufps.marge, sizeof (aufps.marge), aufps.auf_lad_pr, sizeof (aufps.auf_lad_pr));
		GrpPrice.CalculateLdPr (vk_pr, aufps.marge, sizeof (aufps.marge), aufps.auf_lad_pr0, sizeof (aufps.auf_lad_pr0));
}


void AUFPLIST::EanGew (char *eans, BOOL eangew)
/**
Gewicht aus ERAN-Nr holen oder Defaultwert.
**/
{
	   char gews [6];
	   double gew;

	   if (eangew)
	   {
		   memcpy (gews, &eans[7], 5);
		   gews [5] = (char) 0;
		   gew = ratod (gews) / 1000;
		   sprintf (aufps.auf_me, "%.3lf", gew);
	   }
	   else if (auf_me_default)
	   {
		   sprintf (aufps.auf_me, "%d", auf_me_default);
	   }
}


int AUFPLIST::ReadEan (double ean)
{
	   double a;
	   char eans [14];
	   int dsqlstatus;
	   char PLU [7];
	   long a_krz;
	   BOOL eangew;

       DbClass.sqlin ((double *) &ean,    3, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_ean "
                                     "where ean = ?");
	   if (dsqlstatus == 0)
	   {
		   sprintf (aufps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (aufps.a));
		   return dsqlstatus;
	   }
	   sprintf (eans, "%.0lf", a);
	   if (strlen (eans) < 13)
	   {
		   return dsqlstatus;
	   }

	   eangew = FALSE;
	   if (memcmp (eans, "20", 2) == 0);
	   else if (memcmp (eans, "21", 2) == 0);
	   else if (memcmp (eans, "22", 2) == 0);
	   else if (memcmp (eans, "23", 2) == 0);
	   else if (memcmp (eans, "24", 2) == 0);
	   else if (memcmp (eans, "27", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else if (memcmp (eans, "28", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else if (memcmp (eans, "29", 2) == 0)
	   {
		   eangew = TRUE;
	   }
	   else
	   {
		   return dsqlstatus;
	   }
	   memcpy (PLU, &eans[2], plu_size);
	   PLU[plu_size] = (char) 0;
	   a_krz = atol (PLU);
       DbClass.sqlin ((long *) &a_krz, 2, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_krz "
                                     "where a_krz = ?");
	   if (dsqlstatus == 0)
	   {
		   sprintf (aufps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (aufps.a));
		   EanGew (eans, eangew);
		   return dsqlstatus;
	   }
	   a = (double) a_krz;

       DbClass.sqlin ((double *) &a,    3, 0);
       DbClass.sqlout ((double *)  &a, 3, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_bas "
                                     "where a = ?");
	   if (dsqlstatus == 0)
	   {
		   sprintf (aufps.a, "%.0lf", a);
           dsqlstatus = lese_a_bas (ratod (aufps.a));
		   EanGew (eans, eangew);
	   }
	   return dsqlstatus;
}


int AUFPLIST::Testa_kun (void)
/**
Test, ob fuer den Kunden ein Eintrag in a_kun existiert.
Wenn ja, wird gepr�ft, ob der aktuelle Artikel in a_kun existiert.
**/
{
	   int dsqlstatus;

       DbClass.sqlin ((short *) &aufk.mdn,    1, 0);
       DbClass.sqlin ((short *) &aufk.fil,    1, 0);
       DbClass.sqlin ((long *)  &aufk.kun,   2, 0);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? ");

	   if (dsqlstatus == 100 & TestBranSmt)
       {
	       DbClass.sqlin ((short *) &aufk.mdn,     1, 0);
           DbClass.sqlin ((short *) &aufk.fil,     1, 0);
           DbClass.sqlin ((char *)  kun.kun_bran2, 0, 3);
           dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                         "where mdn = ? "
                                         "and   fil = ? "
                                         "and   kun = 0 "
                                         "and   kun_bran2 = ? ");
       }

	   if (dsqlstatus == 100) return 0;

       DbClass.sqlin ((short *) &aufk.mdn,    1, 0);
       DbClass.sqlin ((short *) &aufk.fil,    1, 0);
       DbClass.sqlin ((long *)  &aufk.kun,   2, 0);
       DbClass.sqlin ((double*) &_a_bas.a, 3,0);
       DbClass.sqlout ((char *) aufps.a_kun, 0,17);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? "
                                     "and  a    = ?");
	   if (dsqlstatus == 0) return 0;


       if (TestBranSmt)
       {
           DbClass.sqlin ((short *) &aufk.mdn,    1, 0);
           DbClass.sqlin ((short *) &aufk.fil,    1, 0);
           DbClass.sqlin ((char *)  kun.kun_bran2, 0, 3);
           DbClass.sqlin ((double*) &_a_bas.a, 3,0);
           DbClass.sqlout ((char *) aufps.a_kun, 0,17);
           dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                         "where mdn = ? "
                                         "and   fil = ? "
                                         "and   kun = 0 "
                                         "and   kun_bran2 = ? "
                                         "and  a    = ?");
       }
	   if (dsqlstatus == 0) return 0;

	   if (NoArtMess == FALSE)
	   {
              	   print_mess (2, "Der Artikel ist nicht im Sortiment des Kunden");
	   }
	   if (a_kun_smt == 1)
	   {
		   return 0;
	   }
       return -1;
}


BOOL AUFPLIST::Testa_kun (double art)
/**
Test, ob fuer den Kunden ein Eintrag in a_kun existiert.
Wenn ja, wird gepr�ft, ob der aktuelle Artikel in a_kun existiert.
**/
{
	   int dsqlstatus;
	   char lieferdat[12];
	   short sa;
	   double pr_ek;
	   double pr_vk;
	   static double a;
	   static int a_kun_cursor = -1;
	   static int a_kun_bran_cursor = -1;
	   static int a_kuna_cursor = -1;
	   static int a_kuna_bran_cursor = -1;

//	   return TRUE  ;  //TESTTEST

	   if (preis0_mess == 2)
	   {
		   WaPreis.SetAufArt (auf_art);
	       dlong_to_asc (aufk.lieferdat, lieferdat);


		   if (DllPreise.PriceLib != NULL && 
			DllPreise.preise_holen != NULL)
		   {
					  dsqlstatus = (DllPreise.preise_holen) (aufk.mdn, 
											   0,
											   aufk.kun_fil,
											   aufk.kun,
											   art,
											   lieferdat,
											   &sa,
											   &pr_ek,
											   &pr_vk);
					  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
		   }
		   else
		   {
						dsqlstatus = WaPreis.preise_holen (aufk.mdn,
											  0,
											  aufk.kun_fil,
											  aufk.kun,
											  art,
											  lieferdat,
											  &sa,
											  &pr_ek,
											  &pr_vk);
		   }
		   if (pr_ek == 0.0)
		   {
			   return FALSE;
		   }
		   return TRUE;

	   }

	   a = art;
//250812	   if (a_kun_smt == 1)
	   if (a_kun_smt == 0)  // 250812 bei 0 soll nicht gepr�ft werden 
	   {
		   return TRUE;
	   }

	   if (a_kun_cursor == -1)
	   {
		   DbClass.sqlin ((short *) &aufk.mdn,    1, 0);
		   DbClass.sqlin ((short *) &aufk.fil,    1, 0);
		   DbClass.sqlin ((long *)  &aufk.kun,   2, 0);
		   a_kun_cursor = DbClass.sqlcursor ("select a from a_kun "
										 "where mdn = ? "
										 "and   fil = ? "
										 "and   kun = ? ");

	       DbClass.sqlin ((short *) &aufk.mdn,     1, 0);
           DbClass.sqlin ((short *) &aufk.fil,     1, 0);
           DbClass.sqlin ((char *)  kun.kun_bran2, 0, 3);
           a_kun_bran_cursor = DbClass.sqlcursor ("select a from a_kun "
                                         "where mdn = ? "
                                         "and   fil = ? "
                                         "and   kun = 0 "
                                         "and   kun_bran2 = ? ");
		   DbClass.sqlin ((short *) &aufk.mdn,    1, 0);
		   DbClass.sqlin ((short *) &aufk.fil,    1, 0);
		   DbClass.sqlin ((long *)  &aufk.kun,   2, 0);
		   DbClass.sqlin ((double*) &a, 3,0);
		   a_kuna_cursor = DbClass.sqlcursor ("select a from a_kun "
										 "where mdn = ? "
										 "and   fil = ? "
										 "and   kun = ? "
										 "and   a    = ?");
           DbClass.sqlin ((short *) &aufk.mdn,    1, 0);
           DbClass.sqlin ((short *) &aufk.fil,    1, 0);
           DbClass.sqlin ((char *)  kun.kun_bran2, 0, 3);
           DbClass.sqlin ((double*) &_a_bas.a, 3,0);
           a_kuna_bran_cursor = DbClass.sqlcursor ("select a from a_kun "
                                         "where mdn = ? "
                                         "and   fil = ? "
                                         "and   kun = 0 "
                                         "and   kun_bran2 = ? "
                                         "and  a    = ?");
	   }
	   DbClass.sqlopen (a_kun_cursor);
	   dsqlstatus = DbClass.sqlfetch (a_kun_cursor);

	   if (dsqlstatus == 100 & TestBranSmt)
       {
		   DbClass.sqlopen (a_kun_bran_cursor);
		   dsqlstatus = DbClass.sqlfetch (a_kun_bran_cursor);
       }

	   if (dsqlstatus == 100) return TRUE;

	   DbClass.sqlopen (a_kuna_cursor);
	   dsqlstatus = DbClass.sqlfetch (a_kuna_cursor);
	   if (dsqlstatus == 0) return TRUE;


       if (TestBranSmt)
       {
		   DbClass.sqlopen (a_kuna_bran_cursor);
		   dsqlstatus = DbClass.sqlfetch (a_kuna_bran_cursor);
       }
	   if (dsqlstatus == 0) return TRUE;

       return FALSE;
}


void AUFPLIST::GetLastMeAuf (double a)
/**
Letzte Bestellmenge des Kunden holen.
**/
{
//	   int cursork;
//	   int cursorp;
	   int cursorsum;
	   long auf;
	   double auf_me = 0.0;
	   double pr_vk = 0.0;
	   double auf_me_ges = 0.0;
	   char ldat[12] = "";

	   if (AddLastLief);
	   else if (AddLastVkPr);
	   else if (DelLastMe) return;

	   if (LetztLiefHolen)
	   {
		   DbClass.sqlin ((short *) &aufk.kun_fil, 1, 0);
		   DbClass.sqlin ((long *)  &aufk.kun, 2, 0);
		   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		   DbClass.sqlin ((long *)  &LetztLiefAnzTage, 2, 0);
		   DbClass.sqlin ((double *)  &a, 3, 0);
		   DbClass.sqlout ((long *)  &auf, 2, 0);
		   DbClass.sqlout ((char *)  ldat, 0, 11);
		   DbClass.sqlout ((double *) &auf_me, 3, 0);
		   DbClass.sqlout ((double *) &pr_vk, 3, 0);
		   // erst mal en halbes Jahr zur�ckschauen , sp�ter vielleicht parametrieren
		   cursorsum = DbClass.sqlcursor ("select lsk.ls, lsk.lieferdat,sum(lief_me),avg(ls_vk_euro) from lsk,lsp where lsk.kun_fil = ? and lsk.kun = ? and lsk.mdn = ? "
		                                                  "and lsk.fil = ? "
														  "and lsk.lieferdat >= today - ?  "    
														  "and lsp.a = ? "
														  "and lsp.lief_me != 0 "
														  "and lsp.mdn = lsk.mdn and lsp.fil = lsk.fil and lsp.ls = lsk.ls "
														  "group by 1,2 "
														  "order by lsk.lieferdat desc, "
														  "ls desc");
/****
		   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		   DbClass.sqlin ((long *)  &aufk.kun, 2, 0);
		   DbClass.sqlout ((long *)  &auf, 2, 0);
		   DbClass.sqlout ((char *)  ldat, 0, 11);
		   cursork = DbClass.sqlcursor ("select ls, lieferdat from lsk where mdn = ? "
		                                                  "and fil = ? "
														  "and kun = ? "
														  "order by lieferdat desc, "
														  "ls desc");

		   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		   DbClass.sqlin ((long *)  &auf, 2, 0);
		   DbClass.sqlin ((double *)  &a, 3, 0);
		   DbClass.sqlout ((double *) &auf_me, 3, 0);
		   DbClass.sqlout ((double *) &pr_vk, 3, 0);
		   cursorp = DbClass.sqlcursor ("select lief_me, ls_vk_euro from lsp "
		                            "where mdn = ? "
									"and fil = ? "
									"and ls = ? "
									"and a = ?");
									********/
	   }
	   else
	   {

		   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		   DbClass.sqlin ((long *)  &aufk.kun, 2, 0);
		   DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
		   DbClass.sqlin ((long *)  &LetztLiefAnzTage, 2, 0);
		   DbClass.sqlin ((double *)  &a, 3, 0);
		   DbClass.sqlout ((long *)  &auf, 2, 0);
		   DbClass.sqlout ((char *)  ldat, 0, 11);
		   DbClass.sqlout ((double *) &auf_me, 3, 0);
		   DbClass.sqlout ((double *) &pr_vk, 3, 0);
		   cursorsum = DbClass.sqlcursor ("select aufk.auf, aufk.lieferdat,sum(auf_me),avg(auf_vk_euro) from aufk,aufp where aufk.mdn = ? "
		                                                  "and aufk.fil = ? "
														  "and aufk.kun = ? "
														  "and aufk.auf != ? "
														  "and aufk.lieferdat >= today - ? "
														  "and aufp.a = ? " 
														  "and aufp.auf_me != 0 "
														  "and aufp.mdn = aufk.mdn and aufp.fil = aufk.fil and aufp.auf = aufk.auf "
														  "group by 1,2 "
														  "order by aufk.lieferdat desc, "
														  "auf desc");


/*************

		   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		   DbClass.sqlin ((long *)  &aufk.kun, 2, 0);
		   DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
		   DbClass.sqlout ((long *)  &auf, 2, 0);
		   DbClass.sqlout ((char *)  ldat, 0, 11);
		   cursork = DbClass.sqlcursor ("select auf, lieferdat from aufk where mdn = ? "
		                                                  "and fil = ? "
														  "and kun = ? "
														  "and auf != ? "
														  "order by lieferdat desc, "
														  "auf desc");

		   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		   DbClass.sqlin ((long *)  &auf, 2, 0);
		   DbClass.sqlin ((double *)  &a, 3, 0);
		   DbClass.sqlout ((double *) &auf_me, 3, 0);
		   DbClass.sqlout ((double *) &pr_vk, 3, 0);
		   cursorp = DbClass.sqlcursor ("select auf_me, auf_vk_euro from aufp "
		                            "where mdn = ? "
									"and fil = ? "
									"and auf = ? "
									"and a = ?");
									************/
	   }
	   auf_me_ges = (double) 0.0;
/***
       while (DbClass.sqlfetch (cursork) == 0)
	   {
		   if (DbClass.sqlopen (cursorp)) break;
		   while (DbClass.sqlfetch (cursorp) == 0)
		   {
			   auf_me_ges += auf_me;
		   }
		   if (auf_me_ges != (double) 0.0) break;
	   }
	   DbClass.sqlclose (cursorp);
	   DbClass.sqlclose (cursork);
	   *************/
	   DbClass.sqlfetch (cursorsum);
	   auf_me_ges = auf_me;
	   DbClass.sqlclose (cursorsum);
	   sprintf (aufps.last_me, "%.3lf", auf_me_ges);
	   sprintf (aufps.last_ldat, "%s"  , ldat);
	   sprintf (aufps.last_pr_vk,"%.3lf"  , pr_vk);
}


void AUFPLIST::GetLastMe (double a)
/**
Letzte Bestellmenge des Kunden holen.
**/
{
	   long auf;
	   double auf_me_ges;
	   double auf_vk_pr;

	   if (AddLastLief);
	   else if (AddLastVkPr);
	   else if (DelLastMe) return;

	   if (!LastFromAufKun)
	   {
			GetLastMeAuf (a);
			return;
	   }

	   auf_me_ges = 0;
	   strcpy (aufps.last_ldat, "");
	   aufkun.auf_me = (double) 0.0;
	   aufkun.mdn     = aufk.mdn;
	   aufkun.fil     = aufk.fil;
	   aufkun.kun     = aufk.kun;
	   aufkun.a       = a;
	   aufkun.kun_fil = aufk.kun_fil;

	   AufKun.dbreadlast ();

	   auf = aufkun.auf;
	   auf_me_ges = aufkun.auf_me;
	   auf_vk_pr = aufkun.auf_vk_pr;
	   dlong_to_asc (aufkun.lieferdat, aufps.last_ldat );
	   while (AufKun.dbreadnextlast () == 0)
	   {
		   if (auf != aufkun.auf) break;
		   auf_me_ges += aufkun.auf_me;
		   auf_vk_pr = aufkun.auf_vk_pr;
	   }
	   if (auf_me_ges == (double) 0.0)
	   {
		   GetLastMeAuf (a);
	   }
	   else
	   {
           sprintf (aufps.last_me,     "%.3lf", auf_me_ges);
           sprintf (aufps.last_pr_vk, "%.4lf", auf_vk_pr);
	   }
}


int AUFPLIST::TestNewArt (double a)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
	     int i;
		 int aufpanz;
		 int akt_pos;

		 akt_pos = eListe.GetAktRow ();
         aufpanz = eListe.GetRecanz ();
	     for (i = 0; i < aufpanz; i ++)
		 {
			 if (i == akt_pos) continue;
			 if (a == ratod (aufptab[i].a)) break;
		 }
		 if (i == aufpanz) return -1;
		 return (i);
}

int AUFPLIST::TestNewArtAuf (double a)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
		int auf = 0;
		int posi = 0;
		double auf_me;
		char auf_me_bz [7];
		char cmess [256];
		 static int cursor = -1;

		 if (art_un_tst != 2) return -1;  
		 if (cursor == -1)
		 {
			DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
			DbClass.sqlin ((short *) &aufk.fil, 1, 0);
			DbClass.sqlin ((long *) &aufk.lieferdat, 2, 0); 
			DbClass.sqlin ((long *) &aufk.adr, 2, 0); 
			DbClass.sqlin ((double *) &a, 3, 0);
			DbClass.sqlin ((long *) &aufk.auf, 2, 0); 
			DbClass.sqlout ((long *) &auf, 2, 0);
			DbClass.sqlout ((long *) &posi, 2, 0);
			DbClass.sqlout ((double *) &auf_me, 3, 0);
			DbClass.sqlout ((char *) auf_me_bz, 0, 7);
			cursor = DbClass.sqlcursor ("select aufp.auf, aufp.posi, auf_me, auf_me_bz  from aufk,aufp where aufk.mdn = aufp.mdn and aufk.fil = aufp.fil "
										 "and aufk.auf = aufp.auf and aufk.mdn = ? and aufk.fil = ? and aufk.lieferdat = ? and aufk.adr = ?  and aufp.a = ? and aufp.auf != ? ");
		 }
		 DbClass.sqlopen (cursor);
		 DbClass.sqlfetch (cursor);
		 if (auf > 0 && sqlstatus == 0)
		 {
			 sprintf (cmess, "Der Artikel ist schon im Auftrag %ld (Pos: %ld) vorhanden ( Menge %4.2lf %s)  , Trotzdem erfassen ?", auf,posi,auf_me, auf_me_bz ); 
	          if (abfragejn (eListe.Getmamain3 (), cmess, "N") == 0)
			  {
				  return 0;
			  }
		 }

		 return (-1);
}


int AUFPLIST::fetchaDirect (int lrow, BOOL AufEinhFestgelegt)
/**
Artikel holen.
**/
{

       int dsqlstatus;
       char wert [5];
       long posi;
       int i;


	   clipped (aufps.a);
	   sprintf (aufps.a, "%13.0lf", ratod (aufps.a));

//FS-342 Hier darf nicht alles auf 1 gesetzt werden         einh_class.SetAufEinh (1);  
	   //FS-342 A
	   if (AufEinhFestgelegt == FALSE)
	   {
			einh_class.SetAufEinh (1);  
	   }
	   //FS-342 E
       sprintf (aufps.aufp_txt, "%ld", 0l);
       SetFkt (9, leer, NULL);
       set_fkt (NULL, 10);
       SetFkt (10, leer, NULL);
       posi = atol (aufps.posi);
       if (posi == 0)
       {
             i = lrow;
             if (i == 0)
             {
                 posi = 1;
             }
             else
             {
                 posi = atol (aufptab[i - 1].posi);
                 posi ++;
             }
             if (PosiEnd (posi) == 0)
             {
                 sprintf (aufps.posi, "%ld", posi);
             }
       }

       dsqlstatus = lese_a_bas (ratod (aufps.a));
       if (dsqlstatus == 100)
       {
           if (syskey != KEYCR &&syskey != KEYTAB
               && eListe.IsAppend ())
           {
               return (-1);
           }
           sprintf (aufptab[lrow].a, "%.0lf", Akta);
           memcpy (&aufps, &aufptab[lrow], sizeof (struct AUFPS));
           return (-1);
       }


	   GetLastMe (ratod (aufps.a));

	   aufps.a_typ  = _a_bas.a_typ;
	   aufps.a_leih = _a_bas.a_leih;
	   if (IsPartyService && _a_bas.a_typ == DIENSTLEISTUNG)
	   {
 	       SetToFullTax ();
		   EnableFullTax ();
//		   aufk.psteuer_kz = 2;
	   }

	   if (lutz_mdn_par)
	   {
	              sprintf (aufps.gruppe, "%ld", mdnprod);
	   }
       sprintf (aufps.lang_bez,       "%s %s",      _a_bas.a_bz1, _a_bas.a_bz2);
       sprintf (aufps.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (aufps.a_bz2,       "%s",      _a_bas.a_bz2);
       if (aufps.pos_id == 0)  aufps.pos_id = GetNextPos_id ();
	   aufps.userdef1 = GetDefaultUserdef1 (aufk.kun,_a_bas.a); //WAL-70
       aufps.a_gew = _a_bas.a_gew;
       aufps.dr_folge = _a_bas.dr_folge;
       sprintf (wert, "%hd", _a_bas.me_einh);
       sprintf (aufps.me_einh, "%hd", _a_bas.me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
       strcpy (aufps.basis_me_bz, ptabn.ptbezk);

       memcpy (&aufptab[lrow], &aufps, sizeof (struct AUFPS));
       if (eListe.IsNewRec ())
       {
           if (ratod (aufps.auf_vk_pr) == (double) 0.0)
           {
                    ReadPr ();
           }
           else if (akt_preis)
           {
                    ReadPr ();
           }
           kumebest.mdn = aufk.mdn;
           kumebest.fil = aufk.fil;
           kumebest.kun = aufk.kun;
           strcpy (kumebest.kun_bran2, kun.kun_bran2);
           kumebest.a = ratod (aufps.a);
           ReadMeEinh ();
           if (kun.einz_ausw > 3 || splitls == 2)
           {
			   _a_bas.teil_smt = (short) Smtg.Get (aufk.mdn, aufk.kun, (long) _a_bas.teil_smt);
               sprintf (aufps.teil_smt, "%hd", _a_bas.teil_smt);
           }
           else
           {
               strcpy (aufps.teil_smt, "0");
          }
       }
	   else if (ratod (aufps.auf_vk_pr) == (double) 0.0)
	   {
		           ReadPr ();
	   }
       memcpy (&aufptab[lrow], &aufps, sizeof (struct AUFPS));
       return 0;
}

int AUFPLIST::fetchMatchCode (void)
{
	   double a;
	   int dsqlstatus;
	   char matchorg [25];
	   char matchupper[25];
	   char matchlower[25];
	   char matchuplow[25];

	   strcpy (matchorg, aufps.a);
       strupcpy (matchupper, aufps.a);
       strlowcpy (matchlower, aufps.a);
       struplowcpy (matchuplow, aufps.a);

	   DbClass.sqlout ((double *) &a, 3, 0);
	   DbClass.sqlin ((char *) matchorg, 0, 13);
	   DbClass.sqlin ((char *) matchupper, 0, 13);
	   DbClass.sqlin ((char *) matchlower, 0, 13);
	   DbClass.sqlin ((char *) matchuplow, 0, 13);
	   dsqlstatus = DbClass.sqlcomm ("select a from a_bas where a_bz3 = ? "
		                                                    "or a_bz3 = ? "
														    "or a_bz3 = ? "
														    "or a_bz3 = ?");
	   if (dsqlstatus == 100)
	   {
		   print_mess (2, "Artikel %s nicht gefunden", aufps.a);
		   sprintf (aufps.a, "%13.0lf", 0.0);
	   }
	   else
	   {
	       sprintf (aufps.a, "%13.0lf", a);
	   }
	   return 0;
}

BOOL AUFPLIST::AddPosTxt (char *txt)
/**
Text fuer Nicht- osder Nachliefern einfuegen.
**/
{
       long aufp_txt;

       aufp_txt = atol (aufps.aufp_txt);
       if (aufp_txt == 0l) aufp_txt = GenAufpTxt ();
       if (aufp_txt == 0l)
       {
           disp_mess ("Fehler beim Generieren einer Textnummer", 2);
           return FALSE;
       }
       sprintf (aufps.aufp_txt, "%ld", aufp_txt);
       aufpt.nr = aufp_txt;
       DbClass.sqlin ((long *) &aufpt.nr, 2, 0);
       DbClass.sqlout ((long *) &aufpt.zei, 2, 0);
       if (DbClass.sqlcomm ("select max (zei) from aufpt where nr = ?") != 0)
       {
                    aufpt.zei = 0;
       }
       else if (DbClass.IsLongnull (aufpt.zei))
       {
                    aufpt.zei = 0;
       }
       aufpt.zei += 10;
       strcpy (aufpt.txt, txt);
       aufpt_class.dbupdate ();
       return TRUE;
}


BOOL AUFPLIST::DispErsatz (void)
{
        char ptwert [5];
        char ers_txt [80];
        char a_bz1 [25];
        char buffer [256];

        if (_a_bas.a_ers_kz == 0)
        {
            return TRUE;
        }

        sprintf (ptwert, "%d", _a_bas.a_ers_kz);
        if (ptab_class.lese_ptab ("a_ers_kz", ptwert) == 0)
        {
            strcpy (ers_txt, ptabn.ptbez);
        }

        if (_a_bas.a_ersatz > 0.0)
        {
            DbClass.sqlin ((double *) &_a_bas.a_ersatz, 3, 0);
            DbClass.sqlout ((char *) a_bz1, 0, 25);
            int dsqlstatus =  DbClass.sqlcomm ("select a_bz1 from a_bas where a = ?");
            if (dsqlstatus == 0)
            {
                 print_mess (1, "%.0lf %s %s\n"
                                "Wir liefern statt dessen %.0lf %s",
                                 _a_bas.a, clipped (_a_bas.a_bz1),
                                  ers_txt, _a_bas.a_ersatz, a_bz1);
                 sprintf (aufps.a,     "%.0lf", _a_bas.a_ersatz);
                 if (AErsInLs)
                 {
                           sprintf (aufps.a_ers, "%0.lf", _a_bas.a);
                 }
                 else
                 {
                           sprintf (aufps.a_ers, "%0.lf", _a_bas.a);
                 }
                 sprintf (buffer, "%.0lf %s %s\n",
                          _a_bas.a, clipped (_a_bas.a_bz1), ers_txt);
                 AddPosTxt (buffer);
                 sprintf (buffer, "Wir liefern statt dessen %.0lf %s",
                                   _a_bas.a_ersatz, a_bz1);
                 AddPosTxt (buffer);
            }
            return TRUE;
        }
        print_mess (2, "%.0lf %s %s\n"
                                "Wir liefern statt dessen %.0lf %s",
                                 _a_bas.a, ers_txt, clipped (_a_bas.a_bz1));
        return FALSE;
}


void AUFPLIST::ReadChargeGew (void)
{
       double a_bsd;
       char buffer [256];


       aufps.charge_gew = 0.0;
       if (lsc2_par == 0 &&
           lsc3_par == 0)
       {
           return;
       }

       if (strcmp (aufps.ls_charge, " ") <= 0)
       {
           return;
       }

       a_bsd = _a_bas.a;


       if (_a_bas.a_grund != 0.0)
       {
           DbClass.sqlin  ((double *) &_a_bas.a_grund, 3, 0);
           DbClass.sqlout ((short *)  &_a_bas.charg_hand, 1, 0);
           int dsqlstatus = DbClass.sqlcomm ("select charg_hand from a_bas where a = ?");
           if (dsqlstatus != 0)
           {
               return;
           }
           a_bsd = _a_bas.a_grund;
       }
       if (_a_bas.charg_hand != 1)
       {
           return;
       }

       sprintf (buffer, "select bsd.bsd_gew "
	 			       "from bsd "
                       "where mdn = ? "
                       "and fil = ? "
                       "and  a = ? "
                       "and chargennr = ?");
      DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
      DbClass.sqlin ((short *) &aufk.fil, 1, 0);
      DbClass.sqlin ((double *) &a_bsd, 3, 0);
      DbClass.sqlin ((char *) aufps.ls_charge, 0, 31);
      DbClass.sqlout ((double *) &aufps.charge_gew, 3, 0);
      int dsqlstatus = DbClass.sqlcomm (buffer);
      aufps.charge_gew += ratod (aufps.lief_me);
      return;
}


BOOL AUFPLIST::DispCharge (void)
{
       char keys [80];
       double a_bsd;

       if (lsc2_par == 0 &&
           lsc3_par == 0)
       {
           return TRUE;
       }

       a_bsd = _a_bas.a;


       if (_a_bas.a_grund != 0.0)
       {
           DbClass.sqlin  ((double *) &_a_bas.a_grund, 3, 0);
           DbClass.sqlout ((short *)  &_a_bas.charg_hand, 1, 0);
           int dsqlstatus = DbClass.sqlcomm ("select charg_hand from a_bas where a = ?");
           if (dsqlstatus != 0)
           {
               return TRUE;
           }
           a_bsd = _a_bas.a_grund;
       }
       if (_a_bas.charg_hand != 1)
       {
           return TRUE;
       }
       sprintf (keys, "%hd %hd %.0lf", aufk.mdn, aufk.fil, a_bsd);

       SEARCHCHARGE *SearchCharge = new SEARCHCHARGE ();
       SearchCharge->SetParams (hMainInst, hMainWin);
       SearchCharge->Setawin (hMainWin);
       EnableWindow (mamain1, FALSE);
       EnableWindow (hWndMain, FALSE);
       SearchCharge->Search (keys);
       EnableWindow (mamain1, TRUE);
       EnableWindow (hWndMain, TRUE);
       if (syskey != KEY5)
       {
           SearchCharge->GetSelectedRow (keys);
           int anz = wsplit (keys, " ", '|');
           strcpy (aufps.ls_charge, wort[0]);
           clipped (aufps.ls_charge);
           aufps.charge_gew = ratod (wort[1]);
       }
       delete SearchCharge;
       return TRUE;
}


BOOL AUFPLIST::BestandOk (void)
{
       double bsd_gew = 0.0;

       if (a_ersatz == 3)
       {
           return DispErsatz ();
       }
 	   if (bsd_kz == 0) return TRUE;
       if (AufCharge)
       {
           return DispCharge ();
       }
	   if (BsdArtikel (ratod (aufps.a)) == FALSE) return TRUE;
       DbClass.sqlout ((double *) &bsd_gew, 3, 0);
       DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
       DbClass.sqlin ((short *) &aufk.fil, 1, 0);
       DbClass.sqlin ((double *) &_a_bas.a, 3, 0);
       DbClass.sqlcomm ("select sum (bsd_gew) from bsd "
                        "where mdn = ? "
                        "and fil = ? "
                        "and a = ?");
       if (a_ersatz == 2 && bsd_gew <= 0.0 && _a_bas.a_grund > 0.0)
       {
           if (abfragejn (NULL, "Artikel nicht mehr vorr�tig\n"
                               "Ersatzartikel buntzen ?", "J"))
           {
//               sprintf (aufps.a,     "%.0lf", _a_bas.a_ersatz);
               sprintf (aufps.a,     "%.0lf", _a_bas.a_grund);
               if (AErsInLs)
               {
                     sprintf (aufps.a_ers, "%0.lf", 0.0);
               }
               else
               {
                     sprintf (aufps.a_ers, "%0.lf", _a_bas.a);
               }
               return TRUE;
           }
           else
           {
               return FALSE;
           }
       }
       else if (a_ersatz > 0 && bsd_gew <= 0.0)
       {
           if (abfragejn (NULL, "Artikel nicht mehr vorr�tig\n"
                               "Artikel erfassen ?", "J"))
           {
//               sprintf (aufps.a,     "%.0lf", _a_bas.a_ersatz);
               sprintf (aufps.a,     "%.0lf", _a_bas.a_grund);
               if (AErsInLs)
               {
                     sprintf (aufps.a_ers, "%0.lf", 0.0);
               }
               else
               {
                     sprintf (aufps.a_ers, "%0.lf", _a_bas.a);
               }
               return TRUE;
           }
           else
           {
               return FALSE;
           }
       }
       return TRUE;
}



int AUFPLIST::fetcha (void)
/**
Artikel holen.
**/
{

       char buffer [256];
	   RECT rect;
       int dsqlstatus;
       char wert [5];
       long posi;
       int art_pos;
       int i;
	   long gruppe;


	   clipped (aufps.a);
	   if (syskey != KEYCR && ratod (aufps.a) == (double) 0.0)
	   {
            return 0;
	   }

	   if (Sonderablauf == ESSIG)
	   {
		if (numeric (aufps.a)) //150712
		{
			dsqlstatus = lese_a_bas (ratod (aufps.a));
			if (dsqlstatus == 0)
			{
				if (_a_bas.a == _a_bas.a_grund)
				{
					dsqlstatus = lese_a_bas_verk_beg (_a_bas.a_grund, aufk.lieferdat);
					if (dsqlstatus == 0)
					{
							dsqlstatus = lese_a_bas_verk_beg ();
							if (dsqlstatus == 0 )
							{
								//nicht eindeutig, daher nochmal den eingegebenen Artikel lesen
							}
							else
							{
							sprintf (aufps.a, "%13.0lf", _a_bas.a);
							}
						dsqlstatus = lese_a_bas (ratod (aufps.a));
					}
				}
			}
		}
	   }

	   if (a_kun_smt == 2 || preis0_mess == 2)
	   {
		   SetQueryKun (kun.kun);
	   }

	   if (!numeric (aufps.a))
	   {
		   if (matchcode == 2)
		   {
			      fetchMatchCode ();
		   }
		   else if (searchmodedirect)
		   {
                  QClass.searcha_direct (eListe.Getmamain3 (),aufps.a);
		   }
		   else
		   {
			   if (matchcode == 3) //WAL-3 �ber a_bas_erw.a_bez
			   {
			       strupcpy (aufps.a, aufps.a);

 		          QClass.querya_erw_direct (eListe.Getmamain3 (),aufps.a);
			   }
			   else
			   {
 		          QClass.querya_direct (eListe.Getmamain3 (),aufps.a);
			   }
		   }
		   if (ratod (aufps.a) == (double) 0.0)
		   {
	           sprintf (aufps.a, "%13.0lf", ratod (aufps.a));
               memcpy (&aufptab[eListe.GetAktRow()],
				       &aufps, sizeof (struct AUFPS));
			   UpdateWindow (mamain1);
               eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
               eListe.ShowAktRow ();
			   return (-1);
		   }
	   }
       aufps.charge_gew = 0.0;
	   sprintf (aufps.a, "%13.0lf", ratod (aufps.a));

	   art_pos = TestNewArt (ratod (aufps.a));
	   if (art_pos != -1 && a_kum_par == FALSE && art_un_tst >= 1)
	   {
				   memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
		           if (abfragejn (eListe.Getmamain3 (),
					        "Artikel bereits im Auftrag, OK?", "J") == 0)
				   {
                        sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
                        memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
                        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                        eListe.ShowAktRow ();
                        return (-1);
				   }
				   WriteLogfile ("Fenster geschlossen\n");
	   }

	   if (art_pos != -1 && a_kum_par && art_un_tst >= 1)
	   {
		           if (abfragejn (eListe.Getmamain3 (),
					        "Der Artikel ist bereits erfasst.\n\n"
							"Artikel bearbeiten ?", "J") == 0)
				   {
                        sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
                        memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
                        eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                        eListe.ShowAktRow ();
                        return (-1);
				   }
	   }

	   if (TestNewArtAuf (ratod (aufps.a)) == 0)
	   {
               sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
               memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
               eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
               eListe.ShowAktRow ();
               return (-1);
	   }

	   if (art_pos != -1 && a_kum_par)
	   {
  		           GetClientRect (eListe.Getmamain3 (), &rect);
                   DeleteLine ();
				   if (art_pos > eListe.GetAktRow ()) art_pos --;
                   eListe.SetNewRow (art_pos);
 		           eListe.SetFeldFocus0 (art_pos, eListe.FirstColumn ());
                   InvalidateRect (eListe.Getmamain3 (), &rect, TRUE);
                   UpdateWindow (eListe.Getmamain3 ());
                   memcpy (&aufps, &aufptab[art_pos], sizeof (struct AUFPS));
				   if (add_me)
				   {
				            add = TRUE;
				            aufme_old = ratod (aufps.auf_me);
				            CreatePlus ();
				   }
				   SetRowItem ();
                   return (0);
	   }

	   WriteLogfile ("GetLastMe\n");
	   GetLastMe (ratod (aufps.a));

	   memset (aufps.bsd, 0, sizeof (aufps.bsd));
	   memcpy (&CronBest.cron_best, &cron_best_null, sizeof (CRON_BEST));
/*
	   CronBest.cron_best.a = ratod (aufps.a);
	   dsqlstatus = CronBest.dbreadfirst_a ();
	   if (dsqlstatus == 0)
	   {
		   sprintf (aufps.bsd, "%.3lf", CronBest.cron_best.me);
	   }
*/

	   strcpy (aufps.bsd, "0.000");
	   CronBest.cron_best.a = ratod (aufps.a);
	   CronBest.cron_best.pr_kz = (int) CClientOrder::BsdPrice;
	   dsqlstatus = CronBest.dbreadfirst ();
	   if (dsqlstatus == 0)
	   {
		   sprintf (aufps.bsd, "%.3lf", CronBest.cron_best.me);
	   }

	   strcpy (aufps.bsd2, "0.000");
	   CronBest.cron_best.pr_kz = (int) (int) CClientOrder::BsdNoPrice;
	   dsqlstatus = CronBest.dbreadfirst ();
	   if (dsqlstatus == 0)
	   {
		   sprintf (aufps.bsd2, "%.3lf", CronBest.cron_best.me);
	   }


	   WriteLogfile ("SetAufEinh\n");
       einh_class.SetAufEinh (1);
       sprintf (aufps.aufp_txt, "%ld", 0l);
       SetFkt (9, leer, NULL);
       set_fkt (NULL, 10);
       SetFkt (10, leer, NULL);
       posi = atol (aufps.posi);
       if (posi == 0)
       {
             i = eListe.GetAktRow ();
             if (i == 0)
             {
                 posi = 1;
             }
             else
             {
                 posi = atol (aufptab[i - 1].posi);
                 posi ++;
             }
             if (PosiEnd (posi) == 0)
             {
                 sprintf (aufps.posi, "%ld", posi);
             }
       }

       dsqlstatus = lese_a_bas (ratod (aufps.a));
	   if (dsqlstatus == 100)
	   {
		   dsqlstatus = ReadEan (ratod (aufps.a));
	   }


// Umvorhersehbare Ereignisse


	   if (dsqlstatus < 0)
	   {
		   print_mess (2, "Fehler %d beim Lesen von Artikel %.0lf", dsqlstatus,
			                                                        ratod (aufps.a));
           sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
	   }


       if (dsqlstatus == 100 || dsqlstatus == 2 || dsqlstatus == 1) //FS-26
       {
           if (syskey != KEYCR &&syskey != KEYTAB
               && eListe.IsAppend ())
           {
               return (-1);
           }
		   if (NoArtMess == FALSE)
		   {
			   if (dsqlstatus < 100) //FS-26
			   {
                   print_mess (2, "Artikel %.0lf ist deaktiviert",
                          ratod (aufps.a));
			   }
			   else
			   {
                   print_mess (2, "Artikel %.0lf nicht gefunden",
                          ratod (aufps.a));
			   }
		   }
           sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

	   WriteLogfile ("Artikel gelesen\n");
	   aufps.a_typ = _a_bas.a_typ;
	   aufps.a_leih = _a_bas.a_leih;
	   if (IsPartyService && _a_bas.a_typ == DIENSTLEISTUNG)
	   {
		   if (aufk.psteuer_kz == 1)
		   {
			   MessageBox (NULL, "Achtung !!\nAuftrag wird auf vollen Steuersatz gesetzt",
				           NULL, MB_OK | MB_ICONWARNING);        
		   }
		   SetToFullTax ();
		   DisableFullTax ();
//		   aufk.psteuer_kz = 2;
	   }

       if (!BestandOk ())
       {
           sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
           return (-1);
       }

	   if (a_kun_smt && Testa_kun () == -1)
	   {
           sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
           memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
           eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
           eListe.ShowAktRow ();
		   return -1;
	   }

	   if (lutz_mdn_par)
	   {

                  memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
                  eListe.ShowAktRow ();
				  gruppe = TestProdLgr (mdnprod);
				  if (gruppe == 0)
				  {
					  gruppe = aufk.mdn;
				  }
	              sprintf (aufps.gruppe, "%ld", gruppe);
	   }
       aufps.dr_folge = _a_bas.dr_folge;
       sprintf (aufps.lang_bez,       "%s %s",      _a_bas.a_bz1, _a_bas.a_bz2);
       sprintf (aufps.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (aufps.a_bz2,       "%s",      _a_bas.a_bz2); 
       if (aufps.pos_id == 0)  aufps.pos_id = GetNextPos_id ();
	   aufps.userdef1 = GetDefaultUserdef1 (aufk.kun,_a_bas.a); //WAL-70

       aufps.a_gew = _a_bas.a_gew;
       sprintf (wert, "%hd", _a_bas.me_einh);
       sprintf (aufps.me_einh, "%hd", _a_bas.me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
       strcpy (aufps.basis_me_bz, ptabn.ptbezk);

	   WriteLogfile ("Felder belegt\n");
       memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
	   WriteLogfile ("Zeile umkopiert\n");
       eListe.ShowAktRow ();
       if (eListe.IsNewRec ())
       {
           aufps.ls_pos_kz = 2;
//           if (ratod (aufps.auf_vk_pr) == (double) 0.0)
           {
                    ReadPr ();
                    if ((Muster == FALSE) && (ratod (aufps.auf_vk_pr) == 0.0) && (preis0_mess == 2 &&
						aufps.a_typ != LEIHARTIKEL))
					{
						disp_mess ("Der Artikel ist nicht im Sortiment des Kunden", 2);
						return -1;
					}
                    if ((Muster == FALSE) && (ratod (aufps.auf_vk_pr) == 0.0) && (preis0_mess == 1 &&
						aufps.a_typ != LEIHARTIKEL))
					{
			          sprintf (buffer, "Achtung !! Preis 0 gelesen\n"
				                       "            OK ?");
                      if (abfragejn (eListe.Getmamain3 (),
					       buffer , "N") == 0)
					  {
                              sprintf (aufptab[eListe.GetAktRow ()].a, "%13.0lf", Akta);
                              memcpy (&aufps, &aufptab[eListe.GetAktRow ()],
								      sizeof (struct AUFPS));
                              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
                              eListe.ShowAktRow ();
		                      return -1;
					  }
			}
           }
           kumebest.mdn = aufk.mdn;
           kumebest.fil = aufk.fil;
           kumebest.kun = aufk.kun;
           strcpy (kumebest.kun_bran2, kun.kun_bran2);
           kumebest.a = ratod (aufps.a);
           ReadMeEinh ();
           if (kun.einz_ausw > 3 || splitls == 2)
           {
			   _a_bas.teil_smt = (short) Smtg.Get (aufk.mdn, aufk.kun, (long) _a_bas.teil_smt);
               sprintf (aufps.teil_smt, "%hd", _a_bas.teil_smt);
           }
           else
           {
               strcpy (aufps.teil_smt, "0");
           }
		   if (vertr_abr_par && aufk.vertr > 0)
		   {
			   Prov.GetProvSatz (aufk.mdn,
				                 aufk.fil,
								 aufk.vertr,
								 aufk.kun,
								 kun.kun_bran2,
								 _a_bas.a,
								 _a_bas.ag,
								 _a_bas.wg,
								 _a_bas.hwg);
			   if (atoi (aufps.sa_kz_sint) && prov_satz.prov_sa_satz != (double) 0.0)
			   {
				   sprintf (aufps.prov_satz, "%4.2lf", prov_satz.prov_sa_satz);
			   }
			   else
			   {
				   sprintf (aufps.prov_satz, "%4.2lf", prov_satz.prov_satz);
			   }


		   }
       }
       memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));

       eListe.ShowAktRow ();
       eListe.SetRowItem ("a", aufptab[eListe.GetAktRow ()].a);
       set_fkt (NULL, 9);
	   if (bsd_kz && ShowBsd == 1)
	   {
			double bsd =  Bsd.Calculate (aufk.mdn, _a_bas.a);
			BsdInfo.SetBsd (bsd);
	   }
	   if (strlen(clipped(ProcessVerkFragen)) >2 ) //WAL-15
	   {
		  SetzeEventVerkFragen(aufps.pos_id, ratod (aufps.a));
	   }

       return 0;
}


int AUFPLIST::fetchkun_bran2(void)
/**
Artikel ueber Kunden-Artikelnummer holen.
**/
{
       int dsqlstatus;
	   char kun_bran2 [3];

       DbClass.sqlin  ((short *) &aufk.mdn,   1, 0);
       DbClass.sqlin  ((short *) &aufk.fil,   1, 0);
       DbClass.sqlin  ((long *)  &aufk.kun,   2, 0);
	   DbClass.sqlout ((char *)  kun_bran2,   0, 3);
	   dsqlstatus = DbClass.sqlcomm ("select kun_bran2 from kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ?");

	   if (dsqlstatus == 100)
	   {
 		      print_mess (2, "Kundenartikel-Nummer %.0lf nicht gefunden",
				                                    ratod (aufps.a_kun));
              sprintf (aufptab[eListe.GetAktRow ()].a_kun, "%.0lf", (double) 0);
              memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
              eListe.ShowAktRow ();
			  return -1;
	   }

       DbClass.sqlin ((short *) &aufk.mdn,   1, 0);
       DbClass.sqlin ((short *) &aufk.fil,   1, 0);
	   DbClass.sqlin ((char *)  kun_bran2,   0, 2);
       DbClass.sqlin ((long *)  &aufps.a_kun, 0, 13);
       DbClass.sqlout ((char *)  aufps.a, 0,14);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun_bran2 = ? "
                                     "and   a_kun = ?");

       if (dsqlstatus == 100)
       {
 		      print_mess (2, "Kundenartikel-Nummer %.0lf nicht gefunden",
				                                    ratod (aufps.a_kun));
              sprintf (aufptab[eListe.GetAktRow ()].a_kun, "%.0lf", (double) 0);
              memcpy (&aufps, &aufptab[eListe.GetAktRow ()], sizeof (struct AUFPS));
              eListe.SetPos (eListe.GetAktRow (), eListe.GetAktColumn ());
              eListe.ShowAktRow ();
			  return -1;
       }
       return fetcha ();
}


int AUFPLIST::fetcha_kun (void)
/**
Artikel ueber Kunden-Artikelnummer holen.
**/
{
       int dsqlstatus;

       DbClass.sqlin ((short *) &aufk.mdn,    1, 0);
       DbClass.sqlin ((short *) &aufk.fil,    1, 0);
       DbClass.sqlin ((long *)  &aufk.kun,   2, 0);
       DbClass.sqlin ((char *)   aufps.a_kun, 0,13);
       DbClass.sqlout ((char *)  aufps.a, 0,14);
       dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? "
                                     "and  a_kun = ?");

	   if (dsqlstatus == 100)
	   {
		       return (fetchkun_bran2 ());
	   }

       return fetcha ();
}

void AUFPLIST::DeleteBsd (void)
/**
Bestand updaten.
**/
{
	    return;
        int dsqlstatus;
        if (aufk.kun_fil == 0)
        {
            memcpy (&best_res, &best_res_null, sizeof (best_res));
            best_res.mdn       = aufp.mdn;
            best_res.fil       = aufp.fil;
            best_res.auf       = aufp.auf;
            dsqlstatus = best_res_class.delete_auf ();
        }
}


double AUFPLIST::GetAufMeVgl (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;
         double auf_me_vgl;

         strcpy (kumebest.kun_bran2, kun.kun_bran2);
         einh_class.AktAufEinh (aufp.mdn, aufp.fil,
                                aufk.kun, aufp.a, aufp.me_einh_kun);
         strcpy (kumebest.kun_bran2, kun.kun_bran2);
         einh_class.GetKunEinh (aufp.mdn, aufp.fil,
                                aufk.kun, aufp.a, &keinheit);

         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                      auf_me_vgl = aufp.auf_me;
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     auf_me_vgl = aufp.auf_me * keinheit.inh;
         }
         return auf_me_vgl;
}


void AUFPLIST::UpdateBsd (void)
/**
Bestand updaten.
**/
{
        char melde_term [12];
        int dsqlstatus;

		return;
        sysdate (melde_term);
        memcpy (&best_res, &best_res_null, sizeof (best_res));
        best_res.mdn       = aufp.mdn;
        best_res.fil       = aufp.fil;
        best_res.a         = aufp.a;
        best_res.auf       = aufp.auf;
        best_res.lief_term = aufk.lieferdat;
        best_res.melde_term = dasc_to_long (melde_term);
        dsqlstatus = best_res_class.dbreadfirst ();
        best_res.me += GetAufMeVgl ();
        best_res.kun_fil = aufk.kun_fil;
        best_res.kun = aufk.kun;
        strcpy (best_res.chargennr, aufp.ls_charge);
        best_res_class.dbupdate ();
}


void AUFPLIST::WriteAufkun (void)
/**
Tabelle aufkun updaten.
**/
{
	   char datum [12];

	   sysdate (datum);
	   aufkun.mdn    = aufk.mdn;
	   aufkun.fil    = aufk.fil;
	   aufkun.kun    = aufk.kun;
	   aufkun.auf    = aufk.auf;
	   aufkun.a      = aufp.a;
	   aufkun.auf_me = aufp.auf_me;
//	   aufkun.auf_vk_pr = aufp.auf_vk_pr;
	   aufkun.auf_vk_pr = aufp.auf_vk_euro;
	   aufkun.lieferdat = aufk.lieferdat;
	   aufkun.bearb     = dasc_to_long (datum);
	   aufkun.kun_fil   = aufk.kun_fil;
	   AufKun.dbupdatelast (aufkunanz);
}

void AUFPLIST::SetPreise (void)
{
       aufp.auf_vk_pr      = ratod (aufps.auf_vk_dm);
       aufp.auf_lad_pr     = ratod (aufps.auf_lad_dm);
       aufp.auf_vk_euro    = ratod (aufps.auf_vk_euro);
       aufp.auf_lad_euro   = ratod (aufps.auf_lad_euro);
       aufp.auf_vk_fremd   = ratod (aufps.auf_vk_fremd);
       aufp.auf_lad_fremd  = ratod (aufps.auf_lad_fremd);
}


BOOL AUFPLIST::testdecval (void)
/**
Groesse der Decimalwerte pruefen.
**/
{
	static double * vars[] = {&aufp.auf_me,
		                      &aufp.lief_me,
							  &aufp.auf_vk_pr,
							  &aufp.auf_lad_pr,
//							  &aufp.tara,
							  &aufp.prov_satz,
//							  &aufp.auf_me_vgl,
							  &aufp.rab_satz,
							  &aufp.auf_me1,
							  &aufp.inh1,
							  &aufp.auf_me2,
							  &aufp.inh2,
							  &aufp.auf_me3,
							  &aufp.inh3,
							  NULL};

	static double values[] =  {99999.999,
		                       99999.999,
							   9999.9999,
							   9999.99,
//							   99999.999,
							   99.99,
//							   999999999.999,
							   999.99,
							   99999.999,
							   99999.999,
							   99999.999,
							   99999.999,
							   99999.999,
							   99999.999};

	int i;

	for (i = 0; vars [i]; i ++)
	{
		if (*vars[i] > values [i])
		{
			*vars[i] = values [i];
		}
		if (*vars[i] < 0 - values [i])
		{
			*vars[i] = 0 - values [i];
		}
	}
	return 0;
}


void AUFPLIST::WritePos (int pos)
/**
Position schreiben.
**/
{
	   KEINHEIT keinheit;
	   double pr_vk;
       char ptwert [4];

       aufp.posi       = atol  (aufptab[pos].posi);
       aufp.pos_id       = aufptab[pos].pos_id;
       aufp.sa_kz_sint = atoi  (aufptab[pos].sa_kz_sint);
       aufp.a          = ratod (aufptab[pos].a);
       aufp.auf_me     = ratod (aufptab[pos].auf_me);
       if (aufp.auf_me == (double) 0) return;

       aufp.rab_satz   = ratod (aufptab[pos].rab_satz);
       aufp.aufschlag   = atoi (aufptab[pos].aufschlag);
       aufp.aufschlag_wert = ratod (aufptab[pos].aufschlag_wert);
       aufp.prov_satz  = ratod (aufptab[pos].prov_satz);
       sprintf (aufp.auf_me_bz,"%s",aufptab[pos].me_bz);
/*
       aufp.auf_vk_pr  = ratod (aufptab[pos].auf_vk_pr);
       aufp.auf_lad_pr  = ratod (aufptab[pos].auf_lad_pr);
*/

       aufp.auf_vk_pr  = ratod (aufptab[pos].auf_vk_dm);
       aufp.auf_lad_pr  = ratod (aufptab[pos].auf_lad_dm);
       aufp.auf_vk_euro  = ratod (aufptab[pos].auf_vk_euro);
       aufp.auf_lad_euro  = ratod (aufptab[pos].auf_lad_euro);
       aufp.auf_vk_fremd  = ratod (aufptab[pos].auf_vk_fremd);
       aufp.auf_lad_fremd  = ratod (aufptab[pos].auf_lad_fremd);

       sprintf (aufp.lief_me_bz,"%s",aufptab[pos].basis_me_bz);
       if (aufp.a == (double) 0)
       {
           return;
       }
       aufp.teil_smt = atoi (aufptab[pos].teil_smt);
       aufp.me_einh_kun = atoi (aufptab[pos].me_einh_kun);


       strcpy (kumebest.kun_bran2, kun.kun_bran2);
       aufp.lief_me = ratod (aufptab[pos].lief_me);

       einh_class.AktAufEinh (aufk.mdn, aufk.fil,
                                aufk.kun, aufp.a, aufp.me_einh_kun);
       strcpy (kumebest.kun_bran2, kun.kun_bran2);
       einh_class.GetKunEinh (aufk.mdn, aufk.fil,
                              aufk.kun, aufp.a, &keinheit);

       if (keinheit.me_einh_kun == keinheit.me_einh_bas)
	   {
             aufp.lief_me = aufp.auf_me;
	   }
       else
	   {
            if (keinheit.inh <= (double) 0.0)
            {
                     keinheit.inh = (double) 1.0;
            }
            aufp.lief_me = aufp.auf_me * keinheit.inh;
	   }

       aufp.me_einh     = atoi (aufptab[pos].me_einh);
       sprintf (ptwert, "%hd", aufp.me_einh);
       if (ptab_class.lese_ptab ("me_einh", ptwert) == 0)
       {
            strcpy (aufp.lief_me_bz, ptabn.ptbezk);
       }
       aufp.aufp_txt    = atol (aufptab[pos].aufp_txt);

       aufp.me_einh_kun1 = atoi (aufptab[pos].me_einh_kun1);
       aufp.auf_me1      = ratod (aufptab[pos].auf_me1);
       aufp.inh1         = ratod (aufptab[pos].inh1);

       aufp.me_einh_kun2 = atoi (aufptab[pos].me_einh_kun2);
       aufp.auf_me2      = ratod (aufptab[pos].auf_me2);
       aufp.inh2         = ratod (aufptab[pos].inh2);

       aufp.me_einh_kun3 = atoi (aufptab[pos].me_einh_kun3);
       aufp.auf_me3      = ratod (aufptab[pos].auf_me3);
       aufp.inh3         = ratod (aufptab[pos].inh3);
       aufp.gruppe       = atol (aufptab[pos].gruppe);

       strcpy (ptabn.ptitem,"sap_kond_art");
	   aufp.a_grund = ratod (aufptab[pos].a_grund);
       aufp.a_ers   = ratod (aufptab[pos].a_ers);
/*
       strcpy (ptabn.ptwer2, aufptab[pos].kond_art);

       DbClass.sqlin ((char *) ptabn.ptitem, 0, 19);
       DbClass.sqlin ((char *) ptabn.ptwer2, 0, 9);
       DbClass.sqlout ((char *) ptabn.ptwer1, 0, 9);
       dsqlstatus = DbClass.sqlcomm ("select ptwer1 from ptabn "
		                             "where ptitem = ? "
									 "and ptwer2 = ?");
*/
       strcpy (aufp.kond_art, aufptab[pos].kond_art0);
       aufp.posi_ext  = atol (aufptab[pos].posi_ext);
       strcpy (aufp.ls_charge, aufptab[pos].ls_charge);
       aufp.ls_pos_kz = aufptab[pos].ls_pos_kz;
       aufp.dr_folge = aufptab[pos].dr_folge;
	   aufp.pos_txt_kz = aufptab[pos].pos_txt_kz;
	   aufp.userdef1 = aufptab[pos].userdef1; //WAL-70
       testdecval ();
       ls_class.update_aufp (aufk.mdn, aufk.fil, aufk.auf,
                             aufp.a, aufp.posi);
       pr_vk = ratod (aufptab[pos].auf_vk_pr);
       StndAuf.UpdStdAuftrag (aufp.mdn, aufp.fil, aufk.kun, aufk.kun_fil, aufp.a, pr_vk);
       if (Getib ())
       {
           UpdateBsd ();
       }
	   if (WriteLastMe)
	   {
			WriteAufkun ();
	   }

}

BOOL AUFPLIST::TestStndActive () 
{
	BOOL ret = FALSE;

    if (ClientOrder.enumGetStatus () == CThread::Running)
	{
		disp_mess ("Einlesen der Standardauftr�ge ist aktiv\n"
			       "Ein Abbruch des Vorgangs ist nicht m�glich", 2);
		ret = TRUE;
	}
    return ret; 
}


int AUFPLIST::WriteAllPos (void)
/**
Alle Positionen schreiben.
**/
{
    int i;
    int recs;
    int row;

    if (ClientOrder.enumGetStatus () == CThread::Running)
	{
		disp_mess ("Einlesen der Standardauftr�ge ist aktiv\n"
			       "Beenden der Listenerfassung ist nicht m�glich", 2);
		return 0;
	}

    row     = eListe.GetAktRow ();

    if (TestRow () == -1)
    {

            eListe.SetFeldFocus0 (eListe.GetAktRow (),
                                  eListe.GetAktColumn ());
            return -1;
    }

	if (ratod (aufps.a))
	{
 	      BucheBsd (ratod (aufps.a), ratod (aufps.auf_me), atoi (aufps.me_einh_kun),
		      ratod (aufps.auf_vk_pr));
	}
    memcpy (&aufptab[row], &aufps, sizeof (struct AUFPS));
    recs = eListe.GetRecanz ();
    ls_class.delete_aufpauf (aufk.mdn, aufk.fil, aufk.auf);
    if (Getib ())
    {
            DeleteBsd ();
    }
    GenNewPosi ();

	if (F2Userdef1) //WAL-70
	{
		for (i = 0; i < recs; i ++)
		{
		   WriteVerkKunAntw (i);	
		}
	}

    for (i = 0; i < recs; i ++)
    {
        WritePos (i);
    }
	StndAuf.CleanUpdError ();
	KillProcess () ; //testtest
    eListe.BreakList ();
	SetFlgInEnterListe (FALSE); //FS-126

    return 0;
}

int AUFPLIST::_WriteAllPos (void) //030611
/**
Alle Positionen schreiben.
**/
{
    int i;
    int recs;
    int row;


    if (ClientOrder.enumGetStatus () == CThread::Running)
	{
		disp_mess ("Einlesen der Standardauftr�ge ist aktiv\n"
			       "Beenden der Listenerfassung ist nicht m�glich", 2);
		return 0;
	}

    row     = eListe.GetAktRow ();

    if (TestRow () == -1)
    {

            eListe.SetFeldFocus0 (eListe.GetAktRow (),
                                  eListe.GetAktColumn ());
            return -1;
    }

	if (ratod (aufps.a))
	{
 	      BucheBsd (ratod (aufps.a), ratod (aufps.auf_me), atoi (aufps.me_einh_kun),
		      ratod (aufps.auf_vk_pr));
	}
    memcpy (&aufptab[row], &aufps, sizeof (struct AUFPS));
    recs = eListe.GetRecanz ();
    ls_class.delete_aufpauf (aufk.mdn, aufk.fil, aufk.auf);
    if (Getib ())
    {
            DeleteBsd ();
    }
    GenNewPosi ();
    for (i = 0; i < recs; i ++)
    {
        WritePos (i);
    }



    return 0;
}


void AUFPLIST::PrVkFromList ()
/**
Preise neu aus Datenbanl holen.
**/
{
    int i;
    int recs;
    int row;

    eListe.DestroyFocusWindow ();
    row     = eListe.GetAktRow ();

    memcpy (&aufptab[row], &aufps, sizeof (struct AUFPS));
    recs = eListe.GetRecanz ();
    for (i = 0; i < recs; i ++)
    {
        UpdatePrVk (i);
		WritePos (i);
    }
	InvalidateRect (eListe.Getmamain3 (), NULL, TRUE);
    memcpy (&aufps, &aufptab[row], sizeof (struct AUFPS));
}


void AUFPLIST::UpdatePrVk (int pos)
{
	double pr_ek;
	double pr_vk;
	short sa;
	char lieferdat [12];
	int dsqlstatus;

	dlong_to_asc (lsk.lieferdat, lieferdat);


    memcpy (&aufps, &aufptab[pos], sizeof (struct AUFPS));
    if (DllPreise.PriceLib != NULL && 
	    DllPreise.preise_holen != NULL)
   {
				  dsqlstatus = (DllPreise.preise_holen) (aufk.mdn, 
			                               0,
				                           aufk.kun_fil,
					                       aufk.kun,
						                   ratod (aufps.a),
							               lieferdat,
								           &sa,
									       &pr_ek,
										   &pr_vk);
				  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
   }
   else
   {
				 dsqlstatus = WaPreis.preise_holen (aufk.mdn,
			                               0,
				                           aufk.kun_fil,
					                       aufk.kun,
						                   ratod (aufps.a),
							               lieferdat,
								           &sa,
									       &pr_ek,
										   &pr_vk);
   }
	if (dsqlstatus != 0) return;

    sprintf (aufps.auf_vk_pr, "%lf",  pr_ek);
    sprintf (aufps.auf_lad_pr,"%lf", pr_vk);
    auf_vk_pr = pr_ek;
	FillWaehrung (pr_ek, pr_vk);
    memcpy (&aufptab[pos], &aufps, sizeof (struct AUFPS));
}


void AUFPLIST::SaveAuf (void)
{
    save_fkt (5);
    save_fkt (6);
    save_fkt (7);
    save_fkt (8);
    save_fkt (12);
}

void AUFPLIST::SetAuf (void)
{
    set_fkt (dokey5, 5);
    set_fkt (InsertLine, 6);
    set_fkt (DeleteLine, 7);
    set_fkt (AppendLine, 8);
    set_fkt (WriteAllPos, 12);
}

void AUFPLIST::RestoreAuf (void)
{
    restore_fkt (5);
    restore_fkt (6);
    restore_fkt (7);
    restore_fkt (8);
    restore_fkt (12);
}


int AUFPLIST::dokey5 (void)
/**
Listenerfassung abbrechen.
**/
{
	short sql_sav;
	extern short sql_mode;

    if (ClientOrder.enumGetStatus () == CThread::Running)
	{
		disp_mess ("Einlesen der Standardauftr�ge ist aktiv\n"
			       "Beenden der Listenerfassung ist nicht m�glich", 2);
		return 0;
	}

    if (abfragejn (mamain1, "Positionen speichern ?", "J"))
    {
           WriteAllPos ();
           syskey = KEY12;
    }

	sql_sav = sql_mode;
	sql_mode = 1;
    DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
    DbClass.sqlin ((short *) &aufk.fil, 1, 0);
    DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
    DbClass.sqlcomm ("delete from aufp where mdn = ? and fil = ? "
                     "and auf = ? and auf_me = 0");

	sql_mode = sql_sav;
    syskey = KEY5;
    RestoreAuf ();

    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());

	KillProcess () ; //testtest
    eListe.BreakList ();
	SetFlgInEnterListe (FALSE); //FS-126
    return 1;
}

int AUFPLIST::doreload (void) //LS-126
/**
Listenerfassung abbrechen.
**/
{
	extern short sql_mode;

    //  Kl�ren ! WriteAllPos ();

//    RestoreAuf ();

    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());
	// KillProcess () ; //testtest

    eListe.BreakList ();
	SetFlgInEnterListe (TRUE); 
    return 1;
}


void AUFPLIST::DoBreak (void)
{
	short sql_sav;
	extern short sql_mode;

	sql_sav = sql_mode;
	sql_mode = 1;
    DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
    DbClass.sqlin ((short *) &aufk.fil, 1, 0);
    DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
    DbClass.sqlcomm ("delete from aufp where mdn = ? and fil = ? "
                     "and auf = ? and auf_me = 0");

	sql_mode = sql_sav;
    syskey = KEY5;
    RestoreAuf ();

    eListe.SetListFocus (0);
    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ());

	KillProcess () ; //testtest
	SetFlgInEnterListe (FALSE); //FS-126

    eListe.BreakList ();
}


void AUFPLIST::InitSwSaetze (void)
{
       int i;

       for (i = 0; i < MAXPOS; i ++)
       {
               SwSaetze [i] = (char *) &aufptab [i];
       }
}

void AUFPLIST::SetRecHeight (void)
/**
Anzahl Zeilen pro Listzeile ermitteln.
**/
{
       int height;
       int i;

       height = 0;
       for (i = 0; i < dataform.fieldanz; i ++)
       {
           if (dataform.mask[i].attribut & REMOVED) continue;
           if (dataform.mask[i].pos[0] > height)
           {
               height = dataform.mask[i].pos[0];
           }
       }
       height ++;
       eListe.SetRecHeight (height);
}

int AUFPLIST::ToMemory (int pos)
/**
SW-Satz in Ascii-Format Speichern.
**/
{

       memcpy (SwSaetze[pos], ausgabesatz, zlen);
       eListe.SetRecanz (pos + 1);
       return 0;
}


void AUFPLIST::SetStringEnd (char *feldname, int len)
/**
Stringende Setzen.
**/
{

       return;
}


void AUFPLIST::uebertragen (void)
/**
Eingabesatz in ASCII-Struktur uebertragen.
**/
{
	   static int cursor = -1;

	   if (cursor == -1 && (GetFieldAttr ("a_kun") & REMOVED) == 0)
	   {
                 DbClass.sqlin ((short *) &aufk.mdn,    1, 0);
                 DbClass.sqlin ((short *) &aufk.fil,    1, 0);
                 DbClass.sqlin ((long *)  &aufk.kun,   2, 0);
                 DbClass.sqlin ((char *)  aufps.a, 0,14);
                 DbClass.sqlout ((char *) aufps.a_kun, 0,13);
                 cursor = DbClass.sqlcursor ("select a_kun from a_kun "
                                     "where mdn = ? "
                                     "and   fil = ? "
                                     "and   kun = ? "
                                     "and   a = ?");
	   }
       lese_a_bas (aufp.a);
	   aufps.a_typ = _a_bas.a_typ;
	   aufps.a_leih = _a_bas.a_leih;

       sprintf (aufps.posi,        "%4ld",    aufp.posi);
       sprintf (aufps.sa_kz_sint,  "%1hd",    aufp.sa_kz_sint);
       sprintf (aufps.a,           "%13.0lf", aufp.a);
       sprintf (aufps.lang_bez,       "%s %s",      _a_bas.a_bz1, _a_bas.a_bz2);
       sprintf (aufps.a_bz1,       "%s",      _a_bas.a_bz1);
       sprintf (aufps.a_bz2,       "%s",      _a_bas.a_bz2);
	   aufps.pos_id = aufp.pos_id;
       if (aufps.pos_id == 0)  aufps.pos_id = GetNextPos_id ();
	   aufps.userdef1 = aufp.userdef1; //WAL-70
       aufps.a_gew = _a_bas.a_gew;
       sprintf (aufps.last_me,     "%8.3lf",  0,0);
       sprintf (aufps.auf_me,      "%8.3lf",  aufp.auf_me);
       sprintf (aufps.lief_me,     "%8.3lf",  aufp.lief_me);
       sprintf (aufps.rab_satz,    "%5.2lf",  aufp.rab_satz);
       sprintf (aufps.aufschlag,    "%hd",  aufp.aufschlag);
       sprintf (aufps.aufschlag_wert, "%12.4lf",  aufp.aufschlag_wert);
       sprintf (aufps.prov_satz,    "%5.2lf", aufp.prov_satz);
       sprintf (aufps.me_bz, "%s",            aufp.auf_me_bz);
       sprintf (aufps.auf_vk_dm,  "%8.4lf",   aufp.auf_vk_pr);
       sprintf (aufps.auf_lad_dm, "%8.4lf",   aufp.auf_lad_pr);
       sprintf (aufps.auf_vk_euro,  "%8.4lf",   aufp.auf_vk_euro);
       sprintf (aufps.auf_lad_euro, "%8.4lf",   aufp.auf_lad_euro);
       sprintf (aufps.auf_vk_fremd,  "%8.4lf",   aufp.auf_vk_fremd);
       sprintf (aufps.auf_lad_fremd, "%8.4lf",   aufp.auf_lad_fremd);
	   FillAktWaehrung ();

	   CalcLdPrPrc (ratod (aufps.auf_vk_pr), ratod (aufps.auf_lad_pr));
       sprintf (aufps.basis_me_bz, "%s",      aufp.lief_me_bz);
       sprintf (aufps.teil_smt,    "%hd",     aufp.teil_smt);
       sprintf (aufps.me_einh_kun, "%hd",     aufp.me_einh_kun);
       sprintf (aufps.me_einh,     "%hd",     aufp.me_einh);
       sprintf (aufps.aufp_txt,    "%ld",     aufp.aufp_txt);

       sprintf (aufps.me_einh_kun1,"%hd",     aufp.me_einh_kun1);
       sprintf (aufps.auf_me1,     "%8.3lf",  aufp.auf_me1);
       sprintf (aufps.inh1,        "%8.3lf",  aufp.inh1);

       sprintf (aufps.me_einh_kun2,"%hd",     aufp.me_einh_kun2);
       sprintf (aufps.auf_me2,     "%8.3lf",  aufp.auf_me2);
       sprintf (aufps.inh2,        "%8.3lf",  aufp.inh2);

       sprintf (aufps.me_einh_kun3,"%hd",     aufp.me_einh_kun3);
       sprintf (aufps.auf_me3,     "%8.3lf",  aufp.auf_me3);
       sprintf (aufps.inh3,        "%8.3lf",  aufp.inh3);
       sprintf (aufps.gruppe,      "%ld",     aufp.gruppe);
       sprintf (aufps.kond_art,    "%s",     aufp.kond_art);
       sprintf (aufps.kond_art0,   "%s",     aufp.kond_art);
       sprintf (aufps.posi_ext, "%ld", aufp.posi_ext);
       sprintf (aufps.ls_charge, "%s", aufp.ls_charge);
	   aufps.pos_txt_kz = aufp.pos_txt_kz;
       aufps.charge_gew = 0.0;
	   FillKondArt (aufps.kond_art);
       aufps.ls_pos_kz = aufp.ls_pos_kz;
	   sprintf (aufps.a_grund, "%4.0lf", aufp.a_grund);

	   strcpy (aufps.a_kun, " ");
	   if (ReadLastMe)
	   {
			GetLastMe (ratod (aufps.a));
	   }
	   memset (aufps.bsd, 0, sizeof (aufps.bsd));
	   memcpy (&CronBest.cron_best, &cron_best_null, sizeof (CRON_BEST));
	   CronBest.cron_best.a = ratod (aufps.a);
	   int dsqlstatus = CronBest.dbreadfirst_a ();
	   if (dsqlstatus == 0)
	   {
		   sprintf (aufps.bsd, "%.3lf", CronBest.cron_best.me);
	   }
	   if (cursor != -1 && (GetFieldAttr ("a_kun") & REMOVED) == 0)
	   {
	           DbClass.sqlopen (cursor);
	           DbClass.sqlfetch (cursor);
	   }
       aufps.dr_folge = aufp.dr_folge;
       ReadChargeGew ();

}

void AUFPLIST::AfterPaint (HDC hdc, RECT *rect, int pos, int SubRow)
{
	if (SubRow != 0) return;

	if (NoteIcon != NULL && atol (aufptab[pos].aufp_txt) != 0l)
	{
		int plus = max (0, (rect->bottom - rect->top - 16) / 2);
		int y = rect->top + plus; 
		BitMap.DrawBitmap (hdc, NoteIcon, rect->left + 1, y);
	}
}

void AUFPLIST::ShowDB (short mdn, short fil, long auf)
/**
Auftragspositionen lesen und anzeigen.
**/
{
        int i;
        HCURSOR oldcursor;
        extern short do_exit;
        int dsqlstatus;
		CColBkName **it;
		CColBkName *ColBkName;


        InitSwSaetze ();
        eListe.SetRecanz (0);
        i = eListe.GetRecanz ();
        eListe.SetUbForm (&ubform);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        dsqlstatus = ls_class.lese_aufp (mdn, fil, auf);
        while (dsqlstatus == 0)
        {
                     uebertragen ();
		 			 LISTHANDLER->Update (atoi(aufps.posi), &aufps);

                     if (ToMemory (i) != 0) break;
                     i = eListe.GetRecanz ();
                     if (i >= MAXPOS) break;
                     dsqlstatus = ls_class.lese_aufp ();
        }

        SetRecHeight ();
        SetCursor (oldcursor);
        i = eListe.GetRecanz ();

        eListe.SetRecanz (i);
        SwRecs = eListe.GetRecanz ();

        SetFieldAttr ("a", DISPLAYONLY);
        eListe.SetDataForm0 (&dataform, &lineform);
        eListe.SetSaetze (SwSaetze);
        eListe.SetChAttr (ChAttr);
        eListe.SetUbRows (ubrows);
		eListe.InitColBkColor ();
		ColBkNames.Start ();
		while ((it = ColBkNames.GetNext ()) != NULL)
		{
			ColBkName = *it;
			if (ColBkName != NULL)
			{
				eListe.AddColBkColor (ColBkName->GetName (), ColBkName->GetColor ());
			}
		}

        if (i == 0)
        {
			     Posanz = 0;
				 memcpy (&aufps, &aufps_null, sizeof (struct AUFPS));
                 eListe.AppendLine ();
				 AktRow = AktColumn = 0;
                 eListe.SetPos (AktRow, AktColumn);
                 i = eListe.GetRecanz ();
        }
		else
		{
			     Posanz = i;
		}

        SendMessage (eListe.Getmamain2 (), WM_SIZE, NULL, NULL);
        InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
        UpdateWindow (eListe.Getmamain3 ());
        memcpy (&aufps, &aufptab[0], sizeof (struct AUFPS));
 	    if (! numeric (aufps.a))
		{
		   sprintf (aufps.a, "%.0lf", 0.0);
           memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
		}
   }


void AUFPLIST::ReadDB (short mdn, short fil, long auf)
/**
Auftragspositionen lesen.
**/
{
        ausgabesatz = (unsigned char *) &aufps;
        zlen = sizeof (struct AUFPS);

        PageView = 0;
        eListe.SetPageView (0);
        feld_anz = dataform.fieldanz;
        Lstzlen = zlen;
        LstSatz = ausgabesatz;

        Lstbanz = feld_anz;

        eListe.SetPos (AktRow, AktColumn);
        eListe.Setbanz (feld_anz);
        eListe.Setzlen (zlen);
        eListe.Initscrollpos ();
        eListe.SetAusgabeSatz (ausgabesatz);
}

void AUFPLIST::DestroyWindows (void)
{
       eListe.DestroyListWindow ();
       DestroyMainWindow ();
       InitMax ();
       InitMin ();
       MoveMamain1 ();
       ListAktiv = 0;
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
}

void AUFPLIST::SetSchirm (void)
{
       set_fkt (Schirm, 11);
       SetFkt (11, vollbild, KEY11);
}


void AUFPLIST::SetNoRecNr (void)
{
	   static BOOL SetOK = FALSE;
	   int i;

	   if (SetOK) return;

	   for (i = 0; i < dataform.fieldanz; i ++)
	   {
		   dataform.mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < ubform.fieldanz; i ++)
	   {
		   ubform.mask[i].pos[1] -= 6;
	   }
	   for (i = 0; i < lineform.fieldanz; i ++)
	   {
		   lineform.mask[i].pos[1] -= 6;
	   }
	   eListe.SetNoRecNr (TRUE);
       SetOK = TRUE;
}

void AUFPLIST::FillWaehrungBz (void)
{
       static BOOL WaeOK = FALSE;
       char ptwert [5];

       if (WaeOK) return;

       WaeOK = TRUE;
       sprintf (ptwert, "%hd", 1);
       if (ptab_class.lese_ptab ("waehrung", ptwert) == 0)
       {
           clipped (ptabn.ptbezk);
           VK_DM = new char [20];
           LD_DM = new char [20];
           if (VK_DM != NULL)
           {
                 sprintf (VK_DM, " VK %s", ptabn.ptbezk);
           }
           else
           {
                 VK_DM     = " VK EURO  ";
           }
           if (LD_DM != NULL)
           {
                 sprintf (LD_DM, " LD %s", ptabn.ptbezk);
           }
           else
           {
                  LD_DM     = " LD EURO  ";
           }
       }
       sprintf (ptwert, "%hd", 2);
       if (ptab_class.lese_ptab ("waehrung", ptwert) == 0)
       {
           clipped (ptabn.ptbezk);
           VK_EURO = new char [20];
           LD_EURO = new char [20];
           if (VK_EURO != NULL)
           {
                 sprintf (VK_EURO, " VK %s", ptabn.ptbezk);
           }
           else
           {
                 VK_EURO     = " VK DM  ";
           }
           if (LD_EURO != NULL)
           {
                 sprintf (LD_EURO, " LD %s", ptabn.ptbezk);
           }
           else
           {
                  LD_EURO     = " LD DM  ";
           }
       }

       sprintf (ptwert, "%hd", 3);
       if (ptab_class.lese_ptab ("waehrung", ptwert) == 0)
       {
           clipped (ptabn.ptbezk);
           VK_FREMD = new char [20];
           LD_FREMD = new char [20];
           if (VK_FREMD != NULL)
           {
                 sprintf (VK_FREMD, " VK %s", ptabn.ptbezk);
           }
           else
           {
                 VK_FREMD     = " VK FREMD  ";
           }
           if (LD_FREMD != NULL)
           {
                 sprintf (LD_FREMD, " LD %s", ptabn.ptbezk);
           }
           else
           {
                  LD_FREMD     = " LD FREMD  ";
           }
       }
}


void AUFPLIST::SetPrPicture (void)
{
       int fpos;

       fpos = GetItemPos (&dataform, "pr_vk");
       if (fpos == -1) return;

       if (nachkpreis == 0)
       {
           dataform.mask[fpos].picture = "%4.0f";
       }
       else if (nachkpreis == 1)
       {
           dataform.mask[fpos].picture = "%6.1f";
       }
       else if (nachkpreis == 3)
       {
           dataform.mask[fpos].picture = "%7.3f";
       }

       else if (nachkpreis == 4)
       {
           dataform.mask[fpos].picture = "%8.4f";
       }
}


void AUFPLIST::ShowAufp (short mdn, short fil, long auf,
                         short kunfil, long kun, char *ldat)
/**
Auftragsliste bearbeiten.
**/

{
	   int pos;

       if (ListAktiv) return;

//	   eListe.RowEvent = this;
       FillWaehrungBz ();
	   if (aufk.waehrung == 0)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK);
				 }
	             pos = GetItemPos (&ubform, "lad_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD);
				 }
	   }
	   else if (aufk.waehrung == 1)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_DM);
				 }
	             pos = GetItemPos (&ubform, "ld_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_DM);
				 }
	   }
	   else if (aufk.waehrung == 2)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_EURO);
				 }
	             pos = GetItemPos (&ubform, "ld_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_EURO);
				 }
	   }
	   else
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_FREMD);
				 }
	             pos = GetItemPos (&ubform, "ld_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_FREMD);
				 }
	   }

       Geta_bz2_par ();
       Geta_kum_par ();
	   GetCfgValues ();
	   if (LdVkAsPercent)
	   {
		    DelLadVKPrc = FALSE;
	   }
       strcpy (sys_par.sys_par_nam,"nachkpreis");
       if (sys_par_class.dbreadfirst () == 0)
       {
            nachkpreis = atoi (sys_par.sys_par_wrt);
            SetPrPicture ();
       }
       strcpy (sys_par.sys_par_nam,"lsc2_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
            lsc2_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam,"lsc3_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
            lsc3_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam,"pos_txt_kz_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
		     int value =  atoi (sys_par.sys_par_wrt);
             if (value == 0)
			 {
				 TxtByDefault = TRUE;
			 }
			 else
			 {
				 TxtByDefault = FALSE;
			 }
       }
       if (FlgInEnterListe == FALSE) //FS-126
	   {
		   if (AddLastLief)
			{
		        addLastLdat ("auf_me");
			}
			if (AddLastVkPr)
			{
		        addLastVkPr ("auf_me");
			}
			if (AddLangBez) //WAL-11
			{
		        addLangBez ("a_bz1");
		        DelListField ("a_bz1");
			}
	   }
	   if (FormOK == FALSE)
	   {
           if (DelLadVK)
		   {
			     DelListField ("ld_pr");
		   }
           if (DelLadVK0)
		   {
			     DelListField ("ld_pr0");
		   }
           if (DelPrVK)
		   {
			     DelListField ("pr_vk");
		   }
           if (DelLastMe)
		   {
			     DelListField ("last_me");
		   }
           if (DelLadVKPrc)
		   {
			     DelListField ("ld_pr_prc");
		   }
           if (RemoveBasisMe)
		   {
			     DelListField ("basis_me_bz");
		   }
           if (RemoveMarge)
		   {
			     DelListField ("marge");
		   }
           if (RemoveBsd)
		   {
			     DelListField ("bsd");
			     DelListField ("bsd2");
		   }
		   FormOK = TRUE;
	   }
       aufk.mdn = mdn;
       aufk.fil = fil;
       aufk.auf = auf;
       aufk.kun = kun;
       aufk.lieferdat = dasc_to_long (ldat);

   	   add = FALSE;
	   aufme_old = (double) 0.0;
	   DestroyPlus ();
       SetSchirm ();
       eListe.SetInfoProc (InfoProc);
       sprintf (InfoCaption, "Auftrag %ld", auf);
       mamain1 = CreateMainWindow ();
       eListe.InitListWindow (mamain1);
       ReadDB (mdn, fil, auf);

       eListe.SetListFocus (0);
       eListe.Initscrollpos ();
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
       if (FlgInEnterListe == FALSE) //FS-126
	   {
	       ShowDB (mdn, fil, auf);
	   }

       eListe.SetRowItem ("a", aufptab[0].a);

       ListAktiv = 1;

       return;
}

void AUFPLIST::GetListColor (COLORREF *color, char *cfg_v)
/**
Farbunterlegung fuer Listen uebertragen.
**/
{
	static char *ColTxt[] = {"BLACKCOL",
		                     "WHITECOL",
							 "BLUECOL",
							 "REDCOL",
							 "LTGRAYCOL",
							 "GREENCOL",
							 "YELLOWCOL",
							 "DKYELLOWCOL",
							 NULL};

	static COLORREF ColVal[] = {BLACKCOL,
		                        WHITECOL,
					  		    BLUECOL,
							    REDCOL,
							    LTGRAYCOL,
							    GREENCOL,
							    YELLOWCOL,
								DKYELLOWCOL};
	int i;
	int red, blue, green;
	char *ColR;
	int Cols = 0;

	for (i = 0; ColTxt[i]; i ++)
	{
		if (strcmp (ColTxt[i], cfg_v) == 0)
		{
			*color = ColVal[i];
			return;
		}
	}


	ColR = strstr (cfg_v, "red");
	if (ColR)
	{
		red = atoi (&ColR[3]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "blue");
	if (ColR)
	{
		blue = atoi (&ColR[4]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "green");
	if (ColR)
	{
		green = atoi (&ColR[5]);
		Cols ++;
	}
	if (Cols == 3)
	{
		*color = RGB (red, green, blue);
	}

}

void AUFPLIST::SetPreisTest (int mode)
{
	if (mode == 0) return;

	preistest = min (4, max (1, mode));
	if (preistest == 3)
	{
		SetItemAttr (&dataform, "pr_vk", DISPLAYONLY);
        VkAttribut = DISPLAYONLY;
	}
}



void AUFPLIST::GetCfgValues (void)
/**
Werte aus 51100.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
       char cfg_v [512];

 	   if (cfgOK) return;




	   cfgOK = TRUE;
       if (ProgCfg.GetCfgValue ("F2Userdef1", cfg_v) == TRUE)
	   {
                     F2Userdef1 = atoi (cfg_v);
	   }
        if (ProgCfg.GetCfgValue ("userdef1_BCOLOR", cfg_v) == TRUE)  //LAC-8
        {
		             GetListColor (&userdef1_bColor, cfg_v);
        }

       if (ProgCfg.GetCfgValue ("3DList", cfg_v) == TRUE)
       {
                    eListe.Set3D (atoi (cfg_v));
	   }
       if (ProgCfg.GetCfgValue ("InfoProcess", cfg_v, "$$") == TRUE)
       {
                    strcpy (InfoProcess,clipped(cfg_v));
	   }
	   else
	   {
	       if (ProgCfg.GetCfgValue ("InfoProcess", cfg_v) == TRUE)
		   {
                    strcpy (InfoProcess,clipped(cfg_v));
		   }
	   }
       if (ProgCfg.GetCfgValue ("ProcessVerkFragen", cfg_v, "$$") == TRUE)  //WAL-15
       {
                    strcpy (ProcessVerkFragen,clipped(cfg_v));
	   }
	   else
	   {
	       if (ProgCfg.GetCfgValue ("ProcessVerkFragen", cfg_v) == TRUE)  //WAL-15
		   {
                    strcpy (ProcessVerkFragen,clipped(cfg_v));
		   }
	   }

       if (ProgCfg.GetCfgValue ("InfoProcessRefresh", cfg_v) == TRUE)
	   {
                     InfoProcessRefresh = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("InfoSQL", cfg_v, "$$") == TRUE)
       {
                    strcpy (InfoSQL,clipped(cfg_v));
	   }


       if (ProgCfg.GetCfgValue ("Sonderablauf", cfg_v) == TRUE)
       {
                     Sonderablauf = atol (cfg_v);
       }
       if (ProgCfg.GetCfgValue ("3DSize", cfg_v) == TRUE)
       {
                    eListe.SetPlus3D (atoi (cfg_v));
	   }
       if (ProgCfg.GetCfgValue ("ListFocus", cfg_v) == TRUE)
       {
                    ListFocus = atoi (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("plu_size", cfg_v) ==TRUE)
       {
                    plu_size = atoi (cfg_v);
       }
       else
        {
                    plu_size = 0;
        }
        if (ProgCfg.GetCfgValue ("auf_me_default", cfg_v) == TRUE)
        {
                    auf_me_default = atoi (cfg_v);
        }
        else
        {
                    auf_me_default = 0;
        }
        if (ProgCfg.GetCfgValue ("searchadirect", cfg_v) == TRUE)
		{
			        searchadirect = min (1, atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("searchmodedirect", cfg_v) == TRUE)
		{
			        searchmodedirect = min (1, atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("rowheight", cfg_v) == TRUE)
		{
			        RowHeight = ratod (cfg_v);
					if (RowHeight < (double) 1.0)
					{
						RowHeight = (double) 1.0;
					}
		}
        if (ProgCfg.GetCfgValue ("ubheight", cfg_v) == TRUE)
		{
			        UbHeight = atoi (cfg_v);
					SetUbHeight ();
		}
		eListe.SetRowHeight (RowHeight);
/*
        if (ProgCfg.GetCfgValue ("listfocus", cfg_v) == TRUE)
		{
			        ListFocus = min (4, atoi (cfg_v));
					ListFocus = max (3, ListFocus);
		}
*/
        if (ProgCfg.GetCfgValue ("matchcode", cfg_v) == TRUE)
		{
			         matchcode = atoi (cfg_v);
			         SetMatchCode (atoi (cfg_v));
					 if (matchcode == 3) searchmodedirect = 0;  //WAL-3
		}
        if (ProgCfg.GetCfgValue ("bsd_kz", cfg_v) == TRUE)
		{
			         bsd_kz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("ShowBsd", cfg_v) == TRUE)
		{
			         ShowBsd = atoi (cfg_v);
					 if (ShowBsd == 1) BsdInfo.Refresh = TRUE;
		}
        if (ProgCfg.GetCfgValue ("rab_prov_kz", cfg_v) == TRUE)
		{
			         rab_prov_kz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("auf_wert_anz", cfg_v) == TRUE)
		{
			         auf_wert_anz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("auf_gew_anz", cfg_v) == TRUE)
		{
			         auf_gew_anz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("a_kun_smt", cfg_v) == TRUE)
		{
			         a_kun_smt = atoi (cfg_v);
					 if (a_kun_smt == 2)
					 {
						QClass.testa_kun = Testa_kun;		
					 }
		}
		if (ProgCfg.GetCfgValue ("split_ls", cfg_v) == TRUE)
		{

		              splitls = (atoi (cfg_v));
		}


        if (ProgCfg.GetCfgValue ("LISTCOLORS", cfg_v) == TRUE)
        {
		             ListColors =  min (1, max (0, atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("KOMPFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompfColor, cfg_v);
        }

        if (ProgCfg.GetCfgValue ("ActBkColor", cfg_v) == TRUE)
        {
			         COLORREF ActBkColor;
		             GetListColor (&ActBkColor, cfg_v);
		             GetListColor (&AktZeile_bColor, cfg_v); //WAL-70
		             eListe.SetActBkColor (ActBkColor);
        }


        if (ProgCfg.GetCfgValue ("PrVkBkColor", cfg_v) == TRUE)
        {
			         COLORREF Color;
		             GetListColor (&Color, cfg_v);
					 ColBkNames.Add (new CColBkName ("pr_vk", Color));
        }

        if (ProgCfg.GetCfgValue ("LadPrBkColor", cfg_v) == TRUE)
        {
			         COLORREF Color;
		             GetListColor (&Color, cfg_v);
					 ColBkNames.Add (new CColBkName ("ld_pr", Color));
        }

        if (ProgCfg.GetCfgValue ("LadPrPrcBkColor", cfg_v) == TRUE)
        {
			         COLORREF Color;
		             GetListColor (&Color, cfg_v);
					 ColBkNames.Add (new CColBkName ("ld_pr_prc", Color));
        }

        if (ProgCfg.GetCfgValue ("LastMeBkColor", cfg_v) == TRUE)
        {
			         COLORREF Color;
		             GetListColor (&Color, cfg_v);
					 ColBkNames.Add (new CColBkName ("last_me", Color));
        }

        if (ProgCfg.GetCfgValue ("LPrVkBkColor", cfg_v) == TRUE)
        {
			         COLORREF Color;
		             GetListColor (&Color, cfg_v);
					 ColBkNames.Add (new CColBkName ("lpr_vk", Color));
        }

        if (ProgCfg.GetCfgValue ("LieferdatBkColor", cfg_v) == TRUE)
        {
			         COLORREF Color;
		             GetListColor (&Color, cfg_v);
					 ColBkNames.Add (new CColBkName ("lieferdat", Color));
        }

        if (ProgCfg.GetCfgValue ("LadPr0BkColor", cfg_v) == TRUE)
        {
			         COLORREF Color;
		             GetListColor (&Color, cfg_v);
					 ColBkNames.Add (new CColBkName ("ld_pr0", Color));
        }

        if (ProgCfg.GetCfgValue ("MargeBkColor", cfg_v) == TRUE)
        {
			         COLORREF Color;
		             GetListColor (&Color, cfg_v);
					 ColBkNames.Add (new CColBkName ("marge", Color));
        }

        if (ProgCfg.GetCfgValue ("KOMPBCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompbColor, cfg_v);
        }
        if (ProgCfg.GetCfgValue ("SAFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SafColor, cfg_v);
					 MessCol = SafColor;
        }
        if (ProgCfg.GetCfgValue ("SABCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SabColor, cfg_v);
					 MessBkCol = SafColor;
        }
        if (ProgCfg.GetCfgValue ("sortstnd", cfg_v) == TRUE)
        {
		             StndAuf.SetSortMode (atoi (cfg_v));
					 ClientOrder.SetSortMode ((CClientOrder::STD_SORT) atoi (cfg_v));
        }
        if (ProgCfg.GetCfgValue ("StndRdOptimize", cfg_v) == TRUE)
        {
			         StndRdOptimize = (CClientOrder::STD_RDOPTIMIZE) atoi (cfg_v);
					 ClientOrder.SetRdOptimize ((CClientOrder::STD_RDOPTIMIZE) atoi (cfg_v));
        }
        if (ProgCfg.GetCfgValue ("preistest", cfg_v) == TRUE)
        {
		             SetPreisTest (atoi (cfg_v));
        }

        if (ProgCfg.GetCfgValue ("sacreate", cfg_v) == TRUE)
        {
		             sacreate = min (1, max (0, (atoi (cfg_v))));
        }
        if (ProgCfg.GetCfgValue ("pr_vk", cfg_v) == TRUE)
        {
					 Spalte_pr_vk_Ok = TRUE;
			         if (atoi (cfg_v) == 0)
					 {
						 DelPrVK = TRUE;
						 Spalte_pr_vk_Ok = FALSE;
					 }

                     if (atoi (cfg_v) == 1)
                     {
   		                  SetItemAttr (&dataform, "pr_vk", DISPLAYONLY);
                          VkAttribut = DISPLAYONLY;
                     }

        }
			if (sys_ben.berecht == 1 )  //bei berecht 1 kein Vk editieren 
			{
   		                  SetItemAttr (&dataform, "pr_vk", DISPLAYONLY);
                          VkAttribut = DISPLAYONLY;
			}

        if (ProgCfg.GetCfgValue ("LastFromAufKun", cfg_v) == TRUE)
        {
			         LastFromAufKun = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("proptimize", cfg_v) == TRUE)
        {
			          WaPreis.SetOptimize (atoi (cfg_v));
        }
        if (ProgCfg.GetGroupDefault ("pr_alarm", cfg_v) == TRUE)
        {
                      prproz_diff = ratod (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("art_un_tst", cfg_v) == TRUE)
        {
		             art_un_tst =   atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("add_me", cfg_v) == TRUE)
        {
		             add_me =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("a_kum", cfg_v) == TRUE)
        {
                     a_kum_par =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("preis0_mess", cfg_v) == TRUE)
        {
                     preis0_mess = atoi (cfg_v);
					 if (preis0_mess == 2)
					 {
						QClass.testa_kun = Testa_kun;		
					 }
        }
        if (ProgCfg.GetCfgValue ("dauertief", cfg_v) == TRUE)
        {
                     dauertief = atol (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("NoRecNr", cfg_v) == TRUE)
        {
			        NoRecNr = atoi (cfg_v);
		            if (atoi (cfg_v))
					{
	                        SetNoRecNr (); 
					}
		}
        if (ProgCfg.GetCfgValue ("RemoveKondArt", cfg_v) == TRUE)
        {
                    RemoveKondArt = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("RemoveAGrund", cfg_v) == TRUE)
        {
                    RemoveAGrund = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("RemoveAGrund0", cfg_v) == TRUE)
        {
                    RemoveAGrund0 = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("len_a", cfg_v) == TRUE)
        {
		            SetNewLen ("a", atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("picture_a", cfg_v) == TRUE)
        {
		            SetNewPicture ("a", cfg_v);
		}
        if (ProgCfg.GetCfgValue ("len_a_bz1", cfg_v) == TRUE)
        {
		            SetNewLen ("a_bz1", atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("len_me_bz", cfg_v) == TRUE)
        {
		            SetNewLen ("me_bz", atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("len_basis_me_bz", cfg_v) == TRUE)
        {
		            SetNewLen ("basis_me_bz", atoi (cfg_v));
		}
        if (ProgCfg.GetCfgValue ("ld_pr_prim", cfg_v) == TRUE)
		{
			         ld_pr_prim = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("autopfand", cfg_v) == TRUE)
		{
			         autopfand = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("PosSave", cfg_v) == TRUE)
		{
			         PosSave = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("PosSaveMess", cfg_v) == TRUE)
		{
			         PosSaveMess = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("LiefMeDirect", cfg_v) == TRUE)
		{
			         LiefMeDirect = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("meoptimize", cfg_v) == TRUE)
		{
			         meoptimize = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("a_ersatz", cfg_v) == TRUE)
		{
			         a_ersatz = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("AErsInLs", cfg_v) == TRUE)
		{
			         AErsInLs = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("StndDirect", cfg_v) == TRUE)
		{
			         StndDirect = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("StndWithWindow", cfg_v) == TRUE)
		{
			         StndWithWindow = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("StndWithSound", cfg_v) == TRUE)
		{
			         StndWithSound = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("ReadLastMe", cfg_v) == TRUE)
		{
			         ReadLastMe = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("WriteLastMe", cfg_v) == TRUE)
		{
			         WriteLastMe = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("AufCharge", cfg_v) == TRUE)
		{
			         AufCharge = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("FilStandard", cfg_v) == TRUE)
		{
			         FilStandard = atoi (cfg_v);
		}
        if (RemoveKondArt)
		{
			     DelListField ("kond_art");
		}
        if (RemoveAGrund)
		{
			     DelListField ("a_grund");
		}
        if (RemoveAGrund0)
		{
			     DelListField ("a_grund0");
		}
        if (ProgCfg.GetCfgValue ("TestBranSmt", cfg_v) == TRUE)
		{
			         TestBranSmt = atoi (cfg_v);
		}
       if (ProgCfg.GetCfgValue ("akt_preis", cfg_v) == TRUE)
       {
                     akt_preis = atol (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("TxtByDefault", cfg_v) == TRUE)
       {
                     TxtByDefault = atol (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("RemoveMarge", cfg_v) == TRUE)
       {
                     RemoveMarge = atol (cfg_v);
	   }

       if (ProgCfg.GetCfgValue ("RemoveLdMarge", cfg_v) == TRUE)
	   {
			     DelLadVK0  = atoi (cfg_v);
				 Spalte_lad_pr0_Ok = FALSE; 
				 if (DelLadVK0 == 0) Spalte_lad_pr0_Ok = TRUE; 
	   }

       if (ProgCfg.GetCfgValue ("RemoveBsd", cfg_v) == TRUE)
       {
                     RemoveBsd = atol (cfg_v);
	   }
       if (ProgCfg.GetCfgValue ("ShowLqd", cfg_v) == TRUE)
       {
                     ShowLqd = atol (cfg_v);
	   }
	   strcpy (sys_par.sys_par_nam,"pos_txt_nr_par");
	   if (sys_par_class.dbreadfirst () == 0)
	   {
			PosTxtNrPar = atoi (sys_par.sys_par_wrt);
	   }
       if (ProgCfg.GetCfgValue ("letzte_lieferung_holen", cfg_v) == TRUE)
       {
		            LetztLiefHolen = atoi (cfg_v); //271010   
       }
       if (ProgCfg.GetCfgValue ("letzte_lieferung_anzahl_tage", cfg_v) == TRUE)
       {
		            LetztLiefAnzTage = atoi (cfg_v); //041212   
       }

        if (ProgCfg.GetCfgValue ("lad_vk", cfg_v) == TRUE)
        {
              lad_vk_edit = TRUE;
        }







		char buffer [512];
		FILE *fp;
	    char *etc;
		etc = getenv ("BWSETC");
		int anz = 0;


   sprintf (buffer, "%s\\%s.rct", etc,"51100_Spalten");
  // if (getenv ("testmode")) disp_mess (buffer,1);
   fp = fopen (buffer, "r");
   //if (getenv ("testmode")) disp_mess ("fopen",1);
   if (fp != NULL) 
   {
	while (fgets (buffer, 511, fp))
	{
	 //  if (getenv ("testmode")) disp_mess ("fgets",1);
		cr_weg (buffer);
		anz = wsplit (buffer, " ");
		if (anz < 3) continue;
		if (strcmp (wort[0], "AktAuf") == 0)
		{
			if (strcmp (wort[1], "Spalte_langbez_gewaehlt") == 0)	Spalte_langbez_gewaehlt = atoi (wort[2]);
			if (strcmp (wort[1], "Spalte_last_lieferung_gewaehlt") == 0)	Spalte_last_lieferung_gewaehlt = atoi (wort[2]); //??
			if (strcmp (wort[1], "Spalte_last_me_gewaehlt") == 0)	Spalte_last_me_gewaehlt = atoi (wort[2]);
			if (strcmp (wort[1], "Spalte_lieferdat_gewaehlt") == 0)	Spalte_lieferdat_gewaehlt = atoi (wort[2]);
			if (strcmp (wort[1], "Spalte_lpr_vk_gewaehlt") == 0)	Spalte_lpr_vk_gewaehlt = atoi (wort[2]);
			if (strcmp (wort[1], "Spalte_lad_vk_gewaehlt") == 0)	Spalte_lad_pr_gewaehlt = atoi (wort[2]);
			if (strcmp (wort[1], "Spalte_basis_me_bz_gewaehlt") == 0)	Spalte_basis_me_bz_gewaehlt = atoi (wort[2]);
			if (strcmp (wort[1], "Spalte_lad_pr_prc_gewaehlt") == 0)	Spalte_lad_pr_prc_gewaehlt = atoi (wort[2]);
		}
	}
   //if (getenv ("testmode")) disp_mess ("ende 51100_Spalten",1);
   }






   if (fp == NULL)
   {
        if (ProgCfg.GetCfgValue ("langeArtikelbezeichnung", cfg_v) == TRUE) //WAL-11
        {
					 if (atoi (cfg_v))
					 {
						AddLangBez = atoi (cfg_v);
						Spalte_langbez_Ok = FALSE; 

					 }
        }
        if (ProgCfg.GetCfgValue ("last_me", cfg_v) == TRUE)
        {
			         if (atoi (cfg_v))
					 {
						 DelLastMe = FALSE;
						 Spalte_last_me_Ok = TRUE; 
					 }
					 else
					 {
						 DelLastMe = TRUE;
						 Spalte_last_me_Ok = FALSE; 
					 }
        }
        if (ProgCfg.GetCfgValue ("last_ldat", cfg_v) == TRUE)
        {
			         AddLastLief = atoi (cfg_v);
					 Spalte_lieferdat_Ok = FALSE;
        }
        if (ProgCfg.GetCfgValue ("last_pr_vk", cfg_v) == TRUE)
        {
			         AddLastVkPr = atoi (cfg_v);
					 Spalte_lpr_vk_Ok = FALSE; 
        }
        if (ProgCfg.GetCfgValue ("lad_vk", cfg_v) == TRUE)
        {
					 Spalte_lad_pr_Ok = FALSE;
			         if (atoi (cfg_v))
					 {
						 DelLadVK = FALSE;
						 Spalte_lad_pr_Ok = TRUE;
					 }

                     if (atoi (cfg_v) == 2)
                     {
   		                  SetItemAttr (&dataform, "ld_pr", EDIT);
                     }

        }
       if (ProgCfg.GetCfgValue ("RemoveBasisMe", cfg_v) == TRUE)
       {
                     RemoveBasisMe = atol (cfg_v);
					 Spalte_basis_me_bz_Ok = FALSE; 
					 if (RemoveBasisMe == 0) Spalte_basis_me_bz_Ok = TRUE; 
	   }
       if (ProgCfg.GetCfgValue ("LdVkAsPercent", cfg_v) == TRUE)
       {
                     LdVkAsPercent = atol (cfg_v);
					 Spalte_lad_pr_prc_Ok = atoi (cfg_v); //FS-126
	   }

   }
   else
   {
	   fclose (fp);

		AddLangBez = Spalte_langbez_gewaehlt;
        if (Spalte_langbez_gewaehlt == TRUE) //WAL-11
        {
				Spalte_langbez_Ok = FALSE; 
        }
        if (Spalte_last_me_gewaehlt == TRUE) 
        {
				 DelLastMe = FALSE;
				 Spalte_last_me_Ok = TRUE; 
        }
		else
		{
				 DelLastMe = TRUE;
				 Spalte_last_me_Ok = FALSE; 
		}
         AddLastLief = Spalte_lieferdat_gewaehlt;
        if (Spalte_lieferdat_gewaehlt == TRUE) 
        {
					 Spalte_lieferdat_Ok = FALSE;
        }
         AddLastVkPr = Spalte_lpr_vk_gewaehlt;
        if (Spalte_lpr_vk_gewaehlt == TRUE) 
        {
					 Spalte_lpr_vk_Ok = FALSE;
        }
  	    Spalte_lad_pr_Ok = Spalte_lad_pr_gewaehlt;
        if (Spalte_lad_pr_gewaehlt == TRUE) 
		{
				 Spalte_lad_pr_Ok = TRUE;
		}
		else
		{
			DelLadVK = TRUE;
		}

        if (lad_vk_edit == TRUE && Spalte_lad_pr_Ok == TRUE)
        {
                SetItemAttr (&dataform, "ld_pr", EDIT);
        }
  	    Spalte_basis_me_bz_Ok = Spalte_basis_me_bz_gewaehlt;
        if (Spalte_basis_me_bz_gewaehlt == TRUE) 
		{
					 Spalte_basis_me_bz_Ok = FALSE; 
                     RemoveBasisMe = 0;
		}
		else
		{
                     RemoveBasisMe = 1;
		}

  	    Spalte_lad_pr_prc_Ok = Spalte_lad_pr_prc_gewaehlt;
        LdVkAsPercent = Spalte_lad_pr_prc_gewaehlt;

   }




}

static char paufschlval [37];


static int testaufschlag (void);
static int showtestaufschlag (void);


static char prabval [10];
static char pprovval [10];
static char ptext [20];

static ITEM iprab  ("rab_satz",  prabval,              "Rabatt......:", 0);
static ITEM ipaufschlag  ("aufschlag",  paufschlval,   "Aufschlag...:", 0);
static ITEM itext  ("",  ptext,   "", 0);
static ITEM ipprov ("prov_satz", pprovval,             "Provision...:", 0);

static field _prab1 [] = {
&iprab,       8,  0, 1,10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab1 = {3, 0, 0, _prab1, 0, 0, 0, 0, NULL};

static field _prab2 [] = {
&ipprov,      8,  0, 1,10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab2 = {3, 0, 0, _prab2, 0, 0, 0, 0, NULL};

static field _prab3 [] = {
&iprab,       8,  0, 1, 10, 0, "%5.2f", EDIT, 0, 0, 0,
&ipprov,      8,  0, 2, 10, 0, "%5.2f", EDIT, 0, 0, 0,
&iOK,        15, 0,  4,  4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  4, 21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static form prab3 = {4, 0, 0, _prab3, 0, 0, 0, 0, NULL};


char *aufschlagtab = NULL;

combofield prc4[] = {0, 37, 3, (char *) aufschlagtab};  
combofield prc5[] = {1, 37, 3, (char *) aufschlagtab};  
combofield prc6[] = {2, 37, 3, (char *) aufschlagtab};  

combofield *prc = NULL;

static field _prab4 [] = {
&ipaufschlag,  3,  0,  1, 4, 0, "%hd", EDIT, 0, testaufschlag, 0,
&ishow,        2,  0,  1, 21, 0, "", BUTTON, 0,showtestaufschlag ,KEY9,
&itext,        20,  0, 1, 24,0, "", DISPLAYONLY, 0,0 ,0,
&iOK,         15,  0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,         15,  0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

static field _prab41 [] = {
&ipaufschlag, 20, 10,  1, 4, 0, "", COMBOBOX | DROPLIST, 0, 0, 0,
&iOK,         15,  0,  3, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,         15,  0,  3,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

// static form prab4 = {5, 0, 0, _prab4, 0, 0, 0, 0, NULL};
static form prab4 = {3, 0, 0, _prab41, 0, 0, 0, prc4, NULL};

static field _prab5 [] = {
&iprab,      8,  0, 1, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipaufschlag,3,  0, 2, 4, 0, "%hd", EDIT, 0, testaufschlag, 0,
&ishow,        2,  0,  2, 21, 0, "", BUTTON, 0,showtestaufschlag ,KEY9,
&itext,        20,  0, 2, 24,0, "", DISPLAYONLY, 0,0 ,0,
&iOK,        15, 0,  4, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  4,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};



static field _prab51 [] = {
&iprab,       8,  0, 1, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipaufschlag,20, 10, 2, 4, 0, "", COMBOBOX | DROPLIST, 0, 0, 0,
&iOK,        15, 0,  4, 4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  4,21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

// static form prab5 = {6, 0, 0, _prab5, 0, 0, 0, 0, NULL};
static form prab5 = {4, 0, 0, _prab51, 0, 0, 0, prc5, NULL};

static field _prab6 [] = {
&iprab,       8,  0, 1, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipprov,      8,  0, 2, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipaufschlag, 3,  0, 3, 4, 0, "%hd", EDIT, 0, testaufschlag, 0,
&ishow,       2,  0, 3, 21, 0, "", BUTTON, 0,showtestaufschlag ,KEY9,
&itext,       20,  0,3, 24,0, "", DISPLAYONLY, 0,0 ,0,
&iOK,        15, 0,  5,  4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  5, 21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};


static field _prab61 [] = {
&iprab,       8,  0, 1, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipprov,      8,  0, 2, 4, 0, "%5.2f", EDIT, 0, 0, 0,
&ipaufschlag,20, 10, 3, 4, 0, "", COMBOBOX | DROPLIST, 0, 0, 0,
&iOK,        15, 0,  5,  4, 0, "", BUTTON, 0,EnterTest ,KEY12,
&iCA,        15, 0,  5, 21, 0, "", BUTTON, 0,EnterTest ,KEY5,
};

// static form prab6 = {7, 0, 0, _prab6, 0, 0, 0, 0, NULL};
static form prab6 = {5, 0, 0, _prab61, 0, 0, 0, prc6, NULL};

static form *prab;

static void schreibe_postxt (void)
{
	extern struct AUFP_TXT aufp_txt;
	static AUFP_TCLASS aufp_tclass;
	extern struct AUFPT auft;
	static AUFPT_CLASS aufpt_class;
       static int prep_ok = 0;
	   int dsqlstatus;
       int zeile;

	long aufptxt = atol (aufps.aufp_txt);


	if (aufptxt == 0l)
	{
		aufptxt = AUFPLIST::GenAufpTxt ();
		sprintf (aufps.aufp_txt, "%ld", aufptxt);
		memcpy (&aufptab[eListe.GetAktRow()],
                   &aufps, sizeof (struct AUFPS));
	}
   if (wa_pos_txt)
   {
		aufp_txt.mdn = aufk.mdn;
		aufp_txt.fil = aufk.fil;
		aufp_txt.auf = aufk.auf;
		aufp_txt.posi = aufptxt;
       DbClass.sqlin ((short *) &aufp_txt.mdn, 1, 0);
       DbClass.sqlin ((short *) &aufp_txt.fil, 1, 0);
       DbClass.sqlin ((long *) &aufp_txt.auf, 2, 0);
       DbClass.sqlin ((long *) &aufp_txt.posi, 2, 0);
       DbClass.sqlout ((long *) &aufp_txt.zei, 2, 0);
       if (DbClass.sqlcomm ("select max (zei) from aufp_txt where mdn = ? and fil = ? and auf = ? and posi = ?") != 0)
       {
                    aufp_txt.zei = 0;
       }
       else if (DbClass.IsLongnull (aufp_txt.zei))
       {
                    aufp_txt.zei = 0;
       }
	   else
	   {

		   aufp_txt.zei += 1;
	   }
	   strcpy (aufp_txt.txt,ptabn.ptbez);
 	   aufp_tclass.dbupdate();
	   if (aufp_txt.zei > 0)
	   {
		   AUFPLIST::Texte ();
	   }
   }
   else
   {

               if (prep_ok == 0)
			   {
                    aufpt.nr = 0;
                    aufpt_class.dbreadfirst ();
                    prep_ok = 1;
			   }
		       aufpt.nr = aufptxt;
               zeile = 0;
			   aufpt.zei = 0;
               dsqlstatus = aufpt_class.dbreadfirst ();
               while (dsqlstatus == 0)
			   {
                     dsqlstatus = aufpt_class.dbread ();
                     zeile += 1;
                     aufpt.zei = zeile;
			   }
			   strcpy (aufpt.txt,ptabn.ptbez);
               aufpt_class.dbupdate ();
			   if (aufpt.zei > 0)
			   {
					AUFPLIST::Texte ();
			   }
   }

}
static void rechne_preis_aufschlag (void)
{
	double vk_pr = 0.0;
	double aufschl = 0.0;
	double aufschl_euro = 0.0;
	double aufschl_proz = 0.0;
	if (ratod(aufps.a) < 1.0) return;
	vk_pr = ratod(aufps.auf_vk_pr);
	aufschl = ratod(aufps.aufschlag_wert);
	if (vk_pr == 0.0) return;
	vk_pr = vk_pr - aufschl;
	aufschl_euro = ratod(ptabn.ptwer1);
	aufschl_proz = ratod(ptabn.ptwer2);
	aufschl = 0.0;
	if (aufschl_proz != 0.0)
	{
		aufschl = vk_pr * aufschl_proz / 100;
		vk_pr = vk_pr + aufschl;
	}
	if (aufschl_euro != 0.0)
	{
		aufschl = aufschl + aufschl_euro;
		vk_pr = vk_pr + aufschl_euro;
	}
	sprintf(aufps.auf_vk_pr,"%.4lf",vk_pr);
	sprintf(aufps.aufschlag_wert,"%.4lf",aufschl);
}

static int testaufschlag (void)
{


        if (ptab_class.lese_ptab ("aufschlag", paufschlval) != 0 && atoi(paufschlval) != 0)
		{
			    wiedereinstieg = 1;
	            no_break_enter ();
				return 0;
		}
		strcpy (ptext, ptabn.ptbez);

        if (syskey == KEY5 || syskey == KEY12)
        {
            break_enter ();
			if (syskey == KEY12)
			{
				if (atoi(paufschlval) != atoi(aufps.aufschlag))
				{
					schreibe_postxt ();
					rechne_preis_aufschlag ();
					strcpy(aufps.aufschlag,paufschlval);
					memcpy (&aufptab[eListe.GetAktRow()],
	                   &aufps, sizeof (struct AUFPS));
					eListe.ShowAktRow ();
			}
			}
            return 0;
        }
        if (syskey == KEY9)
        {
		    int dsqlstatus = ptab_class.Show ("aufschlag");
			if (syskey == KEY5)
			{
				wiedereinstieg = 0;
	            break_enter ();
		        return 0;
			}
			strcpy (paufschlval, ptabn.ptwert);
			ptab_class.lese_ptab ("aufschlag", paufschlval);
			strcpy (ptext, ptabn.ptbez);
			if (atoi(paufschlval) != atoi(aufps.aufschlag))
			{
				schreibe_postxt ();
				rechne_preis_aufschlag ();
				strcpy(aufps.aufschlag,paufschlval);
				memcpy (&aufptab[eListe.GetAktRow()],
	                   &aufps, sizeof (struct AUFPS));
				eListe.ShowAktRow ();
			}
			wiedereinstieg = 1;
            break_enter ();
	        return 0;

		}

		if (atoi(paufschlval) != atoi(aufps.aufschlag))
		{
			schreibe_postxt ();
			rechne_preis_aufschlag ();
			strcpy(aufps.aufschlag,paufschlval);
			memcpy (&aufptab[eListe.GetAktRow()],
	                   &aufps, sizeof (struct AUFPS));
			eListe.ShowAktRow ();
		}
        return 1;
}


static void TestAufschlag (void)
{

        if (syskey != KEY12 && syskey != KEYCR) return;
		
        if (ptab_class.lese_ptab ("aufschlag", paufschlval) != 0 && atoi(paufschlval) != 0)
		{
//			    wiedereinstieg = 1;
	            no_break_enter ();
				return;
		}
//		strcpy (ptext, ptabn.ptbez);

	    if (atoi(paufschlval) != atoi(aufps.aufschlag))
		{
				schreibe_postxt ();
				rechne_preis_aufschlag ();
				strcpy(aufps.aufschlag,paufschlval);
				memcpy (&aufptab[eListe.GetAktRow()],
	                   &aufps, sizeof (struct AUFPS));
				eListe.ShowAktRow ();
		}
}


static int showtestaufschlag (void)
{
	syskey = KEY9;
	testaufschlag ();
	return (0);
}

void AUFPLIST::ChoiseLines (HWND eWindow, HDC hdc)
{
         static HPEN hPenG = NULL;
         static HPEN hPenW = NULL;
         TEXTMETRIC tm;
         RECT rect;
         int x, y;
         int cx, cy;

 		 if (eWindow == NULL) return;

		 memcpy (&tm, &textm, sizeof (tm));
         GetClientRect (eWindow, &rect);
         x = tm.tmAveCharWidth / 2;
         y = tm.tmHeight / 2;

         cx = rect.right - tm.tmAveCharWidth / 2;
         cy = rect.bottom - tm.tmHeight / 2;
         if (hPenG == NULL)
         {
                   hPenG = CreatePen (PS_SOLID, 0, GRAYCOL);
                   hPenW = CreatePen (PS_SOLID, 0, WHITECOL);
         }
         SelectObject (hdc, hPenG);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);

         SelectObject (hdc, hPenW);


         y ++;
         x ++;
         cx --;
         cy --;
         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, cx, y);

         MoveToEx (hdc, x, y, NULL);
         LineTo (hdc, x, cy);

         cx += 2;
         cy += 2;
         MoveToEx (hdc, cx, y, NULL);
         LineTo (hdc, cx, cy);

         MoveToEx (hdc, x, cy, NULL);
         LineTo (hdc, cx, cy);
}


HWND AUFPLIST::CreateEnter (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        HDC hdc;

        if (hMainWin == NULL) return NULL;    
           
        if (eWindow) return eWindow;

//        eListe.GetTextMetric (&tm);
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);
       
        y = (wrect.bottom - 16 * tm.tmHeight);
        cx = 40 * tm.tmAveCharWidth;
        x = wrect.left + 2 + (rect.right - cx) / 2;
		if (rab_prov_kz == 3)
		{
                  cy = 9 * tm.tmHeight;
		}
		else if (rab_prov_kz == 4)
		{
                  cy = 7 * tm.tmHeight;
			      cx = 50 * tm.tmAveCharWidth;
		}
		else if (rab_prov_kz == 5)
		{
                  cy = 9 * tm.tmHeight;
			      cx = 50 * tm.tmAveCharWidth;
		}
		else if (rab_prov_kz == 6)
		{
                  cy = 10 * tm.tmHeight;
			      cx = 50 * tm.tmAveCharWidth;
		}
		else
		{
                  cy = 7 * tm.tmHeight;
		}

        eWindow       = CreateWindow (
                                       "ListMain",
//                                       "hListWindow", 
                                       "",
                                       WS_DLGFRAME | 
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);
        hdc = GetDC (eWindow);
        ChoiseLines (eWindow, hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}


       
static void DisableWindows (HWND hWnd)
/**
Hauptfenster der Anwendung holen.
**/
{

         while (TRUE)
         {
                 EnableWindow (hWnd, FALSE);     
                 hWnd = GetParent (hWnd);
                 if (hWnd == NULL) break;
         }
}

static void EnableWindows (HWND hWnd)
/**
Hauptfenster der Anwendung holen.
**/
{

         while (TRUE)
         {
                 EnableWindow (hWnd, TRUE);     
                 hWnd = GetParent (hWnd);
                 if (hWnd == NULL) break;
         }
}

void AUFPLIST::ReadPtabAufschlag ()
{
	if (aufschlagtab != NULL) return;

	int i = 0;
	int dsqlstatus = ptab_class.lese_ptab_all ("aufschlag");
	while (dsqlstatus == 0)
	{
		i ++;
		dsqlstatus = ptab_class.lese_ptab_all ();
	}

	prc[0].cbanz = i;
	aufschlagtab = new char [(i + 3) * prc[0].cbdim];
	if (aufschlagtab == NULL) return;

	memset (aufschlagtab, 0, (i + 3) * prc[0].cbdim);
	prc[0].cbwerte = aufschlagtab;

	char *pos = aufschlagtab;
	dsqlstatus = ptab_class.lese_ptab_all ("aufschlag");
	while (dsqlstatus == NULL)
	{
		Text t;
		t.Format ("%d %s", atoi (ptabn.ptwert), ptabn.ptbez);
		strncpy (pos, t.GetBuffer (), prc[0].cbdim - 1);
		pos += prc[0].cbdim;
		dsqlstatus = ptab_class.lese_ptab_all ();
	}
}

long AUFPLIST::EnterPosRab (void)
/**
Nummer Eingeben.
**/
{
          int currentf;

		  save_fkt (5);
		  save_fkt (6);
		  save_fkt (7);
		  save_fkt (8);
		  save_fkt (9);
		  save_fkt (10);
		  save_fkt (11);
		  save_fkt (12);
		  set_fkt (NULL, 9);
		  SetFkt (9, leer, 0);
          set_fkt (EnterBreak, 5);
		  CreateEnter ();
          currentf = currentfield;
		  switch (rab_prov_kz)
		  {
		        case 1 :
			        prab = &prab1;
					break;
		        case 2 :
			        prab = &prab2;
					break;
		        case 3 :
			        prab = &prab3;
					break;
		        case 4 :
			        prab = &prab4;
					prc = prc4;
					break;
		        case 5 :
			        prab = &prab5;
					prc = prc5;
					break;
		        case 6 :
			        prab = &prab6;
					prc = prc6;
					break;
		  }
          sprintf (paufschlval , "%hd", (short) atoi (aufps.aufschlag));          
          sprintf (prabval , "%.2lf", (double) ratod (aufps.rab_satz));          
          sprintf (pprovval, "%.2lf", (double) ratod (aufps.prov_satz));          
          if (ptab_class.lese_ptab ("aufschlag", paufschlval) == 0)
		  {
  			strcpy (ptext, ptabn.ptbez);
		  }
		  else
		  {
  			strcpy (ptext, " ");
		  }

          break_end ();
/*
		  EnableWindows (hMainWin, FALSE);
		  EnableWindow (eListe.Getmamain2 (), FALSE);
*/
		  DisableWindows (eListe.Getmamain3 ());

		  if (rab_prov_kz > 3)
		  {
			ReadPtabAufschlag ();
			strcpy (ipaufschlag.feld, aufschlagtab);
			ComboBreak = FALSE;
		  }

          enter_form (eWindow, prab, 0, 0);
		  if (rab_prov_kz > 3)
		  {
			ComboBreak = TRUE;
			Token t;
			t.SetSep (" ");
			t = ipaufschlag.feld;
			if (t.GetAnzToken () > 0)
			{
				  strcpy (paufschlval, t.GetToken (0));
				TestAufschlag ();
			}
		  }

		  EnableWindows (eListe.Getmamain3 ());
		  EnableWindow (eListe.Getmamain2 (), TRUE);
		  EnableWindows (hMainWin, TRUE);
		  DestroyWindow (eWindow);
		  eWindow = NULL;
          no_break_end ();
          currentfield = currentf;
          eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
		  if (syskey != KEY5)
		  {
                   sprintf (aufps.rab_satz, "%.2lf", (double) ratod (prabval));          
                   sprintf (aufps.aufschlag, "%hd", (short) atoi (paufschlval));          
                   sprintf (aufps.prov_satz, "%.2lf", (double) ratod (pprovval));          
                   memcpy (&aufptab[eListe.GetAktRow ()], &aufps, sizeof (struct AUFPS));
		  }
		  restore_fkt (5);
		  restore_fkt (6);
		  restore_fkt (7);
		  restore_fkt (8);
		  restore_fkt (9);
		  restore_fkt (10);
		  restore_fkt (11);
		  restore_fkt (12);
		  syskey = 0;
          return 0l;
}
      
void AUFPLIST::SetNewRow (char *item, int row)
{
        int lenu;
        int len;
        int diff;
        int i;
		int datapos;
		int ipos;


        ipos = GetItemPos (&ubform, item);
		if (ipos <= 0) return;

		for (i = ipos; i < dataform.fieldanz; i ++)
		{
		    datapos = dataform.mask[i].pos[1];
			if (datapos >= ubform.mask[ipos].pos[1]) break;
		}

        diff = row - ubform.mask[ipos].pos[1]; 

		if (i == dataform.fieldanz)
		{
	        datapos = dataform.mask[i].pos[1];
            len  = dataform.mask[i].length + diff;
		}
		else
		{
	        datapos = dataform.mask[i - 1].pos[1];
            len  = dataform.mask[i- 1].length + diff;
		}

        lenu = ubform.mask[ipos - 1].length + diff;

        if (ipos < ubform.fieldanz - 1)
        {
             ubform.mask[ipos].pos[1] = row;
             lineform.mask[ipos - 1].pos[1] = row;

             for ( i = ipos + 1; i < ubform.fieldanz; i ++)
             {
                  ubform.mask[i].pos[1]   += diff;
                  lineform.mask[i - 1].pos[1] += diff;
             }
        }

		for (i = 0; i < dataform.fieldanz; i ++)
		{

			   if (dataform.mask[i].pos[1] == datapos)
			   {
                         dataform.mask[i].length = len;
			   }

			   if (dataform.mask[i].pos[1] > datapos)
			   {
                  dataform.mask[i].pos[1] += diff;
			   }
		}
        ubform.mask[ipos - 1].length = lenu;
}

void AUFPLIST::SetNewPicture (char *item, char *picture)
{
		int ipos;
		char *pic;
        ipos = GetItemPos (&dataform, item);
        if (ipos == -1) return;

		pic = new char (strlen (picture + 2));
		if (pic == NULL) return;

		dataform.mask[ipos].picture = pic;
}


void AUFPLIST::SetNewLen (char *item, int len)
{
        int lenu;
        int diff;
        int i;
		int datapos;
		int ipos;


		if (len == 0) return;
        ipos = GetItemPos (&ubform, item);
        if (ipos == -1) return;

        diff = len - ubform.mask[ipos].length + 2; 
		for (i = ipos; i < dataform.fieldanz; i ++)
		{
		    datapos = dataform.mask[i].pos[1];
			if (datapos >= ubform.mask[ipos].pos[1]) break;
		}

		if (i < dataform.fieldanz)
		{
                 datapos = dataform.mask[i].pos[1];
		}

        lenu = ubform.mask[ipos].length + diff;

        if (ipos < ubform.fieldanz - 1)
        {
             for ( i = ipos + 1; i < ubform.fieldanz; i ++)
             {
                  ubform.mask[i].pos[1]   += diff;
                  lineform.mask[i - 1].pos[1] += diff;
             }
        }

		for (i = 0; i < dataform.fieldanz; i ++)
		{

			   if (dataform.mask[i].pos[1] == datapos)
			   {
                         dataform.mask[i].length = len;
			   }

			   if (dataform.mask[i].pos[1] > datapos)
			   {
                  dataform.mask[i].pos[1] += diff;
			   }
		}
        ubform.mask[ipos].length = lenu;
}





void AUFPLIST::EnterAufp (short mdn, short fil, long auf,
                          short kunfil, long kun, char *ldat)
/**
Auftragsliste bearbeiten.
**/

{
       static int initaufp = 0;
	   int pos;
//	   form *savecurrent;

//	   savecurrent = current_form;


	   if (F2Userdef1)
	   {
			set_fkt (Setuserdef1, 2); //WAL-70
            SetFkt (2, userdef1, KEY2); //WAL-70
	   }
	   else
	   {
			set_fkt (NULL, 2); //WAL-70
            SetFkt (2, leer, 0); //WAL-70
	   }
       SetFkt (3, leer, 0);
       SetFkt (4, info, KEY4);
//	   set_fkt (ItemFunc, 4); //WAL-70
       SetFkt (5, zurueck, KEY5);

	   eListe.DropMessage (StndHandler);
	   eListe.AddMessage (StndHandler);
	   eListe.DropMessage (StndFailed);
	   eListe.AddMessage (StndFailed);
	   eListe.DropMessage (StndReload);
	   eListe.AddMessage (StndReload);
	   LISTHANDLER->SetLoadComplete (FALSE);
//	   eListe.RowEvent = this;
       if ((DelPrVK == FALSE) && Muster)
       {
 	         SetItemAttr (&dataform, "pr_vk", DISPLAYONLY);
       }
       else
       {
 	         SetItemAttr (&dataform, "pr_vk", VkAttribut);
       }

       InsCount = 0;
       FillWaehrungBz ();
       strcpy (sys_par.sys_par_nam,"vertr_abr_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
                 vertr_abr_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam,"lutz_mdn_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
                 lutz_mdn_par = atoi (sys_par.sys_par_wrt);
       }

       strcpy (sys_par.sys_par_nam,"lsc2_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
            lsc2_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam,"lsc3_par");
       if (sys_par_class.dbreadfirst () == 0)
       {
            lsc3_par = atoi (sys_par.sys_par_wrt);
       }
       strcpy (sys_par.sys_par_nam,"wa_pos_txt");
       if (sys_par_class.dbreadfirst () == 0)
	   {
                 wa_pos_txt = atoi (sys_par.sys_par_wrt); 
	   }
	   if (aufk.waehrung == 0)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK);
				 }
	             pos = GetItemPos (&ubform, "ld_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD);
				 }
	   }
	   else if (aufk.waehrung == 1)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_DM);
				 }
	             pos = GetItemPos (&ubform, "ld_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_DM);
				 }
	   }
	   else if (aufk.waehrung == 2)
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_EURO);
				 }
	             pos = GetItemPos (&ubform, "ld_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_EURO);
				 }
	   }
	   else
	   {
	             pos = GetItemPos (&ubform, "pr_vk");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (VK_FREMD);
				 }
	             pos = GetItemPos (&ubform, "ld_pr");
				 if (pos > -1)
				 {
				        ubform.mask[pos].item->SetFeldPtr (LD_FREMD);
				 }
	   }


       Geta_bz2_par ();
       Geta_kum_par ();
       Getauf_me_pr0 ();
//	   ColBkNames.DestroyElements ();
	   GetCfgValues ();
	   if (LdVkAsPercent)
	   {
		    DelLadVKPrc = FALSE;
	   }
   	   add = FALSE;
	   aufme_old = (double) 0.0;
	   DestroyPlus ();
	   if (FormOK == FALSE)
	   {
           if (DelLadVK)
		   {
		   }
           if (DelLastMe)
		   {
			     DelListField ("last_me");
		   }
		   FormOK = TRUE;
	   }

	   akt_me = (double) 0.0;
	   strcpy (aufps.auf_me, "0");
	   strcpy (aufptab[0].auf_me, "0");
       aufk.fil = fil;
       aufk.auf = auf;
       aufk.kun = kun;
       aufk.kun_fil = kunfil;
       aufk.lieferdat = dasc_to_long (ldat);

	   set_fkt (NULL, 8);
	   SetFkt (8, leer, NULL);

       set_fkt (dokey5, 5);
       set_fkt (AppendLine, 6);
       set_fkt (DeleteLine, 7);

       if (lutz_mdn_par) 
	   {
		   set_fkt (ProdMdn, 8);
	   }
       else if (rab_prov_kz) 
	   {
		   set_fkt (PosRab, 8);
	   }
	   if (lutz_mdn_par)
	   {
	  	   SetFkt (8, prodmdn, KEY8);
	   }
       else if (rab_prov_kz == 4)
	   {
		   SetFkt (8, aufschlag, KEY8);
	   }
       else if (rab_prov_kz)
	   {
		   SetFkt (8, posrab, KEY8);
	   }

       set_fkt (WriteAllPos, 12);
       set_fkt (Schirm, 11);

       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);
       SetFkt (11, vollbild, KEY11);
       AktRow = 0;
       AktColumn = 0;
       if (initaufp == 0)
       {
                 eListe.SetInfoProc (InfoProc);
                 eListe.SetTestAppend (TestAppend);
                 sprintf (InfoCaption, "Auftrag %ld", auf);
                 mamain1 = CreateMainWindow ();
                 eListe.InitListWindow (mamain1);
                 ReadDB (mdn, fil, auf);
                 initaufp = 1;
       }

       eListe.SetListFocus (ListFocus);
       eListe.Initscrollpos ();
       scrollpos = 0;
       SetAktivWindow (eListe.Getmamain2 ());
	   ShowDB (mdn, fil, auf);
       if (FlgInEnterListe == FALSE) //FS-126
	   {
		   ShowDB (mdn, fil, auf);
	   }
	   else
	   {
		   doAktAuf ();
//		   doHWG (Gethwg(),Getwg(),GetReiterIdx());
		//	RClientOrder.Load ();

	   }


       eListe.SetRowItem ("a", aufptab[0].a);
       SetRowItem ();

	   AnzAufWert ();
	   AnzAufGew ();
       ListAktiv = 1;
	   InvalidateRect (eListe.Getmamain2 (), NULL, FALSE);
	   UpdateWindow (eListe.Getmamain2 ());
// Testversion
//	   SendMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_F12, 0l);
// Ende TestVersion
        if (Posanz == 0 && StndDirect > 0)
		{
			     PostMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_F10, 0l);
		}
		else if (StndDirect == 3)
		{
			
			     PostMessage (eListe.Getmamain3 (), WM_STND_RELOAD, 0, 0l);
		}
		//030611 A
	    if (PosSave > 0) 
		{
			//Hier wird jetzt neu durchnummeriert, wg. Fehler -239
			_WriteAllPos (); 
		    eListe.SetFeldFocus0 (eListe.GetAktRow (), eListe.GetAktColumn ()); 
		}
		//030611 E

        eListe.ProcessMessages ();

	    Ampel.SetState (CAmpel::No);
		Ampel.Invalidate ();
        InitMax ();
        InitMin ();
        MoveMamain1 ();
        ListAktiv = 0;
        SetFkt (6, leer, NULL);
        SetFkt (7, leer, NULL);
        SetFkt (8, leer, NULL);
        SetFkt (10, leer, NULL);
        SetFkt (11, leer, NULL);
        set_fkt (NULL, 11);
	    CloseAufw ();
	    CloseAufGew ();
		if (bsd_kz && ShowBsd != 0) BsdInfo.Destroy ();
	    DestroySa ();
        eListe.DestroyListWindow ();
        DestroyMainWindow ();
        initaufp = 0;
//	    current_form = savecurrent;
        return;
}

void AUFPLIST::RollbackBsd ()
{
    akt_me = (double) 0.0;
    int recs = eListe.GetRecanz ();
    for (int i = 0; i < recs; i ++)
    {
        memcpy (&aufps, &aufptab[i], sizeof (struct AUFPS));
		BucheBsd (ratod (aufps.a), ratod (aufps.auf_me) * -1, atoi (aufps.me_einh_kun),
			      ratod (aufps.auf_vk_pr));
    }
}


void AUFPLIST::WorkAufp ()
/**
Auftragsliste bearbeiten.
**/

{

//	   eListe.RowEvent = this;
       beginwork ();

	   set_fkt (NULL, 8);
	   SetFkt (8, leer, NULL);

       set_fkt (dokey5, 5);
       set_fkt (InsertLine, 6);
       set_fkt (DeleteLine, 7);

	   if (lutz_mdn_par) 
	   {
		set_fkt (ProdMdn, 8);
	   }
       else if (rab_prov_kz) 
	   {
		set_fkt (PosRab, 8);
	   }
	   if (lutz_mdn_par)
	   {
	  	   SetFkt (8, prodmdn, KEY8);
	   }
       else if (rab_prov_kz)
	   {
		   SetFkt (8, posrab, KEY8);
	   }

       set_fkt (WriteAllPos, 12);
       SetFkt (6, einfuegen, KEY6);
       SetFkt (7, loeschen, KEY7);


       eListe.SetDataForm0 (&dataform, &lineform);
       eListe.SetChAttr (ChAttr); 
       eListe.SetUbRows (ubrows); 
       eListe.SetListFocus (ListFocus);
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
       AktRow = 0;
       AktColumn = 0;
       scrollpos = 0;

       SetAktivWindow (eListe.Getmamain2 ());

       ListAktiv = 1;
	   AnzAufWert ();
	   AnzAufGew ();
       eListe.ProcessMessages ();

       commitwork ();
       if (syskey == KEY5)
       {
                  eListe.Initscrollpos ();
                  ShowDB (aufk.mdn, aufk.fil, aufk.auf);
       }
       ListAktiv = 0;
       SetFkt (6, leer, NULL);
       SetFkt (7, leer, NULL);
       SetFkt (8, leer, NULL);
       SetFkt (11, leer, NULL);
       set_fkt (NULL, 11);
       return;
}


void AUFPLIST::DestroyMainWindow (void)
/**
Hauptfenster fuer Liste loeschen.
**/
{
        if (mamain1 == NULL) return;
        DestroyWindow (mamain1);
        mamain1 = NULL;
}


HWND AUFPLIST::CreateMainWindow (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWindow == NULL) return NULL;    
           
        if (mamain1) return mamain1;

        eListe.GetTextMetric (&tm);

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);


		if (liney > 0)
		{
			y = liney;
		}
		else
		{
            y = (wrect.bottom - 12 * tm.tmHeight);
		}
        x = wrect.left + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;

        mamain1 = CreateWindow ("ListMain",
                                    InfoCaption,
                                    WS_THICKFRAME | 
//                                    WS_CAPTION | 
//                                    WS_CHILD |
                                    WS_POPUP |
                                    WS_SYSMENU |
                                    WS_MINIMIZEBOX |
                                    WS_MAXIMIZEBOX,
                                    x, y,
                                    cx, cy,
                                    hMainWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (mamain1, SW_SHOWNORMAL);
        UpdateWindow (mamain1);
        return mamain1;
}

static int IsMax = 0;
static int IsMin = 0;

void AUFPLIST::SetMax0 (int val)
{
    IsMax = val;
    if (IsMax)
    {
             SetFkt (11, fenster, KEY11);
    }
    else
    {
             SetFkt (11, vollbild, KEY11);
    }
}

void AUFPLIST::SetMin0 (int val)
{
    IsMin = val;
}


int AUFPLIST::Schirm (void)
{
         if (IsMin) return 0;
         
         if (IsMax)
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_RESTORE, 0l);
             IsMax = 0;
         }
         else
         {
             SendMessage (mamain1, WM_SYSCOMMAND, SC_MAXIMIZE, 0l);
             IsMax = 1;
         }
         return 0;
}


void AUFPLIST::MoveMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;
        TEXTMETRIC tm;

        if (mamainmin) return;
        eListe.GetTextMetric (&tm);
        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        if (mamainmax)
        {
                 x = wrect.left + 2;
                 y = wrect.top + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        else
        {
 		         if (liney)
				 {
			            y = liney;
				 }
				 else
				 {
                        y = (wrect.bottom - 12 * tm.tmHeight);
				 }
                 x = wrect.left + 2;
                 cy = wrect.bottom - y - 2;
                 cx = rect.right;
        }
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}


void AUFPLIST::MaximizeMamain1 ()
{
        RECT rect;
        RECT wrect;
        int x, y, cx, cy;

        GetClientRect (hMainWindow, &rect);
        GetWindowRect (hMainWindow, &wrect);
        x = wrect.left + 2;
        y = wrect.top + 2;
        cy = wrect.bottom - y - 2;
        cx = rect.right;
        MoveWindow (mamain1, x,y, cx, cy, TRUE);
}



static char aufwert [20];
static ITEM iaufwert  ("auf_wert",  aufwert,   "Auftragswert in � ", 0);

static field _faufwert [] = {
&iaufwert,        13,  0, 1, 1, 0, "%10.2f", READONLY, 0, 0, 0,
};

static form faufwert = {1, 0, 0, _faufwert, 0, 0, 0, 0, NULL};    

static char aufgew [20];
static char auflqd [20];
static char aufanteil [20];
static char *lKG = "kg";
static char *lEuro = "�";
static ITEM iaufgrw  ("auf_gew",        aufgew,    "Auftragsgew. in kg", 0);
static ITEM iauflqd  ("auf_lqd",        auflqd,    "Liquidit�t  in �  ", 0);
static ITEM iaufanteil("auf_anteil",    aufanteil, "Anteil SMT in %   ", 0);

static field _faufgew [] = {
&iaufgrw,        13,  0, 1, 1, 0, "%11.3f", READONLY, 0, 0, 0,
};

static form faufgew = {1, 0, 0, _faufgew, 0, 0, 0, 0, NULL};    

static field _faufgewex [] = {
&iaufgrw,        13,  0, 1, 1, 0, "%11.3f", READONLY, 0, 0, 0,
&iauflqd,        13,  0, 2, 1, 0, "%11.2f", READONLY, 0, 0, 0,
&iaufanteil,     13,  0, 3, 1, 0, "%11.2f", READONLY, 0, 0, 0,
};

static form faufgewex = {3, 0, 0, _faufgewex, 0, 0, 0, 0, NULL};    

void AUFPLIST::MoveAufw (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;    
        if (AufMehWnd == NULL) return;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight + 8;
		MoveWindow (AufMehWnd, x, y, cx, cy, TRUE);
}

void AUFPLIST::MoveAufGew (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        RECT merect;
        int x,y,cx, cy;
        TEXTMETRIC tm;

        if (hMainWin == NULL) return;    
        if (AufGewhWnd == NULL) return;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  2);
        if (AufMehWnd)
        {
             GetWindowRect (AufMehWnd, &merect);
             y = merect.bottom;
        }

        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
		if (ShowLqd && StndDirect == 3)
		{
			cy = 6 * tm.tmHeight + 8;
		}
		else if (ShowLqd)
		{
            faufgewex.fieldanz = 2;
			cy = 5 * tm.tmHeight + 8;
		}
		else
		{
			cy = 4 * tm.tmHeight + 8;
		}

		MoveWindow (AufGewhWnd, x, y, cx, cy, TRUE);
}


HWND AUFPLIST::CreateAufw (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        HDC hdc;
		HWND eWindow;

        if (hMainWin == NULL) return NULL;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);

/*		
        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight;
*/

        y = (wrect.top +  2);
        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
        cy = 4 * tm.tmHeight + 8;

        eWindow       = CreateWindow (
//                                       "StaticWhite",
                                       "StaticGray",
                                       "",
                                       WS_DLGFRAME | 
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);

        AufMehWnd0      = CreateWindowEx (
			                           WS_EX_CLIENTEDGE, 
                                       "StaticWhite",
                                       "",
									   WS_CHILD | WS_VISIBLE,
                                       2, 4,
                                       cx - 10, cy - 14,
                                       eWindow,
                                       NULL,
                                       hMainInst,
                                       NULL);

        hdc = GetDC (eWindow);
//        ChoiseLines (eWindow, hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}

HWND AUFPLIST::CreateAufGew (void)
/**
Hauptfenster fuer Liste erzeugen.
**/
{
        RECT rect;
        RECT wrect;
        RECT merect;
        int x,y,cx, cy;
        TEXTMETRIC tm;
        HDC hdc;
		HWND eWindow;

        if (hMainWin == NULL) return NULL;    
           
	    memcpy (&tm, &textm, sizeof (tm)); 

        GetClientRect (hMainWin, &rect);
        GetWindowRect (hMainWin, &wrect);


        y = (wrect.top +  2);
        if (AufMehWnd)
        {
             GetWindowRect (AufMehWnd, &merect);
             y = merect.bottom;
        }
        cx = 34 * tm.tmAveCharWidth + 4;
        x = wrect.left + rect.right - cx + 1;
		if (ShowLqd && StndDirect == 3)
		{
			cy = 6 * tm.tmHeight + 8;
		}
		else if (ShowLqd)
		{
            faufgewex.fieldanz = 2;
			cy = 5 * tm.tmHeight + 8;
		}
		else
		{
			cy = 4 * tm.tmHeight + 8;
		}

        eWindow       = CreateWindow (
//                                       "StaticWhite",
                                       "StaticGray",
                                       "",
                                       WS_DLGFRAME | 
                                       WS_POPUP,
                                       x, y,
                                       cx, cy,
                                       hMainWin,
                                       NULL,
                                       hMainInst,
                                    NULL);
        ShowWindow (eWindow, SW_SHOWNORMAL);
        UpdateWindow (eWindow);

        AufGewhWnd0      = CreateWindowEx (
			                           WS_EX_CLIENTEDGE, 
                                       "StaticWhite",
                                       "",
									   WS_CHILD | WS_VISIBLE,
                                       2, 4,
                                       cx - 10, cy - 14,
                                       eWindow,
                                       NULL,
                                       hMainInst,
                                       NULL);

        hdc = GetDC (eWindow);
//        ChoiseLines (eWindow, hdc);
        ReleaseDC (eWindow, hdc);
        return eWindow;
}


void AUFPLIST::CloseAufw (void)
{
        if (auf_wert_anz == 0) return;

        if (AufMehWnd)
		{
			CloseControls (&faufwert);
            if (AufMehWnd0)
			{
			         DestroyWindow (AufMehWnd0);
 			         AufMehWnd0 = NULL;
			}
			DestroyWindow (AufMehWnd);
			AufMehWnd = NULL;
		}
}
			
void AUFPLIST::CloseAufGew (void)
{
        if (auf_gew_anz == 0) return;

        if (AufGewhWnd)
		{
			CloseControls (&faufgew);
			CloseControls (&faufgewex);
            if (AufGewhWnd0)
			{
			         DestroyWindow (AufGewhWnd0);
 			         AufGewhWnd0 = NULL;
			}
			DestroyWindow (AufGewhWnd);
			AufGewhWnd = NULL;
		}
}
			

void AUFPLIST::AnzAufWert (void)
/**
Auftragswert anzeigen.
**/
{
          int currentf;
		  int i;
		  double wert;
		  int recanz;

      
		  if (bsd_kz && ShowBsd == 1) BsdInfo.Create ();
          if (auf_wert_anz == 0) return;

		  recanz = eListe.GetRecanz ();
		  if (recanz == 0)
		  {
			  if (AufMehWnd) 
			  {
				  DestroyWindow (AufMehWnd);
				  AufMehWnd = NULL;
			  }
			  return;
		  }
          if (AufMehWnd == NULL)
		  {
		            AufMehWnd = CreateAufw ();
					if (AufMehWnd == NULL) return;
		  }
		  wert = 0;

		  for (i = 0; i < recanz; i ++)  
		  {
			      wert = wert + ratod (aufptab[i].lief_me) *
					            ratod (aufptab[i].auf_vk_pr);
          }


		  SetStaticWhite (TRUE);
		  sprintf (aufwert, "%10.2lf", wert);
		  display_form (AufMehWnd0, &faufwert, 0, 0); 
		  SetStaticWhite (FALSE);

          currentfield = currentf;
}

double AUFPLIST::GetArtGew (int i)
{
          static double a_gew;
          static double a;
		  static cursor = -1;
          
          if (atoi (aufptab[i].me_einh) == 2)
          {
                    return ratod (aufptab[i].lief_me);   
          }

          return aufptab[i].a_gew * ratod (aufptab[i].lief_me);

/*

RoW 12.11.2008 : dieser Teil war bei sehr gro�en Auftr�gen (�ber 300 Positionen zu langsam)
                 Firma Lackmann. Das Gewicht wird jetzt beim Lesen 
				 der Artikelbezeichnung gesetzt in der Positionsliste im Speicher gesetzt..

          char buffer [512];
          a_gew = 0.0;
		  if (cursor == -1)
		  {
			  sprintf (buffer, "select a_gew from a_bas where a = ?");
			  DbClass.sqlout ((double *) &a_gew, 3, 0);
			  DbClass.sqlin  ((double *) &a, 3, 0);
			  cursor = DbClass.sqlcursor (buffer);
		  }
		  a = ratod (aufptab[i].a);
		  DbClass.sqlopen (cursor);
		  DbClass.sqlfetch (cursor);
          if (a_gew == (double) 0.0)
          {
              a_gew = 1.0;
          }

          return a_gew * ratod (aufptab[i].lief_me);
*/
}
          



void AUFPLIST::AnzAufGew (void)
/**
Auftragswert anzeigen.
**/
{
          int currentf;
		  int i;
		  double gew = 0.0;
		  double wert = 0.0;
		  double lqd = 0.0;
		  double anteil_smt = 0.0;
		  int recanz;

      
          if (auf_gew_anz == 0) return;

		  recanz = eListe.GetRecanz ();
		  if (recanz == 0)
		  {
			  if (AufGewhWnd) 
			  {
				  DestroyWindow (AufGewhWnd);
				  AufGewhWnd = NULL;
			  }
			  return;
		  }
          if (AufGewhWnd == NULL)
		  {
		            AufGewhWnd = CreateAufGew ();
					if (AufGewhWnd == NULL) return;
		  }
		  for (i = 0; i < recanz; i ++)
		  {
			      gew = gew + GetArtGew (i);
			      wert = wert + ratod (aufptab[i].lief_me) *
					            ratod (aufptab[i].auf_vk_pr);
          }

		  if (gew > 0)
		  {
			lqd = wert / gew;
		  }

		  if (ShowLqd && StndDirect == 3)
		  {
				anteil_smt = LISTHANDLER->GetSmtPart ();
		  }

		  SetStaticWhite (TRUE);
		  sprintf (aufgew, "%10.3lf", gew);
		  sprintf (auflqd, "%10.3lf", lqd);
		  sprintf (aufanteil, "%10.3lf", anteil_smt);
		  if (ShowLqd)
		  {
			display_form (AufGewhWnd0, &faufgewex, 0, 0); 
		  }
		  else
		  {
			display_form (AufGewhWnd0, &faufgew, 0, 0); 
		  }
		  SetStaticWhite (FALSE);

          currentfield = currentf;
}

void AUFPLIST::EnterLdVk (void)
/**
Laden-VK editieren.
**/
{
       char ld_pr [11]; 
	   EnterF Enter;

       if (ratod (aufps.a) == (double) 0.0)
       {
           return;
       }
	   sprintf (ld_pr, "%s", aufps.auf_lad_pr);
       Enter.SetColor (LTGRAYCOL);
	   Enter.EnterText (hMainWindow, "Laden-Preis ", ld_pr, 10, "%8.2lf");

	   if (syskey == KEY5)
	   {
                eListe.ShowAktRow ();
	            return;
	   }

       if (ratod (aufps.auf_lad_pr) != ratod (ld_pr))
       {
           aufps.ls_pos_kz = 1;
       }
       sprintf (aufps.auf_lad_pr, "%8.4lf", ratod (ld_pr));
       memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
       eListe.ShowAktRow ();
}


void AUFPLIST::EnterPrVk (void)
/**
Laden-VK editieren.
**/
{
       char vk_pr [11]; 
       int fpos;
       char *picture = "%8.2lf";
	   EnterF Enter;

       if (ratod (aufps.a) == (double) 0.0)
       {
           return;
       }
       fpos = GetItemPos (&dataform, "pr_vk");
       if (fpos != -1)
       {
           picture = dataform.mask[fpos].picture;
       }

	   sprintf (vk_pr, "%s", aufps.auf_vk_pr);
       Enter.SetColor (LTGRAYCOL);
	   Enter.EnterText (hMainWindow, "   VK-Preis ", vk_pr, 10, picture);

	   if (syskey == KEY5)
	   {
                eListe.ShowAktRow ();
	            return;
	   }

       sprintf (aufps.auf_vk_pr, "%lf", ratod (vk_pr));
       memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
       eListe.ShowAktRow ();
}


long AUFPLIST::TestProdLgr (long gruppe)
/**
Test, ob der Artikel beim Aktuellen Mandanten vorhanden ist.
**/
{
	      int dsqlstatus;
          short mdn;
		  double a;

		  a = ratod (aufps.a);
		  mdn = (short) gruppe;
		  DbClass.sqlin ((short *) &mdn, 1, 0);
		  DbClass.sqlin ((double *) &a, 3, 0);
		  dsqlstatus = DbClass.sqlcomm ("select a from a_lgr where mdn = ? and a = ?");
		  if (dsqlstatus == 0) return gruppe;
		  return ChoiseProdLgr (gruppe);
}


long AUFPLIST::ChoiseProdLgr (long gruppe)
/**
Produktionslager fuer Artikel auswaehlen.
**/
{
	   CHOISE *Choise;
	   HWND Pwindow;
	   char Mandant [80];
	   char Row [80];
	   char adr_krz [17];
	   short mdn;
	   char *Caption = "Produktions-Mandanten";
	   int cursor;
	   int mdn_cursor;
	   double a;
	   char pmdn[20];
	   char Ub [80];

       memset (pmdn, ' ', 19);
	   a = ratod (aufps.a);
	   if (a == (double) 0.0) return gruppe;

	   sprintf (Mandant, "  %ld", gruppe);
	   Choise = new CHOISE (-1, -1, 70, 16, Caption, 120, "Zugeordneter Mandant",
		                                                   "Gew�hlter Mandant");
	   Pwindow = Choise->OpenWindow (hMainInst, hMainWin);
	   Choise->SetChoiseDefault (Mandant);

	   sprintf (Ub, " Mandanten f�r Artikel %.0lf", ratod (aufps.a));
	   Choise->FillListUb (Ub);
	   DbClass.sqlin ((double *) &a, 3, 0);
	   DbClass.sqlout ((char *) &pmdn [2], 0, 9);
	   cursor = DbClass.sqlcursor ("select distinct mdn from a_lgr where a = ?");
	   DbClass.sqlin ((short *) &mdn, 1, 0);
	   DbClass.sqlout ((char *) adr_krz, 0, 17);
	   mdn_cursor = DbClass.sqlcursor ("select adr_krz from mdn,adr where mdn.mdn = ? "
		                               "and adr.adr = mdn.adr");
       while (DbClass.sqlfetch (cursor) == 0)
	   {
		           strcpy (adr_krz, ""); 
				   mdn = atoi (&pmdn[2]);
		           DbClass.sqlopen (mdn_cursor); 
		           DbClass.sqlfetch (mdn_cursor); 
				   sprintf (Row, "%s \"%s\"", pmdn, adr_krz);
	               Choise->FillListRow (Row);
	   }
       DbClass.sqlclose (cursor);  
       DbClass.sqlclose (mdn_cursor);  

	   Choise->ProcessMessages ();
	   Choise->DestroyWindow ();
       delete Choise;
       eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                             eListe.GetAktColumn ());
	   gruppe = atol (Mandant);
	   return gruppe;
}

      

HWND AUFPLIST::GetMamain1 (void)
{
       return (mamain1);
}

void AUFPLIST::SethwndTB (HWND hwndTB)
{
         this->hwndTB = hwndTB; 
         eListe.SethwndTB (hwndTB);
}

void AUFPLIST::SetTextMetric (TEXTMETRIC *tm)
{
         memcpy (&textm, tm, sizeof (TEXTMETRIC));
         eListe.SetTextMetric (tm);
}


void AUFPLIST::SetLineRow (int LineRow)
{
         eListe.SetLineRow (0);
}

void AUFPLIST::SetListLines (int i)
{ 
//         TListe.SetListLines (i);
         eListe.SetListLines (i);
}

void AUFPLIST::OnPaint (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	    HDC hdc;

        TListe.OnPaint (hWnd, msg, wParam, lParam);
        eListe.OnPaint (hWnd, msg, wParam, lParam);
        if (hWnd == eWindow)  
        {
                    hdc = BeginPaint (eWindow, &aktpaint);
                    ChoiseLines (eWindow, hdc); 
                    EndPaint (eWindow, &aktpaint);
        }
        else if (hWnd == AufMehWnd)  
        {
                    hdc = BeginPaint (AufMehWnd, &aktpaint);
//                    ChoiseLines (AufMehWnd, hdc); 
                    EndPaint (AufMehWnd, &aktpaint);
        }
        else if (hWnd == BasishWnd)  
        {
                    hdc = BeginPaint (BasishWnd, &aktpaint);
                    ChoiseLines (BasishWnd, hdc); 
                    EndPaint (BasishWnd, &aktpaint);
        }
        else if (hWnd == SaWindow)  
        {
                    hdc = BeginPaint (hWnd, &aktpaint);
                    PaintSa (hdc); 
                    EndPaint (hWnd, &aktpaint);
        }
        else if (hWnd == PlusWindow)  
        {
                    hdc = BeginPaint (hWnd, &aktpaint);
                    PaintPlus (hdc); 
                    EndPaint (hWnd, &aktpaint);
        }
}


void AUFPLIST::MoveListWindow (void)
{
        TListe.MoveListWindow ();
        eListe.MoveListWindow ();
}


void AUFPLIST::BreakList (void)
{
        eListe.BreakList ();
}


void AUFPLIST::OnHScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        TListe.OnHScroll (hWnd, msg,wParam, lParam);
        eListe.OnHScroll (hWnd, msg,wParam, lParam);
}

void AUFPLIST::OnVScroll (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        TListe.OnVScroll (hWnd, msg,wParam, lParam);
        eListe.OnVScroll (hWnd, msg,wParam, lParam);
}

void AUFPLIST::OnSize (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        if (hWnd == GetMamain1 ())
        {
                    MoveListWindow ();
                    MoveAufw ();
                    MoveAufGew ();
                    MoveSaW ();
                    MovePlus ();
        }
        else if (hWnd == TListe.GetMamain1 ())
        {
                    TListe.MoveListWindow ();
        }
}


void AUFPLIST::StdAuftrag (void)
{
         if (eListe.Getmamain3 () == NULL) return; 
         doStd ();
}

void AUFPLIST::FunkKeys (WPARAM wParam, LPARAM lParam)
{
	                if (TListe.Getmamain3())
					{
                            TListe.FunkKeys (wParam, lParam);
					}
					else
					{
                            eListe.FunkKeys (wParam, lParam);
					}
}


int AUFPLIST::GetRecanz (void)
{
          return eListe.GetRecanz ();
}


void AUFPLIST::SwitchPage0 (int rows)
{
           eListe.SwitchPage0 (rows);
}


HWND AUFPLIST::Getmamain2 (void)
{
           return eListe.Getmamain2 ();
}


HWND AUFPLIST::Getmamain3 (void)
{
           return eListe.Getmamain3 ();
}


void AUFPLIST::ChoiseFont (mfont *lfont)
{
           eListe.ChoiseFont (lfont);
}

void AUFPLIST::SetFont (mfont *lfont)
{
           TListe.SetFont (lfont);
           eListe.SetFont (lfont);
}

void AUFPLIST::SetListFont (mfont *lfont)
{
           TListe.SetListFont (lfont);
           eListe.SetListFont (lfont);
}

void AUFPLIST::FindString (void)
{
           eListe.FindString ();
}


void AUFPLIST::SetLines (int Lines)
{
           TListe.SetLines (Lines);
           eListe.SetLines (Lines);
}


int AUFPLIST::GetAktRow (void)
{
                 return eListe.GetAktRow ();
}

int AUFPLIST::GetAktRowS (void)
{
                 return eListe.GetAktRowS ();
}

void AUFPLIST::SetColors (COLORREF Color, COLORREF BkColor)
{
                 TListe.SetColors (Color, BkColor); 
                 eListe.SetColors (Color, BkColor); 
}

void AUFPLIST::SetListFocus (void)
{
                 eListe.SetFeldFocus0 (eListe.GetAktRow (), 
                                       eListe.GetAktColumn ()); 
}

void AUFPLIST::PaintUb (void)
{
                 eListe.PaintUb (); 
}

BOOL AUFPLIST::ContainsService ()
{
    int recs = eListe.GetRecanz ();
	for (int i = 0; i < recs; i ++)
	{
		if (aufptab[i].a_typ == DIENSTLEISTUNG)
		{
			return TRUE;
		}
	}
	return FALSE;
}

void AUFPLIST::StndComplete ()
{
	SetFkt (7, "Laden", KEY7);
    set_fkt (doActivate, 7);
    set_fkt (InsertLine, 6);
    Ampel.SetState (CAmpel::Green);
	Ampel.Invalidate ();
}

void AUFPLIST::StndComFailed ()
{
	disp_mess ("Com-Object (FitObjects) konnte nicht gestartet werden", 2), 
    Ampel.SetState (CAmpel::No);
	Ampel.Invalidate ();
}

void AUFPLIST::PrintAmpel (HDC hDC)
{
	if (Ampel.GetState () == CAmpel::Red)
	{
		Ampel.ShowRed (hDC);
	}
	else if (Ampel.GetState () == CAmpel::Green)
	{
		Ampel.ShowGreen (hDC);
	    Ampel.SetPlay (FALSE);
	}
	else
	{
		Ampel.SetUnvisible ();
	}
}


// Aufruf aus Workthread

int AUFPLIST::Execute (void *Param)
{
	if (Param == NULL)
	{
		PostMessage (hMainWindow, WM_STND_COMPLETE, 0, 0l);  
	}
	else if (strcmp ((LPCSTR) Param, "FAILED") == 0)
	{
		PostMessage (hMainWindow, WM_STND_COM_FAILED, 0, 0l);  
	}
	return 0;
}

BOOL AUFPLIST::CStndHandler::Run ()
{
	if (Instance != NULL)
	{
		Instance->StndComplete ();
		return TRUE;
	}
	return FALSE;
}

BOOL AUFPLIST::CStndFailed::Run ()
{
	if (Instance != NULL)
	{
		Instance->StndComFailed ();
		return TRUE;
	}
	return FALSE;
}

BOOL AUFPLIST::CStndReload::Run ()
{
	if (Instance != NULL)
	{
		Instance->doStd ();
		return TRUE;
	}
	return FALSE;
}

int AUFPLIST::SpalteAbmelden (char *spalte) //FS-126
{
	if (strcmp (clipped(spalte), "Verkaufspreis") == 0)
	{
		if (Spalte_pr_vk_Ok == TRUE) DelListField ("pr_vk"); 
		Spalte_pr_vk_Ok = FALSE;
		Spalte_pr_vk_gewaehlt = FALSE;
		DelPrVK = TRUE;
	}
	if (strcmp (clipped(spalte), "Ladenpreis") == 0)
	{
		if (Spalte_lad_pr_Ok == TRUE) DelListField ("ld_pr"); 
		Spalte_lad_pr_Ok = FALSE;
		Spalte_lad_pr_gewaehlt = FALSE;
		DelLadVK = TRUE;
	}

	if (strcmp (clipped(spalte), "Rabatt %") == 0)
	{
		if (Spalte_lad_pr_prc_Ok == TRUE) DelListField ("ld_pr_prc"); 
		Spalte_lad_pr_prc_Ok = FALSE;
		Spalte_lad_pr_prc_gewaehlt = FALSE;
		DelLadVKPrc = TRUE;
		LdVkAsPercent = FALSE;
	}
	if (strcmp (clipped(spalte), "letzter Verkaufspreis") == 0)
	{
		if (Spalte_lpr_vk_Ok == TRUE) DelListField ("lpr_vk"); 
		Spalte_lpr_vk_Ok = FALSE;
		Spalte_lpr_vk_gewaehlt = FALSE;
		AddLastVkPr = FALSE;
	}
	if (strcmp (clipped(spalte), "lange Artikelbezeichnung") == 0) //WAL-11
	{
		if (Spalte_langbez_Ok == TRUE) 
		{
			adda_bz1 ("lang_bez")	;		
			DelListField ("lang_bez");
		}
		Spalte_langbez_Ok = FALSE;
		Spalte_langbez_gewaehlt = FALSE;
		AddLangBez = FALSE;
	}
	if (strcmp (clipped(spalte), "letztes Lieferdatum") == 0)
	{
		if (Spalte_lieferdat_Ok == TRUE) DelListField ("lieferdat"); 
		Spalte_lieferdat_Ok = FALSE;
		Spalte_lieferdat_gewaehlt = FALSE;
		AddLastLief = FALSE;
	}
	if (strcmp (clipped(spalte), "letzte Liefermenge") == 0)
	{
		if (Spalte_last_me_Ok == TRUE) DelListField ("last_me"); 
		Spalte_last_me_Ok = FALSE;
		Spalte_last_me_gewaehlt = FALSE;
		DelLastMe = TRUE;
	}
	if (strcmp (clipped(spalte), "Basis Mengeneinheit") == 0)
	{
		if (Spalte_basis_me_bz_Ok == TRUE) DelListField ("basis_me_bz"); 
		Spalte_basis_me_bz_Ok = FALSE;
		Spalte_basis_me_bz_gewaehlt = FALSE;
		RemoveBasisMe = TRUE;
	}
//	DelListField (clipped(spalte));
	return 0;
}

int AUFPLIST::SpalteHinzufuegen (char *spalte) //FS-126
{

	if (strcmp (clipped(spalte), "Verkaufspreis") == 0)
	{
		if (Spalte_pr_vk_Ok == FALSE) addPrVk ("dummy"); 
		Spalte_pr_vk_Ok = TRUE;
		Spalte_pr_vk_gewaehlt = TRUE;
		DelPrVK = FALSE;
	}
	if (strcmp (clipped(spalte), "Ladenpreis") == 0)
	{
		if (Spalte_lad_pr_Ok == FALSE) addLadPr ("dummy"); 
		Spalte_lad_pr_Ok = TRUE;
		Spalte_lad_pr_gewaehlt = TRUE;
		DelLadVK = FALSE;
	}
	if (strcmp (clipped(spalte), "Rabatt %") == 0)
	{
		if (Spalte_lad_pr_prc_Ok == FALSE) addLadPrPrc ("dummy"); 
		Spalte_lad_pr_prc_Ok = TRUE;
		Spalte_lad_pr_prc_gewaehlt = TRUE;
		DelLadVKPrc = FALSE;
		LdVkAsPercent = TRUE;
	}
	if (strcmp (clipped(spalte), "letzter Verkaufspreis") == 0)
	{
		if (Spalte_lpr_vk_Ok == FALSE) addLastVkPr ("auf_me"); 
		Spalte_lpr_vk_Ok = TRUE;
		Spalte_lpr_vk_gewaehlt = TRUE;
		AddLastVkPr = TRUE;
	}
	if (strcmp (clipped(spalte), "lange Artikelbezeichnung") == 0) //WAL-11
	{
		if (Spalte_langbez_Ok == FALSE) 
		{
			addLangBez ("a_bz1"); 
	        DelListField ("a_bz1");
		}
		Spalte_langbez_Ok = TRUE;
		Spalte_langbez_gewaehlt = TRUE;
		AddLangBez = TRUE;
	}
	if (strcmp (clipped(spalte), "letztes Lieferdatum") == 0)
	{
		if (Spalte_lieferdat_Ok == FALSE) addLastLdat ("auf_me"); 
		Spalte_lieferdat_Ok = TRUE;
		Spalte_lieferdat_gewaehlt = TRUE;
		AddLastLief = TRUE;
	}
	if (strcmp (clipped(spalte), "letzte Liefermenge") == 0)
	{
		if (Spalte_last_me_Ok == FALSE) addLastMe ("auf_me"); 
		DelLastMe = FALSE;
		Spalte_last_me_Ok = TRUE;
		Spalte_last_me_gewaehlt = TRUE;
	}
	if (strcmp (clipped(spalte), "Basis Mengeneinheit") == 0)
	{
		if (Spalte_basis_me_bz_Ok == FALSE) addBasisMeBz ("dummy"); 
		Spalte_basis_me_bz_Ok = TRUE;
		Spalte_basis_me_bz_gewaehlt = TRUE;
		RemoveBasisMe = FALSE;
	}
	return 0;
}



int AUFPLIST::doAktAuf (void) //FS-126
{
	//abgekupfert aus SwitchStd
	 int akt_pos;
     int aufpanz;

    LISTHANDLER->SetNewPosi ();
     aufpanz = LISTHANDLER->HoleStndValue (aufptab, &akt_pos);
   //  if (aufpanz == 0) return 0;
	 auf_vk_pr = 0.0;
	 // testtesttest SendMessage (eListe.Getmamain3 (), WM_KEYDOWN, VK_RETURN, 0);
	 akt_pos = eListe.GetAktRow ();
//     aufpanz = eListe.GetRecanz ();
//     aufpanz = LISTHANDLER->HoleStndValue (aufptab, &akt_pos);
	 eListe.DestroyFocusWindow ();
     InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
     UpdateWindow (eListe.Getmamain3 ());
	 eListe.SetRecanz (aufpanz);
     eListe.DestroyVTrack ();
	 eListe.MoveListWindow ();
     syskey = KEYUP;
     eListe.FocusUp ();
     InvalidateRect (eListe.Getmamain3 (), 0, TRUE);
	 UpdateWindow (eListe.Getmamain3 ());
	 akt_pos = 0;
	 auf_vk_pr = 0.0;
     memcpy (&aufps, &aufptab [akt_pos], sizeof (struct AUFPS));
	 if (aufpanz == 0) 
	 {
		 eListe.SetAppend (1); 
		 eListe.InsertLine ();
	 } else eListe.SetAppend (0);

	 eListe.SetInsAttr ();
	 eListe.SetPos (akt_pos, eListe.FirstColumn ());
	 eListe.SetVPos (akt_pos);
     eListe.ShowAktRow ();
	 AnzAufWert ();
	 AnzAufGew ();

	return 0;
}



//==========================================================================
//=======          Verkaufs Fragen        ==================================
//==========================================================================
void AUFPLIST::SetzeEventVerkFragen (long pos_id, double a) //WAL-15
{
	if (pos_id == 0) return;
	if (syskey != KEYCR) return;

    ins_quest ((char *)   &a, 3, 0);
    ins_quest ((char *)   &pos_id, 2, 0);
    ins_quest ((char *)   &aufk.auf, 2, 0);
	execute_sql ("update event_a_frage set a = ? , pos_id = ? where auf = ?");
}
HANDLE AUFPLIST::StarteProcessVerkFragen (void) //WAL-15
{
			HANDLE Pid;
		   

	    char *etc;
		char buffer [512];
		FILE *fp;
		int x, y, cx, cy;
		int anz;
		BOOL AllwaysOnTop = TRUE;


		etc = getenv ("BWSETC");
		if (etc == NULL) return NULL;
		if (strlen(clipped(ProcessVerkFragen)) <2 ) return NULL;

		x = 400;
		y = 120;
		cx = 800;
		cy = 400;
		sprintf (buffer, "%s\\%s.rct", etc,"51100_VerkFragen");
		fp = fopen (buffer, "r");
		if (fp != NULL) 
		{
			while (fgets (buffer, 511, fp))
			{
				cr_weg (buffer);
				anz = wsplit (buffer, " ");
				if (anz < 2) continue;
				if (strcmp (wort[0], "left") == 0)
				{
					x = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "top") == 0)
				{
					y = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "right") == 0)
				{
					cx = atoi (wort[1]);
				}
				else if (strcmp (wort[0], "bottom") == 0)
				{
					cy = atoi (wort[1]);
				}
			}
			fclose (fp);
		}
		sprintf (buffer, "%s %ld %ld %ld", ProcessVerkFragen, x,y,aufk.auf);


    ins_quest ((char *)   &aufk.auf, 2, 0);
	execute_sql ("delete from event_a_frage where auf = ?");

    ins_quest ((char *)   &aufk.kun, 2, 0);
    ins_quest ((char *)   &aufk.auf, 2, 0);
	execute_sql ("insert into event_a_frage (a,kun,pos_id,auf) values (-1,?,-1,?)");
//	return Pid; //test  
//	disp_mess (buffer,1);
      Pid = ProcExecPid (buffer, SW_SHOWNORMAL, x, y, cx, cy);
    SetProcessIDVerkFragen (Pid);
	return Pid ;
}
int AUFPLIST::KillProcessVerkFragen (void) //WAL-15
{
		   
	if (ProcessIDVerkFragen != NULL) TerminateProcess (ProcessIDVerkFragen, 0);
	ProcessIDVerkFragen = NULL;

    ins_quest ((char *)   &aufk.auf, 2, 0);
	execute_sql ("delete from event_a_frage where auf = ?");

	return 0 ;
}


//==========================================================================
//=======          WAH-Kommission F2        ================================
//==========================================================================
int AUFPLIST::Setuserdef1 (void) //WAL-70
{
	if (aufps.userdef1 == 0 || aufps.userdef1 == -1) 
	{
		aufps.userdef1 = 1;
	}
	else
	{
		aufps.userdef1 = -1;
	}
    memcpy (&aufptab[eListe.GetAktRow()], &aufps, sizeof (struct AUFPS));
    eListe.ShowAktRow ();
	return 0;
}

void AUFPLIST::WriteVerkKunAntw (int pos) //WAL-70
{
	if (aufptab[pos].pos_id != 0)
	{
		if (aufptab[pos].userdef1 == -1)
		{
			aufptab[pos].userdef1 = 0;
			WriteVerkKunAntw (aufk.kun, ratod(aufptab[pos].a), aufptab[pos].userdef1);
		}
		else if (aufptab[pos].userdef1 == 1)
		{
			WriteVerkKunAntw (aufk.kun, ratod(aufptab[pos].a), aufptab[pos].userdef1);
		}
	}
}

void AUFPLIST::WriteVerkKunAntw (long kun , double a, short userdef1) //WAL-70
{
	short verk_wah = 0;
	short sql_mode_sav;
	extern short sql_mode ; 
	sql_mode_sav = sql_mode;
	sql_mode = 1;    // im Fehlerfall (Tabelle gesperrt)  nix tun, ist kein Beinbruch


	DbClass.sqlin ((long *) &kun, 2,0);
	DbClass.sqlin ((double *) &a, 3,0);
	DbClass.sqlout ((short *) &verk_wah, 1,0);
	if (DbClass.sqlcomm ("select wah from verk_kun_antw where kun = ? and a = ?") == 0)
	{
		if (verk_wah != userdef1)
		{
			DbClass.sqlin ((short *) &userdef1, 1,0);
			DbClass.sqlin ((long *) &kun, 2,0);
			DbClass.sqlin ((double *) &a, 3,0);
			DbClass.sqlcomm ("update verk_kun_antw set wah = ? where kun = ? and a = ?"); 
		}
	}
	else
	{
		DbClass.sqlin ((long *) &kun, 2,0);
		DbClass.sqlin ((double *) &a, 3,0);
		DbClass.sqlin ((short *) &userdef1, 1,0);
		DbClass.sqlcomm ("insert into verk_kun_antw (kun,a,frage_id,wah) values (?,?,0,?)"); 
	}
	sql_mode = sql_mode_sav;

}
short AUFPLIST::GetDefaultUserdef1 (long kun, double a)
{
	short verk_wah = 0;

	DbClass.sqlin ((double *) &a, 3,0);
	DbClass.sqlout ((short *) &verk_wah, 1,0);
	if (DbClass.sqlcomm ("select verk_frage_antw.wah from verk_frage_antw, verk_art_frage where verk_art_frage.a = ? and verk_art_frage.frage_id = verk_frage_antw.id") == 0)
	{
		if (verk_wah > 0) return verk_wah;
	}

	DbClass.sqlin ((long *) &kun, 2,0);
	DbClass.sqlin ((double *) &a, 3,0);
	DbClass.sqlout ((short *) &verk_wah, 1,0);
	if (DbClass.sqlcomm ("select wah from verk_kun_antw where kun = ? and a = ?") == 0)
	{
		return verk_wah;
	}

	return 0;
}