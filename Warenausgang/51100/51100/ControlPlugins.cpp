#include "ControlPlugins.h"
#include "Text.h"

#define CString Text

HANDLE CControlPlugins::Controls = NULL;

CControlPlugins::CControlPlugins ()
{
    pGetPosTxtKz =NULL;
	char *bws;

	bws = getenv ("BWS");

	CString DllName;
	if (bws != NULL)
	{
		DllName.Format ("%s\\bin\\Controls.dll", bws);
	}
	else
	{
		DllName = "preisewa.dll";
	}
	Controls = LoadLibrary (DllName.GetBuffer ());
	if (Controls != NULL)
	{
		pGetPosTxtKz = (int (*) (int)) 
					GetProcAddress ((HMODULE) Controls, "GetPosTxtKz");
	}
}

int CControlPlugins::GetPosTxtKz (int PosTxtKz)
{
	if (pGetPosTxtKz != NULL)
	{
		return (*pGetPosTxtKz) (PosTxtKz);
	}
	return PosTxtKz;
}

BOOL CControlPlugins::IsActive ()
{
	if (pGetPosTxtKz != NULL)
	{
		return TRUE;
	}
	return FALSE;
}


