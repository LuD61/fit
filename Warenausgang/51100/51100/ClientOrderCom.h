// ClientOrderCom.h: Schnittstelle f�r die Klasse CClientOrderCom.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ClientORDERCOM_H__BB91BD11_28A4_4013_A951_FBF1DEE80D84__INCLUDED_)
#define AFX_ClientORDERCOM_H__BB91BD11_28A4_4013_A951_FBF1DEE80D84__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ClientOrder.h"
#include "ComObject.h"
#include "StndOrder.h"
#include "CAufps.h"

#import "FitObjects.tlb" no_namespace

class CClientOrderCom : public CClientOrder  
{
private:
	IUnknown *pUnknown;
	ComObject com;
	CStndOrder StndOrder;
	CCAufps *CAufps;
	BOOL Window;
	short client;
	BOOL StopWorking;
	int iCount;

public:
	void SetStopWorking (BOOL StopWorking)
	{
		this->StopWorking = StopWorking;
	}
	void SetWindow (BOOL Window)
	{
		this->Window = Window;
	}

	CClientOrderCom();
	virtual ~CClientOrderCom();
	BOOL StartServer ();
	int UnregisterServer ();
	void Load ();

};

#endif // !defined(AFX_ClientORDERCOM_H__BB91BD11_28A4_4013_A951_FBF1DEE80D84__INCLUDED_)
