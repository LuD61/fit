// Application.cpp: Implementierung der Klasse CApplication.
//
//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "Application.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CApplication *CApplication::Instance;

CApplication::CApplication()
{
	appInstance = NULL;
	Instance = NULL;

}

CApplication::~CApplication()
{

}

CApplication *CApplication::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CApplication ();
	}
	return Instance;
}

void CApplication::Destroy ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}
