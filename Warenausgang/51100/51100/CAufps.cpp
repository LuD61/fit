// CAufps.cpp: Implementierung der Klasse CCAufps.
//
//////////////////////////////////////////////////////////////////////

#include <comdef.h>
#include "Text.h"
#include "CAufps.h"

#define CString Text

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

typedef struct
{
		OLECHAR FAR **Name;
		DISPID *Id;
} NAID;


CCAufps::CCAufps()
{
	iCAufps = NULL;
	IdsOK = FALSE;
	NGetPosi =  L"Posi";
	NGetA =  L"A";
	NGetABz1 =  L"ABz1";
	NGetAufVkPr =  L"AufVkPr";
	NGetAufLadPr =  L"AufLadPr";
	NGetAufLadPr0 =  L"AufLadPr0";
	NGetAufLadPrPrc =  L"AufLadPrPrc";
	NGetMarge =  L"Marge";
	NGetSaKzSint =  L"SaKzSint";
	NGetAGew =  L"AGew";
	NGetDrFolge =  L"DrFolge";
	NGetMeEinh =  L"MeEinh";
	NGetBasisMeBz =  L"BasisMeBz";
	NGetMeEinhKun =  L"MeEinhKun";
	NGetMeEinhKun1 =  L"MeEinhKun1";
	NGetMeEinhKun2 =  L"MeEinhKun2";
	NGetMeEinhKun3 =  L"MeEinhKun3";
	NGetInh1 =  L"Inh1";
	NGetInh2 =  L"Inh2";
	NGetInh3 =  L"Inh3";
	NGetMeBz =  L"MeBz";
	NGetLastMe =  L"LastMe";
	NGetLastLDat =  L"LastLDat";
	NGetLastPrVk =  L"LastPrVk";
	NGetBsd =  L"Bsd";
	NGetBsd2 =  L"Bsd2";
	NGetEndOfRecords =  L"EndOfRecords";
}

CCAufps::~CCAufps()
{

}

CCAufps& CCAufps::operator= (IDispatch *idpt)
{
	Init (idpt);
	return *this;
}

BOOL CCAufps::Init (IDispatch *idp)
{
	HRESULT hr;
	iCAufps = idp;

	if (IdsOK) return TRUE;
	NAID mTab[] = {
			        &NGetPosi, &IdGetPosi,
			        &NGetA, &IdGetA,
			        &NGetABz1, &IdGetABz1,
				    &NGetAufVkPr, &IdGetAufVkPr,
				    &NGetAufLadPr, &IdGetAufLadPr,
					&NGetAufLadPr0, &IdGetAufLadPr0,
					&NGetAufLadPrPrc, &IdGetAufLadPrPrc,
					&NGetMarge, &IdGetMarge,
					&NGetSaKzSint, &IdGetSaKzSint,
					&NGetAGew, &IdGetAGew,
					&NGetDrFolge, &IdGetDrFolge,
					&NGetMeEinh, &IdGetMeEinh,
					&NGetBasisMeBz, &IdGetBasisMeBz,
					&NGetMeEinhKun, &IdGetMeEinhKun,
					&NGetMeEinhKun1, &IdGetMeEinhKun1,
					&NGetMeEinhKun2, &IdGetMeEinhKun2,
					&NGetMeEinhKun3, &IdGetMeEinhKun3,
					&NGetInh1, &IdGetInh1,
					&NGetInh2, &IdGetInh2,
					&NGetInh3, &IdGetInh3,
					&NGetMeBz, &IdGetMeBz,
					&NGetLastMe, &IdGetLastMe,
					&NGetLastLDat, &IdGetLastLDat,
				    &NGetLastPrVk, &IdGetLastPrVk,
				    &NGetBsd, &IdGetBsd,
				    &NGetBsd2, &IdGetBsd2,
			        &NGetEndOfRecords, &IdGetEndOfRecords,
				    NULL, NULL
	};

	for (int i = 0; mTab[i].Name != NULL; i ++)
	{
		hr = iCAufps->GetIDsOfNames (IID_NULL, mTab[i].Name, 
				  			      1, LOCALE_USER_DEFAULT, mTab[i].Id);
		if (FAILED (hr))
		{
			return FALSE;
		}
	}
	IdsOK = TRUE;
    return TRUE;
}

BOOL CCAufps::Failed ()
{
	return !IdsOK;
}


long CCAufps::GetPosi ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetPosi, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.lVal;
}

double CCAufps::GetA ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetA, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

HRESULT CCAufps::GetABz1 (char *a_bz1, int len)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetABz1, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	_bstr_t bAbz1 = pResult.bstrVal;
	memset (a_bz1, 0, len);
    strncpy (a_bz1, bAbz1, len - 1);
	delete dp.rgvarg;
	return hr;
}

double CCAufps::GetAufVkPr ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetAufVkPr, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

double CCAufps::GetAufLadPr ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetAufLadPr, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

double CCAufps::GetAufLadPr0 ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetAufLadPr0, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}


double CCAufps::GetMarge ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetMarge, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

double CCAufps::GetAufLadPrPrc ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetAufLadPrPrc, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}


short CCAufps::GetSaKzSint ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetSaKzSint, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.iVal;
}

double CCAufps::GetAGew ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetAGew, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

short CCAufps::GetDrFolge ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetDrFolge, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.iVal;
}

short CCAufps::GetMeEinh ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetMeEinh, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.iVal;
}

HRESULT CCAufps::GetBasisMeBz (char *basis_me_bz, int len)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetBasisMeBz, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	_bstr_t bBasisMeBz = pResult.bstrVal;
	memset (basis_me_bz, 0, len);
    strncpy (basis_me_bz, bBasisMeBz, len - 1);
	delete dp.rgvarg;
	return hr;
}


short CCAufps::GetMeEinhKun ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetMeEinhKun, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.iVal;
}

short CCAufps::GetMeEinhKun1 ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetMeEinhKun1, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.iVal;
}

short CCAufps::GetMeEinhKun2 ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetMeEinhKun2, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.iVal;
}

short CCAufps::GetMeEinhKun3 ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetMeEinhKun3, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.iVal;
}


double CCAufps::GetInh1 ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetInh1, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

double CCAufps::GetInh2 ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetInh2, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

double CCAufps::GetInh3 ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetInh3, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

HRESULT CCAufps::GetMeBz (char *me_bz, int len)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetMeBz, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	_bstr_t bMeBz = pResult.bstrVal;
	memset (me_bz, 0, len);
	char *bz = bMeBz;
    strncpy (me_bz, bMeBz, len - 1);
	delete dp.rgvarg;
	return hr;
}


double CCAufps::GetLastMe ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetLastMe, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

HRESULT CCAufps::GetLastLDat (char *last_ldat, int len)
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetLastLDat, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}
	_bstr_t bLastLDat = pResult.bstrVal;
	memset (last_ldat, 0, len);
    strncpy (last_ldat, bLastLDat, len - 1);
	delete dp.rgvarg;
	return hr;
}


double CCAufps::GetLastPrVk ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetLastPrVk, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

double CCAufps::GetBsd ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetBsd, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

double CCAufps::GetBsd2 ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetBsd2, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.dblVal;
}

BOOL CCAufps::GetEndOfRecords ()
{
	HRESULT hr;
    DISPPARAMS FAR dp;
    VARIANT FAR  pResult;  

	dp.rgvarg = NULL;
	dp.rgdispidNamedArgs = NULL;
	dp.cArgs = 0;
	dp.cNamedArgs = 0;

	hr = iCAufps->Invoke (IdGetEndOfRecords, IID_NULL, LOCALE_USER_DEFAULT,
 					   DISPATCH_METHOD, &dp,
	                   &pResult, NULL, NULL);

	for (unsigned int i = 0; i < dp.cArgs; i ++)
	{
		VariantClear (&dp.rgvarg[i]);
	}

	delete dp.rgvarg;
	if (FAILED (hr))
	{
			return NULL;
	}
	return pResult.boolVal;
}
