//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by auftest.rc
//
#define IDI_AMPEL_RED                   105
#define IDI_AMPEL_GREEN                 106
#define IDI_AMPEL_REDS                  107
#define IDI_AMPEL_GREENS                108
#define IDD_ADR_DIALOG                  109
#define CallT1                          850
#define CallT2                          851
#define CallT3                          852
#define IDC_LBORDER                     1000
#define IDC_LFIRSTNAME                  1001
#define IDC_LSTREET                     1002
#define IDC_LVILLAGE                    1003
#define IDC_LPHONE                      1004
#define IDC_LFAX                        1005
#define IDC_LNAIL                       1006
#define IDC_NAME                        1007
#define IDC_FIRSTNAME                   1008
#define IDC_STREET                      1009
#define IDC_VILLAGE                     1010
#define IDC_PHONE                       1011
#define IDC_FAX                         1012
#define IDC_MAIL                        1013
#define IDC_PLZ                         1014
#define IDC_LNAME                       -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         40003
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
