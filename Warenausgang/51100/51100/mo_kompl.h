#ifndef _MO_KOMPL_DEF
#define _MO_KOMPL_DEF
#include "mo_progcfg.h"



class KompAuf
{
        private :
            char formattab[20] [30]; 
            short mdntab[1000]; //EF-38 (vonn 1000 auf 10000) -> wieder zur�ckgebaut, LFZ bei Essig
            short filtab[1000]; //EF-38
            long auftab[1000]; //EF-38
            long aufart[1000]; //EF-69a
            long tourtab[1000]; //EF-69
            long kuntourtab[1000]; //EF-69a
            long kuntab[1000]; //EF-69
            long lieferdattab[1000]; //EF-69
            long KumMdn[20]; //EF-69
            long KumTour[20]; //EF-69
            long KumTouAdr[20]; //EF-69
            long KumKunTour[20]; //EF-69
            long KumAuftrag[20]; //EF-69
            long KumLieferdat[20]; //EF-69
            long KumulierteAuftraege[1000]; //EF-69a
            char KumAuf [20] [3000]; //EF-69
            int aufanz; 
            int formatanz;
            short mdntablock[1000]; //EF-38 
            short filtablock[1000]; //EF-38
            long auftablock[1000]; //EF-38
            long mdn_kum[100]; 
            long fil_kum[100]; 
            long auf_kum[100]; 
            int aufanzlock;
            long drklstab [1000]; //EF-38
            int drklsanz;
            int auf_a_sort; 
			int LsInBearbeitung;
			BOOL IsGroup;
			long max_auf;
            static PROG_CFG *ProgCfg; 
			char drformatstd [20];
        public :
			static BOOL TxtByDefault;
			static BOOL RunLs;
			static BOOL SaveDefault;
			static BOOL WithAufb;
			static char drformat [20];
			static char aufbformat [20];
			BOOL ChoicePrinter;
			BOOL AngText;
            KompAuf () : auf_a_sort (0), IsGroup (FALSE), aufanz (0), formatanz (0), aufanzlock (0)
            {
				strcpy (drformat, "51100"); 
				strcpy (drformatstd, "51100"); 
                ProgCfg = NULL;
				ChoicePrinter = TRUE;
				AngText = TRUE;
            }

            void SetProgCfg (PROG_CFG *ProgCfg)
            {
                this->ProgCfg = ProgCfg;
            }

			void SetDrFormat (char *format)
			{
				strcpy (drformat, format);
				strcpy(aufk.feld_bz3,drformat);
			}
			void SetDrFormatStandard (char *format)
			{
				strcpy (drformatstd, format);
			}
			void SetLsInBearbeitung (int dls)
			{
				LsInBearbeitung = dls;
			}
			char* GetDrFormatStandard (void)
			{
				return drformatstd;
			}

			long Getmax_auf (void)
			{
				return max_auf;
			}

            void SetAbt (BOOL);
            void SetDrkKun (BOOL);
            void SetDrkExtra (BOOL);
            void SetDrkTeilSmt (BOOL);
            void SetDrkKunGr2 (BOOL);
            void SetExtraVon (int);
            void SetExtraBis (int);
            void SetLuKun (BOOL);
            void SetKompDefault (int);
            void SetDrkLsSperr (BOOL);
            void SetLiefMePar (int);
            void SetLog (int);
            void SetKlstPar (int);
            void SetLutzMdnPar (int);
            void SetSplitLs (int);
            void SetLieferdatToKommdat (BOOL);
            void BearbKomplett (void);
            int  SetKomplett (HWND);
            long GenLsNr (short, short);
            long GenLsNr0 (short, short);
            long GenAufNr (short, short);
            long GenAufNr0 (short, short);
            void AufktoLsk (int NurBerechnung); //EF-69
            void AufktoAufk (void);
            void aufpttolspt (void);
            long GenLspTxt0 (void);
			BOOL TxtNrExist (long);
            long GenLspTxt (void);
            long GenAufpTxt0 (void);
			BOOL AufpTxtNrExist (long);
            long GenAufpTxt (void);
            void AufptoLsp (long);
            void AufptoUpdLsp (void);
            void angpttoaufpt ();
            double GetGew (void);
            double GetAufMeVgl(void);
            void TeilSmttoLs (void);
            BOOL TeilSmttoAuf (int);
            void ProdMdntoAuf (int);
            int Geteinz_ausw (void);
            void AuftoLs (int NurBerechnung); //EF-69
            void AuftoLsOld (void);
            void ShowLocks (HWND);
            void ShowNoDrk (HWND hWnd);
			void InitAufanz (void);
			void KompParams (void);
            void AuftoLsGroup (char *, int);
            void Getauf_a_sort (void);
            void AuftoPid (void);
            void InitPid (void);
            void K_Liste_Direct ();
            void K_Liste_Fax (void);
            void Print_Liste ();
            void K_Liste ();
			void Kom_Liste_Rab ();
			static BOOL IsLlFormat ();
            void K_LlListe ();
			static void GeneriereLSEinzel_aus_Auftrag (void);
			void Auswerte_kun_gr2 (short kun_gr2);
			void K_LlListeGroup (void);
            BOOL K_ListeGroup (short, short, short, short,long, long,
							   char *, char *, short, short, long, long,
							   short, short, short, short,short, short,long, long, long);
			int FaxAuf (HWND, short, short, long);

            void K_ListeGroup0 (char *);
            int  SetFak_typ (HWND hWnd);
            void FreeAuf (void);
            void LockAuf (void);
            void TestFree (void);
            void K_ListeTS (void);
            void PrintAufbest (void);
            void DrkLsTS (void);
            void AuftoLsTS (void);
            void AufToSmtDrk (int, long *);
			long GetGruppe (void);
            BOOL BsdArtikel (double);
            void BucheBsd (double, double,  double);
            void SetBsdKz (int);
            void AngtoAuf (void);
            void AngktoAufk (void);
            void AngptoAufp (long);
            static long GetLagerort (long, double);
		    void GetEinhIst (void);
			BOOL FuelleKumAuf (short mdn, int auf, int kuntou, int kun, long lieferdat,int tou); //EF-69
			void ErzeugeLsk (short mdn, short fil, int tou, int kun, long lieferdat); //EF-69
			void ErzeugeKumAuf (short mdn, short fil, int kuntou, int kun, long lieferdat, int tou); //EF-69a
			void InitKumAuf (void);
			void ErzeugeKumAuf (void);
			void LskToPid (char *);
			void AufToPid (char *);
			void ErzeugeLsp (char *);
			void ErzeugeAufp (char *); //EF-69a
};
#endif
