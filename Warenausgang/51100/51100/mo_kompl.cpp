#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include <time.h>
#include <direct.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "message.h"
#include "ls.h"
#include "kun.h"
#include "mdn.h"
#include "fil.h"
// #include "mo_numme.h"
#include "mo_kompl.h"
#include "mo_einh.h"
#include "mo_curso.h"
#include "sys_par.h"
#include "dbclass.h"
#include "mo_lsgen.h"
#include "aufp_txt.h"
#include "lsp_txt.h"
#include "angpt.h"
#include "aufpt.h"
#include "lspt.h"
#include "auto_nr.h"
#include "mo_auto.h"
#include "mo_nr.h"
#include "70001.h"
#include "bsd_buch.h"
#include "mo_menu.h"
#include "a_bas.h"
#include "angk.h"
#include "angp.h"
#include "mo_aufl.h"
#include "Text.h"
#include "Process.h"
#include "mo_llgen.h"
#include "ControlPlugins.h"
#include "ls_einzel.h"
#include "tmp_catering.h"
#include "ptab.h"
#include "tou.h"   //EF-69

#define MAXWAIT 60

LS_CLASS ls_class;
class LS_EINZEL_CLASS ls_einzel_class;
static KUN_CLASS kun_class;
class CTMP_CATERING tmp_catering_class;

static EINH_CLASS einh_class;
static SYS_PAR_CLASS sys_par_class;
static DB_CLASS DbClass;
static AUFP_TCLASS aufp_tclass;
static LSP_TCLASS lsp_tclass;
static ANGPT_CLASS angpt_class;
static AUFPT_CLASS aufpt_class;
static LSPT_CLASS lspt_class;
static AUTO_CLASS AutoClass;
static ADR_CLASS adr_class;
static AutoNrClass AutoNr;
static BSD_BUCH_CLASS BsdBuch;
static ANGK_CLASS angk_class;
static ANGP_CLASS angp_class;
extern MELDUNG Mess;
static PTAB_CLASS ptab_class;

extern HWND hMainWindow;
extern char LONGNULL[4];




LLGEN_CLASS Llgen;

/**
Auftrag auf komplett setzen.
**/

struct AP
{
	short mdn;
	short fil;
	long auf;
	long posi;
	double a;
};

BOOL SalatBuffetImAuftrag = FALSE;

static int bsd_kz = 1;
static struct AP *aufptab = NULL;
static long aufptanz;


static BOOL klst_dr_par = FALSE;
static BOOL drk_ls_sperr = 0;
static int lief_me_par = 0;
static int LskOK;
static int AufkOK;
static BOOL AbtRemoved = TRUE;
static BOOL DrkKunRemoved = TRUE;
static BOOL DrkExtraRemoved = TRUE;
static BOOL DrkTeilSmtRemoved = TRUE;
static BOOL DrkKunGr2Removed = TRUE;
static BOOL LuKunRemoved = TRUE;
static int extra_von      = 0;
static int extra_bis      = 3;

static int LsKomplett     = 0;
static int aufKliste      = 0;
static int aufBest        = 0;
static int aufUeandDrk    = 0;
static int aufLsUeb       = 0;
static int aufLsKomp      = 0;
static int drwahl         = 0;

static int *aufbuttons0 [] = {&aufKliste, &aufUeandDrk, &aufLsUeb, &aufLsKomp, &drwahl, NULL};
static int *aufbuttons1 [] = {&aufKliste, &aufBest, &aufUeandDrk, &aufLsUeb, &aufLsKomp, &drwahl, NULL};
static int **aufbuttons = aufbuttons0;

static int SetLsKomp0 (void);
static int SetLsUeb0 (void);
static int SetKlst0 (void);
static int SetAufb0 (void);
static int SetUeandDrk0 (void);

static int (*SetDefTab[]) () = {SetKlst0, SetUeandDrk0, SetLsUeb0, SetLsKomp0, SetAufb0};

static int SetKlst (void);
static int SetAufb (void);
static int SetUeandDrk (void);
static int SetLsUeb (void);
static int SetLsKomp (void);
static int BreakBox (void);
static int BoxBreak (void);

static int IsDrChoise;

static ITEM iKliste   ("",  "K.-Liste",    "", 0);
static ITEM iAufb     ("",  "Auftragsbest�tigunh", "", 0);
static ITEM iUeandDrk ("",  "�berg&Drk",   "", 0);
static ITEM iLsUeb    ("",  "LS�bergabe",  "", 0);
static ITEM iLsKomp   ("",  "LSKomplett",  "", 0);
static ITEM  iOK      ("", "    OK      ",  "", 0);
static ITEM  iCancel  ("", "  Abbruch   ",  "", 0);
static ITEM  iDW      ("", "Druckerwahl ",  "", 0);


static field _cbox [] = {
&iKliste,                19, 2, 0,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetKlst, 101,
&iUeandDrk,              19, 2, 2,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetUeandDrk, 102,
&iLsUeb,                 19, 2, 4,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetLsUeb, 103,
&iLsKomp,                19, 2, 6,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetLsKomp, 104,
&iOK,                    15, 0, 9,  6,  0, "", BUTTON, 0, BreakBox, 107,
&iCancel,                15, 0, 9, 23, 0, "",  BUTTON, 0, BoxBreak, KEY5,
&iDW,                    15, 0, 9, 40, 0, "",  BUTTON, 0, BoxBreak, KEY7};

form cbox = {7, 0, 0, _cbox, 0, SetKlst0, 0, 0, NULL};

static field _cboxex [] = {
&iKliste,                19, 2, 0,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetKlst, 101,
&iAufb,                  19, 2, 2,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetAufb, 105,
&iUeandDrk,              19, 2, 4,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetUeandDrk, 102,
&iLsUeb,                 19, 2, 6,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetLsUeb, 103,
&iLsKomp,                19, 2, 8,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetLsKomp, 104,
&iOK,                    15, 0,11,  6,  0, "", BUTTON, 0, BreakBox, 107,
&iCancel,                15, 0,11, 23, 0, "",  BUTTON, 0, BoxBreak, KEY5,
&iDW,                    15, 0,11, 40, 0, "",  BUTTON, 0, BoxBreak, KEY7};

form cboxex = {8, 0, 0, _cbox, 0, SetKlst0, 0, 0, NULL};


static field _cboxdrk [] = {
&iKliste,                19, 2, 0,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetKlst, 101,
&iOK,                    15, 0,11,  6,  0, "", BUTTON, 0, BreakBox, 107,
&iCancel,                15, 0,11, 23, 0, "",  BUTTON, 0, BoxBreak, KEY5,
&iDW,                    15, 0,11, 40, 0, "",  BUTTON, 0, BoxBreak, KEY7};

form cboxdrk = {4, 0, 0, _cboxdrk, 0, SetKlst0, 0, 0, NULL};

static int auf_text_par;
static int lutz_mdn_par = 0;
static int wa_pos_txt;
static BOOL SplitLs = 0;
static BOOL with_log = TRUE;
static BOOL LieferdatToKommdat = FALSE;

static char *LogName = NULL;
static char *LstName = NULL;


static void CreateLogfile (void)
/**
Log-Datei oeffnen.
**/
{
	      char buffer [512];
		  char count [80];
		  int c;
		  char *tmp;
		  char datum [12];
		  char zeit  [12];
		  FILE *fp;
          time_t timer;
          struct tm *ltime;

 	      static char *days [] = {"Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam", NULL};

		  if (with_log == FALSE) return;

		  sysdate (datum);
		  systime (zeit);
		  tmp = getenv ("TMPPATH");
		  if (tmp == NULL) return;

          time (&timer);
          ltime = localtime (&timer);

          sprintf (buffer, "%s\\%s", tmp, days [ltime->tm_wday]);
          _mkdir (buffer);

          strcat (buffer, "\\");
		  strcat (buffer, "count");
		  c = 0;
		  fp = fopen (buffer, "r");
		  if (fp)
		  {
			  if (fgets (count, 79, fp))
			  {
				  c = atoi (count);
			  }
			  fclose (fp);
		  }
		  if (LogName) delete LogName;
          LogName = new char [512];
		  if (LogName == NULL) return;
		  sprintf (LogName, "%s\\%s\\51100.%d", tmp, days [ltime->tm_wday], c);
		  fp = fopen (LogName, "w");
		  if (fp == NULL)
		  {
			  delete LogName;
			  LogName = NULL;
              return;
		  }
		  fprintf (fp, "Log-Datei Auftragserfassung erzeugt am %s %s\n", datum,
			                                                             zeit);
		  fclose (fp);
		  c ++;
		  if (c == 20) c = 0;
		  fp = fopen (buffer, "w");
		  fprintf (fp, "%d", c);
		  fclose (fp);
		  if (LstName) delete LstName;
		  LstName = new char [512];
		  sprintf (LstName, "%s\\%s\\liste.%d", tmp, days [ltime->tm_wday], atoi (count));
}

void CopyListe (void)
/**
Liste sichern.
**/
{
	  char quelle [512];
	  char ziel [512];

	  if (with_log == FALSE) return;
	  if (LstName == NULL) return;
	  char *tmp;

	  tmp = getenv ("TMPPATH");
	  if (tmp == NULL) return;
	  sprintf (quelle, "%s\\51100.lst", tmp);
	  sprintf (ziel, "%s", LstName);
	  CopyFile (quelle, ziel, FALSE);
}


void CloseLogfile (void)
/**
Dateiname fuer Logfile abschliessen.
**/
{
		  if (with_log == FALSE) return;
	      if (LogName == NULL) return;

	      delete LogName;
		  LogName = NULL;

	      if (LstName == NULL) return;

	      delete LstName;
		  LstName = NULL;
}


void WriteLogfile (char * format, ...)
/**
Debugausgabe.
**/
{
          va_list args;
          FILE *logfile;

		  if (with_log == FALSE) return;
		  if (LogName == NULL)
		  {
			  CreateLogfile ();
		  }

		  logfile = fopen (LogName, "a");
		  if (logfile == NULL) return;

          va_start (args, format);
          vfprintf (logfile, format, args);
          va_end (args);
		  fclose (logfile);
}


int KomplettEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

void UnsetBox (int pos)
/**
Button auf selectierte setzen. (ausser pos);
**/
{
        int i;

        for (i = 0; aufbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (cbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *aufbuttons [i] = 0;
        }
}


int SetKlst (void)
/**
K-Liste setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       aufKliste = 0;
          }
          else
          {
                       aufKliste = 1;
          }
          if (syskey == KEYCR)
          {
                       aufKliste = 1;
                       SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       aufKliste, 0l);
                       break_enter ();
          }
          UnsetBox (0);
          return 1;
}

int SetAufb (void)
/**
K-Liste setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       aufBest = 0;
          }
          else
          {
                       aufBest = 1;
          }
          if (syskey == KEYCR)
          {
                       aufBest = 1;
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       aufBest, 0l);
                       break_enter ();
          }
          UnsetBox (1);
          return 1;
}

int SetUeandDrk (void)
/**
Uebergabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;
          int plus = 0;
          if (KompAuf::WithAufb) plus = 1;

          if (SendMessage (cbox.mask[1 + plus].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        aufUeandDrk = 0;
          }
          else
          {
                        aufUeandDrk  = 1;
          }
          if (syskey == KEYCR)
          {
                       aufUeandDrk  = 1;
                       SendMessage (cbox.mask[1 + plus].feldid, BM_SETCHECK,
                       aufUeandDrk, 0l);
                       break_enter ();
          }
          BOOL stat = aufUeandDrk;
          UnsetBox (1 + plus);
          aufUeandDrk = stat;
          aufBest = 0;
          return 1;
}

int SetLsUeb (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;
          int plus = 0;
          if (KompAuf::WithAufb) plus = 1;

          if (SendMessage (cbox.mask[2 + plus].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       aufLsUeb = 0;
          }
          else
          {
                       aufLsUeb = 1;
          }
          if (syskey == KEYCR)
          {
                       aufLsUeb = 1;
                       SendMessage (cbox.mask[2 + plus].feldid, BM_SETCHECK,
                       aufLsUeb, 0l);
                       break_enter ();
          }
          BOOL stat = aufLsUeb;
          UnsetBox (2 + plus);
          aufLsUeb = stat;
          aufBest = 0;
          return 1;
}

int SetLsKomp (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;
          int plus = 0;
          if (KompAuf::WithAufb) plus = 1;

          if (SendMessage (cbox.mask[3 + plus].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       aufLsKomp = 0;
          }
          else
          {
                       aufLsKomp = 1;
          }
          if (syskey == KEYCR)
          {
                       aufLsKomp = 1;
                       SendMessage (cbox.mask[3 + plus].feldid, BM_SETCHECK,
                       aufLsUeb, 0l);
                       break_enter ();
          }
          BOOL stat = aufLsKomp;
          UnsetBox (3 + plus);
          aufLsKomp = stat;
          aufBest = 0;
          return 1;
}




int SetLsUeb0 (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
		  int plus = 0;
		  if (KompAuf::WithAufb) plus = 1;

          aufLsUeb = 1;
          SendMessage (cbox.mask[2 + plus].feldid, BM_SETCHECK,
                       aufLsUeb, 0l);
          SetFocus (cbox.mask[2 + plus].feldid);
          SetCurrentField (2 + plus);
          UnsetBox (2 + plus);
          return 1;
}

int SetKlst0 (void)
/**
Uebergabe an Lieferschein setzen.
**/
{
          aufKliste = 1;
          SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       aufKliste, 0l);
          SetFocus (cbox.mask[0].feldid);
          SetCurrentField (0);
          UnsetBox (0);
          return 1;
}

int SetAufb0 (void)
/**
Uebergabe an Lieferschein setzen.
**/
{
          aufBest = 1;
          SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       aufBest, 0l);
          SetFocus (cbox.mask[1].feldid);
          SetCurrentField (1);
          UnsetBox (1);
          return 1;
}

int SetLsKomp0 (void)
/**
Uebergabe an Lieferschein setzen.
**/
{
		  int plus = 0;
		  if (KompAuf::WithAufb) plus = 1;

          aufLsKomp = 1;
          SendMessage (cbox.mask[3 + plus].feldid, BM_SETCHECK,
                       aufLsKomp, 0l);
          SetFocus (cbox.mask[3 + plus].feldid);
          SetCurrentField (3 + plus);
          UnsetBox (3 + plus);
          return 1;
}

int SetUeandDrk0 (void)
/**
Uebergabe und Druck setzen.
**/
{
		  int plus = 0;
		  if (KompAuf::WithAufb) plus = 1;

          aufUeandDrk  = 1;
          SendMessage (cbox.mask[1 + plus].feldid, BM_SETCHECK,
                       aufUeandDrk, 0l);
          SetFocus (cbox.mask[1 + plus].feldid);
          SetCurrentField (1 + plus);
          UnsetBox (1 + plus);
          return 1;
}


int BreakBox (void)
/**
EnterButton abbrechen.
**/
{
          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;
          syskey = KEYCR;
          break_enter ();
          return 1;
}

int BoxBreak (void)
/**
EnterButton abbrechen.
**/
{
	      int savecurrent;
		  HWND hWndA;
		  HWND hWndB;
static KompAuf AufKompl; //FS-328

		  savecurrent = currentfield;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;


          switch (syskey)
		  {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
				case KEY7 :
					if (aufBest || KompAuf::IsLlFormat ())
					   {
   					   Llgen.SetDrkWahl (AufKompl.ChoicePrinter); //FS-328
					   Llgen.DrkWahl () ;   
						   return 1;
					   }
	                   hWndA = GetActiveWindow ();
					   hWndB = AktivWindow;
  					   SetCurrentField (0);
					   ChoisePrinter0 ();
					   AktivWindow = hWndB;
					   SetActiveWindow (hWndA);
					   EnableWindow (GetParent (AktivWindow), FALSE);
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
					   return 1;
		  }

          if (current_form->mask[currentfield].BuId == KEY7)
		  {
				    if (aufBest || KompAuf::IsLlFormat ())
					{
   					   Llgen.SetDrkWahl (AufKompl.ChoicePrinter); //FS-328
					   Llgen.DrkWahl () ;   //061112
					   return 1;
					}
					syskey = KEY7;
                    hWndA = GetActiveWindow ();
			        hWndB = AktivWindow;
 					SetCurrentField (0);
					ChoisePrinter0 ();
				    AktivWindow = hWndB;
					SetActiveWindow (hWndA);
					EnableWindow (GetParent (AktivWindow), FALSE);
	                SetButtonTab (TRUE);
 					SetCurrentField (savecurrent);
					SetCurrentFocus (savecurrent);
					return 1;
		  }
		  else if (current_form->mask[currentfield].BuId == KEY5)
		  {
					syskey = KEY5;
					break_enter ();
					return 1;
		  }
		  else if (current_form->mask[currentfield].BuId == 107)
		  {
					syskey = KEY12;
					break_enter ();
					return 1;
		  }

          return 1;
}


void SelectAktCtr (void)
/**
Aktiven Button in EnterButton selectieren.
**/
{
        syskey = KEYCR;
        if (cbox.mask[currentfield].after)
        {
                       (*cbox.mask[currentfield].after) ();
        }
        syskey = KEYCR;
}

PROG_CFG *KompAuf::ProgCfg = NULL;
BOOL KompAuf::RunLs = FALSE;
BOOL KompAuf::SaveDefault = FALSE;
BOOL KompAuf::WithAufb = FALSE;
char KompAuf::aufbformat [20] = {"51400"};
char KompAuf::drformat [20] = {"51100"};
BOOL KompAuf::TxtByDefault = FALSE;

void KompAuf::SetLutzMdnPar (int par)
{
	    lutz_mdn_par = par;
}

void KompAuf::SetAbt (BOOL flag)
{
	    AbtRemoved = flag;
}

void KompAuf::SetDrkKun (BOOL flag)
{
	    DrkKunRemoved = flag;
}

void KompAuf::SetDrkExtra (BOOL flag)
{
	    DrkExtraRemoved = flag;
}
void KompAuf::SetDrkTeilSmt (BOOL flag)
{
	    DrkTeilSmtRemoved = flag;
}
void KompAuf::SetDrkKunGr2 (BOOL flag)
{
	    DrkKunGr2Removed = flag;
}

void KompAuf::SetExtraVon (int flag)
{
	    extra_von = flag;
}
void KompAuf::SetExtraBis (int flag)
{
	    extra_bis = flag;
}

void KompAuf::SetLuKun (BOOL flag)
{
	    LuKunRemoved = flag;
}

void KompAuf::SetSplitLs (int par)
/**
Parameter fuer Liefermenge setzen.
**/
{
	       SplitLs = min (2, par);
}

void KompAuf::SetLieferdatToKommdat (BOOL flag)
/**
**/
{
	       LieferdatToKommdat = flag;
}

void KompAuf::SetLiefMePar (int par)
/**
Parameter fuer Liefermenge setzen.
**/
{
	       lief_me_par = min (1, par);
}

void KompAuf::SetKlstPar (int par)
/**
Parameter fuer Liefermenge setzen.
**/
{
	       klst_dr_par = min (1, par);
}

void KompAuf::SetLog (int par)
/**
Parameter fuer Liefermenge setzen.
**/
{
	       with_log = min (1, par);
}

void KompAuf::SetKompDefault (int def_nr)
/**
Default fuer Auswahlmaske setzen.
**/
{
	       def_nr = min (max (0, def_nr), 4);
		   if (def_nr == 4 && !WithAufb)
		   {
			   def_nr = 0;
		   }
		   cbox.before = SetDefTab[def_nr];
}

void KompAuf::SetDrkLsSperr (BOOL sp)
{
	       drk_ls_sperr = min (1, max (0, sp));
}

void KompAuf::BearbKomplett (void)
/**
Auswahl aus Komplett bearbeiten.
**/
{
			if (aufk.auf_stat == 5) AuftoLsOld (); //Essig

          if (aufKliste)
          {
              K_ListeTS ();
			  if (SaveDefault)
			  {
				SetKompDefault (0);
			  }
		  }
          else if (aufBest)
          {
			  PrintAufbest ();
			  if (SaveDefault)
			  {
				SetKompDefault (4);
			  }
          }
          else if (aufUeandDrk)
          {
			  LsKomplett = 0;
			  DrkLsTS ();
			  if (SaveDefault)
			  {
				SetKompDefault (1);
			  }
          }
          else if (aufLsUeb)
          {
			  LsKomplett = 0;
              AuftoLsTS();
			  if (SaveDefault)
			  {
				SetKompDefault (2);
			  }
          }
          else if (aufLsKomp)
          {
			  LsKomplett = 1;
              AuftoLsTS();
			  if (SaveDefault)
			  {
				SetKompDefault (3);
			  }
          }
          return;
}


int KompAuf::SetKomplett (HWND hWnd)
/**
Komplett setzen.
**/
{

  	     KompParams ();

		 int cy = 11;
         save_fkt (5);
         save_fkt (7);
		 set_fkt (NULL, 7);
         set_fkt (KomplettEnd, 5);

		 aufbuttons = aufbuttons0;
		 if (WithAufb)
		 {
			 cbox.mask = _cboxex;
			 cbox.fieldanz = 8;
			 cy = 13;
			 aufbuttons = aufbuttons1;
		 }
         aufKliste = aufUeandDrk = 0;
         aufLsUeb = 0;
         aufLsKomp = 0;
         LsKomplett = 0;
         EnableWindows (hWnd, FALSE);
         SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
         SetCaption ("Auswahl f�r Komplett");
         if (EnterButtonWindow (hWnd, &cbox, 14, 9, cy, 62) == -1)
         {
			            SetActiveWindow (hWnd);
                        SetAktivWindow (hWnd);
                        EnableWindows (hWnd, TRUE);
                        restore_fkt (5);
                        return 0;
         }

         SetActiveWindow (hWnd);
         SetAktivWindow (hWnd);
         EnableWindows (hWnd, TRUE);
         BearbKomplett ();
         restore_fkt (5);
         restore_fkt (7);
         return 0;
}


long KompAuf::GenLsNr (short mdn, short fil)
/**
Lieferschein-Nummer generieren.
**/
{
	    char buffer [256];
        int dsqlstatus;
        extern  short sql_mode;
        short sqlm;
        int i = 0;


  	    sprintf (buffer, "Eine Lieferscheinnummer wird generiert");
        Mess.Message (buffer);
		Sleep (20);
		commitwork ();
        sqlm = sql_mode;
        sql_mode = 1;
		beginwork ();
		if (lutz_mdn_par)
		{
			mdn = fil = 0;
		}

//        dsqlstatus = AutoClass.nvholid (mdn, fil, "ls");
        dsqlstatus = AutoClass.nvholid (mdn, 0, "ls");
		commitwork ();
		if (dsqlstatus == -1)
		{
 			                sprintf (buffer, "Fehler bei nvholid");
                            Mess.Message (buffer);
 		                    Sleep (20);
			                beginwork ();
/*
					        DbClass.sqlin ((short *) &mdn, 1, 0);
					        DbClass.sqlin ((short *) &fil, 1, 0);
							dsqlstatus = DbClass.sqlcomm
								    ("delete from auto_nr where nr_nam = \"ls\" "
								     "and mdn = ? and fil = ?");
*/
					        DbClass.sqlin ((short *) &mdn, 1, 0);
							dsqlstatus = DbClass.sqlcomm
								    ("delete from auto_nr where nr_nam = \"ls\" "
								     "and mdn = ? and fil = 0");
							commitwork ();
                            if (dsqlstatus < 0)
							{
                                     disp_mess
										 ("Fehler bei der Generierung der Lieferschein-Nummere",2);
                                     return 0l;
							}
							dsqlstatus = 100;
		}


        i = 0;
        while (dsqlstatus < 0)
        {
			  sprintf (buffer, "Fehler 1 : Z�hler %d Maxwait MAXWAIT", i);
              Mess.Message (buffer);
              if (i == MAXWAIT)
			  {
                        disp_mess ("Fehler bei der Generierung der Lieferschein-Nummere",2);
                        return 0l;
			  }
              Sleep (50);
			  beginwork ();
//              dsqlstatus = AutoClass.nvholid (mdn, fil, "ls");
              dsqlstatus = AutoClass.nvholid (mdn, 0, "ls");
			  commitwork ();
              i ++;
        }

        if (dsqlstatus == 100)
        {
			  beginwork ();
/*
              dsqlstatus = AutoClass.nvanmprf (mdn,
                                     fil,
                                     "ls",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");
*/
              dsqlstatus = AutoClass.nvanmprf (mdn,
                                     0,
                                     "ls",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");
			  commitwork ();

              i = 0;
              while (dsqlstatus < 0)
              {
 			          sprintf (buffer, "Fehler 2 : Zaehler %d Maxwait MAXWAIT", i);
                      Mess.Message (buffer);
                      if (i == MAXWAIT)
					  {
                        disp_mess ("Fehler bei der Generierung der Lieferschein-Nummere",2);
                        return 0l;
					  }

                      Sleep (50);
					  beginwork ();
//                    dsqlstatus = AutoClass.nvholid (mdn, fil, "ls");
/*
                      dsqlstatus = AutoClass.nvanmprf (mdn,
                                                       fil,
                                                       "ls",
                                                       (long) 1,
                                                       (long) 999999,
                                                       (long) 10,
                                                        "");
*/
                      dsqlstatus = AutoClass.nvanmprf (mdn,
                                                       0,
                                                       "ls",
                                                       (long) 1,
                                                       (long) 999999,
                                                       (long) 10,
                                                        "");
					  commitwork ();
                      i ++;
              }

              if (dsqlstatus == 0)
              {
 					  beginwork ();
/*
                      dsqlstatus = AutoClass.nvholid (mdn,
                                                fil,
                                                "ls");
*/
                      dsqlstatus = AutoClass.nvholid (mdn,
                                                0,
                                                "ls");
					  commitwork ();
              }
         }

         i = 0;
         while (dsqlstatus < 0)
         {
			  sprintf (buffer, "Fehler 3 : Zaehler %d Maxwait MAXWAIT", i);
              Mess.Message (buffer);
              if (i == MAXWAIT) break;
              Sleep (50);
		      beginwork ();
//              dsqlstatus = AutoClass.nvholid (mdn, fil, "ls");
              dsqlstatus = AutoClass.nvholid (mdn, 0, "ls");
		      commitwork ();
              i ++;
         }

		 beginwork ();
         if (dsqlstatus < 0)
         {
             disp_mess ("Fehler bei der Generierung der Lieferschein-Nummere",2);
             return 0l;
         }
         sql_mode = sqlm;
   	     sprintf (buffer, "Lieferscheinnummer %ld generiert", auto_nr.nr_nr);
         Mess.Message (buffer);
         Sleep (20);
         return auto_nr.nr_nr;
}

long KompAuf::GenLsNr0 (short mdn, short fil)
/**
Lieferschein-Nummer generieren und pruefen, ob die Nummer schon existiert.
**/
{
	     static long ls_nr;
		 static short mdn_nr;
		 static short fil_nr;
		 static int tcursor = -1;
		 int dsqlstatus;
		 int i;


		 if (tcursor == -1)
		 {
			 if (lutz_mdn_par == 0)
			 {
			     DbClass.sqlin ((short *) &mdn_nr, 1, 0);
			     DbClass.sqlin ((short *) &fil_nr, 1, 0);
			     DbClass.sqlin ((long *)  &ls_nr, 2, 0);
			     tcursor = DbClass.sqlcursor ("select ls from lsk where mdn = ? and fil = ? and "
				                          "ls = ?");
			 }
			 else
			 {
			     DbClass.sqlin ((long *)  &ls_nr, 2, 0);
			     tcursor = DbClass.sqlcursor ("select ls from lsk where ls = ?");
			 }
		 }

		 i = 0;
		 mdn_nr = mdn;
		 fil_nr = fil;
		 ls_nr = GenLsNr (mdn, fil);
		 while (ls_nr == 0l)
		 {
             Sleep (50);
			 i ++;
             if (i == MAXWAIT) 
			 {
				 return 0l;
			 }
  		     ls_nr = GenLsNr (mdn, fil);
		 }
		 BOOL lsExist = TRUE;
		 while (lsExist)
		 {
			 DbClass.sqlopen (tcursor);
			 dsqlstatus = DbClass.sqlfetch (tcursor);
			 if (dsqlstatus == 100)
			 {
				 lsExist = FALSE;
			 }
			 else
			 {
				 Sleep (50);
				 i ++;
				 if (i == MAXWAIT)
				 {
					 return 0l;
				 }
  				 ls_nr = GenLsNr (mdn, fil);
				 while (ls_nr == 0l)
				 {
					Sleep (50);
					i ++;
					if (i == MAXWAIT) 
					{
						return 0l;
					}
  					ls_nr = GenLsNr (mdn, fil);
				 }
			 }
		 }
         return ls_nr;
}

double KompAuf::GetAufMeVgl (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;

		 lese_a_bas (lsp.a);
         einh_class.AktAufEinh (lsk.mdn, lsk.fil,
                                lsk.kun, lsp.a, lsp.me_einh_kun);
         einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                                lsk.kun, lsp.a, &keinheit);

         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                      lsp.auf_me_vgl = lsp.auf_me;
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     lsp.auf_me_vgl = lsp.auf_me * keinheit.inh;
         }
         return lsp.auf_me_vgl;
}

void KompAuf::GetEinhIst (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;

	 	 einh_class.SetAufEinh (1);
         einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                                lsk.kun, lsp.a, &keinheit);

         lsp.me_einh_ist = keinheit.me_einh_kun;
         lsp.inh_ist = keinheit.inh;
         strcpy (lsp.lief_me_bz_ist,  clipped (keinheit.me_einh_kun_bez));
}

long KompAuf::GetGruppe (void)
/**
Gruppe aus aufp holen.
**/
{
	     long gruppe;
		 int dsqlstatus;

		 gruppe = 0l;
	     DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
	     DbClass.sqlin ((short *) &aufk.fil, 1, 0);
	     DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
	     DbClass.sqlout ((long *)  &gruppe, 2, 0);
		 dsqlstatus = DbClass.sqlcomm ("select gruppe from aufp "
			                           "where mdn = ? "
									   "and   fil = ? "
									   "and   auf = ?");
		 return gruppe;
}

double KompAuf::GetGew (void)
/**
Gewicht aller Positionen mit me_einh = 2 holen.
**/
{
	     int cursor;
	     int dsqlstatus;
		 double gew;

		 DbClass.sqlin  ((short *) &lsk.mdn, 1, 0);
		 DbClass.sqlin  ((short *) &lsk.fil, 1, 0);
		 DbClass.sqlin  ((long *)  &lsk.auf, 2, 0);

		 DbClass.sqlout ((short *)  &lsp.me_einh, 1, 0);
		 DbClass.sqlout ((double *) &lsp.auf_me, 3, 0);
		 DbClass.sqlout ((double *) &lsp.a, 3, 0);
		 cursor = DbClass.sqlcursor ("select me_einh, auf_me, a from aufp "
			                         "where mdn = ? "
									 "and fil = ? "
									 "and auf = ?");
		 gew = 0.0;
         dsqlstatus = DbClass.sqlfetch (cursor);
		 while (dsqlstatus == 0)
		 {
			 if (lsp.me_einh == 2)
			 {
				 gew += GetAufMeVgl ();
			 }
             dsqlstatus = DbClass.sqlfetch (cursor);
		 }
		 DbClass.sqlclose (cursor);
		 return gew;
}


void KompAuf::AufktoLsk (int NurBerechnung)
/**
Auftragkopf uebertragen.
**/
{
         char datum [12];
		 int dsqlstatus;
         long sysdatum;


		 LskOK = FALSE;
	     WriteLogfile (" Auftrag %hd %hd %ld wird gelesen Kunde %ld Adresse %ld \n",
			 aufk.mdn, aufk.fil, aufk.auf, aufk.kun, aufk.adr);
	     dsqlstatus = ls_class.lese_aufk (aufk.mdn, aufk.fil, aufk.auf);
		 if (dsqlstatus)
		 {
 	         WriteLogfile (" Fehler beim Lesen Auftrag %ld\n", aufk.auf);
			 LskOK = FALSE;
		     return;
		 }

		 WriteLogfile ("Auftrag gefunden kun = %ld adr = %ld\n", aufk.kun, aufk.adr);

         sysdate (datum);
         sysdatum = dasc_to_long (datum);
         lsk.mdn = aufk.mdn;
         lsk.fil = aufk.fil;
         lsk.ls  = GenLsNr0 (aufk.mdn, aufk.fil);
		 if (lsk.ls <= 0l)
		 {
              WriteLogfile (" Fehler beim Generieren der Lieferscheinnummer\n");
			  disp_mess ("Es konnte keine Lieferscheinnummer generiert werden\n"
				         "Der Auftrag wurde nicht �bergeben", 2); 
              LskOK = FALSE;
		      return;
		 }
		 WriteLogfile (" Generierte Lieferscheinnummer %ld Kunde %ld Adresse %ld\n", lsk.ls,
			 aufk.kun,aufk.adr);
         lsk.auf = aufk.auf;
         lsk.adr = aufk.adr;
	     WriteLogfile (" �bergebene Adress-Nummer %ld\n", lsk.adr);
         lsk.kun_fil = aufk.kun_fil;
         lsk.kun = aufk.kun;
		 if (lutz_mdn_par)
		 {
			 lsk.gruppe = (short) GetGruppe ();
//			 lsk.gruppe = (long) GetGruppe (); Fuer spaeter wenn Datenbank OK
		 }
	     WriteLogfile (" �bergebene Kunden-Nummer %ld\n", lsk.kun);
         strcpy (lsk.feld_bz1, aufk.feld_bz1);
         strcpy (lsk.feld_bz2, aufk.feld_bz2);
         strcpy (lsk.feld_bz3, aufk.feld_bz3);
         lsk.lieferdat = aufk.lieferdat;
         strcpy (lsk.lieferzeit, aufk.lieferzeit);
         strncpy (lsk.hinweis, aufk.hinweis,sizeof (lsk.hinweis) - 1);
		 clipped(lsk.hinweis);
		 if (NurBerechnung)
		 {
                 lsk.ls_stat = 5; //EF-69
				 lsk.inka_nr = lsk.kun;   //EF-69  bei Essig inka_nr = kun
		 }
		 else if (LsKomplett)
		 {
                 lsk.ls_stat = 3;
		 }
		 else
		 {
                 lsk.ls_stat = 2;
		 }
         strcpy (lsk.kun_krz1, aufk.kun_krz1);
         Mess.Message ("Adresse wird gelesen");
 		 Sleep (20);
		 if (lsk.adr > 0)
		 {
                dsqlstatus = adr_class.lese_adr (lsk.adr);
		        if (dsqlstatus)
				{
			           LskOK = FALSE;
		               return;
				}
		        Mess.Message ("Adresse OK");
		 }
		 else
		 {
		        Mess.Message ("Adresse 0 : Diverser Kunde");
		 }
 		 Sleep (20);
         strcpy (lsk. partner, _adr.partner);
         lsk.pr_lst = kun.pr_lst;
         lsk.pr_stu = kun.pr_stu;
         lsk.vertr   = aufk.vertr;
         lsk.tou     = aufk.tou;
         lsk.tou_nr  = aufk.tou_nr;
         strcpy (lsk.adr_nam1, _adr.adr_nam1);
         strcpy (lsk.adr_nam2, _adr.adr_nam2);
         strcpy (lsk.pf, _adr.pf);
         strcpy (lsk.str, _adr.str);
         strcpy (lsk.plz, _adr.plz);
         strcpy (lsk.ort1, _adr.ort1);
         lsk.delstatus = 0;
         lsk.rech = 0l;
         lsk.zeit_dec = aufk.zeit_dec;
         lsk.kopf_txt = aufk.kopf_txt;
         lsk.fuss_txt = aufk.fuss_txt;
         strcpy (lsk.auf_ext, aufk.auf_ext);
         strcpy (lsk.pers_nam, aufk.pers_nam);
         lsk.komm_dat = aufk.komm_dat;
         lsk.fix_dat = aufk.fix_dat;
         lsk.best_dat = aufk.best_dat;
		 lsk.psteuer_kz = aufk.psteuer_kz;
		 strcpy (lsk.komm_name, aufk.komm_name);
/*
Achtung !! Diese Pr�fung wurde wieder entfernt. Die Firma Schiller arbeitet
zum Teil mit Kommisionierdatum < Tagesdatum 29.01.2004

         if (lsk.komm_dat < sysdatum)
         {
             lsk.komm_dat = lsk.lieferdat;
         }
*/

// Wenn der Parameter LieferdaToKommdat gesetzt ist wird das Kommisionierdatum immer
// auf das Lieferdatum gesetzt.

		 if (LieferdatToKommdat)
		 {
			 lsk.komm_dat = lsk.lieferdat;
		 }

         lsk.waehrung = aufk.waehrung;
         strcpy (lsk.ueb_kz, "N");
		 lsk.fak_typ = aufk.fak_typ;
		 lsk.ccmarkt = aufk.ccmarkt;
		 lsk.auf_art = aufk.auf_art;
	     if (LsKomplett || lief_me_par)
		 {
		           lsk.gew = GetGew ();
		 }
		 LskOK = TRUE;
}

void KompAuf::aufpttolspt (void)
/**
Positionstexte uebertragen.
**/
{
       int dsqlstatus;
       static int prep_ok = 0;
       int zeile;

	   if (TxtByDefault)
	   {
		if (auf_text_par && aufp.a_ers == 0.0) return;
	   }

       if (aufp.aufp_txt == 0l) return;
       if (lsp.lsp_txt == 0l) return;

	   if (wa_pos_txt)
	   {
               if (prep_ok == 0)
			   {
                    lsp_txt.posi = 0;
                    lsp_tclass.dbreadfirst ();
                    prep_ok = 1;
			   }
               aufp_txt.mdn  = aufk.mdn;
               aufp_txt.fil  = aufk.fil;
               aufp_txt.auf  = aufk.auf;
//150606               aufp_txt.posi = lsp.lsp_txt;
               aufp_txt.posi = aufp.aufp_txt;    //150606
               lsp_txt.mdn   = aufp_txt.mdn;
               lsp_txt.fil   = aufp_txt.fil;
               lsp_txt.ls    = lsk.ls;
//150606               lsp_txt.posi  = aufp_txt.posi;
               lsp_txt.posi  = lsp.lsp_txt;   //150606
               dsqlstatus = aufp_tclass.dbreadfirst ();
               while (dsqlstatus == 0)
			   {
                     lsp_txt.zei   = aufp_txt.zei;
                     strcpy (lsp_txt.txt, aufp_txt.txt);
                     lsp_tclass.dbupdate ();
                     dsqlstatus = aufp_tclass.dbread ();
			   }
	   }
	   else
	   {
               if (prep_ok == 0)
			   {
                    lspt.nr = 0;
                    lspt_class.dbreadfirst ();
                    prep_ok = 1;
			   }
		       aufpt.nr = aufp.aufp_txt;
			   lspt.nr = lsp.lsp_txt;
               zeile = 10;
               dsqlstatus = aufpt_class.dbreadfirst ();
               while (dsqlstatus == 0)
			   {
//                     lspt.zei     = aufpt.zei;
                     lspt.zei = zeile;
                     strcpy (lspt.txt, aufpt.txt);
                     lspt_class.dbupdate ();
                     dsqlstatus = aufpt_class.dbread ();
                     zeile += 10;
			   }
	   }
}

long KompAuf::GenLspTxt0 (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	int count;
	long nr;

	nr = 0l;
    nr = AutoNr.GetNrLs (nr);
	count = 0;
	while (nr == 0l)
	{
		         Sleep (10);
		         count ++;
				 if (count == 10) break;
                 nr = AutoNr.GetNrLs (nr);
	}
	return nr;
}

BOOL KompAuf::TxtNrExist (long nr)
{
	   int dsqlstatus;

	   if (wa_pos_txt)
	   {
			   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
			   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
			   DbClass.sqlin ((long *)  &nr, 2, 0);
			   dsqlstatus = DbClass.sqlcomm ("select posi from lsp_txt "
				                             "where mdn = ? "
											 "and fil = ? "
											 "and posi = ?");
	   }
	   else
	   {
			   DbClass.sqlin ((long *)  &nr, 2, 0);
			   dsqlstatus = DbClass.sqlcomm ("select nr from lspt where nr = ?");
	   }
	   if (dsqlstatus == 100) return FALSE;
	   return TRUE;
}


long KompAuf::GenLspTxt (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	 long nr;

	 while (TRUE)
	 {
		 nr = GenLspTxt0 ();
		 if (TxtNrExist (nr) == FALSE) break;
		 if (nr > 99999999)
		 {
			 return 0l;
		 }
	 }
	 return nr;
}


void KompAuf::angpttoaufpt (void)
/**
Positionstexte uebertragen.
**/
{
       int dsqlstatus;
       static int prep_ok = 0;
       int zeile;


       if (angp.angp_txt == 0l) return;
       if (aufp.aufp_txt == 0l) return;

       if (prep_ok == 0)
	   {
            aufpt.nr = 0;
            aufpt_class.dbreadfirst ();
            prep_ok = 1;
	   }
	   angpt.nr = angp.angp_txt;
	   aufpt.nr = aufp.aufp_txt;
       zeile = 10;
       dsqlstatus = angpt_class.dbreadfirst ();
       while (dsqlstatus == 0)
	   {
//                     lspt.zei     = aufpt.zei;
             aufpt.zei = zeile;
             strcpy (aufpt.txt, angpt.txt);
             aufpt_class.dbupdate ();
             dsqlstatus = angpt_class.dbread ();
             zeile += 10;
	   }
}


long KompAuf::GenAufpTxt0 (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	int count;
	long nr;

	nr = 0l;
    nr = AutoNr.GetNr (nr);
	count = 0;
	while (nr == 0l)
	{
		         Sleep (10);
		         count ++;
				 if (count == 10) break;
                 nr = AutoNr.GetNr (nr);
	}
	return nr;
}

BOOL KompAuf::AufpTxtNrExist (long nr)
{
	   int dsqlstatus;

	   if (wa_pos_txt)
	   {
			   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
			   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
			   DbClass.sqlin ((long *)  &nr, 2, 0);
			   dsqlstatus = DbClass.sqlcomm ("select posi from aufp_txt "
				                             "where mdn = ? "
											 "and fil = ? "
											 "and posi = ?");
	   }
	   else
	   {
			   DbClass.sqlin ((long *)  &nr, 2, 0);
			   dsqlstatus = DbClass.sqlcomm ("select nr from aufpt where nr = ?");
	   }
	   if (dsqlstatus == 100) return FALSE;
	   return TRUE;
}


long KompAuf::GenAufpTxt (void)
/**
Nummer fuer Lieferschein-Positionstexte generieren.
**/
{
	 long nr;

	 while (TRUE)
	 {
		 nr = GenAufpTxt0 ();
		 if (TxtNrExist (nr) == FALSE) break;
		 if (nr > 99999999)
		 {
			 return 0l;
		 }
	 }
	 return nr;
}



void KompAuf::SetBsdKz (int kz)
{
	      bsd_kz = kz;
}

/*
long KompAuf::GetLagerort (double a)
Lagerort aus a_lgr holen.
{
	     long lgr;

		 lgr = 0l;
		 DbClass.sqlin   ((short *)  &lsk.mdn, 1, 0);
		 DbClass.sqlin   ((double *) &a, 3, 0);
		 DbClass.sqlout  ((long *)  &lgr, 2, 0);
		 DbClass.sqlcomm ("select lgr from a_lgr where mdn = ? "
			              "and a = ?");
         return lgr;
}
*/


long KompAuf::GetLagerort (long akt_lager, double a)
/**
Lagerort aus a_lgr holen.
**/
{
	     long lgr;
         static long lgr_gr = 0l;
         static BOOL lgrOK = FALSE;
         char cfg_v [12];

		 lgr = 0l;

         if (akt_lager != 0)
         {
              lgr_gr = akt_lager;
              lgrOK = TRUE;
         }
         else if (! lgrOK)
         {
              if (ProgCfg != NULL &&
                  ProgCfg->GetGroupDefault ("lager", cfg_v) == TRUE)
              {
                     lgr_gr = atol (cfg_v);
              }
              lgrOK = TRUE;
         }

         if (lgr_gr > 0l)
         {
 		     DbClass.sqlin   ((short *)  &aufk.mdn, 1, 0);
		     DbClass.sqlin   ((double *) &a, 3, 0);
 		     DbClass.sqlin   ((long *)   &lgr_gr, 2, 0);
 		     DbClass.sqlout  ((long *)   &lgr, 2, 0);
   		     DbClass.sqlcomm ("select lgr.lgr from lgr, a_lgr "
                              "where lgr.mdn = ? "
                              "and a_lgr.mdn = lgr.mdn "
                              "and a_lgr.a = ? "
                              "and a_lgr.lgr = lgr.lgr "
			                  "and lgr.lgr_gr = ? ");
// Bei mehreren Eintr�gen in lgr hier Auswahl aufrufen 26.06.2002
         }

         if (lgr_gr == 0l)
         {
 		     DbClass.sqlin   ((short *)  &aufk.mdn, 1, 0);
		     DbClass.sqlin   ((double *) &a, 3, 0);
		     DbClass.sqlout  ((long *)  &lgr, 2, 0);
		     DbClass.sqlcomm ("select lgr from a_lgr where mdn = ? "
			                  "and a = ?");
// Bei mehreren Eintr�gen in a_lgr hier Auswahl aufrufen 26.06.2002
         }
         return lgr;
}

BOOL BsdArtikel (double a)
/**
Bestandskennzeichen in a_bas pruefen.
**/
{
	     char bsd_kz [2];

		 strcpy (bsd_kz, "N");
		 DbClass.sqlin ((double *) &a, 3, 0);
		 DbClass.sqlout ((char *) bsd_kz, 0, 2);
		 DbClass.sqlcomm ("select bsd_kz from a_bas where a = ?");
		 if (bsd_kz[0] == 'J') return TRUE;
		 return FALSE;
}


BOOL KompAuf::BsdArtikel (double a)
/**
Bestandskennzeichen in a_bas pruefen.
**/
{
	     char bsd_kz [2];

		 strcpy (bsd_kz, "N");
		 DbClass.sqlin ((double *) &a, 3, 0);
		 DbClass.sqlout ((char *) bsd_kz, 0, 2);
		 DbClass.sqlcomm ("select bsd_kz from a_bas where a = ?");
		 if (bsd_kz[0] == 'J') return TRUE;
		 return FALSE;
}

void KompAuf::BucheBsd (double a, double lief_me,  double pr_vk)
/**
Bestandsbuchung vorbereiten.
**/
{
	double buchme;
	char datum [12];
	long lgrort;

	if (bsd_kz == 0) return;
	if (BsdArtikel (a) == FALSE) return;

	buchme = lief_me;

	bsd_buch.nr  = lsk.ls;
	strcpy (bsd_buch.blg_typ, "WA");
	bsd_buch.mdn = lsk.mdn;
	bsd_buch.fil = lsk.fil;
	bsd_buch.kun_fil = lsk.kun_fil;
	bsd_buch.a   = a;
	sysdate (datum);
	bsd_buch.dat = dasc_to_long (datum);
	systime (bsd_buch.zeit);
	strcpy (bsd_buch.pers, sys_ben.pers);

/*
Lager holen
*/
	lgrort = GetLagerort (0l, a);

    sprintf (bsd_buch.bsd_lgr_ort, "%ld", lgrort);
	bsd_buch.qua_status = 0;
	bsd_buch.me = buchme * -1;
	bsd_buch.bsd_ek_vk = pr_vk;
    strcpy (bsd_buch.chargennr, aufp.ls_charge);
    strcpy (bsd_buch.ident_nr, "");
    strcpy (bsd_buch.herk_nachw, "");
    sprintf (bsd_buch.lief, "%ld", lsk.kun);
    bsd_buch.auf = lsk.auf;
    strcpy  (bsd_buch.verfall, "");
    bsd_buch.delstatus = 0;
    strcpy  (bsd_buch.err_txt, "511:30.06.2004");
	BsdBuch.dbinsert ();
}


void KompAuf::AufptoLsp (long posi)
/**
Aufposition uebertragen.
**/
{
      lsp.ls = lsk.ls;
      lsp.posi = posi;
      lsp.mdn  = aufp.mdn;;
      lsp.fil  = aufp.fil;
      {
             lsp.a    = aufp.a;
      }
      if (AUFPLIST::AErsInLs == FALSE)
      {
             lsp.a    = aufp.a_ers;
      }
      else
      {
             lsp.a    = aufp.a;
      }
      WriteLogfile ("   Artikel %.0lf\n", lsp.a);
      lsp.auf_me = aufp.auf_me;
      strcpy (lsp.auf_me_bz, aufp.auf_me_bz);
      strcpy (lsp.lief_me_bz, aufp.lief_me_bz);
      lsp.ls_vk_pr = aufp.auf_vk_pr;
      lsp.ls_lad_pr = aufp.auf_lad_pr;
      lsp.ls_vk_euro = aufp.auf_vk_euro;
      lsp.ls_lad_euro = aufp.auf_lad_euro;
      lsp.ls_vk_fremd = aufp.auf_vk_fremd;
      lsp.ls_lad_fremd = aufp.auf_lad_fremd;
	  lsp.aufschlag = aufp.aufschlag;
	  lsp.aufschlag_wert = aufp.aufschlag_wert;
	  lsp.teil_smt = aufp.teil_smt;
      lsp.delstatus = 0;
      lsp.tara = (double) 0.0;
	  if (TxtByDefault)
	  {
		if ((aufp.aufp_txt > 0l)  && (auf_text_par == 0))
		{
               lsp.lsp_txt = GenLspTxt ();
			   lsp.pos_txt_kz = aufp.pos_txt_kz;
		}
		else if ((aufp.aufp_txt > 0l) && (aufp.a_ers != 0.0))
		{
               lsp.lsp_txt = GenLspTxt ();
			   lsp.pos_txt_kz = aufp.pos_txt_kz;
		}
		else
		{
               lsp.lsp_txt = 0l;
		}
	  }
	  else
	  {
		  if ((aufp.pos_txt_kz == CControlPlugins::KommOnly) && (aufp.a_ers == 0.0))
		  {
			   lsp.lsp_txt = 0l;
			   lsp.pos_txt_kz = aufp.pos_txt_kz;
		  }
		  else if (aufp.aufp_txt > 0l)
		  {
			   lsp.lsp_txt = GenLspTxt ();
			   lsp.pos_txt_kz = aufp.pos_txt_kz;
		  }
		  else
		  {
			   lsp.lsp_txt = 0l;
			   lsp.pos_txt_kz = aufp.pos_txt_kz;
		  }
	  }

      lsp.sa_kz_sint = aufp.sa_kz_sint;

      lsp.prov_satz = aufp.prov_satz;
      lsp.me_einh_kun = aufp.me_einh_kun;
      lsp.me_einh     = aufp.me_einh;
      lsp.auf_me_vgl = GetAufMeVgl ();
	  if (LsKomplett )
	  {
                lsp.pos_stat = 3;
                lsp.lief_me = lsp.auf_me_vgl;
				BucheBsd (lsp.a, lsp.lief_me, lsp.ls_vk_pr);
	  }
	  else if (lief_me_par)
	  {
                lsp.pos_stat = 2;
                lsp.lief_me = lsp.auf_me_vgl;
	  }
	  else
	  {
                lsp.pos_stat = 2;
                lsp.lief_me = (double) 0.0;
	  }
      lsp.me_einh_kun1 = aufp.me_einh_kun1;
      lsp.auf_me1      = aufp.auf_me1;
      lsp.inh1         = aufp.inh1;
      lsp.me_einh_kun2 = aufp.me_einh_kun2;
      lsp.auf_me2      = aufp.auf_me2;
      lsp.inh2         = aufp.inh2;
      lsp.me_einh_kun3 = aufp.me_einh_kun3;
      lsp.auf_me3      = aufp.auf_me3;
      lsp.inh3         = aufp.inh3;
      lsp.rab_satz     = aufp.rab_satz;
      lsp.aufschlag    = aufp.aufschlag;
      lsp.aufschlag_wert  = aufp.aufschlag_wert;
	  lsp.a_grund      = aufp.a_grund;
	  strcpy (lsp.kond_art, aufp.kond_art);
	  lsp.posi_ext     = aufp.posi_ext;
      lsp.ls_pos_kz    = aufp.ls_pos_kz;
      strcpy (lsp.ls_charge, aufp.ls_charge);
	  lsp.me_ist = 0.0;
	  GetEinhIst ();
	  memcpy (&lsp.hbk_date, LONGNULL, sizeof (LONGNULL));
      if (lsp.ls_pos_kz == 0)
      {
	          lsp.ls_pos_kz    = 2;
      }
//      lsp.teil_smt    = aufp.teil_smt;
}

void KompAuf::AufptoUpdLsp (void)
/**
Aufposition uebertragen.
**/
{
      lsp.auf_me = aufp.auf_me;
      strcpy (lsp.auf_me_bz, aufp.auf_me_bz);
      strcpy (lsp.lief_me_bz, aufp.lief_me_bz);
      lsp.ls_vk_pr = aufp.auf_vk_pr;
      lsp.ls_lad_pr = aufp.auf_lad_pr;
      lsp.ls_vk_euro = aufp.auf_vk_euro;
      lsp.ls_lad_euro = aufp.auf_lad_euro;
      lsp.ls_vk_fremd = aufp.auf_vk_fremd;
      lsp.ls_lad_fremd = aufp.auf_lad_fremd;
	  lsp.aufschlag = aufp.aufschlag;
	  lsp.aufschlag_wert = aufp.aufschlag_wert;
	  lsp.teil_smt = aufp.teil_smt;
      lsp.delstatus = 0;

      lsp.prov_satz = aufp.prov_satz;
      lsp.me_einh_kun = aufp.me_einh_kun;
      lsp.me_einh     = aufp.me_einh;
      lsp.auf_me_vgl = GetAufMeVgl ();
      lsp.me_einh_kun1 = aufp.me_einh_kun1;
      lsp.auf_me1      = aufp.auf_me1;
      lsp.inh1         = aufp.inh1;
      lsp.me_einh_kun2 = aufp.me_einh_kun2;
      lsp.auf_me2      = aufp.auf_me2;
      lsp.inh2         = aufp.inh2;
      lsp.me_einh_kun3 = aufp.me_einh_kun3;
      lsp.auf_me3      = aufp.auf_me3;
      lsp.inh3         = aufp.inh3;
      lsp.rab_satz     = aufp.rab_satz;
      lsp.aufschlag    = aufp.aufschlag;
      lsp.aufschlag_wert  = aufp.aufschlag_wert;
	  lsp.a_grund      = aufp.a_grund;
	  strcpy (lsp.kond_art, aufp.kond_art);
	  lsp.posi_ext     = aufp.posi_ext;
	  if (LsInBearbeitung)
	  {
//                lsp.pos_stat = 3;
//                lsp.lief_me = lsp.auf_me_vgl;
	  }

}


void KompAuf::AuftoPid (void)
/**
Auftrags-Nummer in das Feld PID in aufp einsetzen.
**/
{
         DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufk.fil, 1, 0);
         DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
         DbClass.sqlcomm ("update aufp set pid = ? "
                          "where mdn = ? "
                          "and   fil = ? "
                          "and   auf = ?");
}

void KompAuf::InitPid (void)
/**
Auftrags-Nummer in das Feld PID in aufp einsetzen.
**/
{
         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufk.fil, 1, 0);
         DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
         DbClass.sqlcomm ("update aufp set pid = 0 "
                          "where mdn = ? "
                          "and   fil = ? "
                          "and   auf = ?");
}

void KompAuf::TeilSmttoLs (void)
/**
Positionen sortiert nach Teilsortiment.
Fuer jedes Teilsortiment eine eigen Lieferschein-Nr.
**/
{
         long posi = 0;
         int dsqlstatus;
         int teil_smt = -1;
		 int auf_stat;
  	     char buffer [80];
		 long maxpos;
		 int i;

	     WriteLogfile ("  Auftrag %ld wird Aufgeteilt\n", aufk.auf);
		 auf_stat = aufk.auf_stat;
         Getauf_a_sort ();
		 if (IsGroup == FALSE) beginwork ();
         ls_class.close_aufp_o ();
         AuftoPid ();

		 DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		 DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		 DbClass.sqlin ((long *)  &aufk.auf, 2,  0);
		 DbClass.sqlout ((long *) &maxpos,   2, 0);
		 dsqlstatus = DbClass.sqlcomm ("select count (*) from aufp "
			                           "where mdn = ? "
						               "and fil = ? "
						               "and auf = ?");
		 if (dsqlstatus != 0) return;

		 if (maxpos > aufptanz)
		 {
			 if (aufptab)
			 {
				 GlobalFree (aufptab);
				 aufptab = NULL;
			 }
			 aufptanz = maxpos;
			 aufptab = (struct AP *) GlobalAlloc (GMEM_FIXED,
				        sizeof (struct AP) * (aufptanz + 1));
			 if (aufptab == NULL)
			 {
				 disp_mess ("Kein Speicherplatz in TeilSmt-Zuordnung", 2);
				 return;
			 }
		 }



         if (auf_a_sort == 0)
         {
              dsqlstatus = ls_class.lese_aufp_o
                   (aufk.mdn, aufk.fil, aufk.auf, "order by teil_smt,"
                                                   "posi");
         }
         else if (auf_a_sort == 1)
         {
              dsqlstatus = ls_class.lese_aufp_o
                   (aufk.mdn, aufk.fil, aufk.auf, "order by teil_smt,a");
         }
         else if (auf_a_sort == 2)
         {
              dsqlstatus = ls_class.lese_aufp_o
                   (aufk.mdn, aufk.fil, aufk.auf, "order by teil_smt,dr_folge");
         }
         else
         {
              dsqlstatus = ls_class.lese_aufp_o
                   (aufk.mdn, aufk.fil, aufk.auf, "order by teil_smt,posi");
         }
		 i = 0;
         while (dsqlstatus == 0)
		 {
			 aufptab[i].mdn  = aufk.mdn;
			 aufptab[i].fil  = aufk.fil;
			 aufptab[i].auf  = aufk.auf;
			 aufptab[i].posi = aufp.posi;
			 aufptab[i].a    = aufp.a;
			 i ++;
			 if (i >aufptanz)
			 {
				 disp_mess ("Fehler bei der Artikel�bernahme in TeilSmt-Zuordnung", 2);
				 break;
			 }
             dsqlstatus = ls_class.lese_aufp_o ();
		 }
		 aufptanz = i;

		 for (i = 0; i < aufptanz; i ++)
         {
               dsqlstatus = ls_class.lese_aufp_ap
				             (aufptab[i].mdn, aufptab[i].fil, aufptab[i].auf,
                             aufptab[i].a, aufptab[i].posi);
               if (dsqlstatus != 0) continue;

               if (teil_smt != aufp.teil_smt)
               {
                   posi = 0;
				   lsk.teil_smt = aufp.teil_smt;
                   AufktoLsk (0);
				   if (LskOK == FALSE)
                   {
                           DbClass.sqlin ((short *) &auf_stat, 1, 0);
                           DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
                           DbClass.sqlin ((short *) &aufk.fil, 1, 0);
                           DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
                           DbClass.sqlcomm ("update aufk set auf_stat = ?, delstatus = 0 "
                                           "where mdn = ? "
                                           "and fil = ? "
                                           "and auf = ? ");
                           if (IsGroup == FALSE) rollbackwork ();
                           return;
                   }
                   if (lsk.ls <= 0) 
                   {
                           DbClass.sqlin ((short *) &auf_stat, 1, 0);
                           DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
                           DbClass.sqlin ((short *) &aufk.fil, 1, 0);
                           DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
                           DbClass.sqlcomm ("update aufk set auf_stat = ?, delstatus = 0 "
                                           "where mdn = ? "
                                           "and fil = ? "
                                           "and auf = ? ");
                           if (IsGroup == FALSE) rollbackwork ();
                           return;
                   }
                   ls_class.update_lsk (lsk.mdn, lsk.fil, lsk.ls);
                   teil_smt = aufp.teil_smt;
               }

               posi += 10;
               sprintf (buffer, "Auftrag %ld Position %ld Artikel %.0lf wird �bertragen", 
				                 aufp.auf, posi, aufp.a);
			   Mess.Message (buffer);
               AufptoLsp (posi);
               ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                    lsp.a, lsp.posi);
               aufpttolspt (); 
         }
         InitPid (); 
         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufk.fil, 1, 0);
         DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
         DbClass.sqlcomm ("update aufk set auf_stat = 5, delstatus = 0 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
		 if (IsGroup == FALSE)
		 {
                     commitwork ();
                     Mess.Message ("Der Auftrag wurde �bertragen");
 		             Sleep (20);
		 }
}

int KompAuf::Geteinz_ausw (void)
/**
**/
{
	      int einz_ausw;
		  int dsqlstatus;

		  DbClass.sqlin  ((short *) &aufk.mdn,  1, 0);
		  DbClass.sqlin  ((long *)  &aufk.kun,  2, 0);
		  DbClass.sqlout ((short *) &einz_ausw, 1, 0);
		  einz_ausw = 0;
          dsqlstatus = DbClass.sqlcomm ("select einz_ausw from kun "
			                            "where mdn = ? "
	 						            "and   kun = ? ");
          if (einz_ausw > 6) return 1;
		  return einz_ausw;
}

void KompAuf::AuftoLsOld (void)
//Auftrag in Lieferschein uebertragen. , wenn der Lieferschein schon existiert (nur Essig!)
{
         long posi = 0;
         int dsqlstatus;
  	     char buffer [80];
		 int auf_stat;
		 int dls = 0;

		 auf_stat = aufk.auf_stat;
		 if (aufk.auf_stat != 5)
		 {
			 return;
		 }
	     WriteLogfile (" Auftrag %ld wird �bergeben\n", aufk.auf);
         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufk.fil, 1, 0);
         DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
         DbClass.sqlout ((long *)  &dls, 2, 0);
         if (DbClass.sqlcomm ("select ls from lsk where mdn = ? and fil = ? and auf = ? ") != 0)
		 {
			 return;
		 }

		 beginwork ();
         ls_class.close_aufp_o ();
         if (auf_a_sort == 0)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by posi");
         }
         else if (auf_a_sort == 1)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by a");
         }
         else if (auf_a_sort == 2)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by dr_folge, a");
         }
         else
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by posi");
         }
         while (dsqlstatus == 0)
         {
               sprintf (buffer, "Auftrag %ld Position %ld Artikel %.0lf wird �bertragen", 
				                 aufp.auf, posi, aufp.a);
			   Mess.Message (buffer);

				if (ls_class.lese_lsp_a_lsp (aufk.mdn, aufk.fil, dls, aufp.a) == 100)
				{
				    DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
					DbClass.sqlin ((short *) &aufk.fil, 1, 0);
					DbClass.sqlin ((long *) &dls, 2, 0);
					DbClass.sqlout ((long *)  &lsp.posi, 2, 0);
					DbClass.sqlcomm ("select max(posi) from lsp where mdn = ? and fil = ? and ls = ? ");
					lsp.posi += 10;
					AufptoLsp (lsp.posi);
				}
				else
				{
					AufptoUpdLsp ();
					DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
					DbClass.sqlin ((short *) &aufk.fil, 1, 0);
					DbClass.sqlin ((long *)  &dls, 2, 0);
					DbClass.sqlin ((double *)  &aufp.a, 3, 0);
					if (DbClass.sqlcomm ("select mdn from lspeinzel where mdn = ? and fil = ? and ls = ? and a_lsp = ? and pos_stat = 3") == 100)
					{
	
						DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
						DbClass.sqlin ((short *) &aufk.fil, 1, 0);
						DbClass.sqlin ((long *)  &dls, 2, 0);
						DbClass.sqlin ((double *)  &aufp.a, 3, 0);
						DbClass.sqlcomm ("delete from lspeinzel where mdn = ? and fil = ? and ls = ? and a_lsp = ? and pos_stat < 3");
					}

				}
				ls_class.update_lsp (aufp.mdn, aufp.fil, dls, aufp.a, lsp.posi);
				aufpttolspt (); 
				dsqlstatus = ls_class.lese_aufp_o ();
         }
         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufk.fil, 1, 0);
         DbClass.sqlin ((long *)  &dls, 2, 0);
         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufk.fil, 1, 0);
         DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufk.fil, 1, 0);
         DbClass.sqlin ((long *)  &dls, 2, 0);
         if (DbClass.sqlcomm ("delete from lsp where mdn = ? and fil = ? and ls = ? and lief_me < 0.001 and pos_stat < 3 and a not in (select a from aufp where mdn = ? and fil = ? and auf = ? ) and a not in (select a_lsp from lspeinzel where mdn = ? and fil = ? and ls = ? and pos_stat = 3 )" ))
		 {
	         print_mess (2, "Der Lieferschein %ld konnte nicht korrigiert werden", dls);
			 rollbackwork ();
		 }
		 else
		 {
	         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
			DbClass.sqlin ((short *) &aufk.fil, 1, 0);
			DbClass.sqlin ((long *)  &dls, 2, 0);
			DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
			DbClass.sqlin ((short *) &aufk.fil, 1, 0);
			DbClass.sqlin ((long *)  &dls, 2, 0);
	         DbClass.sqlcomm ("delete from lspeinzel where mdn = ? and fil = ? and ls = ? and a_lsp not in (select a from lsp where mdn = ? and fil = ? and ls = ?)");
			 commitwork ();
		 }
}

void KompAuf::AuftoLs (int NurBerechnung)   //NurBerechnung EF-69
/**
Auftrag in Lieferschein uebertragen. Sortiert nach Positions-Nummer.
**/
{
         long posi = 0;
         int dsqlstatus;
  	     char buffer [80];
		 int auf_stat;

	     WriteLogfile (" Auftrag %ld wird �bergeben\n", aufk.auf);
		 auf_stat = aufk.auf_stat;
		 if (drk_ls_sperr && aufk.auf_stat < 4)
		 {
             if (IsGroup == FALSE) 
			 {
			         print_mess (2, "Auftrag %ld ist noch nicht gedruckt", aufk.auf);
			 }
		     WriteLogfile (" Auftrag %ld wurde noch nicht gedruckt\n", aufk.auf);
			 return;
		 }
         if (IsGroup == FALSE) 
		 {
             DbClass.sqlin ((short *) &aufk.mdn, 1, 0); 
             DbClass.sqlin ((short *) &aufk.fil, 1, 0); 
             DbClass.sqlin ((long *)  &aufk.auf, 2, 0); 
	 	     dsqlstatus = DbClass.sqlcomm ("select auf from aufk "
				                           "where mdn = ? "
							               "and   fil = ? "
						                   "and auf   = ? " 
										   "and delstatus = 0");
			 if (dsqlstatus != 0)
			 {
				 disp_mess ("Der Auftrag wird gerade bearbeitet", 2);
				 return;
			 }

             DbClass.sqlin ((short *) &aufk.mdn, 1, 0); 
             DbClass.sqlin ((short *) &aufk.fil, 1, 0); 
             DbClass.sqlin ((long *)  &aufk.auf, 2, 0); 
	 	     DbClass.sqlcomm ("update aufk set delstatus = -1 "
				              "where mdn = ? "
							  "and   fil = ? "
							  "and auf   = ?");
			 Mess.Message ("Der Auftrag wird in einen Lieferschein �bertragen");
		 }

         lsk.teil_smt = 0;
		 if (Geteinz_ausw () > 3 || SplitLs == 2)
		 {
             if ((SplitLs || aufk.auf_stat < 4))
			 {
                       TeilSmttoLs ();
                       DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
                       DbClass.sqlin ((short *) &aufk.fil, 1, 0);
                       DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
                       DbClass.sqlcomm ("update aufk set delstatus = 0 "
                              "where mdn = ? "
                              "and fil = ? "
                              "and auf = ? ");
                       return;
			 }
			 else
			 {
				       lsk.teil_smt = 0;
				       DbClass.sqlin  ((short *) &aufk.mdn, 1, 0);
                       DbClass.sqlin  ((short *) &aufk.fil, 1, 0);
                       DbClass.sqlin  ((long *)  &aufk.auf, 2, 0);
					   DbClass.sqlout ((short *) &lsk.teil_smt, 1, 0);
					   DbClass.sqlcomm ("select teil_smt from aufp "
                              "where mdn = ? "
                              "and fil = ? "
                              "and auf = ? ");
			 }
		 }

		 if (IsGroup == FALSE) beginwork ();

         Getauf_a_sort ();
         AufktoLsk (NurBerechnung);
  	     if (LskOK == FALSE)
         {
	        if (IsGroup == FALSE) rollbackwork ();
			beginwork ();
            DbClass.sqlin ((short *) &auf_stat, 1, 0);
            DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
            DbClass.sqlin ((short *) &aufk.fil, 1, 0);
            DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
            DbClass.sqlcomm ("update aufk set auf_stat = ?, delstatus = 0 "
                             "where mdn = ? "
                             "and fil = ? "
                             "and auf = ? ");
  		     commitwork ();

         }
         if (lsk.ls <= 0l) 
         {
  		     if (IsGroup == FALSE) rollbackwork ();
		 	 beginwork ();
             DbClass.sqlin ((short *) &auf_stat, 1, 0);
             DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
             DbClass.sqlin ((short *) &aufk.fil, 1, 0);
             DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
             DbClass.sqlcomm ("update aufk set auf_stat = ?, delstatus = 0 "
                              "where mdn = ? "
                              "and fil = ? "
                              "and auf = ? ");
  		     commitwork ();
             return;
         }
         if (IsGroup == FALSE) beginwork ();
         ls_class.update_lsk (lsk.mdn, lsk.fil, lsk.ls);
         ls_class.close_aufp_o ();
         AuftoPid (); 
         if (auf_a_sort == 0)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by posi");
         }
         else if (auf_a_sort == 1)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by a");
         }
         else if (auf_a_sort == 2)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by dr_folge, a");
         }
         else
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by posi");
         }
         while (dsqlstatus == 0)
         {
               posi += 10;
               sprintf (buffer, "Auftrag %ld Position %ld Artikel %.0lf wird �bertragen", 
				                 aufp.auf, posi, aufp.a);
			   Mess.Message (buffer);
               AufptoLsp (posi);
               ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                    lsp.a, lsp.posi);
               aufpttolspt (); 
               dsqlstatus = ls_class.lese_aufp_o ();
         }
         InitPid ();
         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufk.fil, 1, 0);
         DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
		 if (LskOK)
		 {
                DbClass.sqlcomm ("update aufk set auf_stat = 5, delstatus = 0 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
		 }
		 else
		 {
                 DbClass.sqlcomm ("update aufk set delstatus = 0 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
		 }
		 if (IsGroup == FALSE)
		 {
                commitwork ();
				if (RunLs && abfragejn (NULL, "Lieferscheine bearbeiten", "J"))
				{
					EnableWindow (hMainWindow, FALSE);
				    Text Command;
					Command.Format ("53100 %s LS=%hd;%hd;%ld", sys_ben.pers_nam, 
						                                       lsk.mdn, lsk.fil,lsk.ls);
					CProcess LsProc (Command);
					LsProc.Start ();
					LsProc.WaitForEnd ();
					EnableWindow (hMainWindow, TRUE);
					Mess.Message ("");
				}
				else
				{
					Mess.Message ("Der Auftrag wurde �bertragen");
				}
		 }
}


void KompAuf::ShowLocks (HWND hWnd)
/**
Auftraege, die nicht freigeben oder gedruckt werden konnten, anzeigen.
**/
{
	int wlen; 
	int i;
	char buffer [80];

    if (aufanzlock == 0) return;

    SetHLines (0);
    SetVLines (0);
	SetMenSelect (FALSE);
	wlen = 12;
	if (aufanzlock < 12) wlen = aufanzlock; 

	EnableWindows (hWnd, FALSE);
	OpenListWindowBu (hWnd, wlen, 40, 6, 20, 1);
    InsertListUb (NULL);
	SetWindowText (GethListBox (), "Nicht bearbeitete Auftr�ge");
	for (i = 0; i < aufanzlock; i ++)
	{
		sprintf (buffer,"    Auftrag %ld", auftablock [i]);
        InsertListRow (buffer);
	}
    EnterQueryListBox ();
	EnableWindows (hWnd, TRUE);
    SetMenSelect (TRUE);
}

void KompAuf::ShowNoDrk (HWND hWnd)
/**
Auftraege, die beim Freigeben noch nicht gedruckt waren, anzeigen.
**/
{
	int wlen; 
	int i;
	char buffer [80];

    if (drklsanz == 0) return;

    SetHLines (0);
    SetVLines (0);
	SetMenSelect (FALSE);
	wlen = 12;
	if (drklsanz < 12) wlen = drklsanz; 

	EnableWindows (hWnd, FALSE);
	OpenListWindowBu (hWnd, wlen, 40, 6, 20, 1);
    InsertListUb (NULL);
	SetWindowText (GethListBox (), "Nicht gedruckte Auftr�ge");
	for (i = 0; i < drklsanz; i ++)
	{
		sprintf (buffer,"    Auftrag %ld", drklstab [i]);
        InsertListRow (buffer);
	}
    EnterQueryListBox ();
	EnableWindows (hWnd, TRUE);
    SetMenSelect (TRUE);
}


void KompAuf::InitAufanz (void)
/**
aufanz auf 0 setzen.
**/
{
	aufanz = 0;
	aufanzlock = 0;
}

void KompAuf::KompParams (void)
/**
Systemparameter lesen.
**/
{
	static BOOL params_ok = FALSE;

	if (params_ok) return;

	params_ok = TRUE;
    strcpy (sys_par.sys_par_nam,"auf_text_par");
    if (sys_par_class.dbreadfirst () == 0)
	{
                 auf_text_par = atoi (sys_par.sys_par_wrt); 
	}
    strcpy (sys_par.sys_par_nam,"wa_pos_txt");
    if (sys_par_class.dbreadfirst () == 0)
	{
                 wa_pos_txt = atoi (sys_par.sys_par_wrt); 
	}
}

void KompAuf::AuftoLsGroup (char *where, int Sonderablauf)
/**
Mehrere Auftraege uebertragen.
**/
{
	int cursor;
	char sqls [1028];
	int i;
	int dsqlstatus;
	MSG msg;
	extern short sql_mode;
	short sqlm_save;
	int auf_stat;
	char datum [11];
	char zeit [11];

	CreateLogfile ();

	KompParams ();
    sysdate (datum);
	systime (zeit);
    WriteLogfile ("Bereichs�bergabe %s %s\n", datum, zeit);
	WriteLogfile ("%s\n", where);

    sqlm_save = sql_mode;
	sql_mode = 1;
	IsGroup = TRUE; 
    Mess.WaitWindow ("Die Auftr�ge werden �bernommen");
	TestFree ();
	if (aufanz == 0)
	{
// Gesperrte Auftrage merken

		     WriteLogfile ("Gesperrte Auftr�ge\n");
			 if (Sonderablauf == 1)
			 {
				sprintf (sqls, "select aufk.mdn,aufk.fil,aufk.auf from aufk,kun %s", where);
	            DbClass.sqlout ((short *) &aufk.mdn, 1, 0); 
		        DbClass.sqlout ((short *) &aufk.fil, 1, 0); 
				DbClass.sqlout ((long *) &aufk.auf, 2, 0); 
				strcat (sqls, " and aufk.mdn = kun.mdn and aufk.kun = kun.kun and aufk.delstatus != 0");
				cursor = DbClass.sqlcursor (sqls);
			 }
			 else
			 {
				sprintf (sqls, "select mdn,fil,auf from aufk %s", where);
	            DbClass.sqlout ((short *) &aufk.mdn, 1, 0); 
		        DbClass.sqlout ((short *) &aufk.fil, 1, 0); 
			    DbClass.sqlout ((long *) &aufk.auf, 2, 0); 
				strcat (sqls, " and delstatus != 0");
				cursor = DbClass.sqlcursor (sqls);
			 }
	         i = aufanzlock;
	         while (DbClass.sqlfetch (cursor) == 0)
			 {
		              mdntablock[i] = aufk.mdn;
		              filtablock[i] = aufk.fil;
		              auftablock[i] = aufk.auf;
					  WriteLogfile (" Auftrag %ld\n", aufk.auf);
			          if (i == 1999) break; 
			          i ++;
			 }
	         aufanzlock = i;
	         DbClass.sqlclose (cursor);

// Freie Auftraege merken

		     WriteLogfile ("Nicht gesperrte Auftr�ge\n");
			 if (Sonderablauf == 1)
			 {
				sprintf (sqls, "select aufk.mdn,aufk.fil,aufk.auf, tou.htou, tou.adr, aufk.lieferdat from aufk,kun,outer tou %s", where);     //EF-69   mit tou
				DbClass.sqlout ((short *) &aufk.mdn, 1, 0); 
				DbClass.sqlout ((short *) &aufk.fil, 1, 0); 
				DbClass.sqlout ((long *) &aufk.auf, 2, 0); 
				DbClass.sqlout ((long *) &tou.htou, 2, 0); 
				DbClass.sqlout ((long *) &tou.adr, 2, 0); 
				DbClass.sqlout ((long *) &aufk.lieferdat, 2, 0); 
				strcat (sqls, " and aufk.mdn = kun.mdn and aufk.kun = kun.kun and kun.tou = tou.tou and  aufk.delstatus = 0 and aufk.auf_stat < 5");
				cursor = DbClass.sqlcursor (sqls);
			 }
			 else
			 {
				sprintf (sqls, "select mdn,fil,auf from aufk %s", where);
				DbClass.sqlout ((short *) &aufk.mdn, 1, 0); 
				DbClass.sqlout ((short *) &aufk.fil, 1, 0); 
				DbClass.sqlout ((long *) &aufk.auf, 2, 0); 
				strcat (sqls, " and delstatus = 0 and auf_stat < 5");
				cursor = DbClass.sqlcursor (sqls);
			 }
	         i = 0;
	         while (DbClass.sqlfetch (cursor) == 0)
			 {
		              mdntab[i] = aufk.mdn;
		              filtab[i] = aufk.fil;
		              auftab[i] = aufk.auf;
		              tourtab[i] = tou.htou; //EF-69
		              kuntab[i] = tou.adr;//EF-69
					  lieferdattab[i] = aufk.lieferdat; //EF-69
                      DbClass.sqlin ((short *) &aufk.mdn, 1, 0); 
                      DbClass.sqlin ((short *) &aufk.fil, 1, 0); 
                      DbClass.sqlin ((long *)  &aufk.auf, 2, 0); 
					  WriteLogfile (" Auftrag %ld\n", aufk.auf);
					  DbClass.sqlcomm ("update aufk set delstatus = -1 "
						               "where mdn = ? "
									   "and   fil = ? "
									   "and auf   = ?");

			          if (i == 999) break; //EF-38
			          i ++;
			 }
	         aufanz = i;
	         DbClass.sqlclose (cursor);
	}

	else
	{
		     LockAuf ();
	}

    ls_class.close_aufk ();
	drklsanz = 0;
//	if (Sonderablauf == 1) InitKumAuf (); //EF-69
    for (i = 0; i < aufanz; i ++)
	{
	        beginwork ();
			aufk.mdn = mdntab[i];     
			aufk.fil = filtab[i];
			aufk.auf = auftab[i];
			dsqlstatus = ls_class.lock_aufk (aufk.mdn, aufk.fil, aufk.auf);
			if (dsqlstatus < 0)
			{
			    WriteLogfile (" Fehler beim Sperren von Auftrag %ld\n", aufk.auf);
				commitwork ();
				continue;
			}
		    dsqlstatus = ls_class.lese_aufk (aufk.mdn, aufk.fil, aufk.auf);
			if (dsqlstatus)
			{
			    WriteLogfile (" Fehler beim Lesen von Auftrag %ld\n", aufk.auf);
				commitwork ();
				continue;
			}
			if (aufk.auf_stat > 4)
			{
			    WriteLogfile (" Auftrag %ld ist schon �bergeben\n", aufk.auf);
				commitwork ();
				continue;
			}

			auf_stat = aufk.auf_stat;

			/****   16.10.2014  FS-544 ist hier falsch : das LskOK bezieht sich auf dem Auftrag davor !!! , wird  jetzt hinter AuftoLs() gesetzt
			if (LskOK)
			{
                    DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
                    DbClass.sqlin ((short *) &aufk.fil, 1, 0);
                    DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
                    DbClass.sqlcomm ("update aufk set auf_stat = 5 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
			}
			******/
			if (drk_ls_sperr == 0 || aufk.auf_stat > 3)
			{
			      sprintf (sqls, "Auftrag %ld", aufk.auf);
	 	          Mess.Message (sqls);
			}
			else
			{
				  drklstab[drklsanz] = aufk.auf;
				  drklsanz ++;
  			      WriteLogfile (" Auftrag %ld ist noch nicht gedruckt. Wird nicht �bergeben\n", aufk.auf);
				  continue;
			}
			if (aufk.auf_stat < 4)
			{
                     WriteLogfile (" Auftrag %ld ist noch nicht gedruckt. Wird �bergeben\n", 
						             aufk.auf);
			}
            if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
/***
  			 if (Sonderablauf == 1)  //EF-69
			 {
				 if (tourtab[i] > 0 && kuntab[i] > 0) 
				 {
					 FuelleKumAuf (mdntab[i], auftab[i], tourtab[i], kuntab[i], lieferdattab[i]);
//			         commitwork ();
//					 continue;
					 AuftoLs (1);
				 }
				 else AuftoLs (0); 
			 }
******/
			 AuftoLs (0);
			if (LskOK)   // 16.10.2014 FS-544   hier stehts richtig,  LskOK wird in AuftoLs (AuftoLsk) gesetzt 
			{
                    DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
                    DbClass.sqlin ((short *) &aufk.fil, 1, 0);
                    DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
                    DbClass.sqlcomm ("update aufk set auf_stat = 5 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
			}
 	        commitwork ();
	}
//	ErzeugeKumAuf (); //EF-69
	FreeAuf ();
	Mess.Message ("�bertragung OK");
	Mess.CloseWaitWindow ();
	IsGroup = FALSE;
    WriteLogfile (" �bergabe beendet\n");
	CloseLogfile ();
}

void KompAuf::LockAuf (void)
/**
Auftraege sperren.
**/
{
	int i;

    for (i = 0; i < aufanz; i ++)
	{
 			          aufk.mdn = mdntab[i];
			          aufk.fil = filtab[i];
			          aufk.auf = auftab[i];
                      DbClass.sqlin ((short *) &aufk.mdn, 1, 0); 
                      DbClass.sqlin ((short *) &aufk.fil, 1, 0); 
                      DbClass.sqlin ((long *)  &aufk.auf, 2, 0); 
					  DbClass.sqlcomm ("update aufk set delstatus = -1 "
						               "where mdn = ? "
									   "and   fil = ? "
									   "and auf   = ?");
	}
}


void KompAuf::TestFree (void)
/**
Testen, ob freie Auftraege inzwischen gesperrt sind.
**/
{
	int i;
    short mdntab0[1000];  //EF-38
    short filtab0[1000]; //EF-38
    long auftab0[1000]; //EF-38
    int dsqlstatus;
	int j;

	if (aufanz == 0) return;
    for (i = 0, j = 0; i < aufanz; i ++)
	{
 			          aufk.mdn = mdntab[i];
			          aufk.fil = filtab[i];
			          aufk.auf = auftab[i];
                      DbClass.sqlin ((short *) &aufk.mdn, 1, 0); 
                      DbClass.sqlin ((short *) &aufk.fil, 1, 0); 
                      DbClass.sqlin ((long *)  &aufk.auf, 2, 0); 
					  dsqlstatus = DbClass.sqlcomm ("select auf from aufk "
						                            "where mdn = ? "
									                "and   fil = ? "
									                "and auf   = ? "
									                "and delstatus = 0");
					  if (dsqlstatus == 0)
					  {
						  mdntab0[j]  = mdntab[i];
						  filtab0[j]  = filtab[i];
						  auftab0[j]  = auftab[i];
						  j ++;
					  }
					  else
					  {
		                  mdntablock[aufanzlock] = mdntab[i];
		                  filtablock[aufanzlock] = filtab[i];
		                  auftablock[aufanzlock] = auftab[i];
						  aufanzlock ++;
					  }
	}
	if (j == aufanz) return;
	aufanz = j;
	for (i = 0; i < aufanz; i ++)
	{
			  mdntab[i]  = mdntab0[i];
			  filtab[i]  = filtab0[i];
			  auftab[i]  = auftab0[i];
	}
}

		  
void KompAuf::FreeAuf (void)
/**
Gesperrte Auftraege freigeben.
**/
{
	int i;
	int dsqlstatus;
	extern short sql_mode;

    sql_mode = 1;
    for (i = 0; i < aufanz; i ++)
	{
 			          aufk.mdn = mdntab[i];
			          aufk.fil = filtab[i];
			          aufk.auf = auftab[i];
                      DbClass.sqlin ((short *) &aufk.mdn, 1, 0); 
                      DbClass.sqlin ((short *) &aufk.fil, 1, 0); 
                      DbClass.sqlin ((long *)  &aufk.auf, 2, 0); 
					  dsqlstatus = DbClass.sqlcomm ("update aufk set delstatus = 0 "
						                            "where mdn = ? "
									                "and   fil = ? "
									                "and auf   = ?");
					if (dsqlstatus < 0)
					{
						print_mess (2, "Achtung !!\n"
							           "Auftrag %ld kann nicht zur�ckgesetzt werden",
									    aufk.auf);
					}
	}
	sql_mode = 0;
	aufanz = 0;
}

void KompAuf::Getauf_a_sort (void)
{
    static BOOL ParOK = 0;

    if (ParOK) return;

    ParOK = 1;
    auf_a_sort = 0;
    strcpy (sys_par.sys_par_nam,"auf_a_sort");
    if (sys_par_class.dbreadfirst () == 0)
    {
        auf_a_sort = atoi (sys_par.sys_par_wrt);
    }
}

void KompAuf::K_Liste_Direct (void)
/**
Kommissionierliste ohne Listgenerator drucken.
**/
{
     char command [512];
	 
	 sprintf (command, "rswrun aufdr 1 %hd %hd %ld %ld 1 9 01.01.1990 31.12.2099",
		                aufk.mdn, aufk.fil, aufk.auf, aufk.auf);
	 ProcWaitExec (command, SW_SHOWNORMAL, -1, 0, -1, 0); 
	CreateLogfile ();
	WriteLogfile ("Auftrag %ld wurde �ber Direktdruck gedruckt\n", aufk.auf);
	CloseLogfile ();
}
     

void KompAuf::K_Liste_Fax (void)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];
	HWND akthWnd;

	akthWnd = GetActiveWindow ();
	SetAusgabe (FAX_OUT);
    if (syskey == KEY5) return;
    if (IsGroup == FALSE) commitwork ();
    if (LeseFormat (drformat) == FALSE)
    {
		           return;
    }


    sprintf (valfrom, "%hd", aufk.mdn);
    sprintf (valto, "%hd", aufk.mdn);
    FillFormFeld ("aufk.mdn", valfrom, valto);

    sprintf (valfrom, "%hd", aufk.fil);
    sprintf (valto, "%hd", aufk.fil);
    FillFormFeld ("aufk.fil", valfrom, valto);

    sprintf (valfrom, "%ld", aufk.auf);
    sprintf (valto, "%ld", aufk.auf);
    FillFormFeld ("aufk.auf", valfrom, valto);
    StartPrint ();

	SetActiveWindow (akthWnd);
	CreateLogfile ();
	WriteLogfile ("Auftrag %ld wurde gefaxt\n", aufk.auf);
	CloseLogfile ();
    InitPid ();
/*
    DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
    DbClass.sqlin ((short *) &aufk.fil, 1, 0);
    DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
    DbClass.sqlcomm ("update aufk set auf_stat = 4 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
    aufk.auf_stat = 4;
*/
//    if (IsGroup == FALSE) Mess.Message ("Der Auftrag wurde gefaxt");
}

void KompAuf::Print_Liste (void)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];

	SetAusgabe (PRINTER_OUT);
    AuftoPid ();

	if (klst_dr_par)
	{
		commitwork ();
		K_Liste_Direct ();
		beginwork ();
	}
	else
	{
		if(IsLlFormat ())
		{
			  K_LlListe ();
			  return;
		}

        if (LeseFormat (drformat) == FALSE)
		{
		           return;
		}

        sprintf (valfrom, "%hd", aufk.mdn);
        sprintf (valto, "%hd", aufk.mdn);
        FillFormFeld ("aufk.mdn", valfrom, valto);

        sprintf (valfrom, "%hd", aufk.fil);
        sprintf (valto, "%hd", aufk.fil);
        FillFormFeld ("aufk.fil", valfrom, valto);

        sprintf (valfrom, "%ld", aufk.auf);
        sprintf (valto, "%ld", aufk.auf);
        FillFormFeld ("aufk.auf", valfrom, valto);
        StartPrint ();
	}
}


BOOL aufp_to_lspeinzel () //Essig
{
        lspeinzel.mdn = aufp.mdn;
        lspeinzel.fil = aufp.fil;
        lspeinzel.ls = 0;
        lspeinzel.auf = aufp.auf;
        lspeinzel.a_lsp = aufp.a;
        lspeinzel.auf_me = aufp.auf_me;
        lspeinzel.pos_stat = 0;
        strcpy  (lspeinzel.ls_charge, aufp.ls_charge); 
		strcpy (lspeinzel.auf_me_bz, aufp.auf_me_bz);
//        strcpy  (lspeinzel.ls_ident, lsp.ls_ident);
        lspeinzel.lsp_txt = aufp.aufp_txt;
        lspeinzel.me_einh_kun = aufp.me_einh_kun;
        lspeinzel.me_einh     = aufp.me_einh;

        lspeinzel.sa_kz_sint   = aufp.sa_kz_sint;
        lspeinzel.prov_satz    = aufp.prov_satz;
//        lspeinzel.anz_einh    = lsp.anz_einh;

		if (lspeinzel.hbk_date <= 0l)
		{
			memcpy (&lspeinzel.hbk_date, LONGNULL, sizeof (LONGNULL));
		}
        lspeinzel.ls_vk_pr     = aufp.auf_vk_pr;
        lspeinzel.ls_lad_pr    = aufp.auf_lad_pr;
        lspeinzel.ls_vk_euro   = aufp.auf_vk_euro;
        lspeinzel.ls_vk_fremd  = aufp.auf_vk_fremd;
        lspeinzel.ls_lad_euro  = aufp.auf_lad_euro;
        lspeinzel.ls_lad_fremd = aufp.auf_lad_fremd;
        lspeinzel.rab_satz     = aufp.rab_satz;

        lspeinzel.lief_me1     = aufp.lief_me;
		strcpy (lspeinzel.kond_art, aufp.kond_art);
        lspeinzel.a_grund      = aufp.a_grund;
        lspeinzel.ls_pos_kz    = aufp.ls_pos_kz;
        lspeinzel.posi_ext     = aufp.posi_ext;
		strcpy (lspeinzel.lief_me_bz_ist, aufp.auf_me_bz);
        lspeinzel.me_einh_ist = aufp.me_einh;
		lspeinzel.me_ist = 0.0;
		lspeinzel.inh_ist = lsp.inh_ist;
		lspeinzel.pos_txt_kz = aufp.pos_txt_kz;
		lspeinzel.posi_ext = aufp.posi_ext;   //hier steht a_beilage , wenn eindeutig
		lspeinzel.prdk_typ = 0;

		lspeinzel.extra = 0;

		lspeinzel.sort = 0;
		// Jetzt Essig Spezial 
		//zuerst die hauptessen 
		if (_a_bas.a_grund == 5011) lspeinzel.sort = 1;
		else if (_a_bas.a_grund == 6011) lspeinzel.sort = 2;
		else if (_a_bas.a_grund == 7011) lspeinzel.sort = 3;
		else if (_a_bas.a_grund ==   11) lspeinzel.sort = 4;
		else if (_a_bas.a_grund == 5021) lspeinzel.sort = 5;
		else if (_a_bas.a_grund == 6021) lspeinzel.sort = 6;
		else if (_a_bas.a_grund == 7021) lspeinzel.sort = 7;
		else if (_a_bas.a_grund ==   21) lspeinzel.sort = 8;
		else if (_a_bas.a_grund == 5031) lspeinzel.sort = 9;
		else if (_a_bas.a_grund == 6031) lspeinzel.sort = 10;
		else if (_a_bas.a_grund == 7031) lspeinzel.sort = 11;
		else if (_a_bas.a_grund ==   31) lspeinzel.sort = 12;
		else if (_a_bas.a_grund == 5041) lspeinzel.sort = 13;
		else if (_a_bas.a_grund == 6041) lspeinzel.sort = 14;
		else if (_a_bas.a_grund == 7041) lspeinzel.sort = 15;
		else if (_a_bas.a_grund ==   41) lspeinzel.sort = 16;
		else lspeinzel.sort = 20; //dies ist der Rest 
		return TRUE;
}

BOOL lseinzel_to_tmp_catering () //Essig
{
        tmp_catering.mdn = lspeinzel.mdn;
        tmp_catering.fil = lspeinzel.fil;
        tmp_catering.ls = lspeinzel.ls;
        tmp_catering.auf = lspeinzel.auf;
        tmp_catering.a_lsp = lspeinzel.a_lsp;
        tmp_catering.a = lspeinzel.a;
        tmp_catering.auf_me = lspeinzel.auf_me;
		strcpy (tmp_catering.auf_me_bz, lspeinzel.auf_me_bz);
        tmp_catering.lsp_txt = lspeinzel.lsp_txt;
        tmp_catering.kun = lspeinzel.kun;
        tmp_catering.teil_smt = lspeinzel.teil_smt;
        tmp_catering.prdk_typ     = lspeinzel.prdk_typ;
        tmp_catering.posi     = lspeinzel.posi;
		tmp_catering.extra = lspeinzel.extra;
		tmp_catering.sort = lspeinzel.sort;
		return TRUE;
}


BOOL erzeuge_lspeinzel_auf (short Aufruftiefe) //Essig
{
	int dposi;
	short dextra;

//090712	lese_a_bas (lspeinzel.a_lsp);
	lese_a_bas (aufp.a); //090712
	aufp_to_lspeinzel ();
	lspeinzel.teil_smt = _a_bas.teil_smt;

	dposi = ls_einzel_class.lese_max_posi (lspeinzel.mdn,lspeinzel.fil,lspeinzel.ls,lspeinzel.auf);
	dposi += 10;

// Salatbuffet immer auf extra Kommissionierliste 
		lese_a_bas (lspeinzel.a_lsp);
		lspeinzel.extra = 0;
		dextra = 0;
		if (_a_bas.a_grund >= ls_einzel_class.a_grund_von_buffet && _a_bas.a_grund <= ls_einzel_class.a_grund_bis_buffet)
		{
			SalatBuffetImAuftrag = TRUE;
			dextra = 1;
			if (ls_einzel_class.Salat_extra > 0) lspeinzel.extra = ls_einzel_class.Salat_extra;
		}

	ls_einzel_class.update_lsp (lspeinzel.mdn,lspeinzel.fil,lspeinzel.ls, lspeinzel.auf, lspeinzel.a_lsp,lspeinzel.a_lsp, dposi);

	ls_einzel_class.RezAufloesung (Aufruftiefe,aufp.a, aufp.a, aufp.auf_me,dextra,lspeinzel.teil_smt);
	ls_einzel_class.CloseVarb ();
	return TRUE;
}

void KompAuf::GeneriereLSEinzel_aus_Auftrag (void) //Essig
{
	int dteil_smt;
	int dposi;
         int dsqlstatus = ls_class.lese_aufp_o
                   (aufk.mdn, aufk.fil, aufk.auf, "order by posi");

		SalatBuffetImAuftrag = FALSE;

         DbClass.sqlin ((short *) &aufp.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufp.fil, 1, 0);
         DbClass.sqlin ((long *) &aufp.auf, 2, 0);
         DbClass.sqlcomm ("delete from lspeinzel where mdn = ? and fil = ? and ls = 0 and auf = ?");
         while (dsqlstatus == 0)
		 {
//				if (ls_einzel_class.lese_lsp_a_lsp (aufp.mdn,aufp.fil,0,aufp.auf, aufp.a) == 100)   
				{
			  	    erzeuge_lspeinzel_auf(0);
					ls_einzel_class.aktiviere_salat(lspeinzel.mdn,lspeinzel.fil,lspeinzel.auf);
				}
		        dsqlstatus = ls_class.lese_aufp_o ();
		 }  

//		 ls_einzel_class.aktiviere_salat(lspeinzel.mdn,lspeinzel.fil,lspeinzel.auf);

         dsqlstatus = ls_einzel_class.lese_lspeinzel_auf (aufp.mdn,aufp.fil,0,aufp.auf);
		 dteil_smt = 0;
		 BOOL isExtra2 = FALSE;
		 BOOL isExtra1 = FALSE;
		 BOOL isExtra0 = FALSE;
         DbClass.sqlin ((short *) &aufp.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufp.fil, 1, 0);
         DbClass.sqlin ((long *) &aufp.auf, 2, 0);
         DbClass.sqlcomm ("delete from tmp_catering where mdn = ? and fil = ? and ls = 0 and auf = ?");

		 double a_lsp_save = 0.0;

        while (dsqlstatus == 0)
		{
				lseinzel_to_tmp_catering ();
				tmp_catering_class.dbupdate();

				if (lspeinzel.a_lsp != lspeinzel.a)
				{
						if (lspeinzel.extra == 0) isExtra0 = TRUE;
						if (lspeinzel.extra == 1) isExtra1 = TRUE;
						if (lspeinzel.extra == 2) isExtra2 = TRUE;
						tmp_catering_class.dbupdate();

				}
				 if (lspeinzel.a_lsp == lspeinzel.a)   //ist das Men�
				 {

					if (isExtra2 == TRUE && tmp_catering.extra != 2)  
					{
						dposi = tmp_catering.posi * 1000  + 2;
						tmp_catering.posi = dposi;
						tmp_catering.extra = 2;
						tmp_catering_class.dbupdate();
					}
					if (isExtra1 == TRUE && tmp_catering.extra != 1)  
					{
						dposi = tmp_catering.posi * 1000  + 1;
						tmp_catering.posi = dposi;
						tmp_catering.extra = 1;
						tmp_catering_class.dbupdate();
					}
					/**
					if (isExtra0 == TRUE && tmp_catering.extra != 0) 
					{
						dposi = tmp_catering.posi * 1000  + 0;
						tmp_catering.posi = dposi;
						tmp_catering.extra = 0;
						tmp_catering_class.dbupdate();
					}
					***/

					if (ls_einzel_class.ModusCatering == 2 && a_lsp_save != lspeinzel.a_lsp)  
					{
						a_lsp_save = lspeinzel.a_lsp;
						dposi = tmp_catering.posi * 1000  + 3;
						tmp_catering.posi = dposi;
						tmp_catering.extra = -1;
						tmp_catering_class.dbupdate();
					}
					isExtra1 = FALSE;
					isExtra2 = FALSE;
					isExtra0 = FALSE;
				}

/***

				if (dteil_smt != tmp_catering.teil_smt)
				{
					dteil_smt = tmp_catering.teil_smt;
					dposi = tmp_catering.posi;
			        int dsqlstatus1 = ls_einzel_class.lese_lspeinzel (aufp.mdn,aufp.fil,0,aufp.auf,"");
					while (dsqlstatus1 == 0)
					{
							lseinzel_to_tmp_catering ();
							tmp_catering.teil_smt = dteil_smt;
							tmp_catering.posi = tmp_catering.posi * 1000 + dposi ;
							tmp_catering_class.dbupdate();
							dsqlstatus1 = ls_einzel_class.lese_lspeinzel ();
					}

				}
*****/
		        dsqlstatus = ls_einzel_class.lese_lspeinzel_auf ();
		 }


}


void KompAuf::K_LlListe (void)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];

	SetAusgabe (PRINTER_OUT);
    AuftoPid ();
    if (syskey == KEY5) return;
    if (IsGroup == FALSE) commitwork ();
	if (ls_einzel_class.ModusCatering) 
	{
		beginwork ();
		GeneriereLSEinzel_aus_Auftrag(); // Essig
		commitwork ();
	}
    if (Llgen.LeseFormat (drformat) == FALSE)
//    if (Llgen.LeseFormat (aufbformat) == FALSE)
	{
           return;
	}



    sprintf (valfrom, "%hd", aufk.mdn);
    sprintf (valto, "%hd", aufk.mdn);
    Llgen.FillFormFeld ("aufk.mdn", valfrom, valto);

    sprintf (valfrom, "%hd", aufk.fil);
    sprintf (valto, "%hd", aufk.fil);
    Llgen.FillFormFeld ("aufk.fil", valfrom, valto);

    sprintf (valfrom, "%ld", aufk.auf);
    sprintf (valto, "%ld", aufk.auf);
    Llgen.FillFormFeld ("aufk.auf", valfrom, valto);

//    sprintf (valfrom, "%ld", ls_einzel_class.CateringExtra);
//    sprintf (valto, "%ld", ls_einzel_class.CateringExtra);
    sprintf (valfrom, "%ld", extra_von);
    sprintf (valto, "%ld", extra_bis);
    Llgen.FillFormFeld ("tmp_catering.extra", valfrom, valto);

    Llgen.StartPrint ();

	CreateLogfile ();
	WriteLogfile ("Auftrag %ld wurde gedruckt\n", aufk.auf);
	CloseLogfile ();
    InitPid ();
    DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
    DbClass.sqlin ((short *) &aufk.fil, 1, 0);
    DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
    DbClass.sqlcomm ("update aufk set auf_stat = 4 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? and auf_stat < 4");
    aufk.auf_stat = 4;
    if (IsGroup == FALSE) Mess.Message ("Der Auftrag wurde gedruckt");
}


BOOL KompAuf::IsLlFormat ()
{
	static int LlCursor = -1;
	static BOOL IsLlFormat = FALSE;
	static BOOL readed = FALSE;

	if (readed)
	{
		return IsLlFormat;
	}

    readed = TRUE;
	if (LlCursor == -1)
	{
		DbClass.sqlin (drformat, 0, sizeof (drformat));
//		DbClass.sqlin (aufbformat, 0, sizeof (aufbformat));
		LlCursor = DbClass.sqlcursor ("select * from nform_hea where form_nr =?");
	}
	if (LlCursor != -1)
	{
		DbClass.sqlopen (LlCursor);
		if (DbClass.sqlfetch (LlCursor) == 0)
		{
			IsLlFormat = TRUE;
		}
	}
    return IsLlFormat;
}


void KompAuf::K_Liste (void)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];
	/*
	extern short PartyMdn;
	if(aufk.mdn == PartyMdn)
	{
		Kom_Liste_Rab ();
		return;
	} */
	if(IsLlFormat ())
	{
		  K_LlListe ();
          return;
	}

	SetAusgabe (PRINTER_OUT);
    AuftoPid ();
    if (syskey == KEY5) return;
    if (IsGroup == FALSE) commitwork ();

	if (klst_dr_par)
	{
		K_Liste_Direct ();
	}
	else
	{
        if (LeseFormat (drformat) == FALSE)
		{
		           return;
		}

        sprintf (valfrom, "%hd", aufk.mdn);
        sprintf (valto, "%hd", aufk.mdn);
        FillFormFeld ("aufk.mdn", valfrom, valto);

        sprintf (valfrom, "%hd", aufk.fil);
        sprintf (valto, "%hd", aufk.fil);
        FillFormFeld ("aufk.fil", valfrom, valto);

        sprintf (valfrom, "%ld", aufk.auf);
        sprintf (valto, "%ld", aufk.auf);
        FillFormFeld ("aufk.auf", valfrom, valto);
        StartPrint ();
	}
	CreateLogfile ();
	WriteLogfile ("Auftrag %ld wurde gedruckt\n", aufk.auf);
	CloseLogfile ();
    InitPid ();
    DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
    DbClass.sqlin ((short *) &aufk.fil, 1, 0);
    DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
    DbClass.sqlcomm ("update aufk set auf_stat = 4 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? and auf_stat < 4");
    aufk.auf_stat = 4;
    if (IsGroup == FALSE) Mess.Message ("Der Auftrag wurde gedruckt");
}


void KompAuf::K_LlListeGroup (void)
/**
Kommissionierliste drucken.
**/
{
/*
    char valfrom [20];
    char valto [20];

	SetAusgabe (PRINTER_OUT);

    sprintf (valfrom, "%hd", mdn_von);
    sprintf (valto, "%hd", mdn_bis);
    Llgen.FillFormFeld ("aufk.mdn", valfrom, valto);
	WriteLogfile (" Mandant von %hd bis %hd\n", mdn_von, mdn_bis); 

    sprintf (valfrom, "%hd", fil_von);
    sprintf (valto, "%hd", fil_bis);
    Llgen.FillFormFeld ("aufk.fil", valfrom, valto);
	WriteLogfile (" Filiale von %hd bis %hd\n", fil_von, fil_bis); 


    sprintf (valfrom, "%ld", auf_von);
    sprintf (valto, "%ld",   auf_bis);
    Llgen.FillFormFeld ("aufk.auf", valfrom, valto);
	WriteLogfile (" Auftrag von %ld bis %ld\n", auf_von, auf_bis); 

    sprintf (valfrom, "%s", dat_von);
    sprintf (valto, "%s", dat_bis);
    Llgen.FillFormFeld ("aufk.lieferdat", valfrom, valto);
	WriteLogfile (" Lieferdatum von %s bis %s\n", dat_von, dat_bis); 

    sprintf (valfrom, "%hd", stat_von);
    sprintf (valto, "%hd", stat_bis);
    Llgen.FillFormFeld ("aufk.auf_stat", valfrom, valto);
	WriteLogfile (" Status von %hd bis %hd\n", stat_von, stat_bis); 

    sprintf (valfrom, "%ld", tou_von);
    sprintf (valto, "%ld",   tou_bis);
    Llgen.FillFormFeld ("aufk.tou", valfrom, valto);
	WriteLogfile (" Tour von %ld bis %ld\n", tou_von, tou_bis); 

	if (AbtRemoved == FALSE)
	{
             sprintf (valfrom, "%hd", abt_von);
             sprintf (valto, "%hd",   abt_bis);
             Llgen.FillFormFeld ("a_bas.abt", valfrom, valto);
	         WriteLogfile (" Abt von %hd bis %hd\n", abt_von, abt_bis); 
	}

	if (DrkKunRemoved == FALSE)
	{
             sprintf (valfrom, "%ld", kun_von);
             sprintf (valto, "%ld",   kun_bis);
             Llgen.FillFormFeld ("aufk.kun", valfrom, valto);
	         WriteLogfile (" Kunde von %ld bis %ld\n", kun_von, kun_bis); 
	}

    Llgen.StartPrint ();
*/
}


BOOL KompAuf::K_ListeGroup (short mdn_von, short mdn_bis,
							short fil_von, short fil_bis,
							long auf_von, long auf_bis,
							char *dat_von, char *dat_bis,
							short stat_von, short stat_bis,
							long tou_von, long tou_bis,
							short abt_von, short abt_bis,
							short teil_smt_von, short teil_smt_bis,
							short kun_gr2_von, short kun_gr2_bis,
							long kun_von,  long kun_bis, long Sonderablauf)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];
	long ldat_von, ldat_bis;
	int cursor;
	int cursor_kum;
	short mdn, fil;
	long auf;
	int wo_tou_kun = 0; 
	int i;
	int dsqlstatus;
	char datum [11];
	char zeit [11];
	extern short sql_mode;

	CreateLogfile ();

    sysdate (datum);
	systime (zeit);
    WriteLogfile ("Bereichsdruck Kommisionerliste %s %s\n", datum, zeit);

	Mess.WaitWindow ("Die Auftr�ge werden gedruckt");

	max_auf = auf_bis;
    ldat_von = dasc_to_long (dat_von);
    ldat_bis = dasc_to_long (dat_bis);

// Auftr�ge nach Teilsortimentsgruppen splitten 

	if (Sonderablauf == 1)
	{
		DbClass.sqlin ((short *) &mdn_von, 1, 0);
		DbClass.sqlin ((short *) &mdn_bis, 1, 0);
		DbClass.sqlin ((short *) &fil_von, 1, 0);
		DbClass.sqlin ((short *) &fil_bis, 1, 0);
		DbClass.sqlin ((long *)  &auf_von, 2, 0);
		DbClass.sqlin ((long *)  &auf_bis, 2, 0);
		DbClass.sqlin ((long *)  &kun_von, 2, 0);
		DbClass.sqlin ((long *)  &kun_bis, 2, 0);
		DbClass.sqlin ((long *)  &ldat_von, 2, 0);
		DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
		DbClass.sqlin ((short *) &stat_von, 1, 0);
		DbClass.sqlin ((short *) &stat_bis, 1, 0);
		DbClass.sqlin ((long *) &tou_von, 2, 0);
		DbClass.sqlin ((long *) &tou_bis, 2, 0);
	
	    DbClass.sqlout ((long *) &aufk.mdn, 1, 0);
	    DbClass.sqlout ((long *) &aufk.fil, 1, 0);
	    DbClass.sqlout ((long *) &aufk.auf, 2, 0);
	    cursor = DbClass.sqlcursor ("select aufk.mdn, aufk.fil, aufk.auf from aufk,kun "
                          "where aufk.mdn = kun.mdn and aufk.kun = kun.kun and aufk.mdn between ? and ?"
                          "and aufk.fil between ? and ? "
                          "and aufk.auf between  ? and ? "
                          "and aufk.kun between  ? and ? "
                          "and aufk.lieferdat between  ? and ? "
                          "and aufk.auf_stat between  ? and ? "
						  "and kun.tou between ? and ? "
						  "and aufk.delstatus = 0");
	}
	else
	{
		DbClass.sqlin ((short *) &mdn_von, 1, 0);
		DbClass.sqlin ((short *) &mdn_bis, 1, 0);
		DbClass.sqlin ((short *) &fil_von, 1, 0);
		DbClass.sqlin ((short *) &fil_bis, 1, 0);
		DbClass.sqlin ((long *)  &auf_von, 2, 0);
		DbClass.sqlin ((long *)  &auf_bis, 2, 0);
		DbClass.sqlin ((long *)  &kun_von, 2, 0);
		DbClass.sqlin ((long *)  &kun_bis, 2, 0);
		DbClass.sqlin ((long *)  &ldat_von, 2, 0);
		DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
		DbClass.sqlin ((short *) &stat_von, 1, 0);
		DbClass.sqlin ((short *) &stat_bis, 1, 0);
		DbClass.sqlin ((long *) &tou_von, 2, 0);
		DbClass.sqlin ((long *) &tou_bis, 2, 0);
	
	    DbClass.sqlout ((long *) &aufk.mdn, 1, 0);
	    DbClass.sqlout ((long *) &aufk.fil, 1, 0);
	    DbClass.sqlout ((long *) &aufk.auf, 2, 0);
	    cursor = DbClass.sqlcursor ("select aufk.mdn, aufk.fil, aufk.auf from aufk "
                          "where aufk.mdn between ? and ?"
                          "and aufk.fil between ? and ? "
                          "and aufk.auf between  ? and ? "
                          "and aufk.kun between  ? and ? "
                          "and aufk.lieferdat between  ? and ? "
                          "and aufk.auf_stat between  ? and ? "
						  "and aufk.tou between ? and ? "
						  "and aufk.delstatus = 0");
	}

	AufToSmtDrk (cursor, &auf_bis);

// Gesperrte Auftraege merken 

	if (Sonderablauf == 1)
	{
		DbClass.sqlin ((short *) &mdn_von, 1, 0);
		DbClass.sqlin ((short *) &mdn_bis, 1, 0);
		DbClass.sqlin ((short *) &fil_von, 1, 0);
		DbClass.sqlin ((short *) &fil_bis, 1, 0);
		DbClass.sqlin ((long *)  &auf_von, 2, 0);
		DbClass.sqlin ((long *)  &auf_bis, 2, 0);
		DbClass.sqlin ((long *)  &kun_von, 2, 0);
		DbClass.sqlin ((long *)  &kun_bis, 2, 0);
		DbClass.sqlin ((long *)  &ldat_von, 2, 0);
		DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
		DbClass.sqlin ((short *) &stat_von, 1, 0);
		DbClass.sqlin ((short *) &stat_bis, 1, 0);
		DbClass.sqlin ((long *) &tou_von, 2, 0);
		DbClass.sqlin ((long *) &tou_bis, 2, 0);
		DbClass.sqlin ((short *) &kun_gr2_von, 1, 0);
		DbClass.sqlin ((short *) &kun_gr2_bis, 1, 0);
	
	    DbClass.sqlout ((long *) &mdn, 1, 0);
	    DbClass.sqlout ((long *) &fil, 1, 0);
	    DbClass.sqlout ((long *) &auf, 2, 0);
	    cursor = DbClass.sqlcursor ("select aufk.mdn, aufk.fil, aufk.auf from aufk,kun "
                          "where aufk.mdn between ? and ?"
                          "and aufk.fil between ? and ? "
                          "and aufk.auf between  ? and ? "
                          "and aufk.kun between  ? and ? "
                          "and aufk.lieferdat between  ? and ? "
                          "and aufk.auf_stat between  ? and ? "
						  "and kun.tou between ? and ? "
						   "and kun.kun_gr2 between ? and ? "
						  "and aufk.delstatus != 0 and aufk.kun = kun.kun and aufk.mdn = kun.mdn ");
	}
	else
	{
		DbClass.sqlin ((short *) &mdn_von, 1, 0);
		DbClass.sqlin ((short *) &mdn_bis, 1, 0);
		DbClass.sqlin ((short *) &fil_von, 1, 0);
		DbClass.sqlin ((short *) &fil_bis, 1, 0);
		DbClass.sqlin ((long *)  &auf_von, 2, 0);
		DbClass.sqlin ((long *)  &auf_bis, 2, 0);
		DbClass.sqlin ((long *)  &kun_von, 2, 0);
		DbClass.sqlin ((long *)  &kun_bis, 2, 0);
		DbClass.sqlin ((long *)  &ldat_von, 2, 0);
		DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
		DbClass.sqlin ((short *) &stat_von, 1, 0);
		DbClass.sqlin ((short *) &stat_bis, 1, 0);
		DbClass.sqlin ((long *) &tou_von, 2, 0);
		DbClass.sqlin ((long *) &tou_bis, 2, 0);
		DbClass.sqlin ((short *) &kun_gr2_von, 1, 0);
		DbClass.sqlin ((short *) &kun_gr2_bis, 1, 0);
	
	    DbClass.sqlout ((long *) &mdn, 1, 0);
	    DbClass.sqlout ((long *) &fil, 1, 0);
	    DbClass.sqlout ((long *) &auf, 2, 0);
	    cursor = DbClass.sqlcursor ("select aufk.mdn, aufk.fil, aufk.auf from aufk,kun "
                          "where aufk.mdn between ? and ?"
                          "and aufk.fil between ? and ? "
                          "and aufk.auf between  ? and ? "
                          "and aufk.kun between  ? and ? "
                          "and aufk.lieferdat between  ? and ? "
                          "and aufk.auf_stat between  ? and ? "
						  "and aufk.tou between ? and ? "
						   "and kun.kun_gr2 between ? and ? "
						  "and aufk.delstatus != 0 and aufk.kun = kun.kun and aufk.mdn = kun.mdn ");
	}
	aufanzlock = 0;
	DbClass.sqlopen (cursor);
	dsqlstatus = DbClass.sqlfetch (cursor);
	if (dsqlstatus == 0)
	{
		    WriteLogfile  ("Gesperrte Auftr�ge\n");
	}
	while (dsqlstatus == 0)
	{
		    mdntablock[aufanzlock] = mdn;
		    filtablock[aufanzlock] = fil;
		    auftablock[aufanzlock] = auf;
			WriteLogfile ("   Auftrag %ld\n", auf);
			if (aufanz == 999) //EF-38
			{
				disp_mess ("Maximale Anzahl Auftr�ge �berschritten", 2);
				break;
			}
	        dsqlstatus = DbClass.sqlfetch (cursor);
			aufanzlock ++;
	}
	DbClass.sqlclose (cursor);


// Freie Auftraege merken


// EF-69b A
	short stat_bis1 = stat_bis;
	if (auf_bis - auf_von > 99999) 
	{
		if (kun_bis - kun_von > 99999)
		{
			if (stat_bis == 5)   //EF-69b    wg. wiederholtem Druck
			{
				stat_bis1 = 6;

			    DbClass.sqlin ((short *) &mdn_von, 1, 0);
				DbClass.sqlin ((short *) &mdn_bis, 1, 0);
				DbClass.sqlin ((short *) &fil_von, 1, 0);
				DbClass.sqlin ((short *) &fil_bis, 1, 0);
				DbClass.sqlin ((long *)  &ldat_von, 2, 0);
				DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
				DbClass.sqlin ((short *) &kun_gr2_von, 1, 0);
				DbClass.sqlin ((short *) &kun_gr2_bis, 1, 0);
				DbClass.sqlout ((short *) &mdn, 1, 0);
				DbClass.sqlout ((short *) &fil, 1, 0);
				DbClass.sqlout ((long *) &auf, 2, 0);
                cursor_kum = DbClass.sqlcursor  ("select mdn,fil,auf from aufk  "
                        "where aufk.mdn between ? and ? "
                          "and aufk.fil between ? and ? "
                          "and aufk.lieferdat between  ? and ? "
						  " and aufk.auf_art = 99 and aufk.auf_stat < 6 "
						  " and aufk.kun in (select kun from kun where "
						   "kun.kun_gr2 between ? and ? ) ");


				DbClass.sqlopen (cursor_kum);
				dsqlstatus = DbClass.sqlfetch (cursor_kum);
				int danz_kum = 0;
				if (dsqlstatus == 0)
				{
						WriteLogfile  ("L�schen kumulierter Auftr�ge\n");
				}
				while (dsqlstatus == 0)
				{
						mdn_kum[danz_kum] = mdn;
						fil_kum[danz_kum] = fil;
						auf_kum[danz_kum] = auf;
						WriteLogfile ("   Auftrag %ld\n", auf);
						if (danz_kum == 99) 
						{
							disp_mess ("Maximale Anzahl Auftr�ge �berschritten", 2);
							break;
						}
						dsqlstatus = DbClass.sqlfetch (cursor_kum);
						danz_kum ++;
				}
				DbClass.sqlclose (cursor_kum);
				for (i = 0; i < danz_kum; i ++)
				{
					mdn = mdn_kum[i];
					fil = fil_kum[i];
					auf = auf_kum[i];

				    DbClass.sqlin ((short *) &mdn, 1, 0);
					DbClass.sqlin ((short *) &fil, 1, 0);
					DbClass.sqlin ((long *)  &auf, 2, 0);
                     dsqlstatus = DbClass.sqlcomm ("delete from aufk  "
                          "where aufk.mdn = ? and aufk.fil = ? and aufk.auf = ? ");


				    DbClass.sqlin ((short *) &mdn, 1, 0);
					DbClass.sqlin ((short *) &fil, 1, 0);
					DbClass.sqlin ((long *)  &auf, 2, 0);
                     dsqlstatus = DbClass.sqlcomm ("delete from aufp  "
                          "where aufp.mdn = ? and aufp.fil = ? and aufp.auf = ? ");

				    DbClass.sqlin ((short *) &mdn, 1, 0);
					DbClass.sqlin ((short *) &fil, 1, 0);
					DbClass.sqlin ((long *)  &auf, 2, 0);
                     dsqlstatus = DbClass.sqlcomm ("delete from tmp_catering  "
                          "where tmp_catering.mdn = ? and tmp_catering.fil = ? and tmp_catering.ls = 0 and tmp_catering.auf = ? ");

				    DbClass.sqlin ((short *) &mdn, 1, 0);
					DbClass.sqlin ((short *) &fil, 1, 0);
					DbClass.sqlin ((long *)  &auf, 2, 0);
                     dsqlstatus = DbClass.sqlcomm ("delete from lspeinzel  "
                          "where lspeinzel.mdn = ? and lspeinzel.fil = ? and lspeinzel.ls = 0 and lspeinzel.auf = ? ");
				}
			}
		}
	}
// EF-69b E
	if (Sonderablauf== 1)
	{
	    DbClass.sqlin ((short *) &mdn_von, 1, 0);
		DbClass.sqlin ((short *) &mdn_bis, 1, 0);
		DbClass.sqlin ((short *) &fil_von, 1, 0);
		DbClass.sqlin ((short *) &fil_bis, 1, 0);
		DbClass.sqlin ((long *)  &auf_von, 2, 0);
		DbClass.sqlin ((long *)  &auf_bis, 2, 0);
		DbClass.sqlin ((long *)  &kun_von, 2, 0);
		DbClass.sqlin ((long *)  &kun_bis, 2, 0);
		DbClass.sqlin ((long *)  &ldat_von, 2, 0);
		DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
		DbClass.sqlin ((short *) &stat_von, 1, 0);
		DbClass.sqlin ((short *) &stat_bis1, 1, 0);
		DbClass.sqlin ((long *) &tou_von, 2, 0);
		DbClass.sqlin ((long *) &tou_bis, 2, 0);
		DbClass.sqlin ((short *) &kun_gr2_von, 1, 0);
		DbClass.sqlin ((short *) &kun_gr2_bis, 1, 0);
	
	    DbClass.sqlout ((long *) &mdn, 1, 0);
	    DbClass.sqlout ((long *) &fil, 1, 0);
	    DbClass.sqlout ((long *) &auf, 2, 0);

		//EF-69a erweitert um tou , aufk.lieferdat , 
				DbClass.sqlout ((long *) &tou.htou, 2, 0); 
				DbClass.sqlout ((long *) &tou.adr, 2, 0); 
				DbClass.sqlout ((long *) &aufk.lieferdat, 2, 0); 
				DbClass.sqlout ((long *) &aufk.tou_nr, 2, 0); 
				DbClass.sqlout ((short *) &aufk.auf_art, 1, 0); 
				DbClass.sqlout ((short *) &aufk.kun, 2, 0); 
				DbClass.sqlout ((short *) &kun.tou, 2, 0); 

				
	    cursor = DbClass.sqlcursor ("select aufk.mdn, aufk.fil, aufk.auf, tou.htou, tou.adr, aufk.lieferdat, aufk.tou_nr, aufk.auf_art, aufk.kun, kun.tou "
			                        "  from aufk,kun, outer tou "
                          "where aufk.mdn between ? and ? "
                          "and aufk.fil between ? and ? "
                          "and aufk.auf between  ? and ? "
                          "and aufk.kun between  ? and ? "
                          "and aufk.lieferdat between  ? and ? "
                          "and aufk.auf_stat between  ? and ? "
						  "and kun.tou between ? and ? "
						   "and kun.kun_gr2 between ? and ? "
						  "and aufk.delstatus = 0 and aufk.kun = kun.kun and aufk.mdn = kun.mdn "
						  "and kun.tou = tou.tou ");
	}
	else
	{
	    DbClass.sqlin ((short *) &mdn_von, 1, 0);
		DbClass.sqlin ((short *) &mdn_bis, 1, 0);
		DbClass.sqlin ((short *) &fil_von, 1, 0);
		DbClass.sqlin ((short *) &fil_bis, 1, 0);
		DbClass.sqlin ((long *)  &auf_von, 2, 0);
		DbClass.sqlin ((long *)  &auf_bis, 2, 0);
		DbClass.sqlin ((long *)  &kun_von, 2, 0);
		DbClass.sqlin ((long *)  &kun_bis, 2, 0);
		DbClass.sqlin ((long *)  &ldat_von, 2, 0);
		DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
		DbClass.sqlin ((short *) &stat_von, 1, 0);
		DbClass.sqlin ((short *) &stat_bis, 1, 0);
		DbClass.sqlin ((long *) &tou_von, 2, 0);
		DbClass.sqlin ((long *) &tou_bis, 2, 0);
		DbClass.sqlin ((short *) &kun_gr2_von, 1, 0);
		DbClass.sqlin ((short *) &kun_gr2_bis, 1, 0);
	
	    DbClass.sqlout ((long *) &mdn, 1, 0);
	    DbClass.sqlout ((long *) &fil, 1, 0);
	    DbClass.sqlout ((long *) &auf, 2, 0);
	    cursor = DbClass.sqlcursor ("select aufk.mdn, aufk.fil, aufk.auf from aufk,kun "
                          "where aufk.mdn between ? and ?"
                          "and aufk.fil between ? and ? "
                          "and aufk.auf between  ? and ? "
                          "and aufk.kun between  ? and ? "
                          "and aufk.lieferdat between  ? and ? "
                          "and aufk.auf_stat between  ? and ? "
						  "and aufk.tou between ? and ? "
						   "and kun.kun_gr2 between ? and ? "
						  "and aufk.delstatus = 0 and aufk.kun = kun.kun and aufk.mdn = kun.mdn ");
	}

	aufanz = 0;
	DbClass.sqlopen (cursor);
	dsqlstatus = DbClass.sqlfetch (cursor);
	if (dsqlstatus == 0)
	{
		    WriteLogfile  ("Nicht gesperrte Auftr�ge\n");
	}
	InitKumAuf ();
	int dkunde = 0;
	tou.htou = 0; tou.adr = 0;
	while (dsqlstatus == 0)
	{
		     if (kun.tou == 0) 
			 {
				tou.htou = 0; tou.adr = 0;
			 }
			 if (tou.htou < 0 || tou.htou > 99999 || tou.adr < 0 || tou.adr > 9999999)
			 {
				tou.htou = 0; tou.adr = 0;
			 }

		    mdntab[aufanz] = mdn;
		    filtab[aufanz] = fil;
		    auftab[aufanz] = auf;
		    aufart[aufanz] = aufk.auf_art;
			lieferdattab[aufanz] = aufk.lieferdat;
            tourtab[aufanz] = aufk.tou_nr; //EF-69a
            kuntourtab[aufanz] = tou.htou; //EF-69a
			if (tou.htou && tou.adr && Sonderablauf == 1)
			{
				dkunde = tou.adr + aufk.tou_nr;//EF-69a
				kuntab[aufanz] = dkunde;
                    DbClass.sqlin ((long *) &mdn, 1, 0);
                    DbClass.sqlin ((long *) &fil, 1, 0);
                    DbClass.sqlin ((long *) &aufk.kun, 2, 0);
                    dsqlstatus = DbClass.sqlcomm ("select mdn from wo_tou where mdn = ? and fil = ? and kun = ? ");
					if (dsqlstatus != 0)
					{
						print_mess (2,"F�r den Kunden  %ld fehlt der Eintrag f�r die Wochentouren!", aufk.kun);
						return FALSE;
					}
                    DbClass.sqlin ((long *) &mdn, 1, 0);
                    DbClass.sqlin ((long *) &fil, 1, 0);
                    DbClass.sqlin ((long *) &dkunde, 2, 0);
                    dsqlstatus = DbClass.sqlcomm ("select mdn from kun where mdn = ? and fil = ? and kun = ? ");
					if (dsqlstatus != 0)
					{
						print_mess (2,"Kunde %ld f�r kumulierte Auftr�ge muss erst angelegt werden!", dkunde);
						return FALSE;
					}
			}
			WriteLogfile ("   Auftrag %ld\n", auf);
			if (aufanz == 999) //EF-38
			{
 			    WriteLogfile ("Maximale Anzahl Auftr�ge �berschritten\n");
				break;
			}
			tou.htou = 0; tou.adr = 0;
	        dsqlstatus = DbClass.sqlfetch (cursor);
			aufanz ++;
	}
//	DbClass.sqlclose (cursor);

	//EF-69a A
  if (Sonderablauf == 1) //ESSIG
  {
		beginwork();
		for (i = 0; i < aufanz; i ++)
		{
		    mdn = mdntab[i];
			fil = filtab[i];
			auf = auftab[i];

			aufk.mdn = mdn;
			aufk.fil = fil;
			aufk.auf = auf;
			if (ls_class.lese_aufk (aufk.mdn, aufk.fil, aufk.auf) == 0)
			{
                if (kun_class.lese_kun (aufk.mdn,0,aufk.kun) == 0)
				{
	    			Auswerte_kun_gr2 (kun.kun_gr2); //Essig
					ls_class.update_aufk (aufk.mdn,aufk.fil,aufk.auf);  //setzten von feld_bz3 (== Formatnummer)
					GeneriereLSEinzel_aus_Auftrag();
				}
			}
		}
		commitwork();

	int dKumAuf = 0;
	for (i = 0; i < aufanz ; i++)
	{
		 if (aufart[i] == 99) continue;  //EF-69a  auf_art <> 99   keine Musterauftr�ge :  kululierte Auftrag darf nicht noch mal kumuliert werden
		 if (FuelleKumAuf (mdntab[i], auftab[i], kuntourtab[i], kuntab[i], lieferdattab[i], tourtab[i]) == TRUE)
		 {
			KumulierteAuftraege[dKumAuf] = auftab[i];
			dKumAuf ++;
		 }
	}
	ErzeugeKumAuf ();
	for (i = 0; i <1000 ; i++)   //die wirklichen kumulierten Auftr�ge werden  auf Status 6 gestellt,
		                        //damit sie nicht mehr zu Komissionierung �bergeben werden und nicht gedruckt werden
								// diese Auftr�ge m�ssen aber zum Rechnungslauf �bergeben werden
	{
		if (KumulierteAuftraege[i] == 0)  break;
		dKumAuf = KumulierteAuftraege[i];
 	                beginwork ();
                    DbClass.sqlin ((long *) &mdn, 1, 0);
                    DbClass.sqlin ((long *) &fil, 1, 0);
                    DbClass.sqlin ((long *) &dKumAuf, 2, 0);
                    dsqlstatus = DbClass.sqlcomm ("update aufk set auf_stat = 6 "
                             "where mdn = ? "
                             "and fil = ? "
                             "and auf = ? ");
					if (dsqlstatus < 0)
					{
						print_mess (2, "Achtung !!\n"
							           "Auftrag %ld kann nicht auf Status\n"
									   "gedruckt gesetzt werden", auf);
					}
	                commitwork ();
	}

	// jetzt muss das Array nochmal bef�llt werden
		aufanz = 0;
		beginwork ();
		commitwork ();
		DbClass.sqlopen (cursor);
		dsqlstatus = DbClass.sqlfetch (cursor);
		if (dsqlstatus == 0)
		{
				WriteLogfile  ("Nicht gesperrte Auftr�ge\n");
		}
		InitKumAuf ();
		tou.htou = 0; tou.adr = 0;
		while (dsqlstatus == 0)
		{
				mdntab[aufanz] = mdn;
				filtab[aufanz] = fil;
				auftab[aufanz] = auf;
				if (aufanz == 999) //EF-38
				{
					break;
				}
				dsqlstatus = DbClass.sqlfetch (cursor);
				aufanz ++;
		}
		DbClass.sqlclose (cursor);
  }

	//EF-69a E




	SetAusgabe (PRINTER_OUT);
//    AuftoPid ();
//    ChoisePrinter0 ();
    if (syskey == KEY5) return FALSE;

	if (DrkExtraRemoved == FALSE)  //Essig Catering
	{

// ZuTun tmp_catering muss hier noch generiert werden 
/**** EF-69a  hier raus, wurde vorverlegt da hier in auftab die kumulierten schon raus sind, w�rde sonst tmp_catering verf�lschen
		beginwork();
		for (i = 0; i < aufanz; i ++)
		{
		    mdn = mdntab[i];
			fil = filtab[i];
			auf = auftab[i];

			aufk.mdn = mdn;
			aufk.fil = fil;
			aufk.auf = auf;
			if (ls_class.lese_aufk (aufk.mdn, aufk.fil, aufk.auf) == 0)
			{
                if (kun_class.lese_kun (aufk.mdn,0,aufk.kun) == 0)
				{
	    			Auswerte_kun_gr2 (kun.kun_gr2); //Essig
					ls_class.update_aufk (aufk.mdn,aufk.fil,aufk.auf);  //setzten von feld_bz3 (== Formatnummer)
					GeneriereLSEinzel_aus_Auftrag();
				}
			}
		}
		commitwork();
*********EF-69a */

	    DbClass.sqlin ((short *) &mdn_von, 1, 0);
	    DbClass.sqlin ((short *) &mdn_bis, 1, 0);
	    DbClass.sqlin ((short *) &fil_von, 1, 0);
	    DbClass.sqlin ((short *) &fil_bis, 1, 0);
	    DbClass.sqlin ((long *)  &auf_von, 2, 0);
	    DbClass.sqlin ((long *)  &auf_bis, 2, 0);
	    DbClass.sqlin ((long *)  &kun_von, 2, 0);
	    DbClass.sqlin ((long *)  &kun_bis, 2, 0);
	    DbClass.sqlin ((long *)  &ldat_von, 2, 0);
	    DbClass.sqlin ((long *)  &ldat_bis, 2, 0);
	    DbClass.sqlin ((short *) &stat_von, 1, 0);
	    DbClass.sqlin ((short *) &stat_bis, 1, 0);
	    DbClass.sqlin ((long *) &tou_von, 2, 0);
	    DbClass.sqlin ((long *) &tou_bis, 2, 0);
	    DbClass.sqlin ((short *) &extra_von, 1, 0);
	    DbClass.sqlin ((short *) &extra_bis, 1, 0);
	    DbClass.sqlin ((short *) &kun_gr2_von, 1, 0);
		DbClass.sqlin ((short *) &kun_gr2_bis, 1, 0);
	
		char cformat [7];
	    DbClass.sqlout ((char *) cformat, 0, 6);
	    cursor = DbClass.sqlcursor ("select unique(aufk.feld_bz3) from aufk,kun,tmp_catering "
							"where aufk.mdn between ? and ?"
							"and aufk.fil between ? and ? "
							"and aufk.auf between  ? and ? "
							"and aufk.kun between  ? and ? "
							"and aufk.lieferdat between  ? and ? "
							"and aufk.auf_stat between  ? and ? "
							"and kun.tou between ? and ? "
							"and tmp_catering.extra between ? and ? "
							"and aufk.mdn = tmp_catering.mdn "
							"and aufk.fil = tmp_catering.fil "
							"and aufk.auf = tmp_catering.auf "
						   "and kun.kun_gr2 between ? and ? "
						  "and aufk.delstatus = 0 and aufk.kun = kun.kun and aufk.mdn = kun.mdn ");
	
		formatanz = 0;
		DbClass.sqlopen (cursor);
		dsqlstatus = DbClass.sqlfetch (cursor);
		if (dsqlstatus == 0)
		{
				WriteLogfile  ("Formate, die gedruckt werden (aufk.feld_bz3)\n");
		}
		while (dsqlstatus == 0)
		{
				strcpy(formattab[formatanz],cformat);
				WriteLogfile ("   Format %ld\n", atoi(cformat));
				if (formatanz == 20)
				{
	 			    WriteLogfile ("Maximale Anzahl Formate �berschritten\n");
					disp_mess ("Maximale Anzahl Formate �berschritten", 2);
					break;
				}
				dsqlstatus = DbClass.sqlfetch (cursor);
				formatanz ++;
		}
		DbClass.sqlclose (cursor);
	}
	else
	{
		formatanz = 1; 
//FS-328		strcpy(formattab[formatanz],drformat);
		strcpy(formattab[0],drformat); //FS-328
	}
	


    commitwork ();

	if(IsLlFormat ())
	{
	   for (i = 0; i < formatanz; i ++)
	   {
			if (Llgen.LeseFormat (formattab[i]) == FALSE)
			{
				return FALSE;
			}
			sprintf (valfrom, "%hd", mdn_von);
			sprintf (valto, "%hd", mdn_bis);
			Llgen.FillFormFeld ("aufk.mdn", valfrom, valto);
			WriteLogfile (" Mandant von %hd bis %hd\n", mdn_von, mdn_bis); 
	
			sprintf (valfrom, "%hd", fil_von);
			sprintf (valto, "%hd", fil_bis);
			Llgen.FillFormFeld ("aufk.fil", valfrom, valto);
			WriteLogfile (" Filiale von %hd bis %hd\n", fil_von, fil_bis); 
	
	
			sprintf (valfrom, "%ld", auf_von);
			sprintf (valto, "%ld",   auf_bis);
			Llgen.FillFormFeld ("aufk.auf", valfrom, valto);
			WriteLogfile (" Auftrag von %ld bis %ld\n", auf_von, auf_bis); 
	
			sprintf (valfrom, "%s", dat_von);
			sprintf (valto, "%s", dat_bis);
			Llgen.FillFormFeld ("aufk.lieferdat", valfrom, valto);
			WriteLogfile (" Lieferdatum von %s bis %s\n", dat_von, dat_bis); 
	
			sprintf (valfrom, "%hd", stat_von);
			sprintf (valto, "%hd", stat_bis);
			Llgen.FillFormFeld ("aufk.auf_stat", valfrom, valto);
			WriteLogfile (" Status von %hd bis %hd\n", stat_von, stat_bis); 
	
			if (Sonderablauf == 1)
			{
				sprintf (valfrom, "%ld", tou_von);
				sprintf (valto, "%ld",   tou_bis);
				Llgen.FillFormFeld ("kun.tou", valfrom, valto);
				WriteLogfile (" Tour von %ld bis %ld\n", tou_von, tou_bis); 
			}
			else
			{
				sprintf (valfrom, "%ld", tou_von);
				sprintf (valto, "%ld",   tou_bis);
				Llgen.FillFormFeld ("aufk.tou", valfrom, valto);
				WriteLogfile (" Tour von %ld bis %ld\n", tou_von, tou_bis); 
			}
	
			if (AbtRemoved == FALSE)
			{
					sprintf (valfrom, "%hd", abt_von);
					sprintf (valto, "%hd",   abt_bis);
					Llgen.FillFormFeld ("a_bas.abt", valfrom, valto);
					WriteLogfile (" Abt von %hd bis %hd\n", abt_von, abt_bis); 
			}
	
			if (DrkKunRemoved == FALSE)
			{
					sprintf (valfrom, "%ld", kun_von);
					sprintf (valto, "%ld",   kun_bis);
					Llgen.FillFormFeld ("aufk.kun", valfrom, valto);
					WriteLogfile (" Kunde von %ld bis %ld\n", kun_von, kun_bis); 
			}
			if (DrkKunGr2Removed == FALSE)
			{
					sprintf (valfrom, "%ld", kun_gr2_von);
					sprintf (valto, "%ld",   kun_gr2_bis);
					Llgen.FillFormFeld ("kun.kun_gr2", valfrom, valto);
					WriteLogfile (" Kundengruppe von %ld bis %ld\n", kun_gr2_von, kun_gr2_bis); 
			}
			if (DrkTeilSmtRemoved == FALSE)
			{
					sprintf (valfrom, "%ld", teil_smt_von);
					sprintf (valto, "%ld",   teil_smt_bis);
					Llgen.FillFormFeld ("a_bas.teil_smt", valfrom, valto);
					WriteLogfile (" Teilsortiment von %ld bis %ld\n", teil_smt_von, teil_smt_bis); 
			}
	
			if (DrkExtraRemoved == FALSE)
			{

				char *pdest;
				ls_einzel_class.ModusCatering = 0;


				strcpy (ptabn.ptbezk,formattab[i]);
			    DbClass.sqlout ((char *) ptabn.ptwer1, 0, 9);
			    DbClass.sqlout ((char *) ptabn.ptwer2, 0, 9);
			    DbClass.sqlin ((char *) ptabn.ptbezk, 0, 7);
			    cursor = DbClass.sqlcursor ("select ptwer1, ptwer2 from ptabn "
							"where ptitem = \"k_formular\" and ptbezk = ?");
	
				DbClass.sqlopen (cursor);
				dsqlstatus = DbClass.sqlfetch (cursor);
				if (dsqlstatus == 0)
				{
					pdest = strstr (ptabn.ptwer1, "atering");  //Suche nach Catering oder catering --> dann Modus Rezepturaufl�sung
					if (pdest)
					{
						ls_einzel_class.ModusCatering = atoi(ptabn.ptwer2);
					}
				}


				sprintf (valfrom, "%s", formattab[i]);
				sprintf (valto, "%s",   formattab[i]);
				Llgen.FillFormFeld ("aufk.feld_bz3", valfrom, valto);
				WriteLogfile (" feld_bz3 (formular) von %s bis %s\n", formattab[i], formattab[i]); 
				if (ls_einzel_class.ModusCatering) 
				{
					sprintf (valfrom, "%ld", extra_von);
					sprintf (valto, "%ld",   extra_bis);
					Llgen.FillFormFeld ("tmp_catering.extra", valfrom, valto);
					WriteLogfile (" Extra von %ld bis %ld\n", extra_von, extra_bis); 
				}

			}
	
			Llgen.SetDrkWahl (ChoicePrinter); //FS-328 
			Llgen.StartPrint (); //FS-328  ifornmane ist nicht best�ckt
		}
	   }
	else
	{
		if (LeseFormat (drformat) == FALSE)
		{
  			Mess.CloseWaitWindow ();
			return FALSE;
		}

		WriteLogfile ("�bergabewerte an Listgenerator\n");

		sprintf (valfrom, "%hd", mdn_von);
		sprintf (valto, "%hd", mdn_bis);
		FillFormFeld ("aufk.mdn", valfrom, valto);
		WriteLogfile (" Mandant von %hd bis %hd\n", mdn_von, mdn_bis); 

		sprintf (valfrom, "%hd", fil_von);
		sprintf (valto, "%hd", fil_bis);
		FillFormFeld ("aufk.fil", valfrom, valto);
		WriteLogfile (" Filiale von %hd bis %hd\n", fil_von, fil_bis); 


		sprintf (valfrom, "%ld", auf_von);
		sprintf (valto, "%ld",   auf_bis);
		FillFormFeld ("aufk.auf", valfrom, valto);
		WriteLogfile (" Auftrag von %ld bis %ld\n", auf_von, auf_bis); 

		sprintf (valfrom, "%s", dat_von);
		sprintf (valto, "%s", dat_bis);
		FillFormFeld ("aufk.lieferdat", valfrom, valto);
		WriteLogfile (" Lieferdatum von %s bis %s\n", dat_von, dat_bis); 

		sprintf (valfrom, "%hd", stat_von);
		sprintf (valto, "%hd", stat_bis);
		FillFormFeld ("aufk.auf_stat", valfrom, valto);
		WriteLogfile (" Status von %hd bis %hd\n", stat_von, stat_bis); 

		sprintf (valfrom, "%ld", tou_von);
		sprintf (valto, "%ld",   tou_bis);
		FillFormFeld ("aufk.tou", valfrom, valto);
		WriteLogfile (" Tour von %ld bis %ld\n", tou_von, tou_bis); 

		if (AbtRemoved == FALSE)
		{
				 sprintf (valfrom, "%hd", abt_von);
				 sprintf (valto, "%hd",   abt_bis);
				 FillFormFeld ("a_bas.abt", valfrom, valto);
				 WriteLogfile (" Abt von %hd bis %hd\n", abt_von, abt_bis); 
		}

		if (DrkKunRemoved == FALSE)
		{
				 sprintf (valfrom, "%ld", kun_von);
				 sprintf (valto, "%ld",   kun_bis);
				 FillFormFeld ("aufk.kun", valfrom, valto);
				 WriteLogfile (" Kunde von %ld bis %ld\n", kun_von, kun_bis); 
		}

		StartPrint ();
	}
	sql_mode = 1;
	for (i = 0; i < aufanz; i ++)
	{
		    mdn = mdntab[i];
			fil = filtab[i];
			auf = auftab[i];
			{
 	                beginwork ();
                    DbClass.sqlin ((long *) &mdn, 1, 0);
                    DbClass.sqlin ((long *) &fil, 1, 0);
                    DbClass.sqlin ((long *) &auf, 2, 0);
                    dsqlstatus = DbClass.sqlcomm ("update aufk set auf_stat = 4 "
                             "where mdn = ? "
                             "and fil = ? "
                             "and auf = ? "
							 "and auf_stat < 4");
					if (dsqlstatus < 0)
					{
						print_mess (2, "Achtung !!\n"
							           "Auftrag %ld kann nicht auf Status\n"
									   "gedruckt gesetzt werden", auf);
					}
	                commitwork ();
			}
	}
	sql_mode = 0;
	Mess.CloseWaitWindow ();
    Mess.Message ("Die Auftr�ge wurden gedruckt");
	CopyListe ();
	CloseLogfile ();
	return TRUE;
}


void KompAuf::K_ListeGroup0 (char *where)
/**
Mehrere Auftraege uebertragen.
**/
{
	int cursor;
	char sqls [1028];

    ChoisePrinter0 ();
	IsGroup = TRUE; 
	sprintf (sqls, "select mdn,fil,auf, tou, lieferdat from aufk %s", where);

    DbClass.sqlout ((short *) &aufk.mdn, 1, 0); 
    DbClass.sqlout ((short *) &aufk.fil, 1, 0); 
    DbClass.sqlout ((long *) &aufk.auf, 2, 0); 
    DbClass.sqlout ((long *) &aufk.tou, 2,0); 
    DbClass.sqlout ((long *) &aufk.lieferdat, 2, 0); 
	cursor = DbClass.sqlcursor (sqls);
	Mess.WaitWindow ("Die Auftr�ge werden gedruckt");
	beginwork ();
	while (DbClass.sqlfetch (cursor) == 0)
	{
		    ls_class.lese_aufk (aufk.mdn, aufk.fil, aufk.auf);
			sprintf (sqls, "Auftrag %ld", aufk.auf);
	 	    Mess.Message (sqls);
			K_Liste ();
	}
	commitwork ();
	Mess.CloseWaitWindow ();
	IsGroup = FALSE;
	DbClass.sqlclose (cursor);
}





/**
Fen_par fak_typ waehlen.
**/

static int nix       = 0;
static int bar       = 0;
static int anonym    = 0;

static int *fakbuttons [] = {&nix, &bar, &anonym, NULL};

static int SetNix (void);
static int SetBar (void);
static int SetAnonym (void);
static int BreakBox (void);
static int BoxBreak (void);

static int IsFakChoise;

static ITEM iNix      ("",  "Normalverkauf",           "", 0);
static ITEM iBar      ("",  "Barverkauf mit Adresse",  "", 0);
static ITEM iAnonym   ("",  "Barverkauf ohne Adresse", "", 0);

static int SetFakDef (void);

static field _fakbox [] = {
&iNix,                   26, 2, 1,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetNix, 101,
&iBar,                   26, 2, 3,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                              SetBar, 102,
/*
&iAnonym,                26, 2, 4,  6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             SetAnonym, 103,
*/
&iOK,                    15, 0, 7,  6,  0, "", BUTTON, 0, BreakBox, 107,
&iCancel,                15, 0, 7, 23, 0, "",  BUTTON, 0, BoxBreak, KEY5};

form fakbox = {4, 0, 0, _fakbox, 0, SetFakDef, 0, 0, NULL};


int SetFakDef (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          nix = 1;
          SendMessage (fakbox.mask[0].feldid, BM_SETCHECK,
                       nix, 0l);
          SetFocus (fakbox.mask[0].feldid);
          SetCurrentField (0);
          UnsetBox (0);
          return 1;
}


int FakEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

void UnsetFakBox (int pos)
/**
Button auf selectierte setzen. (ausser pos);
**/
{
        int i;

        for (i = 0; fakbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (fakbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *fakbuttons [i] = 0;
        }
}


int SetNix (void)
/**
K-Liste setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (fakbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       nix = 0;
          }
          else
          {
                       nix = 1;
          }
          if (syskey == KEYCR)
          {
                       nix = 1;
                       SendMessage (fakbox.mask[0].feldid, BM_SETCHECK,
                       nix, 0l);
                       break_enter ();
          }
          UnsetFakBox (0);
          return 1;
}

int SetBar (void)
/**
Uebergabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (fakbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        bar = 0;
          }
          else
          {
                        bar  = 1;
          }
          if (syskey == KEYCR)
          {
                       bar  = 1;
                       SendMessage (fakbox.mask[1].feldid, BM_SETCHECK,
                       bar, 0l);
                       break_enter ();
          }
          UnsetFakBox (1);
          return 1;
}

int SetAnonym (void)

/**
Uebergabe an Lieferschein setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (fakbox.mask[2].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       anonym = 0;
          }
          else
          {
                       anonym = 1;
          }
          if (syskey == KEYCR)
          {
                       anonym = 1;
                       SendMessage (cbox.mask[2].feldid, BM_SETCHECK,
                       anonym, 0l);
                       break_enter ();
          }
          UnsetFakBox (2);
          return 1;
}


void SelectFakCtr (void)
/**
Aktiven Button in EnterButton selectieren.
**/
{
        syskey = KEYCR;
        if (fakbox.mask[currentfield].after)
        {
                       (*fakbox.mask[currentfield].after) ();
        }
        syskey = KEYCR;
}



int KompAuf::SetFak_typ (HWND hWnd)
/**
Komplett setzen.
**/
{
 
         save_fkt (5);
         save_fkt (7);
		 set_fkt (NULL, 7);
         set_fkt (KomplettEnd, 5);
		 int savecurrent;

		 savecurrent = currentfield;
         nix = bar = anonym = 0;
         nix = 1;
         EnableWindows (hWnd, FALSE);
         SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
         SetCaption ("Auswahl f�r Fakturier-Typ");
         if (EnterButtonWindow (hWnd, &fakbox, 14, 9, 9, 42) == -1)
         {
			            SetActiveWindow (hWnd);  
                        SetAktivWindow (hWnd);  
                        EnableWindows (hWnd, TRUE);
                        restore_fkt (5);
		                currentfield = savecurrent;
                        return -1;
         }

         SetActiveWindow (hWnd);  
         SetAktivWindow (hWnd);  
         EnableWindows (hWnd, TRUE);
         restore_fkt (5);
         restore_fkt (7);
		 currentfield = savecurrent;
		 if (nix)
		 {
                return 0;
		 }
		 else if (bar)
		 {
                return 1;
		 }
		 else if (anonym)
		 {
                return 2;
		 }
		 return 0;
}


long KompAuf::GenAufNr (short mdn, short fil)
/**
Auftrags-Nummer generieren.
**/
{
	    char buffer [256];
        int dsqlstatus;
        extern  short sql_mode;
        short sqlm;
        int i = 0;


  	    sprintf (buffer, "Eine Auftragsnummer wird generiert"); 
        Mess.Message (buffer);
		Sleep (20);
		commitwork ();
        sqlm = sql_mode;
        sql_mode = 1;
		beginwork ();
//        dsqlstatus = AutoClass.nvholid (mdn, fil, "auf");
        dsqlstatus = AutoClass.nvholid (mdn, 0, "auf");
		commitwork ();
		if (dsqlstatus == -1)
		{
 			                sprintf (buffer, "Fehler bei nvholid"); 
                            Mess.Message (buffer);
 		                    Sleep (20);
			                beginwork ();
					        DbClass.sqlin ((short *) &mdn, 1, 0); 
					        DbClass.sqlin ((short *) &fil, 1, 0); 
							dsqlstatus = DbClass.sqlcomm 
								    ("delete from auto_nr where nr_nam = \"auf\" "
								     "and mdn = ? and fil = ?");
							commitwork ();
                            if (dsqlstatus < 0)
							{
                                     disp_mess 
										 ("Fehler bei der Generierung der Auftrags-Nummer",2);
                                     return 0l;
							}
							dsqlstatus = 100;
		}
					        

        i = 0;
        while (dsqlstatus < 0)
        {
			  sprintf (buffer, "Fehler 1 : Zaehler %d Maxwait MAXWAIT", i); 
              Mess.Message (buffer);
              if (i == MAXWAIT)
			  {
                        disp_mess ("Fehler bei der Generierung der Auftrags-Nummer",2);
                        return 0l;
			  }
              Sleep (50);
			  beginwork ();
//              dsqlstatus = AutoClass.nvholid (mdn, fil, "auf");
              dsqlstatus = AutoClass.nvholid (mdn, 0, "auf");
			  commitwork ();
              i ++;
        }

        if (dsqlstatus == 100)
        {
			  beginwork ();
/*
              dsqlstatus = AutoClass.nvanmprf (mdn,
                                     fil,
                                     "auf",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");
*/
              dsqlstatus = AutoClass.nvanmprf (mdn,
                                     0,
                                     "auf",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");
			  commitwork ();

              i = 0;
              while (dsqlstatus < 0)
              {
 			          sprintf (buffer, "Fehler 2 : Zaehler %d Maxwait MAXWAIT", i); 
                      Mess.Message (buffer);
                      if (i == MAXWAIT)
					  {
                        disp_mess ("Fehler bei der Generierung der Auftrags-Nummer",2);
                        return 0l;
					  }

                      Sleep (50);
					  beginwork ();
//                    dsqlstatus = AutoClass.nvholid (mdn, fil, "ls");
/*
                      dsqlstatus = AutoClass.nvanmprf (mdn,
                                                       fil,
                                                       "auf",
                                                       (long) 1,
                                                       (long) 999999,
                                                       (long) 10,
                                                        "");
*/
                      dsqlstatus = AutoClass.nvanmprf (mdn,
                                                       0,
                                                       "auf",
                                                       (long) 1,
                                                       (long) 999999,
                                                       (long) 10,
                                                        "");
					  commitwork ();
                      i ++;
              }

              if (dsqlstatus == 0)
              {
 					  beginwork ();
/*
                      dsqlstatus = AutoClass.nvholid (mdn,
                                                fil,
                                                "auf");
*/
                      dsqlstatus = AutoClass.nvholid (mdn,
                                                0,
                                                "auf");
					  commitwork ();
              }
         }

         i = 0;
         while (dsqlstatus < 0)
         {
			  sprintf (buffer, "Fehler 3 : Zaehler %d Maxwait MAXWAIT", i); 
              Mess.Message (buffer);
              if (i == MAXWAIT) break;
              Sleep (50);
		      beginwork ();
//              dsqlstatus = AutoClass.nvholid (mdn, fil, "auf");
              dsqlstatus = AutoClass.nvholid (mdn, 0, "auf");
		      commitwork ();
              i ++;
         }

		 beginwork ();
         if (dsqlstatus < 0)
         {
             disp_mess ("Fehler bei der Generierung der Auftrags-Nummer",2);
             return 0l;
         }
         sql_mode = sqlm;
   	     sprintf (buffer, "Auftragsnummer %ld generiert", auto_nr.nr_nr); 
         Mess.Message (buffer);
         Sleep (20);
         return auto_nr.nr_nr;
}

long KompAuf::GenAufNr0 (short mdn, short fil)
/**
Auftrags-Nummer generieren und pruefen, ob die Nummer schon existiert.
**/
{
	     static long auf_nr;
		 static short mdn_nr;
		 static short fil_nr;
		 static int tcursor = -1;
		 int dsqlstatus;
		 int i;

	     
		 if (tcursor == -1)
		 {
			      DbClass.sqlin ((short *) &mdn_nr, 1, 0);
			      DbClass.sqlin ((short *) &fil_nr, 1, 0);
			      DbClass.sqlin ((long *)  &auf_nr, 2, 0);
			      tcursor = DbClass.sqlcursor ("select auf from aufk "
					                      "where mdn = ? "
										  "and fil = ? "
										  "and auf = ?");
		 }

		 i = 0;
		 if (lutz_mdn_par)
		 {
			 mdn = fil = 0;
		 }
		 mdn_nr = mdn;
		 fil_nr = fil;
		 auf_nr = GenAufNr (mdn, fil);
		 while (auf_nr > 0l)
		 {
			 DbClass.sqlopen (tcursor);
			 dsqlstatus = DbClass.sqlfetch (tcursor);
			 if (dsqlstatus == 100) return auf_nr;
             Sleep (50);
			 i ++;
             if (i == MAXWAIT) return 0l;
  		     auf_nr = GenAufNr (mdn, fil);
		 }
         return auf_nr;
}

void KompAuf::AufktoAufk (void)
/**
Auftragkopf uebertragen.
**/
{
         aufk.auf  = GenAufNr0 (aufk.mdn, aufk.fil);
		 WriteLogfile ("  Neue Auftragsnummer %ld\n", aufk.auf); 
		 AufkOK = TRUE;
}


BOOL KompAuf::TeilSmttoAuf (int modus)
/**
Positionen sortiert nach Teilsortiment.
Fuer jedes Teilsortiment eine eigen Auftrags-Nr.
**/
{
         long posi = 0;
         int dsqlstatus;
         int teil_smt = 0;
         BOOL first_auf;
		 int auf_stat;
		 long  auf_org;
		 short mdn_org;
		 short fil_org;
  	     char buffer [80];
		 long maxpos, anzteil_smt;
		 int i;
		 BOOL DelOK;

		 //LuD 230111 A
		 //Muss �berhaupt gesplittet werden ??? 
		 DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		 DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		 DbClass.sqlin ((long *)  &aufk.auf, 2,  0);
		 DbClass.sqlout ((long *) &anzteil_smt,   2, 0);
		 dsqlstatus = DbClass.sqlcomm ("select count (unique teil_smt) from aufp "
			                           "where mdn = ? "
						               "and fil = ? "
						               "and auf = ?");
		 if (anzteil_smt < 2)
		 {
			 //nicht mehrere Teilsortimente vorhanden 
			 return FALSE;
		 }
		 //LuD 230111 E


		 mdn_org = aufk.mdn;
		 fil_org = aufk.fil;
		 auf_org = aufk.auf;

		 auf_stat = aufk.auf_stat;
		 first_auf = TRUE;
		 if (modus)
		 {
			 aufanz = 0;
		 }
         Getauf_a_sort ();
		 if (IsGroup == FALSE) beginwork ();
         ls_class.close_aufp_o ();
         AuftoPid (); 

		 DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		 DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		 DbClass.sqlin ((long *)  &aufk.auf, 2,  0);
		 DbClass.sqlout ((long *) &maxpos,   2, 0);
		 dsqlstatus = DbClass.sqlcomm ("select count (*) from aufp "
			                           "where mdn = ? "
						               "and fil = ? "
						               "and auf = ?");
		 if (dsqlstatus != 0) return FALSE;
		 if (maxpos == 0) return FALSE;

		 if (maxpos > aufptanz)
		 {
			 if (aufptab)
			 {
				 GlobalFree (aufptab);
				 aufptab = NULL;
			 }
			 aufptanz = maxpos;
			 aufptab = (struct AP *) GlobalAlloc (GMEM_FIXED, 
				        sizeof (struct AP) * (aufptanz + 1));
			 if (aufptab == NULL) 
			 {
				 disp_mess ("Kein Speicherplatz in TeilSmt-Zuordnung", 2);
				 return FALSE;
			 }
		 }

         if (auf_a_sort == 0)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by teil_smt,"
                                                   "posi");
         }
         else if (auf_a_sort == 1)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by teil_smt,a");
         }
         else if (auf_a_sort == 2)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by teil_smt,dr_folge");
         }
		 i = 0;
         while (dsqlstatus == 0)
		 {
			 aufptab[i].mdn  = aufp.mdn;
			 aufptab[i].fil  = aufp.fil;
			 aufptab[i].auf  = aufp.auf;
			 aufptab[i].posi = aufp.posi;
			 aufptab[i].a    = aufp.a;
			 i ++;
			 if (i > aufptanz) 
			 {
				 disp_mess ("Fehler bei der Artikel�bernahme in TeilSmt-Zuordnung", 2);
				 break; 
			 }
             dsqlstatus = ls_class.lese_aufp_o ();
		 }
		 aufptanz = i;
 
		 DelOK = FALSE; 
		 for (i = 0; i < aufptanz; i ++) 
         {
               dsqlstatus = ls_class.lese_aufp_ap 
				             (aufptab[i].mdn, aufptab[i].fil, aufptab[i].auf,
                             aufptab[i].a, aufptab[i].posi);
               if (dsqlstatus != 0) 
			   {
				   WriteLogfile ("Fehler beim Lesen in auf Auftrag %ld a %.0lf\n",
					             aufptab[i].auf, aufptab[i].a);
          		   continue;
			   }

               if (teil_smt != aufp.teil_smt)
               {
                   posi = 0;
				   AufktoAufk ();
				   if (aufk.auf > max_auf)
				   {
					   max_auf = aufk.auf;
				   }
				   if (AufkOK == FALSE)
                   {
					       WriteLogfile ("Fehler beim �bertragen des Auftragkopf %ld\n", 
							              auf_org);
                           DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
                           DbClass.sqlin ((short *) &aufk.fil, 1, 0);
                           DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
                           DbClass.sqlcomm ("update aufk set delstatus = 0 "
                                           "where mdn = ? "
                                           "and fil = ? "
                                           "and auf = ? ");
                           if (IsGroup == FALSE) rollbackwork ();
                           return FALSE;
                   }
                   if (aufk.auf <= 0) 
                   {
					       WriteLogfile ("Fehler beim �bertragen des Auftragkopf %ld\n", 
							              auf_org);
                           DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
                           DbClass.sqlin ((short *) &aufk.fil, 1, 0);
                           DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
                           DbClass.sqlcomm ("update aufk set delstatus = 0 "
                                           "where mdn = ? "
                                           "and fil = ? "
                                           "and auf = ? ");
                           if (IsGroup == FALSE) rollbackwork ();
                           return FALSE;
                   }
		           DelOK = TRUE; 
				   ls_class.update_aufk (aufk.mdn, aufk.fil, aufk.auf);
				   first_auf = FALSE;
                   teil_smt = aufp.teil_smt;
				   if (modus)
				   {
					   mdntab[aufanz] = aufk.mdn;
					   filtab[aufanz] = aufk.fil;
					   auftab[aufanz] = aufk.auf;
					   aufanz ++;
				   }
               }

               posi += 10;
               sprintf (buffer, "Auftrag %ld Position %ld Artikel %.0lf wird �bertragen", 
				                 aufk.auf, posi, aufp.a);
			   Mess.Message (buffer);
//               AufptoAufp (posi);
               ls_class.update_aufp (aufp.mdn, aufp.fil, aufk.auf,
                                     aufp.a, aufp.posi);
         }
         InitPid (); 
		 if (DelOK)
		 {
               DbClass.sqlin ((short *) &mdn_org, 1, 0);
               DbClass.sqlin ((short *) &fil_org, 1, 0);
               DbClass.sqlin ((long *)  &auf_org, 2, 0);
               DbClass.sqlcomm ("delete from aufk "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
               DbClass.sqlin ((short *) &mdn_org, 1, 0);
               DbClass.sqlin ((short *) &fil_org, 1, 0);
               DbClass.sqlin ((long *)  &auf_org, 2, 0);
               DbClass.sqlcomm ("delete from aufp "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
		 }
		 if (IsGroup == FALSE)
		 {
                     commitwork ();
                     Mess.Message ("Der Auftrag wurde aufgeteilt");
 		             Sleep (20);
		 }
		 return TRUE;
}


void KompAuf::K_ListeTS (void)
/**
Komissionierliste drucken.
Wenn kun.einz_ausw > 3, Auftraege splitten.
**/
{
	     int i;
		 int dsqlstatus;
		 
		 if (SplitLs)
		 {
			 K_Liste ();
			 return;
		 }

         if (Geteinz_ausw () < 4) 
		 {
			 K_Liste ();
			 return;
		 }

         if (TeilSmttoAuf (1) == FALSE)
		 {
			 K_Liste ();
			 return;
		 }

         for (i = 0; i < aufanz; i ++)
		 {
			 aufk.mdn = mdntab[i];
			 aufk.fil = filtab[i];
			 aufk.auf = auftab[i];
		     dsqlstatus = ls_class.lese_aufk (mdntab[i], filtab[i], auftab[i]);
			 if (dsqlstatus) continue;
			 K_Liste ();
		 }
}
void KompAuf::Kom_Liste_Rab(void)
{
	char buffer [265];
	HWND akthWnd;
	akthWnd = GetActiveWindow ();
	sprintf (buffer, "rswrun angdr U %hd %hd %ld %s", aufk.mdn, aufk.fil, aufk.auf, aufk.pers_nam);  
	ProcWaitExecEx (buffer, SW_SHOWNORMAL, -1, 0, -1, 0);

	
	SetActiveWindow (akthWnd);
	CreateLogfile ();
	WriteLogfile ("Auftrag %ld wurde gedruckt\n", aufk.auf);
	CloseLogfile ();
    DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
    DbClass.sqlin ((short *) &aufk.fil, 1, 0);
    DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
    DbClass.sqlcomm ("update aufk set auf_stat = 4 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? and auf_stat < 4");
    if (IsGroup == FALSE) Mess.Message ("Das Auftrag wurde gedruckt");
	

}

void KompAuf::PrintAufbest (void)
{
    char valfrom [20];
    char valto [20];
	HWND akthWnd;

	akthWnd = GetActiveWindow ();
	SetAusgabe (PRINTER_OUT);
    if (syskey == KEY5) return;
    commitwork ();
	extern short PartyMdn;
	if(aufk.mdn == PartyMdn)
	{
		Kom_Liste_Rab ();
		return;
	} 

	if (ChoicePrinter)
	{
		Llgen.SetDrkWahl (ChoicePrinter);
	}

    if (Llgen.LeseFormat (aufbformat) == FALSE)
	{
		           return;
	}


    sprintf (valfrom, "%hd", aufk.mdn);
    sprintf (valto, "%hd", aufk.mdn);
    Llgen.FillFormFeld ("aufk.mdn", valfrom, valto);

    sprintf (valfrom, "%hd", aufk.fil);
    sprintf (valto, "%hd", aufk.fil);
    Llgen.FillFormFeld ("aufk.fil", valfrom, valto);


    sprintf (valfrom, "%ld", aufk.auf);
    sprintf (valto, "%ld", aufk.auf);
    Llgen.FillFormFeld ("aufk.auf", valfrom, valto);

/*
    sprintf (valfrom, "%ld", aufk.kun);
    sprintf (valto, "%ld", aufk.kun);
    Llgen.FillFormFeld ("aufk.kun", valfrom, valto);
	char lieferdat [12];

	dlong_to_asc (aufk.lieferdat, lieferdat);
    sprintf (valfrom, "%s", lieferdat);
    sprintf (valto, "%s", lieferdat);
    Llgen.FillFormFeld ("aufk.lieferdat", valfrom, valto);
*/

    Llgen.StartPrint ();

	SetActiveWindow (akthWnd);
	CreateLogfile ();
	WriteLogfile ("Auftragsbest�tigung %ld wurde gedruckt\n", aufk.auf);
	CloseLogfile ();
/*
    DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
    DbClass.sqlin ((short *) &aufk.fil, 1, 0);
    DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
    DbClass.sqlcomm ("update aufk set auf_stat = 4 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
*/
    Mess.Message ("Die Auftragsbest�tigung wurde gedruckt");
}


void KompAuf::DrkLsTS (void)
/**
Komissionierliste drucken.
Wenn kun.einz_ausw > 3, Auftraege splitten.
**/
{
	     int i;
		 int dsqlstatus;
		 
         if (SplitLs) 
		 {
			 K_Liste ();
             SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
 	         SetForegroundWindow (hMainWindow);
             UpdateWindow (hMainWindow);
             AuftoLs(0); 
			 return;
		 }
         if (Geteinz_ausw () < 4) 
		 {
			 K_Liste ();
             SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
 	         SetForegroundWindow (hMainWindow);
             UpdateWindow (hMainWindow);
             AuftoLs(0); 
			 return;
		 }

         if (TeilSmttoAuf (1) == FALSE)
		 {
			 K_Liste ();
             SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
 	         SetForegroundWindow (hMainWindow);
             UpdateWindow (hMainWindow);
             AuftoLs(0); 
			 return;
		 }

         for (i = 0; i < aufanz; i ++)
		 {
			 aufk.mdn = mdntab[i];
			 aufk.fil = filtab[i];
			 aufk.auf = auftab[i];
		     dsqlstatus = ls_class.lese_aufk (mdntab[i], filtab[i], auftab[i]);
			 if (dsqlstatus) continue;
             K_Liste ();
             SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
 	         SetForegroundWindow (hMainWindow);
             UpdateWindow (hMainWindow);
             AuftoLs(0); 
		 }
}

void KompAuf::AuftoLsTS (void)
/**
Komissionierliste drucken.
Wenn kun.einz_ausw > 3, Auftraege splitten.
**/
{
	     int i;
		 int dsqlstatus;
		 
         if (SplitLs) 
		 {
             AuftoLs(0); 
			 return;
		 }
         if (Geteinz_ausw () < 4) 
		 {
             AuftoLs(0); 
			 return;
		 }

         if (TeilSmttoAuf (1) == FALSE)
		 {
             AuftoLs(0); 
			 return;
		 }
         for (i = 0; i < aufanz; i ++)
		 {
			 aufk.mdn = mdntab[i];
			 aufk.fil = filtab[i];
			 aufk.auf = auftab[i];
		     dsqlstatus = ls_class.lese_aufk (mdntab[i], filtab[i], auftab[i]);
			 if (dsqlstatus) continue;
             AuftoLs(0); 
		 }
}


void KompAuf::AufToSmtDrk (int cursor, long *auf_bis)
/**
Vor dem Kommisionierlistendruck Auftraege nach Teilsortimentsgruppen aufteilen.
**/
{
	int i;
	int dsqlstatus;
	MSG msg;

	if (SplitLs) return;

	aufanz = 0;
	DbClass.sqlopen (cursor);
	dsqlstatus = DbClass.sqlfetch (cursor);
	while (dsqlstatus == 0)
	{
		    mdntab[aufanz] = aufk.mdn;
		    filtab[aufanz] = aufk.fil;
		    auftab[aufanz] = aufk.auf;
			if (aufanz == 999) //EF-38
			{
				disp_mess ("Maximale Anzahl Auftr�ge �berschritten", 2);
				break;
			}
	        dsqlstatus = DbClass.sqlfetch (cursor);
			aufanz ++;
	}
	DbClass.sqlclose (cursor);

    for (i = 0; i < aufanz; i ++)
	{
		     aufk.mdn = mdntab[i]; 
		     aufk.fil = filtab[i]; 
		     aufk.auf = auftab[i]; 
 		     dsqlstatus = ls_class.lese_aufk (aufk.mdn, aufk.fil, aufk.auf);
			 if (dsqlstatus) continue;
			 if (aufk.auf_stat > 3) continue;
             if (Geteinz_ausw () < 4) 
			 {
				 continue;
			 }
			 WriteLogfile ("Auftrag %ld wird aufgeteilt\n", aufk.auf);
	         if (TeilSmttoAuf (0) == FALSE)
			 {
				 continue;
			 }
			 if (max_auf > *auf_bis)
			 {
				 *auf_bis = max_auf;
			 }
             if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
             {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
             }
    }
    WriteLogfile ("Aufteilen beendet : auf_bis = %ld\n", *auf_bis);
}		 

void KompAuf::ProdMdntoAuf (int modus)
/**
Positionen sortiert nach Gruppe.
Fuer jede Gruppe eine eigen Auftrags-Nr.
**/
{
         long posi = 0;
         int dsqlstatus;
         long gruppe = 0;
         BOOL first_auf;
		 int auf_stat;
		 long  auf_org;
		 short mdn_org;
		 short fil_org;
  	     char buffer [80];
		 long maxpos;
		 int i;
		 BOOL DelOK;

		 mdn_org = aufk.mdn;
		 fil_org = aufk.fil;
		 auf_org = aufk.auf;

		 auf_stat = aufk.auf_stat;
		 first_auf = TRUE;
		 if (modus)
		 {
			 aufanz = 0;
		 }
         Getauf_a_sort ();
		 if (IsGroup == FALSE) beginwork ();
         ls_class.close_aufp_o ();
         AuftoPid (); 

		 DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		 DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		 DbClass.sqlin ((long *)  &aufk.auf, 2,  0);
		 DbClass.sqlout ((long *) &maxpos,   2, 0);
		 dsqlstatus = DbClass.sqlcomm ("select count (*) from aufp "
			                           "where mdn = ? "
						               "and fil = ? "
						               "and auf = ?");
		 if (dsqlstatus != 0) return;
		 if (maxpos == 0) return;

		 if (maxpos > aufptanz)
		 {
			 if (aufptab)
			 {
				 GlobalFree (aufptab);
				 aufptab = NULL;
			 }
			 aufptanz = maxpos;
			 aufptab = (struct AP *) GlobalAlloc (GMEM_FIXED, 
				        sizeof (struct AP) * (aufptanz + 1));
			 if (aufptab == NULL) 
			 {
				 disp_mess ("Kein Speicherplatz in TeilSmt-Zuordnung", 2);
				 return;
			 }
		 }

         if (auf_a_sort == 0)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by gruppe,"
                                                   "posi");
         }
         else if (auf_a_sort == 1)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by gruppe,a");
         }
         else if (auf_a_sort == 2)
         {
              dsqlstatus = ls_class.lese_aufp_o 
                   (aufk.mdn, aufk.fil, aufk.auf, "order by gruppe,dr_folge");
         }
		 i = 0;
         while (dsqlstatus == 0)
		 {
			 aufptab[i].mdn  = aufp.mdn;
			 aufptab[i].fil  = aufp.fil;
			 aufptab[i].auf  = aufp.auf;
			 aufptab[i].posi = aufp.posi;
			 aufptab[i].a    = aufp.a;
			 i ++;
			 if (i > aufptanz) 
			 {
				 disp_mess ("Fehler bei der Artikel�bernahme in TeilSmt-Zuordnung", 2);
				 break; 
			 }
             dsqlstatus = ls_class.lese_aufp_o ();
		 }
		 aufptanz = i;
 
		 DelOK = FALSE; 
		 for (i = 0; i < aufptanz; i ++) 
         {
               dsqlstatus = ls_class.lese_aufp_ap 
				             (aufptab[i].mdn, aufptab[i].fil, aufptab[i].auf,
                             aufptab[i].a, aufptab[i].posi);
               if (dsqlstatus != 0) 
			   {
				   WriteLogfile ("Fehler beim Lesen in auf Auftrag %ld a %.0lf\n",
					             aufptab[i].auf, aufptab[i].a);
          		   continue;
			   }

               if (gruppe != aufp.gruppe)
               {
                   posi = 0;
				   aufk.auf_stat = 3;
				   AufktoAufk ();
				   if (aufk.auf > max_auf)
				   {
					   max_auf = aufk.auf;
				   }
				   if (AufkOK == FALSE)
                   {
					       WriteLogfile ("Fehler beim �bertragen des Auftragkopf %ld\n", 
							              auf_org);
                           DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
                           DbClass.sqlin ((short *) &aufk.fil, 1, 0);
                           DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
                           DbClass.sqlcomm ("update aufk set delstatus = 0 "
                                           "where mdn = ? "
                                           "and fil = ? "
                                           "and auf = ? ");
                           if (IsGroup == FALSE) rollbackwork ();
                           return;
                   }
                   if (aufk.auf <= 0) 
                   {
					       WriteLogfile ("Fehler beim �bertragen des Auftragkopf %ld\n", 
							              auf_org);
                           DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
                           DbClass.sqlin ((short *) &aufk.fil, 1, 0);
                           DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
                           DbClass.sqlcomm ("update aufk set delstatus = 0 "
                                           "where mdn = ? "
                                           "and fil = ? "
                                           "and auf = ? ");
                           if (IsGroup == FALSE) rollbackwork ();
                           return;
                   }
		           DelOK = TRUE; 
				   ls_class.update_aufk (aufk.mdn, aufk.fil, aufk.auf);
				   first_auf = FALSE;
                   gruppe = aufp.gruppe;
				   if (modus)
				   {
					   mdntab[aufanz] = aufk.mdn;
					   filtab[aufanz] = aufk.fil;
					   auftab[aufanz] = aufk.auf;
					   aufanz ++;
				   }
               }

               posi += 10;
               sprintf (buffer, "Auftrag %ld Position %ld Artikel %.0lf wird �bertragen", 
				                 aufk.auf, posi, aufp.a);
			   Mess.Message (buffer);
//               AufptoAufp (posi);
               ls_class.update_aufp (aufp.mdn, aufp.fil, aufk.auf,
                                     aufp.a, aufp.posi);
         }
         InitPid (); 
		 if (DelOK)
		 {
               DbClass.sqlin ((short *) &mdn_org, 1, 0);
               DbClass.sqlin ((short *) &fil_org, 1, 0);
               DbClass.sqlin ((long *)  &auf_org, 2, 0);
               DbClass.sqlcomm ("delete from aufk "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
               DbClass.sqlin ((short *) &mdn_org, 1, 0);
               DbClass.sqlin ((short *) &fil_org, 1, 0);
               DbClass.sqlin ((long *)  &auf_org, 2, 0);
               DbClass.sqlcomm ("delete from aufp "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf = ? ");
		 }
		 if (IsGroup == FALSE)
		 {
                     commitwork ();
                     Mess.Message ("Der Auftrag wurde aufgeteilt");
 		             Sleep (20);
		 }
}



/* Angebot in Auftrag uenertragen   */

void KompAuf::AngtoAuf (void)
/**
Auftrag in Lieferschein uebertragen. Sortiert nach Positions-Nummer.
**/
{
         long posi = 0;
         int dsqlstatus;
  	     char buffer [80];
		 int ang_stat;

		 angk.mdn = aufk.mdn;
		 angk.fil = aufk.fil;
	     WriteLogfile (" Angebot %ld wird �bergeben\n", angk.ang);
		 ang_stat = angk.ang_stat;
         DbClass.sqlin ((short *) &angk.mdn, 1, 0); 
         DbClass.sqlin ((short *) &angk.fil, 1, 0); 
         DbClass.sqlin ((long *)  &angk.ang, 2, 0); 
 	     dsqlstatus = DbClass.sqlcomm ("select ang from angk "
			                           "where mdn = ? "
						               "and   fil = ? "
					                   "and ang   = ? " 
									   "and delstatus = 0");
		 if (dsqlstatus != 0)
		 {
				 disp_mess ("Das Angebot wird gerade bearbeitet", 2);
				 return;
		 }

         DbClass.sqlin ((short *) &angk.mdn, 1, 0); 
         DbClass.sqlin ((short *) &angk.fil, 1, 0); 
         DbClass.sqlin ((long *)  &angk.ang, 2, 0); 
	     DbClass.sqlcomm ("update angk set delstatus = -1 "
				              "where mdn = ? "
							  "and   fil = ? "
							  "and ang   = ?");

         AngktoAufk ();
         ls_class.update_aufk (aufk.mdn, aufk.fil, aufk.auf);
		 angp_class.dbcloseang ();
         angp.mdn = angk.mdn;
         angp.fil = angk.fil;
         angp.ang = angk.ang;
         if (auf_a_sort == 0)
         {
              dsqlstatus = angp_class.dbreadfirstang ("order by posi"); 
         }
         else
         {
              dsqlstatus = angp_class.dbreadfirstang ("order by a"); 
         }
         while (dsqlstatus == 0)
         {
               posi += 10;
               sprintf (buffer, "Angebot %ld Position %ld Artikel %.0lf wird �bertragen", 
				                 angp.ang, posi, angp.a);
//			   Mess.Message (buffer);
               AngptoAufp (posi);
			   ls_class.update_aufp (aufp.mdn, aufp.fil, aufp.auf, aufp.a, aufp.posi);
               angpttoaufpt (); 
               dsqlstatus = angp_class.dbreadang ();
         }
         DbClass.sqlin ((short *) &angk.mdn, 1, 0);
         DbClass.sqlin ((short *) &angk.fil, 1, 0);
         DbClass.sqlin ((long *)  &angk.ang, 2, 0);
		 if (AufkOK)
		 {
                 DbClass.sqlcomm ("update angk set ang_stat = 5, delstatus = 0 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and ang = ? ");
		 }
		 else
		 {
                 DbClass.sqlcomm ("update angk set delstatus = 0 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and ang = ? ");
		 }
//         Mess.Message ("Das Angebot wurde �bertragen");
}


void KompAuf::AngktoAufk (void)
/**
Angebotskopf uebertragen.
**/
{
         char datum [12];
		 int dsqlstatus;
         long sysdatum;

		 
		 AufkOK = FALSE;
	     WriteLogfile (" Angebot %hd %hd %ld wird gelesen Kunde %ld Adresse %ld \n", 
			 angk.mdn, angk.fil, angk.ang, angk.kun, angk.adr);
	     dsqlstatus = angk_class.dbreadfirst ();
		 if (dsqlstatus)
		 {
 	         WriteLogfile (" Fehler beim Lesen Angebot %ld\n", angk.ang);
			 AufkOK = FALSE;
		     return;
		 }

		 WriteLogfile ("Angebot gefunden kun = %ld adr = %ld\n", angk.kun, angk.adr);

         sysdate (datum);
         sysdatum = dasc_to_long (datum);
         aufk.ang = angk.ang;
         aufk.adr = angk.adr;
	     WriteLogfile (" �bergebene Adress-Nummer %ld\n", aufk.adr);
         aufk.kun_fil = angk.kun_fil;
         aufk.kun     = angk.kun;
	     WriteLogfile (" �bergebene Kunden-Nummer %ld\n", aufk.kun);
         strcpy (aufk.feld_bz1, angk.feld_bz1);
         strcpy (aufk.feld_bz2, angk.feld_bz2);
         strcpy (aufk.feld_bz3, angk.feld_bz3);
         aufk.lieferdat = 0l;
         aufk.komm_dat =  0l;
         strcpy (aufk.lieferzeit, angk.lieferzeit);
         strcpy (aufk.hinweis, angk.hinweis);
         aufk.auf_stat = 1;
         strcpy (lsk.kun_krz1, angk.kun_krz1);
         aufk.vertr   = angk.vertr;
         aufk.tou     = angk.tou; 
		 aufk.delstatus = 0;
         aufk.zeit_dec = angk.zeit_dec;
         aufk.kopf_txt = angk.kopf_txt;
         aufk.fuss_txt = angk.fuss_txt;
         strcpy (aufk.pers_nam, angk.pers_nam);
         aufk.waehrung = angk.waehrung;
		 aufk.psteuer_kz = angk.psteuer_kz;
		 AufkOK = TRUE;
}

void KompAuf::AngptoAufp (long posi)
/**
Aufposition uebertragen.
**/
{
      KEINHEIT keinheit;

      aufp.mdn  = aufk.mdn;;
      aufp.fil  = aufk.fil;
      aufp.auf =  aufk.auf;
      aufp.posi = posi;
      aufp.a    = angp.a;
      WriteLogfile ("   Artikel %.0lf\n", angp.a);
      aufp.auf_me = angp.auf_me;
      aufp.me_einh = angp.me_einh;
      aufp.me_einh_kun = angp.me_einh_kun;
      einh_class.AktAufEinh (aufk.mdn, aufk.fil,
                                aufk.kun, aufp.a, aufp.me_einh_kun);
      einh_class.GetKunEinh (aufk.mdn, aufk.fil,
                              aufk.kun, aufp.a, &keinheit);
          
      if (keinheit.me_einh_kun == keinheit.me_einh_bas)
	  {
             aufp.lief_me = aufp.auf_me;
	  }
      else
	  {
            if (keinheit.inh <= (double) 0.0)
            {
                     keinheit.inh = (double) 1.0;
            }
            aufp.lief_me = aufp.auf_me * keinheit.inh;
	  }
      strcpy (aufp.auf_me_bz, angp.auf_me_bz);
      strcpy (aufp.lief_me_bz, angp.lief_me_bz);
      aufp.auf_vk_pr = angp.auf_vk_pr;
      aufp.auf_lad_pr = angp.auf_lad_pr;
      aufp.auf_vk_euro = angp.auf_vk_euro;
      aufp.auf_lad_euro = angp.auf_lad_euro;
      aufp.auf_vk_fremd = angp.auf_vk_fremd;
      aufp.auf_lad_fremd = angp.auf_lad_fremd;
      aufp.delstatus = 0;

	  if (AngText && angp.angp_txt != 0)
	  {
               aufp.aufp_txt = GenAufpTxt ();
	  }
	  else
	  {
               aufp.aufp_txt = 0l;
	  }

      aufp.sa_kz_sint  = angp.sa_kz_sint;

      aufp.prov_satz   = angp.prov_satz;
      aufp.me_einh_kun = angp.me_einh_kun;
      aufp.me_einh     = angp.me_einh;
      aufp.auf_me      = angp.auf_me;
}

int KompAuf::FaxAuf (HWND hMainWindow, short mdn, short fil, long auf)
/**
Auftrag faxen
**/
{
/*
	     char command [512];
		 char *tmp;
		 DWORD Ecode;
		 CA_CLASS Cancel;
		 int ret;
*/


//		 prnret = 0;
//		 if (PrintMode == 0)
		 {
			 K_Liste_Fax ();
			 return 0;

         }

/*
         prnret = 0;
		 SetpDevMode (NULL);
		 WindowOk = TRUE;
		 tmp = getenv ("TMPPATH");
		 if (tmp == NULL)
		 {
			 tmp = "C:";
		 }

		 sprintf (command, "bws_asc -os\"where mdn = %hd and fil = %hd "
			              "and best_blg = %ld\" %s\\32200.lst 32200.if",
						   mdn, fil, best_kopf.best_blg, tmp);

		 EnableWindow (hMainWindow, FALSE);
		 Cancel.SetCaFunc (BreakAufb);
 	     Cancel.Execute (hMainWindow, 41, 4, " \nBitte warten.....\n"
								 "Die Bestellung wird zum Drucken vorbereitet.",
								 NULL);
	     Pid = ProcExecPid (command, SW_MINIMIZE, -1, 0, -1, 0); 
	     Ecode = WaitPid (Pid); 
		 EnableWindow (hMainWindow, TRUE);
		 Cancel.Destroy ();
		 if (Ecode == 99)
		 {
             return -1;
		 }
		 if (prnret == 99)
		 {
             return -1;
		 }

		 if (Ecode != 0)
		 {
			 disp_mess ("Fehler beim Aufbereiten der Druckdaten", 2);
			 return -1;
		 }

		 if (PrintMode == 1)
		 {
		       sprintf (command, "%s\\32200.lst", tmp);
		       Cancel.SetAbrProc (BreakPrn);
		       EnableWindow (hMainWindow, FALSE);
               Cancel.Execute (hMainWindow, 28, 4,
			                          " \nBitte warten.....\n"
									  "Die Bestellung wird gedruckt.",
									   NULL);
               ret = Gdruck (command);
		       EnableWindow (hMainWindow, TRUE);
			   if (prnret) return prnret;
			   return ret;
		 }

		 if (PrintMode == 2)
		 {
		       sprintf (command, "%s\\32200.lst", tmp);
               if (SelectPrinter ())
			   {
				      InvalidateRect (hMainWindow, NULL, TRUE);
				      UpdateWindow (hMainWindow);
		              Cancel.SetAbrProc (BreakPrn);
		              EnableWindow (hMainWindow, FALSE);
	                  Cancel.Execute (hMainWindow, 28, 4,
			                          " \nBitte warten.....\n"
									  "Die Bestellung wird gedruckt.",
									   NULL);
                      ret = Gdruck (command);
		              EnableWindow (hMainWindow, TRUE);
			          if (prnret) return prnret;
			          return ret;
			   }
		       EnableWindow (hMainWindow, TRUE);
			   return -1;
		 }
		 else if (PrintMode == 3)
		 {
		       sprintf (command, "%s\\32200.lst", tmp);
			   if (SetPage ())
			   {
				      InvalidateRect (hMainWindow, NULL, TRUE);
				      UpdateWindow (hMainWindow);
		              Cancel.SetAbrProc (BreakPrn);
		              EnableWindow (hMainWindow, FALSE);
	                  Cancel.Execute (hMainWindow, 28, 4,
			                          " \nBitte warten.....\n"
									  "Die Bestellung wird gedruckt.",
									   NULL);
                      ret = Gdruck (command);
		              EnableWindow (hMainWindow, TRUE);
			          if (prnret) return prnret;
			          return ret;
			   }
		       EnableWindow (hMainWindow, TRUE);
			   return -1;
		 }
         EnableWindow (hMainWindow, TRUE);
		 disp_mess ("Fehler bim Drucken", 2);
		 return -1;
*/
}

void KompAuf::Auswerte_kun_gr2 (short kun_gr2)
{
// Feld aus Kundenstamm noch auswerten.
	char *pdest;
//    char wert [5];

	ls_einzel_class.ModusCatering = 0;
	strcpy(ls_einzel_class.komm_formular, "");
 	memcpy (&ptabn, &ptabn_null, sizeof (ptabn));
	sprintf (ptabn.ptwert, "%hd", kun_gr2);
    if ( ptab_class.lese_ptab ("kun_gr2", ptabn.ptwert) == 100)   
	{
		ptab_class.lese_ptab ("kun_gr", ptabn.ptwert); //fr�her gings �ber kun_gr , jetzt kun_gr2
	}

		//Essig
		strcpy(ls_einzel_class.komm_formular, ptabn.ptwer1);
//		artikel_etikett = ptabn.ptwer2;
//		artikel_etikett.Trim ();

		ls_einzel_class.Salat_extra = 0;
		ls_einzel_class.Dessert_extra = 0;
		ls_einzel_class.Suppe_extra = 0;
		memcpy (&ptabn, &ptabn_null, sizeof (ptabn));
		strcpy (ptabn.ptwert, ls_einzel_class.komm_formular);
		strcpy(ls_einzel_class.komm_formular, "");
		if (ptab_class.lese_ptab ("k_formular", ptabn.ptwert) == 0)
		{
			strcpy(ls_einzel_class.komm_formular, ptabn.ptbezk);
			if (strlen (clipped(ls_einzel_class.komm_formular)) > 2)
			{
				SetDrFormat (ls_einzel_class.komm_formular);
			}
			pdest = strstr (ptabn.ptwer1, "atering");  //Suche nach Catering oder catering --> dann Modus Rezepturaufl�sung
			if (pdest)
			{
				ls_einzel_class.ModusCatering = atoi(ptabn.ptwer2);
				if (ls_einzel_class.ModusCatering == 1)
				{
					ls_einzel_class.Salat_extra = 1;
				}
				if (ls_einzel_class.ModusCatering == 2)
				{
					ls_einzel_class.Salat_extra = 1;
					ls_einzel_class.Dessert_extra = 2;
					ls_einzel_class.Suppe_extra = 2;
				}
			}
		}
		else
		{
				SetDrFormat (GetDrFormatStandard());
		}
/** doch nicht
	    memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
		sprintf (wert, "%s", kun.kun_bran2);
		dsqlstatus = ptab_class.lese_ptab ("kun_bran2", wert);
		if (atoi(ptabn.ptwer1) == 1) ls_einzel_class.Salat_extra = TRUE;
		if (atoi(ptabn.ptwer2) == 1) ls_einzel_class.Dessert_extra = TRUE;
**/


/*
		memcpy (&ptabn, &ptabn_null, sizeof (ptabn));
		strcpy (ptabn.ptwert, artikel_etikett.GetBuffer());
		artikel_etikett = "";
		if (ptab_class.lese_ptab ("etikett", ptabn.ptwert) == 0)
		{
			artikel_etikett = ptabn.ptbezk;
			artikel_etikett.Trim ();
		}
*/
	
}


void KompAuf::InitKumAuf (void) //EF-69
{
	int i;
	for (i = 0; i <20 ; i++)
	{
		strcpy (KumAuf[i],"");
		KumTour[i] = 0;
		KumKunTour[i] = 0;
		KumLieferdat[i] = 0;
		KumAuftrag[i] = 0;
	}
	for (i = 0; i <1000 ; i++)
	{
		if (KumulierteAuftraege[i] == 0)  break;
		KumulierteAuftraege[i] = 0;
	}
}


BOOL KompAuf::FuelleKumAuf (short mdn, int auf, int kuntou, int touadr, long lieferdat, int tou) //EF-69
{
	int i;
	if (! (kuntou > 0 && touadr > 0 && tou > 0)) return FALSE;
	for (i = 0; i <20 ; i++)
	{
		if (KumTour[i] == 0) break;
		if (kuntou == KumKunTour[i] && lieferdat == KumLieferdat[i] && tou == KumTour[i] ) break;
	}
	if (i >=20) return FALSE;
	KumMdn[i] = mdn;
	KumKunTour[i] = kuntou;
	KumTour[i] = tou;
	KumTouAdr[i] = touadr;
	KumLieferdat[i] = lieferdat;

	if (strlen ( KumAuf [i]) == 0)
	{
		sprintf (KumAuf[i],"%ld",auf);
	}
	else
	{
		sprintf (KumAuf[i],"%s,%ld",KumAuf[i],auf);
	}
	return TRUE;
}

void KompAuf::ErzeugeKumAuf (void) //EF-69
{
	int i;
	for (i = 0; i <20 ; i++)
	{
		if (KumTour[i] == 0 || KumTouAdr[i] == 0 || KumLieferdat[i] == 0) break;

        beginwork ();
		ErzeugeKumAuf (KumMdn[i],0,KumKunTour[i],KumTouAdr[i],KumLieferdat[i], KumTour[i]);
		KumAuftrag[i] = aufk.auf;  //wird z.Z. nicht benutzt
		ErzeugeAufp (KumAuf[i]);

		 // tmp_catering muss auch f�r den kum. Auftrag noch erzeugt werden, sonst kommmts nicht mit auf dem Ausdruck 19.11.2014
			if (ls_class.lese_aufk (aufk.mdn, aufk.fil, aufk.auf) == 0)
			{
                if (kun_class.lese_kun (aufk.mdn,0,aufk.kun) == 0)
				{
	    			Auswerte_kun_gr2 (kun.kun_gr2); //Essig
					ls_class.update_aufk (aufk.mdn,aufk.fil,aufk.auf);  //setzten von feld_bz3 (== Formatnummer)
					GeneriereLSEinzel_aus_Auftrag();
				}
			}

		commitwork ();

	}

}

void KompAuf::ErzeugeKumAuf (short mdn, short fil, int kuntou, int dkun, long lieferdat, int tou) //EF-69a
{
         char datum [12];
		 int dsqlstatus;
         long sysdatum;


	     WriteLogfile (" Kum.Auftrag wird erzeugt : Kunde %ld \n", kun);

         sysdate (datum);
         sysdatum = dasc_to_long (datum);
  	     AufktoAufk ();  //neue Auftragsnummer erzeugen
		 WriteLogfile (" Generierte Auftragsnummer %ld Kunde %ld Adresse %ld\n", aufk.auf,
			 kun,kun);
         aufk.adr = dkun;
         aufk.kun_fil = 0;
         aufk.kun = dkun;
         aufk.lieferdat = lieferdat;
         aufk.auf_stat = 3;
         dsqlstatus = kun_class.lese_kun (mdn,fil,dkun);
         if (dsqlstatus)
		 {
		          AufkOK = FALSE;
		           return;
		 }
         dsqlstatus = adr_class.lese_adr (kun.adr2);  //Lieferadresse lesen
         if (dsqlstatus)
		 {
		          AufkOK = FALSE;
		           return;
		 }
         strcpy (aufk.kun_krz1, kun.kun_krz1);
         aufk.tou     = kuntou * 1000;
         aufk.tou_nr  = kuntou;
         aufk.delstatus = 0;
         aufk.zeit_dec = aufk.zeit_dec;
         aufk.kopf_txt = 0;
         aufk.fuss_txt = 0;
         strcpy (aufk.auf_ext, "");
         strcpy (aufk.pers_nam, sys_ben.pers_nam);
         aufk.komm_dat = 0;
         aufk.fix_dat = 0;
         aufk.best_dat = 0;
		 sprintf (aufk.hinweis, "kumuliert auf Tour %ld (%ld) ",tou,kuntou);
		 strcpy (aufk.komm_name, "");

		 if (LieferdatToKommdat)
		 {
			 aufk.komm_dat = aufk.lieferdat;
		 }
         aufk.waehrung = 2;
		 aufk.auf_art = 99;  //"Musterauftrag"    wird nicht berechnet

         ls_class.update_aufk (aufk.mdn, aufk.fil, aufk.auf);



		 return;
}


void KompAuf::ErzeugeLsk (short mdn, short fil, int tou, int dkun, long lieferdat) //EF-69 inaktiv
{
         char datum [12];
		 int dsqlstatus;
         long sysdatum;


		 LskOK = FALSE;
	     WriteLogfile (" Kum.Lieferschein wird erzeugt : Kunde %ld \n", kun);

         sysdate (datum);
         sysdatum = dasc_to_long (datum);
         lsk.mdn = aufk.mdn;
         lsk.fil = aufk.fil;
         lsk.ls  = GenLsNr0 (mdn, fil);
		 if (lsk.ls <= 0l)
		 {
              WriteLogfile (" Fehler beim Generieren der Lieferscheinnummer\n");
			  disp_mess ("Es konnte keine Lieferscheinnummer generiert werden\n"
				         "Der Auftrag wurde nicht �bergeben", 2); 
              LskOK = FALSE;
		      return;
		 }
		 WriteLogfile (" Generierte Lieferscheinnummer %ld Kunde %ld Adresse %ld\n", lsk.ls,
			 kun,kun);
         lsk.auf = -1;
         lsk.adr = dkun;
         lsk.kun_fil = 0;
         lsk.kun = dkun;
         lsk.lieferdat = lieferdat;
		 if (LsKomplett)
		 {
                 lsk.ls_stat = 3;
		 }
		 else
		 {
                 lsk.ls_stat = 2;
		 }
         dsqlstatus = kun_class.lese_kun (mdn,fil,dkun);
         if (dsqlstatus)
		 {
		          LskOK = FALSE;
		           return;
		 }
         dsqlstatus = adr_class.lese_adr (kun.adr2);  //Lieferadresse lesen
         if (dsqlstatus)
		 {
		          LskOK = FALSE;
		           return;
		 }
         strcpy (lsk.kun_krz1, kun.kun_krz1);
         strcpy (lsk.partner, _adr.partner);
         lsk. pr_lst = kun.pr_lst;
         lsk. pr_stu = kun.pr_stu;
         lsk.tou     = tou;
         lsk.tou_nr  = tou;
         strcpy (lsk.adr_nam1, _adr.adr_nam1);
         strcpy (lsk.adr_nam2, _adr.adr_nam2);
         strcpy (lsk.pf, _adr.pf);
         strcpy (lsk.str, _adr.str);
         strcpy (lsk.plz, _adr.plz);
         strcpy (lsk.ort1, _adr.ort1);
         lsk.delstatus = 0;
         lsk.rech = 0l;
         lsk.zeit_dec = aufk.zeit_dec;
         lsk.kopf_txt = 0;
         lsk.fuss_txt = 0;
         strcpy (lsk.auf_ext, "");
         strcpy (lsk.pers_nam, sys_ben.pers_nam);
         lsk.komm_dat = 0;
         lsk.fix_dat = 0;
         lsk.best_dat = 0;
		 strcpy (lsk.komm_name, "");

		 if (LieferdatToKommdat)
		 {
			 lsk.komm_dat = lsk.lieferdat;
		 }
         lsk.waehrung = 2;
         strcpy (lsk.ueb_kz, "N");
		 lsk.auf_art = 99;  //"Musterauftrag"    wird nicht berechnet

		 LskOK = TRUE;
         ls_class.update_lsk (lsk.mdn, lsk.fil, lsk.ls);

		 return;
}





void KompAuf::LskToPid (char *auftraege)
/**
Auftrags-Nummer in das Feld PID in aufp einsetzen.
**/
{
         DbClass.sqlin ((long *)  &lsk.ls, 2, 0);
         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufk.fil, 1, 0);
         DbClass.sqlcomm ("update aufp set pid = ? "
                          "where mdn = ? "
                          "and   fil = ? "
                          "and   auf in ("
                           "%s)", auftraege);

}
void KompAuf::AufToPid (char *auftraege)
/**
Auftrags-Nummer in das Feld PID in aufp einsetzen.
**/
{
         DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufk.fil, 1, 0);
         DbClass.sqlcomm ("update aufp set pid = ? "
                          "where mdn = ? "
                          "and   fil = ? "
                          "and   auf in ("
                           "%s)", auftraege);

}

void KompAuf::ErzeugeLsp (char *auftraege) //EF-69
{
	char order [256];

	int dsqlstatus;
	char buffer [512];
         ls_class.update_lsk (lsk.mdn, lsk.fil, lsk.ls);
         ls_class.close_aufp_kum ();
         LskToPid (auftraege); 
         if (auf_a_sort == 0)
         {
			 sprintf(order, " order by a ");  //posi geht nicht 
              dsqlstatus = ls_class.lese_aufp_kum 
                   (aufk.mdn, aufk.fil, auftraege, order );
         }
         else if (auf_a_sort == 1)
         {
			 sprintf(order, " order by a "); 
              dsqlstatus = ls_class.lese_aufp_kum 
                   (aufk.mdn, aufk.fil, auftraege, order );
         }
         else if (auf_a_sort == 2)
         {
			 sprintf(order, " order by dr_folge,a "); 
              dsqlstatus = ls_class.lese_aufp_kum 
                   (aufk.mdn, aufk.fil, auftraege, order );
         }
         else
         {
			 sprintf(order, " order by a ");   //posi geht nicht
              dsqlstatus = ls_class.lese_aufp_kum 
                   (aufk.mdn, aufk.fil, auftraege, order );
         }
		 int posi = 0;
         while (dsqlstatus == 0)
         {
               posi += 10;
			   aufp.kun = lsk.kun;
               sprintf (buffer, "Auftrag %ld Position %ld Artikel %.0lf wird �bertragen", 
				                 aufp.auf, posi, aufp.a);
			   Mess.Message (buffer);
               AufptoLsp (posi);
               ls_class.update_lsp (lsp.mdn, lsp.fil, lsp.ls,
                                    lsp.a, lsp.posi);
               aufpttolspt (); 
               dsqlstatus = ls_class.lese_aufp_kum ();
         }
         DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
         DbClass.sqlin ((short *) &aufk.fil, 1, 0);
         DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
		 if (LskOK)
		 {
                DbClass.sqlcomm ("update aufk set auf_stat = 6, delstatus = 0 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf in (" 
                           "%s)", auftraege);

		 }
		 else
		 {
                 DbClass.sqlcomm ("update aufk set delstatus = 0 "
                          "where mdn = ? "
                          "and fil = ? "
                          "and auf in (" 
                           "%s)", auftraege);
		 }

		 return;
}

void KompAuf::ErzeugeAufp (char *auftraege) //EF-69a
{
	char order [256];

	int dsqlstatus;
	char buffer [512];
         ls_class.close_aufp_kum ();
         AufToPid (auftraege); 
         if (auf_a_sort == 0)
         {
			 sprintf(order, " order by a ");  //posi geht nicht 
              dsqlstatus = ls_class.lese_aufp_kum 
                   (aufk.mdn, aufk.fil, auftraege, order );
         }
         else if (auf_a_sort == 1)
         {
			 sprintf(order, " order by a "); 
              dsqlstatus = ls_class.lese_aufp_kum 
                   (aufk.mdn, aufk.fil, auftraege, order );
         }
         else if (auf_a_sort == 2)
         {
			 sprintf(order, " order by dr_folge,a "); 
              dsqlstatus = ls_class.lese_aufp_kum 
                   (aufk.mdn, aufk.fil, auftraege, order );
         }
         else
         {
			 sprintf(order, " order by a ");   //posi geht nicht
              dsqlstatus = ls_class.lese_aufp_kum 
                   (aufk.mdn, aufk.fil, auftraege, order );
         }
		 int posi = 0;
         while (dsqlstatus == 0)
         {
               posi += 10;
			   aufp.kun = aufk.kun;
               sprintf (buffer, "Auftrag %ld Position %ld Artikel %.0lf wird �bertragen", 
				                 aufp.auf, posi, aufp.a);
			   Mess.Message (buffer);
			   aufp.auf = aufk.auf;
			   aufp.posi = posi;
               ls_class.update_aufp (aufp.mdn, aufp.fil, aufp.auf,
                                    aufp.a, aufp.posi);
               dsqlstatus = ls_class.lese_aufp_kum ();
         }

		 return;
}

