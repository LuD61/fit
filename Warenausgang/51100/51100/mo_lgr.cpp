#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "itemc.h"
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "mo_meld.h"
#include "mo_lgr.h"

static ITEM iOK ("", "     OK     ", "", 0);
static ITEM iCA ("", "  Abbrechen ", "", 0);

static DB_CLASS DbClass;

static char algr[10];
static char algrbz[37];

static int testquery ();
static int testlgr ();

static ITEM ilgr ("lgr", 
                   algr, 
                  "Lager f�r Standort.......:", 
                   0);

static ITEM ilgrbz  ("lgrbz", 
                      algrbz, 
                     "", 
                      0);

static field _lgrform[] = {
           &ilgr,        9, 0, 1, 1, 0, "%8d", EDIT,     0, testlgr,0,
           &ilgrbz,     40, 0, 2,28, 0,   "",  DISPLAYONLY, 0, 0 ,0,
           &iOK,        15, 0, 4,15, 0,  "",  BUTTON, 0,testquery ,KEY12,
           &iCA,        15, 0, 4,32, 0,  "",  BUTTON, 0,testquery ,KEY5,
};

static form lgrform = {4, 0, 0, _lgrform, 
                       0, 0, 0, 0, NULL};
        

static int testquery (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		}
        return 0;
}


int readlgr (void)
/**
Lager lesen.
**/
{
	    long lgr;
		int dsqlstatus;

        lgr = atol (algr);

		strcpy (algrbz, "");

		DbClass.sqlin ((long *) &lgr, 2, 0);
		DbClass.sqlout ((char *) algrbz, 0, 25);
        dsqlstatus = DbClass.sqlcomm ("select lgr_bz from lgr where lgr = ?");
		return dsqlstatus;
}
   

int testlgr (void)
/**
Lagernummer testen.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
		}
	    if (readlgr () == 100)
		{
			    print_mess (2, "Lager %d nicht gefunden", atol (algr));
				SetCurrentField (currentfield);
				return 0;
		}

		display_field (AktivWindow, &lgrform.mask[1], 0, 0);

        switch (syskey)
        {
                case KEY12 :
                case KEY11 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
		}
		return 0;
}


long LgrClass::DefaultLgr (HWND hWnd, long lgr)
/**
Default-Lager aendern.
**/
{

        HANDLE hMainInst;
		HWND fhWnd;


        HWND lhWnd;
		int savefield;
		form *savecurrent;

        sprintf (algr, "%8d", lgr);
        readlgr ();

        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
		fhWnd = GetFocus ();
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME);
		EnableWindows (hWnd, FALSE);
        lhWnd = OpenWindowChC (7, 62, 9, 10, hMainInst,
                               "");
//        lhWnd = OpenColWindow (7, 62, 9, 10, LTGRAYCOL, NULL);
        enter_form (lhWnd, &lgrform, 0, 0);

		EnableWindows (hWnd, TRUE);
		SetButtonTab (FALSE);
        CloseControls (&lgrform);
        DestroyWindow (lhWnd);
		SetAktivWindow (hWnd);
//		SetActiveWindow (hWnd);
        SetFocus (fhWnd);
		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5) return lgr;
        return atol (algr);
}


