#ifndef _ANGK_DEF
#define _ANFK_DEF
#include "dbclass.h"

struct ANGK {
   short     mdn;
   short     fil;
   long      ang;
   long      adr;
   short     kun_fil;
   long      kun;
   long      lieferdat;
   char      lieferzeit[6];
   char      hinweis[49];
   short     ang_stat;
   char      kun_krz1[17];
   char      feld_bz1[20];
   char      feld_bz2[12];
   char      feld_bz3[8];
   short     delstatus;
   double    zeit_dec;
   long      kopf_txt;
   long      fuss_txt;
   long      vertr;
   char      ang_ext[17];
   long      tou;
   char      pers_nam[9];
   long      komm_dat;
   long      best_dat;
   short     waehrung;
   short     psteuer_kz;
};
extern struct ANGK angk, angk_null;

#line 6 "angk.rh"

class ANGK_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
               short cursor_ausw;
               char aufstring0[0x1000];
               int aufchange;
       public :
               ANGK_CLASS () : cursor_ausw (-1), DB_CLASS ()
               {
                    strcpy (aufstring0, "Start");
               }
               int dbreadfirst (void);

               void ListToMamain1 (HWND);
               int ShowBuAngQuery (HWND, int); 
               int PrepareAngQuery (form *, char **);

               int ShowBuAngQueryEx (HWND, int); 
               int PrepareAngQueryEx (form *, char **);
};
#endif
