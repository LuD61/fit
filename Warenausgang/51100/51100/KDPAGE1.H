// KUNDEN1.h : header file
//

// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1997 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#ifndef _KUNDEN1_H
#define _KUNDEN1_H
/////////////////////////////////////////////////////////////////////////////
// CKunden1Page dialog

class CKunden1Page : public CPropertyPage
{
// Construction
public:
	CKunden1Page();   // standard constructor

// Dialog Data
	//{{AFX_DATA(CKunden1Page)
	enum { IDD = IDD_KUNDEN };
	//}}AFX_DATA
/*	CKunden1 m_AnimateCtrl;
	CRect m_rectAnimateCtrl;
    DWORD m_dwStyle;   // control styles*/
	CWnd m_test;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKunden1Page)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation
protected:


	// Generated message map functions
	//{{AFX_MSG(CKunden1Page)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif
