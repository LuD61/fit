// AdrDialog.cpp: Implementierung der Klasse CAdrDialog.
//
//////////////////////////////////////////////////////////////////////

#include "AdrDialog.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CAdrDialog::CAdrDialog(HWND Parent) : 
			CDialog (Id, Parent)
{
	m_DlgColor  = RGB (255, 255, 204);
	m_DlgBrush  = NULL;
	m_EditColor = RGB (255, 255, 255);
	m_EditBrush  = NULL;
	m_AdrNr = 0l;
	
}

CAdrDialog::~CAdrDialog()
{

}

void CAdrDialog::ReadAdr ()
{
	memcpy (&m_Adr.adr, &_adr, sizeof (ADR));
	if (m_AdrNr != 0l)
	{
		m_Adr.adr.adr = m_AdrNr;
		m_Adr.dbreadfirst ();
	}
}

void CAdrDialog::AttachControls ()
{
    ReadAdr ();
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_NAME,      m_Adr.adr.adr_nam1,  CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_FIRSTNAME, m_Adr.adr.adr_nam2,  CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_STREET,    m_Adr.adr.adr_nam2,  CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_PLZ,       m_Adr.adr.plz,       CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_VILLAGE,   m_Adr.adr.ort1,      CDialogItem::String, 36)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_PHONE,     m_Adr.adr.tel,       CDialogItem::String, 20)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_FAX,       m_Adr.adr.fax,       CDialogItem::String, 20)); 
	m_Controls.push_back (new CDialogItem (m_hWnd, IDC_MAIL,      m_Adr.adr.email,     CDialogItem::String, 36)); 
}

BOOL CAdrDialog::OnInitDialog ()
{
	BOOL ret = CDialog::OnInitDialog ();

	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_NAME,      m_Adr.adr.adr_nam1,  CDialogItem::String, 36)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_FIRSTNAME, m_Adr.adr.adr_nam2,  CDialogItem::String, 36)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_STREET,    m_Adr.adr.adr_nam2,  CDialogItem::String, 36)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_PLZ,       m_Adr.adr.plz,       CDialogItem::String, 36)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_VILLAGE,   m_Adr.adr.ort1,      CDialogItem::String, 36)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_PHONE,     m_Adr.adr.tel,       CDialogItem::String, 20)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_FAX,       m_Adr.adr.fax,       CDialogItem::String, 20)); 
	m_EditItems.push_back (new CDialogItem (m_hWnd, IDC_MAIL,      m_Adr.adr.email,     CDialogItem::String, 36)); 

	Display ();
	return TRUE;
}

BOOL CAdrDialog::OnF5 ()
{
	OnCancel ();
	return TRUE;
}

BOOL CAdrDialog::PreTranslateMessage (MSG *msg)
{
	switch (msg->message)
	{
	case WM_KEYDOWN:
		switch (msg->wParam)
		{
		case VK_F5 :
			return OnF5 ();
		}
	}
	return FALSE;
}

HBRUSH CAdrDialog::OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor)
{
	if (m_DlgBrush == NULL)
	{
		m_DlgBrush = CreateSolidBrush (m_DlgColor);
	}
	if (m_EditBrush == NULL)
	{
		m_EditBrush = CreateSolidBrush (m_EditColor);
	}
	if (nCtlColor == CTLCOLOR_DLG)
	{
			SetBkColor (hDC, m_DlgColor);
			return m_DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (IsEditItem (hWnd))
		{
			SetBkColor (hDC, m_EditColor);
			SetBkMode (hDC, TRANSPARENT);
			return m_EditBrush;
		}
		else
		{
			SetBkColor (hDC, m_DlgColor);
			SetBkMode (hDC, TRANSPARENT);
			return m_DlgBrush;
		}
	}
	return NULL;
}

BOOL CAdrDialog::IsEditItem (HWND hWnd)
{
	BOOL ret = FALSE;

	CDialogItem *item;
	for (vector<CDialogItem *>::iterator it = m_EditItems.begin (); it != m_EditItems.end (); ++it)
	{
		item = *it;
		if (item != NULL)
		{
			if (item->IsControl (hWnd))
			{
				ret = TRUE;
			}
		}
	}
	return ret;
}
