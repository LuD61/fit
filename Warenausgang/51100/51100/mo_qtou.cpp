#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include "itemc.h"
#include "wmaskc.h"
#include "dbfunc.h"
#include "tou.h"
#include "wo_tou.h"
#include "mo_meld.h"
#include "mo_qtou.h"
#include "datum.h"
#include "strfkt.h"
#include "mo_curso.h"

TOU_CLASS tour;
WO_TOU_CLASS WoTour;

static int testquery (void)
/**
Abfrage in Query-Eingabe.
**/
{
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
        }
        return 0;
}


int QueryTou::querytou (HWND hWnd)
/**
Query ueber Touren.
**/
{

        HANDLE hMainInst;

        static char touval[41];
        static char tou_bezval[41];
        static char fz_klaval[41];
        static char fzval[41];
        static char str_zeitval[41];
        static char fah_1val[41];
        static char fah_2val[41];
         
        static ITEM itouval ("tou", 
                             touval, 
                             "Tour.........:", 
                             0);
        static ITEM itou_bezval ("tou_bez",
                             tou_bezval,
                             "Tour-Name....:",
                             0);
        static ITEM ifz_klaval ("fz_kla",
                             fz_klaval,
                             "FZ-Klasse....:", 
                             0);
        static ITEM ifzval ("fz", 
                             fzval, 
                             "Fahrzeug.....:", 
                             0);
        static ITEM istr_zeitval ("srt_zeit",
                             str_zeitval, 
                             "Startzeit....:", 
                             0);
        static ITEM ifah_1val ("fah_1", 
                             fah_1val, 
                             "Fahrer 1.....:", 
                             0);
        static ITEM ifah_2val ("fah_2", 
                             fah_2val, 
                             "Fahrer 2.....:", 
                             0);


        static ITEM iOK ("", "     OK     ", "", 0);
        static ITEM iCA ("", "  Abbrechen ", "", 0);

        static field _qtxtform[] = {
           &itouval,        40, 0, 1,1, 0, "", NORMAL, 0, testquery,0,
           &itou_bezval,    40, 0, 2,1, 0, "", NORMAL, 0, testquery,0,
           &ifz_klaval,     40, 0, 3,1, 0, "", NORMAL, 0, testquery,0,
           &ifzval      ,   40, 0, 4,1, 0, "", NORMAL, 0, testquery,0,
           &istr_zeitval,   40, 0, 5,1, 0, "", NORMAL, 0, testquery,0,
           &ifah_1val,      40, 0, 6,1, 0, "", NORMAL, 0, testquery,0,
           &ifah_2val,      40, 0, 7,1, 0, "", NORMAL, 0, testquery,0,
           &iOK,            15, 0, 9,15, 0, "", BUTTON, 0, 0,KEY12,
           &iCA,            15, 0, 9,32, 0, "", BUTTON, 0, 0,KEY5,
		};

        static form qtxtform = {9, 0, 0, _qtxtform, 
			                    0, 0, 0, 0, NULL};
        
        static char *qnamen[] = {"tou", "tou_bz", "fz_kla", 
                                 "fz", "srt_zeit",
                                 "fah_1", "fah_2",
                                  NULL};

        HWND query;
		int savefield;
		form *savecurrent;

        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);


		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (12, 62,9, 10, hMainInst,
                               "Suchkriterien f�r Touren");
        syskey = 0;
        EnableWindows (hWnd, FALSE); 
        enter_form (query, &qtxtform, 0, 0);

        EnableWindows (hWnd, TRUE); 
        CloseControls (&qtxtform);
        DestroyWindow (query);
		SetActiveWindow (hWnd);
		SetAktivWindow (hWnd);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                  if (tour.PrepareQuery (&qtxtform, qnamen) == 0)
                  {
                             tour.ShowBuQuery (hWnd, 0);
                  }
                  else
                  {
                      syskey = KEY5;
                  }
        }

		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
        if (syskey == KEY5) return FALSE; 
        return TRUE;
}


void QueryTou::infotou (HWND hWnd)
/**
Query ueber Touren.
**/
{

        HANDLE hMainInst;

        static char touval[10];
        static char tou_bezval[50];
        static char fz_klaval[5];
        static char fzval[15];
        static char str_zeitval[7];
        static char dauval [8];
        static char lngval [10];
        static char fah_1val[15];
        static char fah_2val[15];
         
        static ITEM itouval ("tou", 
                             touval, 
                             "Tour.........:", 
                             0);
        static ITEM itou_bezval ("tou_bez",
                             tou_bezval,
                             "Tour-Name....:",
                             0);
        static ITEM ifz_klaval ("fz_kla",
                             fz_klaval,
                             "FZ-Klasse....:", 
                             0);
        static ITEM ifzval ("fz", 
                             fzval, 
                             "Fahrzeug.....:", 
                             0);
        static ITEM istr_zeitval ("srt_zeit",
                             str_zeitval, 
                             "Startzeit....:", 
                             0);
        static ITEM idauval ("dau",
                             dauval, 
                             "Dauer........:", 
                             0);
        static ITEM ilngval ("lng",
                             lngval, 
                             "L�nge........:", 
                             0);
        static ITEM ifah_1val ("fah_1", 
                             fah_1val, 
                             "Fahrer 1.....:", 
                             0);
        static ITEM ifah_2val ("fah_2", 
                             fah_2val, 
                             "Fahrer 2.....:", 
                             0);


        static ITEM iOK ("", "     OK     ", "", 0);
        static ITEM iCA ("", "  Abbrechen ", "", 0);

        static field _dataform[] = {
           &itouval,        40, 0, 1,1, 0,  "", DISPLAYONLY, 0, 0,0,
           &itou_bezval,    40, 0, 2,1, 0,  "", DISPLAYONLY, 0, 0,0,
           &ifz_klaval,     40, 0, 3,1, 0,  "", DISPLAYONLY, 0, 0,0,
           &ifzval      ,   40, 0, 4,1, 0,  "", DISPLAYONLY, 0, 0,0,
           &istr_zeitval,   40, 0, 5,1, 0,  "", DISPLAYONLY, 0, 0,0,
           &idauval,        40, 0, 6,1, 0,  "", DISPLAYONLY, 0, 0,0,
           &ilngval,        40, 0, 7,1, 0,  "", DISPLAYONLY, 0, 0,0,
           &ifah_1val,      40, 0, 8,1, 0,  "", DISPLAYONLY, 0, 0,0,
           &ifah_2val,      40, 0, 9,1, 0,  "", DISPLAYONLY, 0, 0,0,
           &iOK,            15, 0,11,23, 0, "", BUTTON, 0, 0,KEY12,
		};

        static form dataform = {10, 0, 0, _dataform, 
			                     0, 0, 0, 0, NULL};
        static FRMDB dbtou[] = {
         &dataform, 0, (char *) &tou.tou,       2, 0, "%ld",
         &dataform, 1, (char *) tou.tou_bz,    0, 49, "",
         &dataform, 2, (char *) tou.fz_kla,    0,  3, "",
         &dataform, 3, (char *) tou.fz,        0,13, "",
         &dataform, 4, (char *) tou.srt_zeit,  0, 6, "",
         &dataform, 5, (char *) tou.dau,       0, 6, "",
         &dataform, 6, (char *) &tou.lng,       2, 0, "%ld",
         &dataform, 7, (char *) tou.fah_1,     0,13, "",
         &dataform, 8, (char *) tou.fah_2,     0,13, "",
         NULL,      0, NULL, 0, 0, NULL,
        };

        HWND query;
		int savefield;
		form *savecurrent;

        if (tour.dbreadfirst () != 0) return;

        DBtoFrm (dbtou);
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testquery, 5);
        set_fkt (testquery, 11);
        set_fkt (testquery, 12);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);
        query = OpenWindowChC (14, 62,6, 10, hMainInst,
                               "Touren");
        syskey = 0;
        SetButtonTab (TRUE);
        EnableWindows (hWnd, FALSE); 
        DisplayAfterEnter (FALSE); 
        enter_form (query, &dataform, 0, 0);
        DisplayAfterEnter (TRUE); 

        SetButtonTab (FALSE);
        EnableWindows (hWnd, TRUE); 

        CloseControls (&dataform);
        DestroyWindow (query);

		SetActiveWindow (hWnd);
		SetAktivWindow (hWnd);
		currentfield = savefield;
		restore_fkt (5);
        restore_fkt (11);
        restore_fkt (12);
        restore_fkt (6);
        restore_fkt (7);
}

int QueryTou::WoTagPlus (int wo_tag)
/**
Wochentage addieren.
**/
{
	     int i;

		 wo_tag %= 7;
		 for (i = 0; i < wo_tag_plus;)
		 {
			 wo_tag = (wo_tag + 1) % 7;
			 if (wo_tag == 6 && satour == FALSE)
			 {
                 continue;
			 }
			 else if (wo_tag == 0 && sotour == FALSE)
			 {
				 continue;
			 }
			 i ++;
		 }
		 return wo_tag;
}

long QueryTou::DatetoTou (short mdn, short fil, short kun_fil, 
                          long kun, char *datum)
/**
uren.
**/
{
        int dsqlstatus;
        int sys_wo_tag;
        int wo_tag;
        int tz;
        long tou;
        long ldat;

        static long *kun_tou [] = {&wo_tou.so_tour,
                                   &wo_tou.mo_tour, 
                                   &wo_tou.di_tour, 
                                   &wo_tou.mi_tour, 
                                   &wo_tou.do_tour, 
                                   &wo_tou.fr_tour, 
                                   &wo_tou.sa_tour}; 

        wo_tou.mdn     = mdn;
        wo_tou.fil     = fil;
        wo_tou.kun_fil = kun_fil;
        wo_tou.kun     = kun;
        dsqlstatus     = WoTour.dbreadkunfirst ();
        if (dsqlstatus) return 0l; 
        wo_tag = get_wochentag (datum);

        int wt = WoTagPlus (wo_tag);
        tz = wt - wo_tag;
        wo_tag = wt;
		sys_wo_tag = 7;

        tou = 0l;
        for (; tz < 8; tz ++, wo_tag ++)
        {
            if (wo_tag > 6) 
			{
				wo_tag = 0;
			}
            if (wo_tag == sys_wo_tag) break;
            if (*kun_tou[wo_tag] > 0l)
            {
                tou = *kun_tou[wo_tag];
                break;
            }
			if (tz == 1) sys_wo_tag = wo_tag;
        }
        if (tou == 0l) return 0l;
        ldat = dasc_to_long (datum);
        ldat += (long) tz;
        dlong_to_asc (ldat, datum);
        return tou;
}

        
void QueryTou::ToutoDate (short mdn, short fil, short kun_fil, 
                          long kun, char *datum, long tou)
/**
uren.
**/
{
        int dsqlstatus;
        int sys_wo_tag;
        int wo_tag;
        int tz;
        long ldat;
        char dat [12];

        static long *kun_tou [] = {&wo_tou.so_tour,
                                   &wo_tou.mo_tour, 
                                   &wo_tou.di_tour, 
                                   &wo_tou.mi_tour, 
                                   &wo_tou.do_tour, 
                                   &wo_tou.fr_tour, 
                                   &wo_tou.sa_tour}; 
        
        sysdate (dat);
        wo_tou.mdn     = mdn;
        wo_tou.fil     = fil;
        wo_tou.kun_fil = kun_fil;
        wo_tou.kun     = kun;
        dsqlstatus     = WoTour.dbreadkunfirst ();
        if (dsqlstatus) return; 
        wo_tag = get_wochentag (dat);
        sys_wo_tag = wo_tag;
        wo_tag ++;
        for (tz = 1; tz < 7; tz ++, wo_tag ++)
        {
            if (wo_tag > 6) wo_tag = 0;
            if (wo_tag == sys_wo_tag) break;
            if (*kun_tou[wo_tag] == tou)
            {
                break;
            }
        }
        if (tz == 7) return;
        ldat = dasc_to_long (dat);
        ldat += (long) tz;
        dlong_to_asc (ldat, dat);
        strcpy (datum, dat);
}


long QueryTou::DatetoTouDir (short mdn, short fil, short kun_fil, 
                          long kun, char *datum)
/**
Touren.
**/
{
        int dsqlstatus;
        int wo_tag;

        static long *kun_tou [] = {&wo_tou.so_tour,
                                   &wo_tou.mo_tour, 
                                   &wo_tou.di_tour, 
                                   &wo_tou.mi_tour, 
                                   &wo_tou.do_tour, 
                                   &wo_tou.fr_tour, 
                                   &wo_tou.sa_tour,
                                   &wo_tou.so_tour
		};
		 

        wo_tou.mdn     = mdn;
        wo_tou.fil     = fil;
        wo_tou.kun_fil = kun_fil;
        wo_tou.kun     = kun;
        dsqlstatus     = WoTour.dbreadkunfirst ();
        if (dsqlstatus) return 0l; 
        wo_tag = get_wochentag (datum);
        return *kun_tou[wo_tag];
}


        
       
