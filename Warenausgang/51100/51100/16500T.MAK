# Microsoft Developer Studio Generated NMAKE File, Based on 16500T.DSP
!IF "$(CFG)" == ""
CFG=16500T - Win32 Debug
!MESSAGE No configuration specified. Defaulting to 16500T - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "16500T - Win32 Release" && "$(CFG)" != "16500T - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "16500T.MAK" CFG="16500T - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "16500T - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "16500T - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "16500T - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

!IF "$(RECURSE)" == "0" 

ALL : "..\fit\bin\51100.exe"

!ELSE 

ALL : "..\fit\bin\51100.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\A_BAS.OBJ"
	-@erase "$(INTDIR)\A_HNDW.OBJ"
	-@erase "$(INTDIR)\a_kun.obj"
	-@erase "$(INTDIR)\A_PR.OBJ"
	-@erase "$(INTDIR)\AKT_KRZ.OBJ"
	-@erase "$(INTDIR)\AKTION.OBJ"
	-@erase "$(INTDIR)\auftest.obj"
	-@erase "$(INTDIR)\auftest.res"
	-@erase "$(INTDIR)\dbclass.obj"
	-@erase "$(INTDIR)\DBFUNC.OBJ"
	-@erase "$(INTDIR)\FIL.OBJ"
	-@erase "$(INTDIR)\ITEM.OBJ"
	-@erase "$(INTDIR)\itemc.obj"
	-@erase "$(INTDIR)\KUN.OBJ"
	-@erase "$(INTDIR)\listcl.obj"
	-@erase "$(INTDIR)\LS.OBJ"
	-@erase "$(INTDIR)\MDN.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\mo_arg.obj"
	-@erase "$(INTDIR)\mo_aufl.obj"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_DRAW.OBJ"
	-@erase "$(INTDIR)\MO_MELD.OBJ"
	-@erase "$(INTDIR)\MO_MENU.OBJ"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\mo_qa.obj"
	-@erase "$(INTDIR)\PTAB.OBJ"
	-@erase "$(INTDIR)\STDFKT.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\sys_par.obj"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\VERTR.OBJ"
	-@erase "$(INTDIR)\wmaskc.obj"
	-@erase "..\fit\bin\51100.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /ML /W3 /GX /I "d:\informix\incl\esql" /D "WIN32" /D "NDEBUG"\
 /D "_WINDOWS" /Fp"$(INTDIR)\16500T.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\"\
 /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
RSC_PROJ=/l 0x407 /fo"$(INTDIR)\auftest.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\16500T.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib isqlt07c.lib comctl32.lib /nologo /subsystem:windows\
 /incremental:no /pdb:"$(OUTDIR)\51100.pdb" /machine:I386\
 /out:"\user\fit\bin\51100.exe" /libpath:"d:\informix\lib" 
LINK32_OBJS= \
	"$(INTDIR)\A_BAS.OBJ" \
	"$(INTDIR)\A_HNDW.OBJ" \
	"$(INTDIR)\a_kun.obj" \
	"$(INTDIR)\A_PR.OBJ" \
	"$(INTDIR)\AKT_KRZ.OBJ" \
	"$(INTDIR)\AKTION.OBJ" \
	"$(INTDIR)\auftest.obj" \
	"$(INTDIR)\auftest.res" \
	"$(INTDIR)\dbclass.obj" \
	"$(INTDIR)\DBFUNC.OBJ" \
	"$(INTDIR)\FIL.OBJ" \
	"$(INTDIR)\ITEM.OBJ" \
	"$(INTDIR)\itemc.obj" \
	"$(INTDIR)\KUN.OBJ" \
	"$(INTDIR)\listcl.obj" \
	"$(INTDIR)\LS.OBJ" \
	"$(INTDIR)\MDN.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\mo_arg.obj" \
	"$(INTDIR)\mo_aufl.obj" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_DRAW.OBJ" \
	"$(INTDIR)\MO_MELD.OBJ" \
	"$(INTDIR)\MO_MENU.OBJ" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\mo_qa.obj" \
	"$(INTDIR)\PTAB.OBJ" \
	"$(INTDIR)\STDFKT.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\sys_par.obj" \
	"$(INTDIR)\VERTR.OBJ" \
	"$(INTDIR)\wmaskc.obj" \
	".\inflib.lib"

"..\fit\bin\51100.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\16500T.exe"

!ELSE 

ALL : "$(OUTDIR)\16500T.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\A_BAS.OBJ"
	-@erase "$(INTDIR)\A_HNDW.OBJ"
	-@erase "$(INTDIR)\a_kun.obj"
	-@erase "$(INTDIR)\A_PR.OBJ"
	-@erase "$(INTDIR)\AKT_KRZ.OBJ"
	-@erase "$(INTDIR)\AKTION.OBJ"
	-@erase "$(INTDIR)\auftest.obj"
	-@erase "$(INTDIR)\auftest.res"
	-@erase "$(INTDIR)\dbclass.obj"
	-@erase "$(INTDIR)\DBFUNC.OBJ"
	-@erase "$(INTDIR)\FIL.OBJ"
	-@erase "$(INTDIR)\ITEM.OBJ"
	-@erase "$(INTDIR)\itemc.obj"
	-@erase "$(INTDIR)\KUN.OBJ"
	-@erase "$(INTDIR)\listcl.obj"
	-@erase "$(INTDIR)\LS.OBJ"
	-@erase "$(INTDIR)\MDN.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\mo_arg.obj"
	-@erase "$(INTDIR)\mo_aufl.obj"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_DRAW.OBJ"
	-@erase "$(INTDIR)\MO_MELD.OBJ"
	-@erase "$(INTDIR)\MO_MENU.OBJ"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\mo_qa.obj"
	-@erase "$(INTDIR)\PTAB.OBJ"
	-@erase "$(INTDIR)\STDFKT.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\sys_par.obj"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(INTDIR)\VERTR.OBJ"
	-@erase "$(INTDIR)\wmaskc.obj"
	-@erase "$(OUTDIR)\16500T.exe"
	-@erase "$(OUTDIR)\16500T.ilk"
	-@erase "$(OUTDIR)\16500T.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /Zi /Od /I "d:\informix\incl\esql" /D "WIN32"\
 /D "_DEBUG" /D "_WINDOWS" /Fp"$(INTDIR)\16500T.pch" /YX /Fo"$(INTDIR)\\"\
 /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
RSC_PROJ=/l 0x407 /fo"$(INTDIR)\auftest.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\16500T.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib isqlt07c.lib comctl32.lib /nologo /subsystem:windows\
 /incremental:yes /pdb:"$(OUTDIR)\16500T.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)\16500T.exe" /pdbtype:sept /libpath:"d:\informix\lib" 
LINK32_OBJS= \
	"$(INTDIR)\A_BAS.OBJ" \
	"$(INTDIR)\A_HNDW.OBJ" \
	"$(INTDIR)\a_kun.obj" \
	"$(INTDIR)\A_PR.OBJ" \
	"$(INTDIR)\AKT_KRZ.OBJ" \
	"$(INTDIR)\AKTION.OBJ" \
	"$(INTDIR)\auftest.obj" \
	"$(INTDIR)\auftest.res" \
	"$(INTDIR)\dbclass.obj" \
	"$(INTDIR)\DBFUNC.OBJ" \
	"$(INTDIR)\FIL.OBJ" \
	"$(INTDIR)\ITEM.OBJ" \
	"$(INTDIR)\itemc.obj" \
	"$(INTDIR)\KUN.OBJ" \
	"$(INTDIR)\listcl.obj" \
	"$(INTDIR)\LS.OBJ" \
	"$(INTDIR)\MDN.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\mo_arg.obj" \
	"$(INTDIR)\mo_aufl.obj" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_DRAW.OBJ" \
	"$(INTDIR)\MO_MELD.OBJ" \
	"$(INTDIR)\MO_MENU.OBJ" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\mo_qa.obj" \
	"$(INTDIR)\PTAB.OBJ" \
	"$(INTDIR)\STDFKT.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\sys_par.obj" \
	"$(INTDIR)\VERTR.OBJ" \
	"$(INTDIR)\wmaskc.obj" \
	".\inflib.lib"

"$(OUTDIR)\16500T.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(CFG)" == "16500T - Win32 Release" || "$(CFG)" == "16500T - Win32 Debug"
SOURCE=.\A_BAS.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_A_BAS=\
	".\a_bas.h"\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_BAS.OBJ" : $(SOURCE) $(DEP_CPP_A_BAS) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_A_BAS=\
	".\a_bas.h"\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_BAS.OBJ" : $(SOURCE) $(DEP_CPP_A_BAS) "$(INTDIR)"


!ENDIF 

SOURCE=.\A_HNDW.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_A_HND=\
	".\a_hndw.h"\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_HNDW.OBJ" : $(SOURCE) $(DEP_CPP_A_HND) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_A_HND=\
	".\a_hndw.h"\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_HNDW.OBJ" : $(SOURCE) $(DEP_CPP_A_HND) "$(INTDIR)"


!ENDIF 

SOURCE=.\a_kun.cpp

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_A_KUN=\
	".\a_bas.h"\
	".\a_kun.h"\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\a_kun.obj" : $(SOURCE) $(DEP_CPP_A_KUN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_A_KUN=\
	".\a_bas.h"\
	".\a_kun.h"\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\a_kun.obj" : $(SOURCE) $(DEP_CPP_A_KUN) "$(INTDIR)"


!ENDIF 

SOURCE=.\A_PR.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_A_PR_=\
	".\a_pr.h"\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_PR.OBJ" : $(SOURCE) $(DEP_CPP_A_PR_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_A_PR_=\
	".\a_pr.h"\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_PR.OBJ" : $(SOURCE) $(DEP_CPP_A_PR_) "$(INTDIR)"


!ENDIF 

SOURCE=.\AKT_KRZ.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_AKT_K=\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\AKT_KRZ.OBJ" : $(SOURCE) $(DEP_CPP_AKT_K) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_AKT_K=\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\AKT_KRZ.OBJ" : $(SOURCE) $(DEP_CPP_AKT_K) "$(INTDIR)"


!ENDIF 

SOURCE=.\AKTION.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_AKTIO=\
	".\aktion.h"\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\AKTION.OBJ" : $(SOURCE) $(DEP_CPP_AKTIO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_AKTIO=\
	".\aktion.h"\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\AKTION.OBJ" : $(SOURCE) $(DEP_CPP_AKTIO) "$(INTDIR)"


!ENDIF 

SOURCE=.\auftest.cpp

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_AUFTE=\
	".\a_bas.h"\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\fil.h"\
	".\inflib.h"\
	".\itemc.h"\
	".\kun.h"\
	".\listcl.h"\
	".\ls.h"\
	".\mdn.h"\
	".\mdnfil.h"\
	".\mo_arg.h"\
	".\mo_aufl.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_intp.h"\
	".\mo_meld.h"\
	".\mo_menu.h"\
	".\mo_numme.h"\
	".\mo_qa.h"\
	".\ptab.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\vertr.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\auftest.obj" : $(SOURCE) $(DEP_CPP_AUFTE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_AUFTE=\
	".\a_bas.h"\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\fil.h"\
	".\inflib.h"\
	".\itemc.h"\
	".\kun.h"\
	".\listcl.h"\
	".\ls.h"\
	".\mdn.h"\
	".\mdnfil.h"\
	".\mo_arg.h"\
	".\mo_aufl.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_intp.h"\
	".\mo_meld.h"\
	".\mo_menu.h"\
	".\mo_numme.h"\
	".\mo_qa.h"\
	".\ptab.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\vertr.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\auftest.obj" : $(SOURCE) $(DEP_CPP_AUFTE) "$(INTDIR)"


!ENDIF 

SOURCE=.\auftest.rc
DEP_RSC_AUFTES=\
	".\comcthlp.h"\
	".\fax.ico"\
	".\fit.ico"\
	".\mo_meld.h"\
	".\NoIcon.ico"\
	".\oarrow.cur"\
	".\telefon.bmp"\
	".\telefon.ico"\
	".\telefoni.bmp"\
	".\Toolbar.bmp"\
	

"$(INTDIR)\auftest.res" : $(SOURCE) $(DEP_RSC_AUFTES) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\dbclass.cpp

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_DBCLA=\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\dbclass.obj" : $(SOURCE) $(DEP_CPP_DBCLA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_DBCLA=\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\dbclass.obj" : $(SOURCE) $(DEP_CPP_DBCLA) "$(INTDIR)"


!ENDIF 

SOURCE=.\DBFUNC.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_DBFUN=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\DBFUNC.OBJ" : $(SOURCE) $(DEP_CPP_DBFUN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_DBFUN=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\DBFUNC.OBJ" : $(SOURCE) $(DEP_CPP_DBFUN) "$(INTDIR)"


!ENDIF 

SOURCE=.\FIL.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_FIL_C=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\fil.h"\
	".\itemc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\FIL.OBJ" : $(SOURCE) $(DEP_CPP_FIL_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_FIL_C=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\fil.h"\
	".\itemc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\FIL.OBJ" : $(SOURCE) $(DEP_CPP_FIL_C) "$(INTDIR)"


!ENDIF 

SOURCE=.\ITEM.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_ITEM_=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\item.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\ITEM.OBJ" : $(SOURCE) $(DEP_CPP_ITEM_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_ITEM_=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\item.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\ITEM.OBJ" : $(SOURCE) $(DEP_CPP_ITEM_) "$(INTDIR)"


!ENDIF 

SOURCE=.\itemc.cpp

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_ITEMC=\
	".\inflib.h"\
	".\itemc.h"\
	

"$(INTDIR)\itemc.obj" : $(SOURCE) $(DEP_CPP_ITEMC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_ITEMC=\
	".\inflib.h"\
	".\itemc.h"\
	

"$(INTDIR)\itemc.obj" : $(SOURCE) $(DEP_CPP_ITEMC) "$(INTDIR)"


!ENDIF 

SOURCE=.\KUN.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_KUN_C=\
	".\a_kun.h"\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\inflib.h"\
	".\itemc.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\KUN.OBJ" : $(SOURCE) $(DEP_CPP_KUN_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_KUN_C=\
	".\a_kun.h"\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\inflib.h"\
	".\itemc.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\KUN.OBJ" : $(SOURCE) $(DEP_CPP_KUN_C) "$(INTDIR)"


!ENDIF 

SOURCE=.\listcl.cpp

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_LISTC=\
	".\comcthlp.h"\
	".\inflib.h"\
	".\item.h"\
	".\itemc.h"\
	".\listcl.h"\
	".\mo_draw.h"\
	".\mo_intp.h"\
	".\mo_meld.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\listcl.obj" : $(SOURCE) $(DEP_CPP_LISTC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_LISTC=\
	".\comcthlp.h"\
	".\inflib.h"\
	".\item.h"\
	".\itemc.h"\
	".\listcl.h"\
	".\mo_draw.h"\
	".\mo_intp.h"\
	".\mo_meld.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\listcl.obj" : $(SOURCE) $(DEP_CPP_LISTC) "$(INTDIR)"


!ENDIF 

SOURCE=.\LS.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_LS_CP=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\ls.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\LS.OBJ" : $(SOURCE) $(DEP_CPP_LS_CP) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_LS_CP=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\ls.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\LS.OBJ" : $(SOURCE) $(DEP_CPP_LS_CP) "$(INTDIR)"


!ENDIF 

SOURCE=.\MDN.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_MDN_C=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MDN.OBJ" : $(SOURCE) $(DEP_CPP_MDN_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_MDN_C=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MDN.OBJ" : $(SOURCE) $(DEP_CPP_MDN_C) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_A_PR.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_MO_A_=\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\fil.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_A_PR.OBJ" : $(SOURCE) $(DEP_CPP_MO_A_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_MO_A_=\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\fil.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_A_PR.OBJ" : $(SOURCE) $(DEP_CPP_MO_A_) "$(INTDIR)"


!ENDIF 

SOURCE=.\mo_arg.cpp

"$(INTDIR)\mo_arg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mo_aufl.cpp

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_MO_AU=\
	".\a_bas.h"\
	".\a_hndw.h"\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\fil.h"\
	".\inflib.h"\
	".\itemc.h"\
	".\kun.h"\
	".\listcl.h"\
	".\ls.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_arg.h"\
	".\mo_aufl.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_intp.h"\
	".\mo_meld.h"\
	".\mo_preis.h"\
	".\mo_qa.h"\
	".\ptab.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\sys_par.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\mo_aufl.obj" : $(SOURCE) $(DEP_CPP_MO_AU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_MO_AU=\
	".\a_bas.h"\
	".\a_hndw.h"\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\fil.h"\
	".\inflib.h"\
	".\itemc.h"\
	".\kun.h"\
	".\listcl.h"\
	".\ls.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_arg.h"\
	".\mo_aufl.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_intp.h"\
	".\mo_meld.h"\
	".\mo_preis.h"\
	".\mo_qa.h"\
	".\ptab.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\sys_par.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\mo_aufl.obj" : $(SOURCE) $(DEP_CPP_MO_AU) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_CURSO.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_MO_CU=\
	".\mo_curso.h"\
	".\strfkt.h"\
	".\tcursor.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_MO_CU=\
	".\mo_curso.h"\
	".\strfkt.h"\
	".\tcursor.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_DRAW.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_MO_DR=\
	".\itemc.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\MO_DRAW.OBJ" : $(SOURCE) $(DEP_CPP_MO_DR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_MO_DR=\
	".\itemc.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\MO_DRAW.OBJ" : $(SOURCE) $(DEP_CPP_MO_DR) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_MELD.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_MO_ME=\
	".\comcthlp.h"\
	".\fil.h"\
	".\itemc.h"\
	".\mdn.h"\
	".\mdnfil.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_menu.h"\
	".\stdfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\MO_MELD.OBJ" : $(SOURCE) $(DEP_CPP_MO_ME) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_MO_ME=\
	".\comcthlp.h"\
	".\fil.h"\
	".\itemc.h"\
	".\mdn.h"\
	".\mdnfil.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_menu.h"\
	".\stdfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\MO_MELD.OBJ" : $(SOURCE) $(DEP_CPP_MO_ME) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_MENU.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_MO_MEN=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_menu.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_MENU.OBJ" : $(SOURCE) $(DEP_CPP_MO_MEN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_MO_MEN=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_menu.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_MENU.OBJ" : $(SOURCE) $(DEP_CPP_MO_MEN) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_NUMME.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_MO_NU=\
	".\comcthlp.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_numme.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_NUMME.OBJ" : $(SOURCE) $(DEP_CPP_MO_NU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_MO_NU=\
	".\comcthlp.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_numme.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_NUMME.OBJ" : $(SOURCE) $(DEP_CPP_MO_NU) "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_PREIS.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_MO_PR=\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\dbclass.h"\
	".\fil.h"\
	".\itemc.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_preis.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_PREIS.OBJ" : $(SOURCE) $(DEP_CPP_MO_PR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_MO_PR=\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\dbclass.h"\
	".\fil.h"\
	".\itemc.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_preis.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_PREIS.OBJ" : $(SOURCE) $(DEP_CPP_MO_PR) "$(INTDIR)"


!ENDIF 

SOURCE=.\mo_qa.cpp

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_MO_QA=\
	".\a_bas.h"\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\kun.h"\
	".\ls.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_qa.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\mo_qa.obj" : $(SOURCE) $(DEP_CPP_MO_QA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_MO_QA=\
	".\a_bas.h"\
	".\comcthlp.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\kun.h"\
	".\ls.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_qa.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\mo_qa.obj" : $(SOURCE) $(DEP_CPP_MO_QA) "$(INTDIR)"


!ENDIF 

SOURCE=.\PTAB.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_PTAB_=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\ptab.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\PTAB.OBJ" : $(SOURCE) $(DEP_CPP_PTAB_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_PTAB_=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\ptab.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\PTAB.OBJ" : $(SOURCE) $(DEP_CPP_PTAB_) "$(INTDIR)"


!ENDIF 

SOURCE=.\STDFKT.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_STDFK=\
	".\itemc.h"\
	".\mo_draw.h"\
	".\stdfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\STDFKT.OBJ" : $(SOURCE) $(DEP_CPP_STDFK) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_STDFK=\
	".\itemc.h"\
	".\mo_draw.h"\
	".\stdfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\STDFKT.OBJ" : $(SOURCE) $(DEP_CPP_STDFK) "$(INTDIR)"


!ENDIF 

SOURCE=.\STRFKT.CPP
DEP_CPP_STRFK=\
	".\strfkt.h"\
	

"$(INTDIR)\STRFKT.OBJ" : $(SOURCE) $(DEP_CPP_STRFK) "$(INTDIR)"


SOURCE=.\sys_par.cpp

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_SYS_P=\
	".\mo_curso.h"\
	".\sys_par.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\sys_par.obj" : $(SOURCE) $(DEP_CPP_SYS_P) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_SYS_P=\
	".\mo_curso.h"\
	".\sys_par.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\sys_par.obj" : $(SOURCE) $(DEP_CPP_SYS_P) "$(INTDIR)"


!ENDIF 

SOURCE=.\VERTR.CPP

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_VERTR=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\vertr.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\VERTR.OBJ" : $(SOURCE) $(DEP_CPP_VERTR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_VERTR=\
	".\comcthlp.h"\
	".\dbfunc.h"\
	".\itemc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\vertr.h"\
	".\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\VERTR.OBJ" : $(SOURCE) $(DEP_CPP_VERTR) "$(INTDIR)"


!ENDIF 

SOURCE=.\wmaskc.cpp

!IF  "$(CFG)" == "16500T - Win32 Release"

DEP_CPP_WMASK=\
	".\comcthlp.h"\
	".\itemc.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\wmaskc.obj" : $(SOURCE) $(DEP_CPP_WMASK) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "16500T - Win32 Debug"

DEP_CPP_WMASK=\
	".\comcthlp.h"\
	".\itemc.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\wmaskc.h"\
	

"$(INTDIR)\wmaskc.obj" : $(SOURCE) $(DEP_CPP_WMASK) "$(INTDIR)"


!ENDIF 


!ENDIF 

