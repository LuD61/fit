#ifndef _AUFKUN_DEF
#define _AUFKUN_DEF

#include "dbclass.h"

struct AUFKUN {
   short     mdn;
   short     fil;
   long      kun;
   long      auf;
   double    a;
   short     kun_fil;
   long      lfd;
   long      bearb;
   long      lieferdat;
   double    auf_me;
   double    lief_me;
   double    auf_vk_pr;
};
extern struct AUFKUN aufkun, aufkun_null;

#line 7 "aufkun.rh"

class AUFKUN_CLASS : public DB_CLASS 
{
       private :
               int cursorlast; 
               void prepare (void);
       public :
               AUFKUN_CLASS () : DB_CLASS (), cursorlast (-1) 
               {
               }
               int dbreadfirst (void);
               int dbreadlast (void);
               int dbreadnextlast (void);
               int dbupdatelast0 (long); 
               int dbupdatelast (long); 
               int dbclose (void); 
};
#endif
