// GrpPrice.cpp: Implementierung der Klasse GrpPrice.
//
//////////////////////////////////////////////////////////////////////

#include "GrpPrice.h"
#include "Debug.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CGrpPrice::CGrpPrice()
{
	SetKeys (1, 0, 0, 0, 0);
	AGewCursor = -1;
}

CGrpPrice::~CGrpPrice()
{
	if (AGewCursor != -1)
	{
		Ipr.sqlclose (AGewCursor);
	}

}

void CGrpPrice::SetAKey (double a)
{
	if (AGewCursor == -1)
	{
		Ipr.sqlin ((double *) &ipr.a, 3, 0);
		Ipr.sqlout ((double *) &a_gew, 3, 0);
		Ipr.sqlout ((short *) &me_einh, 1, 0);
		AGewCursor = Ipr.sqlcursor ("select a_gew, me_einh from a_bas where a = ?");
	}
	me_einh = 2;
	a_gew = 1.0;
    ipr.a = a;
	Ipr.sqlopen (AGewCursor);
	if (Ipr.sqlfetch (AGewCursor) == 0)
	{
		if (me_einh == 2)
		{
			a_gew = 1.0;
		}
	}
}


void CGrpPrice::SetAKey (double a, short me_einh, double a_gew)
{
	this->me_einh = me_einh;
	this->a_gew = a_gew;
    ipr.a = a;
	if (this->me_einh == 2)
	{
		this->a_gew = 1.0;
	}
}

BOOL CGrpPrice::Read (double *pVkPr, double *pLdPr)
{
	SetKeys (1, 0, 0, 0, ipr.a);
	int dsqlstatus = Ipr.dbreadfirst ();
	if (dsqlstatus != 0)
	{
		return FALSE;
	}
	if (me_einh != 2)
	{
		ipr.vk_pr_eu *= a_gew;
		ipr.ld_pr_eu *= a_gew;
	}
	*pVkPr = ipr.vk_pr_eu;
	*pLdPr = ipr.ld_pr_eu;
	return TRUE;
}

double CGrpPrice::CalculateVkPr (double pr)
{
	double marge = 0.0;
	SetKeys (1, 0, 0, 0, ipr.a);
	int dsqlstatus = Ipr.dbreadfirst ();
	if (dsqlstatus != 0)
	{
		return FALSE;
	}
	if (pr != 0.0)
	{
		if (me_einh != 2)
		{
			ipr.vk_pr_eu *= a_gew;
		}
		marge = ((ipr.vk_pr_eu / pr) - 1) * 100;
	}
	return marge;
}

double CGrpPrice::CalculateLdPr (double pr)
{
	double marge = 0.0;
	SetKeys (1, 0, 0, 0, ipr.a);
	int dsqlstatus = Ipr.dbreadfirst ();
	if (dsqlstatus != 0)
	{
		return FALSE;
	}
	if (me_einh != 2)
	{
		ipr.ld_pr_eu *= a_gew;
	}
	if (pr != 0.0)
	{
		marge = ((ipr.ld_pr_eu / pr) - 1) * 100;
	}
	return marge;
}


void CGrpPrice::CalculateVkPr (double pr, LPSTR sMarge, int size)
{
	double marge = 0.0;
	char m[20];
	SetKeys (1, 0, 0, 0, ipr.a);
	int dsqlstatus = Ipr.dbreadfirst ();
	if (dsqlstatus == 0 && pr != 0.0)
	{
		if (me_einh != 2)
		{
			ipr.vk_pr_eu *= a_gew;
		}
		marge = ((ipr.vk_pr_eu / pr) - 1) * 100;
	}
	sprintf (m, "%.2lf", marge);
	memset (sMarge, 0, size);
    strncpy (sMarge, m, size - 1); 
}

void CGrpPrice::CalculateLdPr (double pr, LPSTR sMarge, int size)
{
	double marge = 0.0;
	char m[20];
	SetKeys (1, 0, 0, 0, ipr.a);
	CDebug *Debug = CDebug::GetInstance ();
	sprintf (Debug->GetLine (), "Berechne Marge, Artike %.0lf Gewicht %.3lf", ipr.a, a_gew); 
	Debug->Write ();
	sprintf (Debug->GetLine (), "mdn %hd pr_gr_stuf %d kun_pr %d kun %d a %.0lf", ipr.mdn, 
		                                                                          ipr.pr_gr_stuf, 
																				  ipr.kun_pr,
																				  ipr.kun,
																				  ipr.a); 
	Debug->Write ();
	int dsqlstatus = Ipr.dbreadfirst ();
	if (dsqlstatus == 0 && pr != 0.0)
	{
		sprintf (Debug->GetLine (), "GruppenPreis %.2lf Ladenepreis Stufe 0 %.2lf", pr, ipr.ld_pr_eu); 
		Debug->Write ();
		if (me_einh != 2)
		{
			sprintf (Debug->GetLine (), "Ladenepreis Stufe 0 * Gewicht %.2lf * %.3lf", ipr.ld_pr_eu, a_gew); 
			Debug->Write ();
			ipr.ld_pr_eu *= a_gew;
			sprintf (Debug->GetLine (), "Ergebnis %.2lf", ipr.ld_pr_eu); 
			Debug->Write ();
		}
		marge = ((ipr.ld_pr_eu / pr) - 1) * 100;
		sprintf (Debug->GetLine (), "Berechnung ((%.2lf / %.2lf) - 1) * 100 = %.2lf", ipr.ld_pr_eu, pr, marge); 
		Debug->Write ();
	}
	sprintf (m, "%.2lf", marge);
	memset (sMarge, 0, size);
    strncpy (sMarge, m, size - 1); 
	sprintf (Debug->GetLine (), "Errechnete Marge = %s", sMarge); 
	Debug->Write ();
}

void CGrpPrice::CalculateLdPr (double pr, LPSTR sMarge, int size, LPSTR sLdPr, int sizeLdPr)
{
	double marge = 0.0;
	char m[20];
	char lp[20];
	SetKeys (1, 0, 0, 0, ipr.a);
	CDebug *Debug = CDebug::GetInstance ();
	sprintf (Debug->GetLine (), "Berechne Marge, Artike %.0lf Gewicht %.3lf", ipr.a, a_gew); 
	Debug->Write ();
	sprintf (Debug->GetLine (), "mdn %hd pr_gr_stuf %d kun_pr %d kun %d a %.0lf", ipr.mdn, 
		                                                                          ipr.pr_gr_stuf, 
																				  ipr.kun_pr,
																				  ipr.kun,
																				  ipr.a); 
	Debug->Write ();
	int dsqlstatus = Ipr.dbreadfirst ();
	if (dsqlstatus == 0 && pr != 0.0)
	{
		sprintf (Debug->GetLine (), "GruppenPreis %.2lf Ladenepreis Stufe 0 %.2lf", pr, ipr.ld_pr_eu); 
		Debug->Write ();
		if (me_einh != 2)
		{
			sprintf (Debug->GetLine (), "Ladenepreis Stufe 0 * Gewicht %.2lf * %.3lf", ipr.ld_pr_eu, a_gew); 
			Debug->Write ();
			ipr.ld_pr_eu *= a_gew;
			sprintf (Debug->GetLine (), "Ergebnis %.2lf", ipr.ld_pr_eu); 
			Debug->Write ();
		}
		if (ipr.ld_pr_eu == 0.0 || pr == 0.0)
		{
			marge = 0.0;
		}
		else
		{
			marge = ((ipr.ld_pr_eu / pr) - 1) * 100;
		}
		sprintf (Debug->GetLine (), "Berechnung ((%.2lf / %.2lf) - 1) * 100 = %.2lf", ipr.ld_pr_eu, pr, marge); 
		Debug->Write ();
	}
	sprintf (m, "%.2lf", marge);
	sprintf (lp, "%.2lf", ipr.ld_pr_eu);
	memset (sMarge, 0, size);
    strncpy (sMarge, m, size - 1); 
	memset (sLdPr, 0, sizeLdPr);
    strncpy (sLdPr, lp, sizeLdPr - 1); 
	sprintf (Debug->GetLine (), "Errechnete Marge = %s", sMarge); 
	Debug->Write ();
}
