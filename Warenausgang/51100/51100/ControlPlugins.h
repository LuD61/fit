#ifndef _CONTROL_PLUGINS_DEF
#define _CONTROL_PLUGINS_DEF
#include <windows.h>

class CControlPlugins
{
public:
	enum
	{
		KommOnly = 0,
		LiefVisible = 1,
		LiefRechVisible = 2,
	};
	static HANDLE Controls;
    int (*pGetPosTxtKz) (int PosTxtKz);
	CControlPlugins ();
	int GetPosTxtKz (int PosTxtKz);
	BOOL IsActive ();
};

#endif
