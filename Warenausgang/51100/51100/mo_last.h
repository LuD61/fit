#ifndef _MO_LAST_DEF
#define _MO_LASR_DEF

#define MAXAUFA 201

struct AUFA
{
    long auf;
    double a;
};

extern struct AUFA aufa [MAXAUFA];

class LastAuf : DB_CLASS
{
        private :
            int anzart;
            struct AUFA aufa [MAXAUFA];
            int aufa_anz;
            int aufa_pos;
        public :
            LastAuf () : DB_CLASS (), anzart (10), aufa_anz (0), aufa_pos (0)
            {
            }
           void AnzArt (int anz)
           {
                 anzart = anz;
           }
           void arttotab (long, double);
           void ReadLstAuf (short, long);
           void ReadLstLief (short, long);
           BOOL GetFirst (long *, double *);
           BOOL GetNext (long *, double *);
           BOOL GetPrior (long *, double *);
           BOOL GetCurrent (long *, double *);
           BOOL GetAbsolute (long *, double *, int);
};
#endif
