#ifndef _DADEF
#define _DADEF
extern "C" {
short get_jahr(char * datum);             // Funktion liefert jahr (short) 
short get_monat(char * datum);            // Funktion liefert monat(short)
short get_tag(char * datum);              // Funktion liefert tag (short)
long get_dat_long(char *datum,int laenge);			  // Funktion liefert tag (long) 	
short get_lauf_tag(char * datum);         // Funktion liefert laufenden Tag im Jahr
short get_schalt_jahr(short jahr);        // Funktion liefert 1/0 wenn Schalt-Jahr
short get_wochentag(char * datum);        // Funktion liefert Wochentag
short get_woche(char * datum);            // Funktion liefert Kalenderwoche im Jahr
}
#endif
