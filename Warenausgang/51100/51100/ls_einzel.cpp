//030812  Mengeneinheit stimmte bei den Beilagen nicht immer auf der Kommissionierliste
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "ls_einzel.h"
#include "ls.h"
#include "a_bas.h"
#include "kun.h"
#include "ptab.h"
#include "dbfunc.h"

struct LSPEINZEL lspeinzel, lspeinzel_null;

static char sqlstring [0x1000];

extern class LS_CLASS ls_class;
static PTAB_CLASS ptab_class;

int AG_BEILAGE = 5; 



void LS_EINZEL_CLASS::prepare  (void)
{

}


void LS_EINZEL_CLASS::lsp_out (void)
{
    out_quest ((char *) &lspeinzel.mdn,1,0);
    out_quest ((char *) &lspeinzel.fil,1,0);
    out_quest ((char *) &lspeinzel.ls,2,0);
    out_quest ((char *) &lspeinzel.auf,2,0);
    out_quest ((char *) &lspeinzel.a,3,0);
    out_quest ((char *) &lspeinzel.auf_me,3,0);
    out_quest ((char *) lspeinzel.auf_me_bz,0,7);
    out_quest ((char *) &lspeinzel.lief_me,3,0);
    out_quest ((char *) lspeinzel.lief_me_bz,0,7);
    out_quest ((char *) &lspeinzel.ls_vk_pr,3,0);
    out_quest ((char *) &lspeinzel.ls_lad_pr,3,0);
    out_quest ((char *) &lspeinzel.delstatus,1,0);
    out_quest ((char *) &lspeinzel.tara,3,0);
    out_quest ((char *) &lspeinzel.posi,2,0);
    out_quest ((char *) &lspeinzel.lsp_txt,2,0);
    out_quest ((char *) &lspeinzel.pos_stat,1,0);
    out_quest ((char *) &lspeinzel.sa_kz_sint,1,0);
    out_quest ((char *) lspeinzel.erf_kz,0,2);
    out_quest ((char *) &lspeinzel.prov_satz,3,0);
    out_quest ((char *) &lspeinzel.leer_pos,1,0);
    out_quest ((char *) &lspeinzel.hbk_date,2,0);
    out_quest ((char *) lspeinzel.ls_charge,0,sizeof (lspeinzel.ls_charge));
    out_quest ((char *) &lspeinzel.auf_me_vgl,3,0);
    out_quest ((char *) &lspeinzel.ls_vk_euro,3,0);
    out_quest ((char *) &lspeinzel.ls_vk_fremd,3,0);
    out_quest ((char *) &lspeinzel.ls_lad_euro,3,0);
    out_quest ((char *) &lspeinzel.ls_lad_fremd,3,0);
    out_quest ((char *) &lspeinzel.rab_satz,3,0);
    out_quest ((char *) &lspeinzel.me_einh_kun,1,0);
    out_quest ((char *) &lspeinzel.me_einh,1,0);
    out_quest ((char *) &lspeinzel.me_einh_kun1,1,0);
    out_quest ((char *) &lspeinzel.auf_me1,3,0);
    out_quest ((char *) &lspeinzel.inh1,3,0);
    out_quest ((char *) &lspeinzel.me_einh_kun2,1,0);
    out_quest ((char *) &lspeinzel.auf_me2,3,0);
    out_quest ((char *) &lspeinzel.inh2,3,0);
    out_quest ((char *) &lspeinzel.me_einh_kun3,1,0);
    out_quest ((char *) &lspeinzel.auf_me3,3,0);
    out_quest ((char *) &lspeinzel.inh3,3,0);
    out_quest ((char *) &lspeinzel.lief_me1,3,0);
    out_quest ((char *) &lspeinzel.lief_me2,3,0);
    out_quest ((char *) &lspeinzel.lief_me3,3,0);
    out_quest ((char *) &lspeinzel.a_grund,3,0);
    out_quest ((char *) lspeinzel.kond_art,0,5);
    out_quest ((char *) &lspeinzel.ls_pos_kz,1,0);
    out_quest ((char *) &lspeinzel.posi_ext,2,0);
    out_quest ((char *) &lspeinzel.na_lief_kz,1,0);
    out_quest ((char *) &lspeinzel.nve_posi,2,0);
    out_quest ((char *) &lspeinzel.teil_smt,1,0);
    out_quest ((char *) &lspeinzel.lief_me_bz_ist,0,11);
    out_quest ((char *) &lspeinzel.me_einh_ist,1,0);
    out_quest ((char *) &lspeinzel.inh_ist,3,0);
    out_quest ((char *) &lspeinzel.me_ist,3,0);
    out_quest ((char *) lspeinzel.ls_ident,0,21);
    out_quest ((char *) &lspeinzel.pos_txt_kz,1,0);
    out_quest ((char *) &lspeinzel.anz_einh,3,0);
    out_quest ((char *) &lspeinzel.a_lsp,3,0);
    out_quest ((char *) &lspeinzel.prdk_stufe,1,0);
    out_quest ((char *) &lspeinzel.prdk_typ,1,0);    //prdk_typ : 10=Verarb.Mat, 11=Grundbr�t  20= Zutaten, 30=H�llen , 40= Verpackungen
    out_quest ((char *) &lspeinzel.extra,1,0);
    out_quest ((char *) &lspeinzel.sort,1,0);
}


void LS_EINZEL_CLASS::lsp_in (void)
{
    ins_quest ((char *) &lspeinzel.mdn,1,0);
    ins_quest ((char *) &lspeinzel.fil,1,0);
    ins_quest ((char *) &lspeinzel.ls,2,0);
    ins_quest ((char *) &lspeinzel.auf,2,0);
    ins_quest ((char *) &lspeinzel.a,3,0);
    ins_quest ((char *) &lspeinzel.auf_me,3,0);
    ins_quest ((char *) lspeinzel.auf_me_bz,0,7);
    ins_quest ((char *) &lspeinzel.lief_me,3,0);
    ins_quest ((char *) lspeinzel.lief_me_bz,0,7);
    ins_quest ((char *) &lspeinzel.ls_vk_pr,3,0);
    ins_quest ((char *) &lspeinzel.ls_lad_pr,3,0);
    ins_quest ((char *) &lspeinzel.delstatus,1,0);
    ins_quest ((char *) &lspeinzel.tara,3,0);
    ins_quest ((char *) &lspeinzel.posi,2,0);
    ins_quest ((char *) &lspeinzel.lsp_txt,2,0);
    ins_quest ((char *) &lspeinzel.pos_stat,1,0);
    ins_quest ((char *) &lspeinzel.sa_kz_sint,1,0);
    ins_quest ((char *) lspeinzel.erf_kz,0,2);
    ins_quest ((char *) &lspeinzel.prov_satz,3,0);
    ins_quest ((char *) &lspeinzel.leer_pos,1,0);
    ins_quest ((char *) &lspeinzel.hbk_date,2,0);
    ins_quest ((char *) lspeinzel.ls_charge,0,sizeof (lspeinzel.ls_charge));
    ins_quest ((char *) &lspeinzel.auf_me_vgl,3,0);
    ins_quest ((char *) &lspeinzel.ls_vk_euro,3,0);
    ins_quest ((char *) &lspeinzel.ls_vk_fremd,3,0);
    ins_quest ((char *) &lspeinzel.ls_lad_euro,3,0);
    ins_quest ((char *) &lspeinzel.ls_lad_fremd,3,0);
    ins_quest ((char *) &lspeinzel.rab_satz,3,0);
    ins_quest ((char *) &lspeinzel.me_einh_kun,1,0);
    ins_quest ((char *) &lspeinzel.me_einh,1,0);
    ins_quest ((char *) &lspeinzel.me_einh_kun1,1,0);
    ins_quest ((char *) &lspeinzel.auf_me1,3,0);
    ins_quest ((char *) &lspeinzel.inh1,3,0);
    ins_quest ((char *) &lspeinzel.me_einh_kun2,1,0);
    ins_quest ((char *) &lspeinzel.auf_me2,3,0);
    ins_quest ((char *) &lspeinzel.inh2,3,0);
    ins_quest ((char *) &lspeinzel.me_einh_kun3,1,0);
    ins_quest ((char *) &lspeinzel.auf_me3,3,0);
    ins_quest ((char *) &lspeinzel.inh3,3,0);
    ins_quest ((char *) &lspeinzel.lief_me1,3,0);
    ins_quest ((char *) &lspeinzel.lief_me2,3,0);
    ins_quest ((char *) &lspeinzel.lief_me3,3,0);
    ins_quest ((char *) &lspeinzel.a_grund,3,0);
    ins_quest ((char *) lspeinzel.kond_art,0,5);
    ins_quest ((char *) &lspeinzel.ls_pos_kz,1,0);
    ins_quest ((char *) &lspeinzel.posi_ext,2,0);
    ins_quest ((char *) &lspeinzel.na_lief_kz,1,0);
    ins_quest ((char *) &lspeinzel.nve_posi,2,0);
    ins_quest ((char *) &lspeinzel.teil_smt,1,0);
    ins_quest ((char *) &lspeinzel.lief_me_bz_ist,0,11);
    ins_quest ((char *) &lspeinzel.me_einh_ist,1,0);
    ins_quest ((char *) &lspeinzel.inh_ist,3,0);
    ins_quest ((char *) &lspeinzel.me_ist,3,0);
    ins_quest ((char *) lspeinzel.ls_ident,0,21);
    ins_quest ((char *) &lspeinzel.pos_txt_kz,1,0);
    ins_quest ((char *) &lspeinzel.anz_einh,3,0);
    ins_quest ((char *) &lspeinzel.a_lsp,3,0);
    ins_quest ((char *) &lspeinzel.prdk_stufe,1,0);
    ins_quest ((char *) &lspeinzel.prdk_typ,1,0);  
    ins_quest ((char *) &lspeinzel.extra,1,0);
    ins_quest ((char *) &lspeinzel.sort,1,0);
 
}

             
void LS_EINZEL_CLASS::prepare_lspeinzel (char *order)
/**
Cursor fuer lsp vorbereiten.
**/
{

    ins_quest ((char *) &lspeinzel.mdn,1,0);
    ins_quest ((char *) &lspeinzel.fil,1,0);
    ins_quest ((char *) &lspeinzel.ls,2,0);
    ins_quest ((char *) &lspeinzel.auf,2,0);

    lsp_out ();

	if (order)
	{
cursor_lspeinzel = prepare_sql ("select lspeinzel.mdn,  lspeinzel.fil,  lspeinzel.ls, lspeinzel.auf,  lspeinzel.a,  "
"lspeinzel.auf_me,  lspeinzel.auf_me_bz,  lspeinzel.lief_me,  lspeinzel.lief_me_bz,  lspeinzel.ls_vk_pr,  "
"lspeinzel.ls_lad_pr,  lspeinzel.delstatus,  lspeinzel.tara,  lspeinzel.posi,  lspeinzel.lsp_txt,  "
"lspeinzel.pos_stat,  lspeinzel.sa_kz_sint,  lspeinzel.erf_kz,  lspeinzel.prov_satz,  "
"lspeinzel.leer_pos,  lspeinzel.hbk_date,  lspeinzel.ls_charge,  lspeinzel.auf_me_vgl,  "
"lspeinzel.ls_vk_euro,  lspeinzel.ls_vk_fremd,  lspeinzel.ls_lad_euro,  lspeinzel.ls_lad_fremd,  "
"lspeinzel.rab_satz,  lspeinzel.me_einh_kun,  lspeinzel.me_einh,  lspeinzel.me_einh_kun1,  "
"lspeinzel.auf_me1,  lspeinzel.inh1,  lspeinzel.me_einh_kun2,  lspeinzel.auf_me2,  lspeinzel.inh2,  "
"lspeinzel.me_einh_kun3,  lspeinzel.auf_me3,  lspeinzel.inh3,  lspeinzel.lief_me1,  lspeinzel.lief_me2,  "
"lspeinzel.lief_me3,  lspeinzel.a_grund,  lspeinzel.kond_art,  lspeinzel.ls_pos_kz, lspeinzel.posi_ext, lspeinzel.na_lief_kz, "
"nve_posi, teil_smt, lief_me_bz_ist, me_einh_ist, inh_ist, me_ist, ls_ident, pos_txt_kz, anz_einh, a_lsp, prdk_stufe, prdk_typ, extra, sort "
                          "from lspeinzel "
                          "where mdn = ? "
                          "and fil = ?  "
                          "and ls = ? and auf = ?  and a_lsp = a  %s", order);
	}
	else
	{
cursor_lspeinzel = prepare_sql ("select lspeinzel.mdn,  lspeinzel.fil,  lspeinzel.ls, lspeinzel.auf,  lspeinzel.a,  "
"lspeinzel.auf_me,  lspeinzel.auf_me_bz,  lspeinzel.lief_me,  lspeinzel.lief_me_bz,  lspeinzel.ls_vk_pr,  "
"lspeinzel.ls_lad_pr,  lspeinzel.delstatus,  lspeinzel.tara,  lspeinzel.posi,  lspeinzel.lsp_txt,  "
"lspeinzel.pos_stat,  lspeinzel.sa_kz_sint,  lspeinzel.erf_kz,  lspeinzel.prov_satz,  "
"lspeinzel.leer_pos,  lspeinzel.hbk_date,  lspeinzel.ls_charge,  lspeinzel.auf_me_vgl,  "
"lspeinzel.ls_vk_euro,  lspeinzel.ls_vk_fremd,  lspeinzel.ls_lad_euro,  lspeinzel.ls_lad_fremd,  "
"lspeinzel.rab_satz,  lspeinzel.me_einh_kun,  lspeinzel.me_einh,  lspeinzel.me_einh_kun1,  "
"lspeinzel.auf_me1,  lspeinzel.inh1,  lspeinzel.me_einh_kun2,  lspeinzel.auf_me2,  lspeinzel.inh2,  "
"lspeinzel.me_einh_kun3,  lspeinzel.auf_me3,  lspeinzel.inh3,  lspeinzel.lief_me1,  lspeinzel.lief_me2,  "
"lspeinzel.lief_me3,  lspeinzel.a_grund,  lspeinzel.kond_art,  lspeinzel.ls_pos_kz, lspeinzel.posi_ext, lspeinzel.na_lief_kz,  "
"nve_posi, teil_smt, lief_me_bz_ist, me_einh_ist, inh_ist, me_ist, ls_ident, pos_txt_kz, anz_einh, a_lsp, prdk_stufe, prdk_typ, extra, sort   "
                          "from lspeinzel "
                          "where mdn = ? "
                          "and fil = ? "
                          "and ls = ? and auf = ? and a_lsp = a ");
	}
}

void LS_EINZEL_CLASS::prepare_lspeinzel_auf (void)
/**
Cursor fuer lsp vorbereiten. Mit Artikel-Nummer und posi lesen.
**/
{

    ins_quest ((char *) &lspeinzel.mdn,1,0);
    ins_quest ((char *) &lspeinzel.fil,1,0);
    ins_quest ((char *) &lspeinzel.ls,2,0);
    ins_quest ((char *) &lspeinzel.auf,2,0);

    lsp_out ();

cursor_lspeinzel_auf = prepare_sql ("select lspeinzel.mdn,  lspeinzel.fil,  lspeinzel.ls, lspeinzel.auf,  lspeinzel.a,  "
"lspeinzel.auf_me,  lspeinzel.auf_me_bz,  lspeinzel.lief_me,  lspeinzel.lief_me_bz,  lspeinzel.ls_vk_pr,  "
"lspeinzel.ls_lad_pr,  lspeinzel.delstatus,  lspeinzel.tara,  lspeinzel.posi,  lspeinzel.lsp_txt,  "
"lspeinzel.pos_stat,  lspeinzel.sa_kz_sint,  lspeinzel.erf_kz,  lspeinzel.prov_satz,  "
"lspeinzel.leer_pos,  lspeinzel.hbk_date,  lspeinzel.ls_charge,  lspeinzel.auf_me_vgl,  "
"lspeinzel.ls_vk_euro,  lspeinzel.ls_vk_fremd,  lspeinzel.ls_lad_euro,  lspeinzel.ls_lad_fremd,  "
"lspeinzel.rab_satz,  lspeinzel.me_einh_kun,  lspeinzel.me_einh,  lspeinzel.me_einh_kun1,  "
"lspeinzel.auf_me1,  lspeinzel.inh1,  lspeinzel.me_einh_kun2,  lspeinzel.auf_me2,  lspeinzel.inh2,  "
"lspeinzel.me_einh_kun3,  lspeinzel.auf_me3,  lspeinzel.inh3,  lspeinzel.lief_me1,  lspeinzel.lief_me2,  "
"lspeinzel.lief_me3,  lspeinzel.a_grund,  lspeinzel.kond_art,  lspeinzel.ls_pos_kz, lspeinzel.posi_ext, lspeinzel.na_lief_kz, "
"nve_posi, teil_smt, lief_me_bz_ist, me_einh_ist, inh_ist, me_ist, ls_ident, pos_txt_kz, anz_einh, a_lsp, prdk_stufe, prdk_typ, extra, sort  "
                          "from lspeinzel "
                          "where mdn = ? "
                          "and fil = ? "
                          "and ls = ? and auf = ? and prdk_typ >= 0 order by teil_smt, a_lsp,prdk_typ desc  ");
}


void LS_EINZEL_CLASS::prepare_lsp_a_lsp (void)
/**
Cursor fuer lsp vorbereiten. Mit Artikel-Nummer und posi lesen.
**/
{

    ins_quest ((char *) &lspeinzel.mdn,1,0);
    ins_quest ((char *) &lspeinzel.fil,1,0);
    ins_quest ((char *) &lspeinzel.ls,2,0);
    ins_quest ((char *) &lspeinzel.auf,2,0);
    ins_quest ((char *) &lspeinzel.a_lsp,3,0);

    lsp_out ();

cursor_lsp_a_lsp = prepare_sql ("select lspeinzel.mdn,  lspeinzel.fil,  lspeinzel.ls, lspeinzel.auf,  lspeinzel.a,  "
"lspeinzel.auf_me,  lspeinzel.auf_me_bz,  lspeinzel.lief_me,  lspeinzel.lief_me_bz,  lspeinzel.ls_vk_pr,  "
"lspeinzel.ls_lad_pr,  lspeinzel.delstatus,  lspeinzel.tara,  lspeinzel.posi,  lspeinzel.lsp_txt,  "
"lspeinzel.pos_stat,  lspeinzel.sa_kz_sint,  lspeinzel.erf_kz,  lspeinzel.prov_satz,  "
"lspeinzel.leer_pos,  lspeinzel.hbk_date,  lspeinzel.ls_charge,  lspeinzel.auf_me_vgl,  "
"lspeinzel.ls_vk_euro,  lspeinzel.ls_vk_fremd,  lspeinzel.ls_lad_euro,  lspeinzel.ls_lad_fremd,  "
"lspeinzel.rab_satz,  lspeinzel.me_einh_kun,  lspeinzel.me_einh,  lspeinzel.me_einh_kun1,  "
"lspeinzel.auf_me1,  lspeinzel.inh1,  lspeinzel.me_einh_kun2,  lspeinzel.auf_me2,  lspeinzel.inh2,  "
"lspeinzel.me_einh_kun3,  lspeinzel.auf_me3,  lspeinzel.inh3,  lspeinzel.lief_me1,  lspeinzel.lief_me2,  "
"lspeinzel.lief_me3,  lspeinzel.a_grund,  lspeinzel.kond_art,  lspeinzel.ls_pos_kz, lspeinzel.posi_ext, lspeinzel.na_lief_kz, "
"nve_posi, teil_smt, lief_me_bz_ist, me_einh_ist, inh_ist, me_ist, ls_ident, pos_txt_kz, anz_einh, a_lsp, prdk_stufe, prdk_typ, extra, sort  "
                          "from lspeinzel "
                          "where mdn = ? "
                          "and fil = ? "
                          "and ls = ? and auf = ? "
                          "and a_lsp = ? and (prdk_typ = 10 or prdk_typ = 20) ");
}

void LS_EINZEL_CLASS::prepare_lsp_a_a_lsp (void)
/**
Cursor fuer lsp vorbereiten. Mit Artikel-Nummer und posi lesen.
**/
{

    ins_quest ((char *) &lspeinzel.mdn,1,0);
    ins_quest ((char *) &lspeinzel.fil,1,0);
    ins_quest ((char *) &lspeinzel.ls,2,0);
    ins_quest ((char *) &lspeinzel.auf,2,0);
    ins_quest ((char *) &lspeinzel.a,3,0);
    ins_quest ((char *) &lspeinzel.a_lsp,3,0);

    lsp_out ();

cursor_lsp_a_a_lsp = prepare_sql ("select lspeinzel.mdn,  lspeinzel.fil,  lspeinzel.ls, lspeinzel.auf,  lspeinzel.a,  "
"lspeinzel.auf_me,  lspeinzel.auf_me_bz,  lspeinzel.lief_me,  lspeinzel.lief_me_bz,  lspeinzel.ls_vk_pr,  "
"lspeinzel.ls_lad_pr,  lspeinzel.delstatus,  lspeinzel.tara,  lspeinzel.posi,  lspeinzel.lsp_txt,  "
"lspeinzel.pos_stat,  lspeinzel.sa_kz_sint,  lspeinzel.erf_kz,  lspeinzel.prov_satz,  "
"lspeinzel.leer_pos,  lspeinzel.hbk_date,  lspeinzel.ls_charge,  lspeinzel.auf_me_vgl,  "
"lspeinzel.ls_vk_euro,  lspeinzel.ls_vk_fremd,  lspeinzel.ls_lad_euro,  lspeinzel.ls_lad_fremd,  "
"lspeinzel.rab_satz,  lspeinzel.me_einh_kun,  lspeinzel.me_einh,  lspeinzel.me_einh_kun1,  "
"lspeinzel.auf_me1,  lspeinzel.inh1,  lspeinzel.me_einh_kun2,  lspeinzel.auf_me2,  lspeinzel.inh2,  "
"lspeinzel.me_einh_kun3,  lspeinzel.auf_me3,  lspeinzel.inh3,  lspeinzel.lief_me1,  lspeinzel.lief_me2,  "
"lspeinzel.lief_me3,  lspeinzel.a_grund,  lspeinzel.kond_art,  lspeinzel.ls_pos_kz, lspeinzel.posi_ext, lspeinzel.na_lief_kz, "
"nve_posi, teil_smt, lief_me_bz_ist, me_einh_ist, inh_ist, me_ist, ls_ident, pos_txt_kz, anz_einh, a_lsp, prdk_stufe, prdk_typ, extra, sort  "
                          "from lspeinzel "
                          "where mdn = ? "
                          "and fil = ? "
                          "and ls = ? and auf = ? "
                          "and a = ? "
                          "and a_lsp = ? and (prdk_typ = 10 or prdk_typ = 20) ");
}


void LS_EINZEL_CLASS::prepare_lsp_w (char *where)
/**
Cursor fuer lsp vorbereiten. Mit Artikel-Nummer und posi lesen.
**/
{


    lsp_out ();

cursor_lsp_w = prepare_sql ("select lspeinzel.mdn,  lspeinzel.fil,  lspeinzel.ls, lspeinzel.auf,  lspeinzel.a,  "
"lspeinzel.auf_me,  lspeinzel.auf_me_bz,  lspeinzel.lief_me,  lspeinzel.lief_me_bz,  lspeinzel.ls_vk_pr,  "
"lspeinzel.ls_lad_pr,  lspeinzel.delstatus,  lspeinzel.tara,  lspeinzel.posi,  lspeinzel.lsp_txt,  "
"lspeinzel.pos_stat,  lspeinzel.sa_kz_sint,  lspeinzel.erf_kz,  lspeinzel.prov_satz,  "
"lspeinzel.leer_pos,  lspeinzel.hbk_date,  lspeinzel.ls_charge,  lspeinzel.auf_me_vgl,  "
"lspeinzel.ls_vk_euro,  lspeinzel.ls_vk_fremd,  lspeinzel.ls_lad_euro,  lspeinzel.ls_lad_fremd,  "
"lspeinzel.rab_satz,  lspeinzel.me_einh_kun,  lspeinzel.me_einh,  lspeinzel.me_einh_kun1,  "
"lspeinzel.auf_me1,  lspeinzel.inh1,  lspeinzel.me_einh_kun2,  lspeinzel.auf_me2,  lspeinzel.inh2,  "
"lspeinzel.me_einh_kun3,  lspeinzel.auf_me3,  lspeinzel.inh3,  lspeinzel.lief_me1,  lspeinzel.lief_me2,  "
"lspeinzel.lief_me3,  lspeinzel.a_grund,  lspeinzel.kond_art,  lspeinzel.ls_pos_kz, lspeinzel.posi_ext, lspeinzel.na_lief_kz, "
"nve_posi, teil_smt,lief_me_bz_ist, me_einh_ist, inh_ist, me_ist, ls_ident, pos_txt_kz, anz_einh, a_lsp, prdk_stufe, prdk_typ, extra, sort   "

                          "from lspeinzel %s", where);
}

void LS_EINZEL_CLASS::prepare_lsp_upd (void)
/**
Vursor fuer Update vorbereiten.
**/
{
    ins_quest ((char *) &lspeinzel.mdn, 1, 0);
    ins_quest ((char *) &lspeinzel.fil, 1, 0);
    ins_quest ((char *) &lspeinzel.ls, 2, 0);
    ins_quest ((char *) &lspeinzel.auf, 2, 0);
    ins_quest ((char *) &lspeinzel.a, 3, 0);
    ins_quest ((char *) &lspeinzel.posi, 2, 0);
    test_lsp_upd = prepare_sql ("select ls from lspeinzel "
                              "where mdn = ? "
                              "and fil = ? "
                              "and ls = ? and auf = ? "
                              "and a = ? "
                              "and posi = ? for update");

    ins_quest ((char *) &lspeinzel.mdn, 1, 0);
    ins_quest ((char *) &lspeinzel.fil, 1, 0);
    ins_quest ((char *) &lspeinzel.ls, 2, 0);
    ins_quest ((char *) &lspeinzel.auf, 2, 0);
	//EF-27  a >= 40000 und a <= 49999 soll mit auf Liste !!
    deaktiviere_lspeinzel_20 = prepare_sql ("update lspeinzel set prdk_typ = -20 where prdk_typ == 20 and (a < 40000 or a > 49999) "
                              "and mdn = ? "
                              "and fil = ? "
                              "and ls = ? and auf = ? ");
                              
    ins_quest ((char *) &lspeinzel.mdn, 1, 0);
    ins_quest ((char *) &lspeinzel.fil, 1, 0);
    ins_quest ((char *) &lspeinzel.ls, 2, 0);
    ins_quest ((char *) &lspeinzel.auf, 2, 0); 
    aktiviere_lspeinzel_20 = prepare_sql ("update lspeinzel set prdk_typ = 20 where prdk_typ == -20 "
                              "and mdn = ? "
                              "and fil = ? "
                              "and ls = ? and auf = ? ");

    ins_quest ((char *) &lspeinzel.mdn, 1, 0);
    ins_quest ((char *) &lspeinzel.fil, 1, 0);
    ins_quest ((char *) &lspeinzel.auf, 2, 0);
    ins_quest ((char *) &a_grund_von_buffet, 3, 0);
    ins_quest ((char *) &a_grund_bis_buffet, 3, 0);
    suche_buffet = prepare_sql ("select mdn from aufp where mdn = ? and fil = ? and auf = ? and a in (select a from a_bas where a_grund >= ? and a_grund <= ? ) ");



    lsp_in ();

    ins_quest ((char *) &lspeinzel.mdn, 1, 0);
    ins_quest ((char *) &lspeinzel.fil, 1, 0);
    ins_quest ((char *) &lspeinzel.ls, 2, 0);
    ins_quest ((char *) &lspeinzel.auf, 2, 0);
    ins_quest ((char *) &lspeinzel.a, 3, 0);
    ins_quest ((char *) &lspeinzel.posi, 2, 0);

cursor_lsp_upd = prepare_sql ("update lspeinzel set lspeinzel.mdn = ?,  "
"lspeinzel.fil = ?,  lspeinzel.ls = ?, lspeinzel.auf = ?,  lspeinzel.a = ?,  lspeinzel.auf_me = ?,  "
"lspeinzel.auf_me_bz = ?,  lspeinzel.lief_me = ?,  lspeinzel.lief_me_bz = ?,  "
"lspeinzel.ls_vk_pr = ?,  lspeinzel.ls_lad_pr = ?,  lspeinzel.delstatus = ?,  "
"lspeinzel.tara = ?,  lspeinzel.posi = ?,  lspeinzel.lsp_txt = ?,  lspeinzel.pos_stat = ?,  "
"lspeinzel.sa_kz_sint = ?,  lspeinzel.erf_kz = ?,  lspeinzel.prov_satz = ?,  "
"lspeinzel.leer_pos = ?,  lspeinzel.hbk_date = ?,  lspeinzel.ls_charge = ?,  "
"lspeinzel.auf_me_vgl = ?,  lspeinzel.ls_vk_euro = ?,  lspeinzel.ls_vk_fremd = ?,  "
"lspeinzel.ls_lad_euro = ?,  lspeinzel.ls_lad_fremd = ?,  lspeinzel.rab_satz = ?,  "
"lspeinzel.me_einh_kun = ?,  lspeinzel.me_einh = ?,  lspeinzel.me_einh_kun1 = ?,  "
"lspeinzel.auf_me1 = ?,  lspeinzel.inh1 = ?,  lspeinzel.me_einh_kun2 = ?,  "
"lspeinzel.auf_me2 = ?,  lspeinzel.inh2 = ?,  lspeinzel.me_einh_kun3 = ?,  "
"lspeinzel.auf_me3 = ?,  lspeinzel.inh3 = ?,  lspeinzel.lief_me1 = ?,  lspeinzel.lief_me2 = ?,  "
"lspeinzel.lief_me3 = ?,  lspeinzel.a_grund = ?,  lspeinzel.kond_art = ?,  "
"lspeinzel.ls_pos_kz = ?, lspeinzel.posi_ext = ?, lspeinzel.na_lief_kz = ?, nve_posi = ?, "
"teil_smt = ?, lief_me_bz_ist = ?, me_einh_ist = ?, inh_ist = ?, me_ist = ?, "
"ls_ident = ?, pos_txt_kz = ?, anz_einh = ? , a_lsp = ? , prdk_stufe = ?, prdk_typ = ?, extra = ?, sort = ? "
                              "where mdn = ? "
                              "and fil = ? "
                              "and ls = ? and auf = ? "
                              "and a = ? "
                              "and posi = ?"); 

    lsp_in ();

cursor_lsp_ins = prepare_sql ("insert into lspeinzel (mdn,  fil,  ls, auf,  a,  "
"auf_me,  auf_me_bz,  lief_me,  lief_me_bz,  ls_vk_pr,  ls_lad_pr,  delstatus,  "
"tara,  posi,  lsp_txt,  pos_stat,  sa_kz_sint,  erf_kz,  prov_satz,  leer_pos,  "
"hbk_date,  ls_charge,  auf_me_vgl,  ls_vk_euro,  ls_vk_fremd,  ls_lad_euro,  "
"ls_lad_fremd,  rab_satz,  me_einh_kun,  me_einh,  me_einh_kun1,  auf_me1,  "
"inh1,  me_einh_kun2,  auf_me2,  inh2,  me_einh_kun3,  auf_me3,  inh3,  lief_me1,  "
"lief_me2,  lief_me3,  a_grund,  kond_art,  ls_pos_kz, lspeinzel.posi_ext, lspeinzel.na_lief_kz, nve_posi, "
"teil_smt, lief_me_bz_ist, me_einh_ist, inh_ist, me_ist, ls_ident, pos_txt_kz, anz_einh, a_lsp, prdk_stufe, prdk_typ, extra, sort ) "
"values ("
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

}


void LS_EINZEL_CLASS::prepare_lsp_del  (void)
{
    ins_quest ((char *) &lspeinzel.mdn, 1, 0);
    ins_quest ((char *) &lspeinzel.fil, 1, 0);
    ins_quest ((char *) &lspeinzel.ls, 2, 0);
    ins_quest ((char *) &lspeinzel.auf, 2, 0);
    ins_quest ((char *) &lspeinzel.a, 3, 0);
    ins_quest ((char *) &lspeinzel.posi, 2, 0);
    cursor_lsp_del = prepare_sql ("delete from lspeinzel "
                                 "where lspeinzel.mdn = ? "
                                 "and lspeinzel.fil = ? "
                                 "and lspeinzel.ls = ? "
                                 "and lspeinzel.auf = ? "
                                 "and lspeinzel.a = ? "
                                 "and lspeinzel.posi = ?");

    ins_quest ((char *) &lspeinzel.mdn, 1, 0);
    ins_quest ((char *) &lspeinzel.fil, 1, 0);
    ins_quest ((char *) &lspeinzel.ls, 2, 0);
    ins_quest ((char *) &lspeinzel.auf, 2, 0);
    cursor_lsp_dells = prepare_sql ("delete from lspeinzel "
                                    "where lspeinzel.mdn = ? "
                                    "and lspeinzel.fil = ? "
                                     "and lspeinzel.ls = ? and lspeinzel.auf = ? ");
}



int LS_EINZEL_CLASS::lese_lspeinzel (short mdn, short fil, long ls, long auf, char *order)
/**
Tabelle lsp mit Lieferschein-Nummer lesen.
**/
{
         if (cursor_lspeinzel == -1)
         {
                      prepare_lspeinzel (order);
         }
         lspeinzel.mdn  = mdn;
         lspeinzel.fil  = fil;
         lspeinzel.ls  = ls;
         lspeinzel.auf  = auf;
         open_sql (cursor_lspeinzel);
         fetch_sql (cursor_lspeinzel);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int LS_EINZEL_CLASS::lese_lspeinzel (void)
/**
Naechsten Satz aus Tabelle lsp lesen.
**/
{
         fetch_sql (cursor_lspeinzel);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

void LS_EINZEL_CLASS::close_lspeinzel (void)
/**
Cursor fuer lsp schliessen.
**/
{
        if (cursor_lspeinzel == -1) return;

        close_sql (cursor_lspeinzel);
        cursor_lspeinzel = -1;
}

int LS_EINZEL_CLASS::lese_lspeinzel_auf (short mdn, short fil, long ls, long auf)
/**
Tabelle lsp mit Lieferschein-Nummer lesen.
**/
{
         if (cursor_lspeinzel_auf == -1)
         {
                      prepare_lspeinzel_auf ();
         }
         lspeinzel.mdn  = mdn;
         lspeinzel.fil  = fil;
         lspeinzel.ls  = ls;
         lspeinzel.auf  = auf;
         open_sql (cursor_lspeinzel_auf);
         fetch_sql (cursor_lspeinzel_auf);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int LS_EINZEL_CLASS::lese_lspeinzel_auf (void)
/**
Naechsten Satz aus Tabelle lsp lesen.
**/
{
         fetch_sql (cursor_lspeinzel_auf);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int LS_EINZEL_CLASS::lese_lsp_a_a_lsp (short mdn, short fil, long ls, long auf, 
                           double a, double a_lsp)
/**
Tabelle lsp mit Lieferschein-Nummer lesen.
**/
{
         if (cursor_lsp_a_a_lsp == -1)
         {
                      prepare_lsp_a_a_lsp ();
         }
         lspeinzel.mdn  = mdn;
         lspeinzel.fil  = fil;
         lspeinzel.ls  = ls;
         lspeinzel.auf  = auf;
         lspeinzel.a   = a;
         lspeinzel.a_lsp = a_lsp;
         open_sql (cursor_lsp_a_a_lsp);
         fetch_sql (cursor_lsp_a_a_lsp);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int LS_EINZEL_CLASS::lese_lsp_a_a_lsp (void)
/**
Naechsten Satz aus Tabelle lsp lesen.
**/
{
         fetch_sql (cursor_lsp_a_lsp);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int LS_EINZEL_CLASS::lese_lsp_a_lsp (short mdn, short fil, long ls, long auf, 
                            double a_lsp)
/**
Tabelle lsp mit Lieferschein-Nummer lesen.
**/
{
         if (cursor_lsp_a_lsp == -1)
         {
                      prepare_lsp_a_lsp ();
         }
         lspeinzel.mdn  = mdn;
         lspeinzel.fil  = fil;
         lspeinzel.ls  = ls;
         lspeinzel.auf  = auf;
         lspeinzel.a_lsp = a_lsp;
         open_sql (cursor_lsp_a_lsp);
         fetch_sql (cursor_lsp_a_lsp);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int LS_EINZEL_CLASS::lese_lsp_a_lsp (void)
/**
Naechsten Satz aus Tabelle lsp lesen.
**/
{
         fetch_sql (cursor_lsp_a_lsp);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}



int LS_EINZEL_CLASS::lese_lsp_w (char *where)
/**
Tabelle lsp mit Lieferschein-Nummer lesen.
**/
{
         static char akt_w [1024] = {" "};
         
         if (cursor_lsp_w == -1)
         {
                      strcpy (akt_w, where);
                      prepare_lsp_w (where);
         }
         else if (strcmp (akt_w, where))
         {
                      strcpy (akt_w, where);
                      close_sql (cursor_lsp_w);
                      prepare_lsp_w (where);
         }

         open_sql (cursor_lsp_w);
         fetch_sql (cursor_lsp_w);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int LS_EINZEL_CLASS::lese_lsp_w (void)
/**
Naechsten Satz aus Tabelle lsp lesen.
**/
{
         fetch_sql (cursor_lsp_w);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}


int LS_EINZEL_CLASS::lese_lsp_upd (void)
/**
Tabelle lsp mit Lieferschein-Nummer lesen.
**/
{
         if (test_lsp_upd == -1)
         {
                      prepare_lsp_upd ();
         }
         open_sql (test_lsp_upd);
         fetch_sql (test_lsp_upd);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int LS_EINZEL_CLASS::aktiviere_salat  (short mdn, short fil,long auf)
{
         if (aktiviere_lspeinzel_20 == -1)
         {
                      prepare_lsp_upd ();
         }
		 lspeinzel.mdn = mdn;
		 lspeinzel.fil = fil;
		 lspeinzel.auf = auf;

         open_sql (suche_buffet);
         fetch_sql (suche_buffet);
         if (sqlstatus == 0)
		 {
			execute_curs (deaktiviere_lspeinzel_20);
		 }
		 else
		 {
			execute_curs (aktiviere_lspeinzel_20);
		 }
         return 0;
}


#ifndef CONSOLE
#endif


int LS_EINZEL_CLASS::lock_lsp (short mdn, short fil, long ls, long auf, double a, long posi)
/**
Tabelle lsp updaten.
**/
{     
          if (cursor_lsp_upd == -1)
          {
                   prepare_lsp_upd ();
          }

          lspeinzel.mdn = mdn;
          lspeinzel.fil = fil;
          lspeinzel.ls  = ls;
          lspeinzel.auf  = auf;
          lspeinzel.a   = a;
          lspeinzel.posi = posi;

          if (open_sql (test_lsp_upd) == 0)
          {
                    fetch_sql (test_lsp_upd);
          }
          return (sqlstatus);
}




int LS_EINZEL_CLASS::update_lsp (short mdn, short fil, long ls, long auf, double a, double a_lsp, long posi)
/**
Tabelle lspeinzel updaten.
**/
{     
          if (cursor_lsp_upd == -1)
          {
                   prepare_lsp_upd ();
          }

          lspeinzel.mdn = mdn;
          lspeinzel.fil = fil;
          lspeinzel.ls  = ls;
          lspeinzel.auf  = auf;
          lspeinzel.a   = a;
          lspeinzel.posi = posi;

		  if (lspeinzel.a == (double) 0.0) return 0;

          open_sql (test_lsp_upd);
          fetch_sql (test_lsp_upd);
          lspeinzel.a_lsp   = a_lsp;
          if (sqlstatus == 0)
          {
                   execute_curs (cursor_lsp_upd);
          }
          else if (sqlstatus == 100)
          {
                   execute_curs (cursor_lsp_ins);
          }
          return (sqlstatus);
}

int LS_EINZEL_CLASS::update_lsp_mehrf (short mdn, short fil, long ls, long auf, double a, double a_lsp, long posi, double AufMe, short varb_me_einh,double beilage_a_gew)
/**
Tabelle lspeinezl updaten. (mehrfach wg. unterschiedlichen Beilagen
**/
{     
    char wert [5];

	int max_arr = 10;
	double beilage = 0.0;
	double a_gew;
	double abeilage_a_gew [10];
	double abeilage [10];
	long beilage_stk = 0;
	long abeilage_stk [10];
	short abeilage_me_einh [10];
	long sum_stk = 0;
	short me_einh = 0;
	int beilage_cursor;
	int dsqlstatus;

    if (cursor_lsp_upd == -1)
    {
             prepare_lsp_upd ();
    }
    lspeinzel.mdn = mdn;
    lspeinzel.fil = fil;
    lspeinzel.ls  = ls;
    lspeinzel.auf  = auf;
    lspeinzel.a   = a;
    lspeinzel.posi = posi - 10;

    if (lspeinzel.a == (double) 0.0) return 0;


    ins_quest ((char *) &lspeinzel.auf, 2, 0);
	ins_quest ((char *) &lspeinzel.a_lsp, 3, 0);
	out_quest ((char *) &beilage, 3, 0);
	out_quest ((char *) &beilage_stk, 2, 0);
	out_quest ((char *) &me_einh, 1, 0);     //030812
	out_quest ((char *) &a_gew, 3, 0);     //030812
    beilage_cursor = prepare_sql  ("select shopaufp.a_beilage, sum(shopaufp.stk), a_bas.me_einh, a_bas.a_gew from shopaufp,a_bas where shopaufp.auf = ? and shopaufp.a  = ? and a_bas.a = shopaufp.a_beilage  group by 1,3,4");
    open_sql (beilage_cursor);
    fetch_sql (beilage_cursor);
	if (sqlstatus == 100)  return -1; //EF-55
	int i = 0;
	sum_stk = 0;
	while (sqlstatus == 0 && i < max_arr)
	{
		abeilage[i] = beilage;
		abeilage_stk[i] = beilage_stk;
		abeilage_me_einh[i] = me_einh;
		abeilage_a_gew[i] = a_gew;
		sum_stk += beilage_stk;
           fetch_sql (beilage_cursor);
		i ++;
	}
	abeilage[i] = 0.0;
	abeilage_stk[i] = 0;
	abeilage_me_einh[i] = 0;
	abeilage_a_gew[i] = 0;


	i = 0;
	while (abeilage[i] > 0.0)
	{
	      lspeinzel.posi += 10;
		  lspeinzel.a = abeilage[i];

		  //030812
		  if (abeilage_me_einh[i] == 2) 
		  {
			lspeinzel.auf_me = AufMe * abeilage_stk[i] / sum_stk;
			if (varb_me_einh != 2 && beilage_a_gew > 0.0)
			{
				// in diesem Fall ist dei Beilabe = St�ck  und die alternative Beilage = kg , dann soll das Artikelgewicht herangezogen werden
				lspeinzel.auf_me = (AufMe * abeilage_stk[i] / sum_stk) * beilage_a_gew ;
			}

		  }
		  else
		  {
			lspeinzel.auf_me = AufMe * abeilage_stk[i] / sum_stk;
			if (varb_me_einh == 2)
			{
				// in diesem Fall ist dei Beilabe = kg und die alternative Beilage = St�ck , dann soll immer 2 stk rein (2 Semmelkn�del im Essen z.b. 030812)
				lspeinzel.auf_me = abeilage_stk[i] * 2 ;
			}
		  }


		  //030812
	        memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
			sprintf (wert, "%hd", abeilage_me_einh[i]);
			dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
			if (dsqlstatus == 0)
			{
					strcpy (lspeinzel.auf_me_bz, ptabn.ptbezk);
					strcpy (lspeinzel.lief_me_bz, ptabn.ptbezk);
			}


          open_sql (test_lsp_upd);
          fetch_sql (test_lsp_upd);
          lspeinzel.a_lsp   = a_lsp;
          if (sqlstatus == 0)
          {
                   execute_curs (cursor_lsp_upd);
          }
          else if (sqlstatus == 100)
          {
                   execute_curs (cursor_lsp_ins);
          }
		  i ++;
	}
    return (sqlstatus);
		  
}

int LS_EINZEL_CLASS::delete_lspls (short mdn, short fil, long ls, long auf)
/**
Tabelle lsp fuer Lieferschein loeschen.
**/
{     
          if (cursor_lsp_dells == -1)
          {
                   prepare_lsp_del ();
          }

          lspeinzel.mdn = mdn;
          lspeinzel.fil = fil;
          lspeinzel.ls  = ls;
          lspeinzel.auf  = auf;

          execute_curs (cursor_lsp_dells);
          return (sqlstatus);
}

int LS_EINZEL_CLASS::delete_lsp (short mdn, short fil, long ls, long auf, long posi, double a)
/**
Tabelle lsp fuer Lieferschein loeschen.
**/
{     
          if (cursor_lsp_del == -1)
          {
                   prepare_lsp_del ();
          }

          lspeinzel.mdn = mdn;
          lspeinzel.fil = fil;
          lspeinzel.ls  = ls;
          lspeinzel.auf  = auf;
          lspeinzel.posi= posi;
          lspeinzel.a   = a;

          execute_curs (cursor_lsp_del);
          return (sqlstatus);
}


void LS_EINZEL_CLASS::close_lsp_upd (void)
/**
Vursor fuer Update vorbereiten.
**/
{
    if (test_lsp_upd != -1)
    {
        close_sql (test_lsp_upd);
        test_lsp_upd = -1;
    }
        
    if (cursor_lsp_upd != -1)
    {
        close_sql (cursor_lsp_upd);
        cursor_lsp_upd = -1;
    }
        
    if (cursor_lsp_ins != -1)
    {
        close_sql (cursor_lsp_ins);
        cursor_lsp_ins = -1;
    }
}

void LS_EINZEL_CLASS::close_lsp_del  (void)
{
    if (cursor_lsp_del != -1)
    {
        close_sql (cursor_lsp_del);
        cursor_lsp_del = -1;
    }

    if (cursor_lsp_dells != -1)
    {
        close_sql (cursor_lsp_dells);
        cursor_lsp_dells = -1;
    }
}


int LS_EINZEL_CLASS::PrepareVarb (void)
{
    ins_quest ((char *)   &lspeinzel.mdn, 1, 0);
    ins_quest ((char *)    &lspeinzel.a_lsp, 3, 0);
    out_quest ((char *) prdk_k_rez, 0, 8);
    out_quest ((char *) &varb_artikel, 3, 0);
    out_quest ((char *) &varb_me_ist, 3, 0);
    out_quest ((char *)   &varb_me_einh, 1, 0);
    out_quest ((char *)   &prdk_me_einh, 1, 0);
    out_quest ((char *)   &varb_typ, 1, 0);
    out_quest ((char *)   &varb_ag, 2, 0);
    varb_cursor = prepare_sql ("select prdk_k.rez, a_mat.a, "
									   "prdk_varb.varb_me, a_basv.me_einh, a_bask.me_einh,  prdk_varb.typ, a_basv.ag from prdk_varb, a_bas a_bask, a_bas a_basv, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_varb.mdn "
                                       "and prdk_k.variante  = prdk_varb.variante "
                                       "and prdk_k.rez  = prdk_varb.rez "
                                       "and a_basv.a = a_mat.a "
									   "and a_mat.mat = prdk_varb.mat "
                                       "and a_bask.a = prdk_k.a "
									   );
    return varb_cursor;
}

int LS_EINZEL_CLASS::PrepareVarb1 (void)
{
    ins_quest ((char *)   &lspeinzel.mdn, 1, 0);
    ins_quest ((char *)    &lspeinzel.a_lsp, 3, 0);
    out_quest ((char *) prdk_k_rez, 0, 8);
    out_quest ((char *) &varb_artikel, 3, 0);
    out_quest ((char *) &varb_me_ist, 3, 0);
    out_quest ((char *)   &varb_me_einh, 1, 0);
    out_quest ((char *)   &prdk_me_einh, 1, 0);
    out_quest ((char *)   &varb_typ, 1, 0);
    out_quest ((char *)   &varb_ag, 2, 0);
    varb_cursor1 = prepare_sql ("select prdk_k.rez, a_mat.a, "
									   "prdk_varb.varb_me, a_basv.me_einh, a_bask.me_einh , prdk_varb.typ, a_basv.ag from prdk_varb, a_bas a_bask, a_bas a_basv, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_varb.mdn "
                                       "and prdk_k.variante  = prdk_varb.variante "
                                       "and prdk_k.rez  = prdk_varb.rez "
                                       "and a_basv.a = a_mat.a "
									   "and a_mat.mat = prdk_varb.mat "
                                       "and a_bask.a = prdk_k.a "
									   );
    return varb_cursor1;
}

int LS_EINZEL_CLASS::PrepareVarb2 (void)
{
    ins_quest ((char *)   &lspeinzel.mdn, 1, 0);
    ins_quest ((char *)    &lspeinzel.a_lsp, 3, 0);
    out_quest ((char *) prdk_k_rez, 0, 8);
    out_quest ((char *) &varb_artikel, 3, 0);
    out_quest ((char *) &varb_me_ist, 3, 0);
    out_quest ((char *)   &varb_me_einh, 1, 0);
    out_quest ((char *)   &prdk_me_einh, 1, 0);
    out_quest ((char *)   &varb_typ, 1, 0);
    out_quest ((char *)   &varb_ag, 2, 0);
    varb_cursor2 = prepare_sql ("select prdk_k.rez, a_mat.a, "
									   "prdk_varb.varb_me, a_basv.me_einh, a_bask.me_einh , prdk_varb.typ, a_basv.ag from prdk_varb, a_bas a_bask, a_bas a_basv, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_varb.mdn "
                                       "and prdk_k.variante  = prdk_varb.variante "
                                       "and prdk_k.rez  = prdk_varb.rez "
                                       "and a_basv.a = a_mat.a "
									   "and a_mat.mat = prdk_varb.mat "
                                       "and a_bask.a = prdk_k.a "
									   );
    return varb_cursor2;
}

int LS_EINZEL_CLASS::OpenVarb (short di)
{
 switch (di)
 {
        case 0 :
		    if (varb_cursor == -1)
			{
				varb_cursor = PrepareVarb ();
		    }
		    return open_sql (varb_cursor);
			break;
			
        case 1 :
		    if (varb_cursor1 == -1)
			{
				varb_cursor1 = PrepareVarb1 ();
		    }
		    return open_sql (varb_cursor1);
			break;
        case 2 :
		    if (varb_cursor2 == -1)
			{
				varb_cursor2 = PrepareVarb2 ();
		    }
		    return open_sql (varb_cursor2);
			break;
 }
 return open_sql (varb_cursor);
}

int LS_EINZEL_CLASS::FetchVarb (short di)
{
 int dsqlstatus;
 switch (di)
 {
        case 0 :
		    if (varb_cursor == -1)
			{
				varb_cursor = PrepareVarb ();
			}
			dsqlstatus = fetch_sql (varb_cursor);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 1 :
		    if (varb_cursor1 == -1)
			{
				varb_cursor1 = PrepareVarb1 ();
			}
			dsqlstatus = fetch_sql (varb_cursor1);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 2 :
		    if (varb_cursor2 == -1)
			{
				varb_cursor2 = PrepareVarb2 ();
			}
			dsqlstatus = fetch_sql (varb_cursor2);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
 }
  return dsqlstatus;

}


int LS_EINZEL_CLASS::CloseVarb (void)
{
    if (varb_cursor != -1)
    {
        close_sql (varb_cursor);
        varb_cursor = -1;
    }
    if (varb_cursor1 != -1)
    {
        close_sql (varb_cursor1);
        varb_cursor1 = -1;
    }
    if (varb_cursor2 != -1)
    {
        close_sql (varb_cursor2);
        varb_cursor2 = -1;
    }
    return 0;
}

int LS_EINZEL_CLASS::lese_max_posi (short dmdn, short dfil, long dls, long dauf)
{
	lspeinzel.posi = 0;
    ins_quest ((char *)   &dmdn, 1, 0);
    ins_quest ((char *)   &dfil, 1, 0);
    ins_quest ((char *)    &dls, 2, 0);
    ins_quest ((char *)    &dauf, 2, 0);
    out_quest ((char *)   &lspeinzel.posi, 1, 0);
	int dsqlstatus = execute_sql ("select posi from lspeinzel "
                                     "where mdn = ? "
                                     "and fil = ? "
								  "and ls = ? and auf = ? order by posi desc " );
	if (dsqlstatus != 0) return 0;
	return lspeinzel.posi;
}


BOOL LS_EINZEL_CLASS::RezAufloesung (short AufrufTiefe,double a_lsp, double rez_artikel, double me, short isExtra, short dteil_smt)
{
	double varb_gew = 0.0;
	double chg_gew = 0.0;
	short dposi;
	double dmenge = 0.0;
    char wert [5];
	short ag_salat = 6;  //Artikelgruppe erst mal so rein 
	short ag_dessert = 9;
	short ag_dressing = 7;
	short ag_suppe = 8;
	double beilage_a_gew ;

	lspeinzel.a_lsp = rez_artikel;

	ins_quest  ((char *) &lspeinzel.mdn, 1, 0);
	ins_quest  ((char *) &lspeinzel.a_lsp, 3, 0);
	out_quest  ((char *) prdk_k_rez, 0, 8);
	out_quest  ((char *) &chg_gew, 3, 0);
	out_quest  ((char *) &varb_gew, 3, 0);
	int dsqlstatus = execute_sql ("select rez, chg_gew, varb_gew from prdk_k "
                                     "where mdn = ? "
                                     "and a = ? "
								  "and akv = 1" );
	if (dsqlstatus != 0) return dsqlstatus;
	if (chg_gew == 0.0) return 1;
//	if (atoi(ChargengroesseVarb) == 1) chg_gew = varb_gew; 


	OpenVarb (AufrufTiefe);
	dposi = lese_max_posi (lspeinzel.mdn,lspeinzel.fil,lspeinzel.ls,lspeinzel.auf);

	while (FetchVarb (AufrufTiefe) == 0)
    {
		lspeinzel.a = varb_artikel;
		lspeinzel.a_lsp = a_lsp;
        lspeinzel.auf_me = 0.0;

		if (lese_lsp_a_a_lsp (lspeinzel.mdn,lspeinzel.fil,lspeinzel.ls, lspeinzel.auf, lspeinzel.a, a_lsp) == 100)
		{
			dposi += 10;
			lspeinzel.posi = dposi;
		}
		lspeinzel.me_einh = varb_me_einh;
		lspeinzel.me_einh_kun = varb_me_einh;
		lspeinzel.me_einh_ist = varb_me_einh;
//Essig
//Die Reihenfolge auf der Seite 1 des MIKRO ges. soll folgender ma�en sein:
//Artikelnr.���� - ���80000 an 1 Stelle ( Suppe)  ag=8
//-����� 20000 an 2 Stelle ( Hauptkomponente) ag 2 
//-����� 30000 an 3 Stelle ( So�e)		 ag 3
//-����� 40000 an 4 Stelle ( Gem�se)	ag 4
//-����� 50000 an 5 Stelle ( Dessert )	ag 5
		lspeinzel.sort = (short) varb_ag * 100;     //090712   * 100
		if (varb_ag == 8) lspeinzel.sort = 100;





// Essig  wenn posi_ext gef�llt, dann wird f�r die ganze Position di alternative Beilage benutzt
		dsqlstatus = lese_a_bas (lspeinzel.a);
		beilage_a_gew = _a_bas.a_gew;
		if (_a_bas.ag == AG_BEILAGE)
		{
			if (lspeinzel.posi_ext > 0)
			{
				if (lspeinzel.posi_ext != (int) lspeinzel.a)
				{
					lspeinzel.a = (double) lspeinzel.posi_ext;

					dsqlstatus = lese_a_bas (lspeinzel.a); //030812
					lspeinzel.me_einh = _a_bas.me_einh; //030812

				}
			}
		}
        memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
        sprintf (wert, "%hd", lspeinzel.me_einh);
        dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);

        if (dsqlstatus == 0)
        {
                   strcpy (lspeinzel.auf_me_bz, ptabn.ptbezk);
                   strcpy (lspeinzel.lief_me_bz, ptabn.ptbezk);
        }



// jetzt Kundenbranche pr�fen   
// je nach Kundenbranche : wird Salat oder/und Dessert seperat kommissioniert :
//	kun.kun_bran2 : ptwer1 = 1 -> Salate sep. Kommissionieren 
//	kun.kun_bran2 : ptwer2 = 1 -> Dessert sep. Kommissionieren 
//	(wenn beides = 1 : Salate und Dessert auf einem Kommissionierzettel

		lspeinzel.teil_smt = dteil_smt;
		lspeinzel.extra = 0;
		if (Salat_extra > 0 && (ag_salat == _a_bas.ag || ag_dressing == _a_bas.ag ))
		{
			lspeinzel.extra = Salat_extra;
		}
		if (Dessert_extra > 0 && ag_dessert == _a_bas.ag)
		{
			lspeinzel.extra = Dessert_extra;
		}
		if (Suppe_extra > 0 && ag_suppe == _a_bas.ag)
		{
			lspeinzel.extra = Suppe_extra;
		}
	// 	if (isOhneSalat) lspeinzel.extra = 1;







		if (prdk_me_einh != 2) chg_gew = 1.0;   //Wenn das Men� St�ckartikel ist, dan 1:1 �bertragen , Chargengewicht ist dann immer 1 St�ck
		lspeinzel.prdk_typ = 10;
		if (varb_typ == 2) lspeinzel.prdk_typ = 11; //Grundbr�t

		if ((ag_salat == _a_bas.ag || ag_dressing == _a_bas.ag)  && isExtra == 0)  //d.h. Artikel ist Salat oder Dressing , kommt aber nicht aus einem Salatbuffet(isExtra == 0)
		{
			lspeinzel.prdk_typ = 20; //Diese Salat und Dressingbeilagen werden mit prdk_typ = 20 gekennzeichnet, damit sie deaktiviert werden k�nnen, wenn im Auftrag Salatbuffet vorkommt
		}


		lspeinzel.prdk_stufe = AufrufTiefe; //wenn > 0 , dann Materialien aus Grundbr�t 
		if (ModusCatering == 1) dmenge = me  * (varb_me_ist / chg_gew);
		if (ModusCatering == 2) dmenge = me ;
        lspeinzel.auf_me += dmenge;
//		lspeinzel.a_me_einh = me_einh;
		if (varb_typ == 2)  //Grundbr�t
        {
			RezAufloesung ((AufrufTiefe + 1) ,a_lsp,lspeinzel.a, dmenge,isExtra,dteil_smt) ;
        }
		else
		{
			if (ModusCatering == 1 || (ModusCatering == 2 && lspeinzel.extra > 0))
			{
				if (lspeinzel.posi_ext == -1 && _a_bas.ag == AG_BEILAGE)
				{
					//im Shop wurden auf einen Artikel mehrere alternative Beilagen f�r ein Men� Bestellt, daher nicht eindeutig zuordbar , also nochmal in shopaufp nachschauen
					int dret = update_lsp_mehrf (lspeinzel.mdn,lspeinzel.fil,lspeinzel.ls, lspeinzel.auf, lspeinzel.a, a_lsp, lspeinzel.posi, lspeinzel.auf_me, varb_me_einh,beilage_a_gew);
					if (dret == -1) lspeinzel.posi_ext = 0; //EF-55
				}

				if (lspeinzel.posi_ext == -1 && _a_bas.ag == AG_BEILAGE) //EF-55
				{
				}
				else
				{

				  //030812
					if (lspeinzel.me_einh == 2) 
					{
						if (varb_me_einh != 2)
						{
							// in diesem Fall ist dei Beilabe = St�ck  und die alternative Beilage = kg , dann soll das Artikelgewicht herangezogen werden
							lspeinzel.auf_me *= beilage_a_gew ;
						}
			
					}
					else
					{
						if (varb_me_einh == 2)
						{
							// in diesem Fall ist dei Beilabe = kg und die alternative Beilage = St�ck , dann soll immer 2 stk rein (2 Semmelkn�del im Essen z.b. 030812)
							lspeinzel.auf_me = me * 2 ;
						}
					}

					update_lsp (lspeinzel.mdn,lspeinzel.fil,lspeinzel.ls, lspeinzel.auf, lspeinzel.a, a_lsp, lspeinzel.posi);
				}
			}
		}

	}


	return TRUE;
}




int LS_EINZEL_CLASS::Schreibe_tmp_catering (short dmdn, short dfil, long dls, long dauf)
{
	return 0;
}
