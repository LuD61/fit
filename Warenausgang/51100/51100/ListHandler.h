// ListHandler.h: Schnittstelle f�r die Klasse CListHandler.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LISTHANDLER_H__939C9EA0_6AFE_44AC_A366_675E08A3C6E5__INCLUDED_)
#define AFX_LISTHANDLER_H__939C9EA0_6AFE_44AC_A366_675E08A3C6E5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>
#include "Aufps.h"
#include "DataCollection.h"

class CListHandler  
{
public:
	enum LIST_MODE
	{
		AllEntries = 0,
		ValueEntries = 1,
	};
public:
	class CIdxItem
	{
		public:
			int IdxAll;
			int IdxValue;
			CIdxItem () 
			{
				IdxAll = 0;
				IdxValue = 0;
			}
			CIdxItem (int IdxAll, int IdxValue) 
			{
				this->IdxAll   = IdxAll;
				this->IdxValue = IdxValue;
			}
		
	};

	class CIdxCollection :public CDataCollection<CIdxItem *>
	{
	public:
		int GetIdxAll   (int IdxValue);
		int GetIdxValue (int IdxAll);
		CIdxItem *GetItemByAll (int IdxAll);
		CIdxItem *GetItemByValue (int IdxValue);
	};

private:
	LIST_MODE ActiveMode;
	BOOL LoadComplete;
	BOOL enableActivate;
	BOOL activated;
	CDataCollection<AUFPS *> StndAll;
	CDataCollection<AUFPS *> StndValue;
	CIdxCollection IdxCollection;
	static CListHandler *instance;
protected:
	CListHandler();
	virtual ~CListHandler();
public:

	void EnableActivate (BOOL enableActivate=TRUE)
	{
		this->enableActivate = enableActivate;
	}

	BOOL CanActivate ()
	{
		return enableActivate;
	}

	void SetLoadComplete (BOOL LoadComplete=TRUE)
	{
		this->LoadComplete = LoadComplete;
	}


	BOOL GetLoadComplete ()
	{
		return LoadComplete;
	}

	CDataCollection<AUFPS *> *GetStndAll ()
	{
		return &StndAll;
	}

	CDataCollection<AUFPS *> *GetStndValue ()
	{
		return &StndValue;
	}

	static CListHandler *GetInstance ();
	void DestroyInstance ();
	void Init ();
	void InitAufMe ();
	void SetNewPosi (); //FS-126
    int HoleStndValue (AUFPS aufparr[MAXPOS], int *idx); //FS-126

	int Activate (AUFPS aufparr[MAXPOS],int aufpanz, int *idx);
	void SetStndAllA (AUFPS *Aufps);
	void SetStndValueA (AUFPS *Aufps, int posi, int idxAll);
	void DropStndValueA (AUFPS *Aufps, int idxAll);
	int GetAllPosiA (AUFPS *Aufps);
	int Switch (AUFPS aufparr[MAXPOS], int *idx);
	void Update (int idx, AUFPS *aufps);
	double GetSmtPart ();
	void SortByPosi (CDataCollection<AUFPS *> *StndCollection);
	static int ComparePosi (const void *el1, const void *el2);

};

#endif // !defined(AFX_LISTHANDLER_H__939C9EA0_6AFE_44AC_A366_675E08A3C6E5__INCLUDED_)
