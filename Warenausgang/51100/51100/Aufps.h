#ifndef __AUFPS_DEF
#define __AUFPS_DEF

#define MAXLEN 40
#define MAXPOS 5000
#define LPLUS 1

struct AUFPS
{
       char posi [80];
	   long pos_id;
       char sa_kz_sint [80];
       char a [80];
       char a_kun [20];
       char lang_bez [100];
       char a_bz1 [80];
       char a_bz2 [80];
	   char last_me[80];
	   char last_pr_vk[80];
       char auf_me [80];
       char me_bz [80];
       char auf_vk_pr [80];
       char auf_lad_pr [80];
       char auf_lad_pr_prc [80];
       char marge [80];
       char basis_me_bz [80];
       char teil_smt [5];
       char me_einh_kun [5];
       char me_einh [5];
       char aufp_txt [9];
       char me_einh_kun1 [5];
       char auf_me1 [14];
       char inh1 [14];
       char me_einh_kun2 [5];
       char auf_me2 [14];
       char inh2 [14];
       char me_einh_kun3 [5];
       char auf_me3 [14];
       char inh3 [14];
	   char rab_satz [10];
	   char prov_satz [10];
       char lief_me [80];
	   char gruppe [9];
       char auf_vk_dm [80];
       char auf_lad_dm [80];
       char auf_vk_euro [80];
       char auf_lad_euro [80];
       char auf_vk_fremd [80];
       char auf_lad_fremd [80];
       char kond_art0 [20];
       char kond_art [20];
       char a_grund  [20];
       char posi_ext  [9];
	   char last_ldat [12];
       short ls_pos_kz;
       char  a_ers [16];
       short dr_folge;
       char  ls_charge [35];
       double charge_gew;
	   char aufschlag [5];
	   char aufschlag_wert [20];
	   short a_typ;
	   double a_leih;
	   short pos_txt_kz;
	   short userdef1; //WAL-70
	   double a_gew;
	   char bsd[80];
	   char bsd2[80];
	   char auf_lad_pr0 [80];

};
#endif