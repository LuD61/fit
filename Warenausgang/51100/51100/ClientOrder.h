// ClientOrder.h: Schnittstelle f�r die Klasse CClientOrder.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIENTORDER_H__211724AB_0CF5_4C96_B9F7_78B8C0CEEE2F__INCLUDED_)
#define AFX_CLIENTORDER_H__211724AB_0CF5_4C96_B9F7_78B8C0CEEE2F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "stnd_auf.h"
#include "ListHandler.h"
#include "DataCollection.h"
#include "Aufps.h"
#include "a_hndw.h"
#include "mdn.h"
#include "fil.h"
#include "kun.h"
#include "aktion.h"
#include "akt_krz.h"
#include "a_pr.h"
#include "mo_a_pr.h"
#include "mo_preis.h"
#include "ptab.h"
#include "mo_einh.h"
#include "kun.h"
#include "aufkun.h"
#include "kumebest.h"
#include "ls.h"
#include "DllPreise.h"
#include <string>
#include "GrpPrice.h"
#include "ComObject.h"
#include "cron_best.h"

using namespace std;

class CClientOrder  
{
public:
	enum STD_SORT
	{
		PosA = 0,
        AgA = 1,
		WgA = 2,
		AgBz = 3,
		WgBz = 4,
	};

	enum STD_RDOPTIMIZE
	{
		No = 0,
        FromFile = 1,
	};

	enum PR_KZ
	{
		BsdNoPrice = 0,
		BsdPrice = 1,
	};


protected:
	short mdn;
	short fil;
	short kun_fil;
	long kun_nr;
    string lieferdat;
    BOOL LastFromAufKun;
	double inh;
	Text KunBran;

	int cursor;
	short ag;
	short wg;
	char a_bz1 [25];
	char kun_bran2 [4];
	STD_SORT SortMode;
	STD_RDOPTIMIZE RdOptimize;
	BOOL RefreshRdOptimize;
	SAUFP_CLASS StndAufp;
	DB_CLASS DbClass;
	EINH_CLASS Einh;
	AUFKUN_CLASS AufKun;
	CRON_BEST_CLASS CronBest;
	LS_CLASS ls_class;
	PTAB_CLASS Ptab;
	HNDW_CLASS Hndw;
	CGrpPrice GrpPrice;
	CListHandler *ListHandler;
	CDataCollection<AUFPS *> *StndAll;
	WA_PREISE WaPreis;
	CDllPreise DllPreise;
	ComObject Com;
public:
	enum A_TYP
	{
		enumHndw = 1,
        enumEig = 2,
		enumEigDiv = 3,
		enumLeih = 12,
		enumDienst = 13,
	};


	void SetLastFromAufKun (BOOL LastFromAufKun)
	{
		this->LastFromAufKun = LastFromAufKun;

	}

	BOOL GetLastFromAufKun ()
	{
		return LastFromAufKun;
	}

	void SetLieferdat (LPSTR lieferdat)
	{
		this->lieferdat = lieferdat;
	}
	void SetSortMode (STD_SORT SortMode)
	{
		if (cursor != -1)
		{
			 StndAufp.sqlclose (cursor);
			 cursor = -1;
		}
		this->SortMode = SortMode;
	}

	STD_SORT GetSortMode ()
	{
		return SortMode;
	}

	void SetRdOptimize (STD_RDOPTIMIZE RdOptimize)
	{
		this->RdOptimize = RdOptimize;
	}

	STD_RDOPTIMIZE GetRdOptimize ()
	{
		return RdOptimize;
	}

	void SetRefreshRdOptimize (BOOL RefreshRdOptimize)
	{
		this->RefreshRdOptimize = RefreshRdOptimize;
	}


	CClientOrder();
	virtual ~CClientOrder();
	void SetKeys (short mdn, short fil, short kun_fil, long kun);
	void Load ();
	void Prepare ();
	int Read ();
	void FillRow (AUFPS *Aufps);
	void FillKondArt (AUFPS *Aufps);
	void GetLastMe (AUFPS *Aufps);
	void GetLastMeAuf (AUFPS *Aufps);
	void ReadMeEinh (AUFPS *Aufps);
	void CalcLdPrPrc (AUFPS *aufps, double vk_pr, double ld_pr);
	void WriteAufps ();
	BOOL ReadAufps ();
};

#endif // !defined(AFX_CLIENTORDER_H__211724AB_0CF5_4C96_B9F7_78B8C0CEEE2F__INCLUDED_)
