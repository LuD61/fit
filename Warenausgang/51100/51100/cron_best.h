#ifndef _CRON_BEST_DEF
#define _CRON_BEST_DEF

#include "dbclass.h"

struct CRON_BEST {
   double    a;
   short     pr_kz;
   long      bsd_stk;
   double    me;
};
extern struct CRON_BEST cron_best, cron_best_null;

#line 7 "cron_best.rh"

class CRON_BEST_CLASS  : public DB_CLASS
{
       private :
               void prepare (void);
               int cursor_a;
       public :
               CRON_BEST cron_best;  
               CRON_BEST_CLASS () : DB_CLASS ()
               {
			cursor_a = -1;	
               }
               int dbreadfirst_a ();
               int dbread_a ();
};
#endif
