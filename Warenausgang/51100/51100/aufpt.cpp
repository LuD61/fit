#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "aufpt.h"

struct AUFPT aufpt, aufpt_null;

void AUFPT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &aufpt.nr, 2, 0);
    out_quest ((char *) &aufpt.nr,2,0);
    out_quest ((char *) &aufpt.zei,2,0);
    out_quest ((char *) aufpt.txt,0,61);
            cursor = prepare_sql ("select aufpt.nr,  aufpt.zei,  "
"aufpt.txt from aufpt "

#line 26 "aufpt.rpp"
                                  "where nr = ? "
                                  "order by zei");

    ins_quest ((char *) &aufpt.nr,2,0);
    ins_quest ((char *) &aufpt.zei,2,0);
    ins_quest ((char *) aufpt.txt,0,61);
            sqltext = "update aufpt set aufpt.nr = ?,  "
"aufpt.zei = ?,  aufpt.txt = ? "

#line 30 "aufpt.rpp"
                                  "where nr = ? "
                                  "and zei  = ?";

            ins_quest ((char *) &aufpt.nr, 2, 0);
            ins_quest ((char *) &aufpt.zei, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &aufpt.nr, 2, 0);
            ins_quest ((char *) &aufpt.zei, 2, 0);
            test_upd_cursor = prepare_sql ("select nr from aufpt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &aufpt.nr, 2, 0);
            ins_quest ((char *) &aufpt.zei, 2, 0);
            del_cursor = prepare_sql ("delete from aufpt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &aufpt.nr, 2, 0);
            del_cursor_posi = prepare_sql ("delete from aufpt "
                                  "where  nr = ?");

    ins_quest ((char *) &aufpt.nr,2,0);
    ins_quest ((char *) &aufpt.zei,2,0);
    ins_quest ((char *) aufpt.txt,0,61);
            ins_cursor = prepare_sql ("insert into aufpt (nr,  "
"zei,  txt) "

#line 54 "aufpt.rpp"
                                      "values "
                                      "(?,?,?)"); 

#line 56 "aufpt.rpp"
}
int AUFPT_CLASS::dbreadfirst (void)
/**

Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int AUFPT_CLASS::delete_aufpposi (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (del_cursor_posi == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_posi);
         return (sqlstatus);
}

