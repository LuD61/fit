#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "wmaskc.h"
#include "stdfkt.h"
#include "mo_meld.h"

#ifndef DBN
#include "mo_menu.h"

class MENUE_CLASS menue_class;

#endif

extern HWND hMainWindow;
extern char fktscolor2[];
extern char headscolor[];
extern char mamain1scolor[];

COLORREF fktcolor2 = GRAYCOL;
COLORREF headcolor = GRAYCOL;
COLORREF mamain1color = LTGRAYCOL;
int ColOk = 0;
HWND hWndToolTip;
HWND hwndEdit;
static char **qInfo;
static UINT *qIdfrom;
static char **qhWndInfo;
static HWND **qhWndFrom; 

static int hMx;
static int hMy;
static int hMcx;
static int hMcy;

mfont fkfont    = {NULL , -1, -1, 1,
                                  GRAYCOL,
                                  RGB (0, 0, 120),
                                  0,
                                  NULL};

static ITEM punkte1 ("", "...........", "", 0); 
static ITEM punkte2 ("", "...........", "", 0); 
static ITEM punkte3 ("", "...........", "", 0); 
static ITEM punkte4 ("", "...........", "", 0); 
static ITEM punkte5 ("", "...........", "", 0); 
static ITEM punkte6 ("", "..........", "", 0); 

static field _fkformt[] = {
&punkte1,11, 0, 0, 3, 0, "", READONLY,0, 0, 0,
&punkte2,11, 0, 0,16, 0, "", READONLY,0, 0, 0,
&punkte3,11, 0, 0,29, 0, "", READONLY,0, 0, 0,
&punkte4,11, 0, 0,42, 0, "", READONLY,0, 0, 0,
&punkte5,11, 0, 0,56, 0, "", READONLY,0, 0, 0,
&punkte6,11, 0, 0,70, 0, "", READONLY,0, 0, 0
};

static form fkformt = {6, 0, 0, _fkformt, 0, 0, 0, NULL, &fkfont};

/*
static ITEM z6  ("", "6 ..........", "", 0);
static ITEM z7  ("", "7 ..........", "", 0);
static ITEM z8  ("", "8 ..........", "", 0);
static ITEM z9  ("", "9 ..........", "", 0);
static ITEM z10 ("", "10 ..........", "", 0);
static ITEM z11 ("", "11 ..........", "", 0);
*/

static ITEM z6  ("", "6 ..........", NULL, 0);
static ITEM z7  ("", "7 ..........", NULL, 0);
static ITEM z8  ("", "8 ..........", NULL, 0);
static ITEM z9  ("", "9 ..........", NULL, 0);
static ITEM z10 ("", "10 ..........", NULL, 0);
static ITEM z11 ("", "11 ..........", NULL, 0);
static ITEM z12 ("", "    ",          NULL, 0);

static field _fkform[] = {
&z6,         13, 0, 0, 0, 0, "", BUTTON | DISABLED,  0, 0, 0, 
&z7,         13, 0, 0,13, 0, "", BUTTON | DISABLED,  0, 0, 0, 
&z8,         13, 0, 0,26, 0, "", BUTTON | DISABLED,  0, 0, 0, 
&z9,         13, 0, 0,39, 0, "", BUTTON | DISABLED,  0, 0, 0, 
&z10,        14, 0, 0,52, 0, "", BUTTON | DISABLED,  0, 0, 0, 
&z11,        13, 0, 0,66, 0, "", BUTTON | DISABLED,  0, 0, 0,
&z12,        60, 0, 0,79, 0, "", BUTTON | DISABLED,  0, 0, 0,
};

static form fkform = {7, 0, 0, _fkform, 0, 0, 0, NULL, NULL};


mfont fkfont2    = {NULL, -1, -1, 0,
                                       BLACKCOL,
                                       fktcolor2,
                                       0};

static ITEM hilfe      ("", "Hilfe", "", 0);
static ITEM zurueck     ("", "Zur�ck", "", 0);
static ITEM vor        ("", "Vor", "", 0);
static ITEM abbruch    ("", "Abbruch", "", 0);
static ITEM ausfuehren ("", "Ausf�hren", "", 0);


static field _fkform2[] = {
&hilfe,         10, 0, 0,  1, 0, "",        BUTTON, 0, 0, KEY1,
&zurueck,       10, 0, 0, 13, 0, "",        BUTTON, 0, 0, KEYPGU,
&vor,            7, 0, 0, 25, 0, "",        BUTTON, 0, 0, KEYPGD,
&abbruch,       13, 0,   0, 34, 0,   "",    BUTTON, 0, 0, KEY5,
&ausfuehren,    15, 0,   0, 49, 0,   "",    BUTTON, 0, 0, KEY12
};

static form fkform2 = {5, 0, 0, _fkform2, 0, 0, 0, NULL, &fkfont2};

HWND ftasten = NULL;
HWND ftasten2 = NULL;
HWND MesshWnd = NULL;
HWND head = NULL;

static ITEM kopffeld1 ("", "SE.TEC            ", "", 0);
static ITEM kopffeld2 ("", "Listengenerator          ", "", 0);
static ITEM aktfeld   ("", "           ", "", 0);
static ITEM svon ("","1 ", "", 0);
static ITEM sbis ("","1 ", "", 0);
static ITEM seite ("", "Seite", "", 0);
static ITEM gr ("", ">", "", 0);
static ITEM kl ("", "<", "", 0);

mfont kopffont2    = {NULL , -1, -1, 1,
                                  BLACKCOL,
                                  LTGRAYCOL,
                                  0,
                                  NULL};

static field _kopf1[] = {
&kopffeld1,          0, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form kopf1 = {1, 0, 0, _kopf1, 0, 0, 0, 0, NULL};

static field _kopf2[] = {
&kopffeld2,          0, 0, 0,20, 0, "", DISPLAYONLY, 0, 0, 0
};

static form kopf2 = {1, 0, 0, _kopf2, 0, 0, 0, 0, &kopffont2};
// static form kopf2 = {1, 0, 0, _kopf2, 0, 0, 0, 0, 0};


static field _kopf3[] = {
&aktfeld,            0, 0, 0, 50, 0, "",    DISPLAYONLY, 0, 0, 0,
&seite,              0, 0, 0, 63, 0, "",    DISPLAYONLY, 0, 0, 0,
&svon,               3, 0, 0, 69, 0, "%2d", READONLY, 0, 0, 0,
&gr,                 2, 0, 0, 72, 0, "",    BUTTON | DISABLED, 0, 0, KEYPGD,
&kl,                 2, 0, 0, 74, 0, "",    BUTTON| DISABLED, 0, 0, KEYPGU,
&sbis,               3, 0, 0, 76, 0, "%2d", READONLY, 0, 0, 0
};

static form kopf3 = {6, 0, 0, _kopf3, 0, 0, 0, 0, NULL};


int fillcolref (char scolor[], char *colref)
/**
**/
{
       char cols [4];
       char *fpos;
       int color;

       color = 0;
       fpos = strstr (scolor, colref);

       if (fpos)
       {
                  fpos += strlen (colref);
                  memcpy (cols, fpos, 3);
                  cols[3] = (char) 0;
                  color = atoi (cols);
       }
       return color;
}

void fillcolor (char scolor[], COLORREF *color)
/**
**/
{
       int red, green, blue;

       red   = fillcolref (scolor, "red");
       green = fillcolref (scolor, "green");
       blue  = fillcolref (scolor, "blue");
       *color = RGB (red, green, blue);
}

void FillColors (void)
/**
**/
{
       if (ColOk) return;
       ColOk = 1;

       if (strcmp (fktscolor2, " "))
       {
                     fillcolor (fktscolor2, &fktcolor2);
       }

       if (strcmp (headscolor, " "))
       {
                     fillcolor (headscolor, &headcolor);
       }
       if (strcmp (mamain1scolor, " "))
       {
                     fillcolor (mamain1scolor, &mamain1color);
       }
}

int Fktx, Fkty, Fktcx, Fktcy;
int Fktx2, Fkty2, Fktcx2, Fktcy2;

void SaveFktPos (void)
{
       RECT frect;
       RECT frect2;
       RECT mrect;

       GetWindowRect (ftasten,  &frect);
       GetWindowRect (ftasten2, &frect2);
       GetWindowRect (hMainWindow, &mrect);

       Fktx = frect.left - mrect.left;
       Fkty = mrect.bottom - frect.top;
       Fktcy = frect.bottom - frect.top;

       Fktx2 = frect2.left - mrect.left;
       Fkty2 = mrect.bottom - frect2.top;
       Fktcy2 = frect2.bottom - frect2.top;
}

void MoveFkt (void)
{
       RECT mrect;
       int x,y,cx, cy;

       GetWindowRect (hMainWindow, &mrect);

       x = 0;
       y = (mrect.bottom - Fkty - mrect.top - 50);
       cx = mrect.right - mrect.left;
       cy = Fktcy;

       MoveWindow (ftasten, x,y,cx,cy, TRUE);

       x = 0;
       y = (mrect.bottom - Fkty2 - mrect.top - 50);
       cx = mrect.right - mrect.left;
       cy = Fktcy2;

       MoveWindow (ftasten2, x,y,cx,cy, TRUE);
}


HWND OpenFkt (HANDLE hInstance)
/**
Funktionstasten-Fenster oeffnen.
**/
{

       SetEnvFont ();
       FillColors ();
       SetBorder (NULL);
       SetAktivWindow (NULL);
       ftasten = OpenColWindow (1, 80, 21, 0, WS_CHILD,
                                  LTGRAYCOL, NULL);
       display_form (ftasten, &fkform, 0, 0);
//       display_form (ftasten, &fkformt, 0, 0);
       SetAktivWindow (NULL);
       ftasten2 = OpenColWindow (1, 80, 23, 0, WS_CHILD,
                                 fktcolor2, NULL);
       display_form (ftasten2, &fkform2, 0, -1);
       if (headcolor != GRAYCOL)
       {
                     SetAktivWindow (NULL);
                     SetDiffPixel ( -2, 3, 7, 0);
                     head = OpenColWindowEx (1, 78, 4, 1, WS_POPUP,
                                           headcolor, NULL);
                     SetDiffPixel ( 0, 0, 0, 0);
                     GetDiffPixel (hMainWindow, head, &hMx, &hMy,
                                                      &hMcx, &hMcy);
       }
       return ftasten;
}

HWND OpenFktM (HANDLE hInstance)
/**
Funktionstasten-Fenster oeffnen.
**/
{

       SetEnvFont ();
       FillColors ();
       SetBorder (NULL);
       SetAktivWindow (NULL);
       ftasten = OpenColWindow (1, 80, 21, 0, WS_CHILD,
                                  LTGRAYCOL, NULL);
       display_form (ftasten, &fkform, 0, 0);
       SetAktivWindow (NULL);
       ftasten2 = OpenColWindow (1, 80, 23, 0, WS_CHILD,
                                 fktcolor2, NULL);
       display_form (ftasten2, &fkform2, 0, -1);
       SaveFktPos ();
       return ftasten;
}

void MoveHeader (void)
/**
Headerwindow anpassen.
**/
{
       RECT rect;

       if (head == NULL) return;

       GetWindowRect (hMainWindow, &rect);
       MoveWindow (head, rect.left + hMx, rect.top + hMy, hMcx, hMcy,
                   TRUE);
}


HWND OpenMamain1Ex (int rows, int columns,
                    int row, int column,
                    HINSTANCE hInstance, WNDPROC WndProc,
                    form *frm, mfont *font)
/**
Haupteingabefenster oeffnen.
**/
{
       SetEnvFont ();
       FillColors ();
       HWND mamain1;

       if (mamain1color != LTGRAYCOL)
       {
              mamain1 = OpenColWindowEx (rows, columns, row , column,
                                           WS_CHILD,
                                           mamain1color, WndProc);
              if (frm != NULL && font != NULL)
              {
                        frm->font = font;
                        font->FontBkColor = mamain1color;
              }
       }
       else
       {
              mamain1  = OpenWindowEx (rows, columns, row, column,
                                       WS_CHILD, hInstance);

       }
       return mamain1;
}
       
HWND OpenMamain1 (int rows, int columns,
                    int row, int column,
                    HINSTANCE hInstance, WNDPROC WndProc,
                    form *frm, mfont *font)
/**
Haupteingabefendtsr oeffnen.
**/
{
       SetEnvFont ();
       FillColors ();
       HWND mamain1;

       if (mamain1color != LTGRAYCOL)
       {
              mamain1 = OpenColWindow (rows, columns, row , column,
                                           WS_CHILD,
                                           mamain1color, WndProc);
              if (frm != NULL && font != NULL)
              {
                        frm->font = font;
                        font->FontBkColor = mamain1color;
              }
       }
       else
       {
              mamain1  = OpenWindow (rows, columns, row, column,
                                     1, hInstance);
       }
       return mamain1;
}

void UpdateFkt (void)
/**
Update fuer Window ftasten.
**/
{
        form *savecurrent;

        savecurrent = current_form;
        current_form = &fkform;
        UpdateWindow (ftasten);
        current_form = &fkform2;
        UpdateWindow (ftasten2);
        current_form = savecurrent;
}

void SetFkt (int fktnr, char *text, int taste)
/**
Text fuer Funktionstaste setzen.
**/
{
         form *savecurrent;
         char fktext [15];

         savecurrent = current_form;
         current_form = &fkform;
         if (fktnr > 11 || fktnr < 6) return;

         fktnr -= 6;
		 if (strlen (text) == 0)
         {
			      strcpy (text, "..........");
		 }
		 sprintf (fktext, " %d %s", fktnr + 6,  text);
         fkform.mask[fktnr].item->SetFeldPtr (fktext);
         fkform.mask[fktnr].BuId = taste;
         if (taste)
         {
                   fkform.mask[fktnr].attribut = BUTTON;
         }
         else
         {
                   fkform.mask[fktnr].attribut |= DISABLED;
         }

         CloseControl (&fkform, fktnr);
         display_field (ftasten, &fkform.mask[fktnr], 0, 0);

         current_form = savecurrent;
}

int SendKey (int key)
/**
Tastaturmelung senden.
**/
{
        if (SendWindow && current_form)
        {
                  PostMessage (current_form->mask[currentfield].feldid,
                               WM_KEYDOWN,  (WPARAM) key, 0l);
        }
        else
        {
                 PostMessage (AktivWindow, WM_KEYDOWN,  (WPARAM) key, 0l);
        }
        return 0;
}

int testjn (void)
/**
Aktuelles Feld auf Zeichen J oder N pruefen.
**/
{
	   
	    char *buffer;
		
		if (testkeys ()) return 1;

		buffer = 

			current_form->mask [currentfield].item->GetFeldPtr ();

		if (buffer[0] != 'J')
		{
			buffer[0] = 'N';
		}

		AktivWindow = GetParent (GetActiveWindow ());
        display_field (AktivWindow, 
			           &current_form->mask[currentfield],
					   0, 0);

		return 1;
}

int abfragejn (HWND hDlg, char *text, char *def)
/**
Window mit Abfrage auf Ja oder Nein.
**/
{
        int ret;

        EnableWindows (hDlg, FALSE);
        if (def[0] == 'J' || def[0] == 'j')
        {
                 ret = MessageBox (NULL, text, "", MB_YESNO |
                                                   MB_TOPMOST | 
                                                   MB_ICONWARNING);
        }
        else
        {
                 ret = MessageBox (NULL, text, "",
                                         MB_YESNO | MB_DEFBUTTON2 |
                                         MB_ICONWARNING);
        }
        EnableWindows (hDlg, TRUE);
        if (ret == IDYES) return (1);
        return (0);
}

#ifndef DBN
void DisplayKopf1 (HWND Window, char *text)
{
        int dsqlstatus;
        SetEnvFont ();
        
        dsqlstatus = menue_class.Lesesys_inst ();
        if (dsqlstatus == 0)
        {
                     kopffeld1.SetFeld (sys_inst.projekt);
        } 
        kopffeld2.SetFeld (text);
        display_form (Window, &kopf1, 0, 0);
        display_form (Window, &kopf2, 0, 0);
}
#endif

void DisplayKopf3 (HWND Window, char *text, int sv, int sb)
{
        static int svb = 0;
        form *frm;

        frm = current_form;
        aktfeld.SetFeld (text);
        svon.SetFormatFeld ("%d", sv);
        sbis.SetFormatFeld ("%d", sb);

        if (sb > 1)
        {
                   kopf3.mask[3].attribut = BUTTON;
                   kopf3.mask[4].attribut = BUTTON;
                   if (svb == 0)
                   {
                             CloseControl (&kopf3, 3);
                             CloseControl (&kopf3, 4);
                   }
                   svb = 1;
        }
        else
        {
                   kopf3.mask[3].attribut |= DISABLED;
                   kopf3.mask[4].attribut |= DISABLED;
                   if (svb == 1)
                   {
                             CloseControl (&kopf3, 3);
                             CloseControl (&kopf3, 4);
                   }
                   svb = 0;
        }
        display_form (Window, &kopf3, 0, 0);
        current_form = frm;
}


HMENU MakeMenue (struct PMENUE *menue)
/**
Menue generieren.
**/
{
	    int i;
        HMENU hMenu, hMenuPopup;
		struct PMENUE *nmenue;

        hMenu = CreateMenu ();

		for (i = 0; menue[i].text; i ++)
		{
			  if (menue [i].typ [0] == ' ')
			  {	  
                  AppendMenu (hMenu, MF_STRING, menue[i].menid,
                              menue[i].text);
			  }
			  else if (menue [i].typ [0] == 'G')
			  {	  
                  AppendMenu (hMenu, MF_STRING , menue[i].menid,
                              menue[i].text);
                  EnableMenuItem (hMenu, menue[i].menid, MF_GRAYED);
			  }
			  else if (menue [i].typ [0] == 'C')
			  {	  
                  AppendMenu (hMenu, MF_STRING | MF_CHECKED, 
					          menue[i].menid,
                              menue[i].text);
			  }
   			  else if (menue [i].typ [0] == 'M')
			  {
				   nmenue = (struct PMENUE *) menue[i].nmenue;
				   hMenuPopup = MakeMenue (nmenue);
		           AppendMenu (hMenu, MF_POPUP, 
					           (UINT) hMenuPopup, menue[i].text);
              }
   			  else if (menue [i].typ [0] == 'S')
			  {
		           AppendMenu (hMenu, MF_SEPARATOR, 
					                  0, NULL);
              }
		}
        return hMenu; 
}


BOOL QuickHwndCpy (LPTOOLTIPTEXT lpttt)
/**
Text in Quickinfo fuer Kindfenster der Toolbar.
**/
{
         int i;

         if (lpttt->uFlags & TTF_IDISHWND == 0) return FALSE;

         for (i = 0; qhWndFrom[i]; i ++)
         {
                       if (lpttt->hdr.idFrom == (UINT) *qhWndFrom[i])
                       {
                                           break;
                       }
        }                            

        if (qhWndFrom [i] == NULL)
        {
                        writelog ("Nicht gefunden");
                        return FALSE;
        }
        lstrcpy (lpttt->szText, qhWndInfo [i]);
        return TRUE;
}


BOOL QuickCpy (LPSTR text, UINT idFrom)
/**
Text in QuickInfo kopieren.
**/
{
        int i;

        for (i = 0; qIdfrom [i]; i ++)
        {
                    if (idFrom == qIdfrom [i]) break;
        }
        if (qIdfrom [i] == 0) return FALSE;
        lstrcpy (text, qInfo [i]);
        return TRUE;
}


BOOL ComboToolTip (HWND hwndToolBar, HWND hwndComboBox)
{
     BOOL bSuccess ;
     TOOLINFO ti ;

     // Add tooltip for main combo box
     ZeroMemory (&ti, sizeof (TOOLINFO)) ;
     ti.cbSize = sizeof (TOOLINFO) ;
     ti.uFlags = TTF_IDISHWND | TTF_CENTERTIP | TTF_SUBCLASS ;
     ti.hwnd   = hwndToolBar ;
     ti.uId    = (UINT) (HWND) hwndComboBox ;
     ti.lpszText = LPSTR_TEXTCALLBACK ;
     bSuccess = ToolTip_AddTool (hWndToolTip, &ti) ;
     if (!bSuccess)
          return FALSE ;

     // Add tooltip for combo box's edit control
     hwndEdit = GetWindow (hwndComboBox, GW_CHILD) ;
     ti.uId    = (UINT) (HWND) hwndEdit ;
     bSuccess = ToolTip_AddTool (hWndToolTip, &ti) ;

     return bSuccess ;
}

HWND MakeToolBar (HWND hWnd, TBBUTTON *tbb, char **qI,UINT *qId,
				                        char **qhWI, HWND **qhWF)
{

	     HWND hWndTB;

         hWndTB = CreateToolbarEx (hWnd,
                                   WS_CHILD | WS_VISIBLE |
                                   CCS_TOP | CCS_NODIVIDER |
                                   TBSTYLE_TOOLTIPS,
                                   1, 15, HINST_COMMCTRL,
                                   IDB_STD_SMALL_COLOR,
                                   tbb,
                                   4,
                                   0, 0,
                                   0, 0,
                                   sizeof (TBBUTTON));
         hWndToolTip = ToolBar_GetToolTips (hWndTB);
		 qInfo = qI;
		 qIdfrom = qId;
		 qhWndInfo = qhWI;
		 qhWndFrom = qhWF;
		 return hWndTB;
}


HWND MakeToolBarEx (HANDLE hMainInst, HWND hWnd, TBBUTTON *tbb,
                                        int tbbanz, 
                                        char **qI,UINT *qId,
				                        char **qhWI, HWND **qhWF)
{
	     HWND hWndTB;


         hWndTB = CreateToolbarEx (hWnd,
                                   WS_CHILD | WS_VISIBLE |
                                   CCS_TOP |
                                   TBSTYLE_TOOLTIPS,
                                   1, 4, hMainInst,
                                   FTOOLBAR,
                                   &tbb[0],
                                   tbbanz,
                                   0, 0,
                                   0, 0,
                                   sizeof (TBBUTTON));
         ShowWindow (hWndTB, SW_SHOWNORMAL);
         UpdateWindow (hWndTB);
         hWndToolTip = ToolBar_GetToolTips (hWndTB);
		 qInfo = qI;
		 qIdfrom = qId;
		 qhWndInfo = qhWI;
		 qhWndFrom = qhWF;
		 return hWndTB;
}

HWND MakeToolBarCombo (HANDLE hMainInst,
                       HWND hWnd,HWND hWndTB, int Id, int tbn1, 
                                                       int tbn2)
/**
Combobox zu Toolbar
**/
{
         int x, y, cx, cy;
         RECT rect;
         HWND hWndCombo;
         HFONT hFont;

         ToolBar_GetItemRect (hWndTB, tbn1, &rect);
         x = rect.left;
         y = rect.top;
         cy = 100;
         ToolBar_GetItemRect (hWndTB, tbn2, &rect);
         cx = rect.right - x + 1;
         hWndCombo = CreateWindow ("combobox",
                                    NULL,
                                    WS_CHILD | WS_VISIBLE | WS_VSCROLL |
                                    CBS_DROPDOWNLIST,
                                    x,y, cx, cy,
                                    hWnd,
                                    (HMENU) Id,
                                    hMainInst,
                                    0);
          SetParent (hWndCombo, hWndTB);
          stdfont ();
          hFont = SetWindowFont (hWnd);
          SendMessage (hWndCombo, WM_SETFONT,
                                 (WPARAM) hFont, (LPARAM) NULL);
          return hWndCombo;
}


#ifndef DBN
/* Mandant unf Filiale bearbeiten    */ 

#include "mdnfil.h"

MDNFIL cmdnfil;

static int testmdn (void);
static int testfil (void);
static int showmdn (void);
static int showfil (void);
static int setkey9mdn (void);
static int setkey9fil (void);

static char vmdn [4];
static char vfil [6];
static char mdn_bz [17] = {"Zentrale"};
static char fil_bz [17];

static ITEM imdn    ("mdn", vmdn, "Mandant........:", 0);
static ITEM ifil    ("fil", vfil, "Filiale........:", 0);
static ITEM imdn_bz ("", mdn_bz, "", 0);
static ITEM ifil_bz ("", fil_bz, "", 0);

static field _mdnfil [] = {
&imdn,                         3, 0, 0, 1, 0, "%2d", NORMAL, setkey9mdn, testmdn,0,
&imdn_bz,                     16, 0, 0,23, 0, "", DISPLAYONLY, 0, 0, 0,
&ifil,                         5, 0, 0,40, 0, "%4d", NORMAL, setkey9fil, testfil, 0,
&ifil_bz,                     16, 0, 0,65, 0, "", DISPLAYONLY, 0, 0, 0
};

static form mdnfil = {4, 0,0, _mdnfil, 0, 0, 0, 0, NULL};

static int field_mdn = 0;
static int field_fil = 2;

static int showmdn ()
{
	   return cmdnfil.showmdn ();
}

static int showfil ()
{
	   return cmdnfil.showfil ();
}

static int setkey9mdn ()
/**
Taste KEY9 auf aktiv setzen.
**/
{
 	   set_fkt (showmdn, 9);
       SetFkt (9, auswahl, KEY9);
       return 0;
}


static int setkey9fil ()
/**
Taste KEY9 auf aktiv setzen.
**/
{
       set_fkt (showfil, 9);
       SetFkt (9, auswahl, KEY9);
       return 0;
}


static int testmdn (void)
/**
Mandanten-Nr testen.
**/
{
         set_fkt (NULL, 9);
         SetFkt (9, leer, NULL);

         if (syskey == KEY9)
         {
                      cmdnfil.showmdn ();
                      SetCurrentField (currentfield);
                      return TRUE;
         }

         if (testkeys ()) return 0;

         if (cmdnfil.GetNoMdnNull () && atoi (vmdn) == 0)
         {
             SetCurrentField (currentfield);
             disp_mess ("Mandant 0 ist nicht erlaubt", 2);
             return FALSE;
         }

         return cmdnfil.GetMdn ();
}

static int testfil (void)
/**
Mandnaten-Nr testen.
**/
{
         set_fkt (NULL, 9);
         SetFkt (9, leer, NULL);

         if (syskey == KEY9)
         {
                      cmdnfil.showfil ();
                      SetCurrentField (currentfield);
                      return TRUE;
         }

         if (testkeys ()) return 0;
        
         if (cmdnfil.GetNoFilNull () && atoi (vfil) == 0)
         {
             disp_mess ("Filiale 0 ist nicht erlaubt", 2);
             SetCurrentField (currentfield);
             return FALSE;
         }

         return cmdnfil.GetFil ();

}


void MDNFIL::SetMandantAttr (int attribut)
/**
Attribut fuer Mandant setzen.
**/
{
         mdnfil.mask [field_mdn].attribut = attribut;
}


void MDNFIL::SetFilialAttr (int attribut)
/**
Attribut fuer Filiale setzen.
**/
{
         mdnfil.mask [field_fil].attribut = attribut;
}


int MDNFIL::showmdn ()
/**
Auswahlfenster ueber Mandanten.
**/
{
	     MDN_CLASS::ShowAllBu (AktivWindow, 0);
         lese_mdn (_mdn.mdn);
         sprintf (vmdn, "%hd", _mdn.mdn);
         strcpy (mdn_bz, _adr.adr_krz);
         display_form (AktivWindow, current_form , 0, 0);
		 SetCurrentFocus (currentfield);
         syskey = 1;
         return 0;
}

int MDNFIL::showfil ()
/**
Auswahlfenster ueber Mandanten.
**/
{
	     FIL_CLASS::ShowAllBu (AktivWindow, 0, atoi (vmdn));
         if (syskey != KEY5)
         {
                 lese_fil (_mdn.mdn, _fil.fil);
                 sprintf (vfil, "%hd", _fil.fil);
                 strcpy (fil_bz, _adr.adr_krz);
                 display_form (AktivWindow, current_form , 0, 0);
         }
		 SetCurrentFocus (currentfield);
         syskey = 1;
         return 0;
}

int MDNFIL::GetMdn ()
/**
Mandant aus Datenbank holen.
**/
{
         if (atoi (vmdn) > 0)
         {
                      if (lese_mdn (atoi (vmdn)) == 100)
                      {
                                disp_mess ("Mandant ist nicht angelegt", 2);
                                SetCurrentField (currentfield);
                                return FALSE;
                      }
                      strcpy (mdn_bz, _adr.adr_krz);

         }
         else
         {
                      strcpy (mdn_bz, "Zentrale");
         }
         display_form (AktivWindow, current_form , 0, 0);
         return 0;
}

int MDNFIL::GetFil ()
/**
Filiale aus Datenbank holen.
**/
{
         if (atoi (vfil) > 0)
         {
                      if (lese_fil (atoi (vmdn), atoi (vfil)) == 100)
                      {
                                disp_mess ("Filiale ist nicht angelegt", 2);
                                SetCurrentField (currentfield);
                                return FALSE;
                      }
                      strcpy (fil_bz, _adr.adr_krz);

         }
         else
         {
                      strcpy (fil_bz, "Mandant");
         }
         display_form (AktivWindow, current_form , 0, 0);
         return 0;
}

void MDNFIL::CloseForm (void)
/**
Maske fuer Mandant/Filiale schliessen.
**/
{
      CloseControls (&mdnfil);
}


int MDNFIL::eingabemdnfil (HWND hWnd, char *cmdn, char *cfil)
/**
Mandant und Filiale eingeben.
**/
{

	  SetAktivWindow (hWnd);
      if (sys_ben.mdn)
      {
               sprintf (vmdn, "%hd", sys_ben.mdn);
               SetMandantAttr (READONLY);
               GetMdn ();
      }

      if (sys_ben.fil)
      {
               sprintf (vfil, "%hd", sys_ben.fil);
               SetFilialAttr (READONLY);
               GetFil ();
      }

      break_end ();
      enter_form (hWnd, &mdnfil, 0, 0);
      display_form (hWnd, &mdnfil, 0, 0);
      if (syskey == KEY5 || syskey == KEYESC) return (-1);;
      if (sys_ben.mdn && sys_ben.fil) return (-1);
	  strcpy (cmdn,vmdn);
	  strcpy (cfil,vfil);
	  return 0;
}


/* Ende von Mandant unf Filiale bearbeiten    */ 
#endif


void MELDUNG::Message (HWND hWnd, char *text, int zeile)
/**
Meldung im Hauptfenster.
**/
{
	   static ITEM imess ("", "", "", 0);
	   static field _fmess [] = 
	   {
		   &imess,     60, 0, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0
	   };
	   
	   static form fmess = {1, 0, 0, _fmess, 0, 0, 0, 0, NULL};

	   HWND oldactive, oldaktiv;
	   form *scurrent;

       oldactive = GetActiveWindow ();
       oldaktiv  = AktivWindow;
	   scurrent = current_form;
	   imess.SetFeldPtr (text);
	   display_form (hWnd, &fmess, zeile, 0);
	   messform = &fmess;
	   SetActiveWindow (oldactive);
	   AktivWindow = oldaktiv;
	   current_form = scurrent;
}

HWND MELDUNG::OpenMessage (void)
/**
Funktionstasten-Fenster oeffnen.
**/
{
       HANDLE hInstance;
       HWND hMainWind, hWnd;

       hMainWind = GetActiveWindow ();
       hWnd = GetParent (hMainWind);
       while (hWnd)
       {
           hMainWind = hWnd;
           hWnd = GetParent (hMainWind);
       }

       hInstance = (HANDLE) GetWindowLong (hMainWind, 
                                               GWL_HINSTANCE);
       SetEnvFont ();
       SetBorder (NULL);
       SetAktivWindow (hMainWind);
       MesshWnd = OpenColWindow (1, 80, 22, 0, WS_CHILD,
                                  LTGRAYCOL, NULL);
       return MesshWnd;
}


void MELDUNG::Message (char *text)
/**
Meldung im Meldungsfenster.
**/
{
	   static ITEM imess ("", "", "", 0);
	   static field _fmess [] = 
	   {
		   &imess,     60, 0, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0
	   };
	   
	   static form fmess = {1, 0, 0, _fmess, 0, 0, 0, 0, NULL};

	   HWND oldactive, oldaktiv;
	   form *scurrent;

       if (MesshWnd == NULL)
       {
           OpenMessage ();
       }
       oldactive = GetActiveWindow ();
       oldaktiv  = AktivWindow;
	   scurrent = current_form;
	   imess.SetFeldPtr (text);
	   display_form (MesshWnd, &fmess, 0, 0);
	   messform = &fmess;
	   SetActiveWindow (oldactive);
	   AktivWindow = oldaktiv;
	   current_form = scurrent;
}


void MELDUNG::CloseMessage (void)
/**
Meldung schliessen.
**/
{
	   if (messform)
	   {
		   CloseControls (messform);
		   messform = NULL;
	   }
}


void MELDUNG::CloseMesshWnd (void)
/**
Meldungsfenster schliessen.
**/
{
        CloseMessage ();
        DestroyWindow (MesshWnd);
        MesshWnd = NULL;
}
