#ifndef _TEST_TMP_DEF
#define _TEST_TMP_DEF

struct TEST_TMP {
   short     mdn;
   long      lieferdat;
   short     kun_gr2;
   double    a_lsp;
   double    a;
   long      posi;
   double    auf_me;
   short     me_einh;
   char      auf_me_bz[7];
   short     teil_smt;
   short     prdk_typ;
   short     extra;
   short     sort;
   long      kun;
   long      auf;
   long      datum;
   char      zeit[12];
};
extern struct TEST_TMP test_tmp, test_tmp_null;

#line 5 "test_tmp.rh"

class TEST_TMP_CLASS {
              private :
                     int ins_cursor;
              public :
              TEST_TMP_CLASS () : ins_cursor (-1)
              {
              }
             int dbinsert (void);
             void dbclose (void);
              
              void prepare (void);
};
#endif

