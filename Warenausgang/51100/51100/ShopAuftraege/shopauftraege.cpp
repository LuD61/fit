//090712
//271010 und 140111 letzte_lieferung_holen 1 Lieferungen statt Bestellungen holen bei F8
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "dbclass.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_progcfg.h"
#include <commctrl.h>
#include "ls.h"
#include "ls_einzel.h"
#include "a_bas.h"
#include "mo_lsgen.h"
#include "mo_menu.h"
//#include "mo_kf.h"
#include "mo_arg.h"
#include "mo_meld.h"
#include "message.h"
#include "sys_par.h"
#include "shopaufk.h"
#include "tmp_bedarf.h"
#include "tables.h"
#include "mo_llgen.h"
#include "test_tmp.h"
//#include "ClientOrder.h"


//#include "ClientOrder.h"
//#include "Application.h"
//#include <vector>


        static char mdn_von [5];
        static char mdn_bis [5];
        static char fil_von [5];
        static char fil_bis [5];
        static char auf_von [9];
        static char auf_bis [9];
        static char kungr_von [9];
        static char kungr_bis [9];
        static char stat_von [3];
        static char stat_bis [3];
        static char dat_von [12];
        static char dat_bis [12];
        static char tou_von [10];
        static char ohne_kst [16];
        static char tou_bis [10];
        static char abt_von [6];
        static char abt_bis [6];
        static char extra_von [6];
        static char extra_bis [6];



	int Anzahl;

	char cmessage [120];
char drformat [20] = {"51190"};
char drformatGes [20] = {"51191"};

struct SHOPAUFP shopaufp, shopaufp_null;
struct KUN kun, kun_null;
LLGEN_CLASS Llgen;

static PROG_CFG ProgCfg ("shopauftraege");
static TMP_BEDARF_CLASS TmpBedarf;


void BereichsAuswahl (void);
void GetCfgValues (void);
static HBITMAP ArrDown;

class LS_EINZEL_CLASS ls_einzel_class;
static DB_CLASS DbClass;
SYS_PAR_CLASS sys_par_class;
extern MELDUNG Mess;

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
static int ProcessMessages(void);
void     InitFirstInstance(HANDLE);
BOOL     InitNewInstance(HANDLE, int);
HACCEL MenAccelerators = NULL;
HANDLE  hMainInst;
HWND   hMainWindow;
HWND   mamain1;
static HWND chwnd;
BOOL   NoMenue = FALSE;
static BOOL NoClose = FALSE;
static long tour_div = 100;
static HWND DlgWindow = NULL;
HWND ForegroundWindow;
HBITMAP SelBmp = NULL;
void     MoveButtons (void);
static PAINTSTRUCT aktpaint;

static ITEM iarrdown ("arrdown",  " < ",     "", 0);
static MENUE_CLASS menue_class;

extern HWND  ftasten;
extern HWND  ftasten2;
static   TEXTMETRIC tm;
static BOOL NewStyle = FALSE;
extern HWND hWndToolTip;
HWND hwndTB;
HWND hwndCombo1;
HWND hwndCombo2;

static char *Programm = "51100";

static int  Startsize = 0;
static char stat_defp_von [3] = {"1"};
static char stat_defp_bis [3] = {"3"};;
static char tou_def_von [10]  = {"0"};
static char tou_def_bis [10]  = {"9999"};
static char abt_def_von [6]   = {"0"};
static char abt_def_bis [6]   = {"9999"};
static char extra_def_von [6]   = {"0"};
static char extra_def_bis [6]   = {"2"};
static BOOL AbtRemoved = TRUE;
static BOOL DrkKunRemoved = TRUE;
static BOOL DrkExtraRemoved = FALSE;
static BOOL LuKunRemoved = TRUE;
static BOOL FixDat = FALSE;
static BOOL EnterKomm = FALSE;
short  StartMdn = 1;

static char InfoCaption [80] = {"\0"};

static COLORREF Colors[] = {BLACKCOL,
                            WHITECOL,
                            WHITECOL,
                            WHITECOL,
							BLACKCOL,
};

static COLORREF BkColors[] = {WHITECOL,
                            BLUECOL,
                            BLACKCOL,
                            GRAYCOL,
							LTGRAYCOL,
};

static COLORREF MessBkCol = DKYELLOWCOL; 
static COLORREF MessCol   = BLACKCOL; 

static int combo1pos = 0;
static int combo2pos = 0;
mfont lFont = {
               "Courier New", 
               120, 
               100, 
               0, 
               RGB (-1, 0, 0), 
               RGB (-1, 0, 0),
               0, 0};

static int m3Size = 0;




void GetForeground (void)
{
	ForegroundWindow = GetForegroundWindow ();
}

void SetForeground (void)
{
	SetForegroundWindow (ForegroundWindow);
}


void MoveRect (void)
/**
Fenster auf gesicherte Daten anpassen.
**/
{
	    char *etc;
		char buffer [512];
		RECT rect;
		FILE *fp;
		int x, y, cx, cy;
		int anz;

		etc = getenv ("BWSETC");
		if (etc == NULL) return;

		GetWindowRect (hMainWindow, &rect);
		x = rect.left;
		y = rect.top;
		cx = rect.right;
		cy = rect.bottom;
		sprintf (buffer, "%s\\%s.rct", etc,Programm);
		fp = fopen (buffer, "r");
		if (fp == NULL) return;
		while (fgets (buffer, 511, fp))
		{
			cr_weg (buffer);
			anz = wsplit (buffer, " ");
			if (anz < 2) continue;
			if (strcmp (wort[0], "left") == 0)
			{
				x = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "top") == 0)
			{
				y = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "right") == 0)
			{
				cx = atoi (wort[1]);
			}
			else if (strcmp (wort[0], "bottom") == 0)
			{
				cy = atoi (wort[1]);
			}
		}
		fclose (fp);
		MoveWindow (hMainWindow, x, y, cx, cy, TRUE);
}


static int lstok (void)
{
    syskey = KEY12;
//    mench = currentfield;
    break_enter ();
    return 0;
}

void tst_arg (char *arg)
{
          for (; *arg; arg += 1)
          {
                 switch (*arg)
                 {
                            case 'u' :
                                     strcpy (InfoCaption, arg + 1);
                                     return;
                 }
          }
          return;
}


static int testber (void)
/**
Abfrage in Query-Eingabe.
**/
{
	    int savecurrent;

		savecurrent = currentfield;
        switch (syskey)
        {
                case KEY5 :
                       break_enter ();
                       return 1;
                case KEY11 :
                case KEY12 :
                       syskey = KEY12;
                       break_enter ();
                       return 1;
				case KEY7 :
					   ChoisePrinter0 ();
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
					   return 1;
        }

		if (syskey == KEYCR)
		{
		        if (current_form->mask[currentfield].BuId == KEY12)
				{
					syskey = KEY12;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY5)
				{
					syskey = KEY5;
					break_enter ();
					return 1;
				}
		        else if (current_form->mask[currentfield].BuId == KEY7)
				{
					   syskey = KEY7; 
					   ChoisePrinter0 ();
 		               SetButtonTab (TRUE);
					   SetCurrentField (savecurrent);
					   SetCurrentFocus (savecurrent);
					   return 1;
				}
		}
        return 0;
}

static int lstcancel (void)
{

    syskey = KEY5;
    break_enter ();
    return 0;
}



static int testwork (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

//    mench = 0;
    break_enter ();
    return 0;
}

static int testshow (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

//    mench = 1;
    break_enter ();
    return 0;
}

static int testdel (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}


static int testprint (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}

static int testfree (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

    break_enter ();
    return 0;
}

static int testcancel (void)
{
    if (testkeys ()) return 0;

    if (syskey != KEYCR &&
        syskey != KEY12) return 0;

	syskey = KEY5;
    break_enter ();
    return 0;
}

        static ColButton LstChoise = {
                              "Auswahl", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
//                               WHITECOL,
//                               RGB (0,0,120),
                               BLACKCOL,
                               GRAYCOL,
                               -2};
   
        static ColButton cWork = {
                              "Bearbeiten", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cShow = {
                              "Anzeigen", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cDel = {
                              "L�schen", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cPrint = {
                              "Drucken", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};

        static ColButton cFree = {
                              "Freigeben", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               WHITECOL,
                               BLUECOL,
                               2};
		  static ColButton *cButtons [] = {&cWork, &cShow, &cDel, &cPrint, &cFree, NULL}; 



        static ITEM iLstChoise ("", (char *) &LstChoise,  "", 0);

        static ITEM iWork      ("", (char *) &cWork,      "", 0);
        static ITEM iShow      ("", (char *) &cShow,      "", 0);
        static ITEM iDel       ("", (char *) &cDel,       "", 0);
        static ITEM iPrint     ("", (char *) &cPrint,     "", 0);
        static ITEM iFree      ("", (char *) &cFree,      "", 0);

        static ITEM iWorkB      ("", (char *) "Bearbeiten",  "", 0);
        static ITEM iShowB      ("", (char *) "Anzeigen",    "", 0);
        static ITEM iDelB       ("", (char *) "L�schen",     "", 0);
        static ITEM iPrintB     ("", (char *) "Drucken",     "", 0);
        static ITEM iFreeB      ("", (char *) "Freigeben",   "", 0);

        static ITEM vOK     ("",  "    OK     ",  "", 0);
        static ITEM vCancel ("",  " Abbrechen ", "", 0);

        static field _fLstChoise[] = {
          &iLstChoise, 36, 0,  0, 0, 0, "", COLBUTTON, 0, 0, 0};

        static form fLstChoise = {1, 0, 0, _fLstChoise, 
                                  0, 0, 0, 0, NULL};    


        static field _clist [] = {
&iWork,     34, 2, 2, 1, 0, "", COLBUTTON,            0, testwork, 
                                                           KEY12 , 
&iShow,     34, 2, 4, 1, 0, "", COLBUTTON,            0, testshow, 
                                                          KEY12,
&iDel,      34, 2, 6, 1, 0, "", COLBUTTON,            0, testdel, 
                                                           KEY12,
&iPrint,    34, 2, 8, 1, 0, "", COLBUTTON,            0, testprint, 
                                                           KEY12,
&iFree,     34, 2,10, 1, 0, "", COLBUTTON,            0, testfree, 
                                                           KEY12,
&vCancel,  12, 0, 10, 12, 0, "",  BUTTON,               0, testcancel, 
                                                           KEY5, 
};


        static field _clistB [] = {
&iWorkB,     30, 0, 2, 3, 0, "", BUTTON,            0, testwork, 
                                                           KEY12 , 
&iShowB,     30, 0, 3, 3, 0, "", BUTTON,            0, testshow, 
                                                          KEY12,
&iDelB,      30, 0, 4, 3, 0, "", BUTTON,            0, testdel, 
                                                           KEY12,
&iPrintB,    30, 0, 5, 3, 0, "", BUTTON,            0, testprint, 
                                                           KEY12,
&iFreeB,     30, 0, 6, 3, 0, "", BUTTON,            0, testfree, 
                                                           KEY12,
&vCancel,   12, 0,  8,12, 0, "",  BUTTON,               0, testcancel, 
                                                           KEY5, 
};


static form clist = {6, 0, 0, _clistB, 0, 0, 0, 0, NULL}; 

void SetcButtons (COLORREF Col, COLORREF BkCol)
/**
Farbe der Button setzen.
**/
{
	    int i;

		for (i = 0; cButtons[i]; i ++)
		{
                 cButtons[i]->Color   = Col;
				 cButtons[i]->BkColor = BkCol;
		}
}

LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        HDC hdc;
//        char cfg_v [20];
		static form *SForm = NULL;
		static int SField = 0;


        switch(msg)
        {
              case WM_PAINT :
                      if (hWnd == mamain1)
                      {
                              hdc = BeginPaint (hWnd, &aktpaint);
//                              PrintLines (hdc);
//							  if (aufpListe.Ampel.GetAWnd () == NULL)
//							  {
//								aufpListe.PrintAmpel (hdc);
//							  }
                              EndPaint (hWnd, &aktpaint);
                      }
/*
					  else if (hWnd == aufpListe.Ampel.GetAWnd ())
					  {
							  aufpListe.PrintAmpel (hdc);
					  }
*/
                      else 
                      {
//                              aufpListe.OnPaint (hWnd, msg, wParam, lParam);
//                              lstxtlist.OnPaint (hWnd, msg, wParam, lParam);
                      }
                      break;
              case WM_SIZE :
                      if (hWnd == hMainWindow)
                      {
                              MoveFkt ();
                              Mess.MoveMess ();
//                              MoveMamain1 ();  
							  InvalidateRect (mamain1, NULL, TRUE);
                              hdc = GetDC (hMainWindow);
                              ReleaseDC (hMainWindow, hdc);
                      }
                      else if (hWnd == mamain1)
                      {
//						      InvalidateLines ();
                      }
                      else
                      {
                      }
                      break;

              case WM_SYSCOMMAND:

              case WM_NOTIFY :
                    {
                      LPNMHDR pnmh = (LPNMHDR) lParam;

                      if (pnmh->code == TTN_NEEDTEXT)
                      {
                           LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
                           if (QuickCpy (lpttt->szText, lpttt->hdr.idFrom)
                               == FALSE)
                           {
                                    QuickHwndCpy (lpttt);
                           }
                      }
                      break;
                    }
	
              case WM_DESTROY :
 				      OnDestroy (hWnd); 
                      if (hWnd == hMainWindow)
                      {
						     closedbase (); 
                             PostQuitMessage (0);
                             return 0;
                      }
                      break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == IDM_EXIT)
                    {
					       if (NoClose)
						   {
						      disp_mess (
									"Abbruch durch Windowmanager ist nicht m�glich", 
									2);
							  return 0;
						   }
                            if (abfragejn (hMainWindow, 
                                            "Bearbeitung abbrechen ?",
                                            "N") == 0)
                            {
                                  return 0;
                            }
                            rollbackwork ();
                            beginwork ();
                            commitwork ();
	                        SetForeground ();
                     		closedbase ();
                            ExitProcess (0);
                            break;
                    }

                    if (LOWORD (wParam) == KEYESC)
                    {
                            syskey = KEYESC;
                            SendKey (VK_ESCAPE);
                            break;
                    }
                    if (LOWORD (wParam) == KEY2)
                    {
                            syskey = KEY2;
                            SendKey (VK_F2);
                            break;
                    }
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYDOWN)
                    {
                            syskey = KEYDOWN;
                            SendKey (VK_DOWN);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYUP)
                    {
                            syskey = KEYUP;
                            SendKey (VK_UP);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYLEFT)
                    {
                            syskey = KEYLEFT;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYRIGHT)
                    {
                            syskey = KEYRIGHT;
                            SendKey (VK_RIGHT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYSTAB)
                    {
                            syskey = KEYSTAB;
                            SendKey (VK_LEFT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYTAB)
                    {
                            syskey = KEYTAB;
                            SendKey (VK_TAB);
                            break;
                    }
                    else if (LOWORD (wParam) == IDM_VINFO)
                    {
                            break;
                    }
			       else if (LOWORD (wParam) == IDM_WORK)
                   {
//                             EingabeMdnFil ();
                   }
			       else if (LOWORD (wParam) == IDM_PRINT)
                   {
                   }

                    else if (LOWORD (wParam) == IDM_INFO)
                    {
                                  syskey = KEY4;
                                  SendKey (VK_F4);
                    }
                    else if (LOWORD (wParam) == IDM_TEXT)
                    {
                    }
                    else if (LOWORD (wParam) == IDM_ETI)
                    {
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL StaticWProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void StartMenu (void)
/**
Liste auswaehlen und Listengenerator starten
**/
{
     form *sform;
     int  sfield;



     while (TRUE)
     {
        sform = current_form;
        sfield = currentfield;
        save_fkt (5);
        save_fkt (12);

        set_fkt (lstcancel, 5);
        set_fkt (lstok, 12);

        no_break_end ();
        SetButtonTab (TRUE);
//        DisablehWnd (hMainWindow);
        SetBorder (WS_POPUP | WS_DLGFRAME);

        SetAktivWindow (mamain1);
        chwnd = OpenWindowCh (12, 36, 10, 22, hMainInst);
		MoveZeWindow (hMainWindow, chwnd);
        DlgWindow = chwnd;

		/**
        display_form (chwnd, &fLstChoise, 0, 0);
        currentfield = 0;
        enter_form (chwnd, &clist, 0, 0);
		mench = currentfield;
        CloseControls (&fLstChoise);
        CloseControls (&clist);
		*/
        SetAktivWindow (mamain1);
        DestroyWindow (chwnd);
        chwnd = NULL;
        SetButtonTab (FALSE);
        restore_fkt (5);
        restore_fkt (12);
        if (sform)
        {
                current_form = sform;
                currentfield = sfield;
                SetCurrentFocus (currentfield);
        }

        if (syskey == KEY5) return;
        if (syskey == KEYESC) return;

        BereichsAuswahl ();
     }
}



BOOL DropLabel (form *Frm, char *Item, int LabelAnz)
{
         int lpos = GetItemPos (Frm, Item);

         if (lpos == -1)
         {
             return FALSE;
         }

         if (Frm->mask[lpos].attribut == REMOVED)
         {
             return FALSE;
         }

         Frm->mask[lpos].attribut = REMOVED;
         for (int i = lpos + 1; i < LabelAnz; i ++)
         {
             Frm->mask[i].pos[0] -= 2;
         }
         return TRUE;
}


BOOL DropText (form *Frm, char *Item, int TextAnz)
{
         int lpos = GetItemPos (Frm, Item);

         if (lpos == -1)
         {
             return FALSE;
         }

         if (Frm->mask[lpos].attribut == REMOVED)
         {
             return FALSE;
         }


         Frm->mask[lpos].attribut = REMOVED;
         Frm->mask[lpos + 1].attribut = REMOVED;
         for (int i = lpos + 2; i < TextAnz; i ++)
         {
             Frm->mask[i].pos[0] -= 2;
         }
         return TRUE;
}

void InitFirstInstance(HANDLE hInstance)
{
        
        WNDCLASS wc;
		HDC hdc;
		int col;

		col = 0;
		hdc = GetDC (NULL);
		col = GetDeviceCaps (hdc, BITSPIXEL);
		ReleaseDC (NULL, hdc);
        if (col < 16) ColBorder = FALSE;

		SetEnvFont ();

//		strcpy (musterfont.FontName, FontName);
//        musterfont.FontHeight = FontHeight;
//        musterfont.FontWidth  = FontWidth;       
//        musterfont.FontAttribute  = FontAttribute;       


		SetSWEnvFont ();

		strcpy (lFont.FontName, FontNameSW);
        lFont.FontHeight = FontHeightSW;
        lFont.FontWidth  = FontWidthSW;       
        lFont.FontAttribute  = FontAttributeSW;       



		if (hbrBackground == NULL)
		{
			hbrBackground = CreateSolidBrush (StdBackCol);
		}


        wc.style         =  CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, "FITICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
//        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
//        wc.hbrBackground =  CreateSolidBrush (StdBackCol);
        wc.hbrBackground =  hbrBackground;
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "hStdWindow";

        RegisterClass(&wc);

        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "ListMain";
        RegisterClass(&wc);


        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "hListWindow";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  StaticWProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.lpszClassName =  "StaticWhite";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  StaticWProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.hbrBackground =  GetStockObject (GRAY_BRUSH);
        wc.lpszClassName =  "StaticGray";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  WndProc;
        wc.hIcon         =  LoadIcon (hInstance, "NOICON");
        wc.lpszMenuName  =  NULL;
        wc.hbrBackground =  CreateSolidBrush (MessBkCol);
        wc.lpszClassName =  "StaticMess";
        RegisterClass(&wc);
        hMainInst = hInstance;
        return;
}


BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        HDC hdc;
        HFONT hFont, oldFont;
        char *Caption;
        SIZE size;

        hdc = GetDC (NULL);
        hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
        oldFont = SelectObject (hdc,hFont);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (0, hdc);
//        aufpListe.SetTextMetric (&tm);
        if (InfoCaption[0])
        {
            Caption = InfoCaption;
        }
        else
        {
            Caption = "Vorbereitungsliste zusammenstell aus Shopauftr�gen und Auftr�gen";
        }

        SetBorder (WS_THICKFRAME | WS_CAPTION | 
                   WS_SYSMENU | WS_MINIMIZEBOX |
                   WS_MAXIMIZEBOX);

        hMainWindow = OpenWindowChC (26, 80, 2, 0, hInstance, 
                      Caption);
        return 0; 
}


static int ProcessMessages(void)
{
        MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
             if (msg.message == WM_KEYDOWN)
             {
                       switch (msg.wParam)
                       {
                              case VK_F5 :
                                  syskey = KEY5;
                                  PostQuitMessage (0);
                                  continue; ;
                              case VK_RETURN :
                                  syskey = KEYCR;
//                                  EingabeMdnFil ();
                                  continue; ;
                       }
             }
             TranslateMessage(&msg);
             DispatchMessage(&msg);
        }
//  	    CloseOfPo ();
		closedbase ();
        return msg.wParam;
}



BOOL  Erstelle_tmp_bedarf (char *where, BOOL isAuftrag)
/**
Mehrere Auftraege uebertragen.
**/
{
	int cursor;
	char sqls [1028];
//	MSG msg;
	extern short sql_mode;
	short sqlm_save;
	char datum [11];
	char zeit [11];
//	double a_grund_von_buffet = (double) 151;
//	double a_grund_bis_buffet = (double) 155;
	short dextra;
	double da;
	static char *LogName = NULL;
	FILE *fp;
	BOOL buffet_vorhanden = FALSE;

//	CreateLogfile ();

//	KompParams ();
    sysdate (datum);
	systime (zeit);
	dextra = 0;
 //  WriteLogfile ("Bereichs�bergabe %s %s\n", datum, zeit);
//	WriteLogfile ("%s\n", where);

    sqlm_save = sql_mode;
	sql_mode = 1;

	double dauf_me;
	int iposi_ext;
	if (isAuftrag == FALSE)
	{
	         sprintf (sqls, "select shopaufk.kun, shopaufk.orders_id,shopaufp.orders_id,kun.tou,kun.kun_gr2,shopaufp.lieferdat, shopaufp.a,shopaufp.stk, shopaufp.a_beilage from shopaufk,shopaufp,kun %s", where);
             DbClass.sqlout ((long *) &shopaufk.kun, 2, 0); //090712
             DbClass.sqlout ((long *) &shopaufk.orders_id, 2, 0); 
             DbClass.sqlout ((long *) &shopaufp.orders_id, 2, 0); 
             DbClass.sqlout ((long *) &kun.tou, 2, 0); 
             DbClass.sqlout ((long *) &kun.kun_gr2, 2, 0);  
             DbClass.sqlout ((long *) &shopaufp.lieferdat, 2, 0); 
             DbClass.sqlout ((double *) &shopaufp.a, 3, 0); 
             DbClass.sqlout ((long *) &shopaufp.stk, 2, 0); 
             DbClass.sqlout ((double *) &shopaufp.a_beilage, 3, 0); 
//090712 			 strcat (sqls, " and shopaufk.orders_id = shopaufp.orders_id and shopaufk.kun = kun.kun and kun.mdn = 1 and kun.fil = 0 order by shopaufk.orders_id ");
			 //090712 order by ge�ndert, wg. Test auf Salatbuffet jetzt nicht �ber orders_id, sondern �ber lieferdat und kun 
 			 strcat (sqls, " and shopaufk.orders_id = shopaufp.orders_id and shopaufk.kun = kun.kun and kun.mdn = 1 and kun.fil = 0 order by shopaufp.lieferdat, shopaufk.kun, shopaufk.orders_id ");    
	}
	else
	{
	         sprintf (sqls, "select aufk.kun,aufk.mdn, aufk.fil, aufk.auf, aufk.auf, kun.tou, kun.kun_gr2, aufk.lieferdat, aufp.a, aufp.auf_me, aufp.posi_ext from aufk,aufp,kun %s", where);
             DbClass.sqlout ((long *) &shopaufk.kun, 2, 0); //090712
             DbClass.sqlout ((short *) &aufk.mdn, 1, 0); 
             DbClass.sqlout ((short *) &aufk.fil, 1, 0); 
             DbClass.sqlout ((long *) &shopaufk.orders_id, 2, 0); 
             DbClass.sqlout ((long *) &shopaufp.orders_id, 2, 0); 
             DbClass.sqlout ((long *) &kun.tou, 2, 0); 
             DbClass.sqlout ((long *) &kun.kun_gr2, 2, 0);  
             DbClass.sqlout ((long *) &shopaufp.lieferdat, 2, 0); 
             DbClass.sqlout ((double *) &shopaufp.a, 3, 0); 
             DbClass.sqlout ((double *) &dauf_me, 3, 0); 
             DbClass.sqlout ((long *) &iposi_ext, 2, 0); 
//090712			 strcat (sqls, " and aufk.auf = aufp.auf and aufk.mdn = aufp.mdn and aufk.fil = aufp.fil  and aufk.mdn = 1 and aufk.fil = 0 and aufk.kun = kun.kun and kun.mdn = 1 and kun.fil = 0 order by aufk.auf ");
			 //090712 order by ge�ndert, wg. Test auf Salatbuffet jetzt nicht �ber orders_id, sondern �ber lieferdat und kun 
			 strcat (sqls, " and aufk.auf = aufp.auf and aufk.mdn = aufp.mdn and aufk.fil = aufp.fil  and aufk.auf > 0 and aufk.mdn = 1 and aufk.fil = 0 and aufk.kun = kun.kun and kun.mdn = 1 and kun.fil = 0 order by aufk.lieferdat, aufk.kun, aufk.auf ");
	}
	         cursor = DbClass.sqlcursor (sqls);

			 if (cursor < 0) 
			 {
				  char *tmp;
				  tmp = getenv ("TMPPATH");
				  if (LogName) delete LogName;
				  LogName = new char [512];
				  if (LogName == NULL) return FALSE ;
				  sprintf (LogName, "%s\\shopauftraege.error", tmp);

				  fp = fopen (LogName, "w");
					if (fp == NULL)
					{
						delete LogName;
						LogName = NULL;
					}
					fprintf (fp, sqls );
					fclose (fp);
				 sqlstatus = cursor;
				 return FALSE;
			 }
			 tmp_bedarf.posi = 0;


			 int sav_orders_id = 0;
			 long sav_lieferdat = 0;
			 long sav_kun = 0;
	         while (DbClass.sqlfetch (cursor) == 0)
			 {
				Anzahl ++;
				sprintf (cmessage,"Die Auftr�ge werden eingelesen ... %ld",Anzahl);
				Mess.CloseWaitWindow ();
				Mess.WaitWindow (cmessage);
			
				if (isAuftrag == TRUE) shopaufp.stk = (int) dauf_me;
				if (isAuftrag == TRUE) shopaufp.a_beilage = (double) iposi_ext;
				tmp_bedarf.mdn = 1;
				tmp_bedarf.lieferdat = shopaufp.lieferdat;
				tmp_bedarf.kun_gr2 = kun.kun_gr2;
				tmp_bedarf.a_lsp = shopaufp.a;
				tmp_bedarf.a = shopaufp.a;
				tmp_bedarf.sort = 0;
				lese_a_bas (shopaufp.a);
				tmp_bedarf.teil_smt = _a_bas.teil_smt;
				strcpy (tmp_bedarf.smt, _a_bas.smt) ;
				if (TmpBedarf.dbreadfirst() == 0) tmp_bedarf.auf_me += shopaufp.stk;
				else tmp_bedarf.auf_me = shopaufp.stk;
				tmp_bedarf.me_einh = 1;
				strcpy (tmp_bedarf.auf_me_bz,"St�ck");
				tmp_bedarf.posi ++;
				if (_a_bas.a_grund >= ls_einzel_class.a_grund_von_buffet && _a_bas.a_grund <= ls_einzel_class.a_grund_bis_buffet)
				{
					dextra = 1;
				}
				else dextra = 0;

				//Test ob ein Salatbuffet im Auftrag mit drin ist, dann wird bei den restlichen Gerichten keine Salatbeilage mit dazukommissioniert
				int dsqlstatus ;
//090712				if (shopaufk.orders_id != sav_orders_id || shopaufp.lieferdat != sav_lieferdat) 
				if (shopaufk.kun != sav_kun || shopaufp.lieferdat != sav_lieferdat)  //090712
				{
					if (isAuftrag == FALSE)
					{
			            DbClass.sqlout ((double *) &da, 3, 0); 
//090712						DbClass.sqlin ((long *) &shopaufp.orders_id, 2, 0); 
						DbClass.sqlin ((long *) &shopaufk.kun, 2, 0);  //090712
						DbClass.sqlin ((long *) &shopaufp.lieferdat, 2, 0); 
						DbClass.sqlin ((double *) &ls_einzel_class.a_grund_von_buffet, 3, 0); 
						DbClass.sqlin ((double *) &ls_einzel_class.a_grund_bis_buffet, 3, 0); 
						dsqlstatus = DbClass.sqlcomm ("select a from shopaufp where kun = ? and lieferdat = ? and a in (select a from a_bas where a_grund >= ? and a_grund <= ? ) ");
					}
					else
					{
			             DbClass.sqlin ((short *) &aufk.mdn, 1, 0); 
						DbClass.sqlin ((short *) &aufk.fil, 1, 0); 
						DbClass.sqlin ((long *) &shopaufp.orders_id, 2, 0); 
						DbClass.sqlin ((double *) &ls_einzel_class.a_grund_von_buffet, 3, 0); 
						DbClass.sqlin ((double *) &ls_einzel_class.a_grund_bis_buffet, 3, 0); 
			            DbClass.sqlout ((double *) &da, 3, 0); 
					    dsqlstatus = DbClass.sqlcomm ("select a from aufp where mdn = ? and fil = ? and auf = ? and a in (select a from a_bas where a_grund >= ? and a_grund <= ? ) ");

					}
					 if (dsqlstatus == 0)
					 {
						 buffet_vorhanden = TRUE;
					 }
					 else buffet_vorhanden = FALSE;


					 sav_orders_id = shopaufk.orders_id;
					 sav_lieferdat = shopaufp.lieferdat;
					 sav_kun = shopaufk.kun; //090712
				}

				TmpBedarf.dbupdate();
				int sav_kun_gr2 = tmp_bedarf.kun_gr2;
				int sav_teil_smt = tmp_bedarf.teil_smt;
				tmp_bedarf.kun_gr2 = 0;
				tmp_bedarf.teil_smt = 0; 

				if (TmpBedarf.dbreadfirst() == 0) tmp_bedarf.auf_me += shopaufp.stk;
				else tmp_bedarf.auf_me = shopaufp.stk;
				TmpBedarf.dbupdate();
				tmp_bedarf.kun_gr2 = sav_kun_gr2;
				tmp_bedarf.teil_smt = sav_teil_smt;

				test_tmp.kun =  shopaufk.kun;
				test_tmp.auf = shopaufp.orders_id;
				test_tmp.datum = dasc_to_long (datum);
				strcpy (test_tmp.zeit,zeit);
				TmpBedarf.RezAufloesung (0,shopaufp.a, shopaufp.a, shopaufp.stk,dextra,tmp_bedarf.teil_smt, tmp_bedarf.smt, buffet_vorhanden, isAuftrag, shopaufp.orders_id, kun.kun_gr2);


			 }
			 return TRUE;
//	Mess.CloseWaitWindow ();
}

void LlListe (void)
/**
Kommissionierliste drucken.
**/
{
    char valfrom [20];
    char valto [20];

    if (Llgen.LeseFormat (drformat) == FALSE)
	{
           return;
	}



    sprintf (valfrom, "%hd", 1);
    sprintf (valto, "%hd", 1);
    Llgen.FillFormFeld ("tmp_bedarf.mdn", valfrom, valto);


    sprintf (valfrom, "%s", dat_von);
    sprintf (valto, "%s", dat_bis);
    Llgen.FillFormFeld ("tmp_bedarf.lieferdat", valfrom, valto);

    sprintf (valfrom, "%ld", kungr_von);
    sprintf (valto, "%ld", kungr_bis);
    Llgen.FillFormFeld ("tmp_bedarf.kun_gr2", valfrom, valto);

    sprintf (valfrom, "%ld", tou_von);
    sprintf (valto, "%ld", tou_bis);
    Llgen.FillFormFeld ("kun.tour", valfrom, valto);

    Llgen.StartPrint ();

//	CreateLogfile ();
//	WriteLogfile ("Auftrag %ld wurde gedruckt\n", aufk.auf);
//	CloseLogfile ();
    Mess.Message ("Die Liste wurde gedruckt");
}
void LlGesListe (void)
/**
Gesamt-Bedarfsliste drucken.
**/
{
    char valfrom [20];
    char valto [20];

    if (Llgen.LeseFormat (drformatGes) == FALSE)
	{
           return;
	}



    sprintf (valfrom, "%hd", 1);
    sprintf (valto, "%hd", 1);
    Llgen.FillFormFeld ("tmp_bedarf.mdn", valfrom, valto);


    sprintf (valfrom, "%s", dat_von);
    sprintf (valto, "%s", dat_bis);
    Llgen.FillFormFeld ("tmp_bedarf.lieferdat", valfrom, valto);

    sprintf (valfrom, "%ld", kungr_von);
    sprintf (valto, "%ld", kungr_bis);
    Llgen.FillFormFeld ("tmp_bedarf.kun_gr2", valfrom, valto);

    sprintf (valfrom, "%ld", tou_von);
    sprintf (valto, "%ld", tou_bis);
    Llgen.FillFormFeld ("kun.tour", valfrom, valto);

    Llgen.StartPrint ();

    Mess.Message ("Die Liste wurde gedruckt");
}


void BereichsAuswahl (void)
/**
Bereich fuer Druck oder Lieferscheinuebergabe eingeben.
**/
{
        HANDLE hMainInst;
		HWND hWnd;
		char where_shop [1028];
		char where_auf [1028];

        static int DrkRows = 15; 
        static int LuRows = 19; 

        static ITEM iOK ("", "     OK     ", "", 0);
        static ITEM iCA ("", "  Abbrechen ", "", 0);
        static ITEM iDW ("", "Druckerwahl ",  "", 0);


		static ITEM ivon     ("", "von",             "", 0);
		static ITEM ibis     ("", "bis",             "", 0);
		static ITEM iMdn     ("mdnlabel", "Mandant",         "", 0);
		static ITEM iFil     ("fillabel", "Filiale",         "", 0);
		static ITEM iAuf     ("auflabel", "Auftrag",         "", 0);
		static ITEM iKunGr     ("kungrlabel", "Kundengruppe2",           "", 0);
		static ITEM iKst     ("kstlabel", "Ohne Kostenstelle",           "", 0);
		static ITEM iStat    ("statlabel", "Auftrags-Status", "", 0);
		static ITEM iDat     ("datlabel", "Lieferdatum",     "", 0);
		static ITEM iTour    ("toulabel", "Tour",            "", 0);
		static ITEM iAbt     ("abtlabel", "Abteilung", "", 0);
		static ITEM iExtra     ("extralabel", "Extra-Liste",           "", 0);

        static ITEM imdn_von ("mdn_von", mdn_von,  "", 0);
        static ITEM imdn_bis ("mdn_bis", mdn_bis,  "", 0);
        static ITEM ifil_von ("fil_von", fil_von,  "", 0);
        static ITEM ifil_bis ("fil_bis", fil_bis,  "", 0);
        static ITEM iauf_von ("auf_von", auf_von,  "", 0);
        static ITEM iauf_bis ("auf_bis", auf_bis,  "", 0);
        static ITEM ikungr_von ("kungr_von", kungr_von,  "", 0);
        static ITEM ikungr_bis ("kungr_bis", kungr_bis,  "", 0);
        static ITEM iohne_kst ("ohne_kst", ohne_kst,  "", 0);
        static ITEM istat_von ("stat_von", stat_von,  "", 0);
        static ITEM istat_bis ("stat_bis", stat_bis,  "", 0);
        static ITEM idat_von ("dat_von", dat_von,  "", 0);
        static ITEM idat_bis ("dat_bis", dat_bis,  "", 0);
        static ITEM itou_von ("tou_von", tou_von,  "", 0);
        static ITEM itou_bis ("tou_bis", tou_bis,  "", 0);
        static ITEM iabt_von ("abt_von", abt_von,  "", 0);
        static ITEM iabt_bis ("abt_bis", abt_bis,  "", 0);
        static ITEM iextra_von ("extra_von", extra_von,  "", 0);
        static ITEM iextra_bis ("extra_bis", extra_bis,  "", 0);


        static int lulabelanz = 9;
        
        static field _berformdr[] = {
           &ivon,     0, 0, 1,18,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &ibis,     0, 0, 1,40,0, "",    DISPLAYONLY, 0, 0 ,0, 
//           &iAuf,     0, 0, 5, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iKunGr,     0, 0, 5, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iDat,     0, 0, 3, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iTour,    0, 0, 7, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
           &iKst,    0, 0, 9, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 
//           &iExtra,   0, 0,11, 1,0, "",    DISPLAYONLY, 0, 0 ,0, 

           &idat_von, 11, 0, 3,18, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 
           &idat_bis, 11, 0, 3,40, 0, "dd.mm.yyyy",    EDIT, 0 ,testber,0, 
//           &iauf_von, 9, 0, 5,18, 0, "%8d",    EDIT, 0 ,testber,0, 
//           &iauf_bis, 9, 0, 5,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ikungr_von, 9, 0, 5,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &ikungr_bis, 9, 0, 5,40, 0, "%8d",    EDIT, 0 ,testber,0, 
           &itou_von,  9, 0, 7,18, 0, "%8d",    EDIT, 0 ,testber,0, 
           &itou_bis,  9, 0, 7,40, 0, "%8d",    EDIT, 0 ,testber,0, 
//           &iextra_von,  3, 0,11,18, 0, "%1d",    EDIT, 0 ,testber,0, 
//           &iextra_bis,  3, 0,11,40, 0, "%1d",    EDIT, 0 ,testber,0, 
           &iohne_kst,  15, 0, 9,26, 0, "",    EDIT, 0 ,testber,0, 

           &iOK,     15, 0,11, 6, 0, "", BUTTON, 0, testber,KEY12,
           &iCA,     15, 0,11,23, 0, "", BUTTON, 0, testber,KEY5,
           &iDW,     15, 0,11,40, 0, "", BUTTON, 0, testber,KEY7,
		};

        static form berformdr = {16, 0, 0, _berformdr, 
			                    0, 0, 0, 0, NULL};


        static int drklabelanz = 8;


        HWND berwin;
		int savefield;
		form *savecurrent;

        NoClose = TRUE;
        hWnd = AktivWindow;
        hMainInst = (HANDLE) GetWindowLong (hWnd, GWL_HINSTANCE);	
        save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (9);
		save_fkt (11);
		save_fkt (12);

        set_fkt (NULL, 6);
        set_fkt (NULL, 7);
        set_fkt (NULL, 9);

        set_fkt (testber, 5);
        set_fkt (testber, 11);
        set_fkt (testber, 12);

		sysdate (dat_von);
		sysdate (dat_bis);
		strcpy (mdn_von, "1");
		strcpy (mdn_bis, "9999");
		strcpy (fil_von, "0");
		strcpy (fil_bis, "9999");
		strcpy (auf_von, "1");
		strcpy (auf_bis, "99999999");
		strcpy (kungr_von, "1");
		strcpy (kungr_bis, "9999");
		strcpy (ohne_kst, "");
         strcpy (stat_von, stat_defp_von);
         strcpy (stat_bis, stat_defp_bis);
		strcpy (tou_von, tou_def_von);
		strcpy (tou_bis, tou_def_bis);
		strcpy (abt_von, abt_def_von);
		strcpy (abt_bis, abt_def_bis);
		strcpy (extra_von, extra_def_von);
		strcpy (extra_bis, extra_def_bis);

		savefield = currentfield;
		savecurrent = current_form;
        SetAktivWindow (hWnd);
		SetButtonTab (TRUE);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_CAPTION);


		    
         berwin = OpenWindowChC (DrkRows, 62, 6, 10, hMainInst,
                           "Druck Vorbereitungsliste");
         MoveZeWindow (hMainWindow, berwin);
	          enter_form (berwin, &berformdr
			   , 0, 0);


		SetButtonTab (FALSE);
        CloseControls (&berformdr);
        DestroyWindow (berwin);
        SetAktivWindow (hWnd);
		current_form = savecurrent;
		currentfield = savefield;
        restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (9);
		restore_fkt (11);
		restore_fkt (12);

		if (syskey == KEY5) 
		{
            NoClose = FALSE;
			return;
		}

				     

		if (strlen (clipped(ohne_kst)) == 0)  strcpy (ohne_kst, "-99");
		sprintf (where_shop, "where shopaufp.auf = 0 and shopaufp.lieferdat between \"%s\" and \"%s\" "
//		sprintf (where_shop, "where shopaufp.lieferdat between \"%s\" and \"%s\" "
						"and   kun.kun_gr2 between %hd and %hd "
						"and   kun.tou  between %ld and %ld "
						"and   kun.kst  not in (%s)",
						clipped (dat_von), clipped (dat_bis),
						atol (kungr_von), atol (kungr_bis),
						atol (tou_von) , atol (tou_bis), ohne_kst);

		sprintf (where_auf, "where aufk.mdn = 1 and aufk.fil = 0 and aufk.auf_stat < 6 and aufk.lieferdat between \"%s\" and \"%s\" "   //auf_stat < 6 ,  wg. Kumulierung, 21.11.2014
						"and   kun.kun_gr2 between %hd and %hd "
						"and   kun.tou  between %ld and %ld "
						"and   kun.kst  not in (%s)",
						clipped (dat_von), clipped (dat_bis),
						atol (kungr_von), atol (kungr_bis),
						atol (tou_von) , atol (tou_bis), ohne_kst);

		EnableWindow (hMainWindow, FALSE);

         beginwork ();

         DbClass.sqlin ((char *) dat_von, 0, 11);
         DbClass.sqlin ((char *) dat_bis, 0, 11);
	     DbClass.sqlcomm ("delete from tmp_bedarf where lieferdat between ? and ?");
		
		 Anzahl = 0;
		sprintf (cmessage,"Die Auftr�ge werden eingelesen                       ");
		Mess.WaitWindow (cmessage);

		Erstelle_tmp_bedarf (where_auf,1);
			 if (sqlstatus < 0)
			 {
				Mess.CloseWaitWindow ();
				sprintf (cmessage,"Fehler in der Abfrage1 (sqlstatus=%ld)",sqlstatus);
				Mess.WaitWindow (cmessage);
				commitwork ();
				Sleep (4000);
				return;
			 }
		Erstelle_tmp_bedarf (where_shop,0); 
			 if (sqlstatus < 0)
			 {
				Mess.CloseWaitWindow ();
				sprintf (cmessage,"Fehler in der Abfrage0 (sqlstatus=%ld)",sqlstatus);
				Mess.WaitWindow (cmessage);
				commitwork ();
				Sleep (4000);
				return;
			 }
	 if (Anzahl == 0)
	 {
		Mess.CloseWaitWindow ();
		sprintf (cmessage,"Keine Auftr�ge gefunden (sqlstatus=%ld)",sqlstatus);
		Mess.WaitWindow (cmessage);
		commitwork ();
		Sleep (4000);
		return;
	 }
		commitwork ();

		Mess.CloseWaitWindow ();
		sprintf (cmessage,"Liste wird aufgerufen ...");
		Mess.WaitWindow (cmessage);
        if (abfragejn (hMainWindow, "Bedarfslisten drucken ?", "N") == TRUE) LlListe ();
		sprintf (cmessage,"Liste wird aufgerufen ...");
		Mess.WaitWindow (cmessage);
        if (abfragejn (hMainWindow, "Gesamtbedarf drucken ?", "N") == TRUE) LlGesListe ();

		current_form = savecurrent;
		currentfield = savefield;
        SetCurrentFocus (currentfield);


        NoClose = FALSE;
		EnableWindow (hMainWindow, TRUE);
        SetWindowPos (hMainWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
        SetForegroundWindow (hMainWindow);
}


int CalcMinusPos (int cy)
{
        TEXTMETRIC tm;
        SIZE size;
        HDC hdc;


        hdc = GetDC (ftasten);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (ftasten, hdc);
        cy -= 3 * tm.tmHeight;
        return cy;
}

int CalcPlusPos (int y)
{
        TEXTMETRIC tm;
        SIZE size;
        HDC hdc;


        hdc = GetDC (ftasten);
        GetTextMetrics (hdc, &tm);
        GetTextExtentPoint32 (hdc, "X", 1, &size);
        tm.tmAveCharWidth = size.cx;
        ReleaseDC (ftasten, hdc);
        y += 1 * tm.tmHeight + 5;
        return y;
}

void MoveMamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;

     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;
     cy = frect.top - mrect.top - y - 50;
     if (ftasten2 == NULL)
     {
         y  = CalcPlusPos (y);
         cy = CalcMinusPos (cy);
     }
     MoveWindow (mamain1, x, y, cx, cy, TRUE);
     MoveFktBitmap ();
}


void Createmamain1 (void)
/**
Hauptarbeitsfenster oeffnen.
**/
{
     RECT trect;
     RECT frect;
     RECT mrect;
     RECT crect;
     int x,y, cx, cy;


     GetWindowRect (hMainWindow, &mrect);
     GetWindowRect (ftasten, &frect);

     GetClientRect (hMainWindow, &crect);
     GetClientRect (hwndTB, &trect);
     x = 5;
     cx = crect.right - x - 5;

     y = trect.bottom + 5;


     cy = frect.top - mrect.top - y - 50;

     if (ftasten2 == NULL)
     {
         y  = CalcPlusPos (y);
         cy = CalcMinusPos (cy);
     }

     mamain1 = CreateWindowEx (
                              WS_EX_CLIENTEDGE, 
                              "hStdWindow",
                              InfoCaption,
                              WS_CHILD |  
                              WS_VISIBLE,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
}

void DeleteRefreshStnd ()
{
//	 HMENU SubMenu = GetSubMenu (hMenu, 2);
//	 DeleteMenu (SubMenu,  2, MF_BYPOSITION);
//	 DeleteMenu (SubMenu,  IDM_REFRESH_STND, MF_BYCOMMAND);
}

int    PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
       int anz; 
       int i;
       char argtab[20][80];
       char *varargs[20];
//	   char *mv;
	   char *tr;
       char *cmdl; 
    
//	   CApplication::GetInstance ()->SetAppInstance (hInstance);
//	   OleInitialize (0);
       anz = wsplit (lpszCmdLine, " ");
	   for (i = 0; i < anz; i ++)
	   {
		   if (memcmp (wort[i], "nomenu=", 7) == 0)
		   {
			   if (memcmp (&wort[i][7], "true", 4) == 0)
			   {
				   NoMenue = TRUE;
			   }

			   else if (memcmp (&wort[i][7], "false", 5) == 0)
			   {
				   NoMenue = FALSE;
			   }
		   }
		   else if (memcmp (wort[i], "mdn=", 4) == 0)
		   {
			   StartMdn = atoi (&wort[i][4]);
		   }
	   }


       cmdl = lpszCmdLine;


       MenAccelerators = LoadAccelerators (hInstance, "MENACCELERATORS");
	   GetForeground ();   
	   SetcButtons (BLACKCOL, LTGRAYCOL);

	   ColBorder = TRUE;
       tr = getenv_default ("COLBORDER");
	   if (tr)
	   {
			ColBorder = max(0, min (1, atoi (tr)));
	   }
	   ArrDown = LoadBitmap (hInstance, "ARRDOWN");
	   iarrdown.SetFeldPtr ((char *) &ArrDown);
       SelBmp = BMAP::LoadBitmap (hInstance, "SEL",   "MSK",  GetSysColor (COLOR_3DFACE));
//       SelBmp = BMAP::LoadBitmap (hInstance, "SEL",   "MSK",  LTGRAYCOL);
       opendbase ("bws");
       menue_class.SetSysBen (cmdl);
//       menue_class.SetSysBenPers ();
//       SetListLine ("HT");
	   GetCfgValues ();


       memcpy (&sys_par, &sys_par_null,  sizeof (struct SYS_PAR));
	   //140111 A
       strcpy (sys_par.sys_par_nam,"tourlang");
       if (sys_par_class.dbreadfirst () == 0)
       {
             if (atoi (sys_par.sys_par_wrt)) 
             {
                             switch (atoi (sys_par.sys_par_wrt))
							 {
							        case 4 :
                                           tour_div = (long) 100;
										   break;
									case 5 :
                                           tour_div = (long) 1000;
										   break;
							 }
             }
       }

       anz = wsplit (lpszCmdLine, " ");
       for (i = 0; i < anz; i ++)
       {
            strcpy (argtab[i], wort[i]);
            varargs[i] = (char *) argtab[i];
       }
       argtst (&anz, varargs, tst_arg);

       SetStdProc (WndProc);
       InitFirstInstance (hInstance);
       InitNewInstance (hInstance, nCmdShow);
        


       if (NewStyle)
       {
//             AddFktButton (MusterButton);
//             AddFktButton (LsButton);
  //           AddFktButton (NachDrButton);
             ftasten = OpenFktEx (hInstance);
             ITEM::SetHelpName ("51400.cmd");
             CreateFktBitmap (hInstance, hMainWindow, hwndTB);
//             SetFktMenue (TestMenue);
       }
       else
       {
             ftasten = OpenFktM (hInstance);
       }
       Mess.OpenMessage ();
       Createmamain1 ();
//       MoveMamain ();
	   if (Startsize == 1)
	   {
	               ShowWindow (hMainWindow, SW_SHOWMAXIMIZED);
	   }
	   else if (Startsize == 2)
	   {
	               MoveRect ();
	   }


       SetEnvFont ();
//	   GetStdFont (&BuFont);
//       lstxtlist.SetListFont (&lFont);
//       aufpListe.SetListFont (&lFont);

//       aufpListe.Ampel.SetHInstance (hMainInst); 
//       aufpListe.Ampel.SetHWnd (mamain1);
/**
	   int pos = GetItemPos (&aufform, "tou_bz");
	   pos = -1;
       if (pos != -1)
	   {
			int tx = aufform.mask[pos].pos[1] + aufform.mask[pos].length;
			int ty = aufform.mask[pos].pos[0];
			aufpListe.Ampel.SetAbsPos (mamain1, tx, ty);
	   }
	   else
	   {
			int sx = GetSystemMetrics (SM_CXFULLSCREEN);
			if (sx > 1400 && x_Ampel <= 650)
			{
				x_Ampel = 850;
			}
			aufpListe.Ampel.SetP (x_Ampel, y_Ampel);
	   }
	   aufpListe.Ampel.SetHbrBackground (hbrBackground);
	   aufpListe.Ampel.SetOwnWindow ();
	   aufpListe.Ampel.SetPlay (TRUE);
	   aufpListe.Ampel.Load ();

       telefon1     = LoadIcon (hMainInst, "tele1");
       fax          = LoadIcon (hMainInst, "fax");



       btelefon       = LoadBitmap (hInstance, "btele");
       btelefoni      = LoadBitmap (hInstance, "btelei");
       CTelefon.bmp   = btelefoni;


       display_form (mamain1, &buform, 0, 0);
       MoveButtons ();
       CreateQuikInfos ();


       CheckMenuItem (hMenu, IDM_PAGEVIEW, MF_UNCHECKED); 
       ToolBar_PressButton(hwndTB, IDM_PAGE, FALSE);
       ToolBar_PressButton(hwndTB, IDM_LIST, TRUE);

//	    aufpListe.NoteIcon = LoadIcon (hInstance, "NOTESICON");
	    aufpListe.NoteIcon = LoadBitmap (hInstance, "NOTEICON");
        aufpListe.SethMainWindow (mamain1);
//       EingabeMdnFil ();
*****/
  
       StartMenu ();

       closedbase ();
       return 0;
}


void GetCfgValues (void)
/**
Werte aus 51100.cfg holen.
**/
{
	   static BOOL cfgOK = FALSE;
  //     char cfg_v [512];

	   if (cfgOK) return;

	   cfgOK = TRUE;
/**
       if (ProgCfg.GetCfgValue ("Startsize", cfg_v) == TRUE)
       {
                    Startsize = atoi (cfg_v);
	   }
**/
}
