#ifndef _SHOPAUFK_DEF
#define _SHOPAUFK_DEF

#include "dbclass.h"

struct SHOPAUFK {
   long      orders_id;
   long      kun;
   char      comments[256];
   long      lieferdat;
   long      auf;
   short     gueltig;
   short     stat;
};
extern struct SHOPAUFK shopaufk, shopaufk_null;

#line 7 "shopaufk.rh"

class SHOPAUFK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               SHOPAUFK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
