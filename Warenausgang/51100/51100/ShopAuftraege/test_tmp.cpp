#include "mo_curso.h"
#include "test_tmp.h"

struct TEST_TMP test_tmp, test_tmp_null;

void TEST_TMP_CLASS::prepare (void)
{
    ins_quest ((char *) &test_tmp.mdn,1,0);
    ins_quest ((char *) &test_tmp.lieferdat,2,0);
    ins_quest ((char *) &test_tmp.kun_gr2,1,0);
    ins_quest ((char *) &test_tmp.a_lsp,3,0);
    ins_quest ((char *) &test_tmp.a,3,0);
    ins_quest ((char *) &test_tmp.posi,2,0);
    ins_quest ((char *) &test_tmp.auf_me,3,0);
    ins_quest ((char *) &test_tmp.me_einh,1,0);
    ins_quest ((char *) test_tmp.auf_me_bz,0,7);
    ins_quest ((char *) &test_tmp.teil_smt,1,0);
    ins_quest ((char *) &test_tmp.prdk_typ,1,0);
    ins_quest ((char *) &test_tmp.extra,1,0);
    ins_quest ((char *) &test_tmp.sort,1,0);
    ins_quest ((char *) &test_tmp.kun,2,0);
    ins_quest ((char *) &test_tmp.auf,2,0);
    ins_quest ((char *) &test_tmp.datum,2,0);
    ins_quest ((char *) test_tmp.zeit,0,12);
            ins_cursor = prepare_sql ("insert into test_tmp ("
"mdn,  lieferdat,  kun_gr2,  a_lsp,  a,  posi,  auf_me,  me_einh,  auf_me_bz,  teil_smt,  "
"prdk_typ,  extra,  sort,  kun,  auf,  datum,  zeit) "

#line 9 "test_tmp.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?)"); 

#line 11 "test_tmp.rpp"

}  

int TEST_TMP_CLASS::dbinsert (void)
/**
In bsd_buch einfuegen.
**/
{
        int dsqlstatus;
        if (ins_cursor == -1)
        {
                       prepare ();
        }
        dsqlstatus = execute_curs (ins_cursor);
        return dsqlstatus;
}

void TEST_TMP_CLASS::dbclose (void)
/**
Cursor schliessen.
**/
{
         if (ins_cursor == -1) return;
         close_sql (ins_cursor);
         ins_cursor = -1;
}
       
           

