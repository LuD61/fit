#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "dbclass.h"
#include "ptab.h"
#include "a_bas.h"
#include "tmp_bedarf.h"
#include "tables.h"

struct TMP_BEDARF tmp_bedarf, tmp_bedarf_null;
static PTAB_CLASS ptab_class;

extern int AG_BEILAGE ; 
TEST_TMP_CLASS Testtmp;

void TMP_BEDARF_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((short *)   &tmp_bedarf.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_bedarf.lieferdat,  SQLLONG, 0);
            sqlin ((short *)   &tmp_bedarf.kun_gr2,  SQLSHORT, 0);
            sqlin ((double *)   &tmp_bedarf.a_lsp,  SQLDOUBLE, 0);
            sqlin ((double *)   &tmp_bedarf.a,  SQLDOUBLE, 0);
    out_quest ((char *) &tmp_bedarf.mdn,1,0);
    out_quest ((char *) &tmp_bedarf.lieferdat,2,0);
    out_quest ((char *) &tmp_bedarf.kun_gr2,1,0);
    out_quest ((char *) &tmp_bedarf.a_lsp,3,0);
    out_quest ((char *) &tmp_bedarf.a,3,0);
    out_quest ((char *) &tmp_bedarf.posi,2,0);
    out_quest ((char *) &tmp_bedarf.auf_me,3,0);
    out_quest ((char *) &tmp_bedarf.me_einh,1,0);
    out_quest ((char *) tmp_bedarf.auf_me_bz,0,7);
    out_quest ((char *) &tmp_bedarf.teil_smt,1,0);
    out_quest ((char *) tmp_bedarf.smt,0,2);
    out_quest ((char *) &tmp_bedarf.prdk_typ,1,0);
    out_quest ((char *) &tmp_bedarf.extra,1,0);
    out_quest ((char *) &tmp_bedarf.sort,1,0);
            cursor = sqlcursor ("select tmp_bedarf.mdn,  "
"tmp_bedarf.lieferdat,  tmp_bedarf.kun_gr2,  tmp_bedarf.a_lsp,  "
"tmp_bedarf.a,  tmp_bedarf.posi,  tmp_bedarf.auf_me,  "
"tmp_bedarf.me_einh,  tmp_bedarf.auf_me_bz,  tmp_bedarf.teil_smt, tmp_bedarf.smt,  "
"tmp_bedarf.prdk_typ,  tmp_bedarf.extra,  tmp_bedarf.sort from tmp_bedarf "

                                  "where mdn = ? and lieferdat = ? and kun_gr2 = ? and a_lsp = ? and a = ? ");
                                  
    ins_quest ((char *) &tmp_bedarf.mdn,1,0);
    ins_quest ((char *) &tmp_bedarf.lieferdat,2,0);
    ins_quest ((char *) &tmp_bedarf.kun_gr2,1,0);
    ins_quest ((char *) &tmp_bedarf.a_lsp,3,0);
    ins_quest ((char *) &tmp_bedarf.a,3,0);
    ins_quest ((char *) &tmp_bedarf.posi,2,0);
    ins_quest ((char *) &tmp_bedarf.auf_me,3,0);
    ins_quest ((char *) &tmp_bedarf.me_einh,1,0);
    ins_quest ((char *) tmp_bedarf.auf_me_bz,0,7);
    ins_quest ((char *) &tmp_bedarf.teil_smt,1,0);
    ins_quest ((char *) tmp_bedarf.smt,0,2);
    ins_quest ((char *) &tmp_bedarf.prdk_typ,1,0);
    ins_quest ((char *) &tmp_bedarf.extra,1,0);
    ins_quest ((char *) &tmp_bedarf.sort,1,0);
            sqltext = "update tmp_bedarf set "
"tmp_bedarf.mdn = ?,  tmp_bedarf.lieferdat = ?,  "
"tmp_bedarf.kun_gr2 = ?,  tmp_bedarf.a_lsp = ?,  tmp_bedarf.a = ?,  "
"tmp_bedarf.posi = ?,  tmp_bedarf.auf_me = ?,  "
"tmp_bedarf.me_einh = ?,  tmp_bedarf.auf_me_bz = ?,  "
"tmp_bedarf.teil_smt = ?, tmp_bedarf.smt = ?,  tmp_bedarf.prdk_typ = ?,  "
"tmp_bedarf.extra = ?,  tmp_bedarf.sort = ? "

                                  "where mdn = ? and lieferdat = ? and kun_gr2 = ? and a_lsp = ? and a = ? ";
                                  
            sqlin ((short *)   &tmp_bedarf.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_bedarf.lieferdat,  SQLLONG, 0);
            sqlin ((short *)   &tmp_bedarf.kun_gr2,  SQLSHORT, 0);
            sqlin ((double *)   &tmp_bedarf.a_lsp,  SQLDOUBLE, 0);
            sqlin ((double *)   &tmp_bedarf.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &tmp_bedarf.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_bedarf.lieferdat,  SQLLONG, 0);
            sqlin ((short *)   &tmp_bedarf.kun_gr2,  SQLSHORT, 0);
            sqlin ((double *)   &tmp_bedarf.a_lsp,  SQLDOUBLE, 0);
            sqlin ((double *)   &tmp_bedarf.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor ("select kun_gr2 from tmp_bedarf "
                                  "where mdn = ? and lieferdat = ? and kun_gr2 = ? and a_lsp = ? and a = ? ");
                                  
            sqlin ((short *)   &tmp_bedarf.mdn,  SQLSHORT, 0);
            sqlin ((long *)   &tmp_bedarf.lieferdat,  SQLLONG, 0);
            sqlin ((short *)   &tmp_bedarf.kun_gr2,  SQLSHORT, 0);
            sqlin ((double *)   &tmp_bedarf.a_lsp,  SQLDOUBLE, 0);
            sqlin ((double *)   &tmp_bedarf.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor ("delete from tmp_bedarf "
                                  "where mdn = ? and lieferdat = ? and kun_gr2 = ? and a_lsp = ? and a = ? ");
                                  
    ins_quest ((char *) &tmp_bedarf.mdn,1,0);
    ins_quest ((char *) &tmp_bedarf.lieferdat,2,0);
    ins_quest ((char *) &tmp_bedarf.kun_gr2,1,0);
    ins_quest ((char *) &tmp_bedarf.a_lsp,3,0);
    ins_quest ((char *) &tmp_bedarf.a,3,0);
    ins_quest ((char *) &tmp_bedarf.posi,2,0);
    ins_quest ((char *) &tmp_bedarf.auf_me,3,0);
    ins_quest ((char *) &tmp_bedarf.me_einh,1,0);
    ins_quest ((char *) tmp_bedarf.auf_me_bz,0,7);
    ins_quest ((char *) &tmp_bedarf.teil_smt,1,0);
    ins_quest ((char *) tmp_bedarf.smt,0,2);
    ins_quest ((char *) &tmp_bedarf.prdk_typ,1,0);
    ins_quest ((char *) &tmp_bedarf.extra,1,0);
    ins_quest ((char *) &tmp_bedarf.sort,1,0); 
            ins_cursor = sqlcursor ("insert into tmp_bedarf ("
"mdn,  lieferdat,  kun_gr2,  a_lsp,  a,  posi,  auf_me,  me_einh,  auf_me_bz,  teil_smt, smt,  "
"prdk_typ,  extra,  sort) "

                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?)"); 

}


int TMP_BEDARF_CLASS::PrepareVarb (void)
{
    ins_quest ((char *)   &tmp_bedarf.mdn, 1, 0);
    ins_quest ((char *)    &tmp_bedarf.a_lsp, 3, 0);
    out_quest ((char *) prdk_k_rez, 0, 8);
    out_quest ((char *) &varb_artikel, 3, 0);
    out_quest ((char *) &varb_me_ist, 3, 0);
    out_quest ((char *)   &varb_me_einh, 1, 0);
    out_quest ((char *)   &prdk_me_einh, 1, 0);
    out_quest ((char *)   &varb_typ, 1, 0);
    out_quest ((char *)   &varb_ag, 2, 0);
    varb_cursor = prepare_sql ("select prdk_k.rez, a_mat.a, "
									   "prdk_varb.varb_me, a_basv.me_einh, a_bask.me_einh,  prdk_varb.typ, a_basv.ag from prdk_varb, a_bas a_bask, a_bas a_basv, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_varb.mdn "
                                       "and prdk_k.variante  = prdk_varb.variante "
                                       "and prdk_k.rez  = prdk_varb.rez "
                                       "and a_basv.a = a_mat.a "
									   "and a_mat.mat = prdk_varb.mat "
                                       "and a_bask.a = prdk_k.a "
									   );
    return varb_cursor;
}

int TMP_BEDARF_CLASS::PrepareVarb1 (void)
{
    ins_quest ((char *)   &tmp_bedarf.mdn, 1, 0);
    ins_quest ((char *)    &tmp_bedarf.a_lsp, 3, 0);
    out_quest ((char *) prdk_k_rez, 0, 8);
    out_quest ((char *) &varb_artikel, 3, 0);
    out_quest ((char *) &varb_me_ist, 3, 0);
    out_quest ((char *)   &varb_me_einh, 1, 0);
    out_quest ((char *)   &prdk_me_einh, 1, 0);
    out_quest ((char *)   &varb_typ, 1, 0);
    out_quest ((char *)   &varb_ag, 2, 0);
    varb_cursor1 = prepare_sql ("select prdk_k.rez, a_mat.a, "
									   "prdk_varb.varb_me, a_basv.me_einh, a_bask.me_einh , prdk_varb.typ, a_basv.ag from prdk_varb, a_bas a_bask, a_bas a_basv, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_varb.mdn "
                                       "and prdk_k.variante  = prdk_varb.variante "
                                       "and prdk_k.rez  = prdk_varb.rez "
                                       "and a_basv.a = a_mat.a "
									   "and a_mat.mat = prdk_varb.mat "
                                       "and a_bask.a = prdk_k.a "
									   );
    return varb_cursor1;
}

int TMP_BEDARF_CLASS::PrepareVarb2 (void)
{
    ins_quest ((char *)   &tmp_bedarf.mdn, 1, 0);
    ins_quest ((char *)    &tmp_bedarf.a_lsp, 3, 0);
    out_quest ((char *) prdk_k_rez, 0, 8);
    out_quest ((char *) &varb_artikel, 3, 0);
    out_quest ((char *) &varb_me_ist, 3, 0);
    out_quest ((char *)   &varb_me_einh, 1, 0);
    out_quest ((char *)   &prdk_me_einh, 1, 0);
    out_quest ((char *)   &varb_typ, 1, 0);
    out_quest ((char *)   &varb_ag, 2, 0);
    varb_cursor2 = prepare_sql ("select prdk_k.rez, a_mat.a, "
									   "prdk_varb.varb_me, a_basv.me_einh, a_bask.me_einh , prdk_varb.typ, a_basv.ag from prdk_varb, a_bas a_bask, a_bas a_basv, a_mat, prdk_k "
                                       "where prdk_k.mdn = ? "
                                       "and prdk_k.a  = ? "
                                       "and prdk_k.akv  = 1 "
                                       "and prdk_k.mdn  = prdk_varb.mdn "
                                       "and prdk_k.variante  = prdk_varb.variante "
                                       "and prdk_k.rez  = prdk_varb.rez "
                                       "and a_basv.a = a_mat.a "
									   "and a_mat.mat = prdk_varb.mat "
                                       "and a_bask.a = prdk_k.a "
									   );
    return varb_cursor2;
}

int TMP_BEDARF_CLASS::OpenVarb (short di)
{
 switch (di)
 {
        case 0 :
		    if (varb_cursor == -1)
			{
				varb_cursor = PrepareVarb ();
		    }
		    return open_sql (varb_cursor);
			break;
			
        case 1 :
		    if (varb_cursor1 == -1)
			{
				varb_cursor1 = PrepareVarb1 ();
		    }
		    return open_sql (varb_cursor1);
			break;
        case 2 :
		    if (varb_cursor2 == -1)
			{
				varb_cursor2 = PrepareVarb2 ();
		    }
		    return open_sql (varb_cursor2);
			break;
 }
 return open_sql (varb_cursor);
}

int TMP_BEDARF_CLASS::FetchVarb (short di)
{
 int dsqlstatus;
 switch (di)
 {
        case 0 :
		    if (varb_cursor == -1)
			{
				varb_cursor = PrepareVarb ();
			}
			dsqlstatus = fetch_sql (varb_cursor);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 1 :
		    if (varb_cursor1 == -1)
			{
				varb_cursor1 = PrepareVarb1 ();
			}
			dsqlstatus = fetch_sql (varb_cursor1);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
        case 2 :
		    if (varb_cursor2 == -1)
			{
				varb_cursor2 = PrepareVarb2 ();
			}
			dsqlstatus = fetch_sql (varb_cursor2);
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
			break;
 }
  return dsqlstatus;

}


int TMP_BEDARF_CLASS::CloseVarb (void)
{
    if (varb_cursor != -1)
    {
        close_sql (varb_cursor);
        varb_cursor = -1;
    }
    if (varb_cursor1 != -1)
    {
        close_sql (varb_cursor1);
        varb_cursor1 = -1;
    }
    if (varb_cursor2 != -1)
    {
        close_sql (varb_cursor2);
        varb_cursor2 = -1;
    }
    return 0;
}


void TMP_BEDARF_CLASS::prepare_lsp_a_a_lsp (void)
/**
Cursor fuer lsp vorbereiten. Mit Artikel-Nummer und posi lesen.
**/
{

    ins_quest ((char *) &tmp_bedarf.mdn,1,0);
    ins_quest ((char *) &tmp_bedarf.lieferdat,2,0);
    ins_quest ((char *) &tmp_bedarf.kun_gr2,1,0);
    ins_quest ((char *) &tmp_bedarf.a,3,0);
    ins_quest ((char *) &tmp_bedarf.a_lsp,3,0);


    out_quest ((char *) &tmp_bedarf.mdn,1,0);
    out_quest ((char *) &tmp_bedarf.lieferdat,2,0);
    out_quest ((char *) &tmp_bedarf.kun_gr2,1,0);
    out_quest ((char *) &tmp_bedarf.a_lsp,3,0);
    out_quest ((char *) &tmp_bedarf.a,3,0);
    out_quest ((char *) &tmp_bedarf.posi,2,0);
    out_quest ((char *) &tmp_bedarf.auf_me,3,0);
    out_quest ((char *) &tmp_bedarf.me_einh,1,0);
    out_quest ((char *) tmp_bedarf.auf_me_bz,0,7);
    out_quest ((char *) &tmp_bedarf.teil_smt,1,0);
    out_quest ((char *) tmp_bedarf.smt,0,2);
    out_quest ((char *) &tmp_bedarf.prdk_typ,1,0);
    out_quest ((char *) &tmp_bedarf.extra,1,0);
    out_quest ((char *) &tmp_bedarf.sort,1,0);
            cursor_lsp_a_a_lsp = sqlcursor ("select tmp_bedarf.mdn,  "
"tmp_bedarf.lieferdat,  tmp_bedarf.kun_gr2,  tmp_bedarf.a_lsp,  "
"tmp_bedarf.a,  tmp_bedarf.posi,  tmp_bedarf.auf_me,  "
"tmp_bedarf.me_einh,  tmp_bedarf.auf_me_bz,  tmp_bedarf.teil_smt, tmp_bedarf.smt, "
"tmp_bedarf.prdk_typ,  tmp_bedarf.extra,  tmp_bedarf.sort from tmp_bedarf "
                          "where mdn = ? "
                          "and lieferdat = ? "
                          "and kun_gr2 = ? "
                          "and a = ? "
                          "and a_lsp = ? and (prdk_typ = 10 or prdk_typ = 20) ");
}


int TMP_BEDARF_CLASS::lese_lsp_a_a_lsp (short mdn, long lieferdat, short kun_gr2, 
                           double a, double a_lsp)
/**
Tabelle lsp mit Lieferschein-Nummer lesen.
**/
{
         if (cursor_lsp_a_a_lsp == -1)
         {
                      prepare_lsp_a_a_lsp ();
         }
         tmp_bedarf.mdn  = mdn;
         tmp_bedarf.lieferdat  = lieferdat;
         tmp_bedarf.kun_gr2  = kun_gr2;
         tmp_bedarf.a   = a;
         tmp_bedarf.a_lsp = a_lsp;
         open_sql (cursor_lsp_a_a_lsp);
         fetch_sql (cursor_lsp_a_a_lsp);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}




int TMP_BEDARF_CLASS::lese_max_posi (short dmdn, long dlieferdat, short dkun_gr2)
{
	tmp_bedarf.posi = 0;
    ins_quest ((char *)   &dmdn, 1, 0);
    ins_quest ((char *)   &dlieferdat, 2, 0);
    ins_quest ((char *)    &dkun_gr2, 1, 0);
    out_quest ((char *)   &tmp_bedarf.posi, 1, 0);
	int dsqlstatus = execute_sql ("select posi from tmp_bedarf "
                                     "where mdn = ? "
                                     "and lieferdat = ? "
								  "and kun_gr2 = ? order by posi desc " );
	if (dsqlstatus != 0) return 0;
	return tmp_bedarf.posi;
}


BOOL TMP_BEDARF_CLASS::RezAufloesung (short AufrufTiefe,double a_lsp, double rez_artikel, double AufMe, short isExtra, short dteil_smt, char *smt, BOOL buffet_vorhanden, BOOL isAuftrag, long dauf, short dkun_gr2)
{
	double varb_gew = 0.0;
	double chg_gew = 0.0;
	short dposi;
	double dmenge = 0.0;
    char wert [5];
	short ag_salat = 6;  //Artikelgruppe erst mal so rein 
	short ag_dessert = 9;
	short ag_dressing = 7;
	short ag_suppe = 8;
	double a_gew;
	short me_einh = 0;



	int max_arr = 10;
	double beilage = 0.0;
	double abeilage [10];
	long beilage_stk = 0;
	long abeilage_stk [10];
	long sum_stk = 0;

	double abeilage_a_gew [10];
	short abeilage_me_einh [10];
	double beilage_a_gew;

	int beilage_cursor;

	tmp_bedarf.a_lsp = rez_artikel;

	ins_quest  ((char *) &tmp_bedarf.mdn, 1, 0);
	ins_quest  ((char *) &tmp_bedarf.a_lsp, 3, 0);
	out_quest  ((char *) prdk_k_rez, 0, 8);
	out_quest  ((char *) &chg_gew, 3, 0);
	out_quest  ((char *) &varb_gew, 3, 0);
	int dsqlstatus = execute_sql ("select rez, chg_gew, varb_gew from prdk_k "
                                     "where mdn = ? "
                                     "and a = ? "
								  "and akv = 1" );
	if (dsqlstatus != 0) return dsqlstatus;
	if (chg_gew == 0.0) return 1;
//	if (atoi(ChargengroesseVarb) == 1) chg_gew = varb_gew; 


	OpenVarb (AufrufTiefe);
	dposi = lese_max_posi (tmp_bedarf.mdn,tmp_bedarf.lieferdat,dkun_gr2);

	while (FetchVarb (AufrufTiefe) == 0)
    {
		tmp_bedarf.a = varb_artikel;
		tmp_bedarf.a_lsp = a_lsp;
        tmp_bedarf.auf_me = 0.0;

// Essig   alternative Beilage 
		tmp_bedarf.me_einh = varb_me_einh;
		dsqlstatus = lese_a_bas (tmp_bedarf.a);
		beilage_a_gew = _a_bas.a_gew ; //290812
		if (_a_bas.ag == AG_BEILAGE)
		{
			if (shopaufp.a_beilage != tmp_bedarf.a && shopaufp.a_beilage > 0)
			{
				if (shopaufp.a_beilage == 50253)
				{
					int di = 1;
				}
				tmp_bedarf.a =  shopaufp.a_beilage;
				tmp_bedarf.extra ++;
				dsqlstatus = lese_a_bas (tmp_bedarf.a);
				tmp_bedarf.me_einh = _a_bas.me_einh;  //030812
			}
		}


//		if (lese_lsp_a_a_lsp (tmp_bedarf.mdn,tmp_bedarf.lieferdat,tmp_bedarf.kun_gr2, tmp_bedarf.a, a_lsp) == 100)
//		{
//			dposi += 10;
//			tmp_bedarf.posi = dposi;
//		}
//Essig
//Die Reihenfolge auf der Seite 1 des MIKRO ges. soll folgender ma�en sein:
//Artikelnr.���� - ���80000 an 1 Stelle ( Suppe)  ag=8
//-����� 20000 an 2 Stelle ( Hauptkomponente) ag 2 
//-����� 30000 an 3 Stelle ( So�e)		 ag 3
//-����� 40000 an 4 Stelle ( Gem�se)	ag 4
//-����� 50000 an 5 Stelle ( Dessert )	ag 5
		tmp_bedarf.sort = (short) varb_ag;
		if (varb_ag == 8) tmp_bedarf.sort = 1; 


        memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
        sprintf (wert, "%hd", tmp_bedarf.me_einh);
        dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);  //Steigt hier aus warum ??????? ??????????????????????????????????????

        if (dsqlstatus == 0)
        {
                   strcpy (tmp_bedarf.auf_me_bz, ptabn.ptbezk);
        }





		tmp_bedarf.teil_smt = dteil_smt;
		strcpy (tmp_bedarf.smt, smt) ;

// jetzt Kundenbranche pr�fen   
// je nach Kundenbranche : wird Salat oder/und Dessert seperat kommissioniert :
//	kun.kun_bran2 : ptwer1 = 1 -> Salate sep. Kommissionieren 

//	kun.kun_bran2 : ptwer2 = 1 -> Dessert sep. Kommissionieren 
//	(wenn beides = 1 : Salate und Dessert auf einem Kommissionierzettel

/**  macht hier keinen Sinn : extra wird erst einmal benutzt , um zu ermitteln, wie oft alternative Beilage bestell wurde
//		tmp_bedarf.extra = 0;
//		if (Salat_extra > 0 && (ag_salat == _a_bas.ag || ag_dressing == _a_bas.ag ))
//		{
//			tmp_bedarf.extra = Salat_extra;
//		}
//		if (Dessert_extra > 0 && ag_dessert == _a_bas.ag)
//		{
//			tmp_bedarf.extra = Dessert_extra;
//		}
//		if (Suppe_extra > 0 && ag_suppe == _a_bas.ag)
//		{
//			tmp_bedarf.extra = Suppe_extra;
//		}
	// 	if (isOhneSalat) tmp_bedarf.extra = 1;
	***********/







		if (prdk_me_einh != 2) chg_gew = 1.0;   //Wenn das Men� St�ckartikel ist, dan 1:1 �bertragen , Chargengewicht ist dann immer 1 St�ck
		tmp_bedarf.prdk_typ = 10;
		if (varb_typ == 2) tmp_bedarf.prdk_typ = 11; //Grundbr�t

		if ((ag_salat == _a_bas.ag || ag_dressing == _a_bas.ag)  && isExtra == 0)  //d.h. Artikel ist Salat oder Dressing , kommt aber nicht aus einem Salatbuffet(isExtra == 0)
		{
			tmp_bedarf.prdk_typ = 20; //Diese Salat und Dressingbeilagen werden mit prdk_typ = 20 gekennzeichnet, damit sie deaktiviert werden k�nnen, wenn im Auftrag Salatbuffet vorkommt
			if (buffet_vorhanden == TRUE) 
			{
				if (_a_bas.a >= 40000 && _a_bas.a <= 49999)  //EF-27
				{
					//dann soll der Artikel auf der K-Liste stehen bleiben
				}
				else
				{
					return TRUE;
				}
			}
		}
		

//		tmp_bedarf.prdk_stufe = AufrufTiefe; //wenn > 0 , dann Materialien aus Grundbr�t 
		dmenge = AufMe  * (varb_me_ist / chg_gew);

		if (_a_bas.ag == AG_BEILAGE)
		{
				  //290812 A
					if (tmp_bedarf.me_einh == 2) 
					{
						if (varb_me_einh != 2 && beilage_a_gew > 0.0)
						{
							dmenge *= beilage_a_gew;
						}
					}
					else
					{
						if (varb_me_einh == 2 )
						{
							dmenge = AufMe * 2; 
						}
					}
		}
				  //290812 E

		if (dbreadfirst () == 0) tmp_bedarf.auf_me += dmenge;
		else tmp_bedarf.auf_me = dmenge;
//		tmp_bedarf.a_me_einh = me_einh;
		if (varb_typ == 2)  //Grundbr�t
        {
			RezAufloesung ((AufrufTiefe + 1) ,a_lsp,tmp_bedarf.a, dmenge,isExtra,dteil_smt, smt, buffet_vorhanden, isAuftrag, dauf, dkun_gr2) ;
        }
		else
		{
//			update_lsp (tmp_bedarf.mdn,tmp_bedarf.fil,tmp_bedarf.ls, tmp_bedarf.auf, tmp_bedarf.a, a_lsp, tmp_bedarf.posi);


			int sav_kun_gr2 = dkun_gr2;
			int sav_teil_smt = dteil_smt;
			strcpy (tmp_bedarf.smt, smt) ;

			if (shopaufp.a_beilage == -1 && _a_bas.ag == AG_BEILAGE)    // -1 : Beilage nicht definiert, ist im shop nicht eindeutig bestellt, sondern vierschiedene Beilagen auf einer Position
			{
			    ins_quest ((char *) &dauf, 2, 0);
				ins_quest ((char *) &tmp_bedarf.a_lsp, 3, 0);
				out_quest ((char *) &beilage, 3, 0);
				out_quest ((char *) &beilage_stk, 2, 0);
				out_quest ((char *) &me_einh, 1, 0);     //290812
				out_quest ((char *) &a_gew, 3, 0);     //290812
				beilage_cursor = prepare_sql  ("select shopaufp.a_beilage, sum(shopaufp.stk), a_bas.me_einh, a_bas.a_gew from shopaufp,a_bas where shopaufp.auf = ? and shopaufp.a  = ? and a_bas.a = shopaufp.a_beilage  group by 1,3,4");
				open_sql (beilage_cursor);
				fetch_sql (beilage_cursor);
				int i = 0;
				sum_stk = 0;
				while (sqlstatus == 0 && i < max_arr)
				{
					if (beilage == 50253)
					{
						int di = 1;
					}
					abeilage[i] = beilage;
					abeilage_stk[i] = beilage_stk;
					abeilage_me_einh[i] = me_einh;
					abeilage_a_gew[i] = a_gew;
					sum_stk += beilage_stk;
					fetch_sql (beilage_cursor);
					i ++;
				}
				abeilage[i] = 0.0;
				abeilage_stk[i] = 0;
				abeilage_me_einh[i] = 0;
				abeilage_a_gew[i] = 0;

			
			
				i = 0;
				double dmenge_beilage = 0.0;
				while (abeilage[i] > 0.0)
				{
					tmp_bedarf.a = abeilage[i];
//290812					dmenge = AufMe * abeilage_stk[i] / sum_stk;
					dmenge_beilage = dmenge * abeilage_stk[i] / sum_stk; //290812
				  //290812 A
					if (abeilage_me_einh[i] == 2) 
					{
						if (varb_me_einh != 2 && beilage_a_gew > 0.0)
						{
							dmenge_beilage *= beilage_a_gew;
						}
					}
					else
					{
						if (varb_me_einh == 2 )
						{
							dmenge_beilage = abeilage_stk[i] * 2; 
						}
					}
				  //290812 E



					abeilage[i] = 0.0;
					abeilage_stk[i] = 0;
					abeilage_me_einh[i] = 0;
					abeilage_a_gew[i] = 0;

//					tmp_bedarf.kun_gr2 = sav_kun_gr2;
//					tmp_bedarf.teil_smt = sav_teil_smt;
					tmp_bedarf.kun_gr2 = dkun_gr2;
					tmp_bedarf.teil_smt = dteil_smt;
					strcpy (tmp_bedarf.smt, smt) ;
					if (dbreadfirst () == 0) tmp_bedarf.auf_me += dmenge_beilage; //290812
					else tmp_bedarf.auf_me = dmenge_beilage;  //290812

					dbupdate ();
					insert_testtmp (dmenge_beilage);
					tmp_bedarf.kun_gr2 = 0;
					tmp_bedarf.teil_smt = 0;
					if (dbreadfirst () == 0) tmp_bedarf.auf_me += dmenge_beilage;
					else tmp_bedarf.auf_me = dmenge_beilage;
					dbupdate ();
					i ++;
					tmp_bedarf.kun_gr2 = dkun_gr2;
					tmp_bedarf.teil_smt = dteil_smt;

				}
			}
			else
			{
//				tmp_bedarf.kun_gr2 = sav_kun_gr2;
//				tmp_bedarf.teil_smt = sav_teil_smt;
				tmp_bedarf.kun_gr2 = dkun_gr2;
				tmp_bedarf.teil_smt = dteil_smt;
				strcpy (tmp_bedarf.smt, smt) ;
				dbupdate ();
					insert_testtmp (dmenge);
				tmp_bedarf.kun_gr2 = 0;
				tmp_bedarf.teil_smt = 0;
				if (dbreadfirst () == 0) tmp_bedarf.auf_me += dmenge;
				else tmp_bedarf.auf_me = dmenge;
				dbupdate ();
//				tmp_bedarf.kun_gr2 = sav_kun_gr2;
//				tmp_bedarf.teil_smt = sav_teil_smt;
				tmp_bedarf.kun_gr2 = dkun_gr2;
				tmp_bedarf.teil_smt = dteil_smt;
			}
		}

	}
	return TRUE;
}




void TMP_BEDARF_CLASS::insert_testtmp (double me)
{
	test_tmp.mdn = tmp_bedarf.mdn;
	test_tmp.lieferdat = tmp_bedarf.lieferdat;
	test_tmp.kun_gr2 = tmp_bedarf.kun_gr2;
	test_tmp.a_lsp = tmp_bedarf.a_lsp;
	test_tmp.a = tmp_bedarf.a;
	test_tmp.posi = tmp_bedarf.posi;
	test_tmp.auf_me = me;
	test_tmp.me_einh = tmp_bedarf.me_einh;
	test_tmp.teil_smt = tmp_bedarf.teil_smt;
	test_tmp.prdk_typ = tmp_bedarf.prdk_typ;
	test_tmp.extra = tmp_bedarf.extra;
	test_tmp.sort = tmp_bedarf.sort;
					Testtmp.dbinsert();
}
