#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "dbclass.h"
#include "shopaufk.h"

struct SHOPAUFK shopaufk, shopaufk_null;

void SHOPAUFK_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((long *)   &shopaufk.orders_id,  SQLLONG, 0);
    out_quest ((char *) &shopaufk.orders_id,2,0);
    out_quest ((char *) &shopaufk.kun,2,0);
    out_quest ((char *) shopaufk.comments,0,256);
    out_quest ((char *) &shopaufk.lieferdat,2,0);
    out_quest ((char *) &shopaufk.auf,2,0);
    out_quest ((char *) &shopaufk.gueltig,1,0);
    out_quest ((char *) &shopaufk.stat,1,0);
            cursor = sqlcursor ("select shopaufk.orders_id,  "
"shopaufk.kun,  shopaufk.comments,  shopaufk.lieferdat,  shopaufk.auf,  "
"shopaufk.gueltig,  shopaufk.stat from shopaufk "

#line 18 "shopaufk.rpp"
                                  "where shopaufk.orders_id = ?");
    ins_quest ((char *) &shopaufk.orders_id,2,0);
    ins_quest ((char *) &shopaufk.kun,2,0);
    ins_quest ((char *) shopaufk.comments,0,256);
    ins_quest ((char *) &shopaufk.lieferdat,2,0);
    ins_quest ((char *) &shopaufk.auf,2,0);
    ins_quest ((char *) &shopaufk.gueltig,1,0);
    ins_quest ((char *) &shopaufk.stat,1,0);
            sqltext = "update shopaufk set "
"shopaufk.orders_id = ?,  shopaufk.kun = ?,  shopaufk.comments = ?,  "
"shopaufk.lieferdat = ?,  shopaufk.auf = ?,  shopaufk.gueltig = ?,  "
"shopaufk.stat = ? "

#line 20 "shopaufk.rpp"
                                  "where orders_id = ?";
            sqlin ((long *)   &shopaufk.orders_id,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &shopaufk.orders_id,  SQLLONG, 0);
            test_upd_cursor = sqlcursor ("select orders_id from shopaufk "
                                  "where orders_id = ?");
            sqlin ((long *)   &shopaufk.orders_id,  SQLLONG, 0);
            del_cursor = sqlcursor ("delete from shopaufk "
                                  "where orders_id = ?");
    ins_quest ((char *) &shopaufk.orders_id,2,0);
    ins_quest ((char *) &shopaufk.kun,2,0);
    ins_quest ((char *) shopaufk.comments,0,256);
    ins_quest ((char *) &shopaufk.lieferdat,2,0);
    ins_quest ((char *) &shopaufk.auf,2,0);
    ins_quest ((char *) &shopaufk.gueltig,1,0);
    ins_quest ((char *) &shopaufk.stat,1,0);
            ins_cursor = sqlcursor ("insert into shopaufk ("
"orders_id,  kun,  comments,  lieferdat,  auf,  gueltig,  stat) "

#line 31 "shopaufk.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?)"); 

#line 33 "shopaufk.rpp"
}
