#ifndef _TMP_BEDARF_DEF
#define _TMP_BEDARF_DEF

#include "dbclass.h"
#include "test_tmp.h"

struct TMP_BEDARF {
   short     mdn;
   long      lieferdat;
   short     kun_gr2;
   double    a_lsp;
   double    a;
   long      posi;
   double    auf_me;
   short     me_einh;
   char      auf_me_bz[7];
   short     teil_smt;
   char      smt[2];
   short     prdk_typ;
   short     extra;
   short     sort;
};
extern struct TMP_BEDARF tmp_bedarf, tmp_bedarf_null;


class TMP_BEDARF_CLASS : public DB_CLASS 
{
       private :
            short varb_cursor;
            short varb_cursor1;
            short varb_cursor2;
			double varb_artikel;
            short cursor_lsp_a_a_lsp;
			double varb_me_ist ;
			short varb_me_einh ;
			short prdk_me_einh ;
			short varb_typ;
			long varb_ag;
			char prdk_k_rez [9];

               void prepare (void);
               void insert_testtmp (double);
       public :
               TMP_BEDARF_CLASS () : DB_CLASS ()
               {
                    varb_cursor = -1;
                    varb_cursor1 = -1;
                    varb_cursor2 = -1;
					varb_artikel = 0.0;
                    cursor_lsp_a_a_lsp = -1;
					varb_me_ist = 0.0;
					varb_me_einh = 0;
					prdk_me_einh = 0;
					varb_typ = 0;
					varb_ag = 0;
					strcpy(prdk_k_rez,"");
               }
		       int PrepareVarb (void);
	           int PrepareVarb1 (void);
			   int PrepareVarb2 (void);
           void prepare_lsp_a_a_lsp (void);
	        int OpenVarb (short);
		    int FetchVarb (short);
			int CloseVarb (void);
		   BOOL RezAufloesung (short,double,double,double,short, short, char *,  BOOL, BOOL, long, short);
           int lese_max_posi (short, long, short);
           int lese_lsp_a_a_lsp (short, long, short, double, double);


};
#endif
