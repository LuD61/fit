create table "fit".tmp_bedarf 
  (
    mdn smallint,
    lieferdat date,
    kun_gr2 smallint,
    a_lsp decimal(13,0),
    a decimal(13,0),
    posi integer,
    auf_me decimal(8,3),
    me_einh smallint,	
    auf_me_bz char(6),
    teil_smt smallint,
    prdk_typ smallint,
    extra smallint,
    sort smallint
  );
revoke all on "fit".tmp_bedarf from "public";

create unique index "fit".i01tmp_bedarf on "fit".tmp_bedarf 
    (mdn,lieferdat,kun_gr2,a_lsp,a);




