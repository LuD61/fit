#pragma once
/*   Datenbankfunktionen                                  */


struct LSPEINZEL {
   short     mdn;
   short     fil;
   long      ls;
   double    a;
   double    auf_me;
   char      auf_me_bz[7];
   double    lief_me;
   char      lief_me_bz[7];
   double    ls_vk_pr;
   double    ls_lad_pr;
   short     delstatus;
   double    tara;
   long      posi;
   long      lsp_txt;
   short     pos_stat;
   short     sa_kz_sint;
   char      erf_kz[2];
   double    prov_satz;
   short     leer_pos;
   long      hbk_date;
   char      ls_charge[31];
   double    auf_me_vgl;
   double    ls_vk_euro;
   double    ls_vk_fremd;
   double    ls_lad_euro;
   double    ls_lad_fremd;
   double    rab_satz;
   short     me_einh_kun;
   short     me_einh;
   short     me_einh_kun1;
   double    auf_me1;
   double    inh1;
   short     me_einh_kun2;
   double    auf_me2;
   double    inh2;
   short     me_einh_kun3;
   double    auf_me3;
   double    inh3;
   double    lief_me1;
   double    lief_me2;
   double    lief_me3;
   double    a_grund;
   char      kond_art[5];
   short     ls_pos_kz;
   long      posi_ext;
   long      kun;
   short     na_lief_kz;  
   long      nve_posi;
   short     teil_smt;  
   char      lief_me_bz_ist [11];
   short     me_einh_ist;
   double    inh_ist;
   double    me_ist;
   char      ls_ident [21];
   short     pos_txt_kz;
   double	 anz_einh;
   double	 a_lsp;
   short prdk_stufe;
   short prdk_typ;
   long auf;
   short extra;
   short sort;

};



extern struct LSPEINZEL lspeinzel, lspeinzel_null;

class LS_EINZEL_CLASS
{
       private :

//  Cursor fuer lspeinzel


            short cursor_lspeinzel;
            short cursor_lspeinzel_auf;
            short cursor_lsp_a_lsp;
            short cursor_lsp_a_a_lsp;
            short cursor_lsp_w;
            short test_lsp_upd;
			short deaktiviere_lspeinzel_20;
			short aktiviere_lspeinzel_20;
			short suche_buffet;
            short cursor_lsp_upd;
            short cursor_lsp_ins;
            short cursor_lsp_del;
            short cursor_lsp_dells;
            short varb_cursor;
            short varb_cursor1;
            short varb_cursor2;
			double varb_artikel;
			double varb_me_ist ;
			short varb_me_einh ;
			short prdk_me_einh ;
			short varb_typ;
			long varb_ag;
			char prdk_k_rez [9];

       public:
		   short ModusCatering;
			short Salat_extra ;
			short Dessert_extra ;
			short Suppe_extra;
		   short CateringExtra;
			double a_grund_von_buffet;
			double a_grund_bis_buffet;

			char komm_formular [9] ;
           LS_EINZEL_CLASS ()
           {
					a_grund_von_buffet = (double) 151;
					a_grund_bis_buffet = (double) 155;
                    cursor_lspeinzel = -1;
                    cursor_lspeinzel_auf = -1;
                    cursor_lsp_a_lsp = -1;
                    cursor_lsp_a_a_lsp = -1;
                    cursor_lsp_w = -1;
                    test_lsp_upd = -1;
					deaktiviere_lspeinzel_20 = -1;
					suche_buffet = -1;
					aktiviere_lspeinzel_20 = -1;
                    cursor_lsp_upd = -1;
                    cursor_lsp_ins = -1;
                    cursor_lsp_dells = -1;
                    cursor_lsp_del = -1;
                    varb_cursor = -1;
                    varb_cursor1 = -1;
                    varb_cursor2 = -1;
					varb_artikel = 0.0;
					varb_me_ist = 0.0;
					varb_me_einh = 0;
					prdk_me_einh = 0;
					varb_typ = 0;
					varb_ag = 0;
					strcpy(prdk_k_rez,"");
					ModusCatering = 0;
					Salat_extra = 0;
					Dessert_extra = 0 ;
					Suppe_extra = 0;

					CateringExtra = 0;
					strcpy(komm_formular, "");


           }

           void prepare (void);
           int PrepareVarb (void);
           int PrepareVarb1 (void);
           int PrepareVarb2 (void);
	        int OpenVarb (short);
		    int FetchVarb (short);
			int CloseVarb (void);

           void lsp_out (void);
           void lsp_in (void);
           void prepare_lspeinzel (char *);
           void prepare_lspeinzel_auf (void);
           void prepare_lsp_a_lsp (void);
           void prepare_lsp_a_a_lsp (void);
           void prepare_lsp_w (char *);
           void prepare_lsp_upd (void);
           int aktiviere_salat (short,short,long);

           void prepare_lsp_del  (void);
           void close_lsp_upd (void);
           void close_lsp_del  (void);

           int lese_lspeinzel (short, short, long,long, char *);
           int lese_lspeinzel (void);
           void close_lspeinzel (void);
           int lese_lspeinzel_auf (short, short, long,long);
           int lese_lsp_a_a_lsp (short, short, long,long, double, double);
           int lese_lsp_a_lsp (short, short, long,long, double);
           int lese_lspeinzel_auf (void);
           int lese_lsp_a_a_lsp (void);
           int lese_lsp_a_lsp (void);
           int lese_lsp_w (char *);
           int lese_lsp_w (void);
           int lese_lsp_upd (void);
           int delete_lspls (short, short, long,long);
		   int Schreibe_tmp_catering (short dmdn, short dfil, long dls, long dauf);
           int delete_lsp (short, short, long, long,long, double);
           int lese_max_posi (short, short, long,long);
           int update_lsp (short, short, long,long, double,double, long);
           int update_lsp_mehrf (short, short, long,long, double,double, long, double,short,double );
           int lock_lsp (short, short, long,long, double, long);
		   BOOL RezAufloesung (short,double,double,double,short, short);

};

