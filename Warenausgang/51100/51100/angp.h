#ifndef _ANGP_DEF
#define _ANFP_DEF
#include "dbclass.h"

struct ANGP {
   short     mdn;
   short     fil;
   long      ang;
   long      posi;
   long      angp_txt;
   double    a;
   double    auf_me;
   char      auf_me_bz[7];
   double    lief_me;
   char      lief_me_bz[7];
   double    auf_vk_pr;
   double    auf_lad_pr;
   short     delstatus;
   short     sa_kz_sint;
   double    prov_satz;
   long      ksys;
   long      pid;
   long      auf_klst;
   short     teil_smt;
   short     dr_folge;
   double    inh;
   double    auf_vk_euro;
   double    auf_vk_fremd;
   double    auf_lad_euro;
   double    auf_lad_fremd;
   double    rab_satz;
   short     me_einh_kun;
   short     me_einh;
};
extern struct ANGP angp, angp_null;

#line 6 "angp.rh"

class ANGP_CLASS : public DB_CLASS
{
       private :
               int del_ang_curs; 
               int sel_ang_curs;
               void prepare (void);
               void prepare_ang (char *);
       public :
               ANGP_CLASS () : del_ang_curs (-1), sel_ang_curs (-1), DB_CLASS ()
               {
               }
               int dbreadfirst (void);
               int dbclose (void);
               int dbreadfirstang (char *);
               int dbreadang (void);
               int dbcloseang (void);
               int dbdeleteang (void);
};
#endif
