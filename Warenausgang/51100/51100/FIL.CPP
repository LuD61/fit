struct FIL {
   char      abr_period[2];
   long      adr;
   long      adr_lief;
   short     afl;
   char      auf_typ[2];
   char      best_kz[2];
   char      bli_kz[2];
   long      dat_ero;
   short     daten_mnp;
   short     delstatus;
   short     fil;
   char      fil_kla[2];
   short     fil_gr;
   double    fl_lad;
   double    fl_nto;
   double    fl_vk_ges;
   short     frm;
   long      iakv;
   char      inv_rht[2];
   long      kun;
   char      lief[17];
   char      lief_rht[2];
   long      lief_s;
   char      ls_abgr[2];
   char      ls_kz[2];
   short     ls_sum;
   short     mdn;
   char      pers[13];
   short     pers_anz;
   char      pos_kum[2];
   char      pr_ausw[2];
   char      pr_bel_entl[2];
   char      pr_fil_kz[2];
   long      pr_lst;
   char      pr_vk_kz[2];
   double    reg_bed_theke_lng;
   double    reg_kt_lng;
   double    reg_kue_lng;
   double    reg_lng;
   double    reg_tks_lng;
   double    reg_tkt_lng;
   char      ret_entl[2];
   char      smt_kz[2];
   short     sonst_einh;
   short     sprache;
   char      sw_kz[2];
   long      tou;
   char      umlgr[2];
   char      verk_st_kz[2];
   short     vrs_typ;
   char      inv_akv[2];
};
extern struct FIL fil, fil_null;

#line 2 "fil.rpp"

    out_quest ((char *) fil.abr_period,0,2);
    out_quest ((char *) &fil.adr,2,0);
    out_quest ((char *) &fil.adr_lief,2,0);
    out_quest ((char *) &fil.afl,1,0);
    out_quest ((char *) fil.auf_typ,0,2);
    out_quest ((char *) fil.best_kz,0,2);
    out_quest ((char *) fil.bli_kz,0,2);
    out_quest ((char *) &fil.dat_ero,2,0);
    out_quest ((char *) &fil.daten_mnp,1,0);
    out_quest ((char *) &fil.delstatus,1,0);
    out_quest ((char *) &fil.fil,1,0);
    out_quest ((char *) fil.fil_kla,0,2);
    out_quest ((char *) &fil.fil_gr,1,0);
    out_quest ((char *) &fil.fl_lad,3,0);
    out_quest ((char *) &fil.fl_nto,3,0);
    out_quest ((char *) &fil.fl_vk_ges,3,0);
    out_quest ((char *) &fil.frm,1,0);
    out_quest ((char *) &fil.iakv,2,0);
    out_quest ((char *) fil.inv_rht,0,2);
    out_quest ((char *) &fil.kun,2,0);
    out_quest ((char *) fil.lief,0,17);
    out_quest ((char *) fil.lief_rht,0,2);
    out_quest ((char *) &fil.lief_s,2,0);
    out_quest ((char *) fil.ls_abgr,0,2);
    out_quest ((char *) fil.ls_kz,0,2);
    out_quest ((char *) &fil.ls_sum,1,0);
    out_quest ((char *) &fil.mdn,1,0);
    out_quest ((char *) fil.pers,0,13);
    out_quest ((char *) &fil.pers_anz,1,0);
    out_quest ((char *) fil.pos_kum,0,2);
    out_quest ((char *) fil.pr_ausw,0,2);
    out_quest ((char *) fil.pr_bel_entl,0,2);
    out_quest ((char *) fil.pr_fil_kz,0,2);
    out_quest ((char *) &fil.pr_lst,2,0);
    out_quest ((char *) fil.pr_vk_kz,0,2);
    out_quest ((char *) &fil.reg_bed_theke_lng,3,0);
    out_quest ((char *) &fil.reg_kt_lng,3,0);
    out_quest ((char *) &fil.reg_kue_lng,3,0);
    out_quest ((char *) &fil.reg_lng,3,0);
    out_quest ((char *) &fil.reg_tks_lng,3,0);
    out_quest ((char *) &fil.reg_tkt_lng,3,0);
    out_quest ((char *) fil.ret_entl,0,2);
    out_quest ((char *) fil.smt_kz,0,2);
    out_quest ((char *) &fil.sonst_einh,1,0);
    out_quest ((char *) &fil.sprache,1,0);
    out_quest ((char *) fil.sw_kz,0,2);
    out_quest ((char *) &fil.tou,2,0);
    out_quest ((char *) fil.umlgr,0,2);
    out_quest ((char *) fil.verk_st_kz,0,2);
    out_quest ((char *) &fil.vrs_typ,1,0);
    out_quest ((char *) fil.inv_akv,0,2);
fil_cursor = prepare_sql ("select fil.abr_period,  fil.adr,  "
"fil.adr_lief,  fil.afl,  fil.auf_typ,  fil.best_kz,  fil.bli_kz,  "
"fil.dat_ero,  fil.daten_mnp,  fil.delstatus,  fil.fil,  fil.fil_kla,  "
"fil.fil_gr,  fil.fl_lad,  fil.fl_nto,  fil.fl_vk_ges,  fil.frm,  fil.iakv,  "
"fil.inv_rht,  fil.kun,  fil.lief,  fil.lief_rht,  fil.lief_s,  fil.ls_abgr,  "
"fil.ls_kz,  fil.ls_sum,  fil.mdn,  fil.pers,  fil.pers_anz,  fil.pos_kum,  "
"fil.pr_ausw,  fil.pr_bel_entl,  fil.pr_fil_kz,  fil.pr_lst,  "
"fil.pr_vk_kz,  fil.reg_bed_theke_lng,  fil.reg_kt_lng,  "
"fil.reg_kue_lng,  fil.reg_lng,  fil.reg_tks_lng,  fil.reg_tkt_lng,  "
"fil.ret_entl,  fil.smt_kz,  fil.sonst_einh,  fil.sprache,  fil.sw_kz,  "
"fil.tou,  fil.umlgr,  fil.verk_st_kz,  fil.vrs_typ,  fil.inv_akv from fil "

#line 4 "fil.rpp"
                          "where mdn = ? "
                          "and fil = ?");