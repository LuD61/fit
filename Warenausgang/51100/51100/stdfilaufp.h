#ifndef _STDFILAUFP_DEF
#define _STDFILAUFP_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmask.h"
#include "dbclass.h"

struct STDFILAUFP {
   short     mdn;
   short     fil;
   char      wo_tag[3];
   long      lfd;
   double    a;
   char      me_einh_bz[7];
   short     me_einh;
   double    me;
   long      posi;
};
extern struct STDFILAUFP stdfilaufp, stdfilaufp_null;

#line 10 "stdfilaufp.rh"

class STDFILAUFP_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               STDFILAUFP_CLASS () : DB_CLASS ()
               {
               }
};
#endif
