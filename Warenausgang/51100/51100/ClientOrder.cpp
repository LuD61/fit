// ClientOrder.cpp: Implementierung der Klasse CClientOrder.
//
//////////////////////////////////////////////////////////////////////

#include <fstream>
#include "ClientOrder.h"
#include <stdlib.h>
#include "strfkt.h"
#include "A_bas.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

#define RD_OPTIMIZE RdOptimize


using namespace std;

CClientOrder::CClientOrder()
{
	ListHandler = NULL;
	StndAll = NULL;
	mdn = 0;
	fil = 0;
	kun_fil = 0;
	kun_nr = 0l;
	SortMode = PosA;
	RdOptimize = No;
	RefreshRdOptimize = FALSE;
	cursor = -1;
    LastFromAufKun = FALSE;
	inh = 0.0;
}

CClientOrder::~CClientOrder()
{

}

void CClientOrder::SetKeys (short mdn, short fil, short kun_fil, long kun)
{
	stnd_aufp.mdn = mdn;
	stnd_aufp.fil = fil;
	stnd_aufp.kun_fil = kun_fil;
	stnd_aufp.kun = kun;
	this->mdn = mdn;
	this->fil = fil;
	this->kun_fil = kun_fil;
	this->kun_nr = kun;
}

void CClientOrder::Load ()
{
	BOOL MustLoad = FALSE;

	if (ListHandler == NULL)
	{
		ListHandler = CListHandler::GetInstance ();
		StndAll = ListHandler->GetStndAll ();
		ListHandler->Init ();
		MustLoad = TRUE;
		mdn = stnd_aufp.mdn;
		fil = stnd_aufp.fil;
		kun_fil = stnd_aufp.kun_fil;
		kun_nr = stnd_aufp.kun;
	}
	else
	{
		if (mdn != stnd_aufp.mdn ||
			fil != stnd_aufp.fil ||
			kun_fil != stnd_aufp.kun_fil ||
			kun_nr != stnd_aufp.kun)
		{
			ListHandler->Init ();
			MustLoad = TRUE;
		    mdn = stnd_aufp.mdn;
			fil = stnd_aufp.fil;
			kun_fil = stnd_aufp.kun_fil;
			kun_nr = stnd_aufp.kun;
		}
		else
		{
			ListHandler->GetStndValue ()->DestroyElements ();
			ListHandler->GetStndValue ()->Destroy ();
			ListHandler->InitAufMe ();
		}
	}
	if (MustLoad)
	{
		if (cursor == -1)
		{
			Prepare ();
			Read ();
		}

	}
}

void CClientOrder::Prepare ()
{
	StndAufp.sqlin ((short *) &mdn, 1, 0);
	StndAufp.sqlin ((short *) &fil, 1, 0);
	StndAufp.sqlin ((long *) &kun_nr, 2, 0);
	StndAufp.sqlin ((short *) &kun_fil, 1, 0);

	StndAufp.sqlout ((short *) &stnd_aufp.mdn, 1, 0);
	StndAufp.sqlout ((short *) &stnd_aufp.fil, 1, 0);
	StndAufp.sqlout ((short *) &stnd_aufp.kun, 2, 0);
	StndAufp.sqlout ((short *) &stnd_aufp.kun_fil, 1, 0);
	StndAufp.sqlout ((long *) &stnd_aufp.posi, 2, 0);
	StndAufp.sqlout ((double *) &stnd_aufp.a, 3, 0);
	StndAufp.sqlout ((short *) &ag, 1, 0);
	StndAufp.sqlout ((short *) &wg, 1, 0);
	StndAufp.sqlout ((char *)  &a_bz1, 0, 25);

	if (SortMode == PosA)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a "
								"order by 5,6");  
	}
	else if (SortMode == AgA)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a "
								"order by 7,6");  
	}
	else if (SortMode == WgA)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a "
								"order by 8,6");  
	}
	else if (SortMode == AgBz)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a "
								"order by 7,9");  
	}
	else if (SortMode == WgBz)
	{
           	cursor = StndAufp.sqlcursor ("select stnd_aufp.mdn, stnd_aufp.fil, "
			                    "stnd_aufp.kun, stnd_aufp.kun_fil, "
								"stnd_aufp.posi, stnd_aufp.a, a_bas.ag, a_bas.wg, "
								"a_bas.a_bz1 "
			                    "from stnd_aufp,a_bas "
								"where stnd_aufp.mdn = ? "
								"and   stnd_aufp.fil = ? "  
								"and   stnd_aufp.kun = ? "  
								"and   stnd_aufp.kun_fil = ? "
								"and a_bas.a = stnd_aufp.a "
								"order by 8,9");  
	}
}


int CClientOrder::Read ()
{
	int i = 0;
	int dsqlstatus = 100;
	int posi;
	AUFPS *Aufps;

	KunBran.Format (_T("kun%ld"), kun_nr);
    if ((dsqlstatus = StndAufp.sqlopen (cursor)) != 0)
	{
		return dsqlstatus;
	}

	if (ReadAufps ())
	{
		StndAufp.sqlclose (cursor); 
		return 0;
	}
    dsqlstatus = StndAufp.sqlfetch (cursor);

	if (dsqlstatus == 100)
	{
			if (stnd_aufp.kun_fil != 0) return dsqlstatus;


            StndAufp.sqlin  ((short *) &mdn, 1, 0);
            StndAufp.sqlin  ((short *) &fil, 1, 0);  
            StndAufp.sqlin  ((long *) &kun_nr, 2, 0);
            StndAufp.sqlout ((char *) kun_bran2, 0, 4);
			dsqlstatus = StndAufp.sqlcomm ("select kun_bran2 from kun "
					              "where mdn = ? "
								  "and fil = ? "
								  "and kun = ?");
			if (dsqlstatus == 100) return dsqlstatus;

			KunBran.Format (_T("bran%s"), kun_bran2);
			KunBran.Trim ();
			if (ReadAufps ())
			{
				StndAufp.sqlclose (cursor); 
				return 0;
			}

			kun_nr = atol (kun_bran2);
			kun_fil = 2;
            stnd_aufp.kun = kun_nr;        
            stnd_aufp.kun_fil = kun_fil;
            dsqlstatus = StndAufp.sqlopen (cursor); 
			if (dsqlstatus != 0)
			{
				return dsqlstatus;
			}
            dsqlstatus = StndAufp.sqlfetch (cursor); 
	}

	posi = 10;
    while (dsqlstatus == 0)
    {
			 Aufps = new AUFPS;   
			 memset (Aufps, 0, sizeof (AUFPS));
             StndAufp.dbreadfirsta ();
             sprintf (Aufps->a, "%13.0lf", stnd_aufp.a); 
             dsqlstatus = lese_a_bas (stnd_aufp.a);
             if (dsqlstatus == 0)
             {
                         strcpy (Aufps->a_bz1, _a_bas.a_bz1); 
             }
             else
             {
                         strcpy (Aufps->a_bz1, " "); 
             }
			 sprintf (Aufps->posi, "%d", posi);
			 FillRow (Aufps);
			 StndAll->Add (Aufps);
             dsqlstatus = StndAufp.sqlfetch (cursor); 
             i ++;
			 posi += 10;
    }
	return 0;
}

void CClientOrder::FillRow (AUFPS *Aufps)
{
   int dsqlstatus;
   char wert[sizeof (ptabn.ptwert)];
   short sa;
   double pr_ek;
   double pr_vk;

   if (DllPreise.PriceLib != NULL && 
	DllPreise.preise_holen != NULL)
   {
			  dsqlstatus = (DllPreise.preise_holen) (stnd_aufp.mdn, 
			                           0,
				                       stnd_aufp.kun_fil,
					                   stnd_aufp.kun,
						               stnd_aufp.a,
							           (LPSTR) lieferdat.c_str (),
								       &sa,
									   &pr_ek,
									   &pr_vk);
			  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
   }
   else
   {
				dsqlstatus = WaPreis.preise_holen (stnd_aufp.mdn,
                                      0,
                                      stnd_aufp.kun_fil,
                                      stnd_aufp.kun,
                                      stnd_aufp.a,
                                      (LPSTR) lieferdat.c_str (),
                                      &sa,
                                      &pr_ek,
                                      &pr_vk);
   }
   if (pr_ek != 0.0)
   {
	  sprintf (Aufps->auf_vk_pr, "%lf",  pr_ek);
	  sprintf (Aufps->sa_kz_sint, "%1d", sa);
   }
   if (pr_vk != 0.0)
   {
      sprintf (Aufps->auf_lad_pr,"%lf", pr_vk);
	  sprintf (Aufps->sa_kz_sint, "%1d", sa);
   }
   CalcLdPrPrc (Aufps, pr_ek, pr_vk);
   strcpy (Aufps->kond_art, WaPreis.GetKondArt ());
   strcpy (Aufps->kond_art0, WaPreis.GetKondArt ());
   sprintf (Aufps->a_grund, "%4.0lf", WaPreis.GetAGrund ());
   FillKondArt (Aufps);
   Einh.SetAufEinh (1);
   sprintf (Aufps->aufp_txt, "%ld", 0l);
   GetLastMe (Aufps);
   memcpy (&CronBest.cron_best, &cron_best_null, sizeof (CRON_BEST));
/*
   CronBest.cron_best.a = stnd_aufp.a;
   dsqlstatus = CronBest.dbreadfirst_a ();
   if (dsqlstatus == 0)
   {
	   sprintf (Aufps->bsd, "%.3lf", CronBest.cron_best.me);
   }
*/

   CronBest.cron_best.a = stnd_aufp.a;
   CronBest.cron_best.pr_kz = (int) BsdPrice;
   dsqlstatus = CronBest.dbreadfirst ();
   if (dsqlstatus == 0)
   {
	   sprintf (Aufps->bsd, "%.3lf", CronBest.cron_best.me);
   }

   CronBest.cron_best.pr_kz = (int) BsdNoPrice;
   dsqlstatus = CronBest.dbreadfirst ();
   if (dsqlstatus == 0)
   {
	   sprintf (Aufps->bsd2, "%.3lf", CronBest.cron_best.me);
   }

   Aufps->a_gew = _a_bas.a_gew;
   Aufps->dr_folge = _a_bas.dr_folge;
   sprintf (wert, "%hd", _a_bas.me_einh);
   sprintf (Aufps->me_einh, "%hd", _a_bas.me_einh);
   dsqlstatus = Ptab.lese_ptab ("me_einh", wert);
   strcpy (Aufps->basis_me_bz, ptabn.ptbezk);

   kumebest.mdn = aufk.mdn;
   kumebest.fil = aufk.fil;
   kumebest.kun = aufk.kun;
   strcpy (kumebest.kun_bran2, kun.kun_bran2);
   kumebest.a = ratod (Aufps->a);
   ReadMeEinh (Aufps);
}

void CClientOrder::FillKondArt (AUFPS *Aufps)
{
	   int dsqlstatus;

       memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));

       strcpy (ptabn.ptitem,"sap_kond_art");
	   strcpy (ptabn.ptwer1, Aufps->kond_art);

	   StndAufp.sqlin ((char *) ptabn.ptitem, 0, 19);
	   StndAufp.sqlin ((char *) ptabn.ptwer1, 0, 9);
	   StndAufp.sqlout ((char *) ptabn.ptwer2, 0, 9);
	   dsqlstatus = StndAufp.sqlcomm ("select ptwer2 from ptabn "
		                             "where ptitem = ? "
									 "and ptwer1 = ?");
       strcpy (Aufps->kond_art0, ptabn.ptwer1);
       strcpy (Aufps->kond_art,  ptabn.ptwer2);
}

void CClientOrder::GetLastMeAuf (AUFPS *Aufps)
/**
Letzte Bestellmenge des Kunden holen.
**/
{
	   double a; 
	   static int cursork = -1;
	   static int cursorp = -1;
	   static long auf;
	   static double auf_me;
	   static double pr_vk;
	   static double auf_me_ges;
	   static char ldat[12];

	   a = ratod (Aufps->a);
	   auf_me = 0.0;
	   pr_vk = 0.0;
	   auf_me_ges = 0.0;
	   memset (ldat, 0, sizeof (ldat));
	   if (cursork == -1)
	   {
		   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		   DbClass.sqlin ((long *)  &aufk.kun, 2, 0);
		   DbClass.sqlin ((long *)  &aufk.auf, 2, 0);
		   DbClass.sqlout ((long *)  &auf, 2, 0);
		   DbClass.sqlout ((char *)  ldat, 0, 11);
		   cursork = DbClass.sqlcursor ("select auf, lieferdat from aufk where mdn = ? "
															  "and fil = ? "
															  "and kun = ? "
															  "and auf != ? "
															  "order by lieferdat desc, "
															  "auf desc");

		   DbClass.sqlin ((short *) &aufk.mdn, 1, 0);
		   DbClass.sqlin ((short *) &aufk.fil, 1, 0);
		   DbClass.sqlin ((long *)  &auf, 2, 0);
		   DbClass.sqlin ((double *)  &a, 3, 0);
		   DbClass.sqlout ((double *) &auf_me, 3, 0);
		   DbClass.sqlout ((double *) &pr_vk, 3, 0);
		   cursorp = DbClass.sqlcursor ("select auf_me, auf_vk_euro from aufp "
										"where mdn = ? "
										"and fil = ? "
										"and auf = ? "
										"and a = ?");
	   }
	   auf_me_ges = (double) 0.0;

	   if (DbClass.sqlopen (cursork) != 0)
	   {
		   return;
	   }
       while (DbClass.sqlfetch (cursork) == 0)
	   {
		   if (DbClass.sqlopen (cursorp) != 0) break;
		   while (DbClass.sqlfetch (cursorp) == 0)
		   {
			   auf_me_ges += auf_me;
		   }
		   if (auf_me_ges != (double) 0.0) break;
	   }
	   sprintf (Aufps->last_me, "%.3lf", auf_me_ges);
	   sprintf (Aufps->last_ldat, "%s"  , ldat);
	   sprintf (Aufps->last_pr_vk,"%.3lf"  , pr_vk);
}


void CClientOrder::GetLastMe (AUFPS *Aufps)
/**
Letzte Bestellmenge des Kunden holen.
**/
{
	   double a; 
	   long auf;
	   double auf_me_ges;
	   double auf_vk_pr;

	   if (!LastFromAufKun)
	   {
			GetLastMeAuf (Aufps);
			return;
	   }

	   a = ratod (Aufps->a);

	   auf_me_ges = 0;
	   strcpy (Aufps->last_ldat, "");
	   aufkun.auf_me = (double) 0.0;
	   aufkun.mdn     = aufk.mdn;
	   aufkun.fil     = aufk.fil;
	   aufkun.kun     = aufk.kun;
	   aufkun.a       = a;
	   aufkun.kun_fil = aufk.kun_fil;

	   AufKun.dbreadlast ();

	   auf = aufkun.auf;
	   auf_me_ges = aufkun.auf_me;
	   auf_vk_pr = aufkun.auf_vk_pr;
	   dlong_to_asc (aufkun.lieferdat, Aufps->last_ldat );
	   while (AufKun.dbreadnextlast () == 0)
	   {
		   if (auf != aufkun.auf) break;
		   auf_me_ges += aufkun.auf_me;
		   auf_vk_pr = aufkun.auf_vk_pr;
	   }
	   if (auf_me_ges == (double) 0.0)
	   {
		   GetLastMeAuf (Aufps);
	   }
	   else
	   {
           sprintf (Aufps->last_me,     "%.3lf", auf_me_ges);
           sprintf (Aufps->last_pr_vk, "%.4lf", auf_vk_pr);
	   }
}

void CClientOrder::ReadMeEinh (AUFPS *Aufps)
/**
Mengeneinheiten holen.
**/
{
        int dsqlstatus;
        char ptwert [5];

        KEINHEIT keinheit;

        Einh.GetKunEinh (aufk.mdn, aufk.fil, aufk.kun,
                         _a_bas.a, &keinheit);
        strcpy (Aufps->basis_me_bz, keinheit.me_einh_bas_bez);
        strcpy (Aufps->me_bz, keinheit.me_einh_kun_bez);
        sprintf (Aufps->me_einh_kun, "%hd", keinheit.me_einh_kun);
        sprintf (Aufps->me_einh,     "%hd", keinheit.me_einh_bas);
        inh = keinheit.inh;
        sprintf (Aufps->me_einh_kun1, "%hd", keinheit.me_einh1);
        sprintf (Aufps->me_einh_kun2, "%hd", keinheit.me_einh2);
        sprintf (Aufps->me_einh_kun3, "%hd", keinheit.me_einh3);

        sprintf (Aufps->inh1, "%.3lf", keinheit.inh1);
        sprintf (Aufps->inh2, "%.3lf", keinheit.inh2);
        sprintf (Aufps->inh3, "%.3lf", keinheit.inh3);

        return;


        switch (_a_bas.a_typ)
        {
             case enumHndw :
                     dsqlstatus = Hndw.lese_a_hndw (_a_bas.a);
                     break;
             case enumEig :
                     dsqlstatus = Hndw.lese_a_eig (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig.me_einh_ek;
                     break;
             case enumEigDiv :
                     dsqlstatus = Hndw.lese_a_eig_div (_a_bas.a);
                     a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                     break;
             case enumDienst :
             case enumLeih :
                     a_hndw.me_einh_kun = _a_bas.me_einh;
                     break;
             default :
                     a_hndw.me_einh_kun = _a_bas.me_einh;
                     break;
        }

		Aufps->a_typ = _a_bas.a_typ;

        sprintf (ptwert, "%hd", _a_bas.me_einh);
        if (Ptab.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (Aufps->basis_me_bz, ptabn.ptbezk);
        }

        if (dsqlstatus) return;

        sprintf (ptwert, "%hd", a_hndw.me_einh_kun);
        if (Ptab.lese_ptab ("me_einh", ptwert) == 0)
        {
            strcpy (Aufps->me_bz, ptabn.ptbezk);
        }
}


void CClientOrder::CalcLdPrPrc (AUFPS *aufps, double vk_pr, double ld_pr)
{
	    double auf_lad_pr_prc = 0.0;
	    if (vk_pr != 0.0 && ld_pr != 0.0)
		{
			auf_lad_pr_prc = 100 - (vk_pr * 100 / ld_pr);	
		}
		sprintf (aufps->auf_lad_pr_prc,"%lf", auf_lad_pr_prc);
		GrpPrice.SetAKey (ratod (aufps->a));
//		GrpPrice.CalculateLdPr (vk_pr, aufps.marge, sizeof (aufps.marge), aufps->auf_lad_pr, sizeof (aufps.auf_lad_pr));
		GrpPrice.CalculateLdPr (vk_pr, aufps->marge, sizeof (aufps->marge), aufps->auf_lad_pr0, sizeof (aufps->auf_lad_pr0));
}


void CClientOrder::WriteAufps ()
{
	string msOutFileName;
	string msLine;
	fstream mfspOut;
	AUFPS **it;
	AUFPS *Aufps;
	CString Etc;
	CString FileName;

	Etc = getenv ("BWSETC");
	FileName.Format ("%s\\stnd", Etc.GetBuffer ());
	CreateDirectory (FileName.GetBuffer (), NULL);
	FileName += "\\";
	FileName += KunBran;
	msOutFileName = FileName.GetBuffer ();
	mfspOut.open (msOutFileName.c_str (), ios_base::out | ios_base::binary);
	if (!mfspOut.is_open ())
	{
		return;
	}
    StndAll->Start ();
	while ((it = StndAll->GetNext ()) != NULL)
	{
		Aufps = *it;
		mfspOut.write ((const char *) Aufps, sizeof (AUFPS));
	}
	mfspOut.close ();
}

BOOL CClientOrder::ReadAufps ()
{
	BOOL ret = FALSE;
	string msInFileName;
	string msLine;
	fstream mfspIn;
	AUFPS *Aufps;
	CString Etc;
	CString FileName;

	if (RD_OPTIMIZE == FromFile)
	{
		Etc = getenv ("BWSETC");
		FileName.Format ("%s\\stnd", Etc.GetBuffer ());
		CreateDirectory (FileName.GetBuffer (), NULL);
		FileName += "\\";
		FileName += KunBran;
		msInFileName = FileName.GetBuffer ();
		mfspIn.open (msInFileName.c_str (), ios_base::in | ios_base::binary);
		if (mfspIn.is_open ())
		{
			StndAll->DestroyElements ();
			StndAll->Destroy ();
			Aufps = new AUFPS;
			mfspIn.read ((char *) Aufps, sizeof (AUFPS));
			while (!mfspIn.eof ())
			{
				StndAll->Add (Aufps);
				Aufps = new AUFPS;
				mfspIn.read ((char *) Aufps, sizeof (AUFPS));
			}
			delete Aufps;
			mfspIn.close ();
			ret = TRUE;
		}
	}
	return ret;
}
