#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "auto_nr.h"

struct AUTO_NR auto_nr, auto_nr_null;

void AUTO_NR_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &auto_nr.mdn, 1, 0);
            ins_quest ((char *) &auto_nr.fil, 1, 0);
            ins_quest ((char *) &auto_nr.nr_nam, 0, 12);
            ins_quest ((char *) &auto_nr.nr_nr, 2, 0);
    out_quest ((char *) &auto_nr.mdn,1,0);
    out_quest ((char *) &auto_nr.fil,1,0);
    out_quest ((char *) auto_nr.nr_nam,0,11);
    out_quest ((char *) &auto_nr.nr_nr,2,0);
    out_quest ((char *) &auto_nr.satz_kng,1,0);
    out_quest ((char *) &auto_nr.max_wert,2,0);
    out_quest ((char *) auto_nr.nr_char,0,11);
    out_quest ((char *) &auto_nr.nr_char_lng,2,0);
    out_quest ((char *) auto_nr.fest_teil,0,11);
    out_quest ((char *) auto_nr.nr_komb,0,21);
    out_quest ((char *) &auto_nr.delstatus,1,0);
            cursor = prepare_sql ("select auto_nr.mdn,  "
"auto_nr.fil,  auto_nr.nr_nam,  auto_nr.nr_nr,  auto_nr.satz_kng,  "
"auto_nr.max_wert,  auto_nr.nr_char,  auto_nr.nr_char_lng,  "
"auto_nr.fest_teil,  auto_nr.nr_komb,  auto_nr.delstatus from auto_nr "

#line 29 "auto_nr.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   nr_nam = ? "
                                  "and   nr_nr = ? for update");

            ins_quest ((char *) &auto_nr.mdn, 1, 0);
            ins_quest ((char *) &auto_nr.fil, 1, 0);
            ins_quest ((char *) &auto_nr.nr_nam, 0, 12);
    out_quest ((char *) &auto_nr.mdn,1,0);
    out_quest ((char *) &auto_nr.fil,1,0);
    out_quest ((char *) auto_nr.nr_nam,0,11);
    out_quest ((char *) &auto_nr.nr_nr,2,0);
    out_quest ((char *) &auto_nr.satz_kng,1,0);
    out_quest ((char *) &auto_nr.max_wert,2,0);
    out_quest ((char *) auto_nr.nr_char,0,11);
    out_quest ((char *) &auto_nr.nr_char_lng,2,0);
    out_quest ((char *) auto_nr.fest_teil,0,11);
    out_quest ((char *) auto_nr.nr_komb,0,21);
    out_quest ((char *) &auto_nr.delstatus,1,0);
            cursor_1 = prepare_sql ("select auto_nr.mdn,  "
"auto_nr.fil,  auto_nr.nr_nam,  auto_nr.nr_nr,  auto_nr.satz_kng,  "
"auto_nr.max_wert,  auto_nr.nr_char,  auto_nr.nr_char_lng,  "
"auto_nr.fest_teil,  auto_nr.nr_komb,  auto_nr.delstatus from auto_nr "

#line 38 "auto_nr.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   nr_nam = ? "
                                  "and   satz_kng = 1 for update");

            ins_quest ((char *) &auto_nr.mdn, 1, 0);
            ins_quest ((char *) &auto_nr.fil, 1, 0);
            ins_quest ((char *) &auto_nr.nr_nam, 0, 12);
    out_quest ((char *) &auto_nr.mdn,1,0);
    out_quest ((char *) &auto_nr.fil,1,0);
    out_quest ((char *) auto_nr.nr_nam,0,11);
    out_quest ((char *) &auto_nr.nr_nr,2,0);
    out_quest ((char *) &auto_nr.satz_kng,1,0);
    out_quest ((char *) &auto_nr.max_wert,2,0);
    out_quest ((char *) auto_nr.nr_char,0,11);
    out_quest ((char *) &auto_nr.nr_char_lng,2,0);
    out_quest ((char *) auto_nr.fest_teil,0,11);
    out_quest ((char *) auto_nr.nr_komb,0,21);
    out_quest ((char *) &auto_nr.delstatus,1,0);
            cursor_2 = prepare_sql ("select auto_nr.mdn,  "
"auto_nr.fil,  auto_nr.nr_nam,  auto_nr.nr_nr,  auto_nr.satz_kng,  "
"auto_nr.max_wert,  auto_nr.nr_char,  auto_nr.nr_char_lng,  "
"auto_nr.fest_teil,  auto_nr.nr_komb,  auto_nr.delstatus from auto_nr "

#line 47 "auto_nr.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   nr_nam = ? "
                                  "and   satz_kng = 2 for update");

            ins_quest ((char *) &auto_nr.mdn, 1, 0);
            ins_quest ((char *) &auto_nr.fil, 1, 0);
            ins_quest ((char *) &auto_nr.nr_nam, 0, 12);
    out_quest ((char *) &auto_nr.mdn,1,0);
    out_quest ((char *) &auto_nr.fil,1,0);
    out_quest ((char *) auto_nr.nr_nam,0,11);
    out_quest ((char *) &auto_nr.nr_nr,2,0);
    out_quest ((char *) &auto_nr.satz_kng,1,0);
    out_quest ((char *) &auto_nr.max_wert,2,0);
    out_quest ((char *) auto_nr.nr_char,0,11);
    out_quest ((char *) &auto_nr.nr_char_lng,2,0);
    out_quest ((char *) auto_nr.fest_teil,0,11);
    out_quest ((char *) auto_nr.nr_komb,0,21);
    out_quest ((char *) &auto_nr.delstatus,1,0);
            cursor_0 = prepare_sql ("select auto_nr.mdn,  "
"auto_nr.fil,  auto_nr.nr_nam,  auto_nr.nr_nr,  auto_nr.satz_kng,  "
"auto_nr.max_wert,  auto_nr.nr_char,  auto_nr.nr_char_lng,  "
"auto_nr.fest_teil,  auto_nr.nr_komb,  auto_nr.delstatus from auto_nr "

#line 56 "auto_nr.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   nr_nam = ? for update");

    ins_quest ((char *) &auto_nr.mdn,1,0);
    ins_quest ((char *) &auto_nr.fil,1,0);
    ins_quest ((char *) auto_nr.nr_nam,0,11);
    ins_quest ((char *) &auto_nr.nr_nr,2,0);
    ins_quest ((char *) &auto_nr.satz_kng,1,0);
    ins_quest ((char *) &auto_nr.max_wert,2,0);
    ins_quest ((char *) auto_nr.nr_char,0,11);
    ins_quest ((char *) &auto_nr.nr_char_lng,2,0);
    ins_quest ((char *) auto_nr.fest_teil,0,11);
    ins_quest ((char *) auto_nr.nr_komb,0,21);
    ins_quest ((char *) &auto_nr.delstatus,1,0);
            sqltext = "update auto_nr set auto_nr.mdn = ?,  "
"auto_nr.fil = ?,  auto_nr.nr_nam = ?,  auto_nr.nr_nr = ?,  "
"auto_nr.satz_kng = ?,  auto_nr.max_wert = ?,  auto_nr.nr_char = ?,  "
"auto_nr.nr_char_lng = ?,  auto_nr.fest_teil = ?,  "
"auto_nr.nr_komb = ?,  auto_nr.delstatus = ? "

#line 61 "auto_nr.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   nr_nam = ? "
                                  "and   nr_nr = ? "
                                  "and   satz_kng  = ?";

            ins_quest ((char *) &auto_nr.mdn, 1, 0);
            ins_quest ((char *) &auto_nr.fil, 1, 0);
            ins_quest ((char *) &auto_nr.nr_nam, 0, 12);
            ins_quest ((char *) &auto_nr.nr_nr, 2, 0);
            ins_quest ((char *) &auto_nr.satz_kng, 1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &auto_nr.mdn, 1, 0);
            ins_quest ((char *) &auto_nr.fil, 1, 0);
            ins_quest ((char *) &auto_nr.nr_nam, 0, 12);
            ins_quest ((char *) &auto_nr.nr_nr, 2, 0);
            ins_quest ((char *) &auto_nr.satz_kng, 1, 0);
            test_upd_cursor = prepare_sql ("select nr_nr from auto_nr "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   nr_nam = ? "
                                  "and   nr_nr = ? "
                                  "and   satz_kng  = ?");


    ins_quest ((char *) &auto_nr.mdn,1,0);
    ins_quest ((char *) &auto_nr.fil,1,0);
    ins_quest ((char *) auto_nr.nr_nam,0,11);
    ins_quest ((char *) &auto_nr.nr_nr,2,0);
    ins_quest ((char *) &auto_nr.satz_kng,1,0);
    ins_quest ((char *) &auto_nr.max_wert,2,0);
    ins_quest ((char *) auto_nr.nr_char,0,11);
    ins_quest ((char *) &auto_nr.nr_char_lng,2,0);
    ins_quest ((char *) auto_nr.fest_teil,0,11);
    ins_quest ((char *) auto_nr.nr_komb,0,21);
    ins_quest ((char *) &auto_nr.delstatus,1,0);
            sqltext = "update auto_nr set auto_nr.mdn = ?,  "
"auto_nr.fil = ?,  auto_nr.nr_nam = ?,  auto_nr.nr_nr = ?,  "
"auto_nr.satz_kng = ?,  auto_nr.max_wert = ?,  auto_nr.nr_char = ?,  "
"auto_nr.nr_char_lng = ?,  auto_nr.fest_teil = ?,  "
"auto_nr.nr_komb = ?,  auto_nr.delstatus = ? "

#line 88 "auto_nr.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   nr_nam = ? "
                                  "and   satz_kng  = 2";

            ins_quest ((char *) &auto_nr.mdn, 1, 0);
            ins_quest ((char *) &auto_nr.fil, 1, 0);
            ins_quest ((char *) &auto_nr.nr_nam, 0, 12);
            upd_cursor_2 = prepare_sql (sqltext);

            ins_quest ((char *) &auto_nr.mdn, 1, 0);
            ins_quest ((char *) &auto_nr.fil, 1, 0);
            ins_quest ((char *) &auto_nr.nr_nam, 0, 12);
            test_upd_cursor_2 = prepare_sql ("select nr_nr from auto_nr "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   nr_nam = ? "
                                  "and   satz_kng  = 2");

            ins_quest ((char *) &auto_nr.mdn, 1, 0);
            ins_quest ((char *) &auto_nr.fil, 1, 0);
            ins_quest ((char *) &auto_nr.nr_nam, 0, 12);
            ins_quest ((char *) &auto_nr.nr_nr, 2, 0);
            ins_quest ((char *) &auto_nr.satz_kng, 1, 0);
            del_cursor = prepare_sql ("delete from auto_nr "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   nr_nam = ? "
                                  "and   nr_nr = ? "
                                  "and   satz_kng  = ?");

            ins_quest ((char *) &auto_nr.mdn, 1, 0);
            ins_quest ((char *) &auto_nr.fil, 1, 0);
            ins_quest ((char *) &auto_nr.nr_nam, 0, 12);
            cursor_del_nr = prepare_sql ("delete from auto_nr "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   nr_nam = ?");

    ins_quest ((char *) &auto_nr.mdn,1,0);
    ins_quest ((char *) &auto_nr.fil,1,0);
    ins_quest ((char *) auto_nr.nr_nam,0,11);
    ins_quest ((char *) &auto_nr.nr_nr,2,0);
    ins_quest ((char *) &auto_nr.satz_kng,1,0);
    ins_quest ((char *) &auto_nr.max_wert,2,0);
    ins_quest ((char *) auto_nr.nr_char,0,11);
    ins_quest ((char *) &auto_nr.nr_char_lng,2,0);
    ins_quest ((char *) auto_nr.fest_teil,0,11);
    ins_quest ((char *) auto_nr.nr_komb,0,21);
    ins_quest ((char *) &auto_nr.delstatus,1,0);
            ins_cursor = prepare_sql ("insert into auto_nr ("
"mdn,  fil,  nr_nam,  nr_nr,  satz_kng,  max_wert,  nr_char,  nr_char_lng,  fest_teil,  "
"nr_komb,  delstatus) "

#line 128 "auto_nr.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?)"); 

#line 130 "auto_nr.rpp"
}
int AUTO_NR_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int AUTO_NR_CLASS::dbreadfirst_0 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (cursor_0 == -1)
         {  
                this->prepare ();
         }
         open_sql (cursor_0);
         fetch_sql (cursor_0);
         return (sqlstatus);
}

int AUTO_NR_CLASS::dbread_0 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         fetch_sql (cursor_0);
         return (sqlstatus);
}


int AUTO_NR_CLASS::dbreadfirst_1 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (cursor_1 == -1)
         {  
                this->prepare ();
         }
         open_sql (cursor_1);
         fetch_sql (cursor_1);
         return (sqlstatus);
}

int AUTO_NR_CLASS::dbread_1 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         fetch_sql (cursor_1);
         return (sqlstatus);
}

int AUTO_NR_CLASS::dbreadfirst_2 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (cursor_2 == -1)
         {  
                this->prepare ();
         }
         open_sql (cursor_2);
         fetch_sql (cursor_2);
         return (sqlstatus);
}

int AUTO_NR_CLASS::dbread_2 (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         fetch_sql (cursor_2);
         return (sqlstatus);
}

int AUTO_NR_CLASS::dbupdate_2 (void)
/**
Eintrag fuer nr_nam loeschen.
**/
{
         open_sql (test_upd_cursor_2); 
         dsqlstatus = fetch_sql (test_upd_cursor_2); 
         if (dsqlstatus == 100)
         {
                   execute_curs (ins_cursor);
         }  
         else if (sqlstatus == 0)
         {
                   execute_curs (upd_cursor_2);
         }  
         return sqlstatus;
}



int AUTO_NR_CLASS::dbdelete_nr_nam (void)
/**
Eintrag fuer nr_nam loeschen.
**/
{
         if (cursor_del_nr == -1)
         {  
                this->prepare ();
         }
         execute_curs (cursor_del_nr);
         return sqlstatus;
}