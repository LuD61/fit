// ListHandler.cpp: Implementierung der Klasse CListHandler.
//
//////////////////////////////////////////////////////////////////////
#include <string>
#include "strfkt.h"
#include "ListHandler.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////


int CListHandler::CIdxCollection::GetIdxAll (int IdxValue)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxValue == IdxValue)
		{
			return Item->IdxAll;
		}
	}
	return -1;
}

int CListHandler::CIdxCollection::GetIdxValue (int IdxAll)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxAll == IdxAll)
		{
			return Item->IdxValue;
		}
	}
	return -1;
}


CListHandler::CIdxItem *CListHandler::CIdxCollection::GetItemByAll (int IdxAll)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxAll == IdxAll)
		{
			return Item;
		}
	}
	return NULL;
}

CListHandler::CIdxItem *CListHandler::CIdxCollection::GetItemByValue (int IdxValue)
{
	CIdxItem **it;
	CIdxItem *Item;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		Item = *it;
		if (Item->IdxValue == IdxValue)
		{
			return Item;
		}
	}
	return NULL;
}


CListHandler *CListHandler::instance = NULL;

CListHandler *CListHandler::GetInstance ()
{
	if (instance == NULL)
	{
		instance = new CListHandler ();
	}
	return instance;
}

void CListHandler::DestroyInstance ()
{
	if (instance != NULL)
	{
		delete instance;
	}
}

CListHandler::CListHandler()
{
	ActiveMode = AllEntries;
	LoadComplete = FALSE;
	enableActivate = FALSE;
	activated = FALSE;
}

CListHandler::~CListHandler()
{
	Init ();
}

void CListHandler::Init ()
{
	StndAll.DestroyElements ();
	StndAll.Destroy ();
	StndValue.DestroyElements ();
	StndValue.Destroy ();
	IdxCollection.DestroyElements ();
	IdxCollection.Destroy ();
	LoadComplete = FALSE;
	enableActivate = FALSE;
	activated = FALSE;
}

void CListHandler::InitAufMe ()
{
	AUFPS **it;
	AUFPS *Item;

	StndAll.Start ();
	while ((it = StndAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			strcpy (Item->auf_me, "0.000");
		}
	}
	activated = FALSE;
}


void CListHandler::SetNewPosi () //FS-126
{
	AUFPS **it;
	AUFPS *Item;

	int dposi = 10;
	StndValue.Start ();
	while ((it = StndValue.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			sprintf (Item->posi, "%ld", dposi);
			dposi += 10;
		}
	}
}

int CListHandler::HoleStndValue (AUFPS aufparr[MAXPOS], int *idx) //FS-126
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
		ListCount = StndValue.anz;
		if (ListCount == 0)
		{
//			return StndAll.anz;
			return 0;
		}
		for (int i = 0; i < ListCount; i ++) 
		{
			it = StndValue.Get (i);
			if (it != NULL)
			{
				Aufps = *it;
				memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
			}
		}
		*idx = IdxCollection.GetIdxValue (*idx);
	return ListCount;
}


int  CListHandler::Activate (AUFPS aufparr[MAXPOS], int aufpanz, int *idx)
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	int i = 0;
	ListCount = StndAll.anz;

	if (ListCount == 0)
	{
		return aufpanz;
	}

	for (i = 0; i < aufpanz; i ++) 
	{
		SetStndAllA (&aufparr[i]);
	}
	ListCount = StndAll.anz;
	for (i = 0; i < ListCount; i ++) 
	{
		it = StndAll.Get (i);
		if (it != NULL)
		{
			Aufps = *it;
			memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
		}
	}
    activated = TRUE;
	return ListCount;
}

void CListHandler::SetStndAllA (AUFPS *Aufps)
{
	AUFPS **it;
	AUFPS *Item;
	int posi;
	int idx = 0;
	BOOL found = FALSE;

	if (ratod (Aufps->a) == 0.0 || ratod (Aufps->auf_me) == 0.0)
	{
		return;
	}

	StndAll.Start ();
	while ((it = StndAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a))
			{
				posi = atoi (Item->posi);
				memcpy (Item, Aufps, sizeof (AUFPS));
				sprintf (Item->posi, "%d", posi);
				if (ratod (Aufps->auf_me) != 0.0)
				{
					SetStndValueA (Aufps, posi, idx);
				}
				else
				{
					DropStndValueA (Aufps, idx);
				}
				found = TRUE;
				break;
			}
		}
		idx ++;
	}
	if (!found)
	{
		Item = new AUFPS;
		posi = atoi (Aufps->posi);
		memcpy (Item, Aufps, sizeof (AUFPS));
		StndAll.Add (Item);

		if (ratod (Aufps->auf_me) != 0.0)
		{
			SetStndValueA (Aufps, posi, idx);
		}
		else
		{
			DropStndValueA (Aufps, idx);
		}
	}
}


void CListHandler::SetStndValueA (AUFPS *Aufps, int posi, int idxAll)
{
	AUFPS **it;
	AUFPS *Item;
	int idxValue;

	BOOL found = FALSE;
	if (ratod (Aufps->auf_me) != 0.0)
	{
		StndValue.Start ();
		while ((it = StndValue.GetNext ()) != NULL)
		{
			Item = *it;
			if (Item != NULL)
			{
				if (ratod (Item->a) == ratod (Aufps->a))
				{
					memcpy (Item, Aufps, sizeof (AUFPS));
					sprintf (Item->posi, "%d", posi);
					found = TRUE;
					break;
				}
			}
		}
		if (!found)
		{
			Item = new AUFPS;
			memcpy (Item, Aufps, sizeof (AUFPS));
			sprintf (Item->posi, "%d", posi);
			idxValue = StndValue.anz;
			StndValue.Add (Item);
			CIdxItem *item = new CIdxItem (idxAll, idxValue);
			IdxCollection.Add (item);
		}
	}
}

void CListHandler::DropStndValueA (AUFPS *Aufps, int idxAll)
{
	AUFPS **it;
	AUFPS *Item;
//	int idxValue;
	int i = 0;

	BOOL found = FALSE;
	StndValue.Start ();
	while ((it = StndValue.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a))
			{
				StndValue.Drop (i);
				CIdxItem *IdxItem = IdxCollection.GetItemByAll (idxAll);
				if (IdxItem != NULL)
				{
					IdxCollection.Drop (IdxItem);
				}
				found = TRUE;
				break;
			}
		}
		i ++;
	}
}

int CListHandler::GetAllPosiA (AUFPS *Aufps)
{
	AUFPS **it;
	AUFPS *Item;
	int posi;

	StndAll.Start ();
	while ((it = StndAll.GetNext ()) != NULL)
	{
		Item = *it;
		if (Item != NULL)
		{
			if (ratod (Item->a) == ratod (Aufps->a))
			{
				posi = atoi (Item->posi);
				return posi;
			}
		}
	}
	return 10;
}

void CListHandler::Update (int idx, AUFPS *aufps)
{
//	AUFPS **it;
//	AUFPS *Aufps;
//	int idxAll;
//	int idxValue;

	/** LS-126
	if (!activated)
	{
		return;
	}
	**/

	if (ratod (aufps->a) == 0.0)
	{
		return;
	}

	SetStndAllA (aufps);
	SortByPosi (&StndAll); 
	SortByPosi (&StndValue); 

/*
	if (ActiveMode == AllEntries)
	{
		idxAll = idx;
		it = StndAll.Get (idxAll);
		if (it != NULL)
		{
			Aufps = *it;
			memcpy (Aufps, aufps, sizeof (AUFPS));
		}
		it = NULL;
		idxValue = IdxCollection.GetIdxValue (idxAll);
		if (idxValue != -1)
		{
			it = StndValue.Get (idxValue);
		}
		if (it != NULL)
		{
			Aufps = *it;
            if (ratod (aufps->auf_me) != 0.0)
			{
				memcpy (Aufps, aufps, sizeof (AUFPS));
			}
			else
			{
				StndValue.Drop (idxValue);
				delete Aufps;
				CIdxItem *item = IdxCollection.GetItemByAll (idxAll);
				if (item != NULL)
				{
					IdxCollection.Drop (item);
					delete item;
				}
			}
		}
		else
		{
            if (ratod (aufps->auf_me) != 0.0)
			{
				Aufps = new AUFPS;
				memcpy (Aufps, aufps, sizeof (AUFPS));
				idxValue = StndValue.anz;
				StndValue.Add (Aufps);
				CIdxItem *item = new CIdxItem (idxAll, idxValue);
				IdxCollection.Add (item);
			}

		}
	}
	else
	{
		idxValue = idx;
		it = StndValue.Get (idxValue);
		if (it != NULL)
		{
			Aufps = *it;
			memcpy (Aufps, aufps, sizeof (AUFPS));
		}
		it = NULL;
		idxAll = IdxCollection.GetIdxAll (idxValue);
		if (idxAll != -1)
		{
			it = StndAll.Get (idxAll);
		}
		if (it != NULL)
		{
			Aufps = *it;
			memcpy (Aufps, aufps, sizeof (AUFPS));
		}
	}
*/
} 

int CListHandler::Switch (AUFPS aufparr[MAXPOS], int *idx)
{
	AUFPS **it;
	AUFPS *Aufps;
	int ListCount = 0;
	if (ActiveMode == AllEntries)
	{
		ListCount = StndValue.anz;
		if (ListCount == 0)
		{
			return StndAll.anz;
		}
		for (int i = 0; i < ListCount; i ++) 
		{
			it = StndValue.Get (i);
			if (it != NULL)
			{
				Aufps = *it;
				memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
			}
		}
		*idx = IdxCollection.GetIdxValue (*idx);
		ActiveMode = ValueEntries;
	}
	else
	{
		ListCount = StndAll.anz;
		for (int i = 0; i < ListCount; i ++) 
		{
			it = StndAll.Get (i);
			if (it != NULL)
			{
				Aufps = *it;
				memcpy (&aufparr[i], Aufps, sizeof (AUFPS));
			}
		}
		*idx = IdxCollection.GetIdxAll (*idx);
		ActiveMode = AllEntries;
	}
	return ListCount;
}

double CListHandler::GetSmtPart ()
{
	double part = 0.0;
	if (StndAll.anz > 0.0 && StndValue.anz > 0.0)
	{
		part = (double) StndValue.anz * 100 / StndAll.anz;
	}
	return part;
}

void CListHandler::SortByPosi (CDataCollection<AUFPS *> *StndCollection)
{
   qsort (StndCollection->Arr, StndCollection->anz, sizeof (AUFPS *),
			   ComparePosi);

}

int CListHandler::ComparePosi (const void *el1, const void *el2)
{
	int ret = 0;
	AUFPS *Aufps1;
	AUFPS *Aufps2;

	Aufps1 = *((AUFPS **) el1);
	Aufps2 = *((AUFPS **) el2);

	ret = (int) (atol (Aufps1->posi) -  atol (Aufps2->posi));

	return ret;
}
