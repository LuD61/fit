#ifndef _AUFPT_DEF
#define _AUFPT_DEF

#include "dbclass.h"

struct AUFPT {
   long      nr;
   long      zei;
   char      txt[61];
};
extern struct AUFPT aufpt, aufpt_null;

#line 7 "aufpt.rh"

class AUFPT_CLASS : public DB_CLASS 
{
       private :
               int del_cursor_posi;
               void prepare (void);
       public :
               AUFPT_CLASS () : DB_CLASS ()
               {
                         del_cursor_posi = -1;
               }
               int dbreadfirst (void);
               int delete_aufpposi (void); 
};
#endif
