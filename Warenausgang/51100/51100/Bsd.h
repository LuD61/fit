#ifndef CBSD_DEF
#define CBSD_DEF
#include "DbClass.h"
#include "sys_par.h"

class CBsd
{
private:
	int zentrallager;
public:
	DB_CLASS DbClass;
	SYS_PAR_CLASS sys_par_class;
	int BsdCursor;
	int BestResCursor;
	int BsdBuchWaCursor;
	int BsdBuchWeCursor;
	short mdn;
	double a;
	double bsd;
	double bsd_res;
	double bsd_buchwa;
	double bsd_buchwe;
	BOOL UseOrders;
	CBsd ()
	{
		a = 0.0;
		bsd = 0.0;
		bsd_res = 0.0;
		bsd_buchwa = 0.0;
		bsd_buchwe = 0.0;
		BsdCursor = -1;
		BestResCursor = -1;
		BsdBuchWaCursor = -1;
		BsdBuchWeCursor = -1;
		UseOrders = FALSE;
		zentrallager = -1;
	}

	~CBsd ()
	{
		Close ();
	}
	void Prepare ();
	void Close ();
	double Calculate ();
	double Calculate (short mdn, double a);
};
#endif