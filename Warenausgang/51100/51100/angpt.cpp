#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "angpt.h"

struct ANGPT angpt, angpt_null;

void ANGPT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &angpt.nr, 2, 0);
    out_quest ((char *) &angpt.nr,2,0);
    out_quest ((char *) &angpt.zei,2,0);
    out_quest ((char *) angpt.txt,0,61);
            cursor = prepare_sql ("select angpt.nr,  angpt.zei,  "
"angpt.txt from angpt "

#line 26 "angpt.rpp"
                                  "where nr = ? "
                                  "order by zei");

    ins_quest ((char *) &angpt.nr,2,0);
    ins_quest ((char *) &angpt.zei,2,0);
    ins_quest ((char *) angpt.txt,0,61);
            sqltext = "update angpt set angpt.nr = ?,  "
"angpt.zei = ?,  angpt.txt = ? "

#line 30 "angpt.rpp"
                                  "where nr = ? "
                                  "and zei  = ?";

            ins_quest ((char *) &angpt.nr, 2, 0);
            ins_quest ((char *) &angpt.zei, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &angpt.nr, 2, 0);
            ins_quest ((char *) &angpt.zei, 2, 0);
            test_upd_cursor = prepare_sql ("select nr from angpt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &angpt.nr, 2, 0);
            ins_quest ((char *) &angpt.zei, 2, 0);
            del_cursor = prepare_sql ("delete from angpt "
                                  "where nr = ? "
                                  "and   zei  = ?");

            ins_quest ((char *) &angpt.nr, 2, 0);
            del_cursor_posi = prepare_sql ("delete from angpt "
                                  "where  nr = ?");

    ins_quest ((char *) &angpt.nr,2,0);
    ins_quest ((char *) &angpt.zei,2,0);
    ins_quest ((char *) angpt.txt,0,61);
            ins_cursor = prepare_sql ("insert into angpt (nr,  "
"zei,  txt) "

#line 54 "angpt.rpp"
                                      "values "
                                      "(?,?,?)"); 

#line 56 "angpt.rpp"
            out_quest ((char *) &angpt.nr, 2, 0);
            max_cursor = prepare_sql ("select max (nr) from angpt");   
}
int ANGPT_CLASS::dbreadfirst (void)
/**

Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int ANGPT_CLASS::delete_angpposi (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (del_cursor_posi == -1)
         {  
                this->prepare ();
         }
         execute_curs (del_cursor_posi);
         return (sqlstatus);
}

void ANGPT_CLASS::dbclose (void)
{
         if (del_cursor_posi != -1)
	   {
		     close_sql (del_cursor_posi);
		     del_cursor_posi = -1;
	   }
         if (max_cursor != -1)
	   {
		     close_sql (max_cursor);
		     max_cursor = -1;
	   }
	   DB_CLASS::dbclose ();
}

long ANGPT_CLASS::GetMaxNr ()
{
	if (max_cursor == -1)
	{
		prepare ();
      	}
        
	angpt.nr = 0;
        open_sql (max_cursor);
        fetch_sql (max_cursor);
        return angpt.nr;
}
