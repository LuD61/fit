#ifndef _KUNLIEF_DEF
#define _KUNLIEF_DEF

#include "windows.h"
#include "mo_draw.h"
#include "wmaskc.h"
#include "dbclass.h"

struct KUNLIEF {
   short     mdn;
   short     fil;
   long      kun;
   long      adr;
   char      kun_krz2[17];
   short     sprache;
   long      tou;
   char      vers_art[3];
   short     lief_art;
   char      fra_ko_ber[3];
   char      form_typ1[3];
   short     auflage1;
   long      ls_fuss_txt;
   long      ls_kopf_txt;
   char      gn_pkt_kz[2];
   char      sw_rab[2];
   short     kun_leer_kz;
   char      iln[17];
   char      plattform[17];
   char      be_log[4];
   long      stat_kun;
};
extern struct KUNLIEF kunlief, kunlief_null;

#line 10 "kunlief.rh"

class KUNLIEF_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               KUNLIEF_CLASS () : DB_CLASS ()
               {
               }
               ~KUNLIEF_CLASS ()
               {
                   dbclose ();
               } 
               int dbreadfirst (void);
};
#endif
