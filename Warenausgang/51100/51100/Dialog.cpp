// Dialog.cpp: Implementierung der Klasse CDialog.
//
//////////////////////////////////////////////////////////////////////

#include "Dialog.h"
#include "Application.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CDialog *CDialog:: Instance = NULL;

vector<CDialogInstance *> CDialog::m_Instances;

CDialog::CDialog(DWORD Id, HWND Parent)
{
	this->Id = Id;
	this->Parent = Parent;
	ret = FALSE;
	m_SetPos = FALSE;
	DialogType = CreateMode;
	Instance = this;
}

CDialog::~CDialog()
{
	CDialogItem *item;
	for (vector<CDialogItem *>::iterator it = m_Controls.begin (); it != m_Controls.end (); ++it)
	{
		item = *it;
		if (item != NULL)
		{
			delete item;
		}
	}
	m_Controls.clear ();
	DropInstance ();
}

void CDialog::SetDlgPos (HWND hWnd)
{
	RECT rect;
	POINT p;

	GetWindowRect (hWnd, &rect);
	p.x = rect.right + 1;
	p.y = rect.bottom + 1;
	if (Parent != NULL)
	{
//		ScreenToClient (Parent, &p);
	}
	m_X = p.x;
	m_Y = p.y;
	m_SetPos = TRUE;
}

void CDialog::AttachControls ()
{

}

BOOL CDialog::OnInitDialog ()
{
	AttachControls ();
	return TRUE;
}

HBRUSH CDialog::OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor)
{
	return NULL;
}

void CDialog::Display ()
{
	CDialogItem *item;
	for (vector<CDialogItem *>::iterator it = m_Controls.begin (); it != m_Controls.end (); ++it)
	{
		item = *it;
		if (item != NULL)
		{
			item->PutValue ();
		}
	}
}

void CDialog::Get ()
{
	CDialogItem *item;
	for (vector<CDialogItem *>::iterator it = m_Controls.begin (); it != m_Controls.end (); ++it)
	{
		item = *it;
		if (item != NULL)
		{
			item->GetValue ();
		}
	}
}

void CDialog::EndDialog(HWND m_hWnd, WPARAM wParam)
{
	if (DialogType == BoxMode)
	{
		::EndDialog(m_hWnd, wParam); 
	}
	else
	{
         PostQuitMessage (0);
	}
}


void CDialog::OnOK ()
{
	ret = IDOK;
	EndDialog(m_hWnd, wParam); 
}

void CDialog::OnCancel ()
{
	ret = IDCANCEL;
	EndDialog(m_hWnd, wParam); 
}

BOOL CDialog::PreTranslateMessage (MSG *msg)
{
	return FALSE;
}

void CDialog::AddInstance ()
{
	if (DialogType == CreateMode && m_hWnd != NULL)
	{

		for (vector<CDialogInstance *>::iterator it = m_Instances.begin (); it != m_Instances.end (); ++it)
		{
			CDialogInstance *instance = *it;
			if (instance != NULL)
			{
				if (this == (CDialog *) instance->m_Instance)
				{
					return;
				}
			}
		}
		m_Instances.push_back (new CDialogInstance (m_hWnd, this));
	}
}

void CDialog::DropInstance ()
{
	if (DialogType == CreateMode)
	{
		for (vector<CDialogInstance *>::iterator it = m_Instances.begin (); it != m_Instances.end (); ++it)
		{
			CDialogInstance *instance = *it;
			if (instance != NULL)
			{
				if (this == (CDialog *) instance->m_Instance)
				{
					m_Instances.erase (it);
				}	break;
			}
		}
	}
}

BOOL CDialog::DoModal ()
{
    MSG msg;
	BOOL IsDlg;
	HINSTANCE hInstance = CApplication::GetInstance ()->GetAppInstance ();


	if (DialogType == BoxMode)
	{
		DialogBox(hInstance,
				  MAKEINTRESOURCE (Id),
				  Parent,
				  ((DLGPROC) DlgProc));
	}
	else
	{
		if (Parent != NULL)
		{
			EnableWindow (Parent, FALSE);
		}
		m_hWnd = CreateDialog( hInstance, 
 							   MAKEINTRESOURCE (Id),
							   Parent,
							   ((DLGPROC) DlgProc));
		if (m_SetPos)
		{
			SetWindowPos (m_hWnd, NULL, m_X, m_Y, 0, 0, SWP_NOSIZE | SWP_NOZORDER); 
		}
		ShowWindow (m_hWnd, SW_SHOWNORMAL);
		UpdateWindow (m_hWnd);
 
		while (GetMessage (&msg, NULL, 0, 0))
		{
			 if (!PreTranslateMessage (&msg))
			 {
				 IsDlg = IsDialogMessage (m_hWnd, &msg);
				 TranslateMessage(&msg);
				 DispatchMessage(&msg);
			 }
		}
		EnableWindow (Parent, TRUE);
		DestroyWindow (m_hWnd);
		m_hWnd = NULL;
	}
	return ret;
}

CDialog *CDialog::GetInstance (HWND hWnd, CDialog *DefInstance)
{
	CDialog *inst = DefInstance;

	for (vector<CDialogInstance *>::iterator it = m_Instances.begin (); it != m_Instances.end (); ++it)
	{
		CDialogInstance *instance = *it;
		if (instance != NULL)
		{
			if (instance->m_hWnd == hWnd)
			{
				inst = (CDialog *) instance->m_Instance;
			}	break;
		}
	}
	return inst;
}

DWORD CALLBACK CDialog::DlgProc (HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{
	CDialog *inst;
	inst = GetInstance (hwndDlg, Instance);
    switch (message) 
    { 
        case WM_INITDIALOG:
			inst->m_hWnd = hwndDlg;
			inst->AddInstance ();
			return inst->OnInitDialog ();
        case WM_COMMAND: 
            switch (LOWORD(wParam)) 
			{
				case IDOK :
					inst->ret = TRUE; 
					inst->wParam = wParam;
					inst->OnOK ();
					return TRUE; 
				case IDCANCEL :
					inst->wParam = wParam;
					inst->OnCancel ();
					return TRUE; 
			}
			break;
		case WM_CTLCOLORDLG :
			return (DWORD) inst->OnCtlColor ((HDC) wParam, (HWND) lParam, CTLCOLOR_DLG);
		case WM_CTLCOLORSTATIC :
			return (DWORD) inst->OnCtlColor ((HDC) wParam, (HWND) lParam, CTLCOLOR_STATIC);
        case WM_SYSCOMMAND: 
            switch (wParam)
			{
				case SC_CLOSE:
					inst->EndDialog(hwndDlg, wParam); 
					return TRUE; 
			}
			break;
	}
	return NULL;
}
