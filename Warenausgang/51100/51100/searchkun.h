#ifndef _SEARCHKUN_DEF
#define _SEARCHKUN_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

struct SKUN
{
	  char kun [15];
	  char adr_nam1 [38];
	  char plz [22];
	  char ort1 [38];
      char kun_krz1[18];
};

class SEARCHKUN
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static struct SKUN *skuntab;
           static struct SKUN skun;
           static int idx;
           static long kunanz;
           static CHQEX *Query;
           static int SearchField;
           static int sokun; 
           static int soadr_nam1; 
           static int soplz; 
           static int soort1; 
           static int sokun_krz1; 
           static short mdn;
           static char *query;

           int SearchPos;
           int OKPos;
           int CAPos;
           DWORD WindowStyle;
           BOOL SetStyle;
        public :
           SEARCHKUN ()
           {
                  SearchPos = 8;
                  OKPos = 9;
                  CAPos = 10;
                  SetStyle = FALSE;
                  mdn = 0;
                  query = NULL;
                  WindowStyle = WS_POPUP | WS_THICKFRAME;
           }

           ~SEARCHKUN ()
           {
                  if (skuntab != NULL)
                  {
                      delete skuntab;
                      skuntab = NULL;
                  }
                  if (Query != NULL)
                  {
                      delete Query;
                      Query = NULL;
                  }
           }

           SKUN *GetSkun (void)
           {
               if (idx == -1) return NULL;
               return &skun;
           }

           void SetQuery (char *query)
           {
               this->query = query;
           }

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetWindowStyle (DWORD style)
           {
               WindowStyle = style;
               SetStyle = TRUE;
           }


           void FillBox (void);
           SKUN *GetNextSkun (void);
           SKUN *GetPriorSkun (void);
           SKUN *GetFirstSkun (void);
           SKUN *GetLastSkun (void);
           static void FillFormat (char *);
           static void FillVlines (char *);
           static void FillCaption (char *);
           static void FillRec (char *, int);
           static int sortkun (const void *, const void *);
           static void SortKun (HWND);
           static int sortadr_nam1 (const void *, const void *);
           static void SortAdr_nam1 (HWND);
           static int sortplz (const void *, const void *);
           static void SortPlz (HWND);
           static int sortort1 (const void *, const void *);
           static void SortOrt1 (HWND);
           static int sortkun_krz1 (const void *, const void *);
           static void SortKunKrz1 (HWND);
           static void SortLst (int, HWND);
           static void UpdateList (void);

           static int SearchLst (char *);
           static int ReadKun (char *);
           void SetParams (HINSTANCE, HWND);
           void SetSearchTxt (char *);
           void SearchKun (short, char *, char *);
};  
#endif