#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "message.h"
#include "ls.h"
#include "kun.h"
#include "mdn.h"
#include "fil.h"
#include "mo_curso.h"
#include "sys_par.h"
#include "dbclass.h"
#include "mo_last.h"

#define MAXWAIT 60

static LS_CLASS ls_class;
static SYS_PAR_CLASS sys_par_class;
static DB_CLASS DbClass;
extern MELDUNG Mess;

static int dsqlstatus;
static long auf;
static double a;

void LastAuf::arttotab (long auf, double a)
/**
Artikel und Auftrag in Matrix einfuegen.
**/
{
         int i;

         for (i = 0; i < aufa_anz; i ++)
         {
             if (a == aufa[i].a) return;
         }
         aufa[i].auf = auf;
         aufa[i].a = a;
         aufa_anz ++;
}

void LastAuf::ReadLstAuf (short mdn, long kun)
/**
Artikel auf letzten Auftraegen holen.
**/
{
         int cursor;
         int cursor_pos;
         char lieferdat [12];
          
         aufa_anz = 0;
      
         sqlin ((short *) &mdn, 1, 0); 
         sqlin ((long *) &kun,  2, 0); 
         sqlout ((long *) &auf, 2, 0);
         sqlout ((long *) &lieferdat, 0, 11);
         cursor = sqlcursor ("select auf,lieferdat from aufk "
                              "where mdn = ? "
                              "and kun = ? "
                              "order by lieferdat desc");
         sqlin ((long *) &auf, 2, 0);
         sqlout ((double *) &a, 3, 0);
         cursor_pos = sqlcursor ("select distinct a from aufp "
                                 "where auf = ? "
                                 "order by a");

         dsqlstatus = sqlfetch (cursor);
         while (dsqlstatus == 0)
         {
             dsqlstatus = sqlopen (cursor_pos);
             dsqlstatus = sqlfetch (cursor_pos);
             while (dsqlstatus == 0)
             {
                 arttotab (auf, a);
                 dsqlstatus = sqlfetch (cursor_pos);
                 if (aufa_anz == anzart) break; 

             }
             if (aufa_anz == anzart) break; 
             dsqlstatus = sqlfetch (cursor);
         }
         sqlclose (cursor_pos);
         sqlclose (cursor);
}
void LastAuf::ReadLstLief (short mdn, long kun) //271010
/**
Artikel auf letzten Auftraegen holen.
**/
{
         int cursor;
         int cursor_pos;
         char lieferdat [12];
          
         aufa_anz = 0;
      
         sqlin ((short *) &mdn, 1, 0); 
         sqlin ((long *) &kun,  2, 0); 
         sqlout ((long *) &auf, 2, 0); 
         sqlout ((long *) &lieferdat, 0, 11);
         cursor = sqlcursor ("select ls,lieferdat from lsk "  
                              "where mdn = ? "
                              "and kun = ? "
                              "order by lieferdat desc");
         sqlin ((long *) &auf, 2, 0);
         sqlout ((double *) &a, 3, 0);
         cursor_pos = sqlcursor ("select distinct a from lsp "
                                 "where ls = ? and ls_vk_pr > 0 "
                                 "order by a");

         dsqlstatus = sqlfetch (cursor);
         while (dsqlstatus == 0)
         {
             dsqlstatus = sqlopen (cursor_pos);
             dsqlstatus = sqlfetch (cursor_pos);
             while (dsqlstatus == 0)
             {
                 arttotab (auf, a);
                 dsqlstatus = sqlfetch (cursor_pos);
                 if (aufa_anz == anzart) break; 

             }
             if (aufa_anz == anzart) break; 
             dsqlstatus = sqlfetch (cursor);
         }
         sqlclose (cursor_pos);
         sqlclose (cursor);
}

BOOL LastAuf::GetFirst (long *auf, double *a)
/**
1. Satz aus Matrix holen.
**/
{
         if (aufa_anz == 0)
         {
             return FALSE;
         }
         *auf = aufa[0].auf; 
         *a   = aufa[0].a;
         aufa_pos = 0;
         return TRUE;
}
         
BOOL LastAuf::GetNext (long *auf, double *a)
/**
Naechsten Satz aus Matrix holen.
**/
{
         aufa_pos ++;
         if (aufa_pos == aufa_anz)
         {
             return FALSE;
         }
         *auf = aufa[aufa_pos].auf; 
         *a   = aufa[aufa_pos].a;
         return TRUE;
}

BOOL LastAuf::GetPrior (long *auf, double *a)
/**
Naechsten Satz aus Matrix holen.
**/
{
         if (aufa_pos == 0) return FALSE;
         aufa_pos --;
         *auf = aufa[aufa_pos].auf; 
         *a   = aufa[aufa_pos].a;
         return TRUE;
}

BOOL LastAuf::GetCurrent (long *auf, double *a)
/**
Naechsten Satz aus Matrix holen.
**/
{
         *auf = aufa[aufa_pos].auf; 
         *a   = aufa[aufa_pos].a;
         return TRUE;
}

BOOL LastAuf::GetAbsolute (long *auf, double *a, int pos)
/**
Naechsten Satz aus Matrix holen.
**/
{
         if (pos >= aufa_anz) return FALSE;
         if (pos < 0) return FALSE;
         aufa_pos = pos;
         *auf = aufa[aufa_pos].auf; 
         *a   = aufa[aufa_pos].a;
         return TRUE;
}
