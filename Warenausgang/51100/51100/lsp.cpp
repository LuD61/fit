struct LSP {
   short     mdn;
   short     fil;
   long      ls;
   double    a;
   double    auf_me;
   char      auf_me_bz[7];
   double    lief_me;
   char      lief_me_bz[7];
   double    ls_vk_pr;
   double    ls_lad_pr;
   short     delstatus;
   double    tara;
   long      posi;
   long      lsp_txt;
   short     pos_stat;
   short     sa_kz_sint;
   char      erf_kz[2];
   double    prov_satz;
   short     leer_pos;
   long      hbk_date;
   char      ls_charge[21];
   double    auf_me_vgl;
   double    ls_vk_euro;
   double    ls_vk_fremd;
   double    ls_lad_euro;
   double    ls_lad_fremd;
   double    rab_satz;
   short     me_einh_kun;
   short     me_einh;
   short     me_einh_kun1;
   double    auf_me1;
   double    inh1;
   short     me_einh_kun2;
   double    auf_me2;
   double    inh2;
   short     me_einh_kun3;
   double    auf_me3;
   double    inh3;
   double    lief_me1;
   double    lief_me2;
   double    lief_me3;
   short     ls_pos_kz;
   double    a_grund;
   char      kond_art[5];
   char      lief_me_bz_ist[7];
   double    inh_ist;
   short     me_einh_ist;
   double    me_ist;
   short     lager;
   long      posi_ext;
   long      kun;
   short     na_lief_kz;
   long      nve_posi;
   short     teil_smt;
   short     aufschlag;
   double    aufschlag_wert;
};
extern struct LSP lsp, lsp_null;

#line 2 "lsp.rpp"

    out_quest ((char *) &lsp.mdn,1,0);
    out_quest ((char *) &lsp.fil,1,0);
    out_quest ((char *) &lsp.ls,2,0);
    out_quest ((char *) &lsp.a,3,0);
    out_quest ((char *) &lsp.auf_me,3,0);
    out_quest ((char *) lsp.auf_me_bz,0,7);
    out_quest ((char *) &lsp.lief_me,3,0);
    out_quest ((char *) lsp.lief_me_bz,0,7);
    out_quest ((char *) &lsp.ls_vk_pr,3,0);
    out_quest ((char *) &lsp.ls_lad_pr,3,0);
    out_quest ((char *) &lsp.delstatus,1,0);
    out_quest ((char *) &lsp.tara,3,0);
    out_quest ((char *) &lsp.posi,2,0);
    out_quest ((char *) &lsp.lsp_txt,2,0);
    out_quest ((char *) &lsp.pos_stat,1,0);
    out_quest ((char *) &lsp.sa_kz_sint,1,0);
    out_quest ((char *) lsp.erf_kz,0,2);
    out_quest ((char *) &lsp.prov_satz,3,0);
    out_quest ((char *) &lsp.leer_pos,1,0);
    out_quest ((char *) &lsp.hbk_date,2,0);
    out_quest ((char *) lsp.ls_charge,0,21);
    out_quest ((char *) &lsp.auf_me_vgl,3,0);
    out_quest ((char *) &lsp.ls_vk_euro,3,0);
    out_quest ((char *) &lsp.ls_vk_fremd,3,0);
    out_quest ((char *) &lsp.ls_lad_euro,3,0);
    out_quest ((char *) &lsp.ls_lad_fremd,3,0);
    out_quest ((char *) &lsp.rab_satz,3,0);
    out_quest ((char *) &lsp.me_einh_kun,1,0);
    out_quest ((char *) &lsp.me_einh,1,0);
    out_quest ((char *) &lsp.me_einh_kun1,1,0);
    out_quest ((char *) &lsp.auf_me1,3,0);
    out_quest ((char *) &lsp.inh1,3,0);
    out_quest ((char *) &lsp.me_einh_kun2,1,0);
    out_quest ((char *) &lsp.auf_me2,3,0);
    out_quest ((char *) &lsp.inh2,3,0);
    out_quest ((char *) &lsp.me_einh_kun3,1,0);
    out_quest ((char *) &lsp.auf_me3,3,0);
    out_quest ((char *) &lsp.inh3,3,0);
    out_quest ((char *) &lsp.lief_me1,3,0);
    out_quest ((char *) &lsp.lief_me2,3,0);
    out_quest ((char *) &lsp.lief_me3,3,0);
    out_quest ((char *) &lsp.ls_pos_kz,1,0);
    out_quest ((char *) &lsp.a_grund,3,0);
    out_quest ((char *) lsp.kond_art,0,5);
    out_quest ((char *) lsp.lief_me_bz_ist,0,7);
    out_quest ((char *) &lsp.inh_ist,3,0);
    out_quest ((char *) &lsp.me_einh_ist,1,0);
    out_quest ((char *) &lsp.me_ist,3,0);
    out_quest ((char *) &lsp.lager,1,0);
    out_quest ((char *) &lsp.posi_ext,2,0);
    out_quest ((char *) &lsp.kun,2,0);
    out_quest ((char *) &lsp.na_lief_kz,1,0);
    out_quest ((char *) &lsp.nve_posi,2,0);
    out_quest ((char *) &lsp.teil_smt,1,0);
    out_quest ((char *) &lsp.aufschlag,1,0);
    out_quest ((char *) &lsp.aufschlag_wert,3,0);
cursor_lsp = prepare_sql ("select lsp.mdn,  lsp.fil,  lsp.ls,  lsp.a,  "
"lsp.auf_me,  lsp.auf_me_bz,  lsp.lief_me,  lsp.lief_me_bz,  lsp.ls_vk_pr,  "
"lsp.ls_lad_pr,  lsp.delstatus,  lsp.tara,  lsp.posi,  lsp.lsp_txt,  "
"lsp.pos_stat,  lsp.sa_kz_sint,  lsp.erf_kz,  lsp.prov_satz,  "
"lsp.leer_pos,  lsp.hbk_date,  lsp.ls_charge,  lsp.auf_me_vgl,  "
"lsp.ls_vk_euro,  lsp.ls_vk_fremd,  lsp.ls_lad_euro,  lsp.ls_lad_fremd,  "
"lsp.rab_satz,  lsp.me_einh_kun,  lsp.me_einh,  lsp.me_einh_kun1,  "
"lsp.auf_me1,  lsp.inh1,  lsp.me_einh_kun2,  lsp.auf_me2,  lsp.inh2,  "
"lsp.me_einh_kun3,  lsp.auf_me3,  lsp.inh3,  lsp.lief_me1,  lsp.lief_me2,  "
"lsp.lief_me3,  lsp.ls_pos_kz,  lsp.a_grund,  lsp.kond_art,  "
"lsp.lief_me_bz_ist,  lsp.inh_ist,  lsp.me_einh_ist,  lsp.me_ist,  "
"lsp.lager,  lsp.posi_ext,  lsp.kun,  lsp.na_lief_kz,  lsp.nve_posi,  "
"lsp.teil_smt,  lsp.aufschlag,  lsp.aufschlag_wert "

#line 4 "lsp.rpp"
                          "from lsp "
                          "where mdn = ? "
                          "and fil = ? "
                          "and ls = ? %s", order);

    out_quest ((char *) &lsp.mdn,1,0);
    out_quest ((char *) &lsp.fil,1,0);
    out_quest ((char *) &lsp.ls,2,0);
    out_quest ((char *) &lsp.a,3,0);
    out_quest ((char *) &lsp.auf_me,3,0);
    out_quest ((char *) lsp.auf_me_bz,0,7);
    out_quest ((char *) &lsp.lief_me,3,0);
    out_quest ((char *) lsp.lief_me_bz,0,7);
    out_quest ((char *) &lsp.ls_vk_pr,3,0);
    out_quest ((char *) &lsp.ls_lad_pr,3,0);
    out_quest ((char *) &lsp.delstatus,1,0);
    out_quest ((char *) &lsp.tara,3,0);
    out_quest ((char *) &lsp.posi,2,0);
    out_quest ((char *) &lsp.lsp_txt,2,0);
    out_quest ((char *) &lsp.pos_stat,1,0);
    out_quest ((char *) &lsp.sa_kz_sint,1,0);
    out_quest ((char *) lsp.erf_kz,0,2);
    out_quest ((char *) &lsp.prov_satz,3,0);
    out_quest ((char *) &lsp.leer_pos,1,0);
    out_quest ((char *) &lsp.hbk_date,2,0);
    out_quest ((char *) lsp.ls_charge,0,21);
    out_quest ((char *) &lsp.auf_me_vgl,3,0);
    out_quest ((char *) &lsp.ls_vk_euro,3,0);
    out_quest ((char *) &lsp.ls_vk_fremd,3,0);
    out_quest ((char *) &lsp.ls_lad_euro,3,0);
    out_quest ((char *) &lsp.ls_lad_fremd,3,0);
    out_quest ((char *) &lsp.rab_satz,3,0);
    out_quest ((char *) &lsp.me_einh_kun,1,0);
    out_quest ((char *) &lsp.me_einh,1,0);
    out_quest ((char *) &lsp.me_einh_kun1,1,0);
    out_quest ((char *) &lsp.auf_me1,3,0);
    out_quest ((char *) &lsp.inh1,3,0);
    out_quest ((char *) &lsp.me_einh_kun2,1,0);
    out_quest ((char *) &lsp.auf_me2,3,0);
    out_quest ((char *) &lsp.inh2,3,0);
    out_quest ((char *) &lsp.me_einh_kun3,1,0);
    out_quest ((char *) &lsp.auf_me3,3,0);
    out_quest ((char *) &lsp.inh3,3,0);
    out_quest ((char *) &lsp.lief_me1,3,0);
    out_quest ((char *) &lsp.lief_me2,3,0);
    out_quest ((char *) &lsp.lief_me3,3,0);
    out_quest ((char *) &lsp.ls_pos_kz,1,0);
    out_quest ((char *) &lsp.a_grund,3,0);
    out_quest ((char *) lsp.kond_art,0,5);
    out_quest ((char *) lsp.lief_me_bz_ist,0,7);
    out_quest ((char *) &lsp.inh_ist,3,0);
    out_quest ((char *) &lsp.me_einh_ist,1,0);
    out_quest ((char *) &lsp.me_ist,3,0);
    out_quest ((char *) &lsp.lager,1,0);
    out_quest ((char *) &lsp.posi_ext,2,0);
    out_quest ((char *) &lsp.kun,2,0);
    out_quest ((char *) &lsp.na_lief_kz,1,0);
    out_quest ((char *) &lsp.nve_posi,2,0);
    out_quest ((char *) &lsp.teil_smt,1,0);
    out_quest ((char *) &lsp.aufschlag,1,0);
    out_quest ((char *) &lsp.aufschlag_wert,3,0);
cursor_lsp_ap = prepare_sql ("select lsp.mdn,  lsp.fil,  lsp.ls,  "
"lsp.a,  lsp.auf_me,  lsp.auf_me_bz,  lsp.lief_me,  lsp.lief_me_bz,  "
"lsp.ls_vk_pr,  lsp.ls_lad_pr,  lsp.delstatus,  lsp.tara,  lsp.posi,  "
"lsp.lsp_txt,  lsp.pos_stat,  lsp.sa_kz_sint,  lsp.erf_kz,  lsp.prov_satz,  "
"lsp.leer_pos,  lsp.hbk_date,  lsp.ls_charge,  lsp.auf_me_vgl,  "
"lsp.ls_vk_euro,  lsp.ls_vk_fremd,  lsp.ls_lad_euro,  lsp.ls_lad_fremd,  "
"lsp.rab_satz,  lsp.me_einh_kun,  lsp.me_einh,  lsp.me_einh_kun1,  "
"lsp.auf_me1,  lsp.inh1,  lsp.me_einh_kun2,  lsp.auf_me2,  lsp.inh2,  "
"lsp.me_einh_kun3,  lsp.auf_me3,  lsp.inh3,  lsp.lief_me1,  lsp.lief_me2,  "
"lsp.lief_me3,  lsp.ls_pos_kz,  lsp.a_grund,  lsp.kond_art,  "
"lsp.lief_me_bz_ist,  lsp.inh_ist,  lsp.me_einh_ist,  lsp.me_ist,  "
"lsp.lager,  lsp.posi_ext,  lsp.kun,  lsp.na_lief_kz,  lsp.nve_posi,  "
"lsp.teil_smt,  lsp.aufschlag,  lsp.aufschlag_wert "

#line 10 "lsp.rpp"
                          "from lsp "
                          "where mdn = ? "
                          "and fil = ? "
                          "and ls = ? "
                          "and a = ? "
                          "and posi = ?");

    ins_quest ((char *) &lsp.mdn,1,0);
    ins_quest ((char *) &lsp.fil,1,0);
    ins_quest ((char *) &lsp.ls,2,0);
    ins_quest ((char *) &lsp.a,3,0);
    ins_quest ((char *) &lsp.auf_me,3,0);
    ins_quest ((char *) lsp.auf_me_bz,0,7);
    ins_quest ((char *) &lsp.lief_me,3,0);
    ins_quest ((char *) lsp.lief_me_bz,0,7);
    ins_quest ((char *) &lsp.ls_vk_pr,3,0);
    ins_quest ((char *) &lsp.ls_lad_pr,3,0);
    ins_quest ((char *) &lsp.delstatus,1,0);
    ins_quest ((char *) &lsp.tara,3,0);
    ins_quest ((char *) &lsp.posi,2,0);
    ins_quest ((char *) &lsp.lsp_txt,2,0);
    ins_quest ((char *) &lsp.pos_stat,1,0);
    ins_quest ((char *) &lsp.sa_kz_sint,1,0);
    ins_quest ((char *) lsp.erf_kz,0,2);
    ins_quest ((char *) &lsp.prov_satz,3,0);
    ins_quest ((char *) &lsp.leer_pos,1,0);
    ins_quest ((char *) &lsp.hbk_date,2,0);
    ins_quest ((char *) lsp.ls_charge,0,21);
    ins_quest ((char *) &lsp.auf_me_vgl,3,0);
    ins_quest ((char *) &lsp.ls_vk_euro,3,0);
    ins_quest ((char *) &lsp.ls_vk_fremd,3,0);
    ins_quest ((char *) &lsp.ls_lad_euro,3,0);
    ins_quest ((char *) &lsp.ls_lad_fremd,3,0);
    ins_quest ((char *) &lsp.rab_satz,3,0);
    ins_quest ((char *) &lsp.me_einh_kun,1,0);
    ins_quest ((char *) &lsp.me_einh,1,0);
    ins_quest ((char *) &lsp.me_einh_kun1,1,0);
    ins_quest ((char *) &lsp.auf_me1,3,0);
    ins_quest ((char *) &lsp.inh1,3,0);
    ins_quest ((char *) &lsp.me_einh_kun2,1,0);
    ins_quest ((char *) &lsp.auf_me2,3,0);
    ins_quest ((char *) &lsp.inh2,3,0);
    ins_quest ((char *) &lsp.me_einh_kun3,1,0);
    ins_quest ((char *) &lsp.auf_me3,3,0);
    ins_quest ((char *) &lsp.inh3,3,0);
    ins_quest ((char *) &lsp.lief_me1,3,0);
    ins_quest ((char *) &lsp.lief_me2,3,0);
    ins_quest ((char *) &lsp.lief_me3,3,0);
    ins_quest ((char *) &lsp.ls_pos_kz,1,0);
    ins_quest ((char *) &lsp.a_grund,3,0);
    ins_quest ((char *) lsp.kond_art,0,5);
    ins_quest ((char *) lsp.lief_me_bz_ist,0,7);
    ins_quest ((char *) &lsp.inh_ist,3,0);
    ins_quest ((char *) &lsp.me_einh_ist,1,0);
    ins_quest ((char *) &lsp.me_ist,3,0);
    ins_quest ((char *) &lsp.lager,1,0);
    ins_quest ((char *) &lsp.posi_ext,2,0);
    ins_quest ((char *) &lsp.kun,2,0);
    ins_quest ((char *) &lsp.na_lief_kz,1,0);
    ins_quest ((char *) &lsp.nve_posi,2,0);
    ins_quest ((char *) &lsp.teil_smt,1,0);
    ins_quest ((char *) &lsp.aufschlag,1,0);
    ins_quest ((char *) &lsp.aufschlag_wert,3,0);
cursor_lsp_upd = prepare_sql ("update lsp set lsp.mdn = ?,  "
"lsp.fil = ?,  lsp.ls = ?,  lsp.a = ?,  lsp.auf_me = ?,  "
"lsp.auf_me_bz = ?,  lsp.lief_me = ?,  lsp.lief_me_bz = ?,  "
"lsp.ls_vk_pr = ?,  lsp.ls_lad_pr = ?,  lsp.delstatus = ?,  "
"lsp.tara = ?,  lsp.posi = ?,  lsp.lsp_txt = ?,  lsp.pos_stat = ?,  "
"lsp.sa_kz_sint = ?,  lsp.erf_kz = ?,  lsp.prov_satz = ?,  "
"lsp.leer_pos = ?,  lsp.hbk_date = ?,  lsp.ls_charge = ?,  "
"lsp.auf_me_vgl = ?,  lsp.ls_vk_euro = ?,  lsp.ls_vk_fremd = ?,  "
"lsp.ls_lad_euro = ?,  lsp.ls_lad_fremd = ?,  lsp.rab_satz = ?,  "
"lsp.me_einh_kun = ?,  lsp.me_einh = ?,  lsp.me_einh_kun1 = ?,  "
"lsp.auf_me1 = ?,  lsp.inh1 = ?,  lsp.me_einh_kun2 = ?,  "
"lsp.auf_me2 = ?,  lsp.inh2 = ?,  lsp.me_einh_kun3 = ?,  "
"lsp.auf_me3 = ?,  lsp.inh3 = ?,  lsp.lief_me1 = ?,  lsp.lief_me2 = ?,  "
"lsp.lief_me3 = ?,  lsp.ls_pos_kz = ?,  lsp.a_grund = ?,  "
"lsp.kond_art = ?,  lsp.lief_me_bz_ist = ?,  lsp.inh_ist = ?,  "
"lsp.me_einh_ist = ?,  lsp.me_ist = ?,  lsp.lager = ?,  "
"lsp.posi_ext = ?,  lsp.kun = ?,  lsp.na_lief_kz = ?,  "
"lsp.nve_posi = ?,  lsp.teil_smt = ?,  lsp.aufschlag = ?,  "
"lsp.aufschlag_wert = ? "

#line 18 "lsp.rpp"
                              "where mdn = ? "
                              "and fil = ? "
                              "and ls = ? "
                              "and a = ? "
                              "and posi = ?"); 

    ins_quest ((char *) &lsp.mdn,1,0);
    ins_quest ((char *) &lsp.fil,1,0);
    ins_quest ((char *) &lsp.ls,2,0);
    ins_quest ((char *) &lsp.a,3,0);
    ins_quest ((char *) &lsp.auf_me,3,0);
    ins_quest ((char *) lsp.auf_me_bz,0,7);
    ins_quest ((char *) &lsp.lief_me,3,0);
    ins_quest ((char *) lsp.lief_me_bz,0,7);
    ins_quest ((char *) &lsp.ls_vk_pr,3,0);
    ins_quest ((char *) &lsp.ls_lad_pr,3,0);
    ins_quest ((char *) &lsp.delstatus,1,0);
    ins_quest ((char *) &lsp.tara,3,0);
    ins_quest ((char *) &lsp.posi,2,0);
    ins_quest ((char *) &lsp.lsp_txt,2,0);
    ins_quest ((char *) &lsp.pos_stat,1,0);
    ins_quest ((char *) &lsp.sa_kz_sint,1,0);
    ins_quest ((char *) lsp.erf_kz,0,2);
    ins_quest ((char *) &lsp.prov_satz,3,0);
    ins_quest ((char *) &lsp.leer_pos,1,0);
    ins_quest ((char *) &lsp.hbk_date,2,0);
    ins_quest ((char *) lsp.ls_charge,0,21);
    ins_quest ((char *) &lsp.auf_me_vgl,3,0);
    ins_quest ((char *) &lsp.ls_vk_euro,3,0);
    ins_quest ((char *) &lsp.ls_vk_fremd,3,0);
    ins_quest ((char *) &lsp.ls_lad_euro,3,0);
    ins_quest ((char *) &lsp.ls_lad_fremd,3,0);
    ins_quest ((char *) &lsp.rab_satz,3,0);
    ins_quest ((char *) &lsp.me_einh_kun,1,0);
    ins_quest ((char *) &lsp.me_einh,1,0);
    ins_quest ((char *) &lsp.me_einh_kun1,1,0);
    ins_quest ((char *) &lsp.auf_me1,3,0);
    ins_quest ((char *) &lsp.inh1,3,0);
    ins_quest ((char *) &lsp.me_einh_kun2,1,0);
    ins_quest ((char *) &lsp.auf_me2,3,0);
    ins_quest ((char *) &lsp.inh2,3,0);
    ins_quest ((char *) &lsp.me_einh_kun3,1,0);
    ins_quest ((char *) &lsp.auf_me3,3,0);
    ins_quest ((char *) &lsp.inh3,3,0);
    ins_quest ((char *) &lsp.lief_me1,3,0);
    ins_quest ((char *) &lsp.lief_me2,3,0);
    ins_quest ((char *) &lsp.lief_me3,3,0);
    ins_quest ((char *) &lsp.ls_pos_kz,1,0);
    ins_quest ((char *) &lsp.a_grund,3,0);
    ins_quest ((char *) lsp.kond_art,0,5);
    ins_quest ((char *) lsp.lief_me_bz_ist,0,7);
    ins_quest ((char *) &lsp.inh_ist,3,0);
    ins_quest ((char *) &lsp.me_einh_ist,1,0);
    ins_quest ((char *) &lsp.me_ist,3,0);
    ins_quest ((char *) &lsp.lager,1,0);
    ins_quest ((char *) &lsp.posi_ext,2,0);
    ins_quest ((char *) &lsp.kun,2,0);
    ins_quest ((char *) &lsp.na_lief_kz,1,0);
    ins_quest ((char *) &lsp.nve_posi,2,0);
    ins_quest ((char *) &lsp.teil_smt,1,0);
    ins_quest ((char *) &lsp.aufschlag,1,0);
    ins_quest ((char *) &lsp.aufschlag_wert,3,0);
cursor_lsp_ins = prepare_sql ("insert into lsp (mdn,  fil,  ls,  a,  "
"auf_me,  auf_me_bz,  lief_me,  lief_me_bz,  ls_vk_pr,  ls_lad_pr,  delstatus,  "
"tara,  posi,  lsp_txt,  pos_stat,  sa_kz_sint,  erf_kz,  prov_satz,  leer_pos,  "
"hbk_date,  ls_charge,  auf_me_vgl,  ls_vk_euro,  ls_vk_fremd,  ls_lad_euro,  "
"ls_lad_fremd,  rab_satz,  me_einh_kun,  me_einh,  me_einh_kun1,  auf_me1,  "
"inh1,  me_einh_kun2,  auf_me2,  inh2,  me_einh_kun3,  auf_me3,  inh3,  lief_me1,  "
"lief_me2,  lief_me3,  ls_pos_kz,  a_grund,  kond_art,  lief_me_bz_ist,  "
"inh_ist,  me_einh_ist,  me_ist,  lager,  posi_ext,  kun,  na_lief_kz,  nve_posi,  "
"teil_smt,  aufschlag,  aufschlag_wert) values ("
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
#line 25 "lsp.rpp"
