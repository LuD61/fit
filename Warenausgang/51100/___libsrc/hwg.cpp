#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "dbclass.h"
#include "strfkt.h"
#include "hwg.h"


struct HWG hwg, hwg_null;

void HWG_CL::prepare (void)
{
            char *sqltext;

            sqlin ((long *)   &hwg.hwg,  SQLSHORT, 0);
    out_quest ((char *) &hwg.hwg,1,0);
    out_quest ((char *) hwg.hwg_bz1,0,25);
    out_quest ((char *) hwg.hwg_bz2,0,25);
    out_quest ((char *) hwg.sp_kz,0,2);
    out_quest ((char *) &hwg.we_kto,2,0);
    out_quest ((char *) &hwg.erl_kto,2,0);
    out_quest ((char *) hwg.durch_vk,0,2);
    out_quest ((char *) hwg.durch_sk,0,2);
    out_quest ((char *) hwg.durch_fil,0,2);
    out_quest ((char *) hwg.durch_sw,0,2);
    out_quest ((char *) hwg.best_auto,0,2);
    out_quest ((char *) &hwg.sp_vk,3,0);
    out_quest ((char *) &hwg.sp_sk,3,0);
    out_quest ((char *) &hwg.sp_fil,3,0);
    out_quest ((char *) &hwg.sw,3,0);
    out_quest ((char *) &hwg.me_einh,1,0);
    out_quest ((char *) &hwg.mwst,1,0);
    out_quest ((char *) &hwg.teil_smt,1,0);
    out_quest ((char *) &hwg.bearb_lad,1,0);
    out_quest ((char *) &hwg.bearb_fil,1,0);
    out_quest ((char *) &hwg.bearb_sk,1,0);
    out_quest ((char *) hwg.sam_ean,0,2);
    out_quest ((char *) hwg.smt,0,2);
    out_quest ((char *) hwg.kost_kz,0,3);
    out_quest ((char *) &hwg.pers_rab,3,0);
    out_quest ((char *) &hwg.akv,2,0);
    out_quest ((char *) &hwg.bearb,2,0);
    out_quest ((char *) hwg.pers_nam,0,9);
    out_quest ((char *) &hwg.delstatus,1,0);
    out_quest ((char *) &hwg.erl_kto_1,2,0);
    out_quest ((char *) &hwg.erl_kto_2,2,0);
    out_quest ((char *) &hwg.erl_kto_3,2,0);
    out_quest ((char *) &hwg.we_kto_1,2,0);
    out_quest ((char *) &hwg.we_kto_2,2,0);
    out_quest ((char *) &hwg.we_kto_3,2,0);
            cursor = sqlcursor ("select hwg.hwg,  hwg.hwg_bz1,  "
"hwg.hwg_bz2,  hwg.sp_kz,  hwg.we_kto,  hwg.erl_kto,  hwg.durch_vk,  "
"hwg.durch_sk,  hwg.durch_fil,  hwg.durch_sw,  hwg.best_auto,  hwg.sp_vk,  "
"hwg.sp_sk,  hwg.sp_fil,  hwg.sw,  hwg.me_einh,  hwg.mwst,  hwg.teil_smt,  "
"hwg.bearb_lad,  hwg.bearb_fil,  hwg.bearb_sk,  hwg.sam_ean,  hwg.smt,  "
"hwg.kost_kz,  hwg.pers_rab,  hwg.akv,  hwg.bearb,  hwg.pers_nam,  "
"hwg.delstatus,  hwg.erl_kto_1,  hwg.erl_kto_2,  hwg.erl_kto_3,  "
"hwg.we_kto_1,  hwg.we_kto_2,  hwg.we_kto_3 from hwg "

                                  "where hwg = ?");
    ins_quest ((char *) &hwg.hwg,1,0);
    ins_quest ((char *) hwg.hwg_bz1,0,25);
    ins_quest ((char *) hwg.hwg_bz2,0,25);
    ins_quest ((char *) hwg.sp_kz,0,2);
    ins_quest ((char *) &hwg.we_kto,2,0);
    ins_quest ((char *) &hwg.erl_kto,2,0);
    ins_quest ((char *) hwg.durch_vk,0,2);
    ins_quest ((char *) hwg.durch_sk,0,2);
    ins_quest ((char *) hwg.durch_fil,0,2);
    ins_quest ((char *) hwg.durch_sw,0,2);
    ins_quest ((char *) hwg.best_auto,0,2);
    ins_quest ((char *) &hwg.sp_vk,3,0);
    ins_quest ((char *) &hwg.sp_sk,3,0);
    ins_quest ((char *) &hwg.sp_fil,3,0);
    ins_quest ((char *) &hwg.sw,3,0);
    ins_quest ((char *) &hwg.me_einh,1,0);
    ins_quest ((char *) &hwg.mwst,1,0);
    ins_quest ((char *) &hwg.teil_smt,1,0);
    ins_quest ((char *) &hwg.bearb_lad,1,0);
    ins_quest ((char *) &hwg.bearb_fil,1,0);
    ins_quest ((char *) &hwg.bearb_sk,1,0);
    ins_quest ((char *) hwg.sam_ean,0,2);
    ins_quest ((char *) hwg.smt,0,2);
    ins_quest ((char *) hwg.kost_kz,0,3);
    ins_quest ((char *) &hwg.pers_rab,3,0);
    ins_quest ((char *) &hwg.akv,2,0);
    ins_quest ((char *) &hwg.bearb,2,0);
    ins_quest ((char *) hwg.pers_nam,0,9);
    ins_quest ((char *) &hwg.delstatus,1,0);
    ins_quest ((char *) &hwg.erl_kto_1,2,0);
    ins_quest ((char *) &hwg.erl_kto_2,2,0);
    ins_quest ((char *) &hwg.erl_kto_3,2,0);
    ins_quest ((char *) &hwg.we_kto_1,2,0);
    ins_quest ((char *) &hwg.we_kto_2,2,0);
    ins_quest ((char *) &hwg.we_kto_3,2,0);
            sqltext = "update hwg set hwg.hwg = ?,  "
"hwg.hwg_bz1 = ?,  hwg.hwg_bz2 = ?,  hwg.sp_kz = ?,  hwg.we_kto = ?,  "
"hwg.erl_kto = ?,  hwg.durch_vk = ?,  hwg.durch_sk = ?,  "
"hwg.durch_fil = ?,  hwg.durch_sw = ?,  hwg.best_auto = ?,  "
"hwg.sp_vk = ?,  hwg.sp_sk = ?,  hwg.sp_fil = ?,  hwg.sw = ?,  "
"hwg.me_einh = ?,  hwg.mwst = ?,  hwg.teil_smt = ?,  hwg.bearb_lad = ?,  "
"hwg.bearb_fil = ?,  hwg.bearb_sk = ?,  hwg.sam_ean = ?,  hwg.smt = ?,  "
"hwg.kost_kz = ?,  hwg.pers_rab = ?,  hwg.akv = ?,  hwg.bearb = ?,  "
"hwg.pers_nam = ?,  hwg.delstatus = ?,  hwg.erl_kto_1 = ?,  "
"hwg.erl_kto_2 = ?,  hwg.erl_kto_3 = ?,  hwg.we_kto_1 = ?,  "
"hwg.we_kto_2 = ?,  hwg.we_kto_3 = ? "

                                  "where hwg = ?";
            sqlin ((long *)   &hwg.hwg,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &hwg.hwg,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor ("select hwg from hwg "
                                  "where hwg = ?");
            sqlin ((long *)   &hwg.hwg,  SQLSHORT, 0);
            del_cursor = sqlcursor ("delete from hwg "
                                  "where hwg = ?");
    ins_quest ((char *) &hwg.hwg,1,0);
    ins_quest ((char *) hwg.hwg_bz1,0,25);
    ins_quest ((char *) hwg.hwg_bz2,0,25);
    ins_quest ((char *) hwg.sp_kz,0,2);
    ins_quest ((char *) &hwg.we_kto,2,0);
    ins_quest ((char *) &hwg.erl_kto,2,0);
    ins_quest ((char *) hwg.durch_vk,0,2);
    ins_quest ((char *) hwg.durch_sk,0,2);
    ins_quest ((char *) hwg.durch_fil,0,2);
    ins_quest ((char *) hwg.durch_sw,0,2);
    ins_quest ((char *) hwg.best_auto,0,2);
    ins_quest ((char *) &hwg.sp_vk,3,0);
    ins_quest ((char *) &hwg.sp_sk,3,0);
    ins_quest ((char *) &hwg.sp_fil,3,0);
    ins_quest ((char *) &hwg.sw,3,0);
    ins_quest ((char *) &hwg.me_einh,1,0);
    ins_quest ((char *) &hwg.mwst,1,0);
    ins_quest ((char *) &hwg.teil_smt,1,0);
    ins_quest ((char *) &hwg.bearb_lad,1,0);
    ins_quest ((char *) &hwg.bearb_fil,1,0);
    ins_quest ((char *) &hwg.bearb_sk,1,0);
    ins_quest ((char *) hwg.sam_ean,0,2);
    ins_quest ((char *) hwg.smt,0,2);
    ins_quest ((char *) hwg.kost_kz,0,3);
    ins_quest ((char *) &hwg.pers_rab,3,0);
    ins_quest ((char *) &hwg.akv,2,0);
    ins_quest ((char *) &hwg.bearb,2,0);
    ins_quest ((char *) hwg.pers_nam,0,9);
    ins_quest ((char *) &hwg.delstatus,1,0);
    ins_quest ((char *) &hwg.erl_kto_1,2,0);
    ins_quest ((char *) &hwg.erl_kto_2,2,0);
    ins_quest ((char *) &hwg.erl_kto_3,2,0);
    ins_quest ((char *) &hwg.we_kto_1,2,0);
    ins_quest ((char *) &hwg.we_kto_2,2,0);
    ins_quest ((char *) &hwg.we_kto_3,2,0);
            ins_cursor = sqlcursor ("insert into hwg (hwg,  "
"hwg_bz1,  hwg_bz2,  sp_kz,  we_kto,  erl_kto,  durch_vk,  durch_sk,  durch_fil,  "
"durch_sw,  best_auto,  sp_vk,  sp_sk,  sp_fil,  sw,  me_einh,  mwst,  teil_smt,  "
"bearb_lad,  bearb_fil,  bearb_sk,  sam_ean,  smt,  kost_kz,  pers_rab,  akv,  bearb,  "
"pers_nam,  delstatus,  erl_kto_1,  erl_kto_2,  erl_kto_3,  we_kto_1,  we_kto_2,  "
"we_kto_3) "

                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

}

//LAC-9 A

static char vEinh [81]; //LAC-132  31-> 81  !!! 

//static ITEM iEinhTxt      ("", "Warengruppenauswahl",  "", 0);   
static ITEM iEinhTxt      ("", " ",  "", 0);   
static ITEM iEinh         ("", vEinh,          "", 0);   

static ITEM vOK     ("",  " OK ",  "", 0);
static ITEM vCancel ("",  "Abbrechen", "", 0);

static int testcombo (void);
static int testOK (void);
static int testCancel (void);

static char Otab [50][81];
static int WgTab [50];
static combofield  cbfield[] = {1,  81, 0, (char *) Otab,
                                0,  0, 0, NULL};


static field _fDrkw [] = {
&iEinhTxt,       1, 0,  0, 10,  0, "", DISPLAYONLY, 0, 0, 0,
&iEinh,          50,50,  0, 0,  0, "DROPDOWN", COMBOBOX | DROPLIST, 0, testcombo, 0,     
// &vOK,            11, 0,  4, 5,  0, "", BUTTON, 0, testOK,     KEY12,
// &vCancel,        11, 0,  4,18,  0, "", BUTTON, 0, testCancel, KEY5, 
//&vCancel,           11, 0,  4,11,  0, "", BUTTON, 0, testCancel, KEY5, 
}; 
static form fDrkw = {2, 0, 0, _fDrkw, 0, 0, 0, cbfield, NULL}; 

static int testcombo (void)
{
       if (testkeys ()) return 0;

       if (syskey == KEYCR)
       {
           syskey = KEY12;
           break_enter ();
       }
       return 0;
}



static int eBreak (void)
{
       switch (syskey)
       {
          case KEYDOWN :
          case KEYUP :
          case KEYTAB :
              return 0;
       }
       syskey = KEY5;
       break_enter ();
       return 0;
}

static int eOk (void)
{
       switch (syskey)
       {
          case KEYDOWN :
          case KEYUP :
          case KEYTAB :
              return 0;
       }
       syskey = KEY12;
       break_enter ();
       return 0;
}





void HWG_CL::InitCombo (int dhwg, int idx, int typ)
/**
Combofelder fuellen.
**/
{
	int cursor ; 
	BOOL WgFound ;
	int wg ;
	char wg_bz1 [26];
	char hwg_bz1 [26];
	char ctext [30];
	char Kennz [2];
	char ckz ; 
	char clieferdat [11];
	int dlieferdat;
	int dauf, danz ;
	int danz_ges;
	short dkw, dkw0;
	short dwochentag;
	static int dkun = 0;


	if (idx < 1 && typ == 2)  //Kennung f�r letzte Bestellung LAC-132
	{
		if (dkun == dhwg) return; //LAC-32 WgTab ist schon bef�llt f�r den Kunden
		dkun = dhwg; //LAC-132
		//Variabel dhwg  = aufk.kun 
		//Variabel idx =  anzahl Wochen zur�ckschauen

		   sqlin ((int *) &dhwg, 2, 0);
		   sqlin ((int *) &dAktLieferdat, 2, 0);
		   sqlin ((int *) &dAktLieferdat, 2, 0);
		   sqlout ((int *) &dlieferdat, 2, 0);
		   sqlout ((int *) &dkw, 1, 0);
		   sqlout ((int *) &dwochentag, 1, 0);
		   sqlout ((int *) &dauf, 2, 0);
		   cursor = sqlcursor ("select lieferdat,kw,wochentag,auf from aufchart "
			                   "where kun = ? and lieferdat between ? - 180 and ?  and auf in (select auf from aufk where lieferdat > today -180 and auf_stat > 1) order by lieferdat desc, auf desc");

		   int i = 1;
		   WgFound = FALSE;
	        strcpy (Otab[0], "=== letzte Bestellungen ===");
			WgTab[0] = 0;
		   while (sqlfetch (cursor) == 0)
		   {
			   if (WgFound == FALSE)  dkw0 = dkw;
			   WgFound = TRUE;
	            switch (dwochentag)
		       {
					case 1: sprintf (ctext, "Montag    "); break;
					case 2: sprintf (ctext, "Dienstag  "); break;
					case 3: sprintf (ctext, "Mittwoch  "); break;
					case 4: sprintf (ctext, "Donnerstag"); break;
					case 5: sprintf (ctext, "Freitag   "); break;
					case 6: sprintf (ctext, "Samstag   "); break;
					case 7: sprintf (ctext, "Sonntag   "); break;
				}
				dlong_to_asc (dlieferdat, clieferdat);

//			   sprintf (Otab [i], "%ld: %10s  %s  KW%hd" , dauf, ctext, clieferdat, dkw);
			   sprintf (Otab [i], "%ld: %s  %10s     KW%2hd" , dauf, clieferdat,ctext, dkw);
			//	if (i == 1) WgTab[0] = dauf;
				WgTab[i] = dauf;
				i ++;
			   if (i == 50) break;
			   if (dkw < dkw0 + idx) break;
		   }
		   i --;
		   sqlclose (cursor);
            cbfield[0].cbanz = i;
            strcpy (vEinh,   Otab[0]);
            fDrkw.mask[1].rows = i + 2;
		   if (WgFound == TRUE) return;

        strcpy (Otab[0], "letzte Bestellungen");
        strcpy (Otab[1], " ");
        strcpy (vEinh,   Otab[AufEinh[0]]);
        cbfield[0].cbanz = 2;
        fDrkw.mask[1].rows = 4;
	}
	else if (typ == 1)  //Kennung f�r Bestellvorschlag LAC-156
	{
//		if (dkun == dhwg) return; //LAC-32 WgTab ist schon bef�llt f�r den Kunden
		dkun = 0; //LAC-132
		//Variabel dhwg  = aufk.kun 
		//Variabel idx =  anzahl Wochen zur�ckschauen   --> wird hier nicht benutzt

		   sqlin ((int *) &dhwg, 2, 0);
		   sqlout ((char *) Kennz, 0, 2);
		   sqlout ((int *) &danz, 2, 0);
		   cursor = sqlcursor ("select kz,count(*) from best_vorschlag "
			                   "where kun = ? group by kz order by kz");

		   int i = 2;
		   WgFound = FALSE;
	        strcpy (Otab[0], "=== Bestellvorschl�ge ===");
			WgTab[0] = 0;
			danz_ges = 0;
		   while (sqlfetch (cursor) == 0)
		   {
			   WgFound = TRUE;
			   ckz = Kennz [0];

	            switch (ckz)
		       {
					case 'S' : sprintf (ctext, "Statistikwerte  "); break;
					case 'A' : sprintf (ctext, "Aktionen        "); break;
					case 'T' : sprintf (ctext, "TV-Werbungen    "); break;
					case 'N' : sprintf (ctext, "neue Artikel    "); break;
					case 'B' : sprintf (ctext, "Vorbestellungen "); break;

				}
				danz_ges += danz;

			   sprintf (Otab [i], " -- davon %3ld %16s" , danz,ctext);
				WgTab[i] = ckz;
				i ++;
			   if (i == 50) break;
		   }
		   sprintf (Otab [1], "%3ld %s" , danz_ges,"Bestellvorschl�ge");
		   WgTab[1] = 'G';  //Gesamt
		   //i --;
		   sqlclose (cursor);
            cbfield[0].cbanz = i;
            strcpy (vEinh,   Otab[0]);
            fDrkw.mask[1].rows = i + 2;
		   if (WgFound == TRUE) return;

        strcpy (Otab[0], "=== Bestellvorschl�ge ===");
        strcpy (Otab[1], " ");
        strcpy (vEinh,   Otab[AufEinh[0]]);
        cbfield[0].cbanz = 2;
        fDrkw.mask[1].rows = 4;
	}
	else  
	{
		dkun = 0; //LAC-132
		   sqlin ((int *) &dhwg, 2, 0);
		   sqlout ((int *) &wg, 2, 0);
		   sqlout ((char *)   wg_bz1, 0, 25); 
		   sqlout ((char *)   hwg_bz1, 0, 25); 
		   cursor = sqlcursor ("select wg.wg,wg.wg_bz1,hwg.hwg_bz1 from hwg,wg "
			                   "where wg.wg > 0 and wg.hwg = ? and hwg.hwg = wg.hwg order by wg.wg");

		   int i = 1;
		   WgFound = FALSE;
	        strcpy (Otab[0], "Hauptwarengruppe gesamt");
			WgTab[0] = 0;
		   while (sqlfetch (cursor) == 0)
		   {
			   WgFound = TRUE;
		        sprintf (Otab[0], "HWG: %s", hwg_bz1);
			   sprintf (Otab [i], "%ld %s", wg, wg_bz1);
				WgTab[i] = wg;
				i ++;
			   if (i == 50) break;
		   }
		   i --;
		   sqlclose (cursor);
            cbfield[0].cbanz = i;
            strcpy (vEinh,   Otab[0]);
            fDrkw.mask[1].rows = i + 2;
		   if (WgFound == TRUE) return;

        strcpy (Otab[0], "Hauptwarengruppe gesamt");
        strcpy (Otab[1], " ");
        strcpy (vEinh,   Otab[AufEinh[idx]]);
        cbfield[0].cbanz = 2;
        fDrkw.mask[1].rows = 4;
	}
}

void HWG_CL::ChoiseCombo (int idx)
/**
Ausgewaehlte Combofelder auswerten.
**/
{
        int i;

        clipped (vEinh);
        for (i = 0; i < cbfield[0].cbanz; i ++)
        {
			clipped (Otab[i]);
            if (strcmp (vEinh,Otab[i]) == 0)
            {
                AufEinh[idx] = i;
                break;
            }
        }
}
char*  HWG_CL::GettxtHWG (int i)
{
	return Otab[AufEinh[i]];
}

int HWG_CL::ChoiseWg (int dhwg, int dPos, int dZeile, int idx, int typ)
/**
String suchen.
**/
{
        HWND hFind;
        HWND ahWnd;
        HWND hMainInst;
		BOOL flg_letzteBestellung = FALSE;
		BOOL flg_Bestellvorschlag = FALSE;

        hMainInst = (HANDLE) 
            GetWindowLong (GetActiveWindow (),  GWL_HINSTANCE);
        save_fkt (5);
        save_fkt (12);
        save_fkt (6);
        InitCombo (dhwg,idx,typ);
		if (typ  == 2) //bei letzte Bestellungen LAC-132
		{
			idx = 0;    // idx der letzten Bestellung ist 0 !!! 
			flg_letzteBestellung = TRUE;
		}
		if (typ  == 1) //bei letzte Bestellungen LAC-156
		{
			idx = 20;  // idx des Bestellvorschlags ist 20 !!!      
			flg_Bestellvorschlag = TRUE;
		}
        strcpy (vEinh, Otab[AufEinh[idx]]);
        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);


        dPos = (int) (dPos * 0.88);
        no_break_end ();
        ahWnd = AktivWindow;
//        EnableWindows (GetActiveWindow (), FALSE);
//        SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
        SetBorder (WS_POPUP | WS_VISIBLE |  WS_DLGFRAME);
//        hFind = OpenWindowChC (3, 38, 10, 20, hMainInst,
//                  "");
        hFind = OpenWindowCh (3, 38, dZeile, dPos, hMainInst);
        SetButtonTab (TRUE);
        SetCurrentField (0);
        EnableWindows (AktivWindow, FALSE);
        enter_form (hFind, &fDrkw,0, 0);
        SetButtonTab (FALSE);
        AktivWindow = ahWnd;
        EnableWindows (AktivWindow, TRUE);
        SetActiveWindow (AktivWindow);
		CloseControls (&fDrkw);
        DestroyWindow (hFind);
        hFind = NULL;
        no_break_end ();
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);

        if (syskey == KEY5) return 0;
        ChoiseCombo (idx);
		if (flg_letzteBestellung) //LAC-132 neg. Auftragsnummer == bei diesen Auftrag die IST-Werte �bernehmen
		{
			if (WgTab[AufEinh[idx]] > 0) 
			{
		        sprintf (Otab[0], "�bernahme %ld",WgTab[AufEinh[idx]] );
				WgTab[0] = WgTab[AufEinh[idx]] * -1; 

			}
		}
		return WgTab[AufEinh[idx]];
}

//LAC-9 E
