#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "dbclass.h"
#include "ag.h"

struct AG ag, ag_null;

void AG_CL::prepare (void)
{
            char *sqltext;

            sqlin ((long *)   &ag.ag,  SQLLONG, 0);
    out_quest ((char *) &ag.abt,1,0);
    out_quest ((char *) &ag.ag,2,0);
    out_quest ((char *) ag.ag_bz1,0,21);
    out_quest ((char *) ag.ag_bz2,0,21);
    out_quest ((char *) &ag.a_typ,1,0);
    out_quest ((char *) &ag.bearb_fil,1,0);
    out_quest ((char *) &ag.bearb_lad,1,0);
    out_quest ((char *) &ag.bearb_sk,1,0);
    out_quest ((char *) ag.best_auto,0,2);
    out_quest ((char *) ag.bsd_kz,0,2);
    out_quest ((char *) &ag.delstatus,1,0);
    out_quest ((char *) ag.durch_fil,0,2);
    out_quest ((char *) ag.durch_sk,0,2);
    out_quest ((char *) ag.durch_sw,0,2);
    out_quest ((char *) ag.durch_vk,0,2);
    out_quest ((char *) &ag.erl_kto,2,0);
    out_quest ((char *) &ag.hbk,1,0);
    out_quest ((char *) ag.hbk_kz,0,2);
    out_quest ((char *) &ag.hbk_ztr,1,0);
    out_quest ((char *) ag.hnd_gew,0,2);
    out_quest ((char *) ag.kost_kz,0,3);
    out_quest ((char *) &ag.me_einh,1,0);
    out_quest ((char *) &ag.mwst,1,0);
    out_quest ((char *) ag.pfa,0,2);
    out_quest ((char *) ag.pr_ueb,0,2);
    out_quest ((char *) ag.reg_eti,0,2);
    out_quest ((char *) &ag.sais1,1,0);
    out_quest ((char *) &ag.sais2,1,0);
    out_quest ((char *) &ag.sg1,1,0);
    out_quest ((char *) &ag.sg2,1,0);
    out_quest ((char *) ag.smt,0,2);
    out_quest ((char *) &ag.sp_fil,3,0);
    out_quest ((char *) ag.sp_kz,0,2);
    out_quest ((char *) &ag.sp_sk,3,0);
    out_quest ((char *) &ag.sp_vk,3,0);
    out_quest ((char *) ag.stk_lst_kz,0,2);
    out_quest ((char *) &ag.sw,3,0);
    out_quest ((char *) &ag.tara,3,0);
    out_quest ((char *) &ag.teil_smt,1,0);
    out_quest ((char *) ag.theke_eti,0,2);
    out_quest ((char *) ag.verk_art,0,2);
    out_quest ((char *) &ag.verk_beg,2,0);
    out_quest ((char *) &ag.verk_end,2,0);
    out_quest ((char *) ag.waren_eti,0,2);
    out_quest ((char *) &ag.we_kto,2,0);
    out_quest ((char *) &ag.wg,1,0);
    out_quest ((char *) &ag.gn_pkt_gbr,3,0);
    out_quest ((char *) ag.a_krz_kz,0,2);
    out_quest ((char *) ag.tier_kz,0,3);
    out_quest ((char *) &ag.erl_kto_1,2,0);
    out_quest ((char *) &ag.erl_kto_2,2,0);
    out_quest ((char *) &ag.erl_kto_3,2,0);
    out_quest ((char *) &ag.we_kto_1,2,0);
    out_quest ((char *) &ag.we_kto_2,2,0);
    out_quest ((char *) &ag.we_kto_3,2,0);
            cursor = sqlcursor ("select ag.abt,  ag.ag,  ag.ag_bz1,  "
"ag.ag_bz2,  ag.a_typ,  ag.bearb_fil,  ag.bearb_lad,  ag.bearb_sk,  "
"ag.best_auto,  ag.bsd_kz,  ag.delstatus,  ag.durch_fil,  ag.durch_sk,  "
"ag.durch_sw,  ag.durch_vk,  ag.erl_kto,  ag.hbk,  ag.hbk_kz,  ag.hbk_ztr,  "
"ag.hnd_gew,  ag.kost_kz,  ag.me_einh,  ag.mwst,  ag.pfa,  ag.pr_ueb,  "
"ag.reg_eti,  ag.sais1,  ag.sais2,  ag.sg1,  ag.sg2,  ag.smt,  ag.sp_fil,  "
"ag.sp_kz,  ag.sp_sk,  ag.sp_vk,  ag.stk_lst_kz,  ag.sw,  ag.tara,  ag.teil_smt,  "
"ag.theke_eti,  ag.verk_art,  ag.verk_beg,  ag.verk_end,  ag.waren_eti,  "
"ag.we_kto,  ag.wg,  ag.gn_pkt_gbr,  ag.a_krz_kz,  ag.tier_kz,  ag.erl_kto_1,  "
"ag.erl_kto_2,  ag.erl_kto_3,  ag.we_kto_1,  ag.we_kto_2,  ag.we_kto_3 from ag "

#line 18 "ag.rpp"
                                  "where ag = ?");
    ins_quest ((char *) &ag.abt,1,0);
    ins_quest ((char *) &ag.ag,2,0);
    ins_quest ((char *) ag.ag_bz1,0,21);
    ins_quest ((char *) ag.ag_bz2,0,21);
    ins_quest ((char *) &ag.a_typ,1,0);
    ins_quest ((char *) &ag.bearb_fil,1,0);
    ins_quest ((char *) &ag.bearb_lad,1,0);
    ins_quest ((char *) &ag.bearb_sk,1,0);
    ins_quest ((char *) ag.best_auto,0,2);
    ins_quest ((char *) ag.bsd_kz,0,2);
    ins_quest ((char *) &ag.delstatus,1,0);
    ins_quest ((char *) ag.durch_fil,0,2);
    ins_quest ((char *) ag.durch_sk,0,2);
    ins_quest ((char *) ag.durch_sw,0,2);
    ins_quest ((char *) ag.durch_vk,0,2);
    ins_quest ((char *) &ag.erl_kto,2,0);
    ins_quest ((char *) &ag.hbk,1,0);
    ins_quest ((char *) ag.hbk_kz,0,2);
    ins_quest ((char *) &ag.hbk_ztr,1,0);
    ins_quest ((char *) ag.hnd_gew,0,2);
    ins_quest ((char *) ag.kost_kz,0,3);
    ins_quest ((char *) &ag.me_einh,1,0);
    ins_quest ((char *) &ag.mwst,1,0);
    ins_quest ((char *) ag.pfa,0,2);
    ins_quest ((char *) ag.pr_ueb,0,2);
    ins_quest ((char *) ag.reg_eti,0,2);
    ins_quest ((char *) &ag.sais1,1,0);
    ins_quest ((char *) &ag.sais2,1,0);
    ins_quest ((char *) &ag.sg1,1,0);
    ins_quest ((char *) &ag.sg2,1,0);
    ins_quest ((char *) ag.smt,0,2);
    ins_quest ((char *) &ag.sp_fil,3,0);
    ins_quest ((char *) ag.sp_kz,0,2);
    ins_quest ((char *) &ag.sp_sk,3,0);
    ins_quest ((char *) &ag.sp_vk,3,0);
    ins_quest ((char *) ag.stk_lst_kz,0,2);
    ins_quest ((char *) &ag.sw,3,0);
    ins_quest ((char *) &ag.tara,3,0);
    ins_quest ((char *) &ag.teil_smt,1,0);
    ins_quest ((char *) ag.theke_eti,0,2);
    ins_quest ((char *) ag.verk_art,0,2);
    ins_quest ((char *) &ag.verk_beg,2,0);
    ins_quest ((char *) &ag.verk_end,2,0);
    ins_quest ((char *) ag.waren_eti,0,2);
    ins_quest ((char *) &ag.we_kto,2,0);
    ins_quest ((char *) &ag.wg,1,0);
    ins_quest ((char *) &ag.gn_pkt_gbr,3,0);
    ins_quest ((char *) ag.a_krz_kz,0,2);
    ins_quest ((char *) ag.tier_kz,0,3);
    ins_quest ((char *) &ag.erl_kto_1,2,0);
    ins_quest ((char *) &ag.erl_kto_2,2,0);
    ins_quest ((char *) &ag.erl_kto_3,2,0);
    ins_quest ((char *) &ag.we_kto_1,2,0);
    ins_quest ((char *) &ag.we_kto_2,2,0);
    ins_quest ((char *) &ag.we_kto_3,2,0);
            sqltext = "update ag set ag.abt = ?,  ag.ag = ?,  "
"ag.ag_bz1 = ?,  ag.ag_bz2 = ?,  ag.a_typ = ?,  ag.bearb_fil = ?,  "
"ag.bearb_lad = ?,  ag.bearb_sk = ?,  ag.best_auto = ?,  ag.bsd_kz = ?,  "
"ag.delstatus = ?,  ag.durch_fil = ?,  ag.durch_sk = ?,  "
"ag.durch_sw = ?,  ag.durch_vk = ?,  ag.erl_kto = ?,  ag.hbk = ?,  "
"ag.hbk_kz = ?,  ag.hbk_ztr = ?,  ag.hnd_gew = ?,  ag.kost_kz = ?,  "
"ag.me_einh = ?,  ag.mwst = ?,  ag.pfa = ?,  ag.pr_ueb = ?,  "
"ag.reg_eti = ?,  ag.sais1 = ?,  ag.sais2 = ?,  ag.sg1 = ?,  ag.sg2 = ?,  "
"ag.smt = ?,  ag.sp_fil = ?,  ag.sp_kz = ?,  ag.sp_sk = ?,  ag.sp_vk = ?,  "
"ag.stk_lst_kz = ?,  ag.sw = ?,  ag.tara = ?,  ag.teil_smt = ?,  "
"ag.theke_eti = ?,  ag.verk_art = ?,  ag.verk_beg = ?,  "
"ag.verk_end = ?,  ag.waren_eti = ?,  ag.we_kto = ?,  ag.wg = ?,  "
"ag.gn_pkt_gbr = ?,  ag.a_krz_kz = ?,  ag.tier_kz = ?,  "
"ag.erl_kto_1 = ?,  ag.erl_kto_2 = ?,  ag.erl_kto_3 = ?,  "
"ag.we_kto_1 = ?,  ag.we_kto_2 = ?,  ag.we_kto_3 = ? "

#line 20 "ag.rpp"
                                  "where ag = ?";
            sqlin ((long *)   &ag.ag,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &ag.ag,  SQLLONG, 0);
            test_upd_cursor = sqlcursor ("select ag from ag "
                                  "where ag = ?");
            sqlin ((long *)   &ag.ag,  SQLLONG, 0);
            del_cursor = sqlcursor ("delete from ag "
                                  "where ag = ?");
    ins_quest ((char *) &ag.abt,1,0);
    ins_quest ((char *) &ag.ag,2,0);
    ins_quest ((char *) ag.ag_bz1,0,21);
    ins_quest ((char *) ag.ag_bz2,0,21);
    ins_quest ((char *) &ag.a_typ,1,0);
    ins_quest ((char *) &ag.bearb_fil,1,0);
    ins_quest ((char *) &ag.bearb_lad,1,0);
    ins_quest ((char *) &ag.bearb_sk,1,0);
    ins_quest ((char *) ag.best_auto,0,2);
    ins_quest ((char *) ag.bsd_kz,0,2);
    ins_quest ((char *) &ag.delstatus,1,0);
    ins_quest ((char *) ag.durch_fil,0,2);
    ins_quest ((char *) ag.durch_sk,0,2);
    ins_quest ((char *) ag.durch_sw,0,2);
    ins_quest ((char *) ag.durch_vk,0,2);
    ins_quest ((char *) &ag.erl_kto,2,0);
    ins_quest ((char *) &ag.hbk,1,0);
    ins_quest ((char *) ag.hbk_kz,0,2);
    ins_quest ((char *) &ag.hbk_ztr,1,0);
    ins_quest ((char *) ag.hnd_gew,0,2);
    ins_quest ((char *) ag.kost_kz,0,3);
    ins_quest ((char *) &ag.me_einh,1,0);
    ins_quest ((char *) &ag.mwst,1,0);
    ins_quest ((char *) ag.pfa,0,2);
    ins_quest ((char *) ag.pr_ueb,0,2);
    ins_quest ((char *) ag.reg_eti,0,2);
    ins_quest ((char *) &ag.sais1,1,0);
    ins_quest ((char *) &ag.sais2,1,0);
    ins_quest ((char *) &ag.sg1,1,0);
    ins_quest ((char *) &ag.sg2,1,0);
    ins_quest ((char *) ag.smt,0,2);
    ins_quest ((char *) &ag.sp_fil,3,0);
    ins_quest ((char *) ag.sp_kz,0,2);
    ins_quest ((char *) &ag.sp_sk,3,0);
    ins_quest ((char *) &ag.sp_vk,3,0);
    ins_quest ((char *) ag.stk_lst_kz,0,2);
    ins_quest ((char *) &ag.sw,3,0);
    ins_quest ((char *) &ag.tara,3,0);
    ins_quest ((char *) &ag.teil_smt,1,0);
    ins_quest ((char *) ag.theke_eti,0,2);
    ins_quest ((char *) ag.verk_art,0,2);
    ins_quest ((char *) &ag.verk_beg,2,0);
    ins_quest ((char *) &ag.verk_end,2,0);
    ins_quest ((char *) ag.waren_eti,0,2);
    ins_quest ((char *) &ag.we_kto,2,0);
    ins_quest ((char *) &ag.wg,1,0);
    ins_quest ((char *) &ag.gn_pkt_gbr,3,0);
    ins_quest ((char *) ag.a_krz_kz,0,2);
    ins_quest ((char *) ag.tier_kz,0,3);
    ins_quest ((char *) &ag.erl_kto_1,2,0);
    ins_quest ((char *) &ag.erl_kto_2,2,0);
    ins_quest ((char *) &ag.erl_kto_3,2,0);
    ins_quest ((char *) &ag.we_kto_1,2,0);
    ins_quest ((char *) &ag.we_kto_2,2,0);
    ins_quest ((char *) &ag.we_kto_3,2,0);
            ins_cursor = sqlcursor ("insert into ag (abt,  ag,  "
"ag_bz1,  ag_bz2,  a_typ,  bearb_fil,  bearb_lad,  bearb_sk,  best_auto,  bsd_kz,  "
"delstatus,  durch_fil,  durch_sk,  durch_sw,  durch_vk,  erl_kto,  hbk,  hbk_kz,  "
"hbk_ztr,  hnd_gew,  kost_kz,  me_einh,  mwst,  pfa,  pr_ueb,  reg_eti,  sais1,  sais2,  "
"sg1,  sg2,  smt,  sp_fil,  sp_kz,  sp_sk,  sp_vk,  stk_lst_kz,  sw,  tara,  teil_smt,  "
"theke_eti,  verk_art,  verk_beg,  verk_end,  waren_eti,  we_kto,  wg,  gn_pkt_gbr,  "
"a_krz_kz,  tier_kz,  erl_kto_1,  erl_kto_2,  erl_kto_3,  we_kto_1,  we_kto_2,  "
"we_kto_3) "

#line 31 "ag.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 33 "ag.rpp"
}
