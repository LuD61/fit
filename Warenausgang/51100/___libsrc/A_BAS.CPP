#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#ifndef CONSOLE
#include "wmaskc.h"
#include "mo_meld.h"
#include "message.h"
#include "dbfunc.h"
#include "dbclass.h"
static MELDUNG Mess;
#endif
#ifdef MO_CURSC
#include "mo_cursc.h"
#else
#include "mo_curso.h"
#endif
#include "strfkt.h"
#include "a_bas.h"

static DB_CLASS DbClass;
static short cursor_a = -1;
static short cursor = -1;
static short cursor_a_ausw = -1;
static short cursor_a_ausw_bez = -1;
struct A_BAS _a_bas, _a_bas_null;

static char sqlstring [0x1000];
static char qstring0[0x1000] = {"Start"};
static int qschange;
static char qstring0_bez[0x1000] = {"Start"};
static int qschange_bez;

static int searchmode = 0;
static int searchfield = 0;
static char CharBuff [20] = {"\0"};
static BOOL CharBuffSet = FALSE;
static BOOL CharMess = TRUE;

static BOOL DisplayList = TRUE;

void SetCharBuffa_bas (char *buffer)
{
	     strcpy (CharBuff, buffer);
		 CharBuffSet = TRUE;
}

void FillArtUb  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%13s %-24s %s",
                          "Artikel", "Bezeichnung1", "Bezeichnung2");
}

static void FillArtValues  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%13.0lf %-24.24s %s", _a_bas.a,
                          _a_bas.a_bz1, _a_bas.a_bz2);
}

static void prepare_a (char *order)
{
    out_quest ((char *) &_a_bas.a,3,0);
    out_quest ((char *) &_a_bas.mdn,1,0);
    out_quest ((char *) &_a_bas.fil,1,0);
    out_quest ((char *) _a_bas.a_bz1,0,25);
    out_quest ((char *) _a_bas.a_bz2,0,25);
    out_quest ((char *) &_a_bas.a_gew,3,0);
    out_quest ((char *) &_a_bas.a_typ,1,0);
    out_quest ((char *) &_a_bas.a_typ2,1,0);
    out_quest ((char *) &_a_bas.abt,1,0);
    out_quest ((char *) &_a_bas.ag,2,0);
    out_quest ((char *) _a_bas.best_auto,0,2);
    out_quest ((char *) _a_bas.bsd_kz,0,2);
    out_quest ((char *) _a_bas.cp_aufschl,0,2);
    out_quest ((char *) &_a_bas.delstatus,1,0);
    out_quest ((char *) &_a_bas.dr_folge,1,0);
    out_quest ((char *) &_a_bas.erl_kto,2,0);
    out_quest ((char *) _a_bas.hbk_kz,0,2);
    out_quest ((char *) &_a_bas.hbk_ztr,1,0);
    out_quest ((char *) _a_bas.hnd_gew,0,2);
    out_quest ((char *) &_a_bas.hwg,1,0);
    out_quest ((char *) _a_bas.kost_kz,0,3);
    out_quest ((char *) &_a_bas.me_einh,1,0);
    out_quest ((char *) _a_bas.modif,0,2);
    out_quest ((char *) &_a_bas.mwst,1,0);
    out_quest ((char *) &_a_bas.plak_div,1,0);
    out_quest ((char *) _a_bas.stk_lst_kz,0,2);
    out_quest ((char *) &_a_bas.sw,3,0);
    out_quest ((char *) &_a_bas.teil_smt,1,0);
    out_quest ((char *) &_a_bas.we_kto,2,0);
    out_quest ((char *) &_a_bas.wg,1,0);
    out_quest ((char *) &_a_bas.zu_stoff,1,0);
    out_quest ((char *) &_a_bas.akv,2,0);
    out_quest ((char *) &_a_bas.bearb,2,0);
    out_quest ((char *) _a_bas.pers_nam,0,9);
    out_quest ((char *) &_a_bas.prod_zeit,3,0);
    out_quest ((char *) _a_bas.pers_rab_kz,0,2);
    out_quest ((char *) &_a_bas.gn_pkt_gbr,3,0);
    out_quest ((char *) &_a_bas.kost_st,2,0);
    out_quest ((char *) _a_bas.sw_pr_kz,0,2);
    out_quest ((char *) &_a_bas.kost_tr,2,0);
    out_quest ((char *) &_a_bas.a_grund,3,0);
    out_quest ((char *) &_a_bas.kost_st2,2,0);
    out_quest ((char *) &_a_bas.we_kto2,2,0);
    out_quest ((char *) _a_bas.a_bz3,0,25);
    out_quest ((char *) &_a_bas.charg_hand,2,0);
    out_quest ((char *) &_a_bas.intra_stat,2,0);
    out_quest ((char *) _a_bas.qual_kng,0,5);
    out_quest ((char *) &_a_bas.inh_ek,2,0);
   if (order)
    {
                       cursor = prepare_sql
                          ("select a_bas.a,  a_bas.mdn,  a_bas.fil,  "
"a_bas.a_bz1,  a_bas.a_bz2,  a_bas.a_gew,  a_bas.a_typ,  a_bas.a_typ2,  "
"a_bas.abt,  a_bas.ag,  a_bas.best_auto,  a_bas.bsd_kz,  a_bas.cp_aufschl,  "
"a_bas.delstatus,  a_bas.dr_folge,  a_bas.erl_kto,  a_bas.hbk_kz,  "
"a_bas.hbk_ztr,  a_bas.hnd_gew,  a_bas.hwg,  a_bas.kost_kz,  "
"a_bas.me_einh,  a_bas.modif,  a_bas.mwst,  a_bas.plak_div,  "
"a_bas.stk_lst_kz,  a_bas.sw,  a_bas.teil_smt,  a_bas.we_kto,  a_bas.wg,  "
"a_bas.zu_stoff,  a_bas.akv,  a_bas.bearb,  a_bas.pers_nam,  "
"a_bas.prod_zeit,  a_bas.pers_rab_kz,  a_bas.gn_pkt_gbr,  "
"a_bas.kost_st,  a_bas.sw_pr_kz,  a_bas.kost_tr,  a_bas.a_grund,  "
"a_bas.kost_st2,  a_bas.we_kto2, a_bas.a_bz3, a_bas.charg_hand, "
"a_bas.intra_stat, a_bas.qual_kng, a_bas.inh_ek from a_bas "
                          "where a > 0 and delstatus = 0 %s", order);
    }
    else
    {
                       cursor = prepare_sql
                          ("select a_bas.a,  a_bas.mdn,  a_bas.fil,  "
"a_bas.a_bz1,  a_bas.a_bz2,  a_bas.a_gew,  a_bas.a_typ,  a_bas.a_typ2,  "
"a_bas.abt,  a_bas.ag,  a_bas.best_auto,  a_bas.bsd_kz,  a_bas.cp_aufschl,  "
"a_bas.delstatus,  a_bas.dr_folge,  a_bas.erl_kto,  a_bas.hbk_kz,  "
"a_bas.hbk_ztr,  a_bas.hnd_gew,  a_bas.hwg,  a_bas.kost_kz,  "
"a_bas.me_einh,  a_bas.modif,  a_bas.mwst,  a_bas.plak_div,  "
"a_bas.stk_lst_kz,  a_bas.sw,  a_bas.teil_smt,  a_bas.we_kto,  a_bas.wg,  "
"a_bas.zu_stoff,  a_bas.akv,  a_bas.bearb,  a_bas.pers_nam,  "
"a_bas.prod_zeit,  a_bas.pers_rab_kz,  a_bas.gn_pkt_gbr,  "
"a_bas.kost_st,  a_bas.sw_pr_kz,  a_bas.kost_tr,  a_bas.a_grund,  "
"a_bas.kost_st2,  a_bas.we_kto2, a_bas.a_bz3, a_bas.charg_hand, "
"a_bas.intra_stat, a_bas.qual_kng, a_bas.inh_ek from a_bas "
                          "where a > 0 and delstatus = 0");
    }
}

static void prepare_a_bas (void)
{
    ins_quest ((char *) &_a_bas.a,3,0);
    out_quest ((char *) &_a_bas.a,3,0);
    out_quest ((char *) &_a_bas.mdn,1,0);
    out_quest ((char *) &_a_bas.fil,1,0);
    out_quest ((char *) _a_bas.a_bz1,0,25);
    out_quest ((char *) _a_bas.a_bz2,0,25);
    out_quest ((char *) &_a_bas.a_gew,3,0);
    out_quest ((char *) &_a_bas.a_typ,1,0);
    out_quest ((char *) &_a_bas.a_typ2,1,0);
    out_quest ((char *) &_a_bas.abt,1,0);
    out_quest ((char *) &_a_bas.ag,2,0);
    out_quest ((char *) _a_bas.best_auto,0,2);
    out_quest ((char *) _a_bas.bsd_kz,0,2);
    out_quest ((char *) _a_bas.cp_aufschl,0,2);
    out_quest ((char *) &_a_bas.delstatus,1,0);
    out_quest ((char *) &_a_bas.dr_folge,1,0);
    out_quest ((char *) &_a_bas.erl_kto,2,0);
    out_quest ((char *) _a_bas.hbk_kz,0,2);
    out_quest ((char *) &_a_bas.hbk_ztr,1,0);
    out_quest ((char *) _a_bas.hnd_gew,0,2);
    out_quest ((char *) &_a_bas.hwg,1,0);
    out_quest ((char *) _a_bas.kost_kz,0,3);
    out_quest ((char *) &_a_bas.me_einh,1,0);
    out_quest ((char *) _a_bas.modif,0,2);
    out_quest ((char *) &_a_bas.mwst,1,0);
    out_quest ((char *) &_a_bas.plak_div,1,0);
    out_quest ((char *) _a_bas.stk_lst_kz,0,2);
    out_quest ((char *) &_a_bas.sw,3,0);
    out_quest ((char *) &_a_bas.teil_smt,1,0);
    out_quest ((char *) &_a_bas.we_kto,2,0);
    out_quest ((char *) &_a_bas.wg,1,0);
    out_quest ((char *) &_a_bas.zu_stoff,1,0);
    out_quest ((char *) &_a_bas.akv,2,0);
    out_quest ((char *) &_a_bas.bearb,2,0);
    out_quest ((char *) _a_bas.pers_nam,0,9);
    out_quest ((char *) &_a_bas.prod_zeit,3,0);
    out_quest ((char *) _a_bas.pers_rab_kz,0,2);
    out_quest ((char *) &_a_bas.gn_pkt_gbr,3,0);
    out_quest ((char *) &_a_bas.kost_st,2,0);
    out_quest ((char *) _a_bas.sw_pr_kz,0,2);
    out_quest ((char *) &_a_bas.kost_tr,2,0);
    out_quest ((char *) &_a_bas.a_grund,3,0);
    out_quest ((char *) &_a_bas.kost_st2,2,0);
    out_quest ((char *) &_a_bas.we_kto2,2,0);
    out_quest ((char *) _a_bas.a_bz3,0,25);
    out_quest ((char *) &_a_bas.charg_hand,2,0);
    out_quest ((char *) &_a_bas.intra_stat,2,0);
    out_quest ((char *) _a_bas.qual_kng,0,5);
    out_quest ((char *) &_a_bas.inh_ek,2,0);
    cursor_a = prepare_sql
                          ("select a_bas.a,  a_bas.mdn,  a_bas.fil,  "
"a_bas.a_bz1,  a_bas.a_bz2,  a_bas.a_gew,  a_bas.a_typ,  a_bas.a_typ2,  "
"a_bas.abt,  a_bas.ag,  a_bas.best_auto,  a_bas.bsd_kz,  a_bas.cp_aufschl,  "
"a_bas.delstatus,  a_bas.dr_folge,  a_bas.erl_kto,  a_bas.hbk_kz,  "
"a_bas.hbk_ztr,  a_bas.hnd_gew,  a_bas.hwg,  a_bas.kost_kz,  "
"a_bas.me_einh,  a_bas.modif,  a_bas.mwst,  a_bas.plak_div,  "
"a_bas.stk_lst_kz,  a_bas.sw,  a_bas.teil_smt,  a_bas.we_kto,  a_bas.wg,  "
"a_bas.zu_stoff,  a_bas.akv,  a_bas.bearb,  a_bas.pers_nam,  "
"a_bas.prod_zeit,  a_bas.pers_rab_kz,  a_bas.gn_pkt_gbr,  "
"a_bas.kost_st,  a_bas.sw_pr_kz,  a_bas.kost_tr,  a_bas.a_grund,  "
"a_bas.kost_st2,  a_bas.we_kto2, a_bas.a_bz3, a_bas.charg_hand, "
"a_bas.intra_stat, a_bas.qual_kng, a_bas.inh_ek from a_bas "
                          "where a = ? and delstatus = 0");
   }

int lese_a (char *order)
/**
Tabelle a_bas lesen.
**/
{
         if (cursor == -1)
         {
                  prepare_a (order);
         }
         open_sql (cursor);
         fetch_sql (cursor);
         if (sqlstatus == 0)
         {
                return (0);
         }
         return (100);
}

int lese_a (void)
/**
Tabelle a_bas lesen.
**/
{
         if (cursor == -1)
         {
                return 100;
         }
         fetch_sql (cursor);
         if (sqlstatus == 0)
         {
                return (0);
         }
         return (100);
}



int lese_a_bas (double a)
/**
Tabelle a_bas lesen.
**/

{
         if (cursor_a == -1)
         {
                prepare_a_bas ();
/*
                ins_quest ((char *) &_a_bas.a, 3, 0);
                out_quest (_a_bas.a_bz1, 0, 24);
                out_quest (_a_bas.a_bz2, 0, 24);
                out_quest ((char *) &_a_bas.me_einh, 1, 0);
                out_quest ((char *) &_a_bas.a_typ, 1, 0);
                cursor_a = prepare_sql ("select a_bz1, a_bz2, me_einh, "
                                        "a_typ "
                                        "from a_bas "
                                        "where a = ?");
*/
         }
         _a_bas.a = a;
         open_sql (cursor_a);
         fetch_sql (cursor_a);
         if (sqlstatus == 0)
         {
                return (0);
         }
/*
#ifndef CONSOLE
         print_mess (0, "Artikel %.0lf nicht gefunden", _a_bas.a);
#endif
*/
         return (100);
}

#ifndef CONSOLE

extern HWND mamain1;

int Auswahla_basQuery ()
/**
Auswahl ueber a_bas mit Qzery-String.
**/
{

         if (StatusQuery ())
         {
                   sprintf (sqlstring ,
                            "select a, a_bz1, a_bz2 from a_bas "
                            "where a > 0 and %s order by a", qstring);
         }
         else
         {
                   strcpy (sqlstring ,
                           "select a, a_bz1, a_bz2 from a_bas "
                           "where a > 0 order by a");
         }
         return Showa_bas (sqlstring);
}

int Auswahla_bas ()
/**
Auswahl ueber a_bas (alle Artikel.
**/
{
         strcpy (sqlstring ,
                 "select a, a_bz1, a_bz2 from a_bas "
                 "where a > 0 order by a");
         return Showa_bas (sqlstring);
}

int Showa_bas (char *sqlstring)
/**
Auswahl ueber Artikel anzeigen.
**/
{
         int i;

         out_quest ((char *) &_a_bas.a, 3, 0);
         out_quest (_a_bas.a_bz1, 0, 24);
         out_quest (_a_bas.a_bz2, 0, 24);

         cursor_a_ausw = prepare_scroll (sqlstring);
         if (sqlstatus)
         {
                     return (-1);
         }
         i = DbAuswahl (cursor_a_ausw, FillArtUb, FillArtValues);
         UpdateFkt ();
         SetFocus (current_form->mask[currentfield].feldid);

         SetCurrentField (currentfield);

         if (syskey == KEYESC || syskey == KEY5)
         {
                     close_sql (cursor_a_ausw);
                     return 0;
         }
         fetch_scroll (cursor_a_ausw, DBABSOLUTE, i + 1);
         close_sql (cursor_a_ausw);
         return 0;
}


/* Variablen fuer Auswahl mit Buttonueberschrift    */

#include "itemc.h"
#define MAXSORT 30000
static long MaxSort;

static BOOL match_code = FALSE;
static int dosort1 ();
static int dosort2 ();
static int dosort3 ();
static int dosort4 ();

struct SORT_AW
{
	     char sort1 [14];
		 char sort2 [30];
		 char sort3 [30];
		 char sort4 [30];
		 int  sortidx;
};

static int sortl1 = 13;
static int sortl2 = 24;
static int sortl3 = 24;
static int sortl4 = 24;


// static struct SORT_AW sort_aw, sort_awtab[MAXSORT];
static struct SORT_AW sort_aw, *sort_awtab;

static void SortMalloc (void) 
/**
Speicher fuer Listbereich zuordnen.
**/
{
	static BOOL SortOK = FALSE;
    long anz;

	if (SortOK) return;

	DbClass.sqlout ((long *) &anz, 2, 0);
	DbClass.sqlcomm ("select count (*) from a_bas");

	if (anz == 0l) return;

    sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   ((anz + 1) * sizeof (struct SORT_AW))); 
	while (sort_awtab == NULL)
	{
		if (anz == 0l) break;
		anz --;
        sort_awtab = (struct SORT_AW *) GlobalAlloc (GMEM_FIXED, 
		                                   (anz * sizeof (struct SORT_AW))); 
	}
	MaxSort = anz + 1;
	if (anz == 0l)
	{
		print_mess (2, "Es konnte kein Speicher fuer die Artikelauswahl zugewiesen werden");
		return;
	}
	SortOK = TRUE;
}


static int sort_awanz;
static int sort1 = 1;
static int sort2 = 1;
static int sort3 = 1;
static int sort4 = 1;
static int sort_idx = 0;

static ITEM isort1    ("", sort_aw.sort1, "", 0);
static ITEM isort2    ("", sort_aw.sort2, "", 0);
static ITEM isort3    ("", sort_aw.sort3, "", 0);
static ITEM isort4    ("", sort_aw.sort4, "", 0);

static field _fsort_aw[] = {
&isort1,     14, 1, 0, 1, 0, "%13.0lf", NORMAL, 0, 0, 0,
&isort2,     25, 1, 0,16, 0, "",        NORMAL, 0, 0, 0,
&isort3,     25, 1, 0,42, 0, "",        NORMAL, 0, 0, 0,
};

static field _fsort_awEx[] = {
&isort1,     13, 1, 0, 1, 0, "%13.0lf", NORMAL, 0, 0, 0,
&isort2,     25, 1, 0,16, 0, "",        NORMAL, 0, 0, 0,
&isort3,     25, 1, 0,42, 0, "",        NORMAL, 0, 0, 0,
&isort4,     25, 1, 0,68, 0, "",        NORMAL, 0, 0, 0,
};

static form fsort_aw = {3, 0, 0, _fsort_aw, 0, 0, 0, 0, NULL};

static ITEM isort1ub    ("", "Artikel-Nummer", "", 0);
static ITEM isort2ub    ("", "Bezeichnung 1",  "", 0);
static ITEM isort3ub    ("", "Bezeichnung 2",  "",0);
static ITEM isort4ub    ("", "Match-Code   ",  "",0);
static ITEM ifillub     ("", " ",              "", 0); 

static field _fsort_awub[] = {
&isort1ub,       15, 1, 0, 0, 0, "", BUTTON, 0, dosort1,    102,
&isort2ub,       26, 1, 0,15, 0, "", BUTTON, 0, dosort2,    103,
&isort3ub,       26, 1, 0,41, 0, "", BUTTON, 0, dosort3,    104,
&ifillub,        80, 1, 0,67, 0, "", BUTTON, 0, 0, 0};

static field _fsort_awubEx[] = {
&isort1ub,       15, 1, 0, 0, 0, "", BUTTON, 0, dosort1,    102,
&isort2ub,       26, 1, 0,15, 0, "", BUTTON, 0, dosort2,    103,
&isort3ub,       26, 1, 0,41, 0, "", BUTTON, 0, dosort3,    104,
&isort4ub,       26, 1, 0,67, 0, "", BUTTON, 0, dosort4,    105,
&ifillub,        80, 1, 0,93, 0, "", BUTTON, 0, 0, 0};

static form fsort_awub = {4, 0, 0, _fsort_awub, 0, 0, 0, 0, NULL};

static ITEM isortl ("", "1", "", 0);

static field _fsort_awl[] = {
&isortl,          1, 1, 0, 15, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 41, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 67, 0, "", NORMAL, 0, 0, 0,
};

static field _fsort_awlEx[] = {
&isortl,          1, 1, 0, 15, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 41, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 67, 0, "", NORMAL, 0, 0, 0,
&isortl,          1, 1, 0, 93, 0, "", NORMAL, 0, 0, 0,
};

static form fsort_awl = {3, 0, 0, _fsort_awl, 0, 0, 0, 0, NULL};


static void SortInit (char *sort, int len)
/**
**/
{
	       memset (sort, ' ', len);
		   sort [len] = 0;
}
          

static void InitSort (int pos)
{
	       SortInit (sort_awtab [pos].sort1, sortl1);
	       SortInit (sort_awtab [pos].sort2, sortl2);
	       SortInit (sort_awtab [pos].sort3, sortl3);
	       SortInit (sort_awtab [pos].sort4, sortl4);
}


void SetMatchCode (BOOL mcode)
{
	     match_code = min (1, mcode);
		 if (match_code)
		 {
			 fsort_aw.fieldanz = 4;
			 fsort_aw.mask = _fsort_awEx;
			 fsort_awub.fieldanz = 5;
			 fsort_awub.mask = _fsort_awubEx;
			 fsort_awl.fieldanz = 4;
			 fsort_awl.mask = _fsort_awlEx;
		 }
		 else 
		 {
			 fsort_aw.fieldanz = 3;
			 fsort_aw.mask = _fsort_aw;
			 fsort_awub.fieldanz = 4;
			 fsort_awub.mask = _fsort_awub;
			 fsort_awl.fieldanz = 3;
			 fsort_awl.mask = _fsort_awl;
		 }
}


static int dosort10 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
		  if (ratod (el1->sort1) > ratod (el2->sort1))
          {
			           return sort1;
		  }
				                                
		  else if (ratod (el1->sort1) < ratod(el2->sort1))
          {
			           return (-1 * sort1);
		  }
		  return 0;
}


static int dosort1 ()
{
	if (searchmode == 1)
	{
		SetSearchField (0);
        SetCharBuffTxt (fsort_awub.mask[0].item->GetFeldPtr (), 1);
        if (CharMess) SetCharBuffMess (" ");
	}
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort10);
	if (DisplayList)
	{
	    if (searchmode != 1) sort1 *= -1;
        ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	}
	return 0;
}


static int dosort20 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
	      return (strcmp (el1->sort2,el2->sort2) * sort2);
}


static int dosort2 ()
{
	if (searchmode == 1)
	{
		SetSearchField (1);
        SetCharBuffTxt (fsort_awub.mask[1].item->GetFeldPtr (), 1);
        if (CharMess) SetCharBuffMess (" ");
	}
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort20);
	if (DisplayList)
	{
	        if (searchmode != 1) sort2 *= -1;
            ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	}
	return 0;
}
static int dosort30 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
          return (strcmp (el1->sort3,el2->sort3) * sort3);
}


static int dosort3()
{
	if (searchmode == 1)
	{
		SetSearchField (2);
        SetCharBuffTxt (fsort_awub.mask[2].item->GetFeldPtr (), 1);
        if (CharMess) SetCharBuffMess (" ");
	}
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort30);
	if (DisplayList)
	{
	       if (searchmode != 1) sort3 *= -1;
           ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	}
	return 0;
}

static int dosort40 (const void *elem1, const void *elem2)
{
	      struct SORT_AW *el1; 
	      struct SORT_AW *el2; 

		  el1 = (struct SORT_AW *) elem1;
		  el2 = (struct SORT_AW *) elem2;
		  if (strcmp (el1->sort4, el2->sort4))
		  {
	                return (strcmp (el1->sort4,el2->sort4) * sort4);
		  }
          return (strcmp (el1->sort2,el2->sort2) * sort4);
}


static int dosort4 ()
{
	if (searchmode == 1)
	{
		SetSearchField (3);
        SetCharBuffTxt (fsort_awub.mask[3].item->GetFeldPtr (), 1);
        if (CharMess) SetCharBuffMess (" ");
	}
	SetFocus (Getlbox());
	qsort (sort_awtab, sort_awanz, sizeof (struct SORT_AW),
				   dosort40);
	if (DisplayList)
	{
	    if (searchmode != 1) sort4 *= -1;
        ShowNewElist ((char *) sort_awtab,
                           sort_awanz,
                           (int) sizeof (struct SORT_AW));
	}
	return 0;
}
static void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        sort_idx = idx;
        break_list ();
        return;
}

static int endsort (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

int Showa_basBu (HWND hWnd, int ws_flag, char *sqlstring)
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		HWND eWindow;
	    form *savecurrent;

		SortMalloc ();
        out_quest ((char *) &_a_bas.a, 3, 0);
        out_quest (_a_bas.a_bz1, 0, 24);
        out_quest (_a_bas.a_bz2, 0, 24);
        out_quest (_a_bas.a_bz3, 0, 24);

        cursor_a_ausw = prepare_scroll (sqlstring);
        if (sqlstatus)
        {
                     return (-1);
        }

		sort_awanz = 0;
		while (fetch_scroll (cursor_a_ausw, NEXT) == 0)
        {
			         sprintf (sort_awtab[sort_awanz].sort1, "%13.0lf",
						      _a_bas.a);
					 strcpy (sort_awtab[sort_awanz].sort2, 
						     _a_bas.a_bz1);
					 strcpy (sort_awtab[sort_awanz].sort3, 
						     _a_bas.a_bz2);
					 strcpy (sort_awtab[sort_awanz].sort4, 
						     _a_bas.a_bz3);
					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 sort_awanz ++;
					 if (sort_awanz == MaxSort) 
					 {
						      print_mess (2, "Es konnten nicht alle Artikel eingelesen werden");       
						      break;
					 }
					 if (sort_awanz == MAXSORT) break;
        }

	    savecurrent = current_form;
		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
        eWindow = OpenListWindowEnF (10, 40, 8, 20, 0);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
		Setlistenter (1);
        ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);

        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_a_ausw);
                     return 0;
         }

		 sort_idx = sort_awtab[sort_idx].sortidx;
         fetch_scroll (cursor_a_ausw, DBABSOLUTE, sort_idx);
         close_sql (cursor_a_ausw);
         return 0;
}

int Auswahla_basBu (HWND hWnd, int ws_flag)
/**
Auswahl ueber a_bas (alle Artikel.
**/
{
         strcpy (sqlstring ,
                 "select a, a_bz1, a_bz2, a_bz3 from a_bas "
                 "where a > 0 order by a");
         return Showa_basBu (hWnd, ws_flag, sqlstring);
}

void ListToMamain1 (HWND eWindow)
/**
Fenster an mamain1 anpassen.
**/
{
	    RECT rect;

		GetWindowRect (mamain1, &rect);

		rect.left ++;
		rect.top ++;
		rect.right = rect.right - rect.left - 1;
		rect.bottom = rect.bottom - rect.top - 1;
		MoveWindow (eWindow, rect.left, rect.top, rect.right, rect.bottom, TRUE);
}


int Showa_basQuery (HWND hWnd, int ws_flag)
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		HWND eWindow;
	    form *savecurrent;
		static struct LST lst;

		SortMalloc ();
		SetCaption ("Auswahl �ber Artikel");
           SetBorder (WS_VISIBLE | WS_CAPTION |WS_THICKFRAME);
		if (searchmode == 1)
		{
                  SetCharBuffTxt (fsort_awub.mask[searchfield].item->GetFeldPtr (), 0);
                  SetBorder (WS_VISIBLE | WS_CAPTION |WS_THICKFRAME);
                  eWindow = OpenListWindowChoise (17, 73, 6, 2, 0);
		}
		else
		{
                  eWindow = OpenListWindowEnF (17, 73, 6, 2, 0);
		}
		ListToMamain1 (eWindow);
        if (qschange)
        {
  	         Mess.WaitWindow ("Die Artikel werden eingelesen ");
  		     sort_awanz = 0;
		     while (fetch_scroll (cursor_a_ausw, NEXT) == 0)
             {
				     InitSort (sort_awanz);
			         sprintf (sort_awtab[sort_awanz].sort1, "%13.0lf",
						      _a_bas.a);
					 strcpy (sort_awtab[sort_awanz].sort2, 
						     _a_bas.a_bz1);
					 strcpy (sort_awtab[sort_awanz].sort3, 
						     _a_bas.a_bz2);
					 strcpy (sort_awtab[sort_awanz].sort4, 
						     _a_bas.a_bz3);

					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 memcpy (&sort_aw, &sort_awtab [sort_awanz],
                                       (int) sizeof (struct SORT_AW));
//                     SetElistRow (&lst.menue, &fsort_aw, sort_awanz);
					 sort_awanz ++;
					 if (sort_awanz == MaxSort) 
					 {
						      print_mess (2, "Es konnten nicht alle Artikel eingelesen werden");       
						      break;
					 }
					 if (sort_awanz == MAXSORT) break;
             }
 	         Mess.CloseWaitWindow ();
  		     if (searchmode == 1)
			 {
				   CharMess = FALSE;
				   DisplayList = FALSE;
			      (*fsort_awub.mask[searchfield].after) ();
				   DisplayList = TRUE;
				   CharMess = TRUE;
			 }
        }
		else
		{
			 SetAktLst (&lst);
		}

	    savecurrent = current_form;
		save_fkt (5); 
        set_fkt (endsort, 5);

        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
		SetSearchField (searchfield);
		SetSearchMode (searchmode);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
        Setlistenter (1);
		if (qschange)
		{

/*
            ShowElistMen (&lst.menue, (char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
*/

            ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);

                   SetCharBuff (CharBuff);
				   CharBuffSet = FALSE;
		}
		else
		{
			 SetAktLst (&lst);
 		     if (searchmode == 1)
			 {
				   CharMess = FALSE;
			      (*fsort_awub.mask[searchfield].after) ();
				   CharMess = TRUE;
			 }

			 if (CharBuffSet)
			 {
                   SetCharBuff (CharBuff);
				   CharBuffSet = FALSE;
			 }
		}
        SetSaveList (&lst);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
		strcpy (CharBuff, lst.CharBuff);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);

        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_a_ausw);
                     return 0;
         }

//		 sort_idx = sort_awtab[sort_idx].sortidx;
//         open_sql (cursor_a_ausw);
//         fetch_scroll (cursor_a_ausw, DBABSOLUTE, sort_idx);
         _a_bas.a = ratod (sort_awtab[sort_idx].sort1);
         strcpy (_a_bas.a_bz1,sort_awtab[sort_idx].sort2); 
         strcpy (_a_bas.a_bz2,sort_awtab[sort_idx].sort3); 
         strcpy (_a_bas.a_bz3,sort_awtab[sort_idx].sort4); 
         return 0;
}


int Preparea_basQuery (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         char sqlstring [1000]; 
         short old_mode;
         char *order;

         ReadQuery (qform, qnamen); 

         if (strcmp (qstring0, qstring) == 0)
         {
                   qschange = 0;
         }
         else
         {
                   qschange = 1;
         }

         close_sql (cursor_a_ausw);
         strcpy (qstring0, qstring);
         if (StatusQuery ())
         {
                   if (strstr (qstring, "a_bz1"))
                   {
                             order = "order by a_bz1";
                   }
                   else
                   {
                             order = "order by a";
                   }
                   sprintf (sqlstring ,
                     "select a, a_bz1, a_bz2, a_bz3 from a_bas "
                     "where a > 0  and delstatus = 0 and %s %s",
					  qstring, order);
         }
         else
         {
			      if (match_code)
				  {
                       strcpy (sqlstring ,
                             "select a, a_bz1, a_bz2, a_bz3 from a_bas "
                              "where a > 0  and delstatus = 0 order by a_bz3, a_bz1");
				  }
				  else
				  {
                       strcpy (sqlstring ,
                             "select a, a_bz1, a_bz2, a_bz3 from a_bas "
                              "where a > 0  and delstatus = 0 order by a_bz1");
				  }
         }

         out_quest ((char *) &_a_bas.a, 3, 0);
         out_quest (_a_bas.a_bz1, 0, 25);
         out_quest (_a_bas.a_bz2, 0, 25);
         out_quest (_a_bas.a_bz3, 0, 25);

         old_mode = sql_mode;
         sql_mode = 2;
         cursor_a_ausw = prepare_scroll (sqlstring);
         sql_mode = old_mode;
         if (cursor_a_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}


int Preparea_basQuery (void)
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         char sqlstring [1000]; 
         short old_mode;

         qstring[0] = (char) 0; 

         if (strcmp (qstring0, qstring) == 0)
         {
                   qschange = 0;
         }
         else
         {
                   qschange = 1;
         }

         close_sql (cursor_a_ausw);
         strcpy (qstring0, qstring);
		 if (match_code)
		 {
                  strcpy (sqlstring ,
                     "select a, a_bz1, a_bz2,a_bz3 from a_bas "
                     "where a > 0  and delstatus = 0 order by a_bz3, a_bz1");
		 }
		 else
		 {
                  strcpy (sqlstring ,
                     "select a, a_bz1, a_bz2,a_bz3 from a_bas "
                     "where a > 0  and delstatus = 0 order by a_bz1");
		 }

         out_quest ((char *) &_a_bas.a, 3, 0);
         out_quest (_a_bas.a_bz1, 0, 25);
         out_quest (_a_bas.a_bz2, 0, 25);
         out_quest (_a_bas.a_bz3, 0, 25);

         old_mode = sql_mode;
         sql_mode = 2;
         cursor_a_ausw = prepare_scroll (sqlstring);
         sql_mode = old_mode;
         if (cursor_a_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}


int Showa_basQuery_Bez (HWND hWnd, int ws_flag)
/**
Auswahl ueber Gruppen anzeigen.
**/
{
		HWND eWindow;
	    form *savecurrent;
		static struct LST lst;


		SortMalloc ();
        if (qschange_bez)
        {
  		     sort_awanz = 0;
		     while (fetch_scroll (cursor_a_ausw_bez, NEXT) == 0)
             {
				     InitSort (sort_awanz);
			         sprintf (sort_awtab[sort_awanz].sort1, "%13.0lf",
						      _a_bas.a);
					 strcpy (sort_awtab[sort_awanz].sort2, 
						     _a_bas.a_bz1);
					 strcpy (sort_awtab[sort_awanz].sort3, 
						     _a_bas.a_bz2);
					 strcpy (sort_awtab[sort_awanz].sort4, 
						     _a_bas.a_bz3);
					 sort_awtab[sort_awanz].sortidx = sort_awanz + 1;
					 sort_awanz ++;
					 if (sort_awanz == MaxSort) 
					 {
						      print_mess (2, "Es konnten nicht alle Artikel eingelesen werden");       
						      break;
					 }
					 if (sort_awanz == MAXSORT) break;
             }
        }

	    savecurrent = current_form;
		save_fkt (5); 
        set_fkt (endsort, 5);
        SetDblClck (IsAwClck, 1);
        SetAktivWindow (hWnd);
        SetListEWindow (1);
		SetHLines (ws_flag);
		SetVLines (TRUE);
		SetMouseLock (TRUE);
		SetCaption ("Auswahl �ber Artikel");
		SetSearchMode (searchmode);
		SetSearchField (searchfield);
        SetBorder (WS_VISIBLE | WS_CAPTION |WS_THICKFRAME);
        eWindow = OpenListWindowEnF (20, 70, 4, 5, 0);
        ElistVl (&fsort_awl);
        ElistUb (&fsort_awub);
        Setlistenter (1);
		if (qschange_bez)
		{
            ShowElist ((char *) sort_awtab,
                    sort_awanz,
                   (char *) &sort_aw,
                   (int) sizeof (struct SORT_AW),
                   &fsort_aw);
		}
		else
		{
			 SetAktLst (&lst);
		}
	    SetSaveList (&lst);
        EnterElist (eWindow, (char *) sort_awtab,
                             sort_awanz,
                             (char *) &sort_aw,
                             (int) sizeof (struct SORT_AW),
                             &fsort_aw);
	    current_form = savecurrent;
		restore_fkt (5); 
		SetMouseLock (FALSE);
        CloseUbControls (); 
        CloseEWindow (eWindow);
        SetListFont (FALSE);
        SetDblClck (NULL, 1);

        if (syskey == KEYESC || syskey == KEY5)
        {
                     close_sql (cursor_a_ausw_bez);
                     return 0;
         }

//		 sort_idx = sort_awtab[sort_idx].sortidx;
//         open_sql (cursor_a_ausw_bez);
//         fetch_scroll (cursor_a_ausw_bez, DBABSOLUTE, sort_idx);
         _a_bas.a = ratod (sort_awtab[sort_idx].sort1);
         strcpy (_a_bas.a_bz1,sort_awtab[sort_idx].sort2); 
         strcpy (_a_bas.a_bz2,sort_awtab[sort_idx].sort3); 
         strcpy (_a_bas.a_bz3,sort_awtab[sort_idx].sort4); 
         return 0;
}


int Preparea_basQuery_Bez (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         extern short sql_mode;
         char sqlstring [1000]; 
         short old_mode;
         char *order;

         ReadQuery (qform, qnamen); 

         if (strcmp (qstring0_bez, qstring) == 0)
         {
                   qschange_bez = 0;
         }
         else
         {
                   qschange_bez = 1;
         }

         close_sql (cursor_a_ausw_bez);
         strcpy (qstring0_bez, qstring);
         if (StatusQuery ())
         {
                   if (strstr (qstring, "a_bz1"))
                   {
                             order = "order by a_bz1";
                   }
                   else
                   {
                             order = "order by a";
                   }
                   sprintf (sqlstring ,
                     "select a, a_bz1, a_bz2, a_bz3 from a_bas "
                     "where a > 0 and delstatus = 0 and %s %s",
					  qstring, order);
         }
         else
         {
                  if (match_code)
				  {
                         strcpy (sqlstring ,
                              "select a, a_bz1, a_bz2, a_bz3 from a_bas "
                              "where a > 0  and delstatus = 0 order by a_bz1");
				  }
				  else
				  {
                         strcpy (sqlstring ,
                              "select a, a_bz1, a_bz2, a_bz3 from a_bas "
                              "where a > 0  and delstatus = 0 order by a_bz3");
				  }
         }

         out_quest ((char *) &_a_bas.a, 3, 0);
         out_quest (_a_bas.a_bz1, 0, 25);
         out_quest (_a_bas.a_bz2, 0, 25);
         out_quest (_a_bas.a_bz3, 0, 25);

         old_mode = sql_mode;
         sql_mode = 2;
         cursor_a_ausw_bez = prepare_scroll (sqlstring);
         sql_mode = old_mode;
         if (cursor_a_ausw == -1)
         {
                       return (-1);
         }
         return (0);
}


void SetBasSearchMode (int mode)
{
	      searchmode = min (2,mode);
}


void SetBasSearchField (int fieldnr)
{
	      searchfield = fieldnr;
}


/* Ende Variablen fuer Auswahl mit Buttonueberschrift    */


#endif  // CONSOLE
