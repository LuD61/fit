#ifndef SINIX
#ident  "@(#) strfkt.c	2.000f	19.09.95	by RO"
#endif
/***
_-------------------------------------------------------------------------------
-
-       BIZERBA         System-Engineering         TE-IS-W
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/12-0
-
--------------------------------------------------------------------------------
-
-       Modulname               :       strfkt.c
-

-       Interne Prozeduren      :  
                                        
-
-       Autor                   :       RO
-       Erstellungsdatum        :       17.01.92
-       Modifikationsdatum      :       TT.MM.JJ
-       Modifikationsdatum      :       TT.MM.JJ
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       UNIX 
-       Sprache                 :       ESQL-C
-
-       Aenderungsjournal       :
-
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :   Stringfunktionen
-
-
-	lfd. -	Version	Datum	    Name	Beschreibung
-       ----------------------------------------------------------------------
-       
--------------------------------------------------------------------------------
***/

#include<string.h>
#include<malloc.h>

#define char_diff ('a' - 'A')
#define TAB 9

/*  Exports                        */

char *strstr ();
char *strstrs ();
int  instring ();
int matchcmp ();
int frzcmp ();
int mtchcmp ();
char * strstr ();
char *strstrs ();
char * upstrstr ();
char *cmpcmp ();
int strupcmp ();
int numeric ();
void upmem ();
void upstr ();
float ratof ();
double ratod ();
void getstr ();
char *clipped ();
char *clipp ();
char *sclipp ();
char *make_name (char *,char *);
char *env_mname (char *, char *);
char *env_name (char *, char *, char *);
char *basename ();
void cr_weg ();

/* Imports             */

/* char *malloc (); */
char *getenv ();

int instring(ziel,teilstr)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       instring
-
-       In              :       string
                                teilstring
-
-       Out             :       = 0  String ist in Teilestring
                               "= 0  String ist nicht in Teilstring
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Test,ob ein Teilstrings in einem String 
                                vorhanden ist..
-
--------------------------------------------------------------------------------
**/

char *ziel, *teilstr;
{

      if (strchr (teilstr,'*'))
                 return (matchcmp (ziel,teilstr));
      if (strchr (teilstr,'?'))
                 return (matchcmp (ziel,teilstr));

      return (strncmp(ziel,teilstr,strlen (teilstr)));
}


int matchcmp(string,mtch)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       matchcmp
-
-       In              :       string
                                mtch
-
-       Out             :       = 0  mtch ist in string
                               "= 0  mtch ist nicht in string
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Vergleich von String mit mtch.
                                In mtch darf '*' oder '?' stehen.
-
--------------------------------------------------------------------------------
**/
char *string, *mtch;
{
      char *pos1;

      pos1 = strchr(mtch,'*');   /* Mit '*' ? */
      if (pos1 == 0)
      {
	     return(frzcmp(string,mtch));   /* Nein, '?' pruefen   */
      }
      else
      {
             return(mtchcmp(mtch,string));  /* Ja, '*' pruefen    */
      }
}


int frzcmp(dname,mtch)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       frzcmp
-
-       In              :       dname
                                mtch
-
-       Beschreibung    :       Vergleich von String mit mtch.
                                In mtch duerfen '?' stehen.
-
--------------------------------------------------------------------------------
**/
char *dname;
char *mtch;
{
      char *pos;
      int i, len;

      pos = strchr(mtch,'?');
      if (pos == 0)
	       return(strcmp(dname,mtch));

      len = strlen(dname);
      if (len != strlen(mtch))
	       return(-1);

      for (i = 0; i < len; i ++)
      {
	       if ((mtch[i] != '?') && (mtch[i] != dname[i]))
			          return(-1);
      }
      return(0);
}


int mtchcmp(dname1,dname2)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       mtchcmp
-
-       In              :       dname
                                mtch
-
-       Beschreibung    :       Vergleich von String mit mtch.
                                In mtch darf '*' stehen.
-
--------------------------------------------------------------------------------
**/
char *dname1;
char *dname2;
{
      char *spos;
      char *ppos;
      char dname3[16];
      char *ppos2;
      int len;
      int cp;

      spos = strchr(dname1,'*');
      if (spos == 0)
      {
		  return(strcmp(dname1,dname2));
      } 
      return(cmpmtch(dname1,dname2,strlen(dname1)));
}


cmpmtch(dname1,dname2,n)
char *dname1;
char *dname2;
int n;
{
       char *spos, *spos2;
       int ret;

       if (strcmp (dname1,"*") == 0)
       {
             return (0);
       }

       spos = strchr(dname1,'*');
       if (spos == 0)
       {
	     return(strncmp(dname1,dname2,n));
       }

       else if (spos == dname1)
       {
             spos2 = strstrs (dname2,spos + 1);
             if (spos2 == 0)
             {
                         return (1); 
             }
             return (cmpmtch (spos + 1,spos2,strlen (spos + 1)));
       }

       ret = strncmp(dname1,dname2,(int) (spos - dname1));
       if (ret != 0)
       {
             return (ret);
       }

       spos2 = dname2 + (int) (spos - dname1);
       return (cmpmtch (spos,spos2,strlen (spos)));
}

char *strstr(str1,str2)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       strstr
-
-       In              :       str1
                                str2
-
-       Out             :      Position von str2 in str1 oder 0.
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Position von str2 in str1 suchen.
-
--------------------------------------------------------------------------------
**/
char *str1; 
char *str2;
{
       char *strend1, *strend2;
       int len;

       strend1 = str1 + strlen(str1);
       strend2 = str2 + strlen(str2);
       len = strlen(str2);

       for (;str1 < strend1; str1 ++)
       {
	    if (*str1 == *str2)
	    {
		    if (strncmp(str1,str2,len) == 0)
			       return(str1);
            }
       }
       return (0);
 }

char * strstrs(str1,str2)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       strstrs
-
-       In              :       string
                                mtch
-
-       Out             :      Position von str2 in str1 oder 0.
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Position von str2 in str1 suchen.
                                In str2 darf '*' stehen.
-
--------------------------------------------------------------------------------
**/
char *str1, *str2;
{
       char *strend1, *strend2;
       char *slen;
       int len;

       strend1 = str1 + strlen(str1);
       strend2 = str2 + strlen(str2);
       slen = strchr (str2,'*');
       if (slen == 0)
       {
                  len = strlen(str2);
       }
       else
       {
                  len = slen - str2; 
       }

       for (;str1 < strend1; str1 ++)
       {
	    if (*str1 == *str2)
	    {
		    if (strncmp(str1,str2,len) == 0)
			       return(str1);
            }
       }
       return (0);
 }


char * upstrstr(str1,str2)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       upstrstr
-
-       In              :       string
                                mtch
-
-       Out             :      Position von str2 in str1 oder 0.
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Position von str2 in str1 suchen.
                                Gross - und Kleinschreibung wird ignoriert.
-
--------------------------------------------------------------------------------
**/
char *str1, *str2;
{
       char *strend1, *strend2;
       int len;
       char upstr1, upstr2;

       strend1 = str1 + strlen(str1);
       strend2 = str2 + strlen(str2);
       len = strlen(str2);
       upstr2 = toupper(*str2);

       for (;str1 < strend1; str1 ++)
       {
	    if (*str1 == *str2)
	    {
		    if (strncmp(str1,str2,len) == 0)
			       return(str1);
            }
            upstr1 = toupper(*str1);
            if (upstr1 == upstr2)
            {
                     if (strupcmp (str1,str2,len) == 0)
                               return (str1);
            }
       }
       return (0);
 }

char * cmpcmp(str1,str2,slen,clen)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       cmpcmp
-
-       In              :       str1
                                str2
                                slen
                                clen
-
-       Out             :      Position von str2 in str1 oder 0.
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Position von str2 in str1 suchen.
                                Es werden slen Zeichen von str1 mit
                                String str2 der Laenge clen verglichen. 
                                
-
--------------------------------------------------------------------------------
**/
char *str1, *str2;
int slen,clen;
{
       int len;
       char *strend;

       len = slen - clen;
       strend = str1 + len;

       if (len < 1)
       {
             return (0);
       }

       for (;str1 < strend; str1 ++)
       {
	    if (*str1 == *str2)
	    {
		    if (strncmp(str1,str2,clen) == 0)
			       return(str1);
            }
       }
       return (0);
 }


int strupcmp (str1,str2,len)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       strupcmp
-
-       In              :       str1
                                str2
                                len
-
-       Out             :      0     str1 = str2.
                               sonst str1 != str2
-
-       Errorcodes      :       -
-
-       Beschreibung    :       str1 mit str2 in der Laenge len vergleichen.
                                Gross - und Kleinschreibung wird ignoriert.
-
--------------------------------------------------------------------------------
**/
char *str1, *str2;
int len;
{
       short i;
       char upstr1;
       char upstr2;


       for (i = 0; i < len; i ++, str1 ++, str2 ++)
       {
                    if (*str1 == 0)
                                 return (-1);
                    if (*str2 == 0)
                                 return (1);
                    upstr1 = toupper(*str1);
                    upstr2 = toupper(*str2);
                    if (upstr1 < upstr2)
                    {
                                return(-1);
                    }
                    else
                    if (upstr1 > upstr2)
                    {
                                return (1);
                    }
       }
       return (0);
 }
                    

int numeric(string)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       numeric
-
-       In              :       string
-
-       Out             :      1  String besthet aus Ziffern.
                               0  String besteht nicht aus Ziffern.
-
-       Errorcodes      :       -
-
-       Beschreibung    :      Test, ob String nur aus Ziffern besteht.
-
--------------------------------------------------------------------------------
**/
char *string;
{

      while (*string == ' ')
      {
             string ++;
      }
      if (*string == 0)
      {
             return (0);
      }
      if (*string == '+')
      {
             string ++;
      }
      if (*string == '-')
      {
             string ++;
      }
      for (;; string ++)
      {
             if (*string == 0)
                            return (1);
             if (*string == ',');
             else
             if (*string == '.');
             else
             if (*string < 0x30)
                            return (0);
             else
             if (*string > 0x39)
                            return (0);
      }
      return (1);
}

void upmem (string,len)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       upmem
-
-       In              :       string
                                len
-
-       Out             :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Alle Buchstaben in string in der Laenge len in
                                Grossbuchstaben konvertieren.
-
--------------------------------------------------------------------------------
**/
char *string;
short len;
{
       short i;

       for (i = 0; i < len; i ++)
       {
               if ((string[i] >= 'a') &&
                   (string[i] <= 'z'))
                    string[i] -= char_diff;
       }
}

void upstr (string)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       upstr
-
-       In              :       string
-
-       Out             :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Alle Buchstaben in string in
                                Grossbuchstaben konvertieren.
-
--------------------------------------------------------------------------------
**/
char * string;
{
       short i;

       for (i = 0; string[i] > 0; i ++)
       {
               if ((string[i] >= 'a') &&
                   (string[i] <= 'z'))
                    string[i] -= char_diff;
       }
}

void getstr (string1,string2,pos,anz)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       getstr
-
-       In              :       string1    Quellstring
-                               string2    Zielstring 
                                pos        Beginposition
                                anz        Anzahl zeichen 
-       Out             : 
-
-       Globals         :       -
-
-       Errorcodes      :       -
-
-       Beschreibung    :   Teistring aus String holen
-
--------------------------------------------------------------------------------
**/
char *string1;
char *string2;
short pos;
short anz;
{
       short i;
       char * ps;
       short len;
       char *quelle;
       static char szeichen[5] = {' ',',',';'};

       if (pos > strlen (string1))
       {
                         return;
       }
       quelle = string1 + pos;
       len = strlen (quelle);
       if (anz == 0)
       {
            for (i = 0; i < 5; i ++)
            {
                  ps = strchr (quelle,szeichen[i]);
                  if (ps)
                  {
                               break;
                  }
            }
            if (i < 5)
            {
                  anz = ps - quelle;
            }
            else
            {
                  anz = len;
            }
        }
        if (anz > len)
        {
            anz = len;
        }
        if (anz == 0)
        {
            return;
        }
        strncpy (string2,quelle,anz);
        string2[anz] = 0;
}


float ratof (string)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       ratof
-
-       In              :       string
-
-       Out             :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       String in float konvertieren.
-
--------------------------------------------------------------------------------
**/
char *string;
{
       float fl;
       float ziffer;
       float teiler;
       short minus;

       fl = 0;
       teiler = 10;
       minus = 1;
       while (*string < 0X30) 
       {
                if (*string == 0) return (0.0);
                if (*string == '-') break;
                if (*string == '+') break;
                string ++;
       }
       if (*string == '-')
       {
                minus = -1;
                string ++;
       }
       else if (*string == '+')
       {
                string ++;
       }

       while (*string)
       {
                if (*string == '.')
                {
                          break;
                }
                if (*string == ',')
                {
                          break;
                }
                if (*string < 0X30)
                {
                          break;
                }
                if (*string > 0X39)
                {
                          break; 
                }
                ziffer = *string - 0X30;
                fl = (fl * teiler) + ziffer;
                string ++;
       }

       if (*string == '.');
       else if (*string == ',');
       else
       {
                fl *= minus;
                return (fl);
       }

       string ++;
       while (*string)
       {
                if (*string < 0X30)
                {
                          break;
                }
                if (*string > 0X39)
                {
                          break;
                }
                ziffer = *string - 0X30;
                fl = fl + (ziffer / teiler);
                teiler *= 10;
                string ++;
       }
       fl *= minus;
       return (fl);
}

double ratod (string)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       ratod
-
-       In              :       string
-
-       Out             :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       String in double konvertieren.
-
--------------------------------------------------------------------------------
**/
char *string;
{
       double fl;
       double ziffer;
       double teiler;
       short minus;

       fl = 0;
       teiler = 10;
       minus = 1;
       while (*string < 0X30) 
       {
                if (*string == 0) return (0.0);
                if (*string == '-') break;
                if (*string == '+') break;
                string ++;
       }
       if (*string == '-')
       {
                minus = -1;
                string ++;
       }
       else if (*string == '+')
       {
                string ++;
       }

       while (*string)
       {
                if (*string == '.')
                {
                          break;
                }
                if (*string == ',')
                {
                          break;
                }
                if (*string < 0X30)
                {
                          break;
                }
                if (*string > 0X39)
                {
                          break; 
                }
                ziffer = *string - 0X30;
                fl = (fl * teiler) + ziffer;
                string ++;
       }

       if (*string == '.');
       else if (*string == ',');
       else
       {
                fl *= minus;
                return (fl);
       }

       string ++;
       while (*string)
       {
                if (*string < 0X30)
                {
                          break;
                }
                if (*string > 0X39)
                {
                          break;
                }
                ziffer = *string - 0X30;
                fl = fl + (ziffer / teiler);
                teiler *= 10;
                string ++;
       }
       fl *= minus;
       return (fl);
}

char *clipped (string)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       clipped
-
-       In              :       string
-
-       Out             :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Blanks am Ende eines Strings entfernen.
-
--------------------------------------------------------------------------------
**/
char *string;
{
        char *clstring;
        short i,len;

        len = strlen (string);

        clstring = string;
        for (i = len; i > 0; i --)
        {
                   if (clstring[i] > 0x20)
                   {
                              break;
                   }
        }
        clstring [i + 1] = 0;
        len = strlen (clstring);

        for (i = 0; i < len; i ++, clstring ++)
        {
                   if (*clstring > 0X20)
                   {
                              break;
                   }
        }
        return (clstring);
}

/* Speicherbereich und Zeiger fuer clipp     */

static char stringbuff[20][256];
static short akt_str = 0;

char *clipp (string)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       clipp
-
-       In              :       string
-
-       Out             :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :       Blanks am Ende eines Strings entfernen.
                                Der String wird nur tempor. im Speicher
                                gehalten.
-
--------------------------------------------------------------------------------
**/
char *string;
{
        char *clstring;
        short i,len;

        len = strlen (string);
        if (len > 255)
        {
                   return (clipped (string));
        }

        clstring = stringbuff [akt_str];
        strcpy (clstring,string);

        for (i = len; i > 0; i --)
        {
                   if (clstring[i] > 0x20)
                   {
                              break;
                   }
        }
        clstring [i + 1] = 0;
        len = strlen (clstring);

        for (i = 0; i < len; i ++, clstring ++)
        {
                   if (*clstring > 0X20)
                   {
                              break;
                   }
        }
        akt_str = (akt_str + 1) % 20;
        return (clstring);
}


char *sclipp (zielstring, quellstring)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       sclipp
-
-       In              :       zielstring
                                quellstring
-
-       Out             :       zielstring
-
-       Errorcodes      :       -
-
-       Beschreibung    :       quellstring ohne Blanks am Ende
                                in zielstring kopieren.
                                Die Adresse von zielstring zurueckgeben.
-
--------------------------------------------------------------------------------
**/
char *zielstring;
char *quellstring;
{
        char *clstring;
        short i,len;

        len = strlen (quellstring);

        clstring = zielstring;
        strcpy (clstring,quellstring);

        for (i = len; i > 0; i --)
        {
                   if (clstring[i] > 0x20)
                   {
                              break;
                   }
        }
        clstring [i + 1] = 0;
        len = strlen (clstring);

        for (i = 0; i < len; i ++, clstring ++)
        {
                   if (*clstring > 0X20)
                   {
                              break;
                   }
        }
        return (clstring);
}

char *make_name (env,name)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       make_name
-
-       In              :       env
                                name
-
-       Out             :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :   String mit Env.-Variablen zusammenbausen.
-
--------------------------------------------------------------------------------
**/
char *env;
char *name;
{
          static char gname[512];
          char *ename;

          ename = getenv (env);
          if (ename == 0)
          {
                      strcpy (gname,name);
          }
          else
          {
                      if (strlen (ename) + strlen (name) > 509)
                      {
                                          return (0);
                      }
                      sprintf (gname,"%s/%s",ename,name);
          }
          return (gname);
}

char *env_mname (env,name)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       make_name
-
-       In              :       env
                                name
-
-       Out             :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :   String mit Env.-Variablen zusammenbausen.
-
--------------------------------------------------------------------------------
**/
char *env;
char *name;
{
          char *gname;
          char *ename;

          ename = getenv (env);
          if (ename == 0)
          {
                      strcpy (gname,name);
          }
          else
          {
                      gname = malloc (strlen (ename) + strlen (name) + 2);
                      if (gname == 0)
                      {
                                         return (gname);
                      }
                      sprintf (gname,"%s/%s",ename,name);
          }
          return (gname);
}

char *env_name (ziel,quelle,env)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       make_name
-
-       In              :       env
                                name
-
-       Out             :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :   String mit Env.-Variablen zusammenbausen.
-
--------------------------------------------------------------------------------
**/
char *ziel;
char *quelle;
char *env;
{
          char *ename;

          ename = getenv (env);
          if (ename == 0)
          {
                      strcpy (ziel,quelle);
          }
          else
          {
                      sprintf (ziel,"%s/%s",ename,quelle);
          }
          return (ziel);
}

char *wort[0x1000];              /* Bereich fuer Woerter eines Strings    */

char wstring[0x1000];        
short wptr;

split (string)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       split
-
-       In              :       string
-
-       Out             :       
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :      String in Woerter aufteilen.
-
--------------------------------------------------------------------------------
**/
char *string;
{

        static short wz = 0;         /* Wortzaehler                           */

        short i;
        short j;
        short len;

        wptr = 0;
        wz = 0;
        i = 0;
        len = strlen (string);

        while (1)
        {
                 while (1)
                 {
                         if (i >= len)
                         {
                                  return (wz);
                         }
                         if (string[i] > ' ')
                         {
                                  break;
                         }
                         i ++;
                  }  

                  j = 0;
                  wz ++;
                  wort[wz] = &wstring[wptr];
                  while (1)
                  {
                         if (i >= len)
                         {
                                  wort[wz][j] = 0;
                                  return (wz);
                         }
                         if ((string[i] == ' ') ||
                             (string [i] == TAB))
                         {
                                  wort[wz][j] = 0;
                                  break;
                         }
                         wort[wz][j] = string[i];
                         i ++;
                         j ++;
                  }
                  wptr += j + 1;     
        } 
        return (wz);
}
         
/*
split (string)
?**
--------------------------------------------------------------------------------
-
-       Procedure       :       split
-
-       In              :       string
-
-       Out             :       
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :      String in Woerter aufteilen.
-
--------------------------------------------------------------------------------
**?
char *string;
{

        static short wz = 0;         ?* Wortzaehler                           *?

        short i;
        short j;
        short len;

        for (i = 1; i <= wz; i ++)
        {
                 free (wort[i]);
        }
        wz = 0;
        i = 0;
        len = strlen (string);

        while (1)
        {
                 while (1)
                 {
                         if (i >= len)
                         {
                                  return (wz);
                         }
                         if (string[i] > ' ')
                         {
                                  break;
                         }
                         i ++;
                  }  

                  j = 0;
                  wz ++;
                  wort[wz] = (char *) malloc (80);
                  while (1)
                  {
                         if (i >= len)
                         {
                                  wort[wz][j] = 0;
                                  return (wz);
                         }
                         if ((string[i] == ' ') ||
                             (string [i] == TAB))
                         {
                                  wort[wz][j] = 0;
                                  break;
                         }
                         wort[wz][j] = string[i];
                         i ++;
                         j ++;
                  }
        } 
        return (wz);
}
*/
         
char *zwort[0x1000];          /* Bereich fuer Woerter eines Strings    */
static short wsize = 0;       /* Wortgroesse                           */

int splitsize (size)
/**
konstante Wortgroesse fuer zsplit setzen.
**/
short size;
{
        wsize = size;
}

int zsplit (string, zeichen)
/**
--------------------------------------------------------------------------------
-
-       Procedure       :       zsplit
-
-       In              :       string
                                zeichen;
-
-       Out             :       
-
-       Globals         :      
-
-       Errorcodes      :       -
-
-       Beschreibung    :      String in Woerter aufteilen.
-
--------------------------------------------------------------------------------
**/
char *string;
char zeichen;
{

        static short wz = 0;         /* Wortzaehler                           */
        short worts;

        short i;
        short j;
        short len;

        for (i = 1; i <= wz; i ++)
        {
                 free (zwort[i]);
        }
        wz = 0;
        i = 0;
        len = strlen (string);

        while (1)
        {

                  j = 0;
                  wz ++;
                  worts = wortsize (&string [i], zeichen);
                  zwort[wz] = (char *) malloc (worts);
                  while (1)
                  {
                         if (i >= len)
                         {
                                  zwort[wz][j] = 0;
                                  return (wz);
                         }
                         if (string[i] == zeichen)
                         {
                                  zwort[wz][j] = 0;
                                  break;
                         }
                         zwort[wz][j] = string[i];
                         i ++;
                         j ++;
                  }
                  i ++;
                  if (i >= len)
                  {
                          return (wz);
                  }
        } 
        return (wz);
}

int wortsize (string, zeichen)
/**
Groesse eines durch zeichen getrenten Wortes ermitteln.
**/
char *string;
char zeichen;
{
       short i;

/* Wenn wsize belegt ist, wird immer dieser Wert fuer malloc genommen */

       if (wsize) return (wsize);

       for (i = 0; string [i]; i ++)
       {
                   if (string[i] == zeichen) break;
       }

       return (i + 2);
}

char *basename (pfad)
/**
Dateiname aus Pfad holen.
**/
char *pfad;
{
       short anz;
       static char datei [20];

       anz = zsplit (pfad,'/');
       if (anz == 0) return (pfad);

       strncpy (datei, zwort [anz], 19); 
       datei [19] = 0;
       return (datei);
}
       
         
         
void cr_weg (string)
/**
CR aus String entfernen.
**/
char *string;
{
 
        for (;*string; string += 1)
        {
                   if ((*string == 10) ||
                       (*string == 13))
                   {
                            *string = 0;
                             break;
                   }
        }
}
