#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mo_menu.h"
#include "dbfunc.h"

#define sql_in ins_quest
#define sql_out out_quest


struct MENUE menue;
struct PROG_KOPF  prog_kopf;
struct SYS_BEN    sys_ben;
struct SYS_INST   sys_inst;

int MENUE_CLASS::LeseMenue (void)
{
        if (menue_cursor == -1)
        {
                  sql_in ((char *) &menue.mdn, 1, 0);
                  sql_in ((char *) &menue.fil, 1, 0);
                  sql_in (menue.pers, 0, 13);
                  sql_in (menue.prog, 0, 5);
                  sql_out ((char *) &menue.anz_menue, 1, 0);
                  sql_out (menue.zei0, 0, 28);
                  sql_out (menue.zei1, 0, 28);
                  sql_out (menue.zei2, 0, 28);
                  sql_out (menue.zei3, 0, 28);
                  sql_out (menue.zei4, 0, 28);
                  sql_out (menue.zei5, 0, 28);
                  sql_out (menue.zei6, 0, 28);
                  sql_out (menue.zei7, 0, 28);
                  sql_out (menue.zei8, 0, 28);
                  sql_out (menue.zei9, 0, 28);
                  sql_out (menue.menue0, 0, 6);
                  sql_out (menue.menue1, 0, 6);
                  sql_out (menue.menue2, 0, 6);
                  sql_out (menue.menue3, 0, 6);
                  sql_out (menue.menue4, 0, 6);
                  sql_out (menue.menue5, 0, 6);
                  sql_out (menue.menue6, 0, 6);
                  sql_out (menue.menue7, 0, 6);
                  sql_out (menue.menue8, 0, 6);
                  sql_out (menue.menue9, 0, 6);

                  menue_cursor = prepare_sql (
                     "select anz_menue,"
                            "zei0,"
                            "zei1,"
                            "zei2,"
                            "zei3,"
                            "zei4,"
                            "zei5,"
                            "zei6,"
                            "zei7,"
                            "zei8,"
                            "zei9,"
                            "menue0,"
                            "menue1,"
                            "menue2,"
                            "menue3,"
                            "menue4,"
                            "menue5,"
                            "menue6,"
                            "menue7,"
                            "menue8,"
                            "menue9 "
                            "from menue_data "
                            "where mdn = ? and fil = ? "
                            "and pers = ? and prog = ?");
        }

        open_sql (menue_cursor);
        fetch_sql (menue_cursor);
        return sqlstatus;
}

int MENUE_CLASS::Leseprog_kopf (char *prog)
{
         if (prog_cursor == -1)
         {
                   sql_in (prog_kopf.prog, 0, 6);
                   sql_out (prog_kopf.prog_typ, 0, 2);
                   sql_out (prog_kopf.p_name, 0, 33);
                   prog_cursor = prepare_sql ("select prog_typ,p_name from "
                                              "prog_kopf "
                                              "where prog = ?");
                   if (sqlstatus) return sqlstatus;
         }
         strcpy (prog_kopf.prog, prog); 
         open_sql (prog_cursor);
         fetch_sql (prog_cursor);
         return sqlstatus;
}

int MENUE_CLASS::Lesesys_ben (char *pers_nam, char *passwd)
{
         if (sys_ben_cursor == -1)
         {
                   sql_in (sys_ben.pers_nam, 0, 9);
                   sql_in (sys_ben.pers_passwd, 0, 13);
                   sql_out ((char *) &sys_ben.mdn, 1, 0);
                   sql_out ((char *) &sys_ben.fil, 1, 0);
                   sql_out ((char *) sys_ben.pers, 0, 13);
                   sql_out ((char *) &sys_ben.berecht, 1, 0);
                   sql_out ((char *) sys_ben.ben_txt, 0, 33);
                   sys_ben_cursor = prepare_sql ("select mdn, fil, pers, berecht,ben_txt from "
                                              "sys_ben "
                                              "where pers_nam = ? "
                                              "and pers_passwd = ?");
                   if (sqlstatus) return sqlstatus;
         }
         strcpy (sys_ben.pers_nam, pers_nam);
         if (strcmp (passwd, "            ") <= 0)
         {
                 strcpy (sys_ben.pers_passwd,"            " );
         }
         else
         {
                 strcpy (sys_ben.pers_passwd, passwd);
         }
         open_sql (sys_ben_cursor);
         fetch_sql (sys_ben_cursor);
         return sqlstatus;
}

int MENUE_CLASS::Lesesys_ben_pers (char *pers_nam)
{
         if (sys_ben_cursor_pers == -1)
         {
                   sql_in (sys_ben.pers_nam, 0, 9);
                   sql_out ((char *) &sys_ben.mdn, 1, 0);
                   sql_out ((char *) &sys_ben.fil, 1, 0);
                   sql_out ((char *) sys_ben.pers, 0, 13);
                   sql_out ((char *) &sys_ben.berecht, 1, 0);
                   sql_out ((char *) sys_ben.ben_txt, 0, 33);
                   sys_ben_cursor_pers = prepare_sql ("select mdn, fil, "
                                              "pers, berecht, ben_txt from "
                                              "sys_ben "
                                              "where pers_nam = ?");
                   if (sqlstatus) return sqlstatus;
         }
         strcpy (sys_ben.pers_nam, pers_nam);
         open_sql (sys_ben_cursor_pers);
         fetch_sql (sys_ben_cursor_pers);
         return sqlstatus;
}

void MENUE_CLASS::SetSysBen (void)
/**
Mandant und Filiale fuer Benutzer aus Commandline holen.
**/
{
         int anz;

         anz = wsplit (GetCommandLine (), " ");
         if (anz < 3)
         {
                      SetSysBenPers ();
                      return;
         }
         else
         {
                      sys_ben.mdn = atoi (wort [1]);
                      sys_ben.fil = atoi (wort [2]);
         }
}

void MENUE_CLASS::SetSysBen (LPSTR lpszCmdLine)
/**
Mandant und Filiale fuer Benutzer aus Commandline holen.
**/
{
         int anz;

         anz = wsplit (lpszCmdLine, " ");
         if (anz > 0)
         {
            if (strncmp (wort[0], "nomenu=", 7) != 0 &&
				strncmp (wort[0], "mdn=", 4) != 0)
			{
                      clipped (wort [0]);
                      Lesesys_ben_pers (wort [0]);
			}
         }
}

void MENUE_CLASS::SetSysBenPers (void)
/**
Mandant und Filiale fuer Benutzer aus Pers holen.
**/
{
         int anz;

         anz = wsplit (GetCommandLine (), " ");
         if (anz > 1)
         {
                      clipped (wort [1]);
                      Lesesys_ben_pers (wort [1]);
         }
}


int MENUE_CLASS::Lesesys_inst (void)
/**
Einen Satz in sys_inst lesen.
**/
{

    int cursor_sys_inst;  
    int dsqlstatus;
   
    out_quest ((char *) sys_inst.auf_nr,0,9);
    out_quest ((char *) &sys_inst.dat,2,0);
    out_quest ((char *) &sys_inst.fil,1,0);
    out_quest ((char *) sys_inst.kun_nam,0,31);
    out_quest ((char *) &sys_inst.mdn,1,0);
    out_quest ((char *) &sys_inst.monitor,1,0);
    out_quest ((char *) &sys_inst.rechnertyp,1,0);
    out_quest ((char *) sys_inst.unix,0,13);
    out_quest ((char *) sys_inst.usv,0,2);
    out_quest ((char *) sys_inst.projekt,0,13);
    cursor_sys_inst = prepare_sql ("select sys_inst.auf_nr,  "
"sys_inst.dat,  sys_inst.fil,  sys_inst.kun_nam,  sys_inst.mdn,  "
"sys_inst.monitor,  sys_inst.rechnertyp,  sys_inst.unix,  sys_inst.usv,  "
"sys_inst.projekt from sys_inst");

     open_sql (cursor_sys_inst);
     fetch_sql (cursor_sys_inst);
     dsqlstatus = sqlstatus;
     close_sql (cursor_sys_inst);
     sqlstatus = dsqlstatus;
     return sqlstatus;
}

