#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include<winbase.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_draw.h"
#include "wmaskc.h"
#include "mo_meld.h"
#include "cmask.h"
#include "colbut.h"
#include "mo_enter.h" 
#include "mo_gdruck.h" 

extern BOOL ColBorder;

#define BuOk 1001
#define BuCancel 1002
#define BuUp 760
#define BuDown 761
#define PLBOX 600


int fEnter::StdSize = 120;

CFIELD **fEnter::_fWork;
CFORM *fEnter::fWork = NULL;
BOOL fEnter::IsModal = FALSE;
int (*fEnter::FpProg) (void) = NULL;

fEnter::fEnter (int x, int y, int cx, int cy, char *Caption)
{
		TEXTMETRIC tm;
		HFONT hFont, oldfont;
		HDC hdc;
		SIZE size;
		int xfull, yfull;

        this->Font = &textfont;  
		FocusSet = FALSE;
		BkColor = GRAYCOL;

        if (StdSize > 150) StdSize = 150;
        xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
        yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

//		if (xfull < 1000) StdSize -= 20;

        textfont.FontHeight = StdSize;

		this->Caption = NULL;
		if (Caption && Caption[0])
		{
			        this->Caption = Caption; 
		}
		hdc = GetDC (NULL);
        hFont = SetDeviceFont (hdc, Font, &tm);
        oldfont = SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
		GetTextExtentPoint32 (hdc, "X", 1, &size);
		DeleteObject (SelectObject (hdc, oldfont));
	

		this->cx = size.cx * cx;
		this->cy = (int) (double) ((double) tm.tmHeight * cy * 1.3);

		this->x = size.cx * x;
		this->y = (int) (double) ((double) tm.tmHeight * y * 1.3);
}

fEnter::~fEnter ()
{

	    if (fWork)
		{
               fWork->destroy (); 
			   fWork = NULL;
		}
/*
		       delete _fWork[0];
		       delete _fWork[1];
		       delete _fWork;
		       delete fWork;
*/
}

void fEnter::GetWindowText (HWND hWnd)
{
            DWORD Id; 
			CFIELD *field;

            Id = fWork->GetID (hWnd);
            if (Id == NULL) return;
			field = fWork->GetCfield (Id);
            field->GetText ();
            field->SetText ();
            field->GetText ();
}

void fEnter::SetWindowText (HWND hWnd)
{
            DWORD Id; 
			CFIELD *field;

            Id = fWork->GetID (hWnd);
            if (Id == NULL) return;
			field = fWork->GetCfield (Id);
            field->SetText ();
}

BOOL fEnter::OnKeyup (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
            DWORD Id; 
            
            if (fWork->GetAttribut () == CLISTBOX)    return FALSE; 
            if (fWork->GetAttribut () == COWNLISTBOX) return FALSE; 
            if (fWork->GetAttribut () == CCOMBOBOX)   return FALSE; 
            Id = fWork->GetID (hWnd);
			CFIELD *c = fWork->GetCfield (Id);
			if (c != NULL)
			{
/*
				         if (c->GetAttribut () == CCOMBOBOX &&
							 c->GetBkMode () | CBS_DROPDOWNLIST)
*/
				         if (c->GetAttribut () == CCOMBOBOX)
						 {
							 return FALSE;
						 }
			}
			if (syskey == KEYSTAB)
			{
		                SendMessage (hWnd, WM_KILLFOCUS, 0, 0l); 
                        fWork->PriorFormField ();
			}
			else if (syskey == KEYUP)
			{
			             SendMessage (hWnd, WM_KILLFOCUS, 0, 0l); 
                         fWork->PriorField ();
			}
			return TRUE;
} 

BOOL fEnter::OnKeydown (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
            DWORD Id; 
            
            if (fWork->GetAttribut () == CLISTBOX)    return FALSE; 
            if (fWork->GetAttribut () == COWNLISTBOX) return FALSE; 
//            if (fWork->GetAttribut () == CCOMBOBOX)   return FALSE;
			hWnd = GetFocus ();
            Id = fWork->GetID (hWnd);
			CFIELD *c = fWork->GetCfield (Id);
			if (c == NULL)
			{
                         Id = fWork->GetID (GetParent (hWnd));
			             c = fWork->GetCfield (Id);
			}
			if ((syskey == KEYDOWN || syskey == KEYUP) && c != NULL)
			{
/*
				         if (c->GetAttribut () == CCOMBOBOX &&
							 c->GetBkMode () | CBS_DROPDOWNLIST)
*/
				         if (c->GetAttribut () == CCOMBOBOX)
						 {
							 return FALSE;
						 }
			}
			if (syskey == KEYTAB)
			{
			             SendMessage (hWnd, WM_KILLFOCUS, 0, 0l); 
                         fWork->NextFormField ();
			}
			else if (syskey == KEYDOWN)
			{
			             SendMessage (hWnd, WM_KILLFOCUS, 0, 0l); 
                         fWork->NextField ();
			}
			else if (syskey == KEYCR)
			{
				if (fWork->GetAttribut () == CBUTTON ||
                    fWork->GetAttribut () == CCOLBUTTON)
				{
                         Id = fWork->GetID (hWnd);
			             SendMessage (this->hWnd, WM_COMMAND, 
							                      (WPARAM) MAKELONG (Id, BN_CLICKED), 
												  (LPARAM) hWnd); 
					      
				}
				else
				{
			             SendMessage (hWnd, WM_KILLFOCUS, 0, 0l); 
	                     fWork->NextField ();
				}
			}
			return TRUE;
} 

void fEnter::ProcessMessages (void)
{
	      HWND hWnd;

	      MSG msg;
		  IsModal = TRUE;
		  syskey = 0;
		  if (FocusSet == FALSE)
		  {
		            fWork->SetFirstFocus ();
		  }
		  else
		  {
					hWnd = GetFocus ();
					aktId = fWork->GetID (hWnd);
					if (aktId == 0) fWork->SetFirstFocus ();
		  }
		  hWnd = GetFocus ();
	      aktId = fWork->GetID (hWnd);
		  if (aktId == 0)
		  {
	          aktId = fWork->GetID (GetParent (hWnd));
		  }
		  if (aktId == 0) return;
		  SendMessage (hWnd, WM_USER, 0, 0l);
          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
			      if (msg.wParam == VK_TAB)
				  {

                      if (GetKeyState (VK_SHIFT) < 0)
                      {
                              syskey = KEYSTAB;
                              if (OnKeyup (msg.hwnd, msg.wParam, msg.lParam))
							  {
				                        continue;
							  }
					  }
					  else
					  {
					          syskey = KEYTAB;
                              if (OnKeydown (msg.hwnd, msg.wParam, msg.lParam))
							  {
				                        continue;
							  }
					  }
					  break;
				  }
				  else if (msg.wParam == VK_F5)
				  {
					  syskey = KEY5;
					  break;
				  }
				  else if (msg.wParam == VK_UP)
				  {
					  syskey = KEYUP;
                      if (OnKeyup (msg.hwnd, msg.wParam, msg.lParam))
                      {
				           continue;
                      }
				  }
				  else if (msg.wParam == VK_DOWN)
				  {
					  syskey = KEYDOWN;
                      if (OnKeydown (msg.hwnd, msg.wParam, msg.lParam))
                      {
				           continue;
                      }
				  }
				  else if (msg.wParam == VK_RETURN)
				  {
					  syskey = KEYCR;
                      if (OnKeydown (msg.hwnd, msg.wParam, msg.lParam))
                      {
				           continue;
                      }
				  }
			  }
			  else if (msg.message == WM_COMMAND)
			  {
			  }
              TranslateMessage(&msg);
			  DispatchMessage(&msg);
		  }
		  IsModal = FALSE;
}
   

CALLBACK fEnter::CProc (HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;
		DWORD Id;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					if (fWork)
					{
						fWork->display (hWnd, hdc);
					}
                    EndPaint (hWnd, &ps);
					break;
			  case WM_USER :
				    if (FpProg)
					{
						(*FpProg) ();
					}
					break;
              case WM_SETFOCUS :
				    Id = fWork->GetID (hWnd);
					if (Id)
					{
			              fWork->before (Id);
					}
                    break; 
              case WM_KILLFOCUS :
				    Id = fWork->GetID (hWnd);
					if (Id)
					{
			            if (fWork->after (Id) > 0)
						{
						    if (IsModal)
							{ 
				                   PostQuitMessage (0);
							}
						}
					}
                    break; 
              case WM_COMMAND :
				   switch (HIWORD (wParam)) 
				   {
                        case EN_KILLFOCUS :
							    GetWindowText (HWND (lParam));
				                Id = fWork->GetID (HWND (lParam));
					            if (Id)
								{
			                        if (fWork->after (Id) > 0)
									{
						                if (IsModal)
										{ 
				                             PostQuitMessage (0);
										}
									}
								}
                                break; 
                        case CBN_KILLFOCUS :
   					            GetWindowText (HWND (lParam));
				                Id = fWork->GetID (HWND (lParam));
					            if (Id)
								{
			                        if (fWork->after (Id) > 0)
									{
						                if (IsModal)
										{ 
				                             PostQuitMessage (0);
										}
									}
								}
                                break; 
                        case EN_SETFOCUS :
							    SetWindowText (HWND (lParam));
				                Id = fWork->GetID ((HWND (lParam)));
					            if (Id)
								{
			                          fWork->before (Id);
								}
								break;
                        case CBN_SETFOCUS :
							    SetWindowText (HWND (lParam));
				                Id = fWork->GetID ((HWND (lParam)));
					            if (Id)
								{
			                          fWork->before (Id);
								}
								break;
						case BN_CLICKED :
							    Id = fWork->GetID ((HWND (lParam)));
					            if (Id)
								{
			                        if (fWork->after (Id) > 0)
									{
						                if (IsModal)
										{ 
				                             PostQuitMessage (0);
										}
									}
								}
								break;
					}
                    break; 

              case WM_DESTROY :
				    if (IsModal)
					{
				         PostQuitMessage (0);
					}
					break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void fEnter::DestroyWindow (void)
{
	       fWork->destroy ();
		   FocusSet = FALSE;
		   ::DestroyWindow (hWnd);
		   fWork = NULL;
}

void fEnter::SetBkColor (COLORREF col)
{
			BkColor = col;
			textfont.FontBkColor = col;
}

						
HWND fEnter::OpenWindow (HANDLE hInstance, HWND hMainWindow, DWORD style)
{
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  RECT rect, rect1;
		  int xfull, yfull;

		  this->hInstance   = hInstance;
		  this->hMainWindow = hMainWindow;
          xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
          yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
//                  wc.hbrBackground =  GetStockObject (GRAY_BRUSH);
		          wc.hbrBackground =  CreateSolidBrush (BkColor);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CFEnter";
                  RegisterClass(&wc);

				  registered = TRUE;
		  }

		  GetClientRect (hMainWindow, &rect);
		  GetWindowRect (hMainWindow, &rect1);

          if (hMainWindow)
          {
		          this->x  = max (0, (rect.right - cx) / 2)  + rect1.left;
		          this->y  = max (0, (rect.bottom - cy) / 2) + rect1.top;
		  }
		  else
		  {
			  if (this->x < 0)
			  {
		          this->x  = max (0, (xfull - cx) / 2);
			  }
			  if (this->y < 0)
			  {
		          this->y  = max (0, (yfull - cy) / 2);
			  }
		  }

          hWnd = CreateWindowEx (0, 
                                 "CFEnter",
                                 Caption,
                                 style,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL);
//          SetClassLong (hWnd,GCL_HBRBACKGROUND, 
//                                      (long) CreateSolidBrush (BkColor));
		  ShowWindow (hWnd, SW_SHOWNORMAL);
		  UpdateWindow (hWnd);
		  return hWnd;
}

void fEnter::SetFocus (DWORD Id)
{
	       if (fWork->SetCurrentFieldID (Id))
		   {
			   FocusSet = TRUE;
		   }
}

void fEnter::MoveWindow (void)
{
		  RECT rect;

		  GetClientRect (hMainWindow, &rect);
          ::MoveWindow (hWnd, rect.left, rect.top, rect.right, rect.bottom, TRUE);

}

void fEnter::SetMask (CFORM *frm)
{
	      fWork = frm;
}
   


