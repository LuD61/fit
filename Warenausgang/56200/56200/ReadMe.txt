================================================================================
    MICROSOFT FOUNDATION CLASS LIBRARY : 56200 Projekt�bersicht
================================================================================

Der Anwendungs-Assistent hat diese 56200-Anwendung erstellt. 
Diese Anwendung zeigt die prinzipielle Anwendung der Microsoft Foundation Classes 
und dient als Ausgangspunkt f�r die Erstellung Ihrer eigenen Anwendungen.

Diese Datei enth�lt die Zusammenfassung der Bestandteile aller Dateien, die 
Ihre 56200-Anwendung bilden.

56200.vcproj
    Dies ist die Hauptprojektdatei f�r VC++-Projekte, die vom Anwendungs-Assistenten 
    erstellt wird. Sie enth�lt Informationen �ber die Version von Visual C++, mit der
    die Datei generiert wurde, �ber die Plattformen, Konfigurationen und Projektfeatures,
    die mit dem Anwendungs-Assistenten ausgew�hlt wurden.

56200.h
    Hierbei handelt es sich um die Haupt-Headerdatei der Anwendung. Diese enth�lt 
    andere projektspezifische Header (einschlie�lich Resource.h) und deklariert die
    CMyApp-Anwendungsklasse.

56200.cpp
    Hierbei handelt es sich um die Haupt-Quellcodedatei der Anwendung. Diese enth�lt die
    Anwendungsklasse CMyApp.

56200.rc
    Hierbei handelt es sich um eine Auflistung aller Ressourcen von Microsoft Windows, die 
    vom Programm verwendet werden. Sie enth�lt die Symbole, Bitmaps und Cursors, die im 
    Unterverzeichnis RES gespeichert sind. Diese Datei l�sst sich direkt in Microsoft
    Visual C++ bearbeiten. Ihre Projektressourcen befinden sich in 1031.

res\My.ico
    Dies ist eine Symboldatei, die als Symbol f�r die Anwendung verwendet wird. Dieses 
    Symbol wird durch die Haupt-Ressourcendatei 56200.rc eingebunden.

res\My.rc2
    Diese Datei enth�lt Ressourcen, die nicht von Microsoft Visual C++ bearbeitet wurden.
    In dieser Datei werden alle Ressourcen gespeichert, die vom Ressourcen-Editor nicht bearbeitet 
    werden k�nnen.

/////////////////////////////////////////////////////////////////////////////

Der Anwendungs-Assistent erstellt eine Dialogklasse:
56200Dlg.h, 56200Dlg.cpp - das Dialogfeld
    Diese Dateien enthalten die Klasse CMyDlg. Diese Klasse legt das
    Verhalten des Haupt-Dialogfelds der Anwendung fest. Die Vorlage des Dialog-
    felds befindet sich in 56200.rc, die mit Microsoft Visual C++
    bearbeitet werden kann.
/////////////////////////////////////////////////////////////////////////////

Hilfeunterst�tzung:

hlp\56200.hhp
    Diese Datei ist eine Hilfeprojektdatei. Sie enth�lt die Daten, die zum Kompilieren der
    Hilfedateien in eine CHM-Datei erforderlich sind.

hlp\56200.hhc
    Diese Datei listet den Inhalt des Hilfeprojekts auf.

hlp\56200.hhk
    Diese Datei enth�lt einen Index der Hilfethemen.

hlp\afxcore.htm
    Diese Datei enth�lt die Standardhilfethemen f�r MFC-Standardbefehle und Oberfl�chenobjekte. 
    Sie k�nnen ihre eigenen Hilfethemen zu dieser Datei hinzuf�gen.

makehtmlhelp.bat
    Diese Datei wird vom Buildsystem zum Kompilieren der Hilfedateien verwendet.

hlp\Images\*.gif
    Diese sind Bitmapdateien, die f�r die Standardbefehle der Microsoft Foundation Class 
    Library erforderlich sind.

/////////////////////////////////////////////////////////////////////////////

Weitere Standarddateien:

StdAfx.h, StdAfx.cpp
    Mit diesen Dateien werden vorkompilierte Headerdateien (PCH) mit der Bezeichnung
    56200.pch und eine vorkompilierte Typdatei mit der Bezeichnung 
    StdAfx.obj erstellt.

Resource.h
    Dies ist die Standard-Headerdatei, die neue Ressourcen-IDs definiert.
    Microsoft Visual C++ liest und aktualisiert diese Datei.

/////////////////////////////////////////////////////////////////////////////

Weitere Hinweise:

Der Anwendungs-Assistent verwendet "TODO:", um die Stellen anzuzeigen, 
an denen Sie Erweiterungen oder Anpassungen vornehmen k�nnen.

Wenn Ihre Anwendung die MFC in einer gemeinsam genutzten DLL verwendet und 
eine andere als die aktuell auf dem Betriebssystem eingestellte Sprache verwendet, muss 
die entsprechend lokalisierte Ressource MFC70XXX.DLL von der Microsoft Visual C++ CD-ROM 
in das Verzeichnis system oder system32 kopiert und in MFCLOC.DLL umbenannt werden  
("XXX" steht f�r die Abk�rzung der Sprache. So enth�lt beispielsweise MFC70DEU.DLL die ins Deutsche 
�bersetzten Ressourcen.)  Anderenfalls werden einige Oberfl�chenelemente Ihrer Anwendung 
in der Sprache des Betriebssystems angezeigt.

/////////////////////////////////////////////////////////////////////////////
