#include "StdAfx.h"
#include "EditListCtrl.h"
#include "ListEdit.h"

CListEdit::CListEdit(void)
{
}

CListEdit::~CListEdit(void)
{
}

BEGIN_MESSAGE_MAP(CListEdit, CEdit)
	ON_WM_KEYDOWN ()
END_MESSAGE_MAP()

void CListEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	int diff = 0;
	switch (nChar)
	{
	case VK_RETURN :
	case VK_DOWN :
	case VK_UP :
	case VK_TAB :
	case VK_F6 :
	case VK_F7 :
	case VK_NEXT :
	case VK_PRIOR :
		((CEditListCtrl *)GetParent ())->OnKeyD (nChar);
		return;
	}
    CEdit::OnKeyDown (nChar, nRepCnt, nFlags);
}
