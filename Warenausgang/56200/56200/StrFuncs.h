#pragma once

class CStrFuncs
{
public:
	static CString Format;
	CStrFuncs(void);
	~CStrFuncs(void);
	static void Round (double, CString&);
	static void Round (double, int, CString&);
	static BOOL IsDoubleEqual (double, double);
    static double StrToDouble (LPTSTR);
    static double StrToDouble (CString&);
	static BOOL IsDecimal (CString&);
	static void SysDate (CString& Date);
	static void SysTime (CString& Time);
    static void ToClipboard (CString& Text);
    static void FromClipboard (CString& Text);
};
