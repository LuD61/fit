// 56200Dlg.h : Headerdatei
//

#pragma once
#include "NumEdit.h"
#include "CtrlGrid.h"
#include "FillList.h"
#include "sys_par.h"
#include "mdn.h"
#include "adr.h"
#include "Kun.h"
#include "ptabn.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "FormTab.h"
#include "Process.h"
#include "mo_progcfg.h"
#include "Controls.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
//#include "DateTimePicker.h"
#include "DbUniCode.h"
#include "afxdtctl.h"

#define IDC_MDNCHOICE 3001
#define IDC_KUN1CHOICE 3002
#define IDC_KUN2CHOICE 3003


// CMyDlg Dialogfeld
class CMyDlg : public CDialog
{
// Konstruktion
public:
	CMyDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_MY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
    void OnMdnchoice(); 
    void OnKunchoice(short); 
    void OnKun1choice(); 
    void OnKun2choice(); 
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceKun *ChoiceKun;
	BOOL ModalChoiceKun;


	CFormTab Form;
	MDN_CLASS Mdn;
	KUN_CLASS Kun;
	PTABN_CLASS Ptabn;
	ADR_CLASS MdnAdr;
	ADR_CLASS KunAdr;
	CString PersName;
	CString CS_Datum_von;
	CString CS_Datum_bis;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid Kun1Grid;
	CCtrlGrid Kun2Grid;
	CCtrlGrid RechartGrid;
	CCtrlGrid FakGrid;
	CButton m_MdnChoice;
	CButton m_Kun1Choice;
	CButton m_Kun2Choice;
//	DateTimePicker dateTimePicker1;
	short dmdn ;
	long dkun_von, dkun_bis;
   	TCHAR dkun_bran2_von[4], dkun_bran2_bis[4], dStatus[3] ;
   	TCHAR dCheck_Rech[2], dCheck_Gut[2], dCheck_Bar[2];
	TCHAR dFak_all[2], dFak_mit[2], dFak_ohne[2];
	TCHAR dFormtyp [11];


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	BOOL ReadMdn ();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnEnChangeEditMdnvon();
	CFont Font;
	CFont lFont;

	CNumEdit m_Mdn;
	CNumEdit m_MdnBez;
	CNumEdit m_Kun_von;
	CNumEdit m_Kun_bis;
	CComboBoxEx m_Kunbran2_von;
	CComboBoxEx m_Kunbran2_bis;
	CComboBoxEx m_Formtyp;
	CDateTimeCtrl  m_Datum_von;
	CDateTimeCtrl  m_Datum_bis;
	CComboBoxEx m_Status;
	CStatic m_StaticMdn;
	CStatic m_StaticFormtyp;
	CStatic m_StaticKun;
	CStatic m_Staticvon;
	CStatic m_Staticbis1;
	CStatic m_StaticKunbran2;
	CStatic m_StaticDatum;
	CStatic m_StaticStatus;
	CStatic m_GroupRechart;
	CStatic m_GroupFak;
	CStatic m_Group1;
	CButton m_Check_Rech;
	CButton m_Check_Gut;
	CButton m_Check_Bar;
	CButton m_Fak_all;
	CButton m_Fak_mit;
	CButton m_Fak_ohne;

	BOOL ModalChoice;
	BOOL CloseChoice;
    void FillKunBran2 ();
    void FillStatus ();
    void FillFormtyp ();
    BOOL Print (BOOL);


	afx_msg void OnCbnSelchangeKunbran2bis2();
	afx_msg void OnNMKillfocusDatetimepicker1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusDatetimepicker2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedOk2();
};
