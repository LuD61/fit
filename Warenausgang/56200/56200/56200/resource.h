//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 56200.rc
//
#define IDOK2                           3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MY_DIALOG                   102
#define IDR_HTML_DATETIMEPICKER         103
#define IDR_MAINFRAME                   128
#define IDC_Edit_Mdn_von                1009
#define IDC_STATIC_Mdn                  1010
#define IDC_STATIC_bis1                 1011
#define IDC_STATIC_Kunde                1012
#define IDC_Edit_Kunde_von              1013
#define IDC_Edit_Kunde_von2             1014
#define IDC_Edit_Kunde_bis              1014
#define IDC_STATIC_Kunbran2             1015
#define IDC_STATIC_FORMTYP              1016
#define IDC_STATIC_von                  1019
#define IDC_STATIC_Datum                1020
#define IDC_STATIC_Datum2               1021
#define IDC_STATIC_STATUS               1021
#define IDC_MDN_BEZ                     1027
#define IDC_DATETIMEPICKER1             1030
#define IDC_DATETIMEPICKER2             1031
#define IDC_KUNBRAN2_von1               1032
#define IDC_KUNB2_von1                  1032
#define IDC_KUNBRAN2_bis                1033
#define IDC_Kunbran2_von                1034
#define IDC_KUNBRAN2_bis2               1034
#define IDC_STATUS                      1035
#define IDC_CHECK_RECH                  1036
#define IDC_CHECK_GUT                   1037
#define IDC_CHECK_BAR                   1038
#define IDC_GROUP_RECHART               1039
#define IDC_FORMTYP                     1040
#define IDC_CHECK1                      1041
#define IDC_Group1                      1042
#define IDC_Group0                      1043
#define IDC_RADIO_FAK_all               1044
#define IDC_RADIO_FAK_mit               1045
#define IDC_RADIO_FAK_ohne              1046
#define IDC_GROUP_FAK                   1047

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1047
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
