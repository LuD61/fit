#include "stdafx.h"
#include "sys_par.h"

struct SYS_PAR sys_par, sys_par_null;

void SYS_PAR_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((char *)   sys_par.sys_par_nam,  SQLCHAR, sizeof (sys_par.sys_par_nam));
    sqlout ((char *) sys_par.sys_par_nam,SQLCHAR,19);
    sqlout ((char *) sys_par.sys_par_wrt,SQLCHAR,2);
    sqlout ((char *) sys_par.sys_par_besch,SQLCHAR,33);
    sqlout ((long *) &sys_par.zei,SQLLONG,0);
    sqlout ((short *) &sys_par.delstatus,SQLSHORT,0);
            cursor = sqlcursor ("select sys_par.sys_par_nam,  "
"sys_par.sys_par_wrt,  sys_par.sys_par_besch,  sys_par.zei,  "
"sys_par.delstatus from sys_par "

#line 12 "sys_par.rpp"
                                  "where sys_par_nam = ?");
    sqlin ((char *) sys_par.sys_par_nam,SQLCHAR,19);
    sqlin ((char *) sys_par.sys_par_wrt,SQLCHAR,2);
    sqlin ((char *) sys_par.sys_par_besch,SQLCHAR,33);
    sqlin ((long *) &sys_par.zei,SQLLONG,0);
    sqlin ((short *) &sys_par.delstatus,SQLSHORT,0);
            sqltext = "update sys_par set "
"sys_par.sys_par_nam = ?,  sys_par.sys_par_wrt = ?,  "
"sys_par.sys_par_besch = ?,  sys_par.zei = ?,  "
"sys_par.delstatus = ? "

#line 14 "sys_par.rpp"
                                  "where sys_par_nam = ?";
            sqlin ((char *)   sys_par.sys_par_nam,  SQLCHAR, sizeof (sys_par.sys_par_nam));
            upd_cursor = sqlcursor (sqltext);

            sqlin ((char *)   sys_par.sys_par_nam,  SQLCHAR, sizeof (sys_par.sys_par_nam));
            test_upd_cursor = sqlcursor ("select sys_par_wrt from sys_par "
                                  "where sys_par_nam = ?");
            sqlin ((char *)   sys_par.sys_par_nam,  SQLCHAR, sizeof (sys_par.sys_par_nam));
            del_cursor = sqlcursor ("delete from sys_par "
                                  "where sys_par_nam = ?");
    sqlin ((char *) sys_par.sys_par_nam,SQLCHAR,19);
    sqlin ((char *) sys_par.sys_par_wrt,SQLCHAR,2);
    sqlin ((char *) sys_par.sys_par_besch,SQLCHAR,33);
    sqlin ((long *) &sys_par.zei,SQLLONG,0);
    sqlin ((short *) &sys_par.delstatus,SQLSHORT,0);
            ins_cursor = sqlcursor ("insert into sys_par ("
"sys_par_nam,  sys_par_wrt,  sys_par_besch,  zei,  delstatus) "

#line 25 "sys_par.rpp"
                                      "values "
                                      "(?,?,?,?,?)"); 

#line 27 "sys_par.rpp"
}
