#include "stdafx.h"
#include "ChoiceKun.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceKun::Sort1 = -1;
int CChoiceKun::Sort2 = -1;
int CChoiceKun::Sort3 = -1;
int CChoiceKun::Sort4 = -1;

CChoiceKun::CChoiceKun(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_Mdn = 0;
}

CChoiceKun::~CChoiceKun() 
{
	DestroyList ();
}

void CChoiceKun::DestroyList() 
{
	for (std::vector<CKunList *>::iterator pabl = KunList.begin (); pabl != KunList.end (); ++pabl)
	{
		CKunList *abl = *pabl;
		delete abl;
	}
    KunList.clear ();
}

void CChoiceKun::FillList () 
{
    short  mdn;
	long kun;
    TCHAR adr_krz [34];
	int cursor;

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Kunden"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Mandant"),        1, 50, LVCFMT_RIGHT);
    SetCol (_T("Kunde"),          2, 80, LVCFMT_RIGHT);
    SetCol (_T("Name"),  3, 250);

	if (KunList.size () == 0)
	{
		if (m_Mdn != 0)
		{
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((long *)&kun,      SQLLONG, 0);
			DbClass->sqlout ((LPTSTR)  adr_krz, SQLCHAR, sizeof (adr_krz));
			DbClass->sqlin ((LPTSTR)  &m_Mdn,   SQLSHORT, 0);
			cursor = DbClass->sqlcursor (_T("select kun.mdn, kun.kun, adr.adr_krz ")
				                             _T("from kun, adr where kun.kun > 0 ")
											 _T("and adr.adr = kun.adr1 ")
											 _T("and kun.mdn = ?"));
		}
		else
		{
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((long *)&kun,      SQLLONG, 0);
			DbClass->sqlout ((LPTSTR)  adr_krz, SQLCHAR, sizeof (adr_krz));
			cursor = DbClass->sqlcursor (_T("select kun.mdn, kun.kun, adr.adr_krz ")
			                             _T("from kun, adr where kun.kun > 0 ")
											 _T("and adr.adr = kun.adr1"));
		}
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) adr_krz;
			CDbUniCode::DbToUniCode (adr_krz, pos);
			CKunList *abl = new CKunList (mdn, kun, adr_krz);
			KunList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CKunList *>::iterator pabl = KunList.begin (); pabl != KunList.end (); ++pabl)
	{
		CKunList *abl = *pabl;
		CString Mdn;
		Mdn.Format (_T("%hd"), abl->mdn); 
		CString Kun;
		Kun.Format (_T("%ld"), abl->kun); 
		CString Num;
		CString Bez;
		_tcscpy (adr_krz, abl->adr_krz.GetBuffer ());

		CString LText;
		LText.Format (_T("%ld %s"), abl->kun, 
									abl->adr_krz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Mdn;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (Kun.GetBuffer (), i, 2);
        ret = SetItemText (adr_krz, i, 3);
        i ++;
    }

	SortRow = 2;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceKun::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceKun::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceKun::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
	int i;
    int count = ListBox->GetItemCount ();

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceKun::SearchKun (CListCtrl *ListBox, LPTSTR Search)
{
	int i;
    int count = ListBox->GetItemCount ();

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceKun::SearchAdrKrz (CListCtrl *ListBox, LPTSTR Search)
{
	int i;
    int count = ListBox->GetItemCount ();

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceKun::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             SearchKun (ListBox, EditText.GetBuffer (8));
             break;
        case 3 :
             EditText.MakeUpper ();
             SearchAdrKrz (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceKun::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceKun::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   short li1 = _tstoi (strItem1.GetBuffer ());
	   short li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort3;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort3);
	   }
	   return 0;
   }
   else if (SortRow == 4)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   return 0;
}


void CChoiceKun::Sort (CListCtrl *ListBox)
{
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
        case 4:
              Sort4 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CKunList *abl = KunList [i];
		   
		   abl->mdn     = _tstoi (ListBox->GetItemText (i, 1));
		   abl->kun     = _tstol (ListBox->GetItemText (i, 2));
		   abl->adr_krz = ListBox->GetItemText (i, 3);
	}
}

void CChoiceKun::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = KunList [idx];
}

CKunList *CChoiceKun::GetSelectedText ()
{
	CKunList *abl = (CKunList *) SelectedRow;
	return abl;
}

