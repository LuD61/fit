#include "stdafx.h"
#include "kun.h"

struct KUN kun, kun_null;

void KUN_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &kun.mdn, SQLSHORT, 0);
            sqlin ((long *)    &kun.kun, SQLLONG, 0);
    sqlout ((short *) &kun.mdn,SQLSHORT,0);
    sqlout ((short *) &kun.fil,SQLSHORT,0);
    sqlout ((long *) &kun.kun,SQLLONG,0);
    sqlout ((long *) &kun.adr1,SQLLONG,0);
    sqlout ((long *) &kun.adr2,SQLLONG,0);
    sqlout ((long *) &kun.adr3,SQLLONG,0);
    sqlout ((DATE_STRUCT *) &kun.kun_seit,SQLDATE,0);
    sqlout ((long *) &kun.txt_nr1,SQLLONG,0);
    sqlout ((TCHAR *) kun.frei_txt1,SQLCHAR,65);
    sqlout ((TCHAR *) kun.kun_krz1,SQLCHAR,17);
    sqlout ((TCHAR *) kun.kun_bran,SQLCHAR,2);
    sqlout ((TCHAR *) kun.kun_krz2,SQLCHAR,17);
    sqlout ((TCHAR *) kun.kun_krz3,SQLCHAR,17);
    sqlout ((short *) &kun.kun_typ,SQLSHORT,0);
    sqlout ((long *) &kun.bbn,SQLLONG,0);
    sqlout ((short *) &kun.pr_stu,SQLSHORT,0);
    sqlout ((long *) &kun.pr_lst,SQLLONG,0);
    sqlout ((TCHAR *) kun.vereinb,SQLCHAR,6);
    sqlout ((long *) &kun.inka_nr,SQLLONG,0);
    sqlout ((long *) &kun.vertr1,SQLLONG,0);
    sqlout ((long *) &kun.vertr2,SQLLONG,0);
    sqlout ((TCHAR *) kun.statk_period,SQLCHAR,2);
    sqlout ((TCHAR *) kun.a_period,SQLCHAR,2);
    sqlout ((short *) &kun.sprache,SQLSHORT,0);
    sqlout ((long *) &kun.txt_nr2,SQLLONG,0);
    sqlout ((TCHAR *) kun.frei_txt2,SQLCHAR,65);
    sqlout ((TCHAR *) kun.freifeld1,SQLCHAR,9);
    sqlout ((TCHAR *) kun.freifeld2,SQLCHAR,9);
    sqlout ((long *) &kun.tou,SQLLONG,0);
    sqlout ((TCHAR *) kun.vers_art,SQLCHAR,3);
    sqlout ((short *) &kun.lief_art,SQLSHORT,0);
    sqlout ((TCHAR *) kun.fra_ko_ber,SQLCHAR,3);
    sqlout ((short *) &kun.rue_schei,SQLSHORT,0);
    sqlout ((TCHAR *) kun.form_typ1,SQLCHAR,3);
    sqlout ((short *) &kun.auflage1,SQLSHORT,0);
    sqlout ((TCHAR *) kun.freifeld3,SQLCHAR,9);
    sqlout ((TCHAR *) kun.freifeld4,SQLCHAR,9);
    sqlout ((short *) &kun.zahl_art,SQLSHORT,0);
    sqlout ((short *) &kun.zahl_ziel,SQLSHORT,0);
    sqlout ((TCHAR *) kun.form_typ2,SQLCHAR,3);
    sqlout ((short *) &kun.auflage2,SQLSHORT,0);
    sqlout ((long *) &kun.txt_nr3,SQLLONG,0);
    sqlout ((TCHAR *) kun.frei_txt3,SQLCHAR,65);
    sqlout ((TCHAR *) kun.nr_bei_rech,SQLCHAR,17);
    sqlout ((TCHAR *) kun.rech_st,SQLCHAR,3);
    sqlout ((short *) &kun.sam_rech,SQLSHORT,0);
    sqlout ((short *) &kun.einz_ausw,SQLSHORT,0);
    sqlout ((short *) &kun.gut,SQLSHORT,0);
    sqlout ((TCHAR *) kun.rab_schl,SQLCHAR,9);
    sqlout ((double *) &kun.bonus1,SQLDOUBLE,0);
    sqlout ((double *) &kun.bonus2,SQLDOUBLE,0);
    sqlout ((double *) &kun.tdm_grenz1,SQLDOUBLE,0);
    sqlout ((double *) &kun.tdm_grenz2,SQLDOUBLE,0);
    sqlout ((double *) &kun.jr_plan_ums,SQLDOUBLE,0);
    sqlout ((TCHAR *) kun.deb_kto,SQLCHAR,9);
    sqlout ((double *) &kun.kred_lim,SQLDOUBLE,0);
    sqlout ((short *) &kun.inka_zaehl,SQLSHORT,0);
    sqlout ((TCHAR *) kun.bank_kun,SQLCHAR,37);
    sqlout ((long *) &kun.blz,SQLLONG,0);
    sqlout ((TCHAR *) kun.kto_nr,SQLCHAR,17);
    sqlout ((short *) &kun.hausbank,SQLSHORT,0);
    sqlout ((short *) &kun.kun_of_po,SQLSHORT,0);
    sqlout ((short *) &kun.kun_of_lf,SQLSHORT,0);
    sqlout ((short *) &kun.kun_of_best,SQLSHORT,0);
    sqlout ((short *) &kun.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) kun.kun_bran2,SQLCHAR,3);
    sqlout ((long *) &kun.rech_fuss_txt,SQLLONG,0);
    sqlout ((long *) &kun.ls_fuss_txt,SQLLONG,0);
    sqlout ((TCHAR *) kun.ust_id,SQLCHAR,12);
    sqlout ((long *) &kun.rech_kopf_txt,SQLLONG,0);
    sqlout ((long *) &kun.ls_kopf_txt,SQLLONG,0);
    sqlout ((TCHAR *) kun.gn_pkt_kz,SQLCHAR,2);
    sqlout ((TCHAR *) kun.sw_rab,SQLCHAR,2);
    sqlout ((TCHAR *) kun.bbs,SQLCHAR,9);
    sqlout ((long *) &kun.inka_nr2,SQLLONG,0);
    sqlout ((short *) &kun.sw_fil_gr,SQLSHORT,0);
    sqlout ((short *) &kun.sw_fil,SQLSHORT,0);
    sqlout ((TCHAR *) kun.ueb_kz,SQLCHAR,2);
    sqlout ((TCHAR *) kun.modif,SQLCHAR,2);
    sqlout ((short *) &kun.kun_leer_kz,SQLSHORT,0);
    sqlout ((TCHAR *) kun.ust_id16,SQLCHAR,17);
    sqlout ((TCHAR *) kun.iln,SQLCHAR,17);
    sqlout ((short *) &kun.waehrung,SQLSHORT,0);
    sqlout ((short *) &kun.pr_ausw,SQLSHORT,0);
    sqlout ((TCHAR *) kun.pr_hier,SQLCHAR,2);
    sqlout ((short *) &kun.pr_ausw_ls,SQLSHORT,0);
    sqlout ((short *) &kun.pr_ausw_re,SQLSHORT,0);
    sqlout ((short *) &kun.kun_gr1,SQLSHORT,0);
    sqlout ((short *) &kun.kun_gr2,SQLSHORT,0);
    sqlout ((short *) &kun.eg_kz,SQLSHORT,0);
    sqlout ((short *) &kun.bonitaet,SQLSHORT,0);
    sqlout ((short *) &kun.kred_vers,SQLSHORT,0);
    sqlout ((long *) &kun.kst,SQLLONG,0);
    sqlout ((TCHAR *) kun.edi_typ,SQLCHAR,2);
    sqlout ((long *) &kun.sedas_dta,SQLLONG,0);
    sqlout ((TCHAR *) kun.sedas_kz,SQLCHAR,3);
    sqlout ((short *) &kun.sedas_umf,SQLSHORT,0);
    sqlout ((short *) &kun.sedas_abr,SQLSHORT,0);
    sqlout ((short *) &kun.sedas_gesch,SQLSHORT,0);
    sqlout ((short *) &kun.sedas_satz,SQLSHORT,0);
    sqlout ((short *) &kun.sedas_med,SQLSHORT,0);
    sqlout ((TCHAR *) kun.sedas_nam,SQLCHAR,11);
    sqlout ((short *) &kun.sedas_abk1,SQLSHORT,0);
    sqlout ((short *) &kun.sedas_abk2,SQLSHORT,0);
    sqlout ((short *) &kun.sedas_abk3,SQLSHORT,0);
    sqlout ((TCHAR *) kun.sedas_nr1,SQLCHAR,9);
    sqlout ((TCHAR *) kun.sedas_nr2,SQLCHAR,9);
    sqlout ((TCHAR *) kun.sedas_nr3,SQLCHAR,9);
    sqlout ((short *) &kun.sedas_vb1,SQLSHORT,0);
    sqlout ((short *) &kun.sedas_vb2,SQLSHORT,0);
    sqlout ((short *) &kun.sedas_vb3,SQLSHORT,0);
    sqlout ((TCHAR *) kun.sedas_iln,SQLCHAR,17);
    sqlout ((long *) &kun.kond_kun,SQLLONG,0);
    sqlout ((short *) &kun.kun_schema,SQLSHORT,0);
    sqlout ((TCHAR *) kun.plattform,SQLCHAR,17);
    sqlout ((TCHAR *) kun.be_log,SQLCHAR,4);
    sqlout ((long *) &kun.stat_kun,SQLLONG,0);
    sqlout ((TCHAR *) kun.ust_nummer,SQLCHAR,25);
            cursor = sqlcursor (_T("select kun.mdn,  kun.fil,  ")
_T("kun.kun,  kun.adr1,  kun.adr2,  kun.adr3,  kun.kun_seit,  kun.txt_nr1,  ")
_T("kun.frei_txt1,  kun.kun_krz1,  kun.kun_bran,  kun.kun_krz2,  ")
_T("kun.kun_krz3,  kun.kun_typ,  kun.bbn,  kun.pr_stu,  kun.pr_lst,  ")
_T("kun.vereinb,  kun.inka_nr,  kun.vertr1,  kun.vertr2,  kun.statk_period,  ")
_T("kun.a_period,  kun.sprache,  kun.txt_nr2,  kun.frei_txt2,  kun.freifeld1,  ")
_T("kun.freifeld2,  kun.tou,  kun.vers_art,  kun.lief_art,  kun.fra_ko_ber,  ")
_T("kun.rue_schei,  kun.form_typ1,  kun.auflage1,  kun.freifeld3,  ")
_T("kun.freifeld4,  kun.zahl_art,  kun.zahl_ziel,  kun.form_typ2,  ")
_T("kun.auflage2,  kun.txt_nr3,  kun.frei_txt3,  kun.nr_bei_rech,  ")
_T("kun.rech_st,  kun.sam_rech,  kun.einz_ausw,  kun.gut,  kun.rab_schl,  ")
_T("kun.bonus1,  kun.bonus2,  kun.tdm_grenz1,  kun.tdm_grenz2,  ")
_T("kun.jr_plan_ums,  kun.deb_kto,  kun.kred_lim,  kun.inka_zaehl,  ")
_T("kun.bank_kun,  kun.blz,  kun.kto_nr,  kun.hausbank,  kun.kun_of_po,  ")
_T("kun.kun_of_lf,  kun.kun_of_best,  kun.delstatus,  kun.kun_bran2,  ")
_T("kun.rech_fuss_txt,  kun.ls_fuss_txt,  kun.ust_id,  kun.rech_kopf_txt,  ")
_T("kun.ls_kopf_txt,  kun.gn_pkt_kz,  kun.sw_rab,  kun.bbs,  kun.inka_nr2,  ")
_T("kun.sw_fil_gr,  kun.sw_fil,  kun.ueb_kz,  kun.modif,  kun.kun_leer_kz,  ")
_T("kun.ust_id16,  kun.iln,  kun.waehrung,  kun.pr_ausw,  kun.pr_hier,  ")
_T("kun.pr_ausw_ls,  kun.pr_ausw_re,  kun.kun_gr1,  kun.kun_gr2,  kun.eg_kz,  ")
_T("kun.bonitaet,  kun.kred_vers,  kun.kst,  kun.edi_typ,  kun.sedas_dta,  ")
_T("kun.sedas_kz,  kun.sedas_umf,  kun.sedas_abr,  kun.sedas_gesch,  ")
_T("kun.sedas_satz,  kun.sedas_med,  kun.sedas_nam,  kun.sedas_abk1,  ")
_T("kun.sedas_abk2,  kun.sedas_abk3,  kun.sedas_nr1,  kun.sedas_nr2,  ")
_T("kun.sedas_nr3,  kun.sedas_vb1,  kun.sedas_vb2,  kun.sedas_vb3,  ")
_T("kun.sedas_iln,  kun.kond_kun,  kun.kun_schema,  kun.plattform,  ")
_T("kun.be_log,  kun.stat_kun,  kun.ust_nummer from kun ")

#line 13 "kun.rpp"
                                  _T("where mdn = ? ")
				  _T("and kun = ?"));
    sqlin ((short *) &kun.mdn,SQLSHORT,0);
    sqlin ((short *) &kun.fil,SQLSHORT,0);
    sqlin ((long *) &kun.kun,SQLLONG,0);
    sqlin ((long *) &kun.adr1,SQLLONG,0);
    sqlin ((long *) &kun.adr2,SQLLONG,0);
    sqlin ((long *) &kun.adr3,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &kun.kun_seit,SQLDATE,0);
    sqlin ((long *) &kun.txt_nr1,SQLLONG,0);
    sqlin ((TCHAR *) kun.frei_txt1,SQLCHAR,65);
    sqlin ((TCHAR *) kun.kun_krz1,SQLCHAR,17);
    sqlin ((TCHAR *) kun.kun_bran,SQLCHAR,2);
    sqlin ((TCHAR *) kun.kun_krz2,SQLCHAR,17);
    sqlin ((TCHAR *) kun.kun_krz3,SQLCHAR,17);
    sqlin ((short *) &kun.kun_typ,SQLSHORT,0);
    sqlin ((long *) &kun.bbn,SQLLONG,0);
    sqlin ((short *) &kun.pr_stu,SQLSHORT,0);
    sqlin ((long *) &kun.pr_lst,SQLLONG,0);
    sqlin ((TCHAR *) kun.vereinb,SQLCHAR,6);
    sqlin ((long *) &kun.inka_nr,SQLLONG,0);
    sqlin ((long *) &kun.vertr1,SQLLONG,0);
    sqlin ((long *) &kun.vertr2,SQLLONG,0);
    sqlin ((TCHAR *) kun.statk_period,SQLCHAR,2);
    sqlin ((TCHAR *) kun.a_period,SQLCHAR,2);
    sqlin ((short *) &kun.sprache,SQLSHORT,0);
    sqlin ((long *) &kun.txt_nr2,SQLLONG,0);
    sqlin ((TCHAR *) kun.frei_txt2,SQLCHAR,65);
    sqlin ((TCHAR *) kun.freifeld1,SQLCHAR,9);
    sqlin ((TCHAR *) kun.freifeld2,SQLCHAR,9);
    sqlin ((long *) &kun.tou,SQLLONG,0);
    sqlin ((TCHAR *) kun.vers_art,SQLCHAR,3);
    sqlin ((short *) &kun.lief_art,SQLSHORT,0);
    sqlin ((TCHAR *) kun.fra_ko_ber,SQLCHAR,3);
    sqlin ((short *) &kun.rue_schei,SQLSHORT,0);
    sqlin ((TCHAR *) kun.form_typ1,SQLCHAR,3);
    sqlin ((short *) &kun.auflage1,SQLSHORT,0);
    sqlin ((TCHAR *) kun.freifeld3,SQLCHAR,9);
    sqlin ((TCHAR *) kun.freifeld4,SQLCHAR,9);
    sqlin ((short *) &kun.zahl_art,SQLSHORT,0);
    sqlin ((short *) &kun.zahl_ziel,SQLSHORT,0);
    sqlin ((TCHAR *) kun.form_typ2,SQLCHAR,3);
    sqlin ((short *) &kun.auflage2,SQLSHORT,0);
    sqlin ((long *) &kun.txt_nr3,SQLLONG,0);
    sqlin ((TCHAR *) kun.frei_txt3,SQLCHAR,65);
    sqlin ((TCHAR *) kun.nr_bei_rech,SQLCHAR,17);
    sqlin ((TCHAR *) kun.rech_st,SQLCHAR,3);
    sqlin ((short *) &kun.sam_rech,SQLSHORT,0);
    sqlin ((short *) &kun.einz_ausw,SQLSHORT,0);
    sqlin ((short *) &kun.gut,SQLSHORT,0);
    sqlin ((TCHAR *) kun.rab_schl,SQLCHAR,9);
    sqlin ((double *) &kun.bonus1,SQLDOUBLE,0);
    sqlin ((double *) &kun.bonus2,SQLDOUBLE,0);
    sqlin ((double *) &kun.tdm_grenz1,SQLDOUBLE,0);
    sqlin ((double *) &kun.tdm_grenz2,SQLDOUBLE,0);
    sqlin ((double *) &kun.jr_plan_ums,SQLDOUBLE,0);
    sqlin ((TCHAR *) kun.deb_kto,SQLCHAR,9);
    sqlin ((double *) &kun.kred_lim,SQLDOUBLE,0);
    sqlin ((short *) &kun.inka_zaehl,SQLSHORT,0);
    sqlin ((TCHAR *) kun.bank_kun,SQLCHAR,37);
    sqlin ((long *) &kun.blz,SQLLONG,0);
    sqlin ((TCHAR *) kun.kto_nr,SQLCHAR,17);
    sqlin ((short *) &kun.hausbank,SQLSHORT,0);
    sqlin ((short *) &kun.kun_of_po,SQLSHORT,0);
    sqlin ((short *) &kun.kun_of_lf,SQLSHORT,0);
    sqlin ((short *) &kun.kun_of_best,SQLSHORT,0);
    sqlin ((short *) &kun.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) kun.kun_bran2,SQLCHAR,3);
    sqlin ((long *) &kun.rech_fuss_txt,SQLLONG,0);
    sqlin ((long *) &kun.ls_fuss_txt,SQLLONG,0);
    sqlin ((TCHAR *) kun.ust_id,SQLCHAR,12);
    sqlin ((long *) &kun.rech_kopf_txt,SQLLONG,0);
    sqlin ((long *) &kun.ls_kopf_txt,SQLLONG,0);
    sqlin ((TCHAR *) kun.gn_pkt_kz,SQLCHAR,2);
    sqlin ((TCHAR *) kun.sw_rab,SQLCHAR,2);
    sqlin ((TCHAR *) kun.bbs,SQLCHAR,9);
    sqlin ((long *) &kun.inka_nr2,SQLLONG,0);
    sqlin ((short *) &kun.sw_fil_gr,SQLSHORT,0);
    sqlin ((short *) &kun.sw_fil,SQLSHORT,0);
    sqlin ((TCHAR *) kun.ueb_kz,SQLCHAR,2);
    sqlin ((TCHAR *) kun.modif,SQLCHAR,2);
    sqlin ((short *) &kun.kun_leer_kz,SQLSHORT,0);
    sqlin ((TCHAR *) kun.ust_id16,SQLCHAR,17);
    sqlin ((TCHAR *) kun.iln,SQLCHAR,17);
    sqlin ((short *) &kun.waehrung,SQLSHORT,0);
    sqlin ((short *) &kun.pr_ausw,SQLSHORT,0);
    sqlin ((TCHAR *) kun.pr_hier,SQLCHAR,2);
    sqlin ((short *) &kun.pr_ausw_ls,SQLSHORT,0);
    sqlin ((short *) &kun.pr_ausw_re,SQLSHORT,0);
    sqlin ((short *) &kun.kun_gr1,SQLSHORT,0);
    sqlin ((short *) &kun.kun_gr2,SQLSHORT,0);
    sqlin ((short *) &kun.eg_kz,SQLSHORT,0);
    sqlin ((short *) &kun.bonitaet,SQLSHORT,0);
    sqlin ((short *) &kun.kred_vers,SQLSHORT,0);
    sqlin ((long *) &kun.kst,SQLLONG,0);
    sqlin ((TCHAR *) kun.edi_typ,SQLCHAR,2);
    sqlin ((long *) &kun.sedas_dta,SQLLONG,0);
    sqlin ((TCHAR *) kun.sedas_kz,SQLCHAR,3);
    sqlin ((short *) &kun.sedas_umf,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_abr,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_gesch,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_satz,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_med,SQLSHORT,0);
    sqlin ((TCHAR *) kun.sedas_nam,SQLCHAR,11);
    sqlin ((short *) &kun.sedas_abk1,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_abk2,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_abk3,SQLSHORT,0);
    sqlin ((TCHAR *) kun.sedas_nr1,SQLCHAR,9);
    sqlin ((TCHAR *) kun.sedas_nr2,SQLCHAR,9);
    sqlin ((TCHAR *) kun.sedas_nr3,SQLCHAR,9);
    sqlin ((short *) &kun.sedas_vb1,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_vb2,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_vb3,SQLSHORT,0);
    sqlin ((TCHAR *) kun.sedas_iln,SQLCHAR,17);
    sqlin ((long *) &kun.kond_kun,SQLLONG,0);
    sqlin ((short *) &kun.kun_schema,SQLSHORT,0);
    sqlin ((TCHAR *) kun.plattform,SQLCHAR,17);
    sqlin ((TCHAR *) kun.be_log,SQLCHAR,4);
    sqlin ((long *) &kun.stat_kun,SQLLONG,0);
    sqlin ((TCHAR *) kun.ust_nummer,SQLCHAR,25);
            sqltext = _T("update kun set kun.mdn = ?,  ")
_T("kun.fil = ?,  kun.kun = ?,  kun.adr1 = ?,  kun.adr2 = ?,  kun.adr3 = ?,  ")
_T("kun.kun_seit = ?,  kun.txt_nr1 = ?,  kun.frei_txt1 = ?,  ")
_T("kun.kun_krz1 = ?,  kun.kun_bran = ?,  kun.kun_krz2 = ?,  ")
_T("kun.kun_krz3 = ?,  kun.kun_typ = ?,  kun.bbn = ?,  kun.pr_stu = ?,  ")
_T("kun.pr_lst = ?,  kun.vereinb = ?,  kun.inka_nr = ?,  kun.vertr1 = ?,  ")
_T("kun.vertr2 = ?,  kun.statk_period = ?,  kun.a_period = ?,  ")
_T("kun.sprache = ?,  kun.txt_nr2 = ?,  kun.frei_txt2 = ?,  ")
_T("kun.freifeld1 = ?,  kun.freifeld2 = ?,  kun.tou = ?,  ")
_T("kun.vers_art = ?,  kun.lief_art = ?,  kun.fra_ko_ber = ?,  ")
_T("kun.rue_schei = ?,  kun.form_typ1 = ?,  kun.auflage1 = ?,  ")
_T("kun.freifeld3 = ?,  kun.freifeld4 = ?,  kun.zahl_art = ?,  ")
_T("kun.zahl_ziel = ?,  kun.form_typ2 = ?,  kun.auflage2 = ?,  ")
_T("kun.txt_nr3 = ?,  kun.frei_txt3 = ?,  kun.nr_bei_rech = ?,  ")
_T("kun.rech_st = ?,  kun.sam_rech = ?,  kun.einz_ausw = ?,  kun.gut = ?,  ")
_T("kun.rab_schl = ?,  kun.bonus1 = ?,  kun.bonus2 = ?,  ")
_T("kun.tdm_grenz1 = ?,  kun.tdm_grenz2 = ?,  kun.jr_plan_ums = ?,  ")
_T("kun.deb_kto = ?,  kun.kred_lim = ?,  kun.inka_zaehl = ?,  ")
_T("kun.bank_kun = ?,  kun.blz = ?,  kun.kto_nr = ?,  kun.hausbank = ?,  ")
_T("kun.kun_of_po = ?,  kun.kun_of_lf = ?,  kun.kun_of_best = ?,  ")
_T("kun.delstatus = ?,  kun.kun_bran2 = ?,  kun.rech_fuss_txt = ?,  ")
_T("kun.ls_fuss_txt = ?,  kun.ust_id = ?,  kun.rech_kopf_txt = ?,  ")
_T("kun.ls_kopf_txt = ?,  kun.gn_pkt_kz = ?,  kun.sw_rab = ?,  ")
_T("kun.bbs = ?,  kun.inka_nr2 = ?,  kun.sw_fil_gr = ?,  kun.sw_fil = ?,  ")
_T("kun.ueb_kz = ?,  kun.modif = ?,  kun.kun_leer_kz = ?,  ")
_T("kun.ust_id16 = ?,  kun.iln = ?,  kun.waehrung = ?,  kun.pr_ausw = ?,  ")
_T("kun.pr_hier = ?,  kun.pr_ausw_ls = ?,  kun.pr_ausw_re = ?,  ")
_T("kun.kun_gr1 = ?,  kun.kun_gr2 = ?,  kun.eg_kz = ?,  kun.bonitaet = ?,  ")
_T("kun.kred_vers = ?,  kun.kst = ?,  kun.edi_typ = ?,  kun.sedas_dta = ?,  ")
_T("kun.sedas_kz = ?,  kun.sedas_umf = ?,  kun.sedas_abr = ?,  ")
_T("kun.sedas_gesch = ?,  kun.sedas_satz = ?,  kun.sedas_med = ?,  ")
_T("kun.sedas_nam = ?,  kun.sedas_abk1 = ?,  kun.sedas_abk2 = ?,  ")
_T("kun.sedas_abk3 = ?,  kun.sedas_nr1 = ?,  kun.sedas_nr2 = ?,  ")
_T("kun.sedas_nr3 = ?,  kun.sedas_vb1 = ?,  kun.sedas_vb2 = ?,  ")
_T("kun.sedas_vb3 = ?,  kun.sedas_iln = ?,  kun.kond_kun = ?,  ")
_T("kun.kun_schema = ?,  kun.plattform = ?,  kun.be_log = ?,  ")
_T("kun.stat_kun = ?,  kun.ust_nummer = ? ")

#line 16 "kun.rpp"
                                  _T("where mdn = ? ")
				  _T("and kun = ?");
            sqlin ((short *)   &kun.mdn, SQLSHORT, 0);
            sqlin ((long *)    &kun.kun, SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &kun.mdn, SQLSHORT, 0);
            sqlin ((long *)    &kun.kun, SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select kun from kun ")
                                  _T("where mdn = ? ")
				  _T("and kun = ?"));
            sqlin ((short *)   &kun.mdn, SQLSHORT, 0);
            sqlin ((long *)    &kun.kun, SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from kun ")
                                  _T("where mdn = ? ")
				  _T("and kun = ?"));
    sqlin ((short *) &kun.mdn,SQLSHORT,0);
    sqlin ((short *) &kun.fil,SQLSHORT,0);
    sqlin ((long *) &kun.kun,SQLLONG,0);
    sqlin ((long *) &kun.adr1,SQLLONG,0);
    sqlin ((long *) &kun.adr2,SQLLONG,0);
    sqlin ((long *) &kun.adr3,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &kun.kun_seit,SQLDATE,0);
    sqlin ((long *) &kun.txt_nr1,SQLLONG,0);
    sqlin ((TCHAR *) kun.frei_txt1,SQLCHAR,65);
    sqlin ((TCHAR *) kun.kun_krz1,SQLCHAR,17);
    sqlin ((TCHAR *) kun.kun_bran,SQLCHAR,2);
    sqlin ((TCHAR *) kun.kun_krz2,SQLCHAR,17);
    sqlin ((TCHAR *) kun.kun_krz3,SQLCHAR,17);
    sqlin ((short *) &kun.kun_typ,SQLSHORT,0);
    sqlin ((long *) &kun.bbn,SQLLONG,0);
    sqlin ((short *) &kun.pr_stu,SQLSHORT,0);
    sqlin ((long *) &kun.pr_lst,SQLLONG,0);
    sqlin ((TCHAR *) kun.vereinb,SQLCHAR,6);
    sqlin ((long *) &kun.inka_nr,SQLLONG,0);
    sqlin ((long *) &kun.vertr1,SQLLONG,0);
    sqlin ((long *) &kun.vertr2,SQLLONG,0);
    sqlin ((TCHAR *) kun.statk_period,SQLCHAR,2);
    sqlin ((TCHAR *) kun.a_period,SQLCHAR,2);
    sqlin ((short *) &kun.sprache,SQLSHORT,0);
    sqlin ((long *) &kun.txt_nr2,SQLLONG,0);
    sqlin ((TCHAR *) kun.frei_txt2,SQLCHAR,65);
    sqlin ((TCHAR *) kun.freifeld1,SQLCHAR,9);
    sqlin ((TCHAR *) kun.freifeld2,SQLCHAR,9);
    sqlin ((long *) &kun.tou,SQLLONG,0);
    sqlin ((TCHAR *) kun.vers_art,SQLCHAR,3);
    sqlin ((short *) &kun.lief_art,SQLSHORT,0);
    sqlin ((TCHAR *) kun.fra_ko_ber,SQLCHAR,3);
    sqlin ((short *) &kun.rue_schei,SQLSHORT,0);
    sqlin ((TCHAR *) kun.form_typ1,SQLCHAR,3);
    sqlin ((short *) &kun.auflage1,SQLSHORT,0);
    sqlin ((TCHAR *) kun.freifeld3,SQLCHAR,9);
    sqlin ((TCHAR *) kun.freifeld4,SQLCHAR,9);
    sqlin ((short *) &kun.zahl_art,SQLSHORT,0);
    sqlin ((short *) &kun.zahl_ziel,SQLSHORT,0);
    sqlin ((TCHAR *) kun.form_typ2,SQLCHAR,3);
    sqlin ((short *) &kun.auflage2,SQLSHORT,0);
    sqlin ((long *) &kun.txt_nr3,SQLLONG,0);
    sqlin ((TCHAR *) kun.frei_txt3,SQLCHAR,65);
    sqlin ((TCHAR *) kun.nr_bei_rech,SQLCHAR,17);
    sqlin ((TCHAR *) kun.rech_st,SQLCHAR,3);
    sqlin ((short *) &kun.sam_rech,SQLSHORT,0);
    sqlin ((short *) &kun.einz_ausw,SQLSHORT,0);
    sqlin ((short *) &kun.gut,SQLSHORT,0);
    sqlin ((TCHAR *) kun.rab_schl,SQLCHAR,9);
    sqlin ((double *) &kun.bonus1,SQLDOUBLE,0);
    sqlin ((double *) &kun.bonus2,SQLDOUBLE,0);
    sqlin ((double *) &kun.tdm_grenz1,SQLDOUBLE,0);
    sqlin ((double *) &kun.tdm_grenz2,SQLDOUBLE,0);
    sqlin ((double *) &kun.jr_plan_ums,SQLDOUBLE,0);
    sqlin ((TCHAR *) kun.deb_kto,SQLCHAR,9);
    sqlin ((double *) &kun.kred_lim,SQLDOUBLE,0);
    sqlin ((short *) &kun.inka_zaehl,SQLSHORT,0);
    sqlin ((TCHAR *) kun.bank_kun,SQLCHAR,37);
    sqlin ((long *) &kun.blz,SQLLONG,0);
    sqlin ((TCHAR *) kun.kto_nr,SQLCHAR,17);
    sqlin ((short *) &kun.hausbank,SQLSHORT,0);
    sqlin ((short *) &kun.kun_of_po,SQLSHORT,0);
    sqlin ((short *) &kun.kun_of_lf,SQLSHORT,0);
    sqlin ((short *) &kun.kun_of_best,SQLSHORT,0);
    sqlin ((short *) &kun.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) kun.kun_bran2,SQLCHAR,3);
    sqlin ((long *) &kun.rech_fuss_txt,SQLLONG,0);
    sqlin ((long *) &kun.ls_fuss_txt,SQLLONG,0);
    sqlin ((TCHAR *) kun.ust_id,SQLCHAR,12);
    sqlin ((long *) &kun.rech_kopf_txt,SQLLONG,0);
    sqlin ((long *) &kun.ls_kopf_txt,SQLLONG,0);
    sqlin ((TCHAR *) kun.gn_pkt_kz,SQLCHAR,2);
    sqlin ((TCHAR *) kun.sw_rab,SQLCHAR,2);
    sqlin ((TCHAR *) kun.bbs,SQLCHAR,9);
    sqlin ((long *) &kun.inka_nr2,SQLLONG,0);
    sqlin ((short *) &kun.sw_fil_gr,SQLSHORT,0);
    sqlin ((short *) &kun.sw_fil,SQLSHORT,0);
    sqlin ((TCHAR *) kun.ueb_kz,SQLCHAR,2);
    sqlin ((TCHAR *) kun.modif,SQLCHAR,2);
    sqlin ((short *) &kun.kun_leer_kz,SQLSHORT,0);
    sqlin ((TCHAR *) kun.ust_id16,SQLCHAR,17);
    sqlin ((TCHAR *) kun.iln,SQLCHAR,17);
    sqlin ((short *) &kun.waehrung,SQLSHORT,0);
    sqlin ((short *) &kun.pr_ausw,SQLSHORT,0);
    sqlin ((TCHAR *) kun.pr_hier,SQLCHAR,2);
    sqlin ((short *) &kun.pr_ausw_ls,SQLSHORT,0);
    sqlin ((short *) &kun.pr_ausw_re,SQLSHORT,0);
    sqlin ((short *) &kun.kun_gr1,SQLSHORT,0);
    sqlin ((short *) &kun.kun_gr2,SQLSHORT,0);
    sqlin ((short *) &kun.eg_kz,SQLSHORT,0);
    sqlin ((short *) &kun.bonitaet,SQLSHORT,0);
    sqlin ((short *) &kun.kred_vers,SQLSHORT,0);
    sqlin ((long *) &kun.kst,SQLLONG,0);
    sqlin ((TCHAR *) kun.edi_typ,SQLCHAR,2);
    sqlin ((long *) &kun.sedas_dta,SQLLONG,0);
    sqlin ((TCHAR *) kun.sedas_kz,SQLCHAR,3);
    sqlin ((short *) &kun.sedas_umf,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_abr,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_gesch,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_satz,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_med,SQLSHORT,0);
    sqlin ((TCHAR *) kun.sedas_nam,SQLCHAR,11);
    sqlin ((short *) &kun.sedas_abk1,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_abk2,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_abk3,SQLSHORT,0);
    sqlin ((TCHAR *) kun.sedas_nr1,SQLCHAR,9);
    sqlin ((TCHAR *) kun.sedas_nr2,SQLCHAR,9);
    sqlin ((TCHAR *) kun.sedas_nr3,SQLCHAR,9);
    sqlin ((short *) &kun.sedas_vb1,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_vb2,SQLSHORT,0);
    sqlin ((short *) &kun.sedas_vb3,SQLSHORT,0);
    sqlin ((TCHAR *) kun.sedas_iln,SQLCHAR,17);
    sqlin ((long *) &kun.kond_kun,SQLLONG,0);
    sqlin ((short *) &kun.kun_schema,SQLSHORT,0);
    sqlin ((TCHAR *) kun.plattform,SQLCHAR,17);
    sqlin ((TCHAR *) kun.be_log,SQLCHAR,4);
    sqlin ((long *) &kun.stat_kun,SQLLONG,0);
    sqlin ((TCHAR *) kun.ust_nummer,SQLCHAR,25);
            ins_cursor = sqlcursor (_T("insert into kun (mdn,  ")
_T("fil,  kun,  adr1,  adr2,  adr3,  kun_seit,  txt_nr1,  frei_txt1,  kun_krz1,  kun_bran,  ")
_T("kun_krz2,  kun_krz3,  kun_typ,  bbn,  pr_stu,  pr_lst,  vereinb,  inka_nr,  vertr1,  ")
_T("vertr2,  statk_period,  a_period,  sprache,  txt_nr2,  frei_txt2,  freifeld1,  ")
_T("freifeld2,  tou,  vers_art,  lief_art,  fra_ko_ber,  rue_schei,  form_typ1,  ")
_T("auflage1,  freifeld3,  freifeld4,  zahl_art,  zahl_ziel,  form_typ2,  auflage2,  ")
_T("txt_nr3,  frei_txt3,  nr_bei_rech,  rech_st,  sam_rech,  einz_ausw,  gut,  ")
_T("rab_schl,  bonus1,  bonus2,  tdm_grenz1,  tdm_grenz2,  jr_plan_ums,  deb_kto,  ")
_T("kred_lim,  inka_zaehl,  bank_kun,  blz,  kto_nr,  hausbank,  kun_of_po,  ")
_T("kun_of_lf,  kun_of_best,  delstatus,  kun_bran2,  rech_fuss_txt,  ")
_T("ls_fuss_txt,  ust_id,  rech_kopf_txt,  ls_kopf_txt,  gn_pkt_kz,  sw_rab,  bbs,  ")
_T("inka_nr2,  sw_fil_gr,  sw_fil,  ueb_kz,  modif,  kun_leer_kz,  ust_id16,  iln,  ")
_T("waehrung,  pr_ausw,  pr_hier,  pr_ausw_ls,  pr_ausw_re,  kun_gr1,  kun_gr2,  ")
_T("eg_kz,  bonitaet,  kred_vers,  kst,  edi_typ,  sedas_dta,  sedas_kz,  sedas_umf,  ")
_T("sedas_abr,  sedas_gesch,  sedas_satz,  sedas_med,  sedas_nam,  sedas_abk1,  ")
_T("sedas_abk2,  sedas_abk3,  sedas_nr1,  sedas_nr2,  sedas_nr3,  sedas_vb1,  ")
_T("sedas_vb2,  sedas_vb3,  sedas_iln,  kond_kun,  kun_schema,  plattform,  be_log,  ")
_T("stat_kun,  ust_nummer) ")

#line 33 "kun.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 35 "kun.rpp"
}
