// 56200Dlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "56200.h"
#include "56200Dlg.h"
#include ".\56200dlg.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

	COleDateTime oCurTime;

// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMyDlg Dialogfeld



CMyDlg::CMyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMyDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	ModalChoice = FALSE;
	CloseChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ChoiceKun = NULL;
	ModalChoiceKun = TRUE;

}


void CMyDlg::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDC_STATIC_Mdn, m_StaticMdn);
	DDX_Control(pDX, IDC_STATIC_FORMTYP, m_StaticFormtyp);
	DDX_Control(pDX, IDC_STATIC_Kunde, m_StaticKun);
	DDX_Control(pDX, IDC_STATIC_von, m_Staticvon);
	DDX_Control(pDX, IDC_STATIC_bis1, m_Staticbis1);
	DDX_Control(pDX, IDC_STATIC_Kunbran2, m_StaticKunbran2);
	DDX_Control(pDX, IDC_STATIC_Datum, m_StaticDatum);
	DDX_Control(pDX, IDC_STATIC_STATUS, m_StaticStatus);
	DDX_Control(pDX, IDC_Edit_Mdn_von, m_Mdn);
	DDX_Control(pDX, IDC_MDN_BEZ, m_MdnBez);
	DDX_Control(pDX, IDC_FORMTYP, m_Formtyp);
	DDX_Control(pDX, IDC_Edit_Kunde_von, m_Kun_von);
	DDX_Control(pDX, IDC_Edit_Kunde_bis, m_Kun_bis);
	DDX_Control(pDX, IDC_Kunbran2_von, m_Kunbran2_von);
	DDX_Control(pDX, IDC_KUNBRAN2_bis, m_Kunbran2_bis);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_Datum_von);
	DDX_Control(pDX, IDC_DATETIMEPICKER2, m_Datum_bis);
	DDX_Control(pDX, IDC_STATUS, m_Status);
	DDX_Control(pDX, IDC_GROUP_RECHART, m_GroupRechart);
	DDX_Control(pDX, IDC_GROUP_FAK, m_GroupFak);
	DDX_Control(pDX, IDC_Group1, m_Group1);
	DDX_Control(pDX, IDC_CHECK_RECH, m_Check_Rech);
	DDX_Control(pDX, IDC_CHECK_GUT, m_Check_Gut);
	DDX_Control(pDX, IDC_CHECK_BAR, m_Check_Bar);
	DDX_Control(pDX, IDC_RADIO_FAK_all, m_Fak_all);
	DDX_Control(pDX, IDC_RADIO_FAK_mit, m_Fak_mit);
	DDX_Control(pDX, IDC_RADIO_FAK_ohne, m_Fak_ohne);

	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMyDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
	ON_BN_CLICKED(IDC_KUN1CHOICE , OnKun1choice)
	ON_BN_CLICKED(IDC_KUN2CHOICE , OnKun2choice)
	ON_EN_CHANGE(IDC_Edit_Mdn_von, OnEnChangeEditMdnvon)

	ON_CBN_SELCHANGE(IDC_KUNBRAN2_bis2, OnCbnSelchangeKunbran2bis2)
	ON_NOTIFY(NM_KILLFOCUS, IDC_DATETIMEPICKER1, OnNMKillfocusDatetimepicker1)
    ON_NOTIFY(NM_KILLFOCUS, IDC_DATETIMEPICKER2, OnNMKillfocusDatetimepicker2)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDOK2, OnBnClickedOk2)
END_MESSAGE_MAP()


// CMyDlg Meldungshandler

BOOL CMyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 10);  //Spaltenabstand und Zeilenabstand

	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	Kun1Grid.Create (this, 2, 2);
    Kun1Grid.SetBorder (0, 0);
    Kun1Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun1 = new CCtrlInfo (&m_Kun_von, 0, 0, 1, 1);
	Kun1Grid.Add (c_Kun1);
	CtrlGrid.CreateChoiceButton (m_Kun1Choice, IDC_KUN1CHOICE, this);
	CCtrlInfo *c_Kun1Choice = new CCtrlInfo (&m_Kun1Choice, 1, 0, 1, 1);
	Kun1Grid.Add (c_Kun1Choice);

	Kun2Grid.Create (this, 2, 2);
    Kun2Grid.SetBorder (0, 0);
    Kun2Grid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun2 = new CCtrlInfo (&m_Kun_bis, 0, 0, 1, 1);
	Kun2Grid.Add (c_Kun2);
	CtrlGrid.CreateChoiceButton (m_Kun2Choice, IDC_KUN2CHOICE, this);
	CCtrlInfo *c_Kun2Choice = new CCtrlInfo (&m_Kun2Choice, 1, 0, 1, 1);
	Kun2Grid.Add (c_Kun2Choice);

	RechartGrid.Create (this, 8, 8);
    RechartGrid.SetBorder (0, 0);
    RechartGrid.SetGridSpace (15, 3);
	CCtrlInfo *c_Group_Rechart = new CCtrlInfo (&m_GroupRechart, 1, 0, 4, 1);
	RechartGrid.Add (c_Group_Rechart);
	CCtrlInfo *c_Check_Rech = new CCtrlInfo (&m_Check_Rech, 2, 1, 1, 1);
	RechartGrid.Add (c_Check_Rech);
	CCtrlInfo *c_Check_Gut = new CCtrlInfo (&m_Check_Gut, 3, 1, 1, 1);
	RechartGrid.Add (c_Check_Gut);
	CCtrlInfo *c_Check_Bar = new CCtrlInfo (&m_Check_Bar, 4, 1, 1, 1);
	RechartGrid.Add (c_Check_Bar);


	FakGrid.Create (this, 8, 8);
    FakGrid.SetBorder (0, 0);
    FakGrid.SetGridSpace (15, 3);
	CCtrlInfo *c_Group_Fak = new CCtrlInfo (&m_GroupFak, 1, 0, 4, 1);
	FakGrid.Add (c_Group_Fak);
	CCtrlInfo *c_Fak_all = new CCtrlInfo (&m_Fak_all, 2, 1, 1, 1);
	FakGrid.Add (c_Fak_all);
	CCtrlInfo *c_Fak_mit = new CCtrlInfo (&m_Fak_mit, 3, 1, 1, 1);
	FakGrid.Add (c_Fak_mit);
	CCtrlInfo *c_Fak_ohne = new CCtrlInfo (&m_Fak_ohne, 4, 1, 1, 1);
	FakGrid.Add (c_Fak_ohne);



//Mandant
	CCtrlInfo *c_StaticMdn     = new CCtrlInfo (&m_StaticMdn, 1, 0, 1, 1); 
	CtrlGrid.Add (c_StaticMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnBez     = new CCtrlInfo (&m_MdnBez, 3, 0, 2, 1); 
	CtrlGrid.Add (c_MdnBez);
//Formular
	CCtrlInfo *c_StaticFormtyp     = new CCtrlInfo (&m_StaticFormtyp, 1, 2, 1, 1); 
	CtrlGrid.Add (c_StaticFormtyp);
	CCtrlInfo *c_Formtyp     = new CCtrlInfo (&m_Formtyp, 2, 2, 4, 1); 
	CtrlGrid.Add (c_Formtyp);


	CCtrlInfo *c_Group1 = new CCtrlInfo (&m_Group1, 0, 3, 6, 1);
	CtrlGrid.Add (c_Group1);
//von-bis Items
	CCtrlInfo *c_Staticvon     = new CCtrlInfo (&m_Staticvon, 2, 4, 1, 1); 
	CtrlGrid.Add (c_Staticvon);
	CCtrlInfo *c_Staticbis1     = new CCtrlInfo (&m_Staticbis1, 5, 4, 1, 1); 
	CtrlGrid.Add (c_Staticbis1);
//Kunde von-bis
	CCtrlInfo *c_StaticKun     = new CCtrlInfo (&m_StaticKun, 1, 5, 1, 1); 
	CtrlGrid.Add (c_StaticKun);
	CCtrlInfo *c_Kun1Grid     = new CCtrlInfo (&Kun1Grid, 2, 5, 1, 1); 
	CtrlGrid.Add (c_Kun1Grid);
	CCtrlInfo *c_Kun2Grid     = new CCtrlInfo (&Kun2Grid, 5, 5, 1, 1); 
	CtrlGrid.Add (c_Kun2Grid);
//Kundenbranche von-bis
	CCtrlInfo *c_StaticKunbran2     = new CCtrlInfo (&m_StaticKunbran2, 1, 6, 1, 1); 
	CtrlGrid.Add (c_StaticKunbran2);
	CCtrlInfo *c_Kunbran2_von     = new CCtrlInfo (&m_Kunbran2_von, 2, 6, 2, 1); 
	CtrlGrid.Add (c_Kunbran2_von);
	CCtrlInfo *c_Kunbran2_bis     = new CCtrlInfo (&m_Kunbran2_bis, 5, 6, 2, 1); 
	CtrlGrid.Add (c_Kunbran2_bis);
//Datum von-bis
	CCtrlInfo *c_StaticDatum     = new CCtrlInfo (&m_StaticDatum, 1, 7, 1, 1); 
	CtrlGrid.Add (c_StaticDatum);
	CCtrlInfo *c_Datum_von     = new CCtrlInfo (&m_Datum_von, 2, 7, 3, 1); 
	CtrlGrid.Add (c_Datum_von);
	CCtrlInfo *c_Datum_bis     = new CCtrlInfo (&m_Datum_bis, 5, 7, 3, 1); 
	CtrlGrid.Add (c_Datum_bis);
//Rechnungsstatus
	CCtrlInfo *c_StaticStatus     = new CCtrlInfo (&m_StaticStatus, 1, 9, 1, 1); 
	CtrlGrid.Add (c_StaticStatus);
	CCtrlInfo *c_Status     = new CCtrlInfo (&m_Status, 2, 9, 2, 1); 
	CtrlGrid.Add (c_Status);
//Rechnungsarten
	CCtrlInfo *c_Rechart     = new CCtrlInfo (&RechartGrid, 2, 11, 5, 1); 
	CtrlGrid.Add (c_Rechart);
//Fakturierungskennzeichen
	CCtrlInfo *c_Fak     = new CCtrlInfo (&FakGrid, 2, 14, 5, 1); 
	CtrlGrid.Add (c_Fak);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();

	Mdn.opendbase (_T("bws"));

	CUtil::GetPersName (PersName);
     
//ZUTUN	ReadCfg ();
	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen
    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnBez,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Kun_von,EDIT,        (long *) &dkun_von, VLONG));
    Form.Add (new CFormField (&m_Kun_bis,EDIT,        (long *) &dkun_bis, VLONG));
	Form.Add (new CUniFormField (&m_Kunbran2_von,COMBOBOX, (char *) dkun_bran2_von, VCHAR));
	Form.Add (new CUniFormField (&m_Kunbran2_bis,COMBOBOX, (char *) dkun_bran2_bis, VCHAR));
	Form.Add (new CUniFormField (&m_Status,COMBOBOX, (char *) dStatus, VCHAR));
	Form.Add (new CFormField (&m_Check_Rech,CHECKBOX, (char *) dCheck_Rech, VCHAR));
	Form.Add (new CFormField (&m_Check_Gut,CHECKBOX, (char *) dCheck_Gut, VCHAR));
	Form.Add (new CFormField (&m_Check_Bar,CHECKBOX, (char *) dCheck_Bar, VCHAR));
	Form.Add (new CUniFormField (&m_Formtyp,COMBOBOX, (char *) dFormtyp, VCHAR));
	Form.Add (new CFormField (&m_Fak_all,CHECKBOX, (char *) dFak_all, VCHAR));
	Form.Add (new CFormField (&m_Fak_mit,CHECKBOX, (char *) dFak_mit, VCHAR));
	Form.Add (new CFormField (&m_Fak_ohne,CHECKBOX, (char *) dFak_ohne, VCHAR));
	FillKunBran2 ();
	FillStatus ();
	FillFormtyp ();

	Mdn.mdn.mdn = 1;
	Form.Show();
	ReadMdn ();
	dkun_von = 1;
	dkun_bis = 99999999;
	strcpy(dCheck_Rech,"J");
	strcpy(dCheck_Gut,"J");
	strcpy(dCheck_Bar,"J");
	strcpy(dFak_all,"J");
	m_Datum_von.GetTime(oCurTime);
    CS_Datum_von.Format (_T("%02hd.%02hd.%04hd"), oCurTime.GetDay(), oCurTime.GetMonth(), oCurTime.GetYear());
    CS_Datum_bis.Format (_T("%02hd.%02hd.%04hd"), oCurTime.GetDay(), oCurTime.GetMonth(), oCurTime.GetYear());

	Form.Show();
	
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CMyDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CMyDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CMyDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMyDlg::OnEnChangeEditMdnvon()
{
  int di = 1;
}

BOOL CMyDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
	
//	CWnd *cWnd;
	switch (pMsg->message)
	{
		case WM_KEYDOWN :

			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				StepBack ();
				EndDialog(0);
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
//				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Print (1);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				Print (0);
				return TRUE;
/*
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
//					OnAchoice ();
				}
*/
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Kun_von)
				{
					OnKun1choice ();
					return TRUE;
				}
				if (GetFocus () == &m_Kun_bis)
				{
					OnKun2choice ();
					return TRUE;
				}
				return TRUE;
			}
	
	}
	return CDialog::PreTranslateMessage(pMsg);
}
BOOL CMyDlg::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}
	if (Control == &m_Kun_von)
	{
	}
	if (Control == &m_Kun_bis)
	{
	}
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;

	return FALSE;
}
BOOL CMyDlg::OnKeyup ()
{
	CWnd *Control = GetFocus ();
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	return FALSE;
}
BOOL CMyDlg::ReadMdn ()
{
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Mdn.dbreadfirst () == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn.mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}
void CMyDlg::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&Mdn);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}
void CMyDlg::OnKun1choice ()
{
	OnKunchoice(1);
}
void CMyDlg::OnKun2choice ()
{
	OnKunchoice(2);
}

void CMyDlg::OnKunchoice (short dort)
{
	if (ChoiceKun != NULL && !ModalChoiceKun)
	{
		ChoiceKun->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceKun == NULL)
	{
		ChoiceKun = new CChoiceKun (this);
	    ChoiceKun->IsModal = ModalChoiceKun;
		ChoiceKun->CreateDlg ();
	}

    ChoiceKun->SetDbClass (&Mdn);
	CString MdnNr;
	m_Mdn.GetWindowText (MdnNr);
	ChoiceKun->m_Mdn = _tstoi (MdnNr.GetBuffer ());
	if (ModalChoiceKun)
	{
			ChoiceKun->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceKun->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceKun->MoveWindow (&rect);
		ChoiceKun->SetFocus ();
		return;
	}
    if (ChoiceKun->GetState ())
    {
		  CKunList *abl = ChoiceKun->GetSelectedText (); 
		  if (abl == NULL) return;
		  if (dort == 1) dkun_von = abl->kun;
		  if (dort == 2) dkun_bis = abl->kun;

		  memcpy (&Kun.kun, &kun_null, sizeof (KUN));
		  memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
		  Kun.kun.mdn = abl->mdn;
		  Kun.kun.kun = abl->kun;
		  if (Kun.dbreadfirst () == 0)
		  {
			  KunAdr.adr.adr = Kun.kun.adr1;
			  KunAdr.dbreadfirst ();
		  }
		  if (dort == 1)
		  {
			m_Kun_von.SetSel (0, -1, TRUE);
			m_Kun_von.SetFocus ();
		  } else if (dort == 2)
		  {
			m_Kun_bis.SetSel (0, -1, TRUE);
			m_Kun_bis.SetFocus ();
		  }
		  memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  Form.Show ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CMyDlg::FillKunBran2 ()
{
	CFormField *f = Form.GetFormField (&m_Kunbran2_von);
	CFormField *f2 = Form.GetFormField (&m_Kunbran2_bis);
	short di = -1;
	if (f != NULL)
	{
		f->ComboValues.clear ();
		f2->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"kun_bran2\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			di++;
			Ptabn.dbreadfirst ();
			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		    f2->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
		f2->FillComboBox ();
		f2->SetSel (di);
		f2->Get ();
	}
}
void CMyDlg::FillStatus ()
{
	CFormField *f = Form.GetFormField (&m_Status);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"rech_stat\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}
void CMyDlg::FillFormtyp ()
{
      TCHAR form_nr[11];
      TCHAR form_ube [129];

	CFormField *f = Form.GetFormField (&m_Formtyp);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) form_nr, SQLCHAR, sizeof (form_nr)); 
		Ptabn.sqlout ((char *) form_ube, SQLCHAR, sizeof (form_ube)); 
        int cursor = Ptabn.sqlcursor (_T("select form_nr,form_ube from nform_hea ")
			                          _T("where lila = 0 and form_nr matches \"562*\" ")
									  _T("order by form_nr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) form_nr;
			CDbUniCode::DbToUniCode (form_nr, pos);
			pos = (LPSTR) form_ube;
			CDbUniCode::DbToUniCode (form_ube, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), form_nr,
				                             form_ube);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CMyDlg::OnCbnSelchangeKunbran2bis2()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CMyDlg::OnNMKillfocusDatetimepicker1(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;
	m_Datum_von.GetTime(oCurTime);
    CS_Datum_von.Format (_T("%02hd.%02hd.%04hd"), oCurTime.GetDay(), oCurTime.GetMonth(), oCurTime.GetYear());
}

void CMyDlg::OnNMKillfocusDatetimepicker2(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;
	m_Datum_bis.GetTime(oCurTime);
    CS_Datum_bis.Format (_T("%02hd.%02hd.%04hd"), oCurTime.GetDay(), oCurTime.GetMonth(), oCurTime.GetYear());
}

BOOL CMyDlg::Print (BOOL flgDrkWahl)
{
	CProcess print;
	Form.Get ();
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;

    if (strcmp(dCheck_Rech,"N") == 0 && strcmp(dCheck_Gut,"N") == 0 && strcmp(dCheck_Bar,"N") == 0 ) 
	{
		MessageBox (_T("Bitte eine Rechnungsart w�hlen ! "), NULL, 
			MB_OK | MB_ICONWARNING);
		return FALSE;
	}
	if (tmp != NULL)
	{
		dName.Format ("%s\\%s.llf", tmp,dFormtyp);
	}
	else
	{
		dName.Format ("%s.llf", dFormtyp);
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME %s\n",dFormtyp);
		if (flgDrkWahl == TRUE) fprintf (fp, "DRUCK 1\n");
		if (flgDrkWahl == FALSE) fprintf (fp, "DRUCK 0\n");
		
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "kun %ld %ld\n",dkun_von,dkun_bis);
		fprintf (fp, "kun_bran2 %s %s\n",dkun_bran2_von,dkun_bran2_bis);
		fprintf (fp, "rech_dat %s %s\n",CS_Datum_von.GetBuffer(),CS_Datum_bis.GetBuffer());
		if (atoi(dStatus) == 0)
		{
			fprintf (fp, "rech_stat %s %s\n","0","9");
		} else
		{
			fprintf (fp, "rech_stat %s %s\n",dStatus,dStatus);
		}
		if (strcmp(dFak_mit,"J")== 0 ) 
		{
			fprintf (fp, "fak_kz %s %s\n","J","J");
		} else if (strcmp(dFak_ohne,"J")== 0 ) 
		{
			fprintf (fp, "fak_kz %s %s\n","N","N");
		} ;

		if (strcmp(dCheck_Rech,"J")== 0  && strcmp(dCheck_Gut,"N")== 0  && strcmp(dCheck_Bar,"N")== 0 ) 
		{
			fprintf (fp, "blg_typ %s %s\n","R","R");
		} else if (strcmp(dCheck_Rech,"N")== 0  && strcmp(dCheck_Gut,"J")== 0  && strcmp(dCheck_Bar,"N")== 0 ) 
		{
			fprintf (fp, "blg_typ %s %s\n","G","G");
		} else if (strcmp(dCheck_Rech,"N")== 0  && strcmp(dCheck_Gut,"N")== 0  && strcmp(dCheck_Bar,"J")== 0 ) 
		{
			fprintf (fp, "blg_typ %s %s\n","B","B");
		} else if (strcmp(dCheck_Rech,"J")== 0  && strcmp(dCheck_Gut,"J")== 0  && strcmp(dCheck_Bar,"N")== 0 ) 
		{
			fprintf (fp, "blg_typ %s %s\n","G","R");
		} else if (strcmp(dCheck_Rech,"N")== 0  && strcmp(dCheck_Gut,"J")== 0  && strcmp(dCheck_Bar,"J")== 0 ) 
		{
			fprintf (fp, "blg_typ %s %s\n","B","G");
		} else if (strcmp(dCheck_Rech,"J")== 0  && strcmp(dCheck_Gut,"J")== 0  && strcmp(dCheck_Bar,"J")== 0 ) 
		{
			fprintf (fp, "blg_typ %s %s\n","B","R");
		} ;
		fclose (fp);
		Command.Format ("dr70001 -name %s -datei %s",dFormtyp, dName.GetBuffer ());
	}
	else
	{
		Command.Format ("dr70001 -name %s",dFormtyp);
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CMyDlg::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Print(FALSE);
}
void CMyDlg::OnBnClickedOk2()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Print(TRUE);
}

