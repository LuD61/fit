#pragma once
#include "vector.h"

class CControls :
	public CVector
{
public:
	CControls(void);
	~CControls(void);
	void SetVisible (BOOL v);
	void Enable (BOOL b);
};
