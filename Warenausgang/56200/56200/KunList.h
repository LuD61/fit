#ifndef _KUN_LIST_DEF
#define _KUN_LIST_DEF
#pragma once

class CKunList
{
public:
	short mdn;
	long kun;
	CString adr_krz;
	CKunList(void);
	CKunList(short, long, LPTSTR);
	~CKunList(void);
};
#endif
