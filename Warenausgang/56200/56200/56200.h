// 56200.h : Hauptheaderdatei f�r die 56200-Anwendung
//

#pragma once

#ifndef __AFXWIN_H__
	#error 'stdafx.h' muss vor dieser Datei in PCH eingeschlossen werden.
#endif

#include "resource.h"		// Hauptsymbole


// CMyApp:
// Siehe 56200.cpp f�r die Implementierung dieser Klasse
//

class CMyApp : public CWinApp
{
public:
	CMyApp();

// �berschreibungen
	public:
	virtual BOOL InitInstance();

// Implementierung

	DECLARE_MESSAGE_MAP()
};

extern CMyApp theApp;
