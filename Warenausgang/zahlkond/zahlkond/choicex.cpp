// ChoiceX.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "ChoiceX.h"
#include "MatchCmp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CChoice 

DLGTEMPLATE CChoiceX::DlgHeader = {
    WS_THICKFRAME | WS_POPUP | WS_VISIBLE | WS_CAPTION | 
    DS_SETFONT |
    WS_SYSMENU, 
    NULL,
    6,
    0, 0, 276, 226
};


DLGITEMTEMPLATE CChoiceX::SearchEdit  =
{ 
    ES_AUTOHSCROLL | WS_VISIBLE | WS_TABSTOP, 
    WS_EX_CLIENTEDGE, 
    70,10,135,12,
    IDC_SEARCH
}; 

DLGITEMTEMPLATE CChoiceX::Enter =
{
//    BS_PUSHBUTTON | WS_VISIBLE | WS_TABSTOP,
    WS_VISIBLE | WS_TABSTOP | SS_BLACKRECT | SS_OWNERDRAW | SS_NOTIFY,
    NULL,
    70,25,40,14,
    IDC_ENTER,
};

DLGITEMTEMPLATE CChoiceX::SearchLabel  =
{ 
    WS_VISIBLE,
    NULL, 
    20,10,50,10, 
    IDC_STATIC
}; 

DLGITEMTEMPLATE CChoiceX::SearchList =
{
    WS_VISIBLE | WS_TABSTOP,
    WS_EX_CLIENTEDGE, 
//    20,30,234,160,
    20,50,234,130,
    IDC_CHOICE,
};
 
DLGITEMTEMPLATE CChoiceX::SearchOK =
{
    BS_DEFPUSHBUTTON | WS_VISIBLE | WS_TABSTOP,
    NULL,
    79,205,50,14,
    IDOK,
};
 
DLGITEMTEMPLATE CChoiceX::SearchCancel =
{
    BS_PUSHBUTTON | WS_VISIBLE | WS_TABSTOP,
    NULL,
    145,205,50,14,
    IDCANCEL,
};
 
DLGITEMTEMPLATE CChoiceX::ListType =
{
//    BS_OWNERDRAW | BS_ICON | WS_VISIBLE,
    BS_OWNERDRAW | BS_ICON,
    NULL,
    215,10,20,15,
    IDC_LISTTYPE,
};

//    CONTROL         "",IDC_LISTTYPE,"Button",BS_OWNERDRAW | BS_ICON | 
//                       WS_TABSTOP,215,10,20,15,WS_EX_TRANSPARENT

int CChoiceX::CmpRow = 0;
CImageList CChoiceX::HeaderImageList;
BOOL CChoiceX::ImageListCreated = FALSE;

void CChoiceX::SetDbClass (DB_CLASS *DbClass)
{
    this->DbClass = DbClass;
}


CChoiceX::CChoiceX(CWnd* pParent /*=NULL*/)
	: CDialog()
{
	//{{AFX_DATA_INIT(CChoice)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
    this->pParent = pParent;
    SetDlgSize (276, 226);
    SetDlgStyle (WS_THICKFRAME | WS_CAPTION | WS_SYSMENU); 
     pDlgTemplate = NULL;
	 IsModal = TRUE;
	 SelectedRow = NULL;
	 SelectedItem = -1;
	 CloseTxt = "Schlie�en";
	 SearchText = "";
	 SearchPos = 0;
 	 FocusBack = FALSE;
	 InSort = FALSE;
	 SingleSelection = TRUE;
	 IdArrDown = 0;
	 IdArrUp = 0;
	 IdArrNo = 0;
	 DlgBkColor = NULL;
	 DlgBrush = NULL;
//	 BeanLoaded = FALSE;
	 HideOK = FALSE;
	 HideEnter = TRUE;
}

void CChoiceX::SetDlgSize (int cx, int cy)
{
    DlgHeader.cx = cx;
    DlgHeader.cy = cy;
}

void CChoiceX::SetDlgStyle (DWORD style)
{
    DlgHeader.style = WS_POPUP | WS_VISIBLE | DS_SETFONT | style;
}

DWORD CChoiceX::GetDlgStyle (void)
{
    return DlgHeader.style;
}

void CChoiceX::CreateDlg (int cx, int cy)
{
    SetDlgSize (cx, cy);
    CreateDlg ();
}

void CChoiceX::CreateDlg (void)
{
    State = FALSE;
    pDlgTemplate = NULL;
    DlgMenu  = NULL;
    DlgClass = NULL;
    DlgCaption = NULL;
    DlgItempos = NULL;
    int size = 0x1000;

    pDlgTemplate = new BYTE [size];
    if (pDlgTemplate == NULL)
    {
        return;
    }
    BYTE *pos = (BYTE *) pDlgTemplate;
    memcpy (pDlgTemplate, &DlgHeader, sizeof (DlgHeader));
    pos += sizeof (DlgHeader);
    if (((size_t) pos % 2) != 0)
    {
        pos += 1;
    }

    pos = (BYTE *) AddHeader ((WORD *) pos, (LPSTR) &DlgMenu);
    pos = (BYTE *) AddHeader ((WORD *) pos, (LPSTR) &DlgClass);
//    pos = (BYTE *) AddHeader ((WORD *) pos, (LPSTR) &DlgCaption);
    pos = (BYTE *) AddHeader ((WORD *) pos, "Auswahl");
    pos = (BYTE *) AddFont ((WORD *) pos, (WORD) 8, "MS Sans Serif");
    DlgItempos = pos;
    AddItem (&SearchEdit,   DLGEDIT, "");
    AddItem (&SearchLabel,  DLGSTATIC, "Suchen");
    AddItem (&Enter,        "Static", "Bearbeiten");
	m_Enter.nID = IDC_ENTER;
//    AddItem (&Enter,        DLGBUTTON, "Bearbeiten");
    AddItem (&SearchList,  "SysListView32", "");
    AddItem (&SearchOK,     DLGBUTTON, "OK");
    AddItem (&SearchCancel, DLGBUTTON, CloseTxt);
    AddItem (&ListType,     "Button",  "");

	if (IsModal)
	{
		if (InitModalIndirect (pDlgTemplate, pParent) == FALSE)
		{
           delete []pDlgTemplate;
           pDlgTemplate = NULL;
		}
	}
	else
	{
		if (CreateIndirect (pDlgTemplate, pParent) == FALSE)
		{
           delete []pDlgTemplate;
           pDlgTemplate = NULL;
		}
		if (HideOK)
		{
			CButton *OK = (CButton *) GetDlgItem (IDOK);
			OK->ShowWindow (SW_HIDE);
		}

		if (HideEnter)
		{
			CButton *b = (CButton *) GetDlgItem (IDC_ENTER);
			b->ShowWindow (SW_HIDE);
		}

	}

/*
	if (!IsModal)
	{
		SetHeaderImageList ();
	}
*/
/*
	CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
	ListBox->DragAcceptFiles (TRUE);
*/
}

CChoiceX::~CChoiceX ()
{
	if (!IsModal)
	{
		Save ();
	}
    if (pDlgTemplate != NULL)
    {
        delete []pDlgTemplate;
    }
	if (DlgBrush != NULL)
	{
		DeleteObject (DlgBrush);		
	}
}

WORD *CChoiceX::AddHeader (WORD *pos, LPSTR HeaderItem)
{
    WORD headerWord;
    LPWSTR NewItem;

    memcpy ((LPSTR) &headerWord, HeaderItem, sizeof (WORD));
    if (headerWord == NULL)
    {
            memcpy (pos, (BYTE *) &headerWord, sizeof (WORD));
            pos ++;
    }
    else if (headerWord == 0xFFFF)
    {
            memcpy (pos++, (BYTE *) &headerWord, sizeof (WORD));
            HeaderItem += sizeof (WORD);
            memcpy ((LPSTR) &headerWord, HeaderItem, sizeof (WORD));
            memcpy (pos++, (BYTE *) &headerWord, sizeof (WORD));
            pos ++;
    }
    else
    {
            int bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, HeaderItem, -1, NULL, 0);
            NewItem   = new WCHAR [bytes];
            bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, HeaderItem, -1, NewItem, bytes);
            int len = bytes * 2;
            memcpy (pos, (LPSTR) NewItem, len);
            delete NewItem;
            pos += bytes;
    }
    return pos;
}

WORD *CChoiceX::AddFont (WORD *pos, WORD Size, LPSTR Name)
{
    LPWSTR WideName;

    memcpy (pos, (BYTE *) &Size, sizeof (WORD));
    pos ++;
    int bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Name, -1, NULL, 0);
    WideName  = new WCHAR [bytes];
    bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Name, -1, WideName, bytes);
    int len = bytes * 2;
    memcpy (pos, (LPSTR) WideName, len);
    delete WideName;
    pos += bytes;
    return pos;
}

void CChoiceX::SetHeaderImageList ()
{
	if (IdArrDown != 0 && IdArrUp != 0 && IdArrNo != 0)
	{
	    if (!ImageListCreated)
		{
			HeaderImageList.Create(16, 16, ILC_MASK, 0, 4);
			HeaderImageList.Add(LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE (IdArrUp)));
			HeaderImageList.Add(LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE (IdArrDown)));
			HeaderImageList.Add(LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE (IdArrNo)));
			ImageListCreated = TRUE;
		}
		CListCtrl *listView = GetListView ();
		CHeaderCtrl *m_pHdrCtrl = listView->GetHeaderCtrl ();
		m_pHdrCtrl->SetImageList(&HeaderImageList);
	}
}

void CChoiceX::SetItemSort (int row, int image)
{
   if (IdArrDown != 0 && IdArrUp != 0 && IdArrNo != 0)
   {

	HDITEM    curItem;

	CListCtrl *listView = GetListView ();
	CHeaderCtrl *m_pHdrCtrl = listView->GetHeaderCtrl ();

	curItem.mask=  HDI_FORMAT;
	m_pHdrCtrl->GetItem(row, &curItem);
	curItem.mask= HDI_IMAGE | HDI_FORMAT;
	curItem.iImage= image;
	curItem.fmt |= HDF_IMAGE | HDF_STRING;
	m_pHdrCtrl->SetItem(row, &curItem);
   }
}

BOOL CChoiceX::AddItem (DLGITEMTEMPLATE *DlgItem, WORD CtrClass, LPCSTR Value)
{
    LPWSTR NewClass;

    while (((size_t) DlgItempos % 4) != 0)
    {
        DlgItempos += 1;
    }
    memcpy (DlgItempos, DlgItem, sizeof (DLGITEMTEMPLATE));
    DlgItempos += sizeof (DLGITEMTEMPLATE);

    if (((size_t) DlgItempos % 2) != 0)
    {
        DlgItempos += 1;
    }

    WORD CtrFlag = 0xFFFF;
    memcpy (DlgItempos, &CtrFlag, sizeof (WORD));
    DlgItempos += sizeof (WORD);
    memcpy (DlgItempos, &CtrClass, sizeof (WORD));
    DlgItempos += sizeof (WORD);

    int bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Value, -1, NULL, 0);
    NewClass = new WCHAR [bytes];
    bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Value, -1, NewClass, bytes);
    int len = bytes * 2;
    memcpy (DlgItempos, (LPSTR) NewClass, len);
    delete NewClass;
    DlgItempos += len;

    while (((size_t) DlgItempos % 2) != 0)
    {
        DlgItempos += 1;
    }
    WORD Creation = 0;
    memcpy (DlgItempos, (LPSTR) &Creation, sizeof (WORD));
    DlgItempos += sizeof (Creation);
    return TRUE;
}

BOOL CChoiceX::AddItem (DLGITEMTEMPLATE *DlgItem, WORD CtrClass, CString& cValue)
{
    LPWSTR NewClass;
	LPSTR Value;

	int wbytes = cValue.GetLength ();
	Value = new char [wbytes + 1];
	memset (Value, ' ', wbytes);
	Value[wbytes] = 0;
#ifdef _UNICODE
    wbytes = WideCharToMultiByte(CP_ACP,WC_COMPOSITECHECK, cValue.GetBuffer (), -1, Value, wbytes, NULL, NULL);
#else
	strcpy (Value, cValue.GetBuffer ());
#endif

    while (((size_t) DlgItempos % 4) != 0)
    {
        DlgItempos += 1;
    }
    memcpy (DlgItempos, DlgItem, sizeof (DLGITEMTEMPLATE));
    DlgItempos += sizeof (DLGITEMTEMPLATE);

    if (((size_t) DlgItempos % 2) != 0)
    {
        DlgItempos += 1;
    }

    WORD CtrFlag = 0xFFFF;
    memcpy (DlgItempos, &CtrFlag, sizeof (WORD));
    DlgItempos += sizeof (WORD);
    memcpy (DlgItempos, &CtrClass, sizeof (WORD));
    DlgItempos += sizeof (WORD);

    int bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Value, -1, NULL, 0);
    NewClass = new WCHAR [bytes];
    bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Value, -1, NewClass, bytes);
    int len = bytes * 2;
    memcpy (DlgItempos, (LPSTR) NewClass, len);
    delete NewClass;
	delete Value;
    DlgItempos += len;

    while (((size_t) DlgItempos % 2) != 0)
    {
        DlgItempos += 1;
    }
    WORD Creation = 0;
    memcpy (DlgItempos, (LPSTR) &Creation, sizeof (WORD));
    DlgItempos += sizeof (Creation);
    return TRUE;
}

BOOL CChoiceX::AddItem (DLGITEMTEMPLATE *DlgItem, LPCSTR CtrClass, LPCSTR Value)
{
    LPWSTR NewClass;

    while (((size_t) DlgItempos % 4) != 0)
    {
        DlgItempos += 1;
    }

    memcpy (DlgItempos, DlgItem, sizeof (DLGITEMTEMPLATE));
    DlgItempos += sizeof (DLGITEMTEMPLATE);

    if (((size_t) DlgItempos % 2) != 0)
    {
        DlgItempos += 1;
    }

    int bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, CtrClass, -1, NULL, 0);
    NewClass = new WCHAR [bytes];
    bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, CtrClass, -1, NewClass, bytes);
    int len = bytes * 2;
    memcpy ((LPSTR) DlgItempos, (LPSTR) NewClass, len);
    delete NewClass;
    DlgItempos += len;

    bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Value, -1, NULL, 0);
    NewClass = new WCHAR [bytes];
    bytes = MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, Value, -1, NewClass, bytes);
    len = bytes * 2;
    memcpy ((LPSTR) DlgItempos, (LPSTR) NewClass, len);
    delete NewClass;
    DlgItempos += len;

    while (((size_t) DlgItempos % 2) != 0)
    {
        DlgItempos += 1;
    }
    WORD Creation = 0;
    memcpy (DlgItempos, (LPSTR) &Creation, sizeof (WORD));
    DlgItempos += sizeof (Creation);
    return TRUE;
}


BOOL CChoiceX::DlgOK (void)
{
    if (pDlgTemplate == NULL)
    {
        return FALSE;
    }
    return TRUE;
}

void CChoiceX::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChoice)
	DDX_Control(pDX, IDC_ENTER, m_Enter);
	DDX_Control(pDX, IDC_CHOICE, m_List);
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChoiceX, CDialog)
	//{{AFX_MSG_MAP(CChoiceX)
	ON_WM_CTLCOLOR ()
	ON_WM_DESTROY()
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_CHOICE, OnColumnclickChoice)
	ON_NOTIFY(NM_DBLCLK, IDC_CHOICE, OnDblclkChoice)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_CHOICE, OnItemChanged)
	ON_WM_DRAWITEM()
//	ON_BN_CLICKED(IDC_LISTTYPE, OnListtype)
	ON_EN_CHANGE(IDC_SEARCH, OnChangeSearch)
	ON_WM_SIZE()
	ON_NOTIFY(NM_SETFOCUS, IDC_CHOICE, OnNMSetfocusList)
	ON_NOTIFY(LVN_BEGINRDRAG, IDC_CHOICE, OnLvnBeginrdragList)
//	ON_COMMAND(IDC_ENTER , OnEnter)
	ON_BN_CLICKED(IDC_ENTER , OnEnter)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CChoice 

BOOL CChoiceX::OnInitDialog() 
{
    CRect Rect;
    HICON hiconItem;     // icon for list-view items 

	CDialog::OnInitDialog();

	MoveCtrl ();

	// TODO: Zus�tzliche Initialisierung hier einf�gen
	
    hLarge = ImageList_Create(GetSystemMetrics(SM_CXICON), 
        GetSystemMetrics(SM_CYICON), ILC_MASK, 1, 1); 
    hSmall = ImageList_Create(GetSystemMetrics(SM_CXSMICON), 
        GetSystemMetrics(SM_CYSMICON), ILC_MASK, 1, 1); 
 
    hiconItem = LoadIcon(AfxGetApp()->m_hInstance, 
		                 MAKEINTRESOURCE(IDI_ICON2)); 
    ImageList_AddIcon(hLarge, hiconItem); 
    ImageList_AddIcon(hSmall, hiconItem); 
    DestroyIcon(hiconItem); 

    CListCtrl *listView = GetListView ();
	DWORD state = LVS_SHOWSELALWAYS;
	if (SingleSelection)
	{
//		SetListStyle (LVS_SINGLESEL | LVS_SHOWSELALWAYS); 
		state |= LVS_SINGLESEL;
	}
	SetListStyle (state); 

    HWND hWndListView = listView->m_hWnd;

    ListView_SetImageList(hWndListView, hLarge, LVSIL_NORMAL); 
    ListView_SetImageList(hWndListView, hSmall, LVSIL_SMALL); 

    FillList ();
	SetHeaderImageList ();
	if (SearchText != "")
	{
		CEdit *Edit = (CEdit *) GetDlgItem (IDC_SEARCH);
		if (Edit != NULL)
		{
			Edit->SetWindowText (SearchText);
		}
		int eLen = SearchText.GetLength ();
		Edit->SetFocus ();
		Edit->SetSel (eLen, eLen);
        return FALSE;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

HBRUSH CChoiceX::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CChoiceX::PreTranslateMessage(MSG* pMsg)
{

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
			if (pMsg->wParam == VK_F3)
			{
				NextMatch ();
				return TRUE;
			}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


void CChoiceX::MoveCtrl (void)
{
    CRect Rect;

    GetClientRect (&Rect);
    MoveCtrl (Rect.right, Rect.bottom);
}

void CChoiceX::MoveCtrl (int mcx, int mcy)
{
    CRect Rect;

    GetClientRect (&Rect);
    int y = GetSystemMetrics (SM_CYCAPTION); 

    Rect.left   = 0;
    Rect.top    = 0;
    Rect.right  = mcx;
    Rect.bottom = mcy;

    int cx = Rect.right - 40;
    int cy = Rect.bottom - (70 * 5 / 3);
    CListCtrl *listView = GetListView ();
	if (HideEnter)
	{
		listView->MoveWindow (20, y + 30, cx, cy, TRUE);
	}
	else
	{
		listView->MoveWindow (20, y + 50, cx, cy, TRUE);
	}
	CWnd *cOK = GetDlgItem (IDOK);
	if (IsModal || !HideOK)
	{
		int oky   = Rect.bottom - 19 * 5 / 3;
		int cay   = Rect.bottom - 19 * 5 / 3;
		int okcx  = 50 * 5 / 3;
		int cacx  = 50 * 5 / 3;
		int okcy  = 14 * 5 / 3;
		int cacy  = 14 * 5 / 3;

	    int buspace = 16 * 5 / 3;
		int Panellen = okcx + cacx + buspace;

	    int okx   = (Rect.right - Panellen) / 2;
	
		int cax   = okx + okcx + buspace;

		cOK->MoveWindow (okx, oky, okcx, okcy, TRUE);

		CWnd *cCancel = GetDlgItem (IDCANCEL);
		cCancel->MoveWindow (cax, cay, cacx, cacy, TRUE);
		Rect.top = oky - 2;
	}
	else
	{
		CRect cRect;
		CRect clRect;
		CWnd *cCancel = GetDlgItem (IDCANCEL);
		cCancel->GetClientRect (&clRect);
		cCancel->GetWindowRect (&cRect);
		ScreenToClient (&cRect);
		GetClientRect (&Rect);
		int x = max (0, (Rect.right  - clRect.right) / 2);
		int cx = cRect.right - cRect.left;
		int cy = cRect.bottom - cRect.top;
		int y = Rect.bottom - cy - 10;
		cCancel->MoveWindow (x, y, cx, cy);
	}
	InvalidateRect (&Rect, TRUE);
}

void CChoiceX::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen
	
    if (GetDlgItem (IDC_CHOICE) != NULL)
    {
           MoveCtrl (cx, cy);
    }
}

DWORD CChoiceX::SetStyle (DWORD st)
{
    CListCtrl *listView = (CListCtrl *) GetDlgItem (IDC_CHOICE);

    DWORD Style = GetWindowLong (listView->m_hWnd, GWL_STYLE);

    Style &= ~(LVS_ICON | LVS_SMALLICON | LVS_LIST | LVS_REPORT);
    SetWindowLong (listView->m_hWnd, GWL_STYLE, Style |= LVS_REPORT); 
    Style = st;
    ListStyle = st;
    return Style;
}

DWORD CChoiceX::SetListStyle (DWORD st)
{

    CListCtrl *listView = (CListCtrl *) GetDlgItem (IDC_CHOICE);

    DWORD Style = GetWindowLong (listView->m_hWnd, GWL_STYLE);
    Style |= st;
    SetWindowLong (listView->m_hWnd, GWL_STYLE, Style); 
    return Style;
}

DWORD CChoiceX::SetExtendedStyle (DWORD st)
{

    CListCtrl *listView = (CListCtrl *) GetDlgItem (IDC_CHOICE);

    DWORD style = listView->GetExtendedStyle ();
    style |= st;
    listView->SetExtendedStyle (style);
    return style;
}



CListCtrl *CChoiceX::GetListView (void)
{
    CListCtrl *listView = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    return listView;
}

BOOL CChoiceX::SetCol (LPTSTR Txt, int idx, int Width)
{  
   LV_COLUMN Col;

   CListCtrl *listView = GetListView ();
   HWND hWndListView = listView->m_hWnd;

   Col.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;   
   Col.fmt = LVCFMT_LEFT;
   Col.cx  = Width;
   Col.pszText = Txt;
   Col.iSubItem = idx;
   return ListView_InsertColumn(hWndListView, idx, &Col); 
}

BOOL CChoiceX::SetCol (LPTSTR Txt, int idx, int Width, int Align)
{  
   LV_COLUMN Col;

   CListCtrl *listView = GetListView ();
   HWND hWndListView = listView->m_hWnd;

   Col.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;   
   Col.fmt = Align;
   Col.cx  = Width;
   Col.pszText = Txt;
   Col.iSubItem = idx;
   return ListView_InsertColumn(hWndListView, idx, &Col); 
}

BOOL CChoiceX::SetColFmt (int Column, int Align)
{  
   LV_COLUMN Col;

   CListCtrl *listView = GetListView ();
   HWND hWndListView = listView->m_hWnd;

   Col.mask = LVCF_FMT | LVCF_SUBITEM;   
   Col.fmt = Align;
   Col.iSubItem = Column;
   return listView->SetColumn (Column, &Col);
}


BOOL CChoiceX::InsertItem (int idx)
{
   LV_ITEM Item;

   CListCtrl *listView = GetListView ();

   Item.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
   Item.state      = 0; 
   Item.stateMask  = 0; 
   Item.iImage     = 0;
   Item.iItem      = idx;
   Item.iSubItem   = 0;
   Item.pszText    = NULL;
   Item.lParam     = (LPARAM) idx;
   return listView->InsertItem (&Item);
}

BOOL CChoiceX::InsertItem (int idx, int image)
{
   LV_ITEM Item;

   CListCtrl *listView = GetListView ();

   Item.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE; 
   Item.state      = 0; 
   Item.stateMask  = 0; 
   Item.iImage     = image;
   Item.iItem      = idx;
   Item.iSubItem   = 0;
   Item.pszText    = NULL;
   Item.lParam     = (LPARAM) idx;
   return listView->InsertItem (&Item);
}

BOOL CChoiceX::SetItemText (LPTSTR Txt, int idx, int pos)
{
   CListCtrl *listView = GetListView ();

   return listView->SetItemText (idx, pos, Txt);
}


void CChoiceX::FillList () 
{
}

void CChoiceX::RefreshList () 
{
}

void CChoiceX::ClearList ()
{
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
	if (ListBox != NULL)
	{
		ListBox->DeleteAllItems ();
	}
	CHeaderCtrl *Header = ListBox->GetHeaderCtrl ();
	if (Header != NULL)
	{
		int iCount = Header->GetItemCount ();
		for (int i = 0; i < iCount; i ++)
		{
			ListBox->DeleteColumn (0);
		}
	}
}

void CChoiceX::OnColumnclickChoice (NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
	SortRow = pNMListView->iSubItem;
	InSort = TRUE;
    Sort (ListBox);
	InSort = FALSE;
	*pResult = 0;
}


void CChoiceX::Sort (CListCtrl *ListBox)
{
}

void CChoiceX::SetSelText (CListCtrl *ListBox, int idx)
{
}

void CChoiceX::SaveSelection (CListCtrl *ListBox)
{
}

void CChoiceX::FirstSelection ()
{
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);

	ListBox->SetItemState (0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
}

void CChoiceX::DecSelection ()
{
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);

    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	if (idx > 0)
	{
		ListBox->SetItemState (idx - 1, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
	}
}

void CChoiceX::IncSelection ()
{
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);

    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	if (idx < ListBox->GetItemCount () - 1)
	{
		ListBox->SetItemState (idx + 1, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
	}
}

void CChoiceX::LastSelection ()
{
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);

    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	if (ListBox->GetItemCount ()  > 0)
	{
		ListBox->SetItemState (ListBox->GetItemCount () - 1, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
	}
}

void CChoiceX::OnDblclkChoice (NMHDR* pNMHDR, LRESULT* pResult) 
{

	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);

    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
        return;
    }
    State = TRUE;
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	if (IsModal)
	{
		EndDialog (0);
	}
	else
	{
		if (pParent != NULL)
		{
			FocusBack = FALSE;
			SendSelect ();
//			pParent->SendMessage (WM_COMMAND, SELECTED, 0l);
		}
	}
	
	*pResult = 0;
}

void CChoiceX::SetListFocus ()
{
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
	ListBox->SetFocus ();
}

void CChoiceX::OnItemChanged (NMHDR* pNMHDR, LRESULT* pResult) 
{

	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);

	if (InSort) return;
	if (IsModal) return;
	if (GetFocus () == GetDlgItem (IDC_SEARCH)) return;
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED | LVNI_FOCUSED);
    if (idx == -1)
    {
        return;
    }
    State = TRUE;
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	if (pParent != NULL)
	{
			FocusBack = TRUE;
			SendSelect ();
//			pParent->SendMessage (WM_COMMAND, SELECTED, 0l);
	}
	*pResult = 0;
}

void CChoiceX::OnOK() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    SelectedItem = idx;
    if (idx == -1)
    {
		EndDialog (0);
        return;
    }
    State = TRUE;
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (256));
	if (SingleSelection)
	{
		SetSelText (ListBox, idx);
	}
	else
	{
		SetSelText (ListBox, idx);
		SaveSelection (ListBox);
	}
	if (IsModal)
	{
		Save ();
		EndDialog (0);
	}
	else
	{
		if (pParent != NULL)
		{
			FocusBack = FALSE;
			SendSelect ();
//			pParent->SendMessage (WM_COMMAND, SELECTED, 0l);
		}
	}
}

void CChoiceX::OnCancel() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
    State = FALSE;
	if (IsModal)
	{
		Save ();
		EndDialog (0);
	}
	else
	{
		SendCancel ();
//		pParent->SendMessage (WM_COMMAND, CANCELED, 0l);
	}
}


void CChoiceX::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen und/oder Standard aufrufen
	
//	return;
    if (nIDCtl == IDC_LISTTYPE)
    {
        if (lpDrawItemStruct->itemAction == ODA_DRAWENTIRE)
        {
            CDC *cDC = CDC::FromHandle (lpDrawItemStruct->hDC);
            RECT rect = lpDrawItemStruct->rcItem;
            CPen Pen;
            Pen.CreatePen (PS_SOLID, 1, RGB (0,0,0));
            CPen *oldPen = cDC->SelectObject (&Pen);
//            cDC->Rectangle (&rect);
            cDC->SelectObject (oldPen);
            Pen.DeleteObject ();

/*
            CBitmap View;
            if (!View.LoadBitmap (MAKEINTRESOURCE (IDB_BITMAP2)))
			{
				return;
			}


            CDC srcDC;
            srcDC.CreateCompatibleDC (cDC);
            srcDC.SelectObject (&View);
            srcDC.SetMapMode (cDC->GetMapMode ());

            BITMAP bm;
            int bytes = View.GetObject (sizeof (BITMAP), &bm);
            POINT ptSize;
            ptSize.x = bm.bmWidth;
            ptSize.y = bm.bmHeight;
            cDC->DPtoLP (&ptSize); 

            cDC->BitBlt (1, 1, ptSize.x, ptSize.y,
                 &srcDC, 0, 0, SRCCOPY);
            srcDC.DeleteDC (); 
*/

/*
            HICON Icon = LoadIcon (AfxGetApp()->m_hInstance, MAKEINTRESOURCE (IDI_ICON2)); 
            DrawIcon (lpDrawItemStruct->hDC, rect.left + 1,
                                             rect.top  + 1,
                                             Icon); 
*/

        }
        else if (lpDrawItemStruct->itemAction == ODA_FOCUS)
        {
        }
        else if (lpDrawItemStruct->itemAction == ODA_SELECT)
        {
        }
    }
	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

BOOL CChoiceX::OnCommand(WPARAM wParam, LPARAM lParam) 
{
    CListCtrl *listView;
    DWORD Style;

	// TODO: Speziellen Code hier einf�gen und/oder Basisklasse aufrufen
	
    switch (LOWORD (wParam)) 
    {
        case IDM_LIST :
            listView = (CListCtrl *) GetDlgItem (IDC_CHOICE);
            Style = GetWindowLong (listView->m_hWnd, GWL_STYLE);
            if (ListStyle == LVS_REPORT)
            {
                    BezLabel (listView);
            }
            Style &= ~(LVS_ICON | LVS_SMALLICON | LVS_LIST | LVS_REPORT);
            SetWindowLong (listView->m_hWnd, GWL_STYLE, Style |= LVS_LIST); 
            ListStyle = LVS_ICON;
            return TRUE;
        case IDM_ICON :
            listView = (CListCtrl *) GetDlgItem (IDC_CHOICE);
            Style = GetWindowLong (listView->m_hWnd, GWL_STYLE);
            if (ListStyle == LVS_REPORT)
            {
                    BezLabel (listView);
            }
            Style &= ~(LVS_ICON | LVS_SMALLICON | LVS_LIST | LVS_REPORT);
            SetWindowLong (listView->m_hWnd, GWL_STYLE, Style |= LVS_ICON); 
            ListStyle = LVS_ICON;
            return TRUE;
        case IDM_SMALLICON :
            listView = (CListCtrl *) GetDlgItem (IDC_CHOICE);
            Style = GetWindowLong (listView->m_hWnd, GWL_STYLE);
            if (ListStyle == LVS_REPORT)
            {
                    BezLabel (listView);
            }
            Style &= ~(LVS_ICON | LVS_SMALLICON | LVS_LIST | LVS_REPORT);
            SetWindowLong (listView->m_hWnd, GWL_STYLE, Style |= LVS_SMALLICON); 
            ListStyle = LVS_ICON;
            return TRUE;
        case IDM_REPORT :
            listView = (CListCtrl *) GetDlgItem (IDC_CHOICE);
            Style = GetWindowLong (listView->m_hWnd, GWL_STYLE);
            if (ListStyle != LVS_REPORT)
            {
                    NumLabel (listView);
            }
            Style &= ~(LVS_ICON | LVS_SMALLICON | LVS_LIST | LVS_REPORT);
            SetWindowLong (listView->m_hWnd, GWL_STYLE, Style |= LVS_REPORT); 
            ListStyle = LVS_REPORT;
            return TRUE;
    }
	return CDialog::OnCommand(wParam, lParam);
}

void CChoiceX::OnListtype() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
    CMenu Menu, *popupMenu;
    CRect Rect;


	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen

    CWnd *cWnd = GetDlgItem (IDC_LISTTYPE);
    cWnd->GetWindowRect (&Rect);


    Menu.LoadMenu (IDR_VIEWTYPE);

    popupMenu = Menu.GetSubMenu (0);
    popupMenu->TrackPopupMenu (TPM_CENTERALIGN, Rect.left, Rect.bottom, (CWnd *) this, NULL);
    Menu.DestroyMenu ();
}

void CChoiceX::Search ()
{
}


void CChoiceX::SearchMatch (CString& Match)
{
	MatchFound = FALSE;
	Match.Trim ();
	if (SearchText != Match)
	{
		SearchText = Match;
		SearchPos = 0;
	}

	CMatchCmp MatchCmp;
	MatchCmp.MatchCode = &Match;
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
	int iCount = ListBox->GetItemCount ();

	int i = 0;
	for (i = SearchPos; i < iCount; i ++)
	{
		CString Item = ListBox->GetItemText (i, SortRow);
		Item.Trim ();
		if (MatchCmp.Compare (Item)) break;
	}

	if (i == iCount) 
	{
		SearchPos = 0;
		return;
	}

	MatchFound = TRUE;
	SearchPos = i + 1;
	if (i >= iCount - 1) SearchPos = 0;
    ScrolltoIdx (ListBox,i);
}

void CChoiceX::NextMatch ()
{
	if (SearchText != "")
	{
		SearchMatch (SearchText);
	}
}

void CChoiceX::OnChangeSearch() 
{

	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_CHANGE-Attribut ORed in die Maske lParam zu senden.
	
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen

     Search ();
}

   
void CChoiceX::ScrolltoIdx(CListCtrl *ListBox, int i) 
{
        int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
/*
        UINT State = ListBox->GetItemState (idx, LVIS_SELECTED | LVIS_FOCUSED | LVIS_DROPHILITED);
        State &= ~(LVIS_SELECTED | LVIS_FOCUSED | LVIS_DROPHILITED);
        ListBox->SetItemState (idx, State, LVIS_SELECTED | LVIS_FOCUSED | LVIS_DROPHILITED); 
        State = LVIS_SELECTED  | LVIS_FOCUSED | LVIS_DROPHILITED;
        ListBox->SetItemState (i, State, State); 
        ListBox->EnsureVisible (i, FALSE);
*/

        UINT State = ListBox->GetItemState (idx, LVIS_SELECTED | LVIS_FOCUSED | LVIS_DROPHILITED);
        State &= ~(LVIS_SELECTED | LVIS_FOCUSED | LVIS_DROPHILITED);
        ListBox->SetItemState (idx, State, LVIS_SELECTED | LVIS_FOCUSED); 
        State = LVIS_SELECTED  | LVIS_FOCUSED;
        ListBox->SetItemState (i, State, State); 
        ListBox->EnsureVisible (i, FALSE);

}

void CChoiceX::NumLabel (CListCtrl *ListBox)
{
}


void CChoiceX::BezLabel (CListCtrl *ListBox)
{
}

void CChoiceX::OnNMSetfocusList(NMHDR *pNMHDR, LRESULT *pResult)
{
		*pResult = 0;
        CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
        int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
        ListBox->SetItemState (idx, 0, LVIS_DROPHILITED); 
}

double CChoiceX::StrToDouble (LPTSTR string)
{
 double fl;
 double ziffer;
 double teiler;
 short minus;

 if (string == (LPTSTR) 0) return (double) 0.0;
 fl = 0;
 teiler = 10;
 minus = 1;
 while (*string < 0X30)
 {
  if (*string == 0)
    return (0.0);
  if (*string == '-')
    break;
  if (*string == '+')
    break;
  if (*string == '.')
    break;
  if (*string == ',')
    break;
  string ++;
 }

 if (*string == '-')
 {
  minus = -1;
  string ++;
 }
 else if (*string == '+')
 {
  string ++;
 }

 while (*string)
 {
  if (*string == '.')
  {
   break;
  }
  if (*string == ',')
  {
   break;
  }
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = (fl * teiler) + ziffer;
  string ++;
 }

 if (*string == '.')
   ;
 else if (*string == ',')
   ;
 else
 {
  fl *= minus;
  return (fl);
 }

 string ++;
 while (*string)
 {
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = fl + (ziffer / teiler);
  teiler *= 10;
  string ++;
 }
 fl *= minus;
 return (fl);
}

double CChoiceX::StrToDouble (CString& Text)
{
	return StrToDouble (Text.GetBuffer (0));
}

void CChoiceX::OnLvnBeginrdragList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;
	DragList ();
}

void CChoiceX::DragList ()
{

    COleDataSource dataSource;
/*
    CString Path;
    HGLOBAL hglbCopy;

	int size = 0;

	CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
		return;
	}

    CString Text;
    Text = ListBox->GetItemText (idx, 0);
    strcpy (SelText, Text.GetBuffer (0));
    size = Text.GetLength ();

	hglbCopy = GlobalAlloc(GMEM_SHARE, size + 1 + sizeof (DROPFILES)); 
    LPSTR lptstrCopy = (LPSTR) GlobalLock(hglbCopy); 
	DROPFILES *dp;
	dp = (DROPFILES *) lptstrCopy;
	dp->pFiles = sizeof (DROPFILES);
	memset (&dp->pt, 0, sizeof (POINT));
	dp->fNC = FALSE;
	dp->fWide = FALSE;
	LPSTR p = (LPSTR) dp + sizeof (DROPFILES); 
	strcpy (p, Text.GetBuffer (0));
    GlobalUnlock(hglbCopy); 
	STGMEDIUM stg;
	stg.tymed = TYMED_HGLOBAL;
	stg.hGlobal = hglbCopy;
    stg.pUnkForRelease = NULL;
	HANDLE cData = NULL;
//		cData = ::SetClipboardData( CF_HDROP, hglbCopy );  
    dataSource.CacheGlobalData (CF_HDROP, hglbCopy);
*/
	DROPEFFECT effect = dataSource.DoDragDrop (DROPEFFECT_COPY,
		                                       NULL, NULL);
	switch (effect) 
	{
	case DROPEFFECT_COPY :
		OnOK ();
		break;
	case DROPEFFECT_MOVE :
		break;
	case DROPEFFECT_LINK :
		break;
	case DROPEFFECT_NONE :
		break;
	default :
		break;
	}
    dropTarget.Revoke ();
}

void CChoiceX::SetItem (int idx, BOOL scroll) 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
	ListBox->SetItemState (idx, LVIS_SELECTED | LVIS_FOCUSED, 
			                    LVIS_SELECTED | LVIS_FOCUSED);
	if (scroll)
	{
		ListBox->EnsureVisible (idx, FALSE);
	}
}

void CChoiceX::UnSetItem (int idx) 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    DWORD state = ListBox->GetItemState (idx, LVIS_SELECTED);
    state &= ~LVIS_SELECTED;
    ListBox->SetItemState (idx, state, LVIS_SELECTED);
}

void *CChoiceX::GetSelectedRow ()
{
	return SelectedRow;
}

void CChoiceX::SendSelect ()
{
	pParent->SendMessage (WM_COMMAND, SELECTED, 0l);
}

void CChoiceX::SendCancel ()
{
	pParent->SendMessage (WM_COMMAND, CANCELED, 0l);
}

void CChoiceX::OnDestroy ()
{
	if (!IsModal)
	{
		Save ();
	}
}


void CChoiceX::Save ()
{
//    if (Bean.ArchiveName == "") return;
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
//	Bean.SetListCtrl (ListBox);
//	Bean.SetSortRow (SortRow);
//	Bean.Save ();
}

void CChoiceX::Load ()
{
//	if (Bean.ArchiveName == "") return;
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
//	Bean.SetListCtrl (ListBox);
//	Bean.Load ();
//	SortRow = Bean.GetSortRow ();
}

void CChoiceX::SetDefault ()
{
//	if (Bean.ArchiveName == "") return;
//	Bean.DropArchive ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
	CHeaderCtrl *Header = ListBox->GetHeaderCtrl ();
	if (Header != NULL)
	{
		int iCount = Header->GetItemCount ();
		int *OrderArray = new int [iCount];
		for (int i = 0; i < iCount; i ++)
		{
			OrderArray[i] = i;
		}
		Header->SetOrderArray (iCount, OrderArray);
		delete OrderArray;
	}
}

void CChoiceX::SetSearchText ()
{
	CEdit *Edit = (CEdit *) GetDlgItem (IDC_SEARCH);
	if (Edit != NULL)
	{
		Edit->SetWindowText (SearchText);
	}
	int eLen = SearchText.GetLength ();
	Edit->SetFocus ();
	Edit->SetSel (eLen, eLen);
}

void CChoiceX::OnEnter ()
{
}

void CChoiceX::ToClipboard (CString& Text)
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	try
	{
/*
#ifndef UNICODE
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Text.GetLength () + 1) * 2 ); 
        wchar_t *wp = (LPWSTR) GlobalLock(hglbCopy);
        LPSTR p = Text.GetBuffer ();
		int l = (int) strlen (p);
		MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, p, -1, wp, l);
		wp[l] = 0;
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
#else
*/

		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Text.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Text.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
// #endif
	}
	catch (...) {}
    CloseClipboard();
}

void CChoiceX::FromClipboard (CString& Text)
{
    if (!OpenClipboard() )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return;
    }

    HGLOBAL hglbCopy;
    LPTSTR data;

	try
	{
		 hglbCopy =  ::GetClipboardData(CF_TEXT);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
		 data = (LPTSTR) GlobalLock ((HGLOBAL) hglbCopy);
         Text = data;
		 GlobalUnlock ((HGLOBAL) hglbCopy);
	}
	catch (...) {};
    CloseClipboard();
}
