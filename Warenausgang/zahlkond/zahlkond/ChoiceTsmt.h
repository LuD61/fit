#ifndef _CHOICETSMT_DEF
#define _CHOICETSMT_DEF

#include "ChoiceX.h"
#include "TsmtList.h"
#include "Vector.h"
#include <vector>

class CChoiceTsmt : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
      
    public :
		long m_mdn;
		TCHAR ptwert[5] ;
		TCHAR ptbezk[36] ;
		CVector *Rows;
	    std::vector<CTsmtList *> TsmtList;
      	CChoiceTsmt(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceTsmt(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchMeEinh (CListCtrl *,  LPTSTR);
        void SearchZusBz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CTsmtList *GetSelectedText ();
        int GetPtBezk (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
