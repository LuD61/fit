#pragma once
#include <vector>
#include "editlistctrl.h"
#include "dbClass.h"
//#include "a_bas.h"
// #include "ChoiceMeEinh.h"
#include "ChoiceTsmt.h"

#define MAXLISTROWS 30

class CList1Ctrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

	enum LISTPOS
	{
//		POSTSMT			= 1,
		POSZAHL_KOND	= 1,	
		POSTXT   		= 2,
		POSZIEL1		= 3,
		POSSKTO1		= 4,
		POSZIEL2		= 5,
		POSSKTO2		= 6,
		POSZIEL3		= 7,
		POSSKTO3		= 8,
	};


//	int PosTsmt;
    int PosTxt;
	int PosZiel1;
	int PosSkto1;
	int PosZiel2;
	int PosSkto2;
	int PosZiel3;
	int PosSkto3;
	int PosZahl_kond;
    int *Position[12];


	int Mode;
	int MaxComboEntries;
	BOOL ActiveListRow;
	short m_mdn;
	short mdn;
//    long schlklknr;

//	int oldsel;
	std::vector<BOOL> vSelect;

	CVector TsmtCombo ;

	CChoiceTsmt *ChoiceTsmt;
	BOOL ModalChoiceTsmt;
	BOOL TsmtChoiceStat;

	CVector ListRows;

	CList1Ctrl(void);
	~CList1Ctrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
//	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
//	void FillMeEinhCombo( CVector&);
	void FillTsmtCombo( CVector&);

	void OnChoice ();
	BOOL ReadZahl_kond ();
    void GetColValue (int row, int col, CString& Text);
	void ScrollPositions (int pos);
	BOOL LastCol ();
};
