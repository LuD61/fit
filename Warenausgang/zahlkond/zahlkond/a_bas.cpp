#include "stdafx.h"
#include "DbClass.h"
#include "a_bas.h"

extern DB_CLASS dbClass;


struct ZAHL_KOND zahl_kond,  zahl_kond_null;

struct B_ZAHL_KOND b_zahl_kond ;

static int anzzfelder ;



ZAHL_KOND_CLASS zahl_kond_class ;


int ZAHL_KOND_CLASS::deletezahl_kond (void)
{
	preparedel() ;	// immer wieder neu preparieren 	
	int di = dbClass.sqlexecute (del_cursor);
	return di;
}

int ZAHL_KOND_CLASS::insertzahl_kond (void)
{
	if ( readcursor < 0 ) prepare ();	
	int di = dbClass.sqlexecute (ins_cursor);
	return di;
}

int ZAHL_KOND_CLASS::updzahl_kond (void)
{
	if ( readcursor < 0 ) prepare ();	
	int di = dbClass.sqlexecute (upd_cursor);
	return di;
}


int ZAHL_KOND_CLASS::leseallzahl_kond (void)
{
      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int ZAHL_KOND_CLASS::lesezahl_kond (void){
      int di = dbClass.sqlfetch (test_upd_cursor);
	  return di;
}


int ZAHL_KOND_CLASS::openallzahl_kond (void)
{

		if ( readcursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (readcursor);
}


int ZAHL_KOND_CLASS::openzahl_kond (void)
{

		if ( test_upd_cursor < 0 ) prepare ();	
		
         return dbClass.sqlopen (test_upd_cursor);
}



void ZAHL_KOND_CLASS::preparedel(void)
{

	char hilfehilfe[ 3300 ] ;	

	if ( del_cursor > -1 )
		dbClass.sqlclose ( del_cursor ) ;
/* ---->
	sprintf ( hilfehilfe ," delete from zahl_kond where  "
		" zahl_kond not in (  %s ) and zahl_kond between %1d and %1d " ,b_zahl_kond.intsmtzahlko, b_zahl_kond.vonwert, b_zahl_kond.biswert ) ;
funzt halt nur f�r cirka 600 Konditionen ( bei 4-stellig )
		< ----- */

	sprintf ( hilfehilfe ," delete from zahl_kond where  "
		" zahl_kond not in (  %s ) " ,b_zahl_kond.intsmtzahlko ) ;


	del_cursor = (short) dbClass.sqlcursor ( hilfehilfe ) ;
}


void ZAHL_KOND_CLASS::prepare (void)
{

	dbClass.sqlin (( short *)&zahl_kond.zahl_kond, SQLSHORT, 0 ) ;

//	dbClass.sqlin (( short *)&tsmtzahlko.mdn, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( long *) &tsmtzahlko.kun, SQLLONG, 0 ) ;
//	dbClass.sqlin (( char *)  tsmtzahlko.kun_bran2, SQLCHAR, 3 ) ;
//	dbClass.sqlin (( double *) &tsmtzahlko.teil_smt, SQLSHORT, 0 ) ;
	
//	dbClass.sqlout (( short *)&tsmtzahlko.mdn, SQLSHORT, 0 ) ;
//	dbClass.sqlout (( long *) &tsmtzahlko.kun, SQLLONG, 0 ) ;
//	dbClass.sqlout (( char *)  tsmtzahlko.kun_bran2, SQLCHAR, 3 ) ;
//	dbClass.sqlout (( short *) &tsmtzahlko.teil_smt, SQLSHORT, 0 ) ;
	dbClass.sqlout (( char *)  zahl_kond.txt, SQLCHAR, 61 ) ;

	dbClass.sqlout (( double *) &zahl_kond.skto1, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( double *) &zahl_kond.skto2, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( double *) &zahl_kond.skto3, SQLDOUBLE, 0 ) ;

	dbClass.sqlout (( short *) &zahl_kond.ziel1,	SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &zahl_kond.ziel2,	SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &zahl_kond.ziel3,	SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &zahl_kond.zahl_kond, SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &zahl_kond.delstatus, SQLSHORT, 0 ) ;

	test_upd_cursor = (short) dbClass.sqlcursor ("select "
	" txt "
	" ,skto1 ,skto2 ,skto3 ,ziel1 ,ziel2 ,ziel3, zahl_kond, delstatus "
	" from zahl_kond where zahl_kond = ? " ) ;

// lese komplette Liste 


//	dbClass.sqlin (( short *)&tsmtzahlko.mdn, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( long *) &tsmtzahlko.kun, SQLLONG, 0 ) ;
//	dbClass.sqlin (( char *)  tsmtzahlko.kun_bran2, SQLCHAR, 3 ) ;

//	dbClass.sqlout (( short *)&tsmtzahlko.mdn, SQLSHORT, 0 ) ;
//	dbClass.sqlout (( long *) &tsmtzahlko.kun, SQLLONG, 0 ) ;
//	dbClass.sqlout (( char *)  tsmtzahlko.kun_bran2, SQLCHAR, 3 ) ;
//	dbClass.sqlout (( short *) &tsmtzahlko.teil_smt, SQLSHORT, 0 ) ;

	dbClass.sqlout (( char *)  zahl_kond.txt, SQLCHAR, 61 ) ;

	dbClass.sqlout (( double *) &zahl_kond.skto1, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( double *) &zahl_kond.skto2, SQLDOUBLE, 0 ) ;
	dbClass.sqlout (( double *) &zahl_kond.skto3, SQLDOUBLE, 0 ) ;

	dbClass.sqlout (( short *) &zahl_kond.ziel1,	SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &zahl_kond.ziel2,	SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &zahl_kond.ziel3,	SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &zahl_kond.zahl_kond, SQLSHORT, 0 ) ;
	dbClass.sqlout (( short *) &zahl_kond.delstatus, SQLSHORT, 0 ) ;


	readcursor = (short) dbClass.sqlcursor ("select "
	" txt "
	" ,skto1 ,skto2 ,skto3 ,ziel1 ,ziel2 ,ziel3, zahl_kond ,delstatus "
	
	" from zahl_kond   " 
	" order by zahl_kond.zahl_kond " ) ;

// einen Satz inserten 

//	dbClass.sqlin (( short *)&tsmtzahlko.mdn, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( long *) &tsmtzahlko.kun, SQLLONG, 0 ) ;
//	dbClass.sqlin (( char *)  tsmtzahlko.kun_bran2, SQLCHAR, 3 ) ;
//	dbClass.sqlin (( short *) &tsmtzahlko.teil_smt, SQLSHORT, 0 ) ;
	dbClass.sqlin (( char *)  zahl_kond.txt, SQLCHAR, 61 ) ;

	dbClass.sqlin (( double *) &zahl_kond.skto1, SQLDOUBLE, 0 ) ;
	dbClass.sqlin (( double *) &zahl_kond.skto2, SQLDOUBLE, 0 ) ;
	dbClass.sqlin (( double *) &zahl_kond.skto3, SQLDOUBLE, 0 ) ;

	dbClass.sqlin (( short *) &zahl_kond.ziel1,	SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *) &zahl_kond.ziel2,	SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *) &zahl_kond.ziel3,	SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *) &zahl_kond.zahl_kond, SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *) &zahl_kond.delstatus, SQLSHORT, 0 ) ;

	ins_cursor = (short) dbClass.sqlcursor ("insert into zahl_kond  "
	" ( txt "
	" ,skto1 ,skto2 ,skto3 ,ziel1 ,ziel2 ,ziel3, zahl_kond , delstatus "
	" ) values ( "
	"  ? "
	" ,? ,? ,? ,? ,? ,? ,? ,?) "
	) ;


// einen Satz updaten 

	dbClass.sqlin (( char *)  zahl_kond.txt, SQLCHAR, 61 ) ;

	dbClass.sqlin (( double *) &zahl_kond.skto1, SQLDOUBLE, 0 ) ;
	dbClass.sqlin (( double *) &zahl_kond.skto2, SQLDOUBLE, 0 ) ;
	dbClass.sqlin (( double *) &zahl_kond.skto3, SQLDOUBLE, 0 ) ;

	dbClass.sqlin (( short *) &zahl_kond.ziel1,	SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *) &zahl_kond.ziel2,	SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *) &zahl_kond.ziel3,	SQLSHORT, 0 ) ;
	dbClass.sqlin (( short *)&zahl_kond.delstatus, SQLSHORT, 0 ) ;


	dbClass.sqlin (( short *) &zahl_kond.zahl_kond, SQLSHORT, 0 ) ;

//	dbClass.sqlin (( short *)&tsmtzahlko.mdn, SQLSHORT, 0 ) ;
//	dbClass.sqlin (( long *) &tsmtzahlko.kun, SQLLONG, 0 ) ;
//	dbClass.sqlin (( char *)  tsmtzahlko.kun_bran2, SQLCHAR, 3 ) ;
//	dbClass.sqlin (( short *) &tsmtzahlko.teil_smt, SQLSHORT, 0 ) ;


	upd_cursor = (short) dbClass.sqlcursor ("update zahl_kond set "
	" txt = ? "
	" ,skto1 = ? ,skto2 = ? ,skto3 = ? ,ziel1 = ? ,ziel2 = ? ,ziel3 = ? ,delstatus = ? "
	" where zahl_kond = ? " 
	);

}

