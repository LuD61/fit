#include "StdAfx.h"
#include "EditListCtrl.h"
#include "listcheckbox.h"

CListCheckBox::CListCheckBox(void)
{
}

CListCheckBox::~CListCheckBox(void)
{
}

BEGIN_MESSAGE_MAP(CListCheckBox, CButton)
	ON_WM_KEYDOWN ()
END_MESSAGE_MAP()

void CListCheckBox::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	int diff = 0;
	switch (nChar)
	{
	case VK_RETURN :
//	case VK_DOWN :
//	case VK_UP :
	case VK_TAB :
	case VK_F6 :
	case VK_F7 :
	case VK_NEXT :
	case VK_PRIOR :
		((CEditListCtrl *)GetParent ())->OnKeyD (nChar);
		return;
	}
CButton::OnKeyDown (nChar, nRepCnt, nFlags);
}

BOOL CListCheckBox::Create (DWORD dwStyle, const RECT& rect, CWnd *pParentWnd, UINT nID)
{
	dwStyle |= BS_AUTOCHECKBOX;
	return CButton::Create (_T(""), dwStyle, rect, pParentWnd, nID); 
}

