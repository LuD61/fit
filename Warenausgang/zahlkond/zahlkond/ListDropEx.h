#ifndef _CLISTDROPEX_DEF
#define _CLISTDROPEX_DEF
#include "afxole.h"
#include "ListDropTarget.h"

class CListDropEx : public CListDropTarget
{
private :
	  CDialog *pWnd; 
   public :
   	  CListDropEx ();
//	  void SetpWnd (DbFormView *pWnd);
	  void SetpWnd (CDialog *pWnd);
	  virtual DROPEFFECT OnDropEx (CWnd* , COleDataObject*,  DROPEFFECT,  DROPEFFECT, CPoint);
};
#endif