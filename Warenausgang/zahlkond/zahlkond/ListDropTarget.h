#ifndef _CLISTDROPTARGE_DEF
#define _CLISTDROPTARGE_DEF
#include "afxole.h"

class CListDropTarget : public COleDropTarget
{
private :
	  CWnd *pWnd; 
   public :
   	  CListDropTarget ();
	  void SetpWnd (CWnd *pWnd);
	  virtual DROPEFFECT OnDragEnter (CWnd* , COleDataObject*,  DWORD,  CPoint);
	  virtual DROPEFFECT OnDragOver (CWnd* , COleDataObject*,  DWORD,  CPoint);
	  virtual DROPEFFECT OnDropEx (CWnd* , COleDataObject*,  DROPEFFECT,  DROPEFFECT, CPoint);
};
#endif