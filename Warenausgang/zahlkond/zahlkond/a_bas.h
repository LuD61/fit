#ifndef _A_BAS_DEF
#define _A_BAS_DEF

/* ----->
// die einzige wirklich wichtige Struktur
struct TSMTZAHLKO
{
short mdn ;
long kun ;
char kun_bran2[3];
short teil_smt;
char txt[81];
double skto1;
double skto2;
double skto3;
short ziel1;
short ziel2;
short ziel3;
short zahl_kond;
}
;
< ----- */


// die einzige wirklich wichtige Struktur
struct ZAHL_KOND
{
// short mdn ;
// long kun ;
// char kun_bran2[3];
// short teil_smt;
char txt[61];
double skto1;
double skto2;
double skto3;
short ziel1;
short ziel2;
short ziel3;
short zahl_kond;
short delstatus;
}
;


extern struct ZAHL_KOND zahl_kond, zahl_kond_null;


struct B_ZAHL_KOND {
char intsmtzahlko [3099] ;
short vonwert ;	// Range auf teil_smt ...
short biswert ;
};

extern struct B_ZAHL_KOND b_zahl_kond ;

class ZAHL_KOND_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
			   void preparedel(void);
       public :
               int leseallzahl_kond (void);
               int openallzahl_kond (void);
               int lesezahl_kond (void);
               int openzahl_kond (void);
			   int deletezahl_kond (void) ;
			   int updzahl_kond(void) ;
			   int insertzahl_kond (void);
               ZAHL_KOND_CLASS () : DB_CLASS ()
               {
               }
};

extern class ZAHL_KOND_CLASS zahl_kond_class ;

#endif

