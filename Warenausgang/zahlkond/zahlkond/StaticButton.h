#pragma once
#include "afxwin.h"

class CStaticButton :
	public CStatic
{
	DECLARE_DYNCREATE(CStaticButton)

protected:
	DWORD Style;
	DECLARE_MESSAGE_MAP()
	virtual void DrawItem (LPDRAWITEMSTRUCT  lpDrawItemStruct);
// ?	virtual BOOL PreTranslateMessage(MSG* pMsg ); 
public:
	enum
	{
		Center = 0,
		Left = 1,
		Right = 2,
	};
	int Orientation;
	COLORREF TextColor;
	HCURSOR Hand;
	HCURSOR Arrow;
    BOOL ButtonCursor;
	UINT nID;
	COLORREF BkColor;
	BOOL ColorSet;
	BOOL NoUpdate;
	CStaticButton(void);
	~CStaticButton(void);
    virtual BOOL Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect,
                        CWnd* pParentWnd,   UINT nID = 0xffff);
	void SetBkColor (COLORREF color);
	void Draw (CDC&);
    void SetButtonCursor (BOOL b);
    BOOL InClient (CPoint p);
    void SetButtonCursor (CPoint p);
    afx_msg void OnMouseMove(UINT nFlags,  CPoint p);
    afx_msg void OnLButtonDown(UINT nFlags,  CPoint p);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest,  UINT message );
	afx_msg void OnKeyDown(UINT nChar,  UINT nRepCnt,  UINT nFlags);
	afx_msg void OnSetFocus(CWnd *cWnd);
	afx_msg void OnKillFocus(CWnd *cWnd);
};
