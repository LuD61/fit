#include "StdAfx.h"
#include "List1Ctrl.h"
#include "StrFuncs.h"
#include "resource.h"
#include "zahlkondDlg.h"
#include "a_bas.h"
#include "ptabn.h"


extern int ZakoGross ;



CList1Ctrl::CList1Ctrl(void)
{
	m_mdn = 1;
//	PosTsmt			= POSTSMT;
	PosTxt			= POSTXT;
	PosZiel1		= POSZIEL1;
	PosSkto1		= POSSKTO1;
	PosZiel2		= POSZIEL2;
	PosSkto2		= POSSKTO2;
	PosZiel3		= POSZIEL3;
	PosSkto3		= POSSKTO3;
	PosZahl_kond    = POSZAHL_KOND;


//	Position[0] = &PosTsmt;
	Position[0] = &PosZahl_kond;
	Position[1] = &PosTxt;
	Position[2] = &PosZiel1;
	Position[3] = &PosSkto1;
	Position[4] = &PosZiel2;
	Position[5] = &PosSkto2;
	Position[6] = &PosZiel3;
	Position[7] = &PosSkto3;

	MaxComboEntries = 100;

	ListRows.Init ();
}

CList1Ctrl::~CList1Ctrl(void)
{
	CString *c;
/* ---->
	MeEinhCombo.FirstPosition ();
	while ((c = (CString *) MeEinhCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MeEinhCombo.Init ();
< --- */
    TsmtCombo.FirstPosition ();
	while ((c = (CString *) TsmtCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	TsmtCombo.Init ();
}

BEGIN_MESSAGE_MAP(CList1Ctrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()



void CList1Ctrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
//		StartEnter (PosA_kun, 0);
		StartEnter (PosTxt, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
//		StartEnter (PosArtNr, 0);
//		StartEnter (PosTsmt, 0);
		StartEnter (PosZahl_kond, 0);
	}

}


void CList1Ctrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
//	if (col == PosArtBez) return;
//	if ( col == PosTsmt )
	if ( col == PosZahl_kond )
	{
//		CString cTsmt = GetItemText (row, PosTsmt);
//		if (StrToDouble (cTsmt) > 0  )	// Teilsmt gefunden/vorhanden 
		CString cZako = GetItemText (row, PosZahl_kond);
		if (StrToDouble (cZako) > 0  )	// Zahl_kond gefunden/vorhanden 
		{
			return ;
		}


		CEditListCtrl::StartEnter (col, row); // versuch : so gehts ,aber ohne Combobox-Funktion

	
		return ;	// Versuch ......

	}


/* --->
	if (col == PosAktiv)
	{
		CEditListCtrl::StartEnterCheckBox (col, row);
		return ;
	}
< ---- */

	CEditListCtrl::StartEnter (col, row);
//	if (col == PosKun_me_einh)
//	if (col == PosTsmt)
	if (col == PosZahl_kond)
	{
		// prinzipielll toter Zweig
		if (IsWindow (ListComboBox.m_hWnd))
		{
			StopEnter ();
		}


//		CString zUBASIS ;
//		zUBASIS =  FillList.GetItemText (row, PosTsmt);	// == col //
		
//		int nCurSel = ListComboBox.FindString( 0,zUBASIS.GetBuffer(0));

		CEditListCtrl::StartEnterCombo (col, row, &TsmtCombo);

//		if (nCurSel > 0)
//			ListComboBox.SetCurSel( nCurSel ) ;
//		else
//			ListComboBox.SetCurSel( 0 ) ;
//		oldsel = ListComboBox.GetCurSel ();
		return ;
	}

/* ---->
	if (col == PosZuschlag || col == PosKost_bez)
	{
		CEditListCtrl::StartEnter (col, row);
	}
< ----- */
}

void CList1Ctrl::StopEnter ()
{

	CEditListCtrl::StopEnter ();

	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = _T("");
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
	}
/* --->
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		int check = ListCheckBox.GetCheck ();
        if (check == BST_CHECKED)
		{
			FillList.SetItemText (_T("X"), EditRow, EditCol);
		}
		else
		{
			FillList.SetItemText (_T(" "), EditRow, EditCol);
		}
		ListCheckBox.DestroyWindow ();
	}
< ---- */

	/* ---->
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		int count = GetItemCount ();
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	< ----- */
}

void CList1Ctrl::SetSel (CString& Text)
{

//   if (EditCol == PosArtNr )   // ||  EditCol == PosLdPr ||  EditCol == PosEkProz)


//   if (EditCol == PosTsmt )   // ||  EditCol == PosLdPr ||  EditCol == PosEkProz)
   if (EditCol == PosZahl_kond )   // ||  EditCol == PosLdPr ||  EditCol == PosEkProz)
   {
		ListEdit.SetSel (0, -1);
   }

/* ----->
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
//		ListEdit.SetSel (cpos, cpos);
   }
< ------ */
   }

void CList1Ctrl::FormatText (CString& Text)
{


// if (EditCol == PosTsmt)
//	{
//		DoubleToString (StrToDouble (Text), Text, 0);
//	}
  if (EditCol == PosZiel1)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
  if (EditCol == PosZiel2)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
  if (EditCol == PosZiel3)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
 
  if (EditCol == PosSkto1)
	{
		DoubleToString (StrToDouble (Text), Text, 1);
	}

  if (EditCol == PosSkto2)
	{
		DoubleToString (StrToDouble (Text), Text, 1);
	}
  if (EditCol == PosSkto3)
	{
		DoubleToString (StrToDouble (Text), Text, 1);
	}

   if (EditCol == PosZahl_kond)
	{
// VERSUCH		if ( _tcslen(Text.Trim() ) > 0 )
			DoubleToString (StrToDouble (Text), Text, 0);
	}

 /* ------>

 if (EditCol == PosArtNr)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
 
  if (EditCol == PosEan)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
  if (EditCol == PosEan_VK)
	{
		DoubleToString (StrToDouble (Text), Text, 0);
	}
  if (EditCol == PosGeb_fakt)
	{
		DoubleToString (StrToDouble (Text), Text, 1);
	}
  if (EditCol == PosKun_inh)
	{
		DoubleToString (StrToDouble (Text), Text, 3);
	}
< ---- */

/* ---
    if (EditCol == PosZuschlag)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
< ---- */
/* --->
   if (EditCol == PosZuschlag || EditCol == PosZubasis )
   {
	   double dizuschlag ;
	   short dibasis ;
	   if ( EditCol == PosZuschlag )
	   {
			dizuschlag = StrToDouble (Text) ;
			CString zUBASIS ;
			zUBASIS =  FillList.GetItemText (EditRow, PosZubasis);
			dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!
	   }
	   else
	   {

 			CString zUSCHLAG =  FillList.GetItemText (EditRow, PosZuschlag);
			dizuschlag = CStrFuncs::StrToDouble (zUSCHLAG);
			CString zUBASIS ;
			zUBASIS =  FillList.GetItemText (EditRow, PosZubasis);
			dibasis = CStrFuncs::StrToDouble (zUBASIS);	// Nimmt den Wert bis zum blank ?!
	   }
		double diposwert, diposwertkg ;
		switch ( dibasis )
		{
//		case 0 :	// je kg
		case 1 :	// je Stck.
			diposwert = dizuschlag * lianzahl ;
			if ( dikaltgew != 0.0 )
			{
			    diposwertkg = (dizuschlag *	lianzahl ) / dikaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;
		case 2 :	// pauschal
			diposwert = dizuschlag ;
			if ( dikaltgew != 0.0 )
			{
			    diposwertkg = dizuschlag / dikaltgew ;
			}
			else
			{
				diposwertkg = 0.0 ;
			}
			break ;

		default :	// auffang-Linie : je kg
		    diposwert = dizuschlag * dikaltgew ;
			diposwertkg = dizuschlag ;
			break ;
		}

		CString wERT;
		DoubleToString (diposwert, wERT, 2);
//		Schlaklkp.schlaklkp.wert = CStrFuncs::StrToDouble (wERT);
// 		FillList.SetItemText (wERT.GetBuffer (), i, m_list1.PosWert);
		FillList.SetItemText (wERT.GetBuffer (), EditRow, PosWert);

		CString wERTKG;
		DoubleToString (diposwertkg, wERTKG, 4);
//		Schlaklkp.schlaklkp.wertkg = CStrFuncs::StrToDouble (wERTKG);
//		FillList.SetItemText (wERTKG.GetBuffer (), i, m_list1.PosWertkg);
		FillList.SetItemText (wERTKG.GetBuffer (), EditRow, PosWertkg);
	}

< ---- */

	/* --->
   CString weRTKG ;
   dizuabkg = 0.0 ; 
 	for (int i = GetItemCount()  ; i > 0 ; )
	{
		i -- ;
		weRTKG =  FillList.GetItemText (i, PosWertkg);
		dizuabkg += CStrFuncs::StrToDouble (weRTKG);
	}

	Zuschlagsetzen  ( ) ;

< ----- */

/*
    else if (EditCol == 4)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
*/

}

void CList1Ctrl::NextRow ()
{
//	CString Zuschlag = GetItemText (EditRow, PosZuschlag);
//	double 	dZuschlag = StrToDouble (Zuschlag) ;

	int count = GetItemCount ();

	BOOL ob = TRUE ;

/* --->
	if (EditCol == PosArtNr)
	{
		ob = ReadArtikel ();
	}
< ---- */
/* --->
	if (EditCol == PosTsmt)
	{
		ob = ReadTsmt ();
	}
< --- */

	if (EditCol == PosZahl_kond)
	{

		ob = ReadZahl_kond ();

/* ---->
		CString Zahl_Kond = GetItemText (EditRow, PosZahl_kond);
		if (_tcslen(Zahl_Kond.Trim())== 0 || CStrFuncs::StrToDouble(Zahl_Kond) == 0.0 )
		{	// auf "blank" stehen bleiben
			ob = FALSE ;
		}
< ---- */
	}


	
/* ->
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< --- */

/* ---->
	if (EditCol == PosKun_me_einh)
	{
//		ReadZuBasis ();	// ReadPrGrStuf
	}
< ---- */

	SetEditText ();

//	TestIprIndex ();
	count = GetItemCount ();
	if ( ob == TRUE )
	{
		if (EditRow >= count - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
			StopEnter ();
			EditRow ++;
			EditCol = 0;

		}
		else
		{
			StopEnter ();
			EditRow ++;
		}
	}
	else
	{	// Auf Schluessel stehen bleiben
//		EditCol = PosArtNr ;
//		EditCol = PosTsmt ;
		EditCol = PosZahl_kond ;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);

	if (GetKeyState (VK_SHIFT) < 0)
	{
		BOOL &b = vSelect[EditRow - 1];
		b = TRUE;
		FillList.SetItemImage (EditRow - 1, 1);
	}
	else if (GetKeyState (VK_CONTROL) == 0)
	{
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
	}
}

void CList1Ctrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();

	BOOL ob = TRUE ;

//	if (EditCol == PosArtNr)
//	if (EditCol == PosTsmt)
	if (EditCol == PosZahl_kond)
	{
		ob = FALSE ;
		if (IsWindow (ListEdit.m_hWnd))
		{
			CString Text;
			ListEdit.GetWindowText (Text);
			if (atol (Text.GetBuffer()) == 0l)
			{
				DeleteRow() ;
			}
		}
		else

			EditCol = PosTxt ;
//			EditCol = PosA_kun ;
	}

	if (EditRow == count - 1)
	{
/* --->
		CString Zuschlag = GetItemText (EditRow, PosZuschlag);
		CString Kost_bez = GetItemText (EditRow, PosKost_bez);
		if ((StrToDouble (Zuschlag) == 0.0)
			&& (_tcslen (Kost_bez.Trim()) == 0 ))
		{
	        DeleteItem (EditRow);
		}
< ---- */
	}
	else
	{
		if (EditRow <= 0)
		{
			return;
		}
/* ---->
		if (EditCol == PosKun)
		{
			ReadKunName ();
		}
		else if (EditCol == PosKunPr)
		{
			ReadKunPr ();
		}
		else if (EditCol == PosPrGrStuf)
		{
			ReadPrGrStuf ();
		}
		TestIprIndex ();
< ---- */
	}
	StopEnter ();
 
	EditRow --;
	EnsureVisible (EditRow, FALSE);
	if ( ob == FALSE )

//		EditCol = PosA_kun ;
		EditCol = PosTxt ;

	StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CList1Ctrl::LastCol ()
{

//	if (EditCol < PosLi_a) return FALSE;
//	if (EditCol < PosZahl_kond) return FALSE;
	if (EditCol < PosSkto3) return FALSE;

/* --->
	if (Mode == TERMIN && EditCol >= PosLdPr)
	{
		return TRUE;
	}
	if (Aufschlag == LIST || Aufschlag == ALL)
	{
		if (EditCol < PosEkProz) return FALSE;
	}
< ---- */

	return TRUE;
}

void CList1Ctrl::OnReturn ()
{
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{
/* --->
Ganz am Ende der Liste .....
		CString Zuschlag = GetItemText (EditRow, PosZuschlag);
		CString Kost_bez = GetItemText (EditRow, PosKost_bez);
		if ((StrToDouble (Zuschlag) == 0.0 ) 
			&&	(_tcslen(Kost_bez.Trim())== 0 ))
		{
			EditCol --;
			return;
		}
< ----- */


			CString cZahl_kond = GetItemText (EditRow, PosZahl_kond);
			if (StrToDouble (cZahl_kond) < 0.1 ) 
			{
				EditCol --;
				return;
			}
	}
/* --->
	if (EditCol == PosTsmt)
	{
		ReadTsmt ();
	}
< ---- */

	if (EditCol == PosZahl_kond)
	{
		ReadZahl_kond ();
//		CString Zahl_Kond = GetItemText (EditRow, PosZahl_kond);
// VERSUCH		if (_tcslen(Zahl_Kond.Trim())== 0 )
//		{
//			return;
//		}
	}


/* -->
	if (EditCol == PosArtNr)
	{
		ReadArtikel ();
	}
< ----- */
	if (LastCol ())
	{
//		TestIprIndex ();

	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}

		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;
		if (EditRow == rowCount)
		{
//			EditCol = PosArtNr;
//			EditCol = PosTsmt;
			EditCol = PosZahl_kond;
		}
		else
		{
//			EditCol = PosA_kun;
			EditCol = PosTxt;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
/* ----- > ro-Felder ueberlesen 
		if (EditCol == PosArtBez)
		{
			EditCol ++;
		}
		if (EditCol == PosKante)
		{
			EditCol ++;
		}
< ----- */
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CList1Ctrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}

/* --->
	if (EditCol == PosArtNr)
	{
		ReadArtikel ();
	}
< --- */

	if (EditCol == PosZahl_kond)
	{
		ReadZahl_kond ();
	}


	StopEnter ();
	EditCol ++;
/* --->
	if (EditCol == PosKunName)
	{
		EditCol ++;
	}
< --- */
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CList1Ctrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
/* ---->
	if (EditCol == PosKun)
	{
		ReadKunName ();
	}
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
< -- */

	StopEnter ();
	EditCol --;
/* ---->
	if (EditCol == PosKunName)
	{
		EditCol --;
	}
< ---- */
    StartEnter (EditCol, EditRow);
}

BOOL CList1Ctrl::InsertRow ()
{
//	return FALSE ;
/* ---->
	CString Zuschlag = GetItemText (EditRow, PosZuschlag);
	CString Kost_bez = GetItemText (EditRow, PosKost_bez);
	if ((GetItemCount () > 0) && 
		(StrToDouble (Zuschlag) == 0.0) &&
		 (_tcslen(Kost_bez) == 0))
	{
		return FALSE;
	}
< ---- */
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
//		FillList.SetItemText (" ", EditRow, PosArtNr);
//		FillList.SetItemText (" ", EditRow, PosArtBez);
		FillList.SetItemText (" ", EditRow, PosTxt);


//		CString pKANTE;
//		pKANTE = _T("|") ;
//		FillList.SetItemText (pKANTE.GetBuffer (), EditRow, PosKante);

//		FillList.SetItemText (" " , EditRow, PosA_kun);
//		FillList.SetItemText (" " , EditRow, PosA_bz1);
//		FillList.SetItemText (" " , EditRow, PosA_bz2);

		CString pTWERT ;
		TCHAR ptbezk [37] ; 
		pTWERT.Format (_T("0"));	// Default : gar nix  
		CChoiceTsmt HoleBezk ;
		HoleBezk.GetPtBezk ( pTWERT.GetBuffer(), ptbezk );
		CString zUBASIS;
// das Problem : Formatierung numerischer Wert mit 2 Folgeblanks 
//		zUBASIS.Format (_T("0    %s"), ptbezk);
//		FillList.SetItemText (zUBASIS.GetBuffer (), EditRow, PosTsmt);

		FillList.SetItemText (" " , EditRow, PosZahl_kond);	// blank editiert sich besser
		FillList.SetItemText (" " , EditRow, PosSkto1);	// blank editiert sich besser
		FillList.SetItemText (" " , EditRow, PosZiel1);	// blank editiert sich besser
		FillList.SetItemText (" " , EditRow, PosSkto2);	// blank editiert sich besser
		FillList.SetItemText (" " , EditRow, PosZiel2);	// blank editiert sich besser
		FillList.SetItemText (" " , EditRow, PosSkto3);	// blank editiert sich besser
		FillList.SetItemText (" " , EditRow, PosZiel3);	// blank editiert sich besser

		memcpy ( &zahl_kond , &zahl_kond_null , sizeof ( ZAHL_KOND )) ;
		
		
//		FillList.SetItemText (" ",EditRow, PosKun_inh);	// blank  editiert sich besser//
//		FillList.SetItemText (" " , EditRow, PosGeb_fakt);	// blank editiert sich besser
//		FillList.SetItemText (" ", EditRow, PosEan);
//		FillList.SetItemText (" ", EditRow, PosEan_VK);
//		FillList.SetItemText (" ", EditRow, PosLi_a);
		
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CList1Ctrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
//	return FALSE ;
	return CEditListCtrl::DeleteRow ();
}

BOOL CList1Ctrl::AppendEmpty ()
{
	int rowCount = GetItemCount ();

	if (rowCount > 0)
	{

/* ---
		CString cZahl_kond = GetItemText (EditRow + 1 , PosZahl_kond);
		double hilfe = StrToDouble (cZahl_kond) ;
		if (hilfe < 1 )	// Bereits neue Zeile angelegt
		{
			return FALSE;
		}
<   --- */
	}

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);
/* --->
	CString pOSI ;
	pOSI.Format (_T("%d"), rowCount + 1 );
	FillList.SetItemText (_T(pOSI.GetBuffer()), rowCount, PosPosi);

	FillList.SetItemText (_T(""), rowCount, PosKost_bez);
	FillList.SetItemText (_T("0,00"), rowCount, PosZuschlag);
	FillList.SetItemText (_T(""), rowCount, PosZubasis);
	FillList.SetItemText (_T("0,00"), rowCount, PosWert);
	FillList.SetItemText (_T("0,00"), rowCount, PosWertkg);
< ---- */

		FillList.SetItemText (" ", rowCount,PosTxt);
		FillList.SetItemText (" " , rowCount, PosSkto1);	// blank editiert sich besser
		FillList.SetItemText (" " , rowCount, PosZiel1);	// blank editiert sich besser
		FillList.SetItemText (" " , rowCount, PosSkto2);	// blank editiert sich besser
		FillList.SetItemText (" " , rowCount, PosZiel2);	// blank editiert sich besser
		FillList.SetItemText (" " , rowCount, PosSkto3);	// blank editiert sich besser
		FillList.SetItemText (" " , rowCount, PosZiel3);	// blank editiert sich besser

		FillList.SetItemText (" " , rowCount, PosZahl_kond);	// blank editiert sich besser

		
		//		FillList.SetItemText (" ", rowCount, PosArtNr);
//		FillList.SetItemText (" ", rowCount, PosArtBez);

//		CString pKANTE;
//		pKANTE = _T("|") ;
//		FillList.SetItemText (pKANTE.GetBuffer (), rowCount, PosKante);

//		FillList.SetItemText (" " , rowCount, PosA_kun);
//		FillList.SetItemText (" " , rowCount, PosA_bz1);
//		FillList.SetItemText (" " , rowCount, PosA_bz2);

		CString pTWERT ;
		TCHAR ptbezk [37] ; 
		pTWERT.Format (_T("0"));	// Default : goor nix 
		CChoiceTsmt HoleBezk ;
		HoleBezk.GetPtBezk ( pTWERT.GetBuffer(), ptbezk );
		CString zUBASIS;
// das Problem : Formatierung numerischer Wert mit 2 Folgeblanks 
//		zUBASIS.Format (_T("0    %s"), ptbezk);
//		FillList.SetItemText (zUBASIS.GetBuffer (), rowCount, PosTsmt);

//		FillList.SetItemText (" ",rowCount, PosKun_inh); // blank editiert sdich besser
//		FillList.SetItemText (" " , rowCount, PosGeb_fakt);	// blank editiert sich besser 
//		FillList.SetItemText (" ", rowCount, PosEan);
//		FillList.SetItemText (" ", rowCount, PosEan_VK);
//		FillList.SetItemText (" ", rowCount, PosLi_a);
		
		
	rowCount = GetItemCount ();
	return TRUE;
}

void CList1Ctrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/

}

/* --->
void CKstArtListCtrl::FillPrGrStufCombo (CVector& Values)
{
	CString *c;
    PrGrStufCombo.FirstPosition ();
	while ((c = (CString *) PrGrStufCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	PrGrStufCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		PrGrStufCombo.Add (c);
	}
}
< ---- */


void CList1Ctrl::FillTsmtCombo (CVector& Values)
{
	CString *c;
    TsmtCombo.FirstPosition ();
	while ((c = (CString *) TsmtCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	TsmtCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		TsmtCombo.Add (c);
	}
}

/* --->
void CList1Ctrl::FillMeEinhCombo (CVector& Values)
{
	CString *c;
    MeEinhCombo.FirstPosition ();
	while ((c = (CString *) MeEinhCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	MeEinhCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		MeEinhCombo.Add (c);
	}
}
< ------- */


/* ----->
void CKstArtListCtrl::RunItemClicked (int Item)
{
**
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
**
}
< ----- */


void CList1Ctrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CList1Ctrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CList1Ctrl::OnChoice ()
{
	/* -> 
	if (EditCol == PosKun)
	{
		OnKunChoice (CString (_T("")));
	}
	else if (EditCol == PosKunPr)
	{
		OnIKunPrChoice (CString (_T("")));
	}
	else if (EditCol == PosPrGrStuf)
	{
		OnPrGrStufChoice (CString (_T("")));
	}
	< ----- */
}


/* --->
void CList1Ctrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}
< ---- */



BOOL CList1Ctrl::ReadZahl_kond ()
{
	if (EditCol != PosZahl_kond) 
		return TRUE ;
//    memcpy (&a_bas, &a_bas_null, sizeof (A_BAS));
//	memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
/* ---->
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		return;
	}
< ---- */

    CString Text;
	ListEdit.GetWindowText (Text);
/* ---->
	if (!CStrFuncs::IsDecimal (Text))
	{
		OnKunChoice (Text);
	    SearchListCtrl.Edit.GetWindowText (Text);
		Text.Format (_T("%ld"), atol (Text.GetBuffer ()));
	    SearchListCtrl.Edit.SetWindowText (Text);
		if (!KunChoiceStat)
		{
			EditCol --;
			return;
		}
	}
< ---- */
	if (atol (Text) == 0L)
	{
		MessageBox (_T("eine Eingabe ist erforderlich"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return FALSE ;
	}


	long vzako =  atol (Text);

	// Es sind maximal 4 Stellen erlaubt ( smallint ) 
	if ( ZakoGross == 1 )
	{
		if ( vzako > 9 || vzako < 1 )
		{
			MessageBox (_T("Maximalwert 9 überschritten "), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return FALSE ;
		}

	}
	
	if ( ZakoGross == 2 )
	{
		if ( vzako > 99 || vzako < 1 )
		{
			MessageBox (_T("Maximalwert 99 überschritten "), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return FALSE ;
		}
	}

	if ( ZakoGross == 3 )
	{
		if ( vzako > 999 || vzako < 1 )
		{
			MessageBox (_T("Maximalwert 999 überschritten "), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return FALSE ;
		}

	}
	if ( ZakoGross == 4 )
	{
		if ( vzako > 9999 || vzako < 1 )
		{
			MessageBox (_T("Maximalwert 9999 überschritten "), NULL, MB_OK | MB_ICONERROR); 
			EditCol --;
			return FALSE ;
		}

	}

	int i = 0 ;	// Nur in der Liste vergleichen ....
	if (!i)
    {
		CString hilfart ;
		long dart ;
		for (int rowco = GetItemCount()  ; rowco > 0 ; )
		{
			rowco -- ;
			hilfart =  FillList.GetItemText (rowco, PosZahl_kond);
			dart = (long) CStrFuncs::StrToDouble (hilfart) ;  
			if ( ( dart == vzako ) && ( rowco != EditRow ) && dart > 0 )
			{
				MessageBox (_T("Kondition ist bereits in der Liste"), NULL, MB_OK | MB_ICONERROR); 
				EditCol --;
				return FALSE ;
			}
		}
	}

	return TRUE ;

}

void CList1Ctrl::GetColValue (int row, int col, CString& Text)
{
	/* ---->
    CString cText = GetItemText (row, col);
	int pos = 0;
	if (col == PosPrGrStuf)
	{
		Text = cText.Tokenize (_T(" "), pos);
	}
	else if (col == PosKunPr)
	{
		Text = cText.Tokenize (_T(" "), pos);
	}
	else
	{
		Text = cText.Trim ();
	}
	< ---- */
}

/* ---->
void CList1Ctrl::TestIprIndex ()
{
** ---->
	int Items = GetItemCount ();
	if (Items <= 1) return;
	CString Value;
	GetColValue (EditRow, PosPrGrStuf, Value);
	long rPrGrStuf = atol (_T(Value));
	GetColValue (EditRow, PosKunPr, _T(Value));
	long rKunPr = atol (_T(Value));
	GetColValue (EditRow, PosKun, _T(Value));
	long rKun = atol (_T(Value));
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosPrGrStuf, Value);
		long lPrGrStuf = atol (_T(Value));
		GetColValue (i, PosKunPr, Value);
		long lKunPr = atol (_T(Value));
		GetColValue (i, PosKun, Value);
		long lKun = atol (Value);
		if (lKun == rKun && rKun != 0)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
		else if (lKunPr == rKunPr && rKunPr != 0)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
		else if (lPrGrStuf == rPrGrStuf && lKunPr == rKunPr
				&& lKun == rKun)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
	}
	< --- **
}
< ---- */

void CList1Ctrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}

