#include "stdafx.h"
#include "DbClass.h"
#include "ptabn.h"


struct PTABN ptabn,  ptabn_null;
extern DB_CLASS dbClass;

PTABN_CLASS ptabn_class ;

// int itxt_nr ;

static int anzzfelder ;

int PTABN_CLASS::dbcount (void)
/**
Tabelle ptabn lesen.
**/
{
         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         dbClass.sqlopen (count_cursor);
         dbClass.sqlfetch (count_cursor);
         if (sqlstatus == 0)
         {
                      return anzzfelder;
         }
         return sqlstatus;

}


int PTABN_CLASS::leseptabn (							)
{

      int di = dbClass.sqlfetch (readcursor);

	  return di;
}

int PTABN_CLASS::leseallptabn (							)
{

      int di = dbClass.sqlfetch (ins_cursor);

	  return di;
}


int PTABN_CLASS::openptabn (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

int PTABN_CLASS::openallptabn (char * eptabn)
{
	sprintf ( ptabn.ptitem , "%s" , eptabn ) ;

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (ins_cursor);
}


void PTABN_CLASS::prepare (void)
{


 	dbClass.sqlin ((char *) ptabn.ptwert, SQLCHAR, 4);
	dbClass.sqlin ((char *) ptabn.ptitem, SQLCHAR, 19);
    dbClass.sqlout ((long *)  &anzzfelder, SQLLONG, 0);


    count_cursor = dbClass.sqlcursor ("select count(*) from ptabn "
										"where ptabn.ptwert = ? and ptabn.ptitem = ? ");
										
	test_upd_cursor = 1;


	dbClass.sqlin ((char *) ptabn.ptwert, SQLCHAR, 4);
	dbClass.sqlin ((char *) ptabn.ptitem, SQLCHAR, 19);

	dbClass.sqlout ((long *) &ptabn.ptlfnr, SQLLONG, 0);
	dbClass.sqlout ((char *) ptabn.ptbez,  SQLCHAR,33);
	dbClass.sqlout ((char *) ptabn.ptbezk, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer1, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer2, SQLCHAR, 9);

			
	readcursor = dbClass.sqlcursor ("select "
		" ptlfnr, ptbez, ptbezk, ptwer1, ptwer2  "
		" from ptabn where ptwert = ? and ptitem = ? " ) ;


	dbClass.sqlin ((char *) ptabn.ptitem, SQLCHAR, 19);

	dbClass.sqlout ((char *) ptabn.ptwert, SQLCHAR, 4);
	dbClass.sqlout ((long *) &ptabn.ptlfnr, SQLLONG, 0);
	dbClass.sqlout ((char *) ptabn.ptbez,  SQLCHAR,33);
	dbClass.sqlout ((char *) ptabn.ptbezk, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer1, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer2, SQLCHAR, 9);
	
			
	ins_cursor = dbClass.sqlcursor ("select "
		" ptwert, ptlfnr, ptbez, ptbezk, ptwer1, ptwer2  "
		" from ptabn where ptitem = ?  order by  ptlfnr " ) ;
	
// Sortierung nach ptlfnr !! 

  }
