/* ************************************************************************ */
/*       Definitionen und extern Variablen fuer Programme die das 
         Modul mo_intp benutzen.

         Autor : Wilhelm Roth
*/
/* ************************************************************************ */
#ifndef _MO_INTP_DEF
#define _MO_INTP_DEF
#include <stdio.h>
#include <stdarg.h>

typedef struct
       {   char  feldname [42];
           short feldlen;
           char  feldtyp [2];
           short nachkomma;
           short dimension;
       } FELDER;

typedef struct
       {   FELDER **felder;
           char   **buffer;
       } RECORD;

union FADR
      { unsigned char  *fchar;
        short *fshort;
        int   *fint;
        long  *flong;
        void  *fvoid;
        double *fdouble;
      };

struct IFITEM
      { char name [42];
        char bezlang [18];
      };


typedef struct
{
 char *bedingung;
 char *anweisung;
 short bflag;
} iff;


extern struct IFITEM ifitemtab [];
extern ifitemanz;

extern void *malloc ();
extern char *strchr ();
extern long atol ();

extern char *wort [];             /* Bereich fuer split              */
extern char *eingabe;             /* Name der Ascii-Datei            */
extern char *info_name;           /* Name der Info-Datei             */
extern short ziel_len;      /* Laenge Zielsatz. Wird zur Laufzeit gesetzt    */
extern unsigned char *eingabesatz;   /* Buffer fuer Ascii-Eingabesatz                 */
extern short qlen;          /* Laenge Eingabesatz                            */
extern unsigned char *ausgabesatz;   /* Buffer fuer Binaer-Ausgabesatz                */
extern short zlen;          /* Laenge ausgabesatz                            */
extern char *daten;         /* Buffer fuer allgemeien Variablen              */
extern short dlen;          /* Anzahl allgemeien Daten                       */
extern char CRT[];

extern short constat;
extern short ecode;         /* Exit-Status                                   */
extern short do_exit;       /* Kennzeichen fuer exit                         */
extern FELDER *quelle;      /* Tabelle mit Satzaufbau fuer Eingabefelder     */
extern short feld_anz;      /* Anzahl Felder im Satz                         */
extern RECORD insatz;

extern FELDER *ziel;        /* Tabelle mit Tabellenaufbau aus der Datenbank  */
extern short db_anz;        /* Anzahl Felder im Satz                         */
static lesestatus;          /* SQL-Status beim Lesen                        */
extern RECORD outsatz;

extern FELDER *data;         /* Tabelle mit Variablen                       */
extern short data_anz;       /* Anzahl Variablen                           */
extern RECORD datasatz;

extern short binaer;         /* Dateimodus                                   */

int znullvalue (char *feldname);
int uebertrage (RECORD *,
                char *,
                RECORD *,
                char *);

extern short blklen;            /* SW-Blocklaenge                        */
extern short blkanz;            /* Anzahl SW-Bloecke                     */
extern short banz;              /* Anzahl SW-Felder pro Satz             */
extern short feld_anz;          /* Anzahl SW-Felder pro Satz             */
extern short klen;              /* Anzahl SW-Felder pro Satz             */


int perform_test_write (void);
int perform_code_upd (void);
int perform_code_del (void);
int getdbtyp (FELDER *);
int feldpos (FELDER *, char *);
int instruct (FELDER *, char *);
int mov (char *, char *, ...);
int belege_data (FILE *, char *);
void belege_code (FILE *, char *);
int belege_structs (FILE *, char *);
int perform_begin (void);
int perform_code (void);
int perform_ende (void);
int set_dprog (int (*prog) (iff *, short));
void set_debug (int);
int getwert (char *, char *, short i);
int do_call (char *);
void SetDbExecute (int (*proc) (char *));
void SetDbPrepare (int (*proc) (char *));
void SetDbOpen (int (*proc) (int));
void SetDbfetch (int (*proc) (int));
void SetDbexecurs (int (*proc) (int));
void SetDbclose (int (*proc) (int));
void SetDboutquest (int (*proc) (char *, int, int));
void SetDbinsquest (int (*proc) (char *, int, int));
char *feldadr (char *);           /* Feldadresse holen                              */
char *GetIfItem (char *, char *);

int readsw (char *, FILE *);
int writesw (char *, short, FILE *);
int uebertrage (RECORD *, char *, RECORD *, char *);
char *feldadr (char *);
int init_sw_start (void);
int lese_sw (char *, int);
int sw_info (FELDER *);
int sw_infoa (FELDER *);
int satzlen (FELDER *);
int GetSwKopf (void **, int *);

#endif
