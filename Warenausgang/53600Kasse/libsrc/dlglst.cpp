#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_gdruck.h" 
#include "colbut.h"
#include "help.h"
#include "lbox.h"
#ifdef BIWAK
#include "conf_env.h"
#endif
#include "mo_progcfg.h"
#include "dlglst.h"


static int StdSize = STDSIZE;

static int dlgsize    = STDSIZE;
static int ltgraysize = STDSIZE;
static int dlgpossize = STDSIZE;
static int cpsize     = 200;


static mfont dlgfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont ltgrayfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                        0,
                         NULL};

static mfont checkfonth = {
                           "ARIAL", STDSIZE, 0, 0,
                            BLACKCOL,
                            LTGRAYCOL,
                            0,
                            NULL};

static mfont dlgposfont = {
                         "ARIAL", STDSIZE, 0, 0,
                         BLACKCOL,
                         LTGRAYCOL,
                         0,
                         NULL};


static mfont cpfont = {
                        "Times New Roman", 200, 0, 1,
                         BLUECOL,
                         GRAYCOL,
                         0,
                         NULL};

static mfont *Font = &dlgposfont;

int DlgLst::ListRows = 0;
BOOL DlgLst::InsMode = FALSE;
BOOL DlgLst::WriteOK= FALSE;
char *DlgLst::HelpName = "texte.cmd";
UCHAR DlgLst::TChar = '|';

void DlgLst::SetListDimension (int cx, int cy)
{
	     if (fWork == NULL) return;
         fWork->GetCfield ("Liste")->SetCX (cx);
         fWork->GetCfield ("Liste")->SetCY (cy);
}

void DlgLst::DiffListDimension (int cx, int cy)
{
	     if (fWork == NULL) return;
         fWork->GetCfield ("Liste")->SetCX (fWork->GetCfield ("Liste")->GetCXorg () + cx);
         fWork->GetCfield ("Liste")->SetCY (fWork->GetCfield ("Liste")->GetCYorg () + cy);
}

int DlgLst::ListChanged (WPARAM wParam, LPARAM lParam)
{
        int idx;

        InsMode = FALSE;
        if (WriteOK) return 0;

        fWork->GetText ();
        idx = ::SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
        if (idx != -1)
        {
            FillEnterList (idx);
        }
        return TRUE;
}

int DlgLst::GetAktRow (void)
{
	    int idx;
        idx = ::SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
		return idx;
}

int DlgLst::GetListRows (void)
{
         ListRows = ::SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCOUNT, 0, 0l);
		return ListRows;
}

void DlgLst::FillEnterList (int idx)
{
}

void DlgLst::FillEnterList (char *buffer)
{
}
                         
                         
DlgLst::DlgLst (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel) :
         DLG (x, y, cx, cy, Caption, Size, Pixel)
{
          
             BorderType = HIGHCOLBORDER;
             Init (x, y, cx, cy, Caption, Size, Pixel);
}


void DlgLst::Init (int x, int y, int cx, int cy, char *Caption, int Size, BOOL Pixel)
{
             int xfull, yfull;

             xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
             yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

             StdSize = Size;
             dlgfont.FontHeight    = Size;
             dlgposfont.FontHeight = Size;
             ltgrayfont.FontHeight = Size;
             cpfont.FontHeight     = cpsize;


             if (xfull < 1000) 
             {
                 dlgfont.FontHeight = 90;
                 dlgposfont.FontHeight = 90;
                 ltgrayfont.FontHeight = 90;
                 checkfonth.FontHeight = 90;
                 cpfont.FontHeight = 150;
             }

             if (xfull < 800) 
             {

                 dlgfont.FontHeight = 70;
                 dlgposfont.FontHeight = 70;
                 ltgrayfont.FontHeight = 70;
                 checkfonth.FontHeight = 70;
                 cpfont.FontHeight = 120;
             }

             SetTmFont (&dlgfont);
			 Dlg = NULL;
}


int DlgLst::WriteRow (void)
{
	    InsMode = FALSE;
        FillRow ();
        WriteOK = FALSE;
        return 0;
}

void DlgLst::InitRec (void)
{
}

void DlgLst::Scroll (int mode)
{
}

void DlgLst::ToForm (void)
{
}

void DlgLst::FromForm (void)
{
}


void DlgLst::FillRow (char *buffer)
{
}

BOOL DlgLst::InsPosOk (char *buffer)
{
       return FALSE;
}

BOOL DlgLst::SelPosOk (char *buffer)
{
       return TRUE;
}

int DlgLst::DeleteRow (void)
{
        HWND lBox;
        int idx;
        char buffer [512];

        lBox = fWork->GetCfield ("Liste")->GethWnd ();
        if (lBox == NULL) return 0;

        idx = ::SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
        InsMode = FALSE;
        fWork->GetText ();
        ScrollList (lBox, idx, SCDELETE);
		Scroll (SCDELETE);
        ListRows = ::SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCOUNT, 0, 0l);
        if (ListRows == 0)
        {
               InitRec ();
               ToForm ();
               fWork->SetText ();
               InvalidateRect (lBox, NULL, TRUE);
               UpdateWindow (lBox);
               return 0;
        }
        if (idx > ListRows - 1) idx --;
        ::SendMessage (lBox, LB_GETTEXT, idx, (LPARAM) buffer);
        FillEnterList (buffer);
        ToForm (); 
        fWork->SetText ();

        InvalidateRect (lBox, NULL, TRUE);
        UpdateWindow (lBox);
        return 0;
}

void DlgLst::FillRow (void)
{
        char buffer [512];
        int idx;

        if (fWork->GetCfield ("Liste")->GethWnd () == NULL) return;

        idx = ::SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCURSEL, 0, 0l);
        if (idx < 0) return;

        FillRow (buffer);

        ::SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_INSERTSTRING, idx,  
                           (LPARAM) (char *) buffer);
}

void DlgLst::InsertRow (void)
{
        int i;
        char buffer [512];
        HWND lBox;

        if (InsMode) return;

        lBox = fWork->GetCfield ("Liste")->GethWnd ();
        if (lBox == NULL) return;

        ListRows = ::SendMessage (lBox, LB_GETCOUNT, 0, 0l);

        for (i = 0; i < ListRows; i ++)
        {

            ::SendMessage (lBox, LB_GETTEXT, i, (LPARAM) buffer);
               if (InsPosOk (buffer)) break;
        }

        FillRow (buffer);

		Scroll (SCINSERT);
        ScrollList (lBox, i, SCINSERT);
        InvalidateRect (lBox, NULL, TRUE);
        UpdateWindow (lBox);
        ::SendMessage (lBox, LB_INSERTSTRING, i, (LPARAM) (char *) buffer);
        ::SendMessage (lBox, LB_SETCURSEL, (WPARAM) i, (LPARAM) 0l);
        InsMode = TRUE;
}

void DlgLst::SelectRow (void)
{
        int i;
        char buffer [512];
        HWND lBox;
        int ListPos;

        lBox = fWork->GetCfield ("Liste")->GethWnd ();
        if (lBox == NULL) return;


        ListRows = ::SendMessage (lBox, LB_GETCOUNT, 0, 0l);
        ListPos  = ::SendMessage (lBox, LB_GETCURSEL, 0, 0l);
         
        for (i = 0; i < ListRows; i ++)
        {

            ::SendMessage (lBox, LB_GETTEXT, i, (LPARAM) buffer);
                 if (SelPosOk (buffer)) break;
        }
        if (i == ListPos) return;
        ::SendMessage (lBox, LB_SETCURSEL, (WPARAM) i, (LPARAM) 0l);
}

BOOL DlgLst::OnKeyDown (void)
{
        HWND hWnd;


        hWnd == GetFocus ();

        if (hWnd == fWork->GetCfield ("Liste")->GethWnd ())
        {
            return FALSE;
        }
        if (fWork->GetCfield ("Liste")->GethWnd () == NULL) 
        {
           return FALSE;
        }
        if (WriteRow () == -1)
        {
                 return TRUE;
        }

        syskey = KEYDOWN;
        ::SendMessage (fWork->GetCfield ("Liste")->GethWnd (), WM_KEYDOWN, VK_DOWN, 0l);
        return TRUE;
}


BOOL DlgLst::OnKeyUp (void)
{
        HWND hWnd;


        hWnd == GetFocus ();

        if (hWnd == fWork->GetCfield ("Liste")->GethWnd ())
        {
            return FALSE;
        }
        if (fWork->GetCfield ("Liste")->GethWnd () == NULL) 
        {
           return FALSE;
        }
        if (WriteRow () == -1)
        {
                 return TRUE;
        }

        syskey = KEYDOWN;
        SendMessage (fWork->GetCfield ("Liste")->GethWnd (), WM_KEYDOWN, VK_UP, 0l);
        return TRUE;
}

BOOL DlgLst::OnKey1 ()
{
        syskey = KEY1;
        Help ();
        return TRUE;
}

BOOL DlgLst::OnKey3 ()
{
        syskey = KEY3;
        return TRUE;
}

BOOL DlgLst::OnKey4 ()
{
        syskey = KEY4;
        CallInfo ();
        return TRUE;
}

BOOL DlgLst::OnKey5 ()
{
        InsMode = FALSE;
        syskey = KEY5;
		DestroyWindow ();
        return FALSE;
}

BOOL DlgLst::OnKey12 ()
{
        syskey = KEY12;

        InsMode = FALSE;
        fWork->GetText ();
        FromForm (); 
        return TRUE;
}

BOOL DlgLst::OnKeyDelete ()
{
        
        if (GetKeyState (VK_CONTROL) < 0)
        {
            return OnRowDelete ();
        }
        return FALSE;
}

BOOL DlgLst::OnRowDelete ()
{
        CFIELD *Cfield; 
        

        if (fWork->GetCfield ("Liste")->GethWnd () == NULL) return FALSE;

        ListRows = SendMessage (fWork->GetCfield ("Liste")->GethWnd (), LB_GETCOUNT, 0, 0l);
        if (ListRows == NULL) return FALSE;
/*
        if (abfragejn (hWnd, "Position l�schen ?", "J"))
        {
            DeleteRow ();
        }
*/
        DeleteRow ();
//        Cfield = ActiveDlg->GetCurrentCfield ();
        Cfield = ActiveDlg->GetCurrentCfield ();
        if (Cfield != NULL)
        {
            Cfield->SetFocus ();
        }
        return TRUE;
}

BOOL DlgLst::OnKeyPrior ()
{

        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEUP, 0l);
             return TRUE;
        }
        return FALSE;
}

BOOL DlgLst::OnKeyNext ()
{
        if (vTrack != NULL)
        {
             SendMessage (hWnd, WM_VSCROLL, SB_PAGEDOWN, 0l);
             return TRUE;
        }
        return FALSE;
}


BOOL DlgLst::OnPaint (HWND hWnd,HDC hdc, UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork && this->hWnd == hWnd)
        {
             PrintMess (MessText);
             fWork->display (hWnd, hdc);
             return TRUE;
        }
        return DLG::OnPaint (hWnd,hdc, msg, wParam,lParam);
}


BOOL DlgLst::OnActivate (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (LOWORD (wParam) == WA_ACTIVE ||
                LOWORD (wParam) == WA_CLICKACTIVE)
        {
              if (WithTabStops)
              {
                  SetTabFocus (); 
              }
              else
              {
                  fWork->SetFocus ();
              }
              return TRUE;
        }
        return FALSE;
}

BOOL DlgLst::OnCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        if (fWork)
        {

              if (LOWORD (wParam) == CANCEL_CTL)
              {
                  return OnKey5 ();
              }
              if (LOWORD (wParam) == OK_CTL)
              {
                  return OnKey12 ();
              }

		}
        return DLG::OnCommand (hWnd, msg, wParam, lParam); 
}

BOOL DlgLst::OnSysCommand (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
        
        if (wParam == SC_CLOSE)
        {
            if (abfragejn (hWnd, "Verarbeitung abbrechen ?", "N") == 0)
            {
                ActiveDlg->SetCurrentFocus ();
                return TRUE;
            }
            syskey = KEY5;
        }
        return DLG::OnSysCommand (hWnd, msg, wParam, lParam); 
}

BOOL DlgLst::OnDestroy (HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{

        if (hWnd == this->hWnd)
        {
             PostQuitMessage (0);
        }
        return TRUE;
}


void DlgLst::ToClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          Cfield->GetText ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          hb = GlobalAlloc (GHND, strlen (text) + 1);
          if (hb == NULL) return;
          p = GlobalLock (hb);
          strcpy ((char *) p, text);
          GlobalUnlock (hb);
          OpenClipboard (hWnd);
          EmptyClipboard ();
          SetClipboardData (CF_TEXT, hb);
          CloseClipboard ();
}

void DlgLst::FromClipboard (void)
{
          CFIELD *Cfield;
          HGLOBAL hb;
          LPVOID p;
          char *text;

          Cfield = GetCurrentCfield ();
          text = (char *) Cfield->GetFeld ();
          if (text == NULL) return;
          if (IsClipboardFormatAvailable (CF_TEXT) == FALSE) return;
          OpenClipboard (hWnd);
          hb = GetClipboardData (CF_TEXT);
          if (hb == NULL)
          {
                 CloseClipboard ();
                 return;
          }
          p = GlobalLock (hb);
          if (strlen ((char *) p) > (unsigned int ) Cfield->GetLength ())
          {
                 GlobalUnlock (hb);
                 CloseClipboard ();
                 return;
          }
          strcpy (text, (char *) p);
          GlobalUnlock (hb);
          CloseClipboard ();
          Cfield->SetText ();
}

void DlgLst::Help (void)
{
          CFIELD *Cfield;
          HELP *Help; 
          char *Item;

          Cfield = GetCurrentCfield ();
          if (Cfield == NULL) return;

          Item = Cfield->GetName ();

          Help = new HELP (HelpName, Item, hInstance,hMainWindow);
          delete Help;
          Cfield->SetFocus ();
}


void DlgLst::FillListRows (void)
{
}
           

void DlgLst::FillListCaption (void)
{
}


HWND DlgLst::OpenWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar (TChar);
          hWnd = DLG::OpenWindow (hInstance, hMainWindow);
          FillListCaption ();
          return hWnd;
}

HWND DlgLst::OpenScrollWindow (HINSTANCE hInstance, HWND hMainWindow)
{
          HWND hWnd;

          Settchar (TChar);
          hWnd = DLG::OpenScrollWindow (hInstance, hMainWindow);
          FillListCaption ();
          return hWnd;
}

void DlgLst::PrintComment (char *Name)
{
          HELP *Help; 
          char Comment [256];

          Help = new HELP (HelpName, Name, hInstance,hMainWindow, INITCOMM);
          Help->GetItem (Name, Comment);
          PrintMess (Comment);
          delete Help;
}


void DlgLst::FillFrmTitle (form *frm, char *dest, int diff, int bsize)
{
		char buffer [256];
		int pos, frmlen;
		int plus;
		ColButton *Colbut;
		int i;

		FillMemory (dest, bsize, ' '); 
        frmlen = 0;
        for (i = 0; i < frm->fieldanz; i ++)
        {
			    if (frm->mask[i].attribut & COLBUTTON)
				{
					Colbut = (ColButton *) frm->mask[i].item->GetFeldPtr ();
					if (Colbut != NULL)
					{
						sprintf (buffer, Colbut->text1);
					}
				}
                else
				{
                    ToFormat (buffer, &frm->mask[i]);
				}

                pos = frm->mask[i].pos[1];
                if (frmlen <  pos + (int) strlen (buffer))
                {
                       frmlen = pos + strlen (buffer);
                }
				pos -= diff;
				clipped (buffer);
                plus = max (1, (frm->mask[i].length - strlen (buffer)) / 2); 
				pos += plus;
                memcpy (&dest[pos], buffer,
                           strlen (buffer));
        }
        dest[frmlen] = (char) 0;
}

void DlgLst::FillFrmLines (form *frm, char *dest, int diff, int bsize)
{
		char buffer [256];
		int pos, frmlen;
		int i;

		FillMemory (dest, bsize, ' '); 
        frmlen = 0;
        for (i = 0; i < frm->fieldanz; i ++)
        {
                ToFormat (buffer, &frm->mask[i]);
                pos = frm->mask[i].pos[1];
                if (frmlen <  pos + (int) strlen (buffer))
                {
                       frmlen = pos + strlen (buffer);
                }
				pos -= diff;
                memcpy (&dest[pos], buffer,
                           strlen (buffer));
        }
        dest[frmlen] = (char) 0;
}

void DlgLst::FillFrmPos (form *frm, char *dest, int diff, int bsize)
{
		char buffer [256];
		int pos;
		int i;

		FillMemory (dest, bsize, ' '); 
        for (i = 0; i < frm->fieldanz; i ++)
        {
			    if (frm->mask[i].pos[0] != 0) continue;
			    if (frm->mask[i].attribut & BUTTON) continue;

			    pos = frm->mask[i].pos[1];
				pos -= diff;
				sprintf (buffer, "%d;%d", pos, frm->mask[i].length);
				if (i == 0)
				{
					strcpy (dest, buffer);
				}
				else
				{
					strcat (dest, " ");
					strcat (dest, buffer);
				}
        }
}

void DlgLst::FillFrmAttr (form *frm, char *dest, int bsize)
{
		char buffer [256];
		int i;

		FillMemory (dest, bsize, ' '); 
        for (i = 0; i < frm->fieldanz; i ++)
        {

			    if (frm->mask[i].pos[0] != 0) continue;
			    if (frm->mask[i].attribut & BUTTON) continue;

				if (frm->mask[i].picture[0] == '%' &&
					strchr (frm->mask[i].picture, 'd'))
				{
					strcpy (buffer, "%d");
				}
				else if (frm->mask[i].picture[0] == '%' &&
					     strchr (frm->mask[i].picture, 'f'))
				{
					strcpy (buffer, "%f");
				}
				else
				{
					strcpy (buffer, "%s");
				}

				if (i == 0)
				{
					strcpy (dest, buffer);
				}
				else
				{
					strcat (dest, " ");
					strcat (dest, buffer);
				}
        }
}



void DlgLst::FillFrmData (form *datafrm, char *dest, int diff, int bsize)
{
		char buffer [256];
		int pos, frmlen;
		int i;

		FillMemory (dest, bsize, ' '); 
        frmlen = 0;
        for (i = 0; i < datafrm->fieldanz; i ++)
        {
			    if (datafrm->mask[i].pos[0] != 0) continue;
			    if (datafrm->mask[i].attribut & BUTTON) continue;
                ToFormat (buffer, &datafrm->mask[i]);
                pos = datafrm->mask[i].pos[1];
                if (frmlen <  pos + (int) strlen (buffer))
                {
                       frmlen = pos + strlen (buffer);
                }
				pos -= diff;
				pos = AddSepAnz (dest, pos);
                if (i > 0 && dest[pos - 1] != ' ')
                {
                       dest[pos] = ' ';
                       pos ++;
                }
				if (datafrm->mask[i].picture[0] == 0 ||
					datafrm->mask[i].picture[0] == ' ')
				{
 			           dest[pos] = TChar;
					   pos ++;
				}
                memcpy (&dest[pos], buffer,
                           strlen (buffer));
				if (datafrm->mask[i].picture[0] == 0 ||
					datafrm->mask[i].picture[0] == ' ')
				{
					   pos += datafrm->mask[i].length;
				       dest[pos] = TChar;
				}
        }
        dest[pos + 1] = (char) 0;
}

int DlgLst::NextFieldPos (form * frm, int pos, int i, int diff)
{
	    int idx;

		idx = i;
        for (i = i + 1; i < frm->fieldanz; i ++)
		{
			if (frm->mask[i].pos[0] == 0)
			{
				pos = frm->mask[i].pos[1] - 1;
				return pos - diff;
			}
		}
		return pos + frm->mask[idx].length;
}

int DlgLst::AddSepAnz (char *dest, int pos)
{
	    int anz;
		int i;

		anz = 0;

		for (i = 0; i < pos; i ++)
		{
			if ((UCHAR) dest[i] == TChar) anz ++;
		}
		return pos + anz;
}

