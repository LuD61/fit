#include "RPen.h"

RPen::RPen ()
{
	style = 0;
	width = 0;
	color = NULL;
	hPen = NULL;
	Instances = 0;
}

RPen::RPen (int style, int width, COLORREF color,  HPEN hPen)
{
	this->style = style;
	this->width = width;
	this->hPen  = hPen;
	this->color = color;
	Instances   = 0;
}


RPen::~RPen ()
{
}

BOOL RPen::operator== (RPen& RPen)
{
	if (hPen == NULL)
	{
		return FALSE;
	}
	if (RPen.GetStyle () != style)
	{
		return FALSE;
	}
	if (RPen.GetWidth () != width)
	{
		return FALSE;
	}
	if (RPen.GetColor () != color)
	{
		return FALSE;
	}
	return TRUE;
}

RPen& RPen::operator=(RPen& RPen)
{
	style = RPen.GetStyle ();
	width = RPen.GetWidth ();
	color = RPen.GetColor ();
	return *this;
}


void RPen::SetStyle (int style)
{
	this->style = style;
}

int RPen::GetStyle ()
{
	return color;
}

void RPen::SetWidth (int width)
{
	this->width = width;
}

int RPen::GetWidth ()
{
	return width;
}

void RPen::SetColor (COLORREF color)
{
	this->color = color;
}

COLORREF RPen::GetColor ()
{
	return color;
}


void RPen::SetPen (HPEN hPen)
{
	this->hPen = hPen;
}

HBRUSH RPen::GetPen ()
{
	return hPen;
}

void RPen::inc ()
{
	Instances ++;
}

void RPen::dec ()
{
	if (Instances > 0)
	{
	     Instances --;
	}
}

int RPen::GetInstances ()
{
	return Instances;
}