#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "akt_iprz.h"

struct AKT_IPRZ akt_iprz, akt_iprz_null;

void AKT_IPRZ_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &akt_iprz.mdn, 2, 0);
            ins_quest ((char *) &akt_iprz.a, 3, 0);
            ins_quest ((char *) &akt_iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_iprz.kun_pr, 2, 0);
            ins_quest ((char *) &akt_iprz.kun, 2, 0);
            ins_quest ((char *) &akt_iprz.aki_von, 2, 0);
    out_quest ((char *) &akt_iprz.mdn,2,0);
    out_quest ((char *) &akt_iprz.pr_gr_stuf,2,0);
    out_quest ((char *) &akt_iprz.kun_pr,2,0);
    out_quest ((char *) &akt_iprz.a,3,0);
    out_quest ((char *) &akt_iprz.aki_pr,3,0);
    out_quest ((char *) &akt_iprz.aki_von,2,0);
    out_quest ((char *) &akt_iprz.aki_bis,2,0);
    out_quest ((char *) &akt_iprz.ld_pr,3,0);
    out_quest ((char *) &akt_iprz.aktion_nr,1,0);
    out_quest ((char *) akt_iprz.a_akt_kz,0,2);
    out_quest ((char *) akt_iprz.modif,0,2);
    out_quest ((char *) &akt_iprz.waehrung,1,0);
    out_quest ((char *) &akt_iprz.kun,2,0);
    out_quest ((char *) &akt_iprz.a_grund,3,0);
    out_quest ((char *) akt_iprz.kond_art,0,5);
            cursor = prepare_sql ("select akt_iprz.mdn,  "
"akt_iprz.pr_gr_stuf,  akt_iprz.kun_pr,  akt_iprz.a,  akt_iprz.aki_pr,  "
"akt_iprz.aki_von,  akt_iprz.aki_bis,  akt_iprz.ld_pr,  "
"akt_iprz.aktion_nr,  akt_iprz.a_akt_kz,  akt_iprz.modif,  "
"akt_iprz.waehrung,  akt_iprz.kun,  akt_iprz.a_grund,  "
"akt_iprz.kond_art from akt_iprz "

#line 30 "akt_iprz.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ? "
                                  "and aki_von = ?"); 



    ins_quest ((char *) &akt_iprz.mdn,2,0);
    ins_quest ((char *) &akt_iprz.pr_gr_stuf,2,0);
    ins_quest ((char *) &akt_iprz.kun_pr,2,0);
    ins_quest ((char *) &akt_iprz.a,3,0);
    ins_quest ((char *) &akt_iprz.aki_pr,3,0);
    ins_quest ((char *) &akt_iprz.aki_von,2,0);
    ins_quest ((char *) &akt_iprz.aki_bis,2,0);
    ins_quest ((char *) &akt_iprz.ld_pr,3,0);
    ins_quest ((char *) &akt_iprz.aktion_nr,1,0);
    ins_quest ((char *) akt_iprz.a_akt_kz,0,2);
    ins_quest ((char *) akt_iprz.modif,0,2);
    ins_quest ((char *) &akt_iprz.waehrung,1,0);
    ins_quest ((char *) &akt_iprz.kun,2,0);
    ins_quest ((char *) &akt_iprz.a_grund,3,0);
    ins_quest ((char *) akt_iprz.kond_art,0,5);
            sqltext = "update akt_iprz set akt_iprz.mdn = ?,  "
"akt_iprz.pr_gr_stuf = ?,  akt_iprz.kun_pr = ?,  akt_iprz.a = ?,  "
"akt_iprz.aki_pr = ?,  akt_iprz.aki_von = ?,  akt_iprz.aki_bis = ?,  "
"akt_iprz.ld_pr = ?,  akt_iprz.aktion_nr = ?,  "
"akt_iprz.a_akt_kz = ?,  akt_iprz.modif = ?,  akt_iprz.waehrung = ?,  "
"akt_iprz.kun = ?,  akt_iprz.a_grund = ?,  akt_iprz.kond_art = ? "

#line 40 "akt_iprz.rpp"
                               "where mdn = ? "
                               "and   a = ? "
                               "and   pr_gr_stuf = ? "
                               "and   kun_pr = ? "
                               "and   kun = ? "
                               "and aki_von = ?";
  
            ins_quest ((char *) &akt_iprz.mdn, 2, 0);
            ins_quest ((char *) &akt_iprz.a, 3, 0);
            ins_quest ((char *) &akt_iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_iprz.kun_pr,  2, 0);  
            ins_quest ((char *) &akt_iprz.kun,  2, 0);  
            ins_quest ((char *) &akt_iprz.aki_von, 2, 0);

            upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &akt_iprz.mdn,2,0);
    ins_quest ((char *) &akt_iprz.pr_gr_stuf,2,0);
    ins_quest ((char *) &akt_iprz.kun_pr,2,0);
    ins_quest ((char *) &akt_iprz.a,3,0);
    ins_quest ((char *) &akt_iprz.aki_pr,3,0);
    ins_quest ((char *) &akt_iprz.aki_von,2,0);
    ins_quest ((char *) &akt_iprz.aki_bis,2,0);
    ins_quest ((char *) &akt_iprz.ld_pr,3,0);
    ins_quest ((char *) &akt_iprz.aktion_nr,1,0);
    ins_quest ((char *) akt_iprz.a_akt_kz,0,2);
    ins_quest ((char *) akt_iprz.modif,0,2);
    ins_quest ((char *) &akt_iprz.waehrung,1,0);
    ins_quest ((char *) &akt_iprz.kun,2,0);
    ins_quest ((char *) &akt_iprz.a_grund,3,0);
    ins_quest ((char *) akt_iprz.kond_art,0,5);
            ins_cursor = prepare_sql ("insert into akt_iprz ("
"mdn,  pr_gr_stuf,  kun_pr,  a,  aki_pr,  aki_von,  aki_bis,  ld_pr,  aktion_nr,  "
"a_akt_kz,  modif,  waehrung,  kun,  a_grund,  kond_art) "

#line 57 "akt_iprz.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?)"); 

#line 59 "akt_iprz.rpp"
            ins_quest ((char *) &akt_iprz.mdn, 2, 0);
            ins_quest ((char *) &akt_iprz.a, 3, 0);
            ins_quest ((char *) &akt_iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_iprz.kun_pr,  2, 0);  
            ins_quest ((char *) &akt_iprz.kun,  2, 0);  
            ins_quest ((char *) &akt_iprz.aki_von, 2, 0);
            test_upd_cursor = prepare_sql ("select a from akt_iprz "
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ? "
                                  "and aki_von = ?"); 
              
            ins_quest ((char *) &akt_iprz.mdn, 2, 0);
            ins_quest ((char *) &akt_iprz.a, 3, 0);
            ins_quest ((char *) &akt_iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_iprz.kun_pr,  2, 0);  
            ins_quest ((char *) &akt_iprz.kun,  2, 0);  
            del_cursor = prepare_sql ("delete from akt_iprz "
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ?"); 
}

void AKT_IPRZ_CLASS::prepare_a (char *order)
{
            ins_quest ((char *) &akt_iprz.mdn, 2, 0);
            ins_quest ((char *) &akt_iprz.a, 3, 0);
            if (order)
            {
    out_quest ((char *) &akt_iprz.mdn,2,0);
    out_quest ((char *) &akt_iprz.pr_gr_stuf,2,0);
    out_quest ((char *) &akt_iprz.kun_pr,2,0);
    out_quest ((char *) &akt_iprz.a,3,0);
    out_quest ((char *) &akt_iprz.aki_pr,3,0);
    out_quest ((char *) &akt_iprz.aki_von,2,0);
    out_quest ((char *) &akt_iprz.aki_bis,2,0);
    out_quest ((char *) &akt_iprz.ld_pr,3,0);
    out_quest ((char *) &akt_iprz.aktion_nr,1,0);
    out_quest ((char *) akt_iprz.a_akt_kz,0,2);
    out_quest ((char *) akt_iprz.modif,0,2);
    out_quest ((char *) &akt_iprz.waehrung,1,0);
    out_quest ((char *) &akt_iprz.kun,2,0);
    out_quest ((char *) &akt_iprz.a_grund,3,0);
    out_quest ((char *) akt_iprz.kond_art,0,5);
                        cursor_a = prepare_sql ("select "
"akt_iprz.mdn,  akt_iprz.pr_gr_stuf,  akt_iprz.kun_pr,  akt_iprz.a,  "
"akt_iprz.aki_pr,  akt_iprz.aki_von,  akt_iprz.aki_bis,  "
"akt_iprz.ld_pr,  akt_iprz.aktion_nr,  akt_iprz.a_akt_kz,  "
"akt_iprz.modif,  akt_iprz.waehrung,  akt_iprz.kun,  akt_iprz.a_grund,  "
"akt_iprz.kond_art from akt_iprz "

#line 93 "akt_iprz.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "%s", order);
            }
            else
            {
    out_quest ((char *) &akt_iprz.mdn,2,0);
    out_quest ((char *) &akt_iprz.pr_gr_stuf,2,0);
    out_quest ((char *) &akt_iprz.kun_pr,2,0);
    out_quest ((char *) &akt_iprz.a,3,0);
    out_quest ((char *) &akt_iprz.aki_pr,3,0);
    out_quest ((char *) &akt_iprz.aki_von,2,0);
    out_quest ((char *) &akt_iprz.aki_bis,2,0);
    out_quest ((char *) &akt_iprz.ld_pr,3,0);
    out_quest ((char *) &akt_iprz.aktion_nr,1,0);
    out_quest ((char *) akt_iprz.a_akt_kz,0,2);
    out_quest ((char *) akt_iprz.modif,0,2);
    out_quest ((char *) &akt_iprz.waehrung,1,0);
    out_quest ((char *) &akt_iprz.kun,2,0);
    out_quest ((char *) &akt_iprz.a_grund,3,0);
    out_quest ((char *) akt_iprz.kond_art,0,5);
                        cursor_a = prepare_sql ("select "
"akt_iprz.mdn,  akt_iprz.pr_gr_stuf,  akt_iprz.kun_pr,  akt_iprz.a,  "
"akt_iprz.aki_pr,  akt_iprz.aki_von,  akt_iprz.aki_bis,  "
"akt_iprz.ld_pr,  akt_iprz.aktion_nr,  akt_iprz.a_akt_kz,  "
"akt_iprz.modif,  akt_iprz.waehrung,  akt_iprz.kun,  akt_iprz.a_grund,  "
"akt_iprz.kond_art from akt_iprz "

#line 100 "akt_iprz.rpp"
                                  "where mdn = ? "
                                  "and   a   = ?");
            }
            ins_quest ((char *) &akt_iprz.mdn, 2, 0);
            ins_quest ((char *) &akt_iprz.a, 3, 0);
            del_a_curs = prepare_sql ("delete from akt_iprz "
                                  "where mdn = ? "
                                  "and   a   = ?");
}

void AKT_IPRZ_CLASS::prepare_gr (char *order)
{
            ins_quest ((char *) &akt_iprz.mdn, 2, 0);
            ins_quest ((char *) &akt_iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_iprz.kun_pr, 2, 0);
            if (order)
            {
    out_quest ((char *) &akt_iprz.mdn,2,0);
    out_quest ((char *) &akt_iprz.pr_gr_stuf,2,0);
    out_quest ((char *) &akt_iprz.kun_pr,2,0);
    out_quest ((char *) &akt_iprz.a,3,0);
    out_quest ((char *) &akt_iprz.aki_pr,3,0);
    out_quest ((char *) &akt_iprz.aki_von,2,0);
    out_quest ((char *) &akt_iprz.aki_bis,2,0);
    out_quest ((char *) &akt_iprz.ld_pr,3,0);
    out_quest ((char *) &akt_iprz.aktion_nr,1,0);
    out_quest ((char *) akt_iprz.a_akt_kz,0,2);
    out_quest ((char *) akt_iprz.modif,0,2);
    out_quest ((char *) &akt_iprz.waehrung,1,0);
    out_quest ((char *) &akt_iprz.kun,2,0);
    out_quest ((char *) &akt_iprz.a_grund,3,0);
    out_quest ((char *) akt_iprz.kond_art,0,5);
                        cursor_gr = prepare_sql ("select "
"akt_iprz.mdn,  akt_iprz.pr_gr_stuf,  akt_iprz.kun_pr,  akt_iprz.a,  "
"akt_iprz.aki_pr,  akt_iprz.aki_von,  akt_iprz.aki_bis,  "
"akt_iprz.ld_pr,  akt_iprz.aktion_nr,  akt_iprz.a_akt_kz,  "
"akt_iprz.modif,  akt_iprz.waehrung,  akt_iprz.kun,  akt_iprz.a_grund,  "
"akt_iprz.kond_art from akt_iprz "

#line 118 "akt_iprz.rpp"
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ? "
                                  "%s", order);
            }
            else
            {
    out_quest ((char *) &akt_iprz.mdn,2,0);
    out_quest ((char *) &akt_iprz.pr_gr_stuf,2,0);
    out_quest ((char *) &akt_iprz.kun_pr,2,0);
    out_quest ((char *) &akt_iprz.a,3,0);
    out_quest ((char *) &akt_iprz.aki_pr,3,0);
    out_quest ((char *) &akt_iprz.aki_von,2,0);
    out_quest ((char *) &akt_iprz.aki_bis,2,0);
    out_quest ((char *) &akt_iprz.ld_pr,3,0);
    out_quest ((char *) &akt_iprz.aktion_nr,1,0);
    out_quest ((char *) akt_iprz.a_akt_kz,0,2);
    out_quest ((char *) akt_iprz.modif,0,2);
    out_quest ((char *) &akt_iprz.waehrung,1,0);
    out_quest ((char *) &akt_iprz.kun,2,0);
    out_quest ((char *) &akt_iprz.a_grund,3,0);
    out_quest ((char *) akt_iprz.kond_art,0,5);
                        cursor_gr = prepare_sql ("select "
"akt_iprz.mdn,  akt_iprz.pr_gr_stuf,  akt_iprz.kun_pr,  akt_iprz.a,  "
"akt_iprz.aki_pr,  akt_iprz.aki_von,  akt_iprz.aki_bis,  "
"akt_iprz.ld_pr,  akt_iprz.aktion_nr,  akt_iprz.a_akt_kz,  "
"akt_iprz.modif,  akt_iprz.waehrung,  akt_iprz.kun,  akt_iprz.a_grund,  "
"akt_iprz.kond_art from akt_iprz "

#line 126 "akt_iprz.rpp"
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ?");
            }
            ins_quest ((char *) &akt_iprz.mdn, 2, 0);
            ins_quest ((char *) &akt_iprz.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_iprz.kun_pr, 2, 0);
            del_gr_curs = prepare_sql ("delete from akt_iprz "
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ?");
}

int AKT_IPRZ_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int AKT_IPRZ_CLASS::dbreadfirst_a (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_a == -1)
         {
                this->prepare_a (order);
         }
         open_sql (cursor_a);
         return fetch_sql (cursor_a);
}

int AKT_IPRZ_CLASS::dbread_a (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         return fetch_sql (cursor_a);
}

int AKT_IPRZ_CLASS::dbreadfirst_gr (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_gr == -1)
         {
                this->prepare_gr (order);
         }
         open_sql (cursor_gr);
         return fetch_sql (cursor_gr);
}

int AKT_IPRZ_CLASS::dbread_gr (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         return fetch_sql (cursor_gr);
}

int AKT_IPRZ_CLASS::dbdelete_gr (void)
{
         if (del_gr_curs == -1)
         {
                this->prepare_a (NULL);
         }
         return execute_curs (del_gr_curs);
}
       

int AKT_IPRZ_CLASS::dbdelete_a (void)
{
         if (del_a_curs == -1)
         {
                this->prepare_a (NULL);
         }
         return execute_curs (del_a_curs);
}
       


int AKT_IPRZ_CLASS::dbclose_a (void)
{
        if (cursor_a != -1)
        { 
                 close_sql (cursor_a);
                 cursor_a = -1;
        }
        return 0;
}


int AKT_IPRZ_CLASS::dbclose_gr (void)
{
        if (cursor_gr != -1)
        { 
                 close_sql (cursor_gr);
                 cursor_gr = -1;
        }
        return 0;
}

