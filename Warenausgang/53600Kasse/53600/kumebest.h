#ifndef _KMBDEF
#define _KMBDEF
#include "dbclass.h"

struct KUMEBEST {
   short     mdn;
   short     fil;
   long      kun;
   char      kun_bran2[3];
   double    a;
   double    tara0;
   short     me_einh0;
   double    inh0;
   short     me_einh1;
   double    inh1;
   double    tara1;
   short     me_einh2;
   double    inh2;
   double    tara2;
   short     me_einh3;
   double    inh3;
   double    tara3;
   short     me_einh4;
   double    inh4;
   double    tara4;
   short     me_einh5;
   double    inh5;
   double    tara5;
   short     me_einh6;
   double    inh6;
   double    tara6;
   short     knuepf1;
   short     knuepf2;
   short     knuepf3;
   short     knuepf4;
   short     knuepf5;
   short     knuepf6;
};
extern struct KUMEBEST kumebest, kumebest_null;

#line 6 "kumebest.rh"

class KMB_CLASS : DB_CLASS
{
       private :
               void prepare (void);
               int cursor_bra;
               int cursor_a;
       public :
               KMB_CLASS () : DB_CLASS ()
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_bra(void);
               int dbread_bra (void);
               int dbreadfirst_a(void);
               int dbread_a (void);
};
#endif
