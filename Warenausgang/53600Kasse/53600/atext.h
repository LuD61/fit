#ifndef ATEXTE_DEF
#define ATEXTE_DEF
#include "dbclass.h"

struct ATEXTE {
   long      sys;
   short     waa;
   long      txt_nr;
   char      txt[1025];
   char      disp_txt[1025];
   short     alignment;
   short     send_ctrl;
   short     txt_typ;
   short     txt_platz;
   double    a;
};
extern struct ATEXTE atexte, atexte_null;

#line 6 "atext.rh"
class ATEXTE_CLASS
{
       private:
                int cursor_atexte;
                void prepare_atexte (void);
       public:
                ATEXTE_CLASS ()
                {
                   cursor_atexte = -1;
                }
	        int lese_atexte (long);
};

#endif
