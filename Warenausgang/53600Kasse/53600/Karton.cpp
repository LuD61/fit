#include "Karton.h"
#include "strfkt.h"
#include "mo_curso.h"

Karton::Karton ()
{
	mdn = 0;
	fil = 0;
	ls = 0l;
	a = 0.0;
	nve = "";
	nve_palette = "";
	char datum [12];
	sysdate (datum);
	mhd = dasc_to_long (datum);
	mhd = 0l;
}

Karton::~Karton ()
{
}


void Karton::SetMdn (short mdn)
{
	this->mdn = mdn;
}

short Karton::GetMdn ()
{
	return mdn;
}

void Karton::SetFil (short fil)
{
	this->fil = fil;
}

short Karton::GetFil ()
{
	return fil;
}

void Karton::SetLs (long ls)
{
	this->ls = ls;
}

long Karton::GetLs ()
{
	return ls;
}

void Karton::SetA (double a)
{
	this->a = a;
}

double Karton::GetA ()
{
	return a;
}

void Karton::SetGew (double gew)
{
	this->gew = gew;
}

double Karton::GetGew ()
{
	return gew;
}

void Karton::SetMeEinh (short me_einh)
{
	this->me_einh = me_einh;
}

short Karton::GetMeEinh ()
{
	return me_einh;
}


long Karton::GetMhd ()
{
	return mhd;
}

void Karton::SetMhd (LPSTR mhd)
{
	this->mhd = dasc_to_long (mhd);
}

void Karton::SetNve (Text& nve)
{
	this->nve = nve;
}

void Karton::SetNve (char *nve)
{
	this->nve = nve;
}

Text& Karton::GetNve ()
{
	return nve;
}

void Karton::SetNvePalette (Text& nve_palette)
{
	this->nve_palette = nve_palette;
}

void Karton::SetNvePalette (char *nve_palette)
{
	this->nve_palette = nve_palette;
}

Text& Karton::GetNvePalette ()
{
	return nve_palette;
}

BOOL Karton::Equals (Karton &karton)
{
	if (karton.GetMdn () != mdn)
	{
		return FALSE;
	}

	if (karton.GetFil () != fil)
	{
		return FALSE;
	}

	if (karton.GetLs () != ls)
	{
		return FALSE;
	}

	if (karton.GetA () != a)
	{
		return FALSE;
	}

	if (karton.GetNve () != nve)
	{
		return FALSE;
	}

	if (karton.GetNvePalette () != nve_palette)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL Karton::operator== (Karton &karton)
{
	return Equals (karton); 
}
