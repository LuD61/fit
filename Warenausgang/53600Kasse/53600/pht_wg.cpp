#include <windows.h>
#include "mo_curso.h"
#include "pht_wg.h"

struct PHT_WG pht_wg, pht_wg_null;

void PHT_WG_CLASS::prepare (void)
{
    ins_quest ((char *) &pht_wg.sys, 2, 0);
    out_quest ((char *) &pht_wg.mdn,1,0);
    out_quest ((char *) &pht_wg.fil,1,0);
    out_quest ((char *) &pht_wg.sys,2,0);
    out_quest ((char *) &pht_wg.prio_kng,1,0);
    out_quest ((char *) &pht_wg.wg,1,0);
    out_quest ((char *) pht_wg.wg_bz1,0,25);
    out_quest ((char *) &pht_wg.zuord_dat,2,0);
    out_quest ((char *) &pht_wg.mf_zuord,1,0);
        cursor = prepare_sql ("select pht_wg.mdn,  pht_wg.fil,  "
"pht_wg.sys,  pht_wg.prio_kng,  pht_wg.wg,  pht_wg.wg_bz1,  "
"pht_wg.zuord_dat,  pht_wg.mf_zuord from pht_wg "
                              "where sys = ?");

    ins_quest ((char *) &pht_wg.sys, 2, 0);
    ins_quest ((char *) &pht_wg.wg,1,0);
    out_quest ((char *) &pht_wg.mdn,1,0);
    out_quest ((char *) &pht_wg.fil,1,0);
    out_quest ((char *) &pht_wg.sys,2,0);
    out_quest ((char *) &pht_wg.prio_kng,1,0);
    out_quest ((char *) &pht_wg.wg,1,0);
    out_quest ((char *) pht_wg.wg_bz1,0,25);
    out_quest ((char *) &pht_wg.zuord_dat,2,0);
    out_quest ((char *) &pht_wg.mf_zuord,1,0);
        cursor_wg = prepare_sql ("select pht_wg.mdn,  pht_wg.fil,  "
"pht_wg.sys,  pht_wg.prio_kng,  pht_wg.wg,  pht_wg.wg_bz1,  "
"pht_wg.zuord_dat,  pht_wg.mf_zuord from pht_wg "
                              "where sys = ? "
							  "and wg = ?");
}

int PHT_WG_CLASS::dbreadfirst (void)
{
        if (cursor == -1)
        {
                    this->prepare ();
        }
        open_sql (cursor);
        if (sqlstatus) return (sqlstatus);
        return (fetch_sql (cursor));
}

int PHT_WG_CLASS::dbread (void)
{
        return (fetch_sql (cursor));
}


int PHT_WG_CLASS::dbreadfirst_wg (void)
{
        if (cursor_wg == -1)
        {
                    this->prepare ();
        }
        open_sql (cursor_wg);
        if (sqlstatus) return (sqlstatus);
        return (fetch_sql (cursor_wg));
}

int PHT_WG_CLASS::dbread_wg (void)
{
        return (fetch_sql (cursor_wg));
}

 