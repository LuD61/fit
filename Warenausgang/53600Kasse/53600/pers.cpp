#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmaskc.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "pers.h"

struct PERS pers, pers_null;

void PERS_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &pers.mdn, 1, 0);
            ins_quest ((char *) &pers.fil, 1, 0);     
            ins_quest ((char *) pers.pers, 0, sizeof (pers.pers));
    out_quest ((char *) &pers.abt,1,0);
    out_quest ((char *) &pers.adr,2,0);
    out_quest ((char *) &pers.anz_tag_wo,1,0);
    out_quest ((char *) &pers.arb_zeit_tag,3,0);
    out_quest ((char *) &pers.arb_zeit_wo,3,0);
    out_quest ((char *) pers.beschaef_kz,0,2);
    out_quest ((char *) &pers.eintr_dat,2,0);
    out_quest ((char *) &pers.fil,1,0);
    out_quest ((char *) &pers.kost_st,1,0);
    out_quest ((char *) &pers.mdn,1,0);
    out_quest ((char *) pers.pers,0,13);
    out_quest ((char *) &pers.pers_mde,2,0);
    out_quest ((char *) &pers.pers_pkt,3,0);
    out_quest ((char *) &pers.rab_proz,3,0);
    out_quest ((char *) &pers.std_satz,3,0);
    out_quest ((char *) pers.taet,0,25);
    out_quest ((char *) pers.taet_kz,0,2);
    out_quest ((char *) &pers.tar_gr,1,0);
    out_quest ((char *) pers.zahlw,0,2);
    out_quest ((char *) &pers.txt_nr,2,0);
    out_quest ((char *) &pers.delstatus,1,0);
            cursor = prepare_sql ("select pers.abt,  pers.adr,  "
"pers.anz_tag_wo,  pers.arb_zeit_tag,  pers.arb_zeit_wo,  "
"pers.beschaef_kz,  pers.eintr_dat,  pers.fil,  pers.kost_st,  pers.mdn,  "
"pers.pers,  pers.pers_mde,  pers.pers_pkt,  pers.rab_proz,  "
"pers.std_satz,  pers.taet,  pers.taet_kz,  pers.tar_gr,  pers.zahlw,  "
"pers.txt_nr,  pers.delstatus from pers "

#line 27 "pers.rpp"
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   pers = ?");


    ins_quest ((char *) &pers.abt,1,0);
    ins_quest ((char *) &pers.adr,2,0);
    ins_quest ((char *) &pers.anz_tag_wo,1,0);
    ins_quest ((char *) &pers.arb_zeit_tag,3,0);
    ins_quest ((char *) &pers.arb_zeit_wo,3,0);
    ins_quest ((char *) pers.beschaef_kz,0,2);
    ins_quest ((char *) &pers.eintr_dat,2,0);
    ins_quest ((char *) &pers.fil,1,0);
    ins_quest ((char *) &pers.kost_st,1,0);
    ins_quest ((char *) &pers.mdn,1,0);
    ins_quest ((char *) pers.pers,0,13);
    ins_quest ((char *) &pers.pers_mde,2,0);
    ins_quest ((char *) &pers.pers_pkt,3,0);
    ins_quest ((char *) &pers.rab_proz,3,0);
    ins_quest ((char *) &pers.std_satz,3,0);
    ins_quest ((char *) pers.taet,0,25);
    ins_quest ((char *) pers.taet_kz,0,2);
    ins_quest ((char *) &pers.tar_gr,1,0);
    ins_quest ((char *) pers.zahlw,0,2);
    ins_quest ((char *) &pers.txt_nr,2,0);
    ins_quest ((char *) &pers.delstatus,1,0);
            sqltext = "update pers set pers.abt = ?,  "
"pers.adr = ?,  pers.anz_tag_wo = ?,  pers.arb_zeit_tag = ?,  "
"pers.arb_zeit_wo = ?,  pers.beschaef_kz = ?,  pers.eintr_dat = ?,  "
"pers.fil = ?,  pers.kost_st = ?,  pers.mdn = ?,  pers.pers = ?,  "
"pers.pers_mde = ?,  pers.pers_pkt = ?,  pers.rab_proz = ?,  "
"pers.std_satz = ?,  pers.taet = ?,  pers.taet_kz = ?,  "
"pers.tar_gr = ?,  pers.zahlw = ?,  pers.txt_nr = ?,  "
"pers.delstatus = ? "

#line 33 "pers.rpp"
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   pers = ?";
  
            ins_quest ((char *) &pers.mdn, 1, 0);
            ins_quest ((char *) &pers.fil, 1, 0);     
            ins_quest ((char *) pers.pers, 0, sizeof (pers.pers));

            upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &pers.abt,1,0);
    ins_quest ((char *) &pers.adr,2,0);
    ins_quest ((char *) &pers.anz_tag_wo,1,0);
    ins_quest ((char *) &pers.arb_zeit_tag,3,0);
    ins_quest ((char *) &pers.arb_zeit_wo,3,0);
    ins_quest ((char *) pers.beschaef_kz,0,2);
    ins_quest ((char *) &pers.eintr_dat,2,0);
    ins_quest ((char *) &pers.fil,1,0);
    ins_quest ((char *) &pers.kost_st,1,0);
    ins_quest ((char *) &pers.mdn,1,0);
    ins_quest ((char *) pers.pers,0,13);
    ins_quest ((char *) &pers.pers_mde,2,0);
    ins_quest ((char *) &pers.pers_pkt,3,0);
    ins_quest ((char *) &pers.rab_proz,3,0);
    ins_quest ((char *) &pers.std_satz,3,0);
    ins_quest ((char *) pers.taet,0,25);
    ins_quest ((char *) pers.taet_kz,0,2);
    ins_quest ((char *) &pers.tar_gr,1,0);
    ins_quest ((char *) pers.zahlw,0,2);
    ins_quest ((char *) &pers.txt_nr,2,0);
    ins_quest ((char *) &pers.delstatus,1,0);
            ins_cursor = prepare_sql ("insert into pers (abt,  "
"adr,  anz_tag_wo,  arb_zeit_tag,  arb_zeit_wo,  beschaef_kz,  eintr_dat,  fil,  "
"kost_st,  mdn,  pers,  pers_mde,  pers_pkt,  rab_proz,  std_satz,  taet,  taet_kz,  "
"tar_gr,  zahlw,  txt_nr,  delstatus) "

#line 44 "pers.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?)"); 

#line 46 "pers.rpp"
            ins_quest ((char *) &pers.mdn, 1, 0);
            ins_quest ((char *) &pers.fil, 1, 0);     
            ins_quest ((char *) pers.pers, 0, sizeof (pers.pers));
            test_upd_cursor = prepare_sql ("select pers from pers "
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   pers = ?");
              
            ins_quest ((char *) &pers.mdn, 1, 0);
            ins_quest ((char *) &pers.fil, 1, 0);     
            ins_quest ((char *) pers.pers, 0, sizeof (pers.pers));
            del_cursor = prepare_sql ("delete from pers "
                                  "where mdn = ? "
                                  "and   fil   = ? "
                                  "and   pers = ?");
}

