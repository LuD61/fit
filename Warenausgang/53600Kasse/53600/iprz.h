#ifndef _IPRZ_DEF
#define _IPRZ_DEF

#include "dbclass.h"

struct IPRZ {
   long      mdn;
   long      pr_gr_stuf;
   long      kun_pr;
   double    a;
   double    vk_pr_i;
   double    ld_pr;
   short     aktion_nr;
   char      a_akt_kz[2];
   char      modif[2];
   short     waehrung;
   long      kun;
   double    a_grund;
   char      kond_art[5];
};
extern struct IPRZ iprz, iprz_null;

#line 7 "iprz.rh"

class IPRZ_CLASS : public DB_CLASS 
{
       private :
               int cursor_a;
               int cursor_gr;
               int del_a_curs;
               int del_gr_curs; 
               void prepare (void);
               void prepare_a (char *);
               void prepare_gr (char *);
       public :
               IPRZ_CLASS () : DB_CLASS (),
                                  cursor_a (-1) ,
                                  cursor_gr (-1) ,
                                  del_a_curs (-1) ,
                                  del_gr_curs (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_a (char *);
               int dbread_a (void);
               int dbreadfirst_gr (char *);
               int dbread_gr (void);
               int dbdelete_a (void);
               int dbdelete_gr (void);
               int dbclose_a (void);
               int dbclose_gr (void);
};
#endif
