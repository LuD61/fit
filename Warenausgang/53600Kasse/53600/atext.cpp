#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <tchar.h>
#include "wmask.h"
#include "strfkt.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "atext.h"

struct ATEXTE atexte, atexte_null;

void ATEXTE_CLASS::prepare_atexte (void)
{
    ins_quest ((char *) &atexte.txt_nr, 2, 0);
    out_quest ((char *) &atexte.sys,2,0);
    out_quest ((char *) &atexte.waa,1,0);
    out_quest ((char *) &atexte.txt_nr,2,0);
    out_quest ((char *) atexte.txt,0,1025);
    out_quest ((char *) atexte.disp_txt,0,1025);
    out_quest ((char *) &atexte.alignment,1,0);
    out_quest ((char *) &atexte.send_ctrl,1,0);
    out_quest ((char *) &atexte.txt_typ,1,0);
    out_quest ((char *) &atexte.txt_platz,1,0);
    out_quest ((char *) &atexte.a,3,0);
    cursor_atexte = prepare_sql (_T("select atexte.sys,  "
"atexte.waa,  atexte.txt_nr,  atexte.txt,  atexte.disp_txt,  "
"atexte.alignment,  atexte.send_ctrl,  atexte.txt_typ,  "
"atexte.txt_platz,  atexte.a from atexte where txt_nr = ? "));

#line 19 "atext.rpp"
}

int ATEXTE_CLASS::lese_atexte ( long txt_nr )
{
    long sqlstatus1;
    if (cursor_atexte == -1)
    {
        prepare_atexte ();
    }
    atexte.txt_nr = txt_nr;
    open_sql (cursor_atexte);
    fetch_sql (cursor_atexte);
    sqlstatus1 = sqlstatus;
    if ( sqlstatus1 )
    {
        atexte.txt[0] = '\0';
    }
    else
    {
        clipped( atexte.txt);
    }
    writelog ("atexte.txt_nr=%ld sqlstatus=%ld", atexte.a, sqlstatus1);
    close_sql (cursor_atexte);
	cursor_atexte = -1;
    return sqlstatus1;
}
