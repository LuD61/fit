#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <windows.h>
#include "strfkt.h"
#include "cmask.h"

static DWORD BuOk = 900;

void CFIELD::display (void)
{
	  return;
}

void CFIELD::destroy (void)
{
	  HDC hdc;
	  
	  hdc = GetDC (hWnd); 
	  if (hFont)
	  {
		   SelectObject (hdc, oldfont);
		   DeleteObject (hFont);
		   hFont = NULL;
	  }
	  ReleaseDC (hWnd, hdc);
	  if (hWnd)
	  {
		   DestroyWindow (hWnd);
		   hWnd = NULL;
	  }
}

void CFIELD::displaybu (HWND MainWindow,HDC hdc)
{
	  TEXTMETRIC tm;
	  int x, y;
	  int cx, cy;
	  SIZE size;
      DWORD bustyle;
	  RECT rect;
	  HANDLE hMainInst;

      if (hWnd) return;

	  if (mFont)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  GetClientRect (MainWindow, &rect);
      oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);

	  cx = this->cx;
	  if (cx == 0)
	  {
              GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
			  cx = size.cx;
	  }
	  else
	  {
              GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		      cx *= size.cx;
	  }

      cy = this->cy;
	  if (cy == 0)
	  {
		  cy = (int) (double) ((double) tm.tmHeight * 1.3);
	  }

	  if (this->x == -1)
	  {
		         x = max (0, (rect.right - cx) / 2);
	  }
	  else
	  {
                 x = this->x * tm.tmAveCharWidth;
	  }

	  if (this->y == -1 && this->cy == 0)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * 1.3) / 2));
	  }

	  if (this->y == -1 && this->cy)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * this->cy) / 2));
	  }
	  else
	  {
                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
	  }

      if (attribut & DEFBUTTON)
      {
                  bustyle = BS_DEFPUSHBUTTON;
      }
      else if (attribut & CHECKBUTTON)
      {
                  bustyle = BS_AUTOCHECKBOX;
      }
      else
      {
                  bustyle = BS_PUSHBUTTON;
      }
      hMainInst = (HANDLE) GetWindowLong (MainWindow, GWL_HINSTANCE);
      hWnd = CreateWindow ("BUTTON",
                           (char *) feld,
                           WS_CHILD | WS_VISIBLE | bustyle,
                           x, y,
                           cx, cy,
                           MainWindow,
                           (HMENU) ID,
                           hMainInst,
                           NULL);

      SendMessage (hWnd,WM_SETFONT, (WPARAM) hFont, 0);
      if (attribut & DISABLED)
      {
                        EnableWindow (hWnd, FALSE);
      }
      else
      {
                        EnableWindow (hWnd, TRUE);
      }
//	  DeleteObject (SelectObject (hdc, oldfont));
}


void CFIELD::displaytxt (HWND MainWindow, HDC hdc)
{
	  HFONT hFont, oldfont;
	  RECT rect;
	  TEXTMETRIC tm;
	  int x, y;
	  SIZE size;

	  if (attribut & BUTTON)
	  {
		          displaybu (MainWindow, hdc);
				  return;
	  }
      if ((attribut & DISPLAYONLY) == 0) return;

	  if (mFont)
	  {
                  hFont = SetDeviceFont (hdc, mFont, &tm);
	  }
	  else
	  {
		          hFont = CreateStdFont (NULL);
	  }

	  GetClientRect (MainWindow, &rect);
	  oldfont = SelectObject (hdc, hFont);
	  GetTextMetrics (hdc, &tm);
	  if (this->x == -1)
	  {
                 GetTextExtentPoint32 (hdc, (char *) feld, strlen ((char *) feld) , &size); 
		         x = max (0, (rect.right - size.cx) / 2);
	  }
	  else
	  {
                 x = this->x * tm.tmAveCharWidth;
	  }

	  if (this->y == -1 && this->cy == 0)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * 1.3) / 2));
	  }

	  if (this->y == -1 && this->cy)
	  {
		         y = max (0, (rect.bottom - (int) (double) 
					         ((double) tm.tmHeight * cy) / 2));
	  }
	  else
	  {
                 y = (int) (double) ((double) this->y * tm.tmHeight * 1.3);
	  }

      SetBkMode (hdc, TRANSPARENT);
	  if (mFont)
	  {
                  SetTextColor (hdc,mFont->FontColor);
	  }
	  else
	  {
                  SetTextColor (hdc,BLACKCOL);

	  }
	  if (this->cx == 0)
	  {
                  TextOut (hdc, x, y, (char *) feld, strlen ((char *) feld));
	  }
      else
	  {
                  TextOut (hdc, x, y, (char *) feld, this->cx);
	  }
	  DeleteObject (SelectObject (hdc, oldfont));
}

void CFIELD::enter (void)
{
	  return;
}


void CFORM::display (void)
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
		cfield[i]->display ();
	}
}


void CFORM::displaytxt (HWND MainWindow, HDC hdc)
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
		cfield[i]->displaytxt (MainWindow, hdc);
	}
}


void CFORM::destroy ()
{
	int i;
	for (i = 0; i < fieldanz; i ++)
	{
		cfield[i]->destroy ();
	}
}


void CFORM::enter (void)
{
	return;
}

static form *TextForm;
static HWND TextWindow;

CFIELD **CTextField;
CFORM *CTextForm;

void WFORM::ShowDlg (HDC hdc, form *frm)
/**
Dialogtext anzeigen.
**/
{
	      TEXTMETRIC tm;
          int i;
		  int x, y;
		  HFONT hFont, oldfont;;


		  if (frm->font)
		  {
                   hFont = SetDeviceFont (hdc, frm->font, &tm);
		  }
		  else
		  {
			       hFont = CreateStdFont (NULL);
		  }

		  oldfont = SelectObject (hdc, hFont);
		  for (i = 0; i < frm->fieldanz; i ++)
		  {
			      x = frm->mask[i].pos[1] * tm.tmAveCharWidth;
				  y = (int) (double) ((double) frm->mask[i].pos[0] * tm.tmHeight * 1.3);
                  SetBkMode (hdc, TRANSPARENT);
				  if (frm->font)
				  {
                            SetTextColor (hdc,frm->font->FontColor);
				  }
				  else
				  {
                            SetTextColor (hdc,BLACKCOL);
				  }
				  if (frm->mask[i].length == 0)
				  {
                            TextOut (hdc, x, y, frm->mask[i].feld, strlen (frm->mask[i].feld));
				  }
				  else
				  {
                            TextOut (hdc, x, y, frm->mask[i].feld, frm->mask[i].length);
				  }
		  }
		  DeleteObject (SelectObject (hdc, oldfont));
}


void WFORM::ProcessMessages (void)
{
	      MSG msg;

          while (GetMessage (&msg, NULL, 0, 0))
		  {
			  if (msg.message == WM_KEYDOWN)
			  {
				  break;
			  }
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
		  }
}

CALLBACK WFORM::Proc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					ShowDlg (hdc, TextForm);
                    EndPaint (hWnd, &ps);
					break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

CALLBACK WFORM::CProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    hdc = BeginPaint (hWnd, &ps);
					CTextForm->displaytxt (hWnd, hdc);
                    EndPaint (hWnd, &ps);
					break;
              
              case WM_COMMAND :
                    if (LOWORD (wParam) == BuOk)
                    {
                           PostQuitMessage (0);
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


HWND MESSCLASS::OpenWindow (HANDLE hInstance, HWND hMainWindow, char **text,mfont *WindowFont)
{
	      TEXTMETRIC tm;
		  int cx, cy, x, y;
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  int i;
		  HFONT hFont, oldfont;
		  HDC hdc;
		  SIZE size;
		  RECT rect;

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) Proc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "MessWind";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }
	      
		  cx = 0;
		  hdc = GetDC (hMainWindow);
          hFont = SetDeviceFont (hdc, WindowFont, &tm);
          oldfont = SelectObject (hdc, hFont); 
		  for (i = 0; i < tlines; i ++)
		  {
              GetTextExtentPoint32 (hdc, text[i], strlen (text[i]) , &size); 
			  if (size.cx > cx)
			  {
				  cx = size.cx;
			  }
		  }
          GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		  cx += 2 * size.cx;
		  DeleteObject (SelectObject (hdc, oldfont));
		  ReleaseDC (hMainWindow, hdc);
		  cy = (int) (double) ((double) tlines  * tm.tmHeight * 1.3);  

		  GetWindowRect (hMainWindow, &rect);
		  x = max (0, (rect.right - rect.left - cx) / 2) + rect.left;
		  y = max (0, (rect.bottom - rect.top - cy) / 2) + rect.top;

          return (CreateWindowEx (0, 
                                 "MessWind",
                                 "",
                                 WS_VISIBLE | WS_DLGFRAME | WS_POPUP,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL));
}

void MESSCLASS::MessageText (HANDLE hInstance, HWND hWnd, char **text, mfont *WindowFont)
{
	      static form *frm;
		  int lines;
		  int i, j;
		  int start;

		  for (tlines = 0; text[tlines]; tlines ++);
		  hMainWindow = hWnd;
		  hMainInst = hInstance;
		  frm = (form *) GlobalAlloc (GMEM_FIXED, sizeof (form));
		  if (frm == NULL) return;
		  
	      lines = 10;
		  if (tlines < lines)
		  {
			  lines = tlines;
		  }

		  frm->mask = (field *) GlobalAlloc (GMEM_FIXED, tlines * sizeof (field));
		  if (frm->mask == NULL)
		  {
			  GlobalFree (frm);
			  return;
		  }

		  frm->fieldanz = tlines;
		  frm->frmstart = 0;
		  frm->frmscroll = 0;
          frm->caption = "";
		  frm->before = NULL;
		  frm->after = NULL;
	      frm->cbfield = NULL;
		  frm->font = WindowFont;
		  for (i = 0; i < frm->fieldanz; i ++)
		  {
			  frm->mask[i].length = 0;
			  frm->mask[i].rows = 0;
			  frm->mask[i].pos[0] = i;
			  frm->mask[i].pos[1] = 1;
			  frm->mask[i].feldid = NULL;
			  frm->mask[i].picture = "";
			  frm->mask[i].attribut = DISPLAYONLY;
			  frm->mask[i].before = NULL;
			  frm->mask[i].after = NULL;
			  frm->mask[i].BuId = 0;
		  }
          start = 0;
		  for (i = start, j = 0; j < tlines; i ++, j ++)
		  {
			  frm->mask[j].feld = text[i];
		  }

		  TextForm = frm;
		  SetwForm (frm);
		  EnableWindows (hMainWindow, FALSE);
          MessageBeep (MB_ICONEXCLAMATION);
          TextWindow = OpenWindow (hInstance, hMainWindow, text, WindowFont);
		  InvalidateRect (TextWindow, NULL, TRUE);
		  UpdateWindow (TextWindow);
}

HWND MESSCLASS::COpenWindow (HANDLE hInstance, HWND hMainWindow, char **text, mfont *WindowFont)
{
	      TEXTMETRIC tm;
		  int cx, cy, x, y;
		  BOOL registered = FALSE;
          WNDCLASS wc;
		  int i;
		  HFONT hFont, oldfont;
		  HDC hdc;
		  SIZE size;
		  RECT rect;

		  if (registered == FALSE)
		  {
                  wc.style =  CS_BYTEALIGNWINDOW | CS_CLASSDC;
                  wc.lpfnWndProc   =  (WNDPROC) CProc;
                  wc.cbClsExtra    =  0;
                  wc.cbWndExtra    =  0;
                  wc.hInstance     =  hInstance;
                  wc.hIcon         =  LoadIcon (hInstance, "FITICON");
                  wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                  wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                  wc.lpszMenuName  =  "";
                  wc.lpszClassName =  "CMessWind";

                  RegisterClass(&wc);
				  registered = TRUE;
		  }
	      
		  cx = 0;
		  hdc = GetDC (hMainWindow);
          hFont = SetDeviceFont (hdc, WindowFont, &tm);
          oldfont = SelectObject (hdc, hFont); 
		  for (i = 0; text[i]; i ++)
		  {
              GetTextExtentPoint32 (hdc, text[i], strlen (text[i]) , &size); 
			  if (size.cx > cx)
			  {
				  cx = size.cx;
			  }
		  }
          GetTextExtentPoint32 (hdc, "X", 1 , &size); 
		  cx += 2 * size.cx;
		  DeleteObject (SelectObject (hdc, oldfont));
		  ReleaseDC (hMainWindow, hdc);
          
		  cy = (int) (double) ((double) tlines  * tm.tmHeight * 1.3);  

		  GetWindowRect (hMainWindow, &rect);
		  x = max (0, (rect.right - rect.left - cx) / 2) + rect.left;
		  y = max (0, (rect.bottom - rect.top - cy) / 2) + rect.top;

          return (CreateWindowEx (0, 
                                 "CMessWind",
                                 "",
                                 WS_VISIBLE | WS_DLGFRAME | WS_POPUP,
                                 x, y,
                                 cx, cy,
                                 hMainWindow,
                                 NULL,
                                 hInstance,
                                 NULL));
}


void MESSCLASS::CMessageText (HANDLE hInstance, HWND hWnd, char **text, 
							  mfont **textfont, mfont *WindowFont)
{
		  int lines;
		  int i;

		  for (tlines = 0; text[tlines]; tlines ++);

		  hMainWindow = hWnd;
		  hMainInst = hInstance;

	      lines = 10;
		  if (tlines < lines)
		  {
			  lines = tlines;
		  }

          CTextField = (CFIELD **) GlobalAlloc (GMEM_FIXED, tlines * sizeof (CFIELD *));
          CTextForm  = new CFORM (tlines, CTextField); 

		  for (i = 0; i < tlines; i ++)
		  {
                      CTextField[i] = new CFIELD ("Text",
						                          text[i], 
						                          0, 0, 1, i, 
												  NULL, "", DISPLAYONLY,
										          NULL, textfont[i]); 		  
		  }
		  SetcwForm (CTextForm);
		  EnableWindows (hMainWindow, FALSE);
          MessageBeep (MB_ICONEXCLAMATION);
          TextWindow = COpenWindow (hInstance, hMainWindow, text, WindowFont);
		  InvalidateRect (TextWindow, NULL, TRUE);
		  UpdateWindow (TextWindow);
}


void MESSCLASS::CMessageTextOK (HANDLE hInstance, HWND hWnd, char **text, 
								mfont **textfont, mfont *WindowFont, mfont *BuFont)
{
		  int lines;
		  int i;

		  for (tlines = 0; text[tlines]; tlines ++);

		  hMainWindow = hWnd;
		  hMainInst = hInstance;

	      lines = 10;
		  if (tlines < lines)
		  {
			  lines = tlines;
		  }

          CTextField = (CFIELD **) GlobalAlloc (GMEM_FIXED, (tlines + 1) * sizeof (CFIELD *));
          CTextForm  = new CFORM (tlines + 1, CTextField); 

		  for (i = 0; i < tlines; i ++)
		  {
                      CTextField[i] = new CFIELD ("Text",
						                          text[i], 
						                          0, 0, 1, i, 
												  NULL, "", DISPLAYONLY,
										          NULL, textfont[i]); 		  
		  }
          CTextField[i] = new CFIELD ("BuOK",
			                          "  OK  ",
			                          10, 0, -1, i + 1,
									  NULL, "", BUTTON,
									  BuOk, BuFont);
		  tlines += 3;
		  SetcwForm (CTextForm);
		  EnableWindows (hMainWindow, FALSE);
          MessageBeep (MB_ICONEXCLAMATION);
          TextWindow = COpenWindow (hInstance, hMainWindow, text, WindowFont);
		  InvalidateRect (TextWindow, NULL, TRUE);
		  UpdateWindow (TextWindow);
}


void MESSCLASS::DestroyText (void)
{
 		  EnableWindows (hMainWindow, TRUE);
		  DestroyWindow (TextWindow);
		  GlobalFree (TextForm->mask);
		  GlobalFree (TextForm);
}

void MESSCLASS::CDestroyText ()
{
	      int i;
		  int lines;

		  CTextForm->destroy ();
          lines = CTextForm->GetFieldanz ();

 		  EnableWindows (hMainWindow, TRUE);
		  DestroyWindow (TextWindow);
          for (i = 0; i < lines; i ++)
		  {
			  delete CTextField[i];
		  }
		  GlobalFree (CTextField);
		  delete (CTextForm);
}
        
