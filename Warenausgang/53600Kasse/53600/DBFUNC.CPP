#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "dbfunc.h"

char qstring[0x1000];

int (*DbAwSpez) (short,void (*) (char *), void (*) (char *)) = NULL;

void SetDbAwSpez (int
              (*AwSpez) (short, void (*) (char *), void (*) (char *)))
/**
DbAwSpez setzen.
**/
{
           DbAwSpez = AwSpez;
}

static char aktfeld [20];
static char aktfeld0 [20];
static short typ;
static int quotOK = 0;
static short col_cursor = -1;

void testquot (char *qfeld, char *colname)
/**
Typ der Feldes Testen. Wenn 0 (Character) oder 7 (date)
Anfuehrungszeichen setzen.
**/
{

           if (col_cursor == -1)
           {
                     ins_quest ((char *) aktfeld, 0, 20);
                     out_quest ((char *) &typ, 1, 0);
                     col_cursor = prepare_sql ("select coltype from "
                                               "syscolumns "
                                               "where colname = ?");
           }
                     
           memset (aktfeld, ' ', 19);
           aktfeld [19] = (char) 0;
           memcpy (aktfeld, colname, strlen (colname));
           if (strcmp (aktfeld, aktfeld0) == 0)
           {
                     if (quotOK == 0) return;
                     strcat (qfeld, "\"");
                     return;
           }
           strcpy (aktfeld0, aktfeld);
           typ = 1;
           open_sql (col_cursor);
           fetch_sql (col_cursor);
           if (sqlstatus) return;

           if (typ == 0 || typ == 7)
           {
                      quotOK = 1;
           }
           else
           {
                      quotOK = 0;
           }
           if (quotOK) strcat (qfeld, "\"");
}
                                 

static void TestQueryField (char *qfeld, char *qname)
/**
Ein Query-Feld testen.
**/
{
         int anz;
         int i;
         char *pos;

         if (qname == NULL) return;
         clipped (qfeld);
         if (strlen (qfeld) == 0) return;

         if (memcmp (qfeld, " ", 1) <= 0) return;

         if (strlen (qstring))
         {
                    strcat (qstring, " and ");
         }
         strcat (qstring, qname);
         strcat (qstring, " ");
         if (pos = strchr (qfeld, '=')) 
         {
                    strcat (qstring, "= ");
                    testquot (qstring, qname);
                    strcat (qstring, pos + 1);
                    testquot (qstring, qname);
         }
         else if (pos = strchr (qfeld, '>')) 
         {
                    strcat (qstring, "> ");
                    testquot (qstring, qname);
                    strcat (qstring, pos + 1);
                    testquot (qstring, qname);
         }
         else if (pos = strchr (qfeld, '<')) 
         {
                    strcat (qstring, "< ");
                    testquot (qstring, qname);
                    strcat (qstring, pos + 1);
                    testquot (qstring, qname);
         }
         else if (strchr (qfeld, '*')) 
         {
                    strcat (qstring, "matches \"");
                    strcat (qstring, qfeld);
                    strcat (qstring, "\"");
         }
         else if (strchr (qfeld, ':'))
         {
                    anz = zsplit (qfeld, ':');
                    if (anz > 1)
                    {
                            strcat (qstring, "between ");
                            testquot (qstring, qname);
                            strcat (qstring, zwort[1]);
                            testquot (qstring, qname);
                            strcat (qstring, " and ");
                            testquot (qstring, qname);
                            strcat (qstring, zwort[2]);
                            testquot (qstring, qname);
                    }
         }
         else if (strchr (qfeld, ','))
         {
                    anz = zsplit (qfeld, ',');
                    if (anz > 1)
                    {
                            strcat (qstring, "in (");
                            strcat (qstring, zwort[1]);
                            for (i = 2; i <= anz; i ++)
                            {
                                  strcat (qstring, ",");
                                  strcat (qstring, zwort[i]);
                            }
                            strcat (qstring, ")");
                     }
         }
         else if (strchr (qfeld, ','))
         {
                    anz = zsplit (qfeld, ',');
                    if (anz > 1)
                    {
                            strcat (qstring, "in (");
                            strcat (qstring, zwort[1]);
                            for (i = 2; i <= anz; i ++)
                            {
                                  strcat (qstring, ",");
                                  strcat (qstring, zwort[i]);
                            }
                            strcat (qstring, ")");
                     }
         }
         else 
         {
                    strcat (qstring, " = ");
                    testquot (qstring, qname);
                    strcat (qstring, qfeld);
                    testquot (qstring, qname);
         }
}

int StatusQuery ()
/**
Query Eingabe testen.
**/
{
         if (strlen (qstring))
         {
                      return TRUE;
         }
         return FALSE;
}

void ReadQuery (form *qform, char *qnamen[])
/**
Query-Eingabe bearbeiten.
**/
{
         int i;

         qstring[0] = (char) 0;

         for (i = 0; i < qform->fieldanz; i ++)
         {
                   TestQueryField (qform->mask[i].feld,
                                   qnamen[i]);
         }
}

int DbAuswahl (short cursor, void (*fillub) (char *),
               void (*fillvalues) (char *))
/**
Auswahl mit Scroll-Cursor azeigen.
**/
{
         int i;
         char buffer [256];

         if (DbAwSpez)
         {
                   return ((*DbAwSpez) (cursor, fillub, fillvalues));
         }
         if (fillvalues == NULL) return -1;

         SaveMenue ();
         InitMenue ();
         OpenListWindowBu (10, 40, 8, 20, 1);
         if (fillub != NULL)
         {
                      (*fillub) (buffer);
                      InsertListUb (buffer);
         }
         while (fetch_scroll (cursor, NEXT) == 0)
         {
                      (*fillvalues) (buffer);
                      InsertListRow (buffer);
         }
         i = EnterQueryListBox ();
         RestoreMenue ();
         SetLbox ();
         // RefreshLbox ();
         return i;
}

