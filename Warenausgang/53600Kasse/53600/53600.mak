# Microsoft Developer Studio Generated NMAKE File, Based on 53600.dsp
!IF "$(CFG)" == ""
CFG=53600 - Win32 Debug
!MESSAGE No configuration specified. Defaulting to 53600 - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "53600 - Win32 Release" && "$(CFG)" != "53600 - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "53600.mak" CFG="53600 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "53600 - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "53600 - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "53600 - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

!IF "$(RECURSE)" == "0" 

ALL : "..\fit\bin\53600.exe"

!ELSE 

ALL : "..\fit\bin\53600.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\A_BAS.OBJ"
	-@erase "$(INTDIR)\A_HNDW.OBJ"
	-@erase "$(INTDIR)\A_PR.OBJ"
	-@erase "$(INTDIR)\AKT_KRZ.OBJ"
	-@erase "$(INTDIR)\AKTION.OBJ"
	-@erase "$(INTDIR)\dbclass.obj"
	-@erase "$(INTDIR)\DBFUNC.OBJ"
	-@erase "$(INTDIR)\FIL.OBJ"
	-@erase "$(INTDIR)\KOMIS.OBJ"
	-@erase "$(INTDIR)\KOMIS.res"
	-@erase "$(INTDIR)\kumebest.obj"
	-@erase "$(INTDIR)\KUN.OBJ"
	-@erase "$(INTDIR)\LS.OBJ"
	-@erase "$(INTDIR)\MDN.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_DRAW.OBJ"
	-@erase "$(INTDIR)\MO_EINH.OBJ"
	-@erase "$(INTDIR)\mo_geb.obj"
	-@erase "$(INTDIR)\MO_MELD.OBJ"
	-@erase "$(INTDIR)\MO_MENU.OBJ"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\mo_progcfg.obj"
	-@erase "$(INTDIR)\pht_wg.obj"
	-@erase "$(INTDIR)\PTAB.OBJ"
	-@erase "$(INTDIR)\STDFKT.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\sys_par.obj"
	-@erase "$(INTDIR)\SYS_PERI.OBJ"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\WMASK.OBJ"
	-@erase "..\fit\bin\53600.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /ML /W3 /GX /I "d:\informix\incl\esql" /I "\user\include" /D\
 "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_NODBCLASS" /Fp"$(INTDIR)\53600.pch" /YX\
 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
RSC_PROJ=/l 0x407 /fo"$(INTDIR)\KOMIS.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\53600.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib isqlt07c.lib /nologo /subsystem:windows /incremental:no\
 /pdb:"$(OUTDIR)\53600.pdb" /machine:I386 /out:"\user\fit\bin\53600.exe"\
 /libpath:"d:\informix\lib" 
LINK32_OBJS= \
	"$(INTDIR)\A_BAS.OBJ" \
	"$(INTDIR)\A_HNDW.OBJ" \
	"$(INTDIR)\A_PR.OBJ" \
	"$(INTDIR)\AKT_KRZ.OBJ" \
	"$(INTDIR)\AKTION.OBJ" \
	"$(INTDIR)\dbclass.obj" \
	"$(INTDIR)\DBFUNC.OBJ" \
	"$(INTDIR)\FIL.OBJ" \
	"$(INTDIR)\KOMIS.OBJ" \
	"$(INTDIR)\KOMIS.res" \
	"$(INTDIR)\kumebest.obj" \
	"$(INTDIR)\KUN.OBJ" \
	"$(INTDIR)\LS.OBJ" \
	"$(INTDIR)\MDN.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_DRAW.OBJ" \
	"$(INTDIR)\MO_EINH.OBJ" \
	"$(INTDIR)\mo_geb.obj" \
	"$(INTDIR)\MO_MELD.OBJ" \
	"$(INTDIR)\MO_MENU.OBJ" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\mo_progcfg.obj" \
	"$(INTDIR)\pht_wg.obj" \
	"$(INTDIR)\PTAB.OBJ" \
	"$(INTDIR)\STDFKT.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\sys_par.obj" \
	"$(INTDIR)\SYS_PERI.OBJ" \
	"$(INTDIR)\WMASK.OBJ" \
	"..\CS\FNB.LIB"

"..\fit\bin\53600.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\53600.exe"

!ELSE 

ALL : "$(OUTDIR)\53600.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\A_BAS.OBJ"
	-@erase "$(INTDIR)\A_HNDW.OBJ"
	-@erase "$(INTDIR)\A_PR.OBJ"
	-@erase "$(INTDIR)\AKT_KRZ.OBJ"
	-@erase "$(INTDIR)\AKTION.OBJ"
	-@erase "$(INTDIR)\dbclass.obj"
	-@erase "$(INTDIR)\DBFUNC.OBJ"
	-@erase "$(INTDIR)\FIL.OBJ"
	-@erase "$(INTDIR)\KOMIS.OBJ"
	-@erase "$(INTDIR)\KOMIS.res"
	-@erase "$(INTDIR)\kumebest.obj"
	-@erase "$(INTDIR)\KUN.OBJ"
	-@erase "$(INTDIR)\LS.OBJ"
	-@erase "$(INTDIR)\MDN.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_DRAW.OBJ"
	-@erase "$(INTDIR)\MO_EINH.OBJ"
	-@erase "$(INTDIR)\mo_geb.obj"
	-@erase "$(INTDIR)\MO_MELD.OBJ"
	-@erase "$(INTDIR)\MO_MENU.OBJ"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\mo_progcfg.obj"
	-@erase "$(INTDIR)\pht_wg.obj"
	-@erase "$(INTDIR)\PTAB.OBJ"
	-@erase "$(INTDIR)\STDFKT.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\sys_par.obj"
	-@erase "$(INTDIR)\SYS_PERI.OBJ"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(INTDIR)\WMASK.OBJ"
	-@erase "$(OUTDIR)\53600.exe"
	-@erase "$(OUTDIR)\53600.ilk"
	-@erase "$(OUTDIR)\53600.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /Zi /Od /I "d:\informix\incl\esql" /I\
 "\user\include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_NODBCLASS"\
 /Fp"$(INTDIR)\53600.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
RSC_PROJ=/l 0x407 /fo"$(INTDIR)\KOMIS.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\53600.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib isqlt07c.lib /nologo /subsystem:windows /incremental:yes\
 /pdb:"$(OUTDIR)\53600.pdb" /debug /machine:I386 /out:"$(OUTDIR)\53600.exe"\
 /pdbtype:sept /libpath:"d:\informix\lib" 
LINK32_OBJS= \
	"$(INTDIR)\A_BAS.OBJ" \
	"$(INTDIR)\A_HNDW.OBJ" \
	"$(INTDIR)\A_PR.OBJ" \
	"$(INTDIR)\AKT_KRZ.OBJ" \
	"$(INTDIR)\AKTION.OBJ" \
	"$(INTDIR)\dbclass.obj" \
	"$(INTDIR)\DBFUNC.OBJ" \
	"$(INTDIR)\FIL.OBJ" \
	"$(INTDIR)\KOMIS.OBJ" \
	"$(INTDIR)\KOMIS.res" \
	"$(INTDIR)\kumebest.obj" \
	"$(INTDIR)\KUN.OBJ" \
	"$(INTDIR)\LS.OBJ" \
	"$(INTDIR)\MDN.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_DRAW.OBJ" \
	"$(INTDIR)\MO_EINH.OBJ" \
	"$(INTDIR)\mo_geb.obj" \
	"$(INTDIR)\MO_MELD.OBJ" \
	"$(INTDIR)\MO_MENU.OBJ" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\mo_progcfg.obj" \
	"$(INTDIR)\pht_wg.obj" \
	"$(INTDIR)\PTAB.OBJ" \
	"$(INTDIR)\STDFKT.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\sys_par.obj" \
	"$(INTDIR)\SYS_PERI.OBJ" \
	"$(INTDIR)\WMASK.OBJ" \
	"..\CS\FNB.LIB"

"$(OUTDIR)\53600.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(CFG)" == "53600 - Win32 Release" || "$(CFG)" == "53600 - Win32 Debug"
SOURCE=..\CS\A_BAS.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_A_BAS=\
	"..\cs\a_bas.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_BAS.OBJ" : $(SOURCE) $(DEP_CPP_A_BAS) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_A_BAS=\
	"..\cs\a_bas.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_BAS.OBJ" : $(SOURCE) $(DEP_CPP_A_BAS) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\A_HNDW.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_A_HND=\
	"..\cs\a_hndw.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_HNDW.OBJ" : $(SOURCE) $(DEP_CPP_A_HND) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_A_HND=\
	"..\cs\a_hndw.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_HNDW.OBJ" : $(SOURCE) $(DEP_CPP_A_HND) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\A_PR.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_A_PR_=\
	"..\cs\a_pr.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_PR.OBJ" : $(SOURCE) $(DEP_CPP_A_PR_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_A_PR_=\
	"..\cs\a_pr.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\A_PR.OBJ" : $(SOURCE) $(DEP_CPP_A_PR_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\AKT_KRZ.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_AKT_K=\
	"..\cs\a_pr.h"\
	"..\cs\akt_krz.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\AKT_KRZ.OBJ" : $(SOURCE) $(DEP_CPP_AKT_K) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_AKT_K=\
	"..\cs\a_pr.h"\
	"..\cs\akt_krz.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\AKT_KRZ.OBJ" : $(SOURCE) $(DEP_CPP_AKT_K) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\AKTION.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_AKTIO=\
	"..\cs\aktion.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\AKTION.OBJ" : $(SOURCE) $(DEP_CPP_AKTIO) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_AKTIO=\
	"..\cs\aktion.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\AKTION.OBJ" : $(SOURCE) $(DEP_CPP_AKTIO) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\dbclass.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_DBCLA=\
	"..\cs\dbclass.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\dbclass.obj" : $(SOURCE) $(DEP_CPP_DBCLA) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_DBCLA=\
	"..\cs\dbclass.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\dbclass.obj" : $(SOURCE) $(DEP_CPP_DBCLA) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\DBFUNC.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_DBFUN=\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\DBFUNC.OBJ" : $(SOURCE) $(DEP_CPP_DBFUN) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_DBFUN=\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\DBFUNC.OBJ" : $(SOURCE) $(DEP_CPP_DBFUN) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\FIL.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_FIL_C=\
	"..\cs\dbfunc.h"\
	"..\cs\fil.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\FIL.OBJ" : $(SOURCE) $(DEP_CPP_FIL_C) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_FIL_C=\
	"..\cs\dbfunc.h"\
	"..\cs\fil.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\FIL.OBJ" : $(SOURCE) $(DEP_CPP_FIL_C) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\KOMIS.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_KOMIS=\
	"..\cs\a_bas.h"\
	"..\cs\a_hndw.h"\
	"..\cs\a_pr.h"\
	"..\cs\akt_krz.h"\
	"..\cs\aktion.h"\
	"..\cs\dbclass.h"\
	"..\cs\dbfunc.h"\
	"..\cs\fil.h"\
	"..\CS\fit.h"\
	"..\cs\fnb.h"\
	"..\cs\kun.h"\
	"..\cs\ls.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_a_pr.h"\
	"..\cs\mo_curs0.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_einh.h"\
	"..\cs\mo_expl.h"\
	"..\cs\mo_geb.h"\
	"..\cs\mo_meld.h"\
	"..\cs\mo_menu.h"\
	"..\cs\mo_numme.h"\
	"..\cs\mo_preis.h"\
	"..\cs\mo_progcfg.h"\
	"..\cs\pht_wg.h"\
	"..\cs\ptab.h"\
	"..\cs\stdfkt.h"\
	"..\cs\strfkt.h"\
	"..\cs\sys_par.h"\
	"..\cs\sys_peri.h"\
	"..\cs\wmask.h"\
	

"$(INTDIR)\KOMIS.OBJ" : $(SOURCE) $(DEP_CPP_KOMIS) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_KOMIS=\
	"..\cs\a_bas.h"\
	"..\cs\a_hndw.h"\
	"..\cs\a_pr.h"\
	"..\cs\akt_krz.h"\
	"..\cs\aktion.h"\
	"..\cs\dbclass.h"\
	"..\cs\dbfunc.h"\
	"..\cs\fil.h"\
	"..\CS\fit.h"\
	"..\cs\fnb.h"\
	"..\cs\kun.h"\
	"..\cs\ls.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_a_pr.h"\
	"..\cs\mo_curs0.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_einh.h"\
	"..\cs\mo_expl.h"\
	"..\cs\mo_geb.h"\
	"..\cs\mo_meld.h"\
	"..\cs\mo_menu.h"\
	"..\cs\mo_numme.h"\
	"..\cs\mo_preis.h"\
	"..\cs\mo_progcfg.h"\
	"..\cs\pht_wg.h"\
	"..\cs\ptab.h"\
	"..\cs\stdfkt.h"\
	"..\cs\strfkt.h"\
	"..\cs\sys_par.h"\
	"..\cs\sys_peri.h"\
	"..\cs\wmask.h"\
	

"$(INTDIR)\KOMIS.OBJ" : $(SOURCE) $(DEP_CPP_KOMIS) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\KOMIS.RC
DEP_RSC_KOMIS_=\
	"..\CS\fit.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\KOMIS.res" : $(SOURCE) $(DEP_RSC_KOMIS_) "$(INTDIR)"
	$(RSC) /l 0x407 /fo"$(INTDIR)\KOMIS.res" /i "\USER\CS" /d "NDEBUG" $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\KOMIS.res" : $(SOURCE) $(DEP_RSC_KOMIS_) "$(INTDIR)"
	$(RSC) /l 0x407 /fo"$(INTDIR)\KOMIS.res" /i "\USER\CS" /d "_DEBUG" $(SOURCE)


!ENDIF 

SOURCE=..\CS\kumebest.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_KUMEB=\
	"..\cs\dbclass.h"\
	"..\cs\dbfunc.h"\
	"..\cs\kumebest.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\kumebest.obj" : $(SOURCE) $(DEP_CPP_KUMEB) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_KUMEB=\
	"..\cs\dbclass.h"\
	"..\cs\dbfunc.h"\
	"..\cs\kumebest.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\kumebest.obj" : $(SOURCE) $(DEP_CPP_KUMEB) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\KUN.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_KUN_C=\
	"..\cs\a_kun.h"\
	"..\cs\dbclass.h"\
	"..\cs\dbfunc.h"\
	"..\cs\kun.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\KUN.OBJ" : $(SOURCE) $(DEP_CPP_KUN_C) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_KUN_C=\
	"..\cs\a_kun.h"\
	"..\cs\dbclass.h"\
	"..\cs\dbfunc.h"\
	"..\cs\kun.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\KUN.OBJ" : $(SOURCE) $(DEP_CPP_KUN_C) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\LS.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_LS_CP=\
	"..\cs\dbfunc.h"\
	"..\cs\ls.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\LS.OBJ" : $(SOURCE) $(DEP_CPP_LS_CP) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_LS_CP=\
	"..\cs\dbfunc.h"\
	"..\cs\ls.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\LS.OBJ" : $(SOURCE) $(DEP_CPP_LS_CP) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\MDN.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MDN_C=\
	"..\cs\dbfunc.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MDN.OBJ" : $(SOURCE) $(DEP_CPP_MDN_C) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MDN_C=\
	"..\cs\dbfunc.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MDN.OBJ" : $(SOURCE) $(DEP_CPP_MDN_C) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\MO_A_PR.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_A_=\
	"..\cs\a_pr.h"\
	"..\cs\akt_krz.h"\
	"..\cs\aktion.h"\
	"..\cs\fil.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_a_pr.h"\
	"..\cs\mo_curso.h"\
	"..\cs\strfkt.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_A_PR.OBJ" : $(SOURCE) $(DEP_CPP_MO_A_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_A_=\
	"..\cs\a_pr.h"\
	"..\cs\akt_krz.h"\
	"..\cs\aktion.h"\
	"..\cs\fil.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_a_pr.h"\
	"..\cs\mo_curso.h"\
	"..\cs\strfkt.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_A_PR.OBJ" : $(SOURCE) $(DEP_CPP_MO_A_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\MO_CURSO.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_CU=\
	"..\cs\mo_curso.h"\
	"..\cs\strfkt.h"\
	"..\cs\tcursor.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_CU=\
	"..\cs\mo_curso.h"\
	"..\cs\strfkt.h"\
	"..\cs\tcursor.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\MO_DRAW.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_DR=\
	"..\cs\mo_draw.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	

"$(INTDIR)\MO_DRAW.OBJ" : $(SOURCE) $(DEP_CPP_MO_DR) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_DR=\
	"..\cs\mo_draw.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	

"$(INTDIR)\MO_DRAW.OBJ" : $(SOURCE) $(DEP_CPP_MO_DR) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\MO_EINH.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_EI=\
	"..\cs\a_bas.h"\
	"..\cs\a_hndw.h"\
	"..\cs\a_kun.h"\
	"..\cs\dbclass.h"\
	"..\cs\kumebest.h"\
	"..\cs\kun.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_einh.h"\
	"..\cs\mo_meld.h"\
	"..\cs\ptab.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_EINH.OBJ" : $(SOURCE) $(DEP_CPP_MO_EI) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_EI=\
	"..\cs\a_bas.h"\
	"..\cs\a_hndw.h"\
	"..\cs\a_kun.h"\
	"..\cs\dbclass.h"\
	"..\cs\kumebest.h"\
	"..\cs\kun.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_einh.h"\
	"..\cs\mo_meld.h"\
	"..\cs\ptab.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_EINH.OBJ" : $(SOURCE) $(DEP_CPP_MO_EI) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\mo_geb.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_GE=\
	"..\cs\mo_curso.h"\
	"..\cs\mo_geb.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\mo_geb.obj" : $(SOURCE) $(DEP_CPP_MO_GE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_GE=\
	"..\cs\mo_curso.h"\
	"..\cs\mo_geb.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\mo_geb.obj" : $(SOURCE) $(DEP_CPP_MO_GE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\MO_MELD.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_ME=\
	"..\cs\mo_draw.h"\
	"..\cs\mo_menu.h"\
	"..\cs\wmask.h"\
	

"$(INTDIR)\MO_MELD.OBJ" : $(SOURCE) $(DEP_CPP_MO_ME) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_ME=\
	"..\cs\mo_draw.h"\
	"..\cs\mo_menu.h"\
	"..\cs\wmask.h"\
	

"$(INTDIR)\MO_MELD.OBJ" : $(SOURCE) $(DEP_CPP_MO_ME) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\MO_MENU.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_MEN=\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\mo_menu.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_MENU.OBJ" : $(SOURCE) $(DEP_CPP_MO_MEN) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_MEN=\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\mo_menu.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_MENU.OBJ" : $(SOURCE) $(DEP_CPP_MO_MEN) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\MO_NUMME.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_NU=\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\mo_numme.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_NUMME.OBJ" : $(SOURCE) $(DEP_CPP_MO_NU) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_NU=\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\mo_numme.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_NUMME.OBJ" : $(SOURCE) $(DEP_CPP_MO_NU) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\MO_PREIS.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_PR=\
	"..\cs\a_pr.h"\
	"..\cs\akt_krz.h"\
	"..\cs\aktion.h"\
	"..\cs\fil.h"\
	"..\cs\kun.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_a_pr.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_preis.h"\
	"..\cs\strfkt.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_PREIS.OBJ" : $(SOURCE) $(DEP_CPP_MO_PR) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_PR=\
	"..\cs\a_pr.h"\
	"..\cs\akt_krz.h"\
	"..\cs\aktion.h"\
	"..\cs\fil.h"\
	"..\cs\kun.h"\
	"..\cs\mdn.h"\
	"..\cs\mo_a_pr.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_preis.h"\
	"..\cs\strfkt.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\MO_PREIS.OBJ" : $(SOURCE) $(DEP_CPP_MO_PR) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\libsrc\mo_progcfg.cpp
DEP_CPP_MO_PRO=\
	"..\include\mo_progcfg.h"\
	"..\include\strfkt.h"\
	

"$(INTDIR)\mo_progcfg.obj" : $(SOURCE) $(DEP_CPP_MO_PRO) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\CS\pht_wg.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PHT_W=\
	"..\cs\mo_curso.h"\
	"..\cs\pht_wg.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\pht_wg.obj" : $(SOURCE) $(DEP_CPP_PHT_W) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PHT_W=\
	"..\cs\mo_curso.h"\
	"..\cs\pht_wg.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\pht_wg.obj" : $(SOURCE) $(DEP_CPP_PHT_W) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\PTAB.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PTAB_=\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\ptab.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\PTAB.OBJ" : $(SOURCE) $(DEP_CPP_PTAB_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PTAB_=\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\ptab.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\PTAB.OBJ" : $(SOURCE) $(DEP_CPP_PTAB_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\STDFKT.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_STDFK=\
	"..\cs\mo_draw.h"\
	"..\cs\stdfkt.h"\
	"..\cs\wmask.h"\
	

"$(INTDIR)\STDFKT.OBJ" : $(SOURCE) $(DEP_CPP_STDFK) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_STDFK=\
	"..\cs\mo_draw.h"\
	"..\cs\stdfkt.h"\
	"..\cs\wmask.h"\
	

"$(INTDIR)\STDFKT.OBJ" : $(SOURCE) $(DEP_CPP_STDFK) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\STRFKT.CPP
DEP_CPP_STRFK=\
	"..\cs\strfkt.h"\
	

"$(INTDIR)\STRFKT.OBJ" : $(SOURCE) $(DEP_CPP_STRFK) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\CS\sys_par.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_SYS_P=\
	"..\cs\mo_curso.h"\
	"..\cs\sys_par.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\sys_par.obj" : $(SOURCE) $(DEP_CPP_SYS_P) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_SYS_P=\
	"..\cs\mo_curso.h"\
	"..\cs\sys_par.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\sys_par.obj" : $(SOURCE) $(DEP_CPP_SYS_P) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\SYS_PERI.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_SYS_PE=\
	"..\cs\dbclass.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\sys_peri.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\SYS_PERI.OBJ" : $(SOURCE) $(DEP_CPP_SYS_PE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_SYS_PE=\
	"..\cs\dbclass.h"\
	"..\cs\dbfunc.h"\
	"..\cs\mo_curso.h"\
	"..\cs\mo_draw.h"\
	"..\cs\mo_meld.h"\
	"..\cs\strfkt.h"\
	"..\cs\sys_peri.h"\
	"..\cs\wmask.h"\
	"d:\informix\incl\esql\sqlca.h"\
	"d:\informix\incl\esql\sqlda.h"\
	"d:\informix\incl\esql\sqlhdr.h"\
	"d:\informix\incl\esql\sqlproto.h"\
	

"$(INTDIR)\SYS_PERI.OBJ" : $(SOURCE) $(DEP_CPP_SYS_PE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\CS\WMASK.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_WMASK=\
	"..\cs\mo_draw.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	

"$(INTDIR)\WMASK.OBJ" : $(SOURCE) $(DEP_CPP_WMASK) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_WMASK=\
	"..\cs\mo_draw.h"\
	"..\cs\strfkt.h"\
	"..\cs\wmask.h"\
	

"$(INTDIR)\WMASK.OBJ" : $(SOURCE) $(DEP_CPP_WMASK) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 


!ENDIF 

