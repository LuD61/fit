#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "mo_pasc.h"
#include "strfkt.h"

Mfifo::Mfifo (int messanz, char *fifoname) 
{
         int i;
	     this->messanz = messanz;
		 messp = 0;
	     messbuff = new char * [messanz];
	     for (i = 0; i < messanz; i ++) messbuff [i] = NULL;
		 this->fifoname = fifoname;
}

Mfifo::~Mfifo (void)
{
	     int i;

		 for (i = 0; i < messp; i ++)
		 {
			   if (messbuff[i]) delete messbuff[i];
		 }
		 delete messbuff;
}

void Mfifo::WaitMessage (long secs)
/**
secs Tausendstel Sekunden warten und Windowsmeldungen abfragen.
**/
{
#ifdef _CONSOLE
	     Sleep (secs);
#else
	     MSG msg; 
	     int i;

		 for (i = 0; i < secs; i += 10)
		 {
               if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
			   {
                        if (msg.message == WM_QUIT)
						{
                                  PostQuitMessage (0);
						          return;
						}
                        TranslateMessage(&msg);
                        DispatchMessage(&msg);
			   }
			   Sleep (10);
		 }
#endif
}


void Mfifo::ScrollMsg (void)
/**
Buffer scrollen.
**/
{
	int i;

	if (messp == 0) return;

	if (messbuff[0]) delete messbuff [0];

	messp --;

	for (i = 0; i < messp; i ++)
	{
		messbuff[i] = messbuff[i + 1];
	}
	messbuff[i] = NULL;
}


void Mfifo::FromMsg (char *buffer)
/**
Naechste Meldung holen.
**/
{
	if (messp == 0)
	{
		strcpy (buffer, "");
		return;
	}

	strcpy (buffer, messbuff[0]);
	ScrollMsg ();
}


BOOL Mfifo::ToMsg (char *buffer)
/**
Meldung speichern.
**/
{
	if (messp == messanz) return FALSE;
	
	if (messbuff [messp] == NULL)
	{
	          messbuff [messp] = new char [512];
	}

    strcpy (messbuff [messp], buffer);
	messp ++;
	return TRUE;
}

void Mfifo::InitMsg (void)
/**
Meldung in Datei fifoname schreiben.
**/
{
	char buffer [512];
	char *tmp;


    tmp = getenv ("TMPPATH");
    if (tmp == NULL)
	{
			 return;
	}

    
    sprintf (buffer, "%s\\%s", tmp, fifoname);
	unlink (buffer);
}

BOOL Mfifo::WriteMsg (char *msg)
/**
Meldung in Datei fifoname schreiben.
**/
{
	FILE *fp;
	char buffer [512];
	char *tmp;
	char * p;

	while (ToMsg (msg) == FALSE)
	{
		return FALSE;
	}

    tmp = getenv ("TMPPATH");
    if (tmp == NULL)
	{
			 return (0);
	}

    
    sprintf (buffer, "%s\\%s", tmp, fifoname);
	fp = fopen (buffer, "r");
	if (fp) 
	{
		p = fgets (buffer, 255, fp);
		fclose (fp);
		if (p && buffer[0] != '*') return TRUE;
	}

    sprintf (buffer, "%s\\%s", tmp, fifoname);
	fp = fopen (buffer, "w");
    if (fp == NULL) return FALSE;

    FromMsg (buffer);
	fprintf (fp, "%s\n", buffer);
	fclose (fp);
	return TRUE;
}


BOOL Mfifo::NextMsg (void)
/**
Meldung in Datei fifoname schreiben.
**/
{
	FILE *fp;
	char buffer [512];
	char *tmp;
	char * p;


    tmp = getenv ("TMPPATH");
    if (tmp == NULL)
	{
			 return (0);
	}

    
    sprintf (buffer, "%s\\%s", tmp, fifoname);
	fp = fopen (buffer, "r");
	if (fp) 
	{
		p = fgets (buffer, 255, fp);
		fclose (fp);
		if (p && buffer[0] != '*') return TRUE;
	}

    sprintf (buffer, "%s\\%s", tmp, fifoname);
	fp = fopen (buffer, "w");
    if (fp == NULL) return FALSE;

    FromMsg (buffer);
	fprintf (fp, "%s\n", buffer);
	fclose (fp);
	return TRUE;
}

 
void Mfifo::WaitMsg (void)
/**
Warten, bis alle Meldungen abgearbeitet sind.
**/
{
     while (messp) 
	 {
		 WaitMessage (100);
		 NextMsg ();
	 }
}

BOOL Mfifo::WaitMsg (HANDLE prog)
/**
Warten, bis alle Meldungen abgearbeitet sind.
**/
{
	 DWORD ExitCode;

     while (messp) 
	 {
	    WaitMessage (100);
        GetExitCodeProcess (prog, &ExitCode);
        if (ExitCode != STILL_ACTIVE)
		{
			return FALSE;
		}
		NextMsg ();
	 }
	 return TRUE;
}

int Mfifo::TestMsgError (void)
{
	 FILE *fp;
	 char *tmp;
	 char buffer [512];
	 int ecode;

	 tmp = getenv ("TMPPATH");
	 if (tmp == NULL) return 1;

     sprintf (buffer, "%s\\bws_ascw.err", tmp);
	 fp = fopen (buffer, "r");
	 if (fp == NULL) return 0;

	 ecode = 0;
	 if (fgets (buffer, 511, fp))
	 {
		 ecode = atoi (buffer);
	 }
	 fclose (fp);
     sprintf (buffer, "%s\\bws_ascw.err", tmp);
	 unlink (buffer);
	 return ecode;
}


BOOL Mfifo::WriteMsgPa (char *msg)
/**
Parameterdaten in Datei fifoname schreiben.
**/
{
	FILE *fp;
	char buffer [512];
	char *tmp;
	char **PaLine;
	int anz, words;
	int i;

    tmp = getenv ("TMPPATH");
    if (tmp == NULL)
	{
			 return (0);
	}

    
    sprintf (buffer, "%s\\%s", tmp, fifoname);

    sprintf (buffer, "%s\\%s", tmp, fifoname);
	fp = fopen (buffer, "w");
    if (fp == NULL) return FALSE;

	anz = wsplit (msg, ";");
	PaLine = new char * [anz + 1];
	if (PaLine == NULL) return FALSE;

    for (i = 0; i < anz; i ++)
	{
		PaLine[i] = new char [512];
		if (PaLine [i] == NULL)
		{
			delete PaLine;
			return FALSE;
		}
		strcpy (PaLine[i], wort[i]);
	}

	for (i = 0; i < anz; i ++)
	{
		words = wsplit (PaLine[i], " ");
		if (words > 1)
		{
			fprintf (fp, "%s %s\n", wort[0], wort[1]);
		}
	}

	for (i = 0; i < anz; i ++)
	{
		 if (PaLine[i]) delete PaLine [i];
	}
	delete PaLine;
	fclose (fp);
	return TRUE;
}








