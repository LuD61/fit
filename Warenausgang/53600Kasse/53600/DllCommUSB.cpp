#include "DllCommUSB.h"
#include "Text.h"

#define CString Text

HANDLE CDllCommUSB::CommLib = NULL;
HANDLE CDllCommUSB::schnittstelle = NULL;

char* CDllCommUSB::comport = "";

char display1[60];
char display2[60];


CDllCommUSB::CDllCommUSB ()
{
	OpenUsbPort = NULL;
	char *bws;

	bws = getenv ("BWS");


	CString CommUSBDll;
	if (bws != NULL)
	{
		CommUSBDll.Format ("%s\\bin\\CommUSB.dll", bws);
	}
	else
	{
		CommUSBDll = "CommUSB.dll";
	}
	CommLib = LoadLibrary (CommUSBDll.GetBuffer ());
	if (CommLib != NULL)
	{
		OpenUsbPort = (int (*) (int)) 
					GetProcAddress ((HMODULE) CommLib, "OpenUsbPort");
	}
}
int CDllCommUSB::OpenKundenDisplay (void)
{
	int dret = 0;
	if (strlen (comport) <= 2 ) return 0;
    schnittstelle = CreateFile ( (LPCTSTR) comport ,
                        GENERIC_READ | GENERIC_WRITE ,
                        0 , /* prevents the file from being shared */
                        NULL ,  /* no security attributes */
                        OPEN_EXISTING , /* comm devices must use "OPEN_EXISTING */
                        0 , /* not overlapped I/O */
                        NULL /* hTemplate must be NULL for comm devices */
   						) ;


	if (schnittstelle == INVALID_HANDLE_VALUE)
	{
		return 0;
	}

/*
typedef struct _DCB {
    DWORD DCBlength;      // sizeof(DCB)                     
    DWORD BaudRate;       // Baudrate at which running       
    DWORD fBinary: 1;     // Binary Mode (skip EOF check)    
    DWORD fParity: 1;     // Enable parity checking          
    DWORD fOutxCtsFlow:1; // CTS handshaking on output       
    DWORD fOutxDsrFlow:1; // DSR handshaking on output       
    DWORD fDtrControl:2;  // DTR Flow control                
    DWORD fDsrSensitivity:1; // DSR Sensitivity              
    DWORD fTXContinueOnXoff: 1; // Continue TX when Xoff sent 
    DWORD fOutX: 1;       // Enable output X-ON/X-OFF        
    DWORD fInX: 1;        // Enable input X-ON/X-OFF         
    DWORD fErrorChar: 1;  // Enable Err Replacement          
    DWORD fNull: 1;       // Enable Null stripping           
    DWORD fRtsControl:2;  // Rts Flow control                
    DWORD fAbortOnError:1; // Abort all reads and writes on Error 
    DWORD fDummy2:17;     // Reserved                        
    WORD wReserved;       // Not currently used              
    WORD XonLim;          // Transmit X-ON threshold         
    WORD XoffLim;         // Transmit X-OFF threshold        
    BYTE ByteSize;        // Number of bits/byte, 4-8        
    BYTE Parity;          // 0-4=None,Odd,Even,Mark,Space    
    BYTE StopBits;        // 0,1,2 = 1, 1.5, 2               
    char XonChar;         // Tx and Rx X-ON character        
    char XoffChar;        // Tx and Rx X-OFF character       
    char ErrorChar;       // Error replacement char          
    char EofChar;         // End of Input character          
    char EvtChar;         // Received Event character        
    WORD wReserved1;      // Fill for now.                   
} DCB, *LPDCB;
*/
    a_CommDcb.BaudRate = (DWORD) 9600 ;
    a_CommDcb.ByteSize = (BYTE) 8 ;
	a_CommDcb.Parity = NOPARITY ;
	a_CommDcb.StopBits = ONESTOPBIT ;
    dret = SetCommState ( schnittstelle, &a_CommDcb ) ;

// das sind gerade die Werte fuer 9600
	int tfaktor = 1;
     a_CommTmo.ReadIntervalTimeout = 30 * tfaktor ;         /* 30 millisec -> Endekenn !! */
     a_CommTmo.ReadTotalTimeoutMultiplier = 1 ;          /* x 2 ( Zeit je Byte+x */ 
     a_CommTmo.ReadTotalTimeoutConstant = 30000 ; /* 30 sek warten */ /* 30 millisec (RITO) */ 

    dret = GetCommTimeouts(schnittstelle, &a_CommTmo) ;
	
	
	
	return 1;

}
int CDllCommUSB::CloseKundenDisplay (void)
{
	int dret = 1;
	if (strlen (comport) <= 2 ) return 0;
     //dret =  DeleteFile ( (LPCTSTR) comport);

	dret = CloseHandle (schnittstelle);
	 return dret;
}

int umlaute_konv (char *string, char *uml_disp, char *uml_db)
/**
Umlaute konvertieren.
**/
{
 int i;
 int j;
       
 int len = strlen (string);
 for (i = 0; i < len; i ++)
 {
  for (j = 0; uml_db [j]; j ++)
  {
   if (string [i] == uml_db [j])
   {
    string [i] = uml_disp [j];
     break;
   }
  }
 }
 return (0);
}         



int CDllCommUSB::WriteKundenDisplay (char* buffer1, char* buffer2)
{

unsigned int Zeilenlaenge = 20;
DWORD   NumberOfBytesWritten ;
BOOL rc ;       /* returncode */

unsigned int nNumberOfBytesToWrite;
char buf [2];
char uml_disp [50] = {(char) 154, (char) 132, (char) 148, (char) 129, (char) 0};
char uml_db [50] = {'�', '�', '�', '�', (char) 0};

if (strlen(buffer1) > 2)
{
	strcpy(display1, buffer1);
}
else
{
//	strcpy(buffer1, display1);
}
strcpy(display2, buffer2);

umlaute_konv ( display1, uml_disp, uml_db);
umlaute_konv ( display2, uml_disp, uml_db);
	

if (strlen(display1) > 1)
{
	//SCREEN l�schen
	memset ( buf, '\0', 2 ) ;
	*buf = (char) 12;
	rc = WriteFile ( schnittstelle , (LPVOID) buf, (DWORD) 1,
       (LPDWORD) &NumberOfBytesWritten , NULL );

	//1.Zeile schreiben
	nNumberOfBytesToWrite = strlen(display1);
	if (nNumberOfBytesToWrite > Zeilenlaenge) nNumberOfBytesToWrite = Zeilenlaenge;
	rc = WriteFile ( schnittstelle , (LPVOID) display1, (DWORD) nNumberOfBytesToWrite,
       (LPDWORD) &NumberOfBytesWritten , NULL );
}

if (nNumberOfBytesToWrite < Zeilenlaenge)
{
	//Cursor auf die n�chste Zeile setzen
	memset ( buf, '\0', 2 ) ;
	*buf = (char) 10;
	rc = WriteFile ( schnittstelle , (LPVOID) buf, (DWORD) 1,
       (LPDWORD) &NumberOfBytesWritten , NULL );

	//Cursor in der Zeile nach links setzen   
	memset ( buf, '\0', 2 ) ;
	*buf = (char) 13;
	rc = WriteFile ( schnittstelle , (LPVOID) buf, (DWORD) 1,
       (LPDWORD) &NumberOfBytesWritten , NULL );
}

//2.Zeile schreiben
nNumberOfBytesToWrite = strlen(display2);
if (nNumberOfBytesToWrite > Zeilenlaenge) nNumberOfBytesToWrite = Zeilenlaenge;
rc = WriteFile ( schnittstelle , (LPVOID) display2, (DWORD) nNumberOfBytesToWrite,
       (LPDWORD) &NumberOfBytesWritten , NULL );

	return NumberOfBytesWritten;
}
