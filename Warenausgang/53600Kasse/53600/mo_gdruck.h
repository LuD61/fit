#ifndef _mo_gdruck_def
#define mo_gdruck_def
HDC GetPrinterDC (void);
int Gdruck (char *);
void SetPage (void);
void SelectPrinter (void);
char *GetNextPrinter (void);
void GetAllPrinters (void);
HDC GetPrinterbyName (char *);
char *GetPrinter (char *);
void SetPrinter (char *);
int IsGdiPrint (void);
void SetPersName (char *);
void ShowGdiPrinters (void);
void SetGdiPrinter (char *);
#endif	   
       

