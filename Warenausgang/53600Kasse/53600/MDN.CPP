#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#ifndef CONSOLE
#include "wmask.h"
#include "mo_meld.h"
#include "dbfunc.h"
#endif
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"

struct MDN _mdn, _mdn_null;
struct ADR _adr, _adr_null;
struct SMT_ZUORD smt_zuord, smt_zuord_null;

static void FillUb  (char *buffer)
/**
Ueberschrift fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%7s %-16s",
                          "Mandant", "Name");
}

static void FillValues  (char *buffer)
/**
Werte fuer Auswahl fuellen.
**/
{
         sprintf (buffer, "%7hd %-16.16s",
                          _mdn.mdn, _adr.adr_krz);
}

int MDN_CLASS::lese_mdn (short mdn)
/**
Tabelle mdn fuer Mandant mdn lesen.
**/
{
         if (cursor_mdn == -1)
         {
                ins_quest ((char *) &_mdn.mdn, 1, 0);
                out_quest ((char *) _mdn.abr_period,0,2);
                out_quest ((char *) &_mdn.adr,2,0);
                out_quest ((char *) &_mdn.adr_lief,2,0);
                out_quest ((char *) &_mdn.dat_ero,2,0);
                out_quest ((char *) &_mdn.daten_mnp,1,0);
                out_quest ((char *) &_mdn.delstatus,1,0);
                out_quest ((char *) _mdn.fil_bel_o_sa,0,2);
                out_quest ((char *) &_mdn.fl_lad,3,0);
                out_quest ((char *) &_mdn.fl_nto,3,0);
                out_quest ((char *) &_mdn.fl_vk_ges,3,0);
                out_quest ((char *) &_mdn.iakv,2,0);
                out_quest ((char *) _mdn.inv_rht,0,2);
                out_quest ((char *) &_mdn.kun,2,0);
                out_quest ((char *) _mdn.lief,0,17);
                out_quest ((char *) _mdn.lief_rht,0,2);
                out_quest ((char *) &_mdn.lief_s,2,0);
                out_quest ((char *) &_mdn.mdn,1,0);
                out_quest ((char *) _mdn.mdn_kla,0,2);
                out_quest ((char *) &_mdn.mdn_gr,1,0);
                out_quest ((char *) _mdn.pers,0,13);
                out_quest ((char *) &_mdn.pers_anz,1,0);
                out_quest ((char *) _mdn.pr_bel_entl,0,2);
                out_quest ((char *) &_mdn.pr_lst,2,0);
                out_quest ((char *) &_mdn.reg_bed_theke_lng,3,0);
                out_quest ((char *) &_mdn.reg_kt_lng,3,0);
                out_quest ((char *) &_mdn.reg_kue_lng,3,0);
                out_quest ((char *) &_mdn.reg_lng,3,0);
                out_quest ((char *) &_mdn.reg_tks_lng,3,0);
                out_quest ((char *) &_mdn.reg_tkt_lng,3,0);
                out_quest ((char *) _mdn.smt_kz,0,2);
                out_quest ((char *) &_mdn.sonst_einh,1,0);
                out_quest ((char *) _mdn.sp_kz,0,2);
                out_quest ((char *) &_mdn.sprache,1,0);
                out_quest ((char *) _mdn.sw_kz,0,2);
                out_quest ((char *) &_mdn.tou,2,0);
                out_quest ((char *) _mdn.umlgr,0,2);
                out_quest ((char *) _mdn.verk_st_kz,0,2);
                out_quest ((char *) &_mdn.vrs_typ,1,0);
                out_quest ((char *) _mdn.inv_akv,0,2);
                out_quest ((char *) &_mdn.gbr,3,0);
                out_quest ((char *) _mdn.waehr_prim,0,2);
                out_quest ((char *) _mdn.waehr_sek,0,2);
                out_quest ((char *) &_mdn.konversion,3,0);
                out_quest ((char *) _adr.adr_krz, 0,17);
                out_quest ((char *) _mdn.iln, 0,17);

                cursor_mdn =
prepare_sql ("select mdn.abr_period,  mdn.adr,  mdn.adr_lief,  "
"mdn.dat_ero,  mdn.daten_mnp,  mdn.delstatus,  mdn.fil_bel_o_sa,  "
"mdn.fl_lad,  mdn.fl_nto,  mdn.fl_vk_ges,  mdn.iakv,  mdn.inv_rht,  mdn.kun,  "
"mdn.lief,  mdn.lief_rht,  mdn.lief_s,  mdn.mdn,  mdn.mdn_kla,  mdn.mdn_gr,  "
"mdn.pers,  mdn.pers_anz,  mdn.pr_bel_entl,  mdn.pr_lst,  "
"mdn.reg_bed_theke_lng,  mdn.reg_kt_lng,  mdn.reg_kue_lng,  "
"mdn.reg_lng,  mdn.reg_tks_lng,  mdn.reg_tkt_lng,  mdn.smt_kz,  "
"mdn.sonst_einh,  mdn.sp_kz,  mdn.sprache,  mdn.sw_kz,  mdn.tou,  mdn.umlgr,  "
"mdn.verk_st_kz,  mdn.vrs_typ,  mdn.inv_akv,  mdn.gbr,  mdn.waehr_prim,  "
"mdn.waehr_sek,  mdn.konversion, adr.adr_krz, mdn.iln "
                                 "from mdn,adr "
                                 "where mdn.mdn = ? "
                                 "and mdn.adr = adr.adr");
         }
         _mdn.mdn = mdn;
         open_sql (cursor_mdn);
         fetch_sql (cursor_mdn);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int MDN_CLASS::lese_mdn (void)
/**
Naechsten Satz aus Tabelle mdn lesen.
**/
{
         fetch_sql (cursor_mdn);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int MDN_CLASS::lese_mdn_gr (short mdn_gr)
/**
Tabelle mdn fuer Mandant mdn lesen.
**/
{
         if (cursor_mdn_gr == -1)
         {
                ins_quest ((char *) &_mdn.mdn_gr, 1, 0);
                out_quest ((char *) &_mdn.mdn, 1, 0);
                out_quest ((char *) &_mdn.fil_bel_o_sa, 0, 2);
                out_quest ((char *) &_adr.adr_krz, 0, 16);
                cursor_mdn_gr =
                    prepare_sql ("select mdn.mdn,fil_bel_o_sa,adr.adr_krz "
                                 "from mdn,adr "
                                 "where mdn.mdn_gr = ? "
                                 "and mdn.adr = adr.adr");
         }
         _mdn.mdn_gr = mdn_gr;
         open_sql (cursor_mdn_gr);
         fetch_sql (cursor_mdn_gr);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int MDN_CLASS::lese_mdn_gr (void)
/**
Naechsten Satz aus Tabelle mdn lesen.
**/
{
         fetch_sql (cursor_mdn_gr);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}


#ifndef CONSOLE

int MDN_CLASS::ShowAllMdn ()
/**
Auswahl ueber Mandanten anzeigen.
**/
{
         int cursor_ausw;
         int i;

         out_quest ((char *) &_mdn.mdn, 1, 0);
         out_quest (_adr.adr_krz, 0, 16);

         cursor_ausw = prepare_scroll ("select mdn.mdn, adr.adr_krz from "
                                       "mdn,adr "
                                       "where mdn.mdn > 0 "
                                       "and adr.adr = mdn.adr "
                                       "order by mdn");
         if (sqlstatus)
         {
                     return (-1);
         }
         i = DbAuswahl (cursor_ausw, FillUb,
                                     FillValues);
         UpdateFkt ();
         SetFocus (current_form->mask[currentfield].feldid);

         SetCurrentField (currentfield);

         if (syskey == KEYESC || syskey == KEY5)
         {
                     close_sql (cursor_ausw);
                     return 0;
         }
         fetch_scroll (cursor_ausw, DBABSOLUTE, i + 1);
         close_sql (cursor_ausw);
         return 0;
}

#endif // CONSOLE
void ADR_CLASS::prepare_adr (void)
/**
Lese-Cursor fuer adr vorbereiten.
**/
{
    if (cursor_adr != -1) return;

    out_quest ((char *) &_adr.adr,2,0);
    out_quest ((char *) _adr.adr_krz,0,17);
    out_quest ((char *) _adr.adr_nam1,0,37);
    out_quest ((char *) _adr.adr_nam2,0,37);
    out_quest ((char *) &_adr.adr_typ,1,0);
    out_quest ((char *) _adr.adr_verkt,0,17);
    out_quest ((char *) &_adr.anr,1,0);
    out_quest ((char *) &_adr.delstatus,1,0);
    out_quest ((char *) _adr.fax,0,21);
    out_quest ((char *) &_adr.fil,1,0);
    out_quest ((char *) &_adr.geb_dat,2,0);
    out_quest ((char *) &_adr.land,1,0);
    out_quest ((char *) &_adr.mdn,1,0);
    out_quest ((char *) _adr.merkm_1,0,3);
    out_quest ((char *) _adr.merkm_2,0,3);
    out_quest ((char *) _adr.merkm_3,0,3);
    out_quest ((char *) _adr.merkm_4,0,3);
    out_quest ((char *) _adr.merkm_5,0,3);
    out_quest ((char *) _adr.modem,0,21);
    out_quest ((char *) _adr.ort1,0,37);
    out_quest ((char *) _adr.ort2,0,37);
    out_quest ((char *) _adr.partner,0,37);
    out_quest ((char *) _adr.pf,0,17);
    out_quest ((char *) _adr.plz,0,9);
    out_quest ((char *) &_adr.staat,1,0);
    out_quest ((char *) _adr.str,0,37);
    out_quest ((char *) _adr.tel,0,21);
    out_quest ((char *) _adr.telex,0,21);
    out_quest ((char *) &_adr.txt_nr,2,0);

    ins_quest ((char *) &_adr.adr,2,0);
cursor_adr = prepare_sql ("select adr.adr,  adr.adr_krz,  "
"adr.adr_nam1,  adr.adr_nam2,  adr.adr_typ,  adr.adr_verkt,  adr.anr,  "
"adr.delstatus,  adr.fax,  adr.fil,  adr.geb_dat,  adr.land,  adr.mdn,  "
"adr.merkm_1,  adr.merkm_2,  adr.merkm_3,  adr.merkm_4,  adr.merkm_5,  "
"adr.modem,  adr.ort1,  adr.ort2,  adr.partner,  adr.pf,  adr.plz,  adr.staat,  "
"adr.str,  adr.tel,  adr.telex,  adr.txt_nr from adr "
                           "where adr = ?");
}

int ADR_CLASS::lese_adr (long adr_nr)
/**
Tabelle mdn fuer Adresse lesen.
**/
{
         if (cursor_adr == -1)
         {
                   prepare_adr ();
         }
         _adr.adr = adr_nr;
         open_sql (cursor_adr);
         fetch_sql (cursor_adr);
         return sqlstatus;
}

int ADR_CLASS::lese_adr (void)
/**
Tabelle adr lesen.
**/
{
         if (cursor_adr == -1)
         {
                   prepare_adr ();
         }
         fetch_sql (cursor_adr);
         return sqlstatus;
}

void SMT_ZU_CLASS::prepare (void)
/**
Cursor fuer smt_zuord vorbereiten.
**/
{
        ins_quest ((char *) &smt_zuord.mdn, 1, 0);
        ins_quest ((char *) &smt_zuord.fil, 1, 0);
        ins_quest ((char *) &smt_zuord.verk_st, 1, 0);
        ins_quest ((char *) &smt_zuord.hwg, 1, 0);
        out_quest ((char *) &smt_zuord.fil,1,0);
        out_quest ((char *) &smt_zuord.hwg,1,0);
        out_quest ((char *) &smt_zuord.mdn,1,0);
        out_quest ((char *) &smt_zuord.verk_st,1,0);
        out_quest ((char *) &smt_zuord.zuord_dat,2,0);
        out_quest ((char *) &smt_zuord.fil_gr,1,0);
        cursor_smt_zuord = prepare_sql ("select smt_zuord.fil,  "
"smt_zuord.hwg,  smt_zuord.mdn,  smt_zuord.verk_st,  "
"smt_zuord.zuord_dat,  smt_zuord.fil_gr "

                                      "from smt_zuord "
                                      "where mdn = ? "
                                      "and fil = ? "
                                      "and verk_st = ? "
                                      "and hwg = ?");
}

int SMT_ZU_CLASS::lese_smt_zuord (short Mdn, short Fil, short verk_st, short hwg)
/**
Tabell smt_zuord lesen.
**/
{
         if (cursor_smt_zuord == -1)
         {
                       prepare ();
         }

         smt_zuord.mdn     = Mdn;
         smt_zuord.fil     = Fil;
         smt_zuord.verk_st = verk_st;
         smt_zuord.hwg     = hwg;

         open_sql (cursor_smt_zuord);         
         fetch_sql (cursor_smt_zuord);         
         return sqlstatus;
}
       
