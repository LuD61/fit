#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "mo_curso.h"
#include "wa_charge.h"

struct WA_CHARGE wa_charge, wa_charge_null;

void WA_CHARGE_CLASS::prepare (void)
{
            TCHAR *sqltext;

            ins_quest ((char *)   &wa_charge.a, SQLDOUBLE, 0);
    out_quest ((char *) &wa_charge.a,3,0);
    out_quest ((char *) wa_charge.charge,0,31);
            cursor = sqlcursor ("select wa_charge.a,  "
"wa_charge.charge from wa_charge "

#line 18 "wa_charge.rpp"
				  "where a = ? ");
    ins_quest ((char *) &wa_charge.a,3,0);
    ins_quest ((char *) wa_charge.charge,0,31);
            sqltext = "update wa_charge set wa_charge.a = ?,  "
"wa_charge.charge = ? "

#line 20 "wa_charge.rpp"
				  "where a = ? ";
            ins_quest ((char *)   &wa_charge.a, SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            ins_quest ((char *)   &wa_charge.a, SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor ("select a from wa_charge "
				  "where a = ? ");
            ins_quest ((char *)   &wa_charge.a, SQLDOUBLE, 0);
            del_cursor = sqlcursor ("delete from wa_charge "
				  "where a = ? ");
    ins_quest ((char *) &wa_charge.a,3,0);
    ins_quest ((char *) wa_charge.charge,0,31);
            ins_cursor = sqlcursor ("insert into wa_charge ("
"a,  charge) "

#line 31 "wa_charge.rpp"
                                      "values "
                                      "(?,?)"); 

#line 33 "wa_charge.rpp"
}

BOOL WA_CHARGE_CLASS::operator== (WA_CHARGE& wa_charge)
{
            if (this->wa_charge.a          != wa_charge.a) return FALSE;  
            if (strcmp (this->wa_charge.charge, wa_charge.charge) != 0) return FALSE;  
            return TRUE;
} 

void WA_CHARGE_CLASS::operator= (WA_CHARGE& wa_charge)
{
            this->wa_charge.a = wa_charge.a;  
            strcpy (this->wa_charge.charge, wa_charge.charge);  
} 
