// LeergutPart.cpp: Implementierung der Klasse CLeergutPart.
//
//////////////////////////////////////////////////////////////////////

#include "LeergutPart.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CLeergutPart::CLeergutPart()
{
	m_Number = "";
	m_Name = "";
}

CLeergutPart::~CLeergutPart()
{

}

void CLeergutPart::Set (CString& source)
{
	Token t;
	t.SetSep (" ");
	t = source;
	if (t.GetAnzToken () > 0)
	{
		m_Number = t.GetToken (0);
	}
	if (t.GetAnzToken () > 1)
	{
		m_Name = t.GetToken (1);
	}

}
