// PtabTable.h: Schnittstelle f�r die Klasse CPtabTable.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PTABTABLE_H__973339E5_DCDF_4044_981B_DF54BB5FC950__INCLUDED_)
#define AFX_PTABTABLE_H__973339E5_DCDF_4044_981B_DF54BB5FC950__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "DataCollection.h"
#include "Ptab.h"

class CPtabTable : public CDataCollection<PTABN>  
{
public:
	CPtabTable();
	virtual ~CPtabTable();
	LPSTR GetArticle (LPSTR Name);
};

#endif // !defined(AFX_PTABTABLE_H__973339E5_DCDF_4044_981B_DF54BB5FC950__INCLUDED_)
