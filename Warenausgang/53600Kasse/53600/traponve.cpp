#include <windows.h>
#include "wmask.h"
#include "dbclass.h"
#include "mo_curso.h"
#include "traponve.h"

struct TRAPONVE traponve, traponve_null;

void TRAPONVE_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &traponve.mdn, 1, 0);
            ins_quest ((char *) &traponve.ls, 2, 0);     
            ins_quest ((char *) traponve.blg_typ, 0, sizeof (traponve.blg_typ));     
            ins_quest ((char *) &traponve.lfd, 2, 0);     
            ins_quest ((char *) traponve.nve, 0, sizeof (traponve.nve));
    out_quest ((char *) &traponve.mdn,1,0);
    out_quest ((char *) &traponve.fil,1,0);
    out_quest ((char *) &traponve.ls,2,0);
    out_quest ((char *) traponve.blg_typ,0,2);
    out_quest ((char *) &traponve.lfd,2,0);
    out_quest ((char *) traponve.nve,0,21);
    out_quest ((char *) &traponve.a,3,0);
    out_quest ((char *) &traponve.delstatus,1,0);
            cursor = prepare_sql ("select traponve.mdn,  "
"traponve.fil,  traponve.ls,  traponve.blg_typ,  traponve.lfd,  "
"traponve.nve,  traponve.a,  traponve.delstatus from traponve "

#line 19 "traponve.rpp"
                                  "where mdn = ? "
                                  "and   ls   = ? "
                                  "and   blg_typ   = ? "
                                  "and   lfd   = ? "
                                  "and   nve = ?");

    ins_quest ((char *) &traponve.mdn,1,0);
    ins_quest ((char *) &traponve.fil,1,0);
    ins_quest ((char *) &traponve.ls,2,0);
    ins_quest ((char *) traponve.blg_typ,0,2);
    ins_quest ((char *) &traponve.lfd,2,0);
    ins_quest ((char *) traponve.nve,0,21);
    ins_quest ((char *) &traponve.a,3,0);
    ins_quest ((char *) &traponve.delstatus,1,0);
            sqltext = "update traponve set traponve.mdn = ?,  "
"traponve.fil = ?,  traponve.ls = ?,  traponve.blg_typ = ?,  "
"traponve.lfd = ?,  traponve.nve = ?,  traponve.a = ?,  "
"traponve.delstatus = ? "

#line 26 "traponve.rpp"
                                  "where mdn = ? "
                                  "and   ls   = ? "
                                  "and   blg_typ   = ? "
                                  "and   lfd   = ? "
                                  "and   nve = ?";
  

            ins_quest ((char *) &traponve.mdn, 1, 0);
            ins_quest ((char *) &traponve.ls, 2, 0);     
            ins_quest ((char *) traponve.blg_typ, 0, sizeof (traponve.blg_typ));     
            ins_quest ((char *) &traponve.lfd, 2, 0);     
            ins_quest ((char *) traponve.nve, 0, sizeof (traponve.nve));
            upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &traponve.mdn,1,0);
    ins_quest ((char *) &traponve.fil,1,0);
    ins_quest ((char *) &traponve.ls,2,0);
    ins_quest ((char *) traponve.blg_typ,0,2);
    ins_quest ((char *) &traponve.lfd,2,0);
    ins_quest ((char *) traponve.nve,0,21);
    ins_quest ((char *) &traponve.a,3,0);
    ins_quest ((char *) &traponve.delstatus,1,0);
            ins_cursor = prepare_sql ("insert into traponve ("
"mdn,  fil,  ls,  blg_typ,  lfd,  nve,  a,  delstatus) "

#line 41 "traponve.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?)"); 

#line 43 "traponve.rpp"

            ins_quest ((char *) &traponve.mdn, 1, 0);
            ins_quest ((char *) &traponve.ls, 2, 0);     
            ins_quest ((char *) traponve.blg_typ, 0, sizeof (traponve.blg_typ));     
            ins_quest ((char *) &traponve.lfd, 2, 0);     
            ins_quest ((char *) traponve.nve, 0, sizeof (traponve.nve));
            test_upd_cursor = prepare_sql ("select a from traponve "
                                  "where mdn = ? "
                                  "and   ls   = ? "
                                  "and   blg_typ   = ? "
                                  "and   lfd   = ? "
                                  "and   nve = ?");

            ins_quest ((char *) &traponve.mdn, 1, 0);
            ins_quest ((char *) &traponve.ls, 2, 0);     
            ins_quest ((char *) traponve.blg_typ, 0, sizeof (traponve.blg_typ));     
            ins_quest ((char *) &traponve.lfd, 2, 0);     
            ins_quest ((char *) traponve.nve, 0, sizeof (traponve.nve));
            del_cursor = prepare_sql ("delete from traponve "
                                  "where mdn = ? "
                                  "and   ls   = ? "
                                  "and   blg_typ   = ? "
                                  "and   lfd   = ? "
                                  "and   nve = ?");

}

