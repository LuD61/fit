#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "a_kun.h"
#include "kun.h"
#include "dbfunc.h"

struct KUN kun;
struct A_KUN a_kun, a_kun_null;

void KUN_CLASS::prepare  (void)
{
    ins_quest ((char *) &kun.mdn,1,0);
    ins_quest ((char *) &kun.fil,1,0);
    ins_quest ((char *) &kun.kun,2,0);
    
    out_quest ((char *) &kun.mdn,1,0);
    out_quest ((char *) &kun.fil,1,0);
    out_quest ((char *) &kun.kun,2,0);
    out_quest ((char *) &kun.adr1,2,0);
    out_quest ((char *) &kun.adr2,2,0);
    out_quest ((char *) &kun.adr3,2,0);
    out_quest ((char *) &kun.kun_seit,2,0);
    out_quest ((char *) &kun.txt_nr1,2,0);
    out_quest ((char *) kun.frei_txt1,0,65);
    out_quest ((char *) kun.kun_krz1,0,17);
    out_quest ((char *) kun.kun_bran,0,2);
    out_quest ((char *) kun.kun_krz2,0,17);
    out_quest ((char *) kun.kun_krz3,0,17);
    out_quest ((char *) &kun.kun_typ,1,0);
    out_quest ((char *) &kun.bbn,2,0);
    out_quest ((char *) &kun.pr_stu,1,0);
    out_quest ((char *) &kun.pr_lst,2,0);
    out_quest ((char *) kun.vereinb,0,6);
    out_quest ((char *) &kun.inka_nr,2,0);
    out_quest ((char *) &kun.vertr1,2,0);
    out_quest ((char *) &kun.vertr2,2,0);
    out_quest ((char *) kun.statk_period,0,2);
    out_quest ((char *) kun.a_period,0,2);
    out_quest ((char *) &kun.sprache,1,0);
    out_quest ((char *) &kun.txt_nr2,2,0);
    out_quest ((char *) kun.frei_txt2,0,65);
    out_quest ((char *) kun.freifeld1,0,9);
    out_quest ((char *) kun.freifeld2,0,9);
    out_quest ((char *) &kun.tou,2,0);
    out_quest ((char *) kun.vers_art,0,3);
    out_quest ((char *) &kun.lief_art,1,0);
    out_quest ((char *) kun.fra_ko_ber,0,3);
    out_quest ((char *) &kun.rue_schei,1,0);
    out_quest ((char *) kun.form_typ1,0,3);
    out_quest ((char *) &kun.auflage1,1,0);
    out_quest ((char *) kun.freifeld3,0,9);
    out_quest ((char *) kun.freifeld4,0,9);
    out_quest ((char *) &kun.zahl_art,1,0);
    out_quest ((char *) &kun.zahl_ziel,1,0);
    out_quest ((char *) kun.form_typ2,0,3);
    out_quest ((char *) &kun.auflage2,1,0);
    out_quest ((char *) &kun.txt_nr3,2,0);
    out_quest ((char *) kun.frei_txt3,0,65);
    out_quest ((char *) kun.nr_bei_rech,0,17);
    out_quest ((char *) kun.rech_st,0,3);
    out_quest ((char *) &kun.sam_rech,1,0);
    out_quest ((char *) &kun.einz_ausw,1,0);
    out_quest ((char *) &kun.gut,1,0);
    out_quest ((char *) kun.rab_schl,0,5);
    out_quest ((char *) &kun.bonus1,3,0);
    out_quest ((char *) &kun.bonus2,3,0);
    out_quest ((char *) &kun.tdm_grenz1,3,0);
    out_quest ((char *) &kun.tdm_grenz2,3,0);
    out_quest ((char *) &kun.jr_plan_ums,3,0);
    out_quest ((char *) kun.deb_kto,0,9);
    out_quest ((char *) &kun.kred_lim,3,0);
    out_quest ((char *) &kun.inka_zaehl,1,0);
    out_quest ((char *) kun.bank_kun,0,37);
    out_quest ((char *) &kun.blz,2,0);
    out_quest ((char *) kun.kto_nr,0,17);
    out_quest ((char *) &kun.hausbank,1,0);
    out_quest ((char *) &kun.kun_of_po,1,0);
    out_quest ((char *) &kun.kun_of_lf,1,0);
    out_quest ((char *) &kun.kun_of_best,1,0);
    out_quest ((char *) &kun.delstatus,1,0);
    out_quest ((char *) kun.kun_bran2,0,3);
    out_quest ((char *) &kun.rech_fuss_txt,2,0);
    out_quest ((char *) &kun.ls_fuss_txt,2,0);
    out_quest ((char *) kun.ust_id,0,12);
    out_quest ((char *) &kun.rech_kopf_txt,2,0);
    out_quest ((char *) &kun.ls_kopf_txt,2,0);
    out_quest ((char *) kun.gn_pkt_kz,0,2);
    out_quest ((char *) kun.sw_rab,0,2);
    out_quest ((char *) kun.bbs,0,9);
    out_quest ((char *) &kun.inka_nr2,2,0);
    out_quest ((char *) &kun.sw_fil_gr,1,0);
    out_quest ((char *) &kun.sw_fil,1,0);
    out_quest ((char *) kun.ueb_kz,0,2);
    out_quest ((char *) kun.modif,0,2);
    out_quest ((char *) &kun.kun_leer_kz,1,0);
    out_quest ((char *) kun.ust_id16,0,17);
    out_quest ((char *) kun.iln,0,17);
    out_quest ((char *) &kun.waehrung,1,0);
    out_quest ((char *) &kun.pr_ausw,1,0);
    out_quest ((char *) kun.pr_hier,0,2);
    out_quest ((char *) &kun.pr_ausw_ls,1,0);
    out_quest ((char *) &kun.pr_ausw_re,1,0);
    out_quest ((char *) &kun.kst,2,0);
    out_quest ((char *) &kun.kond_kun,2,0);
    out_quest ((char *) &kun.kun_gr1,1,0);
    out_quest ((char *) &kun.kun_gr2,1,0);
    out_quest ((char *) &kun.kun_schema,1,0);
    out_quest ((char *) &kun.etikett,1,0);
     cursor_kun = prepare_sql ("select kun.mdn,  kun.fil,  kun.kun,  "
"kun.adr1,  kun.adr2,  kun.adr3,  kun.kun_seit,  kun.txt_nr1,  kun.frei_txt1,  "
"kun.kun_krz1,  kun.kun_bran,  kun.kun_krz2,  kun.kun_krz3,  kun.kun_typ,  "
"kun.bbn,  kun.pr_stu,  kun.pr_lst,  kun.vereinb,  kun.inka_nr,  kun.vertr1,  "
"kun.vertr2,  kun.statk_period,  kun.a_period,  kun.sprache,  kun.txt_nr2,  "
"kun.frei_txt2,  kun.freifeld1,  kun.freifeld2,  kun.tou,  kun.vers_art,  "
"kun.lief_art,  kun.fra_ko_ber,  kun.rue_schei,  kun.form_typ1,  "
"kun.auflage1,  kun.freifeld3,  kun.freifeld4,  kun.zahl_art,  "
"kun.zahl_ziel,  kun.form_typ2,  kun.auflage2,  kun.txt_nr3,  "
"kun.frei_txt3,  kun.nr_bei_rech,  kun.rech_st,  kun.sam_rech,  "
"kun.einz_ausw,  kun.gut,  kun.rab_schl,  kun.bonus1,  kun.bonus2,  "
"kun.tdm_grenz1,  kun.tdm_grenz2,  kun.jr_plan_ums,  kun.deb_kto,  "
"kun.kred_lim,  kun.inka_zaehl,  kun.bank_kun,  kun.blz,  kun.kto_nr,  "
"kun.hausbank,  kun.kun_of_po,  kun.kun_of_lf,  kun.kun_of_best,  "
"kun.delstatus,  kun.kun_bran2,  kun.rech_fuss_txt,  kun.ls_fuss_txt,  "
"kun.ust_id,  kun.rech_kopf_txt,  kun.ls_kopf_txt,  kun.gn_pkt_kz,  "
"kun.sw_rab,  kun.bbs,  kun.inka_nr2,  kun.sw_fil_gr,  kun.sw_fil,  "
"kun.ueb_kz,  kun.modif,  kun.kun_leer_kz,  kun.ust_id16,  kun.iln,  "
"kun.waehrung,  kun.pr_ausw,  kun.pr_hier,  kun.pr_ausw_ls,  "
"kun.pr_ausw_re, kun.kst, kond_kun, kun_gr1, kun_gr2, kun_schema, etikett "
                          "from kun "
                          "where mdn = ? "
                          "and fil = ? "
                          "and kun = ?");
}

void KUN_CLASS::prepare_a_kun  (void)
{
    ins_quest ((char *) &a_kun.mdn,1,0);
    ins_quest ((char *) &a_kun.fil,1,0);
    ins_quest ((char *) &a_kun.kun,2,0);
    ins_quest ((char *) &a_kun.a,3,0);

    out_quest ((char *) &a_kun.mdn,1,0);
    out_quest ((char *) &a_kun.fil,1,0);
    out_quest ((char *) &a_kun.kun,2,0);
    out_quest ((char *) &a_kun.a,3,0);
    out_quest ((char *) a_kun.a_kun,0,13);
    out_quest ((char *) a_kun.a_bz1,0,25);
    out_quest ((char *) &a_kun.me_einh_kun,1,0);
    out_quest ((char *) &a_kun.inh,3,0);
    out_quest ((char *) a_kun.kun_bran2,0,3);
    out_quest ((char *) &a_kun.tara,3,0);
    out_quest ((char *) &a_kun.ean,3,0);
    out_quest ((char *) a_kun.a_bz2,0,25);
    out_quest ((char *) &a_kun.hbk_ztr,1,0);
    out_quest ((char *) &a_kun.kopf_text,2,0);
    out_quest ((char *) a_kun.pr_rech_kz,0,2);
    out_quest ((char *) a_kun.modif,0,2);
    cursor_a_kun = prepare_sql ("select a_kun.mdn,  a_kun.fil,  "
"a_kun.kun,  a_kun.a,  a_kun.a_kun,  a_kun.a_bz1,  a_kun.me_einh_kun,  "
"a_kun.inh,  a_kun.kun_bran2,  a_kun.tara,  a_kun.ean,  a_kun.a_bz2,  "
"a_kun.hbk_ztr,  a_kun.kopf_text,  a_kun.pr_rech_kz,  a_kun.modif from a_kun "
                              "where a_kun.mdn = ? "
                              "and   a_kun.fil = ? "
                              "and   a_kun.kun = ? "
                              "and   a_kun.a = ?");
}
    
void KUN_CLASS::prepare_a_kun_bran  (void)
{
    ins_quest ((char *) &a_kun.mdn,1,0);
    ins_quest ((char *) &a_kun.fil,1,0);
    ins_quest ((char *) a_kun.kun_bran2, 0, 3);
    ins_quest ((char *) &a_kun.a,3,0);

    out_quest ((char *) &a_kun.mdn,1,0);
    out_quest ((char *) &a_kun.fil,1,0);
    out_quest ((char *) &a_kun.kun,2,0);
    out_quest ((char *) &a_kun.a,3,0);
    out_quest ((char *) a_kun.a_kun,0,13);
    out_quest ((char *) a_kun.a_bz1,0,25);
    out_quest ((char *) &a_kun.me_einh_kun,1,0);
    out_quest ((char *) &a_kun.inh,3,0);
    out_quest ((char *) a_kun.kun_bran2,0,3);
    out_quest ((char *) &a_kun.tara,3,0);
    out_quest ((char *) &a_kun.ean,3,0);
    out_quest ((char *) a_kun.a_bz2,0,25);
    out_quest ((char *) &a_kun.hbk_ztr,1,0);
    out_quest ((char *) &a_kun.kopf_text,2,0);
    out_quest ((char *) a_kun.pr_rech_kz,0,2);
    out_quest ((char *) a_kun.modif,0,2);
    cursor_a_kun_bran = prepare_sql ("select a_kun.mdn,  a_kun.fil,  "
"a_kun.kun,  a_kun.a,  a_kun.a_kun,  a_kun.a_bz1,  a_kun.me_einh_kun,  "
"a_kun.inh,  a_kun.kun_bran2,  a_kun.tara,  a_kun.ean,  a_kun.a_bz2,  "
"a_kun.hbk_ztr,  a_kun.kopf_text,  a_kun.pr_rech_kz,  a_kun.modif from a_kun "
                              "where a_kun.mdn = ? "
                              "and   a_kun.fil = ? "
                              "and   a_kun.kun_bran2 = ? "
                              "and   a_kun.a = ?");
}
    

int KUN_CLASS::lese_kun (short mdn, short fil, long kun_nr)
/**
Tabelle kun mit Kunden-Nummer lesen.
**/
{
         if (cursor_kun == -1)
         {
                      prepare ();
         }
         kun.mdn = mdn;
         kun.fil = fil;
         kun.kun  = kun_nr ;
         open_sql (cursor_kun);
         fetch_sql (cursor_kun);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int KUN_CLASS::lese_kun (void)
/**
Naechsten Satz aus Tabelle kun lesen.
**/
{
         fetch_sql (cursor_kun);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int KUN_CLASS::lese_a_kun (short mdn, short fil, long kun_nr, double a)
/**
Tabelle a_kun mit Kunden-Nummer lesen.
**/
{
         if (cursor_a_kun == -1)
         {
                      prepare_a_kun ();
         }
         a_kun.mdn = mdn;
         a_kun.fil = fil;
         a_kun.kun = kun_nr ;
         a_kun.a   = a;
         open_sql (cursor_a_kun);
         fetch_sql (cursor_a_kun);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int KUN_CLASS::lese_a_kun (void)
/**
Naechsten Satz aus Tabelle a_kun lesen.
**/
{
         fetch_sql (cursor_a_kun);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int KUN_CLASS::lese_a_kun_bran (short mdn, short fil, char *kun_bran,
                                double a)
/**
Tabelle a_kun mit Kunden-Nummer lesen.
**/
{
         if (cursor_a_kun_bran == -1)
         {
                      prepare_a_kun_bran ();
         }
         a_kun.mdn = mdn;
         a_kun.fil = fil;
         strcpy (a_kun.kun_bran2, kun_bran);
         a_kun.a   = a;
         open_sql (cursor_a_kun_bran);
         fetch_sql (cursor_a_kun_bran);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int KUN_CLASS::lese_a_kun_bran (void)
/**
Naechsten Satz aus Tabelle a_kun lesen.
**/
{
         fetch_sql (cursor_a_kun_bran);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

