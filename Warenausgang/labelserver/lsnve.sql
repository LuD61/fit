{ TABLE "Administ".lsnve row size = 122 number of columns = 20 index size = 75 }
create table "Administ".lsnve 
  (
    mdn smallint,
    fil smallint,
    nve_posi integer,
    nve char(9),
    nve_palette char(9),
    ls integer,
    a decimal(13,0),
    me decimal(8,3),
    me_einh smallint,
    gue smallint,
    palette_kz smallint,
    tag date,
    mhd date,
    brutto_gew decimal(12,3),
    verp_art integer,
    ps_art decimal(13,0),
    ps_stck decimal(12,3),
    ps_art1 decimal(13,0),
    ps_stck1 decimal(12,3),
    charge char(20)
  ) in fit_stat extent size 32 next size 256 lock mode row;
revoke all on "Administ".lsnve from "public";

create index "Administ".i01lsnve on "Administ".lsnve (ls,a,nve_posi,
    mdn,fil);
create index "Administ".i02lsnve on "Administ".lsnve (nve);
create index "Administ".i03lsnve on "Administ".lsnve (nve_palette);
    




