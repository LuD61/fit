# IDE:
#/I "c:\informix\client-sdk\incl\esql" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Gm /EHsc /RTC1 /MLd /Fo"Debug/" /Fd"Debug/vc70.pdb" /W3 /nologo /c /Wp64 /ZI /TP

# -MD = binden mit LIBMSVCRT.LIB (Standard)
# -MDd = binden mit LIBMSVCRT.LIB (Debugversion)
# -ML = mit LIBC.LIB
# -MLd = mit LIBC.LIB (Debugversion)

ESQL_FLAGS=-dcmdl -subsystem:windows -c

#ESQL_C_FLAGS=-cc -nologo -D"WIN32" -D"_DEBUG" -D"_WINDOWS" -D"_MBCS" -W3 -Wp64 -MLd -ZI -TP
ESQL_C_FLAGS=-cc -nologo -D"WIN32" -D"_DEBUG" -D"_WINDOWS" -D"_MBCS" -W3 -Wp64 -wd4996 -ZI -TP


gxsql.obj : gxsql.ec
	esql $(ESQL_FLAGS) $(ESQL_C_FLAGS) gxsql.ec

ifmxdiag.obj : ifmxdiag.ec
	esql $(ESQL_FLAGS) $(ESQL_C_FLAGS) ifmxdiag.ec

