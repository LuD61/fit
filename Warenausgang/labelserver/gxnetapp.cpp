#include "stdafx.h"
#include "gxnetapp.h"
#include "gxcom.h"
#include "msgline.h"
#include "gxaedvsoc.h"
#include "ddeserver.h"
#include "items.h"
#include "gxsql.h"

#include <time.h>

#define WMAX 5
#define NCAB 10
#define NAKTIVSAETZE 10

char tbuf[2000];

char *pZutaten;

int fehlerfall;

char szCAB[NCAB][200];
char szAktivSatz[NAKTIVSAETZE][7];

us lenCAB[NCAB];

char SendTEXTE[7];
char SendZUTATEN[7];
char SendPREIS[7];
char SendETIKETT[7];
char SendETIKETT_S1[7];
char SendETIKETT_S2[7];
char SendCODE1[7];
char SendCODE2[7];
char SendCODE3[7];
char SendCODE4[7];
char SendCODE5[7];
char SendDATUM1[7];
char SendTAGE1[7];
char SendDATUM2[7];
char SendUHR12[7];
char Transport[7];
char SendTara[7];
char Gewichtsklassen[2];
char Automatenpar[2];
// char KuBaTara[2];
char gxformatstr[7];
char gxsendekanal[2];
char summenvorwahl[5];
char cabtable[20];
char cmdline[200];
char quitcheck[2];
char pazstatistik[2];
int wartezeit;

/*
STARTUPINFO sti;
PROCESS_INFORMATION pi;
*/

static long tmpstksum, tmpnetsum;

void initSend( void )
{
	strcpy( SendTEXTE, itmval("SEND_TEXTE") );
	fprintf( fperr, "SEND_TEXTE = %s\n", SendTEXTE );

	strcpy( SendZUTATEN, itmval("ZUTATEN") );
	fprintf( fperr, "SendZUTATEN = %s\n", SendZUTATEN );

	strcpy( SendPREIS, itmval("SEND_PREIS") );
	fprintf( fperr, "SEND_PREIS = %s\n", SendPREIS );
	
	strcpy( SendETIKETT, itmval("SEND_ETIKETT") );
	fprintf( fperr, "SEND_ETIKETT = %s\n", SendETIKETT );

	strcpy( SendETIKETT_S1, itmval("SEND_ETIKETT_S1") );
	fprintf( fperr, "SEND_ETIKETT_S1 = %s\n", SendETIKETT_S1 );

	strcpy( SendETIKETT_S2, itmval("SEND_ETIKETT_S2") );
	fprintf( fperr, "SEND_ETIKETT_S2 = %s\n", SendETIKETT_S2 );
	
	strcpy( SendCODE1, itmval("SEND_CODE1") );
	fprintf( fperr, "SEND_CODE1 = %s\n", SendCODE1 );
	
	strcpy( SendCODE2, itmval("SEND_CODE2") );
	fprintf( fperr, "SEND_CODE2 = %s\n", SendCODE2 );

	strcpy( SendCODE3, itmval("SEND_CODE3") );
	fprintf( fperr, "SEND_CODE3 = %s\n", SendCODE3 );

	strcpy( SendCODE4, itmval("SEND_CODE4") );
	fprintf( fperr, "SEND_CODE4 = %s\n", SendCODE4 );

	strcpy( SendCODE5, itmval("SEND_CODE5") );
	fprintf( fperr, "SEND_CODE5 = %s\n", SendCODE5 );

	strcpy( SendDATUM1, itmval("SEND_DATUM1") );
	fprintf( fperr, "SEND_DATUM1 = %s\n", SendDATUM1 );

	strcpy( SendTAGE1, itmval("SEND_TAGE1") );
	fprintf( fperr, "SEND_TAGE1 = %s\n", SendTAGE1 );

	strcpy( SendDATUM2, itmval("SEND_DATUM2") );
	fprintf( fperr, "SEND_DATUM2 = %s\n", SendDATUM2 );
	
	/* 16.10.03 Transportsteuerung ggf. abschalten, z.B. bei GLP */ 
	strcpy( Transport, itmval("TRANSPORT") );
	fprintf( fperr, "TRANSPORT = %s\n", Transport );

	/* 24.08.06 */ 
	strcpy( SendTara, itmval("SEND_TARA") );
	fprintf( fperr, "SEND_TARA = %s\n", SendTara );

	strcpy( Gewichtsklassen, itmval("GEWICHTSKLASSEN") );
	fprintf( fperr, "GEWICHTSKLASSEN = %s\n", Gewichtsklassen );

	strcpy( Automatenpar, itmval("AUTOMATENPAR") );
	fprintf( fperr, "AUTOMATENPAR = %s\n", Automatenpar );

	
	/*
	strcpy( KuBaTara, itmval("KuBaTara") );
	fprintf( fperr, "KuBaTara = %s\n", KuBaTara );
    */

	strcpy( SendUHR12, itmval("SEND_UHR12") );
	fprintf( fperr, "SEND_UHR12 = %s\n", SendUHR12 );

	strcpy( gxformatstr, itmval("GXFORMAT") );
	fprintf( fperr, "GXFORMAT = %s\n", gxformatstr );
    if ( strcmp( gxformatstr, "GXDATA" ) == 0 )
	{
		gxformat = GXDATA;
	}
	else
	{
		gxformat = GXNET;
	}
   	strcpy( summenvorwahl, itmval("SUMMENVORWAHL") );
	fprintf( fperr, "VORWAHL S1 S2 S3 = %c %c %c\n",
		summenvorwahl[1], summenvorwahl[2], summenvorwahl[3] );

	strcpy( gxsendekanal, itmval("GX_SENDEKANAL") );
	fprintf( fperr, "GX_SENDEKANAL = %s\n", gxsendekanal );

	wartezeit = atoi( itmval( "WARTEZEIT" ) );
	fprintf( fperr, "WARTEZEIT = %5d\n", wartezeit );

	strcpy( quitcheck, itmval("QUITCHECK") );
	fprintf( fperr, "QUITCHECK = %s\n", quitcheck );

	strcpy( pazstatistik, itmval("PAZSTATISTIK") );
	fprintf( fperr, "PAZSTATISTIK = %s\n", pazstatistik );

	pazmdn = atoi( itmval("PAZMDN") );
	fprintf( fperr, "PAZMDN = %d\n", pazmdn );
}

void initAktivSaetze( void )
{
	int i;
	char key[12];
	strcpy( szAktivSatz[0], "000000" ); /* bloss kein N oder J */
	for ( i = 1; i < NAKTIVSAETZE; i++ )
	{
		wsprintf( key, "AKTIVSATZ%1d", i );
		strcpy( szAktivSatz[i], itmval( key ) );
		fprintf( fperr, "AKTIVSATZ%1d = %s\n", i, szAktivSatz[i] );
	}
}

void formCAB( GGCAB_t * ggcab )
{
	int i;
	if ( strcmp( cabtable, "PAZ_CAB" ) == 0 )
	{
		Query_paz_cab( ggcab );
	}
	else
	{
	    i = ggcab->cabnr;
        strcpy( ggcab->cab, szCAB[i] );
		ggcab->cablen = lenCAB[i];
	}
}

void initCAB( void )
{
	int i;
	char key[12];
	if ( strcmp( anwender, "Boesinger" ) == 0 )
	{
		return;
    }
	strcpy( cabtable, itmval("CAB_TABLE") );
	fprintf( fperr, "CAB_TABLE = %s\n", cabtable );
	if ( strcmp( cabtable, "PAZ_CAB" ) != 0 )
	{
		for ( i = 0; i < NCAB; i++ )
		{
			wsprintf( key, "CAB%1d", i );
			strcpy( szCAB[i], itmval( key ) );
			lenCAB[i] = (us) strlen( szCAB[i] ) + 1;
			fprintf( fperr, "CAB%1d = %s\n", i, szCAB[i] );
		}
	}
	ggcab3.cabnr = atoi( itmval( "CAB_CODE3" ) );
	ggcab4.cabnr = atoi( itmval( "CAB_CODE4" ) );
	ggcab5.cabnr = atoi( itmval( "CAB_CODE5" ) );
	fprintf( fperr, "CAB_CODE3/4/5 = %1d/%1d/%1d\n",
		ggcab3.cabnr, ggcab4.cabnr, ggcab5.cabnr );
    formCAB( &ggcab3 );
    formCAB( &ggcab4 );
    formCAB( &ggcab5 );
}


void sum123clear(  )
{
    us len;
    char *p;
    int srv;
    int w;
	srv = 2;
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
		p+= formfu( p, XCW_SUM_CLEAR, ETI_SUMME_1 );
		p+= formfu( p, XCW_SUM_CLEAR, ETI_SUMME_2 );
		p+= formfu( p, XCW_SUM_CLEAR, ETI_SUMME_3 );
		len = (us)(p - tbuf);
		len = formfu( tbuf, LGV_SEQUENZ, len, tbuf );
		toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );

	tmpstksum = ggdat.stksum = 0;
    tmpnetsum = ggdat.netsum = 0;
	ggdat.stksum2x = ggdat.stksum3x = 0;
	ggdat.netsum2x = ggdat.netsum3x = 0;
	ggdat.stksum2 = ggdat.stksum3 = 0;
	ggdat.netsum2 = ggdat.netsum3 = 0;

	psw_etikettyp = ETI_INIT;
	formErgebnis('R');
}

void sendGXMSG( int srv, char *msg  )
{
    us len;
	int w;
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		len = formfu( tbuf, GGT_ATX2, (us) strlen(msg), msg  );
		len = toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void sendGGATX( int srv, int tf  )
{
    us len;
    char *p;
	int w;
	if ( tf  > 2 && strcmp( anwender, "Dietz" ) == 0 )
	{
		return;
	}
	if ( tf  > 7 && strcmp( anwender, "Boesinger" ) == 0 )
	{
		return;
	}
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
		switch ( tf )
		{
		case 1: p += formfu( p, GGT_ATX1, ggatx1.atxlen, ggatx1.atx ); break;
		case 2: p += formfu( p, GGT_ATX2, ggatx2.atxlen, ggatx2.atx ); break;
		case 3: p += formfu( p, GGT_ATX3, ggatx3.atxlen, ggatx3.atx ); break;
		case 4: p += formfu( p, GGT_ATX4, ggatx4.atxlen, ggatx4.atx ); break;
		case 5: p += formfu( p, GGT_ATX5, ggatx5.atxlen, ggatx5.atx ); break;
		case 6: p += formfu( p, GGT_ATX6, ggatx6.atxlen, ggatx6.atx ); break;
		case 7: p += formfu( p, GGT_ATX7, ggatx7.atxlen, ggatx7.atx ); break;
		case 8: p += formfu( p, GGT_ATX8, ggatx8.atxlen, ggatx8.atx ); break;
		case 9: p += formfu( p, GGT_ATX9, ggatx9.atxlen, ggatx9.atx ); break;
		case 10: p += formfu( p, GGT_ATX10, ggatx10.atxlen, ggatx10.atx ); break;
		case 11: p += formfu( p, GGT_ATX11, ggatx11.atxlen, ggatx11.atx ); break;
		case 12: p += formfu( p, GGT_ATX12, ggatx12.atxlen, ggatx12.atx ); break;
		case 13: p += formfu( p, GGT_ATX13, ggatx13.atxlen, ggatx13.atx ); break;
		case 14: p += formfu( p, GGT_ATX14, ggatx14.atxlen, ggatx14.atx ); break;
		case 15: p += formfu( p, GGT_ATX15, ggatx15.atxlen, ggatx15.atx ); break;
		case 16: p += formfu( p, GGT_ATX16, ggatx16.atxlen, ggatx16.atx ); break;
		case 17: p += formfu( p, GGT_ATX17, ggatx17.atxlen, ggatx17.atx ); break;
		case 18: p += formfu( p, GGT_ATX18, ggatx18.atxlen, ggatx18.atx ); break;
		case 19: p += formfu( p, GGT_ATX19, ggatx19.atxlen, ggatx19.atx ); break;
		case 20: p += formfu( p, GGT_ATX20, ggatx20.atxlen, ggatx20.atx ); break;
		}
		len = (us)(p - tbuf);
		len = toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void sendAllGGATX( int srv  )
{
	us len;
	char *p;
	int c;
	int w;
	int zutaten;

	/* TF2 ggf. ohne Zutaten, erweiterte Logik Roesener 21.05.06 */
	if ( srv == 5 )
	{
		zutaten = 'N';
	}
	else
	{
		zutaten = ggdat.zutaten;
	}

	if (SendTEXTE[srv] != 'J')
		return;

	fprintf( fperr, _T("ATX1:%s\n"), ggatx1.atx );
	fprintf( fperr, _T("ATX2:%s\n"), ggatx2.atx );
	fprintf( fperr, _T("ATX3:%s\n"), ggatx3.atx );
	fprintf( fperr, _T("ATX4:%s\n"), ggatx4.atx );
	fprintf( fperr, _T("ATX5:%s\n"), ggatx5.atx );
	fprintf( fperr, _T("ATX6:%s\n"), ggatx6.atx );
	fprintf( fperr, _T("ATX7:%s\n"), ggatx7.atx );
	fprintf( fperr, _T("ATX8:%s\n"), ggatx8.atx );
	fprintf( fperr, _T("ATX9:%s\n"), ggatx9.atx );

	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
		p += formfu( p, GGT_ATX1, ggatx1.atxlen, ggatx1.atx );
		if ( SendZUTATEN[srv] == 'N' && zutaten == 'N' && pZutaten != NULL )
		{
			/* TF2 ggf. ohne Zutaten, Wunsch Roesener 29.03.04 */
			c = *pZutaten;
			*pZutaten = '\0';
			ggatx1.atxlen = (us) strlen( ggatx1.atx );
		}
		p += formfu( p, GGT_ATX2, ggatx2.atxlen, ggatx2.atx );
		if ( SendZUTATEN[srv] == 'N' && zutaten == 'N' && pZutaten != NULL )
		{
			*pZutaten = c;
			ggatx2.atxlen = (us) strlen( ggatx2.atx );
		}
		p += formfu( p, GGT_ATX3, ggatx3.atxlen, ggatx3.atx );
		p += formfu( p, GGT_ATX4, ggatx4.atxlen, ggatx4.atx );
		p += formfu( p, GGT_ATX5, ggatx5.atxlen, ggatx5.atx );
		p += formfu( p, GGT_ATX6, ggatx6.atxlen, ggatx6.atx );
		p += formfu( p, GGT_ATX7, ggatx7.atxlen, ggatx7.atx );
		p += formfu( p, GGT_ATX8, ggatx8.atxlen, ggatx8.atx );
		p += formfu( p, GGT_ATX9, ggatx9.atxlen, ggatx9.atx );
		len = (us)(p - tbuf);
		len = formfu( tbuf, LGV_SEQUENZ, len, tbuf );
		len = toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
		Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );

	fprintf( fperr, _T("ATX10:%s\n"), ggatx10.atx );
	fprintf( fperr, _T("ATX11:%s\n"), ggatx11.atx );
	fprintf( fperr, _T("ATX12:%s\n"), ggatx12.atx );
	fprintf( fperr, _T("ATX13:%s\n"), ggatx13.atx );
	fprintf( fperr, _T("ATX14:%s\n"), ggatx14.atx );
	fprintf( fperr, _T("ATX15:%s\n"), ggatx15.atx );
	fprintf( fperr, _T("ATX16:%s\n"), ggatx16.atx );
	fprintf( fperr, _T("ATX17:%s\n"), ggatx17.atx );
	fprintf( fperr, _T("ATX18:%s\n"), ggatx18.atx );
	fprintf( fperr, _T("ATX19:%s\n"), ggatx19.atx );
	fprintf( fperr, _T("ATX20:%s\n"), ggatx20.atx );
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
		p += formfu( p, GGT_ATX10, ggatx10.atxlen, ggatx10.atx );
		p += formfu( p, GGT_ATX11, ggatx11.atxlen, ggatx11.atx );
		p += formfu( p, GGT_ATX12, ggatx12.atxlen, ggatx12.atx );
		p += formfu( p, GGT_ATX13, ggatx13.atxlen, ggatx13.atx );
		p += formfu( p, GGT_ATX14, ggatx14.atxlen, ggatx14.atx );
		p += formfu( p, GGT_ATX15, ggatx15.atxlen, ggatx15.atx );
		p += formfu( p, GGT_ATX17, ggatx17.atxlen, ggatx17.atx );
		p += formfu( p, GGT_ATX18, ggatx18.atxlen, ggatx18.atx );
		p += formfu( p, GGT_ATX19, ggatx19.atxlen, ggatx19.atx );
		p += formfu( p, GGT_ATX20, ggatx20.atxlen, ggatx20.atx );
		len = (us)(p - tbuf);
		len = formfu( tbuf, LGV_SEQUENZ, len, tbuf );
		len = toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
		Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void sendGGDAT( int srv )
{
    us len;
    char *p;
	int w;
	time_t t;
	long lHHMM;
	char szHHMM[6];

	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
		dim_prs = ggdat.devise;
		/*
		p += formfu( p, GGL_ETIKETPARNR, ggdat.etin );
		p += formfu( p, GGL_DTXNR2, ggdat.dtxnr2 );
		p += formfu( p, GGL_LOGONR, ggdat.logo1 );
		p += formfu( p, GGL_LOGONR2, ggdat.logo2 );
		p += formfu( p, GGW_PLUW_TARA, 1 );
		p += formfu( p, GGW_AUSZEICHART_WECHS, 0x02 );
		p += formfu( p, GGW_LAND_SKND, ggdat.devise2 ) ;
		p += formfu( p, GGW_LAND, ggdat.devise );
		if (ggdat.aart != 0 && ggdat.stk_pro_pck > 0)
		p += formfu( p, GGL_ETIKETFOLGE_VORWAHL, ggdat.stk_pro_pck );
		else       
		p += formfu( p, GGL_ETIKETFOLGE_VORWAHL, 0 );
		p += formfu( p, GGL_CABNR1, ggdat.cabnr1 );
		p += formfu( p, GGD_PRS_SPREIS, dim_prs, 0 );
		p += formfu( p, GGW_SUM2VORWAHL_ART, ggdat.sum2vwa );
		p += formfu( p, GGL_STCK_SUM2_VORWAHL, ggdat.stk2vw );
		p += formfu( p, GGW_SUM3VORWAHL_ART, ggdat.sum3vwa );
		p += formfu( p, GGL_STCK_SUM3_VORWAHL, ggdat.stk3vw );
		*/
		p += formfu( p, GGW_AUSZEICH_ART, ggdat.aart );
		p += formfu( p, GGW_PREIS_BERECHNUNG, ggdat.prbe );
		fprintf( fperr, "SendTara: %s\n", SendTara ); 
		if ( SendTara[srv] == 'J' )
		{
		    p += formfu( p, GGD_GEW_TARA_EINZEL, DIM_GEW, ggdat.tara );
		}
		p += formfu( p ,GGD_GEW_NETTO_EINZEL, DIM_GEW , ggdat.fgew );
		/* Dietz: */
		p += formfu( p ,GGL_CHARGENNR, ggdat.chargennr );
		// p += formfu( p ,GGL_LOSNR, ggdat.chargennr );
		p += formfu( p ,GGL_PLUNR, ggdat.plunr );
       		
		switch (srv)
		{

		case 2: /* master */
			if ( summenvorwahl[1] == 'J' )
			{
			    p += formfu( p, GGW_SUM1VORWAHL_ART, ggdat.sum1vwa );
			    p += formfu( p, GGL_STCK_SUM1_VORWAHL, ggdat.sum1vw );
			}
			if ( summenvorwahl[2] == 'J' )
			{
			    p += formfu( p, GGW_SUM2VORWAHL_ART, ggdat.sum2vwa );
			    p += formfu( p, GGL_STCK_SUM2_VORWAHL, ggdat.sum2vw );
			}
			if ( summenvorwahl[3] == 'J' )
			{
			    p += formfu( p, GGW_SUM3VORWAHL_ART, ggdat.sum3vwa );
			    p += formfu( p, GGL_STCK_SUM3_VORWAHL, ggdat.sum3vw );
			}
			if ( szAktivSatz[ggdat.sonder_eti_krit][srv] == 'J' )
			{
				/* Aktiviere */
    			p += formfu( p, GGW_DRUCKINTERN_ENABLE, (us) 1 );
			}
			if ( szAktivSatz[ggdat.sonder_eti_krit][srv] == 'N' )
			{
				/* Deaktiviere */
    			p += formfu( p, GGW_DRUCKINTERN_ENABLE, (us) 0 );
			}
			break;

		case 5: /* Summendrucker */
		case 3: /* slave1 */
		case 4: /* slave2 */
		case 1: /* slave3 */
			if ( szAktivSatz[ggdat.sonder_eti_krit][srv] == 'J' )
			{
				/* Aktiviere, Druckausloesung = 5 = Remote Automat */
			    p += formfu( p, GGW_ETIKETDRUCK_START, (us) 5 );
			}
			if ( szAktivSatz[ggdat.sonder_eti_krit][srv] == 'N' )
			{
				/* Deaktiviere, Druckausloesung = 6 = Remote nicht aktiv */
			    p += formfu( p, GGW_ETIKETDRUCK_START, (us) 6 );
			}
			break;
		}
		if (SendPREIS[srv] == 'J')
		{
			p += formfu( p, GGD_PRS_GPREIS, dim_prs, ggdat.gpreis );
			p += formfu( p, GGL_ANUM1, ggdat.gpreis );
		    p += formfu( p, GGD_PRS_SPREIS, dim_prs, ggdat.spreis );
		}
		if (SendETIKETT[srv] == 'J')
		{
			if ( strcmp( anwender, "Boesinger" ) == 0 )
			{
				// siehe sendDATASET
			}
			else
			{
			    p += formfu( p, GGW_ETIKET_ART, (us)(ggdat.etin + 8191) );
			}
		}
		if (SendETIKETT_S1[srv] == 'J')
		{
			if ( srv == 3 && strcmp( anwender, "Boesinger" ) == 0 )
			{
			    p += formfu( p, GGL_NR_KDETIK_SUM1, ggdat.eti_nve1 );
			}
			else
			{
			    p += formfu( p, GGL_NR_KDETIK_SUM1, ggdat.ets1 );
			}
		}
		if (SendETIKETT_S2[srv] == 'J')
		{
			if ( srv == 3 && strcmp( anwender, "Boesinger" ) == 0 )
			{
			    p += formfu( p, GGL_NR_KDETIK_SUM2, ggdat.eti_nve2 );
			}
			else
			{
			    p += formfu( p, GGL_NR_KDETIK_SUM2, ggdat.ets2 );
			}
		}
		if (SendDATUM1[srv] == 'J')
		{
			p += formfu( p, GGL_DATUM1, ggdat.datum1 );
		}
		if (SendTAGE1[srv] == 'J')
		{
			p += formfu( p, GGL_HALTBAR1, ggdat.haltbar1 );
		}
		if (SendDATUM2[srv] == 'J')
		{
			p += formfu( p, GGL_DATUM2, ggdat.datum2 );
		}
		if (SendUHR12[srv] == 'J')
		{
			time( &t );
         	strftime( szHHMM, 5, "%H%M", localtime(&t) );
			lHHMM = (ul) atol( szHHMM );
			p += formfu( p, GGL_UHR1, lHHMM );
			p += formfu( p, GGL_UHR2, lHHMM );
		}
		if (SendCODE1[srv] == 'J')
		{
			if ( strcmp( anwender, "Boesinger" ) != 0 )
			{
				if (strcmp( anwender, "KUEBLER" ) == 0 && ggcab1.cabnr == 0)
				{
					ggcab1.cabnr = 1; /* like bws_defa codestruk 1 */
				}
			    formCAB( &ggcab1 );
			    p += formfu( p, GGT_CAB1, ggcab1.cablen, ggcab1.cab ) ;
			}
			p += formfu( p, GGT_CTS1, ggdat.cts1len, ggdat.cts1 ) ;
		}
		if (SendCODE2[srv] == 'J')
		{
			if ( strcmp( anwender, "Boesinger" ) != 0 )
			{
			    formCAB( &ggcab2 );
			    p += formfu( p, GGT_CAB2, ggcab2.cablen, ggcab2.cab ) ;
			}
			p += formfu( p, GGT_CTS2, ggdat.cts2len, ggdat.cts2 ) ;
		}
		/* Erweiterung 20041012 */
		if (SendCODE3[srv] == 'J')
		{
			// formCAB( &ggcab3 );
			p += formfu( p, GGT_CAB3, ggcab3.cablen, ggcab3.cab ) ;
		}
		if (SendCODE4[srv] == 'J')
		{
			// formCAB( &ggcab4 );
			p += formfu( p, GGT_CAB4, ggcab4.cablen, ggcab4.cab ) ;
		}
		if (SendCODE5[srv] == 'J')
		{
			// formCAB( &ggcab5 );
			p += formfu( p, GGT_CAB5, ggcab5.cablen, ggcab5.cab ) ;
		}
		len = (us)(p - tbuf);
		if (len < 1)
			return;
		len = formfu( tbuf, LGV_SEQUENZ, len, tbuf );
		toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void sendDATASET( int srv )
{
    us len;
    char *p;
	int w;
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
		switch (srv)
		{
		case 2:
			if ( Automatenpar[0] == 'J' && ggdat.ampar != 0  )
				p += formfu( p, GGL_AM_PARNR, ggdat.ampar );
			if ( Gewichtsklassen[0] == 'J' )
				p += formfu( p, GGL_MINMAXPARNR, ggdat.gwpar );
			if ( strcmp( anwender, "Boesinger" ) == 0 )
			{
				p += formfu( p, GGL_ETIKETPARNR, ggdat.etin );
				p += formfu( p, GGL_DTXNR2, ggdat.dtxnr2 );
				p += formfu( p, GGL_CABNR1, ggdat.cabnr1 );
				p += formfu( p, GGL_CABNR2, ggdat.cabnr2 );
			}
			break;
		case 3:
			if ( strcmp( anwender, "Boesinger" ) == 0 )
			{
				// p += formfu( p, GGL_ETIKETPARNR, ggdat.etin );
				// p += formfu( p, GGL_DTXNR2, ggdat.dtxnr2 );
				p += formfu( p, GGL_CABNR1, ggdat.cabnr3 );
				p += formfu( p, GGL_CABNR2, ggdat.cabnr4 );
			}
			break;
		}
		len = (us)(p - tbuf);
		if (len < 1)
			return;
		len = formfu( tbuf, XCV_DBTAB_DATASET, len, tbuf );
		len = toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void loadplu( int srv )
{
    us len;
    char *p;
	int w;
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
		switch (srv)
		{
		case 2:
			p += formfu( p, GGL_PLUNR, ggdat.plunr );
			p += formfu( p, GGL_KDNR, ggdat.kdnr );
			break;
		}
		len = (us)(p - tbuf);
		if (len < 1)
			return;
		len = formfu( tbuf, XCV_DBTAB_DATASET, len, tbuf );
		len = toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void getchargennr( void )
{
	us len;
	char *p;
	int w;
	int srv;
	srv = 2;
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
		p += formfu( p, GGL_CHARGENNR, 0 );
		len = (us)(p - tbuf);
		len = toGX( tbuf, srv, GXRD, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
	fprintf( fperr, " Chargenr: %ld\n", ggdat.chargennr );
}

void setchargennr( void )
{
	us len;
	char *p;
	int w;
	int srv;
	srv = 2;
	fprintf( fperr, "Chargenr: %ld\n", ggdat.chargennr );
	// if (ggdat.chargennr < 1) return;
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
		p += formfu( p, GGL_CHARGENNR, ggdat.chargennr );
		len = (us)(p - tbuf);
		len = toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void setS1Vorwahl( void )
{
	us len;
	char *p;
	int w;
	int srv;
	srv = 2;
	fprintf( fperr, "S1 Vorwahl: %ld\n", ggdat.sum1vw );
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
			    //p += formfu( p, GGW_SUM1VORWAHL_ART, ggdat.sum1vwa );
		p += formfu( p, GGL_STCK_SUM1_VORWAHL, ggdat.sum1vw );
		len = (us)(p - tbuf);
		len = toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}


void sendLabelData( void )
{
    long chargennr1; // 14.02.2013 gk
    fprintf( fperr, "GGW_ETIKET_ART - 8191 = %d, GGW_ETIKET_ART = %d = 0x%04X\n",
	    ggdat.etin, ggdat.etin+8191, ggdat.etin+8191 );
    if ( strcmp( anwender, "OKLE" ) == 0
		 ||
		 strcmp( anwender, "LOHMANN" ) == 0
	)
	{
	    sum123clear();
		loadplu(2);
		
        if ( strcmp( anwender, "LOHMANN" ) == 0
		)
		{
            return;
		}

		chargennr1 = ggdat.chargennr;
		setchargennr();
		setS1Vorwahl();
		/***
		getchargennr(); // zur Kontrolle
		if ( ggdat.chargennr != chargennr1 )
		{
			TimeMsgLine( "Chargenr: Falsch %ld Richtig: %ld", chargennr1, ggdat.chargennr );
			ggdat.chargennr = chargennr1;
		    setchargennr();
		}
		***/
		return;
	}

	/* Systembus 2 = Oberetikettierer 1 (Master) */
	sum123clear();
	sendAllGGATX(2);
	sendDATASET(2);
    sendGGDAT(2);
	// disconnectserver(2);

	/* Systembus 3 = Oberetikettierer 2 */
	sendAllGGATX(3);
	sendDATASET(3);
	sendGGDAT(3);
	// disconnectserver(3);

	/* Systembus 4 = Unteretikettierer */
	sendAllGGATX(4);
	sendGGDAT(4);
	// disconnectserver(4);

	/* 3. Slave */
	sendAllGGATX(1);
	sendGGDAT(1);
	// disconnectserver(4);

	/* Systembus 10 = Summendrucker */
	sendAllGGATX(5);
	sendGGDAT(5);
	// disconnectserver(5);

}

void sendDBGGATX( int srv, GGATX_t * ggatx )
{
	us len;
	char *p;
	int w;
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
		p += formfu( p, DBW_TAB, GGATX );
		p += formfu( p, DBW_ATTRDESCRIP, 1 );
		p += formfu( p, GGL_ATXNR, ggatx->atxnr );
		p += formfu( p, GGT_ATX, ggatx->atxlen, ggatx->atx );
		len = (us)(p - tbuf);
		len = formfu( tbuf, DBV_DATA_VDBT, len, tbuf );
		toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void sendDBGGDAT( int srv, GGDAT_t * ggdat )
{
	us len;
	char *p;
	int w;
    fprintf( fperr, "GGL_ETIKETPARNR = %d\n", ggdat->etin );
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
		p = tbuf;
		dim_prs = ggdat->devise;
		p += formfu( p, DBW_TAB, GGDAT );
		p += formfu( p, DBW_ATTRDESCRIP, 1 );
		p += formfu( p, GGL_PLUNR, ggdat->plunr );
		p += formfu( p, GGL_KDNR, ggdat->kdnr );
		p += formfu( p, GGL_ATXNR1, ggdat->atxnr1 );
		p += formfu( p, GGL_ATXNR2, ggdat->atxnr2 );
		p += formfu( p, GGL_ATXNR3, ggdat->atxnr3 );
		p += formfu( p, GGL_ATXNR4, ggdat->atxnr4 );
		p += formfu( p, GGL_ATXNR5, ggdat->atxnr5 );
		p += formfu( p, GGL_ATXNR6, ggdat->atxnr6 );
		p += formfu( p, GGL_ATXNR7, ggdat->atxnr7 );
		p += formfu( p, GGL_ATXNR8, ggdat->atxnr8 );
		p += formfu( p, GGL_ATXNR9, ggdat->atxnr9 );
		p += formfu( p, GGW_AUSZEICH_ART, ggdat->aart );
		p += formfu( p, GGW_PREIS_BERECHNUNG, ggdat->prbe );
		p += formfu( p, GGW_SUM1VORWAHL_ART, ggdat->sum1vwa );
		// p += formfu( p, GGW_SUM2VORWAHL_ART, ggdat->sum2vwa );
		// p += formfu( p, GGW_SUM3VORWAHL_ART, ggdat->sum3vwa );
		// p += formfu( p, GGW_SONDETIKET_KRITERIUM, ggdat->sonder_eti_krit );
		// p += formfu( p, GGW_SONDETIKET_WDKANAL, ggdat->sonder_eti_kanal );
		p += formfu( p, GGD_GEW_TARA_EINZEL, DIM_GEW, ggdat->tara );
		p += formfu( p ,GGD_GEW_NETTO_EINZEL, DIM_GEW , ggdat->fgew ) ;
		p += formfu( p, GGD_PRS_GPREIS, dim_prs, ggdat->gpreis );
		p += formfu( p, GGL_ETIKETPARNR, ggdat->etin );
		p += formfu( p, GGL_HALTBAR1, ggdat->haltbar1 );
		p += formfu( p, GGL_STCK_SUM1_VORWAHL, ggdat->sum1vw );
		p += formfu( p, GGL_STCK_SUM2_VORWAHL, ggdat->sum2vw );
		p += formfu( p, GGL_STCK_SUM3_VORWAHL, ggdat->sum3vw );
		p += formfu( p, GGL_AM_PARNR, ggdat->ampar );
		p += formfu( p, GGL_CABNR1, ggdat->cabnr1 );
		p += formfu( p, GGL_CABNR2, ggdat->cabnr2 );
		p += formfu( p, GGT_CTS1, ggdat->cts1len, ggdat->cts1 ) ;
		p += formfu( p, GGT_CTS2, ggdat->cts2len, ggdat->cts2 ) ;
		len = (us)(p - tbuf);
		len = formfu( tbuf, DBV_DATA_VDBT, len, tbuf );
		toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void sendLabelDataToGXDB( void )
{
	int srv;
	srv = 2;
	sendDBGGATX( srv, &ggatx1 );
	sendDBGGATX( srv, &ggatx2 );
	sendDBGGATX( srv, &ggatx3 );
	sendDBGGATX( srv, &ggatx4 );
	sendDBGGATX( srv, &ggatx5 );
	sendDBGGATX( srv, &ggatx6 );
	sendDBGGATX( srv, &ggatx7 );
	sendDBGGATX( srv, &ggatx8 );
	sendDBGGATX( srv, &ggatx9 );
	sendDBGGDAT( srv, &ggdat );
	memmove( ggatx2.atx + 13, ggatx2.atx, ggatx2.atxlen );
    sprintf( ggatx2.atx, "^Ac;^8;%5ld", ggdat.plunr );
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
	ggatx2.atx[ggatx2.atxlen] = '\n';
	sendGXMSG( srv, ggatx2.atx );
}

void receiveSumme( us psw_etikettyp )
{
	char *p;
	int len;
	int w;
	int srv;
	srv = 2;
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
        p = tbuf ;
        p += formfu( p, PSW_GETPCK, psw_etikettyp );
        len = (us)(p - tbuf);
        toGX( tbuf, srv, GXRD, len );
		returncode = 0;
        fromGX( tbuf, srv );
	    if ( returncode != 3 && returncode != 9999 )
		    break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void sendkanaldisable( void )
{
	us len;
	char *p;
	int w;
	int srv;
	srv = 2;
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
    	p = tbuf;
		switch( gxsendekanal[0] )
		{
		case 'A': p += formfu( p, GGW_SENDKANAL_A_ENABLE, (us) 0 ); break;
		case 'B': p += formfu( p, GGW_SENDKANAL_B_ENABLE, (us) 0 ); break;
		case 'C': p += formfu( p, GGW_SENDKANAL_C_ENABLE, (us) 0 ); break;
		case 'D': p += formfu( p, GGW_SENDKANAL_D_ENABLE, (us) 0 ); break;
		case 'E': p += formfu( p, GGW_SENDKANAL_E_ENABLE, (us) 0 ); break;
		}
		len = (us)(p - tbuf);
		if (len < 1)
			return;
		len = formfu( tbuf, LGV_SEQUENZ, len, tbuf );
		toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void lockpaz( void )
{
	us len;
	char *p;
	int w;
	int srv;
	srv = 2;
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
    	p = tbuf;
		switch( gxsendekanal[0] )
		{
		case 'A': p += formfu( p, GGW_SENDKANAL_A_ENABLE, (us) 0 ); break;
		case 'B': p += formfu( p, GGW_SENDKANAL_B_ENABLE, (us) 0 ); break;
		case 'C': p += formfu( p, GGW_SENDKANAL_C_ENABLE, (us) 0 ); break;
		case 'D': p += formfu( p, GGW_SENDKANAL_D_ENABLE, (us) 0 ); break;
		case 'E': p += formfu( p, GGW_SENDKANAL_E_ENABLE, (us) 0 ); break;
		}
		if ( strcmp( anwender, "Dietz" ) != 0
		     &&
			 strcmp( anwender, "OKLE" ) != 0
		     &&
			 strcmp( anwender, "LOHMANN" ) != 0
		     &&
			 strcmp( anwender, "KUEBLER" ) != 0
		)
		{
		    p += formfu( p, XCW_LOCK, 1 );
		}
		p += formfu( p, GGL_STCK_SUM1_VORWAHL, 0 );
		len = (us)(p - tbuf);
		if (len < 1)
			return;
		len = formfu( tbuf, LGV_SEQUENZ, len, tbuf );
		toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void unlockpaz( void )
{
    us len;
    char *p;
	int w;
	int srv;
	srv = 2;
	/* 
	if ( fehlerfall )
		return;
	*/
	if ( wartezeit > 0 )
	    Sleep( wartezeit );
	for ( w=0; w<WMAX; w++ )
	{
		hptr = header;
        p = tbuf;
		switch( gxsendekanal[0] )
		{
		case 'A': p += formfu( p, GGW_SENDKANAL_A_ENABLE, (us) 1 ); break;
		case 'B': p += formfu( p, GGW_SENDKANAL_B_ENABLE, (us) 1 ); break;
		case 'C': p += formfu( p, GGW_SENDKANAL_C_ENABLE, (us) 1 ); break;
		case 'D': p += formfu( p, GGW_SENDKANAL_D_ENABLE, (us) 1 ); break;
		case 'E': p += formfu( p, GGW_SENDKANAL_E_ENABLE, (us) 1 ); break;
		}
		p += formfu( p, XCW_UNLOCK, (us) 0x80 );
		if ( Transport[srv] == 'J' )
		{
			p += formfu( p, AMW_START_STOP, 1 ); 
		}
		len = (us)(p - tbuf);
		if (len < 1)
			return;
		len = formfu( tbuf, LGV_SEQUENZ, len, tbuf );
		toGX( tbuf, srv, GXWR, len );
		returncode = 0;
		fromGX( tbuf, srv );
		if ( returncode != 3 && returncode != 9999 )
			break;
		disconnectserver( srv );
		TimeMsgLine( "Wiederholung gescheitert" );
        Sleep(100);
	}
	if (w == WMAX) FatalError( "Kommunikationsfehler - ABBRUCH\n" );
}

void formErgebnis( int wrstat )
{  
    char mhd[20];
    long mhdtt, mhdmm, mhdjj;

	switch ( psw_etikettyp )
	{
	case ETI_INIT:

		sprintf( Ergebnis, " %c 1 0 %4lu %8lu %4lu %8lu %4lu %8lu",
			wrstat, 
			ggdat.stksum,  ggdat.netsum,
			ggdat.stksum2x, ggdat.netsum2x,
			ggdat.stksum3x, ggdat.netsum3x );

		// TimeMsgLine( Ergebnis );
		// SendMessage( hDlgMsg, WM_USER_DDE_PACKUNG, 0, 0l );
		// Hier nur fuer Testzwecke
		PostMessage( hDlgMsg, WM_USER_DDE_PACKUNG, 0, 0l );
		break;

	case ETI_EINZEL:

        tmpnetsum += ggdat.netto;
		if ( strcmp( anwender, "Boesinger" ) == 0
			 ||
			 strcmp( anwender, "DFW" ) == 0
			 ||
			 strcmp( anwender, "OKLE" ) == 0
			 ||
			 strcmp( anwender, "LOHMANN" ) == 0
			 ||
			 strcmp( anwender, "KUEBLER" ) == 0
	    )
		{
		    tmpstksum++;
		}
		/* else: Bauerngut, Lieferstueckzahl = Kistenzahl, also hier Stueckzahl nicht zaehlen */

        mhdtt = ggdat.datum2 / 10000;
        mhdmm = (ggdat.datum2 - mhdtt * 10000) / 100;
        mhdjj = ggdat.datum2 - mhdmm * 100 - mhdtt * 10000;
        sprintf( mhd, "%02ld.%02ld.20%02ld", mhdtt, mhdmm, mhdjj );

		sprintf( Ergebnis, " %c 1 0 %4lu %8lu %4lu %8lu %4lu %8lu %9lu %9lu %s",
			wrstat, 
			ggdat.stksum + tmpstksum,  ggdat.netsum + tmpnetsum,
			ggdat.stksum2x + tmpstksum, ggdat.netsum2x + tmpnetsum,
			ggdat.stksum3x + tmpstksum, ggdat.netsum3x + tmpnetsum,
			ggdat.s1numerator, ggdat.s2numerator, mhd );

		TimeMsgLine( "tmpstksum: %ld ggdat.stksum%ld", tmpstksum, ggdat.stksum );
		// SendMessage( hDlgMsg, WM_USER_DDE_PACKUNG, 0, 0l );

		PostMessage( hDlgMsg, WM_USER_DDE_PACKUNG, 0, 0l );
		break;


	case ETI_SUMME_1:

		/* Temporaere Summen werden ersetzt durch die aktuellen PAZ Summen */

		tmpstksum = 0;
		tmpnetsum = 0;		
      
		if ( strcmp( anwender, "Boesinger" ) == 0
			 ||
			 strcmp( anwender, "OKLE" ) == 0
			 ||
			 strcmp( anwender, "LOHMANN" ) == 0
			 ||
			 strcmp( anwender, "KUEBLER" ) == 0
	    )
		{
			ggdat.stksum2x += ggdat.stksum;
            ggdat.stksum3x += ggdat.stksum;
		}
		else /* Bauerngut, Lieferstueckzahl = Kistenzahl */
		{
		    ggdat.stksum2x++;
		    ggdat.stksum3x++;
		}
		ggdat.netsum2x += ggdat.netsum;
		ggdat.netsum3x += ggdat.netsum;

        mhdtt = ggdat.datum2 / 10000;
        mhdmm = (ggdat.datum2 - mhdtt * 10000) / 100;
        mhdjj = ggdat.datum2 - mhdmm * 100 - mhdtt * 10000;
        sprintf( mhd, "%02ld.%02ld.20%02ld", mhdtt, mhdmm, mhdjj );
		sprintf( Ergebnis, " S1 1 0 %4lu %8lu %4lu %8lu %4lu %8lu %9lu %9lu %s",
			ggdat.stksum,  ggdat.netsum,
			ggdat.stksum2x, ggdat.netsum2x,
			ggdat.stksum3x, ggdat.netsum3x,
			ggdat.s1numerator, ggdat.s2numerator, mhd );
		    TimeMsgLine( Ergebnis );
		if ( strcmp( anwender, "OKLE" ) != 0
			 &&
			 strcmp( anwender, "LOHMANN" ) != 0
			 &&
			 strcmp( anwender, "KUEBLER" ) != 0
		)
		{
		    // SendMessage( hDlgMsg, WM_USER_DDE_PACKUNG, 0, 0l );
		    PostMessage( hDlgMsg, WM_USER_DDE_PACKUNG, 0, 0l );
		}
		break;

	case ETI_SUMME_2:

		/* 20100616 GK */
		ggdat.stksum2x = ggdat.stksum2;
		ggdat.netsum2x = ggdat.netsum2;

		/* 20100510 GK */
		mhdtt = ggdat.datum2 / 10000;
        mhdmm = (ggdat.datum2 - mhdtt * 10000) / 100;
        mhdjj = ggdat.datum2 - mhdmm * 100 - mhdtt * 10000;
        sprintf( mhd, "%02ld.%02ld.20%02ld", mhdtt, mhdmm, mhdjj );
		sprintf( Ergebnis, " S2 1 0 %4lu %8lu %4lu %8lu %4lu %8lu %9lu %9lu %s",
			ggdat.stksum,  ggdat.netsum,
			ggdat.stksum2x, ggdat.netsum2x,
			ggdat.stksum3x, ggdat.netsum3x,
			ggdat.s1numerator, ggdat.s2numerator, mhd );

		if ( strcmp( anwender, "OKLE" ) != 0
			 &&
			 strcmp( anwender, "LOHMANN" ) != 0
			 &&
			 strcmp( anwender, "KUEBLER" ) != 0
	    )
		{
		    TimeMsgLine( Ergebnis );
		    // SendMessage( hDlgMsg, WM_USER_DDE_PACKUNG, 0, 0l );
		    PostMessage( hDlgMsg, WM_USER_DDE_PACKUNG, 0, 0l );
		}
		
		/* 20100616 GK */
		ggdat.stksum2x = 0;
		ggdat.netsum2x = 0;
		break;

	case ETI_SUMME_3:
 
		mhdtt = ggdat.datum2 / 10000;
        mhdmm = (ggdat.datum2 - mhdtt * 10000) / 100;
        mhdjj = ggdat.datum2 - mhdmm * 100 - mhdtt * 10000;
        sprintf( mhd, "%02ld.%02ld.20%02ld", mhdtt, mhdmm, mhdjj );

		if ( wrstat == 'R' )
		{
			sprintf( Ergebnis, " S3 1 0 %4lu %8lu %4lu %8lu %4lu %8lu %9lu %9lu %s",
				ggdat.stksum,  ggdat.netsum,
				ggdat.stksum2x, ggdat.netsum2x,
				ggdat.stksum3x, ggdat.netsum3x,
				ggdat.s1numerator, ggdat.s2numerator, mhd );

			if ( strcmp( anwender, "Boesinger" ) == 0 )
			{
				PostMessage( hDlgMsg, WM_USER_DDE_PACKUNG, 0, 0l );
			}
			else
			{
				PostMessage( hDlgMsg, WM_USER_DDE_PACKUNG, 1, 0l );
			}
		}
		else
		{
			sprintf( Ergebnis, " %c 1 0 %4lu %8lu %4lu %8lu %4lu %8lu %9lu %9lu %s",
				wrstat, 
				ggdat.stksum,  ggdat.netsum,
				ggdat.stksum2x, ggdat.netsum2x,
				ggdat.stksum3x, ggdat.netsum3x,
				ggdat.s1numerator, ggdat.s2numerator, mhd );

			PostMessage( hDlgMsg, WM_USER_DDE_PACKUNG, 0, 0l );
		}
		break;
	}
	PazStatistikInsert( psw_etikettyp );
}

