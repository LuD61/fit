#include "gxnetfun.h"
#include <stdio.h>

extern int fehlerfall;
extern int pazmdn;

extern char tbuf[];
extern char *pZutaten;

extern char SendTEXTE[];
extern char SendPREIS[];
extern char SendCODE1[];
extern char SendCODE2[];
extern char SendETIKETT[];
extern char SendDATUM1[];
extern char SendTAGE1[];
extern char SendDATUM2[];
//extern char KuBaTara[];
extern char anwender[];
extern char quitcheck[];
extern char pazstatistik[2];



void initSend( void );
void initCAB( void );
void initAktivSaetze( void );
void sendGXMSG( int, char * );
void sendLabelData( void );
void sendLabelDataToGXDB( void );
void sendkanaldisable( void );
void lockpaz( void );
void unlockpaz( void );
void formErgebnis( int );
