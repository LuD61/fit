#include "stdafx.h"
#include "global.h"
#include "msgline.h"
#include "ddeserver.h"
#include <stdio.h>

char szAppName[] = "gxserver";
char szTopic[32];

HSZ hszService, hszTopic, hszItem;
DWORD idInst;

char Ergebnis[512];
char Params[512];

int advstop;

void SaveCommand (HDDEDATA hData)
{
	DdeGetData (hData, (unsigned char*) Params, 512, 0);
}

HDDEDATA CALLBACK DdeCallback (UINT iType, UINT iFmt, HCONV hConv,
							   HSZ hsz1, HSZ hsz2, HDDEDATA hData,
							   DWORD dwData1, DWORD dwData2)
{
	char szBuffer[32];
	char szItem[32];

	switch (iType)
	{
	case XTYP_CONNECT :
		DdeQueryString (idInst, hsz2, szBuffer, sizeof (szBuffer), 0);
		if (strcmp (szBuffer, szAppName) != 0)
		{
			return (HDDEDATA) FALSE;
		}
		DdeQueryString (idInst, hsz1, szBuffer, sizeof (szBuffer), 0);
		strcpy( szTopic, szBuffer );
		if ( strcmp( szTopic, "auszeichnen" ) == 0)
		{
			return (HDDEDATA) TRUE;
		}
		if ( strcmp( szTopic, "laden" ) == 0)
		{
			return (HDDEDATA) TRUE;
		}
		return (HDDEDATA) FALSE;
	case XTYP_ADVSTART :
		if (iFmt != CF_TEXT)
		{
			return (HDDEDATA) NULL;
		}
		advstop = 0;
		PostMessage (hDlgGlobal, WM_USER_DDE_START, 0, 0l);
		fprintf(fperr,"advstop = %d\n", advstop);
		return (HDDEDATA) TRUE;
	case XTYP_REQUEST :
	case XTYP_ADVREQ :
		DdeQueryString (idInst, hsz2, szItem, sizeof (szItem), 0);
		strcat( Ergebnis, " DDE ..." );
		TimeMsgLine( Ergebnis );
		return DdeCreateDataHandle( idInst, (unsigned char*) Ergebnis,
			(int) strlen(Ergebnis) + 1,	0, hsz2, CF_TEXT, 0);
	case XTYP_POKE :
		DdeQueryString (idInst, hsz2, szItem, sizeof (szItem), 0);
		if (strcmp (szItem, "terminate") == 0)
		{
			PostMessage (hDlgGlobal, WM_USER_DDE_TERMINATE, 0, 0l);
			return (HDDEDATA) DDE_FACK;
		}
		if ( strcmp(szItem,"start") == 0  )
		{
			SaveCommand (hData);
			if ( advstop == 1 )
			{
				fprintf(fperr,"advstop = %d\n", advstop);
				PostMessage (hDlgGlobal, WM_USER_DDE_START, 0, 0l);
				advstop = 0;
			}
			return (HDDEDATA) 0;
		}
		if ( strcmp(szItem, "laden") == 0 )
		{
			SaveCommand (hData);
			return (HDDEDATA) 0;
		}
		return (HDDEDATA) DDE_FACK;
	case XTYP_ADVSTOP :
		if (iFmt != CF_TEXT)
		{
			return (HDDEDATA) NULL;
		}
		advstop = 1;
		return (HDDEDATA) TRUE;
	}
	return (HDDEDATA) NULL;
}

