!include <win32.mak>

cflags = $(cflags) -I $(INFORMIXDIR)\incl\esql -DWINDOWS -D_MBCS -GF -FD -EHsc -Gy -TP -MTd -wd4996


ifmxdiag.obj : ifmxdiag.ec
	esql.exe -dcmdl -e ifmxdiag.ec
	cl.exe $(cflags) ifmxdiag.c

gxsql.obj : gxsql.ec
	esql.exe -dcmdl -e gxsql.ec
	cl.exe $(cflags) gxsql.c
