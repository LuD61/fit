#ifndef GXSQLH
#define GXSQLH

#include "gxnetapp.h"
#include "gxnetfun.h"

#ifdef _cplusplus
extern "C" {
#endif

extern int patmdn;
extern char anwender[];
extern char command[];
void getfields( char * );
void getLabelData( void );
int getKunData( void );
int getBranData( void );
int getAufData( void );
void initcursors( void );
void CloseDatabase( void );
void Query_paz_cab( GGCAB_t * );
void PazStatistikInsert( int );

#ifdef _cplusplus
}
#endif

#endif
