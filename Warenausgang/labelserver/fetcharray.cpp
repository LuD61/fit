/********************************************************************************

void printsqlda( struct sqlda *sqldap )
{
	int ic, nc;
	nc = sqldap->sqld;
	fprintf(fperr,"sqlda:\n");
	fprintf(fperr,"sqld=%d\n",nc);
	fprintf(fperr,"sqlvar:\n");
	fprintf(fperr,"%4s %20s %10s %6s\n","    ","sqlname","sqltype","sqllen");
	for ( ic = 0; ic < nc; ic++ )
	{
		colp = sqldap->sqlvar + ic; 
		fprintf(fperr,"%4d %20s %10s %6d\n",ic,colp->sqlname,
			rtypname(colp->sqltype), colp->sqllen);
	}
}

void Initsqlda( struct sqlda *sqldap )
{
	int ic, nc;
	int pos, size;

	pos = 0;
	nc = sqldap->sqld;
	for ( ic = 0; ic < nc; ic++ )
	{
		colp = sqldap->sqlvar + ic;
		if ( colp->sqltype == SQLCHAR )
			colp->sqllen += 1;
		pos = rtypalign( pos, colp->sqltype );
		colp->sqldata = databuff + pos;
		size = rtypmsize( colp->sqltype, colp->sqllen );
		fprintf(fperr,"%5d %5d\n",pos,size);
		pos += size;
		if ( pos > DATASIZE )
		{
			fprintf(fperr,"databuff zu klein\n");
			exit(1);
		}
	}
}

void SaveData( struct sqlda *sqldap )
{
	char *fieldp;
	int ic, nc;
	int rc;
	int shortval;
	long longval;
	double doubleval;
	nc = sqldap->sqld;
	for ( ic = 0; ic < nc; ic++ )
	{
		colp = sqldap->sqlvar + ic;
		byfill(field,FIELDSIZE,0);
		fieldp = field;
		switch (colp->sqltype)
		{
		case SQLSMFLOAT:
			doubleval = (double) *((float *) (colp->sqldata));
			break;

		case SQLFLOAT:
			doubleval = *((double *) (colp->sqldata));
			break;

		case SQLDECIMAL:
			rc = dectodbl( (dec_t *) colp->sqldata, &doubleval );

			if ( rc != 0 )
				fprintf(fperr,"%s %s %d Konvertierungsfehler %d\n",
				colp->sqlname,rtypname(colp->sqltype),
				colp->sqllen, rc);

			if ( strcmp( colp->sqlname, "ean" ) == 0 ) {
				a_kun_gx.ean = doubleval;
			} else if ( strcmp( colp->sqlname, "ean1" ) == 0 ) {
				a_kun_gx.ean1 = doubleval;
			} else if ( strcmp( colp->sqlname, "a_tara" ) == 0 ) {
				data.a_tara = doubleval;
			} else if ( strcmp( colp->sqlname, "a_gew" ) == 0 ) {
				data.a_gew = doubleval;
            }
				break;

		case SQLCHAR:
			if ( strcmp( (char*) colp->sqlname, "a_bz3" ) == 0 ) {
				strcpy( a_kun_gx.a_bz3, colp->sqldata);
			} else if ( strcmp( colp->sqlname, "a_bz4" ) == 0 ) {
				strcpy( a_kun_gx.a_bz4, colp->sqldata);
			} else if ( strcmp( colp->sqlname, "pr_rech_kz" ) == 0 ) {
				strcpy( a_kun_gx.pr_rech_kz, colp->sqldata);
			}
			break;

		case SQLSMINT:
			if ( risnull( CSHORTTYPE, colp->sqldata ) )
			{
				shortval = 0;
			}
			else
			{
				shortval = *((short *)(colp->sqldata));
			}
			if ( strcmp( colp->sqlname, "hbk_ztr" ) == 0 ) {
				data.hbk_ztr = shortval;
			} else if ( strcmp( colp->sqlname, "sg1" ) == 0 ) {
				a_kun_gx.sg1 = shortval;
			} else if ( strcmp( colp->sqlname, "sg2" ) == 0 ) {
				a_kun_gx.sg2 = shortval;
			} else if ( strcmp( colp->sqlname, "ausz_art" ) == 0 ) {
				a_kun_gx.ausz_art = shortval;
			} else if ( strcmp( colp->sqlname, "cab" ) == 0 ) {
				a_kun_gx.cab = shortval;
			} else if ( strcmp( colp->sqlname, "eti_typ" ) == 0 ) {
				a_kun_gx.eti_typ = shortval;
			} else if ( strcmp( colp->sqlname, "eti_sum1" ) == 0 ) {
				a_kun_gx.eti_sum1 = shortval;
			} else if ( strcmp( colp->sqlname, "eti_sum2" ) == 0 ) {
				a_kun_gx.eti_sum2 = shortval;
			} else if ( strcmp( colp->sqlname, "eti_sum3" ) == 0 ) {
				a_kun_gx.eti_sum3 = shortval;
			} else if ( strcmp( colp->sqlname, "sonder_eti" ) == 0 ) {
				a_kun_gx.sonder_eti = shortval;
			} else if ( strcmp( colp->sqlname, "up_ka" ) == 0 ) {
				data.up_ka = shortval;
			} else if ( strcmp( colp->sqlname, "a_typ" ) == 0 ) {
				data.a_typ = shortval;
			}
			break;

		case SQLINT:
			if ( risnull( CLONGTYPE, colp->sqldata ) )
			{
				longval = 0l;
			}
			else
			{
				longval = *((long *)(colp->sqldata));
			}
			if ( strcmp( colp->sqlname, "kopf_text" ) == 0 ) {
				a_kun_gx.kopf_text = longval;
			} else if ( strcmp( colp->sqlname, "text_nr" ) == 0 ) {
				a_kun_gx.text_nr = longval;
			} else if ( strcmp( colp->sqlname, "text_nr2" ) == 0 ) {
				a_kun_gx.text_nr2 = longval;
			} else if ( strcmp( colp->sqlname, "mhd_text" ) == 0 ) {
				a_kun_gx.mhd_text = longval;
			} else if ( strcmp( colp->sqlname, "ampar" ) == 0 ) {
				a_kun_gx.ampar = longval;
			} else if ( strcmp( colp->sqlname, "gwpar" ) == 0 ) {
				a_kun_gx.gwpar = longval;
			} else if ( strcmp( colp->sqlname, "vppar" ) == 0 ) {
				a_kun_gx.vppar = longval;
			}
			break;

		case SQLDATE:
			rc = rdatestr( *((long *)(colp->sqldata)), field );
			if ( rc != 0 )
			{
				if ( risnull( CDATETYPE, colp->sqldata ) )
				{
					field[0] = '\0';
				}
				else
				{
					fprintf(fperr,"%s %s %d Konvertierungsfehler %d\n",
						colp->sqlname,rtypname(colp->sqltype),
						colp->sqllen, rc);
				}
			}
			break;

		default:
			fprintf(fperr,
				"SaveData: %s : Datentyp %s nicht unterstuetzt.\n",
				colp->sqlname,rtypname(colp->sqltype));
			break;
		}
	}
}

long LoopQuery( struct sqlda *sqldap )
{
	long n;   
	EXEC SQL open sel_curs;
	if ( SQLCODE != 0 ) 
	{
		fprintf(fperr,"open sel_curs: SQLCODE=%ld\n",SQLCODE);
		CloseDatabase();
		exit(1);
	}

	for ( n = 0 ; ; n++ )
	{
		EXEC SQL fetch sel_curs using descriptor sqldap;
		if ( SQLCODE == 100L )
		{
			break;
		}
		if ( SQLCODE < 0L )
		{
			fprintf(fperr,"Satz %ld SQLCODE=%ld\n",n+1,SQLCODE);
			fflush( fperr ); 
			EXEC SQL close sel_curs;
			CloseDatabase();
			exit(1);
		}
		SaveData( sqldap );
	}
	EXEC SQL close sel_curs;
	fflush(fperr);
	return n;
}    

*******************************************************************************/
