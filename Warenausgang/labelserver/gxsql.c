#include <sqlhdr.h>
#include <sqliapi.h>
static const char *_Cn35 = "pr_ausz_gx_curs";
static const char *_Cn34 = "p_pr_ausz_gx_curs";
static const char *_Cn33 = "adr_curs";
static const char *_Cn32 = "p_adr_curs";
static const char *_Cn31 = "kun_curs";
static const char *_Cn30 = "p_kun_curs";
static const char *_Cn29 = "kun_geb_curs";
static const char *_Cn28 = "p_kun_geb_curs";
static const char *_Cn27 = "paz_cab_curs";
static const char *_Cn26 = "p_paz_cab_curs";
static const char *_Cn25 = "paz_ipr_curs";
static const char *_Cn24 = "p_paz_ipr_curs";
static const char *_Cn23 = "a_bas_erw_curs";
static const char *_Cn22 = "p_a_bas_erw_curs";
static const char *_Cn21 = "a_lgr_curs";
static const char *_Cn20 = "p_a_lgr_curs";
static const char *_Cn19 = "a_bas_curs";
static const char *_Cn18 = "p_a_bas_curs";
static const char *_Cn17 = "a_hndw_curs";
static const char *_Cn16 = "p_a_hndw_curs";
static const char *_Cn15 = "a_eig_curs";
static const char *_Cn14 = "p_a_eig_curs";
static const char *_Cn13 = "a_kun_gx_kb_curs";
static const char *_Cn12 = "p_a_kun_gx_kb_curs";
static const char *_Cn11 = "pazlsp_curs";
static const char *_Cn10 = "p_pazlsp_curs";
static const char *_Cn9 = "a_kun_gx_ab_curs";
static const char *_Cn8 = "p_a_kun_gx_ab_curs";
static const char *_Cn7 = "a_kun_gx_ak_curs";
static const char *_Cn6 = "p_a_kun_gx_ak_curs";
static const char *_Cn5 = "a_kun_gx_akb_curs";
static const char *_Cn4 = "p_a_kun_gx_akb_curs";
static const char *_Cn3 = "a_kun_curs";
static const char *_Cn2 = "p_a_kun_curs";
static const char *_Cn1 = "bws";
#line 1 "gxsql.ec"
#include "gxsql.h"
#include "items.h"
#include "msgline.h"
#include "gxnetfun.h"
#include "gxnetapp.h"
#include "ifmxdiag.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "sqlhdr.h"

/* 
 * EXEC SQL include sqlproto.h;
 */
#line 13 "gxsql.ec"

#line 13 "gxsql.ec"
#line 1 "d:/informix/incl/esql/sqlproto.h"
/****************************************************************************
 *
 *                               IBM INC.
 *
 *                           PROPRIETARY DATA
 *
 * Licensed Material - Property Of IBM
 *
 * "Restricted Materails of IBM"
 *
 * IBM Informix Client SDK
 *
 * (c)  Copyright IBM Corporation 2002. All rights reserved.
 *
 *  Title:	sqlproto.h
 *  Sccsid:     @(#)sqlproto.h
 *  Description:
 *		Prototypes of user functions exported by ISQLIxxx.DLL
 *
 ***************************************************************************
 */

#ifndef _SQLPROTO
#define _SQLPROTO
#ifdef __cplusplus
extern "C" {
#endif

int __cdecl bycmpr(char *s1,char *s2,int cnt);
void __cdecl bycopy(char *s1,char *s2,int n);
void __cdecl byfill(char *s,int n,char c);
int __cdecl byleng(char *beg,int cnt);
int __cdecl decadd(struct decimal *n1,struct decimal *n2,struct decimal *n3);
int __cdecl deccmp(struct decimal *n1,struct decimal *n2);
void __cdecl deccopy(struct decimal *n1,struct decimal *n2);
int __cdecl deccvasc(char *cp,int len,struct decimal *np);
int __cdecl deccvdbl(double dbl,struct decimal *np);
int __cdecl deccvint(int in,struct decimal *np);
int __cdecl deccvlong(long lng,struct decimal *np);
int __cdecl decdiv(struct decimal *n1,struct decimal *n2,struct decimal *n3);
char * __cdecl dececvt(struct decimal *np,int ndigit,int *decpt,int *sign);
char * __cdecl decfcvt(struct decimal *np,int ndigit,int *decpt,int *sign);
int __cdecl decmul(struct decimal *n1,struct decimal *n2,struct decimal *n3);
void __cdecl decround(struct decimal *np,int dec_round);
int __cdecl decsub(struct decimal *n1,struct decimal *n2,struct decimal *n3);
int __cdecl dectoasc(struct decimal *np,char *cp,int len,int right);
int __cdecl dectodbl(struct decimal *np,double *dblp);
int __cdecl dectoint(struct decimal *np,int *ip);
int __cdecl dectolong(struct decimal *np,long *lngp);
void __cdecl dectrunc(struct decimal *np,int trunc);
void __cdecl dtcurrent(struct dtime *d);
int __cdecl dtcvasc(char *str,struct dtime *d);
int __cdecl ifx_dtcvasc(char *str,struct dtime *d, char db_century);
int __cdecl dtcvfmtasc(char *input,char *fmt,struct dtime *d);
int __cdecl ifx_dtcvfmtasc(char *input,char *fmt,struct dtime *d, char db_century);
int __cdecl dtextend(struct dtime *id,struct dtime *od);
int __cdecl dttoasc(struct dtime *d,char *str);
int __cdecl dttofmtasc(struct dtime *d,char *output,int str_len,char *fmtstr);
int __cdecl ifx_dttofmtasc(struct dtime *d,char *output,int str_len,char *fmtstr, char db_century);
int __cdecl incvasc(char *str,struct intrvl *i);
int __cdecl incvfmtasc(char *input,char *fmt,struct intrvl *intvl);
int __cdecl intoasc(struct intrvl *i,char *str);
int __cdecl intofmtasc(struct intrvl *i,char *output,int str_len,char *fmtstr);
void __cdecl ldchar(char *from,int count,char *to);
int __cdecl rdatestr(long jdate,char *str);
int __cdecl rdayofweek(long date);
int __cdecl rdefmtdate(long *pdate,char *fmtstring,char *input);
int __cdecl ifx_defmtdate(long *pdate,char *fmtstring,char *input, char db_century);
void __cdecl rdownshift(char *s);
int __cdecl rfmtdate(long date,char *fmtstring,char *result);
int __cdecl rfmtdec(struct decimal *dec,char *format,char *outbuf);
int __cdecl rfmtdouble(double dvalue,char *format,char *outbuf);
int __cdecl rfmtlong(long lvalue,char *format,char *outbuf);
int __cdecl rget_lmsg(long msgnum,char *s,int maxsize,int *msg_length);
int __cdecl rgetlmsg(long msgnum,char *s,int maxsize,int *msg_length);
int __cdecl rgetmsg(int msgnum,char *s,int maxsize);
int __cdecl risnull(int vtype,char *pcvar);
int __cdecl rjulmdy(long jdate,short *mdy);
int __cdecl rleapyear(int year);
int __cdecl rmdyjul(short *mdy,long *jdate);
int __cdecl rsetnull(int vtype,char *pcvar);
int __cdecl rstrdate(char *str,long *jdate);
int __cdecl ifx_strdate(char *str,long *jdate, char db_century);
void __cdecl rtoday(long *today);
int __cdecl rtypalign(int offset,int type);
int __cdecl rtypmsize(int type,int len);
char * __cdecl rtypname(int type);
int __cdecl rtypwidth(int type,int len);
void __cdecl rupshift(char *s);
int __cdecl sqlexit(void);
void __cdecl stcat(char *src,char *dst);
void __cdecl stchar(char *from,char *to,int count);
int __cdecl stcmpr(char *s1,char *s2);
void __cdecl stcopy(char *src,char *dst);
int __cdecl stleng(char *src);
int __cdecl rstod(char *str,double *val);
int __cdecl rstoi(char *s,int *val);
int __cdecl rstol(char *s,long *val);
int __cdecl sqgetdbs(int *ret_fcnt,char **fnames ,int fnsize,char *farea,int fasize);
int __cdecl sqlbreak(void);
int __cdecl sqlbreakcallback(long timeout,void (*callback)(int));
int __cdecl sqldetach(void);
int __cdecl sqldone(void);
int __cdecl sqlstart(void);
int __cdecl dtaddinv(struct dtime *d,struct intrvl *i,struct dtime *r);
int __cdecl dtsub(struct dtime *d1,struct dtime *d2,struct intrvl *i);
int __cdecl dtsubinv(struct dtime *d,struct intrvl *i,struct dtime *r);
int __cdecl invdivdbl(struct intrvl *iv,double dbl,struct intrvl *ov);
int __cdecl invdivinv(struct intrvl *i1,struct intrvl *i2,double *res);
int __cdecl invextend(struct intrvl *i,struct intrvl *o);
int __cdecl invmuldbl(struct intrvl *iv,double dbl,struct intrvl *ov);
int __cdecl ifx_to_gl_datetime(char *source_str, char *target_str, int maxlen);
void * __cdecl GetConnect ( void );
void * __cdecl SetConnect ( void *NewConInfo );
void * __cdecl ReleaseConnect ( void *NewConInfo );
int __cdecl deccvflt(double source, struct decimal *destination);
int __cdecl dectoflt(struct decimal *source, float *destination);
char * __cdecl ifx_getenv(char *env_name);
int __cdecl ifx_putenv(const char *env_name);
int __cdecl _infxcexp(int RtnNum, void * RtnAddr, int OptParm);
void __cdecl SqlFreeMem( void * MemAddr, int FreeType );
char * __cdecl ifx_getcur_conn_name(void);

int __cdecl ifx_int8cmp(struct ifx_int8 *op1, struct ifx_int8 *op2);
int __cdecl ifx_int8cvlong(long lng, struct ifx_int8 *int8p);
int __cdecl ifx_int8tolong(struct ifx_int8 *int8p, long *lngp);
int __cdecl ifx_int8cvint(int in, struct ifx_int8 *int8p);
int __cdecl ifx_int8toint(struct ifx_int8 *int8p, int *intp);
int __cdecl ifx_int8toint2(struct ifx_int8 *int8p,short *int2p);
int __cdecl ifx_int8toint4(struct ifx_int8 *int8p,long *int4p);
int __cdecl ifx_int8cvasc(char *cp, int len, struct ifx_int8 *int8p);
int __cdecl ifx_int8toasc(struct ifx_int8 *int8p, char *cp, int len);
int __cdecl ifx_int8cvdec(struct decimal *decp, struct ifx_int8 *int8p);
int __cdecl ifx_int8todec(struct ifx_int8 *int8p, struct decimal *decp);
int __cdecl ifx_int8cvdbl(double dbl, struct ifx_int8 *int8p);
int __cdecl ifx_int8todbl(struct ifx_int8 *int8p, double *dblp);
int __cdecl ifx_int8cvflt(double flt, struct ifx_int8 *int8p);
int __cdecl ifx_int8toflt(struct ifx_int8 *int8p, float *fltp);
int __cdecl deccvint8(struct ifx_int8 *int8p, struct decimal *decp);
int __cdecl ifx_int8sub(struct ifx_int8 *int8op1, struct ifx_int8 *int8op2, struct ifx_int8 *int8result);
int __cdecl ifx_int8add(struct ifx_int8 *int8op1, struct ifx_int8 *int8op2, struct ifx_int8 *int8result);
int __cdecl ifx_int8mul(struct ifx_int8 *int8op1, struct ifx_int8 *int8op2, struct ifx_int8 *int8result);
int __cdecl ifx_int8div(struct ifx_int8 *int8op1, struct ifx_int8 *int8op2, struct ifx_int8 *int8result);
void __cdecl ifx_int8copy(struct ifx_int8 *sint8p, struct ifx_int8 *dint8p);
void __cdecl ifx_getserial8(struct ifx_int8 *int8p);

int __cdecl ifx_cl_dealloc(struct _ifx_collection_struct **collection);
long __cdecl ifx_get_row_xid(struct _ifx_collection_struct *collp, int *colnum);
char * __cdecl ifx_get_row_extended_name(struct _ifx_collection_struct *collp);
char * __cdecl ifx_get_msg_param(void);

int __cdecl ifx_var_flag(void **variable, short alloc_flag);
int __cdecl ifx_var_alloc(void **variable, long size);
int __cdecl ifx_var_dealloc(void **variable);
int __cdecl ifx_var_setdata(void **variable, char *data, long size);
int __cdecl ifx_var_isnull(void **variable);
int __cdecl ifx_var_setnull(void **variable, int flag);
int __cdecl ifx_var_setlen(void **variable, long size);
void * __cdecl ifx_var_getdata(void **variable);
int __cdecl ifx_var_init(void **variable);
int __cdecl ifx_var_getlen(void **variable);
int __cdecl ifx_lvar_alloc(int alloc);

/*
 * Set of ifx_lo_* accessors for opaque data types:
 *	- ifx_lo_create_spec_t
 *	- ifx_lo_stat_t
 *
 * See also: locator.h for description of possible values for arguments
 */

/* smartblob create spec SET accessors: */
int __cdecl ifx_lo_specset_flags(ifx_lo_create_spec_t *cspec, int flags);
int __cdecl ifx_lo_specset_def_open_flags(ifx_lo_create_spec_t *cspec, int def_open_flags);
int __cdecl ifx_lo_specset_estbytes(ifx_lo_create_spec_t *cspec, ifx_int8_t *size);
int __cdecl ifx_lo_specset_maxbytes(ifx_lo_create_spec_t *cspec, ifx_int8_t *size);
int __cdecl ifx_lo_specset_extsz(ifx_lo_create_spec_t *cspec, int n);
int __cdecl ifx_lo_specset_sbspace(ifx_lo_create_spec_t *cspec, const char *str);

/* smartblob create spec GET accessors: */
int __cdecl ifx_lo_specget_flags(ifx_lo_create_spec_t *cspec);
int __cdecl ifx_lo_specget_def_open_flags(ifx_lo_create_spec_t *cspec);
int __cdecl ifx_lo_specget_estbytes(ifx_lo_create_spec_t *cspec, ifx_int8_t *size);
int __cdecl ifx_lo_specget_maxbytes(ifx_lo_create_spec_t *cspec, ifx_int8_t *size);
int __cdecl ifx_lo_specget_extsz(ifx_lo_create_spec_t *cspec);
int __cdecl ifx_lo_specget_sbspace(ifx_lo_create_spec_t *cspec, char *str, int len);

/* smartblob stat GET accessors: */
int __cdecl ifx_lo_stat_size(ifx_lo_stat_t *lostat, ifx_int8_t *size);
int __cdecl ifx_lo_stat_uid(ifx_lo_stat_t *lostat);
int __cdecl ifx_lo_stat_atime(ifx_lo_stat_t *lostat);
int __cdecl ifx_lo_stat_mtime_sec(ifx_lo_stat_t *lostat);
int __cdecl ifx_lo_stat_mtime_usec(ifx_lo_stat_t *lostat);
int __cdecl ifx_lo_stat_ctime(ifx_lo_stat_t *lostat);
int __cdecl ifx_lo_stat_refcnt(ifx_lo_stat_t *lostat);
ifx_lo_create_spec_t * __cdecl ifx_lo_stat_cspec(ifx_lo_stat_t *lostat);

/* smartblob spec and stat destructors: */
int __cdecl ifx_lo_spec_free(ifx_lo_create_spec_t *cspec);
int __cdecl ifx_lo_stat_free(ifx_lo_stat_t *lostat);
/*
 * Set of ifx_lo_ functions for support of Large Objects
 */
int __cdecl ifx_lo_col_info(char *column_name,ifx_lo_create_spec_t *create_spec);
int __cdecl ifx_lo_def_create_spec(ifx_lo_create_spec_t **cspec);
int __cdecl ifx_lo_create(ifx_lo_create_spec_t *create_spec, int flags, ifx_lo_t *loptr, int *error);
int __cdecl ifx_lo_open(ifx_lo_t *loptr, int flags, int *error);
int __cdecl ifx_lo_close(int lofd);
int __cdecl ifx_lo_seek(int lofd, ifx_int8_t *off, int whence, ifx_int8_t *seek_pos);
int __cdecl ifx_lo_tell(int lofd, ifx_int8_t *seek_pos);
int __cdecl ifx_lo_truncate(int lofd, ifx_int8_t *off);
int __cdecl ifx_lo_filename(ifx_lo_t *loptr, char *fname, char *result, int result_buffer_nbytes);
int __cdecl ifx_lo_alter(ifx_lo_t *loptr, ifx_lo_create_spec_t *create_spec);
int __cdecl ifx_lo_stat(int lofd, ifx_lo_stat_t **lostat);
int __cdecl ifx_lo_read(int lofd, char *buf, int nbytes, int *error);
int __cdecl ifx_lo_readwithseek(int lofd, char *buf, int nbytes, ifx_int8_t *off, int whence, int *error);
int __cdecl ifx_lo_write(int lofd, char *buf, int nbytes, int *error);
int __cdecl ifx_lo_writewithseek(int lofd, char *buf, int nbytes, ifx_int8_t *off, int whence, int *error);
int __cdecl ifx_lo_copy_to_lo(int lofd, char *fname, int flags);
int __cdecl ifx_lo_copy_to_file(ifx_lo_t *loptr, char *fname, int flags, char *result);
int __cdecl ifx_lo_release(ifx_lo_t *loptr);
int __cdecl ifx_lo_to_buffer(struct ifx_lo_ts *lo_handle,int size_limit,char **buffer ,int *error);
int __cdecl ifx_lo_from_buffer(struct ifx_lo_ts *lo_handle,int size,char *buffer,int *error);
int __cdecl ifx_lo_lock(int lofd, ifx_int8_t *off, int whence, ifx_int8_t *range, int lockmode);
int __cdecl ifx_lo_unlock(int lofd, ifx_int8_t *off, int whence, ifx_int8_t *range);

int __cdecl ifx_xactevent(void *);
int __cdecl ifx_getserowner(char *serowner);

void *__cdecl ifx_alloc_conn_user(char *username,char *passwd);
void __cdecl ifx_free_conn_user(struct ifx_connect_struct **conn);
void *__cdecl ifx_alloc_conn_cred(void *cred);
void __cdecl ifx_free_conn_cred(struct ifx_connect_struct **conn);
int __cdecl ifx_isius(void);
int __cdecl ifx_cl_card(struct _ifx_collection_struct *colt, int *isnull);
char * __cdecl	sqli_server_version(void);
int    __cdecl	ldcollection(char *cp, ifx_collection_t *collp, mint readsize,
								mint longidver);
void   __cdecl	stcollection(ifx_collection_t *collp, char *start, mint longidver);

#ifdef __cplusplus
}
#endif

#endif /* !_SQLPROTO */
#line 245 "d:/informix/incl/esql/sqlproto.h"
/* 
 * EXEC SQL include sqlca.h;
 */
#line 14 "gxsql.ec"

#line 14 "gxsql.ec"
#line 1 "d:/informix/incl/esql/sqlca.h"
/****************************************************************************
 *
 *                               IBM INC.
 *
 *                           PROPRIETARY DATA
 *
 * Licensed Material - Property Of IBM
 *
 * "Restricted Materails of IBM"
 *
 * IBM Informix Client SDK
 *
 * (c)  Copyright IBM Corporation 2002. All rights reserved.
 *
 *  Title:	sqlca.h
 *  Sccsid:	@(#)sqlca.h	9.4	1/18/93  11:09:48
 *  Description:
 *		SQL Control Area
 *
 ***************************************************************************
 */

#ifndef SQLCA_INCL
#define SQLCA_INCL

#ifdef __BORLANDC__
#pragma option -a8
#else
#pragma pack (8)
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include "ifxtypes.h"

typedef struct sqlca_s
{
    int4 sqlcode;
    char sqlerrm[72]; /* error message parameters */
    char sqlerrp[8];
    int4 sqlerrd[6];
		    /* 0 - estimated number of rows returned */
		    /* 1 - serial value after insert or  ISAM error code */
		    /* 2 - number of rows processed */
		    /* 3 - estimated cost */
		    /* 4 - offset of the error into the SQL statement */
		    /* 5 - rowid after insert  */
#ifdef _FGL_
    char sqlawarn[8];
#else
    struct sqlcaw_s
	{
	char sqlwarn0; /* = W if any of sqlwarn[1-7] = W */
	char sqlwarn1; /* = W if any truncation occurred or
				database has transactions or
			        no privileges revoked */
	char sqlwarn2; /* = W if a null value returned or
				ANSI database */
	char sqlwarn3; /* = W if no. in select list != no. in into list or
				turbo backend or no privileges granted */
	char sqlwarn4; /* = W if no where clause on prepared update, delete or
				incompatible float format */
	char sqlwarn5; /* = W if non-ANSI statement */
	char sqlwarn6; /* = W if server is in data replication secondary mode */
	char sqlwarn7; /* = W if database locale is different from proc_locale
			*/
	} sqlwarn;
#endif
    } ifx_sqlca_t;

#define SQLNOTFOUND 100

#ifdef __cplusplus
}
#endif

#ifdef __BORLANDC__
#pragma option -a-
#else
#pragma pack ()
#endif

#endif /* SQLCA_INCL */
#line 85 "d:/informix/incl/esql/sqlca.h"
/* 
 * EXEC SQL include sqlda.h;
 */
#line 15 "gxsql.ec"

#line 15 "gxsql.ec"
#line 1 "d:/informix/incl/esql/sqlda.h"
/****************************************************************************
 *
 *                               IBM INC.
 *
 *                           PROPRIETARY DATA
 *
 * Licensed Material - Property Of IBM
 *
 * "Restricted Materails of IBM"
 *
 * IBM Informix Client SDK
 *
 * (c)  Copyright IBM Corporation 2002. All rights reserved.
 *
 *  Title:	sqlda.h
 *  Sccsid:	@(#)sqlda.h	9.2	1/12/92  12:10:21
 *  Description:
 *		SQL Data Description Area
 *
 ***************************************************************************
 */


#ifndef _SQLDA
#define _SQLDA

#ifdef __BORLANDC__
#pragma option -a8
#else
#pragma pack (8)
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include "ifxtypes.h"

typedef struct sqlvar_struct
    {
    int2 sqltype;		/* variable type		*/
    int4 sqllen;		/* length in bytes		*/
    char *sqldata;		/* pointer to data		*/
    int2 *sqlind;		/* pointer to indicator	*/
    char  *sqlname;		/* variable name		*/
    char  *sqlformat;	/* reserved for future use 	*/
    int2 sqlitype;		/* ind variable type		*/
    int2 sqlilen;		/* ind length in bytes		*/
    char *sqlidata;		/* ind data pointer		*/
    int4  sqlxid;       /* extended id type             */
    char *sqltypename;  /* extended type name           */
    int2 sqltypelen;   /* length of extended type name */
    int2 sqlownerlen;  /* length of owner name         */
    int2 sqlsourcetype;/* source type for distinct of built-ins */
    char *sqlownername; /* owner name                   */
    int4 sqlsourceid;	/* extended id of source type   */

    /*
     * sqlilongdata is new.  It supports data that exceeds the 32k
     * limit.  sqlilen and sqlidata are for backward compatibility
     * and they have maximum value of <32K.
     */
    char *sqlilongdata;	/* for data field beyond 32K	*/
    int4 sqlflags;		/* for internal use only        */
    void *sqlreserved;	/* reserved for future use      */
    } ifx_sqlvar_t;

typedef struct sqlda
    {
    int2 sqld;
    ifx_sqlvar_t *sqlvar;
    char desc_name[19];	/* descriptor name 			*/
    int2 desc_occ;		/* size of sqlda structure 	*/
    struct sqlda *desc_next;	/* pointer to next sqlda struct */
    void *reserved;		/* reserved for future use	*/
    } ifx_sqlda_t;

#ifdef __cplusplus
}
#endif

#ifdef __BORLANDC__
#pragma option -a-
#else
#pragma pack ()
#endif

#endif /* _SQLDA */
#line 88 "d:/informix/incl/esql/sqlda.h"
/* 
 * EXEC SQL include sqlstype.h;
 */
#line 16 "gxsql.ec"

#line 16 "gxsql.ec"
#line 1 "d:/informix/incl/esql/sqlstype.h"
/****************************************************************************
 *
 *                               IBM INC.
 *
 *                           PROPRIETARY DATA
 *
 * Licensed Material - Property Of IBM
 *
 * "Restricted Materails of IBM"
 *
 * IBM Informix Client SDK
 *
 * (c)  Copyright IBM Corporation 2002. All rights reserved.
 *
 *  Title:	sqlstype.h
 *  Sccsid:	@(#)sqlstype.h	9.6	3/30/93  18:06:39
 *  Description:
 *		defined symbols for SQL statement types
 *
 ***************************************************************************
 */

/*
 * SQL statement types
 */

#ifdef __cplusplus
extern "C" {
#endif

#define SQ_DATABASE	1	/* DATABASE              */
#define SQ_SELECT	2	/* SELECT		 */
#define SQ_SELINTO	3	/* SELECT INTO           */
#define SQ_UPDATE	4	/* UPDATE...WHERE        */
#define SQ_DELETE	5	/* DELETE...WHERE        */
#define SQ_INSERT	6	/* INSERT                */
#define SQ_UPDCURR	7	/* UPDATE WHERE CURRENT OF */
#define SQ_DELCURR	8	/* DELETE WHERE CURRENT OF */
#define SQ_LDINSERT	9	/* for internal use only */
#define SQ_LOCK		10	/* LOCK TABLE            */
#define SQ_UNLOCK	11	/* UNLOCK TABLE          */
#define SQ_CREADB	12	/* CREATE DATABASE       */
#define SQ_DROPDB	13	/* DROP DATABASE         */
#define SQ_CRETAB	14	/* CREATE TABLE          */
#define SQ_DRPTAB	15	/* DROP TABLE            */
#define SQ_CREIDX	16	/* CREATE INDEX          */
#define SQ_DRPIDX	17	/* DROP INDEX            */
#define SQ_GRANT	18	/* GRANT                 */
#define SQ_REVOKE	19	/* REVOKE                */
#define SQ_RENTAB	20	/* RENAME TABLE          */
#define SQ_RENCOL	21	/* RENAME COLUMN         */
#define SQ_CREAUD	22	/* CREATE AUDIT          */
#define SQ_STRAUD	23	/* for internal use only */
#define SQ_STPAUD	24	/* for internal use only */
#define SQ_DRPAUD	25	/* DROP AUDIT            */
#define SQ_RECTAB	26	/* RECOVER TABLE         */
#define SQ_CHKTAB	27	/* for internal use only */
#define SQ_REPTAB	28	/* for internal use only */
#define SQ_ALTER	29	/* ALTER TABLE           */
#define SQ_STATS	30	/* UPDATE STATISTICS     */
#define SQ_CLSDB	31	/* CLOSE DATABASE        */
#define SQ_DELALL	32	/* DELETE (no WHERE)     */
#define SQ_UPDALL	33	/* UPDATE (no WHERE)     */
#define SQ_BEGWORK	34	/* BEGIN WORK            */
#define SQ_COMMIT	35	/* COMMIT WORK           */
#define SQ_ROLLBACK	36	/* ROLLBACK WORK         */
#define SQ_SAVEPOINT	37	/* for internal use only */
#define SQ_STARTDB	38	/* START DATABASE        */
#define SQ_RFORWARD	39	/* ROLLFORWARD DATABASE  */
#define SQ_CREVIEW	40	/* CREATE VIEW           */
#define SQ_DROPVIEW	41	/* DROP VIEW             */
#define SQ_DEBUG	42	/* for internal use only */
#define SQ_CREASYN	43	/* CREATE SYNONYM        */
#define SQ_DROPSYN	44	/* DROP SYNONYM          */
#define SQ_CTEMP	45	/* CREATE TEMP TABLE     */
#define SQ_WAITFOR	46	/* SET LOCK MODE         */
#define SQ_ALTIDX       47	/* ALTER INDEX           */
#define SQ_ISOLATE	48	/* SET ISOLATION         */
#define SQ_SETLOG	49	/* SET LOG               */
#define SQ_EXPLAIN	50	/* SET EXPLAIN           */
#define SQ_SCHEMA	51	/* CREATE SCHEMA         */
#define SQ_OPTIM	52	/* SET OPTIMIZATION      */
#define SQ_CREPROC	53	/* CREATE PROCEDURE      */
#define SQ_DRPPROC	54	/* DROP PROCEDURE        */
#define SQ_CONSTRMODE   55	/* SET CONSTRAINTS       */
#define SQ_EXECPROC	56	/* EXECUTE PROCEDURE     */
#define SQ_DBGFILE	57	/* SET DEBUG FILE TO     */
#define SQ_CREOPCL	58	/* CREATE OPTICAL CLUSTER */
#define SQ_ALTOPCL	59	/* ALTER OPTICAL CLUSTER */
#define SQ_DRPOPCL	60	/* DROP OPTICAL CLUSTER  */
#define SQ_OPRESERVE	61	/* RESERVE (optical)     */
#define SQ_OPRELEASE	62	/* RELEASE (optical)     */
#define SQ_OPTIMEOUT	63	/* SET OPTICAL TIMEOUT  */
#define SQ_PROCSTATS    64	/* UPDATE STATISTICS (for procedure) */

/* 65 and 66 were used for SQ_GRANTGRP and SQ_REVOKGRP for KANJI
 * their functionality is replaced by ROLE
 */

/* 67, 68 and 69 were used for SQ_SKINHIBIT, SQ_SKSHOW and SQ_SKSMALL
 * which are no longer supported
 */

#define SQ_CRETRIG	70	/* CREATE TRIGGER        */
#define SQ_DRPTRIG	71	/* DROP TRIGGER          */

/*
 * This statement type is reserved for identifying new statements with
 * custom syntax extensions to the generic SQL parser
 */
#define SQ_UNKNOWN	72
#define SQ_SETDATASKIP	73	/* SET DATASKIP          */
#define SQ_PDQPRIORITY  74	/* SET PDQPRIORITY       */
#define SQ_ALTFRAG	75	/* ALTER FRAGMENT        */

#define SQ_SETOBJMODE   76      /* SET MODE ENABLED/DISABLED/FILTERING   */
#define SQ_START        77      /* START VIOLATIONS TABLE   */
#define SQ_STOP         78      /* STOP VIOLATIONS TABLE   */

#define SQ_SETMAC       79      /* SET SESSION LEVEL */
#define SQ_SETDAC       80      /* SET SESSION AUTHORIZATION */
#define SQ_SETTBLHI     81	/* SET TABLE HIGH */
#define SQ_SETLVEXT     82	/* SET EXTENT SIZE */

#ifdef ROLES
#define SQ_CREATEROLE   83	/* CREATE ROLE */
#define SQ_DROPROLE     84  	/* DROP ROLE */
#define SQ_SETROLE      85	/* SET ROLE */
#define SQ_PASSWD       86	/* SET DBPASSWORD */
#endif  /* ROLES */

#define SQ_RENDB	87	/* RENAME DATABASE */

#define SQ_CREADOM	88	/* CREATE DOMAIN */
#define SQ_DROPDOM	89	/* DROP DOMAIN   */

#define SQ_CREANRT	90	/* CREATE NAMED ROW TYPE */
#define SQ_DROPNRT	91	/* DROP NAMED ROW TYPE   */

#define SQ_CREADT       92      /* CREATE DISTINCT TYPE */
#define SQ_CREACT       93      /* CREATE CAST */
#define SQ_DROPCT       94      /* DROP CAST */

#define SQ_CREABT       95      /* CREATE OPAQUE TYPE */
#define SQ_DROPTYPE     96      /* DROP TYPE */

#define SQ_ALTERROUTINE 97      /* ALTER routine */

#define SQ_CREATEAM	98	/* CREATE ACCESS_METHOD */
#define SQ_DROPAM	99	/* DROP ACCESS_METHOD   */
#define SQ_ALTERAM     100	/* ALTER ACCESS_METHOD   */

#define SQ_CREATEOPC   101	/* CREATE OPCLASS */
#define SQ_DROPOPC     102	/* DROP OPCLASS   */

#define SQ_CREACST     103      /* CREATE CONSTRUCTOR */

#define SQ_SETRES      104      /* SET (MEMORY/NON)_RESIDENT */

#define SQ_CREAGG      105      /* CREATE AGGREGATE */
#define SQ_DRPAGG      106      /* DROP AGGREGATE   */

#define SQ_PLOADFILE    107     /* pload log file command*/
#define SQ_CHKIDX       108     /* onutil check index command */
#define SQ_SCHEDULE     109      /* set schedule          */
#define SQ_SETENV       110      /* "set environment ..." */
#define SQ_XPS_RES2     111     /* reserved for future use */
#define SQ_XPS_RES3     112     /* reserved for future use */
#define SQ_XPS_RES4     113     /* reserved for future use */
#define SQ_XPS_RES5     114     /* reserved for future use */

#define SQ_STMT_CACHE  115      /* SET STMT_CACHE */

#define SQ_RENIDX       116     /* RENAME INDEX   */

#define SQ_MAXSTMT      SQ_RENIDX

#ifdef __cplusplus
}
#endif

#line 181 "d:/informix/incl/esql/sqlstype.h"
/* 
 * EXEC SQL include sqltypes.h;
 */
#line 17 "gxsql.ec"

#line 17 "gxsql.ec"
#line 1 "d:/informix/incl/esql/sqltypes.h"
/****************************************************************************
 *
 *                               IBM INC.
 *
 *                           PROPRIETARY DATA
 *
 * Licensed Material - Property Of IBM
 *
 * "Restricted Materails of IBM"
 *
 * IBM Informix Client SDK
 *
 * (c)  Copyright IBM Corporation 2002. All rights reserved.
 *
 *  Title:	sqltypes.h
 *  Sccsid:     @(#)sqltypes.h	9.6	11/2/93  10:31:37
 *  Description:
 *		type definition
 *
 ***************************************************************************
 */

#ifndef CCHARTYPE
#ifdef __cplusplus
extern "C" {
#endif

#include "ifxtypes.h"

/***********************
 * ++++ CAUTION ++++
 * Any new type to be added to the following lists should not
 * have the following bit pattern (binary short):
 *
 *	xxxxx111xxxxxxxx
 *
 * where x can be either 0 or 1.
 *
 * This is due to the use of the bits as SQLNONULL, SQLHOST and SQLNETFLT
 * (see below).
 *
 * FAILURE TO DO SO WOULD RESULT IN POSSIBLE ERRORS DURING CONVERSIONS.
 *
 ***********************/

 /* C language types */

#define CCHARTYPE	100
#define CSHORTTYPE	101
#define CINTTYPE	102
#define CLONGTYPE	103
#define CFLOATTYPE	104
#define CDOUBLETYPE	105
#define CDECIMALTYPE	107
#define CFIXCHARTYPE	108
#define CSTRINGTYPE	109
#define CDATETYPE	110
#define CMONEYTYPE	111
#define CDTIMETYPE	112
#define CLOCATORTYPE    113
#define CVCHARTYPE	114
#define CINVTYPE	115
#define CFILETYPE	116
#define CINT8TYPE	117
#define CCOLLTYPE       118
#define CLVCHARTYPE     119
#define CFIXBINTYPE     120
#define CVARBINTYPE     121
#define CBOOLTYPE       122
#define CROWTYPE        123
#define CLVCHARPTRTYPE  124
#define CTYPEMAX         25

#define USERCOLL(x)	((x))

#define COLLMASK        0x007F  /* mask out CTYPEDCOLL or CCLIENTCOLL */
                                /* bit set for CCOLLTYPE or CROWTYPE */
#define ISCOLLECTIONVAR(n)  (((n) & COLLMASK) == CCOLLTYPE)
#define ISROWVAR(n)         (((n) & COLLMASK) == CROWTYPE)
#define ISCOLL_OR_ROWVAR(n)   ((ISCOLLECTIONVAR(n)) || (ISROWVAR(n)))
#define CCLIENTCOLL     SQLCLIENTCOLL /* client collection bit */
#define ISCLIENTCOLLECTION(n) (ISCOLLECTIONVAR(n) && ((n) & CCLIENTCOLL))
#define ISCLIENTCOMPLEX(n)    ((ISCLIENTCOLLECTION(n)) || (ISROWVAR(n)))

/*
 * The following are for client side only. They are included here
 * because of the above related definitions.
 */
#define CTYPEDCOLL       0x0080  /* typed collection bit */
#define CTYPEDCOLLUNMASK 0xFF7F  /* unmask typed collection bit */
#define ISTYPEDCOLLECTION(n)  (ISCOLLECTIONVAR(n) && ((n) & CTYPEDCOLL))
#define ISTYPEDROW(n)         (ISROWVAR(n) && ((n) & CTYPEDCOLL))
#define ISTYPEDCOLL_OR_ROW(n)  ( (ISTYPEDCOLLECTION(n)) || (ISTYPEDROW(n)) )

/*
 * Define all possible database types
 *   include C-ISAM types here as well as in isam.h
 */

#define SQLCHAR		0
#define SQLSMINT	1
#define SQLINT		2
#define SQLFLOAT	3
#define SQLSMFLOAT	4
#define SQLDECIMAL	5
#define SQLSERIAL	6
#define SQLDATE		7
#define SQLMONEY	8
#define SQLNULL		9
#define SQLDTIME	10
#define SQLBYTES	11
#define SQLTEXT		12
#define SQLVCHAR	13
#define SQLINTERVAL	14
#define SQLNCHAR	15
#define SQLNVCHAR	16
#define SQLINT8		17
#define SQLSERIAL8	18
#define SQLSET          19
#define SQLMULTISET     20
#define SQLLIST         21
#define SQLROW          22
#define SQLCOLLECTION   23
#define SQLROWREF   	24
/*
 * Note: SQLXXX values from 25 through 39 are reserved to avoid collision
 *       with reserved PTXXX values in that same range. See p_types_t.h
 *
 * REFSER8: create tab with ref: referenced serial 8 rsam counter
 *	this is essentially a SERIAL8, but is an additional rsam counter
 *	this type only lives in the system catalogs and when read from
 *	disk is converted to SQLSERIAL8 with CD_REFSER8 set in ddcol_t
 *      ddc_flags we must distinguish from SERIAL8 to allow both
 *      counters in one tab
 */
#define SQLUDTVAR   	40
#define SQLUDTFIXED   	41
#define SQLREFSER8   	42

/* These types are used by FE, they are not real major types in BE */
#define SQLLVARCHAR     43
#define SQLSENDRECV     44
#define SQLBOOL         45
#define SQLIMPEXP       46
#define SQLIMPEXPBIN    47

/* This type is used by the UDR code to track default parameters,
   it is not a real major type in BE */
#define SQLUDRDEFAULT   48

#define SQLMAXTYPES	49

#define SQLLABEL        SQLINT

#define SQLTYPE		0xFF	/* type mask		*/

#define SQLNONULL	0x0100	/* disallow nulls	*/
/* a bit to show that the value is from a host variable */
#define SQLHOST		0x0200	/* Value is from host var. */
#define SQLNETFLT	0x0400	/* float-to-decimal for networked backend */
#define SQLDISTINCT	0x0800	/* distinct bit		*/
#define SQLNAMED	0x1000	/* Named row type vs row type */
#define SQLDLVARCHAR    0x2000  /* Distinct of lvarchar */
#define SQLDBOOLEAN     0x4000  /* Distinct of boolean */
#define SQLCLIENTCOLL   0x8000  /* Collection is processed on client */

/* we are overloading SQLDBOOLEAN for use with row types */
#define SQLVARROWTYPE   0x4000  /* varlen row type */

/* We overload SQLNAMED for use with constructor type, this flag
 * distinguish constructor types from other UDT types.
 *
 * Please do not test this bit directly, use macro ISCSTTYPE() instead.
 */
#define SQLCSTTYPE	0x1000	/* constructor type flag */

#define TYPEIDMASK      (SQLTYPE | SQLDISTINCT | SQLNAMED | \
                         SQLDLVARCHAR | SQLDBOOLEAN )

#define SIZCHAR		1
#define SIZSMINT	2
#define SIZINT		4
#define SIZFLOAT	(sizeof(double))
#define SIZSMFLOAT	(sizeof(float))
#define SIZDECIMAL	17	/* decimal(32) */
#define SIZSERIAL	4
#define SIZDATE		4
#define SIZMONEY	17	/* decimal(32) */
#define SIZDTIME	7	/* decimal(12,0) */
#define SIZVCHAR	1
#define SIZINT8		(sizeof(short) + sizeof(muint) * 2)
#define SIZSERIAL8	SIZINT8
#define SIZCOLL		sizeof (ifx_collection_t)
#define SIZSET		SIZCOLL
#define SIZMULTISET	SIZCOLL
#define SIZLIST		SIZCOLL
#define SIZROWREF	sizeof (ifx_ref_t)

#define MASKNONULL(t)	((t) & (SQLTYPE))
#define ISSQLTYPE(t)	(MASKNONULL(t) >= SQLCHAR && MASKNONULL(t) < SQLMAXTYPES)

/*
 * SQL types macros
 */
#define ISDECTYPE(t)		(MASKNONULL(t) == SQLDECIMAL || \
				 MASKNONULL(t) == SQLMONEY || \
				 MASKNONULL(t) == SQLDTIME || \
				 MASKNONULL(t) == SQLINTERVAL)
#define ISNUMERICTYPE(t)	(MASKNONULL(t) == SQLSMINT || \
				 MASKNONULL(t) == SQLINT || \
				 MASKNONULL(t) == SQLINT8 || \
				 MASKNONULL(t) == SQLFLOAT || \
				 MASKNONULL(t) == SQLSMFLOAT || \
				 MASKNONULL(t) == SQLMONEY || \
				 MASKNONULL(t) == SQLSERIAL || \
				 MASKNONULL(t) == SQLSERIAL8 || \
  				 MASKNONULL(t) == SQLDECIMAL)
#define ISBLOBTYPE(type)	(ISBYTESTYPE (type) || ISTEXTTYPE(type))
#define ISBYTESTYPE(type)	(MASKNONULL(type) == SQLBYTES)
#define ISTEXTTYPE(type)	(MASKNONULL(type) == SQLTEXT)
#define ISSQLHOST(t)            (((t) & SQLHOST) == SQLHOST)

#define ISVCTYPE(t)		(MASKNONULL(t) == SQLVCHAR || \
				 MASKNONULL(t) == SQLNVCHAR)
#define ISCHARTYPE(t)		(MASKNONULL(t) == SQLCHAR || \
				 MASKNONULL(t) == SQLNCHAR)
#define ISNSTRINGTYPE(t)	(MASKNONULL(t) == SQLNCHAR || \
				 MASKNONULL(t) == SQLNVCHAR)

#define ISSTRINGTYPE(t)		(ISVCTYPE(t) || ISCHARTYPE(t))

#define	ISUDTVARTYPE(t)		(MASKNONULL(t) == SQLUDTVAR)
#define	ISUDTFIXEDTYPE(t)	(MASKNONULL(t) == SQLUDTFIXED)
#define	ISUDTTYPE(t)		(ISUDTVARTYPE(t) || ISUDTFIXEDTYPE(t))

#define	ISCOMPLEXTYPE(t)	(ISROWTYPE(t) || ISCOLLTYPE(t))
#define	ISROWTYPE(t)		(MASKNONULL(t) == SQLROW)
#define	ISLISTTYPE(t)		(MASKNONULL(t) == SQLLIST)
#define	ISMULTISETTYPE(t)	(MASKNONULL(t) == SQLMULTISET)
#define	ISREFTYPE(t)		(MASKNONULL(t) == SQLROWREF)
#define	ISSETTYPE(t)		(MASKNONULL(t) == SQLSET)
#define	ISCOLLECTTYPE(t)	(MASKNONULL(t) == SQLCOLLECTION)
#define ISCOLLTYPE(t)		(ISSETTYPE(t) || ISMULTISETTYPE(t) ||\
				 ISLISTTYPE(t) || ISCOLLECTTYPE(t))

#define ISSERIALTYPE(t)		(((t) & SQLTYPE) == SQLSERIAL || \
				 ((t) & SQLTYPE) == SQLSERIAL8 || \
				 ((t) & SQLTYPE) == SQLREFSER8)

#define ISDISTINCTTYPE(t)	((t) & SQLDISTINCT)
#define ISCSTTYPE(t)		(ISUDTTYPE(t) && ((t) & SQLCSTTYPE))

/* these macros are used to distinguish NLS char types and non-nls (ASCII)
 * char types
 */
#define ISNONNLSCHAR(t)		(MASKNONULL(t) == SQLCHAR || \
				 MASKNONULL(t) == SQLVCHAR)

/* these macros should be used in case statements
 */
#define CHARCASE		SQLCHAR: case SQLNCHAR
#define VCHARCASE		SQLVCHAR: case SQLNVCHAR

#define UDTCASE		 	SQLUDTVAR: case SQLUDTFIXED

/*
 * C types macros
 */
#define ISBLOBCTYPE(type)	(ISLOCTYPE(type) || ISFILETYPE(type))
#define ISLOCTYPE(type)		(MASKNONULL(type) == CLOCATORTYPE)
#define ISFILETYPE(type)	(MASKNONULL(type) == CFILETYPE)
#define ISLVCHARCTYPE(type)     (MASKNONULL(type) == CLVCHARTYPE)
#define ISLVCHARCPTRTYPE(type)  (MASKNONULL(type) == CLVCHARPTRTYPE)
#define ISFIXBINCTYPE(type)     (MASKNONULL(type) == CFIXBINTYPE)
#define ISVARBINCTYPE(type)     (MASKNONULL(type) == CVARBINTYPE)
#define ISBOOLCTYPE(type)       (MASKNONULL(type) == CBOOLTYPE)


#define ISOPTICALCOL(type)	(type == 'O')

#define DEFDECIMAL	9	/* default decimal(16) size */
#define DEFMONEY	9	/* default decimal(16) size */

#define SYSPUBLIC	"public"

/*
 * if an SQL type is specified, convert to default C type
 *  map C int to either short or long
 */


#define TYPEMAX	SQLMAXTYPES

extern int2 sqlctype[];

#define toctype(ctype, type) \
    { \
    if (type == CINTTYPE) \
    { \
      if (sizeof(mint) == sizeof(mlong)) \
          { ctype = type = CLONGTYPE; } \
      else if (sizeof(mint) == sizeof(int2)) \
          { ctype = type = CSHORTTYPE; } \
      else \
          { ctype = CLONGTYPE; type = CINTTYPE; } \
    } \
    else if (type >= 0 && type < TYPEMAX) \
        ctype = sqlctype[type]; \
    else \
        ctype = type; \
    }



/* Extended ID definitions for predefined UDT's */
/* These can not be changed because sqli is using
 * them.  If we change them, the client has to recompile.
 * NOTE: This order must match the definitions in boot90.sql
 */

#define XID_LVARCHAR            1
#define XID_SENDRECV            2
#define XID_IMPEXP              3
#define XID_IMPEXPBIN           4
#define XID_BOOLEAN             5
#define XID_POINTER             6
#define XID_INDEXKEYARRAY       7
#define XID_RTNPARAMTYPES	8
#define XID_SELFUNCARGS         9
#define XID_BLOB                10
#define XID_CLOB                11
#define XID_LOLIST              12
#define XID_STAT                15
#define XID_CLIENTBINVAL        16

/* Max size definitions for the predefined UDT's.
 * Only a few are currently defined.
 */
#define SIZINDEXKEYARRAY	1024
#define SIZLVARCHAR		2048
#define SIZRTNPARAMTYPES	4096
#define SIZSTAT             272

/* Alignment required by predefined UDT's.
 * Only a few are currently defined.  At a minimum,
 * all with alignment not 1 should be defined here
 * and used throughout the code.
 */
#define ALNINDEXKEYARRAY	4
#define ALNSTAT             8


#define USER_XTDTYPEID_START	2048

/* These macros should be used to test lvarchar and distinct of lvarchar */

#define ISLVARCHARXTYPE(t, xid)  (ISUDTTYPE((t)) && (xid) == XID_LVARCHAR)
#define ISDISTINCTLVARCHAR(t)	((t) & SQLDLVARCHAR)
#define LIKELVARCHARXTYPE(t,xid) ((ISLVARCHARXTYPE(t,xid))||\
				   ISDISTINCTLVARCHAR(t))

#define ISSMARTBLOB(type, xid)  (ISUDTTYPE((type)) && \
				    ((xid == XID_CLOB) || (xid == XID_BLOB)))

/* These macros should be used to test boolean and distinct of boolean */
#define ISBOOLEANXTYPE(t, xid)  (ISUDTTYPE((t)) && (xid) == XID_BOOLEAN)
#define ISDISTINCTBOOLEAN(t)	(((t) & SQLDBOOLEAN) && \
				    (ISUDTTYPE(t)))
#define LIKEBOOLEANXTYPE(t,xid) ((ISBOOLEANXTYPE(t,xid))||\
				  ISDISTINCTBOOLEAN(t))

#define ISFIXLENGTHTYPE(t)	(!ISBYTESTYPE(t) && !ISTEXTTYPE(t) \
					&& !ISCOMPLEXTYPE(t) \
					&& !ISUDTVARTYPE(t) \
					&& !ISVCTYPE(t))

#ifdef __cplusplus
}
#endif

#endif /* CCHARTYPE */
#line 381 "d:/informix/incl/esql/sqltypes.h"
/* 
 * EXEC SQL include decimal.h;
 */
#line 18 "gxsql.ec"

#line 18 "gxsql.ec"
#line 1 "d:/informix/incl/esql/decimal.h"
/****************************************************************************
 *
 *                               IBM INC.
 *
 *                           PROPRIETARY DATA
 *
 * Licensed Material - Property Of IBM
 *
 * "Restricted Materails of IBM"
 *
 * IBM Informix Client SDK
 *
 * (c)  Copyright IBM Corporation 2002. All rights reserved.
 *
 *  Title:	decimal.h
 *  Sccsid:	@(#)decimal.h	9.2	1/12/92  12:06:37
 *		(created from isam/decimal.h version 6.1)
 *  Description:
 *		Header file for decimal data type.
 *
 ***************************************************************************
 */

#ifndef _DECIMAL_H
#define _DECIMAL_H

#ifdef __BORLANDC__
#pragma option -a8
#else
#pragma pack (8)
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include "ifxtypes.h"

/*
 * Unpacked Format (format for program usage)
 *
 *    Signed exponent "dec_exp" ranging from  -64 to +63
 *    Separate sign of mantissa "dec_pos"
 *    Base 100 digits (range 0 - 99) with decimal point
 *	immediately to the left of first digit.
 */

#define DECSIZE 16
#define DECUNKNOWN -2

struct decimal
    {
    int2 dec_exp;		/* exponent base 100		*/
    int2 dec_pos;		/* sign: 1=pos, 0=neg, -1=null	*/
    int2 dec_ndgts;		/* number of significant digits	*/
    char  dec_dgts[DECSIZE];	/* actual digits base 100	*/
    };
#ifdef OSF_MLS
typedef struct decimal ix_dec_t;
#else
typedef struct decimal dec_t;
#endif

/*
 * Datatypes for DataBlade developers
 */
typedef struct decimal mi_money;
typedef struct decimal mi_decimal;

/*
 *  A decimal null will be represented internally by setting dec_pos
 *  equal to DECPOSNULL
 */

#define DECPOSNULL	(-1)

/*
 * DECLEN calculates minumum number of bytes
 * necessary to hold a decimal(m,n)
 * where m = total # significant digits and
 *	 n = significant digits to right of decimal
 */

#define DECLEN(m,n)	(((m)+((n)&1)+3)/2)
#define DECLENGTH(len)	DECLEN(PRECTOT(len),PRECDEC(len))

/*
 * DECPREC calculates a default precision given
 * number of bytes used to store number
 */

#define DECPREC(size)	(((size-1)<<9)+2)

/* macros to look at and make encoded decimal precision
 *
 *  PRECTOT(x)		return total precision (digits total)
 *  PRECDEC(x) 		return decimal precision (digits to right)
 *  PRECMAKE(x,y)	make precision from total and decimal
 */

#define PRECTOT(x)	(((x)>>8) & 0xff)
#define PRECDEC(x)	((x) & 0xff)
#define PRECMAKE(x,y)	(((x)<<8) + (y))

/*
 * Packed Format  (format in records in files)
 *
 *    First byte =
 *	  top 1 bit = sign 0=neg, 1=pos
 *	  low 7 bits = Exponent in excess 64 format
 *    Rest of bytes = base 100 digits in 100 complement format
 *    Notes --	This format sorts numerically with just a
 *		simple byte by byte unsigned comparison.
 *		Zero is represented as 80,00,00,... (hex).
 *		Negative numbers have the exponent complemented
 *		and the base 100 digits in 100's complement
 */

#ifdef __cplusplus
}
#endif

#ifdef __BORLANDC__
#pragma option -a-
#else
#pragma pack ()
#endif

#endif /* _DECIMAL_H */
#line 129 "d:/informix/incl/esql/decimal.h"
#line 19 "gxsql.ec"

mint stleng( char * );

int a_kun_gx_kb_cursor_open;
int pazlsp_cursor_open;
int nArtikel;
char command[80];
char frmtxt1[2];
char frmtxt2[2];
char frmtxt3[2];
char frmtxt4[2];
char frmtxt5[2];
char frmtxt6[2];
char frmtxt7[2];
char frmtxt8[2];
char frmtxt9[2];

extern int testmode;
extern FILE *fperr;

char txtNR[80];

void SendLabelDataToGXDB( void );

/*
 * EXEC SQL BEGIN DECLARE SECTION;
 */
#line 43 "gxsql.ec"
#line 44 "gxsql.ec"
#line 45 "gxsql.ec"
long system_num;
#line 46 "gxsql.ec"
int pazmdn;
typedef struct
  {
    long mdn;
    long fil;
    long kun;
      char charge[30];
    long partie;
      char esnum[30];
      char es2num[30];
      char es3num[30];
      char ez1num[16];
      char ez2num[16];
      char ez3num[16];
    long tour;
    long amenge;
    int up_ka;
    int up_pal;
    int aeinheit;
    int keinheit;
    int paleinheit;
    int hbk_ztr;
    double a_gew;
    double a_tara;
    int a_typ;
    int a_typ2;
      char datum1[30];
      char datum2[30];
      char kuna[20];
    long charg_hand;
    long inh_wanne;
      char glg[2];
      char krebs[2];
      char ei[2];
      char fisch[2];
      char erdnuss[2];
      char soja[2];
      char milch[2];
      char schal[2];
      char sellerie[2];
      char senfsaat[2];
      char sesamsamen[2];
      char sulfit[2];
      char weichtier[2];
      char brennwert[21];
    double eiweiss;
    double kh;
    double fett;
  }  DATA_t;
#line 96 "gxsql.ec"
DATA_t data;
typedef struct
  {
    double a;
      char pp_a_bz1[100];
      char pp_a_bz2[100];
      char lgr_tmpr[100];
      char lupine[2];
      char schutzgas[2];
    short huelle;
    double salz;
    double davonfett;
    double davonzucker;
    double ballaststoffe;
      char zutat[2001];
  }  A_BAS_ERW_t;
#line 113 "gxsql.ec"
A_BAS_ERW_t a_bas_erw;
typedef struct
  {
    int mdn;
    int fil;
    long kun;
    long adr1;
    long stat_kun;
    int kun_gr1;
      char kun_bran2[3];
      char adr_nam1[37];
      char adr_nam2[37];
      char plz[9];
      char str[37];
      char ort1[37];
  }  KUN_t;
#line 130 "gxsql.ec"
KUN_t kun;
typedef struct
  {
    int geb_fill;
    int pal_fill;
    long geb_anz;
    long pal_anz;
    double tara;
  }  KUN_GEB_t;
#line 140 "gxsql.ec"
KUN_GEB_t kun_geb;
typedef struct
  {
    int mdn;
    long ls;
    long kun;
      char kun_bran2[3];
    double a;
    double ls_lad_euro;
  }  PAZLSP_t;
#line 151 "gxsql.ec"
PAZLSP_t pazlsp;
typedef struct
  {
    long bran_kun;
    double a;
    double ld_pr;
    int akz;
  }  PAZ_IPR_t;
#line 160 "gxsql.ec"
PAZ_IPR_t paz_ipr;
typedef struct
  {
    int cabnum;
      char cab[201];
  }  PAZ_CAB_t;
#line 167 "gxsql.ec"
PAZ_CAB_t paz_cab;
typedef struct
  {
    long kun;
    double a;
      char a_kun[26];
  }  A_KUN_t;
#line 175 "gxsql.ec"
A_KUN_t a_kun;
typedef struct
  {
    int mdn;
    int fil;
    long kun;
    double a;
      char a_kun[13];
      char a_bz1[25];
    int me_einh_kun;
    double inh;
      char kun_bran2[3];
    double tara;
    double ean;
      char a_bz2[25];
    int hbk_ztr;
    long kopf_text;
      char pr_rech_kz[2];
      char modif[2];
    long text_nr;
    int devise;
      char geb_eti[2];
    int geb_fill;
    long geb_anz;
      char pal_eti[2];
    int pal_fill;
    long pal_anz;
      char pos_eti[2];
    int sg1;
    int sg2;
    int pos_fill;
    int ausz_art;
    long text_nr2;
    int cab;
      char a_bz3[100];
      char a_bz4[100];
    int eti_typ;
    long mhd_text;
    long freitext1;
    long freitext2;
    long freitext3;
    int sg3;
    int eti_sum1;
    int eti_sum2;
    int eti_sum3;
    int eti_nve1;
    int eti_nve2;
    long ampar;
    int sonder_eti;
    long text_nr_a;
    double ean1;
    int cab1;
    double ean2;
    int cab2;
    double ean3;
    int cab3;
    int cab_nve1;
    int cab_nve2;
    long gwpar;
    long vppar;
    int devise2;
  }  A_KUN_GX_t;
#line 238 "gxsql.ec"
A_KUN_GX_t a_kun_gx;
typedef struct
  {
    long text_nr;
    int eti_typ;
    int sg1;
      char txt1[100];
    int sg2;
      char txt2[100];
    int sg3;
      char txt3[100];
    int sg4;
      char txt4[100];
    int sg5;
      char txt5[100];
    int sg6;
      char txt6[100];
    int sg7;
      char txt7[100];
    int sg8;
      char txt8[100];
    int sg9;
      char txt9[100];
    int sg10;
      char txt10[100];
    int sg11;
      char txt11[100];
    int sg12;
      char txt12[100];
    int sg13;
      char txt13[100];
    int sg14;
      char txt14[100];
    int sg15;
      char txt15[100];
    int sg16;
      char txt16[100];
    int sg17;
      char txt17[100];
    int sg18;
      char txt18[100];
    int sg19;
      char txt19[100];
    int sg20;
      char txt20[100];
  }  PR_AUSZ_GX_t;
#line 265 "gxsql.ec"
PR_AUSZ_GX_t pr_ausz_gx;
typedef struct
  {
    int etyp;
    long a;
    long ls;
    long gpreis;
    long spreis;
    long stk;
    long netto;
    long tara;
    long datum1;
    long datum2;
    long geraet;
    long numerator;
    long sys;
    long satz;
      dtime_t zeit_ins;
  }  PAZ_STATISTIK_t;
#line 285 "gxsql.ec"
PAZ_STATISTIK_t paz_statistik =
#line 285 "gxsql.ec"
  {
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    0,
#line 285 "gxsql.ec"
    {3594},
#line 285 "gxsql.ec"
  };
#line 287 "gxsql.ec"
  char statement[3000];
#line 288 "gxsql.ec"
  char anytxt[300];
/*
 * EXEC SQL END DECLARE SECTION;
 */
#line 290 "gxsql.ec"


int cursors_initialized;

int OpenDatabase( void )
{
    int has_trans;
/*
 * 	EXEC SQL DATABASE BWS;
 */
#line 297 "gxsql.ec"
  {
#line 297 "gxsql.ec"
  sqli_db_open("bws", 0);
#line 297 "gxsql.ec"
  }
	if (SQLCODE != 0)
	{
		fprintf(fperr,"DATABASE BWS: SQLCODE=%ld\n",SQLCODE);
		exit(1);
	}
	has_trans = (sqlca.sqlwarn.sqlwarn1 == 'W');
	if (has_trans == 0)
	{
		TimeMsgLine( "Datenbank BWS geoeffnet: Transaktionen AUS." );
	}
	else
	{
		TimeMsgLine( "Datenbank BWS geoeffnet: Transaktionen EIN." );
	}
/*
 * 	EXEC SQL SET ISOLATION TO DIRTY READ;
 */
#line 312 "gxsql.ec"
  {
#line 312 "gxsql.ec"
  static const char *sqlcmdtxt[] =
#line 312 "gxsql.ec"
    {
#line 312 "gxsql.ec"
    " SET ISOLATION TO DIRTY READ",
    0
    };
#line 312 "gxsql.ec"
  static ifx_statement_t _SQ0 = {0};
#line 312 "gxsql.ec"
  sqli_stmt(ESQLINTVERSION, &_SQ0, (char **) sqlcmdtxt, 0, (ifx_sqlvar_t *) 0, (struct value *) 0, (ifx_literal_t *) 0, (ifx_namelist_t *) 0, (ifx_cursor_t *) 0, -1, 0, 0);
#line 312 "gxsql.ec"
  }
	return 0;
}

void CloseDatabase( void )
{
/*
 * 	EXEC SQL CLOSE BWS;
 */
#line 318 "gxsql.ec"
  {
#line 318 "gxsql.ec"
#line 318 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn1, 768));
#line 318 "gxsql.ec"
  }
/*
 * 	EXEC SQL DISCONNECT CURRENT;
 */
#line 319 "gxsql.ec"
  {
#line 319 "gxsql.ec"
  sqli_connect_close(3, (char *) 0, 0, 0);
#line 319 "gxsql.ec"
  }
    TimeMsgLine( "Datenbank BWS geschlossen." );
}

void FormQuery_a_kun( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT A_KUN FROM A_KUN WHERE A = ? AND KUN = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

/*
 * 	EXEC SQL prepare p_a_kun_curs from :statement;
 */
#line 330 "gxsql.ec"
  {
#line 330 "gxsql.ec"
#line 330 "gxsql.ec"
#line 330 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn2, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 330 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare a_kun_curs cursor for p_a_kun_curs;
 */
#line 332 "gxsql.ec"
  {
#line 332 "gxsql.ec"
#line 332 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn3, 512), (char *) _Cn3, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn2, 513), 0, 0);
#line 332 "gxsql.ec"
  }
	exp_chk( statement, WARN );
}


void FormQuery_a_kun_gx_akb( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT EAN, KOPF_TEXT, PR_RECH_KZ, TEXT_NR, " );
	p += sprintf( p, "SG1, SG2, AUSZ_ART, CAB, A_BZ3, A_BZ4, ETI_TYP, " );
	p += sprintf( p, "MHD_TEXT, ETI_SUM1, ETI_SUM2, AMPAR, SONDER_ETI, "  );
	p += sprintf( p, "EAN1, CAB1, GWPAR, VPPAR, TARA " );
	p += sprintf( p, " FROM A_KUN_GX WHERE A = ? AND KUN = ? AND KUN_BRAN2 = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

/*
 * 	EXEC SQL prepare p_a_kun_gx_akb_curs from :statement;
 */
#line 348 "gxsql.ec"
  {
#line 348 "gxsql.ec"
#line 348 "gxsql.ec"
#line 348 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn4, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 348 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare a_kun_gx_akb_curs cursor for p_a_kun_gx_akb_curs;
 */
#line 350 "gxsql.ec"
  {
#line 350 "gxsql.ec"
#line 350 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn5, 512), (char *) _Cn5, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn4, 513), 0, 0);
#line 350 "gxsql.ec"
  }
	exp_chk( statement, WARN );
}

void FormQuery_a_kun_gx_ak( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT EAN, KOPF_TEXT, PR_RECH_KZ, TEXT_NR, " );
	p += sprintf( p, "SG1, SG2, AUSZ_ART, CAB, A_BZ3, A_BZ4, ETI_TYP, " );
	p += sprintf( p, "MHD_TEXT, HBK_ZTR, ETI_SUM1, ETI_SUM2, AMPAR, SONDER_ETI, "  );
	p += sprintf( p, "EAN1, CAB1, GWPAR, VPPAR, TARA, INH, DEVISE, PR_RECH_KZ, " );
	p += sprintf( p, "ETI_SUM3, ETI_NVE1, ETI_NVE2, CAB_NVE1, CAB_NVE2, " );
	p += sprintf( p, "TEXT_NR2, GEB_ANZ, PAL_ANZ, GEB_FILL, PAL_FILL, GEB_ETI, FREITEXT1 " );
	p += sprintf( p, " FROM A_KUN_GX WHERE MDN = ? AND A = ? AND KUN = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

/*
 * 	EXEC SQL prepare p_a_kun_gx_ak_curs from :statement;
 */
#line 367 "gxsql.ec"
  {
#line 367 "gxsql.ec"
#line 367 "gxsql.ec"
#line 367 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn6, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 367 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare a_kun_gx_ak_curs cursor for p_a_kun_gx_ak_curs;
 */
#line 369 "gxsql.ec"
  {
#line 369 "gxsql.ec"
#line 369 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn7, 512), (char *) _Cn7, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn6, 513), 0, 0);
#line 369 "gxsql.ec"
  }
	exp_chk( statement, WARN );
}

void FormQuery_a_kun_gx_ab( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT EAN, KOPF_TEXT, PR_RECH_KZ, TEXT_NR, " );
	p += sprintf( p, "SG1, SG2, AUSZ_ART, CAB, A_BZ3, A_BZ4, ETI_TYP, " );
	p += sprintf( p, "MHD_TEXT, HBK_ZTR, ETI_SUM1, ETI_SUM2, AMPAR, SONDER_ETI, "  );
	p += sprintf( p, "EAN1, CAB1, GWPAR, VPPAR, TARA, INH, DEVISE, PR_RECH_KZ, " );
	p += sprintf( p, "ETI_SUM3, ETI_NVE1, ETI_NVE2, CAB_NVE1, CAB_NVE2, " );
	p += sprintf( p, "TEXT_NR2, GEB_ANZ, PAL_ANZ, GEB_FILL, PAL_FILL, GEB_ETI, FREITEXT1 " );
	p += sprintf( p, " FROM A_KUN_GX WHERE MDN = ? AND A = ? AND KUN_BRAN2 = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

/*
 * 	EXEC SQL prepare p_a_kun_gx_ab_curs from :statement;
 */
#line 386 "gxsql.ec"
  {
#line 386 "gxsql.ec"
#line 386 "gxsql.ec"
#line 386 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn8, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 386 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare a_kun_gx_ab_curs cursor for p_a_kun_gx_ab_curs;
 */
#line 388 "gxsql.ec"
  {
#line 388 "gxsql.ec"
#line 388 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn9, 512), (char *) _Cn9, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn8, 513), 0, 0);
#line 388 "gxsql.ec"
  }
	exp_chk( statement, WARN );
}

void FormQuery_pazlsp( void )
{
    char *p;
  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     ||
  	     strcmp( anwender, "DFW" ) == 0
  	     ||
  	     strcmp( anwender, "OKLE" ) == 0
  	     ||
  	     strcmp( anwender, "LOHMANN" ) == 0
  	     ||
  	     strcmp( anwender, "KUEBLER" ) == 0
  	)
    {
        return;
    }
    p = statement;
	p += sprintf( p, "SELECT A, KUN, KUN_BRAN2, LS_LAD_EURO " );
	p += sprintf( p, " FROM PAZLSP WHERE MDN = ? AND LS = ? AND A > 1000 " );
	p += sprintf( p, " ORDER BY A" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

/*
 * 	EXEC SQL prepare p_pazlsp_curs from :statement;
 */
#line 416 "gxsql.ec"
  {
#line 416 "gxsql.ec"
#line 416 "gxsql.ec"
#line 416 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn10, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 416 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare pazlsp_curs cursor for p_pazlsp_curs;
 */
#line 418 "gxsql.ec"
  {
#line 418 "gxsql.ec"
#line 418 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn11, 512), (char *) _Cn11, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn10, 513), 0, 0);
#line 418 "gxsql.ec"
  }
	exp_chk( statement, WARN );

    pazlsp_cursor_open = 0;
}

void FormQuery_a_kun_gx_kb( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT A, EAN, KOPF_TEXT, PR_RECH_KZ, TEXT_NR, " );
	p += sprintf( p, "SG1, SG2, AUSZ_ART, CAB, A_BZ3, A_BZ4, ETI_TYP, " );
	p += sprintf( p, "MHD_TEXT, ETI_SUM1, ETI_SUM2, AMPAR, SONDER_ETI, "  );
	p += sprintf( p, "EAN1, CAB1, GWPAR, VPPAR, TARA " );
	p += sprintf( p, " FROM A_KUN_GX WHERE KUN = ? AND KUN_BRAN2 = ?" );
	p += sprintf( p, " ORDER BY A" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

/*
 * 	EXEC SQL prepare p_a_kun_gx_kb_curs from :statement;
 */
#line 436 "gxsql.ec"
  {
#line 436 "gxsql.ec"
#line 436 "gxsql.ec"
#line 436 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn12, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 436 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare a_kun_gx_kb_curs cursor for p_a_kun_gx_kb_curs;
 */
#line 438 "gxsql.ec"
  {
#line 438 "gxsql.ec"
#line 438 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn13, 512), (char *) _Cn13, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn12, 513), 0, 0);
#line 438 "gxsql.ec"
  }
	exp_chk( statement, WARN );

    a_kun_gx_kb_cursor_open = 0;
}

void FormQuery_a_bas( void )
{
    char *p;

        /* a_eig */
	    p = statement;	
		p += sprintf( p, "SELECT TARA FROM A_EIG WHERE A = ?" );
	    fprintf( fperr, "SQLCMD: %s\n", statement );
/*
 * 	   	EXEC SQL prepare p_a_eig_curs from :statement;
 */
#line 452 "gxsql.ec"
  {
#line 452 "gxsql.ec"
#line 452 "gxsql.ec"
#line 452 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn14, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 452 "gxsql.ec"
  }
	    exp_chk( statement, WARN );
/*
 * 	    EXEC SQL declare a_eig_curs cursor for p_a_eig_curs;
 */
#line 454 "gxsql.ec"
  {
#line 454 "gxsql.ec"
#line 454 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn15, 512), (char *) _Cn15, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn14, 513), 0, 0);
#line 454 "gxsql.ec"
  }
	    exp_chk( statement, WARN );
	
        /* a_hndw */
	    p = statement;	
		p += sprintf( p, "SELECT TARA FROM A_HNDW WHERE A = ?" );
        fprintf( fperr, "SQLCMD: %s\n", statement );
/*
 *      	EXEC SQL prepare p_a_hndw_curs from :statement;
 */
#line 461 "gxsql.ec"
  {
#line 461 "gxsql.ec"
#line 461 "gxsql.ec"
#line 461 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn16, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 461 "gxsql.ec"
  }
        exp_chk( statement, WARN );
/*
 *         EXEC SQL declare a_hndw_curs cursor for p_a_hndw_curs;
 */
#line 463 "gxsql.ec"
  {
#line 463 "gxsql.ec"
#line 463 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn17, 512), (char *) _Cn17, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn16, 513), 0, 0);
#line 463 "gxsql.ec"
  }
        exp_chk( statement, WARN );

  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     ||
  	     strcmp( anwender, "DFW" ) == 0
  	     ||
  	     strcmp( anwender, "KUEBLER" ) == 0
    )
    {
        /* a_bas */
   	    p = statement;
	    p += sprintf( p, "SELECT A_TYP,A_TYP2, HBK_ZTR, A_GEW, CHARG_HAND,\n" );
	    p += sprintf( p, "BRENNWERT,EIWEISS,KH,FETT,\n" );
	    p += sprintf( p, "GLG,KREBS,EI,FISCH,ERDNUSS,SOJA,MILCH,SCHAL,\n" );
        p += sprintf( p, "SELLERIE,SENFSAAT,SESAMSAMEN,SULFIT,WEICHTIER \n" );
        p += sprintf( p, "FROM A_BAS WHERE A = ?" );
	}
	else /* Bauerngut */
	{
        /* a_bas */
   	    p = statement;
	    p += sprintf( p, "SELECT A_TYP, HBK_ZTR, A_TARA, A_GEW, UP_KA, CHARG_HAND" );
	    p += sprintf( p, " FROM A_BAS WHERE A = ?" );
	}
	/* a_bas */
	fprintf( fperr, "SQLCMD: %s\n", statement );
/*
 * 	EXEC SQL prepare p_a_bas_curs from :statement;
 */
#line 492 "gxsql.ec"
  {
#line 492 "gxsql.ec"
#line 492 "gxsql.ec"
#line 492 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn18, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 492 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare a_bas_curs cursor for p_a_bas_curs;
 */
#line 494 "gxsql.ec"
  {
#line 494 "gxsql.ec"
#line 494 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn19, 512), (char *) _Cn19, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn18, 513), 0, 0);
#line 494 "gxsql.ec"
  }
	exp_chk( statement, WARN );
	
    /* a_lgr */
	p = statement;	
	p += sprintf( p, "SELECT INH_WANNE FROM A_LGR" );
	p += sprintf( p, " WHERE MDN=1 AND FIL=0 AND HAUPT_LGR=\"J\" AND A = ?" );
    fprintf( fperr, "SQLCMD: %s\n", statement );
/*
 *     EXEC SQL prepare p_a_lgr_curs from :statement;
 */
#line 502 "gxsql.ec"
  {
#line 502 "gxsql.ec"
#line 502 "gxsql.ec"
#line 502 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn20, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 502 "gxsql.ec"
  }
    exp_chk( statement, WARN );
/*
 *     EXEC SQL declare a_lgr_curs cursor for p_a_lgr_curs;
 */
#line 504 "gxsql.ec"
  {
#line 504 "gxsql.ec"
#line 504 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn21, 512), (char *) _Cn21, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn20, 513), 0, 0);
#line 504 "gxsql.ec"
  }
    exp_chk( statement, WARN );
}

void FormQuery_a_bas_erw( void )
{
    char *p;
   	    p = statement;
	    p += sprintf( p, "SELECT PP_A_BZ1,PP_A_BZ2, LGR_TMPR,LUPINE,SCHUTZGAS,\n" );
	    p += sprintf( p, "HUELLE,SALZ,DAVONFETT,DAVONZUCKER,BALLASTSTOFFE,ZUTAT \n" );
	    p += sprintf( p, "FROM A_BAS_ERW WHERE A = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );
/*
 * 	EXEC SQL prepare p_a_bas_erw_curs from :statement;
 */
#line 516 "gxsql.ec"
  {
#line 516 "gxsql.ec"
#line 516 "gxsql.ec"
#line 516 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn22, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 516 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare a_bas_erw_curs cursor for p_a_bas_erw_curs;
 */
#line 518 "gxsql.ec"
  {
#line 518 "gxsql.ec"
#line 518 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn23, 512), (char *) _Cn23, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn22, 513), 0, 0);
#line 518 "gxsql.ec"
  }
	exp_chk( statement, WARN );
	
}

void FormQuery_paz_ipr( void )
{
  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     ||
  	     strcmp( anwender, "DFW" ) == 0
  	     ||
  	     strcmp( anwender, "OKLE" ) == 0
  	     ||
  	     strcmp( anwender, "LOHMANN" ) == 0
  	     ||
  	     strcmp( anwender, "KUEBLER" ) == 0
  	)
    {
        return;
    }
	strcpy( statement,
	    "SELECT LD_PR FROM PAZ_IPR WHERE BRAN_KUN = ? AND A = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );
	
/*
 * 	EXEC SQL prepare p_paz_ipr_curs from :statement;
 */
#line 544 "gxsql.ec"
  {
#line 544 "gxsql.ec"
#line 544 "gxsql.ec"
#line 544 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn24, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 544 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare paz_ipr_curs cursor for p_paz_ipr_curs;
 */
#line 546 "gxsql.ec"
  {
#line 546 "gxsql.ec"
#line 546 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn25, 512), (char *) _Cn25, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn24, 513), 0, 0);
#line 546 "gxsql.ec"
  }
	exp_chk( statement, WARN );
}

void FormQuery_paz_cab( void )
{
    if ( strcmp( anwender, "Boesinger" ) == 0 )
    {
        return;
    }
	strcpy( statement,
	    "SELECT CAB FROM PAZ_CAB WHERE CABNUM = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );
	
/*
 * 	EXEC SQL prepare p_paz_cab_curs from :statement;
 */
#line 560 "gxsql.ec"
  {
#line 560 "gxsql.ec"
#line 560 "gxsql.ec"
#line 560 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn26, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 560 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare paz_cab_curs cursor for p_paz_cab_curs;
 */
#line 562 "gxsql.ec"
  {
#line 562 "gxsql.ec"
#line 562 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn27, 512), (char *) _Cn27, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn26, 513), 0, 0);
#line 562 "gxsql.ec"
  }
	exp_chk( statement, WARN );
}

void FormQuery_kun_geb( void )
{
	system_num = atoi(itmval("SYSTEM_NUM"));
	fprintf( fperr, "SYSTEM_NUM = %ld\n", system_num );
    if ( system_num < 1 )
    {
        return;
    }
	sprintf( statement,
	    "SELECT GEB_FILL, PAL_FILL, GEB_ANZ, PAL_ANZ, TARA FROM KUN_GEB WHERE SYS = %ld",
	     system_num );	
	fprintf( fperr, "SQLCMD: %s\n", statement );
	
/*
 * 	EXEC SQL prepare p_kun_geb_curs from :statement;
 */
#line 579 "gxsql.ec"
  {
#line 579 "gxsql.ec"
#line 579 "gxsql.ec"
#line 579 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn28, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 579 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare kun_geb_curs cursor for p_kun_geb_curs;
 */
#line 581 "gxsql.ec"
  {
#line 581 "gxsql.ec"
#line 581 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn29, 512), (char *) _Cn29, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn28, 513), 0, 0);
#line 581 "gxsql.ec"
  }
	exp_chk( statement, WARN );
}

void FormQuery_kun( void )
{
	strcpy( statement, "SELECT KUN_BRAN2,KUN_GR1,ADR1,STAT_KUN FROM KUN" );
	strcat( statement, " WHERE MDN = ? AND FIL = ? AND KUN = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

/*
 * 	EXEC SQL prepare p_kun_curs from :statement;
 */
#line 591 "gxsql.ec"
  {
#line 591 "gxsql.ec"
#line 591 "gxsql.ec"
#line 591 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn30, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 591 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare kun_curs cursor for p_kun_curs;
 */
#line 593 "gxsql.ec"
  {
#line 593 "gxsql.ec"
#line 593 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn31, 512), (char *) _Cn31, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn30, 513), 0, 0);
#line 593 "gxsql.ec"
  }
	exp_chk( statement, WARN );
	
	strcpy( statement, "SELECT ADR_NAM1, ADR_NAM2, STR, PLZ, ORT1 FROM ADR" );
	strcat( statement, " WHERE ADR = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

/*
 * 	EXEC SQL prepare p_adr_curs from :statement;
 */
#line 600 "gxsql.ec"
  {
#line 600 "gxsql.ec"
#line 600 "gxsql.ec"
#line 600 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn32, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 600 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare adr_curs cursor for p_adr_curs;
 */
#line 602 "gxsql.ec"
  {
#line 602 "gxsql.ec"
#line 602 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn33, 512), (char *) _Cn33, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn32, 513), 0, 0);
#line 602 "gxsql.ec"
  }
	exp_chk( statement, WARN );
}

void FormQuery_pr_ausz_gx( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT SG1, TXT1, " );
	p += sprintf( p, "SG2, TXT2, " );
	p += sprintf( p, "SG3, TXT3, " );
	p += sprintf( p, "SG4, TXT4, " );
	p += sprintf( p, "SG5, TXT5, " );
	p += sprintf( p, "SG6, TXT6, " );
	p += sprintf( p, "SG7, TXT7, " );
	p += sprintf( p, "SG8, TXT8, " );
	p += sprintf( p, "SG9, TXT9, " );
	p += sprintf( p, "SG10, TXT10, " );
	p += sprintf( p, "SG11, TXT11, " );
	p += sprintf( p, "SG12, TXT12 " );	
	p += sprintf( p, "FROM PR_AUSZ_GX WHERE TEXT_NR = ? AND ETI_TYP = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

/*
 * 	EXEC SQL prepare p_pr_ausz_gx_curs from :statement;
 */
#line 625 "gxsql.ec"
  {
#line 625 "gxsql.ec"
#line 625 "gxsql.ec"
#line 625 "gxsql.ec"
  sqli_prep(ESQLINTVERSION, (char *) _Cn34, statement,(ifx_literal_t *) 0, (ifx_namelist_t *) 0, -1, 0,0 ); 
#line 625 "gxsql.ec"
  }
	exp_chk( statement, WARN );
/*
 * 	EXEC SQL declare pr_ausz_gx_curs cursor for p_pr_ausz_gx_curs;
 */
#line 627 "gxsql.ec"
  {
#line 627 "gxsql.ec"
#line 627 "gxsql.ec"
  sqli_curs_decl_dynm(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn35, 512), (char *) _Cn35, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn34, 513), 0, 0);
#line 627 "gxsql.ec"
  }
	exp_chk( statement, WARN );
}

char *allergene( void )
{
   char *p;
   p = anytxt;

   fprintf( fperr, "12345 67890 1234\n" );
   fprintf( fperr, "%s%s%s%s%s %s%s%s%s%s %s%s%s%s\n",
       data.glg,
       data.krebs,
       data.ei,
       data.fisch,
       data.erdnuss,
       data.soja,
       data.milch,
       data.schal,
       data.sellerie,
       data.senfsaat,
       data.sesamsamen,
       data.sulfit,
       data.weichtier,
       a_bas_erw.lupine );

   p += sprintf( p, "Enthält " );
   if ( data.glg[0] != 'J'
        && data.krebs[0] != 'J'
        && data.ei[0] != 'J'
        && data.fisch[0] != 'J'
        && data.erdnuss[0] != 'J'
        && data.soja[0] != 'J'
        && data.milch[0] != 'J'
        && data.schal[0] != 'J'
        && data.sellerie[0] != 'J'
        && data.senfsaat[0] != 'J'
        && data.sesamsamen[0] != 'J'
        && data.sulfit[0] != 'J'
        && data.weichtier[0] != 'J'
        && a_bas_erw.lupine[0] != 'J'
   )
   {
          p += sprintf( p, " keine allergene Stoffe." );
          return anytxt;
   }
   if ( data.glg[0]         == 'J' ) p += sprintf( p, "Glutene. " );
   if ( data.krebs[0]       == 'J' ) p += sprintf( p, "Krebse. " );
   if ( data.ei[0]          == 'J' ) p += sprintf( p, "Eier. " );
   if ( data.fisch[0]       == 'J' ) p += sprintf( p, "Fisch. " );
   if ( data.erdnuss[0]     == 'J' ) p += sprintf( p, "Erdnuss. " );
   if ( data.soja[0]        == 'J' ) p += sprintf( p, "Soja. " );
   if ( data.milch[0]       == 'J' ) p += sprintf( p, "Milch. " );
   if ( data.schal[0]       == 'J' ) p += sprintf( p, "Schalenfrüchte. " );
   if ( data.sellerie[0]    == 'J' ) p += sprintf( p, "Sellerie. " );
   if ( data.senfsaat[0]    == 'J' ) p += sprintf( p, "Senfsaat. " );
   if ( data.sesamsamen[0]  == 'J' ) p += sprintf( p, "Sesamsamen. " );
   if ( data.sulfit[0]      == 'J' ) p += sprintf( p, "Sulfit. " );
   if ( data.weichtier[0]   == 'J' ) p += sprintf( p, "Weichtiere. " );
   if ( a_bas_erw.lupine[0] == 'J' ) p += sprintf( p, "Lupine. " );
   return anytxt;
}

void trim( char *fieldp )
{
    size_t len;
    char * cp;
	len = strlen(fieldp);
	cp = fieldp + len - 1;
	while ( len > 1 && *cp == ' ' )
	{
		*cp = '\0';
		len--, cp--;
	}
}

void substitutechar( char *s, char a, char b )
{
    char *p;
    p = strchr( s, a );
    if ( p != NULL ) *p = b;
}

void init_a_kun_gx( void )
{
	 a_kun_gx.a_kun[0] = '\0';
	 a_kun_gx.a_bz1[0] = '\0';
	 a_kun_gx.me_einh_kun = 0;
	 a_kun_gx.inh = 0;
	 a_kun_gx.tara = 0;
	 a_kun_gx.ean = 0;
	 a_kun_gx.a_bz2[0] = '\0';
	 a_kun_gx.hbk_ztr = 0;
	 a_kun_gx.kopf_text = 0;
	 a_kun_gx.pr_rech_kz[0] = '\0';
	 a_kun_gx.modif[0] = '\0';
	 a_kun_gx.text_nr = 0;
	 a_kun_gx.devise = 0;
	 a_kun_gx.geb_eti[0] = '\0';
	 a_kun_gx.geb_fill = 0;
	 a_kun_gx.geb_anz = 0;
	 a_kun_gx.pal_eti[0] = '\0';
	 a_kun_gx.pal_fill = 0;
	 a_kun_gx.pal_anz = 0;
	 a_kun_gx.pos_eti[0] = '\0';
	 a_kun_gx.sg1 = 0;
	 a_kun_gx.sg2 = 0;
	 a_kun_gx.pos_fill = 0;
	 a_kun_gx.ausz_art = 0;
	 a_kun_gx.text_nr2 = 0;
	 a_kun_gx.cab = 0;
	 a_kun_gx.a_bz3[0] = '\0';
	 a_kun_gx.a_bz4[0] = '\0';
	 a_kun_gx.eti_typ = 0;
	 a_kun_gx.mhd_text = 0;
	 a_kun_gx.freitext1 = 0;
	 a_kun_gx.freitext2 = 0;
	 a_kun_gx.freitext3 = 0;
	 a_kun_gx.sg3 = 0;
	 a_kun_gx.eti_sum1 = 0;
	 a_kun_gx.eti_sum2 = 0;
	 a_kun_gx.eti_sum3 = 0;
	 a_kun_gx.eti_nve1 = 0;
	 a_kun_gx.eti_nve2 = 0;
	 a_kun_gx.ampar = 0;
	 a_kun_gx.sonder_eti = 0;
	 a_kun_gx.text_nr_a = 0;
	 a_kun_gx.ean1 = 0;
	 a_kun_gx.cab1 = 0;
	 a_kun_gx.ean2 = 0;
	 a_kun_gx.cab2 = 0;
	 a_kun_gx.ean3 = 0;
	 a_kun_gx.cab3 = 0;
	 a_kun_gx.cab_nve1 = 0;
	 a_kun_gx.cab_nve2 = 0;
	 a_kun_gx.gwpar = 0;
	 a_kun_gx.vppar = 0;
	 a_kun_gx.devise2 = 0;
}

int fetch_a_kun_gx_kb( void )
{   	
	if ( a_kun_gx_kb_cursor_open == 0 )
	{
/*
 * 	    EXEC SQL open a_kun_gx_kb_curs using $a_kun_gx.kun, $a_kun_gx.kun_bran2;
 */
#line 771 "gxsql.ec"
  {
#line 771 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 103, sizeof(a_kun_gx.kun), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 771 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 2, _sqibind, {0}, 2, 0 };
#line 771 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &a_kun_gx.kun;
#line 771 "gxsql.ec"
  _sqibind[1].sqldata = a_kun_gx.kun_bran2;
#line 771 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn13, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 771 "gxsql.ec"
  }
	    if ( SQLCODE != 0 )
	    {
		    FatalError( "open a_kun_gx_kb_curs: SQLCODE=%ld",  SQLCODE );
            return 0;
	    }
	}
    a_kun_gx_kb_cursor_open = 1;

/*
 *    	EXEC SQL fetch a_kun_gx_kb_curs into
 * 	    $a_kun_gx.a,
 * 	    $a_kun_gx.ean,
 *         $a_kun_gx.kopf_text,
 * 	    $a_kun_gx.pr_rech_kz,
 * 	    $a_kun_gx.text_nr,
 * 	    $a_kun_gx.sg1,
 * 	    $a_kun_gx.sg2,
 * 	    $a_kun_gx.ausz_art,
 * 	    $a_kun_gx.cab,
 * 	    $a_kun_gx.a_bz3,
 * 	    $a_kun_gx.a_bz4,
 * 	    $a_kun_gx.eti_typ,
 * 	    $a_kun_gx.mhd_text,
 * 	    $a_kun_gx.eti_sum1,
 * 	    $a_kun_gx.eti_sum2,
 * 	    $a_kun_gx.ampar,
 * 	    $a_kun_gx.sonder_eti,
 * 	    $a_kun_gx.ean1,
 * 	    $a_kun_gx.cab1,
 * 	    $a_kun_gx.gwpar,
 * 	    $a_kun_gx.vppar,
 * 	    $a_kun_gx.tara;
 */
#line 780 "gxsql.ec"
  {
#line 802 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 105, sizeof(a_kun_gx.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.ean), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.kopf_text), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.text_nr), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sg1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sg2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.ausz_art), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_typ), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.mhd_text), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_sum1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_sum2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.ampar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sonder_eti), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.ean1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.gwpar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.vppar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.tara), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 802 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 22, _sqobind, {0}, 22, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 802 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &a_kun_gx.a;
#line 802 "gxsql.ec"
  _sqobind[1].sqldata = (char *) &a_kun_gx.ean;
#line 802 "gxsql.ec"
  _sqobind[2].sqldata = (char *) &a_kun_gx.kopf_text;
#line 802 "gxsql.ec"
  _sqobind[3].sqldata = a_kun_gx.pr_rech_kz;
#line 802 "gxsql.ec"
  _sqobind[4].sqldata = (char *) &a_kun_gx.text_nr;
#line 802 "gxsql.ec"
  _sqobind[5].sqldata = (char *) &a_kun_gx.sg1;
#line 802 "gxsql.ec"
  _sqobind[6].sqldata = (char *) &a_kun_gx.sg2;
#line 802 "gxsql.ec"
  _sqobind[7].sqldata = (char *) &a_kun_gx.ausz_art;
#line 802 "gxsql.ec"
  _sqobind[8].sqldata = (char *) &a_kun_gx.cab;
#line 802 "gxsql.ec"
  _sqobind[9].sqldata = a_kun_gx.a_bz3;
#line 802 "gxsql.ec"
  _sqobind[10].sqldata = a_kun_gx.a_bz4;
#line 802 "gxsql.ec"
  _sqobind[11].sqldata = (char *) &a_kun_gx.eti_typ;
#line 802 "gxsql.ec"
  _sqobind[12].sqldata = (char *) &a_kun_gx.mhd_text;
#line 802 "gxsql.ec"
  _sqobind[13].sqldata = (char *) &a_kun_gx.eti_sum1;
#line 802 "gxsql.ec"
  _sqobind[14].sqldata = (char *) &a_kun_gx.eti_sum2;
#line 802 "gxsql.ec"
  _sqobind[15].sqldata = (char *) &a_kun_gx.ampar;
#line 802 "gxsql.ec"
  _sqobind[16].sqldata = (char *) &a_kun_gx.sonder_eti;
#line 802 "gxsql.ec"
  _sqobind[17].sqldata = (char *) &a_kun_gx.ean1;
#line 802 "gxsql.ec"
  _sqobind[18].sqldata = (char *) &a_kun_gx.cab1;
#line 802 "gxsql.ec"
  _sqobind[19].sqldata = (char *) &a_kun_gx.gwpar;
#line 802 "gxsql.ec"
  _sqobind[20].sqldata = (char *) &a_kun_gx.vppar;
#line 802 "gxsql.ec"
  _sqobind[21].sqldata = (char *) &a_kun_gx.tara;
#line 802 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn13, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 802 "gxsql.ec"
  }
	
	ggdat.plunr = (long) a_kun_gx.a;

	fprintf( fperr, "K : %8ld  A : %8ld  SQLCODE: %4ld\n", a_kun_gx.kun, ggdat.plunr, SQLCODE );

  	if ( SQLCODE != 0L )
	{
/*
 *         EXEC SQL close a_kun_gx_kb_curs;
 */
#line 810 "gxsql.ec"
  {
#line 810 "gxsql.ec"
#line 810 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn13, 768));
#line 810 "gxsql.ec"
  }
		a_kun_gx_kb_cursor_open = 0;
		return 0;
	}
	
	trim( a_kun_gx.a_bz3 );
	trim( a_kun_gx.a_bz4 );

	return 1;
}	

int fetch_pazlsp( void )
{
  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     ||
  	     strcmp( anwender, "DFW" ) == 0
  	     ||
  	     strcmp( anwender, "OKLE" ) == 0
  	     ||
  	     strcmp( anwender, "LOHMANN" ) == 0
  	     ||
  	     strcmp( anwender, "KUEBLER" ) == 0
  	)
    {
        return 0;
    }
	if ( pazlsp_cursor_open == 0 )
	{
/*
 * 	    EXEC SQL open pazlsp_curs using $pazlsp.mdn, $pazlsp.ls;
 */
#line 840 "gxsql.ec"
  {
#line 840 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 102, sizeof(pazlsp.mdn), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(pazlsp.ls), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 840 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 2, _sqibind, {0}, 2, 0 };
#line 840 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &pazlsp.mdn;
#line 840 "gxsql.ec"
  _sqibind[1].sqldata = (char *) &pazlsp.ls;
#line 840 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn11, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 840 "gxsql.ec"
  }
	    if ( SQLCODE != 0 )
	    {
		    FatalError( "open pazlsp_curs: SQLCODE=%ld",  SQLCODE );
            return 0;
	    }
	}
    pazlsp_cursor_open = 1;

/*
 *    	EXEC SQL fetch pazlsp_curs into
 * 	        $pazlsp.a,
 *             $pazlsp.kun,
 *     	    $pazlsp.kun_bran2,
 * 	        $pazlsp.ls_lad_euro;
 */
#line 849 "gxsql.ec"
  {
#line 853 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 105, sizeof(pazlsp.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(pazlsp.kun), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(pazlsp.ls_lad_euro), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 853 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 4, _sqobind, {0}, 4, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 853 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &pazlsp.a;
#line 853 "gxsql.ec"
  _sqobind[1].sqldata = (char *) &pazlsp.kun;
#line 853 "gxsql.ec"
  _sqobind[2].sqldata = pazlsp.kun_bran2;
#line 853 "gxsql.ec"
  _sqobind[3].sqldata = (char *) &pazlsp.ls_lad_euro;
#line 853 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn11, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 853 "gxsql.ec"
  }

	ggdat.plunr = (long) pazlsp.a;
    fprintf( fperr, "LS: %8ld  A: %8ld  SQLCODE: %4ld\n", pazlsp.ls, ggdat.plunr, SQLCODE );	
	
  	if ( SQLCODE != 0L )
	{
/*
 *         EXEC SQL close pazlsp_curs;
 */
#line 860 "gxsql.ec"
  {
#line 860 "gxsql.ec"
#line 860 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn11, 768));
#line 860 "gxsql.ec"
  }
		pazlsp_cursor_open = 0;
		return 0;
	}
	return 1;
}	

long Query_a_kun( void )
{
	long sqlcode;
/*
 * 	EXEC SQL open a_kun_curs using $a_kun.a, $a_kun.kun;
 */
#line 870 "gxsql.ec"
  {
#line 870 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 105, sizeof(a_kun.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun.kun), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 870 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 2, _sqibind, {0}, 2, 0 };
#line 870 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &a_kun.a;
#line 870 "gxsql.ec"
  _sqibind[1].sqldata = (char *) &a_kun.kun;
#line 870 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn3, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 870 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open a_kun_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
/*
 * 	EXEC SQL fetch a_kun_curs into
 * 	    $a_kun.a_kun;
 */
#line 876 "gxsql.ec"
  {
#line 877 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 100, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 877 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqobind, {0}, 1, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 877 "gxsql.ec"
  _sqobind[0].sqldata = a_kun.a_kun;
#line 877 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn3, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 877 "gxsql.ec"
  }
	sqlcode = SQLCODE;
/*
 *     EXEC SQL close a_kun_curs;
 */
#line 879 "gxsql.ec"
  {
#line 879 "gxsql.ec"
#line 879 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn3, 768));
#line 879 "gxsql.ec"
  }
	if ( sqlcode != 0L )
	{
		TimeMsgLine( "K : %8ld  A : %8ld  SQLCODE: %4ld", a_kun.kun, (long) a_kun.a, sqlcode );
		return 0;
	}
	trim( a_kun.a_kun );
	return 1;
}

long Query_a_kun_gx_akb( void )
{
	long sqlcode;
/*
 * 	EXEC SQL open a_kun_gx_akb_curs using $a_kun_gx.a, $a_kun_gx.kun, $a_kun_gx.kun_bran2;
 */
#line 892 "gxsql.ec"
  {
#line 892 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 105, sizeof(a_kun_gx.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.kun), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 892 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 3, _sqibind, {0}, 3, 0 };
#line 892 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &a_kun_gx.a;
#line 892 "gxsql.ec"
  _sqibind[1].sqldata = (char *) &a_kun_gx.kun;
#line 892 "gxsql.ec"
  _sqibind[2].sqldata = a_kun_gx.kun_bran2;
#line 892 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn5, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 892 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open a_kun_gx_akb_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
/*
 * 	EXEC SQL fetch a_kun_gx_akb_curs into
 * 	    $a_kun_gx.ean,
 *         $a_kun_gx.kopf_text,
 * 	    $a_kun_gx.pr_rech_kz,
 * 	    $a_kun_gx.text_nr,
 * 	    $a_kun_gx.sg1,
 * 	    $a_kun_gx.sg2,
 * 	    $a_kun_gx.ausz_art,
 * 	    $a_kun_gx.cab,
 * 	    $a_kun_gx.a_bz3,
 * 	    $a_kun_gx.a_bz4,
 * 	    $a_kun_gx.eti_typ,
 * 	    $a_kun_gx.mhd_text,
 * 	    $a_kun_gx.eti_sum1,
 * 	    $a_kun_gx.eti_sum2,
 * 	    $a_kun_gx.ampar,
 * 	    $a_kun_gx.sonder_eti,
 * 	    $a_kun_gx.ean1,
 * 	    $a_kun_gx.cab1,
 * 	    $a_kun_gx.gwpar,
 * 	    $a_kun_gx.vppar,
 * 	    $a_kun_gx.tara;
 */
#line 898 "gxsql.ec"
  {
#line 919 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 105, sizeof(a_kun_gx.ean), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.kopf_text), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.text_nr), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sg1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sg2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.ausz_art), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_typ), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.mhd_text), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_sum1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_sum2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.ampar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sonder_eti), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.ean1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.gwpar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.vppar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.tara), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 919 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 21, _sqobind, {0}, 21, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 919 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &a_kun_gx.ean;
#line 919 "gxsql.ec"
  _sqobind[1].sqldata = (char *) &a_kun_gx.kopf_text;
#line 919 "gxsql.ec"
  _sqobind[2].sqldata = a_kun_gx.pr_rech_kz;
#line 919 "gxsql.ec"
  _sqobind[3].sqldata = (char *) &a_kun_gx.text_nr;
#line 919 "gxsql.ec"
  _sqobind[4].sqldata = (char *) &a_kun_gx.sg1;
#line 919 "gxsql.ec"
  _sqobind[5].sqldata = (char *) &a_kun_gx.sg2;
#line 919 "gxsql.ec"
  _sqobind[6].sqldata = (char *) &a_kun_gx.ausz_art;
#line 919 "gxsql.ec"
  _sqobind[7].sqldata = (char *) &a_kun_gx.cab;
#line 919 "gxsql.ec"
  _sqobind[8].sqldata = a_kun_gx.a_bz3;
#line 919 "gxsql.ec"
  _sqobind[9].sqldata = a_kun_gx.a_bz4;
#line 919 "gxsql.ec"
  _sqobind[10].sqldata = (char *) &a_kun_gx.eti_typ;
#line 919 "gxsql.ec"
  _sqobind[11].sqldata = (char *) &a_kun_gx.mhd_text;
#line 919 "gxsql.ec"
  _sqobind[12].sqldata = (char *) &a_kun_gx.eti_sum1;
#line 919 "gxsql.ec"
  _sqobind[13].sqldata = (char *) &a_kun_gx.eti_sum2;
#line 919 "gxsql.ec"
  _sqobind[14].sqldata = (char *) &a_kun_gx.ampar;
#line 919 "gxsql.ec"
  _sqobind[15].sqldata = (char *) &a_kun_gx.sonder_eti;
#line 919 "gxsql.ec"
  _sqobind[16].sqldata = (char *) &a_kun_gx.ean1;
#line 919 "gxsql.ec"
  _sqobind[17].sqldata = (char *) &a_kun_gx.cab1;
#line 919 "gxsql.ec"
  _sqobind[18].sqldata = (char *) &a_kun_gx.gwpar;
#line 919 "gxsql.ec"
  _sqobind[19].sqldata = (char *) &a_kun_gx.vppar;
#line 919 "gxsql.ec"
  _sqobind[20].sqldata = (char *) &a_kun_gx.tara;
#line 919 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn5, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 919 "gxsql.ec"
  }
	sqlcode = SQLCODE;
/*
 *     EXEC SQL close a_kun_gx_akb_curs;
 */
#line 921 "gxsql.ec"
  {
#line 921 "gxsql.ec"
#line 921 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn5, 768));
#line 921 "gxsql.ec"
  }
	if ( sqlcode != 0L )
	{
	    FatalError( "K : %8ld  B : %8s  SQLCODE: %4ld", a_kun_gx.kun, a_kun_gx.kun_bran2, sqlcode );
	}
	trim( a_kun_gx.a_bz3 );
	trim( a_kun_gx.a_bz4 );
	return 1;
}

long Query_a_kun_gx_ak( void )
{
	long sqlcode;
	a_kun_gx.mdn = pazmdn;
/*
 * 	EXEC SQL open a_kun_gx_ak_curs using $a_kun_gx.mdn, $a_kun_gx.a, $a_kun_gx.kun;
 */
#line 935 "gxsql.ec"
  {
#line 935 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 102, sizeof(a_kun_gx.mdn), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.kun), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 935 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 3, _sqibind, {0}, 3, 0 };
#line 935 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &a_kun_gx.mdn;
#line 935 "gxsql.ec"
  _sqibind[1].sqldata = (char *) &a_kun_gx.a;
#line 935 "gxsql.ec"
  _sqibind[2].sqldata = (char *) &a_kun_gx.kun;
#line 935 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn7, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 935 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open a_kun_gx_ak_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
/*
 * 	EXEC SQL fetch a_kun_gx_ak_curs into
 * 	    $a_kun_gx.ean,
 *         $a_kun_gx.kopf_text,
 * 	    $a_kun_gx.pr_rech_kz,
 * 	    $a_kun_gx.text_nr,
 * 	    $a_kun_gx.sg1,
 * 	    $a_kun_gx.sg2,
 * 	    $a_kun_gx.ausz_art,
 * 	    $a_kun_gx.cab,
 * 	    $a_kun_gx.a_bz3,
 * 	    $a_kun_gx.a_bz4,
 * 	    $a_kun_gx.eti_typ,
 * 	    $a_kun_gx.mhd_text,
 * 	    $a_kun_gx.hbk_ztr,
 * 	    $a_kun_gx.eti_sum1,
 * 	    $a_kun_gx.eti_sum2,
 * 	    $a_kun_gx.ampar,
 * 	    $a_kun_gx.sonder_eti,
 * 	    $a_kun_gx.ean1,
 * 	    $a_kun_gx.cab1,
 * 	    $a_kun_gx.gwpar,
 * 	    $a_kun_gx.vppar,
 * 	    $a_kun_gx.tara,
 * 	    $a_kun_gx.inh,
 * 	    $a_kun_gx.devise,
 * 	    $a_kun_gx.pr_rech_kz,
 * 	    $a_kun_gx.eti_sum3,
 * 	    $a_kun_gx.eti_nve1,
 * 	    $a_kun_gx.eti_nve2,
 * 	    $a_kun_gx.cab_nve1,
 * 	    $a_kun_gx.cab_nve2,
 * 	    $a_kun_gx.text_nr2,
 * 	    $a_kun_gx.geb_anz,
 * 	    $a_kun_gx.pal_anz,
 * 	    $a_kun_gx.geb_fill,
 * 	    $a_kun_gx.pal_fill,
 * 	    $a_kun_gx.geb_eti,
 * 	    $a_kun_gx.freitext1;
 */
#line 941 "gxsql.ec"
  {
#line 978 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 105, sizeof(a_kun_gx.ean), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.kopf_text), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.text_nr), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sg1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sg2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.ausz_art), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_typ), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.mhd_text), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.hbk_ztr), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_sum1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_sum2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.ampar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sonder_eti), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.ean1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.gwpar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.vppar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.tara), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.inh), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.devise), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_sum3), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_nve1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_nve2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab_nve1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab_nve2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.text_nr2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.geb_anz), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.pal_anz), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.geb_fill), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.pal_fill), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.freitext1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 978 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 37, _sqobind, {0}, 37, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 978 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &a_kun_gx.ean;
#line 978 "gxsql.ec"
  _sqobind[1].sqldata = (char *) &a_kun_gx.kopf_text;
#line 978 "gxsql.ec"
  _sqobind[2].sqldata = a_kun_gx.pr_rech_kz;
#line 978 "gxsql.ec"
  _sqobind[3].sqldata = (char *) &a_kun_gx.text_nr;
#line 978 "gxsql.ec"
  _sqobind[4].sqldata = (char *) &a_kun_gx.sg1;
#line 978 "gxsql.ec"
  _sqobind[5].sqldata = (char *) &a_kun_gx.sg2;
#line 978 "gxsql.ec"
  _sqobind[6].sqldata = (char *) &a_kun_gx.ausz_art;
#line 978 "gxsql.ec"
  _sqobind[7].sqldata = (char *) &a_kun_gx.cab;
#line 978 "gxsql.ec"
  _sqobind[8].sqldata = a_kun_gx.a_bz3;
#line 978 "gxsql.ec"
  _sqobind[9].sqldata = a_kun_gx.a_bz4;
#line 978 "gxsql.ec"
  _sqobind[10].sqldata = (char *) &a_kun_gx.eti_typ;
#line 978 "gxsql.ec"
  _sqobind[11].sqldata = (char *) &a_kun_gx.mhd_text;
#line 978 "gxsql.ec"
  _sqobind[12].sqldata = (char *) &a_kun_gx.hbk_ztr;
#line 978 "gxsql.ec"
  _sqobind[13].sqldata = (char *) &a_kun_gx.eti_sum1;
#line 978 "gxsql.ec"
  _sqobind[14].sqldata = (char *) &a_kun_gx.eti_sum2;
#line 978 "gxsql.ec"
  _sqobind[15].sqldata = (char *) &a_kun_gx.ampar;
#line 978 "gxsql.ec"
  _sqobind[16].sqldata = (char *) &a_kun_gx.sonder_eti;
#line 978 "gxsql.ec"
  _sqobind[17].sqldata = (char *) &a_kun_gx.ean1;
#line 978 "gxsql.ec"
  _sqobind[18].sqldata = (char *) &a_kun_gx.cab1;
#line 978 "gxsql.ec"
  _sqobind[19].sqldata = (char *) &a_kun_gx.gwpar;
#line 978 "gxsql.ec"
  _sqobind[20].sqldata = (char *) &a_kun_gx.vppar;
#line 978 "gxsql.ec"
  _sqobind[21].sqldata = (char *) &a_kun_gx.tara;
#line 978 "gxsql.ec"
  _sqobind[22].sqldata = (char *) &a_kun_gx.inh;
#line 978 "gxsql.ec"
  _sqobind[23].sqldata = (char *) &a_kun_gx.devise;
#line 978 "gxsql.ec"
  _sqobind[24].sqldata = a_kun_gx.pr_rech_kz;
#line 978 "gxsql.ec"
  _sqobind[25].sqldata = (char *) &a_kun_gx.eti_sum3;
#line 978 "gxsql.ec"
  _sqobind[26].sqldata = (char *) &a_kun_gx.eti_nve1;
#line 978 "gxsql.ec"
  _sqobind[27].sqldata = (char *) &a_kun_gx.eti_nve2;
#line 978 "gxsql.ec"
  _sqobind[28].sqldata = (char *) &a_kun_gx.cab_nve1;
#line 978 "gxsql.ec"
  _sqobind[29].sqldata = (char *) &a_kun_gx.cab_nve2;
#line 978 "gxsql.ec"
  _sqobind[30].sqldata = (char *) &a_kun_gx.text_nr2;
#line 978 "gxsql.ec"
  _sqobind[31].sqldata = (char *) &a_kun_gx.geb_anz;
#line 978 "gxsql.ec"
  _sqobind[32].sqldata = (char *) &a_kun_gx.pal_anz;
#line 978 "gxsql.ec"
  _sqobind[33].sqldata = (char *) &a_kun_gx.geb_fill;
#line 978 "gxsql.ec"
  _sqobind[34].sqldata = (char *) &a_kun_gx.pal_fill;
#line 978 "gxsql.ec"
  _sqobind[35].sqldata = a_kun_gx.geb_eti;
#line 978 "gxsql.ec"
  _sqobind[36].sqldata = (char *) &a_kun_gx.freitext1;
#line 978 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn7, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 978 "gxsql.ec"
  }
	sqlcode = SQLCODE;
/*
 *     EXEC SQL close a_kun_gx_ak_curs;
 */
#line 980 "gxsql.ec"
  {
#line 980 "gxsql.ec"
#line 980 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn7, 768));
#line 980 "gxsql.ec"
  }
	if ( sqlcode != 0L )
	{
		TimeMsgLine( "K : %8ld  A : %8ld  SQLCODE: %4ld", a_kun_gx.kun, (long) a_kun_gx.a, sqlcode );
		return 0;
	}
	trim( a_kun_gx.a_bz3 );
	trim( a_kun_gx.a_bz4 );
	return 1;
}

long Query_a_kun_gx_ab( void )
{
	long sqlcode;
	a_kun_gx.mdn = pazmdn;
/*
 * 	EXEC SQL open a_kun_gx_ab_curs using $a_kun_gx.mdn, $a_kun_gx.a, $a_kun_gx.kun_bran2;
 */
#line 995 "gxsql.ec"
  {
#line 995 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 102, sizeof(a_kun_gx.mdn), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 995 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 3, _sqibind, {0}, 3, 0 };
#line 995 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &a_kun_gx.mdn;
#line 995 "gxsql.ec"
  _sqibind[1].sqldata = (char *) &a_kun_gx.a;
#line 995 "gxsql.ec"
  _sqibind[2].sqldata = a_kun_gx.kun_bran2;
#line 995 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn9, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 995 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open a_kun_gx_ab_curs: SQLCODE=%ld",  SQLCODE );
        return 0;
	}
/*
 * 	EXEC SQL fetch a_kun_gx_ab_curs into
 * 	    $a_kun_gx.ean,
 *         $a_kun_gx.kopf_text,
 * 	    $a_kun_gx.pr_rech_kz,
 * 	    $a_kun_gx.text_nr,
 * 	    $a_kun_gx.sg1,
 * 	    $a_kun_gx.sg2,
 * 	    $a_kun_gx.ausz_art,
 * 	    $a_kun_gx.cab,
 * 	    $a_kun_gx.a_bz3,
 * 	    $a_kun_gx.a_bz4,
 * 	    $a_kun_gx.eti_typ,
 * 	    $a_kun_gx.mhd_text,
 * 	    $a_kun_gx.hbk_ztr,
 * 	    $a_kun_gx.eti_sum1,
 * 	    $a_kun_gx.eti_sum2,
 * 	    $a_kun_gx.ampar,
 * 	    $a_kun_gx.sonder_eti,
 * 	    $a_kun_gx.ean1,
 * 	    $a_kun_gx.cab1,
 * 	    $a_kun_gx.gwpar,
 * 	    $a_kun_gx.vppar,
 * 	    $a_kun_gx.tara,
 * 	    $a_kun_gx.inh,
 * 	    $a_kun_gx.devise,
 * 	    $a_kun_gx.pr_rech_kz,
 * 	    $a_kun_gx.eti_sum3,
 * 	    $a_kun_gx.eti_nve1,
 * 	    $a_kun_gx.eti_nve2,
 * 	    $a_kun_gx.cab_nve1,
 * 	    $a_kun_gx.cab_nve2,
 * 	    $a_kun_gx.text_nr2,
 * 	    $a_kun_gx.geb_anz,
 * 	    $a_kun_gx.pal_anz,
 * 	    $a_kun_gx.geb_fill,
 * 	    $a_kun_gx.pal_fill,
 * 	    $a_kun_gx.geb_eti,
 * 	    $a_kun_gx.freitext1;
 */
#line 1001 "gxsql.ec"
  {
#line 1038 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 105, sizeof(a_kun_gx.ean), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.kopf_text), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.text_nr), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sg1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sg2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.ausz_art), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_typ), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.mhd_text), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.hbk_ztr), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_sum1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_sum2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.ampar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.sonder_eti), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.ean1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.gwpar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.vppar), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.tara), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_kun_gx.inh), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.devise), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_sum3), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_nve1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.eti_nve2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab_nve1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.cab_nve2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.text_nr2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.geb_anz), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.pal_anz), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.geb_fill), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(a_kun_gx.pal_fill), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(a_kun_gx.freitext1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1038 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 37, _sqobind, {0}, 37, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1038 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &a_kun_gx.ean;
#line 1038 "gxsql.ec"
  _sqobind[1].sqldata = (char *) &a_kun_gx.kopf_text;
#line 1038 "gxsql.ec"
  _sqobind[2].sqldata = a_kun_gx.pr_rech_kz;
#line 1038 "gxsql.ec"
  _sqobind[3].sqldata = (char *) &a_kun_gx.text_nr;
#line 1038 "gxsql.ec"
  _sqobind[4].sqldata = (char *) &a_kun_gx.sg1;
#line 1038 "gxsql.ec"
  _sqobind[5].sqldata = (char *) &a_kun_gx.sg2;
#line 1038 "gxsql.ec"
  _sqobind[6].sqldata = (char *) &a_kun_gx.ausz_art;
#line 1038 "gxsql.ec"
  _sqobind[7].sqldata = (char *) &a_kun_gx.cab;
#line 1038 "gxsql.ec"
  _sqobind[8].sqldata = a_kun_gx.a_bz3;
#line 1038 "gxsql.ec"
  _sqobind[9].sqldata = a_kun_gx.a_bz4;
#line 1038 "gxsql.ec"
  _sqobind[10].sqldata = (char *) &a_kun_gx.eti_typ;
#line 1038 "gxsql.ec"
  _sqobind[11].sqldata = (char *) &a_kun_gx.mhd_text;
#line 1038 "gxsql.ec"
  _sqobind[12].sqldata = (char *) &a_kun_gx.hbk_ztr;
#line 1038 "gxsql.ec"
  _sqobind[13].sqldata = (char *) &a_kun_gx.eti_sum1;
#line 1038 "gxsql.ec"
  _sqobind[14].sqldata = (char *) &a_kun_gx.eti_sum2;
#line 1038 "gxsql.ec"
  _sqobind[15].sqldata = (char *) &a_kun_gx.ampar;
#line 1038 "gxsql.ec"
  _sqobind[16].sqldata = (char *) &a_kun_gx.sonder_eti;
#line 1038 "gxsql.ec"
  _sqobind[17].sqldata = (char *) &a_kun_gx.ean1;
#line 1038 "gxsql.ec"
  _sqobind[18].sqldata = (char *) &a_kun_gx.cab1;
#line 1038 "gxsql.ec"
  _sqobind[19].sqldata = (char *) &a_kun_gx.gwpar;
#line 1038 "gxsql.ec"
  _sqobind[20].sqldata = (char *) &a_kun_gx.vppar;
#line 1038 "gxsql.ec"
  _sqobind[21].sqldata = (char *) &a_kun_gx.tara;
#line 1038 "gxsql.ec"
  _sqobind[22].sqldata = (char *) &a_kun_gx.inh;
#line 1038 "gxsql.ec"
  _sqobind[23].sqldata = (char *) &a_kun_gx.devise;
#line 1038 "gxsql.ec"
  _sqobind[24].sqldata = a_kun_gx.pr_rech_kz;
#line 1038 "gxsql.ec"
  _sqobind[25].sqldata = (char *) &a_kun_gx.eti_sum3;
#line 1038 "gxsql.ec"
  _sqobind[26].sqldata = (char *) &a_kun_gx.eti_nve1;
#line 1038 "gxsql.ec"
  _sqobind[27].sqldata = (char *) &a_kun_gx.eti_nve2;
#line 1038 "gxsql.ec"
  _sqobind[28].sqldata = (char *) &a_kun_gx.cab_nve1;
#line 1038 "gxsql.ec"
  _sqobind[29].sqldata = (char *) &a_kun_gx.cab_nve2;
#line 1038 "gxsql.ec"
  _sqobind[30].sqldata = (char *) &a_kun_gx.text_nr2;
#line 1038 "gxsql.ec"
  _sqobind[31].sqldata = (char *) &a_kun_gx.geb_anz;
#line 1038 "gxsql.ec"
  _sqobind[32].sqldata = (char *) &a_kun_gx.pal_anz;
#line 1038 "gxsql.ec"
  _sqobind[33].sqldata = (char *) &a_kun_gx.geb_fill;
#line 1038 "gxsql.ec"
  _sqobind[34].sqldata = (char *) &a_kun_gx.pal_fill;
#line 1038 "gxsql.ec"
  _sqobind[35].sqldata = a_kun_gx.geb_eti;
#line 1038 "gxsql.ec"
  _sqobind[36].sqldata = (char *) &a_kun_gx.freitext1;
#line 1038 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn9, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1038 "gxsql.ec"
  }
	sqlcode = SQLCODE;
/*
 *     EXEC SQL close a_kun_gx_ab_curs;
 */
#line 1040 "gxsql.ec"
  {
#line 1040 "gxsql.ec"
#line 1040 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn9, 768));
#line 1040 "gxsql.ec"
  }
	if ( sqlcode != 0L )
	{
		TimeMsgLine( "K : %8ld  A : %8ld  SQLCODE: %4ld", a_kun_gx.kun, (long) a_kun_gx.a, sqlcode );
		return 0;
	}
	trim( a_kun_gx.a_bz3 );
	trim( a_kun_gx.a_bz4 );
	return 1;
}

long Query_a_bas( void )
{
	long sqlcode;
/*
 * 	EXEC SQL open a_bas_curs using $a_kun_gx.a;
 */
#line 1054 "gxsql.ec"
  {
#line 1054 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 105, sizeof(a_kun_gx.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1054 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqibind, {0}, 1, 0 };
#line 1054 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &a_kun_gx.a;
#line 1054 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn19, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 1054 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open a_bas_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     ||
  	     strcmp( anwender, "DFW" ) == 0
  	     ||
  	     strcmp( anwender, "OKLE" ) == 0
  	     ||
  	     strcmp( anwender, "LOHMANN" ) == 0
  	     ||
  	     strcmp( anwender, "KUEBLER" ) == 0
    )
	{
/*
 * 	    EXEC SQL fetch a_bas_curs into
 *             $data.a_typ, $data.a_typ2, $data.hbk_ztr, $data.a_gew, $data.charg_hand,
 *             $data.brennwert, $data.eiweiss, $data.kh, $data.fett, $data.glg,
 *             $data.krebs, $data.ei, $data.fisch, $data.erdnuss, $data.soja,
 *             $data.milch, $data.schal, $data.sellerie, $data.senfsaat,
 *             $data.sesamsamen, $data.sulfit, $data.weichtier;
 */
#line 1073 "gxsql.ec"
  {
#line 1078 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 102, sizeof(data.a_typ), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(data.a_typ2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(data.hbk_ztr), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(data.a_gew), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(data.charg_hand), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(data.eiweiss), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(data.kh), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(data.fett), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1078 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 22, _sqobind, {0}, 22, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1078 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &data.a_typ;
#line 1078 "gxsql.ec"
  _sqobind[1].sqldata = (char *) &data.a_typ2;
#line 1078 "gxsql.ec"
  _sqobind[2].sqldata = (char *) &data.hbk_ztr;
#line 1078 "gxsql.ec"
  _sqobind[3].sqldata = (char *) &data.a_gew;
#line 1078 "gxsql.ec"
  _sqobind[4].sqldata = (char *) &data.charg_hand;
#line 1078 "gxsql.ec"
  _sqobind[5].sqldata = data.brennwert;
#line 1078 "gxsql.ec"
  _sqobind[6].sqldata = (char *) &data.eiweiss;
#line 1078 "gxsql.ec"
  _sqobind[7].sqldata = (char *) &data.kh;
#line 1078 "gxsql.ec"
  _sqobind[8].sqldata = (char *) &data.fett;
#line 1078 "gxsql.ec"
  _sqobind[9].sqldata = data.glg;
#line 1078 "gxsql.ec"
  _sqobind[10].sqldata = data.krebs;
#line 1078 "gxsql.ec"
  _sqobind[11].sqldata = data.ei;
#line 1078 "gxsql.ec"
  _sqobind[12].sqldata = data.fisch;
#line 1078 "gxsql.ec"
  _sqobind[13].sqldata = data.erdnuss;
#line 1078 "gxsql.ec"
  _sqobind[14].sqldata = data.soja;
#line 1078 "gxsql.ec"
  _sqobind[15].sqldata = data.milch;
#line 1078 "gxsql.ec"
  _sqobind[16].sqldata = data.schal;
#line 1078 "gxsql.ec"
  _sqobind[17].sqldata = data.sellerie;
#line 1078 "gxsql.ec"
  _sqobind[18].sqldata = data.senfsaat;
#line 1078 "gxsql.ec"
  _sqobind[19].sqldata = data.sesamsamen;
#line 1078 "gxsql.ec"
  _sqobind[20].sqldata = data.sulfit;
#line 1078 "gxsql.ec"
  _sqobind[21].sqldata = data.weichtier;
#line 1078 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn19, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1078 "gxsql.ec"
  }
  	    sqlcode = SQLCODE;
/*
 *         EXEC SQL close a_bas_curs;
 */
#line 1080 "gxsql.ec"
  {
#line 1080 "gxsql.ec"
#line 1080 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn19, 768));
#line 1080 "gxsql.ec"
  }
	    if ( sqlcode != 0L )
	    {
		    FatalError( "fetch a_bas_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
		    return 0;
        }
        fprintf( fperr, "data.fett = %4.2f\n", data.fett );
        fprintf( fperr, "data.glg = %s\n", data.glg );
        fprintf( fperr, "data.krebs = %s\n", data.krebs );
        fprintf( fperr, "data.ei = %s\n", data.ei );
        fprintf( fperr, "data.fisch = %s\n", data.fisch );
        fprintf( fperr, "data.erdnuss = %s\n", data.erdnuss );
        fprintf( fperr, "data.soja = %s\n", data.soja );
        fprintf( fperr, "data.milch = %s\n", data.milch );
        fprintf( fperr, "data.schal = %s\n", data.schal );
        fprintf( fperr, "data.sellerie = %s\n", data.sellerie );
        fprintf( fperr, "data.senfsaat = %s\n", data.senfsaat );
        fprintf( fperr, "data.sesamsamen = %s\n", data.sesamsamen );
        fprintf( fperr, "12345 67890 1234\n" );
        fprintf( fperr, "%1s%1s%1s%1s%1s %1s%1s%1s%1s%1s %1s%1s%1s %s %3.1f %3.1f %3.1f\n",
       data.glg,
       data.krebs,
       data.ei,
       data.fisch,
       data.erdnuss,
       data.soja,
       data.milch,
       data.schal,
       data.sellerie,
       data.senfsaat,
       data.sesamsamen,
       data.sulfit,
       data.weichtier,
       data.brennwert,
       data.eiweiss,
       data.kh,
       data.fett );
        if ( data.a_typ == 2 )
        {
/*
 *  	        EXEC SQL open a_eig_curs using $a_kun_gx.a;
 */
#line 1119 "gxsql.ec"
  {
#line 1119 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 105, sizeof(a_kun_gx.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1119 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqibind, {0}, 1, 0 };
#line 1119 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &a_kun_gx.a;
#line 1119 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn15, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 1119 "gxsql.ec"
  }
	        if ( SQLCODE != 0 )
	        {
		        FatalError( "open a_eig_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
/*
 * 			EXEC SQL fetch a_eig_curs into $data.a_tara;
 */
#line 1125 "gxsql.ec"
  {
#line 1125 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 105, sizeof(data.a_tara), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1125 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqobind, {0}, 1, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1125 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &data.a_tara;
#line 1125 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn15, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1125 "gxsql.ec"
  }
 	        sqlcode = SQLCODE;
/*
 *             EXEC SQL close a_eig_curs;
 */
#line 1127 "gxsql.ec"
  {
#line 1127 "gxsql.ec"
#line 1127 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn15, 768));
#line 1127 "gxsql.ec"
  }
	        if ( sqlcode != 0L )
	        {
		        FatalError( "fetch a_eig_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
		        return 0;
            }
        }
        else if ( data.a_typ == 1 )
        {
/*
 *  	        EXEC SQL open a_hndw_curs using $a_kun_gx.a;
 */
#line 1136 "gxsql.ec"
  {
#line 1136 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 105, sizeof(a_kun_gx.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1136 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqibind, {0}, 1, 0 };
#line 1136 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &a_kun_gx.a;
#line 1136 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn17, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 1136 "gxsql.ec"
  }
	        if ( SQLCODE != 0 )
	        {
		        FatalError( "open a_hndw_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
/*
 *  			EXEC SQL fetch a_hndw_curs into $data.a_tara;
 */
#line 1142 "gxsql.ec"
  {
#line 1142 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 105, sizeof(data.a_tara), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1142 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqobind, {0}, 1, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1142 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &data.a_tara;
#line 1142 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn17, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1142 "gxsql.ec"
  }
 	        sqlcode = SQLCODE;
/*
 *             EXEC SQL close a_hndw_curs;
 */
#line 1144 "gxsql.ec"
  {
#line 1144 "gxsql.ec"
#line 1144 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn17, 768));
#line 1144 "gxsql.ec"
  }
	        if ( sqlcode != 0L )
	        {
                data.a_tara = 0.0;
            }
        }
        else if ( data.a_typ2 == 2 )
        {
/*
 *  	        EXEC SQL open a_eig_curs using $a_kun_gx.a;
 */
#line 1152 "gxsql.ec"
  {
#line 1152 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 105, sizeof(a_kun_gx.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1152 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqibind, {0}, 1, 0 };
#line 1152 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &a_kun_gx.a;
#line 1152 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn15, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 1152 "gxsql.ec"
  }
	        if ( SQLCODE != 0 )
	        {
		        FatalError( "open a_eig_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
/*
 * 			EXEC SQL fetch a_eig_curs into $data.a_tara;
 */
#line 1158 "gxsql.ec"
  {
#line 1158 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 105, sizeof(data.a_tara), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1158 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqobind, {0}, 1, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1158 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &data.a_tara;
#line 1158 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn15, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1158 "gxsql.ec"
  }
 	        sqlcode = SQLCODE;
/*
 *             EXEC SQL close a_eig_curs;
 */
#line 1160 "gxsql.ec"
  {
#line 1160 "gxsql.ec"
#line 1160 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn15, 768));
#line 1160 "gxsql.ec"
  }
	        if ( sqlcode != 0L )
	        {
		        FatalError( "fetch a_eig_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
		        return 0;
            }
        }
        else if ( data.a_typ2 == 1 )
        {
/*
 *  	        EXEC SQL open a_hndw_curs using $a_kun_gx.a;
 */
#line 1169 "gxsql.ec"
  {
#line 1169 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 105, sizeof(a_kun_gx.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1169 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqibind, {0}, 1, 0 };
#line 1169 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &a_kun_gx.a;
#line 1169 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn17, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 1169 "gxsql.ec"
  }
	        if ( SQLCODE != 0 )
	        {
		        FatalError( "open a_hndw_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
/*
 *  			EXEC SQL fetch a_hndw_curs into $data.a_tara;
 */
#line 1175 "gxsql.ec"
  {
#line 1175 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 105, sizeof(data.a_tara), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1175 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqobind, {0}, 1, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1175 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &data.a_tara;
#line 1175 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn17, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1175 "gxsql.ec"
  }
 	        sqlcode = SQLCODE;
/*
 *             EXEC SQL close a_hndw_curs;
 */
#line 1177 "gxsql.ec"
  {
#line 1177 "gxsql.ec"
#line 1177 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn17, 768));
#line 1177 "gxsql.ec"
  }
	        if ( sqlcode != 0L )
	        {
                data.a_tara = 0.0;
            }
        }
        else
        {
            data.a_tara = 0.0;
        }
	}
	else  /* Bauerngut */
	{
/*
 * 	    EXEC SQL fetch a_bas_curs into
 *             $data.a_typ, $data.hbk_ztr, $data.a_tara, $data.a_gew, $data.up_ka,
 *             $data.charg_hand;
 */
#line 1190 "gxsql.ec"
  {
#line 1192 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 102, sizeof(data.a_typ), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(data.hbk_ztr), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(data.a_tara), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(data.a_gew), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(data.up_ka), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(data.charg_hand), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1192 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 6, _sqobind, {0}, 6, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1192 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &data.a_typ;
#line 1192 "gxsql.ec"
  _sqobind[1].sqldata = (char *) &data.hbk_ztr;
#line 1192 "gxsql.ec"
  _sqobind[2].sqldata = (char *) &data.a_tara;
#line 1192 "gxsql.ec"
  _sqobind[3].sqldata = (char *) &data.a_gew;
#line 1192 "gxsql.ec"
  _sqobind[4].sqldata = (char *) &data.up_ka;
#line 1192 "gxsql.ec"
  _sqobind[5].sqldata = (char *) &data.charg_hand;
#line 1192 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn19, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1192 "gxsql.ec"
  }
 	    sqlcode = SQLCODE;
/*
 *         EXEC SQL close a_bas_curs;
 */
#line 1194 "gxsql.ec"
  {
#line 1194 "gxsql.ec"
#line 1194 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn19, 768));
#line 1194 "gxsql.ec"
  }
	    if ( sqlcode != 0L )
	    {
		    FatalError( "fetch a_bas_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
		    return 0;
        }
        /*
        if ( data.a_typ == 2 )
        {
 	        EXEC SQL open a_eig_curs using $a_kun_gx.a;
	        if ( SQLCODE != 0 )
	        {
		        FatalError( "open a_eig_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
			EXEC SQL fetch a_eig_curs into $a_kun_gx.tara;
 	        sqlcode = SQLCODE;
            EXEC SQL close a_eig_curs;
	        if ( sqlcode != 0L )
	        {
		        FatalError( "fetch a_eig_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
		        return 0;
            }
        }
        else if ( data.a_typ == 1 )
        {
 	        EXEC SQL open a_hndw_curs using $a_kun_gx.a;
	        if ( SQLCODE != 0 )
	        {
		        FatalError( "open a_hndw_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
 			EXEC SQL fetch a_hndw_curs into $a_kun_gx.tara;
 	        sqlcode = SQLCODE;
            EXEC SQL close a_hndw_curs;
	        if ( sqlcode != 0L )
	        {
                data.a_tara = 0.0;
            }
        }

 	    EXEC SQL open a_lgr_curs using $a_kun_gx.a;
	    if ( SQLCODE != 0 )
	    {
		    FatalError( "open a_lgr_curs: SQLCODE: %ld\n",  SQLCODE );
            return 0;
	    }

 	    EXEC SQL fetch a_lgr_curs into $data.inh_wanne;
 	    sqlcode = SQLCODE;
        EXEC SQL close a_lgr_curs;
	    if ( sqlcode != 0L )
	    {
		    fprintf( fperr, "fetch a_lgr_curs: %ld SQLCODE: %ld\n", (long) a_kun_gx.a, sqlcode );
            data.inh_wanne = 1;
        }
        if ( data.inh_wanne < 1 )
        {
           data.inh_wanne = 1;
        }
        */
    }
	ggdat.fgew = (long) (1000.0 * data.a_gew);
	ggdat.haltbar1 = data.hbk_ztr;
	ggdat.tara = (long) (1000.0 * data.a_tara);
	ggdat.sum1vw = data.up_ka;
	return 1;
}

long Query_a_bas_erw( void )
{
	long sqlcode;
	memset( (char*) &a_bas_erw, '\0', sizeof (a_bas_erw) );
/*
 * 	EXEC SQL open a_bas_erw_curs using $a_kun_gx.a;
 */
#line 1267 "gxsql.ec"
  {
#line 1267 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 105, sizeof(a_kun_gx.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1267 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqibind, {0}, 1, 0 };
#line 1267 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &a_kun_gx.a;
#line 1267 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn23, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 1267 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open a_bas_erw_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
/*
 *     EXEC SQL fetch a_bas_erw_curs into
 * 	   $a_bas_erw.pp_a_bz1,
 * 	   $a_bas_erw.pp_a_bz2,
 * 	   $a_bas_erw.lgr_tmpr,
 * 	   $a_bas_erw.lupine,
 * 	   $a_bas_erw.schutzgas,
 * 	   $a_bas_erw.huelle,
 * 	   $a_bas_erw.salz,
 * 	   $a_bas_erw.davonfett,
 * 	   $a_bas_erw.davonzucker,
 * 	   $a_bas_erw.ballaststoffe,
 * 	   $a_bas_erw.zutat;
 */
#line 1273 "gxsql.ec"
  {
#line 1284 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 101, sizeof(a_bas_erw.huelle), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_bas_erw.salz), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_bas_erw.davonfett), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_bas_erw.davonzucker), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(a_bas_erw.ballaststoffe), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 2001, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1284 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 11, _sqobind, {0}, 11, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1284 "gxsql.ec"
  _sqobind[0].sqldata = a_bas_erw.pp_a_bz1;
#line 1284 "gxsql.ec"
  _sqobind[1].sqldata = a_bas_erw.pp_a_bz2;
#line 1284 "gxsql.ec"
  _sqobind[2].sqldata = a_bas_erw.lgr_tmpr;
#line 1284 "gxsql.ec"
  _sqobind[3].sqldata = a_bas_erw.lupine;
#line 1284 "gxsql.ec"
  _sqobind[4].sqldata = a_bas_erw.schutzgas;
#line 1284 "gxsql.ec"
  _sqobind[5].sqldata = (char *) &a_bas_erw.huelle;
#line 1284 "gxsql.ec"
  _sqobind[6].sqldata = (char *) &a_bas_erw.salz;
#line 1284 "gxsql.ec"
  _sqobind[7].sqldata = (char *) &a_bas_erw.davonfett;
#line 1284 "gxsql.ec"
  _sqobind[8].sqldata = (char *) &a_bas_erw.davonzucker;
#line 1284 "gxsql.ec"
  _sqobind[9].sqldata = (char *) &a_bas_erw.ballaststoffe;
#line 1284 "gxsql.ec"
  _sqobind[10].sqldata = a_bas_erw.zutat;
#line 1284 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn23, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1284 "gxsql.ec"
  }
    sqlcode = SQLCODE;
/*
 *     EXEC SQL close a_bas_erw_curs;
 */
#line 1286 "gxsql.ec"
  {
#line 1286 "gxsql.ec"
#line 1286 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn23, 768));
#line 1286 "gxsql.ec"
  }
    if ( sqlcode != 0L )
    {
	    FatalError( "fetch a_bas_erw_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
	    return 0;
    }
	trim( a_bas_erw.pp_a_bz1 );
	trim( a_bas_erw.pp_a_bz2 );
	trim( a_bas_erw.lgr_tmpr );
	trim( a_bas_erw.zutat );
	return 1;
}

long Query_paz_ipr( void )
{
	long sqlcode;
  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     ||
  	     strcmp( anwender, "DFW" ) == 0
  	     ||
  	     strcmp( anwender, "OKLE" ) == 0
  	     ||
  	     strcmp( anwender, "LOHMANN" ) == 0
  	     ||
  	     strcmp( anwender, "KUEBLER" ) == 0
  	)
    {
        return 0;
    }
/*
 * 	EXEC SQL open paz_ipr_curs using $paz_ipr.bran_kun,$paz_ipr.a;
 */
#line 1317 "gxsql.ec"
  {
#line 1317 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 103, sizeof(paz_ipr.bran_kun), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(paz_ipr.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1317 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 2, _sqibind, {0}, 2, 0 };
#line 1317 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &paz_ipr.bran_kun;
#line 1317 "gxsql.ec"
  _sqibind[1].sqldata = (char *) &paz_ipr.a;
#line 1317 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn25, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 1317 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open paz_ipr_curs: SQLCODE: %ld", SQLCODE );
        return 0;
	}
/*
 * 	EXEC SQL fetch paz_ipr_curs into $paz_ipr.ld_pr;
 */
#line 1323 "gxsql.ec"
  {
#line 1323 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 105, sizeof(paz_ipr.ld_pr), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1323 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqobind, {0}, 1, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1323 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &paz_ipr.ld_pr;
#line 1323 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn25, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1323 "gxsql.ec"
  }
	sqlcode = SQLCODE;
/*
 *     EXEC SQL close paz_ipr_curs;
 */
#line 1325 "gxsql.ec"
  {
#line 1325 "gxsql.ec"
#line 1325 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn25, 768));
#line 1325 "gxsql.ec"
  }

	fprintf( fperr, "BK: %8ld  A : %8ld  SQLCODE: %4ld\n", paz_ipr.bran_kun, (long) paz_ipr.a, sqlcode );
	
	if ( sqlcode != 0L )
	{
        return 0;
	}
	return 1;
}

long Query_kun( void )
{
	long sqlcode;
/*
 * 	EXEC SQL open kun_curs using $kun.mdn, $kun.fil, $kun.kun;
 */
#line 1339 "gxsql.ec"
  {
#line 1339 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 102, sizeof(kun.mdn), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(kun.fil), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(kun.kun), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1339 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 3, _sqibind, {0}, 3, 0 };
#line 1339 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &kun.mdn;
#line 1339 "gxsql.ec"
  _sqibind[1].sqldata = (char *) &kun.fil;
#line 1339 "gxsql.ec"
  _sqibind[2].sqldata = (char *) &kun.kun;
#line 1339 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn31, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 1339 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open kun_curs: SQLCODE=%ld",  SQLCODE );
        return 0;
	}
/*
 * 	EXEC SQL fetch kun_curs into $kun.kun_bran2, $kun.kun_gr1, $kun.adr1, $kun.stat_kun;
 */
#line 1345 "gxsql.ec"
  {
#line 1345 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 100, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(kun.kun_gr1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(kun.adr1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(kun.stat_kun), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1345 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 4, _sqobind, {0}, 4, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1345 "gxsql.ec"
  _sqobind[0].sqldata = kun.kun_bran2;
#line 1345 "gxsql.ec"
  _sqobind[1].sqldata = (char *) &kun.kun_gr1;
#line 1345 "gxsql.ec"
  _sqobind[2].sqldata = (char *) &kun.adr1;
#line 1345 "gxsql.ec"
  _sqobind[3].sqldata = (char *) &kun.stat_kun;
#line 1345 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn31, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1345 "gxsql.ec"
  }
	sqlcode = SQLCODE;
/*
 *     EXEC SQL close kun_curs;
 */
#line 1347 "gxsql.ec"
  {
#line 1347 "gxsql.ec"
#line 1347 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn31, 768));
#line 1347 "gxsql.ec"
  }
	if ( sqlcode != 0L )
	{
		FatalError( "fetch kun_curs: mdn: %ld, fil: %ld, kun: %ld SQLCODE: %ld",
		            kun.mdn, kun.fil, kun.kun, sqlcode );
		return 0;
	}
	if (a_kun_gx.kun_bran2[2] == ' ') a_kun_gx.kun_bran2[2] = '\0';
	
	MsgLine( "kun = %ld,  adr1 = %ld,  kun_gr1: %d, stat_kun: %ld",
	         kun.kun, kun.adr1, kun.kun_gr1, kun.stat_kun );
	
	kun.adr_nam1[0] = '\0';
	kun.adr_nam2[0] = '\0';
	kun.str[0] = '\0';
	kun.plz[0] = '\0';
	kun.ort1[0] = '\0';
	if ( kun.kun_gr1 == 0 )
    {
        return 1;
    }
	
/*
 * 	EXEC SQL open adr_curs using $kun.adr1;
 */
#line 1369 "gxsql.ec"
  {
#line 1369 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 103, sizeof(kun.adr1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1369 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqibind, {0}, 1, 0 };
#line 1369 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &kun.adr1;
#line 1369 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn33, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 1369 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open adr_curs: SQLCODE=%ld",  SQLCODE );
        return 0;
	}
/*
 * 	EXEC SQL fetch adr_curs	into $kun.adr_nam1, $kun.adr_nam2,
 * 	    $kun.str, $kun.plz, $kun.ort1;
 */
#line 1375 "gxsql.ec"
  {
#line 1376 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 100, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1376 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 5, _sqobind, {0}, 5, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1376 "gxsql.ec"
  _sqobind[0].sqldata = kun.adr_nam1;
#line 1376 "gxsql.ec"
  _sqobind[1].sqldata = kun.adr_nam2;
#line 1376 "gxsql.ec"
  _sqobind[2].sqldata = kun.str;
#line 1376 "gxsql.ec"
  _sqobind[3].sqldata = kun.plz;
#line 1376 "gxsql.ec"
  _sqobind[4].sqldata = kun.ort1;
#line 1376 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn33, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1376 "gxsql.ec"
  }
	sqlcode = SQLCODE;
/*
 *     EXEC SQL close adr_curs;
 */
#line 1378 "gxsql.ec"
  {
#line 1378 "gxsql.ec"
#line 1378 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn33, 768));
#line 1378 "gxsql.ec"
  }
	if ( sqlcode != 0L )
	{
		MsgLine( "fetch adr_curs: adr: %ld SQLCODE: %ld", kun.adr1, sqlcode );
		kun.kun_gr1 = 0;
		return 1;
	}
	trim(kun.adr_nam1);
	trim(kun.adr_nam2);
	trim(kun.str);
	trim(kun.plz);
	trim(kun.ort1);
	
	return 1;
}

void Query_paz_cab( GGCAB_t * ggcab )
{
    long sqlcode;
    paz_cab.cabnum = ggcab->cabnr;
/*
 * 	EXEC SQL open paz_cab_curs using $paz_cab.cabnum;
 */
#line 1398 "gxsql.ec"
  {
#line 1398 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 102, sizeof(paz_cab.cabnum), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1398 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqibind, {0}, 1, 0 };
#line 1398 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &paz_cab.cabnum;
#line 1398 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn27, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 1398 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open paz_cab_curs: SQLCODE: %ld",  SQLCODE );
	}
/*
 * 	EXEC SQL fetch paz_cab_curs into $paz_cab.cab;
 */
#line 1403 "gxsql.ec"
  {
#line 1403 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 100, 201, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1403 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 1, _sqobind, {0}, 1, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1403 "gxsql.ec"
  _sqobind[0].sqldata = paz_cab.cab;
#line 1403 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn27, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1403 "gxsql.ec"
  }
	sqlcode = SQLCODE;
/*
 *     EXEC SQL close paz_cab_curs;
 */
#line 1405 "gxsql.ec"
  {
#line 1405 "gxsql.ec"
#line 1405 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn27, 768));
#line 1405 "gxsql.ec"
  }
    if ( sqlcode != 0L )
	{
		MsgLine( "fetch paz_cab_curs: %ld SQLCODE: %ld", paz_cab.cabnum, sqlcode );
		paz_cab.cab[0] = '\0';
	}
	trim( paz_cab.cab );
	strcpy( ggcab->cab, paz_cab.cab );
	ggcab->cablen = (us) strlen(ggcab->cab) + 1;
}

void Query_kun_geb( void )
{
    long sqlcode;

    if ( system_num < 1 )
       return;
	
/*
 * 	EXEC SQL open kun_geb_curs;
 */
#line 1423 "gxsql.ec"
  {
#line 1423 "gxsql.ec"
#line 1423 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn29, 768), (ifx_sqlda_t *) 0, (char *) 0, (struct value *) 0, 0, 0);
#line 1423 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open kun_geb_curs: SQLCODE: %ld",  SQLCODE );
	}
/*
 * 	EXEC SQL fetch kun_geb_curs into
 * 	    :kun_geb.geb_fill, :kun_geb.pal_fill,
 * 	    :kun_geb.geb_anz,  :kun_geb.pal_anz,
 * 	    :kun_geb.tara;
 */
#line 1428 "gxsql.ec"
  {
#line 1431 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 102, sizeof(kun_geb.geb_fill), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(kun_geb.pal_fill), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(kun_geb.geb_anz), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(kun_geb.pal_anz), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 105, sizeof(kun_geb.tara), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1431 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 5, _sqobind, {0}, 5, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1431 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &kun_geb.geb_fill;
#line 1431 "gxsql.ec"
  _sqobind[1].sqldata = (char *) &kun_geb.pal_fill;
#line 1431 "gxsql.ec"
  _sqobind[2].sqldata = (char *) &kun_geb.geb_anz;
#line 1431 "gxsql.ec"
  _sqobind[3].sqldata = (char *) &kun_geb.pal_anz;
#line 1431 "gxsql.ec"
  _sqobind[4].sqldata = (char *) &kun_geb.tara;
#line 1431 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn29, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1431 "gxsql.ec"
  }
	sqlcode = SQLCODE;
/*
 *     EXEC SQL close kun_geb_curs;
 */
#line 1433 "gxsql.ec"
  {
#line 1433 "gxsql.ec"
#line 1433 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn29, 768));
#line 1433 "gxsql.ec"
  }
    if ( sqlcode != 0L )
	{
		MsgLine( "fetch kun_geb_curs: %ld SQLCODE: %ld", system_num, sqlcode );
	}
	fprintf( fperr, "Vorgabe S1: (ME %d) %ld\n", kun_geb.geb_fill, kun_geb.geb_anz );
	fprintf( fperr, "Vorgabe S2: (ME %d) %ld\n", kun_geb.pal_fill, kun_geb.pal_anz );
  	fprintf( fperr, "Tara: %lf kg\n", kun_geb.tara);

}

long Query_pr_ausz_gx( long text_nr, int eti_typ )
{
	long sqlcode;
	pr_ausz_gx.text_nr = text_nr;
	pr_ausz_gx.eti_typ = eti_typ;
/*
 * 	EXEC SQL open pr_ausz_gx_curs using $pr_ausz_gx.text_nr, $pr_ausz_gx.eti_typ;
 */
#line 1449 "gxsql.ec"
  {
#line 1449 "gxsql.ec"
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 103, sizeof(pr_ausz_gx.text_nr), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.eti_typ), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1449 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 2, _sqibind, {0}, 2, 0 };
#line 1449 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &pr_ausz_gx.text_nr;
#line 1449 "gxsql.ec"
  _sqibind[1].sqldata = (char *) &pr_ausz_gx.eti_typ;
#line 1449 "gxsql.ec"
  sqli_curs_open(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn35, 768), &_SD0, (char *) 0, (struct value *) 0, 1, 0);
#line 1449 "gxsql.ec"
  }
	if ( SQLCODE != 0 )
	{
		FatalError( "open pr_ausz_gx_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
/*
 * 	EXEC SQL fetch pr_ausz_gx_curs into
 * 	   $pr_ausz_gx.sg1, $pr_ausz_gx.txt1,
 * 	   $pr_ausz_gx.sg2, $pr_ausz_gx.txt2,
 * 	   $pr_ausz_gx.sg3, $pr_ausz_gx.txt3,
 * 	   $pr_ausz_gx.sg4, $pr_ausz_gx.txt4,
 * 	   $pr_ausz_gx.sg5, $pr_ausz_gx.txt5,
 * 	   $pr_ausz_gx.sg6, $pr_ausz_gx.txt6,
 * 	   $pr_ausz_gx.sg7, $pr_ausz_gx.txt7,
 * 	   $pr_ausz_gx.sg8, $pr_ausz_gx.txt8,
 * 	   $pr_ausz_gx.sg9, $pr_ausz_gx.txt9,
 * 	   $pr_ausz_gx.sg10, $pr_ausz_gx.txt10,
 * 	   $pr_ausz_gx.sg11, $pr_ausz_gx.txt11,
 * 	   $pr_ausz_gx.sg12, $pr_ausz_gx.txt12;	
 */
#line 1455 "gxsql.ec"
  {
#line 1467 "gxsql.ec"
  static ifx_sqlvar_t _sqobind[] = 
    {
      { 102, sizeof(pr_ausz_gx.sg1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.sg2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.sg3), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.sg4), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.sg5), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.sg6), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.sg7), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.sg8), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.sg9), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.sg10), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.sg11), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 102, sizeof(pr_ausz_gx.sg12), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 1467 "gxsql.ec"
    };
  static ifx_sqlda_t _SD0 = { 24, _sqobind, {0}, 24, 0 };
  static _FetchSpec _FS1 = { 0, 1, 0 };
#line 1467 "gxsql.ec"
  _sqobind[0].sqldata = (char *) &pr_ausz_gx.sg1;
#line 1467 "gxsql.ec"
  _sqobind[1].sqldata = pr_ausz_gx.txt1;
#line 1467 "gxsql.ec"
  _sqobind[2].sqldata = (char *) &pr_ausz_gx.sg2;
#line 1467 "gxsql.ec"
  _sqobind[3].sqldata = pr_ausz_gx.txt2;
#line 1467 "gxsql.ec"
  _sqobind[4].sqldata = (char *) &pr_ausz_gx.sg3;
#line 1467 "gxsql.ec"
  _sqobind[5].sqldata = pr_ausz_gx.txt3;
#line 1467 "gxsql.ec"
  _sqobind[6].sqldata = (char *) &pr_ausz_gx.sg4;
#line 1467 "gxsql.ec"
  _sqobind[7].sqldata = pr_ausz_gx.txt4;
#line 1467 "gxsql.ec"
  _sqobind[8].sqldata = (char *) &pr_ausz_gx.sg5;
#line 1467 "gxsql.ec"
  _sqobind[9].sqldata = pr_ausz_gx.txt5;
#line 1467 "gxsql.ec"
  _sqobind[10].sqldata = (char *) &pr_ausz_gx.sg6;
#line 1467 "gxsql.ec"
  _sqobind[11].sqldata = pr_ausz_gx.txt6;
#line 1467 "gxsql.ec"
  _sqobind[12].sqldata = (char *) &pr_ausz_gx.sg7;
#line 1467 "gxsql.ec"
  _sqobind[13].sqldata = pr_ausz_gx.txt7;
#line 1467 "gxsql.ec"
  _sqobind[14].sqldata = (char *) &pr_ausz_gx.sg8;
#line 1467 "gxsql.ec"
  _sqobind[15].sqldata = pr_ausz_gx.txt8;
#line 1467 "gxsql.ec"
  _sqobind[16].sqldata = (char *) &pr_ausz_gx.sg9;
#line 1467 "gxsql.ec"
  _sqobind[17].sqldata = pr_ausz_gx.txt9;
#line 1467 "gxsql.ec"
  _sqobind[18].sqldata = (char *) &pr_ausz_gx.sg10;
#line 1467 "gxsql.ec"
  _sqobind[19].sqldata = pr_ausz_gx.txt10;
#line 1467 "gxsql.ec"
  _sqobind[20].sqldata = (char *) &pr_ausz_gx.sg11;
#line 1467 "gxsql.ec"
  _sqobind[21].sqldata = pr_ausz_gx.txt11;
#line 1467 "gxsql.ec"
  _sqobind[22].sqldata = (char *) &pr_ausz_gx.sg12;
#line 1467 "gxsql.ec"
  _sqobind[23].sqldata = pr_ausz_gx.txt12;
#line 1467 "gxsql.ec"
  sqli_curs_fetch(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn35, 768), (ifx_sqlda_t *) 0, &_SD0, (char *) 0, &_FS1);
#line 1467 "gxsql.ec"
  }
	sqlcode = SQLCODE;
/*
 *     EXEC SQL close pr_ausz_gx_curs;
 */
#line 1469 "gxsql.ec"
  {
#line 1469 "gxsql.ec"
#line 1469 "gxsql.ec"
  sqli_curs_close(ESQLINTVERSION, sqli_curs_locate(ESQLINTVERSION, (char *) _Cn35, 768));
#line 1469 "gxsql.ec"
  }

	fprintf( fperr, "TF: %8d  TN: %8ld  SQLCODE: %4ld\n", pr_ausz_gx.eti_typ, pr_ausz_gx.text_nr, sqlcode );

	if ( sqlcode != 0L )
	{
	    pr_ausz_gx.sg1 = 0;
	    pr_ausz_gx.sg2 = 0;
	    pr_ausz_gx.sg3 = 0;
	    pr_ausz_gx.sg4 = 0;
	    pr_ausz_gx.sg5 = 0;
	    pr_ausz_gx.sg6 = 0;
	    pr_ausz_gx.sg7 = 0;
	    pr_ausz_gx.sg8 = 0;
	    pr_ausz_gx.sg9 = 0;
	    pr_ausz_gx.sg10 = 0;
	    pr_ausz_gx.sg11 = 0;
	    pr_ausz_gx.sg12 = 0;
	    pr_ausz_gx.txt1[0] = '\0';
	    pr_ausz_gx.txt2[0] = '\0';
	    pr_ausz_gx.txt3[0] = '\0';
	    pr_ausz_gx.txt4[0] = '\0';
	    pr_ausz_gx.txt5[0] = '\0';
	    pr_ausz_gx.txt6[0] = '\0';
	    pr_ausz_gx.txt7[0] = '\0';
	    pr_ausz_gx.txt8[0] = '\0';
	    pr_ausz_gx.txt9[0] = '\0';
	    pr_ausz_gx.txt10[0] = '\0';
	    pr_ausz_gx.txt11[0] = '\0';
	    pr_ausz_gx.txt12[0] = '\0';
		return 0;
	}
	trim( pr_ausz_gx.txt1 );
	trim( pr_ausz_gx.txt2 );
	trim( pr_ausz_gx.txt3 );
	trim( pr_ausz_gx.txt4 );
	trim( pr_ausz_gx.txt5 );
	trim( pr_ausz_gx.txt6 );
	trim( pr_ausz_gx.txt7 );
	trim( pr_ausz_gx.txt8 );
	trim( pr_ausz_gx.txt9 );
	trim( pr_ausz_gx.txt10 );
	trim( pr_ausz_gx.txt11 );
	trim( pr_ausz_gx.txt12 );
	return 1;
}

void initcursors( void )
{
	OpenDatabase();

	FormQuery_a_kun();
	FormQuery_a_kun_gx_akb();
	FormQuery_a_kun_gx_ak();
	FormQuery_a_kun_gx_ab();
	FormQuery_a_kun_gx_kb();
	FormQuery_pr_ausz_gx();
	FormQuery_a_bas();
	FormQuery_a_bas_erw();
	FormQuery_paz_ipr();
	FormQuery_paz_cab();
	FormQuery_kun();
	FormQuery_pazlsp();
	FormQuery_kun_geb();

	cursors_initialized = 1;
	
	strcpy( frmtxt1, itmval("TEXTFORM1") );
	strcpy( frmtxt2, itmval("TEXTFORM2") );
	strcpy( frmtxt3, itmval("TEXTFORM3") );
	strcpy( frmtxt4, itmval("TEXTFORM4") );
	strcpy( frmtxt5, itmval("TEXTFORM5") );
	strcpy( frmtxt6, itmval("TEXTFORM6") );
	strcpy( frmtxt7, itmval("TEXTFORM7") );
	strcpy( frmtxt8, itmval("TEXTFORM8") );
	strcpy( frmtxt9, itmval("TEXTFORM9") );
	fprintf( fperr, "Formatierung: %s %s %s %s %s %s %s %s %s\n",
	    frmtxt1, frmtxt2, frmtxt3, frmtxt4, frmtxt5, frmtxt6, frmtxt7,
	    frmtxt8, frmtxt9 );

}

void vorbelegung( void )
{
	data.charge[0] = '\0';
	data.datum1[0] = '\0';
	data.datum2[0] = '\0';
	data.kuna[0] = '\0';
	// strcpy( data.kuna, "4711" );
	data.tour = 0;
	data.amenge = 0;
	data.up_ka = 0;
	data.up_pal = 0;
	data.aeinheit = 0;
	data.keinheit = 0;
	data.paleinheit = 0;
	data.hbk_ztr = 0;
	data.a_gew = 0;
	data.a_tara = 0;
	data.a_typ = 0;
	data.partie = 0;
	data.esnum[0] = '\0';
	data.es2num[0] = '\0';
	data.es3num[0] = '\0';
	data.ez1num[0] = '\0';
	data.ez2num[0] = '\0';
	data.ez3num[0] = '\0';
	
	a_kun_gx.kun = 0;
	strcpy( a_kun_gx.kun_bran2, "0" );
	
	ggdat.aart = 0 ;
	ggdat.gpreis = 0 ;
	ggdat.spreis = 0 ;
	ggdat.devise = 6 ;
	ggdat.devise2 = 0 ;
	ggdat.stk_pro_pck = 0 ;
	ggdat.cabnr1 = 0 ;
	ggdat.cabnr2 = 0 ;
	ggdat.cabnr3 = 0 ;
	ggdat.cabnr4 = 0 ;
	ggdat.etin = 1 ;    /* Wichtig, wenn ohne Daten: 0 geht nicht. */
	ggdat.ets1 = 0 ;
	ggdat.ets2 = 0 ;
	ggdat.logo1 = 0 ;
	ggdat.logo2 = 0 ;
	strcpy( ggdat.cts1, "000000000000" ) ;
	ggdat.cts1len = (us) strlen ( ggdat.cts1 ) ;
	strcpy( ggdat.cts2, "000000000000" ) ;
	ggdat.cts2len = (us) strlen ( ggdat.cts2 ) ;
	ggdat.ampar = 0 ;
	ggdat.gwpar = 0xFFFFFFFF ;
	ggdat.haltbar1 = 0 ;
	ggdat.datum1 = 0 ;
	ggdat.datum2 = 0 ;
	ggdat.tara = 0 ;
	ggdat.fgew = 0 ;
	ggdat.aart = 0 ;
	ggdat.stksum = 0;
	ggdat.stksum2 = 0;
	ggdat.stksum3 = 0;
	ggdat.netsum = 0;
	ggdat.netsum2 = 0;
	ggdat.netsum3 = 0;
	ggdat.sum1vwa = 1;
	ggdat.sum2vwa = 1;
	ggdat.sum3vwa = 8;
	ggdat.sum1vw = 0;
	ggdat.sum2vw = 0;
	ggdat.sum3vw = 0;
	ggdat.chargennr = 0;

	strcpy( ggatx1.atx, "" );
	ggatx1.atxlen = (us) strlen ( ggatx1.atx ) ;

	strcpy( ggatx2.atx, "" );
	ggatx2.atxlen = (us) strlen ( ggatx2.atx ) ;

	strcpy( ggatx3.atx, "" );
	ggatx3.atxlen = (us) strlen ( ggatx3.atx ) ;

	strcpy( ggatx4.atx, "" );
	ggatx4.atxlen = (us) strlen ( ggatx4.atx ) ;

	strcpy( ggatx5.atx, "" );
	ggatx5.atxlen = (us) strlen ( ggatx5.atx ) ;

	strcpy( ggatx6.atx, "" );
	ggatx6.atxlen = (us) strlen ( ggatx6.atx ) ;

	strcpy( ggatx7.atx, "" );
	ggatx7.atxlen = (us) strlen ( ggatx7.atx ) ;

	strcpy( ggatx8.atx, "" );
	ggatx8.atxlen = (us) strlen ( ggatx8.atx ) ;

	strcpy( ggatx9.atx, "" );
	ggatx9.atxlen = (us) strlen ( ggatx9.atx ) ;

	strcpy( ggatx10.atx, "" );
	ggatx10.atxlen = (us) strlen ( ggatx10.atx ) ;

	strcpy( ggatx11.atx, "" );
	ggatx11.atxlen = (us) strlen ( ggatx11.atx ) ;

	strcpy( ggatx12.atx, "" );
	ggatx12.atxlen = (us) strlen ( ggatx12.atx ) ;

}

void evalparameter( char *p )
{
	char *q;
	q = strchr( p, '=' );
	if ( q == NULL )
		return;
	q++;
	if ( memcmp( p, "Command", strlen("Command") ) == 0 ) {
		strcpy( command, q );
	} else if ( memcmp( p, "Artikeltyp", strlen("Artikeltyp") ) == 0 ) {
		data.a_typ = atol( q );
	} else if ( memcmp( p, "Artikel", strlen("Artikel") ) == 0 ) {
		ggdat.plunr = atol( q );
		a_kun_gx.a = (double) ggdat.plunr;
	} else if ( memcmp( p, "Kundenartikel", strlen("Kundenartikel") ) == 0 ) {
		strcpy( data.kuna, q );
		if ( strcmp( data.kuna, "0" ) <= 0 )
		{
		    strcpy( data.kuna, "  " );
     		fprintf( fperr, "Kundenartikel: <= 0 --> '  '\n" );
		}
	} else if ( memcmp( p, "LsKunde", strlen("LsKunde") ) == 0 ) {
		kun.kun = atol( q );
		if ( risnull( CLONGTYPE, (char*) &kun.kun) )
		{
				kun.kun = 0;
     		    fprintf( fperr, "LsKunde: NULL --> 0\n" );
		}
	} else if ( memcmp( p, "LsMdn", strlen("LsMdn") ) == 0 ) {
		kun.mdn = atol( q );
		if ( risnull( CLONGTYPE, (char*) &kun.mdn) )
		{
				kun.mdn = 0;
     		    fprintf( fperr, "LsMdn: NULL --> 0\n" );
		}
	} else if ( memcmp( p, "LsFil", strlen("LsFil") ) == 0 ) {
		kun.fil = atol( q );
		if ( risnull( CLONGTYPE, (char*) &kun.fil) )
		{
				kun.fil = 0;
     		    fprintf( fperr, "LsFil: NULL --> 0\n" );
		}
	} else if ( memcmp( p, "Kunde", strlen("Kunde") ) == 0 ) {
		a_kun_gx.kun = atol( q );
		if ( risnull( CLONGTYPE, (char*) &a_kun_gx.kun) )
		{
				a_kun_gx.kun = 0;
     		    fprintf( fperr, "Kunde: NULL --> 0\n" );
		}
	} else if ( memcmp( p, "Branche", strlen("Branche") ) == 0 ) {
		strcpy( a_kun_gx.kun_bran2, q );
		if ( a_kun_gx.kun_bran2[0] == '\0' )
		{
		    strcpy( a_kun_gx.kun_bran2, "0" );
		    fprintf( fperr, "Branche: NULL --> 0\n" );
		}
		ggdat.kdnr = atol( a_kun_gx.kun_bran2 );
	} else if ( memcmp( p, "Auftragsmengeneinheit", strlen("Auftragsmengeneinheit") ) == 0 ) {
		data.aeinheit = atol( q );
	} else if ( memcmp( p, "Auftragsmenge", strlen("Auftragsmenge") ) == 0 ) {
		data.amenge = atol( q );
	} else if ( memcmp( p, "Auftrag", strlen("Auftrag") ) == 0 ) {
		pazlsp.mdn = 1;
		pazlsp.ls = atol( q );
	} else if ( memcmp( p, "Kartonmengeneinheit", strlen("Kartonmengeneinheit") ) == 0 ) {
		data.keinheit = atol( q );
	} else if ( memcmp( p, "Kartonmenge", strlen("Kartonmenge") ) == 0 ) {
		data.up_ka = atol( q );
	} else if ( memcmp( p, "Palettenmengeneinheit", strlen("Palettenmengeneinheit") ) == 0 ) {
		data.paleinheit = atol( q );
	} else if ( memcmp( p, "Palettenmenge", strlen("Palettenmenge") ) == 0 ) {
		data.up_pal = atol( q );
	} else if ( memcmp( p, "Haltbarkeitstage", strlen("Haltbarkeitstage") ) == 0 ) {
		ggdat.haltbar1 = atol( q );
	} else if ( memcmp( p, "Datum1", strlen("Datum1") ) == 0 ) {
        strcpy( data.datum1, q );
		/*   i  = 0123456789 */
		/* q[i] = DD.MM.JJJJ */
		ggdat.datum1 = atol(data.datum1 + 8);         /* JJ */
		data.datum1[5] = '\0';
		ggdat.datum1 += atol(data.datum1 + 3) * 100;  /* MM */
		data.datum1[2] = '\0';
		ggdat.datum1 += atol(data.datum1) * 10000;    /* DD */
		/* Jetzt sollte sein: ggdat.datum1 = DDMMJJ */
		fprintf( fperr, "datum1: %d\n", ggdat.datum1 );
	} else if ( memcmp( p, "Datum2", strlen("Datum2") ) == 0 ) {
		strcpy( data.datum2, q );
		/*   i  = 0123456789 */
		/* q[i] = DD.MM.JJJJ */
		ggdat.datum2 = atol(data.datum2 + 8);         /* JJ */
		data.datum2[5] = '\0';
		ggdat.datum2 += atol(data.datum2 + 3) * 100;  /* MM */
		data.datum2[2] = '\0';
		ggdat.datum2 += atol(data.datum2) * 10000;    /* DD */
		/* Jetzt sollte sein: ggdat.datum2 = DDMMJJ */
		fprintf( fperr, "datum2: %d\n", ggdat.datum2 );
	} else if ( memcmp( p, "Charge", strlen("Charge") ) == 0 ) {
		strcpy( data.charge, q );
	} else if ( memcmp( p, "Partie", strlen("Partie") ) == 0 ) {
		data.partie = atol( q );		
	} else if ( memcmp( p, "Zul_nr_es2", strlen("Zul_nr_es2") ) == 0 ) {
		strcpy( data.es2num, q );		
	} else if ( memcmp( p, "Zul_nr_es3", strlen("Zul_nr_es3") ) == 0 ) {
		strcpy( data.es3num, q );		
	} else if ( memcmp( p, "Zul_nr_es", strlen("Zul_nr_es") ) == 0 ) {
		strcpy( data.esnum, q );		
	} else if ( memcmp( p, "Zul_nr_ez1", strlen("Zul_nr_ez1") ) == 0 ) {
		strcpy( data.ez1num, q );		
	} else if ( memcmp( p, "Zul_nr_ez2", strlen("Zul_nr_ez2") ) == 0 ) {
		strcpy( data.ez2num, q );		
	} else if ( memcmp( p, "Zul_nr_ez3", strlen("Zul_nr_ez3") ) == 0 ) {
		strcpy( data.ez3num, q );		
	} else if ( memcmp( p, "Tour", strlen("Tour") ) == 0 ) {
		data.tour = atol( q );
	} else if ( memcmp( p, "Preis", strlen("Preis") ) == 0 ) {
		ggdat.gpreis = 100 * atol( q );
		q = strchr( q, '.' );
		if ( q != NULL )
		{
		    *(q+3) = '\0';
		    ggdat.gpreis += atol( q+1 );
		}
		if (ggdat.gpreis < 0) ggdat.gpreis = 0;
		fprintf( fperr, "gpreis %ld\n", ggdat.gpreis );
	} else if ( memcmp( p, "Spreis", strlen("Spreis") ) == 0 ) {
		ggdat.spreis = 100 * atol( q );
		q = strchr( q, '.' );
		if ( q != NULL )
		{
		    *(q+3) = '\0';
		    ggdat.spreis += atol( q+1 );
		}
		if (ggdat.spreis < 0) ggdat.spreis = 0;
		fprintf( fperr, "spreis %ld\n", ggdat.spreis );
	} else if ( memcmp( p, "Gewicht", strlen("Gewicht") ) == 0 ) {
		ggdat.fgew = 1000 * atol( q );
		q = strchr( q, '.' );
		if ( q != NULL )
		{
		    *(q+4) = '\0';
		    ggdat.fgew += atol( q+1 );
		}
		fprintf( fperr, "fgew %ld\n", ggdat.fgew );
	} else if ( memcmp( p, "Tara", strlen("Tara") ) == 0 ) {
		ggdat.tara = 1000 * atol( q );
		q = strchr( q, '.' );
		if ( q != NULL )
		{
		    *(q+4) = '\0';
		    ggdat.tara += atol( q+1 );
		}
		fprintf( fperr, "DDE tara %ld\n", ggdat.tara );
	}	
}

void getfields( char *params )
{
	char parameter[80];
	char *p, *q;
	int sep;
	int len;
	sep = 0xA;
	p = params;
	vorbelegung();
	for (;;)
	{
		q = strchr( p, sep );
		if ( q == NULL )
			break;
		len = (int)(q - p) ;
		memcpy( parameter, p, len );
		memset( parameter + len, '\0', 1 );
		fprintf( fperr, "%s\n", parameter );
		evalparameter( parameter );
		p = q + 1;
	}
	q = strchr( p, '\0' );
	if ( q == NULL )
		return;
	len = (int)(q - p) ;
	memcpy( parameter, p, len );
	memset( parameter + len, '\0', 1 );
	fprintf( fperr, "%s\n", parameter );
	evalparameter( parameter );
}

int zeile( char *p, int sg, int ausrichtung, int nl, char *txt )
{
    if ( sg < 1 )
        return 0;
    if ( ausrichtung == ' ' )
    {
        return sprintf( p, "%s%c", txt, nl );
    }
    if ( strcmp( anwender, "DFW" ) == 0 )
    {
        return sprintf( p, "%s", txt );
    }
    if ( sg < 99 )
    {
        return sprintf( p, "^%02hd;^A%c;%s%c", sg, ausrichtung, txt, nl );
    }
    else  /* SG = 99: reserviert fuer Bizerba Formelinterpreter */
    {
        return sprintf( p, "%s", txt );
    }
}

void formatatx( GGATX_t *ggatx, int ausrichtung, int nl )
{
    char *p;
    int zlen;
    zlen = 0;
    ggatx->atx[0] = '\0';
    p = ggatx->atx;

    if ( pr_ausz_gx.eti_typ == 2 && strcmp( anwender, "DFW" ) != 0 )
    {
        p += zlen;
        zlen = zeile( p, a_kun_gx.sg1, ausrichtung, '\n', a_kun_gx.a_bz3 );

        p += zlen;
        zlen = zeile( p, a_kun_gx.sg2, ausrichtung, '\n', a_kun_gx.a_bz4 );
        pZutaten = p + zlen;
    }

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg1, ausrichtung, nl, pr_ausz_gx.txt1 );
    if ( zlen == 0 )
        return;

    if ( nl == ' ' )
    {
        /*
           101203 GK
           wo kein Zeilentrenner gibts ab Zeile 2 keine Ausrichtung
           und auch keine Schriftgroessensteuerung
         */

        ausrichtung = ' ';
    }

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg2, ausrichtung, nl, pr_ausz_gx.txt2 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg3, ausrichtung, nl, pr_ausz_gx.txt3 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg4, ausrichtung, nl, pr_ausz_gx.txt4 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg5, ausrichtung, nl, pr_ausz_gx.txt5 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg6, ausrichtung, nl, pr_ausz_gx.txt6 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg7, ausrichtung, nl, pr_ausz_gx.txt7 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg8, ausrichtung, nl, pr_ausz_gx.txt8 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg9, ausrichtung, nl, pr_ausz_gx.txt9 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg10, ausrichtung, nl, pr_ausz_gx.txt10 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg11, ausrichtung, nl, pr_ausz_gx.txt11 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg12, ausrichtung, nl, pr_ausz_gx.txt12 );
    if ( zlen == 0 )
        return;
}

void replace( char *txt, char *oldtxt, char *newtxt )
{
    // A + Bold + C --> A + Bnew + C
    char *p, *q, *r;
    int oldlen, newlen, shiftlen;
    p = strstr( txt, oldtxt );
    if ( p == NULL )
        return;
    oldlen = strlen( oldtxt );
    newlen = strlen( newtxt );
    q = p + oldlen;
    r = p + newlen;
    shiftlen = strlen( q );
    memmove( r, q, shiftlen );
    r[shiftlen] = '\0';
    memcpy( p, newtxt, newlen );
}

int make_es( char *txt )
{
    char *p;
    p = txt;

    if ( data.esnum[0] == '\0' )
    {
         txt[0] = '\0';
         return 0;
    }

    /*
    if ( '0' <= data.esnum[0] && data.esnum[0] <= '9' )
    {
        p += sprintf( p, "ES " );
    }
    */

    p += sprintf( p, "%s", data.esnum );

    if ( data.es2num[0] != '\0' )
    {
        p += sprintf( p, ", %s", data.es2num );
    }

    if ( data.es3num[0] != '\0' )
    {
        p += sprintf( p, ", %s", data.es3num );
    }

    return (p - txt);
}

int make_ez( char *txt )
{
    char *p;
    p = txt;

    if ( data.ez1num[0] == '\0' )
    {
         txt[0] = '\0';
         return 0;
    }

    /*
    if ( '0' <= data.ez1num[0] && data.ez1num[0] <= '9' )
    {
        p += sprintf( p, "EZ " );
    }
    */

    p += sprintf( p, "%s", data.ez1num );

    if ( data.ez2num[0] != '\0' )
    {
        p += sprintf( p, ", %s", data.ez2num );
    }

    if ( data.ez3num[0] != '\0' )
    {
        p += sprintf( p, ", %s", data.ez3num );
    }

    return (p - txt);
}

void BauerngutLabelData( void )
{
    int ausrichtung;
    int zeilenvorschub;
	sprintf( ggdat.cts1, "%.0lf", a_kun_gx.ean );
	ggdat.cts1len = (us) strlen( ggdat.cts1 );
	if ( a_kun_gx.ean1 > 0 )
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean1 );
	else
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean );
	ggdat.cts2len = (us) strlen( ggdat.cts2 );

	if ( a_kun_gx.cab > 0 )
	{
		ggcab1.cabnr = a_kun_gx.cab;
	}
	else
	{
		ggcab1.cabnr = 1;
	}	
	if ( a_kun_gx.cab1 > 0 )
	{
		ggcab2.cabnr = a_kun_gx.cab1;
	}
	else
	{
		ggcab2.cabnr = ggcab1.cabnr;
	}
	ggdat.cabnr1 = ggcab1.cabnr;
	ggdat.cabnr2 = ggcab2.cabnr;
	
	ggdat.etin = a_kun_gx.eti_typ;
	ggdat.ets1 = a_kun_gx.eti_sum1;
	ggdat.ets2 = a_kun_gx.eti_sum2;
	
	ggdat.prbe = (a_kun_gx.pr_rech_kz[0] == 'J' || a_kun_gx.pr_rech_kz[0] == 'j') ? 1 : 0 ;

	Query_a_bas();
	
	/*
	if ( KuBaTara[0] == 'J' )
	{
		ggdat.tara = (long)(1000.0 * a_kun_gx.tara);
		fprintf( fperr, "KuBa tara %ld\n", ggdat.tara );
	}
	*/

	ggdat.ampar = a_kun_gx.ampar;
	if (a_kun_gx.gwpar > 0)
	{
	    ggdat.gwpar = a_kun_gx.gwpar;
    }
    else
    {
        ggdat.gwpar = 0xFFFFFFFF;
    }
    ggdat.aart = a_kun_gx.ausz_art;
 	
 	if ( a_kun_gx.sonder_eti > 0 && a_kun_gx.sonder_eti < 10 )
 	{
	    ggdat.sonder_eti_krit = a_kun_gx.sonder_eti;
	}
	else
	{
	    ggdat.sonder_eti_krit = 0;
	}

	ggdat.sum1vwa = 1; /* Stueck */
	ggdat.sum1vw = data.up_ka;

    ggdat.sum2vwa = ggdat.sum1vwa;
    if ( data.charg_hand > 0 )
    {
        ggdat.sum2vw = ggdat.sum1vw;
    }
    else
    {
        ggdat.sum2vw = 0;
    }
    /* 20100429 gk */
    ggdat.sum2vw = data.up_pal;
    /* 20100528 gk */
 	if ( ggdat.sum2vw > 0 )
 	{
 	    ggdat.sum2vwa = 8;
    }
    else
 	{
 	    ggdat.sum2vwa = 1;
    }
	/* Vorgabe Summe 3 in Anzahl Summe1 */
	ggdat.sum3vwa = 8;
	ggdat.sum3vw = data.amenge;
	
	/* Vorgabe Summe 3 in Stueck  */
	/*
	ggdat.sum3vwa = 1;
	ggdat.sum3vw = data.amenge * data.up_ka;
    */

    Query_kun();

    ggdat.zutaten = a_kun_gx.geb_eti[0];

/*  Adressen eti_typ 1 --> ATX1 */
    ggatx1.atxnr = 100000 + a_kun_gx.kopf_text;
    ggdat.atxnr1 = ggatx1.atxnr;
	Query_pr_ausz_gx( a_kun_gx.kopf_text, 1 );
	ausrichtung = frmtxt1[0];
	zeilenvorschub = (frmtxt1[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx1, ausrichtung, zeilenvorschub );
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    if ( ggatx1.atxlen == 0 )
    {
        strcpy( ggatx1.atx, "  " );
    }
    ggatx1.atxlen = (us) strlen(ggatx1.atx);

/*  Bez. 1, 2 + Zutaten eti_typ2 --> ATX2 */
    ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr2 = ggatx2.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 2 );
	ausrichtung = frmtxt2[0];
	zeilenvorschub = (frmtxt2[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx2, ausrichtung, zeilenvorschub );
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
    if ( ggatx2.atxlen == 0 )
    {
        strcpy( ggatx2.atx, "  " );
    }
    ggatx2.atxlen = (us) strlen(ggatx2.atx);

/*  Mengen  eti_typ 3 --> ATX3 */
/*  Mail von C. Roesener. TF3 wurde nicht bisher nicht benutzt. Jetzt sollen Herkunftsdaten hierein.
 *  20111111 GK
 */
/*  Herkunft eti_typ 3 --> ATX7 */
    ggatx3.atxnr = 300000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr3 = ggatx3.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 3 );
	ausrichtung = frmtxt3[0];
	zeilenvorschub = (frmtxt3[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx3, ausrichtung, zeilenvorschub );
	
    if ( make_es( txtNR ) > 0 )
    {
        replace( ggatx3.atx, "##ESNR##", txtNR );
    }
    if ( make_ez( txtNR ) > 0)
    {
        replace( ggatx3.atx, "##EZNR##", txtNR );
    }
    if ( data.partie > 0 )
    {
        sprintf( txtNR, "%ld", data.partie );
        replace( ggatx3.atx, "##IDENTNR##", txtNR );
    }

    ggatx3.atxlen = (us) strlen(ggatx3.atx);
    if ( ggatx3.atxlen == 0 )
    {
        strcpy( ggatx3.atx, "  " );
    }
    ggatx3.atxlen = (us) strlen(ggatx3.atx);
    fprintf( fperr, "ggatx7: len=%d\n%s\n", ggatx3.atxlen, ggatx3.atx );

/*  Zubereitung eti_typ 4 --> ATX4 */
    ggatx4.atxnr = 400000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr4 = ggatx4.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 4 );
	ausrichtung = frmtxt4[0];
	zeilenvorschub = (frmtxt4[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx4, ausrichtung, zeilenvorschub );
    ggatx4.atxlen = (us) strlen(ggatx4.atx);
    if ( ggatx4.atxlen == 0 )
    {
        strcpy( ggatx4.atx, "  " );
    }
    ggatx4.atxlen = (us) strlen(ggatx4.atx);

/*  Sondertext eti_typ 5 --> ATX5 */
    ggatx5.atxnr = 500000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr5 = ggatx5.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 5 );
	ausrichtung = frmtxt5[0];
	zeilenvorschub = (frmtxt5[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx5, ausrichtung, zeilenvorschub );
    ggatx5.atxlen = (us) strlen(ggatx5.atx);
    if ( ggatx5.atxlen == 0 )
    {
        strcpy( ggatx5.atx, "  " );
    }
    ggatx5.atxlen = (us) strlen(ggatx5.atx);

/*  Herkunft --> ATX7 */
/*  Mail von C. Roesener. Herkunft nach TF3 verlagert.
 *  Jetzt kommen hier in TF7 nur 2 Leerzeichen rein.
 *  20111111 GK
 */

    ggatx7.atxnr = 700000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr7 = ggatx7.atxnr;
/*
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 7 );
	ausrichtung = frmtxt7[0];
	zeilenvorschub = (frmtxt7[1] == '-') ? (' ') : ('\n');
	
	formatatx( &ggatx7, ausrichtung, zeilenvorschub );
	
    if ( make_es( txtNR ) > 0 )
    {
        replace( ggatx7.atx, "##ESNR##", txtNR );
    }
    if ( make_ez( txtNR ) > 0)
    {
        replace( ggatx7.atx, "##EZNR##", txtNR );
    }
    if ( data.partie > 0 )
    {
        sprintf( txtNR, "%ld", data.partie );
        replace( ggatx7.atx, "##IDENTNR##", txtNR );
    }

    ggatx7.atxlen = (us) strlen(ggatx7.atx);
    if ( ggatx7.atxlen == 0 )
    {
        strcpy( ggatx7.atx, "  " );
    }
 */
    strcpy( ggatx7.atx, "  " );
    ggatx7.atxlen = (us) strlen(ggatx7.atx);
    fprintf( fperr, "ggatx7: len=%d\n%s\n", ggatx7.atxlen, ggatx7.atx );

/*  Charge --> ATX16 */
    ggatx16.atxnr = 650000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr16 = ggatx16.atxnr;
	// strcpy( ggatx16.atx, data.charge );
	sprintf( ggatx16.atx, "%ld", data.partie );
    ggatx16.atxlen = (us) strlen(ggatx16.atx);
    if ( ggatx16.atxlen == 0 )
    {
        strcpy( ggatx16.atx, "  " );
    }
    ggatx16.atxlen = (us) strlen(ggatx16.atx);

/*  Charge[0..3] --> ATX6 */
/*
    ggatx6.atxnr = 600000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr6 = ggatx6.atxnr;
    data.charge[4] = '\0';
	strcpy( ggatx6.atx, data.charge );
    ggatx6.atxlen = (us) strlen(ggatx6.atx);
    if ( ggatx6.atxlen == 0 )
    {
        strcpy( ggatx6.atx, "  " );
    }
    ggatx6.atxlen = (us) strlen(ggatx6.atx);
*/

/*  MHD-Text eti_typ 11 --> ATX8 */
    ggatx8.atxnr = 800000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr8 = ggatx8.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 11 );
	ausrichtung = frmtxt8[0];
	zeilenvorschub = (frmtxt8[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx8, ausrichtung, zeilenvorschub );
    ggatx8.atxlen = (us) strlen(ggatx8.atx);
    if ( ggatx8.atxlen == 0 )
    {
        strcpy( ggatx8.atx, "  " );
    }
    ggatx8.atxlen = (us) strlen(ggatx8.atx);

/*  Abpackungdatumstext eti_typ 12 --> ATX9 */
    ggatx9.atxnr = 900000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr9 = ggatx9.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 12 );
	ausrichtung = frmtxt9[0];
	zeilenvorschub = (frmtxt9[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx9, ausrichtung, zeilenvorschub );
    ggatx9.atxlen = (us) strlen(ggatx9.atx);
    if ( ggatx9.atxlen == 0 )
    {
        strcpy( ggatx9.atx, "  " );
    }
    ggatx9.atxlen = (us) strlen(ggatx9.atx);

/*  Kundenartikelnummer --> ATX10 */
    ggatx10.atxnr = 950000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr10 = ggatx10.atxnr;
    strcpy( ggatx10.atx, data.kuna );
    ggatx10.atxlen = (us) strlen(ggatx10.atx);
    if ( ggatx10.atxlen == 0 )
    {
        strcpy( ggatx10.atx, "  " );
    }
    ggatx10.atxlen = (us) strlen(ggatx10.atx);

/*  Kundenadresse --> ATX11 */
    ggatx11.atxnr = 0 ;
    ggdat.atxnr11 = ggatx11.atxnr;
    if ( kun.kun_gr1 != 0 )
    {
         sprintf( ggatx11.atx, "%s\n%s\n%s", kun.adr_nam1, kun.str, kun.ort1 );
    }
    else
    {
        strcpy( ggatx11.atx, "  " );
    }
    ggatx11.atxlen = (us) strlen(ggatx11.atx);
    if ( ggatx11.atxlen == 0 )
    {
        strcpy( ggatx11.atx, "  " );
    }
    ggatx11.atxlen = (us) strlen(ggatx11.atx);
}

void BoesingerLabelData( void )
{
    int ausrichtung;
    int zeilenvorschub;
	sprintf( ggdat.cts1, "%.0lf", a_kun_gx.ean );
	ggdat.cts1len = (us) strlen( ggdat.cts1 );
	if ( a_kun_gx.ean1 > 0 )
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean1 );
	else
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean );
	ggdat.cts2len = (us) strlen( ggdat.cts2 );

    /*	cab = 0 kein Barcode. Kann durchaus beabsichtigt sein. */
    ggcab1.cabnr = a_kun_gx.cab;
	if ( a_kun_gx.cab1 > 0 )
	{
		ggcab2.cabnr = a_kun_gx.cab1;
	}
	else
	{
		ggcab2.cabnr = ggcab1.cabnr;
	}
	ggdat.cabnr1 = ggcab1.cabnr;
	ggdat.cabnr2 = ggcab2.cabnr;
	ggdat.cabnr3 = a_kun_gx.cab_nve1;
	ggdat.cabnr4 = a_kun_gx.cab_nve2;

	ggdat.etin = a_kun_gx.eti_typ;
	ggdat.ets1 = a_kun_gx.eti_sum1;
	ggdat.ets2 = a_kun_gx.eti_sum2;
	ggdat.eti_nve1 = a_kun_gx.eti_nve1;
	ggdat.eti_nve2 = a_kun_gx.eti_nve2;
	
	ggdat.prbe = (a_kun_gx.pr_rech_kz[0] == 'J' || a_kun_gx.pr_rech_kz[0] == 'j') ? 1 : 0 ;
	
	/*
	if ( KuBaTara[0] == 'J' )
	{
		ggdat.tara = (long)(1000.0 * a_kun_gx.tara);
		fprintf( fperr, "KuBa tara %ld\n", ggdat.tara );
	}
	*/
	
    ggdat.aart = a_kun_gx.ausz_art;
 	
 	/* Vorgabe Summe 1 */
	if ( a_kun_gx.geb_fill == 2 )
	{
	    /* Gewichtsvorgabe */
	    ggdat.sum1vwa = 2;
	    ggdat.sum1vw = a_kun_gx.geb_anz;
	}
	else
	{
	    /* Stueckvorgabe */
	    ggdat.sum1vwa = 1;
	    ggdat.sum1vw = a_kun_gx.geb_anz / 1000;
	}
	
 	/* Vorgabe Summe 2 */
	if ( a_kun_gx.pal_fill == 5 )
	{
	    /* Kartonvorgabe */
 	    ggdat.sum2vwa = 8;
	    ggdat.sum2vw = a_kun_gx.pal_anz / 1000;
	}
	else if ( a_kun_gx.pal_fill == 2 )
	{
	    /* Gewichtsvorgabe */
 	    ggdat.sum2vwa = 2;
	    ggdat.sum2vw = a_kun_gx.pal_anz;
	}
 	else
	{
	    /* Stueckvorgabe */
 	    ggdat.sum2vwa = 1;
	    ggdat.sum2vw = a_kun_gx.pal_anz / 1000;
	}

    /* Boesinger: Ohne Vorgabe Summe 3, SUMMENVORWAHL[3] = 'N' */
	
/*  Adressen eti_typ 1 --> ATX1 */
    ggatx1.atxnr = 100000 + a_kun_gx.text_nr2;
    ggdat.atxnr1 = ggatx1.atxnr;
	Query_pr_ausz_gx( a_kun_gx.kopf_text, 1 );
	ausrichtung = frmtxt1[0];
	zeilenvorschub = (frmtxt1[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx1, ausrichtung, zeilenvorschub );
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    if ( ggatx1.atxlen == 0 )
    {
        strcpy( ggatx1.atx, "  " );
    }
    ggatx1.atxlen = (us) strlen(ggatx1.atx);

/*  Bez. 1 --> ATX2 */
    ggatx2.atxnr = 200000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr2 = ggatx2.atxnr;
    zeile( ggatx2.atx, a_kun_gx.sg1, 'c', '\n', a_kun_gx.a_bz3 );
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
    if ( ggatx2.atxlen == 0 )
    {
        strcpy( ggatx2.atx, "  " );
    }
    ggatx2.atxlen = (us) strlen(ggatx2.atx);

/*  Bez. 2 --> ATX3 */
    ggatx3.atxnr = 300000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr3 = ggatx3.atxnr;
    zeile( ggatx3.atx, a_kun_gx.sg2, 'c', '\n', a_kun_gx.a_bz4 );
    ggatx3.atxlen = (us) strlen(ggatx3.atx);
    if ( ggatx3.atxlen == 0 )
    {
        strcpy( ggatx3.atx, "  " );
    }
    ggatx3.atxlen = (us) strlen(ggatx3.atx);

/*  Zutaten eti_typ 1--> ATX4 */
    ggatx4.atxnr = 400000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr4 = ggatx4.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 1 );
	ausrichtung = frmtxt4[0];
	zeilenvorschub = (frmtxt4[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx4, ausrichtung, zeilenvorschub );
    ggatx4.atxlen = (us) strlen(ggatx4.atx);
    if ( ggatx4.atxlen == 0 )
    {
        strcpy( ggatx4.atx, "  " );
    }
    ggatx4.atxlen = (us) strlen(ggatx4.atx);

/*  MHD Text eti_typ 5 --> ATX5 */
    ggatx5.atxnr = 500000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr5 = ggatx5.atxnr;
	Query_pr_ausz_gx( a_kun_gx.mhd_text, 1 );
	ausrichtung = frmtxt5[0];
	zeilenvorschub = (frmtxt5[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx5, ausrichtung, zeilenvorschub );
    ggatx5.atxlen = (us) strlen(ggatx5.atx);
    if ( ggatx5.atxlen == 0 )
    {
        strcpy( ggatx5.atx, "  " );
    }
    ggatx5.atxlen = (us) strlen(ggatx5.atx);

/*  Kunden-Artikel-Nr --> ATX6 */
    ggatx6.atxnr = 600000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr6 = ggatx6.atxnr;
    zeile( ggatx6.atx, a_kun_gx.sg3, 'c', '\n', a_kun_gx.a_kun );
    ggatx6.atxlen = (us) strlen(ggatx6.atx);
    if ( ggatx6.atxlen == 0 )
    {
        strcpy( ggatx6.atx, "  " );
    }
    ggatx6.atxlen = (us) strlen(ggatx6.atx);

/*  Artikel-Nr --> ATX7 */
    ggatx7.atxnr = 700000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr7 = ggatx7.atxnr;
    sprintf( a_kun_gx.a_kun, "%ld", a_kun_gx.a );
    zeile( ggatx7.atx, a_kun_gx.sg3, 'c', '\n', a_kun_gx.a_kun );
    ggatx7.atxlen = (us) strlen(ggatx7.atx);
    if ( ggatx7.atxlen == 0 )
    {
        strcpy( ggatx7.atx, "  " );
    }
    ggatx7.atxlen = (us) strlen(ggatx7.atx);
}

void LOHMANNLabelData( void )
{
    /* PLU wird aus GX DB aufgeschaltet */
    kun.kun = a_kun_gx.kun;
    kun.mdn = 1;
    kun.fil = 0;
    Query_kun();
    ggdat.kdnr = kun.stat_kun;

    ggdat.sum1vw = data.amenge;

    a_kun.a = a_kun_gx.a;
    a_kun.kun = a_kun_gx.kun;

    Query_a_kun();
    if ( a_kun.a_kun[0] > '0' )
    {
        ggdat.plunr = atol( a_kun.a_kun );
    }

    TimeMsgLine("LOHMANN PLU: %ld KUNDE %ld CHARGE %ld STUECK %ld\n",
        ggdat.plunr, ggdat.kdnr, ggdat.chargennr, ggdat.sum1vw );
    return;
}

void OKLELabelData( void )
{
    /* PLU wird aus GX DB aufgeschaltet */
    kun.kun = a_kun_gx.kun;
    kun.mdn = 1;
    kun.fil = 0;
    Query_kun();
    ggdat.kdnr = kun.stat_kun;
    ggdat.chargennr = atol( data.charge );
 	fprintf( fperr, "OKLELabelData Chargenr: %ld\n", ggdat.chargennr );
    if ( ggdat.chargennr < 0 || ggdat.chargennr > 999999 )
    {
        ggdat.chargennr = 0;
    }
    Query_a_bas();
    if ( data.charg_hand == 0 )
    {
        ggdat.chargennr = 0;  // 02.04.2013 GK
    }

    ggdat.sum1vw = data.amenge;

    TimeMsgLine("OKLE PLU: %ld KUNDE %ld CHARGE %ld STUECK %ld\n",
        ggdat.plunr, ggdat.kdnr, ggdat.chargennr, ggdat.sum1vw );
    return;
}

void DietzLabelData( void )
{
    int ausrichtung;
    int zeilenvorschub;
	sprintf( ggdat.cts1, "%.0lf", a_kun_gx.ean );
	ggdat.cts1len = (us) strlen( ggdat.cts1 );
	if ( a_kun_gx.ean1 > 0 )
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean1 );
	else
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean );
	ggdat.cts2len = (us) strlen( ggdat.cts2 );

    /*	cab = 0 kein Barcode. Kann durchaus beabsichtigt sein. */
    ggcab1.cabnr = a_kun_gx.cab;
	if ( a_kun_gx.cab1 > 0 )
	{
		ggcab2.cabnr = a_kun_gx.cab1;
	}
	else
	{
		ggcab2.cabnr = ggcab1.cabnr;
	}
	ggdat.cabnr1 = ggcab1.cabnr;
	ggdat.cabnr2 = ggcab2.cabnr;
	ggdat.cabnr3 = a_kun_gx.cab_nve1;
	ggdat.cabnr4 = a_kun_gx.cab_nve2;

	ggdat.etin = a_kun_gx.eti_typ;
    if ( a_kun_gx.eti_typ < 1 )
    {
        /* Bizerba Standard Etikett 0 (68mm) */
        ggdat.etin = - 8191;
    }
    if ( a_kun_gx.eti_typ > 999 )
    {
        /* Bizerba Standard Etikettten */
        ggdat.etin = a_kun_gx.eti_typ - 1000 - 8191;
    }
	ggdat.ets1 = a_kun_gx.eti_sum1;
	ggdat.ets2 = a_kun_gx.eti_sum2;
	ggdat.eti_nve1 = a_kun_gx.eti_nve1;
	ggdat.eti_nve2 = a_kun_gx.eti_nve2;
	
	ggdat.prbe = (a_kun_gx.pr_rech_kz[0] == 'J' || a_kun_gx.pr_rech_kz[0] == 'j') ? 1 : 0 ;
	
    ggdat.aart = a_kun_gx.ausz_art;

    // Query_a_bas();

    Query_kun_geb();

	ggdat.tara = (long)(1000.0 * kun_geb.tara);	
	fprintf( fperr, "Tara = %ld g\n", ggdat.tara );
	
 	/* Vorgabe Summe 1 */
	if ( kun_geb.geb_fill == 2 )
	{
	    /* Gewichtsvorgabe */
	    ggdat.sum1vwa = 2;
	    ggdat.sum1vw = kun_geb.geb_anz;
	}
	else
	{
	    /* Stueckvorgabe */
	    ggdat.sum1vwa = 1;
	    ggdat.sum1vw = kun_geb.geb_anz / 1000;
	}
	
 	/* Vorgabe Summe 2 */
	if ( kun_geb.pal_fill == 5 )
	{
	    /* Kartonvorgabe */
 	    ggdat.sum2vwa = 8;
	    ggdat.sum2vw = kun_geb.pal_anz / 1000;
	}
	else if ( kun_geb.pal_fill == 2 )
	{
	    /* Gewichtsvorgabe */
 	    ggdat.sum2vwa = 2;
	    ggdat.sum2vw = kun_geb.pal_anz;
	}
 	else
	{
	    /* Stueckvorgabe */
 	    ggdat.sum2vwa = 1;
	    ggdat.sum2vw = kun_geb.pal_anz / 1000;
	}
	
	fprintf( fperr, "Vorgabe S1: (ME %d) %ld\n", ggdat.sum1vwa, ggdat.sum1vw );
	fprintf( fperr, "Vorgabe S2: (ME %d) %ld\n", ggdat.sum2vwa, ggdat.sum2vw );
      	
/*  Adressen eti_typ 1 --> ATX1 */
    ggatx1.atxnr = 100000 + a_kun_gx.text_nr2;
    ggdat.atxnr1 = ggatx1.atxnr;
	Query_pr_ausz_gx( a_kun_gx.kopf_text, 1 );
	ausrichtung = frmtxt1[0];
	zeilenvorschub = (frmtxt1[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx1, ausrichtung, zeilenvorschub );
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    if ( ggatx1.atxlen == 0 )
    {
        strcpy( ggatx1.atx, "  " );
    }
    ggatx1.atxlen = (us) strlen(ggatx1.atx);

/*  Bez. 1, 2 + Zutaten eti_typ2 --> ATX2, ATX3, ATX5 */
    ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr2 = ggatx2.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 2 );
	ausrichtung = frmtxt2[0];
	zeilenvorschub = (frmtxt2[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx2, ausrichtung, zeilenvorschub );
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
    if ( ggatx2.atxlen == 0 )
    {
        strcpy( ggatx2.atx, "  " );
    }
    ggatx2.atxlen = (us) strlen(ggatx2.atx);

    ggdat.chargennr = atol( data.charge );
    if ( ggdat.chargennr < 0 || ggdat.chargennr > 999999 )
    {
        ggdat.chargennr = 0;
    }
    else
    {
        strcat( ggatx2.atx, "\nIdent-Nr. " );
        strcat( ggatx2.atx, data.charge );
        ggatx2.atxlen = (us) strlen(ggatx2.atx);
    }
    if ( strcmp( anwender, "Dietz" ) == 0 )
    {
        memcpy( &ggatx3, &ggatx2, sizeof( ggatx2 ) );
        memcpy( &ggatx5, &ggatx2, sizeof( ggatx2 ) );
    }

/*  MHD Text eti_typ 3 --> ATX4, DTX1, DTX2 */
    ggatx4.atxnr = 400000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr4 = ggatx4.atxnr;
	Query_pr_ausz_gx( a_kun_gx.mhd_text, 3 );
	ausrichtung = frmtxt4[0];
	zeilenvorschub = (frmtxt4[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx4, ausrichtung, zeilenvorschub );
    ggatx4.atxlen = (us) strlen(ggatx4.atx);
    if ( ggatx4.atxlen == 0 )
    {
        strcpy( ggatx4.atx, "  " );
    }
    ggatx4.atxlen = (us) strlen(ggatx4.atx);

}

void DFWLabelData( void )
{
    int ausrichtung;
    int zeilenvorschub;
	sprintf( ggdat.cts1, "%.0lf", a_kun_gx.ean );
	ggdat.cts1len = (us) strlen( ggdat.cts1 );
	if ( a_kun_gx.ean1 > 0 )
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean1 );
	else
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean );
	ggdat.cts2len = (us) strlen( ggdat.cts2 );

    /*	cab = 0 kein Barcode. Kann durchaus beabsichtigt sein. */
    ggcab1.cabnr = a_kun_gx.cab;
	if ( a_kun_gx.cab1 > 0 )
	{
		ggcab2.cabnr = a_kun_gx.cab1;
	}
	else
	{
		ggcab2.cabnr = ggcab1.cabnr;
	}
	ggdat.cabnr1 = ggcab1.cabnr;
	ggdat.cabnr2 = ggcab2.cabnr;
	ggdat.cabnr3 = a_kun_gx.cab_nve1;
	ggdat.cabnr4 = a_kun_gx.cab_nve2;
	fprintf( fperr, "CAB1, EAN1 = %d, %s\n", ggdat.cabnr1, ggdat.cts1 );
	fprintf( fperr, "CAB1, EAN1 = %d, %s\n", ggdat.cabnr2, ggdat.cts2 );

	ggdat.etin = a_kun_gx.eti_typ;
    if ( a_kun_gx.eti_typ < 1 )
    {
        /* Bizerba Standard Etikett 0 (68mm) */
        ggdat.etin = - 8191;
    }
    if ( a_kun_gx.eti_typ > 999 )
    {
        /* Bizerba Standard Etikettten */
        ggdat.etin = a_kun_gx.eti_typ - 1000 - 8191;
    }
	ggdat.ets1 = a_kun_gx.eti_sum1;
	ggdat.ets2 = a_kun_gx.eti_sum2;
	ggdat.eti_nve1 = a_kun_gx.eti_nve1;
	ggdat.eti_nve2 = a_kun_gx.eti_nve2;
	
	ggdat.prbe = (a_kun_gx.pr_rech_kz[0] == 'J' || a_kun_gx.pr_rech_kz[0] == 'j') ? 1 : 0 ;
	
    ggdat.aart = a_kun_gx.ausz_art;
    // Espera hat aart 0 - 5, Bizerba nur 0 - 3
    // Handling entspr Mail von H. Mauer vom 21.01.09
    if ( ggdat.aart == 4 ) ggdat.aart = 2;
    if ( ggdat.aart == 5 ) ggdat.aart = 3;


    // wegen Espera
	ggdat.sonder_eti_krit = (us) a_kun_gx.ampar;
	if ( ggdat.sonder_eti_krit < 1 ) ggdat.sonder_eti_krit = 3;
	if ( ggdat.sonder_eti_krit > 3 ) ggdat.sonder_eti_krit = 3;

    // 15.12.2008 GK Absprache mit Herrn Mauer, Verwechselung nicht sehr schoen
	ggdat.ampar = a_kun_gx.sonder_eti;

    Query_a_bas();
	fprintf( fperr, "BW,EW,KH,FT = %s, %d g,%d g,%d g\n", data.brennwert,data.eiweiss,data.kh,data.fett );

    // Query_kun_geb();

    ggdat.haltbar1 = a_kun_gx.hbk_ztr;
	ggdat.tara = (long)(1000.0 * a_kun_gx.tara);
	ggdat.fgew = (long)(1000.0 * a_kun_gx.inh);
	if ( ggdat.tara < 0L || ggdat.tara > 999999L ) ggdat.tara = 0;	
	if ( ggdat.fgew < 0L || ggdat.fgew > 999999L ) ggdat.fgew = 0;	
	fprintf( fperr, "Tara = %ld g Festgew = %ld g\n", ggdat.tara, ggdat.fgew );
		
 	/* Vorgabe Summe 1  immer Stueck */
    ggdat.sum1vwa = 1;
	ggdat.sum1vw = a_kun_gx.geb_anz / 1000;
	if ( ggdat.sum1vw < 0L || ggdat.sum1vw > 999L ) ggdat.sum1vw = 0;	
	
 	/* Vorgabe Summe 2  immer Kartons */
 	ggdat.sum2vwa = 8;
	ggdat.sum2vw = a_kun_gx.pal_anz / 1000;
	if ( ggdat.sum2vw < 0L || ggdat.sum2vw > 999L ) ggdat.sum2vw = 0;	
	
	fprintf( fperr, "Vorgabe S1: (ME %d) %ld\n", ggdat.sum1vwa, ggdat.sum1vw );
	fprintf( fperr, "Vorgabe S2: (ME %d) %ld\n", ggdat.sum2vwa, ggdat.sum2vw );
      	
/*  Adressen eti_typ 2 --> ATX1 */
    ggatx1.atxnr = 100000 + a_kun_gx.text_nr2;
    ggdat.atxnr1 = ggatx1.atxnr;
	Query_pr_ausz_gx( a_kun_gx.kopf_text, 2 );
	ausrichtung = frmtxt1[0];
	zeilenvorschub = (frmtxt1[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx1, ausrichtung, zeilenvorschub );
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    if ( ggatx1.atxlen == 0 )
    {
        strcpy( ggatx1.atx, "  " );
    }
    ggatx1.atxlen = (us) strlen(ggatx1.atx);

/*  Bez. 1, 2 --> ATX2 */
    ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr2 = ggatx2.atxnr;
    // Mauer 19.01.09 sprintf( ggatx2.atx, "%s\\n%s\\n", a_kun_gx.a_bz3, a_kun_gx.a_bz4 );
    sprintf( ggatx2.atx, "%s\\n", a_kun_gx.a_bz3 );
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
    if ( ggatx2.atxlen == 0 )
    {
        strcpy( ggatx2.atx, "  " );
    }
    ggatx2.atxlen = (us) strlen(ggatx2.atx);

/*  MHD Text eti_typ 2 --> ATX3 */
    ggatx3.atxnr = 400000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr3 = ggatx3.atxnr;
	Query_pr_ausz_gx( a_kun_gx.mhd_text, 2 );
	ausrichtung = frmtxt3[0];
	zeilenvorschub = (frmtxt3[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx3, ausrichtung, zeilenvorschub );
    ggatx3.atxlen = (us) strlen(ggatx3.atx);
    if ( ggatx3.atxlen == 0 )
    {
        strcpy( ggatx3.atx, "  " );
    }
    ggatx3.atxlen = (us) strlen(ggatx3.atx);

/*  Zutaten eti_typ2 --> ATX4 */
    ggatx4.atxnr = 400000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr4 = ggatx4.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 2 );
	ausrichtung = frmtxt2[0];
	zeilenvorschub = (frmtxt2[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx4, ausrichtung, zeilenvorschub );
    ggatx4.atxlen = (us) strlen(ggatx4.atx);
    if ( ggatx4.atxlen == 0 )
    {
        strcpy( ggatx4.atx, "  " );
    }
    ggatx4.atxlen = (us) strlen(ggatx4.atx);

/*  Freitext1 eti_typ2 --> ATX5 */
    ggatx5.atxnr = 500000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr5 = ggatx5.atxnr;
	Query_pr_ausz_gx( a_kun_gx.freitext1, 2 );
	ausrichtung = frmtxt2[0];
	zeilenvorschub = (frmtxt2[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx5, ausrichtung, zeilenvorschub );
    ggatx5.atxlen = (us) strlen(ggatx5.atx);
    if ( ggatx5.atxlen == 0 )
    {
        strcpy( ggatx5.atx, "  " );
    }
    ggatx5.atxlen = (us) strlen(ggatx5.atx);

/*  Fett --> ATX17 */
    if ( data.fett > 0 )
    {
        wsprintf( ggatx17.atx, "%d", data.fett );
    }
    else
    {
        wsprintf( ggatx17.atx, "  " );
    }
    ggatx17.atxlen = (us) strlen(ggatx17.atx);

/*  Kohlenhydrate --> ATX18 */
    if ( data.kh > 0 )
    {
        wsprintf( ggatx18.atx, "%d", data.kh );
    }
    else
    {
        wsprintf( ggatx18.atx, "  " );
    }
    ggatx18.atxlen = (us) strlen(ggatx18.atx);

/*  Eiweiss --> ATX19 */
    if ( data.eiweiss > 0 )
    {
        wsprintf( ggatx19.atx, "%d", data.eiweiss );
    }
    else
    {
        wsprintf( ggatx19.atx, "  " );
    }
    ggatx19.atxlen = (us) strlen(ggatx19.atx);

/*  Brennwert --> ATX20 */
    wsprintf( ggatx20.atx, "%s", data.brennwert );
    ggatx20.atxlen = (us) strlen(ggatx20.atx);

}

void KUEBLERLabelData( int fehlerfall )
{
    char *p;
    // int ausrichtung;
    // int zeilenvorschub;

    // CTS1
    if ( a_kun_gx.ean > 0 )
    {
	    sprintf( ggdat.cts1, "%.0lf", a_kun_gx.ean );
	}
	else
	{
	    if ( a_kun_gx.a < 10000 )
	    {
	        sprintf( ggdat.cts1, "21%4.0lf000000", a_kun_gx.a );
	    }
	    else
	    {
	        sprintf( ggdat.cts1, "21%.0lf000000", a_kun_gx.a );
	    }
	}
	ggdat.cts1len = (us) strlen( ggdat.cts1 );
	
	// CTS2
	if ( a_kun_gx.ean1 > 0 )
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean1 );
	else
		strcpy( ggdat.cts2, ggdat.cts2 );
	ggdat.cts2len = (us) strlen( ggdat.cts2 );

    // CAB1
    ggcab1.cabnr = a_kun_gx.cab;
    if ( ggcab1.cabnr == 0 ) ggcab1.cabnr = 1;

    // CAB2
	ggcab2.cabnr = ggcab1.cabnr;
	if ( a_kun_gx.cab1 > 0 ) ggcab2.cabnr = a_kun_gx.cab1;

	ggdat.cabnr1 = ggcab1.cabnr;
	ggdat.cabnr2 = ggcab2.cabnr;
	ggdat.cabnr3 = a_kun_gx.cab_nve1;
	ggdat.cabnr4 = a_kun_gx.cab_nve2;
	fprintf( fperr, "CAB1, EAN1 = %d, %s\n", ggdat.cabnr1, ggdat.cts1 );
	fprintf( fperr, "CAB2, EAN2 = %d, %s\n", ggdat.cabnr2, ggdat.cts2 );

	ggdat.etin = a_kun_gx.eti_typ;
    if ( a_kun_gx.eti_typ < 1 )
    {
        /* Bizerba Standard Etikett 0 (68mm) */
        ggdat.etin = - 8191;
    }
    if ( a_kun_gx.eti_typ > 999 )
    {
        /* Bizerba Standard Etikettten */
        ggdat.etin = a_kun_gx.eti_typ - 1000 - 8191;
    }
	ggdat.ets1 = a_kun_gx.eti_sum1;
	ggdat.ets2 = a_kun_gx.eti_sum2;
	ggdat.eti_nve1 = a_kun_gx.eti_nve1;
	ggdat.eti_nve2 = a_kun_gx.eti_nve2;
	
	ggdat.prbe = (a_kun_gx.pr_rech_kz[0] == 'J' || a_kun_gx.pr_rech_kz[0] == 'j') ? 1 : 0 ;
	
    ggdat.aart = a_kun_gx.ausz_art;

    Query_a_bas();
	fprintf( fperr, "BW,EW,KH,FT = %s, %4.2f g, %4.2f g, %4.2f g\n",
	         data.brennwert, data.eiweiss, data.kh, data.fett );

    Query_a_bas_erw();
	fprintf( fperr, "%s %s: GsFs = %4.2f g, Zucker = %4.2f g\n",
	    a_bas_erw.pp_a_bz1, a_bas_erw.pp_a_bz2, a_bas_erw.davonfett, a_bas_erw.davonzucker );

    kun.kun = a_kun_gx.kun;
    kun.mdn = a_kun_gx.mdn;
    kun.fil = 0;
    Query_kun();

    /* kommt per DDE
    if ( fehlerfall )
    {
        ggdat.haltbar1 = data.hbk_ztr;
 	    ggdat.tara = (long)(1000.0 * data.tara);
	    ggdat.fgew = (long)(1000.0 * data.inh);
    }
    else
    {
        ggdat.haltbar1 = a_kun_gx.hbk_ztr;
 	    ggdat.tara = (long)(1000.0 * a_kun_gx.tara);
	    ggdat.fgew = (long)(1000.0 * a_kun_gx.inh);
    }
	if ( ggdat.tara < 0L || ggdat.tara > 999999L ) ggdat.tara = 0;	
	if ( ggdat.fgew < 0L || ggdat.fgew > 999999L ) ggdat.fgew = 0;	
	*/
	fprintf( fperr, "Tara = %ld g Festgew = %ld g\n", ggdat.tara, ggdat.fgew );
		
 	/* Vorgabe Summe 1  immer Stueck */
    ggdat.sum1vwa = 1;
	ggdat.sum1vw = a_kun_gx.geb_anz / 1000;
	if ( ggdat.sum1vw < 0L || ggdat.sum1vw > 999L ) ggdat.sum1vw = 0;	
	
 	/* Vorgabe Summe 2  immer Kartons */
 	ggdat.sum2vwa = 8;
	ggdat.sum2vw = a_kun_gx.pal_anz / 1000;
	if ( ggdat.sum2vw < 0L || ggdat.sum2vw > 999L ) ggdat.sum2vw = 0;	
	
	fprintf( fperr, "Vorgabe S1: (ME %d) %ld\n", ggdat.sum1vwa, ggdat.sum1vw );
	fprintf( fperr, "Vorgabe S2: (ME %d) %ld\n", ggdat.sum2vwa, ggdat.sum2vw );
      	
/*  Adresse --> ATX1 */
    // ggatx1.atxnr = 100000;
    // ggdat.atxnr1 = ggatx1.atxnr;
    sprintf( ggatx1.atx, "%s %s@0A%s . %s %s",
        kun.adr_nam1, kun.adr_nam2, kun.str, kun.plz, kun.ort1 );
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    if ( ggatx1.atxlen == 0 )
    {
        strcpy( ggatx1.atx, "  " );
    }
    ggatx1.atxlen = (us) strlen(ggatx1.atx);

/*  Bez. 1, 2, Allergene, Zutaten --> ATX2 */
    // ggatx2.atxnr = 200000;
    // ggdat.atxnr2 = ggatx2.atxnr;
    p = ggatx2.atx;
    p += sprintf( p,"^12;%s@0A^8%s@0A", a_bas_erw.pp_a_bz1, a_bas_erw.pp_a_bz2 );
    /*
    if ( a_bas_erw.pp_a_bz1[0] > ' ' )
    {
        p += sprintf( p,"^12;%s@0A^8%s@0A", a_bas_erw.pp_a_bz1, a_bas_erw.pp_a_bz2 );
    }
    else
    {
        p += sprintf( p,"^12;%s@0A^8%s@0A", data.a_bz1, data.a_bz2 );
    }
    */
    p += sprintf( p,"^6%s@0A%s@0A", allergene(), a_bas_erw.zutat );
    p += sprintf( p, "Hergestellt ohne Geschmacksverstärker u. ohne Farbstoffe." );
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
    if ( ggatx2.atxlen == 0 )
    {
        strcpy( ggatx2.atx, "  " );
    }
    ggatx2.atxlen = (us) strlen(ggatx2.atx);

/*  MHD Text --> ATX7 */
    if ( ! fehlerfall && a_kun_gx.mhd_text > 0 )
    {
	    Query_pr_ausz_gx( a_kun_gx.mhd_text, 2 );
        sprintf( ggatx7.atx, "%s %s", pr_ausz_gx.txt1, pr_ausz_gx.txt2 );
	}
    else
    {
        sprintf( ggatx7.atx, "%s", a_bas_erw.lgr_tmpr );
    }
    ggatx7.atxlen = (us) strlen(ggatx7.atx);
    if ( ggatx7.atxlen == 0 )
    {
        strcpy( ggatx7.atx, "  " );
    }
    ggatx7.atxlen = (us) strlen(ggatx7.atx);

/*  Freitext1 eti_typ2 --> ATX5 */
    /*
    ggatx5.atxnr = 500000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr5 = ggatx5.atxnr;
	Query_pr_ausz_gx( a_kun_gx.freitext1, 2 );
	ausrichtung = frmtxt2[0];
	zeilenvorschub = (frmtxt2[1] == '-') ? (' ') : ('\n');
	formatatx( &ggatx5, ausrichtung, zeilenvorschub );
    ggatx5.atxlen = (us) strlen(ggatx5.atx);
    if ( ggatx5.atxlen == 0 )
    {
        strcpy( ggatx5.atx, "  " );
    }
    ggatx5.atxlen = (us) strlen(ggatx5.atx);
    */

/*  Fett --> ATX12 */
    if ( data.fett > 0 )
    {
        sprintf( ggatx12.atx, "%4.2f", data.fett );
        substitutechar( ggatx12.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx12.atx, "  " );
    }
    ggatx12.atxlen = (us) strlen(ggatx12.atx);

/*  Kohlenhydrate --> ATX13 */
    if ( data.kh > 0 )
    {
        sprintf( ggatx13.atx, "%4.2f", data.kh );
        substitutechar( ggatx13.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx13.atx, "  " );
    }
    ggatx13.atxlen = (us) strlen(ggatx13.atx);

/*  Eiweiss --> ATX14 */
    if ( data.eiweiss > 0 )
    {
        sprintf( ggatx14.atx, "%4.2f", data.eiweiss );
        substitutechar( ggatx14.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx14.atx, "  " );
    }
    ggatx14.atxlen = (us) strlen(ggatx14.atx);

/*  Brennwert --> ATX15 */
    wsprintf( ggatx15.atx, "%s", data.brennwert );
    ggatx15.atxlen = (us) strlen(ggatx15.atx);

/*  Gesaettigte Fettsaeuren --> ATX16 */
    if ( a_bas_erw.davonfett > 0 )
    {
        sprintf( ggatx16.atx, "%4.2f", a_bas_erw.davonfett );
        substitutechar( ggatx16.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx16.atx, "  " );
    }
    ggatx16.atxlen = (us) strlen(ggatx16.atx);

/*  Zucker --> ATX17 */
    if ( a_bas_erw.davonzucker > 0 )
    {
        sprintf( ggatx17.atx, "%4.2f", a_bas_erw.davonzucker );
        substitutechar( ggatx17.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx17.atx, "  " );
    }
    ggatx17.atxlen = (us) strlen(ggatx17.atx);

/*  Salz --> ATX18 */
    if ( a_bas_erw.davonzucker > 0 )
    {
        sprintf( ggatx18.atx, "%4.2f", a_bas_erw.davonzucker );
        substitutechar( ggatx18.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx18.atx, "  " );
    }
    ggatx18.atxlen = (us) strlen(ggatx18.atx);

}

void getLabelData( void )
{
	init_a_kun_gx();
    if ( strcmp( anwender, "OKLE" ) == 0 )
    {
        OKLELabelData();
        return;
    }
    else if ( strcmp( anwender, "LOHMANN" ) == 0 )
    {
        LOHMANNLabelData();
        return;
    }
 	if ( a_kun_gx.kun == 0 )
 	{
	    fehlerfall = ! Query_a_kun_gx_ab();	
	    /* 20051220 GK */
	    if ( fehlerfall != 0  )
	        {
	            strcpy( a_kun_gx.kun_bran2, "0" );
	            fehlerfall = ! Query_a_kun_gx_ab();
	        }
	}
	else
 	{
 	    /* Bauerngut: Wenn kun != 0 uebergeben wurde, dann muss es auch ein Kundenetikett geben.
 	     * Und Kuebler!
 	     */
	    fehlerfall = ! Query_a_kun_gx_ak();
	    if ( strcmp( anwender, "Boesinger" ) == 0
	         || strcmp( anwender, "Dietz" ) == 0
	         || strcmp( anwender, "DFW" ) == 0 )
	    {
	    	if ( fehlerfall != 0  )
	        {
	            fehlerfall = ! Query_a_kun_gx_ab();
	            /* 20051220 GK */
	            if ( fehlerfall != 0  )
	            {
	                strcpy( a_kun_gx.kun_bran2, "0" );
	                fehlerfall = ! Query_a_kun_gx_ab();
	            }
	        }
	    }
	}
	
	if (strcmp( anwender, "KUEBLER" ) == 0 )
	{
	    // Kuebler: a_kun_gx Satz muss nicht notwendig da sein.
	    KUEBLERLabelData( fehlerfall );
	    return;
	}

	if ( fehlerfall != 0  )
	{
        /*  Fehlermeldung --> ATX2 */
        ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
        ggdat.atxnr2 = ggatx2.atxnr;
        sprintf( ggatx2.atx, "^14;^Ac;FEHLER\nArtikel %.0ld ohne Daten\n", (long) a_kun_gx.a );
        ggatx2.atxlen = (us) strlen(ggatx2.atx);
		TimeMsgLine( "FEHLER\nArtikel %.0ld ohne Daten\n", (long) a_kun_gx.a );
	}
	else if ( ggdat.etin == 0 )
	{
        /*  Fehlermeldung --> ATX2 */
        ggdat.etin = 1;
        ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
        ggdat.atxnr2 = ggatx2.atxnr;
        sprintf( ggatx2.atx, "^14;^Ac;FEHLER\nArtikel %.0ld ohne Layout\n", (long) a_kun_gx.a );
        ggatx2.atxlen = (us) strlen(ggatx2.atx);
		TimeMsgLine( "FEHLER\nArtikel %.0ld ohne Layout\n", (long) a_kun_gx.a );
	}
	else if (strcmp( anwender, "Boesinger" ) == 0 )
	{
	    BoesingerLabelData();
	}
	else if (strcmp( anwender, "Dietz" ) == 0 )
	{
	    DietzLabelData();
	}
	else if (strcmp( anwender, "DFW" ) == 0 )
	{
	    DFWLabelData();
	}
	else /* Bauerngut */
	{
        BauerngutLabelData();
    }
}

int getKunData( void )
{
	nArtikel = 0;
	
	strcpy( a_kun_gx.kun_bran2, "0" );

	if ( fetch_a_kun_gx_kb() == 0 )
	    return 0;
    Query_a_bas();
    paz_ipr.a = a_kun_gx.a;
    paz_ipr.bran_kun = a_kun_gx.kun;
    if ( Query_paz_ipr() == 0 )
    {
        kun.kun = a_kun_gx.kun;
        Query_kun();
        paz_ipr.bran_kun = atol( kun.kun_bran2 );
        if ( Query_paz_ipr() == 0 )
        {
            paz_ipr.bran_kun = 0;
            if ( Query_paz_ipr() == 0 )
            {
                paz_ipr.ld_pr = 0;
            }
        }
    }

    ggdat.gpreis = (long)(100.0 * paz_ipr.ld_pr);

    BauerngutLabelData();

    nArtikel++;

	fprintf( fperr, "%5ld. Artikel: A: %7ld BK: %7ld, P: %7ld wird gesendet ...\n",
	    	  nArtikel, ggdat.plunr, paz_ipr.bran_kun, ggdat.gpreis );

    return 1;
}

int getBranData( void )
{
    nArtikel = 0;
	
    a_kun_gx.kun = 0;
    TimeMsgLine( "fetch...");   	
	if ( fetch_a_kun_gx_kb() == 0 )
	    return 0;
    Query_a_bas();

    paz_ipr.a = a_kun_gx.a;
    paz_ipr.bran_kun = atol( a_kun_gx.kun_bran2 );
    if ( Query_paz_ipr() == 0 )
    {
        paz_ipr.bran_kun = 0;
        if ( Query_paz_ipr() == 0 )
        {
            paz_ipr.ld_pr = 0;
        }
    }
    ggdat.gpreis = (long)(100.0 * paz_ipr.ld_pr);

    BauerngutLabelData();

    nArtikel++;

	fprintf( fperr, "%5ld. Artikel: A: %7ld BK: %7ld, P: %7ld wird gesendet ...\n",
	    	  nArtikel, ggdat.plunr, paz_ipr.bran_kun, ggdat.gpreis );
	    	
    return 1;
}

int getAufData( void )
{
	nArtikel = 0;
	
	if ( fetch_pazlsp() == 0 )
	    return 0;

    Query_a_bas();

    ggdat.gpreis = (long)(100.0 * pazlsp.ls_lad_euro);

	a_kun_gx.a = pazlsp.a;
	
 	if ( pazlsp.kun == 0 )
 	{
 	    a_kun_gx.kun = 0;
 	    strcpy( a_kun_gx.kun_bran2, pazlsp.kun_bran2 );
	    fehlerfall = ! Query_a_kun_gx_ab();
	}
	else
 	{
 	    a_kun_gx.kun = pazlsp.kun;
 	    a_kun_gx.kun_bran2[0] = '\0';
	    fehlerfall = ! Query_a_kun_gx_ak();
	}
	ggdat.kdnr = 0;
	if ( fehlerfall != 0  )
	{
        /*  Fehlermeldung --> ATX2 */
        ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
        ggdat.atxnr2 = ggatx2.atxnr;
        sprintf( ggatx2.atx, "^14;^Ac;FEHLER\nArtikel %.0lf ohne Daten\n", a_kun_gx.a );
        ggatx2.atxlen = (us) strlen(ggatx2.atx);
	}
	else
	{
        BauerngutLabelData();
    }   	

    nArtikel++;

	fprintf( fperr, "%5ld. Artikel gesendet: A: %7ld: LS: %7ld, P: %7ld wird gesendet ...\n",
	    	  nArtikel, ggdat.plunr, pazlsp.ls, ggdat.gpreis );

    return 1;
}

void gx_to_ifmx_datum( long gxdat, long *ifmxdat )
{
    long tt, mm, jj;
    char ttmmjjjj[11];
    tt = gxdat / 10000;
    mm = (gxdat - tt * 10000) / 100;
    jj = gxdat - mm * 100 - tt * 10000;
    sprintf( ttmmjjjj, "%02ld.%02ld.20%02ld", tt, mm, jj );
	rstrdate( ttmmjjjj, ifmxdat );
}

void PazStatistikInsert( int etyp )
{
    if ( pazstatistik[0] == 'N' )
        return;

    paz_statistik.etyp      = etyp;
    paz_statistik.a         = (long) a_kun_gx.a;
    paz_statistik.ls        = pazlsp.ls;
    paz_statistik.gpreis    = ggdat.gpreis;
    paz_statistik.spreis    = ggdat.spreis;
    switch ( etyp )
    {
    case ETI_EINZEL:
        paz_statistik.netto     = ggdat.netto;
        paz_statistik.stk       = 1;
        break;
    case ETI_SUMME_1:
        paz_statistik.netto     = ggdat.netsum;
        paz_statistik.stk       = ggdat.stksum;
        break;
    case ETI_SUMME_2:
        paz_statistik.netto     = ggdat.netsum2;
        paz_statistik.stk       = ggdat.stksum2;
        break;
    case ETI_SUMME_3:
        paz_statistik.netto     = ggdat.netsum3;
        paz_statistik.stk       = ggdat.stksum3;
        break;
    default:
        paz_statistik.netto     = 0;
        paz_statistik.stk       = 0;
        break;
    }
    paz_statistik.tara      = ggdat.tara;
	gx_to_ifmx_datum( ggdat.datum1, &paz_statistik.datum1 );
	gx_to_ifmx_datum( ggdat.datum2, &paz_statistik.datum2 );
    paz_statistik.geraet    = ggdat.geraet;
    paz_statistik.numerator = ggdat.numerator;
    paz_statistik.sys       = system_num;
    paz_statistik.satz      = 0;
    dtcurrent( &paz_statistik.zeit_ins );

/*
 *     EXEC SQL BEGIN WORK;
 */
#line 3442 "gxsql.ec"
  {
#line 3442 "gxsql.ec"
  sqli_trans_begin2((mint)1);
#line 3442 "gxsql.ec"
  }
/*
 *     EXEC SQL INSERT INTO PAZ_STATISTIK
 *              (
 *                etyp,
 *                a,
 *                ls,
 *                gpreis,
 *                spreis,
 *                stk,
 *                netto,
 *                tara,
 *                datum1,
 *                datum2,
 *                geraet,
 *                numerator,
 *                sys,
 *                satz,
 *                zeit_ins
 *              ) VALUES (
 *                :paz_statistik.etyp,
 * 			   :paz_statistik.a,
 * 			   :paz_statistik.ls,
 * 			   :paz_statistik.gpreis,
 * 			   :paz_statistik.spreis,
 * 			   :paz_statistik.stk,
 * 			   :paz_statistik.netto,
 * 			   :paz_statistik.tara,
 * 			   :paz_statistik.datum1,
 * 			   :paz_statistik.datum2,
 * 			   :paz_statistik.geraet,
 * 			   :paz_statistik.numerator,
 * 			   :paz_statistik.sys,
 * 			   :paz_statistik.satz,
 * 			   :paz_statistik.zeit_ins
 *              );
 */
#line 3443 "gxsql.ec"
  {
#line 3476 "gxsql.ec"
  static const char *sqlcmdtxt[] =
#line 3476 "gxsql.ec"
    {
#line 3476 "gxsql.ec"
    " INSERT INTO PAZ_STATISTIK ( etyp , a , ls , gpreis , spreis , stk , netto , tara , datum1 , datum2 , geraet , numerator , sys , satz , zeit_ins ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? )",
    0
    };
#line 3476 "gxsql.ec"
  static ifx_statement_t _SQ0 = {0};
  static ifx_sqlvar_t _sqibind[] = 
    {
      { 102, sizeof(paz_statistik.etyp), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.a), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.ls), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.gpreis), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.spreis), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.stk), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.netto), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.tara), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.datum1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.datum2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.geraet), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.numerator), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.sys), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 103, sizeof(paz_statistik.satz), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
      { 112, 3594, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
#line 3476 "gxsql.ec"
    };
#line 3476 "gxsql.ec"
#line 3476 "gxsql.ec"
  _sqibind[0].sqldata = (char *) &paz_statistik.etyp;
#line 3476 "gxsql.ec"
  _sqibind[1].sqldata = (char *) &paz_statistik.a;
#line 3476 "gxsql.ec"
  _sqibind[2].sqldata = (char *) &paz_statistik.ls;
#line 3476 "gxsql.ec"
  _sqibind[3].sqldata = (char *) &paz_statistik.gpreis;
#line 3476 "gxsql.ec"
  _sqibind[4].sqldata = (char *) &paz_statistik.spreis;
#line 3476 "gxsql.ec"
  _sqibind[5].sqldata = (char *) &paz_statistik.stk;
#line 3476 "gxsql.ec"
  _sqibind[6].sqldata = (char *) &paz_statistik.netto;
#line 3476 "gxsql.ec"
  _sqibind[7].sqldata = (char *) &paz_statistik.tara;
#line 3476 "gxsql.ec"
  _sqibind[8].sqldata = (char *) &paz_statistik.datum1;
#line 3476 "gxsql.ec"
  _sqibind[9].sqldata = (char *) &paz_statistik.datum2;
#line 3476 "gxsql.ec"
  _sqibind[10].sqldata = (char *) &paz_statistik.geraet;
#line 3476 "gxsql.ec"
  _sqibind[11].sqldata = (char *) &paz_statistik.numerator;
#line 3476 "gxsql.ec"
  _sqibind[12].sqldata = (char *) &paz_statistik.sys;
#line 3476 "gxsql.ec"
  _sqibind[13].sqldata = (char *) &paz_statistik.satz;
#line 3476 "gxsql.ec"
  _sqibind[14].sqldata = (char *) &paz_statistik.zeit_ins;
  sqli_stmt(ESQLINTVERSION, &_SQ0, (char **) sqlcmdtxt, 15, _sqibind, (struct value *) 0, (ifx_literal_t *) 0, (ifx_namelist_t *) 0, (ifx_cursor_t *) 0, 6, 0, 0);
#line 3476 "gxsql.ec"
  }
    fprintf( fperr, "PazStatistikInsert: SQLCODE = %ld\n", SQLCODE );
/*
 *     EXEC SQL COMMIT WORK;
 */
#line 3478 "gxsql.ec"
  {
#line 3478 "gxsql.ec"
  sqli_trans_commit();
#line 3478 "gxsql.ec"
  }
}

#line 3479 "gxsql.ec"
