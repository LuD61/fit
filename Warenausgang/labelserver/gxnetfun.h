#ifndef GXNETFUNH
#define GXNETFUNH

#include <stdio.h>

typedef unsigned short us;
typedef unsigned long ul;

/* DB Tabellen */
#define GGDAT             (us) 0x0000
#define GGATX             (us) 0x0001
#define GGETIKET          (us) 0x0002
#define GGCAB             (us) 0x0006

#define GGW_AUSZEICH_ART            (us) 0x0109
#define GGW_AUSZEICHART_WECHS       (us) 0x0108
#define GGW_ETIKETDRUCK_START       (us) 0x0115
#define GGW_ETIKET_ART              (us) 0x0117
#define GGW_ETIKET_GR               (us) 0x0118
#define GGW_GEW_DIMENSION           (us) 0x0120
#define GGW_GPREIS_TEILUNG          (us) 0x0122
#define GGW_PLUTEXT_DISPLAY         (us) 0x0137
#define GGW_SUM_ART                 (us) 0x015C
#define GGW_SUM_START               (us) 0x0162
#define GGW_SUM1VORWAHL_ART         (us) 0x0163
#define GGW_SUM2VORWAHL_ART         (us) 0x0164
#define GGW_PLUW_TARA               (us) 0x01AF
#define GGW_PREIS_BERECHNUNG        (us) 0x01B6
#define GGW_SUM_TEXT                (us) 0x01B9
#define GGW_DRUCKINTERN_ENABLE      (us) 0x01BA
#define GGW_SENDKANAL_A_ENABLE      (us) 0x01BF
#define GGW_SENDKANAL_B_ENABLE      (us) 0x01C0
#define GGW_SENDKANAL_C_ENABLE      (us) 0x01C1
#define GGW_SENDKANAL_D_ENABLE      (us) 0x01C2
#define GGW_SENDKANAL_E_ENABLE      (us) 0x01C3
#define GGW_SONDETIKET_KRITERIUM    (us) 0x01C5
#define GGW_SONDETIKET_WDKANAL      (us) 0x01C6
#define GGW_STOP_SUM2               (us) 0x01C9
#define GGW_SPRACHE_ETIKET          (us) 0x01D2
#define GGW_SUM3VORWAHL_ART         (us) 0x01DC
#define GGW_STOP_SUM3               (us) 0x01DB
#define GGW_LAND_SKND               (us) 0x01F2
#define GGW_LAND                    (us) 0x0101

#define GGL_UHR2                    (us) 0x0205
#define GGL_DATUM1                  (us) 0x0206
#define GGL_DATUM2                  (us) 0x0207
#define GGL_UHR1                    (us) 0x0209
#define GGL_HALTBAR1                (us) 0x020A
#define GGL_HALTBAR2                (us) 0x020B
#define GGL_GERAETENR               (us) 0x0214
#define GGL_EINZEL_NUMERATOR        (us) 0x0216
#define GGL_SUM1_NUMERATOR          (us) 0x0217
#define GGL_STCK_PRO_PCK            (us) 0x0218
#define GGL_PLUNR                   (us) 0x0219
#define GGL_KDNR                    (us) 0x021A
#define GGL_WGNR                    (us) 0x021B
#define GGL_VM_PCKNR                (us) 0x021C
#define GGL_AM_PARNR                (us) 0x021E
#define GGL_ATXNR1                  (us) 0x021F
#define GGL_ATXNR2                  (us) 0x0220
#define GGL_ATXNR3                  (us) 0x0221
#define GGL_ATXNR4                  (us) 0x0222
#define GGL_ATXNR5                  (us) 0x0223
#define GGL_LOGONR1                 (us) 0x0224
#define GGL_ETIKETFOLGE_VORWAHL     (us) 0x0225
#define GGL_STCK_SUM1_VORWAHL       (us) 0x0228
#define GGL_STCK_SUM2_VORWAHL       (us) 0x0229
#define GGL_UHR_GX                  (us) 0x022C
#define GGL_ATXNR                   (us) 0x022F
#define GGL_LOSNR                   (us) 0x0244
#define GGL_CABNR1                  (us) 0x024A
#define GGL_CABNR2                  (us) 0x024B
#define GGL_CABNR3                  (us) 0x024C
#define GGL_CABNR4                  (us) 0x024D
#define GGL_CABNR5                  (us) 0x024E
#define GGL_DTXNR1                  (us) 0x024F
#define GGL_DTXNR2                  (us) 0x0250
#define GGL_DTXNR3                  (us) 0x0251
#define GGL_DTXNR                   (us) 0x0252
#define GGL_CABNR                   (us) 0x0253
#define GGL_SUM2_NUMERATOR          (us) 0x0254
#define GGL_CHARGENNR               (us) 0x0255
#define GGL_ETIKETPARNR             (us) 0x025C
#define GGL_VM_PARNR                (us) 0x0260
#define GGL_MINMAXPARNR             (us) 0x0261
#define GGL_LOGONR2                 (us) 0x0262
#define GGL_LOGONR3                 (us) 0x0263
#define GGL_ATXNR6                  (us) 0x0264
#define GGL_ATXNR7                  (us) 0x0265
#define GGL_ATXNR8                  (us) 0x0266
#define GGL_ATXNR9                  (us) 0x0267
#define GGL_ATXNR10                 (us) 0x0268
#define GGL_ANUM1                   (us) 0x026A
#define GGL_ANUM2                   (us) 0x026B
#define GGL_ANUM3                   (us) 0x026C
#define GGL_NUTRITION_PARNR         (us) 0x0272
#define GGL_STCK_SUM3_VORWAHL       (us) 0x028B
#define GGL_SUM3_NUMERATOR          (us) 0x028C
#define GGL_ETIK_KOPIEN             (us) 0x0295
#define GGL_NR_KDETIK_SUM1          (us) 0x029C
#define GGL_NR_KDETIK_SUM2          (us) 0x029D
#define GGL_NR_KDETIK_SUM3          (us) 0x029E
#define GGL_ATXNR11                 (us) 0x02B2
#define GGL_ATXNR12                 (us) 0x02B3
#define GGL_ATXNR13                 (us) 0x02B4
#define GGL_ATXNR14                 (us) 0x02B5
#define GGL_ATXNR15                 (us) 0x02B6
#define GGL_ATXNR16                 (us) 0x02B7
#define GGL_ATXNR17                 (us) 0x02B8
#define GGL_ATXNR18                 (us) 0x02B9
#define GGL_ATXNR19                 (us) 0x02BA
#define GGL_ATXNR20                 (us) 0x02BB

/* GG Parameter(DIM.-BEHAFTET) */
#define GGD_GEW_NETTO_EINZEL        (us) 0x0301
#define GGD_GEW_TARA_EINZEL         (us) 0x0302
#define GGD_GEW_SUM1_VORWAHL        (us) 0x0303
#define GGD_GEW_SUM2_VORWAHL        (us) 0x0304
#define GGD_GEW_SUM3_VORWAHL        (us) 0x0306
#define GGD_GEW_ZUTATEN             (us) 0x0305
#define GGD_PRS_GPREIS              (us) 0x0310
#define GGD_PRS_SPREIS              (us) 0x0311
#define GGD_PRS_SUM1_VORWAHL        (us) 0x0312
#define GGD_PRS_SUM2_VORWAHL        (us) 0x0313
#define GGD_PRS_SUM3_VORWAHL        (us) 0x0314

/* GG Parameter(TEXTE/CODES) */
#define GGT_ATX                     (us) 0x0700
#define GGT_ATX1                    (us) 0x0701
#define GGT_ATX2                    (us) 0x0702
#define GGT_ATX3                    (us) 0x0703
#define GGT_ATX4                    (us) 0x0704
#define GGT_ATX5                    (us) 0x0705
#define GGT_ATX6                    (us) 0x0706
#define GGT_ATX7                    (us) 0x0707
#define GGT_ATX8                    (us) 0x0708
#define GGT_ATX9                    (us) 0x0709
#define GGT_ATX10                   (us) 0x070A
#define GGT_ATX11                   (us) 0x070B
#define GGT_ATX12                   (us) 0x070C
#define GGT_ATX13                   (us) 0x070D
#define GGT_ATX14                   (us) 0x070E
#define GGT_ATX15                   (us) 0x070F
#define GGT_DTX1                    (us) 0x0711
#define GGT_DTX2                    (us) 0x0712
#define GGT_ATX16                   (us) 0x071B
#define GGT_ATX17                   (us) 0x071C
#define GGT_ATX18                   (us) 0x071D
#define GGT_ATX19                   (us) 0x071E
#define GGT_ATX20                   (us) 0x071F
#define GGT_CAB                     (us) 0x0720
#define GGT_CAB1                    (us) 0x0721
#define GGT_CAB2                    (us) 0x0722
#define GGT_CAB3                    (us) 0x0723
#define GGT_CAB4                    (us) 0x0724
#define GGT_CAB5                    (us) 0x0725
#define GGT_CTS1                    (us) 0x0731
#define GGT_CTS2                    (us) 0x0732
#define GGT_CTS3                    (us) 0x0733
#define GGT_CTS4                    (us) 0x0734
#define GGT_CTS5                    (us) 0x0735
#define GGT_CODE1                   (us) 0x0751

/* Logische Funktionen */
#define LGX_CLOSE                (us) 0x4002
#define LGW_QUIT_OK              (us) 0x4100
#define LGW_RETURN               (us) 0x4101
#define LGW_UFKENN               (us) 0x4102
#define LGW_DEBUG                (us) 0x4103
#define LGW_ACCEPT               (us) 0x4104
#define LGW_UFKENN_INTERN        (us) 0x4105
#define LGV_QUIT                 (us) 0x4600
#define LGV_SEQUENZ              (us) 0x4601

/* Automatenparameter */
#define AMW_START_STOP           (us) 0x1101
#define AMW_BAENDER_BETRART      (us) 0x1109

#define START (us) 0x0001
#define STOP  (us) 0x0000

#define EGALSUM  (us) 0x0000
#define STOPSUM  (us) 0x0001

/* Packungssynchrone Daten */
#define PSW_ETIKETTYP                (us) 0x3100
#define PSW_PCKHDL                   (us) 0x3102
#define PSW_QUIT_OK                  (us) 0x3103
#define PSW_FEHL_PCK                 (us) 0x3105
#define PSW_GETPCK                   (us) 0x3106
#define PSW_AUSZEICH_ART             (us) 0x3109

#define PSL_STCK_SUM                 (us) 0x3200
#define PSL_STCK_SUM1                (us) 0x3204
#define PSL_STCK_SUM2                (us) 0x3205
#define PSL_STCK_SUM3                (us) 0x3209
#define PSL_PCK_CNT                  (us) 0x320F
#define PSL_SUM1_CNT                 (us) 0x3210
#define PSL_SUM2_CNT                 (us) 0x3211

#define PSD_GEW_NETTO_EINZEL         (us) 0x3300
#define PSD_GEW_TARA_EINZEL          (us) 0x3301
#define PSD_GEW_NETTO_SUM            (us) 0x3303
#define PSD_GEW_NETTO_INFO           (us) 0x3304
#define PSD_GEW_SUM1                 (us) 0x3305
#define PSD_GEW_SUM2                 (us) 0x3306
#define PSD_GEW_BRUTTO_SUM           (us) 0x330D
#define PSD_GEW_ABTROPF_SUM          (us) 0x330E
#define PSD_GEW_FUELLMENGE_SUM       (us) 0x330F
#define PSD_PRS_VKPREIS              (us) 0x3310
#define PSD_PRS_SUM                  (us) 0x3314
#define PSD_PRS_SUM1                 (us) 0x3315
#define PSD_PRS_SUM2                 (us) 0x3316
#define PSD_GEW_TARA_SUM             (us) 0x3321
#define PSD_GEW_ZUTATEN_SUM          (us) 0x3322
#define PSD_GEW_SUM3                 (us) 0x3323

#define PSV_QUIT                     (us) 0x3600
#define PSV_PCK                      (us) 0x3601
#define PSV_DATA                     (us) 0x3604   

/* DB Funktionen */
#define DBW_END_OK        (us) 0x5100
#define DBW_TAB           (us) 0x5101
#define DBW_ATTRDESCRIP   (us) 0x5102
#define DBV_INFO          (us) 0x5604
#define DBV_DATA_VDBT     (us) 0x5605
#define DBV_DEL_VDBT      (us) 0x5606
#define DBT_FILTER        (us) 0x5700

/* Werkzeugbefehle */
#define WZW_END_OK                     (us) 0x9100
#define WZW_SUMVORWAHL_ART             (us) 0x9101
#define WZW_MINUS_SUM                  (us) 0x9104
#define WZW_REMOTE_SOFTKEY_NR          (us) 0x9106
#define WZW_REMOTE_SOFTKEY_TYP         (us) 0x9107
#define WZW_REMOTE_SOFTKEY_ATTR        (us) 0x9108
#define WZW_REMOTE_SOFTKEY_STELLEN     (us) 0x9109
#define WZW_REMOTE_DISPLAY_ATTR        (us) 0x910B
#define WZW_MODE                       (us) 0x910C
#define WZW_SOFTKEY_PAGE               (us) 0x9112
#define WZL_STCK_SUM_VORWAHL           (us) 0x9201
#define WZL_MINUS_STCK_EINZEL          (us) 0x9203
#define WZL_REMOTE_SOFTKEY_EINGABE     (us) 0x920A
#define WZT_REMOTE_SOFTKEY_TEXT        (us) 0x9700
#define WZT_REMOTE_SOFTKEY_EINGABE     (us) 0x970A /* bis 3.3: (us) 0x9701 */
#define WZT_REMOTE_DISPLAY_TEXT        (us) 0x9702
#define WZD_GEW_SUMVORWAHL             (us) 0x9301
#define WZD_PRS_SUMVORWAHL             (us) 0x9310
#define WZV_SUM                        (us) 0x9600
#define WZV_SUMKLAS                    (us) 0x9601
#define WZV_MINUS                      (us) 0x9602
#define WZV_REMOTE_TO_SOFTKEY          (us) 0x9604
#define WZV_SOFTKEY_TO_REMOTE          (us) 0x9605
#define WZV_REMOTE_DISPLAY             (us) 0x9606

#define DIM_PRS                        (us) 0x0000
#define DIM_GEW                        (us) 0x00FD

#define NORMAL                         (us) 0x0000
#define CLEAR                          (us) 0xFFFF

#define AKTIV                          (us) 0x0001
#define PASSIV                         (us) 0x0000

#define TASTER                         (us) 0x0000
#define ALPNUM                         (us) 0x0001
#define NUM                            (us) 0x0002

/* Steuerbefehle */
#define XCW_PRINT_OLD                  (us) 0xA100
#define XCW_DBSUM_UPDATE               (us) 0xA103
#define XCW_DISPLAY_TEXT               (us) 0xA104
#define XCW_LOCK                       (us) 0xA105
#define XCW_UNLOCK                     (us) 0xA106
#define XCW_SUM_CLEAR                  (us) 0xA107
#define XCW_PRINT                      (us) 0xA10C
#define XCV_DBTAB_DATASET              (us) 0xA600

#define ETI_EINZEL    (us) 0x0000
#define ETI_SUMME_1   (us) 0x0001
#define ETI_SUMME_2   (us) 0x0002
#define ETI_SUMME_A   (us) 0x0003
#define ETI_SUMME_K   (us) 0x0005
#define ETI_SUMME_TG  (us) 0x0006
#define ETI_SUMME_TR  (us) 0x0009
#define ETI_SUMME_3   (us) 0x000A
#define ETI_INIT      (us) 0x00EE
#define ETI_FEHL      (us) 0x00CC

#define SUMSTART_EXPLIZIT (us) 0
#define SUMSTART_IMPLIZIT (us) 1
#define SUMSTART_EXTERN   (us) 2

#define DRUCK_INTERN   (us) 0
#define DRUCK_EXPLIZIT (us) 1
#define DRUCK_AUTOMAT  (us) 2

#define GXRD (char) 0x90
#define GXWR (char) 0xD0

#define M_1        (us) 0x0031
#define M_TERMINAL (us) 0x0054

#define AE "\xC4"
#define OE "\xD6"
#define UE "\xDC"
#define ae "\xE4"
#define oe "\xF6"
#define ue "\xFC"
#define ss "\xDF"

char fukennung[];

typedef struct {
    long plunr;
    long kdnr;
    long atxnr1;
    long atxnr2;
    long atxnr3;
    long atxnr4;
    long atxnr5;
    long atxnr6;
    long atxnr7;
    long atxnr8;
    long atxnr9;
    long atxnr10;
    long atxnr11;
    long atxnr12;
    long atxnr13;
    long atxnr14;
    long atxnr15;
    long atxnr16;
    long atxnr17;
    long atxnr18;
    long atxnr19;
    long atxnr20;
    long cabnr1;
    long cabnr2;
	long cabnr3;
	long cabnr4;
    long dtxnr1;
    long dtxnr2;
    long dtxnr3;
    long gpreis;
    long spreis;
    long stck;
    long netto;
    long tara;
    long haltbar1;
    long datum1;
	long datum2;
	long stk_pro_pck;
    long gwpar;
    long ampar;
    long prbe;
    us aart;
    char cts1[14];
    us cts1len;
    char cts2[14];
    us cts2len;
    long stksum;
    long stksum2;
    long stksum3;
    long stksum2x;
    long stksum3x;
    long netsum;
    long netsum2;
    long netsum3;
    long netsum2x;
    long netsum3x;
    long prsum;
    long prsum2;
    long prsum3;
    long et1_dr;
    long et2_dr;
    long et3_dr;
    long sum1vw;
    long sum2vw;
    long sum3vw;
    long sum1vwa;
    long sum2vwa;
    long sum3vwa;
    long stpsum2a;
    long stpsum3a;
    long liefmeeinh;
    long liefmesoll;
    long fgew ;
    long etin;
    long ets1;
    long ets2;
    long ets3;
	long eti_nve1;
	long eti_nve2;
    long logo1;
    long logo2;
    long chargennr;
    long geraet;
    long numerator;
    long s1numerator;
    long s2numerator;
    us   sonder_eti_krit; 
    us   sonder_eti_kanal;
    us   devise; 
    us   devise2;
	int zutaten;
} GGDAT_t;

extern GGDAT_t ggdat;

typedef struct {
    long atxnr;
    char atx[2000];
    us atxlen;
} GGATX_t;

extern GGATX_t ggatx1, ggatx2, ggatx3, ggatx4, ggatx5, ggatx6, ggatx7,
               ggatx8, ggatx9, ggatx10, ggatx11, ggatx12,
			   ggatx13, ggatx14, ggatx15, ggatx16,
			   ggatx17, ggatx18, ggatx19, ggatx20;

typedef struct {
    long cabnr;
    char cab[200];
    us cablen;
} GGCAB_t;

extern GGCAB_t ggcab1, ggcab2, ggcab3, ggcab4, ggcab5;

extern char gx[3];
extern us psw_pckhdl;
extern us quit_ok;
extern us end_ok;
extern us returncode;
extern us debugcode;
extern us lgx_close;
extern us dim_prs;
extern us psw_etikettyp;
extern us psw_fehl_pck;

extern char header[];
extern char *hptr;

char *funame( us );
char *reverse( char *, us );
char *reversecpy( char *, char *, us );
us formfu( char *, us, ... );
us checkfu( char * );

#endif
