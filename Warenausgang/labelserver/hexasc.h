#ifndef HEXASCH
#define HEXASCH

typedef unsigned short us;

us a2x( char *x, char *a, us alen );
us x2a( char *a, char *x, us xlen );

#endif
