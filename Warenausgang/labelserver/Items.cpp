#include "stdafx.h"
#include "items.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

extern int testmode;
extern FILE *fperr;

/****
   "key" = "val" Kommentar
****/

#define KEYLEN 40
#define VALLEN 200
#define LINELEN 260  /* 40 + 200 + sicherheit ... */

typedef struct { char key[KEYLEN]; char val[VALLEN]; } Item;
Item item[301];
int numitm;

void printitem( Item *itemptr, FILE *fp )
{
    fprintf(fp,"\"%s\" = \"%s\"\n", itemptr->key, itemptr->val );
}

int itmcmp( const void *a, const void *b )
{
    return strcmp( ((Item*)a)->key, ((Item*)b)->key );
}

int getitem( Item  *itemptr, char *line )
{
    char *p, *q;
    memset( itemptr->key, KEYLEN, '\0' );
    memset( itemptr->val, VALLEN, '\0' );
    p = line;

    /* 1. " */
    q = strchr( p, '"' );
    if ( q == NULL )
        return 0;

    /* 2. " */
    p = q + 1;
    q = strchr( p, '"' );
    if ( q == NULL )
        return 0;
    if ( q == p )
        return 0;
    if ( q - p > KEYLEN )
        return 0;

    memcpy( itemptr->key, p, q-p );

    /* 3. " */
    p = q + 1;
    q = strchr( p, '"' );
    if ( q == NULL )
        return 0;

    /* 4. " */
    p = q + 1;
    q = strchr( p, '"' );
    if ( q == NULL )
        return 0;
    if ( q == p )
        return 0;
    if ( q - p > VALLEN )
        return 0;

    memcpy( itemptr->val, p, q-p );
    
    if (testmode)
        printitem( itemptr, fperr );
    return 1;
}

void inititems( char *name )
{
    FILE *fp;
    Item *itemptr;
    char line[LINELEN];
    sprintf( line, "%s", name );
    fp = fopen( line, "r" );
    if (fp == NULL)
    {
        perror(line);
        return;
    }
    strcpy( item[0].key, "0" );
    strcpy( item[0].val, "undefiniert" );
    itemptr = item + 1;
    for (;;)
    {
        if (fgets(line,LINELEN,fp) == NULL)
            break;
        if ( !getitem( itemptr, line ) )
            continue;
        itemptr++;
    }
    fclose(fp);
    numitm = (int)(itemptr - item);
    if (testmode)
        fprintf(fperr,"Anzahl Items = %d\n", numitm );
    qsort( item, numitm, sizeof(Item), itmcmp );
    if (testmode)
    {
        for ( itemptr = item; itemptr - item < numitm; itemptr++ )
        {
            printitem( itemptr, fperr );
        }
    }
    return;
}

char *itmval( char *key )
{
    Item *r;
    r = (Item*) bsearch( key, item, numitm, sizeof(Item), itmcmp );
    if (r == NULL)
        return item[0].val;
    return r->val;
}
