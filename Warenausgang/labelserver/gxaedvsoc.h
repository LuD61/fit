#ifndef GXAEDVSOCH
#define GXAEDVSOCH

#include "global.h"
#include "gxnetfun.h"
#include "gxbct.h"
#include <winsock2.h>

typedef struct {
	int sb;
	int connect_ok;
	char shost[80];
	us sport;
	us cport;
    SOCKET soc;
	BCSConnection * pBCSConnection;
    int telegramtype;
} SERVER_t;

extern SERVER_t serverarr[];

void initsockets( void );
int toGXSB( char *, int, char, int );
int fromGXSB( char *, int );

#endif
