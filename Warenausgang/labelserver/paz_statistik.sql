
CREATE TABLE paz_statistik
  (
    etyp                  smallint,
    a                     integer,
    ls                    integer,
    gpreis                integer,
    spreis                integer,
    stk                   integer,
    netto                 integer,
    tara                  integer,
    datum1                date,
    datum2                date,
    geraet                integer,
    numerator             integer,
    sys                   integer,
    satz                  serial,
    zeit_ins              datetime
  ) IN fit_stat EXTENT SIZE 100 NEXT SIZE 100 LOCK MODE ROW;

{
  Inserts nur wenn "PAZSTATISTIK" = "J" (labelserver.ini)
   
  etyp entspricht Bizerbas PSW_ETIKETTYP (PW00):
    ETI_EINZEL    = 0x0000 =   0
    ETI_SUMME_1   = 0x0001 =   1
    ETI_SUMME_2   = 0x0002 =   2
    ETI_SUMME_3   = 0x000A =  10

  zusaetzlich nur Labelserver intern:
    ETI_INIT      = 0x00EE = 238
}