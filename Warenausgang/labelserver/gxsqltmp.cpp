#include "gxsql.h"
#include "gxnetfun.h"
#include "gxnetapp.h"
#include "ifmxdiag.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "sqlhdr.h"

EXEC SQL include sqlproto.h;
EXEC SQL include sqlca.h;
EXEC SQL include sqlda.h;
EXEC SQL include sqlstype.h;
EXEC SQL include sqltypes.h;
EXEC SQL include decimal.h;

struct sqlda *sqldap;
struct sqlvar_struct *colp;

char command[80];

#define FIELDLEN 40
#define FIELDSIZE (FIELDLEN+1)
#define DATASIZE 4000

char field[FIELDSIZE];
char databuff[DATASIZE];


extern testmode;
extern FILE *fperr;

int has_trans;

typedef struct {
	char charge[30];
	long tour;
	long amenge;
	char aeinheit[30];
	long up_ka;
	int hbk_ztr;
	double a_gew;
	double a_tara;
	int a_typ;
} DATA_t;

DATA_t data;

typedef struct {
	int mdn;
	int fil;
	long kun;
	double a;
	char a_kun[13];
	char a_bz1[25];
	int me_einh_kun;
	double inh;
	char kun_bran2[3];
	double tara;
	double ean;
	char a_bz2[25];
	int hbk_ztr;
	long kopf_text;
	char pr_rech_kz[2];
	char modif[2];
	long text_nr;
	int devise;
	char geb_eti[2];
	int geb_fill;
	long geb_anz;
	char pal_eti[2];
	int pal_fill;
	long pal_anz;
	char pos_eti[2];
	int sg1;
	int sg2;
	int pos_fill;
	int ausz_art;
	long text_nr2;
	int cab;
	char a_bz3[100];
	char a_bz4[100];
	int eti_typ;
	long mhd_text;
	long freitext1;
	long freitext2;
	long freitext3;
	int sg3;
	int eti_sum1;
	int eti_sum2;
	int eti_sum3;
	long ampar;
	int sonder_eti;
	long text_nr_a;
	double ean1;
	int cab1;
	double ean2;
	int cab2;
	double ean3;
	int cab3;
	long gwpar;
	long vppar;
	int devise2;
} A_KUN_GX_t;

A_KUN_GX_t a_kun_gx;

typedef struct {
	long text_nr;
	int eti_typ;
	int sg1;
	char txt1[100];
	int sg2;
	char txt2[100];
	int sg3;
	char txt3[100];
	int sg4;
	char txt4[100];
	int sg5;
	char txt5[100];
	int sg6;
	char txt6[100];
	int sg7;
	char txt7[100];
	int sg8;
	char txt8[100];
	int sg9;
	char txt9[100];
	int sg10;
	char txt10[100];
	int sg11;
	char txt11[100];
	int sg12;
	char txt12[100];
	int sg13;
	char txt13[100];
	int sg14;
	char txt14[100];
	int sg15;
	char txt15[100];
	int sg16;
	char txt16[100];
	int sg17;
	char txt17[100];
	int sg18;
	char txt18[100];
	int sg19;
	char txt19[100];
	int sg20;
	char txt20[100];
} PR_AUSZ_GX_t;

PR_AUSZ_GX_t pr_ausz_gx;

EXEC SQL BEGIN DECLARE SECTION;
char statement[300];
EXEC SQL END DECLARE SECTION;

int cursors_initialized;


int OpenDatabase( void )
{
	EXEC SQL database bws;
	if (SQLCODE != 0)
	{
		fprintf(stderr,"Database bws: SQLCODE=%ld\n",SQLCODE);
		exit(1);
	}
	has_trans = (sqlca.sqlwarn.sqlwarn1 == 'W');
	if (has_trans == 0)
	{
		fprintf(stderr,"Database bws has no transaction logging.\n");
	}
	else
	{
		fprintf(stderr,"Database bws has transaction logging.\n");
	}
	EXEC SQL set isolation to dirty read;
	return 0;
}

void CloseDatabase( void )
{
	EXEC SQL close bws ;
	EXEC SQL disconnect current; 
}

void printsqlda( struct sqlda *sqldap )
{
	int ic, nc;
	nc = sqldap->sqld;
	fprintf(fperr,"sqlda:\n");
	fprintf(fperr,"sqld=%d\n",nc);
	fprintf(fperr,"sqlvar:\n");
	fprintf(fperr,"%4s %20s %10s %6s\n","    ","sqlname","sqltype","sqllen");
	for ( ic = 0; ic < nc; ic++ )
	{
		colp = sqldap->sqlvar + ic; 
		fprintf(fperr,"%4d %20s %10s %6d\n",ic,colp->sqlname,
			rtypname(colp->sqltype), colp->sqllen);
	}
}

void FormQuery( void )
{
	int ic, nc;
	int pos, size;


	EXEC SQL prepare p_sel_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL describe p_sel_curs into sqldap;
	exp_chk( statement, WARN );
	printsqlda( sqldap );
	EXEC SQL declare sel_curs cursor for p_sel_curs;
	exp_chk( statement, WARN );

	pos = 0;
	nc = sqldap->sqld;
	for ( ic = 0; ic < nc; ic++ )
	{
		colp = sqldap->sqlvar + ic;
		if ( colp->sqltype == SQLCHAR )
			colp->sqllen += 1;
		pos = rtypalign( pos, colp->sqltype );
		colp->sqldata = databuff + pos;
		size = rtypmsize( colp->sqltype, colp->sqllen );
		fprintf(fperr,"%5d %5d\n",pos,size);
		pos += size;
		if ( pos > DATASIZE )
		{
			fprintf(fperr,"databuff zu klein\n");
			exit(1);
		}
	}

}

void SaveData( void )
{
	char *fieldp;
	int ic, nc;
	int rc;
	int shortval;
	long longval;
	double doubleval;
	nc = sqldap->sqld;
	for ( ic = 0; ic < nc; ic++ )
	{
		colp = sqldap->sqlvar + ic;
		byfill(field,FIELDSIZE,0);
		fieldp = field;
		switch (colp->sqltype)
		{
		case SQLSMFLOAT:
			doubleval = (double) *((float *) (colp->sqldata));
			break;

		case SQLFLOAT:
			doubleval = *((double *) (colp->sqldata));
			break;

		case SQLDECIMAL:
			rc = dectodbl( (dec_t *) colp->sqldata, &doubleval );

			if ( rc != 0 )
				fprintf(fperr,"%s %s %d Konvertierungsfehler %d\n",
				colp->sqlname,rtypname(colp->sqltype),
				colp->sqllen, rc);

			if ( strcmp( colp->sqlname, "ean" ) == 0 ) {
				a_kun_gx.ean = doubleval;
			} else if ( strcmp( colp->sqlname, "ean1" ) == 0 ) {
				a_kun_gx.ean1 = doubleval;

				break;

		case SQLCHAR:
			if ( strcmp( (char*) colp->sqlname, "a_bz3" ) == 0 ) {
				strcpy( a_kun_gx.a_bz3, colp->sqldata);
			} else if ( strcmp( colp->sqlname, "a_bz4" ) == 0 ) {
				strcpy( a_kun_gx.a_bz4, colp->sqldata);
			} else if ( strcmp( colp->sqlname, "pr_rech_kz" ) == 0 ) {
				strcpy( a_kun_gx.pr_rech_kz, colp->sqldata);
			}
			break;

		case SQLSMINT:
			if ( risnull( CSHORTTYPE, colp->sqldata ) )
			{
				shortval = 0;
			}
			else
			{
				shortval = *((short *)(colp->sqldata));
			}
			if ( strcmp( colp->sqlname, "hbk_ztr" ) == 0 ) {
				a_kun_gx.hbk_ztr = shortval;
			} else if ( strcmp( colp->sqlname, "sg1" ) == 0 ) {
				a_kun_gx.sg1 = shortval;
			} else if ( strcmp( colp->sqlname, "sg2" ) == 0 ) {
				a_kun_gx.sg2 = shortval;
			} else if ( strcmp( colp->sqlname, "ausz_art" ) == 0 ) {
				a_kun_gx.ausz_art = shortval;
			} else if ( strcmp( colp->sqlname, "cab" ) == 0 ) {
				a_kun_gx.cab = shortval;
			} else if ( strcmp( colp->sqlname, "eti_typ" ) == 0 ) {
				a_kun_gx.eti_typ = shortval;
			} else if ( strcmp( colp->sqlname, "eti_sum1" ) == 0 ) {
				a_kun_gx.eti_sum1 = shortval;
			} else if ( strcmp( colp->sqlname, "eti_sum2" ) == 0 ) {
				a_kun_gx.eti_sum2 = shortval;
			} else if ( strcmp( colp->sqlname, "eti_sum3" ) == 0 ) {
				a_kun_gx.eti_sum3 = shortval;
			} else if ( strcmp( colp->sqlname, "sonder_eti" ) == 0 ) {
				a_kun_gx.sonder_eti = shortval;
			}
			break;

		case SQLINT:
			if ( risnull( CLONGTYPE, colp->sqldata ) )
			{
				longval = 0l;
			}
			else
			{
				longval = *((long *)(colp->sqldata));
			}
			if ( strcmp( colp->sqlname, "kopf_text" ) == 0 ) {
				a_kun_gx.kopf_text = longval;
			} else if ( strcmp( colp->sqlname, "text_nr" ) == 0 ) {
				a_kun_gx.text_nr = longval;
			} else if ( strcmp( colp->sqlname, "text_nr2" ) == 0 ) {
				a_kun_gx.text_nr2 = longval;
			} else if ( strcmp( colp->sqlname, "mhd_text" ) == 0 ) {
				a_kun_gx.mhd_text = longval;
			} else if ( strcmp( colp->sqlname, "ampar" ) == 0 ) {
				a_kun_gx.ampar = longval;
			} else if ( strcmp( colp->sqlname, "gwpar" ) == 0 ) {
				a_kun_gx.gwpar = longval;
			} else if ( strcmp( colp->sqlname, "vppar" ) == 0 ) {
				a_kun_gx.vppar = longval;
			}
			break;

		case SQLDATE:
			rc = rdatestr( *((long *)(colp->sqldata)), field );
			if ( rc != 0 )
			{
				if ( risnull( CDATETYPE, colp->sqldata ) )
				{
					field[0] = '\0';
				}
				else
				{
					fprintf(fperr,"%s %s %d Konvertierungsfehler %d\n",
						colp->sqlname,rtypname(colp->sqltype),
						colp->sqllen, rc);
				}
			}
			break;

		default:
			fprintf(fperr,
				"SaveData: %s : Datentyp %s nicht unterstuetzt.\n",
				colp->sqlname,rtypname(colp->sqltype));
			break;
			}
		}
	}
}

long LoopQuery( void )
{
	long n;   
	EXEC SQL open sel_curs;
	if ( SQLCODE != 0 ) 
	{
		fprintf(fperr,"open sel_curs: SQLCODE=%ld\n",SQLCODE);
		CloseDatabase();
		exit(1);
	}

	for ( n = 0 ; ; n++ )
	{
		EXEC SQL fetch sel_curs using descriptor sqldap;
		if ( SQLCODE == 100L )
		{
			break;
		}
		if ( SQLCODE < 0L )
		{
			fprintf(fperr,"Satz %ld SQLCODE=%ld\n",n+1,SQLCODE);
			fflush( fperr ); 
			EXEC SQL close sel_curs;
			CloseDatabase();
			exit(1);
		}
		SaveData();
	}
	EXEC SQL close sel_curs;
	fflush(fperr);
	return n;
}    

void initcursors( void )
{
	cursors_initialized = 0;
}

void initcursors1( void )
{
	OpenDatabase();
	cursors_initialized = 1;
}

long fetch_a_kun_gx( void )
{

	if ( cursors_initialized == 0 )
	{
		initcursors1();
	}

	sprintf( statement,
		"SELECT * FROM A_KUN_GX WHERE A = %lf AND KUN = %ld AND KUN_BRAN2 = \"%s\"",
		a_kun_gx.a, a_kun_gx.kun, a_kun_gx.kun_bran2 );
	fprintf( fperr, "SQLCMD: %s\n", statement );
	FormQuery();
	return LoopQuery();
}

long fetch_a_kun_gx0( void )
{ 
	if ( cursors_initialized == 0 )
	{
		initcursors1();
	}   
	sprintf( statement,
		"SELECT * FROM A_KUN_GX WHERE A = %lf AND KUN = 0", a_kun_gx.a );
	fprintf( fperr, "SQLCMD: %s\n", statement );
	FormQuery();
	return LoopQuery();
}

void fetch_kun( void )
{
	/*
	a = 6088.0;
	kun = 0;
	strcpy( kun_bran2, "23" );
	*/

	if ( cursors_initialized == 0 )
	{
		initcursors1();
	}

	sprintf( statement,
		"SELECT KUN_BRAN2 FROM KUN WHERE MDN = 1 AND KUN = %ld", a_kun_gx.kun );
	fprintf( fperr, "SQLCMD: %s\n", statement );
	FormQuery();
	LoopQuery();
}

void fetch_a_bas( void )
{   
	if ( cursors_initialized == 0 )
	{
		initcursors1();
	}  
	sprintf( statement,
		"SELECT A_TYP, HBK_ZTR,A_TARA FROM A_BAS WHERE A = %ld", a_kun_gx.a );
	fprintf( fperr, "SQLCMD: %s\n", statement );
	FormQuery();
	LoopQuery();
}

void fetch_pr_ausz_gx( void )
{   
	if ( cursors_initialized == 0 )
	{
		initcursors1();
	}  
	sprintf( statement,
		"SELECT * FROM PR_AUSZ_GX WHERE TEXT_NR = %ld AND ETI_TYP = %ld",
		pr_ausz_gx.text_nr, pr_ausz_gx.eti_typ );
	fprintf( fperr, "SQLCMD: %s\n", statement );
	FormQuery();
	LoopQuery();
}

void vorbelegung( void )
{
	ggdat.aart = 0 ;
	ggdat.gpreis = 0 ;
	ggdat.devise = 6 ;
	ggdat.devise2 = 0 ;
	ggdat.stk_pro_pck = 0 ;
	ggdat.cabnr1 = 0 ;
	ggdat.cabnr2 = 0 ;
	ggdat.etin = 0 ;
	ggdat.logo1 = 0 ;
	ggdat.logo2 = 0 ;
	strcpy( ggdat.cts1, "" ) ;
	ggdat.cts1len = (us) strlen ( ggdat.cts1 ) ;
	strcpy( ggdat.cts2, "" ) ;
	ggdat.cts2len = (us) strlen ( ggdat.cts2 ) ;
	ggdat.ampar = 0 ;
	ggdat.haltbar1 = 0 ;
	ggdat.tara = 15 ;
	ggdat.fgew = 0 ;
	ggdat.aart = 0 ;
	ggdat.stksum = 0;
	ggdat.stksum2 = 0;
	ggdat.stksum3 = 0;
	ggdat.netsum = 0;
	ggdat.netsum2 = 0;
	ggdat.netsum3 = 0;
	ggdat.sum1vwa = 1;
	ggdat.sum2vwa = 1;
	ggdat.sum3vwa = 1;
	ggdat.stk1vw = 0;
	ggdat.stk2vw = 0;
	ggdat.stk3vw = 0;
	ggdat.prs1vw = 0;
	ggdat.prs2vw = 0;
	ggdat.prs3vw = 0;
	ggdat.gew1vw = 0;
	ggdat.gew2vw = 0;
	ggdat.gew3vw = 0;
	ggdat.geb2vw = 0;
	ggdat.geb3vw = 0;
	ggdat.pal3vw = 0;

	strcpy( ggatx1.atx, "Textfeld1" );
	ggatx1.atxlen = (us) strlen ( ggatx1.atx ) ;

	strcpy( ggatx2.atx, "Textfeld2" );
	ggatx2.atxlen = (us) strlen ( ggatx2.atx ) ;

	strcpy( ggatx3.atx, "Textfeld3" );
	ggatx3.atxlen = (us) strlen ( ggatx3.atx ) ;

	strcpy( ggatx4.atx, "Textfeld4" );
	ggatx4.atxlen = (us) strlen ( ggatx4.atx ) ;

	strcpy( ggatx5.atx, "Textfeld5" );
	ggatx5.atxlen = (us) strlen ( ggatx5.atx ) ;

	strcpy( ggatx6.atx, "Textfeld6" );
	ggatx6.atxlen = (us) strlen ( ggatx6.atx ) ;

	strcpy( ggatx7.atx, "Textfeld7" );
	ggatx7.atxlen = (us) strlen ( ggatx7.atx ) ;

	strcpy( ggatx8.atx, "Textfeld8" );
	ggatx8.atxlen = (us) strlen ( ggatx8.atx ) ;

	strcpy( ggatx9.atx, "Textfeld9" );
	ggatx9.atxlen = (us) strlen ( ggatx9.atx ) ;

	alterwiegesatz[0] = '\0';
}

void evalparameter( char *p )
{
	char *q;
	q = strchr( p, '=' );
	if ( q == NULL )
		return;
	q++;
	if ( memcmp( p, "Command", strlen("Command") ) == 0 ) {
		strcpy( command, q );
	} else if ( memcmp( p, "Artikel", strlen("Artikel") ) == 0 ) {
		ggdat.plunr = atol( q );
		a_kun_gx.a = (double) ggdat.plunr;
	} else if ( memcmp( p, "Kunde", strlen("Kunde") ) == 0 ) {
		ggdat.kdnr = atol( q );
		a_kun_gx.kun = ggdat.kdnr;
	} else if ( memcmp( p, "Auftragsmenge", strlen("Auftragsmenge") ) == 0 ) {
		data.amenge = atol( q );
	} else if ( memcmp( p, "Auftragsmengeneinheit", strlen("Auftragsmengeneinheit") ) == 0 {
		strcpy( data.aeinheit, q );
	} else if ( memcmp( p, "Charge", strlen("Charge") ) == 0 {
		strcpy( data.charge, q );
	} else if ( memcmp( p, "Tour", strlen("Tour") ) == 0 {
		data.tour = atol( q );
	} else if ( memcmp( p, "Preis", strlen("Preis") ) == 0 ) {
		ggdat.gpreis = 100 * atol( q );
		q = strchr( q, '.' );
		if ( q == NULL )
			return;
		q++;
		ggdat.gpreis += atol( q );
	}
}

void getfields( char *params )
{
	char parameter[80];
	char *p, *q;
	int sep;
	long len;
	sep = 0xA;
	p = params;
	for (;;)
	{
		q = strchr( p, sep );
		if ( q == NULL )
			break;
		len = q - p ;
		memcpy( parameter, p, len );
		memset( parameter + len, '\0', 1 );
		fprintf( fperr, "%s\n", parameter );
		evalparameter( parameter );
		p = q + 1;
	}
	q = strchr( p, '\0' );
	if ( q == NULL )
		return;
	len = q - p ;
	memcpy( parameter, p, len );
	memset( parameter + len, '\0', 1 );
	fprintf( fperr, "%s\n", parameter );
	evalparameter( parameter );
}

void getLabelData( char *Params )
{
	long n;
	vorbelegung();
	getfields( Params );	// Parameterstring auswerten

	n = fetch_a_kun_gx();
	if ( n == 0 )
	{
		fetch_kun();
		a_kun_gx.kun = 0;
		n = fetch_a_kun_gx();
	}
	if ( n == 0 )
	{
		n = fetch_a_kun_gx0();
	}
	if ( n == 0 )
	{  
		exit(1);
	}

	sprintf( ggdat.cts1, "%.0fl", a_kun_gx.ean );
	ggdat.cts1len = strlen( ggdat.cts1 );
	if ( a_kun_gx.ean1 > 0 )
		sprintf( ggdat.cts2, a_kun_gx.ean1 );
	else
		sprintf( ggdat.cts2, a_kun_gx.ean ); 
	ggdat.cts2len = strlen( ggdat.cts2 );
	if ( a_kun_gx.cab > 0 )
		ggdat.cabnr1 = a_kun_gx.cab1 + 70;
	else
		ggdat.cabnr1 = 71;
	if ( a_kun_gx.cab1 > 0 )
		ggdat.cabnr2 = a_kun_gx.cab1 + 70;
	else
		ggdat.cabnr2 = ggdat.cabnr1;

	ggdat.etin = a_kun_gx.eti_typ;

	if (testmode)
		fprintf(fperr,"Feld sonder_eti, 2, S, 0, 1: %s\n",p);
	switch( a_kun_gx.sonder_eti )
	{ 
	case 1: /* Master druckt */
		ggdat.sonder_eti_krit = 0;
		ggdat.sonder_eti_kanal = 1;
		break;
	case 2: /* Slave druckt */
		ggdat.sonder_eti_krit = 3;
		ggdat.sonder_eti_kanal = 1;
		break;
	default:
		ggdat.sonder_eti_krit = 0;
		ggdat.sonder_eti_kanal = 0;
		break;
	}

	fetch_a_bas();
	ggdat.fgew = 1000 * data.a_gew;
	ggdat.haltbar1 =  data.hbk_ztr;                   
	ggdat.tara = (long) 1000 * data.a_tara;

	ggdat.sum1vwa = 1;
	ggdat.stk1vw = data.up_ka;

	ggdat.sum3vwa = 1;
	ggdat.stk3vw = data.up_ka * data.amenge;

	pr_ausz_gx.text_nr = a_kun_gx.kopf_text;
	pr_ausz_gx.eti_typ = 1;
	fetch_pr_ausz_gx();    

	pr_ausz_gx.text_nr = a_kun_gx.text_nr / 100;
	pr_ausz_gx.eti_typ = 2;
	fetch_pr_ausz_gx();    

	pr_ausz_gx.text_nr = a_kun_gx.text_nr / 100;
	pr_ausz_gx.eti_typ = 3;
	fetch_pr_ausz_gx();    

	pr_ausz_gx.text_nr = a_kun_gx.text_nr / 100;
	pr_ausz_gx.eti_typ = 4;
	fetch_pr_ausz_gx();    

	pr_ausz_gx.text_nr = a_kun_gx.text_nr / 100;
	pr_ausz_gx.eti_typ = 5;
	fetch_pr_ausz_gx();    

	pr_ausz_gx.text_nr = a_kun_gx.text_nr / 100;
	pr_ausz_gx.eti_typ = 7;
	fetch_pr_ausz_gx();    

	pr_ausz_gx.text_nr = a_kun_gx.text_nr / 100;
	pr_ausz_gx.eti_typ = 11;
	fetch_pr_ausz_gx();    

	pr_ausz_gx.text_nr = a_kun_gx.text_nr / 100;
	pr_ausz_gx.eti_typ = 12;
	fetch_pr_ausz_gx();    
}

void getData( char *Params )
{
	// Parameterstring auswerten
	getfields( Params );

	// Weiteres per SQL

}

