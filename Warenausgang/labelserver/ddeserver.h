#include <ddeml.h>

// #define WM_USER_DDE_INI (WM_USER + 11)
#define WM_USER_DDE_START (WM_USER + 12)
#define WM_USER_DDE_PACKUNG (WM_USER + 13)
#define WM_USER_DDE_STOP (WM_USER + 14)
#define WM_USER_DDE_TERMINATE (WM_USER + 15)

extern char szAppName[], szTopic[] ;
extern DWORD idInst;
extern HSZ hszService, hszTopic, hszItem;

extern char Ergebnis[], Params[];
	
HDDEDATA CALLBACK DdeCallback (UINT, UINT, HCONV, HSZ, HSZ, HDDEDATA, DWORD, DWORD);
