#include "StdAfx.h"
#include "global.h"
#include "msgline.h"
#include "resource.h"
#include <time.h>
#include <stdio.h>

HWND hDlgMsg;

void LineSel( void )
{
    LRESULT lLine;
	lLine = SendDlgItemMessage( hDlgMsg, IDC_LIST1, LB_GETCOUNT, 0, 0 );
	SendDlgItemMessage( hDlgMsg, IDC_LIST1, LB_SETCURSEL, lLine - 1, 0 );
}

void MsgLine( const TCHAR *szMsg, ... )
{
    LRESULT lRet;
    TCHAR szTmp[200];
	va_list ap;

	va_start( ap, szMsg );
	wvsprintf( szTmp, szMsg, ap ); 
	va_end( ap );

	fprintf( fperr, "%s\n", szTmp );
	fflush( fperr );

	lRet = (long) SendDlgItemMessage( hDlgMsg, IDC_LIST1, LB_ADDSTRING, 0, (LPARAM) szTmp );
	if ( lRet < 0 )
	{
		SendDlgItemMessage( hDlgMsg, IDC_LIST1, LB_RESETCONTENT, 0, 0 );
		SendDlgItemMessage( hDlgMsg, IDC_LIST1, LB_ADDSTRING, 0, (LPARAM) szTmp );
	}
	LineSel();
}


void TimeMsgLine( const TCHAR *szMsg, ... )
{
    TCHAR szTime[20];
    TCHAR szTmp[200];
    time_t t;
	va_list ap;

	va_start( ap, szMsg );
	wvsprintf( szTmp, szMsg, ap ); 
	va_end( ap );

	time( &t );
	strftime( szTime, 20, "%H:%M:%S: ", localtime(&t) );
	MsgLine( "%s%s", szTime, szTmp );
}

void FatalError( const TCHAR *szMsg, ... )
{
    TCHAR szTmp[200];
	va_list ap;

	va_start( ap, szMsg );
	wvsprintf( szTmp, szMsg, ap ); 
	va_end( ap );

	TimeMsgLine( szTmp );
	Sleep(5000);
	exit(1);
}

void MsgHRESULT( HRESULT hr, const TCHAR *szMsg, ...  )
{
    TCHAR szTmp1[200];
    TCHAR szTmp2[200];
	va_list ap;

	va_start( ap, szMsg );
	wvsprintf( szTmp1, szMsg, ap );
	va_end( ap );

	FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM, NULL, hr, LANG_SYSTEM_DEFAULT, szTmp2, 120, NULL );
	MsgLine( "%s: %s", szTmp1, szTmp2 );
}