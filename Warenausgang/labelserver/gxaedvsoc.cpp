#include "stdafx.h"
#include "global.h"
#include "gxbct.h"
#include "msgline.h"
#include "gxaedvsoc.h"
#include "hexasc.h"
#include "gxnetfun.h"
#include "items.h"
#include <Windows.h>
#include <Winsock2.h>
#include <stdio.h>
#include <stdlib.h>

#define SB_SELF 1 /* Eigene Systembusadresse */

#define BIZERBA_OK "BIZERBA_OK"
#define BIZERBA_OK_LEN (int) strlen("BIZERBA_OK") 
#define BIZERBA_CONNECT_OK "BIZERBA_CONNECT_OK"
#define BIZERBA_CONNECT_OK_LEN (int) strlen("BIZERBA_CONNECT_OK") 

#define RBUFLEN 4000
#define SBUFLEN 4000

char rbuf[RBUFLEN];
char sbuf[SBUFLEN];

SERVER_t serverarr[6];

void initsockets( void )
{
	WORD wVersionRequested;
	WSADATA wsaData;
	wVersionRequested = MAKEWORD( 2, 2 );

	if ( WSAStartup( wVersionRequested, &wsaData ) != 0 ) {
		/* Tell the user that we could not find a usable */
		/* WinSock DLL.                                  */
		FatalError( "WinSock DLL not found" );
	}

	/* Confirm that the WinSock DLL supports 2.2.*/
	/* Note that if the DLL supports versions greater    */
	/* than 2.2 in addition to 2.2, it will still return */
	/* 2.2 in wVersion since that is the version we      */
	/* requested.                                        */

	if ( LOBYTE( wsaData.wVersion ) != 2 ||	HIBYTE( wsaData.wVersion ) != 2 )
	{
		/* Tell the user that we could not find a usable */
		/* WinSock DLL.                                  */
		WSACleanup();
		FatalError( "Falsche WinSock DLL Version" );
	}

	/* The WinSock DLL is acceptable. Proceed. */
}


SOCKET connectsocket( int srv )
{
	int rlen;
	int c;
	char *p; 
	struct hostent *h;			/* info about server */
	struct sockaddr_in channel;		/* holds IP address */
	SOCKET soc;
	char *shost;
	us sport, cport;

	/* bereits connected ? */
	if ( serverarr[srv].connect_ok )
		return serverarr[srv].soc;

	shost = serverarr[srv].shost;
	sport = serverarr[srv].sport;
	cport = serverarr[srv].cport;

	/* sport == 0 meint inaktiven HOST */
	if (sport == 0)
		return INVALID_SOCKET;

	h = gethostbyname(shost);
	if (!h)
	{
		FatalError( "Verbindungsaufbau (gethostbyname) zu %s missglueckt: WSA Error %d",
			shost, WSAGetLastError() );
	}
	soc = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (soc == INVALID_SOCKET)
	{
		FatalError( "Verbindungsaufbau (socket) zu %s missglueckt: WSA Error %d",
			shost, WSAGetLastError() );
	}
	memset( &channel, 0, sizeof(channel) );
	channel.sin_family = AF_INET;
	memcpy( &channel.sin_addr.s_addr, h->h_addr, h->h_length );
	channel.sin_port= htons( sport );
	c = connect( soc, (struct sockaddr *) &channel, sizeof(channel) );
	if ( c == SOCKET_ERROR )
	{
		FatalError( "Verbindungsaufbau (connect) zu %s missglueckt: WSA Error %d",
			shost, WSAGetLastError() );
	}

	TimeMsgLine( "Verbunden mit %s", shost );

	/* Informationstelegramm senden */

	p = sbuf;
	p += sprintf( p, "BIZERBA_TCP_INFO" );
	memset( sbuf+16, cport, 2 ) ;
	memset( sbuf+18, 1, SB_SELF ) ; 
	memset( sbuf+19, 0, 1 ) ; /* Reserve */
	memset( sbuf+20, 1, 1 ) ; /* mit Empfangssynchronisation */
	memset( sbuf+21, 1, 1 ) ; /* Hex-Ascii Konvertierung */
	memset( sbuf+22, 0, 1 ) ; /* Trennzeichen */
	memset( sbuf+23, 0, 1 ) ; /* Fluchtzeichen */
	if ( send( soc, sbuf, 24, 0 ) == SOCKET_ERROR )
	{
		FatalError( "Senden von BIZERBA_TCP_INFO an %s missglueckt: WSA Error %d",
			shost, WSAGetLastError() );
	}

	TimeMsgLine( "BIZERBA_TCP_INFO" );
	Sleep(30);

	rlen = recv( soc, rbuf, RBUFLEN, 0 );
	if ( rlen == SOCKET_ERROR )
	{
		FatalError( "Kein BIZERBA_CONNECT_OK von %s: WSA Error %d",
			shost, WSAGetLastError() );
	}
	rbuf[rlen] = '\0' ;
	// TimeMsgLine( rbuf );
	serverarr[srv].connect_ok = 1;
	serverarr[srv].soc = soc;
	return soc;
}

int toGXSB( char *buf, int srv, char kontrollbyte, int len )
{
	Sleep(30);
	int slen, rlen;
	int slenret, slen1;
	SOCKET soc;

	soc = connectsocket(srv);
	if (soc == INVALID_SOCKET)
		return 0;

	checkfu( buf );
	memmove( buf + 4, buf, len );
	buf[0] = kontrollbyte;
	buf[1] = fukennung[ buf[4] & 7 ];
	buf[2] = (char) SB_SELF;
	buf[3] = (char) serverarr[srv].sb;

	len += 4; 
	slen = a2x( (char*) sbuf, buf, len );
	for ( slen1 = slen, slenret = 0 ; slen1 > 0; slen1 -= slenret )
	{
		slenret = send( soc, sbuf, slen1, 0 );
		if ( slenret == SOCKET_ERROR )
		{ 
			FatalError( "Senden an %s missglueckt: WSA Error %d",
				serverarr[srv].shost, WSAGetLastError() );
			return 0;
		}
		fprintf( fperr, "slen=%d, slen1=%d, slenret=%d\n", slen, slen1, slenret );
	}
	// TimeMsgLine( sbuf);

	rlen = recv( soc, rbuf, BIZERBA_OK_LEN, 0 );
	if ( rlen == SOCKET_ERROR )
	{
		FatalError( "Kein BIZERBA_OK von %s: WSA Error %d",
			serverarr[srv].shost, WSAGetLastError() );
		return 0;
	}
	rbuf[rlen] = '\0' ;
	return slen;
}

int fromGXSB( char *buf, int srv )
{
	int rlen, len;
	SOCKET soc;
	soc = connectsocket(srv);

	Sleep(30);
	rlen = recv( soc, rbuf, RBUFLEN, 0 );
	if ( rlen == SOCKET_ERROR )
	{
		FatalError( "Keine Antwort von %s: WSA Error %d",
			serverarr[srv].shost, WSAGetLastError() );
		return 0;
	}
	rbuf[rlen] = '\0' ;
	// TimeMsgLine( rbuf );
	len = x2a( buf, (char*) rbuf, rlen ); 
	checkfu( buf + 4 );
	strcpy( sbuf, BIZERBA_OK );
	if ( send( soc, sbuf, BIZERBA_OK_LEN, 0 ) == SOCKET_ERROR )
	{ 
		FatalError( "Senden von BIZERBA_OK an %s missglueckt: WSA Error %d",
			serverarr[srv].shost, WSAGetLastError() );
		return 0;
	}
	return len;
}
