#include "stdafx.h"
#include "labelserver.h"
#include "global.h"
#include "msgline.h"
#include "ddeserver.h"
#include "items.h"
#include "gxnetfun.h"
#include "gxnetapp.h"
#include "gxcom.h"
#include "gxaedvsoc.h"
#include "gxsql.h"
#include <windows.h>
#include <winsock.h>
#include <stdio.h>
#include <stdlib.h>

HINSTANCE hInst;
HWND hDlgGlobal;

static UINT_PTR IdTimer = NULL;
int iTimerEinzel, iTimerS1;

FILE *fperr;
int testmode;
int simulation;

char itemfile[80];
char errfile[80];
char errfile_old[80];
char errfile_old_old[80];
char anwender[80];

static char cmd[200];

BOOL CALLBACK WndProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	hDlgMsg = hDlgGlobal = hDlg;
	int winStateMinimized = 1;

	switch( uMsg )
	{

	case WM_INITDIALOG:
      
        ShowWindow( hDlg, SW_SHOWMINNOACTIVE );
		winStateMinimized = 1;

		if ( DdeInitialize( &idInst, (PFNCALLBACK) &DdeCallback,
			CBF_FAIL_EXECUTES |	CBF_SKIP_REGISTRATIONS | CBF_SKIP_UNREGISTRATIONS, 0) != 0)
		{
			MessageBox (hDlg, "Fehler beim Initialisieren Server",
				szAppName, MB_ICONEXCLAMATION | MB_OK);
			DestroyWindow (hDlg);
		}
		strcpy( szTopic, "auszeichnen" );
		hszService = DdeCreateStringHandle (idInst, szAppName, 0);
		hszTopic   = DdeCreateStringHandle (idInst, szTopic, 0);

		DdeNameService (idInst, hszService, NULL, DNS_REGISTER);

        TimeMsgLine( "START" );

		strcpy( cmd, itmval("STARTCMD") );
        if ( cmd[0] != '\0' )
		{
			system( cmd );
    		TimeMsgLine( "Befehl \"%s\" wurde ausgefuehrt.", cmd );
		}

		return TRUE;

	case WM_USER_DDE_START :

        TimeMsgLine( "AUFTRAGSPOSITION" );
		getfields( Params );

		if (command[0] == 'S')
		{
			lockpaz();
			getLabelData();
			sendLabelData();
			unlockpaz();
		    if (IdTimer != NULL) KillTimer( hDlg, IdTimer );
			if (simulation > 0)
			{
		        SetTimer( hDlg, IdTimer, 1000, 0 );
			}
			iTimerEinzel = 0;
            iTimerS1 = 0;
            ggdat.s1numerator = 910001;
			ggdat.s2numerator =	920001;
			ggdat.datum2 = 030333;
		}
		if (command[0] == 'X')
		{
		    if (IdTimer != NULL) KillTimer( hDlg, IdTimer );
            iTimerEinzel = 0;
            iTimerS1 = 0;
			PostMessage (hDlg, WM_USER_DDE_STOP, 0, 0l);
		}
		if (command[0] == 'K')
		{
			PostMessage (hDlg, WM_USER_KUNDAT, 0, 0l);
		}
		if (command[0] == 'B')
		{
			PostMessage (hDlg, WM_USER_BRANDAT, 0, 0l);
		}
		if (command[0] == 'A')
		{
			PostMessage (hDlg, WM_USER_AUFDAT, 0, 0l);
		}
		return FALSE;

	case WM_USER_DDE_STOP :
        iTimerEinzel = 0;
		iTimerS1 = 0;
		if (IdTimer != NULL) KillTimer( hDlg, IdTimer );
	    lockpaz();
		// disconnectserver(2);

		return FALSE; 

	case WM_USER_DDE_PACKUNG :

		hszItem = DdeCreateStringHandle (idInst, "start", 0);
		DdePostAdvise (idInst, hszTopic, hszItem);
		DdeFreeStringHandle (idInst, hszItem);

        if ( iTimerEinzel > 9 )
		{
			iTimerEinzel = 0;
            psw_etikettyp = ETI_SUMME_1;
            ggdat.netsum = 3000;
            formErgebnis('R');
			if ( iTimerS1 > 2 )
			{
				iTimerS1 = 0;
                psw_etikettyp = ETI_SUMME_2;
                formErgebnis('R');
			}
			iTimerS1++;
			ggdat.s1numerator++;
		}
		if ( simulation > 0 )
		{
		    iTimerEinzel++;
		    if (IdTimer == NULL) SetTimer( hDlg, IdTimer, 1000, 0 );
		}
        
		if ( wParam == 1 )
		{
			formErgebnis('E');
		}
		return FALSE;

	case WM_TIMER:
		if (IdTimer != NULL) KillTimer( hDlg, IdTimer );
        psw_etikettyp = ETI_EINZEL;
		ggdat.netto = 300;
		formErgebnis('R');
		break;

	case WM_USER_DDE_TERMINATE :

		sendkanaldisable(); /* 20130821 */
		disconnectallservers();
        Sleep( 2000 );
		CloseDatabase();
        Sleep( 2000 );

		DdeNameService (idInst, hszService, NULL, DNS_UNREGISTER );
		DdeFreeStringHandle (idInst, hszService );
		DdeFreeStringHandle (idInst, hszTopic );

		PostMessage (hDlg, WM_DESTROY, 0, 0l);
		return FALSE;

	case WM_USER_KUNDAT :

        if ( winStateMinimized )
		{
		    ShowWindow( hDlg, SW_SHOWNORMAL );
			winStateMinimized = 0;
		}
		// TimeMsgLine( "KUNDENDATEN" );

		if ( getKunData() )
		{
			sendLabelDataToGXDB();
			PostMessage (hDlg, WM_USER_KUNDAT, 0, 0l);
		}
		else
		{
    		TimeMsgLine( "ENDE DER UEBERTRAGUNG DER KUNDENDATEN" );
			sendGXMSG( 2, "Ende der Uebertragung" );
			PostMessage (hDlg, WM_USER_DDE_TERMINATE, 0, 0l);
		}
		return FALSE;

	case WM_USER_BRANDAT :

        if ( winStateMinimized )
		{
		    ShowWindow( hDlg, SW_SHOWNORMAL );
			winStateMinimized = 0;
		}
		TimeMsgLine( "BRANCHENDATEN: NAECHSTER ARTIKEL ..." );

		if ( getBranData() )
		{
			sendLabelDataToGXDB();
			PostMessage (hDlg, WM_USER_BRANDAT, 0, 0l);
		}
		else
		{
    		TimeMsgLine( "ENDE DER UEBERTRAGUNG DER BRANCHENDATEN" );
			sendGXMSG( 2, "Ende der Uebertragung" );
			PostMessage (hDlg, WM_USER_DDE_TERMINATE, 0, 0l);
		}
		return FALSE;

	case WM_USER_AUFDAT :

		// if ( ! IsWindowVisible( hDlg ) ) ShowWindow( hDlg, SW_SHOWNORMAL );
		// if ( ! IsIconic( hDlg ) ) ShowWindow( hDlg, SW_SHOWNORMAL );
        if ( winStateMinimized )
		{
		    ShowWindow( hDlg, SW_SHOWNORMAL );
			winStateMinimized = 0;
		}
		// TimeMsgLine( "AUFTRAGSDATEN" );

		if ( getAufData() )
		{
			sendLabelDataToGXDB();
			PostMessage (hDlg, WM_USER_AUFDAT, 0, 0l);
		}
		else
		{
    		TimeMsgLine( "ENDE DER UEBERTRAGUNG DER AUFTRAGSDATEN" );
			sendGXMSG( 2, "Ende der Uebertragung" );
			PostMessage (hDlg, WM_USER_DDE_TERMINATE, 0, 0l);
		}
		return FALSE;

	case WM_DESTROY :

		EndDialog( hDlg, LOWORD( wParam ) );
		
		return FALSE ;

	case WM_COMMAND:

		switch( LOWORD( wParam ) )
		{ 
		case IDCANCEL:
			PostMessage (hDlg, WM_USER_DDE_TERMINATE, 0, 0l);
			break; 
		}
		break;

	default:
		// fprintf( fperr, "WndProv: Window Message 0x%04X erhalten\n", uMsg );
		// fflush( fperr );
		break;

	}
	return FALSE;
}

int WINAPI WinMain( HINSTANCE hInst, HINSTANCE hPrev, LPSTR lpCmdLine, int nShowCmd )
{   
	// char szPid[20];
	// wsprintf( szPid, "%ld", GetCurrentProcessId() );
	
	wsprintf( itemfile, "%s\\labelserver.ini", getenv("BWSETC") );
	inititems( itemfile );
	strcpy( errfile, itmval("errfile") );
	if ( strcmp( errfile, "stderr" ) == 0 )
	{
		fperr = stderr;
	}
	else
	{
        wsprintf( errfile_old, "%s4", errfile );
		remove( errfile_old );
            wsprintf( errfile_old, "%s3", errfile );
            wsprintf( errfile_old_old, "%s4", errfile );
            rename( errfile_old, errfile_old_old );	

            wsprintf( errfile_old, "%s2", errfile );
            wsprintf( errfile_old_old, "%s3", errfile );
            rename( errfile_old, errfile_old_old );	

            wsprintf( errfile_old, "%s1", errfile );
            wsprintf( errfile_old_old, "%s2", errfile );
            rename( errfile_old, errfile_old_old );

            wsprintf( errfile_old, "%s", errfile );
			wsprintf( errfile_old_old, "%s1", errfile );
            rename( errfile_old, errfile_old_old );

		fperr = fopen( errfile, "w" );
	}
	if (fperr == NULL)
	{
		fperr = stderr;
		fprintf(fperr, "Problem beim Oeffnen des Fehlerkanals\n" );
	}
	testmode = atoi(itmval("testmode"));
	fprintf( fperr, "%s: Letzte Kompilierung: %s %s\n", __FILE__, __DATE__, __TIME__ );
	fprintf( fperr, "%s: Letzte Aenderung:    %s\n", __FILE__, __TIMESTAMP__ );

	strcpy( anwender, itmval("ANWENDER") );
	fprintf( fperr, "ANWENDER: %s\n", anwender );
	simulation = atoi(itmval("SIMULATION"));
	fprintf( fperr, "SIMULATION: %d\n", simulation );

	initcursors();
	initSend();
	initCAB();
	initAktivSaetze();
	initservers();

	DialogBox( hInst, MAKEINTRESOURCE( IDD_DIALOG1 ), NULL, WndProc );

	return 0;
}
