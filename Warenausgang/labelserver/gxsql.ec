#include "gxsql.h"
#include "items.h"
#include "msgline.h"
#include "gxnetfun.h"
#include "gxnetapp.h"
#include "ifmxdiag.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "sqlhdr.h" 

EXEC SQL include sqlproto.h;
EXEC SQL include sqlca.h;
EXEC SQL include sqlda.h;
EXEC SQL include sqlstype.h;
EXEC SQL include sqltypes.h;
EXEC SQL include decimal.h;

mint stleng( char * );

int a_kun_gx_kb_cursor_open;
int pazlsp_cursor_open;
int nArtikel;
char command[80];
char frmtxt1[2];
char frmtxt2[2];
char frmtxt3[2];
char frmtxt4[2];
char frmtxt5[2];
char frmtxt6[2];
char frmtxt7[2];
char frmtxt8[2];
char frmtxt9[2];

extern int testmode;
extern FILE *fperr;

char txtNR[80];

void SendLabelDataToGXDB( void );

EXEC SQL BEGIN DECLARE SECTION;

long system_num;
int pazmdn;

typedef struct {
    long mdn;
    long fil;
    long kun;
	char charge[30];
	long partie;
	char esnum[30];
	char es2num[30];
	char es3num[30];
	char ez1num[16];
	char ez2num[16];
	char ez3num[16];
	long tour;
	long amenge;
	int up_ka;
	int up_pal;
	int aeinheit;
	int keinheit;
	int paleinheit;
	int hbk_ztr;
	double a_gew;
	double a_tara;
	int a_typ;
	int a_typ2;
	char datum1[30];
	char datum2[30];
	char kuna[20];
    long charg_hand;
    long inh_wanne;
    char glg[2];
    char krebs[2];
    char ei[2];
    char fisch[2];
    char erdnuss[2];
    char soja[2];
    char milch[2];
    char schal[2];
    char sellerie[2];
    char senfsaat[2];
    char sesamsamen[2];
    char sulfit[2];
    char weichtier[2];
    char brennwert[21];
    double eiweiss;
    double kh;
    double fett;
} DATA_t;

DATA_t data;

typedef struct  {
   double    a;
   char      pp_a_bz1[100];
   char      pp_a_bz2[100];
   char      lgr_tmpr[100];
   char      lupine[2];
   char      schutzgas[2];
   short     huelle;
   double    salz;
   double    davonfett;
   double    davonzucker;
   double    ballaststoffe;
   char      zutat[2001];
} A_BAS_ERW_t;

A_BAS_ERW_t a_bas_erw;

typedef struct {
    int mdn;
    int fil;
    long kun;
    long adr1;
    long stat_kun;
    int kun_gr1;
    char kun_bran2[3];
    char adr_nam1[37];
    char adr_nam2[37];
    char plz[9];
    char str[37];
    char ort1[37];
} KUN_t;

KUN_t kun;

typedef struct {
	int geb_fill;
	int pal_fill;
	long geb_anz;
	long pal_anz;
    double tara;
} KUN_GEB_t;

KUN_GEB_t kun_geb;

typedef struct {
    int mdn;
    long ls;
	long kun;
	char kun_bran2[3];
	double a;
	double ls_lad_euro;
} PAZLSP_t;

PAZLSP_t pazlsp;

typedef struct {
	long bran_kun;
	double a;
	double ld_pr;
	int akz;
} PAZ_IPR_t;

PAZ_IPR_t paz_ipr;

typedef struct {
	int cabnum;
	char cab[201];
} PAZ_CAB_t;

PAZ_CAB_t paz_cab;

typedef struct {
	long kun;
	double a;
	char a_kun[26];
} A_KUN_t;

A_KUN_t a_kun;

typedef struct {
	int mdn;
	int fil;
	long kun;
	double a;
	char a_kun[13];
	char a_bz1[25];
	int me_einh_kun;
	double inh;
	char kun_bran2[3];
	double tara;
	double ean;
	char a_bz2[25];
	int hbk_ztr;
	long kopf_text;
	char pr_rech_kz[2];
	char modif[2];
	long text_nr;
	int devise;
	char geb_eti[2];
	int geb_fill;
	long geb_anz;
	char pal_eti[2];
	int pal_fill;
	long pal_anz;
	char pos_eti[2];
	int sg1;
	int sg2;
	int pos_fill;
	int ausz_art;
	long text_nr2;
	int cab;
	char a_bz3[100];
	char a_bz4[100];
	int eti_typ;
	long mhd_text;
	long freitext1;
	long freitext2;
	long freitext3;
	int sg3;
	int eti_sum1;
	int eti_sum2;
	int eti_sum3;
	int eti_nve1;
	int eti_nve2;
	long ampar;
	int sonder_eti;
	long text_nr_a;
	double ean1;
	int cab1;
	double ean2;
	int cab2;
	double ean3;
	int cab3;
	int cab_nve1;
	int cab_nve2;
	long gwpar;
	long vppar;
	int devise2;
} A_KUN_GX_t;

A_KUN_GX_t a_kun_gx;

typedef struct {
	long text_nr;
	int eti_typ;
	int sg1; char txt1[100];
	int sg2; char txt2[100];
	int sg3; char txt3[100];
	int sg4; char txt4[100];
	int sg5; char txt5[100];
	int sg6; char txt6[100];
	int sg7; char txt7[100];
	int sg8; char txt8[100];
	int sg9; char txt9[100];
	int sg10; char txt10[100];
	int sg11; char txt11[100];
	int sg12; char txt12[100];
	int sg13; char txt13[100];
	int sg14; char txt14[100];
	int sg15; char txt15[100];
	int sg16; char txt16[100];
	int sg17; char txt17[100];
	int sg18; char txt18[100];
	int sg19; char txt19[100];
	int sg20; char txt20[100];
} PR_AUSZ_GX_t;

PR_AUSZ_GX_t pr_ausz_gx;

typedef struct {
	int etyp;
	long a;
	long ls;
    long gpreis;
    long spreis;
    long stk;
    long netto;
    long tara;
    long datum1;
    long datum2;
    long geraet;
    long numerator;
    long sys;
    long satz;
    datetime year to second zeit_ins;
} PAZ_STATISTIK_t;

PAZ_STATISTIK_t paz_statistik;

char statement[3000];
char anytxt[300];

EXEC SQL END DECLARE SECTION;

int cursors_initialized;

int OpenDatabase( void )
{
    int has_trans;
	EXEC SQL DATABASE BWS;
	if (SQLCODE != 0)
	{
		fprintf(fperr,"DATABASE BWS: SQLCODE=%ld\n",SQLCODE);
		exit(1);
	}
	has_trans = (sqlca.sqlwarn.sqlwarn1 == 'W');
	if (has_trans == 0)
	{
		TimeMsgLine( "Datenbank BWS geoeffnet: Transaktionen AUS." );
	}
	else
	{
		TimeMsgLine( "Datenbank BWS geoeffnet: Transaktionen EIN." );
	}
	EXEC SQL SET ISOLATION TO DIRTY READ;
	return 0;
}

void CloseDatabase( void )
{
	EXEC SQL CLOSE BWS;
	EXEC SQL DISCONNECT CURRENT; 
    TimeMsgLine( "Datenbank BWS geschlossen." );   
}

void FormQuery_a_kun( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT A_KUN FROM A_KUN WHERE A = ? AND KUN = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_a_kun_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare a_kun_curs cursor for p_a_kun_curs;
	exp_chk( statement, WARN );
}


void FormQuery_a_kun_gx_akb( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT EAN, KOPF_TEXT, PR_RECH_KZ, TEXT_NR, " ); 
	p += sprintf( p, "SG1, SG2, AUSZ_ART, CAB, A_BZ3, A_BZ4, ETI_TYP, " );
	p += sprintf( p, "MHD_TEXT, ETI_SUM1, ETI_SUM2, AMPAR, SONDER_ETI, "  );
	p += sprintf( p, "EAN1, CAB1, GWPAR, VPPAR, TARA " );
	p += sprintf( p, " FROM A_KUN_GX WHERE A = ? AND KUN = ? AND KUN_BRAN2 = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_a_kun_gx_akb_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare a_kun_gx_akb_curs cursor for p_a_kun_gx_akb_curs;
	exp_chk( statement, WARN );
}

void FormQuery_a_kun_gx_ak( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT EAN, KOPF_TEXT, PR_RECH_KZ, TEXT_NR, " ); 
	p += sprintf( p, "SG1, SG2, AUSZ_ART, CAB, A_BZ3, A_BZ4, ETI_TYP, " );
	p += sprintf( p, "MHD_TEXT, HBK_ZTR, ETI_SUM1, ETI_SUM2, AMPAR, SONDER_ETI, "  );
	p += sprintf( p, "EAN1, CAB1, GWPAR, VPPAR, TARA, INH, DEVISE, PR_RECH_KZ, " );
	p += sprintf( p, "ETI_SUM3, ETI_NVE1, ETI_NVE2, CAB_NVE1, CAB_NVE2, " );
	p += sprintf( p, "TEXT_NR2, GEB_ANZ, PAL_ANZ, GEB_FILL, PAL_FILL, GEB_ETI, FREITEXT1 " );
	p += sprintf( p, " FROM A_KUN_GX WHERE MDN = ? AND A = ? AND KUN = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_a_kun_gx_ak_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare a_kun_gx_ak_curs cursor for p_a_kun_gx_ak_curs;
	exp_chk( statement, WARN );
}

void FormQuery_a_kun_gx_ab( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT EAN, KOPF_TEXT, PR_RECH_KZ, TEXT_NR, " ); 
	p += sprintf( p, "SG1, SG2, AUSZ_ART, CAB, A_BZ3, A_BZ4, ETI_TYP, " );
	p += sprintf( p, "MHD_TEXT, HBK_ZTR, ETI_SUM1, ETI_SUM2, AMPAR, SONDER_ETI, "  );
	p += sprintf( p, "EAN1, CAB1, GWPAR, VPPAR, TARA, INH, DEVISE, PR_RECH_KZ, " );
	p += sprintf( p, "ETI_SUM3, ETI_NVE1, ETI_NVE2, CAB_NVE1, CAB_NVE2, " );
	p += sprintf( p, "TEXT_NR2, GEB_ANZ, PAL_ANZ, GEB_FILL, PAL_FILL, GEB_ETI, FREITEXT1 " );
	p += sprintf( p, " FROM A_KUN_GX WHERE MDN = ? AND A = ? AND KUN_BRAN2 = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_a_kun_gx_ab_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare a_kun_gx_ab_curs cursor for p_a_kun_gx_ab_curs;
	exp_chk( statement, WARN );
}

void FormQuery_pazlsp( void )
{
    char *p;
  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     ||
  	     strcmp( anwender, "DFW" ) == 0
  	     || 
  	     strcmp( anwender, "OKLE" ) == 0
  	     || 
  	     strcmp( anwender, "LOHMANN" ) == 0
  	     || 
  	     strcmp( anwender, "KUEBLER" ) == 0
  	)
    {
        return;
    }
    p = statement;
	p += sprintf( p, "SELECT A, KUN, KUN_BRAN2, LS_LAD_EURO " ); 
	p += sprintf( p, " FROM PAZLSP WHERE MDN = ? AND LS = ? AND A > 1000 " );
	p += sprintf( p, " ORDER BY A" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_pazlsp_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare pazlsp_curs cursor for p_pazlsp_curs;
	exp_chk( statement, WARN );
   
    pazlsp_cursor_open = 0;
}

void FormQuery_a_kun_gx_kb( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT A, EAN, KOPF_TEXT, PR_RECH_KZ, TEXT_NR, " ); 
	p += sprintf( p, "SG1, SG2, AUSZ_ART, CAB, A_BZ3, A_BZ4, ETI_TYP, " );
	p += sprintf( p, "MHD_TEXT, ETI_SUM1, ETI_SUM2, AMPAR, SONDER_ETI, "  );
	p += sprintf( p, "EAN1, CAB1, GWPAR, VPPAR, TARA " );
	p += sprintf( p, " FROM A_KUN_GX WHERE KUN = ? AND KUN_BRAN2 = ?" );
	p += sprintf( p, " ORDER BY A" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_a_kun_gx_kb_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare a_kun_gx_kb_curs cursor for p_a_kun_gx_kb_curs;
	exp_chk( statement, WARN );
   
    a_kun_gx_kb_cursor_open = 0;
}

void FormQuery_a_bas( void )
{
    char *p;
    
        /* a_eig */
	    p = statement;	    
		p += sprintf( p, "SELECT TARA FROM A_EIG WHERE A = ?" );
	    fprintf( fperr, "SQLCMD: %s\n", statement );    
	   	EXEC SQL prepare p_a_eig_curs from :statement;
	    exp_chk( statement, WARN );
	    EXEC SQL declare a_eig_curs cursor for p_a_eig_curs;
	    exp_chk( statement, WARN );
	    
        /* a_hndw */
	    p = statement;	    
		p += sprintf( p, "SELECT TARA FROM A_HNDW WHERE A = ?" );
        fprintf( fperr, "SQLCMD: %s\n", statement );    
     	EXEC SQL prepare p_a_hndw_curs from :statement;
        exp_chk( statement, WARN );
        EXEC SQL declare a_hndw_curs cursor for p_a_hndw_curs;
        exp_chk( statement, WARN );

  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     || 
  	     strcmp( anwender, "DFW" ) == 0
  	     || 
  	     strcmp( anwender, "KUEBLER" ) == 0
    )
    {
        /* a_bas */
   	    p = statement;
	    p += sprintf( p, "SELECT A_TYP,A_TYP2, HBK_ZTR, A_GEW, CHARG_HAND,\n" );
	    p += sprintf( p, "BRENNWERT,EIWEISS,KH,FETT,\n" );
	    p += sprintf( p, "GLG,KREBS,EI,FISCH,ERDNUSS,SOJA,MILCH,SCHAL,\n" );
        p += sprintf( p, "SELLERIE,SENFSAAT,SESAMSAMEN,SULFIT,WEICHTIER \n" );
        p += sprintf( p, "FROM A_BAS WHERE A = ?" );
	}
	else /* Bauerngut */
	{
        /* a_bas */
   	    p = statement;
	    p += sprintf( p, "SELECT A_TYP, HBK_ZTR, A_TARA, A_GEW, UP_KA, CHARG_HAND" );
	    p += sprintf( p, " FROM A_BAS WHERE A = ?" );
	}
	/* a_bas */
	fprintf( fperr, "SQLCMD: %s\n", statement );    
	EXEC SQL prepare p_a_bas_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare a_bas_curs cursor for p_a_bas_curs;
	exp_chk( statement, WARN );
	
    /* a_lgr */
	p = statement;	    
	p += sprintf( p, "SELECT INH_WANNE FROM A_LGR" );
	p += sprintf( p, " WHERE MDN=1 AND FIL=0 AND HAUPT_LGR=\"J\" AND A = ?" );
    fprintf( fperr, "SQLCMD: %s\n", statement );    
    EXEC SQL prepare p_a_lgr_curs from :statement;
    exp_chk( statement, WARN );
    EXEC SQL declare a_lgr_curs cursor for p_a_lgr_curs;
    exp_chk( statement, WARN );
}

void FormQuery_a_bas_erw( void )
{
    char *p;
   	    p = statement;
	    p += sprintf( p, "SELECT PP_A_BZ1,PP_A_BZ2, LGR_TMPR,LUPINE,SCHUTZGAS,\n" );
	    p += sprintf( p, "HUELLE,SALZ,DAVONFETT,DAVONZUCKER,BALLASTSTOFFE,ZUTAT \n" );
	    p += sprintf( p, "FROM A_BAS_ERW WHERE A = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );    
	EXEC SQL prepare p_a_bas_erw_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare a_bas_erw_curs cursor for p_a_bas_erw_curs;
	exp_chk( statement, WARN );
	
}

void FormQuery_paz_ipr( void )
{
  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     || 
  	     strcmp( anwender, "DFW" ) == 0
  	     || 
  	     strcmp( anwender, "OKLE" ) == 0 
  	     || 
  	     strcmp( anwender, "LOHMANN" ) == 0
  	     || 
  	     strcmp( anwender, "KUEBLER" ) == 0
  	)
    {
        return;
    }
	strcpy( statement,
	    "SELECT LD_PR FROM PAZ_IPR WHERE BRAN_KUN = ? AND A = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );
	
	EXEC SQL prepare p_paz_ipr_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare paz_ipr_curs cursor for p_paz_ipr_curs;
	exp_chk( statement, WARN );
}

void FormQuery_paz_cab( void )
{
    if ( strcmp( anwender, "Boesinger" ) == 0 )
    {
        return;
    }
	strcpy( statement,
	    "SELECT CAB FROM PAZ_CAB WHERE CABNUM = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );
	
	EXEC SQL prepare p_paz_cab_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare paz_cab_curs cursor for p_paz_cab_curs;
	exp_chk( statement, WARN );
}

void FormQuery_kun_geb( void )
{
	system_num = atoi(itmval("SYSTEM_NUM"));
	fprintf( fperr, "SYSTEM_NUM = %ld\n", system_num );
    if ( system_num < 1 )
    {
        return;
    }
	sprintf( statement,
	    "SELECT GEB_FILL, PAL_FILL, GEB_ANZ, PAL_ANZ, TARA FROM KUN_GEB WHERE SYS = %ld",
	     system_num );	     
	fprintf( fperr, "SQLCMD: %s\n", statement );
	
	EXEC SQL prepare p_kun_geb_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare kun_geb_curs cursor for p_kun_geb_curs;
	exp_chk( statement, WARN );
}

void FormQuery_kun( void )
{
	strcpy( statement, "SELECT KUN_BRAN2,KUN_GR1,ADR1,STAT_KUN FROM KUN" );
	strcat( statement, " WHERE MDN = ? AND FIL = ? AND KUN = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_kun_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare kun_curs cursor for p_kun_curs;
	exp_chk( statement, WARN );
	
	strcpy( statement, "SELECT ADR_NAM1, ADR_NAM2, STR, PLZ, ORT1 FROM ADR" );
	strcat( statement, " WHERE ADR = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_adr_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare adr_curs cursor for p_adr_curs;
	exp_chk( statement, WARN );
}

void FormQuery_pr_ausz_gx( void )
{
    char *p;
    p = statement;
	p += sprintf( p, "SELECT SG1, TXT1, " );
	p += sprintf( p, "SG2, TXT2, " );
	p += sprintf( p, "SG3, TXT3, " );
	p += sprintf( p, "SG4, TXT4, " );
	p += sprintf( p, "SG5, TXT5, " );
	p += sprintf( p, "SG6, TXT6, " );
	p += sprintf( p, "SG7, TXT7, " );
	p += sprintf( p, "SG8, TXT8, " );
	p += sprintf( p, "SG9, TXT9, " );
	p += sprintf( p, "SG10, TXT10, " );
	p += sprintf( p, "SG11, TXT11, " );
	p += sprintf( p, "SG12, TXT12 " );	   
	p += sprintf( p, "FROM PR_AUSZ_GX WHERE TEXT_NR = ? AND ETI_TYP = ?" );
	fprintf( fperr, "SQLCMD: %s\n", statement );

	EXEC SQL prepare p_pr_ausz_gx_curs from :statement;
	exp_chk( statement, WARN );
	EXEC SQL declare pr_ausz_gx_curs cursor for p_pr_ausz_gx_curs;
	exp_chk( statement, WARN );
}

char *allergene( void )
{
   char *p;
   p = anytxt;
   
   fprintf( fperr, "12345 67890 1234\n" );
   fprintf( fperr, "%s%s%s%s%s %s%s%s%s%s %s%s%s%s\n",
       data.glg,
       data.krebs,
       data.ei,
       data.fisch,
       data.erdnuss,
       data.soja,
       data.milch,
       data.schal,
       data.sellerie, 
       data.senfsaat,
       data.sesamsamen, 
       data.sulfit,
       data.weichtier,
       a_bas_erw.lupine );

   p += sprintf( p, "Enthält " );
   if ( data.glg[0] != 'J'
        && data.krebs[0] != 'J' 
        && data.ei[0] != 'J' 
        && data.fisch[0] != 'J' 
        && data.erdnuss[0] != 'J' 
        && data.soja[0] != 'J' 
        && data.milch[0] != 'J' 
        && data.schal[0] != 'J' 
        && data.sellerie[0] != 'J' 
        && data.senfsaat[0] != 'J' 
        && data.sesamsamen[0] != 'J' 
        && data.sulfit[0] != 'J' 
        && data.weichtier[0] != 'J' 
        && a_bas_erw.lupine[0] != 'J' 
   )
   {
          p += sprintf( p, " keine allergene Stoffe." );
          return anytxt;
   }
   if ( data.glg[0]         == 'J' ) p += sprintf( p, "Glutene. " );
   if ( data.krebs[0]       == 'J' ) p += sprintf( p, "Krebse. " );
   if ( data.ei[0]          == 'J' ) p += sprintf( p, "Eier. " );
   if ( data.fisch[0]       == 'J' ) p += sprintf( p, "Fisch. " );
   if ( data.erdnuss[0]     == 'J' ) p += sprintf( p, "Erdnuss. " );
   if ( data.soja[0]        == 'J' ) p += sprintf( p, "Soja. " );
   if ( data.milch[0]       == 'J' ) p += sprintf( p, "Milch. " );
   if ( data.schal[0]       == 'J' ) p += sprintf( p, "Schalenfrüchte. " );
   if ( data.sellerie[0]    == 'J' ) p += sprintf( p, "Sellerie. " );
   if ( data.senfsaat[0]    == 'J' ) p += sprintf( p, "Senfsaat. " );
   if ( data.sesamsamen[0]  == 'J' ) p += sprintf( p, "Sesamsamen. " );
   if ( data.sulfit[0]      == 'J' ) p += sprintf( p, "Sulfit. " );
   if ( data.weichtier[0]   == 'J' ) p += sprintf( p, "Weichtiere. " );
   if ( a_bas_erw.lupine[0] == 'J' ) p += sprintf( p, "Lupine. " );
   return anytxt;
}

void trim( char *fieldp )
{
    size_t len;
    char * cp;
	len = strlen(fieldp);
	cp = fieldp + len - 1;
	while ( len > 1 && *cp == ' ' )
	{
		*cp = '\0';
		len--, cp--;
	}
}

void substitutechar( char *s, char a, char b )
{
    char *p;
    p = strchr( s, a );
    if ( p != NULL ) *p = b; 
}

void init_a_kun_gx( void )
{
	 a_kun_gx.a_kun[0] = '\0';
	 a_kun_gx.a_bz1[0] = '\0';
	 a_kun_gx.me_einh_kun = 0;
	 a_kun_gx.inh = 0;
	 a_kun_gx.tara = 0;
	 a_kun_gx.ean = 0;
	 a_kun_gx.a_bz2[0] = '\0';
	 a_kun_gx.hbk_ztr = 0;
	 a_kun_gx.kopf_text = 0;
	 a_kun_gx.pr_rech_kz[0] = '\0';
	 a_kun_gx.modif[0] = '\0';
	 a_kun_gx.text_nr = 0;
	 a_kun_gx.devise = 0;
	 a_kun_gx.geb_eti[0] = '\0';
	 a_kun_gx.geb_fill = 0;
	 a_kun_gx.geb_anz = 0;
	 a_kun_gx.pal_eti[0] = '\0';
	 a_kun_gx.pal_fill = 0;
	 a_kun_gx.pal_anz = 0;
	 a_kun_gx.pos_eti[0] = '\0';
	 a_kun_gx.sg1 = 0;
	 a_kun_gx.sg2 = 0;
	 a_kun_gx.pos_fill = 0;
	 a_kun_gx.ausz_art = 0;
	 a_kun_gx.text_nr2 = 0;
	 a_kun_gx.cab = 0;
	 a_kun_gx.a_bz3[0] = '\0';
	 a_kun_gx.a_bz4[0] = '\0';
	 a_kun_gx.eti_typ = 0;
	 a_kun_gx.mhd_text = 0;
	 a_kun_gx.freitext1 = 0;
	 a_kun_gx.freitext2 = 0;
	 a_kun_gx.freitext3 = 0;
	 a_kun_gx.sg3 = 0;
	 a_kun_gx.eti_sum1 = 0;
	 a_kun_gx.eti_sum2 = 0;
	 a_kun_gx.eti_sum3 = 0;
	 a_kun_gx.eti_nve1 = 0;
	 a_kun_gx.eti_nve2 = 0;
	 a_kun_gx.ampar = 0;
	 a_kun_gx.sonder_eti = 0;
	 a_kun_gx.text_nr_a = 0;
	 a_kun_gx.ean1 = 0;
	 a_kun_gx.cab1 = 0;
	 a_kun_gx.ean2 = 0;
	 a_kun_gx.cab2 = 0;
	 a_kun_gx.ean3 = 0;
	 a_kun_gx.cab3 = 0;
	 a_kun_gx.cab_nve1 = 0;
	 a_kun_gx.cab_nve2 = 0;
	 a_kun_gx.gwpar = 0;
	 a_kun_gx.vppar = 0;
	 a_kun_gx.devise2 = 0;
}

int fetch_a_kun_gx_kb( void )
{   	
	if ( a_kun_gx_kb_cursor_open == 0 )
	{
	    EXEC SQL open a_kun_gx_kb_curs using $a_kun_gx.kun, $a_kun_gx.kun_bran2;
	    if ( SQLCODE != 0 ) 
	    {
		    FatalError( "open a_kun_gx_kb_curs: SQLCODE=%ld",  SQLCODE );
            return 0;
	    }
	}
    a_kun_gx_kb_cursor_open = 1;
  
   	EXEC SQL fetch a_kun_gx_kb_curs into
	    $a_kun_gx.a,
	    $a_kun_gx.ean,
        $a_kun_gx.kopf_text,
	    $a_kun_gx.pr_rech_kz,
	    $a_kun_gx.text_nr,
	    $a_kun_gx.sg1,
	    $a_kun_gx.sg2,
	    $a_kun_gx.ausz_art,
	    $a_kun_gx.cab,
	    $a_kun_gx.a_bz3,
	    $a_kun_gx.a_bz4,
	    $a_kun_gx.eti_typ,
	    $a_kun_gx.mhd_text,
	    $a_kun_gx.eti_sum1,
	    $a_kun_gx.eti_sum2,
	    $a_kun_gx.ampar,
	    $a_kun_gx.sonder_eti,
	    $a_kun_gx.ean1,
	    $a_kun_gx.cab1,
	    $a_kun_gx.gwpar,
	    $a_kun_gx.vppar,
	    $a_kun_gx.tara;
	    
	ggdat.plunr = (long) a_kun_gx.a;

	fprintf( fperr, "K : %8ld  A : %8ld  SQLCODE: %4ld\n", a_kun_gx.kun, ggdat.plunr, SQLCODE ); 
    
  	if ( SQLCODE != 0L )
	{
        EXEC SQL close a_kun_gx_kb_curs;
		a_kun_gx_kb_cursor_open = 0;
		return 0;
	}
	
	trim( a_kun_gx.a_bz3 );
	trim( a_kun_gx.a_bz4 );

	return 1;
}	

int fetch_pazlsp( void )
{
  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     || 
  	     strcmp( anwender, "DFW" ) == 0
  	     || 
  	     strcmp( anwender, "OKLE" ) == 0
  	     || 
  	     strcmp( anwender, "LOHMANN" ) == 0 
  	     || 
  	     strcmp( anwender, "KUEBLER" ) == 0 
  	)
    {
        return 0;
    }
	if ( pazlsp_cursor_open == 0 )
	{
	    EXEC SQL open pazlsp_curs using $pazlsp.mdn, $pazlsp.ls;
	    if ( SQLCODE != 0 ) 
	    {
		    FatalError( "open pazlsp_curs: SQLCODE=%ld",  SQLCODE );
            return 0;
	    }
	}
    pazlsp_cursor_open = 1;
    
   	EXEC SQL fetch pazlsp_curs into
	        $pazlsp.a,
            $pazlsp.kun,
    	    $pazlsp.kun_bran2,
	        $pazlsp.ls_lad_euro;

	ggdat.plunr = (long) pazlsp.a;
    fprintf( fperr, "LS: %8ld  A: %8ld  SQLCODE: %4ld\n", pazlsp.ls, ggdat.plunr, SQLCODE );	
	    
  	if ( SQLCODE != 0L )
	{
        EXEC SQL close pazlsp_curs;
		pazlsp_cursor_open = 0;
		return 0;
	}
	return 1;
}	

long Query_a_kun( void )
{
	long sqlcode;   
	EXEC SQL open a_kun_curs using $a_kun.a, $a_kun.kun;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open a_kun_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch a_kun_curs into
	    $a_kun.a_kun;
	sqlcode = SQLCODE;
    EXEC SQL close a_kun_curs;
	if ( sqlcode != 0L )
	{
		TimeMsgLine( "K : %8ld  A : %8ld  SQLCODE: %4ld", a_kun.kun, (long) a_kun.a, sqlcode );
		return 0;
	}
	trim( a_kun.a_kun );
	return 1;
}

long Query_a_kun_gx_akb( void )
{
	long sqlcode;   
	EXEC SQL open a_kun_gx_akb_curs using $a_kun_gx.a, $a_kun_gx.kun, $a_kun_gx.kun_bran2;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open a_kun_gx_akb_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch a_kun_gx_akb_curs into
	    $a_kun_gx.ean,
        $a_kun_gx.kopf_text,
	    $a_kun_gx.pr_rech_kz,
	    $a_kun_gx.text_nr,
	    $a_kun_gx.sg1,
	    $a_kun_gx.sg2,
	    $a_kun_gx.ausz_art,
	    $a_kun_gx.cab,
	    $a_kun_gx.a_bz3,
	    $a_kun_gx.a_bz4,
	    $a_kun_gx.eti_typ,
	    $a_kun_gx.mhd_text,
	    $a_kun_gx.eti_sum1,
	    $a_kun_gx.eti_sum2,
	    $a_kun_gx.ampar,
	    $a_kun_gx.sonder_eti,
	    $a_kun_gx.ean1,
	    $a_kun_gx.cab1,
	    $a_kun_gx.gwpar,
	    $a_kun_gx.vppar,
	    $a_kun_gx.tara;
	sqlcode = SQLCODE;
    EXEC SQL close a_kun_gx_akb_curs;
	if ( sqlcode != 0L )
	{
	    FatalError( "K : %8ld  B : %8s  SQLCODE: %4ld", a_kun_gx.kun, a_kun_gx.kun_bran2, sqlcode ); 
	}
	trim( a_kun_gx.a_bz3 );
	trim( a_kun_gx.a_bz4 );
	return 1;
}

long Query_a_kun_gx_ak( void )
{
	long sqlcode;   
	a_kun_gx.mdn = pazmdn;   
	EXEC SQL open a_kun_gx_ak_curs using $a_kun_gx.mdn, $a_kun_gx.a, $a_kun_gx.kun;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open a_kun_gx_ak_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch a_kun_gx_ak_curs into
	    $a_kun_gx.ean,
        $a_kun_gx.kopf_text,
	    $a_kun_gx.pr_rech_kz,
	    $a_kun_gx.text_nr,
	    $a_kun_gx.sg1,
	    $a_kun_gx.sg2,
	    $a_kun_gx.ausz_art,
	    $a_kun_gx.cab,
	    $a_kun_gx.a_bz3,
	    $a_kun_gx.a_bz4,
	    $a_kun_gx.eti_typ,
	    $a_kun_gx.mhd_text,
	    $a_kun_gx.hbk_ztr,
	    $a_kun_gx.eti_sum1,
	    $a_kun_gx.eti_sum2,
	    $a_kun_gx.ampar,
	    $a_kun_gx.sonder_eti,
	    $a_kun_gx.ean1,
	    $a_kun_gx.cab1,
	    $a_kun_gx.gwpar,
	    $a_kun_gx.vppar,
	    $a_kun_gx.tara,
	    $a_kun_gx.inh,
	    $a_kun_gx.devise,
	    $a_kun_gx.pr_rech_kz,
	    $a_kun_gx.eti_sum3,
	    $a_kun_gx.eti_nve1,
	    $a_kun_gx.eti_nve2,
	    $a_kun_gx.cab_nve1,
	    $a_kun_gx.cab_nve2,
	    $a_kun_gx.text_nr2,
	    $a_kun_gx.geb_anz,
	    $a_kun_gx.pal_anz,
	    $a_kun_gx.geb_fill,
	    $a_kun_gx.pal_fill,
	    $a_kun_gx.geb_eti,
	    $a_kun_gx.freitext1;
	sqlcode = SQLCODE;
    EXEC SQL close a_kun_gx_ak_curs;
	if ( sqlcode != 0L )
	{
		TimeMsgLine( "K : %8ld  A : %8ld  SQLCODE: %4ld", a_kun_gx.kun, (long) a_kun_gx.a, sqlcode );
		return 0;
	}
	trim( a_kun_gx.a_bz3 );
	trim( a_kun_gx.a_bz4 );
	return 1;
}

long Query_a_kun_gx_ab( void )
{
	long sqlcode;
	a_kun_gx.mdn = pazmdn;   
	EXEC SQL open a_kun_gx_ab_curs using $a_kun_gx.mdn, $a_kun_gx.a, $a_kun_gx.kun_bran2;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open a_kun_gx_ab_curs: SQLCODE=%ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch a_kun_gx_ab_curs into
	    $a_kun_gx.ean,
        $a_kun_gx.kopf_text,
	    $a_kun_gx.pr_rech_kz,
	    $a_kun_gx.text_nr,
	    $a_kun_gx.sg1,
	    $a_kun_gx.sg2,
	    $a_kun_gx.ausz_art,
	    $a_kun_gx.cab,
	    $a_kun_gx.a_bz3,
	    $a_kun_gx.a_bz4,
	    $a_kun_gx.eti_typ,
	    $a_kun_gx.mhd_text,
	    $a_kun_gx.hbk_ztr,
	    $a_kun_gx.eti_sum1,
	    $a_kun_gx.eti_sum2,
	    $a_kun_gx.ampar,
	    $a_kun_gx.sonder_eti,
	    $a_kun_gx.ean1,
	    $a_kun_gx.cab1,
	    $a_kun_gx.gwpar,
	    $a_kun_gx.vppar,
	    $a_kun_gx.tara,
	    $a_kun_gx.inh,
	    $a_kun_gx.devise,
	    $a_kun_gx.pr_rech_kz,
	    $a_kun_gx.eti_sum3,
	    $a_kun_gx.eti_nve1,
	    $a_kun_gx.eti_nve2,
	    $a_kun_gx.cab_nve1,
	    $a_kun_gx.cab_nve2,
	    $a_kun_gx.text_nr2,
	    $a_kun_gx.geb_anz,
	    $a_kun_gx.pal_anz,
	    $a_kun_gx.geb_fill,
	    $a_kun_gx.pal_fill,
	    $a_kun_gx.geb_eti,
	    $a_kun_gx.freitext1;
	sqlcode = SQLCODE;
    EXEC SQL close a_kun_gx_ab_curs;
	if ( sqlcode != 0L )
	{
		TimeMsgLine( "K : %8ld  A : %8ld  SQLCODE: %4ld", a_kun_gx.kun, (long) a_kun_gx.a, sqlcode );
		return 0;
	}
	trim( a_kun_gx.a_bz3 );
	trim( a_kun_gx.a_bz4 );
	return 1;
}

long Query_a_bas( void )
{
	long sqlcode;   
	EXEC SQL open a_bas_curs using $a_kun_gx.a;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open a_bas_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     ||
  	     strcmp( anwender, "DFW" ) == 0
  	     ||
  	     strcmp( anwender, "OKLE" ) == 0
  	     ||
  	     strcmp( anwender, "LOHMANN" ) == 0
  	     ||
  	     strcmp( anwender, "KUEBLER" ) == 0
    )
	{
	    EXEC SQL fetch a_bas_curs into
            $data.a_typ, $data.a_typ2, $data.hbk_ztr, $data.a_gew, $data.charg_hand,
            $data.brennwert, $data.eiweiss, $data.kh, $data.fett, $data.glg,
            $data.krebs, $data.ei, $data.fisch, $data.erdnuss, $data.soja,
            $data.milch, $data.schal, $data.sellerie, $data.senfsaat,
            $data.sesamsamen, $data.sulfit, $data.weichtier; 
  	    sqlcode = SQLCODE;
        EXEC SQL close a_bas_curs;
	    if ( sqlcode != 0L )
	    {
		    FatalError( "fetch a_bas_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
		    return 0;
        }
        fprintf( fperr, "data.fett = %4.2f\n", data.fett );
        fprintf( fperr, "data.glg = %s\n", data.glg );
        fprintf( fperr, "data.krebs = %s\n", data.krebs );
        fprintf( fperr, "data.ei = %s\n", data.ei );
        fprintf( fperr, "data.fisch = %s\n", data.fisch );
        fprintf( fperr, "data.erdnuss = %s\n", data.erdnuss );
        fprintf( fperr, "data.soja = %s\n", data.soja );
        fprintf( fperr, "data.milch = %s\n", data.milch );
        fprintf( fperr, "data.schal = %s\n", data.schal );
        fprintf( fperr, "data.sellerie = %s\n", data.sellerie );
        fprintf( fperr, "data.senfsaat = %s\n", data.senfsaat );
        fprintf( fperr, "data.sesamsamen = %s\n", data.sesamsamen );
        fprintf( fperr, "12345 67890 1234\n" );
        fprintf( fperr, "%1s%1s%1s%1s%1s %1s%1s%1s%1s%1s %1s%1s%1s %s %3.1f %3.1f %3.1f\n",
       data.glg,
       data.krebs,
       data.ei,
       data.fisch,
       data.erdnuss,
       data.soja,
       data.milch,
       data.schal,
       data.sellerie, 
       data.senfsaat,
       data.sesamsamen, 
       data.sulfit,
       data.weichtier,
       data.brennwert,
       data.eiweiss,
       data.kh,
       data.fett );
        if ( data.a_typ == 2 )
        {
 	        EXEC SQL open a_eig_curs using $a_kun_gx.a;
	        if ( SQLCODE != 0 ) 
	        {
		        FatalError( "open a_eig_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
			EXEC SQL fetch a_eig_curs into $data.a_tara;
 	        sqlcode = SQLCODE;
            EXEC SQL close a_eig_curs;
	        if ( sqlcode != 0L )
	        {
		        FatalError( "fetch a_eig_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
		        return 0;
            }
        }
        else if ( data.a_typ == 1 )
        {
 	        EXEC SQL open a_hndw_curs using $a_kun_gx.a;
	        if ( SQLCODE != 0 ) 
	        {
		        FatalError( "open a_hndw_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
 			EXEC SQL fetch a_hndw_curs into $data.a_tara;
 	        sqlcode = SQLCODE;
            EXEC SQL close a_hndw_curs;
	        if ( sqlcode != 0L )
	        {
                data.a_tara = 0.0;
            }
        }
        else if ( data.a_typ2 == 2 )
        {
 	        EXEC SQL open a_eig_curs using $a_kun_gx.a;
	        if ( SQLCODE != 0 ) 
	        {
		        FatalError( "open a_eig_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
			EXEC SQL fetch a_eig_curs into $data.a_tara;
 	        sqlcode = SQLCODE;
            EXEC SQL close a_eig_curs;
	        if ( sqlcode != 0L )
	        {
		        FatalError( "fetch a_eig_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
		        return 0;
            }
        }
        else if ( data.a_typ2 == 1 )
        {
 	        EXEC SQL open a_hndw_curs using $a_kun_gx.a;
	        if ( SQLCODE != 0 ) 
	        {
		        FatalError( "open a_hndw_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
 			EXEC SQL fetch a_hndw_curs into $data.a_tara;
 	        sqlcode = SQLCODE;
            EXEC SQL close a_hndw_curs;
	        if ( sqlcode != 0L )
	        {
                data.a_tara = 0.0;
            }
        }
        else
        {
            data.a_tara = 0.0;
        }
	}
	else  /* Bauerngut */
	{
	    EXEC SQL fetch a_bas_curs into
            $data.a_typ, $data.hbk_ztr, $data.a_tara, $data.a_gew, $data.up_ka,
            $data.charg_hand;
 	    sqlcode = SQLCODE;
        EXEC SQL close a_bas_curs;
	    if ( sqlcode != 0L )
	    {
		    FatalError( "fetch a_bas_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
		    return 0;
        }
        /*
        if ( data.a_typ == 2 )
        {
 	        EXEC SQL open a_eig_curs using $a_kun_gx.a;
	        if ( SQLCODE != 0 ) 
	        {
		        FatalError( "open a_eig_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
			EXEC SQL fetch a_eig_curs into $a_kun_gx.tara;
 	        sqlcode = SQLCODE;
            EXEC SQL close a_eig_curs;
	        if ( sqlcode != 0L )
	        {
		        FatalError( "fetch a_eig_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
		        return 0;
            }
        }
        else if ( data.a_typ == 1 )
        {
 	        EXEC SQL open a_hndw_curs using $a_kun_gx.a;
	        if ( SQLCODE != 0 ) 
	        {
		        FatalError( "open a_hndw_curs: SQLCODE: %ld",  SQLCODE );
                return 0;
	        }
 			EXEC SQL fetch a_hndw_curs into $a_kun_gx.tara;
 	        sqlcode = SQLCODE;
            EXEC SQL close a_hndw_curs;
	        if ( sqlcode != 0L )
	        {
                data.a_tara = 0.0;
            }
        }
        
 	    EXEC SQL open a_lgr_curs using $a_kun_gx.a;
	    if ( SQLCODE != 0 ) 
	    {
		    FatalError( "open a_lgr_curs: SQLCODE: %ld\n",  SQLCODE );
            return 0;
	    }
       
 	    EXEC SQL fetch a_lgr_curs into $data.inh_wanne;
 	    sqlcode = SQLCODE;
        EXEC SQL close a_lgr_curs;
	    if ( sqlcode != 0L )
	    {
		    fprintf( fperr, "fetch a_lgr_curs: %ld SQLCODE: %ld\n", (long) a_kun_gx.a, sqlcode );
            data.inh_wanne = 1;
        }
        if ( data.inh_wanne < 1 )
        {
           data.inh_wanne = 1;
        }
        */      
    }
	ggdat.fgew = (long) (1000.0 * data.a_gew);
	ggdat.haltbar1 = data.hbk_ztr;                   
	ggdat.tara = (long) (1000.0 * data.a_tara);
	ggdat.sum1vw = data.up_ka;
	return 1;
}

long Query_a_bas_erw( void )
{
	long sqlcode;
	memset( (char*) &a_bas_erw, '\0', sizeof (a_bas_erw) );   
	EXEC SQL open a_bas_erw_curs using $a_kun_gx.a;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open a_bas_erw_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
    EXEC SQL fetch a_bas_erw_curs into
	   $a_bas_erw.pp_a_bz1,
	   $a_bas_erw.pp_a_bz2,
	   $a_bas_erw.lgr_tmpr,
	   $a_bas_erw.lupine,
	   $a_bas_erw.schutzgas,
	   $a_bas_erw.huelle,
	   $a_bas_erw.salz,
	   $a_bas_erw.davonfett,
	   $a_bas_erw.davonzucker,
	   $a_bas_erw.ballaststoffe,
	   $a_bas_erw.zutat;
    sqlcode = SQLCODE;
    EXEC SQL close a_bas_erw_curs;
    if ( sqlcode != 0L )
    {
	    FatalError( "fetch a_bas_erw_curs: %ld SQLCODE: %ld", a_kun_gx.a, sqlcode );
	    return 0;
    }
	trim( a_bas_erw.pp_a_bz1 );
	trim( a_bas_erw.pp_a_bz2 );
	trim( a_bas_erw.lgr_tmpr );
	trim( a_bas_erw.zutat );
	return 1;
}

long Query_paz_ipr( void )
{
	long sqlcode;   
  	if ( strcmp( anwender, "Boesinger" ) == 0
  	     ||
  	     strcmp( anwender, "Dietz" ) == 0
  	     ||
  	     strcmp( anwender, "DFW" ) == 0
  	     ||
  	     strcmp( anwender, "OKLE" ) == 0
  	     ||
  	     strcmp( anwender, "LOHMANN" ) == 0
  	     ||
  	     strcmp( anwender, "KUEBLER" ) == 0
  	)
    {
        return 0;
    }
	EXEC SQL open paz_ipr_curs using $paz_ipr.bran_kun,$paz_ipr.a;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open paz_ipr_curs: SQLCODE: %ld", SQLCODE );
        return 0;
	}
	EXEC SQL fetch paz_ipr_curs into $paz_ipr.ld_pr;
	sqlcode = SQLCODE;
    EXEC SQL close paz_ipr_curs;
    
	fprintf( fperr, "BK: %8ld  A : %8ld  SQLCODE: %4ld\n", paz_ipr.bran_kun, (long) paz_ipr.a, sqlcode );
	
	if ( sqlcode != 0L )
	{
        return 0;
	}
	return 1;
}    

long Query_kun( void )
{
	long sqlcode;   
	EXEC SQL open kun_curs using $kun.mdn, $kun.fil, $kun.kun;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open kun_curs: SQLCODE=%ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch kun_curs into $kun.kun_bran2, $kun.kun_gr1, $kun.adr1, $kun.stat_kun;
	sqlcode = SQLCODE;
    EXEC SQL close kun_curs;
	if ( sqlcode != 0L )
	{
		FatalError( "fetch kun_curs: mdn: %ld, fil: %ld, kun: %ld SQLCODE: %ld",
		            kun.mdn, kun.fil, kun.kun, sqlcode );
		return 0;
	}
	if (a_kun_gx.kun_bran2[2] == ' ') a_kun_gx.kun_bran2[2] = '\0';
	
	MsgLine( "kun = %ld,  adr1 = %ld,  kun_gr1: %d, stat_kun: %ld",
	         kun.kun, kun.adr1, kun.kun_gr1, kun.stat_kun );
	
	kun.adr_nam1[0] = '\0';
	kun.adr_nam2[0] = '\0';
	kun.str[0] = '\0';
	kun.plz[0] = '\0';
	kun.ort1[0] = '\0';
	if ( kun.kun_gr1 == 0 )
    {
        return 1;
    }
	
	EXEC SQL open adr_curs using $kun.adr1;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open adr_curs: SQLCODE=%ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch adr_curs	into $kun.adr_nam1, $kun.adr_nam2,
	    $kun.str, $kun.plz, $kun.ort1;
	sqlcode = SQLCODE;
    EXEC SQL close adr_curs;
	if ( sqlcode != 0L )
	{
		MsgLine( "fetch adr_curs: adr: %ld SQLCODE: %ld", kun.adr1, sqlcode );
		kun.kun_gr1 = 0;
		return 1;
	}
	trim(kun.adr_nam1);
	trim(kun.adr_nam2);
	trim(kun.str);
	trim(kun.plz);
	trim(kun.ort1);
	
	return 1;
}    

void Query_paz_cab( GGCAB_t * ggcab )
{
    long sqlcode;
    paz_cab.cabnum = ggcab->cabnr;   
	EXEC SQL open paz_cab_curs using $paz_cab.cabnum;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open paz_cab_curs: SQLCODE: %ld",  SQLCODE );
	}
	EXEC SQL fetch paz_cab_curs into $paz_cab.cab;
	sqlcode = SQLCODE;
    EXEC SQL close paz_cab_curs;
    if ( sqlcode != 0L )
	{
		MsgLine( "fetch paz_cab_curs: %ld SQLCODE: %ld", paz_cab.cabnum, sqlcode );
		paz_cab.cab[0] = '\0';
	}
	trim( paz_cab.cab );
	strcpy( ggcab->cab, paz_cab.cab );
	ggcab->cablen = (us) strlen(ggcab->cab) + 1;
}

void Query_kun_geb( void )
{
    long sqlcode;
    
    if ( system_num < 1 )
       return;
	
	EXEC SQL open kun_geb_curs;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open kun_geb_curs: SQLCODE: %ld",  SQLCODE );
	}
	EXEC SQL fetch kun_geb_curs into
	    :kun_geb.geb_fill, :kun_geb.pal_fill,
	    :kun_geb.geb_anz,  :kun_geb.pal_anz,
	    :kun_geb.tara;
	sqlcode = SQLCODE;
    EXEC SQL close kun_geb_curs;
    if ( sqlcode != 0L )
	{
		MsgLine( "fetch kun_geb_curs: %ld SQLCODE: %ld", system_num, sqlcode );
	}
	fprintf( fperr, "Vorgabe S1: (ME %d) %ld\n", kun_geb.geb_fill, kun_geb.geb_anz );
	fprintf( fperr, "Vorgabe S2: (ME %d) %ld\n", kun_geb.pal_fill, kun_geb.pal_anz );
  	fprintf( fperr, "Tara: %lf kg\n", kun_geb.tara);

}

long Query_pr_ausz_gx( long text_nr, int eti_typ )
{
	long sqlcode;   
	pr_ausz_gx.text_nr = text_nr;
	pr_ausz_gx.eti_typ = eti_typ;
	EXEC SQL open pr_ausz_gx_curs using $pr_ausz_gx.text_nr, $pr_ausz_gx.eti_typ;
	if ( SQLCODE != 0 ) 
	{
		FatalError( "open pr_ausz_gx_curs: SQLCODE: %ld",  SQLCODE );
        return 0;
	}
	EXEC SQL fetch pr_ausz_gx_curs into
	   $pr_ausz_gx.sg1, $pr_ausz_gx.txt1,
	   $pr_ausz_gx.sg2, $pr_ausz_gx.txt2,
	   $pr_ausz_gx.sg3, $pr_ausz_gx.txt3,
	   $pr_ausz_gx.sg4, $pr_ausz_gx.txt4,
	   $pr_ausz_gx.sg5, $pr_ausz_gx.txt5,
	   $pr_ausz_gx.sg6, $pr_ausz_gx.txt6,
	   $pr_ausz_gx.sg7, $pr_ausz_gx.txt7,
	   $pr_ausz_gx.sg8, $pr_ausz_gx.txt8,
	   $pr_ausz_gx.sg9, $pr_ausz_gx.txt9,
	   $pr_ausz_gx.sg10, $pr_ausz_gx.txt10,
	   $pr_ausz_gx.sg11, $pr_ausz_gx.txt11,
	   $pr_ausz_gx.sg12, $pr_ausz_gx.txt12;	   
	sqlcode = SQLCODE;
    EXEC SQL close pr_ausz_gx_curs;
    
	fprintf( fperr, "TF: %8d  TN: %8ld  SQLCODE: %4ld\n", pr_ausz_gx.eti_typ, pr_ausz_gx.text_nr, sqlcode );

	if ( sqlcode != 0L )
	{
	    pr_ausz_gx.sg1 = 0;
	    pr_ausz_gx.sg2 = 0;
	    pr_ausz_gx.sg3 = 0;
	    pr_ausz_gx.sg4 = 0;
	    pr_ausz_gx.sg5 = 0;
	    pr_ausz_gx.sg6 = 0;
	    pr_ausz_gx.sg7 = 0;
	    pr_ausz_gx.sg8 = 0;
	    pr_ausz_gx.sg9 = 0;
	    pr_ausz_gx.sg10 = 0;
	    pr_ausz_gx.sg11 = 0;
	    pr_ausz_gx.sg12 = 0;
	    pr_ausz_gx.txt1[0] = '\0';
	    pr_ausz_gx.txt2[0] = '\0';
	    pr_ausz_gx.txt3[0] = '\0';
	    pr_ausz_gx.txt4[0] = '\0';
	    pr_ausz_gx.txt5[0] = '\0';
	    pr_ausz_gx.txt6[0] = '\0';
	    pr_ausz_gx.txt7[0] = '\0';
	    pr_ausz_gx.txt8[0] = '\0';
	    pr_ausz_gx.txt9[0] = '\0';
	    pr_ausz_gx.txt10[0] = '\0';
	    pr_ausz_gx.txt11[0] = '\0';
	    pr_ausz_gx.txt12[0] = '\0';
		return 0;
	}
	trim( pr_ausz_gx.txt1 );
	trim( pr_ausz_gx.txt2 );
	trim( pr_ausz_gx.txt3 );
	trim( pr_ausz_gx.txt4 );
	trim( pr_ausz_gx.txt5 );
	trim( pr_ausz_gx.txt6 );
	trim( pr_ausz_gx.txt7 );
	trim( pr_ausz_gx.txt8 );
	trim( pr_ausz_gx.txt9 );
	trim( pr_ausz_gx.txt10 );
	trim( pr_ausz_gx.txt11 );
	trim( pr_ausz_gx.txt12 );
	return 1;
}

void initcursors( void )
{
	OpenDatabase();

	FormQuery_a_kun();
	FormQuery_a_kun_gx_akb();
	FormQuery_a_kun_gx_ak();
	FormQuery_a_kun_gx_ab();
	FormQuery_a_kun_gx_kb();
	FormQuery_pr_ausz_gx();
	FormQuery_a_bas();
	FormQuery_a_bas_erw();
	FormQuery_paz_ipr();
	FormQuery_paz_cab();
	FormQuery_kun();
	FormQuery_pazlsp();
	FormQuery_kun_geb();

	cursors_initialized = 1;
	
	strcpy( frmtxt1, itmval("TEXTFORM1") );
	strcpy( frmtxt2, itmval("TEXTFORM2") );
	strcpy( frmtxt3, itmval("TEXTFORM3") );
	strcpy( frmtxt4, itmval("TEXTFORM4") );
	strcpy( frmtxt5, itmval("TEXTFORM5") );
	strcpy( frmtxt6, itmval("TEXTFORM6") );
	strcpy( frmtxt7, itmval("TEXTFORM7") );
	strcpy( frmtxt8, itmval("TEXTFORM8") );
	strcpy( frmtxt9, itmval("TEXTFORM9") );
	fprintf( fperr, "Formatierung: %s %s %s %s %s %s %s %s %s\n",
	    frmtxt1, frmtxt2, frmtxt3, frmtxt4, frmtxt5, frmtxt6, frmtxt7,
	    frmtxt8, frmtxt9 );

}

void vorbelegung( void )
{
	data.charge[0] = '\0';
	data.datum1[0] = '\0';
	data.datum2[0] = '\0';
	data.kuna[0] = '\0';
	// strcpy( data.kuna, "4711" );
	data.tour = 0;
	data.amenge = 0;
	data.up_ka = 0;
	data.up_pal = 0;
	data.aeinheit = 0;
	data.keinheit = 0;
	data.paleinheit = 0;
	data.hbk_ztr = 0;
	data.a_gew = 0;
	data.a_tara = 0;
	data.a_typ = 0;
	data.partie = 0;
	data.esnum[0] = '\0';
	data.es2num[0] = '\0';
	data.es3num[0] = '\0';
	data.ez1num[0] = '\0';
	data.ez2num[0] = '\0';
	data.ez3num[0] = '\0';
	
	a_kun_gx.kun = 0;
	strcpy( a_kun_gx.kun_bran2, "0" );
	
	ggdat.aart = 0 ;
	ggdat.gpreis = 0 ;
	ggdat.spreis = 0 ;
	ggdat.devise = 6 ;
	ggdat.devise2 = 0 ;
	ggdat.stk_pro_pck = 0 ;
	ggdat.cabnr1 = 0 ;
	ggdat.cabnr2 = 0 ;
	ggdat.cabnr3 = 0 ;
	ggdat.cabnr4 = 0 ;
	ggdat.etin = 1 ;    /* Wichtig, wenn ohne Daten: 0 geht nicht. */
	ggdat.ets1 = 0 ;
	ggdat.ets2 = 0 ;
	ggdat.logo1 = 0 ;
	ggdat.logo2 = 0 ;
	strcpy( ggdat.cts1, "000000000000" ) ;
	ggdat.cts1len = (us) strlen ( ggdat.cts1 ) ;
	strcpy( ggdat.cts2, "000000000000" ) ;
	ggdat.cts2len = (us) strlen ( ggdat.cts2 ) ;
	ggdat.ampar = 0 ;
	ggdat.gwpar = 0xFFFFFFFF ;
	ggdat.haltbar1 = 0 ;
	ggdat.datum1 = 0 ;
	ggdat.datum2 = 0 ;
	ggdat.tara = 0 ;
	ggdat.fgew = 0 ;
	ggdat.aart = 0 ;
	ggdat.stksum = 0;
	ggdat.stksum2 = 0;
	ggdat.stksum3 = 0;
	ggdat.netsum = 0;
	ggdat.netsum2 = 0;
	ggdat.netsum3 = 0;
	ggdat.sum1vwa = 1;
	ggdat.sum2vwa = 1;
	ggdat.sum3vwa = 8;
	ggdat.sum1vw = 0;
	ggdat.sum2vw = 0;
	ggdat.sum3vw = 0;
	ggdat.chargennr = 0;

	strcpy( ggatx1.atx, "" );
	ggatx1.atxlen = (us) strlen ( ggatx1.atx ) ;

	strcpy( ggatx2.atx, "" );
	ggatx2.atxlen = (us) strlen ( ggatx2.atx ) ;

	strcpy( ggatx3.atx, "" );
	ggatx3.atxlen = (us) strlen ( ggatx3.atx ) ;

	strcpy( ggatx4.atx, "" );
	ggatx4.atxlen = (us) strlen ( ggatx4.atx ) ;

	strcpy( ggatx5.atx, "" );
	ggatx5.atxlen = (us) strlen ( ggatx5.atx ) ;

	strcpy( ggatx6.atx, "" );
	ggatx6.atxlen = (us) strlen ( ggatx6.atx ) ;

	strcpy( ggatx7.atx, "" );
	ggatx7.atxlen = (us) strlen ( ggatx7.atx ) ;

	strcpy( ggatx8.atx, "" );
	ggatx8.atxlen = (us) strlen ( ggatx8.atx ) ;

	strcpy( ggatx9.atx, "" );
	ggatx9.atxlen = (us) strlen ( ggatx9.atx ) ;

	strcpy( ggatx10.atx, "" );
	ggatx10.atxlen = (us) strlen ( ggatx10.atx ) ;

	strcpy( ggatx11.atx, "" );
	ggatx11.atxlen = (us) strlen ( ggatx11.atx ) ;

	strcpy( ggatx12.atx, "" );
	ggatx12.atxlen = (us) strlen ( ggatx12.atx ) ;

}

void evalparameter( char *p )
{
	char *q;
	q = strchr( p, '=' );
	if ( q == NULL )
		return;
	q++;
	if ( memcmp( p, "Command", strlen("Command") ) == 0 ) {
		strcpy( command, q );
	} else if ( memcmp( p, "Artikeltyp", strlen("Artikeltyp") ) == 0 ) {
		data.a_typ = atol( q );
	} else if ( memcmp( p, "Artikel", strlen("Artikel") ) == 0 ) {
		ggdat.plunr = atol( q );
		a_kun_gx.a = (double) ggdat.plunr;
	} else if ( memcmp( p, "Kundenartikel", strlen("Kundenartikel") ) == 0 ) {
		strcpy( data.kuna, q );
		if ( strcmp( data.kuna, "0" ) <= 0 )
		{
		    strcpy( data.kuna, "  " );
     		fprintf( fperr, "Kundenartikel: <= 0 --> '  '\n" );
		}
	} else if ( memcmp( p, "LsKunde", strlen("LsKunde") ) == 0 ) {
		kun.kun = atol( q );
		if ( risnull( CLONGTYPE, (char*) &kun.kun) )
		{
				kun.kun = 0;
     		    fprintf( fperr, "LsKunde: NULL --> 0\n" );
		}
	} else if ( memcmp( p, "LsMdn", strlen("LsMdn") ) == 0 ) {
		kun.mdn = atol( q );
		if ( risnull( CLONGTYPE, (char*) &kun.mdn) )
		{
				kun.mdn = 0;
     		    fprintf( fperr, "LsMdn: NULL --> 0\n" );
		}
	} else if ( memcmp( p, "LsFil", strlen("LsFil") ) == 0 ) {
		kun.fil = atol( q );
		if ( risnull( CLONGTYPE, (char*) &kun.fil) )
		{
				kun.fil = 0;
     		    fprintf( fperr, "LsFil: NULL --> 0\n" );
		}
	} else if ( memcmp( p, "Kunde", strlen("Kunde") ) == 0 ) {
		a_kun_gx.kun = atol( q );
		if ( risnull( CLONGTYPE, (char*) &a_kun_gx.kun) )
		{
				a_kun_gx.kun = 0;
     		    fprintf( fperr, "Kunde: NULL --> 0\n" );
		}
	} else if ( memcmp( p, "Branche", strlen("Branche") ) == 0 ) {
		strcpy( a_kun_gx.kun_bran2, q );
		if ( a_kun_gx.kun_bran2[0] == '\0' )
		{
		    strcpy( a_kun_gx.kun_bran2, "0" );
		    fprintf( fperr, "Branche: NULL --> 0\n" );
		}
		ggdat.kdnr = atol( a_kun_gx.kun_bran2 );
	} else if ( memcmp( p, "Auftragsmengeneinheit", strlen("Auftragsmengeneinheit") ) == 0 ) {
		data.aeinheit = atol( q );
	} else if ( memcmp( p, "Auftragsmenge", strlen("Auftragsmenge") ) == 0 ) {
		data.amenge = atol( q );
	} else if ( memcmp( p, "Auftrag", strlen("Auftrag") ) == 0 ) {
		pazlsp.mdn = 1;
		pazlsp.ls = atol( q );
	} else if ( memcmp( p, "Kartonmengeneinheit", strlen("Kartonmengeneinheit") ) == 0 ) {
		data.keinheit = atol( q );
	} else if ( memcmp( p, "Kartonmenge", strlen("Kartonmenge") ) == 0 ) {
		data.up_ka = atol( q );
	} else if ( memcmp( p, "Palettenmengeneinheit", strlen("Palettenmengeneinheit") ) == 0 ) {
		data.paleinheit = atol( q );
	} else if ( memcmp( p, "Palettenmenge", strlen("Palettenmenge") ) == 0 ) {
		data.up_pal = atol( q );
	} else if ( memcmp( p, "Haltbarkeitstage", strlen("Haltbarkeitstage") ) == 0 ) {
		ggdat.haltbar1 = atol( q );
	} else if ( memcmp( p, "Datum1", strlen("Datum1") ) == 0 ) {
        strcpy( data.datum1, q );
		/*   i  = 0123456789 */
		/* q[i] = DD.MM.JJJJ */
		ggdat.datum1 = atol(data.datum1 + 8);         /* JJ */
		data.datum1[5] = '\0';
		ggdat.datum1 += atol(data.datum1 + 3) * 100;  /* MM */
		data.datum1[2] = '\0';
		ggdat.datum1 += atol(data.datum1) * 10000;    /* DD */
		/* Jetzt sollte sein: ggdat.datum1 = DDMMJJ */ 
		fprintf( fperr, "datum1: %d\n", ggdat.datum1 ); 
	} else if ( memcmp( p, "Datum2", strlen("Datum2") ) == 0 ) {
		strcpy( data.datum2, q );
		/*   i  = 0123456789 */
		/* q[i] = DD.MM.JJJJ */
		ggdat.datum2 = atol(data.datum2 + 8);         /* JJ */
		data.datum2[5] = '\0';
		ggdat.datum2 += atol(data.datum2 + 3) * 100;  /* MM */
		data.datum2[2] = '\0';
		ggdat.datum2 += atol(data.datum2) * 10000;    /* DD */
		/* Jetzt sollte sein: ggdat.datum2 = DDMMJJ */
		fprintf( fperr, "datum2: %d\n", ggdat.datum2 ); 
	} else if ( memcmp( p, "Charge", strlen("Charge") ) == 0 ) {
		strcpy( data.charge, q );
	} else if ( memcmp( p, "Partie", strlen("Partie") ) == 0 ) {
		data.partie = atol( q );		
	} else if ( memcmp( p, "Zul_nr_es2", strlen("Zul_nr_es2") ) == 0 ) {
		strcpy( data.es2num, q );		
	} else if ( memcmp( p, "Zul_nr_es3", strlen("Zul_nr_es3") ) == 0 ) {
		strcpy( data.es3num, q );		
	} else if ( memcmp( p, "Zul_nr_es", strlen("Zul_nr_es") ) == 0 ) {
		strcpy( data.esnum, q );		
	} else if ( memcmp( p, "Zul_nr_ez1", strlen("Zul_nr_ez1") ) == 0 ) {
		strcpy( data.ez1num, q );		
	} else if ( memcmp( p, "Zul_nr_ez2", strlen("Zul_nr_ez2") ) == 0 ) {
		strcpy( data.ez2num, q );		
	} else if ( memcmp( p, "Zul_nr_ez3", strlen("Zul_nr_ez3") ) == 0 ) {
		strcpy( data.ez3num, q );		
	} else if ( memcmp( p, "Tour", strlen("Tour") ) == 0 ) {
		data.tour = atol( q );
	} else if ( memcmp( p, "Preis", strlen("Preis") ) == 0 ) {
		ggdat.gpreis = 100 * atol( q );
		q = strchr( q, '.' );
		if ( q != NULL )
		{
		    *(q+3) = '\0'; 
		    ggdat.gpreis += atol( q+1 );
		}
		if (ggdat.gpreis < 0) ggdat.gpreis = 0;
		fprintf( fperr, "gpreis %ld\n", ggdat.gpreis );
	} else if ( memcmp( p, "Spreis", strlen("Spreis") ) == 0 ) {
		ggdat.spreis = 100 * atol( q );
		q = strchr( q, '.' );
		if ( q != NULL )
		{
		    *(q+3) = '\0'; 
		    ggdat.spreis += atol( q+1 );
		}
		if (ggdat.spreis < 0) ggdat.spreis = 0;
		fprintf( fperr, "spreis %ld\n", ggdat.spreis );
	} else if ( memcmp( p, "Gewicht", strlen("Gewicht") ) == 0 ) {
		ggdat.fgew = 1000 * atol( q );
		q = strchr( q, '.' );
		if ( q != NULL )
		{
		    *(q+4) = '\0';
		    ggdat.fgew += atol( q+1 );
		}
		fprintf( fperr, "fgew %ld\n", ggdat.fgew );
	} else if ( memcmp( p, "Tara", strlen("Tara") ) == 0 ) {
		ggdat.tara = 1000 * atol( q );
		q = strchr( q, '.' );
		if ( q != NULL )
		{
		    *(q+4) = '\0';
		    ggdat.tara += atol( q+1 );
		}
		fprintf( fperr, "DDE tara %ld\n", ggdat.tara );
	}	
}

void getfields( char *params )
{
	char parameter[80];
	char *p, *q;
	int sep;
	int len;
	sep = 0xA;
	p = params;
	vorbelegung();
	for (;;)
	{
		q = strchr( p, sep );
		if ( q == NULL )
			break;
		len = (int)(q - p) ;
		memcpy( parameter, p, len );
		memset( parameter + len, '\0', 1 );
		fprintf( fperr, "%s\n", parameter );
		evalparameter( parameter );
		p = q + 1;
	}
	q = strchr( p, '\0' );
	if ( q == NULL )
		return;
	len = (int)(q - p) ;
	memcpy( parameter, p, len );
	memset( parameter + len, '\0', 1 );
	fprintf( fperr, "%s\n", parameter );
	evalparameter( parameter );
}

int zeile( char *p, int sg, int ausrichtung, int nl, char *txt )
{
    if ( sg < 1 )
        return 0;
    if ( ausrichtung == ' ' )
    {
        return sprintf( p, "%s%c", txt, nl );   
    }
    if ( strcmp( anwender, "DFW" ) == 0 )
    {
        return sprintf( p, "%s", txt );
    }
    if ( sg < 99 )
    {
        return sprintf( p, "^%02hd;^A%c;%s%c", sg, ausrichtung, txt, nl );
    }
    else  /* SG = 99: reserviert fuer Bizerba Formelinterpreter */
    {
        return sprintf( p, "%s", txt );
    }
}

void formatatx( GGATX_t *ggatx, int ausrichtung, int nl )
{
    char *p;
    int zlen;
    zlen = 0;
    ggatx->atx[0] = '\0';
    p = ggatx->atx;
    
    if ( pr_ausz_gx.eti_typ == 2 && strcmp( anwender, "DFW" ) != 0 )
    {
        p += zlen;
        zlen = zeile( p, a_kun_gx.sg1, ausrichtung, '\n', a_kun_gx.a_bz3 );
        
        p += zlen;
        zlen = zeile( p, a_kun_gx.sg2, ausrichtung, '\n', a_kun_gx.a_bz4 );
        pZutaten = p + zlen;
    }
  
    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg1, ausrichtung, nl, pr_ausz_gx.txt1 );
    if ( zlen == 0 )
        return;

    if ( nl == ' ' )
    {
        /*
           101203 GK
           wo kein Zeilentrenner gibts ab Zeile 2 keine Ausrichtung
           und auch keine Schriftgroessensteuerung
         */
          
        ausrichtung = ' ';
    }
        
    p += zlen;            
    zlen = zeile( p, pr_ausz_gx.sg2, ausrichtung, nl, pr_ausz_gx.txt2 );
    if ( zlen == 0 )
        return;
        
    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg3, ausrichtung, nl, pr_ausz_gx.txt3 );
    if ( zlen == 0 )
        return;
        
    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg4, ausrichtung, nl, pr_ausz_gx.txt4 );
    if ( zlen == 0 )
        return;
        
    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg5, ausrichtung, nl, pr_ausz_gx.txt5 );
    if ( zlen == 0 )
        return;
        
    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg6, ausrichtung, nl, pr_ausz_gx.txt6 );
    if ( zlen == 0 )
        return;
        
    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg7, ausrichtung, nl, pr_ausz_gx.txt7 );
    if ( zlen == 0 )
        return;
        
    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg8, ausrichtung, nl, pr_ausz_gx.txt8 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg9, ausrichtung, nl, pr_ausz_gx.txt9 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg10, ausrichtung, nl, pr_ausz_gx.txt10 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg11, ausrichtung, nl, pr_ausz_gx.txt11 );
    if ( zlen == 0 )
        return;

    p += zlen;
    zlen = zeile( p, pr_ausz_gx.sg12, ausrichtung, nl, pr_ausz_gx.txt12 );
    if ( zlen == 0 )
        return;
}   

void replace( char *txt, char *oldtxt, char *newtxt )
{
    // A + Bold + C --> A + Bnew + C
    char *p, *q, *r;
    int oldlen, newlen, shiftlen;
    p = strstr( txt, oldtxt );
    if ( p == NULL )
        return;
    oldlen = strlen( oldtxt );
    newlen = strlen( newtxt );
    q = p + oldlen;
    r = p + newlen;
    shiftlen = strlen( q );
    memmove( r, q, shiftlen );
    r[shiftlen] = '\0';
    memcpy( p, newtxt, newlen );
}

int make_es( char *txt )
{
    char *p;
    p = txt;
    
    if ( data.esnum[0] == '\0' )
    {
         txt[0] = '\0';
         return 0;
    }
    
    /*
    if ( '0' <= data.esnum[0] && data.esnum[0] <= '9' )
    {
        p += sprintf( p, "ES " );
    }
    */
    
    p += sprintf( p, "%s", data.esnum );
    
    if ( data.es2num[0] != '\0' )
    {
        p += sprintf( p, ", %s", data.es2num );
    }
    
    if ( data.es3num[0] != '\0' )
    {
        p += sprintf( p, ", %s", data.es3num );
    }
    
    return (p - txt);
}

int make_ez( char *txt )
{
    char *p;
    p = txt;
    
    if ( data.ez1num[0] == '\0' )
    {
         txt[0] = '\0';
         return 0;
    }
    
    /*
    if ( '0' <= data.ez1num[0] && data.ez1num[0] <= '9' )
    {
        p += sprintf( p, "EZ " );
    }
    */
    
    p += sprintf( p, "%s", data.ez1num );
    
    if ( data.ez2num[0] != '\0' )
    {
        p += sprintf( p, ", %s", data.ez2num );
    }
    
    if ( data.ez3num[0] != '\0' )
    {
        p += sprintf( p, ", %s", data.ez3num );
    }
    
    return (p - txt);
}

void BauerngutLabelData( void )
{
    int ausrichtung;
    int zeilenvorschub;
	sprintf( ggdat.cts1, "%.0lf", a_kun_gx.ean );
	ggdat.cts1len = (us) strlen( ggdat.cts1 );
	if ( a_kun_gx.ean1 > 0 )
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean1 );
	else
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean ); 
	ggdat.cts2len = (us) strlen( ggdat.cts2 );

	if ( a_kun_gx.cab > 0 )
	{
		ggcab1.cabnr = a_kun_gx.cab;
	}
	else
	{
		ggcab1.cabnr = 1;
	}	
	if ( a_kun_gx.cab1 > 0 )
	{
		ggcab2.cabnr = a_kun_gx.cab1;
	}
	else
	{
		ggcab2.cabnr = ggcab1.cabnr;
	}
	ggdat.cabnr1 = ggcab1.cabnr;
	ggdat.cabnr2 = ggcab2.cabnr;
	
	ggdat.etin = a_kun_gx.eti_typ;
	ggdat.ets1 = a_kun_gx.eti_sum1;
	ggdat.ets2 = a_kun_gx.eti_sum2;
	
	ggdat.prbe = (a_kun_gx.pr_rech_kz[0] == 'J' || a_kun_gx.pr_rech_kz[0] == 'j') ? 1 : 0 ;

	Query_a_bas();
	
	/*
	if ( KuBaTara[0] == 'J' )
	{
		ggdat.tara = (long)(1000.0 * a_kun_gx.tara);
		fprintf( fperr, "KuBa tara %ld\n", ggdat.tara );
	}
	*/

	ggdat.ampar = a_kun_gx.ampar;
	if (a_kun_gx.gwpar > 0)
	{
	    ggdat.gwpar = a_kun_gx.gwpar;
    }
    else
    {
        ggdat.gwpar = 0xFFFFFFFF;
    }
    ggdat.aart = a_kun_gx.ausz_art;
 	
 	if ( a_kun_gx.sonder_eti > 0 && a_kun_gx.sonder_eti < 10 )
 	{
	    ggdat.sonder_eti_krit = a_kun_gx.sonder_eti;
	}
	else
	{
	    ggdat.sonder_eti_krit = 0;
	}

	ggdat.sum1vwa = 1; /* Stueck */
	ggdat.sum1vw = data.up_ka;

    ggdat.sum2vwa = ggdat.sum1vwa;
    if ( data.charg_hand > 0 )
    {
        ggdat.sum2vw = ggdat.sum1vw;
    }
    else
    {
        ggdat.sum2vw = 0;
    }
    /* 20100429 gk */
    ggdat.sum2vw = data.up_pal;
    /* 20100528 gk */
 	if ( ggdat.sum2vw > 0 )
 	{
 	    ggdat.sum2vwa = 8;
    }
    else
 	{
 	    ggdat.sum2vwa = 1;
    }
	/* Vorgabe Summe 3 in Anzahl Summe1 */
	ggdat.sum3vwa = 8;
	ggdat.sum3vw = data.amenge;
	
	/* Vorgabe Summe 3 in Stueck  */
	/*
	ggdat.sum3vwa = 1;
	ggdat.sum3vw = data.amenge * data.up_ka;
    */

    Query_kun();
    
    ggdat.zutaten = a_kun_gx.geb_eti[0];
        
/*  Adressen eti_typ 1 --> ATX1 */
    ggatx1.atxnr = 100000 + a_kun_gx.kopf_text;
    ggdat.atxnr1 = ggatx1.atxnr;
	Query_pr_ausz_gx( a_kun_gx.kopf_text, 1 );
	ausrichtung = frmtxt1[0];
	zeilenvorschub = (frmtxt1[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx1, ausrichtung, zeilenvorschub );
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    if ( ggatx1.atxlen == 0 )
    {
        strcpy( ggatx1.atx, "  " );       
    }
    ggatx1.atxlen = (us) strlen(ggatx1.atx);

/*  Bez. 1, 2 + Zutaten eti_typ2 --> ATX2 */
    ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr2 = ggatx2.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 2 );
	ausrichtung = frmtxt2[0];
	zeilenvorschub = (frmtxt2[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx2, ausrichtung, zeilenvorschub );
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
    if ( ggatx2.atxlen == 0 )
    {
        strcpy( ggatx2.atx, "  " );       
    }
    ggatx2.atxlen = (us) strlen(ggatx2.atx);

/*  Mengen  eti_typ 3 --> ATX3 */
/*  Mail von C. Roesener. TF3 wurde nicht bisher nicht benutzt. Jetzt sollen Herkunftsdaten hierein.
 *  20111111 GK
 */
/*  Herkunft eti_typ 3 --> ATX7 */
    ggatx3.atxnr = 300000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr3 = ggatx3.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 3 );
	ausrichtung = frmtxt3[0];
	zeilenvorschub = (frmtxt3[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx3, ausrichtung, zeilenvorschub );
	
    if ( make_es( txtNR ) > 0 )
    {
        replace( ggatx3.atx, "##ESNR##", txtNR );
    }
    if ( make_ez( txtNR ) > 0)
    {
        replace( ggatx3.atx, "##EZNR##", txtNR );
    }
    if ( data.partie > 0 )
    {
        sprintf( txtNR, "%ld", data.partie );
        replace( ggatx3.atx, "##IDENTNR##", txtNR );
    }
    
    ggatx3.atxlen = (us) strlen(ggatx3.atx);
    if ( ggatx3.atxlen == 0 )
    {
        strcpy( ggatx3.atx, "  " );       
    }
    ggatx3.atxlen = (us) strlen(ggatx3.atx);
    fprintf( fperr, "ggatx7: len=%d\n%s\n", ggatx3.atxlen, ggatx3.atx );

/*  Zubereitung eti_typ 4 --> ATX4 */
    ggatx4.atxnr = 400000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr4 = ggatx4.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 4 );
	ausrichtung = frmtxt4[0];
	zeilenvorschub = (frmtxt4[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx4, ausrichtung, zeilenvorschub );
    ggatx4.atxlen = (us) strlen(ggatx4.atx);
    if ( ggatx4.atxlen == 0 )
    {
        strcpy( ggatx4.atx, "  " );       
    }
    ggatx4.atxlen = (us) strlen(ggatx4.atx);

/*  Sondertext eti_typ 5 --> ATX5 */
    ggatx5.atxnr = 500000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr5 = ggatx5.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 5 );
	ausrichtung = frmtxt5[0];
	zeilenvorschub = (frmtxt5[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx5, ausrichtung, zeilenvorschub );
    ggatx5.atxlen = (us) strlen(ggatx5.atx);
    if ( ggatx5.atxlen == 0 )
    {
        strcpy( ggatx5.atx, "  " );       
    }
    ggatx5.atxlen = (us) strlen(ggatx5.atx);

/*  Herkunft --> ATX7 */
/*  Mail von C. Roesener. Herkunft nach TF3 verlagert.
 *  Jetzt kommen hier in TF7 nur 2 Leerzeichen rein.
 *  20111111 GK
 */

    ggatx7.atxnr = 700000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr7 = ggatx7.atxnr;
/*
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 7 );
	ausrichtung = frmtxt7[0];
	zeilenvorschub = (frmtxt7[1] == '-') ? (' ') : ('\n');
	
	formatatx( &ggatx7, ausrichtung, zeilenvorschub );
	
    if ( make_es( txtNR ) > 0 )
    {
        replace( ggatx7.atx, "##ESNR##", txtNR );
    }
    if ( make_ez( txtNR ) > 0)
    {
        replace( ggatx7.atx, "##EZNR##", txtNR );
    }
    if ( data.partie > 0 )
    {
        sprintf( txtNR, "%ld", data.partie );
        replace( ggatx7.atx, "##IDENTNR##", txtNR );
    }
    
    ggatx7.atxlen = (us) strlen(ggatx7.atx);
    if ( ggatx7.atxlen == 0 )
    {
        strcpy( ggatx7.atx, "  " );       
    }
 */
    strcpy( ggatx7.atx, "  " );       
    ggatx7.atxlen = (us) strlen(ggatx7.atx);
    fprintf( fperr, "ggatx7: len=%d\n%s\n", ggatx7.atxlen, ggatx7.atx );
    
/*  Charge --> ATX16 */
    ggatx16.atxnr = 650000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr16 = ggatx16.atxnr;
	// strcpy( ggatx16.atx, data.charge );
	sprintf( ggatx16.atx, "%ld", data.partie );    
    ggatx16.atxlen = (us) strlen(ggatx16.atx);
    if ( ggatx16.atxlen == 0 )
    {
        strcpy( ggatx16.atx, "  " );       
    }
    ggatx16.atxlen = (us) strlen(ggatx16.atx);

/*  Charge[0..3] --> ATX6 */
/*
    ggatx6.atxnr = 600000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr6 = ggatx6.atxnr;
    data.charge[4] = '\0';    
	strcpy( ggatx6.atx, data.charge );    
    ggatx6.atxlen = (us) strlen(ggatx6.atx);
    if ( ggatx6.atxlen == 0 )
    {
        strcpy( ggatx6.atx, "  " );       
    }
    ggatx6.atxlen = (us) strlen(ggatx6.atx);
*/
    
/*  MHD-Text eti_typ 11 --> ATX8 */
    ggatx8.atxnr = 800000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr8 = ggatx8.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 11 );
	ausrichtung = frmtxt8[0];
	zeilenvorschub = (frmtxt8[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx8, ausrichtung, zeilenvorschub );
    ggatx8.atxlen = (us) strlen(ggatx8.atx);
    if ( ggatx8.atxlen == 0 )
    {
        strcpy( ggatx8.atx, "  " );       
    }
    ggatx8.atxlen = (us) strlen(ggatx8.atx);

/*  Abpackungdatumstext eti_typ 12 --> ATX9 */
    ggatx9.atxnr = 900000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr9 = ggatx9.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 12 );
	ausrichtung = frmtxt9[0];
	zeilenvorschub = (frmtxt9[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx9, ausrichtung, zeilenvorschub );
    ggatx9.atxlen = (us) strlen(ggatx9.atx);
    if ( ggatx9.atxlen == 0 )
    {
        strcpy( ggatx9.atx, "  " );       
    }
    ggatx9.atxlen = (us) strlen(ggatx9.atx);

/*  Kundenartikelnummer --> ATX10 */
    ggatx10.atxnr = 950000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr10 = ggatx10.atxnr;
    strcpy( ggatx10.atx, data.kuna );
    ggatx10.atxlen = (us) strlen(ggatx10.atx);
    if ( ggatx10.atxlen == 0 )
    {
        strcpy( ggatx10.atx, "  " );       
    }
    ggatx10.atxlen = (us) strlen(ggatx10.atx);

/*  Kundenadresse --> ATX11 */
    ggatx11.atxnr = 0 ;
    ggdat.atxnr11 = ggatx11.atxnr;
    if ( kun.kun_gr1 != 0 )
    {
         sprintf( ggatx11.atx, "%s\n%s\n%s", kun.adr_nam1, kun.str, kun.ort1 );
    }
    else
    {
        strcpy( ggatx11.atx, "  " );       
    }
    ggatx11.atxlen = (us) strlen(ggatx11.atx);
    if ( ggatx11.atxlen == 0 )
    {
        strcpy( ggatx11.atx, "  " );       
    }
    ggatx11.atxlen = (us) strlen(ggatx11.atx);
}

void BoesingerLabelData( void )
{
    int ausrichtung;
    int zeilenvorschub;
	sprintf( ggdat.cts1, "%.0lf", a_kun_gx.ean );
	ggdat.cts1len = (us) strlen( ggdat.cts1 );
	if ( a_kun_gx.ean1 > 0 )
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean1 );
	else
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean ); 
	ggdat.cts2len = (us) strlen( ggdat.cts2 );

    /*	cab = 0 kein Barcode. Kann durchaus beabsichtigt sein. */
    ggcab1.cabnr = a_kun_gx.cab;
	if ( a_kun_gx.cab1 > 0 )
	{
		ggcab2.cabnr = a_kun_gx.cab1;
	}
	else
	{
		ggcab2.cabnr = ggcab1.cabnr;
	}
	ggdat.cabnr1 = ggcab1.cabnr;
	ggdat.cabnr2 = ggcab2.cabnr;
	ggdat.cabnr3 = a_kun_gx.cab_nve1;
	ggdat.cabnr4 = a_kun_gx.cab_nve2;

	ggdat.etin = a_kun_gx.eti_typ;
	ggdat.ets1 = a_kun_gx.eti_sum1;
	ggdat.ets2 = a_kun_gx.eti_sum2;
	ggdat.eti_nve1 = a_kun_gx.eti_nve1;
	ggdat.eti_nve2 = a_kun_gx.eti_nve2;
	
	ggdat.prbe = (a_kun_gx.pr_rech_kz[0] == 'J' || a_kun_gx.pr_rech_kz[0] == 'j') ? 1 : 0 ;
	
	/*
	if ( KuBaTara[0] == 'J' )
	{
		ggdat.tara = (long)(1000.0 * a_kun_gx.tara);
		fprintf( fperr, "KuBa tara %ld\n", ggdat.tara );
	}
	*/
	
    ggdat.aart = a_kun_gx.ausz_art;
 	
 	/* Vorgabe Summe 1 */
	if ( a_kun_gx.geb_fill == 2 )
	{
	    /* Gewichtsvorgabe */
	    ggdat.sum1vwa = 2;
	    ggdat.sum1vw = a_kun_gx.geb_anz;
	}
	else
	{
	    /* Stueckvorgabe */
	    ggdat.sum1vwa = 1;
	    ggdat.sum1vw = a_kun_gx.geb_anz / 1000;
	}
	
 	/* Vorgabe Summe 2 */
	if ( a_kun_gx.pal_fill == 5 )
	{
	    /* Kartonvorgabe */
 	    ggdat.sum2vwa = 8;
	    ggdat.sum2vw = a_kun_gx.pal_anz / 1000;
	}
	else if ( a_kun_gx.pal_fill == 2 )
	{
	    /* Gewichtsvorgabe */
 	    ggdat.sum2vwa = 2;
	    ggdat.sum2vw = a_kun_gx.pal_anz;
	}
 	else
	{
	    /* Stueckvorgabe */
 	    ggdat.sum2vwa = 1;
	    ggdat.sum2vw = a_kun_gx.pal_anz / 1000;
	}
       
    /* Boesinger: Ohne Vorgabe Summe 3, SUMMENVORWAHL[3] = 'N' */
	    
/*  Adressen eti_typ 1 --> ATX1 */
    ggatx1.atxnr = 100000 + a_kun_gx.text_nr2;
    ggdat.atxnr1 = ggatx1.atxnr;
	Query_pr_ausz_gx( a_kun_gx.kopf_text, 1 );
	ausrichtung = frmtxt1[0];
	zeilenvorschub = (frmtxt1[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx1, ausrichtung, zeilenvorschub );
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    if ( ggatx1.atxlen == 0 )
    {
        strcpy( ggatx1.atx, "  " );       
    }
    ggatx1.atxlen = (us) strlen(ggatx1.atx);

/*  Bez. 1 --> ATX2 */
    ggatx2.atxnr = 200000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr2 = ggatx2.atxnr;
    zeile( ggatx2.atx, a_kun_gx.sg1, 'c', '\n', a_kun_gx.a_bz3 );
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
    if ( ggatx2.atxlen == 0 )
    {
        strcpy( ggatx2.atx, "  " );       
    }
    ggatx2.atxlen = (us) strlen(ggatx2.atx);

/*  Bez. 2 --> ATX3 */
    ggatx3.atxnr = 300000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr3 = ggatx3.atxnr;
    zeile( ggatx3.atx, a_kun_gx.sg2, 'c', '\n', a_kun_gx.a_bz4 );
    ggatx3.atxlen = (us) strlen(ggatx3.atx);
    if ( ggatx3.atxlen == 0 )
    {
        strcpy( ggatx3.atx, "  " );       
    }
    ggatx3.atxlen = (us) strlen(ggatx3.atx);

/*  Zutaten eti_typ 1--> ATX4 */
    ggatx4.atxnr = 400000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr4 = ggatx4.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 1 );
	ausrichtung = frmtxt4[0];
	zeilenvorschub = (frmtxt4[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx4, ausrichtung, zeilenvorschub );
    ggatx4.atxlen = (us) strlen(ggatx4.atx);
    if ( ggatx4.atxlen == 0 )
    {
        strcpy( ggatx4.atx, "  " );       
    }
    ggatx4.atxlen = (us) strlen(ggatx4.atx);

/*  MHD Text eti_typ 5 --> ATX5 */
    ggatx5.atxnr = 500000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr5 = ggatx5.atxnr;
	Query_pr_ausz_gx( a_kun_gx.mhd_text, 1 );
	ausrichtung = frmtxt5[0];
	zeilenvorschub = (frmtxt5[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx5, ausrichtung, zeilenvorschub );
    ggatx5.atxlen = (us) strlen(ggatx5.atx);
    if ( ggatx5.atxlen == 0 )
    {
        strcpy( ggatx5.atx, "  " );       
    }
    ggatx5.atxlen = (us) strlen(ggatx5.atx);

/*  Kunden-Artikel-Nr --> ATX6 */
    ggatx6.atxnr = 600000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr6 = ggatx6.atxnr;
    zeile( ggatx6.atx, a_kun_gx.sg3, 'c', '\n', a_kun_gx.a_kun );
    ggatx6.atxlen = (us) strlen(ggatx6.atx);
    if ( ggatx6.atxlen == 0 )
    {
        strcpy( ggatx6.atx, "  " );       
    }
    ggatx6.atxlen = (us) strlen(ggatx6.atx);
    
/*  Artikel-Nr --> ATX7 */
    ggatx7.atxnr = 700000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr7 = ggatx7.atxnr;
    sprintf( a_kun_gx.a_kun, "%ld", a_kun_gx.a );
    zeile( ggatx7.atx, a_kun_gx.sg3, 'c', '\n', a_kun_gx.a_kun );
    ggatx7.atxlen = (us) strlen(ggatx7.atx);
    if ( ggatx7.atxlen == 0 )
    {
        strcpy( ggatx7.atx, "  " );       
    }
    ggatx7.atxlen = (us) strlen(ggatx7.atx);    
}

void LOHMANNLabelData( void )
{
    /* PLU wird aus GX DB aufgeschaltet */
    kun.kun = a_kun_gx.kun;
    kun.mdn = 1;
    kun.fil = 0;
    Query_kun();
    ggdat.kdnr = kun.stat_kun;

    ggdat.sum1vw = data.amenge;
    
    a_kun.a = a_kun_gx.a;
    a_kun.kun = a_kun_gx.kun;

    Query_a_kun();
    if ( a_kun.a_kun[0] > '0' )
    {
        ggdat.plunr = atol( a_kun.a_kun );
    }
    
    TimeMsgLine("LOHMANN PLU: %ld KUNDE %ld CHARGE %ld STUECK %ld\n",
        ggdat.plunr, ggdat.kdnr, ggdat.chargennr, ggdat.sum1vw );
    return;   
}

void OKLELabelData( void )
{
    /* PLU wird aus GX DB aufgeschaltet */
    kun.kun = a_kun_gx.kun;
    kun.mdn = 1;
    kun.fil = 0;
    Query_kun();
    ggdat.kdnr = kun.stat_kun;
    ggdat.chargennr = atol( data.charge );
 	fprintf( fperr, "OKLELabelData Chargenr: %ld\n", ggdat.chargennr );
    if ( ggdat.chargennr < 0 || ggdat.chargennr > 999999 )
    {
        ggdat.chargennr = 0;
    }
    Query_a_bas();
    if ( data.charg_hand == 0 )
    {
        ggdat.chargennr = 0;  // 02.04.2013 GK
    }
    
    ggdat.sum1vw = data.amenge;
      
    TimeMsgLine("OKLE PLU: %ld KUNDE %ld CHARGE %ld STUECK %ld\n",
        ggdat.plunr, ggdat.kdnr, ggdat.chargennr, ggdat.sum1vw );
    return;   
}

void DietzLabelData( void )
{   
    int ausrichtung;
    int zeilenvorschub;
	sprintf( ggdat.cts1, "%.0lf", a_kun_gx.ean );
	ggdat.cts1len = (us) strlen( ggdat.cts1 );
	if ( a_kun_gx.ean1 > 0 )
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean1 );
	else
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean ); 
	ggdat.cts2len = (us) strlen( ggdat.cts2 );

    /*	cab = 0 kein Barcode. Kann durchaus beabsichtigt sein. */
    ggcab1.cabnr = a_kun_gx.cab;
	if ( a_kun_gx.cab1 > 0 )
	{
		ggcab2.cabnr = a_kun_gx.cab1;
	}
	else
	{
		ggcab2.cabnr = ggcab1.cabnr;
	}
	ggdat.cabnr1 = ggcab1.cabnr;
	ggdat.cabnr2 = ggcab2.cabnr;
	ggdat.cabnr3 = a_kun_gx.cab_nve1;
	ggdat.cabnr4 = a_kun_gx.cab_nve2;

	ggdat.etin = a_kun_gx.eti_typ;
    if ( a_kun_gx.eti_typ < 1 )
    {
        /* Bizerba Standard Etikett 0 (68mm) */
        ggdat.etin = - 8191;
    }
    if ( a_kun_gx.eti_typ > 999 )
    {
        /* Bizerba Standard Etikettten */
        ggdat.etin = a_kun_gx.eti_typ - 1000 - 8191;
    }
	ggdat.ets1 = a_kun_gx.eti_sum1;
	ggdat.ets2 = a_kun_gx.eti_sum2;
	ggdat.eti_nve1 = a_kun_gx.eti_nve1;
	ggdat.eti_nve2 = a_kun_gx.eti_nve2;
	
	ggdat.prbe = (a_kun_gx.pr_rech_kz[0] == 'J' || a_kun_gx.pr_rech_kz[0] == 'j') ? 1 : 0 ;
	
    ggdat.aart = a_kun_gx.ausz_art;
    
    // Query_a_bas();

    Query_kun_geb();
  
	ggdat.tara = (long)(1000.0 * kun_geb.tara);	
	fprintf( fperr, "Tara = %ld g\n", ggdat.tara );
	
 	/* Vorgabe Summe 1 */
	if ( kun_geb.geb_fill == 2 )
	{
	    /* Gewichtsvorgabe */
	    ggdat.sum1vwa = 2;
	    ggdat.sum1vw = kun_geb.geb_anz;
	}
	else
	{
	    /* Stueckvorgabe */
	    ggdat.sum1vwa = 1;
	    ggdat.sum1vw = kun_geb.geb_anz / 1000;
	}
	
 	/* Vorgabe Summe 2 */
	if ( kun_geb.pal_fill == 5 )
	{
	    /* Kartonvorgabe */
 	    ggdat.sum2vwa = 8;
	    ggdat.sum2vw = kun_geb.pal_anz / 1000;
	}
	else if ( kun_geb.pal_fill == 2 )
	{
	    /* Gewichtsvorgabe */
 	    ggdat.sum2vwa = 2;
	    ggdat.sum2vw = kun_geb.pal_anz;
	}
 	else
	{
	    /* Stueckvorgabe */
 	    ggdat.sum2vwa = 1;
	    ggdat.sum2vw = kun_geb.pal_anz / 1000;
	}
	
	fprintf( fperr, "Vorgabe S1: (ME %d) %ld\n", ggdat.sum1vwa, ggdat.sum1vw );
	fprintf( fperr, "Vorgabe S2: (ME %d) %ld\n", ggdat.sum2vwa, ggdat.sum2vw );
      	    
/*  Adressen eti_typ 1 --> ATX1 */
    ggatx1.atxnr = 100000 + a_kun_gx.text_nr2;
    ggdat.atxnr1 = ggatx1.atxnr;
	Query_pr_ausz_gx( a_kun_gx.kopf_text, 1 );
	ausrichtung = frmtxt1[0];
	zeilenvorschub = (frmtxt1[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx1, ausrichtung, zeilenvorschub );
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    if ( ggatx1.atxlen == 0 )
    {
        strcpy( ggatx1.atx, "  " );       
    }
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    
/*  Bez. 1, 2 + Zutaten eti_typ2 --> ATX2, ATX3, ATX5 */
    ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr2 = ggatx2.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 2 );
	ausrichtung = frmtxt2[0];
	zeilenvorschub = (frmtxt2[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx2, ausrichtung, zeilenvorschub );
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
    if ( ggatx2.atxlen == 0 )
    {
        strcpy( ggatx2.atx, "  " );       
    }
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
    
    ggdat.chargennr = atol( data.charge );
    if ( ggdat.chargennr < 0 || ggdat.chargennr > 999999 )
    {
        ggdat.chargennr = 0;
    }
    else
    {
        strcat( ggatx2.atx, "\nIdent-Nr. " );
        strcat( ggatx2.atx, data.charge );
        ggatx2.atxlen = (us) strlen(ggatx2.atx);
    }
    if ( strcmp( anwender, "Dietz" ) == 0 )
    {
        memcpy( &ggatx3, &ggatx2, sizeof( ggatx2 ) );
        memcpy( &ggatx5, &ggatx2, sizeof( ggatx2 ) );
    }
    
/*  MHD Text eti_typ 3 --> ATX4, DTX1, DTX2 */
    ggatx4.atxnr = 400000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr4 = ggatx4.atxnr;
	Query_pr_ausz_gx( a_kun_gx.mhd_text, 3 );
	ausrichtung = frmtxt4[0];
	zeilenvorschub = (frmtxt4[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx4, ausrichtung, zeilenvorschub );
    ggatx4.atxlen = (us) strlen(ggatx4.atx);
    if ( ggatx4.atxlen == 0 )
    {
        strcpy( ggatx4.atx, "  " );       
    }
    ggatx4.atxlen = (us) strlen(ggatx4.atx);
    
}

void DFWLabelData( void )
{   
    int ausrichtung;
    int zeilenvorschub;
	sprintf( ggdat.cts1, "%.0lf", a_kun_gx.ean );
	ggdat.cts1len = (us) strlen( ggdat.cts1 );
	if ( a_kun_gx.ean1 > 0 )
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean1 );
	else
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean ); 
	ggdat.cts2len = (us) strlen( ggdat.cts2 );

    /*	cab = 0 kein Barcode. Kann durchaus beabsichtigt sein. */
    ggcab1.cabnr = a_kun_gx.cab;
	if ( a_kun_gx.cab1 > 0 )
	{
		ggcab2.cabnr = a_kun_gx.cab1;
	}
	else
	{
		ggcab2.cabnr = ggcab1.cabnr;
	}
	ggdat.cabnr1 = ggcab1.cabnr;
	ggdat.cabnr2 = ggcab2.cabnr;
	ggdat.cabnr3 = a_kun_gx.cab_nve1;
	ggdat.cabnr4 = a_kun_gx.cab_nve2;
	fprintf( fperr, "CAB1, EAN1 = %d, %s\n", ggdat.cabnr1, ggdat.cts1 );
	fprintf( fperr, "CAB1, EAN1 = %d, %s\n", ggdat.cabnr2, ggdat.cts2 );

	ggdat.etin = a_kun_gx.eti_typ;
    if ( a_kun_gx.eti_typ < 1 )
    {
        /* Bizerba Standard Etikett 0 (68mm) */
        ggdat.etin = - 8191;
    }
    if ( a_kun_gx.eti_typ > 999 )
    {
        /* Bizerba Standard Etikettten */
        ggdat.etin = a_kun_gx.eti_typ - 1000 - 8191;
    }
	ggdat.ets1 = a_kun_gx.eti_sum1;
	ggdat.ets2 = a_kun_gx.eti_sum2;
	ggdat.eti_nve1 = a_kun_gx.eti_nve1;
	ggdat.eti_nve2 = a_kun_gx.eti_nve2;
	
	ggdat.prbe = (a_kun_gx.pr_rech_kz[0] == 'J' || a_kun_gx.pr_rech_kz[0] == 'j') ? 1 : 0 ;
	
    ggdat.aart = a_kun_gx.ausz_art;
    // Espera hat aart 0 - 5, Bizerba nur 0 - 3
    // Handling entspr Mail von H. Mauer vom 21.01.09 
    if ( ggdat.aart == 4 ) ggdat.aart = 2;
    if ( ggdat.aart == 5 ) ggdat.aart = 3;
    
    
    // wegen Espera
	ggdat.sonder_eti_krit = (us) a_kun_gx.ampar;
	if ( ggdat.sonder_eti_krit < 1 ) ggdat.sonder_eti_krit = 3;
	if ( ggdat.sonder_eti_krit > 3 ) ggdat.sonder_eti_krit = 3;

    // 15.12.2008 GK Absprache mit Herrn Mauer, Verwechselung nicht sehr schoen
	ggdat.ampar = a_kun_gx.sonder_eti;
    
    Query_a_bas();
	fprintf( fperr, "BW,EW,KH,FT = %s, %d g,%d g,%d g\n", data.brennwert,data.eiweiss,data.kh,data.fett );

    // Query_kun_geb();
    
    ggdat.haltbar1 = a_kun_gx.hbk_ztr;
	ggdat.tara = (long)(1000.0 * a_kun_gx.tara);
	ggdat.fgew = (long)(1000.0 * a_kun_gx.inh);
	if ( ggdat.tara < 0L || ggdat.tara > 999999L ) ggdat.tara = 0;	
	if ( ggdat.fgew < 0L || ggdat.fgew > 999999L ) ggdat.fgew = 0;	
	fprintf( fperr, "Tara = %ld g Festgew = %ld g\n", ggdat.tara, ggdat.fgew );
		
 	/* Vorgabe Summe 1  immer Stueck */
    ggdat.sum1vwa = 1;
	ggdat.sum1vw = a_kun_gx.geb_anz / 1000;
	if ( ggdat.sum1vw < 0L || ggdat.sum1vw > 999L ) ggdat.sum1vw = 0;	
	
 	/* Vorgabe Summe 2  immer Kartons */
 	ggdat.sum2vwa = 8;
	ggdat.sum2vw = a_kun_gx.pal_anz / 1000;
	if ( ggdat.sum2vw < 0L || ggdat.sum2vw > 999L ) ggdat.sum2vw = 0;	
	
	fprintf( fperr, "Vorgabe S1: (ME %d) %ld\n", ggdat.sum1vwa, ggdat.sum1vw );
	fprintf( fperr, "Vorgabe S2: (ME %d) %ld\n", ggdat.sum2vwa, ggdat.sum2vw );
      	    
/*  Adressen eti_typ 2 --> ATX1 */
    ggatx1.atxnr = 100000 + a_kun_gx.text_nr2;
    ggdat.atxnr1 = ggatx1.atxnr;
	Query_pr_ausz_gx( a_kun_gx.kopf_text, 2 );
	ausrichtung = frmtxt1[0];
	zeilenvorschub = (frmtxt1[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx1, ausrichtung, zeilenvorschub );
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    if ( ggatx1.atxlen == 0 )
    {
        strcpy( ggatx1.atx, "  " );       
    }
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    
/*  Bez. 1, 2 --> ATX2 */
    ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr2 = ggatx2.atxnr;
    // Mauer 19.01.09 sprintf( ggatx2.atx, "%s\\n%s\\n", a_kun_gx.a_bz3, a_kun_gx.a_bz4 );
    sprintf( ggatx2.atx, "%s\\n", a_kun_gx.a_bz3 );
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
    if ( ggatx2.atxlen == 0 )
    {
        strcpy( ggatx2.atx, "  " );       
    }
    ggatx2.atxlen = (us) strlen(ggatx2.atx);   
    
/*  MHD Text eti_typ 2 --> ATX3 */
    ggatx3.atxnr = 400000 + a_kun_gx.text_nr2 ;
    ggdat.atxnr3 = ggatx3.atxnr;
	Query_pr_ausz_gx( a_kun_gx.mhd_text, 2 );
	ausrichtung = frmtxt3[0];
	zeilenvorschub = (frmtxt3[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx3, ausrichtung, zeilenvorschub );
    ggatx3.atxlen = (us) strlen(ggatx3.atx);
    if ( ggatx3.atxlen == 0 )
    {
        strcpy( ggatx3.atx, "  " );       
    }
    ggatx3.atxlen = (us) strlen(ggatx3.atx);
    
/*  Zutaten eti_typ2 --> ATX4 */
    ggatx4.atxnr = 400000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr4 = ggatx4.atxnr;
	Query_pr_ausz_gx( a_kun_gx.text_nr / 100, 2 );
	ausrichtung = frmtxt2[0];
	zeilenvorschub = (frmtxt2[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx4, ausrichtung, zeilenvorschub );
    ggatx4.atxlen = (us) strlen(ggatx4.atx);
    if ( ggatx4.atxlen == 0 )
    {
        strcpy( ggatx4.atx, "  " );       
    }
    ggatx4.atxlen = (us) strlen(ggatx4.atx);
    
/*  Freitext1 eti_typ2 --> ATX5 */
    ggatx5.atxnr = 500000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr5 = ggatx5.atxnr;
	Query_pr_ausz_gx( a_kun_gx.freitext1, 2 );
	ausrichtung = frmtxt2[0];
	zeilenvorschub = (frmtxt2[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx5, ausrichtung, zeilenvorschub );
    ggatx5.atxlen = (us) strlen(ggatx5.atx);
    if ( ggatx5.atxlen == 0 )
    {
        strcpy( ggatx5.atx, "  " );       
    }
    ggatx5.atxlen = (us) strlen(ggatx5.atx);

/*  Fett --> ATX17 */
    if ( data.fett > 0 )
    {
        wsprintf( ggatx17.atx, "%d", data.fett );
    }
    else
    {
        wsprintf( ggatx17.atx, "  " );
    }
    ggatx17.atxlen = (us) strlen(ggatx17.atx);
        
/*  Kohlenhydrate --> ATX18 */
    if ( data.kh > 0 )
    {
        wsprintf( ggatx18.atx, "%d", data.kh );
    }
    else
    {
        wsprintf( ggatx18.atx, "  " );
    }
    ggatx18.atxlen = (us) strlen(ggatx18.atx);
        
/*  Eiweiss --> ATX19 */
    if ( data.eiweiss > 0 )
    {
        wsprintf( ggatx19.atx, "%d", data.eiweiss );
    }
    else
    {
        wsprintf( ggatx19.atx, "  " );
    }
    ggatx19.atxlen = (us) strlen(ggatx19.atx);
        
/*  Brennwert --> ATX20 */
    wsprintf( ggatx20.atx, "%s", data.brennwert );
    ggatx20.atxlen = (us) strlen(ggatx20.atx);
        
}

void KUEBLERLabelData( int fehlerfall )
{ 
    char *p;   
    // int ausrichtung;
    // int zeilenvorschub;
    
    // CTS1
    if ( a_kun_gx.ean > 0 )
    {
	    sprintf( ggdat.cts1, "%.0lf", a_kun_gx.ean );
	}
	else
	{
	    if ( a_kun_gx.a < 10000 )
	    {
	        sprintf( ggdat.cts1, "21%4.0lf000000", a_kun_gx.a );
	    }
	    else
	    {
	        sprintf( ggdat.cts1, "21%.0lf000000", a_kun_gx.a );
	    }
	}
	ggdat.cts1len = (us) strlen( ggdat.cts1 );
	
	// CTS2
	if ( a_kun_gx.ean1 > 0 )
		sprintf( ggdat.cts2, "%.0lf", a_kun_gx.ean1 );
	else
		strcpy( ggdat.cts2, ggdat.cts2 ); 
	ggdat.cts2len = (us) strlen( ggdat.cts2 );

    // CAB1
    ggcab1.cabnr = a_kun_gx.cab;
    if ( ggcab1.cabnr == 0 ) ggcab1.cabnr = 1;
    
    // CAB2
	ggcab2.cabnr = ggcab1.cabnr;
	if ( a_kun_gx.cab1 > 0 ) ggcab2.cabnr = a_kun_gx.cab1;

	ggdat.cabnr1 = ggcab1.cabnr;
	ggdat.cabnr2 = ggcab2.cabnr;
	ggdat.cabnr3 = a_kun_gx.cab_nve1;
	ggdat.cabnr4 = a_kun_gx.cab_nve2;
	fprintf( fperr, "CAB1, EAN1 = %d, %s\n", ggdat.cabnr1, ggdat.cts1 );
	fprintf( fperr, "CAB2, EAN2 = %d, %s\n", ggdat.cabnr2, ggdat.cts2 );

	ggdat.etin = a_kun_gx.eti_typ;
    if ( a_kun_gx.eti_typ < 1 )
    {
        /* Bizerba Standard Etikett 0 (68mm) */
        ggdat.etin = - 8191;
    }
    if ( a_kun_gx.eti_typ > 999 )
    {
        /* Bizerba Standard Etikettten */
        ggdat.etin = a_kun_gx.eti_typ - 1000 - 8191;
    }
	ggdat.ets1 = a_kun_gx.eti_sum1;
	ggdat.ets2 = a_kun_gx.eti_sum2;
	ggdat.eti_nve1 = a_kun_gx.eti_nve1;
	ggdat.eti_nve2 = a_kun_gx.eti_nve2;
	
	ggdat.prbe = (a_kun_gx.pr_rech_kz[0] == 'J' || a_kun_gx.pr_rech_kz[0] == 'j') ? 1 : 0 ;
	
    ggdat.aart = a_kun_gx.ausz_art;

    Query_a_bas();
	fprintf( fperr, "BW,EW,KH,FT = %s, %4.2f g, %4.2f g, %4.2f g\n",
	         data.brennwert, data.eiweiss, data.kh, data.fett );

    Query_a_bas_erw();
	fprintf( fperr, "%s %s: GsFs = %4.2f g, Zucker = %4.2f g\n",
	    a_bas_erw.pp_a_bz1, a_bas_erw.pp_a_bz2, a_bas_erw.davonfett, a_bas_erw.davonzucker );

    kun.kun = a_kun_gx.kun;
    kun.mdn = a_kun_gx.mdn;
    kun.fil = 0;
    Query_kun();
    
    /* kommt per DDE
    if ( fehlerfall )
    {
        ggdat.haltbar1 = data.hbk_ztr;
 	    ggdat.tara = (long)(1000.0 * data.tara);
	    ggdat.fgew = (long)(1000.0 * data.inh);
    }
    else
    {
        ggdat.haltbar1 = a_kun_gx.hbk_ztr;
 	    ggdat.tara = (long)(1000.0 * a_kun_gx.tara);
	    ggdat.fgew = (long)(1000.0 * a_kun_gx.inh);
    }
	if ( ggdat.tara < 0L || ggdat.tara > 999999L ) ggdat.tara = 0;	
	if ( ggdat.fgew < 0L || ggdat.fgew > 999999L ) ggdat.fgew = 0;	
	*/
	fprintf( fperr, "Tara = %ld g Festgew = %ld g\n", ggdat.tara, ggdat.fgew );
		
 	/* Vorgabe Summe 1  immer Stueck */
    ggdat.sum1vwa = 1;
	ggdat.sum1vw = a_kun_gx.geb_anz / 1000;
	if ( ggdat.sum1vw < 0L || ggdat.sum1vw > 999L ) ggdat.sum1vw = 0;	
	
 	/* Vorgabe Summe 2  immer Kartons */
 	ggdat.sum2vwa = 8;
	ggdat.sum2vw = a_kun_gx.pal_anz / 1000;
	if ( ggdat.sum2vw < 0L || ggdat.sum2vw > 999L ) ggdat.sum2vw = 0;	
	
	fprintf( fperr, "Vorgabe S1: (ME %d) %ld\n", ggdat.sum1vwa, ggdat.sum1vw );
	fprintf( fperr, "Vorgabe S2: (ME %d) %ld\n", ggdat.sum2vwa, ggdat.sum2vw );
      	    
/*  Adresse --> ATX1 */
    // ggatx1.atxnr = 100000;
    // ggdat.atxnr1 = ggatx1.atxnr;
    sprintf( ggatx1.atx, "%s %s@0A%s . %s %s",
        kun.adr_nam1, kun.adr_nam2, kun.str, kun.plz, kun.ort1 );
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    if ( ggatx1.atxlen == 0 )
    {
        strcpy( ggatx1.atx, "  " );       
    }
    ggatx1.atxlen = (us) strlen(ggatx1.atx);
    
/*  Bez. 1, 2, Allergene, Zutaten --> ATX2 */
    // ggatx2.atxnr = 200000;
    // ggdat.atxnr2 = ggatx2.atxnr;
    p = ggatx2.atx;
    p += sprintf( p,"^12;%s@0A^8%s@0A", a_bas_erw.pp_a_bz1, a_bas_erw.pp_a_bz2 );
    /*
    if ( a_bas_erw.pp_a_bz1[0] > ' ' )
    {
        p += sprintf( p,"^12;%s@0A^8%s@0A", a_bas_erw.pp_a_bz1, a_bas_erw.pp_a_bz2 );
    }
    else
    {
        p += sprintf( p,"^12;%s@0A^8%s@0A", data.a_bz1, data.a_bz2 );
    }
    */
    p += sprintf( p,"^6%s@0A%s@0A", allergene(), a_bas_erw.zutat );
    p += sprintf( p, "Hergestellt ohne Geschmacksverstärker u. ohne Farbstoffe." );
    ggatx2.atxlen = (us) strlen(ggatx2.atx);
    if ( ggatx2.atxlen == 0 )
    {
        strcpy( ggatx2.atx, "  " );       
    }
    ggatx2.atxlen = (us) strlen(ggatx2.atx);   
    
/*  MHD Text --> ATX7 */
    if ( ! fehlerfall && a_kun_gx.mhd_text > 0 )
    {
	    Query_pr_ausz_gx( a_kun_gx.mhd_text, 2 );
        sprintf( ggatx7.atx, "%s %s", pr_ausz_gx.txt1, pr_ausz_gx.txt2 );
	}
    else
    {
        sprintf( ggatx7.atx, "%s", a_bas_erw.lgr_tmpr );
    }
    ggatx7.atxlen = (us) strlen(ggatx7.atx);
    if ( ggatx7.atxlen == 0 )
    {
        strcpy( ggatx7.atx, "  " );       
    }
    ggatx7.atxlen = (us) strlen(ggatx7.atx);
      
/*  Freitext1 eti_typ2 --> ATX5 */
    /*
    ggatx5.atxnr = 500000 + a_kun_gx.text_nr / 100 ;
    ggdat.atxnr5 = ggatx5.atxnr;
	Query_pr_ausz_gx( a_kun_gx.freitext1, 2 );
	ausrichtung = frmtxt2[0];
	zeilenvorschub = (frmtxt2[1] == '-') ? (' ') : ('\n');    
	formatatx( &ggatx5, ausrichtung, zeilenvorschub );
    ggatx5.atxlen = (us) strlen(ggatx5.atx);
    if ( ggatx5.atxlen == 0 )
    {
        strcpy( ggatx5.atx, "  " );       
    }
    ggatx5.atxlen = (us) strlen(ggatx5.atx);
    */
    
/*  Fett --> ATX12 */
    if ( data.fett > 0 )
    {
        sprintf( ggatx12.atx, "%4.2f", data.fett );
        substitutechar( ggatx12.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx12.atx, "  " );
    }
    ggatx12.atxlen = (us) strlen(ggatx12.atx);
        
/*  Kohlenhydrate --> ATX13 */
    if ( data.kh > 0 )
    {
        sprintf( ggatx13.atx, "%4.2f", data.kh );
        substitutechar( ggatx13.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx13.atx, "  " );
    }
    ggatx13.atxlen = (us) strlen(ggatx13.atx);
        
/*  Eiweiss --> ATX14 */
    if ( data.eiweiss > 0 )
    {
        sprintf( ggatx14.atx, "%4.2f", data.eiweiss );
        substitutechar( ggatx14.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx14.atx, "  " );
    }
    ggatx14.atxlen = (us) strlen(ggatx14.atx);
        
/*  Brennwert --> ATX15 */
    wsprintf( ggatx15.atx, "%s", data.brennwert );
    ggatx15.atxlen = (us) strlen(ggatx15.atx);
    
/*  Gesaettigte Fettsaeuren --> ATX16 */
    if ( a_bas_erw.davonfett > 0 )
    {
        sprintf( ggatx16.atx, "%4.2f", a_bas_erw.davonfett );
        substitutechar( ggatx16.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx16.atx, "  " );
    }
    ggatx16.atxlen = (us) strlen(ggatx16.atx);

/*  Zucker --> ATX17 */
    if ( a_bas_erw.davonzucker > 0 )
    {
        sprintf( ggatx17.atx, "%4.2f", a_bas_erw.davonzucker );
        substitutechar( ggatx17.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx17.atx, "  " );
    }
    ggatx17.atxlen = (us) strlen(ggatx17.atx);
    
/*  Salz --> ATX18 */
    if ( a_bas_erw.davonzucker > 0 )
    {
        sprintf( ggatx18.atx, "%4.2f", a_bas_erw.davonzucker );
        substitutechar( ggatx18.atx, '.', ',' );
    }
    else
    {
        wsprintf( ggatx18.atx, "  " );
    }
    ggatx18.atxlen = (us) strlen(ggatx18.atx);
        
}

void getLabelData( void )
{
	init_a_kun_gx();
    if ( strcmp( anwender, "OKLE" ) == 0 )
    {
        OKLELabelData();
        return;
    }
    else if ( strcmp( anwender, "LOHMANN" ) == 0 )
    {
        LOHMANNLabelData();
        return;
    }
 	if ( a_kun_gx.kun == 0 )
 	{
	    fehlerfall = ! Query_a_kun_gx_ab();	
	    /* 20051220 GK */
	    if ( fehlerfall != 0  )
	        {
	            strcpy( a_kun_gx.kun_bran2, "0" );
	            fehlerfall = ! Query_a_kun_gx_ab();
	        }
	}
	else
 	{
 	    /* Bauerngut: Wenn kun != 0 uebergeben wurde, dann muss es auch ein Kundenetikett geben.
 	     * Und Kuebler!
 	     */
	    fehlerfall = ! Query_a_kun_gx_ak();
	    if ( strcmp( anwender, "Boesinger" ) == 0
	         || strcmp( anwender, "Dietz" ) == 0
	         || strcmp( anwender, "DFW" ) == 0 )
	    {
	    	if ( fehlerfall != 0  )
	        {
	            fehlerfall = ! Query_a_kun_gx_ab();
	            /* 20051220 GK */
	            if ( fehlerfall != 0  )
	            {
	                strcpy( a_kun_gx.kun_bran2, "0" );
	                fehlerfall = ! Query_a_kun_gx_ab();
	            }
	        }
	    }
	}
	
	if (strcmp( anwender, "KUEBLER" ) == 0 )
	{
	    // Kuebler: a_kun_gx Satz muss nicht notwendig da sein.
	    KUEBLERLabelData( fehlerfall );
	    return;
	}

	if ( fehlerfall != 0  )
	{
        /*  Fehlermeldung --> ATX2 */
        ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
        ggdat.atxnr2 = ggatx2.atxnr;
        sprintf( ggatx2.atx, "^14;^Ac;FEHLER\nArtikel %.0ld ohne Daten\n", (long) a_kun_gx.a );   
        ggatx2.atxlen = (us) strlen(ggatx2.atx);
		TimeMsgLine( "FEHLER\nArtikel %.0ld ohne Daten\n", (long) a_kun_gx.a );
	}
	else if ( ggdat.etin == 0 )
	{
        /*  Fehlermeldung --> ATX2 */
        ggdat.etin = 1;
        ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
        ggdat.atxnr2 = ggatx2.atxnr;
        sprintf( ggatx2.atx, "^14;^Ac;FEHLER\nArtikel %.0ld ohne Layout\n", (long) a_kun_gx.a );   
        ggatx2.atxlen = (us) strlen(ggatx2.atx);
		TimeMsgLine( "FEHLER\nArtikel %.0ld ohne Layout\n", (long) a_kun_gx.a );
	}
	else if (strcmp( anwender, "Boesinger" ) == 0 )
	{
	    BoesingerLabelData();
	}
	else if (strcmp( anwender, "Dietz" ) == 0 )
	{
	    DietzLabelData();
	}
	else if (strcmp( anwender, "DFW" ) == 0 )
	{
	    DFWLabelData();
	}
	else /* Bauerngut */
	{        
        BauerngutLabelData();
    }  
}

int getKunData( void )
{
	nArtikel = 0;
	
	strcpy( a_kun_gx.kun_bran2, "0" );

	if ( fetch_a_kun_gx_kb() == 0 )
	    return 0;
    Query_a_bas();
    paz_ipr.a = a_kun_gx.a;
    paz_ipr.bran_kun = a_kun_gx.kun;
    if ( Query_paz_ipr() == 0 )
    {
        kun.kun = a_kun_gx.kun;
        Query_kun();
        paz_ipr.bran_kun = atol( kun.kun_bran2 );
        if ( Query_paz_ipr() == 0 )
        {
            paz_ipr.bran_kun = 0;
            if ( Query_paz_ipr() == 0 )
            {
                paz_ipr.ld_pr = 0;
            }
        }
    }
    
    ggdat.gpreis = (long)(100.0 * paz_ipr.ld_pr);
     
    BauerngutLabelData();

    nArtikel++;

	fprintf( fperr, "%5ld. Artikel: A: %7ld BK: %7ld, P: %7ld wird gesendet ...\n",
	    	  nArtikel, ggdat.plunr, paz_ipr.bran_kun, ggdat.gpreis );

    return 1;
}

int getBranData( void )
{
    nArtikel = 0;
	
    a_kun_gx.kun = 0;
    TimeMsgLine( "fetch...");   	
	if ( fetch_a_kun_gx_kb() == 0 )
	    return 0;
    Query_a_bas();
    
    paz_ipr.a = a_kun_gx.a;
    paz_ipr.bran_kun = atol( a_kun_gx.kun_bran2 );
    if ( Query_paz_ipr() == 0 )
    {
        paz_ipr.bran_kun = 0;
        if ( Query_paz_ipr() == 0 )
        {
            paz_ipr.ld_pr = 0;
        }
    }
    ggdat.gpreis = (long)(100.0 * paz_ipr.ld_pr);
    
    BauerngutLabelData();
    
    nArtikel++;

	fprintf( fperr, "%5ld. Artikel: A: %7ld BK: %7ld, P: %7ld wird gesendet ...\n",
	    	  nArtikel, ggdat.plunr, paz_ipr.bran_kun, ggdat.gpreis );
	    	  
    return 1;
}

int getAufData( void )
{
	nArtikel = 0;
	
	if ( fetch_pazlsp() == 0 )
	    return 0;

    Query_a_bas();
    
    ggdat.gpreis = (long)(100.0 * pazlsp.ls_lad_euro);
        
	a_kun_gx.a = pazlsp.a;
	
 	if ( pazlsp.kun == 0 )
 	{
 	    a_kun_gx.kun = 0;
 	    strcpy( a_kun_gx.kun_bran2, pazlsp.kun_bran2 );
	    fehlerfall = ! Query_a_kun_gx_ab();
	}
	else
 	{
 	    a_kun_gx.kun = pazlsp.kun;
 	    a_kun_gx.kun_bran2[0] = '\0';
	    fehlerfall = ! Query_a_kun_gx_ak();
	}
	ggdat.kdnr = 0;
	if ( fehlerfall != 0  )
	{
        /*  Fehlermeldung --> ATX2 */
        ggatx2.atxnr = 200000 + a_kun_gx.text_nr / 100 ;
        ggdat.atxnr2 = ggatx2.atxnr;
        sprintf( ggatx2.atx, "^14;^Ac;FEHLER\nArtikel %.0lf ohne Daten\n", a_kun_gx.a );   
        ggatx2.atxlen = (us) strlen(ggatx2.atx);
	}
	else
	{        
        BauerngutLabelData();
    }   	
    
    nArtikel++;

	fprintf( fperr, "%5ld. Artikel gesendet: A: %7ld: LS: %7ld, P: %7ld wird gesendet ...\n",
	    	  nArtikel, ggdat.plunr, pazlsp.ls, ggdat.gpreis );

    return 1;
}

void gx_to_ifmx_datum( long gxdat, long *ifmxdat )
{
    long tt, mm, jj;
    char ttmmjjjj[11];
    tt = gxdat / 10000;
    mm = (gxdat - tt * 10000) / 100;
    jj = gxdat - mm * 100 - tt * 10000;
    sprintf( ttmmjjjj, "%02ld.%02ld.20%02ld", tt, mm, jj );
	rstrdate( ttmmjjjj, ifmxdat );
}

void PazStatistikInsert( int etyp )
{
    if ( pazstatistik[0] == 'N' )
        return;

    paz_statistik.etyp      = etyp;
    paz_statistik.a         = (long) a_kun_gx.a;
    paz_statistik.ls        = pazlsp.ls;
    paz_statistik.gpreis    = ggdat.gpreis;
    paz_statistik.spreis    = ggdat.spreis;
    switch ( etyp )
    {
    case ETI_EINZEL:
        paz_statistik.netto     = ggdat.netto;
        paz_statistik.stk       = 1;
        break;
    case ETI_SUMME_1:
        paz_statistik.netto     = ggdat.netsum;
        paz_statistik.stk       = ggdat.stksum;
        break;
    case ETI_SUMME_2:
        paz_statistik.netto     = ggdat.netsum2;
        paz_statistik.stk       = ggdat.stksum2;
        break;
    case ETI_SUMME_3:
        paz_statistik.netto     = ggdat.netsum3;
        paz_statistik.stk       = ggdat.stksum3;
        break;
    default:
        paz_statistik.netto     = 0;
        paz_statistik.stk       = 0;
        break;
    }
    paz_statistik.tara      = ggdat.tara;
	gx_to_ifmx_datum( ggdat.datum1, &paz_statistik.datum1 );
	gx_to_ifmx_datum( ggdat.datum2, &paz_statistik.datum2 );
    paz_statistik.geraet    = ggdat.geraet;
    paz_statistik.numerator = ggdat.numerator;
    paz_statistik.sys       = system_num;
    paz_statistik.satz      = 0;
    dtcurrent( &paz_statistik.zeit_ins );
        
    EXEC SQL BEGIN WORK;
    EXEC SQL INSERT INTO PAZ_STATISTIK
             ( 
               etyp,
               a,
               ls,
               gpreis,
               spreis,
               stk,
               netto,
               tara,
               datum1,
               datum2,
               geraet,
               numerator,
               sys,
               satz,
               zeit_ins
             ) VALUES (
               :paz_statistik.etyp,
			   :paz_statistik.a,
			   :paz_statistik.ls,
			   :paz_statistik.gpreis,
			   :paz_statistik.spreis,
			   :paz_statistik.stk,
			   :paz_statistik.netto,
			   :paz_statistik.tara,
			   :paz_statistik.datum1,
			   :paz_statistik.datum2,
			   :paz_statistik.geraet,
			   :paz_statistik.numerator,
			   :paz_statistik.sys,
			   :paz_statistik.satz,
			   :paz_statistik.zeit_ins
             );
    fprintf( fperr, "PazStatistikInsert: SQLCODE = %ld\n", SQLCODE );
    EXEC SQL COMMIT WORK;
}
