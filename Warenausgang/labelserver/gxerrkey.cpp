#include "stdafx.h"

/*
 * gxerrkey.c
 *
 * (errkey.doc)
 */
 
/*
 * GX-Fehlerschl�ssel:
 * 
 * In der Anlage wird die Definition der in der Software definierten
 * Fehler beschrieben. Diese Fehlernummern und -kennungen dienen
 * ausschlie�lich TE-AS der internen Fehlersuche; sie m�ssen bei
 * der Fehlerr�ckmeldung soweit vorhanden vollst�ndig angegeben
 * werden. Da die Software st�ndigen Funktionserweiterungen unterliegt,
 * sind die in der Anzeige und im Logbuchausdruck erscheinenden
 * Fehlernummern ma�geblich; die Zuordnung der Kennung durch die
 * Tabelle dokumentieren nur den Entwicklungsstand der Software zum
 * Zeitpunkt der Ver�ffentlichung dieses Dokuments.
 * 
 * AUFBAU DER FEHLERCODES
 * 
 * Die Fehlercodes werden wie folgt gebildet:
 * 
 *  allgemeine BOS-Fehler:     0 - 15000
 *                         40000 - ...
 * 
 *  AS-Fehler:  15000
 *      Komponentenoffset:  n*100
 *      Gruppenoffset (alternativ): BOS 0
 *  AS 50
 *      interne Codierung jeweils  1...50
 *      Summe <Fehlercode>
 * 
 */

char *boskomponente( long offset )
{

/*
 * allgemeine BOS-Fehlercodes
 * 
 * KOMPONENTENOFFSET
 * 
 * Jede BOS-Softwarekomponente erh�lt einen Offset (in 100er-Schritten).
 * Bisher sind folgende Offsets definiert:
 * 
 */

switch ( offset )
{
case 0: return "Task-Manager 0";
case 100: return "System-Manager 100";
case 200: return "Ressourcen-Manager 200";
case 300: return "Logbuch-Manager 300";
case 400: return "Device-Driver-Manager 400";
case 1000: return "Bus-Manager 1000";
case 1100: return "Semaphoren-Manager 1100";
case 1200: return "Komponentenbus-Manager 1200";
case 1300: return "Systembus-Manager 1300";
case 1400: return "EDV-Manager 1400";
case 1600: return "LOCK-Manager 1600";
case 1900: return "Modem-Manager 1900";
case 2000: return "ADW-Manager 2000";
case 2100: return "Archiv-Manager 2100";
case 2300: return "Memory-Manager 2300";
case 2400: return "Timer-Manager 2400";
case 2500: return "TCP-Manager 2500";
case 2700: return "LSV1-Manager 2700";
case 2800: return "AEDV-Manager 2800";
case 2900: return "Hard-Disk-Manager 2900";
case 3100: return "Keyboard-Manager 3100";
case 3300: return "Grafik-Manager 3300";
case 3400: return "Printer-Manager 3400";
case 3500: return "Ton-Manager 3500";
case 3600: return "Port-I/O-Manager 3600";
case 3700: return "Uhren-Manager 3700";
case 3800: return "Men�-Manager 3800";
case 3900: return "Control-Manager 3900";
case 4000: return "Event-Manager 4000";
case 4100: return "Editor-Manager 4100";
case 4200: return "Layout-Manager 4200";
case 4270: return "Layout-Manager f�r Master/Slave-Anwendungen 4270";
case 4400: return "Datenbank-Manager 4400";
case 4500: return "Verteilte Datenbank-Manager 4 500";
case 4600: return "Telegramm-Manager 4600";
case 4700: return "Code-Manager 4700";
case 4800: return "Arithmetik-Manager 4800";
case 4900: return "String-Manager 4900";
case 5000: return "Utility-Manager 5000";
case 5300: return "Diagnose-Manager 5300";
case 5400: return "Datenbank-Hard-Disk-Manager 5400";
case 5500: return "Hard-Disk-Konfigurations-Manager 5500";
case 5700: return "Dialog-Manager 5700";
case 5900: return "Text-Manager 5900";
case 6100: return "Profibus-PC-Karte, DPR-Manager 6100";
case 6200: return "16-Bit-Editor-Manager 6200";
case 40000: return "ADW90-Manager 40000";
default: return "Unbekannte BOS-Komponente";
}
}

char *askomponente( long offset )
{

/*
 * AS-FEHLERCODES
 * 
 * AS-KOMPONENTENOFFSET
 * 
 * Jede AS-Softwarekomponente erh�lt einen Offset (in 100er-Schritten).
 * Bisher sind folgende Offsets definiert:
 * 
 */

offset -= 15000;

switch ( offset )
{
case 100: return "Ablaufsteuerung 100";
case 200: return "Automatensteuerung 200";
case 300: return "Anzeige 300";
case 400: return "Codeleseeinheit 400";
case 500: return "Schnittstelle 500";
case 600: return "Drucker 600";
case 700: return "Etikettenaufbereitung 700";
case 800: return "Gewichtsbearbeitung 800";
case 900: return "Initialisierung 900";
case 1000: return "Eingabe 1000";
case 1100: return "PLU-Bearbeitung 1100";
case 1200: return "Summierung 1200";
case 1300: return "Tastatureingabe 1300";
case 1400: return "Verpackungssteuerung 1400";
case 2000: return "Code-Editor 2000";
case 2100: return "Memo-Card 2100";
case 2200: return "Verbindungsschicht 2200";
case 2300: return "Ger�tedatenverwalter 2300";
case 2400: return "Fernsteuerinterpreter 2400";
case 2500: return "Auftragsbearbeitung 2500";
case 2600: return "Datenbank 2600";
case 2700: return "Eingabewerkzeuge 2700";
case 2800: return "Anbindung PXT -> GX 2800";
case 2900: return "Listendrucker 2900";
case 3000: return "Service & Diagnosewerkzeug 3000";
case 3100: return "Automaten-CPU 3100";
default: return "Unbekannte AS-Komponente";
}
}

char *asfehler( long key )
{

/*
 * GRUPPENOFFSET
 * 0: Allgemeine Manager- und Komponentenfehler.
 * 50: Komponentenspezifische Fehler.
 *
 * INTERNE CODIERUNG DER ALLGEMEINEN MANAGER- UND KOMPONENTENFEHLER
 *
 * Beispiel: 15801 = AS-Komponentenoffset + Gewichtsbearbeitung + Overflow
 *           15801 =              15000   +   800               +   1
 */

switch ( key )
{
case 1: return "OVF_E * Overflow";
case 2: return "UVF_E * Underflow";
case 3: return "DAT_E * invalid data";
case 4: return "INIT_E * Initialisierungs-Fehler";
case 5: return "FKT_E * Funktion nicht verf�gbar";
case 6: return "MGR_E * fataler Manager-Fehler";
case 7: return "ID_E * Manager ID-Fehler";
case 8: return "SHDL_E * ung�ltige Semaphor-Handle";
case 9: return "EHDL_E * ung�ltige Event-Handle";
case 10: return "HDL_E * allgemeiner Handle-Fehler";
case 11: return "QID_E * ung�ltige Queue-ID";
case 12: return "RID_E * ung�ltige Resource-ID";
case 13: return "TID_E * ung�ltige Task-ID";
case 14: return "PID_E * ung�ltige Partitions-ID";
case 15: return "MEM_E * Fehler Memory-Manager";
case 16: return "KBUS_E * Komponenten-Bus-Fehler";
case 17: return "SBUS_E * System-Bus-Fehler";
case 18: return "MSG_E * ung�ltige Message";
case 19: return "PARA_E * ung�ltige Parameter";
case 20: return "DRIV_E * Treiber-Fehler";
case 21: return "TAS_E * Task-Fehler";
case 22: return "TOUT_E * Timeout-Fehler";
case 47: return "BUSY_E * I/O in Arbeit";
case 48: return "KILL_E * I/O abgebrochen";
case 49: return "ERR_E * Allgemeiner Fehler/Manager";
default: return "Unbekannter AS-Fehler";
}
}

char *komponente( long key )
{
    long offset;
    offset =  key / 100;
    offset *= 100;

    if ( offset >= 15000 && offset < 40000 )
    {
        return askomponente( offset );
    }
    return boskomponente( offset );
}

char *gxerrkey( long key )
{
    
switch ( key )
{

case 0: return "kein Fehler";

/*
 * TASK-MANAGER
 */

case 1: return "0x01 TAS_TID_E task ID error";
case 2: return "0x02 TAS_TCB_E no TCB's available";
case 3: return "0x03 TAS_MEM_E no memory available";
case 4: return "0x04 TAS_NMB_E not a memory block";
case 5: return "0x05 TAS_MIU_E mailbox in use";
case 6: return "0x06 TAS_ZMW_E zero message";
case 7: return "0x07 TAS_BUF_E buffer full";
case 8: return "0x08 TAS_WTC_E WAITC in progress";
case 9: return "0x09 TAS_ISC_E invalid system call";
case 10: return "0x0a TAS_TMO_E timeout";
case 11: return "0x0b TAS_NMP_E no message present";
case 12: return "0x0c TAS_QID_E queue ID error";
case 13: return "0x0d TAS_QFL_E queue full";
case 14: return "0x0e TAS_PID_E partition ID error";
case 15: return "0x0f TAS_INI_E fatal initialization error ";
case 16: return "0x10 TAS_NCP_E no character present";
case 17: return "0x11 TAS_ICP_E invalid configuration table parameter";
case 18: return "0x12 TAS_IIP_E invalid input parameter";
case 32: return "0x20 TAS_CVT_E component vector table not present";
case 33: return "0x21 TAS_COM_E undefined component";
case 34: return "0x22 TAS_OPC_E undefined opcode for component";
case 48: return "0x30 TAS_NOCB_E no control blocks avail.";
case 49: return "0x31 TAS_SID_E ev flag group or semaphore ID error";
case 50: return "0x32 TAS_PND_E Tasks pending on ev flag group/semaphore";
case 51: return "0x33 TAS_DEL_E ev flag group or semaphore is deleted";
case 52: return "0x34 TAS_OVF_E ev flag already set or semaphore overflow";

/*
 * SYSTEM-MANAGER
 */

case 150: return "0x96 SYS_ERR Allg. System-Fehler";
case 151: return "0x97 SYS_NIL_E NIL Adresse";
case 152: return "0x98 SYS_FKT_E Ungueltige Funktions-Nummer";
case 153: return "0x99 SYS_MODUL_E Ungueltige Modul-Nummer";
case 154: return "0x9a SYS_FOUND_E Konfiguration nicht gefunden";
case 155: return "0x9b SYS_USERHB_E Fehler beim USER_HB anlegen";
case 156: return "0x9c SYS_USERHU_E Fehler beim USER_HU anlegen";
case 157: return "0x9d SYS_SYSHB_E Fehler beim SYS_HB anlegen";
case 158: return "0x9e SYS_SYSHU_E Fehler beim SYS_HU anlegen";
case 159: return "0x9f SYS_TESTADDR_E BusError beim Test";

/*
 * RESSOURCEN-MANAGER
 */

case 250: return "0xfa RSC_ERR Allg. Rsc.Fehler";
case 251: return "0xfb RSC_FOUND_E Resource nicht existent";
case 252: return "0xfc RSC_METHOD_E Falsche Methode, Vorsicht!";
case 253: return "0xfd RSC_PATH_E unguelt. Res. Pfad Name";
case 254: return "0xfe RSC_ID_E";
case 255: return "0xff RSC_FAIL_E Resource fehlerhaft";
case 256: return "0x100 RSC_MAP_E Resource-Map voll";

/*
 * LOGBUCH-MANAGER
 */

case 304: return "0x130 LOG_INIT_E Initialisierungsfehl. ";
case 351: return "0x15f LOG_CRQUEUE_E Queue nicht angelegt";
case 352: return "0x160 LOG_TASK_E Task nicht gestartet";
case 353: return "0x161 LOG_WRQUEUE_E Schreibfehler in Queue ";
case 354: return "0x162 LOG_RQUEUE_E Lesefehler aus Queue ";
case 355: return "0x163 LOG_NOTFOUND_E Logbuch nicht gefunden ";
case 356: return "0x164 LOG_FULL_E Logbuch ist voll";
case 357: return "0x165 LOG_UPDATE_E Logbuch Updatefehler";
case 358: return "0x166 LOG_NOTDO_E Logbuch Updatefehler";
case 359: return "0x167 LOG_PTRLIST_E Fehler in PtrListe des Logbuchs";
case 360: return "0x168 LOG_AF_E Akt-Fkt nicht vorh.";

/*
 * DEVICE-DRIVER-MANAGER
 */

case 401: return "0x191 DD_OVF_E Overflow-Fehler";
case 403: return "0x193 DD_DAT_E Fehler in Parameter";
case 404: return "0x194 DD_INIT_E Initialisiserungs-Fehler";
case 405: return "0x195 DD_FKT_E Device Driver Funktion error";
case 407: return "0x197 DD_ID_E Device Driver ID error";
case 450: return "0x1c2 DD_REPEAT_E Fehler bei Wiederholung des Druckauftrags";
case 451: return "0x1c3 DD_TIMEOUT_E Timeout-Fehler";
case 452: return "0x1c4 DD_LRC_E LRC Fehler";
case 453: return "0x1c5 DD_WARN_E Warnung zuviele LRC Fehler";
case 454: return "0x1c6 DD_LOOP_FULL_E IdleLoop-Tabelle voll";
case 455: return "0x1c7 DD_LOOP_PTR_E Zeiger in der IdleLoop-";
case 456: return "0x1c8 DD_FIP_GRID_E Gitterfehler(1. oder 2.Anz.)";
case 457: return "0x1c9 DD_FIP2_GRID_E Gitterfehler(2.Anz. fehlt)";
case 458: return "0x1ca DD_FIP_ANOD_E Anodenfehler";
case 459: return "0x1cb DD_SCSI_TMO_E Timeout";
case 460: return "0x1cc DD_SCSI_NOTRDY_E Drive not Ready";
case 461: return "0x1cd DD_SCSI_CRC_E CRC Error";
case 462: return "0x1ce DD_SCSI_SEEK_E Seek Error";
case 463: return "0x1cf DD_SCSI_SECTOR_E Sector not found";
case 464: return "0x1d0 DD_SCSI_WRITE_E Write Error";
case 465: return "0x1d1 DD_SCSI_GEN_E Generic Error";
case 466: return "0x1d2 DD_SCSI_HW_E Hardware Error";
case 467: return "0x1d3 DD_SCSI_CHKC_E Check Condition";
case 480: return "0x1e0 DD_ARC_ERR Basis-Nr. fuer Memo und Flash";
case 481: return "0x1e1 DD_MEMO_RMV_E Karte wurde abgezogen";
case 482: return "0x1e2 DD_MEMO_WP_E Karte schreibgesch�tzt";
case 483: return "0x1e3 DD_MEMO_VCC_E Vcc ist nicht angelegt";
case 484: return "0x1e4 DD_MEMO_VPP_E Vpp ist nicht angelegt";
case 485: return "0x1e5 DD_MEMO_DAT_E Datenfehler beim Schreiben";
case 486: return "0x1e6 DD_MEMO_ADD_E Ungerade Adressierungsart";
case 487: return "0x1e7 DD_MEMO_OVR_E Karte ist voll, Ueberlauf";
case 488: return "0x1e8 DD_MEMO_LBT_E Kartenbatterie wird leer";
case 489: return "0x1e9 DD_MEMO_EBT_E Kartenbatterie ist leer";
case 490: return "0x1ea DD_MEMO_KILL Zugriff wurde abgebrochen";

/*
 * BUS-MANAGER
 */

case 1004: return "0x3ec SER_INIT_E";
case 1005: return "0x3ed SER_FKT_E";
case 1015: return "0x3f7 SER_MEM_E";
case 1051: return "0x41b SER_PARNOTOK_E";
case 1052: return "0x41c SER_HDL_E";
case 1053: return "0x41d SER_NOHDL_E";
case 1054: return "0x41e SER_UNCONSIST_E";
case 1055: return "0x41f SER_RCVQ_E";
case 1056: return "0x420 SER_OFFL_E Konstanten fuer Status-Kodes";

/*
 * SEMAPHOREN-MANAGER
 */

case 1115: return "0x45b GLB_MEM_E Kein Speicher mehr frei";
case 1151: return "0x47f GLB_SEMOWNER_E gew�nschtes Zugriffsrecht f�r Sperre bereits belegt";
case 1152: return "0x480 GLB_SEMREQ_E es wird bereits ein Zugriffsrecht f�r diese Sperre �ber den Bus angefordert";
case 1154: return "0x482 GLB_SEMUSED_E angefordertes Zugriffsrecht von Station, die exklusives Zugriffsrecht h�lt, zur�ckgewiesen";
case 1155: return "0x483 GLB_PROERROR_E Fehler im Sperrprotokoll: ausbleibende oder falsche Antworttelegrame";
case 1156: return "0x484 GLB_TOKERROR_E Fehler im Sperrprotokoll: Anforderung wegen unerlaubtem Token abgebrochen";
case 1157: return "0x485 GLB_BUSERROR_E Busfehler";
case 1158: return "0x486 GLB_OFFERROR_E Bus nicht f�r GLB-Management bereit";
case 1159: return "0x487 GLB_CACHEFULL_E Cache�berlauf";
case 1160: return "0x488 GLBIE_NOT freizugebende Sperre nicht belegt";
case 1161: return "0x489 GLBIE_START Anforderung gestartet";
case 1162: return "0x48a GLB_PARNOTOK_E Parameterfehler";

/*
 * KOMPONENTENBUS-MANAGER
 */

case 1201: return "0x4b1 KBUS_OVF_E Overflow";
case 1204: return "0x4b4 KBUS_INIT_E Kbus ist nicht installiert";
case 1205: return "0x4b5 KBUS_FKT_E Funktion nicht verfuegbar";
case 1215: return "0x4bf KBUS_MEM_E Kein Speicher mehr vorhanden";
case 1219: return "0x4c3 KBUS_PARA_E Kbus Parameter nicht ok.";
case 1251: return "0x4e3 KBUS_PARNOTOK_E";
case 1252: return "0x4e4 KBUS_HDL_E";
case 1253: return "0x4e5 KBUS_NOHDL_E";
case 1254: return "0x4e6 KBUS_UNCONSIST_E";
case 1255: return "0x4e7 KBUS_RCVQ_E";
case 1270: return "0x4f6 KBUS_SAP_E LSAPs sind nicht angelegt";
case 1271: return "0x4f7 KBUS_GAP_E Gapbereich hat sich veraendert";
case 1272: return "0x4f8 KBUS_POLLDEF_E Poll-Liste konnte nicht definiert werden ";
case 1275: return "0x4fb Bus erfolgreich initialisiert. ";
case 1276: return "0x4fc neue Station am Bus";
case 1277: return "0x4fd Bus vom Anwender gestoppt";
case 1278: return "0x4fe Bus im Zustand Listen Token wegen Stoeruung beim Master, beim Slave Bus Offline, da Timeout-Timer abgelaufen";
case 1279: return "0x4ff Bus in Zustand Offline wegen schwerem Fehler";
case 1280: return "0x500 Bus kann nicht in Ring eintreten, da Sperren belegt";
case 1281: return "0x501 Hardware Receive Error";
case 1282: return "0x502 Station nicht gepollt wegen Warteschlangen Ueberlauf";
case 1283: return "0x503 Verlust von Meldungen wegen Mangel an Empfangsbloecken";
case 1284: return "0x504 Buffermangel";
case 1285: return "0x505 interner Fehler (bis Version 5.3)";
case 1286: return "0x506 SYN-Intervalltimer hat angesprochen";
case 1287: return "0x507 Bufferverwaltung korrupt";
case 1290: return "0x50a T2START-Fehler";
case 1291: return "0x50b T2CANCEL-Fehler";
case 1292: return "0x50c PbSendeAufrufTel-Fehler, kein freier TxBD";
case 1293: return "0x50d kein freier TxBD sonst";
case 1294: return "0x50e kein Platz in QPost-Tabelle";
case 1295: return "0x50f QPost-Fehler";
case 1296: return "0x510 Post-Fehler";
case 1297: return "0x511 kein Platz mehr fuer neue Kennung von fremder Station";

/*
 * SYSTEMBUS-MANAGER
 */

case 1304: return "0x518 SBUS_INIT_E";
case 1305: return "0x519 SBUS_FKT_E";
case 1315: return "0x523 SBUS_MEM_E";
case 1351: return "0x547 SBUS_PARNOTOK_E";
case 1352: return "0x548 SBUS_HDL_E";
case 1353: return "0x549 SBUS_NOHDL_E";
case 1354: return "0x54a SBUS_UNCONSIST_E";
case 1355: return "0x54b SBUS_RCVQ_E";
case 1360: return "0x550 SBUS_ADR_E ungueltige Adresse 1<=x<=31";
case 1361: return "0x551 SBUS_GAP_E GAP-Bereich veraendert";
case 1362: return "0x552 SBUS_LSAP_E LSAP ist nicht angelegt";
case 1363: return "0x553 SBUS_TEL_E Empfangsfehler";
case 1364: return "0x554 SBUS_BROADCAST_E BroadCast nicht druchgefuehrt";
case 1365: return "0x555 SBUS_EFGTIMEOUT_E Event nicht eingetroffen nach t ";
case 1366: return "0x556 SBUS_GAPCHK_E Fehler beim Stnlisten-Check";
case 1367: return "0x557 SBUS_EFG_E Bitmaske stimmt nicht ueberein";
case 1368: return "0x558 SBUS_DDADR_E Doppelte Adresse im System";
case 1369: return "0x559 SBUS_OLDVER_E Checksummencheck arbeitet in diesem System nicht, da alte Prog.Version im System";
case 1370: return "0x55a SBUS_MASTER_E Checksumme stimmt nicht mit Vorgabechecksumme ueberein";
case 1371: return "0x55b SBUS_TIMEOUT_E Fkt mit Timeout abgebrochen";
case 1372: return "0x55c SBUS_CHECK_E Checksummencheck kann nicht durchgefuehrt werden.";
case 1373: return "0x55d SBUS_DIFF_E Checksumme stimmt mit Master- checksumme nicht ueberein";
case 1374: return "0x55e SBUS_BITSET_E Userbits in Stationsliste kann nicht gesetzt/geloescht werden";
case 1375: return "0x55f SBUS_ONOFF_E Protokolliert Einschalten (Power ON)";
case 1376: return "0x560 SBUS_AF_E Aktionsfunktion nicht install. ";
case 1377: return "0x561 Bus vom Anwender gestoppt";
case 1378: return "0x562 Bus im Zustand Listen Token wegen Stoerung beim Master, beim Slave Bus Offline, da Timeout-Timer abgelaufen";
case 1379: return "0x563 Bus in Zustand Offline wegen schwerem Fehler";
case 1380: return "0x564 Bus kann nicht in Ring eintreten, da Sperren belegt";
case 1381: return "0x565 Hardware Receive Error";
case 1382: return "0x566 Station nicht gepollt wegen Warteschlangen-Ueberlauf";
case 1383: return "0x567 Verlust von Meldungen wegen Mangel an Empfangsbloecken";
case 1384: return "0x568 Buffermangel";
case 1385: return "0x569 interner Fehler (bis Version 5.3)";
case 1386: return "0x56a SYN-Intervalltimer hat angesprochen";
case 1387: return "0x56b Bufferverwaltung korrupt";
case 1390: return "0x56e T2START-Fehler";
case 1391: return "0x56f T2CANCEL-Fehler";
case 1392: return "0x570 PbSendeAufrufTel-Fehler, kein freier TxBD";
case 1393: return "0x571 kein freier TxBD sonst";
case 1394: return "0x572 kein Platz in QPost-Tabelle";
case 1395: return "0x573 QPost-Fehler";
case 1396: return "0x574 Post-Fehler";
case 1397: return "0x575 ein Platz mehr fuer neue Kennung von fremder Station";

/*
 * EDV-MANAGER
 */

case 1404: return "0x57c EDV_INIT_E Schnittstelle schon initialisiert bei EdvInit";
case 1405: return "0x57d EDV_FKT_E Funktion nicht implementiert";
case 1415: return "0x587 EDV_MEM_E Verbindungsaufbau nicht moeglich, da nicht empfangsbereit oder Platzmangel";
case 1451: return "0x5ab EDV_PARNOTOK_E falsche Parameter";
case 1452: return "0x5ac EDV_HDL_E falsche Handle";
case 1453: return "0x5ad EDV_NOHDL_E keine Handle frei oder bei EdvPut: bereits hochpriorer Auftrag aktiv";
case 1454: return "0x5ae EDV_ACTIVE_E Verbindung oder hochpriorer Auftrag aktiv";
case 1455: return "0x5af EDV_WAIT_E Wartezustand";
case 1456: return "0x5b0 EDV_RNR_E Wartezustand und";
case 1457: return "0x5b1 EDV_RNRAB_E Put nicht ausgefuehrt, Gegenstelle nicht empfangsbereit";
case 1458: return "0x5b2 EDV_LR_E Auftrag durch EdvDisc abgebrochen";
case 1459: return "0x5b3 EDV_NC_E Verbindung kann nicht aufgebaut werden";
case 1460: return "0x5b4 EDV_CTS_E CTS weggefallen";
case 1461: return "0x5b5 EDV_AB_E Sendevorgang nach wiederholtem Versuch abgebrochen";
case 1462: return "0x5b6 EDV_NOCONN_E keine Verbindung aktiv";
case 1463: return "0x5b7 EDV_XID_E XID ist schiefgegangen";
case 1464: return "0x5b8 EDV_TEST_E TEST ist schiefgegangen";
case 1465: return "0x5b9 EDV_RCVQ_E keine VRTX-Queue fuer Empfangs-WS";
case 1466: return "0x5ba EDV_OVFL_E hochpriores Telegramm verlorengegangen wegen Buffermangel";
case 1467: return "0x5bb EDV_NOUART_E Uart steht nicht zur Verfuegung";
case 1468: return "0x5bc EDV_TMR_E Fehler im Timer-Mangaer";
case 1469: return "0x5bd EDV_NOCARRIER_E Fehler im Timer-Mangaer";
case 1470: return "0x5be EDV_KILLHDL_E Senden mit EdvKillHdl abgebrochen";
case 1471: return "0x5bf EDV_KILL_E Senden mit EdvKillHdl nicht abgebrochen";

/*
 * LOCK-MANAGER
 */

case 1651: return "0x673 GLB_LCKOWNER_E gew�nschtes Zugriffsrecht f�r Sperre bereits belegt";
case 1652: return "0x674 GLB_LCKREQ_E es wird bereits ein Zugriffsrecht f�r diese Sperre �ber den Bus angefordert";
case 1653: return "0x675 GLB_LCKSHARED_E angefordertes Zugriffsrecht von Station, die teilbares Zugriffsrecht h�lt, zur�ckgewiesen";
case 1654: return "0x676 GLB_LCKEXCLUSIV_E angefordertes Zugriffsrecht von Station, die exklusives Zugriffsrecht h�lt, zur�ckgewiesen";

/*
 * MODEM-MANAGER
 */

case 1904: return "0x770 MDM_INIT_E bei Init: bereits initialisiert. sonst: nocht nicht initialisiert";
case 1915: return "0x77b MDM_MEM_E nicht gen�gend Speicherplatz";
case 1951: return "0x79f MDM_PARNOTOK_E falsche Parameter";
case 1952: return "0x7a0 MDM_FKT_E nicht implementierte Funktion";
case 1954: return "0x7a2 MDM_NOUART_E UART nicht belegt";
case 1955: return "0x7a3 MDM_EXIT_E f�r Loggen von Exit";
case 1957: return "0x7a5 MDM_TX_E Task-Konflikt";
case 1958: return "0x7a6 MDM_BUG_E interner Fehler z.B. VRTX";
case 1959: return "0x7a7 MDM_ABORT_E Abbruch einer Transaktion";
case 1960: return "0x7a8 MDM_NOTOK_E kein OK nach Test, kein Connect nach Call, Accept, kein anliegender Ruf";
case 1961: return "0x7a9 MDM_NOANSWER_E Modem antwortet nicht";
case 1962: return "0x7aa MDM_NODSR_E DSR liegt nach Call/Accept nicht an";
case 1963: return "0x7ab MDM_HWERR_E Hardware-Fehler bei Empfang einer Meldung";
case 1964: return "0x7ac MDM_TMR_E Timer-Manager Fehler";

/*
 * ADW-MANAGER
 */

case 2004: return "0x7d4 ADW_INIT_E Inititalisierungsfehler ";
case 2005: return "0x7d5 ADW_FKT_E Fkt. nicht verfuegbar";
case 2006: return "0x7d6 ADW_MGR_E Manager arbeitet nicht";
case 2007: return "0x7d7 ADW_ID_E falsche ID angegeben";
case 2015: return "0x7df ADW_MEM_E Memory-Fehler";
/*
 * case 2015: return "0x7df ADW_UVF_E Underflow";
 */
case 2016: return "0x7e0 ADW_KBUS_E Komponenten-Bus-Fehler";
case 2019: return "0x7e3 ADW_PARA_E falsche Parameter";
case 2051: return "0x803 ADW_KAL_E Kal. nicht korrekt";
case 2052: return "0x804 ADW_Q_E Queue Fehler";
case 2053: return "0x805 ADW_OFFL_E ADW kann nicht mehr angesprochen werden";
case 2054: return "0x806 ADW_MODE_E Funktion darf in diesem Modus nicht benutzt werden";
case 2055: return "0x807 ADW_KNN_E unzulaessige Kennung";
case 2056: return "0x808 ADW_CHK_E Checksummenfehler";
case 2057: return "0x809 ADW_TOUT_E Timeoutfehler";

/*
 * ARCHIV-MANAGER
 */

case 2104: return "0x838 ARCHIV_INIT_E nicht Initialisiert";
case 2105: return "0x839 ARCHIV_FKT_E Fkt. nicht verfuegbar ";
case 2107: return "0x83b ARCHIV_ID_E falsche ID";
case 2119: return "0x847 ARCHIV_PARA_E falsche Parameter";
case 2151: return "0x867 ARCHIV_CHK_E Checksumme falsch";
case 2152: return "0x868 ARCHIV_CMD_E Kommando falsch";
case 2153: return "0x869 ARCHIV_WR_E kein Schreibrecht";
case 2154: return "0x86a ARCHIV_TRA_E Uebertragung nicht";
case 2155: return "0x86b ARCHIV_MOD_E falscher Adw90-Mode";

/*
 * MEMORY-MANAGER
 */

case 2304: return "0x900 MEM_INIT_E Heap zu klein zum Initialisieren";
case 2306: return "0x902 MEM_MGR_E Fehler in der Heapverwaltung";
case 2351: return "0x92f MEM_OUT_E Kein Speicher mehr frei";
case 2352: return "0x930 MEM_REQ_E Anforderungsfehler beim Aufrufer";
case 2353: return "0x931 MEM_RANGE_E Zeiger im unzulaessigen Bereich";
case 2354: return "0x932 MEM_HEAPID_E Falsche HeapID angegeben";
case 2355: return "0x933 MEM_FRAG_E Heap stark fragmentiert";
case 2357: return "0x935 MEM_CREATE_E Heap zu klein zum Anlegen";
case 2382: return "0x94e MEM_LESS_E Weniger uebertragen als angegeben";
case 2383: return "0x94f MEM_STAT_E Heapverwaltung ist gestoert";

/*
 * TIMER-MANAGER
 */

case 2401: return "0x961 TMR_OVF_E max. Timeranzahl erreicht ";
case 2404: return "0x964 TMR_INIT_E Timertask nicht install.";
case 2405: return "0x965 TMR_FKT_E keine AktFktn vorhanden";
case 2410: return "0x96a TMR_HDL_E Timer-Hdl existiert nicht ";
case 2451: return "0x993 TMR_PARA_E Parameter nicht ok.";

/*
 * TCP-MANAGER
 */

case 2504: return "0x9c8 TCP_INIT_E Fehler in der Initialisierung";
case 2519: return "0x9d7 TCP_PARNATOK_E Parameterfehler";
case 2551: return "0x9f7 TCP_PORT_MAX_E Zuviele Ports";
case 2552: return "0x9f8 TCP_WAIT_S_E Warte auf Daten";
case 2561: return "0xa01 TCP_PERM_E Not owner";
case 2563: return "0xa03 TCP_SRCH_E No such process";
case 2566: return "0xa06 TCP_NXIO_E No such device or address";
case 2573: return "0xa0d TCP_ACCES_E Permission denied";
case 2577: return "0xa11 TCP_EXIST_E File exists";
case 2582: return "0xa16 TCP_INVAL_E Invalid argument";
case 2584: return "0xa18 TCP_MFILE_E Too many open files";
case 2592: return "0xa20 TCP_PIPE_E Broken pipe";
case 2595: return "0xa23 TCP_WOULDBLOCK_E Operation would block";
case 2596: return "0xa24 TCP_INPROGRESS_E Operation now in progress";
case 2597: return "0xa25 TCP_ALREADY_E Operation already in progress";
case 2598: return "0xa26 TCP_NOTSOCK_E Socket operation on non-socket";
case 2599: return "0xa27 TCP_DESTADDRREQ_E Destination address required";
case 2600: return "0xa28 TCP_MSGSIZE_E Message too long";
case 2601: return "0xa29 TCP_PROTOTYPE_E Protocol wrong type for socket";
case 2602: return "0xa2a TCP_NOPROTOOPT_E Protocol not available";
case 2603: return "0xa2b TCP_PROTONOSUPPORT Protocol not supported";
case 2605: return "0xa2d TCP_OPNOTSUPP_E Operation not supported on socket";
case 2607: return "0xa2f TCP_AFNOSUPPORT_E Address family not supported";
case 2608: return "0xa30 TCP_ADDRINUSE_E Address already in use";
case 2609: return "0xa31 TCP_ADDRNOTAVAIL_E Can't assign requested address";
case 2610: return "0xa32 TCP_NETDOWN_E Network is down";
case 2611: return "0xa33 TCP_NETUNREACH_E Network is unreachable";
case 2613: return "0xa35 TCP_CONNABORTED_E Software caused connection abort";
case 2614: return "0xa36 TCP_CONNRESET_E Connection reset by peer";
case 2615: return "0xa37 TCP_NOBUFS_E No buffer space available";
case 2616: return "0xa38 TCP_ISCONN_E Socket is already connected";
case 2617: return "0xa39 TCP_NOTCONN_E Socket is not connected";
case 2620: return "0xa3c TCP_TIMEDOUT_E Connection timed out";
case 2621: return "0xa3d TCP_CONNREFUSED_E Connection refused";
case 2624: return "0xa40 TCP_HOSTDOWN_E Host is down";
case 2625: return "0xa41 TCP_HOSTUNREACH_E No route to host";
case 2640: return "0xa50 TCP_SCALL_E illegal scall parameter in config";
case 2641: return "0xa51 TCP_LMEM_E no enough local memory";
case 2642: return "0xa52 TCP_ARP_E ARP table can not hold all";
case 2643: return "0xa53 TCP_HOSTUNKNOWN_E unknown host";
case 2644: return "0xa54 TCP_BADINET_E bad internet address";
case 2645: return "0xa55 TCP_BADNET_E bad net part in internet address";
case 2646: return "0xa56 TCP_GMEM_E no enough global memory";
case 2647: return "0xa57 TCP_BADBLOCK_E bad balock paramaeter in config";
case 2648: return "0xa58 TCP_GLOCK_E fail to lock global lock";
case 2649: return "0xa59 TCP_CDT_E Missing CDT in configuration table";
case 2650: return "0xa5a TCP_PIN_E panic; in - in_control";
case 2651: return "0xa5b TCP_PICMPE_E panic; icmp - icmp_error";
case 2652: return "0xa5c TCP_PICMPLEN_E panic; icmp - length";
case 2653: return "0xa5d TCP_PIPINIT_E panic; ip - init";
case 2654: return "0xa5e TCP_PRAWREQ_E panic; raw - user req";
case 2655: return "0xa5f TCP_PRTFREE_E panic; route- free";
case 2656: return "0xa60 TCP_PTCPPULL_E panic; tcp - pull oob";
case 2657: return "0xa61 TCP_PTCPOUT_E panic; tcp - output";
case 2658: return "0xa62 TCP_PTCPOUTX_E panic; tcp - output REXMT";
case 2659: return "0xa63 TCP_PTCPUSR_E panic; tcp - usrreq";
case 2660: return "0xa64 TCP_PUDPUSR_E panic; udp - usrreq";
case 2661: return "0xa65 TCP_PMGET_E panic; m_get";
case 2662: return "0xa66 TCP_PMFREE_E panic; m_free";
case 2663: return "0xa67 TCP_PMCOPY1_E panic; m_copy";
case 2664: return "0xa68 TCP_PMCOPY2_E panic; m_copy";
case 2665: return "0xa69 TCP_PMCOPY3_E panic; m_copy";
case 2666: return "0xa6a TCP_PSOFREE_E panic; sofree";
case 2667: return "0xa6b TCP_PSOCLOSE_E panic; soclose";
case 2668: return "0xa6c TCP_PSOACCEPT_E panic; soaccept";
case 2669: return "0xa6d TCP_PSORECV1_E panic; sorecv";
case 2670: return "0xa6e TCP_PSORECV2_E panic; sorecv";
case 2671: return "0xa6f TCP_PSORECV3_E panic; sorecv";
case 2672: return "0xa70 TCP_PSORECV4_E panic; sorecv";
case 2673: return "0xa71 TCP_PSOISCON_E panic; soisconnected";
case 2674: return "0xa72 TCP_PSBAPP_E panic; sbappendrights";
case 2675: return "0xa73 TCP_PSBFLUSH1_E panic; sbflush";
case 2676: return "0xa74 TCP_PSBFLUSH2_E panic; sbflush";
case 2677: return "0xa75 TCP_PSBDROP_E panic; sbdrop";
case 2678: return "0xa76 TCP_PACCEPT_E panic; accept";
case 2679: return "0xa77 TCP_PARP_E panic; arp - no free entry";
case 2680: return "0xa78 TCP_PSYNCH1_E panic; synch mechanism";
case 2681: return "0xa79 TCP_PSYNCH2_E panic; synch mechanism";
case 2682: return "0xa7a TCP_PSYNCH3_E panic; synch mechanism";
case 2683: return "0xa7b TCP_PSYNCH4_E panic; synch mechanism";
case 2684: return "0xa7c TCP_PSYNCH5_E panic; synch mechanism";
case 2685: return "0xa7d TCP_PSYNCH6_E panic; synch mechanism";
case 2686: return "0xa7e TCP_PSYNCH7_E panic; synch mechanism";
case 2687: return "0xa7f TCP_PSYNCH8_E panic; synch mechanism";
case 2688: return "0xa80 TCP_PGMSEND_E panic; gm_send";

/*
 * LSV1-MANAGER
 */

case 2704: return "0xa90 LSV1_INIT_E Fehler beim Initialisieren von Lsv1";
case 2715: return "0xa9b LSV1_MEM_E Speichermangel";
case 2719: return "0xa9f LSV1_PARA_E unzulaessige Parameter";
case 2756: return "0xac4 LSV1_OFFLINE_E keine Verbindung mit externem Master";
case 2757: return "0xac5 LSV1_NOTREADY_E Protokollschicht ist nicht sendebereit";
case 2758: return "0xac6 LSV1_SENDERROR_E Protokofehler beim Senden";
case 2759: return "0xac7 LSV1_SENDACTIVE_E es ist bereits ein Sendeauftrag aktiv";
case 2760: return "0xac8 LSV1_SENDNOTACTIVE es ist kein Sendeauftrag aktiv";

/*
 * AEDV-MANAGER
 */

case 2804: return "0xaf4 AEDV_INIT_E Fehler beim Initialisieren von AEdv";
case 2815: return "0xaff AEDV_MEM_E Speichermangel";
case 2819: return "0xb03 AEDV_PARA_E unzulaessige Parameter ";
case 2851: return "0xb23 AEDV_PUT_E Senden ist aktiv";
case 2852: return "0xb24 AEDV_HDL_E falsches Handle";
case 2853: return "0xb25 AEDV_NOHDL_E kein Handle mehr frei";
case 2854: return "0xb26 AEDV_WAIT_E Wartezustand";
case 2855: return "0xb27 AEDV_TIMEOUT_E Timeoutfehler";
case 2856: return "0xb28 AEDV_AUTOMATEN_E Fehler im Automat";
case 2857: return "0xb29 AEDV_TIMEOUTA_E Timeout A abgelaufen";
case 2858: return "0xb2a AEDV_TIMEOUTC_E Timeout C abgelaufen";
case 2859: return "0xb2b AEDV_SEND_E Sendefehler";
case 2860: return "0xb2c AEDV_NODATA_E keine Datenvorhanden";
case 2861: return "0xb2d AEDV_CTRLON_E Ctrlfunktion wurde aufgerufen";
case 2862: return "0xb2e AEDV_EXIT_E Exitfunktion wurde aufgerufen";
case 2863: return "0xb2f AEDV_OVERFLOW_E Queueoverflow";
case 2864: return "0xb30 AEDV_KILL_E Telegramm konnte nicht aus Sendequeue gel�scht werden";

/*
 * HARD-DISK-MANAGER
 */

case 2903: return "0xb57 HD_DAT_E Parameter error";
case 2950: return "0xb86 HD_GEN_E generic error";
case 2951: return "0xb87 HD_NODEV_E no such device";
case 2952: return "0xb88 HD_READ_E read error";
case 2953: return "0xb89 HD_WRITE_E write error";
case 2954: return "0xb8a HD_PART_E partition error";
case 2955: return "0xb8b HD_BUF_E Buffer Fehler";
case 2956: return "0xb8c HD_PWD_E falsches Passw";
case 2957: return "0xb8d HD_HDL_E Handel Fehler";
case 2958: return "0xb8e HD_MODE_E falscher Modus";
case 2959: return "0xb8f HD_CHECK_E Checksum Fehler";
case 2960: return "0xb90 HD_NOPART_E no partition error";
case 2961: return "0xb91 HD_DISK_FULL_E disk_full error";

/*
 * KEYBOARD-MANAGER
 */

case 3101: return "0xc1d KEYB_OVF_E Ueberlauf";
case 3103: return "0xc1f KEYB_DAT_E Tastatur-Ebene nicht gueltig";
case 3104: return "0xc20 KEYB_INIT_E Manager nicht initialisiert";
case 3105: return "0xc21 KEYB_FKT_E Funktions-Fehler";
case 3150: return "0xc4e KEYB_ERR Allgemeiner Fehler";
case 3151: return "0xc4f KEYB_STAT_E Fehler in STATUS-Fkt.";
case 3152: return "0xc50 KEYB_INITA_E Fehler in INIT-Fkt.";
case 3153: return "0xc51 KEYB_CTRL_E Fehler in CTRL-Fkt.";
case 3154: return "0xc52 KEYB_FLUSH_E Fehler in FLUSH-Fkt.";

/*
 * GRAFIK-MANAGER
 */

case 3301: return "0xce5 GR_OVF_E Koord. ausser Bereich";
case 3303: return "0xce7 GR_DAT_E Invalid Input Parameter";
case 3304: return "0xce8 GR_INIT_E Initialisierungs-Fehler";
case 3305: return "0xce9 GR_FKT_E Funktion nicht vorhanden";
case 3312: return "0xcf0 GR_RID_E Ungueltige Resource-ID";
case 3350: return "0xd16 GR_ERR Allg. Grafik-Fehler";
case 3351: return "0xd17 GR_POLY_E Polygon-Fehler";
case 3352: return "0xd18 GR_HDL_E Ungueltige Handle";
case 3353: return "0xd19 GR_ID_E Ungueltige ID";
case 3354: return "0xd1a GR_MEM_E Speicher-Fehler Grafik";
case 3356: return "0xd1c GR_FONT_E Font-Fehler Grafik";

/*
 * PRINTER-MANAGER
 */

case 3401: return "0xd49 PR_OVF_E Empfangsbuffer zu klein bzw. Stoerung im Uebertragungs-Kanal (DMA-Ueberlauf)";
case 3403: return "0xd4b PR_DAT_E Uebergabe-Parameter nicht ok";
case 3404: return "0xd4c PR_INIT_E Init. nicht ok.";
case 3405: return "0xd4d PR_FKT_E Funktion existiert nicht";
case 3407: return "0xd4f PR_ID_E Printer nicht gemountet";
case 3422: return "0xd5e PR_TOUT_E Printer-Task-Timeout";
case 3450: return "0xd7a PR_ERR Allgemeiner Printer-Mgr.Error";
case 3451: return "0xd7b PR_TIMEOUT_E Printer-Timeout-Error";
case 3452: return "0xd7c PR_NOBLK_E No Blocks request";
case 3453: return "0xd7d PR_READY_E Auftrag in Ready-Queue";
case 3454: return "0xd7e PR_LRC_E LRC Fehler";
case 3455: return "0xd7f PR_REPEAT_E Fehler bei mehrmaligem Wiederholen";
case 3456: return "0xd80 PR_WARN_E zuviele LRC Fehler";

/*
 * TON-MANAGER
 */

case 3503: return "0xdaf TON_DAT_E Ungueltige Daten";
case 3504: return "0xdb0 TON_INIT_E Ton-Manager Init.Fehler";
case 3512: return "0xdb8 TON_RID_E";
case 3550: return "0xdde TON_ERR Allg. Ton-Fehler";
case 3551: return "0xddf TON_DEV_E Ungueltiges Ton-Device";

/*
 * PORT-I/O-MANAGER
 */

case 3603: return "0xe13 PIO_DAT_E Ungueltige Parameter";
case 3604: return "0xe14 PIO_INIT_E Initialisierungsfehler";
case 3607: return "0xe17 PIO_ID_E Ungueltige Port-ID";
case 3650: return "0xe42 PIO_ERR Allgemeiner Pio-Fehler";

/*
 * UHREN-MANAGER
 */

case 3701: return "0xe75 UHR_OVF_E ungueltige Uhrzeit";
case 3702: return "0xe76 UHR_UVF_E ungueltige Uhrzeit";
case 3751: return "0xea7 UHR_BAU_E Baustein fehlerhaft";
case 3752: return "0xea8 UHR_DAT_E Daten nicht gueltig";
case 3753: return "0xea9 UHR_RID_E Resource nicht gefunden ";
case 3754: return "0xeaa UHR_INST_E Task nicht inst.";
case 3755: return "0xeab UHR_TIME_E falsche interne Zeit";

/*
 * MEN�-MANAGER
 */

case 3801: return "0xed9 MENU_OVF_E";
case 3802: return "0xeda MENU_UVF_E Es gibt kein Menue";
case 3803: return "0xedb MENU_DAT_E Ungueltige Daten";
case 3804: return "0xedc MENU_INIT_E Fehlerhafte Initialisierung";
case 3805: return "0xedd MENU_FKT_E Funktion nicht vorhanden";
case 3810: return "0xee2 MENU_HDL_E Ungueltiges Menu-Handle";
case 3818: return "0xeea MENU_MSG_E";

/*
 * CONTROL-MANAGER
 */

case 3901: return "0xf3d CTRL_OVF_E";
case 3903: return "0xf3f CTRL_DAT_E Ungueltige Daten";
case 3904: return "0xf40 CTRL_INIT_E Fehlerhafte Initialisierung";
case 3910: return "0xf46 CTRL_HDL_E Ungueltiges Control-Handle";
case 3918: return "0xf4e CTRL_MSG_E";

/*
 * EVENT-MANAGER
 */

case 4001: return "0xfa1 EVENT_OVF_E Event-Overflow";
case 4003: return "0xfa3 EVENT_DAT_E Daten-Fehler";
case 4004: return "0xfa4 EVENT_INIT_E Initialisierungs-Fehler";

/*
 * EDITOR-MANAGER
 */

case 4101: return "0x1005 EDIT_OVF_E Editor-Man. ist voll";
case 4103: return "0x1007 EDIT_DAT_E Uebergabedaten nicht";
case 4104: return "0x1008 EDIT_INIT_E Init. fehlerhaft";
case 4110: return "0x100e EDIT_HDL_E Handle ungueltig";
case 4118: return "0x1016 EDIT_MSG_E unerlaubter Zugriff";
case 4150: return "0x1036 EDIT_ERR_EOL String-Ende";
case 4151: return "0x1037 EDIT_ERR_TOL String-Anfang";
case 4152: return "0x1038 EDIT_ABB_RET Enter-Funktion";
case 4153: return "0x1039 EDIT_ABB_ESC Escape-Funktion";
case 4154: return "0x103A EDIT_ABB_UCUR Cursor-Up";
case 4155: return "0x103B EDIT_ABB_DCUR Cursor-Down";
case 4156: return "0x103C EDIT_ABB_SINGLE Single-Character";
case 4157: return "0x103D EDIT_ABB_USER User def. Abbruch-Code";

/*
 * LAYOUT-MANAGER
 */

case 4201: return "0x1069 LAY_OVF_E max. Anzahl von Layouts bzw. Feldern erreicht";
case 4203: return "0x106b LAY_DAT_E Daten nicht in Ordnung";
case 4204: return "0x106c LAY_INIT_E Initialisierungs-Fehler";
case 4205: return "0x106d LAY_FKT_E Funktion nicht verfuegbar";
case 4218: return "0x107a LAY_MSG_E Message nicht verfuegbar";
case 4250: return "0x109a LAY_ERR Allg. Layout-Fehler";
case 4252: return "0x109c LAY_HDL_E Ungueltige Handle";
case 4253: return "0x109d LAY_ID_E Ungueltige ID";
case 4254: return "0x109e LAY_MEM_E Speicher-Fehler";
case 4255: return "0x109f LAY_TEXT_E Text zu lang";
case 4256: return "0x10a0 LAY_FONT_E Font-Fehler Layout";

/*
 * LAYOUT-MANAGER F�R MASTER/SLAVE-ANWENDUNGEN
 */

/*
 * case 4270: return "0x10ae LAYPB_ERR";
 */
case 4270: return "0x10ae LAYPB_INIT_E Init-Fehler";
case 4271: return "0x10af LAYPB_BUS_E Bus nicht bereit";
case 4272: return "0x10b0 LAYPB_ANZ_E Anzeige n. bereit ";
case 4273: return "0x10b1 LAYPB_STAS_E Task nicht bereit ";
case 4274: return "0x10b2 LAYPB_TEL_E unguelt. Telegr. erhalten";
case 4275: return "0x10b3 LAYPB_SEND_E Fehler beim Senden";
case 4276: return "0x10b4 LAYPB_EMPF_E Fehler beim Empfangen";
case 4277: return "0x10b5 LAYPB_MOD_E Uebertr.modus nicht vorh.";
case 4278: return "0x10b6 LAYPB_NODEV_E kein Geraet am K-Bus verf. ";
case 4279: return "0x10b7 LAYPB_NOMEM_E kein Speicher mehr vorh.";
case 4280: return "0x10b8 LAYPB_SEM_E keine Semaphore erh.";

/*
 * DATENBANK-MANAGER
 */

case 4404: return "0x1134 DBM_INIT_E Datenbank nicht angelegt";
case 4405: return "0x1135 DBM_FKT_E Funktion nicht implementiert";
case 4406: return "0x1136 DBM_MGR_E Fundamentaler Fehler Dbm-Mgr";
case 4419: return "0x1143 DBM_PARA_E Falscher Parameter";
case 4451: return "0x1163 DBM_OPEN_E Fehler beim Dateioeffnen";
case 4452: return "0x1164 DBM_VERSION_E Falsche Programmversion";
case 4455: return "0x1167 DBM_CONF_E Falsche Attributkonfiguration";
case 4456: return "0x1168 DBM_NOATT_E Attribut nicht vorhanden";
case 4457: return "0x1169 DBM_TAB_E Tabelle schon angelegt";
case 4460: return "0x116c DBM_NOTAB_E Tabelle nicht vorhanden ";
case 4461: return "0x116d DBM_TYP_E Falscher Attributtyp";
case 4462: return "0x116e DBM_NOPART_E Partition nicht vorhanden ";
case 4470: return "0x1176 DBM_NOERW_E keine Erweiterung konfiguriert";
case 4471: return "0x1177 DBM_KEY_E Schluesseleigenschaft verletzt";
case 4472: return "0x1178 DBM_STR_E Fehler String";
case 4473: return "0x1179 DBM_MEMOUT_E Speicher zu Ende";
case 4474: return "0x117a DBM_SIZ_E Telegramm zu Ende";
case 4475: return "0x117b DBM_NOREC_E kein Datensatz (mehr) vorhanden";
case 4476: return "0x117c DBM_IND_E kein solcher Index vorhanden";
case 4477: return "0x117d DBM_SYN_E Syntaxfehler Selektionsformel";
case 4478: return "0x117e DBM_SEM_E Semantikfehler Selektionsformel";
case 4479: return "0x117f DBM_CODE_E Fehler beim Codegenerieren";
case 4480: return "0x1180 DBM_EXEC_E Fehler beim Ausfuehren";
case 4481: return "0x1181 DBM_VW_E Indexverweis falsch gesetzt";
case 4482: return "0x1182 DBM_INDST_E Index noch/schon angelegt";
case 4483: return "0x1183 DBM_VDEL_E vorlaeufig geloeschter Satz";
case 4484: return "0x1184 DBM_TBAL_E Balancefehler im Indexbaum";
case 4485: return "0x1185 DBM_COM_E Kein Commit moeglich";
case 4486: return "0x1186 DBM_MOD_E Falscher Zugriffsmode in Logfile";
case 4487: return "0x1187 DBM_TRANS_E Keine Transaktion moeglich";
case 4488: return "0x1188 DBM_COMPF_E Commit durch Power-Fail unterbrochen";
case 4489: return "0x1189 DBM_TPTR_E Zeigerfehler im Indexbaum";
case 4490: return "0x118a DBM_GLOB_E Tabelle ist nicht global";
case 4491: return "0x118b DBM_RIBUFULL_E Ringpuffer ist voll";
case 4492: return "0x118c DBM_REFDAT_E Referenzdatum war falsch";

/*
 * VERTEILTE DATENBANK-MANAGER
 */

case 4503: return "0x1197 VDBM_DAT_E Falsche Eingabeparameter";
case 4504: return "0x1198 VDBM_INIT_E Init-Fehler beim vDbm";
case 4505: return "0x1199 VDBM_FKT_E Funktion nicht implementiert";
case 4506: return "0x119a VDBM_MGR_E Fataler Fehler beim vDbm";
case 4522: return "0x11aa VDBM_TOUT_E Timeout beim Empfangen";
case 4551: return "0x11c7 VDBM_SIZ_E Satz zu lang fuer ein Telegramm";
case 4552: return "0x11c8 VDBM_REM_E Remote-Station antwortet nicht";
case 4553: return "0x11c9 VDBM_NOTAB_E Keine Tabelleninformation verfuegbar";
case 4554: return "0x11ca VDBM_TYP_E unbekannter Attributtyp ";
case 4555: return "0x11cb VDBM_PARA_E falscher Parameter ";
case 4556: return "0x11cc VDBM_TEL_E falsches Telegramm ";
case 4557: return "0x11cd VDBM_UPDREADY_E Update fertig ";
case 4558: return "0x11ce VDBM_UPDSTART_E Update gestartet ";
case 4559: return "0x11cf VDBM_UPDAF_E Fehler in Update-Aktionsfunktion ";
case 4560: return "0x11d0 VDBM_NOUPD_E kein Update noetig";
case 4561: return "0x11d1 VDBM_NODAT_E keine Daten in Part.";
case 4562: return "0x11d2 VDBM_TAB_E falsche Partition";
case 4563: return "0x11d3 VDBM_UPDPART_E kein Updatepartner";
case 4570: return "0x11da VDBM_OFFL_E Station ist Offline";
case 4580: return "0x11e4 VDBM_SUPAF_E Fehler Supertabellenaufbau";
case 4590: return "0x11ee VDBM_GLOB_E Tabelle nicht global";

/*
 * TELEGRAMM-MANAGER
 */

case 4604: return "0x11fc TEL_INIT_E Initialisierungs-Fehler";
case 4606: return "0x11fe TEL_MGR_E Fataler Fehler";
case 4610: return "0x1202 TEL_HDL_E Handle-Fehler";
case 4611: return "0x1203 TEL_QID_E Falsche Queue-ID";
case 4613: return "0x1205 TEL_TID_E Fehlerhafte TaskID";
case 4615: return "0x1207 TEL_MEM_E Speicher-Fehler";
case 4619: return "0x120b TEL_PARA_E Fehlerhafte I-Parameter";
case 4622: return "0x120e TEL_TIMEOUT_E Timeout-Fehler";
case 4650: return "0x122a TEL_DISCON_E Verbindung kann nicht abgebaut werden";
case 4651: return "0x122b TEL_CONNECT_E Verbindung ist abgebaut oder fehlerhaft";
case 4652: return "0x122c TEL_LEN_E Telegramm ist zu lang oder Laenge = 0";
case 4653: return "0x122d TEL_TART_E Falsche Telegrammart";
case 4654: return "0x122e TEL_PART_E Falsche Protokollart";
case 4655: return "0x122f TEL_QFL_E Queue voll";
case 4656: return "0x1230 TEL_SBUS_E Systembus overflow";
case 4657: return "0x1231 TEL_KBUS_E Fehler Komponentenbus";
case 4658: return "0x1232 TEL_LSV1_E Fehler LSV1-Protokoll";
case 4659: return "0x1233 TEL_ACK_E Fehler beim Quittieren";
case 4660: return "0x1234 TEL_CHKS_E Fehlerhafte Checksumme bei Depaketierung";
case 4661: return "0x1235 TEL_FRM_E Fehlerhafter Paket-Rahmen";
case 4662: return "0x1236 TEL_BREAK_E Senden und Empfangen abgebrochen";
case 4663: return "0x1237 TEL_WRTMO_E Timeout zu klein gewaehlt";
case 4664: return "0x1238 TEL_LSAPINIT_E LSAPs konnten nicht angelegt werden";
case 4665: return "0x1239 TEL_LSAP_E LSAP-Anforderung/Freigabe nicht m�glich";
case 4666: return "0x123a TEL_OFFL_E PROFIBUS befindet sich im Zustand OFFLINE";
case 4667: return "0x123b TEL_QINIT_E Queues konnten nicht angelegt werden";
case 4668: return "0x123c TEL_Q_E Queue-Anforderung/Freigabe nicht m�glich";
case 4669: return "0x123d TEL_NA_E Station nicht aktiv";
case 4670: return "0x123e TEL_DSAP_E LSAP-Anf. in Remote-Station nicht m�glich ";
case 4671: return "0x123f TEL_TIDINIT_E TID-Verwaltung konnte nicht init. werden";
case 4672: return "0x1240 TEL_AKTID_E Falsche Aktionstask-ID";
case 4673: return "0x1241 TEL_STL_E Stationslisten-Fehler";
case 4674: return "0x1242 TEL_ACK2_E Quittungs-Fehler von der Ebene 2";
case 4675: return "0x1243 TEL_WAANR_E Fehlerhafte Waagennummer";
case 4676: return "0x1244 TEL_HDLC_E Fehler HDLC-Protokoll";
case 4677: return "0x1245 TEL_HDLC_QUEUE_E Fehler in der Queue auf der Gegenseite";
case 4678: return "0x1246 TEL_S2WAIT_E Timeout";
case 4679: return "0x1247 TEL_S2RR_E Resourcenmangel";
case 4680: return "0x1248 TEL_S2RS_E Resource nicht vorhanden";
case 4681: return "0x1249 TEL_S2UE_E Station antwortet nicht";
case 4682: return "0x124a TEL_S2NA_E Station antwortet nicht";
case 4683: return "0x124b TEL_S2LR_E lokaler Profibusf.";
case 4684: return "0x124c TEL_S2RD_E SRD-Dienstfehler";
case 4685: return "0x124d TEL_TCREATE_E VRTX-Task konnte nicht gestartet werden";
case 4686: return "0x124e TEL_DELVERB_E Verbindung wird durch Watchdog gel�scht";
case 4687: return "0x124f TEL_DELTEL_E Schrott-Telegramm beseitigt";
case 4688: return "0x1250 TEL_S2XX_E nicht erlaubter Stat. ";
case 4689: return "0x1251 TEL_VERB_E Verbindungsbeschreibung falsch ! CTRL-Kanal ";
case 4690: return "0x1252 TEL_SNDHDL_E Achtung ! Sendehandle ging verloren";
case 4691: return "0x1253 TEL_INCONSIST_E Telegrammadr. stimmt nicht mit VerbBesch- Adr �berein";
case 4692: return "0x1254 TEL_PC_LISTE_E Fehler in Verbindungsliste";
case 4693: return "0x1255 TEL_DDINIT_E Wiederholte Managerinitialisierung";
case 4694: return "0x1256 TEL_CONSC_E SC-Fall, �berlast in Remotestn aufgetreten zu der eine Verbindung aufgebaut werden soll";
case 4695: return "0x1257 TEL_DDPAKET_E Statusmeldung: als erstes Tel. n-tes Paket empfangen, Tel wird gel�scht";

/*
 * CODE-MANAGER
 */

case 4701: return "0x125d CODE_OVF_E Code pa�t nicht in Rechteck";
case 4703: return "0x125f CODE_DAT_E Code-Daten falsch";
case 4704: return "0x1260 CODE_INIT_E Initialisierungs-Fehler";
case 4705: return "0x1261 CODE_FKT_E Funktion nicht verf�gbar";
case 4707: return "0x1263 CODE_ID_E Code nicht vorhanden";
case 4750: return "0x128e CODE_ERR Allg. Code-Fehler";
case 4751: return "0x128f CODE_CHECK_E Pr�fziffer falsch";

/*
 * ARITHMETIK-MANAGER
 */

case 4801: return "0x12c1 ARI_OVF_E �berlauf";
case 4802: return "0x12c2 ARI_UVF_E Unterlauf";
case 4851: return "0x12f3 ARI_LT_E kleiner als";
case 4852: return "0x12f4 ARI_GT_E gr��er als";
case 4853: return "0x12f5 ARI_EQ_E ist gleich";
case 4854: return "0x12f6 ARI_FALSE_E Vergleich nicht erf�llt";
case 4855: return "0x12f7 ARI_NULL_E Division durch Null";

/*
 * STRING-MANAGER
 */

case 4901: return "0x1325 STR_CHAR_E Zeichen belegt mehr als 2 Bytes ";
case 4902: return "0x1326 STR_SIZE_E size ist kleiner als Source-String";
case 4903: return "0x1327 STR_MEM_E Speicher-Fehler ";
case 4904: return "0x1328 STR_GT String 1 > String 2";
case 4905: return "0x1329 STR_LT String 1 < String 2 ";
case 4906: return "0x132a STR_NIL_E Null-Pointer";
case 4907: return "0x132b STR_DAT_E String-Daten-Fehler";
case 4950: return "0x1356 STR_ERR Allgemeiner String-Fehler";
case 4951: return "0x1357 STR_FORMAT_E String-Format-Fehler";
case 4952: return "0x1358 STR_RSC_E L�nder-Resource fehlt";

/*
 * UTILITY-MANAGER
 */

case 5001: return "0x1389 UTIL_OVF_E �berlauffehler";
case 5003: return "0x138b UTIL_DAT_E Util-Datenfehler";
case 5004: return "0x138c UTIL_INIT_E Fehler beim Initial.";
case 5010: return "0x1392 UTIL_HDL_E Ung�ltige Handle";
case 5050: return "0x13ba UTIL_ERR Allg. Util-Fehler";
case 5051: return "0x13bb UTIL_LT_E kleiner als";
case 5052: return "0x13bc UTIL_GT_E gr��er als";
case 5053: return "0x13bd UTIL_KON_E Fehler beim Konvertieren";

/*
 * DIAGNOSE-MANAGER
 */

case 5305: return "0x14b9 DIA_FKT_E";
case 5315: return "0x14c3 DIA_MEM_E";
case 5319: return "0x14c7 DIA_PARA_E";
case 5351: return "0x14e7 DIA_AKTIV_E Diagnose an dieser Waage";
case 5352: return "0x14e8 DIA_FERTIG_E alle Waagen bearbeitet";
case 5353: return "0x14e9 DIA_WAAGEN_E Station nicht vorhanden";
case 5354: return "0x14ea DIA_SBUS_E Fehler beim Profibus";
case 5357: return "0x14ed DIA_TIMEOUT_E Timeout";
case 5358: return "0x14ee DIA_Q_E Queue";
case 5359: return "0x14ef DIA_ACK_E Quittungsfehler";
case 5360: return "0x14f0 DIA_MBOX_OVFL_E Mailbox�berlauf";
case 5361: return "0x14f1 DIA_QUEUE_OVFL_E Queueueberlauf";
case 5362: return "0x14f2 DIA_SIZ_E falsche Datenl�nge";
case 5363: return "0x14f3 DIA_NOTAB_E Fehlerhafter Diagnosetyp";
case 5364: return "0x14f4 DIA_MGR_E";

/*
 * DATENBANK-HARD-DISK-MANAGER
 */

case 5451: return "0x154b DBMHD_FREE_E FreeTab error";
case 5452: return "0x154c DBMHD_FPFULL_E Aloc error";
case 5453: return "0x154d DBMHD_UEBER_E PartFull error";
case 5454: return "0x154e DBMHD_BACKUP_E Backup error";
case 5455: return "0x154f DBMHD_NODATBER_E No DatBer";
case 5460: return "0x1554 DBMHD_NOTAB_E NoTab error";
case 5461: return "0x1555 DBMHD_TAB_E Tab error";
case 5462: return "0x1556 DBMHD_REQ_E Request error";
case 5463: return "0x1557 DBMHD_DBA_E Dba error";
case 5464: return "0x1558 DBMHD_PARA_E Para error";
case 5465: return "0x1559 DBMHD_NOERW_E No-Erw error";
case 5475: return "0x1563 DBMHD_NOREC_E NoRec error";
case 5476: return "0x1564 DBMHD_REC_E Rec error";

/*
 * HARD-DISK-KONFIGURATIONS-MANAGER
 */

case 5551: return "0x15af HDCF_NOKONF_E Suche nach Konfigdata nicht erfolgreich";
case 5552: return "0x15b0 HDCF_FPFULL_E Kein freier Platz in Freiplatzliste gefunden";
case 5562: return "0x15ba HDCF_REQ_E Anforderung eines freien KonfigBereichs nicht erfolgreich";

/*
 * DIALOG-MANAGER
 */

case 5704: return "0x1648 DIALOG_INIT_E Init. Fehler";
case 5707: return "0x164b DIALOG_ID_E ung�ltige ID";
case 5715: return "0x1653 DIALOG_MEM_E kein Speicher";
case 5749: return "0x1675 DIALOG_ERR allg. Fehler";

/*
 * TEXT-MANAGER
 */

case 5901: return "0x170d TEXT_OVF_E Overflow";
case 5903: return "0x170f TEXT_DAT_E Parameter-Fehler";
case 5904: return "0x1710 TEXT_INIT_E Initialisierungs-Fehler";
case 5950: return "0x173e TEXT_ERR Allg. Text-Fehler";
case 5954: return "0x1742 TEXT_MEM_E Speicher-Fehler";
case 5955: return "0x1743 TEXT_TEXT_E Text zu lang";
case 5956: return "0x1744 TEXT_FONT_E Font-Fehler";
case 5957: return "0x1745 TEXT_PARSE_E Parse-Fehler";
case 5962: return "0x174a TEXT_FOUND_E Text nicht gefunden";

/*
 * PROFIBUS-PC-KARTE, DPR-MANAGER
 */

case 6103: return "0x17d7 DPR_NO_DATA_E";
case 6104: return "0x17d8 DPR_TIMEOUT_E";
case 6106: return "0x17da DPR_TASK_ANZAHL_E";
case 6107: return "0x17db DPR_DATA_SIZE_E";
case 6108: return "0x17dc DPR_PARA_E";
case 6109: return "0x17dd DPR_FKT_E";
case 6110: return "0x17de DPR_CTRL_E";

/*
 * 16-BIT-EDITOR-MANAGER
 */

case 6201: return "0x1839 EDIT16_OVF_E";

/*
 * AS-spezifische Fehlercodes
 */

/*
 * ABLAUFSTEUERUNG
 */

case 15150: return "0x3b2e PAE_ABL_ETI_IN_BEARB";
case 15151: return "0x3b2f PAE_ABL_INPUT_LOCKED";
case 15152: return "0x3b30 PAE_ABL_TMO_GEW_RUHE";
case 15153: return "0x3b31 PAE_ABL_TMO_GEW_STOP";
case 15154: return "0x3b32 PAE_ABL_TMO_DRU_PRINT";
case 15155: return "0x3b33 PAE_ABL_ETI_NICHT_FERTIG";
case 15156: return "0x3b34 PAE_ABL_TMO_PRINT_SUM";
case 15157: return "0x3b35 PAE_ABL_FIFO_VOLL";
case 15158: return "0x3b36 PAE_ABL_FIFO_LEER";
case 15159: return "0x3b37 PAE_ABL_SUMST";
case 15160: return "0x3b38 PAE_ABL_REMETI_NICHT_FERTIG";
case 15161: return "0x3b39 PAE_ABL_NO_MINMAX";
case 15162: return "0x3b3a PAE_ABL_CASE_FAULT";
case 15163: return "0x3b3b PAE_ABL_UNLOCK_DISABLE Freigeben ist dem Bediener nicht erlaubt";
case 15164: return "0x3b3c PAE_ABL_STATISTIK Initialisierung Statistikfunktion";
case 15165: return "0x3b3d PAE_ABL_STATI_MW_O Mittelwert der Grundverteilung weicht oberhalb von dem Nenngewicht stark ab";
case 15166: return "0x3b3e PAE_ABL_STATI_MW_U Mittelwert der Grundverteilung weicht unterhalb von dem Nenngewicht stark ab";
case 15167: return "0x3b3f PAE_ABL_STATI_ABW Standardabweichung der Grundverteilung ist gro�";
case 15168: return "0x3b40 PAE_ABL_STATI_PAK_O Neue Packung liegt zu weit �ber dem Nenngewicht";
case 15169: return "0x3b41 PAE_ABL_STATI_PAK_U Neue Packung liegt zu weit unter dem Nenngewicht";

/*
 * AUTOMATENSTEUERUNG
 */

case 15250: return "PAE_AM_CASE_FAULT Fehler hat keine Auswirkungen";
case 15251: return "PAE_AM_FIFO_VOLL";
case 15252: return "PAE_AM_FIFO_LEER";
case 15253: return "PAE_AM_ACSTAT";
case 15254: return "PAE_AM_KONVTAB";
case 15255: return "PAE_AM_KBU_ERR";
case 15256: return "PAE_AM_AC_REVISION";
case 15257: return "PAE_AMABL_HDL_FALSCH";
case 15258: return "PAE_AMABL_FIFO_LEER";
case 15259: return "PAE_AMABL_PACK_ZUORDNUNG";
case 15260: return "PAE_AMABL_CASE_FAULT Fehler hat keine Auswirkungen";
case 15261: return "PAE_AMABL_PCKST";

/*
 * ANZEIGE
 */

case 15350: return "PAE_ANZ_OCC Layout belegt";
case 15351: return "PAE_ANZ_NOENTRY Layout frei";


/*
 * SCHNITTSTELLE
 */

case 15550: return "PAE_COM_ADMIN_K_A";
case 15551: return "PAE_COM_WORK1_K_A_E Falsche Aktion im lfdn. Betrieb";
case 15552: return "PAE_COM_WORK2_K_A_E Falsche Aktion bei neuer Datei";
case 15553: return "PAE_COM_PASSW_FEHLT_E Passwort benoetigt";
case 15554: return "PAE_COM_PASSW_FALSCH_E Passwort falsch";
case 15555: return "PAE_COM_WRITE_K_D_E Keine Datei fuer ComWrite";
case 15556: return "PAE_COM_GLOBL1_K_D_E Unbekannte Datei in ComUtil";
case 15557: return "PAE_COM_GLOBL2_K_D_E Unbekannte Datei in ComDevUtil";
case 15558: return "PAE_COM_GLOBL3_K_D_E Unbekannte Nachricht in ComUtil";
case 15559: return "PAE_COM_READ_K_D_E Keine Datei fuer ComRead ";
case 15560: return "PAE_COM_GLOBL1_WAA_NU_E Keine gueltige Geraetenummer";
case 15561: return "PAE_COM_GLOBL2_WAA_NU_E Nachricht: Geraetenummer falsch";
case 15562: return "PAE_COM_TAB_NICHT_AKTIV_E Tabelle nicht aktiv";
case 15563: return "PAE_COM_BLOCK_E Falsche Blocknummer in ComRead";
case 15564: return "PAE_COM_BUFFSIZE_E Buffer zu klein fuer TYatt-Struktur";
case 15565: return "PAE_COM_GLOBL1_F_E Fataler Fehler in ComGlobl";
case 15566: return "PAE_COM_GLOBL2_F_E Fataler Fehler in ComUtil";
case 15567: return "PAE_COM_WRITE_F_E Fataler Fehler in ComWrite";
case 15568: return "PAE_COM_ERROR_ABB_ERZW_E Abbruch erzwungen in ComError";
case 15569: return "PAE_COM_WORK1_ABB_ERZW_E Abbruch erzwungen im lfdn. Betrieb";
case 15570: return "PAE_COM_WORK2_ABB_ERZW_E Abbruch erzwungen bei neuer Datei";
case 15571: return "PAE_COM_MSG_K_N_E Keine gueltige Nachricht";
case 15572: return "PAE_COM_MSG_P_E Nachrichten-Parameter falsch";
case 15573: return "PAE_COM_MSG_N_A_E Nachricht nicht ausgefuehrt";
case 15574: return "PAE_COM_SWC_VERSION_E SWCom-Version falsch";
case 15575: return "PAE_COM_SWC_REVISION_E SWCom-Revision falsch";
case 15576: return "PAE_COM_SWC_DATUM_E SWCom-Programmdatum falsch";
case 15577: return "PAE_COM_CAS_VERSION_E CAS-Version falsch";
case 15578: return "PAE_COM_ALLG_E Ende COM-Fehlerbereich";

/*
 * DRUCKER
 */

case 15650: return "0x3d22 PAE_DRU_TASK_STATUS";
case 15651: return "0x3d23 PAE_DRU_ALLG";
case 15652: return "0x3d24 PAE_DRU_CMD_FOLGE";
case 15653: return "0x3d25 PAE_DRU_TAB_DAT";
case 15654: return "0x3d26 PAE_DRU_OVF_ERR";
case 15655: return "0x3d27 PAE_DRU_UNBEK_CMD";
case 15656: return "0x3d28 PAE_DRU_LRC1";
case 15657: return "0x3d29 PAE_DRU_LRC2";
case 15658: return "0x3d2a PAE_DRU_AKT_CMD";
case 15659: return "0x3d2b PAE_DRU_ALLG_TAB";
case 15660: return "0x3d2c PAE_DRU_WEG_ETI";
case 15661: return "0x3d2d PAE_DRU_REGELUEBERLAUF";
case 15662: return "0x3d2e PAE_DRU_ANTR_STROM";
case 15663: return "0x3d2f PAE_DRU_WICKEL_STROM";
case 15664: return "0x3d30 PAE_DRU_DOT_TEMP";
case 15665: return "0x3d31 PAE_DRU_RUECK_COD";
case 15666: return "0x3d32 PAE_DRU_SYNC_BUS";
case 15667: return "0x3d33 PAE_DRU_W_MOT";
case 15668: return "0x3d34 PAE_DRU_KOPF_ABGEH";
case 15669: return "0x3d35 PAE_DRU_KEIN_PAPIER";
case 15670: return "0x3d36 PAE_DRU_FATAL";
case 15671: return "0x3d37 PAE_DRU_LS";
case 15672: return "0x3d38 PAE_DRU_TTF_PAPIER";
case 15673: return "0x3d39 PAE_DRU_LOKAL_BREMSWEG";
case 15674: return "0x3d3a PAE_DRU_DC_VERS";
case 15675: return "0x3d3b PAE_DRU_TABBUF";
case 15676: return "0x3d3c PAE_DRU_UNBEK_ETILEN";
case 15677: return "0x3d3d PAE_DRU_DEND";
case 15678: return "0x3d3e PAE_DRU_BON_LEN";
case 15679: return "0x3d3f PAE_DRU_NICHT_DA";
case 15680: return "0x3d40 PAE_DRU_STRLEN Wenn String f�r Bondruck zu lang";

/*
 * ETIKETTENAUFBEREITUNG
 */

case 15750: return "0x3d86 PAE_ETI_PREISBER";
case 15751: return "0x3d87 PAE_ETI_NOLAYOUT";
case 15752: return "0x3d88 PAE_ETI_VORSCHUB Vorschub zu weit, Numerikfelder werden vorgedruckt";
case 15753: return "0x3d89 PAE_ETI_FEHLER4";

/*
 * GEWICHTSVERARBEITUNG
 */

case 15850: return "0x3dea PAE_HAI_SEND Fehler beim Senden zum ADW-Manager";
case 15851: return "0x3deb PAE_HAI_RCV Fehler beim Empfangen vom ADW_Manager";
case 15852: return "0x3dec PAE_HAI_NOINT Fehler beim initialisieren des ADW-Managers";
case 15853: return "0x3ded PAE_HAI_TELETYP Falscher Telegrammtype erhalten";
case 15854: return "0x3dee PAE_HAI_ZUSTAND Falscher Zustand f�r empfangene Nachricht";
case 15855: return "0x3def PAE_HAI_QUELLE Fehler beim Auslesen der Nachrichtenqueue";
case 15856: return "0x3df0 PAE_HAI_ABLAUF Ablauffehler in Durchlaufverw�gung";
case 15857: return "0x3df1 PAE_HAI_INT_CHG Fehler beim Speichern der Waagenparameter im GX";

/*
 * INITIALISIERUNG
 */

case 15950: return "PAE_INI_FATAL";
case 15951: return "PAE_INI_TASK_START";

/*
 * EINGABE
 */

case 16050: return "0x3eb2 PAE_MEN_KEIN_TEXT Text nicht vorhanden";
case 16051: return "0x3eb3 PAE_MEN_TOGGLE Fehler beim Toggeln eines Softkeyschalters";
case 16052: return "0x3eb4 PAE_MEN_SBUS Keine verteilte Datenbankfunktion beim n�chsten Einschalten m�glich";
case 16053: return "0x3eb5 PAE_MEN_NO_ANZFKT Keine Anzeigefunktion";
case 16054: return "0x3eb6 PAE_MEN_NO_FKT Keine Softkeyaktion";
case 16055: return "0x3eb7 PAE_MEN_ABRUND Wert wurde abgerundet";
case 16056: return "0x3eb8 PAE_MEN_AUFRUND Wert wurde aufgerundet";
case 16057: return "0x3eb9 PAE_MEN_ETIDATEN Keine Etikettendaten vorhanden";
case 16058: return "0x3eba PAE_MEN_ETIDATDEL Etikettendatensatz konnte nicht gel�scht werden";
case 16059: return "0x3ebb PAE_MEN_DIM Fehler bei Gewichtsdimensionierung";
case 16060: return "0x3ebe PAE_MEN_DB_NOPARAM Fehlender Eintrag in MEN-DB-Tabelle";
case 16061: return "0x3ebc PAE_MEN_DIM_SUM_LOAD Dimensionsfehler beim Laden von Summendaten";
case 16062: return "0x3ebd PAE_MEN_DIM_SUM_STOR Dimensionsfehler beim Speichern von Summendaten";
case 16063: return "0x3ebf PAE_MEN_KONV Datenkonvertierungsfehler";
case 16064: return "0x3ec0 PAE_MEN_NO_MINMAX Keine Gewichtsklassentabelle vorhanden";
case 16065: return "0x3ec1 PAE_MEN_REM_NO_SK Softkeyaktion �ber REMOTE gesperrt";
case 16066: return "0x3ec2 PAE_MEN_SCROLL Fehler innerhalb Scrollmen�s";
case 16067: return "0x3ec3 PAE_MEN_GDV_SK Softkeyaktion 'offen' bei gleichzeitigem Schreiben �ber Ger�tedatenverwalter";
case 16068: return "0x3ec4 PAE_MEN_WAA_TYP Fehler bei angeschlossener Waage";
case 16069: return "0x3ec5 PAE_MEN_DBV_TOGGLE Unzul�ssige Systembusadresse";
case 16070: return "0x3ec6 PAE_AMMEN_AMDATEN Automatendatensatz nicht vorhanden";
case 16071: return "0x3ec7 PAE_AMMEN_XXX reserviert";
case 16080: return "0x3ed0 PAE_VMMEN_AMDATEN Verpackungsdatensatz nicht vorhanden";
case 16081: return "0x3ed1 PAE_VMMEN_XXX reserviert";
case 16085: return "0x3ed5 PAE_MEN_READ_SK_TXT Fehler beim Lesen von Softkeytexten";
case 16086: return "0x3ed6 PAE_MEN_READ_SK_DATA Fehler beim Lesen von Softkeydaten";
case 16087: return "0x3ed7 PAE_MEN_NO_NUTRITION Kein Nutritiondatensatz vorhanden";
case 16088: return "0x3ed8 PAE_MEN_VERWBUF_OVERFLOW �berlauf Textverwaltungsbuffer";
case 16089: return "0x3ed9 PAE_MEN_NO_CAB Keine Codeaufbauvorschrift vorhanden";

/*
 * PLU-VERARBEITUNG
 */

case 16150: return "0x3f16 PAE_PLU_NULL PLU 0 nicht zul�ssig";
case 16151: return "0x3f17 PAE_PLU_WECHSEL PLU nicht vorhanden";
case 16152: return "0x3f18 PAE_PLU_TEXTFELD1_NEU Textfeld 1 neu nach PLU-Wechsel";
case 16153: return "0x3f19 PAE_PLU_NULL_PARAM PLU-Nummer 0 nicht zul�ssig";

/*
 * SUMMIERUNG
 */

case 16254: return "PAE_SUM_BUF_LEER";
case 16255: return "PAE_SUM_BUF_TOP";
case 16256: return "PAE_SUM_BUF_BOT";
case 16257: return "PAE_SUM_GRUPPENNR";
case 16258: return "PAE_SUM_STORNO";
case 16259: return "PAE_SUM_ID";
case 16260: return "PAE_SUM_TAB_BEG";
case 16261: return "PAE_SUM_TAB_END";
case 16262: return "PAE_SUM_OVF";
case 16263: return "PAE_SUM_TSNR";
case 16264: return "PAE_SUM_VORWAHL";
case 16265: return "PAE_SUM_LIST_END";
case 16266: return "PAE_SUM_LIST_STATEND";
case 16267: return "PAE_SUM_LIST_LEER";
case 16268: return "PAE_SUM_LIST_EXIST";
case 16278: return "PAE_SUM_MISCHSUMME";
case 16279: return "PAE_SUM_LIST_NOT_EXIST";
case 16280: return "PAE_SUM_PLUNR";
case 16281: return "PAE_SUM_MODE";

/*
 * VERPACKUNGSMASCHINE
 */

case 16450: return "0x4042 PAE_VPM_ERR_1 Allgemeiner Fehler";
case 16451: return "0x4043 PAE_VPM_ERR_2 Allgemeiner Fehler";
case 16452: return "0x4044 PAE_VPM_ERR_QCE Queue create Fehler";
case 16453: return "0x4045 PAE_VPM_ERR_PE EDV Protokoll Fehler";
case 16454: return "0x4046 PAE_VPM_ERR_MTL EDV Nachricht ist zu lang";
case 16455: return "0x4047 PAE_VPM_ERR_CSE EDV checksum Fehler";
case 16456: return "0x4048 PAE_VPM_ERR_STOP W31-Stop Fehler";
case 16457: return "0x4049  PAE_VPM_ERR_GV33_92 W31/33 Fehler 92";
case 16458: return "0x404a PAE_VPM_ERR_GV33_95 W31/33 Fehler 95";
case 16459: return "0x404b PAE_VPM_ERR_GV33_UK Kommando unbekannt";
case 16460: return "0x404c PAE_VPM_ERR_GV33_ABL Fehler in der Kommandoreihenfolge ";
case 16461: return "0x404d PAE_VPM_ERR_DRU_NR Drucker nicht bereit";
case 16470: return "0x4056 PAE_VM_CASE_FAULT interner Softwarefehler";
case 17471: return "0x4057 PAE_VM_FIFO_VOLL interner Softwarefehler";
case 16472: return "0x4058 PAE_VM_FIFO_LEER interner Softwarefehler";
case 16473: return "0x4059 PAE_VM_VC_REVISION falsche VCPU Version";
case 16474: return "0x405a PAE_VM_KONVTAB interner Softwarefehler";
case 16475: return "0x405b PAE_VM_KBU_ERR Komponentenbus Fehler";
case 16476: return "0x405c PAE_VM_HDL_ERR interner Softwarefehler ";
case 16477: return "0x405d PAE_VMABL_FIFO_LEER interner Softwarefehler";
case 16478: return "0x405e PAE_VMABL_PACK_ZUORDNUNG interner Softwarefehler";
case 16479: return "0x405f PAE_VMABL_CASE_FAULT interner Softwarefehler";
case 16480: return "0x4060 PAE_VMABL_DRU_BEREIT_ERR Drucker ist nicht bereit";
case 16481: return "0x4061 PAE_VMABL_DRU_PRINT_ERR Druckerfehler";
case 16482: return "0x4062 PAE_VMABL_GEW_NULL Gewichtsruhe bei Gewicht Null";

/*
 * CODE-EDITOR
 */

case 17004: return "0x426c PAE_CED_INIT";
case 17050: return "0x429a PAE_CED_ALLG_ERR";
case 17051: return "0x429b PAE_CED_MEN_FELDNR";
case 17052: return "0x429c PAE_CED_MEN_NOMENU";
case 17053: return "0x429d PAE_CED_MEN_ART";
case 17054: return "0x429e PAE_CED_MEN_FELDINAKTIV";
case 17055: return "0x429f PAE_CED_MEN_CURS_POS";
case 17056: return "0x42a0 PAE_CED_EING_STR_FULL";
case 17057: return "0x42a1 PAE_CED_EING_STR_EMPTY";
case 17058: return "0x42a2 PAE_CED_DAT_ERR";
case 17059: return "0x42a3 PAE_CED_TAB_END";
case 17060: return "0x42a4 PAE_CED_TAB_BEG";
case 17061: return "0x42a5 PAE_CED_INTERP_STR_FULL max. Codestringlaenge bereits erreicht";
case 17062: return "0x42a6 PAE_CED_INTERP_DAT_ERR Datenfehler: z.B. Codeteilstr fehlt";
case 17063: return "0x42a7 PAE_CED_INTERP_AUFB_ERR Fehler im Codeaufbau";
case 17064: return "0x42a8 PAE_CED_INTERP_VARSTELLEN_ERR Variable laenger als vorgeg";
case 17065: return "0x42a9 PAE_CED_INTERP_AUFBAU_EMPTY Wenn Aufbauvorschrift leer";
case 17066: return "0x42aa PAE_CED_INTERP_FLK_ERR Wenn Fliesskomma unerlaubterweise";
case 17067: return "0x42ab PAE_CED_NO_MEM";
case 17068: return "0x42ac PAE_CED_CAB_FULL";
case 17069: return "0x42ad PAE_CED_CAB_NO_INHALT";
case 17070: return "0x42ae PAE_CED_CABNR_ERR";
case 17071: return "0x42af PAE_CED_CODEID_ERR";
case 17072: return "0x42b0 PAE_CED_MODBR_ERR";
case 17073: return "0x42b1 PAE_CED_RATIO_ERR";
case 17074: return "0x42b2 PAE_CED_CODEHOEHE_ERR";
case 17075: return "0x42b3 PAE_CED_CODESTRUKT_ERR";
case 17076: return "0x42b4 PAE_CED_STRUKT_U_RATIO_ERR";
case 17077: return "0x42b5 PAE_CED_FORM1_ERR";
case 17078: return "0x42b6 PAE_CED_FORM2_ERR";
case 17079: return "0x42b7 PAE_CED_INH_ERR";
case 17080: return "0x42b8 PAE_CED_KONST_ERR";
case 17081: return "0x42b9 PAE_CED_OPEN_ERR";
case 17082: return "0x42ba PAE_CED_CLOSE_ERR";
case 17083: return "0x42bb PAE_CED_FLAG_ERR";
case 17084: return "0x42bc PAE_CED_NOCAB_ERR";
case 17085: return "0x42bd PAE_CED_TMPBUFNR_ERR";
case 17086: return "0x42be PAE_CED_INHPOS_ERR";
case 17087: return "0x42bf PAE_CED_CAB_ERR";
case 17088: return "0x42c0 PAE_CED_INTERP_PREIS";
case 17089: return "0x42c1 PAE_CED_INTERP_GEWICHT";
case 17090: return "0x42c2 PAE_CED_INTERP_STUECK";
case 17091: return "0x42c3 PAE_CED_CFELD_FLK_ERR Wenn Fliesskomma unerlaubt verwendet wird";
case 17092: return "0x42c4 PAE_CED_ Wenn ung�ltige Stellenanzahl im Code einkodiert wird";

/*
 * MEMO-CARD
 */

case 17151: return "0x42ff PAE_MC_NOTFIND Daten nicht gefunden";
case 17152: return "0x4300 PAE_MC_NOPART keine freie Partition mehr vorhanden";
case 17153: return "0x4301 PAE_MC_NOGRIP kein Zugriff auf MemoCard";
case 17154: return "0x4302 PAE_MC_HARDW Hardware-Fehler vom Treiber";
case 17155: return "0x4303 PAE_MC_NOTYP Unbekanntes Ger�t";
case 17156: return "0x4304 PAE_MC_FIND Daten bereits vorhanden";
case 17157: return "0x4305 PAE_MC_SIZE Nicht genug Platz auf der MemoCard";
case 17158: return "0x4306 PAE_MC_NOTDO Anwenderanweisung nicht durchf�hren";
case 17159: return "0x4307 PAE_MC_VERSION Falsche Version";
case 17160: return "0x4308 PAE_MC_BUSY MemoCard ist bereits in Bearbeitung";
case 17161: return "0x4309 PAE_MC_PARS Fehler bei der Auswertung eines Telegramms �ber Schnittstelle";
case 17162: return "0x430a PAE_MC_ANZREC Anzahl der Records stimmt nicht mit gespeicherter anzRec �berein";
case 17163: return "0x430b PAE_MC_NOSAVE Abbruch der Datensicherung";
case 17164: return "0x430c PAE_MC_NORSC keine Resource vorhanden";
case 17165: return "0x430d PAE_MC_FULL MemoCard ist voll";
case 17166: return "0x430e PAE_MC_NOREC kein Datensatz auf MemoCard vorhanden";

/*
 * VERBINDUNGSSCHICHT
 */

case 17250: return "0x4362 PAE_NET_CASE_FAULT Case gibts nicht";
case 17251: return "0x4363 PAE_NET_FIFO_VOLL";
case 17252: return "0x4364 PAE_NET_FIFO_LEER";
case 17253: return "0x4365 PAE_NET_GER_ID";
case 17254: return "0x4366 PAE_NET_BUS_TYP";
case 17255: return "0x4367 PAE_NET_SW Programmier/Denkfehler";
case 17256: return "0x4368 PAE_NET_BUF_END Bufferende ueberschritten";
case 17257: return "0x4369 PAE_NET_ASCII_HEX Falsches Ascii-ZeichenFEHLER";
case 17258: return "0x436a PAE_NET_WD_QUITPAR_VOLL WD-QuittungsParameter-Verwaltung voll";
case 17259: return "0x436b PAE_NET_WD_QUITPAR_FAULT WD-QuittungsParameter nicht gefunden";
case 17260: return "0x436c PAE_NET_NESTING_LEVEL WD-QuittungsParameter nicht gefunden";
case 17261: return "0x436d PAE_NET_EDV_BELEGT EDV-Schnittst. anders belegt";
case 17262: return "0x436e PAE_NET_DB_VERB_VOLL Verbindung zur DB-Task ueberlastet";
case 17263: return "0x436f PAE_NET_DB_HDL Handle der Net-DB-Nachricht falsch";
case 17264: return "0x4370 PAE_NET_DB_VERB_ZUST Verbindungszustand Net-DB-Nachricht falsch";
case 17265: return "0x4371 PAE_NET_SBUS_FEHLT Systembus nicht initialisiert";
case 17266: return "0x4372 PAE_NET_VERB_FEHLT Verbindung existiert nicht (mehr)";
case 17267: return "0x4373 PAE_NET_REM_RPC_BACK";
case 17268: return "0x4374 PAE_NET_PRIO_ZU_HOCH eine Aktionstask mit zu hoher Prioritaet";
case 17269: return "0x4375 PAE_NET_BUF_UNGERADE Bei Ascii-Hex-Wandl unger. Telegr. L�nge";
case 17270: return "0x4376 PAE_NET_PSVCTRL_NOHDL Kein Handle f�r PSVCTRL-Kommando frei";
case 17271: return "0x4377 PAE_NET_TXT_TOO_BIG WD-Text gr��er als erlaubt";
case 17272: return "0x4378 PAE_NET_BRIDG_VOLL keine Bridgeverbindung mehr m�glich";
case 17273: return "0x4379 PAE_NET_BRIDG_RUECKSENDEN R�cksenden aus Bridge fehlgeschlagen";
case 17274: return "0x437a PAE_NET_WD_SYNTAX PSV_Telegramm Syntaxfehler";
case 17275: return "0x437b PAE_NET_MC_VERB_VOLL Verbindung zur MC-Task �berlastet";
case 17276: return "0x437c PAE_NET_MC_HDL Handle der Net-MC-Nachricht falsch";
case 17277: return "0x437d PAE_NET_MC_VERB_ZUST Verbindungszustand Net-MC-Nachricht falsch";
case 17278: return "0x437e PAE_NET_SINGL_TAB_DIM Gr��e der Singl-Kanalbitmaps zu klein";
case 17279: return "0x437f PAE_NET_WD_IN_PSVDATA Gx akzeptiert kein Empfang von PsvData";
case 17280: return "0x4380 PAE_NET_SINGL_EREIG_BELEGT letzter SinglEreignisSend nicht abgeschlossen";
case 17281: return "0x4381 PAE_NET_BUS_ADR_FALSCH";
case 17282: return "0x4382 PAE_NET_ASCII_SYNTAX";

/*
 * GER�TEDATENVERWALTER
 */

case 17350: return "0x43c6 PAE_GDV_MIN Datenbereichsgrenze unterschritten";
case 17351: return "0x43c7 PAE_GDV_MAX Datenbereichsgrenze �berschritten";
case 17352: return "0x43c8 PAE_GDV_NO_FKT Schreiben der Daten verhindert";
case 17353: return "0x43c9 PAE_GDV_NO_AKTION Keine GDV-Aktion";
case 17354: return "0x43ca PAE_GDV_ZUGRIFF_VERWEIGERT Zugriff auf Daten verweigert";
case 17355: return "0x43cb PAE_GDV_DIM_WARN Warnung aufgrund Dimensions�nderung";
case 17356: return "0x43cc PAE_GDV_SAME_DATA Warnung: Wert wurde nicht gesetzt, da alter Wert mit neuem Wert identisch";
case 17357: return "0x43cd PAE_DIM_WARN_LAND Dimensionswechsel des Preises";
case 17358: return "0x43ce PAE_DIM_WARN_GEWART Dimensionswechsel der Gewichtsart";

/*
 * FERNSTEUERINTERPRETER
 */

case 17450: return "0x442a PAE_REM_TAB_NOT_FOUND";
case 17451: return "0x442b PAE_REM_CMD_NOT_FOUND";
case 17452: return "0x443c PAE_REM_ZUGRIFF";
case 17453: return "0x443d PAE_REM_NOENTRY";
 
/*
 * AUFTRAGSBEARBEITUNG
 * DATENBANK
 */

case 17650: return "0x44f2 PAE_DB_KEY Schl�sselattribut ist fehlerhaft oder nicht vorhanden";
case 17651: return "0x44f3 PAE_DB_ATT Attribut ist fehlerhaft oder nicht vorhanden";
case 17652: return "0x44f4 PAE_DB_PARA �bergabeparameter ist fehlerhaft oder ist nicht vorhanden";
case 17653: return "0x44f5 PAE_DB_GLOBAL Fehler im globalen Teil";
case 17654: return "0x44f6 PAE_DB_LOKAL Fehler im lokalen Teil";
case 17655: return "0x44f7 PAE_DB_SIZE falsche Gr��e";
case 17656: return "0x44f8 PAE_DB_MODE falscher Modus verwendet";
case 17657: return "0x44f9 PAE_DB_UPD Fehler beim Update";
case 17658: return "0x44fa PAE_DB_DIFF unterschiedliche Attributkonfiguration";
case 17659: return "0x44fb PAE_DB_DIM fehlerhafte Dimension";
case 17660: return "0x44fc PAE_DB_SELECT falscher Zugriff auf die DB-Tabelle";
case 17661: return "0x44fd PAE_DB_KONV Dimensionsbehaftete Attribute wurden konvertiert";
case 17662: return "0x44fe PAE_DB_DEFAULT noch nicht verwendet";
case 17663: return "0x44ff PAE_DB_OVERFLOW Der Wertebereich eines Attributs wurde �berschritten";
case 17664: return "0x4500 PAE_DB_INPUT Die Eingabe eines Attributs ist erforderlich";

/*
 * EINGABEWERKZEUGE
 */

case 17750: return "0x4556 PAE_EWZ_DUMMY";

/*
 * ANBINDUNG PXT->GX
 */

case 17850: return "0x45ba PAE_PXT_NOEND Kein Endezeichen gefunden";
case 17851: return "0x45bb PAE_PXT_NOKENN Kennung wurde nicht gefunden";
case 17852: return "0x45bc PAE_PXT_NOLAND L�nderoption wurde nicht gefunden";
case 17853: return "0x45bd PAE_PXT_DIM Falsche Gewichtsdimension";
case 17854: return "0x45be PAE_PXT_TEXT Falsche Zeilennummer oder Zeichengr��e";

/*
 * LISTENDRUCKER
 */

case 17950: return "0x461e PAE_LPT_CMD Unbekanntes Kommando f�r Listendruckertreiber";
case 17951: return "0x461f PAE_LPT_CHAR Unbekanntes Zeichen f�r Listendruckertreiber";
case 17952: return "0x4620 PAE_LPT_RSC Listen-, Kommando-, Steuerzeichen- oder Sonderzeichenresource kann nicht gefunden werden.";
case 17953: return "0x4621 PAE_LPT_BUF Buffer�berlauf";
case 17954: return "0x4622 PAE_LPT_DAT Datenfehler oder keine Daten vorhanden";
case 17955: return "0x4623 PAE_LPT_TAS Listendruckertask existiert nicht oder kann nicht angelegt werden.";
case 17956: return "0x4624 PAE_LPT_LIST Liste nicht vorhanden";
case 17957: return "0x4625 PAE_LPT_TYP Ung�ltiger Listentyp";
case 17958: return "0x4626 PAE_LPT_LINE Listenzeile nicht gefunden";
case 17959: return "0x4627 PAE_LPT_KOPF Listenkopf nicht gefunden";
case 17960: return "0x4628 PAE_LPT_FIFO_VOLL Listenpuffer ist voll";
case 17961: return "0x4629 PAE_LPT_FIFO_LEER Listenpuffer enth�lt keine Daten";
case 17962: return "0x462a PAE_LPT_FIFO_KNAPP Listenpuffer l�uft zu. Nur noch 25% frei";
case 17963: return "0x462b PAE_LPT_STR_LEN String zu lang";
case 17964: return "0x462c PAE_LPT_DBM Datenbankauswertung f�r Listendruck wurde abgebrochen, weil die Datenbank einen Fehler lieferte";

/*
 * SERVICE & DIAGNOSEWERKZEUG
 */

case 18050: return "0x4682 PAE_SRV_AUSWAHL Falsche Auswahl getroffen";
case 18051: return "0x4683 PAE_SRV_NOINIT Keine entsprechende Initialisierung vorhanden";

/*
 * AUTOMATEN-CPU
 */

case 18105: return "0x46b9 Packung zu lang";
case 18106: return "0x46ba Fehler Etikettierer";
case 18107: return "0x46bb Etikettierer gest�rt";
case 18108: return "0x46bc Fehler Etikettierposition 1";
case 18109: return "0x46bd Fehler Etikettierposition 2";
case 18110: return "0x46be Fehler Weichenposition 1";
case 18111: return "0x46bf Fehler Weichenposition 2";
case 18112: return "0x46c0 Fehler Ausscheiderposition";
case 18113: return "0x46c1 Stop WB au�erhalb Fenster";
case 18114: return "0x46c2 Vereinzelungsfehler";
case 18115: return "0x46c3 Packung zu kurz";
case 18116: return "0x46c4 W�gung ung�ltig weil zwei Packungen auf Waage";
case 18150: return "0x46e6 Motorfehler";
case 18154: return "0x46ea Etiketten�ffnung versperrt";
case 18155: return "0x46eb Automatenbusfehler";
case 18156: return "0x46ec Vakuum fehlt";
case 18157: return "0x46ed Druckluft fehlt";
case 18158: return "0x46ee Weichenfehler";
case 18159: return "0x46ef Fehler Ausscheider";
case 18160: return "0x46f0 St�rung Referenzsensor (Rot)";
case 18161: return "0x46f1 Stau am Einlauf";
case 18162: return "0x46f2 Fehler Lichtschranke 1";
case 18163: return "0x46f3 Fehler Lichtschranke 2";
case 18164: return "0x46f4 Externer St�rungseingang TR";
case 18165: return "0x46f5 Fehler Netzteil";
case 18166: return "0x46f6 Befehl nicht verstanden";
case 18167: return "0x46f7 Fehler H�henverstellung 1";
case 18168: return "0x46f8 Fehler H�henverstellung 2";
case 18169: return "0x46f9 zu viele Packungen im System";
case 18170: return "0x46fa Fehler Gesamtkonfiguration intern";
case 18171: return "0x46fb Fehler Gesamtkonfiguration extern";
case 18172: return "0x46fc Fehler Eti1 Testumlauf";
case 18174: return "0x46fe Etikett zu sp�t fertig";
case 18175: return "0x46ff Etikett gar nicht fertig";
case 18176: return "0x4700 �berstrom Etikettierer";
case 18177: return "0x4701 Etikettiererschacht nicht eingerastet";
case 18178: return "0x4702 Remote_error (Packung unbekannt)";
case 18179: return "0x4703 Fehler Andruckrolle";
case 18181: return "0x4704 Daten-R�cklesen";

/*
 * ADW90-MANAGER
 */

case 40001: return "0x9c41 ADW90_RAM_E RAM Fehler";
case 40002: return "0x9c42 ADW90_EPROM_E EPROM Fehler";
case 40050: return "0x9c72 ADW90_CHKS_E Chksumfehler Abgleichdaten ";
case 40051: return "0x9c73 ADW90_PRW_E Adw90 Pruefwertfehler";
case 40053: return "0x9c75 ADW90_DIV0_E Division durch 0";
case 40054: return "0x9c76 ADW90_HWST_E Hardwarestatusfehler";
case 40055: return "0x9c77 ADW90_TBL_E Fehler Aufschalttabelle";
case 40056: return "0x9c78 ADW90_CHK1R_E Chksumfehl. 1.Block Recall ";
case 40057: return "0x9c79 ADW90_CHK2R_E Chksumfehl. 2.Block Recall ";
case 40058: return "0x9c7a ADW90_CHK3R_E Chksumfehl. 3.Block Recall ";
case 40059: return "0x9c7b ADW90_CHK4R_E Chksumfehl. 4.Block Recall ";
case 40060: return "0x9c7c ADW90_CHK1U_E Chksumfehl. 1.Block Empf.";
case 40061: return "0x9c7d ADW90_CHK2U_E Chksumfehl. 2.Block Empf.";
case 40062: return "0x9c7e ADW90_CHK3U_E Chksumfehl. 3.Block Empf.";
case 40063: return "0x9c7f ADW90_CHK4U_E Chksumfehl. 4.Block Empf.";
case 40064: return "0x9c80 ADW90_COMM_E unzulaessiger Befehl";
case 40065: return "0x9c81 ADW90_KENN_E unzulaessiger Kennung";
case 40066: return "0x9c82 ADW90_MODE_E nicht in Linearkal.mode";
case 40067: return "0x9c83 ADW90_PROM_E EEPROM kann nicht prog. werden";
default: return "Unbekannter GX Fehler";
}
}

/*
 * TASK-IDS
 * 0 TID_BOS_RTSCOPE RTscope (gesch�tzt)
 * 3 TID_BOS_SYN Synchron-Manager-Task
 * 4 TID_BOS_LOG Logbuch-Manager-Task
 * 5 TID_BOS_WAA Waagen-Manager-Task
 * 6 TID_BOS_ADW Adw-Manager-Task
 * 7 TID_BOS_IDLE Idle-Task
 * 8 TID_BOS_URAPP Applikations Task
 * 9 TID_BOS_UHR Uhr-Manager-Task
 * 10 TID_BOS_TELCTRL Telegramm-Task PB
 * 11 TID_BOS_TELHDLC Telegramm-Task HDLC
 * 12 TID_BOS_TEL_MUX Mux-Task des Telegramm-Manager
 * 13 TID_BOS_TMR Timer-Task
 * 14 TID_BOS_DIA Diagnose-Task
 * 15 TID_BOS_DIATEL Diag-Telegramm-Task
 * 16 TID_BOS_DIAEDV Diag-Manager-Device
 * 17 TID_BOS_SCSI SCSI-Server-Task
 * 18 TID_BOS_SBUS SBUS-Task
 * 19 TID_BOS_SUPER Supertabellenaufbau
 * 20 TID_BOS_TELCONN Telegramm-Task PB
 * 21 TID_BOS_TELDELPB Telegramm-Task PB
 * 22 TID_BOS_UPDATE Update-Manager
 * 23 TID_BOS_TP Touch-Panel-Server-Task
 * 24 TID_BOS_SBUSAKT SBUS-Task
 * 25 TID_BOS_KEYB Keyboard-Task f�r Slave
 * 26 TID_BOS_KEYB_S Keyboard-Task f�r Slave
 * 27 TID_BOS_SWAP Swap-Manager
 * 28 TID_BOS_SWAP_R Swap-Remote-Task
 * 29 TID_BOS_KBUS KBUS-Task
 * 60 TID_BOS_TELUG Telegramm-Manager-Untergrenze
 * 100 TID_BOS_TELOG Telegramm-Manager-Obergrenze
 * 101 TID_BOS_TCP_UG TCP-Manager-Untergrenze
 * 110 TID_BOS_TCP_OG TCP-Manager-Obergrenze
 * 201 PA_TID_COMIN COM-Schnittstelle
 * 202 PA_TID_COMOUT COM-Schnittstelle
 * 203 PA_TID_MEN Menuetask
 * 204 PA_TID_DRU Druckersteuerung
 * 205 PA_TID_GEW Eichanzeige
 * 206 PA_TID_ABLGEW Gewichtsanforderung von EW-Schnittstelle
 * 207 PA_TID_AUTO Schnittstelle Automat
 * 208 PA_TID_HAUTO Hilfstask Automat 2###
 * 209 PA_TID_VPM Verpackungsmaschine
 * 210 PA_TID_HVPM Hilfstask Verpackungsmaschine
 * 211 PA_TID_ABLAUF Ger�te-Ablaufsteuerung
 * 212 PA_TID_ANZ Anzeigetask
 * 213 PA_TID_ERR Fehlerbehandlung/Aufzeichnung
 * 214 PA_TID_TAST Tastatur-Task
 * 215 PA_TID_TMF zeitgesteuerte Funktionen
 * 216 PA_TID_NET_WDOUT Netzwerk-Sendetask
 * 217 PA_TID_NET_ASCII_IN Ascii-EDV-Empfangstask
 * 218 PA_TID_DB Datenbanktask f�r Net- und Menue Anforderungen
 * 219 PA_TID_REMTAST Tastatur mit REMOTE-Anbindung der Eingabe
 * 220 PA_TID_DIA Dialogtask
 * 221 PA_TID_EWZ_KBUS Empfangstask der Eingabewerkzeuge
 * 222 PA_TID_QS_TAST QS-Tastaturtask
 * 223 PA_TID_MC Memokarte
 * 224 PA_TID_LPT Listendruckertask
 * 225 PA_TID_HAI Empfangstask von Haigis Gewichtswerten
 * 226 PA_TID_BRIDG_FIRST erste Bridgefunktionstask
 * 235 PA_TID_BRIDG_LAST letzte Bridgefunktionstask
 * 236 PA_TID_LPTIN Schnittstellentask f�r Listendrucker
 * 237 PA_TID_LPTOUT Schnittstellentask f�r Listendrucker
 */

/*
 * BIZERBA
 * 
 * Fachbereich: TE-AS
 * Datum: 08.02.96
 * Uhrzeit: 19:19
 * Datenbeschreibung GxNet
 * Programmstand: 4.0
 * 6.561.98.000.05
 * Zeichnungsnummer
 * file: GXNET\DATEN\ERRKEY.DOC
 * Seite: E-42
 */
