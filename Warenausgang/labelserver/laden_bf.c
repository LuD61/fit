

void loadplux ( void )
{
	lockpaz();
	getsend_kungtex(&ggatx) ;
	get_kungx(&ggdat) ;
	sendDBGGDAT( &ggdat) ;
	loadplu( ggdat.plunr, ggdat.kdnr ) ;
	// sumclear (ETI_SUMME_TG); 
	sumclear (ETI_SUMME_1); 
	sumclear (ETI_SUMME_2); 
	sumclear (ETI_SUMME_3); 
	setsumstart();
	setnve();
	getmhd();

	unlockpaz();
}

void sendDBGGDAT( GGDAT_t * ggdat )
{
	us len;
	char *p;
	if (ggdatcmp( ggdat, &ggdat_alt) == 0)
	{
		return;
	}
	dontload = 0;
	dim_prs = ggdat->devise;
	p = tbuf;
	p += formfu( p, DBW_TAB, GGDAT );
	p += formfu( p, GGL_PLUNR, ggdat->plunr );
	p += formfu( p, GGL_KDNR, ggdat->kdnr );
	p += formfu( p, DBW_ATTRDESCRIP, 1 );
	p += formfu( p, GGW_AUSZEICH_ART, ggdat->aart );
	p += formfu( p, GGL_ATXNR1, ggdat->atxnr1 );
	p += formfu( p, GGL_ATXNR2, ggdat->atxnr2 );
	p += formfu( p, GGL_ATXNR3, ggdat->atxnr3 );
	p += formfu( p, GGL_ATXNR4, ggdat->atxnr4 );
	p += formfu( p, GGL_ATXNR5, ggdat->atxnr5 );
	p += formfu( p, GGL_ATXNR6, ggdat->atxnr6 );
	p += formfu( p, GGL_ATXNR7, ggdat->atxnr7 );
	// p += formfu( p, GGL_ATXNR8, ggdat->atxnr8 );
	// p += formfu( p, GGL_ATXNR9, ggdat->atxnr9 );
	if (ggdat->aart != 0 && ggdat->stk_pro_pck > 0)
		p += formfu( p, GGL_ETIKETFOLGE_VORWAHL, ggdat->stk_pro_pck );
	else       
		p += formfu( p, GGL_ETIKETFOLGE_VORWAHL, 0 );
	p += formfu( p, GGL_CABNR1, ggdat->cabnr1 );
	p += formfu( p, GGL_CABNR2, ggdat->cabnr2 );
	p += formfu( p, GGL_LOGONR, ggdat->logo1 );
	p += formfu( p, GGL_LOGONR2, ggdat->logo2 );
	p += formfu( p, GGL_HALTBAR1, ggdat->haltbar1 );
	p += formfu( p, GGD_PRS_GPREIS, dim_prs, ggdat->gpreis );
	p += formfu( p, GGD_PRS_SPREIS, dim_prs, 0 );
	p += formfu( p, GGD_GEW_TARA_EINZEL, DIM_GEW, ggdat->tara );
	if ( &(ggdat->fgew) > 0L )
		p += formfu( p ,GGD_GEW_NETTO_EINZEL, DIM_GEW , ggdat->fgew ) ;
	p += formfu( p, GGL_ETIKETPARNR, ggdat->etin );
	p += formfu( p, GGL_AM_PARNR, ggdat->ampar );
	p += formfu( p, GGL_DTXNR2, ggdat->dtxnr2 );
	p += formfu( p, GGT_CTS1, ggdat->cts1len, ggdat->cts1 ) ;
	p += formfu( p, GGT_CTS2, ggdat->cts2len, ggdat->cts2 ) ;
	// LAND2 nicht aktiviert, LAND nicht im PLU Satz
	// EURO kommt ueber dim_prs GK 30.12.2001
	// p += formfu( p, GGW_LAND_SKND, ggdat->devise2 ) ;
	// p += formfu( p, GGW_LAND, ggdat->devise );
	len = (us)(p - tbuf);
	len = formfu( tbuf, DBV_DATA_VDBT, len, tbuf );
	len = toGX( tbuf, GXWR, len );
	if ( len > 0 )
		fromGX( tbuf, seconds );
	return;
}

void setsumstart( void )
{
	us len;
	char *p;
	// setsums();
	p = tbuf;
	p += formfu( p, GGW_SUM1VORWAHL_ART, 1 );
	p += formfu( p, GGL_STCK_SUM1_VORWAHL, 0 );
	p += formfu( p, GGW_SUM2VORWAHL_ART, 1 );
	p += formfu( p, GGL_STCK_SUM2_VORWAHL, 0 );
	p += formfu( p, GGW_SUM3VORWAHL_ART, 1 );
	p += formfu( p, GGL_STCK_SUM3_VORWAHL, 0 );
	if (ggdat.aart != 0 && ggdat.stk_pro_pck > 0)
	{
		ggdat.stk1vw = 0;
	}
	switch ( ggdat.sum1vwa )
	{      
	case 1:
		p += formfu( p, GGW_SUM1VORWAHL_ART, ggdat.sum1vwa );
		p += formfu( p, GGL_STCK_SUM1_VORWAHL, ggdat.stk1vw );
		break;
	case 2:
		p += formfu( p, GGW_SUM1VORWAHL_ART, ggdat.sum1vwa );
		p += formfu( p, GGD_GEW_SUM1_VORWAHL, DIM_GEW, ggdat.gew1vw );
		break;
	case 4:
		p += formfu( p, GGW_SUM1VORWAHL_ART, ggdat.sum1vwa );
		p += formfu( p, GGD_PRS_SUM1_VORWAHL, dim_prs, ggdat.prs1vw );
		break;
	}
	switch ( ggdat.sum2vwa )
	{      
	case 1:
		p += formfu( p, GGW_SUM2VORWAHL_ART, ggdat.sum2vwa );
		p += formfu( p, GGL_STCK_SUM2_VORWAHL, ggdat.stk2vw );
		break;
	case 2:
		p += formfu( p, GGW_SUM2VORWAHL_ART, ggdat.sum2vwa );
		p += formfu( p, GGD_GEW_SUM2_VORWAHL, DIM_GEW, ggdat.gew2vw );
		break;
	case 4:
		p += formfu( p, GGW_SUM2VORWAHL_ART, ggdat.sum2vwa );
		p += formfu( p, GGD_PRS_SUM2_VORWAHL, dim_prs, ggdat.prs2vw );
		break;
	case 8:
		p += formfu( p, GGW_SUM2VORWAHL_ART, ggdat.sum2vwa );
		p += formfu( p, GGL_STCK_SUM2_VORWAHL, ggdat.geb2vw );
		break;
	}
	switch ( ggdat.sum3vwa )
	{      
	case 1:
		p += formfu( p, GGW_SUM3VORWAHL_ART, ggdat.sum3vwa );
		if (ggdat.stk_pro_pck > 0)
		{
			/*
			* Bei Stuecklistenartikeln mit Festgewicht:
			* in einem Hieb alle Etiketten raus
			*
			*/
			p += formfu( p, GGL_STCK_SUM3_VORWAHL, 0 );
			p += formfu( p, GGL_ETIKETFOLGE_VORWAHL, ggdat.stk_pro_pck );
		}
		else
		{
			p += formfu( p, GGL_STCK_SUM3_VORWAHL, ggdat.stk3vw );
			p += formfu( p, GGL_ETIKETFOLGE_VORWAHL, 0 );
		}
		break;
	case 2:
		p += formfu( p, GGW_SUM3VORWAHL_ART, ggdat.sum3vwa );
		p += formfu( p, GGD_GEW_SUM3_VORWAHL, DIM_GEW, ggdat.gew3vw );
		break;
	case 4:
		p += formfu( p, GGW_SUM3VORWAHL_ART, ggdat.sum3vwa );
		p += formfu( p, GGD_PRS_SUM3_VORWAHL, dim_prs, ggdat.prs3vw );
		break;
	case 8:
		p += formfu( p, GGW_SUM3VORWAHL_ART, ggdat.sum3vwa );
		p += formfu( p, GGL_STCK_SUM3_VORWAHL, ggdat.geb3vw );
		break;
	case 16:
		p += formfu( p, GGW_SUM3VORWAHL_ART, ggdat.sum3vwa );
		p += formfu( p, GGL_STCK_SUM3_VORWAHL, ggdat.pal3vw );
		break;
	}
	if ( strcmp( itmtxt("STOPSUM2"), "EGALSUM" ) == 0 )
	{
		p += formfu( p, GGW_STOP_SUM2, EGALSUM );
	}
	if ( strcmp( itmtxt("STOPSUM2"), "STOPSUM" ) == 0 )
	{
		p += formfu( p, GGW_STOP_SUM2, STOPSUM );
	}
	if ( strcmp( itmtxt("STOPSUM3"), "EGALSUM" ) == 0 )
	{
		p += formfu( p, GGW_STOP_SUM3, EGALSUM );
	}
	if ( strcmp( itmtxt("STOPSUM3"), "STOPSUM" ) == 0 )
	{
		p += formfu( p, GGW_STOP_SUM3, STOPSUM );
	}
	// p += formfu( p, GGW_SUM_START, SUMSTART_EXPLIZIT );
	// p += formfu( p, GGW_ETIKETDRUCK_START, DRUCK_INTERN );
	p += formfu( p, GGW_PLUW_TARA, 1 );
	p += formfu( p, GGW_AUSZEICHART_WECHS, 0x02 );
	p += formfu( p, GGL_NR_KDETIK_SUM1, ggdat.ets1 );
	p += formfu( p, GGL_NR_KDETIK_SUM2, ggdat.ets2 );
	// p += formfu( p, GGL_NR_KDETIK_SUM3, ggdat.ets3 ); 20040125 GK
	p += formfu( p, GGW_SONDETIKET_KRITERIUM, ggdat.sonder_eti_krit );
	p += formfu( p, GGW_SONDETIKET_WDKANAL, ggdat.sonder_eti_kanal );
	p += formfu( p, GGW_PREIS_BERECHNUNG, ggdat.prbe );
	len = (us)(p - tbuf);
	len = formfu( tbuf, LGV_SEQUENZ, len, tbuf );
	len = toGX( tbuf, GXWR, len );
	fromGX( tbuf, seconds );
	return;
}

void setnve( void )
{
	/* NVE-Layouts, NVE-Cabs an NVE-Drucker SB=3 20040125 */
	/* Achtung: hier werden die LOGO-Nummern missbraucht */

	us len;
	char *p;

	p = tbuf;
	p += formfu( p, GGL_NR_KDETIK_SUM1, ggdat.logo1 );
	p += formfu( p, GGL_NR_KDETIK_SUM2, ggdat.logo2 );
	len = (us)(p - tbuf);
	len = formfu( tbuf, LGV_SEQUENZ, len, tbuf );
	len = toGXSB( tbuf, 3, GXWR, len );
	fromGX( tbuf, seconds );

	p = tbuf;
	p += formfu( p, GGL_CABNR1, ggdat.cabnr3 );
	p += formfu( p, GGL_CABNR2, ggdat.cabnr4 );
	len = (us)(p - tbuf);
	if ( len > 0 )
	{
		len = formfu( tbuf, XCV_DBTAB_DATASET, len, tbuf );
		toGXSB( tbuf, 3, GXWR, len );
		fromGX( tbuf, seconds );
	}
	return;
}

void getmhd( void )
{
	us len;
	len = formfu( tbuf, GGL_DATUM2, 0 );
	len = toGX( tbuf, GXRD, len );
	fromGX( tbuf, seconds );
	fprintf( fperr, "MHD: %ld\n", ggdat.datum2 ); 
	return;     
}

void loadplu ( long plunr, long kdnr )
{
	us len;
	char *p;
	if (dontload == 1)
		return;
	quit_ok = 0;
	p = tbuf;
	p += formfu( p, GGL_PLUNR, plunr );
	p += formfu( p, GGL_KDNR, kdnr );
	len = (us)(p - tbuf);
	len = formfu( tbuf, XCV_DBTAB_DATASET, len, tbuf );
	len = toGX( tbuf, GXWR, len );
	fromGX( tbuf, seconds );
	dontload = 1;
	memcpy( &ggdat_alt, &ggdat, sizeof ggdat );
	return;
}

