/* 
 *  hexasc.c
 *  Guenter Koenig
 *  26.10.1994
 */

#include "stdafx.h"

#include "hexasc.h"
#include <stdio.h>

extern FILE *fperr;
extern int testmode;

us a2x( char *x, char *a, us alen )
{
    us i, k;
    int c;
    for ( i = 0, k = 0;  i < alen; )
    {
        c = ((a[i] >> 4) & 0xF);   
        c = (c < 0xA) ? (c + '0') : (c - 0xA + 'A');
        x[k++] = (char) c;
        c = a[i++] & 0xF; 
        c = (c < 0xA) ? (c + '0') : (c - 0xA + 'A');
        x[k++] = (char) c;
    }
    x[k] = '\0';
    // if (testmode > 2) fprintf(fperr,"a2x: xlen: %d  x:\n%s\n",k,x);
    return k;
}

us x2a( char *a, char *x, us xlen )
{
    us i, k;
    int c;
    for ( i = 0, k = 0; k < xlen; )
    {
        c = x[k++]; 
        c = (c < 'A') ? (c - '0') : (c + 0xA - 'A'); 
        a[i] = (char) (c << 4);
        c = x[k++]; 
        c = (c < 'A') ? (c - '0') : (c + 0xA - 'A'); 
        a[i++] += (char) c;
    }
    x[k] = '\0';
    // if (testmode > 2) fprintf(fperr,"x2a: xlen: %d  x:\n%s\n",k,x);
    return i;
}
