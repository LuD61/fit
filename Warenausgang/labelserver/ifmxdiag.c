#include <sqlhdr.h>
#include <sqliapi.h>
#line 1 "ifmxdiag.ec"
#include "ifmxdiag.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern FILE *fperr;

/* 
 * EXEC SQL include sqlproto.h;
 */
#line 8 "ifmxdiag.ec"

#line 8 "ifmxdiag.ec"
#line 1 "d:/informix/incl/esql/sqlproto.h"
/****************************************************************************
 *
 *                               IBM INC.
 *
 *                           PROPRIETARY DATA
 *
 * Licensed Material - Property Of IBM
 *
 * "Restricted Materails of IBM"
 *
 * IBM Informix Client SDK
 *
 * (c)  Copyright IBM Corporation 2002. All rights reserved.
 *
 *  Title:	sqlproto.h
 *  Sccsid:     @(#)sqlproto.h
 *  Description:
 *		Prototypes of user functions exported by ISQLIxxx.DLL
 *
 ***************************************************************************
 */

#ifndef _SQLPROTO
#define _SQLPROTO
#ifdef __cplusplus
extern "C" {
#endif

int __cdecl bycmpr(char *s1,char *s2,int cnt);
void __cdecl bycopy(char *s1,char *s2,int n);
void __cdecl byfill(char *s,int n,char c);
int __cdecl byleng(char *beg,int cnt);
int __cdecl decadd(struct decimal *n1,struct decimal *n2,struct decimal *n3);
int __cdecl deccmp(struct decimal *n1,struct decimal *n2);
void __cdecl deccopy(struct decimal *n1,struct decimal *n2);
int __cdecl deccvasc(char *cp,int len,struct decimal *np);
int __cdecl deccvdbl(double dbl,struct decimal *np);
int __cdecl deccvint(int in,struct decimal *np);
int __cdecl deccvlong(long lng,struct decimal *np);
int __cdecl decdiv(struct decimal *n1,struct decimal *n2,struct decimal *n3);
char * __cdecl dececvt(struct decimal *np,int ndigit,int *decpt,int *sign);
char * __cdecl decfcvt(struct decimal *np,int ndigit,int *decpt,int *sign);
int __cdecl decmul(struct decimal *n1,struct decimal *n2,struct decimal *n3);
void __cdecl decround(struct decimal *np,int dec_round);
int __cdecl decsub(struct decimal *n1,struct decimal *n2,struct decimal *n3);
int __cdecl dectoasc(struct decimal *np,char *cp,int len,int right);
int __cdecl dectodbl(struct decimal *np,double *dblp);
int __cdecl dectoint(struct decimal *np,int *ip);
int __cdecl dectolong(struct decimal *np,long *lngp);
void __cdecl dectrunc(struct decimal *np,int trunc);
void __cdecl dtcurrent(struct dtime *d);
int __cdecl dtcvasc(char *str,struct dtime *d);
int __cdecl ifx_dtcvasc(char *str,struct dtime *d, char db_century);
int __cdecl dtcvfmtasc(char *input,char *fmt,struct dtime *d);
int __cdecl ifx_dtcvfmtasc(char *input,char *fmt,struct dtime *d, char db_century);
int __cdecl dtextend(struct dtime *id,struct dtime *od);
int __cdecl dttoasc(struct dtime *d,char *str);
int __cdecl dttofmtasc(struct dtime *d,char *output,int str_len,char *fmtstr);
int __cdecl ifx_dttofmtasc(struct dtime *d,char *output,int str_len,char *fmtstr, char db_century);
int __cdecl incvasc(char *str,struct intrvl *i);
int __cdecl incvfmtasc(char *input,char *fmt,struct intrvl *intvl);
int __cdecl intoasc(struct intrvl *i,char *str);
int __cdecl intofmtasc(struct intrvl *i,char *output,int str_len,char *fmtstr);
void __cdecl ldchar(char *from,int count,char *to);
int __cdecl rdatestr(long jdate,char *str);
int __cdecl rdayofweek(long date);
int __cdecl rdefmtdate(long *pdate,char *fmtstring,char *input);
int __cdecl ifx_defmtdate(long *pdate,char *fmtstring,char *input, char db_century);
void __cdecl rdownshift(char *s);
int __cdecl rfmtdate(long date,char *fmtstring,char *result);
int __cdecl rfmtdec(struct decimal *dec,char *format,char *outbuf);
int __cdecl rfmtdouble(double dvalue,char *format,char *outbuf);
int __cdecl rfmtlong(long lvalue,char *format,char *outbuf);
int __cdecl rget_lmsg(long msgnum,char *s,int maxsize,int *msg_length);
int __cdecl rgetlmsg(long msgnum,char *s,int maxsize,int *msg_length);
int __cdecl rgetmsg(int msgnum,char *s,int maxsize);
int __cdecl risnull(int vtype,char *pcvar);
int __cdecl rjulmdy(long jdate,short *mdy);
int __cdecl rleapyear(int year);
int __cdecl rmdyjul(short *mdy,long *jdate);
int __cdecl rsetnull(int vtype,char *pcvar);
int __cdecl rstrdate(char *str,long *jdate);
int __cdecl ifx_strdate(char *str,long *jdate, char db_century);
void __cdecl rtoday(long *today);
int __cdecl rtypalign(int offset,int type);
int __cdecl rtypmsize(int type,int len);
char * __cdecl rtypname(int type);
int __cdecl rtypwidth(int type,int len);
void __cdecl rupshift(char *s);
int __cdecl sqlexit(void);
void __cdecl stcat(char *src,char *dst);
void __cdecl stchar(char *from,char *to,int count);
int __cdecl stcmpr(char *s1,char *s2);
void __cdecl stcopy(char *src,char *dst);
int __cdecl stleng(char *src);
int __cdecl rstod(char *str,double *val);
int __cdecl rstoi(char *s,int *val);
int __cdecl rstol(char *s,long *val);
int __cdecl sqgetdbs(int *ret_fcnt,char **fnames ,int fnsize,char *farea,int fasize);
int __cdecl sqlbreak(void);
int __cdecl sqlbreakcallback(long timeout,void (*callback)(int));
int __cdecl sqldetach(void);
int __cdecl sqldone(void);
int __cdecl sqlstart(void);
int __cdecl dtaddinv(struct dtime *d,struct intrvl *i,struct dtime *r);
int __cdecl dtsub(struct dtime *d1,struct dtime *d2,struct intrvl *i);
int __cdecl dtsubinv(struct dtime *d,struct intrvl *i,struct dtime *r);
int __cdecl invdivdbl(struct intrvl *iv,double dbl,struct intrvl *ov);
int __cdecl invdivinv(struct intrvl *i1,struct intrvl *i2,double *res);
int __cdecl invextend(struct intrvl *i,struct intrvl *o);
int __cdecl invmuldbl(struct intrvl *iv,double dbl,struct intrvl *ov);
int __cdecl ifx_to_gl_datetime(char *source_str, char *target_str, int maxlen);
void * __cdecl GetConnect ( void );
void * __cdecl SetConnect ( void *NewConInfo );
void * __cdecl ReleaseConnect ( void *NewConInfo );
int __cdecl deccvflt(double source, struct decimal *destination);
int __cdecl dectoflt(struct decimal *source, float *destination);
char * __cdecl ifx_getenv(char *env_name);
int __cdecl ifx_putenv(const char *env_name);
int __cdecl _infxcexp(int RtnNum, void * RtnAddr, int OptParm);
void __cdecl SqlFreeMem( void * MemAddr, int FreeType );
char * __cdecl ifx_getcur_conn_name(void);

int __cdecl ifx_int8cmp(struct ifx_int8 *op1, struct ifx_int8 *op2);
int __cdecl ifx_int8cvlong(long lng, struct ifx_int8 *int8p);
int __cdecl ifx_int8tolong(struct ifx_int8 *int8p, long *lngp);
int __cdecl ifx_int8cvint(int in, struct ifx_int8 *int8p);
int __cdecl ifx_int8toint(struct ifx_int8 *int8p, int *intp);
int __cdecl ifx_int8toint2(struct ifx_int8 *int8p,short *int2p);
int __cdecl ifx_int8toint4(struct ifx_int8 *int8p,long *int4p);
int __cdecl ifx_int8cvasc(char *cp, int len, struct ifx_int8 *int8p);
int __cdecl ifx_int8toasc(struct ifx_int8 *int8p, char *cp, int len);
int __cdecl ifx_int8cvdec(struct decimal *decp, struct ifx_int8 *int8p);
int __cdecl ifx_int8todec(struct ifx_int8 *int8p, struct decimal *decp);
int __cdecl ifx_int8cvdbl(double dbl, struct ifx_int8 *int8p);
int __cdecl ifx_int8todbl(struct ifx_int8 *int8p, double *dblp);
int __cdecl ifx_int8cvflt(double flt, struct ifx_int8 *int8p);
int __cdecl ifx_int8toflt(struct ifx_int8 *int8p, float *fltp);
int __cdecl deccvint8(struct ifx_int8 *int8p, struct decimal *decp);
int __cdecl ifx_int8sub(struct ifx_int8 *int8op1, struct ifx_int8 *int8op2, struct ifx_int8 *int8result);
int __cdecl ifx_int8add(struct ifx_int8 *int8op1, struct ifx_int8 *int8op2, struct ifx_int8 *int8result);
int __cdecl ifx_int8mul(struct ifx_int8 *int8op1, struct ifx_int8 *int8op2, struct ifx_int8 *int8result);
int __cdecl ifx_int8div(struct ifx_int8 *int8op1, struct ifx_int8 *int8op2, struct ifx_int8 *int8result);
void __cdecl ifx_int8copy(struct ifx_int8 *sint8p, struct ifx_int8 *dint8p);
void __cdecl ifx_getserial8(struct ifx_int8 *int8p);

int __cdecl ifx_cl_dealloc(struct _ifx_collection_struct **collection);
long __cdecl ifx_get_row_xid(struct _ifx_collection_struct *collp, int *colnum);
char * __cdecl ifx_get_row_extended_name(struct _ifx_collection_struct *collp);
char * __cdecl ifx_get_msg_param(void);

int __cdecl ifx_var_flag(void **variable, short alloc_flag);
int __cdecl ifx_var_alloc(void **variable, long size);
int __cdecl ifx_var_dealloc(void **variable);
int __cdecl ifx_var_setdata(void **variable, char *data, long size);
int __cdecl ifx_var_isnull(void **variable);
int __cdecl ifx_var_setnull(void **variable, int flag);
int __cdecl ifx_var_setlen(void **variable, long size);
void * __cdecl ifx_var_getdata(void **variable);
int __cdecl ifx_var_init(void **variable);
int __cdecl ifx_var_getlen(void **variable);
int __cdecl ifx_lvar_alloc(int alloc);

/*
 * Set of ifx_lo_* accessors for opaque data types:
 *	- ifx_lo_create_spec_t
 *	- ifx_lo_stat_t
 *
 * See also: locator.h for description of possible values for arguments
 */

/* smartblob create spec SET accessors: */
int __cdecl ifx_lo_specset_flags(ifx_lo_create_spec_t *cspec, int flags);
int __cdecl ifx_lo_specset_def_open_flags(ifx_lo_create_spec_t *cspec, int def_open_flags);
int __cdecl ifx_lo_specset_estbytes(ifx_lo_create_spec_t *cspec, ifx_int8_t *size);
int __cdecl ifx_lo_specset_maxbytes(ifx_lo_create_spec_t *cspec, ifx_int8_t *size);
int __cdecl ifx_lo_specset_extsz(ifx_lo_create_spec_t *cspec, int n);
int __cdecl ifx_lo_specset_sbspace(ifx_lo_create_spec_t *cspec, const char *str);

/* smartblob create spec GET accessors: */
int __cdecl ifx_lo_specget_flags(ifx_lo_create_spec_t *cspec);
int __cdecl ifx_lo_specget_def_open_flags(ifx_lo_create_spec_t *cspec);
int __cdecl ifx_lo_specget_estbytes(ifx_lo_create_spec_t *cspec, ifx_int8_t *size);
int __cdecl ifx_lo_specget_maxbytes(ifx_lo_create_spec_t *cspec, ifx_int8_t *size);
int __cdecl ifx_lo_specget_extsz(ifx_lo_create_spec_t *cspec);
int __cdecl ifx_lo_specget_sbspace(ifx_lo_create_spec_t *cspec, char *str, int len);

/* smartblob stat GET accessors: */
int __cdecl ifx_lo_stat_size(ifx_lo_stat_t *lostat, ifx_int8_t *size);
int __cdecl ifx_lo_stat_uid(ifx_lo_stat_t *lostat);
int __cdecl ifx_lo_stat_atime(ifx_lo_stat_t *lostat);
int __cdecl ifx_lo_stat_mtime_sec(ifx_lo_stat_t *lostat);
int __cdecl ifx_lo_stat_mtime_usec(ifx_lo_stat_t *lostat);
int __cdecl ifx_lo_stat_ctime(ifx_lo_stat_t *lostat);
int __cdecl ifx_lo_stat_refcnt(ifx_lo_stat_t *lostat);
ifx_lo_create_spec_t * __cdecl ifx_lo_stat_cspec(ifx_lo_stat_t *lostat);

/* smartblob spec and stat destructors: */
int __cdecl ifx_lo_spec_free(ifx_lo_create_spec_t *cspec);
int __cdecl ifx_lo_stat_free(ifx_lo_stat_t *lostat);
/*
 * Set of ifx_lo_ functions for support of Large Objects
 */
int __cdecl ifx_lo_col_info(char *column_name,ifx_lo_create_spec_t *create_spec);
int __cdecl ifx_lo_def_create_spec(ifx_lo_create_spec_t **cspec);
int __cdecl ifx_lo_create(ifx_lo_create_spec_t *create_spec, int flags, ifx_lo_t *loptr, int *error);
int __cdecl ifx_lo_open(ifx_lo_t *loptr, int flags, int *error);
int __cdecl ifx_lo_close(int lofd);
int __cdecl ifx_lo_seek(int lofd, ifx_int8_t *off, int whence, ifx_int8_t *seek_pos);
int __cdecl ifx_lo_tell(int lofd, ifx_int8_t *seek_pos);
int __cdecl ifx_lo_truncate(int lofd, ifx_int8_t *off);
int __cdecl ifx_lo_filename(ifx_lo_t *loptr, char *fname, char *result, int result_buffer_nbytes);
int __cdecl ifx_lo_alter(ifx_lo_t *loptr, ifx_lo_create_spec_t *create_spec);
int __cdecl ifx_lo_stat(int lofd, ifx_lo_stat_t **lostat);
int __cdecl ifx_lo_read(int lofd, char *buf, int nbytes, int *error);
int __cdecl ifx_lo_readwithseek(int lofd, char *buf, int nbytes, ifx_int8_t *off, int whence, int *error);
int __cdecl ifx_lo_write(int lofd, char *buf, int nbytes, int *error);
int __cdecl ifx_lo_writewithseek(int lofd, char *buf, int nbytes, ifx_int8_t *off, int whence, int *error);
int __cdecl ifx_lo_copy_to_lo(int lofd, char *fname, int flags);
int __cdecl ifx_lo_copy_to_file(ifx_lo_t *loptr, char *fname, int flags, char *result);
int __cdecl ifx_lo_release(ifx_lo_t *loptr);
int __cdecl ifx_lo_to_buffer(struct ifx_lo_ts *lo_handle,int size_limit,char **buffer ,int *error);
int __cdecl ifx_lo_from_buffer(struct ifx_lo_ts *lo_handle,int size,char *buffer,int *error);
int __cdecl ifx_lo_lock(int lofd, ifx_int8_t *off, int whence, ifx_int8_t *range, int lockmode);
int __cdecl ifx_lo_unlock(int lofd, ifx_int8_t *off, int whence, ifx_int8_t *range);

int __cdecl ifx_xactevent(void *);
int __cdecl ifx_getserowner(char *serowner);

void *__cdecl ifx_alloc_conn_user(char *username,char *passwd);
void __cdecl ifx_free_conn_user(struct ifx_connect_struct **conn);
void *__cdecl ifx_alloc_conn_cred(void *cred);
void __cdecl ifx_free_conn_cred(struct ifx_connect_struct **conn);
int __cdecl ifx_isius(void);
int __cdecl ifx_cl_card(struct _ifx_collection_struct *colt, int *isnull);
char * __cdecl	sqli_server_version(void);
int    __cdecl	ldcollection(char *cp, ifx_collection_t *collp, mint readsize,
								mint longidver);
void   __cdecl	stcollection(ifx_collection_t *collp, char *start, mint longidver);

#ifdef __cplusplus
}
#endif

#endif /* !_SQLPROTO */
#line 245 "d:/informix/incl/esql/sqlproto.h"
/* 
 * EXEC SQL include sqlhdr.h;
 */
#line 9 "ifmxdiag.ec"

#line 9 "ifmxdiag.ec"
#line 1 "d:/informix/incl/esql/sqlhdr.h"
/****************************************************************************
 *
 *                               IBM INC.
 *
 *                           PROPRIETARY DATA
 *
 * Licensed Material - Property Of IBM
 *
 * "Restricted Materails of IBM"
 *
 * IBM Informix Client SDK
 *
 * (c)  Copyright IBM Corporation 2002. All rights reserved.
 *
 *  Title:	sqlhdr.h
 *  Sccsid:     @(#)sqlhdr.h	9.28     1/31/94  13:31:16
 *  Description:
 *		header file for all embedded sql programs
 *
 ***************************************************************************
 */

#ifndef _SQLHDR
#define _SQLHDR

#ifdef __cplusplus
extern "C" {
#endif

#include "ifxtypes.h"

#include "sqlda.h"
#include "sqlca.h"
#include "collct.h"
#include "int8.h"
#include "locator.h"
#include "datetime.h"
#include "value.h"
#include "sqlproto.h"

#ifdef __BORLANDC__
#pragma option -a8
#else
#pragma pack (8)
#endif

#if !defined(__STDC__) && !defined(__cplusplus)
#define const
#endif

typedef struct _ifx_squlist
    {
    char **_SQUulist;			/* columns to be updated */
    struct _squlist *_SQUnext;	/* next update list */
    mint _SQUid;					/* update list id */
    } ifx_squlist_t;

typedef struct _ifx_cursor
    {
    struct _ifx_cursor  *_SQCcursptr;   /* pointer to the statement/cursor
                                         * block.
                                         */
    int2 _SQCstmttype;			/* SQ_SELECT, SQ_INSERT, etc. */
    int2 _SQCsqlid;			/* SQL's id for this cursor */
    int2 _SQCnfields;			/* number of result fields
					 * (number supplied by SQL)
					 */
    int2 _SQCnibind;			/* number of input args */
    int2 _SQCnobind;			/* number of output args */
    int2 _SQCnrettype;			/* number of fields sent for the
					 * rettype msg
					 */
    int2 _SQCtcount;			/* tuples remaining in buffer */
    int2 _SQCtsize;			/* length of data expected from
					 * SQL
					 */
    int4 _SQCtbsize;			/* tuple buffer size */
    int4 _SQCflags;			/* CROPEN, CREOF, CRSINGLE ,CRPREPARE*/
		/* used for scroll cursors */
    int4 _SQCfirst;			/* rowid of 1st tuple in buffer */
    int4 _SQClast;			/* rowid of last tuple in buffer */

    ifx_sqlvar_t *_SQCibind;	/* pointer to first in array of
					 * binding structures for arguments
					 * to be taken from the user
					 * program and sent to SQL;
					 */
    ifx_sqlvar_t *_SQCobind;	/* pointer to first in array of
					 * binding structures for values
					 * to be received from SQL and
					 * supplied to the user program;
					 */
    ifx_sqlvar_t *_SQCrettype;	/* pointer to first in array of
					 * binding structure for values
					 * to be sent thru the rettype
					 * msg
					 */
    ifx_sqlvar_t *_SQCfields;	/* pointer to first in array of
					 * structures describing the data
					 * to be received from SQL;
					 * (fields described by SQL)
					 */
    ifx_sqlda_t  *_SQCsqlda;	/* pointer to sqlda */
    char **_SQCcommand;			/* pointer to ptrs to pieces of
					 * the command
					 */
    char *_SQCstrtab;			/* pointer to table of strings - the
					 * names of the attributes to be
					 * received from SQL
					 * (table supplied by SQL)
					 */
    char *_SQCtbuf;				/* tuple buffer */
    char *_SQCtuple;			/* pointer to current tuple within
					 * buffer
					 */
    char *_SQCname;				/* cursor name */

    char *_SQCapiname;			/* column name buffer when */
					/* DBAPICODE is set */
    mint _SQCposition;			/* Used only for INSERT AT stmts */
    mint _SQCiscollstmt;			/* Set if cursor or stmt is for  */
					/* collection table */
    mint _SQCcl_putcnt;			/* Number of variable length rows
					 * inserted using an insert cursor.
					 */
    ifx_collection_t *_SQCcollection;	/* pointer to collection var    */
    ifx_coll_cur_t _SQCcollcur;         /* collection manager cursor block */
    ifx_cl_tupinfo_t *_SQCcl_tupleinfo; /* collection tuple information */
    ifx_literal_t *_SQClitvalues;	/* pointer to the input values
					 * specified for the collection table */
    ifx_literal_t *_SQC_savlitvalues;	/* pointer to the saved
					 * literal values that have
					 * the QMARKBIND entries in
					 * it */
    ifx_namelist_t *_SQCnamelist;	/* Pointer to column names
					 * specified in the projection
					 * list for SELECT, UPDATE, INSERT
					 * and DELETE			*/
    int2 _SQCcl_num; 			/* number of collection columns */
	/* the following 4 fields are for open-fetch-close optimization */
    int2 _SQCopen_state;               /* see open_state below		*/
    char *_SQCdesc_name;	        /* saved sql desc name 		*/
    ifx_sqlda_t *_SQCidesc;            /* saved idesc value 		*/
    int2 _SQCreopt;			/* saved reoptimization flag 	*/
    void *_SQCcls;			/* reserved for fast path */
    void *_SQCcur7;			/* points to the corresponding
					 * 7.x cursor structure		*/
    int4 _SQCflags3;			/* reserved flag for future use */
    void *_SQCreserved;			/* reserved for future use */
    } ifx_cursor_t;


typedef struct _ifx_statement
    {
    int2 _SQSstmttype;			/* SQ_SELECT, SQ_INSERT, etc. */
    int2 _SQSsqlid;			/* SQL's id for this cursor */
    void  *_SQSreserved;		/* reserved for future use */
    } ifx_statement_t;

typedef struct _ifx_hostvar {
    char *hostaddr;    			/* address of host variable */
    int2 fieldtype;			/* field entry requested by GET */
    int2 hosttype;			/* host type */
    int4 hostlen;			/* length of field type */
    int2 qualifier;			/* qualifier for DATETIME/INTERVAL */
    char *btname;                       /* Base type name if provided in */
                                        /* host variable declaration */
    char *btownername;                  /* Base type owner name */
    char *cstr;                         /* Collection or row host string */
    void *reserved;			/* reserved for future use */
    } ifx_hostvar_t;

/*
 * CURSOR state when (OPTOFC) Open-Fetch-Close Optimization is being used
 */
#define CURSOR_NOT_OPEN		0
#define CURSOR_USER_OPEN	1
#define CURSOR_OPEN_FETCH	2
#define CURSOR_FETCHING		3

/*
 * SQL field type codes
 */
#define XSQLD 		0
#define XSQLTYPE 	1
#define XSQLLEN 	2
#define XSQLPRECISION	3
#define XSQLNULLABLE	4
#define XSQLIND		5
#define XSQLDATA	6
#define XSQLNAME	7
#define XSQLSCALE	8
#define XSQLILEN	9
#define XSQLITYPE	10
#define XSQLIDATA	11
#define XSQLXID         12
#define XSQLTYPENAME    13
#define XSQLTYPELEN     14
#define XSQLOWNERLEN    15
#define XSQLOWNERNAME   16
#define XSQLSOURCETYPE	17
#define XSQLSOURCEID	18


/*
 * Specifications for FETCH
 */
typedef struct _fetchspec
    {
    int4 fval;			/* scroll quantity */
    mint fdir;			/* direction of FETCH (NEXT, PREVIOUS..) */
    mint findchk;		/* check for indicator? */
    } _FetchSpec;

/*
 * Connection type
 */

#define	IFX_CONN_TYPE_NO_AUTH	0x0
#define	IFX_CONN_TYPE_USER_AUTH	0x1
#define	IFX_CONN_TYPE_CRED_AUTH	0x2

/*
 * User connection structure
 */
typedef struct ifx_user_struct
    {
    char	*name;
    char	*passwd;
    } ifx_user_t;

/*
 * ASF connection structure
 */
typedef struct ifx_connect_struct
    {
    int2	conn_type;		/* connection type */
    void	*conn_cred_hdl;		/* connection credential handle */
					/* could be pointer to ifx_user_t */
					/* or any CSS supported credentials */
    } ifx_conn_t;

/*
 * Types stored in csblock_t (iqutil.c)
 */
#define IQ_CURSOR	   0x0000
#define IQ_STMT		   0x0001
#define IQ_ALL		   0x0002
#define IQ_SKIP_CUR_CHK	   0x0100
#define IQ_SKIP_DOWNSHIFT  0x0200

/*
 * The following defines will be used to check the installed version of
 * the libraries against the ones used during compilation. A handshaking
 * method was introduced with shared libraries to indicate if the
 * API changed between one release of the library to the other.
 */
#define IFX_GENLIB_ID  1    /* identifier for libgen.so */
#define IFX_OSLIB_ID   2    /* identifier for libos.so  */
#define IFX_SQLILIB_ID 3    /* identifier for libsql.so */
#define IFX_GLSLIB_ID  4    /* identifier for libgls.so */

/*
 * provide macro definition for the library versions
 * being used while generating client's application executable.
 */
#define CLIENT_GEN_VER		710	/* libgen.so used for compiling application */
#define CLIENT_OS_VER		710	/* libos.so used for compiling application  */
#define CLIENT_SQLI_VER		720	/* libsql.so used for compiling application */
#define CLIENT_GLS_VER		710	/* libgls.so used for compiling application */

#ifdef IFX_THREAD

/* defines for dynamic thread functions */
#define TH_ONCE                 0
#define TH_MUTEXATTR_CREATE     1
#define TH_MUTEXATTR_SETKIND    2
#define TH_MUTEXATTR_DELETE     3
#define TH_MUTEX_INIT           4
#define TH_MUTEX_DESTROY        5
#define TH_MUTEX_LOCK           6
#define TH_MUTEX_UNLOCK         7
#define TH_MUTEX_TRYLOCK        8
#define TH_CONDATTR_CREATE      9
#define TH_CONDATTR_DELETE      10
#define TH_COND_INIT            11
#define TH_COND_DESTROY         12
#define TH_COND_TIMEDWAIT       13
#define TH_KEYCREATE            14
#define TH_GETSPECIFIC          15
#define TH_SETSPECIFIC          16
#define TH_SELF                 17

/* Number of dynamic thread functions */
#define TH_MAXIMUM              18

mint ifxOS_set_thrfunc(mint func, mulong (*funcptr)());

#endif  /* IFX_THREAD */

/* defines for SqlFreeMem, FreeType */
#define CURSOR_FREE 1
#define STRING_FREE 2
#define SQLDA_FREE  3
#define CONN_FREE   4
#define LOC_BUFFER_FREE 5


/*
 * This global variable FetBufSize (Fetch Buffer Size) will allow
 * the application to over-ride cursor->_SQCtbsize which dictates the
 * size of the buffer that holds the data that the BE will fill.
 */

int4   *(fnsqlcode());
char   (*(fnsqlstate()))[6];
struct sqlca_s *(fnsqlca());
struct LoginInfoStructTag *(fninetlogin());
int2 *(fnFetArrSize());
int2 *(fnFetBufSize());
int2 *(fnOptMsg());
int    *(fndbfltprec());

/* Functions that replaces global errors and status */
#define SQLCODE    (*(fnsqlcode()))
#define SQLSTATE   (*(fnsqlstate()))
#define sqlca      (*(fnsqlca()))
#define InetLogin  (*(fninetlogin()))
#define FetArrSize (*(fnFetArrSize()))
#define FetBufSize (*(fnFetBufSize()))
#define OptMsg     (*(fnOptMsg()))
#define dbfltprec  (*(fndbfltprec()))

#ifdef __BORLANDC__
#define SetConnect 		setconnect
#define GetConnect 		getconnect
#define ReleaseConnect 	releaseconnect
#define Mondecm 		mondecm
#define Monsep 			monsep
#define SqlFreeMem  	sqlfreemem
#define ASF_Call 		asf_call
#endif

#ifdef __BORLANDC__
#pragma option -a-
#else
#pragma pack ()
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _SQLHDR */
#line 354 "d:/informix/incl/esql/sqlhdr.h"
/* 
 * EXEC SQL include sqlca.h;
 */
#line 10 "ifmxdiag.ec"

#line 10 "ifmxdiag.ec"
#line 1 "d:/informix/incl/esql/sqlca.h"
/****************************************************************************
 *
 *                               IBM INC.
 *
 *                           PROPRIETARY DATA
 *
 * Licensed Material - Property Of IBM
 *
 * "Restricted Materails of IBM"
 *
 * IBM Informix Client SDK
 *
 * (c)  Copyright IBM Corporation 2002. All rights reserved.
 *
 *  Title:	sqlca.h
 *  Sccsid:	@(#)sqlca.h	9.4	1/18/93  11:09:48
 *  Description:
 *		SQL Control Area
 *
 ***************************************************************************
 */

#ifndef SQLCA_INCL
#define SQLCA_INCL

#ifdef __BORLANDC__
#pragma option -a8
#else
#pragma pack (8)
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include "ifxtypes.h"

typedef struct sqlca_s
{
    int4 sqlcode;
    char sqlerrm[72]; /* error message parameters */
    char sqlerrp[8];
    int4 sqlerrd[6];
		    /* 0 - estimated number of rows returned */
		    /* 1 - serial value after insert or  ISAM error code */
		    /* 2 - number of rows processed */
		    /* 3 - estimated cost */
		    /* 4 - offset of the error into the SQL statement */
		    /* 5 - rowid after insert  */
#ifdef _FGL_
    char sqlawarn[8];
#else
    struct sqlcaw_s
	{
	char sqlwarn0; /* = W if any of sqlwarn[1-7] = W */
	char sqlwarn1; /* = W if any truncation occurred or
				database has transactions or
			        no privileges revoked */
	char sqlwarn2; /* = W if a null value returned or
				ANSI database */
	char sqlwarn3; /* = W if no. in select list != no. in into list or
				turbo backend or no privileges granted */
	char sqlwarn4; /* = W if no where clause on prepared update, delete or
				incompatible float format */
	char sqlwarn5; /* = W if non-ANSI statement */
	char sqlwarn6; /* = W if server is in data replication secondary mode */
	char sqlwarn7; /* = W if database locale is different from proc_locale
			*/
	} sqlwarn;
#endif
    } ifx_sqlca_t;

#define SQLNOTFOUND 100

#ifdef __cplusplus
}
#endif

#ifdef __BORLANDC__
#pragma option -a-
#else
#pragma pack ()
#endif

#endif /* SQLCA_INCL */
#line 85 "d:/informix/incl/esql/sqlca.h"
#line 11 "ifmxdiag.ec"

/* Werte fuer warn_flg */
#define WARN   1
#define NOWARN 0

/*
 *  exp_chk.ec
 */

/* 
 * EXEC SQL define SUCCESS 0;
 */
#line 20 "ifmxdiag.ec"

/* 
 * EXEC SQL define WARNING 1;
 */
#line 21 "ifmxdiag.ec"

/* 
 * EXEC SQL define NODATA 100;
 */
#line 22 "ifmxdiag.ec"

/* 
 * EXEC SQL define RTERROR -1;
 */
#line 23 "ifmxdiag.ec"


/*
 * The sqlstate_err() function checks the SQLSTATE status variable to see
 * if an error or warning has occurred following an SQL statement.
 */
int sqlstate_err( void )
{
    int err_code = -1;

    if(SQLSTATE[0] == '0') /* trap '00', '01', '02' */
        {
        switch(SQLSTATE[1])
            {
            case '0': /* success - return 0 */
                err_code = 0;
                break;
            case '1': /* warning - return 1 */
                err_code = 1;
                break;
            case '2': /* end of data - return 100 */
                err_code = 100;
                break;
            default: /* error - return SQLCODE */
                break;
            }
        }
    return(err_code);
}


/*
 * The disp_sqlstate_err() function executes the GET DIAGNOSTICS
 * statement and prints the detail for each exception that is returned.
 */
void disp_sqlstate_err( void )
{
int j;

/*
 * EXEC SQL BEGIN DECLARE SECTION;
 */
#line 62 "ifmxdiag.ec"
#line 63 "ifmxdiag.ec"
#line 63 "ifmxdiag.ec"
int exception_count;
#line 64 "ifmxdiag.ec"
  char overflow[2];
#line 68 "ifmxdiag.ec"
  char class_id[255];
#line 69 "ifmxdiag.ec"
  char subclass_id[255];
#line 70 "ifmxdiag.ec"
  char message[255];
#line 71 "ifmxdiag.ec"
int messlen;
#line 72 "ifmxdiag.ec"
  char sqlstate_code[6];
#line 73 "ifmxdiag.ec"
int i;
/*
 * EXEC SQL END DECLARE SECTION;
 */
#line 74 "ifmxdiag.ec"


    fprintf(fperr,"---------------------------------");
    fprintf(fperr,"-------------------------\n");
    fprintf(fperr,"SQLSTATE: %s\n",SQLSTATE);
    fprintf(fperr,"SQLCODE: %d\n", SQLCODE);
    fprintf(fperr,"\n");

/*
 *     EXEC SQL get diagnostics :exception_count = NUMBER,
 *         :overflow = MORE;
 */
#line 82 "ifmxdiag.ec"
  {
#line 83 "ifmxdiag.ec"
  static ifx_hostvar_t _SQhtab[] =
    {
      { 0, 1, 102, sizeof(exception_count), 0, 0, 0, 0 },
      { 0, 2, 100, 2, 0, 0, 0, 0 },
      { 0, 0, 0, 0, 0, 0, 0, 0 }
#line 83 "ifmxdiag.ec"
    };
  _SQhtab[0].hostaddr = (char *)&exception_count;
  _SQhtab[1].hostaddr = (overflow);
#line 83 "ifmxdiag.ec"
  sqli_diag_get(ESQLINTVERSION, _SQhtab, -1);
#line 83 "ifmxdiag.ec"
  }
    fprintf(fperr,"EXCEPTIONS:  Number=%d\t", exception_count);
    fprintf(fperr,"More? %s\n", overflow);
    for (i = 1; i <= exception_count; i++)
        {
/*
 *         EXEC SQL get diagnostics  exception :i
 *             :sqlstate_code = RETURNED_SQLSTATE,
 *             :class_id = CLASS_ORIGIN, :subclass_id = SUBCLASS_ORIGIN,
 *             :message = MESSAGE_TEXT, :messlen = MESSAGE_LENGTH;
 */
#line 88 "ifmxdiag.ec"
  {
#line 91 "ifmxdiag.ec"
  static ifx_hostvar_t _SQhtab[] =
    {
      { 0, 3, 100, 6, 0, 0, 0, 0 },
      { 0, 4, 100, 255, 0, 0, 0, 0 },
      { 0, 5, 100, 255, 0, 0, 0, 0 },
      { 0, 6, 100, 255, 0, 0, 0, 0 },
      { 0, 7, 102, sizeof(messlen), 0, 0, 0, 0 },
      { 0, 0, 0, 0, 0, 0, 0, 0 }
#line 91 "ifmxdiag.ec"
    };
  _SQhtab[0].hostaddr = (sqlstate_code);
  _SQhtab[1].hostaddr = (class_id);
  _SQhtab[2].hostaddr = (subclass_id);
  _SQhtab[3].hostaddr = (message);
  _SQhtab[4].hostaddr = (char *)&messlen;
#line 91 "ifmxdiag.ec"
  sqli_diag_get(ESQLINTVERSION, _SQhtab, i);
#line 91 "ifmxdiag.ec"
  }
        fprintf(fperr,"- - - - - - - - - - - - - - - - - - - -\n");
        fprintf(fperr,"EXCEPTION %d: SQLSTATE=%s\n", i,
            sqlstate_code);
        message[messlen-1] = '\0';
        fprintf(fperr,"MESSAGE TEXT: %s\n", message);

        //j = byleng(class_id, stleng(class_id));
        j = byleng(class_id, strlen(class_id));
        class_id[j] = '\0';
        fprintf(fperr,"CLASS ORIGIN: %s\n",class_id);

        j = byleng(subclass_id, strlen(subclass_id));
        subclass_id[j] = '\0';
        fprintf(fperr,"SUBCLASS ORIGIN: %s\n",subclass_id);
        }

    fprintf(fperr,"---------------------------------");
    fprintf(fperr,"-------------------------\n");
}

void disp_error( char *stmt )
{
    fprintf(fperr,"\n********Error encountered in %s********\n",
        stmt);
    disp_sqlstate_err();
}

void disp_warning( char *stmt )
{
    fprintf(fperr,"\n********Warning encountered in %s********\n",
        stmt);
    disp_sqlstate_err();
}

void disp_exception( char *stmt, int sqlerr_code, int warn_flg )
{
    switch (sqlerr_code)
        {
        case 0:
        case 100:
            break;
        case 1:
            if(warn_flg)
                disp_warning(stmt);
            break;
        case -1:
            disp_error(stmt);
            break;
        default:
            fprintf(fperr,"\n********INVALID EXCEPTION STATE for %s********\n",
                stmt);
            break;
        }
}

/*
 * The exp_chk() function calls sqlstate_err() to check the SQLSTATE
 * status variable to see if an error or warning has occurred following
 * an SQL statement. If either condition has occurred, exp_chk()
 * calls disp_sqlstate_err() to print the detailed error information.
 *
 * This function handles exceptions as follows:
 *   runtime errors - call exit(1)
 *   warnings - continue execution, returning "1"
 *   success - continue execution, returning "0"
 *   Not Found - continue execution, returning "100"
 */
long exp_chk( char *stmt, int warn_flg )
{
    int sqlerr_code = 0;

    sqlerr_code = sqlstate_err();
    disp_exception(stmt, sqlerr_code, warn_flg);

    if(sqlerr_code == -1)   
        {
        /* Exit the program after examining the error */
        fprintf(fperr,"********Program terminated*******\n\n");
        exit(1);
        }
    /* else */                  /* Exception is "success", "Not Found",*/
        return(sqlerr_code);    /*  or "warning"                       */
}

/*
 * The exp_chk2() function calls sqlstate_err() to check the SQLSTATE
 * status variable to see if an error or warning has occurred following
 * an SQL statement. If either condition has occurred, exp_chk2()
 * calls disp_exception() to print the detailed error information.
 *
 * This function handles exceptions as follows:
 *   runtime errors - continue execution, returning SQLCODE (<0)
 *   warnings - continue execution, returning one (1)
 *   success - continue execution, returning zero (0)
 *   Not Found - continue execution, returning 100
 */
long exp_chk2( char *stmt, int warn_flg )
{
    int sqlerr_code = 0;
    long sqlcode;

    sqlcode = SQLCODE;  /* save SQLCODE in case of error */
    sqlerr_code = sqlstate_err();
    disp_exception(stmt, sqlerr_code, warn_flg);

    if(sqlerr_code == -1)  
        sqlerr_code = sqlcode;

    return(sqlerr_code);
}

/*
 * The whenexp_chk() function calls sqlstate_err() to check the SQLSTATE
 * status variable to see if an error or warning has occurred following
 * an SQL statement. If either condition has occurred, whenerr_chk()
 * calls disp_sqlstate_err() to print the detailed error information.
 *
 * This function is expected to be used with the WHENEVER SQLERROR
 * statement: it executes an exit(1) when it encounters a negative
 * error code. It also assumes the presence of the "statement" global
 * variable, set by the calling program to the name of the statement
 * encountering the error.
*/
int whenexp_chk( void )
{
    int sqlerr_code = 0;
    int disp = 0;

    sqlerr_code = sqlstate_err();

    if(sqlerr_code == 1)
        {
        disp = 1;
        fprintf(fperr,"\n********Warning encountered in %s********\n",
            statement);
        }
    else
        if(sqlerr_code == -1)
            {
            fprintf(fperr,"\n********Error encountered in %s********\n",
                statement);
            disp = 1;
            }
    if(disp)
        disp_sqlstate_err();

    if(sqlerr_code == -1)
        {
        /* Exit the program after examining the error */
        fprintf(fperr,"********Program terminated*******\n\n");
        exit(1);
        return(sqlerr_code);
        }
    else
        {
        if(sqlerr_code == 1)
            fprintf(fperr,"\n********Program execution continues********\n\n");
        return(sqlerr_code);
        }
}

#line 251 "ifmxdiag.ec"
