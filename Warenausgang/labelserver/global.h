#ifndef GLOBALH
#define GLOBALH

#include <windows.h>

#define NUMLINES 150
#define LINELENMAX 80

#define GXDATA 1
#define GXNET  2

#define WM_USER_CREATE_UI (WM_USER + 1)
#define WM_USER_CREATE_LD (WM_USER + 2)
#define WM_USER_KUNDAT (WM_USER + 6)
#define WM_USER_BRANDAT (WM_USER + 7)
#define WM_USER_AUFDAT (WM_USER + 8)

extern FILE *fperr;
extern int testmode;
extern int simulation;
extern int gxformat;
extern char tbuf[];

extern HWND hDlgGlobal;

#endif