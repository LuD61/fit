#include <windows.h>

extern HWND hDlgMsg;

void MsgLine( const TCHAR *, ... );
void MsgHRESULT( HRESULT, const TCHAR *, ...  );
void TimeMsgLine( const TCHAR *, ... );
void FatalError( const TCHAR *, ... );
