#include "stdafx.h"
#include "global.h"
#include "msgline.h"
#include "gxnetfun.h"
#include "items.h"
#include "gxerrkey.h"

#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

us psw_pckhdl;
us quit_ok;
us end_ok;
us returncode;
us debugcode;
us lgx_close;
us psw_etikettyp; 
us psw_fehl_pck;
us dim_prs;

int gxformat;

char fukennung[] = { 'q', 'w', 'l', 'e', 0, 0, 'Q', 'T' };

us infolen;
static char infobuf[2000];

char header[500];

char *hptr;

extern int testmode;
extern FILE *fperr;

GGATX_t ggatx1, ggatx2, ggatx3, ggatx4, ggatx5, ggatx6, ggatx7,
                ggatx8, ggatx9, ggatx10, ggatx11, ggatx12,
 			    ggatx13, ggatx14, ggatx15, ggatx16,
			    ggatx17, ggatx18, ggatx19, ggatx20;
GGDAT_t ggdat;

GGCAB_t ggcab1, ggcab2, ggcab3, ggcab4, ggcab5;

char *funame( us fu )
{
	switch (fu)
	{
	case GGW_AUSZEICH_ART: return "GGW_AUSZEICH_ART";
	case GGW_AUSZEICHART_WECHS: return "GGW_AUSZEICHART_WECHS";
	case GGW_PLUTEXT_DISPLAY: return "GGW_PLUTEXT_DISPLAY";
	case GGW_ETIKETDRUCK_START: return "GGW_ETIKETDRUCK_START";
	case GGW_ETIKET_ART: return "GGW_ETIKET_ART";
	case GGW_ETIKET_GR: return "GGW_ETIKET_GR";
	case GGW_SUM_ART: return "GGW_SUM_ART";
	case GGW_SUM_START: return "GGW_SUM_START";
	case GGW_SUM1VORWAHL_ART: return "GGW_SUM1VORWAHL_ART";
	case GGW_SUM2VORWAHL_ART: return "GGW_SUM2VORWAHL_ART";
	case GGW_SUM3VORWAHL_ART: return "GGW_SUM3VORWAHL_ART";
	case GGW_GEW_DIMENSION: return "GGW_GEW_DIMENSION";
	case GGW_GPREIS_TEILUNG: return "GGW_GPREIS_TEILUNG";
	case GGW_PLUW_TARA: return "GGW_PLUW_TARA";
	case GGW_PREIS_BERECHNUNG: return "GGW_PREIS_BERECHNUNG";    
	case GGW_SUM_TEXT: return "GGW_SUM_TEXT";
	case GGW_DRUCKINTERN_ENABLE: return "GGW_DRUCKINTERN_ENABLE";
	case GGW_SENDKANAL_A_ENABLE: return "GGW_SENDKANAL_A_ENABLE";
	case GGW_SENDKANAL_B_ENABLE: return "GGW_SENDKANAL_B_ENABLE";
	case GGW_SENDKANAL_C_ENABLE: return "GGW_SENDKANAL_C_ENABLE";
	case GGW_SENDKANAL_D_ENABLE: return "GGW_SENDKANAL_D_ENABLE";
	case GGW_SENDKANAL_E_ENABLE: return "GGW_SENDKANAL_E_ENABLE";
	case GGW_SONDETIKET_KRITERIUM: return "GGW_SONDETIK_KRITERIUM";
	case GGW_SONDETIKET_WDKANAL: return "GGW_SONDETIK_WDKANAL";
	case GGW_STOP_SUM2: return "GGW_STOP_SUM2";
	case GGW_STOP_SUM3: return "GGW_STOP_SUM3";
	case GGW_LAND_SKND: return "GGW_LAND_SKND";
	case GGW_LAND:      return "GGW_LAND";

	case GGL_UHR2: return "GGL_UHR2";
	case GGL_DATUM1: return "GGL_DATUM1";
	case GGL_DATUM2: return "GGL_DATUM2";
	case GGL_UHR1: return "GGL_UHR1";
	case GGL_HALTBAR1: return "GGL_HALTBAR1";
	case GGL_HALTBAR2: return "GGL_HALTBAR2";
	case GGL_GERAETENR: return "GGL_GERAETENR";
	case GGL_EINZEL_NUMERATOR: return "GGL_EINZEL_NUMERATOR";
	case GGL_SUM1_NUMERATOR: return "GGL_SUM1_NUMERATOR";
	case GGL_STCK_PRO_PCK: return "GGL_STCK_PRO_PCK";
	case GGL_PLUNR: return "GGL_PLUNR";
	case GGL_KDNR: return "GGL_KDNR";
	case GGL_WGNR: return "GGL_WGNR";
	case GGL_VM_PCKNR: return "GGL_VM_PCKNR";
	case GGL_AM_PARNR: return "GGL_AM_PARNR";
	case GGL_ATXNR: return "GGL_ATXNR";
	case GGL_ATXNR1: return "GGL_ATXNR1";
	case GGL_ATXNR2: return "GGL_ATXNR2";
	case GGL_ATXNR3: return "GGL_ATXNR3";
	case GGL_ATXNR4: return "GGL_ATXNR4";
	case GGL_ATXNR5: return "GGL_ATXNR5";
	case GGL_LOGONR1: return "GGL_LOGONR1";
	case GGL_ETIKETFOLGE_VORWAHL: return "GGL_ETIKETFOLGE_VORWAHL";
	case GGL_LOSNR: return "GGL_LOSNR";
	case GGL_CABNR1: return "GGL_CABNR1";
	case GGL_CABNR2: return "GGL_CABNR2";
	case GGL_CABNR3: return "GGL_CABNR3";
	case GGL_CABNR4: return "GGL_CABNR4";
	case GGL_CABNR5: return "GGL_CABNR5";
	case GGL_DTXNR1: return "GGL_DTXNR1";
	case GGL_DTXNR2: return "GGL_DTXNR2";
	case GGL_DTXNR3: return "GGL_DTXNR3";
	case GGL_CABNR: return "GGL_CABNR";
	case GGL_SUM2_NUMERATOR: return "GGL_SUM2_NUMERATOR";
	case GGL_CHARGENNR: return "GGL_CHARGENNR";
	case GGL_ETIKETPARNR: return "GGL_ETIKETPARNR";
	case GGL_VM_PARNR: return "GGL_VM_PARNR";
	case GGL_MINMAXPARNR: return "GGL_MINMAXPARNR";
	case GGL_LOGONR2: return "GGL_LOGONR2";
	case GGL_LOGONR3: return "GGL_LOGONR3";
	case GGL_ATXNR6: return "GGL_ATXNR6";
	case GGL_ATXNR7: return "GGL_ATXNR7";
	case GGL_ATXNR8: return "GGL_ATXNR8";
	case GGL_ATXNR9: return "GGL_ATXNR9";
	case GGL_ATXNR10: return "GGL_ATXNR10";
	case GGL_ATXNR11: return "GGL_ATXNR11";
	case GGL_ATXNR12: return "GGL_ATXNR12";
	case GGL_ATXNR13: return "GGL_ATXNR13";
	case GGL_ATXNR14: return "GGL_ATXNR14";
	case GGL_ATXNR15: return "GGL_ATXNR15";
	case GGL_ATXNR16: return "GGL_ATXNR16";
	case GGL_ATXNR17: return "GGL_ATXNR17";
	case GGL_ATXNR18: return "GGL_ATXNR18";
	case GGL_ATXNR19: return "GGL_ATXNR19";
	case GGL_ATXNR20: return "GGL_ATXNR20";
	case GGL_ANUM1: return "GGL_ANUM1";
	case GGL_ANUM2: return "GGL_ANUM2";
	case GGL_ANUM3: return "GGL_ANUM3";
	case GGL_UHR_GX: return "GGL_UHR_GX";
	case GGL_NUTRITION_PARNR: return "GGL_NUTRITION_PARNR";
	case GGL_ETIK_KOPIEN: return "GGL_ETIK_KOPIEN";
	case GGL_NR_KDETIK_SUM1: return "GGL_NR_KDETIK_SUM1";
	case GGL_NR_KDETIK_SUM2: return "GGL_NR_KDETIK_SUM2";
	case GGL_NR_KDETIK_SUM3: return "GGL_NR_KDETIK_SUM3";
	case GGL_STCK_SUM1_VORWAHL: return "GGL_STCK_SUM1_VORWAHL";
	case GGL_STCK_SUM2_VORWAHL: return "GGL_STCK_SUM2_VORWAHL";
	case GGL_STCK_SUM3_VORWAHL: return "GGL_STCK_SUM3_VORWAHL";
	case GGL_SUM3_NUMERATOR: return "GGL_SUM3_NUMERATOR";

	case GGD_GEW_NETTO_EINZEL: return "GGD_GEW_NETTO_EINZEL";
	case GGD_GEW_TARA_EINZEL: return "GGD_GEW_TARA_EINZEL";
	case GGD_GEW_ZUTATEN: return "GGD_GEW_ZUTATEN";
	case GGD_PRS_GPREIS: return "GGD_PRS_GPREIS";
	case GGD_PRS_SPREIS: return "GGD_PRS_SPREIS";
	case GGD_GEW_SUM1_VORWAHL:  return "GGD_GEW_SUM1_VORWAHL";
	case GGD_GEW_SUM2_VORWAHL:  return "GGD_GEW_SUM2_VORWAHL";
	case GGD_GEW_SUM3_VORWAHL:  return "GGD_GEW_SUM3_VORWAHL";
	case GGD_PRS_SUM1_VORWAHL: return "GGD_PRS_SUM1_VORWAHL";
	case GGD_PRS_SUM2_VORWAHL: return "GGD_PRS_SUM2_VORWAHL";
	case GGD_PRS_SUM3_VORWAHL: return "GGD_PRS_SUM3_VORWAHL";

	case GGT_ATX: return "GGT_ATX";
	case GGT_ATX1: return "GGT_ATX1";
	case GGT_ATX2: return "GGT_ATX2";
	case GGT_ATX3: return "GGT_ATX3";
	case GGT_ATX4: return "GGT_ATX4";
	case GGT_ATX5: return "GGT_ATX5";
	case GGT_ATX6: return "GGT_ATX6";
	case GGT_ATX7: return "GGT_ATX7";
	case GGT_ATX8: return "GGT_ATX8";
	case GGT_ATX9: return "GGT_ATX9";
	case GGT_ATX10: return "GGT_ATX10";
	case GGT_ATX11: return "GGT_ATX11";
	case GGT_ATX12: return "GGT_ATX12";
	case GGT_ATX13: return "GGT_ATX13";
	case GGT_ATX14: return "GGT_ATX14";
	case GGT_ATX15: return "GGT_ATX15";
	case GGT_ATX16: return "GGT_ATX16";
	case GGT_ATX17: return "GGT_ATX17";
	case GGT_ATX18: return "GGT_ATX18";
	case GGT_ATX19: return "GGT_ATX19";
	case GGT_ATX20: return "GGT_ATX20";
	case GGT_DTX1: return "GGT_DTX1";
	case GGT_DTX2: return "GGT_DTX2";
	case GGT_CAB: return "GGT_CAB";
	case GGT_CAB1: return "GGT_CAB1";
	case GGT_CAB2: return "GGT_CAB2";
	case GGT_CAB3: return "GGT_CAB3";
	case GGT_CAB4: return "GGT_CAB4";
	case GGT_CAB5: return "GGT_CAB5";
	case GGT_CTS1: return "GGT_CTS1";
	case GGT_CTS2: return "GGT_CTS2";
	case GGT_CTS3: return "GGT_CTS3";
	case GGT_CTS4: return "GGT_CTS4";
	case GGT_CTS5: return "GGT_CTS5";
	case GGT_CODE1: return "GGT_CODE1";

	case AMW_START_STOP: return "AMW_START_STOP";
	case AMW_BAENDER_BETRART: return "AMW_BAENDER_BETRART";

	case LGX_CLOSE: return "LGX_CLOSE";
	case LGW_QUIT_OK: return "LGW_QUIT_OK";
	case LGW_RETURN: return "LGW_RETURN";
	case LGW_UFKENN: return "LGW_UFKENN";
	case LGW_DEBUG: return "LGW_DEBUG";
	case LGW_ACCEPT: return "LGW_ACCEPT";
	case LGW_UFKENN_INTERN: return "LGW_UFKENN_INTERN";
	case LGV_QUIT: return "LGV_QUIT";
	case LGV_SEQUENZ: return "LGV_SEQUENZ";

	case PSW_ETIKETTYP: return "PSW_ETIKETTYP";
	case PSW_PCKHDL: return "PSW_PCKHDL";
	case PSW_FEHL_PCK: return "PSW_FEHL_PCK";
	case PSW_GETPCK: return "PSW_GETPCK";
	case PSW_QUIT_OK: return "PSW_QUIT_OK";
	case PSW_AUSZEICH_ART: return "PSW_AUSZEICH_ART";
	case PSL_STCK_SUM: return "PSL_STCK_SUM";
	case PSL_STCK_SUM1: return "PSL_STCK_SUM1";
	case PSL_STCK_SUM2: return "PSL_STCK_SUM2";
	case PSL_STCK_SUM3: return "PSL_STCK_SUM3";
	case PSL_PCK_CNT: return "PSL_PCK_CNT";
	case PSL_SUM1_CNT: return "PSL_SUM1_CNT";
	case PSL_SUM2_CNT: return "PSL_SUM2_CNT";
	case PSD_GEW_NETTO_EINZEL: return "PSD_GEW_NETTO_EINZEL";
	case PSD_GEW_SUM1: return "PSD_GEW_SUM1";
	case PSD_GEW_SUM2: return "PSD_GEW_SUM2";
	case PSD_GEW_SUM3: return "PSD_GEW_SUM3";
	case PSD_PRS_SUM1: return "PSD_PRS_SUM1";
	case PSD_PRS_SUM2: return "PSD_PRS_SUM2";
	case PSD_GEW_BRUTTO_SUM: return "PSD_GEW_BRUTTO_SUM";
	case PSD_GEW_ABTROPF_SUM: return "PSD_GEW_ABTROPF_SUM";
	case PSD_GEW_FUELLMENGE_SUM: return "PSD_GEW_FUELLMENGE_SUM";
	case PSD_GEW_TARA_SUM: return "PSD_GEW_TARA_SUM";
	case PSD_GEW_ZUTATEN_SUM: return "PSD_GEW_ZUTATEN_SUM";
	case PSD_GEW_NETTO_INFO: return "PSD_GEW_NETTO_INFO";
	case PSD_GEW_TARA_EINZEL: return "PSD_GEW_TARA_EINZEL";
	case PSD_GEW_NETTO_SUM: return "PSD_GEW_NETTO_SUM";
	case PSD_PRS_VKPREIS: return "PSD_PRS_VKPREIS";
	case PSD_PRS_SUM: return "PSD_PRS_SUM";
	case PSV_PCK: return "PSV_PCK";
	case PSV_DATA: return "PSV_DATA";
	case PSV_QUIT: return "PSV_QUIT";

	case DBW_END_OK: return "DBW_END_OK";
	case DBW_TAB: return "DBW_TAB";
	case DBW_ATTRDESCRIP: return "DBW_ATTRDESCRIP";
	case DBV_INFO: return "DBV_INFO";
	case DBV_DATA_VDBT: return "DBV_DATA_VDBT";
	case DBV_DEL_VDBT: return "DBV_DEL_VDBT";
	case DBT_FILTER: return "DBT_FILTER";

	case WZW_END_OK: return "WZW_END_OK";
	case WZW_SUMVORWAHL_ART: return "WZW_SUMVORWAHL_ART";
	case WZW_REMOTE_SOFTKEY_NR: return "WZW_REMOTE_SOFTKEY_NR";
	case WZW_REMOTE_SOFTKEY_TYP: return "WZW_REMOTE_SOFTKEY_TYP";
	case WZW_REMOTE_SOFTKEY_ATTR: return "WZW_REMOTE_SOFTKEY_ATTR";
	case WZW_REMOTE_SOFTKEY_STELLEN: return "WZW_REMOTE_SOFTKEY_STELLEN";
	case WZW_REMOTE_DISPLAY_ATTR: return "WZW_REMOTE_DISPLAY_ATTR";
	case WZW_MODE: return "WZW_MODE";
	case WZW_SOFTKEY_PAGE: return "WZW_SOFTKEY_PAGE";
	case WZW_MINUS_SUM: return "WZW_MINUS_SUM";
	case WZL_STCK_SUM_VORWAHL: return "WZL_STCK_SUM_VORWAHL";
	case WZL_REMOTE_SOFTKEY_EINGABE: return "WZL_REMOTE_SOFTKEY_EINGABE";
	case WZL_MINUS_STCK_EINZEL: return "WZL_MINUS_STCK_EINZEL";
	case WZD_GEW_SUMVORWAHL: return "WZD_GEW_SUMVORWAHL";
	case WZD_PRS_SUMVORWAHL: return "WZD_PRS_SUMVORWAHL";
	case WZT_REMOTE_SOFTKEY_TEXT: return "WZT_REMOTE_SOFTKEY_TEXT";
	case WZT_REMOTE_SOFTKEY_EINGABE: return "WZT_REMOTE_SOFTKEY_EINGABE";
	case WZT_REMOTE_DISPLAY_TEXT: return "WZT_REMOTE_DISPLAY_TEXT";
	case WZV_SUM: return "WZV_SUM";
	case WZV_SUMKLAS: return "WZV_SUMKLAS";
	case WZV_REMOTE_TO_SOFTKEY: return "WZV_REMOTE_TO_SOFTKEY";
	case WZV_SOFTKEY_TO_REMOTE: return "WZV_SOFTKEY_TO_REMOTE";
	case WZV_REMOTE_DISPLAY: return "WZV_REMOTE_DISPLAY";
	case WZV_MINUS: return "WZV_MINUS";

	case XCW_PRINT: return "XCW_PRINT";
	case XCW_PRINT_OLD: return "XCW_PRINT_OLD";
	case XCW_DISPLAY_TEXT: return "XCW_DISPLAY_TEXT";
	case XCW_LOCK: return "XCW_LOCK";
	case XCW_DBSUM_UPDATE: return "XCW_DBSUM_UPDATE";
	case XCW_UNLOCK: return "XCW_UNLOCK";
	case XCW_SUM_CLEAR: return "XCW_SUM_CLEAR";
	case XCV_DBTAB_DATASET: return "XCV_DBTAB_DATASET";

	default: 
		fprintf(fperr, "0x%04X: ", fu );
		returncode = 9999;
		return "Funktion ist unbekannt."; 
	}
}

char *reverse( char *s, us n )
{
	char *p, *q;
	char c;
	for ( p = s, q = s + n - 1; p < q; p++, q-- )
		c = *p, *p = *q, *q = c;
	return s;
}

char *reversecpy( char *p, char *q, us n )
{
	memcpy( p, q, n );
	reverse( p, n );
	return p;
}

void fuheaderV( us fu )
{
	int b1, b2;
	int len;
	b1 = fu >> 8;
	b2 = fu & 0x0FF;
	hptr += sprintf( hptr, "|LX02" );
    switch ( b1 )
	{
	case 0x06: hptr += sprintf( hptr, "|GV%02X", b2 ); break; 
	case 0x36: hptr += sprintf( hptr, "|PV%02X", b2 ); break; 
	case 0x46: hptr += sprintf( hptr, "|LV%02X", b2 ); break; 
	case 0x56: hptr += sprintf( hptr, "|DV%02X", b2 ); break; 
	case 0x76: hptr += sprintf( hptr, "|SV%02X", b2 ); break;
	case 0x96: hptr += sprintf( hptr, "|WV%02X", b2 ); break; 
	case 0xA6: hptr += sprintf( hptr, "|XV%02X", b2 ); break; 
	case 0xD6: hptr += sprintf( hptr, "|MV%02X", b2 ); break; 
	}
	len = (us) (hptr - header);
    memmove( header + 5, header, len );
	memmove( header, header + len, 5 );
	memset( header + len, '\0', 5 );
}

void fuheader( us fu )
{
    int b1, b2;
	b1 = fu >> 8;
	b2 = fu & 0x00FF;

	// fprintf(fperr, "fuheader: 0x%04X\n", fu );

    switch ( b1 )
	{
	case 0x01: hptr += sprintf( hptr, "|GW%02X", b2 ); break;
	case 0x02: hptr += sprintf( hptr, "|GL%02X", b2 ); break;
	case 0x03: hptr += sprintf( hptr, "|GD%02X", b2 ); break; 
	case 0x07: hptr += sprintf( hptr, "|GT%02X", b2 ); break; 

	case 0x11: hptr += sprintf( hptr, "|AW%02X", b2 ); break; 
	case 0x12: hptr += sprintf( hptr, "|AL%02X", b2 ); break;

	case 0x31: hptr += sprintf( hptr, "|PW%02X", b2 ); break; 
	case 0x32: hptr += sprintf( hptr, "|PL%02X", b2 ); break;
	case 0x33: hptr += sprintf( hptr, "|PD%02X", b2 ); break; 
	case 0x37: hptr += sprintf( hptr, "|PT%02X", b2 ); break; 

	case 0x40: hptr += sprintf( hptr, "|LX%02X", b2 ); break; 
	case 0x41: hptr += sprintf( hptr, "|LW%02X", b2 ); break; 

	case 0x50: hptr += sprintf( hptr, "|DX%02X", b2 ); break; 
	case 0x51: hptr += sprintf( hptr, "|DW%02X", b2 ); break; 
	case 0x52: hptr += sprintf( hptr, "|DL%02X", b2 ); break; 
	case 0x57: hptr += sprintf( hptr, "|DT%02X", b2 ); break; 

	case 0x71: hptr += sprintf( hptr, "|SW%02X", b2 ); break;
	case 0x72: hptr += sprintf( hptr, "|SL%02X", b2 ); break;
	case 0x77: hptr += sprintf( hptr, "|ST%02X", b2 ); break; 

	case 0x91: hptr += sprintf( hptr, "|WW%02X", b2 ); break; 
	case 0x92: hptr += sprintf( hptr, "|WL%02X", b2 ); break; 
	case 0x93: hptr += sprintf( hptr, "|WD%02X", b2 ); break; 
	case 0x97: hptr += sprintf( hptr, "|WT%02X", b2 ); break; 

	case 0xA0: hptr += sprintf( hptr, "|XW%02X", b2 ); break; 
	case 0xA1: hptr += sprintf( hptr, "|XW%02X", b2 ); break; 
	case 0xA2: hptr += sprintf( hptr, "|XL%02X", b2 ); break; 

	case 0xB1: hptr += sprintf( hptr, "|VW%02X", b2 ); break; 
	case 0xB2: hptr += sprintf( hptr, "|VL%02X", b2 ); break; 

	case 0xD1: hptr += sprintf( hptr, "|MW%02X", b2 ); break; 

	default: 
		fprintf(fperr, "0x%04X: fu unbekannt.", fu );
		returncode = 9999;
		break; 

	}
}

us getfu( void )
{
	us fu;
    int c;
	fu = 0;
    
	switch ( hptr[0] )
	{
	case 'G': fu += 0x0000; break;
	case 'A': fu += 0x1000; break; 
	case 'P': fu += 0x3000; break; 
	case 'L': fu += 0x4000; break; 
	case 'D': fu += 0x5000; break; 
	case 'S': fu += 0x7000; break; 
	case 'W': fu += 0x9000; break; 
	case 'X': fu += 0xA000; break; 
	case 'V': fu += 0xB000; break; 
	case 'M': fu += 0xD000; break; 
	}

	switch ( hptr[1] )
	{
	case 'X': fu += 0x0000; break;
	case 'W': fu += 0x0100; break;
	case 'L': fu += 0x0200; break;
	case 'D': fu += 0x0300; break;
	case 'V': fu += 0x0600; break;
	case 'T': fu += 0x0700; break;
	}

	c = hptr[2];
	fu += 0x10 * ((c <'A')?(c-'0'):(c + 0xA - 'A' ));
       
	c = hptr[3];
	fu += (c <'A')?(c-'0'):(c + 0xA - 'A' );

	hptr = strchr( hptr, '|' ) + 1;

	return fu;
}

us formfu( char *buf, us fu, ... )
{
	us len;
	char *p;
	ul lw;
	us w;
	va_list ap;
	va_start( ap, fu );
	switch( fukennung[ fu >> 8 & 7 ] )
	{
	case 'Q':
		len = va_arg( ap, us );
		p = va_arg( ap, char * );
		if ( gxformat == GXDATA )
		{
			fuheaderV( fu  );
			/* Daten unveraendert, aber Stringabschluss */           
		}
		else
		{
			len += formfu( p + len, LGX_CLOSE );
			memmove( buf + 4, p, len ); 
			reversecpy( buf + 2, (char*) &len, 2 );
			reversecpy( buf, (char*) &fu, 2 ); 
			len += 4;
		}
		break;
	case 'T':
		len = va_arg( ap, us );
		p = va_arg( ap, char * );
		if ( gxformat == GXDATA )
		{
			fuheader( fu  );
			len = sprintf( buf, "|%s", p );
		}
		else
		{
			memmove( buf + 4, p, len ); 
			if ( len & 1 )
			{
				buf[len + 4] = '\0';
				len++;
			}
			reversecpy( buf + 2, (char*) &len, 2 );
			reversecpy( buf, (char*) &fu, 2 ); 
			len += 4;
		}
		break;
	case 'e':
		w = va_arg( ap, us );
		lw = va_arg( ap, ul );
		if ( gxformat == GXDATA )
		{
			fuheader( fu  );
			switch( w )
			{
			case DIM_GEW: len = sprintf( buf, "|KG|-3|%lu", lw ); break;
			default:      len = sprintf( buf, "|%u|%lu", w, lw ); break;
			}
		}
		else
		{
			reversecpy( buf + 2, (char*) &w, 2 );
			reversecpy( buf + 4, (char*) &lw, 4 );
			reversecpy( buf, (char*) &fu, 2 ); 
			len = 8;
		}
		break;
	case 'l':
		lw = va_arg( ap, ul );
		if ( gxformat == GXDATA )
		{
			fuheader( fu  );
			len = sprintf( buf, "|%u", lw );
		}
		else
		{
			reversecpy( buf + 2, (char*) &lw, 4 );
			reversecpy( buf, (char*) &fu, 2 ); 
			len = 6;
		}
		break;
	case 'w':
		w = va_arg( ap, us );
		if ( gxformat == GXDATA )
		{
			fuheader( fu  );
			len = sprintf( buf, "|%u", w );
		}
		else
		{
			reversecpy( buf + 2, (char*) &w, 2 );
			reversecpy( buf, (char*) &fu, 2 ); 
			len = 4;
		}
		break;
	case 'q': 
		if ( gxformat == GXDATA )
		{
			fuheader( fu  );
			len = 0;
		}
		else
		{
			reversecpy( buf, (char*) &fu, 2 ); 
			len = 2;
		}
		break;
	default:
		if (testmode) fprintf(fperr,"formfu: Falsche Fu: 0x%04X\n",fu);
		len = 0;
		break;
	}
	va_end(ap);
	return len;
}

void evalfu( us fu, us *w, ul *lw )
{
	switch (fu)
	{

	case GGD_PRS_GPREIS:
		*w = dim_prs;
		break;

	case PSD_GEW_NETTO_EINZEL:
	case PSD_GEW_NETTO_INFO:
		ggdat.netto = *lw;
		*w = DIM_GEW;
		break;

	case PSD_GEW_TARA_EINZEL:
		ggdat.tara = *lw;
		*w = DIM_GEW;
		break;

	case PSD_GEW_SUM1:
		ggdat.netsum = *lw;
		*w = DIM_GEW;
		break;

	case PSD_GEW_SUM2:
		ggdat.netsum2 = *lw;
		*w = DIM_GEW;
		break;
	case PSD_GEW_SUM3:
		ggdat.netsum3 = *lw;
		*w = DIM_GEW;
		break;

	case PSW_ETIKETTYP:
		psw_etikettyp = *w;
		break;

	case PSW_GETPCK:
		psw_etikettyp = *w;
		break;

	case PSD_GEW_NETTO_SUM:
		switch ( psw_etikettyp )
		{
		case ETI_SUMME_1:
			ggdat.netsum = *lw;
			break;
		case ETI_SUMME_2:
			ggdat.netsum2 = *lw;
			break;
		case ETI_SUMME_3:
			ggdat.netsum3 = *lw;
			break;
		}
		*w = DIM_GEW;

		break;

	case GGL_EINZEL_NUMERATOR:
             ggdat.numerator = *lw;
             break;
	case GGL_SUM1_NUMERATOR:
             ggdat.s1numerator = *lw;
             break;

	case GGL_SUM2_NUMERATOR:
             ggdat.s2numerator = *lw;
             break;

	case GGL_GERAETENR:
             ggdat.geraet = *lw;
             break;
	case GGL_DATUM1:
             ggdat.datum1 = *lw;
             break;

	case GGL_DATUM2:
             ggdat.datum2 = *lw;
             break;

	case GGD_GEW_NETTO_EINZEL:
	case GGD_GEW_TARA_EINZEL:
	case GGD_GEW_SUM1_VORWAHL:
	case GGD_GEW_SUM2_VORWAHL:
	case GGD_GEW_SUM3_VORWAHL:
	case GGD_GEW_ZUTATEN:
		*w = DIM_GEW;
		break;

	case PSD_PRS_SUM:
		switch ( psw_etikettyp )
		{
		case ETI_SUMME_1:
			ggdat.prsum = *lw;
			break;
		case ETI_SUMME_2:
			ggdat.prsum2 = *lw;
			break;
		case ETI_SUMME_3:
			ggdat.prsum3 = *lw;
			break;
		}
		*w = dim_prs;
		break;

	case PSL_STCK_SUM:
		switch ( psw_etikettyp )
		{
		case ETI_SUMME_1:
			ggdat.stksum = *lw;
			break;
		case ETI_SUMME_2:
			ggdat.stksum2 = *lw;
			break;
		case ETI_SUMME_3:
			ggdat.stksum3 = *lw;
			break;
		}
		break;

	case GGL_PLUNR:
		ggdat.plunr = *lw;
		break;

	case GGL_CHARGENNR:
		ggdat.chargennr = *lw;
		break;

	case PSW_PCKHDL:
		psw_pckhdl = *w;
		break;

	case PSW_FEHL_PCK:
		psw_fehl_pck = *w;
		break;

	case LGW_QUIT_OK:
		quit_ok = *w;
		break;

	case LGW_RETURN:
		returncode = *w;
        break;

	case LGW_DEBUG:
		debugcode = *w;
        // fprintf( fperr, "LGW_DEBUG: %s\n", gxerrkey(debugcode) );
		break;

	case DBW_END_OK:
		end_ok = *w;
		break;

	case LGX_CLOSE:
		lgx_close = fu;
		break;

	}
}

us checkfu( char* buf )
{
	us len, slen, c1;
	us fu;
	us w;
	ul lw;
	char *s;
	char *p;
	if (gxformat == GXDATA)
	{
		fu = getfu();
	}
	else
	{
		reversecpy( (char*) &fu, buf, 2 );
	}

	fprintf( fperr, "%s", funame(fu) );

	switch( fukennung[ fu >> 8 & 7 ] )
	{
	case 'Q':
		if ( gxformat == GXDATA )
		{
            len = (us) strlen( buf );
			/* Grj: Wo ist das Ende Krit. ? */
			fprintf(fperr, " len: %u\n", len );
            for ( c1 = 0; c1 < len; )
			{
				c1 += checkfu( buf + c1 );
			}
			len = 0;
		}
		else
		{
			len = 0;
			reversecpy( (char*) &len, buf + 2, 2 );
			fprintf(fperr, " len: %u\n", len );
			len += 4;
			for ( c1 = 4; c1 < len; )
			{
				c1 += checkfu( buf + c1 );
			}
			if ( fu == DBV_INFO )
			{
				infolen = (us) (len - 6);
				memcpy( infobuf, buf + 4, infolen );
			}
		}
		break;

	case 'T':
		if ( gxformat == GXDATA )
		{
			p = strchr( buf, '|' );
			len = (us)(p - buf);
			memcpy( infobuf, buf, len );
			infobuf[len] = '\0';
		}
		else
		{
			len = 0;
			reversecpy( (char*) &len, buf + 2, 2 );
			if ( len & 1 )
			{
				len++;
			}
			memcpy( infobuf, buf + 4, len );
			infobuf[len] = '\0';
			slen = len;
			s = buf + 4;
			len += 4;
		}
		fprintf( fperr, " len: %u :\n%s\n", len, infobuf );
		break;

	case 'e':
		if ( gxformat == GXDATA )
		{
			if ( memcmp( buf, "KG", 2) == 0 )
			{
				w = DIM_GEW;
				p = buf + strlen("KG|-3|");
				lw = atol(p);
				p = strchr( p, '|' ) + 1;
				len = (us)(p - buf);
			}
			else
			{
				w = atoi( buf );
				p = strchr( buf, '|' ) + 1;
				lw = atol( p );
				p = strchr( p, '|' ) + 1;
				len = (us)(p - buf) ;
			}
		}
		else
		{
			reversecpy( (char*) &w, buf + 2, 2 );
			reversecpy( (char*) &lw, buf + 4, 4 );
			len = 8;
		}
		fprintf(fperr, " w: 0x%04X lw: %lu\n", w, lw );
		break;          

	case 'l':
		if ( gxformat == GXDATA )
		{
			lw = atol( buf );
		    p = strchr( buf, '|' ) + 1;
			len = (us)(p - buf);
		}
		else
		{
			reversecpy( (char*) &lw, buf + 2, 4 );
			len = 6;
		}
		fprintf(fperr, " lw: %lu\n", lw );
		break;

	case 'w':
		if ( gxformat == GXDATA )
		{
			w = atoi( buf );
		    p = strchr( buf, '|' ) + 1;
			len = (us)(p - buf);
		}
		else
		{
			reversecpy( (char*) &w, buf + 2, 2 );
			len = 4;
		}
		fprintf(fperr, " w: 0x%04X\n", w );
		break;

	case 'q': 
		fprintf(fperr, "\n" );
		len = 2;
		break;

	default:
		fprintf(fperr, " Ungueltige UF\n" );
		len = 0;
		break;

	}
	evalfu( fu, &w, &lw );
	return len;
}

