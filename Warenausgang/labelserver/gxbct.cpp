#include "StdAfx.h"
#include "msgline.h"
#include "gxbct.h"
#include "gxnetapp.h"
#include "gxaedvsoc.h"

using namespace std;

#define DEFAULT_TIMEOUT 10000

CComModule _Module;

static IBCSCommunication *pDBCSwithEvents;

static char szIdentUser[] = "labelserver";

static long lStatus;
static CString szHeader, szData;
static BSTR bstrHeader, bstrData;

STDMETHODIMP CShareEvents::raw_DataArrival( BSTR bstrQueueName )
{
	char szTmp[200];
	TimeMsgLine( "Data Arrival ... " );
	for (;;)
	{
		bstrHeader = bstrData = NULL;
		// serverarr[2].pBCSConnection->m_pDBCS->Receive( &bstrHeader, &bstrData, bstrQueueName, 0, &lStatus);
		// 20070910 GK in Berlin keine Quittungsverzoegerung
		if ( quitcheck[0] != 'J' )
		{
		    pDBCSwithEvents->Receive( &bstrHeader, &bstrData, bstrQueueName, 0, &lStatus);
		}
		else
		{
		    pDBCSwithEvents->ReceiveWithoutAck( &bstrHeader, &bstrData, bstrQueueName, 0, &lStatus);
		}
		if ( lStatus == 1 )
		{
			MsgLine( "Data Arrival: Receive Error" );
			break;
		}

		szHeader = (CString) bstrHeader;
		strcpy( header, szHeader );
		strncpy( szTmp, header, 120 );
    	strcpy( szTmp + 120, " ..." );
		MsgLine( szTmp );

		szData = (CString) bstrData;
		strcpy( tbuf, szData );
		strncpy( szTmp, tbuf, 120 );
    	strcpy( szTmp + 120, " ..." );
		MsgLine( szTmp );

		hptr = header+1;

        psw_etikettyp = ETI_FEHL;

		checkfu( tbuf );

		switch ( psw_etikettyp )
		{
		case ETI_EINZEL:
		    // 20070910 GK in Berlin keine Quittungsverzoegerung
			if ( quitcheck[0] == 'J' )
			{
     		    pDBCSwithEvents->SendAcknowledge( bstrQueueName );
			}
			formErgebnis('R');
			break;
		case ETI_SUMME_1:
		    // 20070910 GK in Berlin keine Quittungsverzoegerung
			if ( quitcheck[0] == 'J' )
			{
     		    pDBCSwithEvents->SendAcknowledge( bstrQueueName );
			}
			formErgebnis('R');
			break;
		case ETI_SUMME_2:
		    // 20070910 GK in Berlin keine Quittungsverzoegerung
			if ( quitcheck[0] == 'J' )
			{
     		    pDBCSwithEvents->SendAcknowledge( bstrQueueName );
			}
			formErgebnis('R');
			break;
		case ETI_SUMME_3:
			if ( strcmp( anwender, "Boesinger" ) != 0 )
			{
                lockpaz();
			}
 		    // 20070910 GK in Berlin keine Quittungsverzoegerung
			if ( quitcheck[0] == 'J' )
			{
     		    pDBCSwithEvents->SendAcknowledge( bstrQueueName );
			}
			formErgebnis('R');
			break;
		default:
		    // 20070910 GK in Berlin keine Quittungsverzoegerung
			if ( quitcheck[0] == 'J' )
			{
     		    pDBCSwithEvents->SendAcknowledge( bstrQueueName );
			}
			break;
		}
		if ( lStatus == 0 )
		{
			break;
		}
	}
	return S_OK;
}

STDMETHODIMP CShareEvents::raw_RemoteDataArrival( BSTR bstrQueueName )
{
	TimeMsgLine( "Remote Data Arrival ... " );
	return S_OK;
}

BCSConnection::BCSConnection( void )
{
	HRESULT hr;
	MULTI_QI qi = {&IID_IBCSCommunication, NULL, 0};
	m_lRet = 1;

	TimeMsgLine( "BCS Initialisierung..." );

	hr = CoInitialize( NULL );
	if ( FAILED(hr) )
	{
		MsgHRESULT( hr, "CoInitialize" );
		m_pDBCS = NULL;
	}

	try
	{
		hr = CoCreateInstanceEx( CLSID_BCSCommunication, NULL, 
			CLSCTX_LOCAL_SERVER|CLSCTX_REMOTE_SERVER, NULL, 1, &qi );
	}
	catch (_com_error &e)
	{
		MsgLine( e.ErrorMessage() );
	}
	catch (...) 
	{
		MsgHRESULT( hr, "CoCreateInstanceEx" );
	}
	if ( FAILED(hr) )
	{
		MsgHRESULT( hr, "CoCreateInstanceEx" );
		m_pDBCS = NULL;
	}
	if ( FAILED(qi.hr) )
	{
		MsgHRESULT( qi.hr, "QueryInterface" );
		m_pDBCS = NULL;
	}

	CComObject<CShareEvents>* pSvr;
	try
	{
		hr = CComObject<CShareEvents>::CreateInstance(&pSvr);
	}
	catch (_com_error &e)
	{
		MsgLine( e.ErrorMessage() );
	}
	catch (...) 
	{
		MsgHRESULT( hr, "CreateInstance" );
	}
	if ( FAILED(hr) )
	{
		MsgHRESULT( hr, "CreateInstance" );
		m_pDBCS = NULL;
	}

	m_dwCookie		= 0;
	m_pDBCS = (IBCSCommunication*) qi.pItf;
	try
	{
		hr = AtlAdvise( m_pDBCS, pSvr->GetUnknown(), IID__ICBCSCommunicationEvents,	&m_dwCookie);
	}
	catch (_com_error &e)
	{
		MsgLine( e.ErrorMessage() );
	}
	catch (...) 
	{
		MsgHRESULT( hr, "AtlAdvise" );
	}
	if ( FAILED(hr) )
	{
		MsgHRESULT( hr, "AtlAdvise" );
		m_pDBCS = NULL;
	}

	TimeMsgLine( "... BCS Initialisierung abgeschlossen" );
}

BCSConnection::~BCSConnection( void )
{

	HRESULT hr;
	USES_CONVERSION;

	try 
	{
		hr = AtlUnadvise( m_pDBCS, IID__ICBCSCommunicationEvents, m_dwCookie );
	}
	catch (_com_error &e)
	{
		MsgLine( e.ErrorMessage() );
	}
	catch (...) 
	{
		MsgHRESULT( hr, "AtlUnadvise" );
	}
	if ( FAILED(hr) )
	{
		MsgHRESULT( hr, "AtlUnadvise" );
		m_pDBCS = NULL;
	}

	m_pDBCS->Release();
	m_pDBCS = NULL;

	try 
	{
	    CoUninitialize();
	}
	catch (_com_error &e)
	{
		MsgLine( e.ErrorMessage() );
	}
	catch (...) 
	{
		MsgLine( "CoUninitialize" );
	} 
}

void BCSConnection::mf_GetError( char *szMsg )
{
	CString szErrTxt;
	BSTR bstrErrTxt = NULL;
	long nErrNr = 0, nSystemNr = 0;

	m_pDBCS->Error( &nErrNr, &nSystemNr, &bstrErrTxt );
	szErrTxt = (CString) bstrErrTxt;

	TimeMsgLine(  "%s: Fehler: %d: System %d: %s\n",
		szMsg, nErrNr, nSystemNr, szErrTxt.GetBuffer(0) );

	//FatalError(  "%s: Fehler: %d: System %d: %s\n",
	//	szMsg, nErrNr, nSystemNr, szErrTxt.GetBuffer(0) );
}

long BCSConnection::mf_lOpen( CString szDevice )
{
	char szTmp[200];
	m_szDevice = szDevice;

	if ( m_nTelegramType == 1)
	{
		pDBCSwithEvents = m_pDBCS;
	}

	try
	{
	    m_lRet = m_pDBCS->Open( szIdentUser, m_szDevice.GetBuffer(0), m_nTelegramType, 0, 0 );
	}
	catch (...)
	{
		wsprintf( szTmp, "Verbindungaufbau zu %s gescheitert: Ret: %ld",
			m_szDevice.GetBuffer(0), m_lRet );
		mf_GetError( szTmp );
	}
	if ( m_lRet != 0 )
	{
		wsprintf( szTmp, "Verbindungaufbau zu %s gescheitert: Ret: %ld",
			m_szDevice.GetBuffer(0), m_lRet );
		mf_GetError( szTmp );
	}
	else
	{
		TimeMsgLine( "Verbindung zu %s aufgebaut, Telegrammtyp=%d",
			m_szDevice.GetBuffer(0), m_nTelegramType );
	}
	return m_lRet;
}

long BCSConnection::mf_lClose( void )
{
	if ( m_pDBCS == NULL )
	{
	    TimeMsgLine( "Verbindung zun %s ist bereits beendet", m_szDevice.GetBuffer(0) );
		return 1;
	}
	m_lRet = m_pDBCS->Close();
	return m_lRet;
}

long BCSConnection::mf_lSend( CString szHeader, CString szBuffer )
{
	char szTmp[200];
	m_szHandle = NULL;
	m_lStatus = 0;
	try
	{
		m_pDBCS->Send( szHeader.GetBuffer(0), szData.GetBuffer(0), &m_szHandle,
			DEFAULT_TIMEOUT, &m_lStatus);
	}
	catch (...)
	{
		wsprintf( szTmp, "Fehler beim Senden an %s: Status: %ld", m_szDevice.GetBuffer(0), m_lStatus );
		mf_GetError( szTmp );
	}
	if ( m_lStatus == 1 )
	{
		wsprintf( szTmp, "Fehler beim Senden an %s: Status: %ld", m_szDevice.GetBuffer(0), m_lStatus );
		mf_GetError( szTmp );
	}
	else 
	{
		TimeMsgLine( "... Gesendet an %s: Status %ld", m_szDevice.GetBuffer(0), m_lStatus );
	}
	return m_lStatus;
}

long BCSConnection::mf_lReceive( BSTR *bstrHeader, BSTR *bstrData )
{
	try
	{
		m_lStatus = m_pDBCS->Receive( bstrHeader, bstrData, m_szHandle,
			DEFAULT_TIMEOUT, &m_lStatus );
	}
	catch (...)
	{
		mf_GetError( "BCS->Receive: Error" );
	}
	return m_lStatus;
}

long BCSConnection::mf_lSendCheck( void )
{
	try
	{
		m_pDBCS->SendCheck( m_szHandle, DEFAULT_TIMEOUT, &m_lStatus );
	}
	catch (...)
	{
		mf_GetError( "BCS->SendCheck: Error" );
	}
	if ( m_lStatus == 1 )
	{
		mf_GetError( "BCS->SendCheck: Error" );
	}
	return m_lStatus;
}

int toGXBCT( char *buf, int srv, char kontrollbyte, int len )
{
	char szTmp[200];
	if ( serverarr[srv].connect_ok == 0 )
	{
		if ( serverarr[srv].pBCSConnection == NULL )
		{
		    serverarr[srv].pBCSConnection = new BCSConnection;
		}
		serverarr[srv].pBCSConnection->m_nTelegramType = serverarr[srv].telegramtype;
		serverarr[srv].pBCSConnection->mf_lOpen( serverarr[srv].shost );
		serverarr[srv].connect_ok = 1;	
	}

	if ( kontrollbyte == GXRD )
	{
		header[0] = '?';
	}
	else if ( kontrollbyte == GXWR )
	{
		header[0] = '!';
	}

	TimeMsgLine( "Data -> %s ...", serverarr[srv].shost );

	szHeader = header;
    strncpy( szTmp, header, 120 );
	strcpy( szTmp + 120, " ..." );
	MsgLine( "%s", szTmp );

	szData = buf+1;
    strncpy( szTmp, buf+1, 120 );
	strcpy( szTmp + 120, " ..." );
	MsgLine( "%s", szTmp );

	lStatus = serverarr[srv].pBCSConnection->mf_lSend( szHeader, szData );
	if ( lStatus == 1 )
	{
		return 0;
	}
	return len;
}

int fromGXBCT( char *buf, int srv )
{	
	for (;;)
	{
		if ( lStatus == 2 )
		{
			bstrHeader = bstrData = NULL;
			lStatus = serverarr[srv].pBCSConnection->mf_lReceive( &bstrHeader, &bstrData );

			szHeader = (CString) bstrHeader;
			strcpy( header, szHeader );

			szData = (CString) bstrData;
			strcpy( tbuf, szData );

			hptr = header +  1;
			checkfu( buf );

			continue;
		}
		if ( lStatus == 1 )
		{
			lStatus = serverarr[srv].pBCSConnection->mf_lSendCheck();
			if (lStatus == 1)
				break;
		}
		if ( lStatus == 0 )
		{
			break;
		}
	}
	if ( lStatus != 0 )
	{
		FatalError( "fromGXBCT: ABBRUCH" );
		serverarr[srv].connect_ok = 0;
	}
	return 1;
}
