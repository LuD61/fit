#include "stdafx.h"
#include "global.h"
#include "gxaedvsoc.h"
#include "hexasc.h"
#include "gxnetfun.h"
#include "items.h"
#include <Windows.h>
#include <Winsock.h>
#include <stdio.h>
#include <stdlib.h>

#include <time.h>

#define SB_SELF 1 /* Eigene Systembusadresse */

#define BIZERBA_OK "BIZERBA_OK"
#define BIZERBA_OK_LEN strlen("BIZERBA_OK") 
#define BIZERBA_CONNECT_OK "BIZERBA_CONNECT_OK"
#define BIZERBA_CONNECT_OK_LEN strlen("BIZERBA_CONNECT_OK") 

#define RBUFLEN 4000
#define SBUFLEN 4000

char rbuf[RBUFLEN];
char sbuf[SBUFLEN];

char szTime[80];
char szMsg[120];

typedef struct {
	int sb;
	int connect_ok;
	char shost[80];
	us sport;
	us cport;
    SOCKET soc;
} SERVER_t;

static SERVER_t serverarr[6];

time_t t;

void sendlog( int slen )
{
	time(&t);
	strftime( szTime, 80, "%H:%M:%S >>>>>>", localtime(&t) );
	fprintf(fperr,"%s\n%s\n", szTime, sbuf );

	lenMsgLD[iLineLD] = sprintf( szMsgLD[iLineLD], szTime );
	if ( ++iLineLD >= NUMLINES ) iLineLD = 0;

	lenMsgLD[iLineLD] = slen;
	if ( lenMsgLD[iLineLD] >= LINELENMAX ) lenMsgLD[iLineLD] = LINELENMAX;
	memcpy( szMsgLD[iLineLD], sbuf, lenMsgLD[iLineLD] );
	if ( ++iLineLD >= NUMLINES ) iLineLD = 0;
}

void receivelog( int rlen )
{
	time(&t);
	strftime( szTime, 80, "%H:%M:%S <<<<<<", localtime(&t) );
	fprintf(fperr,"%s\n%s\n", szTime, rbuf );

	lenMsgLD[iLineLD] = sprintf( szMsgLD[iLineLD], szTime );
	if ( ++iLineLD >= NUMLINES ) iLineLD = 0;

	lenMsgLD[iLineLD] = rlen;
	if ( lenMsgLD[iLineLD] >= LINELENMAX ) lenMsgLD[iLineLD] = LINELENMAX;
	memcpy( szMsgLD[iLineLD], rbuf, lenMsgLD[iLineLD] );
	if ( ++iLineLD >= NUMLINES ) iLineLD = 0;
}

void fehler(char *string)
{
	int err;
	char szBuffer[100];
	err = WSAGetLastError();
	wsprintf( szBuffer, "%s %d", string, err );
	fprintf(fperr,"%s \n", szBuffer );
	MessageBox (hWndLabelDevice, szBuffer, szTitle, MB_ICONEXCLAMATION | MB_OK) ;
	exit(1);
}

void fatal(char *string)
{
	KillTimer( hWndLabelDevice, ID_TIMER );
	fehler(string);
	exit(1);
}

void initsockets( void )
{
	WORD wVersionRequested;
	WSADATA wsaData;
	wVersionRequested = MAKEWORD( 2, 2 );

	if ( WSAStartup( wVersionRequested, &wsaData ) != 0 ) {
		/* Tell the user that we could not find a usable */
		/* WinSock DLL.                                  */
		fatal("WinSock DLL not found");
	}

	/* Confirm that the WinSock DLL supports 2.2.*/
	/* Note that if the DLL supports versions greater    */
	/* than 2.2 in addition to 2.2, it will still return */
	/* 2.2 in wVersion since that is the version we      */
	/* requested.                                        */

	if ( LOBYTE( wsaData.wVersion ) != 2 ||	HIBYTE( wsaData.wVersion ) != 2 ) {
		/* Tell the user that we could not find a usable */
		/* WinSock DLL.                                  */
		WSACleanup();
		fatal("Falsche WinSock DLL Version.");
	}

	/* The WinSock DLL is acceptable. Proceed. */

	strcpy( serverarr[0].shost, itmval("SERVER_HOST0") ) ;
	strcpy( serverarr[1].shost, itmval("SERVER_HOST1") ) ;
	strcpy( serverarr[2].shost, itmval("SERVER_HOST2") ) ;
	strcpy( serverarr[3].shost, itmval("SERVER_HOST3") ) ;
	strcpy( serverarr[4].shost, itmval("SERVER_HOST4") ) ;
	strcpy( serverarr[5].shost, itmval("SERVER_HOST5") ) ;

	serverarr[0].sport = atoi( itmval("SERVER_PORT0") ) ;
	serverarr[1].sport = atoi( itmval("SERVER_PORT1") ) ;
	serverarr[2].sport = atoi( itmval("SERVER_PORT2") ) ;
	serverarr[3].sport = atoi( itmval("SERVER_PORT3") ) ;
	serverarr[4].sport = atoi( itmval("SERVER_PORT4") ) ;
	serverarr[5].sport = atoi( itmval("SERVER_PORT5") ) ;

	serverarr[0].cport = atoi( itmval("CLIENT_PORT0") ) ;
	serverarr[1].cport = atoi( itmval("CLIENT_PORT1") ) ;
	serverarr[2].cport = atoi( itmval("CLIENT_PORT2") ) ;
	serverarr[3].cport = atoi( itmval("CLIENT_PORT3") ) ;
	serverarr[4].cport = atoi( itmval("CLIENT_PORT4") ) ;
	serverarr[5].cport = atoi( itmval("CLIENT_PORT5") ) ;

	serverarr[0].connect_ok = 0;
	serverarr[1].connect_ok = 0;
	serverarr[2].connect_ok = 0;
	serverarr[3].connect_ok = 0;
	serverarr[4].connect_ok = 0;
	serverarr[5].connect_ok = 0;

	serverarr[0].sb = atoi( itmval("SYSTEMBUS0") );
	serverarr[1].sb = atoi( itmval("SYSTEMBUS1") );
	serverarr[2].sb = atoi( itmval("SYSTEMBUS2") );
	serverarr[3].sb = atoi( itmval("SYSTEMBUS3") );
	serverarr[4].sb = atoi( itmval("SYSTEMBUS4") );
	serverarr[5].sb = atoi( itmval("SYSTEMBUS5") );
    
}

void socketdisconnect( int srv )
{
	if ( serverarr[srv].connect_ok )
	{
		closesocket( serverarr[srv].soc );
		serverarr[srv].soc = INVALID_SOCKET;
		serverarr[srv].connect_ok = 0;
		fprintf(fperr,"Server %s (SB=%d): disconnected.\n",
			serverarr[srv].shost, serverarr[srv].sb );
	}
}

void socketdisconnectall( void ) 
{
	int srv;
	for ( srv=0; srv<6; srv++ )
	{
		socketdisconnect( srv );
	}
}

SOCKET socketconnect( int srv )
{
	int rlen;
	int c;
	char *p; 
	struct hostent *h;			/* info about server */
	struct sockaddr_in channel;		/* holds IP address */
    SOCKET soc;
	char *shost;
	us sport, cport;

	/* bereits connected ? */
	if ( serverarr[srv].connect_ok )
		return serverarr[srv].soc;

    shost = serverarr[srv].shost;
    sport = serverarr[srv].sport;
    cport = serverarr[srv].cport;

	/* sport == 0 meint inaktiven HOST */
	if (sport == 0)
		return INVALID_SOCKET;

	h = gethostbyname(shost);
	if (!h)
	{
     	time(&t);
		strftime( szTime, 80, "%H:%M:%S", localtime(&t) );
		sprintf( szMsg,"%s : Verbindungsaufbau (gethostbyname) zu %s missglueckt.",
			szTime, shost );
		fatal(szMsg);
	}
	soc = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (soc == INVALID_SOCKET)
	{
     	time(&t);
		strftime( szTime, 80, "%H:%M:%S", localtime(&t) );
		sprintf( szMsg,"%s : Verbindungsaufbau (socket) zu %s missglueckt.",
			szTime, shost );
		fatal(szMsg);
	}
	memset( &channel, 0, sizeof(channel) );
	channel.sin_family = AF_INET;
	memcpy( &channel.sin_addr.s_addr, h->h_addr, h->h_length );
	channel.sin_port= htons( sport );
	c = connect( soc, (struct sockaddr *) &channel, sizeof(channel) );
	if (c == SOCKET_ERROR)
	{
     	time(&t);
		strftime( szTime, 80, "%H:%M:%S", localtime(&t) );
		sprintf( szMsg,"%s : Verbindungsaufbau (connect) zu %s missglueckt.",
			szTime, shost );
		fatal(szMsg);
	}  
	time(&t);
	strftime( szTime, 80, "%H:%M:%S", localtime(&t) );
	lenMsgLD[iLineLD] = sprintf( szMsgLD[iLineLD], "%s Verbunden mit %s.",
		szTime, shost );
	fprintf(fperr,"%s\n", szMsgLD[iLineLD] );
    fflush( fperr );
	if (++iLineLD == NUMLINES) iLineLD = 0;
	/* Connection is now established. */

	/* Informationstelegramm senden */

	p = sbuf;
	p += sprintf( p, "BIZERBA_TCP_INFO" );
	memset( sbuf+16, cport, 2 ) ;
	memset( sbuf+18, 1, SB_SELF ) ; 
	memset( sbuf+19, 0, 1 ) ; /* Reserve */
	memset( sbuf+20, 1, 1 ) ; /* mit Empfangssynchronisation */
	memset( sbuf+21, 1, 1 ) ; /* Hex-Ascii Konvertierung */
	memset( sbuf+22, 0, 1 ) ; /* Trennzeichen */
	memset( sbuf+23, 0, 1 ) ; /* Fluchtzeichen */
	if ( send( soc, sbuf, 24, 0 ) == SOCKET_ERROR )
	{
     	time(&t);
		strftime( szTime, 80, "%H:%M:%S", localtime(&t) );
		sprintf( szMsg,"%s : Senden von BIZERBA_TCP_INFO an %s missglueckt.",
			szTime,serverarr[srv].shost );
		fatal(szMsg);
	}
    sendlog( strlen("BIZERBA_TCP_INFO") );
	Sleep(30);
	rlen = recv( soc, rbuf, RBUFLEN, 0 );
	if ( rlen == SOCKET_ERROR )
	{
		time(&t);
		strftime( szTime, 80, "%H:%M:%S", localtime(&t) );
		sprintf( szMsg,"%s : Kein BIZERBA_CONNECT_OK von %s.",
			szTime,serverarr[srv].shost );
		fatal(szMsg);
	}
	rbuf[rlen] = '\0' ;
    receivelog( rlen );
	fflush( fperr );
	serverarr[srv].connect_ok = 1;
	serverarr[srv].soc = soc;
	return soc;
}

int toGXSB( char *buf, int srv, char kontrollbyte, int len )
{
	Sleep(30);
	size_t slen, rlen;
	SOCKET soc;

	soc = socketconnect(srv);
	if (soc == INVALID_SOCKET)
		return 0;

	checkfu( buf );
	memmove( buf + 4, buf, len );
	buf[0] = kontrollbyte;
	buf[1] = fukennung[ buf[4] & 7 ];
	buf[2] = (char) SB_SELF;
	buf[3] = (char) serverarr[srv].sb;

	len += 4; 
	slen = a2x( (char*) sbuf, buf, len ); 
	if ( send( soc, sbuf, slen, 0 ) == SOCKET_ERROR )
	{ 
     	time(&t);
		strftime( szTime, 80, "%H:%M:%S", localtime(&t) );
		sprintf( szMsg,"%s : Senden an %s missglueckt.",
			szTime,serverarr[srv].shost );
		fatal(szMsg);
		return 0;
	}
    sendlog( slen );
	Sleep(10);
	rlen = recv( soc, rbuf, BIZERBA_OK_LEN, 0 );
	if ( rlen == SOCKET_ERROR )
	{
     	time(&t);
		strftime( szTime, 80, "%H:%M:%S", localtime(&t) );
		sprintf( szMsg,"%s : Kein BIZERBA_OK von %s.",
			szTime,serverarr[srv].shost );
		fatal(szMsg);
		return 0;
	}
	if ( rlen == SOCKET_ERROR )
	{
		fehler("Empfangspufferueberlauf");
		return 0;
	}
	rbuf[rlen] = '\0' ;
	// receivelog( rlen );
	return slen;
}

int fromGXSB( char *buf, int srv )
{
	size_t rlen, len;
	SOCKET soc;

	soc = socketconnect(srv);
	if (soc == INVALID_SOCKET)
		return 0;

	Sleep(10);
	rlen = recv( soc, rbuf, RBUFLEN, 0 );
	if ( rlen == SOCKET_ERROR )
	{
     	time(&t);
		strftime( szTime, 80, "%H:%M:%S", localtime(&t) );
		sprintf( szMsg,"%s : Keine Antwort von %s.",
			szTime,serverarr[srv].shost );
		fatal(szMsg);
		return 0;
	}
	if (rlen>=RBUFLEN)
	{
		fehler("Empfangspufferueberlauf");
		return 0;
	}
	rbuf[rlen] = '\0' ;
    receivelog( rlen );
	len = x2a( buf, (char*) rbuf, rlen ); 
	checkfu( buf + 4 );
	strcpy( sbuf, BIZERBA_OK );
	if ( send( soc, sbuf, BIZERBA_OK_LEN, 0 ) == SOCKET_ERROR )
	{ 
     	time(&t);
		strftime( szTime, 80, "%H:%M:%S", localtime(&t) );
		sprintf( szMsg,"%s : Senden von BIZERBA_OK an %s missglueckt.",
			szTime,serverarr[srv].shost );
		fatal(szMsg);
		return 0;
	}
    // sendlog(BIZERBA_OK_LEN);
	return len;
}
