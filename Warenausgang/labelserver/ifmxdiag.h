#ifndef IFMXDIAGH
#define IFMXDIAGH

#ifdef _cplusplus
extern "C" {
#endif

extern char statement[];

#define WARN   1
#define NOWARN 0

/*
 * The sqlstate_err() function checks the SQLSTATE status variable to see
 * if an error or warning has occurred following an SQL statement.
 */

int sqlstate_err( void );
void disp_sqlstate_err( void );
void disp_error( char *stmt );
void disp_warning( char *stmt );
void disp_exception( char *stmt, int sqlerr_code, int warn_flg );

/*
 * The exp_chk() function calls sqlstate_err() to check the SQLSTATE
 * status variable to see if an error or warning has occurred following
 * an SQL statement. If either condition has occurred, exp_chk()
 * calls disp_sqlstate_err() to print the detailed error information.
 *
 * This function handles exceptions as follows:
 *   runtime errors - call exit(1)
 *   warnings - continue execution, returning "1"
 *   success - continue execution, returning "0"
 *   Not Found - continue execution, returning "100"
 */

long exp_chk( char *stmt, int warn_flg );

/*
 * The exp_chk2() function calls sqlstate_err() to check the SQLSTATE
 * status variable to see if an error or warning has occurred following
 * an SQL statement. If either condition has occurred, exp_chk2()
 * calls disp_exception() to print the detailed error information.
 *
 * This function handles exceptions as follows:
 *   runtime errors - continue execution, returning SQLCODE (<0)
 *   warnings - continue execution, returning one (1)
 *   success - continue execution, returning zero (0)
 *   Not Found - continue execution, returning 100
 */

long exp_chk2( char *stmt, int warn_flg );

/*
 * The whenexp_chk() function calls sqlstate_err() to check the SQLSTATE
 * status variable to see if an error or warning has occurred following
 * an SQL statement. If either condition has occurred, whenerr_chk()
 * calls disp_sqlstate_err() to print the detailed error information.
 *
 * This function is expected to be used with the WHENEVER SQLERROR
 * statement: it executes an exit(1) when it encounters a negative
 * error code. It also assumes the presence of the "statement" global
 * variable, set by the calling program to the name of the statement
 * encountering the error.
 */

int whenexp_chk( void );

#ifdef _cplusplus
}
#endif

#endif
