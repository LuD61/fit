#pragma once
#include "atlbase.h"
#include "atlstr.h"
#include "Atlcom.h"

#import "bcs.tlb" no_namespace named_guids

class CShareEvents :
	public CComObjectRootEx<CComSingleThreadModel>,
	public IDispatchImpl<_ICBCSCommunicationEvents, &IID__ICBCSCommunicationEvents, &LIBID_BCSLib>
{
public:
	BEGIN_COM_MAP(CShareEvents)
		COM_INTERFACE_ENTRY(IDispatch)
		COM_INTERFACE_ENTRY(_ICBCSCommunicationEvents)
	END_COM_MAP()

	STDMETHOD(raw_DataArrival)(BSTR szQueueName);
	STDMETHOD(raw_RemoteDataArrival)(BSTR szQueueName);
};

class BCSConnection
{
public:
	BCSConnection( void );
	~BCSConnection( void );
	void mf_GetError( char * );
	long mf_lOpen( CString );
	long mf_lClose( void );
    long mf_lSend( CString, CString );
    long mf_lReceive( BSTR *, BSTR * );
    long mf_lSendCheck( void );

public:
	DWORD m_dwCookie;
    IBCSCommunication *m_pDBCS;
	int m_nTelegramType;

private:
	CString m_szDevice;
	BSTR m_szHandle;
	long m_lStatus;
	long m_lRet;
};

int toGXBCT( char *, int, char , int );
int fromGXBCT( char *, int );
