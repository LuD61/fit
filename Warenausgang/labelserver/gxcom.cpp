#include "stdafx.h"
#include "global.h"
#include "gxcom.h"
#include "msgline.h"
#include "items.h"
#include "gxaedvsoc.h"
#include "gxbct.h"

void initservers( void )
{
	if ( gxformat != GXDATA )
	{
	    initsockets();
	}
	strcpy( serverarr[0].shost, itmval("SERVER_HOST0") ) ;
	strcpy( serverarr[1].shost, itmval("SERVER_HOST1") ) ;
	strcpy( serverarr[2].shost, itmval("SERVER_HOST2") ) ;
	strcpy( serverarr[3].shost, itmval("SERVER_HOST3") ) ;
	strcpy( serverarr[4].shost, itmval("SERVER_HOST4") ) ;
	strcpy( serverarr[5].shost, itmval("SERVER_HOST5") ) ;

	serverarr[0].sport = atoi( itmval("SERVER_PORT0") ) ;
	serverarr[1].sport = atoi( itmval("SERVER_PORT1") ) ;
	serverarr[2].sport = atoi( itmval("SERVER_PORT2") ) ;
	serverarr[3].sport = atoi( itmval("SERVER_PORT3") ) ;
	serverarr[4].sport = atoi( itmval("SERVER_PORT4") ) ;
	serverarr[5].sport = atoi( itmval("SERVER_PORT5") ) ;

	serverarr[0].cport = atoi( itmval("CLIENT_PORT0") ) ;
	serverarr[1].cport = atoi( itmval("CLIENT_PORT1") ) ;
	serverarr[2].cport = atoi( itmval("CLIENT_PORT2") ) ;
	serverarr[3].cport = atoi( itmval("CLIENT_PORT3") ) ;
	serverarr[4].cport = atoi( itmval("CLIENT_PORT4") ) ;
	serverarr[5].cport = atoi( itmval("CLIENT_PORT5") ) ;

	serverarr[0].connect_ok = 0;
	serverarr[1].connect_ok = 0;
	serverarr[2].connect_ok = 0;
	serverarr[3].connect_ok = 0;
	serverarr[4].connect_ok = 0;
	serverarr[5].connect_ok = 0;

	serverarr[0].sb = atoi( itmval("SYSTEMBUS0") );
	serverarr[1].sb = atoi( itmval("SYSTEMBUS1") );
	serverarr[2].sb = atoi( itmval("SYSTEMBUS2") );
	serverarr[3].sb = atoi( itmval("SYSTEMBUS3") );
	serverarr[4].sb = atoi( itmval("SYSTEMBUS4") );
	serverarr[5].sb = atoi( itmval("SYSTEMBUS5") );

	serverarr[0].pBCSConnection = NULL;
	serverarr[1].pBCSConnection = NULL;
	serverarr[2].pBCSConnection = NULL;
	serverarr[3].pBCSConnection = NULL;
	serverarr[4].pBCSConnection = NULL;
	serverarr[5].pBCSConnection = NULL;

	serverarr[0].telegramtype = atoi( itmval( "TELEGRAMTYP0" ) );
	serverarr[1].telegramtype = atoi( itmval( "TELEGRAMTYP1" ) );
	serverarr[2].telegramtype = atoi( itmval( "TELEGRAMTYP2" ) );
	serverarr[3].telegramtype = atoi( itmval( "TELEGRAMTYP3" ) );
	serverarr[4].telegramtype = atoi( itmval( "TELEGRAMTYP4" ) );
	serverarr[5].telegramtype = atoi( itmval( "TELEGRAMTYP5" ) );

	fprintf( fperr, "SRV  SB      SHOST  SPORT  CPORT  TTYP\n" );
	fprintf( fperr, "  0  %2d  %9s  %5d  %5d  %4d\n", serverarr[0].sb, serverarr[0].shost,
		serverarr[0].sport, serverarr[0].cport, serverarr[0].telegramtype);
	fprintf( fperr, "  1  %2d  %9s  %5d  %5d  %4d\n", serverarr[1].sb, serverarr[1].shost,
		serverarr[1].sport, serverarr[1].cport, serverarr[1].telegramtype);
	fprintf( fperr, "  2  %2d  %9s  %5d  %5d  %4d\n", serverarr[2].sb, serverarr[2].shost,
		serverarr[2].sport, serverarr[2].cport, serverarr[2].telegramtype);
	fprintf( fperr, "  3  %2d  %9s  %5d  %5d  %4d\n", serverarr[3].sb, serverarr[3].shost,
		serverarr[3].sport, serverarr[3].cport, serverarr[3].telegramtype);
	fprintf( fperr, "  4  %2d  %9s  %5d  %5d  %4d\n", serverarr[4].sb, serverarr[4].shost,
		serverarr[4].sport, serverarr[4].cport, serverarr[4].telegramtype);
	fprintf( fperr, "  5  %2d  %9s  %5d  %5d  %4d\n", serverarr[5].sb, serverarr[5].shost,
		serverarr[5].sport, serverarr[5].cport, serverarr[5].telegramtype);
}

void disconnectserver( int srv )
{
	if ( serverarr[srv].connect_ok )
	{
		if (gxformat == GXDATA)
		{
			serverarr[srv].pBCSConnection->mf_lClose();
			delete serverarr[srv].pBCSConnection;
            serverarr[srv].pBCSConnection = NULL;

		}
		else
		{
		    closesocket( serverarr[srv].soc );			
		}
		TimeMsgLine( "Verbindung zu %s wurde beendet.", serverarr[srv].shost );
		serverarr[srv].soc = INVALID_SOCKET;
		serverarr[srv].connect_ok = 0;
	}
}

void disconnectallservers( void ) 
{
	int srv;
	for ( srv=0; srv<6; srv++ )
	{
		disconnectserver( srv );
	}
}

int toGX( char *buf, int srv, char kontrollbyte, int len )
{   
	if ( simulation > 0 )
	{
	    return 0;
	}
	/* sport == 0 meint inaktiven HOST */
	if ( serverarr[srv].sport == 0)
		return 0;
	if (gxformat == GXDATA)
		return toGXBCT( buf, srv, kontrollbyte, len );
	else
		return toGXSB( buf, srv, kontrollbyte, len );
}

int fromGX( char *buf, int srv )
{
	if ( simulation > 0 )
	{
	    return 0;
	}
	if ( serverarr[srv].sport == 0)
		return 0;
	if (gxformat == GXDATA)
		return fromGXBCT( buf, srv );
	else
		return fromGXSB( buf, srv );
}
