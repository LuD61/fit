{ TABLE "fit".shopaufk row size = 164 number of columns = 53 index size = 9 }
create table "fit".shopaufk 
  (
    orders_id integer,
    kun integer,
    comments char(255), { kopftext}
    lieferdat date, 
    auf integer,
    gueltig smallint,
    stat smallint
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".shopaufk from "public";


create unique index i01shopaufk on shopaufk (orders_id);
 


