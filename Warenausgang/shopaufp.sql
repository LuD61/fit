{ TABLE "fit".shopaufp row size = 164 number of columns = 53 index size = 9 }
create table "fit".shopaufp 
  (
    orders_id integer,
     a decimal (13,0),
     stk integer,
     a_beilage decimal (13,0),
     auf integer
  ) in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".shopaufp from "public";

create unique index i01shopaufp on shopaufp (orders_id,a,a_beilage);
create index i02shopaufp on shopaufp (auf);




