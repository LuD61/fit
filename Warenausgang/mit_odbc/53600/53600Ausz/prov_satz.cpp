#include <windows.h>
#include <string.h>
#include "strfkt.h"
#include "wmask.h"
#include "prov_satz.h"

struct PROV_SATZ prov_satz, prov_satz_null;

int PROV::GetProvSatz (short mdn, 
                        short fil, 
                        long vertr, 
                        long kun,
                        char *kun_bran2,
                        double a,
                        long ag,
                        short wg,
                        short hwg)
{
	memcpy ((char *) &prov_satz, &prov_satz_null, sizeof (PROV_SATZ));
	if (GetProvSatzKunA (mdn,fil,vertr, kun, a) == 0) 
	{
		return 0;
	}
	else if (GetProvSatzKunAg (mdn,fil,vertr, kun, ag) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzKunWg (mdn,fil,vertr, kun, wg) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzKunHwg (mdn,fil,vertr, kun, hwg) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzKunAll (mdn,fil,vertr,kun) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzBranA (mdn,fil,vertr, kun_bran2, a) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzBranAg (mdn,fil,vertr, kun_bran2, ag) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzBranWg (mdn,fil,vertr, kun_bran2, wg) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzBranHwg (mdn,fil,vertr, kun_bran2, hwg) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzBranAll (mdn,fil,vertr, kun_bran2) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzAllA (mdn,fil,vertr, a) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzAllAg (mdn,fil,vertr, ag) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzAllWg (mdn,fil,vertr, wg) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzAllHwg (mdn,fil,vertr, hwg) == 0) 
	{
		 return 0;
	}
	else if (GetProvSatzAllAll (mdn,fil,vertr) == 0) 
	{
		 return 0;
	}
	return 100;
}


int PROV::GetProvSatzKunA (short mdn, 
                        short fil, 
                        long vertr, 
                        long kun,
                        double a)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,   1, 0);
	sqlin ((long *)   &vertr, 2, 0);
	sqlin ((long *)   &kun,   2, 0);
	sqlin ((double *) &a,     3, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = ? "
							"and kun_bran2 = \"0\" "
							"and hwg = 0 "
							"and wg = 0 "
							"and ag = 0 "
							"and a     = ?"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzKunAg (short mdn, 
                        short fil, 
                        long vertr, 
                        long kun,
                        long ag)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,   1, 0);
	sqlin ((long *)   &vertr, 2, 0);
	sqlin ((long *)   &kun,   2, 0);
	sqlin ((short *)  &ag,    1, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = ? "
							"and kun_bran2 = \"0\" "
							"and hwg = 0 "
							"and wg = 0 "
							"and ag = ? "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzKunWg (short mdn, 
                        short fil, 
                        long vertr, 
                        long kun,
                        short wg)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,   1, 0);
	sqlin ((long *)   &vertr, 2, 0);
	sqlin ((long *)   &kun,   2, 0);
	sqlin ((short *)  &wg,    1, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = ? "
							"and kun_bran2 = \"0\" "
							"and hwg = 0 "
							"and wg = ? "
							"and ag = 0 "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzKunHwg (short mdn, 
                        short fil, 
                        long vertr, 
                        long kun,
                        short hwg)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,   1, 0);
	sqlin ((long *)   &vertr, 2, 0);
	sqlin ((long *)   &kun,   2, 0);
	sqlin ((short *)  &hwg,   1, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = ? "
							"and kun_bran2 = \"0\" "
							"and hwg = ? "
							"and wg = 0 "
							"and ag = 0 "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzKunAll (short mdn, 
                             short fil, 
                             long vertr, 
                             long kun)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,   1, 0);
	sqlin ((long *)   &vertr, 2, 0);
	sqlin ((long *)   &kun,   2, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = ? "
							"and kun_bran2 = \"0\" "
							"and hwg = 0 "
							"and wg = 0 "
							"and ag = 0 "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}
      
int PROV::GetProvSatzBranA (short mdn, 
                        short fil, 
                        long vertr, 
                        char *kun_bran2,
                        double a)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,      1, 0);
	sqlin ((long *)   &vertr,    2, 0);
	sqlin ((char *)   kun_bran2, 0, 3);
	sqlin ((double *) &a,        3, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = 0 "
							"and kun_bran2 = ? "
							"and hwg = 0 "
							"and wg = 0 "
							"and ag = 0 "
							"and a  = ?"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzBranAg (short mdn, 
                        short fil, 
                        long vertr, 
                        char *kun_bran2,
                        long ag)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,      1, 0);
	sqlin ((long *)   &vertr,    2, 0);
	sqlin ((char *)   kun_bran2, 0, 3);
	sqlin ((short *)  &ag,       1, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = 0 "
							"and kun_bran2 = ? "
							"and hwg = 0 "
							"and wg = 0 "
							"and ag = ? "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzBranWg (short mdn, 
                        short fil, 
                        long vertr, 
                        char *kun_bran2,
                        short wg)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,      1, 0);
	sqlin ((long *)   &vertr,    2, 0);
	sqlin ((char *)   kun_bran2, 0, 3);
	sqlin ((short *)  &wg,       1, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = 0 "
							"and kun_bran2 = ? "
							"and hwg = 0 "
							"and wg = ? "
							"and ag = 0 "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzBranHwg (short mdn, 
                        short fil, 
                        long vertr, 
                        char *kun_bran2,
                        short hwg)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,      1, 0);
	sqlin ((long *)   &vertr,    2, 0);
	sqlin ((char *)   kun_bran2, 0, 3);
	sqlin ((short *)  &hwg,      1, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = 0 "
							"and kun_bran2 = ? "
							"and hwg = ? "
							"and wg = 0 "
							"and ag = 0 "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzBranAll (short mdn, 
                        short fil, 
                        long vertr, 
                        char *kun_bran2)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,      1, 0);
	sqlin ((long *)   &vertr,    2, 0);
	sqlin ((char *)   kun_bran2, 0, 3);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = 0 "
							"and kun_bran2 = ? "
							"and hwg = 0 "
							"and wg = 0 "
							"and ag = 0 "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}


int PROV::GetProvSatzAllA (short mdn, 
                           short fil, 
                           long vertr, 
                           double a)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,      1, 0);
	sqlin ((long *)   &vertr,    2, 0);
	sqlin ((double *) &a,        3, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = 0 "
							"and kun_bran2 = \"0\" "
							"and hwg = 0 "
							"and wg = 0 "
							"and ag = 0 "
							"and a  = ?"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzAllAg (short mdn, 
                           short fil, 
                           long vertr, 
                           long ag)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,      1, 0);
	sqlin ((long *)   &vertr,    2, 0);
	sqlin ((short *)  &ag,       1, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = 0 "
							"and kun_bran2 = \"0\" "
							"and hwg = 0 "
							"and wg = 0 "
							"and ag = ? "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzAllWg (short mdn, 
                           short fil, 
                           long vertr, 
                           short wg)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,      1, 0);
	sqlin ((long *)   &vertr,    2, 0);
	sqlin ((short *)  &wg,       1, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = 0 "
							"and kun_bran2 = \"0\" "
							"and hwg = 0 "
							"and wg = ? "
							"and ag = 0 "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzAllHwg (short mdn, 
                           short fil, 
                           long vertr, 
                           short hwg)
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,      1, 0);
	sqlin ((long *)   &vertr,    2, 0);
	sqlin ((short *)  &hwg,       1, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = 0 "
							"and kun_bran2 = \"0\" "
							"and hwg = ? "
							"and wg = 0 "
							"and ag = 0 "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

int PROV::GetProvSatzAllAll (short mdn, 
                             short fil, 
                             long vertr) 
{
	int dsqlstatus;
	int cursor;

	sqlin ((short *)  &mdn,      1, 0);
	sqlin ((long *)   &vertr,    2, 0);

	sqlout ((double *) &prov_satz.prov_satz, 3, 0);
	sqlout ((double *) &prov_satz.prov_sa_satz, 3, 0);

	cursor = sqlcursor ("select prov_satz, prov_sa_satz from prov_satz "
		                    "where mdn = ? "
							"and vertr = ? "
							"and kun   = 0 "
							"and kun_bran2 = \"0\" "
							"and hwg = 0 "
							"and wg = 0 "
							"and ag = 0 "
							"and a  = 0"); 
	dsqlstatus = sqlfetch (cursor);
	sqlclose (cursor);
	return dsqlstatus;
}

