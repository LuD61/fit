#ifndef _KOARTON_DEF
#define _KOARTON_DEF
#include "Text.h"

class Karton
{
   private :
 	   Text nve;
	   Text nve_palette;
	   long ls;
	   short mdn;
	   short fil;
	   double a;
	   double gew;
	   short me_einh;

   public :
	   long mhd;
	   Karton ();
	   ~Karton ();
	   void SetNve (Text&);
	   void SetNve (char *);
	   Text& GetNve ();
       void SetNvePalette (Text&);
       void SetNvePalette (char *);
	   Text& GetNvePalette ();
	   void SetLs (long);
	   long GetLs ();
	   void SetMdn (short);
	   short GetMdn ();
	   void SetFil (short);
	   short GetFil ();
	   void SetA (double);
	   double GetA();
	   void SetGew (double);
	   double GetGew();
	   void SetMeEinh (short);
	   short GetMeEinh ();
	   void SetMhd (LPSTR);
	   long GetMhd ();
       BOOL Equals (Karton&);
       BOOL operator== (Karton&);
};
#endif

