// NveDb.h: Schnittstelle f�r die Klasse CNveDb.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NVEDB_H__49564620_5079_45F7_ABAF_2AD06A9C3CED__INCLUDED_)
#define AFX_NVEDB_H__49564620_5079_45F7_ABAF_2AD06A9C3CED__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "traponve.h"
#include "trapopspm.h"
#include "ptab.h"
#include "ptabtable.h"

#define ME_EINH_LEER "me_einh_leer"

class CNveDb  
{
private:
	TRAPONVE_CLASS  m_TrapoNve;
	TRAPOPSPM_CLASS m_TrapoPsPm;
	PTAB_CLASS      m_Ptab;
	CPtabTable      m_PtabTable;
	int             m_MaxCursor;  
	long            m_Lfd;
	int             m_LsCursor;
	int             m_DeleteLsCursorNve;
	int             m_DeleteLsCursorPsPm;
	BOOL            m_Error;
	int             m_ErrorNumber;
	static const LPSTR    
		            m_ErrorStrings[];    

	void PrepareLsCursor ();
public:
	BOOL Error ()
	{
		return m_Error;
	}

	void set_Mdn (short mdn)
	{
		m_TrapoNve.traponve.mdn = mdn;
		m_TrapoPsPm.trapopspm.mdn = mdn;
	}

	short Mdn ()
	{
		return m_TrapoNve.traponve.mdn;
	}

	void set_Fil (short fil)
	{
		m_TrapoNve.traponve.fil = fil;
		m_TrapoPsPm.trapopspm.fil = fil;
	}

	short Fil ()
	{
		return m_TrapoNve.traponve.fil;
	}

	void set_Ls (long ls)
	{
		m_TrapoNve.traponve.ls = ls;
		m_TrapoPsPm.trapopspm.ls = ls;
	}

	long Ls ()
	{
		return m_TrapoNve.traponve.ls;
	}

	void set_BlgTyp (LPSTR blg_typ)
	{
		strcpy (m_TrapoNve.traponve.blg_typ,blg_typ);
		strcpy (m_TrapoPsPm.trapopspm.blg_typ,blg_typ);
	}

	const LPSTR BlgTyp ()
	{
		return m_TrapoNve.traponve.blg_typ;
	}

	void set_Lfd (long lfd)
	{
		m_TrapoNve.traponve.lfd = lfd;
		m_TrapoPsPm.trapopspm.lfd = lfd;
	}

	long Lfd ()
	{
		return m_TrapoNve.traponve.lfd;
	}


	void set_Nve (LPSTR nve)
	{
		strcpy (m_TrapoNve.traponve.nve, nve);
	}

	const LPSTR Nve ()
	{
		return m_TrapoNve.traponve.nve;
	}

	void set_A (double a)
	{
		m_TrapoNve.traponve.a = a;
	}

	long A ()
	{
		return m_TrapoNve.traponve.a;
	}

	void set_Pm (double pm)
	{
		m_TrapoPsPm.trapopspm.pm = pm;
	}

	long Pm ()
	{
		return m_TrapoPsPm.trapopspm.pm;
	}

	void set_PmZahl (double pmzahl)
	{
		m_TrapoPsPm.trapopspm.pmzahl = pmzahl;
	}

	long PmZahl ()
	{
		return m_TrapoPsPm.trapopspm.pmzahl;
	}

	void set_Ps1 (double ps1)
	{
		m_TrapoPsPm.trapopspm.ps1 = ps1;
	}

	long Ps1 ()
	{
		return m_TrapoPsPm.trapopspm.ps1;
	}

	void set_Ps1Zahl (double ps1zahl)
	{
		m_TrapoPsPm.trapopspm.ps1zahl = ps1zahl;
	}

	long Ps1Zahl ()
	{
		return m_TrapoPsPm.trapopspm.ps1zahl;
	}

	void set_Ps2 (double ps2)
	{
		m_TrapoPsPm.trapopspm.ps2 = ps2;
	}

	long Ps2 ()
	{
		return m_TrapoPsPm.trapopspm.ps1;
	}

	void set_Ps2Zahl (double ps2zahl)
	{
		m_TrapoPsPm.trapopspm.ps2zahl = ps2zahl;
	}

	long Ps2Zahl ()
	{
		return m_TrapoPsPm.trapopspm.ps2zahl;
	}

	void set_Ps3 (double ps3)
	{
		m_TrapoPsPm.trapopspm.ps3 = ps3;
	}

	long Ps3 ()
	{
		return m_TrapoPsPm.trapopspm.ps3;
	}

	void set_Ps3Zahl (double ps3zahl)
	{
		m_TrapoPsPm.trapopspm.ps3zahl = ps3zahl;
	}

	long Ps3Zahl ()
	{
		return m_TrapoPsPm.trapopspm.ps3zahl;
	}
	void set_Ps4 (double ps4)
	{
		m_TrapoPsPm.trapopspm.ps4 = ps4;
	}

	long Ps4 ()
	{
		return m_TrapoPsPm.trapopspm.ps4;
	}

	void set_Ps4Zahl (double ps4zahl)
	{
		m_TrapoPsPm.trapopspm.ps4zahl = ps4zahl;
	}

	long Ps4Zahl ()
	{
		return m_TrapoPsPm.trapopspm.ps4zahl;
	}


	CNveDb();
	virtual ~CNveDb();
	long GetMaxLfd ();
	void DeleteLs ();
	void InitNve ();
	void InitPsPm ();
	void UpdateNve ();
	void UpdatePsPm ();
	void ReadFirst ();
	void Read ();
	void FillPtab ();
	void Start ();
	PTABN *GetNext ();
	double GetArticle (LPSTR Name);
	const LPSTR GetErrorText ();
};

#endif // !defined(AFX_NVEDB_H__49564620_5079_45F7_ABAF_2AD06A9C3CED__INCLUDED_)
