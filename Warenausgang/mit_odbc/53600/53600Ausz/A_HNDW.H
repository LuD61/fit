struct A_HNDW {
   double    a;
   char      a_grnd[2];
   long      a_krz;
   double    a_leer;
   double    a_pfa;
   char      a_pfa_kz[2];
   short     anz_reg_eti;
   short     anz_theke_eti;
   short     anz_waren_eti;
   short     delstatus;
   short     fil;
   double    gew_bto;
   short     herk_land;
   char      hdkl[5];
   double    inh;
   char      mwst_ueb[2];
   short     me_einh_kun;
   short     mdn;
   char      pr_ausz[2];
   char      pr_man[2];
   char      pr_ueb[2];
   char      reg_eti[2];
   short     sg1;
   short     sg2;
   char      smt[2];
   double    tara;
   char      theke_eti[2];
   long      verk_beg;
   char      verk_art[2];
   char      vk_typ[2];
   char      vpk[2];
   char      waren_eti[2];
};

struct A_EIG {
   double    a;
   long      a_krz;
   short     anz_theke_eti;
   long      bem_offs;
   short     delstatus;
   short     fil;
   double    gew_bto;
   double    inh;
   long      mat;
   short     mdn;
   short     me_einh_ek;
   char      mwst_ueb[2];
   char      pr_ausz[2];
   char      pr_man[2];
   char      pr_ueb[2];
   char      rez[9];
   short     sg1;
   short     sg2;
   double    tara;
   char      theke_eti[2];
   char      verk_art[2];
   long      verk_beg;
   char      waren_eti[2];
   short     anz_waren_eti;
};

struct A_EIG_DIV {
   double    a;
   long      a_krz;
   short     anz_reg_eti;
   short     anz_theke_eti;
   short     anz_waren_eti;
   short     delstatus;
   short     fil;
   double    gew_bto;
   double    inh;
   short     mdn;
   short     me_einh_ek;
   char      mwst_ueb[2];
   char      pr_ausz[2];
   char      pr_man[2];
   char      pr_ueb[2];
   char      reg_eti[2];
   char      rez[9];
   short     sg1;
   short     sg2;
   double    tara;
   char      theke_eti[2];
   char      verk_art[2];
   long      verk_beg;
   char      vpk_kz[2];
   char      waren_eti[2];
};

struct A_PFA {
   double    a;
   long      a_krz;
   short     delstatus;
   short     fil;
   short     mdn;
   char      mwst_ueb[2];
   long      verk_beg;
   short     sg1;
   short     sg2;
   char      verk_art[2];
};

struct A_LEER {
   double    a;
   long      a_krz;
   double    a_pfa;
   short     delstatus;
   short     fil;
   short     mdn;
   char      mwst_ueb[2];
   long      verk_beg;
   short     sg1;
   short     sg2;
   char      verk_art[2];
};

extern struct A_HNDW a_hndw, a_hndw_null;
extern struct A_EIG a_eig, a_eig_null;
extern struct A_EIG_DIV a_eig_div, a_eig_div_null;
extern struct A_PFA a_pfa, a_pfa_null;
extern struct A_LEER a_leer, a_leer_null;

class HNDW_CLASS
{
       private :
            short cursor_a_hndw;
            short cursor_a_eig;
            short cursor_a_eig_div;
            short cursor_a_pfa;
            short cursor_a_leer;

            void prepare_hndw (void);       
            void prepare_eig (void);       
            void prepare_eig_div (void);       
            void prepare_pfa (void);       
            void prepare_leer (void);       
       public:
           HNDW_CLASS ()
           {
                    cursor_a_hndw     = -1;
                    cursor_a_eig      = -1;
                    cursor_a_eig_div  = -1;
                    cursor_a_pfa      = -1;
                    cursor_a_leer     = -1;
           }
           int lese_a_hndw (double);
           int lese_a_hndw (void);
           int lese_a_eig (double);
           int lese_a_eig (void);
           int lese_a_eig_div (double);
           int lese_a_eig_div (void);
           int lese_a_pfa (double);
           int lese_a_pfa (void);
           int lese_a_leer (double);
           int lese_a_leer (void);

           void close_a_hndw (void);
           void close_a_eig (void);
           void close_a_eig_div (void);
           void close_a_pfa (void);
           void close_a_leer (void);
};
