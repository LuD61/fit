/***************************************************************************/
/* Programmname :  Log.h                                                   */
/*-------------------------------------------------------------------------*/
/* Funktion :  Klasse CToken                                               */
/*-------------------------------------------------------------------------*/
/* Revision : 1.0    Datum : 07.06.04   erstellt von : W. Roth             */
/*                   allgemneine Klasse zum Zerlegen von Strings           */
/*                                                                         */    
/*-------------------------------------------------------------------------*/
/* Aufruf :                                                                */
/* Funktionswert :                                                         */
/* Eingabeparameter :                                                      */
/*                                                                         */
/* Ausgabeparameter :                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
#pragma once
#ifndef _DLOG_DEF
#define _DLOG_DEF
#include "Text.h"

#ifndef MAXLINES
#define MAXLINES 1000
#endif

#define CString Text

class CLog
{
public:
	CString LogName;
	FILE *fp;
	CString Line;
	BOOL Active;

	CLog(void);
	~CLog(void);

    virtual void Write (CString&);
	virtual void Write ();
	virtual void Open ();
	virtual void Close ();
    void Sysdate (char *);
    void Systime (char *);
};

class CContLog : public CLog
{
public:
	virtual void Open ();
};

class CRingLog : public CLog
{
public:
	long maxrows;
	long row;
	CRingLog () : CLog () 
	{
		maxrows = 1000;
		row = 0;
	}
	virtual void Open ();
    virtual void Write (CString&);
	virtual void Write ();
};

class CFifoLog  : public CLog
{
public:
    virtual void Write (CString& text);
    void WriteFirstLine (CString& text);
};


#endif
