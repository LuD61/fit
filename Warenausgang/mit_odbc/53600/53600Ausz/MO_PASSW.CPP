/* *********************************************************************** */
/*                                                                         */
/*                        Eingabe der Benutzerkennung                      */
/*                                                                         */
/*                        Wilhelm Roth 27.04.1998                          */
/*                                                                         */
/* *********************************************************************** */


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include <math.h>
#include "strfkt.h"
#include "mo_menu.h"
#include "mo_curso.h"
#include "wmask.h"
#include "textblock.h" 
#include "pers.h"

extern BOOL ColBorder;
extern void disp_messG (char *, int);
extern HWND     hMainWindow;
LONG FAR PASCAL PassProc(HWND,UINT,WPARAM,LPARAM);

static char pers_nam [9];
static char persnr [13];

static int DestroyOk = 0;
static int PassWord = 1;

static int lastfield (void); 

static TEXTBLOCK *ptextblock = NULL;

static HWND PassWindow;

int FHS = 0;

static mfont fitfont = {"Courier New", 150, 0, 1,
                                       RGB (0, 255, 255),
                                       BLUECOL,
                                       0};

static mfont fhsfont = {"Courier New", 150, 0, 1,
                                       BLACKCOL,
                                       LTGRAYCOL,
                                       0};

static mfont passfont;

static field _benutzer [] = {
"Benutzer      ",  14, 0, 1, 2, 0,  "", DISPLAYONLY, 0, 0, 0,
"Passwort      ",  14, 0, 3, 2, 0,  "", DISPLAYONLY, 0, 0, 0,
pers_nam,          12, 0, 1, 17, 0, "", NORMAL, 0, testkeys, 0,
persnr,              16, 0, 3, 17, 0, "", PASSWORD, 0, lastfield, 0,
" OK ",             4, 2, 7,10,  0, "", BUTTON, 0, 0, KEY12,
" Abbrechen ",     11, 2, 7,17,  0, "", BUTTON, 0, 0, KEY5};

static form benutzer = {6, 0, 0, _benutzer, 0,0,0,0, &passfont};

static class MENUE_CLASS menue_class;

void SetPassWordFlag (int flag)
{
	PassWord = flag;
}

char *GetPers (char *benutzer)
/**
Benutzername holen.
**/
{
            if (benutzer == NULL)
            {
                      return (persnr);
            }
            strcpy (benutzer, persnr);
            return (benutzer);
}

static int test_benutzer (void)
/**
Eingegebenen Benutzer pruefen.
**/
{
	        
            if (PassWord == 1)
			{
                     menue_class.Lesesys_ben_pers (pers_nam);
			}
			else
			{
                     menue_class.Lesesys_ben (pers_nam, persnr);
			}
            if (sqlstatus == 0)
            {
                          return TRUE;
            }
            return FALSE;
}

static int dokey5 ()
/**
Aktion bei Taste F5
**/
{
            break_enter ();
            return TRUE;
}

int lastfield ()
/**
Letztes Eingabefeld bearbeiten.
**/
{
           if (testkeys ()) return 0;

           GetWindowText (benutzer.mask [2].feldid,
                          benutzer.mask [2].feld,
                          benutzer.mask [2].length);
           GetWindowText (benutzer.mask [3].feldid,
                          benutzer.mask [3].feld,
                          benutzer.mask [3].length);
           if (test_benutzer () == FALSE)
           {
                        disp_mess ("Benutzer oder Passwort falsch", 0);
                        SetCurrentField (0);
                        return 0;
           }
           syskey = KEY12;
           break_enter ();
           return 1;
}

static void PassBorder (HDC hdc, RECT *rect, COLORREF BkColor)
/**
Rahmen um Fenster.
**/
{
	     int i;
		 int BS;

         static HPEN hPenBlack = NULL;
         static HPEN hPenWhite;
         static HPEN hPenGray;

         static HPEN hPenLtBlue;
         static HPEN hPenBlue;

         static HPEN hPenLtRed;
         static HPEN hPenRed;

         static HPEN hPenLtGreen;
         static HPEN hPenGreen;

		 int blue, red, green;

		 if (FHS || (ColBorder == FALSE)) return; 
         blue = GetBValue (BkColor);
         red  = GetRValue (BkColor);
 		 green = GetGValue (BkColor);

		 BS = 10;
         if (hPenBlack == NULL)
         {
                     hPenBlack   = CreatePen (PS_SOLID, 1, BLACKCOL);
                     hPenWhite   = CreatePen (PS_SOLID, 1, WHITECOL);
                     hPenGray    = CreatePen (PS_SOLID, 1, GRAYCOL);

                     hPenLtBlue  = CreatePen (PS_SOLID, 1, RGB (0, 120, 255));
                     hPenBlue    = CreatePen (PS_SOLID, 1, RGB ( 0, 0, 120));

                     hPenLtRed  = CreatePen (PS_SOLID, 1, RGB (255, 120, 0));
                     hPenRed    = CreatePen (PS_SOLID, 1, RGB ( 120, 0, 0));

                     hPenLtGreen  = CreatePen (PS_SOLID, 1, RGB (120, 255, 0));
                     hPenGreen    = CreatePen (PS_SOLID, 1, RGB ( 0, 120, 0));
         }
         if (hPenGray == NULL)
         {
                          disp_mess ("Fehler bei CreatePen", 2);
                          ExitProcess (1);
         }

		 if (BkColor == BLUECOL)
		 {
                    SelectObject (hdc, hPenBlue);

			 for (i = 1; i < BS; i ++)
			 {
                    MoveToEx (hdc, rect->right - i, 2, NULL);
                    LineTo (hdc, rect->right - i, rect->bottom - 3);
			 }


			 for (i = 1; i < BS; i ++)
			 {
                    MoveToEx (hdc, 2, rect->bottom - i, NULL);
                    LineTo (hdc, rect->right, rect->bottom - i);
			 }

		 }

		 if (BkColor == BLUECOL)
		 {
			 for (i = 0; i < BS; i ++)
			 {
                   SelectObject (hdc, hPenLtBlue);
                   MoveToEx (hdc, i, 1, NULL);
                   LineTo (hdc, i, rect->bottom - (i + 1));
			 }


			 for (i = 0; i < BS; i ++)
			 {
                   MoveToEx (hdc, 1, i, NULL);
                   LineTo (hdc, rect->right - (i + 1), i);
			 }
		 }
}

static HWND OpenPassWindow (int rows, int columns, HANDLE hInst, HWND hWnd)
/**
Fenster fuer Benutzereingabe oeffnen.
**/
{
           static int registered = 0;
           WNDCLASS wc;
           int x, y, cx, cy;
           int scx, scy;
           HWND Window;
           HWND AktivWindow;
           HDC hdc;
           TEXTMETRIC tm;
           HFONT hFont, oldFont;


           if (registered == 0)
           {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
                   wc.lpfnWndProc   =  PassProc;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hInst;
                   wc.hIcon         =  LoadIcon (hInst, "FITICON");
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   if (FHS)
                   {
                        memcpy (&passfont, &fhsfont, sizeof (mfont));
                        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   }
                   else
                   {
                        memcpy (&passfont, &fitfont, sizeof (mfont));
                        wc.hbrBackground =  CreateSolidBrush (BLUECOL);
                   }
                   wc.lpszMenuName  =  "";
                   wc.lpszClassName =  "Benutzer";

                   RegisterClass(&wc);
                   registered = 1;
           }

           AktivWindow = GetActiveWindow ();
           hdc = GetDC (AktivWindow);
           if (benutzer.font)
           {
                spezfont (benutzer.font);
                hFont = EzCreateFont (hdc, "Courier New",
                                    100,
                                    0,
                                    0,
                                    TRUE);
                oldFont = SelectObject (hdc,hFont);
           }

           GetTextMetrics (hdc, &tm);
           ReleaseDC (AktivWindow, hdc);
           cy = rows * (tm.tmHeight + tm.tmHeight / 2);
           cx = columns * tm.tmAveCharWidth;
           scx = GetSystemMetrics (SM_CXSCREEN);
           scy = GetSystemMetrics (SM_CYSCREEN);
           x = (scx - cx) / 2;
           y = (scy - cy) / 2;
           SelectObject (hdc, oldFont);

		   if (FHS || (ColBorder == FALSE))
		   {
                   Window = CreateWindow ("Benutzer",
                                 "Benutzeranmeldung",
                                 WS_DLGFRAME | WS_CAPTION | WS_VISIBLE |
                                 WS_MINIMIZEBOX,
                                 x, y,
                                 cx, cy,
                                 hWnd,
                                 NULL,
                                 hInst,
                                 NULL);
		   }
		   else
		   {
                   Window = CreateWindow ("Benutzer",
                                 "Benutzeranmeldung",
                                 WS_VISIBLE |
                                 WS_POPUP,
                                 x, y,
                                 cx, cy,
                                 hWnd,
                                 NULL,
                                 hInst,
                                 NULL);
		   }
           ShowWindow (Window, SW_SHOW);
           UpdateWindow (Window);
           return (Window);
}


BOOL EnterPassw (HANDLE hInst, HWND hWnd)
/**
Benutzername und Passwort eingeben.
**/
{
            DestroyOk = 0; 
            set_fkt (dokey5, 5);
            pers_nam[0] = (char) 0;
            persnr[0] = (char) 0;
            PassWindow = OpenPassWindow (8, 37, hInst, hWnd);
            SetMouseLock (TRUE);
            enter_form (PassWindow, &benutzer, 0, 0);
            SetMouseLock (FALSE);

//            if (! DestroyOk)
            {
                        DestroyWindow (PassWindow);
            }
            if (syskey == KEY5)
            {
                        return FALSE;
            }
            menue.mdn  = sys_ben.mdn;
            menue.fil  = sys_ben.fil;
            strcpy (menue.pers,sys_ben.pers);
            return TRUE;
}


void PutPersNam (void)
/**
Benutzername in $TMPPATH/user.akt speichern.
**/
{
            char *tmp;
            char buffer [512];
            FILE *fp;

            tmp = getenv ("TMPPATH");
            if (tmp == NULL) return;

            sprintf (buffer, "%s\\53600.akt", tmp);
            fp = fopen (buffer, "w");
            if (fp == NULL) return;

            fprintf (fp, "%s\n",  sys_ben.pers_nam);
            fprintf (fp, "%ld\n",   GetCurrentThreadId ()); 
            fprintf (fp, "%d\n",   GetCurrentProcessId ()); 
            fclose (fp);
}


BOOL TestUserAkt (void)
/**
Test, ob 53600 schon laeuft.
**/
{ 
            char *tmp;
            char buffer [512];
            DWORD Id;
            int Pid;
            FILE *fp;
            extern void disp_messG (char *, int);

            tmp = getenv ("TMPPATH");
            if (tmp == NULL) return TRUE;

            sprintf (buffer, "%s\\53600.akt", tmp);
            fp = fopen (buffer, "r");
            if (fp == NULL) return TRUE;

            if (fgets (buffer, 511, fp) == NULL) 
            {
                  fclose (fp);
                  return TRUE;
            }
            if (fgets (buffer, 511, fp) == NULL)
            {
                  fclose (fp);
                  return TRUE;
            }
            cr_weg (buffer);
            Id = atol (buffer);
            if (fgets (buffer, 511, fp))
            {
                  cr_weg (buffer);
                  Pid = atoi (buffer);
            }
            fclose (fp);
            HANDLE pid = OpenProcess (PROCESS_ALL_ACCESS, TRUE, Pid);
            if (pid != NULL)
            {
                CloseHandle (pid);
            }
            else
            {
                return TRUE;
            }
//			disp_mess ("Das Programm l�uft schon", 2);
            if (PostThreadMessage (Id, WM_USER + 98, 0l, 0l) == FALSE)
            {
                return TRUE;
            }
            return FALSE;
}


BOOL TestUserKill(LPSTR lpszCmdLine)
/**
Test, ob 53600 schon laeuft.
**/
{ 
            char *tmp;
            char buffer [512];
            DWORD Id;
            FILE *fp;

            int anz = wsplit (lpszCmdLine, " ");
            if (anz == 0)
            {
                return TRUE;
            }

            if (strcmp (wort[0], "-k") != 0 &&
                strcmp (wort[0], "-kill") != 0)
            {
                return TRUE;
            }

            tmp = getenv ("TMPPATH");
            if (tmp == NULL) return TRUE;

            sprintf (buffer, "%s\\53600.akt", tmp);
            fp = fopen (buffer, "r");
            if (fp == NULL) return TRUE;

            if (fgets (buffer, 511, fp) == NULL) 
            {
                  fclose (fp);
                  return TRUE;
            }
            if (fgets (buffer, 511, fp) == NULL)
            {
                  fclose (fp);
                  return TRUE;
            }
            fclose (fp);
            cr_weg (buffer);

            Id = atol (buffer);
            if (PostThreadMessage (Id, WM_USER + 99, 0l, 0l) == FALSE)
            {
                return TRUE;
            }
            return FALSE;
}

void DelUserAkt (void)
{
            char *tmp;
            char buffer [512];
            tmp = getenv ("TMPPATH");
            if (tmp == NULL) return;
            sprintf (buffer, "%s\\53600.akt", tmp);
            unlink (buffer);
}




LONG FAR PASCAL PassProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
        static int SendChild = 0;

        GetCursorPos (&mousepos);

        switch(msg)
        {
		      case WM_PAINT :
			        break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY12)
                    {
                            if (lastfield () == FALSE)
                            {
                                     SetCurrentFocus (2);
                                     return 0;
                            }
                            return 0;
                    }
                    else if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            break_enter ();
                            return 0;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


LONG FAR PASCAL EntPassProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
	    if (ptextblock != NULL) 
		{
			switch(msg)
			{
              case WM_PAINT :
				    ptextblock->OnPaint (hWnd, msg, wParam, lParam);
					break;

              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
				    ptextblock->OnButton (hWnd, msg, wParam, lParam);
                    return TRUE;
			}
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


BOOL EnterPasswTouch (HANDLE hInst, HWND hWnd)
/**
Benutzername und Passwort eingeben.
**/
{
	        double scrfx, scrfy;
			int xfull, yfull;

            xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
            yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

			scrfx = (double) ((double) xfull / 800);
			scrfy = (double) ((double) yfull / 600);

            DestroyOk = 0; 
            set_fkt (dokey5, 5);
            pers_nam[0] = (char) 0;
            persnr[0] = (char) 0;
			LockWindowUpdate (hWnd);
            ptextblock = new TEXTBLOCK;
            ptextblock->MainInstance (hInst, hMainWindow);  
            ptextblock->ScreenParam (scrfx, scrfy);
            ptextblock->SetUpshift (FALSE);
            ptextblock->EnterTextBox (hWnd, "Bediener", pers_nam, 8, "", EntPassProc);
        
            if (syskey == KEY5)
            {
				        LockWindowUpdate (NULL);
	                    delete ptextblock;
		                ptextblock = NULL;
                        return FALSE;
            }
			if (PassWord == 2)
			{
                        ptextblock->EnterTextBox (hWnd, "Passwort", persnr, 12, "", EntPassProc);
			}
		    delete ptextblock;
		    ptextblock = NULL;
			LockWindowUpdate (NULL);
			SetActiveWindow (hMainWindow);
			EnableWindow (hMainWindow, TRUE);
            if (syskey == KEY5)
            {
                        return FALSE;
            }

            if (test_benutzer () == FALSE)
            {
				        ShowWindow (hWnd, SW_HIDE); 
                        disp_messG ("Benutzer oder Passwort falsch", 0);
                        SetCurrentField (0);
                        return 0;
            }

            menue.mdn  = sys_ben.mdn;
            menue.fil  = sys_ben.fil;
            strcpy (menue.pers,sys_ben.pers);
            return TRUE;
}


BOOL EnterPersTouch (HANDLE hInst, HWND hWnd)
/**
Benutzername und Passwort eingeben.
**/
{
	        double scrfx, scrfy;
			int xfull, yfull;

            xfull = GetSystemMetrics (SM_CXFULLSCREEN);        
            yfull = GetSystemMetrics (SM_CYFULLSCREEN);        

			scrfx = (double) ((double) xfull / 800);
			scrfy = (double) ((double) yfull / 600);

            DestroyOk = 0; 
            set_fkt (dokey5, 5);
            pers_nam[0] = (char) 0;
            persnr[0] = (char) 0;
			LockWindowUpdate (hWnd);
            ptextblock = new TEXTBLOCK;
            ptextblock->MainInstance (hInst, hMainWindow);  
            ptextblock->ScreenParam (scrfx, scrfy);
            ptextblock->SetUpshift (FALSE);
            ptextblock->EnterTextBox (hWnd, "Kommissionierer", persnr, 13, "", EntPassProc);
        
            if (syskey == KEY5)
            {
				        LockWindowUpdate (NULL);
	                    delete ptextblock;
		                ptextblock = NULL;
                        return FALSE;
            }
/*
			if (PassWord == 2)
			{
                        ptextblock->EnterTextBox (hWnd, "Passwort", pers, 12, "", EntPassProc);
			}
*/
		    delete ptextblock;
		    ptextblock = NULL;
			LockWindowUpdate (NULL);
			SetActiveWindow (hMainWindow);
			EnableWindow (hMainWindow, TRUE);
            if (syskey == KEY5)
            {
                        return FALSE;
            }

			PERS_CLASS Pers;
			strcpy (pers.pers, persnr);
			if (Pers.dbreadfirst () == 100 || pers.taet_kz[0] != 'K')
			{
                        disp_messG ("Kommissionierer nicht gefunden", 0);
                        SetCurrentField (0);
                        return 0;
            }
            return TRUE;
}
