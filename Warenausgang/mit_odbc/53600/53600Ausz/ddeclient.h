#include <ddeml.h>

#define DDE_TIMEOUT 10000
#define WM_USER_ERGEBNIS (WM_USER + 2)
#define WM_USER_STOP (WM_USER + 3)
#define WM_USER_MESSAGE (WM_USER + 4)

class DdeClient
{
   private :
         char Service [80];
         char Topic [80];
		 char Item [80];
		 HCONV hConv;
         static DWORD idInst;
         static HWND hWnd;
		 static char Ergebnis[];
		 static char MText[];
   public : 		 
	     DdeClient (char *, char *);
	     ~DdeClient ();
		 BOOL Init (HWND);
		 BOOL Start (char *);
		 void TerminateServer ();
		 void Stop (char *);
		 static void GetErgebnis (HDDEDATA);
		 static void Message (HDDEDATA);
		 static HDDEDATA CALLBACK DdeCallback (UINT, UINT, HCONV, HSZ, HSZ, 
				                			   HDDEDATA, DWORD, DWORD);
};

