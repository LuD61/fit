// WDialog.h: Schnittstelle f�r die Klasse WDialog.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WDIALOG_H__4A6A6451_6C63_4346_859A_6BB4E64FACC0__INCLUDED_)
#define AFX_WDIALOG_H__4A6A6451_6C63_4346_859A_6BB4E64FACC0__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>

class WDialog  
{
private:
	DWORD IDD;
protected:
	HWND m_hWnd;
	HWND m_Parent;
	HBRUSH m_hBrush;
	HINSTANCE m_hInstance;
	static WDialog *m_DlgInstance;
	virtual bool OnInitDialog ();
	virtual HBRUSH OnCtlColor (HDC gDC, HWND hWnd, UINT nCtlColor); 
	static BOOL CALLBACK DialogProc( HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam); 
public:
	enum FONT_ATTRIB
	{
		ATTR_BOLD = 1,
		ATTR_ITALIC = 2,
		ATTR_UNDERLINE = 4,
		ATTR_STRIKEOUT = 8,
	};
	void SethWnd (HWND hWnd)
	{
		m_hWnd = hWnd;
	}

	void SetParent (HWND Parent)
	{
		m_Parent = Parent;
	}

	HWND GetParent ()
	{
		return m_Parent;
	}

	WDialog(DWORD IDD);
	virtual ~WDialog();
	int DoModal ();
	void SetControlFont (DWORD Id, HFONT font);
	HFONT CreateFont (HDC hdc, char * szFaceN, int iDeciPtH,
                      int iDeciPtW=0, int iAttrib=0, BOOL fLogRes=FALSE);
};

#endif // !defined(AFX_WDIALOG_H__4A6A6451_6C63_4346_859A_6BB4E64FACC0__INCLUDED_)
