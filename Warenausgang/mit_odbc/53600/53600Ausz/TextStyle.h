// TextStyle.h: Schnittstelle f�r die Klasse CTextStyle.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXTSTYLE_H__79BFD2A7_146C_424B_8F12_DA2FD5FD9F51__INCLUDED_)
#define AFX_TEXTSTYLE_H__79BFD2A7_146C_424B_8F12_DA2FD5FD9F51__INCLUDED_

#include<windows.h>
#include<winuser.h>
#include <string>

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#define EZ_ATTR_BOLD 1
#define EZ_ATTR_ITALIC 2
#define EZ_ATTR_UNDERLINE 4
#define EZ_ATTR_STRIKEOUT 8

using namespace std;

class CTextStyle  
{
public:
	enum FIT_SIZE
	{
		BigFit = 1500,
        NormalFit = 1300,
		SmallFit = 1000
	};

	enum TEXT_SIZE
	{
		BigText = 600,
        NormalText = 500,
		SmallText = 300
	};

private:
	FIT_SIZE m_FitSize;
	TEXT_SIZE m_TextSize;
	string m_Fit1;
	string m_Fit2;
	int m_xFit1;
	int m_xFit2;
	int m_yFit1;
	int m_yFit2;
	int cxScreen;
	int cyScreen;
	int m_Space1;
	COLORREF m_TextColor1;
	COLORREF m_TextColor2;

public:
	FIT_SIZE GetFitSize ()
	{
		return m_FitSize;
	}

	void SetFitSize (FIT_SIZE FitSize)
	{
		m_FitSize = FitSize;
	}

	TEXT_SIZE GetTextSize ()
	{
		return m_TextSize;
	}

	void SetTextSize (TEXT_SIZE TextSize)
	{
		m_TextSize = TextSize;
	}

	const char *GetFit1 ()
	{
		return m_Fit1.c_str ();
	}

	void SetFit1 (char *Fit1)
	{
		m_Fit1 = Fit1;
	}

	const char *GetFit2 ()
	{
		return m_Fit2.c_str ();
	}

	void SetFit2 (char *Fit2)
	{
		m_Fit2 = Fit2;
	}

	int GetYFit1 ()
	{
		return m_yFit1;
	}

	void SetYFit1 (int yFit1)
	{
		m_yFit1 = yFit1;
	}


	int GetYFit2 ()
	{
		return m_yFit2;
	}

	void SetYFit2 (int yFit2)
	{
		m_yFit2 = yFit2;
	}

	int GetXFit1 ()
	{
		return m_xFit1;
	}

	void SetXFit1 (int xFit1)
	{
		m_xFit1 = xFit1;
	}


	int GetXFit2 ()
	{
		return m_xFit2;
	}

	void SetXFit2 (int xFit2)
	{
		m_xFit2 = xFit2;
	}

	COLORREF GetTextColor1 ()
	{
		return m_TextColor1;
	}

	COLORREF GetTextColor2 ()
	{
		return m_TextColor2;
	}

	CTextStyle();
	virtual ~CTextStyle();
	static HFONT CreateLogoFont (HDC hdc, char * szFaceN, int iDeciPtH,
		                  int iDeciPtW, int iAttrib, BOOL fLogRes);
	void SetSizes ();
	void CalculatePositions (HDC hdc, RECT *rect, HFONT hFont1, HFONT hFont2);
	int GetShadow1 (int shadowSize);
	int GetShadow2 (int shadowSize);
};

#endif // !defined(AFX_TEXTSTYLE_H__79BFD2A7_146C_424B_8F12_DA2FD5FD9F51__INCLUDED_)
