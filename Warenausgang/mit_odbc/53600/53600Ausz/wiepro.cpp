#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "a_kun.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "wiepro.h"

struct WIEPRO wiepro, wiepro_null;

void WIEPRO_CLASS::prepare (void)
{
//            char *sqltext;

            ins_quest ((char *) &wiepro.mdn, 1, 0);
            ins_quest ((char *) &wiepro.fil, 1, 0);
    out_quest ((char *) wiepro.zeit,0,7);
    out_quest ((char *) &wiepro.dat,2,0);
    out_quest ((char *) &wiepro.sys,2,0);
    out_quest ((char *) wiepro.peri_nam,0,21);
    out_quest ((char *) wiepro.blg_typ,0,2);
    out_quest ((char *) &wiepro.nr,2,0);
    out_quest ((char *) &wiepro.kun_fil,1,0);
    out_quest ((char *) &wiepro.fil,1,0);
    out_quest ((char *) &wiepro.mdn,1,0);
    out_quest ((char *) &wiepro.kun,2,0);
    out_quest ((char *) &wiepro.a,3,0);
    out_quest ((char *) &wiepro.gew_bto,3,0);
    out_quest ((char *) &wiepro.gew_nto,3,0);
    out_quest ((char *) &wiepro.tara,3,0);
    out_quest ((char *) wiepro.erf_kz,0,2);
    out_quest ((char *) wiepro.pers,0,13);
    out_quest ((char *) &wiepro.pr,3,0);
    out_quest ((char *) &wiepro.delstatus,1,0);
    out_quest ((char *) wiepro.ls_charge,0,21);
    out_quest ((char *) &wiepro.anz,2,0);
    out_quest ((char *) wiepro.ls_ident,0,21);
    out_quest ((char *) wiepro.ls_char30,0,31);
    out_quest ((char *) &wiepro.nett_alt,3,0);
    out_quest ((char *) &wiepro.hbk,2,0);
            cursor = prepare_sql ("select wiepro.zeit,  "
"wiepro.dat,  wiepro.sys,  wiepro.peri_nam,  wiepro.blg_typ,  wiepro.nr,  "
"wiepro.kun_fil,  wiepro.fil,  wiepro.mdn,  wiepro.kun,  wiepro.a,  "
"wiepro.gew_bto,  wiepro.gew_nto,  wiepro.tara,  wiepro.erf_kz,  "
"wiepro.pers,  wiepro.pr,  wiepro.delstatus,  wiepro.ls_charge,  "
"wiepro.anz,  wiepro.ls_ident,  wiepro.ls_char30,  wiepro.nett_alt,  "
"wiepro.hbk from wiepro "

#line 27 "wiepro.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? ");

    ins_quest ((char *) wiepro.zeit,0,7);
    ins_quest ((char *) &wiepro.dat,2,0);
    ins_quest ((char *) &wiepro.sys,2,0);
    ins_quest ((char *) wiepro.peri_nam,0,21);
    ins_quest ((char *) wiepro.blg_typ,0,2);
    ins_quest ((char *) &wiepro.nr,2,0);
    ins_quest ((char *) &wiepro.kun_fil,1,0);
    ins_quest ((char *) &wiepro.fil,1,0);
    ins_quest ((char *) &wiepro.mdn,1,0);
    ins_quest ((char *) &wiepro.kun,2,0);
    ins_quest ((char *) &wiepro.a,3,0);
    ins_quest ((char *) &wiepro.gew_bto,3,0);
    ins_quest ((char *) &wiepro.gew_nto,3,0);
    ins_quest ((char *) &wiepro.tara,3,0);
    ins_quest ((char *) wiepro.erf_kz,0,2);
    ins_quest ((char *) wiepro.pers,0,13);
    ins_quest ((char *) &wiepro.pr,3,0);
    ins_quest ((char *) &wiepro.delstatus,1,0);
    ins_quest ((char *) wiepro.ls_charge,0,21);
    ins_quest ((char *) &wiepro.anz,2,0);
    ins_quest ((char *) wiepro.ls_ident,0,21);
    ins_quest ((char *) wiepro.ls_char30,0,31);
    ins_quest ((char *) &wiepro.nett_alt,3,0);
    ins_quest ((char *) &wiepro.hbk,2,0);
            ins_cursor = prepare_sql ("insert into wiepro ("
"zeit,  dat,  sys,  peri_nam,  blg_typ,  nr,  kun_fil,  fil,  mdn,  kun,  a,  gew_bto,  gew_nto,  "
"tara,  erf_kz,  pers,  pr,  delstatus,  ls_charge,  anz,  ls_ident,  ls_char30,  "
"nett_alt,  hbk) "

#line 31 "wiepro.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 33 "wiepro.rpp"
}
int WIEPRO_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int WIEPRO_CLASS::dbinsert (void)
/**
Insert in WIEPRO
**/
{
         if (ins_cursor == -1)
         {
                     prepare ();
         }
         execute_curs (ins_cursor);
         return sqlstatus;
}
         
int WIEPRO_CLASS::dbupdate (void)
/**
Insert in WIEPRO
**/
{
        return dbinsert ();
}

int WIEPRO_CLASS::dbdelete (void)
/**
Insert in WIEPRO
**/
{
        return 0;
}

int WIEPRO_CLASS::dbclose (void)
/**
Insert in WIEPRO
**/
{
        if (cursor != -1)
        { 
                 close_sql (cursor);
                 cursor = -1;
        }
        if (ins_cursor != -1)
        { 
                 close_sql (ins_cursor);
                 cursor = -1;
        }
        return 0;
}

