// LeergutPart.h: Schnittstelle f�r die Klasse CLeergutPart.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEERGUTPART_H__180C313A_A75D_4E8A_8D8D_15793EA027EE__INCLUDED_)
#define AFX_LEERGUTPART_H__180C313A_A75D_4E8A_8D8D_15793EA027EE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "Text.h"

#define CString Text

class CLeergutPart  
{
private:
	CString m_Number;
	CString m_Name;
public:
	const LPSTR Number ()
	{
		return m_Number;
	}

	const LPSTR Name ()
	{
		return m_Name;
	}

	CLeergutPart();
	virtual ~CLeergutPart();
	void Set (CString& source);

};

#endif // !defined(AFX_LEERGUTPART_H__180C313A_A75D_4E8A_8D8D_15793EA027EE__INCLUDED_)
