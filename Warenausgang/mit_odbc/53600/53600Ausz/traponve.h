#ifndef _TRAPONVE_DEF
#define _TRAPONVE_DEF
#define CONSOLE
#include "wmask.h"
#include "dbclass.h"

struct TRAPONVE {
   short     mdn;
   short     fil;
   long      ls;
   char      blg_typ[2];
   long      lfd;
   char      nve[21];
   double    a;
   short     delstatus;
};
extern struct TRAPONVE traponve, traponve_null;

#line 8 "traponve.rh"

class TRAPONVE_CLASS : public DB_CLASS 
{
       private:
       public :
               TRAPONVE traponve; 
               TRAPONVE_CLASS () : DB_CLASS ()
               {
               }
       virtual void prepare (void);
};
#endif
