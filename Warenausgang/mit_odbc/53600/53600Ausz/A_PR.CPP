#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "a_pr.h"
#include "dbfunc.h"

struct A_PR _a_pr;

void A_PR_CLASS::prepare_mdn (void)
{
          ins_quest ((char *) &_a_pr.a, 3, 0);
          ins_quest ((char *) &_a_pr.mdn, 1, 0);
          out_quest ((char *) &_a_pr.a, 3, 0);
          out_quest ((char *) &_a_pr.mdn_gr, 1, 0);
          out_quest ((char *) &_a_pr.mdn, 1, 0);
          out_quest ((char *) &_a_pr.fil_gr, 1, 0);
          out_quest ((char *) &_a_pr.fil, 1, 0);
          out_quest ((char *) &_a_pr.pr_ek, 3, 0);
          out_quest ((char *) &_a_pr.pr_ek_euro, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk_euro, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk1, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk2, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk3, 3, 0);
          out_quest ((char *) &_a_pr.akt, 2, 0);
          out_quest ((char *) &_a_pr.lad_akv, 0, 2);
          out_quest ((char *) &_a_pr.lief_akv, 0, 2);
          cursor_a_pr_mdn = prepare_sql ("select a, mdn_gr, mdn, fil_gr,fil,"
                             "pr_ek,pr_ek_euro,pr_vk,pr_vk:euro,pr_vk1,pr_vk2,pr_vk3,"
                             "akt,lad_akv,lief_akv "
                             "from a_pr "
                             "where a = ? "
                             "and mdn = ? "
                             "order by fil_gr,fil");
}

void A_PR_CLASS::prepare_fil (void)
{
          ins_quest ((char *) &_a_pr.a, 3, 0);
          ins_quest ((char *) &_a_pr.mdn, 1, 0);
          ins_quest ((char *) &_a_pr.fil, 1, 0);
          out_quest ((char *) &_a_pr.a, 3, 0);
          out_quest ((char *) &_a_pr.mdn_gr, 1, 0);
          out_quest ((char *) &_a_pr.mdn, 1, 0);
          out_quest ((char *) &_a_pr.fil_gr, 1, 0);
          out_quest ((char *) &_a_pr.fil, 1, 0);
          out_quest ((char *) &_a_pr.pr_ek, 3, 0);
          out_quest ((char *) &_a_pr.pr_ek_euro, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk_euro, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk1, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk2, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk3, 3, 0);
          out_quest ((char *) &_a_pr.akt, 2, 0);
          out_quest ((char *) &_a_pr.lad_akv, 0, 2);
          out_quest ((char *) &_a_pr.lief_akv, 0, 2);
          cursor_a_pr_fil = prepare_sql ("select a, mdn_gr, mdn, fil_gr,fil,"
                             "pr_ek,pr_ek_euro,pr_vk,pr_vk_euro,pr_vk1,pr_vk2,pr_vk3,"
                             "akt,lad_akv,lief_akv "
                             "from a_pr "
                             "where a = ? "
                             "and mdn = ? "
                             "and fil = ? ");
}

void A_PR_CLASS::prepare  (void)
{
          ins_quest ((char *) &_a_pr.a, 3, 0);
          out_quest ((char *) &_a_pr.a, 3, 0);
          out_quest ((char *) &_a_pr.mdn_gr, 1, 0);
          out_quest ((char *) &_a_pr.mdn, 1, 0);
          out_quest ((char *) &_a_pr.fil_gr, 1, 0);
          out_quest ((char *) &_a_pr.fil, 1, 0);
          out_quest ((char *) &_a_pr.pr_ek, 3, 0);
          out_quest ((char *) &_a_pr.pr_ek_euro, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk_euro, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk1, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk2, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk3, 3, 0);
          out_quest ((char *) &_a_pr.akt, 2, 0);
          out_quest ((char *) &_a_pr.lad_akv, 0, 2);
          out_quest ((char *) &_a_pr.lief_akv, 0, 2);
          cursor_a_pr = prepare_sql ("select a, mdn_gr, mdn, fil_gr,fil,"
                             "pr_ek,pr_ek_euro,pr_vk,pr_vk_euro,pr_vk1,pr_vk2,pr_vk3,"
                             "akt,lad_akv,lief_akv "
                             "from a_pr where a = ? "
                             "order by mdn_gr, mdn, fil_gr,fil");
           prepare_mdn ();
           prepare_fil ();

          ins_quest ((char *) &_a_pr.a, 3, 0);
          ins_quest ((char *) &_a_pr.mdn_gr, 1, 0);
          ins_quest ((char *) &_a_pr.mdn, 1, 0);
          ins_quest ((char *) &_a_pr.fil_gr, 1, 0);
          ins_quest ((char *) &_a_pr.fil, 1, 0);
          test_upd_cursor = prepare_sql
                    ("select a from a_pr where a = ? "
                                        "and mdn_gr = ? "
                                        "and mdn = ? " 
                                        "and fil_gr = ? " 
                                        "and fil = ? for update");
          ins_quest ((char *) &_a_pr.pr_ek, 3, 0);
          ins_quest ((char *) &_a_pr.pr_ek_euro, 3, 0);
          ins_quest ((char *) &_a_pr.pr_vk, 3, 0);
          ins_quest ((char *) &_a_pr.pr_vk_euro, 3, 0);
          upd_cursor = prepare_sql
                    ("update a_pr set pr_ek = ?, pr_ek_euro = ?, "
                     "pr_vk = ?, pr_vk_euro = ? "
                     "where current of %s",
                      (char *) cursor_name (test_upd_cursor));

          ins_quest ((char *) &_a_pr.a, 3, 0);
          ins_quest ((char *) &_a_pr.mdn_gr, 1, 0);
          ins_quest ((char *) &_a_pr.mdn, 1, 0);
          ins_quest ((char *) &_a_pr.fil_gr, 1, 0);
          ins_quest ((char *) &_a_pr.fil, 1, 0);
          ins_quest ((char *) &_a_pr.pr_ek, 3, 0);
          ins_quest ((char *) &_a_pr.pr_ek_euro, 3, 0);
          ins_quest ((char *) &_a_pr.pr_vk, 3, 0);
          ins_quest ((char *) &_a_pr.pr_vk_euro, 3, 0);
          ins_quest ((char *) &_a_pr.key_typ_dec13, 3, 0);
          ins_quest ((char *) &_a_pr.bearb, 2, 0);
          ins_quest (_a_pr.pers_nam, 0, 8);
          ins_cursor = prepare_sql
                    ("insert into a_pr "
                     "(a, mdn_gr, mdn, fil_gr, fil, pr_ek, pr_ek_euro, pr_vk, pr_vk_euro, "
                     "delstatus,akt, key_typ_dec13, lad_akv, lief_akv, "
                     "modif, bearb, pers_nam, pr_vk1, pr_vk2, pr_vk3) "
                     "values "
                     "(?,?,?,?,?,?,?, 0, -1, ?, \" \", \" \", \"B\", "
                     " ?, ?, 0, 0, 0)");
           ins_quest ((char *) &_a_pr.a, 3, 0);
           ins_quest ((char *) &_a_pr.mdn_gr, 1, 0);
           ins_quest ((char *) &_a_pr.mdn, 1, 0);
           ins_quest ((char *) &_a_pr.fil_gr, 1, 0);
           ins_quest ((char *) &_a_pr.fil, 1, 0);
           del_cursor = prepare_sql
                    ("delete from a_pr where a = ? "
                                        "and mdn_gr = ? "
                                        "and mdn = ? " 
                                         "and fil_gr = ? " 
                                         "and fil = ?");
}

void A_PR_CLASS::prepare_h  (void)
{
          ins_quest ((char *) &_a_pr.mdn_gr, 1, 0);
          ins_quest ((char *) &_a_pr.mdn, 1, 0);
          ins_quest ((char *) &_a_pr.fil_gr, 1, 0);
          ins_quest ((char *) &_a_pr.fil, 1, 0);
          ins_quest ((char *) &_a_pr.a, 3, 0);

          out_quest ((char *) &_a_pr.a, 3, 0);
          out_quest ((char *) &_a_pr.mdn_gr, 1, 0);
          out_quest ((char *) &_a_pr.mdn, 1, 0);
          out_quest ((char *) &_a_pr.fil_gr, 1, 0);
          out_quest ((char *) &_a_pr.fil, 1, 0);
          out_quest ((char *) &_a_pr.pr_ek, 3, 0);
          out_quest ((char *) &_a_pr.pr_ek_euro, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk, 3, 0);
          out_quest ((char *) &_a_pr.pr_vk_euro, 3, 0);
          out_quest ((char *) &_a_pr.akt, 2, 0);
          out_quest ((char *) &_a_pr.lad_akv, 0, 2);
          out_quest ((char *) &_a_pr.lief_akv, 0, 2);
          cursor_a_pr_h = prepare_sql ("select a, mdn_gr, mdn, fil_gr,fil,"
                             "pr_ek,pr_ek_euro,pr_vk,pr_vk_euro,akt,lad_akv,lief_akv "
                             "from a_pr "
                             "where mdn_gr = ? "
                             "and   mdn = ? "
                             "and   fil_gr = ? "
                             "and   fil = ? "
                             "and   a = ?");
}

int A_PR_CLASS::lese_a_pr (double a)
/**
Tabelle a_pr fuer Artikel a lesen.
**/
{
         if (cursor_a_pr == -1)
         {
                        prepare ();
         }
         _a_pr.a = a;
         open_sql (cursor_a_pr);
         fetch_sql (cursor_a_pr);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int A_PR_CLASS::lese_a_pr (void)
/**
Naechsten Satz aus Tabelle a_pr lesen.
**/
{
         fetch_sql (cursor_a_pr);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int A_PR_CLASS::lese_a_pr_mdn (double a, short mdn)
/**
Tabelle a_pr fuer Artikel a lesen.
**/
{
         if (cursor_a_pr == -1)
         {
                        prepare ();
         }
         _a_pr.a = a;
         _a_pr.mdn = mdn;
         open_sql (cursor_a_pr_mdn);
         fetch_sql (cursor_a_pr_mdn);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int A_PR_CLASS::lese_a_pr_mdn (void)
/**
Naechsten Satz aus Tabelle a_pr lesen.
**/
{
         fetch_sql (cursor_a_pr_mdn);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int A_PR_CLASS::lese_a_pr_fil (double a, short mdn, short fil)
/**
Tabelle a_pr fuer Artikel a lesen.
**/
{
         if (cursor_a_pr == -1)
         {
                        prepare ();
         }
         _a_pr.a = a;
         _a_pr.mdn = mdn;
         _a_pr.fil = fil;
         open_sql (cursor_a_pr_fil);
         fetch_sql (cursor_a_pr_fil);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int A_PR_CLASS::lese_a_pr_fil (void)
/**
Naechsten Satz aus Tabelle a_pr lesen.
**/
{
         fetch_sql (cursor_a_pr_fil);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int A_PR_CLASS::lese_a_pr_h (short mdn_gr, short mdn, short fil_gr,
                             short fil, double a)
/**
Tabelle a_pr fuer Artikel a lesen.
**/
{
         if (cursor_a_pr_h == -1)
         {
                        prepare_h ();
         }
         _a_pr.mdn_gr = mdn_gr;
         _a_pr.mdn    = mdn;
         _a_pr.fil_gr = fil_gr;
         _a_pr.fil    = fil;
         _a_pr.a      = a;
         open_sql (cursor_a_pr_h);
         fetch_sql (cursor_a_pr_h);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int A_PR_CLASS::lese_a_pr_h (void)
/**
Naechsten Satz aus Tabelle a_pr lesen.
**/
{
         fetch_sql (cursor_a_pr_h);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}


int A_PR_CLASS::update_a_pr (double a, short mdn_gr, short mdn, short fil_gr, short fil)
/**
Tabelle a_pr aendern.
**/
{
         _a_pr.a      = a;
         _a_pr.mdn_gr = mdn_gr;
         _a_pr.mdn    = mdn;
         _a_pr.fil_gr = fil_gr;
         _a_pr.fil    = fil;
         _a_pr.key_typ_dec13  = a;
         // strcpy (_a_pr.bearb, "03.26.1998");
         // strcpy (_a_pr.pers_nam, "TEST");
         open_sql (test_upd_cursor);
         fetch_sql (test_upd_cursor);
         if (sqlstatus == 0)
         {
                     execute_curs (upd_cursor);
         }
         else
         {
                     execute_curs (ins_cursor);
         }
         return (0);
}

int A_PR_CLASS::delete_a_pr (double a, short mdn_gr, short mdn, short fil_gr, short fil)
/**
Tabelle a_pr aendern.
**/
{
         _a_pr.a      = a;
         _a_pr.mdn_gr = mdn_gr;
         _a_pr.mdn    = mdn;
         _a_pr.fil_gr = fil_gr;
         _a_pr.fil    = fil;
         execute_curs (del_cursor);
         return (0);
}

int A_PR_CLASS::gueltig_a_pr ()
/**
Preisebene auf gueltig setzen.
**/
{
         char buffer [256];
         char and_part [80];

         if (_a_pr.fil > 0) return 0;

         if (_a_pr.fil_gr > 0)
         {
                   sprintf (and_part, " and mdn_gr = %hd "
                                     "and mdn = %hd "
                                     "and fil_gr = %hd "
                                     "and fil > 0",
                                     _a_pr.mdn_gr, _a_pr.mdn, _a_pr.fil_gr);
         }

         if (_a_pr.mdn > 0)
         {
                   sprintf (and_part, " and mdn = %hd "
                                      "and (fil_gr > 0 or fil > 0)",
                                      _a_pr.mdn);
         }

         if (_a_pr.mdn_gr > 0)
         {
                   sprintf (and_part, " and mdn_gr = %hd "
                                     "and (mdn > 0 or fil_gr > 0 "
                                     "or fil > 0)", _a_pr.mdn_gr);
         }

         if (_a_pr.mdn_gr == 0 && _a_pr.mdn == 0 &&
             _a_pr.fil_gr == 0 && _a_pr.fil == 0)
         {
                   strcpy (and_part, " and (mdn_gr > 0 or mdn > 0 or "
                                     " fil_gr > 0 or fil > 0)");
         }

         sprintf (buffer, "delete from a_pr where a = %.0lf%s",
                         _a_pr.a, and_part);
         execute_sql (buffer);
         return 0;
}

