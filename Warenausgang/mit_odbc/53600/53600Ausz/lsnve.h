#ifndef _LSNVE_DEF
#define _LSNVE_DEF
#include "wmaskc.h"
#include "dbclass.h"

struct LSNVE {
   short     mdn;
   short     fil;
   long      nve_posi;
   char      nve[21];
   char      nve_palette[21];
   long      ls;
   double    a;
   double    me;
   short     me_einh;
   short     gue;
   short     palette_kz;
   long      tag;
   long      mhd;
   double    brutto_gew;
   long      verp_art;
   double    ps_art;
   double    ps_stck;
   double    ps_art1;
   double    ps_stck1;
   double    netto_gew;
   char      masternve[21];
   char      ls_charge[31];
};
extern struct LSNVE lsnve, lsnve_null;

#line 7 "lsnve.rh"

class LSNVE_CLASS : public DB_CLASS 
{
       private :
       public :
               LSNVE_CLASS () : DB_CLASS ()
               {
               }
               void prepare (void);
               int dbreadfirst (void);
};
#endif
