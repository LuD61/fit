// LicenceDialog.cpp: Implementierung der Klasse LicenceDialog.
//
//////////////////////////////////////////////////////////////////////

#include "LicenceDialog.h"
#include "strfkt.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

LicenceDialog::LicenceDialog() : 
				WDialog (IDD)
{
	text1 = "";
	text2 = "";
	text3 = "";
	text4 = "";
	text5 = "";
	text6 = "";
	text7 = "";
	text8 = "";
	text9 = "";
    m_DlgBrush = NULL;
    m_WhiteBrush = NULL;
	m_hFont1 = NULL;
	m_hFont4 = NULL;
	m_hFont5 = NULL;
	m_hFont10 = NULL;
	AbbortRow = FALSE;
}

LicenceDialog::~LicenceDialog()
{
	if (m_DlgBrush != NULL)
	{
		DeleteObject (m_DlgBrush);
	}
	if (m_WhiteBrush != NULL)
	{
		DeleteObject (m_WhiteBrush);
	}
	if (m_hFont1 != NULL)
	{
		DeleteObject (m_hFont1);
	}
	if (m_hFont4 != NULL)
	{
		DeleteObject (m_hFont4);
	}
	if (m_hFont5 != NULL)
	{
		DeleteObject (m_hFont5);
	}
	if (m_hFont10 != NULL)
	{
		DeleteObject (m_hFont10);
	}
}

void LicenceDialog::SetRowText (string *text, DWORD Idc)
{
	HWND hWnd;

	hWnd = GetDlgItem (m_hWnd, Idc);
	if (hWnd != NULL)
	{
		SetWindowText (hWnd, text->c_str ());
	}
}

void LicenceDialog::SetRowFont (char *FontName, int FontHeight, DWORD Idc, HFONT *hFont, BOOL Bold)
{
	HWND hWnd;
    HDC hdc;

	hWnd = GetDlgItem (m_hWnd, Idc);
	if (hWnd != NULL)
	{
		hdc = GetDC (hWnd);
		*hFont = CreateFont (hdc, FontName, FontHeight, 0, Bold);
		ReleaseDC (hWnd, hdc);
		SetControlFont (Idc, *hFont);
	}
}

bool LicenceDialog::OnInitDialog ()
{

    SetRowText (&text1, IDC_ROW1);
    SetRowText (&text2, IDC_ROW2);
    SetRowText (&text3, IDC_ROW3);
    SetRowText (&text4, IDC_ROW4);
    SetRowText (&text5, IDC_ROW5);
    SetRowText (&text6, IDC_ROW6);
    SetRowText (&text7, IDC_ROW7);
    SetRowText (&text8, IDC_ROW8);
	if(AbbortRow)
	{
		SetRowText (&text9, IDC_ROW10);
	}
	else
	{
		SetRowText (&text9, IDC_ROW9);
	}

	SetRowFont ("Impact", 250, IDC_ROW1,  &m_hFont1);
	SetRowFont ("Dialog", 150, IDC_ROW10, &m_hFont10, TRUE);

	SetRowFont ("Dialog", 110, IDC_ROW4, &m_hFont4, TRUE);
	SetRowFont ("Dialog", 110, IDC_ROW5, &m_hFont5, TRUE);

    HWND hWnd9 = GetDlgItem (m_hWnd, IDC_ROW9);
	HWND hWnd10 = GetDlgItem (m_hWnd, IDC_ROW10);
	if(AbbortRow)
	{
		ShowWindow (hWnd9,  SW_HIDE);
		ShowWindow (hWnd10, SW_SHOWNORMAL);
	}
	else
	{
		ShowWindow (hWnd10,  SW_HIDE);
		ShowWindow (hWnd9, SW_SHOWNORMAL);
	}

	return false;
}

HBRUSH LicenceDialog::OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor)
{

	 if (m_DlgBrush == NULL)
	 {
//		m_DlgBrush = CreateSolidBrush (GetSysColor (COLOR_3DFACE));
		m_DlgBrush = CreateSolidBrush (RGB (255, 255, 204));
	 }

	 if (m_WhiteBrush == NULL)
	 {
		m_WhiteBrush = CreateSolidBrush (RGB (0, 0, 255));
	 }

	 if (nCtlColor == CTLCOLOR_DLG)
	 {
		 return m_DlgBrush;
	 }
	 if (nCtlColor == CTLCOLOR_STATIC)
	 {
		 HWND hWnd1 = GetDlgItem (m_hWnd, IDC_ROW1);
		 HWND hWnd4 = GetDlgItem (m_hWnd, IDC_ROW4);
		 HWND hWnd5 = GetDlgItem (m_hWnd, IDC_ROW5);
		 HWND hWnd7 = GetDlgItem (m_hWnd, IDC_ROW7);
		 HWND hWnd8 = GetDlgItem (m_hWnd, IDC_ROW8);
		 HWND hWnd9 = GetDlgItem (m_hWnd, IDC_ROW9);
		 HWND hWnd10 = GetDlgItem (m_hWnd, IDC_ROW10);
		 SetBkMode (hDC, TRANSPARENT);
		 if (hWnd == hWnd1)
		 {
			 SetTextColor (hDC, RGB (255, 0, 0));
			 return m_DlgBrush;
		 }
		 else if (hWnd == hWnd4)
		 {
			 SetTextColor (hDC, RGB (0, 0, 192));
			 return m_DlgBrush;
		 }
		 else if (hWnd == hWnd5)
		 {
			 SetTextColor (hDC, RGB (0, 0, 192));
			 return m_DlgBrush;
		 }
		 else if (hWnd == hWnd9)
		 {
			 SetTextColor (hDC, RGB (128, 0, 0));
			 return m_DlgBrush;
		 }
		 else if (hWnd == hWnd10)
		 {
			 SetTextColor (hDC, RGB (255, 0, 0));
			 return m_DlgBrush;
		 }
		 return m_DlgBrush;
	 }
	 return WDialog::OnCtlColor (hDC, hWnd, nCtlColor);
}

void LicenceDialog::SetText (char *text)
{
	      int count;

		  count = wsplit (text, "\n");
		  if (count == 0)
		  {
			  return;
		  }

		  text1 = wort[0];
		  if (count > 1)
		  {
			  text2 = wort[1];
		  }

		  if (count > 1)
		  {
			  text2 = wort[1];
		  }

		  if (count > 2)
		  {
			  text3 = wort[2];
		  }

		  if (count > 3)
		  {
			  text4 = wort[3];
		  }

		  if (count > 4)
		  {
			  text5 = wort[4];
		  }

		  if (count > 5)
		  {
			  text6 = wort[5];
		  }

		  if (count > 6)
		  {
			  text7 = wort[6];
		  }
		  if (count > 7)
		  {
			  text8 = wort[7];
		  }
		  if (count > 8)
		  {
			  text9 = wort[8];
		  }

}
