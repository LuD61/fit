#include <stdio.h>
#include <windows.h>
#include <process.h>
// #include <windowsx.h>
#ifndef CONSOLE
#include "wmask.h"
#endif
#include "stdfkt.h"

static char batname [512];
static FILE *fpbat;


int DCcopy (char *ziel, char *quelle)
/**
Datei kopieren.
**/
{
           FILE *fz;
           FILE *fq;
           int bytes;
           char buffer [0x1000];

           fq = fopen (quelle, "rb");
           if (fq == (FILE *) 0)
           {
                       return (-1);
           }

           fz = fopen (ziel, "wb");
           if (fz == (FILE *) 0)
           {
                       fclose (fq);
                       return (-1);
           }

           while  (bytes = fread (buffer, 1, 0x1000, fq))
           {
                        fwrite (buffer, 1, bytes, fz);
           }
           fclose (fq);
           fclose (fz);
           return (0);
}

int WaitConsoleExec (LPSTR prog, DWORD SW_CONSOLE)
/**
Console-Process starten und auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       SW_CONSOLE,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  WaitForSingleObject (pi.hProcess, INFINITE);
                  CloseHandle (pi.hProcess);
        }
        return ret;
}

int ConsoleExec (LPSTR prog, DWORD SW_CONSOLE)
/**
Console-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       SW_CONSOLE,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  CloseHandle (pi.hProcess);
        }
        return ret;
}

int ProcExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  CloseHandle (pi.hProcess);
        }
        return ret;
}


HANDLE ProcExecPid (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        int ret;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                  CloseHandle (pi.hThread);
                  return (pi.hProcess);
        }
        return NULL;
}

void ClosePid (HANDLE Pid)
/**
Handle fuer Process schliessen.
**/
{
	     CloseHandle (Pid);
}


DWORD ProcWaitExec (LPSTR prog, WORD SHOW_MODE, int x, int y, int cx, int cy)
/**
Fenster-Process starten und nicht auf Ende Warten.
**/
{
        STARTUPINFO sti;
        PROCESS_INFORMATION pi;
        DWORD ExitCode; 
        int ret;
        MSG msg;

        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = SHOW_MODE;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       prog,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);
        if (ret)
        {
                    CloseHandle (pi.hThread);
//                    WaitForSingleObject (pi.hProcess, INFINITE);
//                    CloseHandle (pi.hProcess);
        }
		else
        {
			      return (DWORD) (-1);
        }	

		GetExitCodeProcess (pi.hProcess, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
                  {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                  }
                  Sleep (5);
                  GetExitCodeProcess (pi.hProcess, &ExitCode);
		}
        CloseHandle (pi.hProcess);
        return ExitCode;
}

int WaitPid (HANDLE Pid)
{
	    DWORD ExitCode;
		MSG msg;

		GetExitCodeProcess (Pid, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
                  if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
                  {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                  }
                  Sleep (5);
                  GetExitCodeProcess (Pid, &ExitCode);
		}
        CloseHandle (Pid);
		return ExitCode;
}

	

int CreateBatch (char *format, ...)
/**
Programm starten.
**/
{
      char programm [512];
      va_list ap;
      char *bws;

      bws = getenv ("BWS");
      if (bws == NULL)
      {
               strcpy (batname, "fitcmd.bat");
      }
      else
      {
               sprintf (batname, "%s\\bin\\fitcmd.bat", bws);
      }

      fpbat = fopen (batname, "w");
      if (fpbat == NULL) return (1);

//      set_env (format, format);
      va_start (ap, format);
      vsprintf (programm, format, ap);
      va_end (ap);
//      fprintf (fpbat, "echo off\n");
      fprintf (fpbat, "%s\n", programm);
      return (0);
}

int AppendBatch (char *format, ...)
/**
Programm starten.
**/
{
      char programm [512];
      va_list ap;

      if (fpbat == NULL) return (1);

//      set_env (format, format);
      va_start (ap, format);
      vsprintf (programm, format, ap);
      va_end (ap);
      fprintf (fpbat, "%s\n", programm);
      return (0);
}
int RunBatch (char *format, ...)
/**
Programm starten.
**/
{
      char programm [512];
      va_list ap;
      int ex;

      if (fpbat == NULL) return (1);

//      set_env (format, format);
      va_start (ap, format);
      vsprintf (programm, format, ap);
      va_end (ap);
      fprintf (fpbat, "%s\n", programm);
      fclose (fpbat);
      ex = ProcWaitExec (batname, SW_SHOW, -1, 0, -1, 0);
      if (ex == 0)
      {
          unlink (batname);
      }
      return (ex);
}


int RunBatchPause (char *format, ...)
/**
Programm starten.
**/
{
      char programm [512];
      va_list ap;
      int ex;

      if (fpbat == NULL) return (1);

//      set_env (format, format);
      va_start (ap, format);
      vsprintf (programm, format, ap);
      va_end (ap);
      fprintf (fpbat, "%s\n", programm);
      fprintf (fpbat, "pause\n");
      fclose (fpbat);
      ex = ProcWaitExec (batname, SW_SHOW, -1, 0, -1, 0);
      if (ex == 0)
      {
          unlink (batname);
      }
      return (ex);
}

