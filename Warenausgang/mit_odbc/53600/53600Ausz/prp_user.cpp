#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <tchar.h>
#include "wmask.h"
#include "strfkt.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "prp_user.h"

struct PRP_USER prp_user, prp_user_null;

void PRP_USER_CLASS::prepare_prp_user (void)
{
    ins_quest ((char *) &prp_user.a,  3, 0);
    out_quest ((char *) &prp_user.a,3,0);
    out_quest ((char *) prp_user.name1,0,49);
    out_quest ((char *) prp_user.wert1,0,513);
    out_quest ((char *) prp_user.name2,0,49);
    out_quest ((char *) prp_user.wert2,0,513);
    out_quest ((char *) prp_user.name3,0,49);
    out_quest ((char *) prp_user.wert3,0,513);
    out_quest ((char *) prp_user.name4,0,49);
    out_quest ((char *) prp_user.wert4,0,513);
    out_quest ((char *) prp_user.name5,0,49);
    out_quest ((char *) prp_user.wert5,0,513);
    out_quest ((char *) prp_user.name6,0,49);
    out_quest ((char *) prp_user.wert6,0,513);
    out_quest ((char *) prp_user.name7,0,49);
    out_quest ((char *) prp_user.wert7,0,513);
    out_quest ((char *) prp_user.name8,0,49);
    out_quest ((char *) prp_user.wert8,0,513);
    out_quest ((char *) prp_user.name9,0,49);
    out_quest ((char *) prp_user.wert9,0,513);
    out_quest ((char *) prp_user.name10,0,49);
    out_quest ((char *) prp_user.wert10,0,513);
    out_quest ((char *) prp_user.name11,0,49);
    out_quest ((char *) prp_user.wert11,0,513);
    out_quest ((char *) prp_user.name12,0,49);
    out_quest ((char *) prp_user.wert12,0,513);
    out_quest ((char *) prp_user.name13,0,49);
    out_quest ((char *) prp_user.wert13,0,513);
    out_quest ((char *) prp_user.name14,0,49);
    out_quest ((char *) prp_user.wert14,0,513);
    out_quest ((char *) prp_user.name15,0,49);
    out_quest ((char *) prp_user.wert15,0,513);
    out_quest ((char *) prp_user.name16,0,49);
    out_quest ((char *) prp_user.wert16,0,513);
    out_quest ((char *) prp_user.name17,0,49);
    out_quest ((char *) prp_user.wert17,0,513);
    out_quest ((char *) prp_user.name18,0,49);
    out_quest ((char *) prp_user.wert18,0,513);
    out_quest ((char *) prp_user.name19,0,49);
    out_quest ((char *) prp_user.wert19,0,513);
    out_quest ((char *) prp_user.name20,0,49);
    out_quest ((char *) prp_user.wert20,0,513);
    out_quest ((char *) prp_user.name21,0,49);
    out_quest ((char *) prp_user.wert21,0,513);
    out_quest ((char *) prp_user.name22,0,49);
    out_quest ((char *) prp_user.wert22,0,513);
    out_quest ((char *) prp_user.name23,0,49);
    out_quest ((char *) prp_user.wert23,0,513);
    out_quest ((char *) prp_user.name24,0,49);
    out_quest ((char *) prp_user.wert24,0,513);
    out_quest ((char *) prp_user.name25,0,49);
    out_quest ((char *) prp_user.wert25,0,513);
    out_quest ((char *) prp_user.name26,0,49);
    out_quest ((char *) prp_user.wert26,0,513);
    out_quest ((char *) prp_user.name27,0,49);
    out_quest ((char *) prp_user.wert27,0,513);
    out_quest ((char *) prp_user.name28,0,49);
    out_quest ((char *) prp_user.wert28,0,513);
    out_quest ((char *) prp_user.name29,0,49);
    out_quest ((char *) prp_user.wert29,0,513);
    out_quest ((char *) prp_user.name30,0,49);
    out_quest ((char *) prp_user.wert30,0,513);
    cursor_prp_user = prepare_sql (_T("select prp_user.a,  "
"prp_user.name1,  prp_user.wert1,  prp_user.name2,  prp_user.wert2,  "
"prp_user.name3,  prp_user.wert3,  prp_user.name4,  prp_user.wert4,  "
"prp_user.name5,  prp_user.wert5,  prp_user.name6,  prp_user.wert6,  "
"prp_user.name7,  prp_user.wert7,  prp_user.name8,  prp_user.wert8,  "
"prp_user.name9,  prp_user.wert9,  prp_user.name10,  prp_user.wert10,  "
"prp_user.name11,  prp_user.wert11,  prp_user.name12,  prp_user.wert12,  "
"prp_user.name13,  prp_user.wert13,  prp_user.name14,  prp_user.wert14,  "
"prp_user.name15,  prp_user.wert15,  prp_user.name16,  prp_user.wert16,  "
"prp_user.name17,  prp_user.wert17,  prp_user.name18,  prp_user.wert18,  "
"prp_user.name19,  prp_user.wert19,  prp_user.name20,  prp_user.wert20,  "
"prp_user.name21,  prp_user.wert21,  prp_user.name22,  prp_user.wert22,  "
"prp_user.name23,  prp_user.wert23,  prp_user.name24,  prp_user.wert24,  "
"prp_user.name25,  prp_user.wert25,  prp_user.name26,  prp_user.wert26,  "
"prp_user.name27,  prp_user.wert27,  prp_user.name28,  prp_user.wert28,  "
"prp_user.name29,  prp_user.wert29,  prp_user.name30,  prp_user.wert30 from prp_user where a = ? "));

#line 19 "prp_user.rpp"
}

int PRP_USER_CLASS::lese_prp_user (double a)
{
    long sqlstatus1;
    if (cursor_prp_user == -1)
    {
        prepare_prp_user ();
    }
    prp_user.a = a;
    open_sql (cursor_prp_user);
    fetch_sql (cursor_prp_user);
    sqlstatus1 = sqlstatus;
    /* close_sql (cursor_prp_user); */
    if ( sqlstatus )
    {
	prp_user.name1[0] = '\0';	prp_user.wert1[0] = '\0';
	prp_user.name2[0] = '\0';	prp_user.wert2[0] = '\0';
	prp_user.name3[0] = '\0';	prp_user.wert3[0] = '\0';
	prp_user.name4[0] = '\0';	prp_user.wert4[0] = '\0';
	prp_user.name5[0] = '\0';	prp_user.wert5[0] = '\0';
	prp_user.name6[0] = '\0';	prp_user.wert6[0] = '\0';
	prp_user.name7[0] = '\0';	prp_user.wert7[0] = '\0';
	prp_user.name8[0] = '\0';	prp_user.wert8[0] = '\0';
	prp_user.name9[0] = '\0';	prp_user.wert9[0] = '\0';
	prp_user.name10[0] = '\0';	prp_user.wert10[0] = '\0';
	prp_user.name11[0] = '\0';	prp_user.wert11[0] = '\0';
	prp_user.name12[0] = '\0';	prp_user.wert12[0] = '\0';
    }
    else
    {
	clipped( prp_user.name1 );	clipped( prp_user.wert1 );
	clipped( prp_user.name2 );	clipped( prp_user.wert2 );
	clipped( prp_user.name3 );	clipped( prp_user.wert3 );
	clipped( prp_user.name4 );	clipped( prp_user.wert4 );
	clipped( prp_user.name5 );	clipped( prp_user.wert5 );
	clipped( prp_user.name6 );	clipped( prp_user.wert6 );
	clipped( prp_user.name7 );	clipped( prp_user.wert7 );
	clipped( prp_user.name8 );	clipped( prp_user.wert8 );
	clipped( prp_user.name9 );	clipped( prp_user.wert9 );
	clipped( prp_user.name10 ); clipped( prp_user.wert10 );
	clipped( prp_user.name11 ); clipped( prp_user.wert11 );
	clipped( prp_user.name12 ); clipped( prp_user.wert12 );
    }
    writelog ("prp_user.a=%.0lf sqlstatus=%ld", prp_user.a, sqlstatus1);
    close_sql (cursor_prp_user); 
    cursor_prp_user = -1;
    
    return sqlstatus1;
}
