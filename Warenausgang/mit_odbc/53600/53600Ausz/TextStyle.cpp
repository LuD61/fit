// TextStyle.cpp: Implementierung der Klasse CTextStyle.
//
//////////////////////////////////////////////////////////////////////

#include <math.h>
#include "TextStyle.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CTextStyle::CTextStyle()
{
	m_FitSize  = NormalFit;
	m_TextSize = NormalText;
	m_Fit1 = "fit";
	m_Fit2 = "in Frische und Fleisch";
	m_Space1 = 20;
	m_TextColor1 = RGB (255, 0, 0);
	m_TextColor2 = RGB (255, 255, 204);
	SetSizes ();
}

CTextStyle::~CTextStyle()
{

}

void CTextStyle::SetSizes ()
{
	cxScreen = GetSystemMetrics (SM_CXFULLSCREEN);
	cyScreen = GetSystemMetrics (SM_CYFULLSCREEN);
	if (cxScreen > 1300)
	{
		m_FitSize  = BigFit;
		m_TextSize = BigText;
	}
	else if (cxScreen > 1000)
	{
		m_FitSize  = NormalFit;
		m_TextSize = NormalText;
	}

	else
	{
		m_FitSize  = SmallFit;
		m_TextSize = SmallText;
	}

}

HFONT CTextStyle::CreateLogoFont (HDC hdc, char * szFaceN, int iDeciPtH,
                                  int iDeciPtW, int iAttrib, BOOL fLogRes)
     {
     float      cxDpi, cyDpi ;
     HFONT      hFont, oldfont ;
     LOGFONT    lf ;
     POINT      pt ;
     TEXTMETRIC tm ;

     SaveDC (hdc) ;

     SetGraphicsMode (hdc, GM_ADVANCED) ;
     ModifyWorldTransform (hdc, NULL, MWT_IDENTITY) ;

     SetViewportOrgEx (hdc, 0, 0, NULL) ;
     SetWindowOrgEx   (hdc, 0, 0, NULL) ;

     if (fLogRes)
          {
          cxDpi = (float) GetDeviceCaps (hdc, LOGPIXELSX) ;
          cyDpi = (float) GetDeviceCaps (hdc, LOGPIXELSY) ;
          }
     else
          {
          cxDpi = (float) (25.4 * GetDeviceCaps (hdc, HORZRES) /
                                  GetDeviceCaps (hdc, HORZSIZE)) ;

          cyDpi = (float) (25.4 * GetDeviceCaps (hdc, VERTRES) /
                                  GetDeviceCaps (hdc, VERTSIZE)) ;
          }

     pt.x = (int) (iDeciPtW  * cxDpi / 72) ;
     pt.y = (int) (iDeciPtH * cyDpi / 72) ;

     DPtoLP (hdc, &pt, 1) ;

     lf.lfHeight         = - (int) (fabs (pt.y) / 10.0 + 0.5) ;
     lf.lfWidth          = 0 ;
     lf.lfEscapement     = 0 ;
     lf.lfOrientation    = 0 ;
     lf.lfWeight         = iAttrib & EZ_ATTR_BOLD      ? 700 : 0 ;
     lf.lfItalic         = iAttrib & EZ_ATTR_ITALIC    ?   1 : 0 ;
     lf.lfUnderline      = iAttrib & EZ_ATTR_UNDERLINE ?   1 : 0 ;
     lf.lfStrikeOut      = iAttrib & EZ_ATTR_STRIKEOUT ?   1 : 0 ;
     lf.lfCharSet        = 0 ;
     lf.lfOutPrecision   = 0 ;
     lf.lfClipPrecision  = 0 ;
     lf.lfQuality        = 0 ;
     lf.lfPitchAndFamily = 0 ;

     strcpy (lf.lfFaceName, szFaceN) ;

     hFont = CreateFontIndirect (&lf) ;

     if (iDeciPtW != 0)
          {
          oldfont = (HFONT) SelectObject (hdc, hFont) ;

          GetTextMetrics (hdc, &tm) ;

          DeleteObject (SelectObject (hdc, oldfont)) ;

          lf.lfWidth = (int) (tm.tmAveCharWidth *
                              fabs (pt.x) / fabs (pt.y) + 0.5) ;

          hFont = CreateFontIndirect (&lf) ;
          }

     RestoreDC (hdc, -1) ;
     return hFont ;
}

void CTextStyle::CalculatePositions (HDC hdc, RECT *rect, HFONT hFont1, HFONT hFont2)
{
	HFONT oldFont;
	SIZE size1;
	SIZE size2;
	int ysize;

	oldFont = SelectObject (hdc, hFont1);
	GetTextExtentPoint32( hdc, m_Fit1.c_str (), m_Fit1.length (), &size1);
	SelectObject (hdc, hFont2);
	GetTextExtentPoint32( hdc, m_Fit2.c_str (), m_Fit2.length (), &size2);
	ysize = size1.cy + size2.cy + m_Space1;
	m_yFit1 = (rect->bottom - ysize) / 2;
	m_yFit2 = m_yFit1 + size1.cy + m_Space1;
	while ((m_yFit2 + size2.cy + 10) > rect->bottom) m_yFit2 --; 
	m_xFit1 = (rect->right - size1.cx) / 2;
	m_xFit2 = (rect->right - size2.cx) / 2;
}

int CTextStyle::GetShadow1 (int ShadowSize)
{
	if (m_FitSize == NormalFit)
	{
		ShadowSize *= (double) NormalFit / BigFit;
	}
	else if (m_FitSize == SmallFit)
	{
		ShadowSize *= (double) SmallFit / BigFit;
	}
	return ShadowSize;
}

int CTextStyle::GetShadow2 (int ShadowSize)
{
	if (m_TextSize == NormalText)
	{
		ShadowSize *= (double) NormalText / BigText;
	}
	else if (m_TextSize == SmallText)
	{
		ShadowSize *= (double) SmallText / BigText;
	}
	return ShadowSize;
}
