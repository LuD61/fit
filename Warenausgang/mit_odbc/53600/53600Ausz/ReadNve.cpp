#include "ReadNve.h"
#include "mo_curso.h"

ReadNve::ReadNve (Text& nve, short mdn, short fil, long ls)
{
	nve_karton = FALSE;
	this->nve = nve;
	this->mdn = mdn;
	this->fil = fil;
	this->ls = ls;
	nve_posi = 0l;
	Lsnve.prepare ();
}


long ReadNve::GenNvePosi ()
{
           lsnve.nve_posi = 0l;
		   static int cursor = -1;
		   static int cursor_max = -1;

		   if (cursor == -1)
		   {
	             Lsnve.sqlin ((short *) &lsnve.mdn, 1, 0);
	             Lsnve.sqlin ((short *) &lsnve.fil, 1, 0);
	             Lsnve.sqlin ((long *) &lsnve.ls, 2, 0);
		         cursor = Lsnve.sqlcursor ("select * from lsnve "
			                               "where mdn = ? "
						                   "and fil = ? "
						                   "and ls = ?");
				Lsnve.sqlin ((short *) &lsnve.mdn, 1, 0);
				Lsnve.sqlin ((short *) &lsnve.fil, 1, 0);
				Lsnve.sqlin ((long *) &lsnve.ls, 2, 0);
				Lsnve.sqlout ((long *) &lsnve.nve_posi, 2, 0);
				cursor_max = Lsnve.sqlcursor ("select max (nve_posi) from lsnve "
			                               "where mdn = ? "
						                   "and fil = ? "
						                   "and ls = ?");
		   }
		   Lsnve.sqlopen (cursor);
		   int dsqlstatus = Lsnve.sqlfetch (cursor);
		   if (dsqlstatus == 100)
		   {
                   lsnve.nve_posi ++;
		           return lsnve.nve_posi;
		   }

		   Lsnve.sqlopen (cursor_max);
		   dsqlstatus = Lsnve.sqlfetch (cursor_max);
		   lsnve.nve_posi = (lsnve.nve_posi < 0) ? 0 : lsnve.nve_posi;
           lsnve.nve_posi ++;
		   return lsnve.nve_posi;
}
	        

BOOL ReadNve::IsKarton ()
{
	return nve_karton;
}

BOOL ReadNve::IsGue ()
{
	return gue;
}

long ReadNve::GetNvePosi ()
{
	return nve_posi;
}


double ReadNve::getPaletteMe ()
{
	static double lief_me = 0.0;
	static int cursor_me = -1;

	static short mdn;
	static short fil;

	mdn = this->mdn;
	fil = this->fil;
//    nve_palette.Format ("%09.0lf", (double) atof (nve_palette.GetBuffer ())); 
//    strcpy (lsnve.nve_palette, nve_palette.GetBuffer ());
 
	if (cursor_me == -1)
	{
			Lsnve.sqlin ((short *) &mdn, 1, 0);
			Lsnve.sqlin ((char *)  lsnve.nve_palette, 0, sizeof (lsnve.nve));
			Lsnve.sqlout ((double *) &lief_me, 3, 0);
			cursor_me = Lsnve.sqlcursor ("select me from lsnve "
		                            "where mdn = ? "
									"and nve = ?");
	}
	lief_me = 0.0;
	Lsnve.sqlopen (cursor_me);
	int dsqlstatus = Lsnve.sqlfetch (cursor_me);
	return lief_me;
}


int ReadNve::read ()
{
	static long nvels;
	static int cursor_ls = -1;
	static short mdn;
	static short fil;

	mdn = this->mdn;
	fil = this->fil;

	lsnve.mdn = mdn;
	lsnve.fil = fil;
	lsnve.ls  = ls;
    nve.Format ("%09.0lf", (double) atof (nve.GetBuffer ())); 
    strcpy (lsnve.nve, nve.GetBuffer ());
 
	if (cursor_ls == -1)
	{
		Lsnve.sqlin ((short *) &mdn, 1, 0);
		Lsnve.sqlin ((char *)  lsnve.nve, 0, sizeof (lsnve.nve));
		Lsnve.sqlout ((long *) &nvels, 2, 0);
		cursor_ls = Lsnve.sqlcursor ("select ls from lsnve "
		                             "where mdn = ? "
								 	 "and nve = ?");
	}
	Lsnve.sqlopen (cursor_ls);
	int dsqlstatus = Lsnve.sqlfetch (cursor_ls);
	if (dsqlstatus == 100)
	{
		return 100;
	}
	lsnve.ls  = nvels;
	dsqlstatus = Lsnve.dbreadfirst ();
	if (dsqlstatus == 100)
	{
		return 100;
	}

	nve_posi = lsnve.nve_posi;
	mhd = lsnve.mhd;
	if (lsnve.palette_kz == 1)
	{
  	    lsnve.ls  = ls;
		SetPalettePosUngueltig ();
		lsnve.ls = nvels;
	}
	else
	{
        SetPaletteUngueltig ();
  	    nve_karton = TRUE;
	}
	gue = lsnve.gue;
	if (lsnve.gue == 0)
	{
		return -1;
	}
	else if (lsnve.gue == -1)
	{
		return -2;
	}
	else if (lsnve.me == 0.0)
	{
		return -3;
	}
	return 0;
}


int ReadNve::readPalette (double *me)
{
	static long nvels;
	static int cursor_ls = -1;
	static short mdn;
	static short fil;

	mdn = this->mdn;
	fil = this->fil;

	lsnve.mdn = mdn;
	lsnve.fil = fil;
	lsnve.ls  = ls;

    nve.Format ("%09.0lf", (double) atof (nve.GetBuffer ())); 
    strcpy (lsnve.nve_palette, nve.GetBuffer ());
 
	if (cursor_ls == -1)
	{
		Lsnve.sqlin ((short *) &mdn, 1, 0);
		Lsnve.sqlin ((char *)  lsnve.nve_palette, 0, sizeof (lsnve.nve_palette));
		Lsnve.sqlout ((long *) &nvels, 2, 0);
		Lsnve.sqlout ((char *) lsnve.nve, 0, sizeof (lsnve.nve));
		cursor_ls = Lsnve.sqlcursor ("select ls, nve from lsnve "
		                             "where mdn = ? "
								 	 "and nve_palette = ?");
	}
	Lsnve.sqlopen (cursor_ls);
	int dsqlstatus = Lsnve.sqlfetch (cursor_ls);
	if (dsqlstatus == 100)
	{
		return 100;
	}
	*me = 0.0;

	while (dsqlstatus == 0)
	{
		lsnve.ls  = nvels;
		dsqlstatus = Lsnve.dbreadfirst ();
		if (dsqlstatus == 100)
		{
			return 100;
		}

		*me += lsnve.me;
		nve_posi = lsnve.nve_posi;
		if (lsnve.palette_kz == 1)
		{
  			lsnve.ls  = ls;
			SetPalettePosUngueltig ();
			lsnve.ls = nvels;
		}
		else
		{
			SetPaletteUngueltig ();
  			nve_karton = TRUE;
		}
		gue = lsnve.gue;
		if (lsnve.gue == 0)
		{
			return -1;
		}
		else if (lsnve.gue == -1)
		{
			return -2;
		}
	    dsqlstatus = Lsnve.sqlfetch (cursor_ls);
	}
	return 0;
}

int ReadNve::InsertNve ()
{
	return Lsnve.dbupdate ();
}

int ReadNve::TestPaletteGue ()
{
	static int cursor_gue = -1;

	if (cursor_gue == -1)
	{
		Lsnve.sqlin ((short *) &mdn, 1, 0);
		Lsnve.sqlin ((short *) &fil, 1, 0);
		Lsnve.sqlin ((char *)  lsnve.nve_palette, 0, sizeof (lsnve.nve));

	    cursor_gue = Lsnve.sqlcursor ("select gue fron lsnve "
		                  "where mdn = ? "
				          "and fil = ? "
				          "and nve = ? "
				          "and gue = 1");
	}
	Lsnve.sqlopen (cursor_gue);
	return Lsnve.sqlfetch (cursor_gue);
}




void ReadNve::SetPaletteUngueltig ()
{
	static int cursor1 = -1;
	static int cursor2 = -1;
	static short mdn;
	static short fil;

	mdn = this->mdn;
	fil = this->fil;
	if (cursor1 == -1)
	{
			Lsnve.sqlin ((short *) &mdn, 1, 0);
			Lsnve.sqlin ((short *) &fil, 1, 0);
			Lsnve.sqlin ((char *)  lsnve.nve_palette, 0, sizeof (lsnve.nve));
			cursor1 = Lsnve.sqlcursor ("update lsnve set gue = -1 "
		           "where mdn = ? "
				   "and fil = ? "
				   "and nve = ?");
			Lsnve.sqlin ((short *) &mdn, 1, 0);
			Lsnve.sqlin ((short *) &fil, 1, 0);
			Lsnve.sqlin ((char *)  lsnve.nve, 0, sizeof (lsnve.nve));
			cursor2 = Lsnve.sqlcursor ("update lsnve set gue = 0 "
		           "where mdn = ? "
				   "and fil = ? "
				   "and nve = ?");
	}
	Lsnve.sqlexecute (cursor1);
	Lsnve.sqlexecute (cursor2);
}

void ReadNve::SetPalettePosUngueltig ()
{

	static int cursor1 = -1;
	static int cursor2 = -1;
	static long ls;
	static short mdn;
	static short fil;

	ls = this->ls;
	mdn = this->mdn;
	fil = this->fil;
	if (cursor1 == -1)
	{
		Lsnve.sqlin ((long *) &ls,   2, 0);
		Lsnve.sqlin ((short *) &mdn, 1, 0);
		Lsnve.sqlin ((short *) &fil, 1, 0);
		Lsnve.sqlin ((char *)  lsnve.nve_palette, 0, sizeof (lsnve.nve));
		cursor1 = Lsnve.sqlcursor ("update lsnve set gue = 0, ls = ? "
		           "where mdn = ? "
				   "and fil = ? "
				   "and nve_palette = ?");

		Lsnve.sqlin ((long *) &ls,   2, 0);
		Lsnve.sqlin ((short *) &mdn, 1, 0);
		Lsnve.sqlin ((short *) &fil, 1, 0);
		Lsnve.sqlin ((char *)  lsnve.nve_palette, 0, sizeof (lsnve.nve));
		cursor2 = Lsnve.sqlcursor ("update lsnve set gue = 0, ls = ? "
		           "where mdn = ? "
				   "and fil = ? "
				   "and nve = ?");
	}
	Lsnve.sqlexecute (cursor1);
	Lsnve.sqlexecute (cursor2);
}

void ReadNve::SetNvePosi (long nve_posi)
{
	if (nve_karton)
	{
		SetPaletteNvePosi (nve_posi);
	}
	else
	{
		SetPalettePosNvePosi (nve_posi);
	}
}

void ReadNve::SetPaletteNvePosi (long nve_posi0)
{
	static int cursor = -1;
	static short mdn;
	static short fil;
	static long nve_posi;

	mdn = this->mdn;
	fil = this->fil;
	nve_posi = nve_posi0;
	if (cursor == -1)
	{
		Lsnve.sqlin ((long *) &nve_posi,   2, 0);
		Lsnve.sqlin ((short *) &mdn, 1, 0);
		Lsnve.sqlin ((short *) &fil, 1, 0);
		Lsnve.sqlin ((char *)  lsnve.nve, 0, sizeof (lsnve.nve));
		cursor = Lsnve.sqlcursor ("update lsnve set nve_posi = ? "
		           "where mdn = ? "
				   "and fil = ? "
				   "and nve = ?");
	}
	Lsnve.sqlexecute (cursor);
}

void ReadNve::SetPalettePosNvePosi (long nve_posi0)
{
	static int cursor = -1;

	static short mdn;
	static short fil;
	static long nve_posi;

	mdn = this->mdn;
	fil = this->fil;
	nve_posi = nve_posi0;
	if (cursor == -1)
	{
		Lsnve.sqlin ((long *) &nve_posi,   2, 0);
		Lsnve.sqlin ((short *) &mdn, 1, 0);
		Lsnve.sqlin ((short *) &fil, 1, 0);
		Lsnve.sqlin ((char *)  lsnve.nve_palette, 0, sizeof (lsnve.nve));
		cursor = Lsnve.sqlcursor ("update lsnve set nve_posi = ? "
		           "where mdn = ? "
				   "and fil = ? "
				   "and nve_palette = ?");
	}
	Lsnve.sqlexecute (cursor);
}

void ReadNve::UpdateNve (long ls0, Text& nve_palette, long nve_posi0)
{

	static int cursor1 = -1;
	static int cursor2 = -1;
	static short mdn;
	static short fil;
	static long ls;
	static long nve_posi;

	mdn = this->mdn;
	fil = this->fil;
	ls  = ls0;
	strcpy (lsnve.nve, nve.GetBuffer ());
	nve_posi = nve_posi0;

	if (cursor1 == -1)
	{
		nve.Format ("%09.0lf", (double) atof (nve.GetBuffer ())); 
		strcpy (lsnve.nve, nve.GetBuffer ());
		Lsnve.sqlin ((short *) &mdn, 1, 0);
		Lsnve.sqlin ((char *)  lsnve.nve, 0, sizeof (lsnve.nve));
		Lsnve.sqlout ((long *) &lsnve.ls, 2, 0);
		cursor1 = Lsnve.sqlcursor ("select ls from lsnve "
		                            "where mdn = ? "
									"and nve = ?");
	}
	Lsnve.sqlopen (cursor1);
	int dsqlstatus = Lsnve.sqlfetch (cursor1);
	if (dsqlstatus == 100)
	{
		return;
	}
	dsqlstatus = Lsnve.dbreadfirst ();
	if (dsqlstatus == 100)
	{
		return;
	}

    nve_palette.Format ("%09.0lf", (double) atof (nve_palette.GetBuffer ())); 
	strcpy (lsnve.nve_palette, nve_palette.GetBuffer ());
	if (cursor2 == -1)
	{
		Lsnve.sqlin ((long *)  &ls, 2, 0);
		Lsnve.sqlin ((char *)  lsnve.nve_palette, 0, sizeof (lsnve.nve_palette));
		Lsnve.sqlin ((long *)  &nve_posi, 2, 0);

		Lsnve.sqlin ((short *) &mdn, 1, 0);
		Lsnve.sqlin ((char *)  lsnve.nve, 0, sizeof (lsnve.nve));
		cursor2 = Lsnve.sqlcursor ("update lsnve set ls = ?, nve_palette = ?, "
		           "nve_posi = ? " 
                   "where mdn = ? "
		  	       "and nve = ?");
	}
	Lsnve.sqlexecute (cursor2);
}
		            
void ReadNve::SetMhd (LPSTR mhd)
{
    this->mhd = dasc_to_long (mhd);
}
