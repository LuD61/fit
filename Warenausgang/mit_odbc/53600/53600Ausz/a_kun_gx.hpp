#ifndef _A_KUN_GX_DEF
#define _A_KUN_GX_DEF
#include "dbclass.h"

table a_kun_gx

extern struct A_KUN_GX a_kun_gx, a_kun_gx_null;

class A_KUN_GX_CLASS : public DB_CLASS
{
     private :
       int cursor_bra;
     public :  
       A_KUN_GX_CLASS () : DB_CLASS (), cursor_bra (-1)
       {
       }
       void prepare (void);
       int dbreadfirst (void);
       int dbread_bra (void);
};

#endif