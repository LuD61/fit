#ifndef _PERS_DEF
#define _PERS_DEF

#include "dbclass.h"

struct PERS {
   short     abt;
   long      adr;
   short     anz_tag_wo;
   double    arb_zeit_tag;
   double    arb_zeit_wo;
   char      beschaef_kz[2];
   long      eintr_dat;
   short     fil;
   short     kost_st;
   short     mdn;
   char      pers[13];
   long      pers_mde;
   double    pers_pkt;
   double    rab_proz;
   double    std_satz;
   char      taet[25];
   char      taet_kz[2];
   short     tar_gr;
   char      zahlw[2];
   long      txt_nr;
   short     delstatus;
};
extern struct PERS pers, pers_null;

#line 7 "pers.rh"

class PERS_CLASS : public DB_CLASS 
{
       public :
               PERS_CLASS () : DB_CLASS ()
               {
               }
       virtual void prepare (void);
};
#endif
