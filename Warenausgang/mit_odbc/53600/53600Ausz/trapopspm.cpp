#include <windows.h>
#include "wmask.h"
#include "dbclass.h"
#include "mo_curso.h"
#include "trapopspm.h"

struct TRAPOPSPM trapopspm, trapopspm_null;

void TRAPOPSPM_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &trapopspm.mdn, 1, 0);
            ins_quest ((char *) &trapopspm.ls, 2, 0);     
            ins_quest ((char *) trapopspm.blg_typ, 0, sizeof (trapopspm.blg_typ));     
            ins_quest ((char *) &trapopspm.lfd, 2, 0);     
    out_quest ((char *) &trapopspm.mdn,1,0);
    out_quest ((char *) &trapopspm.fil,1,0);
    out_quest ((char *) &trapopspm.ls,2,0);
    out_quest ((char *) trapopspm.blg_typ,0,2);
    out_quest ((char *) &trapopspm.lfd,2,0);
    out_quest ((char *) &trapopspm.pm,3,0);
    out_quest ((char *) &trapopspm.pmzahl,3,0);
    out_quest ((char *) &trapopspm.ps1,3,0);
    out_quest ((char *) &trapopspm.ps1zahl,3,0);
    out_quest ((char *) &trapopspm.ps2,3,0);
    out_quest ((char *) &trapopspm.ps2zahl,3,0);
    out_quest ((char *) &trapopspm.ps3,3,0);
    out_quest ((char *) &trapopspm.ps3zahl,3,0);
    out_quest ((char *) &trapopspm.ps4,3,0);
    out_quest ((char *) &trapopspm.ps4zahl,3,0);
            cursor = prepare_sql ("select trapopspm.mdn,  "
"trapopspm.fil,  trapopspm.ls,  trapopspm.blg_typ,  trapopspm.lfd,  "
"trapopspm.pm,  trapopspm.pmzahl,  trapopspm.ps1,  trapopspm.ps1zahl,  "
"trapopspm.ps2,  trapopspm.ps2zahl,  trapopspm.ps3,  trapopspm.ps3zahl,  "
"trapopspm.ps4,  trapopspm.ps4zahl from trapopspm "

#line 18 "trapopspm.rpp"
                                  "where mdn = ? "
                                  "and   ls   = ? "
                                  "and   blg_typ   = ? "
                                  "and   lfd   = ?");

    ins_quest ((char *) &trapopspm.mdn,1,0);
    ins_quest ((char *) &trapopspm.fil,1,0);
    ins_quest ((char *) &trapopspm.ls,2,0);
    ins_quest ((char *) trapopspm.blg_typ,0,2);
    ins_quest ((char *) &trapopspm.lfd,2,0);
    ins_quest ((char *) &trapopspm.pm,3,0);
    ins_quest ((char *) &trapopspm.pmzahl,3,0);
    ins_quest ((char *) &trapopspm.ps1,3,0);
    ins_quest ((char *) &trapopspm.ps1zahl,3,0);
    ins_quest ((char *) &trapopspm.ps2,3,0);
    ins_quest ((char *) &trapopspm.ps2zahl,3,0);
    ins_quest ((char *) &trapopspm.ps3,3,0);
    ins_quest ((char *) &trapopspm.ps3zahl,3,0);
    ins_quest ((char *) &trapopspm.ps4,3,0);
    ins_quest ((char *) &trapopspm.ps4zahl,3,0);
            sqltext = "update trapopspm set "
"trapopspm.mdn = ?,  trapopspm.fil = ?,  trapopspm.ls = ?,  "
"trapopspm.blg_typ = ?,  trapopspm.lfd = ?,  trapopspm.pm = ?,  "
"trapopspm.pmzahl = ?,  trapopspm.ps1 = ?,  trapopspm.ps1zahl = ?,  "
"trapopspm.ps2 = ?,  trapopspm.ps2zahl = ?,  trapopspm.ps3 = ?,  "
"trapopspm.ps3zahl = ?,  trapopspm.ps4 = ?,  trapopspm.ps4zahl = ? "

#line 24 "trapopspm.rpp"
                                  "where mdn = ? "
                                  "and   ls   = ? "
                                  "and   blg_typ   = ? "
                                  "and   lfd   = ?";
  

            ins_quest ((char *) &trapopspm.mdn, 1, 0);
            ins_quest ((char *) &trapopspm.ls, 2, 0);     
            ins_quest ((char *) trapopspm.blg_typ, 0, sizeof (trapopspm.blg_typ));     
            ins_quest ((char *) &trapopspm.lfd, 2, 0);     
            upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &trapopspm.mdn,1,0);
    ins_quest ((char *) &trapopspm.fil,1,0);
    ins_quest ((char *) &trapopspm.ls,2,0);
    ins_quest ((char *) trapopspm.blg_typ,0,2);
    ins_quest ((char *) &trapopspm.lfd,2,0);
    ins_quest ((char *) &trapopspm.pm,3,0);
    ins_quest ((char *) &trapopspm.pmzahl,3,0);
    ins_quest ((char *) &trapopspm.ps1,3,0);
    ins_quest ((char *) &trapopspm.ps1zahl,3,0);
    ins_quest ((char *) &trapopspm.ps2,3,0);
    ins_quest ((char *) &trapopspm.ps2zahl,3,0);
    ins_quest ((char *) &trapopspm.ps3,3,0);
    ins_quest ((char *) &trapopspm.ps3zahl,3,0);
    ins_quest ((char *) &trapopspm.ps4,3,0);
    ins_quest ((char *) &trapopspm.ps4zahl,3,0);
            ins_cursor = prepare_sql ("insert into trapopspm ("
"mdn,  fil,  ls,  blg_typ,  lfd,  pm,  pmzahl,  ps1,  ps1zahl,  ps2,  ps2zahl,  ps3,  ps3zahl,  ps4,  "
"ps4zahl) "

#line 37 "trapopspm.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?)"); 

#line 39 "trapopspm.rpp"

            ins_quest ((char *) &trapopspm.mdn, 1, 0);
            ins_quest ((char *) &trapopspm.ls, 2, 0);     
            ins_quest ((char *) trapopspm.blg_typ, 0, sizeof (trapopspm.blg_typ));     
            ins_quest ((char *) &trapopspm.lfd, 2, 0);     
            test_upd_cursor = prepare_sql ("select mdn from trapopspm "
                                  "where mdn = ? "
                                  "and   ls   = ? "
                                  "and   blg_typ   = ? "
                                  "and   lfd   = ?");

            ins_quest ((char *) &trapopspm.mdn, 1, 0);
            ins_quest ((char *) &trapopspm.ls, 2, 0);     
            ins_quest ((char *) trapopspm.blg_typ, 0, sizeof (trapopspm.blg_typ));     
            ins_quest ((char *) &trapopspm.lfd, 2, 0);     
            del_cursor = prepare_sql ("delete from trapopspm "
                                  "where mdn = ? "
                                  "and   ls   = ? "
                                  "and   blg_typ   = ? "
                                  "and   lfd   = ?");

}

