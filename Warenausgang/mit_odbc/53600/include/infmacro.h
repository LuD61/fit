#ifndef _INFMACRO_DEF
#define _INFMACRO_DEF
#include <stdio.h>

class InfMacro 
{
  private :
      char *McName;
      FILE *inf;
      BOOL Loaded;

  public :
      InfMacro (char *);
      ~InfMacro ();
      BOOL LoadMacro (void);
      BOOL RunMacro (char *, char *, char *);
      BOOL RunMacro (char *, char *, char *, char *);
};
#endif