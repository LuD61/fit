#ifndef _MO_CURSOR_DEF
#define _MO_CURSOR_DEF
#ifndef DBN
/*** 
--------------------------------------------------------------------------------
-
-       BSA         System-Engineering         BSA - Sued
-       Postfach 10 01 64, 7460 Balingen, Tel.: 07433/2413
-
--------------------------------------------------------------------------------
-
-       Modulname               :       sqlfkt.h
-
-       Autor                   :       W.Roth
-       Erstellungsdatum        :       09.06.1992
-       Modifikationsdatum      :       09.06.1992
-
-       Projekt                 :       BWS
-       Version                 :       1.00
-       Laendervariante         :       BRD
-
-       Rechner                 :       UNISYS 386
-       Betriebssystem          :       XENIX  386
-       Sprache                 :       ESQL-C
-
-       Aenderungsjournal       :
-
-       lfd.    Version Datum           Name       Beschreibung
-       ------------------------------------------------------------------------
--------------------------------------------------------------------------------
-
-       Modulbeschreibung       :	Definitionen zu sqlfkt.ec
-					
--------------------------------------------------------------------------------
***/
#include <sqltypes.h>
#include <sqlca.h>
#include <sqlda.h>
#include <sqlhdr.h>
#include <sqlproto.h>

#define SQLCHAR		0
#define SQLSMINT	1
#define SQLINT		2
#define SQLFLOAT	3
#define SQLSMFLOAT	4
#define SQLDECIMAL	5
#define SQLSERIAL	6
#define SQLDATE		7

#define CCHARTYPE	100
#define CSHORTTYPE	101
#define CINTTYPE	102
#define CLONGTYPE	103
#define CFLOATTYPE	104
#define CDOUBLETYPE	105

#define sqlstatus sqlca.sqlcode 
#define sqlerror sqlca.sqlerrd

#define fields   q_desc->sqld
#define fieldinf q_desc->sqlvar

#define cfields    rcurs[cu_nr].fields
#define cfieldinf  rcurs[cu_nr].fieldinf

#define asqlind  vind[i]
#define csqlind  rcurs[cu_nr].asqlind
#define cvind    rcurs[cu_nr].vind

#define NEXT 1
#define PRIOR 2
#define PREVIOUS 2
#define FIRST 3
#define LAST 4
#define CURRENT 5
#define RELATIV 7
#define DBABSOLUTE 6

#ifndef BOOL
#define BOOL int
#endif

typedef struct 
         { short typ;
           short len;
         } TyLen;

extern short DEBUG;

  /* Funktion, um Werte aus sqlwerte zu holen */
/* ******************************************************************** */

int sqlwert ();                      /* Adresse der Zielvariablen        */ 
                                     /* wird als Argument uebergeben     */

char *csqlvalue (short);             /* String            */
char *csqlnvalue (char *);

short ssqlvalue (short);             /* Strinvariable     */
short ssqlnvalue (char *);

long isqlvalue (short);              /* Longwert          */
long isqlnvalue (char *);

double dsqlvalue (short);           /* Doublewert        */
double dsqlnvalue (char *);

/* ******************************************************************** */

short sqltyp (short);             
                                 /* Funktion, um Typ aus select zu holen */
short sqllen (short);            /* Laenge eines Feldes           */
char *sqlname (short);           /* Name eines SQL-Feldes         */
char *sqlmalloc (int);           /* Speicher fuer Selectwerte zuordnen */
void  sqlfree ();                /* Speicher fuer sqlwerte freigeben       */  
int   sqlfields (void);          /* Anzahl SQL-Felder             */
char *sqlformat (short);         /* Format fuer SQL-Feld          */
void  printfield (short);        /* SQL-Feld anzeigen             */
void  sprintfield (char *,short); 
                                 /* SQL-Feld formatiert in String uebertr. */
int   selectcurs (int);          /* Cursor auswaehlen             */

int   execute_sql (char *, ...);      /* Aus der Datenbank lesen       */
int   prepare_sql (char *, ...);      /* Cursor vorbereiten und oeffnen */
int   prepare_scroll (char *, ...);   /* Cursor vorbereiten und oeffnen */
int   prepare_spez (int, char *, ...);/* Cursor vorbereiten und oeffnen */
int   open_sql (short);               /* Cursor vorbereiten und oeffnen */
int   fetch_sql (short);              /* Cursor nach Nummer lesen       */
int   fetch_scroll (short, int, ...); /* Scroll-Cursor lesen            */
int   close_sql (short);              /* Cursor freigeben               */
int   close_cursor (short);           /* Cursor schliessen               */
int   execute_curs (short);           /* Cursor ausfuehren              */
int   select_curs (short);            /* Cursor auswaehlen              */

char *usformat (char *);              /* Format fuer feld aus forms holen    */
int   insformat (char *, char *); 
                                   /* Format fuer feld in forms laden     */
void  delformate (void);           /* forms loeschen                      */
short feld_nr (char *);            /* Feldnummer zu Feldnamen suchen      */

char *get_colstring (char *);
                                     /* Typ als String zurueckgeben.         */
int get_coltyp_spez  (char *, char *);
                                     /* Typ eines Feldes holen und konvert.  */
int get_coltyp  (char *, char *);
                                     /* Typ eines Feldes holen.              */
int get_collen  (char *, char *);
                                     /* Laenge eines Feldes holen.           */
short dec_len (short, short *, short *);
int short_null  (short);             /* NULLVALUE fuer shortwerte            */
int long_null   (long);              /* NULLVALUE fuer longwerte             */
int double_null (double);            /* NULLVALUE fuer doublewerte           */
TyLen get_feldlen (char *);          /* Feldlaenge und Typ aus String holen */
TyLen len_aus_typ (char *, short);
short get_spez_typ (short);          /* Typ konvertieren.          */
                                     /* Decimallaenge in Vor - und Nachkomma */
char *cursor_name (short);           /* Cursor-Namen zu Nummer holen */

void opendbase (char *);
void closedbase (void);
BOOL IsInWork ();
void beginwork (void);
void rollbackwork (void);
void commitwork (void);
int ins_quest (char *, short, short);
int out_quest (char *,short, short);
void set_sqlolen (short , short);
void dropdbase (char *);
void createdbase (char *);
void cursor_modus (int, ...);
void debug_sql (char *);
int sql_prot (short, char *);
int showcurs (short, short);
int save_sql (char *, short);
int dlong_to_asc (long, char *);
long dasc_to_long (char *);
int IsShortnull (short);
int IsShortnull (short);
int IsLongnull (long);
int IsDoublenull (double);
void init_sqlout (void);
void init_sqlin (void);
void SetCSqlDebug (int);
#endif
#endif