#include <windows.h>
#include "Process.h"


CProcess::CProcess ()
{
	Init ();
}

CProcess::CProcess (LPSTR Command)
{
	this->Command = Command;
	Init ();
}

CProcess::CProcess (Text& Command)
{
	this->Command = Command;
	ShowMode = SW_SHOWNORMAL;
	Init ();
}

CProcess::~CProcess ()
{
}

void CProcess::Init ()
{
	x = -1;
	y = -1;
	cx = -1;
	cx = -1;
}

void CProcess::SetCommand (LPSTR Command)
{
	this->Command = Command;
}

void CProcess::SetCommand (Text& Command)
{
	this->Command = Command;
}

void CProcess::SetSize (int x, int y, int cx, int cy)
{
	this->x = x;
	this->y = y;
	this->cx = cx;
	this->cx = cy;
}

void CProcess::SetShowMode (WORD ShowMode)
{
	this->ShowMode = ShowMode;
}

HANDLE CProcess::Start ()
{
	return Start (ShowMode);
}

HANDLE CProcess::GetPid ()
{
	return Pid;
}

BOOL CProcess::Stop ()
{
    if (Pid != NULL)
	{
		TerminateProcess (Pid, 0);
        CloseHandle (Pid);
	}
	return TRUE;
}

BOOL CProcess::IsActive ()
{
        DWORD ExitCode; 

		if (Pid == NULL)
		{
			return FALSE;
		}
		GetExitCodeProcess (Pid, &ExitCode);
		if (ExitCode == STILL_ACTIVE)
		{
			      return TRUE;
		}
		CloseHandle (Pid);
        return FALSE;
}

HANDLE CProcess::Start (WORD ShowMode)
{
		int ret;

	    Pid = NULL;
 	    this->ShowMode = ShowMode;
        ZeroMemory (&sti, sizeof (sti));
        sti.cb = sizeof (sti);
        if (x != -1)
        {
                     sti.dwFlags |= STARTF_USEPOSITION;
                     sti.dwX = x;
                     sti.dwY = y;
        }

        if (cx != -1)
        {
                     sti.dwFlags |= STARTF_USESIZE;
                     sti.dwXSize = cx;
                     sti.dwYSize = cy;
        }
        sti.wShowWindow = ShowMode;
        sti.dwFlags |= STARTF_USESHOWWINDOW;

        ret = CreateProcess (NULL,
                       Command.GetBuffer (),
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       &sti,
                       &pi);

        if (ret)
        {
                  CloseHandle (pi.hThread);
                  Pid = pi.hProcess;
	    }
		return Pid;
}


DWORD CProcess::WaitForEnd ()
{

		if (Pid == NULL)
		{
			return -1;
		}
		GetExitCodeProcess (Pid, &ExitCode);
		while (ExitCode == STILL_ACTIVE)
		{
#ifndef CONSOLE
			      MSG msg;

				  if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE) != NULL)
				  {
					  TranslateMessage (&msg);
					  DispatchMessage (&msg);
					  Sleep (10);
				  }

#endif
                  GetExitCodeProcess (Pid, &ExitCode);
		}
        CloseHandle (Pid);
        return ExitCode;
}



