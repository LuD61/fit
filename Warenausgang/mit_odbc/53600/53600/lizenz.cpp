#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <windows.h>
#include "mo_curso.h"
#include "strfkt.h"
#include "lizenz.h"
#include "wmask.h"
#include "cmask.h"
#include "datum.h"


mfont TextFont1 = {
//                  "Comic Sans MS", 
//                  "Times New Roman", 
                  "Impact", 
                   200, 
                   0, 
                   0, 
                   REDCOL, 
                   LTGRAYCOL,
                   0, 0};


mfont TextFont = {
                  "Times New Roman", 
                   150, 
                   0, 
                   0, 
                   BLACKCOL, 
                   LTGRAYCOL,
                   0, 0};

mfont BuFont = {
                  "Times New Roman", 
                   150, 
                   0, 
                   1, 
                   BLACKCOL, 
                   LTGRAYCOL,
                   0, 0};

mfont PidFontR = {
                  "MS SAN SERIF", 
                   150, 
                   0, 
                   0, 
                   REDCOL, 
                   LTGRAYCOL,
                   0, 0};

mfont PidFont = {
                  "MS SAN SERIF", 
                   150, 
                   0, 
                   0, 
                   BLACKCOL, 
                   LTGRAYCOL,
                   0, 0};

static char *text[] = {"Ihre Lizenz ist abgelaufen.",
                       " ",
					   "Sofort nach dem Begleichen Ihrer Rechnung erhalten Sie ",
					   "von uns eine neue unbegrenzte Lizenz per Post oder �ber",
					   "Ferndiagnose.",
					   NULL,
};

static mfont *textfont[] = {&TextFont1, 
                            &TextFont,
							&TextFont,
							&TextFont,
							&TextFont,
							NULL,
};

static char *textpid[] = {"Es sind noch Processe aktiv !!",
                          " ",
					      "Beenden Sie zuerst alle Processe ",
					      "und beenden Sie dann FIT",
					       NULL,
};

static mfont *pidfont[]  = {&PidFontR, 
                            &PidFont,
							&PidFont,
							&PidFont,
							NULL,
};



static char *textdisable[] = {" ",
	                          "Lizenz ung�ltig !!",
 					          NULL,
};

static mfont *disablefont[] = {&TextFont1, 
                               &TextFont1,
					    		NULL,
};

static char *warten[] = {"Bitte warten...",
					     "Lizenz-Countdown wird durchgef�hrt.",
					     NULL,
};

static mfont *wartenfont[] = {&TextFont, 
                              &TextFont,
 							  NULL
};


#define BYTES 14
BOOL LC::TestTable ()
{
	cursor = DbClass.sqlcursor ("select ldat from lc");
	if (cursor < 0)
	{
		cursor = -1;
	}
	if (cursor != -1)
	{
		DbClass.sqlclose (cursor);
        cursor = -1; 
        return TRUE;
	}
	return FALSE;

}


void LC::prepare ()
{
	if (!TestTable ())
	{
		return;
	}
	DbClass.sqlout ((long *)  &ldat,  SQLLONG, 0);
	DbClass.sqlout ((short *) &tag,   SQLSHORT, 0);
	DbClass.sqlout ((short *) &mon,   SQLSHORT, 0);
	DbClass.sqlout ((short *) &jr,    SQLSHORT, 0);
	DbClass.sqlout ((long *)  &ldat2, SQLLONG, 0);
	cursor = DbClass.sqlcursor ("select ldat, tag, mon, jr, ldat2 from lc");
}

int LC::dbreadfirst ()
{
	int dsqlstatus = -1;
	extern short sql_mode;
	short sql_s;

	if (cursor == -2)
	{
		return dsqlstatus;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	if (cursor == -1)
	{
		prepare ();
	}
	if (cursor != -1)
	{
		dsqlstatus = DbClass.sqlopen (cursor);
		if (dsqlstatus == 0)
		{
			dsqlstatus = DbClass.sqlfetch (cursor);
		}
	}
	else
	{
		cursor = -2;
	}
	sql_mode = sql_s;
	return dsqlstatus;
}

void LC::ToBuffer (char *buffer)
{
	memcpy (&buffer[0], &ldat, 4);
	memcpy (&buffer[4], &tag,  2);
	memcpy (&buffer[6], &mon,  2);
	memcpy (&buffer[8], &jr,   2);
	memcpy (&buffer[10],&ldat2,4);
}


int FITL::TestDatum (char *buffer)
{
          short tag;
          short mon;
          short jr;
          char rot[11];
          long diff;
		  long ldat;
		  long ldat2;

		  memcpy (&ldat, &buffer[0], sizeof (long));
          memcpy (&tag,  &buffer[4], sizeof (short)); 
          memcpy (&mon,  &buffer[6], sizeof (short)); 
          memcpy (&jr,   &buffer[8], sizeof (short)); 
		  memcpy (&ldat2,&buffer[10], sizeof (long));
		  ldat2 -= 19890921;
		  if (ldat != ldat2) return 1;
          sprintf (rot, "%02hd.%02hd.%02hd", tag, mon, jr);
          if (strcmp (rot, "05.07.1954") == 0) return 0;

          ldat2 = get_dat_long(rot,10);
		  if (ldat != ldat2) return 1;

          sprintf (datum, "%02hd.%02hd.%04hd", tag,mon,jr);
          sysdate (sysdat);
          ldat    = dasc_to_long (datum);
          lsysdat = dasc_to_long (sysdat);
          diff = lsysdat - ldat;

//		  if (diff < 0) return 1;

		  ldiff = diff;
          if (diff > 62) return 3;
          if (diff > 31) return 2;
          return 0;
}

int FITL::TestLizenz (void)
{
          char *serv160;
          FILE *fp;
          char buffer [512];
          int anz;
          int i;
		  BOOL netpath; 

		  if (lc.dbreadfirst () == 0)
		  {
			  lc.ToBuffer (buffer);
		  }
		  else
		  {
			  serv160 = getenv ("SERVPATH");
			  if (serv160)
			  {
				  strcpy (buffer, serv160);
			  }
			  else
			  {
				  serv160 = getenv ("SERV160");
				  if (serv160 == NULL) return 1;

				  if (memcmp (serv160, "\\\\", 2) == 0)
				  {
						 netpath = TRUE;
				  }
				  else
				  {
						 netpath = FALSE;
				  }

				  anz = wsplit (serv160, "\\");
				  if (anz == 0) return 1;

				  if (netpath)
				  {
						sprintf (buffer, "\\\\%s", wort[0]); 
				  }
				  else
				  {
						strcpy (buffer, wort[0]);
				  }
 
				  for (i = 1; i < anz - 1; i ++)
				  {
					  strcat (buffer, "\\");
					  strcat (buffer, wort[i]);
				  }
				  strcat (buffer, "\\");
				  strcat (buffer, "texte");
			  }
			  strcat (buffer, "\\51108.cmg");
			  fp = fopen (buffer, "rb");
			  if (fp == NULL) return 1;

			  if (fread (buffer, 1, BYTES, fp) < BYTES) return 1;
			  fclose (fp);
		  }
          return (TestDatum (buffer));
}

void FITL::LizenzMessage (HANDLE hInstance, HWND hMainWindow)
{

		  Msg.CMessageTextOK (hInstance, hMainWindow, text, textfont, &TextFont, &BuFont);
		  Msg.ProcessMessages ();
		  Msg.CDestroyText ();
		  Msg.CMessageText (hInstance, hMainWindow, warten, wartenfont, &TextFont);
		  Sleep (1000 * (ldiff - 31));
		  Msg.CDestroyText ();
}

void FITL::DisableLizenz (HANDLE hInstance, HWND hMainWindow)
{

		  Msg.CMessageTextOK (hInstance, hMainWindow, textdisable, disablefont, &TextFont1, &TextFont1);
		  Msg.ProcessMessages ();
		  Msg.CDestroyText ();
}

void FITL::Processes (HANDLE hInstance, HWND hMainWindow)
{

		  Msg.CMessageTextOK (hInstance, hMainWindow, textpid, pidfont, &PidFont, &PidFont);
		  Msg.ProcessMessages ();
		  Msg.CDestroyText ();
}

           
 

          

          
                    
          