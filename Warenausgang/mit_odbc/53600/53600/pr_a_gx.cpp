#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "pr_a_gx.h"

struct PR_AUSZ_GX pr_ausz_gx, pr_ausz_gx_null;

void PR_AUSZ_GX_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &pr_ausz_gx.text_nr,   2, 0);
            ins_quest ((char *) &pr_ausz_gx.eti_typ,   1, 0);
    out_quest ((char *) &pr_ausz_gx.text_nr,2,0);
    out_quest ((char *) &pr_ausz_gx.eti_typ,1,0);
    out_quest ((char *) &pr_ausz_gx.sg1,1,0);
    out_quest ((char *) pr_ausz_gx.txt1,0,71);
    out_quest ((char *) &pr_ausz_gx.sg2,1,0);
    out_quest ((char *) pr_ausz_gx.txt2,0,71);
    out_quest ((char *) &pr_ausz_gx.sg3,1,0);
    out_quest ((char *) pr_ausz_gx.txt3,0,71);
    out_quest ((char *) &pr_ausz_gx.sg4,1,0);
    out_quest ((char *) pr_ausz_gx.txt4,0,71);
    out_quest ((char *) &pr_ausz_gx.sg5,1,0);
    out_quest ((char *) pr_ausz_gx.txt5,0,71);
    out_quest ((char *) &pr_ausz_gx.sg6,1,0);
    out_quest ((char *) pr_ausz_gx.txt6,0,71);
    out_quest ((char *) &pr_ausz_gx.sg7,1,0);
    out_quest ((char *) pr_ausz_gx.txt7,0,71);
    out_quest ((char *) &pr_ausz_gx.sg8,1,0);
    out_quest ((char *) pr_ausz_gx.txt8,0,71);
    out_quest ((char *) &pr_ausz_gx.sg9,1,0);
    out_quest ((char *) pr_ausz_gx.txt9,0,71);
    out_quest ((char *) &pr_ausz_gx.sg10,1,0);
    out_quest ((char *) pr_ausz_gx.txt10,0,71);
    out_quest ((char *) &pr_ausz_gx.sg11,1,0);
    out_quest ((char *) pr_ausz_gx.txt11,0,71);
    out_quest ((char *) &pr_ausz_gx.sg12,1,0);
    out_quest ((char *) pr_ausz_gx.txt12,0,71);
    out_quest ((char *) &pr_ausz_gx.sg13,1,0);
    out_quest ((char *) pr_ausz_gx.txt13,0,71);
    out_quest ((char *) &pr_ausz_gx.sg14,1,0);
    out_quest ((char *) pr_ausz_gx.txt14,0,71);
    out_quest ((char *) &pr_ausz_gx.sg15,1,0);
    out_quest ((char *) pr_ausz_gx.txt15,0,71);
    out_quest ((char *) &pr_ausz_gx.sg16,1,0);
    out_quest ((char *) pr_ausz_gx.txt16,0,71);
    out_quest ((char *) &pr_ausz_gx.sg17,1,0);
    out_quest ((char *) pr_ausz_gx.txt17,0,71);
    out_quest ((char *) &pr_ausz_gx.sg18,1,0);
    out_quest ((char *) pr_ausz_gx.txt18,0,71);
    out_quest ((char *) &pr_ausz_gx.sg19,1,0);
    out_quest ((char *) pr_ausz_gx.txt19,0,71);
    out_quest ((char *) &pr_ausz_gx.sg20,1,0);
    out_quest ((char *) pr_ausz_gx.txt20,0,71);
            cursor = prepare_sql ("select pr_ausz_gx.text_nr,  "
"pr_ausz_gx.eti_typ,  pr_ausz_gx.sg1,  pr_ausz_gx.txt1,  "
"pr_ausz_gx.sg2,  pr_ausz_gx.txt2,  pr_ausz_gx.sg3,  pr_ausz_gx.txt3,  "
"pr_ausz_gx.sg4,  pr_ausz_gx.txt4,  pr_ausz_gx.sg5,  pr_ausz_gx.txt5,  "
"pr_ausz_gx.sg6,  pr_ausz_gx.txt6,  pr_ausz_gx.sg7,  pr_ausz_gx.txt7,  "
"pr_ausz_gx.sg8,  pr_ausz_gx.txt8,  pr_ausz_gx.sg9,  pr_ausz_gx.txt9,  "
"pr_ausz_gx.sg10,  pr_ausz_gx.txt10,  pr_ausz_gx.sg11,  "
"pr_ausz_gx.txt11,  pr_ausz_gx.sg12,  pr_ausz_gx.txt12,  "
"pr_ausz_gx.sg13,  pr_ausz_gx.txt13,  pr_ausz_gx.sg14,  "
"pr_ausz_gx.txt14,  pr_ausz_gx.sg15,  pr_ausz_gx.txt15,  "
"pr_ausz_gx.sg16,  pr_ausz_gx.txt16,  pr_ausz_gx.sg17,  "
"pr_ausz_gx.txt17,  pr_ausz_gx.sg18,  pr_ausz_gx.txt18,  "
"pr_ausz_gx.sg19,  pr_ausz_gx.txt19,  pr_ausz_gx.sg20,  "
"pr_ausz_gx.txt20 from pr_ausz_gx "

#line 25 "pr_a_gx.rpp"
                                  "where text_nr = ? "
                                  "and   eti_typ = ?");

    ins_quest ((char *) &pr_ausz_gx.text_nr,2,0);
    ins_quest ((char *) &pr_ausz_gx.eti_typ,1,0);
    ins_quest ((char *) &pr_ausz_gx.sg1,1,0);
    ins_quest ((char *) pr_ausz_gx.txt1,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg2,1,0);
    ins_quest ((char *) pr_ausz_gx.txt2,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg3,1,0);
    ins_quest ((char *) pr_ausz_gx.txt3,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg4,1,0);
    ins_quest ((char *) pr_ausz_gx.txt4,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg5,1,0);
    ins_quest ((char *) pr_ausz_gx.txt5,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg6,1,0);
    ins_quest ((char *) pr_ausz_gx.txt6,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg7,1,0);
    ins_quest ((char *) pr_ausz_gx.txt7,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg8,1,0);
    ins_quest ((char *) pr_ausz_gx.txt8,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg9,1,0);
    ins_quest ((char *) pr_ausz_gx.txt9,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg10,1,0);
    ins_quest ((char *) pr_ausz_gx.txt10,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg11,1,0);
    ins_quest ((char *) pr_ausz_gx.txt11,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg12,1,0);
    ins_quest ((char *) pr_ausz_gx.txt12,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg13,1,0);
    ins_quest ((char *) pr_ausz_gx.txt13,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg14,1,0);
    ins_quest ((char *) pr_ausz_gx.txt14,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg15,1,0);
    ins_quest ((char *) pr_ausz_gx.txt15,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg16,1,0);
    ins_quest ((char *) pr_ausz_gx.txt16,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg17,1,0);
    ins_quest ((char *) pr_ausz_gx.txt17,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg18,1,0);
    ins_quest ((char *) pr_ausz_gx.txt18,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg19,1,0);
    ins_quest ((char *) pr_ausz_gx.txt19,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg20,1,0);
    ins_quest ((char *) pr_ausz_gx.txt20,0,71);
            sqltext = "update pr_ausz_gx set "
"pr_ausz_gx.text_nr = ?,  pr_ausz_gx.eti_typ = ?,  "
"pr_ausz_gx.sg1 = ?,  pr_ausz_gx.txt1 = ?,  pr_ausz_gx.sg2 = ?,  "
"pr_ausz_gx.txt2 = ?,  pr_ausz_gx.sg3 = ?,  pr_ausz_gx.txt3 = ?,  "
"pr_ausz_gx.sg4 = ?,  pr_ausz_gx.txt4 = ?,  pr_ausz_gx.sg5 = ?,  "
"pr_ausz_gx.txt5 = ?,  pr_ausz_gx.sg6 = ?,  pr_ausz_gx.txt6 = ?,  "
"pr_ausz_gx.sg7 = ?,  pr_ausz_gx.txt7 = ?,  pr_ausz_gx.sg8 = ?,  "
"pr_ausz_gx.txt8 = ?,  pr_ausz_gx.sg9 = ?,  pr_ausz_gx.txt9 = ?,  "
"pr_ausz_gx.sg10 = ?,  pr_ausz_gx.txt10 = ?,  pr_ausz_gx.sg11 = ?,  "
"pr_ausz_gx.txt11 = ?,  pr_ausz_gx.sg12 = ?,  pr_ausz_gx.txt12 = ?,  "
"pr_ausz_gx.sg13 = ?,  pr_ausz_gx.txt13 = ?,  pr_ausz_gx.sg14 = ?,  "
"pr_ausz_gx.txt14 = ?,  pr_ausz_gx.sg15 = ?,  pr_ausz_gx.txt15 = ?,  "
"pr_ausz_gx.sg16 = ?,  pr_ausz_gx.txt16 = ?,  pr_ausz_gx.sg17 = ?,  "
"pr_ausz_gx.txt17 = ?,  pr_ausz_gx.sg18 = ?,  pr_ausz_gx.txt18 = ?,  "
"pr_ausz_gx.sg19 = ?,  pr_ausz_gx.txt19 = ?,  pr_ausz_gx.sg20 = ?,  "
"pr_ausz_gx.txt20 = ? "

#line 29 "pr_a_gx.rpp"
                                  "where text_nr = ? "
                                  "and   eti_typ = ?";

            ins_quest ((char *) &pr_ausz_gx.text_nr,   2, 0);
            ins_quest ((char *) &pr_ausz_gx.eti_typ,   1, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &pr_ausz_gx.text_nr,   2, 0);
            ins_quest ((char *) &pr_ausz_gx.eti_typ,   1, 0);
            test_upd_cursor = prepare_sql ("select text_nr from pr_ausz_gx "
                                  "where text_nr = ? "
                                  "and   eti_typ = ?");

            ins_quest ((char *) &pr_ausz_gx.text_nr,   2, 0);
            ins_quest ((char *) &pr_ausz_gx.eti_typ,   1, 0);
            del_cursor = prepare_sql ("delete from pr_ausz_gx "
                                  "where text_nr = ? "
                                  "and   eti_typ = ?");
    ins_quest ((char *) &pr_ausz_gx.text_nr,2,0);
    ins_quest ((char *) &pr_ausz_gx.eti_typ,1,0);
    ins_quest ((char *) &pr_ausz_gx.sg1,1,0);
    ins_quest ((char *) pr_ausz_gx.txt1,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg2,1,0);
    ins_quest ((char *) pr_ausz_gx.txt2,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg3,1,0);
    ins_quest ((char *) pr_ausz_gx.txt3,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg4,1,0);
    ins_quest ((char *) pr_ausz_gx.txt4,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg5,1,0);
    ins_quest ((char *) pr_ausz_gx.txt5,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg6,1,0);
    ins_quest ((char *) pr_ausz_gx.txt6,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg7,1,0);
    ins_quest ((char *) pr_ausz_gx.txt7,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg8,1,0);
    ins_quest ((char *) pr_ausz_gx.txt8,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg9,1,0);
    ins_quest ((char *) pr_ausz_gx.txt9,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg10,1,0);
    ins_quest ((char *) pr_ausz_gx.txt10,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg11,1,0);
    ins_quest ((char *) pr_ausz_gx.txt11,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg12,1,0);
    ins_quest ((char *) pr_ausz_gx.txt12,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg13,1,0);
    ins_quest ((char *) pr_ausz_gx.txt13,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg14,1,0);
    ins_quest ((char *) pr_ausz_gx.txt14,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg15,1,0);
    ins_quest ((char *) pr_ausz_gx.txt15,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg16,1,0);
    ins_quest ((char *) pr_ausz_gx.txt16,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg17,1,0);
    ins_quest ((char *) pr_ausz_gx.txt17,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg18,1,0);
    ins_quest ((char *) pr_ausz_gx.txt18,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg19,1,0);
    ins_quest ((char *) pr_ausz_gx.txt19,0,71);
    ins_quest ((char *) &pr_ausz_gx.sg20,1,0);
    ins_quest ((char *) pr_ausz_gx.txt20,0,71);
            ins_cursor = prepare_sql ("insert into pr_ausz_gx ("
"text_nr,  eti_typ,  sg1,  txt1,  sg2,  txt2,  sg3,  txt3,  sg4,  txt4,  sg5,  txt5,  sg6,  txt6,  sg7,  "
"txt7,  sg8,  txt8,  sg9,  txt9,  sg10,  txt10,  sg11,  txt11,  sg12,  txt12,  sg13,  txt13,  sg14,  "
"txt14,  sg15,  txt15,  sg16,  txt16,  sg17,  txt17,  sg18,  txt18,  sg19,  txt19,  sg20,  txt20) "

#line 48 "pr_a_gx.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?)"); 

#line 50 "pr_a_gx.rpp"
}

int PR_AUSZ_GX_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}
