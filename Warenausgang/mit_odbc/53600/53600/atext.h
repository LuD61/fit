#ifndef ATEXTE_DEF
#define ATEXTE_DEF
#include "dbclass.h"

struct ATEXTE {
   long      sys;
   short     waa;
   long      txt_nr;
   char      txt[1025];
   char      disp_txt[1025];
   short     alignment;
   short     send_ctrl;
   short     txt_typ;
   short     txt_platz;
   double    a;
};
extern struct ATEXTE atexte, atexte_null;

class ATEXTE_CLASS
{
       private:
                int cursor_atexte;
                int cursor_atexte_a;
                void prepare_atexte (void);
                void prepare_atexte_a (void);
       public:
                ATEXTE_CLASS ()
                {
                   cursor_atexte = -1;
                   cursor_atexte_a = -1;
                }
	        int lese_atexte (long);
			int lese_atexte_a (double);
};

#endif
