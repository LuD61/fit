// Stc.cpp: Implementierung der Klasse CStc.
//
//////////////////////////////////////////////////////////////////////
#include <windows.h>
#include "wmask.h"
#include "Stc.h"
#include "strfkt.h"
#include "mo_curso.h"

#define MAXTRIES 100
#define TRIEWARNING1 50
#define TRIEWARNING2 80

void Stc::prepare ()
{
	sql_s = (SQL_MODE) sql_mode;
	sql_mode = (int) AppAction;
	sqlout ((short *) &stc_number, SQLSHORT, 0);
	sqlout ((char *) &kun_name, SQLCHAR, sizeof (kun_name));
	sqlout ((char *) &text, SQLCHAR, sizeof (text));
	sqlout ((short *) &try_count, SQLSHORT, 0);
	cursor = sqlcursor ("select stc_number, kun_name, text, try_count from stc");
	if (cursor < 0)
	{
		dsqlstatus = sqlcomm ((char *) sql_create.c_str ());
		if (dsqlstatus == 0)
		{
				cursor = sqlcursor ("select stc_number, kun_name, text, try_count from stc");
		}
	}
	sql_mode = (int) sql_s;

	sqlin ((short *) &stc_number, SQLSHORT, 0);
	sqlin ((char *) kun_name, SQLCHAR, sizeof (kun_name));
	sqlin ((char *) text, SQLCHAR, sizeof (text));
	sqlin ((short *) &try_count, SQLSHORT, 0);
	upd_cursor = sqlcursor ("update stc set stc_number = ?, kun_name = ?, text = ?, try_count = ?");
	sqlin ((short *) &stc_number, SQLSHORT, 0);
	sqlin ((char *) kun_name, SQLCHAR, sizeof (kun_name));
	sqlin ((char *) text, SQLCHAR, sizeof (text));
	sqlin ((short *) &try_count, SQLSHORT, 0);
	ins_cursor = sqlcursor ("insert into stc (stc_number, kun_name, text, try_count) values (?,?,?,?)");
	test_upd_cursor = sqlcursor ("select * from stc");
	del_cursor = sqlcursor ("delete from stc");

}

Stc::Stc ()
{
	sql_create = "create table stc (" 
				 "stc_number smallint, "
				 "kun_name char (36), "
				 "text char (80), "
				 "try_count smallint "
				 ") in fit_dat extent size 32 next size 256 lock mode row";
	strcpy (kun_name, "SE.TEC");
	strcpy (text, "Telefon : 07423-81095-0");
	try_count = 0;
	prepare ();
}

Stc::~Stc ()
{
}

void Stc::IncTryCount ()
{
	if (dbreadfirst () == 0)
	{
		try_count ++;
		dbupdate ();
	}
}

void Stcu::prepare ()
{
	sql_s = (SQL_MODE) sql_mode;
	sql_mode = (int) AppAction;
	sqlout ((char *) &computer, SQLCHAR, sizeof (computer));
	sqlout ((char *) &user, SQLCHAR, sizeof (user));
	cursor = sqlcursor ("select computer, fit_user from stcu");
	if (cursor < 0)
	{
		dsqlstatus = sqlcomm ((char *) sql_create.c_str ());
/*
		if (dsqlstatus == 0)
		{
			dsqlstatus = sqlcomm ((char *) idx_create.c_str ());
		}
*/
		if (dsqlstatus == 0)
		{
			cursor = sqlcursor ("select computer, user from stcu");
		}
	}
	sql_mode = (int) sql_s;

	sqlin ((char *) &computer, SQLCHAR, sizeof (computer));
	sqlin ((char *) &user, SQLCHAR, sizeof (user));
	test_cursor = sqlcursor ("select * from stcu where computer = ? and fit_user = ?");

	sqlin ((char *) &computer, SQLCHAR, sizeof (computer));
	sqlin ((char *) &user, SQLCHAR, sizeof (user));
	ins_cursor = sqlcursor ("insert into stcu (computer, fit_user) values (?,?)");
	sqlin ((char *) &computer, SQLCHAR, sizeof (computer));
	sqlin ((char *) &user, SQLCHAR, sizeof (user));
	del_cursor = sqlcursor ("delete from stcu where computer = ? and fit_user = ?");
	sqlout ((long *) &count, SQLLONG, 0);
	count_cursor = sqlcursor ("select count (*) from stcu");

}

Stcu::Stcu ()
{
	sql_create = "create table stcu (" 
				 "computer char (30), "
				 "fit_user char (30) "
				 ") in fit_dat extent size 32 next size 256 lock mode row";
	idx_create = "create unique index stcu01 on stcu (computer,user)";
	prepare ();
}

Stcu::~Stcu ()
{
	sqlclose (count_cursor); 
	sqlclose (test_cursor); 
}

long Stcu::GetCount ()
{
	count = 0l;
	sqlopen (count_cursor);
	sqlfetch (count_cursor);
	return count;
}

BOOL Stcu::Exist ()
{
	int ret = FALSE;
	sqlopen (test_cursor);
	dsqlstatus = sqlfetch (test_cursor);
	if (dsqlstatus == 0)
	{
		ret = TRUE;
	}
	return ret;
}

int Stcu::Insert ()
{
	return sqlexecute (ins_cursor);
}

int Stcu::Delete ()
{
	return sqlexecute (del_cursor);
}


void Stcp::prepare ()
{
	sql_s = (SQL_MODE) sql_mode;
	sql_mode = (int) AppAction;
	sqlout ((long *) &dat, SQLLONG, 0);
	sqlout ((char *) time, SQLCHAR, sizeof (time));
	sqlout ((char *) text, SQLCHAR, sizeof (text));
	cursor = sqlcursor ("select dat, time, text from stcp");
	if (cursor < 0)
	{
		dsqlstatus = sqlcomm ((char *) sql_create.c_str ());
/*
		if (dsqlstatus == 0)
		{
			dsqlstatus = sqlcomm ((char *) idx_create.c_str ());
		}
*/
		if (dsqlstatus == 0)
		{
			cursor = sqlcursor ("select date, time, text from stcp");
		}
	}
	sql_mode = (int) sql_s;

	sqlin ((long *) &dat, SQLLONG, 0);
	sqlin ((char *) time, SQLCHAR, sizeof (time));
	sqlin ((char *) text, SQLCHAR, sizeof (text));
	ins_cursor = sqlcursor ("insert into stcp (dat, time, text) values (?,?, ?)");
}


Stcp::Stcp ()
{
	sql_create = "create table stcp (" 
				 "dat date, "
				 "time char (9), "
				 "text char (255) "
				 ") in fit_dat extent size 32 next size 256 lock mode row";
	prepare ();
}

Stcp::~Stcp ()
{
}

int Stcp::Insert ()
{
	char cdate [12];
	char ctime [10];
	
	sysdate (cdate);
	systime (ctime);
	dat = dasc_to_long (cdate);
	strcpy (time, ctime);
	return sqlexecute (ins_cursor);
}


CStc *CStc::Instance = NULL;

CStc::CStc()
{
	m_Stc  = new Stc ();
	m_Stcu = new Stcu ();
	m_Stcp = new Stcp ();
	TryError = NoError;
	ErrorText[NoError] = "";
	ErrorText[NoWarning] = "";
	ErrorText[FirstWarning]  = "Warnung : Sie können das System noch 50 mal benutzen";
	ErrorText[SecondWarning] = "Warnung : Sie können das System noch 20 mal benutzen";
	ErrorText[Abbort]        = "Achtung : Das System kann nicht mehr benutzt werden";
}

CStc::~CStc()
{
	delete m_Stc;
	delete m_Stcu;
	delete m_Stcp;
}


CStc *CStc::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CStc ();
	}
	return Instance;
}


void CStc::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

char *CStc::GetErrorText (char *text, int size)
{
	char *ret = (char *) ErrorText[TryError].c_str ();
	if (text != NULL && size != 0)
	{
		memset (text, 0, size);
		strncpy (text, ret, size - 1);
		ret = text;
	}
	return ret;
}


bool CStc::SetLicenceCount (short licence_count)
{
	    bool ret = true;
		m_Stc->SetStcNumber (licence_count);
		m_Stc->dbupdate ();
		return ret;
}

bool CStc::TestLicenceCount ()
{
	bool ret = true;
	char *text;
	int licence_count = 0;
	int user_count = 1;
	int dsqlstatus;

	m_Stc->sqlcomm ("begin work");
	dsqlstatus = m_Stc->dbreadfirst ();
	if (dsqlstatus == 100)
	{
		m_Stc->SetStcNumber (licence_count);
		m_Stc->dbupdate ();
	}
	else
	{
		licence_count = m_Stc->GetStcNumber ();
	}
	TryError = NoError;
	if (licence_count > 0)
	{
		user_count = m_Stcu->GetCount ();
		if (user_count >= licence_count)
		{
			m_Stc->IncTryCount ();
			short try_count = m_Stc->GetTryCount ();
			if (try_count >= MAXTRIES)
			{
				TryError = Abbort;
			}
			else if (try_count >=  TRIEWARNING2)
			{
				TryError = SecondWarning;
			}
			else if (try_count >= TRIEWARNING1)
			{
				TryError = FirstWarning;
			}
			else
			{
				TryError = NoWarning;
			}

			ret = false;
			m_Stcu->Insert ();
			text = m_Stcp->GetText ();
			sprintf (text, "Lizenzen bei Anmeldung überschritten : Lizenzen : %d, Benutzer : %d. Anzahl überschreitungen %d",
				            licence_count, user_count + 1, try_count);
			m_Stcp->Insert ();

		}
		else
		{
			m_Stcu->Insert ();
		}
	}
	m_Stc->sqlcomm ("commit work");
	return ret;
}


bool CStc::AddUser ()
{
	bool ret = true;
	char computer [21];
	char user [21];
	unsigned long length;

	memset (computer, 0, sizeof (computer));
	memset (user, 0, sizeof (user));

	length = sizeof (computer) - 1;
	if (!GetComputerName (computer, &length))
	{
		ret = false;
	}
    if (ret) 
	{
		length = sizeof (user) - 1;
		if (!GetUserName (user, &length))
		{
			ret = false;
		}
	}
	if (ret)
	{
		m_Stcu->SetComputer (computer);
		m_Stcu->SetUser (user);
		if (!m_Stcu->Exist ())
		{
			ret = TestLicenceCount ();
		}
	}
	return ret;
}

bool CStc::DropUser ()
{
	bool ret = true;
    m_Stcu->Delete ();
	return ret;
}


int CStc::GetUserCount ()
{
	return m_Stcu->GetCount ();
}

int CStc::GetLicenceCount ()
{
	return m_Stc->GetStcNumber ();
}

char *CStc::GetKunName ()
{
	return m_Stc->GetKunName ();
}

char *CStc::GetText ()
{
	return m_Stc->GetText ();
}