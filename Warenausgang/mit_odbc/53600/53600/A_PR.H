#ifndef A_PR_OK
#define A_PR_OK 1
struct A_PR
{
    double a;
    short akt;
    short delstatus;
    short fil;
    short fil_gr;
    double key_typ_dec13;
    short key_typ_sint;
    char lad_akv[2];
    char lief_akv[2];
    short mdn;
    short mdn_gr;
    char modif[2];
    double pr_ek;
    double pr_ek_euro;
    double pr_vk;
    double pr_vk_euro;
    long bearb;
    char pers_nam[9];
    double pr_vk1;
    double pr_vk2;
    double pr_vk3;
    };

extern struct A_PR _a_pr;

class A_PR_CLASS
{
       private :
            short cursor_a_pr;
            short cursor_a_pr_mdn;
            short cursor_a_pr_fil;
            short cursor_a_pr_h;
            short test_upd_cursor;
            short upd_cursor;
            short ins_cursor;
            short del_cursor;
            void prepare_mdn (void);       
            void prepare_fil (void);       
       public:
           A_PR_CLASS ()
           {
                      cursor_a_pr     = -1;
                      cursor_a_pr_mdn = -1;
                      cursor_a_pr_fil = -1;
                      cursor_a_pr_h   = -1;
                      test_upd_cursor = -1;
                      upd_cursor      = -1;
                      ins_cursor      = -1;
                      del_cursor      = -1;
           }
           void prepare (void);       
           void prepare_h (void);       
           int lese_a_pr (double);
           int lese_a_pr (void);
           int lese_a_pr_mdn (double, short);
           int lese_a_pr_mdn (void);
           int lese_a_pr_fil (double, short, short);
           int lese_a_pr_fil (void);
           int lese_a_pr_h (short, short, short, short, double);
           int lese_a_pr_h (void);
           int update_a_pr (double, short, short, short, short);
           int delete_a_pr (double, short, short, short, short);
           int gueltig_a_pr (void);
};
#endif
