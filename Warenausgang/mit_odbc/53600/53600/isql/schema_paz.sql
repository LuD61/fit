drop table pazlsk;
create table "guenter".pazlsk 
  (
    ls integer,
    adr integer,
    mdn smallint,
    auf integer,
    kun_fil smallint,
    kun integer,
    fil smallint,
    feld_bz1 char(19),
    lieferdat date,
    lieferzeit char(5),
    hinweis char(48),
    ls_stat smallint,
    kun_krz1 char(16),
    auf_sum decimal(8,2),
    feld_bz2 char(11),
    lim_er decimal(5,2),
    partner char(36),
    pr_lst integer,
    feld_bz3 char(7),
    pr_stu smallint,
    vertr integer,
    tou integer,
    adr_nam1 char(36),
    adr_nam2 char(36),
    pf char(16),
    str char(36),
    plz char(8),
    ort1 char(36),
    of_po decimal(12,2),
    delstatus smallint,
    rech integer,
    blg_typ char(1),
    zeit_dec decimal(4,2),
    kopf_txt integer,
    fuss_txt integer,
    inka_nr integer,
    auf_ext char(16),
    teil_smt integer,
    pers_nam char(8),
    brutto decimal(12,3),
    komm_dat date,
    of_ek decimal(12,2),
    of_po_euro decimal(12,2),
    of_po_fremd decimal(12,2),
    of_ek_euro decimal(12,2),
    of_ek_fremd decimal(12,2),
    waehrung smallint,
    ueb_kz char(1),
    gew decimal(8,3),
    auf_art smallint,
    gruppe smallint,
    ccmarkt smallint,
    fak_typ smallint,
    tou_nr integer,
    wieg_kompl smallint,
    best_dat date,
    hinweis2 char(30),
    hinweis3 char(30),
    lgr integer
  ) in fit_dat extent size 1000 next size 500 lock mode row;
revoke all on "guenter".pazlsk from "public";

create unique index "guenter".i01pazlsk on "guenter".pazlsk (ls,
    fil,mdn);
create index "guenter".i02pazlsk on "guenter".pazlsk (kun_fil,
    kun,ls_stat,mdn,fil);
create index "guenter".i03pazlsk on "guenter".pazlsk (mdn,fil,
    rech,blg_typ);
create index "guenter".i04pazlsk on "guenter".pazlsk (auf,fil,
    mdn);
create index "guenter".i05pazlsk on "guenter".pazlsk (inka_nr,
    kun_fil,ls_stat,mdn,fil);



drop table pazlsp;
create table "guenter".pazlsp 
  (
    mdn smallint,
    fil smallint,
    ls integer,
    kun integer,
    kun_bran2 char(2),
    a decimal(13,0),
    auf_me decimal(8,3),
    auf_me_bz char(6),
    lief_me decimal(8,3),
    lief_me_bz char(6),
    ls_vk_pr decimal(12,4),
    ls_lad_pr decimal(6,2),
    delstatus smallint,
    tara decimal(8,3),
    posi integer,
    lsp_txt integer,
    pos_stat smallint,
    sa_kz_sint smallint,
    erf_kz char(1),
    prov_satz decimal(4,2),
    leer_pos smallint,
    hbk_date date,
    hbk_date2 date,
    ls_charge char(20),
    auf_me_vgl decimal(12,3),
    ls_vk_euro decimal(12,4),
    ls_vk_fremd decimal(12,2),
    ls_lad_euro decimal(12,2),
    ls_lad_fremd decimal(12,2),
    rab_satz decimal(5,2),
    me_einh_kun smallint,
    me_einh smallint,
    me_einh_kun1 smallint,
    auf_me1 decimal(8,3),
    inh1 decimal(8,3),
    me_einh_kun2 smallint,
    auf_me2 decimal(8,3),
    inh2 decimal(8,3),
    me_einh_kun3 smallint,
    auf_me3 decimal(8,3),
    inh3 decimal(8,3),
    lief_me_bz_ist char(6),
    inh_ist decimal(8,3),
    me_einh_ist smallint,
    me_ist decimal(8,3),
    lager smallint,
    lief_me1 decimal(8,3),
    lief_me2 decimal(8,3),
    lief_me3 decimal(8,3),
    ls_pos_kz smallint,
    a_grund decimal(13,0),
    kond_art char(4),
    posi_ext integer
  ) in fit_dat extent size 2000 next size 500 lock mode row;
revoke all on "guenter".pazlsp from "public";

create index "guenter".i01pazlsp on "guenter".pazlsp (mdn,fil,
    ls,a);
create index "guenter".i02pazlsp on "guenter".pazlsp (ls,mdn);
    




