/***************************************************************************/
/* Programmname :  Log.CPP                                                 */
/*-------------------------------------------------------------------------*/
/* Funktion :  Klasse CLog                                               */
/*-------------------------------------------------------------------------*/
/* Revision : 1.0    Datum : 07.06.04   erstellt von : W. Roth             */
/*                   allgemneine Klasse zum Zerlegen von Strings           */
/*                                                                         */    
/*-------------------------------------------------------------------------*/
/* Aufruf :                                                                */
/* Funktionswert :                                                         */
/* Eingabeparameter :                                                      */
/*                                                                         */
/* Ausgabeparameter :                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
#include "windows.h"
#include "stdio.h"
#include <share.h>
#include "log.h"

CLog::CLog(void)
{
	LogName = "Log.log";
	fp = NULL;
	Active = FALSE;
}

CLog::~CLog(void)
{
	Close ();
}

void CLog::Open ()
{
	LPSTR tmp = getenv ("TMPPATH");
	if (tmp != NULL)
	{
		LogName = tmp;
		LogName += "\\";
		LogName += "Nve53600.log";
	}

	fp = fopen (LogName.GetBuffer (), "w");
}

void CLog::Close ()
{
	if (fp != NULL)
	{
		fclose (fp);
		fp = NULL;
	}
}

void CLog::Write ()
{
	Write (Line);
}

void CLog::Write (CString& Txt)
{
char date [12];
char time [10];
CString FileText;

if (!Active) return;
if (fp == NULL)
{
	Open ();
}
if (fp == NULL) return;

Sysdate (date);
Systime (time);

FileText.Format ("%s %s %s", date, time, Txt.GetBuffer ());
fprintf (fp, "%s\n", FileText.GetBuffer ()); 
fflush (fp);
}


void CLog::Sysdate (char *datum)
{
 SYSTEMTIME SysTime;
 GetSystemTime((LPSYSTEMTIME) &SysTime );
 sprintf (datum, "%02d.%02d.%04d", SysTime.wDay,
	                               SysTime.wMonth,
                                   SysTime.wYear);
}

void CLog::Systime (char *zeit)
{
 SYSTEMTIME SysTime;
 GetLocalTime((LPSYSTEMTIME) &SysTime );
 sprintf (zeit, "%02d:%02d:%02d", SysTime.wHour,
	                              SysTime.wMinute,
                                  SysTime.wSecond);
}


void CContLog::Open ()
{
	LPSTR tmp = getenv ("TMPPATH");
	if (tmp != NULL)
	{
		LogName = tmp;
		LogName += "\\";
		LogName += "Nve53600.log";
	}
	fp = fopen (LogName.GetBuffer (), "a");
}

void CRingLog::Open ()
{
	  char buffer [256];
	  long fpos;
	  long i;

	  if (!Active) return;
	  LPSTR tmp = getenv ("TMPPATH");
	  if (tmp != NULL)
	  {
		LogName = tmp;
		LogName += "\\";
		LogName += "Nve53600.log";
	  }
	  LogName.Trim ();
	  row = 0l;
	  if (fp == NULL)
	  {
			fp = _fsopen (LogName.GetBuffer (), "r+w", _SH_DENYNO);
			if (fp == NULL)
			{
				fp = fopen (LogName.GetBuffer (), "w");
			}
			if (fp != NULL)
			{
				fclose (fp);
				fp = _fsopen (LogName.GetBuffer (), "r+w", _SH_DENYNO);
			}
	   }
	   if (fp == NULL) 
	   {
		   int err = GetLastError ();
		   CString Error;
		   Error.Format ("Fehler %d beim �ffnen", err);
		   return;
	   }


	   fseek (fp, 0l, SEEK_SET);
	   if (fgets (buffer, 82, fp) == NULL)
	   {
		  row = (long) 1;
		  sprintf (buffer, "%ld", row);
          fprintf (fp, "%-78.78s\n", buffer);
          fpos = ftell (fp);
		  memset (buffer, '*', 78);
          fprintf (fp, "%-78.78s\n", buffer);
		  fflush (fp);
		  fseek (fp, fpos, SEEK_SET);
		  return;
	   }
	  row = atol (buffer);
	  i = 1;
	  while (fgets (buffer, 82, fp) != NULL)
	  {
		  i ++;
		  if (i == row)
		  {
			  break;
		  }
	  }
	  return;
}

void CRingLog::Write ()
{
	Write (Line);
}

void CRingLog::Write (CString& Txt)
{
	   long fpos = 0;
	   char date [12];
	   char time [10];
	   CString FileText;

	   if (!Active) return;
	   if (fp == NULL)
	   {
		   Open ();
	   }
	   if (fp == NULL) return;

	   Sysdate (date);
	   Systime (time);
	   FileText.Format ("%s %s | %s", date, time, Txt.GetBuffer ());

	   if (row >= maxrows)
	   {
		   row = 2;
	   }
	   else
	   {
		   row ++;
		   fpos = ftell (fp);
	   }

	   fseek (fp, 0l, SEEK_SET);
	   CString Row;
	   Row.Format ("%ld", row);
	   fprintf (fp, "%-78.78s\n", Row.GetBuffer ());
	   fflush (fp);
       if (fpos > 0l)
	   {
		   fseek (fp, fpos, SEEK_SET);
	   }

	   CString Buffer;
	   Buffer.Format ("%-78.78s", FileText.GetBuffer ());
	   fprintf (fp, "%s\n", Buffer.GetBuffer ());
	   fflush (fp);
	   if (row < maxrows)
	   {
        char buffer[80];
		fpos = ftell (fp);
 		memset (buffer, '*', 78);
		fprintf (fp, "%-78.78s\n", buffer);
		fflush (fp);
		fseek (fp, fpos, SEEK_SET);
	   }
}

void CFifoLog::Write (CString& Txt)
/**
Text mit Datum, Uhrzeit, Letzter Satz ist auf der 1. Zeile
**/
{
char date [12];
char time [10];
CString FileText;


if (!Active) return;
Sysdate (date);
Systime (time);

FileText.Format ("%s %s %s", date, time, Txt.GetBuffer ());
WriteFirstLine (FileText);
}

void CFifoLog::WriteFirstLine (CString& FileText)
{
FILE *in;
FILE *out; 
DWORD pid = 0;
int z;
char fname [256];
char buffer [256];

if (!Active) return;
pid = GetCurrentProcessId ();
sprintf (fname, "touchwrite.%ld", pid);
CopyFile (LogName.GetBuffer (), fname, (int) FALSE);
out = fopen (LogName.GetBuffer (), "w");
if(out == NULL) return;
fprintf (out, "%s\n", FileText.GetBuffer ());
in = fopen (fname, "r");
if(in == NULL)
{
	fclose (out);
	return;
}
z = 0;
while( fgets (buffer, 255, in) != NULL )
{
  fputs (buffer, out);
  if(z >= MAXLINES) break;
  z ++;
}
fclose (in);
fclose (out);
DeleteFile ((LPCTSTR) fname);
}



