#ifndef _TOUCHF_DEF
#define _TOUCHF_DEF
#include "nublock.h"

#define WM_DIRECT WM_USER + 100


class TOUCHF
{
      private :
      public :
		  int etianz;
	      int lastbu;
		  static BOOL EnterAnz;
		  TOUCHF ();
		  void SetWriteParams (void (*) (int), void (*) (int),void (*) (int), void (*) (int));
		  void SetWriteParams (void (*) (int), void (*) (int),void (*) (int), void (*) (int),
								void (*) (int));
		  void SetWriteParams (void (*) (int), void (*) (int),void (*) (int), void (*) (int),
								void (*) (int), void (*) (int));
          int FunkEnterBreak ();
          HWND IsChild (POINT *);
          int OnPaint (HWND,UINT,WPARAM,LPARAM);
          int OnButton (HWND,UINT,WPARAM,LPARAM);
          void RegisterFunk (WNDPROC);
          int GetAktButton (void);
          void PrevKey (void);
          void NextKey (void);
          void ActionKey (void);
          int IsFunkKey (MSG *);
          void MainInstance (HANDLE, HWND);
          void SetParent (HWND);
          void ScreenParam (double scrx, double scry);
          void EnterFunkBox (HWND, int, int, WNDPROC);
          void SetEtiAttr (char *, char *, char *, char *);
          void SetEtiAttr (char *, char *, char *, char *, char *);
          void SetEtiAttr (char *, char *, char *, char *, char *, char *);
          void SetBuText (ColButton&, char *);
          void SetEtiText (char *, char *, char *, char *);
          void SetEtiText (char *, char *, char *, char *, char *);
          void SetEtiText (char *, char *, char *, char *, char *, char *);
          void SetEtiPrinterReset (char *); //WAL-156
          void StartDirect ();
};
#endif