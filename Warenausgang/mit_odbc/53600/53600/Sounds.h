#include "Text.h"

#ifndef __SOUND_DEF
#define __SOUND_DEF
class CSounds
{
   private :
      	int len;
        int wait;
        UINT type;
		Text SoundName;

   public :
	   void SetSoundName (Text& SoundName)
	   {
		   this->SoundName = SoundName;
	   }
	   CSounds ();
	   CSounds (LPSTR);
	   CSounds (int, int, UINT);
	   void Run ();
	   void Play ();
	   void Play (int Wave);
};
#endif

