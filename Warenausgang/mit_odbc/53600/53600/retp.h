#ifndef _RETP_DEF
#define _RETP_DEF
#include "dbclass.h"

struct RETP {
   short     mdn;
   short     fil;
   long      ret;
   double    a;
   double    ret_me;
   double    ret_vk_pr;
   double    ret_lad_pr;
   short     delstatus;
   long      retp_txt;
   char      erf_kz[2];
   long      posi;
   double    tara;
   double    prov_satz;
   short     leer_pos;
   double    ret_vk_euro;
   double    ret_vk_fremd;
   double    ret_lad_euro;
   double    ret_lad_fremd;
   char      verfall[2];
   double    rab_satz;
   short     retp_kz;
};
extern struct RETP retp, retp_null;

#line 6 "retp.hpp"

extern struct RETP retp, retp_null;

class RETP_CLASS : public DB_CLASS
{
     private :
       int del_cursor_ret;
     public :  
       RETP_CLASS () : DB_CLASS (), del_cursor_ret (-1)
       {
       }
       void prepare (void);
       int dbreadfirst (void);
       int dbdelete_ret (void);
};

#endif