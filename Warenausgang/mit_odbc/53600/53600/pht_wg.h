#ifndef _PHT_WG_DEF
#define _PHT_WG_DEF

struct PHT_WG {
   short     mdn;
   short     fil;
   long      sys;
   short     prio_kng;
   short     wg;
   char      wg_bz1[25];
   long      zuord_dat;
   short     mf_zuord;
};

extern struct PHT_WG pht_wg, pht_wg_null;

class PHT_WG_CLASS
{
            private:
                int cursor;
                int cursor_wg;
            public:
                PHT_WG_CLASS () : cursor (-1),cursor_wg (-1)
                {
                }
                void prepare ();
                int dbreadfirst (void);
                int dbread (void);
                int dbreadfirst_wg (void);
                int dbread_wg (void);
};
#endif

