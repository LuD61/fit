#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "retp.h"

struct RETP retp, retp_null;

void RETP_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &retp.mdn, 1, 0);
            ins_quest ((char *) &retp.fil, 1, 0);
            ins_quest ((char *) &retp.ret, 2, 0);
            ins_quest ((char *) &retp.posi,2, 0);
            ins_quest ((char *) &retp.a,   3, 0);
    out_quest ((char *) &retp.mdn,1,0);
    out_quest ((char *) &retp.fil,1,0);
    out_quest ((char *) &retp.ret,2,0);
    out_quest ((char *) &retp.a,3,0);
    out_quest ((char *) &retp.ret_me,3,0);
    out_quest ((char *) &retp.ret_vk_pr,3,0);
    out_quest ((char *) &retp.ret_lad_pr,3,0);
    out_quest ((char *) &retp.delstatus,1,0);
    out_quest ((char *) &retp.retp_txt,2,0);
    out_quest ((char *) retp.erf_kz,0,2);
    out_quest ((char *) &retp.posi,2,0);
    out_quest ((char *) &retp.tara,3,0);
    out_quest ((char *) &retp.prov_satz,3,0);
    out_quest ((char *) &retp.leer_pos,1,0);
    out_quest ((char *) &retp.ret_vk_euro,3,0);
    out_quest ((char *) &retp.ret_vk_fremd,3,0);
    out_quest ((char *) &retp.ret_lad_euro,3,0);
    out_quest ((char *) &retp.ret_lad_fremd,3,0);
    out_quest ((char *) retp.verfall,0,2);
    out_quest ((char *) &retp.rab_satz,3,0);
    out_quest ((char *) &retp.retp_kz,1,0);
            cursor = prepare_sql ("select retp.mdn,  retp.fil,  "
"retp.ret,  retp.a,  retp.ret_me,  retp.ret_vk_pr,  retp.ret_lad_pr,  "
"retp.delstatus,  retp.retp_txt,  retp.erf_kz,  retp.posi,  retp.tara,  "
"retp.prov_satz,  retp.leer_pos,  retp.ret_vk_euro,  retp.ret_vk_fremd,  "
"retp.ret_lad_euro,  retp.ret_lad_fremd,  retp.verfall,  retp.rab_satz,  "
"retp.retp_kz from retp "

#line 28 "retp.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ret = ? "
                                  "and   posi = ? "
                                  "and   a   = ?");

    ins_quest ((char *) &retp.mdn,1,0);
    ins_quest ((char *) &retp.fil,1,0);
    ins_quest ((char *) &retp.ret,2,0);
    ins_quest ((char *) &retp.a,3,0);
    ins_quest ((char *) &retp.ret_me,3,0);
    ins_quest ((char *) &retp.ret_vk_pr,3,0);
    ins_quest ((char *) &retp.ret_lad_pr,3,0);
    ins_quest ((char *) &retp.delstatus,1,0);
    ins_quest ((char *) &retp.retp_txt,2,0);
    ins_quest ((char *) retp.erf_kz,0,2);
    ins_quest ((char *) &retp.posi,2,0);
    ins_quest ((char *) &retp.tara,3,0);
    ins_quest ((char *) &retp.prov_satz,3,0);
    ins_quest ((char *) &retp.leer_pos,1,0);
    ins_quest ((char *) &retp.ret_vk_euro,3,0);
    ins_quest ((char *) &retp.ret_vk_fremd,3,0);
    ins_quest ((char *) &retp.ret_lad_euro,3,0);
    ins_quest ((char *) &retp.ret_lad_fremd,3,0);
    ins_quest ((char *) retp.verfall,0,2);
    ins_quest ((char *) &retp.rab_satz,3,0);
    ins_quest ((char *) &retp.retp_kz,1,0);
            sqltext = "update retp set retp.mdn = ?,  "
"retp.fil = ?,  retp.ret = ?,  retp.a = ?,  retp.ret_me = ?,  "
"retp.ret_vk_pr = ?,  retp.ret_lad_pr = ?,  retp.delstatus = ?,  "
"retp.retp_txt = ?,  retp.erf_kz = ?,  retp.posi = ?,  retp.tara = ?,  "
"retp.prov_satz = ?,  retp.leer_pos = ?,  retp.ret_vk_euro = ?,  "
"retp.ret_vk_fremd = ?,  retp.ret_lad_euro = ?,  "
"retp.ret_lad_fremd = ?,  retp.verfall = ?,  retp.rab_satz = ?,  "
"retp.retp_kz = ? "

#line 35 "retp.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ret = ? "
                                  "and   posi = ? "
                                  "and   a   = ?";

            ins_quest ((char *) &retp.mdn, 1, 0);
            ins_quest ((char *) &retp.fil, 1, 0);
            ins_quest ((char *) &retp.ret, 2, 0);
            ins_quest ((char *) &retp.posi,2, 0);
            ins_quest ((char *) &retp.a,   3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &retp.mdn, 1, 0);
            ins_quest ((char *) &retp.fil, 1, 0);
            ins_quest ((char *) &retp.ret, 2, 0);
            ins_quest ((char *) &retp.posi,2, 0);
            ins_quest ((char *) &retp.a,   3, 0);
            test_upd_cursor = prepare_sql ("select ret from retp "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ret = ? "
                                  "and   posi = ? "
                                  "and   a   = ?");
            ins_quest ((char *) &retp.mdn, 1, 0);
            ins_quest ((char *) &retp.fil, 1, 0);
            ins_quest ((char *) &retp.ret, 2, 0);
            ins_quest ((char *) &retp.posi,2, 0);
            ins_quest ((char *) &retp.a,   3, 0);
            del_cursor = prepare_sql ("delete from retp "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ret = ? "
                                  "and   posi = ? "
                                  "and   a   = ?");
    ins_quest ((char *) &retp.mdn,1,0);
    ins_quest ((char *) &retp.fil,1,0);
    ins_quest ((char *) &retp.ret,2,0);
    ins_quest ((char *) &retp.a,3,0);
    ins_quest ((char *) &retp.ret_me,3,0);
    ins_quest ((char *) &retp.ret_vk_pr,3,0);
    ins_quest ((char *) &retp.ret_lad_pr,3,0);
    ins_quest ((char *) &retp.delstatus,1,0);
    ins_quest ((char *) &retp.retp_txt,2,0);
    ins_quest ((char *) retp.erf_kz,0,2);
    ins_quest ((char *) &retp.posi,2,0);
    ins_quest ((char *) &retp.tara,3,0);
    ins_quest ((char *) &retp.prov_satz,3,0);
    ins_quest ((char *) &retp.leer_pos,1,0);
    ins_quest ((char *) &retp.ret_vk_euro,3,0);
    ins_quest ((char *) &retp.ret_vk_fremd,3,0);
    ins_quest ((char *) &retp.ret_lad_euro,3,0);
    ins_quest ((char *) &retp.ret_lad_fremd,3,0);
    ins_quest ((char *) retp.verfall,0,2);
    ins_quest ((char *) &retp.rab_satz,3,0);
    ins_quest ((char *) &retp.retp_kz,1,0);
            ins_cursor = prepare_sql ("insert into retp (mdn,  "
"fil,  ret,  a,  ret_me,  ret_vk_pr,  ret_lad_pr,  delstatus,  retp_txt,  erf_kz,  posi,  "
"tara,  prov_satz,  leer_pos,  ret_vk_euro,  ret_vk_fremd,  ret_lad_euro,  "
"ret_lad_fremd,  verfall,  rab_satz,  retp_kz) "

#line 71 "retp.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?)"); 

#line 73 "retp.rpp"
            ins_quest ((char *) &retp.mdn, 1, 0);
            ins_quest ((char *) &retp.fil, 1, 0);
            ins_quest ((char *) &retp.ret, 2, 0);
            del_cursor_ret = prepare_sql ("delete from retp "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ret = ?");
}
int RETP_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int RETP_CLASS::dbdelete_ret (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         execute_curs (del_cursor_ret);
         return sqlstatus;
}
