#ifndef _NVEGEW_DEF
#define _NVEGEW_DEF
#include "Vector.h"

class NveGew
{
     private :
	   double gew;
	   CVector Kartons;

     public : 
	   double a;
	   NveGew ();
       ~NveGew ();
	   void Init ();
	   void SetGew (double);
	   double GetGew ();
	   void AddGew (double);
       void SubGew (double);
	   void AddKarton (void *);
       void *GetKarton (int);
       void DropKarton (int);
       void DropLastKarton ();
	   int size ();
};
#endif
