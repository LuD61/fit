#ifndef _PR_AUSZ_GX_DEF
#define _PR_AUSZ_GX_DEF
#include "dbclass.h"

struct PR_AUSZ_GX {
   long      text_nr;
   short     eti_typ;
   short     sg1;
   char      txt1[71];
   short     sg2;
   char      txt2[71];
   short     sg3;
   char      txt3[71];
   short     sg4;
   char      txt4[71];
   short     sg5;
   char      txt5[71];
   short     sg6;
   char      txt6[71];
   short     sg7;
   char      txt7[71];
   short     sg8;
   char      txt8[71];
   short     sg9;
   char      txt9[71];
   short     sg10;
   char      txt10[71];
   short     sg11;
   char      txt11[71];
   short     sg12;
   char      txt12[71];
   short     sg13;
   char      txt13[71];
   short     sg14;
   char      txt14[71];
   short     sg15;
   char      txt15[71];
   short     sg16;
   char      txt16[71];
   short     sg17;
   char      txt17[71];
   short     sg18;
   char      txt18[71];
   short     sg19;
   char      txt19[71];
   short     sg20;
   char      txt20[71];
};
extern struct PR_AUSZ_GX pr_ausz_gx, pr_ausz_gx_null;

#line 6 "pr_a_gx.rh"

extern struct PR_AUSZ_GX pr_ausz_gx, pr_ausz_gx_null;

class PR_AUSZ_GX_CLASS : public DB_CLASS
{
     private :
     public :  
       PR_AUSZ_GX_CLASS () : DB_CLASS ()
       {
       }
       void prepare (void);
       int dbreadfirst (void);
};

#endif