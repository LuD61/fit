
DBSCHEMA Schema-Dienstprogramm INFORMIX-SQL Version 7.31.FD7    
Copyright (C) Informix Software, Inc., 1984-1998
Software Serial Number AAC#J666821
{ TABLE "bws8000".cschlacht_betr row size = 117 number of columns = 7 index size 
              = 37 }
create table "bws8000".cschlacht_betr 
  (
    iln_schl char(13),
    es_nr integer not null ,
    schl_name char(30),
    schl_plz char(8),
    schl_ort char(30),
    schl_strasse char(30),
    delstatus smallint
  ) extent size 16 next size 16 lock mode row;
revoke all on "bws8000".cschlacht_betr from "public";

create unique index "bws8000".ix1152_1 on "bws8000".cschlacht_betr 
    (iln_schl);
create unique index "bws8000".ix1152_2 on "bws8000".cschlacht_betr 
    (es_nr);




