{ TABLE "bws8000".lsnve row size = 138 number of columns = 23 index size = 106 }
create table "bws8000".lsnve 
  (
    mdn smallint,
    fil smallint,
    nve_posi integer,
    nve char(9),
    nve_palette char(9),
    ls integer,
    a decimal(13,0),
    me decimal(8,3),
    me_einh smallint,
    gue smallint,
    palette_kz smallint,
    tag date,
    mhd date,
    brutto_gew decimal(12,3),
    verp_art integer,
    ps_art decimal(13,0),
    ps_stck decimal(12,3),
    ps_art1 decimal(13,0),
    ps_stck1 decimal(12,3),
    charge char(20),
    ins_dat date 
        default today,
    ins_zeit char(8),
    index integer
  ) extent size 32 next size 32 lock mode page;
revoke all on "bws8000".lsnve from "public";

create index "bws8000".i01lsnve on "bws8000".lsnve (ls,a,nve_posi,
    mdn,fil);
create index "bws8000".i02lsnve on "bws8000".lsnve (nve);
create index "bws8000".i03lsnve on "bws8000".lsnve (nve_palette);
    
create unique index "bws8000".i04lsnve on "bws8000".lsnve (mdn,
    fil,nve,tag);



create trigger "bws8000".t01_lsnve insert on "bws8000".lsnve referencing 
    new as new_row
    for each row
        (
        update "bws8000".lsnve set "bws8000".lsnve.ins_zeit = 
    CURRENT hour to second  where (ins_zeit IS NULL ) );


