#ifndef _RETK_DEF
#define _RETK_DEF
#include "dbclass.h"

struct RETK {
   short     mdn;
   short     fil;
   long      ret;
   long      ls;
   short     kun_fil;
   char      feld_bz1[20];
   long      kun;
   long      ret_dat;
   long      lieferdat;
   char      hinweis[49];
   short     ret_stat;
   char      feld_bz2[12];
   char      kun_krz1[17];
   char      feld_bz3[8];
   long      adr;
   short     delstatus;
   long      rech;
   char      blg_typ[2];
   long      kopf_txt;
   long      fuss_txt;
   long      vertr;
   long      inka_nr;
   double    of_po;
   short     teil_smt;
   char      pers_nam[9];
   double    of_ek;
   double    of_po_euro;
   double    of_po_fremd;
   double    of_ek_euro;
   double    of_ek_fremd;
   short     waehrung;
};
extern struct RETK retk, retk_null;

#line 6 "retk.hpp"

extern struct RETK retk, retk_null;

class RETK_CLASS : public DB_CLASS
{
      public :
       RETK_CLASS () : DB_CLASS ()
       {
       }
       void prepare (void);
       int dbreadfirst (void);
};

#endif