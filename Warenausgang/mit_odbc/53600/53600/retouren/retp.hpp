#ifndef _RETP_DEF
#define _RETP_DEF
#include "dbclass.h"

table retp

extern struct RETP retp, retp_null;

class RETP_CLASS : public DB_CLASS
{
     public :  
       RETP_CLASS () : DB_CLASS ()
       {
       }
       void prepare (void);
       int dbreadfirst (void);
};

#endif