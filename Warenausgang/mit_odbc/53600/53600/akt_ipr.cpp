#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "a_bas.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "akt_ipr.h"

struct AKT_IPR akt_ipr, akt_ipr_null;

void AKT_IPR_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &akt_ipr.mdn, 2, 0);
            ins_quest ((char *) &akt_ipr.a, 3, 0);
            ins_quest ((char *) &akt_ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_ipr.kun_pr, 2, 0);
            ins_quest ((char *) &akt_ipr.kun, 2, 0);
            ins_quest ((char *) &akt_ipr.aki_von, 2, 0);
    out_quest ((char *) &akt_ipr.mdn,2,0);
    out_quest ((char *) &akt_ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &akt_ipr.kun_pr,2,0);
    out_quest ((char *) &akt_ipr.a,3,0);
    out_quest ((char *) &akt_ipr.aki_pr,3,0);
    out_quest ((char *) &akt_ipr.aki_von,2,0);
    out_quest ((char *) &akt_ipr.aki_bis,2,0);
    out_quest ((char *) &akt_ipr.ld_pr,3,0);
    out_quest ((char *) &akt_ipr.aktion_nr,1,0);
    out_quest ((char *) akt_ipr.a_akt_kz,0,2);
    out_quest ((char *) akt_ipr.modif,0,2);
    out_quest ((char *) &akt_ipr.waehrung,1,0);
    out_quest ((char *) &akt_ipr.kun,2,0);
    out_quest ((char *) &akt_ipr.a_grund,3,0);
    out_quest ((char *) akt_ipr.kond_art,0,5);
            cursor = prepare_sql ("select akt_ipr.mdn,  "
"akt_ipr.pr_gr_stuf,  akt_ipr.kun_pr,  akt_ipr.a,  akt_ipr.aki_pr,  "
"akt_ipr.aki_von,  akt_ipr.aki_bis,  akt_ipr.ld_pr,  akt_ipr.aktion_nr,  "
"akt_ipr.a_akt_kz,  akt_ipr.modif,  akt_ipr.waehrung,  akt_ipr.kun,  "
"akt_ipr.a_grund,  akt_ipr.kond_art from akt_ipr "

#line 30 "akt_ipr.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ? "
                                  "and aki_von = ?"); 


    ins_quest ((char *) &akt_ipr.mdn,2,0);
    ins_quest ((char *) &akt_ipr.pr_gr_stuf,2,0);
    ins_quest ((char *) &akt_ipr.kun_pr,2,0);
    ins_quest ((char *) &akt_ipr.a,3,0);
    ins_quest ((char *) &akt_ipr.aki_pr,3,0);
    ins_quest ((char *) &akt_ipr.aki_von,2,0);
    ins_quest ((char *) &akt_ipr.aki_bis,2,0);
    ins_quest ((char *) &akt_ipr.ld_pr,3,0);
    ins_quest ((char *) &akt_ipr.aktion_nr,1,0);
    ins_quest ((char *) akt_ipr.a_akt_kz,0,2);
    ins_quest ((char *) akt_ipr.modif,0,2);
    ins_quest ((char *) &akt_ipr.waehrung,1,0);
    ins_quest ((char *) &akt_ipr.kun,2,0);
    ins_quest ((char *) &akt_ipr.a_grund,3,0);
    ins_quest ((char *) akt_ipr.kond_art,0,5);
            sqltext = "update akt_ipr set akt_ipr.mdn = ?,  "
"akt_ipr.pr_gr_stuf = ?,  akt_ipr.kun_pr = ?,  akt_ipr.a = ?,  "
"akt_ipr.aki_pr = ?,  akt_ipr.aki_von = ?,  akt_ipr.aki_bis = ?,  "
"akt_ipr.ld_pr = ?,  akt_ipr.aktion_nr = ?,  akt_ipr.a_akt_kz = ?,  "
"akt_ipr.modif = ?,  akt_ipr.waehrung = ?,  akt_ipr.kun = ?,  "
"akt_ipr.a_grund = ?,  akt_ipr.kond_art = ? "

#line 39 "akt_ipr.rpp"
                               "where mdn = ? "
                               "and   a = ? "
                               "and   pr_gr_stuf = ? "
                               "and   kun_pr = ? "
                               "and   kun = ? "
                               "and   aki_von = ?";
  
            ins_quest ((char *) &akt_ipr.mdn, 2, 0);
            ins_quest ((char *) &akt_ipr.a, 3, 0);
            ins_quest ((char *) &akt_ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_ipr.kun_pr,  2, 0);  
            ins_quest ((char *) &akt_ipr.kun,  2, 0);  
            ins_quest ((char *) &akt_ipr.aki_von, 2, 0);

            upd_cursor = prepare_sql (sqltext);

    ins_quest ((char *) &akt_ipr.mdn,2,0);
    ins_quest ((char *) &akt_ipr.pr_gr_stuf,2,0);
    ins_quest ((char *) &akt_ipr.kun_pr,2,0);
    ins_quest ((char *) &akt_ipr.a,3,0);
    ins_quest ((char *) &akt_ipr.aki_pr,3,0);
    ins_quest ((char *) &akt_ipr.aki_von,2,0);
    ins_quest ((char *) &akt_ipr.aki_bis,2,0);
    ins_quest ((char *) &akt_ipr.ld_pr,3,0);
    ins_quest ((char *) &akt_ipr.aktion_nr,1,0);
    ins_quest ((char *) akt_ipr.a_akt_kz,0,2);
    ins_quest ((char *) akt_ipr.modif,0,2);
    ins_quest ((char *) &akt_ipr.waehrung,1,0);
    ins_quest ((char *) &akt_ipr.kun,2,0);
    ins_quest ((char *) &akt_ipr.a_grund,3,0);
    ins_quest ((char *) akt_ipr.kond_art,0,5);
            ins_cursor = prepare_sql ("insert into akt_ipr ("
"mdn,  pr_gr_stuf,  kun_pr,  a,  aki_pr,  aki_von,  aki_bis,  ld_pr,  aktion_nr,  "
"a_akt_kz,  modif,  waehrung,  kun,  a_grund,  kond_art) "

#line 56 "akt_ipr.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?)"); 

#line 58 "akt_ipr.rpp"
            ins_quest ((char *) &akt_ipr.mdn, 2, 0);
            ins_quest ((char *) &akt_ipr.a, 3, 0);
            ins_quest ((char *) &akt_ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_ipr.kun_pr,  2, 0);  
            ins_quest ((char *) &akt_ipr.kun,  2, 0);  
            ins_quest ((char *) &akt_ipr.aki_von, 2, 0);
            test_upd_cursor = prepare_sql ("select a from akt_ipr "
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ? "
                                  "and aki_von = ?"); 
              
            ins_quest ((char *) &akt_ipr.mdn, 2, 0);
            ins_quest ((char *) &akt_ipr.a, 3, 0);
            ins_quest ((char *) &akt_ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_ipr.kun_pr,  2, 0);  
            ins_quest ((char *) &akt_ipr.kun,  2, 0);  
            ins_quest ((char *) &akt_ipr.aki_von, 2, 0);
            del_cursor = prepare_sql ("delete from akt_ipr "
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? "
                                  "and kun = ? "
                                  "and aki_von = ?"); 
}

void AKT_IPR_CLASS::prepare_a (char *order)
{
            ins_quest ((char *) &akt_ipr.mdn, 2, 0);
            ins_quest ((char *) &akt_ipr.a, 3, 0);
            if (order)
            {
    out_quest ((char *) &akt_ipr.mdn,2,0);
    out_quest ((char *) &akt_ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &akt_ipr.kun_pr,2,0);
    out_quest ((char *) &akt_ipr.a,3,0);
    out_quest ((char *) &akt_ipr.aki_pr,3,0);
    out_quest ((char *) &akt_ipr.aki_von,2,0);
    out_quest ((char *) &akt_ipr.aki_bis,2,0);
    out_quest ((char *) &akt_ipr.ld_pr,3,0);
    out_quest ((char *) &akt_ipr.aktion_nr,1,0);
    out_quest ((char *) akt_ipr.a_akt_kz,0,2);
    out_quest ((char *) akt_ipr.modif,0,2);
    out_quest ((char *) &akt_ipr.waehrung,1,0);
    out_quest ((char *) &akt_ipr.kun,2,0);
    out_quest ((char *) &akt_ipr.a_grund,3,0);
    out_quest ((char *) akt_ipr.kond_art,0,5);
                        cursor_a = prepare_sql ("select "
"akt_ipr.mdn,  akt_ipr.pr_gr_stuf,  akt_ipr.kun_pr,  akt_ipr.a,  "
"akt_ipr.aki_pr,  akt_ipr.aki_von,  akt_ipr.aki_bis,  akt_ipr.ld_pr,  "
"akt_ipr.aktion_nr,  akt_ipr.a_akt_kz,  akt_ipr.modif,  "
"akt_ipr.waehrung,  akt_ipr.kun,  akt_ipr.a_grund,  akt_ipr.kond_art from akt_ipr "

#line 94 "akt_ipr.rpp"
                                  "where mdn = ? "
                                  "and   a   = ? "
                                  "%s", order);
            }
            else
            {
    out_quest ((char *) &akt_ipr.mdn,2,0);
    out_quest ((char *) &akt_ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &akt_ipr.kun_pr,2,0);
    out_quest ((char *) &akt_ipr.a,3,0);
    out_quest ((char *) &akt_ipr.aki_pr,3,0);
    out_quest ((char *) &akt_ipr.aki_von,2,0);
    out_quest ((char *) &akt_ipr.aki_bis,2,0);
    out_quest ((char *) &akt_ipr.ld_pr,3,0);
    out_quest ((char *) &akt_ipr.aktion_nr,1,0);
    out_quest ((char *) akt_ipr.a_akt_kz,0,2);
    out_quest ((char *) akt_ipr.modif,0,2);
    out_quest ((char *) &akt_ipr.waehrung,1,0);
    out_quest ((char *) &akt_ipr.kun,2,0);
    out_quest ((char *) &akt_ipr.a_grund,3,0);
    out_quest ((char *) akt_ipr.kond_art,0,5);
                        cursor_a = prepare_sql ("select "
"akt_ipr.mdn,  akt_ipr.pr_gr_stuf,  akt_ipr.kun_pr,  akt_ipr.a,  "
"akt_ipr.aki_pr,  akt_ipr.aki_von,  akt_ipr.aki_bis,  akt_ipr.ld_pr,  "
"akt_ipr.aktion_nr,  akt_ipr.a_akt_kz,  akt_ipr.modif,  "
"akt_ipr.waehrung,  akt_ipr.kun,  akt_ipr.a_grund,  akt_ipr.kond_art from akt_ipr "

#line 101 "akt_ipr.rpp"
                                  "where mdn = ? "
                                  "and   a   = ?");
            }
            ins_quest ((char *) &akt_ipr.mdn, 2, 0);
            ins_quest ((char *) &akt_ipr.a, 3, 0);
            del_a_curs = prepare_sql ("delete from akt_ipr "
                                  "where mdn = ? "
                                  "and   a   = ?");
}

void AKT_IPR_CLASS::prepare_gr (char *order)
{
            ins_quest ((char *) &akt_ipr.mdn, 2, 0);
            ins_quest ((char *) &akt_ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_ipr.kun_pr, 2, 0);
            if (order)
            {
    out_quest ((char *) &akt_ipr.mdn,2,0);
    out_quest ((char *) &akt_ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &akt_ipr.kun_pr,2,0);
    out_quest ((char *) &akt_ipr.a,3,0);
    out_quest ((char *) &akt_ipr.aki_pr,3,0);
    out_quest ((char *) &akt_ipr.aki_von,2,0);
    out_quest ((char *) &akt_ipr.aki_bis,2,0);
    out_quest ((char *) &akt_ipr.ld_pr,3,0);
    out_quest ((char *) &akt_ipr.aktion_nr,1,0);
    out_quest ((char *) akt_ipr.a_akt_kz,0,2);
    out_quest ((char *) akt_ipr.modif,0,2);
    out_quest ((char *) &akt_ipr.waehrung,1,0);
    out_quest ((char *) &akt_ipr.kun,2,0);
    out_quest ((char *) &akt_ipr.a_grund,3,0);
    out_quest ((char *) akt_ipr.kond_art,0,5);
                        cursor_gr = prepare_sql ("select "
"akt_ipr.mdn,  akt_ipr.pr_gr_stuf,  akt_ipr.kun_pr,  akt_ipr.a,  "
"akt_ipr.aki_pr,  akt_ipr.aki_von,  akt_ipr.aki_bis,  akt_ipr.ld_pr,  "
"akt_ipr.aktion_nr,  akt_ipr.a_akt_kz,  akt_ipr.modif,  "
"akt_ipr.waehrung,  akt_ipr.kun,  akt_ipr.a_grund,  akt_ipr.kond_art from akt_ipr "

#line 119 "akt_ipr.rpp"
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ? "
                                  "%s", order);
            }
            else
            {
    out_quest ((char *) &akt_ipr.mdn,2,0);
    out_quest ((char *) &akt_ipr.pr_gr_stuf,2,0);
    out_quest ((char *) &akt_ipr.kun_pr,2,0);
    out_quest ((char *) &akt_ipr.a,3,0);
    out_quest ((char *) &akt_ipr.aki_pr,3,0);
    out_quest ((char *) &akt_ipr.aki_von,2,0);
    out_quest ((char *) &akt_ipr.aki_bis,2,0);
    out_quest ((char *) &akt_ipr.ld_pr,3,0);
    out_quest ((char *) &akt_ipr.aktion_nr,1,0);
    out_quest ((char *) akt_ipr.a_akt_kz,0,2);
    out_quest ((char *) akt_ipr.modif,0,2);
    out_quest ((char *) &akt_ipr.waehrung,1,0);
    out_quest ((char *) &akt_ipr.kun,2,0);
    out_quest ((char *) &akt_ipr.a_grund,3,0);
    out_quest ((char *) akt_ipr.kond_art,0,5);
                        cursor_gr = prepare_sql ("select "
"akt_ipr.mdn,  akt_ipr.pr_gr_stuf,  akt_ipr.kun_pr,  akt_ipr.a,  "
"akt_ipr.aki_pr,  akt_ipr.aki_von,  akt_ipr.aki_bis,  akt_ipr.ld_pr,  "
"akt_ipr.aktion_nr,  akt_ipr.a_akt_kz,  akt_ipr.modif,  "
"akt_ipr.waehrung,  akt_ipr.kun,  akt_ipr.a_grund,  akt_ipr.kond_art from akt_ipr "

#line 127 "akt_ipr.rpp"
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ?");
            }
            ins_quest ((char *) &akt_ipr.mdn, 2, 0);
            ins_quest ((char *) &akt_ipr.pr_gr_stuf, 1, 0);
            ins_quest ((char *) &akt_ipr.kun_pr, 2, 0);
            del_gr_curs = prepare_sql ("delete from akt_ipr "
                                  "where mdn = ? "
                                  "and   pr_gr_stuf   = ? "
                                  "and   kun_pr   = ?");
}

int AKT_IPR_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int AKT_IPR_CLASS::dbreadfirst_a (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_a == -1)
         {
                this->prepare_a (order);
         }
         open_sql (cursor_a);
         return fetch_sql (cursor_a);
}

int AKT_IPR_CLASS::dbread_a (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         return fetch_sql (cursor_a);
}

int AKT_IPR_CLASS::dbreadfirst_gr (char *order)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor_gr == -1)
         {
                this->prepare_gr (order);
         }
         open_sql (cursor_gr);
         return fetch_sql (cursor_gr);
}

int AKT_IPR_CLASS::dbread_gr (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         return fetch_sql (cursor_gr);
}

int AKT_IPR_CLASS::dbdelete_gr (void)
{
         if (del_gr_curs == -1)
         {
                this->prepare_a (NULL);
         }
         return execute_curs (del_gr_curs);
}
       

int AKT_IPR_CLASS::dbdelete_a (void)
{
         if (del_a_curs == -1)
         {
                this->prepare_a (NULL);
         }
         return execute_curs (del_a_curs);
}
       


int AKT_IPR_CLASS::dbclose_a (void)
{
        if (cursor_a != -1)
        { 
                 close_sql (cursor_a);
                 cursor_a = -1;
        }
        return 0;
}


int AKT_IPR_CLASS::dbclose_gr (void)
{
        if (cursor_gr != -1)
        { 
                 close_sql (cursor_gr);
                 cursor_gr = -1;
        }
        return 0;
}

