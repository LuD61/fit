#ifndef _LSPT_DEF
#define _LSPT_DEF

#include "dbclass.h"

struct LSPT {
   long      nr;
   long      zei;
   char      txt[61];
};
extern struct LSPT lspt, lspt_null;

#line 7 "lspt.rh"

class LSPT_CLASS : public DB_CLASS 
{
       private :
               int del_cursor_posi;
               void prepare (void);
       public :
               LSPT_CLASS () : DB_CLASS ()
               {
                         del_cursor_posi = -1;
               }
               int dbreadfirst (void);
               int delete_lspposi (void); 
};
#endif
