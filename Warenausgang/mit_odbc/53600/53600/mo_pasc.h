#ifndef PASC_DEF
#define PASC
class Mfifo 
{
    private :
         int messanz;
         int messp;
         char **messbuff;
		 char *fifoname;
	public :
		 Mfifo (int, char *);
		 ~Mfifo ();
		 void WaitMessage (long);
		 void ScrollMsg (void);
		 void FromMsg (char *);
		 BOOL NextMsg (void);
		 BOOL ToMsg (char *);
		 void InitMsg (void);
		 BOOL WriteMsg (char *);
		 void WaitMsg (void);
		 BOOL WaitMsg (HANDLE);
		 int  TestMsgError (void);
		 BOOL WriteMsgPa (char *);
};
#endif

