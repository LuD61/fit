#ifndef _A_KUN_GX_DEF
#define _A_KUN_GX_DEF
#include "dbclass.h"

struct A_KUN_GX {
   short     mdn;
   short     fil;
   long      kun;
   double    a;
   char      a_kun[26];
   char      a_bz1[25];
   short     me_einh_kun;
   double    inh;
   char      kun_bran2[3];
   double    tara;
   double    ean;
   char      a_bz2[25];
   short     hbk_ztr;
   long      kopf_text;
   char      pr_rech_kz[2];
   char      modif[2];
   long      text_nr;
   short     devise;
   char      geb_eti[2];
   short     geb_fill;
   long      geb_anz;
   char      pal_eti[2];
   short     pal_fill;
   long      pal_anz;
   char      pos_eti[2];
   short     sg1;
   short     sg2;
   short     pos_fill;
   short     ausz_art;
   long      text_nr2;
   short     cab;
   char      a_bz3[100];
   char      a_bz4[100];
   short     eti_typ;
   long      mhd_text;
   long      freitext1;
   long      freitext2;
   long      freitext3;
   short     sg3;
   short     eti_sum1;
   short     eti_sum2;
   short     eti_sum3;
   long      ampar;
   short     sonder_eti;
   long      text_nr_a;
   double    ean1;
   short     cab1;
   double    ean2;
   short     cab2;
   double    ean3;
   short     cab3;
   long      gwpar;
   long      vppar;
   short     devise2;
   short     eti_nve1;
   double    ean_nve1;
   short     cab_nve1;
   short     eti_nve2;
   double    ean_nve2;
   short     cab_nve2;
   long      freitext4;
   long      freitext5;
   long      freitext6;
   long      freitext7;
   double    zut_gew;
   double    zut_proz;
};
extern struct A_KUN_GX a_kun_gx, a_kun_gx_null;

#line 6 "a_kun_gx.rh"

extern struct A_KUN_GX a_kun_gx, a_kun_gx_null;

class A_KUN_GX_CLASS : public DB_CLASS
{
     private :
       int cursor_bra;
       int cursor_art;
     public :  
       A_KUN_GX_CLASS () : DB_CLASS (), cursor_bra (-1), cursor_art (-1)
       {
       }
       void prepare (void);
       int dbreadfirst (void);
       int dbread_bra (void);
       int dbread_art (void);
};

#endif