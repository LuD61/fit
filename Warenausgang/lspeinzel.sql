{ TABLE "fit".lspeinzel row size = 328 number of columns = 65 index size = 75 }
create table "fit".lspeinzel 
  (
    mdn smallint,
    fil smallint,
    ls integer,
    a decimal(13,0),
    auf_me decimal(8,3),
    auf_me_bz char(6),
    lief_me decimal(8,3),
    lief_me_bz char(6),
    ls_vk_pr decimal(12,4),
    ls_lad_pr decimal(6,2),
    delstatus smallint,
    tara decimal(8,3),
    posi integer,
    lsp_txt integer,
    pos_stat smallint,
    sa_kz_sint smallint,
    erf_kz char(1),
    prov_satz decimal(4,2),
    leer_pos smallint,
    hbk_date date,
    ls_charge char(30),
    auf_me_vgl decimal(12,3),
    ls_vk_euro decimal(12,4),
    ls_vk_fremd decimal(12,2),
    ls_lad_euro decimal(12,2),
    ls_lad_fremd decimal(12,2),
    rab_satz decimal(5,2),
    me_einh_kun smallint,
    me_einh smallint,
    me_einh_kun1 smallint,
    auf_me1 decimal(8,3),
    inh1 decimal(8,3),
    me_einh_kun2 smallint,
    auf_me2 decimal(8,3),
    inh2 decimal(8,3),
    me_einh_kun3 smallint,
    auf_me3 decimal(8,3),
    inh3 decimal(8,3),
    lief_me_bz_ist char(10),
    inh_ist decimal(8,3),
    me_einh_ist smallint,
    me_ist decimal(8,3),
    lager smallint,
    lief_me1 decimal(8,3),
    lief_me2 decimal(8,3),
    lief_me3 decimal(8,3),
    ls_pos_kz smallint,
    a_grund decimal(13,0),
    kond_art char(4),
    posi_ext integer,
    kun integer,
    na_lief_kz smallint,
    nve_posi integer,
    teil_smt smallint,
    aufschlag smallint,
    aufschlag_wert decimal(12,4),
    ls_ident char(20),
    pos_txt_kz smallint,
    anz_einh decimal(8,3),
    a_lsp decimal(13,0),
    prdk_stufe smallint,
    prdk_typ smallint,
    auf integer,
    extra smallint,
    sort smallint
  );
revoke all on "fit".lspeinzel from "public";

create index "Administ".i02lspeinzel on "fit".lspeinzel (ls,mdn);
    
create index "Administ".i03lspeinzel on "fit".lspeinzel (mdn,fil,
    lsp_txt);
create unique index "Detlef L".i01lspeinzel on "fit".lspeinzel 
    (mdn,fil,ls,auf,a,posi);




