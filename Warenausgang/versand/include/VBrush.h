#ifndef _VBRUSH_DEF
#define _VBRUSH_DEF
#include "vector.h"
#include "RBrush.h"

class VBrush : public CVector
{
private :
//	HBRUSH hBrush;
public :
	VBrush ();
	~VBrush ();
//	void SetStdFont (HFONT);
	BOOL Exist (RBrush*);
	BOOL CanDestroy (HBRUSH);
};
#endif