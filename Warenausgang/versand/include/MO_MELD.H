#ifndef _MO_MELD_DEF
#define _MO_MELD_DEF
#include <commctrl.h>
#include "comcthlp.h"

#define IDM_WORK           100
#define IDM_SHOW           101
#define IDM_DEL            102
#define IDM_PRINT          103
#define IDM_INFO           104
#define IDM_TEXT           105
#define IDM_ETI            106
#define IDM_DELETE         107
#define IDM_VINFO          108
#define IDM_PRCHOISE       109
#define IDM_FIRST          110
#define IDM_LAST           111
#define IDM_PRIOR          1112
#define IDM_NEXT           1113
#define IDM_CHPRINT        1114
#define IDM_COPY           1115
#define IDM_INS            1116
#define IDM_COPY_ETI       1117
#define IDM_WRITE          1118

#define IDM_EXIT           199

#define FTOOLBAR 400

static char liste[]     = {"Liste....."};
static char einfuegen[] = {"einf�gen.."};
static char anfuegen[]  = {"anf�gen..."};
static char aufteilen[] = {"aufteilen."};
static char loeschen[]  = {"l�schen..."};
static char ansehen[]   = {"ansehen..."};
static char gueltig[]   =  {"g�ltig...."};
static char aktivieren[]= {"aktivieren"};
static char deaktiv[]   = {"deaktiv..."};
static char auswahl[]   = {"Auswahl..."};
static char suchen[]    = {"suchen...."};
static char query[]     = {"Query....."};
static char bildschirm[]= {"Bildschirm"};
static char drucker []  = {"Drucker..."};
static char datei []    = {"Datei....."};
static char druckerwahl[]= {"Druckwahl."};
static char texte[]      = {"Texte....."};
static char artikel[]    = {"Artikel..."};
static char kopieren[]   = {"Kopieren.."};
static char zutaten[]    = {"Zutaten..."};
static char vollbild[]   = {"Vollbild.."};
static char fenster[]    = {"Fenster..."};
static char komplett[]   = {"komplett.."};
static char kundefil[]   = {"Kun.-/Fil."};
static char kunart[]     = {"Kun.-Art.."};
static char eigart[]     = {"Eig.-Art.."};
static char liefart[]    = {"Lief.-Art."};
static char einzei[]     = {"1 Listz..."};
static char zweizei[]    = {"2 Listz..."};
static char a_bz2_off[]  = {"Bez. 1...."};
static char a_bz2_on[]   = {"Bez..2...."};
static char einhausw[]   = {"Auf.Einh.."};
static char bestellung[] = {"Letzte Bst"};
static char kopftext[]   = {"Kopf-Text."};
static char fusstext[]   = {"Fuss-Text."};
static char standardauf[] = {"Standard.."};
static char adresse[]     = {"Anschrift."};
static char basisme[]     = {"Basis-Me.."};
static char posrab[]      = {"Pos.Rabatt"};
static char prodmdn[]     = {"Prod.Mdn.."};
static char angebote[]    = {"Angebote.."};
static char lieferanten[] = {"Lieferant."};
static char grundpreis[]  = {"Grundpreis"};
static char kontrakt[]    = {"Kontrakt.."};
static char fktpartner[]  = {"Partner..."};
static char lkond[]       = {"Lief.Kond."};
static char kunlieftxt[]  = {"Lieferadr."};
static char mauftrag[]    = {"Auftrag."};
static char aufschlag[]   = {"Aufschlag."};
static char leer[]        = {".........."};

struct PMENUE
{
	     char *text;
		 char *typ;
		 void *nmenue;
		 int menid;
};


class CBsdInfo
{
public:
	int left;
	int top;
	int right;
	int bottom;
	HWND Parent;
	HWND LBsdInfo;
	HWND BsdInfo;
	HBRUSH InfoBrush;

	CBsdInfo ()
	{
		left = top = right = bottom = 0;
		Parent   = NULL;
		LBsdInfo = NULL;
		BsdInfo  = NULL;
		InfoBrush = NULL;
	}

	void Set (int left, int top, int right, int bottom);
	void Create ();
	void Destroy ();
};

extern CBsdInfo BsdInfo;

void MoveFkt (void);
HWND OpenFkt (HANDLE);
HWND OpenFktM (HANDLE);
void EnableFkt (BOOL);
void EnableFkt1 (BOOL);
void EnableFkt2 (BOOL);
void UpdateFkt (void);
void SetFkt (int, char *, int);
int SendKey (int);
int testjn (void);
int abfragejn (HWND, char *, char *);
HWND OpenMamain1 (int, int, int, int, HINSTANCE, WNDPROC, form *, mfont *);
HWND OpenMamain1Ex (int, int, int, int, HINSTANCE, WNDPROC, form *, mfont *);
void MoveHeader (void);
void DisplayKopf1 (HWND, char *);
void DisplayKopf3 (HWND, char *, int, int);
void InsertMenueItem (struct PMENUE *, struct PMENUE *, int);
HMENU MakeMenue (struct PMENUE *);
HWND MakeToolBar (HWND, TBBUTTON *, char **, 
				  UINT *, char **, HWND **);
HWND MakeToolBarEx (HANDLE, HWND, TBBUTTON *, int, 
                    char **, 
				    UINT *, char **, HWND **);
HWND MakeToolBarCombo (HANDLE , HWND,HWND, int, int, int);
BOOL QuickHwndCpy (LPTOOLTIPTEXT);
BOOL QuickCpy (LPSTR, UINT);
BOOL ComboToolTip (HWND, HWND);

HWND OpenFktEx (HANDLE);
void CreateFktBitmap (HINSTANCE, HWND, HWND);
void DisplayFktBitmap (void);
void MoveFktBitmap (void);
void SetFktQuikInfo (HWND, TOOLINFO *);
void SetFktMenue (int (*) (HWND, UINT, WPARAM, LPARAM));
void AddFktButton (void *);

#endif
