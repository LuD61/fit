#ifndef _SEARCHPTAB_DEF
#define _SEARCHPTAB_DEF
#include "wmaskc.h"
#include "cmask.h"
#include "mo_chqex.h"
#include "dbclass.h"

class SEARCHPTAB
{
       private :
           static HINSTANCE hMainInst;
           static HWND hMainWindow;
           static DB_CLASS DbClass; 
           static HWND awin;
           static int idx;
           static long anz;
           static char *Item;

           int SearchPos;
           int OKPos;
           int CAPos;
           char Key [512];
       protected : 
           static BOOL NumVal;
           static CHQEX *Query;

        public :

           void SetNumWert (BOOL NumWert)
           {
               this->NumVal = NumWert;
           }

           BOOL GetNumVal (void)
           {
               return NumVal;
           }

           SEARCHPTAB (char *);

           ~SEARCHPTAB ()
           {
                   if (Query)
                   {
                       delete Query;
                       Query = NULL;
                   }
           }

/*
           SWG *GetLine (void)
           {
               if (idx == -1) return NULL;
               return &sline;
           }
*/

           void Setawin (HWND awin)
           {
               this->awin = awin;
           }

           void SetItem (char *);
           static int SearchLst (char *);
           static int ReadLst (char *);
           BOOL GetKey (char *);
           void SetParams (HINSTANCE, HWND);
           void Search (void);
};  
#endif