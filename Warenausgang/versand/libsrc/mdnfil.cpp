#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "wmaskc.h"
#include "stdfkt.h"
#include "mdnfil.h"

MDNFIL cmdnfil;

static int testmdn (void);
static int testfil (void);
static int showmdn (void);
static int showfil (void);
static int setkey9mdn (void);
static int setkey9fil (void);

static char vmdn [4];
static char vfil [6];
static char mdn_bz [17] = {"Zentrale"};
static char fil_bz [17];

static ITEM imdn    ("mdn", vmdn, "Mandant........:", 0);
static ITEM ifil    ("fil", vfil, "Filiale........:", 0);
static ITEM imdn_bz ("", mdn_bz, "", 0);
static ITEM ifil_bz ("", fil_bz, "", 0);

static field _mdnfil [] = {
&imdn,                         3, 0, 0, 1, 0, "%2d", NORMAL, setkey9mdn, testmdn,0,
&imdn_bz,                     16, 0, 0,23, 0, "", DISPLAYONLY, 0, 0, 0,
&ifil,                         5, 0, 0,40, 0, "%4d", NORMAL, setkey9fil, testfil, 0,
&ifil_bz,                     16, 0, 0,65, 0, "", DISPLAYONLY, 0, 0, 0
};

static form mdnfil = {4, 0,0, _mdnfil, 0, 0, 0, 0, NULL};

static int field_mdn = 0;
static int field_fil = 2;

static int showmdn ()
{
	   return cmdnfil.showmdn ();
}

static int showfil ()
{
	   return cmdnfil.showfil ();
}

static int setkey9mdn ()
/**
Taste KEY9 auf aktiv setzen.
**/
{
 	   set_fkt (showmdn, 9);
       SetFkt (9, auswahl, KEY9);
       return 0;
}


static int setkey9fil ()
/**
Taste KEY9 auf aktiv setzen.
**/
{
       set_fkt (showfil, 9);
       SetFkt (9, auswahl, KEY9);
       return 0;
}


static int testmdn (void)
/**
Mandnaten-Nr testen.
**/
{
         set_fkt (NULL, 9);
         SetFkt (9, leer, NULL);

         if (syskey == KEY9)
         {
                      cmdnfil.showmdn ();
                      SetCurrentField (currentfield);
                      return TRUE;
         }

         if (testkeys ()) return 0;

         if ((cmdnfil.GetNoMdnNull ()) && atoi (vmdn) == 0)
         {
             SetCurrentField (currentfield);
             disp_mess ("Mandant 0 ist nicht erlaubt", 2);
             return FALSE;
         }

         return cmdnfil.GetMdn ();
}

static int testfil (void)
/**
Mandnaten-Nr testen.
**/
{
         set_fkt (NULL, 9);
         SetFkt (9, leer, NULL);

         if (syskey == KEY9)
         {
                      cmdnfil.showfil ();
                      SetCurrentField (currentfield);
                      return TRUE;
         }

         if (testkeys ()) return 0;
        
         if (cmdnfil.GetNoFilNull () && atoi (vfil) == 0)
         {
             disp_mess ("Filiale 0 ist nicht erlaubt", 2);
             SetCurrentField (currentfield);
             return FALSE;
         }

         return cmdnfil.GetFil ();

}


void MDNFIL::SetMandantAttr (int attribut)
/**
Attribut fuer Mandant setzen.
**/
{
         mdnfil.mask [field_mdn].attribut = attribut;
}


void MDNFIL::SetFilialAttr (int attribut)
/**
Attribut fuer Filiale setzen.
**/
{
         mdnfil.mask [field_fil].attribut = attribut;
}


int MDNFIL::showmdn ()
/**
Auswahlfenster ueber Mandanten.
**/
{
	     MDN_CLASS::ShowAllBu (AktivWindow, 0);
         lese_mdn (_mdn.mdn);
         sprintf (vmdn, "%hd", _mdn.mdn);
         strcpy (mdn_bz, _adr.adr_krz);
         display_form (AktivWindow, current_form , 0, 0);
		 SetCurrentFocus (currentfield);
         syskey = 1;
         return 0;
}

int MDNFIL::showfil ()
/**
Auswahlfenster ueber Mandanten.
**/
{
	     FIL_CLASS::ShowAllBu (AktivWindow, 0, atoi (vmdn));
         if (syskey != KEY5)
         {
                 lese_fil (_mdn.mdn, _fil.fil);
                 sprintf (vfil, "%hd", _fil.fil);
                 strcpy (fil_bz, _adr.adr_krz);
                 display_form (AktivWindow, current_form , 0, 0);
         }
		 SetCurrentFocus (currentfield);
         syskey = 1;
         return 0;
}

int MDNFIL::GetMdn ()
/**
Mandant aus Datenbank holen.
**/
{
         if (atoi (vmdn) > 0)
         {
                      if (lese_mdn (atoi (vmdn)) == 100)
                      {
                                disp_mess ("Mandant ist nicht angelegt", 2);
                                SetCurrentField (currentfield);
                                return FALSE;
                      }
                      strcpy (mdn_bz, _adr.adr_krz);

         }
         else
         {
                      strcpy (mdn_bz, "Zentrale");
         }
         display_form (AktivWindow, current_form , 0, 0);
         return 0;
}

int MDNFIL::GetFil ()
/**
Filiale aus Datenbank holen.
**/
{
         if (atoi (vfil) > 0)
         {
                      if (lese_fil (atoi (vmdn), atoi (vfil)) == 100)
                      {
                                disp_mess ("Filiale ist nicht angelegt", 2);
                                SetCurrentField (currentfield);
                                return FALSE;
                      }
                      strcpy (fil_bz, _adr.adr_krz);

         }
         else
         {
                      strcpy (fil_bz, "Mandant");
         }
         display_form (AktivWindow, current_form , 0, 0);
         return 0;
}

void MDNFIL::CloseForm (void)
/**
Maske fuer Mandant/Filiale schliessen.
**/
{
      CloseControls (&mdnfil);
}


int MDNFIL::eingabemdnfil (HWND hWnd, char *cmdn, char *cfil)
/**
Mandant und Filiale eingeben.
**/
{

	  SetAktivWindow (hWnd);
      if (sys_ben.mdn)
      {
		       current_form = &mdnfil; 
               sprintf (vmdn, "%hd", sys_ben.mdn);
               SetMandantAttr (READONLY);
               GetMdn ();
      }

      if (sys_ben.fil)
      {
		       current_form = &mdnfil; 
               sprintf (vfil, "%hd", sys_ben.fil);
               SetFilialAttr (READONLY);
               GetFil ();
      }

      break_end ();
      enter_form (hWnd, &mdnfil, 0, 0);
      display_form (hWnd, &mdnfil, 0, 0);
      if (syskey == KEY5 || syskey == KEYESC) return (-1);;
      if (sys_ben.mdn && sys_ben.fil) return (-1);
	  strcpy (cmdn,vmdn);
	  strcpy (cfil,vfil);
	  return 0;
}

