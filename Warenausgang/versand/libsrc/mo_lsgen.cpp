#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include <commctrl.h>
#include <time.h>
#include "comcthlp.h"
#include "wmaskc.h"
#include "itemc.h"
#include "mo_meld.h"
#include "70001.h"
#include "strfkt.h"
#include "stdfkt.h"
#include "dbfunc.h"
#include "formfeld.h"
#include "mo_menu.h"
#include "mo_gdruck.h"

static BOOL GraphikMode = FALSE;
static char vOutput [41];
static char vDrkChoise [41];
static HWND ahWnd = NULL;

#define MAXTAB 100

extern HANDLE  hMainInst;
static HWND hMainWindow = NULL;
static HWND  mamain1 = NULL;
static HFONT StdFont = NULL;

static int dsqlstatus;
static int formular = 1;

/* AAAA  */
static char iformname[30] ;
static char outmodus[30] ;
static int anzlpsz ;
static int ubzeilen ;
static int MenStart ;
static int MenScroll ;

static char akt_drucker [40] [64];
static int drkanz = 0;
static int ausgabe = SCREEN_OUT;

static char  feld_tab_txt [15][25] ;
static char feld_feld_txt [15][25] ;

static char feld1_von_txt [40];
static char feld2_von_txt [40];
static char feld3_von_txt [40];
static char feld4_von_txt [40];
static char feld5_von_txt [40];
static char feld6_von_txt [40];
static char feld7_von_txt [40];
static char feld8_von_txt [40];
static char feld9_von_txt [40];
static char feld10_von_txt [40];
static char feld11_von_txt [40];
static char feld12_von_txt [40];
static char feld13_von_txt [40];
static char feld14_von_txt [40];
static char feld15_von_txt [40];

static char feld1_von [40];
static char feld1_bis [40];
static char feld2_von [40];
static char feld2_bis [40];
static char feld3_von [40];
static char feld3_bis [40];
static char feld4_von [40];
static char feld4_bis [40];
static char feld5_von [40];
static char feld5_bis [40];
static char feld6_von [40];
static char feld6_bis [40];
static char feld7_von [40];
static char feld7_bis [40];
static char feld8_von [40];
static char feld8_bis [40];
static char feld9_von [40];
static char feld9_bis [40];
static char feld10_von [40];
static char feld10_bis [40];
static char feld11_von [40];
static char feld11_bis [40];
static char feld12_von [40];
static char feld12_bis [40];
static char feld13_von [40];
static char feld13_bis [40];
static char feld14_von [40];
static char feld14_bis [40];
static char feld15_von [40];
static char feld15_bis [40];

char *feld_von[] = {feld1_von,
                    feld2_von,
                    feld3_von,
                    feld4_von,
                    feld5_von,
                    feld6_von,
                    feld7_von,
                    feld8_von,
                    feld9_von,
                    feld10_von,
                    feld11_von,
                    feld12_von,
                    feld13_von,
                    feld14_von,
                    feld15_von
                   };

char *feld_bis[] = {feld1_bis,
                    feld2_bis,
                    feld3_bis,
                    feld4_bis,
                    feld5_bis,
                    feld6_bis,
                    feld7_bis,
                    feld8_bis,
                    feld9_bis,
                    feld10_bis,
                    feld11_bis,
                    feld12_bis,
                    feld13_bis,
                    feld14_bis,
                    feld15_bis
                   };

static int feld_nr = 0;
static char akt_form[20];

static int textdatei = 0;
static int windatei = 0;
static int exeldatei = 0;

static int SetWin (void);
static int SetText (void);
static int SetExel (void);
static int BreakBox (void);

static ITEM iWinword   ("", " Winword ",    "", 0);
static ITEM iExcel     ("", " Excel ",      "", 0);
static ITEM iTextdatei ("", " Textdatei ",  "", 0);
static ITEM iOK        ("", " OK ",         "", 0);

static field _cbox [] = {
&iWinword,       12, 2, 0, 1, 0, "", BUTTON | CHECKBUTTON, 0, SetWin, 101,
&iExcel,          9, 2, 2, 1, 0, "", BUTTON | CHECKBUTTON, 0, SetExel, 102,
&iTextdatei,     14, 2, 4, 1, 0, "", BUTTON | CHECKBUTTON, 0, SetText, 103,
&iOK,             4, 2, 7, 6, 0, "", BUTTON, 0, BreakBox, 104};

static form cbox = {4, 0, 0, _cbox, 0,0,0,0,0}; 

FORMFELD_CLASS formfeld_class;
// ITEM_CLASS item_class;
FORMFRM_CLASS formfrm_class;
FORMHEAD_CLASS formhead_class;

static int error;

int SetWin (void)
/**
Ausgabe Winword setzen.
**/
{
          if (syskey == KEY5)
          {
                       break_enter ();
                       return (1);
          }

          if (SendMessage (cbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       windatei = 0;
          }
          else
          {
                       windatei = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       !windatei, 0l);
                       windatei = !windatei;
          }
          return 1;
}

int SetExel (void)
/**
Ausgabe Exel setzen.
**/
{
          if (syskey == KEY5)
          {
                       break_enter ();
                       return (1);
          }
          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       exeldatei = 0;
          }
          else
          {
                       exeldatei = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       !exeldatei, 0l);
                       exeldatei = !exeldatei;
          }
          return 1;
}

int SetText (void)

/**
Ausgabe Winword setzen.
**/
{
          if (syskey == KEY5)
          {
                       break_enter ();
                       return (1);
          }
          if (SendMessage (cbox.mask[2].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       textdatei = 0;
          }
          else
          {
                       textdatei = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[2].feldid, BM_SETCHECK,
                       !textdatei, 0l);
                       textdatei = !textdatei;
          }
          return 1;
}

int BreakBox (void)
/**
EnterButton abbrechen.
**/
{
          if (syskey == KEY5)
          {
                       break_enter ();
                       return (1);
          }
          if (syskey == KEYUP || syskey == KEYDOWN) return 0;
          syskey = KEYCR;
          break_enter ();
          return 1;
}

void DruckeFile (void)
/**
Hier wird der Listgenerator in Rosi oder C aufgrufen.
**/
{

  FILE *lst ;
  char bfnam[22] ;
  char datenam[22] ;
  char buffer [512] ;
  char zeilbuffer[80] ;
  char komma[128] ;
  int funki ;
  int schleifi ;
  char zwi_von[25] ;
  char zwi_bis[25] ;
  char  xxx ;    /* Vorerst nur realisiert : temp_table selber createn */
  char  yyy ;    /* Vorerst nur realisiert : Druck im Higru */
  char  zzz ;    /* formular oder nicht */
  xxx = 'J' ;
  yyy = 'N' ;
  zzz = 'N' ;    /* vorerst nur realisert : Liste ohne Formular */
  funki = formfrm_class.lese_formfrm (iformname);
  if (formular && funki == 0) zzz = 'J' ;
  
  time_t t = GetCurrentTime() ;
  sprintf (bfnam,"%dL", t ) ;
  sprintf ( datenam , "%s.lgf", iformname ) ;

  if (getenv ("TMPPATH"))
  {
    sprintf (buffer, "%s\\%s", getenv("TMPPATH"), datenam);
  }
  else
  {
    strcpy (buffer, datenam );
  }
  remove (buffer) ;
  if ( (lst = fopen (buffer, "at" )) == NULL ) 
  {
                     print_mess (2, "%s kann nicht ge�ffnet werden", buffer);
                     return ;
  }

  for ( funki = 0 ; funki < 14 ; funki ++ )
  {
    switch (funki)
    {
    case 0:             /* Formatname */
        memset ( zeilbuffer, '\0', 128 ) ;
        sprintf ( zeilbuffer, ".0$%s" , akt_form ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;

    case 1:             /* Bereiche */
        for ( schleifi = 0; schleifi < feld_nr ; schleifi++ )
        { 
          switch (schleifi)
          {
            case 0:
                sprintf ( zwi_von , "%s" , feld1_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld1_bis ) ;
                break ;
            case 1:
                sprintf ( zwi_von , "%s" , feld2_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld2_bis ) ;
                break ;                                       
            case 2:
                sprintf ( zwi_von , "%s" , feld3_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld3_bis ) ;
                break ;
            case 3:
                sprintf ( zwi_von , "%s" , feld4_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld4_bis ) ;
                break ;
            case 4:
                sprintf ( zwi_von , "%s" , feld5_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld5_bis ) ;
                break ;
            case 5:
                sprintf ( zwi_von , "%s" , feld6_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld6_bis ) ;
                break ;
            case 6:
                sprintf ( zwi_von , "%s" , feld7_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld7_bis ) ;
                break ;
            case 7:
                sprintf ( zwi_von , "%s" , feld8_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld8_bis ) ;
                break ;
            case 8:
                sprintf ( zwi_von , "%s" , feld9_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld9_bis ) ;
                break ;
            case 9:
                sprintf ( zwi_von , "%s" , feld10_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld10_bis ) ;
                break ;
            case 10:
                sprintf ( zwi_von , "%s" , feld11_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld11_bis ) ;
                break ;
            case 11:
                sprintf ( zwi_von , "%s" , feld12_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld12_bis ) ;
                break ;
            case 12:
                sprintf ( zwi_von , "%s" , feld13_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld13_bis ) ;
                break ;
            case 13:
                sprintf ( zwi_von , "%s" , feld14_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld14_bis ) ;
                break ;
            case 14:
                sprintf ( zwi_von , "%s" , feld15_von ) ;
                sprintf ( zwi_bis  ,"%s" , feld15_bis ) ;
                break ;
            default:
                memset ( zwi_von , '\0', 25 ) ;
                memset ( zwi_bis , '\0', 25 ) ;
                break ;
          }     /* END switch */
          memset ( zeilbuffer, '\0', 80 ) ;
          sprintf ( zeilbuffer, ".1$%s$%s$%s$%s",feld_tab_txt[schleifi], feld_feld_txt[schleifi], zwi_von, zwi_bis) ;
          fprintf ( lst , "%s\n" , zeilbuffer ) ;
        }      /* END schleifi-while */
        break ;
    case 2:             /* z.Z. leer */
    case 3:
    case 4:
        break ;

    case 5:             /* Persname */
        memset ( zeilbuffer, '\0', 128 ) ;
        /* -
        sprintf ( zeilbuffer, ".5$%s\n" , persname  ) ;
        -- */
        if (ausgabe == FAX_OUT)
        {
                 sprintf ( zeilbuffer ,".5$fax" ) ; 
        }
        else
        {
//                 sprintf ( zeilbuffer ,".5$service" ) ; 
                 sprintf ( zeilbuffer ,".5$%s", sys_ben.pers_nam ) ; 
        }
        fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;

    case 6:             /* Temptablename oder leer */
        memset ( zeilbuffer, '\0', 128 ) ;
        if ( xxx != 'J' ) sprintf ( zeilbuffer, ".6$lg%s" , bfnam ) ;
        else  sprintf (zeilbuffer,".6$" ) ;
        fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;

    case 7:             /* Druckziel */
          memset ( zeilbuffer, '\0', 128 ) ;
          switch (ausgabe)
          {
            case SCREEN_OUT:
             sprintf ( zeilbuffer, ".7$1" ) ;
             break ;
            case FILE_DSAVE:
            case FILE_OUT:
              if (exeldatei)
              {
                         sprintf ( zeilbuffer, ".7$4" ) ;
              }
              else if (windatei)
              {
                         sprintf ( zeilbuffer, ".7$5" ) ;
              }
              else
              {
                         sprintf ( zeilbuffer, ".7$2" ) ;
              }
              break ;

            case FAX_OUT:
              sprintf ( zeilbuffer, ".7$3" ) ;
              break ;
            case PRINTER_OUT:
              sprintf ( zeilbuffer, ".7$3" ) ;
              break ;
          }
          // Immer in Datei .... -> muss Kurt noch machen
          // sprintf ( zeilbuffer, ".7$2" ) ;
          fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;
        
    case 8:             /* TempTable erstellen */
          memset ( zeilbuffer, '\0', 128 ) ;
          if ( xxx == 'J' ) 
            sprintf ( zeilbuffer, ".8$J" ) ;
          else
            sprintf ( zeilbuffer, ".8$N" ) ;
          fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;
    case 9:             /* TempTable loeschen */
          memset ( zeilbuffer, '\0', 128 ) ;
          if ( xxx == 'J' ) 
            sprintf ( zeilbuffer, ".9$J" ) ;
          else
            sprintf ( zeilbuffer, ".9$N" ) ;
          fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;
    case 10:             /* Higru-Druck */
          memset ( zeilbuffer, '\0', 128 ) ;
          if ( yyy == 'J' ) 
            sprintf ( zeilbuffer, ".A$J" ) ;
          else
            sprintf ( zeilbuffer, ".A$N" ) ;
          fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;
    case 11:             /* Formular  */
          memset ( zeilbuffer, '\0', 128 ) ;
          if ( zzz == 'J' ) 
            sprintf ( zeilbuffer, ".B$J" ) ;
          else
            sprintf ( zeilbuffer, ".B$N" ) ;
          fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;
    case 12 :            /* Silent Print  */
          memset ( zeilbuffer, '\0', 128 ) ;
          sprintf ( zeilbuffer, ".C$%s", form_head.form_distinct) ;
          fprintf ( lst , "%s\n" , zeilbuffer ) ;
          break;
    case 13:             /* Dateiende x */
          memset ( zeilbuffer, '\0', 128 ) ;
          sprintf ( zeilbuffer, ".E" ) ;
          fprintf ( lst , "%s\n" , zeilbuffer ) ;
        break ;
        }       /* ende switch */
    }          /* ende for funki */
    fclose ( lst ) ;

    if (getenv ("TMPPATH"))
    {
      sprintf ( komma, "%s\\%s.sta", getenv ("TMPPATH"), iformname);
    }
    else
    {
      strcpy (komma, "liste.lst");
    }
    remove (komma) ;


    if (ausgabe == FAX_OUT) 
    {
        SaveDruckerAkt ("gdi.cfg");
    }

    // sprintf ( komma , "rswrun -vwabk 7000f %s" , iformname ) ;
    sprintf ( komma , "rswrun 7000f %s" , clipped (iformname) ) ;

    HCURSOR oldcursor = SetCursor ( LoadCursor ( NULL , IDC_WAIT)) ;
//    ProcWaitExec ( komma, SW_MINIMIZE, -1, 0, -1, 0);

    ProcWaitExecEx ( komma, SW_SHOWNORMAL, 0, 40, -1, 0);
    if (ausgabe == FAX_OUT) 
    {
        RestoreDruckerAkt ();
    }
}

void WaitListEnd (void)
/**
Auf Ende der Listenerstellung warten.
**/
{
          FILE *ls;
          char buffer [512];


          if (getenv ("TMPPATH"))
          {
                        sprintf (buffer, "%s\\%s.sta",
                                 getenv ("TMPPATH"), iformname);
          }
          else
          {
                        strcpy (buffer, "liste.lst");
          }

          HCURSOR oldcursor =SetCursor ( LoadCursor ( NULL , IDC_WAIT)) ;

          ls = fopen (buffer, "r");
          if (ls == (FILE *) 0)
          {
                   return;
          }

     /*
          while (ls == (FILE *) 0)
          {
            Sleep ((DWORD) 1500) ;  
            ls = fopen (buffer, "r") ;
          }
     */

          SetCursor ( oldcursor ) ;

          error = ubzeilen = MenStart = 0;
          if (fgets (buffer, 255, ls))
          {
                        cr_weg (buffer);
                        error =  atoi (buffer);
          }
          if (fgets (buffer, 255, ls))
          {
                        cr_weg (buffer);
                        ubzeilen =  atoi (buffer);
          }
          if (fgets (buffer, 255, ls))
          {
                        cr_weg (buffer);
                        MenStart =  atoi (buffer);
          }
          MenScroll = 0;
          if (fgets (buffer, 255, ls))
          {
                        cr_weg (buffer);
                        MenScroll =  atoi (buffer);
          }
          fclose (ls);
          return;
}

int IsError ()
/**
Fehler bei Druck testen.
**/
{
           static char *errtab []= {"",
                                    "Liste zu breit",
                                    "Liste abgebrochen",
                                    "Liste abgebrochen",
                                    "Fehler beim Einlesen der temp table",
                                    "Fehler beim Einlesen der Adress-Tabelle",
                                    "Fehler beim Aktivieren des Druckers",
                                    "Fehler beim Lesen der Steuerzeichen"};

           static char *NoData =    "Keine Daten vorhanden";
           static char *SQL    =    "Fehler beim Ausf�hren des SQL-Befehls";             

          if (error == 0)
          {
                    return (0);
          }
          else if (error < 7 && error > 0)
          {
                    disp_mess (errtab [error], 2);
                    return (1);
          }
          else if (error == 100)
          {
                    disp_mess (NoData, 2);
                    return (1);
          }
          else
          {
                    disp_mess (SQL, 2);
                    return (1);
          }
          return (1);
}


void print_file (void)
/**
Druck am Bildschirm.
**/
{
         int x,y;
         int cx, cy;
         int mx, my;
         RECT rect;
         TEXTMETRIC tm;
         HFONT hFont;
         HDC hdc;
         HWND hWnd;

         if (mamain1 == NULL)
         {
             mamain1 = GetActiveWindow ();
             hWnd = GetParent (mamain1);
             if (hWnd) mamain1 = hWnd;
         }
         if (hWnd == NULL)
         {
             hMainWindow = mamain1;
         }

         while (hWnd)
         {
             hMainWindow = hWnd;
             hWnd = GetParent (hMainWindow);
         }

         stdfont ();
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (mamain1);
         SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         ReleaseDC (mamain1, hdc);
         DeleteObject (hFont);

         GetClientRect (hMainWindow, &rect);
         mx = rect.right / tm.tmAveCharWidth;
         my = rect.bottom / tm.tmHeight;
         GetFrmSize (&cbox, &cx, &cy);

         my -= my / 3;
         cy -= 2;

         x = max (0, (mx - cx) / 2);
         y = max (0, (my - cy) / 2) + 2;

         windatei = exeldatei = textdatei = 0;
         if (EnterButtonWindow (mamain1, &cbox, 11, x, cy, cx) == -1)
         {
                        return;
         }
         DruckeFile ();
         WaitListEnd ();
		 if (ahWnd) 
         {
             SetWindowPos (ahWnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
/*
             SetForegroundWindow (ahWnd);
             SetActiveWindow (ahWnd);
*/
         }
         IsError ();
}

void ShowFile (void)
/**
Liste am Bildschirm anzeigen.
**/
{
          FILE *ls;
          char buffer [512];
          int i;

          WaitListEnd ();
 		  if (ahWnd) SetForegroundWindow (ahWnd);

          if (IsError ()) return; 

          if (getenv ("TMPPATH"))
          {
                        sprintf (buffer, "%s\\%s.lst",
                                 getenv ("TMPPATH"), iformname);
          }
          else
          {
                        strcpy (buffer, "liste.lst");
          }

          ls = fopen (buffer, "r");
          if (ls == (FILE *) 0) return;

          SetMenStart (MenStart);
          OpenSListWindow (20, 75, 3, 1, TRUE);

          i = 0;
          while (fgets (buffer, 511, ls))
          {
                        cr_weg (buffer);
                        if (i < ubzeilen)
                        {
                               if (strlen (buffer) == 0)
                               {
                                         strcpy (buffer, "  ");
                               }
                               InsertListUb (buffer);
                               i ++;
                        }
                        else
                        {
                               InsertListRow (buffer);
                        }
          }
          SetSscroll (MenScroll, MenStart);
          EnterQueryListBox ();
          StdFont = CreateStdFont (hMainWindow);
          return;
}

void printerfile (void)
/**
Liste auf Drucker schicken
**/
{
          char buffer [512];

          WaitListEnd ();
 		  if (ahWnd) SetForegroundWindow (ahWnd);
          if (IsError ()) return; 

          if (getenv ("TMPPATH"))
          {
             sprintf (buffer, "fitprint %s\\%s.lst %s",
                      getenv ("TMPPATH"), iformname, sys_ben.pers_nam);
          }
          else
          {
             sprintf (buffer, "fitprint liste.lst %s", sys_ben.pers_nam);
          }

          ProcWaitExecEx (buffer, SW_SHOW, -1, 0, -1, 0);  
          if (ahWnd) 
          {
             SetWindowPos (ahWnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
/*
             SetForegroundWindow (ahWnd);
*/
             SetActiveWindow (ahWnd);

          }
}


void faxfile (void)
/**
Liste auf Fax schicken
**/
{
          char buffer [512];

          WaitListEnd ();
 		  if (ahWnd) SetForegroundWindow (ahWnd);
          if (IsError ()) return; 

          if (getenv ("TMPPATH"))
          {
             sprintf (buffer, "gprint -rfax %s\\%s.lst",
                      getenv ("TMPPATH"), iformname);
          }
          else
          {
             sprintf (buffer, "gprint -rfax liste.lst");
          }

          ProcWaitExecEx (buffer, SW_SHOW, -1, 0, -1, 0);  
          if (ahWnd) 
          {
             SetWindowPos (ahWnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
/*
             SetForegroundWindow (ahWnd);
*/
             SetActiveWindow (ahWnd);

          }
}

BOOL LeseFormat (char *form_nr)
/**
Format einlesen.
**/
{

      strcpy (form_head.form_nr, form_nr);
      formhead_class.dbreadfirst ();
      dsqlstatus = formfeld_class.lese_formfeld (form_nr);
      if (dsqlstatus == 100)
      {
               print_mess (2, "Format %s nicht gefunden", form_nr); 
               return FALSE; 
      }
      strcpy (akt_form, form_nr);
      clipped (akt_form);
      strcpy (iformname, akt_form);
      feld_nr = 0;
      return TRUE;
}

BOOL FillFormFeld (char *feldname, char *valfrom, char *valto)
/**
Formfeld fuellen.
**/
{
        int anz;
        int dsqlstatus;

        char tab_nam [21];
        char feld_nam [21];

        anz = wsplit (feldname, ".");
        if (anz < 2) return FALSE;
 
        strcpy (tab_nam, wort[0]); 
        strcpy (feld_nam, wort[1]); 
        dsqlstatus = formfeld_class.lese_feld (akt_form, tab_nam, feld_nam);
        if (dsqlstatus == 100)
        {
                       return FALSE;
        }
        
        sprintf ( feld_tab_txt[feld_nr],  "%s", _form_feld.tab_nam )  ;
        sprintf ( feld_feld_txt[feld_nr], "%s", _form_feld.feld_nam) ;
        sprintf (feld_von[feld_nr],       "%s", valfrom);
        sprintf (feld_bis[feld_nr],       "%s", valto);
        feld_nr ++;
        return TRUE;
}


void SetMamain1 (HWND hWnd)
{
      mamain1 = hWnd;
}

void SetFormular (BOOL frml)
{
          formular = frml;
}

void BelegeGdiDrucker (void)
/**
Drucker Win.ini holen
**/
{
       int i;
	   char *pr;
	   char AktDevice [80];
       char ziel [256];
       char quelle [256];
       char *frmenv;

       frmenv = getenv ("FRMPATH");
       if (frmenv == NULL) return;

       GetAllPrinters ();
     
       i = 0;
       while (TRUE)
       {
                pr = GetNextPrinter ();
                if (pr == NULL) break;
                strcpy (akt_drucker[i], pr);
                i ++;
       }
       drkanz = i;

	   if (GetPrinter (AktDevice))
	   {
		        strcpy (vDrkChoise, AktDevice);      
	   }
       sprintf (quelle, "%s\\%s", getenv ("FRMPATH"), "gdi.cfg");
       sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
       CopyFile (quelle, ziel, FALSE);
       sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 clipped (sys_ben.pers_nam),
                                 "akt");
       CopyFile (quelle, ziel, FALSE);
}


void BelegeDrucker (void)
/**
Drucker aus Formatverzeichnis holen.
**/
{
      char frmdir [256];
      char ziel [256];
      char quelle [256];
      char *frmenv;
      WIN32_FIND_DATA lpffd;
      HANDLE hFile;
      char *p;
      int i;

      GraphikMode = IsGdiPrint ();

      if (GraphikMode)
      {
               BelegeGdiDrucker ();
               return;
      }
      frmenv = getenv ("FRMPATH");
      if (frmenv == NULL) return;

      sprintf (frmdir, "%s\\*.cfg", frmenv);

      hFile = FindFirstFile (frmdir, &lpffd);

      if (hFile == INVALID_HANDLE_VALUE)
      {
                      return;
      }

      i = 0;
      p = strchr (lpffd.cFileName, '.');
      if (p) *p = (char) 0;

      sprintf (quelle, "%s\\%s.cfg", getenv ("FRMPATH"), lpffd.cFileName);
      sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
      strcpy (akt_drucker[i], lpffd.cFileName);
      i ++;

      while (FindNextFile (hFile, &lpffd))
      {
               p = strchr (lpffd.cFileName, '.');
               if (p) *p = (char) 0;
               strcpy (akt_drucker[i], lpffd.cFileName);
               i ++;
      }
      FindClose (hFile);

      drkanz = i;
}

void drucker_akt (char *dname)
/**
Ausgewaehlten Drucker in drucker.akt_uebertragen.
**/
{
        char ziel [256];
        char quelle [256];

		if (GraphikMode)
		{
		        SetPrinter (clipped (dname));
				return;
		}
        sprintf (quelle, "%s\\%s.cfg", getenv ("FRMPATH"), dname);
        sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
        CopyFile (quelle, ziel, FALSE);
        clipped (sys_ben.pers_nam);
        if (strlen (sys_ben.pers_nam))
        {
                  sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 sys_ben.pers_nam,
                                 "akt");
        }
        else
        {
                  sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                  "drucker",
                                 "akt");
        }
        CopyFile (quelle, ziel, FALSE);
}


void StartPrint  (void)
/**
Drucken.
**/
{
         ahWnd = GetForegroundWindow ();
         switch (ausgabe)
         {
                  case FILE_OUT :
                            print_file ();
                            break;
                  case SCREEN_OUT :
                            DruckeFile ();
                            ShowFile ();
                            break;
                  case PRINTER_OUT :
                            DruckeFile ();
                            printerfile();
                            break;
                  case FAX_OUT :
                            DruckeFile ();
                            faxfile();
                            break;
                  case FILE_DSAVE :
                            break;
         }
}



static ITEM iOutputTxt    ("", "Ausgabe",      "", 0);   
//static ITEM iDrkChoiseTxt ("", "Drucker",      "", 0);
static ITEM iDrkChoiseTxt ("", "",      "", 0);

static ITEM iOutput       ("", vOutput,        "", 0);   
static ITEM iDrkChoise    ("", vDrkChoise,     "", 0);

static ITEM vOK     ("",  " OK ",  "", 0);
static ITEM vCancel ("",  "Abbrechen", "", 0);

static int testcombo (void);
static int testOK (void);
static int testCancel (void);

static field _fDrkw [] = {
&iOutputTxt,      8, 0,  1, 2,  0, "", DISPLAYONLY, 0, 0, 0,
&iOutput,        40,10,  1,11,  0, "", COMBOBOX | DROPLIST, 0, testcombo, 0,     
&iDrkChoiseTxt,   8, 0,  3, 2,  0, "", DISPLAYONLY, 0, 0, 0,
&iDrkChoise,     40,10,  3,11,  0, "", COMBOBOX| DROPLIST, 0, testcombo, 0,     
&vOK,            11, 0,  5,15,  0, "", BUTTON, 0, testOK,     KEY12,
&vCancel,        11, 0,  5,28,  0, "", BUTTON, 0, testCancel, KEY5, 
};

static char Otab [MAXTAB][21];
static char Dtab [MAXTAB][21];

static combofield  cbfield[] = {1,  21, 0, (char *) Otab,
                                3,  21, 0, (char *) Dtab, 
                                0,  0, 0, NULL};


static form fDrkw = {6, 0, 0, _fDrkw, 0, 0, 0, cbfield, NULL}; 

static field _fDrkw0 [] = {
//&iDrkChoiseTxt,   8, 0,  1, 2,  0, "", DISPLAYONLY, 0, 0, 0,
&iDrkChoiseTxt,   0, 0,  1, 2,  0, "", DISPLAYONLY, 0, 0, 0,
&iDrkChoise,     40,10,  1,11,  0, "", COMBOBOX| DROPLIST, 0, testcombo, 0,     
&vOK,            11, 0,  3,15,  0, "", BUTTON, 0, testOK,     KEY12,
&vCancel,        11, 0,  3,28,  0, "", BUTTON, 0, testCancel, KEY5, 
};


static combofield  cbfield0[] = {1,  21, 0, (char *) Dtab,
                                0,  0, 0, NULL};


static form fDrkw0 = {4, 0, 0, _fDrkw0, 0, 0, 0, cbfield0, NULL}; 


static int testcombo (void)
{
       if (testkeys ()) return 0;

       if (syskey == KEYCR)
       {
           syskey = KEY12;
           break_enter ();
       }
       return 0;
}


static int testCancel (void)
{
       if (testkeys ()) return 0;

       switch (syskey)
       {
          case KEYDOWN :
          case KEYUP :
          case KEYTAB :
              return 0;
       }
       syskey = KEY5;
       break_enter ();
       return 0;
}

static int testOK (void)
{
       if (testkeys ()) return 0;

       switch (syskey)
       {
          case KEYDOWN :
          case KEYUP :
          case KEYTAB :
              return 0;
       }

       syskey = KEY12;
       break_enter ();
       return 0;
}

static int eBreak (void)
{
       switch (syskey)
       {
          case KEYDOWN :
          case KEYUP :
          case KEYTAB :
              return 0;
       }
       syskey = KEY5;
       break_enter ();
       return 0;
}

static int eOk (void)
{
       switch (syskey)
       {
          case KEYDOWN :
          case KEYUP :
          case KEYTAB :
              return 0;
       }
       syskey = KEY12;
       break_enter ();
       return 0;
}

void InitCombo (void)
/**
Combofelder fuellen.
**/
{
        static BOOL ComboOK = FALSE;
        int i;

        if (ComboOK) return;

        strcpy (vOutput, "Drucker");
        strcpy (Otab[0], "Drucker");
        strcpy (Otab[1], "Bildschirm");
        strcpy (Otab[2], "Datei");
        cbfield[0].cbanz = 3;
        BelegeDrucker ();
        for (i = 0; i < drkanz; i ++)
        {
               strcpy (Dtab[i], akt_drucker[i]);
        }
        cbfield[1].cbanz = i;
//        strcpy (vDrkChoise, akt_drucker[0]);
        ComboOK = TRUE;
}

void InitCombo0 (void)
/**
Combofelder fuellen.
**/
{
        static BOOL ComboOK0 = FALSE;
        int i;

//        if (ComboOK0) return;

        BelegeDrucker ();
        for (i = 0; i < drkanz; i ++)
        {
               strcpy (Dtab[i], akt_drucker[i]);
        }
        cbfield0[0].cbanz = i;
//        strcpy (vDrkChoise, akt_drucker[0]);
        ComboOK0 = TRUE;
}

void InitCombo1 (void)
/**
Combofelder fuellen.
**/
{
        static BOOL ComboOK0 = FALSE;
        int i;

        if (ComboOK0) return;

        BelegeGdiDrucker ();
        for (i = 0; i < drkanz; i ++)
        {
               strcpy (Dtab[i], akt_drucker[i]);
        }
        cbfield0[0].cbanz = i;
//        strcpy (vDrkChoise, akt_drucker[0]);
        ComboOK0 = TRUE;
}

void ChoiseCombo (void)
/**
Ausgewaehlte Combofelder auswerten.
**/
{
        int i;
        static int Output [] = {PRINTER_OUT, SCREEN_OUT, FILE_OUT};

        clipped (vOutput);
        for (i = 0; i < 3; i ++)
        {
            if (strcmp (vOutput,Otab[i]) == 0)
            {
                ausgabe = Output[i];
                break;
            }
        }
        clipped (vDrkChoise);
        if (strlen (vDrkChoise) == 0) return; 
        drucker_akt (vDrkChoise);
}


void ChoisePrinter (void)
/**
Ausgabe und Drucker w�hlen.
**/
{
        HWND hFind;
        HWND ahWnd;

        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        InitCombo ();
        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        no_break_end ();
        ahWnd = AktivWindow;
        EnableWindows (GetActiveWindow (), FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
        hFind = OpenWindowChC (8, 55, 10, 13, hMainInst,
                "Parameter f�r Liste");
        SetButtonTab (TRUE);
        enter_form (hFind, &fDrkw,0, 0);
        SetButtonTab (FALSE);
        AktivWindow = ahWnd;
        EnableWindows (AktivWindow, TRUE);
        SetActiveWindow (AktivWindow);
        DestroyWindow (hFind);
        hFind = NULL;
        no_break_end ();
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);
       if (syskey == KEY5) return;
       ChoiseCombo ();
}

void ChoiseCombo0 (void)
/**
Ausgewaehlte Combofelder auswerten.
**/
{
        clipped (vDrkChoise);
        if (strlen (vDrkChoise) == 0) return; 
        drucker_akt (vDrkChoise);
}

void ChoiseCombo01 (void)
/**
Ausgewaehlte Combofelder auswerten.
**/
{
        clipped (vDrkChoise);
        if (strlen (vDrkChoise) == 0) return; 
        SetPrinter (clipped (vDrkChoise));
}

void SetAusgabe (int ausg)
/**
Ausgabe-Medium setzen.
**/
{
	   ausgabe = ausg;
}

void ChoisePrinter0 (void)
/**
Drucker w�hlen.
**/
{
        HWND hFind;
        HWND ahWnd;

        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        ausgabe = PRINTER_OUT;
        InitCombo0 ();
        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        no_break_end ();
        ahWnd = AktivWindow;
		ahWnd = GetActiveWindow ();
        EnableWindows (GetActiveWindow (), FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
        hFind = OpenWindowChC (6, 55, 10, 13, hMainInst,
                "Auswahl Drucker");
        SetButtonTab (TRUE);
        enter_form (hFind, &fDrkw0,0, 0);
        SetButtonTab (FALSE);
        AktivWindow = ahWnd;
        EnableWindows (AktivWindow, TRUE);
        SetActiveWindow (AktivWindow);
        DestroyWindow (hFind);
        hFind = NULL;
        no_break_end ();
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);
        if (syskey == KEY5) return;
        ChoiseCombo0 ();
}

void ChoisePrinter1 (void)
/**
Drucker w�hlen.
**/
{
        HWND hFind;
        HWND ahWnd;

        save_fkt (5);
        save_fkt (12);
        save_fkt (6);

        ausgabe = PRINTER_OUT;
        InitCombo1 ();
        set_fkt (eBreak, 5);
        set_fkt (eOk, 12);

        no_break_end ();
        ahWnd = AktivWindow;
		ahWnd = GetActiveWindow ();
        EnableWindows (GetActiveWindow (), FALSE);
        SetBorder (WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_DLGFRAME);
        hFind = OpenWindowChC (6, 55, 10, 13, hMainInst,
                "Auswahl Drucker");
        SetButtonTab (TRUE);
        enter_form (hFind, &fDrkw0,0, 0);
        SetButtonTab (FALSE);
        AktivWindow = ahWnd;
        EnableWindows (AktivWindow, TRUE);
        SetActiveWindow (AktivWindow);
        DestroyWindow (hFind);
        hFind = NULL;
        no_break_end ();
        restore_fkt (5);
        restore_fkt (12);
        restore_fkt (6);
        if (syskey == KEY5) return;
        ChoiseCombo01 ();
}

