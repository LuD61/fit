#ifndef _TOUCHF_DEF
#define _TOUCHF_DEF
#include "nublock.h"

class TOUCHF
{
      private :
      public :
		  TOUCHF ();
		  void SetWriteParams (void (*) (int), void (*) (int),void (*) (int), void (*) (int));
          int FunkEnterBreak ();
          HWND IsChild (POINT *);
          int OnPaint (HWND,UINT,WPARAM,LPARAM);
          int OnButton (HWND,UINT,WPARAM,LPARAM);
          void RegisterFunk (WNDPROC);
          int GetAktButton (void);
          void PrevKey (void);
          void NextKey (void);
          void ActionKey (void);
          int IsFunkKey (MSG *);
          void MainInstance (HANDLE, HWND);
          void SetParent (HWND);
          void ScreenParam (double scrx, double scry);
          void EnterFunkBox (HWND, int, int, WNDPROC);
          void SetEtiAttr (char *, char *, char *, char *);
};
#endif