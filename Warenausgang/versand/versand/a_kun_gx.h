#ifndef _A_KUN_GX_DEF
#define _A_KUN_GX_DEF
#include "dbclass.h"

struct A_KUN_GX {
   short     mdn;
   short     fil;
   long      kun;
   double    a;
   char      a_kun[13];
   char      a_bz1[25];
   short     me_einh_kun;
   double    inh;
   char      kun_bran2[3];
   double    tara;
   double    ean;
   char      a_bz2[25];
   short     hbk_ztr;
   long      kopf_text;
   char      pr_rech_kz[2];
   char      modif[2];
   long      text_nr;
   short     devise;
   char      geb_eti[2];
   short     geb_fill;
   long      geb_anz;
   char      pal_eti[2];
   short     pal_fill;
   long      pal_anz;
   char      pos_eti[2];
   short     sg1;
   short     sg2;
   short     pos_fill;
   short     ausz_art;
   long      text_nr2;
   short     cab;
   char      a_bz3[25];
   char      a_bz4[25];
   short     eti_typ;
   long      mhd_text;
   long      freitext1;
   long      freitext2;
   long      freitext3;
   short     sg3;
   short     eti_sum1;
   short     eti_sum2;
   short     eti_sum3;
   long      ampar;
   short     sonder_eti;
   long      text_nr_a;
};
extern struct A_KUN_GX a_kun_gx, a_kun_gx_null;

#line 6 "a_kun_gx.hpp"

extern struct A_KUN_GX a_kun_gx, a_kun_gx_null;

class A_KUN_GX_CLASS : public DB_CLASS
{
     private :
       int cursor_bra;
     public :  
       A_KUN_GX_CLASS () : DB_CLASS (), cursor_bra (-1)
       {
       }
       void prepare (void);
       int dbreadfirst (void);
       int dbread_bra (void);
};

#endif