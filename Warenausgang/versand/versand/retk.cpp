#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "retk.h"

struct RETK retk, retk_null;

void RETK_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &retk.mdn, 1, 0);
            ins_quest ((char *) &retk.fil, 1, 0);
            ins_quest ((char *) &retk.ret, 2, 0);
    out_quest ((char *) &retk.mdn,1,0);
    out_quest ((char *) &retk.fil,1,0);
    out_quest ((char *) &retk.ret,2,0);
    out_quest ((char *) &retk.ls,2,0);
    out_quest ((char *) &retk.kun_fil,1,0);
    out_quest ((char *) retk.feld_bz1,0,20);
    out_quest ((char *) &retk.kun,2,0);
    out_quest ((char *) &retk.ret_dat,2,0);
    out_quest ((char *) &retk.lieferdat,2,0);
    out_quest ((char *) retk.hinweis,0,49);
    out_quest ((char *) &retk.ret_stat,1,0);
    out_quest ((char *) retk.feld_bz2,0,12);
    out_quest ((char *) retk.kun_krz1,0,17);
    out_quest ((char *) retk.feld_bz3,0,8);
    out_quest ((char *) &retk.adr,2,0);
    out_quest ((char *) &retk.delstatus,1,0);
    out_quest ((char *) &retk.rech,2,0);
    out_quest ((char *) retk.blg_typ,0,2);
    out_quest ((char *) &retk.kopf_txt,2,0);
    out_quest ((char *) &retk.fuss_txt,2,0);
    out_quest ((char *) &retk.vertr,2,0);
    out_quest ((char *) &retk.inka_nr,2,0);
    out_quest ((char *) &retk.of_po,3,0);
    out_quest ((char *) &retk.teil_smt,1,0);
    out_quest ((char *) retk.pers_nam,0,9);
    out_quest ((char *) &retk.of_ek,3,0);
    out_quest ((char *) &retk.of_po_euro,3,0);
    out_quest ((char *) &retk.of_po_fremd,3,0);
    out_quest ((char *) &retk.of_ek_euro,3,0);
    out_quest ((char *) &retk.of_ek_fremd,3,0);
    out_quest ((char *) &retk.waehrung,1,0);
            cursor = prepare_sql ("select retk.mdn,  retk.fil,  "
"retk.ret,  retk.ls,  retk.kun_fil,  retk.feld_bz1,  retk.kun,  retk.ret_dat,  "
"retk.lieferdat,  retk.hinweis,  retk.ret_stat,  retk.feld_bz2,  "
"retk.kun_krz1,  retk.feld_bz3,  retk.adr,  retk.delstatus,  retk.rech,  "
"retk.blg_typ,  retk.kopf_txt,  retk.fuss_txt,  retk.vertr,  retk.inka_nr,  "
"retk.of_po,  retk.teil_smt,  retk.pers_nam,  retk.of_ek,  "
"retk.of_po_euro,  retk.of_po_fremd,  retk.of_ek_euro,  "
"retk.of_ek_fremd,  retk.waehrung from retk "

#line 26 "retk.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ret = ? ");

    ins_quest ((char *) &retk.mdn,1,0);
    ins_quest ((char *) &retk.fil,1,0);
    ins_quest ((char *) &retk.ret,2,0);
    ins_quest ((char *) &retk.ls,2,0);
    ins_quest ((char *) &retk.kun_fil,1,0);
    ins_quest ((char *) retk.feld_bz1,0,20);
    ins_quest ((char *) &retk.kun,2,0);
    ins_quest ((char *) &retk.ret_dat,2,0);
    ins_quest ((char *) &retk.lieferdat,2,0);
    ins_quest ((char *) retk.hinweis,0,49);
    ins_quest ((char *) &retk.ret_stat,1,0);
    ins_quest ((char *) retk.feld_bz2,0,12);
    ins_quest ((char *) retk.kun_krz1,0,17);
    ins_quest ((char *) retk.feld_bz3,0,8);
    ins_quest ((char *) &retk.adr,2,0);
    ins_quest ((char *) &retk.delstatus,1,0);
    ins_quest ((char *) &retk.rech,2,0);
    ins_quest ((char *) retk.blg_typ,0,2);
    ins_quest ((char *) &retk.kopf_txt,2,0);
    ins_quest ((char *) &retk.fuss_txt,2,0);
    ins_quest ((char *) &retk.vertr,2,0);
    ins_quest ((char *) &retk.inka_nr,2,0);
    ins_quest ((char *) &retk.of_po,3,0);
    ins_quest ((char *) &retk.teil_smt,1,0);
    ins_quest ((char *) retk.pers_nam,0,9);
    ins_quest ((char *) &retk.of_ek,3,0);
    ins_quest ((char *) &retk.of_po_euro,3,0);
    ins_quest ((char *) &retk.of_po_fremd,3,0);
    ins_quest ((char *) &retk.of_ek_euro,3,0);
    ins_quest ((char *) &retk.of_ek_fremd,3,0);
    ins_quest ((char *) &retk.waehrung,1,0);
            sqltext = "update retk set retk.mdn = ?,  "
"retk.fil = ?,  retk.ret = ?,  retk.ls = ?,  retk.kun_fil = ?,  "
"retk.feld_bz1 = ?,  retk.kun = ?,  retk.ret_dat = ?,  "
"retk.lieferdat = ?,  retk.hinweis = ?,  retk.ret_stat = ?,  "
"retk.feld_bz2 = ?,  retk.kun_krz1 = ?,  retk.feld_bz3 = ?,  "
"retk.adr = ?,  retk.delstatus = ?,  retk.rech = ?,  retk.blg_typ = ?,  "
"retk.kopf_txt = ?,  retk.fuss_txt = ?,  retk.vertr = ?,  "
"retk.inka_nr = ?,  retk.of_po = ?,  retk.teil_smt = ?,  "
"retk.pers_nam = ?,  retk.of_ek = ?,  retk.of_po_euro = ?,  "
"retk.of_po_fremd = ?,  retk.of_ek_euro = ?,  retk.of_ek_fremd = ?,  "
"retk.waehrung = ? "

#line 31 "retk.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ret = ?";

            ins_quest ((char *) &retk.mdn, 1, 0);
            ins_quest ((char *) &retk.fil, 1, 0);
            ins_quest ((char *) &retk.ret, 2, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &retk.mdn, 1, 0);
            ins_quest ((char *) &retk.fil, 1, 0);
            ins_quest ((char *) &retk.ret, 2, 0);
            test_upd_cursor = prepare_sql ("select ret from retk "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ret = ? for update");
            ins_quest ((char *) &retk.mdn, 1, 0);
            ins_quest ((char *) &retk.fil, 1, 0);
            ins_quest ((char *) &retk.ret, 2, 0);
            del_cursor = prepare_sql ("delete from retk "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ret = ? ");
    ins_quest ((char *) &retk.mdn,1,0);
    ins_quest ((char *) &retk.fil,1,0);
    ins_quest ((char *) &retk.ret,2,0);
    ins_quest ((char *) &retk.ls,2,0);
    ins_quest ((char *) &retk.kun_fil,1,0);
    ins_quest ((char *) retk.feld_bz1,0,20);
    ins_quest ((char *) &retk.kun,2,0);
    ins_quest ((char *) &retk.ret_dat,2,0);
    ins_quest ((char *) &retk.lieferdat,2,0);
    ins_quest ((char *) retk.hinweis,0,49);
    ins_quest ((char *) &retk.ret_stat,1,0);
    ins_quest ((char *) retk.feld_bz2,0,12);
    ins_quest ((char *) retk.kun_krz1,0,17);
    ins_quest ((char *) retk.feld_bz3,0,8);
    ins_quest ((char *) &retk.adr,2,0);
    ins_quest ((char *) &retk.delstatus,1,0);
    ins_quest ((char *) &retk.rech,2,0);
    ins_quest ((char *) retk.blg_typ,0,2);
    ins_quest ((char *) &retk.kopf_txt,2,0);
    ins_quest ((char *) &retk.fuss_txt,2,0);
    ins_quest ((char *) &retk.vertr,2,0);
    ins_quest ((char *) &retk.inka_nr,2,0);
    ins_quest ((char *) &retk.of_po,3,0);
    ins_quest ((char *) &retk.teil_smt,1,0);
    ins_quest ((char *) retk.pers_nam,0,9);
    ins_quest ((char *) &retk.of_ek,3,0);
    ins_quest ((char *) &retk.of_po_euro,3,0);
    ins_quest ((char *) &retk.of_po_fremd,3,0);
    ins_quest ((char *) &retk.of_ek_euro,3,0);
    ins_quest ((char *) &retk.of_ek_fremd,3,0);
    ins_quest ((char *) &retk.waehrung,1,0);
            ins_cursor = prepare_sql ("insert into retk (mdn,  "
"fil,  ret,  ls,  kun_fil,  feld_bz1,  kun,  ret_dat,  lieferdat,  hinweis,  ret_stat,  "
"feld_bz2,  kun_krz1,  feld_bz3,  adr,  delstatus,  rech,  blg_typ,  kopf_txt,  "
"fuss_txt,  vertr,  inka_nr,  of_po,  teil_smt,  pers_nam,  of_ek,  of_po_euro,  "
"of_po_fremd,  of_ek_euro,  of_ek_fremd,  waehrung) "

#line 55 "retk.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 57 "retk.rpp"
}
int RETK_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

