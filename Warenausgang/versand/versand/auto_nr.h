#ifndef _AUTO_NR_DEF
#define _AUTO_NR_DEF

#include "dbclass.h"

#ifndef AUTO_NR_STRUCT
#define AUTO_NR_STRUCT
struct AUTO_NR {
   short     mdn;
   short     fil;
   char      nr_nam[11];
   long      nr_nr;
   short     satz_kng;
   long      max_wert;
   char      nr_char[11];
   long      nr_char_lng;
   char      fest_teil[11];
   char      nr_komb[21];
   short     delstatus;
};
#endif

extern struct AUTO_NR auto_nr, auto_nr_null;

#line 7 "auto_nr.rh"

class AUTO_NR_CLASS : public DB_CLASS 
{
       private :
               int cursor_0; 
               int cursor_1;
               int cursor_2;
               int cursor_del_nr; 
               int upd_cursor_2;
               int test_upd_cursor_2;
               int dsqlstatus;
               void prepare (void);
       public :
               AUTO_NR_CLASS () : DB_CLASS (), cursor_0 (-1), cursor_1 (-1), 
                                  cursor_2 (-1), cursor_del_nr (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_0 (void);
               int dbread_0 (void);
               int dbreadfirst_1 (void);
               int dbread_1 (void);
               int dbreadfirst_2 (void);
               int dbread_2 (void);
               int dbdelete_nr_nam (void);
               int dbupdate_2 (void);
};
#endif
