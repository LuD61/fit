create table "fit".shop_beilagen
(
	a decimal(13,0),
	a_beilage decimal(13,0),
	sort smallint,
	a_beilage_bez char (64)
) in fit_dat extent size 32 next size 256 lock mode row;

create unique index i01shop_beilagen on shop_beilagen (a,a_beilage);
