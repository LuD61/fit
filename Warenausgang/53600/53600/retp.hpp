#ifndef _RETP_DEF
#define _RETP_DEF
#include "dbclass.h"

table retp

extern struct RETP retp, retp_null;

class RETP_CLASS : public DB_CLASS
{
     private :
       int del_cursor_ret;
     public :  
       RETP_CLASS () : DB_CLASS (), del_cursor_ret (-1)
       {
       }
       void prepare (void);
       int dbreadfirst (void);
       int dbdelete_ret (void);
};

#endif