#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "ptab.h"
#include "dbfunc.h"

struct PTABK ptabk;
struct PTABN ptabn, ptabn_null;

void PTAB_CLASS::prepare  (void)
{
    ins_quest (ptabk.ptitem,0,19);
    out_quest (ptabk.ptitem,0,19);
    out_quest (ptabk.ptbenutz,0,9);
    out_quest (ptabk.pttitle,0,19);
    out_quest ((char *) &ptabk.ptanzpos,2,0);
    out_quest (ptabk.ptdtyp,0,16);
    out_quest (ptabk.ptwer1,0,9);
    out_quest (ptabk.ptwer2,0,9);
    out_quest ((char *) &ptabk.delstatus,1,0);
    out_quest ((char *) &ptabk.akv,2,0);
    out_quest ((char *) &ptabk.bearb,2,0);
    out_quest (ptabk.pers_nam,0,9);
cursor_ptabk = prepare_sql ("select ptabk.ptitem,  "
"ptabk.ptbenutz,  ptabk.pttitle,  ptabk.ptanzpos,  ptabk.ptdtyp,  "
"ptabk.ptwer1,  ptabk.ptwer2,  ptabk.delstatus,  ptabk.akv,  ptabk.bearb,  "
"ptabk.pers_nam "
                            "from ptabk "
                            "where ptitem = ?");

    ins_quest (ptabn.ptitem,0,19);
    ins_quest (ptabn.ptwert,0,4);

    out_quest (ptabn.ptitem,0,19);
    out_quest ((char *) &ptabn.ptlfnr,2,0);
    out_quest (ptabn.ptwert,0,4);
    out_quest (ptabn.ptbez,0,33);
    out_quest (ptabn.ptbezk,0,9);
    out_quest (ptabn.ptwer1,0,9);
    out_quest (ptabn.ptwer2,0,9);
    out_quest ((char *) &ptabn.delstatus,1,0);
    out_quest ((char *) &ptabn.akv,2,0);
    out_quest ((char *) &ptabn.bearb,2,0);
    out_quest (ptabn.pers_nam,0,9);
cursor_ptabn = prepare_sql ("select ptabn.ptitem,  ptabn.ptlfnr,  "
"ptabn.ptwert,  ptabn.ptbez,  ptabn.ptbezk,  ptabn.ptwer1,  ptabn.ptwer2,  "
"ptabn.delstatus,  ptabn.akv,  ptabn.bearb,  ptabn.pers_nam "
                            "from ptabn "
                            "where ptitem = ? "
                            "and ptwert = ?");

    ins_quest (ptabn.ptitem,0,19);

    out_quest (ptabn.ptitem,0,19);
    out_quest ((char *) &ptabn.ptlfnr,2,0);
    out_quest (ptabn.ptwert,0,4);
    out_quest (ptabn.ptbez,0,33);
    out_quest (ptabn.ptbezk,0,9);
    out_quest (ptabn.ptwer1,0,9);
    out_quest (ptabn.ptwer2,0,9);
    out_quest ((char *) &ptabn.delstatus,1,0);
    out_quest ((char *) &ptabn.akv,2,0);
    out_quest ((char *) &ptabn.bearb,2,0);
    out_quest (ptabn.pers_nam,0,9);
cursor_ptabn_all = prepare_sql ("select ptabn.ptitem,  ptabn.ptlfnr,  "
"ptabn.ptwert,  ptabn.ptbez,  ptabn.ptbezk,  ptabn.ptwer1,  ptabn.ptwer2,  "
"ptabn.delstatus,  ptabn.akv,  ptabn.bearb,  ptabn.pers_nam "
                            "from ptabn "
                            "where ptitem = ? "
                            "order by ptlfnr");
}

int PTAB_CLASS::lese_ptab (char *item, char *ptwert)
/**
Tabelle ptabk und ptabn.
**/
{
         if (cursor_ptabk == -1)
         {
                      prepare ();
         }
         strcpy (ptabk.ptitem, item);
         strcpy (ptabn.ptitem, item);
         strcpy (ptabn.ptwert, ptwert);
         open_sql (cursor_ptabk);
         fetch_sql (cursor_ptabk);
         if (sqlstatus == 100)
         {
                    return 100;
         }
         open_sql (cursor_ptabn);
         fetch_sql (cursor_ptabn);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int PTAB_CLASS::lese_ptab (void)
/**
Naechsten Satz aus Tabelle ptabn lesen.
**/
{
         fetch_sql (cursor_ptabn);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int PTAB_CLASS::lese_ptab_all (char *item)
/**
Tabelle ptabk und ptabn.
**/
{
         if (cursor_ptabk == -1)
         {
                      prepare ();
         }
         strcpy (ptabk.ptitem, item);
         strcpy (ptabn.ptitem, item);
         open_sql (cursor_ptabk);
         fetch_sql (cursor_ptabk);
         if (sqlstatus == 100)
         {
                    return 100;
         }
         open_sql (cursor_ptabn_all);
         fetch_sql (cursor_ptabn_all);
         if (sqlstatus == 0)
         {
                    return 0;
         }
         return 100;
}

int PTAB_CLASS::lese_ptab_all (void)
/**
Naechsten Satz aus Tabelle ptabn lesen.
**/
{
         fetch_sql (cursor_ptabn_all);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

