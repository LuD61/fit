#ifndef _RETK_DEF
#define _RETK_DEF
#include "dbclass.h"

table retk

extern struct RETK retk, retk_null;

class RETK_CLASS : public DB_CLASS
{
      public :
       RETK_CLASS () : DB_CLASS ()
       {
       }
       void prepare (void);
       int dbreadfirst (void);
};

#endif