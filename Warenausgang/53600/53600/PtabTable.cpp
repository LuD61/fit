// PtabTable.cpp: Implementierung der Klasse CPtabTable.
//
//////////////////////////////////////////////////////////////////////

#include "PtabTable.h"
#include "Text.h"

#define CString Text

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CPtabTable::CPtabTable()
{

}

CPtabTable::~CPtabTable()
{

}

LPSTR CPtabTable::GetArticle (LPSTR Name)
{
	PTABN *ptabn;
	LPSTR article = NULL;
	CString cName = Name;
	cName.MakeUpper ();
	cName.Trim ();
	Start ();
	while (((ptabn = GetNext ()) != NULL) && (article == NULL))
	{
		CString pName = ptabn->ptbezk;
		pName.MakeUpper ();
		pName.TrimRight ();
		if (cName == pName)
		{
			article = ptabn->ptwer1;
		}
	}

    return article;
}
