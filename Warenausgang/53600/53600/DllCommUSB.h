#ifndef _DLLCOMMUSB_DEF
#define _DLLCOMMUSB_DEF
#include <windows.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>


class CDllCommUSB
{
public:
	static HANDLE CommLib;
	DCB a_CommDcb ; 
	COMMTIMEOUTS a_CommTmo ;


    int (*OpenUsbPort)(int dm);
    int (*CloseUsbPort)(void);
    int (*WriteUsbPort)(char *cKundenAnzeige, int LenKundenAnzeige);
	CDllCommUSB ();


	static HANDLE schnittstelle;
	static char* comport;
	int OpenKundenDisplay (void);
	int WriteKundenDisplay (char* buffer1, char* buffer2);

};

#endif
