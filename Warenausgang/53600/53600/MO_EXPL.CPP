/* *********************************************************************** */
/*                                                                         */
/*                        Eingabe der Menue als Explorer                   */
/*                                                                         */
/*                        Wilhelm Roth 27.04.1998                          */
/*                                                                         */
/* *********************************************************************** */


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include <commctrl.h>
#include <math.h>
#include "strfkt.h"
#include "mo_menu.h"
#include "mo_expl.h"
#include "wmask.h"


LONG FAR PASCAL ExpProc(HWND,UINT,WPARAM,LPARAM);


void MEN_EXPL::RegisterExplorer (HWND hWnd, HANDLE hMainInst)
/**
Hauptwindow fuer Explorer registrieren.
**/
{
          static int registered = 0;
          WNDCLASS wc;


          if (registered) return;

          registered = 1;
          wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
          wc.lpfnWndProc   =  ExpProc;
          wc.cbClsExtra    =  0;
          wc.cbWndExtra    =  0;
          wc.hInstance     =  hMainInst;
          wc.hIcon         =  LoadIcon (hMainInst, "FITICON");
          wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
          wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
          wc.lpszMenuName  =  NULL;
          wc.lpszClassName =  "MenExplorer";
          RegisterClass(&wc);
          return;
}


void MEN_EXPL::OpenExplorer (HWND hWnd, HANDLE hMainInst)
/**
Explorerfenster Oeffnen.
**/
{
          int x, y, cx, cy;
          RECT rect;

          if (init_ok == 0)
          {
                 InitCommonControls ();
          }
          RegisterExplorer (hWnd, hMainInst);
          GetClientRect (hWnd,  &rect);
          x = 14;
          y = 45;
          cx = 400;
          cy = rect.bottom - 100;
          ExpMainWindow = CreateWindowEx (
                             0,
                            "MenExplorer",
                             NULL,
                             WS_POPUP | WS_VISIBLE | WS_DLGFRAME |
                             0,
                             x,y,
                             cx, cy,
                             hWnd,
                             0,
                             hMainInst,
                             0);
          x = 5;
          y = 5;
          cx = 380;
          cy -= 15;
          ExpWindow = CreateWindowEx (
                             WS_EX_CLIENTEDGE,
                             WC_TREEVIEW,
                             NULL,
                             WS_CHILD | WS_VISIBLE |
                             WS_CLIPSIBLINGS,
                             x,y,
                             cx, cy,
                             ExpMainWindow,
                             0,
                             hMainInst,
                             0);
          return;
}

int MEN_EXPL::IsDlgMessage (MSG *msg)
/**
Auf Dialogmeldung testen.
**/
{
       if (msg->message == WM_KEYDOWN)
       {
             switch (msg->wParam)
             {
                 case VK_F5 :
                         syskey = KEY5;
                         break_enter ();
                         return TRUE;
             }
       }
       return FALSE;
}

void MEN_EXPL::MenueExplorer (HWND hWnd, HANDLE hMainInst)
/**
Menue-Explorer ausfuehren.
**/
{
         MSG msg;
          
         OpenExplorer (hWnd, hMainInst);
         enter_break = 0;
         while (enter_break == 0)
         {
             if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
             {
                  if (IsDlgMessage (&msg) == 0)
                  {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
                  }
             }
         }
         DestroyWindow (ExpMainWindow);
         return;
}

LONG FAR PASCAL ExpProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        return DefWindowProc(hWnd, msg, wParam, lParam);
}


 
          
