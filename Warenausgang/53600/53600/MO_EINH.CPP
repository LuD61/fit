#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#ifndef CONSOLE
#include "wmask.h"
#include "mo_meld.h"
#endif
#include "mo_curso.h"
#include "strfkt.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "a_kun.h"
#include "kun.h"
#include "ptab.h"
#include "mo_einh.h"
#include "kumebest.h"

static KUN_CLASS kun_class;
static HNDW_CLASS hndw_class;
static PTAB_CLASS ptab_class;
static KMB_CLASS kmb_class;

static char (*Otab) [31];

static int KmbOK = 0;
static int dsqlstatus;
short knleer = 0;

static short *kmbtab[] = 
{                         &kumebest.me_einh0,
                          &kumebest.me_einh1, 
                          &kumebest.me_einh2, 
                          &kumebest.me_einh3, 
                          &kumebest.me_einh4, 
                          &kumebest.me_einh5, 
                          &kumebest.me_einh6,
};

static short *me_einh[] = {&kumebest.me_einh0,
                           &kumebest.me_einh1,
                           &kumebest.me_einh2,
                           &kumebest.me_einh3,
                           &kumebest.me_einh4,
                           &kumebest.me_einh5,
                           &kumebest.me_einh6};
                               
static short *knuepf[] = {&knleer,
                          &kumebest.knuepf1,
                          &kumebest.knuepf2,
                          &kumebest.knuepf3,
                          &kumebest.knuepf4,
                          &kumebest.knuepf5,
                          &kumebest.knuepf6};

static double *inh[]    = {&kumebest.inh0,
                           &kumebest.inh1,
                           &kumebest.inh2,
                           &kumebest.inh3,
                           &kumebest.inh4,
                           &kumebest.inh5,
                           &kumebest.inh6};


void EINH_CLASS::GetBasEinh (double a, KEINHEIT *keinheit)
/**
Basis-Einheit aus a_bas holen.
**/
{
        char wert [5];

        keinheit->me_einh_bas = _a_bas.me_einh;
        memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
        sprintf (wert, "%hd", _a_bas.me_einh);

        dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);

        if (dsqlstatus == 0)
        {
                   strcpy (keinheit->me_einh_bas_bez, ptabn.ptbezk);
        }
}
        
void EINH_CLASS::GetKunEinhBas (double a, KEINHEIT *keinheit)
/**
Kunden-Einheit aus a_hndw, a_eig oder a_eig_div holen.
**/
{
         char wert [5];

         switch (_a_bas.a_typ)
         {
                     case 1 :
                           dsqlstatus = hndw_class.lese_a_hndw (a);
                           break;
                     case 2 :
                           dsqlstatus = hndw_class.lese_a_eig (a);
                           break;
                     case 3 :
                           dsqlstatus = hndw_class.lese_a_eig_div (a);
                           break;
                     default :
                           return;
         }

         if (dsqlstatus)
         {
                     return;
         }

         switch (_a_bas.a_typ)
         {
                     case 2 :
                           a_hndw.me_einh_kun = a_eig.me_einh_ek;
                           a_hndw.inh         = a_eig.inh;
                           break;
                     case 3 :
                           a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                           a_hndw.inh         = a_eig_div.inh;
                           break;
          }

          keinheit->me_einh_kun = a_hndw.me_einh_kun;
          keinheit->inh      = a_hndw.inh;

          memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
          sprintf (wert, "%hd", a_hndw.me_einh_kun);

          dsqlstatus = ptab_class.lese_ptab ("me_einh_kun", wert);

          if (dsqlstatus == 0)
          {
                   strcpy (keinheit->me_einh_kun_bez, ptabn.ptbezk);
          }
}
 
int EINH_CLASS::ReadKmb (void)
/**
Combofelder fuellen.
**/
{
        int dsqlstatus;


        dsqlstatus = kmb_class.dbreadfirst ();
        if (dsqlstatus != 0 && kumebest.kun > 0)
        {
            dsqlstatus = kmb_class.dbreadfirst_bra ();
        }
        if (dsqlstatus != 0)
        {
            dsqlstatus = kmb_class.dbreadfirst_a ();
        }
        return dsqlstatus;
}

double EINH_CLASS::GetInh (int pos, double inh0)
/**
Inhalt in Basiseinheit ermitteln.
**/
{
        double inh;

        switch (pos)
        {
        case 0 :
                return inh0;
        case 1 :
                if (kumebest.knuepf1 == 1) return inh0; 
                inh = kumebest.inh1 * inh0;
                return GetInh (kumebest.knuepf1, inh);
        case 2 :
                if (kumebest.knuepf2 == 2) return inh0; 
                inh = kumebest.inh2 * inh0;
                return GetInh (kumebest.knuepf2, inh);
        case 3 :
                if (kumebest.knuepf3 == 3) return inh0; 
                inh = kumebest.inh3 * inh0;
                return GetInh (kumebest.knuepf3, inh);
        case 4 :
                if (kumebest.knuepf4 == 4) return inh0; 
                inh = kumebest.inh4 * inh0;
                return GetInh (kumebest.knuepf4, inh);
        case 5 :
                if (kumebest.knuepf5 == 5) return inh0; 
                inh = kumebest.inh5 * inh0;
                return GetInh (kumebest.knuepf5, inh);
        case 6 :
                if (kumebest.knuepf6 == 6) return inh0; 
                inh = kumebest.inh6 * inh0;
                return GetInh (kumebest.knuepf6, inh);
        }
        return inh0;
}

void EINH_CLASS::FillGeb0 (KEINHEIT *keinheit, int pos)
/**
Erste Gebindeebene holen.
**/
{
    char wert [5];         
    int i;

    i = pos;
    if (*me_einh[i] == 10) return;
    sprintf (wert, "%hd", *me_einh[i]);
    dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
    clipped (ptabn.ptwer1);
    if (strcmp(ptabn.ptwer1, "G") == 0) return;
    keinheit->me_einh1 = *me_einh[i];
    keinheit->inh1     = *inh[i];
    return;
}

void EINH_CLASS::FillGeb1P (KEINHEIT *keinheit, int pos)
/**
Mittlere und hoechste Gebindeebene holen.
**/
{
    char wert [5];         
    int i;

    for (i = pos + 1; i < 6; i ++)
    {
        if (*knuepf[i] == pos)
        {
            sprintf (wert, "%hd", *me_einh[i]);
            dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
            clipped (ptabn.ptwer1);
            if (*me_einh[i] == 10) return;
            if (strcmp(ptabn.ptwer1, "G") != 0)
            {
                    return;
            }
            keinheit->me_einh2 = *me_einh[i];
            keinheit->inh2     = *inh[i];
            FillGeb2 (keinheit, AufEinh + 1);
            return;
        }
    }
    for (i = 0; i < pos; i ++)
    {
        if (*knuepf[i] == pos)
        {
            sprintf (wert, "%hd", *me_einh[i]);
            dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
            clipped (ptabn.ptwer1);
            if (*me_einh[i] == 10) return;
            if (strcmp(ptabn.ptwer1, "G") != 0)
            {
                    return;
            }
            keinheit->me_einh2 = *me_einh[i];
            keinheit->inh2     = *inh[i];
            FillGeb2 (keinheit, AufEinh + 1);
            return;
        }
    }
}


void EINH_CLASS::FillGeb1M (KEINHEIT *keinheit, int pos)
/**
Mittlere und erste Gebindeebene holen.
**/
{
    char wert [5];         
    int i;

    i = pos;
    if (*me_einh[i] == 10) return;
    sprintf (wert, "%hd", *me_einh[i]);
    dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
    clipped (ptabn.ptwer1);
    if (strcmp(ptabn.ptwer1, "G") != 0) return;
    keinheit->me_einh2 = *me_einh[i];
    keinheit->inh2     = *inh[i];
    FillGeb0 (keinheit, *knuepf[i]);
    return;
}


void EINH_CLASS::FillGeb2 (KEINHEIT *keinheit, int pos)
/**
Hoechste Gebindeebene holen.
**/
{
    char wert [5];         
    int i;

    for (i = pos + 1; i < 6; i ++)
    {
        if (*knuepf[i] == pos)
        {
            sprintf (wert, "%hd", *me_einh[i]);
            dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
            clipped (ptabn.ptwer1);
            if (*me_einh[i] == 10);
            else if (strcmp(ptabn.ptwer1, "G") == 0);
            else
            {
                    return;
            }
            keinheit->me_einh3 = *me_einh[i];
            keinheit->inh3     = *inh[i];
            return;
        }
    }

    for (i = 0; i < pos; i ++)
    {
        if (*knuepf[i] == pos)
        {
            sprintf (wert, "%hd", *me_einh[i]);
            dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
            clipped (ptabn.ptwer1);
            if (*me_einh[i] == 10);
            else if (strcmp(ptabn.ptwer1, "G") == 0);
            else
            {
                    return;
            }
            keinheit->me_einh3 = *me_einh[i];
            keinheit->inh3     = *inh[i];
            return;
        }
    }
}


void EINH_CLASS::FillGeb (KEINHEIT *keinheit)
/**
Gebindeeinheiten aus kumebest fuellen.
**/
{
         char wert [5];         
         short is_palette = 0;
         short is_gebinde = 0;

         keinheit->me_einh1 = 0;
         keinheit->inh1 = (double) 0.0;
         keinheit->me_einh2 = 0;
         keinheit->inh2 = (double) 0.0;
         keinheit->me_einh3 = 0;
         keinheit->inh3 = (double) 0.0;

         if (*me_einh[AufEinh] == 10)
         {
                    is_palette = 1;
         }
         else
         {
                     sprintf (wert, "%hd", *me_einh[AufEinh]);
                     dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
                     clipped (ptabn.ptwer1);
                     if (strcmp(ptabn.ptwer1, "G") == 0)
                     {
                         is_gebinde = 1;
                     }
          }
          if (is_gebinde)
          {
                     FillGeb0 (keinheit, *knuepf[AufEinh]);
                     keinheit->me_einh2 = *me_einh[AufEinh];
                     keinheit->inh2     = *inh[AufEinh];
                     FillGeb2 (keinheit, AufEinh);
          }
          else if (is_palette)
          {
                keinheit->me_einh3 = *me_einh[AufEinh];
                keinheit->inh3     = *inh[AufEinh];
                FillGeb1M (keinheit, *knuepf[AufEinh]);
          }
          else
          {
                keinheit->me_einh1 = *me_einh[AufEinh];
                keinheit->inh1     = *inh[AufEinh];
                FillGeb1P (keinheit, AufEinh);
          }
}


void EINH_CLASS::FillKmb (KEINHEIT *keinheit)
/**
Einheit aus kumebest fuellen.
**/
{
        char wert [5];

        switch (AufEinh)
        {
             case 0 :
                   keinheit->me_einh_kun = kumebest.me_einh0;
                   keinheit->inh      = kumebest.inh0;
                   sprintf (wert, "%hd", kumebest.me_einh0);
                   break;
             case 1 :
                   keinheit->me_einh_kun = kumebest.me_einh1;
                   keinheit->inh      = GetInh (kumebest.knuepf1, 
                                                kumebest.inh1);
                   sprintf (wert, "%hd", kumebest.me_einh1);
                   break;
             case 2 :
                   keinheit->me_einh_kun = kumebest.me_einh2;
                   keinheit->inh      = GetInh (kumebest.knuepf2, 
                                                kumebest.inh2);
                   sprintf (wert, "%hd", kumebest.me_einh2);
                   break;
             case 3 :
                   keinheit->me_einh_kun = kumebest.me_einh3;
                   keinheit->inh      = GetInh (kumebest.knuepf3, 
                                                kumebest.inh3);
                   sprintf (wert, "%hd", kumebest.me_einh3);
                   break;
             case 4 :
                   keinheit->me_einh_kun = kumebest.me_einh4;
                   keinheit->inh      = GetInh (kumebest.knuepf4, 
                                                kumebest.inh4);
                   sprintf (wert, "%hd", kumebest.me_einh4);
                   break;
             case 5 :
                   keinheit->me_einh_kun = kumebest.me_einh5;
                   keinheit->inh      = GetInh (kumebest.knuepf5, 
                                                kumebest.inh5);
                   sprintf (wert, "%hd", kumebest.me_einh5);
                   break;
             case 6 :
                   keinheit->me_einh_kun = kumebest.me_einh6;
                   keinheit->inh      = GetInh (kumebest.knuepf6, 
                                                kumebest.inh6);
                   sprintf (wert, "%hd", kumebest.me_einh6);
                   break;
        }

        memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
        dsqlstatus = ptab_class.lese_ptab ("me_einh_kun", wert);
        if (dsqlstatus == 0)
        {
                    strcpy (keinheit->me_einh_kun_bez,
                                      ptabn.ptbezk);
        }
}
         
void EINH_CLASS::AktAufEinh (short mdn, short fil, long kun_nr,
                             double a, int me_einh_kun)
/**
Mengeneinheit als Klartext fuer Kunde und Artikel holen.
**/
{

        kumebest.mdn = mdn;
        kumebest.fil = fil;
        kumebest.kun = kun_nr;
        kumebest.a   = a;
        kun_class.lese_kun (mdn, fil, kun_nr); 

        if (ReadKmb () == 0)
        {
                if (kumebest.me_einh0 == me_einh_kun)
                {
                    AufEinh = 0;
                }
                else if (kumebest.me_einh1 == me_einh_kun)
                {
                    AufEinh = 1;
                }
                else if (kumebest.me_einh2 == me_einh_kun)
                {
                    AufEinh = 2;
                }
                else if (kumebest.me_einh3 == me_einh_kun)
                {
                    AufEinh = 3;
                }
                else if (kumebest.me_einh4 == me_einh_kun)
                {
                    AufEinh = 4;
                }
                else if (kumebest.me_einh5 == me_einh_kun)
                {
                    AufEinh = 5;
                }
                else if (kumebest.me_einh6 == me_einh_kun)
                {
                    AufEinh = 6;
                }
                else
                {
                    AufEinh = 1;
                }
                return;
        }
        dsqlstatus = kun_class.lese_a_kun (mdn, fil, kun_nr, a);
        if (dsqlstatus == 0)
        {
                  if (me_einh_kun == a_kun.me_einh_kun)
                  {
                            AufEinh = 1;
                  }
                  else
                  {
                            AufEinh = 0;
                  }
                  return;

        }
        dsqlstatus = kun_class.lese_kun (mdn, fil, kun_nr);
        if (dsqlstatus == 0 && strcmp (kun.kun_bran2, "0") > 0)
        {
                 dsqlstatus = kun_class.lese_a_kun_bran (mdn, fil,
                                                kun.kun_bran2,
                                                a);
                   
                 if (dsqlstatus == 0)
                 {
                       if (me_einh_kun == a_kun.me_einh_kun)
                       {
                            AufEinh = 1;
                       }
                       else
                       {
                            AufEinh = 0;
                       }
                       return;
                 }
        }
        AufEinh = 0;
        dsqlstatus = lese_a_bas (a,-1);
        if (dsqlstatus)
        {
                     return;
        }
        switch (_a_bas.a_typ)
         {
                     case 1 :
                           dsqlstatus = hndw_class.lese_a_hndw (a);
                           break;
                     case 2 :
                           dsqlstatus = hndw_class.lese_a_eig (a);
                           break;
                     case 3 :
                           dsqlstatus = hndw_class.lese_a_eig_div (a);
                           break;
                     default :
                           return;
         }

         switch (_a_bas.a_typ)
         {
                     case 2 :
                           a_hndw.me_einh_kun = a_eig.me_einh_ek;
                           break;
                     case 3 :
                           a_hndw.me_einh_kun = a_eig_div.me_einh_ek;
                           break;
         }
         if (me_einh_kun == a_hndw.me_einh_kun)
         {
                     AufEinh = 1;
         }
}

void EINH_CLASS::GetKunEinh (short mdn, short fil, long kun_nr,
                            double a, KEINHEIT *keinheit)
/**
Mengeneinheit als Klartext fuer Kunde und Artikel holen.
**/
{
        char wert [5];

        keinheit->me_einh1 = 0;
        keinheit->inh1 = (double) 0.0;
        keinheit->me_einh2 = 0;
        keinheit->inh2 = (double) 0.0;
        keinheit->me_einh3 = 0;
        keinheit->inh3 = (double) 0.0;

        kumebest.mdn = mdn;
        kumebest.fil = fil;
        kumebest.kun = kun_nr;
        strcpy (kumebest.kun_bran2, "");
        kumebest.a   = a;
        kun_class.lese_kun (mdn, fil, kun_nr); 

        if (ReadKmb () == 0)
        {
                  GetBasEinh (a, keinheit);
                  FillKmb (keinheit);
                  FillGeb (keinheit);
                  return;
        }

        dsqlstatus = lese_a_bas (a,-1);
        dsqlstatus = kun_class.lese_a_kun (mdn, fil, kun_nr, a);
        if (dsqlstatus == 0)
        {
                  keinheit->me_einh_kun = a_kun.me_einh_kun;
                  keinheit->inh      = a_kun.inh;
                  memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
                  sprintf (wert, "%hd", a_kun.me_einh_kun);

                  dsqlstatus = ptab_class.lese_ptab ("me_einh_kun",
                                           wert);

                  if (dsqlstatus == 0)
                  {
                             strcpy (keinheit->me_einh_kun_bez,
                                     ptabn.ptbezk);
                  }
                  GetBasEinh (a, keinheit);
                  if (AufEinh == 0)
                  {
                              keinheit->me_einh_kun = keinheit->me_einh_bas;
                              strcpy (keinheit->me_einh_kun_bez,
                                      keinheit->me_einh_bas_bez); 
                  }
                  return;
        }

        dsqlstatus = kun_class.lese_kun (mdn, fil, kun_nr);
        if (dsqlstatus || strcmp (kun.kun_bran2, "  ") <= 0)
        {
                   GetKunEinhBas (a, keinheit);
                   GetBasEinh (a, keinheit);
                   if (AufEinh == 0)
                   {
                              keinheit->me_einh_kun = keinheit->me_einh_bas;
                              strcpy (keinheit->me_einh_kun_bez,
                                      keinheit->me_einh_bas_bez); 
                   }
                   return;
        }
        dsqlstatus = kun_class.lese_a_kun_bran (mdn, fil,
                                                kun.kun_bran2,
                                                a);
                   
        if (dsqlstatus == 0)
        {
                  keinheit->me_einh_kun = a_kun.me_einh_kun;
                  keinheit->inh      = a_kun.inh;
                  memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
                  sprintf (wert, "%hd", a_kun.me_einh_kun);

                  dsqlstatus = ptab_class.lese_ptab ("me_einh_kun",
                                           wert);

                  if (dsqlstatus == 0)
                  {
                             strcpy (keinheit->me_einh_kun_bez,
                                     ptabn.ptbezk);
                  }
                  GetBasEinh (a, keinheit);
                  if (AufEinh == 0)
                  {
                              keinheit->me_einh_kun = keinheit->me_einh_bas;
                              strcpy (keinheit->me_einh_kun_bez,
                                      keinheit->me_einh_bas_bez); 
                  }
                  return;
        }
        GetKunEinhBas (a, keinheit);
        GetBasEinh (a, keinheit);
        if (AufEinh == 0)
        {
                  keinheit->me_einh_kun = keinheit->me_einh_bas;
                  strcpy (keinheit->me_einh_kun_bez,
                          keinheit->me_einh_bas_bez); 
        }
}                 

void EINH_CLASS::FillOtab (int i)
/**
Text fuer Menegeneinheit fuellen.
**/
{
       short me_einh;
       char wert [4];
       int dsqlstatus;

       me_einh = *kmbtab[i];
       sprintf (wert, "%hd", me_einh);
       dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
       if (dsqlstatus == 0)
       {
                   sprintf (Otab[i], "%hd %s", me_einh, 
                                               clipped (ptabn.ptbezk));
       }
}

void EINH_CLASS::FillEinh (char EinhTab[][31])
/**
Auswahlbox fuellen.
**/
{
        static BOOL ComboOK = FALSE;
        int dsqlstatus;
        int i;

        Otab = EinhTab;
        dsqlstatus = kmb_class.dbreadfirst ();
        if (dsqlstatus != 0 && kumebest.kun > 0)
        {
            dsqlstatus = kmb_class.dbreadfirst_bra ();
        }
        if (dsqlstatus != 0)
        {
            dsqlstatus = kmb_class.dbreadfirst_a ();
        }
        if (dsqlstatus == 0)
        {
            for (i = 0; i < 7; i ++)
            {
                    if  (*kmbtab[i] == 0) break;
                    FillOtab (i);
            }
			Otab[i][0] = 0;
            return;
        }
 
        strcpy (Otab[0],    "1 Basis-Artikeleinheit");
        strcpy (Otab[1],    "2 Kunden-Bestelleinheit");
        Otab[2][0] =  0;
}

