#include<stdio.h>
#include<stdarg.h>
#include<string.h>
#include<windows.h>
#include<winbase.h>
#include "fit.h"
#include "stdfkt.h"
#include "strfkt.h"
#include "mo_curs0.h"
#include "mo_menu.h"
#include "mo_expl.h"
#include "mo_draw.h"
#include "wmask.h"
#include "mo_meld.h"
#include "ls.h"
#include "a_bas.h"
#include "a_hndw.h"
#include "kun.h"
#include "ptab.h"
#include "dbfunc.h"
#include "fnb.h"
#include "mdn.h"
#include "fil.h"
#include "a_pr.h"
#include "akt_krz.h"
#include "aktion.h"
#include "mo_a_pr.h"
#include "mo_preis.h"
#include "mo_einh.h"
#include "mo_geb.h"
#include "mo_numme.h"
#include "mo_auto.h"
#include "sys_peri.h"
#include "pht_wg.h"
#include "sys_par.h"
#include "mo_progcfg.h"
#include "datum.h"
#include "auto_nr.h"
#include "ls_txt.h"
#include "dbclass.h"
//#include "bws_asc.h"
#include "mo_gdruck.h"
#include "retk.h"
#include "retp.h"
#include "mo_passw.h"
#include "DllPreise.h"
#include "textblock.h"
#include "scanean.h"
#include "resource.h"
#include "Sounds.h"
#include "ReadNve.h"
#include "WriteNve.h"
#include "NveGew.h"
#include "karton.h"
#include "etubi.h"
#include "a_kun_gx.h"
#include "nublock.h"
#include "Integer.h"
#include "wiepro.h"


#define MAXME 99999.999
#define NO 0

static int ScanTuerke = 0;
static double akt_buch_me;
static BOOL GenPosi = FALSE;
static BOOL a_sort = FALSE;
static BOOL a_kum_par = FALSE;

BOOL AlphaCharge = FALSE;

static BOOL GenNve = FALSE;
BOOL IsEmptyScannBuffer (char *buffer);
BOOL MultiScan ();
BOOL ScanMultiA ();
int ScanA (void) ;
int DoScann ();
BOOL MeOK ();
int  ChangeChar (int key);
void FindCharge (char *buffer);
void EnterEan (char *buffer);
BOOL TestArtikel (double a);
int BreakEanEnter ();
void SetScannFocus ();
void            TestEnterCharge (void);
BOOL TestScanArticleExist ();
void CompareCharge ();
void            write_wawiepro (double brutto, double netto, double tara, int Anzahl);
void            write_wawiepro (double brutto, double netto, double tara, int Anzahl, char *erf_kz);
void KumAufPos (int art_pos);
int SysChoise0 ();

enum ChargeForceEnumeration
{
	No = 0,
	ForceIfEmpty = 1,
	ForceAllways = 2
};
int GetChargeInputLen ();
int dochargenwechsel (void);
void charge2a_bz1(  char *bez, char *charge );

char *IdentTarget = NULL;

BOOL ChargenWechsel = FALSE; /* GK 25.06.12 */
static int BezChargeLen = 0; /* GK 25.06.12 */
static char ls_charge_neu[40];
static BOOL ChargeDefault = FALSE;
BOOL TestChargeLen (char *buffer);
int				TestNewArtCharge (double a, Text& ListCharge);


static double ean_a = 0.0;
static double ean = 0.0;
int ScanArtikel (double a);
int ScanKunEan (double ean);


BOOL ScanMulti = FALSE;
BOOL IgnoreEmptyScann = FALSE;
static NveGew *nveGew = NULL;
HWND waithWnd;
HWND OldFocus;
static char NveFile[512] = {"nvenr"};

int DoScann (void);
WriteNve *LastWriteNvePalette = NULL;
void GenNveNr (char *NveNr);
int ReadAKunGx (double a);

static BOOL NveDebug = FALSE;
static char DebugFile [512] = {""};
static FILE *FDebug;
void WriteNveParafile (char *tmp);
BOOL WriteNewNve (char *tmp);
static char EtiPath [512];

static NUBLOCK NuBlock;

extern char LONGNULL[4];
static char *eti_kiste   = NULL;
static char *eti_nveex   = NULL;
static char *eti_nve   = NULL;
static char *eti_ez      = NULL;
static char *eti_ev      = NULL;
static char *eti_artikel = NULL;
static char *eti_palette = NULL;

BOOL AutoEtiArt = FALSE;
BOOL AutoEtiKi = FALSE;
BOOL AutoHandEtiArt = FALSE;
BOOL AutoHandEtiKi = FALSE;
BOOL AutoEtiArt0 = FALSE;
BOOL AutoHandEtiArt0 = FALSE;

static char *eti_nveex_drk  = NULL;
static char *eti_ki_drk  = NULL;
static char *eti_nve_drk  = NULL;
static char *eti_ez_drk  = NULL;
static char *eti_ev_drk  = NULL;
static char *eti_a_drk   = NULL;
static char *eti_palette_drk   = NULL;

static int eti_nveex_typ  = -1;
static int eti_ki_typ  = -1;
static int eti_ki_gew  = -1;
static int eti_nve_typ  = -1;
static int eti_ez_typ  = -1;
static int eti_ev_typ  = -1;
static int eti_a_typ   = -1;
static int eti_palette_typ  = -1;

static WIEPRO_CLASS WiePro;

static char wiegwert [40];

void CreateMessageWait (HWND hWnd, char *message);

void CalculateScaleAmount ();

int artikelOK (double a, BOOL DispMode);
void WriteNveDebug (char *format, ...);
int FillReadNve (ReadNve *readNve);
void TestNveEtikett ();
static char *nveEtikett = "Etikett";
static char *nvePalette = "Palette";
static int NveEtikett (void);
static          int TestNewArt (double, long);
BOOL StornoNve (ReadNve *readNve);

static BOOL GraphikMode = FALSE;

#define AUFMAX 1000

#define ENTER 0
#define SUCHEN 1
#define AUSWAHL 2

#define BWS "BWS"
#define RSDIR "RSDIR"

#define sql_in ins_quest
#define sql_out out_quest

#define BAR 6
#define SOFORT 1

#define WIEGEN 9
#define AUSZEICHNEN 1
#define STOP 2
#define MINUS -1
#define PLUS 1

#define VK_CLS 910

CDllPreise DllPreise;

BOOL DynColor = FALSE;
static int kunquery = 2;
static TEXTBLOCK *textblock = NULL;

static int screenwidth  = 800;
static int screenheight = 580;

static int EnterTyp = ENTER;

static SCANEAN Scanean ((Text) "scanean.cfg");
BOOL WithZerlData = FALSE;

static int mit_trans = 1;
static int paz_direkt = 0;
static long auszeichner;
static int ls_charge_par = 0;
static int lsc2_par = 0;
static int lsc3_par = 0;
static long StdWaage;
static double AktPrWieg[1000];
static double StornoWieg;
static int AktWiegPos = 0;
static BOOL CalcOn = FALSE;
static int aufart9999;
static short auf_art = 0;

static char KomDatum [12];
static char AktTour [6] = {"0"};

static long AutoSleep = 20;
static int  PassWord = FALSE;
static int testmode = 0;
extern BOOL ColBorder;
static BOOL colborder = FALSE;
static BOOL auf_me_kz = TRUE;
static BOOL wieg_neu_direct = FALSE;
static BOOL wieg_direct = FALSE;
static BOOL hand_direct = FALSE;
static BOOL paz_direct = FALSE;
static BOOL a_ausw_direct = FALSE;
static BOOL kun_ausw_direct = FALSE;

static BOOL komplett_default = 0;
static int break_lief_enter = 0;
static BOOL lief_me_mess = FALSE;
static BOOL ListColors = TRUE;
static BOOL ber_komplett = TRUE;
static COLORREF KompfColor = BLACKCOL;
static COLORREF KompbColor = GRAYCOL;
static COLORREF SafColor   = WHITECOL;
static COLORREF SabColor   = BLACKCOL;

static BOOL sort_a       = FALSE;

static HANDLE waaLibrary = NULL;
static char *fnbname = "fnb";
static char *fnbInstancename = "setzefnbInstance";

int (*fnbproc) (int, char **);
int (*fnbInstanceproc) (void *);


//  Prototypen

int      PASCAL WinMain(HANDLE,HANDLE,LPSTR,int);
LONG FAR PASCAL WndProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL GebProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL FitLogoProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL UhrProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EntNumProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL EntNuProc(HWND,UINT,WPARAM,LPARAM);
LONG FAR PASCAL PreisProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL ListKopfProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL PtFussProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL MsgProc(HWND,UINT, WPARAM,LPARAM);
LONG FAR PASCAL PosLeerProc(HWND,UINT, WPARAM,LPARAM);
int             ShowWKopf (HWND, char * , int, char *,
                 int, form *, form *, form *);
BOOL            (*ColorProc) (COLORREF *, COLORREF *, int) = NULL;
BOOL            ListRowColor (COLORREF *, COLORREF *, int);
void            fetch_std_geb (void);
void            update_kun_geb (void);
long            GetStdAuszeichner (void);
long            GetStdWaage (void);
int             AuswahlSysPeri (void);
int             AuswahlKopf (void);
void            IsDblClck (int);
void            GetAufArt9999 (void);
int             deltaraw (void);

int             ShowPtBox (HWND, char *, int, char *, int, form *,
                           form *, form *);
int IsWiegMessage (MSG *);
void InitAufKopf (void);
int dokunchoise (void);

static     void SelectAktCtr (void);
void            InitFirstInstance(HANDLE);
BOOL            InitNewInstance(HANDLE, int);
int             ProcessMessages(void);
LONG            MenuJob(HWND, WPARAM, LPARAM);
void            AuftragKopf (int);
void            RetKopf (void);
void            EnterNumBox (HWND, char *, char *, int, char *);
void            EnterCalcBox (HWND, char *, char *, int, char *);
void            EnterPreisBox (HWND, int, int);
void            EnterListe (void);
void            EnterLeerPos (void);
void            ArtikelWiegen (HWND);
void            ArtikelHand (void);
void            WaageInit (void);
static int      DbAuswahl (short,void (*) (char *), void (*) (char *));
void            SetStorno (int (*) (void), int);
int             ShowFestTara (char *);
int             SetKomplett (void);
void            DisplayRetKopf (void);
void            CloseRetKopf (void);
void            LiefFocus (void);
int             print_messG (int, char *, ...);
void            disp_messG (char *, int);
int             abfragejnG (HWND, char *, char *);
int             AuswahlWieg (void);
void            fnbexec (int, char *[]);
BOOL            TestPosStatus (void);
BOOL            LsLeerGut (void);
void            DeleteAuftrag (void);
void            ReadKopftext (void);
void            WriteKopftext (void);
void            FillLskAdr (void);
void            setautotara (BOOL);
int				ShowRetpKz (char *retp_kz);

BOOL            Lock_Lsp (void);
static int      la_zahl = 0;
static int      AktKopf = 0;

//  Globale Variablen

static char *FITICON = "FITICON";
HWND     hMainWindow;
HWND     hFitWindow = NULL;
HWND     BuDialog1;

HWND     BackWindow1;
HWND     BackWindow2;
HWND     BackWindow3;

HWND     LogoWindow;

HWND     FitWindow = NULL;
HWND     UhrWindow = NULL;
HWND     MessWindow = NULL;
HANDLE   hMainInst;
static   HWND eWindow = NULL;
static   HWND InsWindow = NULL;
static   HWND PtWindow = NULL;
static   HWND eKopfWindow = NULL;
static   HWND eFussWindow = NULL;
static   HWND PtFussWindow = NULL;
static   HWND GebWindow = NULL;
static   HWND GebWindButton = NULL;
static   HWND ChoiseWindow = NULL;

static   int nachkpreis = 2;
char     *preispicture = "%6.2f";
static int      enterpass = 0;

static int PtX = -1;
static int PtY = -1;

static int AuswahlEinhAnz = 7;
static double AuswahlEinhHeight = 0.75;


LPSTR    lpszMainTitle = "TOUCH Komissionierung ";
LPSTR    lpszClassName = "fit";
HBITMAP  fitbmp;
HBITMAP   pfeill = NULL;
HBITMAP   pfeilo = NULL;
HBITMAP   pfeilu = NULL;
HBITMAP   pfeilr = NULL;
HBITMAP   hbmOld;
HDC       hdc;
HDC       hdcMemory;
BITMAP    bm;
BITMAP    fitbm;
BITMAP    hfitbm;
HDC       fithdc;
int       procwait = 0;
int       IsFhs = 0;
BOOL      LockAuf = 1;
BOOL      AufLock = 0;
double scrfx, scrfy;
int Sebmpw;
int Sebmph;

/*
int lmx = 10;
int lmx2 = 0;
int lmy = 50;
int lmxs = 20;
int lmys = 0;
*/


BOOL break_all = FALSE;

WA_PREISE WaPreis;

static char waadir [128] = {"C:\\waakt"};
static char ergebnis [128] = {"C:\\waakt\\ergebnis"};
static int WaaInit = 0;

static DWORD GebColor = RGB (0, 0, 255);

/* Globale Parameter fuer printlogo                   */

static long twait = 50;
static long twaitU = 500;
static int plus =  3;
static int xplus = 3;
static int ystretch = 0;
static int xstretch = 0;
static int logory = 0;
static int logorx = 1;
static int sign = 1;
static int signx = 1;

static short akt_mdn = 1;
static short akt_fil = 0;
static int showmdnfil = 0;

static char fitpath [256];
static char rosipath [256];
static char progname [256];

struct MENUE scmenue;
struct MENUE leermenue;

static int ListeAktiv = 0;
static int PtListeAktiv = 0;
static int NumEnterAktiv = 0;
static int PreisAktiv = 0;
static int KopfEnterAktiv = 0;
static int WiegenAktiv = 0;
static int LiefEnterAktiv = 0;
static int KomplettAktiv = 0;
static int WorkModus = WIEGEN;
static HWND NumEnterWindow;
static HWND AufKopfWindow;
static HWND WiegWindow;


static int dsqlstatus;
static MDN_CLASS mdn_class;
static A_KUN_GX_CLASS AKunGx;
class RETK_CLASS Retk;
class RETP_CLASS Retp;
class LS_CLASS ls_class;
class LSPT_CLASS lspt_class;
class KUN_CLASS kun_class;
class FIL_CLASS fil_class;
class ADR_CLASS adr_class;
class PTAB_CLASS ptab_class;
class MENUE_CLASS menue_class;
class EINH_CLASS einh_class;
class HNDW_CLASS hndw_class;
class GEB_CLASS geb_class;
class SYS_PERI_CLASS sys_peri_class;
class PHT_WG_CLASS pht_wg_class;
class SYS_PAR_CLASS sys_par_class;
static DB_CLASS DbClass;
static AUTO_CLASS AutoClass;
static LS_TXT_CLASS ls_txt_class;
static PROG_CFG ProgCfg ("53600");
static BMAP bMap;
static int tara_kz_ausz;
static int tara_kz;
static int mhd_kz;
static int tour_kz = 0;
static int tourlang = 4;

// HMENU hMenu;
static char *mentab[] = {scmenue.menue1,
                         scmenue.menue2,
                         scmenue.menue3,
                         scmenue.menue4,
                         scmenue.menue5,
                         scmenue.menue6,
                         scmenue.menue7,
                         scmenue.menue8,
                         scmenue.menue9,
                         scmenue.menue0};
static char *zeitab [] = {scmenue.zei1,
                          scmenue.zei2,
                          scmenue.zei3,
                          scmenue.zei4,
                          scmenue.zei5,
                          scmenue.zei6,
                          scmenue.zei7,
                          scmenue.zei8,
                          scmenue.zei9,
                          scmenue.zei0};

static char *mentab0[] = {menue.menue1,
                          menue.menue2,
                          menue.menue3,
                          menue.menue4,
                          menue.menue5,
                          menue.menue6,
                          menue.menue7,
                          menue.menue8,
                          menue.menue9,
                          menue.menue0};

static char *zeitab0[] = {menue.zei1,
                          menue.zei2,
                          menue.zei3,
                          menue.zei4,
                          menue.zei5,
                          menue.zei6,
                          menue.zei7,
                          menue.zei8,
                          menue.zei9,
                          menue.zei0};


static HMENU hMenu;

/* Masken fuer Logofenster                                 */

mfont fittextlogo = {"Arial", 1000, 0, 3, RGB (255, 0, 0),
                                               RGB (0, 255, 255),
                                               1,
                                               NULL};

mfont fittextlogo2 = {"Arial", 200, 0, 3, RGB (0, 0, 255),
                                      RGB (0, 255, 255),
                                      1,
                                      NULL};

mfont buttonfont = {"", 90, 0, 1,
                                       RGB (0, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont uhrfont     = {"Arial", 80, 0, 1,
                                       BLACKCOL,
                                       LTGRAYCOL,
                                       1,
                                       NULL};
mfont MdnBuFont = {"Courier New", 120, 0, 1, BLACKCOL,
                                       GREENCOL,
                                       1,
                                       NULL};

field _fittext[] = {"     f i t", 0, 0, 50, -1, 0, "", DISPLAYONLY, 0, 0, 0};

form fittext = {1, 0, 0, _fittext, 0, 0, 0, 0, &fittextlogo};

field _fittext2[] = {"in Frische und Fleisch", 0, 0, 250, -1, 0, "",
                       DISPLAYONLY, 0, 0, 0};

form fittext2 = {1, 0, 0, _fittext2, 0, 0, 0, 0, &fittextlogo2};

static int menu_ebene = 0;
static int ohne_preis = 0;
static int immer_preis = 0;
static int ls_leer_zwang = 0;


struct FITMENU
{
           char menutext[80];
           char menuprog[256];
           int progtyp;
           int start_mode;
};

struct FITMENU fitmenu [11];

static BOOL ld_pr_prim = FALSE;

static char mdn_nr [5];
static char fil_nr [5];
static char auftrag_nr [22];
static char lief_nr [22];
static char kun_nr [22];
static char kun_krz1 [17];
static char lieferdatum  [22];
static char komm_dat [22];
static char lieferzeit   [22];
static char ret_stat [22];
static char ret_stat5 [22] = {"< 5"};
static char ret_stat_txt [27];

static int menfunc1 (void);
static int menfunc2 (void);
static int menfunc3 (void);
static int menfunc4 (void);
static int menfunc5 (void);
static int menfunc6 (void);
static int menfunc7 (void);
static int menfunc8 (void);
static int menfunc9 (void);
static int menfunc0 (void);

static int func1 (void);
static int func2 (void);
static int func3 (void);
static int func4 (void);
static int func5 (void);
static int func6 (void);
static int func7 (void);
static int func8 (void);

static int mdnfunc (void);
static int filfunc (void);

ColButton Ende = {"Ende", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   TRUE};


ColButton Auftrag  =   {"&Auftrag-Nr", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Lieferschein  =   {"&Lieferschein", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               RGB (0, 255, 255),
                               BLUECOL,
                               TRUE};

ColButton Retoure        =   {"&Retoure", -1, -1,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               NULL, 0, 0,
                               RGB (0, 255, 255),
                               BLUECOL,
                               TRUE};

ColButton Liste    = {
                        "L&iste", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         -1};

ColButton Suchen   =  {"&Suchen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                          TRUE};

ColButton Auswahl  =  {"Aus&wahl", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};


ColButton Geraet = {
                        "&Ger�t", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Tour = {
                        "&Tour", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton KDatum    =  {"&Komm-Datum", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Vorratsauftrag =  {
                        "Vor.Auftrag", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton BestInv =  {
                        "&Bestand, Inventur", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Auswertungen =  {
                        "Auswer&tungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Anwendungen =  {
                        "Anwen&dungen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Systemfunktionen =  {
                        "Systemf&unktionen", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Benutzer =  {
                        "Ben&utzer", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};



ColButton Hilfe =  {
                        "Hilfe &?", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};


ColButton Hauptmenue =  {
                        "Hauptmen&�", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton Back  =  {
                        "Zur�ck F5", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         TRUE};

ColButton BuOK = {"OK", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   BLUECOL,
                   TRUE};

ColButton BuKomplett =
                  {"komplett", -1, -1,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   NULL, 0, 0,
                   RGB (0, 255, 255),
                   REDCOL,
                   -1};


ColButton PfeilL       =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeill, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilO     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilo, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilU     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilu, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton PfeilR     =  {
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilr, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         10};

ColButton Cmdn     =  {
                         "Mandant", -1, 25,
                         mdn_nr,    -1, 50,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         2};
ColButton Cfil     =  {
                         "Filiale", -1, 25,
                         fil_nr,    -1, 50,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 255, 255),
                         BLUECOL,
                         2};


static int bsb  = 85;
static bsizemdn = 100;

static int bss0 = 73;
static int bss  = 77;
static int bsz  = 40;
static int bsz0 = 36;
static int AktButton = 0;

field _MainButton[] = {
(char *) &Retoure,         128, bsz0, 115, -1, 0, "", COLBUTTON, 0,
                                                     menfunc3, 0,
(char *) &Geraet,           128, bsz0, 115 + 1 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc6, 0,
(char *) &Liste,            128, bsz0, 115 + 4 * bsz, -1, 0, "", COLBUTTON, 0,
                                                     menfunc2, 0,
(char *) &Ende,             128, bsz0, 130 + 9 * bsz,  -1, 0, "", COLBUTTON, 0,
                                                          menfunc0, 0};

form MainButton = {4, 0, 0, _MainButton, 0, 0, 0, 0, &buttonfont};

field _MainButton2[] = {
(char *) &Hilfe,             bss0, bsz0, 5, 4+7*bss, 0, "", COLBUTTON, 0,
                                                              0, 0,
(char *) &Back,              bss0, bsz0, 5, 4+6*bss, 0, "", COLBUTTON, 0,
 *                                                              func5, 0,
(char *) &BuOK,              bss0, bsz0, 5, 4+5*bss, 0, "", COLBUTTON, 0,
 *                                                              func6, 0,
(char *) &BuKomplett,        bss0, bsz0, 5, 4+4*bss, 0, "", COLBUTTON, 0,
 *                                                              func8, 0,
(char *) &PfeilR,            bss0, bsz0, 5, 4+3*bss, 0, "", COLBUTTON, 0,
                                                          func4, 0,
(char *) &PfeilU,            bss0, bsz0, 5, 4+2*bss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,            bss0, bsz0, 5, 4+1*bss, 0, "", COLBUTTON, 0,
                                                          func2, 0,
(char *) &PfeilL,            bss0,  bsz0,5, 4,     0, "", COLBUTTON, 0,
                                                          func1, 0};

form MainButton2 = {8, 0, 0, _MainButton2, 0, 0, 0, 0, &buttonfont};

field _MdnFil[] = {
(char *) &Cmdn,              bsizemdn, bsizemdn, 0, 0, 0, "", COLBUTTON, 0,
                                                              mdnfunc, 0,
(char *) &Cfil,              bsizemdn, bsizemdn, 0, bsizemdn, 0, "", COLBUTTON, 0,
                                                              filfunc, 0,
};

form MdnFil = {2, 0, 0, _MdnFil, 0, 0, 0, 0, &MdnBuFont};

static char datum [12];
static char zeit [12];
static char datumzeit [24];

/*
static field _uhrform [] = {
datum,            0, 0,  5, -1, 0, "", DISPLAYONLY, 0, 0, 0,
zeit,             0, 0, 20, -1, 0, "", DISPLAYONLY, 0, 0, 0};
*/

static field _uhrform [] = {
datumzeit,        0, 0,  -1, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form uhrform = {1, 0, 0, _uhrform, 0, 0, 0, 0, &uhrfont};

int GetAktButton (void)
/**
Aktiven Button holen.
**/
{
        HWND hWnd;
        int i;

        hWnd = GetFocus ();

        for (i = 0; i < MainButton.fieldanz; i ++)
        {
                    if (hWnd == MainButton.mask[i].feldid)
                    {
                               return (i);
                    }
        }

        for (i = 0; i < MainButton2.fieldanz; i ++)
        {
                    if (hWnd == MainButton2.mask[i].feldid)
                    {
                            return (MainButton.fieldanz + i);
                    }
        }
        return (AktButton);
}

field *GetAktField ()
/**
Field zu AktButton holen.
**/
{
        if (AktButton < MainButton.fieldanz)
        {
                      return (&MainButton.mask [AktButton]);
        }
        return (&MainButton2.mask[AktButton - MainButton.fieldanz]);
}

int menfunc1 (void)
/**
Function fuer Button 1
**/
{
         HWND aktfocus;

         InitAufKopf ();
         AuftragKopf (ENTER);
         aktfocus = GetFocus ();
         SetColFocus (MainButton.mask[0].feldid);
         AktButton = GetAktButton ();
         return 0;
}

int menfunc2 (void)
/**
Function fuer Button 2
**/
{
         HWND aktfocus;
		 extern form AufKopfE;
		 extern form AufKopfED;
		 extern form RetKopfE;
		 extern form RetKopfED;

         if (AufLock) return 0;

         if (atol (auftrag_nr) > 0 && KopfEnterAktiv)
         {
                      EnterListe ();
    	        	  if (AufKopfWindow)
					  {
			                  sprintf (ret_stat, "%ld", retk.ret_stat);
                              display_form (AufKopfWindow, &AufKopfE, 0, 0);
                              display_form (AufKopfWindow, &AufKopfED, 0, 0);
					  }
         }
         else if (atol (lief_nr) > 0 && LiefEnterAktiv)
         {
                      EnterListe ();
       	              if (AufKopfWindow)
					  {
                             sprintf (ret_stat, "%ld", retk.ret_stat);
                             display_form (AufKopfWindow, &RetKopfE, 0, 0);
                             display_form (AufKopfWindow, &RetKopfED, 0, 0);
					  }
                      break_lief_enter = 1;
        }


         aktfocus = GetFocus ();
         AktButton = GetAktButton ();
         return 0;
}

int menfunc3 (void)
/**
Function fuer Button 3
**/
{
         HWND aktfocus;

         auf_art = 0;
		 while (TRUE)
		 {
                 RetKopf ();
				 if (syskey == KEY5) break;
		 }
         aktfocus = GetFocus ();
         SetColFocus (MainButton.mask[0].feldid);
         AktButton = GetAktButton ();
         return 0;
}

int menfunc4 (void)
/**
Function fuer Button 4
**/
{
         HWND aktfocus;

         EnterTyp = SUCHEN;
         AuftragKopf (SUCHEN);
         EnterTyp = ENTER;
         if (syskey == KEY5)
         {
                       InitAufKopf ();
         }
         else
         {
                      AuftragKopf (ENTER);
         }
         aktfocus = GetFocus ();
         AktButton = GetAktButton ();
         return 0;
}



int menfunc5 (void)
/**
Function fuer Button 5
**/
{
         HWND aktfocus;

         AuftragKopf (AUSWAHL);
         if (syskey == KEY5)
         {
                       InitAufKopf ();
         }
         else
         {
                      AuftragKopf (ENTER);
         }
         aktfocus = GetFocus ();
         AktButton = GetAktButton ();
         return 0;
}

int menfunc6 (void)
/**
Function fuer Button 6
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         AuswahlSysPeri ();
         AktButton = GetAktButton ();
         return 0;
}

int menfunc7 (void)
/**
Function fuer Button 7
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         EnterNumBox (LogoWindow,
                                "  Komm-Datum  ", KomDatum, 11,
                                "dd.mm.yyyy");
         AktButton = GetAktButton ();
         return 0;
}

int menfunc8 (void)
/**
Function fuer Button 8
**/
{
         HWND aktfocus;

         auf_art = 9999;
		 RetKopf ();
         aktfocus = GetFocus ();
         SetColFocus (MainButton.mask[0].feldid);
         AktButton = GetAktButton ();
         return 0;
}

int menfunc9 (void)
/**
Function fuer Button 9
**/
{
         HWND aktfocus;

         aktfocus = GetFocus ();
         EnterNumBox (LogoWindow,
                                "  Tour  ", AktTour, 5,
                                "%4d");
         AktButton = GetAktButton ();
         return 0;
}

void menue_end (void)
/**
Menue beenden.
**/
{
         PostQuitMessage (0);
}

int menfunc0 (void)
/**
Function fuer Button 10
**/
{
         HWND aktfocus;
         field *feld;

         feld = &MainButton2.mask[3];
         aktfocus = GetFocus ();
         menue_end ();
         return 0;
}

int func1 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 // PostMessage (NULL, WM_CHAR,  (WPARAM) ' ', 0l);
                 SelectAktCtr ();
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_LEFT, 0l);
                 return 1;
         }

         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_LEFT,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func2 (void)
/**
Button fuer Pfeil oben bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_UP, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_UP, 0l);
                 return 1;
         }
         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_UP,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func3 (void)
/**
Button fuer Pfeil unten bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_DOWN, 0l);
                 return 1;
         }
         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_DOWN,
                                             0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

int func4 (void)
/**
Button fuer Pfeil rechts bearbeiten.
**/
{
         field *feld;

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) ' ', 0l);
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 SendMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_RIGHT, 0l);
                 return 1;
         }

         feld = GetAktField ();
         SetFocus (feld->feldid);
         PostMessage (GetParent (feld->feldid), WM_KEYDOWN,
                                            (WPARAM) VK_RIGHT,
                                            0l);
         feld = GetAktField ();
         SetColFocus (feld->feldid);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}

void backmenue (char *men)
/**
Vorhergehendes Menue ermitteln.
**/
{
          char *pos;

          menue_end ();
          return;

          pos = men + strlen (men) - 1;

          for (; pos >= men; pos --)
          {
                    if (*pos != '0')
                     {
                            *pos = '0';
                            break;
                    }
          }
}


int func5 (void)
/**
Function fuer Zurueckbutton bearbeiten
**/
{
         if (PtListeAktiv)
         {
                 PostMessage (PtWindow, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (ListeAktiv)
         {
                 PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (KomplettAktiv)
         {
                 PostMessage (AktivDialog, WM_KEYDOWN,  (WPARAM) VK_F5, 0l);
                 return 1;
         }
         if (NumEnterAktiv)
         {
                 SetFocus (NumEnterWindow);
                 PostMessage (NumEnterWindow, WM_KEYDOWN,
                                     (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (KopfEnterAktiv)
         {
			     syskey = KEY5;
                 PostMessage (AufKopfWindow, WM_KEYDOWN,
                                     (WPARAM) VK_F5, 0l);
                 return 1;
         }

         if (LiefEnterAktiv)
         {
			     syskey = KEY5;
                 PostMessage (AufKopfWindow, WM_KEYDOWN,
                                     (WPARAM) VK_F5, 0l);
                 return 1;
         }

         backmenue (menue.prog);
         return 0;
}

int dokey5 (void)
/**
Aktion bei Taste F5 (zurueck)
**/
{
         field *feld;

         menue_end ();
         return 0;

         backmenue (menue.prog);
         feld = &MainButton.mask[0];
         SetFocus (feld->feldid);
         AktButton = GetAktButton ();
         display_form (BackWindow2, &MainButton, 0, 0);
         UpdateWindow (BackWindow2);
         display_form (BackWindow3, &MainButton2, 0, 0);
         AktButton = GetAktButton ();
         return 0;
}


int func6 (void)
/**
Function fuer OK setzen.
**/
{

         if (PtListeAktiv)
         {
                  PostMessage (PtWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
                 return 1;
         }


         if (KomplettAktiv)
         {
                 SelectAktCtr ();
                 return 1;
         }

         if (KopfEnterAktiv && ListeAktiv == 0)
         {
                 if (EnterTyp == SUCHEN)
                 {
                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
                 }
                 else
                 {
                        PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
                 }
                 return 1;
         }

         if (LiefEnterAktiv && ListeAktiv == 0)
         {
                 syskey = KEY12;
                 PostMessage (AufKopfWindow,
                               WM_KEYDOWN,  (WPARAM) VK_F12, 0l);
                 return 1;
         }

         if (ListeAktiv == 0)
         {
                 return 0;
         }

         PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
         return 0;
}

int func8 (void)
/**
Function fuer Benutzer bearbeiten
**/
{
         if (ListeAktiv == 0) return 0;
         if (KomplettAktiv) return 0;

         EnableWindow (eKopfWindow, FALSE);
         EnableWindow (eFussWindow, FALSE);
         SetKomplett ();
         EnableWindow (eKopfWindow, TRUE);
         EnableWindow (eFussWindow, TRUE);
         return 0;
}

int mdnfunc (void)
/**
Mandanten eingeben.
**/
{
         EnterNumBox (LogoWindow,
                                "  Mandant  ", mdn_nr, 5,
                                "%4d");
		 if (syskey != KEY5)
		 {
			 akt_mdn = atoi (mdn_nr);
		 }
		 sprintf (mdn_nr, "%hd", akt_mdn);
 	     SetFocus (MdnFil.mask[0].feldid);
		 return 0;
}

int filfunc (void)
/**
Mandanten eingeben.
**/
{
         EnterNumBox (LogoWindow,
                                "  Filiale  ", fil_nr, 5,
                                "%4d");
		 if (syskey != KEY5)
		 {
			 akt_fil = atoi (fil_nr);
		 }
		 sprintf (fil_nr, "%hd", akt_fil);
		 SetFocus (MdnFil.mask[1].feldid);
		 return 0;
}



class RCScanMessage
{
public:
	CSounds Sounds;
	BOOL Extern;
	Text ErrorName;
	Text OkName;
	Text ScanOkName;
	Text CompleteName;
	RCScanMessage ()
	{
		char *etc;
		Extern = FALSE;
		etc = getenv ("BWSETC");
		if (etc != NULL)
		{
			ErrorName.Format ("%s\\wav\\Error.wav", etc);
			ScanOkName.Format ("%s\\wav\\ScanOK.wav", etc);
			OkName.Format ("%s\\wav\\OK.wav", etc);
			CompleteName.Format ("%s\\wav\\Complete.wav", etc);
		}
		else
		{
			ErrorName = "";
			ScanOkName = "";
			OkName = "";
			CompleteName = "";
		}

	};

	virtual void Error ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (ErrorName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_ERROR);
		}
			
	}

	virtual void ScanOK ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (ScanOkName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_SCANOK);
		}
	}

	virtual void OK ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (OkName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_OK);
		}
	}

	virtual void Complete ()
	{
		if (Extern)
		{
			Sounds.SetSoundName (CompleteName);
			Sounds.Play ();
		}
		else
		{
			Sounds.Play (IDR_COMPLETE);
		}
	}
};

RCScanMessage ScanMessage;


class CScanValues 
{
public:
	double a;
	double eangew;
	int eananz;
	double gew;
	int anz;
	Text Charge;
	Text Mhd;
	double auf_me;
	Text ACharge;
	BOOL aflag;
	BOOL gewflag;
	BOOL anzflag;
	BOOL ChargeFlag;
	BOOL MhdFlag;
	CScanValues ()
	{
		a = 0.0;
		eangew = 0.0;
		gew = 0.0;
		anz  = 0;
        Charge = "";
		Mhd = "";
		auf_me = 0;
		ACharge = "";
		aflag = FALSE;
		gewflag = FALSE;
		ChargeFlag = FALSE;
		MhdFlag = FALSE;
		anzflag = FALSE;
	}

	void Init ()
	{
		eangew = 0.0;
		a = 0.0;
		anz = 0;
        Charge = "";
		Mhd = "";
		aflag = FALSE;
		gewflag = FALSE;
		ChargeFlag = FALSE;
		MhdFlag = FALSE;
		anzflag = FALSE;
	}

	void SetA (double a)
	{
		this->a = a;
		aflag = TRUE;
	}

	void SetGew (double gew)
	{
		this->eangew = gew;
		this->gew += gew;
		gewflag = TRUE;
	}
	void SetAnz (int anz)
	{
		this->eananz = anz;
		this->anz += anz;
		anzflag = TRUE;
	}

	void SetCharge (Text& Charge)
	{
		this->Charge = Charge;
		ChargeFlag = TRUE;
	}

	void SetMhd (Text& Mhd)
	{
		this->Mhd = Mhd;
		MhdFlag = TRUE;
	}

	void IncAufMe ()
	{
		auf_me ++;
	}

	BOOL SetACharge ()
	{
		if (ACharge != "")
		{
			if (ACharge != Charge) return FALSE;
// Sp�ter eventuell eine neue Position erzeugen
			ACharge = Charge;
			return TRUE;
		}
        ACharge = Charge;
		return TRUE;
	}

	BOOL ScanOK ()
	{
		if (!aflag) return FALSE;
		if (!gewflag) return FALSE;
		if (!ChargeFlag) return FALSE;
		return TRUE;
	}

	BOOL ScanValuesOK ()
	{
		if (a == 0.0) return FALSE;
		if (gew == 0.0) return FALSE;
		if (Charge == "") return FALSE;
		return TRUE;
	}
};

CScanValues *ScanValues = NULL;



static BOOL SmallScann = TRUE;
static int ScannMeZwang = 0;
static Text	ScannedCharge;
static long scangew = 0l;
CEan128Date Ean128Date;
static BOOL Scannen = FALSE;
static int Scanlen = 14;
static BOOL ScanMode = FALSE;
static BOOL BreakScan = FALSE;
static BOOL ScanAFromEan = TRUE;
BOOL DelAufTara = TRUE;
BOOL EnterGewAnz = FALSE;
BOOL ChargeZwang = FALSE;
static int ChargeLen = 0;
static int MinChargeLen = 0;
static int MaxChargeLen = 0;
BOOL ScanCharge = FALSE;
BOOL TestHbkDate = FALSE;
double LiefMeDiff = 0.0;
short IdentZwang = FALSE;
BOOL ScannIdent = FALSE;
int IdentMinChars = 20;
BOOL SinglePrice = FALSE;
int ChargeFromEan128 = 0;
int Ean128StartLen = 10;
static int Ean128Eanlen = 12;


/*
void printlogo (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
        HDC      hdc;

        GetObject (hbr, sizeof (BITMAP), &bm);
        bmy = bm.bmHeight / 2;
        bmx = bm.bmWidth / 2;
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bm.bmWidth - xstretch, bm.bmHeight - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bm.bmHeight * 2)
            {
                        ystretch = bm.bmHeight * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bm.bmWidth * 2)
            {
                        xstretch = bm.bmWidth * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}
*/

void paintlogo (HDC hdc, HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;

        GetObject (hbr, sizeof (BITMAP), &bm);
        bmy = bm.bmHeight / 2;
        bmx = bm.bmWidth / 2;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bm.bmWidth - xstretch, bm.bmHeight - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bm.bmHeight * 2)
            {
                        ystretch = bm.bmHeight * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bm.bmWidth * 2)
            {
                        xstretch = bm.bmWidth * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}

void strechbmp (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
		int bmyst;
        int bmxst;
        HDC      hdc;

        xstretch = ystretch = 0;
        GetObject (hbr, sizeof (BITMAP), &bm);
		bmxst = (int) (double) ((double) bm.bmWidth * scrfx);
		bmyst = (int) (double) ((double) bm.bmHeight * scrfy);

        bmy = bmyst / 2;
        bmx = bmxst / 2;
//        bmy = bm.bmHeight / 2;
//        bmx = bm.bmWidth / 2;
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bmxst - xstretch, bmyst - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
}


void printlogo (HBITMAP hbr, int x, int y, DWORD mode)
{

        int bmy;
        int bmx;
		int bmyst;
        int bmxst;
        HDC      hdc;

//        GetObject (hbr, sizeof (BITMAP), &bm);
		bmxst = (int) (double) ((double) bm.bmWidth * scrfx);
		bmyst = (int) (double) ((double) bm.bmHeight * scrfy);

        bmy = bmyst / 2;
        bmx = bmxst / 2;
//        bmy = bm.bmHeight * scrfy / 2);
//        bmx = bm.bmWidth / 2;
//        hdc = fithdc;

/*
		xstretch = (int) (double) ((double) xstretch * scrfx);
		ystretch = (int) (double) ((double) ystretch * scrfy);
*/

        hdc = GetDC (FitWindow);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, -bmx + xstretch / 2, -bmy + ystretch / 2,
                         bmxst - xstretch, bmyst - ystretch,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

//        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        ReleaseDC (FitWindow, hdc);
        if (sign == 1)
        {
            ystretch += (plus * logory);
            if (ystretch >= bmyst * 2)
            {
                        ystretch = bmyst * 2;
                        sign = -1;
            }
        }
        else if (sign == -1)
        {
            ystretch -= (plus * logory);
            if (ystretch <= 0)
            {
                        ystretch = 0;
                        sign = 1;
            }
        }
        if (signx == 1)
        {
            xstretch += (xplus * logorx);
            if (xstretch >= bmxst * 2)
            {
                        xstretch = bmxst * 2;
                        signx = -1;
            }
        }
        else if (signx == -1)
        {
            xstretch -= (xplus * logorx);
            if (xstretch <= 0)
            {
                        xstretch = 0;
                        signx = 1;
            }
        }
}

void LogoBitmap (HDC hdc)
/**
Bitmap in Lo�gofenster schreiben.
**/
{
	    RECT rect;

		GetClientRect (LogoWindow, &rect);

		bMap.StrechBitmap (hdc, rect.right, rect.bottom);
}


/*
void strechbmp (HBITMAP hbr, int x, int y, DWORD mode)
{

        HDC      hdc;

        GetObject (hbr, sizeof (BITMAP), &bm);
        hdc = fithdc;
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        // SetViewportOrgEx (hdc, bmx, bmy, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
        StretchBlt (hdc, 0, 0,
                         bm.bmWidth, bm.bmHeight,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, mode);

        SelectObject (hdcMemory, hbmOld);
        DeleteDC (hdcMemory);
}
*/

void printbmp (HBITMAP hbr, int x, int y, DWORD mode)
{
        RECT rect;

        GetClientRect (hMainWindow,  &rect);
        GetObject (hbr, sizeof (BITMAP), &bm);
        x = (rect.right - bm.bmWidth) / 2;
        y = (rect.bottom - bm.bmHeight) / 2;
        hdc = GetDC (hFitWindow);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = SelectObject (hdcMemory, hbr);

        BitBlt (hdc, x, y, bm.bmWidth, bm.bmHeight,
                           hdcMemory,0, 0, mode);

        SelectObject (hdcMemory, hbmOld);

        DeleteDC (hdcMemory);
        ReleaseDC (hFitWindow, hdc);
}

int ScrollMenue ()
/**
Leere MenueZeilen entfernrn.
**/
{
        int i, j;

        memcpy (&scmenue, &menue, sizeof (MENUE));
        memcpy (&menue,   &leermenue, sizeof (MENUE));
        menue.mdn = scmenue.mdn;
        menue.fil = scmenue.fil;
        strcpy (menue.pers, scmenue.pers);
        strcpy (menue.prog, scmenue.prog);
        menue.anz_menue = scmenue.anz_menue;

        for (i = 0,j = 0; j < 10; j ++)
        {
                clipped (mentab[j]);
                if (strcmp (mentab[j], " ") > 0)
                {
                                    strcpy (mentab0[i], mentab[j]);
                                    strcpy (zeitab0[i], zeitab[j]);
                                    i ++;
                }
        }
        return (i);
}

void GetAutotara (void)
/**
Default fuer Atou-Tara holen und setzen.
**/
{
	    char tara_auto [2];

        strcpy (tara_auto, "N");
        DbClass.sqlin   ((long *) &StdWaage, 2, 0);
        DbClass.sqlout  ((char *) tara_auto, 0, 2);
        DbClass.sqlcomm ("select tara_auto from opt_mci where sys = ?");
		if (tara_auto [0] == 'J')
		{
			setautotara (TRUE);
		}
		else
		{
			setautotara (FALSE);
		}
}

void GetListColor (COLORREF *color, char *cfg_v)
/**
Farbunterlegung fuer Listen uebertragen.
**/
{
	static char *ColTxt[] = {"BLACKCOL",
		                     "WHITECOL",
							 "BLUECOL",
							 "REDCOL",
							 "LTGRAYCOL",
							 "GREENCOL",
							 "YELLOWCOL",
							 "DKYELLOWCOL",
							 NULL};

	static COLORREF ColVal[] = {BLACKCOL,
		                        WHITECOL,
					  		    BLUECOL,
							    REDCOL,
							    LTGRAYCOL,
							    GREENCOL,
							    YELLOWCOL,
								DKYELLOWCOL};
	int i;
	int red, blue, green;
	char *ColR;
	int Cols = 0;

	for (i = 0; ColTxt[i]; i ++)
	{
		if (strcmp (ColTxt[i], cfg_v) == 0)
		{
			*color = ColVal[i];
			return;
		}
	}

	ColR = strstr (cfg_v, "red");
	if (ColR)
	{
		red = atoi (&ColR[3]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "blue");
	if (ColR)
	{
		blue = atoi (&ColR[4]);
		Cols ++;
	}
	ColR = strstr (cfg_v, "green");
	if (ColR)
	{
		green = atoi (&ColR[5]);
		Cols ++;
	}
	if (Cols == 3)
	{
		*color = RGB (red, green, blue);
	}
}


/***
BOOL TestNewSmt ()
{
	if (KommSmt == 0) return TRUE;

    SMTG_CLASS Smtg;
    _a_bas.teil_smt = (short) Smtg.Get (aufk.mdn, aufk.kun, (long) _a_bas.teil_smt);
	if (_a_bas.teil_smt != KommSmt)
	{
		disp_messG ("Der Artikel hat dat falsche Teilsortiment", 2);
		return FALSE;
	}
	return TRUE;
}
**/




//  Hauptprogramm

int      PASCAL WinMain(HANDLE hInstance,HANDLE hPrevInstance,
                        LPSTR lpszCmdLine,int nCmdShow)
{
        HCURSOR oldcursor;
        char cfg_v [256];


        GraphikMode = IsGdiPrint ();

        if (ProgCfg.GetCfgValue ("AlphaCharge", cfg_v) == TRUE)
        {
                     AlphaCharge
						 = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("a_sort", cfg_v) ==TRUE)
        {
                     a_sort = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("ScanTuerke", cfg_v) ==TRUE)
        {
                    ScanTuerke = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("Scannen", cfg_v) == TRUE)
		{
			         Scannen = atoi (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("WithZerlData", cfg_v) ==TRUE)
        {
                     WithZerlData = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("ChargeFromEan128", cfg_v) == TRUE)
        {
                     ChargeFromEan128 = atol (cfg_v);

        }
        if (ProgCfg.GetCfgValue ("Ean128StartLen", cfg_v) == TRUE)
        {
                     Ean128StartLen = atol (cfg_v);

        }

        if (ProgCfg.GetCfgValue ("AuswahlEinhAnz", cfg_v) == TRUE)
		{
			         AuswahlEinhAnz = atol (cfg_v);
		}
        if (ProgCfg.GetCfgValue ("AuswahlEinhHeight", cfg_v) == TRUE)
		{
			         AuswahlEinhHeight = ratod (cfg_v);
		}


        if (ProgCfg.GetCfgValue ("a_auswahl_direct", cfg_v) ==TRUE)
        {
                     a_ausw_direct = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("kun_auswahl_direct", cfg_v) ==TRUE)
        {
                     kun_ausw_direct = atoi (cfg_v);
        }

        if (ProgCfg.GetCfgValue ("sort_a", cfg_v) == TRUE)
        {
		             sort_a =  min (1, max (0, atoi (cfg_v)));
        }

        if (ProgCfg.GetCfgValue ("tara", cfg_v) ==TRUE)  //nur bei Auszeichnen
        {
                    tara_kz_ausz = atoi (cfg_v);
        }
        else
        {
                    tara_kz_ausz = 0;
        }
        if (ProgCfg.GetCfgValue ("TaraHolen", cfg_v) ==TRUE)  //nicht bei Auszeichnen
        {
                    tara_kz = atoi (cfg_v);
        }
        else
        {
                    tara_kz = 0;
        }
        if (ProgCfg.GetCfgValue ("mhd", cfg_v) ==TRUE)
        {
                    mhd_kz = atoi (cfg_v);
        }
        else
        {
                    mhd_kz = 0;
		}
        if (ProgCfg.GetCfgValue ("tour", cfg_v) ==TRUE)
        {
                    tour_kz = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("mdn_default", cfg_v) ==TRUE)
        {
                    akt_mdn = atoi (cfg_v);

        }
        if (ProgCfg.GetCfgValue ("fil_default", cfg_v) ==TRUE)
        {
                    akt_fil = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("showmdnfil", cfg_v) ==TRUE)
        {
                    showmdnfil = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("colborder", cfg_v) ==TRUE)
        {
                     colborder = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("auf_me_kz", cfg_v) ==TRUE)
        {
                     auf_me_kz = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("wieg_neu_direct", cfg_v) ==TRUE)
        {
                     wieg_neu_direct = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("wieg_direct", cfg_v) ==TRUE)
        {
                     wieg_direct = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("hand_direct", cfg_v) ==TRUE)
        {
                     hand_direct = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("paz_direct", cfg_v) ==TRUE)
        {
                     paz_direct = atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("komplett_default", cfg_v) == TRUE)
        {
		             komplett_default =  atoi (cfg_v);
        }
        if (ProgCfg.GetCfgValue ("lief_me_mess", cfg_v) == TRUE)
        {
		             lief_me_mess =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("ber_komplett", cfg_v) == TRUE)
        {
		             ber_komplett =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("LISTCOLORS", cfg_v) == TRUE)
        {
		             ListColors =  min (1, max (0, atoi (cfg_v)));
        }
        if (ProgCfg.GetCfgValue ("KOMPFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompfColor, cfg_v);
        }
        if (ProgCfg.GetCfgValue ("KOMPBCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&KompbColor, cfg_v);
        }
        if (ProgCfg.GetCfgValue ("SAFCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SafColor, cfg_v);
        }
        if (ProgCfg.GetCfgValue ("SABCOLOR", cfg_v) == TRUE)
        {
		             GetListColor (&SabColor, cfg_v);
        }
        if (ProgCfg.GetCfgValue ("PassWord", cfg_v) == TRUE)
		{
			         PassWord = atoi (cfg_v);
					 SetPassWordFlag (PassWord);
		}

        Scanean.Read ();
		sprintf (mdn_nr, "%hd", akt_mdn);
		sprintf (fil_nr, "%hd", akt_fil);
        if (showmdnfil == 0)
		{
			MdnFil.fieldanz = 0;
		}
		else if (showmdnfil == 1)
		{
            ActivateColButton (NULL, &MdnFil, 0, -1, 0);
            ActivateColButton (NULL, &MdnFil, 1, -1, 0);
		}
        if (getenv ("testmode"))
        {
                     testmode = atoi (getenv ("testmode"));
        }
		if (tour_kz)
		{
			MainButton.mask[8].attribut = COLBUTTON;
		}
        opendbase ("bws");
        sysdate (KomDatum);
        menue_class.SetSysBen ();
        if (sys_ben.berecht != 0)
        {
                   ohne_preis = 1;
        }

        if (getenv ("WAAKT"))
        {
                    strcpy (waadir, getenv ("WAAKT"));
                    sprintf (ergebnis, "%s\\ergebnis", waadir);
        }

        menue_class.Lesesys_inst ();


        auszeichner = GetStdAuszeichner ();
        sys_peri_class.lese_sys (auszeichner);

        StdWaage = GetStdWaage ();
        sys_peri_class.lese_sys (StdWaage);
        la_zahl = sys_peri.la_zahl;

       strcpy (sys_par.sys_par_nam,"nachkpreis");
       if (sys_par_class.dbreadfirst () == 0)
       {
            nachkpreis = atoi (sys_par.sys_par_wrt);
            preispicture = new char [6];
            preispicture = "%6.2f";
            if (nachkpreis == 0)
            {
                preispicture = "%4.0f";
            }
            else if (nachkpreis == 1)
            {
                preispicture = "%6.1f";
            }
            else if (nachkpreis == 3)
            {
                preispicture = "%7.3f";
            }

            else if (nachkpreis == 4)
            {
                preispicture = "%8.4f";
            }

       }

        strcpy (sys_par.sys_par_nam, "a_kum_par");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    a_kum_par = atoi (sys_par.sys_par_wrt);
        }
		if (a_kum_par == 0 && a_sort)
		{
			wieg_neu_direct = FALSE;
		}

        strcpy (sys_par.sys_par_nam, "immer_preis");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    immer_preis = atoi (sys_par.sys_par_wrt);
        }

        strcpy (sys_par.sys_par_nam, "ls_leer_zwang");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    ls_leer_zwang = atoi (sys_par.sys_par_wrt);
        }

        strcpy (sys_par.sys_par_nam, "ls_charge_par");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    ls_charge_par = atoi (sys_par.sys_par_wrt);
        }
        strcpy (sys_par.sys_par_nam, "lsc2_par");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    lsc2_par = atoi (sys_par.sys_par_wrt);
        }
        strcpy (sys_par.sys_par_nam, "lsc3_par");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    lsc3_par = atoi (sys_par.sys_par_wrt);
        }
        strcpy (sys_par.sys_par_nam, "tourlang");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    tourlang = atoi (sys_par.sys_par_wrt);
        }

		GetAufArt9999 ();

		ColBorder = colborder;
        if (strcmp (clipped (sys_inst.projekt), "F H S"))
        {
                    fitbmp   = LoadBitmap (hInstance, "fit1");
        }
        else
        {
                    fitbmp   = LoadBitmap (hInstance, "fhs");
					FITICON = "FHSICON";
                    IsFhs = 1;
        }
        PfeilL.bmp = LoadBitmap (hInstance, "pfeill");
        PfeilR.bmp = LoadBitmap (hInstance, "pfeilr");
        PfeilU.bmp = LoadBitmap (hInstance, "pfeilu");
        PfeilO.bmp = LoadBitmap (hInstance, "pfeilo");
        GetObject (fitbmp, sizeof (BITMAP), &bm);

        hMainInst = hInstance;
        if (getenv (BWS))
        {
                   strcpy (fitpath, getenv (BWS));
        }
        else
        {
                   strcpy (fitpath, "\\USER\\BWS8000");
        }

		GetAutotara ();

        strcpy (rosipath, getenv (RSDIR));

        GetObject (fitbmp, sizeof (BITMAP), &fitbm);

        InitFirstInstance (hInstance);
        if (InitNewInstance (hInstance, nCmdShow) == 0) return 0;

        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        WaageInit ();
        SetCursor (oldcursor);
        SetFocus (MainButton.mask[0].feldid);
        return ProcessMessages ();
}

void InitFirstInstance(HANDLE hInstance)
{
        WNDCLASS wc;

		HDC hdc;
		int col;

		col = 0;
		hdc = GetDC (NULL);
		col = GetDeviceCaps (hdc, BITSPIXEL);
		ReleaseDC (NULL, hdc);
		if (col < 16) ColBorder = FALSE;

        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  WndProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hInstance;
        wc.hIcon         =  LoadIcon (hInstance, FITICON);
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  lpszClassName;

        RegisterClass(&wc);

        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_OWNDC;
        wc.lpfnWndProc   =  FitLogoProc;
        wc.lpszMenuName  =  "";
        wc.hbrBackground =  GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "Fitlogo";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  UhrProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "Uhr";
        RegisterClass(&wc);


        wc.lpfnWndProc   =  WndProc;
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "Backwind";

        RegisterClass(&wc);

        wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
        wc.lpszClassName =  "LogoWind";
        RegisterClass(&wc);

        wc.lpfnWndProc   =  GebProc;
        wc.hbrBackground =  CreateSolidBrush (GebColor);
        wc.lpszClassName =  "GebWind";
        RegisterClass(&wc);

        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszClassName =  "GebWindB";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   MsgProc;
        wc.hbrBackground =   GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "AufWiegWhite";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   MsgProc;
        wc.hbrBackground =   CreateSolidBrush (RGB (0, 0, 255));
        wc.lpszClassName =  "AufWiegBlue";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   PosLeerProc;
        wc.hbrBackground =   CreateSolidBrush (RGB (0, 255, 255));
        wc.lpszClassName =  "PosLeer";
        RegisterClass(&wc);

        wc.lpfnWndProc   =   PosLeerProc;
        wc.hbrBackground =   GetStockObject (WHITE_BRUSH);
        wc.lpszClassName =  "PosLeerKopf";
        RegisterClass(&wc);
}

void hKorrMainButton (double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    fittextlogo.FontHeight = (short) (double)
			           ((double) fittextlogo.FontHeight * fcy);
	    fittextlogo2.FontHeight = (short) (double)
			           ((double) fittextlogo2.FontHeight * fcy);
//	    fittextlogo3.FontHeight = (short) (double)
//			           ((double) fittextlogo3.FontHeight * fcy);
	    if (fittext.mask[0].pos[0] != -1)
		{
			     fittext.mask[0].pos[0] = (short) (double)
					 ((double) fittext.mask[0].pos[0] * fcy);
		}
		if (fittext.mask[0].pos[1] != -1)
		{
			     fittext.mask[0].pos[1] = (short) (double)
					 ((double) fittext.mask[0].pos[1] * fcx);

		}

	    if (fittext2.mask[0].pos[0] != -1)
		{
			     fittext2.mask[0].pos[0] = (short) (double)
					 ((double) fittext2.mask[0].pos[0] * fcy);
		}
		if (fittext2.mask[0].pos[1] != -1)
		{
			     fittext2.mask[0].pos[1] = (short) (double)
					 ((double) fittext2.mask[0].pos[1] * fcx);

		}

/*
	    if (fittext3.mask[0].pos[0] != -1)
		{
			     fittext3.mask[0].pos[0] = (short) (double)
					 ((double) fittext3.mask[0].pos[0] * fcy);
		}
		if (fittext3.mask[0].pos[1] != -1)
		{
			     fittext3.mask[0].pos[1] = (short) (double)
					 ((double) fittext3.mask[0].pos[1] * fcx);

		}
*/
}


void KorrMainButton (form * MainButton, double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    int i;

		for (i = 0; i < MainButton->fieldanz; i ++)
		{
			if (MainButton->mask[i].pos[0] != -1)
			{
			     MainButton->mask[i].pos[0] = (short) (double)
					 ((double) MainButton->mask[i].pos[0] * fcy);
			}
			if (MainButton->mask[i].pos[1] != -1)
			{
			     MainButton->mask[i].pos[1] = (short) (double)
					 ((double) MainButton->mask[i].pos[1] * fcx);

			}
	        MainButton->mask[i].rows = (short) (double)
			 ((double) MainButton->mask[i].rows * fcy);
	        MainButton->mask[i].length = (short) (double)
			 ((double) MainButton->mask[i].length * fcx);
		}
}

void KorrMainButton2 (form * MainButton, double fcx, double fcy)
/**
Position von Mainbutton korrigieren.
**/
{
	    int i;

		for (i = 0; i < MainButton->fieldanz; i ++)
		{
			if (MainButton->mask[i].pos[0] != -1)
			{
			     MainButton->mask[i].pos[0] = (short) (double)
					 ((double) MainButton->mask[i].pos[0] * fcy);
			}
			if (MainButton->mask[i].pos[1] != -1)
			{
			     MainButton->mask[i].pos[1] = (short) (double)
					 ((double) MainButton->mask[i].pos[1] * fcx);

			}
	        MainButton->mask[i].rows = (short) (double)
			 ((double) MainButton->mask[i].rows * fcy);
	        MainButton->mask[i].length = (short) (double)
			 ((double) MainButton->mask[i].length * fcx);
		}
}


void SetMdnFilPos (void)
/**
Position f�er Form MdnFil setzen.
**/
{
	     RECT rect;
		 static BOOL SetOK = FALSE;

		 if (SetOK) return;


		 GetClientRect (LogoWindow, &rect);

		 MdnFil.mask[0].pos[0] = rect.bottom - bsizemdn;
		 MdnFil.mask[1].pos[0] = rect.bottom - bsizemdn;
		 SetOK = TRUE;
}


BOOL InitNewInstance(HANDLE hInstance, int nCmdShow)
{
        RECT rect;
        RECT rect1;
        HCURSOR oldcursor;
		TEXTMETRIC tm;
		HDC hdc;
		double fcx, fcy;
        extern mfont ListFont;
		extern form lFussform;
		extern form EtiButton;
        extern form fScanform;
        extern mfont lScanFont;

        hMainWindow = CreateWindow (lpszClassName,
                                    lpszMainTitle,
                                    WS_DLGFRAME | WS_CAPTION | WS_SYSMENU |
                                    WS_MINIMIZEBOX,
                                    0, 0,
                                    800, 580,
                                    NULL,
                                    NULL,
                                    hInstance,
                                    NULL);
        if (hMainWindow == 0) return FALSE;

        ShowWindow(hMainWindow, SW_SHOWMAXIMIZED);
        hdc = GetDC (hMainWindow);
		GetTextMetrics (hdc, &tm);
		ReleaseDC (hMainWindow, hdc);

		GetWindowRect (hMainWindow, &rect);
		rect.bottom -= (tm.tmHeight + tm.tmHeight / 2);
        ShowWindow(hMainWindow, nCmdShow);
        MoveWindow (hMainWindow, rect.left,
                                rect.top, rect.right, rect.bottom, TRUE);


        fcx = fcy = 1;
        if (rect.right > 900 || rect.right < 700)
        {
		            fcx = (double) rect.right / 800;
 		            fcy = (double) rect.bottom / 562;
                    hKorrMainButton (fcx, fcy);
		            KorrMainButton (&MainButton, fcx, fcy);
		            KorrMainButton2 (&MainButton2, fcx, fcy);
		            KorrMainButton2 (&lFussform, fcx, fcy);
		            KorrMainButton2 (&fScanform, fcx, fcy);
					ListFont.FontHeight = (int) (double)
						((double)  ListFont.FontHeight * fcy);
					ListFont.FontWidth = (int) (double)
						((double)  ListFont.FontWidth * fcx);
					EtiButton.mask[0].pos[1] = (int)
						(double) ((double) EtiButton.mask[0].pos[1] *
						         fcx);
					EtiButton.mask[1].pos[1] = (int)
						(double) ((double) EtiButton.mask[1].pos[1] *
						         fcx);
					lScanFont.FontHeight = 320;
        }

		scrfx = fcx;
		scrfy = fcy;
        hFitWindow = hMainWindow;


//        ShowWindow(hMainWindow, nCmdShow);
//        UpdateWindow(hMainWindow);

        hFitWindow = hMainWindow;

        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        strcpy (menue.prog, "00000");
        strcpy (menue.prog, "00000");
        SetCursor (oldcursor);

        GetClientRect (hMainWindow,  &rect);

        Sebmpw = (int) (double) ((double) (fitbm.bmWidth + 40) *
                                           scrfx);
        BackWindow1  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              10, 10,
/*
                              rect.right - (fitbm.bmWidth + 40),
                              rect.bottom - 100,
*/
                              rect.right - Sebmpw,
                              rect.bottom - 100,
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);

        enterpass = 1;
		if (PassWord)
		{

            if (! EnterPasswTouch (hInstance, BackWindow1))
			{
                      enterpass = 0;
                      return FALSE;
			}
		}

		SetActiveWindow (hMainWindow);
		EnableWindow (hMainWindow, TRUE);
        ShowWindow(BackWindow1, SW_SHOWNORMAL);
        UpdateWindow(BackWindow1);
        GetClientRect (BackWindow1,  &rect1);
        Sebmpw = (int) (double) ((double) (fitbm.bmWidth + 25) *
                                           scrfx);

        BackWindow2  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
//                              rect.right - (fitbm.bmWidth + 25),
                              rect.right - Sebmpw,
                              10,
//                              fitbm.bmWidth + 15,
                              (int) (double) ((double) (fitbm.bmWidth + 15) * scrfx),
                              rect.bottom - 20,
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);

        BackWindow3  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "Backwind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              10,
                              rect.bottom - (int) ((double) 60 * scrfy),
//                              rect.bottom - 60,
//                              rect.right - (fitbm.bmWidth + 40),
                              rect.right - (int) (double) ((double) (fitbm.bmWidth + 40) *
                                           scrfx),
//                              rect.bottom - 20 - (rect.bottom - 70),
                              (int) ((double) (rect.bottom - 20 - (rect.bottom - 70)) * scrfy),
                              hMainWindow,
                              NULL,
                              hInstance,
                              NULL);


        LogoWindow  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "LogoWind",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              5, 5,
                              rect1.right - 10,
                              rect1.bottom - 10,
                              BackWindow1,
                              NULL,
                              hInstance,
                              NULL);
        UpdateWindow (BackWindow1);
        UpdateWindow (BackWindow2);
        UpdateWindow (BackWindow3);
        UpdateWindow (LogoWindow);
        return TRUE;
}

int CreateFitWindow (void)
/**
FitWindow erzeugen.
**/
{

        FitWindow = CreateWindow   ("Fitlogo",
                                    "",
                                    WS_CHILD | WS_VISIBLE | WS_DLGFRAME,
                                    5, 5,
//                                    fitbm.bmWidth, fitbm.bmHeight,
                                    (int) (double) ((double) fitbm.bmWidth * scrfx),
									(int) (double) ((double) fitbm.bmHeight * scrfy),
                                    BackWindow2,
                                    NULL,
                                    hMainInst,
                                    NULL);
        fithdc = GetDC (FitWindow);
        if (IsFhs == 0)
        {
                    SetTimer (FitWindow , 1, twait, 0);
        }
        return TRUE;
}

int CreateUhrWindow (void)
/**
UhrWindow erzeugen.
**/
{
        RECT rect;
        int x,y, cx, cy;

        GetClientRect (LogoWindow,  &rect);
        cx = fitbm.bmWidth;
        cy = 13;
        x = 5;
        y = fitbm.bmHeight + 5;

        UhrWindow = CreateWindowEx (
                                    0,
                                    "Uhr",
                                    "",
                                    WS_CHILD | WS_VISIBLE,
                                    x, y,
                                    cx, cy,
                                    BackWindow2,
                                    NULL,
                                    hMainInst,
                                    NULL);
        SetTimer (UhrWindow , 1, twaitU, 0);
        return TRUE;
}


int IsHotKey (MSG *msg)
/**
HotKey Testen.
**/
{
         UCHAR taste;
         int i;
         ColButton *ColBut;
         char *pos;

         if (msg->message != WM_CHAR)
         {
                      return FALSE;
         }

         taste = (char) msg->wParam;
         for (i = 0; i < MainButton.fieldanz; i ++)
         {
                      ColBut = (ColButton *) MainButton.mask[i].feld;
                      if (ColBut->text1)
                      {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                              ((UCHAR) toupper (*(pos + 1)) == toupper (taste)))
                           {
                                   SetFocus (MainButton.mask[i].feldid);
                                   AktButton = GetAktButton ();
                                   SendMessage (AktivWindow,WM_KEYDOWN,
                                                VK_RETURN, 0);
                                   return TRUE;
                           }
                      }
         }

         for (i = 0; i < MainButton2.fieldanz; i ++)
         {
                      ColBut = (ColButton *) MainButton2.mask[i].feld;
                      if (ColBut->text1)
                      {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                              ((UCHAR) toupper (*(pos + 1)) == toupper (taste)))
                           {
                                   SetFocus (MainButton2.mask[i].feldid);
                                   AktButton = GetAktButton ();
                                   return TRUE;
                           }
                      }
         }
         return FALSE;
}

BOOL IsInaktivButton (field *feld)
/**
**/
{
	     ColButton *ColBut;

		 if (feld->attribut & REMOVED)
		 {
			 return TRUE;
		 }
		 if (feld->attribut & COLBUTTON == 0)
		 {
			 return FALSE;
		 }
		 ColBut = (ColButton *) feld->feld;
		 if (ColBut->aktivate == -1)
		 {
			 return TRUE;
		 }
		 return FALSE;
}

int TestMdnFil (void)
/**
Test, ob der Focus auf der Form MdnFil steht.
**/
{
	     HWND hWnd;
		 int i;

		 hWnd = GetFocus ();

		 for (i = 0; i < MdnFil.fieldanz; i ++)
		 {
			 if (MdnFil.mask[i].feldid == hWnd) return i + 1;
		 }
		 return 0;
}

BOOL MdnFilActiv (void)
/**
Test, ob Mandant und Filiale aktiv sind.
**/
{
	     int i;

	     if (MdnFil.fieldanz == 0) return FALSE;

		 for (i = 0; i < MdnFil.fieldanz; i ++)
		 {
			 if (IsInaktivButton (&MdnFil.mask[i]) == FALSE) return TRUE;
		 }
		 return FALSE;
}


void PrevKey (void)
/**
Vorhergenden Button aktivieren.
**/
{
	     int aktform = 0;

         if (AktButton >= MainButton.fieldanz) return;

		 aktform = TestMdnFil ();

		 if (aktform == 0)
		 {
             AktButton = GetAktButton ();
             if (AktButton == 0)
			 {
					 if (MdnFilActiv () == 0)
					 {
                            AktButton = MainButton.fieldanz - 1;
					 }
					 else
					 {
						    AktButton = MdnFil.fieldanz;
							aktform = 1;
					 }
			 }
             else
			 {
                     AktButton --;
			 }
		 }
		 else
		 {

			         AktButton = aktform - 1;
		 }
		 if (aktform)
		 {
                     if (AktButton == 0)
					 {
                               AktButton == MdnFil.fieldanz - 1;
							   aktform = 0;
					 }
                     else
					 {
                               AktButton --;
					 }
		 }
         if (aktform == 0)
		 {
            if (AktButton < MainButton.fieldanz)
			{
			   while (IsInaktivButton (&MainButton.mask[AktButton]))
			   {
				   AktButton --;
				   if (AktButton < 0) AktButton = MainButton.fieldanz - 1;
			   }
               SetFocus (
                 MainButton.mask [AktButton].feldid);
			}
            else
			{
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
			}
		 }
		 else
		 {
           if (AktButton < MdnFil.fieldanz)
		   {
			   while (IsInaktivButton (&MdnFil.mask[AktButton]))
			   {
				   AktButton --;
			       if (AktButton < 0) AktButton = MdnFil.fieldanz;
			   }
               SetFocus (
                 MdnFil.mask [AktButton].feldid);
		   }
           else
		   {
               SetFocus (
                 MdnFil.mask [AktButton - MdnFil.fieldanz].feldid);
		   }
		 }
}


void NextKey (void)
/**
Naechsten Button aktivieren.
**/
{
	     int aktform = 0;

         if (AktButton >= MainButton.fieldanz) return;

		 aktform = TestMdnFil ();

		 if (aktform == 0)
		 {
                     AktButton = GetAktButton ();

                     if (AktButton == MainButton.fieldanz - 1)
					 {
						 if (MdnFilActiv () == 0)
						 {
                               AktButton = 0;
						 }
						 else
						 {
							   aktform = 1;
							   AktButton = -1;
						 }
					 }
                     else
					 {
                               AktButton ++;
					 }
		 }
		 else
		 {

			         AktButton = aktform - 1;
		 }
		 if (aktform)
		 {
                     if (AktButton == MdnFil.fieldanz - 1)
					 {
                               AktButton = 0;
							   aktform = 0;
					 }
                     else
					 {
                               AktButton ++;
					 }
		 }

		 if (aktform == 0)
		 {
           if (AktButton < MainButton.fieldanz)
		   {
			   while (IsInaktivButton (&MainButton.mask[AktButton]))
			   {
				   AktButton ++;
			       if (AktButton >= MainButton.fieldanz) AktButton = 0;
			   }
               SetFocus (
                 MainButton.mask [AktButton].feldid);
		   }
           else
		   {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
		   }
		 }
		 else
		 {
           if (AktButton < MdnFil.fieldanz)
		   {
			   while (IsInaktivButton (&MdnFil.mask[AktButton]))
			   {
				   AktButton ++;
			       if (AktButton >= MdnFil.fieldanz) AktButton = 0;
			   }
               SetFocus (
                 MdnFil.mask [AktButton].feldid);
		   }
           else
		   {
               SetFocus (
                 MdnFil.mask [AktButton - MdnFil.fieldanz].feldid);
		   }
		 }
}

/*
void PrevKey (void)
/?**
Vorhergenden Button aktivieren.
**?/
{

         if (AktButton >= MainButton.fieldanz) return;
         AktButton = GetAktButton ();
         if (AktButton == 0)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         if (AktButton < MainButton.fieldanz)
         {
			   while (IsInaktivButton (&MainButton.mask[AktButton]))
			   {
				   AktButton --;
				   if (AktButton < 0) AktButton = MainButton.fieldanz - 1;
			   }
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}



void NextKey (void)
/?**
Naechsten Button aktivieren.
**?/
{

         if (AktButton >= MainButton.fieldanz) return;

         AktButton = GetAktButton ();
         if (AktButton == MainButton.fieldanz - 1)
         {
                     AktButton = 0;
         }
         else
         {
                     AktButton ++;
         }
         if (AktButton < MainButton.fieldanz)
         {
			   while (IsInaktivButton (&MainButton.mask[AktButton]))
			   {
				   AktButton ++;
			       if (AktButton >= MainButton.fieldanz) AktButton = 0;
			   }
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}
*/

void RightKey (void)
/**
Button nach rechts.
**/
{

         AktButton = GetAktButton ();
         if (AktButton <  MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz +
                                 MainButton2.fieldanz - 5;
         }
         else if (AktButton == MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         if (AktButton < MainButton.fieldanz)
         {
			   while (IsInaktivButton (&MainButton.mask[AktButton]))
			   {
				   AktButton ++;
			       if (AktButton >= MainButton.fieldanz) AktButton = 0;
			   }
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}

void LeftKey (void)
/**
Button nach rechts.
**/
{

         AktButton = GetAktButton ();
         if (AktButton <  MainButton.fieldanz)
         {
                     AktButton = MainButton.fieldanz;
         }
         else if (AktButton == MainButton.fieldanz +
                               MainButton2.fieldanz - 5)
         {
                     AktButton = MainButton.fieldanz - 1;
         }
         else
         {
                     AktButton ++;
         }

         if (AktButton < MainButton.fieldanz)
         {
			   while (IsInaktivButton (&MainButton.mask[AktButton]))
			   {
				   AktButton ++;
			       if (AktButton >= MainButton.fieldanz) AktButton = 0;
			   }
               SetFocus (
                 MainButton.mask [AktButton].feldid);
         }
         else
         {
               SetFocus (
                 MainButton2.mask [AktButton - MainButton.fieldanz].feldid);
         }
}

void ActionKey (void)
/**
Button-Aktion ausfuehren.
**/
{
         field *feld;
		 int aktform;

		 aktform = TestMdnFil ();

		 if (aktform == 0)
		 {
              AktButton = GetAktButton ();
              feld = GetAktField ();
		 }
		 else
		 {
			  feld = &MdnFil.mask [aktform -1];
		 }

         if (feld->after != (int (*) (void)) 0)
         {
                    (*feld->after) ();
         }
         else if (feld->BuId != 0)
         {
                    SendMessage (GetParent (feld->feldid), WM_COMMAND,
                                            MAKELONG (feld->BuId,
                                            feld->feldid),
                                            0l);
         }
}


/*
void ActionKey (void)
/?**
Button-Aktion ausfuehren.
**?/
{
         field *feld;

         AktButton = GetAktButton ();
         feld = GetAktField ();

         if (feld->after != (int (*) (void)) 0)
         {
                    (*feld->after) ();
         }
         else if (feld->BuId != 0)
         {
                    SendMessage (GetParent (feld->feldid), WM_COMMAND,
                                            MAKELONG (feld->BuId,
                                            feld->feldid),
                                            0l);
         }
}
*/


int IsKeyPress (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         int taste;
         int i;
         ColButton *ColBut;
         char *pos;



         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     if (keypressed)
                     {
                                  return FALSE;
                     }
                     taste = (int) msg->wParam;
                     if (taste == VK_UP)
                     {
                               PrevKey ();
                               return TRUE;
                     }
                     if (taste == VK_DOWN)
                     {
                               NextKey ();
                               return TRUE;
                     }
                     if (taste == VK_RIGHT)
                     {
                               RightKey ();
                               return TRUE;
                     }
                     if (taste == VK_LEFT)
                     {
                               LeftKey ();
                               return TRUE;
                     }
                     if (taste == VK_TAB)
                     {
                         if (GetKeyState (VK_SHIFT) < 0)
                         {
                              PrevKey ();
                              return TRUE;
                         }
                         NextKey ();
                         return TRUE;
                     }
                     if (taste == VK_RETURN)
                     {
                               ActionKey ();
                               return TRUE;
                     }
                     for (i = 0; i < MainButton.fieldanz; i ++)
                     {
                         ColBut = (ColButton *) MainButton.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                        keypressed = i + 1;
                                        keyhwnd = MainButton.mask[i].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                              }
                         }
                    }

                    for (i = 0; i < MainButton2.fieldanz; i ++)
                    {
                         ColBut = (ColButton *) MainButton2.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                         keypressed = i + MainButton.fieldanz;
                                         keyhwnd = MainButton2.mask[i].feldid;
                                         SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                         return TRUE;
                              }
                         }
                      }
              }
              case WM_KEYUP :
              {
                     if (!keypressed)
                     {
                                  return FALSE;
                     }
                     taste = (int) msg->wParam;
                     for (i = 0; i < MainButton.fieldanz; i ++)
                     {
                         ColBut = (ColButton *) MainButton.mask[i].feld;
                         if (ColBut->text1)
                         {
                             if ((pos = strchr (ColBut->text1, '&')) &&
                                ((UCHAR) toupper (*(pos + 1)) == taste))
                             {
                                   if (keyhwnd != MainButton.mask[i].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = MainButton.mask[i].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                    }

                    for (i = 0; i < MainButton2.fieldanz; i ++)
                    {
                         ColBut = (ColButton *) MainButton2.mask[i].feld;
                         if (ColBut->text1)
                         {
                              if ((pos = strchr (ColBut->text1, '&')) &&
                                 ((UCHAR) toupper (*(pos + 1)) == taste))
                              {
                                   if (keyhwnd != MainButton2.mask[i].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = MainButton2.mask[i].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                      }
             }
         }
         return FALSE;
}


int     ProcessMessages(void)
{
       MSG msg;

        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsMouseMessage (&msg));
              else if (IsHotKey (&msg));
              else if (IsKeyPress (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
        }
        DestroyFonts ();
        return msg.wParam;
}


LONG FAR PASCAL WndProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
		HDC hdc;

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == BackWindow2)
                    {
                             if (FitWindow == 0)
                             {
                                        CreateFitWindow ();
                             }
                             display_form (BackWindow2,&MainButton, 0, 0);
                    }
                    else if (hWnd == BackWindow3)
                    {
                             if (MainButton2.mask[0].feldid == 0)
                             {
                                  display_form (BackWindow3,&MainButton2, 0, 0);
                             }
                    }
                    else if (hWnd == FitWindow)
                    {
                           hdc = BeginPaint (hWnd, &ps);
                           paintlogo (hdc, fitbmp, 0, 0, SRCCOPY);
                           EndPaint (hWnd, &ps);
                    }
                    else if (hWnd == LogoWindow)
                    {
 						   SetMdnFilPos ();
						   display_form (LogoWindow, &MdnFil, 0, 0);
                    }
                    break;
              case WM_KEYDOWN :
                         switch (wParam)
                         {
                              case VK_F5 :
                                      func5 ();
                                      return 0;

                         }
                         break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    return MenuJob(hWnd,wParam,lParam);
              case WM_DESTROY :
                    if (hWnd == hMainWindow)
                    {
                             ExitProcess (0);
                             return 0;
                    }
                    return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG MenuJob(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
        HMENU hMenu;

        hMenu = GetMenu (hWnd);

        switch (wParam)
        {
              case CM_HROT :
                     logorx = (logorx + 1) % 2;
                     if (logorx)
                     {
                               CheckMenuItem (hMenu, CM_HROT, MF_CHECKED);
                               CheckMenuItem (hMenu, CM_VROT, MF_UNCHECKED);
                               logory = 0;
                     }
                     else
                     {
                               CheckMenuItem (hMenu, CM_HROT, MF_UNCHECKED);
                     }
                     sign = signx = 1;
                     xstretch = ystretch = 0;
                     return 0;
              case CM_VROT :
                     logory = (logory + 1) % 2;
                     if (logory)
                     {
                               CheckMenuItem (hMenu, CM_VROT, MF_CHECKED);
                               CheckMenuItem (hMenu, CM_HROT, MF_UNCHECKED);
                               logorx = 0;
                     }
                     else
                     {
                               CheckMenuItem (hMenu, CM_VROT, MF_UNCHECKED);
                     }
                     sign = signx = 1;
                     xstretch = ystretch = 0;
                     return 0;
              case CM_PROGRAMMENDE :
                     DeleteObject (fitbmp);
                     DestroyWindow (hMainWindow);
                     KillTimer (FitWindow, 1);
                     ReleaseDC (FitWindow, fithdc);
                     PostQuitMessage(0);
                     return 0;
        }
        return (0);
}

LONG FAR PASCAL UhrProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {
              case WM_TIMER :
                    switch (wParam)
                    {
                        case 1 :
                           sysdate (datum);
                           systime (zeit);
                           sprintf (datumzeit, "%s %s", datum, zeit);
                           InvalidateRect (UhrWindow, NULL, TRUE);
                           UpdateWindow (UhrWindow);
                           return 0;
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

LONG FAR PASCAL FitLogoProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;

        switch(msg)
        {
              case WM_TIMER :
                    switch (wParam)
                    {
                        case 1 :
                           printlogo (fitbmp, 0, 0, SRCCOPY);
                           return 0;
                    }
                    break;
              case WM_PAINT :
                    fithdc = BeginPaint (hWnd, &ps);
                    strechbmp (fitbmp, 0, 0, SRCCOPY);
                    EndPaint (hWnd, &ps);
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


/** Ab hier Eingabe einer Nummer in einem extra Fenster mit
    numerischen Block fuer die Maus.
**/

static HWND eNumWindow;
static break_num_enter = 0;

static form *eForm = NULL;
static HWND ehWnd = NULL;
static int (*eFormProc) (WPARAM) = NULL;
static int (*MeLPProc) (HWND) = NULL;
static void (*SetNumCaret) (void) = NULL;
static BOOL Num3Mode = FALSE;
static HWND NumParent = NULL;
static BOOL NumZent = FALSE;          // zentriert in x-Richtung
static int Numlcx = 0;                // Fenster-breite
static int Numlcy = 0;                // y - Abstand vom unteren Rand

static void SetEform (form *eF, HWND ehW, int (*eFP) (WPARAM))
{
	   eForm     = eF;
	   ehWnd     = ehW;
	   eFormProc = eFP;
}

static void SetMeProc (int (*MeLp) (HWND))
{
	   MeLPProc = MeLp;
}

static void SetNumCaretProc (void (*NuCr) (void))
{
	   SetNumCaret = NuCr;
}

static void SetNum3 (BOOL mode)
{
	   Num3Mode = min (TRUE, mode);
}

static void SetNumParent (HWND hWnd)
{
	   NumParent = hWnd;
}

static void SetNumZent (BOOL mode, int x, int y)
{
	   NumZent = mode;
	   Numlcx = x;
	   Numlcy = y;
}


mfont EditNumFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};

/*
mfont TextNumFont = {"Arial", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};
*/

mfont TextNumFont = {"Courier New", 200, 0, 1, BLACKCOL,
                                         LTGRAYCOL,
                                         1,
                                         NULL};

mfont numfieldfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont ctrlfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont stornofont = {"", 150, 0, 1,
                                       RGB (255, 0, 0),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont choisefont = {"", 150, 0, 1,
                                       WHITECOL,
                                       RGB (120, 120, 0),
//                                       BLUECOL,
                                       1,
                                       NULL};

mfont OKfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont Cafont = {"", 90, 0, 1,
                                       RGB (255, 255, 255),
                                       REDCOL,
                                       1,
                                       NULL};

mfont ausweinhfont = {"", 150, 0, 1,
                                       RGB (255, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

ColButton Num1      =   {"1", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num2      =   {"2", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num3      =   {"3", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num4      =   {"4", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num5      =   {"5", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num6      =   {"6", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num7      =   {"7", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num8      =   {"8", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num9      =   {"9", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton Num0      =   {"0", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumP     =   {",", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumM     =   {"-", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};


ColButton NumLeft   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeill, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumRight   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilr, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumUp   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilo, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumDown   =   {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         pfeilu, -1, -1,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumDel     =   {"DEL", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

ColButton NumPlus   =   {"+", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumMinus   =   {"-", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         14};

ColButton NumCls    =   {"C", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

ColButton NumAbbruch  =   {"Abbruch", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         0};

ColButton NumOK       =   {"OK", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         BLACKCOL,
                         GREENCOL,
                         0};

ColButton BuStorno =   {"&Storno", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         REDCOL,
                         14};

ColButton BuChoise =   {"&Auswahl", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         RGB (120,120, 0),
//                         RGB (255, 255, 255),
//                         REDCOL,
                         14};


ColButton AuswahlEinh1= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};

ColButton AuswahlEinh2= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};

ColButton AuswahlEinh3= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};
ColButton AuswahlEinh4= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};
ColButton AuswahlEinh5= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};
ColButton AuswahlEinh6= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};
ColButton AuswahlEinh7= {"", -1, -1,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, -1, -1,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         15};

static int nbsb  = 85;

static int nbsx   = 5;
int ny    = 10;
/*
static int nbss0 = 80;
static int nbss  = 64;
static int nbsz  = 40;
static int nbsz0 = 64;
*/
static int nbss0 = 64;
static int nbss  = 64;
static int nbsz  = 40;
static int nbsz0 = 64;
static int nAktButton = 0;

static int donum1 ();
static int donum2 ();
static int donum3 ();
static int donum4 ();
static int donum5 ();
static int donum6 ();
static int donum7 ();
static int donum8 ();
static int donum9 ();
static int donum0 ();
static int donump ();
static int donumm ();

static int doleft ();
static int doright ();
static int doup ();
static int dodown ();
static int dodel ();
static int doabbruch ();
static int doOK ();
static int doplus ();
static int dominus ();
static int docls ();

static field _NumField[] = {
(char *) &Num1,          nbss0, nbsz0, ny, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum1, 0,
(char *) &Num2,          nbss0, nbsz0, ny, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum2, 0,
(char *) &Num3,          nbss0, nbsz0, ny, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum3, 0,
(char *) &Num4,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum4, 0,
(char *) &Num5,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum5, 0,
(char *) &Num6,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum6, 0,
(char *) &Num7,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum7, 0,
(char *) &Num8,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 2 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum8, 0,
(char *) &Num9,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx + 3 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           donum9, 0,
(char *) &Num0,          nbss0, nbsz0, ny + 2 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donum0, 0,
(char *) &NumP,          nbss0, nbsz0, ny + 1 * nbsz0, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donump, 0,
(char *) &NumM,          nbss0, nbsz0, ny, nbsx, 0, "",
                                                           COLBUTTON, 0,
                                                           donumm, 0};

static form NumField = {12, 0, 0, _NumField, 0, 0, 0, 0, &numfieldfont};

void ChangeNumBkColor ()
{
	int i;
	ColButton *c;

	if (DynColor)
	{
		for (i = 0; i < NumField.fieldanz; i ++)
		{
			c = (ColButton *) NumField.mask[i].feld;
			if (c->bmp == NULL)
			{
				c->BkColor = RGB (0, 100, 255);
			}
		}
	}
}
/*
static int cbss0 = 80;
static int cbss  = 64;
static int cbsz  = 40;
static int cbsz0 = 64;
*/

static int cbss0 = 64;
static int cbss  = 64;
static int cbsz  = 40;
static int cbsz0 = 64;
static int cbsx = nbsx + 5 * nbss0;

static field _NumControl1[] = {
(char *) &NumLeft,       cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doleft, 0,
(char *) &NumRight,       cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doright, 0,
(char *) &NumDel,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           dodel, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl1 = {3, 0, 0, _NumControl1, 0, 0, 0, 0, &ctrlfont};

static field _NumControl2[] = {
(char *) &NumPlus,       cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doplus, 0,
(char *) &NumMinus,       cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dominus, 0,
(char *) &NumCls,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           docls, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl2 = {3, 0, 0, _NumControl2, 0, 0, 0, 0, &ctrlfont};

static field _NumControl3[] = {
(char *) &NumUp,          cbss0, cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                           doup, 0,
(char *) &NumDown,        cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dodown, 0,
(char *) &NumCls,         cbss0, cbsz0, ny, cbsx            , 0, "",
                                                           COLBUTTON, 0,
                                                           docls, 0,
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};

static form NumControl3 = {3, 0, 0, _NumControl3, 0, 0, 0, 0, &ctrlfont};

static form NumControl;


static field _CaControl[] = {
(char *) &NumAbbruch,     cbss0, cbsz0, ny, cbsx + 1 * nbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           doabbruch, 0};
static form CaControl = {1, 0, 0, _CaControl, 0, 0, 0, 0, &Cafont};

static field _OKControl[] = {
(char *) &NumOK,         cbss0 * 2,
                                cbsz0, ny, cbsx,             0, "",
                                                           COLBUTTON, 0,
                                                             doOK, 0};
static form OKControl = {1, 0, 0, _OKControl, 0, 0, 0, 0, &OKfont};


static char editbuff [40];
static double editsum;
static char eaction = ' ';

static field _EditForm [] = {
editbuff,             0, 0, 0, 0, 0, "", EDIT, 0, 0, 0};

static form EditForm = {1, 0, 0, _EditForm, 0, 0, 0, 0, &EditNumFont};


static field _TextForm [] = {
editbuff,             0, 0, 0, 0, 0, "", READONLY, 0, 0, 0};

static form TextForm = {1, 0, 0, _TextForm, 0, 0, 0, 0, &TextNumFont};

static field _fAuswahlEinh [] = {
(char *) &AuswahlEinh1,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh2,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh3,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh4,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh5,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh6,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
(char *) &AuswahlEinh7,           18, 0, 0, 0, 0, "", COLBUTTON, 0, 0, 0,
};

static form fAuswahlEinh = {7, 0, 0, _fAuswahlEinh, 0, 0, 0, 0, &ausweinhfont};


static field _StornoForm [] = {
(char *) &BuStorno,       100, 64, 0, 0, 0, "", COLBUTTON, 0, 0, 0};

static form StornoForm = {1, 0, 0, _StornoForm, 0, 0, 0, 0, &stornofont};


void SetStorno (int (*sproc) (void), int BuId)
/**
Procedure fuer Storno Setzen.
**/
{
           StornoForm.mask[0].after = sproc;
           StornoForm.mask[0].BuId  = BuId;
}

static field _ChoiseForm [] = {
(char *) &BuChoise,       100, 64, 0, 0, 0, "", COLBUTTON, 0, 0, 0};

static form ChoiseForm = {1, 0, 0, _ChoiseForm, 0, 0, 0, 0, &choisefont};


void SetChoise (int (*sproc) (void), int BuId)
/**
Procedure fuer Storno Setzen.
**/
{
           ChoiseForm.mask[0].after = sproc;
           ChoiseForm.mask[0].BuId  = BuId;
}


int donum1 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '1');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '1', 0l);
        return 1;
}

int donum2 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '2');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '2', 0l);
        return 1;
}

int donum3 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '3');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '3', 0l);
        return 1;
}

int donum4 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '4');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '4', 0l);
        return 1;
}

int donum5 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '5');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '5', 0l);
        return 1;
}

int donum6 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '6');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '6', 0l);
        return 1;
}

int donum7 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '7');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '7', 0l);
        return 1;
}

int donum8 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '8');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '8', 0l);
        return 1;
}

int donum9 ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '9');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '9', 0l);
        return 1;
}

int donum0 ()
/**
Aktion bei Button ,
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '0');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '0', 0l);
        return 1;
}

int donump ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '.');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '.', 0l);
        return 1;
}

int donumm ()
/**
Aktion bei Button 1
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '-');
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_CHAR, (WPARAM) '-', 0l);
        return 1;
}


int doleft ()
/**
Aktion bei Button links
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_LEFT);
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_LEFT, 0l);
        return 1;
}

int doright ()
/**
Aktion bei Button rechts
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_RIGHT);
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RIGHT, 0l);
        return 1;
}

int doup ()
/**
Aktion bei Button links
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_UP);
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_LEFT, 0l);
        return 1;
}

int dodown ()
/**
Aktion bei Button links
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_DOWN);
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_LEFT, 0l);
        return 1;
}

int dodel ()
/**
Aktion bei Button rechts
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_DELETE);
		}
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_DELETE, 0l);
        return 1;
}


int docls (void)
/**
EditFenster loeschen.
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_CLS);
		}
        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
		editsum = (double) 0.0;
		strcpy (editbuff, "");
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int doplus (void)
/**
EditFenster loeschen.
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '+');
		}
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}
		eaction = '+';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int dominus (void)
/**
EditFenster loeschen.
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '-');
		}
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}

		eaction = '-';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int dogleich (void)
/**
EditFenster loeschen.
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) '=');
		}
	    GetWindowText (EditForm.mask[0].feldid, editbuff, 39);
		if (eaction == '+')
		{
		        editsum += ratod (editbuff);
		}
		else if (eaction == '-')
		{
		        editsum -= ratod (editbuff);
		}
		else
		{
		        editsum = ratod (editbuff);
		}

		eaction = ' ';
	    sprintf (editbuff, "%lf", editsum);
		CloseControls (&EditForm);
        create_enter_form (eNumWindow, &EditForm, 0, 0);
        SetFocus (EditForm.mask[0].feldid);
        SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
		return 1;
}

int doabbruch ()
/**
Aktion bei Button F5
**/
{
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_F5);
		}
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_F5, 0l);
        return 1;
}

int doOK ()
/**
Aktion bei Button rechts
**/
{
	    if (CalcOn && eaction != ' ')
		{
			      dogleich ();
				  return 1;
		}
	    if (eFormProc)
		{
			return (*eFormProc) ((WPARAM) VK_RETURN);
		}
        SetFocus (EditForm.mask[0].feldid);
        PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RETURN, 0l);
        return 1;
}

void NumEnterBreak ()
/**
Endeflag fuer Erfassung setzen.
**/
{
        break_num_enter = 1;
}

HWND IsNumChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < NumField.fieldanz; i ++)
        {
                   if (MouseinWindow (NumField.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumField.mask[i].feldid);
                   }
        }

        for (i = 0; i < NumControl.fieldanz; i ++)
        {
                   if (MouseinWindow (NumControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (NumControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < OKControl.fieldanz; i ++)
        {
                   if (MouseinWindow (OKControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (OKControl.mask[i].feldid);
                   }
        }
        for (i = 0; i < CaControl.fieldanz; i ++)
        {
                   if (MouseinWindow (CaControl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (CaControl.mask[i].feldid);
                   }
        }

        for (i = 0; i < EditForm.fieldanz; i ++)
        {
			       if (EditForm.mask[i].feldid == NULL) break;
                   if (MouseinWindow (EditForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (EditForm.mask[i].feldid);
                   }
        }
        if (StornoForm.mask[0].feldid == 0)
        {
                   return FALSE;
        }

        for (i = 0; i < StornoForm.fieldanz; i ++)
        {
                   if (MouseinWindow (StornoForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (StornoForm.mask[i].feldid);
                   }
        }
        if (ChoiseForm.mask[0].feldid == 0)
        {
                   return FALSE;
        }

        for (i = 0; i < ChoiseForm.fieldanz; i ++)
        {
                   if (MouseinWindow (ChoiseForm.mask[i].feldid,
                                      mousepos))
                   {
                                 return (ChoiseForm.mask[i].feldid);
                   }
        }

        return NULL;
}


LONG FAR PASCAL EntNumProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
        HWND enchild;
        static int SendChild = 0;
        int HasFocus = 1;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
				    if (textblock)
					{
						textblock->OnPaint (hWnd, msg, wParam, lParam);
					}
					else
					{
                        display_form (eNumWindow, &NumField, 0, 0);
                        display_form (eNumWindow, &NumControl, 0, 0);
                        display_form (eNumWindow, &OKControl, 0, 0);
					    if (eForm == NULL)
						{
                             display_form (eNumWindow, &TextForm, 0, 0);
                             display_form (eNumWindow, &EditForm, 0, 0);
					         SendMessage (EditForm.mask[0].feldid, EM_SETSEL,
                                                 (WPARAM) 0, MAKELONG (-1, 0));
						}
					    else
						{
                             display_form (eNumWindow, &fAuswahlEinh, 0, 0);
						}

                        display_form (eNumWindow, &CaControl, 0, 0);

                        if (StornoForm.mask[0].after)
						{
                              display_form (eNumWindow, &StornoForm, 0, 0);
						}
                        else if (StornoForm.mask[0].BuId)
						{
                              display_form (eNumWindow, &StornoForm, 0, 0);
						}

                        if (ChoiseForm.mask[0].after)
						{
                              display_form (eNumWindow, &ChoiseForm, 0, 0);
						}
                        else if (ChoiseForm.mask[0].BuId)
						{
                              display_form (eNumWindow, &ChoiseForm, 0, 0);
						}

					    if (eForm == NULL)
						{
                               SetFocus (EditForm.mask[0].feldid);
						}
					}
                    break;

              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
				    if (textblock)
					{
						textblock->OnButton (hWnd, msg, wParam, lParam);
					}
					else
					{
                        enchild = IsNumChild (&mousepos);
                        MouseTest (enchild, msg);
                        if (enchild)
						{
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
						}
					}
                    return TRUE;
              case WM_ACTIVATE :
                    switch (wParam)
                    {
                         case WA_ACTIVE :
                         case WA_CLICKACTIVE :
                              break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterNumEnter (void)
/**
Fenster fuer numerische Eingabe registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, FITICON);
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  EntNumProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 120));
                   wc.lpszClassName =  "EnterNum";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

int IsNumKey (MSG *msg)
/**
HotKey Testen.
**/
{
	     POINT mousepos;
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         ColButton *ColBut;
         int taste;
         char *pos;
		 int i;

         GetCursorPos (&mousepos);

         switch (msg->message)
         {
	          case EM_SETSEL :
				  break;
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    NumEnterBreak ();
                                    syskey = KEY5;
                                    return TRUE;
                            case VK_DELETE :
								     docls ();
									 return TRUE;
                            case VK_LEFT :
								    if (eFormProc)
									{
			                            (*eFormProc) ((WPARAM) VK_UP);
										return TRUE;
									}
									break;
                            case VK_RIGHT :
								    if (eFormProc)
									{
			                            (*eFormProc) ((WPARAM) VK_DOWN);
										return TRUE;
									}
									break;
                            case VK_DOWN :
								    if (eFormProc && ehWnd)
									{
										SendMessage (ehWnd, WM_COMMAND, KEYDOWN, 0l);
										return TRUE;
									}
									break;

                            case VK_UP :
								    if (eFormProc && ehWnd)
									{
										SendMessage (ehWnd, WM_COMMAND, KEYUP, 0l);
										return TRUE;
									}
                                    break;

                            case VK_RETURN :
								    if (CalcOn && eaction != ' ')
									{
										dogleich ();
										return TRUE;
									}
               					    if (eFormProc)
									{
//                                            (*eFormProc) ((WPARAM) VK_RETURN);
                                            NumEnterBreak ();
                                            syskey = KEYCR;
                                            return TRUE;
									}

                                    NumEnterBreak ();
                                    GetWindowText (EditForm.mask[0].feldid,
                                                   EditForm.mask [0].feld,
                                                   EditForm.mask [0].length);
                                    syskey = KEYCR;
                                    return TRUE;
                     }
                     taste = (int) msg->wParam;
                     ColBut = (ColButton *) StornoForm.mask[0].feld;
                     if (ColBut->text1)
                     {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = StornoForm.mask[0].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                     }
                     ColBut = (ColButton *) ChoiseForm.mask[0].feld;
                     if (ColBut->text1)
                     {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = ChoiseForm.mask[0].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                     }

                     return FALSE;
              }
              case WM_KEYUP :
              {
                         taste = (int) msg->wParam;
                         ColBut = (ColButton *) StornoForm.mask[0].feld;
                         if (ColBut->text1)
                         {
                             if ((pos = strchr (ColBut->text1, '&')) &&
                                ((UCHAR) toupper (*(pos + 1)) == taste))
                             {
                                   if (keyhwnd != StornoForm.mask[0].feldid)
                                   {
                                                return FALSE;
                                   }
                                   keypressed = 0;
                                   keyhwnd = StornoForm.mask[0].feldid;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                            }
                         }
                         ColBut = (ColButton *) ChoiseForm.mask[0].feld;
                         if (ColBut->text1)
						 {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = ChoiseForm.mask[0].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
						 }

                         return FALSE;
              }
              case WM_CHAR :
              {
				     switch (msg->wParam)
					 {
					       case '+' :
								     doplus ();
									 return TRUE;
						   case '-' :
								     dominus ();
									 return TRUE;
					 }
				     if (eFormProc)
					 {

// Keine WM_CHAR-Meldung an eNumWindow. Die Werte kommen sondt doppelt.

 	                        if (msg->hwnd == eNumWindow)return TRUE;
						    (*eFormProc) (msg->wParam);
                            return TRUE;
					 }
					 else
					 {
                            SendMessage (EditForm.mask[0].feldid,
                                   msg->message,
                                   msg->wParam,
                                   msg->lParam);
                                   return TRUE;
					 }
               }
               case WM_LBUTTONDOWN :
			   {
		             if (MeLPProc)
					 {
						      for (i = 0; i < fAuswahlEinh.fieldanz; i ++)
							  {
			                        if (MouseinWindow (fAuswahlEinh.mask[i].feldid, &mousepos))
									{
										          currentfield = i;
								                  return FALSE;
									}
							  }
					 }
			   }
			   break;
          }
          return FALSE;
}


/****
void EnterNumBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic)
//Fenster fuer numerische Eingabe oeffnen.
{
        RECT rect, rect1;
        MSG msg;
        TEXTMETRIC tm;
        HFONT hFont;
        HDC hdc;
        int x, y, cx, cy;
        int i, j;
        int tlen;
        int elen, epos;
        int eheight;
        HCURSOR oldcursor;
        static int BitMapOK = 0;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;
		static BOOL NumKorr = FALSE;
		DWORD border;

		if (NumKorr == FALSE)
		{
			      NumKorr = TRUE;
				  nbsz0 = (int) (double) ((double) nbsz0 * scrfy);
				  nbss0 = (int) (double) ((double) nbss0 * scrfx);
		          KorrMainButton (&NumField, scrfx, scrfy);
		          KorrMainButton (&NumControl1, scrfx, scrfy);
		          KorrMainButton (&NumControl2, scrfx, scrfy);
		          KorrMainButton (&NumControl3, scrfx, scrfy);
		          KorrMainButton (&StornoForm, scrfx, scrfy);
		          KorrMainButton (&OKControl, scrfx, scrfy);
		          KorrMainButton (&CaControl, scrfx, scrfy);
				  ctrlfont.FontHeight = (int) (double)
					  ((double) ctrlfont.FontHeight * scrfy);
				  ctrlfont.FontWidth = (int) (double)
					  ((double) ctrlfont.FontWidth * scrfx);
		}

		if (Num3Mode)
		{
			memcpy (&NumControl, &NumControl3, sizeof (form));
		}
		else if (CalcOn)
		{
			memcpy (&NumControl, &NumControl2, sizeof (form));
			editsum = (double) 0.0;
			eaction = ' ';
		}
		else
		{
			memcpy (&NumControl, &NumControl1, sizeof (form));
		}

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();
        tlen = strlen (text);

        if (BitMapOK == 0)
        {
                 NumLeft.bmp  = LoadBitmap (hMainInst, "pfeill");
                 NumRight.bmp = LoadBitmap (hMainInst, "pfeilr");
                 NumUp.bmp    = LoadBitmap (hMainInst, "pfeilo");
                 NumDown.bmp  = LoadBitmap (hMainInst, "pfeilu");
                 NumPlus.bmp  = LoadBitmap (hMainInst, "plus");
                 NumMinus.bmp = LoadBitmap (hMainInst, "minus");
                 NumCls.bmp = LoadBitmap (hMainInst, "clear");
                 BitMapOK = 1;
        }

        RegisterNumEnter ();
		if (NumParent)
		{
                 GetWindowRect (NumParent, &rect);
                 GetWindowRect (LogoWindow, &rect1);
				 if (NumZent)
				 {
					 cx = rect1.right - rect1.left;
					 x = (rect.right - cx) / 2;
					 x = max (0, x);
				 }
				 else if (Numlcx)
				 {
					 cx = Numlcx;
					 x = (rect.right - cx) / 2;
					 x = max (0, x);
				 }
                 y = (int) (double) ((double) rect.bottom - (3 * nbsz0) - (46 * scrfy));
                 cy = rect.bottom - y;
				 if (Numlcy)
				 {
					 y -= Numlcy;
				 }
		}
		else
		{
                 GetWindowRect (LogoWindow, &rect);
                 x = rect.left;
                 y = (int) (double) ((double) rect.bottom - (3 * nbsz0) - (46 * scrfy));
                 cx = rect.right - rect.left;
                 cy = rect.bottom - y;
		}

        ny = (int) (double) ((double) cy - (3 * nbsz0) - (30 * scrfy));
        for (i = 0,j = 0; j < NumField.fieldanz - 3; i ++,j += 3)
        {
                     NumField.mask[j].pos[0] = ny + i * nbsz0;
                     NumField.mask[j + 1].pos[0] = ny + i * nbsz0;
                     NumField.mask[j + 2].pos[0] = ny + i * nbsz0;
        }
        NumField.mask[j].pos[0] = ny + 2 * nbsz0;
        NumField.mask[j + 1].pos[0] = ny + 1 * nbsz0;
        NumField.mask[j + 2].pos[0] = ny;


        OKControl.mask[0].pos[0]  = ny;

        NumControl.mask[0].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[1].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[2].pos[0] = ny + 2 * nbsz0;
        NumControl.mask[3].pos[0] = ny + 2 * nbsz0;
        CaControl.mask[0].pos[0]  = ny + 2 * nbsz0;

        cbsx = cx - 2 * nbss0 - 5;
        OKControl.mask[0].pos[1]  = cbsx;

        NumControl.mask[0].pos[1] = cbsx;
        NumControl.mask[1].pos[1] = cbsx + nbss0;
        NumControl.mask[2].pos[1] = cbsx;
        NumControl.mask[3].pos[1] = cbsx + nbss0;
        CaControl.mask[0].pos[1] = cbsx + nbss0;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
		if (Num3Mode)
		{
		             ausweinhfont.FontHeight = 150;
                     spezfont (&ausweinhfont);
                     hFont = SetWindowFont (hMainWindow);
                     hdc = GetDC (hMainWindow);
                     SelectObject (hdc, hFont);
                     GetTextMetrics (hdc, &tm);
                     DelFont (hFont);
                     ReleaseDC (hMainWindow, hdc);

                     eheight = 2 * tm.tmHeight;
                     elen = tm.tmAveCharWidth * 18;

                     epos = max (0, (epos - elen) / 2);
                     epos = NumField.mask[2].pos[1] + nbss0 + epos;
                     fAuswahlEinh.mask[0].length = elen;
					 if (scrfy > 1)
					 {
                                    fAuswahlEinh.mask[0].rows   =
										(int) (double) ((double) eheight * 0.75);
					 }
					 else
					 {
						            ausweinhfont.FontHeight = 100;
                                    fAuswahlEinh.mask[0].rows   =
										(int) (double) ((double) eheight * 0.6);
					 }
                     fAuswahlEinh.mask[0].pos[1] = epos;
                     fAuswahlEinh.mask[0].pos[0] = NumField.mask[0].pos[0];
					 for (i = 1; i < fAuswahlEinh.fieldanz; i ++)
					 {
                              fAuswahlEinh.mask[i].length = elen;
                              fAuswahlEinh.mask[i].rows   = fAuswahlEinh.mask[0].rows;
                              fAuswahlEinh.mask[i].pos[1] = epos;
                              fAuswahlEinh.mask[i].pos[0] = fAuswahlEinh.mask[i - 1].pos[0] +
								                            fAuswahlEinh.mask[0].rows   ;
					 }
		}

        EditNumFont.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&EditNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * nlen;
                 if (elen < epos) break;
                 EditNumFont.FontHeight -= 20;
                 if (EditNumFont.FontHeight <= 80) break;
        }


        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        eheight = tm.tmHeight;
        eheight += eheight / 3;
        EditForm.mask[0].length = elen;
        EditForm.mask[0].rows   = eheight;
        EditForm.mask[0].pos[1] = epos;
        EditForm.mask[0].pos[0] = ny;

		nlen = strlen (text);
        TextNumFont.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&TextNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * nlen;
                 if (elen < epos) break;
                 TextNumFont.FontHeight -= 20;
                 if (TextNumFont.FontHeight <= 80) break;
        }


        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        eheight = tm.tmHeight;
        eheight += eheight / 3;
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        elen = StornoForm.mask[0].length;
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        StornoForm.mask[0].pos[1] = epos;
        StornoForm.mask[0].pos[0] = cy - 30 - StornoForm.mask[0].rows;

        if (pic)
        {
                       EditForm.mask[0].picture = pic;
        }

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DelFont (hFont);
        ReleaseDC (hMainWindow, hdc);

        elen = tm.tmAveCharWidth * strlen (text);
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (5, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;

		while ((epos + elen) > NumControl.mask[0].pos[1])
		{
			    elen -= 5;
		}
        eheight = tm.tmHeight;
        TextForm.mask[0].length = elen;
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;
        TextForm.mask[0].pos[0] = (short)
                            EditForm.mask[0].pos[0] + 2 * (short) tm.tmHeight;
        TextForm.mask[0].feld = text;

		if (Num3Mode)
		{
                     fAuswahlEinh.mask[0].length = elen;
                     fAuswahlEinh.mask[0].pos[1] = epos;
                     fAuswahlEinh.mask[0].pos[0] = NumField.mask[0].pos[0];
					 for (i = 1; i < fAuswahlEinh.fieldanz; i ++)
					 {
                              fAuswahlEinh.mask[i].length = elen;
                              fAuswahlEinh.mask[i].pos[1] = epos;
                              fAuswahlEinh.mask[i].pos[0] = fAuswahlEinh.mask[i - 1].pos[0] +
								                            fAuswahlEinh.mask[i - 1].rows;
					 }
		}

        while (TRUE)
        {
                 spezfont (&TextNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 elen = tm.tmAveCharWidth * tlen;
                 if (elen < epos) break;
                 TextNumFont.FontHeight -= 20;
                 if (TextNumFont.FontHeight <= 80) break;
        }

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DelFont (hFont);
        ReleaseDC (hMainWindow, hdc);
        elen = tm.tmAveCharWidth * strlen (text);

        TextNumFont.FontHeight -= 20;

        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
        memcpy (editbuff, nummer, strlen (nummer));

        if (eForm == NULL)
		{
			border = WS_POPUP | WS_CAPTION;
		}
		else
		{
			border = WS_POPUP | WS_CAPTION;
		}

        eNumWindow  = CreateWindowEx (
                              0,
                              "EnterNum",
                              "",
                              border,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

        if (eForm == NULL)
		{
			create_enter_form (eNumWindow, &EditForm, 0, 0);
		}
        ShowWindow (eNumWindow, SW_SHOW);
        UpdateWindow (eNumWindow);

//        if (eForm == NULL)
		{
                    EnableWindows (hWnd, FALSE);
		}
        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));

		if (eForm == NULL)
		{
             SetFocus (EditForm.mask[0].feldid);
             SendMessage (EditForm.mask[0].feldid, EM_SETSEL,

                          (WPARAM) 0, MAKELONG (-1, 0));
             NumEnterWindow = EditForm.mask[0].feldid;
		}
		else
		{
             SetFocus (eForm->mask[0].feldid);
             NumEnterWindow = eForm->mask[0].feldid;
			 if (SetNumCaret) (*SetNumCaret) ();
             SetFocus (fAuswahlEinh.mask[currentfield].feldid);
		}

        NumEnterAktiv = 1;
        break_num_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsNumKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_num_enter) break;
        }
		if (eForm == 0)
		{
                 display_form (eNumWindow, &EditForm, 0, 0);
		}
        SetActiveWindow (hWnd);
        EnableWindows (hWnd, TRUE);
        CloseControls (&NumField);
        CloseControls (&NumControl);
        CloseControls (&OKControl);
        CloseControls (&CaControl);
		if (eForm == 0)
		{
                CloseControls (&EditForm);
		}
		else if (fAuswahlEinh.mask[0].feldid)
		{
                CloseControls (&fAuswahlEinh);
		}
        CloseControls (&TextForm);
        if (StornoForm.mask[0].after)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
        else if (StornoForm.mask[0].BuId)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
        DestroyWindow (eNumWindow);
        NumEnterAktiv = 0;
        strcpy (nummer, editbuff);
        SetCursor (oldcursor);

        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
  	    eForm     = NULL;
	    ehWnd     = NULL;
	    eFormProc = NULL;
		SetNumCaret = NULL;
	    MeLPProc  = NULL;
		SetNum3 (FALSE);
		SetNumParent (NULL);
}
******************/


void EnterNumBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic)
/**
Fenster fuer numerische Eingabe oeffnen.
**/
{
        RECT rect, rect1;
        MSG msg;
        TEXTMETRIC tm;
        HFONT hFont;
        HDC hdc;
        int x, y, cx, cy;
        int i, j;
        int tlen;
        int elen, epos;
        int eheight;
        HCURSOR oldcursor;
        static int BitMapOK = 0;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;
		static BOOL NumKorr = FALSE;
		DWORD border;
		SIZE size;

		if (NumKorr == FALSE)
		{
			      NumKorr = TRUE;
				  nbsz0 = (int) (double) ((double) nbsz0 * scrfy);
				  nbss0 = (int) (double) ((double) nbss0 * scrfx);
		          KorrMainButton (&NumField, scrfx, scrfy);
		          KorrMainButton (&NumControl1, scrfx, scrfy);
		          KorrMainButton (&NumControl2, scrfx, scrfy);
		          KorrMainButton (&NumControl3, scrfx, scrfy);
		          KorrMainButton (&StornoForm, scrfx, scrfy);
		          KorrMainButton (&ChoiseForm, scrfx, scrfy);
		          KorrMainButton (&OKControl, scrfx, scrfy);
		          KorrMainButton (&CaControl, scrfx, scrfy);
				  ctrlfont.FontHeight = (int) (double)
					  ((double) ctrlfont.FontHeight * scrfy);
				  ctrlfont.FontWidth = (int) (double)
					  ((double) ctrlfont.FontWidth * scrfx);
		}

		if (Num3Mode)
		{
			memcpy (&NumControl, &NumControl3, sizeof (form));
		}
		else if (CalcOn)
		{
			memcpy (&NumControl, &NumControl2, sizeof (form));
			editsum = (double) 0.0;
			eaction = ' ';
		}
		else
		{
			memcpy (&NumControl, &NumControl1, sizeof (form));
		}

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();
        tlen = strlen (text);

        if (BitMapOK == 0)
        {
                 NumLeft.bmp  = LoadBitmap (hMainInst, "pfeill");
                 NumRight.bmp = LoadBitmap (hMainInst, "pfeilr");
                 NumUp.bmp    = LoadBitmap (hMainInst, "pfeilo");
                 NumDown.bmp  = LoadBitmap (hMainInst, "pfeilu");
                 NumPlus.bmp  = LoadBitmap (hMainInst, "plus");
                 NumMinus.bmp = LoadBitmap (hMainInst, "minus");
                 NumCls.bmp = LoadBitmap (hMainInst, "clear");
                 BitMapOK = 1;
        }

        RegisterNumEnter ();
		if (NumParent)
		{
                 GetWindowRect (NumParent, &rect);
                 GetWindowRect (LogoWindow, &rect1);
				 if (NumZent)
				 {
					 cx = rect1.right - rect1.left;
					 x = (rect.right - cx) / 2;
					 x = max (0, x);
				 }
				 else if (Numlcx)
				 {
					 cx = Numlcx;
					 x = (rect.right - cx) / 2;
					 x = max (0, x);
				 }
                 y = (int) (double) ((double) rect.bottom - (3 * nbsz0) - (46 * scrfy));
                 cy = rect.bottom - y;
				 if (Numlcy)
				 {
					 y -= Numlcy;
				 }
		}
		else
		{
                 GetWindowRect (LogoWindow, &rect);
                 x = rect.left;
                 y = (int) (double) ((double) rect.bottom - (3 * nbsz0) - (46 * scrfy));
                 cx = rect.right - rect.left;
                 cy = rect.bottom - y;
		}

        ny = (int) (double) ((double) cy - (3 * nbsz0) - (30 * scrfy));
        for (i = 0,j = 0; j < NumField.fieldanz - 3; i ++,j += 3)
        {
                     NumField.mask[j].pos[0] = ny + i * nbsz0;
                     NumField.mask[j + 1].pos[0] = ny + i * nbsz0;
                     NumField.mask[j + 2].pos[0] = ny + i * nbsz0;
        }
        NumField.mask[j].pos[0] = ny + 2 * nbsz0;
        NumField.mask[j + 1].pos[0] = ny + 1 * nbsz0;
        NumField.mask[j + 2].pos[0] = ny;


        OKControl.mask[0].pos[0]  = ny;

        NumControl.mask[0].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[1].pos[0] = ny + 1 * nbsz0;
        NumControl.mask[2].pos[0] = ny + 2 * nbsz0;
        NumControl.mask[3].pos[0] = ny + 2 * nbsz0;
        CaControl.mask[0].pos[0]  = ny + 2 * nbsz0;

        cbsx = cx - 2 * nbss0 - 5;
        OKControl.mask[0].pos[1]  = cbsx;

        NumControl.mask[0].pos[1] = cbsx;
        NumControl.mask[1].pos[1] = cbsx + nbss0;
        NumControl.mask[2].pos[1] = cbsx;
        NumControl.mask[3].pos[1] = cbsx + nbss0;
        CaControl.mask[0].pos[1] = cbsx + nbss0;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
		if (Num3Mode)
		{
		             ausweinhfont.FontHeight = 150;
                     spezfont (&ausweinhfont);
                     hFont = SetWindowFont (hMainWindow);
                     hdc = GetDC (hMainWindow);
                     SelectObject (hdc, hFont);
                     GetTextMetrics (hdc, &tm);
                     DelFont (hFont);
                     ReleaseDC (hMainWindow, hdc);

                     eheight = 2 * tm.tmHeight;
                     elen = tm.tmAveCharWidth * 18;

                     epos = max (0, (epos - elen) / 2);
                     epos = NumField.mask[2].pos[1] + nbss0 + epos;
                     fAuswahlEinh.mask[0].length = elen;
					 if (scrfy > 1)
                     {
                                    fAuswahlEinh.mask[0].rows   =
										(int) (double) ((double) eheight * AuswahlEinhHeight);
					 }
					 else
					 {
						            ausweinhfont.FontHeight = 100;
                                    fAuswahlEinh.mask[0].rows   =
								 	 (int) (double) ((double) eheight * 0.6 * AuswahlEinhHeight / 0.75);
					 }
                     fAuswahlEinh.mask[0].pos[1] = epos;
                     if (AuswahlEinhAnz > 6)
                     {
                        fAuswahlEinh.mask[0].pos[0] = NumField.mask[0].pos[0];
                     }
                     else
                     {
                        fAuswahlEinh.mask[0].pos[0] = 2;
                     }
					 for (i = 1; i < fAuswahlEinh.fieldanz; i ++)
					 {
                              fAuswahlEinh.mask[i].length = elen;
                              fAuswahlEinh.mask[i].rows   = fAuswahlEinh.mask[0].rows;
                              fAuswahlEinh.mask[i].pos[1] = epos;
                              fAuswahlEinh.mask[i].pos[0] = fAuswahlEinh.mask[i - 1].pos[0] +
								                            fAuswahlEinh.mask[0].rows   ;
					 }
		}

        EditForm.mask[0].attribut & (0xFFFF ^ SCROLL);
		if (nlen == -1)
		{
                 EditForm.mask[0].attribut |= SCROLL;
                 EditForm.mask[0].picture = "40"; 
				 nlen = 20;
		}
        EditNumFont.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&EditNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 GetTextExtentPoint32 (hdc, "X", 1 , &size);
                 ReleaseDC (hMainWindow, hdc);

//                 elen = tm.tmAveCharWidth * nlen;
				 elen = size.cx * nlen;
                 if (elen < epos) break;
                 EditNumFont.FontHeight -= 20;
                 if (EditNumFont.FontHeight <= 80) break;
        }


        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        eheight = tm.tmHeight;
        eheight += eheight / 3;
        EditForm.mask[0].length = elen;
        EditForm.mask[0].rows   = eheight;
        EditForm.mask[0].pos[1] = epos;
        EditForm.mask[0].pos[0] = ny;

		nlen = strlen (text);
        TextNumFont.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&TextNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 GetTextExtentPoint32 (hdc, "X", 1 , &size);
                 ReleaseDC (hMainWindow, hdc);

//                 elen = tm.tmAveCharWidth * nlen;
				 elen = size.cx * nlen;
                 if (elen < epos) break;
                 TextNumFont.FontHeight -= 20;
                 if (TextNumFont.FontHeight <= 80) break;
        }


        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        eheight = tm.tmHeight;
        eheight += eheight / 3;
/*
        EditForm.mask[0].length = elen;
        EditForm.mask[0].rows   = eheight;
        EditForm.mask[0].pos[1] = epos;
        EditForm.mask[0].pos[0] = ny;
*/
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        elen = StornoForm.mask[0].length;
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        StornoForm.mask[0].pos[1] = epos;
        StornoForm.mask[0].pos[0] = cy - 30 - StornoForm.mask[0].rows;

        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        elen = ChoiseForm.mask[0].length;
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        ChoiseForm.mask[0].pos[1] = epos;
        ChoiseForm.mask[0].pos[0] = cy - 30 - ChoiseForm.mask[0].rows;

        if (pic)
        {
                       EditForm.mask[0].picture = pic;
					   if (strchr (pic, '%"') == NULL)
					   {
						   EditForm.mask[0].attribut |= SCROLL;
					   }

        }

		if ((EditForm.mask[0].attribut & SCROLL) == 1)
		{
                 EditForm.mask[0].picture = "40"; 
		}
        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DelFont (hFont);
        ReleaseDC (hMainWindow, hdc);

        elen = tm.tmAveCharWidth * strlen (text);
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (5, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;

		while ((epos + elen) > NumControl.mask[0].pos[1])
		{
			    elen -= 5;
		}
        eheight = tm.tmHeight;
        TextForm.mask[0].length = elen;
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;
        TextForm.mask[0].pos[0] = (short)
                            EditForm.mask[0].pos[0] + 2 * (short) tm.tmHeight;
        TextForm.mask[0].feld = text;

		if (Num3Mode)
		{
                     fAuswahlEinh.mask[0].length = elen;
                     fAuswahlEinh.mask[0].pos[1] = epos;
                     if (AuswahlEinhAnz > 6)
                     {
                       fAuswahlEinh.mask[0].pos[0] = NumField.mask[0].pos[0];
                     }
                     else
                     {
                       fAuswahlEinh.mask[0].pos[0] = 2;
                     }
					 for (i = 1; i < fAuswahlEinh.fieldanz; i ++)
					 {
                              fAuswahlEinh.mask[i].length = elen;
                              fAuswahlEinh.mask[i].pos[1] = epos;
                              fAuswahlEinh.mask[i].pos[0] = fAuswahlEinh.mask[i - 1].pos[0] +
								                            fAuswahlEinh.mask[i - 1].rows;
					 }
		}

        while (TRUE)
        {
                 spezfont (&TextNumFont);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 GetTextExtentPoint32 (hdc, "X", 1 , &size);
                 ReleaseDC (hMainWindow, hdc);

//                 elen = tm.tmAveCharWidth * tlen;
				 elen = size.cx * tlen;
                 if (elen < epos) break;
                 TextNumFont.FontHeight -= 20;
                 if (TextNumFont.FontHeight <= 80) break;
        }

        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DelFont (hFont);
        ReleaseDC (hMainWindow, hdc);
        elen = tm.tmAveCharWidth * strlen (text);

        TextNumFont.FontHeight -= 20;
/*
        spezfont (&TextNumFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        DelFont (hFont);
        ReleaseDC (hMainWindow, hdc);
        eheight = tm.tmHeight + tm.tmHeight / 3;
        epos = NumControl.mask[0].pos[1] - (NumField.mask[2].pos[1] + nbss0);
        epos = max (0, (epos - elen) / 2);
        epos = NumField.mask[2].pos[1] + nbss0 + epos;
        TextForm.mask[0].length = elen;
        TextForm.mask[0].rows   = eheight;
        TextForm.mask[0].pos[1] = epos;
*/

        memset (editbuff, ' ', 39);
        editbuff [39] = (char) 0;
        memcpy (editbuff, nummer, strlen (nummer));

		ChangeNumBkColor ();
        if (eForm == NULL)
		{
			border = WS_POPUP | WS_CAPTION;
		}
		else
		{
			border = WS_POPUP | WS_CAPTION;
		}

        eNumWindow  = CreateWindowEx (
                              0,
                              "EnterNum",
                              "",
                              border,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

        if (eForm == NULL)
		{
			create_enter_form (eNumWindow, &EditForm, 0, 0);
		}
        ShowWindow (eNumWindow, SW_SHOW);
        UpdateWindow (eNumWindow);

//        if (eForm == NULL)
		{
                    EnableWindows (hWnd, FALSE);
		}
        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));

		if (eForm == NULL)
		{
             SetFocus (EditForm.mask[0].feldid);
             SendMessage (EditForm.mask[0].feldid, EM_SETSEL,

                          (WPARAM) 0, MAKELONG (-1, 0));
             NumEnterWindow = EditForm.mask[0].feldid;
		}
		else
		{
             SetFocus (eForm->mask[0].feldid);
             NumEnterWindow = eForm->mask[0].feldid;
			 if (SetNumCaret) (*SetNumCaret) ();
             SetFocus (fAuswahlEinh.mask[currentfield].feldid);
		}

        EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (hWnd, FALSE);

        NumEnterAktiv = 1;
        break_num_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsNumKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_num_enter) break;
        }
		if (eForm == 0)
		{
                 display_form (eNumWindow, &EditForm, 0, 0);
		}
        SetActiveWindow (hWnd);
        EnableWindows (eKopfWindow, TRUE);
        EnableWindows (eFussWindow, TRUE);
        EnableWindows (BackWindow2, TRUE);
        EnableWindows (BackWindow3, TRUE);
        EnableWindows (hWnd, TRUE);
        CloseControls (&NumField);
        CloseControls (&NumControl);
        CloseControls (&OKControl);
        CloseControls (&CaControl);
		if (eForm == 0)
		{
                CloseControls (&EditForm);
		}
		else if (fAuswahlEinh.mask[0].feldid)
		{
                CloseControls (&fAuswahlEinh);
		}
        CloseControls (&TextForm);
        if (StornoForm.mask[0].after)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
        else if (StornoForm.mask[0].BuId)
        {
                    CloseControls (&StornoForm);
                    StornoForm.mask[0].after = NULL;
                    StornoForm.mask[0].BuId = 0;
        }
        if (ChoiseForm.mask[0].after)
        {
                    CloseControls (&ChoiseForm);
                    ChoiseForm.mask[0].after = NULL;
                    ChoiseForm.mask[0].BuId = 0;
        }
        else if (ChoiseForm.mask[0].BuId)
        {
                    CloseControls (&ChoiseForm);
                    ChoiseForm.mask[0].after = NULL;
                    ChoiseForm.mask[0].BuId = 0;
        }
        DestroyWindow (eNumWindow);
        NumEnterAktiv = 0;
        strcpy (nummer, editbuff);
        SetCursor (oldcursor);

        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
  	    eForm     = NULL;
	    ehWnd     = NULL;
	    eFormProc = NULL;
		SetNumCaret = NULL;
	    MeLPProc  = NULL;
		SetNum3 (FALSE);
		SetNumParent (NULL);
}


void EnterCalcBox (HWND hWnd, char *text, char *nummer, int nlen, char *pic)
/**
Numbox als rechner aufrufen.
**/
{
	     CalcOn = TRUE;
         EnterNumBox (hWnd, text, nummer, nlen, pic);
		 CalcOn = FALSE;
}


/**

  Listenerfassung

**/

struct AUFP_S
{
        char pnr [5];
        char s [2];
        char a [14];
        char a_kun [14];
        char a_bz1 [25];
        char a_bz2 [25];
        char a_me_einh [5];
        char auf_me [13];
        char lief_me [13];
        char auf_me_vgl [13];
        char tara [13];
        char auf_me_bz [10];
        char lief_me_bz [10];
        char auf_pr_vk [13];
        char auf_lad_pr [13];
        char basis_me_bz [7];
        char erf_kz [2];
        char rest [13];
        char ls_charge [21];
        char lsp_txt [10];
        char me_einh_kun [5];
        char me_einh [5];

        char sa_kz_sint [9];
        char prov_satz [11];
        char leer_pos [9];
        char hbk_date [11];
        char ls_vk_dm [13];
        char ls_vk_euro [13];
        char ls_vk_fremd [13];
        char ls_lad_dm [13];
        char ls_lad_euro [13];
        char ls_lad_fremd [13];
        char rab_satz [10];
        char me_einh_kun1 [5];
        char auf_me1 [14];
        char inh1 [14];
        char me_einh_kun2 [5];
        char auf_me2 [14];
        char inh2 [14];
        char me_einh_kun3 [5];
        char auf_me3 [14];
        char inh3 [14];
        char lief_me1 [14];
        char lief_me2 [14];
        char lief_me3 [14];
		char retp_kz [30];
        char ls_ident [22];
		long nve_posi;
        char anz_einh [14];
		char ls_pos_kz [3];

};

struct AUFP_S aufps, aufps_null;
struct AUFP_S aufparr [AUFMAX];
int aufpanz = 0;
int aufpidx = 0;
int aufpanz_upd = 0;
static char auf_me [12];

// static int lfbsz0 = 36;
static int lfbsz0 = 42;
static int lfbss0 = 73;
static int lfbszs = 45;
static int lfbss1 = 73;

static int doinsert (void);
static int dodelete (void);
static int Auszeichnen ();
static int Nachliefern ();
static int Nichtliefern ();
static int SysChoise ();
static int SwitchwKopf ();
static int doPosLeer ();
void arrtoretp (int);
void LeseRetp (void) ;
BOOL LeseRetpUpd (void);
static int CheckAusz ();

static int sorta (void);
static int sortb (void);
static int sortame (void);
static int sortlme (void);
static int sorts (void);

mfont ListFont = {"Courier New", 200, 150, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      0,
                                      NULL};
mfont InsFontT = {"Courier New", 110, 0, 1, BLACKCOL,
                                            LTGRAYCOL,
                                            1,
                                            NULL};

mfont InsFont = {"Courier New", 150, 0, 1, BLACKCOL,
                                           LTGRAYCOL,
                                           1,
                                           NULL};

mfont lKopfFont = {"Courier New", 150, 0, 1, BLACKCOL,
                                             LTGRAYCOL,
                                             1,
                                             NULL};

mfont lFussFont = {"", 90, 0, 1,       RGB (0, 255, 255),
                                       BLUECOL,
                                       1,
                                       NULL};

mfont lScanFont = {"", 270, 0, 0,       RGB (0, 255, 255),
                                        BLUECOL,
                                        1,
                                        NULL};

ColButton BuInsert = {"Neu", -1, 5,
                       "F6", -1, 21,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (0, 255, 255),
                       BLUECOL,
                       FALSE};

ColButton BuNach = {"nachliefern", -1, 5,
                    "F7", -1, 21,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       RGB (0, 255, 255),
                       BLUECOL,
                       FALSE};

ColButton BuNicht = {"nicht vorh.", -1, 5,
                      "F8", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};
ColButton BuDel   = {"l�schen", -1, 5,
                      "F9", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuSys =   {"Ger�t", -1, 5,
                      "F11", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuKopf =   {"Wiegekopf", -1, 5,
                      "F10", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuLeer =  {"Leergut", -1, 5,
                      "F4", -1, 21,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      RGB (0, 255, 255),
                      BLUECOL,
                      FALSE};

ColButton BuAusZ = {"auszeich.", -1, 5,
                     "F9", -1, 21,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     YELLOWCOL,
                     -1};



char eanbuffer [256] = {""};

field _fScanform[] = {
eanbuffer,   6 * lfbss0, lfbszs, -1, 10 + 0 * (15 + lfbss0),      0, "255", EDIT | SCROLL, 0, BreakEanEnter, 0,
};         

form fScanform = {1, 0, 0, _fScanform, 0, 0, 0, 0, &lScanFont};


static field _lKopfform [] =
{
        "Auftrag",      0, 0, -1, 10, 0, "", DISPLAYONLY, 0, 0, 0,
        auftrag_nr,     0, 0, -1, 100, 0, "%8d", DISPLAYONLY, 0, 0, 0
};

static form lKopfform = {2, 0, 0, _lKopfform, 0, 0, 0, 0, &lKopfFont};

static field _fLeerGut [] =
{
	    "Leergut erfassen", 0, 0, -1, 200, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form fLeerGut = {1, 0, 0, _fLeerGut, 0, 0, 0, 0, &lKopfFont};

field _lFussform[] = {
(char *) &BuNach,        lfbss0, lfbsz0,-1, 10 + 0 * (15 + lfbss0),   0, "", REMOVED, 0,
                                                           Nachliefern, 0,
(char *) &BuNicht,       lfbss0, lfbsz0,-1, 10 + 1 * (15 + lfbss0),   0, "", REMOVED, 0,
                                                           Nichtliefern, 0,
(char *) &BuInsert,      lfbss0, lfbsz0,-1, 10 + 3 * (15 + lfbss0),   0, "", COLBUTTON, 0,
// (char *) &BuInsert,      lfbss0, lfbsz0,-1, -1,                          0, "", COLBUTTON, 0,
                                                           doinsert,  0,
(char *) &BuAusZ,        lfbss0, lfbsz0,-1, 40 + 8 * (5 + lfbss0),    0, "", REMOVED, 0,
                                                           Auszeichnen, 0,
(char *) &BuSys,         lfbss0, lfbsz0,-1, 10 + 1 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           SysChoise0, 0,
(char *) &BuDel,         lfbss0, lfbsz0,-1, 10 + 5 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           dodelete, 0,
(char *) &BuKopf,        lfbss0, lfbsz0,-1, 10 + 7 * (15 + lfbss0),   0, "", COLBUTTON, 0,
                                                           SwitchwKopf, 0,
(char *) &BuLeer,        lfbss0, lfbsz0,-1, 10 + 5 * (15 + lfbss0),   0, "", REMOVED, 0,
                                                           doPosLeer, 0,
};

form lFussform = {8, 0, 0, _lFussform, 0, 0, 0, 0, &lFussFont};

static field _insform [] =
{
        aufps.a,           12, 0, 1, 1, 0, "%11.0f", READONLY, 0, 0, 0,
        aufps.a_bz1,       20, 0, 1, 14,0, "",       READONLY, 0, 0, 0,
        aufps.auf_pr_vk,   11, 0, 1, 36, 0, "%9.4f", READONLY, 0, 0, 0,
        aufps.auf_lad_pr,  10, 0, 1, 48, 0, "%9.3f", READONLY , 0, 0, 0,
};

static form insform = {4, 0, 0, _insform, 0, 0, 0, 0, &InsFont};

static field _insub [] = {
"Artikel-Nr",            0, 0, 0,  1, 0, "", DISPLAYONLY, 0, 0, 0,
"Bezeichnung",           0, 0, 0, 14, 0, "", DISPLAYONLY, 0, 0, 0,
"VK-Preis",              0, 0, 0, 36, 0, "", DISPLAYONLY, 0, 0, 0,
"Ladenpreis",            0, 0, 0, 48, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form insub = { 4, 0, 0,_insub, 0, 0, 0, 0, &InsFontT};


static field _testform [] =
{
        aufps.a,           11, 1, 0, 0, 0, "%11.0f", EDIT, 0, 0, 0,
        aufps.a_bz1,       20, 1, 0, 13,0, "",       READONLY, 0, 0, 0,
//        aufps.auf_me,       9, 1, 0, 35, 0, "%9.3f", EDIT, 0, 0, 0,
        aufps.auf_me,       6, 1, 0, 35, 0, "%6.0f", EDIT, 0, 0, 0,
		aufps.auf_me_bz,    2, 1, 0, 42, 0, "",      READONLY, 0, 0, 0,
        aufps.lief_me,      9, 1, 0, 46, 0, "%9.3f", READONLY , 0, 0, 0,
        aufps.s,            2, 1, 0, 57, 0, "",      READONLY, 0, 0, 0,
};


static form testform = { 6, 0, 4,_testform, 0, CheckAusz, 0, 0, &ListFont};

/*
static field _testub [] = {
"Artikel-Nr",           11, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0,
"Bezeichnung",          20, 1, 0,11, 0, "", DISPLAYONLY, 0, 0, 0,
"Auf-Me",                9, 1, 0,35, 0, "", DISPLAYONLY, 0, 0, 0,
"Lief-Me",               9, 1, 0,46, 0, "", DISPLAYONLY, 0, 0, 0,
"S",                     1, 1, 0,57, 0, "", DISPLAYONLY, 0, 0, 0
};
*/

static field _testub [] = {
"Artikel-Nr",           12, 1, 0, 0, 0, "", BUTTON, 0, sorta,  106,
"Bezeichnung",          22, 1, 0,12, 0, "", BUTTON, 0, sortb,  102,
"Auf-Me",               11, 1, 0,34, 0, "", BUTTON, 0, sortame,103,
"Lief-Me",              11, 1, 0,45, 0, "", BUTTON, 0, sortlme,104,
"S",                     4, 1, 0,56, 0, "", BUTTON, 0, sorts,  105
};

static form testub = { 5, 0, 0,_testub, 0, 0, 0, 0, &ListFont};

static field _testvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 34 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 45 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 56 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0
};

static form testvl = {5, 0, 0,_testvl, 0, 0, 0, 0, &ListFont};

static int asort = 1;
static int bsort = 1;
static int amesort = 1;
static int lmesort = 1;
static int ssort = 1;


static int sorta0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((int) (ratod(el1->a) - ratod (el2->a)) * asort);
}


static int sorta (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sorta0);
			asort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sortb0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((int) (strcmp (el1->a_bz1,el2->a_bz1)) * bsort);
}


static int sortb (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortb0);
			bsort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


static int sortame0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
			if (ratod (el1->auf_me) > ratod (el2->auf_me))
			{
				          return (1 * amesort);
            }
			if (ratod (el1->auf_me) < ratod (el2->auf_me))
			{
				          return (-1 * amesort);
            }
			return 0;
}

static int sortame (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortame0);
			amesort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sortlme0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
			if (ratod (el1->lief_me) > ratod (el2->lief_me))
			{
				          return (1 * lmesort);
            }
			if (ratod (el1->lief_me) < ratod (el2->lief_me))
			{
				          return (-1 * lmesort);
            }
			return 0;
}


static int sortlme (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sortlme0);
			lmesort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}

static int sorts0 (const void *elem1, const void *elem2)
{
	        struct AUFP_S *el1;
	        struct AUFP_S *el2;

			el1 = (struct AUFP_S *) elem1;
			el2 = (struct AUFP_S *) elem2;
	        return ((atoi (el1->s) - atoi (el2->s)) * ssort);
}


static int sorts (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufparr, aufpanz, sizeof (struct AUFP_S),
				   sorts0);
			ssort *= -1;
            ActivateColButton (eFussWindow, &lFussform, 2, -1, 1);
            ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
			return 0;
}


BOOL IsAuszArtikel (void)
/**
Feld pr_ausz pruefen.
**/
{
            char pr_ausz [2];

            if (lese_a_bas (ratod (aufps.a)) != 0)
            {
                          return FALSE;
            }
            strcpy (pr_ausz, "N");
            switch (_a_bas.a_typ)
            {
                   case 1 :
                         strcpy (a_hndw.pr_ausz, "N");
                         hndw_class.lese_a_hndw (_a_bas.a);
                         strcpy (pr_ausz, a_hndw.pr_ausz);
                         break;
                   case 2 :
                         strcpy (a_eig.pr_ausz, "N");
                         hndw_class.lese_a_eig (_a_bas.a);
                         strcpy (pr_ausz, a_eig.pr_ausz);
                         a_hndw.tara = a_eig.tara;
                         break;
                   case 3 :
                         strcpy (a_eig_div.pr_ausz, "N");
                         hndw_class.lese_a_eig_div (_a_bas.a);
                         strcpy (pr_ausz, a_eig_div.pr_ausz);
                         a_hndw.tara = a_eig_div.tara;
                         break;
                   default :
                         return FALSE;
            }
            if (pr_ausz[0] == 'N')
            {
                    return FALSE;
            }
            return TRUE;
}


int CheckAusz ()
/**
Pruefen, ob der Artikel ausgezeichnet werden kann.
**/
{
            if (IsAuszArtikel ())
            {
                         ActivateColButton (eFussWindow, &lFussform, 3, 0, 1);
            }
            else
            {
                         ActivateColButton (eFussWindow, &lFussform, 3, -1, 1);
            }
            return (0);
}

void GetMeEinh (void)
/**
Mengeneinheit fuer Kunde und Artikel holen.
**/
{
         KEINHEIT keinheit;
         double auf_me, auf_me_vgl;

         einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                                lsk.kun, _a_bas.a, &keinheit);

         strcpy (aufps.auf_me_bz,    keinheit.me_einh_kun_bez);
         strcpy (aufps.lief_me_bz,   keinheit.me_einh_bas_bez);
         sprintf (aufps.me_einh_kun, "%d", keinheit.me_einh_kun);
         sprintf (aufps.me_einh,     "%d", keinheit.me_einh_bas);
         clipped (aufps.auf_me_bz);
         clipped (aufps.lief_me_bz);
         if (keinheit.me_einh_kun == keinheit.me_einh_bas)
         {
                     strcpy (aufps.auf_me_vgl, aufps.auf_me);
         }
         else
         {
                     if (keinheit.inh <= (double) 0.0)
                     {
                                   keinheit.inh = (double) 1.0;
                     }
                     auf_me = (double) ratod (aufps.auf_me);
                     auf_me_vgl = auf_me * keinheit.inh;
                     sprintf (aufps.auf_me_vgl, "%.3lf", auf_me_vgl);
         }
         return;
}


void FillDM (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kundenwaehrung ist DM
**/
{
	  double pr_ek_euro;
	  double pr_vk_euro;
	  double kurs;

	  kurs = _mdn.konversion;
	  if (kurs == 0.0) kurs = 1.0;

      if (atoi (_mdn.waehr_prim) == 1)
	  {
/* Basiswaehrung ist DM                  */
	           pr_ek_euro = pr_ek / kurs;
	           pr_vk_euro = pr_vk / kurs;
	  }
	  else if (atoi (_mdn.waehr_prim) == 2)
	  {
/* Basiswaehrung ist EURO                   */
	           pr_ek_euro = pr_ek * kurs;
	           pr_vk_euro = pr_vk * kurs;
	  }
	  else
	  {
	           pr_ek_euro = pr_ek;
	           pr_vk_euro = pr_vk;
	  }
      sprintf (aufps.auf_pr_vk,   preispicture,  pr_ek);
      sprintf (aufps.auf_lad_pr,    "%6.2lf",    pr_vk);
      sprintf (aufps.ls_vk_dm,    preispicture,  pr_ek);
      sprintf (aufps.ls_lad_dm,    "%6.2lf",     pr_vk);
      sprintf (aufps.ls_vk_euro,  preispicture,  pr_ek_euro);
      sprintf (aufps.ls_lad_euro,  "%6.2lf",     pr_vk_euro);
}

void FillEURO (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kunden waehrung ist EURO
**/
{
	  double pr_ek_dm;
	  double pr_vk_dm;
	  double kurs;

	  kurs = _mdn.konversion;
	  if (kurs == 0.0) kurs = 1.0;

	  if (atoi (_mdn.waehr_prim) == 1)
/* Basiswaehrung ist DM                  */
	  {
	          pr_ek_dm = pr_ek * kurs;
	          pr_vk_dm = pr_vk * kurs;
	  }
	  else if (atoi (_mdn.waehr_prim) == 2)
	  {
/* Basiswaehrung ist EURO                   */
	           pr_ek_dm = pr_ek * kurs;
	           pr_vk_dm = pr_vk * kurs;
	  }
      sprintf (aufps.ls_vk_dm,   preispicture, pr_ek_dm);
      sprintf (aufps.ls_lad_dm,   "%6.2lf",    pr_vk_dm);
      sprintf (aufps.ls_vk_euro, preispicture, pr_ek);
      sprintf (aufps.ls_lad_euro, "%6.2lf",    pr_vk);
      sprintf (aufps.auf_pr_vk,  preispicture, pr_ek);
      sprintf (aufps.auf_lad_pr,  "%6.2lf",    pr_vk);

	  if (ld_pr_prim)
	  {
                sprintf (aufps.ls_lad_dm,   "%6.2lf",    pr_vk);
	  }

}

void FillFremd (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen. Kunden waehrung ist DM
**/
{
	  double pr_ek_dm;
	  double pr_vk_dm;
	  double pr_ek_euro;
	  double pr_vk_euro;
	  double kurs1;
	  double kurs2;
	  short  faktor;
	  int dsqlstatus;

	  kurs1 = _mdn.konversion;
	  if (kurs1 == 0.0) kurs1 = 1.0;

      DbClass.sqlin ((short *)   &aufk.mdn, 1, 0);
      DbClass.sqlin ((short *)   &aufk.waehrung, 1, 0);
	  DbClass.sqlout ((double *) &kurs2, 3, 0);
	  DbClass.sqlout ((short *)  &faktor, 1, 0);
	  dsqlstatus = DbClass.sqlcomm ("select kurs, faktor from devise "
		                            "where mdn = ? "
									"and devise_nr = ?");
	  if (dsqlstatus != 0)
	  {
            if (atoi (_mdn.waehr_prim) == 1)
			{
				pr_ek_dm = pr_ek;
				pr_vk_dm = pr_vk;
				pr_ek_euro = pr_ek / kurs1;
				pr_vk_euro = pr_vk / kurs1;
			}
			else
			{
				pr_ek_dm = pr_ek * kurs1;
				pr_vk_dm = pr_vk * kurs1;
				pr_ek_euro = pr_ek;
				pr_vk_euro = pr_vk;
			}
	  }
	  else
	  {
		    if (atoi (_mdn.waehr_prim) == 1)
			{
	            pr_ek_dm = pr_ek * kurs2 / faktor;
	            pr_vk_dm = pr_vk * kurs2 / faktor;
				pr_ek_euro = pr_ek_dm / kurs1;
				pr_vk_euro = pr_vk_dm / kurs1;
			}
			else
			{
                pr_ek_euro = pr_ek * kurs2 / faktor;
                pr_vk_euro = pr_vk * kurs2 / faktor;
				pr_ek_dm   = pr_ek_euro / kurs1;
				pr_vk_dm   = pr_vk_euro / kurs1;
			}
	  }
      sprintf (aufps.ls_vk_dm,    preispicture, pr_ek_dm);
      sprintf (aufps.ls_lad_dm,    "%6.2lf",    pr_vk_dm);
      sprintf (aufps.ls_vk_euro,  preispicture, pr_ek_euro);
      sprintf (aufps.ls_lad_euro,  "%6.2lf",    pr_vk_euro);
//      sprintf (aufps.ls_vk_fremd, preispicture, pr_ek);
//      sprintf (aufps.ls_lad_fremd, "%6.2lf",    pr_vk);
      sprintf (aufps.auf_pr_vk,   preispicture, pr_ek);
      sprintf (aufps.auf_lad_pr,    "%6.2lf",   pr_vk);

	  if (ld_pr_prim)
	  {
                sprintf (aufps.ls_lad_dm,   "%6.2lf",    pr_vk);
	  }

}

void InitWaehrung (void)
{
       sprintf (aufps.ls_vk_euro,  preispicture, 0.0);
       sprintf (aufps.ls_lad_euro, "%.2lf",      0.0);
//       sprintf (aufps.ls_vk_fremd, preispicture, 0.0);
//       sprintf (aufps.ls_lad_fremd,"%.2lf",      0.0);
}

void FillWaehrung (double pr_ek, double pr_vk)
/**
Felder fuer die verschiedenen Waehrungen fuellen.
**/
{
       mdn_class.lese_mdn (akt_mdn);
       if (retk.waehrung == 0)
       {
           retk.waehrung = atoi (_mdn.waehr_prim);
       }
	   InitWaehrung ();
	   switch (retk.waehrung )
	   {
	         case 1 :
				 FillDM (pr_ek, pr_vk);
				 break;
	         case 2 :
				 FillEURO (pr_ek, pr_vk);
				 break;
	         case 3 :
			    FillFremd (pr_ek, pr_vk);
 				 break;
	         default :
				 FillDM (pr_ek, pr_vk);
				 break;
	   }
}

void FillAktWaehrung ()
/**
Anzeigefelder fuer EK und VK fuellen.
**/
{
       mdn_class.lese_mdn (akt_mdn);
       if (retk.waehrung == 0)
       {
           retk.waehrung = atoi (_mdn.waehr_prim);
       }
	   if (retk.waehrung == 1 || retk.waehrung == 0)
	   {
           sprintf (aufps.auf_pr_vk,  preispicture,  retp.ret_vk_pr);
           sprintf (aufps.auf_lad_pr,   "%6.2lf",    retp.ret_lad_pr);
	   }
	   else if (retk.waehrung == 2)
	   {
           sprintf (aufps.auf_pr_vk,  preispicture,  retp.ret_vk_euro);
           sprintf (aufps.auf_lad_pr,    "%6.2lf",   retp.ret_lad_euro);
     	   if (ld_pr_prim)
		   {
                sprintf (aufps.ls_lad_dm,   "%6.2lf",retp.ret_lad_euro);
		   }
	   }
/*
	   else if (retk.waehrung == 3)
	   {
           sprintf (aufps.auf_pr_vk,   preispicture, retp.ret_vk_fremd);
           sprintf (aufps.auf_lad_pr,      "%6.2lf", retp.ret_lad_fremd);
     	   if (ld_pr_prim)
		   {
                sprintf (aufps.ls_lad_dm,  "%6.2lf", retp.ret_lad_fremd);
		   }
	   }
*/
	   else
	   {
           sprintf (aufps.auf_pr_vk,  preispicture,  retp.ret_vk_pr);
           sprintf (aufps.auf_lad_pr,   "%6.2lf",    retp.ret_lad_pr);
	   }
}

void ReadExtraPr (void)
{
    char lieferdat [12];
    int dsqlstatus;
    short sa;
    double pr_ek;
    double pr_vk;
	short extra_mdn;
	long extra_kun;

	if (retk.kun_fil == 0)
	{
		if (_mdn.tou == 0l || _mdn.kun == 0l)
		{
			return;
		}
		extra_mdn = (short) _mdn.tou;
		extra_kun = _mdn.kun;
	}
	else if (retk.kun_fil == 1)
	{
		if (_fil.verr_mdn == 0l || _fil.kun == 0l)
		{
			return;
		}
		extra_mdn = (short) _fil.verr_mdn;
		extra_kun = _fil.kun;
	}


	dlong_to_asc (retk.ret_dat, lieferdat);


	   WaPreis.SetAufArt (auf_art);

	   if (DllPreise.PriceLib != NULL && 
	    DllPreise.preise_holen != NULL)
	   {
				  dsqlstatus = (DllPreise.preise_holen) (extra_mdn, 
                                          0,
                                          retk.kun_fil,
                                          extra_kun,
                                          ratod (aufps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
				  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
	   }
  	   else
	   {
				dsqlstatus = WaPreis.preise_holen (extra_mdn,
                                          0,
                                          retk.kun_fil,
                                          extra_kun,
                                          ratod (aufps.a),
                                          lieferdat,
                                          &sa,
                                          &pr_ek,
                                          &pr_vk);
	   }
	   sprintf (aufps.ls_vk_fremd, "%lf", pr_ek);
}


int artikelOK (double a, BOOL DispMode)
/**
Artikel-Nummer in Basisdaten suchen.
**/
{
        short sa;
        double pr_ek;
        double pr_vk;
        char lieferdat [12];

        mdn_class.lese_mdn (akt_mdn);
        dsqlstatus = lese_a_bas (a);
        if (dsqlstatus != 0)
        {
                     return FALSE;
        }
        sprintf (aufps.a, "%11.0lf", _a_bas.a);
        strcpy (aufps.a_bz1, _a_bas.a_bz1);
        strcpy (aufps.a_bz2, _a_bas.a_bz2);
        pr_ek = pr_vk = (double) 0.0;
        sa = 0;
//        dlong_to_asc (lsk.lieferdat, lieferdat);
		dlong_to_asc (retk.ret_dat, lieferdat);

        if (DllPreise.PriceLib != NULL && 
		    DllPreise.preise_holen != NULL)
		{
				  dsqlstatus = (DllPreise.preise_holen) (retk.mdn, 
                                             retk.fil,
                                             retk.kun_fil,
                                             retk.kun,
                                             _a_bas.a,
                                             lieferdat,
                                             &sa,
                                             &pr_ek,
                                              &pr_vk);
				  if (dsqlstatus ==1) dsqlstatus = 0;
		 }
		 else
		 {
		        dsqlstatus =   WaPreis.preise_holen (retk.mdn,
                                             retk.fil,
                                             retk.kun_fil,
                                             retk.kun,
                                             _a_bas.a,
                                             lieferdat,
                                             &sa,
                                             &pr_ek,
                                             &pr_vk);
		 }
         if (dsqlstatus != 0)
         {
                      return TRUE;
         }
         sprintf (aufps.auf_pr_vk, preispicture, pr_ek);
         sprintf (aufps.auf_lad_pr, "%8.2lf",    pr_vk);
		 if (DispMode)
		 {
                  display_form (InsWindow, &insform, 0, 0);
                  CheckAusz ();
		 }
 	     FillWaehrung (pr_ek, pr_vk);
		 ReadExtraPr ();
         return TRUE;
}

void KorrInsPos (TEXTMETRIC *tm)
/**
Positionen in insform korrigieren.
**/
{
        static int posOK = 0;
        int i;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < insform.fieldanz; i ++)
        {
                    insform.mask[i].length *= (short) tm->tmAveCharWidth;
                    insform.mask[i].pos[1] *= (short) tm->tmAveCharWidth;
                    insform.mask[i].pos[0] *= (short) tm->tmHeight;
        }

        for (i = 0; i < insub.fieldanz; i ++)
        {
                    insub.mask[i].pos[1] *= (short) tm->tmAveCharWidth;
                    insub.mask[i].pos[0] *= (short) tm->tmHeight;
        }
}

void OpenInsert (void)
/**
Fenster fuer Insert-Row oeffnen.
**/
{
        RECT rect;
        RECT frmrect;
        HFONT hFont;
        TEXTMETRIC tm;
        int x, y, cx, cy;

        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&insform, insform.font, &frmrect);

        x = rect.left + 5;
        y = rect.top + 60;
        cx = frmrect.right + 26;
        cy = rect.bottom - rect.top - 200;

        spezfont (&InsFont);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        ReleaseDC (hMainWindow, hdc);
        DelFont (hFont);

        KorrInsPos (&tm);

        InsWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x, y,
                                    cx + 24, 72,
                                    eWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);
}

void DeleteInsert (void)
/**
Insertwindow schliessen.
**/
{
       CloseControls (&insform);
       CloseControls (&insub);
       DestroyWindow (InsWindow);
       InsWindow = NULL;
}

BOOL NewPosi (void)
/**
Positionsnummern neu vergeben und in Datenbank schreiben.
**/
{
          int i;

          beginwork ();
          if (LeseRetpUpd () == FALSE) return FALSE;
		  retp.mdn = retk.mdn;
		  retp.fil = retk.fil;
		  retp.ret = retk.ret;
          Retp.dbdelete_ret ();
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   sprintf (aufparr[i].pnr, "%d", (i + 1) * 10);
                   arrtoretp (i);
                   Retp.dbupdate ();
          }
          commitwork ();
          return TRUE;
}

BOOL DelLeerPos (void)
/**
Positionsnummern neu vergeben und in Datenbank schreiben.
**/
{
          int i;

          beginwork ();
          if (LeseRetpUpd () == FALSE) return FALSE;
		  retp.mdn = retk.mdn;
		  retp.fil = retk.fil;
		  retp.ret = retk.ret;
          Retp.dbdelete_ret ();
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) > 2)
                   {
                         arrtoretp (i);
                         Retp.dbupdate ();
                   }
          }
          commitwork ();
          if (eWindow)
          {
                   LeseRetp ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
          return TRUE;
}

BOOL SollToIst (void)
/**
Bei nicht bearbeiteten Positionen Ist-Wert auf Soll-Wert setzen.
**/
{
          int i;

          beginwork ();
          if (LeseRetpUpd () == FALSE) return FALSE;
		  retp.mdn = retk.mdn;
		  retp.fil = retk.fil;
		  retp.ret = retk.ret;
          Retp.dbdelete_ret ();
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) < 3)
                   {
                       strcpy (aufparr[i].lief_me,
                               aufparr[i].auf_me_vgl);
                       strcpy (aufparr[i].s, "3");
                   }
                   arrtoretp (i);
                   Retp.dbupdate ();
          }
          commitwork ();
          if (eWindow)
          {
                   LeseRetp ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
          return TRUE;
}

BOOL SetAllPosStat (void)
/**
Bei nicht bearbeiteten Positionen Ist-Wert auf Soll-Wert setzen.
**/
{
          int i;

          beginwork ();
          if (LeseRetpUpd () == FALSE) return FALSE;
	      retp.mdn = retk.mdn;
		  retp.fil = retk.fil;
		  retp.ret = retk.ret;
          Retp.dbdelete_ret ();
          for (i = 0; i < aufpanz_upd; i ++)
          {
                   if (atoi (aufparr[i].s) < 3)
                   {
                       strcpy (aufparr[i].s, "3");
                   }
                   arrtoretp (i);
				   retp.mdn = retk.mdn;
				   retp.fil = retk.fil;
				   retp.ret = retk.ret;
                   Retp.dbupdate ();
          }
          commitwork ();
          if (eWindow)
          {
                   LeseRetp ();
                   ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
          }
          return TRUE;
}

long GetNextPosi (int pos)
/**
Naechste gueltige Positionnummer ermitteln.
**/
{
          long ap;
          long posi;

          if (pos > 0)
          {
                strcpy (aufparr[pos].pnr, aufparr[pos - 1].pnr);
          }
          for (pos ++;pos <= aufpanz; pos ++)
          {
                 ap = atol (aufparr[pos].pnr);
                 if ((ap % 10) == 0) break;
          }
          posi = atol (aufparr[pos - 1].pnr) + 1;
          return (posi);
}

void ScrollAufp (int pos)
/**
aufparr ab pos zum Einfuegen scrollen.
**/
{
         int i;

         for (i = aufpanz; i > pos; i --)
         {
                       memcpy (&aufparr [i], &aufparr [i - 1],
                                sizeof (struct AUFP_S));
         }
         aufpanz ++;
}

void ScrollAufpDel (int pos)
/**
aufparr ab pos nach Oben scrollen.
**/
{
         int i;

         for (i = pos; i < aufpanz; i ++)
         {
                       memcpy (&aufparr [i], &aufparr [i + 1],
                                sizeof (struct AUFP_S));
         }
         aufpanz --;
}


int dodelete (void)
/**
Eine Zeile aus Liste loeschen.
**/
{
	     int pos;

		 if (BuDel.aktivate == -1) return (0);
		 if (abfragejnG (eWindow, "Position wirklich l�schen ?", "N") == 0)
		 {
			 return (0);
		 }

         pos = GetListPos ();
         arrtoretp (pos);
         beginwork ();
		 DbClass.sqlin ((short *)  &retp.mdn, 1, 0);
		 DbClass.sqlin ((short *)  &retp.fil, 1, 0);
		 DbClass.sqlin ((long *)   &retp.ret, 1, 0);
		 DbClass.sqlin ((double *) &retp.a, 3, 0);
		 DbClass.sqlin ((short *)  &retp.posi, 2, 0);
		 DbClass.sqlcomm ("delete from retp  "
			              "where mdn = ? "
						  "and fil = ? "
						  "and ret = ? "
						  "and a  = ? "
						  "and posi = ?");
         commitwork ();
         LeseRetp ();
         ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
         return 0;
}



// Auswahl ueber Artikel

mfont cartfont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CA
{
char a [14];
char a_bz1 [25];
char a_bz2 [25];
};

static struct CA ca, *caarr = NULL;
static int caanz;

static int sortca ();
static int sortcbz ();

static field _cartform [] = {
	ca.a,         13, 0, 0, 1, 0, "%13.0f", DISPLAYONLY, 0, 0, 0,
	ca.a_bz1,     24, 0, 0,16, 0, "",       DISPLAYONLY, 0, 0, 0,
	ca.a_bz2,     24, 0, 0,41, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form cartform = {3, 0, 0, _cartform, 0, 0, 0, 0, &cartfont};


static field _artub [] =
{
        "    Artikel",     15, 1, 0, 0, 0, "", BUTTON, 0, sortca, 0,
        " Bezeichnung 1",  25, 1, 0,15, 0, "", BUTTON, 0, sortcbz, 1,
        " Bezeichnung 2",  40, 1, 0,40, 0, "", BUTTON, 0, 0, 2,
};

static form artub = {3, 0, 0, _artub, 0, 0, 0, 0, &cartfont};

static field _artvl [] =
{
        "1",                1, 1, 0,15, 0, "", DISPLAYONLY, 0, 0, 0,
        "1",                1, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form artvl = {2, 0, 0, _artvl, 0, 0, 0, 0, &cartfont};
static int casort = 1;
static int cbsort = 1;


static int sortca0 (const void *elem1, const void *elem2)
{
	        struct CA *el1;
	        struct CA *el2;

			el1 = (struct CA *) elem1;
			el2 = (struct CA *) elem2;
	        return ((int) (ratod(el1->a) - ratod (el2->a)) * casort);
}


static int sortca (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (caarr, caanz, sizeof (struct CA),
				   sortca0);
			casort *= -1;
            ShowNewElist ((char *) caarr,
                             caanz,
                            (int) sizeof (struct CA));
			return 0;
}


static int sortcbz0 (const void *elem1, const void *elem2)
{
	        struct CA *el1;
	        struct CA *el2;

			el1 = (struct CA *) elem1;
			el2 = (struct CA *) elem2;
			return ( (int) (strcmp (el1->a_bz1, el2->a_bz1)) * cbsort);
}

static int sortcbz (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (caarr, caanz, sizeof (struct CA),
				   sortcbz0);
			cbsort *= -1;
            ShowNewElist ((char *) caarr,
                             caanz,
                            (int) sizeof (struct CA));
			return 0;
}


int dochoise (void)
/**
Auswahl ueber Artikel
**/
{
	    static long aanz = 0;
		double a;
		char a_bz1 [25];
		char a_bz2 [25];
		int cursor;
		int idx;
		int i;
		char a_bz [25];
		char buffer [0x400];
 	    char matchorg [25];
	    char matchupper[25];
	    char matchlower[25];
	    char matchuplow[25];

		strcpy (a_bz, "");
		if (kunquery)
		{
            textblock = new TEXTBLOCK;
            textblock->MainInstance (hMainInst, hMainWindow);
            textblock->ScreenParam (scrfx, scrfy);
            textblock->EnterTextBox (AufKopfWindow, "Bezeichnung", a_bz, 25, "", EntNumProc);

		    delete textblock;
		    textblock = NULL;
		}
		a_bz[24] = 0;
		if (syskey == KEY5 || syskey == KEYESC)
		{
                return FALSE;
		}
		if (scrfx > (double) 1.0)
		{
                cartfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		casort = cbsort = 1;
		if (caarr)
		{
			    GlobalFree (caarr);
		}
        DbClass.sqlout  ((long *) &aanz, 2, 0);
        DbClass.sqlcomm ("select count (*) from a_bas");
		if (aanz == 0)
		{
			disp_messG ("Fehler beim Zuwaeisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        caarr = (struct CA *) GlobalAlloc (GMEM_FIXED, aanz * sizeof (struct CA));
		if (caarr == NULL)
		{
			disp_messG ("Fehler beim Zuwaeisen von Speicherplatz", 2);
  	        EnableWindows (eKopfWindow, FALSE);
            EnableWindows (eFussWindow, FALSE);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
            EnableWindows (eWindow, FALSE);
			return 0;
		}

        DbClass.sqlout  ((double *) &a, 3, 0);
        DbClass.sqlout  ((char *)  a_bz1, 0, 25);
        DbClass.sqlout  ((char *)  a_bz2, 0, 25);

		clipped (a_bz);
		if (strcmp (a_bz, " ") <= 0)
		{
		    if (sort_a == FALSE)
			{
		        cursor = DbClass.sqlcursor ("select a, a_bz1, a_bz2 from a_bas "
			                                "where a > 0 order by a_bz1");
			}
		    else
			{
		        cursor = DbClass.sqlcursor ("select a, a_bz1, a_bz2 from a_bas "
			                                "where a > 0 order by a");
			}
		}
		else if (kunquery == 1)
		{
		       sprintf (buffer, "select a, a_bz1, a_bz2 from a_bas "
			                    "where a_bz1 matches \"%s*\" order by a", a_bz);
			   cursor = DbClass.sqlcursor (buffer);
		}

		else
		{
	           strcpy (matchorg, a_bz);
               strupcpy (matchupper, a_bz);
               strlowcpy (matchlower, a_bz);
               struplowcpy (matchuplow, a_bz);
		       sprintf (buffer, "select a, a_bz1, a_bz2 from a_bas "
			                    "where a_bz1 matches \"%s*\" "
								"or a_bz1 matches \"%s*\" "
								"or a_bz1 matches \"%s*\" "
								"or a_bz1 matches \"%s*\" "
								"order by a", matchorg, matchupper, matchlower, matchuplow);
			   cursor = DbClass.sqlcursor (buffer);
		}
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			sprintf (caarr[i].a, "%13.0lf", a);
			strcpy  (caarr[i].a_bz1, a_bz1);
			strcpy  (caarr[i].a_bz2, a_bz2);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		aanz = caanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfy);
		}
        syskey = 0;

        EnableWindow (eNumWindow, FALSE);
        idx = ShowWKopf (eNumWindow, (char *) caarr, aanz,
                        (char *) &ca, sizeof (struct CA),
                         &cartform, &artvl, &artub);
		CloseControls (&artub);
        EnableWindow (eNumWindow, TRUE);
        SetHoLines (1);
	    EnableWindows (eKopfWindow, FALSE);
        EnableWindows (eFussWindow, FALSE);
        EnableWindows (BackWindow2, FALSE);
        EnableWindows (BackWindow3, FALSE);
        EnableWindows (eWindow, FALSE);
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (caarr);
		        caarr = NULL;
                return FALSE;
        }

        strcpy (editbuff, caarr[idx].a);
		GlobalFree (caarr);
		caarr = NULL;
		Sleep (50);
		display_form (eNumWindow, &EditForm, 0, 0);
		SetFocus (EditForm.mask[0].feldid);
		if (a_ausw_direct)
		{
		       break_num_enter = TRUE;
		}
		return 0;
}















int doinsert (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
         struct AUFP_S aufp;
         char buffer [20];
//         char me_buffer [20];
         int pos;
         long akt_posi;

		 if (BuInsert.aktivate == -1) return (0);
         OpenInsert ();
         memcpy (&aufp, &aufps, sizeof (struct AUFP_S));
         memcpy (&aufps, &aufps_null, sizeof (struct AUFP_S));

         while (TRUE)
         {
                   strcpy (buffer, "0");

                   SetChoise (dochoise, 0);
                   EnterNumBox (eWindow,
                                "  Artikel  ", buffer, 12,
                                   testform.mask[0].picture);
                   InvalidateRect (eWindow, NULL, TRUE);
                   UpdateWindow (eWindow);

                   if ((double) ratod (buffer) <= (double) 0.0)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
                              return 0;
                   }
                   if (artikelOK ((double) ratod (buffer), TRUE))
                   {
                              break;
                   }

                   disp_messG ("Falsche Artikelnummer", 2);
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
                              return 0;
                   }
         }


         strcpy (buffer, "0");

         if (_a_bas.a_typ == 11 && auf_me_kz)
         {
                 EnterCalcBox (eWindow, " Menge ", buffer, 10,
                                   "%9.3f");
                 strcpy (aufps.lief_me, buffer);
                 if (ratod (aufps.lief_me) != (double) 0.0)
                 {
                     strcpy (aufps.s, "3");
                 }
         }

/*
         else if (auf_me_kz)
         {
                 GetMeEinh ();
				 sprintf (me_buffer, "Menge in %s", aufps.auf_me_bz);
                 EnterCalcBox (eWindow, me_buffer, buffer, 10,
                                   "%9.3f");
                 strcpy (aufps.auf_me, buffer);
         }
*/



         if (ohne_preis == 0 && immer_preis)
         {
                   EnterPreisBox (eWindow, -1, -1);
         }

         current_form = &testform;
         DeleteInsert ();
         InvalidateRect (eWindow, NULL, TRUE);
         UpdateWindow (eWindow);
         pos = GetListPos () + 1;
         akt_posi = GetNextPosi (pos);
         sprintf (aufps.pnr, "%ld", akt_posi);
         ScrollAufp (pos);
         memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
         arrtoretp (pos);
         beginwork ();
		 Retp.dbupdate ();
         commitwork ();
         LeseRetp ();
         ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
		 PostMessage (eWindow, WM_KEYDOWN, VK_DOWN, 0l);
		 if (wieg_neu_direct)
		 {
 		              PostMessage (eWindow, WM_KEYDOWN, VK_RETURN, 0l);
		 }
         return 0;
}

int endlist (void)
/**
Liste beenden.
**/
{
        break_list ();
        return (1);
}

BOOL testdecval (void)
/**
Groesse der Decimalkwerte pruefen.
**/
{
	static double * vars[] = {&retp.ret_me,
							  &retp.ret_vk_pr,
							  &retp.ret_lad_pr,
							  &retp.tara,
							  &retp.prov_satz,
							  NULL};

	static double values[] =  {9999.999,
							   9999.9999,
							   9999.99,
							   99999.999,
							   99.99};

	int i;

	for (i = 0; vars [i]; i ++)
	{
		if (*vars[i] > values [i])
		{
			*vars[i] = values [i];
		}
	}
	return 0;
}


void arrtoretp (int idx)
/**
Satz aus lsp in aufarr uebertragen.
**/
{
        retp.posi = atol (aufparr[idx].pnr);
        retp.a = (double) ratod (aufparr[idx].a);
        retp.ret_me = (double) ratod (aufparr[idx].lief_me);
//        retp.ret_vk_pr = (double) ratod (aufparr[idx].auf_pr_vk);
//        retp.ret_lad_pr = (double) ratod (aufparr[idx].auf_lad_pr);
        retp.tara = (double) ratod (aufparr[idx].tara);
        strcpy  (retp.erf_kz, aufparr[idx].erf_kz);
        retp.retp_txt = atol (aufparr[idx].lsp_txt);
        retp.prov_satz    = ratod (aufparr[idx].prov_satz);
/*
        retp.ret_vk_euro   = ratod (aufparr[idx].ls_vk_euro);
        retp.ret_vk_fremd  = ratod (aufparr[idx].ls_vk_fremd);
        retp.ret_lad_euro  = ratod (aufparr[idx].ls_lad_euro);
        retp.ret_lad_fremd = ratod (aufparr[idx].ls_lad_fremd);
*/

        retp.ret_vk_pr     = ratod (aufparr[idx].ls_vk_dm);
        retp.ret_lad_pr    = ratod (aufparr[idx].ls_lad_dm);
        retp.ret_vk_euro   = ratod (aufparr[idx].ls_vk_euro);
        retp.ret_vk_fremd  = ratod (aufparr[idx].ls_vk_fremd);
        retp.ret_lad_euro  = ratod (aufparr[idx].ls_lad_euro);
        retp.ret_lad_fremd = ratod (aufparr[idx].ls_lad_fremd);
		retp.retp_kz       = atoi (aufparr[idx].retp_kz);

		testdecval ();
}

long GenLText (void)
/**
Text-Nr fuer lsp.lsp_txt genereieren.
**/
{
        int dsqlstatus;
        short mdn;
        short fil;
        long gen_nr;
        extern short sql_mode;

        mdn = akt_mdn;
        fil = akt_fil;

        sql_mode = 1;
        dsqlstatus = AutoClass.nvholid (mdn, fil, "lspt_txt");
        if (dsqlstatus == 100)
        {
              dsqlstatus = AutoClass.nvanmprf (mdn,
                                     fil,
                                     "lspt_txt",
                                     (long) 1,
                                     (long) 999999,
                                     (long) 10,
                                      "");

              if (dsqlstatus == 0)
              {
                          dsqlstatus = AutoClass.nvholid (mdn,
                                                fil,
                                                "lspt_txt");
              }
         }
         gen_nr = auto_nr.nr_nr;
         sql_mode = 0;
         return gen_nr;
}


void TextInsert (void)
/**
Text fuer Nicht- osder Nachliefern einfuegen.
**/
{
       int aufidx;
       long lsp_txt;

       beginwork ();
       if (Lock_Lsp () == FALSE)
       {
            commitwork ();
            return;
       }
       aufidx = GetListPos ();
       memcpy (&aufparr [aufidx], &aufps, sizeof (struct AUFP_S));
       lsp_txt = atol (aufparr [aufidx].lsp_txt);
       if (lsp_txt == 0l)
       {
           lsp_txt = GenLText ();
       }
       if (lsp_txt == 0l) return;
       strcpy (aufparr[aufidx].s, "3");
       sprintf (aufparr[aufidx].lsp_txt, "%ld", lsp_txt);
       memcpy (&aufps, &aufparr [aufidx], sizeof (struct AUFP_S));
       lspt.nr = lsp_txt;
       lspt.zei = 10;
       strcpy (lspt.txt, ptabn.ptbez);
       lspt_class.dbupdate ();
       arrtoretp (aufidx);
       ls_class.update_lsp (0, lsp.mdn, lsp.fil, lsp.ls,
                                        lsp.a, lsp.posi);

       commitwork ();
       ShowNewElist ((char *) aufparr,
                      aufpanz,
                      (int) sizeof (struct AUFP_S));
       memcpy (&aufps, &aufparr [aufidx], sizeof (struct AUFP_S));
}

int Nachliefern ()
/**
Status NACHLIEFERN setzen.
**/
{
      ptab_class.lese_ptab ("na_lief_kz", "2");
      TextInsert ();
      return (0);
}


int Nichtliefern ()
/**
Status NICHTLIEFERN setzen.
**/
{
      ptab_class.lese_ptab ("na_lief_kz", "1");
      TextInsert ();
      return (0);
}

int doPosLeer ()
{
	  TEXTMETRIC tm;

      SetTextMetrics (eKopfWindow, &tm, &lKopfFont);

	  fLeerGut.mask[0].pos[1] = (short) lKopfform.mask[lKopfform.fieldanz - 1].pos[1] +
		                        10 * (short) tm.tmAveCharWidth;
      display_form (eKopfWindow, &fLeerGut, 0, 0);
	  EnterLeerPos ();
      CloseFontControls(&fLeerGut);
	  commitwork ();
      beginwork ();

      LeseRetp ();
      ShowNewElist ((char *) aufparr,
                             aufpanz,
                             (int) sizeof (struct AUFP_S));
	  return (0);
}

int SwitchwKopf ()
{
        static int arganz;
        static char *args[4];
        char kopf [4];

        if (la_zahl < 0) return 0;

        if (la_zahl == 2)
        {
            AktKopf = (AktKopf  + 1) % la_zahl;
            sprintf (kopf, "%d", AktKopf + 1);
            arganz = 4;
            args[0] = "Leer";
            args[1] = "6";
            args[2] = waadir;
            args[3] = (char *) kopf;
            fnbexec (arganz, args);
            print_messG (1, "Wiegkopf %s gew�hlt", kopf);
            return 0;
        }
        SaveMenue ();
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
		SetPosColorProc (NULL);
		ColorProc = ListRowColor;
        AktKopf = AuswahlKopf ();
		ColorProc = NULL;
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetActiveWindow (eWindow);
        AktivWindow = eWindow;
        RestoreMenue ();
        SetDblClck (IsDblClck, 0);

        sprintf (kopf, "%d", AktKopf + 1);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "6";
        args[2] = waadir;
        args[3] = (char *) kopf;
        fnbexec (arganz, args);
        return 0;
}

int SysChoise0 ()
{
        if (Scannen)
        {
            return DoScann ();
        }
        return SysChoise ();
}

int SysChoise ()
/**
Auswahl ueber Geraete.
**/
{
        SaveMenue ();
        EnableWindow (eWindow, FALSE);
        EnableWindow (eKopfWindow, FALSE);
        EnableWindow (eFussWindow, FALSE);
		SetPosColorProc (NULL);
		ColorProc = ListRowColor;
        AuswahlSysPeri ();
		ColorProc = NULL;
        EnableWindow (eWindow, TRUE);
        EnableWindow (eKopfWindow, TRUE);
        EnableWindow (eFussWindow, TRUE);
        SetActiveWindow (eWindow);
        AktivWindow = eWindow;
        RestoreMenue ();
        SetDblClck (IsDblClck, 0);
        return (0);
}


int Auszeichnen ()
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        if (BuAusZ.aktivate == -1) return (0);
        WorkModus = AUSZEICHNEN;
        PostMessage (eWindow, WM_KEYDOWN,  (WPARAM) VK_RETURN, 0l);
        return (0);
}


void IsDblClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
//	    int i;

        beginwork ();
        if (Lock_Lsp () == FALSE)
        {
            commitwork ();
            return;
        }
        aufpidx = idx;
        memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
        strcpy (auf_me, aufps.rest);
		SetPosColorProc (NULL);
		lese_a_bas (ratod (aufps.a));
        if (_a_bas.me_einh == 2)
        {
                       ArtikelWiegen (eFussWindow);
        }
        else
        {
                       ArtikelHand ();
        }
        WorkModus = WIEGEN;
        SetDblClck (IsDblClck, 0);
        current_form = &testform;
        strcpy (aufps.rest, auf_me);
        memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtoretp (idx);
        Retp.dbupdate ();
		SetPosColorProc (ListRowColor);
        ShowNewElist ((char *) aufparr,
                             aufpanz,
                            (int) sizeof (struct AUFP_S));
        /*
        ShowElist ((char *) aufparr,
                    aufpanz,
                  (char *) &aufps,
                  (int) sizeof (struct AUFP_S),
                  &testform);
        */
        commitwork ();
        return;
}

BOOL TestSysWg (void)
/**
Pruefen, ob die Warengruppe dem aaktiven Geraet zugeordnet ist.
**/
{
        pht_wg.sys = auszeichner;
        pht_wg.wg  = _a_bas.wg;
        if (pht_wg_class.dbreadfirst_wg () != 0)
        {
            return FALSE;
        }
        return TRUE;
}



BOOL retptoarr (int idx)
/**
Satz aus lsp in aufarr uebertragen.
**/
{
	    char wert [4];

        lese_a_bas (retp.a);
        /*
        if (TestSysWg () == FALSE)
        {
            return 1;
        }
        */
        sprintf (aufparr[idx].pnr, "%ld", retp.posi);
        sprintf (aufparr[idx].a, "%.0lf", retp.a);
        strcpy (aufparr[idx].a_bz1,_a_bas.a_bz1);
		sprintf (wert, "%hd", _a_bas.me_einh);
        dsqlstatus = ptab_class.lese_ptab ("me_einh", wert);
		strcpy (aufparr[idx].lief_me_bz, ptabn.ptbezk);

        sprintf (aufparr[idx].lief_me, "%.3lf",         retp.ret_me);
        sprintf (aufparr[idx].auf_pr_vk,preispicture,   retp.ret_vk_pr);
        sprintf (aufparr[idx].auf_lad_pr, "%.2lf",      lsp.ls_lad_pr);
        sprintf (aufparr[idx].tara, "%.3lf",            retp.tara);
//        sprintf (aufparr[idx].s, "%hd", lsp.pos_stat);
        strcpy  (aufparr[idx].erf_kz,                   retp.erf_kz);
        sprintf (aufparr[idx].lsp_txt, "%ld",           retp.retp_txt);
        sprintf (aufparr[idx].prov_satz, "%.3lf",       retp.prov_satz);
        sprintf (aufparr[idx].ls_vk_euro ,preispicture, retp.ret_vk_euro);
        sprintf (aufparr[idx].ls_vk_fremd,preispicture, retp.ret_vk_fremd);
        sprintf (aufparr[idx].ls_lad_euro,"%.2lf",      retp.ret_lad_euro);
        sprintf (aufparr[idx].ls_lad_fremd,"%.2lf",     retp.ret_lad_fremd);

        sprintf (aufparr[idx].ls_vk_dm,  preispicture,  retp.ret_vk_pr);
        sprintf (aufparr[idx].ls_lad_dm, "%6.2lf",      retp.ret_lad_pr);
        sprintf (aufparr[idx].ls_vk_euro, preispicture, retp.ret_vk_euro);
        sprintf (aufparr[idx].ls_lad_euro, "%6.2lf",    retp.ret_lad_euro);
        sprintf (aufparr[idx].ls_vk_fremd,preispicture, retp.ret_vk_fremd);
        sprintf (aufparr[idx].ls_lad_fremd, "%6.2lf",   retp.ret_lad_fremd);
		memcpy (&aufps, &aufparr[idx], sizeof (struct AUFP_S));
	    FillAktWaehrung ();
		memcpy (&aufparr[idx], &aufps, sizeof (struct AUFP_S));
		char retp_kz[10];
		sprintf (retp_kz, "%hd", retp.retp_kz);
        if (ptab_class.lese_ptab ("retp_kz", retp_kz) == 0)
		{
			sprintf (aufparr[idx].retp_kz, "%hd %s", retp.retp_kz, ptabn.ptbezk);
		}
		else
		{
			sprintf (aufparr[idx].retp_kz, "%hd %s", 0, "kein Grund");
		}

        return 0;
}

BOOL LeseRetpUpd (void)
/**
Daten aus lsp in aufarr einlesen.
**/
{
        extern short sql_mode;
        aufpanz = 0;
        aufpidx = 0;

	    static int cursor = -1;

		retp.mdn = akt_mdn;
		retp.fil = akt_fil;
		retp.ret = retk.ret;
		if (cursor == -1)
		{
			Retp.dbreadfirst ();
			DbClass.sqlin  ((short *)   &retp.mdn,  1, 0);
			DbClass.sqlin  ((short *)   &retp.fil,  1, 0);
			DbClass.sqlin  ((long *)    &retp.ret,  2, 0);
			DbClass.sqlout ((long *)    &retp.posi, 2, 0);
			DbClass.sqlout ((double *)  &retp.a,    3, 0);
			cursor = DbClass.sqlcursor ("select posi, a from retp "
				                        "where mdn = ? "
										"and fil = ? "
										"and ret = ? "
										"order by posi,a");
			if (cursor < 0)
			{
				print_messG (2, "Fehler beim Lesen in retp");
				ExitProcess (1);
			}
		}
        sql_mode = 1;
		dsqlstatus = DbClass.sqlopen (cursor);
		if (dsqlstatus) return FALSE;
        dsqlstatus = DbClass.sqlfetch (cursor);
        while (dsqlstatus == 0)
        {
                   if (Retp.dblock () != 0)
                   {
					   DbClass.sqlclose (cursor);
					   cursor = -1;
                       return FALSE;
                   }
		           if (Retp.dbreadfirst () != 0)
				   {
                       dsqlstatus = DbClass.sqlfetch (cursor);
                       continue;
				   }
                   if (retptoarr (aufpanz) != 0)
                   {
                       dsqlstatus = DbClass.sqlfetch (cursor);
                       continue;
                   }

                   aufpanz ++;
                   if (aufpanz == AUFMAX) break;
                    dsqlstatus = DbClass.sqlfetch (cursor);
        }
        aufpanz_upd = aufpanz;
		DbClass.sqlclose (cursor);
		cursor = -1;
        return TRUE;
}

void LeseRetp (void)
/**
Daten aus lsp in aufarr einlesen.
**/
{
	    static int cursor = -1;

		retp.mdn = akt_mdn;
		retp.fil = akt_fil;
		retp.ret = retk.ret;
		if (cursor == -1)
		{
			Retp.dbreadfirst ();
			DbClass.sqlin  ((short *)   &retp.mdn,  1, 0);
			DbClass.sqlin  ((short *)   &retp.fil,  1, 0);
			DbClass.sqlin  ((long *)    &retp.ret,  2, 0);
			DbClass.sqlout ((long *)    &retp.posi, 2, 0);
			DbClass.sqlout ((double *)  &retp.a,    3, 0);
			cursor = DbClass.sqlcursor ("select posi, a from retp "
				                        "where mdn = ? "
										"and fil = ? "
										"and ret = ? "
										"order by posi,a");
			if (cursor < 0)
			{
				print_messG (2, "Fehler beim Lesen in retp");
				ExitProcess (1);
			}
		}
        aufpanz = 0;
        aufpidx = 0;
		memcpy (&aufps, &aufps_null, sizeof (struct AUFP_S));
		memcpy (&aufparr[0], &aufps_null, sizeof (struct AUFP_S));
		dsqlstatus = DbClass.sqlopen (cursor);
		if (dsqlstatus) return;
        dsqlstatus = DbClass.sqlfetch (cursor);
        while (dsqlstatus == 0)
        {
                   dsqlstatus = Retp.dbreadfirst ();
				   if (dsqlstatus != 0)
				   {
                       dsqlstatus = DbClass.sqlfetch (cursor);
                       continue;
				   }
                   if (retptoarr (aufpanz) != 0)
                   {
                       dsqlstatus = DbClass.sqlfetch (cursor);
                       continue;
                   }

                   aufpanz ++;
                   if (aufpanz == AUFMAX) break;
                    dsqlstatus = DbClass.sqlfetch (cursor);
        }
		DbClass.sqlclose (cursor);
		cursor = -1;
}

void RegisterListe (void)
/**
Kopf - und Fuss - Fenster fuer Liste registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, FITICON);
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  ListKopfProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "ListKopf";
                   RegisterClass(&wc);
                   registered = 1;
        }
}


LONG FAR PASCAL ListKopfProc(HWND hWnd,UINT msg,
                          WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == eKopfWindow)
                    {
                             SetListEWindow (0);
                             display_form (eKopfWindow, &lKopfform, 0, 0);
//                             SetListEWindow (1);
                    }
                    if (hWnd == eFussWindow)
                    {
                             display_form (eFussWindow, &lFussform, 0, 0);
                    }
                    if (hWnd == InsWindow)
                    {
                             SetListEWindow (0);
                             display_form (InsWindow, &insub, 0, 0);
                             display_form (InsWindow, &insform, 0, 0);
//                             SetListEWindow (1);
                    }
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

BOOL ListRowColor (COLORREF *fcolor, COLORREF *bcolor, int idx)
/**
Farbe fuer Positionszeile ermitteln.
**/
{

	     if (atoi (aufparr [idx].s) > 2)
		 {
			 *fcolor = KompfColor;
			 *bcolor = KompbColor;
			 return TRUE;
		 }
		 else if (atoi (aufparr [idx].sa_kz_sint) != 0)
		 {


			 *fcolor = SafColor;
			 *bcolor = SabColor;

			 return TRUE;
		 }
		 return FALSE;
}



void EnterListe (void)
/**
Listenerfassung
**/
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HCURSOR oldcursor;
        static long ret_alt = 0;
        HWND phWnd;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        if (retk.ret == 0l) return;

        if (LiefEnterAktiv)
        {
            lKopfform.mask[0].feld = "Retoure";
            lKopfform.mask[0].length = 0;
            lKopfform.mask[1].feld = lief_nr;
            lKopfform.mask[1].pos[1] = 160;
        }
        else
        {
            lKopfform.mask[0].feld = "Auftrag";
            lKopfform.mask[0].length = 0;
            lKopfform.mask[1].feld = auftrag_nr;
            lKopfform.mask[1].pos[1] = 100;
        }

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
        ActivateColButton (eFussWindow, &lFussform, 2, 0, 0);

        if (AufKopfWindow)
        {
                   phWnd = AufKopfWindow;
        }
        else
        {
                   phWnd = LogoWindow;
        }
        SetActiveWindow (phWnd);
        SetAktivWindow (phWnd);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        if (retk.ret != ret_alt)
        {
                       LeseRetp ();
        }

        RegisterNumEnter ();
        RegisterListe ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&testform, testform.font, &frmrect);

        x = rect.left + 5;
        y = rect.top + 60;
//        cx = frmrect.right + 15;
//        cy = rect.bottom - rect.top - 200;
        cx = (int) (double) ((double) frmrect.right + 15 * scrfx);
        cy = (int) (double) ((double) rect.bottom - rect.top - 200 * scrfy);

        if (Scannen)
        {
                BuSys.text1 = "Scannen";
        }
        else
        {
                BuSys.text1 = "Ger�t";
        }


        SetAktivWindow (phWnd);
        set_fkt (endlist, 5);
        set_fkt (doinsert, 6);
        set_fkt (Nachliefern, 7);
        set_fkt (Nichtliefern, 8);
        set_fkt (Auszeichnen, 9);
        set_fkt (SysChoise, 11);
        set_fkt (doPosLeer, 4);
        set_fkt (SetKomplett, 12);
        SetDblClck (IsDblClck, 0);
        ListeAktiv = 1;
        SetListFont (TRUE);
        spezfont (testform.font);
        eKopfWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_DLGFRAME,
                                    x, y - 35,
                                    (int) ((double) cx + 24 * scrfx),
									(int) ((double) 36 * scrfy),
                                    phWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);

        eWindow = OpenListWindowEnEx (x, y, cx, cy);

        GetWindowRect (eWindow, &rect);
        eFussWindow = CreateWindow ("ListKopf",
                                    "",
                                    WS_POPUP | WS_DLGFRAME,
                                    x, rect.bottom - 2,
                                    (int) ((double) cx + 24 * scrfx),
									(int) ((double) 50 * scrfy),
                                    phWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        ShowWindow (eKopfWindow, SW_SHOWNORMAL);
        ShowWindow (eFussWindow, SW_SHOWNORMAL);
        ElistUb (&testub);
        ElistVl (&testvl);
		SetPosColorProc (ListRowColor);
        ActivateColButton (eFussWindow, &lFussform, 3, -1, 1);
        ShowElist ((char *) aufparr,
                    aufpanz,
                  (char *) &aufps,
                  (int) sizeof (struct AUFP_S),
                  &testform);
        SetCursor (oldcursor);
        EnterElist (eWindow, (char *) aufparr,
                             aufpanz,
                             (char *) &aufps,
                             (int) sizeof (struct AUFP_S),
                             &testform);
         NewPosi ();
         CloseEWindow (eWindow);
         CloseControls (&lKopfform);
         CloseControls (&lFussform);
         CloseControls (&fLeerGut);
         if (testub.mask[0].attribut & BUTTON)
         {
                    CloseControls (&testub);
         }
         DestroyWindow (eFussWindow);
         DestroyWindow (eKopfWindow);
         set_fkt (NULL, 4);
         set_fkt (NULL, 5);
         set_fkt (NULL, 6);
         set_fkt (NULL, 7);
         set_fkt (NULL, 8);
         set_fkt (NULL, 9);
         set_fkt (NULL, 11);
         set_fkt (NULL, 12);
         SetListFont (FALSE);
         ListeAktiv = 0;
         ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
         SetDblClck (NULL, 1);
//         if (mit_trans)  commitwork ();
         SetMenue (&smenue);
         SethListBox (shListBox);
         Setlbox (slbox);
         set_fkt (menfunc2, 6);
		 SetPosColorProc (NULL);
 		 ColorProc = NULL;
}


/** Auswahl ueber Auftraege                                 */

struct AUF_S
{
        char ret [10];
        char kun [10];
        char kun_krz1 [18];
        char lieferdat [12];
        char komm_dat [12];
        char ret_stat [3];
		int  idx;
		char wieg_kompl [3];
};

struct AUF_S aufs, aufs_null;
struct AUF_S aufsarr [AUFMAX];
int aufanz = 0;
int aufidx = 0;

static int aufsort ();
static int kunsort ();
static int kunnsort ();
static int datsort ();
static int kdatsort ();
static int statsort ();

static field _aufform [] =
{
        aufs.ret,          9, 1, 0, 0, 0, "%8d", READONLY, 0, 0, 0,
        aufs.kun,          9, 1, 0, 10,0, "%8d", READONLY, 0, 0, 0,
        aufs.kun_krz1,    17, 1, 0, 21,0, "",    READONLY, 0, 0, 0,
        aufs.lieferdat,    9, 1, 0, 40,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.komm_dat,     9, 1, 0, 49,0, "dd.mm.yy", READONLY, 0, 0, 0,
        aufs.ret_stat,      2, 1, 0, 58, 0, "%1d", READONLY, 0, 0, 0
};


static form aufform = { 6, 0, 0,_aufform, 0, 0, 0, 0, &ListFont};

/*
static field _aufub [] = {
"Auftrag",            9, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0,
"Kunden-Nr",         10, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
"Kunden-Name",       17, 1, 0,21, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferdatum",       11, 1, 0,40, 0, "", DISPLAYONLY, 0, 0, 0,
"S",                  1, 1, 0,57, 0, "", DISPLAYONLY, 0, 0, 0
};
*/

static field _aufub [] = {
"Auftrag",            9, 1, 0, 0, 0, "", BUTTON, 0, aufsort,  102,
"Kunden-Nr",         11, 1, 0, 9, 0, "", BUTTON, 0, kunsort,  103,
"Kunden-Name",       20, 1, 0,20, 0, "", BUTTON, 0, kunnsort, 104,
"L-Datum",            9, 1, 0,40, 0, "", BUTTON, 0, datsort,  105,
"K-Datum",            9, 1, 0,49, 0, "", BUTTON, 0, kdatsort,  105,
"S",                  2, 1, 0,58, 0, "", BUTTON, 0, statsort, 106,
" ",                  4, 1, 0,60, 0, "", BUTTON, 0, 0, 0,
};

static form aufub = { 6, 0, 0,_aufub, 0, 0, 0, 0, &ListFont};

static field _aufvl [] =
{      "1",                  1,  0, 0,  9, 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 20 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 40 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 49 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 58 , 0, "", NORMAL, 0, 0, 0,
       "1",                  1,  0, 0, 60 , 0, "", NORMAL, 0, 0, 0,
};
static form aufvl = {6, 0, 0,_aufvl, 0, 0, 0, 0, &ListFont};


static int faufsort = 1;
static int fkunsort = 1;
static int fkunnsort = 1;
static int fdatsort = 1;
static int fkdatsort = 1;
static int fstatsort = 1;

static int aufsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	      return ((int) (atol(el1->ret) - atol (el2->ret)) *
				                                  faufsort);
}


static int aufsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   aufsort0);
		  faufsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}

static int kunsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atol(el1->kun) - atol (el2->kun)) *
		                                  fkunsort);
}

static int kunsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	      qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				    kunsort0);
		fkunsort *= -1;
            ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		return 0;
}


static int kunnsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) strcmp(el1->kun_krz1, el2->kun_krz1) *
				                                  fkunnsort);
}

static int kunnsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   kunnsort0);
 		  fkunnsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


static int datsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;
              long dat1;
              long dat2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
              dat1 = dasc_to_long (el1->lieferdat);
              dat2 = dasc_to_long (el2->lieferdat);
              if (dat1 > dat2)
              {
                            return  (1 * fdatsort);
              }
              else
              if (dat1 < dat2)
              {
                            return  (-1 * fdatsort);
              }
              return 0;
}

static int datsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   datsort0);
		  fdatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}
static int kdatsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;
              long dat1;
              long dat2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
              dat1 = dasc_to_long (el1->komm_dat);
              dat2 = dasc_to_long (el2->komm_dat);
              if (dat1 > dat2)
              {
                            return  (1 * fkdatsort);
              }
              else
              if (dat1 < dat2)
              {
                            return  (-1 * fkdatsort);
              }
              return 0;
}

static int kdatsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   datsort0);
		  fkdatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


static int statsort0 (const void *elem1, const void *elem2)
{
	        struct AUF_S *el1;
	        struct AUF_S *el2;

		  el1 = (struct AUF_S *) elem1;
		  el2 = (struct AUF_S *) elem2;
	        return ((int) (atoi(el1->ret_stat) - atoi (el2->ret_stat)) *
		                                  fstatsort);
}

static int statsort (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (aufsarr, aufanz, sizeof (struct AUF_S),
				   statsort0);
		  fstatsort *= -1;
              ShowNewElist ((char *) aufsarr,
                             aufanz,
                            (int) sizeof (struct AUF_S));
		  return 0;
}


void IsAwClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        aufidx = idx;
        break_list ();
        return;
}

int GetKunFil (void)
/**
Feld kun_fil aus lsk lesen.
**/
{
	    static int cursor = -1;
		static short kun_fil;

		if (cursor == -1)
		{
			DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
			DbClass.sqlin ((long *)  &lsk.auf,  2, 0);
			DbClass.sqlout ((short *) &kun_fil, 1, 0);
			cursor = DbClass.sqlcursor ("select kun_fil from lsk where mdn = ? "
				                        "and auf = ?");
			if (cursor == -1) return 0;
		}
		DbClass.sqlopen (cursor);
		dsqlstatus = DbClass.sqlfetch (cursor);
		if (dsqlstatus != 0) return 0;
		return kun_fil;
}


void lese_kunfil (short mdn, short fil, long kun_nr)
/**
Kunde oder Filiale lesen.
**/
{
	    int kunfil;
		int dsqlstatus;

		strcpy (kun.kun_krz1, " ");
		lsk.mdn = mdn;
	    kunfil = GetKunFil ();
	    if (kunfil == 0)
		{
                kun_class.lese_kun (mdn, fil, lsk.kun);
		}
		else if (kunfil == 1)
		{
                dsqlstatus = fil_class.lese_fil (mdn, (short) lsk.kun);
                if (dsqlstatus == 0)
                 {
                      adr_class.lese_adr (_fil.adr);
                      strcpy (kun.kun_krz1, _adr.adr_krz);
				}
		}
}


long FetchTou (void)
/**
Tour holen.
**/
{
		static long tou = 0;
		int dsqlstatus;

		lsk.tou = 0l;
        DbClass.sqlout ((long *) &lsk.tou, 2, 0);
        DbClass.sqlin  ((short *) &akt_mdn, 1, 0);
        DbClass.sqlin  ((short *) &akt_fil, 1, 0);
        DbClass.sqlin  ((long *) &lsk.auf, 2, 0);
	    dsqlstatus = DbClass.sqlcomm ("select tou from lsk "
				                           "where mdn = ? "
										   "and fil = ? "
										   "and auf = ?");
		tou = lsk.tou;
        return tou;
}

BOOL TestTou (void)
{
	    if (tour_kz == 0) return TRUE;
		if (EnterTyp == SUCHEN) return TRUE;
		lsk.tou = FetchTou ();
	 	if (tourlang == 4)
		{
				lsk.tou /= 100;
		}
		if (tourlang == 5)
		{
				lsk.tou /= 1000;
		}
		if (atol (AktTour) != lsk.tou)
		{
				return FALSE;
		}
		return TRUE;
}


BOOL LskRowColor (COLORREF *fcolor, COLORREF *bcolor, int idx)
/**
Farbe fuer Positionszeile ermitteln.
**/
{

	     if (atoi (aufsarr [idx].wieg_kompl) == 1)
		 {
			 *fcolor = KompfColor;
			 *bcolor = KompbColor;
			 return TRUE;
		 }
		 return FALSE;
}


static int DbAuswahl (short cursor,void (*fillub) (char *),
                      void (*fillvalues) (char *))
/**
Auswahl ueber Auftraege.
**/
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        static long ret_alt = 0;
        HCURSOR oldcursor;
		int scrollpos;

	    SetPosColorProc (LskRowColor);
        aufanz = scrollpos = 0;
        oldcursor = SetCursor (LoadCursor (NULL, IDC_WAIT));
        while (fetch_scroll (cursor, NEXT) == 0)
        {
				if (TestTou () == FALSE)
				{
					scrollpos ++;
					continue;
				}
			    lese_kunfil (akt_mdn, akt_fil, lsk.kun);
                kun_class.lese_kun (akt_mdn, akt_fil, lsk.kun);
                sprintf (aufsarr[aufanz].ret, "%8ld",  retk.ret);
                sprintf (aufsarr[aufanz].kun, "%8ld",  retk.kun);
                dlong_to_asc (lsk.lieferdat, aufsarr[aufanz].lieferdat);
                dlong_to_asc (lsk.komm_dat, aufsarr[aufanz].komm_dat);
                strcpy  (aufsarr[aufanz].kun_krz1, kun.kun_krz1);
                sprintf (aufsarr [aufanz].ret_stat, "%1hd", retk.ret_stat);
				if (lsk.wieg_kompl < 0) lsk.wieg_kompl = 0;
                sprintf (aufsarr [aufanz].wieg_kompl, "%1hd", lsk.wieg_kompl);
				aufsarr [aufanz].idx = scrollpos;
				scrollpos ++;
                aufanz ++;
                if (aufanz == AUFMAX) break;
        }

        RegisterNumEnter ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (&testform, testform.font, &frmrect);

        x = rect.left + 5;
        y =  rect.top + 20;
        cx = (int) (double) ((double) frmrect.right + 15 * scrfx);
        cy = (int) (double) ((double) rect.bottom - rect.top - 120 * scrfy);

        set_fkt (endlist, 5);
        syskey = 1;
        SetDblClck (IsAwClck, 0);
        ListeAktiv = 2;
        SetAktivWindow (hMainWindow);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (testform.font);
        eWindow = OpenListWindowEnEx (x, y, cx, cy);
        ElistVl (&aufvl);
        ElistUb (&aufub);
        ShowElist ((char *) aufsarr,
                    aufanz,
                  (char *) &aufs,
                  (int) sizeof (struct AUF_S),
                  &aufform);
        SetCursor (oldcursor);
        EnterElist (eWindow, (char *) aufsarr,
                             aufanz,
                             (char *) &aufs,
                             (int) sizeof (struct AUF_S),
                             &aufform);
        if (aufub.mask[0].attribut & BUTTON)
        {
                    CloseControls (&aufub);
        }
        CloseEWindow (eWindow);
        set_fkt (NULL, 5);
        SetListFont (FALSE);
        ListeAktiv = 0;
        SetDblClck (NULL, 1);
	    SetPosColorProc (NULL);
        return aufsarr [aufidx].idx;
}

/**

  Auftragskopf anzeigen und Eingabefelder waehlen.

**/

mfont AufKopfFontT = {"Courier New", 200, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont AufKopfFontE = {"Courier New", 200, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


mfont AufKopfFontTD = {"Courier New", 150, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont AufKopfFontED = {"Courier New", 150, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


static char KundeFilialeK [14] = {"Kunden-Nr   :"};
static char KundeFilialeF [14] = {"Filial-Nr   :"};
static char KundeFiliale [14] = {"Kunden-Nr   :"};
static int ReadAuftrag (void);
static int AufAuswahl (void);
static int BreakAuswahl (void);

static field _AufKopfT[] = {
"Auftrag-Nr  :",     12, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0};

static form AufKopfT = {1, 0, 0, _AufKopfT, 0, 0, 0, 0, &AufKopfFontT};


static field _AufKopfTD[] = {
KundeFiliale,        13, 0, 4, 2, 0, "", DISPLAYONLY, 0, 0, 0,
kun_krz1,            16, 0, 4,28, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferdatum :",     13, 0, 5, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Komm.-Datum :",     13, 0, 6, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Lieferzeit  :",     13, 0, 7, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Status      :",     13, 0, 8, 2, 0, "", DISPLAYONLY, 0, 0, 0,
 ret_stat_txt,        16, 0, 8,28, 0, "", DISPLAYONLY,        0, 0, 0
};

static form AufKopfTD = {7,0, 0, _AufKopfTD, 0, 0, 0, 0, &AufKopfFontTD};


static field _AufKopfE[] = {
  auftrag_nr,           9, 0, 1,18, 0, "%8d", EDIT,        0, ReadAuftrag, 0};

form AufKopfE = {1, 0, 0, _AufKopfE, 0, 0, 0, 0, &AufKopfFontE};


static field _AufKopfED[] = {
  kun_nr,              20, 0, 4,18, 0, "%8d", EDIT,        0, 0, 0,
  lieferdatum,         20, 0, 5,18, 0, "dd.mm.yyyy",
                                              EDIT,        0, 0, 0,
  komm_dat,            20, 0, 6,18, 0, "dd.mm.yyyy",
                                              EDIT,        0, 0, 0,
  lieferzeit,          20, 0, 7,18, 0, "",
                                              EDIT,        0, 0, 0,
  ret_stat,             20, 0, 8,18, 0, "",
                                              EDIT,        0, BreakAuswahl, 0
};

form AufKopfED = {5, 0, 0, _AufKopfED, 0, 0, 0, 0, &AufKopfFontED};

static field _AufKopfQuery[] = {
  auftrag_nr,           9, 0, 1,18, 0, "", EDIT,        0, 0, 0,
  kun_nr,               9, 0, 4,18, 0, "", EDIT,        0, 0, 0,
  lieferdatum,         11, 0, 5,18, 0, "",
                                           EDIT,        0, 0, 0,
  komm_dat,            11, 0, 6,18, 0, "",
                                           EDIT,        0, 0, 0,
  lieferzeit,           6, 0, 7,18, 0, "",
                                              EDIT,        0, 0, 0,
  ret_stat,              2, 0, 8,18, 0, "",
                                              EDIT,        0, 0, 0,
  ret_stat5,             2, 0, 8,18, 0, "",
                                              REMOVED,     0, 0, 0,
};

static form AufKopfQuery = {7, 0, 0, _AufKopfQuery, 0, 0, 0, 0, 0};
static char *AufKopfnamen[] = {"auf", "kun", "lieferdat", "komm_dat",
                                     "lieferzeit",
                                     "ret_stat","ret_stat"};

static int break_kopf_enter = 0;
static int aspos = 0;
static int asend = 4;

int BreakAuswahl (void)
/**
Auswahl ueber Auftraege.
**/
{
        break_kopf_enter = 1;
        return 0;
}


int AufAuswahl (void)
/**
Auswahl ueber Auftraege.
**/
{
        SetDbAwSpez (DbAuswahl);
        break_kopf_enter = 1;
        ReadQuery (&AufKopfQuery, AufKopfnamen);
        if (ls_class.Auswahl_lsk_aufQuery (1) == 0)
        {
                     sprintf (auftrag_nr, "%ld", lsk.auf);
                     if (aufanz)
                     {
                               LockAuf = 0;
                               ReadAuftrag ();
                               LockAuf = 1;
                     }
                     else
                     {
                               InitAufKopf ();
                     }
                     SetDbAwSpez (NULL);
                     return 0;
        }
        InitAufKopf ();
        SetDbAwSpez (NULL);
        return 0;
}

void FillAufForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{

             kun_krz1[0] = (char) 0;
             if (lsk.kun_fil == 0)
			 {
                  dsqlstatus = kun_class.lese_kun (akt_mdn, akt_fil, atol (kun_nr));
                  if (dsqlstatus == 0)
                  {
                         strcpy (kun_krz1, kun.kun_krz1);
                         strcpy (lsk.kun_krz1,kun.kun_krz1);
                         adr_class.lese_adr (kun.adr2);
						 strcpy (KundeFiliale, KundeFilialeK);
//                         FillLskAdr ();
                  }
			 }
             else
			 {
                  dsqlstatus = fil_class.lese_fil (1, (short) atol (kun_nr));
                  if (dsqlstatus == 0)
                  {
                      adr_class.lese_adr (_fil.adr);
                      strcpy (kun_krz1, _adr.adr_krz);
                      strcpy (kun.kun_krz1, _adr.adr_krz);
                      strcpy (lsk.kun_krz1, _adr.adr_krz);
					  strcpy (KundeFiliale, KundeFilialeF);
//                      FillLskAdr ();
                  }
			 }

             sprintf (kun_nr, "%ld", lsk.kun);
             dlong_to_asc (lsk.lieferdat, lieferdatum);
             dlong_to_asc (lsk.komm_dat, komm_dat);
             strcpy (lieferzeit, lsk.lieferzeit);
             sprintf (ret_stat, "%hd", retk.ret_stat);
             strcpy (kun_krz1, kun.kun_krz1);
             ptab_class.lese_ptab ("ret_stat", ret_stat);
             strcpy (ret_stat_txt, ptabn.ptbezk);
             sprintf (ret_stat_txt, ptabn.ptbezk);
}

BOOL Lock_Retk (void)
/**
Lieferschein sperren.
**/
{
    short sqlm;
    extern short sql_mode;

    if (LockAuf == 0)
        return TRUE;
    sqlm = sql_mode;
    sql_mode = 1;

    retk.mdn = akt_mdn;
    retk.fil = akt_fil;
    dsqlstatus = Retk.dbreadfirst ();
    dsqlstatus = Retk.dblock ();
    if (dsqlstatus < 0)
    {
        if (atol (auftrag_nr))
        {
               print_messG (2, "Auftrag %ld ist gesperrt",
                           atol (auftrag_nr));
        }
        else
        {
               print_messG (2, "Retoure %ld ist gesperrt",
                               lsk.ls);
        }
        sql_mode = sqlm;
        AufLock = 1;
        ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
        return FALSE;
    }
    AufLock = 0;
    ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
    sql_mode = sqlm;
    return TRUE;
}

BOOL Lock_Lsp (void)
/**
Lieferschein sperren.
**/
{
    short sqlm;
    extern short sql_mode;

    sqlm = sql_mode;
    sql_mode = 1;
    dsqlstatus = ls_class.lock_lsp (0,akt_mdn, akt_fil, lsp.ls, ratod (aufps.a),
                                                  atol (aufps.pnr));
    if (dsqlstatus < 0)
    {
               print_messG (2, "Position wird von einem anderen "
                               "Benutzer bearbeitet");
               return FALSE;
    }
    sql_mode = sqlm;
    return TRUE;
}


int ReadAuftrag (void)
/**
Lieferschein nach Auftragsnummer lesen.
**/
{
            if (atol (auftrag_nr) <= 0l) return (-1);

            dsqlstatus = ls_class.lese_lsk_auf (akt_mdn, akt_fil, atol (auftrag_nr));
            if (dsqlstatus)
            {
                   ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
                   set_fkt (NULL, 6);
                   print_messG (0, "Auftrag %s nicht gefunden", auftrag_nr);
                   return -1;
            }

            if (lsk.auf_art == 9999)
			{
				  disp_messG ("Achtung ! Vorratsauftrag", 2);
			}
//            if (Lock_Lsk () == FALSE) return 0;

			sprintf (kun_nr, "%ld",  lsk.kun);
            ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
            set_fkt (menfunc2, 6);
            FillAufForm ();
//            display_form (AufKopfWindow, &AufKopfED, 0, 0);
//            display_form (AufKopfWindow, &AufKopfTD, 0, 0);
            return 0;
}


void KonvTextPos (mfont *Font1,  mfont *Font2, int *z, int *s)
/**
Textposition von einer Schriftgroesse zur anderen konvertieren.
**/
{
         TEXTMETRIC tm1, tm2;
         HDC hdc;
         HFONT hFont, oldfont;
         int ax, ay;

         spezfont (Font1);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm1);
         DelFont (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
//         DeleteObject (hFont);

         spezfont (Font2);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm2);
         DelFont (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
//         DeleteObject (hFont);

         ax = *s;
         ay = *z;

         ax = ax * tm1.tmAveCharWidth / tm2.tmAveCharWidth;
         ay = ay * tm1.tmHeight / tm2.tmHeight;

         *s = ax;
         *z = ay;
}

void KonvTextAbsPos (mfont *Font, int *cy, int *cx)
/**
Absolute Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DelFont (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
//         DeleteObject (hFont);
         *cx *= tm.tmAveCharWidth;
         *cy *= tm.tmHeight;
}

void GetTextPos (mfont *Font, int *cy, int *cx)
/**
Zeichenorientierte Textposition fuer eine Schriftgroesse ermitteln.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DelFont (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
//         DeleteObject (hFont);

         *cx /= tm.tmAveCharWidth;
         *cy /= tm.tmHeight;
}



void FontTextLen (mfont *Font, char *text, int *cx, int *cy)
/**
Laenge und Hoehe eines Textes fuer den gewaehlten Font holen.
**/
{
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont, oldfont;
         SIZE size;

         spezfont (Font);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         GetTextExtentPoint (hdc, text, *cx, &size);
         DelFont (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);
//         DeleteObject (hFont);

         *cx = size.cx;
         *cy = tm.tmHeight;
}

void SetAufFocus (int pos)
/**
Focus auf Eingabefelder AuftragKopfsetzen.
**/
{
        switch (pos)
        {
               case 0 :
                      SetFocus (AufKopfE.mask[0].feldid);
                      SendMessage (AufKopfE.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               default :
                      SetFocus (AufKopfED.mask[pos - 1].feldid);
                      SendMessage (AufKopfED.mask[pos -1].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
        }
}

void PrintKLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
         RECT rect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen;
         int x, y;
		 HPEN oldpen;

         GetClientRect (AufKopfWindow, &rect);

         hdc = BeginPaint (AufKopfWindow, &ps);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         oldpen = SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 10;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         oldpen = SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);


         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         oldpen = SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 100;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         oldpen = SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         oldpen = SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 300;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         oldpen = SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}

static int NoDisplay;


LONG FAR PASCAL AufKopfProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (LiefEnterAktiv)
                    {
                                DisplayRetKopf ();
                                PrintKLines ();
                                if (MessWindow == 0)
                                {
                                        LiefFocus ();
                                }
                    }
                    else if (KopfEnterAktiv)
                    {
                                display_form (AufKopfWindow, &AufKopfT, 0, 0);
                                display_form (AufKopfWindow, &AufKopfTD, 0, 0);
                                if (AufKopfE.mask[0].feldid)
                                {
                                      display_form (AufKopfWindow, &AufKopfE, 0, 0);
                                }
                                if (AufKopfE.mask[0].feldid || asend == 0)
                                {
                                      display_form (AufKopfWindow, &AufKopfED, 0, 0);
                                }
                                PrintKLines ();
                                if (MessWindow == 0)
                                {
                                         SetAufFocus (aspos);
                                }
                    }
                    break;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterKopfEnter (void)
/**
Fenster fuer Auftragskopf registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, FITICON);
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  AufKopfProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
                   wc.lpszClassName =  "AufKopf";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

void GetEditText (void)
/**
Text aus Editierfeld holen.
**/
{
        switch (aspos)
        {
                 case 0 :
                    GetWindowText (AufKopfE.mask [0].feldid,
                                   AufKopfE.mask [0].feld,
                                   AufKopfE.mask[0].length);
                    break;
                 case 1 :
                    GetWindowText (AufKopfED.mask [0].feldid,
                                   AufKopfED.mask [0].feld,
                                   AufKopfED.mask[0].length);
                    break;
                 case 2 :
                    GetWindowText (AufKopfED.mask [1].feldid,
                                   AufKopfED.mask [1].feld,
                                   AufKopfED.mask[1].length);
                    break;
                 case 3 :
                    GetWindowText (AufKopfED.mask [2].feldid,
                                   AufKopfED.mask [2].feld,
                                   AufKopfED.mask[2].length);
                    break;
                 case 4 :
                    GetWindowText (AufKopfED.mask [3].feldid,
                                   AufKopfED.mask [3].feld,
                                   AufKopfED.mask[3].length);

                 case 5 :
                    GetWindowText (AufKopfED.mask [4].feldid,
                                   AufKopfED.mask [4].feld,
                                   AufKopfED.mask[4].length);
                    break;
        }
}

int MenKey (int taste)
{
    int i;
    ColButton *ColBut;
    char *pos;

    for (i = 0; i < MainButton.fieldanz; i ++)
    {
           ColBut = (ColButton *) MainButton.mask[i].feld;
           if (ColBut->aktivate < 0) continue;
           if (ColBut->text1)
           {
                 if ((pos = strchr (ColBut->text1, '&')) &&
                    ((UCHAR) toupper (*(pos + 1)) == taste))
                 {
                             if (MainButton.mask[i].after)
                             {
                                 (*MainButton.mask[i].after) ();
                             }
                             return TRUE;
                  }
           }
    }
    return FALSE;
}

int IsKopfMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    break_kopf_enter = 1;
                                    return TRUE;
                            case VK_F6 :
                                    syskey = KEY6;
                                    testkeys ();
                                    return TRUE;
                            case VK_F12 :
                                    GetEditText ();
                                    syskey = KEY12;
                                    testkeys ();
                                    return TRUE;
                            case VK_DOWN :
                            case VK_RETURN :
                                    GetEditText ();
                                    if (asend == 0 && AufKopfE.mask[0].after)
                                    {
                                         if ((*AufKopfE.mask[0].after) () == -1)
                                         {
                                                  return TRUE;
                                         }
                                    }
                                    if (aspos > 0 &&
                                        AufKopfED.mask [aspos - 1].after)
                                    {
                                      (*AufKopfED.mask [aspos - 1].after) ();
                                    }
                                    aspos ++;
                                    if (aspos > asend) aspos = 0;
                                    display_form (AufKopfWindow,
                                                  &AufKopfE, 0, 0);
                                    display_form (AufKopfWindow,
                                                  &AufKopfED, 0, 0);
                                    SetAufFocus (aspos);
                                    return TRUE;
                            case VK_UP :
                                    GetEditText ();
                                    aspos --;
                                    if (aspos < 0) aspos = asend;
                                    display_form (AufKopfWindow,
                                                  &AufKopfE, 0, 0);
                                    display_form (AufKopfWindow,
                                                  &AufKopfED, 0, 0);
                                    SetAufFocus (aspos);
                                    return TRUE;
                            case VK_TAB :
                                    GetEditText ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                            aspos --;
                                            if (aspos < 0) aspos = 4;
                                            SetAufFocus (aspos);
                                     }
                                     else
                                     {
                                            aspos ++;
                                            if (aspos > 4) aspos = 0;
                                            SetAufFocus (aspos);
                                     }
                                     display_form (AufKopfWindow,
                                                 &AufKopfE, 0, 0);
                                     display_form (AufKopfWindow,
                                                 &AufKopfED, 0, 0);
                                      return TRUE;
                            default :
                                    return MenKey (msg->wParam);
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (AufKopfE.mask[0].feldid, &mousepos))
                    {
                          EnterNumBox (AufKopfWindow, "  Auftrags-Nr  ",
                                       auftrag_nr, 9, AufKopfE.mask[0].picture);
                          if (asend == 0 && AufKopfE.mask[0].after)
                          {
                                if ((*AufKopfE.mask[0].after) () == -1)
                                {
                                       return TRUE;
                                }
                          }
                          display_form (AufKopfWindow, &AufKopfE, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[0].feldid, &mousepos))
                    {
                          if (asend == 0) return TRUE;
                          EnterNumBox (AufKopfWindow,"  Kunden-Nr  ",
                                          kun_nr, 9, AufKopfED.mask[0].picture);
                          display_form (AufKopfWindow, &AufKopfE, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[1].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Lieferdatum  ",
                                      lieferdatum, 11,
//                                      "dd.mm.yyyy");
                                            AufKopfED.mask[1].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[2].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Komm.-Datum  ",
                                      komm_dat   , 11,
//                                      "dd.mm.yyyy");
                                          AufKopfED.mask[2].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[3].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Lieferzeit  ",
                                      lieferzeit, 6,
                                            AufKopfED.mask[3].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
                         return TRUE;
                    }
                    else if (MouseinWindow (AufKopfED.mask[4].feldid, &mousepos))
                    {
                         if (asend == 0) return TRUE;
                         EnterNumBox (AufKopfWindow,"  Status  ",
                                         ret_stat,  2,
                                            AufKopfED.mask[4].picture);
                         display_form (AufKopfWindow, &AufKopfE, 0, 0);
                         return TRUE;
                    }
             }
          }

          return FALSE;
}

void KonvAufKopf (void)
/**
Spaltenposition von AufKopfED anpassen.
**/
{
        static int KonvOK = 0;
        int z, s;
        int i;

        if (KonvOK) return;

        KonvOK = 1;
        for (i = 0; i < AufKopfED.fieldanz; i ++)
        {
                    s = AufKopfED.mask[i].pos [1];
                    KonvTextPos (AufKopfE.font,
                                 AufKopfED.font,
                                 &z, &s);
                    AufKopfED.mask[i].pos [1] = s;
        }

        s = AufKopfTD.mask[1].pos [1];
        KonvTextPos (AufKopfE.font,
                    AufKopfED.font,
                    &z, &s);
        AufKopfTD.mask[1].pos [1] = s;

        s = AufKopfTD.mask[5].pos [1];
        KonvTextPos (AufKopfE.font,
                    AufKopfED.font,
                    &z, &s);
        AufKopfTD.mask[5].pos [1] = s;
}

void InitAufKopf (void)
/**
Auftrag-Kopf initialisieren.
**/
{
        int i;

        AufKopfE.mask[0].feld[0] = (char) 0;

        for (i = 0; i < AufKopfED.fieldanz; i ++)
        {
                      AufKopfED.mask[i].feld[0] = (char) 0;
        }
        kun_krz1[0] = (char) 0;
        ret_stat_txt[0] = (char) 0;
}


void MainBuInactiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 0, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 1, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
/*
          ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 3, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 5, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 6, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 7, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 8, -1, 1);
*/
}

void MainBuActiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (BackWindow2, &MainButton, 0, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 1, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 2, -1, 1);
/*
          ActivateColButton (BackWindow2, &MainButton, 2, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 3, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 5, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 6, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
          ActivateColButton (BackWindow2, &MainButton, 7, 1, 1);
          ActivateColButton (BackWindow2, &MainButton, 8, 1, 1);
*/
          set_fkt (NULL, 6);
}


void AuftragKopf (int eType)
/**
Auftragskopf bearbeiten.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;

        if (KopfEnterAktiv) return;
        if (LiefEnterAktiv) return;

        MainBuInactiv ();

        if (eType == ENTER)
        {
            beginwork ();
        }
        else
        {
                   InitAufKopf ();
        }

        if (atoi (auftrag_nr))
        {
                  if (ReadAuftrag () == -1) return;;
        }
        CloseRetKopf ();
        if (eType == AUSWAHL)
        {
                     KopfEnterAktiv = 2;
                     asend = 5;
//                     InitAufKopf ();
                     AufKopfE.mask[0].picture = "";
                     AufKopfED.mask[0].picture = "";
                     AufKopfED.mask[1].picture = "";
                     AufKopfED.mask[2].picture = "";

                     AufKopfED.mask[0].attribut = EDIT;
                     AufKopfED.mask[1].attribut = EDIT;
                     AufKopfED.mask[2].attribut = EDIT;
                     AufKopfED.mask[3].attribut = EDIT;
                     AufKopfED.mask[4].attribut = EDIT;
                     AufKopfTD.mask[1].attribut = REMOVED;
                     AufKopfTD.mask[6].attribut = REMOVED;

                     AufKopfE.mask[0].length = 20;
                     AufKopfED.mask[0].length = 20;
                     AufKopfED.mask[1].length = 20;
                     AufKopfED.mask[2].length = 20;
                     AufKopfED.mask[3].length = 20;
                     AufKopfED.mask[4].length = 20;
                     strcpy (ret_stat, "< 3");
                     strcpy (komm_dat, KomDatum);
                     AufAuswahl ();
                     KopfEnterAktiv = 0;
                     MainBuActiv ();
                     return;
        }

        if (eType == ENTER)
        {
                     asend = 0;
                     AufKopfE.mask[0].picture  = "%8d";
                     AufKopfED.mask[0].picture = "%8d";
                     AufKopfED.mask[1].picture = "dd.mm.yyyy";
                     AufKopfED.mask[2].picture = "dd.mm.yyyy";

                     AufKopfED.mask[0].attribut = READONLY;
                     AufKopfED.mask[1].attribut = READONLY;
                     AufKopfED.mask[2].attribut = READONLY;
                     AufKopfED.mask[3].attribut = READONLY;
                     AufKopfED.mask[4].attribut = READONLY;
                     AufKopfTD.mask[1].attribut = DISPLAYONLY;
                     AufKopfTD.mask[6].attribut = DISPLAYONLY;

                     AufKopfE.mask[0].length = 9;
                     AufKopfED.mask[0].length = 9;
                     AufKopfED.mask[1].length = 11;
                     AufKopfED.mask[2].length = 11;
                     AufKopfED.mask[3].length = 6;
                     AufKopfED.mask[4].length = 2;
        }
        else if (eType == SUCHEN)
        {
                     asend = 5;
                     set_fkt (BreakAuswahl, 12);
                     InitAufKopf ();
                     AufKopfE.mask[0].picture = "";
                     AufKopfED.mask[0].picture = "";
                     AufKopfED.mask[1].picture = "";
                     AufKopfED.mask[2].picture = "";

                     AufKopfED.mask[0].attribut = EDIT;
                     AufKopfED.mask[1].attribut = EDIT;
                     AufKopfED.mask[2].attribut = EDIT;
                     AufKopfED.mask[3].attribut = EDIT;
                     AufKopfED.mask[4].attribut = EDIT;
                     AufKopfTD.mask[1].attribut = REMOVED;
                     AufKopfTD.mask[6].attribut = REMOVED;

                     AufKopfE.mask[0].length = 20;
                     AufKopfED.mask[0].length = 20;
                     AufKopfED.mask[1].length = 20;
                     AufKopfED.mask[2].length = 20;
                     AufKopfED.mask[3].length = 20;
                     AufKopfED.mask[4].length = 20;
        }

        aspos = 0;
        RegisterKopfEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = rect.top;
        cx = rect.right - rect.left;
        cy = rect.bottom - rect.top;

        KonvAufKopf ();

        KopfEnterAktiv = 1;
        if (AufKopfWindow == 0)
        {
                  AufKopfWindow  = CreateWindowEx (
                              0,
                              "AufKopf",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
        }

        if (AufKopfWindow == NULL)
        {
                    KopfEnterAktiv = 0;
                    MainBuActiv ();
                    return;
        }
        ShowWindow (AufKopfWindow, SW_SHOW);
        UpdateWindow (AufKopfWindow);
        create_enter_form (AufKopfWindow, &AufKopfE, 0, 0);
        if (asend > 0)
        {
                  create_enter_form (AufKopfWindow, &AufKopfED, 0, 0);
        }
        if (eType == ENTER)
        {
               set_fkt (menfunc2, 6);
        }
        SetAufFocus (aspos);
        break_kopf_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsKopfMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_kopf_enter) break;
        }
        CloseControls (&AufKopfT);
        CloseControls (&AufKopfE);
        CloseControls (&AufKopfTD);
        CloseControls (&AufKopfED);
        if (eType == ENTER)
        {
               set_fkt (NULL, 6);
        }

        if (eType == ENTER)
        {
                     DestroyWindow (AufKopfWindow);
                     AufKopfWindow = NULL;
                     commitwork ();
//                     InitAufKopf ();
        }

        if (eType == SUCHEN)
        {
                     set_fkt (NULL, 12);
                     DestroyWindow (AufKopfWindow);
                     AufKopfWindow = NULL;
                     AufAuswahl ();
        }
        KopfEnterAktiv = 0;
        MainBuActiv ();
}

/**

Wiegen

**/


#define BUTARA  502
static int break_wieg = 0;

HWND WiegKg;
HWND WiegCtr;

static char WiegText [30] = {"Kiloware"};
static char WiegMe [12] = {"14.150"};


mfont WiegKgFont  = {"Courier New", 300, 0, 1, BLACKCOL,
                                               WHITECOL,
                                               1,
                                               NULL};

mfont WiegKgFontMe = {"Courier New", 300, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                1,
                                                NULL};


mfont WiegKgFontT  = {"Courier New", 150, 0, 1, WHITECOL,
                                                BLUECOL,
                                                1,
                                                NULL};

mfont WiegBuFontE  = {"Courier New", 200, 0, 1, BLACKCOL,
                                                REDCOL,
                                                1,
                                                NULL};

mfont WiegBuFontW  = {"Courier New", 150, 0, 1, BLACKCOL,
                                                GREENCOL,
                                                1,
                                                NULL};

mfont EtiFont  = {"Courier New", 120, 0, 1, WHITECOL,
                                            BLUECOL,
                                            1,
                                            NULL};


ColButton WieOK = { "OK", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     GREENCOL,
                     2};


ColButton WieEnde = {"Zur�ck", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     REDCOL,
                     2};

ColButton WieWie = {"Wiegen", -1, -1,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     NULL, 0, 0,
                     BLACKCOL,
                     GREENCOL,
                     2};

ColButton WieHand = {"Hand", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      BLACKCOL,
                      YELLOWCOL,
                      2};

ColButton WiePreis = {"Preise", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      BLACKCOL,
                      YELLOWCOL,
                      2};

ColButton WieTara = {"Tara", -1, -1,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton AutoTara = {"Auto-", -1,15,
                      "Tara ", -1,40,
                      "aus",   -1,65,
                       NULL, 0, 0,
                       NULL, 0, 0,
                       WHITECOL,
                       RGB (0, 0, 255),
                       3};

ColButton DelTara = { "Tara   ", -1, 25,
                      "L�schen", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton FestTara = {"Fest", -1, 25,
                      "Tara", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};


ColButton SetTara = { "Tara  ", -1, 15,
                      "�ber- ", -1, 40,
                      "nehmen", -1, 65,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton HandTara = {"Hand", -1, 25,
                      "Tara", -1, 50,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      RGB (0, 0, 255),
                      2};

ColButton Etikett = {"Etikett", -1, -1,
                     "", 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};

ColButton SumEti  = {"Summen", -1,   5,
                     "Etikett",-1 , 30,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      NULL, 0, 0,
                      WHITECOL,
                      BLUECOL,
                      2};


static int current_wieg;
static int CtrMode;
static int wiegend = 6;
static int dowiegen (void);
static int doWOK (void);
static int dohand (void);
static int dotara (void);
static int dogebinde (void);
static int dopreis (void);
static int doauto (void);
static int deltara (void);
static int handtara (void);
static int festtara (void);
static int settara (void);
static int closetara (void);
static int doetikett (void);
static int dosumeti (void);
static int autoan = 0;
static int autopos = 1;
static char *anaus[] = {"aus", "ein"};
static char chargen_nr [18];
static int WiegY;
static double akt_tara = (double) 0.0;
static char last_charge [21] = {""};


static int bsize = 100;
static int babs = 100;

static int bsizek = 60;

static BOOL WithEti = FALSE;;
static int eti_nr;
static char krz_txt [18];

static BOOL WithSumEti = FALSE;;
static int sum_eti_nr;
static char sum_krz_txt [18];

static double eti_netto;
static double eti_brutto;
static double eti_tara;
static double eti_stk;

static double sum_eti_netto;
static double sum_eti_brutto;
static double sum_eti_tara;
static double sum_eti_stk;

static void SetEti (double netto, double brutto, double tara)
/**
Wete fuer Etikettendruck setzen.
**/
{
	      eti_netto  = netto;
          eti_brutto = brutto;
		  eti_tara   = tara;
	      sum_eti_netto  += netto;
          sum_eti_brutto += brutto;
		  sum_eti_tara   += tara;
}


static void SetEtiStk (double stk)
/**
Wete fuer Etikettendruck setzen.
**/
{
	      eti_stk  = stk;
	      sum_eti_stk  += stk;
}

static field _EtiButton[] = {
(char *) &Etikett,          bsize, bsizek, 30, 700, 0, "", COLBUTTON, 0,
                                                      doetikett, 0,
(char *) &SumEti,           bsize, bsizek,100, 700, 0, "", COLBUTTON, 0,
                                                      dosumeti, 0};

form EtiButton = {2, 0, 0, _EtiButton, 0, 0, 0, 0, &EtiFont};


static field _WieButton[] = {
(char *) &WieTara,          bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      dotara, BUTARA,
(char *) &WiePreis,         bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      dopreis, 0,
(char *) &WieHand,          bsize, bsize, -1, 15, 0, "", COLBUTTON, 0,
                                                      dohand, 0,
(char *) &WieWie,           bsize, bsize, -1, 145, 0, "", COLBUTTON, 0,
                                                      dowiegen, 0,
(char *) &WieOK,            bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      doWOK, 0,
(char *) &WieEnde,          bsize, bsize, -1, 405, 0, "", COLBUTTON, 0,
                                                      0, VK_F5};

static form WieButton = {6, 0, 0, _WieButton, 0, 0, 0, 0, &WiegBuFontW};

static int bsize2 = 100;
static int babs2 = 100;

static field _WieButtonT[] = {
(char *) &AutoTara,         bsize, bsize, -1, 275, 0, "", COLBUTTON, 0,
                                                      doauto, 0,
(char *) &DelTara,          bsize2, bsize2, -1, 145, 0, "", COLBUTTON, 0,
                                                      deltara, 0,
(char *) &FestTara,         bsize2, bsize2, -1, 15, 0, "", COLBUTTON, 0,
                                                      festtara, 0,
(char *) &HandTara,         bsize2, bsize2, -1, 405, 0, "", COLBUTTON, 0,
                                                      handtara, 0,
(char *) &SetTara,          bsize2, bsize2, -1, 15, 0, "", COLBUTTON, 0,
                                                      settara, 0,
(char *) &WieEnde,          bsize2, bsize2, -1, 405, 0, "", COLBUTTON, 0,
                                                      0, VK_F5};

static form WieButtonT = {6, 0, 0, _WieButtonT, 0, 0, 0, 0, &WiegBuFontW};

static field _WiegKgT [] = {
WiegText,           0, 0, 10, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form WiegKgT = {1, 0, 0, _WiegKgT, 0, 0, 0, 0, &WiegKgFontT};

static field _WiegKgE [] = {
WiegMe,             0, 0, 0, -1, 0, "%11.3f", READONLY, 0, 0, 0};

static form WiegKgE = {1, 0, 0, _WiegKgE, 0, 0, 0, 0, &WiegKgFont};

static field _WiegKgMe [] = {
"KG",                   10, 1, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};

static form WiegKgMe = {1, 0, 0, _WiegKgMe, 0, 0, 0, 0, &WiegKgFontMe};


mfont ArtWFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      1,
                                      NULL};
mfont ArtWFontT = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                        RGB (0, 255, 255),
                                        1,
                                        NULL};


mfont ArtWCFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                      RGB (255, 255, 255),
                                      1,
                                      NULL};

mfont ArtWCFontT = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                        RGB (0, 255, 255),
                                        1,
                                        NULL};

mfont ftarafont  = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                              RGB (255, 255, 255),
                                              1,
                                              NULL};

mfont ftaratfont  = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                               RGB (0, 255, 255),
                                               1,
                                               NULL};

static char KF []  = {"Kunde    "};
static char KFK [] = {"Kunde    "};
static char KFF [] = {"Filiale  "};



static field _artwconstT [] =
{
"Artikel",                 9, 1, 3, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Artikelbezeichnung",     18, 1, 3,14,  0, "", DISPLAYONLY,    0, 0, 0,
"Auftrag",                 7, 1, 1, 3,  0, "", DISPLAYONLY,    0, 0, 0,
KF,                        9, 1, 1,14,  0, "", DISPLAYONLY,    0, 0, 0,
"Retourengrund",          13, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0};

static form artwconstT = {5, 0, 0,_artwconstT, 0, 0, 0, 0, &ArtWCFontT};


static char summe1 [40];
static char summe2 [40];
static char summe3 [40];
static char zsumme1 [40];
static char zsumme2 [40];
static char zsumme3 [40];

static char vorgabe1[40] = {"100"};
static char veinh1[10] = {"kg"};
static char vorgabe2[40] = {"100"};
static char veinh2[10] = {"kg"};
static char vorgabe3[40] = {"100"};

static char wiegzahl [40];
static char ret_grund[40];

static field _artwconst [] =
{
        aufps.a,            9, 1,  4, 3,  0, "%8.0f",  READONLY, 0, 0, 0,
        aufps.a_bz1,       21, 1,  4, 14, 0, "",       READONLY, 0, 0, 0,
        auftrag_nr,         9, 1,  2,  3, 0, "%8d",    READONLY , 0, 0, 0,
        kun_krz1,          21, 1,  2, 14, 0, "",       READONLY , 0, 0, 0,
        aufps.retp_kz,     21, 1,  2, 37, 0, "",       EDIT , 0, 0, 0
};


static form artwconst = {5, 0, 0,_artwconst, 0, 0, 0, 0, &ArtWCFont};


static form artwformT;
static form artwform;

char *TextCharge = "Chargen-Nr";
char *TextIdent  = "Ident-Nr";

char *LChargenNr = TextCharge;
char *ChargenNr  = aufps.ls_charge;
char Ident_vorher [40];


// Masken fuer Wiegen

static field _artwformTW [] =
{
"Chargen-Nr",             10, 1, 5, 3,  0, "", REMOVED,    0, 0, 0,
"Restmenge",              10, 1, 1,37,  0, "", REMOVED,    0, 0, 0,
"Liefermenge",            11, 1, 5,37,  0, "", DISPLAYONLY,0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,37,  0, "", DISPLAYONLY,0, 0, 0,
"Auftragsmenge",          13, 1, 5,23,  0, "", REMOVED,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7,23,  0, "", REMOVED,    0, 0, 0,
"Sollmenge",              11, 1, 3,37,  0, "", REMOVED,    0, 0, 0,
"Summe1",                  6, 1, 3,49,  0, "", REMOVED,    0, 0, 0,
"Summe2",                  6, 1, 5,49,  0, "", REMOVED,    0, 0, 0,
};

static form artwformTW = {9, 0, 0,_artwformTW, 0, 0, 0, 0, &ArtWFontT};


static field _artwformW [] =
{
        aufps.ls_charge,   19, 0, 6,  3, 0, "",       REMOVED,     0, 0, 0,
        auf_me,            10, 1, 2, 37, 0, "%9.3f",  REMOVED , 0, 0, 0,
        aufps.lief_me,     10, 1, 6, 37, 0, "%9.3f",  READONLY, 0, 0, 0,
        aufps.auf_me,      12, 1, 6, 23, 0, "%11.3f", REMOVED, 0, 0, 0,
        aufps.auf_me_vgl,  10, 1, 4, 37, 0, "%9.3f",  REMOVED, 0, 0, 0,
        summe1,            10, 1, 4, 49, 0, "%9.3f",  REMOVED, 0, 0, 0,
        summe2,            10, 1, 6, 49, 0, "%9.3f",  REMOVED, 0, 0, 0,
        zsumme1,            4, 1, 4, 61, 0, "%3d",    REMOVED, 0, 0, 0,
        zsumme2,            4, 1, 6, 61, 0, "%3d",    REMOVED, 0, 0, 0,
};

static form artwformW = {9, 0, 0,_artwformW, 0, 0, 0, 0, &ArtWFont};


// Masken fuer Auszeichnung

static field _artwformTP [] =
{
"Chargen-Nr",             10, 1, 5, 3,  0, "", DISPLAYONLY,    0, 0, 0,
"Vorgaben",                10, 1, 1,37,  0, "", DISPLAYONLY,    0, 0, 0,
veinh2,                    9, 1, 5,37,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,37,  0, "", REMOVED,        0, 0, 0,
"Liefermenge",            13, 1, 5,23,  0, "", DISPLAYONLY,        0, 0, 0,
aufps.lief_me_bz,         10, 1, 7,23,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7,37,  0, "", DISPLAYONLY,    0, 0, 0,
veinh1,                    9, 1, 3,37,  0, "", DISPLAYONLY,    0, 0, 0,
"Summen",                  6, 1, 1,49,  0, "", DISPLAYONLY,    0, 0, 0,
"Summe2",                  6, 1, 3,49,  0, "", REMOVED,    0, 0, 0,
"Summe3",                  6, 1, 5,49,  0, "", REMOVED,    0, 0, 0,
"kg",                      6, 1, 7,49,  0, "", REMOVED,    0, 0, 0,
"St�ck",                   7, 1, 3, 61, 0, "", DISPLAYONLY , 0, 0, 0,
"Karton",                  7, 1, 5, 61, 0, "", DISPLAYONLY , 0, 0, 0,
"Palette",                 7, 1, 7, 61, 0, "", DISPLAYONLY , 0, 0, 0,
veinh1,                    9, 1, 3, 49,  0, "", DISPLAYONLY,    0, 0, 0,
veinh2,                    9, 1, 5, 49,  0, "", DISPLAYONLY,    0, 0, 0,
aufps.auf_me_bz,          10, 1, 7, 49,  0, "", DISPLAYONLY,    0, 0, 0,
};

static form artwformTP = {18, 0, 0,_artwformTP, 0, 0, 0, 0, &ArtWFontT};


static field _artwformP [] =
{
        aufps.ls_charge,   19, 0, 6,  3, 0, "",       EDIT,     0, 0, 0,
        vorgabe1,          12, 1, 2, 37, 0, "%11.3f", READONLY, 0, 0, 0,
        aufps.lief_me,     10, 1, 6, 23, 0, "%9.3f",  READONLY,  0, 0, 0,
//        aufps.auf_me,      12, 1, 6, 37, 0, "%11.3f", READONLY, 0, 0, 0,
        vorgabe3,          12, 1, 6, 37, 0, "%11.3f", READONLY, 0, 0, 0,
        vorgabe2,          12, 1, 4, 37, 0, "%11.3f",  READONLY, 0, 0, 0,
        summe1,            10, 1, 2, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe2,            10, 1, 4, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        summe3,            10, 1, 6, 49, 0, "%9.3f",  READONLY, 0, 0, 0,
        zsumme1,            4, 1, 2, 61, 0, "%3d",    READONLY, 0, 0, 0,
        zsumme2,            4, 1, 4, 61, 0, "%3d",    READONLY, 0, 0, 0,
        zsumme3,            4, 1, 6, 61, 0, "%3d",    READONLY, 0, 0, 0,
};

static form artwformP = {11, 0, 0,_artwformP, 0, 0, 0, 0, &ArtWFont};

// Ende Maske fuer Preisauszeichnung

static field _ftaratxt [] =
{
"Tara ",                   5, 1,  8, 13, 0, "",  DISPLAYONLY, 0, 0, 0
};

static form ftaratxt = {1, 0, 0, _ftaratxt, 0, 0, 0, 0, &ftaratfont};

char ScaleTara[9] = {"0,0"};

static field _ftara [] =
{
        ScaleTara,        8, 1,  8, 20, 0, "%7.3f",  READONLY, 0, 0, 0
};

form ftara = {1, 0, 0, _ftara, 0, 0, 0, 0, &ftarafont};

/*
static field _ftara [] =
{
        aufps.tara,        8, 1,  8, 20, 0, "%7.3f",  READONLY, 0, 0, 0
};

static form ftara = {1, 0, 0, _ftara, 0, 0, 0, 0, &ftarafont};
*/


void SwitchLiefAuf (void)
/**
Text fuer Autrags-Nr oder Lieferschein belegen.
**/
{
         if (LiefEnterAktiv)
         {
                   artwconstT.mask[2].feld   = "Retoure";
                   artwconstT.mask[2].length = 0;
                   artwconst.mask[2].feld    = lief_nr;
         }
         else
         {
                   artwconstT.mask[2].feld   = "Auftrag";
                   artwconstT.mask[2].length = 0;
                   artwconst.mask[2].feld    = auftrag_nr;
         }
}



int GetGewicht (char *datei, double *brutto, double *netto, double *tara)
/**
Gewichtswerte aus der Datei ergebnis holen.
**/
{
         FILE *fp;
         char buffer [81];

         fp = fopen (datei, "r");
         if (fp == NULL) return (-1);

/*
         if (testmode)
         {
                    disp_messG ("ergebnis ge�ffnet", 1);
         }
*/

         while (fgets (buffer, 80, fp))
         {
/*
               if (testmode)
               {
                     print_messG (1, "gelesen %s", buffer);
               }
*/
               switch (buffer [0])
               {
                        case 'B' :
                                *brutto = (double) ratod (&buffer[1]);
                                break;
                        case 'N' :
                                *netto = (double) ratod (&buffer[1]);
                                break;
                        case 'T' :
                                *tara = (double) ratod (&buffer[1]);
                                break;
               }
         }
         fclose (fp);

         if (*netto == (double) 0)
         {
                        *netto = *brutto - *tara;
         }
         else if (*brutto == (double) 0)
         {
                        *brutto = *netto + *tara;
         }
         if (autoan)
         {
                       akt_tara = *brutto;
         }
         else
         {
                       akt_tara = *tara;
         }
/*
         if (testmode)
         {
                    print_messG (1,"%.2lf  %.2lf %-2lf", *brutto, *netto, *tara);
         }
*/
         return 0;
}

void fnbexec (int arganz, char *argv[])
/**
**/
{
        char aufruf [256];
        int i;

        if (getenv (BWS) == NULL) return;
        if (fnbproc != NULL)
		{
                     if (fnbInstanceproc != NULL)
					 {
			              (*fnbInstanceproc) (hMainInst);
					 }
			         int ret = (*fnbproc) (arganz, argv);
					 return;
		}


        sprintf (aufruf, "%s\\fnb.exe", waadir);

        for (i = 1; i < arganz; i ++)
        {
                     strcat (aufruf, " ");
                     strcat (aufruf, argv[i]);
        }

        ProcWaitExec (aufruf, SW_SHOWMINIMIZED, -1, 0, -1, 0);
}


BOOL WaaInitOk (char *wa)
/**
Test, ob die Waage schon initialisiert wurde.
**/
{
        static char *waadirs [20];
        static int waaz = 0;
        int i;

        if (waaz == 20) return FALSE;
        clipped (wa);
        for (i = 0; i < waaz; i ++)
        {
            if (strcmp (wa, waadirs [i]) == 0)
            {
                return TRUE;
            }
        }
        waadirs [i] = (char *) malloc (strlen (wa) + 3);
        if (waadirs [i] == NULL)
        {
            return FALSE;
        }
        strcpy (waadirs[i], wa);
        if (waaz < 20) waaz ++;
        return FALSE;
}



void WaageInit (void)
/**
Waage Initialisieren.
**/
{
        static int arganz;
        static char *args[4];
		char WaaDll [512];


        if (WaaInitOk (waadir)) return;

		waaLibrary = fnbproc = NULL;
        sprintf (WaaDll, "%s\\fnb.dll", waadir);
        waaLibrary = LoadLibrary (WaaDll);
        if (waaLibrary != NULL)
		{
               fnbInstanceproc = (int (*) (void*)) GetProcAddress (waaLibrary, fnbInstancename);
               fnbproc = (int  (*) (int, char **)) GetProcAddress (waaLibrary, fnbname);
		}

        deltaraw ();
		Sleep (50);
        WaaInit = 1;
        arganz = 3;
        args[0] = "Leer";
        args[1] = "0";
        args[2] = waadir;
        fnbexec (arganz, args);
}

int doWOK (void)
/**
Wiegen durchfuehren.
**/
{
	    if (artwform.mask[0].attribut == EDIT)
		{
                 GetWindowText (artwform.mask[0].feldid,
                      artwform.mask[0].feld,
                      artwform.mask[0].length);
		}
        strcpy (aufps.s, "3");
        break_wieg = 1;
        return 0;
}

void setautotara (BOOL an)
/**
Wiegen durchfuehren.
**/
{
	    if (an)
		{
              AutoTara.aktivate = 4;
              autoan = 1;
        }
        else
        {
              AutoTara.aktivate = 3;
              autoan = 0;
        }
        AutoTara.text3 = anaus [autoan];
}


int doauto (void)
/**
Wiegen durchfuehren.
**/
{
        if (AutoTara.aktivate == 4)
        {
              autoan = 1;
        }
        else
        {
              autoan = 0;
        }
        AutoTara.text3 = anaus [autoan];
        current_wieg = autopos;
        return 0;
}

void PressAuto ()
/**
Autotara druecken.
**/
{
       SendMessage (WieButton.mask[autopos].feldid, WM_LBUTTONDOWN, 0l, 0l);
       SendMessage (WieButton.mask[autopos].feldid, WM_LBUTTONUP, 0l, 0l);
}


int festtara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char ftara [11];
        static char *args[5];
        char buffer [20];

        ShowFestTara (buffer);
        sprintf (ftara, "%.3lf", (double) ratod (buffer));
//		if (fest_tara_sum || (autoan == 1))
		if (autoan == 1)
		{
                  akt_tara = akt_tara + (double) ratod (ftara);
		}
		else
		{
                  akt_tara = (double) ratod (ftara);
		}
        sprintf (ftara, "%.3lf", akt_tara);
        sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ScaleTara, "%.3lf", akt_tara);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
//        GetGewicht (ergebnis, &brutto, &netto, &tara);
        return 0;
}


int deltaraw (void)
/**
Tara l�schen
**/
{
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        return 0;
}


int deltara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char *args[4];

        sprintf (aufps.tara, "%.3lf", (double) 0.0);
        sprintf (ScaleTara, "%.3lf", (double) 0.0);
        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
//        GetGewicht (ergebnis, &brutto, &netto, &tara);
        akt_tara = (double) 0.0;
		display_form (WiegWindow, &ftara, 0, 0);
        return 0;
}

/*
int settara (void)
/
Wiegen durchfuehren.
/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "4";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
		sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ScaleTara, "%.3lf", akt_tara);
        return 0;
}
*/


int settara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "5";
        args[2] = waadir;
        fnbexec (arganz, args);

        Sleep (AutoSleep);

        arganz = 3;
        args[0] = "Leer";
        args[1] = "8";
        args[2] = waadir;
        fnbexec (arganz, args);

        Sleep (AutoSleep);

        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);
		akt_tara = brutto;
        if (akt_tara == 0.0)
        {
            akt_tara = netto;
        }
        if (akt_tara == 0.0)
        {
            akt_tara = tara;
        }
        arganz = 3;
        args[0] = "Leer";
        args[1] = "4";
        args[2] = waadir;
        fnbexec (arganz, args);
		sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ScaleTara, "%.3lf", akt_tara);
        sprintf (WiegMe, "%11.3lf", 0.0);
		display_form (WiegWindow, &ftara, 0, 0);
        display_form (WiegKg, &WiegKgE, 0, 0);
        return 0;
}


int setautotara (void)
/**
Wiegen durchfuehren.
**/
{
        static int arganz;
        static char *args[4];

        arganz = 3;
        args[0] = "Leer";
        args[1] = "4";
        args[2] = waadir;
        fnbexec (arganz, args);
/*
		sprintf (aufps.tara, "%.3lf", akt_tara);
		display_form (WiegWindow, &ftara, 0, 0);
*/
        return 0;
}


int handtara (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        static char ftara [11];
        static int arganz;
        static char *args[4];

        EnterCalcBox (WiegWindow,"  Tara  ",  ftara, 8, "%7.3f");
        SetAktivWindow (WiegWindow);
//  		if (hand_tara_sum || (autoan == 1))
  		if (autoan == 1)
		{
                  akt_tara = akt_tara + (double) ratod (ftara);
		}
		else
		{
                  akt_tara = (double) ratod (ftara);
		}
      sprintf (ftara, "%.3lf", akt_tara);
		sprintf (aufps.tara, "%.3lf", akt_tara);
        sprintf (ScaleTara, "%.3lf", akt_tara);
        arganz = 4;
        args[0] = "Leer";
        args[1] = "23";
        args[2] = waadir;
        args[3] = (char *) ftara;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
//        GetGewicht (ergebnis, &brutto, &netto, &tara);
        current_wieg = 2;
        return 0;
}

static int StornoSet = 0;

int dostorno (void)
/**
SornoFlag setzen.
**/
{
       StornoSet = 1;
       PostMessage (EditForm.mask[0].feldid, WM_KEYDOWN,
                    (WPARAM) VK_RETURN, 0l);
       return 0;
}

BOOL TestAllStorno (void)
/**
Abfragem, ob die ganze Position stormiert werden soll.
**/
{
         double rest;
         double lief_me;

         if (WorkModus == AUSZEICHNEN && AktWiegPos)
         {
                 if (abfragejnG (WiegWindow,
                         "Einzelne Wiegung stornieren", "J"))
                 {
                          AuswahlWieg ();
                          rest    = (double) ratod (auf_me);
                          lief_me = (double) ratod (aufps.lief_me);
                          rest += StornoWieg;
                          lief_me -= StornoWieg;
                          sprintf (auf_me, "%.3lf", rest);
                          sprintf (aufps.lief_me, "%3lf", lief_me);
                          return FALSE;
                 }
         }

         if (abfragejnG (WiegWindow,
                         "Die gesamte Position stornieren", "N"))
         {
             return TRUE;
         }
         return FALSE;
}


void stornoaction (void)
/**
Storno ausfuehren.
**/
{
        double lief_me;
        double rest;
        double wiegme;

        lief_me = (double) ratod (aufps.lief_me);
        rest    = (double) ratod (auf_me);
        wiegme = ratod (editbuff);
        if (wiegme != (double) 0.0)
        {
            rest += wiegme;
            lief_me -= wiegme;
            sprintf (auf_me, "%.3lf", rest);
        }
        else
        {
            if (TestAllStorno () == FALSE) return;
            rest += lief_me;
            lief_me = (double) 0;
            sprintf (aufps.lief_me1, "%.3lf", (double) 0);
            sprintf (aufps.lief_me2, "%.3lf", (double) 0);
            sprintf (aufps.lief_me3, "%.3lf", (double) 0);
            strcpy (auf_me, aufps.auf_me_vgl);
            strcpy (vorgabe3, aufps.auf_me);
            strcpy (summe1, "0");
            strcpy (summe2, "0");
            strcpy (summe3, "0");
            strcpy (zsumme1, "0");
            strcpy (zsumme2, "0");
            strcpy (zsumme3, "0");
        }
        sprintf (aufps.lief_me, "%3lf", lief_me);
        strcpy (WiegMe, "0.000");
        StornoSet = 0;
        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtoretp (aufpidx);
        Retp.dbupdate ();
        commitwork ();
        beginwork ();
}

int dohand (void)
/**
Wiegen durchfuehren.
**/
{
        double alt;
        double netto;

        StornoSet = 0;
        SetStorno (dostorno, 0);
        sprintf (WiegMe, "%.3lf", (double) 0);
        EnterCalcBox (WiegWindow,"  Gewicht  ",  WiegMe, 12, WiegKgE.mask[0].picture);
        if (syskey != KEY5)
        {
              strcpy (aufps.erf_kz, "H");
              if (StornoSet)
              {
                  stornoaction ();
              }
              else
              {
                  netto = (double) ratod (WiegMe);
				  if (netto <= 99999.999)
				  {
                          alt = (double) ratod (auf_me);
                          alt -= netto;
                          sprintf (auf_me, "%8.3lf", alt);
                          alt = (double) ratod (aufps.lief_me);
                          alt += netto;
						  if (alt <= 99999.999)
						  {
                                   sprintf (aufps.lief_me, "%8.3lf", alt);
						  }
				  }
                  memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                  arrtoretp (aufpidx);
                  Retp.dbupdate ();
                  commitwork ();
                  beginwork ();
              }
              display_form (WiegKg, &WiegKgE, 0, 0);
              display_form (WiegWindow, &artwform, 0, 0);
              display_form (WiegWindow, &artwconst, 0, 0);
        }
        current_wieg = 2;
		if (hand_direct)
		{
               strcpy (aufps.s, "3");
               break_wieg = 1;
		}
        return 0;
}

int GetDefault (char *env, char *wert)
/**
Wert aus bws_default holen.
**/
{
        char *etc;
        int anz;
        char buffer [512];
        FILE *fp;

        etc = getenv ("BWSETC");
        if (etc == (char *) 0)
        {
                    etc = "C:\\USER\\FIT\\ETC";
        }

        sprintf (buffer, "%s\\bws_defa", etc);
        fp = fopen (buffer, "r");
/*
        if (fp == NULL)
        {
                    sprintf (buffer, "%s\\bws_defa", etc);
                    fp = fopen (buffer, "r");
        }
*/
        if (fp == NULL) return -1;

        clipped (env);
        while (fgets (buffer, 511, fp))
        {
                     cr_weg (buffer);
                     anz = split (buffer);
                     if (anz < 2) continue;
                     if (strupcmp (wort[1], env, strlen (env)) == 0)
                     {
                                 strcpy (wert, wort [2]);
                                 fclose (fp);
                                 return 0;
                     }
         }
         fclose (fp);
         return (-1);
}

long GetStdAuszeichner (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
        static long auszeichner = 0;
        char defwert [20];

        if (auszeichner) return auszeichner;

        if (GetDefault ("TOUCH-AUSZEICHNER", defwert) == -1)
        {
                             return (long) 1;
        }
        auszeichner = atol (defwert);
        if (GetDefault ("PAZ_DIREKT", defwert) != -1)
        {
                        paz_direkt = atoi (defwert);
        }
        return auszeichner;
}

long GetStdWaage (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
        static long waage = 0;
        char defwert [20];

        if (waage) return waage;

        if (GetDefault ("TOUCH-WAAGE", defwert) == -1)
        {
                             return (long) 1;
        }
        waage = atol (defwert);
        return waage;
}

void GetAufArt9999 (void)
/**
System-Nummer fuer Touch-Screen holen.
**/
{
/*
        char defwert [20];
        if (GetDefault ("aufart9999", defwert) == -1)
        {
                             return (long) 1;
        }
        aufart9999 = atol (defwert);
        return waage;
*/
		aufart9999 = 0;
        strcpy (sys_par.sys_par_nam, "aufart9999");
        if (sys_par_class.dbreadfirst () == 0)
        {
                    aufart9999 = atoi (sys_par.sys_par_wrt);
        }
}

static long start_aufme;
static long start_liefme;
static long alarm_me = 0;
static short alarm_ok = 0;
static HWND AlarmWindow = NULL;
static short a_me_einh;
static int me_faktor = 1;
static long akt_me = 0;
static int break_ausz = 0;
int wiegx, wiegy, wiegcx, wiegcy;
int alarmy;
static int GxStop = 0;

static char amenge [80];

mfont AlarmFont  = {"Courier New", 150, 0, 1, BLACKCOL,
                                              LTGRAYCOL,
                                              1,
                                              NULL};

static field _alfield [] =
                 {"A C H T U N G ! ! !", 0, 0, 40, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  amenge,                0, 0, 70, -1, 0, "", DISPLAYONLY, 0, 0, 0};

static form alform = {2, 0, 0, _alfield, 0, 0, 0, 0, &AlarmFont};

mfont MessFont  = {"Courier New", 150, 0, 1, BLACKCOL,
                                             WHITECOL,
                                             1,
                                             NULL};

mfont MessFontBlue  = {"Courier New", 180, 0, 1, WHITECOL,
                                                 BLUECOL,
                                                 1,
                                                 NULL};

static char msgstring  [80];
static char msgstring1 [80];
static char msgstring2 [80];

static int msgy = 1;
static int oky = 4;
static int okcy = 3;
static int okcx = 8;
static int BreakKey5 (void);
static int BreakKey12 (void);
static int BreakOK (void);

static field _msgfield [] =
{
                  msgstring,  0, 0, 1, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring1, 0, 0, 2, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring2, 0, 0, 3, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  " OK ",     0, 2, 4, -1, 0, "", BUTTON, 0, 0, KEYCR,
};


static form msgform = {1, 0, 0, _msgfield, 0, 0, 0, 0, &MessFont};

static field _msgfieldjn [] =
{
                  msgstring,  0, 0, 1, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring1, 0, 0, 2, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  msgstring2, 0, 0, 3, -1, 0, "", DISPLAYONLY, 0, 0, 0,
                  "  Ja  ",   0, 2, 4, -1, 0, "", BUTTON, 0, 0,  KEY12,
                  " Nein ",   0, 2, 4, -1, 0, "", BUTTON, 0, 0,  KEY5,
};


static form msgformjn = {5, 0, 0, _msgfieldjn, 0, 0, 0, 0, &MessFont};

static form *aktmessform;


void TestMessage (void)
/**
Window-Meldungen testen.
**/
{
         MSG msg;

         if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE) == 0) return;

         if (msg.message == WM_KEYDOWN && msg.wParam != VK_RETURN) return;

         if (IsWiegMessage (&msg) == 0)
         {
                          TranslateMessage(&msg);
                          DispatchMessage(&msg);
         }
}

void WritePrcomm (char *comm)
/**
Datei Prcomm schreiben.
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        int z;

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prcomm", tmp);
        z = 0;
        fp = fopen (buffer, "w");
        while (fp == NULL)
        {
                    if (z == 1000) break;
                    TestMessage ();
                    if (break_ausz) break;
                    Sleep (10);
                    z ++;
                    fp = fopen (buffer, "w");
        }
        if (fp == NULL) return;
        fprintf (fp, "%s\n", comm);
        fclose (fp);
}

void Alarm (void)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int i;

        if (AlarmWindow) return;

        sprintf (amenge, "Sollmenge fast erreicht");

/*
        AlarmWindow  = CreateWindowEx (
                              0,
                              "AufWiegCtr",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              wiegx, wiegy - wiegcy - 5,
                              wiegcx, wiegcy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
*/
        AlarmWindow  = CreateWindowEx (
                              0,
                              "AufWiegCtr",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              wiegx,
//                              wiegy,
                              alarmy,
                              wiegcx, wiegcy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        if (AlarmWindow == NULL) return;
        ShowWindow (AlarmWindow, SW_SHOWNORMAL);
        UpdateWindow (AlarmWindow);
        for (i = 0; i < 3; i ++)
        {
                    MessageBeep (0xFFFFFFFF);
        }
}


LONG FAR PASCAL MsgProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (hWnd == MessWindow)
                    {
                            display_form (hWnd, aktmessform, 0, 0);
                    }
                    break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                     {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void CreateMessage (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        int i;
        RECT rect;
        TEXTMETRIC tm;

        if (MessWindow) return;

        aktmessform = &msgform;
        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.fieldanz = 1;
        msgform.mask[0].pos[0] = -1;
        msgform.mask[0].pos[1] = -1;
        msgform.font = &MessFont;
        SetTextMetrics (hWnd, &tm, msgform.font);

        GetClientRect (hWnd, &rect);
        strcpy (msgstring, message);

        cx = (strlen (msgstring) + 3) * tm.tmAveCharWidth;
        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 2;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegWhite",
                              "",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
        UpdateWindow (MessWindow);
}

void DestroyMessage (void)
{
        if (MessWindow == NULL) return;
        CloseControls (&msgform);
        DestroyWindow (MessWindow);
        MessWindow = 0;
}
void DestroyMessageWait (void)
{
        if (MessWindow == NULL) return;
        CloseControls (&msgform);
        DestroyWindow (MessWindow);
        MessWindow = 0;
        EnableWindows (waithWnd, TRUE);
        EnableWindows (hMainWindow, TRUE);
        DestroyMessage ();
        SetFocus (OldFocus);
}


int BreakKey5 ()
{
        syskey = KEY5;
        break_enter ();
        return 0;
}

int BreakKey12 ()
{
        syskey = KEY12;
        break_enter ();
        return 0;
}

int BreakOK ()
{
        break_enter ();
        return 0;
}


int CreateMessageJN (HWND hWnd, char *message, char *def)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        int okpos;
        unsigned int len, pos;
        HWND OldFocus;

        if (MessWindow) return 0;

        aktmessform = &msgformjn;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return 0;

        for (i = 0; i < msgformjn.fieldanz; i ++)
        {
            msgformjn.mask[i].length = 0;
        }

        msgformjn.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgformjn.font);

        msgformjn.mask[1].attribut = REMOVED;
        msgformjn.mask[2].attribut = REMOVED;
        strcpy (msgstring, wort[0]);
        len = strlen (msgstring);
        ltext = msgstring;
        okpos = oky;
        if (anz > 1)
        {
            strcpy (msgstring1, wort[1]);
            if (strlen (msgstring1) > len)
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgformjn.mask[1].attribut = DISPLAYONLY;
            okpos += 1;
        }
        if (anz > 2)
        {
            strcpy (msgstring2, wort[2]);
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgformjn.mask[2].attribut = DISPLAYONLY;
            okpos += 1;
        }

        msgform.fieldanz = 5;

        GetClientRect (hMainWindow, &rect);

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 4;
        y = (rect.bottom - cy) / 2 ;

        msgformjn.mask[0].pos[0] = tm.tmHeight * msgy;
        msgformjn.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgformjn.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));
        msgformjn.mask[msgformjn.fieldanz - 2].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);
        msgformjn.mask[msgformjn.fieldanz - 1].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);

        msgformjn.mask[msgformjn.fieldanz - 2].rows   = tm.tmHeight * okcy;
        msgformjn.mask[msgformjn.fieldanz - 2].length = tm.tmAveCharWidth * okcx;
        msgformjn.mask[msgformjn.fieldanz - 1].rows   = tm.tmHeight * okcy;
        msgformjn.mask[msgformjn.fieldanz - 1].length = tm.tmAveCharWidth * okcx;
        strcpy (msgformjn.mask[msgformjn.fieldanz - 2].feld, "  Ja  ");
        strcpy (msgformjn.mask[msgformjn.fieldanz - 1].feld, " Nein ");

        pos = (len + 3 - 16) / 2;
        if (pos <= 0) pos = 0;

        msgformjn.mask[msgformjn.fieldanz - 2].pos[1] = pos * tm.tmAveCharWidth;
        msgformjn.mask[msgformjn.fieldanz - 1].pos[1] = (pos + 9)
                                                     * tm.tmAveCharWidth;

        cy =  msgformjn.mask[msgformjn.fieldanz - 1].pos[0] +
              msgformjn.mask[msgformjn.fieldanz - 1].rows +
              tm.tmHeight;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return 0;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
//        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
//                   SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        display_form (MessWindow, &msgformjn, 0, 0);
        save_fkt (5);
        save_fkt (12);
        set_fkt (BreakKey5, 5);
        set_fkt (BreakKey12, 12);
        no_break_end ();
        EnableWindows (hMainWindow, FALSE);
        EnableWindows (hWnd, FALSE);
        SetButtonTab (TRUE);
        enter_form (MessWindow, &msgformjn, 0, 0);
        SetButtonTab (FALSE);
        EnableWindows (hWnd, TRUE);
        EnableWindows (hMainWindow, TRUE);
        DestroyMessage ();
        SetFocus (OldFocus);
        restore_fkt (5);
        restore_fkt (12);
        if (syskey == KEY5) return 0;
        return 1;
}


void CreateMessageOK (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        int okpos;
        unsigned int len;
        HWND OldFocus;

        if (MessWindow) return;

        aktmessform = &msgform;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return;

        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgform.font);

        msgform.mask[1].attribut = REMOVED;
        msgform.mask[2].attribut = REMOVED;
        strcpy (msgstring, wort[0]);
        len = strlen (msgstring);
        ltext = msgstring;
        okpos = oky;
        if (anz > 1)
        {
            strcpy (msgstring1, wort[1]);
            if (strlen (msgstring1) > len)
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgform.mask[1].attribut = DISPLAYONLY;
            okpos += 1;
        }
        if (anz > 2)
        {
            strcpy (msgstring2, wort[2]);
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgform.mask[2].attribut = DISPLAYONLY;
            okpos += 1;
        }

        msgform.fieldanz = 4;

        GetClientRect (hMainWindow, &rect);

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * 4;
        y = (rect.bottom - cy) / 2 ;

        msgform.mask[0].pos[0] = tm.tmHeight * msgy;
        msgform.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgform.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));
        msgform.mask[msgform.fieldanz - 1].pos[0] =
                                 (int) (double) (tm.tmHeight * okpos);

        msgform.mask[msgform.fieldanz - 1].rows   = tm.tmHeight * okcy;
        msgform.mask[msgform.fieldanz - 1].length = tm.tmAveCharWidth * okcx;
        strcpy (msgform.mask[msgform.fieldanz - 1].feld, " OK ");

        cy =  msgform.mask[msgform.fieldanz - 1].pos[0] +
              msgform.mask[msgform.fieldanz - 1].rows +
              tm.tmHeight;
        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
                              0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
//        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
//                   SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        display_form (MessWindow, &msgform, 0, 0);
        set_fkt (BreakOK, 12);
        break_end ();
        EnableWindows (hMainWindow, FALSE);
        EnableWindows (hWnd, FALSE);
        SetButtonTab (TRUE);
        enter_form (MessWindow, &msgform, 0, 0);
        SetButtonTab (FALSE);
        EnableWindows (hWnd, TRUE);
        EnableWindows (hMainWindow, TRUE);
        DestroyMessage ();
        SetFocus (OldFocus);
}

void disp_messG (char *text, int modus)
{
       if (AktivWindow == NULL)
       {
                 AktivWindow = GetActiveWindow ();
       }
        if (modus == 2)
        {
                    MessageBeep (0xFFFFFFFF);
        }
        CreateMessageOK (AktivWindow, text);
}

static char messbuffer [1024];

int print_messG (int mode, char * format, ...)
/**
Ausgabe in dbfile.
**/
{
        va_list args;

        va_start (args, format);
        vsprintf (messbuffer, format, args);
        va_end (args);
        disp_messG (messbuffer, mode);
        return (0);
}


int abfragejnG (HWND hWnd, char *text, char *def)
/**
Ausgabe in dbfile.
**/
{
        return CreateMessageJN (hWnd, text, def);
}


void WaitStat (char *stat)
/**
Ergebnis-Datei vom Preisauszeichner lesen. Auf stat warten
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        char satz [512];
        int z;

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);

        z = 0;
        while (TRUE)
        {
                  if (z == 30000) break;
                  fp = fopen (buffer, "r");
                  if (fp == NULL)
                  {
                              z ++;
                              Sleep (10);
                              continue;
                  }
                  if (fgets (satz, 511, fp) == 0)
                  {
                              z ++;
                              fclose (fp);
                              Sleep (10);
                              continue;
                  }
                  fclose (fp);
                  cr_weg (satz);
                  split (satz);
                  if (strcmp (wort[1], stat) == 0) break;
                  z ++;
                  TestMessage ();
                  if (break_ausz) break;
                  Sleep (10);
         }
}


static long sup;
static long sup0;
static long sup1;
static short kunme_einh = 0;
static long  kunmez = 0l;
static long  kunme = 0l;
static double akt_zsu1 = (double) 0;
static double akt_zsu2 = (double) 0;
static char rec [256] = {" "};


void TestSu (double zsu1, double zsu2, double zsu3)
{
/*

		 if (a_kun_geb.pal_fill == 5)
		 {
			      sup = (long) zsu2;
		 }
		 else if (a_kun_geb.pal_fill == 8)
		 {
			      sup = (long) zsu2;
		 }
         else if (akt_zsu1 && zsu1 != akt_zsu1 && zsu1 <= (double) 1)
         {
                  sup ++;
                  sup1 ++;
         }

		 if (kunme_einh == 16)
		 {
			      sup0 = (long) zsu3;
		 }

         else if (akt_zsu2 && zsu2 != akt_zsu2 && zsu2 <= (double) 1)
         {
                  sup = (long) zsu1;
                  sup0 ++;
         }
         akt_zsu1 = zsu1;
         akt_zsu2 = zsu2;
*/
         sup = (long) zsu2;
         sup0 = (long) zsu3;
}


int AuszKomplett (void)
/**
Test, ob die Auftragsmenge erreicht ist.
**/
{
        long auf_me_kun;

        auf_me_kun = atol (aufps.auf_me);
        switch (kunme_einh)
        {
            case 2 :
                 if (ratod (auf_me) <= (double) 0.0)
                 {
                         return 1;
                 }
                 return 0;
            case 1 :
                 if (kunme > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
            case 8 :
                 if (sup1 > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
            case 16 :
                 if (sup0 > auf_me_kun)
                 {
                         return 1;
                 }
                 return 0;
        }
        return 0;
}


void GetKunMe ()
/**
Auftragsmenge in Kundenbestelleinheit
**/
{
         int me_einh_kun;

         me_einh_kun = atoi (aufps.me_einh_kun);
         switch (me_einh_kun)
         {
             case 2 :
                   kunme_einh = 2;
                   break;
             case 6 :
             case 7 :
                   kunme_einh = 8;
                   break;
             case 10 :
                   kunme_einh = 16;
                   break;
             default :
                   kunme_einh = 1;
                   break;
         }
}

void FillSummen (double su1, double su2,
                 double zsu1, double zsu2)
/**
Aktuellen Inhalte der einzelnen Summen in Abhaengigkeit
von der Einheit fuellen.
**/
{

         switch (a_kun_geb.geb_fill)
         {
               case 2 :
                    sprintf (summe1, "%.3lf", su1 / me_faktor);
                    break;
               case 1:
                    sprintf (summe1, "%.0lf", zsu1);
                    break;
               default :
                    sprintf (summe1, "%.3lf", su1 / me_faktor);
                    break;
         }

         switch (a_kun_geb.pal_fill)
         {
               case 2 :
                    sprintf (summe2, "%.3lf", su2 / me_faktor);
                    break;
               case 1:
                    sprintf (summe2, "%.0lf", zsu2);
                    break;
               case 5:
                    sprintf (summe2, "%ld", sup);
                    break;
               default :
                    sprintf (summe2, "%.3lf", su2 / me_faktor);
                    break;
         }
}

void FillSumme3 (double su3, double zsu3)
/**
Aktuellen Inhalte von Summe3 in Abhaengigkeit
von der Einheit fuellen.
**/
{

/*
         switch (kunme_einh)
         {
            case 2 :
                    sprintf (summe3, "%.3lf", su3 / me_faktor);
                    break;
            case 1 :
                    sprintf (summe3, "%.0lf", zsu3);
                    break;
            case 5 :
            case 8 :
                    sprintf (summe3, "%.0lf", sup);
                    break;
            case 16 :
                    sprintf (summe3, "%ld", sup0);
                    break;
        }
*/
	    if (kunme_einh == 2)
		{
                    sprintf (summe3, "%s", aufps.lief_me);
		}
		else
		{
                    sprintf (summe3, "%ld", sup0);
		}
}

int KorrMe (char *satz)
/**
Auftragsmenge korrigieren.
**/
{
         int anz;
         long me;
         long zielme;
         double me_vgl;
         double su1;
         double su2;
         double zsu1;
         double zsu2;
         double zsu3;
		 char *fpos;

         if (atoi (aufps.a_me_einh) == 2)
         {
                    me_vgl = ratod (aufps.auf_me_vgl) * 1000;
         }
         else
         {
                    me_vgl = ratod (aufps.auf_me_vgl);
         }

         cr_weg (satz);
         anz = split (satz);
         if (anz < 9)
         {
                   return 1;
         }

         if (wort[1][0] == 'F')
         {
			       if (anz > 1)
				   {
					   fpos = strstr (satz, wort[2]);
					   if (fpos == NULL)
					   {
 				               print_messG (2, "%s", fpos);
					   }
				   }
                   return 0;
         }
         if (wort[1][0] == 'E')
         {
                   return 0;
         }

         if (GxStop) return 1;


		 if (strcmp (rec," ") ==  0) strcpy (rec,satz);
		 if (strcmp (satz, rec) == 0) return 1;

		 strcpy (rec, satz);
         me   = atol (wort[9]);
		 if (me > 99999.999) return 1;

         zsu1 = ratod (wort[4]);
         su1  = ratod (wort[5]);
         zsu2 = ratod (wort[6]);
         su2  = ratod (wort[7]);
         zsu3 = ratod (wort[8]);

         TestSu (zsu1, zsu2, zsu3);
         FillSummen (su1, su2, zsu1, zsu2);

         zsu2 = (double) sup;


         sprintf (zsumme1, "%.0lf", zsu1);
		 if (a_kun_geb.pal_fill == 5 || a_kun_geb.pal_fill == 8)
		 {
                 sprintf (zsumme2, "%.0lf", zsu2);
		 }
		 else if (kunme_einh == 8)
		 {
                 sprintf (zsumme2, "%.0lf", zsu3);
		 }
         if (!alarm_ok && me > 0l)
         {
                     alarm_me = (long) ((double) me_vgl - 3 * me);
                     alarm_ok = 1;
         }

		 if (me <= 99999.999)
		 {
                 kunme ++;
                 zielme = start_aufme - me;
                 sprintf (auf_me, "%8.3lf", (double) ((double) zielme / me_faktor));
		 }

		 if (me <= 99999.999)
		 {
                 zielme = start_liefme + me;
		 }
		 if (zielme <= 99999.999)
		 {
                 sprintf (aufps.lief_me, "%8.3lf", (double) ((double) zielme / me_faktor));
		 }
         FillSumme3 ((double) zielme, zsu3);
         zsu3 = (double) sup0;
		 if (kunme_einh == 16)
		 {
                   sprintf (zsumme3, "%.0lf", zsu3);
		 }
         AktPrWieg[AktWiegPos] = ((double) (me - akt_me)) / me_faktor;
         sprintf (WiegMe, "%8.3lf", (double) ((double) (me - akt_me) / me_faktor));


         display_field (WiegWindow, &artwform.mask[1], 0, 0);
         display_field (WiegWindow, &artwform.mask[2], 0, 0);

         display_field (WiegWindow, &artwform.mask[3], 0, 0);
         display_field (WiegWindow, &artwform.mask[4], 0, 0);
         display_field (WiegWindow, &artwform.mask[5], 0, 0);
         display_field (WiegWindow, &artwform.mask[6], 0, 0);
         display_field (WiegWindow, &artwform.mask[7], 0, 0);
         display_field (WiegWindow, &artwform.mask[8], 0, 0);
         display_field (WiegWindow, &artwform.mask[9], 0, 0);
         display_field (WiegWindow, &artwform.mask[10], 0, 0);

/*
		 InvalidateRect (WiegWindow, NULL, TRUE);
		 UpdateWindow (WiegWindow);
*/


         display_field (WiegKg,     &WiegKgE.mask[0], 0, 0);

         strcpy (aufps.erf_kz, "E");
         memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
         arrtoretp (aufpidx);
         ls_class.update_lsp (0,lsp.mdn, lsp.fil, lsp.ls,
                                        lsp.a, lsp.posi);
         commitwork ();
         beginwork ();

         SetFocus (WieButton.mask[3].feldid);
         if (AktWiegPos < 998) AktWiegPos ++;
         akt_me = (long) me;
/*
         if (AuszKomplett ())
         {
                         return 0;
         }


         if (zielme >= alarm_me)
         {
                         Alarm ();
         }
*/

         return 1;
}

void PollAusz (void)
/**
Ergebnis-Datei vom Preisauszeichner lesen.
**/
{
        FILE *fp;
        char *tmp;
        char buffer [512];
        char satz [512];
        int pollanz;

        a_me_einh = atoi (aufps.a_me_einh);
        alarm_me = 0l;
        alarm_ok = 0;
        kunme = 0;
        kunmez = 0l;
        GetKunMe ();
        sup = (long) 0;
        sup0 = 0l;
        sup1 = 0l;
		akt_zsu1 = (double) 0.0;
		akt_zsu2 = (double) 0.0;
		strcpy (rec, " ");
        if (a_me_einh == 2)
        {
                      me_faktor = 1000;
        }
        else
        {
                      me_faktor = 1;
        }

        start_aufme  =  (long) ((double) ratod (auf_me) * me_faktor);
        start_liefme =  (long) ((double) ratod (aufps.lief_me) * me_faktor);

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);

        GxStop = pollanz = 0;
        while (TRUE)
        {
                  TestMessage ();
                  if (GxStop) pollanz ++;
                  if (pollanz >= 40) break;
                  fp = fopen (buffer, "r");
                  if (fp == NULL)
                  {
                              TestMessage ();
                              Sleep (50);
                              continue;
                  }
                  if (fgets (satz, 511, fp) == 0)
                  {
                              TestMessage ();
                              fclose (fp);
                              Sleep (50);
                              continue;
                  }
                  fclose (fp);
                  if (KorrMe (satz) == 0)
                  {
                              break;
                  }
                  TestMessage ();
                  SetFocus (WieButton.mask[3].feldid);
                  Sleep (50);
         }
}

void starte_prakt (void)
/**
Prgramm prakt starten.
**/
{
        char *tmp;
        char *bws;
        char buffer [512];

        tmp = getenv ("TMPPATH");
        if (tmp == NULL)
        {
                  tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (buffer, "%s\\prstat", tmp);
        unlink (buffer);

        bws = getenv ("BWS");
        if (bws == NULL)
        {
                  bws = "C:\\USER\\FIT";
        }
        sprintf (buffer, "%s\\bin\\gxakt", bws);

        if (ProcExec (buffer, SW_SHOWMINNOACTIVE, -1, 0, -1, 0) == 0)
        {
                       print_messG (2, "Fehler beim Start von %s", buffer);
//                       break_ausz = 1;
        }
}

void SetAuszText (int mode)
/**
Text fuer AuszecihnenButton setzen.
**/
{
         if (mode == 0)
         {
               WieWie.text1 = "Aus-";
               WieWie.text2 = "zeichnen";
               WieWie.tx1 = -1;
               WieWie.ty1 = 25;
               WieWie.tx2 = -1;
               WieWie.ty2 = 50;
         }
         else
         {
               WieWie.text1 = "Stop";
               WieWie.text2 = "";
               WieWie.tx1 = -1;
               WieWie.ty1 = -1;
               WieWie.tx2 = -1;
               WieWie.ty2 = -1;
          }
          WieWie.aktivate = 2;
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 1);
          SetFocus (WieButton.mask[3].feldid);
}

void BuInactiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (WiegCtr, &WieButton, 0, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 1, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 2, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 3, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 4, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 5, -1, 1);
          ActivateColButton (WiegCtr, &WieButton, 6, -1, 1);
}

void BuActiv (void)
/**
Alle Button auf Inaktiv setzen.
**/
{
          ActivateColButton (WiegCtr, &WieButton, 0, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 1, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 2, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 3, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 4, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 5, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 6, 2, 1);
          ActivateColButton (WiegCtr, &WieButton, 6, 2, 1);
}

static void FileError (void)
{

          disp_messG ("Es wurden keine Daten zum Auszeichnen gefunden\n"
                     "Pr�fen Sie bitte den Verkaufspreis\n und die Hauptwarengruppenzuordnung",
                     2);
          return;
}


BOOL KunGxOk (void)
/**
Pruefen, ob die Dateien fuer gxakt leer sind.
**/
{
        char *tmp;
        char dname [256];
        HANDLE fp;
        DWORD fsize;

        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }
        sprintf (dname, "%s\\kungx", tmp);
        fp = CreateFile (dname, GENERIC_READ, 0, NULL,
                         OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (fp == INVALID_HANDLE_VALUE)
        {
                         FileError ();
                         return FALSE;
        }

        fsize = GetFileSize (fp, NULL);
        if (fsize == (DWORD) 0 ||
            fsize == 0xFFFFFFFF)
        {
                         CloseHandle (fp);
                         FileError ();
                         return FALSE;
        }

        CloseHandle (fp);
        return TRUE;
}


/*
void RunBwsasc (long auszeichner, double auf_me_vgl)
{
        char buffer [256];
        char *tmp;
        char args [80] [20];
        char and_part [512];


        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }

         sprintf (and_part,
                 "where a between %.0lf and %.0lf",
                 ratod (aufps.a), ratod (aufps.a));

         sethochkomma ();
         setomodus ("w");
         settrenner (' ');
         setclipped (1);
         setand_part (and_part);
         sprintf (buffer, "%s\\kungx", tmp);
         setascii_name (buffer);
         setinfo_name ("kungx.if");

         sprintf (args [0], "%ld", auszeichner);
         sprintf (args [1],"%ld", atol (kun_nr));
         strcpy (args [2], lieferdatum);
         sprintf (args [3], "%.0lf", auf_me_vgl);

         argsp [0] = args [0];
         argsp [1] = args [1];
         argsp [2] = args [2];
         argsp [3] = args [3];

         do_bwsasc ();

         sprintf (buffer, "%s\\kungtex", tmp);
         setascii_name (buffer);
         setinfo_name ("kungtex.if");

         do_bwsasc ();
}
*/

int doauszeichnen (void)
/**
Auftragsposition auszeichnen.
**/
{
        char buffer [512];
        char *tmp;
        double auf_me_vgl;
        double auf_me_kun;
		long kunl;
		int kunfil = 0;
        DWORD ex;

        akt_me = 0;
        break_ausz = 0;
        tmp = getenv ("TMPPATH");
        if (tmp == (char *) 0)
        {
                        tmp = "C:\\USER\\FIT\\TMP";
        }

//        auszeichner = GetStdAuszeichner ();

        GetKunMe ();

        if (atoi (aufps.me_einh_kun) == 2)
        {
                    auf_me_kun = ratod (aufps.auf_me) * 1000;
        }
        else
        {
                    auf_me_kun = ratod (aufps.auf_me);
        }

        if (auf_me_kun <= (double) 0.0)
        {

                       disp_messG ("Menge 0 kann nicht ausgezeichnet werden", 2);
                       return (0);
        }

        if (ratod (aufps.lief_me) > (double) 0.0 && lief_me_mess)
        {

                       if (abfragejnG (WiegWindow, "Achtung !! Die Liefermenge ist > 0. "
						               "Auszeichnen ?", "J") == 0)
					   {
						   return (0);
					   }
					   sprintf (aufps.auf_me_vgl, "%.3lf", ratod (aufps.auf_me_vgl) -
						                                   ratod (aufps.lief_me));
        }


        if (atoi (aufps.a_me_einh) == 2)
        {
                    auf_me_vgl = ratod (aufps.auf_me_vgl) * 1000;
        }

/*
        if (auf_me_vgl <= (double) 0.0)
        {

                       disp_messG ("Menge 0 kann nicht ausgezeichnet werden", 2);
                       return (0);
        }
*/

/*
        if (ratod (auf_me) <= (double) 0.0)
        {

                       disp_messG ("Die Sollmenge ist erreicht", 2);
                       return (0);
        }
*/
		kunfil = GetKunFil ();
		if (kunfil)
		{
			kunl = 0l;
		}
		else
		{
			kunl = atol (kun_nr);
		}
        if (paz_direkt == 3)
        {
/*
                      RunBwsasc (auszeichner, auf_me_vgl);
                      BuInactiv ();
*/
        }
        else if (paz_direkt == 2)
        {

/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -dokt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh);

                      BuInactiv ();
                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL,
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2,
                              "Fehler beim 1. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }

                      sprintf (buffer, "pr_paz -odkt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl);

                      ex = ProcWaitExec (buffer, SW_SHOWNORMAL,
                                                 -1, 0, -1, 0);
                      if (ex != 0l)
                      {
                          print_messG (2,
                              "Fehler beim 2. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
       }
        else if (paz_direkt == 1)
        {
/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh);


                      BuInactiv ();
                      CreateBatch (buffer);

                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl);

                     RunBatch (buffer);
        }
        else
        {
/*
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, atol (kun_nr), lieferdatum, auf_me_vgl,
                          aufps.auf_lad_pr);
*/
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungx kungx.if %ld %ld %s %.0lf %s %hd",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl, lieferdatum, auf_me_kun,
                          aufps.auf_lad_pr, kunme_einh );

                      BuInactiv ();
                      CreateMessage (WiegWindow, "Die Daten werden vorbereitet");
                      ex = ProcWaitExec (buffer, SW_HIDE,
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage ();
                          AktivWindow = WiegWindow;
                          print_messG (2,
                              "Fehler beim 1. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
                      sprintf (buffer, "pr_paz -okt\" \" -l1 "
                         "-s\"where a between %.0lf and %.0lf\" "
                         "%s\\kungtex kungtex.if %ld %ld",
                          ratod (aufps.a), ratod (aufps.a),
                          tmp, auszeichner, kunl);

                      ex = ProcWaitExec (buffer, SW_HIDE,
                                                 -1, 0, -1, 0);
                      if (ex == (long) -1)
                      {
                          DestroyMessage ();
                          AktivWindow = WiegWindow;
                          print_messG (2,
                              "Fehler beim 2. Start von pr_paz");
                          BuActiv ();
                          return (-1);
                      }
                      DestroyMessage ();
                      AktivWindow = WiegWindow;
       }
       if (! KunGxOk ())
       {
                    BuActiv ();
                    WorkModus = AUSZEICHNEN;
                    return (-1);
        }

        WorkModus = STOP;
        WritePrcomm ("G");
        if (! break_ausz)
        {
                   SetAuszText (1);
                   starte_prakt ();
                   SetActiveWindow (WiegCtr);
        }
        if (! break_ausz)
        {
                   PollAusz ();
        }

		sprintf (aufps.lief_me1, "%.3lf", ratod (aufps.lief_me1) + ratod (summe1));
		sprintf (aufps.lief_me2, "%.3lf", ratod (aufps.lief_me2) + ratod (summe2));
		sprintf (aufps.lief_me3, "%.3lf", ratod (aufps.lief_me3) + ratod (summe3));

        WritePrcomm ("E");
        SetAuszText (0);
        BuActiv ();
        WorkModus = AUSZEICHNEN;
        if (AlarmWindow)
        {
                     CloseControls (&alform);
                     DestroyWindow (AlarmWindow);
                     AlarmWindow = NULL;
        }
		if (paz_direct)
		{
               strcpy (aufps.s, "3");
               break_wieg = 1;
		}
        return 0;
}

int StopAuszeichnen (void)
{

        GxStop = 1;
        if (abfragejnG (WiegWindow, "Auszeichnen stoppen ?", "N") == 0)
        {
            GxStop = 0;
            return 0;
        }
        WritePrcomm ("E");
        GxStop = 1;
        return 0;
}

int dowiegen (void)
/**
Wiegen durchfuehren.
**/
{
        double netto;
        double brutto;
        double tara;
        double alt;
        static int arganz;
        static char *args[4];

        if (WorkModus == AUSZEICHNEN)
        {
                       return (doauszeichnen ());
        }
        else if (WorkModus == STOP)
        {
                       return (StopAuszeichnen ());
        }
        arganz = 3;
        args[0] = "Leer";
        args[1] = "11";
        args[2] = waadir;
        fnbexec (arganz, args);
        brutto = netto = tara = (double) 0.0;
        GetGewicht (ergebnis, &brutto, &netto, &tara);

        if (tara == (double) 0.0)
        {
                   strcpy (aufps.erf_kz, "W");
        }
        else
        {
                   strcpy (aufps.erf_kz, "E");
        }
		if (netto <= 99999.999)
		{
                   alt = (double) ratod (auf_me);
                   alt -= netto;
                   sprintf (WiegMe, "%11.3lf", netto);
                   sprintf (auf_me, "%3lf", alt);
                   alt = (double) ratod (aufps.lief_me);
                   alt += netto;
				   if (alt <= 99999.999)
				   {
                             sprintf (aufps.lief_me, "%8.3lf", alt);
				   }
		}
		if (tara <= 99999.999)
		{
               sprintf (aufps.tara, "%.3lf", tara);
	           sprintf (ScaleTara, "%.3lf", akt_tara);
		}
        display_form (WiegKg, &WiegKgE, 0, 0);
        display_form (WiegWindow, &artwform, 0, 0);
        display_form (WiegWindow, &artwconst, 0, 0);
        if (autoan == 1)
        {
            Sleep (AutoSleep);
            setautotara ();
        }
        current_wieg = 3;
        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
                  arrtoretp (aufpidx);
        Retp.dbupdate ();
        commitwork ();
        beginwork ();
		if (wieg_direct)
		{
		            doWOK ();
		}
        return 0;
}

void PrintWLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
         RECT rect, frect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen, oldpen;
         int x, y;

         GetClientRect (WiegWindow, &rect);
         GetFrmRect (&artwform, &frect);

         hdc = BeginPaint (WiegWindow, &ps);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         oldpen = SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = 10;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         oldpen = SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);


         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         oldpen = SelectObject (hdc, hPen);
         x = rect.right - 4;
         y = frect.bottom + 25;
         alarmy = y;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         oldpen = SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}


LONG FAR PASCAL WiegProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

              case WM_PAINT :
                    if (hWnd == MessWindow)
                    {
                            display_form (hWnd, &msgform, 0, 0);
                    }


                    if (hWnd == WiegKg)
                    {
                            display_form (hWnd, &WiegKgT, 0, 0);
                            display_form (hWnd, &WiegKgE, 0, 0);
                    }
                    else if (hWnd == WiegCtr)
                    {       if (CtrMode == 0)
                            {
                                    display_form (hWnd, &WieButton, 0, 0);
                                    SetFocus (
                                           WieButton.mask[current_wieg].feldid);
                            }
                            else
                            {
                                    display_form (hWnd, &WieButtonT, 0, 0);
                                    SetFocus (
                                           WieButtonT.mask[current_wieg].feldid);
                            }
                    }
                    else if (hWnd == WiegWindow)
                    {
                            display_form (hWnd, &WiegKgMe, 0, 0);
                            display_form (hWnd, &artwformT, 0, 0);
                            display_form (hWnd, &artwconstT, 0, 0);
                            if (WorkModus != AUSZEICHNEN)
							{
                                 display_form (hWnd, &EtiButton, 0, 0);
							}
                            if (artwform.mask[0].feldid)
                            {
                                 display_form (hWnd, &artwform, 0, 0);
                                 display_form (hWnd, &artwconst, 0, 0);
                            }
                            display_form (hWnd, &ftara, 0, 0);
                            display_form (hWnd, &ftaratxt, 0, 0);
                            PrintWLines ();
                    }
                    else if (hWnd == AlarmWindow)
                    {
                            display_form (hWnd, &alform, 0, 0);
                    }
                    break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == BUTARA)
                    {
                            PostMessage (WiegWindow, WM_USER, wParam, 0l);
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}


void RegisterWieg (void)
/**
Fenster fuer Auftragskopf registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, FITICON);
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  WiegProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 255, 255));
                   wc.lpszClassName =  "AufWieg";
                   RegisterClass(&wc);

                   // wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 120));
                   wc.hbrBackground =   GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "AufWiegCtr";
                   RegisterClass(&wc);

                   wc.lpfnWndProc   =   WiegProc;
                   wc.hbrBackground =  CreateSolidBrush (RGB (0, 0, 255));
                   wc.lpszClassName =  "AufWiegKg";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

void TestEnter (void)
/**
Eingabefeld testen.
**/
{
       GetWindowText (artwform.mask[0].feldid,
                      artwform.mask[0].feld,
                      artwform.mask[0].length);
}


void SetWiegFocus (void)
/**
Focus fuer Wiegmaske setzen.
**/
{
       form *wform;

       if (CtrMode == 1)
       {
                  wform = &WieButtonT;
       }
       else
       {
                  wform = &WieButton;
       }

       if (current_wieg < wiegend)
       {
                  SetFocus (wform->mask[current_wieg].feldid);
       }
       else if (artwform.mask[0].attribut != EDIT)
	   {
		          current_wieg = 0;
                  SetFocus (wform->mask[current_wieg].feldid);
	   }
       else
       {
                  SetFocus (artwform.mask[0].feldid);
                  SendMessage (artwform.mask[0].feldid, EM_SETSEL,
                               (WPARAM) 0, MAKELONG (-1, 0));
       }
}

int ReturnActionW (void)
/**
Returntaste behandeln.
**/
{
       form *wform;

       if (current_wieg == wiegend)
       {
                  PostMessage (WiegWindow, WM_KEYDOWN, VK_DOWN, 0l);
                  return TRUE;
       }

       if (CtrMode == 1)
       {
                  wform = &WieButtonT;
       }
       else
       {
                  wform = &WieButton;
       }

       if (CtrMode == 0 && current_wieg == autopos)
       {
                        PressAuto ();
       }
       else if (wform->mask[current_wieg].after)
       {
                       (*wform->mask[current_wieg].after) ();
       }
       else if (wform->mask[current_wieg].BuId)
       {
                       SendKey
                                 (wform->mask[current_wieg].BuId);
       }
       return TRUE;
}

int TestAktivButton (int sign, int current_wieg)
/**
Status von Colbutton pruefen.
**/
{
       ColButton *Cub;
       int run;
       form *wform;

       if (CtrMode == 1)
       {
                  wform = &WieButtonT;
       }
       else
       {
                  wform = &WieButton;
       }

       run = 1;
       while (TRUE)
       {
              if (current_wieg >= wform->fieldanz) break;
              Cub = (ColButton *) wform->mask[current_wieg].feld;
              if (Cub->aktivate != -1) break;
              current_wieg += sign;
              if (sign == PLUS && current_wieg > wiegend)
              {
                                if (run == 2) break;
                                current_wieg = 0;
                                run ++;
              }
              else if (sign == MINUS && current_wieg < 0)
              {
                                if (run == 2) break;
                                current_wieg = wiegend;
                                run ++;
              }
       }
       return (current_wieg);
}


int IsWiegMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     TestEnter ();
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    if (CtrMode == 1)
                                    {
                                          closetara ();
                                    }
                                    else
                                    {
                                          break_wieg = 1;
                                    }
                                    return TRUE;
                            case VK_RETURN :
                                    return (ReturnActionW ());
                            case VK_DOWN :
                            case VK_RIGHT :
                                    current_wieg ++;
                                    if (current_wieg > wiegend)
                                                current_wieg = 0;
                                    current_wieg = TestAktivButton (PLUS, current_wieg);
                                    SetWiegFocus ();
                                    return TRUE;
                            case VK_UP :
                            case VK_LEFT :
                                   current_wieg --;
                                   if (current_wieg < 0)
                                                current_wieg = wiegend;
                                    current_wieg = TestAktivButton (MINUS, current_wieg);
                                    SetWiegFocus ();
                                    return TRUE;
                            case VK_TAB :
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                             current_wieg --;
                                             if (current_wieg < 0)
                                                  current_wieg = wiegend;
                                             current_wieg =
                                                 TestAktivButton (MINUS, current_wieg);
                                     }
                                     else
                                     {
                                             current_wieg ++;
                                             if (current_wieg > wiegend)
                                                  current_wieg = 0;
                                             current_wieg =
                                                 TestAktivButton (PLUS, current_wieg);
                                     }
                                     SetWiegFocus ();
                                     return TRUE;
                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (artwform.mask[0].feldid, &mousepos) &&
						artwform.mask[0].attribut == EDIT)
                    {
                          EnterNumBox (WiegWindow,"  Chargen-Nr  ",
                                       aufps.ls_charge, 17,
                                       artwform.mask[0].picture);
                          strcpy (last_charge, aufps.ls_charge);
                          display_form (WiegWindow, &artwform, 0, 0);
                          current_wieg = TestAktivButton (PLUS, current_wieg);
                          SetWiegFocus ();
                          return TRUE;
                    }
                    if (MouseinWindow (artwconst.mask[4].feldid, &mousepos))
                    {
						  char retp_kz[30];
						  strcpy (retp_kz, "");
						  int idx = ShowRetpKz (retp_kz);
						  if (syskey != KEY5)
						  {
								strcpy (aufps.retp_kz, retp_kz);
								display_form (WiegWindow, &artwconst, 0, 0);
						  }
                          SetWiegFocus ();
                          return TRUE;
                    }
              }
              case WM_USER :
                    if (LOWORD (msg->wParam) == BUTARA)
                    {
                          dotara ();
                          return TRUE;
                    }
         }
         return FALSE;
}

void PosArtFormW (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;


        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwformT.fieldanz; i ++)
        {
                    s = artwformT.mask[i].pos [1];
                    z = artwformT.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].pos [1] = s;
                    artwformT.mask[i].pos [0] = z;
                    s = artwformT.mask[i].length;
                    z = artwformT.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].length = s;
                    artwformT.mask[i].rows = z;
        }
        for (i = 0; i < artwform.fieldanz; i ++)
        {
                    s = artwform.mask[i].pos [1];
                    z = artwform.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].pos [1] = s;
                    artwform.mask[i].pos [0] = z;
                    s = artwform.mask[i].length;
                    z = artwform.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].length = s;
                    artwform.mask[i].rows = z;
        }
}

void PosArtFormC (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwconstT.fieldanz; i ++)
        {
                    s = artwconstT.mask[i].pos [1];
                    z = artwconstT.mask[i].pos [0];
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconstT.mask[i].pos [1] = s;
                    artwconstT.mask[i].pos [0] = z;
                    s = artwconstT.mask[i].length;
                    z = artwconstT.mask[i].rows;
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconstT.mask[i].length = s;
                    artwconstT.mask[i].rows = z;
        }
        for (i = 0; i < artwconst.fieldanz; i ++)
        {
                    s = artwconst.mask[i].pos [1];
                    z = artwconst.mask[i].pos [0];
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconst.mask[i].pos [1] = s;
                    artwconst.mask[i].pos [0] = z;
                    s = artwconst.mask[i].length;
                    z = artwconst.mask[i].rows;
                    KonvTextAbsPos (artwconst.font,
                                    &z, &s);
                    artwconst.mask[i].length = s;
                    artwconst.mask[i].rows = z;
        }
}

void PosArtFormP (void)
/**
Position fuer artform festlegen.
**/
{
        static int posOK = 0;
        int i, s, z;

        if (posOK) return;

        posOK = 1;
        for (i = 0; i < artwformT.fieldanz; i ++)
        {
                    s = artwformT.mask[i].pos [1];
                    z = artwformT.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].pos [1] = s;
                    artwformT.mask[i].pos [0] = z;
                    s = artwformT.mask[i].length;
                    z = artwformT.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwformT.mask[i].length = s;
                    artwformT.mask[i].rows = z;
        }
        for (i = 0; i < artwform.fieldanz; i ++)
        {
                    s = artwform.mask[i].pos [1];
                    z = artwform.mask[i].pos [0];
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].pos [1] = s;
                    artwform.mask[i].pos [0] = z;
                    s = artwform.mask[i].length;
                    z = artwform.mask[i].rows;
                    KonvTextAbsPos (artwform.font,
                                    &z, &s);
                    artwform.mask[i].length = s;
                    artwform.mask[i].rows = z;
        }
}

void PosMeForm (int yw, int xw, int cyw, int cxw)
/**
Position fuer WiegKgMe festlegen.
**/
{
        static int posOK = 0;
        int s, z;

        if (posOK) return;

        posOK = 1;

        WiegKgMe.mask[0].pos[0] = yw + WiegKgE.mask[0].pos[0];
        WiegKgMe.mask[0].pos[1] = xw + cxw + 30;
        s = WiegKgMe.mask[0].length;
        z = WiegKgMe.mask[0].rows;
        KonvTextAbsPos (WiegKgMe.font, &z, &s);
        WiegKgMe.mask[0].length = s;
        WiegKgMe.mask[0].rows = z;
}

void GetZentrierWerte (int *buglen, int *buabs, int wlen, int anz,
                       int abs, int size)
/**
Werte fuer Zentrierung ermitteln.
**/
{
        int blen;
        int spos;

        while (TRUE)
        {
                 blen = anz * (size + abs) - abs;
                 spos = (wlen - blen) / 2;
                 if (spos > 9)
                 {
                              *buglen = blen;
                              *buabs  = abs + size;
                              return;
                 }
                 abs -= 10;
                 if (abs <= 0) return;
       }
}

int closetara (void)
/**
Tara-Leiste schliessen.
**/
{
        CtrMode = 0;
        CloseControls (&WieButtonT);
        current_wieg = 0;
        if (ohne_preis)
        {
                 wiegend = 5;
        }
        else
        {
                 wiegend = 6;
        }
        return 0;
}


void testeti (void)
/**
Wiegen durchfuehren.
**/
{
		double a;
		int dsqlstatus;

		a = ratod (aufps.a);

		DbClass.sqlin ((long *)   &sys_peri.sys, 2, 0);
		DbClass.sqlin ((double *) &a, 3, 0);
        DbClass.sqlout ((long *) &eti_nr, 2, 0);
        DbClass.sqlout ((char *) krz_txt, 0, 17);
		dsqlstatus = DbClass.sqlcomm ("select eti_nr, krz_txt from klebdest "
			                          "where sys = ? "
									  "and a = ? "
									  "and wa = 1 "
									  "and einz = 1");
		if (dsqlstatus)
		{
			WithEti = FALSE;
		}
		else
		{
			WithEti = TRUE;
		}

		DbClass.sqlin ((long *)   &sys_peri.sys, 2, 0);
		DbClass.sqlin ((double *) &a, 3, 0);
        DbClass.sqlout ((long *) &sum_eti_nr, 2, 0);
        DbClass.sqlout ((char *) sum_krz_txt, 0, 17);
		dsqlstatus = DbClass.sqlcomm ("select eti_nr, krz_txt from klebdest "
			                          "where sys = ? "
									  "and a = ? "
									  "and wa = 1 "
									  "and summ = 1");
		if (dsqlstatus)
		{
			WithSumEti = FALSE;
		}
		else
		{
			WithSumEti = TRUE;
		}
		if (WithEti == FALSE && WithSumEti == FALSE)
		{
			EtiButton.mask[0].attribut = REMOVED;
			EtiButton.mask[1].attribut = REMOVED;
			wiegend = 6;
		}
		else
		{
			EtiButton.mask[0].attribut = COLBUTTON;
			EtiButton.mask[1].attribut = COLBUTTON;
			wiegend = 8;
		}
		if (WithEti)
		{
            ActivateColButton (NULL, &EtiButton, 0, 2, 0);
		}
		else
		{
            ActivateColButton (NULL, &EtiButton, 0, -1, 0);
		}

		if (WithSumEti)
		{
            ActivateColButton (NULL, &EtiButton, 1, 2, 0);
		}
		else
		{
            ActivateColButton (NULL, &EtiButton, 1, -1, 0);
		}
}


void WriteParafile (char *tmp)
/**
Parameter-Datei fuer put_eti schreiben.
**/
{
	    char dname [512];
		FILE *pa;

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$KUN;%ld\n",       lsk.kun);
		fprintf (pa, "$$LIEFNR;%ld\n",    lsk.ls);
		fprintf (pa, "$$ABEZ1;%s\n",      aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",      aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",   ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",      lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  eti_stk );
		fprintf (pa, "$$KUNNAME;%s\n",    kun_krz1);
		fclose (pa);

}

void WriteSumParafile (char *tmp)
/**
Parameter-Datei fuer put_eti schreiben.
**/
{
	    char dname [512];
		FILE *pa;

		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		fprintf (pa, "$$KUN;%ld\n",       lsk.kun);
		fprintf (pa, "$$LIEFNR;%ld\n",    aufs.ret);
		fprintf (pa, "$$ABEZ1;%s\n",      aufps.a_bz1);
		fprintf (pa, "$$ABEZ2;%s\n",      aufps.a_bz2);
		fprintf (pa, "$$ARTNR;%.0lf\n",   ratod (aufps.a));
		fprintf (pa, "$$WEDAT;%s\n",      lieferdatum);
		fprintf (pa, "$$NETTOG;%.3lf\n",  sum_eti_netto);
		fprintf (pa, "$$TARAG;%.3lf\n",   sum_eti_tara);
		fprintf (pa, "$$BRUTTOG;%.3lf\n", sum_eti_brutto);
		fprintf (pa, "$$STUECK;%.2lf\n",  sum_eti_stk );
		fprintf (pa, "$$ANZAHL;%ld\n",    atol (wiegzahl));
		fprintf (pa, "$$KUNNAME;%s\n",    kun_krz1);
		fclose (pa);

}


int doetikett (void)
/**
Wiegen durchfuehren.
**/
{
	    char progname [512];
		char *etc;
		int etianz;
		char *tmp;


        if (WithEti == FALSE) return 0;

		etc = getenv ("BWSETC");
		if (etc == NULL) etc = "c:\\user\\fit\\etc";

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";

		WriteParafile (tmp);

		etianz = 1;
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\eti%04d.prm %s\\parafile %d",
                                     rosipath, krz_txt, etc, eti_nr, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);

        return 0;
}


int dosumeti (void)
/**
Wiegen durchfuehren.
**/
{
	    char progname [256];
		char *etc;
		int etianz;
		char *tmp;


        if (WithSumEti == FALSE) return 0;

		etc = getenv ("BWSETC");
		if (etc == NULL) etc = "c:\\user\\fit\\etc";

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";

		WriteSumParafile (tmp);

		etianz = 1;
        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\eti%04d.prm %s\\parafile %d",
                                     rosipath, sum_krz_txt, etc, sum_eti_nr, tmp, etianz);
        ProcWaitExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);

        return 0;
}


int dotara (void)
/**
Wiegen durchfuehren.
**/
{
        if (WieTara.aktivate == -1) return 0;

        if (WorkModus == AUSZEICHNEN)
        {
                        dogebinde ();
                        return (0);
        }
        CtrMode = 1;
        CloseControls (&WieButton);
        current_wieg = 0;
        CtrMode = 1;
        wiegend = 6;
        return 0;
}

int dopreis (void)
/**
Wiegen durchfuehren.
**/
{
        HWND fhWnd;

        fhWnd = GetFocus ();
        EnterPreisBox (WiegWindow, -1, WiegY - 86);
        SetFocus (fhWnd);
        return 0;
}

void ArtikelHand (void)
/**
Artikel-Menge von Hand eingeben.
**/
{
        double alt;
        double netto;

        StornoSet = 0;
        SetStorno (dostorno, 0);
        sprintf (WiegMe, "%.3lf", (double) 0);
        EnterCalcBox (eWindow,"  Anzahl  ",  WiegMe, 12, WiegKgE.mask[0].picture);
        if (syskey != KEY5)
        {
              strcpy (aufps.erf_kz, "H");
              if (StornoSet)
              {
                  stornoaction ();
              }
              else
              {
                  netto = (double) ratod (WiegMe);
                  alt = (double) ratod (auf_me);
                  alt -= netto;
                  sprintf (auf_me, "%3lf", alt);
                  alt = (double) ratod (aufps.lief_me);
                  alt += netto;
                  sprintf (aufps.lief_me, "%3lf", alt);
                  strcpy (aufps.s, "3");
              }
        }
}

char *GenCharge (void)
/**
Chargen-Nummer generieren.
**/
{
	    static char cha [21];
		char datum [12];
		long datl;
		int wk;
		char yy [5];
		char ww [3];

//		sysdate (datum);
//		datl = dasc_to_long (datum);
		datl = lsk.lieferdat;
		datl += 2;
		dlong_to_asc (datl, datum);
		strcpy (yy, &datum[8]);
		wk = get_woche (datum);
		sprintf (ww, "%02d", wk);
		sprintf (cha, "%s%s",yy,ww);
		return cha;
}


void TestCharge (void)
/**
Parameter fuer die Chargennummer bearbeiten.
**/
{

        return;
	    if (ls_charge_par == 0 || lsc2_par == 0)
		{
			return;
		}
		if (_a_bas.charg_hand == 0)
		{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
		}
		else if (_a_bas.charg_hand == 1)
		{
			    artwformW.mask[0].attribut = EDIT;
			    artwformP.mask[0].attribut = EDIT;
				if (strcmp (aufps.ls_charge, " ") <= 0)
				{
					strcpy (aufps.ls_charge,last_charge);
				}
		}
		else if (_a_bas.charg_hand == 2)
		{
			    artwformW.mask[0].attribut = READONLY;
			    artwformP.mask[0].attribut = READONLY;
				strcpy (aufps.ls_charge, GenCharge ());
		}
}



void ArtikelWiegen (HWND hWnd)
/**
Artikel wiegen.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
        int xw, yw, cxw, cyw;
        int xc, yc, cxc, cyc;
        int wlen, wrows;
        int buglen, buabs;
        int z, s;
        static int first = 0;

        if (WiegenAktiv) return;

		if (lsk.kun_fil == 0)
		{
			strcpy (KF, KFK);
		}
		else if (lsk.kun_fil == 1)
		{
			strcpy (KF, KFF);
		}

        sum_eti_netto  = (double) 0.0 ;
        sum_eti_brutto = (double) 0.0;
        sum_eti_tara   = (double) 0.0;
        sum_eti_stk    = (double) 0.0;
        testeti ();
        SwitchLiefAuf ();
		TestCharge ();
        if (WorkModus == AUSZEICHNEN)
        {
//               WieWie.text1 = "Auszeichn.";
               strcpy (summe1, "0");
               strcpy (summe2, "0");
               strcpy (summe3, "0");
               strcpy (zsumme1, "0");
               strcpy (zsumme2, "0");
               strcpy (zsumme3, "0");
			   sprintf (vorgabe3, "%.3lf", ratod (aufps.auf_me) - ratod (aufps.lief_me3));
			   if (ratod (vorgabe3) < (0.0))
			   {
				   sprintf (vorgabe3, "%.3lf", (double) 0.0);
			   }
			   if ((ratod (aufps.lief_me3) >=  ratod (aufps.auf_me)) &&
				    (ratod (aufps.auf_me) > (double) 0.0))
			   {
				   disp_messG ("Achtung die Sollmenge ist erreicht", 2);
			   }

               WieWie.text1 = "Aus-";
               WieWie.text2 = "zeichnen";
               WieWie.tx1 = -1;
               WieWie.ty1 = 25;
               WieWie.tx2 = -1;
               WieWie.ty2 = 50;
//               WieTara.aktivate = -1;
               WieTara.text1 = "Gebinde";
               WieTara.text2 = "Tara";
               WieTara.tx1 = -1;
               WieTara.ty1 = 25;
               WieTara.tx2 = -1;
               WieTara.ty2 = 50;
               fetch_std_geb ();
               memcpy (&artwformT,&artwformTP, sizeof (form));
               memcpy (&artwform,&artwformP, sizeof (form));
               PosArtFormP ();
        }
        else
        {
               WieWie.text1 = "Wiegen";
               WieWie.text2 = NULL;
               WieWie.tx1 = -1;
               WieWie.ty1 = -1;
               WieWie.tx2 = 0;
               WieWie.ty2 = 0;
//               WieTara.aktivate = 2;
               WieTara.text1 = "Tara";
               WieTara.text2 = NULL;
               WieTara.tx1 = -1;
               WieTara.ty1 = -1;
               WieTara.tx2 = 0;
               WieTara.ty2 = 0;
               memcpy (&artwformT,&artwformTW, sizeof (form));
               memcpy (&artwform,&artwformW, sizeof (form));
               PosArtFormW ();
 		       strcpy (wiegzahl, "0");
        }
//        autoan = 0;
//        AutoTara.aktivate = 3;
        WieButton.mask[1].BuId = 0;
        CtrMode = 0;

        AutoTara.text3 = anaus [autoan];
        SetListEWindow (0);
        strcpy (WiegMe, "0.000");
        sprintf (summe1, "%.0lf", (double) 0);
        sprintf (summe2, "%.0lf", (double) 0);
        sprintf (zsumme1, "0");
        sprintf (zsumme2, "0");
        AktWiegPos = 0;
        RegisterWieg ();
        GetWindowRect (hMainWindow, &rect);
        x = rect.left;
        y = rect.top + 24;
        cx = rect.right - rect.left;
        cy = rect.bottom - rect.top - 24;

        PosArtFormC ();
        wlen = strlen (WiegText);
        FontTextLen (WiegKgT.font, WiegText, &wlen,
                                             &wrows);
        WiegKgT.mask[0].length = wlen;
        WiegKgT.mask[0].rows   = wrows;

        WiegKgE.mask[0].pos[0]  = WiegKgT.mask[0].pos[0] + 2 * wrows;

        wlen = 11;
        FontTextLen (WiegKgE.font, "0000000,000", &wlen,
                                                  &wrows);

        WiegKgE.mask[0].length = wlen;
        WiegKgE.mask[0].rows   = wrows;

        if (wlen > WiegKgT.mask[0].length)
        {
                   cxw = wlen;
        }
        else
        {
                   cxw = WiegKgT.mask[0].length;
        }

        cxw += 20;

        cyw = WiegKgE.mask[0].pos[0] + wrows + wrows / 2 + 10;
        xw = (cx - cxw) / 2;
        yw = (cy - cxw) / 2 + 120;

        PosMeForm (yw, xw, cyw, cxw);

        if (first == 0)
        {
                  ftaratxt.mask[0].pos[1] = xw;
                  ftaratxt.mask[0].pos[0] = yw - 40;

                  z = yw - 40;
                  s = xw;
                  GetTextPos (ftaratxt.font, &z, &s);
                  s += 7;
                  KonvTextAbsPos (ftaratxt.font, &z, &s);
                  ftara.mask[0].pos[0] = z + 10;
                  ftara.mask[0].pos[1] = s;

                  s = strlen (ftaratxt.mask[0].feld);
                  FontTextLen (ftaratxt.font, ftaratxt.mask[0].feld, &s, &z);
                  ftaratxt.mask[0].length = s;
                  ftaratxt.mask[0].rows   = z;

                  s = 8;
                  FontTextLen (ftara.font, "0000.000", &s, &z);
                  ftara.mask[0].length = s;
                  ftara.mask[0].rows   = z;
                  first = 1;
        }

        WiegWindow  = CreateWindowEx (
                              0,
                              "AufWieg",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              // hMainWindow,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);
        if (WiegWindow == NULL) return;
        EnableWindow (hMainWindow, FALSE);

        WiegY = yw + cyw;
        wiegx  = xw;
        wiegy  = yw;
        wiegcx = cxw;
        wiegcy = cyw;

        WiegKg  = CreateWindowEx (
                              0,
                              "AufWiegKg",
                              "",
                              WS_CHILD | WS_BORDER,
                              xw, yw,
                              cxw, cyw,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        cxc = cx - 20;
        cyc = 150;
        xc  = 10;
        yc  = cy - 160;

        if (ohne_preis)
        {
              WieButton.mask[1].attribut = REMOVED;
              GetZentrierWerte (&buglen, &buabs, cxc, 5, babs, bsize);
              WieButton.mask[0].pos[1] = (cxc - buglen) / 2;
              WieButton.mask[2].pos[1] = WieButton.mask[0].pos[1] + buabs;
              WieButton.mask[3].pos[1] = WieButton.mask[2].pos[1] + buabs;
              WieButton.mask[4].pos[1] = WieButton.mask[3].pos[1] + buabs;
              WieButton.mask[5].pos[1] = WieButton.mask[4].pos[1] + buabs;
        }
        else
        {
              WieButton.mask[1].attribut = COLBUTTON;
              GetZentrierWerte (&buglen, &buabs, cxc, 6, babs, bsize);
              WieButton.mask[0].pos[1] = (cxc - buglen) / 2;
              WieButton.mask[1].pos[1] = WieButton.mask[0].pos[1] + buabs;
              WieButton.mask[2].pos[1] = WieButton.mask[1].pos[1] + buabs;
              WieButton.mask[3].pos[1] = WieButton.mask[2].pos[1] + buabs;
              WieButton.mask[4].pos[1] = WieButton.mask[3].pos[1] + buabs;
              WieButton.mask[5].pos[1] = WieButton.mask[4].pos[1] + buabs;
        }

        GetZentrierWerte (&buglen, &buabs, cxc, 6, babs2, bsize2);
        WieButtonT.mask[0].pos[1] = (cxc - buglen) / 2;
        WieButtonT.mask[1].pos[1] = WieButtonT.mask[0].pos[1] + buabs;
        WieButtonT.mask[2].pos[1] = WieButtonT.mask[1].pos[1] + buabs;
        WieButtonT.mask[3].pos[1] = WieButtonT.mask[2].pos[1] + buabs;
        WieButtonT.mask[4].pos[1] = WieButtonT.mask[3].pos[1] + buabs;
        WieButtonT.mask[5].pos[1] = WieButtonT.mask[4].pos[1] + buabs;

        WiegCtr  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "AufWiegCtr",
                              "",
                              WS_CHILD,
                              xc, yc,
                              cxc, cyc,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);

        ShowWindow (WiegWindow, SW_SHOW);
        UpdateWindow (WiegWindow);
        ShowWindow (WiegKg, SW_SHOW);
        UpdateWindow (WiegKg);
        ShowWindow (WiegCtr, SW_SHOW);
        UpdateWindow (WiegCtr);
        create_enter_form (WiegWindow, &artwform, 0, 0);
        create_enter_form (WiegWindow, &artwconst, 0, 0);

        display_form (WiegWindow, &artwconst, 0, 0);
        current_wieg = TestAktivButton (PLUS, 0);
        SetFocus (WieButton.mask[current_wieg].feldid);

        WiegenAktiv = 1;
        current_wieg = 0;
        break_wieg = 0;

/** Test  **/

/*
      Sleep (250);
  	  break_wieg = 1;
*/


/** Test Ende **/

        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsWiegMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_wieg) break;
        }
        if (AlarmWindow)
        {
                     CloseControls (&alform);
                     DestroyWindow (AlarmWindow);
                     AlarmWindow = NULL;
        }
        EnableWindow (hMainWindow, TRUE);
        CloseFontControls (&WiegKgT);
        CloseFontControls (&WiegKgE);
        CloseFontControls (&WieButton);
        CloseControls (&EtiButton);
        CloseFontControls (&artwform);
        CloseFontControls (&artwformT);
        CloseFontControls (&artwconst);
        CloseFontControls (&artwconstT);
        CloseFontControls (&WiegKgMe);
        CloseFontControls (&ftara);
        CloseFontControls (&ftaratxt);
        DestroyWindow (WiegWindow);
        WiegenAktiv = 0;
}

/** Ab hier Auswahl fuer Preiseingabe
**/

static HWND ePreisWindow;
static break_preis_enter = 0;


mfont PreisTextFont = {"Arial", 200, 0, 1, RGB (0, 0, 0),
                                         RGB (255, 255, 255),
                                         1,
                                         NULL};


mfont preisfont   = {"", 120, 0, 1,
                     RGB (255, 0, 0),
                     BLUECOL,
                     1,
                     NULL};


ColButton BuVkPreis =   {"&Rampen", -1, 25,
                         "Preis", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (255, 255, 255),
                         BLUECOL,
                         2};

ColButton BuLadPreis =   {"&Laden", -1, 25,
                          "Preis", -1, 60,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         NULL, 0, 0,
                         RGB (0, 0, 0),
                         YELLOWCOL,
                         2};

ColButton BuPreisOK =   {"&Zur�ck", -1, -1,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          NULL, 0, 0,
                          RGB (255, 255, 255),
                          REDCOL,
                          2};

static int dovk ();
static int dolad ();
static int doprok ();

static HWND fhWnd;
static int pnbss0 = 80;
int pny    = 3;
int pnx    = 1;
static int pcbss0 = 100;
static int pcbss  = 64;
static int pcbsz  = 64;
static int pcbsz0 = 100;

static field _PrWahl[] = {
(char *) &BuVkPreis,      pcbss0, pcbsz0, pny, pnx,        0, "",
                                                           COLBUTTON, 0,
                                                           dovk, 0,
(char *) &BuLadPreis,     pcbss0, pcbsz0, pny, pnx + 1 * pcbss0, 0, "",
                                                           COLBUTTON, 0,
                                                           dolad, 0,
(char *) &BuPreisOK,      pcbss0, pcbsz0, pny, pnx + 2 * pcbss0 , 0, "",
                                                           COLBUTTON, 0,
                                                           doprok, 0};

static form PrWahl = {3, 0, 0, _PrWahl, 0, 0, 0, 0, &preisfont};

static char preisbuff [256];

static field _PrTextForm [] = {
preisbuff,             0, 0, 0, 0, 0, "", DISPLAYONLY, 0, 0, 0};

static form PrTextForm = {1, 0, 0, _PrTextForm, 0, 0, 0, 0, &PreisTextFont};

void PreisEnterBreak ()
/**
Endeflag fuer Erfassung setzen.
**/
{
        break_preis_enter = 1;
}

int dovk (void)
{
        char preis [20];

        strcpy (preis, aufps.auf_pr_vk);
        EnterCalcBox (ePreisWindow,"  Rampen-Preis  ", preis, 10, preispicture);
        SetActiveWindow (ePreisWindow);
        if (ratod (preis) > (double) 0.0)
        {
                   strcpy (aufps.auf_pr_vk, preis);
                   if (InsWindow)
                   {
                             InvalidateRect (InsWindow, NULL, TRUE);
                             UpdateWindow (InsWindow);
                   }
        }
        SetFocus (fhWnd);
        return 0;
}

int dolad (void)
{
        char preis [20];


        strcpy (preis, aufps.auf_lad_pr);
        EnterCalcBox (ePreisWindow,"  Laden-Preis  ", preis, 8, "%6.2f");
        if (ratod (preis) > (double) 0.0)
        {
                   strcpy (aufps.auf_lad_pr, preis);
                   if (InsWindow)
                   {
                             InvalidateRect (InsWindow, NULL, TRUE);
                             UpdateWindow (InsWindow);
                   }
        }
        SetActiveWindow (ePreisWindow);
        SetFocus (fhWnd);
        return 0;
}

int doprok (void)
{
        PreisEnterBreak ();
        return 0;
}

HWND IsPreisChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < PrWahl.fieldanz; i ++)
        {
                   if (MouseinWindow (PrWahl.mask[i].feldid,
                                      mousepos))
                   {
                                 return (PrWahl.mask[i].feldid);
                   }
        }

        return NULL;
}


LONG FAR PASCAL PreisProc(HWND hWnd,UINT msg,
                          WPARAM wParam,LPARAM lParam)
{
        POINT mousepos;
//        HWND enchild;
        static int SendChild = 0;
        int HasFocus = 1;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    display_form (ePreisWindow, &PrWahl, 0, 0);
                    SetFocus (fhWnd);
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
/*
                    enchild = IsPreisChild (&mousepos);
                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
*/
                    return TRUE;
              case WM_ACTIVATE :
                    switch (wParam)
                    {
                         case WA_ACTIVE :
                         case WA_CLICKACTIVE :
                              break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

void RegisterPreis (void)
/**
Fenster fuer Preis registrieren.
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, FITICON);
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  PreisProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  CreateSolidBrush (LTGRAYCOL);
                   wc.lpszClassName =  "EnterPreis";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

int PrGetAktButton (void)
/**
Aktiven Button holen.
**/
{
        HWND hWnd;
        int i;

        hWnd = GetFocus ();

        for (i = 0; i < PrWahl.fieldanz; i ++)
        {
                    if (hWnd == PrWahl.mask[i].feldid)
                    {
                               return (i);
                    }
        }
        return (0);
}


void PrPrevKey (void)
/**
Vorhergenden Button aktivieren.
**/
{
         int AktButton;

         AktButton = PrGetAktButton ();
         if (AktButton == 0)
         {
                     AktButton = PrWahl.fieldanz - 1;
         }
         else
         {
                     AktButton --;
         }
         SetFocus (PrWahl.mask [AktButton].feldid);
         fhWnd = GetFocus ();
}

void PrNextKey (void)
/**
Naechsten Button aktivieren.
**/
{
         int AktButton;

         AktButton = PrGetAktButton ();
         if (AktButton == PrWahl.fieldanz - 1)
         {
                     AktButton = 0;
         }
         else
         {
                     AktButton ++;
         }
         SetFocus (PrWahl.mask [AktButton].feldid);
         fhWnd = GetFocus ();
}

void PrActionKey (void)
/**
Button-Aktion ausfuehren.
**/
{
         int AktButton;
         field *feld;

         AktButton = PrGetAktButton ();
         feld =  &PrWahl.mask[AktButton];

         if (feld->after != (int (*) (void)) 0)
         {
                    (*feld->after) ();
         }
         else if (feld->BuId != 0)
         {
                    SendMessage (GetParent (feld->feldid), WM_COMMAND,
                                            MAKELONG (feld->BuId,
                                            feld->feldid),
                                            0l);
         }
}

int IsPreisKey (MSG *msg)
/**
HotKey Testen.
**/
{
         static char keypressed = 0;
         static HWND keyhwnd = 0;
         ColButton *ColBut;
         int taste;
         int i;
         char *pos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     if (keypressed)
                     {
                                  return FALSE;
                     }
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                               PreisEnterBreak ();
                               syskey = KEY5;
                               return TRUE;
                            case  VK_UP :
                               PrPrevKey ();
                               return TRUE;
                            case VK_DOWN :
                               PrNextKey ();
                               return TRUE;
                            case VK_RIGHT :
                               PrNextKey ();
                               return TRUE;
                            case VK_LEFT :
                               PrPrevKey ();
                               return TRUE;
                            case VK_TAB :
                               if (GetKeyState (VK_SHIFT) < 0)
                               {
                                      PrPrevKey ();
                                      return TRUE;
                               }
                               PrNextKey ();
                               return TRUE;
                             case VK_RETURN :
                               PrActionKey ();
                               return TRUE;
                     }
                     taste = (int) msg->wParam;
                     for (i = 0; i < 3; i ++)
                     {
                        ColBut = (ColButton *) PrWahl.mask[i].feld;
                        if (ColBut->text1)
                        {
                           if ((pos = strchr (ColBut->text1, '&')) &&
                               ((UCHAR) toupper (*(pos + 1)) == taste))
                           {
                                        keypressed = 1;
                                        keyhwnd = PrWahl.mask[i].feldid;
                                        SendMessage (keyhwnd,
                                                WM_LBUTTONDOWN,
                                                0l,
                                                0l);
                                        return TRUE;
                            }
                       }
                     }
                     return FALSE;
              }
              case WM_KEYUP :
              {
                     taste = (int) msg->wParam;
                     for (i = 0; i < 3; i ++)
                     {
                         if (keyhwnd == PrWahl.mask[i].feldid)
                         {
                                   keypressed = 0;
                                   SendMessage (keyhwnd,
                                                WM_LBUTTONUP,
                                                0l,
                                                0l);
                                   keyhwnd = NULL;
                                   return TRUE;
                         }
                     }
                     return FALSE;
              }
          }
          return FALSE;
}


void EnterPreisBox (HWND hWnd, int px, int py)
/**
Fenster fuer Preisauswahl oeffnen.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;
        HCURSOR oldcursor;
        static int BitMapOK = 0;

        RegisterPreis ();
        GetWindowRect (hWnd, &rect);
        cx = 3 * pcbss0 + 7;
        cy = pcbsz0 + 10;
        if (px == -1)
        {
                  x = rect.left + (rect.right - rect.left - cx) / 2;
        }
        else
        {
                  x = px;
        }
        if (py == -1)
        {
                  y = (rect.bottom - cy) / 2;
        }
        else
        {
                  y = py;
        }

        ePreisWindow  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "EnterPreis",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hWnd,
                              NULL,
                              hMainInst,
                              NULL);

        ShowWindow (ePreisWindow, SW_SHOW);
        UpdateWindow (ePreisWindow);
        oldcursor = SetCursor (LoadCursor (NULL, IDC_ARROW));
        SetFocus (PrWahl.mask[0].feldid);
        fhWnd = GetFocus ();

		EnableWindow (hWnd, FALSE);
		EnableWindow (hMainWindow, FALSE);
        PreisAktiv = 1;
        break_preis_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsPreisKey (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_preis_enter) break;
        }
		EnableWindow (hWnd, TRUE);
		EnableWindow (hMainWindow, TRUE);
        CloseControls (&PrWahl);
        DestroyWindow (ePreisWindow);
        PreisAktiv = 0;
        SetCursor (oldcursor);
}

/*

Auswahl fuer Festtara.

*/

struct PT
{
        char ptbez [33];
        char ptwer1 [9];
};

struct PT ptab, ptabarr [20];
static int ptabanz = 0;


static int ptidx;

mfont PtFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};

static field _ftaraform [] =
{
        ptab.ptbez,     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptab.ptwer1,     9, 1, 0,13, 0, "%7.3f", DISPLAYONLY, 0, 0, 0
};

static form ftaraform = {2, 0, 0, _ftaraform, 0, 0, 0, 0, &PtFont};

static field _ftaraub [] =
{
        "Bezeichnung",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Tara",             9, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0
};

static form ftaraub = {2, 0, 0, _ftaraub, 0, 0, 0, 0, &PtFont};

static field _ftaravl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
};

static form ftaravl = {1, 0, 0, _ftaravl, 0, 0, 0, 0, &PtFont};

int ShowFestTara (char *pttara)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (ptabanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("tara_fest");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabarr[ptabanz].ptbez,  ptabn.ptbez);
                   strcpy (ptabarr[ptabanz].ptwer1, ptabn.ptwer1);
                   ptabanz ++;
                   if (ptabanz == 20) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        EnableWindow (WiegWindow, FALSE);
        ptidx = ShowPtBox (WiegWindow, (char *) ptabarr, ptabanz,
                     (char *) &ptab,(int) sizeof (struct PT),
                     &ftaraform, &ftaravl, &ftaraub);
        EnableWindow (WiegWindow, TRUE);
        strcpy (pttara, ptabarr [ptidx].ptwer1);
        return ptidx;
}

/*

Auswahl fuer Drucker.

*/

struct DR
{
        char drucker [33];
};

struct DR ldrucker, ldruckarr [20];
static int ldruckanz = 0;
static int ldidx;

// mfont DrFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
mfont DrFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                               RGB (255, 255, 255),
                               1,
                               NULL};
static field _ldruckform [] =
{
        ldrucker.drucker,    22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ldruckform = {1, 0, 0, _ldruckform, 0, 0, 0, 0, &DrFont};

static field _ldruckub [] =
{
        "Drucker",     22, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form ldruckub = {1, 0, 0, _ldruckub, 0, 0, 0, 0, &DrFont};

void BelegeGdiDrucker (void)
/**
Drucker Win.ini holen
**/
{
       int i;
	   char *pr;
//	   char AktDevice [80];
       char ziel [256];
       char quelle [256];
       char *frmenv;

       frmenv = getenv ("FRMPATH");
       if (frmenv == NULL) return;

       GetAllPrinters ();

       i = 0;
       while (TRUE)
       {
                pr = GetNextPrinter ();
                if (pr == NULL) break;
                strcpy (ldruckarr[i].drucker, pr);
                i ++;
       }
       ldruckanz = i;

       sprintf (quelle, "%s\\%s", getenv ("FRMPATH"), "gdi.cfg");
       sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
       CopyFile (quelle, ziel, FALSE);
       sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 clipped (sys_ben.pers_nam),
                                 "akt");
       CopyFile (quelle, ziel, FALSE);

}



void BelegeDrucker (void)
/**
Drucker aus Formatverzeichnis holen.
**/
{
      char frmdir [256];
      char *frmenv;
      WIN32_FIND_DATA lpffd;
      HANDLE hFile;
      char *p;
      int i;
      static int druckerOK = 0;

      if (druckerOK) return;

      druckerOK = 1;
      if (GraphikMode)
      {
               BelegeGdiDrucker ();
               return;
      }

      frmenv = getenv ("FRMPATH");
      if (frmenv == NULL) return;

      sprintf (frmdir, "%s\\*.cfg", frmenv);


      hFile = FindFirstFile (frmdir, &lpffd);

      if (hFile == INVALID_HANDLE_VALUE)
      {
                      return;
      }

      i = 0;
      p = strchr (lpffd.cFileName, '.');
      if (p) *p = (char) 0;

      strcpy (ldruckarr[i].drucker, lpffd.cFileName);
      i ++;

      while (FindNextFile (hFile, &lpffd))
      {
               p = strchr (lpffd.cFileName, '.');
               if (p) *p = (char) 0;
               strcpy (ldruckarr[i].drucker, lpffd.cFileName);
               i ++;
      }
      FindClose (hFile);

      ldruckanz = i;
}

void drucker_akt (char *dname)
/**
Ausgewaehlten Drucker in drucker.akt_uebertragen.
**/
{
        char ziel [256];
        char quelle [256];

		if (GraphikMode)
		{
		        SetPrinter (dname);
				return;
		}
        sprintf (quelle, "%s\\%s.cfg", getenv ("FRMPATH"), dname);
        sprintf (ziel, "%s\\%s", getenv ("FRMPATH"), "drucker.akt");
        CopyFile (quelle, ziel, FALSE);
        sprintf (ziel, "%s\\%s.%s", getenv ("FRMPATH"),
                                 clipped (sys_ben.pers_nam),
                                 "akt");
        CopyFile (quelle, ziel, FALSE);
}

int DruckChoise ()
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        char aktdrucker [80];

        BelegeDrucker ();
        ldidx = ShowPtBox (eWindow, (char *) ldruckarr, ldruckanz,
                     (char *) &ldrucker,(int) sizeof (struct DR),
                     &ldruckform, NULL, &ldruckub);
        strcpy (aktdrucker, ldruckarr [ldidx].drucker);
        clipped (aktdrucker);
        drucker_akt (aktdrucker);
        CloseControls (&testub);
		display_form (Getlbox (), &testub, 0, 0);
        return ldidx;
}


/** Ptabanzeige
**/

/*
static int ptbss0 = 73;
static int ptbss  = 77;
static int ptbsz  = 40;
static int ptbsz0 = 36;
*/

static int ptbss0 = 103;
static int ptbss  = 107;
static int ptbsz  = 60;
static int ptbsz0 = 56;

field _PtButton[] = {
(char *) &Back,         ptbss0, ptbsz0, -1, 4+0*ptbss, 0, "", COLBUTTON, 0,
 *                                                        func5, 0,
(char *) &BuOK,         ptbss0, ptbsz0, -1, 4+1*ptbss, 0, "", COLBUTTON, 0,
 *                                                        func6, 0,
(char *) &PfeilU,       ptbss0, ptbsz0, -1, 4+2*ptbss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,       ptbss0, ptbsz0, -1, 4+3*ptbss, 0, "", COLBUTTON, 0,
                                                          func2, 0
};

form PtButton = {4, 0, 0, _PtButton, 0, 0, 0, 0, &buttonfont};


void IsPtClck (int idx)
/**
Reaktion auf Doppelclick in der Liste.
**/
{
        ptidx = idx;
        break_list ();
        return;
}

void RegisterPt (void)
/**
Kopf - und Fuss - Fenster fuer Liste registrieren
**/
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered == 0)
        {
                   wc.style =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                               | CS_OWNDC;
                   wc.cbClsExtra    =  0;
                   wc.cbWndExtra    =  0;
                   wc.hInstance     =  hMainInst;
                   wc.hIcon         =  LoadIcon (hMainInst, FITICON);
                   wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
                   wc.lpszMenuName  =  "";
                   wc.lpfnWndProc   =  PtFussProc;
                   wc.lpszMenuName  =  "";
                   wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
                   wc.lpszClassName =  "PtFuss";
                   RegisterClass(&wc);
                   registered = 1;
        }
}

HWND IsPtFussChild (POINT *mousepos)
/**
Test, ob sich der Maus-Cursor in einem KindFensert befinden.
**/
{
        int i;

        for (i = 0; i < PtButton.fieldanz; i ++)
        {
                   if (MouseinWindow (PtButton.mask[i].feldid,
                                      mousepos))
                   {
                                 return (PtButton.mask[i].feldid);
                   }
        }
        return NULL;
}

int MouseInPt (POINT *mpos)
{
        HWND hListBox;

        hListBox = GethListBox ();

        if (MouseinDlg (hListBox, mpos)) return TRUE;
        if (MouseinDlg (PtFussWindow, mpos)) return TRUE;
        return FALSE;
}


LONG FAR PASCAL PtFussProc(HWND hWnd,UINT msg,
                           WPARAM wParam,LPARAM lParam)
{
        HWND enchild;
        POINT mousepos;
        HWND lbox;

        GetCursorPos (&mousepos);

        switch(msg)
        {
              case WM_PAINT :
                    display_form (PtFussWindow, &PtButton, 0, 0);
                    break;
              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
//              case WM_MOUSEMOVE :
                    enchild = IsPtFussChild (&mousepos);
                    MouseTest (enchild, msg);
                    if (enchild)
                    {
                              SendMessage (enchild, msg, wParam, lParam);
                              return TRUE;
                    }
                    lbox = Getlbox ();
                    if (MouseinWindow (lbox, &mousepos))
                    {
                              ScreenToClient (lbox, &mousepos);
                              lParam = MAKELONG (mousepos.x, mousepos.y);
                              SendMessage (lbox, msg, wParam, lParam);
                              return TRUE;
                    }
                    return TRUE;
              case WM_VSCROLL :
                    lbox = Getlbox ();
                    if (MouseinWindow (lbox, &mousepos))
                    {
                              SendMessage (lbox, msg, wParam, lParam);
                    }
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

int ShowPtBox (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
               int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 45;
        cy = 185;

        set_fkt (endlist, 5);
		SaveDblClck ();
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
//                                    cx + 24, 70,
                                    cx , 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
		RestoreDblClck ();
        return ptidx;
}

/**
Lieferschein auf komplett setzen.
**/

static int lspos = 0;
static int lsend = 4;

static int ldruck   = 0;
static int ldrfr    = 0;
static int lfrei    = 0;
static int lbar     = 0;
static int lsofort  = 0;
static int drwahl   = 0;
static int lsdel    = 0;

static int *lbuttons1 [] = {&ldruck, &ldrfr, &lfrei, &lbar, &lsofort,
                          &drwahl, NULL};
static int *lbuttons2 [] = {&ldruck, &lsdel,
                          &drwahl, NULL};
static int *lbuttons3 [] = {&ldruck, &drwahl,
                          &drwahl, NULL};
static int **lbuttons;

static int Setldruck0 (void);
static int Setldrfr0 (void);
static int Setlfrei0 (void);
static int Setlbar0 (void);
static int Setlsofort0 (void);
static int SetUeandDrk0 (void);

static int (*SetDefTab[]) () = {Setldruck0, Setldrfr0, Setlfrei0, Setlbar0,Setlsofort0};

static int Setldruck (void);
static int Setldrfr (void);
static int Setlfrei (void);
static int Setlbar (void);
static int Setlsofort (void);
static int Setdrwahl (void);
static int Setldel (void);
static int BreakBox (void);
static int BoxBreak (void);

static int IsDrChoise;


static field _cbox1 [] = {
" Ret.Drucken     ",     22, 2, 0, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setldruck, 101,
" Freig.+ Druck   ",     19, 2, 2, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setldrfr, 102,
" Freigeben       ",     19, 2, 4, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setlfrei, 103,
/*
" Barverkauf      ",     19, 2, 6, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setlbar, 104,
" Sofortrechnung  ",     19, 2, 8, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setlsofort, 105,
*/
" Druckerwahl     ",     19, 2, 6, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setdrwahl, 106,
"   OK    ",             10, 1, 9, 2,  0, "", BUTTON, 0, BreakBox, 107,
" Abbruch ",             10, 1, 9, 15, 0, "", BUTTON, 0, BoxBreak, KEY5};

form cbox1 = {6, 0, 0, _cbox1, 0,Setldruck0,0,0,&ListFont};

static field _cbox2 [] = {
" LS-Drucken      ",     22, 2, 0, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setldruck, 101,
" L�schen         ",     19, 2, 2, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setldel, 108,
" Druckerwahl     ",     19, 2, 4, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setdrwahl, 106,
"   OK    ",             10, 1, 7, 2,  0, "", BUTTON, 0, BreakBox, 107,
" Abbruch ",             10, 1, 7, 15, 0, "", BUTTON, 0, BoxBreak, KEY5};

form cbox2 = {5, 0, 0, _cbox2, 0,Setldruck0,0,0,&ListFont};

static field _cbox3 [] = {

" LS-Drucken      ",     22, 2, 0, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setldruck, 101,
" Druckerwahl     ",     19, 2, 2, 6, 0, "", BUTTON | CHECKBUTTON, 0,
                                             Setdrwahl, 106,
"   OK    ",             10, 1, 5, 2,  0, "", BUTTON, 0, BreakBox, 107,
" Abbruch ",             10, 1, 5, 15, 0, "", BUTTON, 0, BoxBreak, KEY5};

form cbox3 = {4, 0, 0, _cbox3, 0,Setldruck0,0,0,&ListFont};

form cbox;

int KomplettEnd (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEY5)
           {
                    break_enter ();
           }
           return 1;
}

void UnsetBox (int pos)
/**
Buttona auf selectierte setzen. (ausser pos);
**/
{
        int i;

        for (i = 0; lbuttons [i]; i ++)
        {
                  if (i == pos) continue;

                  SendMessage (cbox.mask[i].feldid, BM_SETCHECK, 0, 0l);
                  *lbuttons [i] = 0;
        }
}


int Setldruck (void)
/**
Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[0].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       ldruck = 0;
          }
          else
          {
                       ldruck = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       !ldruck, 0l);
                       ldruck = !ldruck;
          }
          UnsetBox (0);
          return 1;
}


int Setldruck0 (void)
/**
Druck setzen.
**/
{
          ldruck = 1;
          SendMessage (cbox.mask[0].feldid, BM_SETCHECK,
                       ldruck, 0l);
          SetFocus (cbox.mask[0].feldid);
          SetCurrentField (0);
          UnsetBox (0);
          return 1;
}


int Setldel (void)
/**
Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lsdel = 0;
          }
          else
          {
                       lsdel = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       !lsdel, 0l);
                       lsdel = !lsdel;
          }
          UnsetBox (1);
          return 1;
}

int Setldrfr (void)
/**
Freigabe und Druck setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[1].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                        ldrfr = 0;
          }
          else
          {
                        ldrfr = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       !ldrfr, 0l);
                       ldrfr = !ldrfr;
          }
          UnsetBox (1);
          return 1;
}

int Setldrfr0 (void)
/**
Freigabe und Druck setzen.
**/
{
          ldrfr = 1;
          SendMessage (cbox.mask[1].feldid, BM_SETCHECK,
                       ldrfr, 0l);
          SetFocus (cbox.mask[1].feldid);
          SetCurrentField (1);
          UnsetBox (1);
          return 1;
}

int Setlfrei (void)
/**
Freigabe setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[2].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lfrei = 0;
          }
          else
          {
                       lfrei = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[2].feldid, BM_SETCHECK,
                       !lfrei, 0l);
                       lfrei = !lfrei;
          }
          UnsetBox (2);
          return 1;
}

int Setlfrei0 (void)
/**
Freigabe setzen.
**/
{
          lfrei = 1;
          SendMessage (cbox.mask[2].feldid, BM_SETCHECK,
                       lfrei, 0l);
          SetFocus (cbox.mask[2].feldid);
          SetCurrentField (2);
          UnsetBox (2);
          return 1;
}

int Setlbar (void)

/**
Barverkauf setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[3].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lbar = 0;
          }
          else
          {
                       lbar = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[3].feldid, BM_SETCHECK,
                       !lbar, 0l);
                       lbar = !lbar;
          }
          UnsetBox (3);
          return 1;
}

int Setlbar0 (void)
/**
Barverkauf setzen.
**/
{
          lbar = 1;
          SendMessage (cbox.mask[3].feldid, BM_SETCHECK,
                       lbar, 0l);
          SetFocus (cbox.mask[3].feldid);
          SetCurrentField (3);
          UnsetBox (3);
          return 1;
}

int Setlsofort (void)

/**
Sofortrechnung setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          if (SendMessage (cbox.mask[4].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       lsofort = 0;
          }
          else
          {
                       lsofort = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[4].feldid, BM_SETCHECK,
                       !lsofort, 0l);
                       lsofort = !lsofort;
          }
          UnsetBox (4);
          return 1;
}

int Setlsofort0 (void)
/**
Sofortrechnung setzen.
**/
{
          lsofort = 1;
          SendMessage (cbox.mask[4].feldid, BM_SETCHECK,
                       lsofort, 0l);
          SetFocus (cbox.mask[4].feldid);
          SetCurrentField (4);
          UnsetBox (4);
          return 1;
}


int Setdrwahl (void)

/**
Sofortrechnung setzen.
**/
{
          if (testkeys ()) return 0;

          if (syskey == KEYLEFT) return 0;
          if (syskey == KEYRIGHT) return 0;
          if (syskey == KEYDOWN) return 0;
          if (syskey == KEYUP) return 0;
          if (syskey == KEYTAB) return 0;

          DruckChoise ();
          set_fkt (KomplettEnd, 5);
          SetFocus (cbox.mask[5].feldid);
          return 1;
          /*
          if (SendMessage (cbox.mask[5].feldid, BM_GETCHECK, 0l, 0l) == FALSE)
          {
                       drwahl = 0;
          }
          else
          {
                       drwahl = 1;
          }
          if (syskey == KEYCR)
          {
                       SendMessage (cbox.mask[5].feldid, BM_SETCHECK,
                       !drwahl, 0l);
                       drwahl = !drwahl;
          }
          UnsetBox (5);
          return 1;
          */
}


int BreakBox (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEYUP || syskey == KEYDOWN) return 0;
           syskey = KEYCR;
           break_enter ();
           return 1;
}

int BoxBreak (void)
/**
EnterButton abbrechen.
**/
{
           if (syskey == KEYUP || syskey == KEYDOWN) return 0;
           syskey = KEY5;
           break_enter ();
           return 1;
}

int MouseInButton (POINT *mpos)
{
        int i;

        if (MouseinDlg (MainButton2.mask[2].feldid, mpos))
        {
                  return TRUE;
        }

        for (i = 5; i < 7; i ++)
        {
                if (MouseinDlg (MainButton2.mask[i].feldid, mpos))
                {
                                   return TRUE;
                }
        }
        return FALSE;
}

static void SelectAktCtr (void)
/**
Aktiven Button in EnterButton selectieren.
**/
{
        syskey = KEYCR;
        if (cbox.mask[currentfield].after)
        {
                       (*cbox.mask[currentfield].after) ();
        }
        syskey = KEYCR;
}


BOOL SetLskStatus (int ret_stat)
/**
ret_stat in lsk auf 1 setzen.
**/
{
        beginwork ();
        if (Lock_Retk () == FALSE)
        {
            commitwork ();
            return FALSE;
        }

        retk.ret_stat = ret_stat;
        Retk.dbupdate ();
        commitwork ();
        return TRUE;
}


BOOL SetLskStatusDirekt (int ret_stat)
/**
ret_stat in lsk auf 1 setzen.
**/
{
        beginwork ();
        if (Lock_Retk () == FALSE)
        {
            commitwork ();
            return FALSE;
        }

        beginwork ();
        retk.ret_stat = ret_stat;
        ls_class.update_lsk (lsk.mdn, lsk.fil, lsk.ls);
        commitwork ();
        return TRUE;
}

BOOL SetWiegKompl (BOOL kz)
/**
ret_stat in lsk auf 3 setzen.
**/
{
        beginwork ();
        if (Lock_Retk () == FALSE)
        {
            commitwork ();
            return FALSE;
        }

        beginwork ();
        lsk.wieg_kompl = kz;
        ls_class.update_lsk (lsk.mdn, lsk.fil, lsk.ls);
        commitwork ();
        return TRUE;
}

void BearbKomplett (void)
/**
Auswahl aus Komplett bearbeiten.
**/
{
         char progname [256];

         if (strcmp (sys_ben.pers_nam, " ") <= 0)
         {
             strcpy (sys_ben.pers_nam, "fit");
         }
         commitwork ();
         if (lsk.kun_fil == 0 && kun.kun_typ == BAR)
         {
                ldruck = 0;
                lbar = 1;
         }

         else if (lsk.kun == 0 && kun.sam_rech == SOFORT &&
                  kun.kun_typ != 4 && kun.kun_typ != 7 &&
                  kun.kun_typ != 10)
         {
                ldruck = 0;
                lsofort = 1;
         }

         if (ldruck)
         {
                sprintf (progname, "%s\\BIN\\rswrun beldr X "
                                     "%hd %hd %ld %s D",
                                     rosipath,
                                     retk.mdn, retk.fil, retk.ret,
                                     sys_ben.pers_nam);
                ProcExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
         }
         else if (ldrfr)
         {
                sprintf (progname, "%s\\BIN\\rswrun beldr X "
                                     "%hd %hd %ld %s X",
                                     rosipath,
                                     retk.mdn, retk.fil, retk.ret,
                                     sys_ben.pers_nam);
                ProcExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);


//  Hilfsmassnahme: beldr setzt ret_stat nicht auf 3. Wird wieder korrigiert, wenn beldr
//	richtig funktioniert.			
				retk.ret_stat = 3;
				retk.ret_grund = 1;
         }
         else if (lfrei)
         {
                if (retk.ret_stat < 2)
                {
                             disp_messG ("Die Retoure mu� zuerst gedruckt werden", 2);
                             return;
                }
                sprintf (progname, "%s\\BIN\\rswrun beldr X "
                                     "%hd %hd %ld %s F",
                                     rosipath,
                                     retk.mdn, retk.fil, retk.ls,
                                     sys_ben.pers_nam);
                ProcExec (progname, SW_SHOWMINIMIZED, 0, 40, -1, 0);
//  Hilfsmassnahme: beldr setzt ret_stat nicht auf 3. Wird wieder korrigiert, wenn beldr
//	richtig funktioniert.			
				retk.ret_stat = 3;
				retk.ret_grund = 1;
         }
         else if (lsdel)
         {
                 beginwork ();
 			     DeleteAuftrag ();
				 commitwork ();
         }
         beginwork ();
}

void DeleteAuftrag (void)
/**
Vorratsauftrag beim Komplettsetzen loeschen.
**/
{
	     int dsqlstatus;

	     DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
	     DbClass.sqlin ((short *) &lsk.fil, 1, 0);
	     DbClass.sqlin ((short *) &lsk.ls, 2, 0);
		 DbClass.sqlcomm ("delete from lsk where mdn = ? and fil = ? and ls = ?");

	     DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
	     DbClass.sqlin ((short *) &lsk.fil, 1, 0);
	     DbClass.sqlin ((short *) &lsk.ls, 2, 0);
		 DbClass.sqlcomm ("delete from lsp where mdn = ? and fil = ? and ls = ?");
         dsqlstatus = nveinid (lsk.mdn, lsk.fil, "lsv", lsk.ls);
		 break_list ();
		 break_lief_enter = 1;
		 break_enter ();
}

// Auswahl nur Wiegen / Auszecihnen komplett, oder ganzer Lieferschein komplett


mfont KtFont = {"Courier New", 150, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static char ktext [61];
static char ktextarr [10][61];

static int ktidx;

static field _ktform [] =
{
        ktext,     50, 1, 0, 1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form ktform = {1, 0, 0, _ktform, 0, 0, 0, 0, &KtFont};

static field _ktub [] =
{
        "Auswahl",     50, 1, 0, 1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

static form ktub = {1, 0, 0, _ktub, 0, 0, 0, 0, &KtFont};


BOOL WiegKompl ()
{
        static BOOL CpOK = FALSE;

        if (! CpOK)
        {
            strcpy (ktextarr[0],
                "Wiegen / Auszeichnen komplett");
            strcpy (ktextarr[1],
                "Lieferschein komplett");
        }
        SetHoLines (0);
        syskey = 0;
        if (eWindow)
        {
                EnableWindow (eWindow, FALSE);
                EnableWindow (eFussWindow, FALSE);
                EnableWindow (eKopfWindow, FALSE);
        }
		ColorProc = ListRowColor;
        ktidx = ShowWKopf (eWindow, (char *) ktextarr, 2,
                     (char *) &ktext, 61,
                     &ktform, NULL, &ktub);
        if (eWindow)
        {
                EnableWindow (eWindow, TRUE);
                EnableWindow (eFussWindow, TRUE);
                EnableWindow (eKopfWindow, TRUE);
        }

        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return FALSE;
        }
        switch (ktidx)
        {
                case 0 :
                    SetWiegKompl (TRUE);
                    return FALSE;
                case 1 :
                    return TRUE;
        }
        return FALSE;
}



int SetKomplett ()
/**
Komplett setzen.
**/
{
         int x,y;
         int cx, cy;
		 int cy0;
         int mx;
         RECT rect;
         TEXTMETRIC tm;
         HDC hdc;
         HFONT hFont;
		 int def_nr;
//		 int i;

		 def_nr = komplett_default;
         def_nr = min (max (0, def_nr), 4);
 	     cbox1.before = SetDefTab[def_nr];

         if (retk.ret_stat > 2)
         {
                             disp_messG ("Die Retoure wurde schon freigegeben", 2);
                             return (0);
         }


         if (retk.ret_stat < 1)
		 {
			if (SetLskStatus (1) == FALSE)
			{
				 return 0;
			}
		 }

		 beginwork ();
		 Retk.dbupdate ();
		 commitwork ();

		 memcpy (&cbox, &cbox1, sizeof (form));
		 lbuttons = lbuttons1;

         y = 1;
         cy = 9;

         set_fkt (KomplettEnd, 5);
         spezfont (&ListFont);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tm);
         DelFont (hFont);
         ReleaseDC (hMainWindow, hdc);
         GetClientRect (eWindow, &rect);
         mx = rect.right / tm.tmAveCharWidth;
         GetFrmSize (&cbox, &cx, &cy0);

         x = max (0, (mx - cx) / 2);


         KomplettAktiv = 1;

         ldruck = ldrfr = lfrei = lbar = lsofort = drwahl = 0;
         SetMouseLockProc (MouseInButton);
         SetBorder (WS_VISIBLE | WS_DLGFRAME | WS_POPUP);
         if (EnterButtonWindow (eWindow, &cbox, y, x, cy, cx) == -1)
         {
                        KomplettAktiv = 0;
                        set_fkt (endlist, 5);
                        CloseControls (&testub);
                        display_form (Getlbox (), &testub, 0, 0);
						syskey = 0;
                        return 0;
         }

	     syskey = 0;
         CloseControls (&testub);
         display_form (Getlbox (), &testub, 0, 0);
         KomplettAktiv = 0;
         SetMouseLockProc (NULL);
         BearbKomplett ();
//		 Retk.dbreadfirst ();
//  Hilfsmassnahme: beldr setzt ret_stat nicht auf 3. Wird wieder korrigiert, wenn beldr
//	richtig funktioniert.			
		 Retk.dbupdate ();
         set_fkt (endlist, 5);
		 break_list ();
         return 0;
}


/*   Gebinde eingeben          */

mfont GebUbFont = {"Courier New", 190, 0, 1, RGB (255, 255, 255),
                                             GebColor,
                                             0,
                                             NULL};
mfont GebFont   = {"Courier New", 190, 0, 1, RGB (255, 255, 255),
                                             GebColor,
                                             0,
                                             NULL};

static char gebinde [4] = {"N"};
static char palette [4] = {"N"};
static char geb_einh [4] = {"1"};
static char pal_einh [4] = {"1"};
static char geb_me [9];
static char pal_me [9];
static char gtara [9];
static char gtara_einh [3] = {"2"};

static int gebcx = 620;
static int gebcy = 400;

static int break_geb = 0;

static int buttony = gebcy - bsz0 - 20;
static int buttonpos = 5;

static int current_geb = 0;
static int gebend = 6;

static int gbanz = 0;

static int testgebinde ();
static int testpalette ();
static int testgeb_einh ();
static int testpal_einh ();

static int gebfunc1 (void);
static int gebfunc4 (void);

field _gebub[] = {
           "Etikett",  8, 0, 1,10, 0, "", DISPLAYONLY, 0, 0, 0,
           "Einheit",  8, 0, 1,20, 0, "", DISPLAYONLY, 0, 0, 0,
           "Menge  ",  0, 0, 1,30, 0, "", DISPLAYONLY, 0, 0, 0,
           "Gebinde",  10, 0, 3,  1, 0, "",      DISPLAYONLY, 0, 0, 0,
           "Palette",  10, 0, 5,  1, 0, "",      DISPLAYONLY, 0, 0, 0,
           gtara_einh,  3, 0, 7, 22, 0, "%2d",   READONLY, 0, 0, 0,
           "Einzel-Tara", 11, 0, 7,  1, 0, "",   DISPLAYONLY, 0, 0, 0,
};

form gebub = {7, 0, 0, _gebub, 0, 0, 0, 0, &GebUbFont};


field _gebform[] = {

         gebinde,     2, 0, 3, 13, 0, "",      NORMAL, 0, testgebinde, 0,
         geb_einh,    3, 0, 3, 22, 0, "%2d",   NORMAL, 0, testgeb_einh, 0,
         geb_me,      8, 0, 3, 30, 0, "%3.3f", NORMAL, 0, 0, 0,

         palette,     2, 0, 5, 13, 0, "",      NORMAL, 0, testpalette, 0,
         pal_einh,    3, 0, 5, 22, 0, "%2d",   NORMAL, 0, testpal_einh, 0,
         pal_me,      8, 0, 5, 30, 0, "%3.3f", NORMAL, 0, 0, 0,
         gtara,       8, 0, 7, 30, 0, "%3.3f", NORMAL, 0, 0, 0,
};

form gebform = {7, 0, 0, _gebform, 0, 0, 0, 0, &GebFont};

field _GebButton[] = {
(char *) &Back,              bss0, bsz0, buttonpos, 4+5*bss, 0, "", COLBUTTON, 0,
 *                                                              func5, 0,
(char *) &BuOK,              bss0, bsz0, buttonpos, 4+4*bss, 0, "", COLBUTTON, 0,
 *                                                              func6, 0,
(char *) &PfeilR,            bss0, bsz0, buttonpos, 4+3*bss, 0, "", COLBUTTON, 0,
                                                          gebfunc4, 0,
(char *) &PfeilU,            bss0, bsz0, buttonpos, 4+2*bss, 0, "", COLBUTTON, 0,
                                                          func3, 0,
(char *) &PfeilO,            bss0, bsz0, buttonpos, 4+1*bss, 0, "", COLBUTTON, 0,
                                                          func2, 0,
(char *) &PfeilL,            bss0, bsz0, buttonpos, 4,     0, "", COLBUTTON, 0,
                                                          gebfunc1, 0};

form GebButton = {6, 0, 0, _GebButton, 0, 0, 0, 0, &buttonfont};

int gebfunc1 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{

         SetFocus (gebform.mask[current_geb].feldid);
         SendMessage (gebform.mask[current_geb].feldid, WM_KEYDOWN,  (WPARAM) VK_LEFT, 0l);
         return 1;
}

int gebfunc4 (void)
/**
Button fuer Pfeil links bearbeiten.
**/
{

         SetFocus (gebform.mask[current_geb].feldid);
         SendMessage (gebform.mask[current_geb].feldid, WM_KEYDOWN,  (WPARAM) VK_RIGHT, 0l);
         return 1;
}

void PrintGLines ()
/**
Icons am Bildschirm anzeigen.
**/
{
//         RECT rect, frect;
         PAINTSTRUCT ps;
         HDC hdc;
         HPEN hPen, oldpen;
         int x, y;

         hdc = BeginPaint (GebWindow, &ps);

/*
         GetFrmRectEx (&gebform, &GebFont, &frect);

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         SelectObject (hdc, hPen);
         x = gebcx - 4;
         y = frect.top - 30;
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
         DeleteObject (hPen);
*/

         y = buttony - 10;
         x = gebcx - 4;

         hPen = CreatePen (PS_SOLID, 2, BLACKCOL);
         oldpen = SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y, NULL);
         LineTo (hdc, x, y);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);

         hPen = CreatePen (PS_SOLID, 2, GRAYCOL);
         oldpen = SelectObject (hdc, hPen);
         MoveToEx (hdc, 2, y - 1, NULL);
         LineTo (hdc, x, y - 2);
		 SelectObject (hdc, oldpen);
         DeleteObject (hPen);

         EndPaint (WiegWindow, &ps);
}

void SetGebFocus (void)
/**
Focus fuer Wiegmaske setzen.
**/
{

          SetFocus (gebform.mask[current_geb].feldid);
          SendMessage (gebform.mask[current_geb].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
}


LONG FAR PASCAL GebProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
                    if (hWnd == GebWindow)
                    {
                                 display_form (GebWindow, &gebub, 0, 0);
                                 display_form (GebWindow, &gebform, 0, 0);
                        //        PrintGLines ();
                                 SetGebFocus ();
                    }
                    else if (hWnd == GebWindButton)
                    {
                                 display_form (GebWindButton, &GebButton, 0, 0);
                    }
                    break;
              case WM_KEYDOWN :
                         switch (wParam)
                         {
                              case VK_F5 :
                                      func5 ();
                                      return 0;

                         }
                         break;
              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    break;
              case WM_DESTROY :
                    if (hWnd == hMainWindow)
                    {
                             ExitProcess (0);
                             return 0;
                    }
                    return 0;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

static int testgeb_einh (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
/*
	       if (ptab_class.lese_ptab ("me_einh_fill",
                   geb_einh)
			!= 0)
		{
					strcpy (geb_einh, "0");
		}
*/
            display_form (GebWindow, &gebform, 0, 0);
		return 0;
}

static int testpal_einh (void)
/**
Felde Kundenbestell-Einheit testen.
**/
{
/*
	       if (ptab_class.lese_ptab ("me_einh_fill",
                   pal_einh)
			!= 0)
		{
					strcpy (pal_einh, "0");
		}
*/
            display_form (GebWindow, &gebform, 0, 0);
		return 0;
}


int testgebinde ()
/**
Eingabe fuer Gebinde testen.
**/
{
         if (gebinde[0] == 'J' || gebinde[0] == 'j')
         {
                            gebinde[0] = 'J';
                            display_form (GebWindow, &gebform, 0, 0);
                            return 0;
         }

         if (atoi (gebinde))
         {
                            gebinde[0] = 'J';
                            display_form (GebWindow, &gebform, 0, 0);
                            return 0;
         }
         gebinde[0] = 'N';
         display_form (GebWindow, &gebform, 0, 0);
         return 0;
}

int testpalette ()
/**
Eingabe fuer Gebinde testen.
**/
{
         if (palette[0] == 'J' || palette[0] == 'j')
         {
                            palette[0] = 'J';
                            return 0;
         }

         if (atoi (palette))
         {
                            palette[0] = 'J';
                            return 0;
         }
         palette[0] = 'N';
         return 0;
}

struct PT ptabmh, ptabmharr [30];
static int gebanz = 0;

static field _fptabform [] =
{
        ptabmh.ptbez,     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        ptabmh.ptwer1,     9, 1, 0,13, 0, "%2d", DISPLAYONLY, 0, 0, 0
};

static form fptabform = {2, 0, 0, _fptabform, 0, 0, 0, 0, &PtFont};

static field _fptabub [] =
{
        "Bezeichnung",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Wert",             9, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0
};

static form fptabub = {2, 0, 0, _fptabub, 0, 0, 0, 0, &PtFont};

static field _fptabvl [] =
{      "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
};

static form fptabvl = {1, 0, 0, _fptabvl, 0, 0, 0, 0, &PtFont};



int ShowGebMeEinh (char *me_einh)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (gebanz == 0)
        {
               dsqlstatus = ptab_class.lese_ptab_all ("me_einh_fill");
               while (dsqlstatus == 0)
               {
                   strcpy (ptabmharr[gebanz].ptbez,  ptabn.ptbezk);
                   strcpy (ptabmharr[gebanz].ptwer1, ptabn.ptwert);
                   gebanz ++;
                   if (gebanz == 30) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
               }
        }
        syskey = 0;
        ptidx = ShowPtBox (GebWindow, (char *) ptabmharr, gebanz,
                     (char *) &ptabmh,(int) sizeof (struct PT),
                     &fptabform, &fptabvl, &fptabub);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                     strcpy (me_einh, ptabmharr [ptidx].ptwer1);
        }
        return ptidx;
}

struct PT ptabrtkz, ptabrtkzharr [30];
static int rtkzanz = 0;

int ShowRetpKz (char *retp_kz)
/**
Ausahlfenster fuer Festtara anzeigen.
**/
{
        if (rtkzanz == 0)
        {
			dsqlstatus = ptab_class.lese_ptab_all ("retp_kz");
			while (dsqlstatus == 0)
			{
                   strcpy (ptabrtkzharr[rtkzanz].ptbez,  ptabn.ptbezk);
                   strcpy (ptabrtkzharr[rtkzanz].ptwer1, ptabn.ptwert);
                   rtkzanz ++;
                   if (rtkzanz == 30) break;
                   dsqlstatus = ptab_class.lese_ptab_all ();
			}
        }
	    syskey = 0;
        ptidx = ShowPtBox (WiegWindow, (char *) ptabrtkzharr, rtkzanz,
                     (char *) &ptabmh,(int) sizeof (struct PT),
                     &fptabform, &fptabvl, &fptabub);
        if (syskey != KEY5 && syskey != KEYESC)
        {
                     sprintf (retp_kz, "%s %s", ptabrtkzharr [ptidx].ptwer1, ptabrtkzharr [ptidx].ptbez);
        }
        return ptidx;
}

int testgebfields (void)
/**
Eingabefelder testen.
**/
{
        if (current_geb == 0)
        {
                   testgebinde ();
        }
        else if (current_geb == 3)
        {
                   testpalette ();
        }
        else if (current_geb == 1)
        {
                   testgeb_einh ();
        }
        else if (current_geb == 4)
        {
                   testpal_einh ();
        }
        return (0);
}

void gebaktion (void)
/**
Aktion auf aktivem Feld.
**/
{
        switch (current_geb)
        {
                  case 0 :
                          if (gebinde[0] == 'J')
                          {
                                       gebinde[0] = 'N';
                          }
                          else
                          {
                                       gebinde[0] = 'J';
                          }
                          testgebinde ();
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 1 :
                          ShowGebMeEinh (geb_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 2 :
                          EnterCalcBox (GebWindow,"  Gebinde-Menge  ",
                                       geb_me, gebform.mask[2].length,
                                       gebform.mask[2].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;

                  case 3 :
                          if (palette[0] == 'J')
                          {
                                       palette[0] = 'N';
                          }
                          else
                          {
                                       palette[0] = 'J';
                          }
                          testpalette ();
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 4 :
                          ShowGebMeEinh (pal_einh);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 5 :
                          EnterCalcBox (GebWindow,"  Paletten-Menge  ",
                                       pal_me, gebform.mask[5].length,
                                       gebform.mask[5].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
                  case 6 :
                          EnterCalcBox (GebWindow,"       Tara       ",
                                       gtara, gebform.mask[6].length,
                                       gebform.mask[6].picture);
                          display_form (GebWindow, &gebform, 0, 0);
                          break;
             }
}

int IsGebMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_ESCAPE :
                            case VK_F5 :
                                    break_geb = 1;
                                    return TRUE;
                            case VK_RETURN :
                                    gebaktion ();
                                    return TRUE;
                            case VK_DOWN :
                                    testgebfields ();
                                    current_geb ++;
                                    if (current_geb > gebend)
                                                current_geb = 0;
                                    SetGebFocus ();
                                    return TRUE;
                            case VK_UP :
                                   testgebfields ();
                                   current_geb --;
                                   if (current_geb < 0)
                                                current_geb = gebend;
                                    SetGebFocus ();
                                    return TRUE;
                            case VK_TAB :
                                    testgebfields ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                             current_geb --;
                                             if (current_geb < 0)
                                                  current_geb = gebend;
                                     }
                                     else
                                     {
                                             current_geb ++;
                                             if (current_geb > gebend)
                                                  current_geb = 0;
                                     }
                                     SetGebFocus ();
                                     return TRUE;

                             case VK_F9 :
                                     syskey = KEY9;
                                     if (current_geb == 1)
                                     {
                                               ShowGebMeEinh (geb_einh);
                                               display_form (GebWindow, &gebform, 0, 0);
                                     }
                                     else if (current_geb == 4)
                                     {
                                               ShowGebMeEinh (pal_einh);
                                               display_form (GebWindow, &gebform, 0, 0);
                                     }
                                     return TRUE;

                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (gebform.mask[0].feldid, &mousepos))
                    {
                          current_geb = 0;
/*
                          EnterNumBox (GebWindow,"  Gebinde-Etikett  ",
                                       gebinde, gebform.mask[0].length,
                                       "%1d");
*/
                          if (gebinde[0] == 'J')
                          {
                                       gebinde[0] = 'N';
                          }
                          else
                          {
                                       gebinde[0] = 'J';
                          }
                          testgebinde ();
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[1].feldid, &mousepos))
                    {
/*
                          EnterNumBox (GebWindow,"  Gebinde-Einheit  ",
                                       geb_einh, gebform.mask[1].length,
                                       gebform.mask[1].picture);
*/
                          current_geb = 1;
                          ShowGebMeEinh (geb_einh);
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[2].feldid, &mousepos))
                    {
                          current_geb = 2;
                          EnterCalcBox (GebWindow,"  Gebinde-Menge  ",
                                       geb_me, gebform.mask[2].length,
                                       gebform.mask[2].picture);
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }

                    else if (MouseinWindow (gebform.mask[3].feldid, &mousepos))
                    {
                          current_geb = 3;
/*
                          EnterNumBox (GebWindow,"  Paletten-Etikett  ",
                                       palette, gebform.mask[3].length,
                                       "%1d");
*/
                          if (palette[0] == 'J')
                          {
                                       palette[0] = 'N';
                          }
                          else
                          {
                                       palette[0] = 'J';
                          }
                          testpalette ();
                          display_form (GebWindow, &gebform, 0, 0);
//                          PostMessage (GebWindow, WM_KEYDOWN, VK_RETURN, 0l);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[4].feldid, &mousepos))
                    {
/*
                          EnterNumBox (GebWindow,"  Paletten-Einheit  ",
                                       pal_einh, gebform.mask[4].length,
                                       gebform.mask[4].picture);
*/
                          current_geb = 4;
                          ShowGebMeEinh (pal_einh);
                          EnableWindow (WiegWindow, FALSE);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[5].feldid, &mousepos))
                    {
                          current_geb = 5;
                          EnterCalcBox (GebWindow,"  Paletten-Menge  ",
                                       pal_me, gebform.mask[5].length,
                                       gebform.mask[5].picture);
                          EnableWindow (WiegWindow, FALSE);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
                    else if (MouseinWindow (gebform.mask[6].feldid, &mousepos))
                    {
                          current_geb = 6;
                          EnterCalcBox (GebWindow, "       Tara       ",
                                       gtara, gebform.mask[6].length,
                                       gebform.mask[6].picture);
                          EnableWindow (WiegWindow, FALSE);
                          display_form (GebWindow, &gebform, 0, 0);
                          return TRUE;
                    }
              }
              case WM_USER :
                    if (LOWORD (msg->wParam) == BUTARA)
                    {
                          dotara ();
                          return TRUE;
                    }
         }
         return FALSE;
}


int dogebinde ()
/**
Parameter fuer Gebinde - und Palettenetikett eingeben.
**/
{
        MSG msg;
        int  x, y;
        int cx, cy;

        cx = gebcx;
        x = (screenwidth - cx) / 2;
        cy = gebcy;
        y = (screenheight - cy) / 2;

        GebWindow  = CreateWindowEx (
                              0,
                              "GebWind",
                              "",
                              WS_POPUP | WS_DLGFRAME | WS_VISIBLE,
                              x, y,
                              cx, cy,
                              WiegWindow,
                              NULL,
                              hMainInst,
                              NULL);
        if (GebWindow == NULL) return (0);

        x = 5;
        cx -= 15;
        y = buttony;
        cy = bsz0 + 10;

        GebWindButton  = CreateWindowEx (
                              WS_EX_CLIENTEDGE,
                              "GebWindB",
                              "",
                              WS_CHILD | WS_VISIBLE,
                              x, y,
                              cx, cy,
                              GebWindow,
                              NULL,
                              hMainInst,
                              NULL);

        EnableWindow (WiegWindow, FALSE);
        create_enter_form (GebWindow, &gebform, 0, 0);
        current_geb = 0;
        SetGebFocus ();
        break_geb = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsGebMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_geb) break;
        }
        CloseControls (&gebub);
        CloseControls (&gebform);
        CloseControls (&GebButton);
        DestroyWindow (GebWindButton);
        DestroyWindow (GebWindow);
        GebWindow = NULL;
        SetActiveWindow (WiegWindow);
        EnableWindow (WiegWindow, TRUE);
        update_kun_geb ();
        return (0);
}

void ToVorgabe (char *veinh, short einh)
/**
Vorgabe-Einheit als Klartext.
**/
{
        switch (einh)
        {
           case 1 :
                strcpy (veinh, "St�ck");
                break;
           case 2:
                strcpy (veinh, "kg");
                break;
           case 4:
                strcpy (veinh, "Preis");
                break;
           case 5:
                strcpy (veinh, "Karton");
                break;
           case 6:
                strcpy (veinh, "Palette");
                break;
           default:
                strcpy (veinh, "kg");
                break;
        }
}


void gebindelsp (void)
/**
Gebindevorgaben in lsp testen.
Wenn brauchbare Gebindewerte in der lsp vorhanden sind,
Gebindewerte aus a_kun_gx ueberschreiben.
**/
{
        short   me_einh_kun1;
        double  auf_me1;
        double  inh1;
        short   me_einh_kun2;
        double  auf_me2;
        double  inh2;
        short   me_einh_kun3;
        double  auf_me3;
        double  inh3;

        me_einh_kun1 = atoi (aufps.me_einh_kun1);
        auf_me1      = ratod (aufps.auf_me1);
        inh1         = ratod (aufps.inh1);

        me_einh_kun2 = atoi (aufps.me_einh_kun2);
        auf_me2      = ratod (aufps.auf_me2);
        inh2         = ratod (aufps.inh2);

        me_einh_kun3 = atoi (aufps.me_einh_kun3);
        auf_me3      = ratod (aufps.auf_me3);
        inh3         = ratod (aufps.inh3);

        if (me_einh_kun1 <= 0 &&
            me_einh_kun2 <= 0 &&
            me_einh_kun3 <= 0)
        {
            return;
        }

        if (me_einh_kun1 == 2)
        {
              a_kun_geb.geb_fill = 2;
              sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
              ToVorgabe (veinh1, a_kun_geb.geb_fill);
        }
        else
        {
              a_kun_geb.geb_fill = 1;
              sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
              ToVorgabe (veinh1, a_kun_geb.geb_fill);
        }

        if (me_einh_kun2 == 5)
        {
              a_kun_geb.pal_fill = 5;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }
        else if (me_einh_kun2 == 2)
        {
              a_kun_geb.pal_fill = 2;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }
        else
        {
              a_kun_geb.pal_fill = 1;
              sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
              ToVorgabe (veinh2, a_kun_geb.pal_fill);
        }

        if (auf_me2 > (double) 0.0 && me_einh_kun2)
        {
              if (inh3)
              {
                  auf_me2 = inh3;
              }
              sprintf (pal_me,   "%.0lf",  auf_me2);
              strcpy (vorgabe2, pal_me);
        }
		else if (inh3)
		{
              sprintf (pal_me,   "%.3lf",  inh3);
              strcpy (vorgabe2, pal_me);
		}

        if (auf_me1 > (double) 0.0 && me_einh_kun1)
        {
              if (inh2)
              {
                  auf_me1 = inh2;
              }
              sprintf (geb_me,   "%.3lf", auf_me1);
              strcpy (vorgabe1, geb_me);
        }
		else if (inh2)
		{
              sprintf (geb_me,   "%.3lf",  inh2);
              strcpy (vorgabe1, geb_me);
		}
}


void fetch_std_geb (void)
/**
Standard-Gebindeeinstellungen aus a_kun lesen und in kun_geb uebertragen.
Der Eintrag wird nur temporaer benutzt        .
**/
{
        short sqm;
        extern short sql_mode;


        strcpy (a_kun_geb.geb_eti, "N");
        strcpy (a_kun_geb.pal_eti, "N");
        a_kun_geb.geb_fill = 0;
        a_kun_geb.pal_fill = 0;
        a_kun_geb.geb_anz = 0;
        a_kun_geb.pal_anz = 0;

        a_kun_geb.mdn = akt_mdn;
        a_kun_geb.fil = akt_fil;
        a_kun_geb.kun = lsk.kun;
        a_kun_geb.a   = ratod (aufps.a);
        a_kun_geb.tara   = 0;
        a_kun_geb.mhd   = 0;
        strcpy (gebinde, a_kun_geb.geb_eti);

		a_kun_geb.sys = auszeichner;
		geb_class.read_a_kun_first ();

        sprintf (geb_einh, "%hd", a_kun_geb.geb_fill);
        ToVorgabe (veinh1, a_kun_geb.geb_fill);

        sprintf (geb_me,   "%.3lf",
            (double) ((double) a_kun_geb.geb_anz / 1000));
        strcpy (vorgabe1, geb_me);

        strcpy (palette, a_kun_geb.pal_eti);

        sprintf (pal_einh, "%hd", a_kun_geb.pal_fill);
        ToVorgabe (veinh2, a_kun_geb.pal_fill);

        sprintf (pal_me,   "%.3lf",
            (double) ((double) a_kun_geb.pal_anz / 1000));
        strcpy (vorgabe2, pal_me);

        gebindelsp ();
        a_kun_geb.geb_anz = (long) (double) (ratod (geb_me) * 1000);
        a_kun_geb.pal_anz = (long) (double) (ratod (pal_me) * 1000);

        sprintf (gtara,     "%.3lf", a_kun_geb.tara);

        if (tara_kz_ausz == 1)
        {
                  sprintf (gtara,     "%.3lf", a_hndw.tara);
                  a_kun_geb.tara =  a_hndw.tara;
        }
        if (mhd_kz == 1)
        {
              if (_a_bas.hbk_kz[0] == 'T' ||
                  _a_bas.hbk_kz[0] <= ' ')
              {
                  a_kun_geb.mhd =  _a_bas.hbk_ztr;
              }
        }
        sprintf (aufps.tara,     "%.3lf", a_kun_geb.tara);
        sqm = sql_mode;
        sql_mode = 1;
		a_kun_geb.sys = auszeichner;
        geb_class.update_kun_geb ();
/*
		commitwork ();
        beginwork ();
*/
        sql_mode = sqm;
}

void updategeblsp (void)
/**
Gebinde in lsp aktialisieren.
**/
{
 	    sprintf (aufps.inh2, "%.3lf", (double) ((double)
				        a_kun_geb.geb_anz / 1000));
	    sprintf (aufps.inh3, "%.3lf", (double) ((double)
				        a_kun_geb.pal_anz / 1000));

		sprintf (aufps.me_einh_kun1, "%d", a_kun_geb.geb_fill);
		sprintf (aufps.me_einh_kun2, "%d", a_kun_geb.pal_fill);

        memcpy (&aufparr [aufpidx], &aufps, sizeof (struct AUFP_S));
}


void update_kun_geb (void)
/**
Tabelle kun_geb aktualisieren.
**/
{
        short sqm;
        extern short sql_mode;

        sqm = sql_mode;
        strcpy (a_kun_geb.geb_eti, gebinde);
        a_kun_geb.geb_fill = atoi (geb_einh);
        ToVorgabe (veinh1, a_kun_geb.geb_fill);
        strcpy (vorgabe1, geb_me);
        a_kun_geb.geb_anz =  (long) (double) (ratod (geb_me) * 1000);

        strcpy (a_kun_geb.pal_eti, palette);
        a_kun_geb.pal_fill = atoi (pal_einh);
        ToVorgabe (veinh2, a_kun_geb.pal_fill);
        strcpy (vorgabe2, pal_me);
        a_kun_geb.pal_anz =  (long) (double) ratod (pal_me) * 1000;
        a_kun_geb.tara    =  ratod (gtara);
        sprintf (aufps.tara,     "%.3lf", a_kun_geb.tara);
        sql_mode = 1;
		a_kun_geb.sys = auszeichner;
        geb_class.update_kun_geb ();
        sql_mode = sqm;
        updategeblsp ();
		commitwork ();
		beginwork ();
}


/**

Neuen Lieferschein eingeben.

**/

mfont RetKopfFontT = {"Courier New", 200, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont RetKopfFontE = {"Courier New", 200, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


mfont RetKopfFontTD = {"Courier New", 150, 0, 1, BLACKCOL,
                                                RGB (0, 255, 255),
                                                0,
                                                NULL};

mfont RetKopfFontED = {"Courier New", 150, 0, 1,
                                                RGB (0, 0, 0),
                                                RGB (255, 255, 255),
                                                0,
                                                NULL};


static char KundeFil0 [14] = {"Kunden-Nr   :"};
static char KundeFil1 [14] = {"Filial-Nr   :"};
static char kunfil[3];
static long gen_lief_nr;

static field _RetKopfT[] = {
"Retouren-Nr:",     16, 0, 1, 2, 0, "", DISPLAYONLY, 0, 0, 0};

static form RetKopfT = {1, 0, 0, _RetKopfT, 0, 0, 0, 0, &RetKopfFontT};


static field _RetKopfTD[] = {
"Kunde/Fil   :",     13, 0, 4, 2, 0, "", DISPLAYONLY, 0, 0, 0,
KundeFil0,           13, 0, 5, 2, 0, "", DISPLAYONLY, 0, 0, 0,
kun_krz1,            16, 0, 5,28, 0, "", DISPLAYONLY, 0, 0, 0,
"Retourendat.:",     13, 0, 6, 2, 0, "", DISPLAYONLY, 0, 0, 0,
"Status      :",     13, 0,10, 2, 0, "", DISPLAYONLY, 0, 0, 0,
ret_stat_txt,         16, 0,10,28, 0, "", DISPLAYONLY, 0, 0, 0
};

static form RetKopfTD = {6, 0, 0, _RetKopfTD, 0, 0, 0, 0, &RetKopfFontTD};

static int ret_stat_pos = 5;
static int SetKunFil (void);
static int ReadKun (void);
static int ReadLS (void);


static field _RetKopfE[] = {
  lief_nr,                9, 0, 1,18, 0, "%8d", EDIT,        0, ReadLS, 0};

form RetKopfE = {1, 0, 0, _RetKopfE, 0, 0, 0, 0, &RetKopfFontE};


static field _RetKopfED[] = {
  kunfil,               2, 0, 4,18, 0, "%1", EDIT,      0, SetKunFil, 0,
  kun_nr,               9, 0, 5,18, 0, "%8.0f", EDIT,     0, ReadKun, 0,
  lieferdatum,         11, 0, 6,18, 0, "dd.mm.yyyy",
                                              EDIT,     0, 0, 0,
  ret_stat,              2, 0,10,18, 0, "",
                                              READONLY, 0, 0, 0
};

form RetKopfED = {4, 0, 0, _RetKopfED, 0, 0, 0, 0, &RetKopfFontED};



void FillLSForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
             kun_class.lese_kun (akt_mdn, akt_fil, retk.kun);
             sprintf (kun_nr, "%ld", retk.kun);
             dlong_to_asc (retk.ret_dat, lieferdatum);
             sprintf (ret_stat, "%hd", retk.ret_stat);
             strcpy (kun_krz1, kun.kun_krz1);
             ptab_class.lese_ptab ("ret_stat", ret_stat);
             strcpy (ret_stat_txt, ptabn.ptbezk);
}

void FromLSForm (void)
/**
Datenbankfelder in Maske uebertragn.
**/
{
             short mdn;
             short fil;

             mdn = akt_mdn;
             fil = akt_fil;

             retk.mdn = mdn;
             retk.fil = fil;
             retk.ret  = atol (lief_nr);
			 retk.kun_fil = atoi (kunfil);
             retk.kun = atol (kun_nr);
             retk.ret_dat = dasc_to_long (lieferdatum);
             retk.lieferdat = dasc_to_long (lieferdatum);
             retk.ret_stat = atoi (ret_stat);
}

static int readstatus = 0;
static int freestat = 0;


void FreeLS (void)
/**
Retouren-Nummer freigeben.
**/
{
     short mdn;
     short fil;

     mdn = akt_mdn;
     fil = akt_fil;

     if (freestat == 1) return;

     beginwork ();
     dsqlstatus = nveinid (mdn, fil,
                          "ret",  gen_lief_nr);
     commitwork ();
     freestat = 1;
}


int WriteLS (void)
/**
Lieferscheinkopf schreiben.
**/
{
    char datum [12];
    long dat1, dat2;

    if (readstatus > 1) return 0;

    if (atol (kun_nr) == 0l)
	{
		 disp_messG ("Es wurde keine Kunden-Nummer erfasst", 2);
		 return 0;
	}

    sysdate (datum);
    dat1 = dasc_to_long (datum);
    dat2 = dasc_to_long (lieferdatum);

// Nicht mehr aktiv 14.04.2009
    if (dat1 > dat2)
    {
//        disp_messG ("Lieferdatum nicht korrekt", 2);
//        return (0);
    }

    FromLSForm ();
    if (Lock_Retk () == FALSE)
    {
        PostQuitMessage (0);
        break_lief_enter = 1;
        commitwork ();
        return 0;
    }
    Retk.dbupdate ();
    ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
    set_fkt (menfunc2, 6);
    if (readstatus == 1 || retk.ret == gen_lief_nr)
    {
        freestat = 1;
    }
    commitwork ();
    return 0;
}


int ReadLS (void)
/**
Lieferschein nach Auftragsnummer lesen.
**/
{
            readstatus = 0;
            if (atol (lief_nr) <= 0l) return (-1);

            freestat = 0;
			memcpy (&retk, &retk_null, sizeof (struct RETK));
            retk.mdn = akt_mdn;
			retk.fil = akt_fil;
			retk.ret = atol (lief_nr);
            dsqlstatus = Retk.dbreadfirst ();
            if (dsqlstatus == 100 && atol (lief_nr) != gen_lief_nr)
            {
                   ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
                   set_fkt (NULL, 6);
                   print_messG (0, "Lieferschein %s nicht gefunden", lief_nr);
                   readstatus = 2;
                   break_lief_enter = 1;
                   return -1;
            }
            else if (dsqlstatus < 0)
            {
                   ActivateColButton (BackWindow2, &MainButton, 4, -1, 1);
                   set_fkt (NULL, 6);
                   print_messG (0, "Datenbankfehler %d", dsqlstatus);
                   readstatus = 2;
                   return -1;
            }

            if (dsqlstatus == 0)
            {
                   if (retk.ret != gen_lief_nr)
                   {
                           commitwork ();
                           FreeLS ();
                 }
                   beginwork ();
                   if (Lock_Retk () == FALSE)
                   {
                          break_lief_enter = 1;
                          return -1;
                   }
                   ActivateColButton (BackWindow2, &MainButton, 4, 1, 1);
                   set_fkt (menfunc2, 6);
                   FillLSForm ();
            }
            else
            {
                   sprintf (ret_stat,"%hd", 0);
                   ptab_class.lese_ptab ("ret_stat", ret_stat);
                   readstatus = 1;
            }
            return 0;
}


int SetKunFil (void)
{
          if (atoi (kunfil) == 0)
          {
                      sprintf (kunfil, "0");
                      RetKopfTD.mask[1].feld = KundeFil0;
          }
          else
          {
                      sprintf (kunfil, "1");
                      RetKopfTD.mask[1].feld = KundeFil1;
          }

          display_form (AufKopfWindow, &RetKopfTD, 0, 0);
          display_form (AufKopfWindow, &RetKopfED, 0, 0);
          return (0);
}


void FillLskAdr (void)
/**
Adressfelder in Lsk fuellen.
**/
{
          retk.adr = kun.adr2;
/*
          strcpy (retk.adr_nam1, _adr.adr_nam1);
          strcpy (retk.adr_nam2,  _adr.adr_nam2);
          strcpy (retk.str,       _adr.str);
          strcpy (retk.plz,       _adr.plz);
          strcpy (retk.ort1,      _adr.ort1);
          strcpy (retk.pf,        _adr.pf);
*/
}

int MdnWaehrung (short mdn)
/**
Mandant lesen und Waehrung zurueckgeben.
**/
{
	       int dsqlstatus;
		   short waehrung;


           DbClass.sqlin ((short *) &mdn, 1, 0);
		   DbClass.sqlout ((short *) &waehrung, 1, 0);
		   waehrung = 0;
		   dsqlstatus = DbClass.sqlcomm ("select waehr_prim from mdn where mdn = ?");
		   return waehrung;
}


int ReadKun (void)
{
          short mdn;
          short fil;

          mdn = akt_mdn;
          fil = akt_fil;
          kun_krz1[0] = (char) 0;
          if (atoi (kunfil) == 0)
          {
                  dsqlstatus = kun_class.lese_kun (mdn, fil, atol (kun_nr));
                  if (dsqlstatus == 0)
                  {
                         strcpy (kun_krz1, kun.kun_krz1);
                         strcpy (retk.kun_krz1,kun.kun_krz1);
                         adr_class.lese_adr (kun.adr2);
						 strcpy (KundeFiliale, KundeFilialeK);
                         retk.waehrung = kun.waehrung;
                         FillLskAdr ();
                         retk.waehrung = kun.waehrung;
                  }
				  else
				  {
					     disp_messG ("Kunde nicht angelegt", 2);
					     sprintf (kun_nr, "%ld", 0l);
                         display_field (AufKopfWindow, &RetKopfTD.mask[1], 0, 0);
				  }
          }
          else
          {
                  dsqlstatus = fil_class.lese_fil (mdn, (short) atol (kun_nr));
                  if (dsqlstatus == 0)
                  {
                      adr_class.lese_adr (_fil.adr);
                      strcpy (kun_krz1, _adr.adr_krz);
                      strcpy (lsk.kun_krz1, _adr.adr_krz);
					  strcpy (KundeFiliale, KundeFilialeF);
					  kun.adr2 = _fil.adr;
                      FillLskAdr ();
                      retk.waehrung = MdnWaehrung (mdn);
                  }
				  else
				  {
					     disp_messG ("Filiale nicht angelegt", 2);
					     sprintf (kun_nr, "%ld", 0l);
                         display_field (AufKopfWindow, &RetKopfTD.mask[1], 0, 0);
				  }
          }
          if (dsqlstatus)
          {
                  printf (kun_nr, "%ld", 0l);
          }

          current_form = &RetKopfTD;
          display_field (AufKopfWindow, &RetKopfTD.mask[2], 0, 0);
//          display_form (AufKopfWindow, &RetKopfTD, 0, 0);
          return (0);
}


void DisplayRetKopf (void)
/**
Masken fuer RetKopf anzeigen.
**/
{
          display_form (AufKopfWindow, &RetKopfT, 0, 0);
          display_form (AufKopfWindow, &RetKopfTD, 0, 0);
          if (RetKopfE.mask[0].feldid)
          {
              display_form (AufKopfWindow, &RetKopfE, 0, 0);
              display_form (AufKopfWindow, &RetKopfED, 0, 0);
          }
}


void CloseRetKopf (void)
/**
Masken fuer RetKopf anzeigen.
**/
{
        CloseControls (&RetKopfT);
        CloseControls (&RetKopfE);
        CloseControls (&RetKopfTD);
        CloseControls (&RetKopfED);
}


void GetEditTextL (void)
/**
Text aus Editierfeld holen.
**/
{
        switch (lspos)
        {
                 case 0 :
                    GetWindowText (RetKopfE.mask [0].feldid,
                                   RetKopfE.mask [0].feld,
                                   RetKopfE.mask[0].length);
                    break;
                 case 1 :
                    GetWindowText (RetKopfED.mask [0].feldid,
                                   RetKopfED.mask [0].feld,
                                   RetKopfED.mask[0].length);
                    break;
                 case 2 :
                    GetWindowText (RetKopfED.mask [1].feldid,
                                   RetKopfED.mask [1].feld,
                                   RetKopfED.mask[1].length);
                    break;
                 case 3 :
                    GetWindowText (RetKopfED.mask [2].feldid,
                                   RetKopfED.mask [2].feld,
                                   RetKopfED.mask[2].length);
                    break;
                 case 4 :
                    GetWindowText (RetKopfED.mask [3].feldid,
                                   RetKopfED.mask [3].feld,
                                   RetKopfED.mask[3].length);

                    break;
        }
        if (lspos == 0 && RetKopfE.mask[0].after)
        {
                    (*RetKopfE.mask[0].after) ();
        }
        else if (lspos &&  RetKopfED.mask[lspos - 1].after)
        {
                    (*RetKopfED.mask[lspos - 1].after) ();
        }
}

void LiefAfter (int pos)
{
        lspos = pos;
        if (lspos == 0 && RetKopfE.mask[0].after)
        {
                    (*RetKopfE.mask[0].after) ();
        }
        else if (lspos &&  RetKopfED.mask[lspos - 1].after)
        {
                    (*RetKopfED.mask[lspos - 1].after) ();
        }
}

void SetLiefFocus (int pos)
/**
Focus auf Eingabefelder AuftragKopfsetzen.
**/
{
        switch (pos)
        {
               case 0 :
                      SetFocus (RetKopfE.mask[0].feldid);
                      SendMessage (RetKopfE.mask[0].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
               default :
                      SetFocus (RetKopfED.mask[pos - 1].feldid);
                      SendMessage (RetKopfED.mask[pos -1].feldid, EM_SETSEL,
                           (WPARAM) 0, MAKELONG (-1, 0));
                      break;
        }
}

void LiefFocus (void)
{
        SetLiefFocus (lspos);
}


int IsLiefMessage (MSG *msg)
/**
HotKey Testen.
**/
{
         POINT mousepos;

         switch (msg->message)
         {
              case WM_KEYDOWN :
              {
                     switch (msg->wParam)
                     {
                            case VK_F5 :
                                    break_lief_enter = 1;
									syskey = KEY5;
                                    return TRUE;
                            case VK_F6 :
                                    syskey = KEY6;
                                    testkeys ();
                                    return TRUE;
                            case VK_F12 :
                                    GetEditTextL ();
                                    syskey = KEY12;
                                    WriteLS ();
                                    return TRUE;
                            case VK_DOWN :
                            case VK_RETURN :
                                    GetEditTextL ();
                                    lspos ++;
                                    if (lspos > lsend - 1) lspos = 0;
                                    SetLiefFocus (lspos);
                                    return TRUE;
                            case VK_UP :
                                    GetEditTextL ();
                                    lspos --;
                                    if (lspos < 0) lspos = lsend - 1;
                                    SetLiefFocus (lspos);
                                    return TRUE;
                            case VK_TAB :
                                    GetEditTextL ();
                                    if (GetKeyState (VK_SHIFT) < 0)
                                    {
                                            lspos --;
                                            if (lspos < 0) lspos = 4;
                                            SetLiefFocus (lspos);
                                     }
                                     else
                                     {
                                            lspos ++;
                                            if (lspos > 4) lspos = 0;
                                            SetLiefFocus (lspos);
                                     }
                                      return TRUE;
                            default :
                                    return FALSE;
                     }
                     break;
              }
              case WM_LBUTTONDOWN :
              {
                    GetCursorPos (&mousepos);

                    if (MouseinWindow (RetKopfE.mask[0].feldid, &mousepos))
                    {
                          EnterNumBox (AufKopfWindow, "  Lieferschein-Nr  ",
                                       lief_nr, 9, RetKopfE.mask[0].picture);
                          display_field (AufKopfWindow, &RetKopfE.mask[0], 0, 0);
                          LiefAfter (0);
                          return TRUE;
                    }
                    else if (MouseinWindow (RetKopfED.mask[0].feldid, &mousepos))
                    {
                          EnterNumBox (AufKopfWindow,"  Kunden/Fil  ",
                                          kunfil, 2, RetKopfED.mask[0].picture);
                          display_field (AufKopfWindow, &RetKopfED.mask[0], 0, 0);
                          LiefAfter (1);
                          return TRUE;
                    }
                    else if (MouseinWindow (RetKopfED.mask[1].feldid, &mousepos))
                    {
						  if (atol (kunfil) == 0)
						  {
		                          SetChoise (dokunchoise, 0);
                                  EnterNumBox (AufKopfWindow,"  Kunden-Nr  ",
                                          kun_nr, 9, RetKopfED.mask[1].picture);
						  }
						  else
						  {
                                  EnterNumBox (AufKopfWindow,"  Filial-Nr  ",
                                          kun_nr, 9, RetKopfED.mask[1].picture);
						  }
                          display_field (AufKopfWindow, &RetKopfED.mask[1], 0, 0);
                          LiefAfter (2);
                          return TRUE;
                    }
                    else if (MouseinWindow (RetKopfED.mask[2].feldid, &mousepos))
                    {
                         EnterNumBox (AufKopfWindow,"  Lieferdatum  ",
                                      lieferdatum, 11,
                                            RetKopfED.mask[2].picture);
                         display_field (AufKopfWindow, &RetKopfE.mask[2], 0, 0);
                         LiefAfter (3);
                         return TRUE;
                    }
             }
          }

          return FALSE;
}

void KonvRetKopf (void)
/**
Spaltenposition von RetKopfED anpassen.
**/
{
        static int KonvOK = 0;
        int z, s;
        int i;

        if (KonvOK) return;

        KonvOK = 1;
        for (i = 0; i < RetKopfED.fieldanz; i ++)
        {
                    s = RetKopfED.mask[i].pos [1];
                    KonvTextPos (RetKopfE.font,
                                 RetKopfED.font,
                                 &z, &s);
                    RetKopfED.mask[i].pos [1] = s;
        }

        s = RetKopfTD.mask[1].pos [1];
        KonvTextPos (RetKopfE.font,
                    RetKopfED.font,
                    &z, &s);
        RetKopfTD.mask[1].pos [1] = s;

        s = RetKopfTD.mask[2].pos [1];
        KonvTextPos (RetKopfE.font,
                    RetKopfED.font,
                    &z, &s);
        RetKopfTD.mask[2].pos [1] = s;

        s = RetKopfTD.mask[5].pos [1];
        KonvTextPos (RetKopfE.font,
                     RetKopfED.font,
                    &z, &s);
        RetKopfTD.mask[5].pos [1] = s;
}

void LiefInit (field *feld)
/**
Feld Initialisieren.
**/
{
        memset (feld->feld, ' ', feld->length);
        feld->feld[feld->length - 1] = (char) 0;
}

void InitRetKopf (void)
/**
Auftrag-Kopf initialisieren.
**/
{
        int i;

        RetKopfE.mask[0].feld[0] = (char) 0;

        for (i = 0; i < RetKopfED.fieldanz; i ++)
        {
                      LiefInit (&RetKopfED.mask[i]);
        }
        LiefInit (&RetKopfTD.mask[2]);
        LiefInit (&RetKopfTD.mask[ret_stat_pos]);
}

void GenLS (void)
/**
Retourennummer generieren.
**/
{
        int dsqlstatus;
        short mdn;
        short fil;
		long start;
		long end;
		char buffer [256];

        mdn = akt_mdn;
        fil = akt_fil;

        beginwork ();
        dsqlstatus = AutoClass.nvholid (mdn, fil, "ret");
		if (dsqlstatus == -1)
		{
			    sprintf (buffer, "delete from auto_nr where mdn = %hd and fil = %hd "
					              "and nr_nam = \"ret\"", mdn, fil);
			    DbClass.sqlcomm (buffer);
				dsqlstatus = 100;
		}
        if (dsqlstatus == 100)
        {
              ptab_class.lese_ptab ("ret", "1");
			  start = atol (ptabn.ptwer1);
              ptab_class.lese_ptab ("ret", "2");
			  end = atol (ptabn.ptwer1);
              dsqlstatus = AutoClass.nvanmprf (mdn,
                                     fil,
                                     "ret",
                                     start,
                                     end,
                                     (long) 10,
                                      "");

              if (dsqlstatus == 0)
              {
                          dsqlstatus = AutoClass.nvholid (mdn,
                                                fil,
                                                "ret");
              }
         }
         commitwork ();
         sprintf (lief_nr, "%ld", auto_nr.nr_nr);
         gen_lief_nr = auto_nr.nr_nr;
         sysdate (lieferdatum);
}



void RetKopf (void)
/**
Retourenkopf bearbeiten.
**/
{
        RECT rect;
        MSG msg;
        int x, y, cx, cy;

        if (KopfEnterAktiv) return;
        if (LiefEnterAktiv) return;

		memcpy (&retk, &retk_null, sizeof (struct RETK));
        MainBuInactiv ();
        freestat = 0;
/*
        CloseControls (&AufKopfT);
        CloseControls (&AufKopfE);
        CloseControls (&AufKopfTD);
        CloseControls (&AufKopfED);
*/

        lspos = 0;
        RegisterKopfEnter ();
        GetWindowRect (LogoWindow, &rect);
        x = rect.left;
        y = rect.top;
        cx = rect.right - rect.left;
        cy = rect.bottom - rect.top;

		lsk.auf_art = auf_art;
        KonvRetKopf ();
        InitRetKopf ();
        GenLS ();

        beginwork ();
        LiefEnterAktiv = 1;
		strcpy (ret_stat,"0");
        if (AufKopfWindow == 0)
        {
                  AufKopfWindow  = CreateWindowEx (
                              0,
                              "AufKopf",
                              "",
                              WS_POPUP,
                              x, y,
                              cx, cy,
                              hMainWindow,
                              NULL,
                              hMainInst,
                              NULL);
        }

        if (AufKopfWindow == NULL)
        {
                 LiefEnterAktiv = 0;
                 return;
        }
        ShowWindow (AufKopfWindow, SW_SHOW);
        UpdateWindow (AufKopfWindow);
        create_enter_form (AufKopfWindow, &RetKopfE, 0, 0);
        create_enter_form (AufKopfWindow, &RetKopfED, 0, 0);
        SetLiefFocus (lspos);
        break_lief_enter = 0;
        while (GetMessage (&msg, NULL, 0, 0))
        {
              if (IsLiefMessage (&msg));
              else
              {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
              }
              if (break_lief_enter) break;
        }
        CloseControls (&RetKopfT);
        CloseControls (&RetKopfE);
        CloseControls (&RetKopfTD);
        CloseControls (&RetKopfED);

        DestroyWindow (AufKopfWindow);
        AufKopfWindow = NULL;
        LiefEnterAktiv = 0;
        MainBuActiv ();
        FreeLS ();
}


/* Auswahl ueber Geraete (sys_peri    */

#define MCI 6
#define GX  16

struct SYSPS
{
        char sys [9];
        char peri_typ [3];
        char txt [61];
        char peri_nam [21];
};


struct SYSPS sysarr [50], sysps;


static int sysidx;
static int sysanz = 0;

mfont SysFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _sysform [] =
{
        sysps.sys,      12, 1, 0, 1, 0, "%8d", DISPLAYONLY, 0, 0, 0,
        sysps.peri_typ,  4, 1, 0,13, 0, "",    DISPLAYONLY, 0, 0, 0,
        sysps.txt,      20, 1, 0,17, 0, "",    DISPLAYONLY, 0, 0, 0
};

static form sysform = {3, 0, 0, _sysform, 0, 0, 0, 0, &SysFont};

static field _sysub [] =
{
        "Ger�te-Nr",     12, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Typ",            4, 1, 0,13, 0, "", DISPLAYONLY, 0, 0, 0,
        "Ger�t",         20, 1, 0,17, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form sysub = {3, 0, 0, _sysub, 0, 0, 0, 0, &SysFont};

static field _sysvl [] =
{
    "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 16, 0, "", NORMAL, 0, 0, 0,
};

static form sysvl = {2, 0, 0, _sysvl, 0, 0, 0, 0, &SysFont};

int ShowSysPeri (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15;
        cy = 185;

        set_fkt (endlist, 5);
		SaveDblClck ();
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
		SetPosColorProc (ColorProc);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
		RestoreDblClck ();
        return ptidx;
}

int AuswahlSysPeri (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        if (sysanz == 0)
        {
               dsqlstatus = sys_peri_class.lese_sys_peri ("order by sys");
               while (dsqlstatus == 0)
               {
                   if (sys_peri.peri_typ == MCI ||
                       sys_peri.peri_typ == GX)
                   {
                       sprintf (sysarr[sysanz].sys, "%ld",
                                     sys_peri.sys);
                       sprintf (sysarr[sysanz].peri_typ,"%hd",
                                     sys_peri.peri_typ);
                       strcpy (sysarr[sysanz].txt, sys_peri.txt);
                       strcpy (sysarr[sysanz].peri_nam,
                                      sys_peri.peri_nam);
                       sysanz ++;
                       if (sysanz == 50) break;
                   }
                   dsqlstatus = sys_peri_class.lese_sys_peri ();
               }
        }
        if (sysanz == 0)
        {
               disp_messG ("Keine Ger�te gefunden", 1);
               return 0;
        }
        syskey = 0;
        sysidx = ShowSysPeri (LogoWindow, (char *) sysarr, sysanz,
                     (char *) &sysps,(int) sizeof (struct SYSPS),
                     &sysform, &sysvl, &sysub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        if (atoi (sysarr[sysidx].peri_typ) == 6)
        {
                strcpy (waadir, sysarr[sysidx].peri_nam);
                clipped (waadir);
                sprintf (ergebnis, "%s\\ergebnis", waadir);
                WaaInit = 0;
                WaageInit ();
				StdWaage = atol (sysarr[sysidx].sys);
                sys_peri_class.lese_sys (atol (sysarr[sysidx].sys));
                la_zahl = sys_peri.la_zahl;
                AktKopf = 0;
        }
        else
        {
              auszeichner = atol (sysarr[sysidx].sys);
              sys_peri_class.lese_sys (atol (sysarr[sysidx].sys));
//            F�r Preisauszeichner noch nicht aktiv.
        }

        return sysidx;
}


/* Auswahl ueber Wiegungen                    */


struct WIEGT
{
        char wieg [5];
        char menge [12];
};


struct WIEGT wiegarr [1000], wiegt;


static int wiegidx;
static int wieganz = 0;

mfont WiegFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wiegform [] =
{
        wiegt.wieg,      9, 1, 0, 1, 0, "%7d",   DISPLAYONLY, 0, 0, 0,
        wiegt.menge,    13, 1, 0,10, 0, ":9.3f", DISPLAYONLY, 0, 0, 0,
};

static form wiegform = {2, 0, 0, _wiegform, 0, 0, 0, 0, &WiegFont};

static field _wiegub [] =
{
        "Wiegung",        9, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "Menge",         13, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wiegub = {2, 0, 0, _wiegub, 0, 0, 0, 0, &WiegFont};

static field _wiegvl [] =
{
    "1",                  1,  0, 0,  9, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 22, 0, "", NORMAL, 0, 0, 0,
};

static form wiegvl = {2, 0, 0, _wiegvl, 0, 0, 0, 0, &WiegFont};

int ShowWieg (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15;
        cy = 185;

        set_fkt (endlist, 5);
		SaveDblClck ();
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
//        SetListEWindow (1);
        SetListFont (TRUE);
        spezfont (ptform->font);
        SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
//        PtWindow = OpenListWindowEnEx (x, y, cx, cy);
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
        PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
//                                    y + cy + 23,
                                    y,
                                    cx + 24, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
//        SetMouseLock (TRUE);
        SetMouseLockProc (MouseInPt);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
        CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
//        SetMouseLock (FALSE);
        SetMouseLockProc (NULL);
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
		RestoreDblClck ();
        return ptidx;
}

int AuswahlWieg (void)
/**
Ausahlfenster ueber Wiegungen anzeigen
**/
{
        int i;
        int j;

        for (i = AktWiegPos - 1, j = 0; i >= 0; i --, j ++)
        {
            sprintf (wiegarr[j].wieg, "%d", i + 1);
            sprintf (wiegarr[j].menge, "%.3lf", AktPrWieg[i]);
        }
        wieganz = j;

        if (wieganz == 0)
        {
               return 0;
        }
        syskey = 0;
        wiegidx = ShowWieg (WiegWindow, (char *) wiegarr, wieganz,
                     (char *) &wiegt,(int) sizeof (struct WIEGT),
                     &wiegform, &wiegvl, &wiegub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        StornoWieg = ratod (wiegarr[wiegidx].menge);
        return TRUE;
}


/* Auswahl ueber Wiegekoepfe                    */


struct WKOPF
{
        char nr [10];
        char akt [10];
};


struct WKOPF wkopfarr [20], wkopf;


static int kopfidx;
static int kopfanz = 0;

mfont KopfFont = {"Courier New", 200, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                1,
                                NULL};

static field _wkopfform [] =
{
        wkopf.nr,     11, 1, 0, 1, 0, "%4d",   DISPLAYONLY, 0, 0, 0,
        wkopf.akt,    13, 1, 0,12, 0, "",      DISPLAYONLY, 0, 0, 0,
};

static form wkopfform = {2, 0, 0, _wkopfform, 0, 0, 0, 0, &KopfFont};

static field _wkopfub [] =
{
        "Wiegekopf",       11, 1, 0, 1, 0, "", DISPLAYONLY, 0, 0, 0,
        "  aktiv",         13, 1, 0,12, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form wkopfub = {2, 0, 0, _wkopfub, 0, 0, 0, 0, &KopfFont};

static field _wkopfvl [] =
{
    "1",                  1,  0, 0, 12, 0, "", NORMAL, 0, 0, 0,
    "1",                  1,  0, 0, 24, 0, "", NORMAL, 0, 0, 0,
};

static form wkopfvl = {2, 0, 0, _wkopfvl, 0, 0, 0, 0, &KopfFont};

int ShowWKopf (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub, LPSTR Caption)
{
        RECT rect;
        RECT frmrect;
        int x, y, cx, cy;
        HWND shListBox;
        HWND slbox;
        struct LMENUE smenue;

        GetMenue (&smenue);
        MenTextSave (&smenue);
        shListBox = GethListBox ();
        slbox = Getlbox ();

        RegisterPt ();
        GetWindowRect (hMainWindow, &rect);
        GetFrmRectEx (ptform, ptform->font, &frmrect);

        x = rect.left + 50;
        y = rect.top + 20;
        cx = frmrect.right + 15;
		if (PtY > -1)
		{
			cy = PtY;
			PtY = -1;
		}
		else
		{
            cy = 185;
		}

        save_fkt (5);
        set_fkt (endlist, 5);
		SaveDblClck ();
        SetDblClck (IsPtClck, 0);
        PtListeAktiv = 1;
        SetAktivWindow (hWnd);
        SetListFont (TRUE);
        spezfont (ptform->font);
		if (Caption == NULL)
		{
			SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE);
		}
		else
		{
			SetBorder (WS_POPUP | WS_DLGFRAME | WS_VISIBLE | WS_CAPTION);
		}
        PtWindow = OpenListWindowBoEx (x, y, cx, cy);
		if (Caption != NULL)
		{
			SetWindowText (PtWindow, Caption);
		}
        GetWindowRect (PtWindow, &rect);
        y = rect.bottom;
		cx = rect.right - rect.left;
		PtFussWindow = CreateWindow ("PtFuss",
                                    "",
                                    WS_POPUP | WS_VISIBLE | WS_DLGFRAME,
                                    x,
                                    y,
                                    cx, 70,
                                    hWnd,
                                    NULL,
                                    hMainInst,
                                    NULL);
        if (ptvl) ElistVl (ptvl);
        if (ptub) ElistUb (ptub);
        EnableWindow (hMainWindow, FALSE);
	    SetPosColorProc (NULL);
        ShowElist ((char *) ptabarr,
                  ptabanz,
                  (char *) ptab,
                  (int) ptsize,
                  ptform);
        EnterElist (PtWindow, (char *) ptabarr,
                             ptabanz,
                             (char *) ptab,
                             (int) ptsize,
                             ptform);
	    SetPosColorProc (ColorProc);
           CloseControls (&PtButton);
        CloseEWindow (PtWindow);
        SetActiveWindow (hMainWindow);
        EnableWindow (hMainWindow, TRUE);
        init_break_list ();
        DestroyWindow (PtFussWindow);
        PtListeAktiv = 0;
        SetMenue (&smenue);
        SethListBox (shListBox);
        Setlbox (slbox);
        SetAktivWindow (hWnd);
		RestoreDblClck ();
        restore_fkt (5);
        return ptidx;
}


int AuswahlKopf (void)
/**
Ausahlfenster ueber Geraete anzeigen.
**/
{
        int i;

        for (i = 0; i < la_zahl; i ++)
        {
                       sprintf (wkopfarr[i].nr, "%d", i + 1);
                       if (i == AktKopf)
                       {
                               sprintf (wkopfarr[i].akt, "   * ");
                       }
                       else
                       {
                               sprintf (wkopfarr[i].akt, "     ");
                       }
        }
        kopfanz = i;
        if (kopfanz == 0)
        {
               return 0;
        }
        syskey = 0;
        kopfidx = ShowWKopf (LogoWindow, (char *) wkopfarr, kopfanz,
                     (char *) &wkopf,(int) sizeof (struct WKOPF),
                     &wkopfform, &wkopfvl, &wkopfub);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return 0;
        }
        return kopfidx;
}
int ShowWKopf (HWND hWnd, char * ptabarr, int ptabanz, char * ptab,
                 int ptsize, form *ptform, form *ptvl, form *ptub)
{
    return ShowWKopf (hWnd, ptabarr, ptabanz, ptab,
				      ptsize, ptform, ptvl, ptub, NULL);
}



/* Auswahl bei leeren Positionen   */


BOOL LeerPosAuswahl ()
{
        static BOOL CpOK = FALSE;

        if (! CpOK)
        {
            strcpy (ktextarr[0],
                "Unbearbeitete Positionen ignorieren");
            strcpy (ktextarr[1],
                "Unbearbeitete Positionen l�schen");
            strcpy (ktextarr[2],
                "Ist-Menge = Soll-Menge");
        }
        SetHoLines (0);
        syskey = 0;
        if (eWindow)
        {
                EnableWindow (eWindow, FALSE);
                EnableWindow (eFussWindow, FALSE);
                EnableWindow (eKopfWindow, FALSE);
        }
        ktidx = ShowWKopf (eWindow, (char *) ktextarr, 3,
                     (char *) &ktext, 61,
                     &ktform, NULL, &ktub);
        if (eWindow)
        {
                EnableWindow (eWindow, TRUE);
                EnableWindow (eFussWindow, TRUE);
                EnableWindow (eKopfWindow, TRUE);
        }

        SetHoLines (1);
        if (syskey == KEY5 || syskey == KEYESC)
        {
                return FALSE;
        }
        switch (ktidx)
        {
                case 0 :
                    SetAllPosStat ();
                    return TRUE;
                case 1 :
                    DelLeerPos ();
                    return TRUE;
                case 2:
                    SollToIst ();
                    return TRUE;
        }
        return TRUE;
}



BOOL PosLeer (void)
/**
Reaktion auf nicht abgeschlossene Positionen.
**/
{
          EnableWindow (hMainWindow, FALSE);
          EnableWindow (eWindow, FALSE);
          EnableWindow (eFussWindow, FALSE);
          EnableWindow (eKopfWindow, FALSE);
          if (abfragejnG (eWindow,
                          "Es sind nicht bearbeitete Positionen vorhanden.\n"
                          "Abbrechen ?", "J"))
          {
              EnableWindow (hMainWindow, TRUE);
              EnableWindow (eWindow, TRUE);
              EnableWindow (eFussWindow, TRUE);
              EnableWindow (eKopfWindow, TRUE);
              return FALSE;
          }
          EnableWindow (hMainWindow, TRUE);
          EnableWindow (eWindow, TRUE);
          EnableWindow (eFussWindow, TRUE);
          EnableWindow (eKopfWindow, TRUE);
          return LeerPosAuswahl ();
}


BOOL TestPosis (void)
/**
Einzelne Positionen testen.
**/
{
          int i;

          for (i = 0; i < aufpanz_upd; i ++)
          {
              if (atoi (aufparr[i].s) < 3)
              {
                  return PosLeer ();
              }
          }
          return TRUE;
}

void TestWriteStat (void)
{
          int posanz = 0;
}



BOOL TestPosStatus (void)
/**
Test, ob der Lieferschein auf komplett gesetzt werden darf.
**/
{
          if (NewPosi () == FALSE)
          {
              disp_messG ("Der Lieferschein wird noch von einem\n"
                          "anderen Benutzer bearbeitet", 2);
              return FALSE;
          }

// Test, ob scghon alle Saetze in die Datenbank geschrieben sind
// Es gibt Problem beim Lieferscheindruck. Zum Zeipunkt des Druck's sind noch nicht alle
// Lieferscheinpositionen in der Datenbank.

          TestWriteStat ();

          if (TestPosis () == FALSE)
          {
              return FALSE;
          }
          return TRUE;
}

int LeerGutExist (void)
/**
Test, ob Leergutartikel erfasst wurden.
**/
{
          int i;
          int lganz;

          lganz = 0;
          for (i = 0; i < aufpanz_upd; i ++)
          {
              if (lese_a_bas (ratod (aufparr[i].a)) != 0)
              {
                  continue;
              }
              if (_a_bas.a_typ == 11)
              {
                  lganz += atoi (aufparr[i].lief_me);
              }
          }
          return lganz;
}

BOOL PrintAufkleber (int lganz)
/**
Druck fuer Aufkleber starten.
**/
{
          char buffer [256];
          DWORD ret;

          sprintf (buffer, "%d", lganz);

          EnterCalcBox (eWindow," Anzahl Aufkleber", buffer, 9,
                                   "%4d");

/*
          EnterNumBox (eWindow,
              " Wieviele Aufkleber sollen ausgedruckt werden", buffer, 9,
                                   "%4d");
*/
          lganz = atoi (buffer);
          if (lganz == 0) return TRUE;

          if (strcmp (sys_ben.pers_nam, " ") <= 0)
          {
             strcpy (sys_ben.pers_nam, "fit");
          }
          sprintf (buffer, "rswrun 53800 0 %s %hd %hd %ld %d",
                                 clipped (sys_ben.pers_nam),
                                 lsk.mdn, lsk.fil, lsk.kun, lganz);
          SetAktivWindow (eWindow);

//          disp_messG (buffer, 1);

/*
          sprintf (buffer, "rswrun 53800 0 %s %hd",
                                 clipped (sys_ben.pers_nam));
*/
          ret = ProcExec (buffer, SW_SHOWMINIMIZED, -1, 0, -1, 0);
          SetAktivWindow (eWindow);
          if (ret == 0)
          {
              disp_messG ("Fehler beim Start des Etikettendrucks",2);
          }
          else
          {
              disp_messG ("Etikettendruck gestartet", 2);
          }
          return TRUE;
}


BOOL LsLeerGut (void)
/**
Leergut testen.
**/
{
         int lganz;

         if (ls_leer_zwang == 0) return TRUE;
         if (lganz = LeerGutExist ())
         {
             return PrintAufkleber (lganz);
         }
         if (abfragejnG (eWindow, "Es wurde kein Leergutartikel erfasst\n"
                                  "Leergut erfassen", "J"))
         {
// Durch PostMessage wuerde das Fenster zum Erfassen von Artikeln
// geoeffnet.
//             PostMessage (eWindow, WM_KEYDOWN, VK_F6, 0l);
             return FALSE;
         }
         return TRUE;
}

/**
Leergut als Kopf oder Fusstext eingeben.
**/

#define LEERMAX 100

struct LP
{
	double lart;
	int    lanz;
} LeerPosArt [LEERMAX];


static BOOL BreakLeerEnter = FALSE;
static HWND lPosWindow;
static HWND hWndLeer;

static char KopfText[61];
static char KopfTextTab[LEERMAX][61];
static char KopfTextFTab[10][61];
static int kopftextfpos;
static int kopftextfscroll;

static int kopftextpos = 0;
static int kopftextanz = 0;
static int ktpos = 0;
static int ktend = 0;
static TEXTMETRIC tmLP;
static BOOL CrCaret = FALSE;
static char *ztab[] = {" = ", " + "};
static int  zpos = 0;
static BOOL NumIn = FALSE;

static char *meeinhLP[100];
static char *meeinhLPK[100];
static double leer_art [100];

static int meeinhpos  = 0;
static int palettepos = 0;
static int ptanz = 0;
static int ptscroll = 0;
static int ptstart  = 0;
static int ptend    = 0;
static int LeerScrollDown (void);
static int LeerScrollUp (void);

mfont PosLeerFontE = {"Courier New", 200, 0, 1,
                                     RGB (0, 0, 0),
                                     RGB (255, 255, 255),
                                     1,
                                     NULL};

COLORREF LeerBuColor = RGB (0, 255, 255);
COLORREF LeerBuColorPf = RGB (0, 0, 255);


static field _PosLeerE[] = {
  KopfTextFTab[0],          61, 0, 0, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[1],          61, 0, 1, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[2],          61, 0, 2, 1, 0, "", DISPLAYONLY,        0, 0, 0,
  KopfTextFTab[3],          61, 0, 3, 1, 0, "", DISPLAYONLY,        0, 0, 0};

form PosLeerE = {4, 0, 0, _PosLeerE, 0, 0, 0, 0, &PosLeerFontE};

int StopPosLeer (void)
{
	     BreakLeerEnter = TRUE;
		 return (0);
  }


LONG FAR PASCAL PosLeerProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        switch(msg)
        {

             case WM_PAINT :
				    if (hWnd == hWndLeer)
					{
				           display_form (hWnd, &PosLeerE, 0, 0);
					}
                    break;

             case WM_KEYDOWN :
				    if (wParam == VK_F5)
					{
						syskey = KEY5;
						StopPosLeer ();
					}
					break;
             case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5 ||
                        LOWORD (wParam) == VK_F5)
                     {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    if (LOWORD (wParam) == KEYCR)
                    {
                            syskey = KEYCR;
                            SendKey (VK_RETURN);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    if (LOWORD (wParam) == KEYUP)
                    {
                            syskey = KEYUP;
							if (currentfield > ptstart)
							{
								currentfield --;
							    SetFocus (fAuswahlEinh.mask[currentfield].feldid);
							}
							else
							{
								LeerScrollDown ();
							}
							meeinhpos = ptscroll + currentfield - 1;
                            break;
                    }
                    if (LOWORD (wParam) == KEYDOWN)
                    {
                            syskey = KEYUP;
							if (currentfield < ptend)
							{
								currentfield ++;
							    SetFocus (fAuswahlEinh.mask[currentfield].feldid);
							}
							else
							{
								LeerScrollUp ();
							}
							meeinhpos = ptscroll + currentfield - 1;
                            break;
					}

        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}

static void KonvFormParams (void)
/**
Laenge des Eingabefeldes anpassen.
**/
{
	     static BOOL paramsOK = FALSE;
		 HDC hdc;
		 HFONT hFont, oldfont;
		 int i;

		 if (paramsOK) return;

		 paramsOK = TRUE;
         spezfont (&PosLeerFontE);
         hFont = SetWindowFont (hMainWindow);
         hdc = GetDC (hMainWindow);
         oldfont = SelectObject (hdc, hFont);
         GetTextMetrics (hdc, &tmLP);
         DelFont (SelectObject (hdc, oldfont));
         ReleaseDC (hMainWindow, hdc);

		 for (i = 0; i < PosLeerE.fieldanz; i ++)
		 {
			 PosLeerE.mask[i].length = PosLeerE.mask[i].length * (short) tmLP.tmAveCharWidth;
			 if (PosLeerE.mask[i].rows == 0)
			 {
				 PosLeerE.mask[i].rows = (int) (double) ((double) tmLP.tmHeight * 1.3);
			 }
			 else
			 {
				 PosLeerE.mask[i].rows = PosLeerE.mask[i].rows * (short) tmLP.tmHeight;
			 }

			 if (PosLeerE.mask[i].pos[0] != -1)
			 {
				 PosLeerE.mask[i].pos[0] = PosLeerE.mask[i].pos[0] * (short) tmLP.tmHeight;
			 }

			 if (PosLeerE.mask[i].pos[1] != -1)
			 {
				 PosLeerE.mask[i].pos[1] = PosLeerE.mask[i].pos[1] * (short) tmLP.tmAveCharWidth;
			 }
		 }
}

void SetLPCaret (void)
{
	int x,y;
	int CaretH;
	BOOL cret;

	DestroyCaret ();

    CaretH = (int) (double) ((double) tmLP.tmHeight * 1.1);
    SetFocus (PosLeerE.mask[kopftextfpos].feldid);
    cret = CreateCaret (PosLeerE.mask[kopftextfpos].feldid, NULL, 0, CaretH);
    CrCaret = TRUE;

	x = ktpos * tmLP.tmAveCharWidth;
	y = 0;
	cret = ShowCaret (PosLeerE.mask[kopftextfpos].feldid);
	cret = SetCaretPos (x,y);
	ShowCaret (PosLeerE.mask[kopftextfpos].feldid);
}

static void DispWindowText (HWND hWnd, char *text)
/**
Text in Fenster anzeigen.
**/
{
	 InvalidateRect (hWnd, NULL, TRUE);
	 UpdateWindow (hWnd);
}

static void ToKopfFtab (void)
/**
Kopfdelder aus Array in FormFelder uebertragen.
**/
{
      int i, j;

	  while (kopftextfscroll < 0) kopftextfscroll ++;

      for (i = 0, j = kopftextfscroll; i < PosLeerE.fieldanz; i ++, j ++)
	  {
		  strcpy (KopfTextFTab[i], KopfTextTab[j]);
	  }
	  display_form (hWndLeer, &PosLeerE, 0, 0);
}



static void EinhToStr (void)
/**
Einheit an String anhaengen.
**/
{
	char Einheit[20];
	char *MeH;

	if (NumIn == FALSE) return;


    strcpy (Einheit, meeinhLPK[meeinhpos]);
	MeH = strtok (Einheit, " ");
	strcat (KopfTextFTab[kopftextfpos], " ");
    strcat (KopfTextFTab[kopftextfpos], MeH);

	if (zpos == 0)
	{
	        strcat (KopfTextFTab[kopftextfpos], ztab [zpos]);
	        zpos = 2;
	}

	ktend = strlen (KopfTextFTab[kopftextfpos]);
	ktpos = ktend;
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	SetLPCaret ();
	NumIn = FALSE;
    SetFocus (fAuswahlEinh.mask[currentfield].feldid);
}

void TestKopfText (char *kText)
/**
Beim Verlassen einer Zeile, den Kopfinhalt testen.
**/
{
	 int len;
	 char *gpos;


	 len = strlen (kText);
	 if (kText[len - 1] != '=' &&
		 kText[len - 2] != '=')
	 {
		 return;
	 }

	 gpos = strchr (kText, '=');

	 for (gpos = gpos - 1;  gpos != kText; gpos -=1)
	 {
		 if (*gpos > ' ')
		 {
			 *(gpos + 1) = 0;
			 return;
		 }
	 }
}


void TestKopfTextBef (char *kText)
/**
Vor dem Enter einer Zeile, den Kopfinhalt testen.
**/
{
	 if (strcmp (kText, " ") <= 0) return;

	 if (strchr (kText, '=') == NULL)
	 {
		 strcat (kText, " = ");
	 }
}

void ScrollKopfTextDown (int zeilen)
/**
Kopftext nach unten scrollen.
**/
{

	if (kopftextfscroll > 0)
	{
		kopftextfscroll --;
	}
	ToKopfFtab ();
}


void ScrollKopfTextUp (int zeilen)
/**
Kopftext nach oben scrollen.
**/
{
	if (kopftextfscroll < 95)
	{
		kopftextfscroll ++;
	}
	ToKopfFtab ();
}


void NextKopfZeile (void)
{
	int pos;

	EinhToStr ();
	strcpy (KopfTextTab[kopftextpos], KopfTextFTab [kopftextfpos]);
	TestKopfText (KopfTextTab[kopftextpos]);
	if (kopftextpos > 59) return;

	if (strcmp (KopfTextTab[kopftextpos], " ") <= 0) return;

	kopftextpos ++;
	if (kopftextfpos < PosLeerE.fieldanz - 1)
	{
	          kopftextfpos ++;
	}
	else
	{
              ScrollKopfTextUp (1);
	}
	if (kopftextpos >= kopftextanz) kopftextanz = kopftextpos + 1;
    strcpy (KopfTextFTab[kopftextfpos], KopfTextTab[kopftextpos]);
	clipped (KopfTextFTab[kopftextfpos]);
	TestKopfTextBef (KopfTextFTab[kopftextfpos]);
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	ktend = strlen (KopfTextFTab[kopftextfpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
    ktpos = ktend;
	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
	strcpy (KopfTextFTab[kopftextfpos], KopfText);
    NumIn = FALSE;
}


void PriorKopfZeile (void)
{
	int pos;

	EinhToStr ();
    GetWindowText (PosLeerE.mask[0].feldid, KopfText, 60);
	strcpy (KopfTextTab[kopftextpos], KopfTextFTab[kopftextfpos]);
	TestKopfText (KopfTextTab[kopftextpos]);
	if (kopftextpos < 1) return;
	kopftextpos --;
	if (kopftextfpos > 0)
	{
	          kopftextfpos --;
	}
	else
	{
              ScrollKopfTextDown (1);
	}
    strcpy (KopfTextFTab[kopftextfpos], KopfTextTab[kopftextpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
	clipped (KopfTextFTab[kopftextfpos]);
	TestKopfTextBef (KopfTextFTab[kopftextfpos]);
    DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
	ktend = strlen (KopfTextFTab[kopftextfpos]);
	strcpy (KopfText,KopfTextFTab[kopftextfpos]);
    ktpos = ktend;
	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
	strcpy (KopfTextFTab[kopftextfpos], KopfText);
    NumIn = FALSE;
}


void SetThisCaret(void)
{
	int pos;

	if (ktend == 0)
	{
 	         zpos = 0;
	}
	else
	{
		if (strchr (KopfText, '+'))
		{
		     zpos = 1;
		}
		else if (strchr (KopfText, '='))
		{
            pos = (int) strlen (KopfText);
			if (KopfText[pos - 1] == '=' ||
				KopfText[pos - 2] == '=')
			{
           	        zpos = 2;
			}
			else
			{
				    zpos = 1;
			}
		}
		else
		{
			zpos = 0;
		}
	}
    NumIn = FALSE;
	ktend = strlen (KopfText);
    ktpos = ktend;
	SetLPCaret ();
}


int ProcPosLeer (WPARAM wParam)
/**
Aktion auf Tasten in EnterNum ausfuehren.
**/
{
	      if (wParam >= (WPARAM) '0' &&
			  wParam <= (WPARAM) '9')
		  {
              	   if (NumIn == FALSE && zpos == 2)
				   {
	                      zpos = 1;
				   }
	               else if (NumIn == FALSE && zpos == 1)
				   {
	                      strcat (KopfTextFTab[kopftextfpos], ztab [zpos]);
						  ktpos = strlen (KopfTextFTab[kopftextfpos]);
				   }

			       KopfTextFTab[kopftextfpos][ktpos] = (char) wParam;
				   if (ktpos < 59) ktpos ++;
				   if (ktend < ktpos) ktend = ktpos;
			       KopfTextFTab[kopftextfpos][ktend] = (char) 0;
				   DispWindowText (PosLeerE.mask[kopftextfpos].feldid, NULL);
                   SetFocus (eNumWindow);
	               NumIn = TRUE;
		  }
		  else if (wParam == (WPARAM) ' ')
		  {
			       EinhToStr ();
		  }

		  else if (wParam == VK_F5)
		  {
			       syskey = KEY5;
  		           PostMessage (NULL, WM_USER, 0, 0);
                   PostMessage (NULL, WM_KEYDOWN,
                               (WPARAM) VK_F5, 0l);
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == (WPARAM) '=')
		  {
			       EinhToStr ();
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == (WPARAM) VK_RETURN)
		  {
			       EinhToStr ();
//                 SetFocus (eNumWindow);
			       syskey = KEYCR;
  		           PostMessage (NULL, WM_USER, 0, 0);
                   PostMessage (NULL, WM_KEYDOWN,
                               (WPARAM) VK_RETURN, 0l);
                   SetFocus (eNumWindow);
		  }
		  else if (wParam == VK_CLS)
		  {
 		          zpos = 0;
		          NumIn = FALSE;
                  memset (KopfTextFTab[kopftextfpos], ' ', 60);
                  KopfTextFTab[kopftextfpos] [0] = (char) 0;
				  ktpos = ktend = 0;
			      DispWindowText (PosLeerE.mask[kopftextpos].feldid, NULL);
                  SetFocus (eNumWindow);
		  }

		  else if (wParam == VK_DOWN)
		  {
			      NextKopfZeile ();
                  SetFocus (eNumWindow);
		  }
		  else if (wParam == VK_UP)
		  {
			      PriorKopfZeile ();
                  SetFocus (eNumWindow);
		  }

          SetLPCaret ();

          SetFocus (fAuswahlEinh.mask[currentfield].feldid);
		  return 1;
}

static int MeProc (HWND hWnd)
/**
Mengeneinheit wechseln.
**/
{
		  meeinhpos ++;
		  if (meeinhLP[meeinhpos] == NULL)
		  {
			  meeinhpos = 0;
		  }
          TextForm.mask[0].feld = meeinhLP[meeinhpos];
		  InvalidateRect (hWnd, NULL, TRUE);
		  return 0;
}

static void strzcpy (char *dest, char *source, int len)
/**
source in dest zentrieren.
**/
{
	    int zpos;

		memset (dest, ' ', len);
		dest[len] = (char) 0;
		if ((int) strlen (source) > len - 1)
		{
			source[len - 1] = (char) 0;
		}
		zpos = (int) (double) ((double) (len - strlen (source)) / 2 + 0.5);
		zpos = max (0, zpos);
		memcpy (&dest[zpos], source, strlen (source));
}

void FillEinhLP (void)
/**
Einheiten aus Prueftabelle lesen.
**/
{
	    static BOOL EinhOK = FALSE;
		int dsqlstatus;

		if (EinhOK) return;

		EinhOK = TRUE;

		ptanz = 0;
		palettepos = ptanz;
        dsqlstatus = ptab_class.lese_ptab_all ("me_einh_leer");
        while (dsqlstatus == 0)
        {
			if ((meeinhLP[ptanz] = (char *) malloc (40)) == 0)
			{
				break;
			}
			if ((meeinhLPK[ptanz] = (char *) malloc (10)) == 0)
			{
				break;
			}
            strzcpy (meeinhLP[ptanz],  clipped (ptabn.ptbez), 16);
            strcpy  (meeinhLPK[ptanz], clipped (ptabn.ptbezk));
			leer_art[ptanz] = ratod (ptabn.ptwer1);
			if (strupcmp (ptabn.ptbez, "PALETTE", 7) == 0)
			{
				palettepos = ptanz;
			}
            ptanz ++;
            if (ptanz == 99) break;
            dsqlstatus = ptab_class.lese_ptab_all ();
        }
		meeinhLP[ptanz]  = NULL;
 	    meeinhLPK[ptanz] = NULL;
		ptscroll = 0;
}

void LeerBuScrollDown (int start, int end, int ze)
{
	    int i;
		ColButton *ColBut1, *ColBut2;

        while (start - ze + 1 < 0) ze --;
		for (i = end; i > start; i --)
		{
			ColBut1 = (ColButton *) fAuswahlEinh.mask[i].feld;
			ColBut2 = (ColButton *) fAuswahlEinh.mask[i - ze].feld;
			ColBut1->text1 = ColBut2->text1;
		}
		ColBut1 = (ColButton *) fAuswahlEinh.mask[i].feld;
		ColBut1->text1 = "";
}


int GetMeEinhPos (char *text)
{
	    int i;

		for (i = 0; i < ptanz; i ++)
		{
			if (strcmp (text, meeinhLP[i]) == 0)
			{
				return i;
			}
		}
		return meeinhpos;
}


int SetLeerText (void)
{
		ColButton *ColBut;

	    ColBut = (ColButton *) fAuswahlEinh.mask[currentfield].feld;
		meeinhpos = GetMeEinhPos (ColBut->text1);
		EinhToStr ();
		return 0;
}

int LeerScrollUp (void)
{
        int i, j;
		int scrollanz;
		int scrollplus;
		ColButton *ColBut;

		scrollanz  = fAuswahlEinh.fieldanz - 2;
		scrollplus = fAuswahlEinh.fieldanz - 3;

		scrollplus = max (0, scrollplus);
        while (scrollplus > 0)
		{
			if ((ptscroll + scrollplus + scrollanz) <= ptanz)
			{
				break;
			}
			scrollplus --;
		}

		ptscroll += scrollplus;
		for (i = ptscroll, j = 1; j <= scrollanz; i ++, j ++)
		{
			ColBut = (ColButton *) fAuswahlEinh.mask[j].feld;
			ColBut->text1 = meeinhLP[i];
		}
		display_form (eNumWindow, &fAuswahlEinh, 0, 0);
		return 0;
}

int LeerScrollDown (void)
{
        int i, j;
		int scrollanz;
		int scrollplus;
		ColButton *ColBut;

		scrollanz  = fAuswahlEinh.fieldanz - 2;
		scrollplus = fAuswahlEinh.fieldanz - 3;

		scrollplus = max (0, scrollplus);
        while (scrollplus > 0)
		{
			if ((ptscroll - scrollplus) >= 0)
			{
				break;
			}
			scrollplus --;
		}


		ptscroll -= scrollplus;
		for (i = ptscroll, j = 1; j <= scrollanz; i ++, j ++)
		{
			ColBut = (ColButton *) fAuswahlEinh.mask[j].feld;
			ColBut->text1 = meeinhLP[i];
		}
		display_form (eNumWindow, &fAuswahlEinh, 0, 0);
		return 0;
}


void SetLeerBuColors (void)
{
	    int i;
		ColButton *ColBut;

		for (i = 0; i < fAuswahlEinh.fieldanz; i ++)
		{
			ColBut = (ColButton *) fAuswahlEinh.mask[i].feld;
			ColBut->BkColor = LeerBuColor;
		}
}

void SetLeerBuTxt (void)
{
	    int i;
		ColButton *ColBut;

		currentfield = 0;
		ptstart = 0;
		ptend = fAuswahlEinh.fieldanz - 1;
        if (pfeilu == NULL)
		{
                  pfeilu = LoadBitmap (hMainInst, "pfeilu");
		}
        if (pfeilo == NULL)
		{
                  pfeilo = LoadBitmap (hMainInst, "pfeilo");
		}

		for (i = 0; i < fAuswahlEinh.fieldanz; i ++)
		{
			if (meeinhLP[i] == NULL) break;
			ColBut = (ColButton *) fAuswahlEinh.mask[i].feld;
			ColBut->text1 = meeinhLP[i];
			fAuswahlEinh.mask[i].after = SetLeerText;
		}
		if (ptanz >= fAuswahlEinh.fieldanz)
		{
			i = fAuswahlEinh.fieldanz - 1;
			ColBut = (ColButton *) fAuswahlEinh.mask[i].feld;
			ColBut->text1 = "";
			ColBut->bmp   = pfeilu;
			ColBut->BkColor = LeerBuColorPf;
			ColBut->aktivate = 14;
			fAuswahlEinh.mask[i].after = LeerScrollUp;
 		    currentfield = 1;
			ptstart = 1;
			ptend = fAuswahlEinh.fieldanz - 2;

		}
        LeerBuScrollDown (0, fAuswahlEinh.fieldanz - 2, 1);
		ColBut = (ColButton *) fAuswahlEinh.mask[0].feld;
		ColBut->text1 = "";
		ColBut->bmp   = pfeilo;
		ColBut->BkColor = LeerBuColorPf;
		ColBut->aktivate = 14;
		fAuswahlEinh.mask[0].after = LeerScrollDown;
}

void EnterLeerPos (void)
/**
Leergut auf Positionsebene einegen.
**/
{
	    int x,y, cx, cy;
		HFONT hFont;
        HDC hdc;
		TEXTMETRIC tm;
		int i;

		FillEinhLP ();
		if (ptanz == 0)
		{
			disp_messG ("Keine Pr�ftabelleneintr�ge f�r Leergut", 2);
		    return;
		}
		RECT eRect, fRect;

 	    save_fkt (4);
		save_fkt (5);
		save_fkt (6);
		save_fkt (7);
		save_fkt (8);
		save_fkt (9);
		save_fkt (10);
		save_fkt (11);

		set_fkt (NULL, 4);
		set_fkt (NULL, 5);
		set_fkt (NULL, 6);
		set_fkt (NULL, 7);
		set_fkt (NULL, 8);
		set_fkt (NULL, 9);
		set_fkt (NULL, 10);
		set_fkt (NULL, 11);

		set_fkt (StopPosLeer, 5);

		SetNum3 (TRUE);

		GetWindowRect (eWindow,     &eRect);
		GetWindowRect (eFussWindow, &fRect);


		EnableWindows (eWindow, FALSE);
		EnableWindows (BackWindow3, FALSE);
		EnableWindows (BackWindow2, FALSE);
        x  = eRect.left;
		y  = eRect.top;
		cx = eRect.right - eRect.left;
		cy = eRect.bottom - eRect.top + fRect.bottom - fRect.top;

        PosLeerFontE.FontHeight = 200;
        while (TRUE)
        {
                 spezfont (&PosLeerFontE);
                 hFont = SetWindowFont (hMainWindow);
                 hdc = GetDC (hMainWindow);
                 SelectObject (hdc, hFont);
                 GetTextMetrics (hdc, &tm);
                 DelFont (hFont);
                 ReleaseDC (hMainWindow, hdc);

                 if ((tm.tmAveCharWidth * 60) < cx) break;
                 PosLeerFontE.FontHeight -= 5;
                 if (PosLeerFontE.FontHeight <= 80) break;
        }
        KonvFormParams ();
        spezfont (&PosLeerFontE);
        hFont = SetWindowFont (hMainWindow);
        hdc = GetDC (hMainWindow);
        SelectObject (hdc, hFont);
        GetTextMetrics (hdc, &tm);
        for (i = 1; i < PosLeerE.fieldanz; i ++)
		{
			PosLeerE.mask[i].pos[0] = (int) (double)
				((double) PosLeerE.mask[i - 1].pos[0] + tm.tmHeight * 1.3);
		}

        lPosWindow = CreateWindow ("PosLeer",
                                    "",
                                    WS_VISIBLE | WS_POPUP | WS_DLGFRAME,
                                    x, y,
                                    cx, cy,
                                    eWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);


		x = (cx - 62 * tm.tmAveCharWidth) / 2;
		y = (int) (double) ((double) 1 * tm.tmHeight * 1.3);
		cx = 62 * tm.tmAveCharWidth;
		cy = (int) (double) ((double) 4 * tm.tmHeight * 1.3);

		hWndLeer = CreateWindowEx (WS_EX_CLIENTEDGE,
			                       "PosLeerKopf",
								   "",
                                    WS_VISIBLE | WS_CHILD,
                                    x, y,
                                    cx, cy,
                                    lPosWindow,
                                    NULL,
                                    hMainInst,
                                    NULL);

		SetNumParent (lPosWindow);
		SetNumZent (TRUE, 0, (int) (double) ((double) 30 * scrfy));
//		FillEinhLP ();
		SetLeerBuColors ();
		SetLeerBuTxt ();
		ktpos = ktend = 0;
		meeinhpos = palettepos;
		kopftextpos = 0;
		zpos = 0;
		NumIn = FALSE;
		CrCaret = FALSE;
		KopfText[ktend] = (char) 0;
        create_enter_form (hWndLeer, &PosLeerE, 0, 0);

        SetEform (&PosLeerE, lPosWindow, ProcPosLeer);
        SetMeProc (MeProc);

        ReadKopftext ();
        strcpy (KopfTextFTab[0], KopfTextTab[0]);
        strcpy (KopfText, KopfTextTab[0]);
	    TestKopfTextBef (KopfTextFTab[0]);
   	    ktend = strlen (KopfTextFTab[0]);
	    ktpos = ktend;
        DispWindowText (PosLeerE.mask[0].feldid, KopfTextFTab[0]);
        SetNumCaretProc (SetThisCaret);

        EnterCalcBox (lPosWindow, meeinhLP[1], "", 40, "");
	    strcpy (KopfTextTab[kopftextpos], KopfTextFTab[kopftextfpos]);
	    TestKopfText (KopfTextTab[kopftextpos]);

		SetActiveWindow (lPosWindow);
		set_fkt (StopPosLeer, 5);

		EnableWindows (eWindow, TRUE);
		EnableWindows (BackWindow3, TRUE);
		EnableWindows (BackWindow2, TRUE);

		DestroyWindow (hWndLeer);
		DestroyWindow (lPosWindow);

		DestroyCaret ();
		SetActiveWindow (eWindow);
		if (syskey != KEY5)
		{
		         WriteKopftext ();
		}
        ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
 	    restore_fkt (4);
		restore_fkt (5);
		restore_fkt (6);
		restore_fkt (7);
		restore_fkt (8);
		restore_fkt (9);
		restore_fkt (10);
		restore_fkt (11);
}


void ReadKopftext (void)
/**
Kopftext lesen.
**/
{
		int dsqlstatus;
		int zei;
		int i;


		for (i = 0; i < PosLeerE.fieldanz; i ++)
		{
		          memset (KopfTextFTab[i], ' ', 60);
				  KopfTextFTab[i][60] = 0;
	 		      clipped (KopfTextFTab[i]);
		}
		kopftextfpos = kopftextfscroll = 0;


		for (i = 0; i < kopftextanz; i ++)
		{
		          memset (KopfTextTab[i], ' ', 60);
				  KopfTextTab[i][60] = 0;
	 		      clipped (KopfTextTab[i]);
		}

		kopftextanz = zei = 0;

		if (lsk.kopf_txt == 0l)
		{
		           ToKopfFtab ();
				   return;
		}

		ls_txt.nr = lsk.kopf_txt;
	    dsqlstatus = ls_txt_class.dbreadfirst ();
		while (dsqlstatus == 0)
		{

			      clipped (ls_txt.txt);
                  strcpy (KopfTextTab[zei], ls_txt.txt);

				  kopftextanz ++;
				  zei ++;
				  dsqlstatus = ls_txt_class.dbread ();
		}
		ToKopfFtab ();
}


static void InitLeerPosArt (void)
/**
Tabelle mit Leergutartikel initialisieren.
**/
{
	    int i;

		for (i = 0; i < LEERMAX; i ++)
		{
			LeerPosArt[i].lart = (double) 0.0;
			LeerPosArt[i].lanz = 0;
		}
}

static void SetLeerPosArt (double art, int anz)
/**
Tabelle mit Leergutartikel initialisieren.
**/
{
	    int i;

		if (anz == 0) return;
		if (art == (double) 0.0) return;

		for (i = 0; LeerPosArt[i].lanz; i ++)
		{
			if (i == LEERMAX) return;
			if (LeerPosArt[i].lart == art)
			{
				break;
			}
		}

		LeerPosArt[i].lart  = art;
		LeerPosArt[i].lanz += anz;
}

static double GetLeerArt (char *LeerBez)
/**
Artikel-Nummer zu Leergutbezeichnung holen.
**/
{
	    int i;

		for (i = 0; i < ptanz; i ++)
		{
			if (strcmp (meeinhLPK[i], LeerBez) == 0)
			{
				return leer_art[i];
			}
		}
		return (double) 0.0;
}

static BOOL GetNextEinh (char *LeerZeile, int *anz, char *LeerBez, BOOL mode)
/**
Nexte Leergutbezeichnung und Leergutanzahl aus Zeile holen.
**/
{
	     static char *pos = NULL;
		 static char *LZ = NULL;
		 char anzstr [10];
		 int i;

		 if (LZ != LeerZeile)
		 {
			 pos = LeerZeile;
		 }
		 if (mode == FALSE)
		 {
			 pos = LeerZeile;
		 }

		 for (; *pos; pos += 1)
		 {
			 if (*pos == '+');
			 else if (*pos == '=');
			 else if (*pos > ' ')
			 {
				 break;
			 }
		 }

         if (*pos == 0)
		 {
			  *anz = 0;
              LeerBez[0] = 0;
			  return FALSE;
		 }

		 i = 0;
		 for (; *pos; pos += 1)
		 {
			 if (*pos < '0' || *pos > '9')
			 {
				 break;
			 }
			 anzstr[i] = *pos;
			 i ++;
		 }

         if (*pos == 0)
		 {
			  *anz = 0;
              LeerBez[0] = 0;
			  return FALSE;
		 }

		 LZ = LeerZeile;
         anzstr[i] = 0;
		 *anz = atoi (anzstr);
		 for (; *pos; pos += 1)
		 {
			 if (*pos == '+');
			 else if (*pos == '=');
			 else if (*pos > ' ')
			 {
				 break;
			 }
		 }

         if (*pos == 0)
		 {
              LeerBez[0] = 0;
			  return FALSE;
		 }


		 i = 0;
		 for (; *pos; pos += 1)
		 {
			 if (*pos <= ' ')
			 {
				 break;
			 }
             LeerBez[i] = *pos;
			 i ++;
		 }
		 LeerBez[i] = 0;
		 return TRUE;
}

static BOOL IsLeerGutZeile (char *Zeile)
/**
Test, ob die Zeile eine Leergutzeile ist.
**/
{
	     int i, j;
		 int anz;

	     for (i = 0; i < ptanz; i ++)
		 {
			 if (strstr (Zeile, meeinhLPK[i]) == NULL)
			 {
				 break;
			 }
		 }
		 if (i == ptanz) return FALSE;

		 anz = wsplit (Zeile, " ");
		 if (anz == 0) return FALSE;

		 for (i = 0; i < anz; i ++)
		 {
			 if (strcmp (wort[i], "+") == 0) continue;
			 if (strcmp (wort[i], "=") == 0)continue;
			 if (numeric (wort[i])) continue;
  	         for (j = 0; j < ptanz; j ++)
			 {
			        if (strcmp (wort[i], meeinhLPK[j]) == 0)
					{
				              break;
					}
			 }
			 if (j == ptanz) return FALSE;
		 }
		 return TRUE;
}

static BOOL IsLeerGut (double a)
/**
Artikeltyp pruefen.
**/
{
	     int dsqlstatus;

		 if (a <= (double) 0.0) return FALSE;
		 dsqlstatus = lese_a_bas (a);
		 if (dsqlstatus == 100) return FALSE;
         if (_a_bas.a_typ != 11) return FALSE;
		 return TRUE;
}


void WriteLeerZeileArt (char *LeerZeile)
/**
Eine Zeile fuer Leegutartikel bearbeiten.
**/
{
	     double art;
		 int anz;
		 char Bez[20];

		 if (IsLeerGutZeile(LeerZeile) == FALSE) return;

		 if (GetNextEinh (LeerZeile, &anz, Bez, 0) == FALSE) return;

		 art = GetLeerArt (Bez);
		 if (IsLeerGut (art))
		 {
			 SetLeerPosArt (art, anz);
		 }

		 while (GetNextEinh (LeerZeile, &anz, Bez, 1))
		 {
           		 art = GetLeerArt (Bez);
		         if (IsLeerGut (art))
				 {
				         SetLeerPosArt (art, anz);
				 }
		 }
}

static BOOL IsinLeerPos (double art)
/**
Test, ob Leergutartikel aud Positionen im Kopftext erfasst wurden.
**/
{
        int i;

		for (i = 0; LeerPosArt[i].lanz; i ++)
		{
			if (i == LEERMAX) return FALSE;
			if (LeerPosArt[i].lart == art)
			{
				return TRUE;
			}
		}
		return FALSE;
}

long DelLeerPosArt (void)
/**
Positionszeilen mit Leergutartikeln, die als KopfTexte erfass wurden aus den Positionen
loeschen.
**/
{
	     int i;
		 static double art;
		 long posi;
		 long max_posi;
		 static int cursor = -1;
         static int ls;

		 if (cursor == -1)
		 {
			 DbClass.sqlin ((short *)  &lsk.mdn, 1, 0);
			 DbClass.sqlin ((short *)  &lsk.fil, 1, 0);
			 DbClass.sqlin ((long *)   &lsk.ls,  2, 0);
			 DbClass.sqlin ((double *) &art,     3, 0);
			 cursor = DbClass.sqlcursor ("delete from lsp where mdn = ? "
				                                          "and fil  = ? "
														  "and ls   = ? "
														  "and a    = ?");
		 }
		 max_posi = 0;
		 for (i = 0; i < aufpanz; i ++)
		 {
			 art = ratod (aufparr[i].a);
			 posi = atol (aufparr[i].pnr);
			 if (posi > max_posi) max_posi = posi;
			 if (IsinLeerPos (art))
			 {
//				 ScrollAufpDel (i);
				 DbClass.sqlexecute (cursor);

			 }
		 }
		 return max_posi;
}


void WriteLeerArt (void)
/**
Artikel als Positionen fuer Leergut schreiben.
**/
{
	     int i;
		 long max_posi;
		 int pos;
         struct AUFP_S aufp;

         memcpy (&aufp, &aufps, sizeof (struct AUFP_S));
		 InitLeerPosArt ();
		 for (i = 0; i < kopftextanz; i ++)
		 {
			 WriteLeerZeileArt (KopfTextTab[i]);
		 }
		 max_posi = DelLeerPosArt ();
         pos = aufpanz;
		 max_posi += 10;
 		 for (i = 0; LeerPosArt[i].lanz; i ++)
		 {
                 memcpy (&aufps, &aufps_null, sizeof (struct AUFP_S));
                 if (! artikelOK (LeerPosArt[i].lart, FALSE)) continue;
			     sprintf (aufps.a, "%.0lf", LeerPosArt[i].lart);
                 GetMeEinh ();
			     strcpy (aufps.auf_me_bz, aufps.lief_me_bz);
			     strcpy (aufps.me_einh_kun, aufps.me_einh);
				 sprintf (aufps.auf_me,  "%d", LeerPosArt[i].lanz);
				 sprintf (aufps.lief_me, "%d", LeerPosArt[i].lanz);
				 strcpy (aufps.s, "3");
				 sprintf (aufps.pnr, "%ld", max_posi);
                 memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
                 arrtoretp (pos);
                 Retp.dbupdate ();
				 max_posi += 10;
				 pos += 1;
		 }
		 aufpanz = pos;
         memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
}


void WriteKopftext (void)
/**
Kopftext schreiben.
**/
{
        extern short sql_mode;
		int dsqlstatus;
		int zei;

		if (kopftextanz == 0 && strcmp (KopfText, "  "))
		{
			strcpy (KopfTextTab[0], KopfText);
			kopftextanz ++;
		}
		commitwork ();
		beginwork ();
        if (lsk.kopf_txt == 0)
        {
                sql_mode = 1;
                dsqlstatus = AutoClass.nvholid (0, 0, "ls_txt");
				if (dsqlstatus == -1)
				{
							DbClass.sqlcomm ("delete from auto_nr where nr_nam = \"ls_txt\" "
								              "and mdn = 0 and fil = 0");
							dsqlstatus = 100;
				}

                if (dsqlstatus == 100)
                {
                           dsqlstatus = AutoClass.nvanmprf (0,
                                                  0,
                                                  "ls_txt",
                                                  (long) 1,
                                                  (long) 999999,
                                                  (long) 10,
                                                  "");

                           if (dsqlstatus == 0)
                           {
                                  dsqlstatus = AutoClass.nvholid (0,
                                                        0,
                                                        "ls_txt");
                           }
                }
                sql_mode = 0;
                if (auto_nr.nr_nr == 0l)
                {
                    disp_messG ("Es konnte keine Text-Nr generiert werden", 2);
                    return;
                }

                lsk.kopf_txt = auto_nr.nr_nr;
                ls_class.update_lsk (lsk.mdn, lsk.fil, lsk.ls);

                auto_nr.nr_nr = 0;

//        Die Transtion wird an dieser Stelle geschlossen.

                 commitwork ();

                 beginwork ();
		}
		ls_txt.nr = lsk.kopf_txt;
		ls_txt_class.delete_aufpposi ();
		for (zei = 0; zei < kopftextanz; zei ++)
		{
			      if (KopfTextTab[zei][0] == 0) continue;
			      ls_txt.nr  = lsk.kopf_txt;
				  ls_txt.zei = zei + 1;
				  strcpy (ls_txt.txt, KopfTextTab[zei]);
				  ls_txt_class.dbupdate ();
		}
		WriteLeerArt ();
}

// Auswahl ueber Kunden

mfont ckunfont = {"Courier New", 120, 0, 1, RGB (0, 0, 0),
                                RGB (255, 255, 255),
                                0,
                                NULL};

struct CKUN
{
char kun [9];
char kun_name1 [37];
};

static struct CKUN ckun, *ckunarr = NULL;
static int ckunanz;

static int sortckun ();
static int sortcname ();

static field _ckunform [] = {
	ckun.kun,          8, 0, 0, 1, 0, "%8d",    DISPLAYONLY, 0, 0, 0,
	ckun.kun_name1,   50, 0, 0,12, 0, "",       DISPLAYONLY, 0, 0, 0,
};

static form ckunform = {2, 0, 0, _ckunform, 0, 0, 0, 0, &ckunfont};


static field _kunub [] =
{
        "Kunde",              10, 1, 0, 0, 0, "", BUTTON, 0, sortckun,  0,
        " Name         ",     63, 1, 0,10, 0, "", BUTTON, 0, sortcname, 1,
};

static form kunub = {2, 0, 0, _kunub, 0, 0, 0, 0, &ckunfont};

static field _kunvl [] =
{
        "1",                1, 1, 0,10, 0, "", DISPLAYONLY, 0, 0, 0,
};

static form kunvl = {1, 0, 0, _kunvl, 0, 0, 0, 0, &ckunfont};
static int ckunsort  = 1;
static int cnamesort = 1;


static int sortckun0 (const void *elem1, const void *elem2)
{
	        struct CKUN *el1;
	        struct CKUN *el2;

			el1 = (struct CKUN *) elem1;
			el2 = (struct CKUN *) elem2;
	        return ((int) (atol(el1->kun) - atol (el2->kun)) * ckunsort);
}


static int sortckun (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (ckunarr, ckunanz, sizeof (struct CKUN),
				   sortckun0);
			ckunsort *= -1;
            ShowNewElist ((char *) ckunarr,
                             ckunanz,
                            (int) sizeof (struct CKUN));
			return 0;
}


static int sortcname0 (const void *elem1, const void *elem2)
{
	        struct CKUN *el1;
	        struct CKUN *el2;

			el1 = (struct CKUN *) elem1;
			el2 = (struct CKUN *) elem2;
			return ( (int) (strcmp (el1->kun_name1, el2->kun_name1)) * cnamesort);
}

static int sortcname (void)
/**
Nach Artikelnummer sortieren.
**/
{
	        qsort (ckunarr, ckunanz, sizeof (struct CKUN),
				   sortcname0);
			cnamesort *= -1;
            ShowNewElist ((char *) ckunarr,
                             ckunanz,
                            (int) sizeof (struct CKUN));
			return 0;
}



int dokunchoise (void)
/**
Auswahl ueber Artikel
**/
{
	    static long kunanz = 0;
		long kun;
		char name [37];
		int cursor;
		int idx;
		int i;
		char kun_krz [18];
		char buffer [0x400];
 	    char matchorg [25];
	    char matchupper[25];
	    char matchlower[25];
	    char matchuplow[25];


		strcpy (kun_krz, "");
		if (kunquery)
		{
            textblock = new TEXTBLOCK;
//			textblock->SetLongText (TRUE);
//        textblock->EnterTextBox (eNumWindow, "Name", kun_krz, 17, "", EntNumProc);
            textblock->MainInstance (hMainInst, hMainWindow);
            textblock->ScreenParam (scrfx, scrfy);
            textblock->EnterTextBox (AufKopfWindow, "Kunden-Name", kun_krz, 17, "", EntNumProc);

		    delete textblock;
		    textblock = NULL;
		}
		kun_krz[16] = 0;
		if (syskey == KEY5 || syskey == KEYESC)
		{
			    ChoiseWindow = NULL;
                return FALSE;
		}


 	    if (ChoiseWindow == NULL)
		{
			if (eNumWindow)
			{
			          ChoiseWindow = eNumWindow;
			}
			else
			{
				      ChoiseWindow = AufKopfWindow;
			}
		}
		if (scrfx > (double) 1.0)
		{
                ckunfont.FontHeight = (int) (double) ((double) 120 *  scrfx);
		}
		ckunsort = cnamesort = 1;
		if (ckunarr)
		{
			    GlobalFree (ckunarr);
		}
        DbClass.sqlout  ((long *) &kunanz, 2, 0);
        DbClass.sqlcomm ("select count (*) from kun");
		if (kunanz == 0)
		{
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
			return 0;
		}

        ckunarr = (struct CKUN *) GlobalAlloc (GMEM_FIXED, kunanz * sizeof (struct CKUN));
		if (ckunarr == NULL)
		{
			disp_messG ("Fehler beim Zuweisen von Speicherplatz", 2);
            EnableWindows (BackWindow2, FALSE);
            EnableWindows (BackWindow3, FALSE);
			return 0;
		}

		lsk.mdn = akt_mdn;
		lsk.fil = akt_fil;
		DbClass.sqlin ((short *) &lsk.mdn, 1, 0);
		DbClass.sqlin ((short *) &lsk.fil, 1, 0);

        DbClass.sqlout  ((double *) &kun, 2, 0);
        DbClass.sqlout  ((char *)  name, 0, 37);
		if (strcmp (kun_krz, "                 ") <= 0)
		{
		       cursor = DbClass.sqlcursor ("select kun.kun, adr.adr_nam1 from kun,adr "
			                               "where kun > 0 "
					          			   "and kun.mdn = ? "
								 	       "and kun.fil = ? "
									       "and kun.adr1 = adr.adr "
									       "order by adr.adr_nam1");
		}

		else if (kunquery == 1)
		{
			   sprintf (buffer, "select kun.kun, kun.kun_krz1 from kun "
			                    "where kun > 0 "
					          	"and kun.mdn = ? "
								"and kun.fil = ? "
								"and kun.kun_krz1 matches \"%s*\" "
								"order by kun_krz1", kun_krz);
			   cursor = DbClass.sqlcursor (buffer);
		}

		else
		{
	           strcpy (matchorg, kun_krz);
               strupcpy (matchupper, kun_krz);
               strlowcpy (matchlower, kun_krz);
               struplowcpy (matchuplow, kun_krz);
			   sprintf (buffer, "select kun.kun, kun.kun_krz1 from kun "
			                    "where kun > 0 "
					          	"and kun.mdn = ? "
								"and kun.fil = ? "
								"and (kun.kun_krz1 matches \"%s*\" "
								"or   kun.kun_krz1 matches \"%s*\" "
								"or   kun.kun_krz1 matches \"%s*\" "
								"or   kun.kun_krz1 matches \"%s*\") "
								"order by kun_krz1", matchorg, matchupper,
								matchlower, matchuplow);
			   cursor = DbClass.sqlcursor (buffer);
		}
		i = 0;
		while (DbClass.sqlfetch (cursor) == 0)
		{
			sprintf (ckunarr[i].kun, "%8d", kun);
			strcpy  (ckunarr[i].kun_name1, name);
			i = i + 1;
		}
		DbClass.sqlclose (cursor);

		kunanz = ckunanz = i;
        SetHoLines (1);
        SetVeLines (1);
		PtY = 410;
		if (scrfx > (double) 1.0)
		{
			PtY = (int) (double) ((double) PtY * scrfy);
		}
        syskey = 0;

		if (eNumWindow)
		{
                 EnableWindow (eNumWindow, FALSE);
                 EnableWindow (ChoiseWindow, FALSE);
		}
        idx = ShowWKopf (ChoiseWindow, (char *) ckunarr, kunanz,
                        (char *) &ckun, sizeof (struct CKUN),
                         &ckunform, &kunvl, &kunub);
		if (eNumWindow)
		{
                 EnableWindow (eNumWindow, TRUE);
                 EnableWindow (ChoiseWindow, TRUE);
		}
		CloseControls (&kunub);
        SetHoLines (1);
/*
		if (ChoiseWindow == eNumWindow)
		{
                   EnableWindows (BackWindow2, FALSE);
                   EnableWindows (BackWindow3, FALSE);
		}
		else
		{
                   EnableWindows (BackWindow2, TRUE);
                   EnableWindows (BackWindow3, TRUE);
		}
*/
        if (syskey == KEY5 || syskey == KEYESC)
        {
			    GlobalFree (ckunarr);
		        ckunarr = NULL;
		        ChoiseWindow = NULL;
                return FALSE;
        }

        strcpy (editbuff, ckunarr[idx].kun);
		GlobalFree (ckunarr);
		ckunarr = NULL;
		if (ChoiseWindow == eNumWindow)
		{
		        display_form (eNumWindow, &EditForm, 0, 0);
		        SetFocus (EditForm.mask[0].feldid);
		        if (kun_ausw_direct)
				{
		                  break_num_enter = TRUE;
				}
		}
		ChoiseWindow = NULL;
		return 0;
}

int ScanEan128Ai01 (Text& Ean128)
{
        double ean1 = 0.0;

		if (ScanValues == NULL) return -1;

		if (ScanValues->aflag) return 0;

		Text *ean = Scanean.GetEan128Ai (Text ("01"), Ean128);
		if (ean == NULL)
		{

				ScanValues->Init (); //020712
				return 100;
		}

		Text Ean = ean->SubString (1, Ean128Eanlen);
		delete ean;
		double a = ratod (Ean.GetBuffer ());
		ean1 = a;
		if (artikelOK (a, TRUE) == FALSE)
		{
			ScanValues->Init (); //020712
			return 100;
		}
		ScanValues->SetA (_a_bas.a);
		return 0;
}

int ScanEan128Ai31 (Text& Ean128)
{
        double gew = 0.0;

		if (ScanValues == NULL) return -1;

		if (ScanValues->gewflag) return 0;

		Text *Gew = Scanean.GetEan128Ai (Text ("3103"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ()) / 1000.0;
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		Gew = Scanean.GetEan128Ai (Text ("3102"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ()) / 100.0;
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		Gew = Scanean.GetEan128Ai (Text ("3101"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ()) / 10.0;
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

// St�ck ohne Nachkomma wird im Moment nicht benutzt

		Gew = Scanean.GetEan128Ai (Text ("30"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ());
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		Gew = Scanean.GetEan128Ai (Text ("37"), Ean128);
		if (Gew != NULL)
		{

				gew = ratod (Gew->GetBuffer ());
				delete Gew;
				ScanValues->SetGew (gew);
				return 0;
		}

		return 100;
}



int ScanEan128Ai30 (Text& Ean128)
{

		if (ScanValues == NULL) return -1;

		if (ScanValues->anzflag) return 0;


		Text *Anz = Scanean.GetEan128Ai (Text ("30"), Ean128);
		if (Anz != NULL)
		{

				double anz = ratod (Anz->GetBuffer ());
				delete Anz;
				ScanValues->SetAnz (anz);
				return 0;
		}

		return 100;
}



int ScanEan128Ai15 (Text& Ean128)
{

		if (ScanValues == NULL) return -1;

		if (ScanValues->MhdFlag) return 0;
		Text Mhd = "";
		Text *mhd = Scanean.GetEan128Ai (Text ("15"), Ean128);
		if (mhd == NULL)
		{
			return 100;
		}
		Mhd = *mhd;
		delete mhd;
		ScanValues->SetMhd (Mhd);

        return 0;
}


int ScanEan128Ai10 (Text& Ean128)
{

		if (ScanValues == NULL) return -1;
		if (ScanValues->ChargeFlag) return 0;
		Text Charge = "";
		if (_a_bas.charg_hand == 0) 
		{
			ScanValues->SetCharge (Charge);
			return 0;
		}
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

		if (charge != NULL)
		{
				Charge = *charge;
				delete charge;
				ScanValues->SetCharge (Charge);
				return 0;
		}
        return 0;
}


int ScanEan128MultiAi (Text& Ean128)
{
	int dret;
	dret = ScanEan128Ai01 (Ean128);
	if (dret == 0)  //020712  Nur wenn Artikel erkannt wurde weitermachen !!!
	{
		ScanEan128Ai31 (Ean128);
		ScanEan128Ai10 (Ean128);
		ScanEan128Ai30 (Ean128);  //Anzahl
	    if (mhd_kz == 2) //040811
		{
			ScanEan128Ai15 (Ean128);  
		}
	}
    return 0; 
}

int ScanEan128Ai (Text& Ean128)
{
        if (ScanMulti)
		{
			return ScanEan128MultiAi (Ean128);
		}

//    	ReadNve *readNve;
//		Text nve;

/* Teil f�r allgemeine Ean128verarbeitung */

//	    char buffer [256]; 
        double ean1 = 0.0;
		double Gew;

		Text *ean = Scanean.GetEan128Ai (Text ("01"), Ean128);
		if (ean == NULL)
		{

		        disp_messG ("Fehler bei Ean128 : Eancode", 2);
				return 100;
		}

		Text Ean = ean->SubString (1, Ean128Eanlen);
		delete ean;
		double a = ratod (Ean.GetBuffer ());
		ean1 = a;
		if (artikelOK (a, TRUE) == FALSE)
		{
			return 100;
		}

		sprintf (aufps.a, "%.0lf", _a_bas.a);
		BreakScan = FALSE;

		Gew = 0.0;
		if (ScannMeZwang != 1)
		{
			if (_a_bas.hnd_gew[0] == 'G')
			{
				Text *gew = Scanean.GetEan128Ai (Text ("3103"), Ean128);
				if (gew == NULL)
				{
					if (ScannMeZwang == 2)
					{
						disp_messG ("Fehler bei Ean128 : keine Gewichtauszeichnung", 2);
						return 100;
					}
				}
				else
				{
					Gew = ratod (gew->GetBuffer ());
					delete gew;
				}
			}
			else
			{
				Text *gew = Scanean.GetEan128Ai (Text ("30"), Ean128);
				if (gew == NULL)
				{
					gew = Scanean.GetEan128Ai (Text ("37"), Ean128);
				}
				if (gew == NULL)
				{
					if (ScannMeZwang == 2)
					{
						disp_messG ("Fehler bei Ean128 : keine St�ckauszeichnung", 2);
						return 100;
					}
				}
				else
				{
					Gew = ratod (gew->GetBuffer ()) * 1000.0;
					delete gew;
				}
			}
		}
		Text Mhd = "";
		Text *mhd = Scanean.GetEan128Ai (Text ("15"), Ean128);
		if (mhd == NULL) 
		if (mhd != NULL)
		{
				Text Mhd = *mhd;
				delete mhd;
		}
		Ean128Date = Mhd;
		strcpy (aufps.hbk_date, Ean128Date.ToString ().GetBuffer ());
		Text Charge = "";
		ScannedCharge = "";
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

		if (ChargeZwang && _a_bas.charg_hand == 1 && charge == NULL)
		{
		        disp_messG ("Fehler bei Ean128 : Charge", 2);
				return 100;
		}

		if (charge != NULL)
		{
				Charge = *charge;
				ScannedCharge = *charge;
				strcpy (aufps.ls_charge, Charge.GetBuffer ());
		}
		else
		{
				strcpy (aufps.ls_charge, "");
		}
		sprintf (aufps.a, "%.0lf", _a_bas.a);
		sprintf (aufps.s, "%hd", 2);
		scangew = (long) (Gew);
		sprintf (aufps.me_einh, "%hd", 2);
		memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
		if (ptab_class.lese_ptab ("me_einh", aufps.me_einh) == 0)
		{
                  strcpy (aufps.lief_me_bz, ptabn.ptbezk);
		}
        return 0;
}


int ScanEan128Charge (Text& Ean128)
{
		Text Charge = "";
		Text *charge = Scanean.GetEan128Ai (Text ("10"), Ean128);

/*
		if (ChargeZwang && _a_bas.charg_hand == 1 && charge == NULL)
		{
		        disp_messG ("Fehler bei Ean128 : Charge", 2);
				return 100;
		}
*/

		if (charge != NULL)
		{
				Charge = *charge;
				if (Charge.GetLength () > 20)
				{
					  Charge = Charge.SubString (0, 20);
				}
				if (WithZerlData && (_a_bas.charg_hand == 9 || _a_bas.zerl_eti == 1))
				{
					strcpy (aufps.ls_ident, Charge.GetBuffer ());
				}
				else
				{
					strcpy (aufps.ls_charge, Charge.GetBuffer ());
				}
		}
		else
		{
				if (WithZerlData && (_a_bas.charg_hand == 9 || _a_bas.zerl_eti == 1))
				{
					strcpy (aufps.ls_ident, "");
				}
				else
				{
					strcpy (aufps.ls_charge, "");
				}
		}
		sprintf (aufps.a, "%.0lf", _a_bas.a);
		sprintf (aufps.s, "%hd", 2);
        return 0;
}


int ScanEan128Nve (Text& Ean128)
{
    	ReadNve *readNve;
		Text nve;


		beginwork ();
		nve = Scanean.GetEan128Varpart (Ean128);
		if (nve == NULL)
		{
			return 100;
		}

		WriteNveDebug ("NVE %s eingescannt\n", nve.GetBuffer ());
        readNve = new ReadNve (nve, lsk.mdn, lsk.fil, lsk.ls);
        int dsqlstatus = readNve->read ();
		WriteNveDebug ("Sqlstatus nach dem lesen der NVE %d\n"
			           "Artikelnummer %.0lf posi %ld, Menge %.3lf\n", 
					   dsqlstatus,lsnve.a, lsnve.nve_posi, lsnve.me);
		if (dsqlstatus == 100)
		{
			 double me = 0.0;
			 dsqlstatus = readNve->readPalette (&me);
			 if (dsqlstatus == 0)
			 {
				 strcpy (lsnve.nve, lsnve.nve_palette);
				 lsnve.me = me;
                 lsnve.palette_kz = 1;
				 lsnve.gue = 1;
				 readNve->nve_karton = FALSE;
				 readNve->InsertNve ();
                 int dsqlstatus = readNve->read ();
			 }
		}
		if (dsqlstatus == 100)
		{
			        BOOL ret = abfragejnG (eWindow, "NVE nicht gefunden\nNVE-Werte eingeben ?", "N");
//			        disp_messG ("NVE nicht gefunden", 2);
					if (ret)
					{
						lsnve.a = 0.0;
						lsnve.me = 0.0;
						ret = FillReadNve (readNve);
					}
					if (ret == 0)
					{
						rollbackwork ();
           				delete readNve;
           				return 100;
					}
				    dsqlstatus = 0;
		}

		if (dsqlstatus == -2)
		{
		            WriteNveDebug ("NVE %s wurde schon verarbeitet, kann nicht storniert werden\n", lsnve.nve);
			        disp_messG ("NVE wurde schon verarbeitet", 2);
           		    delete readNve;
					rollbackwork ();
           			return 100;
		}

		if (dsqlstatus == -3)
		{
		            WriteNveDebug ("Kein Gewicht f�r NVE %s\n", lsnve.nve);
			        disp_messG ("Kein Gewicht in NVE", 2);
           		    delete readNve;
					rollbackwork ();
           			return 100;
		}

		if (readNve->IsKarton ())
		{
			TestNveEtikett ();
		}
		else if (nveGew != NULL)
		{
	         disp_messG ("Begonnene Palette ist noch nicht abgeschlossen", 2);
           	 delete readNve;
 			 rollbackwork ();
           	 return 100;
		}

		if (nveGew != NULL && dsqlstatus > -1)
		{
			while (TRUE)
			{
			 Karton *karton = (Karton *) nveGew->GetKarton (0);
			 if (karton != NULL && karton->GetA () != lsnve.a)
			 {
		            WriteNveDebug ("Artikelnummer falsch.\n"
					               "Aktuelle Artikelnummer %.0lf NVE-Artikelnummer %.0lf\n",
                                    karton->GetA (), lsnve.a);
					Text Msg;
					Msg.Format ("Artikelnummer nicht korrekt !\n"
					            "Aktuelle Artikelnummer %.0lf NVE-Artikelnummer %.0lf\n"
								"Etikett f�r Artikel %.0lf ausdrucken ?",
                                karton->GetA (), lsnve.a, karton->GetA ());

 	                if (abfragejnG (eWindow, Msg.GetBuffer(), "N") == 0)
					{
						BOOL ret = abfragejnG (eWindow, "NVE korrigieren ?\n ?", "N");
						if (ret)
						{
						    lsnve.a = karton->GetA ();
							ret = FillReadNve (readNve);
						}
						if (ret == 0)
						{
	           				delete readNve;
							rollbackwork ();
 							nveGew->SubGew (lsnve.me);
							nveGew->DropLastKarton ();
           					return 100;
						}
						dsqlstatus = 0;
 		                nveGew->SubGew (lsnve.me);
						nveGew->DropLastKarton ();
						if (readNve->IsKarton ())
						{
							 TestNveEtikett ();
						}
						break;
					}
					else
					{
						delete readNve;
 		                nveGew->SubGew (lsnve.me);
						nveGew->DropLastKarton ();
						NveEtikett ();
						delete nveGew;
						nveGew = NULL;
						readNve = new ReadNve (nve, lsk.mdn, lsk.fil, lsk.ls);
						int dsqlstatus = readNve->read ();
						lsnve.gue = 1;
						if (readNve->IsKarton ())
						{
							 TestNveEtikett ();
						}
					}
			 }
			 else
			 {
				 break;
			 }
			}
		}

        if (artikelOK (lsnve.a, TRUE) == FALSE)
		{
		    WriteNveDebug ("Artikel %.0lf in a_bas nicht gefunden\n", lsnve.a);
   		    delete readNve;
			if (dsqlstatus > -1)
			{
				nveGew->SubGew (lsnve.me);
				nveGew->DropLastKarton ();
			}
			rollbackwork ();
			return 100;
		}

	    int idx = TestNewArt (lsnve.a, -1);
// 	    int idx = GetListPos ();
        if (idx > -1)
		{
      		memcpy (&aufps, &aufparr[idx], sizeof (aufps));
		}
		if (dsqlstatus == -1)
		{
		    WriteNveDebug ("NVE %s wurde schon verarbeitet, kann storniert werden\n", lsnve.nve);
			if (StornoNve (readNve) == FALSE)
			{
			        disp_messG ("NVE wurde schon verarbeitet", 2);
           		    delete readNve;
					rollbackwork ();
           			return 100;
			}
   		    delete readNve;
			if (nveGew != NULL)
			{
				if (nveGew->size () > 0)
				{
					Karton *karton = (Karton *) nveGew->GetKarton (nveGew->size () - 1);
					nveGew->SubGew (lsnve.me);
				}
				nveGew->DropLastKarton ();
			}
			return 0;
		}

		if (aufps.nve_posi == 0l)
		{
		    aufps.nve_posi = readNve->GenNvePosi ();
		}
        readNve->SetNvePosi (aufps.nve_posi); 
        sprintf (aufps.a, "%.0lf", lsnve.a);
        sprintf (aufps.s, "%hd", 2);
		if (!readNve->IsKarton ())
		{
			strcpy (aufps.s, "3"); 
			memcpy (&aufparr [idx], &aufps, sizeof (struct AUFP_S));
			ShowNewElist ((char *) aufparr,
                       aufpanz,
                      (int) sizeof (struct AUFP_S));
		}
	    delete readNve;
		scangew = (long) (lsnve.me * 1000);
//        sprintf (aufps.lief_me, "%.3lf", ratod (aufps.lief_me) + lsnve.me);
        sprintf (aufps.me_einh, "%hd", lsnve.me_einh);
        memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
        if (ptab_class.lese_ptab ("me_einh", aufps.me_einh) == 0)
        {
                   strcpy (aufps.lief_me_bz, ptabn.ptbezk);
        }

/*
		if (lsnve.palette_kz == 0)
		{
		         sprintf (aufps.me_einh_kun, "%hd", KARTON);
		}
		else
		{
		         sprintf (aufps.me_einh_kun, "%hd", PALETTE);
		}

        memcpy (&ptabn, &ptabn_null, sizeof (struct PTABN));
        if (ptab_class.lese_ptab ("me_einh_kun", aufps.me_einh_kun) == 0)
        {
                   strcpy (aufps.auf_me_bz, ptabn.ptbezk);
        }
*/

		commitwork ();
        return dsqlstatus;
}


int ScanEan128 (Text& Ean128)
{
	if (Text (Ean128.SubString (0, 2)) == "00")
	{
		return ScanEan128Nve (Ean128);
	}
	return ScanEan128Ai (Ean128);
}

void WriteNveDebug (char *format, ...)
{
          va_list args;

		  if (NveDebug == FALSE)
		  {
			  return;
		  }
		  FDebug = fopen (DebugFile, "a");
		  if (FDebug == NULL) return;
          va_start (args, format);
          vfprintf (FDebug, format, args);
          va_end (args);
		  fclose (FDebug);
}

int FillReadNve (ReadNve *readNve)
{
	   char sart [15];
	   char sme [15];
	   while (TRUE && lsnve.a == 0.0)
	   {
			EnterNumBox (eWindow,"  ArtikelNummer  ",
                            sart, 14, "%13.0f");
			if (syskey == KEY5)
			{
				return 0;
			}
		    if (lese_a_bas (ratod (sart)) == 0) 
			{
  				lsnve.a = ratod (sart);
				break;
			}
	   }
	   if (_a_bas.me_einh == 2)
	   {
	        sprintf (sme, "%.3lf", lsnve.me);
			EnterNumBox (eWindow,"  Gewicht  ",
                          sme, 13, "%12.3f");
	   }
	   else
	   {
	        sprintf (sme, "%.0lf", lsnve.me);
			EnterNumBox (eWindow,"  Menge  ",
                          sme, 13, "%12.0f");
	   }
	   if (syskey == KEY5)
	   {
				return 0;
	   }
	   lsnve.me = ratod (sme);
	   lsnve.me_einh = _a_bas.me_einh;
       lsnve.palette_kz = 0;
	   lsnve.gue = 0;
	   strcpy (lsnve.nve_palette, "000000000");
	   readNve->nve_palette = lsnve.nve_palette;
	   readNve->nve_karton = TRUE;
	   readNve->nve_posi = 0l;
	   readNve->mhd = lsnve.mhd;
	   readNve->InsertNve ();
	   lsnve.gue = 1;
	   return 1;
}

void TestNveEtikett ()
{
	    if (nveGew == NULL)
		{
			nveGew = new NveGew ();
			if (nveGew == NULL)
			{
				return;
			}
		}

		if (lsnve.gue <= 0)
		{
			return;
		}

		nveGew->AddGew (lsnve.me);
//	    if (nveGew->size () == 0 && nveGew->GetGew () > 0.0)
	    if ((Hilfe.text1 != nveEtikett) && (nveGew->GetGew () > 0.0))
		{
			    Hilfe.text1 = nveEtikett;
                display_field (BackWindow3, &MainButton2.mask[0], 0, 0);
		}
		Karton *karton = new Karton ();
		karton->SetMdn (lsnve.mdn);
		karton->SetFil (lsnve.fil);
		karton->SetLs (lsnve.ls);
		karton->SetA (lsnve.a);
		karton->SetNve (lsnve.nve);
		karton->SetNvePalette (lsnve.nve_palette);
		karton->SetGew (lsnve.me);
		nveGew->AddKarton (karton);
		nveGew->a = lsnve.a;
}

int NveEtikett (void)
/**
NVE-Etikett ausdrucken.
**/
{
	    char progname [512];
		char *etc;
		int etianz;
		char *tmp;
	    char dname [512];


		if (nveGew == NULL)
		{
			return -1;
		}

		tmp = getenv ("TMPPATH");
		if (tmp == NULL) tmp = "c:\\user\\fit\\tmp";
		sprintf (dname, "%s\\parafile", tmp);
		etc = getenv ("BWSETC");
		if (etc == NULL) etc = "c:\\user\\fit\\etc";


	    int pos = TestNewArt (nveGew->a, -1);
		if (pos == -1)
		{
				pos = GetListPos ();
		}
		else
		{
			memcpy (&aufps, &aufparr [pos], sizeof (struct AUFP_S));
			SetListPos (pos);
		}
		WriteNveParafile (tmp);

		etianz = 1;


        sprintf (progname, "%s\\BIN\\rswrun put_eti %s %s\\eti%04d.prm %s\\parafile %d",
                                     rosipath, krz_txt, etc, eti_nr, tmp, etianz);


        sprintf (EtiPath, "%s\\%s", etc, eti_nve);

        if (AktivWindow == NULL)
        {
                 AktivWindow = GetActiveWindow ();
        }

		CreateMessageWait (AktivWindow, "Das Etikett wird gedruckt\n"
			                        "Bitte warten"); 
         int ret = 0; // todo PutEti (eti_ki_drk, EtiPath, dname, etianz, eti_nve_typ) ;
		DestroyMessageWait ();

        if (ret == -1)
        {
//            disp_messG ("Etikett kann nicht gedruckt werden", 2);
//			return 0;
        }


		if (WriteNewNve (tmp))
		{
			strcpy (aufps.s, "3"); 
			memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
			arrtoretp (pos);
			beginwork ();
            Retp.dbupdate ();
			commitwork ();
		}
		ShowNewElist ((char *) aufparr,
                       aufpanz,
                      (int) sizeof (struct AUFP_S));
        return 0;
}

int TestNewArt (double a, long posi)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
	     int i;

	     for (i = 0; i < aufpanz; i ++)
		 {
			 if (a == ratod (aufparr[i].a) && posi == -1)
			 {
				 break;
			 }
			 else if (a == ratod (aufparr[i].a) &&
				      posi == atol (aufparr[i].pnr) )
			 {
					  break;
			 }
		 }
		 if (i == aufpanz) return -1;
		 return (i);
}

BOOL StornoNve (ReadNve *readNve)
{
	    int i;
/*
	    if (nveGew == NULL)
		{
			     return FALSE;
		}
*/

		/** todo
		if (readNve->IsKarton () == FALSE)
		{
		         BOOL ret = StornoLsNve (readNve);
				 return ret;
		}
		***/

		Karton *karton = new Karton ();
		karton->SetMdn (lsnve.mdn);
		karton->SetFil (lsnve.fil);
		karton->SetLs (lsnve.ls);
		karton->SetA (lsnve.a);
		karton->SetNve (lsnve.nve);
		karton->mhd  = lsnve.mhd;
		if (karton->mhd == 0l)
		{
			memcpy (&karton->mhd, LONGNULL, sizeof (LONGNULL));
		}
		karton->SetNvePalette (lsnve.nve_palette);
		for (i = 0;; i ++)
		{
			Karton *vkarton = (Karton *) nveGew->GetKarton (i);
			if (vkarton == NULL)
			{
				BOOL ret = TRUE;// todo StornoLsNve (readNve);
				delete karton;
				return ret;
			}
			if (*karton == *vkarton)
			{
				break;
			}
		}

 	    if (abfragejnG (eWindow, "   NVE storniern ?   ", "N") == 0)
		{
			delete karton;
			syskey = 0;
		    return TRUE;
		}

 	    int idx = GetListPos ();
        if (idx < 0)
		{
			delete karton;
			return FALSE;
		}
		memcpy (&aufps, &aufparr[idx], sizeof (aufps));
        nveGew->DropKarton (i);
		nveGew->SubGew (lsnve.me);
		double lief_me = ratod (aufps.lief_me);
		lief_me -= lsnve.me;
		lief_me = (lief_me < 0.0) ? 0.0 : lief_me;
        sprintf (aufps.lief_me, "%.3lf", lief_me);
        CalculateScaleAmount ();
        Text nve_karton = lsnve.nve; 
        Text nve_palette = lsnve.nve_palette; 
	    WriteNve *writeNve = new WriteNve (nve_karton, nve_palette,
		                                 lsnve.mdn, lsnve.fil, lsnve.ls,
		                                 lsnve.a, lsnve.me,
							             lsnve.me_einh);

        writeNve->mhd = lsnve.mhd;
        writeNve->write (1);
		 
		memcpy (&aufparr[idx], &aufps, sizeof (aufps));
		delete karton;
		delete writeNve;
		disp_messG ("NVE wurde storniert und kann wieder eingescannt werden", 2);
}


void WriteNveParafile (char *tmp)
/**
Parameter-Datei fuer put_eti schreiben.
**/
{
	    char dname [512];
		FILE *pa;
		char NveNr[20] = {"0"};
		Text a_bz1;
		Text a_bz2;

      
        aufidx = GetListPos ();
		sprintf (dname, "%s\\parafile", tmp);
		pa = fopen (dname, "w");
		if (pa == NULL) return;
		double gew = nveGew->GetGew ();
		if (_a_bas.me_einh != 2 && _a_bas.a_gew != 0.0)
		{
			gew *= _a_bas.a_gew;
		}
		if (GenNve)
		{
			GenNveNr (NveNr);
		}

		if (ReadAKunGx (ratod (aufparr[aufidx].a)) == 0)
		{
			a_bz1 = a_kun_gx.a_bz3;
			a_bz2 = a_kun_gx.a_bz4;
		}
		else
		{
			a_bz1 = aufparr[aufidx].a_bz1;
			a_bz2 = aufparr[aufidx].a_bz2;
			if (strcmp (_a_bas.hbk_kz,"T") == 0)
			{
				a_kun_gx.hbk_ztr = _a_bas.hbk_ztr;
			}
			else if (strcmp (_a_bas.hbk_kz,"W") == 0)
			{
				a_kun_gx.hbk_ztr = _a_bas.hbk_ztr * 7;
			}
			else if (strcmp (_a_bas.hbk_kz,"M") == 0)
			{
				a_kun_gx.hbk_ztr = _a_bas.hbk_ztr * 30;
			}
			else
			{
				a_kun_gx.hbk_ztr = _a_bas.hbk_ztr;
			}
		}

		a_bz1.Trim ();
		a_bz2.Trim ();

		fprintf (pa, "$$NVENR;%s\n", NveNr);
		fprintf (pa, "$$KUNNR;%ld\n",       lsk.kun);
		fprintf (pa, "$$BRANCHE;%s\n",      kun.kun_bran2);
		fprintf (pa, "$$LIEFNR;%ld\n",    lsk.ls);
		fprintf (pa, "$$ABEZ1;%s\n",      a_bz1.GetBuffer ());
		fprintf (pa, "$$ABEZ2;%s\n",      a_bz2.GetBuffer ());
		fprintf (pa, "$$ARTNR;%.0lf\n",   ratod (aufparr[aufidx].a));
		fprintf (pa, "$$LIDAT;%s\n",      lieferdatum);
		fprintf (pa, "$$LIZEIT;%s\n",     lieferzeit);
		fprintf (pa, "$$NETTOG;%.3lf\n",  gew);
		fprintf (pa, "$$EAN;%.0lf\n",     a_kun_gx.ean);
		fprintf (pa, "$$EAN1;%.0lf\n",    a_kun_gx.ean1);
		fprintf (pa, "$$EAN2;%.0lf\n",    a_kun_gx.ean2);
		fprintf (pa, "$$EANNVE1;%.0lf\n", a_kun_gx.ean_nve1);
		fprintf (pa, "$$EANNVE2;%.0lf\n", a_kun_gx.ean_nve2);
		fprintf (pa, "$$CAB;%d\n",        a_kun_gx.cab); 
		fprintf (pa, "$$CAB1;%d\n",       a_kun_gx.cab1); 
		fprintf (pa, "$$CAB2;%d\n",       a_kun_gx.cab2); 
		fprintf (pa, "$$CABNVE1;%d\n",    a_kun_gx.cab_nve1); 
		fprintf (pa, "$$CABNVE2;%d\n",    a_kun_gx.cab_nve2);
		
		fprintf (pa, "$$ETITYP;%d\n",     a_kun_gx.eti_typ); 
		fprintf (pa, "$$ETISUM1;%d\n",    a_kun_gx.eti_sum1); 
		fprintf (pa, "$$ETISUM2;%d\n",    a_kun_gx.eti_sum2); 
		fprintf (pa, "$$ETINVE1;%d\n",    a_kun_gx.eti_nve1); 
		fprintf (pa, "$$ETINVE2;%d\n",    a_kun_gx.eti_nve2); 
		fprintf (pa, "$$MHD;%s\n",        aufps.hbk_date);
        if (lsk.kun_fil == 0)
        {
            fprintf (pa, "$$KUNNAME;%s\n",    kun_krz1);
        }
        else
        {
            fprintf (pa, "$$KUNNAME;%s\n",    _adr.adr_krz);
        }
        fclose (pa);

}

BOOL WriteNewNve (char *tmp)
{
	    FILE *fp;
		char dname [512];
		char buffer [512];

 	    int aufpidx = GetListPos ();
		memcpy (&aufps, &aufparr[aufpidx], sizeof (aufps));
		sprintf (dname, "%s\\%s", tmp, NveFile);
		fp = fopen (dname, "r");
		if (fp == NULL)
		{
			disp_messG ("Es kann keine NVE-Nummer generiert werden", 2);
			return FALSE;
		}
		if (fgets (buffer, 511, fp) == NULL)
		{
			disp_messG ("Es kann keine NVE-Nummer generiert werden", 2);
			fclose (fp);
			return FALSE;
		}
		fclose (fp);
		cr_weg (buffer);
		Text Nve = buffer;
		Nve.TrimRight ();
		double gew = nveGew->GetGew ();
    
 	    if (gew == 0.0)
		{
		        return FALSE;
		}
		if (atoi (aufps.me_einh) == 0)
		{
			    strcpy (aufps.me_einh, "2");
		}

	    WriteNve *writeNve = new WriteNve (Nve, Nve,
		                     lsk.mdn, lsk.fil, lsk.ls,
		                     ratod (aufps.a), gew,
							 (short) atoi (aufps.me_einh));
		writeNve->SetMhd (aufps.hbk_date);
	    if (writeNve->mhd == 0l)
		{
			memcpy (&writeNve->mhd, LONGNULL, sizeof (LONGNULL));
		}
	    if (aufps.nve_posi == 0l)
		{
	          aufps.nve_posi = writeNve->GenNvePosi ();
		}
	    else
		{
		      writeNve->SetNvePosi (aufps.nve_posi);
		}

        beginwork ();
	    writeNve->write (0);
	    delete writeNve;
        for (int i = 0; nveGew->GetKarton (i) != NULL; i ++)
		{
			    Karton *karton  = (Karton *) nveGew->GetKarton (i);
         	    ReadNve *readNve = new ReadNve (karton->GetNve (),
		                                        karton->GetMdn (),
										  	    karton->GetFil (), 
												karton->GetLs ());
				readNve->UpdateNve (lsk.ls, Nve, aufps.nve_posi);
				delete readNve;
		}

        commitwork ();
		return TRUE;
}



void CreateMessageWait (HWND hWnd, char *message)
/**
Warnung beim Ueberschreiten der Alarmmenge.
**/
{
        int x, y, cx, cy;
        RECT rect;
        TEXTMETRIC tm;
        char *ltext;
        int anz;
        int i;
        unsigned int len;

        if (MessWindow) return;

		waithWnd = hWnd;
        aktmessform = &msgform;
        OldFocus = GetFocus ();
        anz = wsplit (message, "\n");
        if (anz == 0) return;

        for (i = 0; i < msgform.fieldanz; i ++)
        {
            msgform.mask[i].length = 0;
        }

        msgform.font = &MessFontBlue;
        SetTextMetrics (hWnd, &tm, msgform.font);

		strcpy (msgstring, "");
		strcpy (msgstring1, "");
		strcpy (msgstring2, "");
        strcpy (msgstring, wort[0]);
        len = strlen (msgstring);
        ltext = msgstring;
        msgform.fieldanz = 1;
        if (anz > 1)
        {
            strcpy (msgstring1, wort[1]);
            if (strlen (msgstring1) > len)
            {
                 len = strlen (msgstring1);
                 ltext = msgstring1;
            }
            msgform.mask[1].attribut = DISPLAYONLY;
			msgform.fieldanz ++;
        }
        if (anz > 2)
        {
            strcpy (msgstring2, wort[2]);
            if (strlen (msgstring2) > len)
            {
                 len = strlen (msgstring2);
                 ltext = msgstring2;
            }
            msgform.mask[2].attribut = DISPLAYONLY;
  			msgform.fieldanz ++;
        }


        GetClientRect (hMainWindow, &rect);

        cx = (len + 3) * tm.tmAveCharWidth;

        x = (rect.right - cx) / 2;
        if (x == 0) x = 0;

        cy = tm.tmHeight * msgform.fieldanz;
        y = (rect.bottom - cy) / 2 ;

        msgform.mask[0].pos[0] = tm.tmHeight * msgy;
        msgform.mask[1].pos[0] = (int) (double) (tm.tmHeight * (msgy + 1 * 1.5));
        msgform.mask[2].pos[0] = (int) (double) (tm.tmHeight * (msgy + 2 * 1.5));

        cy =  msgform.mask[msgform.fieldanz - 1].pos[0] +
              2 * tm.tmHeight;

        y = (rect.bottom - cy) / 2 ;
        MessWindow  = CreateWindowEx (
//                              WS_EX_TOPMOST,
                               0,
                              "AufWiegBlue",
                              "Meldung",
                              WS_POPUP | WS_DLGFRAME,
                              x, y,
                              cx, cy,
                              NULL,
                              NULL,
                              hMainInst,
                              NULL);
        if (MessWindow == NULL) return;
        ShowWindow (MessWindow, SW_SHOWNORMAL);
        SetWindowPos (MessWindow, HWND_TOPMOST, 0, 0, 0, 0,
                   SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        UpdateWindow (MessWindow);
        display_form (MessWindow, &msgform, 0, 0);
        set_fkt (BreakOK, 12);
        EnableWindows (hMainWindow, FALSE);
        EnableWindows (hWnd, FALSE);
}


void CalculateScaleAmount ()
{
		double ScaleValue = ratod (aufps.lief_me) * ratod (aufps.auf_pr_vk);
		sprintf (wiegwert, "%.3lf", ScaleValue);
}

void GenNveNr (char *NveNr)
{
	    strcpy (NveNr, "0");
}
int ReadAKunGx (double a)
{
		a_kun_gx = a_kun_gx_null;
	    a_kun_gx.mdn = lsk.mdn;
		a_kun_gx.fil = lsk.fil;
		a_kun_gx.kun = lsk.kun;
		a_kun_gx.a   = a;
		dsqlstatus = AKunGx.dbreadfirst ();
		if (dsqlstatus == 100)
		{
			strcpy (a_kun_gx.kun_bran2, kun.kun_bran2);
			dsqlstatus = AKunGx.dbread_bra ();
		}
		if (dsqlstatus == 100)
		{
//			a_kun_gx.kun = 0l;
		    dsqlstatus = AKunGx.dbread_art ();
/*
			if (dsqlstatus == 0 && (strcmp (clipped (a_kun_gx.kun_bran2), " ") > 0))
			{
				dsqlstatus = 100;
			}
*/
		}
		return dsqlstatus;
}


BOOL IsEmptyScannBuffer (char *buffer)
{
	BOOL ret = FALSE;
	Text ScannBuffer;

	ScannBuffer = buffer;
	ScannBuffer.Trim ();
	if (ScannBuffer.GetLength () == 0)
	{
		if (IgnoreEmptyScann)
		{
			ret = TRUE;
//		    ScanMessage.Error ();
		}
	}
	return ret;
}

BOOL MultiScan ()
{
	char buffer [512];

	ScanValues = new CScanValues;
	ScanValues->auf_me = ratod (aufps.lief_me3); 
	if (ScanValues->auf_me < 0.0) ScanValues->auf_me = 0.0;

    while (!MeOK ())
	{
		while (!ScanValues->ScanOK ())
		{
			strcpy (buffer, "");
			if (SmallScann == FALSE)
			{
		       NuBlock.SetCharChangeProc (ChangeChar);
               NuBlock.EnterNumBox (eWindow,"  Scannen  ",
                                    buffer, -1,
                                    "", EntNuProc);
			}
			else
			{
			 EnterEan (buffer);
			}

			if (syskey == KEY5) return FALSE;
			if (IsEmptyScannBuffer (buffer))
			{
				continue;
			}

            writelog ("buffer nach scannen %s\n", buffer);
			if (strlen (buffer) > 14)
			{
				Text Ean = buffer;
				ScanEan128 (Ean);
			}
			else
			{
				writelog ("kein EAN128\n");
				if (TestArtikel (ratod (buffer)) == FALSE)
				{
					continue;
				}
				ScanValues->SetA (_a_bas.a);
				double gew = (double) ((double) scangew / 1000.0);
				if (ScanTuerke) gew = 10 ;
				if (artikelOK (_a_bas.a, TRUE) == FALSE)
				{
					continue;
				}
				ScanValues->SetGew (gew);
				break;
			}
		}
		ScanValues->SetACharge ();

		/* 120412 GK Chargenzwang absichern
		 * Ohne Charge? Verlasse ScanMode
		 */
		if ( _a_bas.charg_hand == 1 && ScanValues->ACharge == "" )
		{
           BreakScan = TRUE;
		   TestEnterCharge();
	       if ( BreakScan == TRUE )
		   {
 	           Text Charge = aufps.ls_charge;
	           Charge.Trim ();
		       ScanValues->SetCharge(Charge);
		   }
           writelog ("In MultiScan(): BreakScan=%d aufps.ls_charge=%s ScanCharge=%s\n",
			   BreakScan, aufps.ls_charge, ScanValues->Charge.GetBuffer() );

        }
		
		if (TestScanArticleExist ())
		{
			CompareCharge ();
		}
		ScanValues->IncAufMe ();
		ScanMessage.OK ();
		if (_a_bas.me_einh != 2 && _a_bas.me_einh != 3 && _a_bas.me_einh != 4)
		{
			if (_a_bas.a_gew != 0)
			{
				ScanValues->eangew /= _a_bas.a_gew;
				ScanValues->eangew += 0.499; // ab der 1. Dezimalstelle auf ganze Zahlen aufrunden
			}
			sprintf (aufps.lief_me, "%.0lf", ratod (aufps.lief_me) + ScanValues->eangew);
		}
		else
		{
			sprintf (aufps.lief_me, "%.3lf", ratod (aufps.lief_me) + ScanValues->eangew);
		}

		if (mhd_kz == 2)
		{
			Ean128Date = ScanValues->Mhd;
			strcpy (aufps.hbk_date, Ean128Date.ToString ().GetBuffer ());
		}
		sprintf (aufps.anz_einh, "%ld", atoi(aufps.anz_einh) + ScanValues->anz);  
		sprintf (aufps.lief_me3,"%.3lf", ScanValues->auf_me);
		write_wawiepro (ScanValues->eangew, ScanValues->eangew, 0.0,ScanValues->anz, "B");
		ScanValues->Init ();
	    strcpy (aufps.s, "3");
        memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
		arrtoretp (aufpidx);
		Retp.dbupdate ();
        ShowNewElist ((char *) aufparr,
                               aufpanz,
                               (int) sizeof (struct AUFP_S));
        memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));
/*
		if (AutoEti == 1)
		{
			PrintEti ();
		}
*/
		if (ScanTuerke) BreakScan = 0 ;
		if ( BreakScan ) break;		/* 120412 GK Chargenzwang absichern */
	}
	ScanMessage.Complete ();

    delete ScanValues;
	ScanValues = NULL;
	return TRUE;
}


BOOL ScanMultiA ()
{
//        char buffer [512];

//        struct AUFP_S aufps_save, aufps_save0;
//        char where [512];
        int idxsave;

        beginwork ();

        aufpidx = GetListPos ();


//        memcpy (&aufps_save, &aufps,sizeof (struct AUFP_S));
//        idxsave = aufpidx;
//        aufpidx = 0;

//        memcpy (&aufps_save0, &aufparr[aufpidx],sizeof (struct AUFP_S));

        if (Lock_Lsp () == FALSE)
        {
            commitwork ();
            writelog ("In ScanMultiA() nach 1. commitwork(): BreakScan=%d Charge=%s\n",
				BreakScan, lsp.ls_charge );
            return FALSE;
        }

//        memcpy (&aufparr[idxsave], &aufps, sizeof (struct AUFP_S));
        strcpy (auf_me, aufps.rest);
        akt_buch_me = ratod (aufps.lief_me);
        memcpy (&aufps, &aufparr[aufpidx], sizeof (struct AUFP_S));

		if (!MultiScan ()) 
        {
            commitwork ();
            writelog ("In ScanMultiA() nach 2. commitwork(): BreakScan=%d Charge=%s\n",
				BreakScan, lsp.ls_charge );
            return FALSE;
        }

        aufpidx = GetListPos ();
        memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
        arrtoretp (aufpidx);
		Retp.dbupdate ();
        commitwork ();
        writelog ("In ScanMultiA() nach 3. commitwork(): aufps.ls_charge=%s lsp.ls_charge=%s\n",
				 aufps.ls_charge, lsp.ls_charge );
        aufpidx = idxsave;
//        memcpy (&aufps, &aufps_save, sizeof (struct AUFP_S));
//        memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
        ShowNewElist ((char *) aufparr,
                               aufpanz,
                               (int) sizeof (struct AUFP_S));
/*
		if (AutoEti == 2)
		{
			PrintEti ();
		}
*/
        writelog ("In ScanMultiA() am Ende: BreakScan=%d\n", BreakScan );
        return TRUE;
}


int ScanA (void)
/**
Eine Zeile in Liste einfuegen.
**/
{
//	     KEINHEIT keinheit;
         struct AUFP_S aufp;
         char buffer [256];
//         char me_buffer [20];
         int pos, art_pos;
         long akt_posi;
//		 double auf_me auf_me_vgl;

		 if (ScanMulti)
		 {
			 return ScanMultiA ();
		 }

         scangew = 0l;
         GenPosi = TRUE;
         ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
         ActivateColButton (BackWindow3, &MainButton2, 0, -1, 1);
         memcpy (&aufp, &aufps, sizeof (struct AUFP_S));
         memcpy (&aufps, &aufps_null, sizeof (struct AUFP_S));

		 if (a_sort) NewPosi ();
         while (TRUE)
         {
       		       EnableWindow (InsWindow, FALSE);
//       		       EnableWindow (BackWindow3, FALSE);

                   SetChoise (dochoise, 0);
		           if (SmallScann)
				   {
 			             EnterEan (buffer);
				   }
				   else if (Scanlen != 14)
				   {
                         strcpy (buffer, "");
					     NuBlock.SetCharChangeProc (ChangeChar);
//                         EnterNumBox (eWindow,
//                                      "  Artikel  ", buffer, Scanlen, "");
                         NuBlock.EnterNumBox (eWindow,"  Artikel  ",
                                                        buffer, -1,
                                                        "", EntNuProc);
				   }
				   else
				   {
                         strcpy (buffer, "0");
                         EnterNumBox (eWindow,
                                      "  Artikel  ", buffer, 14,
                                                    "%13.0f");
				   }
//				   EnableWindow (BackWindow3, TRUE);
                   InvalidateRect (eWindow, NULL, TRUE);
                   UpdateWindow (eWindow);
                   memcpy (&aufps, &aufps_null, sizeof (struct AUFP_S));

                   if ((double) ratod (buffer) <= (double) 0.0)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
							  GenPosi = FALSE;
                              return 0;
                   }
                   if (syskey == KEY5)
                   {
                              memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                              DeleteInsert ();
							  GenPosi = FALSE;
                              return 0;
                   }
                   OpenInsert ();
				   clipped (buffer);
				   if (strlen (buffer) >=16)
				   {
					          Text Ean = buffer;
							  int ret = ScanEan128 (Ean);
							  if (ret == 0)
							  {
								  break;
							  }
				   }
                   else if (artikelOK ((double) ratod (buffer), TRUE))
                   {
                              break;
                   }
                   memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
                   DeleteInsert ();
                   GenPosi = FALSE;
                   ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
				   ActivateColButton (BackWindow3, &MainButton2, 0, 0, 1);
				   GenPosi = FALSE;
                   return 0;
         }

		 art_pos = TestNewArt (ratod (aufps.a), -1);
		 sprintf (aufps.lief_me, "%.3lf", 0.0);
		 sprintf (aufps.auf_me, "%.3lf", 0.0);
		 sprintf (aufps.auf_me_vgl, "%.3lf", 0.0);
// Positiontexte f�r Lieferschein - und Rechnungsdruck sichtbar machen
	 // z.Z. nicht bei Retouren	 aufps.pos_txt_kz = (short) LiefRechVisible;

         if (scangew != 0l)
         {
            double brutto = (double) scangew / 1000;
			beginwork ();
			write_wawiepro (brutto, brutto, 0.0, 0,  "B"); //Anzahl bisher nur bei MultiScan!! 
			commitwork ();
            sprintf (aufps.lief_me, "%.3lf", brutto);
         }

         CalculateScaleAmount ();

         strcpy (buffer, "0");

        einh_class.SetAufEinh (1);
        GetMeEinh ();

         if (art_pos != -1 && (a_kum_par || ScanMode))
		 {
			     KumAufPos (art_pos);
		 }


		 if (art_pos == -1 || (a_kum_par == FALSE && ScanMode == FALSE))
		 {
                  current_form = &testform;
                  DeleteInsert ();
                  InvalidateRect (eWindow, NULL, TRUE);
                  UpdateWindow (eWindow);
                  pos = GetListPos ();
				  if (aufpanz) pos ++;
                  akt_posi = GetNextPosi (pos);
                  sprintf (aufps.pnr, "%ld", akt_posi);
                  ScrollAufp (pos);
                  memcpy (&aufp, &aufps, sizeof (struct AUFP_S));
                  sprintf (aufps.lief_me1, "%.3lf", (double) 0);
                  sprintf (aufps.lief_me2, "%.3lf", (double) 0);
                  sprintf (aufps.lief_me3, "%.3lf", (double) 0);
				  sprintf (aufps.ls_pos_kz, "%hd", 2);
				 // aufps.me_ist = 0.0;
				  //GetMeEinhIst ();
                  memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
                  arrtoretp (pos);
                  beginwork ();
                  if (lsp.lief_me > MAXME || lsp.lief_me3 > MAXME)
                  {
                                   print_messG (2, "Wert %3lf ist zu gro�", lsp.lief_me);
                  }
                  else
                  {
						Retp.dbupdate ();
                  }
                  commitwork ();
                  LeseRetp ();
//		          PostMessage (eWindow, WM_KEYDOWN, (WPARAM) VK_DOWN, 0l);
				  if (a_sort)
				  {
                           memcpy (&aufps, &aufp, sizeof (struct AUFP_S));
		                   pos = TestNewArt (ratod (aufps.a), akt_posi);
				  }
                  ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
		          ListMessage (WM_KEYDOWN, (WPARAM) VK_DOWN, 0l);
				  aufpidx = GetListPos ();
				  InvalidateRect (Getlbox (), NULL, TRUE);
				  UpdateWindow  (Getlbox ());
		 }
		 else
		 {
			      pos = art_pos;
				  if (pos < 0) pos = aufpanz;
			      SetListPos (art_pos);
                  if (scangew != 0l)
                  {
                        sprintf (aufparr [art_pos].lief_me, "%.3lf",
                            ratod (aufparr [art_pos].lief_me) +
                            (double) scangew / 1000);
						if (!Ean128Date.IsEmpty ())
						{
							strcpy (aufparr[art_pos].hbk_date, Ean128Date.ToString ().GetBuffer ());
							Ean128Date.Init ();
						}
                  }
				  if (ScannedCharge != "")
				  {
					    strcpy (aufparr[art_pos].ls_charge, ScannedCharge.GetBuffer ());
				  }
				  memcpy (&aufps, &aufparr [art_pos], sizeof (AUFP_S));
                  current_form = &testform;
                  DeleteInsert ();
				  if (auf_me_kz || scangew != 0l)
				  {
                          arrtoretp (art_pos);
                          beginwork ();
                          if (lsp.lief_me > MAXME || lsp.lief_me3 > MAXME)
                          {
                                   print_messG (2, "Wert %3lf ist zu gro�", lsp.lief_me);
                          }
                          else
                          {
								Retp.dbupdate ();
                          }
                          commitwork ();
				  }
                  ShowNewElist ((char *) aufparr,
                                aufpanz,
                                (int) sizeof (struct AUFP_S));
         }

         GenPosi = FALSE;
         ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
	     ActivateColButton (BackWindow3, &MainButton2, 0, 0, 1);

		 if (wieg_neu_direct && scangew == 0l)
		 {
                      IsDblClck (pos);
		 }

         return 0;
}


int DoScann ()
/**
Scannen f�r Artikel starten.
**/
{
        unsigned int scancount = 0; 

		if (ScanMode)
		{
			return 0;
		}

        ScanMode = TRUE;
		for (int i = 0; i < lFussform.fieldanz; i ++)
		{
                 ActivateColButton (eFussWindow, &lFussform, i, -1, 0);
        }
		for (i = 0; i < MainButton.fieldanz; i ++)
		{
                 ActivateColButton (BackWindow2, &MainButton, i, -1, 1);
		}
        EnableWindow (BackWindow2, FALSE);
        if (eWindow != NULL)
        {
            EnableWindow (eWindow, FALSE);
        }

       ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
        BreakScan = FALSE;
        syskey = 0;
        while (TRUE)
        {
               BOOL ret = ScanA ();
			   if (ret)
               {
                   scancount ++;
               }
               if (syskey == KEY5 || BreakScan)
               {
                   break;
               }
/*
               if (ret && eti_ez != NULL)
               {
                    PrintEti ();
               }
*/
/*
		       if (autocursor && scancount >= (unsigned int) lsp.auf_me)
               {
                   if (aufidx < (int) GetListAnz () - 1)
                   {
                         scancount = 0;
                         SetListPos (GetListPos () + 1);
                         InvalidateRect (eWindow, NULL, FALSE);
                   }
                   else
                   {
                         break;
                   }
               }
*/
        }
        syskey = 0; 
		for (i = 0; i < lFussform.fieldanz; i ++)
		{
                 ActivateColButton (eFussWindow, &lFussform, i, 0, 0);
        }
		CheckAusz ();
		for (i = 0; i < MainButton.fieldanz; i ++)
		{
                 ActivateColButton (BackWindow2, &MainButton, i, 1, 1);
		}
        ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
        ActivateColButton (BackWindow3, &MainButton2, 0, 0, 1);
        EnableWindow (BackWindow2, TRUE);
        if (eWindow != NULL)
        {
            EnableWindow (eWindow, TRUE);
        }
        ScanMode = FALSE;

		/* 120412 GK Chargenzwang absichern */
        writelog ("In DoScann(): BreakScan=%d\n", BreakScan );

        return 0;
}

BOOL MeOK ()
{
// Vorl�ufig kein Abbruch, wenn die Menge erreicht ist.
	return FALSE;

	if (atoi (aufps.s) > 2) return TRUE;
	if (ratod (aufps.auf_me) == 0) return FALSE;

	if (ScanValues->auf_me >= ratod (aufps.auf_me))
	{
		return TRUE;
	}
	return FALSE;
}

int ChangeChar (int key)
{
	CInteger Key (key);
	Scanean.AiInfos.FirstPosition ();
    AiInfo *ai;
	while ((ai = (AiInfo *) Scanean.AiInfos.GetNext ()) != NULL)
	{
		ai->EndChar.FirstPosition ();
		CInteger *ec;
		while ((ec = (CInteger *) ai->EndChar.GetNext ()) != NULL)
		{
			if (Key == *ec)
			{
				return ai->EndCharEdit;
			}
		}
	}
	return key;
}


// Charge aus eingescanntem oder eingelsenem String holen

void FindCharge (char *buffer)
{
	if (ChargeFromEan128 == 0)
	{
// buffer ist charge
	}
	else if (ChargeFromEan128 == 1)
// buffer ist EAN128 charge unter AI 10
    {
 		Text Ean128 = buffer;
		Text *Ean = Scanean.GetEan128Ai (Text ("01"), Ean128);
        if (Ean == NULL)
		{		
// Keine Ean-Nr vorhanden, buffer ist charge
			return;
		}
		Text *Charge = Scanean.GetEan128Ai (Text ("10"), Ean128);
		if (Charge == NULL)
		{
// Keine Chargen-Nr vorhanden, Fehler
			disp_messG ("Das Etikett hat keine Chargennummer", 2);
			strcpy (buffer, "");
			return;
		}
		strcpy (buffer, Charge->GetBuffer ());
	}
	else if (ChargeFromEan128 == 2)
// Stringl�nge entscheidet
	{
		Text Ean128 = buffer;
		Ean128.Trim ();
		if (Ean128.GetLength () < Ean128StartLen)
		{		
// Keine Ean-Nr vorhanden, buffer ist charge
			return;
		}
		Text *Charge = Scanean.GetEan128Ai (Text ("10"), Ean128);
		if (Charge == NULL)
		{
// Keine Chargen-Nr vorhanden, buffer ist charge
			return;
		}
		strcpy (buffer, Charge->GetBuffer ());
	}
}

void EnterEan (char *buffer)
{

       save_fkt (5);
       save_fkt (12);
       set_fkt (BreakEanEnter, 5);
       set_fkt (BreakEanEnter, 12);
	   
	   strcpy (eanbuffer, "");
	   SetEnterCharChangeProc (ChangeChar);
       enter_scan_form (eFussWindow, &fScanform, 0, 0);
       CloseControls (&fScanform);
	   SetEnterCharChangeProc (NULL);
	   if (syskey != KEY5)
	   {
		   strcpy (buffer, eanbuffer);
	   }
       restore_fkt (5);
       restore_fkt (12);
}

BOOL TestArtikel (double a)
{

        writelog ("TestArtikel\n");
        dsqlstatus = ScanArtikel (a);
        if (dsqlstatus != 0)
        {
					 ScanMessage.Error ();
                     disp_messG ("Falsche Artikelnummer", 2);
                     writelog ("Falsche Artikelnummer\n");
                     return FALSE;
        }
        return TRUE;
}

int BreakEanEnter ()
{
	  break_enter ();
	  return 0;
}

void SetScannFocus ()
{
	  if (fScanform.mask[0].feldid != NULL)
	  {
		  SetFocus (fScanform.mask[0].feldid);
	  }
}

void TestEnterCharge (void)
{
		char buffer [0x1000];
		memset ( buffer, '\0', sizeof buffer ); /* GK 06.07.12 */


	    if (ls_charge_par == 0 || (lsc2_par == 0 && lsc3_par == 0))
		{
			return;
		}


        clipped (aufps.ls_charge);


		if (_a_bas.charg_hand == 1 && (!WithZerlData || _a_bas.zerl_eti != 1))
		{

				if (ChargeZwang == No) return;
				if ( ChargenWechsel == TRUE ) /* Doppelpruefung vermeiden GK 10.07.12 */
				{
					ChargenWechsel = FALSE;
					return;
				}
			    while (TRUE)
				{
				  int len = GetChargeInputLen ();

				  if (_a_bas.charg_hand == 1)
				  {

						  if (AlphaCharge)
						  {
							  if (ChargeZwang == ForceIfEmpty && 
								  !ChargeDefault && strcmp (aufps.ls_charge, " ") > 0)
							  {
								  return;
							  }
							  textblock = new TEXTBLOCK;
							  textblock->SetLongText (FALSE);
							  EnableWindows (hMainWindow, FALSE);
							  EnableWindow (WiegWindow, FALSE);
							  LockWindowUpdate (WiegWindow);
							  textblock->MainInstance (hMainInst, WiegWindow);
							  textblock->ScreenParam (scrfx, scrfy);
							  textblock->SetUpshift (TRUE);
						      strcpy (buffer, aufps.ls_charge);
							  textblock->EnterTextBox (BackWindow1, LChargenNr, buffer, -1, 
								                       "", EntNumProc);
							  LockWindowUpdate (NULL);
							  EnableWindows (hMainWindow, TRUE);
							  EnableWindow (WiegWindow, TRUE);
							  delete textblock;
							  textblock = NULL;
						  }
						  else
						  {
							  if (ChargeFromEan128 == 0)
							  {
								   strcpy (buffer, aufps.ls_charge);
								   if (ChargeZwang == ForceIfEmpty && 
									  !ChargeDefault && strcmp (aufps.ls_charge, " ") > 0)
								   {
										return;
								   }
								   EnterNumBox (WiegWindow, LChargenNr,
			                                    buffer, 14,
				                                "255");
							  }
							  else
							  {
								   clipped (aufps.ls_charge);
								   if (ChargeZwang == ForceIfEmpty && 
									  !ChargeDefault && strcmp (aufps.ls_charge, " ") > 0)
								   {
										return;
								   }
								   strcpy (buffer, "");
								   strcpy (buffer, aufps.ls_charge); // 02.01.2013 GK
								   SetEnterCharChangeProc (ChangeChar);
								   EnterNumBox (WiegWindow, LChargenNr,
			                                    buffer, 14,
				                                "255");
								    SetEnterCharChangeProc (NULL);
/*
									NuBlock.SetCharChangeProc (ChangeChar);
									NuBlock.EnterNumBox (WiegWindow, LChargenNr,
											   buffer, -1, "",
										       EntNuProc);

									NuBlock.SetCharChangeProc (NULL);
*/
									if (syskey == KEY5)
									{
										return;
									}
									FindCharge (buffer);
									if (strlen(clipped(buffer)) < 1)
									{
										syskey = KEY5;
										return;
									}

							  }
						  }
						  clipped (buffer ); /* GK 06.07.12 */
						  if (LChargenNr == TextIdent)
						  {
								strncpy (aufps.ls_ident, buffer, 21);
								aufps.ls_ident[21] = 0;
						  }
						  else
						  {
							    if ( BezChargeLen > 0
									 && strcmp( buffer, aufps.ls_charge ) != 0
									 && aufps.ls_charge[0] != '\0'
									 && aufps.ls_charge[0] != ' '
									 && ratod( aufps.lief_me ) > 0.0 )
								{
									/* Neue Position wegen Chargenwechsel GK 25.06.12 */
								    strncpy (ls_charge_neu, buffer, 30);
								    ls_charge_neu[30] = 0;
									doWOK();
									dochargenwechsel();
	                                /* ChargenWechsel = TRUE; erstmal mit Doppelpruefung GK 10.07.12 */
									// disp_messG ( "Chargenwechsel. Neue LS Position!", 2 );
								}
								else
								{
								strncpy (aufps.ls_charge, buffer, 30);
								aufps.ls_charge[30] = 0;
								    charge2a_bz1( aufps.a_bz1, aufps.ls_charge ); /* GK 25.06.12 */
						  }
				  }
				  }
				  UpdateWindow (WiegWindow);
				  buffer[len] = 0;

				if (strlen(clipped(buffer)) < 1)
				{
					syskey = KEY5;
					return;
				}

				   if (TestChargeLen (buffer))
				   {
							  break;
				   }
				}
                strcpy (last_charge, aufps.ls_charge);
				InvalidateRect (WiegWindow, NULL, TRUE);
        }

		/***
		else if (WithZerlData && (_a_bas.charg_hand == 9 || _a_bas.zerl_eti == 1))
		{

				if (!IdentZwang) return;
				if (strlen(clipped(aufps.ls_ident)) > 0) return;  //wenn Identnummer in der Position schon vorhanden, kein Zwang mehr 
			    IdentTarget = NULL;
				strcpy (Ident_vorher,aufps.ls_ident);
				if (ScannIdent)
				{
					  ScannIdentNumber ();
					  return;
				}
				else if (!AuswahlIdn ())
				{
				 	  EnterZerldaten(buffer);
				}
        }
		****/
}

BOOL TestScanArticleExist ()
{
	int art_pos;
	art_pos = TestNewArt (ScanValues->a, -1);
	if (art_pos == -1)
	{
		aufpidx = aufpanz;
		aufpanz ++;
		sprintf (aufps.auf_me, "%.3lf", 0.0);
		sprintf (aufps.lief_me3, "%.3lf", 0.0);
		sprintf (aufps.lief_me, "%.3lf", 0.0);
		sprintf (aufps.pnr, "%ld", (aufpidx + 1) * 10);
		lsp.posi = (aufpidx + 1) * 10;
		strcpy (aufps.ls_charge, ScanValues->Charge.GetBuffer ());

//		if (mhd_kz == 2)
//		{
//			Ean128Date = ScanValues->Mhd;
//			strcpy (aufps.hbk_date, Ean128Date.ToString ().GetBuffer ());
//		}
//		sprintf (aufps.anz_einh, "%ld", ScanValues->anz);
	    

		memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
		arrtoretp (aufpidx);
		Retp.dbupdate ();
		ScanValues->auf_me = 0.0;
		SetListPos (GetListPos () + 1);
		InvalidateRect (eWindow, NULL, FALSE);
		return FALSE;
	}
	SetListPos (art_pos);
	return TRUE;
}

void CompareCharge ()
{
	int art_pos;

	Text Charge = aufps.ls_charge;
	Charge.Trim ();

	if ( BreakScan == TRUE )
	{
		ScanValues->SetCharge(Charge);
	}

    art_pos = TestNewArtCharge (ScanValues->a, ScanValues->Charge);
    
	writelog ("In CompareCharge(): art_pos=%d ScanValue.Charge=%s aufps.ls_charge=%s lsp.ls_charge=%s\n",
		art_pos, ScanValues->Charge.GetBuffer (), aufps.ls_charge, lsp.ls_charge );

	if (art_pos != -1)
	{
		aufpidx = art_pos;
		memcpy (&aufps, &aufparr[aufpidx],sizeof (struct AUFP_S)); 
		if (ScanValues->Charge.GetBuffer () != "")
		{
			strcpy (aufps.ls_charge, ScanValues->Charge.GetBuffer ());
		}
		SetListPos (aufpidx);
		return;
	}


	double auf_me_ges = ratod (aufps.auf_me);

	aufpidx = aufpanz;
	aufpanz ++;
    strcpy (aufps.s, "2");
	sprintf (aufps.lief_me3, "%.3lf", 0.0);
	sprintf (aufps.lief_me, "%.3lf", 0.0);
	sprintf (aufps.auf_me, "%.3lf", 0.0); // 20120514 GK
	sprintf (aufps.pnr, "%ld", (aufpidx + 1) * 10);
	lsp.posi = (aufpidx + 1) * 10;
	strcpy (aufps.ls_charge, ScanValues->Charge.GetBuffer ());
    memcpy (&aufparr[aufpidx], &aufps, sizeof (struct AUFP_S));
    arrtoretp (aufpidx);
	Retp.dbupdate ();
	ScanValues->auf_me = 0.0;
	SetListPos (GetListPos () + 1);
    InvalidateRect (eWindow, NULL, FALSE);
}

void write_wawiepro (double brutto, double netto, double tara, int Anzahl)
/**
In Tabelle we_wiepro schreiben.
**/
{
         char datum [12];
         char zeit  [12];

         if (netto > MAXME)
         {
             print_messG (2, "Wert %3lf ist zu gro�", netto);
             return;
         }
         sysdate (datum);
         systime (zeit);

		 wiepro.mdn = lsk.mdn;
         wiepro.fil = lsk.fil;
         wiepro.a   = ratod (aufps.a);
		 wiepro.pr  = ratod (aufps.auf_lad_pr);
//         wiepro.best_blg = 0;
         strcpy (wiepro.blg_typ, "L");
         wiepro.nr = lsk.ls;
         wiepro.kun = lsk.kun ;
		 wiepro.gew_bto = brutto;
		 wiepro.gew_nto = brutto - tara;
         wiepro.tara = tara;
         memcpy (wiepro.zeit, zeit, 2);
         memcpy (&wiepro.zeit[2], &zeit[3], 2);
         memcpy (&wiepro.zeit[4], &zeit[6], 2);
		 wiepro.zeit[6] = 0;	 ;
         wiepro.dat = dasc_to_long (datum);
         wiepro.sys = sys_peri.sys;
		 wiepro.kun_fil = lsk.kun_fil;
         strcpy (wiepro.pers, sys_ben.pers_nam);
         strcpy (wiepro.erf_kz, aufps.erf_kz);
         strcpy (wiepro.ls_charge, aufps.ls_charge);
         strcpy (wiepro.ls_ident, aufps.ls_ident);
         wiepro.anz = Anzahl;
		 if (wiepro.anz == 0)
		 {
			 wiepro.anz = 1;
		 }
         wiepro.hbk = dasc_to_long (aufps.hbk_date);
		 WiePro.dbinsert ();
         strcpy (wiepro.peri_nam, "");
}


void write_wawiepro (double brutto, double netto, double tara, int Anzahl, char *erf_kz)
/**
In Tabelle we_wiepro schreiben.
**/
{
         char datum [12];
         char zeit  [12];

         if (netto > MAXME)
         {
             print_messG (2, "Wert %3lf ist zu gro�", netto);
             return;
         }
         sysdate (datum);
         systime (zeit);

		 wiepro.mdn = lsk.mdn;
         wiepro.fil = lsk.fil;
         wiepro.a   = ratod (aufps.a);
		 wiepro.pr  = ratod (aufps.auf_lad_pr);
//         wiepro.best_blg = 0;
         strcpy (wiepro.blg_typ, "L");
         wiepro.nr = lsk.ls;
         wiepro.kun = lsk.kun ;
		 wiepro.gew_bto = brutto;
		 wiepro.gew_nto = brutto - tara;
         wiepro.tara = tara;
         memcpy (wiepro.zeit, zeit, 2);
         memcpy (&wiepro.zeit[2], &zeit[3], 2);
         memcpy (&wiepro.zeit[4], &zeit[6], 2);
		 wiepro.zeit[6] = 0;	 ;
         wiepro.dat = dasc_to_long (datum);
         wiepro.sys = sys_peri.sys;
		 wiepro.kun_fil = lsk.kun_fil;
         strcpy (wiepro.pers, sys_ben.pers_nam);
         strcpy (wiepro.erf_kz, erf_kz);
         strcpy (wiepro.ls_charge, aufps.ls_charge);
         strcpy (wiepro.ls_ident, aufps.ls_ident);
         wiepro.anz = Anzahl;
		 if (wiepro.anz == 0)
		 {
			 wiepro.anz = 1;
		 }
         wiepro.hbk = dasc_to_long (aufps.hbk_date);
		 WiePro.dbinsert ();
         strcpy (wiepro.peri_nam, "");
}

void KumAufPos (int art_pos)
/**
Neuen Artikel mit vorhandener Position mit gleichen Artikel kummulieren.
**/
{
	       double auf_me_alt, auf_me_neu;
	       KEINHEIT keinheit;
		 double auf_me, auf_me_vgl;

		 if (auf_me_kz && ScanMode == FALSE)
		 {
			 auf_me_alt = ratod (aufparr[art_pos].auf_me);
			 auf_me_neu = ratod (aufps.auf_me);
			 sprintf (aufparr[art_pos].auf_me, "%.3lf", auf_me_alt + auf_me_neu);
			 sprintf (aufps.auf_me, "%.3lf", auf_me_alt + auf_me_neu);
             einh_class.GetKunEinh (lsk.mdn, lsk.fil,
                                    lsk.kun, _a_bas.a, &keinheit);
             if (keinheit.me_einh_kun == keinheit.me_einh_bas)
			 {
                    strcpy (aufps.auf_me_vgl, aufps.auf_me);
                    strcpy (aufparr[art_pos].auf_me_vgl, aufps.auf_me);
			 }
             else
			 {
                    if (keinheit.inh <= (double) 0.0)
                    {
                                   keinheit.inh = (double) 1.0;
                    }
                    auf_me = (double) ratod (aufps.auf_me);
                    auf_me_vgl = auf_me * keinheit.inh;
                    sprintf (aufps.auf_me_vgl, "%.3lf", auf_me_vgl);
                    sprintf (aufparr[art_pos].auf_me_vgl, "%.3lf", auf_me_vgl);
			 }
             sprintf (aufparr[art_pos].rest, "%.3lf", ratod (aufparr[art_pos].auf_me_vgl)
				 -
				                                      ratod (aufparr[art_pos].lief_me));
             sprintf (aufps.rest, "%.3lf", ratod (aufps.auf_me_vgl) -
				                           ratod (aufps.lief_me));
			 CalculateScaleAmount ();
		 }
}

int ScanArtikel (double a)
{
        Text Ean;
        Text EanDb;
        Text EanA;
        STRUCTEAN *EanStruct;
		static short cursor_ean = -1;

		writelog ("Artikel in Scanartikel %.0lf\n", a);
        scangew = 0l;
        Ean.Format ("%13.0lf", a);
		if (Ean.GetLength () >= 12)
		{
			EanStruct = Scanean.GetStructean (Ean);
		}
		else
		{
			EanStruct = NULL;
		}
		writelog ("EanStruct %ld\n", EanStruct);
		char *sc;
        if (EanStruct != NULL)
        {
            switch (Scanean.GetType (Ean))
            {
            case EANGEW :
				if (Ean.GetLength () < 13)
				{
					break;
				}
                EanDb = Scanean.GetEanForDb (Ean);
                ean = ratod (EanDb.GetBuffer ());
				sc = Scanean.GetEanGew (Ean);
				if (sc != NULL)
				{
					scangew = atol (Scanean.GetEanGew (Ean));
				}
				else
				{
					EanStruct = NULL;
				}
                break;
            case EANPR :
                EanDb = Scanean.GetEanForDb (Ean);
                ean = ratod (EanDb.GetBuffer ());
                BreakScan = TRUE;
                break;
            default :
                ean = ratod (Ean.GetBuffer ());
                BreakScan = TRUE;
                if (ScanKunEan (ean) == 0)
                {
                    return 0;
                }
            }
        }
        else
        {
                ean = ratod (Ean.GetBuffer ());
				writelog ("ScanKunEan : EAN %.0lf", ean);
                BreakScan = TRUE;
                if (ScanKunEan (ean) == 0)
                {
				    writelog ("ScanKunEan OK gefunden: a %.0lf", _a_bas.a);
                    return 0;
                }
			    writelog ("ScanKunEan OK nicht gefunden");
        }
/*
        DbClass.sqlout ((double *) &a, 3, 0);
        DbClass.sqlin  ((double *) &ean, 3, 0);
        int dsqlstatus = DbClass.sqlcomm ("select a from a_ean where ean = ?");
*/

        if (cursor_ean == -1)
		{
              DbClass.sqlout ((double *) &ean_a, 3, 0);
              DbClass.sqlin  ((double *) &ean, 3, 0);
              cursor_ean = DbClass.sqlcursor ("select a from a_ean where ean = ?");
		}
        writelog ("ScanEan: EAN %.0lf", ean);
		DbClass.sqlopen (cursor_ean);
		dsqlstatus = DbClass.sqlfetch (cursor_ean);

        if (dsqlstatus == 0)
        {
			    writelog ("ScanEan OK gefunden: a %.0lf", _a_bas.a);
                 dsqlstatus = lese_a_bas (ean_a);
        }
        if (dsqlstatus == 0)
        {
                 return dsqlstatus;
        }
        if (EanStruct != NULL && EanStruct->GetAFromEan () == TRUE)
//            ((EanStruct->GetType ().GetBuffer ())[0] == 'G' ||
//            (EanStruct->GetType ().GetBuffer ())[0] != 'P'))
        {
                 EanA = Ean.SubString (2, EanStruct->GetLen ());
                 a = ratod (EanA.GetBuffer ());
                 dsqlstatus = lese_a_bas (a);
        }
        return dsqlstatus;
}

int GetChargeInputLen ()
{
	int len = 30;
    if (MaxChargeLen != 0)
	{
		len = MaxChargeLen;
	}
	else if (ChargeLen > MinChargeLen)
	{
		len = ChargeLen;
	}
	return (len < 30) ? len : 30;
}

int dochargenwechsel (void)
/**
Eine Zeile in Liste einfuegen.
Variante von doinsert0()  25.06.12 GK 
**/
{
	int pos;
	long akt_posi;
	ActivateColButton (BackWindow3, &MainButton2, 3, -1, 1);
	strcpy ( aufps.ls_charge, ls_charge_neu );
	memset( ls_charge_neu, '\0', sizeof ls_charge_neu );
	charge2a_bz1(  aufps.a_bz1, aufps.ls_charge ); /* GK 25.06.12 */
	sprintf (aufps.lief_me, "%.3lf", 0.0);
	sprintf (aufps.auf_me, "%.3lf", 0.0);
	sprintf (aufps.auf_me_vgl, "%.3lf", 0.0);
//	aufps.pos_txt_kz = (short) LiefRechVisible;
	CalculateScaleAmount ();
	current_form = &testform;
	DeleteInsert ();
	InvalidateRect (eWindow, NULL, TRUE);
	UpdateWindow (eWindow);
	pos = GetListPos ();
	if (aufpanz) pos ++;
	akt_posi = GetNextPosi (pos);
	sprintf (aufps.pnr, "%ld", akt_posi);
	ScrollAufp (pos);
	sprintf (aufps.lief_me1, "%.3lf", (double) 0);
	sprintf (aufps.lief_me2, "%.3lf", (double) 0);
	sprintf (aufps.lief_me3, "%.3lf", (double) 0);
	sprintf (aufps.ls_pos_kz, "%hd", 2);
//	aufps.me_ist = 0.0;
//	GetMeEinhIst ();
	memcpy (&aufparr [pos], &aufps, sizeof (struct AUFP_S));
	arrtoretp (pos);
	beginwork ();
	Retp.dbupdate();
	commitwork ();
//	WritePosDebug ("Lieferschein %ld posi %ld Artikel %.0lf Charge %s eingef�gt\n",
//		lsk.ls, lsp.posi, lsp.a, lsp.ls_charge);
	LeseRetp ();
	ShowNewElist ((char *) aufparr,
		aufpanz,
		(int) sizeof (struct AUFP_S));
	ListMessage (WM_KEYDOWN, (WPARAM) VK_DOWN, 0l);
	aufpidx = GetListPos ();
	InvalidateRect (Getlbox (), NULL, TRUE);
	UpdateWindow  (Getlbox ());
	ActivateColButton (BackWindow3, &MainButton2, 3, 0, 1);
	if (wieg_neu_direct && (ScanMode == FALSE || scangew == 0l))
	{
		IsDblClck (pos);
	}
	return 0;
}

void charge2a_bz1(  char *bez, char *charge )
{
/* GK 25.06.12 */
	int i;
	char *p;

	if ( BezChargeLen < 1 ) return;
	if ( BezChargeLen > 17 ) return;
	if ( charge[0] == '\0' ) return;
	if ( charge[0] == ' ' ) return;
	
	p = bez + (26 - BezChargeLen);
	p[0] = '/';
	for ( i = 0; i < BezChargeLen; i++ ) p[i+1] = charge[i];
	p[BezChargeLen+1] = '\0';
}

BOOL TestChargeLen (char *buffer)
{
	Text Buffer;
	Buffer = buffer;
	Buffer.Trim ();
	if (MinChargeLen != 0)
	{
		if (Buffer.GetLength () < MinChargeLen) 
		{
			disp_messG ("Die L�nge der eingegebenen Charge ist zu klein", 2);
			return FALSE;
		}
	}

	if (MaxChargeLen != 0)
	{
		if (Buffer.GetLength () > MaxChargeLen) 
		{
			disp_messG ("Die L�nge der eingegebenen Charge ist zu gro�", 2);
			return FALSE;
		}
	}

	if (MaxChargeLen == 0 && MinChargeLen == 0 && ChargeLen != 0)
	{
		if (Buffer.GetLength () != ChargeLen) 
		{
			disp_messG ("Die L�nge der eingegebenen Charge ist nicht korrekt", 2);
			return FALSE;
		}
	}
	strcpy (aufps.ls_charge, buffer);
	return TRUE;
}

int TestNewArtCharge (double a, Text& ListCharge)
/**
Test, ob der Artikel bei diesem Lieferschein existiert.
**/
{
	     int i;

	     for (i = 0; i < aufpanz; i ++)
		 {
			 Text Charge = aufparr[i].ls_charge;
			 Charge.Trim ();
			 if (a == ratod (aufparr[i].a) &&
				      ListCharge == Charge)
			 {
					  break;
			 }
//			 if (a == ratod (aufparr[i].a) && aufparr[i].s[0] < '3' &&
//				      Charge == "")
			 if (a == ratod (aufparr[i].a) && Charge == "")
			 {
					  break;
			 }
		 }
		 if (i == aufpanz) return -1;
		 return (i);
}

int ScanKunEan (double ean)
{
        double a;

        DbClass.sqlin ((short *) &akt_mdn, 1, 0);
        DbClass.sqlin ((long *)  &kun.kun, 2, 0);
        DbClass.sqlin ((double *) &ean, 3, 0);
        DbClass.sqlout ((double *) &a, 3, 0);

        int dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                          "where mdn = ? "
                                          "and kun = ? "
                                          "and kun_bran2 <= \"0\" "
                                          "and ean = ?");
        if (dsqlstatus == 100)
        {
               DbClass.sqlin ((short *) &akt_mdn, 1, 0);
               DbClass.sqlin ((char *)  kun.kun_bran2, 0, 3);
               DbClass.sqlin ((double *) &ean, 3, 0);
               DbClass.sqlout ((double *) &a, 3, 0);

               int dsqlstatus = DbClass.sqlcomm ("select a from a_kun "
                                          "where mdn = ? "
                                          "and kun = 0 "
                                          "and kun_bran2 = ? "
                                          "and ean = ?");
        }

        if (dsqlstatus == 0)
        {
                 dsqlstatus = lese_a_bas (a);
        }
        return dsqlstatus;
}

LONG FAR PASCAL EntNuProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{

        switch(msg)
        {
              case WM_PAINT :
				    NuBlock.OnPaint (hWnd, msg, wParam, lParam);
					break;

              case WM_LBUTTONDOWN :
              case WM_LBUTTONUP :
              case WM_MOUSEMOVE :
				    NuBlock.OnButton (hWnd, msg, wParam, lParam);
                    return TRUE;
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}
