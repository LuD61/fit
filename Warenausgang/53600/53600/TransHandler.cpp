// TransHandler.cpp: Implementierung der Klasse CTransHandler.
//
//////////////////////////////////////////////////////////////////////

#include "TransHandler.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CTransparent::CTransparent ()
{
	Parent = NULL;
	x = 0;
	y = 0;
	cx = 0;
	cy = 0;
	text = NULL;
	TextColor = RGB (0, 0, 0);
	hFont = NULL;
	oldFont = NULL;
	xAlignment = Left; 
	yAlignment = RowCenter; 
}

CTransparent::~CTransparent ()
{
	Destroy ();
}

void CTransparent::Create ()
{
	if (Parent == NULL)
	{
		return;
	}

	CTransHandler::GetInstance ()->Add (this);
}

void CTransparent::Create (HWND Parent, int x, int y, int cx, int cy, char *text)
{
	SetParent (Parent);
	SetX (x);
	SetY (y);
	SetCX (cx);
	SetCY (cy);
	SetText (text);
	CTransHandler::GetInstance ()->Add (this);
	HDC hdc = GetDC (Parent);
	cy = HeightY (cy, hdc);
    ReleaseDC (Parent, hdc);
	Background.ReadFromWindow (Parent, x, y, cx, cy);
}

void CTransparent::Destroy ()
{
	if (text != NULL)
	{
		delete text;
		text = NULL;
	}
	if (hFont != NULL)
	{
		HDC hdc = GetDC (Parent);
		SelectObject (hdc, oldFont);
		ReleaseDC (Parent, hdc);
		DeleteObject (hFont);
	}
	CTransHandler::GetInstance ()->Drop (this);
}

int CTransparent::HeightY (int cy, HDC hdc)
/**
Hoehe ermitteln.
**/
{
        TEXTMETRIC tm;

		if (hFont != NULL)
		{
			oldFont = SelectObject (hdc, hFont);
		}
        GetTextMetrics (hdc, &tm);
		if (hFont != NULL)
		{
			SelectObject (hdc, oldFont);
		}
        if (cy == 0)
        {
				cy = tm.tmHeight + tm.tmHeight / 3;
		}
        return (cy);
}

void CTransparent::Draw (HDC hdc)
{
	SIZE size;
	int x, y;
	int len;

	SetBkMode (hdc, TRANSPARENT);
	if (hFont != NULL)
	{
		oldFont = SelectObject (hdc, hFont);
	}
	len = strlen (text);
    GetTextExtentPoint (hdc, text, len, &size);
	while (size.cx > cx && len > 0)
	{
		len --;
        GetTextExtentPoint (hdc, text, len, &size);
	}

	if (xAlignment == Left)
	{
		x = this->x;
	}
	else if (xAlignment == Right)
	{
		x = this->cx - size.cx;
		x = max (x, this->x);
	}
	else if (xAlignment == Center)
	{
		x = this->x + (this->cx - size.cx) / 2;
		x = max (x, this->x);
	}

	if (this->cy == 0)
	{
		this->cy = HeightY (this->cy, hdc);
	}

	if (yAlignment == Top)
	{
		y = this->y;
	}
	else if (yAlignment == Bottom)
	{
		y = this->cy - size.cy;
		y = max (y, this->y);
	}
	else if (yAlignment == RowCenter)
	{
		y = this->y + (this->cy - size.cy) / 2;
		y = max (y, this->y);
	}

	::SetTextColor (hdc, TextColor);
    TextOut (hdc, x, y, text, len);
	if (hFont != NULL)
	{
		SelectObject (hdc, oldFont);
	}
}

void CTransparent::Draw ()
{
	HDC hdc = GetDC (Parent);
	Draw (hdc);
	ReleaseDC (Parent, hdc);
}

void CTransparent::Invalidate ()
{
	Background.WriteToWindow (Parent, x, y, cx, cy);
	HDC hdc = GetDC (Parent);
	Draw (hdc);
	ReleaseDC (Parent, hdc);
}


CTransHandler *CTransHandler::Instance = NULL;


CTransHandler::CTransHandler()
{
}

CTransHandler::~CTransHandler()
{

}


CTransHandler *CTransHandler::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CTransHandler ();
	}
	return Instance;
}

void CTransHandler::DestroyInstance ()
{
	if (Instance == NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

void CTransHandler::Add (CTransparent *window)
{
	windows.push_back (window);
}

void CTransHandler::Drop (CTransparent *window)
{
	CTransparent *t;

	for (vector<CTransparent *>::iterator it = windows.begin (); it != windows.end (); ++it)
	{
		t = *it;
		if (t == window)
		{
			windows.erase (it);
			break;
		}
	}
}


void CTransHandler::Draw (HWND Parent, HDC hdc)
{
	CTransparent *t;


	for (vector<CTransparent *>::iterator it = windows.begin (); it != windows.end (); ++it)
	{
		t = *it;
		if (t->GetParent () == Parent)
		{
			t->Draw (hdc);
		}
	}
}

void CTransHandler::Draw (HWND Parent)
{
    PAINTSTRUCT ps;

	HDC hdc = BeginPaint (Parent, &ps);
	Draw (Parent, hdc);
    EndPaint (Parent, &ps);
}
