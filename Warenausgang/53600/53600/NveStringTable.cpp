// NveStringTable.cpp: Implementierung der Klasse CNveStringTable.
//
//////////////////////////////////////////////////////////////////////

#include "NveStringTable.h"
#include "Nve.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CNveStringTable::CNveStringTable()
{
	m_Iln = "";
	m_Mdn = 1;
	m_Fil = 0;
	m_Ls  = 0l;
	m_BlgTyp  = "L";
	m_Error = FALSE;
	m_ErrorNumber = 0;
	m_ErrorMessage = "";
}

CNveStringTable::~CNveStringTable()
{
	DestroyElements ();
}

void CNveStringTable::DestroyElements ()
{
	CString *s;
	CString **it;
	Start ();
	while ((it = GetNext ()) != NULL)
	{
		s = *it;
		if (s != NULL)
		{
			delete s;
		}
	}
}

void CNveStringTable::Run ()
{
	CNve * Nve;
	CString *s;
	CString **it;
	Start ();
	CNveDb *NveDb = new CNveDb ();
	NveDb->set_Mdn (m_Mdn);
    NveDb->set_Fil (m_Fil);
    NveDb->set_Ls (m_Ls);
    NveDb->set_BlgTyp (m_BlgTyp);
	NveDb->DeleteLs ();
	while ((it = GetNext ()) != NULL)
	{
		s = *it;
		if (s != NULL)
		{
			 Nve = new CNve ();
			 Nve->set_LGutString ((LPSTR) *s);
			 Nve->set_Iln (m_Iln);
			 Nve->set_Mdn (m_Mdn);
			 Nve->set_Fil (m_Fil);
			 Nve->set_Ls (m_Ls);
			 Nve->set_BlgTyp (m_BlgTyp);
			 Nve->Update ();
			 if (Nve->Error ())
			 {
				 m_Error = Nve->Error ();
				 m_ErrorMessage = Nve->GetErrorText ();
			 }
			 delete Nve;
			 if (m_Error)
			 {
				 break;
			 }
		}
	}
}

void CNveStringTable::AddString (LPSTR line)
{
	CString *Line = new CString (line);
	Add (Line);
}