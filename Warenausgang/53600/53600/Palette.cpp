#include "Palette.h"

CPalette::CPalette ()
{
	a = 0.0;
    brutto = 0.0;
    netto  = 0.0;
	count = 0;
	Charge = "";
	Nve = "";
	ArtList.clear ();
}

CPalette::CPalette (double a)
{
	this->a = a;
    brutto = 0.0;
    netto  = 0.0;
	count = 0;
	Charge = "";
	Nve = "";
	ArtList.clear ();
	ArtList.push_back (a);
}

CPalette::CPalette (double a, Text& Charge)
{
	this->a = a;
    brutto = 0.0;
    netto  = 0.0;
    brutto_gew  = 0.0;
	count = 0;
	this->Charge = Charge;
	this->Nve = "";
	ArtList.clear ();
	ArtList.push_back (a);
}

CPalette::CPalette (double a, LPSTR Charge, int NveLfd )
{
	this->a = a;
    brutto = 0.0;
    netto  = 0.0;
    brutto_gew  = 0.0;
	count = 0;
	this->Charge = Charge;
	this->NveLfd = NveLfd;
	this->Nve = "";
	ArtList.clear ();
	ArtList.push_back (a);
}

CPalette::CPalette (CPalette& p)
{
	a     = p.a;
    brutto   = p.brutto;
    netto   = p.netto;
    brutto_gew  = p.brutto_gew;
	count = p.count;
	Charge = p.Charge;
	Nve = p.Nve;
	ArtList.clear ();
	for (std::vector<double>::iterator ia = p.ArtList.begin (); 
	                           ia != p.ArtList.begin ();
							   ia ++)
   {
	     ArtList.push_back (*ia);
   }
}


CPalette::~CPalette ()
{
}

void CPalette::InitGew ()
{
	brutto = 0.0;
	netto  = 0.0;
    brutto_gew  = 0.0;
	count = 0;
	ArtList.clear ();
}

void CPalette::AddGew (double brutto, double netto)
{
	this->brutto += brutto;
	this->netto  += netto;
	count ++;
}

void CPalette::AddGew (double brutto, double netto, double a)
{
	this->brutto += brutto;
	this->netto  += netto;
	if (a != this->a)
    {
		if (ArtList.size () == 0)
		{
			ArtList.push_back (this->a);
		}
		ArtList.push_back (a);
	}
	count ++;
}


CWiegClass::CWiegClass ()
{
	a = 0.0;
    brutto = 0.0;
    netto  = 0.0;
    brutto_gew  = 0.0;
	count = 0;
	Charge = "";
}

CWiegClass::CWiegClass (double a)
{
	this->a = a;
    brutto = 0.0;
    netto  = 0.0;
    brutto_gew  = 0.0;
	count = 0;
	Charge = "";
}

CWiegClass::CWiegClass (CWiegClass& w)
{
	this->a = w.a;
    brutto = w.brutto;
    netto  = w.netto;
    brutto_gew  = w.brutto_gew;
	count = w.count;
	Charge = w.Charge;
}

CWiegClass::~CWiegClass ()
{
}

void CWiegClass::AddGew (double brutto, double netto)
{
	this->brutto += brutto;
	this->netto  += netto;
	this->brutto_gew += brutto;
	count ++;
}

void CWiegClass::AddGew ( double a, double brutto, double netto)
{
	this->brutto += brutto;
	this->netto  += netto;
	this->brutto_gew += brutto;
	count ++;
}

void CWiegClass::SetGew (double brutto, double netto)
{
	this->brutto = brutto;
	this->netto  = netto;
	this->brutto_gew = brutto;
	count = 1;
}
