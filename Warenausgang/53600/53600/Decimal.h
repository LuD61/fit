// Decimal.h: Schnittstelle f�r die Klasse Decimal.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DECIMAL_H__45DEF8F3_E62C_4234_88FC_0E3F896A5B7C__INCLUDED_)
#define AFX_DECIMAL_H__45DEF8F3_E62C_4234_88FC_0E3F896A5B7C__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>

#define MAXDIGITS 21
#define CHARSIZE 11

class Decimal  
{
private:
	UCHAR m_Digits[CHARSIZE];
	char m_StringValue[MAXDIGITS];
    short m_Scale;
	BOOL m_Signed;
public:
	Decimal();
	virtual ~Decimal();
	void SetValue (LPSTR value);
	LPSTR StringValue ();
};

#endif // !defined(AFX_DECIMAL_H__45DEF8F3_E62C_4234_88FC_0E3F896A5B7C__INCLUDED_)
