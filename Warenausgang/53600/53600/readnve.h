#ifndef _ReadNve_Def
#define _ReadNve_Def
#include "Text.h"
#include "lsnve.h"

class ReadNve 
{
     private :
     public :
        Text nve;
        Text nve_palette;
		BOOL nve_karton;
        short mdn;
        short fil;
        long ls;
		BOOL gue;
		long nve_posi;
		long mhd;
  	    LSNVE_CLASS Lsnve;
	   
        ReadNve (Text&, short, short, long);
        long GenNvePosi ();
		long GetNvePosi (); 
 	    int read ();
        int readPalette (double *);
        int InsertNve ();
        double getPaletteMe ();
		BOOL IsKarton ();
		BOOL IsGue ();
		int TestPaletteGue ();
        void SetPalettePosUngueltig ();
        void SetPaletteUngueltig ();
        void SetNvePosi (long);
        void SetPaletteNvePosi (long);
        void SetPalettePosNvePosi (long);
        void UpdateNve (long, Text&, long);
		void SetMhd (LPSTR);
};
#endif