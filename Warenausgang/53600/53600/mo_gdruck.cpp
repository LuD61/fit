/* ************************************************************************************** */
/*  Module zum Drucken ueber Windows GDI-Aufrufe                                          */
/*                                                                                        */
/*  erstellt am 16.11.1999                                                                */ 
/*                                                                                        */
/*  Autor  W.Roth                                                                         */
/*                                                                                        */
/* ************************************************************************************** */

#include <stdio.h>
#include <windows.h>
#include "process.h"
#include "wmask.h"
#include "mo_meld.h"
#include "strfkt.h"
#include "mo_gdruck.h"

#define TAB 9

LONG FAR PASCAL PrintProc(HWND,UINT,WPARAM,LPARAM);

int WindowOk = 0;
extern HANDLE  hMainInst;

static char szAllPrinters [4096];
static char *PrinterTab [100];
static int printeranz;
static int printerpos;

static BOOL PrinterSet = FALSE;
char FixPrinter [80];

static char *pers_nam;

static TEXTMETRIC tm;
static HFONT hFont, oldfont;;
static int StdRowHeight;
static int RowHeight;
static int AktRowHeight;
static int MaxRowHeight;
static int PageRows;
static int cxPage;
static int cyPage;
static  char *DocName;
static DOCINFO di = {sizeof (DOCINFO), DocName, NULL};
static char text[] = {"Hello, Printer!"};

mfont printfont = {"Arial", 500, 0, 3, RGB (255, 0, 0),
                                       RGB (0, 255, 255),
                                            1,
                                            NULL};

mfont stdprintfont   = {"Courier New", 100, 100, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont80 = {"Courier New", 100, 100, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont81 = {"Courier New", 90, 100, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont82 = {"Courier New", 80, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont83 = {"Courier New", 70, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

mfont stdprintfont84 = {"Courier New", 60, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

mfont stdprintfont85 = {"Courier New", 50, 0, 0, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

mfont stdprintfont90 = {"Courier New", 110, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont91 = {"Courier New", 120, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont92 = {"Courier New", 130, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont93 = {"Courier New", 140, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

mfont stdprintfont94 = {"Courier New", 150, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};
mfont stdprintfont95 = {"Courier New", 200, 0, 1, RGB (255, 0, 0),
                                     RGB (0, 255, 255),
                                     1,
                                     NULL};

static int SzTab[] = {80, 81, 82, 83, 84, 85, 90, 91, 92, 93, 94, 95, 0};
static mfont *FontTab [] = {&stdprintfont80,
                            &stdprintfont81, 
                            &stdprintfont82, 
                            &stdprintfont83, 
                            &stdprintfont84, 
                            &stdprintfont85, 
                            &stdprintfont90,
                            &stdprintfont91,
                            &stdprintfont92,
                            &stdprintfont93,
                            &stdprintfont94,
                            &stdprintfont95,
							NULL};
 

static char buffer [512];

static int tabbl = 8;

static char Dname[80];



char *basname (char *dname)
/**
Basisname ermitteln.
**/
{
	int anz;
	char pid[10];

	anz = wsplit (dname, "\\");
	if (anz < 1) return ("TEST");
	strcpy (Dname, wort [anz - 1]);
	anz = wsplit (Dname, "."); 
	if (anz < 1) return ("TEST");
	strcpy (Dname, wort [0]);
//	sprintf (pid, "%d", getpid ());
	strcpy (pid, "1001");
	strcat (Dname, pid);
	return Dname;
}
	   

void ChangeTabs (unsigned char *buffer)
/**
Tabulatoren ersetzen.
**/
{
	unsigned char tabbuff [512];
	unsigned char *bf;
	unsigned char *tb;

     
	for (tb = tabbuff, bf = buffer; 
	      *bf; 
		  bf += 1, tb += 1)
	{
		if (*bf == TAB)
		{
			memset (tb, ' ', tabbl);
			tb += tabbl - 1;
		}
		else if (*bf < ' ')
		{
			*tb = ' ';
		}
		else
		{
			*tb = *bf;
		}
	}
	*tb = 0;
	strcpy ((char *) buffer, (char *) tabbuff);
}

void SetGdiPrinter (char *pr)
/**
Druckername fest setzen.
**/
{
	int i;
    GetAllPrinters ();
	for (i = 0; i < printeranz; i ++)
	{
		clipped (pr); 
		if (strcmp (pr, PrinterTab[i]) == 0)
		{
	               PrinterSet = TRUE;
				   strcpy (FixPrinter, pr);
				   break;
		}
	}
}


HDC GetPrinterDC (void)
/**
Druckerinformationen holen.
**/
{
      PRINTER_INFO_4 pinfo5 [20];
      DWORD dwNeeded, dwReturned;

// Alle Drucker  

//    if (EnumPrinters (PRINTER_ENUM_LOCALL, 5, (LPBYTE) pinfo5,
//                      sizeof (pinfo5), &dwNeeded, &dwReturned))
// Standard-Drucker

      if (EnumPrinters (PRINTER_ENUM_LOCAL, "", 4, (LPBYTE) pinfo5,
                        sizeof (pinfo5), &dwNeeded, &dwReturned))
      {
          return CreateDC (NULL, pinfo5[0].pPrinterName, NULL, NULL);
      }
      return NULL;
}

HDC GetPrinterDCini (void)
/**
Printername aus WIN.INI holen.
**/
{
	char szPrinter [80];
	char *szDevice, *szDriver, *szOutput;

	GetProfileString ("windows", "device", ",,,", szPrinter, 80);

	if (NULL != (szDevice = strtok (szPrinter, ",")) &&
		NULL != (szDriver = strtok (NULL,      ",")) &&
		NULL != (szOutput = strtok (NULL,      ",")))
	{
		return CreateDC (szDriver, szDevice, szOutput, NULL);
	}
		return 0;
}

void SetPersName (char *pname)
/**
Pers_name setzen.
**/
{
	pers_nam = pname;
}


void SetPrinter (char *szDevice)
/**
Aktuellen Drucker setzen.
**/
{
	 char gdiname [512];
	 char *etc;
	 FILE *fp;

	 etc = getenv ("FRMPATH");
	 if (etc == NULL) return;

     if (pers_nam == NULL || pers_nam[0] <= ' ')
     {
                        pers_nam = "drucker";
     }
	 sprintf (gdiname, "%s\\%s.gdi", etc, pers_nam);
	 fp = fopen (gdiname, "w");
	 if (fp == NULL) return;
	 fprintf (fp, "%s\n", szDevice);
	 fclose (fp);

	 sprintf (gdiname, "%s\\drucker.gdi", etc);
	 fp = fopen (gdiname, "w");
	 if (fp == NULL) return;
	 fprintf (fp, "%s\n", szDevice);
	 fclose (fp);
}

char *GetPrinter (char *szDevice)
/**
Aktuellen Drucker setzen.
**/
{
	 char gdiname [512];
	 char *etc;
	 FILE *fp;

	 etc = getenv ("FRMPATH");
	 if (etc == NULL) return NULL;

     if (pers_nam == NULL || pers_nam[0] <= ' ')
     {
                        pers_nam = "drucker";
     }
	 sprintf (gdiname, "%s\\%s.gdi", etc, pers_nam);
	 fp = fopen (gdiname, "r");
	 if (fp == NULL) return NULL;
	 if (fgets (szDevice, 80, fp) == NULL) 
	 {
 	      fclose (fp);
		  return NULL;
	 }
	 fclose (fp);
	 cr_weg (szDevice);
	 return szDevice;
}


HDC GetPrinterbyName (char *szDevice)
/**
Printername aus WIN.INI holen.
**/
{
	char szPrinter [80];
	char * szDriver, *szOutput;

	GetProfileString ("devices", szDevice, "", szPrinter, 80);

	szDriver = strtok (szPrinter, ",");
	szOutput = strtok (NULL,      ",");
	return CreateDC (szDriver, szDevice, szOutput, NULL);
}

char *GetNextPrinter (void)
/**
Naechsten Druckernamen holen.
Neustart ueber GetAllPrinters oder wenn Ende erreicht.
**/
{
	 char *prptr;
	 
	 if (printerpos == printeranz)
	 {
		 printerpos = 0;
		 return NULL;
	 }
	 prptr = PrinterTab [printerpos];
	 printerpos ++;
	 return prptr;
}


void GetAllPrinters (void)
/**
Alle Printernamen aus WIN.INI holen.
**/
{
	char *prps;

	strcpy (szAllPrinters, "\0\0");
	GetProfileString ("devices", NULL, "", szAllPrinters, sizeof (szAllPrinters));
    prps = szAllPrinters;
	printeranz = 0;
	while (TRUE)
	{
		if (memcmp (prps, "\0\0", 2) == 0)
		{
			break;
		}
		PrinterTab[printeranz] = prps;
		printeranz ++;
		prps += strlen (prps) + 1;
	}
	printerpos = 0;
}


void PrintPage (HDC hdc)
/**
Eine Seite drucken.
**/
{
	HFONT hFont, oldfont;;
	TEXTMETRIC tm;


	Rectangle (hdc, 10, 10, cxPage - 20, cyPage - 20);

	SaveDC (hdc);

	SetMapMode (hdc, MM_ISOTROPIC);
	SetWindowExtEx (hdc, 1000, 1000, NULL);
	SetViewportExtEx (hdc, cxPage / 2, -cyPage / 2, NULL);
	SetViewportOrgEx (hdc, cxPage / 2, cyPage / 2, NULL);

	hFont = SetDeviceFont (hdc, &printfont, &tm);
	oldfont = SelectObject (hdc, hFont);
	
	SetTextAlign (hdc, TA_BASELINE | TA_CENTER);

    SetTextColor (hdc,BLUECOL);
    SetBkColor (hdc,YELLOWCOL);
	TextOut (hdc, 0,0, text, strlen (text));
    DeleteObject (SelectObject (hdc, oldfont)); 
	RestoreDC (hdc, -1);
}


char *SetPrintFont (HDC hdc, char *bn)
/**
Steuerzeichen untersuchen und neuen Font setzen.
**/
{
    int i;
	
	bn += 1;

	for (i = 0; SzTab[i]; i++)
	{
		if (SzTab[i] == *bn)
		{
                 DeleteObject (SelectObject (hdc, oldfont));
	             hFont = SetDeviceFont (hdc, FontTab[i], &tm);
  	             AktRowHeight = (int) (double) ((double) tm.tmHeight * 1.0); 
				 MaxRowHeight= max (MaxRowHeight,AktRowHeight);
	             oldfont = SelectObject (hdc, hFont);
				 break;
		}
	}
	return (bn + 1);
}


void  PrintText (HDC hdc, int x, int y, char *buffer, int len)
/**
Text schreiben, vorher auf Steuerzeichen pruefen..
**/ 
{
	char *bs;
    char *bn;
	char *be;
	char out [512];

	RowHeight = max (AktRowHeight, StdRowHeight);
	MaxRowHeight = RowHeight;

	be = buffer + len;
	bs = buffer;
	if ((bn = strchr (bs, (char) 27)) == NULL)
	{
  	    ChangeTabs ((unsigned char *) bs);
		TextOut (hdc, x, y, bs, strlen (bs));
		return;
	}
	while (bs < be)
	{
		if (bs < bn)
		{
			memcpy (out, bs, (int) (bn - bs));
			out [(int) (bn - bs)]= 0; 
 	  	    ChangeTabs ((unsigned char *) out);
		    TextOut (hdc, x, y, out, strlen (out));
			x += strlen (out) * tm.tmAveCharWidth;
		}
		bs = SetPrintFont (hdc, bn);
		if (AktRowHeight > RowHeight) RowHeight = AktRowHeight;
	    if ((bn = strchr (bs, (char) 27)) == NULL)
		{
  	        ChangeTabs ((unsigned char *) bs);
		    TextOut (hdc, x, y, bs, strlen (bs));
			if (AktRowHeight > RowHeight) RowHeight = AktRowHeight;
		    return;
		}
	}
	if (AktRowHeight > RowHeight) RowHeight = AktRowHeight;
}

  	 


void FormFeed (HDC hdc)
/**
Seite auf Drucker ausgeben.
**/
{
      EndPage (hdc);
}



void PrintPages (HDC hdc, FILE *fp) 
/**
Alle Seiten Drucken.
**/
{
	int Row;
	  
	hFont = SetDeviceFont (hdc, &stdprintfont, &tm);
	RowHeight = (int) (double) ((double) tm.tmHeight * 1.0); 
    StdRowHeight = AktRowHeight = MaxRowHeight = RowHeight;
    PageRows = (int) (double) ((double) cyPage / RowHeight) - 3;
	oldfont = SelectObject (hdc, hFont);

   
	Row = 1;
    if (StartPage (hdc) == 0) return;
	while (fgets (buffer, 511, fp))
	{
		cr_weg (buffer); 
		if (Row == PageRows)
		{
			FormFeed (hdc);
			Row = 1;
		}

 	    PrintText (hdc, tm.tmAveCharWidth, Row * MaxRowHeight, buffer, strlen (buffer));
		Row ++;
	}
    EndPage (hdc);
	DeleteObject (SelectObject (hdc, oldfont));
}
	
	   

int Gdruck (char *dname)
/**
Die Datei dname �ber Window-Funktionen drucken.
**/
{
      HDC hdc;
	  FILE *fp;
	  char szDevice [80];


	  DocName = basname (dname);
	  fp = fopen (dname, "r");
	  if (fp == NULL)
	  {
		  if (WindowOk)
		  {
		              print_mess (2, "%s kann nicht ge�ffnet werden", dname);
		  }
		  return -1;
	  }


	  if (PrinterSet)
	  {
	      hdc = GetPrinterbyName (FixPrinter);
	  }
	  else if (GetPrinter (szDevice))
	  {

	      hdc = GetPrinterbyName (szDevice);
	  }
	  else
	  {
	      hdc = GetPrinterDCini ();
	  }

//	  hdc = GetPrinterDC ();
	  if (hdc == NULL) return -1;

	  cxPage = GetDeviceCaps (hdc, HORZRES);
	  cyPage = GetDeviceCaps (hdc, VERTRES);

	  if (StartDoc (hdc, &di) == 0) return -1;

	  PrintPages (hdc, fp);

	  EndDoc (hdc);
	  DeleteDC (hdc);
	  return 0;
}


void SetPage (void)
/**
Seite Einrichten.
**/
{
 	   
         PAGESETUPDLG pgdlg;


		 pgdlg.lStructSize = sizeof (PAGESETUPDLG);
		 pgdlg.hwndOwner   = NULL; 
		 pgdlg.hDevMode    = NULL; 
		 pgdlg.hDevNames   = NULL; 
		 pgdlg.Flags       = NULL; 
		 pgdlg.ptPaperSize.x = 0;
		 pgdlg.ptPaperSize.y = 0;

		 pgdlg.rtMinMargin.left   = 0; 
		 pgdlg.rtMinMargin.top    = 0; 
		 pgdlg.rtMinMargin.right  = 0; 
		 pgdlg.rtMinMargin.bottom = 0; 

		 pgdlg.rtMargin.left   = 0; 
		 pgdlg.rtMargin.top    = 0; 
		 pgdlg.rtMargin.right  = 0; 
		 pgdlg.rtMargin.bottom = 0; 

		 pgdlg.hInstance   = hMainInst; 
		 pgdlg.lCustData   = NULL; 
		 pgdlg.lpfnPageSetupHook  
			               = NULL;
		 pgdlg.lpfnPagePaintHook  
			               = NULL;
		 pgdlg.lpPageSetupTemplateName 
			               = NULL;
		 pgdlg.hPageSetupTemplate
			               = NULL;

		 PageSetupDlg(&pgdlg);
}

void SelectPrinter (void)
/**
Drucker selektieren.
**/
{
         PRINTDLG prdlg;

		 prdlg.lStructSize = sizeof (PRINTDLG);
		 prdlg.hwndOwner   = NULL; 
		 prdlg.hDevMode    = NULL; 
		 prdlg.hDevNames   = NULL; 
		 prdlg.hDC         = NULL; 
		 prdlg.Flags       = NULL; 
		 prdlg.nFromPage   = NULL;
		 prdlg.nToPage     = NULL;
		 prdlg.nMinPage    = NULL;
		 prdlg.nMaxPage    = NULL;
		 prdlg.nCopies    = 1;

		 prdlg.hInstance   = hMainInst; 
		 prdlg.lCustData   = NULL; 
		 prdlg.lpfnPrintHook  
			               = NULL;
		 prdlg.lpfnSetupHook 
			               = NULL;
		 prdlg.lpPrintTemplateName
			               = NULL;
		 prdlg.lpSetupTemplateName
			               = NULL;
		 prdlg.hPrintTemplate
			               = NULL;
		 prdlg.hSetupTemplate
			               = NULL;

         PrintDlg (&prdlg);
}


int IsGdiPrint (void)
/**
Test, ob im Windows-GDI-Modus gedruckt wird.
**/
{
	      char *gdi;

		  gdi = getenv_default ("GDI_PRINT");
		  if (gdi)
		  {
			  return atoi (gdi);
		  }
		  return 0;
}

static void RegisterPrinter (void)
{
        static int registered = 0;
        WNDCLASS wc;

        if (registered) return;

        registered = 1;
        wc.style         =  CS_HREDRAW | CS_VREDRAW | CS_BYTEALIGNWINDOW
                            | CS_CLASSDC;
        wc.lpfnWndProc   =  PrintProc;
        wc.cbClsExtra    =  0;
        wc.cbWndExtra    =  0;
        wc.hInstance     =  hMainInst;
        wc.hIcon         =  LoadIcon (hMainInst, "ROSIICON");
        wc.hCursor       =  LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground =  GetStockObject (LTGRAY_BRUSH);
        wc.lpszMenuName  =  "";
        wc.lpszClassName =  "PrintWindow";

        RegisterClass(&wc);

}

static char AktPrinter[80];

static field _fprinter[] =
{AktPrinter, 39, 0, 1, 1, 0, "", EDIT, 0, 0, 0};

static form fprinter = {1, 0, 0, _fprinter, 0, 0, 0, 0, NULL}; 
  

void ShowGdiPrinters (void)
/**
Drucker in Listbox anzeigen.
**/
{
	     HDC hdc; 
	     HFONT hFont, oldfont;
		 int x, y, cx, cy;
		 HWND lMainWindow, ListWindow;
		 int wsize;
		 int i;
		 int idx;
		 
         RegisterPrinter ();
		 
		 SetEnvFont ();
		 GetAllPrinters ();
         AktPrinter[0] = 0;
		 GetPrinter (AktPrinter);

		 wsize = printeranz;
		 if (wsize > 12) wsize = 12;

		 hFont = SetWindowFont (NULL);
		 hdc = GetDC (NULL);
		 oldfont = SelectObject (hdc, hFont);
		 GetTextMetrics (hdc, &tm);
		 DeleteObject (SelectObject (hdc, oldfont));
		 ReleaseDC (NULL, hdc);
		 cx = 42 * tm.tmAveCharWidth;
		 cy = (int) (double) ((double) (wsize + 5) * tm.tmHeight * 1.5);
		 x = 20 * tm.tmAveCharWidth;
		 y = (int) (double) ((double) (25 * tm.tmHeight * 1.5 - cy) / 2); 
		 y = max (0,y);

         lMainWindow  = CreateWindow ("PrintWindow",
                                      "Druckerwahl",
                                      WS_POPUP | WS_THICKFRAME | WS_CAPTION | WS_VISIBLE,
                                      x, y,
                                      cx, cy,
                                      NULL,
                                      NULL,
                                      hMainInst,
									  NULL);

//		  display_form (lMainWindow, &fprinter, 0, 0);
		  create_enter_form (lMainWindow, &fprinter, 0, 0);
		  SetBorder (WS_CHILD | WS_VISIBLE);
          ListWindow = OpenListWindowBu (lMainWindow, wsize,
                                         36, 2, 1, FALSE);

          for (i = 0; i < printeranz; i ++)
		  {
			  InsertListRow (PrinterTab [i]);
		  }
          idx = EnterQueryListBox ();
		  if (idx != -1)
		  {
			  SetPrinter (PrinterTab [idx]);
		  }
		  DestroyWindow (lMainWindow);
}



LONG FAR PASCAL PrintProc(HWND hWnd,UINT msg,
                        WPARAM wParam,LPARAM lParam)
{
        PAINTSTRUCT ps;
        HDC hdc;

        switch(msg)
        {

              case WM_USER :
                      return 0;

              case WM_PAINT :
                      hdc = BeginPaint (hWnd, &ps);
                      EndPaint (hWnd, &ps);
                      break;
              case WM_DESTROY :
                      PostQuitMessage (0);
                      break;

              case WM_COMMAND :
                    if (LOWORD (wParam) == KEY5)
                    {
                            syskey = KEY5;
                            SendKey (VK_F5);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY6)
                    {
                            syskey = KEY6;
                            SendKey (VK_F6);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY7)
                    {
                            syskey = KEY7;
                            SendKey (VK_F7);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY8)
                    {
                            syskey = KEY8;
                            SendKey (VK_F8);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY9)
                    {
                            syskey = KEY9;
                            SendKey (VK_F9);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY10)
                    {
                            syskey = KEY10;
                            SendKey (VK_F10);
                            break;
                    }
                    else if (LOWORD (wParam) == KEY11)
                    {
                            syskey = KEY11;
                            SendKey (VK_F11);
                            break;
                    }
                    if (LOWORD (wParam) == KEY12)
                    {
                            syskey = KEY12;
                            SendKey (VK_F12);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGD)
                    {
                            syskey = KEYPGD;
                            SendKey (VK_NEXT);
                            break;
                    }
                    else if (LOWORD (wParam) == KEYPGU)
                    {
                            syskey = KEYPGU;
                            SendKey (VK_PRIOR);
                            break;
                    }
        }
        return DefWindowProc(hWnd, msg, wParam, lParam);
}



