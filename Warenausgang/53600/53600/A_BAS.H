#pragma once
/*   Datenbankfunktionen                                  */

#define HNDW 1
#define EIG 2
#define EIG_DIV 3

struct A_BAS {
   double    a;
   short     mdn;
   short     fil;
   char      a_bz1[25];
   char      a_bz2[25];
   double    a_gew;
   short     a_typ;
   short     a_typ2;
   short     abt;
   long      ag;
   char      best_auto[2];
   char      bsd_kz[2];
   char      cp_aufschl[2];
   short     delstatus;
   short     dr_folge;
   long      erl_kto;
   char      hbk_kz[2];
   short     hbk_ztr;
   short     restlaufzeit;
   char      hnd_gew[2];
   short     hwg;
   char      kost_kz[3];
   short     me_einh;
   char      modif[2];
   short     mwst;
   short     plak_div;
   char      stk_lst_kz[2];
   double    sw;
   short     teil_smt;
   long      we_kto;
   short     wg;
   short     zu_stoff;
   long      akv;
   long      bearb;
   char      pers_nam[9];
   double    prod_zeit;
   char      pers_rab_kz[2];
   double    gn_pkt_gbr;
   long      kost_st;
   char      sw_pr_kz[2];
   long      kost_tr;
   double    a_grund;
   long      kost_st2;
   long      we_kto2;
   long      charg_hand;
   long      intra_stat;
   char      qual_kng[5];
   short     lief_einh;
   double    inh_lief;
   char      produkt_info[513];
   long		 txt_nr1;
   long		 txt_nr2;
   long		 txt_nr3;
   long		 txt_nr4;
   long		 txt_nr5;
   long		 txt_nr6;
   long		 txt_nr7;
   long		 txt_nr8;
   long		 txt_nr9;
   long		 txt_nr10;
   short     zerl_eti;
   char	     lgr_tmpr[21];   // vor 10.05.13: lgr_tempr[20] 
   short     filialsperre;
   short     ghsperre;
   char      glg[2];
   char      krebs[2];
   char      ei[2];
   char      fisch[2];
   char      erdnuss[2];
   char      soja[2];
   char      milch[2];
   char      schal[2];
   char      sellerie[2];
   char      senfsaat[2];
   char      sesamsamen[2];
   char      sulfit[2];
   char      weichtier[2];
   char      brennwert[21];
   double     eiweiss;
   double     kh;
   double     fett;
   char      sonstige[2];
   char      gruener_punkt[2];
   char      vakuumiert[2];
   long		 allgtxt_nr1;
   long		 allgtxt_nr2;
   long		 allgtxt_nr3;
   long		 allgtxt_nr4;
   char      shop_kz[2];
   short     gebindepalette;

};

extern struct A_BAS _a_bas, _a_bas_null;

struct A_BAS_ERW {
   double    a;
   char      pp_a_bz1[100];
   char      pp_a_bz2[100];
   char      lgr_tmpr[100];
   char      lupine[2];
   char      schutzgas[2];
   short     huelle;
   short     shop_wg1;
   short     shop_wg2;
   short     shop_wg3;
   short     shop_wg4;
   short     shop_wg5;
   double    tara2;
   short     a_tara2;
   double    tara3;
   short     a_tara3;
   double    tara4;
   short     a_tara4;
   double    salz;
   double    davonfett;
   double    davonzucker;
   double    ballaststoffe;
   char      zutat[2001];
   short     userdef1;
   short     userdef2;
   short     userdef3;
   short	 paketartikel;
};
extern struct A_BAS_ERW a_bas_erw, a_bas_erw_null, a_bas_erw2;


int lese_a (char *);
int lese_a (void);
int lese_a_bas (double, int);
int lese_a_bas_erw2 (double);
int Auswahla_basQuery ();
int Auswahla_bas ();
int Showa_bas (char *);
