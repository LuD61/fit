#ifndef __PTAB_FUNC_DEF
#define __PTAB_FUNC_DEF
#endif

#define CONSOLE
#include "DbClass.h"
#include "ptab.h"

class PtabFunc : public DB_CLASS
{
     private :
		 PTAB_CLASS Ptab;
		 int max_cursor;
		 int ptk_update;
		 int ptn_select;
		 int ptn_insert;

     public :
        PtabFunc ();
		~PtabFunc ();
		void Prepare ();
		void Append (char *, char *);
};
