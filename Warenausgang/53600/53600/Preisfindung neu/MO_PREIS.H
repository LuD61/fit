/*
-----------------------------------------------------------------------------
-
-	BSA Bizerba Software- und Automationssysteme GmbH
-	Geisental 12, 4630 Bochum 1, Tel: 0234/5065-250
-
-----------------------------------------------------------------------------

/* Eingabestruktur fuer preis_hol          */
#include "ipr.h"
#include "iprz.h"
#include "akt_ipr.h"
#include "akt_iprz.h"
#include "dbclass.h"

extern int _WMDEBUG;
extern int writelog (char *, ...);
extern void disp_mess (char *, int modus);


typedef struct 
         {short mdn;
          short fil;
          short kun_fil;
          int kun;
          double a;
          char datum [11];
         } PREISIN;

/* Ausgabestruktur fuer Preishol          */

typedef struct
         {short sa;
          double pr_ek;
          double pr_vk;
         } PREISOUT;


class WA_PREISE
{
         private :
			  BOOL Optimize;
			  long aktionlen;
              short dstat;
              short dret_stat;
              char  sa_flag [2];
              char  sysdatum [11];
              int dsqlstatus;
              int fetch_akt;
              int fetch_pos_dat;
              int fetch_kopf_dat;
              int fetch_akt_nr;
              int test_termin;
              int fetch_kun_akt;
              int kun_akt_pos;
              int kun_akt_kopf;
              short tauschen;
              int kun_akt_posk;
              int kun_akt_kopfk;
			  char kond_art [5];
			  double a_grund;
              int tst;
              int zustell_par;
              int bg_cc_par;
			  BOOL sysparOK;
			  short auf_art;
		      double plusrab;
			  double pr_ek1;

              double akt_me;
			  
			  DB_CLASS DbClass;
              MDN_CLASS mdn_class;
              FIL_CLASS fil_class;
              KUN_CLASS kun_class;
              AB_PREISE ab_preise;
			  IPR_CLASS ipr_class;
			  IPRZ_CLASS iprz_class;
			  AKT_IPR_CLASS akt_ipr_class;
			  AKT_IPRZ_CLASS akt_iprz_class;


			  void ReadSyspar (void);
              int fetch_gruppen (short, short, long *,
                                        short *, long *, short *);
              int fetch_ku_preis (short, long, short, long,
                                  double, short *, double *, double *);
              int fetch_std_preis (short, short, long, double);
              int fetch_iv_pr (short, short, long, long, double);
              double kleinste_aktion (void);
              int fetchprstakt (long);
              int fetchlskunakt (long);
              int fetchkunakt (long);
              int prep_akt (void);
              double GetEk (double);
              int debug (char *, ...);
              int fetch_ku_preis_bgcc (short, long, short, long,
                                       double, short *, double *, double *);
              int fetch_ku_preis_zust (short, long, short, long,
                                       double, short *, double *, double *);
              int fetch_ku_bestp_ab (short, long, short, long,
                                       double, short *, double *, double *);
              int fetch_ku_rab_ab (short, long, short, long,
                                       double, short *, double *, double *);
              int fetch_ku_bestp_zust (short, long, short, long,
                                       double, short *, double *, double *);
              int fetch_ku_ek_ab (short, long, short, long,
                                       double, short *, double *, double *);
              void test_waehrung (void);
              void TestNullPreis (short, double, double *, double *);

         public :
              static BOOL QuickSearch;
              WA_PREISE ()
              {
				        strcpy (kond_art, ""); 
                        tauschen = 0;
                        tst = -1;
						Optimize = TRUE;
						zustell_par = 0;
						bg_cc_par = 0;
						sysparOK = FALSE;
						auf_art = 0;
                        akt_me = 0.0;
 			  }

              void SetAktMe (double akt_me)
              {
                  this->akt_me = akt_me;
              }

              double GetAktMe (void)
              {
                  return akt_me;
              }

			  void SetAufArt (short auf_art)
			  {
				  this->auf_art = auf_art;
			  }

			  long GetAktionLen ()
			  {
				  return aktionlen;
			  }

			  void SetOptimize (BOOL mode)
			  {
				  Optimize = max(0, min (1, mode));
			  }
			  char *GetKondArt (void)
			  {
				  return kond_art;
			  }
			  double GetAGrund ()
			  {
				  return a_grund;
			  }
              int preis_hol (PREISIN *, PREISOUT *);
              int preise_holen (short, short, short,
                                long, double, char *,
                                short *, double *, double *);
              BOOL MinMaxOk (double, double);
};
