#include <windows.h>
#include <string.h> 
#include "ddeclient.h"

DWORD DdeClient::idInst = NULL;
HWND DdeClient::hWnd = NULL;
char DdeClient::Ergebnis[512];
char DdeClient::MText[512];

DdeClient::DdeClient (char *Service, char *Topic)
{
	strcpy (this->Service, Service);
	strcpy (this->Topic,   Topic);
	strcpy (Item, "start");
}

DdeClient::~DdeClient ()
{
	Stop (NULL);
	if (idInst != NULL)
	{
		DdeUninitialize (idInst);
		idInst = NULL;
	}
}

BOOL DdeClient::Init (HWND hWndParent)
{
	hWnd = hWndParent;
	if (DdeInitialize (&idInst, (PFNCALLBACK) &DdeCallback,
					   APPCLASS_STANDARD | APPCMD_CLIENTONLY, 0l) != 0)
//					   APPCLASS_STANDARD, 0l) != 0)
	{
		idInst = NULL;
		return FALSE;
	}
	return TRUE;
}

BOOL DdeClient::Start (char *ParamString)
{
    HSZ hszService;
    HSZ hszTopic;
	HSZ hszItem;

	if (idInst == NULL)
	{
		return FALSE;
	}

    hszService = DdeCreateStringHandle (idInst, Service, 0);
	hszTopic   = DdeCreateStringHandle (idInst, Topic, 0);

	hConv = DdeConnect (idInst, hszService, hszTopic, NULL);

	DdeFreeStringHandle (idInst, hszService);
	DdeFreeStringHandle (idInst, hszTopic);
	if (hConv == NULL)
	{
		return FALSE;
	}

	strcpy (Item, "start");
    hszItem   = DdeCreateStringHandle (idInst, Item, 0);

    HDDEDATA hData = DdeClientTransaction ((LPBYTE) ParamString, strlen (ParamString) + 1, hConv, hszItem,CF_TEXT,
                                            XTYP_POKE, DDE_TIMEOUT, NULL);
	if (hData != NULL)
	{
		DdeFreeDataHandle (hData);
	}
    DdeClientTransaction (NULL, 0, hConv, hszItem,CF_TEXT,
                          XTYP_ADVSTART | XTYPF_ACKREQ, DDE_TIMEOUT, NULL);
    DdeFreeStringHandle (idInst, hszItem);
	return TRUE;
}


void DdeClient::TerminateServer ()
{
    HSZ hszService;
    HSZ hszTopic;
	HSZ hszItem;

	if (idInst == NULL)
	{
		return;
	}

    hszService = DdeCreateStringHandle (idInst, Service, 0);
	hszTopic   = DdeCreateStringHandle (idInst, Topic, 0);

	hConv = DdeConnect (idInst, hszService, hszTopic, NULL);

	DdeFreeStringHandle (idInst, hszService);
	DdeFreeStringHandle (idInst, hszTopic);
	if (hConv == NULL)
	{
		return;
	}
	strcpy (Item, "terminate");
    hszItem   = DdeCreateStringHandle (idInst, Item, 0);

    HDDEDATA hData = DdeClientTransaction ((LPBYTE) "Command=T", strlen ("Command=T") + 1, hConv, hszItem,CF_TEXT,
                                            XTYP_POKE, DDE_TIMEOUT, NULL);
	DdeFreeStringHandle (idInst, hszItem);
	if (hData != NULL)
	{
		DdeFreeDataHandle (hData);
	}
	Stop (NULL);
}


void DdeClient::Stop (char *ParamString)
{
	HSZ hszItem;
	strcpy (Item, "start");
    hszItem   = DdeCreateStringHandle (idInst, Item, 0);
    DdeClientTransaction (NULL, 0, hConv, hszItem,CF_TEXT,
                          XTYP_ADVSTOP, DDE_TIMEOUT, NULL);
	if (ParamString != NULL)
	{
             HDDEDATA hData = DdeClientTransaction ((LPBYTE) ParamString, strlen (ParamString) + 1, hConv, 
				                                     hszItem,CF_TEXT,
                                                     XTYP_POKE, DDE_TIMEOUT, NULL);
 	         if (hData != NULL)
			 {
		               DdeFreeDataHandle (hData);
			 }
	}
	DdeFreeStringHandle (idInst, hszItem);
	if (hConv != NULL)
	{
		DdeDisconnect (hConv);
		hConv = NULL;
	}
}


void DdeClient::GetErgebnis (HDDEDATA hData)
{
	DdeGetData (hData, (UCHAR *) Ergebnis, 512, 0);
	if (hWnd != NULL)
	{
			PostMessage (hWnd, WM_USER_ERGEBNIS, 0, (LPARAM) Ergebnis);
	}
}


HDDEDATA CALLBACK DdeClient::DdeCallback (UINT iType, UINT iFmt, HCONV hConv,
							              HSZ hsz1, HSZ hsz2, HDDEDATA hData,
							              DWORD dwData1, DWORD dwData2)
{
	char szItem [32];

	switch (iType)
	{
		 case XTYP_ADVDATA :
			 if (iFmt != CF_TEXT)
			 {
				 return (HDDEDATA) NULL;
			 }
			 DdeQueryString (idInst, hsz2, szItem, sizeof (szItem), 0);
			 if (strcmp (szItem, "start") == 0)
			 {
				 GetErgebnis (hData);
			 }
             return (HDDEDATA) DDE_FACK;
		 case XTYP_DISCONNECT :
			 hConv = NULL;
			 if (hWnd != NULL)
			 {
				 DestroyWindow (hWnd);
				 hWnd = NULL;
			 }
             return (HDDEDATA) TRUE;
	}
	return (HDDEDATA) NULL;
}

