#include <windows.h>
#include "mo_curso.h"
#include "bsd_buch.h"

struct BSD_BUCH bsd_buch, bsd_buch_null;

void BSD_BUCH_CLASS::prepare (void)
/**
Cursor fuer bsd_buch vorbereiten.
**/
{
    ins_quest ((char *) &bsd_buch.nr,2,0);
    ins_quest ((char *) bsd_buch.blg_typ,0,3);
    ins_quest ((char *) &bsd_buch.mdn,1,0);
    ins_quest ((char *) &bsd_buch.fil,1,0);
    ins_quest ((char *) &bsd_buch.kun_fil,1,0);
    ins_quest ((char *) &bsd_buch.a,3,0);
    ins_quest ((char *) &bsd_buch.dat,2,0);
    ins_quest ((char *) bsd_buch.zeit,0,9);
    ins_quest ((char *) bsd_buch.pers,0,13);
    ins_quest ((char *) bsd_buch.bsd_lgr_ort,0,13);
    ins_quest ((char *) &bsd_buch.qua_status,1,0);
    ins_quest ((char *) &bsd_buch.me,3,0);
    ins_quest ((char *) &bsd_buch.bsd_ek_vk,3,0);
    ins_quest ((char *) bsd_buch.chargennr,0,sizeof (bsd_buch.chargennr));
    ins_quest ((char *) bsd_buch.ident_nr,0,14);
    ins_quest ((char *) bsd_buch.herk_nachw,0,14);
    ins_quest ((char *) bsd_buch.lief,0,17);
    ins_quest ((char *) &bsd_buch.auf,2,0);
    ins_quest ((char *) bsd_buch.verfall,0,2);
    ins_quest ((char *) &bsd_buch.delstatus,1,0);
    ins_quest ((char *) bsd_buch.err_txt,0,17);
        ins_cursor = prepare_sql ("insert into bsd_buch (nr,  "
"blg_typ,  mdn,  fil,  kun_fil,  a,  dat,  zeit,  pers,  bsd_lgr_ort,  qua_status,  me,  "
"bsd_ek_vk,  chargennr,  ident_nr,  herk_nachw,  lief,  auf,  verfall,  delstatus,  "
"err_txt) "

#line 13 "bsd_buch.rpp"
                                  "values "
                                  "(?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?)");

#line 15 "bsd_buch.rpp"
}

int BSD_BUCH_CLASS::dbinsert (void)
/**
In bsd_buch einfuegen.
**/
{
        int dsqlstatus;
        if (ins_cursor == -1)
        {
                       prepare ();
        }
        dsqlstatus = execute_curs (ins_cursor);
        return dsqlstatus;
}

void BSD_BUCH_CLASS::dbclose (void)
/**
Cursor schliessen.
**/
{
         if (ins_cursor == -1) return;
         close_sql (ins_cursor);
         ins_cursor = -1;
}
       
           

