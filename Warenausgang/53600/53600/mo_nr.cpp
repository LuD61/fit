#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <time.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "mo_nr.h"

DB_CLASS DbClass;
static long nr;
static long tag;

long AutoNrClass::GetNr (long akt_nr)
/**
Naechste Textnummer holen.
**/
{
         time_t timer;
         int dsqlstatus;
         int cursor;
		 extern short sql_mode;
		 short sqls;

         if (akt_nr) return akt_nr;

		 sqls = sql_mode;
		 sql_mode = 1;
         time (&timer);
         tag = (long) timer;
         DbClass.sqlin ((long *) &tag, 2, 0);
         cursor = DbClass.sqlcursor ("select tag from aufpnr where tag = ?");
         dsqlstatus = DbClass.sqlfetch (cursor); 
         while (dsqlstatus == 0)
         {
                      Sleep (1000); 
                      time (&timer);
                      if (tag == (long) timer)
                      {
                                  continue;
                      } 
                      tag = (long) timer;
                      dsqlstatus = DbClass.sqlfetch (cursor); 
         }
		 if (dsqlstatus < 0) 
		 {
			 sql_mode = sqls;
			 return 0;
		 }
         DbClass.sqlclose (cursor);
         DbClass.sqlin ((long *) &tag, 2, 0);
         dsqlstatus = DbClass.sqlcomm ("insert into aufpnr (tag) values (?)");
		 if (dsqlstatus < 0) 
		 {
			 sql_mode = sqls;
			 return 0;
		 }
         DbClass.sqlin ((long *) &tag, 2, 0);
         DbClass.sqlout ((long *) &nr, 2, 0);
         nr = 0l;
         dsqlstatus = DbClass.sqlcomm ("select nr from aufpnr where tag = ?");
		 if (dsqlstatus < 0) 
		 {
			 sql_mode = sqls;
			 return 0;
		 }
         DbClass.sqlin ((long *) &tag, 2, 0);
         DbClass.sqlcomm ("delete from aufpnr where tag = ?");
         return nr; 
}

long AutoNrClass::GetNrLs (long akt_nr)
/**
Naechste Textnummer holen.
**/
{
         time_t timer;
         int dsqlstatus;
         int cursor;
		 extern short sql_mode;
		 short sqls;

         if (akt_nr) return akt_nr;

		 sqls = sql_mode;
		 sql_mode = 1;
         time (&timer);
         tag = (long) timer;
         DbClass.sqlin ((long *) &tag, 2, 0);
         cursor = DbClass.sqlcursor ("select tag from lspnr where tag = ?");
         dsqlstatus = DbClass.sqlfetch (cursor); 
         while (dsqlstatus == 0)
         {
                      Sleep (1000); 
                      time (&timer);
                      if (tag == (long) timer)
                      {
                                  continue;
                      } 
                      tag = (long) timer;
                      dsqlstatus = DbClass.sqlfetch (cursor); 
         }
		 if (dsqlstatus < 0) 
		 {
			 sql_mode = sqls;
			 return 0;
		 }
         DbClass.sqlclose (cursor);
         DbClass.sqlin ((long *) &tag, 2, 0);
         dsqlstatus = DbClass.sqlcomm ("insert into lspnr (tag) values (?)");
		 if (dsqlstatus < 0) 
		 {
			 sql_mode = sqls;
			 return 0;
		 }
         DbClass.sqlin ((long *) &tag, 2, 0);
         DbClass.sqlout ((long *) &nr, 2, 0);
         nr = 0l;
         dsqlstatus = DbClass.sqlcomm ("select nr from lspnr where tag = ?");
		 if (dsqlstatus < 0) 
		 {
			 sql_mode = sqls;
			 return 0;
		 }
         DbClass.sqlin ((long *) &tag, 2, 0);
         DbClass.sqlcomm ("delete from lspnr where tag = ?");
         return nr; 
}

long AutoNrClass::GetNveLfd (void)
/**
Naechste Textnummer holen.
**/
{
         time_t timer;
         int dsqlstatus;
         int cursor;
		 extern short sql_mode;
		 short sqls;


		 sqls = sql_mode;
		 sql_mode = 1;
         time (&timer);
         tag = (long) timer;
         DbClass.sqlin ((long *) &tag, 2, 0);
         cursor = DbClass.sqlcursor ("select tag from nvelfd where tag = ?");
         dsqlstatus = DbClass.sqlfetch (cursor); 
         while (dsqlstatus == 0)
         {
                      Sleep (1000); 
                      time (&timer);
                      if (tag == (long) timer)
                      {
                                  continue;
                      } 
                      tag = (long) timer;
                      dsqlstatus = DbClass.sqlfetch (cursor); 
         }
		 if (dsqlstatus < 0) 
		 {
			 sql_mode = sqls;
			 return 0;
		 }
         DbClass.sqlclose (cursor);
         DbClass.sqlin ((long *) &tag, 2, 0);
         dsqlstatus = DbClass.sqlcomm ("insert into nvelfd (tag) values (?)");
		 if (dsqlstatus < 0) 
		 {
			 sql_mode = sqls;
			 return 0;
		 }
         DbClass.sqlin ((long *) &tag, 2, 0);
         DbClass.sqlout ((long *) &nr, 2, 0);
         nr = 0l;
         dsqlstatus = DbClass.sqlcomm ("select nr from nvelfd where tag = ?");
		 if (dsqlstatus < 0) 
		 {
			 sql_mode = sqls;
			 return 0;
		 }
         DbClass.sqlin ((long *) &tag, 2, 0);
         DbClass.sqlcomm ("delete from nvelfd where tag = ?");
         return nr; 
}

