#ifndef _PALETTE_DEF
#define _PALETTE_DEF

#include <vector>
#include "Text.h"

class CPalette
{
	public:
	  std::vector<double> ArtList;
	  double a;
	  double brutto;
	  double netto;
	  double brutto_gew;
	  Text Charge;
	  Text Nve;
	  int count;
	  int NveLfd;

      CPalette ();
      CPalette (double);
      CPalette (double, LPSTR, int);
      CPalette (double, Text&);
      CPalette (CPalette&);
	  ~CPalette ();
	  void InitGew ();
	  void AddGew (double, double);
	  void AddGew (double, double, double);
};

class CWiegClass
{
public:
	  double a;
	  double brutto;
	  double netto;
	  double brutto_gew;
	  Text Charge;
	  int count;

	  CWiegClass ();
	  CWiegClass (double);
	  CWiegClass (CWiegClass&);
	  ~CWiegClass ();
	  void AddGew (double, double);
	  void AddGew (double, double, double);
      void SetGew (double brutto, double netto);
};

#endif
