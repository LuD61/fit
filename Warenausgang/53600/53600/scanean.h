#ifndef _SCANEAN_DEF
#define _SCANEAN_DEF
#include <string.h> 
#include "Text.h"
#include "Vector.h"

#ifndef BOOL
#define BOOL int
#define TRUE 1
#define FALSE 0
#endif

#define MAXSTRUCTS 100

#define EANGEW 1
#define EANPR  2
#define EANSTD 3
#define EANPR_GEW  4


class STRUCTEAN
{
     private :
           short Code;
           Text Type;
           short Len;
           BOOL AFromEan;
     public :
           STRUCTEAN (short Code, Text Type, short Len, BOOL AFromEan)
           {
                 this->Code = Code;
                 this->Type = Type;
                 this->Len  = Len;
                 if (AFromEan == 0)
                 {
                       this->AFromEan = FALSE;
                 }
                 else
                 {
                       this->AFromEan = TRUE;
                 }
           }

           void SetCode (short Code)
           {
                 this->Code = Code;
           }

           short GetCode (void)
           {
                 return Code;
           }
    
           void SetType (Text Type)
           {
                 this->Type = Type;
           }

           Text GetType (void)
           {
                 return Type;
           }

           void SetLen (short Len)
           {
                 this->Len = Len;
           }

           short GetLen (void)
           {
                 return Len;
           }

           void SetAFromEan (short AFromEan)
           {
                 if (AFromEan == 0)
                 {
                       this->AFromEan = FALSE;
                 }
                 else
                 {
                       this->AFromEan = TRUE;
                 }
           }

           short GetAFromEan (void)
           {
                 return AFromEan;
           }
};

class CEan128Date
{
	private :
	    int Day;
		int Month;
		int Year;
		Text Date;
	public :
		CEan128Date ();
		CEan128Date (Text&);
		void SetDate (Text&);
		CEan128Date& operator= (Text&);
		~CEan128Date ();
		void Init ();
		BOOL IsEmpty ();
		Text& ToString ();
};

class AiInfo
{
    public :
		Text Ai;
		int Len;
		CVector EndChar;
		int EndCharEdit;

		AiInfo (Text& Ai, int Len)
		{
			this->Ai = Ai;
			this->Len = Len;
			EndChar.Init();
			EndCharEdit = 0;
		}

		~AiInfo () 
		{
			EndChar.DestroyAll ();
		}
};


class STRUCTEAN128
{
     private :
           short Id;
           short Code;
		   short Fixedlen;
		   short Varlen;
           BOOL Pruefziffer;
		   CVector EndChar;
           int EndCharEdit;
     public :
           STRUCTEAN128 (short Id, short Code, short Fixedlen, 
			              short Varlen, BOOL Pruefziffer)
           {
               this->Id = Id;
               this->Code = Code;
               this->Fixedlen = Fixedlen;
               this->Varlen  = Varlen;
			   this->Pruefziffer = (Pruefziffer == 0) ? FALSE : TRUE;
			   EndChar.Init ();
			   EndCharEdit = 0; 
           }

		   ~STRUCTEAN128 ()
		   {
			   EndChar.DestroyAll ();
		   }

           void SetId (short Id)
           {
                 this->Id = Id;
           }

           short GetId (void)
           {
                 return Id;
           }

           void SetCode (short Code)
           {
                 this->Code = Code;
           }

           short GetCode (void)
           {
                 return Code;
           }
    
           void SetFixedlen (short Fixedlen)
           {
                 this->Fixedlen = Fixedlen;
           }

           short GetFixedlen (void)
           {
                 return Fixedlen;
           }

           void SetVarlen (short Varlen)
           {
                 this->Varlen = Varlen;
           }

           short GetVarlen (void)
           {
                 return Varlen;
           }

           void SetPruefziffer (BOOL Pruefziffer)
           {
			   this->Pruefziffer = (Pruefziffer == 0) ? FALSE : TRUE;
           }

           short GetPruefziffer (void)
           {
                 return Pruefziffer;
           }
		   void SetEndChar (Text);
};

class SCANEAN
{
      private :
           Text EanDb;
           Text EanGew;
           Text CfgName;
		   Text Varpart;
           STRUCTEAN *Structean[MAXSTRUCTS];           
           STRUCTEAN128 *Structean128[MAXSTRUCTS];           
		   Text *Result;
      public :
		   static SCANEAN *Scanean;
		   CVector AiInfos;
           SCANEAN ()
           {
                CfgName = "";
                for (int i = 0; i < MAXSTRUCTS; i ++)
                { 
                        Structean[i] = NULL;
                        Structean128[i] = NULL;
                }
				Result = NULL;
 			    AiInfos.Init ();
			    AiInfo *aiInfo = new AiInfo (Text ("01"), 14);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("15"), 6);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("13"), 6);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("310"), 7);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("10"), 20);
			    AiInfos.Add (aiInfo);
				Scanean = this;
           }    
           ~SCANEAN ()
           {
                for (int i = 0; i <  MAXSTRUCTS; i ++)
                {
                        if (Structean[i] == NULL)
                        {
                                delete Structean[i];
                                Structean[i] = NULL;
                        }
                        if (Structean128[i] == NULL)
                        {
                                delete Structean128[i];
                                Structean128[i] = NULL;
                        }
                 } 
				 AiInfos.FirstPosition ();
                 AiInfo *ai;
				 while ((ai = (AiInfo *) AiInfos.GetNext ()) != NULL)
				 {
					 delete ai;
				 }
           }    

           SCANEAN (Text& CfgName)
           {
                this->CfgName = CfgName;
                for (int i = 0; i < MAXSTRUCTS; i ++)
                { 
                        Structean[i] = NULL;
                        Structean128[i] = NULL;
                }
				Result = NULL;
				Result = NULL;
 			    AiInfos.Init ();
			    AiInfo *aiInfo = new AiInfo (Text ("01"), 14);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("15"), 6);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("13"), 6);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("3100"), 6);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("3101"), 6);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("3102"), 6);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("3103"), 6);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("3104"), 6);
			    AiInfos.Add (aiInfo);
			    aiInfo = new AiInfo (Text ("10"), 20);
			    AiInfos.Add (aiInfo);
           }

           void SetCfgName (Text& CfgName)    
           {
                this->CfgName = CfgName;
           }

           Text& GetCfgName (void)
           {
                return CfgName;
           }
           BOOL Read (void);
           STRUCTEAN *GetStructean (Text&);
           STRUCTEAN128 *GetStructean128 (Text&);
           int GetType (Text&);
           int GetLen (Text&);
           int GetCode (Text&);
           char *GetEanForDb (Text Ean);
           char *GetEanForDb (Text Ean, int len);
           char *GetEanGew (Text);
           char *GetEan128Varpart (Text&);
           BOOL ReadStandard (FILE *, char *);
           BOOL ReadEan128 (FILE *, char *);
           BOOL ReadEan128Ai (FILE *, char *);
		   void SetAi (Text&, int);
		   Text* GetEan128Ai (Text&, Text&); 
           void SetAiEndChar (Text&, Text&);
           void SetAiEndCharEdit (Text&, Text&);
           BOOL IsEndChar (AiInfo *,int);
           static int ChangeChar (int);
};

#endif