#ifndef _WriteNve_Def
#define _WriteNve_Def
#include "Text.h"
#include "lsnve.h"

class WriteNve 
{
      private :
	   long nve_posi;
	   Text nve;
	   short mdn;
	   short fil;
	   double me;
	   short me_einh;
	   LSNVE_CLASS Lsnve;
	   
       public :
		  Text nve_palette;
		  long ls;
		  double a;
		  BOOL Save;
	      long mhd;
          void SetMhd (LPSTR); 
          long GetMhd (); 
		  void SetMe (double);
		  void SetNve (Text&);
		  void SetNvePalette (Text&);
          void SetNvePosi (long);
		  WriteNve ();
		  ~WriteNve ();
          WriteNve (Text&, Text&, short, short, long, double, double, short);
		  long GenNvePosi ();
		  long GenNvePosiGes ();
		  void write ();
		  void write (short);
		  void write (short, double);
          void writePalettePosGue ();
		  void writePalette (short);
		  void writeGue (short);
		  BOOL Equals (WriteNve *writeNve);
		  BOOL Equals (LPSTR nve_palette, long ls, double a);
};
#endif