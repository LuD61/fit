#include <windows.h>
#include <stdio.h>
#include "PtabFunc.h"
#include "strfkt.h"
#include "mo_curso.h"


PtabFunc::PtabFunc () : DB_CLASS (), 
          max_cursor (-1), ptk_update (-1), ptn_insert (-1),
		  ptn_select (-1)
{	
}

PtabFunc::~PtabFunc ()
{
	if (max_cursor != -1)
	{
		sqlclose (max_cursor);
	}
	if (ptk_update != -1)
	{
		sqlclose (ptk_update);
	}
	if (ptn_insert != -1)
	{
		sqlclose (ptn_insert);
	}
	if (ptn_select != -1)
	{
		sqlclose (ptn_select);
	}
}

void PtabFunc::Prepare ()
{
	sqlin ((char *) ptabn.ptitem, 0, sizeof (ptabn.ptitem));
	sqlin ((char *) ptabn.ptbez, 0, sizeof (ptabn.ptbez));
	ptn_select = sqlcursor ("select ptlfnr from ptabn where ptitem = ? and ptbez = ?");

	sqlin ((char *) ptabn.ptitem, 0, sizeof (ptabn.ptitem));
	sqlout ((long *) &ptabn.ptlfnr, 2, 0);
	max_cursor = sqlcursor ("select max (ptlfnr) from ptabn where ptitem = ?");

	sqlin ((long *) &ptabn.ptlfnr, 2, 0);
	sqlin ((char *) ptabn.ptitem, 0, sizeof (ptabn.ptitem));
	ptk_update = sqlcursor ("update ptabk set ptanzpos = ? where ptitem = ?"); 

	sqlin ((char *) ptabn.ptitem, 0, sizeof (ptabn.ptitem));
	sqlin ((char *) ptabn.ptwert, 0, sizeof (ptabn.ptwert));
	sqlin ((long *) &ptabn.ptlfnr, 2, 0);
	sqlin ((char *) ptabn.ptbez, 0, sizeof (ptabn.ptbez));
	sqlin ((char *) ptabn.ptbezk, 0, sizeof (ptabn.ptbezk));
	sqlin ((char *) ptabn.ptwer1, 0, sizeof (ptabn.ptwer1));
	sqlin ((char *) ptabn.ptwer2, 0, sizeof (ptabn.ptwer2));
	sqlin ((char *) ptabn.pers_nam, 0, sizeof (ptabn.pers_nam));
	sqlin ((char *) &ptabn.delstatus, 1, 0);
	sqlin ((char *) &ptabn.akv, 2, 0);
	sqlin ((char *) &ptabn.bearb, 2, 0);
	ptn_insert = sqlcursor ("insert into ptabn "
		                    "(ptitem, ptwert, ptlfnr,ptbez,"
							"ptbezk,ptwer1, ptwer2, pers_nam,"
							"delstatus, akv, bearb) " 
							"values "
							"(?,?,?,?,?,?,?,?,?,?,?)"); 
}

void PtabFunc::Append (char *Item, char *ptbez)
{
	char datum [12];
	
	if (max_cursor == -1)
	{
		Prepare ();
	}
 
	memcpy (&ptabn, &ptabn_null, sizeof (ptabn));
	strcpy (ptabn.ptitem, Item); 
	strcpy (ptabn.ptbez, ptbez);
    sqlopen (ptn_select);
	if (sqlfetch (ptn_select) == 0)
	{
		return;
	}

	beginwork ();
	ptabn.ptlfnr = 0;
	sqlopen (max_cursor);
	sqlfetch (max_cursor);
	ptabn.ptlfnr ++;
	sprintf (ptabn.ptwert, "%ld", ptabn.ptlfnr);
	strcpy (ptabn.ptbez, ptbez);
	strncpy (ptabn.ptbezk, ptbez, 8);
	ptabn.ptbezk[8] = 0;
    sqlexecute (ptk_update);
	strcpy (ptabn.pers_nam, "53700");
	sysdate (datum);
	ptabn.akv = dasc_to_long (datum);
	ptabn.bearb = ptabn.akv;
	sqlexecute (ptn_insert);
	commitwork ();
}


	


	


