#ifndef _KUN_LEERG_DEF
#define _KUN_LEERG_DEF
#include "dbclass.h"

struct KUN_LEERG {
   short     delstatus;
   short     mdn;
   short     fil;
   short     kun_fil;
   long      kun;
   double    a;
   long      bsd_stk;
   double    bsd_wrt;
   long      lief_dat;
   long      ret_dat;
   char      ben_lief[17];
   char      ben_ret[17];
};
extern struct KUN_LEERG kun_leerg, kun_leerg_null;

#line 6 "kun_leerg.rh"

class KUN_LEERG_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               KUN_LEERG kun_leerg;
               KUN_LEERG_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (KUN_LEERG&);  
};
#endif
