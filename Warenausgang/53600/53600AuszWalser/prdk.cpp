#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <tchar.h>
#include "wmask.h"
#include "strfkt.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "prdk.h"

struct PRDK_K prdk_k, prdk_k_null;

void PRDK_K_CLASS::prepare_prdk_k (void)
{
    ins_quest ((char *) &prdk_k.a,  3, 0);
    out_quest ((char *) &prdk_k.mdn,1,0);
    out_quest ((char *) &prdk_k.a,3,0);
    out_quest ((char *) prdk_k.rez,0,9);
    out_quest ((char *) prdk_k.rez_bz,0,73);
    out_quest ((char *) prdk_k.kalk_stat,0,2);
    out_quest ((char *) prdk_k.grbrt,0,2);
    out_quest ((char *) prdk_k.leits,0,10);
    out_quest ((char *) &prdk_k.chg_gew,3,0);
    out_quest ((char *) &prdk_k.varb_gew,3,0);
    out_quest ((char *) &prdk_k.zut_gew,3,0);
    out_quest ((char *) &prdk_k.huel_gew,3,0);
    out_quest ((char *) &prdk_k.tr_sw,3,0);
    out_quest ((char *) &prdk_k.is_befe_abs,3,0);
    out_quest ((char *) &prdk_k.is_befe_rel,3,0);
    out_quest ((char *) &prdk_k.is_be_rel,3,0);
    out_quest ((char *) &prdk_k.is_fett,3,0);
    out_quest ((char *) &prdk_k.is_fett_tol,3,0);
    out_quest ((char *) &prdk_k.is_fett_fe,3,0);
    out_quest ((char *) &prdk_k.is_f_h2o,3,0);
    out_quest ((char *) &prdk_k.varb_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.varb_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.varb_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.zut_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.zut_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.zut_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.huel_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.huel_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.huel_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.rez_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.rez_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.dat,2,0);
    out_quest ((char *) &prdk_k.sw_kalk,3,0);
    out_quest ((char *) &prdk_k.bto_gew,3,0);
    out_quest ((char *) &prdk_k.nto_gew,3,0);
    out_quest ((char *) prdk_k.bearb_weg_p,0,5);
    out_quest ((char *) &prdk_k.bto_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.bto_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.bto_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.nto_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.nto_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.nto_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.a_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.a_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.delstatus,1,0);
    out_quest ((char *) &prdk_k.rez_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.nto_berech_gew,3,0);
    out_quest ((char *) &prdk_k.bearb_weg,3,0);
    out_quest ((char *) prdk_k.bru,0,2);
    out_quest ((char *) &prdk_k.sw,3,0);
    out_quest ((char *) &prdk_k.variante,1,0);
    out_quest ((char *) prdk_k.variante_bz,0,25);
    out_quest ((char *) &prdk_k.akv,1,0);
    out_quest ((char *) prdk_k.prodphase,0,21);
    out_quest ((char *) &prdk_k.masch_nr1,2,0);
    out_quest ((char *) &prdk_k.masch_nr2,2,0);
    out_quest ((char *) &prdk_k.masch_nr3,2,0);
    out_quest ((char *) &prdk_k.masch_nr4,2,0);
    out_quest ((char *) &prdk_k.masch_nr5,2,0);
    out_quest ((char *) &prdk_k.masch_nr6,2,0);
    out_quest ((char *) &prdk_k.masch_nr7,2,0);
    out_quest ((char *) &prdk_k.masch_nr8,2,0);
    out_quest ((char *) &prdk_k.masch_nr9,2,0);
    out_quest ((char *) &prdk_k.masch_nr10,2,0);
    out_quest ((char *) &prdk_k.huel_wrt,3,0);
    out_quest ((char *) &prdk_k.vpk_wrt,3,0);
    out_quest ((char *) &prdk_k.huel_hk_wrt,3,0);
    out_quest ((char *) &prdk_k.vpk_hk_wrt,3,0);
    out_quest ((char *) &prdk_k.kost_gew,3,0);
    out_quest ((char *) &prdk_k.kost_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.kost_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.kost_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.vpk_gew,3,0);
    out_quest ((char *) &prdk_k.vpk_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.vpk_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.vpk_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.nvpk_mat_o_b,3,0);
    out_quest ((char *) &prdk_k.nvpk_hk_teilk,3,0);
    out_quest ((char *) &prdk_k.nvpk_hk_vollk,3,0);
    out_quest ((char *) &prdk_k.chg_anz_bzg,1,0);
    out_quest ((char *) &prdk_k.kalk_me,3,0);
    out_quest ((char *) &prdk_k.chg_anz_kalk,1,0);
    out_quest ((char *) &prdk_k.pers_wrt_bzg,3,0);
    out_quest ((char *) &prdk_k.pers_wrt_kalk,3,0);
    out_quest ((char *) &prdk_k.kutter_gew,3,0);
    out_quest ((char *) &prdk_k.chargierung,1,0);
    out_quest ((char *) &prdk_k.manr,2,0);
    out_quest ((char *) &prdk_k.a_sk_teilk,3,0);
    out_quest ((char *) &prdk_k.a_sk_vollk,3,0);
    out_quest ((char *) &prdk_k.a_filek_teilk,3,0);
    out_quest ((char *) &prdk_k.a_filek_vollk,3,0);
    out_quest ((char *) &prdk_k.a_filvk_teilk,3,0);
    out_quest ((char *) &prdk_k.a_filvk_vollk,3,0);
    out_quest ((char *) &prdk_k.sk_wrt,3,0);
    out_quest ((char *) &prdk_k.filek_wrt,3,0);
    out_quest ((char *) &prdk_k.filvk_wrt,3,0);
    out_quest ((char *) &prdk_k.bearb_weg_sk,3,0);
    out_quest ((char *) &prdk_k.bearb_weg_fek,3,0);
    out_quest ((char *) &prdk_k.bearb_weg_fvk,3,0);
    out_quest ((char *) &prdk_k.masch1_zeit,1,0);
    out_quest ((char *) &prdk_k.masch2_zeit,1,0);
    out_quest ((char *) &prdk_k.masch3_zeit,1,0);
    out_quest ((char *) &prdk_k.masch4_zeit,1,0);
    out_quest ((char *) &prdk_k.masch5_zeit,1,0);
    out_quest ((char *) &prdk_k.masch6_zeit,1,0);
    out_quest ((char *) &prdk_k.masch7_zeit,1,0);
    out_quest ((char *) &prdk_k.masch8_zeit,1,0);
    out_quest ((char *) &prdk_k.masch9_zeit,1,0);
    out_quest ((char *) &prdk_k.masch10_zeit,1,0);
    out_quest ((char *) &prdk_k.vorlaufzeit,1,0);
    out_quest ((char *) &prdk_k.varb_gew_nto,3,0);
    out_quest ((char *) &prdk_k.rework,1,0);
    out_quest ((char *) &prdk_k.prod_abt,1,0);
    out_quest ((char *) &prdk_k.anz_pers,2,0);
    cursor_prdk_k = prepare_sql (_T("select prdk_k.mdn,  "
"prdk_k.a,  prdk_k.rez,  prdk_k.rez_bz,  prdk_k.kalk_stat,  prdk_k.grbrt,  "
"prdk_k.leits,  prdk_k.chg_gew,  prdk_k.varb_gew,  prdk_k.zut_gew,  "
"prdk_k.huel_gew,  prdk_k.tr_sw,  prdk_k.is_befe_abs,  "
"prdk_k.is_befe_rel,  prdk_k.is_be_rel,  prdk_k.is_fett,  "
"prdk_k.is_fett_tol,  prdk_k.is_fett_fe,  prdk_k.is_f_h2o,  "
"prdk_k.varb_mat_o_b,  prdk_k.varb_hk_teilk,  prdk_k.varb_hk_vollk,  "
"prdk_k.zut_mat_o_b,  prdk_k.zut_hk_teilk,  prdk_k.zut_hk_vollk,  "
"prdk_k.huel_mat_o_b,  prdk_k.huel_hk_teilk,  prdk_k.huel_hk_vollk,  "
"prdk_k.rez_hk_teilk,  prdk_k.rez_hk_vollk,  prdk_k.dat,  "
"prdk_k.sw_kalk,  prdk_k.bto_gew,  prdk_k.nto_gew,  prdk_k.bearb_weg_p,  "
"prdk_k.bto_mat_o_b,  prdk_k.bto_hk_teilk,  prdk_k.bto_hk_vollk,  "
"prdk_k.nto_mat_o_b,  prdk_k.nto_hk_teilk,  prdk_k.nto_hk_vollk,  "
"prdk_k.a_hk_teilk,  prdk_k.a_hk_vollk,  prdk_k.delstatus,  "
"prdk_k.rez_mat_o_b,  prdk_k.nto_berech_gew,  prdk_k.bearb_weg,  "
"prdk_k.bru,  prdk_k.sw,  prdk_k.variante,  prdk_k.variante_bz,  "
"prdk_k.akv,  prdk_k.prodphase,  prdk_k.masch_nr1,  prdk_k.masch_nr2,  "
"prdk_k.masch_nr3,  prdk_k.masch_nr4,  prdk_k.masch_nr5,  "
"prdk_k.masch_nr6,  prdk_k.masch_nr7,  prdk_k.masch_nr8,  "
"prdk_k.masch_nr9,  prdk_k.masch_nr10,  prdk_k.huel_wrt,  "
"prdk_k.vpk_wrt,  prdk_k.huel_hk_wrt,  prdk_k.vpk_hk_wrt,  "
"prdk_k.kost_gew,  prdk_k.kost_mat_o_b,  prdk_k.kost_hk_teilk,  "
"prdk_k.kost_hk_vollk,  prdk_k.vpk_gew,  prdk_k.vpk_mat_o_b,  "
"prdk_k.vpk_hk_teilk,  prdk_k.vpk_hk_vollk,  prdk_k.nvpk_mat_o_b,  "
"prdk_k.nvpk_hk_teilk,  prdk_k.nvpk_hk_vollk,  prdk_k.chg_anz_bzg,  "
"prdk_k.kalk_me,  prdk_k.chg_anz_kalk,  prdk_k.pers_wrt_bzg,  "
"prdk_k.pers_wrt_kalk,  prdk_k.kutter_gew,  prdk_k.chargierung,  "
"prdk_k.manr,  prdk_k.a_sk_teilk,  prdk_k.a_sk_vollk,  "
"prdk_k.a_filek_teilk,  prdk_k.a_filek_vollk,  prdk_k.a_filvk_teilk,  "
"prdk_k.a_filvk_vollk,  prdk_k.sk_wrt,  prdk_k.filek_wrt,  "
"prdk_k.filvk_wrt,  prdk_k.bearb_weg_sk,  prdk_k.bearb_weg_fek,  "
"prdk_k.bearb_weg_fvk,  prdk_k.masch1_zeit,  prdk_k.masch2_zeit,  "
"prdk_k.masch3_zeit,  prdk_k.masch4_zeit,  prdk_k.masch5_zeit,  "
"prdk_k.masch6_zeit,  prdk_k.masch7_zeit,  prdk_k.masch8_zeit,  "
"prdk_k.masch9_zeit,  prdk_k.masch10_zeit,  prdk_k.vorlaufzeit,  "
"prdk_k.varb_gew_nto,  prdk_k.rework,  prdk_k.prod_abt,  "
"prdk_k.anz_pers from prdk_k where a = ? "));

#line 19 "prdk.rpp"
}

int PRDK_K_CLASS::lese_prdk_k (double a)
{
    long sqlstatus1;
    if (cursor_prdk_k == -1)
    {
        prepare_prdk_k ();
    }
    prdk_k.a = a;
    open_sql (cursor_prdk_k);
    fetch_sql (cursor_prdk_k);
    sqlstatus1 = sqlstatus;
    if ( sqlstatus1 )
    {
        prdk_k.rez_bz[0] = '\0';
    }
    else
    {
        clipped( prdk_k.rez_bz );
    }
    writelog ("prdk_k.a=%.0lf sqlstatus=%ld", prdk_k.a, sqlstatus1);
    close_sql (cursor_prdk_k);  
	cursor_prdk_k = -1;
    return sqlstatus1;
}

