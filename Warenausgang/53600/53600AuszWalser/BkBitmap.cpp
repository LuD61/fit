#include <windows.h>
#include "BkBitmap.h"

CBkBitmap::CBkBitmap(void)
{
	parts = 120;
	m_BmColor =  RGB (223, 227, 236);
	m_DynStep = Normal;
	m_Planes = 32;
	m_Bitmap = NULL;
}

CBkBitmap::~CBkBitmap(void)
{
}

HBITMAP  CBkBitmap::Create ()
{
	HDC hdc;
	HDC memDC;
	RECT rect;

	hdc = GetDC (NULL);
	CreateBitmap (m_Width, m_Height, m_Planes, 0, NULL);
	m_Bitmap = CreateCompatibleBitmap (hdc, m_Width, m_Height);
	memDC = CreateCompatibleDC (hdc);
	SelectObject (memDC, m_Bitmap);

	rect.left = 0;
	rect.top = 0;
	rect.right = m_Width;
	rect.bottom = m_Height;

	FillRectParts (memDC, m_BmColor, rect);

	return m_Bitmap;
}

void CBkBitmap::FillVRectParts (HDC hdc, COLORREF BkColor, RECT rect)
{
	int blue = (BkColor >> 16) & 0xFF;
	int green = (BkColor >> 8) & 0xFF;
	int red = (BkColor) & 0xFF;
	int height = rect.bottom - rect.top;
    int parts = 10;
	if (height > 50)
	{
		parts = 70;
	}
	else
	{
	    parts = 15;
	}
	int colordiff = 5;
	int part = (rect.bottom) / parts;
	RECT rect1 = {rect.left, rect.top, rect.right, part}; 
	int redplus = 240 - red;
	int greenplus = 240 - green;
	int blueplus = 240 - blue;
	int redstep = redplus / parts;
	int greenstep = greenplus / parts;
	int bluestep = blueplus / parts;
	red = min (red + redplus, 240);
	green = min (green + greenplus, 240);
	blue = min (blue + blueplus, 240);
	if (part == 1) 
	{
		part = 2;
	}

	for (int i = 0; i < parts; i ++)
	{
		HBRUSH h = CreateSolidBrush (RGB (red, green, blue));
		FillRect (hdc, &rect1, h);
		rect1.top += part - 1;
		rect1.bottom += part;
		if (rect1.top >= rect.bottom - part) return;
		red -= redstep;
		green -= greenstep;
		blue -= bluestep;
	}
	rect1.bottom = rect.bottom; 
	HBRUSH h = CreateSolidBrush (RGB (red, green, blue));
	FillRect (hdc, &rect1, h);
}

void CBkBitmap::FillRectParts (HDC hdc, COLORREF BkColor, RECT rect)
{
	HBRUSH hBrush;

	int blue = (BkColor >> 16) & 0xFF;
	int green = (BkColor >> 8) & 0xFF;
	int red = (BkColor) & 0xFF;
	int part = (rect.right) / parts;
	if (part < 1)
	{
		HBRUSH hBrush = CreateSolidBrush (RGB (red, green, blue));
		FillRect (hdc, &rect, hBrush);
		return;
	}

	int colordiff = 3;
	part ++;

	RECT rect1 = {rect.left, rect.top, part, rect.bottom}; 

	if (m_DynStep == Normal)
	{
		for (int i = 0; i < parts / 2; i++ )
		{
			hBrush = CreateSolidBrush (RGB (red, green, blue));
			FillRect (hdc, &rect1, hBrush);
			DeleteObject (hBrush);
			rect1.left += part - 1;
			rect1.right += part + 1;
			if (red <= 255 - colordiff) red += colordiff;
			if (green <= 255 - colordiff) green += colordiff;
			if (blue <= 255 - colordiff) blue += colordiff;
		}
	}
	else if (m_DynStep == Slow)
	{
		for (int i = 0; i < parts / 3; i ++)
		{
			hBrush = CreateSolidBrush (RGB (red, green, blue));
			FillRect (hdc, &rect1, hBrush);
			DeleteObject (hBrush);
			rect1.left += part - 1;
			rect1.right += part;
			if (red <= 255 - colordiff) red += colordiff;

			hBrush = CreateSolidBrush (RGB (red, green, blue));
			FillRect (hdc, &rect1, hBrush);
			DeleteObject (hBrush);

			rect1.left += part - 1;
			rect1.right += part;
			if (green <= 255 - colordiff) green += colordiff;
			hBrush = CreateSolidBrush (RGB (red, green, blue));
			FillRect (hdc, &rect1, hBrush);
			DeleteObject (hBrush);

			rect1.left += part - 1;
			rect1.right += part;
			if (blue <= 255 - colordiff) blue += colordiff;
		}
	}

	rect1.left += part - 1;
	rect1.right = rect.right; 
	hBrush = CreateSolidBrush (RGB (red, green, blue));
	FillRect (hdc, &rect1, hBrush);
	DeleteObject (hBrush);

}

void  CBkBitmap::ReadFromWindow (HWND hWnd, int x, int y, int cx, int cy)
{
	HDC hdc;
	HDC memDC;

	hdc = GetDC (hWnd);
	CreateBitmap (cx, cy, m_Planes, 0, NULL);
	m_Bitmap = CreateCompatibleBitmap (hdc, cx, cy);
	memDC = CreateCompatibleDC (hdc);
	SelectObject (memDC, m_Bitmap);

    BitBlt (memDC, 0, 0, cx, cy,
            hdc, x, y, SRCCOPY); 
    DeleteDC (memDC); 
	ReleaseDC (hWnd, hdc);
}

void  CBkBitmap::WriteToWindow (HWND hWnd, int x, int y, int cx, int cy)
{
	HDC hdc;
	HDC memDC;

	hdc = GetDC (hWnd);
	CreateBitmap (cx, cy, m_Planes, 0, NULL);
	memDC = CreateCompatibleDC (hdc);
	SelectObject (memDC, m_Bitmap);

    BitBlt (hdc, x, y, cx, cy,
            memDC, 0, 0, SRCCOPY); 
    DeleteDC (memDC); 
	ReleaseDC (hWnd, hdc);
}
