#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <math.h>
#include "wmask.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "strfkt.h"
#include "mdn.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "a_kun_gx.h"

struct A_KUN_GX a_kun_gx, a_kun_gx_null;

void A_KUN_GX_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &a_kun_gx.mdn, 1, 0);
            ins_quest ((char *) &a_kun_gx.fil, 1, 0);
            ins_quest ((char *) &a_kun_gx.kun, 2, 0);
            ins_quest ((char *) &a_kun_gx.a,   3, 0);
    out_quest ((char *) &a_kun_gx.mdn,1,0);
    out_quest ((char *) &a_kun_gx.fil,1,0);
    out_quest ((char *) &a_kun_gx.kun,2,0);
    out_quest ((char *) &a_kun_gx.a,3,0);
    out_quest ((char *) a_kun_gx.a_kun,0,26);
    out_quest ((char *) a_kun_gx.a_bz1,0,25);
    out_quest ((char *) &a_kun_gx.me_einh_kun,1,0);
    out_quest ((char *) &a_kun_gx.inh,3,0);
    out_quest ((char *) a_kun_gx.kun_bran2,0,3);
    out_quest ((char *) &a_kun_gx.tara,3,0);
    out_quest ((char *) &a_kun_gx.ean,3,0);
    out_quest ((char *) a_kun_gx.a_bz2,0,25);
    out_quest ((char *) &a_kun_gx.hbk_ztr,1,0);
    out_quest ((char *) &a_kun_gx.kopf_text,2,0);
    out_quest ((char *) a_kun_gx.pr_rech_kz,0,2);
    out_quest ((char *) a_kun_gx.modif,0,2);
    out_quest ((char *) &a_kun_gx.text_nr,2,0);
    out_quest ((char *) &a_kun_gx.devise,1,0);
    out_quest ((char *) a_kun_gx.geb_eti,0,2);
    out_quest ((char *) &a_kun_gx.geb_fill,1,0);
    out_quest ((char *) &a_kun_gx.geb_anz,2,0);
    out_quest ((char *) a_kun_gx.pal_eti,0,2);
    out_quest ((char *) &a_kun_gx.pal_fill,1,0);
    out_quest ((char *) &a_kun_gx.pal_anz,2,0);
    out_quest ((char *) a_kun_gx.pos_eti,0,2);
    out_quest ((char *) &a_kun_gx.sg1,1,0);
    out_quest ((char *) &a_kun_gx.sg2,1,0);
    out_quest ((char *) &a_kun_gx.pos_fill,1,0);
    out_quest ((char *) &a_kun_gx.ausz_art,1,0);
    out_quest ((char *) &a_kun_gx.text_nr2,2,0);
    out_quest ((char *) &a_kun_gx.cab,1,0);
    out_quest ((char *) a_kun_gx.a_bz3,0,100);
    out_quest ((char *) a_kun_gx.a_bz4,0,100);
    out_quest ((char *) &a_kun_gx.eti_typ,1,0);
    out_quest ((char *) &a_kun_gx.mhd_text,2,0);
    out_quest ((char *) &a_kun_gx.freitext1,2,0);
    out_quest ((char *) &a_kun_gx.freitext2,2,0);
    out_quest ((char *) &a_kun_gx.freitext3,2,0);
    out_quest ((char *) &a_kun_gx.sg3,1,0);
    out_quest ((char *) &a_kun_gx.eti_sum1,1,0);
    out_quest ((char *) &a_kun_gx.eti_sum2,1,0);
    out_quest ((char *) &a_kun_gx.eti_sum3,1,0);
    out_quest ((char *) &a_kun_gx.ampar,2,0);
    out_quest ((char *) &a_kun_gx.sonder_eti,1,0);
    out_quest ((char *) &a_kun_gx.text_nr_a,2,0);
    out_quest ((char *) &a_kun_gx.ean1,3,0);
    out_quest ((char *) &a_kun_gx.cab1,1,0);
    out_quest ((char *) &a_kun_gx.ean2,3,0);
    out_quest ((char *) &a_kun_gx.cab2,1,0);
    out_quest ((char *) &a_kun_gx.ean3,3,0);
    out_quest ((char *) &a_kun_gx.cab3,1,0);
    out_quest ((char *) &a_kun_gx.gwpar,2,0);
    out_quest ((char *) &a_kun_gx.vppar,2,0);
    out_quest ((char *) &a_kun_gx.devise2,1,0);
    out_quest ((char *) &a_kun_gx.eti_nve1,1,0);
    out_quest ((char *) &a_kun_gx.ean_nve1,3,0);
    out_quest ((char *) &a_kun_gx.cab_nve1,1,0);
    out_quest ((char *) &a_kun_gx.eti_nve2,1,0);
    out_quest ((char *) &a_kun_gx.ean_nve2,3,0);
    out_quest ((char *) &a_kun_gx.cab_nve2,1,0);
    out_quest ((char *) &a_kun_gx.zut_gew,3,0);
    out_quest ((char *) &a_kun_gx.zut_proz,3,0);
    out_quest ((char *) &a_kun_gx.freitext4,2,0);
    out_quest ((char *) &a_kun_gx.freitext5,2,0);
    out_quest ((char *) &a_kun_gx.freitext6,2,0);
    out_quest ((char *) &a_kun_gx.freitext7,2,0);
            cursor = prepare_sql ("select a_kun_gx.mdn,  "
"a_kun_gx.fil,  a_kun_gx.kun,  a_kun_gx.a,  a_kun_gx.a_kun,  "
"a_kun_gx.a_bz1,  a_kun_gx.me_einh_kun,  a_kun_gx.inh,  "
"a_kun_gx.kun_bran2,  a_kun_gx.tara,  a_kun_gx.ean,  a_kun_gx.a_bz2,  "
"a_kun_gx.hbk_ztr,  a_kun_gx.kopf_text,  a_kun_gx.pr_rech_kz,  "
"a_kun_gx.modif,  a_kun_gx.text_nr,  a_kun_gx.devise,  "
"a_kun_gx.geb_eti,  a_kun_gx.geb_fill,  a_kun_gx.geb_anz,  "
"a_kun_gx.pal_eti,  a_kun_gx.pal_fill,  a_kun_gx.pal_anz,  "
"a_kun_gx.pos_eti,  a_kun_gx.sg1,  a_kun_gx.sg2,  a_kun_gx.pos_fill,  "
"a_kun_gx.ausz_art,  a_kun_gx.text_nr2,  a_kun_gx.cab,  a_kun_gx.a_bz3,  "
"a_kun_gx.a_bz4,  a_kun_gx.eti_typ,  a_kun_gx.mhd_text,  "
"a_kun_gx.freitext1,  a_kun_gx.freitext2,  a_kun_gx.freitext3,  "
"a_kun_gx.sg3,  a_kun_gx.eti_sum1,  a_kun_gx.eti_sum2,  "
"a_kun_gx.eti_sum3,  a_kun_gx.ampar,  a_kun_gx.sonder_eti,  "
"a_kun_gx.text_nr_a,  a_kun_gx.ean1,  a_kun_gx.cab1,  a_kun_gx.ean2,  "
"a_kun_gx.cab2,  a_kun_gx.ean3,  a_kun_gx.cab3,  a_kun_gx.gwpar,  "
"a_kun_gx.vppar,  a_kun_gx.devise2,  a_kun_gx.eti_nve1,  "
"a_kun_gx.ean_nve1,  a_kun_gx.cab_nve1,  a_kun_gx.eti_nve2,  "
"a_kun_gx.ean_nve2,  a_kun_gx.cab_nve2,  a_kun_gx.zut_gew,  "
"a_kun_gx.zut_proz,  a_kun_gx.freitext4,  a_kun_gx.freitext5,  "
"a_kun_gx.freitext6,  a_kun_gx.freitext7 from a_kun_gx "

#line 27 "a_kun_gx.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun = ? "
                                  "and   a   = ?");

    ins_quest ((char *) &a_kun_gx.mdn,1,0);
    ins_quest ((char *) &a_kun_gx.fil,1,0);
    ins_quest ((char *) &a_kun_gx.kun,2,0);
    ins_quest ((char *) &a_kun_gx.a,3,0);
    ins_quest ((char *) a_kun_gx.a_kun,0,26);
    ins_quest ((char *) a_kun_gx.a_bz1,0,25);
    ins_quest ((char *) &a_kun_gx.me_einh_kun,1,0);
    ins_quest ((char *) &a_kun_gx.inh,3,0);
    ins_quest ((char *) a_kun_gx.kun_bran2,0,3);
    ins_quest ((char *) &a_kun_gx.tara,3,0);
    ins_quest ((char *) &a_kun_gx.ean,3,0);
    ins_quest ((char *) a_kun_gx.a_bz2,0,25);
    ins_quest ((char *) &a_kun_gx.hbk_ztr,1,0);
    ins_quest ((char *) &a_kun_gx.kopf_text,2,0);
    ins_quest ((char *) a_kun_gx.pr_rech_kz,0,2);
    ins_quest ((char *) a_kun_gx.modif,0,2);
    ins_quest ((char *) &a_kun_gx.text_nr,2,0);
    ins_quest ((char *) &a_kun_gx.devise,1,0);
    ins_quest ((char *) a_kun_gx.geb_eti,0,2);
    ins_quest ((char *) &a_kun_gx.geb_fill,1,0);
    ins_quest ((char *) &a_kun_gx.geb_anz,2,0);
    ins_quest ((char *) a_kun_gx.pal_eti,0,2);
    ins_quest ((char *) &a_kun_gx.pal_fill,1,0);
    ins_quest ((char *) &a_kun_gx.pal_anz,2,0);
    ins_quest ((char *) a_kun_gx.pos_eti,0,2);
    ins_quest ((char *) &a_kun_gx.sg1,1,0);
    ins_quest ((char *) &a_kun_gx.sg2,1,0);
    ins_quest ((char *) &a_kun_gx.pos_fill,1,0);
    ins_quest ((char *) &a_kun_gx.ausz_art,1,0);
    ins_quest ((char *) &a_kun_gx.text_nr2,2,0);
    ins_quest ((char *) &a_kun_gx.cab,1,0);
    ins_quest ((char *) a_kun_gx.a_bz3,0,100);
    ins_quest ((char *) a_kun_gx.a_bz4,0,100);
    ins_quest ((char *) &a_kun_gx.eti_typ,1,0);
    ins_quest ((char *) &a_kun_gx.mhd_text,2,0);
    ins_quest ((char *) &a_kun_gx.freitext1,2,0);
    ins_quest ((char *) &a_kun_gx.freitext2,2,0);
    ins_quest ((char *) &a_kun_gx.freitext3,2,0);
    ins_quest ((char *) &a_kun_gx.sg3,1,0);
    ins_quest ((char *) &a_kun_gx.eti_sum1,1,0);
    ins_quest ((char *) &a_kun_gx.eti_sum2,1,0);
    ins_quest ((char *) &a_kun_gx.eti_sum3,1,0);
    ins_quest ((char *) &a_kun_gx.ampar,2,0);
    ins_quest ((char *) &a_kun_gx.sonder_eti,1,0);
    ins_quest ((char *) &a_kun_gx.text_nr_a,2,0);
    ins_quest ((char *) &a_kun_gx.ean1,3,0);
    ins_quest ((char *) &a_kun_gx.cab1,1,0);
    ins_quest ((char *) &a_kun_gx.ean2,3,0);
    ins_quest ((char *) &a_kun_gx.cab2,1,0);
    ins_quest ((char *) &a_kun_gx.ean3,3,0);
    ins_quest ((char *) &a_kun_gx.cab3,1,0);
    ins_quest ((char *) &a_kun_gx.gwpar,2,0);
    ins_quest ((char *) &a_kun_gx.vppar,2,0);
    ins_quest ((char *) &a_kun_gx.devise2,1,0);
    ins_quest ((char *) &a_kun_gx.eti_nve1,1,0);
    ins_quest ((char *) &a_kun_gx.ean_nve1,3,0);
    ins_quest ((char *) &a_kun_gx.cab_nve1,1,0);
    ins_quest ((char *) &a_kun_gx.eti_nve2,1,0);
    ins_quest ((char *) &a_kun_gx.ean_nve2,3,0);
    ins_quest ((char *) &a_kun_gx.cab_nve2,1,0);
    ins_quest ((char *) &a_kun_gx.zut_gew,3,0);
    ins_quest ((char *) &a_kun_gx.zut_proz,3,0);
    ins_quest ((char *) &a_kun_gx.freitext4,2,0);
    ins_quest ((char *) &a_kun_gx.freitext5,2,0);
    ins_quest ((char *) &a_kun_gx.freitext6,2,0);
    ins_quest ((char *) &a_kun_gx.freitext7,2,0);
            sqltext = "update a_kun_gx set a_kun_gx.mdn = ?,  "
"a_kun_gx.fil = ?,  a_kun_gx.kun = ?,  a_kun_gx.a = ?,  "
"a_kun_gx.a_kun = ?,  a_kun_gx.a_bz1 = ?,  a_kun_gx.me_einh_kun = ?,  "
"a_kun_gx.inh = ?,  a_kun_gx.kun_bran2 = ?,  a_kun_gx.tara = ?,  "
"a_kun_gx.ean = ?,  a_kun_gx.a_bz2 = ?,  a_kun_gx.hbk_ztr = ?,  "
"a_kun_gx.kopf_text = ?,  a_kun_gx.pr_rech_kz = ?,  "
"a_kun_gx.modif = ?,  a_kun_gx.text_nr = ?,  a_kun_gx.devise = ?,  "
"a_kun_gx.geb_eti = ?,  a_kun_gx.geb_fill = ?,  "
"a_kun_gx.geb_anz = ?,  a_kun_gx.pal_eti = ?,  "
"a_kun_gx.pal_fill = ?,  a_kun_gx.pal_anz = ?,  "
"a_kun_gx.pos_eti = ?,  a_kun_gx.sg1 = ?,  a_kun_gx.sg2 = ?,  "
"a_kun_gx.pos_fill = ?,  a_kun_gx.ausz_art = ?,  "
"a_kun_gx.text_nr2 = ?,  a_kun_gx.cab = ?,  a_kun_gx.a_bz3 = ?,  "
"a_kun_gx.a_bz4 = ?,  a_kun_gx.eti_typ = ?,  a_kun_gx.mhd_text = ?,  "
"a_kun_gx.freitext1 = ?,  a_kun_gx.freitext2 = ?,  "
"a_kun_gx.freitext3 = ?,  a_kun_gx.sg3 = ?,  a_kun_gx.eti_sum1 = ?,  "
"a_kun_gx.eti_sum2 = ?,  a_kun_gx.eti_sum3 = ?,  a_kun_gx.ampar = ?,  "
"a_kun_gx.sonder_eti = ?,  a_kun_gx.text_nr_a = ?,  "
"a_kun_gx.ean1 = ?,  a_kun_gx.cab1 = ?,  a_kun_gx.ean2 = ?,  "
"a_kun_gx.cab2 = ?,  a_kun_gx.ean3 = ?,  a_kun_gx.cab3 = ?,  "
"a_kun_gx.gwpar = ?,  a_kun_gx.vppar = ?,  a_kun_gx.devise2 = ?,  "
"a_kun_gx.eti_nve1 = ?,  a_kun_gx.ean_nve1 = ?,  "
"a_kun_gx.cab_nve1 = ?,  a_kun_gx.eti_nve2 = ?,  "
"a_kun_gx.ean_nve2 = ?,  a_kun_gx.cab_nve2 = ?,  "
"a_kun_gx.zut_gew = ?,  a_kun_gx.zut_proz = ?,  "
"a_kun_gx.freitext4 = ?,  a_kun_gx.freitext5 = ?,  "
"a_kun_gx.freitext6 = ?,  a_kun_gx.freitext7 = ? "

#line 33 "a_kun_gx.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun = ? "
                                  "and   a   = ?";

            ins_quest ((char *) &a_kun_gx.mdn, 1, 0);
            ins_quest ((char *) &a_kun_gx.fil, 1, 0);
            ins_quest ((char *) &a_kun_gx.kun, 2, 0);
            ins_quest ((char *) &a_kun_gx.a,   3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &a_kun_gx.mdn, 1, 0);
            ins_quest ((char *) &a_kun_gx.fil, 1, 0);
            ins_quest ((char *) &a_kun_gx.kun, 2, 0);
            ins_quest ((char *) &a_kun_gx.a,   3, 0);
            test_upd_cursor = prepare_sql ("select a from a_kun_gx "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun = ? "
                                  "and   a   = ?");
            ins_quest ((char *) &a_kun_gx.mdn, 1, 0);
            ins_quest ((char *) &a_kun_gx.fil, 1, 0);
            ins_quest ((char *) &a_kun_gx.kun, 2, 0);
            ins_quest ((char *) &a_kun_gx.a,   3, 0);
            del_cursor = prepare_sql ("delete from a_kun_gx "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun = ? "
                                  "and   a   = ?");
    ins_quest ((char *) &a_kun_gx.mdn,1,0);
    ins_quest ((char *) &a_kun_gx.fil,1,0);
    ins_quest ((char *) &a_kun_gx.kun,2,0);
    ins_quest ((char *) &a_kun_gx.a,3,0);
    ins_quest ((char *) a_kun_gx.a_kun,0,26);
    ins_quest ((char *) a_kun_gx.a_bz1,0,25);
    ins_quest ((char *) &a_kun_gx.me_einh_kun,1,0);
    ins_quest ((char *) &a_kun_gx.inh,3,0);
    ins_quest ((char *) a_kun_gx.kun_bran2,0,3);
    ins_quest ((char *) &a_kun_gx.tara,3,0);
    ins_quest ((char *) &a_kun_gx.ean,3,0);
    ins_quest ((char *) a_kun_gx.a_bz2,0,25);
    ins_quest ((char *) &a_kun_gx.hbk_ztr,1,0);
    ins_quest ((char *) &a_kun_gx.kopf_text,2,0);
    ins_quest ((char *) a_kun_gx.pr_rech_kz,0,2);
    ins_quest ((char *) a_kun_gx.modif,0,2);
    ins_quest ((char *) &a_kun_gx.text_nr,2,0);
    ins_quest ((char *) &a_kun_gx.devise,1,0);
    ins_quest ((char *) a_kun_gx.geb_eti,0,2);
    ins_quest ((char *) &a_kun_gx.geb_fill,1,0);
    ins_quest ((char *) &a_kun_gx.geb_anz,2,0);
    ins_quest ((char *) a_kun_gx.pal_eti,0,2);
    ins_quest ((char *) &a_kun_gx.pal_fill,1,0);
    ins_quest ((char *) &a_kun_gx.pal_anz,2,0);
    ins_quest ((char *) a_kun_gx.pos_eti,0,2);
    ins_quest ((char *) &a_kun_gx.sg1,1,0);
    ins_quest ((char *) &a_kun_gx.sg2,1,0);
    ins_quest ((char *) &a_kun_gx.pos_fill,1,0);
    ins_quest ((char *) &a_kun_gx.ausz_art,1,0);
    ins_quest ((char *) &a_kun_gx.text_nr2,2,0);
    ins_quest ((char *) &a_kun_gx.cab,1,0);
    ins_quest ((char *) a_kun_gx.a_bz3,0,100);
    ins_quest ((char *) a_kun_gx.a_bz4,0,100);
    ins_quest ((char *) &a_kun_gx.eti_typ,1,0);
    ins_quest ((char *) &a_kun_gx.mhd_text,2,0);
    ins_quest ((char *) &a_kun_gx.freitext1,2,0);
    ins_quest ((char *) &a_kun_gx.freitext2,2,0);
    ins_quest ((char *) &a_kun_gx.freitext3,2,0);
    ins_quest ((char *) &a_kun_gx.sg3,1,0);
    ins_quest ((char *) &a_kun_gx.eti_sum1,1,0);
    ins_quest ((char *) &a_kun_gx.eti_sum2,1,0);
    ins_quest ((char *) &a_kun_gx.eti_sum3,1,0);
    ins_quest ((char *) &a_kun_gx.ampar,2,0);
    ins_quest ((char *) &a_kun_gx.sonder_eti,1,0);
    ins_quest ((char *) &a_kun_gx.text_nr_a,2,0);
    ins_quest ((char *) &a_kun_gx.ean1,3,0);
    ins_quest ((char *) &a_kun_gx.cab1,1,0);
    ins_quest ((char *) &a_kun_gx.ean2,3,0);
    ins_quest ((char *) &a_kun_gx.cab2,1,0);
    ins_quest ((char *) &a_kun_gx.ean3,3,0);
    ins_quest ((char *) &a_kun_gx.cab3,1,0);
    ins_quest ((char *) &a_kun_gx.gwpar,2,0);
    ins_quest ((char *) &a_kun_gx.vppar,2,0);
    ins_quest ((char *) &a_kun_gx.devise2,1,0);
    ins_quest ((char *) &a_kun_gx.eti_nve1,1,0);
    ins_quest ((char *) &a_kun_gx.ean_nve1,3,0);
    ins_quest ((char *) &a_kun_gx.cab_nve1,1,0);
    ins_quest ((char *) &a_kun_gx.eti_nve2,1,0);
    ins_quest ((char *) &a_kun_gx.ean_nve2,3,0);
    ins_quest ((char *) &a_kun_gx.cab_nve2,1,0);
    ins_quest ((char *) &a_kun_gx.zut_gew,3,0);
    ins_quest ((char *) &a_kun_gx.zut_proz,3,0);
    ins_quest ((char *) &a_kun_gx.freitext4,2,0);
    ins_quest ((char *) &a_kun_gx.freitext5,2,0);
    ins_quest ((char *) &a_kun_gx.freitext6,2,0);
    ins_quest ((char *) &a_kun_gx.freitext7,2,0);
            ins_cursor = prepare_sql ("insert into a_kun_gx ("
"mdn,  fil,  kun,  a,  a_kun,  a_bz1,  me_einh_kun,  inh,  kun_bran2,  tara,  ean,  a_bz2,  "
"hbk_ztr,  kopf_text,  pr_rech_kz,  modif,  text_nr,  devise,  geb_eti,  geb_fill,  "
"geb_anz,  pal_eti,  pal_fill,  pal_anz,  pos_eti,  sg1,  sg2,  pos_fill,  ausz_art,  "
"text_nr2,  cab,  a_bz3,  a_bz4,  eti_typ,  mhd_text,  freitext1,  freitext2,  "
"freitext3,  sg3,  eti_sum1,  eti_sum2,  eti_sum3,  ampar,  sonder_eti,  text_nr_a,  "
"ean1,  cab1,  ean2,  cab2,  ean3,  cab3,  gwpar,  vppar,  devise2,  eti_nve1,  ean_nve1,  "
"cab_nve1,  eti_nve2,  ean_nve2,  cab_nve2,  zut_gew,  zut_proz,  freitext4,  "
"freitext5,  freitext6,  freitext7) "

#line 63 "a_kun_gx.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 65 "a_kun_gx.rpp"
            ins_quest ((char *) &a_kun_gx.mdn, 1, 0);
            ins_quest ((char *) &a_kun_gx.fil, 1, 0);
            ins_quest ((char *) &a_kun_gx.kun_bran2, 0, 2);
            ins_quest ((char *) &a_kun_gx.a,   3, 0);
    out_quest ((char *) &a_kun_gx.mdn,1,0);
    out_quest ((char *) &a_kun_gx.fil,1,0);
    out_quest ((char *) &a_kun_gx.kun,2,0);
    out_quest ((char *) &a_kun_gx.a,3,0);
    out_quest ((char *) a_kun_gx.a_kun,0,26);
    out_quest ((char *) a_kun_gx.a_bz1,0,25);
    out_quest ((char *) &a_kun_gx.me_einh_kun,1,0);
    out_quest ((char *) &a_kun_gx.inh,3,0);
    out_quest ((char *) a_kun_gx.kun_bran2,0,3);
    out_quest ((char *) &a_kun_gx.tara,3,0);
    out_quest ((char *) &a_kun_gx.ean,3,0);
    out_quest ((char *) a_kun_gx.a_bz2,0,25);
    out_quest ((char *) &a_kun_gx.hbk_ztr,1,0);
    out_quest ((char *) &a_kun_gx.kopf_text,2,0);
    out_quest ((char *) a_kun_gx.pr_rech_kz,0,2);
    out_quest ((char *) a_kun_gx.modif,0,2);
    out_quest ((char *) &a_kun_gx.text_nr,2,0);
    out_quest ((char *) &a_kun_gx.devise,1,0);
    out_quest ((char *) a_kun_gx.geb_eti,0,2);
    out_quest ((char *) &a_kun_gx.geb_fill,1,0);
    out_quest ((char *) &a_kun_gx.geb_anz,2,0);
    out_quest ((char *) a_kun_gx.pal_eti,0,2);
    out_quest ((char *) &a_kun_gx.pal_fill,1,0);
    out_quest ((char *) &a_kun_gx.pal_anz,2,0);
    out_quest ((char *) a_kun_gx.pos_eti,0,2);
    out_quest ((char *) &a_kun_gx.sg1,1,0);
    out_quest ((char *) &a_kun_gx.sg2,1,0);
    out_quest ((char *) &a_kun_gx.pos_fill,1,0);
    out_quest ((char *) &a_kun_gx.ausz_art,1,0);
    out_quest ((char *) &a_kun_gx.text_nr2,2,0);
    out_quest ((char *) &a_kun_gx.cab,1,0);
    out_quest ((char *) a_kun_gx.a_bz3,0,100);
    out_quest ((char *) a_kun_gx.a_bz4,0,100);
    out_quest ((char *) &a_kun_gx.eti_typ,1,0);
    out_quest ((char *) &a_kun_gx.mhd_text,2,0);
    out_quest ((char *) &a_kun_gx.freitext1,2,0);
    out_quest ((char *) &a_kun_gx.freitext2,2,0);
    out_quest ((char *) &a_kun_gx.freitext3,2,0);
    out_quest ((char *) &a_kun_gx.sg3,1,0);
    out_quest ((char *) &a_kun_gx.eti_sum1,1,0);
    out_quest ((char *) &a_kun_gx.eti_sum2,1,0);
    out_quest ((char *) &a_kun_gx.eti_sum3,1,0);
    out_quest ((char *) &a_kun_gx.ampar,2,0);
    out_quest ((char *) &a_kun_gx.sonder_eti,1,0);
    out_quest ((char *) &a_kun_gx.text_nr_a,2,0);
    out_quest ((char *) &a_kun_gx.ean1,3,0);
    out_quest ((char *) &a_kun_gx.cab1,1,0);
    out_quest ((char *) &a_kun_gx.ean2,3,0);
    out_quest ((char *) &a_kun_gx.cab2,1,0);
    out_quest ((char *) &a_kun_gx.ean3,3,0);
    out_quest ((char *) &a_kun_gx.cab3,1,0);
    out_quest ((char *) &a_kun_gx.gwpar,2,0);
    out_quest ((char *) &a_kun_gx.vppar,2,0);
    out_quest ((char *) &a_kun_gx.devise2,1,0);
    out_quest ((char *) &a_kun_gx.eti_nve1,1,0);
    out_quest ((char *) &a_kun_gx.ean_nve1,3,0);
    out_quest ((char *) &a_kun_gx.cab_nve1,1,0);
    out_quest ((char *) &a_kun_gx.eti_nve2,1,0);
    out_quest ((char *) &a_kun_gx.ean_nve2,3,0);
    out_quest ((char *) &a_kun_gx.cab_nve2,1,0);
    out_quest ((char *) &a_kun_gx.zut_gew,3,0);
    out_quest ((char *) &a_kun_gx.zut_proz,3,0);
    out_quest ((char *) &a_kun_gx.freitext4,2,0);
    out_quest ((char *) &a_kun_gx.freitext5,2,0);
    out_quest ((char *) &a_kun_gx.freitext6,2,0);
    out_quest ((char *) &a_kun_gx.freitext7,2,0);
            cursor_bra = prepare_sql ("select a_kun_gx.mdn,  "
"a_kun_gx.fil,  a_kun_gx.kun,  a_kun_gx.a,  a_kun_gx.a_kun,  "
"a_kun_gx.a_bz1,  a_kun_gx.me_einh_kun,  a_kun_gx.inh,  "
"a_kun_gx.kun_bran2,  a_kun_gx.tara,  a_kun_gx.ean,  a_kun_gx.a_bz2,  "
"a_kun_gx.hbk_ztr,  a_kun_gx.kopf_text,  a_kun_gx.pr_rech_kz,  "
"a_kun_gx.modif,  a_kun_gx.text_nr,  a_kun_gx.devise,  "
"a_kun_gx.geb_eti,  a_kun_gx.geb_fill,  a_kun_gx.geb_anz,  "
"a_kun_gx.pal_eti,  a_kun_gx.pal_fill,  a_kun_gx.pal_anz,  "
"a_kun_gx.pos_eti,  a_kun_gx.sg1,  a_kun_gx.sg2,  a_kun_gx.pos_fill,  "
"a_kun_gx.ausz_art,  a_kun_gx.text_nr2,  a_kun_gx.cab,  a_kun_gx.a_bz3,  "
"a_kun_gx.a_bz4,  a_kun_gx.eti_typ,  a_kun_gx.mhd_text,  "
"a_kun_gx.freitext1,  a_kun_gx.freitext2,  a_kun_gx.freitext3,  "
"a_kun_gx.sg3,  a_kun_gx.eti_sum1,  a_kun_gx.eti_sum2,  "
"a_kun_gx.eti_sum3,  a_kun_gx.ampar,  a_kun_gx.sonder_eti,  "
"a_kun_gx.text_nr_a,  a_kun_gx.ean1,  a_kun_gx.cab1,  a_kun_gx.ean2,  "
"a_kun_gx.cab2,  a_kun_gx.ean3,  a_kun_gx.cab3,  a_kun_gx.gwpar,  "
"a_kun_gx.vppar,  a_kun_gx.devise2,  a_kun_gx.eti_nve1,  "
"a_kun_gx.ean_nve1,  a_kun_gx.cab_nve1,  a_kun_gx.eti_nve2,  "
"a_kun_gx.ean_nve2,  a_kun_gx.cab_nve2,  a_kun_gx.zut_gew,  "
"a_kun_gx.zut_proz,  a_kun_gx.freitext4,  a_kun_gx.freitext5,  "
"a_kun_gx.freitext6,  a_kun_gx.freitext7 from a_kun_gx "

#line 70 "a_kun_gx.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun_bran2 = ? "
                                  "and   a   = ?");
            ins_quest ((char *) &a_kun_gx.mdn, 1, 0);
            ins_quest ((char *) &a_kun_gx.fil, 1, 0);
            ins_quest ((char *) &a_kun_gx.a,   3, 0);
    out_quest ((char *) &a_kun_gx.mdn,1,0);
    out_quest ((char *) &a_kun_gx.fil,1,0);
    out_quest ((char *) &a_kun_gx.kun,2,0);
    out_quest ((char *) &a_kun_gx.a,3,0);
    out_quest ((char *) a_kun_gx.a_kun,0,26);
    out_quest ((char *) a_kun_gx.a_bz1,0,25);
    out_quest ((char *) &a_kun_gx.me_einh_kun,1,0);
    out_quest ((char *) &a_kun_gx.inh,3,0);
    out_quest ((char *) a_kun_gx.kun_bran2,0,3);
    out_quest ((char *) &a_kun_gx.tara,3,0);
    out_quest ((char *) &a_kun_gx.ean,3,0);
    out_quest ((char *) a_kun_gx.a_bz2,0,25);
    out_quest ((char *) &a_kun_gx.hbk_ztr,1,0);
    out_quest ((char *) &a_kun_gx.kopf_text,2,0);
    out_quest ((char *) a_kun_gx.pr_rech_kz,0,2);
    out_quest ((char *) a_kun_gx.modif,0,2);
    out_quest ((char *) &a_kun_gx.text_nr,2,0);
    out_quest ((char *) &a_kun_gx.devise,1,0);
    out_quest ((char *) a_kun_gx.geb_eti,0,2);
    out_quest ((char *) &a_kun_gx.geb_fill,1,0);
    out_quest ((char *) &a_kun_gx.geb_anz,2,0);
    out_quest ((char *) a_kun_gx.pal_eti,0,2);
    out_quest ((char *) &a_kun_gx.pal_fill,1,0);
    out_quest ((char *) &a_kun_gx.pal_anz,2,0);
    out_quest ((char *) a_kun_gx.pos_eti,0,2);
    out_quest ((char *) &a_kun_gx.sg1,1,0);
    out_quest ((char *) &a_kun_gx.sg2,1,0);
    out_quest ((char *) &a_kun_gx.pos_fill,1,0);
    out_quest ((char *) &a_kun_gx.ausz_art,1,0);
    out_quest ((char *) &a_kun_gx.text_nr2,2,0);
    out_quest ((char *) &a_kun_gx.cab,1,0);
    out_quest ((char *) a_kun_gx.a_bz3,0,100);
    out_quest ((char *) a_kun_gx.a_bz4,0,100);
    out_quest ((char *) &a_kun_gx.eti_typ,1,0);
    out_quest ((char *) &a_kun_gx.mhd_text,2,0);
    out_quest ((char *) &a_kun_gx.freitext1,2,0);
    out_quest ((char *) &a_kun_gx.freitext2,2,0);
    out_quest ((char *) &a_kun_gx.freitext3,2,0);
    out_quest ((char *) &a_kun_gx.sg3,1,0);
    out_quest ((char *) &a_kun_gx.eti_sum1,1,0);
    out_quest ((char *) &a_kun_gx.eti_sum2,1,0);
    out_quest ((char *) &a_kun_gx.eti_sum3,1,0);
    out_quest ((char *) &a_kun_gx.ampar,2,0);
    out_quest ((char *) &a_kun_gx.sonder_eti,1,0);
    out_quest ((char *) &a_kun_gx.text_nr_a,2,0);
    out_quest ((char *) &a_kun_gx.ean1,3,0);
    out_quest ((char *) &a_kun_gx.cab1,1,0);
    out_quest ((char *) &a_kun_gx.ean2,3,0);
    out_quest ((char *) &a_kun_gx.cab2,1,0);
    out_quest ((char *) &a_kun_gx.ean3,3,0);
    out_quest ((char *) &a_kun_gx.cab3,1,0);
    out_quest ((char *) &a_kun_gx.gwpar,2,0);
    out_quest ((char *) &a_kun_gx.vppar,2,0);
    out_quest ((char *) &a_kun_gx.devise2,1,0);
    out_quest ((char *) &a_kun_gx.eti_nve1,1,0);
    out_quest ((char *) &a_kun_gx.ean_nve1,3,0);
    out_quest ((char *) &a_kun_gx.cab_nve1,1,0);
    out_quest ((char *) &a_kun_gx.eti_nve2,1,0);
    out_quest ((char *) &a_kun_gx.ean_nve2,3,0);
    out_quest ((char *) &a_kun_gx.cab_nve2,1,0);
    out_quest ((char *) &a_kun_gx.zut_gew,3,0);
    out_quest ((char *) &a_kun_gx.zut_proz,3,0);
    out_quest ((char *) &a_kun_gx.freitext4,2,0);
    out_quest ((char *) &a_kun_gx.freitext5,2,0);
    out_quest ((char *) &a_kun_gx.freitext6,2,0);
    out_quest ((char *) &a_kun_gx.freitext7,2,0);
            cursor_art = prepare_sql ("select a_kun_gx.mdn,  "
"a_kun_gx.fil,  a_kun_gx.kun,  a_kun_gx.a,  a_kun_gx.a_kun,  "
"a_kun_gx.a_bz1,  a_kun_gx.me_einh_kun,  a_kun_gx.inh,  "
"a_kun_gx.kun_bran2,  a_kun_gx.tara,  a_kun_gx.ean,  a_kun_gx.a_bz2,  "
"a_kun_gx.hbk_ztr,  a_kun_gx.kopf_text,  a_kun_gx.pr_rech_kz,  "
"a_kun_gx.modif,  a_kun_gx.text_nr,  a_kun_gx.devise,  "
"a_kun_gx.geb_eti,  a_kun_gx.geb_fill,  a_kun_gx.geb_anz,  "
"a_kun_gx.pal_eti,  a_kun_gx.pal_fill,  a_kun_gx.pal_anz,  "
"a_kun_gx.pos_eti,  a_kun_gx.sg1,  a_kun_gx.sg2,  a_kun_gx.pos_fill,  "
"a_kun_gx.ausz_art,  a_kun_gx.text_nr2,  a_kun_gx.cab,  a_kun_gx.a_bz3,  "
"a_kun_gx.a_bz4,  a_kun_gx.eti_typ,  a_kun_gx.mhd_text,  "
"a_kun_gx.freitext1,  a_kun_gx.freitext2,  a_kun_gx.freitext3,  "
"a_kun_gx.sg3,  a_kun_gx.eti_sum1,  a_kun_gx.eti_sum2,  "
"a_kun_gx.eti_sum3,  a_kun_gx.ampar,  a_kun_gx.sonder_eti,  "
"a_kun_gx.text_nr_a,  a_kun_gx.ean1,  a_kun_gx.cab1,  a_kun_gx.ean2,  "
"a_kun_gx.cab2,  a_kun_gx.ean3,  a_kun_gx.cab3,  a_kun_gx.gwpar,  "
"a_kun_gx.vppar,  a_kun_gx.devise2,  a_kun_gx.eti_nve1,  "
"a_kun_gx.ean_nve1,  a_kun_gx.cab_nve1,  a_kun_gx.eti_nve2,  "
"a_kun_gx.ean_nve2,  a_kun_gx.cab_nve2,  a_kun_gx.zut_gew,  "
"a_kun_gx.zut_proz,  a_kun_gx.freitext4,  a_kun_gx.freitext5,  "
"a_kun_gx.freitext6,  a_kun_gx.freitext7 from a_kun_gx "

#line 78 "a_kun_gx.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   kun = 0 "
                                  "and   kun_bran2 <= \"0\" "
                                  "and   a   = ?");
}

int A_KUN_GX_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int A_KUN_GX_CLASS::dbread_bra (void)
/**
Ersten Satz aus Tabelle nach Kundenbranche lesen .
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         open_sql (cursor_bra);
         fetch_sql (cursor_bra);
         return sqlstatus;
}

int A_KUN_GX_CLASS::dbread_art (void)
/**
Ersten Satz aus Tabelle ohne Kunde und Branche lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         open_sql (cursor_art);
         fetch_sql (cursor_art);
         return sqlstatus;
}
