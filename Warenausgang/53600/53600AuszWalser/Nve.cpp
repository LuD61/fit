// Nve.cpp: Implementierung der Klasse CNve.
//
//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "DbTime.h"
#include "Nve.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

const LPSTR CNve::m_ErrorStrings[] = {"OK",
                                    "Keine Iln-Nummer vorhanden"
};


CNve::CNve()
{
	m_LGutString = "";
	m_LeftPart = "";
	m_RightPart = "";
	m_Lfd = 0l;
	m_Nve = "";
	m_Iln = "";
	m_Mdn = 1;
	m_Fil = 0;
	m_Ls  = 0l;
	m_BlgTyp  = "L";
	m_NveDb = new CNveDb ();

}

CNve::~CNve()
{
	delete m_NveDb;

}

void CNve::CreateParts ()
{
	if (m_LGutString != "")
	{
		Token t;
		t.SetSep ("=");
		t = m_LGutString;
		if (t.GetAnzToken () > 0)
		{
			m_LeftPart = t.GetToken (0);
			m_LeftPart.Trim ();
			m_NvePart.Set (m_LeftPart);
		}
		if (t.GetAnzToken () > 1)
		{
			m_RightPart = t.GetToken (1);
			m_RightPart.Trim ();
			CreateRightParts ();
		}
	}
}

void CNve::CreateRightParts ()
{
	Token t;
	t.SetSep ("+");
	t = m_RightPart;
	for (m_PsPmParts = 0; (m_PsPmParts < t.GetAnzToken ()) && (m_PsPmParts < sizeof (m_PsPmPart)); m_PsPmParts ++)
	{
		CString part = t.GetToken (m_PsPmParts);
		part.Trim ();
		m_PsPmPart[m_PsPmParts].Set (part);
	}
}

void CNve::CreateNve (int lfd)
{
	CString NveStart = "003";
	CString IlnPart;
	CString RandomPart;
	CString Lfd;
	char pz[1];

//    m_Nve = "0040148571301048675";
//	m_Nve += CreatePz ((LPSTR) m_Nve, pz);
//	return;

	m_Iln.Trim ();
	if (m_Iln != "")
	{
		IlnPart = m_Iln.SubString (0,7);
		Sleep (13);
		DbTime Today;
		time_t time = Today.Timer ();
		Lfd.Format ("%d", lfd % 10);
//		RandomPart.Format ("%08.8u", time);
		RandomPart = *Today.MilliSeconds ();
		m_Nve = NveStart;
		m_Nve += IlnPart;
		m_Nve += RandomPart.Right (8);
		m_Nve += Lfd;
		m_Nve += CreatePz ((LPSTR) m_Nve, pz);
	}
}

LPSTR CNve::CreatePz (LPSTR nve, char pz[1])
{
	char newNve [21];
	char spart[2] = {"0"};
	int value;
	int factor = 3;
	int sum = 0;

	memset (newNve, 0, sizeof (newNve));
	strncpy (newNve, nve, sizeof (newNve));

	for (int i = 0; i < sizeof (newNve) - 1; i ++)
	{
		if (newNve[i] == 0)
		{
			newNve[i] = '0';
		}
		if (newNve[i] < '0' || newNve[i] > '9')
		{
			newNve[i] = '0';
		}
		spart[0] = newNve[i];
		value = atoi (spart);
		sum += value * factor;
		factor = (factor == 3) ? 1 : 3;
	}
	value = sum % 10;
	if (value != 0)
	{
		value = 10 - value;
	}
	sprintf (pz, "%1.1d", value);
	return pz;
}

void CNve::Update ()
{
	m_Error = FALSE;
	m_Iln.Trim ();
	if (m_Iln != "")
	{
		if (m_LGutString != "")
		{
			m_NveDb->InitNve ();
			m_NveDb->InitPsPm ();
			CreateParts ();
			m_NveDb->set_Mdn (m_Mdn);
			m_NveDb->set_Fil (m_Fil);
			m_NveDb->set_Ls (m_Ls);
			m_NveDb->set_BlgTyp (m_BlgTyp);
			m_NveDb->set_Lfd (m_NveDb->GetMaxLfd ());
			m_Lfd = m_NveDb->Lfd ();
			int count = atoi (m_NvePart.Number ());
			for (int i = 0; i < count; i ++)
			{
				CreateNve (i);
				m_NveDb->set_Nve (m_Nve);
				m_NveDb->set_A (m_NveDb->GetArticle (m_NvePart.Name ()));
				m_NveDb->UpdateNve ();
			}

			m_NveDb->set_Pm (m_NveDb->GetArticle (m_NvePart.Name ()));
            m_NveDb->set_PmZahl (atoi (m_NvePart.Number ()));
			if (m_PsPmParts > 0)
			{	
				m_NveDb->set_Ps1 (m_NveDb->GetArticle (m_PsPmPart[0].Name ()));
                m_NveDb->set_Ps1Zahl (atoi (m_PsPmPart[0].Number ()));
			}
			if (m_PsPmParts > 1)
			{	
				m_NveDb->set_Ps2 (m_NveDb->GetArticle (m_PsPmPart[1].Name ()));
                m_NveDb->set_Ps2Zahl (atoi (m_PsPmPart[1].Number ()));
			}
			if (m_PsPmParts > 2)
			{	
				m_NveDb->set_Ps3 (m_NveDb->GetArticle (m_PsPmPart[2].Name ()));
                m_NveDb->set_Ps3Zahl (atoi (m_PsPmPart[2].Number ()));
			}
			if (m_PsPmParts > 3)
			{	
				m_NveDb->set_Ps4 (m_NveDb->GetArticle (m_PsPmPart[3].Name ()));
                m_NveDb->set_Ps4Zahl (atoi (m_PsPmPart[3].Number ()));
			}
			m_NveDb->UpdatePsPm ();
		}
	}
	else
	{
		m_Error = TRUE;
        m_ErrorNumber = 1; 
	}
}

const LPSTR CNve::GetErrorText ()
{
	LPSTR text = NULL;
	if (m_Error && m_ErrorNumber < sizeof (m_ErrorStrings) - 1)
	{
		text = m_ErrorStrings[m_ErrorNumber];
	}
	return text;
}
