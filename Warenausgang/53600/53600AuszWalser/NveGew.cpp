#include <windows.h>
#include "NveGew.h"

NveGew::NveGew ()
{
	gew = 0.0;
	a = 0.0;
}

NveGew::~NveGew ()
{
	Kartons.DestroyAll ();
}

void NveGew::SetGew (double gew)
{
	this->gew = gew;
}

double NveGew::GetGew ()
{
	return gew;
}

void NveGew::AddGew (double gew)
{
	this->gew += gew;
}

void NveGew::SubGew (double gew)
{
	this->gew -= gew;
	this->gew = (this->gew < 0.0) ? 0.0 : this->gew;
}

void NveGew::AddKarton (void *karton)
{
	Kartons.Add (karton);
}

void *NveGew::GetKarton (int i)
{
	return Kartons.Get (i);
}

void NveGew::DropKarton (int i)
{
	Kartons.Drop (i);
}

void NveGew::DropLastKarton ()
{
	int i = Kartons.GetAnz ();
	if (i == 0) return;
	Kartons.Drop (i - 1);
}

int NveGew::size ()
{
	return Kartons.GetAnz ();
}
