#ifndef _TRAPOPSPM_DEF
#define _TRAPOPSPM_DEF
#define CONSOLE
#include "wmask.h"
#include "dbclass.h"

struct TRAPOPSPM {
   short     mdn;
   short     fil;
   long      ls;
   char      blg_typ[2];
   long      lfd;
   double    pm;
   double    pmzahl;
   double    ps1;
   double    ps1zahl;
   double    ps2;
   double    ps2zahl;
   double    ps3;
   double    ps3zahl;
   double    ps4;
   double    ps4zahl;
};
extern struct TRAPOPSPM trapopspm, trapopspm_null;

#line 8 "trapopspm.rh"

class TRAPOPSPM_CLASS : public DB_CLASS 
{
       private:
       public :
               TRAPOPSPM trapopspm; 
               TRAPOPSPM_CLASS () : DB_CLASS ()
               {
               }
       virtual void prepare (void);
};
#endif
