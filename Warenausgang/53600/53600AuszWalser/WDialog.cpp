// WDialog.cpp: Implementierung der Klasse WDialog.
//
//////////////////////////////////////////////////////////////////////

#include <math.h>
#include "Wapplication.h"
#include "WDialog.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

#define APP WApplication::GetInstance ()

WDialog *WDialog::m_DlgInstance = NULL;

WDialog::WDialog(DWORD IDD)
{
	this->IDD = IDD;
	m_hBrush = NULL;
	m_Parent = NULL;
	m_hWnd = NULL;
	m_DlgInstance = this;
}

WDialog::~WDialog()
{
	if (m_hBrush != NULL)
	{
		DeleteObject (m_hBrush);
	}
}


bool WDialog::OnInitDialog ()
{
	return false;
}

HBRUSH WDialog::OnCtlColor (HDC hDC, HWND hWnd, UINT nCtlColor)
{
	 if (m_hBrush == NULL)
	 {
		m_hBrush = CreateSolidBrush (GetSysColor (COLOR_3DFACE));
	 }
	 return m_hBrush;
}

int WDialog::DoModal ()
{
	return DialogBox(APP->GethInstance (), MAKEINTRESOURCE (IDD), m_Parent, (DLGPROC) DialogProc);
}
 

BOOL CALLBACK WDialog::DialogProc( HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) 
{
	BOOL ret = FALSE;
	HBRUSH hBrush;
	HDC hDC;

    switch (uMsg) 
    { 
        case WM_INITDIALOG:
			m_DlgInstance->SethWnd (hwndDlg);
			return m_DlgInstance->OnInitDialog ();
        case WM_CTLCOLORSTATIC:
			hDC = (HDC) wParam;
			hBrush =  m_DlgInstance->OnCtlColor (hDC, (HWND) lParam, CTLCOLOR_STATIC);
			return  (BOOL) hBrush;
		case WM_CTLCOLORDLG:
			hDC = (HDC) wParam;
			hBrush =  m_DlgInstance->OnCtlColor (hDC, (HWND) lParam, CTLCOLOR_DLG);
			return (BOOL) hBrush;
		case WM_CTLCOLORBTN:
			hDC = (HDC) wParam;
			hBrush =  m_DlgInstance->OnCtlColor (hDC, (HWND) lParam, CTLCOLOR_BTN);
			return (BOOL) hBrush;
		case WM_CTLCOLOREDIT:
			hDC = (HDC) wParam;
			hBrush =  m_DlgInstance->OnCtlColor (hDC, (HWND) lParam, CTLCOLOR_EDIT);
			return  (BOOL) hBrush;
        case WM_COMMAND: 
            switch (LOWORD(wParam)) 
			{
			case IDOK :
                EndDialog(hwndDlg, wParam); 
                return TRUE; 
			}
			break;
        case WM_SYSCOMMAND: 
            switch (wParam)
			{
			case SC_CLOSE:
                EndDialog(hwndDlg, wParam); 
                return TRUE; 
			}
			break;
	}
	return ret;
}

void WDialog::SetControlFont (DWORD Idc, HFONT Font)
{
	HWND hWnd;

	hWnd = GetDlgItem (m_hWnd, Idc);
	if (hWnd != NULL)
	{
		SendMessage (hWnd, WM_SETFONT, (WPARAM) Font, MAKELPARAM (0, 0));
	}
}


HFONT WDialog::CreateFont (HDC hdc, char * szFaceN, int iDeciPtH,
                          int iDeciPtW, int iAttrib, BOOL fLogRes)
{
     float      cxDpi, cyDpi ;
     HFONT      hFont, oldfont ;
     LOGFONT    lf ;
     POINT      pt ;
     TEXTMETRIC tm ;

     SaveDC (hdc) ;

     SetGraphicsMode (hdc, GM_ADVANCED) ;
     ModifyWorldTransform (hdc, NULL, MWT_IDENTITY) ;

     SetViewportOrgEx (hdc, 0, 0, NULL) ;
     SetWindowOrgEx   (hdc, 0, 0, NULL) ;

     if (fLogRes)
          {
          cxDpi = (float) GetDeviceCaps (hdc, LOGPIXELSX) ;
          cyDpi = (float) GetDeviceCaps (hdc, LOGPIXELSY) ;
          }
     else
          {
          cxDpi = (float) (25.4 * GetDeviceCaps (hdc, HORZRES) /
                                  GetDeviceCaps (hdc, HORZSIZE)) ;

          cyDpi = (float) (25.4 * GetDeviceCaps (hdc, VERTRES) /
                                  GetDeviceCaps (hdc, VERTSIZE)) ;
          }

     pt.x = (int) (iDeciPtW  * cxDpi / 72) ;
     pt.y = (int) (iDeciPtH * cyDpi / 72) ;

     DPtoLP (hdc, &pt, 1) ;

     lf.lfHeight         = - (int) (fabs (pt.y) / 10.0 + 0.5) ;
     lf.lfWidth          = 0 ;
     lf.lfEscapement     = 0 ;
     lf.lfOrientation    = 0 ;
     lf.lfWeight         = iAttrib & ATTR_BOLD      ? 700 : 0 ;
     lf.lfItalic         = iAttrib & ATTR_ITALIC    ?   1 : 0 ;
     lf.lfUnderline      = iAttrib & ATTR_UNDERLINE ?   1 : 0 ;
     lf.lfStrikeOut      = iAttrib & ATTR_STRIKEOUT ?   1 : 0 ;
     lf.lfCharSet        = 0 ;
     lf.lfOutPrecision   = 0 ;
     lf.lfClipPrecision  = 0 ;
     lf.lfQuality        = 0 ;
     lf.lfPitchAndFamily = 0 ;

     strcpy (lf.lfFaceName, szFaceN) ;

     hFont = CreateFontIndirect (&lf) ;

     if (iDeciPtW != 0)
          {
          oldfont = (HFONT) SelectObject (hdc, hFont) ;

          GetTextMetrics (hdc, &tm) ;

          DeleteObject (SelectObject (hdc, oldfont)) ;

          lf.lfWidth = (int) (tm.tmAveCharWidth *
                              fabs (pt.x) / fabs (pt.y) + 0.5) ;

          hFont = CreateFontIndirect (&lf) ;
          }

     RestoreDC (hdc, -1) ;
     return hFont ;
}
