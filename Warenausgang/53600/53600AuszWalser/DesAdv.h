// DesAdv.h: Schnittstelle f�r die Klasse CDesAdv.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DESADV_H__E969076E_DAFD_44D2_87AE_5EBFD004AB15__INCLUDED_)
#define AFX_DESADV_H__E969076E_DAFD_44D2_87AE_5EBFD004AB15__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "lsnve.h"

class CGui 
{
private:
	double m_Netto;
	double m_Brutto;
public:
	double Netto ()
	{
		return m_Netto;
	}
	double Brutto ()
	{
		return m_Brutto;
	}

	CGui ();
	~CGui ();

	void EnterNetto ();
	void EnterBrutto ();
	void PrintNveEti ();
};

class CDesAdv  
{
private:
	LSNVE_CLASS m_Lsnve;
	CGui *m_Gui;
	char m_Nve[21]; 

public:
	void set_Gui (CGui *gui)
	{
		m_Gui = gui;
	}

	LPSTR Nve ()
	{
		return m_Nve;
	}

	CDesAdv();
	virtual ~CDesAdv();
	void Save ();
	void Print ();
	void CreateNve ();
};

#endif // !defined(AFX_DESADV_H__E969076E_DAFD_44D2_87AE_5EBFD004AB15__INCLUDED_)
