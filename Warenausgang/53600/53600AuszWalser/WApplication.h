// WApplication.h: Schnittstelle f�r die Klasse WApplication.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WAPPLICATION_H__73943533_6FE7_4823_AB50_CCBCFEEC34F4__INCLUDED_)
#define AFX_WAPPLICATION_H__73943533_6FE7_4823_AB50_CCBCFEEC34F4__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>
#include <string>

using namespace std;

class WApplication  
{
private:
	HINSTANCE m_hInstance;
	string m_Commandline;
	static WApplication *Instance;
protected:
	WApplication();
	virtual ~WApplication();
public:
	void SethInstance (HINSTANCE hInstance)
	{
		m_hInstance = hInstance;
	}

	HINSTANCE GethInstance ()
	{
		return m_hInstance;
	}

	void SetCommandline (char *Commandline)
	{
		m_Commandline = Commandline;
	}

	char *GetCommandLine (char *Commandline=NULL, int length=0)
	{
		char *ret = (char *) m_Commandline.c_str ();
		if (Commandline != NULL && length != 0)
		{
			memset (Commandline, 0, length);
			strncpy (Commandline, ret, length - 1);
			ret = Commandline;
		}
		return ret;
	}

	static WApplication *GetInstance ();
	static void DestroyInstance ();
};

#endif // !defined(AFX_WAPPLICATION_H__73943533_6FE7_4823_AB50_CCBCFEEC34F4__INCLUDED_)
