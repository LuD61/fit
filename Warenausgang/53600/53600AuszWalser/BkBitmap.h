#pragma once

class CBkBitmap
{
public:
	enum BK_TYPE
	{
		DynamicBlue
	};
	enum DYN_STEP
	{
		Slow = 1,
        Normal = 2,
		Fast = 3
	};
private:
	BK_TYPE m_BkType;
	DYN_STEP m_DynStep;
	HBITMAP m_Bitmap;
	int m_Width;
	int m_Height;
	UINT m_Planes;
	int parts;
	COLORREF m_BmColor;

public:
	void SetBkType (BK_TYPE BkType)
	{
		m_BkType = BkType;
	}
	void SetWidth (int Width)
	{
		m_Width = Width;
	}

	void SetHeight (int Height)
	{
		m_Height = Height;
	}

	void SetPlanes (int Planes)
	{
		m_Planes = Planes;
	}

	void SetBmColor (COLORREF BmColor)
	{
		m_BmColor = BmColor;
	}

	void SetDynStep (DYN_STEP DynStep)
	{
		m_DynStep = DynStep;
	}

	CBkBitmap(void);
	~CBkBitmap(void);

	HBITMAP Create ();
	void ReadFromWindow (HWND hWnd, int x, int y, int cx, int cy);
	void WriteToWindow (HWND hWnd, int x, int y, int cx, int cy);
	virtual void FillVRectParts (HDC hdc, COLORREF color, RECT rect);
	virtual void FillRectParts (HDC hdc, COLORREF color, RECT rect);
};
