#ifndef _FIL_OK
#define _FIL_OK

struct FIL {
   char      abr_period[2];
   long      adr;
   long      adr_lief;
   short     afl;
   char      auf_typ[2];
   char      best_kz[2];
   char      bli_kz[2];
   long      dat_ero;
   short     daten_mnp;
   short     delstatus;
   short     fil;
   char      fil_kla[2];
   short     fil_gr;
   double    fl_lad;
   double    fl_nto;
   double    fl_vk_ges;
   short     frm;
   long      iakv;
   char      inv_rht[2];
   long      kun;
   char      lief[17];
   char      lief_rht[2];
   long      lief_s;
   char      ls_abgr[2];
   char      ls_kz[2];
   short     ls_sum;
   short     mdn;
   char      pers[13];
   short     pers_anz;
   char      pos_kum[2];
   char      pr_ausw[2];
   char      pr_bel_entl[2];
   char      pr_fil_kz[2];
   long      pr_lst;
   char      pr_vk_kz[2];
   double    reg_bed_theke_lng;
   double    reg_kt_lng;
   double    reg_kue_lng;
   double    reg_lng;
   double    reg_tks_lng;
   double    reg_tkt_lng;
   char      ret_entl[2];
   char      smt_kz[2];
   short     sonst_einh;
   short     sprache;
   char      sw_kz[2];
   long      tou;
   char      umlgr[2];
   char      verk_st_kz[2];
   short     vrs_typ;
   char      inv_akv[2];
   short     verr_mdn;
};


extern struct FIL _fil, _fil_null;

class FIL_CLASS 
{
       private :
               struct FIL fil;
               struct ADR adr;
               short cursor_fil;
               short cursor_fil_gr;
               short test_upd_cursor;
               short upd_cursor;
               short ins_cursor;
               short del_cursor;

       public :
               FIL_CLASS ()
               {
                         cursor_fil      = -1;
                         cursor_fil_gr   = -1;
                         test_upd_cursor = -1;
                         upd_cursor      = -1;
                         ins_cursor      = -1;
                         del_cursor      = -1;
               }

               int lese_fil (short, short);
               int lese_fil (void);
               int lese_fil_gr (short, short);
               int lese_fil_gr (void);
               void close_fil (void);
               void close_fil_gr (void);
               int ShowAllFil (short);
};
#endif /*  _FIL_OK  */
