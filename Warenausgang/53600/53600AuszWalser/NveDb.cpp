// NveDb.cpp: Implementierung der Klasse CNveDb.
//
//////////////////////////////////////////////////////////////////////
#include <windows.h>
#include "NveDb.h"
#include "strfkt.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

const LPSTR CNveDb::m_ErrorStrings[] = {"OK",
                                      "Nve nicht gefunden",
									  "Datenende erreicht"
};

CNveDb::CNveDb()
{
	m_TrapoNve.sqlin  ((short *) &m_TrapoNve.traponve.mdn, SQLSHORT, 0);
	m_TrapoNve.sqlin  ((short *) &m_TrapoNve.traponve.fil, SQLSHORT, 0);
	m_TrapoNve.sqlin  ((long *)  &m_TrapoNve.traponve.ls,  SQLLONG, 0);
	m_TrapoNve.sqlin ((char *)  m_TrapoNve.traponve.blg_typ,  SQLCHAR, sizeof (m_TrapoNve.traponve.blg_typ));
	m_TrapoNve.sqlout ((long *) &m_Lfd, SQLLONG, 0);
	m_MaxCursor = m_TrapoNve.sqlcursor ("select max (lfd) from traponve "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and ls = ? "
									  "and blg_typ = ?");

	m_TrapoNve.sqlin  ((short *) &m_TrapoNve.traponve.mdn, SQLSHORT, 0);
	m_TrapoNve.sqlin  ((short *) &m_TrapoNve.traponve.fil, SQLSHORT, 0);
	m_TrapoNve.sqlin  ((long *)  &m_TrapoNve.traponve.ls,  SQLLONG, 0);
	m_TrapoNve.sqlin ((char *)  m_TrapoNve.traponve.blg_typ,  SQLCHAR, sizeof (m_TrapoNve.traponve.blg_typ));
	m_DeleteLsCursorNve = m_TrapoNve.sqlcursor ("delete from traponve "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and ls = ? "
									  "and blg_typ = ?");
	m_TrapoNve.sqlin  ((short *) &m_TrapoNve.traponve.mdn, SQLSHORT, 0);
	m_TrapoNve.sqlin  ((short *) &m_TrapoNve.traponve.fil, SQLSHORT, 0);
	m_TrapoNve.sqlin  ((long *)  &m_TrapoNve.traponve.ls,  SQLLONG, 0);
	m_TrapoNve.sqlin ((char *)  m_TrapoNve.traponve.blg_typ,  SQLCHAR, sizeof (m_TrapoNve.traponve.blg_typ));
	m_DeleteLsCursorPsPm = m_TrapoNve.sqlcursor ("delete from trapopspm "
			                          "where mdn = ? "
									  "and fil = ? "
									  "and ls = ? "
									  "and blg_typ = ?");
	m_LsCursor = -1;
	FillPtab ();
}

CNveDb::~CNveDb()
{
	m_TrapoNve.sqlclose (m_MaxCursor);
	m_TrapoNve.sqlclose (m_DeleteLsCursorNve);
	m_TrapoNve.sqlclose (m_DeleteLsCursorPsPm);
	if (m_LsCursor != -1)
	{
		m_TrapoNve.sqlclose (m_LsCursor);
	}

}

long CNveDb::GetMaxLfd ()
{
	m_Lfd = 0;
	if (m_TrapoNve.sqlopen (m_MaxCursor) == 0)
	{
		m_TrapoNve.sqlfetch (m_MaxCursor);
		if (m_Lfd < 0)
		{
			m_Lfd = 0;
		}
	}
    return m_Lfd + 1;
}

void CNveDb::InitNve ()
{
	memcpy (&m_TrapoNve.traponve, &traponve_null, sizeof (TRAPONVE));
}

void CNveDb::InitPsPm ()
{
	memcpy (&m_TrapoPsPm.trapopspm, &trapopspm_null, sizeof (TRAPOPSPM));
}

void CNveDb::DeleteLs ()
{
	m_TrapoNve.sqlexecute (m_DeleteLsCursorNve);
	m_TrapoPsPm.sqlexecute (m_DeleteLsCursorPsPm);
}

void CNveDb::UpdateNve ()
{
	m_TrapoNve.dbupdate ();
}

void CNveDb::UpdatePsPm ()
{
	m_TrapoPsPm.dbupdate ();
}

void CNveDb::FillPtab ()
{
	m_PtabTable.Clear ();
	int dsqlstatus = m_Ptab.lese_ptab_all (ME_EINH_LEER);
	while (dsqlstatus == 0)
	{
        m_PtabTable.Add (&ptabn);
		dsqlstatus = m_Ptab.lese_ptab_all ();
	}
}

void CNveDb::Start ()
{
	m_PtabTable.Start ();
}

PTABN *CNveDb::GetNext ()
{
	return m_PtabTable.GetNext ();
}

double CNveDb::GetArticle (LPSTR Name)
{
	double art = 0.0;
	LPSTR article = m_PtabTable.GetArticle (Name);
	if (article != NULL)
	{
		art = ratod (article);
	}
	return art;
}

void CNveDb::PrepareLsCursor ()
{
	if (m_LsCursor == -1)
	{
		m_TrapoNve.sqlout ((char *)  m_TrapoNve.traponve.nve,  SQLCHAR, sizeof (m_TrapoNve.traponve.nve));
		m_TrapoNve.sqlout ((long *)  &m_TrapoNve.traponve.lfd, SQLLONG, 0);
		m_TrapoNve.sqlin  ((short *) &m_TrapoNve.traponve.mdn, SQLSHORT, 0);
		m_TrapoNve.sqlin  ((short *) &m_TrapoNve.traponve.fil, SQLSHORT, 0);
		m_TrapoNve.sqlin  ((long *)  &m_TrapoNve.traponve.ls,  SQLLONG, 0);
		m_TrapoNve.sqlin ((char *)  m_TrapoNve.traponve.blg_typ,  SQLCHAR, sizeof (m_TrapoNve.traponve.blg_typ));
		m_LsCursor = m_TrapoNve.sqlcursor ("select nve, lfd from traponve "
			                              "where mdn = ? "
										  "and fil = ? "
										  "and ls = ? "
										  "and blg_typ = ?");
	}
}

void CNveDb::ReadFirst ()
{
	int dsqlstatus = 0;
	m_Error = FALSE;
	PrepareLsCursor ();
	if (m_LsCursor != -1)
	{
		dsqlstatus = m_TrapoNve.sqlopen (m_LsCursor);
	}
	if (dsqlstatus == 0)
	{
		dsqlstatus = m_TrapoNve.sqlfetch (m_LsCursor);
	}
	if (dsqlstatus == 0)
	{
		dsqlstatus = m_TrapoNve.dbreadfirst ();
	}
	if (dsqlstatus == 0)
	{
		m_TrapoPsPm.trapopspm.mdn = m_TrapoNve.traponve.mdn;
		m_TrapoPsPm.trapopspm.fil = m_TrapoNve.traponve.fil;
		m_TrapoPsPm.trapopspm.ls = m_TrapoNve.traponve.ls;
		m_TrapoPsPm.trapopspm.lfd = m_TrapoNve.traponve.lfd;
		strcpy (m_TrapoPsPm.trapopspm.blg_typ, m_TrapoNve.traponve.blg_typ);
        m_TrapoPsPm.dbreadfirst (); 
	}
	if (dsqlstatus != 0)
	{
		m_Error = TRUE;
		m_ErrorNumber = 1;
	}
}

void CNveDb::Read ()
{
	int dsqlstatus = 0;
	dsqlstatus = m_TrapoNve.sqlfetch (m_LsCursor);
	if (dsqlstatus == 0)
	{
		dsqlstatus = m_TrapoNve.dbreadfirst ();
	}
	if (dsqlstatus == 0)
	{
		m_TrapoPsPm.trapopspm.mdn = m_TrapoNve.traponve.mdn;
		m_TrapoPsPm.trapopspm.fil = m_TrapoNve.traponve.fil;
		m_TrapoPsPm.trapopspm.ls = m_TrapoNve.traponve.ls;
		m_TrapoPsPm.trapopspm.lfd = m_TrapoNve.traponve.lfd;
		strcpy (m_TrapoPsPm.trapopspm.blg_typ, m_TrapoNve.traponve.blg_typ);
        m_TrapoPsPm.dbreadfirst (); 
	}
	if (dsqlstatus != 0)
	{
		m_Error = TRUE;
		m_ErrorNumber = 1;
	}
}

const LPSTR CNveDb::GetErrorText ()
{
	LPSTR text = NULL;
	if (m_Error && m_ErrorNumber < sizeof (m_ErrorStrings) - 1)
	{
		text = m_ErrorStrings[m_ErrorNumber];
	}
	return text;
}
