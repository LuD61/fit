// NveStringTable.h: Schnittstelle f�r die Klasse CNveStringTable.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NVESTRINGTABLE_H__FD467EEA_8354_4346_AF04_A98C02B33B0B__INCLUDED_)
#define AFX_NVESTRINGTABLE_H__FD467EEA_8354_4346_AF04_A98C02B33B0B__INCLUDED_
#include "DataCollection.h"
#include "Text.h"

#define CString Text

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CNveStringTable : public CDataCollection<CString *>  
{
private:
	CString m_Iln;
	short           m_Mdn;
	short           m_Fil;
	long            m_Ls;
	CString         m_BlgTyp;
	BOOL            m_Error;
	int             m_ErrorNumber;
	CString         m_ErrorMessage;
public:
	BOOL Error ()
	{
		return m_Error;
	}

	LPSTR ErrorMessage ()
	{
		return m_ErrorMessage;
	}

	void set_Iln (LPSTR iln)
	{
		m_Iln = iln;
	}

	void set_Mdn (short mdn)
	{
		m_Mdn = mdn;
	}

	void set_Fil (short fil)
	{
		m_Fil = fil;
	}

	void set_Ls (long ls)
	{
		m_Ls = ls;
	}

	void set_BlgTyp (LPSTR blg_typ)
	{
		m_BlgTyp = blg_typ;
	}
	const LPSTR Iln ()
	{
		return m_Iln;
	}

	CNveStringTable();
	virtual ~CNveStringTable();
	virtual void DestroyElements ();
	void AddString (LPSTR);
	void Run ();
};

#endif // !defined(AFX_NVESTRINGTABLE_H__FD467EEA_8354_4346_AF04_A98C02B33B0B__INCLUDED_)
