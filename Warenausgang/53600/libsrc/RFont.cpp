#include "RFont.h"

RFont::RFont ()
{
	Name = "";
	Width = 0;
	Height = 0;
	Attributes = 0;
	Font = NULL;
	Instances = 0;
}

RFont::RFont (Text Name, int Width, int Height, int Attributes, HFONT Font)
{
	this->Name = Name;
	this->Width = Width;
	this->Height = Height;
	this->Attributes = Attributes;
	this->Font = Font;
	Instances = 0;
}


RFont::RFont (char * Name, int Width, int Height, int Attributes, HFONT Font)
{
	this->Name = Name;
	this->Width = Width;
	this->Height = Height;
	this->Attributes = Attributes;
	this->Font = Font;
	Instances = 0;
}

RFont::~RFont ()
{
}

BOOL RFont::operator== (RFont& rFont)
{
	if (Font == NULL)
	{
		return FALSE;
	}
	if (rFont.GetName () != Name)
	{
		return FALSE;
	}
	if (rFont.GetWidth () != Width)
	{
		return FALSE;
	}
	if (rFont.GetHeight () != Height)
	{
		return FALSE;
	}
	if (rFont.GetAttributes () != Attributes)
	{
		return FALSE;
	}
	return TRUE;
}

RFont& RFont::operator=(RFont& rFont)
{
	Name       = rFont.GetName ();
	Width      = rFont.GetWidth ();
	Height     = rFont.GetHeight ();
	Attributes = rFont.GetAttributes ();
	return *this;
}

void RFont::SetName (Text& Name)
{
	this->Name = Name;
}

Text& RFont::GetName ()
{
	return Name;
}

void RFont::SetWidth (int Width)
{
	this->Width = Width;
}

int RFont::GetWidth ()
{
	return Width;
}

void RFont::SetHeight (int Height)
{
	this->Height = Height;
}

int RFont::GetHeight ()
{
	return Height;
}

void RFont::SetAttributes (int Attributes)
{
	this->Attributes = Attributes;
}

int RFont::GetAttributes ()
{
	return Attributes;
}

void RFont::SetFont (HFONT Font)
{
	this->Font = Font;
}

HFONT RFont::GetFont ()
{
	return Font;
}

void RFont::inc ()
{
	Instances ++;
}

void RFont::dec ()
{
	if (Instances > 0)
	{
	     Instances --;
	}
}

int RFont::GetInstances ()
{
	return Instances;
}