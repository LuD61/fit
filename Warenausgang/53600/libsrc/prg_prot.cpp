#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "wmaskc.h"
#include "dbclass.h"
#include "dbfunc.h"
#include "prg_prot.h"

struct PRG_PROT prg_prot, prg_prot_null;

void PRG_PROT_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *)   &prg_prot.mdn, 1, 0);
            ins_quest ((char *)   &prg_prot.pr_gr_stuf, 2, 0);
            ins_quest ((char *)   &prg_prot.kun_pr, 2, 0);
            ins_quest ((char *)   &prg_prot.kun, 2, 0);
            ins_quest ((char *)   &prg_prot.bearb, 2, 0);
            ins_quest ((char *)   &prg_prot.a,  3, 0);
    out_quest ((char *) &prg_prot.propramm,2,0);
    out_quest ((char *) &prg_prot.bearb,2,0);
    out_quest ((char *) prg_prot.zeit,0,9);
    out_quest ((char *) prg_prot.pers_nam,0,9);
    out_quest ((char *) &prg_prot.mdn,1,0);
    out_quest ((char *) &prg_prot.pr_gr_stuf,2,0);
    out_quest ((char *) &prg_prot.kun,2,0);
    out_quest ((char *) &prg_prot.kun_pr,2,0);
    out_quest ((char *) &prg_prot.a,3,0);
    out_quest ((char *) &prg_prot.vk_pr_i,3,0);
    out_quest ((char *) &prg_prot.vk_pr_eu,3,0);
    out_quest ((char *) &prg_prot.ld_pr,3,0);
    out_quest ((char *) &prg_prot.ld_pr_eu,3,0);
    out_quest ((char *) &prg_prot.aktion_nr,1,0);
    out_quest ((char *) &prg_prot.aki_von,2,0);
    out_quest ((char *) &prg_prot.aki_bis,2,0);
    out_quest ((char *) &prg_prot.loesch,1,0);
    out_quest ((char *) prg_prot.kond_art,0,5);
    out_quest ((char *) prg_prot.tabelle,0,21);
            cursor = prepare_sql ("select prg_prot.propramm,  "
"prg_prot.bearb,  prg_prot.zeit,  prg_prot.pers_nam,  prg_prot.mdn,  "
"prg_prot.pr_gr_stuf,  prg_prot.kun,  prg_prot.kun_pr,  prg_prot.a,  "
"prg_prot.vk_pr_i,  prg_prot.vk_pr_eu,  prg_prot.ld_pr,  "
"prg_prot.ld_pr_eu,  prg_prot.aktion_nr,  prg_prot.aki_von,  "
"prg_prot.aki_bis,  prg_prot.loesch,  prg_prot.kond_art,  "
"prg_prot.tabelle from prg_prot "

#line 25 "prg_prot.rpp"
                                  "where mdn = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? " 
                                  "and kun = ? "
                                  "and bearb = ? "  
                                  "and   a   = ?");
    ins_quest ((char *) &prg_prot.propramm,2,0);
    ins_quest ((char *) &prg_prot.bearb,2,0);
    ins_quest ((char *) prg_prot.zeit,0,9);
    ins_quest ((char *) prg_prot.pers_nam,0,9);
    ins_quest ((char *) &prg_prot.mdn,1,0);
    ins_quest ((char *) &prg_prot.pr_gr_stuf,2,0);
    ins_quest ((char *) &prg_prot.kun,2,0);
    ins_quest ((char *) &prg_prot.kun_pr,2,0);
    ins_quest ((char *) &prg_prot.a,3,0);
    ins_quest ((char *) &prg_prot.vk_pr_i,3,0);
    ins_quest ((char *) &prg_prot.vk_pr_eu,3,0);
    ins_quest ((char *) &prg_prot.ld_pr,3,0);
    ins_quest ((char *) &prg_prot.ld_pr_eu,3,0);
    ins_quest ((char *) &prg_prot.aktion_nr,1,0);
    ins_quest ((char *) &prg_prot.aki_von,2,0);
    ins_quest ((char *) &prg_prot.aki_bis,2,0);
    ins_quest ((char *) &prg_prot.loesch,1,0);
    ins_quest ((char *) prg_prot.kond_art,0,5);
    ins_quest ((char *) prg_prot.tabelle,0,21);
            sqltext = "update prg_prot set "
"prg_prot.propramm = ?,  prg_prot.bearb = ?,  prg_prot.zeit = ?,  "
"prg_prot.pers_nam = ?,  prg_prot.mdn = ?,  prg_prot.pr_gr_stuf = ?,  "
"prg_prot.kun = ?,  prg_prot.kun_pr = ?,  prg_prot.a = ?,  "
"prg_prot.vk_pr_i = ?,  prg_prot.vk_pr_eu = ?,  prg_prot.ld_pr = ?,  "
"prg_prot.ld_pr_eu = ?,  prg_prot.aktion_nr = ?,  "
"prg_prot.aki_von = ?,  prg_prot.aki_bis = ?,  prg_prot.loesch = ?,  "
"prg_prot.kond_art = ?,  prg_prot.tabelle = ? "

#line 32 "prg_prot.rpp"
                                  "where mdn = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? " 
                                  "and kun = ? "
                                  "and bearb = ? "  
                                  "and   a   = ?";
            ins_quest ((char *)   &prg_prot.mdn, 1, 0);
            ins_quest ((char *)   &prg_prot.pr_gr_stuf, 2, 0);
            ins_quest ((char *)   &prg_prot.kun_pr, 2, 0);
            ins_quest ((char *)   &prg_prot.kun, 2, 0);
            ins_quest ((char *)   &prg_prot.bearb, 2, 0);
            ins_quest ((char *)   &prg_prot.a,  3, 0);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &prg_prot.mdn, 1, 0);
            ins_quest ((char *)   &prg_prot.pr_gr_stuf, 2, 0);
            ins_quest ((char *)   &prg_prot.kun_pr, 2, 0);
            ins_quest ((char *)   &prg_prot.kun, 2, 0);
            ins_quest ((char *)   &prg_prot.bearb, 2, 0);
            ins_quest ((char *)   &prg_prot.a,  3, 0);
            test_upd_cursor = prepare_sql ("select a from prg_prot "
                                  "where mdn = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? " 
                                  "and kun = ? "
                                  "and bearb = ? "  
                                  "and   a   = ? "
                                  "for update");
            ins_quest ((char *) &prg_prot.mdn, 1, 0);
            ins_quest ((char *)   &prg_prot.pr_gr_stuf, 2, 0);
            ins_quest ((char *)   &prg_prot.kun_pr, 2, 0);
            ins_quest ((char *)   &prg_prot.kun, 2, 0);
            ins_quest ((char *)   &prg_prot.bearb, 2, 0);
            ins_quest ((char *)   &prg_prot.a,  3, 0);
            del_cursor = prepare_sql ("delete from prg_prot "
                                  "where mdn = ? "
                                  "and   pr_gr_stuf = ? "
                                  "and kun_pr = ? " 
                                  "and kun = ? "
                                  "and bearb = ? "  
                                  "and   a   = ?");
    ins_quest ((char *) &prg_prot.propramm,2,0);
    ins_quest ((char *) &prg_prot.bearb,2,0);
    ins_quest ((char *) prg_prot.zeit,0,9);
    ins_quest ((char *) prg_prot.pers_nam,0,9);
    ins_quest ((char *) &prg_prot.mdn,1,0);
    ins_quest ((char *) &prg_prot.pr_gr_stuf,2,0);
    ins_quest ((char *) &prg_prot.kun,2,0);
    ins_quest ((char *) &prg_prot.kun_pr,2,0);
    ins_quest ((char *) &prg_prot.a,3,0);
    ins_quest ((char *) &prg_prot.vk_pr_i,3,0);
    ins_quest ((char *) &prg_prot.vk_pr_eu,3,0);
    ins_quest ((char *) &prg_prot.ld_pr,3,0);
    ins_quest ((char *) &prg_prot.ld_pr_eu,3,0);
    ins_quest ((char *) &prg_prot.aktion_nr,1,0);
    ins_quest ((char *) &prg_prot.aki_von,2,0);
    ins_quest ((char *) &prg_prot.aki_bis,2,0);
    ins_quest ((char *) &prg_prot.loesch,1,0);
    ins_quest ((char *) prg_prot.kond_art,0,5);
    ins_quest ((char *) prg_prot.tabelle,0,21);
            ins_cursor = prepare_sql ("insert into prg_prot ("
"propramm,  bearb,  zeit,  pers_nam,  mdn,  pr_gr_stuf,  kun,  kun_pr,  a,  vk_pr_i,  "
"vk_pr_eu,  ld_pr,  ld_pr_eu,  aktion_nr,  aki_von,  aki_bis,  loesch,  kond_art,  "
"tabelle) "

#line 74 "prg_prot.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?)"); 

#line 76 "prg_prot.rpp"
}

int PRG_PROT_CLASS::dbupdate (void)
{
         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         return execute_curs (ins_cursor);
} 


