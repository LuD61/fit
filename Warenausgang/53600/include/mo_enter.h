#ifndef _MO_FENTER_DEF
#define _MO_FENTER_DEF
#include "wmaskc.h"
#include "cmask.h"


#define STDSZ 120 
#define BUSIZE 100 


static StdSz = STDSZ;
static BuSize  = BUSIZE;

static mfont textfont = {
//	                     "MS SANS SERIF", 
	                     "Arial", 
                          StdSz, 0, 0,
                          BLACKCOL,
                          GRAYCOL,
                          1,

                          NULL};
static mfont bufont = {
//	                     "MS SANS SERIF", 
	                     "Arial", 
                          BuSize, 0, 0,
                          BLACKCOL,
                          GRAYCOL,
                          1,
                          NULL};

class fEnter
{
          private :
		        HWND hMainWindow;
		        HINSTANCE hInstance;
		        HWND hWnd;
                mfont *Font;
		        DWORD currentfield;
                int x,y, cx, cy;
		        char *Caption;
				BOOL FocusSet;
				DWORD aktId;
				COLORREF BkColor;
				static int (*FpProg) (void);
                static CFIELD **_fWork;
                static CFORM *fWork;
				static BOOL IsModal;
                static int StdSize;

          public :
		        fEnter (int, int, int, int, char *);
		        ~fEnter ();
				void SetFpProg (int (*FpProg) (void))
				{
					this->FpProg = FpProg;
				}

				void SetBkColor (COLORREF);
                BOOL OnKeyup (HWND, WPARAM, LPARAM);
                BOOL OnKeydown (HWND, WPARAM, LPARAM);
 		        void ProcessMessages (void);
				static void GetWindowText (HWND);
				static void SetWindowText (HWND);
                static void ShowDlg (HDC, form *);
                static CALLBACK CProc(HWND,UINT, WPARAM,LPARAM);
                HWND OpenWindow (HANDLE, HWND, DWORD);
                void DestroyWindow (void);
                void MoveWindow (void);
		        void SetMask (CFORM *frm);
		        void SetFunk ();
		        void SetFocus (DWORD);
};
#endif

