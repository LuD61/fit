// RowEvent.h: Schnittstelle f�r die Klasse CRowEvent.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROWEVENT_H__868608DD_7B2D_417D_9D80_0E481FC8F029__INCLUDED_)
#define AFX_ROWEVENT_H__868608DD_7B2D_417D_9D80_0E481FC8F029__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>

class CRowEvent  
{
public:
	CRowEvent();
	virtual ~CRowEvent();
    virtual void AfterPaint (HDC hdc, RECT *rect, int pos, int SubRow);
};

#endif // !defined(AFX_ROWEVENT_H__868608DD_7B2D_417D_9D80_0E481FC8F029__INCLUDED_)
