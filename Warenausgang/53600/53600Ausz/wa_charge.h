#ifndef _WA_CHARGE_DEF
#define _WA_CHARGE_DEF
#include "wmask.h"
#include "dbclass.h"

struct WA_CHARGE {
   double    a;
   char      charge[31];
};
extern struct WA_CHARGE wa_charge, wa_charge_null;

#line 7 "wa_charge.rh"

class WA_CHARGE_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               WA_CHARGE wa_charge;
               WA_CHARGE_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (WA_CHARGE&);  
               void operator= (WA_CHARGE&);  
};
#endif
