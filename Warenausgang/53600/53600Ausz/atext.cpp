#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <tchar.h>
#include "wmask.h"
#include "strfkt.h"
#include "mo_meld.h"
#include "mo_curso.h"
#include "atext.h"

struct ATEXTE atexte, atexte_null;

void ATEXTE_CLASS::prepare_atexte (void)
{
    ins_quest ((char *) &atexte.txt_nr, 2, 0);
    out_quest ((char *) &atexte.sys,2,0);
    out_quest ((char *) &atexte.waa,1,0);
    out_quest ((char *) atexte.txt,0,1025);
    out_quest ((char *) atexte.disp_txt,0,1025);
    out_quest ((char *) &atexte.alignment,1,0);
    out_quest ((char *) &atexte.send_ctrl,1,0);
    out_quest ((char *) &atexte.txt_typ,1,0);
    out_quest ((char *) &atexte.txt_platz,1,0);
    out_quest ((char *) &atexte.a,3,0);
    cursor_atexte = prepare_sql (_T("select atexte.sys,  "
"atexte.waa,  atexte.txt,  atexte.disp_txt,  "
"atexte.alignment,  atexte.send_ctrl,  atexte.txt_typ,  "
"atexte.txt_platz,  atexte.a from atexte where txt_nr = ? "));

}

void ATEXTE_CLASS::prepare_atexte_a (void)
{
    ins_quest ((char *) &atexte.a, 3, 0);
    out_quest ((char *) &atexte.sys,2,0);
    out_quest ((char *) &atexte.waa,1,0);
    out_quest ((char *) &atexte.txt_nr,2,0);
    out_quest ((char *) atexte.txt,0,1025);
    out_quest ((char *) atexte.disp_txt,0,1025);
    out_quest ((char *) &atexte.alignment,1,0);
    out_quest ((char *) &atexte.send_ctrl,1,0);
    out_quest ((char *) &atexte.txt_typ,1,0);
    out_quest ((char *) &atexte.txt_platz,1,0);
    cursor_atexte_a = prepare_sql (_T("select atexte.sys,  "
"atexte.waa,  atexte.txt_nr,  atexte.txt,  atexte.disp_txt,  "
"atexte.alignment,  atexte.send_ctrl,  atexte.txt_typ,  "
"atexte.txt_platz  from atexte where a = ? "));

}

int ATEXTE_CLASS::lese_atexte ( long txt_nr )
{
    long sqlstatus1;
    if (cursor_atexte == -1)
    {
        prepare_atexte ();
    }
    atexte.txt_nr = txt_nr;
    open_sql (cursor_atexte);
    fetch_sql (cursor_atexte);
    sqlstatus1 = sqlstatus;
    if ( sqlstatus1 )
    {
        atexte.txt[0] = '\0';
    }
    else
    {
        clipped( atexte.txt);
    }
    /* writelog ("atexte.txt_nr=%ld sqlstatus=%ld", atexte.txt_nr, sqlstatus1); GK 08.08.2013 */
    close_sql (cursor_atexte);
	cursor_atexte = -1;
    return sqlstatus1;
}

/* GK 28.07.2013 Text mit ArtNr holen */
int ATEXTE_CLASS::lese_atexte_a ( double a )
{
    long sqlstatus1;
    if (cursor_atexte_a == -1)
    {
        prepare_atexte_a ();
    }
    atexte.a = a;
    open_sql (cursor_atexte_a);
    fetch_sql (cursor_atexte_a);
    sqlstatus1 = sqlstatus;
    if ( sqlstatus1 )
    {
        atexte.txt[0] = '\0';
    }
    else
    {
        clipped( atexte.txt);
    }
    /* writelog ("atexte.txt_nr=%ld sqlstatus=%ld", atexte.txt_nr, sqlstatus1); GK 08.08.2013 */
    close_sql (cursor_atexte_a);
	cursor_atexte_a = -1;
    return sqlstatus1;
}

