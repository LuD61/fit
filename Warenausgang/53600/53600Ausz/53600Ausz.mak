# Von Microsoft Developer Studio generierte NMAKE-Datei, basierend auf 53600Ausz.dsp
!IF "$(CFG)" == ""
CFG=53600 - Win32 Debug
!MESSAGE Keine Konfiguration angegeben. 53600 - Win32 Debug wird als Standard\
 verwendet.
!ENDIF 

!IF "$(CFG)" != "53600 - Win32 Release" && "$(CFG)" != "53600 - Win32 Debug"
!MESSAGE Ung�ltige Konfiguration "$(CFG)" angegeben.
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "53600Ausz.mak" CFG="53600 - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "53600 - Win32 Release" (basierend auf  "Win32 (x86) Application")
!MESSAGE "53600 - Win32 Debug" (basierend auf  "Win32 (x86) Application")
!MESSAGE 
!ERROR Eine ung�ltige Konfiguration wurde angegeben.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "53600 - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

!IF "$(RECURSE)" == "0" 

ALL : "..\..\..\..\user\fit\bin\53600Ausz.exe"

!ELSE 

ALL : "..\..\..\..\user\fit\bin\53600Ausz.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\A_BAS.OBJ"
	-@erase "$(INTDIR)\A_HNDW.OBJ"
	-@erase "$(INTDIR)\a_kun_gx.obj"
	-@erase "$(INTDIR)\A_PR.OBJ"
	-@erase "$(INTDIR)\akt_ipr.obj"
	-@erase "$(INTDIR)\akt_iprz.obj"
	-@erase "$(INTDIR)\AKT_KRZ.OBJ"
	-@erase "$(INTDIR)\AKTION.OBJ"
	-@erase "$(INTDIR)\atext.obj"
	-@erase "$(INTDIR)\auto_nr.obj"
	-@erase "$(INTDIR)\BkBitmap.obj"
	-@erase "$(INTDIR)\bsd_buch.obj"
	-@erase "$(INTDIR)\cmask.obj"
	-@erase "$(INTDIR)\DataCollection.obj"
	-@erase "$(INTDIR)\DATUM.OBJ"
	-@erase "$(INTDIR)\dbclass.obj"
	-@erase "$(INTDIR)\DBFUNC.OBJ"
	-@erase "$(INTDIR)\DbTime.obj"
	-@erase "$(INTDIR)\ddeclient.obj"
	-@erase "$(INTDIR)\Decimal.obj"
	-@erase "$(INTDIR)\DesAdv.obj"
	-@erase "$(INTDIR)\DllPreise.obj"
	-@erase "$(INTDIR)\FIL.OBJ"
	-@erase "$(INTDIR)\Integer.obj"
	-@erase "$(INTDIR)\ipr.obj"
	-@erase "$(INTDIR)\iprz.obj"
	-@erase "$(INTDIR)\Karton.obj"
	-@erase "$(INTDIR)\KOMIS.OBJ"
	-@erase "$(INTDIR)\KOMIS.res"
	-@erase "$(INTDIR)\kumebest.obj"
	-@erase "$(INTDIR)\KUN.OBJ"
	-@erase "$(INTDIR)\kun_leerg.obj"
	-@erase "$(INTDIR)\leer_bel.obj"
	-@erase "$(INTDIR)\LeerGut.obj"
	-@erase "$(INTDIR)\LeergutPart.obj"
	-@erase "$(INTDIR)\LicenceDialog.obj"
	-@erase "$(INTDIR)\lizenz.obj"
	-@erase "$(INTDIR)\Log.obj"
	-@erase "$(INTDIR)\LS.OBJ"
	-@erase "$(INTDIR)\ls_einzel.obj"
	-@erase "$(INTDIR)\ls_txt.obj"
	-@erase "$(INTDIR)\lsnve.obj"
	-@erase "$(INTDIR)\MDN.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\mo_abasis.obj"
	-@erase "$(INTDIR)\mo_auto.obj"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_DRAW.OBJ"
	-@erase "$(INTDIR)\MO_EINH.OBJ"
	-@erase "$(INTDIR)\mo_gdruck.obj"
	-@erase "$(INTDIR)\mo_geb.obj"
	-@erase "$(INTDIR)\MO_MELD.OBJ"
	-@erase "$(INTDIR)\MO_MENU.OBJ"
	-@erase "$(INTDIR)\mo_nr.obj"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\mo_pasc.obj"
	-@erase "$(INTDIR)\MO_PASSW.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\mo_progcfg.obj"
	-@erase "$(INTDIR)\mo_smtg.obj"
	-@erase "$(INTDIR)\nublock.obj"
	-@erase "$(INTDIR)\Nve.obj"
	-@erase "$(INTDIR)\NveDb.obj"
	-@erase "$(INTDIR)\NveGew.obj"
	-@erase "$(INTDIR)\NveStringTable.obj"
	-@erase "$(INTDIR)\Palette.obj"
	-@erase "$(INTDIR)\pers.obj"
	-@erase "$(INTDIR)\pht_wg.obj"
	-@erase "$(INTDIR)\pr_a_gx.obj"
	-@erase "$(INTDIR)\prdk.obj"
	-@erase "$(INTDIR)\Process.obj"
	-@erase "$(INTDIR)\prov_satz.obj"
	-@erase "$(INTDIR)\prp_user.obj"
	-@erase "$(INTDIR)\PTAB.OBJ"
	-@erase "$(INTDIR)\PtabFunc.obj"
	-@erase "$(INTDIR)\PtabTable.obj"
	-@erase "$(INTDIR)\ReadNve.obj"
	-@erase "$(INTDIR)\scanean.obj"
	-@erase "$(INTDIR)\Sounds.obj"
	-@erase "$(INTDIR)\Stc.obj"
	-@erase "$(INTDIR)\STDFKT.OBJ"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\sys_par.obj"
	-@erase "$(INTDIR)\SYS_PERI.OBJ"
	-@erase "$(INTDIR)\Text.obj"
	-@erase "$(INTDIR)\textblock.obj"
	-@erase "$(INTDIR)\touchf.obj"
	-@erase "$(INTDIR)\TransHandler.obj"
	-@erase "$(INTDIR)\traponve.obj"
	-@erase "$(INTDIR)\trapopspm.obj"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\Vector.obj"
	-@erase "$(INTDIR)\wa_charge.obj"
	-@erase "$(INTDIR)\WApplication.obj"
	-@erase "$(INTDIR)\WDialog.obj"
	-@erase "$(INTDIR)\wiepro.obj"
	-@erase "$(INTDIR)\WMASK.OBJ"
	-@erase "$(INTDIR)\writenve.obj"
	-@erase "$(INTDIR)\zerldaten.obj"
	-@erase "..\..\..\..\user\fit\bin\53600Ausz.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /ML /W3 /GX /O2 /I "d:\informix\incl\esql" /I\
 "$(INFORMIXDIR)\incl\esql723tc9" /I "$(INFORMIXDIR)\incl\esql" /I "..\include"\
 /I "..\53600" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "FR_NUMMER" /D "OLDMASK"\
 /Fp"$(INTDIR)\53600Ausz.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
RSC_PROJ=/l 0x407 /fo"$(INTDIR)\KOMIS.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\53600Ausz.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=comctl32.lib isqlt07c.lib kernel32.lib user32.lib gdi32.lib\
 winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib\
 uuid.lib odbc32.lib odbccp32.lib winmm.lib /nologo /subsystem:windows\
 /incremental:no /pdb:"$(OUTDIR)\53600Ausz.pdb" /machine:I386\
 /out:"d:\user\fit\bin\53600Ausz.exe" /libpath:"d:\informix\lib" 
LINK32_OBJS= \
	"$(INTDIR)\A_BAS.OBJ" \
	"$(INTDIR)\A_HNDW.OBJ" \
	"$(INTDIR)\a_kun_gx.obj" \
	"$(INTDIR)\A_PR.OBJ" \
	"$(INTDIR)\akt_ipr.obj" \
	"$(INTDIR)\akt_iprz.obj" \
	"$(INTDIR)\AKT_KRZ.OBJ" \
	"$(INTDIR)\AKTION.OBJ" \
	"$(INTDIR)\atext.obj" \
	"$(INTDIR)\auto_nr.obj" \
	"$(INTDIR)\BkBitmap.obj" \
	"$(INTDIR)\bsd_buch.obj" \
	"$(INTDIR)\cmask.obj" \
	"$(INTDIR)\DataCollection.obj" \
	"$(INTDIR)\DATUM.OBJ" \
	"$(INTDIR)\dbclass.obj" \
	"$(INTDIR)\DBFUNC.OBJ" \
	"$(INTDIR)\DbTime.obj" \
	"$(INTDIR)\ddeclient.obj" \
	"$(INTDIR)\Decimal.obj" \
	"$(INTDIR)\DesAdv.obj" \
	"$(INTDIR)\DllPreise.obj" \
	"$(INTDIR)\FIL.OBJ" \
	"$(INTDIR)\Integer.obj" \
	"$(INTDIR)\ipr.obj" \
	"$(INTDIR)\iprz.obj" \
	"$(INTDIR)\Karton.obj" \
	"$(INTDIR)\KOMIS.OBJ" \
	"$(INTDIR)\KOMIS.res" \
	"$(INTDIR)\kumebest.obj" \
	"$(INTDIR)\KUN.OBJ" \
	"$(INTDIR)\kun_leerg.obj" \
	"$(INTDIR)\leer_bel.obj" \
	"$(INTDIR)\LeerGut.obj" \
	"$(INTDIR)\LeergutPart.obj" \
	"$(INTDIR)\LicenceDialog.obj" \
	"$(INTDIR)\lizenz.obj" \
	"$(INTDIR)\Log.obj" \
	"$(INTDIR)\LS.OBJ" \
	"$(INTDIR)\ls_einzel.obj" \
	"$(INTDIR)\ls_txt.obj" \
	"$(INTDIR)\lsnve.obj" \
	"$(INTDIR)\MDN.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\mo_abasis.obj" \
	"$(INTDIR)\mo_auto.obj" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_DRAW.OBJ" \
	"$(INTDIR)\MO_EINH.OBJ" \
	"$(INTDIR)\mo_gdruck.obj" \
	"$(INTDIR)\mo_geb.obj" \
	"$(INTDIR)\MO_MELD.OBJ" \
	"$(INTDIR)\MO_MENU.OBJ" \
	"$(INTDIR)\mo_nr.obj" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\mo_pasc.obj" \
	"$(INTDIR)\MO_PASSW.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\mo_progcfg.obj" \
	"$(INTDIR)\mo_smtg.obj" \
	"$(INTDIR)\nublock.obj" \
	"$(INTDIR)\Nve.obj" \
	"$(INTDIR)\NveDb.obj" \
	"$(INTDIR)\NveGew.obj" \
	"$(INTDIR)\NveStringTable.obj" \
	"$(INTDIR)\Palette.obj" \
	"$(INTDIR)\pers.obj" \
	"$(INTDIR)\pht_wg.obj" \
	"$(INTDIR)\pr_a_gx.obj" \
	"$(INTDIR)\prdk.obj" \
	"$(INTDIR)\Process.obj" \
	"$(INTDIR)\prov_satz.obj" \
	"$(INTDIR)\prp_user.obj" \
	"$(INTDIR)\PTAB.OBJ" \
	"$(INTDIR)\PtabFunc.obj" \
	"$(INTDIR)\PtabTable.obj" \
	"$(INTDIR)\ReadNve.obj" \
	"$(INTDIR)\scanean.obj" \
	"$(INTDIR)\Sounds.obj" \
	"$(INTDIR)\Stc.obj" \
	"$(INTDIR)\STDFKT.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\sys_par.obj" \
	"$(INTDIR)\SYS_PERI.OBJ" \
	"$(INTDIR)\Text.obj" \
	"$(INTDIR)\textblock.obj" \
	"$(INTDIR)\touchf.obj" \
	"$(INTDIR)\TransHandler.obj" \
	"$(INTDIR)\traponve.obj" \
	"$(INTDIR)\trapopspm.obj" \
	"$(INTDIR)\Vector.obj" \
	"$(INTDIR)\wa_charge.obj" \
	"$(INTDIR)\WApplication.obj" \
	"$(INTDIR)\WDialog.obj" \
	"$(INTDIR)\wiepro.obj" \
	"$(INTDIR)\WMASK.OBJ" \
	"$(INTDIR)\writenve.obj" \
	"$(INTDIR)\zerldaten.obj" \
	"..\lib\etubidll.lib" \
	"..\lib\kasseex.lib" \
	".\FNB.LIB"

"..\..\..\..\user\fit\bin\53600Ausz.exe" : "$(OUTDIR)" $(DEF_FILE)\
 $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Anfang eines benutzerdefinierten Makros
OutDir=.\Debug
# Ende eines benutzerdefinierten Makros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\53600Ausz.exe" "$(OUTDIR)\53600Ausz.bsc"

!ELSE 

ALL : "$(OUTDIR)\53600Ausz.exe" "$(OUTDIR)\53600Ausz.bsc"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\A_BAS.OBJ"
	-@erase "$(INTDIR)\A_BAS.SBR"
	-@erase "$(INTDIR)\A_HNDW.OBJ"
	-@erase "$(INTDIR)\A_HNDW.SBR"
	-@erase "$(INTDIR)\a_kun_gx.obj"
	-@erase "$(INTDIR)\a_kun_gx.sbr"
	-@erase "$(INTDIR)\A_PR.OBJ"
	-@erase "$(INTDIR)\A_PR.SBR"
	-@erase "$(INTDIR)\akt_ipr.obj"
	-@erase "$(INTDIR)\akt_ipr.sbr"
	-@erase "$(INTDIR)\akt_iprz.obj"
	-@erase "$(INTDIR)\akt_iprz.sbr"
	-@erase "$(INTDIR)\AKT_KRZ.OBJ"
	-@erase "$(INTDIR)\AKT_KRZ.SBR"
	-@erase "$(INTDIR)\AKTION.OBJ"
	-@erase "$(INTDIR)\AKTION.SBR"
	-@erase "$(INTDIR)\atext.obj"
	-@erase "$(INTDIR)\atext.sbr"
	-@erase "$(INTDIR)\auto_nr.obj"
	-@erase "$(INTDIR)\auto_nr.sbr"
	-@erase "$(INTDIR)\BkBitmap.obj"
	-@erase "$(INTDIR)\BkBitmap.sbr"
	-@erase "$(INTDIR)\bsd_buch.obj"
	-@erase "$(INTDIR)\bsd_buch.sbr"
	-@erase "$(INTDIR)\cmask.obj"
	-@erase "$(INTDIR)\cmask.sbr"
	-@erase "$(INTDIR)\DataCollection.obj"
	-@erase "$(INTDIR)\DataCollection.sbr"
	-@erase "$(INTDIR)\DATUM.OBJ"
	-@erase "$(INTDIR)\DATUM.SBR"
	-@erase "$(INTDIR)\dbclass.obj"
	-@erase "$(INTDIR)\dbclass.sbr"
	-@erase "$(INTDIR)\DBFUNC.OBJ"
	-@erase "$(INTDIR)\DBFUNC.SBR"
	-@erase "$(INTDIR)\DbTime.obj"
	-@erase "$(INTDIR)\DbTime.sbr"
	-@erase "$(INTDIR)\ddeclient.obj"
	-@erase "$(INTDIR)\ddeclient.sbr"
	-@erase "$(INTDIR)\Decimal.obj"
	-@erase "$(INTDIR)\Decimal.sbr"
	-@erase "$(INTDIR)\DesAdv.obj"
	-@erase "$(INTDIR)\DesAdv.sbr"
	-@erase "$(INTDIR)\DllPreise.obj"
	-@erase "$(INTDIR)\DllPreise.sbr"
	-@erase "$(INTDIR)\FIL.OBJ"
	-@erase "$(INTDIR)\FIL.SBR"
	-@erase "$(INTDIR)\Integer.obj"
	-@erase "$(INTDIR)\Integer.sbr"
	-@erase "$(INTDIR)\ipr.obj"
	-@erase "$(INTDIR)\ipr.sbr"
	-@erase "$(INTDIR)\iprz.obj"
	-@erase "$(INTDIR)\iprz.sbr"
	-@erase "$(INTDIR)\Karton.obj"
	-@erase "$(INTDIR)\Karton.sbr"
	-@erase "$(INTDIR)\KOMIS.OBJ"
	-@erase "$(INTDIR)\KOMIS.res"
	-@erase "$(INTDIR)\KOMIS.SBR"
	-@erase "$(INTDIR)\kumebest.obj"
	-@erase "$(INTDIR)\kumebest.sbr"
	-@erase "$(INTDIR)\KUN.OBJ"
	-@erase "$(INTDIR)\KUN.SBR"
	-@erase "$(INTDIR)\kun_leerg.obj"
	-@erase "$(INTDIR)\kun_leerg.sbr"
	-@erase "$(INTDIR)\leer_bel.obj"
	-@erase "$(INTDIR)\leer_bel.sbr"
	-@erase "$(INTDIR)\LeerGut.obj"
	-@erase "$(INTDIR)\LeerGut.sbr"
	-@erase "$(INTDIR)\LeergutPart.obj"
	-@erase "$(INTDIR)\LeergutPart.sbr"
	-@erase "$(INTDIR)\LicenceDialog.obj"
	-@erase "$(INTDIR)\LicenceDialog.sbr"
	-@erase "$(INTDIR)\lizenz.obj"
	-@erase "$(INTDIR)\lizenz.sbr"
	-@erase "$(INTDIR)\Log.obj"
	-@erase "$(INTDIR)\Log.sbr"
	-@erase "$(INTDIR)\LS.OBJ"
	-@erase "$(INTDIR)\LS.SBR"
	-@erase "$(INTDIR)\ls_einzel.obj"
	-@erase "$(INTDIR)\ls_einzel.sbr"
	-@erase "$(INTDIR)\ls_txt.obj"
	-@erase "$(INTDIR)\ls_txt.sbr"
	-@erase "$(INTDIR)\lsnve.obj"
	-@erase "$(INTDIR)\lsnve.sbr"
	-@erase "$(INTDIR)\MDN.OBJ"
	-@erase "$(INTDIR)\MDN.SBR"
	-@erase "$(INTDIR)\MO_A_PR.OBJ"
	-@erase "$(INTDIR)\MO_A_PR.SBR"
	-@erase "$(INTDIR)\mo_abasis.obj"
	-@erase "$(INTDIR)\mo_abasis.sbr"
	-@erase "$(INTDIR)\mo_auto.obj"
	-@erase "$(INTDIR)\mo_auto.sbr"
	-@erase "$(INTDIR)\MO_CURSO.OBJ"
	-@erase "$(INTDIR)\MO_CURSO.SBR"
	-@erase "$(INTDIR)\MO_DRAW.OBJ"
	-@erase "$(INTDIR)\MO_DRAW.SBR"
	-@erase "$(INTDIR)\MO_EINH.OBJ"
	-@erase "$(INTDIR)\MO_EINH.SBR"
	-@erase "$(INTDIR)\mo_gdruck.obj"
	-@erase "$(INTDIR)\mo_gdruck.sbr"
	-@erase "$(INTDIR)\mo_geb.obj"
	-@erase "$(INTDIR)\mo_geb.sbr"
	-@erase "$(INTDIR)\MO_MELD.OBJ"
	-@erase "$(INTDIR)\MO_MELD.SBR"
	-@erase "$(INTDIR)\MO_MENU.OBJ"
	-@erase "$(INTDIR)\MO_MENU.SBR"
	-@erase "$(INTDIR)\mo_nr.obj"
	-@erase "$(INTDIR)\mo_nr.sbr"
	-@erase "$(INTDIR)\MO_NUMME.OBJ"
	-@erase "$(INTDIR)\MO_NUMME.SBR"
	-@erase "$(INTDIR)\mo_pasc.obj"
	-@erase "$(INTDIR)\mo_pasc.sbr"
	-@erase "$(INTDIR)\MO_PASSW.OBJ"
	-@erase "$(INTDIR)\MO_PASSW.SBR"
	-@erase "$(INTDIR)\MO_PREIS.OBJ"
	-@erase "$(INTDIR)\MO_PREIS.SBR"
	-@erase "$(INTDIR)\mo_progcfg.obj"
	-@erase "$(INTDIR)\mo_progcfg.sbr"
	-@erase "$(INTDIR)\mo_smtg.obj"
	-@erase "$(INTDIR)\mo_smtg.sbr"
	-@erase "$(INTDIR)\nublock.obj"
	-@erase "$(INTDIR)\nublock.sbr"
	-@erase "$(INTDIR)\Nve.obj"
	-@erase "$(INTDIR)\Nve.sbr"
	-@erase "$(INTDIR)\NveDb.obj"
	-@erase "$(INTDIR)\NveDb.sbr"
	-@erase "$(INTDIR)\NveGew.obj"
	-@erase "$(INTDIR)\NveGew.sbr"
	-@erase "$(INTDIR)\NveStringTable.obj"
	-@erase "$(INTDIR)\NveStringTable.sbr"
	-@erase "$(INTDIR)\Palette.obj"
	-@erase "$(INTDIR)\Palette.sbr"
	-@erase "$(INTDIR)\pers.obj"
	-@erase "$(INTDIR)\pers.sbr"
	-@erase "$(INTDIR)\pht_wg.obj"
	-@erase "$(INTDIR)\pht_wg.sbr"
	-@erase "$(INTDIR)\pr_a_gx.obj"
	-@erase "$(INTDIR)\pr_a_gx.sbr"
	-@erase "$(INTDIR)\prdk.obj"
	-@erase "$(INTDIR)\prdk.sbr"
	-@erase "$(INTDIR)\Process.obj"
	-@erase "$(INTDIR)\Process.sbr"
	-@erase "$(INTDIR)\prov_satz.obj"
	-@erase "$(INTDIR)\prov_satz.sbr"
	-@erase "$(INTDIR)\prp_user.obj"
	-@erase "$(INTDIR)\prp_user.sbr"
	-@erase "$(INTDIR)\PTAB.OBJ"
	-@erase "$(INTDIR)\PTAB.SBR"
	-@erase "$(INTDIR)\PtabFunc.obj"
	-@erase "$(INTDIR)\PtabFunc.sbr"
	-@erase "$(INTDIR)\PtabTable.obj"
	-@erase "$(INTDIR)\PtabTable.sbr"
	-@erase "$(INTDIR)\ReadNve.obj"
	-@erase "$(INTDIR)\ReadNve.sbr"
	-@erase "$(INTDIR)\scanean.obj"
	-@erase "$(INTDIR)\scanean.sbr"
	-@erase "$(INTDIR)\Sounds.obj"
	-@erase "$(INTDIR)\Sounds.sbr"
	-@erase "$(INTDIR)\Stc.obj"
	-@erase "$(INTDIR)\Stc.sbr"
	-@erase "$(INTDIR)\STDFKT.OBJ"
	-@erase "$(INTDIR)\STDFKT.SBR"
	-@erase "$(INTDIR)\STRFKT.OBJ"
	-@erase "$(INTDIR)\STRFKT.SBR"
	-@erase "$(INTDIR)\sys_par.obj"
	-@erase "$(INTDIR)\sys_par.sbr"
	-@erase "$(INTDIR)\SYS_PERI.OBJ"
	-@erase "$(INTDIR)\SYS_PERI.SBR"
	-@erase "$(INTDIR)\Text.obj"
	-@erase "$(INTDIR)\Text.sbr"
	-@erase "$(INTDIR)\textblock.obj"
	-@erase "$(INTDIR)\textblock.sbr"
	-@erase "$(INTDIR)\touchf.obj"
	-@erase "$(INTDIR)\touchf.sbr"
	-@erase "$(INTDIR)\TransHandler.obj"
	-@erase "$(INTDIR)\TransHandler.sbr"
	-@erase "$(INTDIR)\traponve.obj"
	-@erase "$(INTDIR)\traponve.sbr"
	-@erase "$(INTDIR)\trapopspm.obj"
	-@erase "$(INTDIR)\trapopspm.sbr"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(INTDIR)\Vector.obj"
	-@erase "$(INTDIR)\Vector.sbr"
	-@erase "$(INTDIR)\wa_charge.obj"
	-@erase "$(INTDIR)\wa_charge.sbr"
	-@erase "$(INTDIR)\WApplication.obj"
	-@erase "$(INTDIR)\WApplication.sbr"
	-@erase "$(INTDIR)\WDialog.obj"
	-@erase "$(INTDIR)\WDialog.sbr"
	-@erase "$(INTDIR)\wiepro.obj"
	-@erase "$(INTDIR)\wiepro.sbr"
	-@erase "$(INTDIR)\WMASK.OBJ"
	-@erase "$(INTDIR)\WMASK.SBR"
	-@erase "$(INTDIR)\writenve.obj"
	-@erase "$(INTDIR)\writenve.sbr"
	-@erase "$(INTDIR)\zerldaten.obj"
	-@erase "$(INTDIR)\zerldaten.sbr"
	-@erase "$(OUTDIR)\53600Ausz.bsc"
	-@erase "$(OUTDIR)\53600Ausz.exe"
	-@erase "$(OUTDIR)\53600Ausz.ilk"
	-@erase "$(OUTDIR)\53600Ausz.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /Zi /Od /I "$(INFORMIXDIR)\incl\esql723tc9"\
 /I "$(INFORMIXDIR)\incl\esql" /I "..\include" /I "..\53600" /D "_DEBUG" /D\
 "WIN32" /D "_WINDOWS" /D "FR_NUMMER" /D "OLDMASK" /FR"$(INTDIR)\\"\
 /Fp"$(INTDIR)\53600Ausz.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.\Debug/

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
RSC_PROJ=/l 0x407 /fo"$(INTDIR)\KOMIS.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\53600Ausz.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\A_BAS.SBR" \
	"$(INTDIR)\A_HNDW.SBR" \
	"$(INTDIR)\a_kun_gx.sbr" \
	"$(INTDIR)\A_PR.SBR" \
	"$(INTDIR)\akt_ipr.sbr" \
	"$(INTDIR)\akt_iprz.sbr" \
	"$(INTDIR)\AKT_KRZ.SBR" \
	"$(INTDIR)\AKTION.SBR" \
	"$(INTDIR)\atext.sbr" \
	"$(INTDIR)\auto_nr.sbr" \
	"$(INTDIR)\BkBitmap.sbr" \
	"$(INTDIR)\bsd_buch.sbr" \
	"$(INTDIR)\cmask.sbr" \
	"$(INTDIR)\DataCollection.sbr" \
	"$(INTDIR)\DATUM.SBR" \
	"$(INTDIR)\dbclass.sbr" \
	"$(INTDIR)\DBFUNC.SBR" \
	"$(INTDIR)\DbTime.sbr" \
	"$(INTDIR)\ddeclient.sbr" \
	"$(INTDIR)\Decimal.sbr" \
	"$(INTDIR)\DesAdv.sbr" \
	"$(INTDIR)\DllPreise.sbr" \
	"$(INTDIR)\FIL.SBR" \
	"$(INTDIR)\Integer.sbr" \
	"$(INTDIR)\ipr.sbr" \
	"$(INTDIR)\iprz.sbr" \
	"$(INTDIR)\Karton.sbr" \
	"$(INTDIR)\KOMIS.SBR" \
	"$(INTDIR)\kumebest.sbr" \
	"$(INTDIR)\KUN.SBR" \
	"$(INTDIR)\kun_leerg.sbr" \
	"$(INTDIR)\leer_bel.sbr" \
	"$(INTDIR)\LeerGut.sbr" \
	"$(INTDIR)\LeergutPart.sbr" \
	"$(INTDIR)\LicenceDialog.sbr" \
	"$(INTDIR)\lizenz.sbr" \
	"$(INTDIR)\Log.sbr" \
	"$(INTDIR)\LS.SBR" \
	"$(INTDIR)\ls_einzel.sbr" \
	"$(INTDIR)\ls_txt.sbr" \
	"$(INTDIR)\lsnve.sbr" \
	"$(INTDIR)\MDN.SBR" \
	"$(INTDIR)\MO_A_PR.SBR" \
	"$(INTDIR)\mo_abasis.sbr" \
	"$(INTDIR)\mo_auto.sbr" \
	"$(INTDIR)\MO_CURSO.SBR" \
	"$(INTDIR)\MO_DRAW.SBR" \
	"$(INTDIR)\MO_EINH.SBR" \
	"$(INTDIR)\mo_gdruck.sbr" \
	"$(INTDIR)\mo_geb.sbr" \
	"$(INTDIR)\MO_MELD.SBR" \
	"$(INTDIR)\MO_MENU.SBR" \
	"$(INTDIR)\mo_nr.sbr" \
	"$(INTDIR)\MO_NUMME.SBR" \
	"$(INTDIR)\mo_pasc.sbr" \
	"$(INTDIR)\MO_PASSW.SBR" \
	"$(INTDIR)\MO_PREIS.SBR" \
	"$(INTDIR)\mo_progcfg.sbr" \
	"$(INTDIR)\mo_smtg.sbr" \
	"$(INTDIR)\nublock.sbr" \
	"$(INTDIR)\Nve.sbr" \
	"$(INTDIR)\NveDb.sbr" \
	"$(INTDIR)\NveGew.sbr" \
	"$(INTDIR)\NveStringTable.sbr" \
	"$(INTDIR)\Palette.sbr" \
	"$(INTDIR)\pers.sbr" \
	"$(INTDIR)\pht_wg.sbr" \
	"$(INTDIR)\pr_a_gx.sbr" \
	"$(INTDIR)\prdk.sbr" \
	"$(INTDIR)\Process.sbr" \
	"$(INTDIR)\prov_satz.sbr" \
	"$(INTDIR)\prp_user.sbr" \
	"$(INTDIR)\PTAB.SBR" \
	"$(INTDIR)\PtabFunc.sbr" \
	"$(INTDIR)\PtabTable.sbr" \
	"$(INTDIR)\ReadNve.sbr" \
	"$(INTDIR)\scanean.sbr" \
	"$(INTDIR)\Sounds.sbr" \
	"$(INTDIR)\Stc.sbr" \
	"$(INTDIR)\STDFKT.SBR" \
	"$(INTDIR)\STRFKT.SBR" \
	"$(INTDIR)\sys_par.sbr" \
	"$(INTDIR)\SYS_PERI.SBR" \
	"$(INTDIR)\Text.sbr" \
	"$(INTDIR)\textblock.sbr" \
	"$(INTDIR)\touchf.sbr" \
	"$(INTDIR)\TransHandler.sbr" \
	"$(INTDIR)\traponve.sbr" \
	"$(INTDIR)\trapopspm.sbr" \
	"$(INTDIR)\Vector.sbr" \
	"$(INTDIR)\wa_charge.sbr" \
	"$(INTDIR)\WApplication.sbr" \
	"$(INTDIR)\WDialog.sbr" \
	"$(INTDIR)\wiepro.sbr" \
	"$(INTDIR)\WMASK.SBR" \
	"$(INTDIR)\writenve.sbr" \
	"$(INTDIR)\zerldaten.sbr"

"$(OUTDIR)\53600Ausz.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=kasseex.lib kernel32.lib user32.lib gdi32.lib winspool.lib\
 comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib\
 odbc32.lib odbccp32.lib comctl32.lib isqlt07c.lib winmm.lib /nologo\
 /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\53600Ausz.pdb" /debug\
 /machine:I386 /out:"$(OUTDIR)\53600Ausz.exe" /pdbtype:sept\
 /libpath:"$(INFORMIXDIR)\lib" /libpath:"..\lib" 
LINK32_OBJS= \
	"$(INTDIR)\A_BAS.OBJ" \
	"$(INTDIR)\A_HNDW.OBJ" \
	"$(INTDIR)\a_kun_gx.obj" \
	"$(INTDIR)\A_PR.OBJ" \
	"$(INTDIR)\akt_ipr.obj" \
	"$(INTDIR)\akt_iprz.obj" \
	"$(INTDIR)\AKT_KRZ.OBJ" \
	"$(INTDIR)\AKTION.OBJ" \
	"$(INTDIR)\atext.obj" \
	"$(INTDIR)\auto_nr.obj" \
	"$(INTDIR)\BkBitmap.obj" \
	"$(INTDIR)\bsd_buch.obj" \
	"$(INTDIR)\cmask.obj" \
	"$(INTDIR)\DataCollection.obj" \
	"$(INTDIR)\DATUM.OBJ" \
	"$(INTDIR)\dbclass.obj" \
	"$(INTDIR)\DBFUNC.OBJ" \
	"$(INTDIR)\DbTime.obj" \
	"$(INTDIR)\ddeclient.obj" \
	"$(INTDIR)\Decimal.obj" \
	"$(INTDIR)\DesAdv.obj" \
	"$(INTDIR)\DllPreise.obj" \
	"$(INTDIR)\FIL.OBJ" \
	"$(INTDIR)\Integer.obj" \
	"$(INTDIR)\ipr.obj" \
	"$(INTDIR)\iprz.obj" \
	"$(INTDIR)\Karton.obj" \
	"$(INTDIR)\KOMIS.OBJ" \
	"$(INTDIR)\KOMIS.res" \
	"$(INTDIR)\kumebest.obj" \
	"$(INTDIR)\KUN.OBJ" \
	"$(INTDIR)\kun_leerg.obj" \
	"$(INTDIR)\leer_bel.obj" \
	"$(INTDIR)\LeerGut.obj" \
	"$(INTDIR)\LeergutPart.obj" \
	"$(INTDIR)\LicenceDialog.obj" \
	"$(INTDIR)\lizenz.obj" \
	"$(INTDIR)\Log.obj" \
	"$(INTDIR)\LS.OBJ" \
	"$(INTDIR)\ls_einzel.obj" \
	"$(INTDIR)\ls_txt.obj" \
	"$(INTDIR)\lsnve.obj" \
	"$(INTDIR)\MDN.OBJ" \
	"$(INTDIR)\MO_A_PR.OBJ" \
	"$(INTDIR)\mo_abasis.obj" \
	"$(INTDIR)\mo_auto.obj" \
	"$(INTDIR)\MO_CURSO.OBJ" \
	"$(INTDIR)\MO_DRAW.OBJ" \
	"$(INTDIR)\MO_EINH.OBJ" \
	"$(INTDIR)\mo_gdruck.obj" \
	"$(INTDIR)\mo_geb.obj" \
	"$(INTDIR)\MO_MELD.OBJ" \
	"$(INTDIR)\MO_MENU.OBJ" \
	"$(INTDIR)\mo_nr.obj" \
	"$(INTDIR)\MO_NUMME.OBJ" \
	"$(INTDIR)\mo_pasc.obj" \
	"$(INTDIR)\MO_PASSW.OBJ" \
	"$(INTDIR)\MO_PREIS.OBJ" \
	"$(INTDIR)\mo_progcfg.obj" \
	"$(INTDIR)\mo_smtg.obj" \
	"$(INTDIR)\nublock.obj" \
	"$(INTDIR)\Nve.obj" \
	"$(INTDIR)\NveDb.obj" \
	"$(INTDIR)\NveGew.obj" \
	"$(INTDIR)\NveStringTable.obj" \
	"$(INTDIR)\Palette.obj" \
	"$(INTDIR)\pers.obj" \
	"$(INTDIR)\pht_wg.obj" \
	"$(INTDIR)\pr_a_gx.obj" \
	"$(INTDIR)\prdk.obj" \
	"$(INTDIR)\Process.obj" \
	"$(INTDIR)\prov_satz.obj" \
	"$(INTDIR)\prp_user.obj" \
	"$(INTDIR)\PTAB.OBJ" \
	"$(INTDIR)\PtabFunc.obj" \
	"$(INTDIR)\PtabTable.obj" \
	"$(INTDIR)\ReadNve.obj" \
	"$(INTDIR)\scanean.obj" \
	"$(INTDIR)\Sounds.obj" \
	"$(INTDIR)\Stc.obj" \
	"$(INTDIR)\STDFKT.OBJ" \
	"$(INTDIR)\STRFKT.OBJ" \
	"$(INTDIR)\sys_par.obj" \
	"$(INTDIR)\SYS_PERI.OBJ" \
	"$(INTDIR)\Text.obj" \
	"$(INTDIR)\textblock.obj" \
	"$(INTDIR)\touchf.obj" \
	"$(INTDIR)\TransHandler.obj" \
	"$(INTDIR)\traponve.obj" \
	"$(INTDIR)\trapopspm.obj" \
	"$(INTDIR)\Vector.obj" \
	"$(INTDIR)\wa_charge.obj" \
	"$(INTDIR)\WApplication.obj" \
	"$(INTDIR)\WDialog.obj" \
	"$(INTDIR)\wiepro.obj" \
	"$(INTDIR)\WMASK.OBJ" \
	"$(INTDIR)\writenve.obj" \
	"$(INTDIR)\zerldaten.obj" \
	"..\lib\etubidll.lib" \
	"..\lib\kasseex.lib" \
	".\FNB.LIB"

"$(OUTDIR)\53600Ausz.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(CFG)" == "53600 - Win32 Release" || "$(CFG)" == "53600 - Win32 Debug"
SOURCE=.\A_BAS.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_A_BAS=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\A_BAS.OBJ" : $(SOURCE) $(DEP_CPP_A_BAS) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_A_BAS=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\A_BAS.OBJ"	"$(INTDIR)\A_BAS.SBR" : $(SOURCE) $(DEP_CPP_A_BAS)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\A_HNDW.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_A_HND=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_hndw.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\A_HNDW.OBJ" : $(SOURCE) $(DEP_CPP_A_HND) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_A_HND=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_hndw.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\A_HNDW.OBJ"	"$(INTDIR)\A_HNDW.SBR" : $(SOURCE) $(DEP_CPP_A_HND)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\a_kun_gx.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_A_KUN=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_kun_gx.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\a_kun_gx.obj" : $(SOURCE) $(DEP_CPP_A_KUN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_A_KUN=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_kun_gx.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\a_kun_gx.obj"	"$(INTDIR)\a_kun_gx.sbr" : $(SOURCE) $(DEP_CPP_A_KUN)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\A_PR.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_A_PR_=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_pr.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\A_PR.OBJ" : $(SOURCE) $(DEP_CPP_A_PR_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_A_PR_=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_pr.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\A_PR.OBJ"	"$(INTDIR)\A_PR.SBR" : $(SOURCE) $(DEP_CPP_A_PR_)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\akt_ipr.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_AKT_I=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_bas.h"\
	".\akt_ipr.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\akt_ipr.obj" : $(SOURCE) $(DEP_CPP_AKT_I) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_AKT_I=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_bas.h"\
	".\akt_ipr.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\akt_ipr.obj"	"$(INTDIR)\akt_ipr.sbr" : $(SOURCE) $(DEP_CPP_AKT_I)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\akt_iprz.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_AKT_IP=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_bas.h"\
	".\akt_iprz.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\akt_iprz.obj" : $(SOURCE) $(DEP_CPP_AKT_IP) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_AKT_IP=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_bas.h"\
	".\akt_iprz.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\akt_iprz.obj"	"$(INTDIR)\akt_iprz.sbr" : $(SOURCE) $(DEP_CPP_AKT_IP)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\AKT_KRZ.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_AKT_K=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\AKT_KRZ.OBJ" : $(SOURCE) $(DEP_CPP_AKT_K) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_AKT_K=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\AKT_KRZ.OBJ"	"$(INTDIR)\AKT_KRZ.SBR" : $(SOURCE) $(DEP_CPP_AKT_K)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\AKTION.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_AKTIO=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\aktion.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\AKTION.OBJ" : $(SOURCE) $(DEP_CPP_AKTIO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_AKTIO=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\aktion.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\AKTION.OBJ"	"$(INTDIR)\AKTION.SBR" : $(SOURCE) $(DEP_CPP_AKTIO)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\atext.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_ATEXT=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\atext.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\atext.obj" : $(SOURCE) $(DEP_CPP_ATEXT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_ATEXT=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\atext.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\atext.obj"	"$(INTDIR)\atext.sbr" : $(SOURCE) $(DEP_CPP_ATEXT)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\auto_nr.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_AUTO_=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_bas.h"\
	".\a_kun.h"\
	".\auto_nr.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\auto_nr.obj" : $(SOURCE) $(DEP_CPP_AUTO_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_AUTO_=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_bas.h"\
	".\a_kun.h"\
	".\auto_nr.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\auto_nr.obj"	"$(INTDIR)\auto_nr.sbr" : $(SOURCE) $(DEP_CPP_AUTO_)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\BkBitmap.cpp
DEP_CPP_BKBIT=\
	".\BkBitmap.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\BkBitmap.obj" : $(SOURCE) $(DEP_CPP_BKBIT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\BkBitmap.obj"	"$(INTDIR)\BkBitmap.sbr" : $(SOURCE) $(DEP_CPP_BKBIT)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\bsd_buch.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_BSD_B=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\bsd_buch.h"\
	".\mo_curso.h"\
	

"$(INTDIR)\bsd_buch.obj" : $(SOURCE) $(DEP_CPP_BSD_B) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_BSD_B=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\bsd_buch.h"\
	".\mo_curso.h"\
	

"$(INTDIR)\bsd_buch.obj"	"$(INTDIR)\bsd_buch.sbr" : $(SOURCE) $(DEP_CPP_BSD_B)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\cmask.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_CMASK=\
	".\BkBitmap.h"\
	".\cmask.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\cmask.obj" : $(SOURCE) $(DEP_CPP_CMASK) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_CMASK=\
	".\BkBitmap.h"\
	".\cmask.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\cmask.obj"	"$(INTDIR)\cmask.sbr" : $(SOURCE) $(DEP_CPP_CMASK)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\DataCollection.cpp
DEP_CPP_DATAC=\
	".\DataCollection.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\DataCollection.obj" : $(SOURCE) $(DEP_CPP_DATAC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\DataCollection.obj"	"$(INTDIR)\DataCollection.sbr" : $(SOURCE)\
 $(DEP_CPP_DATAC) "$(INTDIR)"


!ENDIF 

SOURCE=.\DATUM.C

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\DATUM.OBJ" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\DATUM.OBJ"	"$(INTDIR)\DATUM.SBR" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\dbclass.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_DBCLA=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\dbclass.obj" : $(SOURCE) $(DEP_CPP_DBCLA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_DBCLA=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\dbclass.obj"	"$(INTDIR)\dbclass.sbr" : $(SOURCE) $(DEP_CPP_DBCLA)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\DBFUNC.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_DBFUN=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\DBFUNC.OBJ" : $(SOURCE) $(DEP_CPP_DBFUN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_DBFUN=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\DBFUNC.OBJ"	"$(INTDIR)\DBFUNC.SBR" : $(SOURCE) $(DEP_CPP_DBFUN)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\DbTime.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_DBTIM=\
	"..\include\Text.h"\
	".\dbclass.h"\
	".\DbTime.h"\
	

"$(INTDIR)\DbTime.obj" : $(SOURCE) $(DEP_CPP_DBTIM) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_DBTIM=\
	"..\include\Text.h"\
	".\dbclass.h"\
	".\DbTime.h"\
	

"$(INTDIR)\DbTime.obj"	"$(INTDIR)\DbTime.sbr" : $(SOURCE) $(DEP_CPP_DBTIM)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\ddeclient.cpp
DEP_CPP_DDECL=\
	".\ddeclient.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\ddeclient.obj" : $(SOURCE) $(DEP_CPP_DDECL) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\ddeclient.obj"	"$(INTDIR)\ddeclient.sbr" : $(SOURCE)\
 $(DEP_CPP_DDECL) "$(INTDIR)"


!ENDIF 

SOURCE=.\Decimal.cpp
DEP_CPP_DECIM=\
	".\Decimal.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\Decimal.obj" : $(SOURCE) $(DEP_CPP_DECIM) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\Decimal.obj"	"$(INTDIR)\Decimal.sbr" : $(SOURCE) $(DEP_CPP_DECIM)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\DesAdv.cpp
DEP_CPP_DESAD=\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	".\dbclass.h"\
	".\DesAdv.h"\
	".\lsnve.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\DesAdv.obj" : $(SOURCE) $(DEP_CPP_DESAD) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\DesAdv.obj"	"$(INTDIR)\DesAdv.sbr" : $(SOURCE) $(DEP_CPP_DESAD)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\DllPreise.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_DLLPR=\
	"..\include\Text.h"\
	".\dllpreise.h"\
	

"$(INTDIR)\DllPreise.obj" : $(SOURCE) $(DEP_CPP_DLLPR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_DLLPR=\
	"..\include\Text.h"\
	".\dllpreise.h"\
	

"$(INTDIR)\DllPreise.obj"	"$(INTDIR)\DllPreise.sbr" : $(SOURCE)\
 $(DEP_CPP_DLLPR) "$(INTDIR)"


!ENDIF 

SOURCE=.\FIL.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_FIL_C=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\fil.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\FIL.OBJ" : $(SOURCE) $(DEP_CPP_FIL_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_FIL_C=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\fil.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\FIL.OBJ"	"$(INTDIR)\FIL.SBR" : $(SOURCE) $(DEP_CPP_FIL_C)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Integer.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_INTEG=\
	"..\include\Text.h"\
	".\integer.h"\
	

"$(INTDIR)\Integer.obj" : $(SOURCE) $(DEP_CPP_INTEG) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_INTEG=\
	"..\include\Text.h"\
	".\integer.h"\
	

"$(INTDIR)\Integer.obj"	"$(INTDIR)\Integer.sbr" : $(SOURCE) $(DEP_CPP_INTEG)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\ipr.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_IPR_C=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\ipr.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\ipr.obj" : $(SOURCE) $(DEP_CPP_IPR_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_IPR_C=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\ipr.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\ipr.obj"	"$(INTDIR)\ipr.sbr" : $(SOURCE) $(DEP_CPP_IPR_C)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\iprz.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_IPRZ_=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\iprz.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\iprz.obj" : $(SOURCE) $(DEP_CPP_IPRZ_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_IPRZ_=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\iprz.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\iprz.obj"	"$(INTDIR)\iprz.sbr" : $(SOURCE) $(DEP_CPP_IPRZ_)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Karton.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_KARTO=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	"..\include\Text.h"\
	".\Karton.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	

"$(INTDIR)\Karton.obj" : $(SOURCE) $(DEP_CPP_KARTO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_KARTO=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	"..\include\Text.h"\
	".\Karton.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	

"$(INTDIR)\Karton.obj"	"$(INTDIR)\Karton.sbr" : $(SOURCE) $(DEP_CPP_KARTO)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\KOMIS.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_KOMIS=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	"..\include\Text.h"\
	"..\include\vector.h"\
	"..\include\wmaskc.h"\
	".\a_bas.h"\
	".\a_hndw.h"\
	".\a_kun_gx.h"\
	".\a_pr.h"\
	".\akt_ipr.h"\
	".\akt_iprz.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\atext.h"\
	".\BkBitmap.h"\
	".\bsd_buch.h"\
	".\cmask.h"\
	".\DataCollection.h"\
	".\datum.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\ddeclient.h"\
	".\DesAdv.h"\
	".\dllpreise.h"\
	".\etubi.h"\
	".\fil.h"\
	".\fit.h"\
	".\fnb.h"\
	".\integer.h"\
	".\ipr.h"\
	".\iprz.h"\
	".\Karton.h"\
	".\kun.h"\
	".\kun_leerg.h"\
	".\leer_bel.h"\
	".\LeerGut.h"\
	".\LeergutPart.h"\
	".\LicenceDialog.h"\
	".\lizenz.h"\
	".\Log.h"\
	".\ls.h"\
	".\ls_einzel.h"\
	".\ls_txt.h"\
	".\lsnve.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_auto.h"\
	".\mo_curs0.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_einh.h"\
	".\mo_expl.h"\
	".\mo_gdruck.h"\
	".\mo_geb.h"\
	".\mo_meld.h"\
	".\mo_menu.h"\
	".\mo_nr.h"\
	".\mo_numme.h"\
	".\mo_pasc.h"\
	".\mo_passw.h"\
	".\mo_preis.h"\
	".\mo_progcfg.h"\
	".\mo_smtg.h"\
	".\nublock.h"\
	".\Nve.h"\
	".\NveDb.h"\
	".\NveGew.h"\
	".\NveStringTable.h"\
	".\palette.h"\
	".\pers.h"\
	".\pht_wg.h"\
	".\pr_a_gx.h"\
	".\prdk.h"\
	".\Process.h"\
	".\prov_satz.h"\
	".\prp_user.h"\
	".\ptab.h"\
	".\PtabFunc.h"\
	".\PtabTable.h"\
	".\readnve.h"\
	".\scanean.h"\
	".\Sounds.h"\
	".\Stc.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\sys_par.h"\
	".\sys_peri.h"\
	".\textblock.h"\
	".\textstyle.h"\
	".\touchf.h"\
	".\TransHandler.h"\
	".\traponve.h"\
	".\trapopspm.h"\
	".\wa_charge.h"\
	".\WApplication.h"\
	".\WDialog.h"\
	".\wiepro.h"\
	".\wmask.h"\
	".\writenve.h"\
	".\zerldaten.h"\
	

"$(INTDIR)\KOMIS.OBJ" : $(SOURCE) $(DEP_CPP_KOMIS) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_KOMIS=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	"..\include\Text.h"\
	"..\include\vector.h"\
	"..\include\wmaskc.h"\
	".\a_bas.h"\
	".\a_hndw.h"\
	".\a_kun_gx.h"\
	".\a_pr.h"\
	".\akt_ipr.h"\
	".\akt_iprz.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\atext.h"\
	".\BkBitmap.h"\
	".\bsd_buch.h"\
	".\cmask.h"\
	".\DataCollection.h"\
	".\datum.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\ddeclient.h"\
	".\DesAdv.h"\
	".\dllpreise.h"\
	".\etubi.h"\
	".\fil.h"\
	".\fit.h"\
	".\fnb.h"\
	".\integer.h"\
	".\ipr.h"\
	".\iprz.h"\
	".\Karton.h"\
	".\kun.h"\
	".\kun_leerg.h"\
	".\leer_bel.h"\
	".\LeerGut.h"\
	".\LeergutPart.h"\
	".\LicenceDialog.h"\
	".\lizenz.h"\
	".\Log.h"\
	".\ls.h"\
	".\ls_einzel.h"\
	".\ls_txt.h"\
	".\lsnve.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_auto.h"\
	".\mo_curs0.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_einh.h"\
	".\mo_expl.h"\
	".\mo_gdruck.h"\
	".\mo_geb.h"\
	".\mo_meld.h"\
	".\mo_menu.h"\
	".\mo_nr.h"\
	".\mo_numme.h"\
	".\mo_pasc.h"\
	".\mo_passw.h"\
	".\mo_preis.h"\
	".\mo_progcfg.h"\
	".\mo_smtg.h"\
	".\nublock.h"\
	".\Nve.h"\
	".\NveDb.h"\
	".\NveGew.h"\
	".\NveStringTable.h"\
	".\palette.h"\
	".\pers.h"\
	".\pht_wg.h"\
	".\pr_a_gx.h"\
	".\prdk.h"\
	".\Process.h"\
	".\prov_satz.h"\
	".\prp_user.h"\
	".\ptab.h"\
	".\PtabFunc.h"\
	".\PtabTable.h"\
	".\readnve.h"\
	".\scanean.h"\
	".\Sounds.h"\
	".\Stc.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\sys_par.h"\
	".\sys_peri.h"\
	".\textblock.h"\
	".\textstyle.h"\
	".\touchf.h"\
	".\TransHandler.h"\
	".\traponve.h"\
	".\trapopspm.h"\
	".\wa_charge.h"\
	".\WApplication.h"\
	".\WDialog.h"\
	".\wiepro.h"\
	".\wmask.h"\
	".\writenve.h"\
	".\zerldaten.h"\
	

"$(INTDIR)\KOMIS.OBJ"	"$(INTDIR)\KOMIS.SBR" : $(SOURCE) $(DEP_CPP_KOMIS)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\KOMIS.RC
DEP_RSC_KOMIS_=\
	".\autotaraand1.bmp"\
	".\autotaraausd1.bmp"\
	".\autotaramaskand1.bmp"\
	".\autotaramaskausd1.bmp"\
	".\autotaramaskd.bmp"\
	".\FESTTARA.BMP"\
	".\fhs.bmp"\
	".\fhs.ico"\
	".\fit.h"\
	".\fit.ico"\
	".\fit0.ico"\
	".\fit01.ico"\
	".\fit02.ico"\
	".\logo_s1.bmp"\
	".\NOTE.ICO"\
	".\pfeill.bmp"\
	".\pfeilo.bmp"\
	".\pfeilog.bmp"\
	".\pfeilok.bmp"\
	".\pfeilr.bmp"\
	".\pfeilu.bmp"\
	".\pfeilug.bmp"\
	".\pfeiluk.bmp"\
	".\temptaraand1.bmp"\
	".\TemptaraMaskand1.bmp"\
	

"$(INTDIR)\KOMIS.res" : $(SOURCE) $(DEP_RSC_KOMIS_) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\kumebest.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_KUMEB=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\kumebest.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\kumebest.obj" : $(SOURCE) $(DEP_CPP_KUMEB) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_KUMEB=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\kumebest.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\kumebest.obj"	"$(INTDIR)\kumebest.sbr" : $(SOURCE) $(DEP_CPP_KUMEB)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\KUN.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_KUN_C=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_kun.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\kun.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\KUN.OBJ" : $(SOURCE) $(DEP_CPP_KUN_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_KUN_C=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_kun.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\kun.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\KUN.OBJ"	"$(INTDIR)\KUN.SBR" : $(SOURCE) $(DEP_CPP_KUN_C)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\kun_leerg.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_KUN_L=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\kun_leerg.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\kun_leerg.obj" : $(SOURCE) $(DEP_CPP_KUN_L) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_KUN_L=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\kun_leerg.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\kun_leerg.obj"	"$(INTDIR)\kun_leerg.sbr" : $(SOURCE)\
 $(DEP_CPP_KUN_L) "$(INTDIR)"


!ENDIF 

SOURCE=.\leer_bel.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_LEER_=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\leer_bel.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\leer_bel.obj" : $(SOURCE) $(DEP_CPP_LEER_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_LEER_=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\leer_bel.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\leer_bel.obj"	"$(INTDIR)\leer_bel.sbr" : $(SOURCE) $(DEP_CPP_LEER_)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\LeerGut.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_LEERG=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\kun_leerg.h"\
	".\leer_bel.h"\
	".\LeerGut.h"\
	".\ls.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_menu.h"\
	".\strfkt.h"\
	".\sys_par.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\LeerGut.obj" : $(SOURCE) $(DEP_CPP_LEERG) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_LEERG=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\kun_leerg.h"\
	".\leer_bel.h"\
	".\LeerGut.h"\
	".\ls.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_menu.h"\
	".\strfkt.h"\
	".\sys_par.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\LeerGut.obj"	"$(INTDIR)\LeerGut.sbr" : $(SOURCE) $(DEP_CPP_LEERG)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\LeergutPart.cpp
DEP_CPP_LEERGU=\
	"..\include\Text.h"\
	".\LeergutPart.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\LeergutPart.obj" : $(SOURCE) $(DEP_CPP_LEERGU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\LeergutPart.obj"	"$(INTDIR)\LeergutPart.sbr" : $(SOURCE)\
 $(DEP_CPP_LEERGU) "$(INTDIR)"


!ENDIF 

SOURCE=.\LicenceDialog.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_LICEN=\
	".\LicenceDialog.h"\
	".\strfkt.h"\
	".\WDialog.h"\
	

"$(INTDIR)\LicenceDialog.obj" : $(SOURCE) $(DEP_CPP_LICEN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_LICEN=\
	".\LicenceDialog.h"\
	".\strfkt.h"\
	".\WDialog.h"\
	

"$(INTDIR)\LicenceDialog.obj"	"$(INTDIR)\LicenceDialog.sbr" : $(SOURCE)\
 $(DEP_CPP_LICEN) "$(INTDIR)"


!ENDIF 

SOURCE=.\lizenz.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_LIZEN=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\cmask.h"\
	".\datum.h"\
	".\dbclass.h"\
	".\lizenz.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\lizenz.obj" : $(SOURCE) $(DEP_CPP_LIZEN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_LIZEN=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\cmask.h"\
	".\datum.h"\
	".\dbclass.h"\
	".\lizenz.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\lizenz.obj"	"$(INTDIR)\lizenz.sbr" : $(SOURCE) $(DEP_CPP_LIZEN)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Log.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_LOG_C=\
	"..\include\Text.h"\
	".\Log.h"\
	

"$(INTDIR)\Log.obj" : $(SOURCE) $(DEP_CPP_LOG_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_LOG_C=\
	"..\include\Text.h"\
	".\Log.h"\
	

"$(INTDIR)\Log.obj"	"$(INTDIR)\Log.sbr" : $(SOURCE) $(DEP_CPP_LOG_C)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\LS.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_LS_CP=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\ls.h"\
	".\ls_einzel.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\LS.OBJ" : $(SOURCE) $(DEP_CPP_LS_CP) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_LS_CP=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\ls.h"\
	".\ls_einzel.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\LS.OBJ"	"$(INTDIR)\LS.SBR" : $(SOURCE) $(DEP_CPP_LS_CP) "$(INTDIR)"


!ENDIF 

SOURCE=.\ls_einzel.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_LS_EI=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\kun.h"\
	".\ls.h"\
	".\ls_einzel.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\ptab.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\ls_einzel.obj" : $(SOURCE) $(DEP_CPP_LS_EI) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_LS_EI=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\kun.h"\
	".\ls.h"\
	".\ls_einzel.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\ptab.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\ls_einzel.obj"	"$(INTDIR)\ls_einzel.sbr" : $(SOURCE)\
 $(DEP_CPP_LS_EI) "$(INTDIR)"


!ENDIF 

SOURCE=.\ls_txt.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_LS_TX=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_bas.h"\
	".\a_kun.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\kun.h"\
	".\ls_txt.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\ls_txt.obj" : $(SOURCE) $(DEP_CPP_LS_TX) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_LS_TX=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_bas.h"\
	".\a_kun.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\kun.h"\
	".\ls_txt.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\ls_txt.obj"	"$(INTDIR)\ls_txt.sbr" : $(SOURCE) $(DEP_CPP_LS_TX)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\lsnve.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_LSNVE=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	".\dbclass.h"\
	".\lsnve.h"\
	".\mo_curso.h"\
	

"$(INTDIR)\lsnve.obj" : $(SOURCE) $(DEP_CPP_LSNVE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_LSNVE=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	".\dbclass.h"\
	".\lsnve.h"\
	".\mo_curso.h"\
	

"$(INTDIR)\lsnve.obj"	"$(INTDIR)\lsnve.sbr" : $(SOURCE) $(DEP_CPP_LSNVE)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\MDN.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MDN_C=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MDN.OBJ" : $(SOURCE) $(DEP_CPP_MDN_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MDN_C=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MDN.OBJ"	"$(INTDIR)\MDN.SBR" : $(SOURCE) $(DEP_CPP_MDN_C)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_A_PR.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_A_=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\fil.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	

"$(INTDIR)\MO_A_PR.OBJ" : $(SOURCE) $(DEP_CPP_MO_A_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_A_=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_pr.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\fil.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	

"$(INTDIR)\MO_A_PR.OBJ"	"$(INTDIR)\MO_A_PR.SBR" : $(SOURCE) $(DEP_CPP_MO_A_)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\mo_abasis.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_AB=\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_abasis.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\mo_abasis.obj" : $(SOURCE) $(DEP_CPP_MO_AB) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_AB=\
	".\a_bas.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_abasis.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\mo_abasis.obj"	"$(INTDIR)\mo_abasis.sbr" : $(SOURCE)\
 $(DEP_CPP_MO_AB) "$(INTDIR)"


!ENDIF 

SOURCE=.\mo_auto.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_AU=\
	".\auto_nr.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_auto.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\mo_auto.obj" : $(SOURCE) $(DEP_CPP_MO_AU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_AU=\
	".\auto_nr.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_auto.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\mo_auto.obj"	"$(INTDIR)\mo_auto.sbr" : $(SOURCE) $(DEP_CPP_MO_AU)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_CURSO.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_CU=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	".\tcursor.h"\
	

"$(INTDIR)\MO_CURSO.OBJ" : $(SOURCE) $(DEP_CPP_MO_CU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_CU=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	".\tcursor.h"\
	

"$(INTDIR)\MO_CURSO.OBJ"	"$(INTDIR)\MO_CURSO.SBR" : $(SOURCE) $(DEP_CPP_MO_CU)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_DRAW.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_DR=\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_DRAW.OBJ" : $(SOURCE) $(DEP_CPP_MO_DR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_DR=\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_DRAW.OBJ"	"$(INTDIR)\MO_DRAW.SBR" : $(SOURCE) $(DEP_CPP_MO_DR)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_EINH.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_EI=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_bas.h"\
	".\a_hndw.h"\
	".\a_kun.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\kumebest.h"\
	".\kun.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_einh.h"\
	".\mo_meld.h"\
	".\ptab.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_EINH.OBJ" : $(SOURCE) $(DEP_CPP_MO_EI) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_EI=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_bas.h"\
	".\a_hndw.h"\
	".\a_kun.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\kumebest.h"\
	".\kun.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_einh.h"\
	".\mo_meld.h"\
	".\ptab.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_EINH.OBJ"	"$(INTDIR)\MO_EINH.SBR" : $(SOURCE) $(DEP_CPP_MO_EI)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\mo_gdruck.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_GD=\
	"..\include\Text.h"\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\mo_gdruck.h"\
	".\mo_meld.h"\
	".\Process.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\mo_gdruck.obj" : $(SOURCE) $(DEP_CPP_MO_GD) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_GD=\
	"..\include\Text.h"\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\mo_gdruck.h"\
	".\mo_meld.h"\
	".\Process.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\mo_gdruck.obj"	"$(INTDIR)\mo_gdruck.sbr" : $(SOURCE)\
 $(DEP_CPP_MO_GD) "$(INTDIR)"


!ENDIF 

SOURCE=.\mo_geb.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_GE=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\mo_curso.h"\
	".\mo_geb.h"\
	

"$(INTDIR)\mo_geb.obj" : $(SOURCE) $(DEP_CPP_MO_GE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_GE=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\mo_geb.h"\
	

"$(INTDIR)\mo_geb.obj"	"$(INTDIR)\mo_geb.sbr" : $(SOURCE) $(DEP_CPP_MO_GE)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_MELD.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_ME=\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\mo_menu.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_MELD.OBJ" : $(SOURCE) $(DEP_CPP_MO_ME) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_ME=\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\mo_menu.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_MELD.OBJ"	"$(INTDIR)\MO_MELD.SBR" : $(SOURCE) $(DEP_CPP_MO_ME)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_MENU.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_MEN=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_menu.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_MENU.OBJ" : $(SOURCE) $(DEP_CPP_MO_MEN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_MEN=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_menu.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_MENU.OBJ"	"$(INTDIR)\MO_MENU.SBR" : $(SOURCE) $(DEP_CPP_MO_MEN)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\mo_nr.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_NR=\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	".\dbclass.h"\
	".\mo_nr.h"\
	

"$(INTDIR)\mo_nr.obj" : $(SOURCE) $(DEP_CPP_MO_NR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_NR=\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	".\dbclass.h"\
	".\mo_nr.h"\
	

"$(INTDIR)\mo_nr.obj"	"$(INTDIR)\mo_nr.sbr" : $(SOURCE) $(DEP_CPP_MO_NR)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_NUMME.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_NU=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_numme.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_NUMME.OBJ" : $(SOURCE) $(DEP_CPP_MO_NU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_NU=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\mo_numme.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_NUMME.OBJ"	"$(INTDIR)\MO_NUMME.SBR" : $(SOURCE) $(DEP_CPP_MO_NU)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\mo_pasc.cpp
DEP_CPP_MO_PA=\
	".\mo_pasc.h"\
	".\strfkt.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\mo_pasc.obj" : $(SOURCE) $(DEP_CPP_MO_PA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\mo_pasc.obj"	"$(INTDIR)\mo_pasc.sbr" : $(SOURCE) $(DEP_CPP_MO_PA)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_PASSW.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_PAS=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	"..\include\vector.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_menu.h"\
	".\pers.h"\
	".\strfkt.h"\
	".\textblock.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_PASSW.OBJ" : $(SOURCE) $(DEP_CPP_MO_PAS) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_PAS=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	"..\include\vector.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_menu.h"\
	".\pers.h"\
	".\strfkt.h"\
	".\textblock.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_PASSW.OBJ"	"$(INTDIR)\MO_PASSW.SBR" : $(SOURCE) $(DEP_CPP_MO_PAS)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\MO_PREIS.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_PR=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_pr.h"\
	".\akt_ipr.h"\
	".\akt_iprz.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\fil.h"\
	".\ipr.h"\
	".\iprz.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_abasis.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_preis.h"\
	".\strfkt.h"\
	".\sys_par.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_PREIS.OBJ" : $(SOURCE) $(DEP_CPP_MO_PR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_PR=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_pr.h"\
	".\akt_ipr.h"\
	".\akt_iprz.h"\
	".\akt_krz.h"\
	".\aktion.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\fil.h"\
	".\ipr.h"\
	".\iprz.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_a_pr.h"\
	".\mo_abasis.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_preis.h"\
	".\strfkt.h"\
	".\sys_par.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\MO_PREIS.OBJ"	"$(INTDIR)\MO_PREIS.SBR" : $(SOURCE) $(DEP_CPP_MO_PR)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\mo_progcfg.cpp
DEP_CPP_MO_PRO=\
	".\mo_progcfg.h"\
	".\strfkt.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\mo_progcfg.obj" : $(SOURCE) $(DEP_CPP_MO_PRO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\mo_progcfg.obj"	"$(INTDIR)\mo_progcfg.sbr" : $(SOURCE)\
 $(DEP_CPP_MO_PRO) "$(INTDIR)"


!ENDIF 

SOURCE=.\mo_smtg.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_MO_SM=\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	".\dbclass.h"\
	".\mo_smtg.h"\
	".\tsmtg.h"\
	

"$(INTDIR)\mo_smtg.obj" : $(SOURCE) $(DEP_CPP_MO_SM) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_MO_SM=\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	".\dbclass.h"\
	".\mo_smtg.h"\
	".\tsmtg.h"\
	

"$(INTDIR)\mo_smtg.obj"	"$(INTDIR)\mo_smtg.sbr" : $(SOURCE) $(DEP_CPP_MO_SM)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\nublock.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_NUBLO=\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\nublock.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\nublock.obj" : $(SOURCE) $(DEP_CPP_NUBLO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_NUBLO=\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\nublock.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\nublock.obj"	"$(INTDIR)\nublock.sbr" : $(SOURCE) $(DEP_CPP_NUBLO)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Nve.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_NVE_C=\
	"..\include\Text.h"\
	".\BkBitmap.h"\
	".\DataCollection.h"\
	".\dbclass.h"\
	".\DbTime.h"\
	".\LeergutPart.h"\
	".\mo_draw.h"\
	".\Nve.h"\
	".\NveDb.h"\
	".\ptab.h"\
	".\PtabTable.h"\
	".\TransHandler.h"\
	".\traponve.h"\
	".\trapopspm.h"\
	".\wmask.h"\
	

"$(INTDIR)\Nve.obj" : $(SOURCE) $(DEP_CPP_NVE_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_NVE_C=\
	"..\include\Text.h"\
	".\BkBitmap.h"\
	".\DataCollection.h"\
	".\dbclass.h"\
	".\DbTime.h"\
	".\LeergutPart.h"\
	".\mo_draw.h"\
	".\Nve.h"\
	".\NveDb.h"\
	".\ptab.h"\
	".\PtabTable.h"\
	".\TransHandler.h"\
	".\traponve.h"\
	".\trapopspm.h"\
	".\wmask.h"\
	

"$(INTDIR)\Nve.obj"	"$(INTDIR)\Nve.sbr" : $(SOURCE) $(DEP_CPP_NVE_C)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\NveDb.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_NVEDB=\
	".\BkBitmap.h"\
	".\DataCollection.h"\
	".\dbclass.h"\
	".\mo_draw.h"\
	".\NveDb.h"\
	".\ptab.h"\
	".\PtabTable.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\traponve.h"\
	".\trapopspm.h"\
	".\wmask.h"\
	

"$(INTDIR)\NveDb.obj" : $(SOURCE) $(DEP_CPP_NVEDB) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_NVEDB=\
	".\BkBitmap.h"\
	".\DataCollection.h"\
	".\dbclass.h"\
	".\mo_draw.h"\
	".\NveDb.h"\
	".\ptab.h"\
	".\PtabTable.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\traponve.h"\
	".\trapopspm.h"\
	".\wmask.h"\
	

"$(INTDIR)\NveDb.obj"	"$(INTDIR)\NveDb.sbr" : $(SOURCE) $(DEP_CPP_NVEDB)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\NveGew.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_NVEGE=\
	"..\include\vector.h"\
	".\NveGew.h"\
	

"$(INTDIR)\NveGew.obj" : $(SOURCE) $(DEP_CPP_NVEGE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_NVEGE=\
	"..\include\vector.h"\
	".\NveGew.h"\
	

"$(INTDIR)\NveGew.obj"	"$(INTDIR)\NveGew.sbr" : $(SOURCE) $(DEP_CPP_NVEGE)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\NveStringTable.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_NVEST=\
	"..\include\Text.h"\
	".\BkBitmap.h"\
	".\DataCollection.h"\
	".\dbclass.h"\
	".\LeergutPart.h"\
	".\mo_draw.h"\
	".\Nve.h"\
	".\NveDb.h"\
	".\NveStringTable.h"\
	".\ptab.h"\
	".\PtabTable.h"\
	".\TransHandler.h"\
	".\traponve.h"\
	".\trapopspm.h"\
	".\wmask.h"\
	

"$(INTDIR)\NveStringTable.obj" : $(SOURCE) $(DEP_CPP_NVEST) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_NVEST=\
	"..\include\Text.h"\
	".\BkBitmap.h"\
	".\DataCollection.h"\
	".\dbclass.h"\
	".\LeergutPart.h"\
	".\mo_draw.h"\
	".\Nve.h"\
	".\NveDb.h"\
	".\NveStringTable.h"\
	".\ptab.h"\
	".\PtabTable.h"\
	".\TransHandler.h"\
	".\traponve.h"\
	".\trapopspm.h"\
	".\wmask.h"\
	

"$(INTDIR)\NveStringTable.obj"	"$(INTDIR)\NveStringTable.sbr" : $(SOURCE)\
 $(DEP_CPP_NVEST) "$(INTDIR)"


!ENDIF 

SOURCE=.\Palette.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PALET=\
	"..\include\Text.h"\
	".\palette.h"\
	

"$(INTDIR)\Palette.obj" : $(SOURCE) $(DEP_CPP_PALET) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PALET=\
	"..\include\Text.h"\
	".\palette.h"\
	

"$(INTDIR)\Palette.obj"	"$(INTDIR)\Palette.sbr" : $(SOURCE) $(DEP_CPP_PALET)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\pers.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PERS_=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	".\a_bas.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_meld.h"\
	".\pers.h"\
	".\strfkt.h"\
	

"$(INTDIR)\pers.obj" : $(SOURCE) $(DEP_CPP_PERS_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PERS_=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	".\a_bas.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_meld.h"\
	".\pers.h"\
	".\strfkt.h"\
	

"$(INTDIR)\pers.obj"	"$(INTDIR)\pers.sbr" : $(SOURCE) $(DEP_CPP_PERS_)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\pht_wg.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PHT_W=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\mo_curso.h"\
	".\pht_wg.h"\
	

"$(INTDIR)\pht_wg.obj" : $(SOURCE) $(DEP_CPP_PHT_W) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PHT_W=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\pht_wg.h"\
	

"$(INTDIR)\pht_wg.obj"	"$(INTDIR)\pht_wg.sbr" : $(SOURCE) $(DEP_CPP_PHT_W)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\pr_a_gx.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PR_A_=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\pr_a_gx.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\pr_a_gx.obj" : $(SOURCE) $(DEP_CPP_PR_A_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PR_A_=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\pr_a_gx.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\pr_a_gx.obj"	"$(INTDIR)\pr_a_gx.sbr" : $(SOURCE) $(DEP_CPP_PR_A_)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\prdk.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PRDK_=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\prdk.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\prdk.obj" : $(SOURCE) $(DEP_CPP_PRDK_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PRDK_=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\prdk.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\prdk.obj"	"$(INTDIR)\prdk.sbr" : $(SOURCE) $(DEP_CPP_PRDK_)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Process.cpp
DEP_CPP_PROCE=\
	"..\include\Text.h"\
	".\Process.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\Process.obj" : $(SOURCE) $(DEP_CPP_PROCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\Process.obj"	"$(INTDIR)\Process.sbr" : $(SOURCE) $(DEP_CPP_PROCE)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\prov_satz.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PROV_=\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_draw.h"\
	".\prov_satz.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\prov_satz.obj" : $(SOURCE) $(DEP_CPP_PROV_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PROV_=\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_draw.h"\
	".\prov_satz.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\prov_satz.obj"	"$(INTDIR)\prov_satz.sbr" : $(SOURCE)\
 $(DEP_CPP_PROV_) "$(INTDIR)"


!ENDIF 

SOURCE=.\prp_user.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PRP_U=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\prp_user.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\prp_user.obj" : $(SOURCE) $(DEP_CPP_PRP_U) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PRP_U=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\prp_user.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\prp_user.obj"	"$(INTDIR)\prp_user.sbr" : $(SOURCE) $(DEP_CPP_PRP_U)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\PTAB.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PTAB_=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\ptab.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\PTAB.OBJ" : $(SOURCE) $(DEP_CPP_PTAB_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PTAB_=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\ptab.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\PTAB.OBJ"	"$(INTDIR)\PTAB.SBR" : $(SOURCE) $(DEP_CPP_PTAB_)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\PtabFunc.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PTABF=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\ptab.h"\
	".\PtabFunc.h"\
	".\strfkt.h"\
	

"$(INTDIR)\PtabFunc.obj" : $(SOURCE) $(DEP_CPP_PTABF) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PTABF=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\ptab.h"\
	".\PtabFunc.h"\
	".\strfkt.h"\
	

"$(INTDIR)\PtabFunc.obj"	"$(INTDIR)\PtabFunc.sbr" : $(SOURCE) $(DEP_CPP_PTABF)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\PtabTable.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_PTABT=\
	"..\include\Text.h"\
	".\DataCollection.h"\
	".\ptab.h"\
	".\PtabTable.h"\
	

"$(INTDIR)\PtabTable.obj" : $(SOURCE) $(DEP_CPP_PTABT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_PTABT=\
	"..\include\Text.h"\
	".\DataCollection.h"\
	".\ptab.h"\
	".\PtabTable.h"\
	

"$(INTDIR)\PtabTable.obj"	"$(INTDIR)\PtabTable.sbr" : $(SOURCE)\
 $(DEP_CPP_PTABT) "$(INTDIR)"


!ENDIF 

SOURCE=.\ReadNve.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_READN=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\Text.h"\
	"..\include\wmaskc.h"\
	".\dbclass.h"\
	".\lsnve.h"\
	".\mo_curso.h"\
	".\readnve.h"\
	

"$(INTDIR)\ReadNve.obj" : $(SOURCE) $(DEP_CPP_READN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_READN=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\Text.h"\
	"..\include\wmaskc.h"\
	".\dbclass.h"\
	".\lsnve.h"\
	".\mo_curso.h"\
	".\readnve.h"\
	

"$(INTDIR)\ReadNve.obj"	"$(INTDIR)\ReadNve.sbr" : $(SOURCE) $(DEP_CPP_READN)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\scanean.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_SCANE=\
	"..\include\Text.h"\
	"..\include\vector.h"\
	".\integer.h"\
	".\scanean.h"\
	".\strfkt.h"\
	

"$(INTDIR)\scanean.obj" : $(SOURCE) $(DEP_CPP_SCANE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_SCANE=\
	"..\include\Text.h"\
	"..\include\vector.h"\
	".\integer.h"\
	".\scanean.h"\
	".\strfkt.h"\
	

"$(INTDIR)\scanean.obj"	"$(INTDIR)\scanean.sbr" : $(SOURCE) $(DEP_CPP_SCANE)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Sounds.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_SOUND=\
	"..\include\Text.h"\
	".\Sounds.h"\
	

"$(INTDIR)\Sounds.obj" : $(SOURCE) $(DEP_CPP_SOUND) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_SOUND=\
	"..\include\Text.h"\
	".\Sounds.h"\
	

"$(INTDIR)\Sounds.obj"	"$(INTDIR)\Sounds.sbr" : $(SOURCE) $(DEP_CPP_SOUND)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Stc.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_STC_C=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\Stc.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\Stc.obj" : $(SOURCE) $(DEP_CPP_STC_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_STC_C=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\Stc.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\Stc.obj"	"$(INTDIR)\Stc.sbr" : $(SOURCE) $(DEP_CPP_STC_C)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\STDFKT.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_STDFK=\
	"..\include\process.h"\
	"..\include\Text.h"\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\stdfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\STDFKT.OBJ" : $(SOURCE) $(DEP_CPP_STDFK) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_STDFK=\
	"..\include\process.h"\
	"..\include\Text.h"\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\stdfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\STDFKT.OBJ"	"$(INTDIR)\STDFKT.SBR" : $(SOURCE) $(DEP_CPP_STDFK)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\STRFKT.CPP
DEP_CPP_STRFK=\
	".\strfkt.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\STRFKT.OBJ" : $(SOURCE) $(DEP_CPP_STRFK) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\STRFKT.OBJ"	"$(INTDIR)\STRFKT.SBR" : $(SOURCE) $(DEP_CPP_STRFK)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\sys_par.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_SYS_P=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\mo_curso.h"\
	".\sys_par.h"\
	

"$(INTDIR)\sys_par.obj" : $(SOURCE) $(DEP_CPP_SYS_P) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_SYS_P=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\mo_curso.h"\
	".\sys_par.h"\
	

"$(INTDIR)\sys_par.obj"	"$(INTDIR)\sys_par.sbr" : $(SOURCE) $(DEP_CPP_SYS_P)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\SYS_PERI.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_SYS_PE=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\sys_peri.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\SYS_PERI.OBJ" : $(SOURCE) $(DEP_CPP_SYS_PE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_SYS_PE=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\sys_peri.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\SYS_PERI.OBJ"	"$(INTDIR)\SYS_PERI.SBR" : $(SOURCE) $(DEP_CPP_SYS_PE)\
 "$(INTDIR)"


!ENDIF 

SOURCE=..\libsrc\Text.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_TEXT_=\
	"..\include\Text.h"\
	"..\include\vector.h"\
	

"$(INTDIR)\Text.obj" : $(SOURCE) $(DEP_CPP_TEXT_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_TEXT_=\
	"..\include\Text.h"\
	"..\include\vector.h"\
	

"$(INTDIR)\Text.obj"	"$(INTDIR)\Text.sbr" : $(SOURCE) $(DEP_CPP_TEXT_)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\textblock.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_TEXTB=\
	"..\include\Text.h"\
	"..\include\vector.h"\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\textblock.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\textblock.obj" : $(SOURCE) $(DEP_CPP_TEXTB) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_TEXTB=\
	"..\include\Text.h"\
	"..\include\vector.h"\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\textblock.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\textblock.obj"	"$(INTDIR)\textblock.sbr" : $(SOURCE)\
 $(DEP_CPP_TEXTB) "$(INTDIR)"


!ENDIF 

SOURCE=.\touchf.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_TOUCH=\
	"..\include\Text.h"\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\nublock.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\touchf.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\touchf.obj" : $(SOURCE) $(DEP_CPP_TOUCH) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_TOUCH=\
	"..\include\Text.h"\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\nublock.h"\
	".\stdfkt.h"\
	".\strfkt.h"\
	".\touchf.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\touchf.obj"	"$(INTDIR)\touchf.sbr" : $(SOURCE) $(DEP_CPP_TOUCH)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\TransHandler.cpp
DEP_CPP_TRANS=\
	".\BkBitmap.h"\
	".\TransHandler.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\TransHandler.obj" : $(SOURCE) $(DEP_CPP_TRANS) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\TransHandler.obj"	"$(INTDIR)\TransHandler.sbr" : $(SOURCE)\
 $(DEP_CPP_TRANS) "$(INTDIR)"


!ENDIF 

SOURCE=.\traponve.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_TRAPO=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\traponve.h"\
	".\wmask.h"\
	

"$(INTDIR)\traponve.obj" : $(SOURCE) $(DEP_CPP_TRAPO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_TRAPO=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\traponve.h"\
	".\wmask.h"\
	

"$(INTDIR)\traponve.obj"	"$(INTDIR)\traponve.sbr" : $(SOURCE) $(DEP_CPP_TRAPO)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\trapopspm.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_TRAPOP=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\trapopspm.h"\
	".\wmask.h"\
	

"$(INTDIR)\trapopspm.obj" : $(SOURCE) $(DEP_CPP_TRAPOP) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_TRAPOP=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\trapopspm.h"\
	".\wmask.h"\
	

"$(INTDIR)\trapopspm.obj"	"$(INTDIR)\trapopspm.sbr" : $(SOURCE)\
 $(DEP_CPP_TRAPOP) "$(INTDIR)"


!ENDIF 

SOURCE=..\libsrc\Vector.cpp
DEP_CPP_VECTO=\
	"..\include\vector.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\Vector.obj" : $(SOURCE) $(DEP_CPP_VECTO) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\Vector.obj"	"$(INTDIR)\Vector.sbr" : $(SOURCE) $(DEP_CPP_VECTO)\
 "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\wa_charge.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_WA_CH=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\wa_charge.h"\
	".\wmask.h"\
	

"$(INTDIR)\wa_charge.obj" : $(SOURCE) $(DEP_CPP_WA_CH) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_WA_CH=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\TransHandler.h"\
	".\wa_charge.h"\
	".\wmask.h"\
	

"$(INTDIR)\wa_charge.obj"	"$(INTDIR)\wa_charge.sbr" : $(SOURCE)\
 $(DEP_CPP_WA_CH) "$(INTDIR)"


!ENDIF 

SOURCE=.\WApplication.cpp
DEP_CPP_WAPPL=\
	".\WApplication.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\WApplication.obj" : $(SOURCE) $(DEP_CPP_WAPPL) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\WApplication.obj"	"$(INTDIR)\WApplication.sbr" : $(SOURCE)\
 $(DEP_CPP_WAPPL) "$(INTDIR)"


!ENDIF 

SOURCE=.\WDialog.cpp
DEP_CPP_WDIAL=\
	".\WApplication.h"\
	".\WDialog.h"\
	

!IF  "$(CFG)" == "53600 - Win32 Release"


"$(INTDIR)\WDialog.obj" : $(SOURCE) $(DEP_CPP_WDIAL) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"


"$(INTDIR)\WDialog.obj"	"$(INTDIR)\WDialog.sbr" : $(SOURCE) $(DEP_CPP_WDIAL)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\wiepro.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_WIEPR=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	".\a_bas.h"\
	".\a_kun.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wiepro.h"\
	".\wmask.h"\
	

"$(INTDIR)\wiepro.obj" : $(SOURCE) $(DEP_CPP_WIEPR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_WIEPR=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	".\a_bas.h"\
	".\a_kun.h"\
	".\BkBitmap.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\kun.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_draw.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wiepro.h"\
	".\wmask.h"\
	

"$(INTDIR)\wiepro.obj"	"$(INTDIR)\wiepro.sbr" : $(SOURCE) $(DEP_CPP_WIEPR)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\WMASK.CPP

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_WMASK=\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\WMASK.OBJ" : $(SOURCE) $(DEP_CPP_WMASK) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_WMASK=\
	".\BkBitmap.h"\
	".\mo_draw.h"\
	".\strfkt.h"\
	".\TransHandler.h"\
	".\wmask.h"\
	

"$(INTDIR)\WMASK.OBJ"	"$(INTDIR)\WMASK.SBR" : $(SOURCE) $(DEP_CPP_WMASK)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\writenve.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_WRITE=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\Text.h"\
	"..\include\wmaskc.h"\
	".\dbclass.h"\
	".\lsnve.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	".\writenve.h"\
	

"$(INTDIR)\writenve.obj" : $(SOURCE) $(DEP_CPP_WRITE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_WRITE=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\Text.h"\
	"..\include\wmaskc.h"\
	".\dbclass.h"\
	".\lsnve.h"\
	".\mo_curso.h"\
	".\strfkt.h"\
	".\writenve.h"\
	

"$(INTDIR)\writenve.obj"	"$(INTDIR)\writenve.sbr" : $(SOURCE) $(DEP_CPP_WRITE)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\zerldaten.cpp

!IF  "$(CFG)" == "53600 - Win32 Release"

DEP_CPP_ZERLD=\
	"..\..\..\..\informix\incl\esql723tc9\sqlca.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlda.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql723tc9\sqlproto.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	".\a_bas.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\zerldaten.h"\
	

"$(INTDIR)\zerldaten.obj" : $(SOURCE) $(DEP_CPP_ZERLD) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "53600 - Win32 Debug"

DEP_CPP_ZERLD=\
	"..\..\..\..\informix\incl\esql\sqlca.h"\
	"..\..\..\..\informix\incl\esql\sqlda.h"\
	"..\..\..\..\informix\incl\esql\sqlhdr.h"\
	"..\..\..\..\informix\incl\esql\sqlproto.h"\
	"..\include\itemc.h"\
	"..\include\mo_draw.h"\
	"..\include\wmaskc.h"\
	".\a_bas.h"\
	".\dbclass.h"\
	".\dbfunc.h"\
	".\mdn.h"\
	".\mo_curso.h"\
	".\mo_meld.h"\
	".\strfkt.h"\
	".\zerldaten.h"\
	

"$(INTDIR)\zerldaten.obj"	"$(INTDIR)\zerldaten.sbr" : $(SOURCE)\
 $(DEP_CPP_ZERLD) "$(INTDIR)"


!ENDIF 


!ENDIF 

