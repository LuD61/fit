// DesAdv.cpp: Implementierung der Klasse CDesAdv.
//
//////////////////////////////////////////////////////////////////////
#include <windows.h>

#include "DesAdv.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CDesAdv::CDesAdv()
{
	memset (m_Nve, 0, sizeof (m_Nve));
	m_Gui = NULL;
}

CDesAdv::~CDesAdv()
{

}

void CDesAdv::Save ()
{
}

void CDesAdv::Print ()
{
	if (m_Gui != NULL)
	{
		m_Gui->PrintNveEti ();
	}
}

void CDesAdv::CreateNve ()
{
}
