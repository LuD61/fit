#ifndef _NUBLOCK_DEF
#define _NUBLOCK_DEF

class NUBLOCK 
{
       private :
             int break_num_enter;
             form *eForm;
             HWND ehWnd;
             int (*MeLPProc) (HWND);
             void (*SetNumCaret) (void);
             BOOL Num3Mode;
             HWND NumParent;
             BOOL NumZent;                  // zentriert in x-Richtung  
             int Numlcx;                    // Fenster-breite  
             int Numlcy;                    // y - Abstand vom unteren Rand
             char neditbuff [40];
             double neditsum;
             char neaction;
             mfont nTextNumFont;
             field _nTextForm[1]; 
             form nTextForm;
             mfont nEditNumFont;
             field _nEditForm[1]; 
             form nEditForm;
			 BOOL Topmost;

       public :
// RoW 22.02.2005 Ean128 mit Endezeichen

             int (*CharChangeProc) (int);
// RoW 22.02.2005 Ean128 mit Endezeichen Ende
           NUBLOCK ();

		   void SetTopmost (BOOL Topmost)
		   {
			   this->Topmost = Topmost;
		   }
           void SetEform (form *eF, HWND ehW, int (*eFP) (WPARAM))
           {
	         eForm     = eF;
 	         ehWnd     = ehW;
           }

           void SetMeProc (int (*MeLp) (HWND))
           {
	        MeLPProc = MeLp;
           }

           void SetNumCaretProc (void (*NuCr) (void))
           {
	         SetNumCaret = NuCr;
           }

           void SetNum3 (BOOL mode)
           {
	         Num3Mode = min (TRUE, mode);
           }

           void SetNumParent (HWND hWnd)
           {
	         NumParent = hWnd;
           }

           void SetNumZent (BOOL mode, int x, int y)
           {
	         NumZent = mode;
	         Numlcx = x;              
	         Numlcy = y;
           }
           void MainInstance (HANDLE, HWND);
           void SetParent (HWND);
           void ScreenParam (double scrx, double scry);
           void SetStorno (int (*) (void), int);	    
           void SetChargeNeu (int (*) (void), int);	// 070112 GK    
           void SetChoise (int (*) (void), int);
           void SetChoise (int (*) (void), int, LPSTR);
		   void SetValue (LPSTR);
           void NumEnterBreak ();
           int IsNumKey (MSG *);
           HWND IsNumChild (POINT *);
           void RegisterNumEnter (WNDPROC);
           int sNumKey (MSG *);
           void KorrMainButton (form *,  double, double);
           void EnterNumBox (HWND, char *, char *,  int, char *, WNDPROC);
           void EnterCalcBox (HWND, char *, char *, int, char *, WNDPROC);
           int OnPaint (HWND,UINT, WPARAM,LPARAM);
           int OnButton (HWND,UINT, WPARAM,LPARAM);
// RoW 22.02.2005 Ean128 mit Endezeichen
           void  SetCharChangeProc (int (*) (int));
 		   void SetEditBuffer (LPSTR buff);
		   void GetEditBuffer (LPSTR buff, int len);
		   HWND GeteNumWindow ();
		   void DisplayEditForm ();
		   void SetNumP (char *s);
		   void SetNumM (char *s);
		   void ChangeNumBkColor ();
};

#endif