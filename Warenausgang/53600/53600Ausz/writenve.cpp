#include "WriteNve.h"
#include "strfkt.h"
#include "mo_curso.h"
  

WriteNve::WriteNve ()
{
	LPSTR tmp;

	char buffer [512];
    short tlen;

	tmp = getenv ("TMPPATH");
	if (tmp == NULL) return;
	Text FileName;
	FileName.Format ("%s\\WriteNve.class", tmp);
	FILE *fp = fopen (FileName.GetBuffer (), "rb");
	if (fp == NULL) return;

    fread (&nve_posi, 1, sizeof (long), fp);
    fread (&tlen, 1, sizeof (short), fp);
    fread (buffer, 1, tlen, fp);
	buffer[tlen] = 0;
	nve = buffer;
    fread (&tlen, 1, sizeof (short), fp);
    fread (buffer, 1, tlen, fp);
	buffer[tlen] = 0;
	nve_palette = buffer;
    fread (&mdn, 1, sizeof (short), fp);
    fread (&fil, 1, sizeof (short), fp);
    fread (&ls, 1, sizeof (long), fp);
    fread (&a,  1, sizeof (double), fp);
    fread (&me, 1, sizeof (double), fp);
    fread (&me_einh, 1, sizeof (short), fp);
	fclose (fp);
}


	   
WriteNve::WriteNve (Text& nve, Text& nve_palette, 
					short mdn, short fil,
					long ls, double a,
					double me, short me_einh)
{
	       this->nve = nve;
	       this->nve_palette = nve_palette;
   	       this->mdn = mdn;
   	       this->fil = fil;
   	       this->ls = ls;
   	       this->a = a;
   	       this->me = me;
   	       this->me_einh = me_einh;
           lsnve.nve_posi = 0;
		   lsnve.mdn = mdn;
		   lsnve.fil = fil;
		   lsnve.ls = ls;
		   Lsnve.prepare ();
		   Save = FALSE;
}

WriteNve::~WriteNve ()
{
	if (!Save) return;
	LPSTR tmp;
    short tlen;
	tmp = getenv ("TMPPATH");
	if (tmp == NULL) return;
	Text FileName;
	FileName.Format ("%s\\WriteNve.class", tmp);
	FILE *fp = fopen (FileName.GetBuffer (), "wb");
	if (fp == NULL) return;

    fwrite (&nve_posi, 1, sizeof (long), fp);
	tlen = nve.GetLength ();
    fwrite (&tlen, 1, sizeof (short), fp);
    fwrite (nve.GetBuffer (), 1, tlen, fp);
	tlen = nve_palette.GetLength ();
    fwrite (&tlen, 1, sizeof (short), fp);
    fwrite (nve_palette.GetBuffer (), 1, tlen, fp);
    fwrite (&mdn, 1, sizeof (short), fp);
    fwrite (&fil, 1, sizeof (short), fp);
    fwrite (&ls, 1, sizeof (long), fp);
    fwrite (&a,  1, sizeof (double), fp);
    fwrite (&me, 1, sizeof (double), fp);
    fwrite (&me_einh, 1, sizeof (short), fp);

	fclose (fp);
}

void WriteNve::SetMe (double me)
{
	       this->me = me;
}

void WriteNve::SetMhd (LPSTR mhd)
{
	       this->mhd = dasc_to_long (mhd);
}

long WriteNve::GetMhd ()
{
		   return mhd;
}

void WriteNve::SetNve (Text& nve)
{
	       this->nve = nve;
}

void WriteNve::SetNvePalette (Text& nve_palette)
{
	       this->nve_palette = nve_palette;
}

void WriteNve::SetNvePosi (long nve_posi)
{
           lsnve.nve_posi = nve_posi;
}
	       

long WriteNve::GenNvePosi ()
{
           lsnve.nve_posi = 0l;

	       Lsnve.sqlin ((short *) &lsnve.mdn, 1, 0);
	       Lsnve.sqlin ((short *) &lsnve.fil, 1, 0);
	       Lsnve.sqlin ((long *) &lsnve.ls, 2, 0);
		   int dsqlstatus = Lsnve.sqlcomm ("select * from lsnve "
			                               "where mdn = ? "
						                   "and fil = ? "
						                   "and ls = ?");
		   if (dsqlstatus == 100)
		   {
                   lsnve.nve_posi ++;
		           return lsnve.nve_posi;
		   }

	       Lsnve.sqlin ((short *) &lsnve.mdn, 1, 0);
	       Lsnve.sqlin ((short *) &lsnve.fil, 1, 0);
	       Lsnve.sqlin ((long *) &lsnve.ls, 2, 0);
	       Lsnve.sqlout ((long *) &lsnve.nve_posi, 2, 0);
		   dsqlstatus = Lsnve.sqlcomm ("select max (nve_posi) from lsnve "
			                               "where mdn = ? "
						                   "and fil = ? "
						                   "and ls = ?");
		   lsnve.nve_posi = (lsnve.nve_posi < 0) ? 0 : lsnve.nve_posi;
           lsnve.nve_posi ++;
		   return lsnve.nve_posi;
}
	        

void WriteNve::write ()
{
	       char tag [12];

	       lsnve.mdn = mdn;
		   lsnve.fil = fil;
		   lsnve.ls  = ls;
		   lsnve.a   = a;
		   nve.Format ("%09.0lf", (double) atof (nve.GetBuffer ())); 
		   nve_palette.Format ("%09.0lf", (double) atof (nve_palette.GetBuffer ())); 
		   strcpy (lsnve.nve, nve.GetBuffer ());
		   strcpy (lsnve.nve_palette, nve_palette.GetBuffer ());
		   lsnve.me = me;
		   lsnve.me_einh = me_einh;
		   lsnve.mhd = mhd;
		   lsnve.gue = 1;
		   sysdate (tag);
		   lsnve.tag = dasc_to_long (tag);
		   if (nve == nve_palette)
		   {
			   lsnve.palette_kz = 1;
		   }
		   else
		   {
			   lsnve.palette_kz = 0;
		   }
		   Lsnve.dbupdate ();
}

void WriteNve::write (short gue)
{
	       char tag [12];

	       lsnve.mdn = mdn;
		   lsnve.fil = fil;
		   lsnve.ls  = ls;
		   lsnve.a   = a;
		   nve.Format ("%09.0lf", (double) atof (nve.GetBuffer ())); 
		   nve_palette.Format ("%09.0lf", (double) atof (nve_palette.GetBuffer ())); 
		   strcpy (lsnve.nve, nve.GetBuffer ());
		   strcpy (lsnve.nve_palette, nve_palette.GetBuffer ());
		   lsnve.me = me;
		   lsnve.me_einh = me_einh;
		   lsnve.gue = gue;
		   lsnve.mhd = mhd;
		   sysdate (tag);
		   lsnve.tag = dasc_to_long (tag);
		   if (nve == nve_palette)
		   {
			   writePalette (gue);
			   lsnve.palette_kz = 1;
		   }
		   else
		   {
			   lsnve.palette_kz = 0;
		   }
		   Lsnve.dbupdate ();
}

void WriteNve::write (short gue, double brutto_gew)
{
	       char tag [12];

	       lsnve.mdn = mdn;
		   lsnve.fil = fil;
		   lsnve.ls  = ls;
		   lsnve.a   = a;
		   nve.Format ("%09.0lf", (double) atof (nve.GetBuffer ())); 
		   nve_palette.Format ("%09.0lf", (double) atof (nve_palette.GetBuffer ())); 
		   strcpy (lsnve.nve, nve.GetBuffer ());
		   strcpy (lsnve.nve_palette, nve_palette.GetBuffer ());
		   lsnve.me = me;
		   lsnve.me_einh = me_einh;
		   lsnve.gue = gue;
		   lsnve.mhd = mhd;
		   lsnve.brutto_gew = brutto_gew;
		   sysdate (tag);
		   lsnve.tag = dasc_to_long (tag);
		   if (nve == nve_palette)
		   {
			   writePalette (gue);
			   lsnve.palette_kz = 1;
		   }
		   else
		   {
			   lsnve.palette_kz = 0;
		   }
		   Lsnve.dbupdate ();
}

void WriteNve::writePalettePosGue ()
{

	       lsnve.mdn = mdn;
		   lsnve.fil = fil;
		   lsnve.ls  = ls;
		   lsnve.a   = a;
		   nve.Format ("%09.0lf", (double) atof (nve.GetBuffer ())); 
		   nve_palette.Format ("%09.0lf", (double) atof (nve_palette.GetBuffer ())); 
		   strcpy (lsnve.nve, nve.GetBuffer ());
		   strcpy (lsnve.nve_palette, nve_palette.GetBuffer ());
		   lsnve.me = me;
		   lsnve.me_einh = me_einh;
		   lsnve.gue = 1;
		   lsnve.palette_kz = 0;
           Lsnve.sqlin ((char *) lsnve.nve_palette, 0, 10);
		   Lsnve.sqlcomm ("update lsnve set gue = 1 "
			              "where nve_palette = ? "
						  "and nve_palette != nve");
//		   Lsnve.dbupdate ();
}

void WriteNve::writePalette (short gue)
{
           Lsnve.sqlin ((short *) &gue, 1, 0);
           Lsnve.sqlin ((char *)  lsnve.nve, 0, 10);
		   Lsnve.sqlcomm ("update lsnve set gue = ? "
			              "where nve_palette = ? "
						  "and nve_palette != nve");
}
	       
void WriteNve::writeGue (short gue)
{
           Lsnve.sqlin ((short *) &gue, 1, 0);
           Lsnve.sqlin ((char *)  lsnve.nve, 0, 10);
		   Lsnve.sqlcomm ("update lsnve set gue = ? "
			              "where nve_palette = ?");
}

BOOL WriteNve::Equals (WriteNve *writeNve)
{
	writeNve->nve_palette.Trim ();
	nve_palette.Trim ();

	if (writeNve->nve_palette == nve_palette &&
	    a != writeNve->a) return TRUE;
	return FALSE;
}

BOOL WriteNve::Equals (LPSTR nve_p, long ls, double a)
{

	Text NvePalette = nve_p;

	nve_palette.Trim ();
	NvePalette.Trim ();

	if (NvePalette == nve_palette &&
	    (this->a != a || this->ls != ls)) return TRUE;
	return FALSE;
}

