// LicenceDialog.h: Schnittstelle f�r die Klasse LicenceDialog.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LICENCEDIALOG_H__9F859584_4252_4C37_BC7C_24EF6618414B__INCLUDED_)
#define AFX_LICENCEDIALOG_H__9F859584_4252_4C37_BC7C_24EF6618414B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <windows.h>
#include "WDialog.h"
#include "resource.h"
#include <string>

using namespace std;

class LicenceDialog : public WDialog  
{
private:
	enum {IDD=IDD_LICENCE};

	string text1;
	string text2;
	string text3;
	string text4;
	string text5;
	string text6;
	string text7;
	string text8;
	string text9;

	BOOL AbbortRow;

	HBRUSH m_DlgBrush;
	HBRUSH m_WhiteBrush;
	HFONT m_hFont1;
	HFONT m_hFont4;
	HFONT m_hFont5;
	HFONT m_hFont10;

	void SetRowText (string *text, DWORD Idc);
	void SetRowFont (char *FontName, int FontHeight, DWORD Idc, HFONT *hFont, BOOL Bold=FALSE);

protected:
	virtual bool OnInitDialog ();
	virtual HBRUSH OnCtlColor (HDC gDC, HWND hWnd, UINT nCtlColor); 
public:
	void SetAbbortRow (BOOL AbbortRow=TRUE)
	{
		this->AbbortRow = AbbortRow;
	}

    void SetText1 (char *text)
	{
		this->text1 = text;
	}

    void SetText2 (char *text)
	{
		this->text2 = text;
	}

    void SetText3 (char *text)
	{
		this->text3 = text;
	}

    void SetText4 (char *text)
	{
		this->text4 = text;
	}

    void SetText5 (char *text)
	{
		this->text5 = text;
	}

    void SetText6 (char *text)
	{
		this->text6 = text;
	}

    void SetText7 (char *text)
	{
		this->text7 = text;
	}
    void SetText8 (char *text)
	{
		this->text8 = text;
	}
    void SetText9 (char *text)
	{
		this->text9 = text;
	}

	void SetText (char *text);

	LicenceDialog();
	virtual ~LicenceDialog();

};

#endif // !defined(AFX_LICENCEDIALOG_H__9F859584_4252_4C37_BC7C_24EF6618414B__INCLUDED_)
