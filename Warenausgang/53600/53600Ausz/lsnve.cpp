#include <windows.h>
#include "wmaskc.h"
#include "mo_curso.h"
#include "lsnve.h"

struct LSNVE lsnve, lsnve_null;

void LSNVE_CLASS::prepare (void)
{
            char *sqltext;

            ins_quest ((char *) &lsnve.mdn, 1, 0);
            ins_quest ((char *) &lsnve.fil, 1, 0);
            ins_quest ((char *) &lsnve.ls, 2, 0);
            ins_quest ((char *) lsnve.nve, 0, 10);
    out_quest ((char *) &lsnve.mdn,1,0);
    out_quest ((char *) &lsnve.fil,1,0);
    out_quest ((char *) &lsnve.nve_posi,2,0);
    out_quest ((char *) lsnve.nve,0,21);
    out_quest ((char *) lsnve.nve_palette,0,21);
    out_quest ((char *) &lsnve.ls,2,0);
    out_quest ((char *) &lsnve.a,3,0);
    out_quest ((char *) &lsnve.me,3,0);
    out_quest ((char *) &lsnve.me_einh,1,0);
    out_quest ((char *) &lsnve.gue,1,0);
    out_quest ((char *) &lsnve.palette_kz,1,0);
    out_quest ((char *) &lsnve.tag,2,0);
    out_quest ((char *) &lsnve.mhd,2,0);
    out_quest ((char *) &lsnve.brutto_gew,3,0);
    out_quest ((char *) &lsnve.verp_art,2,0);
    out_quest ((char *) &lsnve.ps_art,3,0);
    out_quest ((char *) &lsnve.ps_stck,3,0);
    out_quest ((char *) &lsnve.ps_art1,3,0);
    out_quest ((char *) &lsnve.ps_stck1,3,0);
    out_quest ((char *) &lsnve.netto_gew,3,0);
    out_quest ((char *) lsnve.masternve,0,21);
    out_quest ((char *) lsnve.ls_charge,0,31);
            cursor = prepare_sql ("select lsnve.mdn,  lsnve.fil,  "
"lsnve.nve_posi,  lsnve.nve,  lsnve.nve_palette,  lsnve.ls,  lsnve.a,  "
"lsnve.me,  lsnve.me_einh,  lsnve.gue,  lsnve.palette_kz,  lsnve.tag,  "
"lsnve.mhd,  lsnve.brutto_gew,  lsnve.verp_art,  lsnve.ps_art,  "
"lsnve.ps_stck,  lsnve.ps_art1,  lsnve.ps_stck1,  lsnve.netto_gew,  "
"lsnve.masternve,  lsnve.ls_charge from lsnve "

#line 17 "lsnve.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ls = ? "
                                  "and   nve = ?");

    ins_quest ((char *) &lsnve.mdn,1,0);
    ins_quest ((char *) &lsnve.fil,1,0);
    ins_quest ((char *) &lsnve.nve_posi,2,0);
    ins_quest ((char *) lsnve.nve,0,21);
    ins_quest ((char *) lsnve.nve_palette,0,21);
    ins_quest ((char *) &lsnve.ls,2,0);
    ins_quest ((char *) &lsnve.a,3,0);
    ins_quest ((char *) &lsnve.me,3,0);
    ins_quest ((char *) &lsnve.me_einh,1,0);
    ins_quest ((char *) &lsnve.gue,1,0);
    ins_quest ((char *) &lsnve.palette_kz,1,0);
    ins_quest ((char *) &lsnve.tag,2,0);
    ins_quest ((char *) &lsnve.mhd,2,0);
    ins_quest ((char *) &lsnve.brutto_gew,3,0);
    ins_quest ((char *) &lsnve.verp_art,2,0);
    ins_quest ((char *) &lsnve.ps_art,3,0);
    ins_quest ((char *) &lsnve.ps_stck,3,0);
    ins_quest ((char *) &lsnve.ps_art1,3,0);
    ins_quest ((char *) &lsnve.ps_stck1,3,0);
    ins_quest ((char *) &lsnve.netto_gew,3,0);
    ins_quest ((char *) lsnve.masternve,0,21);
    ins_quest ((char *) lsnve.ls_charge,0,31);
            sqltext = "update lsnve set lsnve.mdn = ?,  "
"lsnve.fil = ?,  lsnve.nve_posi = ?,  lsnve.nve = ?,  "
"lsnve.nve_palette = ?,  lsnve.ls = ?,  lsnve.a = ?,  lsnve.me = ?,  "
"lsnve.me_einh = ?,  lsnve.gue = ?,  lsnve.palette_kz = ?,  "
"lsnve.tag = ?,  lsnve.mhd = ?,  lsnve.brutto_gew = ?,  "
"lsnve.verp_art = ?,  lsnve.ps_art = ?,  lsnve.ps_stck = ?,  "
"lsnve.ps_art1 = ?,  lsnve.ps_stck1 = ?,  lsnve.netto_gew = ?,  "
"lsnve.masternve = ?,  lsnve.ls_charge = ? "

#line 23 "lsnve.rpp"
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ls = ? "
                                  "and   nve = ?";


            ins_quest ((char *) &lsnve.mdn, 1, 0);
            ins_quest ((char *) &lsnve.fil, 1, 0);
            ins_quest ((char *) &lsnve.ls, 2, 0);
            ins_quest ((char *) lsnve.nve, 0, 10);
            upd_cursor = prepare_sql (sqltext);

            ins_quest ((char *) &lsnve.mdn, 1, 0);
            ins_quest ((char *) &lsnve.fil, 1, 0);
            ins_quest ((char *) &lsnve.ls, 2, 0);
            ins_quest ((char *) lsnve.nve, 0, 10);
            test_upd_cursor = prepare_sql ("select a from lsnve "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ls = ? "
                                  "and   nve = ?");

            ins_quest ((char *) &lsnve.mdn, 1, 0);
            ins_quest ((char *) &lsnve.fil, 1, 0);
            ins_quest ((char *) &lsnve.ls, 2, 0);
            ins_quest ((char *) lsnve.nve, 0, 10);
            del_cursor = prepare_sql ("delete from lsnve "
                                  "where mdn = ? "
                                  "and   fil = ? "
                                  "and   ls = ? "
                                  "and   nve = ?");

    ins_quest ((char *) &lsnve.mdn,1,0);
    ins_quest ((char *) &lsnve.fil,1,0);
    ins_quest ((char *) &lsnve.nve_posi,2,0);
    ins_quest ((char *) lsnve.nve,0,21);
    ins_quest ((char *) lsnve.nve_palette,0,21);
    ins_quest ((char *) &lsnve.ls,2,0);
    ins_quest ((char *) &lsnve.a,3,0);
    ins_quest ((char *) &lsnve.me,3,0);
    ins_quest ((char *) &lsnve.me_einh,1,0);
    ins_quest ((char *) &lsnve.gue,1,0);
    ins_quest ((char *) &lsnve.palette_kz,1,0);
    ins_quest ((char *) &lsnve.tag,2,0);
    ins_quest ((char *) &lsnve.mhd,2,0);
    ins_quest ((char *) &lsnve.brutto_gew,3,0);
    ins_quest ((char *) &lsnve.verp_art,2,0);
    ins_quest ((char *) &lsnve.ps_art,3,0);
    ins_quest ((char *) &lsnve.ps_stck,3,0);
    ins_quest ((char *) &lsnve.ps_art1,3,0);
    ins_quest ((char *) &lsnve.ps_stck1,3,0);
    ins_quest ((char *) &lsnve.netto_gew,3,0);
    ins_quest ((char *) lsnve.masternve,0,21);
    ins_quest ((char *) lsnve.ls_charge,0,31);
            ins_cursor = prepare_sql ("insert into lsnve ("
"mdn,  fil,  nve_posi,  nve,  nve_palette,  ls,  a,  me,  me_einh,  gue,  palette_kz,  tag,  mhd,  "
"brutto_gew,  verp_art,  ps_art,  ps_stck,  ps_art1,  ps_stck1,  netto_gew,  "
"masternve,  ls_charge) "

#line 56 "lsnve.rpp"
                                      "values "
                                      "(?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?)"); 

#line 58 "lsnve.rpp"
}

int LSNVE_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

