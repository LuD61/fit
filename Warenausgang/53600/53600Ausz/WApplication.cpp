// WApplication.cpp: Implementierung der Klasse WApplication.
//
//////////////////////////////////////////////////////////////////////

#include "WApplication.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

WApplication::WApplication()
{

}

WApplication::~WApplication()
{

}

WApplication *WApplication::Instance = NULL;

WApplication *WApplication::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new WApplication ();
	}
	return Instance;
}


void WApplication::DestroyInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}
