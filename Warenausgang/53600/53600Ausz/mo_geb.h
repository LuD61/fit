#ifndef _GEB_DEF
#define _GEB_DEF

struct A_KUN_GEB
{
	     long  sys;
         short mdn;
         short fil; 
         long kun;
         char kun_bran2[3];
         double a;
         char geb_eti [2];
         short geb_fill;
         long geb_anz;
         char pal_eti[2];
         short pal_fill;
         long pal_anz;
         char pos_eti[2];
         short  pos_fill;
         double tara;
         short  mhd;
};

extern struct A_KUN_GEB a_kun_geb, a_kun_geb_null;
         

class GEB_CLASS
{
         private :
              int a_kun_curs; 
              int a_bran_curs; 
              int kun_geb_curs;
              int geb_curs_upd;
              int geb_curs_ins;
              int test_geb_curs;
              void prepare (void);
         public :
            GEB_CLASS () 
            {
                  a_kun_curs = -1;
            }
            read_a_kun_first ();
            read_bran_first ();
            read_kun_geb_first ();
            update_kun_geb ();
};
#endif
