// Decimal.cpp: Implementierung der Klasse Decimal.
//
//////////////////////////////////////////////////////////////////////

#include "Decimal.h"

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

Decimal::Decimal()
{
	memset (m_Digits, 0, sizeof (m_Digits));
	m_Scale = 0;
	m_Signed = FALSE;
}

Decimal::~Decimal()
{

}

void Decimal::SetValue (LPSTR value)
{
	char c;
	UCHAR digits[CHARSIZE];

	int size = strlen (value);

	memset (m_Digits, 0, sizeof (m_Digits));
	memset (digits, 0, sizeof (m_Digits));
	int idx = 0;
	BOOL shift = TRUE;
	if ((size % 2) != 0)
	{
		shift = FALSE;
	}
	for (int i = 0; (i < size) && (i < MAXDIGITS); i ++) 
	{
		c = value[i];
		c &= 0xF;
		if (shift)
		{
			digits[idx] = (c << 4) & 0xF0;
			shift = FALSE;
		}
		else
		{
			digits[idx] |= c;
			idx ++;
			shift = TRUE;
		}
	}
	idx --;
	int pos = CHARSIZE - 1;
	for (; idx >= 0; idx --, pos --)
	{
		m_Digits[pos] = digits[idx];
	}
}

LPSTR Decimal::StringValue ()
{
	int i;
	int idx;
	char c;
	char low;
	char high;

	memset (m_StringValue, 0, sizeof (m_StringValue));
	m_StringValue[0] = '0';

	for (i = 0; i < CHARSIZE; i ++)
	{
		if (m_Digits[i] > 0) break;
	}
	if (i != CHARSIZE)
	{
		idx = 0;
		c = m_Digits[i];
		high = (c >> 4) & 0xF;
		low  = c & 0xF;
        if (high != 0)
		{
			m_StringValue[idx] = high | 0x30;
			idx ++;
		}
		m_StringValue[idx] = low | 0x30;
		idx ++;
		for (i = i + 1; i < CHARSIZE; i ++)
		{
			c = m_Digits[i];
			high = (c >> 4) & 0xF;
			low  = c & 0xF;
			m_StringValue[idx] = high | 0x30;
			idx ++;
			m_StringValue[idx] = low | 0x30;
			idx ++;
		}
	}
	return m_StringValue;

}