// TransHandler.h: Schnittstelle f�r die Klasse CTransHandler.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TRANSHANDLER_H__8D5EDFDC_36D6_4A1F_B02D_F9F441E3CF99__INCLUDED_)
#define AFX_TRANSHANDLER_H__8D5EDFDC_36D6_4A1F_B02D_F9F441E3CF99__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include <windows.h>
#include <vector>
#include "BkBitmap.h"

using namespace std;

class CTransparent
{
public:
	enum XALIGNMENT
	{
		Left = 0,
        Right = 1,
		Center = 2,
	};

	enum YALIGNMENT
	{
		Top = 0,
        Bottom = 1,
		RowCenter = 2,
	};

private :
	int x, y, cx, cy;
	char *text;
	HWND Parent;
	COLORREF TextColor;
	HFONT hFont;
	HFONT oldFont;
	XALIGNMENT xAlignment;
	YALIGNMENT yAlignment;
	CBkBitmap Background;
    int HeightY (int cy, HDC hdc);
public :
	void SetXAlignment (XALIGNMENT xAlignment)
	{
		this->xAlignment = xAlignment;
	}

	void SetYAlignment (YALIGNMENT yAlignment)
	{
		this->yAlignment = yAlignment;
	}

	void SetParent (HWND Parent)
	{
		this->Parent = Parent;
	}

    HWND GetParent ()
	{
		return Parent;
	}


	void SetX (int x)
	{
		this->x = x;
	}

	int GetX ()
	{
		return x;
	}

	void SetY (int y)
	{
		this->y = y;
	}

	int GetY ()
	{
		return y;
	}

	void SetCX (int cx)
	{
		this->cx = cx;
	}

	int GetCX ()
	{
		return cx;
	}

	void SetCY (int c)
	{
		this->cy = cy;
	}

	int GetCY ()
	{
		return cy;
	}

	void SetText (char *text)
	{
		if (this->text != NULL)
		{
			delete this->text;
			this->text = NULL;
		}
		this->text = new char[strlen(text) + 1];
		strcpy (this->text, text);
	}

	char *GetText ()
	{
		return text;
	}

	void SetTextColor (COLORREF TextColor)
	{
		this->TextColor = TextColor;
	}

	COLORREF GetTextColor ()
	{
		return TextColor;
	}

	void SetHFont (HFONT hFont)
	{
		this->hFont = hFont;
	}

	HFONT GetHFont ()
	{
		return hFont;
	}

	CTransparent ();
	~CTransparent ();

	void Create ();
	void Create (HWND Parent, int x, int y, int cx, int cy, char *text);
	void Destroy ();
	void Draw (HDC hdc);
	void Draw ();
	void Invalidate ();
};


class CTransHandler  
{
private :
	std::vector<CTransparent *> windows;
	static CTransHandler *Instance;
protected:
	CTransHandler();
	virtual ~CTransHandler();
public:
	static CTransHandler *GetInstance ();
	static void DestroyInstance ();
	void Add (CTransparent *window);
	void Drop (CTransparent *window);
	void Draw (HWND Parent, HDC hdc);
	void Draw (HWND Parent);
};

#endif // !defined(AFX_TRANSHANDLER_H__8D5EDFDC_36D6_4A1F_B02D_F9F441E3CF99__INCLUDED_)
