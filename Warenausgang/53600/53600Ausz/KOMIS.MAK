# Makefile "maketest": test Testprogramm
# BIZERBA Balingen VI-WW-B

.SUFFIXES: .obj .cpp

#CC   = bcc -ml
CC = cl
#LINK = bcc
LINK = link

#CFLAGS = -c -W -w- -I/borlandc/include 
CFLAGS = -c -W3 -D_NODBCLASS
PROGRAMM = komis.exe 
GUILIBS= user32.lib gdi32.lib winmm.lib comdlg32.lib comctl32.lib \
        isqlt07c.lib fnb.lib
RCVARS=-r -DWIN32

OBJ = komis.obj \
        mo_meld.obj \
        mo_curso.obj \
        mo_numme.obj \
        sys_peri.obj \
        sys_par.obj \
        pht_wg.obj \
        strfkt.obj \
        stdfkt.obj \
        mo_menu.obj \
        mdn.obj \
        fil.obj \
        ls.obj \
        a_bas.obj \
        a_hndw.obj \
        kun.obj \
        mo_geb.obj \
        ptab.obj \
        dbfunc.obj \
        mo_draw.obj \
        a_pr.obj \
        akt_krz.obj \
        aktion.obj \
        mo_preis.obj \
        mo_a_pr.obj \
        mo_einh.obj \
        wmask.obj

$(PROGRAMM): $(OBJ) komis.res
         $(CC) -Fekomis.exe $(OBJ) komis.res $(GUILIBS)
         copy komis.exe \user\fit\bin\53600.exe
.cpp.obj :
	$(CC) $(CFLAGS) $*.cpp

komis.res : komis.rc
        rc $(RCVARS) komis.rc 
