// Nve.h: Schnittstelle f�r die Klasse CNve.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NVE_H__65F7C2C2_6BFE_46AB_9C09_C646F57F6D48__INCLUDED_)
#define AFX_NVE_H__65F7C2C2_6BFE_46AB_9C09_C646F57F6D48__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "traponve.h"
#include "trapopspm.h"
#include "ptab.h"
#include "Text.h"
#include "NveDb.h"
#include "LeergutPart.h"

#define CString Text

#define MAXPARTS 5

class CNve  
{
private:
	CString         m_LGutString;
	CString         m_LeftPart;
    CString         m_RightPart;
	CLeergutPart    m_NvePart;
	CLeergutPart    m_PsPmPart[MAXPARTS];
	int             m_PsPmParts;
	long            m_Lfd;
	CNveDb *        m_NveDb;
	CString         m_Nve;  
	CString         m_Iln;
	short           m_Mdn;
	short           m_Fil;
	long            m_Ls;
	CString         m_BlgTyp;
	BOOL            m_Error;
	int             m_ErrorNumber;
	static const LPSTR      
		            m_ErrorStrings[];

	
public:

	BOOL Error ()
	{
		return m_Error;
	}

    void set_LGutString (LPTSTR lGutString)
	{
		m_LGutString = lGutString;
	}

	const LPTSTR LGutString ()
	{
		return m_LGutString.GetBuffer ();
	}

	void set_Iln (LPSTR iln)
	{
		m_Iln = iln;
	}

	const LPSTR Iln ()
	{
		return m_Iln;
	}

	void set_Mdn (short mdn)
	{
		m_Mdn = mdn;
	}

	void set_Fil (short fil)
	{
		m_Fil = fil;
	}

	void set_Ls (long ls)
	{
		m_Ls = ls;
	}

	void set_BlgTyp (LPSTR blg_typ)
	{
		m_BlgTyp = blg_typ;
	}

	CNveDb *NveDb ()
	{
		return m_NveDb;
	}

	CNve();
	virtual ~CNve();
	void CreateParts ();
	void CreateRightParts ();
	void CreateNve (int lfd);
	LPSTR CreatePz (LPSTR nve, char pz[1]);
	void Update ();
	const LPSTR GetErrorText ();


};

#endif // !defined(AFX_NVE_H__65F7C2C2_6BFE_46AB_9C09_C646F57F6D48__INCLUDED_)
