{ TABLE "fit".lspeinzel row size = 328 number of columns = 65 index size = 75 }
create table "fit".tmp_catering 
  (
    mdn smallint,
    fil smallint,
    ls integer,
    auf integer,
    a decimal(13,0),
    posi integer,
    auf_me decimal(8,3),
    auf_me_bz char(6),
    lsp_txt integer,
    kun integer,
    teil_smt smallint,
    a_lsp decimal(13,0),
    prdk_typ smallint,
    extra smallint,
    sort smallint
    
  );
revoke all on "fit".tmp_catering from "public";

create unique index "fit".i01tmp_catering on "fit".tmp_catering 
    (mdn,fil,ls,auf,a,posi);




