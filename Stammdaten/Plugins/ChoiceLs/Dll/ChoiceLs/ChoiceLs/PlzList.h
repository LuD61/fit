#ifndef _PLZ_LIST_DEF
#define _PLZ_LIST_DEF
#pragma once
#include "plz.h"

class CPlzList
{
public:
	CString          ort;
	CString          zusatz;
	CString          plz;
	CString          vorwahl;
	CString          bundesland;
	CPlzList(void);
	~CPlzList(void);
	CPlzList& operator= (CPlzList& PlzList);
	CPlzList& operator= (PLZ_CLASS& Plz);
};
#endif
