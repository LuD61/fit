#ifndef _PLZ_DEF
#define _PLZ_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct PLZ {
   TCHAR          ort[37];
   TCHAR          zusatz[37];
   TCHAR          plz[21];
   TCHAR          vorwahl[21];
   TCHAR          bundesland[37];
};
extern struct PLZ plz, plz_null;

#line 8 "plz.rh"

class PLZ_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PLZ plz;  
               PLZ_CLASS () : DB_CLASS ()
               {
               }
};
#endif
