#include "stdafx.h"
#include "ChoiceLsk.h"
#include "DbUniCode.h"
#include "Process.h"
#include "StrFuncs.h"
#include "Token.h"
#include "DynDialog.h"
#include "DbQuery.h"
#include "DbTime.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

#ifdef _DEBUG
//#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif

int CChoiceLsk::Sort1 = -1;
int CChoiceLsk::Sort2 = -1;
int CChoiceLsk::Sort3 = -1;
int CChoiceLsk::Sort4 = -1;
int CChoiceLsk::Sort5 = -1;
int CChoiceLsk::Sort6 = -1;
int CChoiceLsk::Sort7 = -1;
int CChoiceLsk::Sort8 = -1;
int CChoiceLsk::Sort9 = -1;
int CChoiceLsk::Sort10 = -1;
int CChoiceLsk::Sort11 = -1;
int CChoiceLsk::Sort12 = -1;

CChoiceLsk::CChoiceLsk(CWnd* pParent) 
        : CChoiceX(pParent)
{
	ptcursor = -1;
	Where = "";
	Types = "";
	EnterMdn = TRUE;
	Status5 = TRUE;
	Bean.ArchiveName = _T("LskList.prp");
	KunFil = 0;
	NewKunFil = 0;
}

CChoiceLsk::~CChoiceLsk() 
{
	DestroyList ();
	if (ptcursor != -1) DbClass->sqlclose (ptcursor);
	ptcursor = -1;
}

void CChoiceLsk::DestroyList() 
{
	for (std::vector<CLskList *>::iterator pabl = LskList.begin (); pabl != LskList.end (); ++pabl)
	{
		CLskList *abl = *pabl;
		delete abl;
	}
    LskList.clear ();
}

void CChoiceLsk::TestKunFil (short KunFil)
{
	NewKunFil = KunFil;
	if (NewKunFil != this->KunFil)
	{
		this->KunFil = KunFil;
		PostMessage (WM_COMMAND, IDC_FILTER, 0l);
	}
}

void CChoiceLsk::FillList () 
{
    short  mdn;
    long  ls;
    long  auf;
    long  kun;
    short  fil;
	DATE_STRUCT lieferdat;
    TCHAR kun_krz1 [50];
    TCHAR kun_bran2 [50];
    TCHAR hinweis [50];
	short ls_stat;
	long inka_nr;
	long tou_nr;
	extern short sql_mode;
	short sql_s;
	CString Sql = _T("");

	sql_s = sql_mode;
	sql_mode = 2;
	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);

    int i = 0;

	KunFil = NewKunFil;
    SetWindowText (_T("Auswahl Lieferscheine"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Mandant"),           1,  75, LVCFMT_RIGHT);
    SetCol (_T("Lieferschein"),      2, 150, LVCFMT_RIGHT);
    SetCol (_T("Auftrag"),           3, 150, LVCFMT_RIGHT);
    SetCol (_T("Lieferdatum"),       4, 150, LVCFMT_RIGHT);
	if (KunFil == 0)
	{
		SetCol (_T("Kunden Nr"),         5, 150, LVCFMT_RIGHT);
	}
	else
	{
		SetCol (_T("Filial Nr"),         5, 150, LVCFMT_RIGHT);
	}
    SetCol (_T("Name"),              6, 250);
	if (KunFil == 0)
	{
		SetCol (_T("Branche"),           7, 150);
	}
    SetCol (_T("Status"),            8, 150);
    SetCol (_T("Hinweistext"),       9, 150);
    SetCol (_T("Tour"),             10, 150);
    SetCol (_T("Inkasso"),          11, 150);
	SortRow = 2;

	if (FirstRead && !HideFilter)
	{
		FirstRead = FALSE;
		Load ();
		PostMessage (WM_COMMAND, IDC_FILTER, 0l);
		return;
	}

	if (LskList.size () == 0)
	{
		DbClass->sqlout ((short *)&mdn,              SQLSHORT, 0);
		DbClass->sqlout ((long *)&ls,               SQLLONG, 0);
		DbClass->sqlout ((long *)&auf,              SQLLONG, 0);
		DbClass->sqlout ((DATE_STRUCT *)&lieferdat, SQLDATE, 0);
		if (KunFil == 0)
		{
			DbClass->sqlout ((long *)&kun,              SQLLONG, 0);
		}
		else
		{
			DbClass->sqlout ((short *)&fil,              SQLSHORT, 0);
		}
		DbClass->sqlout ((TCHAR *)kun_krz1,         SQLCHAR, 17);
		if (KunFil == 0)
		{
			DbClass->sqlout ((TCHAR *)kun_bran2,        SQLCHAR, 3);
		}
		DbClass->sqlout ((short *)&ls_stat,         SQLSHORT, 0);
		DbClass->sqlout ((TCHAR *)hinweis,          SQLCHAR, 49);
		DbClass->sqlout ((long *)&tou_nr,           SQLLONG, 0);
		DbClass->sqlout ((long *)&inka_nr,          SQLLONG, 0);
		if (KunFil == 0)
		{
			Sql  =     _T("select lsk.mdn, lsk.ls, lsk.auf, lsk.lieferdat, kun.kun, kun.kun_krz1, kun.kun_bran2, lsk.ls_stat, ")
					   _T("lsk.hinweis, lsk.tou_nr, kun.inka_nr ")						
					   _T("from lsk,kun where ")
					   _T("lsk.ls > 0 ") 
					   _T("and lsk.kun_fil = 0 ")
					   _T("and kun.mdn = lsk.mdn ")
					   _T("and lsk.kun = kun.kun ");
//					   _T("and lsk.ls in (select ls from lsp where lsp.mdn = lsk.mdn and lsp.ls = lsk.ls)") ;
		}
		else
		{
			Sql  =     _T("select lsk.mdn, lsk.ls, lsk.auf, lsk.lieferdat, fil.fil, lsk.kun_krz1, lsk.ls_stat, ")
					   _T("lsk.hinweis, lsk.tou_nr, lsk.inka_nr ")						
					   _T("from lsk,fil where ")
					   _T("lsk.ls > 0 ") 
					   _T("and lsk.kun_fil = 1 ")
					   _T("and fil.mdn = lsk.mdn ")
					   _T("and lsk.kun = fil.fil ")
					   _T("and lsk.ls in (select ls from lsp where lsp.mdn = lsk.mdn and lsp.ls = lsk.ls)") ;
		}
		Sql += " ";
		Sql += Where;
		if (QueryString != _T(""))
		{
			Sql += _T(" and ");
			Sql += QueryString;
		}
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		if (cursor == -1)
		{
			AfxMessageBox (_T("Die Eingabe f�r den Filter kann nicht bearbeitet werden"), MB_OK | MB_ICONERROR, 0);
			return;
		}
		strcpy (hinweis, _T(""));
		DbClass->sqlopen (cursor);
		DbClass->sqlcomm ("delete from ls"); 
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) kun_krz1;
			CDbUniCode::DbToUniCode (kun_krz1, pos);
			pos = (LPSTR) kun_bran2;
			CDbUniCode::DbToUniCode (kun_bran2, pos);
            if (KunFil != 0)
			{
				kun = fil;
			}
			CLskList *abl = new CLskList (mdn, ls, auf, kun, lieferdat, ls_stat, kun_krz1, kun_bran2);
			DbClass->sqlin ((short *)&mdn, SQLSHORT, 0);
			DbClass->sqlin ((long *)&ls, SQLLONG, 0);
			DbClass->sqlcomm ("insert into ls (mdn,ls) values (?,?)"); 
			pos = (LPSTR) hinweis;
			CDbUniCode::DbToUniCode (hinweis, pos);
			abl->SetHinweis (hinweis);
			abl->inka_nr = inka_nr;
			abl->tou_nr = tou_nr;
			LskList.push_back (abl);
// Hinweis mu� jedesmal initialisiert werden, da das Feld beim select und leerem
// Feldinhalt nicht �berschrieben wird !!?
			strcpy (hinweis, _T(""));
		}
		DbClass->sqlclose (cursor);
		Load ();
	}
	sql_mode = sql_s;

	for (std::vector<CLskList *>::iterator pabl = LskList.begin (); pabl != LskList.end (); ++pabl)
	{
		CLskList *abl = *pabl;
		CString Mdn;
		Mdn.Format (_T("%hd"), abl->mdn); 
		CString Ls;
		Ls.Format (_T("%ld"), abl->ls); 
		CString Auf;
		Auf.Format (_T("%ld"), abl->auf); 
		CString Kun;
		CString Lieferdat;
/*
		Lieferdat.Format (_T("%02hd.%02hd.%04hd"), abl->lieferdat.day,
												  abl->lieferdat.month,
											 	  abl->lieferdat.year);
*/
		Lieferdat = abl->lieferdat;
		Kun.Format (_T("%ld"), abl->kun); 
		_tcscpy (kun_krz1, abl->kun_krz1);
		_tcscpy (kun_bran2, abl->kun_bran2);
		CString Hinweis;
		Hinweis = abl->hinweis;
		Hinweis.TrimRight ();
		_tcscpy (hinweis, Hinweis.GetBuffer ());
		CString InkaNr;
		InkaNr.Format (_T("%ld"), abl->inka_nr);
		CString TouNr;
		TouNr.Format (_T("%ld"), abl->tou_nr);
		TCHAR LsStat [37] = {""};
		TCHAR ptwert [5];
		_stprintf (ptwert, _T("%hd"), abl->ls_stat);
        GetPtBez (_T("ls_stat"), ptwert, LsStat);

		int pos = 1;
        int ret = InsertItem (i, -1);
        ret = SetItemText (Mdn.GetBuffer (), i, pos ++);
        ret = SetItemText (Ls.GetBuffer (), i, pos ++);
        ret = SetItemText (Auf.GetBuffer (), i, pos ++);
        ret = SetItemText (Lieferdat.GetBuffer (), i, pos ++);
        ret = SetItemText (Kun.GetBuffer (), i, pos ++);
        ret = SetItemText (kun_krz1, i, pos ++);
		if (KunFil == 0)
		{
			ret = SetItemText (kun_bran2, i, pos ++);
		}
        ret = SetItemText (LsStat, i, pos ++);
        ret = SetItemText (hinweis, i, pos ++);
        ret = SetItemText (TouNr.GetBuffer (), i, pos ++);
        ret = SetItemText (InkaNr.GetBuffer (), i, pos ++);
        i ++;
    }

    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort6 = -1;
    Sort7 = -1;
    Sort8 = -1;
    Sort9 = -1;
    Sort10 = -1;
    Sort11 = -1;
    Sort12 = -1;
    Sort (listView);
	m_List.SetFocus ();
	if (LskList.size () > 0)
	{
		m_List.SetItemState (0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
	}
}

/*
void CChoiceLsk::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceLsk::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceLsk::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLsk::SearchABz1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLsk::SearchABz2 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}
*/

void CChoiceLsk::SearchCol (CListCtrl *ListBox, LPTSTR Search, int col)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, col);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLsk::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
	if ((EditText.Find (_T("*")) != -1) || EditText.Find (_T("?")) != -1)
	{
		CChoiceX::SearchMatch (EditText);
		return;
	}

    SearchCol (ListBox, EditText.GetBuffer (), SortRow);
}

int CChoiceLsk::GetPtBez (LPTSTR ptitem, LPTSTR ptwert, LPTSTR ptbez)
{
	if (ptcursor == -1)
	{
		DbClass->sqlout ((LPTSTR) this->ptbez,  SQLCHAR, sizeof (this->ptbez));
		DbClass->sqlout ((LPTSTR) this->ptbezk, SQLCHAR, sizeof (this->ptbezk));
		DbClass->sqlin ((LPTSTR) this->ptitem, SQLCHAR, sizeof (this->ptitem));
		DbClass->sqlin  ((LPTSTR) this->ptwert, SQLCHAR, sizeof (this->ptwert));
	    ptcursor = DbClass->sqlcursor (_T("select ptbez,ptbezk from ptabn where ptitem = ? ")
							           _T("and ptwert = ?"));
	}
	_tcscpy (this->ptbez, _T(""));
	_tcscpy (this->ptbezk, _T(""));
	_tcscpy (this->ptitem, ptitem);
	_tcscpy (this->ptwert, ptwert);
	DbClass->sqlopen (ptcursor);
	int dsqlstatus = DbClass->sqlfetch (ptcursor);

	_tcscpy (ptbez, this->ptbez);
	LPSTR pos = (LPSTR) ptbez;
    CDbUniCode::DbToUniCode (ptbez, pos);
	return dsqlstatus;
}

int CALLBACK CChoiceLsk::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 1)
   {

	   short li1 = _tstoi (strItem1.GetBuffer ());
	   short li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   if (SortRow == 2)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort3;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort3);
	   }
	   return 0;
   }
   if (SortRow == 3)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort4;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort4);
	   }
	   return 0;
   }
   if (SortRow == 4)
   {
	   DATE_STRUCT ld1;
	   DATE_STRUCT ld2;
	   DB_CLASS::ToDbDate (CString (strItem1.GetBuffer ()), &ld1);
	   DB_CLASS::ToDbDate (CString (strItem2.GetBuffer ()), &ld2);
	   DbTime dt1 (&ld1);
	   DbTime dt2 (&ld2);
	   time_t li1 = dt1.GetTime ();
	   time_t li2 = dt2.GetTime ();
	   if (li1 < li2)
	   {
		   return Sort5;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort5);
	   }
	   return 0;
   }
   if (SortRow == 5)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort6;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort6);
	   }
	   return 0;
   }
   else if (SortRow == 6)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort7;
   }
   else if (SortRow == 7)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort8;
   }
   else if (SortRow == 8)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort9;
   }
   else if (SortRow == 9)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort10;
   }
   else if (SortRow == 10)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort11;
   }
   else if (SortRow == 11)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort12;
   }
   return 0;
}

void CChoiceLsk::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
        case 4:
              Sort5 *= -1;
			  if (Sort5 < 0) SortPos = 1;
              break;
        case 5:
              Sort6 *= -1;
			  if (Sort6 < 0) SortPos = 1;
              break;
        case 6:
              Sort7 *= -1;
			  if (Sort7 < 0) SortPos = 1;
              break;
        case 7:
              Sort8 *= -1;
			  if (Sort8 < 0) SortPos = 1;
              break;
        case 8:
              Sort9 *= -1;
			  if (Sort9 < 0) SortPos = 1;
              break;
        case 9:
              Sort10 *= -1;
			  if (Sort10 < 0) SortPos = 1;
              break;
        case 10:
              Sort11 *= -1;
			  if (Sort11 < 0) SortPos = 1;
              break;
        case 11:
              Sort12 *= -1;
			  if (Sort12 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CLskList *abl = LskList [i];
		   
		   abl->mdn       = _tstoi (ListBox->GetItemText (i, 1));
		   abl->ls        = _tstol (ListBox->GetItemText (i, 2));
		   abl->auf       = _tstol (ListBox->GetItemText (i, 3));
		   abl->SetLieferdat (ListBox->GetItemText (i,4));
		   abl->kun       = _tstol (ListBox->GetItemText (i, 5));
		   abl->SetKunKrz1 (ListBox->GetItemText (i, 6));
		   abl->SetKunBran2 (ListBox->GetItemText (i, 7));
		   abl->ls_stat   = _tstoi (ListBox->GetItemText (i, 8));
		   abl->SetHinweis (ListBox->GetItemText (i, 9));
		   abl->tou_nr = _tstol (ListBox->GetItemText (i, 10));
		   abl->inka_nr = _tstol (ListBox->GetItemText (i, 11));
	}


	for (int i = 1; i <= 5; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);

}

void CChoiceLsk::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = LskList [idx];
}

CLskList *CChoiceLsk::GetSelectedText ()
{
	CLskList *abl = (CLskList *) SelectedRow;
	return abl;
}

CLskList *CChoiceLsk::GetNextSelectedText ()
{
	IncSelection ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
        return NULL;
    }
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	CLskList *abl = (CLskList *) SelectedRow;
	return abl;
}

CLskList *CChoiceLsk::GetFirstSelectedText ()
{
	FirstSelection ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
        return NULL;
    }
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	CLskList *abl = (CLskList *) SelectedRow;
	return abl;
}

CLskList *CChoiceLsk::GetLastSelectedText ()
{
	LastSelection ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
        return NULL;
    }
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	CLskList *abl = (CLskList *) SelectedRow;
	return abl;
}


CLskList *CChoiceLsk::GetPriorSelectedText ()
{
	DecSelection ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
        return NULL;
    }
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	CLskList *abl = (CLskList *) SelectedRow;
	return abl;
}

CLskList *CChoiceLsk::GetCurrentSelectedText ()
{
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
        return NULL;
    }
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	CLskList *abl = (CLskList *) SelectedRow;
	return abl;
}

void CChoiceLsk::DeleteChoiceLs (short mdn, short fil, long ls)
{
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	if (idx == -1)
	{
		idx = 0;
	}

	for (std::vector<CLskList *>::iterator pabl = LskList.begin () + idx; pabl != LskList.end (); ++pabl)
	{
		CLskList *abl = *pabl;
		if (abl->mdn == mdn &&
			abl->ls == ls)
		{
			ListBox->DeleteItem (idx);
			LskList.erase (pabl);
			delete abl;
			ListBox->SetItemState (idx, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
			return;
		}
		idx ++;
	}

	for (std::vector<CLskList *>::iterator pabl = LskList.begin (); pabl != LskList.begin () + idx - 1; ++pabl)
	{
		CLskList *abl = *pabl;
		if (abl->mdn == mdn &&
			abl->ls == ls)
		{
			ListBox->DeleteItem (idx);
			LskList.erase (pabl);
			delete abl;
			ListBox->SetItemState (idx, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
			return;
		}
	}
}

void CChoiceLsk::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (LskList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}

void CChoiceLsk::SetDefault ()
{
	CChoiceX::SetDefault ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    ListBox->SetColumnWidth (0, 0);
    ListBox->SetColumnWidth (1, 150);
    ListBox->SetColumnWidth (2, 150);
    ListBox->SetColumnWidth (3, 150);
    ListBox->SetColumnWidth (4, 250);
    ListBox->SetColumnWidth (5, 250);
}

void CChoiceLsk::OnF5 ()
{
	OnCancel ();
}

void CChoiceLsk::OnEnter ()
{
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cLs = m_List.GetItemText (idx, 1);  
		long ls = _tstol (cLs.GetBuffer ());
		Message.Format (_T("Lieferschein=%0ld"), ls);
		ToClipboard (Message);
	}
	p.SetCommand (_T("53100"));
	HANDLE Pid = p.Start ();
}

void CChoiceLsk::OnFilter ()
{
	CDynDialog dlg;
	CDynDialogItem *Item;

	dlg.DlgCaption = "Filter";
	dlg.UseFilter = UseFilter;

	if (vFilter.size () > 0)
	{
		for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
		{
			CDynDialogItem *i = *it;
			Item = new CDynDialogItem ();
			*Item = *i;
			if (Item->Name == _T("reset"))
			{
				if (UseFilter)
				{
					Item->Value = _T("aktiv");
				}
				else
				{
					Item->Value = _T("nicht aktiv");
				}
			}
			dlg.AddItem (Item);
		}
		dlg.Header.cy = Item->Template.y + dlg.RowSpace + 5;
	}
	else
	{
		int y = 10;

		if (EnterMdn)
		{
			Item = new CDynDialogItem ();
			Item->Name = _T("lmdn");
			Item->CtrClass = "static";
			Item->Value = _T("Mandant");
			Item->Template.x = 10;
			Item->Template.y = y;
			Item->Template.cx = 60;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1001;
			dlg.AddItem (Item);


			Item = new CDynDialogItem ();
			Item->Name = _T("lsk.mdn");
			Item->Type = Item->Short;
			Item->CtrClass = "edit";
			Item->Value = _T("");
			Item->Template.x = 70;
			Item->Template.y = y;
			Item->Template.cx = 120;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1002;
			dlg.AddItem (Item);


			y += dlg.RowSpace;
		}

		Item = new CDynDialogItem ();
		Item->Name = _T("lls");
		Item->CtrClass = "static";
		Item->Value = _T("Lieferschein");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1003;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("lsk.ls");
		Item->Type = Item->Long;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1004;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lauf");
		Item->CtrClass = "static";
		Item->Value = _T("Auftrag");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1005;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("lsk.auf");
		Item->Type = Item->Long;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1006;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("llieferdat");
		Item->CtrClass = "static";
		Item->Value = _T("Lieferdatum");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1007;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("lsk.lieferdat");
		Item->Type = Item->Date;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1008;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lkun");
		Item->CtrClass = "static";
		Item->Value = _T("Kunde");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1009;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("kun.kun");
		Item->Type = Item->Long;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1010;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lkun_krz1");
		Item->CtrClass = "static";
		Item->Value = _T("Name");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1011;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("kun.kun_krz1");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1012;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		if (KunFil == 0)
		{
			Item = new CDynDialogItem ();
			Item->Name = _T("lkun_bran2");
			Item->CtrClass = "static";
			Item->Value = _T("Branche");
			Item->Template.x = 10;
			Item->Template.y = y;
			Item->Template.cx = 60;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1013;
			dlg.AddItem (Item);

			Item = new CDynDialogItem ();
			Item->Name = _T("kun.kun_bran2");
			Item->CtrClass = "edit";
			Item->Value = _T("");
			Item->Template.x = 70;
			Item->Template.y = y;
			Item->Template.cx = 120;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1014;
			dlg.AddItem (Item);

			y += dlg.RowSpace;
		}

		Item = new CDynDialogItem ();
		Item->Name = _T("lhinweis");
		Item->CtrClass = "static";
		Item->Value = _T("Hinweistext");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1015;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("lsk.hinweis");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1016;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("ltou");
		Item->CtrClass = "static";
		Item->Value = _T("Tour");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1017;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("lsk.tou_nr");
		Item->Type = Item->Long;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1018;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		if (KunFil == 0)
		{
			Item = new CDynDialogItem ();
			Item->Name = _T("linka_nr");
			Item->CtrClass = "static";
			Item->Value = _T("Inkasso");
			Item->Template.x = 10;
			Item->Template.y = y;
			Item->Template.cx = 60;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1019;
			dlg.AddItem (Item);

			Item = new CDynDialogItem ();
			Item->Name = _T("kun.inka_nr");
			Item->Type = Item->Long;
			Item->CtrClass = "edit";
			Item->Value = _T("");
			Item->Template.x = 70;
			Item->Template.y = y;
			Item->Template.cx = 120;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1020;
			dlg.AddItem (Item);

			y += dlg.RowSpace;
		}

		Item = new CDynDialogItem ();
		Item->Name = _T("lls_stat");
		Item->CtrClass = "static";
		Item->Value = _T("Status");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1021;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("lsk.ls_stat");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		if (Status5 == TRUE)
		{
			Item->Value = _T("<5");
		}
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1022;
		dlg.AddItem (Item);

		dlg.Header.cy = y + 40;

		Item = new CDynDialogItem ();
		Item->Name = _T("OK");
		Item->CtrClass = "button";
		Item->Value = _T("OK");
		Item->Template.x = 15;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDOK;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("cancel");
		Item->CtrClass = "button";
		Item->Value = _T("abbrechen");
		Item->Template.x = 70;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDCANCEL;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("reset");
		Item->CtrClass = "button";
		if (UseFilter)
		{
			Item->Value = _T("aktiv");
		}
		else
		{
			Item->Value = _T("nicht aktiv");
		}
		Item->Template.x = 125;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = ID_USE;
		dlg.AddItem (Item);
	}

	dlg.CreateModal ();
	INT_PTR ret = dlg.DoModal ();
	if (ret != IDOK) return;
 
	UseFilter = dlg.UseFilter;
	int count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = dlg.Items.begin (); it != dlg.Items.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (count >= (int) vFilter.size ())
			{
				Item = new CDynDialogItem ();
				*Item = *i;
				vFilter.push_back (Item);
			}
			*vFilter[count] = *i;
			count ++;
	}
    CString Query;
    CDbQuery DbQuery;

	QueryString = _T("");
	if (!UseFilter)
	{
		FillList ();
		return;
	}
	count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (strcmp (i->CtrClass, "edit") != 0) continue; 
			if (i->Value.Trim () != _T(""))
			{
				if (i->Type == i->String || i->Type == i->Date)
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, TRUE,i->Type);
				}
				else
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, FALSE);
				}
				if (count > 0)
				{
					QueryString += _T(" and ");
				}
                QueryString += Query;
				count ++;
			}
	}
	FillList ();
}
