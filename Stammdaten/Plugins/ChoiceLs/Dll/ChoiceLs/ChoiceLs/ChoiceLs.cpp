// ChoiceLs.cpp : Definiert die Initialisierungsroutinen f�r die DLL.
//

#include "stdafx.h"
#include "ChoiceLs.h"
#include "ChoiceLsk.h"
#include "LskList.h"
#include "DbClass.h"
#include "ChoicePos.h"
#include "EnterPlz.h"
#include "PlzListS.h"
#include <vector>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: Wenn diese DLL dynamisch mit MFC-DLLs verkn�pft ist,
//		muss f�r alle aus dieser DLL exportierten Funktionen, die in
//		MFC aufgerufen werden, das AFX_MANAGE_STATE-Makro
//		am Anfang der Funktion hinzugef�gt werden.
//
//		Beispiel:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// Hier normaler Funktionsrumpf
//		}
//
//		Es ist sehr wichtig, dass dieses Makro in jeder Funktion
//		vor allen MFC-Aufrufen angezeigt wird. Dies bedeutet,
//		dass es als erste Anweisung innerhalb der 
//		Funktion angezeigt werden muss, sogar vor jeglichen Deklarationen von Objektvariablen,
//		da ihre Konstruktoren Aufrufe in die MFC-DLL generieren
//		k�nnten.
//
//		Siehe Technische Hinweise f�r MFC 33 und 58 f�r weitere
//		Details.
//


// CChoiceLsApp

DB_CLASS DbClass;

CEnterPlz EnterPlz;

CPlzList PlzList;

BEGIN_MESSAGE_MAP(CChoiceLsApp, CWinApp)
END_MESSAGE_MAP()


// CChoiceLsApp-Erstellung

CChoiceLsApp::CChoiceLsApp()
{
	// TODO: Hier Code zur Konstruktion einf�gen.
	// Alle wichtigen Initialisierungen in InitInstance positionieren.
}


// Das einzige CChoiceLsApp-Objekt

CChoiceLsApp theApp;


// CChoiceLsApp-Initialisierung

BOOL CChoiceLsApp::InitInstance()
{
	CWinApp::InitInstance();
    DbClass.opendbase ("bws");
	return TRUE;
}

CDataCollection<CLskList> LskArray;

// EXPORT CExtraData *LskChoice (CExtraData *ExtraData)
EXPORT CDataCollection<CLskList>* LskChoice ()
//EXPORT void LskChoice ()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
    CChoiceLsk *ChoiceLsk = new CChoiceLsk (NULL);
    ChoiceLsk->IsModal = TRUE;
    ChoiceLsk->HideFilter = FALSE;
	ChoiceLsk->SingleSelection = FALSE;
	ChoiceLsk->CreateDlg (600, 400);
	ChoiceLsk->DoModal ();

	LskArray.Clear ();

	for (std::vector<CLskList *>::iterator 
				pabl = ChoiceLsk->SelectList.begin (); 
				pabl != ChoiceLsk->SelectList.end (); ++pabl)
	{
		CLskList *abl = *pabl;
		CLskList LskList (abl->mdn, abl->ls, abl->auf, abl->kun, abl->lieferdat, 
						  abl->ls_stat, abl->kun_krz1, abl->kun_bran2);	
		LskArray.Add (LskList);
	}

	delete ChoiceLsk;
	return &LskArray;
}

EXPORT void LskChoiceEx (CDataCollection<CLskList> *LskArray)
//EXPORT void LskChoice ()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
    CChoiceLsk *ChoiceLsk = new CChoiceLsk (NULL);
    ChoiceLsk->IsModal = TRUE;
    ChoiceLsk->HideFilter = FALSE;
	ChoiceLsk->SingleSelection = FALSE;
	ChoiceLsk->CreateDlg (600, 400);
	ChoiceLsk->DoModal ();


	for (std::vector<CLskList *>::iterator 
				pabl = ChoiceLsk->SelectList.begin (); 
				pabl != ChoiceLsk->SelectList.end (); ++pabl)
	{
		CLskList *abl = *pabl;
		CLskList LskList (abl->mdn, abl->ls, abl->auf, abl->kun, abl->lieferdat, 
						  abl->ls_stat, abl->kun_krz1, abl->kun_bran2);	
// F�hrt zum Absturz, warum ??? 		LskArray->Add (LskList);

	}

	delete ChoiceLsk;
}


CChoiceLsk *ChoiceLsk = NULL;
short ChoiceMdn = -1;

EXPORT long LskChoiceLs (short mdn, short kun_fil,BOOL Anzeigen)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (ChoiceLsk != NULL && ChoiceMdn != mdn)
	{
		delete ChoiceLsk;
		ChoiceLsk = NULL;
	}
	if (ChoiceLsk != NULL && ChoiceLsk->GetKunFil () != kun_fil)
	{
		delete ChoiceLsk;
		ChoiceLsk = NULL;
	}

	ChoiceMdn = mdn;
	if (ChoiceLsk == NULL)
	{
		ChoiceLsk = new CChoiceLsk (NULL);
		ChoiceLsk->IsModal = FALSE;
		ChoiceLsk->HideFilter = FALSE;
		ChoiceLsk->SingleSelection = TRUE;
		ChoiceLsk->Where = "";
		if (Anzeigen == TRUE) ChoiceLsk->Status5 = FALSE;
		ChoiceLsk->NewKunFil = kun_fil;
		if (mdn != 0)
		{
			ChoiceLsk->EnterMdn = FALSE;
			ChoiceLsk->Where.Format ("and lsk.mdn = %hd", mdn); 
		}
		ChoiceLsk->CloseOnEnd = TRUE;
		ChoiceLsk->CreateDlg (600, 400);
	}

	ChoiceLsk->ShowWindow (SW_SHOWNORMAL);

	MSG msg;

	while (::GetMessage (&msg, NULL, 0, 0))
    {
		if (msg.message == WM_KEYDOWN)
		{
			if (msg.wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					ChoiceLsk->GetNextDlgTabItem (ChoiceLsk->GetFocus ())->SetFocus ();
				}
				else
				{
					ChoiceLsk->GetNextDlgTabItem (ChoiceLsk->GetFocus (), 1)->SetFocus ();
				}
			}
			else if (msg.wParam == VK_RETURN)
			{
				ChoiceLsk->OnOK ();
			}
			else if (msg.wParam == VK_F5)
			{
				ChoiceLsk->OnF5 ();
			}
			else if (msg.wParam == VK_F9)
			{
				ChoiceLsk->OnFilter ();

			}
		}
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

	ChoiceLsk->ShowWindow (SW_HIDE);
	if (ChoiceLsk->Result == CChoiceX::ResultCancel)
	{
		return 0l;
	}

	CLskList *abl = ChoiceLsk->GetSelectedText ();
    if (abl != NULL)
	{
		return abl->ls;
	}
	return 0l;
}

EXPORT long LskNextRow ()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (ChoiceLsk == NULL)
	{
		return 0l;
	}
	CLskList *abl = ChoiceLsk->GetNextSelectedText ();
    if (abl != NULL)
	{
		return abl->ls;
	}
	return 0l;
}

EXPORT long LskPriorRow ()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (ChoiceLsk == NULL)
	{
		return 0l;
	}
	CLskList *abl = ChoiceLsk->GetPriorSelectedText ();
    if (abl != NULL)
	{
		return abl->ls;
	}
	return 0l;
}

EXPORT long LskFirstRow ()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (ChoiceLsk == NULL)
	{
		return 0l;
	}
	CLskList *abl = ChoiceLsk->GetFirstSelectedText ();
    if (abl != NULL)
	{
		return abl->ls;
	}
	return 0l;
}

EXPORT long LskLastRow ()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (ChoiceLsk == NULL)
	{
		return 0l;
	}
	CLskList *abl = ChoiceLsk->GetLastSelectedText ();
    if (abl != NULL)
	{
		return abl->ls;
	}
	return 0l;
}

EXPORT long LskCurrentRow ()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (ChoiceLsk == NULL)
	{
		return 0l;
	}
	CLskList *abl = ChoiceLsk->GetCurrentSelectedText ();
    if (abl != NULL)
	{
		return abl->ls;
	}
	return 0l;
}

EXPORT void LskDeleteRow (short mdn, short fil, long ls)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (ChoiceLsk == NULL)
	{
		return;
	}
	ChoiceLsk->DeleteChoiceLs (mdn, fil, ls);
}

EXPORT void Destroy ()
{
	if (ChoiceLsk != NULL)
	{
		delete ChoiceLsk;
	}
}

EXPORT int GetPosMode (int PosMode)
{
	CChoicePos dlg;
	dlg.SetPosMode (PosMode);
	INT_PTR ret = dlg.DoModal ();
	if (ret != IDOK)
	{
		return dlg.PosDlgCancel;
	}
	return dlg.GetPosMode ();
}

extern "C" _declspec (dllexport) void GetPlzFromPlz (LPTSTR plz, SPlzListS *PlzListS)
{
	PLZ_CLASS Plz = EnterPlz.GetPlzFromPlz (plz);
	_tcscpy (PlzListS->plz, Plz.plz.plz);
	_tcscpy (PlzListS->ort, Plz.plz.ort);
	_tcscpy (PlzListS->vorwahl, Plz.plz.vorwahl);
	_tcscpy (PlzListS->bundesland, Plz.plz.bundesland);
}

EXPORT void GetPlzFromOrt (LPTSTR ort1, SPlzListS *PlzListS)
{
	PLZ_CLASS Plz = EnterPlz.GetPlzFromOrt (ort1);
	_tcscpy (PlzListS->plz, Plz.plz.plz);
	_tcscpy (PlzListS->ort, Plz.plz.ort);
	_tcscpy (PlzListS->vorwahl, Plz.plz.vorwahl);
	_tcscpy (PlzListS->bundesland, Plz.plz.bundesland);
}

extern "C" _declspec (dllexport) BOOL ChoicePlz (SPlzListS *PlzListS)
{
	PLZ_CLASS Plz;
	_tcscpy (Plz.plz.plz, PlzListS->plz);
	_tcscpy (Plz.plz.ort, PlzListS->ort);
	_tcscpy (Plz.plz.vorwahl, PlzListS->vorwahl);
	_tcscpy (Plz.plz.bundesland, PlzListS->bundesland);
	CPlzList *abl = EnterPlz.Choice (Plz);
    if (abl != NULL)
	{
		_tcscpy (PlzListS->plz, abl->plz.GetBuffer ());
		_tcscpy (PlzListS->ort, abl->ort.GetBuffer ());
		_tcscpy (PlzListS->vorwahl, abl->vorwahl.GetBuffer ());
		_tcscpy (PlzListS->bundesland, abl->bundesland.GetBuffer ());
		return TRUE;
	}
	return FALSE;
}

