#pragma once
#include "afxwin.h"
#include <vector>

#define DLG_ID_OFFS 1000
#define ID_USE 2001

class CDynDialogItem
{
public:
	enum
	{
		String = 0,
		Short = 1,
		Long = 2,
		Decimal = 3,
		Date = 4,
	};
	CString Name;
	int Type;
	DLGITEMTEMPLATE Template;
	LPSTR CtrClass;
	CString Value;
	CDynDialogItem ()
	{
		Type = String;
	}
	CDynDialogItem& operator= (CDynDialogItem& Item);
};

class CDynDialog :
	public CDialog
{
public:
	CDynDialog(void);
public:
	~CDynDialog(void);
protected:
	DECLARE_MESSAGE_MAP()
	BOOL PreTranslateMessage(MSG* pMsg);
    void *pDlgTemplate;
public:
    DLGTEMPLATE Header;
	std::vector<CDynDialogItem *> Items;
	int RowSpace;
	CSize CharSize;
	CWnd *Parent;
	DWORD DlgMenu;
	DWORD DlgClass;
    LPSTR DlgCaption;
	LPSTR FontName;
	WORD FontSize;
    BYTE *DlgItempos;
	BOOL UseFilter;
	void AddItem (CDynDialogItem *Item);
	void CreateItems ();
    WORD *AddHeader (WORD *, LPSTR);
    WORD *AddFont (WORD *, WORD, LPSTR);
    BOOL AddItem (DLGITEMTEMPLATE *, LPCSTR, CString&);
	BOOL Create ();
	BOOL CreateModal ();
	virtual void OnOK ();
	afx_msg void OnReset ();
	static void CreateDlgTemplate (DLGITEMTEMPLATE *Template, int X, int Y, int Height, int Length, CSize CharSize);
};
