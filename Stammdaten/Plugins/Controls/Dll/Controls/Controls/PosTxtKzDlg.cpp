// PosTxtKzDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Controls.h"
#include "PosTxtKzDlg.h"


// CPosTxtKzDlg-Dialogfeld

IMPLEMENT_DYNAMIC(CPosTxtKzDlg, CDialog)

CPosTxtKzDlg::CPosTxtKzDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPosTxtKzDlg::IDD, pParent)
{
		PosTxtMode = KommOnly;
		FromLs = FALSE;
}

CPosTxtKzDlg::~CPosTxtKzDlg()
{
}

void CPosTxtKzDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_KOMM_ONLY, m_KommOnly);
	DDX_Control(pDX, IDC_LIEF_VISIBLE, m_LiefVisible);
	DDX_Control(pDX, IDC_LIEF_RECH_VISIBLE, m_LiefRechVisible);
}


BEGIN_MESSAGE_MAP(CPosTxtKzDlg, CDialog)
END_MESSAGE_MAP()


// CPosTxtKzDlg-Meldungshandler

BOOL CPosTxtKzDlg::OnInitDialog()
{
	CDialog::OnInitDialog ();
	if (FromLs)
	{
		if (PosTxtMode == KommOnly)
		{
			PosTxtMode = LiefVisible;
			m_KommOnly.EnableWindow (FALSE);
		}
	}
	if (PosTxtMode == KommOnly)
	{
		m_KommOnly.SetCheck (MF_CHECKED);
	}
	else if (PosTxtMode == LiefVisible)
	{
		m_LiefVisible.SetCheck (MF_CHECKED);
	}
	else if (PosTxtMode == LiefRechVisible)
	{
		m_LiefRechVisible.SetCheck (MF_CHECKED);
	}
	return TRUE;
}

void CPosTxtKzDlg::OnOK ()
{
	if (m_KommOnly.GetCheck () == BST_CHECKED)
	{
		PosTxtMode = KommOnly;
	}
	else if (m_LiefVisible.GetCheck () == BST_CHECKED)
	{
		PosTxtMode = LiefVisible;
	}
	else if (m_LiefRechVisible.GetCheck () == BST_CHECKED)
	{
		PosTxtMode = LiefRechVisible;
	}
	CDialog::OnOK ();
}
