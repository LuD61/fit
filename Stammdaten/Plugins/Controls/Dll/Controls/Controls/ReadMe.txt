========================================================================
    MICROSOFT FOUNDATION CLASS-BIBLIOTHEK: Controls-Projekt�bersicht
========================================================================


Diese Controls-DLL wurde vom Anwendungs-Assistenten f�r Sie erstellt. Diese DLL zeigt nicht nur die Grundlagen der Verwendung von
Microsoft Foundation Classes, sondern dient auch als Ausgangspunkt f�r das
Schreiben Ihrer DLL.

Diese Datei enth�lt eine Zusammenfassung dessen, was sich in den Dateien befindet, aus denen Ihre Controls�DLL besteht.

Controls.vcproj
    Dies ist die Hauptprojektdatei f�r VC++-Projekte, die mit dem Anwendungs-
    Assistenten generiert werden. 
    Sie enth�lt Informationen �ber die Version von Visual C++, in der die Datei 
    erzeugt wurde, sowie �ber die Plattformen, Konfigurationen und 
    Projektfunktionen, die im Anwendungs-Assistenten ausgew�hlt wurden.

Controls.h
    Dies ist die Hauptheaderdatei f�r die DLL. Sie deklariert die
    CControlsApp-Klasse.

Controls.cpp
    Dies ist die Hauptquelldatei der DLL. Sie enth�lt die CControlsApp-
    Klasse.

Controls.rc
    Dies ist eine Auflistung aller Microsoft Windows-Ressourcen, die das
    Programm verwendet. Sie enth�lt die Symbole, Bitmaps und Cursor, die im 
    Unterverzeichnis "RES" gespeichert werden. Diese Datei kann direkt in 
    Microsoft Visual C++ bearbeitet werden.

res\Controls.rc2
    Diese Datei enth�lt Ressourcen, die nicht von Microsoft Visual C++
    bearbeitet werden. Sie sollten alle Ressourcen, die nicht mit dem
    Ressourcen-Editor bearbeitet werden k�nnen, in dieser Datei platzieren.

Controls.def
    Diese Datei enth�lt Informationen �ber die DLL, die f�r die 
    Ausf�hrung mit Microsoft Windows ben�tigt werden. Sie definiert Parameter 
    wie den Namen und die Beschreibung der DLL. Au�erdem exportiert sie 
    Funktionen aus der DLL.

/////////////////////////////////////////////////////////////////////////////
Weitere Standarddateien:

StdAfx.h, StdAfx.cpp
    Diese Dateien werden verwendet, um eine vorkompilierte Headerdatei
    (PCH-Datei) mit dem Namen "Controls.pch" und eine 
    vorkompilierte Typendatei mit dem Namen "StdAfx.obj" zu erstellen.

Resource.h
    Dies ist die Standardheaderdatei, die neue Ressourcen-IDs definiert.
    Microsoft Visual C++ liest und aktualisiert diese Datei.

/////////////////////////////////////////////////////////////////////////////
Weitere Hinweise:

Der Anwendungs-Assistent verwendet "TODO:", um auf Teile des Quellcodes
hinzuweisen, die Sie erg�nzen oder anpassen sollten.

/////////////////////////////////////////////////////////////////////////////