#pragma once


// CPosTxtKzDlg-Dialogfeld

class CPosTxtKzDlg : public CDialog
{
	DECLARE_DYNAMIC(CPosTxtKzDlg)

public:
	enum
	{
		KommOnly = 0,
		LiefVisible = 1,
		LiefRechVisible = 2,
	};
	CPosTxtKzDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CPosTxtKzDlg();

// Dialogfelddaten
	enum { IDD = IDD_POSTXT_KZ };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual void OnOK ();

	DECLARE_MESSAGE_MAP()

	CButton m_KommOnly;
	CButton m_LiefVisible;
	CButton m_LiefRechVisible;
public:
	int  PosTxtMode;
	BOOL FromLs;
};
