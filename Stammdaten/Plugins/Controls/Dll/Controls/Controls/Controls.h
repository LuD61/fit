// Controls.h : Hauptheaderdatei f�r die Controls-DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "'stdafx.h' vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"		// Hauptsymbole
#include "PosTxtKzDlg.h"


// CControlsApp
// Siehe Controls.cpp f�r die Implementierung dieser Klasse
//

#undef EXPORT
#define EXPORT extern "C" _declspec (dllexport)

class CControlsApp : public CWinApp
{
public:
	CControlsApp();

// �berschreibungen
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};

EXPORT int GetPosTxtKz (int PosTxtKz);
EXPORT int GetPosTxtKzEx (int PosTxtKz, BOOL FromLs);

