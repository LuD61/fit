#include "stdafx.h"
#include "DbClass.h"
#include "ptabn.h"

struct PTABN ptabn,  ptabn_null;

extern DB_CLASS dbClass;


int PTABN_CLASS::leseptabnw (							)
{

      int di = dbClass.sqlfetch (readcursor);
	  return di;
}

int PTABN_CLASS::openptabnw (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (readcursor);
}

int PTABN_CLASS::leseptabna (							)
{

      int di = dbClass.sqlfetch (test_upd_cursor);

	  return di;
}


int PTABN_CLASS::openptabna (void)
{

		if ( readcursor < 0 ) prepare ();
		
         return dbClass.sqlopen (test_upd_cursor);
}

void PTABN_CLASS::prepare (void)
{



// read_cursor 
	dbClass.sqlin ((char *) ptabn.ptwert, SQLCHAR, 4);
	dbClass.sqlin ((char *) ptabn.ptitem, SQLCHAR, 19);


	dbClass.sqlout ((long *) &ptabn.ptlfnr, SQLLONG, 0);
	dbClass.sqlout ((char *) ptabn.ptbez,  SQLCHAR,33);
	dbClass.sqlout ((char *) ptabn.ptbezk, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer1, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer2, SQLCHAR, 9);
	
			
	readcursor = (short) dbClass.sqlcursor ("select "

	" ptlfnr, ptbez, ptbezk, ptwer1, ptwer2  "

	" from ptabn where ptwert = ? and ptitem = ? " ) ;
	
// test_upd_cursor 
	dbClass.sqlin ((char *) ptabn.ptitem, SQLCHAR, 19);


	dbClass.sqlout ((char *) ptabn.ptwert, SQLCHAR, 4);
	dbClass.sqlout ((long *) &ptabn.ptlfnr, SQLLONG, 0);
	dbClass.sqlout ((char *) ptabn.ptbez,  SQLCHAR,33);
	dbClass.sqlout ((char *) ptabn.ptbezk, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer1, SQLCHAR, 9);
	dbClass.sqlout ((char *) ptabn.ptwer2, SQLCHAR, 9);
	
			
	test_upd_cursor = (short) dbClass.sqlcursor ("select "

	" ptwert, ptlfnr, ptbez, ptbezk, ptwer1, ptwer2  "

	" from ptabn where ptitem = ?  order by ptlfnr " ) ;
  }
