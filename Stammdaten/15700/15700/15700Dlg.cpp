// wofaktorDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "DbClass.h"
#include "15700.h"
#include "15700Dlg.h"
#include "dbfil_abrech.h"
#include "mdn.h"
#include "fil.h"
#include "adr.h"
#include "ptabn.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern DB_CLASS dbClass;
WOFAKTOR_CLASS Wofaktor;
FIL_ABRECH_CLASS Fil_abrech;
MDN_CLASS Mdn;
FIL_CLASS Fil;
ADR_CLASS Adr;
PTABN_CLASS Ptabn;


char bufh[599];
short wochenzahl;

// CAboutDlg-Dialogfeld f�r Anwendungsbefehl "Info"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// C15700Dlg-Dialogfeld


C15700Dlg::C15700Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(C15700Dlg::IDD, pParent)
	, v_jahr(0)
	, v_mdnnr(_T(""))
	, v_filnr(_T(""))
	, v_mdnname(_T(""))
	, v_filname(_T(""))
	, v_combo1(_T(""))
	, v_raddat(FALSE)
	, v_radproz(FALSE)
	, v_radeuro(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void C15700Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_Jahr, m_jahr);
	DDX_Text(pDX, IDC_Jahr, v_jahr);
	DDV_MinMaxLong(pDX, v_jahr, 2010, 2039);
	DDX_Control(pDX, IDC_EDIT1, m_wo1);
	DDX_Text(pDX, IDC_EDIT1, v_wo1);
	DDX_Control(pDX, IDC_EDIT2, m_wo2);
	DDX_Text(pDX, IDC_EDIT2, v_wo2);
	DDX_Control(pDX, IDC_EDIT3, m_wo3);
	DDX_Text(pDX, IDC_EDIT3, v_wo3);
	DDX_Control(pDX, IDC_EDIT4, m_wo4);
	DDX_Text(pDX, IDC_EDIT4, v_wo4);
	DDX_Control(pDX, IDC_EDIT5, m_wo5);
	DDX_Text(pDX, IDC_EDIT5, v_wo5);
	DDX_Control(pDX, IDC_EDIT6, m_wo6);
	DDX_Text(pDX, IDC_EDIT6, v_wo6);
	DDX_Control(pDX, IDC_EDIT7, m_wo7);
	DDX_Text(pDX, IDC_EDIT7, v_wo7);
	DDX_Control(pDX, IDC_EDIT8, m_wo8);
	DDX_Text(pDX, IDC_EDIT8, v_wo8);
	DDX_Control(pDX, IDC_EDIT9, m_wo9);
	DDX_Text(pDX, IDC_EDIT9, v_wo9);
	DDX_Control(pDX, IDC_EDIT10, m_wo10);
	DDX_Text(pDX, IDC_EDIT10, v_wo10);
	DDX_Control(pDX, IDC_EDIT11, m_wo11);
	DDX_Text(pDX, IDC_EDIT11, v_wo11);
	DDX_Control(pDX, IDC_EDIT12, m_wo12);
	DDX_Text(pDX, IDC_EDIT12, v_wo12);
	DDX_Control(pDX, IDC_EDIT13, m_wo13);
	DDX_Text(pDX, IDC_EDIT13, v_wo13);
	DDX_Control(pDX, IDC_EDIT14, m_wo14);
	DDX_Text(pDX, IDC_EDIT14, v_wo14);
	DDX_Control(pDX, IDC_EDIT15, m_wo15);
	DDX_Text(pDX, IDC_EDIT15, v_wo15);
	DDX_Control(pDX, IDC_EDIT16, m_wo16);
	DDX_Text(pDX, IDC_EDIT16, v_wo16);
	DDX_Control(pDX, IDC_EDIT17, m_wo17);
	DDX_Text(pDX, IDC_EDIT17, v_wo17);
	DDX_Control(pDX, IDC_EDIT18, m_wo18);
	DDX_Text(pDX, IDC_EDIT18, v_wo18);
	DDX_Control(pDX, IDC_EDIT19, m_wo19);
	DDX_Text(pDX, IDC_EDIT19, v_wo19);
	DDX_Control(pDX, IDC_EDIT20, m_wo20);
	DDX_Text(pDX, IDC_EDIT20, v_wo20);
	DDX_Control(pDX, IDC_EDIT21, m_wo21);
	DDX_Text(pDX, IDC_EDIT21, v_wo21);
	DDX_Control(pDX, IDC_EDIT22, m_wo22);
	DDX_Text(pDX, IDC_EDIT22, v_wo22);
	DDX_Control(pDX, IDC_EDIT23, m_wo23);
	DDX_Text(pDX, IDC_EDIT23, v_wo23);
	DDX_Control(pDX, IDC_EDIT24, m_wo24);
	DDX_Text(pDX, IDC_EDIT24, v_wo24);
	DDX_Control(pDX, IDC_EDIT25, m_wo25);
	DDX_Text(pDX, IDC_EDIT25, v_wo25);
	DDX_Control(pDX, IDC_EDIT26, m_wo26);
	DDX_Text(pDX, IDC_EDIT26, v_wo26);
	DDX_Control(pDX, IDC_EDIT27, m_wo27);
	DDX_Text(pDX, IDC_EDIT27, v_wo27);
	DDX_Control(pDX, IDC_EDIT28, m_wo28);
	DDX_Text(pDX, IDC_EDIT28, v_wo28);
	DDX_Control(pDX, IDC_EDIT29, m_wo29);
	DDX_Text(pDX, IDC_EDIT29, v_wo29);
	DDX_Control(pDX, IDC_EDIT30, m_wo30);
	DDX_Text(pDX, IDC_EDIT30, v_wo30);
	DDX_Control(pDX, IDC_EDIT31, m_wo31);
	DDX_Text(pDX, IDC_EDIT31, v_wo31);
	DDX_Control(pDX, IDC_EDIT32, m_wo32);
	DDX_Text(pDX, IDC_EDIT32, v_wo32);
	DDX_Control(pDX, IDC_EDIT33, m_wo33);
	DDX_Text(pDX, IDC_EDIT33, v_wo33);
	DDX_Control(pDX, IDC_EDIT34, m_wo34);
	DDX_Text(pDX, IDC_EDIT34, v_wo34);
	DDX_Control(pDX, IDC_EDIT35, m_wo35);
	DDX_Text(pDX, IDC_EDIT35, v_wo35);
	DDX_Control(pDX, IDC_EDIT36, m_wo36);
	DDX_Text(pDX, IDC_EDIT36, v_wo36);
	DDX_Control(pDX, IDC_EDIT37, m_wo37);
	DDX_Text(pDX, IDC_EDIT37, v_wo37);
	DDX_Control(pDX, IDC_EDIT38, m_wo38);
	DDX_Text(pDX, IDC_EDIT38, v_wo38);
	DDX_Control(pDX, IDC_EDIT39, m_wo39);
	DDX_Text(pDX, IDC_EDIT39, v_wo39);
	DDX_Control(pDX, IDC_EDIT40, m_wo40);
	DDX_Text(pDX, IDC_EDIT40, v_wo40);
	DDX_Control(pDX, IDC_EDIT41, m_wo41);
	DDX_Text(pDX, IDC_EDIT41, v_wo41);
	DDX_Control(pDX, IDC_EDIT42, m_wo42);
	DDX_Text(pDX, IDC_EDIT42, v_wo42);
	DDX_Control(pDX, IDC_EDIT43, m_wo43);
	DDX_Text(pDX, IDC_EDIT43, v_wo43);
	DDX_Control(pDX, IDC_EDIT44, m_wo44);
	DDX_Text(pDX, IDC_EDIT44, v_wo44);
	DDX_Control(pDX, IDC_EDIT45, m_wo45);
	DDX_Text(pDX, IDC_EDIT45, v_wo45);
	DDX_Control(pDX, IDC_EDIT46, m_wo46);
	DDX_Text(pDX, IDC_EDIT46, v_wo46);
	DDX_Control(pDX, IDC_EDIT47, m_wo47);
	DDX_Text(pDX, IDC_EDIT47, v_wo47);
	DDX_Control(pDX, IDC_EDIT48, m_wo48);
	DDX_Text(pDX, IDC_EDIT48, v_wo48);
	DDX_Control(pDX, IDC_EDIT49, m_wo49);
	DDX_Text(pDX, IDC_EDIT49, v_wo49);
	DDX_Control(pDX, IDC_EDIT50, m_wo50);
	DDX_Text(pDX, IDC_EDIT50, v_wo50);
	DDX_Control(pDX, IDC_EDIT51, m_wo51);
	DDX_Text(pDX, IDC_EDIT51, v_wo51);
	DDX_Control(pDX, IDC_EDIT52, m_wo52);
	DDX_Text(pDX, IDC_EDIT52, v_wo52);
	DDX_Control(pDX, IDC_EDIT53, m_wo53);
	DDX_Text(pDX, IDC_EDIT53, v_wo53);
	DDX_Control(pDX, IDC_MDNNR, m_mdnnr);
	DDX_Text(pDX, IDC_MDNNR, v_mdnnr);
	DDX_Control(pDX, IDC_FILNR, m_filnr);
	DDX_Text(pDX, IDC_FILNR, v_filnr);
	DDX_Control(pDX, IDC_MNDNAME, m_mdnname);
	DDX_Text(pDX, IDC_MNDNAME, v_mdnname);
	DDX_Text(pDX, IDC_FILNAME, v_filname);
	DDX_Control(pDX, IDC_COMBO1, m_combo1);
	DDX_CBString(pDX, IDC_COMBO1, v_combo1);

	DDX_Check(pDX, IDC_RADDAT, v_raddat);
	DDX_Check(pDX, IDC_RADPROZ, v_radproz);
	DDX_Check(pDX, IDC_RADEURO, v_radeuro);
}

BEGIN_MESSAGE_MAP(C15700Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_EN_KILLFOCUS(IDC_EDIT1, &C15700Dlg::OnEnKillfocusEdit1)
	ON_EN_KILLFOCUS(IDC_EDIT2, &C15700Dlg::OnEnKillfocusEdit2)
	ON_EN_KILLFOCUS(IDC_EDIT3, &C15700Dlg::OnEnKillfocusEdit3)
	ON_EN_KILLFOCUS(IDC_EDIT4, &C15700Dlg::OnEnKillfocusEdit4)
	ON_EN_KILLFOCUS(IDC_EDIT5, &C15700Dlg::OnEnKillfocusEdit5)
	ON_EN_KILLFOCUS(IDC_EDIT6, &C15700Dlg::OnEnKillfocusEdit6)
	ON_EN_KILLFOCUS(IDC_EDIT7, &C15700Dlg::OnEnKillfocusEdit7)
	ON_EN_KILLFOCUS(IDC_EDIT8, &C15700Dlg::OnEnKillfocusEdit8)
	ON_EN_KILLFOCUS(IDC_EDIT9, &C15700Dlg::OnEnKillfocusEdit9)
	ON_EN_KILLFOCUS(IDC_EDIT10, &C15700Dlg::OnEnKillfocusEdit10)
	ON_EN_KILLFOCUS(IDC_EDIT11, &C15700Dlg::OnEnKillfocusEdit11)
	ON_EN_KILLFOCUS(IDC_EDIT12, &C15700Dlg::OnEnKillfocusEdit12)
	ON_EN_KILLFOCUS(IDC_EDIT13, &C15700Dlg::OnEnKillfocusEdit13)
	ON_EN_KILLFOCUS(IDC_EDIT14, &C15700Dlg::OnEnKillfocusEdit14)
	ON_EN_KILLFOCUS(IDC_EDIT15, &C15700Dlg::OnEnKillfocusEdit15)
	ON_EN_KILLFOCUS(IDC_EDIT16, &C15700Dlg::OnEnKillfocusEdit16)
	ON_EN_KILLFOCUS(IDC_EDIT17, &C15700Dlg::OnEnKillfocusEdit17)
	ON_EN_KILLFOCUS(IDC_EDIT18, &C15700Dlg::OnEnKillfocusEdit18)
	ON_EN_KILLFOCUS(IDC_EDIT19, &C15700Dlg::OnEnKillfocusEdit19)
	ON_EN_KILLFOCUS(IDC_EDIT20, &C15700Dlg::OnEnKillfocusEdit20)
	ON_EN_KILLFOCUS(IDC_EDIT21, &C15700Dlg::OnEnKillfocusEdit21)
	ON_EN_KILLFOCUS(IDC_EDIT22, &C15700Dlg::OnEnKillfocusEdit22)
	ON_EN_KILLFOCUS(IDC_EDIT23, &C15700Dlg::OnEnKillfocusEdit23)
	ON_EN_KILLFOCUS(IDC_EDIT24, &C15700Dlg::OnEnKillfocusEdit24)
	ON_EN_KILLFOCUS(IDC_EDIT25, &C15700Dlg::OnEnKillfocusEdit25)
	ON_EN_KILLFOCUS(IDC_EDIT26, &C15700Dlg::OnEnKillfocusEdit26)
	ON_EN_KILLFOCUS(IDC_EDIT27, &C15700Dlg::OnEnKillfocusEdit27)
	ON_EN_KILLFOCUS(IDC_EDIT28, &C15700Dlg::OnEnKillfocusEdit28)
	ON_EN_KILLFOCUS(IDC_EDIT29, &C15700Dlg::OnEnKillfocusEdit29)
	ON_EN_KILLFOCUS(IDC_EDIT30, &C15700Dlg::OnEnKillfocusEdit30)
	ON_EN_KILLFOCUS(IDC_EDIT31, &C15700Dlg::OnEnKillfocusEdit31)
	ON_EN_KILLFOCUS(IDC_EDIT32, &C15700Dlg::OnEnKillfocusEdit32)
	ON_EN_KILLFOCUS(IDC_EDIT33, &C15700Dlg::OnEnKillfocusEdit33)
	ON_EN_KILLFOCUS(IDC_EDIT34, &C15700Dlg::OnEnKillfocusEdit34)
	ON_EN_KILLFOCUS(IDC_EDIT35, &C15700Dlg::OnEnKillfocusEdit35)
	ON_EN_KILLFOCUS(IDC_EDIT36, &C15700Dlg::OnEnKillfocusEdit36)
	ON_EN_KILLFOCUS(IDC_EDIT37, &C15700Dlg::OnEnKillfocusEdit37)
	ON_EN_KILLFOCUS(IDC_EDIT38, &C15700Dlg::OnEnKillfocusEdit38)
	ON_EN_KILLFOCUS(IDC_EDIT39, &C15700Dlg::OnEnKillfocusEdit39)
	ON_EN_KILLFOCUS(IDC_EDIT40, &C15700Dlg::OnEnKillfocusEdit40)
	ON_EN_KILLFOCUS(IDC_EDIT41, &C15700Dlg::OnEnKillfocusEdit41)
	ON_EN_KILLFOCUS(IDC_EDIT42, &C15700Dlg::OnEnKillfocusEdit42)
	ON_EN_KILLFOCUS(IDC_EDIT43, &C15700Dlg::OnEnKillfocusEdit43)
	ON_EN_KILLFOCUS(IDC_EDIT44, &C15700Dlg::OnEnKillfocusEdit44)
	ON_EN_KILLFOCUS(IDC_EDIT45, &C15700Dlg::OnEnKillfocusEdit45)
	ON_EN_KILLFOCUS(IDC_EDIT46, &C15700Dlg::OnEnKillfocusEdit46)
	ON_EN_KILLFOCUS(IDC_EDIT47, &C15700Dlg::OnEnKillfocusEdit47)
	ON_EN_KILLFOCUS(IDC_EDIT48, &C15700Dlg::OnEnKillfocusEdit48)
	ON_EN_KILLFOCUS(IDC_EDIT49, &C15700Dlg::OnEnKillfocusEdit49)
	ON_EN_KILLFOCUS(IDC_EDIT50, &C15700Dlg::OnEnKillfocusEdit50)
	ON_EN_KILLFOCUS(IDC_EDIT51, &C15700Dlg::OnEnKillfocusEdit51)
	ON_EN_KILLFOCUS(IDC_EDIT52, &C15700Dlg::OnEnKillfocusEdit52)
	ON_EN_KILLFOCUS(IDC_EDIT53, &C15700Dlg::OnEnKillfocusEdit53)


	ON_BN_CLICKED(IDC_BUTTup, &C15700Dlg::OnBnClickedButtup)
	ON_BN_CLICKED(IDC_BUTTdown, &C15700Dlg::OnBnClickedButtdown)
	ON_BN_CLICKED(IDCANCEL, &C15700Dlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &C15700Dlg::OnBnClickedOk)
	ON_EN_KILLFOCUS(IDC_MDNNR, &C15700Dlg::OnEnKillfocusMdnnr)
	ON_EN_KILLFOCUS(IDC_FILNR, &C15700Dlg::OnEnKillfocusFilnr)
	ON_CBN_KILLFOCUS(IDC_COMBO1, &C15700Dlg::OnCbnKillfocusCombo1)
	ON_BN_CLICKED(IDC_RADDAT, &C15700Dlg::OnBnClickedRaddat)
	ON_BN_CLICKED(IDC_RADPROZ, &C15700Dlg::OnBnClickedRadproz)
	ON_BN_CLICKED(IDC_RADEURO, &C15700Dlg::OnBnClickedRadeuro)
END_MESSAGE_MAP()


// C15700Dlg-Meldungshandler

BOOL C15700Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	mdn.mdn = 1;
	fil.mdn = 1;
	fil.fil = 0;
	v_mdnnr.Format("1");
	v_filnr.Format("0");
	v_filname.Format("              ");
	

	int i = lesemdn();

	CString szperiod ;

	sprintf ( ptabn.ptitem,"abr_period"); 
	i = Ptabn.openptabna ();
	i = Ptabn.leseptabna();
	while(!i)
	{
	
			szperiod.Format("%s  %s",ptabn.ptwert ,ptabn.ptbez);
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->AddString(szperiod.GetBuffer(0));
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(0);
	
			i = Ptabn.leseptabna () ;
	}


	v_combo1.Format("M  Monat");
	dmonat = 1;

	v_raddat = TRUE;
	v_radproz = FALSE;
	v_radeuro = FALSE;
	SetWindowText("15700-Filial-Perioden-End-Datum");

	nureinmalfragen=0;

	time_t timer;
	struct tm *ltime;
	time (&timer);
 	ltime = localtime (&timer);
//	sprintf ( bufh , "%02d.%02d.%04d", ltime->tm_mday ,ltime->tm_mon + 1 ,*/ ltime->tm_year + 1900 );

	v_jahr = ltime->tm_year + 1900 ;

	fil_abrech.jr = (short) v_jahr;
	fil_abrech.mdn = mdn.mdn;
	fil_abrech.fil = fil.fil;
	sprintf ( fil_abrech.period_kz,"M");

	setzeperiode("M");

	int ergeb = satzlesen();

	UpdateData(FALSE);

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void C15700Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void C15700Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR C15700Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
char savest[50];

BOOL datumcheck(char * bufi )
{
	int i = (int) strlen(bufi) ;
	int hil1, hil2 ;
	BOOL j = FALSE ;

	char hil5 [5] ;
	if ( i != 6 && i != 8 && i != 10 )
	{
		bufi[0] = '\0' ;
		return TRUE ;
	}
	if ( i == 6 )	// nur ddmmyy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[5] ;
		bufi[ 8] = bufi[4] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 4] = bufi[3] ;
		bufi[ 3] = bufi[2] ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ; 
	}
	if ( i == 8 )	// nur dd,mm,yy erlaubt 
	{
		bufi[10] = '\0' ;
		bufi[ 9] = bufi[7] ;
		bufi[ 8] = bufi[6] ;
		if ( bufi[ 8] > '5' )
		{
			bufi[ 6] = '1' ;
			bufi[ 7] = '9' ;
		}
		else
		{
			bufi[ 6] = '2' ;
			bufi[ 7] = '0' ;
		}
		bufi[ 5] = '.' ;
		bufi[ 2] = '.' ;
		i = 10 ;
		j = TRUE ;
	}
	if ( i == 10 )	// hier kommt nix anderes mehr an ....  
	{
		if (   bufi[0] > '9' || bufi[0] < '0' 
			|| bufi[1] > '9' || bufi[1] < '0'
			|| bufi[3] > '9' || bufi[3] < '0'
			|| bufi[4] > '9' || bufi[4] < '0'
			|| bufi[6] > '9' || bufi[6] < '0'
			|| bufi[7] > '9' || bufi[7] < '0'
			|| bufi[8] > '9' || bufi[8] < '0'
			|| bufi[9] > '9' || bufi[9] < '0'
			)
		{

			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[3] ;
		hil5[1] = bufi[4] ;
		hil5[2] = '\0' ;
		hil1 = atoi (hil5);
		if ( hil1 > 12 || hil1 < 1 )
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}

		hil5[0] = bufi[0] ;
		hil5[1] = bufi[1] ;
		hil5[2] = '\0' ;
		hil2 = atoi (hil5);
		if ( hil2 > 31 || hil2 < 1 )		// 260110 hil1->hil2
		{
			bufi[0] = '\0' ;
			return TRUE ;
		}
		switch ( hil1 )
		{
		case  4 :
		case  6 :
		case  9 :
		case 11 :
			if ( hil2 > 30 )
			{
				bufi[0] = '\0' ;
				return TRUE ;
			}
			break ;
		case 2 :

			hil5[0] = bufi[6] ;
			hil5[1] = bufi[7] ;
			hil5[2] = bufi[8] ;
			hil5[3] = bufi[9] ;
			hil5[4] = '\0' ;
			hil1    = atoi(hil5) ;
			if ( hil2 > 28 )	// IM Jahr 2000 geht dieser Check schief .....
			{
				if ( ! ( hil1 % 4) )
				{
					if ( hil2 > 29 )
					{
						bufi[0] = '\0' ;
						return TRUE ;
					}
				}
				else
				{
					bufi[0] = '\0' ;
					return TRUE ;
				};
			}
			break ;

		}

	}
	return j ;
}	// datumcheck


void C15700Dlg::OnEnKillfocusEdit1()
{
	sprintf ( savest,"%s",v_wo1.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo1.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo1.Format("%s",_T(bufh));
			else
				v_wo1.Format("%s",_T(savest));
		}
		else
				v_wo1.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo1.Replace(",",".");
		double hi = atof ( v_wo1.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo1.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo1.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);
}
void C15700Dlg::OnEnKillfocusEdit2()
{
	sprintf ( savest,"%s",v_wo2.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo2.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo2.Format("%s",_T(bufh));
			else
				v_wo2.Format("%s",_T(savest));
		}
		else
				v_wo2.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo2.Replace(",",".");
		double hi = atof ( v_wo2.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo2.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo2.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);
}
void C15700Dlg::OnEnKillfocusEdit3()
{
	sprintf ( savest,"%s",v_wo3.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo3.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo3.Format("%s",_T(bufh));
			else
				v_wo3.Format("%s",_T(savest));
		}
		else
				v_wo3.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo3.Replace(",",".");
		double hi = atof ( v_wo3.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo3.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo3.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);
}

void C15700Dlg::OnEnKillfocusEdit4()
{
	sprintf ( savest,"%s",v_wo4.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo4.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo4.Format("%s",_T(bufh));
			else
				v_wo4.Format("%s",_T(savest));
		}
		else
				v_wo4.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo4.Replace(",",".");
		double hi = atof ( v_wo4.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo4.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo4.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}
void C15700Dlg::OnEnKillfocusEdit5()
{
	sprintf ( savest,"%s",v_wo5.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo5.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo5.Format("%s",_T(bufh));
			else
				v_wo5.Format("%s",_T(savest));
		}
		else
				v_wo5.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo5.Replace(",",".");
		double hi = atof ( v_wo5.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo5.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo5.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit6()
{
	sprintf ( savest,"%s",v_wo6.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo6.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo6.Format("%s",_T(bufh));
			else
				v_wo6.Format("%s",_T(savest));
		}
		else
				v_wo6.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo6.Replace(",",".");
		double hi = atof ( v_wo6.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo6.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo6.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit7()
{
	sprintf ( savest,"%s",v_wo7.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo7.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo7.Format("%s",_T(bufh));
			else
				v_wo7.Format("%s",_T(savest));
		}
		else
				v_wo7.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo7.Replace(",",".");
		double hi = atof ( v_wo7.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo7.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo7.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit8()
{
	sprintf ( savest,"%s",v_wo8.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo8.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo8.Format("%s",_T(bufh));
			else
				v_wo8.Format("%s",_T(savest));
		}
		else
				v_wo8.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo8.Replace(",",".");
		double hi = atof ( v_wo8.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo8.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo8.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit9()
{
	sprintf ( savest,"%s",v_wo9.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo9.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo9.Format("%s",_T(bufh));
			else
				v_wo9.Format("%s",_T(savest));
		}
		else
				v_wo9.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo9.Replace(",",".");
		double hi = atof ( v_wo9.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo9.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo9.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit10()
{
	sprintf ( savest,"%s",v_wo10.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo10.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo10.Format("%s",_T(bufh));
			else
				v_wo10.Format("%s",_T(savest));
		}
		else
				v_wo10.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo10.Replace(",",".");
		double hi = atof ( v_wo10.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo10.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo10.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit11()
{
	sprintf ( savest,"%s",v_wo11.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo11.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo11.Format("%s",_T(bufh));
			else
				v_wo11.Format("%s",_T(savest));
		}
		else
				v_wo11.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo11.Replace(",",".");
		double hi = atof ( v_wo11.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo11.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo11.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit12()
{
	sprintf ( savest,"%s",v_wo12.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo12.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo12.Format("%s",_T(bufh));
			else
				v_wo12.Format("%s",_T(savest));
		}
		else
				v_wo12.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo12.Replace(",",".");
		double hi = atof ( v_wo12.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo12.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo12.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit13()
{
	sprintf ( savest,"%s",v_wo13.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo13.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo13.Format("%s",_T(bufh));
			else
				v_wo13.Format("%s",_T(savest));
		}
		else
				v_wo13.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo13.Replace(",",".");
		double hi = atof ( v_wo13.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo13.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo13.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit14()
{
	sprintf ( savest,"%s",v_wo14.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo14.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo14.Format("%s",_T(bufh));
			else
				v_wo14.Format("%s",_T(savest));
		}
		else
				v_wo14.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo14.Replace(",",".");
		double hi = atof ( v_wo14.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo14.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo14.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit15()
{
	sprintf ( savest,"%s",v_wo15.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo15.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo15.Format("%s",_T(bufh));
			else
				v_wo15.Format("%s",_T(savest));
		}
		else
				v_wo15.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo15.Replace(",",".");
		double hi = atof ( v_wo15.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo15.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo15.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit16()
{
	sprintf ( savest,"%s",v_wo16.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo16.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo16.Format("%s",_T(bufh));
			else
				v_wo16.Format("%s",_T(savest));
		}
		else
				v_wo16.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo16.Replace(",",".");
		double hi = atof ( v_wo16.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo16.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo16.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit17()
{
	sprintf ( savest,"%s",v_wo17.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo17.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo17.Format("%s",_T(bufh));
			else
				v_wo17.Format("%s",_T(savest));
		}
		else
				v_wo17.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo17.Replace(",",".");
		double hi = atof ( v_wo17.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo17.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo17.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit18()
{
	sprintf ( savest,"%s",v_wo18.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo18.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo18.Format("%s",_T(bufh));
			else
				v_wo18.Format("%s",_T(savest));
		}
		else
				v_wo18.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo18.Replace(",",".");
		double hi = atof ( v_wo18.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo18.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo18.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit19()
{
	sprintf ( savest,"%s",v_wo19.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo19.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo19.Format("%s",_T(bufh));
			else
				v_wo19.Format("%s",_T(savest));
		}
		else
				v_wo19.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo19.Replace(",",".");
		double hi = atof ( v_wo19.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo19.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo19.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit20()
{
	sprintf ( savest,"%s",v_wo20.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo20.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo20.Format("%s",_T(bufh));
			else
				v_wo20.Format("%s",_T(savest));
		}
		else
				v_wo20.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo20.Replace(",",".");
		double hi = atof ( v_wo20.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo20.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo20.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit21()
{
	sprintf ( savest,"%s",v_wo21.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo21.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo21.Format("%s",_T(bufh));
			else
				v_wo21.Format("%s",_T(savest));
		}
		else
				v_wo21.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo21.Replace(",",".");
		double hi = atof ( v_wo21.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo21.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo21.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit22()
{
	sprintf ( savest,"%s",v_wo22.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo22.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo22.Format("%s",_T(bufh));
			else
				v_wo22.Format("%s",_T(savest));
		}
		else
				v_wo22.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo22.Replace(",",".");
		double hi = atof ( v_wo22.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo22.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo22.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit23()
{
	sprintf ( savest,"%s",v_wo23.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo23.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo23.Format("%s",_T(bufh));
			else
				v_wo23.Format("%s",_T(savest));
		}
		else
				v_wo23.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo23.Replace(",",".");
		double hi = atof ( v_wo23.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo23.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo23.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit24()
{
	sprintf ( savest,"%s",v_wo24.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo24.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo24.Format("%s",_T(bufh));
			else
				v_wo24.Format("%s",_T(savest));
		}
		else
				v_wo24.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo24.Replace(",",".");
		double hi = atof ( v_wo24.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo24.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo24.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit25()
{
	sprintf ( savest,"%s",v_wo25.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo25.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo25.Format("%s",_T(bufh));
			else
				v_wo25.Format("%s",_T(savest));
		}
		else
				v_wo25.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo25.Replace(",",".");
		double hi = atof ( v_wo25.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo25.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo25.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit26()
{
	sprintf ( savest,"%s",v_wo26.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo26.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo26.Format("%s",_T(bufh));
			else
				v_wo26.Format("%s",_T(savest));
		}
		else
				v_wo26.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo26.Replace(",",".");
		double hi = atof ( v_wo26.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo26.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo26.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit27()
{
	sprintf ( savest,"%s",v_wo27.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo27.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo27.Format("%s",_T(bufh));
			else
				v_wo27.Format("%s",_T(savest));
		}
		else
				v_wo27.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo27.Replace(",",".");
		double hi = atof ( v_wo27.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo27.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo27.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit28()
{
	sprintf ( savest,"%s",v_wo28.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo28.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo28.Format("%s",_T(bufh));
			else
				v_wo28.Format("%s",_T(savest));
		}
		else
				v_wo28.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo28.Replace(",",".");
		double hi = atof ( v_wo28.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo28.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo28.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit29()
{
	sprintf ( savest,"%s",v_wo29.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo29.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo29.Format("%s",_T(bufh));
			else
				v_wo29.Format("%s",_T(savest));
		}
		else
				v_wo29.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo29.Replace(",",".");
		double hi = atof ( v_wo29.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo29.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo29.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit30()
{
	sprintf ( savest,"%s",v_wo30.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo30.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo30.Format("%s",_T(bufh));
			else
				v_wo30.Format("%s",_T(savest));
		}
		else
				v_wo30.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo30.Replace(",",".");
		double hi = atof ( v_wo30.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo30.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo30.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit31()
{
	sprintf ( savest,"%s",v_wo31.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo31.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo31.Format("%s",_T(bufh));
			else
				v_wo31.Format("%s",_T(savest));
		}
		else
				v_wo31.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo31.Replace(",",".");
		double hi = atof ( v_wo31.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo31.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo31.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit32()
{
	sprintf ( savest,"%s",v_wo32.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo32.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo32.Format("%s",_T(bufh));
			else
				v_wo32.Format("%s",_T(savest));
		}
		else
				v_wo32.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo32.Replace(",",".");
		double hi = atof ( v_wo32.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo32.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo32.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit33()
{
	sprintf ( savest,"%s",v_wo33.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo33.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo33.Format("%s",_T(bufh));
			else
				v_wo33.Format("%s",_T(savest));
		}
		else
				v_wo33.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo33.Replace(",",".");
		double hi = atof ( v_wo33.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo33.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo33.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit34()
{
	sprintf ( savest,"%s",v_wo34.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo34.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo34.Format("%s",_T(bufh));
			else
				v_wo34.Format("%s",_T(savest));
		}
		else
				v_wo34.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo34.Replace(",",".");
		double hi = atof ( v_wo34.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo34.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo34.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit35()
{
	sprintf ( savest,"%s",v_wo35.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo35.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo35.Format("%s",_T(bufh));
			else
				v_wo35.Format("%s",_T(savest));
		}
		else
				v_wo35.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo35.Replace(",",".");
		double hi = atof ( v_wo35.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo35.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo35.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit36()
{
	sprintf ( savest,"%s",v_wo36.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo36.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo36.Format("%s",_T(bufh));
			else
				v_wo36.Format("%s",_T(savest));
		}
		else
				v_wo36.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo36.Replace(",",".");
		double hi = atof ( v_wo36.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo36.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo36.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit37()
{
	sprintf ( savest,"%s",v_wo37.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo37.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo37.Format("%s",_T(bufh));
			else
				v_wo37.Format("%s",_T(savest));
		}
		else
				v_wo37.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo37.Replace(",",".");
		double hi = atof ( v_wo37.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo37.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo37.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit38()
{
	sprintf ( savest,"%s",v_wo38.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo38.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo38.Format("%s",_T(bufh));
			else
				v_wo38.Format("%s",_T(savest));
		}
		else
				v_wo38.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo38.Replace(",",".");
		double hi = atof ( v_wo38.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo38.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo38.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit39()
{
	sprintf ( savest,"%s",v_wo39.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo39.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo39.Format("%s",_T(bufh));
			else
				v_wo39.Format("%s",_T(savest));
		}
		else
				v_wo39.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo39.Replace(",",".");
		double hi = atof ( v_wo39.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo39.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo39.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit40()
{
	sprintf ( savest,"%s",v_wo40.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo40.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo40.Format("%s",_T(bufh));
			else
				v_wo40.Format("%s",_T(savest));
		}
		else
				v_wo40.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo40.Replace(",",".");
		double hi = atof ( v_wo40.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo40.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo40.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit41()
{
	sprintf ( savest,"%s",v_wo41.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo41.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo41.Format("%s",_T(bufh));
			else
				v_wo41.Format("%s",_T(savest));
		}
		else
				v_wo41.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo41.Replace(",",".");
		double hi = atof ( v_wo41.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo41.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo41.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit42()
{
	sprintf ( savest,"%s",v_wo42.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo42.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo42.Format("%s",_T(bufh));
			else
				v_wo42.Format("%s",_T(savest));
		}
		else
				v_wo42.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo42.Replace(",",".");
		double hi = atof ( v_wo42.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo42.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo42.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit43()
{
	sprintf ( savest,"%s",v_wo43.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo43.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo43.Format("%s",_T(bufh));
			else
				v_wo43.Format("%s",_T(savest));
		}
		else
				v_wo43.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo43.Replace(",",".");
		double hi = atof ( v_wo43.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo43.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo43.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit44()
{
	sprintf ( savest,"%s",v_wo44.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo44.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo44.Format("%s",_T(bufh));
			else
				v_wo44.Format("%s",_T(savest));
		}
		else
				v_wo44.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo44.Replace(",",".");
		double hi = atof ( v_wo44.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo44.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo44.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit45()
{
	sprintf ( savest,"%s",v_wo45.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo45.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo45.Format("%s",_T(bufh));
			else
				v_wo45.Format("%s",_T(savest));
		}
		else
				v_wo45.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo45.Replace(",",".");
		double hi = atof ( v_wo45.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo45.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo45.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit46()
{
	sprintf ( savest,"%s",v_wo46.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo46.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo46.Format("%s",_T(bufh));
			else
				v_wo46.Format("%s",_T(savest));
		}
		else
				v_wo46.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo46.Replace(",",".");
		double hi = atof ( v_wo46.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo46.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo46.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}


void C15700Dlg::OnEnKillfocusEdit47()
{
	sprintf ( savest,"%s",v_wo47.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo47.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo47.Format("%s",_T(bufh));
			else
				v_wo47.Format("%s",_T(savest));
		}
		else
				v_wo47.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo47.Replace(",",".");
		double hi = atof ( v_wo47.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo47.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo47.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit48()
{
	sprintf ( savest,"%s",v_wo48.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo48.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo48.Format("%s",_T(bufh));
			else
				v_wo48.Format("%s",_T(savest));
		}
		else
				v_wo48.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo48.Replace(",",".");
		double hi = atof ( v_wo48.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo48.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo48.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit49()
{
	sprintf ( savest,"%s",v_wo49.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo49.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo49.Format("%s",_T(bufh));
			else
				v_wo49.Format("%s",_T(savest));
		}
		else
				v_wo49.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo49.Replace(",",".");
		double hi = atof ( v_wo49.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo49.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo49.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit50()
{
	sprintf ( savest,"%s",v_wo50.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo50.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo50.Format("%s",_T(bufh));
			else
				v_wo50.Format("%s",_T(savest));
		}
		else
				v_wo50.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo50.Replace(",",".");
		double hi = atof ( v_wo50.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo50.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo50.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit51()
{
	sprintf ( savest,"%s",v_wo51.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo51.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo51.Format("%s",_T(bufh));
			else
				v_wo51.Format("%s",_T(savest));
		}
		else
				v_wo51.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo51.Replace(",",".");
		double hi = atof ( v_wo51.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo51.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo51.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit52()
{
	sprintf ( savest,"%s",v_wo52.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo52.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo52.Format("%s",_T(bufh));
			else
				v_wo52.Format("%s",_T(savest));
		}
		else
				v_wo52.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo52.Replace(",",".");
		double hi = atof ( v_wo52.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo52.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo52.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}

void C15700Dlg::OnEnKillfocusEdit53()
{
	sprintf ( savest,"%s",v_wo53.GetBuffer());

	UpdateData(TRUE);
	if ( dmodus == 0 )	// Datumsmodus
	{
		int i = m_wo53.GetLine(0,bufh,500) ;
		bufh[i] = '\0' ;
		if (i)
		{
			if (datumcheck(bufh))
				v_wo53.Format("%s",_T(bufh));
			else
				v_wo53.Format("%s",_T(savest));
		}
		else
				v_wo53.Format("%s",_T(savest));
	}
	else
	{	// Preismodi
		v_wo53.Replace(",",".");
		double hi = atof ( v_wo53.GetBuffer());
		if ( dmodus == 1 )	// Prozent
		{
			if ( hi > 99.99 ) hi = 99.99 ;
			if ( hi < -99.99 ) hi = -99.99 ;
			v_wo53.Format("%1.2f %%",hi);
		}
		if ( dmodus == 2 )
		{
			if ( hi >  999999.99 ) hi =  999999.99 ;
			if ( hi < -999999.99 ) hi = -999999.99 ;
			v_wo53.Format("%1.2f T",hi);
		}
	}
	UpdateData(FALSE);

}




void C15700Dlg::OnBnClickedButtup()
{
	UpdateData(TRUE);
	satzschreiben();
	if ( v_jahr < 2039 )
		v_jahr ++ ;
	satzlesen();
	UpdateData(FALSE);

}

void C15700Dlg::OnBnClickedButtdown()
{
	UpdateData(TRUE);
	satzschreiben();
	if ( v_jahr > 2010 )
		v_jahr -- ;
	satzlesen();
	UpdateData(FALSE);
}

void C15700Dlg::OnBnClickedCancel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	OnCancel();
}

void C15700Dlg::OnBnClickedOk()
{
	UpdateData(TRUE);
	nureinmalfragen=2;
	satzschreiben();
	OnOK();
}

void C15700Dlg::satzschreiben(void)
{

	double hi;

	char hita [4];
	char himo [4];
	char hijr [6];
	char hidatum[15];
	int tag;
	int monat;
	int jahr;

	int aktualen;

	if ( nureinmalfragen==2)	// vorher wurde "ok" gedr�ckt
		nureinmalfragen=1;
	else
		nureinmalfragen=0;

	fil_abrech.jr = (short) v_jahr ;
	for ( int i = 1 ; i < 54 ; i ++ )
	{
		aktualen=0;
		switch (i)
		{
		case 1 :
			fil_abrech.period =1;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo1.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat01.year || monat != s_end_dat01.month || s_end_dat01.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro01;
					fil_abrech.plan_proz = s_plan_proz01;
				}
			}
			else
			{
				v_wo1.Replace(",",".");
				hi = atof ( v_wo1.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz01 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro01;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro01 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz01;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat01.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat01.day;
					else
						fil_abrech.end_dat.day  =s_end_dat01.day;
					fil_abrech.end_dat.month=s_end_dat01.month;
					fil_abrech.end_dat.year =s_end_dat01.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 2 :
			fil_abrech.period =2;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo2.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat02.year || monat != s_end_dat02.month || s_end_dat02.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro02;
					fil_abrech.plan_proz = s_plan_proz02;
				}
			}
			else
			{
				v_wo2.Replace(",",".");
				hi = atof ( v_wo2.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz02 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro02;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro02 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz02;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat02.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat02.day;
					else
						fil_abrech.end_dat.day  =s_end_dat02.day;
					fil_abrech.end_dat.month=s_end_dat02.month;
					fil_abrech.end_dat.year =s_end_dat02.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 3 :
			fil_abrech.period =3;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo3.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat03.year || monat != s_end_dat03.month || s_end_dat03.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro03;
					fil_abrech.plan_proz = s_plan_proz03;
				}
			}
			else
			{
				v_wo3.Replace(",",".");
				hi = atof ( v_wo3.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz03 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro03;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro03 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz03;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat03.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat03.day;
					else
						fil_abrech.end_dat.day  =s_end_dat03.day;
					fil_abrech.end_dat.month=s_end_dat03.month;
					fil_abrech.end_dat.year =s_end_dat03.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 4 :
			fil_abrech.period =4;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo4.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat04.year || monat != s_end_dat04.month || s_end_dat04.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro04;
					fil_abrech.plan_proz = s_plan_proz04;
				}
			}
			else
			{
				v_wo4.Replace(",",".");
				hi = atof ( v_wo4.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz04 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro04;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro04 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz04;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat04.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat04.day;
					else
						fil_abrech.end_dat.day  =s_end_dat04.day;
					fil_abrech.end_dat.month=s_end_dat04.month;
					fil_abrech.end_dat.year =s_end_dat04.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			if ( dmonat == 2 )	// Quartal zuende
				i = 99 ;
			break;

		case 5 :
			fil_abrech.period =5;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo5.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat05.year || monat != s_end_dat05.month || s_end_dat05.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro05;
					fil_abrech.plan_proz = s_plan_proz05;
				}
			}
			else
			{
				v_wo5.Replace(",",".");
				hi = atof ( v_wo5.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz05 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro05;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro05 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz05;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat05.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat05.day;
					else
						fil_abrech.end_dat.day  =s_end_dat05.day;
					fil_abrech.end_dat.month=s_end_dat05.month;
					fil_abrech.end_dat.year =s_end_dat05.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 6 :
			fil_abrech.period =6;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo6.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat06.year || monat != s_end_dat06.month || s_end_dat06.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro06;
					fil_abrech.plan_proz = s_plan_proz06;
				}
			}
			else
			{
				v_wo6.Replace(",",".");
				hi = atof ( v_wo6.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz06 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro06;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro06 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz06;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat06.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat06.day;
					else
						fil_abrech.end_dat.day  =s_end_dat06.day;
					fil_abrech.end_dat.month=s_end_dat06.month;
					fil_abrech.end_dat.year =s_end_dat06.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 7 :
			fil_abrech.period =7;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo7.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat07.year || monat != s_end_dat07.month || s_end_dat07.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro07;
					fil_abrech.plan_proz = s_plan_proz07;
				}
			}
			else
			{
				v_wo7.Replace(",",".");
				hi = atof ( v_wo7.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz07 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro07;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro07 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz07;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat07.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat07.day;
					else
						fil_abrech.end_dat.day  =s_end_dat07.day;
					fil_abrech.end_dat.month=s_end_dat07.month;
					fil_abrech.end_dat.year =s_end_dat07.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 8 :
			fil_abrech.period =8;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo8.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat08.year || monat != s_end_dat08.month || s_end_dat08.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro08;
					fil_abrech.plan_proz = s_plan_proz08;
				}
			}
			else
			{
				v_wo8.Replace(",",".");
				hi = atof ( v_wo8.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz08 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro08;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro08 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz08;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat08.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat08.day;
					else
						fil_abrech.end_dat.day  =s_end_dat08.day;
					fil_abrech.end_dat.month=s_end_dat08.month;
					fil_abrech.end_dat.year =s_end_dat08.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 9 :
			fil_abrech.period =9;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo9.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat09.year || monat != s_end_dat09.month || s_end_dat09.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro09;
					fil_abrech.plan_proz = s_plan_proz09;
				}
			}
			else
			{
				v_wo9.Replace(",",".");
				hi = atof ( v_wo9.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz09 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro09;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro09 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz09;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat09.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat09.day;
					else
						fil_abrech.end_dat.day  =s_end_dat09.day;
					fil_abrech.end_dat.month=s_end_dat09.month;
					fil_abrech.end_dat.year =s_end_dat09.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 10 :
			fil_abrech.period =10;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo10.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat10.year || monat != s_end_dat10.month || s_end_dat10.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro10;
					fil_abrech.plan_proz = s_plan_proz10;
				}
			}
			else
			{
				v_wo10.Replace(",",".");
				hi = atof ( v_wo10.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz10 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro10;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro10 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz10;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat10.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat10.day;
					else
						fil_abrech.end_dat.day  =s_end_dat10.day;
					fil_abrech.end_dat.month=s_end_dat10.month;
					fil_abrech.end_dat.year =s_end_dat10.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 11 :
			fil_abrech.period =11;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo11.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat11.year || monat != s_end_dat11.month || s_end_dat11.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro11;
					fil_abrech.plan_proz = s_plan_proz11;
				}
			}
			else
			{
				v_wo11.Replace(",",".");
				hi = atof ( v_wo11.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz11 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro11;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro11 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz11;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat11.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat11.day;
					else
						fil_abrech.end_dat.day  =s_end_dat11.day;
					fil_abrech.end_dat.month=s_end_dat11.month;
					fil_abrech.end_dat.year =s_end_dat11.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 12 :
			fil_abrech.period =12;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo12.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat12.year || monat != s_end_dat12.month || s_end_dat12.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro12;
					fil_abrech.plan_proz = s_plan_proz12;
				}
			}
			else
			{
				v_wo12.Replace(",",".");
				hi = atof ( v_wo12.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz12 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro12;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro12 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz12;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat12.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat12.day;
					else
						fil_abrech.end_dat.day  =s_end_dat12.day;
					fil_abrech.end_dat.month=s_end_dat12.month;
					fil_abrech.end_dat.year =s_end_dat12.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}

			if ( dmonat == 1 )	// Monatsende
				i = 99 ;
			break;

		case 13 :
			fil_abrech.period =13;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo13.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat13.year || monat != s_end_dat13.month || s_end_dat13.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro13;
					fil_abrech.plan_proz = s_plan_proz13;
				}
			}
			else
			{
				v_wo13.Replace(",",".");
				hi = atof ( v_wo13.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz13 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro13;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro13 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz13;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat13.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat13.day;
					else
						fil_abrech.end_dat.day  =s_end_dat13.day;
					fil_abrech.end_dat.month=s_end_dat13.month;
					fil_abrech.end_dat.year =s_end_dat13.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 14 :
			fil_abrech.period =14;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo14.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat14.year || monat != s_end_dat14.month || s_end_dat14.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro14;
					fil_abrech.plan_proz = s_plan_proz14;
				}
			}
			else
			{
				v_wo14.Replace(",",".");
				hi = atof ( v_wo14.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz14 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro14;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro14 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz14;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat14.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat14.day;
					else
						fil_abrech.end_dat.day  =s_end_dat14.day;
					fil_abrech.end_dat.month=s_end_dat14.month;
					fil_abrech.end_dat.year =s_end_dat14.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 15 :
			fil_abrech.period =15;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo15.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat15.year || monat != s_end_dat15.month || s_end_dat15.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro15;
					fil_abrech.plan_proz = s_plan_proz15;
				}
			}
			else
			{
				v_wo15.Replace(",",".");
				hi = atof ( v_wo15.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz15 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro15;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro15 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz15;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat15.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat15.day;
					else
						fil_abrech.end_dat.day  =s_end_dat15.day;
					fil_abrech.end_dat.month=s_end_dat15.month;
					fil_abrech.end_dat.year =s_end_dat15.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 16 :
			fil_abrech.period =16;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo16.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat16.year || monat != s_end_dat16.month || s_end_dat16.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro16;
					fil_abrech.plan_proz = s_plan_proz16;
				}
			}
			else
			{
				v_wo16.Replace(",",".");
				hi = atof ( v_wo16.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz16 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro16;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro16 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz16;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat16.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat16.day;
					else
						fil_abrech.end_dat.day  =s_end_dat16.day;
					fil_abrech.end_dat.month=s_end_dat16.month;
					fil_abrech.end_dat.year =s_end_dat16.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 17 :
			fil_abrech.period =17;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo17.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat17.year || monat != s_end_dat17.month || s_end_dat17.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro17;
					fil_abrech.plan_proz = s_plan_proz17;
				}
			}
			else
			{
				v_wo17.Replace(",",".");
				hi = atof ( v_wo17.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz17 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro17;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro17 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz17;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat17.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat17.day;
					else
						fil_abrech.end_dat.day  =s_end_dat17.day;
					fil_abrech.end_dat.month=s_end_dat17.month;
					fil_abrech.end_dat.year =s_end_dat17.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 18 :
			fil_abrech.period =18;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo18.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat18.year || monat != s_end_dat18.month || s_end_dat18.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro18;
					fil_abrech.plan_proz = s_plan_proz18;
				}
			}
			else
			{
				v_wo18.Replace(",",".");
				hi = atof ( v_wo18.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz18 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro18;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro18 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz18;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat18.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat18.day;
					else
						fil_abrech.end_dat.day  =s_end_dat18.day;
					fil_abrech.end_dat.month=s_end_dat18.month;
					fil_abrech.end_dat.year =s_end_dat18.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 19 :
			fil_abrech.period =19;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo19.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat19.year || monat != s_end_dat19.month || s_end_dat19.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro19;
					fil_abrech.plan_proz = s_plan_proz19;
				}
			}
			else
			{
				v_wo19.Replace(",",".");
				hi = atof ( v_wo19.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz19 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro19;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro19 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz19;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat19.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat19.day;
					else
						fil_abrech.end_dat.day  =s_end_dat19.day;
					fil_abrech.end_dat.month=s_end_dat19.month;
					fil_abrech.end_dat.year =s_end_dat19.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 20 :
			fil_abrech.period =20;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo20.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat20.year || monat != s_end_dat20.month || s_end_dat20.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro20;
					fil_abrech.plan_proz = s_plan_proz20;
				}
			}
			else
			{
				v_wo20.Replace(",",".");
				hi = atof ( v_wo20.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz20 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro20;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro20 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz20;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat20.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat20.day;
					else
						fil_abrech.end_dat.day  =s_end_dat20.day;
					fil_abrech.end_dat.month=s_end_dat20.month;
					fil_abrech.end_dat.year =s_end_dat20.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 21 :
			fil_abrech.period =21;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo21.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat21.year || monat != s_end_dat21.month || s_end_dat21.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro21;
					fil_abrech.plan_proz = s_plan_proz21;
				}
			}
			else
			{
				v_wo21.Replace(",",".");
				hi = atof ( v_wo21.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz21 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro21;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro21 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz21;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat21.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat21.day;
					else
						fil_abrech.end_dat.day  =s_end_dat21.day;
					fil_abrech.end_dat.month=s_end_dat21.month;
					fil_abrech.end_dat.year =s_end_dat21.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 22 :
			fil_abrech.period =22;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo22.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat22.year || monat != s_end_dat22.month || s_end_dat22.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro22;
					fil_abrech.plan_proz = s_plan_proz22;
				}
			}
			else
			{
				v_wo22.Replace(",",".");
				hi = atof ( v_wo22.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz22 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro22;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro22 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz22;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat22.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat22.day;
					else
						fil_abrech.end_dat.day  =s_end_dat22.day;
					fil_abrech.end_dat.month=s_end_dat22.month;
					fil_abrech.end_dat.year =s_end_dat22.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 23 :
			fil_abrech.period =23;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo23.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat23.year || monat != s_end_dat23.month || s_end_dat23.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro23;
					fil_abrech.plan_proz = s_plan_proz23;
				}
			}
			else
			{
				v_wo23.Replace(",",".");
				hi = atof ( v_wo23.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz23 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro23;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro23 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz23;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat23.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat23.day;
					else
						fil_abrech.end_dat.day  =s_end_dat23.day;
					fil_abrech.end_dat.month=s_end_dat23.month;
					fil_abrech.end_dat.year =s_end_dat23.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 24 :
			fil_abrech.period =24;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo24.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat24.year || monat != s_end_dat24.month || s_end_dat24.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro24;
					fil_abrech.plan_proz = s_plan_proz24;
				}
			}
			else
			{
				v_wo24.Replace(",",".");
				hi = atof ( v_wo24.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz24 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro24;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro24 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz24;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat24.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat24.day;
					else
						fil_abrech.end_dat.day  =s_end_dat24.day;
					fil_abrech.end_dat.month=s_end_dat24.month;
					fil_abrech.end_dat.year =s_end_dat24.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 25 :
			fil_abrech.period =25;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo25.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat25.year || monat != s_end_dat25.month || s_end_dat25.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro25;
					fil_abrech.plan_proz = s_plan_proz25;
				}
			}
			else
			{
				v_wo25.Replace(",",".");
				hi = atof ( v_wo25.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz25 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro25;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro25 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz25;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat25.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat25.day;
					else
						fil_abrech.end_dat.day  =s_end_dat25.day;
					fil_abrech.end_dat.month=s_end_dat25.month;
					fil_abrech.end_dat.year =s_end_dat25.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 26 :
			fil_abrech.period =26;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo26.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat26.year || monat != s_end_dat26.month || s_end_dat26.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro26;
					fil_abrech.plan_proz = s_plan_proz26;
				}
			}
			else
			{
				v_wo26.Replace(",",".");
				hi = atof ( v_wo26.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz26 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro26;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro26 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz26;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat26.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat26.day;
					else
						fil_abrech.end_dat.day  =s_end_dat26.day;
					fil_abrech.end_dat.month=s_end_dat26.month;
					fil_abrech.end_dat.year =s_end_dat26.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 27 :
			fil_abrech.period =27;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo27.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat27.year || monat != s_end_dat27.month || s_end_dat27.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro27;
					fil_abrech.plan_proz = s_plan_proz27;
				}
			}
			else
			{
				v_wo27.Replace(",",".");
				hi = atof ( v_wo27.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz27 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro27;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro27 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz27;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat27.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat27.day;
					else
						fil_abrech.end_dat.day  =s_end_dat27.day;
					fil_abrech.end_dat.month=s_end_dat27.month;
					fil_abrech.end_dat.year =s_end_dat27.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 28 :
			fil_abrech.period =28;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo28.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat28.year || monat != s_end_dat28.month || s_end_dat28.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro28;
					fil_abrech.plan_proz = s_plan_proz28;
				}
			}
			else
			{
				v_wo28.Replace(",",".");
				hi = atof ( v_wo28.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz28 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro28;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro28 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz28;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat28.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat28.day;
					else
						fil_abrech.end_dat.day  =s_end_dat28.day;
					fil_abrech.end_dat.month=s_end_dat28.month;
					fil_abrech.end_dat.year =s_end_dat28.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 29 :
			fil_abrech.period =29;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo29.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat29.year || monat != s_end_dat29.month || s_end_dat29.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro29;
					fil_abrech.plan_proz = s_plan_proz29;
				}
			}
			else
			{
				v_wo29.Replace(",",".");
				hi = atof ( v_wo29.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz29 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro29;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro29 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz29;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat29.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat29.day;
					else
						fil_abrech.end_dat.day  =s_end_dat29.day;
					fil_abrech.end_dat.month=s_end_dat29.month;
					fil_abrech.end_dat.year =s_end_dat29.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 30 :
			fil_abrech.period =30;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo30.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat30.year || monat != s_end_dat30.month || s_end_dat30.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro30;
					fil_abrech.plan_proz = s_plan_proz30;
				}
			}
			else
			{
				v_wo30.Replace(",",".");
				hi = atof ( v_wo30.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz30 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro30;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro30 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz30;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat30.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat30.day;
					else
						fil_abrech.end_dat.day  =s_end_dat30.day;
					fil_abrech.end_dat.month=s_end_dat30.month;
					fil_abrech.end_dat.year =s_end_dat30.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 31 :
			fil_abrech.period =31;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo31.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat31.year || monat != s_end_dat31.month || s_end_dat31.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro31;
					fil_abrech.plan_proz = s_plan_proz31;
				}
			}
			else
			{
				v_wo31.Replace(",",".");
				hi = atof ( v_wo31.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz31 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro31;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro31 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz31;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat31.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat31.day;
					else
						fil_abrech.end_dat.day  =s_end_dat31.day;
					fil_abrech.end_dat.month=s_end_dat31.month;
					fil_abrech.end_dat.year =s_end_dat31.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 32 :
			fil_abrech.period =32;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo32.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat32.year || monat != s_end_dat32.month || s_end_dat32.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro32;
					fil_abrech.plan_proz = s_plan_proz32;
				}
			}
			else
			{
				v_wo32.Replace(",",".");
				hi = atof ( v_wo32.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz32 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro32;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro32 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz32;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat32.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat32.day;
					else
						fil_abrech.end_dat.day  =s_end_dat32.day;
					fil_abrech.end_dat.month=s_end_dat32.month;
					fil_abrech.end_dat.year =s_end_dat32.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 33 :
			fil_abrech.period =33;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo33.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat33.year || monat != s_end_dat33.month || s_end_dat33.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro33;
					fil_abrech.plan_proz = s_plan_proz33;
				}
			}
			else
			{
				v_wo33.Replace(",",".");
				hi = atof ( v_wo33.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz33 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro33;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro33 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz33;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat33.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat33.day;
					else
						fil_abrech.end_dat.day  =s_end_dat33.day;
					fil_abrech.end_dat.month=s_end_dat33.month;
					fil_abrech.end_dat.year =s_end_dat33.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 34 :
			fil_abrech.period =34;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo34.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat34.year || monat != s_end_dat34.month || s_end_dat34.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro34;
					fil_abrech.plan_proz = s_plan_proz34;
				}
			}
			else
			{
				v_wo34.Replace(",",".");
				hi = atof ( v_wo34.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz34 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro34;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro34 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz34;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat34.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat34.day;
					else
						fil_abrech.end_dat.day  =s_end_dat34.day;
					fil_abrech.end_dat.month=s_end_dat34.month;
					fil_abrech.end_dat.year =s_end_dat34.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 35 :
			fil_abrech.period =35;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo35.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat35.year || monat != s_end_dat35.month || s_end_dat35.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro35;
					fil_abrech.plan_proz = s_plan_proz35;
				}
			}
			else
			{
				v_wo35.Replace(",",".");
				hi = atof ( v_wo35.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz35 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro35;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro35 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz35;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat35.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat35.day;
					else
						fil_abrech.end_dat.day  =s_end_dat35.day;
					fil_abrech.end_dat.month=s_end_dat35.month;
					fil_abrech.end_dat.year =s_end_dat35.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 36 :
			fil_abrech.period =36;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo36.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat36.year || monat != s_end_dat36.month || s_end_dat36.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro36;
					fil_abrech.plan_proz = s_plan_proz36;
				}
			}
			else
			{
				v_wo36.Replace(",",".");
				hi = atof ( v_wo36.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz36 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro36;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro36 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz36;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat36.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat36.day;
					else
						fil_abrech.end_dat.day  =s_end_dat36.day;
					fil_abrech.end_dat.month=s_end_dat36.month;
					fil_abrech.end_dat.year =s_end_dat36.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 37 :
			fil_abrech.period =37;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo37.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat37.year || monat != s_end_dat37.month || s_end_dat37.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro37;
					fil_abrech.plan_proz = s_plan_proz37;
				}
			}
			else
			{
				v_wo37.Replace(",",".");
				hi = atof ( v_wo37.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz37 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro37;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro37 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz37;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat37.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat37.day;
					else
						fil_abrech.end_dat.day  =s_end_dat37.day;
					fil_abrech.end_dat.month=s_end_dat37.month;
					fil_abrech.end_dat.year =s_end_dat37.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 38 :
			fil_abrech.period =38;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo38.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat38.year || monat != s_end_dat38.month || s_end_dat38.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro38;
					fil_abrech.plan_proz = s_plan_proz38;
				}
			}
			else
			{
				v_wo38.Replace(",",".");
				hi = atof ( v_wo38.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz38 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro38;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro38 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz38;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat38.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat38.day;
					else
						fil_abrech.end_dat.day  =s_end_dat38.day;
					fil_abrech.end_dat.month=s_end_dat38.month;
					fil_abrech.end_dat.year =s_end_dat38.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 39 :
			fil_abrech.period =39;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo39.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat39.year || monat != s_end_dat39.month || s_end_dat39.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro39;
					fil_abrech.plan_proz = s_plan_proz39;
				}
			}
			else
			{
				v_wo39.Replace(",",".");
				hi = atof ( v_wo39.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz39 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro39;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro39 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz39;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat39.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat39.day;
					else
						fil_abrech.end_dat.day  =s_end_dat39.day;
					fil_abrech.end_dat.month=s_end_dat39.month;
					fil_abrech.end_dat.year =s_end_dat39.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 40 :
			fil_abrech.period =40;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo40.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat40.year || monat != s_end_dat40.month || s_end_dat40.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro40;
					fil_abrech.plan_proz = s_plan_proz40;
				}
			}
			else
			{
				v_wo40.Replace(",",".");
				hi = atof ( v_wo40.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz40 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro40;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro40 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz40;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat40.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat40.day;
					else
						fil_abrech.end_dat.day  =s_end_dat40.day;
					fil_abrech.end_dat.month=s_end_dat40.month;
					fil_abrech.end_dat.year =s_end_dat40.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 41 :
			fil_abrech.period =41;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo41.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat41.year || monat != s_end_dat41.month || s_end_dat41.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro41;
					fil_abrech.plan_proz = s_plan_proz41;
				}
			}
			else
			{
				v_wo41.Replace(",",".");
				hi = atof ( v_wo41.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz41 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro41;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro41 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz41;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat41.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat41.day;
					else
						fil_abrech.end_dat.day  =s_end_dat41.day;
					fil_abrech.end_dat.month=s_end_dat41.month;
					fil_abrech.end_dat.year =s_end_dat41.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 42 :
			fil_abrech.period =42;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo42.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat42.year || monat != s_end_dat42.month || s_end_dat42.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro42;
					fil_abrech.plan_proz = s_plan_proz42;
				}
			}
			else
			{
				v_wo42.Replace(",",".");
				hi = atof ( v_wo42.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz42 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro42;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro42 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz42;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat42.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat42.day;
					else
						fil_abrech.end_dat.day  =s_end_dat42.day;
					fil_abrech.end_dat.month=s_end_dat42.month;
					fil_abrech.end_dat.year =s_end_dat42.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 43 :
			fil_abrech.period =43;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo43.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat43.year || monat != s_end_dat43.month || s_end_dat43.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro43;
					fil_abrech.plan_proz = s_plan_proz43;
				}
			}
			else
			{
				v_wo43.Replace(",",".");
				hi = atof ( v_wo43.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz43 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro43;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro43 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz43;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat43.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat43.day;
					else
						fil_abrech.end_dat.day  =s_end_dat43.day;
					fil_abrech.end_dat.month=s_end_dat43.month;
					fil_abrech.end_dat.year =s_end_dat43.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 44 :
			fil_abrech.period =44;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo44.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat44.year || monat != s_end_dat44.month || s_end_dat44.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro44;
					fil_abrech.plan_proz = s_plan_proz44;
				}
			}
			else
			{
				v_wo44.Replace(",",".");
				hi = atof ( v_wo44.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz44 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro44;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro44 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz44;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat44.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat44.day;
					else
						fil_abrech.end_dat.day  =s_end_dat44.day;
					fil_abrech.end_dat.month=s_end_dat44.month;
					fil_abrech.end_dat.year =s_end_dat44.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 45 :
			fil_abrech.period =45;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo45.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat45.year || monat != s_end_dat45.month || s_end_dat45.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro45;
					fil_abrech.plan_proz = s_plan_proz45;
				}
			}
			else
			{
				v_wo45.Replace(",",".");
				hi = atof ( v_wo45.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz45 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro45;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro45 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz45;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat45.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat45.day;
					else
						fil_abrech.end_dat.day  =s_end_dat45.day;
					fil_abrech.end_dat.month=s_end_dat45.month;
					fil_abrech.end_dat.year =s_end_dat45.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 46 :
			fil_abrech.period =46;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo46.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat46.year || monat != s_end_dat46.month || s_end_dat46.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro46;
					fil_abrech.plan_proz = s_plan_proz46;
				}
			}
			else
			{
				v_wo46.Replace(",",".");
				hi = atof ( v_wo46.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz46 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro46;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro46 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz46;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat46.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat46.day;
					else
						fil_abrech.end_dat.day  =s_end_dat46.day;
					fil_abrech.end_dat.month=s_end_dat46.month;
					fil_abrech.end_dat.year =s_end_dat46.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 47 :
			fil_abrech.period =47;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo47.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat47.year || monat != s_end_dat47.month || s_end_dat47.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro47;
					fil_abrech.plan_proz = s_plan_proz47;
				}
			}
			else
			{
				v_wo47.Replace(",",".");
				hi = atof ( v_wo47.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz47 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro47;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro47 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz47;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat47.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat47.day;
					else
						fil_abrech.end_dat.day  =s_end_dat47.day;
					fil_abrech.end_dat.month=s_end_dat47.month;
					fil_abrech.end_dat.year =s_end_dat47.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 48 :
			fil_abrech.period =48;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo48.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat48.year || monat != s_end_dat48.month || s_end_dat48.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro48;
					fil_abrech.plan_proz = s_plan_proz48;
				}
			}
			else
			{
				v_wo48.Replace(",",".");
				hi = atof ( v_wo48.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz48 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro48;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro48 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz48;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat48.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat48.day;
					else
						fil_abrech.end_dat.day  =s_end_dat48.day;
					fil_abrech.end_dat.month=s_end_dat48.month;
					fil_abrech.end_dat.year =s_end_dat48.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 49 :
			fil_abrech.period =49;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo49.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat49.year || monat != s_end_dat49.month || s_end_dat49.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro49;
					fil_abrech.plan_proz = s_plan_proz49;
				}
			}
			else
			{
				v_wo49.Replace(",",".");
				hi = atof ( v_wo49.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz49 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro49;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro49 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz49;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat49.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat49.day;
					else
						fil_abrech.end_dat.day  =s_end_dat49.day;
					fil_abrech.end_dat.month=s_end_dat49.month;
					fil_abrech.end_dat.year =s_end_dat49.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 50 :
			fil_abrech.period =50;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo50.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat50.year || monat != s_end_dat50.month || s_end_dat50.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro50;
					fil_abrech.plan_proz = s_plan_proz50;
				}
			}
			else
			{
				v_wo50.Replace(",",".");
				hi = atof ( v_wo50.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz50 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro50;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro50 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz50;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat50.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat50.day;
					else
						fil_abrech.end_dat.day  =s_end_dat50.day;
					fil_abrech.end_dat.month=s_end_dat50.month;
					fil_abrech.end_dat.year =s_end_dat50.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 51 :
			fil_abrech.period =51;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo51.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat51.year || monat != s_end_dat51.month || s_end_dat51.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro51;
					fil_abrech.plan_proz = s_plan_proz51;
				}
			}
			else
			{
				v_wo51.Replace(",",".");
				hi = atof ( v_wo51.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz51 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro51;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro51 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz51;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat51.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat51.day;
					else
						fil_abrech.end_dat.day  =s_end_dat51.day;
					fil_abrech.end_dat.month=s_end_dat51.month;
					fil_abrech.end_dat.year =s_end_dat51.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;

		case 52 :
			fil_abrech.period =52;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo52.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat52.year || monat != s_end_dat52.month || s_end_dat52.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro52;
					fil_abrech.plan_proz = s_plan_proz52;
				}
			}
			else
			{
				v_wo52.Replace(",",".");
				hi = atof ( v_wo52.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz52 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro52;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro52 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz52;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat52.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat52.day;
					else
						fil_abrech.end_dat.day  =s_end_dat52.day;
					fil_abrech.end_dat.month=s_end_dat52.month;
					fil_abrech.end_dat.year =s_end_dat52.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			if ( wochenzahl == 52 )
				i = 99 ;
			break;

		case 53 :
			fil_abrech.period =53;
			if ( dmodus == 0 )	// datumsmodus
			{
				sprintf ( hidatum,"%s", v_wo53.GetBuffer());
				hita[0]=hidatum[0];
				hita[1]=hidatum[1];
				hita[2]='\0';
				tag= atoi(hita);
				himo[0]=hidatum[3];
				himo[1]=hidatum[4];
				himo[2]='\0';
				monat= atoi(himo);
				hijr[0]=hidatum[6];
				hijr[1]=hidatum[7];
				hijr[2]=hidatum[8];
				hijr[3]=hidatum[9];
				hijr[4]='\0';
				jahr = atoi(hijr);
				if ( jahr != s_end_dat53.year || monat != s_end_dat53.month || s_end_dat53.day != tag )
				{
					aktualen = 1;
					fil_abrech.end_dat.day  =tag;
					fil_abrech.end_dat.month=monat;
					fil_abrech.end_dat.year =jahr;
					fil_abrech.plan_eur = s_plan_euro53;
					fil_abrech.plan_proz = s_plan_proz53;
				}
			}
			else
			{
				v_wo53.Replace(",",".");
				hi = atof ( v_wo53.GetBuffer());
				if ( dmodus == 1 )	// Prozent
				{
					if ( hi > 99.99 ) hi = 99.99 ;
					if ( hi < -99.99 ) hi = -99.99 ;
					if ( hi != s_plan_proz53 )
					{
						aktualen = 1;
						fil_abrech.plan_eur = s_plan_euro53;
						fil_abrech.plan_proz = hi;
					}
				}
				if ( dmodus == 2 )	// Umsatz
				{
					if ( hi >  999999.99 ) hi =  999999.99 ;
					if ( hi < -999999.99 ) hi = -999999.99 ;
					if ( hi != s_plan_euro53 )
					{
						aktualen =1;
						fil_abrech.plan_eur = hi;
						fil_abrech.plan_proz = s_plan_proz53;
					}
				}
				if ( aktualen==1)
				{
					if ( s_end_dat53.day < 0 )
						fil_abrech.end_dat.day  = 0 - s_end_dat53.day;
					else
						fil_abrech.end_dat.day  =s_end_dat53.day;
					fil_abrech.end_dat.month=s_end_dat53.month;
					fil_abrech.end_dat.year =s_end_dat53.year;
				}
			}
			if ( aktualen == 1 )
			{
				if ( ! Speicherfrage())	{ i = 99; break;}
				Fil_abrech.fil_abrechdelete();
				Fil_abrech.fil_abrechinsert();
			}
			break;
		}

	}
}

char edstr01[12];
char edstr02[12];
char edstr03[12];
char edstr04[12];
char edstr05[12];
char edstr06[12];
char edstr07[12];
char edstr08[12];
char edstr09[12];
char edstr10[12];
char edstr11[12];
char edstr12[12];
char edstr13[12];
char edstr14[12];
char edstr15[12];
char edstr16[12];
char edstr17[12];
char edstr18[12];
char edstr19[12];
char edstr20[12];
char edstr21[12];
char edstr22[12];
char edstr23[12];
char edstr24[12];
char edstr25[12];
char edstr26[12];
char edstr27[12];
char edstr28[12];
char edstr29[12];
char edstr30[12];
char edstr31[12];
char edstr32[12];
char edstr33[12];
char edstr34[12];
char edstr35[12];
char edstr36[12];
char edstr37[12];
char edstr38[12];
char edstr39[12];
char edstr40[12];
char edstr41[12];
char edstr42[12];
char edstr43[12];
char edstr44[12];
char edstr45[12];
char edstr46[12];
char edstr47[12];
char edstr48[12];
char edstr49[12];
char edstr50[12];
char edstr51[12];
char edstr52[12];
char edstr53[12];

short edday01;
short edday02;
short edday03;
short edday04;
short edday05;
short edday06;
short edday07;
short edday08;
short edday09;
short edday10;
short edday11;
short edday12;
short edday13;
short edday14;
short edday15;
short edday16;
short edday17;
short edday18;
short edday19;
short edday20;
short edday21;
short edday22;
short edday23;
short edday24;
short edday25;
short edday26;
short edday27;
short edday28;
short edday29;
short edday30;
short edday31;
short edday32;
short edday33;
short edday34;
short edday35;
short edday36;
short edday37;
short edday38;
short edday39;
short edday40;
short edday41;
short edday42;
short edday43;
short edday44;
short edday45;
short edday46;
short edday47;
short edday48;
short edday49;
short edday50;
short edday51;
short edday52;
short edday53;

short edmon01;
short edmon02;
short edmon03;
short edmon04;
short edmon05;
short edmon06;
short edmon07;
short edmon08;
short edmon09;
short edmon10;
short edmon11;
short edmon12;
short edmon13;
short edmon14;
short edmon15;
short edmon16;
short edmon17;
short edmon18;
short edmon19;
short edmon20;
short edmon21;
short edmon22;
short edmon23;
short edmon24;
short edmon25;
short edmon26;
short edmon27;
short edmon28;
short edmon29;
short edmon30;
short edmon31;
short edmon32;
short edmon33;
short edmon34;
short edmon35;
short edmon36;
short edmon37;
short edmon38;
short edmon39;
short edmon40;
short edmon41;
short edmon42;
short edmon43;
short edmon44;
short edmon45;
short edmon46;
short edmon47;
short edmon48;
short edmon49;
short edmon50;
short edmon51;
short edmon52;
short edmon53;

short edjr01;
short edjr02;
short edjr03;
short edjr04;
short edjr05;
short edjr06;
short edjr07;
short edjr08;
short edjr09;
short edjr10;
short edjr11;
short edjr12;
short edjr13;
short edjr14;
short edjr15;
short edjr16;
short edjr17;
short edjr18;
short edjr19;
short edjr20;
short edjr21;
short edjr22;
short edjr23;
short edjr24;
short edjr25;
short edjr26;
short edjr27;
short edjr28;
short edjr29;
short edjr30;
short edjr31;
short edjr32;
short edjr33;
short edjr34;
short edjr35;
short edjr36;
short edjr37;
short edjr38;
short edjr39;
short edjr40;
short edjr41;
short edjr42;
short edjr43;
short edjr44;
short edjr45;
short edjr46;
short edjr47;
short edjr48;
short edjr49;
short edjr50;
short edjr51;
short edjr52;
short edjr53;




void generiereenddatum ( short jahr, short dmonat )
{
	if (dmonat==1)	// Monats-Logik
	{
		sprintf( edstr01,"31.01.%04d",jahr);
		edday01=31; edmon01=1; edjr01 = jahr;

		sprintf( edstr02,"28.02.%04d",jahr);
		edday02=28; edmon02=2; edjr02 = jahr;

// ich lasse nur Jahre zw. 2010 und 2039 zu 

		if (jahr == 2012 || jahr == 2016 || jahr == 2020 || jahr == 2024 || jahr == 2028 
			                                             || jahr == 2032 || jahr == 2036 )
		{		
					 sprintf( edstr02,"29.02.%04d",jahr);
					edday02=29;
		}

		sprintf( edstr03,"31.03.%04d",jahr);
		edday03=31; edmon03=3; edjr03 = jahr;

		sprintf( edstr04,"30.04.%04d",jahr);
		edday04=30; edmon04=4; edjr04 = jahr;

		sprintf( edstr05,"31.05.%04d",jahr);
		edday05=31; edmon05=5; edjr05 = jahr;

		sprintf( edstr06,"30.06.%04d",jahr);
		edday06=30; edmon06=6; edjr06 = jahr;

		sprintf( edstr07,"31.07.%04d",jahr);
		edday07=31; edmon07=7; edjr07 = jahr;

		sprintf( edstr08,"31.08.%04d",jahr);
		edday08=31; edmon08=8; edjr08 = jahr;

		sprintf( edstr09,"30.09.%04d",jahr);
		edday09=30; edmon09=9; edjr09 = jahr;

		sprintf( edstr10,"31.10.%04d",jahr);
		edday10=31; edmon10=10; edjr10 = jahr;

		sprintf( edstr11,"30.11.%04d",jahr);
		edday11=30; edmon11=11; edjr11 = jahr;

		sprintf( edstr12,"31.12.%04d",jahr);
		edday12=31; edmon12=12; edjr12 = jahr;

		wochenzahl = 12;
		return;
	}

	if (dmonat==2)	// Quartals-Logik
	{
		sprintf( edstr01,"31.03.%04d",jahr);
		edday01=31; edmon01=3; edjr01 = jahr;

		sprintf( edstr02,"30.06.%04d",jahr);
		edday02=30; edmon02=6; edjr02 = jahr;

		sprintf( edstr03,"30.09.%04d",jahr);
		edday03=30; edmon03=9; edjr03 = jahr;

		sprintf( edstr04,"31.12.%04d",jahr);
		edday04=31; edmon04=12; edjr04 = jahr;

		wochenzahl = 4;
		return;
	}
// lt. aktueller ISO-Norm begint jede KW mit dem Montag
// Die erste Woche ist diejenige, die den ersten Donnerstag des Jahres enth�lt
// d.h. falls der 1. Januar auf Montag,Dienstag,Mittwoch oder Donnerstag liegt
// 53 KW gibt es daher genau dann, wenn der erste oder letzte Tag des Jahres ein Donnerstag ist.

// Startwert : 1.1.2010 ist ein Freitag


	if ( dmonat ==0 )	// Wochenlogik
	{

		long starttag;

		starttag = ( jahr - 2010 ) * 365 ;
		if (jahr > 2036 ) starttag ++;
		if (jahr > 2032 ) starttag ++;
		if (jahr > 2028 ) starttag ++;
		if (jahr > 2024 ) starttag ++;
		if (jahr > 2020 ) starttag ++;
		if (jahr > 2016 ) starttag ++;
		if (jahr > 2012 ) starttag ++;
		starttag += 2 ; 	// Zeigt auf Sonntag, weil der absolute start ein Freitag ist 

		long startpunkt;
		startpunkt = ( starttag %7 )  + 5 ;	
		long himo = 1 ;
		if ( startpunkt < 4 ) startpunkt += 7 ;
		sprintf ( edstr01,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday01=startpunkt;edmon01=himo; edjr01 = jahr;
		startpunkt += 7;
		sprintf ( edstr02,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday02=startpunkt; edmon02=himo; edjr02 = jahr;
		startpunkt += 7;
		sprintf ( edstr03,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday03=startpunkt; edmon03=himo; edjr03 = jahr;
		startpunkt += 7;
		if ( startpunkt > 31 )
		{		startpunkt -= 31 ;	himo ++ ;

		}
		sprintf ( edstr04,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday04=startpunkt; edmon04=himo; edjr04 = jahr;
		startpunkt += 7;
		if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr05,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday05=startpunkt; edmon05=himo; edjr05 = jahr;
		startpunkt += 7;
		if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr06,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday06=startpunkt; edmon06=himo; edjr06 = jahr;
// Januar vorbei
		startpunkt += 7;
		sprintf ( edstr07,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday07=startpunkt; edmon07=himo; edjr07 = jahr;
		startpunkt += 7;
		if ( jahr == 2012 || jahr == 2016 || jahr == 2020 ||jahr == 2024 || jahr == 2028 ||jahr == 2032 || jahr == 2036 )
		{
			if ( startpunkt > 29 )
			{		startpunkt -= 29 ;	himo ++ ;
			}
		}
		else
		{
			if ( startpunkt > 28 )
			{	startpunkt -= 28 ;	himo ++ ;
			}
		}
		sprintf ( edstr08,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday08=startpunkt; edmon08=himo; edjr08 = jahr;
		startpunkt += 7;
		if ( jahr == 2012 || jahr == 2016 || jahr == 2020 ||jahr == 2024 || jahr == 2028 ||jahr == 2032 || jahr == 2036 )
		{
			if ( startpunkt > 29 )
			{	startpunkt -= 29 ;	himo ++ ;
			}
		}	
		else
		{
			if ( startpunkt > 28 )
			{	startpunkt -= 28 ;	himo ++ ;
			}
		}
		sprintf ( edstr09,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday09=startpunkt; edmon09=himo; edjr09 = jahr;
		startpunkt += 7;
		if ( jahr == 2012 || jahr == 2016 || jahr == 2020 ||jahr == 2024 || jahr == 2028 ||jahr == 2032 || jahr == 2036 )
		{
			if ( startpunkt > 29 )
			{	startpunkt -= 29 ;	himo ++ ;
			}
		}
		else
		{
			if ( startpunkt > 28 )
			{	startpunkt -= 28 ;	himo ++ ;
			}
		}
		sprintf ( edstr10,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday10=startpunkt; edmon10=himo; edjr10 = jahr;
// Februar vorbei
		startpunkt += 7;
		sprintf ( edstr11,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday11=startpunkt; edmon11=himo; edjr11 = jahr;
		startpunkt += 7;
		sprintf ( edstr12,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday12=startpunkt; edmon12=himo; edjr12 = jahr;
		startpunkt += 7;
		if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr13,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday13=startpunkt; edmon13=himo; edjr13 = jahr;
		startpunkt += 7;
		if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr14,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday14=startpunkt; edmon14=himo; edjr14 = jahr;
// M�rz vorbei 
		startpunkt += 7;
		sprintf ( edstr15,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday15=startpunkt; edmon15=himo; edjr15 = jahr;
		startpunkt += 7;
		sprintf ( edstr16,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday16=startpunkt; edmon16=himo; edjr16 = jahr;
		startpunkt += 7;
		if ( startpunkt > 30 )
		{	startpunkt -= 30 ;	himo ++ ;
		}
		sprintf ( edstr17,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday17=startpunkt; edmon17=himo; edjr17 = jahr;
		startpunkt += 7;
		if ( startpunkt > 30 )
		{	startpunkt -= 30 ;	himo ++ ;
		}
		sprintf ( edstr18,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday18=startpunkt; edmon18=himo; edjr18 = jahr;
		startpunkt += 7;
		if ( startpunkt > 30 )
		{	startpunkt -= 30 ;	himo ++ ;
		}
		sprintf ( edstr19,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday19=startpunkt; edmon19=himo; edjr19 = jahr;
// April vorbei
		startpunkt += 7;
		sprintf ( edstr20,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday20=startpunkt; edmon20=himo; edjr20 = jahr;
		startpunkt += 7;
		sprintf ( edstr21,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday21=startpunkt; edmon21=himo; edjr21 = jahr;
		startpunkt += 7;
		if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr22,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday22=startpunkt; edmon22=himo; edjr22 = jahr;
		startpunkt += 7;
				if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr23,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday23=startpunkt; edmon23=himo; edjr23 = jahr;
// Mai vorbei
		startpunkt += 7;
		sprintf ( edstr24,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday24=startpunkt; edmon24=himo; edjr24 = jahr;
		startpunkt += 7;
		sprintf ( edstr25,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday25=startpunkt; edmon25=himo; edjr25 = jahr;
		startpunkt += 7;
		if ( startpunkt > 30 )
		{	startpunkt -= 30 ;	himo ++ ;
		}
		sprintf ( edstr26,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday26=startpunkt; edmon26=himo; edjr26 = jahr;
		startpunkt += 7;
				if ( startpunkt > 30 )
		{	startpunkt -= 30 ;	himo ++ ;
		}
		sprintf ( edstr27,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday27=startpunkt; edmon27=himo; edjr27 = jahr;
		startpunkt += 7;
				if ( startpunkt > 30 )
		{	startpunkt -= 30 ;	himo ++ ;
		}
		sprintf ( edstr28,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday28=startpunkt; edmon28=himo; edjr28 = jahr;
// Juni vorbei
		startpunkt += 7;
		sprintf ( edstr29,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday29=startpunkt; edmon29=himo; edjr29 = jahr;
		startpunkt += 7;
		sprintf ( edstr30,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday30=startpunkt; edmon30=himo; edjr30 = jahr;
		startpunkt += 7;
		if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr31,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday31=startpunkt; edmon31=himo; edjr31 = jahr;
		startpunkt += 7;
				if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr32,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday32=startpunkt; edmon32=himo; edjr32 = jahr;
// Juli vorbei
		startpunkt += 7;
		sprintf ( edstr33,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday33=startpunkt; edmon33=himo; edjr33 = jahr;
		startpunkt += 7;
		sprintf ( edstr34,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday34=startpunkt; edmon34=himo; edjr34 = jahr;
		startpunkt += 7;
		if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr35,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday35=startpunkt; edmon35=himo; edjr35 = jahr;
		startpunkt += 7;
				if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr36,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday36=startpunkt; edmon36=himo; edjr36 = jahr;
// August vorbei
		startpunkt += 7;
		sprintf ( edstr37,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday37=startpunkt; edmon37=himo; edjr37 = jahr;
		startpunkt += 7;
		sprintf ( edstr38,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday38=startpunkt; edmon38=himo; edjr38 = jahr;
		startpunkt += 7;
		if ( startpunkt > 30 )
		{	startpunkt -= 30 ;	himo ++ ;
		}
		sprintf ( edstr39,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday39=startpunkt; edmon39=himo; edjr39 = jahr;
		startpunkt += 7;
				if ( startpunkt > 30 )
		{	startpunkt -= 30 ;	himo ++ ;
		}
		sprintf ( edstr40,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday40=startpunkt; edmon40=himo; edjr40 = jahr;
		startpunkt += 7;
				if ( startpunkt > 30 )
		{	startpunkt -= 30 ;	himo ++ ;
		}
		sprintf ( edstr41,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday41=startpunkt; edmon41=himo; edjr41 = jahr;
// September vorbei
		startpunkt += 7;
		sprintf ( edstr42,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday42=startpunkt; edmon42=himo; edjr42 = jahr;
		startpunkt += 7;
		sprintf ( edstr43,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday43=startpunkt; edmon43=himo; edjr43 = jahr;
		startpunkt += 7;
		if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr44,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday44=startpunkt; edmon44=himo; edjr44 = jahr;
		startpunkt += 7;
				if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo ++ ;
		}
		sprintf ( edstr45,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday45=startpunkt; edmon45=himo; edjr45 = jahr;
// Oktober vorbei
		startpunkt += 7;
		sprintf ( edstr46,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday46=startpunkt; edmon46=himo; edjr46 = jahr;
		startpunkt += 7;
		sprintf ( edstr47,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday47=startpunkt; edmon47=himo; edjr47 = jahr;
		startpunkt += 7;
		if ( startpunkt > 30 )
		{	startpunkt -= 30 ;	himo ++ ;
		}
		sprintf ( edstr48,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday48=startpunkt; edmon48=himo; edjr48 = jahr;
		startpunkt += 7;
				if ( startpunkt > 30 )
		{	startpunkt -= 30 ;	himo ++ ;
		}
		sprintf ( edstr49,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday49=startpunkt; edmon49=himo; edjr49 = jahr;
// November vorbei
		startpunkt += 7;
		sprintf ( edstr50,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday50=startpunkt; edmon50=himo; edjr50 = jahr;
		startpunkt += 7;
		sprintf ( edstr51,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday51=startpunkt; edmon51=himo; edjr51 = jahr;
		startpunkt += 7;
		if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo = 1 ;
			jahr++;
		}
		sprintf ( edstr52,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday52=startpunkt; edmon52=himo; edjr52 = jahr;
		startpunkt += 7;
				if ( startpunkt > 31 )
		{	startpunkt -= 31 ;	himo = 1 ;
			jahr++;
		}
		if ( startpunkt > 3 )	// Dann ist das die erste Woche des Folgejahres !!!
			wochenzahl = 52 ;
		else
			wochenzahl = 53 ;

		sprintf ( edstr53,"%02d.%02d.%04d", startpunkt, himo,jahr);
		edday53=startpunkt; edmon53=himo; edjr53 = jahr;
	}

}

void C15700Dlg::setzeperiode(char* perikz)
{

	if (perikz[0] == 'W' )
	{
		m_wo5.EnableWindow(TRUE);
		m_wo6.EnableWindow(TRUE);
		m_wo7.EnableWindow(TRUE);
		m_wo8.EnableWindow(TRUE);
		m_wo9.EnableWindow(TRUE);
		m_wo10.EnableWindow(TRUE);
		m_wo11.EnableWindow(TRUE);
		m_wo12.EnableWindow(TRUE);
		m_wo13.EnableWindow(TRUE);
		m_wo14.EnableWindow(TRUE);
		m_wo15.EnableWindow(TRUE);
		m_wo16.EnableWindow(TRUE);
		m_wo17.EnableWindow(TRUE);
		m_wo18.EnableWindow(TRUE);
		m_wo19.EnableWindow(TRUE);
		m_wo20.EnableWindow(TRUE);
		m_wo21.EnableWindow(TRUE);
		m_wo22.EnableWindow(TRUE);
		m_wo23.EnableWindow(TRUE);
		m_wo24.EnableWindow(TRUE);
		m_wo25.EnableWindow(TRUE);
		m_wo26.EnableWindow(TRUE);
		m_wo27.EnableWindow(TRUE);
		m_wo28.EnableWindow(TRUE);
		m_wo29.EnableWindow(TRUE);
		m_wo30.EnableWindow(TRUE);
		m_wo31.EnableWindow(TRUE);
		m_wo32.EnableWindow(TRUE);
		m_wo33.EnableWindow(TRUE);
		m_wo34.EnableWindow(TRUE);
		m_wo35.EnableWindow(TRUE);
		m_wo36.EnableWindow(TRUE);
		m_wo37.EnableWindow(TRUE);
		m_wo38.EnableWindow(TRUE);
		m_wo39.EnableWindow(TRUE);
		m_wo40.EnableWindow(TRUE);
		m_wo41.EnableWindow(TRUE);
		m_wo42.EnableWindow(TRUE);
		m_wo43.EnableWindow(TRUE);
		m_wo44.EnableWindow(TRUE);
		m_wo45.EnableWindow(TRUE);
		m_wo46.EnableWindow(TRUE);
		m_wo47.EnableWindow(TRUE);
		m_wo48.EnableWindow(TRUE);
		m_wo49.EnableWindow(TRUE);
		m_wo50.EnableWindow(TRUE);
		m_wo51.EnableWindow(TRUE);
		m_wo52.EnableWindow(TRUE);
		m_wo53.EnableWindow(TRUE);
		return;
	}
	if (perikz[0] == 'M' )
	{
		m_wo13.EnableWindow(FALSE);
		m_wo14.EnableWindow(FALSE);
		m_wo15.EnableWindow(FALSE);
		m_wo16.EnableWindow(FALSE);
		m_wo17.EnableWindow(FALSE);
		m_wo18.EnableWindow(FALSE);
		m_wo19.EnableWindow(FALSE);
		m_wo20.EnableWindow(FALSE);
		m_wo21.EnableWindow(FALSE);
		m_wo22.EnableWindow(FALSE);
		m_wo23.EnableWindow(FALSE);
		m_wo24.EnableWindow(FALSE);
		m_wo25.EnableWindow(FALSE);
		m_wo26.EnableWindow(FALSE);
		m_wo27.EnableWindow(FALSE);
		m_wo28.EnableWindow(FALSE);
		m_wo29.EnableWindow(FALSE);
		m_wo30.EnableWindow(FALSE);
		m_wo31.EnableWindow(FALSE);
		m_wo32.EnableWindow(FALSE);
		m_wo33.EnableWindow(FALSE);
		m_wo34.EnableWindow(FALSE);
		m_wo35.EnableWindow(FALSE);
		m_wo36.EnableWindow(FALSE);
		m_wo37.EnableWindow(FALSE);
		m_wo38.EnableWindow(FALSE);
		m_wo39.EnableWindow(FALSE);
		m_wo40.EnableWindow(FALSE);
		m_wo41.EnableWindow(FALSE);
		m_wo42.EnableWindow(FALSE);
		m_wo43.EnableWindow(FALSE);
		m_wo44.EnableWindow(FALSE);
		m_wo45.EnableWindow(FALSE);
		m_wo46.EnableWindow(FALSE);
		m_wo47.EnableWindow(FALSE);
		m_wo48.EnableWindow(FALSE);
		m_wo49.EnableWindow(FALSE);
		m_wo50.EnableWindow(FALSE);
		m_wo51.EnableWindow(FALSE);
		m_wo52.EnableWindow(FALSE);
		m_wo53.EnableWindow(FALSE);

		v_wo13="";
		v_wo14="";
		v_wo15="";
		v_wo16="";
		v_wo17="";
		v_wo18="";
		v_wo19="";
		v_wo20="";
		v_wo21="";
		v_wo22="";
		v_wo23="";
		v_wo24="";
		v_wo25="";
		v_wo26="";
		v_wo27="";
		v_wo28="";
		v_wo29="";
		v_wo30="";
		v_wo31="";
		v_wo32="";
		v_wo33="";
		v_wo34="";
		v_wo35="";
		v_wo36="";
		v_wo37="";
		v_wo38="";
		v_wo39="";
		v_wo40="";
		v_wo41="";
		v_wo42="";
		v_wo43="";
		v_wo44="";
		v_wo45="";
		v_wo46="";
		v_wo47="";
		v_wo48="";
		v_wo49="";
		v_wo50="";
		v_wo51="";
		v_wo52="";
		v_wo53="";


		return;
	}

	if (perikz[0] == 'Q' )


	{
		m_wo5.EnableWindow(FALSE);
		m_wo6.EnableWindow(FALSE);
		m_wo7.EnableWindow(FALSE);
		m_wo8.EnableWindow(FALSE);
		m_wo9.EnableWindow(FALSE);
		m_wo10.EnableWindow(FALSE);
		m_wo11.EnableWindow(FALSE);
		m_wo12.EnableWindow(FALSE);
		m_wo13.EnableWindow(FALSE);
		m_wo14.EnableWindow(FALSE);
		m_wo15.EnableWindow(FALSE);
		m_wo16.EnableWindow(FALSE);
		m_wo17.EnableWindow(FALSE);
		m_wo18.EnableWindow(FALSE);
		m_wo19.EnableWindow(FALSE);
		m_wo20.EnableWindow(FALSE);
		m_wo21.EnableWindow(FALSE);
		m_wo22.EnableWindow(FALSE);
		m_wo23.EnableWindow(FALSE);
		m_wo24.EnableWindow(FALSE);
		m_wo25.EnableWindow(FALSE);
		m_wo26.EnableWindow(FALSE);
		m_wo27.EnableWindow(FALSE);
		m_wo28.EnableWindow(FALSE);
		m_wo29.EnableWindow(FALSE);
		m_wo30.EnableWindow(FALSE);
		m_wo31.EnableWindow(FALSE);
		m_wo32.EnableWindow(FALSE);
		m_wo33.EnableWindow(FALSE);
		m_wo34.EnableWindow(FALSE);
		m_wo35.EnableWindow(FALSE);
		m_wo36.EnableWindow(FALSE);
		m_wo37.EnableWindow(FALSE);
		m_wo38.EnableWindow(FALSE);
		m_wo39.EnableWindow(FALSE);
		m_wo40.EnableWindow(FALSE);
		m_wo41.EnableWindow(FALSE);
		m_wo42.EnableWindow(FALSE);
		m_wo43.EnableWindow(FALSE);
		m_wo44.EnableWindow(FALSE);
		m_wo45.EnableWindow(FALSE);
		m_wo46.EnableWindow(FALSE);
		m_wo47.EnableWindow(FALSE);
		m_wo48.EnableWindow(FALSE);
		m_wo49.EnableWindow(FALSE);
		m_wo50.EnableWindow(FALSE);
		m_wo51.EnableWindow(FALSE);
		m_wo52.EnableWindow(FALSE);
		m_wo53.EnableWindow(FALSE);

		v_wo5="";
		v_wo6="";
		v_wo7="";
		v_wo8="";
		v_wo9="";
		v_wo10="";
		v_wo11="";
		v_wo12="";
		v_wo13="";
		v_wo14="";
		v_wo15="";
		v_wo16="";
		v_wo17="";
		v_wo18="";
		v_wo19="";
		v_wo20="";
		v_wo21="";
		v_wo22="";
		v_wo23="";
		v_wo24="";
		v_wo25="";
		v_wo26="";
		v_wo27="";
		v_wo28="";
		v_wo29="";
		v_wo30="";
		v_wo31="";
		v_wo32="";
		v_wo33="";
		v_wo34="";
		v_wo35="";
		v_wo36="";
		v_wo37="";
		v_wo38="";
		v_wo39="";
		v_wo40="";
		v_wo41="";
		v_wo42="";
		v_wo43="";
		v_wo44="";
		v_wo45="";
		v_wo46="";
		v_wo47="";
		v_wo48="";
		v_wo49="";
		v_wo50="";
		v_wo51="";
		v_wo52="";
		v_wo53="";

		return;
	}

}

BOOL C15700Dlg::satzlesen(void)
{	// Bitte immer den Schl�ssel vorher komplett eintragen .........

   fil_abrech.jr = (short) v_jahr;
// fil_abrech.mdn ist definiert
// fil_abrech.fil ist definiert
// fil_abrech.period_kz ist definiert 


	char defaultproz[20];
	// if ( dmonat == 0 ) -> Not-default
		sprintf (defaultproz,"1,92 %%%%");
	if ( dmonat == 1 )
		sprintf (defaultproz,"8,33 %%%%");
	if ( dmonat == 2 )
		sprintf (defaultproz,"25,00 %%%%");
	
	generiereenddatum(fil_abrech.jr, dmonat );

	int di = Fil_abrech.openfil_abrech();

	di = Fil_abrech.lesefil_abrech();

// return FALSE	// es gibt noch nichts zu diesem Zeitraum


	if ( di ) fil_abrech.period = 0;


	for ( int i = 1 ; i < 54 ; i ++ )
	{

		switch ( i )
		{
		case 1 : 
			if ( di || fil_abrech.period != 1 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo1.Format( _T(edstr01));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo1.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo1.Format( _T("0,00"));
					}
				}
				s_plan_proz01 = 0;
				s_plan_euro01 = 0;
				s_end_dat01.day = 0 - edday01;	// negativ = leer
				s_end_dat01.month = edmon01;
				s_end_dat01.year = edjr01;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}
// 			if ( fil_abrech.period == 1 )	nur diese kommt hier an .....
//			{	// Werte aus Daba speichern

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo1.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo1.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo1.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz01 = fil_abrech.plan_proz;
			s_plan_euro01 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat01,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;
//			}

		case 2 : 
			if ( di || fil_abrech.period != 2 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo2.Format( _T(edstr02));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo2.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo2.Format( _T("0,00"));
					}
				}
				s_plan_proz02 = 0;
				s_plan_euro02 = 0;
				s_end_dat02.day = 0 - edday02;
				s_end_dat02.month = edmon02;
				s_end_dat02.year = edjr02;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo2.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo2.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo2.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz02 = fil_abrech.plan_proz;
			s_plan_euro02 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat02,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 3 : 
			if ( di || fil_abrech.period != 3 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo3.Format( _T(edstr03));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo3.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo3.Format( _T("0,00"));
					}
				}
				s_plan_proz03 = 0;
				s_plan_euro03 = 0;
				s_end_dat03.day = 0 - edday03;
				s_end_dat03.month = edmon03;
				s_end_dat03.year = edjr03;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo3.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo3.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo3.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz03 = fil_abrech.plan_proz;
			s_plan_euro03 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat03,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;


		case 4 : 
			if ( di || fil_abrech.period != 4 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo4.Format( _T(edstr04));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo4.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo4.Format( _T("0,00"));
					}
				}
				s_plan_proz04 = 0;
				s_plan_euro04 = 0;
				s_end_dat04.day = 0 - edday04;
				s_end_dat04.month = edmon04;
				s_end_dat04.year = edjr04;

				if ( dmonat == 2 ) i = 99 ;
				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo4.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo4.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo4.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz04 = fil_abrech.plan_proz;
			s_plan_euro04 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat04,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			if ( dmonat == 2) i = 99 ;	// Quartal
			break;

		case 5 : 
			if ( di || fil_abrech.period != 5 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo5.Format( _T(edstr05));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo5.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo5.Format( _T("0,00"));
					}
				}
				s_plan_proz05 = 0;
				s_plan_euro05 = 0;
				s_end_dat05.day = 0 - edday05;
				s_end_dat05.month = edmon05;
				s_end_dat05.year = edjr05;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo5.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo5.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo5.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz05 = fil_abrech.plan_proz;
			s_plan_euro05 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat05,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 6 : 
			if ( di || fil_abrech.period != 6 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo6.Format( _T(edstr06));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo6.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo6.Format( _T("0,00"));
					}
				}
				s_plan_proz06 = 0;
				s_plan_euro06 = 0;
				s_end_dat06.day = 0 - edday06;
				s_end_dat06.month = edmon06;
				s_end_dat06.year = edjr06;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo6.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo6.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo6.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz06 = fil_abrech.plan_proz;
			s_plan_euro06 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat06,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 7 : 
			if ( di || fil_abrech.period != 7 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo7.Format( _T(edstr07));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo7.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo7.Format( _T("0,00"));
					}
				}
				s_plan_proz07 = 0;
				s_plan_euro07 = 0;
				s_end_dat07.day = 0 - edday07;
				s_end_dat07.month = edmon07;
				s_end_dat07.year = edjr07;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo7.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo7.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo7.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz07 = fil_abrech.plan_proz;
			s_plan_euro07 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat07,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 8 : 
			if ( di || fil_abrech.period != 8 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo8.Format( _T(edstr08));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo8.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo8.Format( _T("0,00"));
					}
				}
				s_plan_proz08 = 0;
				s_plan_euro08 = 0;
				s_end_dat08.day = 0 - edday08;
				s_end_dat08.month = edmon08;
				s_end_dat08.year = edjr08;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo8.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo8.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo8.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz08 = fil_abrech.plan_proz;
			s_plan_euro08 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat08,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 9 : 
			if ( di || fil_abrech.period != 9 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo9.Format( _T(edstr09));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo9.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo9.Format( _T("0,00"));
					}
				}
				s_plan_proz09 = 0;
				s_plan_euro09 = 0;
				s_end_dat09.day = 0 - edday09;
				s_end_dat09.month = edmon09;
				s_end_dat09.year = edjr09;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo9.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo9.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo9.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz09 = fil_abrech.plan_proz;
			s_plan_euro09 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat09,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 10 : 
			if ( di || fil_abrech.period != 10 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo10.Format( _T(edstr10));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo10.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo10.Format( _T("0,00"));
					}
				}
				s_plan_proz10 = 0;
				s_plan_euro10 = 0;
				s_end_dat10.day = 0 - edday10;
				s_end_dat10.month = edmon10;
				s_end_dat10.year = edjr10;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo10.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo10.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo10.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz10 = fil_abrech.plan_proz;
			s_plan_euro10 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat10,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 11 : 
			if ( di || fil_abrech.period != 11 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo11.Format( _T(edstr11));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo11.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo11.Format( _T("0,00"));
					}
				}
				s_plan_proz11 = 0;
				s_plan_euro11 = 0;
				s_end_dat11.day = 0 - edday11;
				s_end_dat11.month = edmon11;
				s_end_dat11.year = edjr11;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo11.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo11.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo11.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz11 = fil_abrech.plan_proz;
			s_plan_euro11 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat11,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 12 : 
			if ( di || fil_abrech.period != 12 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo12.Format( _T(edstr12));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo12.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo12.Format( _T("0,00"));
					}
				}
				s_plan_proz12 = 0;
				s_plan_euro12 = 0;
				s_end_dat12.day = 0 - edday12;
				s_end_dat12.month = edmon12;
				s_end_dat12.year = edjr12;

				if ( dmonat == 1 ) i = 99 ;
				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo12.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo12.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo12.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz12 = fil_abrech.plan_proz;
			s_plan_euro12 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat12,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			if ( dmonat == 1 ) i = 99 ;
			break;

		case 13 : 
			if ( di || fil_abrech.period != 13 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo13.Format( _T(edstr13));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo13.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo13.Format( _T("0,00"));
					}
				}
				s_plan_proz13 = 0;
				s_plan_euro13 = 0;
				s_end_dat13.day = 0 - edday13;
				s_end_dat13.month = edmon13;
				s_end_dat13.year = edjr13;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo13.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo13.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo13.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz13 = fil_abrech.plan_proz;
			s_plan_euro13 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat13,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 14 : 
			if ( di || fil_abrech.period != 14 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo14.Format( _T(edstr14));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo14.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo14.Format( _T("0,00"));
					}
				}
				s_plan_proz14 = 0;
				s_plan_euro14 = 0;
				s_end_dat14.day = 0 - edday14;
				s_end_dat14.month = edmon14;
				s_end_dat14.year = edjr14;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo14.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo14.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo14.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz14 = fil_abrech.plan_proz;
			s_plan_euro14 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat14,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 15 : 
			if ( di || fil_abrech.period != 15 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo15.Format( _T(edstr15));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo15.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo15.Format( _T("0,00"));
					}
				}
				s_plan_proz15 = 0;
				s_plan_euro15 = 0;
				s_end_dat15.day = 0 - edday15;
				s_end_dat15.month = edmon15;
				s_end_dat15.year = edjr15;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo15.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo15.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo15.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz15 = fil_abrech.plan_proz;
			s_plan_euro15 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat15,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 16 : 
			if ( di || fil_abrech.period != 16 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo16.Format( _T(edstr16));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo16.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo16.Format( _T("0,00"));
					}
				}
				s_plan_proz16 = 0;
				s_plan_euro16 = 0;
				s_end_dat16.day = 0 - edday16;
				s_end_dat16.month = edmon16;
				s_end_dat16.year = edjr16;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo16.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo16.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo16.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz16 = fil_abrech.plan_proz;
			s_plan_euro16 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat16,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 17 : 
			if ( di || fil_abrech.period != 17 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo17.Format( _T(edstr17));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo17.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo17.Format( _T("0,00"));
					}
				}
				s_plan_proz17 = 0;
				s_plan_euro17 = 0;
				s_end_dat17.day = 0 - edday17;
				s_end_dat17.month = edmon17;
				s_end_dat17.year = edjr17;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo17.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo17.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo17.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz17 = fil_abrech.plan_proz;
			s_plan_euro17 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat17,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 18 : 
			if ( di || fil_abrech.period != 18 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo18.Format( _T(edstr18));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo18.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo18.Format( _T("0,00"));
					}
				}
				s_plan_proz18 = 0;
				s_plan_euro18 = 0;
				s_end_dat18.day = 0 - edday18;
				s_end_dat18.month = edmon18;
				s_end_dat18.year = edjr18;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo18.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo18.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo18.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz18 = fil_abrech.plan_proz;
			s_plan_euro18 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat18,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 19 : 
			if ( di || fil_abrech.period != 19 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo19.Format( _T(edstr19));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo19.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo19.Format( _T("0,00"));
					}
				}
				s_plan_proz19 = 0;
				s_plan_euro19 = 0;
				s_end_dat19.day = 0 - edday19;
				s_end_dat19.month = edmon19;
				s_end_dat19.year = edjr19;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo19.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo19.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo19.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz19 = fil_abrech.plan_proz;
			s_plan_euro19 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat19,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 20 : 
			if ( di || fil_abrech.period != 20 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo20.Format( _T(edstr20));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo20.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo20.Format( _T("0,00"));
					}
				}
				s_plan_proz20 = 0;
				s_plan_euro20 = 0;
				s_end_dat20.day = 0 - edday20;
				s_end_dat20.month = edmon20;
				s_end_dat20.year = edjr20;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo20.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo20.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo20.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz20 = fil_abrech.plan_proz;
			s_plan_euro20 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat20,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 21 : 
			if ( di || fil_abrech.period != 21 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo21.Format( _T(edstr21));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo21.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo21.Format( _T("0,00"));
					}
				}
				s_plan_proz21 = 0;
				s_plan_euro21 = 0;
				s_end_dat21.day = 0 - edday21;
				s_end_dat21.month = edmon21;
				s_end_dat21.year = edjr21;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo21.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo21.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo21.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz21 = fil_abrech.plan_proz;
			s_plan_euro21 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat21,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 22 : 
			if ( di || fil_abrech.period != 22 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo22.Format( _T(edstr22));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo22.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo22.Format( _T("0,00"));
					}
				}
				s_plan_proz22 = 0;
				s_plan_euro22 = 0;
				s_end_dat22.day = 0 - edday22;
				s_end_dat22.month = edmon22;
				s_end_dat22.year = edjr22;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo22.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo22.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo22.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz22 = fil_abrech.plan_proz;
			s_plan_euro22 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat22,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 23 : 
			if ( di || fil_abrech.period != 23 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo23.Format( _T(edstr23));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo23.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo23.Format( _T("0,00"));
					}
				}
				s_plan_proz23 = 0;
				s_plan_euro23 = 0;
				s_end_dat23.day = 0 - edday23;
				s_end_dat23.month = edmon23;
				s_end_dat23.year = edjr23;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo23.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo23.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo23.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz23 = fil_abrech.plan_proz;
			s_plan_euro23 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat23,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 24 : 
			if ( di || fil_abrech.period != 24 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo24.Format( _T(edstr24));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo24.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo24.Format( _T("0,00"));
					}
				}
				s_plan_proz24 = 0;
				s_plan_euro24 = 0;
				s_end_dat24.day = 0 - edday24;
				s_end_dat24.month = edmon24;
				s_end_dat24.year = edjr24;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo24.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo24.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo24.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz24 = fil_abrech.plan_proz;
			s_plan_euro24 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat24,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 25 : 
			if ( di || fil_abrech.period != 25 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo25.Format( _T(edstr25));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo25.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo25.Format( _T("0,00"));
					}
				}
				s_plan_proz25 = 0;
				s_plan_euro25 = 0;
				s_end_dat25.day = 0 - edday25;
				s_end_dat25.month = edmon25;
				s_end_dat25.year = edjr25;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo25.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo25.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo25.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz25 = fil_abrech.plan_proz;
			s_plan_euro25 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat25,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 26 : 
			if ( di || fil_abrech.period != 26 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo26.Format( _T(edstr26));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo26.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo26.Format( _T("0,00"));
					}
				}
				s_plan_proz26 = 0;
				s_plan_euro26 = 0;
				s_end_dat26.day = 0 - edday26;
				s_end_dat26.month = edmon26;
				s_end_dat26.year = edjr26;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo26.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo26.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo26.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz26 = fil_abrech.plan_proz;
			s_plan_euro26 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat26,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 27 : 
			if ( di || fil_abrech.period != 27 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo27.Format( _T(edstr27));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo27.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo27.Format( _T("0,00"));
					}
				}
				s_plan_proz27 = 0;
				s_plan_euro27 = 0;
				s_end_dat27.day = 0 - edday27;
				s_end_dat27.month = edmon27;
				s_end_dat27.year = edjr27;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo27.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo27.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo27.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz27 = fil_abrech.plan_proz;
			s_plan_euro27 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat27,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 28 : 
			if ( di || fil_abrech.period != 28 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo28.Format( _T(edstr28));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo28.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo28.Format( _T("0,00"));
					}
				}
				s_plan_proz28 = 0;
				s_plan_euro28 = 0;
				s_end_dat28.day = 0 - edday28;
				s_end_dat28.month = edmon28;
				s_end_dat28.year = edjr28;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo28.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo28.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo28.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz28 = fil_abrech.plan_proz;
			s_plan_euro28 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat28,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 29 : 
			if ( di || fil_abrech.period != 29 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo29.Format( _T(edstr29));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo29.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo29.Format( _T("0,00"));
					}
				}
				s_plan_proz29 = 0;
				s_plan_euro29 = 0;
				s_end_dat29.day = 0 - edday29;
				s_end_dat29.month = edmon29;
				s_end_dat29.year = edjr29;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo29.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo29.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo29.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz29 = fil_abrech.plan_proz;
			s_plan_euro29 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat29,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 30 : 
			if ( di || fil_abrech.period != 30 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo30.Format( _T(edstr30));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo30.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo30.Format( _T("0,00"));
					}
				}
				s_plan_proz30 = 0;
				s_plan_euro30 = 0;
				s_end_dat30.day = 0 - edday30;
				s_end_dat30.month = edmon30;
				s_end_dat30.year = edjr30;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo30.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo30.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo30.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz30 = fil_abrech.plan_proz;
			s_plan_euro30 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat30,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 31 : 
			if ( di || fil_abrech.period != 31 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo31.Format( _T(edstr31));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo31.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo31.Format( _T("0,00"));
					}
				}
				s_plan_proz31 = 0;
				s_plan_euro31 = 0;
				s_end_dat31.day = 0 - edday31;
				s_end_dat31.month = edmon31;
				s_end_dat31.year = edjr31;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo31.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo31.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo31.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz31 = fil_abrech.plan_proz;
			s_plan_euro31 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat31,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 32 : 
			if ( di || fil_abrech.period != 32 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo32.Format( _T(edstr32));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo32.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo32.Format( _T("0,00"));
					}
				}
				s_plan_proz32 = 0;
				s_plan_euro32 = 0;
				s_end_dat32.day = 0 - edday32;
				s_end_dat32.month = edmon32;
				s_end_dat32.year = edjr32;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo32.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo32.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo32.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz32 = fil_abrech.plan_proz;
			s_plan_euro32 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat32,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 33 : 
			if ( di || fil_abrech.period != 33 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo33.Format( _T(edstr33));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo33.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo33.Format( _T("0,00"));
					}
				}
				s_plan_proz33 = 0;
				s_plan_euro33 = 0;
				s_end_dat33.day = 0 - edday33;
				s_end_dat33.month = edmon33;
				s_end_dat33.year = edjr33;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo33.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo33.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo33.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz33 = fil_abrech.plan_proz;
			s_plan_euro33 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat33,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 34 : 
			if ( di || fil_abrech.period != 34 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo34.Format( _T(edstr34));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo34.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo34.Format( _T("0,00"));
					}
				}
				s_plan_proz34 = 0;
				s_plan_euro34 = 0;
				s_end_dat34.day = 0 - edday34;
				s_end_dat34.month = edmon34;
				s_end_dat34.year = edjr34;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo34.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo34.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo34.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz34 = fil_abrech.plan_proz;
			s_plan_euro34 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat34,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 35 : 
			if ( di || fil_abrech.period != 35 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo35.Format( _T(edstr35));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo35.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo35.Format( _T("0,00"));
					}
				}
				s_plan_proz35 = 0;
				s_plan_euro35 = 0;
				s_end_dat35.day = 0 - edday35;
				s_end_dat35.month = edmon35;
				s_end_dat35.year = edjr35;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo35.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo35.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo35.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz35 = fil_abrech.plan_proz;
			s_plan_euro35 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat35,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 36 : 
			if ( di || fil_abrech.period != 36 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo36.Format( _T(edstr36));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo36.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo36.Format( _T("0,00"));
					}
				}
				s_plan_proz36 = 0;
				s_plan_euro36 = 0;
				s_end_dat36.day = 0 - edday36;
				s_end_dat36.month = edmon36;
				s_end_dat36.year = edjr36;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo36.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo36.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo36.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz36 = fil_abrech.plan_proz;
			s_plan_euro36 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat36,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 37 : 
			if ( di || fil_abrech.period != 37 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo37.Format( _T(edstr37));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo37.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo37.Format( _T("0,00"));
					}
				}
				s_plan_proz37 = 0;
				s_plan_euro37 = 0;
				s_end_dat37.day = 0 - edday37;
				s_end_dat37.month = edmon37;
				s_end_dat37.year = edjr37;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo37.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo37.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo37.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz37 = fil_abrech.plan_proz;
			s_plan_euro37 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat37,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 38 : 
			if ( di || fil_abrech.period != 38 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo38.Format( _T(edstr38));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo38.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo38.Format( _T("0,00"));
					}
				}
				s_plan_proz38 = 0;
				s_plan_euro38 = 0;
				s_end_dat38.day = 0 - edday38;
				s_end_dat38.month = edmon38;
				s_end_dat38.year = edjr38;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo38.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo38.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo38.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz38 = fil_abrech.plan_proz;
			s_plan_euro38 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat38,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 39 : 
			if ( di || fil_abrech.period != 39 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo39.Format( _T(edstr39));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo39.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo39.Format( _T("0,00"));
					}
				}
				s_plan_proz39 = 0;
				s_plan_euro39 = 0;
				s_end_dat39.day = 0 - edday39;
				s_end_dat39.month = edmon39;
				s_end_dat39.year = edjr39;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo39.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo39.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo39.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz39 = fil_abrech.plan_proz;
			s_plan_euro39 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat39,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 40 : 
			if ( di || fil_abrech.period != 40 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo40.Format( _T(edstr40));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo40.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo40.Format( _T("0,00"));
					}
				}
				s_plan_proz40 = 0;
				s_plan_euro40 = 0;
				s_end_dat40.day = 0 - edday40;
				s_end_dat40.month = edmon40;
				s_end_dat40.year = edjr40;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo40.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo40.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo40.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz40 = fil_abrech.plan_proz;
			s_plan_euro40 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat40,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 41 : 
			if ( di || fil_abrech.period != 41 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo41.Format( _T(edstr41));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo41.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo41.Format( _T("0,00"));
					}
				}
				s_plan_proz41 = 0;
				s_plan_euro41 = 0;
				s_end_dat41.day = 0 - edday41;
				s_end_dat41.month = edmon41;
				s_end_dat41.year = edjr41;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo41.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo41.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo41.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz41 = fil_abrech.plan_proz;
			s_plan_euro41 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat41,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 42 : 
			if ( di || fil_abrech.period != 42 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo42.Format( _T(edstr42));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo42.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo42.Format( _T("0,00"));
					}
				}
				s_plan_proz42 = 0;
				s_plan_euro42 = 0;
				s_end_dat42.day = 0 - edday42;
				s_end_dat42.month = edmon42;
				s_end_dat42.year = edjr42;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo42.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo42.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo42.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz42 = fil_abrech.plan_proz;
			s_plan_euro42 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat42,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 43 : 
			if ( di || fil_abrech.period != 43 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo43.Format( _T(edstr43));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo43.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo43.Format( _T("0,00"));
					}
				}
				s_plan_proz43 = 0;
				s_plan_euro43 = 0;
				s_end_dat43.day = 0 - edday43;
				s_end_dat43.month = edmon43;
				s_end_dat43.year = edjr43;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo43.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo43.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo43.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz43 = fil_abrech.plan_proz;
			s_plan_euro43 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat43,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 44 : 
			if ( di || fil_abrech.period != 44 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo44.Format( _T(edstr44));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo44.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo44.Format( _T("0,00"));
					}
				}
				s_plan_proz44 = 0;
				s_plan_euro44 = 0;
				s_end_dat44.day = 0 - edday44;
				s_end_dat44.month = edmon44;
				s_end_dat44.year = edjr44;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo44.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo44.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo44.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz44 = fil_abrech.plan_proz;
			s_plan_euro44 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat44,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 45 : 
			if ( di || fil_abrech.period != 45 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo45.Format( _T(edstr45));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo45.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo45.Format( _T("0,00"));
					}
				}
				s_plan_proz45 = 0;
				s_plan_euro45 = 0;
				s_end_dat45.day = 0 - edday45;
				s_end_dat45.month = edmon45;
				s_end_dat45.year = edjr45;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo45.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo45.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo45.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz45 = fil_abrech.plan_proz;
			s_plan_euro45 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat45,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 46 : 
			if ( di || fil_abrech.period != 46 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo46.Format( _T(edstr46));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo46.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo46.Format( _T("0,00"));
					}
				}
				s_plan_proz46 = 0;
				s_plan_euro46 = 0;
				s_end_dat46.day = 0 - edday46;
				s_end_dat46.month = edmon46;
				s_end_dat46.year = edjr46;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo46.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo46.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo46.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz46 = fil_abrech.plan_proz;
			s_plan_euro46 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat46,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 47 : 
			if ( di || fil_abrech.period != 47 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo47.Format( _T(edstr47));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo47.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo47.Format( _T("0,00"));
					}
				}
				s_plan_proz47 = 0;
				s_plan_euro47 = 0;
				s_end_dat47.day = 0 - edday47;
				s_end_dat47.month = edmon47;
				s_end_dat47.year = edjr47;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo47.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo47.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo47.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz47 = fil_abrech.plan_proz;
			s_plan_euro47 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat47,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 48 : 
			if ( di || fil_abrech.period != 48 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo48.Format( _T(edstr48));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo48.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo48.Format( _T("0,00"));
					}
				}
				s_plan_proz48 = 0;
				s_plan_euro48 = 0;
				s_end_dat48.day = 0 - edday48;
				s_end_dat48.month = edmon48;
				s_end_dat48.year = edjr48;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo48.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo48.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo48.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz48 = fil_abrech.plan_proz;
			s_plan_euro48 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat48,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 49 : 
			if ( di || fil_abrech.period != 49 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo49.Format( _T(edstr49));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo49.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo49.Format( _T("0,00"));
					}
				}
				s_plan_proz49 = 0;
				s_plan_euro49 = 0;
				s_end_dat49.day = 0 - edday49;
				s_end_dat49.month = edmon49;
				s_end_dat49.year = edjr49;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo49.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo49.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo49.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz49 = fil_abrech.plan_proz;
			s_plan_euro49 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat49,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 50 : 
			if ( di || fil_abrech.period != 50 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo50.Format( _T(edstr50));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo50.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo50.Format( _T("0,00"));
					}
				}
				s_plan_proz50 = 0;
				s_plan_euro50 = 0;
				s_end_dat50.day = 0 - edday50;
				s_end_dat50.month = edmon50;
				s_end_dat50.year = edjr50;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo50.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo50.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo50.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz50 = fil_abrech.plan_proz;
			s_plan_euro50 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat50,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 51 : 
			if ( di || fil_abrech.period != 51 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo51.Format( _T(edstr51));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo51.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo51.Format( _T("0,00"));
					}
				}
				s_plan_proz51 = 0;
				s_plan_euro51 = 0;
				s_end_dat51.day = 0 - edday51;
				s_end_dat51.month = edmon51;
				s_end_dat51.year = edjr51;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo51.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo51.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo51.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz51 = fil_abrech.plan_proz;
			s_plan_euro51 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat51,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 52 : 
			if ( di || fil_abrech.period != 52 )
			{
				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo52.Format( _T(edstr52));
				}
				else	// "Preismodi"
				{
					if ( dmodus == 1 )
					{	// Prozente verteilen 
							v_wo52.Format( _T(defaultproz));
					}
					if ( dmodus == 2 )
					{
						v_wo52.Format( _T("0,00"));
					}
				}
				s_plan_proz52 = 0;
				s_plan_euro52 = 0;
				s_end_dat52.day = 0 - edday52;
				s_end_dat25.month = edmon52;
				s_end_dat52.year = edjr52;

				break;	// passt eigentlich immer, da ja sortiert eingelesen wird
			}

			if ( dmodus == 0 )	// Enddatum vorschlagen
			{
				v_wo52.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
			}
			if ( dmodus == 1 )
			{	// Prozente
				v_wo52.Format( "%02.2f %%", fil_abrech.plan_proz);
			}
			if ( dmodus == 2 )
			{
				v_wo52.Format( "%02.2f T", fil_abrech.plan_eur);
			}
			s_plan_proz52 = fil_abrech.plan_proz;
			s_plan_euro52 = fil_abrech.plan_eur;
			memcpy ( &s_end_dat52,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
			di = Fil_abrech.lesefil_abrech();
			break;

		case 53 : 
			// hier kommt man ja nur im Wochenmodus vorbei .........
			if ( wochenzahl == 53 )
			{	m_wo53.EnableWindow(TRUE);
				if ( di || fil_abrech.period != 53 )
				{
					if ( dmodus == 0 )	// Enddatum vorschlagen
					{
						v_wo53.Format( _T(edstr53));
					}
					else	// "Preismodi"
					{
						if ( dmodus == 1 )
						{	// Prozente verteilen 
							v_wo53.Format( _T(defaultproz));
						}
						if ( dmodus == 2 )
						{
							v_wo53.Format( _T("0,00"));
						}
					}
					s_plan_proz53 = 0;
					s_plan_euro53 = 0;
				s_end_dat53.day = 0 - edday53;
				s_end_dat53.month = edmon53;
				s_end_dat53.year = edjr53;
					break;	// passt eigentlich immer, da ja sortiert eingelesen wird
				}

				if ( dmodus == 0 )	// Enddatum vorschlagen
				{
					v_wo53.Format( "%02d.%02d.%04d", fil_abrech.end_dat.day, fil_abrech.end_dat.month,fil_abrech.end_dat.year);
				}
				if ( dmodus == 1 )
				{	// Prozente
					v_wo53.Format( "%02.2f %%", fil_abrech.plan_proz);
				}
				if ( dmodus == 2 )
				{
					v_wo53.Format( "%02.2f T", fil_abrech.plan_eur);
				}
				s_plan_proz53 = fil_abrech.plan_proz;
				s_plan_euro53 = fil_abrech.plan_eur;
				memcpy ( &s_end_dat53,  & fil_abrech.end_dat, sizeof ( TIMESTAMP_STRUCT));
				di = Fil_abrech.lesefil_abrech();
				// red. break;
			}
			else	// Das Jahr hat nur 52 Wochen .....
			{
				m_wo53.EnableWindow(FALSE);
				v_wo53.Format("");
				// red. break;
			}
			break;

		}
	}

	return TRUE ;
}

BOOL C15700Dlg::PreTranslateMessage(LPMSG lpMsg)
{
	CWnd *cWnd;
	 if (lpMsg->message == WM_KEYDOWN)
	 {
		 switch (lpMsg->wParam)
		 {
		 
		case VK_RETURN :
			cWnd = GetFocus ();
			if(cWnd == GetDlgItem (IDOK))
				return CDialog::PreTranslateMessage(lpMsg);
			if(cWnd == GetDlgItem (IDCANCEL))
			{
					OnCancel ();
 					return TRUE;
			}

/* --->
			           if(cWnd == GetDlgItem (IDC_KUNN))
					   {	Read (); return TRUE; }
                       else if (cWnd == GetDlgItem (IDC_MDN))
                       { if (ReadMdn () == FALSE)
                       { return TRUE;
                       } }
 < ----- */
			NextDlgCtrl();
					return TRUE;

			case VK_F5 :
                   OnCancel ();
 			       return TRUE;
/* ----->
			 case VK_F7 :
                     OnDelete ();
 			         return TRUE;
             case VK_F9 :
		             if (OnF9 ())
                     {
                         return TRUE;
                     }
                     break;
		     case VK_F6 :
                     OnFree ();
				     return TRUE;
 <----- */

/* ---->
			 case VK_F12 :
					UpdateData( TRUE ) ;
					ladevartodaba();
					if (  tou_class.testupdtou() )
						tou_class.inserttou () ;
					else
						tou_class.updatetou () ;

					v_edithtour = 0 ;
					tou.tou = 0 ;
					UpdateData(FALSE) ;
					m_edithtour.EnableWindow (TRUE) ;
					m_buttonhtour.EnableWindow ( TRUE) ;
					m_edithtour.ModifyStyle (0, WS_TABSTOP,0) ;
					m_buttonhtour.ModifyStyle ( 0, WS_TABSTOP,0) ;
					m_edithtour.SetFocus () ;	


//                     OnOK ();
				     return TRUE;
< ----- */


			case VK_DOWN :
				NextDlgCtrl();
	 			cWnd = GetFocus ();
				GetNextDlgTabItem(cWnd ,FALSE) ;
				return TRUE;
			case VK_UP :
 				PrevDlgCtrl();
				cWnd = GetFocus ();
			 	GetNextDlgTabItem(cWnd ,FALSE) ;
				return TRUE;
 			}
	 }

	return CDialog::PreTranslateMessage(lpMsg);

}

void C15700Dlg::OnEnKillfocusMdnnr()
{

	UpdateData (TRUE) ;

	int i = m_mdnnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	if (i)	mdn.mdn = (short) atoi ( bufh );
	else mdn.mdn = -2 ;

	i = lesemdn();
}

void C15700Dlg::OnEnKillfocusFilnr()
{
	UpdateData (TRUE) ;

	int i = m_filnr.GetLine(0,bufh,500);
	bufh[i] = '\0' ;
	fil.mdn = mdn.mdn;
	if (i)
		fil.fil = (short) atoi ( bufh );
	else fil.fil = -2 ;

	if ( fil.fil == 0 )
	{
			dmonat = 1 ;
			v_combo1.Format("M  Monat" );	// einigermassen sinnvoller default-Wert ?!
			m_combo1.EnableWindow(TRUE);
			v_filname.Format("              ");
			m_combo1.EnableWindow(TRUE);
			fil_abrech.mdn = mdn.mdn;
			fil_abrech.fil = fil.fil;
			sprintf ( fil_abrech.period_kz,"M");
			setzeperiode("M");
			dmonat = 1;
			satzlesen();

	}
	else
	{
		dmonat = 1 ;
		v_combo1.Format("M  Monat" );	// einigermassen sinnvoller default-Wert ?!
		m_combo1.EnableWindow(TRUE);
		i = lesefil();
		if ( i )	// erfolgreich gefunden
		{
			fil_abrech.mdn = mdn.mdn; 
			fil_abrech.fil = fil.fil;
			if ( fil.abr_period[0] == 'W' )
			{
				dmonat = 0;
				sprintf ( fil_abrech.period_kz,"W");
				setzeperiode("W");
				v_combo1.Format("W  Woche" );
				m_combo1.EnableWindow(FALSE);
			}
			if ( fil.abr_period[0] == 'M' )
			{
				dmonat = 1;
				sprintf ( fil_abrech.period_kz,"M");
				setzeperiode("M");
				v_combo1.Format("M Monat" );
				m_combo1.EnableWindow(FALSE);
			}
			if ( fil.abr_period[0] == 'Q' )
			{
				dmonat = 2;
				sprintf ( fil_abrech.period_kz,"Q");
				setzeperiode("Q");
				v_combo1.Format("Q  Quartal" );
				m_combo1.EnableWindow(FALSE);
			}
			satzlesen();
		}
	}

	UpdateData (FALSE) ;
}

void C15700Dlg::OnCbnKillfocusCombo1()
{
	UpdateData(TRUE) ;
	bufh[0] = '\0' ;

	int nCurSel = ((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetCurSel();
	CString bufx;
	if ( nCurSel > -1 )
	{
		((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);
		bufx.MakeUpper();
		sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
		int i = (int) strlen ( bufh );
		// fixe formatierung : 1 Zeichen und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 2 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[1] = '\0' ;
			}
			sprintf ( ptabn.ptitem, "abr_period" );
			sprintf ( ptabn.ptwert, "%s", bufh);
			i = Ptabn.openptabnw();
			i = Ptabn.leseptabnw();
			if ( i )
			{
				v_combo1.Format("                   ");
			}

		}
		else
		{
				v_combo1.Format("                   ");
		}
	}
	else
	{
		sprintf ( bufh, "%s", v_combo1.MakeUpper() );
		int i = (int) strlen ( bufh );
		// fixe formatierung : 1 Stelle und 2 blanks , danach folgt ein text
		if ( i )
		{
			if ( i < 2 )
			{
				bufh[i ] = '\0' ;
			}
			else
			{
				bufh[1] = '\0' ;
			}
		}
		nCurSel = -1 ;
//		sprintf ( bufh, "%8.0d", hilfe );
		nCurSel=((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->FindString(nCurSel, bufh);
	
		if (nCurSel != CB_ERR)
		{
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->SetCurSel(nCurSel) ;
			((CComboBox *)AfxGetMainWnd()->GetDlgItem(IDC_COMBO1))->GetLBText(nCurSel, bufx);
			bufx.MakeUpper();
			sprintf(bufh,"%s",bufx.GetBuffer(0)) ;
			i = (int) strlen ( bufh );
			// fixe formatierung : 1 Stelle und 2 blanks , danach folgt ein text
			if ( i )
			{
				if ( i < 2 )
				{
					bufh[i] = '\0' ;
				}
				else
				{
				bufh[1] = '\0' ;
				}
			}
			else
				bufh[0] = '\0' ;
			
			sprintf ( ptabn.ptitem,"abr_period");
			sprintf ( ptabn.ptwert,"%s", bufh ) ;
			i = Ptabn.openptabnw();
			i = Ptabn.leseptabnw();
			if ( i )
			{
				v_combo1.Format("                   ");
			}
			else
			{
				v_combo1.Format(bufx.GetBuffer(0));
			}

		}
		else
		{
			v_combo1.Format("                   ");
		}
	}

	sprintf ( bufh, "%s", v_combo1.MakeUpper() );

	fil_abrech.mdn = mdn.mdn;
	fil_abrech.fil = fil.fil;

	if ( dmonat != 0 && bufh[0] == 'W' )
	{
		satzschreiben();
		dmonat = 0;

		sprintf ( fil_abrech.period_kz,"W");
		setzeperiode("W");
		satzlesen();
	}

	sprintf ( bufh, "%s", v_combo1.MakeUpper() );

	if ( dmonat != 1 && bufh[0] == 'M' )
	{
		satzschreiben();
		dmonat = 1;
		sprintf ( fil_abrech.period_kz,"M");
		setzeperiode("M");
		satzlesen();
	}

	sprintf ( bufh, "%s", v_combo1.MakeUpper() );

	if ( dmonat != 2 && bufh[0] == 'Q' )
	{
		satzschreiben();
		dmonat = 2;
		sprintf ( fil_abrech.period_kz,"Q");
		setzeperiode("Q");
		satzlesen();
	}

	UpdateData(FALSE);
}


int C15700Dlg::lesemdn(void)
{
	// mdn.mdn ist aktuell ......

	Mdn.openmdn();
	if (! Mdn.lesemdn())
	{
		adr.adr = mdn.adr ;
		int i = Adr.openadr () ;
		i = Adr.leseadr () ;
		if (!i) 
		{
			v_mdnname.Format("%s",_T(adr.adr_krz));
		}
		else
		{
			v_mdnname.Format("              ");
			FALSE;
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_mdnname.Format("              ");
		return FALSE;
		
	}
return TRUE ;
}

int C15700Dlg::lesefil(void)
{
	// mdn.fil und fil.fil sind aktuell ......

//	m_combo1.EnableWindow(FALSE);

	Fil.openfil();
	if (! Fil.lesefil())
	{
		adr.adr = fil.adr ;
		int i = Adr.openadr () ;
		i = Adr.leseadr () ;
		if (!i) 
		{
			v_filname.Format("%s",_T(adr.adr_krz));

			if ( fil.abr_period[0]=='M' || fil.abr_period[0]=='W' || fil.abr_period[0] == 'Q' )	// rest kommen erst sp�ter
				m_combo1.EnableWindow(FALSE);
		}
		else
		{
			v_filname.Format("              ");
			return FALSE;
		}
	}
	else	// fehlerhafte Eingabe
	{
		v_filname.Format("              ");
		return FALSE;
		
	}
return TRUE ;
}


void C15700Dlg::OnBnClickedRaddat()
{
UpdateData(TRUE);
v_raddat = TRUE;
v_radproz = FALSE;
v_radeuro = FALSE;
if ( dmodus != 0)
{
		satzschreiben();
		dmodus = 0;
		satzlesen();
}
SetWindowText("End-Datum");

UpdateData(FALSE);
}

void C15700Dlg::OnBnClickedRadproz()
{
UpdateData(TRUE);
v_raddat = FALSE;
v_radproz = TRUE;
v_radeuro = FALSE;
if ( dmodus != 1)
{
		satzschreiben();
		dmodus = 1;
		satzlesen();
}
	SetWindowText("15700-Filial-Perioden-Plan-Prozent");

UpdateData(FALSE);
}

void C15700Dlg::OnBnClickedRadeuro()
{
UpdateData(TRUE);
v_raddat = FALSE;
v_radproz = FALSE;
v_radeuro = TRUE;
if ( dmodus != 2)
{
		satzschreiben();
		dmodus = 2;
		satzlesen();
}
SetWindowText("15700-Filial-Perioden-Plan-Umsatz");

UpdateData(FALSE);
}

BOOL C15700Dlg::Speicherfrage()
{
	if ( nureinmalfragen ) return TRUE;
	nureinmalfragen=1;
	if ( MessageBox("Sollen �nderungen gespeichert werden ? ", " ", MB_YESNO ) == IDYES )
		return TRUE;
	return FALSE;
}
