#include "stdafx.h"
#include "a_prot.h"

extern short sql_mode;
extern int sqlstatus;

#define SQLFEHLER(b, s, l) if (sqlstatus b) _SQLF(s,l);

struct A_PROT a_prot, a_prot_null, a_prot_def;

void A_PROT_CLASS::prepare (void)
{
/*
            TCHAR *sqltext;

            sqlin ((double *)   &a_prot.a,  SQLDOUBLE, 0);
    sqlout ((short *) &a_prot.delstatus,SQLSHORT,0);
    sqlout ((double *) &a_prot.a,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &a_prot.bearb,SQLDATE,0);
    sqlout ((TCHAR *) a_prot.pers_nam,SQLCHAR,9);
    sqlout ((long *) &a_prot.prima,SQLLONG,0);
    sqlout ((TCHAR *) a_prot.dr_status,SQLCHAR,2);
            cursor = sqlcursor (_T("select a_prot.delstatus,  ")
_T("a_prot.a,  a_prot.bearb,  a_prot.pers_nam,  a_prot.prima,  ")
_T("a_prot.dr_status from a_prot ")

#line 18 "A_prot.rpp"
                                  _T("where a = ?"));
    sqlin ((short *) &a_prot.delstatus,SQLSHORT,0);
    sqlin ((double *) &a_prot.a,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_prot.bearb,SQLDATE,0);
    sqlin ((TCHAR *) a_prot.pers_nam,SQLCHAR,9);
    sqlin ((long *) &a_prot.prima,SQLLONG,0);
    sqlin ((TCHAR *) a_prot.dr_status,SQLCHAR,2);
            sqltext = _T("update a_prot set ")
_T("a_prot.delstatus = ?,  a_prot.a = ?,  a_prot.bearb = ?,  ")
_T("a_prot.pers_nam = ?,  a_prot.prima = ?,  a_prot.dr_status = ? ")

#line 20 "A_prot.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_prot.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_prot.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_prot ")
                                  _T("where a = ? for update"));
            sqlin ((double *)   &a_prot.a,  SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_prot ")
                                  _T("set delstatus = 0 where a = ? and delstatus = 0 for update"));
            sqlin ((double *)   &a_prot.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_prot ")
                                  _T("where a = ?"));
*/
    sqlin ((short *) &a_prot.delstatus,SQLSHORT,0);
    sqlin ((double *) &a_prot.a,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_prot.bearb,SQLDATE,0);
    sqlin ((TCHAR *) a_prot.pers_nam,SQLCHAR,9);
    sqlin ((long *) &a_prot.prima,SQLLONG,0);
    sqlin ((TCHAR *) a_prot.dr_status,SQLCHAR,2);
            ins_cursor = sqlcursor (_T("insert into a_prot (")
_T("delstatus,  a,  bearb,  pers_nam,  prima,  dr_status) ")

#line 35 "A_prot.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?)"));

#line 37 "A_prot.rpp"
}


int A_PROT_CLASS::dbreadfirst ()
{
	return 100;
}

int A_PROT_CLASS::dbread ()
{
	return 100;
}

int A_PROT_CLASS::dblock ()
{
	return 0;
}

int A_PROT_CLASS::dbupdate ()
{
        if (ins_cursor == -1)
        {
               prepare ();
        }
        int dsqlstatus = sqlexecute (ins_cursor);
        sqlstatus = dsqlstatus;
        SQLFEHLER (!= 0, _T("Fehler beim Insert "),__LINE__)
        return sqlstatus;
}

int A_PROT_CLASS::dbdelete ()
{
        return 0;
}

void A_PROT_CLASS::dbclose ()
{
        int dsqlstatus = sqlclose (ins_cursor); 
        sqlstatus = dsqlstatus;
        SQLFEHLER (!= 0, _T("Fehler beim Schliessen "),__LINE__)
 }
