// EanEmb.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "EanEmb.h"


// CEanEmb-Dialogfeld

IMPLEMENT_DYNAMIC(CEanEmb, CDialog)
CEanEmb::CEanEmb(CWnd* pParent /*=NULL*/)
	: CDialog(CEanEmb::IDD, pParent)
{
	Basis = NULL;
	PageUpdate = NULL;
}

CEanEmb::~CEanEmb()
{
}

void CEanEmb::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CEanEmb, CDialog)
	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CEanEmb-Meldungshandler

BOOL CEanEmb::OnInitDialog()
{

	CDialog::OnInitDialog();
	m_Ean.Create (IDD_EANDATA, this);
	m_Ean.ShowWindow (SW_SHOWNORMAL);
	m_Ean.UpdateWindow ();
	m_Ean.Basis = Basis;

	m_Emb.Create (IDD_EMB_DATA, this);
	m_Emb.ShowWindow (SW_SHOWNORMAL);
	m_Emb.UpdateWindow ();

	SplitPane.Create (this, 0, 0, &m_Ean, &m_Emb, 40, CSplitPane::Vertical);

	return FALSE;
}

void CEanEmb::OnSize (UINT nType, int cx, int cy)
{
	if (IsWindow (SplitPane.m_hWnd))
	{
		CRect rect;
		m_Ean.GetWindowRect (&rect);
		ScreenToClient (&rect);
		rect.right = cx;
        m_Ean.MoveWindow (&rect);

		m_Emb.GetWindowRect (&rect);
		ScreenToClient (&rect);
		rect.right = cx;
		rect.bottom = cy - 2;
        m_Emb.MoveWindow (&rect);

		SplitPane.SetLength (cx);
	}
}

BOOL CEanEmb::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			if (pMsg->wParam == VK_F5)
			{
//				PageUpdate->Back ();
//				return TRUE;
			}
/*
			else if (pMsg->wParam == VK_F7)
			{
				Basis->OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
*/
	}
	return FALSE;
}

void CEanEmb::SetBasis (CBasisdatenPage *Basis)
{
	this->Basis = Basis;
	m_Ean.SetBasis (Basis);
	m_Emb.SetBasis (Basis);
}

void CEanEmb::Update ()
{
	m_Ean.Update ();
	m_Emb.Update ();
}

void CEanEmb::AddUpdateEvent (CUpdateEvent *UpdateEvent)
{
	UpdateTab.push_back (UpdateEvent);
}
