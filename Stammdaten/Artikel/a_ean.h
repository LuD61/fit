#ifndef _A_EAN_DEF
#define _A_EAN_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_EAN {
   double         a;
   short          delstatus;
   double         ean;
   TCHAR          ean_bz[25];
   TCHAR          h_ean_kz[2];
   short          ean_vk_kz;
};
extern struct A_EAN a_ean, a_ean_null;

#line 8 "a_ean.rh"

class A_EAN_CLASS : public DB_CLASS 
{
       private :
               int acursor;    
               int eancursor;    
               void prepare (void);
       public :
               A_EAN a_ean;  
               A_EAN_CLASS () : DB_CLASS ()
               {
		  acursor = -1;	
		  eancursor = -1;	
               }
               int dbreadafirst ();  
               int dbreada ();  
               int dbreadeanfirst ();  
               int dbreadean ();  
};
#endif
