#ifndef _CHOICEEAN_DEF
#define _CHOICEEAN_DEF

#define EANSELECTED 5003
#define EANCANCELED 5004

#include "ChoiceX.h"
#include "EanList.h"
#include <vector>

class CChoiceEan : public CChoiceX
{
    protected :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
      
    public :
		CString Where;
	    std::vector<CEanList *> EanList;
	    std::vector<CEanList *> SelectList;
      	CChoiceEan(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceEan(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchEanBz (CListCtrl *, LPTSTR);
        void SearchA (CListCtrl *,  LPTSTR);
        void SearchABz1 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
	    virtual void SaveSelection (CListCtrl *);
		virtual void SendSelect (); 
		virtual void SendCancel (); 
		CEanList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		virtual void SetDefault ();
};
#endif
