create table "fit".a_grund 
  (
    a decimal(13,0),
    rgb_r smallint,
    rgb_g smallint,
    rgb_b smallint
  ) with crcols  in fit_dat extent size 32 next size 256 lock mode row;
revoke all on "fit".a_mat from "public";

create unique index "fit".i01a_grund on "fit".a_grund (a);



