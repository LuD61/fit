#include "StdAfx.h"
#include "splitpane.h"

IMPLEMENT_DYNCREATE(CSplitPane, CStatic)

CSplitPane::CSplitPane(void)
{
	size = 4;
	m_bIsPressed = FALSE;
	maxY = 0;
	maxX = 0;

}

CSplitPane::~CSplitPane(void)
{
}

BEGIN_MESSAGE_MAP(CSplitPane, CStatic)
	//{{AFX_MSG_MAP(CSplitterControl)
	ON_WM_SIZE ()
	ON_WM_MOVE ()
	ON_WM_MOUSEMOVE ()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CSplitPane::Create (CWnd *Parent, int x, int y, 
				CWnd *View1, CWnd *View2, int FirstPart, int Direction)
{
	BOOL ret;

	this->x = x;
	this->y = y;
	this->View1 = View1;
	this->View2 = View2;
	this->FirstPart = FirstPart;
	this->Direction = Direction;
	try
	{
		CRect rect1;
		CRect rect2;
		View1->GetWindowRect (&rect1);
		View2->GetWindowRect (&rect2);
		Parent->ScreenToClient (&rect1);
		Parent->ScreenToClient (&rect2);
		cx1 = rect1.right - rect1.left;
		cy1 = rect1.bottom - rect1.top;
		cx2 = rect2.right - rect2.left;
		cy2 = rect2.bottom - rect2.top;
		cx = cx1 + cx2;
		cy = cy1 + cy2;
		View1->MoveWindow (x, y, cx1, cy1);
		if (Direction == Horizontal)
		{
			cx += size;
  		    cy = max (cy1, cy2);

			cx1 = cx * FirstPart / 100;
			cx2 = cx - cx1;
			cx1 -= size;
			View1->MoveWindow (x, y, cx1, cy, FALSE);
			View2->MoveWindow (x + cx1 + size, y, cx2, cy, FALSE);
		}
		else
		{
  		    cx = max (cx1, cx2);
			cy += size;

			cy1 = cy * FirstPart / 100;
			cy2 = cy - cy1;
			cy1 -= size;
			View1->MoveWindow (x, y, cx, cy1, FALSE);
			View2->MoveWindow (x, y + cy1 + size, cx, cy2, FALSE);
		}
		CRect srect;
		if (Direction == Horizontal)
		{
			srect.left = x + cx1;
			srect.top  = y;
			srect.right = srect.left + size;
			srect.bottom = srect.top + cy; 
		}
		else
		{
			srect.left = x;
			srect.top  = y + cy1;
			srect.right = srect.left + cx;
			srect.bottom = srect.top + size; 
		}
		ret = CStatic::Create (NULL, WS_CHILD | WS_VISIBLE | SS_OWNERDRAW | SS_NOTIFY, srect, Parent); 
		lRect = srect;

		horizontalCursor = AfxGetApp()->LoadStandardCursor(IDC_SIZEWE);
	    verticalCursor   = AfxGetApp()->LoadStandardCursor(IDC_SIZENS);

		return ret;
	}
	catch (...)
	{
		return FALSE;
	}
    return TRUE;
}

void CSplitPane::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CPen wPen;
	CPen gPen;
	CPen bPen;
	CDC cDC;
	int sx;
	int sy;

	wPen.CreatePen (PS_SOLID, 1, RGB (255, 255, 255));
	gPen.CreatePen (PS_SOLID, 1, RGB (195, 195, 195));
	bPen.CreatePen (PS_SOLID, 1, RGB (0, 0, 0));
	cDC.Attach (lpDrawItemStruct->hDC);
	CPen *oldPen = cDC.SelectObject (&gPen);
	CRect rect;
	GetClientRect (&rect);
	int cx = rect.right;
	int cy = rect.bottom;
	if (Direction == Horizontal)
	{
		sx = 0;
		sy = 0;
		cDC.FillSolidRect (sx, sy, sx + size, sy + cy, RGB (195, 195, 195)); 
		cDC.MoveTo (sx, sy);
		cDC.LineTo (sx, cy);
		cDC.MoveTo (sx, sy);
		cDC.LineTo (sx + size, sy);
	}
	else
	{
		sx = 0;
		sy = 0;
		cDC.FillSolidRect (sx, sy, sx + cx - 2, sy + size -1, RGB (195, 195, 195)); 
		cDC.MoveTo (sx, sy);
		cDC.LineTo (sx + cx, sy);
		cDC.MoveTo (sx, sy);
		cDC.LineTo (sx, sy + size - 1);
	}

	cDC.SelectObject (&wPen);
	if (Direction == Horizontal)
	{
		sx = 0;
		sy = 0;
		cDC.MoveTo (sx + 1, sy + 1);
		cDC.LineTo (sx + 1, sy + cy - 2);
		cDC.MoveTo (sx + 1, sy + 1);
		cDC.LineTo (sx + size - 2, sy + 1);
	}
	else
	{
		sx = 0;
		sy = 0;
		cDC.MoveTo (sx + 1, sy + 1);
		cDC.LineTo (sx + cx - 2, sy + 1);
		cDC.MoveTo (sx + 1, sy + 1);
		cDC.LineTo (sx + 1, sy + size - 2);
	}

	cDC.SelectObject (&bPen);
	if (Direction == Horizontal)
	{
		sx = 0;
		sy = 0;
		cDC.MoveTo (sx + size - 1, sy);
		cDC.LineTo (sx + size - 1, sy + cy - 1);
		cDC.MoveTo (sx + size - 1, sy + cy);
		cDC.LineTo (sx, sy + cy - 1);
	}
	else
	{
		sx = 0;
		sy = 0;
		cDC.MoveTo (sx, sy + size -1);
		cDC.LineTo (sx + cx - 1, sy + size -1);
		cDC.MoveTo (sx + cx - 1, sy + size -1);
		cDC.LineTo (sx + cx - 1, sy);
	}

	cDC.SelectObject (&oldPen);
}


void CSplitPane::OnSize(UINT nType, int cx, int cy )
{
}

void CSplitPane::OnMove(int x, int y )
{
}

void CSplitPane::SetLength (int Length)
{
	CRect rect;
	GetWindowRect (&rect);
	GetParent ()->ScreenToClient (&rect);
	if (Direction == Horizontal)
	{
		rect.bottom = rect.top + Length;
	}
	else
	{
		rect.right = rect.left + Length;
	}
	CWnd::MoveWindow (&rect, TRUE);
	Invalidate ();
}

void CSplitPane::MoveWindow(int x,int y, int cx, int cy, BOOL bRepaint)
{
	this->x = x;
	this->y = y;
	if (Direction == Horizontal)
	{
		cx1 = cx * FirstPart / 100;
		cx2 = cx - cx1;
		cx1 -= size;
		View1->MoveWindow (x, y, cx1, cy, bRepaint);
		View2->MoveWindow (x + cx1 + size, y, cx2, cy, bRepaint);
	}
	else
	{
		cy1 = cy * FirstPart / 100;
		cy2 = cy - cy1;
		cy1 -= size;
		View1->MoveWindow (x, y, cx, cy1, bRepaint);
		View2->MoveWindow (x, y + cy1 + size, cx, cy2, bRepaint);
	}
	CRect srect;
	if (Direction == Horizontal)
	{
		srect.left = x + cx;
		srect.top  = y;
		srect.right = srect.left + size;
		srect.bottom = srect.top + cy1; 
	}
	else
	{
		srect.left = x;
		srect.top  = y + cy1;
		srect.right = srect.left + cx;
		srect.bottom = srect.top + size; 
	}
	lRect = srect;
	CStatic::MoveWindow (srect, bRepaint); 
}

void CSplitPane::MoveWindow(LPRECT rect, BOOL bRepaint)
{
	MoveWindow (rect->left, rect->top, 
		        rect->right - rect->left,
				rect->bottom - rect->top,
				bRepaint);
}

void CSplitPane::OnMouseMove (UINT nFlags, CPoint point)
{
	if (m_bIsPressed)
	{
		CRect srect = lRect;
		this->point = point;
		DrawLine ();
//		SetNewSplit ();
	}
}

void CSplitPane::DrawLine ()
{
	CPoint p;

	CWindowDC wDC (NULL);

	PrintLine (&wDC, m_lx, m_ly);

	CRect rect;
	CRect wrect;
	GetWindowRect (&wrect);
	int x = wrect.left;
	int y = wrect.top;
	GetClientRect (&rect);
	p = point;
	ClientToScreen (&p);
	PrintLine (&wDC, p.x, p.y);
}

void CSplitPane::PrintLine (CDC *wDC, int px, int py)
{
	CPen bPen;

	int nRop = wDC->SetROP2(R2_NOTXORPEN);
	bPen.CreatePen (PS_SOLID, 2, RGB (0, 0, 0));

	CPen *oldPen = wDC->SelectObject (&bPen); 
	CRect rect;
	CRect wrect;
	GetWindowRect (&wrect);
	int x = wrect.left;
	int y = wrect.top;
	GetClientRect (&rect);
	CPoint p (px,py);
	m_lx = px;
	m_ly = py;
//	ClientToScreen (&p);
	if (Direction == Horizontal)
	{
		 x = p.x + rect.right / 2;
		 wDC->MoveTo (x,y);
		 wDC->LineTo (x, wrect.bottom);
		 lRect.left = x;
		 lRect.right = x + 2;
		 lRect.top = y;
		 lRect.bottom = wrect.bottom + 1;
	}
	else
	{
		 y = p.y + rect.bottom / 2;
		 wDC->MoveTo (x,y);
		 wDC->LineTo (wrect.right, y);
		 lRect.left = x;
		 lRect.right = wrect.right + 2;
		 lRect.top = y;
		 lRect.bottom = y + 2;
	}
	GetParent ()->ScreenToClient (&lRect);
	wDC->SetROP2(nRop);
	wDC->SelectObject (oldPen); 
}

void CSplitPane::SetNewSplit ()
{
	CPoint p;
	p = point;
	if (m_bIsPressed)
	{
		CRect rect;
		GetWindowRect (&rect);
		CRect vrect1;
		View1->GetWindowRect (&vrect1);
        GetParent ()->ScreenToClient (&vrect1);
		CRect vrect2;
		View2->GetWindowRect (&vrect2);
        GetParent ()->ScreenToClient (&vrect2);

        GetParent ()->ScreenToClient (&rect);
		ClientToScreen (&p);
		GetParent ()->ScreenToClient (&p);
		if (Direction == Horizontal)
		{
			if (p.x < vrect1.left + maxX) return;
			if (p.x > vrect2.right - maxX) return;

			vrect1.right -= (rect.left - p.x);
			View1->MoveWindow (&vrect1);
			vrect2.left -= (rect.left - p.x);  
			View2->MoveWindow (&vrect2);
			rect.right -= (rect.left - p.x);
			rect.left = p.x;
			CStatic::MoveWindow (&rect);
			cx1 = vrect1.right - vrect1.left;
			FirstPart = cx1 * 100 / cx;
		}
		else
		{
			if (p.y < (vrect1.top + maxY)) return;
			if (p.y > (vrect2.bottom - maxY)) return;

			vrect1.bottom -= (rect.top - p.y);
			View1->MoveWindow (&vrect1);
			vrect2.top -= (rect.top - p.y);  
			View2->MoveWindow (&vrect2);
			rect.bottom -= (rect.top - p.y);
			rect.top = p.y;
			CStatic::MoveWindow (&rect);
			cy1 = vrect1.bottom - vrect1.top;
			FirstPart = cy1 * 100 / cy;
		}
	}
}

BOOL CSplitPane::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	if (nHitTest == HTCLIENT)
	{
		if (Direction == Horizontal)
		{
			::SetCursor(horizontalCursor);
		}
		else
		{
			::SetCursor(verticalCursor);
		}
		return FALSE;
	}
	return CStatic::OnSetCursor(pWnd, nHitTest, message);
}

void CSplitPane::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CStatic::OnLButtonDown(nFlags, point);
	
	m_bIsPressed = TRUE;
	SetCapture();
	CRect wrect;
	GetWindowRect(wrect);

	if (Direction == Horizontal)
	{
		startx = wrect.left;
		starty = wrect.top;
	}
	else
	{
		startx = wrect.left;
		starty = wrect.top;
	}

	CWindowDC wDC(NULL);

	PrintLine (&wDC, startx, starty);
}

void CSplitPane::OnLButtonUp(UINT nFlags, CPoint point) 
{


	if (m_bIsPressed)
	{
		CWindowDC wDC(NULL);
		PrintLine (&wDC, m_lx, m_ly);
		SetNewSplit ();
		m_bIsPressed = FALSE;
	}
	CStatic::OnLButtonUp(nFlags, point);
	ReleaseCapture();
}

void CSplitPane::SetView1 (CWnd *View1)
{
	CRect rect;
	this->View1->GetWindowRect (&rect);
	GetParent ()->ScreenToClient (&rect);
	this->View1 = View1;
	View1->MoveWindow (rect);
}

void CSplitPane::SetView2 (CWnd *View2)
{
	CRect rect;
	this->View2->GetWindowRect (&rect);
	GetParent ()->ScreenToClient (&rect);
	this->View2 = View2;
	View2->MoveWindow (rect);
}