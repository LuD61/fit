#include "stdafx.h"
#include "a_emb.h"

struct A_EMB a_emb, a_emb_null, a_emb_def;

void A_EMB_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_emb.emb,  SQLDOUBLE, 0);
            sqlin ((double *)   &a_emb.unt_emb,  SQLDOUBLE, 0);
    sqlout ((short *) &a_emb.anz_emb,SQLSHORT,0);
    sqlout ((short *) &a_emb.delstatus,SQLSHORT,0);
    sqlout ((double *) &a_emb.emb,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_emb.emb_bz,SQLCHAR,25);
    sqlout ((TCHAR *) a_emb.emb_vk_kz,SQLCHAR,2);
    sqlout ((double *) &a_emb.tara,SQLDOUBLE,0);
    sqlout ((double *) &a_emb.tara_proz,SQLDOUBLE,0);
    sqlout ((double *) &a_emb.unt_emb,SQLDOUBLE,0);
    sqlout ((double *) &a_emb.a_pfa,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select a_emb.anz_emb,  ")
_T("a_emb.delstatus,  a_emb.emb,  a_emb.emb_bz,  a_emb.emb_vk_kz,  ")
_T("a_emb.tara,  a_emb.tara_proz,  a_emb.unt_emb,  a_emb.a_pfa from a_emb ")

#line 13 "a_emb.rpp"
                                  _T("where emb = ? ") 
				  _T("and unt_emb = ?"));
    sqlin ((short *) &a_emb.anz_emb,SQLSHORT,0);
    sqlin ((short *) &a_emb.delstatus,SQLSHORT,0);
    sqlin ((double *) &a_emb.emb,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_emb.emb_bz,SQLCHAR,25);
    sqlin ((TCHAR *) a_emb.emb_vk_kz,SQLCHAR,2);
    sqlin ((double *) &a_emb.tara,SQLDOUBLE,0);
    sqlin ((double *) &a_emb.tara_proz,SQLDOUBLE,0);
    sqlin ((double *) &a_emb.unt_emb,SQLDOUBLE,0);
    sqlin ((double *) &a_emb.a_pfa,SQLDOUBLE,0);
            sqltext = _T("update a_emb set a_emb.anz_emb = ?,  ")
_T("a_emb.delstatus = ?,  a_emb.emb = ?,  a_emb.emb_bz = ?,  ")
_T("a_emb.emb_vk_kz = ?,  a_emb.tara = ?,  a_emb.tara_proz = ?,  ")
_T("a_emb.unt_emb = ?,  a_emb.a_pfa = ? ")

#line 16 "a_emb.rpp"
                                  _T("where emb = ? ") 
				  _T("and unt_emb = ?");
            sqlin ((double *)   &a_emb.emb,  SQLDOUBLE, 0);
            sqlin ((double *)   &a_emb.unt_emb,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_emb.emb,  SQLDOUBLE, 0);
            sqlin ((double *)   &a_emb.unt_emb,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select unt_emb from a_emb ")
                                  _T("where emb = ? ") 
				  _T("and unt_emb = ?"));
            sqlin ((double *)   &a_emb.emb,  SQLDOUBLE, 0);
            sqlin ((double *)   &a_emb.unt_emb,  SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_emb ")
                                  _T("set delstatus = 0 ")
                                  _T("where emb = ? ") 
				  _T("and unt_emb = ?"));
            sqlin ((double *)   &a_emb.emb,  SQLDOUBLE, 0);
            sqlin ((double *)   &a_emb.unt_emb,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_emb ")
                                  _T("where emb = ? ") 
				  _T("and unt_emb = ?"));
    sqlin ((short *) &a_emb.anz_emb,SQLSHORT,0);
    sqlin ((short *) &a_emb.delstatus,SQLSHORT,0);
    sqlin ((double *) &a_emb.emb,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_emb.emb_bz,SQLCHAR,25);
    sqlin ((TCHAR *) a_emb.emb_vk_kz,SQLCHAR,2);
    sqlin ((double *) &a_emb.tara,SQLDOUBLE,0);
    sqlin ((double *) &a_emb.tara_proz,SQLDOUBLE,0);
    sqlin ((double *) &a_emb.unt_emb,SQLDOUBLE,0);
    sqlin ((double *) &a_emb.a_pfa,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into a_emb (")
_T("anz_emb,  delstatus,  emb,  emb_bz,  emb_vk_kz,  tara,  tara_proz,  unt_emb,  a_pfa) ")

#line 39 "a_emb.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?)"));

#line 41 "a_emb.rpp"
            sqlin ((double *)   &a_emb.unt_emb,  SQLDOUBLE, 0);
    sqlout ((short *) &a_emb.anz_emb,SQLSHORT,0);
    sqlout ((short *) &a_emb.delstatus,SQLSHORT,0);
    sqlout ((double *) &a_emb.emb,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_emb.emb_bz,SQLCHAR,25);
    sqlout ((TCHAR *) a_emb.emb_vk_kz,SQLCHAR,2);
    sqlout ((double *) &a_emb.tara,SQLDOUBLE,0);
    sqlout ((double *) &a_emb.tara_proz,SQLDOUBLE,0);
    sqlout ((double *) &a_emb.unt_emb,SQLDOUBLE,0);
    sqlout ((double *) &a_emb.a_pfa,SQLDOUBLE,0);
            UntEmbCursor = sqlcursor (_T("select ")
_T("a_emb.anz_emb,  a_emb.delstatus,  a_emb.emb,  a_emb.emb_bz,  ")
_T("a_emb.emb_vk_kz,  a_emb.tara,  a_emb.tara_proz,  a_emb.unt_emb,  ")
_T("a_emb.a_pfa from a_emb ")

#line 43 "a_emb.rpp"
                                     _T("where unt_emb = ? ")); 
            sqlin ((double *)   &a_emb.emb,  SQLDOUBLE, 0);
    sqlout ((short *) &a_emb.anz_emb,SQLSHORT,0);
    sqlout ((short *) &a_emb.delstatus,SQLSHORT,0);
    sqlout ((double *) &a_emb.emb,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_emb.emb_bz,SQLCHAR,25);
    sqlout ((TCHAR *) a_emb.emb_vk_kz,SQLCHAR,2);
    sqlout ((double *) &a_emb.tara,SQLDOUBLE,0);
    sqlout ((double *) &a_emb.tara_proz,SQLDOUBLE,0);
    sqlout ((double *) &a_emb.unt_emb,SQLDOUBLE,0);
    sqlout ((double *) &a_emb.a_pfa,SQLDOUBLE,0);
            EmbCursor = sqlcursor (_T("select a_emb.anz_emb,  ")
_T("a_emb.delstatus,  a_emb.emb,  a_emb.emb_bz,  a_emb.emb_vk_kz,  ")
_T("a_emb.tara,  a_emb.tara_proz,  a_emb.unt_emb,  a_emb.a_pfa from a_emb ")

#line 46 "a_emb.rpp"
                                     _T("where emb = ? ")); 
}

int A_EMB_CLASS::dbreadfirst_unt_emb ()
{
	   if (UntEmbCursor == -1)
           {
		prepare ();	
           }
	   if (UntEmbCursor == -1) return -1;
           int dsqlstatus = sqlopen (UntEmbCursor);
           if (dsqlstatus != 0) return dsqlstatus;
           return sqlfetch (UntEmbCursor); 
}

int A_EMB_CLASS::dbread_unt_emb ()
{
	   if (UntEmbCursor == -1) return -1;
           return sqlfetch (UntEmbCursor); 
}

int A_EMB_CLASS::dbreadfirst_emb ()
{
	   if (EmbCursor == -1)
           {
		prepare ();	
           }
	   if (EmbCursor == -1) return -1;
           int dsqlstatus = sqlopen (EmbCursor);
           if (dsqlstatus != 0) return dsqlstatus;
           return sqlfetch (EmbCursor); 
}

int A_EMB_CLASS::dbread_emb ()
{
	   if (EmbCursor == -1) return -1;
           return sqlfetch (EmbCursor); 
}