#include "stdafx.h"
#include <stdio.h>
#include "Text.h"
#include "Vector.h"

Text::Text ()
{
         Buffer = new (char [256]);
         strcpy (Buffer, "");
         Len = 0;
         Sub = NULL;
}

Text::Text (char *Txt)
{
         Sub = NULL;
         Buffer = new char [(Len = (int) strlen (Txt)) + 1];
         if (Buffer == NULL)
         {
                Buffer = new (char [1]);
                strcpy (Buffer, "");
                Len = 0;
         }
         else
         {
                strcpy (Buffer, Txt);
         }
}

Text::Text (Text& Txt)
{
         Sub = NULL;
         Buffer = new char [(Len = Txt.GetLen ()) + 1];
         if (Buffer == NULL)
         {
                Buffer = new (char [1]);
                strcpy (Buffer, "");
                Len = 0;
         }
         else
         {
                strcpy (Buffer, Txt.GetBuffer ());
         }
}

Text::~Text ()
{
         if (Buffer != NULL)
         {
                delete Buffer;
         }
}

char *Text::GetBuffer (void)
{
         return Buffer;
}

void Text::SetBuffer (char *Txt)
{
         delete Buffer;
         Buffer = new char [(Len = (int) strlen (Txt)) + 1];
         if (Buffer == NULL)
         {
                Buffer = new (char [1]);
                strcpy (Buffer, "");
                Len = 0;
         }
         else
         {
                strcpy (Buffer, Txt);
         }
}

const Text& Text::operator=(char *Str)
{
	            if (Str == NULL)
				{
					Str = "";
				}
                SetBuffer (Str);  
                return *this;
}

const Text& Text::operator=(int value)
{
	            Format ("%d", value);
				return *this;
}

const Text& Text::operator=(Text& Txt)
{
                char *p = new char [Len + Txt.GetLen () + 1];
                strcpy (p, Txt.GetBuffer ());
                SetBuffer (p);  
                delete p;
                return *this;
}

Text& Text::operator+ (char *Str)
{
                char *p = new char [Len + strlen (Str) + 1];
                strcpy (p, Buffer);
                strcat (p, Str);
                SetBuffer (p);
                delete p;
                return *this;
}

Text& Text::operator+ (Text& t)
{
                char *p = new char [Len + t.GetLen () + 1];
                strcpy (p, Buffer);
                strcat (p, t.GetBuffer ());
                SetBuffer (p);
                delete p;
                return *this;
}

BOOL Text::operator== (Text &t)
{
                if (strcmp (Buffer, t.GetBuffer ()) == 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator== (char *c)
{
	            if (c == NULL)
				{
					c = "";
				}
                if (strcmp (Buffer, c) == 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator!= (Text &t)
{
                if (strcmp (Buffer, t.GetBuffer ()) != 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator!= (char *c)
{
                if (strcmp (Buffer, c) != 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator> (Text &t)
{
                if (strcmp (Buffer, t.GetBuffer ()) > 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator> (char *c)
{
                if (strcmp (Buffer, c) > 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator< (Text &t)
{
                if (strcmp (Buffer, t.GetBuffer ()) < 0)
                {
                    return TRUE;
                }
                return FALSE;
}

BOOL Text::operator< (char *c)
{
                if (strcmp (Buffer, c) < 0)
                {
                    return TRUE;
                }
                return FALSE;
}


char Text::operator[] (int idx)
{
                if (idx >= Len)
                {
                    return 0;
                }
                return Buffer[idx];
}

void Text::TrimLeft (void)
{
         int i;
         unsigned char *p;

         p = new unsigned char [Len + 1];
         if (p == NULL)
         {
             return;
         }

         strcpy ((char *) p, Buffer);
         for (i = 0; i < Len; i ++)
         {
             if (p[i] > (unsigned char ) ' ')
             {
                 break;
             }
         }
         SetBuffer ((char *) &p[i]);
         delete p;
}

void Text::TrimRight (void)
{
         int i;
         unsigned char *p;

         p = new unsigned char [Len + 1];
         if (p == NULL)
         {
             return;
         }

         strcpy ((char *) p, Buffer);
         for (i = Len; i >= 0; i --)
         {
             if (p[i] > (unsigned char ) ' ')
             {
                 break;
             }
         }
         p [i + 1] = 0;
         SetBuffer ((char *) p);
         delete p;
}

void Text::Trim (void)
{
	      TrimLeft ();
		  TrimRight ();
}


char *Text::TrimRight (char *str)
{
         Text String = str;
         String.TrimRight ();
         strcpy (str, String.GetBuffer ());
         return str;
}


char *Text::SubString (int start, int len)
{
         if (Sub != NULL)
         {
             delete Sub;
         }
         Sub = new char [len + 1];
         if (Sub == NULL)
         {
             return NULL;
         }
         if (start + len > (int) strlen (Buffer))
         {
             len = (int) strlen (Buffer) - start;
         }
         memcpy (Sub, &Buffer[start], len);  
         Sub[len] = 0;
         return Sub;
}

void Text::Format (char *format, ...)
{
         va_list args;
         char b [512];

         va_start (args,format);
         vsprintf (b,format,args);
         va_end (args);
         SetBuffer ((char *) b);
}

void Text::MakeUpper ()
{
          for (int i = 0; i < Len; i ++)
          {
               Buffer[i] = (char) toupper ((int) Buffer [i]);
          }
}

BOOL Text::matchcomp (LPSTR m, LPSTR s)
{
		  if (*m == 0 && *s == 0)
		  {
			  return TRUE;
		  }

		  if (*m == 0)
		  {
			  return FALSE;
		  }

		  if (*s == 0)
		  {
			  return FALSE;
		  }

		  if (*m == '*')
		  {
		      while (*m == '*' && *m != 0) m += 1;
		      if (*m == 0)
			  {
			         return TRUE;
			  }

			  int mlen;
			  for (mlen = 0; (m[mlen] != 0) && (m[mlen] != '*'); mlen ++);
		      while (*s != 0)
			  {
			        if (*m == '?')
					{
				          m += 1;
				          continue;
					}
			        if (*m == *s)
					{
						if (memcmp (m, s, mlen) == 0)
						{
				           break;
						}
					}
			        s += 1;
			  }
		      if (*s == 0)
			  {
			       return FALSE;
			  }
		  }
		  else
		  {
			  if (*m != *s && *m != '?')
			  {
				  return FALSE;
			  }
		  }

		  for (int i = 1; ; i ++)
		  {
			  if (m[i] == 0)
			  {
				  if (s[i] == 0)
				  {
				        return TRUE;
				  }
				  else
				  {
				        return FALSE;
				  }
			  }

			  if (m[i] == '*')
			  {
			      return matchcomp (&m[i], &s[i]);
			  }

			  if (s[i] == 0)
			  {
				  return FALSE;
			  }

			  if (m[i] == '?')
			  {
				  continue;
			  }
			  if (m[i] == s[i])
			  {
				  continue;
			  }
			  return matchcomp (&m[i], &s[i]);
//			  return matchcomp (m, s + 1);
		  }
		  return TRUE;
}

BOOL Text::CompareMatch (Text &txt)
{
	      char *p1 = txt.GetBuffer ();
		  char *p2 = GetBuffer ();
		  return matchcomp (p1, p2);
}


void Text::MakeLines (int MaxRowLen)
{
	       char *b;
		   int i, j, ib;
		   CVector Lines;

		   b = new char [MaxRowLen + 2];

		   ib = -1;
		   for (i = 0, j = 0; i < Len; i ++)
		   {
			   if (Buffer[i] == ' ')
			   {
				   ib = i;
			   }

			   if (Buffer[i] == '\n')
			   {
				   b[j] = 0;
				   Lines.Add (b);
				   j = 0;
 		           b = new char [MaxRowLen + 2];
				   ib = -1;
			   }

			   if (j == MaxRowLen)
			   {
				   if (ib != -1)
				   {
				       j -= (i - ib);
				       i = ib + 1;
				       ib = -1;
				   }
				   b[j] = 0;
				   Lines.Add (b);
				   j = 0;
 		           b = new char [MaxRowLen + 2];
			   }

               b[j] = Buffer[i];
			   j ++;
		   }
		   if (j > 0)
		   {
			       b[j] = 0;
				   Lines.Add (b);
		   }

           Text Tlines;
		   char *t;
		   if ((t = (char*) Lines.GetNext ()) != NULL)
		   {
			   Tlines = t;
		   }
		   while ((t = (char*) Lines.GetNext ()) != NULL)
		   {
			   Tlines = Tlines + "\n" + t;
		   }
		   Lines.DestroyAll ();
           SetBuffer ((char *) Tlines.GetBuffer ());
}

	       

int Text::Find (LPCSTR Substr)
{
          char *p;
          
          p = strstr (Buffer, Substr);
          if (p == NULL)
          {
              return -1;
          }
          return  (int) (p - Buffer);
}


