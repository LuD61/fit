#include "stdafx.h"
#include "ChoiceAkrz.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceAkrz::Sort1 = -1;
int CChoiceAkrz::Sort2 = -1;
int CChoiceAkrz::Sort3 = -1;
int CChoiceAkrz::Sort4 = -1;

CChoiceAkrz::CChoiceAkrz(CWnd* pParent) 
        : CChoiceX(pParent)
{
	Where = "";
	Bean.ArchiveName = _T("AkrzList.prp");
}

CChoiceAkrz::~CChoiceAkrz() 
{
	DestroyList ();
}

void CChoiceAkrz::DestroyList() 
{
	for (std::vector<CAkrzList *>::iterator pabl = AkrzList.begin (); pabl != AkrzList.end (); ++pabl)
	{
		CAkrzList *abl = *pabl;
		delete abl;
	}
    AkrzList.clear ();
}

void CChoiceAkrz::FillList () 
{
    long  a_krz;
    double a;
    TCHAR a_bz1 [50];

	DestroyList ();
	ClearList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl �ber Kurznummern"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("PLU"),                 1, 150, LVCFMT_RIGHT);
    SetCol (_T("Artikel-Nr"),          2, 150, LVCFMT_RIGHT);
    SetCol (_T("Artikelbezeichnung"),  3, 250);
	SortRow = 1;

	if (AkrzList.size () == 0)
	{
		DbClass->sqlout ((long *) &a_krz,       SQLLONG, 0);
		DbClass->sqlout ((double *) &a,         SQLDOUBLE, 0);
		DbClass->sqlout ((LPTSTR)  a_bz1,       SQLCHAR, sizeof (a_bz1));

		CString Sql = _T("select a_krz.a_krz, a_bas.a, a_bas.a_bz1 "
					     "from a_krz,a_bas where a_krz.a_krz > 0 and a_bas.a = a_krz.a");
		Sql += " ";
		Sql += Where;

		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
/*
		int cursor = DbClass->sqlcursor (_T("select a_krz.a_krz, a_bas.a, a_bas.a_bz1 "
						         "from a_krz,a_bas where a_krz.a_krz > 0 and a_bas.a = a_krz.a"));
*/
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) a_bz1;
			CDbUniCode::DbToUniCode (a_bz1, pos);
			CAkrzList *abl = new CAkrzList (a_krz, a, a_bz1);
			AkrzList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
		Load ();
	}

	for (std::vector<CAkrzList *>::iterator pabl = AkrzList.begin (); pabl != AkrzList.end (); ++pabl)
	{
		CAkrzList *abl = *pabl;
		CString Akrz;
		Akrz.Format (_T("%ld"), abl->a_krz); 
		CString Num;
		CString Bez;
		CString Art;
		Art.Format (_T("%.0lf"), abl->a); 
		_tcscpy (a_bz1, abl->a_bz1); 

		CString LText;
		LText.Format (_T("%.0lf %s"), abl->a_krz, 
									  abl->a_bz1.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Akrz;
        }
        else
        {
                Num = LText;
        }

        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (Art.GetBuffer (), i, 2);
        ret = SetItemText (a_bz1, i, 3);
        i ++;
    }

    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceAkrz::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceAkrz::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceAkrz::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAkrz::SearchA (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAkrz::SearchABz1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceAkrz::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
	if ((EditText.Find (_T("*")) != -1) || EditText.Find (_T("?")) != -1)
	{
		CChoiceX::SearchMatch (EditText);
		return;
	}
    switch (SortRow)
    {
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (13));
             break;
        case 2 :
             SearchA (ListBox, EditText.GetBuffer (13));
             break;
        case 3 :
             EditText.MakeUpper ();
             SearchABz1 (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceAkrz::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceAkrz::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 1)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort3;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort3);
	   }
	   return 0;
   }
   else if (SortRow == 3)
   {
	  return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   return 0;
}


void CChoiceAkrz::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CAkrzList *abl = AkrzList [i];
		   
		   abl->a_krz = _tstol (ListBox->GetItemText (i, 1));
		   abl->a  = _tstof (ListBox->GetItemText (i, 2));
		   abl->a_bz1 = ListBox->GetItemText (i, 3);
	}
	for (int i = 1; i <= 9; i ++)
	{
		SetItemSort (i, 2);
	}
	SetItemSort (SortRow, SortPos);
}

void CChoiceAkrz::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = AkrzList [idx];
}

CAkrzList *CChoiceAkrz::GetSelectedText ()
{
	CAkrzList *abl = (CAkrzList *) SelectedRow;
	return abl;
}

void CChoiceAkrz::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (AkrzList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}

void CChoiceAkrz::SendSelect ()
{
	pParent->SendMessage (WM_COMMAND, AKRZSELECTED, 0l);
}

void CChoiceAkrz::SendCancel ()
{
	pParent->SendMessage (WM_COMMAND, AKRZCANCELED, 0l);
}

void CChoiceAkrz::SetDefault ()
{
	CChoiceX::SetDefault ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    ListBox->SetColumnWidth (0, 0);
    ListBox->SetColumnWidth (1, 150);
    ListBox->SetColumnWidth (2, 150);
    ListBox->SetColumnWidth (3, 250);
}
