#include "StdAfx.h"
#include "QuidList.h"

CQuidList::CQuidList(void)
{
	quid_nr = 0;
	bezeichnung = _T("");
}

CQuidList::CQuidList(short quid_nr, LPTSTR bezeichnung)
{
	this->quid_nr     = quid_nr;
	this->bezeichnung = bezeichnung;
}

CQuidList::~CQuidList(void)
{
}
