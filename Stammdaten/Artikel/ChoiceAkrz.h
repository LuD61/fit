#ifndef _CHOICEAKRZ_DEF
#define _CHOICEAKRZ_DEF

#define AKRZSELECTED 5005
#define AKRZCANCELED 5006

#include "ChoiceX.h"
#include "AkrzList.h"
#include <vector>

class CChoiceAkrz : public CChoiceX
{
    protected :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
      
    public :
		CString Where;
	    std::vector<CAkrzList *> AkrzList;
	    std::vector<CAkrzList *> SelectList;
      	CChoiceAkrz(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceAkrz(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchA (CListCtrl *,  LPTSTR);
        void SearchABz1 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
	    virtual void SaveSelection (CListCtrl *);
		virtual void SendSelect (); 
		virtual void SendCancel (); 
		CAkrzList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		virtual void SetDefault ();
};
#endif
