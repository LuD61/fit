#pragma once
#include <vector>
#include "editlistctrl.h"
#include "mdn.h"
#include "fil.h"
#include "gr_zuord.h"
#include "Adr.h"
#include "a_pr.h"
#include "A_bas.h"
#include "ChoiceMdn.h"
#include "Choicefil.h"
#include "ChoiceGrZuord.h"

#define MAXLISTROWS 30

class CArtPrListCtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	enum AUFSCHLAG
	{
		NO	     = 0,
		BUTTON   = 1,
		LIST     = 2,
		ALL      = 3,
	};

	enum MODE
	{
		STANDARD = 0,
		TERMIN   = 1,
	};

	
	enum LISTMODE
	{
		Large = 0,
		Compact = 1,
	};

	enum
	{
		LargeWidth = 120,
		CompactWidth = 40,
	};

	enum LISTPOS
	{
		POSMDNGR = 1,
		POSMDN    = 2,
		POSFILGR = 3,
		POSFIL  = 4,
		POSPREK     = 5,
		POSPRVK     = 6,
		POSPREKAB   = 7,
		POSPRVKAB   = 8,
		POSSMT      = 9,
		POSAKT      = 10,
		POSACTIVE   = 11,
		POSPREKKALK     = 12,
		POSPRVKKALK     = 13,
		POSSK     = 14,
		POSSP     = 15,
		POSPRVK1    = 16,
		POSPRVK2    = 17,
		POSPRVK3    = 18,
	};

	BOOL ShowAbverk;
	void SetShowAbverk (BOOL ShowAbverk)
	{
		this->ShowAbverk = ShowAbverk;
	}

	BOOL GetShowAbverk ()
	{
		return ShowAbverk;
	}

	BOOL EnterBasis;
	void SetEnterBasis (BOOL EnterBasis)
	{
		this->EnterBasis = EnterBasis;
	}

	BOOL GetEnterBasis ()
	{
		return EnterBasis;
	}

	int PosMdnGr;
    int PosMdn;
	int PosFilGr;
	int PosFil;
	int PosPrEk;
	int PosPrVk;
	int PosPrEkAb;
	int PosPrVkAb;
	int PosSmt;
	int PosAkt;
	int PosActive;
	int PosPrEkKalk;
	int PosPrVkKalk;
	int PosSk;
	int PosSp;
	int PosPrVk1;
	int PosPrVk2;
	int PosPrVk3;

    int *Position[21];

	int ListMode;
	int CompanyWidth;

	short m_Mdn;
	int FilGrCursor;
	int FilCursor;
	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	int oldsel;
	std::vector<BOOL> vSelect;
	CVector MdnGrCombo;
	CVector MdnCombo;
	CVector FilGrCombo;
	CVector FilCombo;
	CChoiceGrZuord *ChoiceMdnGr;
	BOOL ModalChoiceMdnGr;
	BOOL MdnGrChoiceStat;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	BOOL MdnChoiceStat;
	CChoiceGrZuord *ChoiceFilGr;
	BOOL ModalChoiceFilGr;
	BOOL FilGrChoiceStat;
	CChoiceFil *ChoiceFil;
	BOOL ModalChoiceFil;
	BOOL FilChoiceStat;
	CVector ListRows;
	double sk_vollk;
	double spanne;
	double fil_ek_vollk;
	double fil_vk_vollk;

	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	FIL_CLASS Fil;
	GR_ZUORD_CLASS Gr_zuord;
	ADR_CLASS FilAdr;
	A_BAS_CLASS *A_bas;
	void SetABas (A_BAS_CLASS *A_bas)
	{
		this->A_bas = A_bas;
	}
	CArtPrListCtrl(void);
	~CArtPrListCtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	void FillMdnGrCombo (CVector&);
	void FillMdnCombo (CVector&);
	void FillFilGrCombo (CVector&);
	void FillFilCombo (CVector&);
	void OnChoice ();
	void OnMdnGrChoice (CString &);
	void OnMdnChoice (CString &);
	void OnFilGrChoice (CString &);
	void OnFilChoice (CString &);
	void OnKey9 ();
    void ReadMdnGr ();
    void ReadMdn ();
    void ReadFilGr ();
    void ReadFil ();
	void TestSmt ();
    void FillFilGrCombo (int row);
    void FillFilCombo (int row);
    void ReadFilCombo (int mdn);
    void ReadFilGrCombo (int mdn);
    void GetColValue (int row, int col, CString& Text);
    void TestAprIndex ();
    BOOL TestAprIndexM (int EditRow);
	void ScrollPositions (int pos);
	BOOL LastCol ();
	void Prepare ();
	void SetListMode (int ListMode);
	void CalculateAbverk ();
};
