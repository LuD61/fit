#pragma once
#include "BasisDatenPage.h"
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "a_bas.h"
#include "a_bas_erw.h"
#include "a_hndw.h"
#include "a_eig.h"
#include "ptabn.h"
#include "atext.h"
#include "A_kun_gx.h"
#include "pr_a_gx.h"
#include "DbUniCode.h"
#include "afxwin.h"
#include "ChoiceWG.h"
#include "vector"
#include "ImageCtrl.h"
#include "ImageListener.h"
#include "dbclass.h"
#include "wg.h"
#include "karton.h"
#include "afxdtctl.h"



#define IDC_WG1CHOICE 3011
#define IDC_WG2CHOICE 3012
#define IDC_WG3CHOICE 3013
#define IDC_WG4CHOICE 3014
#define IDC_WG5CHOICE 3015

// CProductPage6-Dialogfeld

class CProductPage6 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CProductPage6)

public:
	CProductPage6();
	virtual ~CProductPage6();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCT_PAGE_SHOP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()

	std::vector<long *> TxtTab;
	std::vector<LPSTR> PrAuszTab;
public:
	int tabx;
	int taby;
	BOOL HideButtons;
	CBasisdatenPage *Basis;
	CWnd *Frame;
	CFormTab Form;
	CFormTab DisableEigs;
	CCtrlGrid CtrlGrid;
	CCtrlGrid WG1Grid;
	CCtrlGrid WG2Grid;
	CCtrlGrid WG3Grid;
	CCtrlGrid WG4Grid;
	CCtrlGrid WG5Grid;

	CFont Font;
	CFont BoldFont;
	COLORREF BoldColor;
	BOOL ProductPage6Read;
	CImageCtrl m_ImageArticle;



	A_BAS_CLASS *A_bas;
	A_BAS_ERW_CLASS *A_bas_erw;
	A_HNDW_CLASS *A_hndw;
	A_EIG_CLASS *A_eig;
	A_KRZ_CLASS *A_krz;
	PTABN_CLASS Ptabn;
	ATEXTE_CLASS Atexte;
	A_KUN_GX_CLASS A_kun_gx;
	PR_A_GX_CLASS Pr_ausz_gx;
	DB_CLASS *DbClass;
	WG_CLASS Wg;
	KARTON_CLASS Karton;


public:

	virtual void UpdatePage ();
	virtual void OnDelete ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeydown ();
    virtual BOOL OnKeyup ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
    virtual void OnCopy ();
    virtual void OnPaste ();
	void OnWGchoice ();
	void OnInitWgBez(short wgInput);
    void FillCombo (LPTSTR Item, CWnd *control);
    void FillVerpackungCombo (void);
	void ReadWGBez();
	void SetVerpackung();

	BOOL Read ();
	CTextEdit m_a_bz01;
	CTextEdit m_a_bz02;
	CStatic m_LA_bz1;
	CStatic m_LVerpackung;
	CStatic m_LInhKarton;
	CNumEdit m_InhKarton;
	CStatic m_LPackungKarton;
	CNumEdit m_PackungKarton;
	CStatic m_LPack_Vol;
	CNumEdit m_Pack_Vol;
	CButton m_WGChoice;
	CChoiceWG *ChoiceWG;


	afx_msg void OnEnChangeABz02();
	CStatic m_LShopAG1;
	CStatic m_LShopAG2;
	CStatic m_LShopAG3;
	CStatic m_LShopAG4;
	CStatic m_LShopAG5;
	CEdit m_ShopAG1;
	CEdit m_ShopAG2;
	CEdit m_ShopAG3;
	CEdit m_ShopAG4;
	CEdit m_ShopAG5;
	CEdit m_ShopAG1_Bez;
	CEdit m_ShopAG2_Bez;
	CEdit m_ShopAG3_Bez;
	CEdit m_ShopAG4_Bez;
	CEdit m_ShopAG5_Bez;
	CEdit m_ShopWG1;
	CEdit m_ShopWG2;
	CEdit m_ShopWG3;
	CEdit m_ShopWG4;
	CEdit m_ShopWG5;
	CEdit m_ShopHWG1;
	CEdit m_ShopHWG2;
	CEdit m_ShopHWG3;
	CEdit m_ShopHWG4;
	CEdit m_ShopHWG5;
	CEdit m_Shop;
	CEdit m_ShopWG1_Bez;
	CEdit m_ShopWG2_Bez;
	CEdit m_ShopWG3_Bez;
	CEdit m_ShopWG4_Bez;
	CEdit m_ShopWG5_Bez;
	CEdit m_ShopHWG1_Bez;
	CEdit m_ShopHWG2_Bez;
	CEdit m_ShopHWG3_Bez;
	CEdit m_ShopHWG4_Bez;
	CEdit m_ShopHWG5_Bez;
	CComboBox m_CShopWG;
	CComboBox m_CShopWG2;
	CComboBox m_CShopWG3;
	CComboBox m_CShopWG4;
	CComboBox m_CShopWG5;
	CStatic m_LWGShopZuordnungen;
	CNumEdit m_SWG1;
	CNumEdit m_SWG2;
	CNumEdit m_SWG3;
	CNumEdit m_SWG4;
	CNumEdit m_SWG5;
	CStatic m_LSWG1;
	CStatic m_LSWG2;
	CStatic m_LSWG3;
	CStatic m_LSWG4;
	CStatic m_LSWG5;
	CEdit m_SWG1_BEZ;
	CEdit m_SWG2_BEZ;
	CEdit m_SWG3_BEZ;
	CEdit m_SWG4_BEZ;
	CEdit m_SWG5_BEZ;
	afx_msg void OnEnChangeShopwg1();
	afx_msg void OnBnClickedChSwg1();
	afx_msg void OnChoiceWg(int wg);
	CButton m_SWGButton1;
	CButton m_SWGButton2;
	CButton m_SWGButton3;
	CButton m_SWGButton4;
	CButton m_SWGButton5;
	afx_msg void OnBnClickedChSwg2();
	afx_msg void OnBnClickedChSwg3();
	afx_msg void OnBnClickedChSwg4();
	afx_msg void OnBnClickedChSwg5();
	CEdit m_ShopWGBez1;
	CEdit m_ShopWGBez2;
	CEdit m_ShopWGBez3;
	CEdit m_ShopWGBez4;
	CEdit m_ShopWGBez5;
	CButton m_ShopAktion;
	CButton m_ShopTV;
	CStatic m_LShopAktion;
	CStatic m_LShopNeu;
	CStatic m_LShopTV;
	CStatic m_LShopAGew;
	CNumEdit m_ShopAGew;
	CStatic m_LLPack;
	CNumEdit m_LPack;
	CStatic m_LHPack;
	CNumEdit m_HPack;
	CStatic m_LBPack;
	CNumEdit m_BPack;
	afx_msg void OnEnKillfocusLpack();
	afx_msg void OnEnKillfocusHpack();
	afx_msg void OnEnKillfocusBpack();
	afx_msg void OnEnKillfocusPackVol();
	CComboBox m_ComboVerpackung;
	afx_msg void OnCbnSelchangeComboverpackung();
	afx_msg void OnCbnDblclkComboverpackung();
	afx_msg void OnBnClickedShoptv();
	afx_msg void OnDtnDatetimechangeDatetimepicker1(NMHDR *pNMHDR, LRESULT *pResult);
	CDateTimeCtrl m_DateTimeShopNeu;
};
