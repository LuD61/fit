#include "stdafx.h"
#include "a_ean.h"

struct A_EAN a_ean, a_ean_null, a_ean_def;

void A_EAN_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_ean.a,  SQLDOUBLE, 0);
            sqlin ((double *)   &a_ean.ean,  SQLDOUBLE, 0);
    sqlout ((double *) &a_ean.a,SQLDOUBLE,0);
    sqlout ((short *) &a_ean.delstatus,SQLSHORT,0);
    sqlout ((double *) &a_ean.ean,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_ean.ean_bz,SQLCHAR,25);
    sqlout ((TCHAR *) a_ean.h_ean_kz,SQLCHAR,2);
    sqlout ((short *) &a_ean.ean_vk_kz,SQLSHORT,0);
            cursor = sqlcursor (_T("select a_ean.a,  ")
_T("a_ean.delstatus,  a_ean.ean,  a_ean.ean_bz,  a_ean.h_ean_kz,  ")
_T("a_ean.ean_vk_kz from a_ean ")

#line 13 "a_ean.rpp"
                                  _T("where a = ? and ean = ?"));
    sqlin ((double *) &a_ean.a,SQLDOUBLE,0);
    sqlin ((short *) &a_ean.delstatus,SQLSHORT,0);
    sqlin ((double *) &a_ean.ean,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_ean.ean_bz,SQLCHAR,25);
    sqlin ((TCHAR *) a_ean.h_ean_kz,SQLCHAR,2);
    sqlin ((short *) &a_ean.ean_vk_kz,SQLSHORT,0);
            sqltext = _T("update a_ean set a_ean.a = ?,  ")
_T("a_ean.delstatus = ?,  a_ean.ean = ?,  a_ean.ean_bz = ?,  ")
_T("a_ean.h_ean_kz = ?,  a_ean.ean_vk_kz = ? ")

#line 15 "a_ean.rpp"
                                  _T("where a = ? and ean = ?");
            sqlin ((double *)   &a_ean.a,  SQLDOUBLE, 0);
            sqlin ((double *)   &a_ean.ean,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_ean.a,  SQLDOUBLE, 0);
            sqlin ((double *)   &a_ean.ean,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_ean ")
                                  _T("where a = ? and ean = ? for update"));
            sqlin ((double *)   &a_ean.a,  SQLDOUBLE, 0);
            sqlin ((double *)   &a_ean.a,  SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_ean ")
                                  _T("set delstatus = 0 where a = ? and ean = ? and delstatus = 0 for update"));
            sqlin ((double *)   &a_ean.a,  SQLDOUBLE, 0);
            sqlin ((double *)   &a_ean.ean,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_ean ")
                                  _T("where a = ? and ean = ?"));
    sqlin ((double *) &a_ean.a,SQLDOUBLE,0);
    sqlin ((short *) &a_ean.delstatus,SQLSHORT,0);
    sqlin ((double *) &a_ean.ean,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_ean.ean_bz,SQLCHAR,25);
    sqlin ((TCHAR *) a_ean.h_ean_kz,SQLCHAR,2);
    sqlin ((short *) &a_ean.ean_vk_kz,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into a_ean (a,  ")
_T("delstatus,  ean,  ean_bz,  h_ean_kz,  ean_vk_kz) ")

#line 33 "a_ean.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?)"));

#line 35 "a_ean.rpp"
            sqlin ((double *)   &a_ean.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_ean.a,SQLDOUBLE,0);
    sqlout ((short *) &a_ean.delstatus,SQLSHORT,0);
    sqlout ((double *) &a_ean.ean,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_ean.ean_bz,SQLCHAR,25);
    sqlout ((TCHAR *) a_ean.h_ean_kz,SQLCHAR,2);
    sqlout ((short *) &a_ean.ean_vk_kz,SQLSHORT,0);
            acursor = sqlcursor (_T("select a_ean.a,  ")
_T("a_ean.delstatus,  a_ean.ean,  a_ean.ean_bz,  a_ean.h_ean_kz,  ")
_T("a_ean.ean_vk_kz from a_ean ")

#line 37 "a_ean.rpp"
                                  _T("where a = ?"));
            sqlin ((double *)   &a_ean.ean,  SQLDOUBLE, 0);
    sqlout ((double *) &a_ean.a,SQLDOUBLE,0);
    sqlout ((short *) &a_ean.delstatus,SQLSHORT,0);
    sqlout ((double *) &a_ean.ean,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_ean.ean_bz,SQLCHAR,25);
    sqlout ((TCHAR *) a_ean.h_ean_kz,SQLCHAR,2);
    sqlout ((short *) &a_ean.ean_vk_kz,SQLSHORT,0);
            eancursor = sqlcursor (_T("select a_ean.a,  ")
_T("a_ean.delstatus,  a_ean.ean,  a_ean.ean_bz,  a_ean.h_ean_kz,  ")
_T("a_ean.ean_vk_kz from a_ean ")

#line 40 "a_ean.rpp"
                                  _T("where ean = ?"));
}

int A_EAN_CLASS::dbreadafirst ()
{
	    if (acursor == -1)
            {
		prepare ();
            }	
	    if (acursor == -1) return -1;
            int dsqlstatus = sqlopen (acursor);
            if (dsqlstatus != 0) return dsqlstatus;
            return sqlfetch (acursor);
}

int A_EAN_CLASS::dbreada ()
{
            return sqlfetch (acursor);
}

int A_EAN_CLASS::dbreadeanfirst ()
{
	    if (eancursor == -1)
            {
		prepare ();
            }	
	    if (eancursor == -1) return -1;
            int dsqlstatus = sqlopen (eancursor);
            if (dsqlstatus != 0) return dsqlstatus;
            return sqlfetch (eancursor);
}

int A_EAN_CLASS::dbreadean ()
{
            return sqlfetch (eancursor);
}
