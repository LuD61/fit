#ifndef _DBUNICODE_DEF
#define _DBUNICODE_DEF

#pragma once
#include "codeproperties.h"

class CDbUniCode
{
public:
	CDbUniCode(void);
	~CDbUniCode(void);
	static void DbToUniCode (LPTSTR tstr, LPSTR str);
    static void DbFromUniCode (LPTSTR tstr, LPSTR str, int len);
	static void DbToUniCode (CCodeProperties& Code, LPTSTR tstr, LPSTR str, int len);
	static void DbFromUniCode (CCodeProperties& Code, LPTSTR tstr, LPSTR str, int len);
    static void TrimRight (UCHAR *s);
};
#endif