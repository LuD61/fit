#pragma once
#include "paintlistctrl.h"
//#include "FillList.h"
#include "EditListCtrl.h"
#include "A_ean.h"
#include "A_krz.h"
#include "A_bas.h"

class CEanListCtrl :
//	public CPaintListCtrl
	public CEditListCtrl
{
public:
	int cursor;
    CImageList image; 
	A_BAS_CLASS *A_bas;
	A_EAN_CLASS *A_ean;
	A_KRZ_CLASS *A_krz;

	CEanListCtrl(void);
	~CEanListCtrl(void);

	CFillList FillList;

	void Init ();

	BOOL Read ();
};
