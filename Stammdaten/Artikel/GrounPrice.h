#pragma once
#include "afxwin.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "APreise.h"


// CGrounPrice-Dialogfeld

class CGrounPrice : public CDialog
{
	DECLARE_DYNAMIC(CGrounPrice)

public:
	CGrounPrice(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CGrounPrice();

// Dialogfelddaten
	enum { IDD = IDD_GROUNDPRICE };
private:
	int StdCellHeight;
	BOOL Initialized;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
//	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnOK ();

	DECLARE_MESSAGE_MAP()

	CFont Font;
	CFormTab Form;
	CCtrlGrid CtrlGrid;
	CCtrlGrid PriceGrid;

public:
	CStatic m_LPrice1;
public:
	CNumEdit m_GroundPrice1;
public:
	CStatic m_LPrice2;
public:
	CNumEdit m_GroundPrice2;
public:
	CStatic m_LPrice3;
public:
	CNumEdit m_GroundPrice3;
public:
	CStatic m_LPriceGroup;
public:
	CButton m_OK;
public:
	CButton m_Cancel;

	double Price1;
	double Price2;
	double Price3;
};
