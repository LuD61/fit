#include "stdafx.h"
#include "a_bas_prot.h"
#include "StrFuncs.h"

struct A_BAS_PROT a_bas_prot, a_bas_prot_null;


void A_BAS_PROT_CLASS::prepare (void)
{

    sqlin ((DATE_STRUCT *) &a_bas_prot.prot_dat,SQLDATE,0);
    sqlin ((TCHAR *) a_bas_prot.prot_zeit,SQLCHAR,9);
    sqlin ((short *) &a_bas_prot.prot_akt,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas_prot.prot_pers,SQLCHAR,9);

    sqlin ((short *) &a_bas_prot.me_einh_kun,SQLSHORT,0);

    sqlin ((double *) &a_bas_prot.a,SQLDOUBLE,0);
    sqlin ((short *) &a_bas_prot.mdn,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.fil,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas_prot.a_bz1,SQLCHAR,25);
    sqlin ((TCHAR *) a_bas_prot.a_bz2,SQLCHAR,25);
    sqlin ((double *) &a_bas_prot.a_gew,SQLDOUBLE,0);
    sqlin ((short *) &a_bas_prot.a_typ,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.a_typ2,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.abt,SQLSHORT,0);
    sqlin ((long *) &a_bas_prot.ag,SQLLONG,0);
    sqlin ((TCHAR *) a_bas_prot.best_auto,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.bsd_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.cp_aufschl,SQLCHAR,2);
    sqlin ((short *) &a_bas_prot.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.dr_folge,SQLSHORT,0);
    sqlin ((long *) &a_bas_prot.erl_kto,SQLLONG,0);
    sqlin ((TCHAR *) a_bas_prot.hbk_kz,SQLCHAR,2);
    sqlin ((short *) &a_bas_prot.hbk_ztr,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas_prot.hnd_gew,SQLCHAR,2);
    sqlin ((short *) &a_bas_prot.hwg,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas_prot.kost_kz,SQLCHAR,3);
    sqlin ((short *) &a_bas_prot.me_einh,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas_prot.modif,SQLCHAR,2);
    sqlin ((short *) &a_bas_prot.mwst,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.plak_div,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas_prot.stk_lst_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas_prot.sw,SQLDOUBLE,0);
    sqlin ((short *) &a_bas_prot.teil_smt,SQLSHORT,0);
    sqlin ((long *) &a_bas_prot.we_kto,SQLLONG,0);
    sqlin ((short *) &a_bas_prot.wg,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.zu_stoff,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &a_bas_prot.akv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &a_bas_prot.bearb,SQLDATE,0);
    sqlin ((TCHAR *) a_bas_prot.pers_nam,SQLCHAR,9);
    sqlin ((double *) &a_bas_prot.prod_zeit,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas_prot.pers_rab_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas_prot.gn_pkt_gbr,SQLDOUBLE,0);
    sqlin ((long *) &a_bas_prot.kost_st,SQLLONG,0);
    sqlin ((TCHAR *) a_bas_prot.sw_pr_kz,SQLCHAR,2);
    sqlin ((long *) &a_bas_prot.kost_tr,SQLLONG,0);
    sqlin ((double *) &a_bas_prot.a_grund,SQLDOUBLE,0);
    sqlin ((long *) &a_bas_prot.kost_st2,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.we_kto2,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.charg_hand,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.intra_stat,SQLLONG,0);
    sqlin ((TCHAR *) a_bas_prot.qual_kng,SQLCHAR,5);
    sqlin ((TCHAR *) a_bas_prot.a_bz3,SQLCHAR,25);
    sqlin ((short *) &a_bas_prot.lief_einh,SQLSHORT,0);
    sqlin ((double *) &a_bas_prot.inh_lief,SQLDOUBLE,0);
    sqlin ((long *) &a_bas_prot.erl_kto_1,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.erl_kto_2,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.erl_kto_3,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.we_kto_1,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.we_kto_2,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.we_kto_3,SQLLONG,0);
    sqlin ((TCHAR *) a_bas_prot.skto_f,SQLCHAR,2);
    sqlin ((double *) &a_bas_prot.sk_vollk,SQLDOUBLE,0);
    sqlin ((double *) &a_bas_prot.a_ersatz,SQLDOUBLE,0);
    sqlin ((short *) &a_bas_prot.a_ers_kz,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.me_einh_abverk,SQLSHORT,0);
    sqlin ((double *) &a_bas_prot.inh_abverk,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas_prot.hnd_gew_abverk,SQLCHAR,2);
    sqlin ((double *) &a_bas_prot.inh_ek,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas_prot.smt,SQLCHAR,2);
    sqlin ((double *) &a_bas_prot.a_leih,SQLDOUBLE,0);
    sqlin ((short *) &a_bas_prot.vk_gr,SQLSHORT,0);
    sqlin ((long *) &a_bas_prot.txt_nr1,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.txt_nr2,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.txt_nr3,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.txt_nr4,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.txt_nr5,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.txt_nr6,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.txt_nr7,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.txt_nr8,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.txt_nr9,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.txt_nr10,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.allgtxt_nr1,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.allgtxt_nr2,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.allgtxt_nr3,SQLLONG,0);
    sqlin ((long *) &a_bas_prot.allgtxt_nr4,SQLLONG,0);
    sqlin ((TCHAR *) a_bas_prot.bild,SQLCHAR,129);
    sqlin ((short *) &a_bas_prot.fil_ktrl,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.sais,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.bereich,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas_prot.lgr_tmpr,SQLCHAR,21);
    sqlin ((TCHAR *) a_bas_prot.prod_mass,SQLCHAR,31);
    sqlin ((TCHAR *) a_bas_prot.produkt_info,SQLCHAR,513);
    sqlin ((TCHAR *) a_bas_prot.huel_art,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.huel_mat,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.ausz_art,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.ean_packung,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.ean_karton,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.pr_ausz,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.huel_peel,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.gruener_punkt,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.gebinde_art,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.gebinde_mass,SQLCHAR,31);
    sqlin ((TCHAR *) a_bas_prot.gebinde_leer_gew,SQLCHAR,11);
    sqlin ((TCHAR *) a_bas_prot.gebinde_brutto_gew,SQLCHAR,11);
    sqlin ((short *) &a_bas_prot.gebinde_pack,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.gebindeprolage,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.gebindeanzlagen,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.gebindepalette,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas_prot.beffe,SQLCHAR,21);
    sqlin ((TCHAR *) a_bas_prot.beffe_fe,SQLCHAR,21);
    sqlin ((TCHAR *) a_bas_prot.brennwert,SQLCHAR,21);
    sqlin ((double *) &a_bas_prot.eiweiss,SQLDOUBLE,0);
    sqlin ((double *) &a_bas_prot.kh,SQLDOUBLE,0);
    sqlin ((double *) &a_bas_prot.fett,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas_prot.aerob,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.coli,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.keime,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.listerien,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.list_mono,SQLCHAR,51);
    sqlin ((TCHAR *) a_bas_prot.prodpass_extra,SQLCHAR,513);
    sqlin ((TCHAR *) a_bas_prot.glg,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.krebs,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.ei,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.fisch,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.erdnuss,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.soja,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.milch,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.schal,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.sellerie,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.senfsaat,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.sesamsamen,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.sulfit,SQLCHAR,2);
    sqlin ((short *) &a_bas_prot.zerl_eti,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.restlaufzeit,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas_prot.vakuumiert,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.loskennung,SQLCHAR,81);
    sqlin ((DATE_STRUCT *) &a_bas_prot.stand,SQLDATE,0);
    sqlin ((TCHAR *) a_bas_prot.version_nr,SQLCHAR,6);
    sqlin ((TCHAR *) a_bas_prot.gueltigkeit,SQLCHAR,21);
    sqlin ((TCHAR *) a_bas_prot.weichtier,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.sonstige,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas_prot.gmo_gvo,SQLCHAR,513);
    sqlin ((double *) &a_bas_prot.pack_vol,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas_prot.shop_kz,SQLCHAR,2);
    sqlin ((short *) &a_bas_prot.min_bestellmenge,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.staffelung,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.inh_karton,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.packung_karton,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.filialsperre,SQLSHORT,0);
    sqlin ((short *) &a_bas_prot.ghsperre,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into a_bas_prot (prot_dat,prot_zeit,prot_akt,prot_pers, me_einh_kun,a, ")
_T("mdn,  fil,  a_bz1,  a_bz2,  a_gew,  a_typ,  a_typ2,  abt,  ag,  best_auto,  bsd_kz,  ")
_T("cp_aufschl,  delstatus,  dr_folge,  erl_kto,  hbk_kz,  hbk_ztr,  hnd_gew,  hwg,  ")
_T("kost_kz,  me_einh,  modif,  mwst,  plak_div,  stk_lst_kz,  sw,  teil_smt,  we_kto,  wg,  ")
_T("zu_stoff,  akv,  bearb,  pers_nam,  prod_zeit,  pers_rab_kz,  gn_pkt_gbr,  ")
_T("kost_st,  sw_pr_kz,  kost_tr,  a_grund,  kost_st2,  we_kto2,  charg_hand,  ")
_T("intra_stat,  qual_kng,  a_bz3,  lief_einh,  inh_lief,  erl_kto_1,  erl_kto_2,  ")
_T("erl_kto_3,  we_kto_1,  we_kto_2,  we_kto_3,  skto_f,  sk_vollk,  a_ersatz,  ")
_T("a_ers_kz,  me_einh_abverk,  inh_abverk,  hnd_gew_abverk,  inh_ek,  smt,  ")
_T("a_leih,  vk_gr,  txt_nr1,  txt_nr2,  txt_nr3,  txt_nr4,  txt_nr5,  txt_nr6,  txt_nr7,  ")
_T("txt_nr8,  txt_nr9,  txt_nr10,  allgtxt_nr1,  allgtxt_nr2,  allgtxt_nr3,  ")
_T("allgtxt_nr4,  bild,  fil_ktrl,  sais,  bereich,  lgr_tmpr,  prod_mass,  ")
_T("produkt_info,  huel_art,  huel_mat,  ausz_art,  ean_packung,  ean_karton,  ")
_T("pr_ausz,  huel_peel,  gruener_punkt,  gebinde_art,  gebinde_mass,  ")
_T("gebinde_leer_gew,  gebinde_brutto_gew,  gebinde_pack,  gebindeprolage,  ")
_T("gebindeanzlagen,  gebindepalette,  beffe,  beffe_fe,  brennwert,  eiweiss,  kh,  ")
_T("fett,  aerob,  coli,  keime,  listerien,  list_mono,  prodpass_extra,  glg,  krebs,  ei,  ")
_T("fisch,  erdnuss,  soja,  milch,  schal,  sellerie,  senfsaat,  sesamsamen,  sulfit,  ")
_T("zerl_eti,  restlaufzeit,  vakuumiert,  loskennung,  stand,  version_nr,  ")
_T("gueltigkeit,  weichtier,  sonstige,  gmo_gvo,  pack_vol,  shop_kz,  ")
_T("min_bestellmenge,  staffelung,  inh_karton,  packung_karton,  ")
_T("filialsperre,  ghsperre) ")

                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

}

int A_BAS_PROT_CLASS::dbreadfirst ()
{
	return 100;
}

int A_BAS_PROT_CLASS::dbread ()
{
	return 100;
}

int A_BAS_PROT_CLASS::dblock ()
{
	return 0;
}

int A_BAS_PROT_CLASS::dbupdate ()
{
	return 0;
}

int A_BAS_PROT_CLASS::dbupdate (short aktivitaet )
{
        if (ins_cursor == -1)
        {
               prepare ();
        }

		CString Time;
		CString Date;
		CStrFuncs::SysDate (Date);
		ToDbDate (Date, &a_bas_prot.prot_dat);
		CStrFuncs::SysTime (Time);
    	strcpy (a_bas_prot.prot_zeit, Time.GetBuffer (8));
		a_bas_prot.prot_akt = aktivitaet;

        int  dsqlstatus = sqlexecute (ins_cursor);
        return dsqlstatus;
}

int A_BAS_PROT_CLASS::dbdelete ()
{
        return 0;
}

void A_BAS_PROT_CLASS::dbclose ()
{
        int dsqlstatus = sqlclose (ins_cursor); 
 }
