#ifndef _A_VARB_DEF
#define _A_VARB_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_VARB {
   double         a;
   TCHAR          a_kz[3];
   double         arb_zeit_fakt;
   double         befe;
   double         befe_i_fett;
   short          delstatus;
   double         eiw_ges;
   short          fil;
   double         fett;
   double         fett_eiw;
   double         fwp;
   double         h2o;
   TCHAR          leits[10];
   long           mat;
   short          mdn;
   short          tier;
   short          vpk_typ;
   short          quid_nr;
};
extern struct A_VARB a_varb, a_varb_null;

#line 8 "a_varb.rh"

class A_VARB_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_VARB a_varb;  
               A_VARB_CLASS () : DB_CLASS ()
               {
               }
};
#endif
