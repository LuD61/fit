#pragma once
#include "dbpropertysheet.h"

class CProductPassSheet :
	public CDbPropertySheet
{
	DECLARE_DYNAMIC(CProductPassSheet)
public:
	CProductPassSheet(void);
	~CProductPassSheet(void);
	virtual BOOL Write ();
    virtual void StepBack ();
	virtual BOOL Delete ();
//	virtual BOOL Print ();
};
