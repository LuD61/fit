#pragma once
#include "BasisDatenPage.h"
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "a_bas.h"
#include "prp_zusatz.h"
#include "hst_info.h"
#include "DklPflichtListCtrl.h"
#include "FillList.h"
#include "vector"


// CHkstInfo-Dialogfeld

#define MAXENTRIES 30

class CHkstInfo : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CHkstInfo)

public:

	CHkstInfo();
	virtual ~CHkstInfo();

// Dialogfelddaten
	enum { IDD = IDD_HKST_INFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()

public:
	int tabx;
	int taby;
	BOOL HideButtons;
	CBasisdatenPage *Basis;
	CWnd *Frame;
	CFormTab Form;
	CFormTab DisableEigs;
	CCtrlGrid CtrlGrid;
	CFont Font;
	CFont BoldFont;
	COLORREF BoldColor;
	CString ZusNames[MAXENTRIES];
	char *ZusRecordNames [MAXENTRIES];
	char *ZusValues[MAXENTRIES];
    BOOL ProductPage5Read;

	A_BAS_CLASS *A_bas;
	HST_INFO_CLASS Hst_info;
	PRP_ZUSATZ_CLASS Prp_zusatz;
	CFillList FillList;
	int cursor;
	int name_cursor;
	int delcursor;

	int ZutIdx;
	int ZutSource;
	CStatic m_LRhStoffe;
	CTextEdit m_RhStoffe;
	CStatic m_LHerstellung;
	CTextEdit m_Herstellung;
	CStatic m_LZbrEmpf;
	CTextEdit m_ZbrEmpf;
	CStatic m_LBlgEmpf;
	CTextEdit m_BlgEmpf;
	CStatic m_LDklPflicht;
	CDklPflichtListCtrl m_DklPflicht;

	BOOL Read ();
	BOOL Write ();
	BOOL Delete ();

	virtual void UpdatePage ();
	virtual void OnDelete ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeydown ();
    virtual BOOL OnKeyup ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
	void FillNames ();
	void ToRecordNames ();
	void FromRecordNames ();
    virtual void OnCopy ();
    virtual void OnPaste ();
};
