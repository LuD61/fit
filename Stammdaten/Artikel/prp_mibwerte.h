#ifndef _PRP_MIBWERTE_DEF
#define _PRP_MIBWERTE_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct PRP_MIBWERTE {
   double         a;
   TCHAR          name1[49];
   TCHAR          wert1[513];
   TCHAR          name2[49];
   TCHAR          wert2[513];
   TCHAR          name3[49];
   TCHAR          wert3[513];
   TCHAR          name4[49];
   TCHAR          wert4[513];
   TCHAR          name5[49];
   TCHAR          wert5[513];
   TCHAR          name6[49];
   TCHAR          wert6[513];
   TCHAR          name7[49];
   TCHAR          wert7[513];
   TCHAR          name8[49];
   TCHAR          wert8[513];
   TCHAR          name9[49];
   TCHAR          wert9[513];
   TCHAR          name10[49];
   TCHAR          wert10[513];
   TCHAR          name11[49];
   TCHAR          wert11[513];
   TCHAR          name12[49];
   TCHAR          wert12[513];
   TCHAR          name13[49];
   TCHAR          wert13[513];
   TCHAR          name14[49];
   TCHAR          wert14[513];
   TCHAR          name15[49];
   TCHAR          wert15[513];
   TCHAR          name16[49];
   TCHAR          wert16[513];
   TCHAR          name17[49];
   TCHAR          wert17[513];
   TCHAR          name18[49];
   TCHAR          wert18[513];
   TCHAR          name19[49];
   TCHAR          wert19[513];
   TCHAR          name20[49];
   TCHAR          wert20[513];
   TCHAR          name21[49];
   TCHAR          wert21[513];
   TCHAR          name22[49];
   TCHAR          wert22[513];
   TCHAR          name23[49];
   TCHAR          wert23[513];
   TCHAR          name24[49];
   TCHAR          wert24[513];
   TCHAR          name25[49];
   TCHAR          wert25[513];
   TCHAR          name26[49];
   TCHAR          wert26[513];
   TCHAR          name27[49];
   TCHAR          wert27[513];
   TCHAR          name28[49];
   TCHAR          wert28[513];
   TCHAR          name29[49];
   TCHAR          wert29[513];
   TCHAR          name30[49];
   TCHAR          wert30[513];
};
extern struct PRP_MIBWERTE prp_mibwerte, prp_mibwerte_null;

#line 8 "prp_mibwerte.rh"

class PRP_MIBWERTE_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PRP_MIBWERTE prp_mibwerte;
               PRP_MIBWERTE_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (PRP_MIBWERTE&);  
};
#endif
