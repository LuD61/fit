#ifndef _CHOICELIEFBEST_DEF
#define _CHOICELIEFBEST_DEF

#define LIEFBESTSELECTED 5007
#define LIEFBESTCANCELED 5008

#include "ChoiceX.h"
#include "LiefBestList.h"
#include <vector>

class CChoiceLiefBest : public CChoiceX
{
    protected :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
      
    public :
		CString Where;
	    std::vector<CLiefBestList *> LiefBestList;
	    std::vector<CLiefBestList *> SelectList;
      	CChoiceLiefBest(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceLiefBest(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchLief (CListCtrl *,  LPTSTR);
        void SearchAdrKrz (CListCtrl *,  LPTSTR);
        void SearchBz (CListCtrl *, LPTSTR);
        void SearchA (CListCtrl *,  LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
	    virtual void SaveSelection (CListCtrl *);
		virtual void SendSelect (); 
		virtual void SendCancel (); 
		CLiefBestList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
		virtual void SetDefault ();
		virtual void OnEnter ();
};
#endif
