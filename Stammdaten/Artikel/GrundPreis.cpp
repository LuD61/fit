// GrundPreis.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "GrundPreis.h"
#include ".\grundpreis.h"
#include "DbUniCode.h"


// CGrundPreis-Dialogfeld

IMPLEMENT_DYNAMIC(CGrundPreis, CDialog)
CGrundPreis::CGrundPreis(CWnd* pParent /*=NULL*/)
	: CDialog(CGrundPreis::IDD, pParent)
{
	Pe = "";
	Ekd = 0.0;
	Ek = 0.0;
	Sk = 0.0;
	DlgBkColor = RGB (255, 255, 221);
	DlgBrush = NULL;
	EditBrush = NULL;
}

CGrundPreis::~CGrundPreis()
{
	if (EditBrush != NULL)
	{
		DeleteObject (EditBrush);
	}
	if (IsWindow (m_hWnd))
	{
		DestroyWindow ();
	}
}

void CGrundPreis::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LPE, m_LPe);
	DDX_Control(pDX, IDC_PE, m_Pe);
	DDX_Control(pDX, IDC_LEKD, m_LEkd);
	DDX_Control(pDX, IDC_EKD, m_Ekd);
	DDX_Control(pDX, IDC_LEK, m_LEk);
	DDX_Control(pDX, IDC_EK, m_Ek);
	DDX_Control(pDX, IDC_LSK, m_LSk);
	DDX_Control(pDX, IDC_SK, m_Sk);
	DDX_Control(pDX, IDC_SP_VK, m_SpVk);
}


BEGIN_MESSAGE_MAP(CGrundPreis, CDialog)
	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()


// CGrundPreis-Meldungshandler

BOOL CGrundPreis::OnInitDialog ()
{
	CDialog::OnInitDialog ();

    Form.Add (new CFormField (&m_Pe,    EDIT,        (CString *) &Pe,  VSTRING));
    Form.Add (new CFormField (&m_Ekd,   EDIT,        (double *) &Ekd,  VDOUBLE, 6,2));
    Form.Add (new CFormField (&m_Ek,    EDIT,        (double *) &Ek,   VDOUBLE,8,4));
    Form.Add (new CFormField (&m_Sk,    EDIT,        (double *) &Sk,   VDOUBLE,7,3));
    Form.Add (new CFormField (&m_SpVk,  EDIT,        (double *) &SpVk, VDOUBLE,5,1));

	Form.Show ();
	return TRUE;
}

HBRUSH CGrundPreis::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{

		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;

	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
		    if ((pWnd == &m_LPe) || (pWnd == &m_LEkd) || (pWnd == &m_LEk) ||
				(pWnd == &m_LSk))
			{
				pDC->SetTextColor (RGB (0, 0, 255));
				pDC->SetBkColor (DlgBkColor);
			}
		    else if ((pWnd == &m_Pe) || (pWnd == &m_Ekd) || (pWnd == &m_Ek) ||
				(pWnd == &m_Sk) || (pWnd == &m_SpVk))
			{
				pDC->SetBkColor (RGB (255, 255, 255));
				if (EditBrush == NULL)
				{
					EditBrush = CreateSolidBrush (RGB (255, 255, 255));
				}
				return EditBrush;
			}
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CGrundPreis::Show (short mdn, short fil, double a, short a_typ, short me_einh)
{

	memcpy (&Ptabn.ptabn, &ptabn_null, sizeof (PTABN));
	memcpy (&A_kalkhndw.a_kalkhndw, &a_kalkhndw_null, sizeof (A_KALKHNDW));
	memcpy (&A_kalk_eig.a_kalk_eig, &a_kalk_eig_null, sizeof (A_KALK_EIG));
	_stprintf (Ptabn.ptabn.ptwert, _T("%hd"), me_einh);
	_tcscpy (Ptabn.ptabn.ptitem, _T("me_einh"));
	CDbUniCode::DbFromUniCode (Ptabn.ptabn.ptitem, (LPSTR) Ptabn.ptabn.ptitem, sizeof (Ptabn.ptabn.ptitem));
	CDbUniCode::DbFromUniCode (Ptabn.ptabn.ptwert, (LPSTR) Ptabn.ptabn.ptwert, sizeof (Ptabn.ptabn.ptwert));
    Ptabn.dbreadfirst ();
	CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, (LPSTR) Ptabn.ptabn.ptbezk);
	Pe = Ptabn.ptabn.ptbezk;
	Pe.Trim ();

	int dsqlstatus = 0;
	Ekd = 0.0;
	Ek = 0.0;
	Sk = 0.0;
	if (a_typ == Hndw)
	{
		m_LEkd.SetWindowText (_T("EK-Durchschnitt"));
		m_LEk.SetWindowText (_T("EK-aktuell"));
		m_LSk.SetWindowText (_T("SK/Sp-VK"));
		A_kalkhndw.a_kalkhndw.mdn = mdn;
		A_kalkhndw.a_kalkhndw.fil = fil;
		A_kalkhndw.a_kalkhndw.a   = a;
		dsqlstatus = A_kalkhndw.dbreadfirst ();
		if (dsqlstatus == 100 && A_kalkhndw.a_kalkhndw.fil > 0)
		{
			A_kalkhndw.a_kalkhndw.fil = 0;
			dsqlstatus = A_kalkhndw.dbreadfirst ();
		}
		if (dsqlstatus == 100 && A_kalkhndw.a_kalkhndw.mdn > 0)
		{
			A_kalkhndw.a_kalkhndw.mdn = 0;
			dsqlstatus = A_kalkhndw.dbreadfirst ();
		}
		if (dsqlstatus == 0)
		{
			Ekd = (A_kalkhndw.a_kalkhndw.pr_ek1  * A_kalkhndw.a_kalkhndw.we_me1 + 
				   A_kalkhndw.a_kalkhndw.pr_ek2  * A_kalkhndw.a_kalkhndw.we_me2 + 
				   A_kalkhndw.a_kalkhndw.pr_ek3  * A_kalkhndw.a_kalkhndw.we_me3) 
				   / 
				   (A_kalkhndw.a_kalkhndw.we_me1 + 
					A_kalkhndw.a_kalkhndw.we_me2 + 
					A_kalkhndw.a_kalkhndw.we_me3);
			Ek   = A_kalkhndw.a_kalkhndw.pr_ek1;
			Sk   = A_kalkhndw.a_kalkhndw.sk_vollk;
			SpVk = A_kalkhndw.a_kalkhndw.sp_vk;
		}
	}
	else if (a_typ == Eig)
	{
		m_LEkd.SetWindowText (_T("Mat.o.B"));
		m_LEk.SetWindowText (_T("Mat.m.B"));
		m_LSk.SetWindowText (_T("SK/Sp-VK"));
		A_kalk_eig.a_kalk_eig.mdn = mdn;
		A_kalk_eig.a_kalk_eig.fil = fil;
		A_kalk_eig.a_kalk_eig.a   = a;
		dsqlstatus = A_kalk_eig.dbreadfirst ();
		if (dsqlstatus == 100 && A_kalk_eig.a_kalk_eig.fil > 0)
		{
			A_kalk_eig.a_kalk_eig.fil = 0;
			dsqlstatus = A_kalk_eig.dbreadfirst ();
		}
		if (dsqlstatus == 100 && A_kalk_eig.a_kalk_eig.mdn > 0)
		{
			A_kalk_eig.a_kalk_eig.mdn = 0;
			dsqlstatus = A_kalk_eig.dbreadfirst ();
		}
		if (dsqlstatus == 0)
		{
			Ekd = A_kalk_eig.a_kalk_eig.mat_o_b;
			Ek = A_kalk_eig.a_kalk_eig.hk_vollk;
			Sk = A_kalk_eig.a_kalk_eig.sk_vollk;
			SpVk = A_kalk_eig.a_kalk_eig.sp_vk;
		}
	}

	Form.Show ();
}

void CGrundPreis::SetVisible (BOOL visible)
{
	if (visible)
	{
		ShowWindow (SW_SHOWNORMAL);
		long style = GetWindowLong (m_Pe.m_hWnd, GWL_STYLE);
		style |= WS_BORDER;
		SetWindowLong (m_Pe.m_hWnd, GWL_STYLE, style);
		m_Pe.UpdateWindow ();
		m_Pe.ShowWindow (SW_SHOWNORMAL);
	}
	else
	{
		ShowWindow (SW_HIDE);
	}
}

