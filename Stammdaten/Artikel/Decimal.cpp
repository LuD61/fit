#include "StdAfx.h"
#include "decimal.h"

CDecimal::CDecimal(void)
{
	sValue = _T("0.0");
	dValue = 0.0;
	long Scale = 0;
	long Precision = 0;
	int sCount = 4;
	CString Format = _T("%.4lf");

}

CDecimal::CDecimal(double dValue, int sCount)
{
	this->dValue = dValue;
	this->sCount = sCount;
	StringToDec ();
	Format.Format (_T("%c.dlf"), sCount);
	sValue.Format (Format, dValue);
}

CDecimal::CDecimal(CString& sValue, int sCount)
{
	dValue = 0.0;
	this->sValue = sValue;
	this->sCount = sCount;
	StringToDec ();
	Format.Format (_T("%c.%dlf"), _T('%'), sCount);
	this->sValue.Format (Format, dValue);
	int pPos = this->sValue.Find (_T(","));
	if (pPos != -1)
	{
		this->sValue.GetBuffer ()[pPos] = _T('.');
		dValue = _tstof (this->sValue);
	}
}

void CDecimal::StringToDec ()
{
	int pPos = sValue.Find (_T("."));
	if (pPos == -1)
	{
		pPos = sValue.Find (_T(","));
	}
	if (pPos == -1)
	{
		Precision = 0;
		ToScale (sValue);
	}
	else
	{
        CString scValue = sValue.Left (pPos);
		ToScale (scValue);
		CString pValue  = sValue.Mid (pPos + 1, sCount);
		Precision = _tstol (pValue);
	}
}

void CDecimal::ToScale (CString& sScale)
{
	LPTSTR p = sScale.GetBuffer ();
	int len = sScale.Trim ().GetLength ();
	Scale = 0;
	for (int i = 0; i < len; i ++, p += 1)
	{
		TCHAR c = *p;
		if ((c < 0x30) || (c > 0x39)) break;
		c -= 0x30;
		Scale *= 10;
		Scale += c;
	}
}

CDecimal::~CDecimal(void)
{
}
