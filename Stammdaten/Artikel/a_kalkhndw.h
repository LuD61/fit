#ifndef _A_KALKHNDW_DEF
#define _A_KALKHNDW_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_KALKHNDW {
   double         a;
   short          bearb_fil;
   short          bearb_lad;
   short          bearb_sk;
   short          delstatus;
   short          fil;
   double         fil_ek_teilk;
   double         fil_ek_vollk;
   double         lad_vk_teilk;
   double         lad_vk_vollk;
   short          mdn;
   double         pr_ek1;
   double         pr_ek2;
   double         pr_ek3;
   double         sk_teilk;
   double         sk_vollk;
   double         sp_fil;
   double         sp_lad;
   double         sp_vk;
   double         we_me1;
   double         we_me2;
   double         we_me3;
   DATE_STRUCT    dat;
   DATE_STRUCT    aend_dat;
};
extern struct A_KALKHNDW a_kalkhndw, a_kalkhndw_null;

#line 8 "a_kalkhndw.rh"

class A_KALKHNDW_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_KALKHNDW a_kalkhndw;  
               A_KALKHNDW_CLASS () : DB_CLASS ()
               {
               }
};
#endif
