#include "StdAfx.h"
#include "choiceaquery.h"
#include "DbUniCode.h"

CChoiceAQuery::CChoiceAQuery(void)
{
	Query = _T("");
}

CChoiceAQuery::~CChoiceAQuery(void)
{
}

void CChoiceAQuery::FillList () 
{
    double  a;
    TCHAR a_bz1 [50];
    TCHAR a_bz2 [50];

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Artikel"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Artikel"),        1, 150, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung 1"),  2, 250);
    SetCol (_T("Bezeichnung 2"),  3, 250);

	if (ABasList.size () == 0)
	{
		DbClass->sqlout ((double *)&a,        SQLDOUBLE, 0);
		DbClass->sqlout ((LPTSTR)  a_bz1,     SQLCHAR, sizeof (a_bz1));
		DbClass->sqlout ((LPTSTR)  a_bz2,     SQLCHAR, sizeof (a_bz2));
		CString Sql = _T("select a, a_bz1, a_bz2 from a_bas ");
		if (Query != _T(""))
		{
			Sql += Query;
		}
		else
		{
			Sql += _T("where a > 0");
		}

		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) a_bz1;
			CDbUniCode::DbToUniCode (a_bz1, pos);
			pos = (LPSTR) a_bz2;
			CDbUniCode::DbToUniCode (a_bz2, pos);
			CABasList *abl = new CABasList (a, a_bz1, a_bz2, 
										    _T(""), _T(""), _T(""), 
											_T(""), _T(""), _T(""), _T(""));
			ABasList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CABasList *>::iterator pabl = ABasList.begin (); pabl != ABasList.end (); ++pabl)
	{
		CABasList *abl = *pabl;
		CString Art;
		Art.Format (_T("%.0lf"), abl->a); 
		CString Num;
		CString Bez;
		_tcscpy (a_bz1, abl->a_bz1.GetBuffer ());
		_tcscpy (a_bz2, abl->a_bz2.GetBuffer ());

		CString LText;
		LText.Format (_T("%.0lf %s"), abl->a, 
									  abl->a_bz1.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Art;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (a_bz1, i, 2);
        ret = SetItemText (a_bz2, i, 3);
        i ++;
    }

	SortRow = 2;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}
