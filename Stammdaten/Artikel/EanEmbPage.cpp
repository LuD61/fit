// EanEmbPage.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "EanEmbPage.h"


// CEanEmbPage-Dialogfeld

IMPLEMENT_DYNCREATE(CEanEmbPage, CDbPropertyPage)

// Meldungszuordnung

BEGIN_MESSAGE_MAP(CEanEmbPage, CDbPropertyPage)
	ON_WM_CTLCOLOR ()
	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CEanEmbPage::CEanEmbPage - Konstruktor

// TODO: Definieren Sie eine Zeichenfolgenressource f�r die Seitenbeschriftung. Ersetzen Sie '0' durch die ID.

CEanEmbPage::CEanEmbPage() 
	: CDbPropertyPage(CEanEmbPage::IDD)
{
	ArchiveName = _T("Artikel.prp");
	Load ();
}

CEanEmbPage::~CEanEmbPage() 
{
}


// CEanEmbPage::DoDataExchange - Verschiebt Daten zwischen der Seite und den Eigenschaften

void CEanEmbPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


// CEanEmbPage-Meldungshandler

BOOL CEanEmbPage::OnInitDialog()
{
	EanEmbListDlg.PageUpdate = Update;

	CPropertyPage::OnInitDialog();
	EanEmbListDlg.SetBasis (Basis);
	EanEmbListDlg.m_EanListDlg.DlgBkColor = DlgBkColor;
	EanEmbListDlg.m_EmbListDlg.DlgBkColor = DlgBkColor;
	EanEmbListDlg.Create (IDD_EAN_EMB_LIST, this);
	EanEmbListDlg.ShowWindow (SW_SHOWNORMAL);
	EanEmbListDlg.UpdateWindow ();


	EanEmb.DlgBkColor = DlgBkColor;
	EanEmb.m_Ean.DlgBkColor = DlgBkColor;
	EanEmb.m_Emb.DlgBkColor = DlgBkColor;
	EanEmb.SetBasis (Basis);
	EanEmb.Create (IDD_EAN_EMB_DATA, this);
	EanEmb.ShowWindow (SW_SHOWNORMAL);
	EanEmb.UpdateWindow ();

	EanEmb.m_Ean.AddUpdateEvent (&EanEmbListDlg.m_EanListDlg);
	EanEmb.m_Ean.AddUpdateEvent (&EanEmbListDlg.m_EmbListDlg);
	EanEmb.m_Emb.AddUpdateEvent (&EanEmbListDlg.m_EmbListDlg);
	EanEmbListDlg.m_EanListDlg.AddUpdateEvent (&EanEmb.m_Ean);
	EanEmbListDlg.m_EmbListDlg.AddUpdateEvent (&EanEmb.m_Emb);

	SplitPane.Create (this, 0, 0, &EanEmbListDlg, &EanEmb, 50, CSplitPane::Horizontal);

//	Ean.SetFocus ();


/*
	Ean.DlgBkColor = DlgBkColor;
	Ean.SetBasis (Basis);
	Ean.Create (IDD_EANDATA, this);
	Ean.ShowWindow (SW_SHOWNORMAL);
	Ean.UpdateWindow ();

	Ean.AddUpdateEvent (&EanEmbListDlg);
	EanEmbListDlg.AddUpdateEvent (&Ean);
	SplitPane.Create (this, 0, 0, &EanEmbListDlg, &Ean, 55, CSplitPane::Horizontal);

	Ean.SetFocus ();
*/
	CRect pRect;
    GetParent ()->GetParent ()->GetWindowRect (&pRect);
    GetParent ()->GetParent ()->GetParent ()->ScreenToClient (&pRect);
//    GetParent ()->MoveWindow (&pRect);

	return FALSE;
}

void CEanEmbPage::OnSize (UINT nType, int cx, int cy)
{
	if (IsWindow (EanEmbListDlg.m_hWnd))
	{
		CRect rect;
		EanEmbListDlg.GetClientRect (&rect);
		EanEmbListDlg.MoveWindow (0, 0, rect.right, cy);
		SplitPane.SetLength (cy);

		EanEmb.GetWindowRect (&rect);
		ScreenToClient (&rect);
		rect.right = cx;
		rect.bottom = cy;
		EanEmb.MoveWindow (&rect);
	}
}

HBRUSH CEanEmbPage::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CEanEmbPage::PreTranslateMessage(MSG* pMsg)
{

	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :

			if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
	}

	return FALSE;
}

void CEanEmbPage::SetBasis (CBasisdatenPage *Basis)
{
	this->Basis = Basis;
	EanEmbListDlg.SetBasis (Basis);
	EanEmb.SetBasis (Basis);
}

void CEanEmbPage::UpdatePage ()
{
	EanEmbListDlg.Read ();
//	EanEmb.Update ();
}

BOOL CEanEmbPage::OnSetActive ()
{
	UpdatePage ();
	return TRUE;
}


void CEanEmbPage::OnDelete ()
{
	EanEmb.m_Ean.Delete ();
	EanEmbListDlg.m_EanListDlg.Delete ();
	EanEmbListDlg.m_EmbListDlg.Delete ();
}
