#include "StdAfx.h"
#include "PrArtListCtrl.h"
#include "StrFuncs.h"
#include "resource.h"

CPrArtListCtrl::CPrArtListCtrl(void)
{
	PrGrStufCombo.Init ();
	KunPrCombo.Init ();
	ChoiceKun = NULL;
	ChoicePrGrStuf = NULL;
	ModalChoiceKun = TRUE;
	ChoiceIKunPr      = NULL;
	ModalChoiceIKunPr = TRUE;
	ModalChoicePrGrStuf = TRUE;
	m_Mdn = 1;
	PosPrGrStuf = POSGRPRSTUF;
    PosKunPr    = POSKUNPR;
	PosKun      = POSKUN;
	PosKunName  = POSKUNNAME;
	PosVkPr     = POSVKPR;
	PosLdPr     = POSLDPR;
	PosVkPrKalk = POSVKPRKALK;
	PosEkAbs	= POSEKABS;
	PosEkProz	= POSEKPROZ;
	PosSk       = POSSK;
	PosSp       = POSSP;

	Position[0] = &PosPrGrStuf;
	Position[1] = &PosKunPr;
	Position[2] = &PosKun;
	Position[3] = &PosKunName;
	Position[4] = &PosVkPr;
	Position[5] = &PosLdPr;
	Position[6] = &PosVkPrKalk;
	Position[7] = &PosEkAbs;
	Position[8] = &PosEkProz;
	Position[9] = &PosSk;
	Position[10] = &PosSp;
	Position[11] = NULL;
	MaxComboEntries = 20;

	Aufschlag = LIST;
	Mode = STANDARD;
	spanne = 0.0;
	ListRows.Init ();
}

CPrArtListCtrl::~CPrArtListCtrl(void)
{
	if (ChoiceKun != NULL)
	{
		delete ChoiceKun;
	}
	if (ChoiceIKunPr != NULL)
	{
		delete ChoiceIKunPr;
	}

	if (ChoicePrGrStuf != NULL)
	{
		delete ChoicePrGrStuf;
	}

	CString *c;
    PrGrStufCombo.FirstPosition ();
	while ((c = (CString *) PrGrStufCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	PrGrStufCombo.Init ();
    KunPrCombo.FirstPosition ();
	while ((c = (CString *) KunPrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	KunPrCombo.Init ();
}

BEGIN_MESSAGE_MAP(CPrArtListCtrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CPrArtListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (PosVkPr, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (PosPrGrStuf, 0);
	}
}

void CPrArtListCtrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
	if (col == PosKunName) return;
	if (col == PosVkPrKalk) return;
	if (col == PosPrGrStuf)
	{
		if (PrGrStufCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &PrGrStufCombo);
			oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}
	else if (col == PosKunPr)
	{
		if (KunPrCombo.GetCount () <= MaxComboEntries)
		{
			if (IsWindow (ListComboBox.m_hWnd))
			{
				StopEnter ();
			}
			CEditListCtrl::StartEnterCombo (col, row, &KunPrCombo);
		    oldsel = ListComboBox.GetCurSel ();
		}
		else
		{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
		}
	}

	else if (col == PosKun)
	{
		if (IsWindow (SearchListCtrl.Edit.m_hWnd))
		{
			StopEnter ();
		}
		CEditListCtrl::StartSearchListCtrl (col, row);
	}
	else
	{
		CEditListCtrl::StartEnter (col, row);
	}
}

void CPrArtListCtrl::StopEnter ()
{

//	CEditListCtrl::StopEnter ();

	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		int count = GetItemCount ();
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
	}
	PerformListChangeHandler (EditRow, EditCol);
}

void CPrArtListCtrl::SetSel (CString& Text)
{

   if (EditCol == PosKun || EditCol == PosVkPr ||
	   EditCol == PosLdPr || EditCol == PosEkAbs ||
	   EditCol == PosEkProz)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CPrArtListCtrl::FormatText (CString& Text)
{
    if (EditCol == PosVkPr)
	{
		DoubleToString (StrToDouble (Text), Text, 4);
	}
    else if (EditCol == PosLdPr)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
    else if (EditCol == PosEkAbs)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
    else if (EditCol == PosEkProz)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
/*
    else if (EditCol == 4)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
    else if (EditCol == 5)
	{
		DatFormat (Text, "dd.mm.yyyy");
	}
*/
}

void CPrArtListCtrl::NextRow ()
{
	CString VkPr = GetItemText (EditRow, PosVkPr);
	CString LdPr = GetItemText (EditRow, PosLdPr);
	if ((StrToDouble (VkPr) == 0.0) &&
			(StrToDouble (LdPr) == 0.0))
	{
		return;
	}
	int count = GetItemCount ();
	if (EditCol == PosKun)
	{
		ReadKunName ();
	}
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
	SetEditText ();
	TestIprIndex ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;

	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
/*
	if (GetKeyState (VK_SHIFT) < 0)
	{
		BOOL &b = vSelect[EditRow - 1];
		b = TRUE;
		FillList.SetItemImage (EditRow - 1, 1);
	}
	else if (GetKeyState (VK_CONTROL) == 0)
	{
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
	}
*/
}

void CPrArtListCtrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();
	if (EditRow == count - 1)
	{
		CString VkPr = GetItemText (EditRow, PosVkPr);
		CString LdPr = GetItemText (EditRow, PosLdPr);
		if ((StrToDouble (VkPr) == 0.0) &&
			(StrToDouble (LdPr) == 0.0))
		{
	        DeleteItem (EditRow);
		}
	}
	else
	{
		if (EditRow <= 0)
		{
			return;
		}
		if (EditCol == PosKun)
		{
			ReadKunName ();
		}
		else if (EditCol == PosKunPr)
		{
			ReadKunPr ();
		}
		else if (EditCol == PosPrGrStuf)
		{
			ReadPrGrStuf ();
		}
		TestIprIndex ();
	}
	StopEnter ();
 
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CPrArtListCtrl::LastCol ()
{
	if (EditCol < PosLdPr) return FALSE;
	if (Mode == TERMIN && EditCol >= PosLdPr)
	{
		return TRUE;
	}
	if (Aufschlag == LIST || Aufschlag == ALL)
	{
		if (EditCol < PosEkProz) return FALSE;
	}
	return TRUE;
}

void CPrArtListCtrl::OnReturn ()
{

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{
		CString VkPr = GetItemText (EditRow, PosVkPr);
		CString LdPr = GetItemText (EditRow, PosLdPr);
		if ((StrToDouble (VkPr) == 0.0) &&
			(StrToDouble (LdPr) == 0.0))
		{
			EditCol --;
			return;
		}
	}
	if (EditCol == PosKun)
	{
		ReadKunName ();
	}
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
	if (LastCol ())
	{
		TestIprIndex ();
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
//		EditCol = 0;
		if (EditRow == rowCount)
		{
			EditCol = PosPrGrStuf;
		}
		else
		{
			EditCol = PosVkPr;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
		if (EditCol == PosKunName)
		{
			EditCol ++;
		}
		else if (EditCol == PosVkPrKalk)
		{
			EditCol ++;
		}
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CPrArtListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	if (EditCol == PosKun)
	{
		ReadKunName ();
	}
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
	StopEnter ();
	EditCol ++;
	if (EditCol == PosKunName)
	{
		EditCol ++;
	}
	else if (EditCol == PosVkPrKalk)
	{
		EditCol ++;
	}
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CPrArtListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	if (EditCol == PosKun)
	{
		ReadKunName ();
	}
	else if (EditCol == PosKunPr)
	{
		ReadKunPr ();
	}
	else if (EditCol == PosPrGrStuf)
	{
		ReadPrGrStuf ();
	}
	StopEnter ();
	EditCol --;
	if (EditCol == PosKunName)
	{
		EditCol --;
	}
	else if (EditCol == PosVkPrKalk)
	{
		EditCol --;
	}
    StartEnter (EditCol, EditRow);
}

BOOL CPrArtListCtrl::InsertRow ()
{
	CString VkPr = GetItemText (EditRow, PosVkPr);
	CString LdPr = GetItemText (EditRow, PosLdPr);
	if ((GetItemCount () > 0) && 
		(StrToDouble (VkPr) == 0.0) &&
			(StrToDouble (LdPr) == 0.0))
	{
		return FALSE;
	}
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (_T("0 "), EditRow, PosPrGrStuf);
	FillList.SetItemText (_T("0 "), EditRow, PosKunPr);
	FillList.SetItemText (_T("0"), EditRow, PosKun);
	FillList.SetItemText (_T(""), EditRow, PosKunName);
	FillList.SetItemText (_T("0,0 "), EditRow, PosVkPr);
	FillList.SetItemText (_T("0,0 "), EditRow, PosLdPr);
	FillList.SetItemText (_T("0,00 "), EditRow, PosEkAbs);
	FillList.SetItemText (_T("0,00 "), EditRow, PosEkProz);

	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CPrArtListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	return CEditListCtrl::DeleteRow ();
}

BOOL CPrArtListCtrl::AppendEmpty ()
{

	int rowCount = GetItemCount ();
	if (rowCount > 0)
	{
		CString VkPr = GetItemText (EditRow, PosVkPr);
		CString LdPr = GetItemText (EditRow, PosLdPr);
		if ((StrToDouble (VkPr) == 0.0) &&
			(StrToDouble (LdPr) == 0.0))
		{
			return FALSE;
		}
	}
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (rowCount, -1);
	FillList.SetItemText (_T("0 "), rowCount, PosPrGrStuf);
	FillList.SetItemText (_T("0 "), rowCount, PosKunPr);
	FillList.SetItemText (_T("0"), rowCount, PosKun);
	FillList.SetItemText (_T(""), rowCount, PosKunName);
	FillList.SetItemText (_T("0,0 "), rowCount, PosVkPr);
	FillList.SetItemText (_T("0,0 "), rowCount, PosLdPr);
	FillList.SetItemText (_T("0,00 "), rowCount, PosEkAbs);
	FillList.SetItemText (_T("0,00 "), rowCount, PosEkProz);
	rowCount = GetItemCount ();
	return TRUE;
}

void CPrArtListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CPrArtListCtrl::FillPrGrStufCombo (CVector& Values)
{
	CString *c;
    PrGrStufCombo.FirstPosition ();
	while ((c = (CString *) PrGrStufCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	PrGrStufCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		PrGrStufCombo.Add (c);
	}
}

void CPrArtListCtrl::FillKunPrCombo (CVector& Values)
{
	CString *c;
    KunPrCombo.FirstPosition ();
	while ((c = (CString *) KunPrCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	KunPrCombo.Init ();
	Values.FirstPosition ();
	while ((c = (CString *) Values.GetNext ()) != NULL)
	{
		KunPrCombo.Add (c);
	}
}

void CPrArtListCtrl::RunItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
*/
}

void CPrArtListCtrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CPrArtListCtrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CPrArtListCtrl::OnChoice ()
{
	if (EditCol == PosKun)
	{
		OnKunChoice (CString (_T("")));
	}
	else if (EditCol == PosKunPr)
	{
		OnIKunPrChoice (CString (_T("")));
	}
	else if (EditCol == PosPrGrStuf)
	{
		OnPrGrStufChoice (CString (_T("")));
	}
}

void CPrArtListCtrl::OnKunChoice (CString& Search)
{
	KunChoiceStat = TRUE;
	if (ChoiceKun != NULL && !ModalChoiceKun)
	{
		ChoiceKun->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceKun == NULL)
	{
		ChoiceKun = new CChoiceKun (this);
	    ChoiceKun->IsModal = ModalChoiceKun;
		ChoiceKun->HideFilter = FALSE;
		ChoiceKun->HideEnter = FALSE;
		ChoiceKun->CreateDlg ();
	}

    ChoiceKun->SetDbClass (&Kun);
	ChoiceKun->m_Mdn = m_Mdn;
	ChoiceKun->SearchText = Search;
	if (ModalChoiceKun)
	{
			ChoiceKun->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceKun->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceKun->MoveWindow (&rect);
		ChoiceKun->SetFocus ();
		return;
	}
    if (ChoiceKun->GetState ())
    {
		  CKunList *abl = ChoiceKun->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  KunChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&Kun.kun, &kun_null, sizeof (KUN));
		  memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
		  Kun.kun.mdn = abl->mdn;
		  Kun.kun.kun = abl->kun;
		  if (Kun.dbreadfirst () == 0)
		  {
			  KunAdr.adr.adr = Kun.kun.adr1;
			  KunAdr.dbreadfirst ();
		  }
		  CString Text;
		  Text.Format (_T("%ld"), Kun.kun.kun);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
		  CString KunName;
		  KunName.Format (_T("%s"), KunAdr.adr.adr_krz);
		  FillList.SetItemText (KunName.GetBuffer (), EditRow, PosKunName);
    }
	else
	{
		 KunChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}


void CPrArtListCtrl::OnIKunPrChoice (CString& Search)
{
    IKunPrChoiceStat = TRUE;
	if (ChoiceIKunPr != NULL && !ModalChoiceIKunPr)
	{
		ChoiceIKunPr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceIKunPr == NULL)
	{
		ChoiceIKunPr = new CChoiceIKunPr (this);
	    ChoiceIKunPr->IsModal = ModalChoiceIKunPr;
		ChoiceIKunPr->CreateDlg ();
	}

    ChoiceIKunPr->SetDbClass (&Kun);
	ChoiceIKunPr->m_Mdn = m_Mdn;
//	ChoiceIKunPr->Rows = &KunPrCombo;
	ChoiceIKunPr->SearchText = Search;
	if (ModalChoiceIKunPr)
	{
			ChoiceIKunPr->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceIKunPr->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceIKunPr->MoveWindow (&rect);
		ChoiceIKunPr->SetFocus ();
		return;
	}
    if (ChoiceIKunPr->GetState ())
    {
		  CIKunPrList *abl = ChoiceIKunPr->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  IKunPrChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&I_kun_prk.i_kun_prk, &i_kun_prk, sizeof (I_KUN_PRK));
		  I_kun_prk.i_kun_prk.mdn = abl->mdn;
		  I_kun_prk.i_kun_prk.kun_pr = abl->kun_pr;
		  I_kun_prk.dbreadfirst ();
		  CString Text;
//		  Text.Format (_T("%ld  %s"), I_kun_prk.i_kun_prk.kun_pr,
//			                          I_kun_prk.i_kun_prk.zus_bz);
		  Text.Format (_T("%ld"), I_kun_prk.i_kun_prk.kun_pr);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
/*
		  CString ZusBz;
		  ZusBz.Format (_T("%s"), I_kun_prk.i_kun_prk.zus_bz);
		  FillList.SetItemText (ZusBz.GetBuffer (), EditRow, PosKunName);
*/
    }
	else
	{
  	     IKunPrChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}

void CPrArtListCtrl::OnPrGrStufChoice (CString& Search)
{
    PrGrStufChoiceStat = TRUE;
	if (ChoicePrGrStuf != NULL && !ModalChoicePrGrStuf)
	{
		ChoicePrGrStuf->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoicePrGrStuf == NULL)
	{
		ChoicePrGrStuf = new CChoicePrGrStuf (this);
	    ChoicePrGrStuf->IsModal = ModalChoicePrGrStuf;
		ChoicePrGrStuf->CreateDlg ();
	}

    ChoicePrGrStuf->SetDbClass (&Kun);
	ChoicePrGrStuf->m_Mdn = m_Mdn;
	ChoicePrGrStuf->SearchText = Search;
	if (ModalChoicePrGrStuf)
	{
			ChoicePrGrStuf->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoicePrGrStuf->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoicePrGrStuf->MoveWindow (&rect);
		ChoicePrGrStuf->SetFocus ();
		return;
	}
    if (ChoicePrGrStuf->GetState ())
    {
		  CPrGrStufList *abl = ChoicePrGrStuf->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  PrGrStufChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk, sizeof (IPRGRSTUFK));
		  Iprgrstufk.iprgrstufk.mdn = abl->mdn;
		  Iprgrstufk.iprgrstufk.pr_gr_stuf = abl->pr_gr_stuf;
		  Iprgrstufk.dbreadfirst ();
		  CString Text;
		  Text.Format (_T("%ld"), Iprgrstufk.iprgrstufk.pr_gr_stuf);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
    }
	else
	{
	     PrGrStufChoiceStat = FALSE;
		 SearchListCtrl.Edit.SetFocus ();
	     CString Text;
		 SearchListCtrl.Edit.GetWindowText (Text);
         SetSearchSel (Text);
	}
}

void CPrArtListCtrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}

void CPrArtListCtrl::ReadKunName ()
{
	if (EditCol != PosKun) return;
    memcpy (&Kun.kun, &kun_null, sizeof (KUN));
	memcpy (&KunAdr.adr, &adr_null, sizeof (ADR));
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		return;
	}
    CString Text;
	SearchListCtrl.Edit.GetWindowText (Text);
	if (!CStrFuncs::IsDecimal (Text))
	{
		OnKunChoice (Text);
	    SearchListCtrl.Edit.GetWindowText (Text);
		Text.Format (_T("%ld"), atol (Text.GetBuffer ()));
	    SearchListCtrl.Edit.SetWindowText (Text);
		if (!KunChoiceStat)
		{
			EditCol --;
			return;
		}
	}
	if (atol (Text) == 0l) return;
    Kun.kun.mdn = m_Mdn;
	Kun.kun.kun = atol (Text);
	if (Kun.dbreadfirst () == 0)
    {
		  KunAdr.adr.adr = Kun.kun.adr1;
		  KunAdr.dbreadfirst ();
	}
	else if (Kun.kun.kun != 0l)
	{
		MessageBox (_T("Kunde nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return;
	}
    CString KunName;
	KunName.Format (_T("%s"), KunAdr.adr.adr_krz);
	FillList.SetItemText (KunName.GetBuffer (), EditRow, PosKunName);

    memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
    Iprgrstufk.iprgrstufk.mdn = m_Mdn;
	Iprgrstufk.iprgrstufk.pr_gr_stuf = Kun.kun.pr_stu;
	if (Iprgrstufk.dbreadfirst () != 0 &&
        Iprgrstufk.iprgrstufk.pr_gr_stuf != 0)
	{
		return;
	}
	Text.Format (_T("%ld %s"), Iprgrstufk.iprgrstufk.pr_gr_stuf, Iprgrstufk.iprgrstufk.zus_bz);
	SetItemText (EditRow, PosPrGrStuf, Text);

    I_kun_prk.i_kun_prk.mdn = m_Mdn;
	I_kun_prk.i_kun_prk.kun_pr = Kun.kun.pr_lst;
	if (I_kun_prk.dbreadfirst () != 0 &&
        I_kun_prk.i_kun_prk.kun_pr != 0)
	{
		return;
	}
	Text.Format (_T("%ld %s"), I_kun_prk.i_kun_prk.kun_pr, I_kun_prk.i_kun_prk.zus_bz);
	SetItemText (EditRow, PosKunPr, Text);

}

void CPrArtListCtrl::ReadPrGrStuf ()
{
	if (EditCol != PosPrGrStuf) return;
    memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
    CString Text;
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		return;
	}

	SearchListCtrl.Edit.GetWindowText (Text);
	if (!CStrFuncs::IsDecimal (Text))
	{
		OnPrGrStufChoice (Text);
	    SearchListCtrl.Edit.GetWindowText (Text);
	}
    Iprgrstufk.iprgrstufk.mdn = m_Mdn;
	Iprgrstufk.iprgrstufk.pr_gr_stuf = atol (Text);
	if (Iprgrstufk.dbreadfirst () != 0 &&
        Iprgrstufk.iprgrstufk.pr_gr_stuf != 0)
	{
		MessageBox (_T("Preisgruppenstufe nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return;
	}
	Text.Format (_T("%ld %s"), Iprgrstufk.iprgrstufk.pr_gr_stuf, Iprgrstufk.iprgrstufk.zus_bz);
    SearchListCtrl.Edit.SetWindowText (Text);
}

void CPrArtListCtrl::ReadKunPr ()
{
	if (EditCol != PosKunPr) return;
    memcpy (&I_kun_prk.i_kun_prk, &i_kun_prk_null, sizeof (I_KUN_PRK));
	if (!IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		return;
	}
    CString Text;
	SearchListCtrl.Edit.GetWindowText (Text);
	if (!CStrFuncs::IsDecimal (Text))
	{
		OnIKunPrChoice (Text);
	    SearchListCtrl.Edit.GetWindowText (Text);
	}
	if (atol (Text) == 0l) return;
    I_kun_prk.i_kun_prk.mdn = m_Mdn;
	I_kun_prk.i_kun_prk.kun_pr = atol (Text);
	if (I_kun_prk.dbreadfirst () != 0 &&
        I_kun_prk.i_kun_prk.kun_pr != 0)
	{
		MessageBox (_T("Kundengruppe nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		EditCol --;
		return;
	}
	Text.Format (_T("%ld %s"), I_kun_prk.i_kun_prk.kun_pr, I_kun_prk.i_kun_prk.zus_bz);
    SearchListCtrl.Edit.SetWindowText (Text);

	Text.Format ("%d", I_kun_prk.i_kun_prk.pr_gr_stuf);
    memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
    Iprgrstufk.iprgrstufk.mdn = m_Mdn;
	Iprgrstufk.iprgrstufk.pr_gr_stuf = atol (Text);
	if (Iprgrstufk.dbreadfirst () != 0 &&
        Iprgrstufk.iprgrstufk.pr_gr_stuf != 0)
	{
		return;
	}
	Text.Format (_T("%ld %s"), Iprgrstufk.iprgrstufk.pr_gr_stuf, Iprgrstufk.iprgrstufk.zus_bz);
	SetItemText (EditRow, PosPrGrStuf, Text);
}

void CPrArtListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	if (col == PosPrGrStuf)
	{
		Text = cText.Tokenize (" ", pos);
	}
	else if (col == PosKunPr)
	{
		Text = cText.Tokenize (" ", pos);
	}
	else
	{
		Text = cText.Trim ();
	}
}

void CPrArtListCtrl::TestIprIndex ()
{
	int Items = GetItemCount ();
	if (Items <= 1) return;
	CString Value;
	GetColValue (EditRow, PosPrGrStuf, Value);
	long rPrGrStuf = atol (Value);
	GetColValue (EditRow, PosKunPr, Value);
	long rKunPr = atol (Value);
	GetColValue (EditRow, PosKun, Value);
	long rKun = atol (Value);
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosPrGrStuf, Value);
		long lPrGrStuf = atol (Value);
		GetColValue (i, PosKunPr, Value);
		long lKunPr = atol (Value);
		GetColValue (i, PosKun, Value);
		long lKun = atol (Value);
		if (lKun == rKun && rKun != 0)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
		else if (lKunPr == rKunPr && rKunPr != 0)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
		else if (lPrGrStuf == rPrGrStuf && lKunPr == rKunPr
				&& lKun == rKun)
		{
			DeleteItem (i);
			InvalidateRect (NULL);
			if ( i < EditRow) EditRow --;
		}
	}
}

void CPrArtListCtrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}


