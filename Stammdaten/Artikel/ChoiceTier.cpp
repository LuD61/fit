#include "stdafx.h"
#include "ChoiceTier.h"
#include "DbUniCode.h"
#include "Process.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceTier::Sort1 = -1;
int CChoiceTier::Sort2 = -1;
int CChoiceTier::Sort3 = -1;
int CChoiceTier::Sort4 = -1;

BEGIN_MESSAGE_MAP(CChoiceTier, CChoiceX)
END_MESSAGE_MAP()

CChoiceTier::CChoiceTier(CWnd* pParent) 
        : CChoiceX(pParent)
{
	Bean.ArchiveName = _T("TierList.prp");
}

CChoiceTier::~CChoiceTier() 
{
	DestroyList ();
}

void CChoiceTier::DestroyList() 
{
	for (std::vector<CTierList *>::iterator pabl = TierList.begin (); pabl != TierList.end (); ++pabl)
	{
		CTierList *abl = *pabl;
		delete abl;
	}
    TierList.clear ();
}

void CChoiceTier::FillList () 
{
    short  tier;
    TCHAR  tier_bz [21];

	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Tier"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Tier"),       1,  40, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),   2, 400);

	if (TierList.size () == 0)
	{
		DbClass->sqlout ((short *) &tier,      SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR) tier_bz,    SQLCHAR, sizeof (tier_bz));
		int cursor = DbClass->sqlcursor (_T("select tier, tier_bz ")
			                             _T("from tier where tier > 0"));

		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) tier_bz;
			CDbUniCode::DbToUniCode (tier_bz, pos);
			CTierList *abl = new CTierList (tier, tier_bz);
			TierList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
		Load ();
	}

	for (std::vector<CTierList *>::iterator pabl = TierList.begin (); pabl != TierList.end (); ++pabl)
	{
		CTierList *abl = *pabl;
		CString Tier;
		CString TierBz;
		Tier.Format (_T("%hd"), abl->tier); 
		TierBz = abl->tier_bz;
        int ret = InsertItem (i, -1);
        ret = SetItemText (Tier.GetBuffer (), i, 1);
        ret = SetItemText (TierBz.GetBuffer (), i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceTier::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceTier::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceTier::SearchTier (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceTier::SearchTxt (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceTier::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchTier (ListBox, EditText.GetBuffer ());
             break;
        case 2 :
             SearchTxt (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceTier::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceTier::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   short li1 = (short) _tstoi (strItem1.GetBuffer ());
	   short li2 = (short) _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   return 0;
}


void CChoiceTier::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CTierList *abl = TierList [i];
		   
		   abl->tier     = (short) _tstoi (ListBox->GetItemText (i, 1));
		   abl->tier_bz  = ListBox->GetItemText (i, 2);
	}
	for (int i = 1; i <= 4; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);
}

void CChoiceTier::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = TierList [idx];
}

CTierList *CChoiceTier::GetSelectedText ()
{
	CTierList *abl = (CTierList *) SelectedRow;
	return abl;
}


void CChoiceTier::OnEnter ()
{
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cTier = m_List.GetItemText (idx, 1);  
		short tier = (short) _tstoi (cTier.GetBuffer ());
//		Message.Format (_T("Tier=%hd"), tier);
		Message.Format (_T("%hd"), tier);
//		ToClipboard (Message);
	}
	p.SetCommand (_T("rswrun 13500"));
	HANDLE Pid = p.Start (SW_SHOWNORMAL);
}
