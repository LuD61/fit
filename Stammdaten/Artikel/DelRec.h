#pragma once
#include <vector>
#include "DbClass.h"

class CDelTable : public DB_CLASS
{
public:
	enum 
	{
		No = 0,
		Break = 1,
	};

	CListBox *Output;
	int TestCursor;
	int DeleteCursor;
	CString Name;
	CString Where;
	int Kritical;

	CDelTable ();
	CDelTable (CString& TableInfo, CString& Where);
	CDelTable (CString& Name, CString& Where, int Kritical);
	~CDelTable ();
	BOOL Test ();
	BOOL Delete ();

};

class CDelSection
{
public:
	CListBox *Output;
	std::vector<CString> Fields;
	std::vector<CString> Tables;
	CString Command;
	CDelSection ();
	~CDelSection ();
    void Init ();
	void SetFields (CString& FieldString);
	void AddTable (CString& Table);
	BOOL Delete (CString& Where);
};


class CDelRec
{
public:
	CListBox *Output;
	std::vector<CDelSection *> Sections;
    CString FileName;
	CDelRec(void);
	CDelRec(CString& DelFileName);
	~CDelRec(void);
	BOOL Load ();
	BOOL Run (CString &Where);
};
