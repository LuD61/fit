#include "stdafx.h"
#include "a_pfa.h"

struct A_PFA a_pfa, a_pfa_null, a_pfa_def;

void A_PFA_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_pfa.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_pfa.a,SQLDOUBLE,0);
    sqlout ((long *) &a_pfa.a_krz,SQLLONG,0);
    sqlout ((short *) &a_pfa.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_pfa.fil,SQLSHORT,0);
    sqlout ((short *) &a_pfa.mdn,SQLSHORT,0);
    sqlout ((short *) &a_pfa.sg1,SQLSHORT,0);
    sqlout ((short *) &a_pfa.sg2,SQLSHORT,0);
    sqlout ((TCHAR *) a_pfa.verk_art,SQLCHAR,2);
    sqlout ((TCHAR *) a_pfa.mwst_ueb,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &a_pfa.verk_beg,SQLDATE,0);
            cursor = sqlcursor (_T("select a_pfa.a,  ")
_T("a_pfa.a_krz,  a_pfa.delstatus,  a_pfa.fil,  a_pfa.mdn,  a_pfa.sg1,  ")
_T("a_pfa.sg2,  a_pfa.verk_art,  a_pfa.mwst_ueb,  a_pfa.verk_beg from a_pfa ")

#line 12 "a_pfa.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_pfa.a,SQLDOUBLE,0);
    sqlin ((long *) &a_pfa.a_krz,SQLLONG,0);
    sqlin ((short *) &a_pfa.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_pfa.fil,SQLSHORT,0);
    sqlin ((short *) &a_pfa.mdn,SQLSHORT,0);
    sqlin ((short *) &a_pfa.sg1,SQLSHORT,0);
    sqlin ((short *) &a_pfa.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) a_pfa.verk_art,SQLCHAR,2);
    sqlin ((TCHAR *) a_pfa.mwst_ueb,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_pfa.verk_beg,SQLDATE,0);
            sqltext = _T("update a_pfa set a_pfa.a = ?,  ")
_T("a_pfa.a_krz = ?,  a_pfa.delstatus = ?,  a_pfa.fil = ?,  a_pfa.mdn = ?,  ")
_T("a_pfa.sg1 = ?,  a_pfa.sg2 = ?,  a_pfa.verk_art = ?,  ")
_T("a_pfa.mwst_ueb = ?,  a_pfa.verk_beg = ? ")

#line 14 "a_pfa.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_pfa.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_pfa.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_pfa ")
                                  _T("where a = ? for update"));
            sqlin ((double *)   &a_pfa.a,  SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_pfa ")
                                  _T("set delstatus = 0 where a = ? and delstatus = 0 for update"));
            sqlin ((double *)   &a_pfa.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_pfa ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_pfa.a,SQLDOUBLE,0);
    sqlin ((long *) &a_pfa.a_krz,SQLLONG,0);
    sqlin ((short *) &a_pfa.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_pfa.fil,SQLSHORT,0);
    sqlin ((short *) &a_pfa.mdn,SQLSHORT,0);
    sqlin ((short *) &a_pfa.sg1,SQLSHORT,0);
    sqlin ((short *) &a_pfa.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) a_pfa.verk_art,SQLCHAR,2);
    sqlin ((TCHAR *) a_pfa.mwst_ueb,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_pfa.verk_beg,SQLDATE,0);
            ins_cursor = sqlcursor (_T("insert into a_pfa (a,  ")
_T("a_krz,  delstatus,  fil,  mdn,  sg1,  sg2,  verk_art,  mwst_ueb,  verk_beg) ")

#line 28 "a_pfa.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?)"));

#line 30 "a_pfa.rpp"
}
