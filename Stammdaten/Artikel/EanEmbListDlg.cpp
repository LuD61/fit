// EanEmbListDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "EanEmbListDlg.h"


// CEanEmbListDlg-Dialogfeld

IMPLEMENT_DYNAMIC(CEanEmbListDlg, CDialog)
CEanEmbListDlg::CEanEmbListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEanEmbListDlg::IDD, pParent)
{
	Basis = NULL;
	PageUpdate = NULL;
}

CEanEmbListDlg::~CEanEmbListDlg()
{
}

void CEanEmbListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CEanEmbListDlg, CDialog)
	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CEanEmbListDlg-Meldungshandler

BOOL CEanEmbListDlg::OnInitDialog()
{
	m_EanListDlg.PageUpdate = PageUpdate;
	m_EmbListDlg.PageUpdate = PageUpdate;

	CDialog::OnInitDialog();
	m_EanListDlg.Create (IDD_EAN_LIST, this);
	m_EanListDlg.ShowWindow (SW_SHOWNORMAL);
	m_EanListDlg.UpdateWindow ();
	m_EanListDlg.Basis = Basis;

	m_EmbListDlg.Create (IDD_EMB_LIST, this);
	m_EmbListDlg.ShowWindow (SW_SHOWNORMAL);
	m_EmbListDlg.UpdateWindow ();

	SplitPane.Create (this, 0, 0, &m_EanListDlg, &m_EmbListDlg, 40, CSplitPane::Vertical);

	return FALSE;
}

void CEanEmbListDlg::OnSize (UINT nType, int cx, int cy)
{
	if (IsWindow (SplitPane.m_hWnd))
	{
		CRect rect;
		m_EanListDlg.GetWindowRect (&rect);
		ScreenToClient (&rect);
		rect.right = cx;
        m_EanListDlg.MoveWindow (&rect);

		m_EmbListDlg.GetWindowRect (&rect);
		ScreenToClient (&rect);
		rect.right = cx;
		rect.bottom = cy - 2;
        m_EmbListDlg.MoveWindow (&rect);

		SplitPane.SetLength (cx);
	}
}

BOOL CEanEmbListDlg::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			if (pMsg->wParam == VK_F5)
			{
				PageUpdate->Back ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F12)
			{
				((CDbPropertyPage *) GetParent ())->Update->Write ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return FALSE;
}

void CEanEmbListDlg::SetBasis (CBasisdatenPage *Basis)
{
	this->Basis = Basis;
	m_EanListDlg.SetBasis (Basis);
	m_EmbListDlg.SetBasis (Basis);
}

void CEanEmbListDlg::Update ()
{
	m_EanListDlg.Update ();
	m_EmbListDlg.Update ();
}

void CEanEmbListDlg::Read ()
{
	m_EanListDlg.Read ();
	m_EmbListDlg.Read ();
}

void CEanEmbListDlg::AddUpdateEvent (CUpdateEvent *UpdateEvent)
{
	m_EanListDlg.AddUpdateEvent (UpdateEvent);
	m_EmbListDlg.AddUpdateEvent (UpdateEvent);
}


