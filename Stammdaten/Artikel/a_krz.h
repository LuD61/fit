#ifndef _A_KRZ_DEF
#define _A_KRZ_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_KRZ {
   double         a;
   double         a_herk;
   long           a_krz;
   long           anz_a;
   short          delstatus;
   TCHAR          herk_kz[2];
   short          hwg;
   TCHAR          modif[2];
   TCHAR          verk_art[2];
   short          wg;
};
extern struct A_KRZ a_krz, a_krz_null;

#line 8 "a_krz.rh"

class A_KRZ_CLASS : public DB_CLASS 
{
       private :
               int a_cursor;
	       int a_herk_cursor;	  
               void prepare (void);
       public :
               A_KRZ a_krz;  
               A_KRZ_CLASS () : DB_CLASS ()
               {
		  a_cursor = -1;
		  a_herk_cursor = -1;
               }
	       int dbreadafirst ();
	       int dbreada ();
	       int dbreada_herkfirst (); 		
	       int dbreada_herk (); 		
};
#endif
