// ArtikelView.h : Schnittstelle der Klasse CArtikelView
//


#pragma once


class CArtikelView : public CView
{
protected: // Nur aus Serialisierung erstellen
	CArtikelView();
	DECLARE_DYNCREATE(CArtikelView)

// Attribute
public:
	CArtikelDoc* GetDocument() const;

// Operationen
public:

// �berschreibungen
	public:
	virtual void OnDraw(CDC* pDC);  // �berladen, um diese Ansicht darzustellen
virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementierung
public:
	virtual ~CArtikelView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
public:
};

#ifndef _DEBUG  // Debugversion in ArtikelView.cpp
inline CArtikelDoc* CArtikelView::GetDocument() const
   { return reinterpret_cast<CArtikelDoc*>(m_pDocument); }
#endif

