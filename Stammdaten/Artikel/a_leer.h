#ifndef _A_LEER_DEF
#define _A_LEER_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_LEER {
   double         a;
   long           a_krz;
   double         a_pfa;
   short          delstatus;
   short          fil;
   short          mdn;
   short          sg1;
   short          sg2;
   TCHAR          verk_art[2];
   TCHAR          mwst_ueb[2];
   DATE_STRUCT    verk_beg;
   double         br_gew;
};
extern struct A_LEER a_leer, a_leer_null;

#line 8 "a_leer.rh"

class A_LEER_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_LEER a_leer;  
               A_LEER_CLASS () : DB_CLASS ()
               {
               }
};
#endif
