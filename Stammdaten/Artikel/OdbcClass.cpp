#include "stdafx.h"
#include "OdbcClass.h"
#include "Token.h"
#include "DbTime.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef UNICODE
#define _TSQLCHAR SQLWCHAR
#else
#define _TSQLCHAR UCHAR
#endif

#define MAXDEBUG 20

extern short sql_mode;

namespace ODBC
{

DataBase *StdDbase = NULL; 
short DB_CLASS::ShortNull = (short) 0x8000;
long DB_CLASS::LongNull = (long) 0x80000000;
double DB_CLASS::DoubleNull = (double) 0xffffffffffffffff;
int DB_CLASS::SQLDEBUG = 0;
FILE *DB_CLASS::sqldebug = NULL;
char DB_CLASS::DebugFile[512] = {"C:\\temp\\sqldebug"};

BOOL DB_CLASS::IsShortnull (short value)
{
	return (value == ShortNull);
}
BOOL DB_CLASS::IsLongnull (long value)
{
	return (value == LongNull);
}

BOOL DB_CLASS::IsDoublenull (double value)
{
	return (value == DoubleNull);
}

void DB_CLASS::SetCSqlDebug (BOOL mode)
{
	     SQLDEBUG = mode;
		 if (SQLDEBUG == 0)
		 {
               sqldebug = NULL;
			   return;
		 }
		 CreateDebugFile ();
         sqldebug = fopen (DebugFile, "w");
		 fclose (sqldebug);
}

void DB_CLASS::CreateDebugFile ()
{
	     FILE *fp;
		 char NrName [512];
		 char buffer [80];
		 char *etc;
		 int fnr = 1;

		 etc = getenv ("BWSETC");
		 if (etc == NULL)
		 {
			 return;
		 }

		 sprintf (NrName, "%s\\sqlnr.txt", etc);
		 fp = fopen (NrName, "r");
		 if (fp != NULL)
		 {
			 fgets (buffer, 79, fp);
			 fnr = atoi (buffer);
			 fnr = (fnr == 0) ? 1 : fnr;
			 fclose (fp);
		 }
 	     sprintf (DebugFile, "c:\\temp\\sqldebug%d", fnr);
		 fnr ++;
		 if (fnr > MAXDEBUG) fnr = 1;
		 fp = fopen (NrName, "w");
		 fprintf (fp, "%d", fnr);
		 fclose (fp);
}


DB_CLASS::DB_CLASS ()
{
            hstmDirect = NULL;
            memset ((LPTSTR) CursTab,  0, MAXCURS * sizeof (int));
            memset ((LPTSTR) HstmtTab, 0, MAXCURS * sizeof (HSTMT));
            OutAnz = 0;
            InAnz  = 0;
            cbLen = SQL_NTS;
            sql_mode = 0;
            SqlErrorProc = ErrProc;
            InWork = FALSE;

            cursor          = -1;
            test_upd_cursor = -1;
            test_lock_cursor = -1;
            upd_cursor      = -1;
            ins_cursor      = -1;
            del_cursor      = -1;
            scrollpos       = 1;
			if (StdDbase != NULL)
			{
				SetDatabase (*StdDbase);
			}
			User = "";
			Passw = "";
			DbName = "bws";
			LoadDbIni ();
}

DB_CLASS::~DB_CLASS ()
{

            if (hdbc == NULL)
			{
				return;
			}

/*
			try
			{
				for (int i = 0; i < MAXCURS; i ++)
				{
                 if (CursTab[i] != 0)
                 {
//                       sqlclose (CursTab[i]);
                       sqlclose (i);
                 }
				}
			}
			catch (...) {}
*/
}

void DB_CLASS::LoadDbIni ()
{
	char *etc = getenv ("BWSETC");
	if (etc != NULL)
	{
		DbIni.FileName.Format ("%s\\Database.ini", etc);
	}
	else
	{
		DbIni.FileName = "Database.ini";;
	}
	DbIni.SectionName = "Database";
	DbIni.Load ();
	CString tName = DbIni.GetValue (CString ("NAME"));
	if (tName != "NAME")
	{
		DbName = tName;
	}
	CString tUser = DbIni.GetValue (CString ("USER"));
	if (tUser != "USER")
	{
		User = tUser;
	}
	CString tPassw = DbIni.GetValue (CString ("PASSW"));
	if (tPassw != "PASSW")
	{
		Passw = tPassw;
	}
}

BOOL DB_CLASS::opendbase (LPTSTR dbase)
{
     name = dbase;

     DBase.Name = name;
     int retcode = SQLAllocEnv(&henv);
     if (retcode == SQL_SUCCESS); 
	 else
	 {
             GetError (NULL);
             return FALSE;
	 }
     DBase.henv = henv;
     retcode = SQLAllocConnect(henv, &hdbc); /* Connection handle */

     if (retcode == SQL_SUCCESS); 
	 else
	 {
             GetError (NULL);
             return FALSE;
	 }
        /* Connect to data source */

     retcode = SQLConnect(hdbc, (_TSQLCHAR *) DbName.GetBuffer (), SQL_NTS, 
		                        (_TSQLCHAR *) User.GetBuffer (),   SQL_NTS, 
								(_TSQLCHAR *) Passw.GetBuffer (),  SQL_NTS);
     DBase.hdbc = hdbc;
     if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO);
     else
     {
             GetError (NULL);
             return FALSE;
     }
	 if (StdDbase == NULL)
	 {
		 StdDbase = &DBase;
	 }
     sqlstatus = 0;
     sqlcomm (_T("set isolation to dirty read"));  //FS-337
     return TRUE;
}


BOOL DB_CLASS::closedbase (LPTSTR dbase)
/**
Datenbank schliessen.
**/

{
/*
     SQLDisconnect(hdbc);
     SQLFreeConnect(hdbc);
     SQLFreeEnv(henv);
     hdbc = NULL;
     henv = NULL;
*/
     return TRUE;
}


int DB_CLASS::sqlconnect (LPTSTR server, LPTSTR user, LPTSTR passw)
{

     int retcode = SQLAllocEnv(&henv);
     if (retcode == SQL_SUCCESS); 
	 else
	 {
		     printf ("Fehler bei SQLAllocEnv\n");
		     exit (1);
	 }
     retcode = SQLAllocConnect(henv, &hdbc); /* Connection handle */

     if (retcode == SQL_SUCCESS); 
	 else
	 {
		     printf ("Fehler bei SQLAllocConnect\n");
		     exit (1);
	 }
     return (0);
}


int DB_CLASS::sqlconnectdbase (LPTSTR server, LPTSTR user, LPTSTR passw, LPTSTR dbase)
{
     opendbase (dbase);
     return sqlstatus;
}

int DB_CLASS::beginwork (void)
{
    if (InWork)
    {
        commitwork ();
    }

    sqlcomm (_T("begin work")); //FS-337
    InWork = TRUE;
    return 0;
}

int DB_CLASS::commitwork (void)
{
    if (InWork)
    {
        sqlcomm (_T("commit work"));//FS-337
//        SQLTransact (henv, hdbc, SQL_COMMIT);
        InWork = FALSE;
    }
    return 0;
}

int DB_CLASS::rollbackwork (void) 
{
    if (InWork)
    {
        sqlcomm (_T("rollback work")); //FS-337
//        SQLTransact (henv, hdbc, SQL_ROLLBACK);
        InWork = FALSE;
    }
    return 0;
}

#ifdef UNICODE
int DB_CLASS::sqlcomm (_TSQLCHAR *statement)
#else
int DB_CLASS::sqlcomm (LPSTR statement)
#endif
{
	 int retcode = SQLAllocStmt (hdbc, &hstmDirect);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
		     return -1;
	 }

     retcode = TestOut (hstmDirect);
     retcode = TestIn (hstmDirect);

     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
		     return -1;
	 }
     retcode = SQLExecDirect((HSTMT) hstmDirect, 
		                  (_TSQLCHAR *) statement,
						  (SDWORD) _tcslen ((TCHAR *) statement));
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
     {
             GetError (hstmDirect);
             return -1;
     }

     CString St = statement;
     St.MakeUpper ();
     if (St.Find (_T("SELECT")) > -1)
     {
             retcode = SQLFetch(hstmDirect); 
             if (retcode == SQL_SUCCESS) 
             {
	                 sqlstatus = 0; 
             }
             else if (retcode == SQL_NO_DATA)
             {
                     sqlstatus = 100;
             }
             else
             { 
                    GetError (hstmDirect);
                    return -1;  
             }
     }
     return sqlstatus;
}

int DB_CLASS::sqlcursor (LPTSTR statement)
{
    int cursor;

    for (cursor  = 0; cursor < MAXCURS; cursor ++)
    {
        if (CursTab[cursor] == 0)
        {
            CursTab[cursor] = 1;
            break;
        }
    }

	 int retcode = SQLAllocStmt (hdbc, &HstmtTab[cursor]);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (NULL);
             CursTab[cursor] = 0;
		     return -1;
	 }

     HSTMT Cursor = HstmtTab[cursor];
     retcode = SQLPrepare(Cursor, 
		                  (_TSQLCHAR *)statement,
						  (SDWORD) _tcslen ((TCHAR *) statement));
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (Cursor);
             CursTab[cursor] = 0;
		     return -1;
	 }

     retcode = TestIn (Cursor);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (Cursor);
             CursTab[cursor] = 0;
		     return -1;
	 }

     retcode = TestOut (Cursor);

     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (Cursor);
             CursTab[cursor] = 0;
		     return -1;
	 }
     return cursor;
}



int DB_CLASS::sqlopen (int cursor)
{
     if (cursor < 0)
     {
         return -1;
     }

     if (CursTab[cursor] == 0)
     {
         return -1;
     }

     HSTMT Cursor = HstmtTab[cursor];


  	 int retcode = SQLFreeStmt (Cursor, SQL_CLOSE);
     if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
     {
                 sqlstatus = 0;
     } 
     else
     {
                 GetError (Cursor);
 		         sqlstatus = 0 - retcode;
                 return -1;
     }


     retcode = SQLExecute(Cursor); 
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
     {
                 GetError (Cursor);
  		         sqlstatus = 0 - retcode;
                 return -1;
     } 

     CursTab[cursor] = 2;
     return sqlstatus;
}


int DB_CLASS::sqlfetch (int cursor)
{
 
        int dsqlstatus;
        int retcode;

        if (cursor < 0)
        {
             return -1;
        }

        if (CursTab[cursor] == 0)
        { 
             return -1;
        }

        if (CursTab [cursor] == 1)
        {
                    sqlopen (cursor);
        }
        HSTMT Cursor = HstmtTab[cursor];

        if ((retcode = SQLFetch(Cursor)) == SQL_SUCCESS) 
        {
	           sqlstatus = 0; 
        }
        else if (retcode == SQL_NO_DATA)
        {
                 sqlstatus = 100;
        }
        else
        {
                 GetError (Cursor);
                 return -1;  
        }
        dsqlstatus = sqlstatus;

        return sqlstatus;
}


int DB_CLASS::sqlexecute (int cursor)
/**
Cursor oeffnen.
**/
{
        if (cursor < 0)
        {
             return -1;
        }

        if (CursTab[cursor] == 0)
        { 
             return -1;
        }

        HSTMT Cursor = HstmtTab[cursor];

  	    int retcode = SQLFreeStmt (Cursor, SQL_CLOSE);
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
		{
                 sqlstatus = 0;
		} 
        else
		{
                 GetError (Cursor);
 		         sqlstatus = 0 - retcode;
                 return -1;
		}
        retcode = SQLExecute(Cursor); 
        if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
        {
                 sqlstatus = 0;
        } 
	    else
        {
                 GetError (Cursor);
                 return -1;
        }
        return sqlstatus;
}


int DB_CLASS::sqlclose (int cursor)
/**
Speicherbereiche fuer sqlerte freigeben.
**/
{
     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
         return -1;
     }

     HSTMT Cursor = HstmtTab[cursor];
     SQLFreeStmt(Cursor, SQL_DROP);
	 CursTab[cursor] = 0;
     return (0);
}


void DB_CLASS::sqlout (void *var, int typ, int len)
{
     if (OutAnz == MAXVARS)
     {
         return;
     }

     OutVars[OutAnz].var = var;
     OutVars[OutAnz].len = len;
     OutVars[OutAnz].typ = typ;
     OutAnz ++;
}


void DB_CLASS::sqlin (void *var, int typ, int len)
{
     if (InAnz == MAXVARS)
     {
         return;
     }

     InVars[InAnz].var = var;
     InVars[InAnz].len = len;
     InVars[InAnz].typ = typ;
     InAnz ++;
}

static SDWORD pcbValue;

int DB_CLASS::TestOut (HSTMT Cursor)
{
     int retcode = 0;

     for (int i = 0; i < OutAnz; i ++)
     {
         switch (OutVars[i].typ)
         {
                 case SQLCHAR:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_CHAR,  
                                         (LPTSTR ) OutVars[i].var, 
                                                   OutVars[i].len, 
                                                   &pcbValue); 
                        break;
                 case SQLSHORT:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_SHORT,  
                                         (short *) OutVars[i].var, 
                                                   sizeof (short), 
                                                   &pcbValue); 
                        break;
                 case SQLLONG:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_LONG,  
                                         (long *) OutVars[i].var, 
                                                   sizeof (long), 
                                                   &pcbValue); 
                        break;
                 case SQLDOUBLE:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_DOUBLE,  
                                         (double *)  OutVars[i].var, 
                                                     sizeof (double), 
                                                     &pcbValue); 
                        break;
                 case SQLDATE:
                        retcode = SQLBindCol(Cursor, i + 1, 
                                         SQL_C_DATE,  
                                         (long *)  OutVars[i].var, 
                                                   sizeof (long), 
                                                   &pcbValue); 
                        break;
         }
     }
     OutAnz = 0;
     return retcode;
}

int DB_CLASS::TestIn (HSTMT Cursor)
{
     int retcode = 0;

     for (int i = 0; i < InAnz; i ++)
     {
         switch (InVars[i].typ)
         {
                 case SQLCHAR:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_CHAR, SQL_CHAR, InVars[i].len, 0,  
                                         (LPTSTR) InVars[i].var, 
                                                  0, 
                                                  &cbLen); 
                        break;
                 case SQLSHORT:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_SSHORT, SQL_SMALLINT, 0, 0,  
                                         (short *) InVars[i].var, 
                                                   sizeof (short), 
                                                   &cbLen); 
                        break;
                 case SQLLONG:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_SLONG, SQL_INTEGER, 0, 0,  
                                         (long *) InVars[i].var, 
                                                   0, 
                                                   &cbLen); 
                        break;
                 case SQLDOUBLE:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_DOUBLE, SQL_DOUBLE, 0, 0,   
                                         (double *)  InVars[i].var, 
                                                     0, 
                                                     &cbLen); 
                        break;
                 case SQLDATE:
                        retcode = SQLBindParameter(Cursor, i + 1, SQL_PARAM_INPUT,
                                         SQL_C_DATE, SQL_DATE, 0, 0,   
                                         (long *)  InVars[i].var, 
                                                     0, 
                                                     &cbLen); 
                        break;
         }
     }
     InAnz = 0;
     return retcode;
}


void DB_CLASS::GetError	(HSTMT Cursor)
{ 
     _TSQLCHAR szSqlState[512];
     SDWORD pfNativeError;
     _TSQLCHAR szErrorMsg [512]; 

     int retcode = SQLError(henv, 
                             hdbc, 
                             Cursor, 
                             szSqlState, &pfNativeError,
                             szErrorMsg, 512, NULL);
     CString ErrText;
     ErrText.Format (_T("Fehler %ld\n%s"), pfNativeError, szErrorMsg);
 
     if (sql_mode == 0)
     {
            MessageBox (NULL, ErrText, _T(""), MB_ICONERROR);
            ErrText = szErrorMsg;
            (*SqlErrorProc) (pfNativeError, ErrText);
     }
}

BOOL DB_CLASS::ErrProc	(SDWORD ErrStatus, CString& ErrText)
{
    ExitProcess (ErrStatus);
    return TRUE;
}

int DB_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
             prepare ();
         }
         sqlopen (cursor);
         sqlfetch (cursor);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int DB_CLASS::dbread (void)
/**
Naechsten Satz aus Tabelle lesen.
**/
{
         sqlfetch (cursor);
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int DB_CLASS::dbupdate (void)
/**
Tabelle eti Updaten.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         sqlopen (test_upd_cursor);
         sqlfetch (test_upd_cursor);
         if (sqlstatus == 100)
         {
                   sqlexecute (ins_cursor);
         }  
         else if (sqlstatus == 0)
         {
                   sqlexecute (upd_cursor);
         }  
          
         return sqlstatus;
} 

int DB_CLASS::dblock (void)
/**
Tabelle eti Updaten.
**/
{

	     sql_mode = 1;
         if (test_lock_cursor == -1)
         {
             prepare ();
         }
         sqlopen (test_lock_cursor);
         sqlfetch (test_lock_cursor);
		 sql_mode = 0;
          
         return sqlstatus;
} 

int DB_CLASS::dbdelete (void)
/**
Tabelle eti lesen.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         sqlopen (test_upd_cursor);
         sqlfetch (test_upd_cursor);
         if (sqlstatus == 0)
         {
                      sqlexecute (del_cursor);
         }
         return sqlstatus;
}

void DB_CLASS::dbclose (void)
/**
Cursor fuer eti schliessen.
**/
{
         if (cursor == -1) return;

         sqlclose (cursor); 
         sqlclose (upd_cursor); 
         sqlclose (ins_cursor); 
         sqlclose (del_cursor); 
         sqlclose (test_upd_cursor);
         sqlclose (test_lock_cursor);

         cursor = -1;
         upd_cursor = -1;
         ins_cursor = -1;
         del_cursor = -1;
         test_upd_cursor = -1;
         test_lock_cursor = -1;
         cursor_ausw = -1;
}

int DB_CLASS::dbmove (int mode)
/**
Scroll-Cursor lesen.
**/
{
         int scrollakt;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }

         scrollakt = scrollpos;
         switch (mode)
         {
             case FIRST :
                         scrollpos = 1;
                         break;
             case NEXT :     
                         scrollpos ++;
                         break;
             case PRIOR :
                         if (scrollpos > 1)
                         {
                                 scrollpos --;
                         }
                         break;
//             case LAST :      
             case CURRENT :
                         break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode);
         if (sqlstatus != 0)
         {
             scrollpos = scrollakt;
         }
         return (sqlstatus);
}

int DB_CLASS::dbmove (int mode, int pos)
/**
Scroll-Cursor lesen.
**/
{
         int scrollakt;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }
         scrollakt = scrollpos;
         switch (mode)
         {
             case DBABSOLUTE :      
             case RELATIVE :      
                    break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode, pos);
         if (sqlstatus != 0)
         {
             scrollpos = scrollakt;
         }
         return (sqlstatus);
}

int DB_CLASS::dbcanmove (int mode)
/**
Scroll-Cursor testen.
**/
{
         int status; 

         if (cursor_ausw == -1)
         {
                    return (-1);
         }

         switch (mode)
         {
             case FIRST :
                         scrollpos = 1;
                         break;
             case NEXT :     
                         scrollpos ++;
                         break;
             case PRIOR :
                         if (scrollpos > 1)
                         {
                                 scrollpos --;
                         }
                         break;
//             case LAST :      
             case CURRENT :
                         break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode);
         if (sqlstatus != 0)
         {
             status = FALSE;
         }
         else
         {
             status = TRUE;
         }
         dbmove (DBABSOLUTE, scrollpos);
         return (status);
}

int DB_CLASS::dbcanmove (int mode, int pos)
/**
Scroll-Cursor lesen.
**/
{
         int status;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }
         switch (mode)
         {
             case DBABSOLUTE :      
             case RELATIVE :      
                    break;
             default :
                   return (-1);
         }
//         fetch_scroll (cursor_ausw, mode, pos);
         if (sqlstatus != 0)
         {
             status = FALSE;
         }
         else
         {
             status = TRUE;
         }
         dbmove (DBABSOLUTE, scrollpos);
         return (status);
}


int DB_CLASS::PrepareTables (char *Table)
{
	int retcode;
    int cursor;
	HSTMT Cursor;

	if (hdbc == NULL)
	{
		return -1;
	}
    sqlstatus = 0;
    for (cursor  = 0; cursor < MAXCURS; cursor ++)
    {
        if (CursTab[cursor] == 0)
        {
            CursTab[cursor] = 3;
            break;
        }
    }

	 retcode = SQLAllocStmt (hdbc, &HstmtTab[cursor]);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (NULL);
             CursTab[cursor] = 0;
                     sqlstatus = -1;
		     return -1;
	 }

     Cursor = HstmtTab[cursor];
	 retcode = SQLTables(Cursor, NULL, 0, NULL, 0, (unsigned char *) Table, SQL_NTS,
		                         NULL, 0);
	 return cursor;
}

int DB_CLASS::PrepareColumns (char *Table, char *Column)
{
	 int retcode;
     int cursor;
	 HSTMT Cursor;

	 if (hdbc == NULL)
	 {
		return -1;
	 }
     sqlstatus = 0;
     for (cursor  = 0; cursor < MAXCURS; cursor ++)
     {
        if (CursTab[cursor] == 0)
        {
            CursTab[cursor] = 3;
            break;
        }
	 }

	 retcode = SQLAllocStmt (hdbc, &HstmtTab[cursor]);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
             GetError (NULL);
             CursTab[cursor] = 0;
                     sqlstatus = -1;
		     return -1;
	 }

     Cursor = HstmtTab[cursor];
	 retcode = SQLColumns(Cursor, NULL, 0, NULL, 0, (UCHAR FAR *) Table, SQL_NTS,
		                          (UCHAR FAR *) Column, SQL_NTS);
     if (retcode != SQL_SUCCESS && retcode != SQL_SUCCESS_WITH_INFO)
	 {
			SQLBindCol(Cursor, 1, SQL_C_CHAR, szQualifier, STR_LEN,&cbQualifier); 
			SQLBindCol(Cursor, 2, SQL_C_CHAR, szOwner, STR_LEN, &cbOwner); 
			SQLBindCol(Cursor, 3, SQL_C_CHAR, szTableName, STR_LEN,&cbTableName); 
			SQLBindCol(Cursor, 4, SQL_C_CHAR, szColName, STR_LEN, &cbColName); 
			SQLBindCol(Cursor, 5, SQL_C_SSHORT, &DataType, 0, &cbDataType); 
			SQLBindCol(Cursor, 6, SQL_C_CHAR, szTypeName, STR_LEN, &cbTypeName); 
			SQLBindCol(Cursor, 7, SQL_C_SLONG, &Precision, 0, &cbPrecision); 
			SQLBindCol(Cursor, 8, SQL_C_SLONG, &Length, 0, &cbLength); 
			SQLBindCol(Cursor, 9, SQL_C_SSHORT, &Scale, 0, &cbScale); 
			SQLBindCol(Cursor, 10, SQL_C_SSHORT, &Radix, 0, &cbRadix); 
			SQLBindCol(Cursor, 11, SQL_C_SSHORT, &Nullable, 0, &cbNullable); 
			SQLBindCol(Cursor, 12, SQL_C_CHAR, szRemarks, REM_LEN, &cbRemarks); 
	 }
	 return cursor;
}

int DB_CLASS::GetColLength (int colpos, int cursor)
{
	 HSTMT Cursor;
     SDWORD collength;
     SDWORD precision;
     SDWORD scale;
     SWORD FAR cbDesc;
     char rgbDesc [128];
	 int ret;
     int type;

     sqlstatus = 0;


     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
        sqlstatus = -1;
         return -1;
     }

     Cursor = HstmtTab[cursor];
     type =  GetColType (colpos, cursor);
     if (type == SQLDECIMAL)
     {
         ret = SQLColAttributes(Cursor, colpos, 
	  			                SQL_COLUMN_PRECISION, rgbDesc, 128, &cbDesc, &precision); 
         if (ret < 0)
         {
	                   GetError (Cursor);
					   return (0);
         }
         ret = SQLColAttributes(Cursor, colpos, 
	  			                SQL_COLUMN_SCALE, rgbDesc, 128, &cbDesc, &scale); 
         if (ret < 0)
         {
	                   GetError (Cursor);
					   return (0);
         }
         collength = (precision << 8) | scale;
         return ((int) collength);
     }

     ret = SQLColAttributes(Cursor, colpos, 
				            SQL_COLUMN_LENGTH, rgbDesc, 128, &cbDesc, &collength); 
     if (ret < 0)
	 {
	                   GetError (Cursor);
					   return (0);
	 }
     if (type == SQLCHAR)
     {
//         collength ++;
     }
	 return ((int) collength);
}


LPSTR DB_CLASS::GetColName (int colpos, int cursor, LPSTR name)
{
	 HSTMT Cursor;
     SDWORD collength;
     SWORD FAR cbDesc;
     char rgbDesc [128];
	 int ret;

     sqlstatus = 0;
     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
        sqlstatus = -1;
         return NULL;
     }

     Cursor = HstmtTab[cursor];
     ret = SQLColAttributes(Cursor, colpos, 
				            SQL_COLUMN_NAME, rgbDesc, 128, &cbDesc, &collength); 
     if (ret < 0)
	 {
	                   GetError (Cursor);
					   return (NULL);
	 }
     strcpy (name, rgbDesc);
	 return (name);
}

int DB_CLASS::GetColType (int colpos, int cursor)
{
	 HSTMT Cursor;
     SDWORD coltype;
     SWORD FAR cbDesc;
     char rgbDesc [128];
	 int ret;

     sqlstatus = 0;
     if (CursTab[cursor] < 0 ||
         CursTab[cursor] > 2)
     {
        sqlstatus = -1;
         return -1;
     }

     Cursor = HstmtTab[cursor];
     ret = SQLColAttributes(Cursor, colpos, 
				            SQL_COLUMN_TYPE, rgbDesc, 128, &cbDesc, &coltype); 
     if (ret < 0)
	 {
	                   GetError (Cursor);
					   return (-1);
	 }
	 return ((int) coltype);
}



void DB_CLASS::ToDbDate (CString& Date, DATE_STRUCT *DbDate)
{
	TCHAR date[12];

    _tcscpy (date, Date.GetBuffer (11));
	FromGerDate (DbDate, date);
}

void DB_CLASS::FromDbDate (CString& Date, DATE_STRUCT *DbDate)
{
	TCHAR date[12];

	if (DbDate->year <= 1900 &&
        DbDate->month <= 1 &&
        DbDate->day <= 1)
	{
		Date = "";
	}
	else
	{
		ToGerDate (DbDate, date);
		Date = date;
	}
}

int DB_CLASS::FromRecDate (DATE_STRUCT *sqldate, LPTSTR cdate)
{
//	   int anz;
       char day [3];
       char month [3];
       char year [5];
	   int len;

	   sqldate->day = 1; 
	   sqldate->month = 1; 
	   sqldate->year = 1900; 

	   len = (int) _tcslen (cdate);
	   if (len < 6)
	   {
                      return (0);
	   }
       strcpy (day,   "01"); 
       strcpy (month, "01"); 
       strcpy (year,  "1900"); 
	   memcpy (day, &cdate[0], 2);
	   memcpy (month, &cdate[2], 2);
	   if (len == 8)
	   {
         	   memcpy (year, &cdate[4], 4);
	   }
	   else if (len == 6)
	   {
		       memcpy (&year[2], &cdate[4], 2);
			   if (atoi (&year[2]) < 80)
			   {
				   memcpy (year, "20", 2);
			   }
	   }


	   sqldate->day = atoi (day); 
	   sqldate->month = atoi (month); 
	   sqldate->year = atoi (year);
	   return (0);
}
		

int DB_CLASS::FromOdbcDate (DATE_STRUCT *sqldate, LPTSTR cdate)
{
	   int anz;
//           char ndate [12];

	   sqldate->day = 1; 
	   sqldate->month = 1; 
	   sqldate->year = 1900; 
	   CToken t;
	   t.SetSep (_T("-"));
	   t = cdate;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
                      return FromRecDate (sqldate, cdate);
	   }

	   sqldate->day = _tstoi (t.GetToken (0)); 
	   sqldate->month = _tstoi (t.GetToken (1)); 
	   sqldate->year = _tstoi (t.GetToken (2));
	   return (0);
}
		
int DB_CLASS::FromGerDate (DATE_STRUCT *sqldate, LPTSTR cdate)
{
	   int anz;
	   sqldate->day = 1; 
	   sqldate->month = 1; 
	   sqldate->year = 1900; 
	   CToken t;
	   t.SetSep (_T("."));
	   t = cdate;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
               return (FromOdbcDate (sqldate, cdate));
	   }

	   sqldate->day = _tstoi (t.GetToken (0)); 
	   sqldate->month = _tstoi (t.GetToken (1)); 
	   sqldate->year = _tstoi (t.GetToken (2));
	   return (0);
}

int DB_CLASS::ToGerDate (DATE_STRUCT *sqldate, LPTSTR cdate)
{
	   _stprintf (cdate, _T("%02hd.%02hd.%02hd"), sqldate->day,
												sqldate->month,
												sqldate->year);
	   return (0);
}

int DB_CLASS::FromOdbcTime (TIME_STRUCT *sqltime, LPTSTR ctime)
{
	   int anz;

	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
	   CToken t;
	   t.SetSep (_T("-"));
	   t = ctime;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
               return (0);
	   }

	   sqltime->hour = _tstoi (t.GetToken (2)); 
	   sqltime->minute = _tstoi (t.GetToken (1)); 
	   sqltime->second = _tstoi (t.GetToken (0));
	   return (0);
}

int DB_CLASS::FromGerTime (TIME_STRUCT *sqltime, LPTSTR ctime)
{
	   int anz;

	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
	   CToken t;
	   t.SetSep (_T(":"));
	   t = ctime;
	   anz = t.GetAnzToken ();
	   if (anz < 3)
	   {
               return (FromOdbcTime (sqltime, ctime));
	   }

	   sqltime->hour   = _tstoi (t.GetToken (0)); 
	   sqltime->minute = _tstoi (t.GetToken (1)); 
	   sqltime->second = _tstoi (t.GetToken (2));
	   return (0);
}

int DB_CLASS::ToGerTime (TIME_STRUCT *sqltime, LPTSTR ctime)
{
	   _stprintf (ctime, _T("%02hd:%02hd:%02hd"), sqltime->hour,
												sqltime->minute,
												sqltime->second);
	   return (0);
}

int DB_CLASS::FromOdbcTimestamp (TIMESTAMP_STRUCT *sqltime, LPTSTR ctime)
{
       DATE_STRUCT dt;
       TIME_STRUCT tm; 

	   sqltime->day = 0; 
	   sqltime->month = 0; 
	   sqltime->year = 0; 
	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
           if (_tcslen (ctime) < 19)
           {
                    return (0);
           }
           FromOdbcDate (&dt, ctime);
           FromOdbcTime (&tm, &ctime[11]);

	   sqltime->day    = dt.day; 
	   sqltime->month  = dt.month; 
	   sqltime->year   = dt.year;
	   sqltime->hour   = tm.hour; 
	   sqltime->minute = tm.minute; 
	   sqltime->second = tm.second;
	   return (0);
}

int DB_CLASS::FromGerTimestamp (TIMESTAMP_STRUCT *sqltime, LPTSTR ctime)
{

       DATE_STRUCT dt;
       TIME_STRUCT tm; 

	   sqltime->day = 0; 
	   sqltime->month = 0; 
	   sqltime->year = 0; 
	   sqltime->hour = 0; 
	   sqltime->minute = 0; 
	   sqltime->second = 0; 
           FromGerDate (&dt, ctime);
	   sqltime->day    = dt.day; 
	   sqltime->month  = dt.month; 
	   sqltime->year    = dt.year;
           if (_tcslen (ctime) < 11)
           {
                return (0);
           }
           FromGerTime (&tm, &ctime[10]);

	   sqltime->hour   = tm.hour; 
	   sqltime->minute = tm.minute; 
	   sqltime->second = tm.second;
	   return (0);
}

int DB_CLASS::ToGerTimestamp (TIMESTAMP_STRUCT *sqltime, LPTSTR ctime)
{
	   _stprintf (ctime, _T("%02hd.%02hd.%02hd %02hd:%02hd:%02hd "),
                                            sqltime->day,
		                                    sqltime->month,
											sqltime->year,
                                            sqltime->hour,
		                                    sqltime->minute,
											sqltime->second);
	   return (0);
}

int DB_CLASS::CompareDate (DATE_STRUCT *Date1, DATE_STRUCT *Date2)
{
	DbTime d1 (Date1);
	DbTime d2 (Date2);

	time_t t1 = d1.GetTime ();
	time_t t2 = d2.GetTime ();
	if (t1 > t2) return 1;
	if (t1 < t2) return -1;
	return 0;
}

long DB_CLASS::DAscToLong (LPSTR sdate)
{
	DATE_STRUCT DbDate;
	CString cDate = sdate;

	ToDbDate (cDate, &DbDate);
	DbTime t (&DbDate);
	return (long) t.GetTime ();
}

DATE_STRUCT DB_CLASS::DAscToLongEx (LPSTR sdate)
{
	DATE_STRUCT DbDate;
	CString cDate = sdate;

	ToDbDate (cDate, &DbDate);
	return DbDate;
}

LPSTR DB_CLASS::DLongToAsc (long ldate, LPSTR sdate)
{
	CString *cDate;

	DbTime t (ldate);
    cDate = t.GetStringTime (); 
	strcpy (sdate, cDate->GetBuffer ());
	return sdate;
}


LPSTR DB_CLASS::DLongToAsc (DATE_STRUCT& ldate, LPSTR sdate)
{
	CString *cDate;

	DbTime t (&ldate);
    cDate = t.GetStringTime (); 
	strcpy (sdate, cDate->GetBuffer ());
	return sdate;
}


void DB_CLASS::SetToday (int diff, DATE_STRUCT *today, BOOL dbset)
{
	DbTime t;
	t = t + diff;
	if (today != NULL)
	{
		*today = t;
		sqlin ((DATE_STRUCT *) today, SQLDATE, 0);
	}
	else
	{
		m_Today =  t;
		if (dbset)
		{
			sqlin ((DATE_STRUCT *) &m_Today, SQLDATE, 0);
		}
	}
}

}
