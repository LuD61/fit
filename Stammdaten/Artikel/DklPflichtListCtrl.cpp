#include "StdAfx.h"
#include "DklPflichtListCtrl.h"
#include "StrFuncs.h"
#include "resource.h"

CDklPflichtListCtrl::CDklPflichtListCtrl(void)
{
	ListRows.Init ();
}

CDklPflichtListCtrl::~CDklPflichtListCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CDklPflichtListCtrl, CEditListCtrl)
//	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
END_MESSAGE_MAP()


void CDklPflichtListCtrl::FirstEnter ()
{
	if (GetItemCount () > 0)
	{
		StartEnter (1, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (1, 0);
	}
}

void CDklPflichtListCtrl::StartEnter (int col, int row)
{
	if (col == 0) col = 1;
	CEditListCtrl::StartEnter (col, row);
}

void CDklPflichtListCtrl::StopEnter ()
{
	CEditListCtrl::StopEnter ();
}

void CDklPflichtListCtrl::SetSel (CString& Text)
{
/*
   {
		ListEdit.SetSel (0, -1);
   }
*/
	Text.TrimRight ();
	int cpos = Text.GetLength ();
	ListEdit.SetSel (cpos, cpos);
}

void CDklPflichtListCtrl::FormatText (CString& Text)
{
}

void CDklPflichtListCtrl::NextRow ()
{
	int count = GetItemCount ();
	SetEditText ();
	count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		if (AppendEmpty () == FALSE)
		{
			return;
		}
	    StopEnter ();
		EditRow ++;
		EditCol = 0;

	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
	StartEnter (EditCol, EditRow);
}

void CDklPflichtListCtrl::PriorRow ()
{
	if (EditRow <= 0)
	{
			return;
	}
	int count = GetItemCount ();
	StopEnter ();
 
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
}


BOOL CDklPflichtListCtrl::LastCol ()
{
	if (EditCol < 2) return FALSE;
	return TRUE;
}

void CDklPflichtListCtrl::OnReturn ()
{

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (LastCol ())
	{
	    rowCount = GetItemCount ();
		if (EditRow >= rowCount - 1)
		{
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
	    StopEnter ();
		EditRow ++;
//		EditCol = 0;
		if (EditRow == rowCount)
		{
			EditCol = 1;
		}
		else
		{
			EditCol = 2;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CDklPflichtListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditCol ++;
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CDklPflichtListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	StopEnter ();
	EditCol --;
    StartEnter (EditCol, EditRow);
}

BOOL CDklPflichtListCtrl::InsertRow ()
{
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (_T("  "), EditRow, 1);
	FillList.SetItemText (_T("  "), EditRow, 2);
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CDklPflichtListCtrl::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	return CEditListCtrl::DeleteRow ();
}

BOOL CDklPflichtListCtrl::AppendEmpty ()
{
	int rowCount = GetItemCount ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CWnd *header = GetHeaderCtrl ();
	FillList.InsertItem (rowCount, -1);
	FillList.SetItemText (_T("  "), rowCount, 1);
	FillList.SetItemText (_T(" "), rowCount, 2);
	rowCount = GetItemCount ();
	return TRUE;
}

void CDklPflichtListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CDklPflichtListCtrl::RunItemClicked (int Item)
{
}

void CDklPflichtListCtrl::RunCtrlItemClicked (int Item)
{
}

void CDklPflichtListCtrl::RunShiftItemClicked (int Item)
{
}

void CDklPflichtListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	int pos = 0;
	Text = cText.Trim ();
}

void CDklPflichtListCtrl::ScrollPositions (int pos)
{
}

