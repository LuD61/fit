#pragma once
#include "Pr_prot.h"
#include "a_pr.h"

class CPrProt
{
public:
	CString PersName;
    CString Programm;

	APR_CLASS *A_pr;
	PR_PROT_CLASS Pr_prot;

	CPrProt(void);
	CPrProt(CString& PersName, CString& Programm, APR_CLASS *A_pr);
	~CPrProt(void);
	void Construct (CString& PersName, CString& Programm, APR_CLASS *A_pr);
	void Write (double pr_ek_alt, double pr_vk_alt);
};
