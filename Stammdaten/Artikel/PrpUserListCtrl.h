#pragma once
#include <vector>
#include "editlistctrl.h"

class CPrpUserListCtrl :
	public CEditListCtrl
{
protected:
	DECLARE_MESSAGE_MAP()
public:

	std::vector<BOOL> vSelect;
	CVector ListRows;

	CPrpUserListCtrl(void);
	~CPrpUserListCtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
        void GetColValue (int row, int col, CString& Text);
	void ScrollPositions (int pos);
	BOOL LastCol ();
};
