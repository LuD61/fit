// SpannDialogMat.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "SpannDialogMat.h"

// CSpannDialogMat-Dialogfeld

IMPLEMENT_DYNAMIC(CSpannDialogMat, CDialog)

CSpannDialogMat::CSpannDialogMat(CWnd* pParent /*=NULL*/)
	: CDialog(CSpannDialogMat::IDD, pParent)
{
	A_bas = NULL;
	A_kalk_mat = NULL;
	StdCellHeight = 15;
	MarktSpPar = Aufschlag;
	mwst = 1.0;
	visible = FALSE;
}

CSpannDialogMat::~CSpannDialogMat()
{
}

void CSpannDialogMat::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMAT_O_B, m_LMatoB);
	DDX_Control(pDX, IDC_LKOST, m_LKost);
	DDX_Control(pDX, IDC_LSPANNE, m_LSpanne);
	DDX_Control(pDX, IDC_LHK_VOLLK, m_LHkVollk);
	DDX_Control(pDX, IDC_LKALK, m_LKalk);

	DDX_Control(pDX, IDC_MAT_O_B, m_MatoB);
	DDX_Control(pDX, IDC_KOST, m_Kost);
	DDX_Control(pDX, IDC_HK_VOLLK, m_HkVollk);

	DDX_Control(pDX, IDC_PSK, m_PSk);
}


BEGIN_MESSAGE_MAP(CSpannDialogMat, CDialog)
//	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CSpannDialogMat-Meldungshandler

BOOL CSpannDialogMat::OnInitDialog ()
{
	CDialog::OnInitDialog ();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	Form.Add (new CFormField (&m_MatoB,EDIT,    (double *) &A_kalk_mat->a_kalk_mat.mat_o_b, VDOUBLE, 7,3));
	Form.Add (new CFormField (&m_Kost,EDIT,     (double *) &A_kalk_mat->a_kalk_mat.kost, VDOUBLE, 7,3));

	Form.Add (new CFormField (&m_PSk,EDIT,     (double *) &A_kalk_mat->a_kalk_mat.sp_hk, VDOUBLE, 5,1));
	Form.Add (new CFormField (&m_HkVollk,EDIT,  (double *) &A_kalk_mat->a_kalk_mat.hk_vollk, VDOUBLE, 7,3));

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (StdCellHeight);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LMatoB     = new CCtrlInfo (&m_LMatoB, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LMatoB);
	CCtrlInfo *c_LKost     = new CCtrlInfo (&m_LKost, 2, 0, 1, 1); 
	CtrlGrid.Add (c_LKost);

	CCtrlInfo *c_MatoB     = new CCtrlInfo (&m_MatoB, 1, 1, 1, 1); 
	CtrlGrid.Add (c_MatoB);
	CCtrlInfo *c_Kost     = new CCtrlInfo (&m_Kost, 2, 1, 1, 1); 
	CtrlGrid.Add (c_Kost);

	CCtrlInfo *c_LSpanne     = new CCtrlInfo (&m_LSpanne, 1, 3, 1, 1); 
	CtrlGrid.Add (c_LSpanne);
	CCtrlInfo *c_LHkVollk     = new CCtrlInfo (&m_LHkVollk, 2, 3, 1, 1); 
	CtrlGrid.Add (c_LHkVollk);
	CCtrlInfo *c_LKalk     = new CCtrlInfo (&m_LKalk, 2, 3, 1, 1); 
	CtrlGrid.Add (c_LKalk);
	m_LKalk.ShowWindow (SW_HIDE);
	CCtrlInfo *c_PSk     = new CCtrlInfo (&m_PSk, 1, 4, 1, 1); 
	CtrlGrid.Add (c_PSk);

	CCtrlInfo *c_HkVollk     = new CCtrlInfo (&m_HkVollk, 2, 4, 1, 1); 
	CtrlGrid.Add (c_HkVollk);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);

	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display (); 
	return TRUE;
}

void CSpannDialogMat::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}


BOOL CSpannDialogMat::PreTranslateMessage(MSG* pMsg)
{
	return FALSE;
}

void CSpannDialogMat::Show ()
{
	if (!visible) return;
	Form.Show ();
	Calculate ();
}

void CSpannDialogMat::Get ()
{
	if (!visible) return;
	Calculate ();
	Form.Get ();
}

void CSpannDialogMat::SetVisible (BOOL visible)
{
	this->visible = visible;
	if (visible)
	{
		ShowWindow (SW_SHOWNORMAL);
	}
	else
	{
		ShowWindow (SW_HIDE);
	}
}

void CSpannDialogMat::Calculate ()
{
	Form.Get ();

	switch (MarktSpPar)
	{
	case Aufschlag:
	case AufschlagPlus:
//		if (A_kalk_mat->a_kalk_mat.sp_hk != 0.0)
		{
			A_kalk_mat->a_kalk_mat.hk_vollk = (A_kalk_mat->a_kalk_mat.mat_o_b +  
				                               A_kalk_mat->a_kalk_mat.kost)	 * 
											   (1 + A_kalk_mat->a_kalk_mat.sp_hk / 100);
		}
		break;
	case Abschlag:
	case AbschlagPlus:
//		if (A_kalk_mat->a_kalk_mat.sp_hk != 0.0)
		{
			if (A_kalk_mat->a_kalk_mat.sp_hk >= 100) 
			{
				A_kalk_mat->a_kalk_mat.sp_hk = 0.0;
			}
			A_kalk_mat->a_kalk_mat.hk_vollk = (A_kalk_mat->a_kalk_mat.mat_o_b + 
				                               A_kalk_mat->a_kalk_mat.kost)	/ 
											   (1 - A_kalk_mat->a_kalk_mat.sp_hk / 100);
		}
		break;
	}
	Form.Show ();
}