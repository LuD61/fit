#include "stdafx.h"
#include "ChoiceLiefBest.h"
#include "DbUniCode.h"
#include "Process.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceLiefBest::Sort1 = -1;
int CChoiceLiefBest::Sort2 = -1;
int CChoiceLiefBest::Sort3 = -1;
int CChoiceLiefBest::Sort4 = -1;
int CChoiceLiefBest::Sort5 = -1;
int CChoiceLiefBest::Sort6 = -1;

CChoiceLiefBest::CChoiceLiefBest(CWnd* pParent) 
        : CChoiceX(pParent)
{
	Where = "";
	Bean.ArchiveName = _T("LiefBestList.prp");
}

CChoiceLiefBest::~CChoiceLiefBest() 
{
	DestroyList ();
}

void CChoiceLiefBest::DestroyList() 
{
	for (std::vector<CLiefBestList *>::iterator pabl = LiefBestList.begin (); pabl != LiefBestList.end (); ++pabl)
	{
		CLiefBestList *abl = *pabl;
		delete abl;
	}
    LiefBestList.clear ();
}

void CChoiceLiefBest::FillList () 
{
    TCHAR lief_best [17];
    TCHAR lief [17];
	TCHAR adr_krz [17];
    TCHAR a_bz1 [25];
    double a;

	DestroyList ();
	ClearList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl �ber Bestellnummern"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Bestell-Nr"),           1, 250);
    SetCol (_T("Lieferant"),            2, 250);
    SetCol (_T("Name des Lieferanten"), 3, 250);
    SetCol (_T("Artikel-Nr"),           4, 150, LVCFMT_RIGHT);
    SetCol (_T("Artikelbezeichnung"),   5, 250);
	SortRow = 1;

	if (LiefBestList.size () == 0)
	{
		DbClass->sqlout ((LPTSTR)  lief_best,   SQLCHAR, sizeof (lief_best));
		DbClass->sqlout ((LPTSTR)  lief,        SQLCHAR, sizeof (lief));
		DbClass->sqlout ((LPTSTR)  adr_krz,     SQLCHAR, sizeof (adr_krz));
		DbClass->sqlout ((double *) &a,         SQLDOUBLE, 0);
		DbClass->sqlout ((LPTSTR)  a_bz1,       SQLCHAR, sizeof (a_bz1));

		CString Sql = _T("select lief_bzg.lief_best, lief_bzg.lief, adr.adr_krz, a_bas.a, a_bas.a_bz1 "
					     "from lief_bzg,a_bas,lief, adr where a_bas.a = lief_bzg.a " 
						 "and lief.lief = lief_bzg.lief "
						 "and adr.adr = lief.adr");
		Sql += " ";
		Sql += Where;

		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) lief_best;
			CDbUniCode::DbToUniCode (lief_best, pos);
			pos = (LPSTR) lief;
			CDbUniCode::DbToUniCode (lief, pos);
			pos = (LPSTR) adr_krz;
			CDbUniCode::DbToUniCode (adr_krz, pos);
			pos = (LPSTR) a_bz1;
			CDbUniCode::DbToUniCode (a_bz1, pos);
			CLiefBestList *abl = new CLiefBestList (lief_best, lief, adr_krz, a_bz1, a);
			LiefBestList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
		Load ();
	}

	for (std::vector<CLiefBestList *>::iterator pabl = LiefBestList.begin (); pabl != LiefBestList.end (); ++pabl)
	{
		CLiefBestList *abl = *pabl;
		CString Num;
		CString Bez;
		_tcscpy (lief_best, abl->a_best.GetBuffer ());
		_tcscpy (lief,    abl->lief.GetBuffer ());
		_tcscpy (adr_krz, abl->adr_krz.GetBuffer ());
		_tcscpy (a_bz1,   abl->a_bz1.GetBuffer ());
		CString Art;
		Art.Format (_T("%.0lf"), abl->a); 

		CString LText;
		LText.Format (_T("%.s %s"), abl->a_best, abl->lief);
		if (Style == LVS_REPORT)
		{
                Num = lief_best;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (lief, i, 2);
        ret = SetItemText (adr_krz, i, 3);
        ret = SetItemText (Art.GetBuffer (), i, 4);
        ret = SetItemText (a_bz1, i, 5);
        i ++;
    }

    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort (listView);
}


void CChoiceLiefBest::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}

void CChoiceLiefBest::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceLiefBest::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;	
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLiefBest::SearchLief (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLiefBest::SearchAdrKrz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLiefBest::SearchA (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 4);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLiefBest::SearchBz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 5);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceLiefBest::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
	if ((EditText.Find (_T("*")) != -1) || EditText.Find (_T("?")) != -1)
	{
		CChoiceX::SearchMatch (EditText);
		return;
	}
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (13));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchLief (ListBox, EditText.GetBuffer ());
             break;
        case 3 :
             EditText.MakeUpper ();
             SearchAdrKrz (ListBox, EditText.GetBuffer ());
             break;
        case 4 :
             SearchA (ListBox, EditText.GetBuffer (13));
             break;
        case 5 :
             EditText.MakeUpper ();
             SearchBz (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceLiefBest::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceLiefBest::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort2;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   else if (SortRow == 4)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort5;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort5);
	   }
	   return 0;
   }
   else if (SortRow == 5)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort6;
   }
   return 0;
}


void CChoiceLiefBest::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
        case 4:
              Sort5 *= -1;
			  if (Sort5 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CLiefBestList *abl = LiefBestList [i];
		   
		   abl->a_best = ListBox->GetItemText (i, 1);
		   abl->lief   = ListBox->GetItemText (i, 2);
		   abl->adr_krz   = ListBox->GetItemText (i, 3);
		   abl->a      = _tstof (ListBox->GetItemText (i, 4));
		   abl->a_bz1  = ListBox->GetItemText (i, 5);
	}
	for (int i = 1; i <= 9; i ++)
	{
		SetItemSort (i, 2);
	}
	SetItemSort (SortRow, SortPos);
}

void CChoiceLiefBest::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = LiefBestList [idx];
}

CLiefBestList *CChoiceLiefBest::GetSelectedText ()
{
	CLiefBestList *abl = (CLiefBestList *) SelectedRow;
	return abl;
}

void CChoiceLiefBest::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (LiefBestList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}

void CChoiceLiefBest::SendSelect ()
{
	pParent->SendMessage (WM_COMMAND, LIEFBESTSELECTED, 0l);
}

void CChoiceLiefBest::SendCancel ()
{
	pParent->SendMessage (WM_COMMAND, LIEFBESTCANCELED, 0l);
}

void CChoiceLiefBest::SetDefault ()
{
	CChoiceX::SetDefault ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    ListBox->SetColumnWidth (0, 0);
    ListBox->SetColumnWidth (1, 150);
    ListBox->SetColumnWidth (2, 250);
    ListBox->SetColumnWidth (3, 150);
    ListBox->SetColumnWidth (4, 250);
    ListBox->SetColumnWidth (5, 250);
}

void CChoiceLiefBest::OnEnter ()
{
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cLief = m_List.GetItemText (idx, 2);  
		Message.Format (_T("%s"), cLief.GetBuffer ());
		ToClipboard (Message);
	}
	p.SetCommand (_T("18510"));
	HANDLE Pid = p.Start ();
}
