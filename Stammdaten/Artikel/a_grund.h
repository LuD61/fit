#ifndef _A_GRUND_DEF
#define _A_GRUND_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_GRUND {
   double         a;
   short          rgb_r;
   short          rgb_g;
   short          rgb_b;
};
extern struct A_GRUND a_grund, a_grund_null;

#line 8 "a_grund.rh"

class A_GRUND_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               A_GRUND a_grund;
               A_GRUND_CLASS () : DB_CLASS ()
               {
               }
};
#endif
