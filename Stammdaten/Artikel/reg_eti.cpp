#include "stdafx.h"
#include "reg_eti.h"

struct REG_ETI reg_eti, reg_eti_null;

void REG_ETI_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &reg_eti.mdn, SQLSHORT, 0);
            sqlin ((short *)   &reg_eti.fil, SQLSHORT, 0);
            sqlin ((long *)  &reg_eti.nr, SQLLONG, 0);
    sqlout ((double *) &reg_eti.a,SQLDOUBLE,0);
    sqlout ((short *) &reg_eti.anz,SQLSHORT,0);
    sqlout ((long *) &reg_eti.nr,SQLLONG,0);
    sqlout ((short *) &reg_eti.reg,SQLSHORT,0);
    sqlout ((short *) &reg_eti.fil,SQLSHORT,0);
    sqlout ((short *) &reg_eti.mdn,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &reg_eti.dat,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &reg_eti.dat_akv,SQLDATE,0);
    sqlout ((TCHAR *) reg_eti.erf_kz,SQLCHAR,2);
    sqlout ((double *) &reg_eti.vk_pr,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select reg_eti.a,  ")
_T("reg_eti.anz,  reg_eti.nr,  reg_eti.reg,  reg_eti.fil,  reg_eti.mdn,  ")
_T("reg_eti.dat,  reg_eti.dat_akv,  reg_eti.erf_kz,  reg_eti.vk_pr from reg_eti ")

#line 14 "reg_eti.rpp"
                                  _T("where mdn = ? ")
				  _T("and fil = ?")
				  _T("and nr = ?"));
    sqlin ((double *) &reg_eti.a,SQLDOUBLE,0);
    sqlin ((short *) &reg_eti.anz,SQLSHORT,0);
    sqlin ((long *) &reg_eti.nr,SQLLONG,0);
    sqlin ((short *) &reg_eti.reg,SQLSHORT,0);
    sqlin ((short *) &reg_eti.fil,SQLSHORT,0);
    sqlin ((short *) &reg_eti.mdn,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &reg_eti.dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &reg_eti.dat_akv,SQLDATE,0);
    sqlin ((TCHAR *) reg_eti.erf_kz,SQLCHAR,2);
    sqlin ((double *) &reg_eti.vk_pr,SQLDOUBLE,0);
            sqltext = _T("update reg_eti set reg_eti.a = ?,  ")
_T("reg_eti.anz = ?,  reg_eti.nr = ?,  reg_eti.reg = ?,  reg_eti.fil = ?,  ")
_T("reg_eti.mdn = ?,  reg_eti.dat = ?,  reg_eti.dat_akv = ?,  ")
_T("reg_eti.erf_kz = ?,  reg_eti.vk_pr = ? ")

#line 18 "reg_eti.rpp"
                                  _T("where mdn = ? ")
				  _T("and fil = ?")
				  _T("and nr = ?");
            sqlin ((short *)   &reg_eti.mdn, SQLSHORT, 0);
            sqlin ((short *)   &reg_eti.fil, SQLSHORT, 0);
            sqlin ((long *)  &reg_eti.nr, SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &reg_eti.mdn, SQLSHORT, 0);
            sqlin ((short *)   &reg_eti.fil, SQLSHORT, 0);
            sqlin ((long *)  &reg_eti.nr, SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select nr from reg_eti ")
                                  _T("where mdn = ? ")
				  _T("and fil = ?")
				  _T("and nr = ?"));
            sqlin ((short *)   &reg_eti.mdn, SQLSHORT, 0);
            sqlin ((short *)   &reg_eti.fil, SQLSHORT, 0);
            sqlin ((long *)  &reg_eti.nr, SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from reg_eti ")
                                  _T("where mdn = ? ")
				  _T("and fil = ?")
				  _T("and nr = ?"));
    sqlin ((double *) &reg_eti.a,SQLDOUBLE,0);
    sqlin ((short *) &reg_eti.anz,SQLSHORT,0);
    sqlin ((long *) &reg_eti.nr,SQLLONG,0);
    sqlin ((short *) &reg_eti.reg,SQLSHORT,0);
    sqlin ((short *) &reg_eti.fil,SQLSHORT,0);
    sqlin ((short *) &reg_eti.mdn,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &reg_eti.dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &reg_eti.dat_akv,SQLDATE,0);
    sqlin ((TCHAR *) reg_eti.erf_kz,SQLCHAR,2);
    sqlin ((double *) &reg_eti.vk_pr,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into reg_eti (")
_T("a,  anz,  nr,  reg,  fil,  mdn,  dat,  dat_akv,  erf_kz,  vk_pr) ")

#line 41 "reg_eti.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?)")); 

#line 43 "reg_eti.rpp"
}

BOOL REG_ETI_CLASS::operator== (REG_ETI& reg_eti)
{
            if (this->reg_eti.mdn    != reg_eti.mdn) return FALSE;  
            if (this->reg_eti.fil    != reg_eti.fil) return FALSE;  
            if (this->reg_eti.nr     != reg_eti.nr) return FALSE;  
            return TRUE;
} 
