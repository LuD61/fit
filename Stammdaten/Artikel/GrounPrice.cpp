// GrounPrice.cpp: Implementierungsdatei
//

#include "stdafx.h"
#ifdef ARTIKEL
#include "Artikel.h"
#else
#include "AprMan.h"
#endif
#include "GrounPrice.h"


// CGrounPrice-Dialogfeld

IMPLEMENT_DYNAMIC(CGrounPrice, CDialog)

CGrounPrice::CGrounPrice(CWnd* pParent /*=NULL*/)
	: CDialog(CGrounPrice::IDD, pParent)
{
	StdCellHeight = 15;
	Initialized = FALSE;
}

CGrounPrice::~CGrounPrice()
{
}

void CGrounPrice::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LPRICE1, m_LPrice1);
	DDX_Control(pDX, IDC_GROUNDPRICE1, m_GroundPrice1);
	DDX_Control(pDX, IDC_LPRICE2, m_LPrice2);
	DDX_Control(pDX, IDC_GROUNDPRICE2, m_GroundPrice2);
	DDX_Control(pDX, IDC_LPRICE3, m_LPrice3);
	DDX_Control(pDX, IDC_GROUNDPRICE3, m_GroundPrice3);
	DDX_Control(pDX, IDC_LPRICE_GROUP, m_LPriceGroup);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDCANCEL, m_Cancel);
}


BEGIN_MESSAGE_MAP(CGrounPrice, CDialog)
END_MESSAGE_MAP()


// CGrounPrice-Meldungshandler

BOOL CGrounPrice::OnInitDialog ()
{
	CDialog::OnInitDialog ();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	Form.Add (new CFormField (&m_GroundPrice1,EDIT,    (double *) &Price1, VDOUBLE, 6,2));
	Form.Add (new CFormField (&m_GroundPrice2,EDIT,    (double *) &Price2, VDOUBLE, 6,2));
	Form.Add (new CFormField (&m_GroundPrice3,EDIT,    (double *) &Price3, VDOUBLE, 6,2));

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (StdCellHeight);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    PriceGrid.Create (this, 20, 20);
	PriceGrid.SetFont (&Font);
    PriceGrid.SetBorder (12, 20);
    PriceGrid.SetCellHeight (StdCellHeight);
    PriceGrid.SetFontCellHeight (this, &Font);
    PriceGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_PriceGrid   = new CCtrlInfo (&PriceGrid, 1, 1, 1, 1); 

	CCtrlInfo *c_PriceGroup  = new CCtrlInfo (&m_LPriceGroup, 0, 0, 4, 5); 
	PriceGrid.Add (c_PriceGroup);
	CCtrlInfo *c_LPrice1  = new CCtrlInfo (&m_LPrice1, 1, 1, 1, 1); 
	PriceGrid.Add (c_LPrice1);
	CCtrlInfo *c_GroundPrice1  = new CCtrlInfo (&m_GroundPrice1, 2, 1, 1, 1); 
	PriceGrid.Add (c_GroundPrice1);
	CCtrlInfo *c_LPrice2  = new CCtrlInfo (&m_LPrice2, 1, 2, 1, 1); 
	PriceGrid.Add (c_LPrice2);
	CCtrlInfo *c_GroundPrice2  = new CCtrlInfo (&m_GroundPrice2, 2, 2, 1, 1); 
	PriceGrid.Add (c_GroundPrice2);
	CCtrlInfo *c_LPrice3  = new CCtrlInfo (&m_LPrice3, 1, 3, 1, 1); 
	PriceGrid.Add (c_LPrice3);
	CCtrlInfo *c_GroundPrice3  = new CCtrlInfo (&m_GroundPrice3, 2, 3, 1, 1); 
	PriceGrid.Add (c_GroundPrice3);

	CCtrlInfo *c_OK  = new CCtrlInfo (&m_OK, DOCKRIGHT, 1, 1, 1); 
	PriceGrid.Add (c_OK);
	CCtrlInfo *c_Cancel  = new CCtrlInfo (&m_Cancel, DOCKRIGHT, 2, 1, 1); 
	PriceGrid.Add (c_Cancel);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display (); 
	Form.Show ();
	Initialized = TRUE;
	return TRUE;
}

BOOL CGrounPrice::PreTranslateMessage(MSG* pMsg)
{
	return CDialog::PreTranslateMessage (pMsg);
}

void CGrounPrice::OnOK ()
{
	Form.Get ();
	CDialog::OnOK ();
}
 
