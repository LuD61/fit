// Ean.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "Ean.h"
#include "UniFormField.h"
#include ".\ean.h"


// CEan-Dialogfeld

IMPLEMENT_DYNAMIC(CEan, CDialog)
CEan::CEan(CWnd* pParent /*=NULL*/)
	: CDialog(CEan::IDD, pParent)
{
	AkrzGen = FALSE;
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBrush = NULL;
	dcs_a_par = -1;
	waa_a_par = -1;
	KzCursor = -1;
	TestKzCursor = -1;
}

CEan::~CEan()
{
	if (KzCursor != -1)
	{
		A_ean->sqlclose (KzCursor);
		KzCursor = -1;
	}
	if (TestKzCursor != -1)
	{
		A_ean->sqlclose (TestKzCursor);
		TestKzCursor = -1;
	}
}

void CEan::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LEAN_NR, m_LEanNr);
	DDX_Control(pDX, IDC_EAN_NR, m_EanNr);
	DDX_Control(pDX, IDC_LEAN_BZ, m_LEanBz);
	DDX_Control(pDX, IDC_EAN_BZ, m_EanBz);
	DDX_Control(pDX, IDC_HEAN, m_HEan);
	DDX_Control(pDX, IDC_LA_KRZ, m_LAkrz);
	DDX_Control(pDX, IDC_A_KRZ, m_Akrz);
	DDX_Control(pDX, IDC_EAN_GROUP, m_EanGroup);
	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_A, m_A);
}


BEGIN_MESSAGE_MAP(CEan, CDialog)
	ON_WM_CTLCOLOR ()
	ON_BN_CLICKED(IDC_HEAN,    OnBnClickedHean)
	ON_BN_CLICKED(IDC_EANSAVE, OnSave)
    ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipNotify)
    ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipNotify)
	ON_EN_KILLFOCUS(IDC_EAN_NR, OnEnKillfocusEanNr)
END_MESSAGE_MAP()


// CEan-Meldungshandler

BOOL CEan::OnInitDialog()
{

	CDialog::OnInitDialog();

//    m_Save.Create (_T("Speichern"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
//		                          CRect (0, 0, 80, 20),
//		                          this, IDC_EANSAVE);
    m_Save.Create (_T("Speichern"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_EANSAVE);
	m_Save.LoadBitmap (IDB_SAVE);
	m_Save.SetToolTip (_T("EAN-Nummer\nspeichern"));

	A_bas = &Basis->A_bas;
	A_hndw = &Basis->A_hndw;
	A_eig = &Basis->A_eig;
	A_ean = &Basis->A_ean;
	A_krz = &Basis->A_krz;
	Sys_par = &Basis->Sys_par;

    A_ean->sqlin ((double *) &A_bas->a_bas.a, SQLDOUBLE, 0);
	KzCursor = A_ean->sqlcursor (_T("update a_ean set h_ean_kz = \"0\" where a = ?"));

    A_ean->sqlin ((double *) &A_bas->a_bas.a, SQLDOUBLE, 0);
    A_ean->sqlin ((double *) &A_ean->a_ean.ean, SQLDOUBLE, 0);
    A_ean->sqlout ((long *) &KzCount, SQLLONG, 0);
	TestKzCursor = A_ean->sqlcursor (_T("select count (*) from a_ean where a = ? and ean != ? and h_ean_kz = \"1\""));

    SetAkrzField ();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

    Form.Add (new CFormField    (&m_A,EDIT,          (double *) &A_bas->a_bas.a, VDOUBLE));
    Form.Add (new CFormField    (&m_EanNr,EDIT,      (double *) &A_ean->a_ean.ean, VDOUBLE));
    Form.Add (new CUniFormField (&m_EanBz,EDIT,      (LPTSTR)   A_ean->a_ean.ean_bz, VCHAR));
    Form.Add (new CUniFormField (&m_HEan, CHECKBOX,  (LPTSTR)   A_ean->a_ean.h_ean_kz, VCHAR));
    Form.Add (new CUniFormField (&m_Akrz, EDIT,      (long *)   &A_krz->a_krz.a_krz,  VLONG));
	m_EanNr.SetLimitText (13);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (10, 10);
    CtrlGrid.SetCellHeight (15);
//    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	m_A.SetReadOnly ();
	CCtrlInfo *c_EanGroup = new CCtrlInfo (&m_EanGroup, 0, 0, 5, 6);
	CtrlGrid.Add (c_EanGroup);

	CCtrlInfo *c_LA = new CCtrlInfo (&m_LA, 1, 1, 1, 1);
	CtrlGrid.Add (c_LA);

	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 2, 1, 1, 1);
	CtrlGrid.Add (c_A);

	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 3, 1, 1, 1);
	c_Save->SetCellPos (30, 0, 0, 0);
	CtrlGrid.Add (c_Save);

	CCtrlInfo *c_LEanNr = new CCtrlInfo (&m_LEanNr, 1, 2, 1, 1);
	CtrlGrid.Add (c_LEanNr);

	CCtrlInfo *c_EanNr = new CCtrlInfo (&m_EanNr, 2, 2, 1, 1);
	CtrlGrid.Add (c_EanNr);

	CCtrlInfo *c_LEanBz = new CCtrlInfo (&m_LEanBz, 1, 3, 1, 1);
	CtrlGrid.Add (c_LEanBz);

	CCtrlInfo *c_EanBz = new CCtrlInfo (&m_EanBz, 2, 3, 2, 1);
	CtrlGrid.Add (c_EanBz);

	CCtrlInfo *c_HEan = new CCtrlInfo (&m_HEan, 2, 4, 1, 1);
	CtrlGrid.Add (c_HEan);

	CCtrlInfo *c_LAkrz = new CCtrlInfo (&m_LAkrz, 1, 5, 1, 1);
	CtrlGrid.Add (c_LAkrz);

	CCtrlInfo *c_Akrz = new CCtrlInfo (&m_Akrz, 2, 5, 1, 1);
	CtrlGrid.Add (c_Akrz);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Save.Tooltip.SetFont (&Font);
	m_Save.Tooltip.SetSize ();
/*
	m_Save.Tooltip.SetText (_T("Speichert den aktuellenSatz.\nEin Rundlauf in der Maske\nbewirkt dasselbe"));
	m_Save.Tooltip.WindowOrientation = CQuikInfo::Center;
	m_Save.Tooltip.Orientation = CQuikInfo::Left;
*/

	CtrlGrid.Display ();
	if (A_ean->a_ean.h_ean_kz[0] == _T('0'))
	{
		A_ean->a_ean.h_ean_kz[0] = _T('N');
	}
	else
	{
		A_ean->a_ean.h_ean_kz[0] = _T('J');
	}

	Form.Show ();
    m_EanNr.SetFocus ();
	return FALSE;
}

HBRUSH CEan::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			m_Save.SetBkColor(DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CEan::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}
	

BOOL CEan::PreTranslateMessage(MSG* pMsg)
{
//	CWnd *cWnd = NULL;
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
                if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				Control = GetNextDlgTabItem (Control, TRUE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
//				Update->Back ();
//				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				UpdateList ();
				((CDbPropertyPage *) GetParent ()->GetParent ())->Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return FALSE;
}

void CEan::SetBasis (CBasisdatenPage *Basis)
{
	this->Basis = Basis;
}

void CEan::Update ()
{
	SetAkrzField ();
	if (A_ean->a_ean.ean != 0.0)
	{
		memcpy (&A_krz->a_krz, &a_krz_null, sizeof (A_KRZ));
		A_krz->a_krz.a_herk = A_ean->a_ean.ean;
		A_krz->dbreada_herkfirst ();
		if (A_krz->a_krz.a_krz == 0l && A_ean->a_ean.ean != 0.0)
		{
			FillAkrz ();
		}
		if (strcmp ((LPSTR) A_ean->a_ean.h_ean_kz, "1") == 0)
		{
			m_HEan.EnableWindow (FALSE);
		}
		else
		{
			m_HEan.EnableWindow ();
		}
	}
	Form.Show ();
}


BOOL CEan::OnReturn ()
{
	CWnd * Control = GetFocus ();
	if (Control == &m_EanNr)
	{
		if (!TestEan ()) return FALSE;
	}
	if (AkrzGen && Control == &m_HEan)
	{
		UpdateList ();
	}
	else if (!m_Akrz.IsWindowVisible () && Control == &m_HEan)
	{
		UpdateList ();
	}
	else if (!AkrzGen && Control == &m_Akrz)
	{
		UpdateList ();
	}
	else if (AkrzGen && Control == &m_EanBz)
	{
		Form.Get ();
		if (_tcscmp (A_ean->a_ean.h_ean_kz, _T("J")) == 0)
		{
			UpdateList ();
		}
	}
	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return TRUE;
	}
	return TRUE;
}

BOOL CEan::TestEan ()
{
	Form.Get ();

	CString Msg;
	if (A_ean->dbreadeanfirst () == 0)
	{
		if (A_bas->a_bas.a != A_ean->a_ean.a)
		{
			Msg.Format (_T("Warnung : Die Eannummer %.0lf ist dem Artikel %.0lf zugeordnet Trotzdem speichern ? "),
		        A_ean->a_ean.ean, A_ean->a_ean.a);
			int ret = MessageBox (Msg.GetBuffer (), NULL, MB_YESNO | MB_ICONQUESTION);
			if (ret == IDNO)
			{
				return FALSE;
			}
		}
	}
	A_ean->a_ean.a = A_bas->a_bas.a;
	if (A_ean->dbreadfirst () == 100)
	{
		_tcscpy (A_ean->a_ean.ean_bz, A_bas->a_bas.a_bz1);
		FillAkrz ();
		_tcscpy (A_ean->a_ean.h_ean_kz, _T("N"));
		m_HEan.EnableWindow ();
		Form.Show ();
		return TRUE;
	}

	if (A_bas->a_bas.a == A_ean->a_ean.a)
	{
		return TRUE;
	}

	return FALSE;

}

void CEan::UpdateList ()
{
	Form.Get ();
	A_ean->a_ean.a = A_bas->a_bas.a;
	if (A_ean->a_ean.h_ean_kz[0] == _T('J'))
	{
		A_ean->a_ean.h_ean_kz[0] = _T('1');
	}
	else
	{
		A_ean->a_ean.h_ean_kz[0] = _T('0');
	}

	A_ean->dbupdate ();
	if (!AkrzGen && GetFocus () == &m_Akrz && A_krz->a_krz.a_krz != 0)
	{
		long a_krz = A_krz->a_krz.a_krz;
		A_krz->a_krz.a = A_bas->a_bas.a;
		A_krz->a_krz.a_herk = A_ean->a_ean.ean;
		if (A_krz->dbreada_herkfirst () == 0)
		{
			A_krz->dbdelete ();
			if (a_krz != A_krz->a_krz.a_krz && 
				_tcscmp (A_hndw->a_hndw.verk_art, _T("K")) == 0)
			{
				Basis->AutoNr.nveinid (0, 0, "a_krz", A_krz->a_krz.a_krz);
			}
		}
		A_krz->a_krz.a_krz = a_krz;
		strcpy ((LPSTR) A_krz->a_krz.herk_kz, "2"); 
		A_krz->a_krz.anz_a = 1;
		A_krz->dbupdate ();
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update ();
	}
}


void CEan::SetAkrzField ()
{
	if (dcs_a_par == -1)
	{
		strcpy ((LPSTR) sys_par.sys_par_nam, "dcs_a_par");
		if (Sys_par->dbreadfirst () == 0)
		{
			LPSTR p = (LPSTR) sys_par.sys_par_wrt;
			CDbUniCode::DbToUniCode (sys_par.sys_par_wrt, p);
			dcs_a_par = _tstoi (sys_par.sys_par_wrt);
		}
	}

	if (waa_a_par == -1)
	{
		strcpy ((LPSTR) sys_par.sys_par_nam, "waa_a_par");
		if (Sys_par->dbreadfirst () == 0)
		{
			LPSTR p = (LPSTR) sys_par.sys_par_wrt;
			CDbUniCode::DbToUniCode (sys_par.sys_par_wrt, p);
			waa_a_par = _tstoi (sys_par.sys_par_wrt);
		}
	}

	if (A_bas->a_bas.a_typ == Basis->Hndw && _tcscmp (A_hndw->a_hndw.verk_art, _T("K")) == 0)
	{
		if (dcs_a_par == 0)
		{
			AkrzGen = FALSE;
			m_LAkrz.ShowWindow (SW_HIDE);
			m_Akrz.ShowWindow (SW_HIDE);
		}
		else if (dcs_a_par == 1)
		{
			AkrzGen = TRUE;
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (FALSE);
			AkrzGen = TRUE;
		}
		else if (dcs_a_par == 2)
		{
			AkrzGen = FALSE;
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (TRUE);
			AkrzGen = FALSE;
		}
	}
	else if (A_bas->a_bas.a_typ == Basis->Hndw && (_tcscmp (A_hndw->a_hndw.verk_art, _T("B")) == 0 ||
												 _tcscmp (A_hndw->a_hndw.verk_art, _T("C")) == 0 ||
												 _tcscmp (A_hndw->a_hndw.verk_art, _T("S")) == 0))
	{
		AkrzGen = FALSE;
		if (waa_a_par == 0)
		{
			m_LAkrz.ShowWindow (SW_HIDE);
			m_Akrz.ShowWindow (SW_HIDE);
		}
		else if (waa_a_par == 1)
		{
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (FALSE);
			AkrzGen = TRUE;
		}
		else if (waa_a_par == 2)
		{
			AkrzGen = FALSE;
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (TRUE);
			AkrzGen = FALSE;
		}
	}

	else if (A_bas->a_bas.a_typ == Basis->Eig && _tcscmp (A_eig->a_eig.verk_art, _T("K")) == 0)
	{
		AkrzGen = FALSE;
		if (dcs_a_par == 0)
		{
			m_LAkrz.ShowWindow (SW_HIDE);
			m_Akrz.ShowWindow (SW_HIDE);
		}
		else if (dcs_a_par == 1)
		{
			AkrzGen = TRUE;
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (FALSE);
		}
		else if (dcs_a_par == 2)
		{
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (TRUE);
		}
	}
	else if (A_bas->a_bas.a_typ == Basis->Eig && (_tcscmp (A_eig->a_eig.verk_art, _T("B")) == 0 ||
												 _tcscmp (A_eig->a_eig.verk_art, _T("C")) == 0 ||
												 _tcscmp (A_eig->a_eig.verk_art, _T("S")) == 0))
	{
		AkrzGen = FALSE;
		if (waa_a_par == 0)
		{
			m_LAkrz.ShowWindow (SW_HIDE);
			m_Akrz.ShowWindow (SW_HIDE);
		}
		else if (waa_a_par == 1)
		{
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (FALSE);
			AkrzGen = TRUE;
		}
		else if (waa_a_par == 2)
		{
			m_LAkrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.ShowWindow (SW_SHOWNORMAL);
			m_Akrz.EnableWindow (TRUE);
		}
	}
	else
	{
			m_LAkrz.ShowWindow (SW_HIDE);
			m_Akrz.ShowWindow (SW_HIDE);
	}
}

void CEan::FillAkrz ()
{

	if (A_hndw->a_hndw.a_krz != 0l)
	{
		A_krz->a_krz.a = A_bas->a_bas.a;
		A_krz->a_krz.a_krz = A_hndw->a_hndw.a_krz;
		A_krz->a_krz.a_herk = A_ean->a_ean.ean;
		strcpy ((LPSTR) A_krz->a_krz.herk_kz, "2"); 
		A_krz->a_krz.anz_a = 1;
		A_krz->dbupdate ();
		A_hndw->a_hndw.a_krz = 0l;
		Basis->Update->Show (1);
		return;
	}
	if (_tcscmp (A_hndw->a_hndw.verk_art, _T("K")) == 0)
	{
		if (dcs_a_par == 1)
		{
			if (!GenAkrz ())
			{
				MessageBox (_T("Es konnte keien LKurznummer generiert werden"));
				return;
			}
			A_krz->a_krz.a = A_bas->a_bas.a;
			A_krz->a_krz.a_herk = A_ean->a_ean.ean;
			A_krz->a_krz.a = A_bas->a_bas.a;
			strcpy ((LPSTR) A_krz->a_krz.herk_kz, "2"); 
			A_krz->a_krz.anz_a = 1;
			A_krz->dbupdate ();
		}
		else if (dcs_a_par == 2)
		{
/*
			if (A_bas->a_bas.a <= 999999.0)
			{
				A_krz->a_krz.a_krz = (long) A_bas->a_bas.a;
				A_krz->a_krz.a = A_bas->a_bas.a;
				A_krz->a_krz.a_herk = A_ean->a_ean.ean;
				strcpy ((LPSTR) A_krz->a_krz.herk_kz, "2"); 
				A_krz->a_krz.anz_a = 1;
				A_krz->dbupdate ();
			}
*/
			if (!GenAkrz ())
			{
				return;
			}
			A_krz->a_krz.a = A_bas->a_bas.a;
			A_krz->a_krz.a_herk = A_ean->a_ean.ean;
			A_krz->a_krz.a = A_bas->a_bas.a;
			strcpy ((LPSTR) A_krz->a_krz.herk_kz, "2"); 
			A_krz->a_krz.anz_a = 1;
			A_krz->dbupdate ();
		}
	}
	else if (_tcscmp (A_hndw->a_hndw.verk_art, _T("B")) == 0 ||
			 _tcscmp (A_hndw->a_hndw.verk_art, _T("C")) == 0 ||
			 _tcscmp (A_hndw->a_hndw.verk_art, _T("S")) == 0)
	{
			if (waa_a_par != 0)
			{
				A_krz->a_krz.a = A_bas->a_bas.a;
				if (A_krz->dbreadafirst () == 0) 
				{
					A_krz->a_krz.a_krz = 0l;
					return;
				}
				if (A_bas->a_bas.a <= 999999.0)
				{
					A_krz->a_krz.a_krz = (long) A_bas->a_bas.a;
//					A_krz->a_krz.a = A_hndw->a_hndw.a;
					A_krz->a_krz.a = A_bas->a_bas.a;
					A_krz->a_krz.a_herk = A_ean->a_ean.ean;
					strcpy ((LPSTR) A_krz->a_krz.herk_kz, "2"); 
					A_krz->a_krz.anz_a = 1;
					A_krz->dbupdate ();
				}
			}
	}
}

BOOL CEan::GenAkrz ()
{
	Basis->AutoNr.GenAutoNr (0, 0, _T("a_krz"));
	A_krz->a_krz.a_krz = Basis->AutoNr.Nr;
    if (A_krz->a_krz.a_krz == 0l) return FALSE;
	memcpy (&a_krz, &A_krz->a_krz, sizeof (A_KRZ));
	A_krz->a_krz.a_krz = Basis->AutoNr.Nr;
    int dsqlstatus = A_krz->dbreadfirst ();
	if (dsqlstatus == 100) return TRUE;
	int i = 0;
	while (dsqlstatus != 100)
	{
		if (A_krz->a_krz.a == A_bas->a_bas.a) break;
		memcpy (&A_krz->a_krz, &a_krz, sizeof (A_KRZ));
		if (i == 100)
		{
			A_krz->a_krz.a_krz = 0;
			return FALSE;
		}
		Sleep (100);
		i ++;
		Basis->AutoNr.GenAutoNr (0, 0, _T("a_krz"));
		A_krz->a_krz.a_krz = Basis->AutoNr.Nr;
		if (A_krz->a_krz.a_krz == 0l) 
		{
			A_krz->a_krz.a_krz = 0;
			return FALSE;
		}
		memcpy (&a_krz, &A_krz->a_krz, sizeof (A_KRZ));
        dsqlstatus = A_krz->dbreadfirst ();
	}
	return TRUE;
}

void CEan::OnBnClickedHean()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	if (A_ean->a_ean.ean == 0.0) return;
	if (m_HEan.GetCheck () == BST_CHECKED)
	{
		A_ean->sqlexecute (KzCursor);
		strcpy ((LPSTR) A_ean->a_ean.h_ean_kz, "1");
        A_ean->a_ean.a = A_bas->a_bas.a;
		A_ean->dbupdate ();
		_tcscpy (A_ean->a_ean.h_ean_kz, _T("J"));
		CWnd *Control = GetNextDlgTabItem (&m_HEan, FALSE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}
		m_HEan.EnableWindow (FALSE);
	}
	else
	{
	}
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update ();
	}
}

void CEan::Delete ()
{

	CWnd *Control = GetFocus ();
	if (Control->GetParent () != this) return;
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->DeleteRow ();
	}

}
	
void CEan::DeleteRow ()
{

	Form.Get ();
	A_ean->dbdelete ();
	memcpy (&A_ean->a_ean, &a_ean_null, sizeof (A_EAN));
	memcpy (&A_krz->a_krz, &a_krz_null, sizeof (A_KRZ));
	Form.Show ();
	for (std::vector<CUpdateEvent *>::iterator e =  UpdateTab.begin (); e != UpdateTab.end (); ++e)
	{
		CUpdateEvent *event = *e;
		event->Update ();
	}
}

void CEan::OnSave ()
{
	if (!TestEan ()) return;
	UpdateList ();
	PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
}


BOOL CEan::OnToolTipNotify(UINT id, NMHDR *pNMHDR,
   LRESULT *pResult)
{
   // need to handle both ANSI and UNICODE versions of the message
   TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
   TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
   CString strTipText;
   UINT_PTR nID = pNMHDR->idFrom;
   if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) ||
      pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
   {
      // idFrom is actually the HWND of the tool
      nID = ::GetDlgCtrlID((HWND)nID);
   }
   if (nID != 0) // will be zero on a separator
      switch (nID)
      {
	  case IDC_EANSAVE :
          strTipText.Format("speichern");
		  break;
      default :
		  return FALSE;
   }
   if (pNMHDR->code == TTN_NEEDTEXTA)
      lstrcpyn(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
   else
      _mbstowcsz(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
   *pResult = 0;

   return TRUE;    // message was handled
}

void CEan::OnEnKillfocusEanNr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
//	TestEan ();
}
