#include "stdafx.h"
#include "a_pr.h"

struct A_PR a_pr, a_pr_null;

void APR_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &a_pr.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_pr.a, SQLDOUBLE, 0);
    sqlout ((double *) &a_pr.a,SQLDOUBLE,0);
    sqlout ((short *) &a_pr.akt,SQLSHORT,0);
    sqlout ((short *) &a_pr.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_pr.fil,SQLSHORT,0);
    sqlout ((short *) &a_pr.fil_gr,SQLSHORT,0);
    sqlout ((double *) &a_pr.key_typ_dec13,SQLDOUBLE,0);
    sqlout ((short *) &a_pr.key_typ_sint,SQLSHORT,0);
    sqlout ((TCHAR *) a_pr.lad_akv,SQLCHAR,2);
    sqlout ((TCHAR *) a_pr.lief_akv,SQLCHAR,2);
    sqlout ((short *) &a_pr.mdn,SQLSHORT,0);
    sqlout ((short *) &a_pr.mdn_gr,SQLSHORT,0);
    sqlout ((TCHAR *) a_pr.modif,SQLCHAR,2);
    sqlout ((double *) &a_pr.pr_ek,SQLDOUBLE,0);
    sqlout ((double *) &a_pr.pr_vk,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &a_pr.bearb,SQLDATE,0);
    sqlout ((TCHAR *) a_pr.pers_nam,SQLCHAR,9);
    sqlout ((double *) &a_pr.pr_vk1,SQLDOUBLE,0);
    sqlout ((double *) &a_pr.pr_vk2,SQLDOUBLE,0);
    sqlout ((double *) &a_pr.pr_vk3,SQLDOUBLE,0);
    sqlout ((double *) &a_pr.pr_ek_euro,SQLDOUBLE,0);
    sqlout ((double *) &a_pr.pr_vk_euro,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select a_pr.a,  a_pr.akt,  ")
_T("a_pr.delstatus,  a_pr.fil,  a_pr.fil_gr,  a_pr.key_typ_dec13,  ")
_T("a_pr.key_typ_sint,  a_pr.lad_akv,  a_pr.lief_akv,  a_pr.mdn,  ")
_T("a_pr.mdn_gr,  a_pr.modif,  a_pr.pr_ek,  a_pr.pr_vk,  a_pr.bearb,  ")
_T("a_pr.pers_nam,  a_pr.pr_vk1,  a_pr.pr_vk2,  a_pr.pr_vk3,  ")
_T("a_pr.pr_ek_euro,  a_pr.pr_vk_euro from a_pr ")

#line 16 "a_pr.rpp"
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ?"));
    sqlin ((double *) &a_pr.a,SQLDOUBLE,0);
    sqlin ((short *) &a_pr.akt,SQLSHORT,0);
    sqlin ((short *) &a_pr.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_pr.fil,SQLSHORT,0);
    sqlin ((short *) &a_pr.fil_gr,SQLSHORT,0);
    sqlin ((double *) &a_pr.key_typ_dec13,SQLDOUBLE,0);
    sqlin ((short *) &a_pr.key_typ_sint,SQLSHORT,0);
    sqlin ((TCHAR *) a_pr.lad_akv,SQLCHAR,2);
    sqlin ((TCHAR *) a_pr.lief_akv,SQLCHAR,2);
    sqlin ((short *) &a_pr.mdn,SQLSHORT,0);
    sqlin ((short *) &a_pr.mdn_gr,SQLSHORT,0);
    sqlin ((TCHAR *) a_pr.modif,SQLCHAR,2);
    sqlin ((double *) &a_pr.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &a_pr.pr_vk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_pr.bearb,SQLDATE,0);
    sqlin ((TCHAR *) a_pr.pers_nam,SQLCHAR,9);
    sqlin ((double *) &a_pr.pr_vk1,SQLDOUBLE,0);
    sqlin ((double *) &a_pr.pr_vk2,SQLDOUBLE,0);
    sqlin ((double *) &a_pr.pr_vk3,SQLDOUBLE,0);
    sqlin ((double *) &a_pr.pr_ek_euro,SQLDOUBLE,0);
    sqlin ((double *) &a_pr.pr_vk_euro,SQLDOUBLE,0);
            sqltext = _T("update a_pr set a_pr.a = ?,  ")
_T("a_pr.akt = ?,  a_pr.delstatus = ?,  a_pr.fil = ?,  a_pr.fil_gr = ?,  ")
_T("a_pr.key_typ_dec13 = ?,  a_pr.key_typ_sint = ?,  a_pr.lad_akv = ?,  ")
_T("a_pr.lief_akv = ?,  a_pr.mdn = ?,  a_pr.mdn_gr = ?,  a_pr.modif = ?,  ")
_T("a_pr.pr_ek = ?,  a_pr.pr_vk = ?,  a_pr.bearb = ?,  a_pr.pers_nam = ?,  ")
_T("a_pr.pr_vk1 = ?,  a_pr.pr_vk2 = ?,  a_pr.pr_vk3 = ?,  ")
_T("a_pr.pr_ek_euro = ?,  a_pr.pr_vk_euro = ? ")

#line 22 "a_pr.rpp"
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ?");
            sqlin ((short *)   &a_pr.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_pr.a, SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &a_pr.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_pr.a, SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_pr ")
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ?"));
            sqlin ((short *)   &a_pr.mdn_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.fil_gr, SQLSHORT, 0);
            sqlin ((short *)   &a_pr.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_pr.a, SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_pr ")
                                  _T("where mdn_gr = ? ")
				  _T("and mdn = ?")
				  _T("and fil_gr = ?")
				  _T("and fil = ?")
				  _T("and a = ?"));
    sqlin ((double *) &a_pr.a,SQLDOUBLE,0);
    sqlin ((short *) &a_pr.akt,SQLSHORT,0);
    sqlin ((short *) &a_pr.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_pr.fil,SQLSHORT,0);
    sqlin ((short *) &a_pr.fil_gr,SQLSHORT,0);
    sqlin ((double *) &a_pr.key_typ_dec13,SQLDOUBLE,0);
    sqlin ((short *) &a_pr.key_typ_sint,SQLSHORT,0);
    sqlin ((TCHAR *) a_pr.lad_akv,SQLCHAR,2);
    sqlin ((TCHAR *) a_pr.lief_akv,SQLCHAR,2);
    sqlin ((short *) &a_pr.mdn,SQLSHORT,0);
    sqlin ((short *) &a_pr.mdn_gr,SQLSHORT,0);
    sqlin ((TCHAR *) a_pr.modif,SQLCHAR,2);
    sqlin ((double *) &a_pr.pr_ek,SQLDOUBLE,0);
    sqlin ((double *) &a_pr.pr_vk,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_pr.bearb,SQLDATE,0);
    sqlin ((TCHAR *) a_pr.pers_nam,SQLCHAR,9);
    sqlin ((double *) &a_pr.pr_vk1,SQLDOUBLE,0);
    sqlin ((double *) &a_pr.pr_vk2,SQLDOUBLE,0);
    sqlin ((double *) &a_pr.pr_vk3,SQLDOUBLE,0);
    sqlin ((double *) &a_pr.pr_ek_euro,SQLDOUBLE,0);
    sqlin ((double *) &a_pr.pr_vk_euro,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into a_pr (a,  ")
_T("akt,  delstatus,  fil,  fil_gr,  key_typ_dec13,  key_typ_sint,  lad_akv,  ")
_T("lief_akv,  mdn,  mdn_gr,  modif,  pr_ek,  pr_vk,  bearb,  pers_nam,  pr_vk1,  pr_vk2,  ")
_T("pr_vk3,  pr_ek_euro,  pr_vk_euro) ")

#line 57 "a_pr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 59 "a_pr.rpp"
}

BOOL APR_CLASS::operator== (A_PR& a_pr)
{
            if (this->a_pr.mdn_gr != a_pr.mdn_gr) return FALSE;  
            if (this->a_pr.mdn    != a_pr.mdn) return FALSE;  
            if (this->a_pr.fil_gr != a_pr.fil_gr) return FALSE;  
            if (this->a_pr.fil    != a_pr.fil) return FALSE;  
            if (this->a_pr.a      != a_pr.a) return FALSE;  
            return TRUE;
} 
