#include "StdAfx.h"
#include ".\delrec.h"
#include "Token.h"


CDelTable::CDelTable () : DB_CLASS ()
{
	TestCursor = -1;
	DeleteCursor = -1;
	Name = "";
	Where = "";
	Kritical = No; 
	Output = NULL;
}

CDelTable::CDelTable (CString& TableInfo, CString& Where) : DB_CLASS () 
{
	Kritical = No; 
	TestCursor = -1;
	DeleteCursor = -1;
	CToken t (TableInfo, _T("$"));
	if (t.GetAnzToken () == 0) return;
    this->Name = t.GetToken (0);
	this->Name.Trim ();
	if (t.GetAnzToken () > 1)
	{
		CString K = t.GetToken (1);
		K.Trim ();
		if (K == _T("L"))
		{
			Kritical = No;
		}
		else if (K == _T("K"))
		{
			Kritical = Break;
		}
		else
		{
			Kritical = Break;
		}
	}
	this->Where = Where;
	Output = NULL;
}

CDelTable::CDelTable (CString& Name, CString& Where, int Kritical) : DB_CLASS () 
{
	TestCursor = -1;
	DeleteCursor = -1;
	this->Name = Name;
	this->Where = Where;
	this->Kritical = Kritical; 
	Output = NULL;
}

CDelTable::~CDelTable ()
{
	if (TestCursor != -1)
	{
		sqlclose (TestCursor);
	}
	if (DeleteCursor != -1)
	{
		sqlclose (DeleteCursor);
	}
}

BOOL CDelTable::Test ()
{
	if (Name == "") return FALSE;
	if (Kritical == No) return TRUE;
	if (Kritical == No) return TRUE;
	if (TestCursor == -1)
	{
		CString Command;
        Command.Format (_T("select * from %s"), Name);
		if (Where != "")
		{
			Command += " ";
			Command += Where;
		}
		TestCursor = sqlcursor (Command.GetBuffer ());
	}

	if (TestCursor == -1) return FALSE;
	sqlopen (TestCursor);
	BOOL ret = TRUE;
	if (sqlfetch (TestCursor) == 0)
	{
		if (Output != NULL)
		{
			CString Line;
			Line.Format (_T("Eintrag in Tabelle %s"), Name);
			Output->AddString (Line.GetBuffer ());
			Line = _T("L�schen wird abgebrochen");
			Output->AddString (Line.GetBuffer ());
		}
		ret = FALSE;
	}
	sqlclose (TestCursor);
	TestCursor = -1;
	return ret;
}

BOOL CDelTable::Delete ()
{
	if (Name == "") return FALSE;
	if (Test () == FALSE) return FALSE;

	if (DeleteCursor == -1)
	{
		CString Command;
        Command.Format (_T("delete from %s"), Name);
		if (Where != "")
		{
			Command += " ";
			Command += Where;
		}
		DeleteCursor = sqlcursor (Command.GetBuffer ());
	}

	if (DeleteCursor == -1) return FALSE;
	if (Output != NULL)
	{
			CString Line;
			Line.Format (_T("Tabelle %s"), Name);
			Output->AddString (Line.GetBuffer ());
	}
	int dsqlstatus = sqlexecute (DeleteCursor);
	sqlclose (DeleteCursor);
	DeleteCursor = -1;
	if (dsqlstatus != 0) return FALSE;
	return TRUE;
}


CDelSection::CDelSection ()
{
	Output = NULL;
}

CDelSection::~CDelSection ()
{
}

void CDelSection::Init ()
{
	Fields.clear ();
	Tables.clear ();
}

void CDelSection::SetFields (CString& FieldString)
{
	Fields.clear ();
	CToken t (FieldString, _T(","));
	LPTSTR p;
	while ((p = t.NextToken ()) != NULL)
	{
		CString Field = p;
		Fields.push_back (Field);
	}
}

void CDelSection::AddTable (CString& Table)
{
	Tables.push_back (Table);
}

BOOL CDelSection::Delete (CString& Where)
{
    BOOL ret = TRUE;
	if (Output != NULL)
	{
		Output->AddString (_T("Pr�fen der Tabellen"));
	}
	for (std::vector<CString>::iterator c = Tables.begin (); c != Tables.end ();
		                            ++ c)
	{
		CString TableInfo = *c;
        CDelTable DelTable (TableInfo, Where);
		DelTable.Output = Output;
		ret = DelTable.Test ();
		if (ret == FALSE) break;
	}
	if (!ret) return ret;

	if (Output != NULL)
	{
		Output->AddString (_T("Tabellen k�nnen gel�scht werden"));
	}
	CDelTable DBase;
	for (std::vector<CString>::iterator c = Tables.begin (); c != Tables.end ();
		                            ++ c)
	{
		CString TableInfo = *c;
        CDelTable DelTable (TableInfo, Where);
		DelTable.Output = Output;
		ret = DelTable.Delete ();
		if (ret == FALSE) break;
	}
	return ret;
}

CDelRec::CDelRec(void)
{
	FileName = "";
	Output = NULL;
}

CDelRec::CDelRec(CString& FielName)
{
	this->FileName = FileName;
	Output = NULL;
}

CDelRec::~CDelRec(void)
{
	for (std::vector<CDelSection *>::iterator d = Sections.begin (); d != Sections.end ();
		                            ++ d)
	{
		CDelSection *ds = *d;
		delete ds;
	}
	Sections.clear ();
}

BOOL CDelRec::Load ()
{
	if (FileName == _T("")) return FALSE;

	CString DelFile = _T("");
	DelFile.GetEnvironmentVariable (_T("bws"));
	if (DelFile != _T(""))
	{
		DelFile += _T("\\etc\\");
	}
	DelFile += FileName;

	TCHAR record[256];
	FILE *fp = _tfopen (DelFile.GetBuffer (), _T("r"));
	if (fp == NULL) return FALSE;

	CDelSection *ds = NULL;
	BOOL ret = TRUE;
	while (_fgetts (record, sizeof (record) - 1, fp) != NULL)
	{
		CString Record = record;
		Record.Trim ();
		if (Record.Find (_T ("SEKTION")) != -1)
		{
			if (_fgetts (record, sizeof (record) - 1, fp) == NULL) break;
			CString Record = record;
			Record.Trim ();
			int pos = Record.Find (_T("$"));
			if (pos != -1)
			{
				Record = Record.Left (pos - 1);
				Record.Trim ();
			}
			ds = new CDelSection ();
			if (ds == NULL) 
			{
				ret = FALSE;
				break;
			}
			ds->Output = Output;
			ds->SetFields (Record);
			Sections.push_back (ds);
		}
		else
		{
			if (ds != NULL)
			{
				ds->AddTable (Record);
			}
		}
	}
	fclose (fp);
    return ret;
}

BOOL CDelRec::Run (CString& Where)
{
	if (Output != NULL)
	{
		CString Line;
		Line.Format (_T("L�schen von Tabellen : %s"), Where);
		Output->AddString (Line.GetBuffer ());
	}
	BOOL ret = TRUE;
	for (std::vector<CDelSection *>::iterator d = Sections.begin (); d != Sections.end ();
		                                      ++d)
	{
		CDelSection *ds = *d;
		ret = ds->Delete (Where);
		if (!ret) break;
	}
	return ret;
}