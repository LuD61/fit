#include "stdafx.h"
#include "a_lgr.h"

struct A_LGR a_lgr, a_lgr_null;

void A_LGR_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &a_lgr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_lgr.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_lgr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &a_lgr.lgr, SQLLONG, 0);
            sqlin ((short *)   &a_lgr.lgr_pla_kz, SQLSHORT, 0);
    sqlout ((double *) &a_lgr.a,SQLDOUBLE,0);
    sqlout ((double *) &a_lgr.a_lgr_kap,SQLDOUBLE,0);
    sqlout ((short *) &a_lgr.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_lgr.fil,SQLSHORT,0);
    sqlout ((long *) &a_lgr.lgr,SQLLONG,0);
    sqlout ((short *) &a_lgr.lgr_pla_kz,SQLSHORT,0);
    sqlout ((short *) &a_lgr.lgr_typ,SQLSHORT,0);
    sqlout ((short *) &a_lgr.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) a_lgr.haupt_lgr,SQLCHAR,2);
    sqlout ((TCHAR *) a_lgr.lgr_platz,SQLCHAR,11);
    sqlout ((double *) &a_lgr.min_bestand,SQLDOUBLE,0);
    sqlout ((double *) &a_lgr.meld_bestand,SQLDOUBLE,0);
    sqlout ((double *) &a_lgr.hoechst_bestand,SQLDOUBLE,0);
    sqlout ((long *) &a_lgr.inh_wanne,SQLLONG,0);
    sqlout ((double *) &a_lgr.buch_artikel,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select a_lgr.a,  ")
_T("a_lgr.a_lgr_kap,  a_lgr.delstatus,  a_lgr.fil,  a_lgr.lgr,  ")
_T("a_lgr.lgr_pla_kz,  a_lgr.lgr_typ,  a_lgr.mdn,  a_lgr.haupt_lgr,  ")
_T("a_lgr.lgr_platz,  a_lgr.min_bestand,  a_lgr.meld_bestand,  ")
_T("a_lgr.hoechst_bestand,  a_lgr.inh_wanne,  a_lgr.buch_artikel from a_lgr ")

#line 16 "a_lgr.rpp"
                                  _T("where mdn = ? ")
				  _T("and fil = ? ")
				  _T("and a = ? ")
				  _T("and lgr = ? ")
				  _T("and lgr_pla_kz = ?"));
    sqlin ((double *) &a_lgr.a,SQLDOUBLE,0);
    sqlin ((double *) &a_lgr.a_lgr_kap,SQLDOUBLE,0);
    sqlin ((short *) &a_lgr.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_lgr.fil,SQLSHORT,0);
    sqlin ((long *) &a_lgr.lgr,SQLLONG,0);
    sqlin ((short *) &a_lgr.lgr_pla_kz,SQLSHORT,0);
    sqlin ((short *) &a_lgr.lgr_typ,SQLSHORT,0);
    sqlin ((short *) &a_lgr.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) a_lgr.haupt_lgr,SQLCHAR,2);
    sqlin ((TCHAR *) a_lgr.lgr_platz,SQLCHAR,11);
    sqlin ((double *) &a_lgr.min_bestand,SQLDOUBLE,0);
    sqlin ((double *) &a_lgr.meld_bestand,SQLDOUBLE,0);
    sqlin ((double *) &a_lgr.hoechst_bestand,SQLDOUBLE,0);
    sqlin ((long *) &a_lgr.inh_wanne,SQLLONG,0);
    sqlin ((double *) &a_lgr.buch_artikel,SQLDOUBLE,0);
            sqltext = _T("update a_lgr set a_lgr.a = ?,  ")
_T("a_lgr.a_lgr_kap = ?,  a_lgr.delstatus = ?,  a_lgr.fil = ?,  ")
_T("a_lgr.lgr = ?,  a_lgr.lgr_pla_kz = ?,  a_lgr.lgr_typ = ?,  ")
_T("a_lgr.mdn = ?,  a_lgr.haupt_lgr = ?,  a_lgr.lgr_platz = ?,  ")
_T("a_lgr.min_bestand = ?,  a_lgr.meld_bestand = ?,  ")
_T("a_lgr.hoechst_bestand = ?,  a_lgr.inh_wanne = ?,  ")
_T("a_lgr.buch_artikel = ? ")

#line 22 "a_lgr.rpp"
                                  _T("where mdn = ? ")
				  _T("and fil = ? ")
				  _T("and a = ? ")
				  _T("and lgr = ? ")
				  _T("and lgr_pla_kz = ?");
            sqlin ((short *)   &a_lgr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_lgr.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_lgr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &a_lgr.lgr, SQLLONG, 0);
            sqlin ((short *)   &a_lgr.lgr_pla_kz, SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &a_lgr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_lgr.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_lgr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &a_lgr.lgr, SQLLONG, 0);
            sqlin ((short *)   &a_lgr.lgr_pla_kz, SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_lgr ")
                                  _T("where mdn = ? ")
				  _T("and fil = ? ")
				  _T("and a = ? ")
				  _T("and lgr = ? ")
				  _T("and lgr_pla_kz = ?"));
            sqlin ((short *)   &a_lgr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &a_lgr.fil, SQLSHORT, 0);
            sqlin ((double *)  &a_lgr.a, SQLDOUBLE, 0);
            sqlin ((long *)    &a_lgr.lgr, SQLLONG, 0);
            sqlin ((short *)   &a_lgr.lgr_pla_kz, SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from a_lgr ")
                                  _T("where mdn = ? ")
				  _T("and fil = ? ")
				  _T("and a = ? ")
				  _T("and lgr = ? ")
				  _T("and lgr_pla_kz = ?"));
    sqlin ((double *) &a_lgr.a,SQLDOUBLE,0);
    sqlin ((double *) &a_lgr.a_lgr_kap,SQLDOUBLE,0);
    sqlin ((short *) &a_lgr.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_lgr.fil,SQLSHORT,0);
    sqlin ((long *) &a_lgr.lgr,SQLLONG,0);
    sqlin ((short *) &a_lgr.lgr_pla_kz,SQLSHORT,0);
    sqlin ((short *) &a_lgr.lgr_typ,SQLSHORT,0);
    sqlin ((short *) &a_lgr.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) a_lgr.haupt_lgr,SQLCHAR,2);
    sqlin ((TCHAR *) a_lgr.lgr_platz,SQLCHAR,11);
    sqlin ((double *) &a_lgr.min_bestand,SQLDOUBLE,0);
    sqlin ((double *) &a_lgr.meld_bestand,SQLDOUBLE,0);
    sqlin ((double *) &a_lgr.hoechst_bestand,SQLDOUBLE,0);
    sqlin ((long *) &a_lgr.inh_wanne,SQLLONG,0);
    sqlin ((double *) &a_lgr.buch_artikel,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into a_lgr (a,  ")
_T("a_lgr_kap,  delstatus,  fil,  lgr,  lgr_pla_kz,  lgr_typ,  mdn,  haupt_lgr,  ")
_T("lgr_platz,  min_bestand,  meld_bestand,  hoechst_bestand,  inh_wanne,  ")
_T("buch_artikel) ")

#line 57 "a_lgr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?)")); 

#line 59 "a_lgr.rpp"
}

BOOL A_LGR_CLASS::operator== (A_LGR& a_lgr)
{
            return TRUE;
} 
