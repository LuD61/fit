#pragma once
#include "NumEdit.h"
#include "TextEdit.h"
#include "a_bas.h"
#include "a_pr.h"
#include "a_kalk_eig.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "Ptabn.h"

// CSpannDialog-Dialogfeld

class CSpannDialog : public CDialog
{
	DECLARE_DYNAMIC(CSpannDialog)

public:
	CSpannDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CSpannDialog();

// Dialogfelddaten
	enum { IDD = IDD_SPANNE };

protected:
	BOOL visible;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	CFont Font;
	CFormTab Form;
	CCtrlGrid CtrlGrid;

public:
	enum
	{
		Aufschlag = 1,
		AufschlagPlus = 2,
		Abschlag = 3,
		AbschlagPlus = 4,
	};

	int MarktSpPar;
	double mwst;
	double SpEkIst;
	double SpVkIst;

	int StdCellHeight;
	A_BAS_CLASS *A_bas;
	A_KALK_EIG_CLASS *A_kalk_eig;
	APR_CLASS *A_pr;
	PTABN_CLASS Ptabn;

	CStatic m_LMatoB;
	CStatic m_LKost;
	CStatic m_LHkVollk;
	CStatic m_LSpanne;
	CStatic m_LSk;
	CStatic m_LFilEk;
	CStatic m_LVk;
	CStatic m_LKalk;
	CStatic m_LIstPr;
	CStatic m_LIstSp;

	CNumEdit m_MatoB;
	CNumEdit m_Kost;
	CNumEdit m_HkVollk;

	CNumEdit m_PSk;
	CNumEdit m_PFilEk;
	CNumEdit m_PVk;

	CNumEdit m_Sk;
	CNumEdit m_FilEk;
	CNumEdit m_Vk;

	CNumEdit m_PrEk;
	CNumEdit m_APrVk;

	CNumEdit m_SpEkIst;
	CNumEdit m_SpVkIst;

	void Calculate ();
	void Show ();
	void Get ();
	void SetVisible (BOOL visible);
};
