#pragma once
#include "afxwin.h"
#include "FormTab.h"
#include "AngebotCore.h"
#include "Datatables.h"
#include "TextEdit.h"


// CCommentDialog-Dialogfeld

class CCommentDialog : public CDialog
{
	DECLARE_DYNAMIC(CCommentDialog)

public:
	CCommentDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CCommentDialog();

// Dialogfelddaten
	enum { IDD = IDD_COMMENTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

private:
	CFormTab Form;

public:
	CStatic m_LCommAngebot;
public:
	CTextEdit m_CommAngebot;
public:
	CStatic m_LCommRechnung;
public:
	CTextEdit m_CommRechnung;
public:
	CStatic m_LCommIntern;
public:
	CTextEdit m_CommIntern;
public:
	afx_msg void OnOK ();
	afx_msg void OnCancel ();
};
