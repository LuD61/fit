#ifndef REZTEXTE_DEF
#define REZTEXTE_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct ATEXTE {
   long           sys;
   short          waa;
   long           txt_nr;
   TCHAR          txt[1025];
   TCHAR          disp_txt[1025];
   short          alignment;
   short          send_ctrl;
   short          txt_typ;
   short          txt_platz;
   double         a;
};
extern struct ATEXTE atexte, atexte_null;

#line 8 "atext.rh"

class ATEXTE_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
               int acursor; 
       public :
               ATEXTE atexte;  
               ATEXTE_CLASS () : DB_CLASS ()
               {
      			acursor = -1;
               }
               int dbreadfirsta ();
               int dbreada (); 
};
#endif
