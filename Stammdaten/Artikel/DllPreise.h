#ifndef _DLLPREISE_DEF
#define _DLLPREISE_DEF
#include <windows.h>

class CDllPreise
{
public:
	static HANDLE PriceLib;
    int (*preise_holen)(short dmdn, short dfil, 
		                        short dkun_fil, 
						        int dkun, double da, char *ddatum, 
						        short *sa, double *pr_ek, double *pr_vk);

    int (*fetch_preis_tag) (short mgruppe,
							short mandant,
							short fgruppe,
							short filiale,
							double artikel,
							char   *datum,
							double *ek_pr,
							double *vk_pr);

	int (*fetch_preis_lad) (short mgruppe,
                           short mandant,
                           short fgruppe,
                           short filiale,
                           double artikel,
                           double *prek,
                           double *prvk);

	void (*mitfilbelosa) ();

	void (*ohnefilbelosa) ();

	int (*SetPriceDbName) (char *);

	CDllPreise ();
};

#endif
