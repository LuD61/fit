#ifndef _TIER_DEF
#define _TIER_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct TIER {
   short          tier;
   TCHAR          tier_bz[21];
   short          tier_gatt;
   short          delstatus;
   double         pr_neu;
   double         pr_alt;
};
extern struct TIER tier, tier_null;

#line 8 "Tier.rh"

class TIER_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               TIER tier;
               TIER_CLASS () : DB_CLASS ()
               {
               }
};
#endif
