#pragma once

class CTxtSg
{
public:
	CFont *PcFont;
	CString DevFontName;
	int row;
	int col;
	int size;
	int attribute;
	CTxtSg(void);
	CTxtSg(CFont* PcFont, int row, int col, int size);
	~CTxtSg(void);
	BOOL operator== (CPoint& p);
	BOOL equals (int row, int col);
};
