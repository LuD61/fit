#include "StdAfx.h"
#include ".\txtsg.h"

CTxtSg::CTxtSg(void)
{
	this->PcFont = NULL;
	this->row = 0;
	this->col = 0;
	this->size = 9;
}

CTxtSg::CTxtSg(CFont* PcFont, int row, int col, int size)
{
	this->PcFont = PcFont;
	this->row = row;
	this->col = col;
	this->size = size;
}

CTxtSg::~CTxtSg(void)
{
	if (PcFont != NULL)
	{
		delete PcFont;
	}
}

BOOL CTxtSg::equals (int row, int col)
{
	if (this->row == row &&
		this->col == col)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL CTxtSg::operator== (CPoint& p)
{
	return equals (p.y, p.x);
}