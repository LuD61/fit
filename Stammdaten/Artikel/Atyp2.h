#pragma once
#include "A_bas.h"
#include "afxwin.h"
#include <vector>


// CAtyp2-Dialogfeld

class CAtyp2 : public CDialog
{
	DECLARE_DYNAMIC(CAtyp2)

public:
	CAtyp2(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CAtyp2();

// Dialogfelddaten
	enum { IDD = IDD_ATYP2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	void FillCombo ();

	DECLARE_MESSAGE_MAP()
public:
	A_BAS_CLASS *A_bas;
public:
	int a_typ_pos;
	BOOL Add0;
	CComboBox m_Atyp2;
	std::vector<CString *> *ATyp2Combo;
public:
	afx_msg void OnCbnSelchangeAtyp2();
};
