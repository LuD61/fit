// ProductPass.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "ProductPass.h"
#include "Process.h"
#include "StrFuncs.h"
#include "Token.h"
#include "PrdpCopy.h"

static BOOL ShopAnbindung = FALSE;

// CProductPass-Dialogfeld

IMPLEMENT_DYNAMIC(CProductPass, CDbPropertyPage)

CProductPass::CProductPass()
	: CDbPropertyPage(CProductPass::IDD)
{
	tabx = 0;
	taby = 0;
	Page6Removed = FALSE;

	dlg.DlgBkColor = DlgBkColor;
	PROG_CFG Cfg ("Artikel");
    char cfg_v [256];
    if (Cfg.GetCfgValue ("ShopAnbindung", cfg_v) == TRUE)
    {
			ShopAnbindung = atoi (cfg_v);
    }

}

CProductPass::~CProductPass()
{
}

void CProductPass::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CProductPass, CDbPropertyPage)
	ON_WM_SIZE ()
END_MESSAGE_MAP()

BOOL CProductPass::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	dlg.Construct (_T(""), this);
	Page1.Construct (IDD_PRODUCT_PAGE1);
	Page1.Frame = Frame;
	Page1.Update = Update;
	Page1.HideButtons = TRUE;
	dlg.AddPage (&Page1);

	Page2.Construct (IDD_PRODUCT_PAGE2);
	Page2.Frame = Frame;
	Page2.Update = Update;
	Page2.HideButtons = TRUE;
	dlg.AddPage (&Page2);

	Page3.Construct (IDD_PRODUCT_PAGE3);
	Page3.Frame = Frame;
	Page3.Update = Update;
	Page3.HideButtons = TRUE;
	dlg.AddPage (&Page3);

	
	Page5.Construct (IDD_HKST_INFO);
	Page5.Frame = Frame;
	Page5.Update = Update;
	Page5.HideButtons = TRUE;
	dlg.AddPage (&Page5);

	Page4.Construct (IDD_PRODUCT_PAGE4);
	Page4.Frame = Frame;
	Page4.Update = Update;
	Page4.HideButtons = TRUE;
	dlg.AddPage (&Page4);


	if (ShopAnbindung == TRUE)
	{
		Page6.Construct (IDD_PRODUCT_PAGE_SHOP);
		Page6.Frame = Frame;
		Page6.Update = Update;
		Page6.HideButtons = TRUE;
		dlg.AddPage (&Page6);
		Page6Removed = FALSE;
	}


    dlg.m_psh.pszCaption = _T("Simple");
    dlg.m_psh.nStartPage = 0;

    dlg.m_psh.dwSize = sizeof (dlg.m_psh);
	dlg.Create (this, WS_CHILD | WS_VISIBLE);
	dlg.SetActivePage (&Page1);
    Page1.SetFocus (); 


/*
	CRect cRect;
    dlg.GetClientRect (&cRect);
    dlg.MoveWindow (0, 0, cRect.right, cRect.bottom);
	CSize Size = GetTotalSize ();
	if (Size.cy < (cRect.bottom) + 10)
	{
		Size.cy = cRect.bottom + 10;
	}
	if (Size.cx < (cRect.right) + 10)
	{
		Size.cx = cRect.right + 10;
	}
	SetScrollSizes (MM_TEXT, Size);
	CRect pRect;
	CRect pcRect;
	CWnd *ChildFrame = GetParent ();
	CWnd *MainFrame = ChildFrame->GetParent ();
	ChildFrame->GetWindowRect (&pRect);
	MainFrame->ScreenToClient (&pRect);
	MainFrame->GetClientRect (&pcRect);
	BOOL Resize = FALSE;
	if (pcRect.right > pRect.left + cRect.right + CXPLUS)
	{
		pRect.right  = pRect.left + cRect.right + CXPLUS;
		Resize = TRUE;
	}
	if (pcRect.bottom > pRect.top + cRect.bottom + CYPLUS)
	{
		pRect.bottom = pRect.top + cRect.bottom + CYPLUS;
		Resize = TRUE;
	}
	if (Resize)
	{
		GetParent ()->MoveWindow (&pRect, TRUE);
	}
*/

	return TRUE;
}

HBRUSH CProductPass::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDbPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CProductPass::OnSize(UINT nType, int cx, int cy)
{
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	if (!IsWindow (dlg.m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	dlg.GetWindowRect (&pRect);
	dlg.GetParent ()->ScreenToClient (&pRect);
	int pcx = pRect.right -pRect.left - tabx;
	int pcy = pRect.bottom - pRect.top - taby;
	if (cx > pcx)
	{
		pcx = cx;
	}
	if (cy > pcy)
	{
		pcy = cy;
	}
	dlg.MoveWindow (tabx, taby, pcx, pcy);
	CTabCtrl *tab = dlg.GetTabControl ();
	tab->MoveWindow (tabx, taby, pcx, pcy);
	Page1.Frame = this;
	Page1.GetWindowRect (&pRect);
	dlg.ScreenToClient (&pRect);

	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = pcx - 10 - tabx;
	frame.bottom = pcy - 10 - taby;
	frame.top += 20;
	frame.left += 2;
    frame.right -= 10;
	frame.bottom -= 10;

	int page = dlg.GetActiveIndex ();
	if (page == 0)
	{
		Page1.MoveWindow (&frame);
	}
/*
	else if (page == 1)
	{
		Page2.MoveWindow (&frame);
	}
	else if (page == 2)
	{
		Page3.MoveWindow (&frame);
	}
*/
	else if (page == 4)
	{
		Page4.MoveWindow (&frame);
	}
}

// CProductPass-Meldungshandler

void CProductPass::SetBasis (CBasisdatenPage *Basis)
{
	this->Basis = Basis;
	Page1.Basis = Basis;
	Page2.Basis = Basis;
	Page3.Basis = Basis;
	Page4.Basis = Basis;
	Page5.Basis = Basis;
	Page6.Basis = Basis;
}

void CProductPass::UpdatePage ()
{
	CDbPropertyPage *p = (CDbPropertyPage *) dlg.GetActivePage ();
	if (p != NULL) 
	{
		p->UpdatePage ();
	}
}
void CProductPass::GetPage ()
{
	CDbPropertyPage *p = (CDbPropertyPage *) dlg.GetActivePage ();
	if (p != NULL) 
	{
		p->OnKillActive ();
	}
}

BOOL CProductPass::OnSetActive ()
{
	CMenu *menu = NULL;
//	dlg.SetActivePage (0);
	if (Page6Removed == FALSE)
	{
		/*
		if (strcmp((LPSTR) &Basis->A_bas.a_bas.shop_kz, "J") != 0)
		{
			dlg.RemovePage (&Page6); //FS-161
			Page6Removed = TRUE; //FS-161
		}
		*/
	}

	if (Update != NULL) 
	{
		menu = Update->GetFrameMenu ();
		if (::IsMenu (menu->m_hMenu))
		{
			menu->InsertMenu (ID_EDIT_COPY, MF_BYCOMMAND, ID_PRODUKTPASS_COPY, _T("Propduktpass kopieren"));
			menu->InsertMenu (ID_EDIT_COPY, MF_BYCOMMAND, ID_PRODUKTPASS_INSERT, _T("Propduktpass einfügen"));

//			CView *parent = (CView *) GetParent ()->GetParent ();
//			CDocument *doc = parent->GetDocument ();
//			doc->UpdateAllViews (parent);
		}
	}
	CDbPropertyPage *p = (CDbPropertyPage *) dlg.GetActivePage ();
	if (p != NULL) 
	{
		p->OnSetActive ();
		p->UpdatePage ();
	}
	return TRUE;
}

void CProductPass::PrdkCopy ()
{
	if (Basis != NULL &&
		Basis->A_bas.a_bas.a > 0.0)
	{
		CString Text;
		Text.Format (_T("Prdp=%.0lf"), Basis->A_bas.a_bas.a);
		CStrFuncs::ToClipboard (Text, CF_TEXT);
		Update->Write ();
	}
}

void CProductPass::PrdkInsert ()
{
	if (Basis != NULL &&
		Basis->A_bas.a_bas.a > 0.0)
	{
		CString Text;
		double a = 0.0;
		Text.Format (_T("Prdp=%.0lf"), Basis->A_bas.a_bas.a);
		CStrFuncs::FromClipboard (Text, CF_TEXT);
		CToken t (Text, _T("="));
		if (t.GetAnzToken () > 1)
		{
			a = CStrFuncs::StrToDouble (t.GetToken (1));
		}
		if (a == 0.0)
		{
			return;
		}

		if (a == Basis->A_bas.a_bas.a)
		{
			return;
		}

		Text.Format (_T("Produktpass von Artikel %.0lf kopieren ?\n")
			         _T("Vorhandene Produktpassdaten werden überschrieben."), a);
		if (MessageBox (Text.GetBuffer (), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
					 IDNO)
		{
			return;
		}

		CPrdpCopy Copy;
		Copy.Run (&Basis->A_bas, &Basis->A_bas_erw, a, 3);
//		Copy.Run (&Basis->A_bas, &Basis->A_bas_erw, a, Basis->ZutSource);
		for (int i = 0; i < dlg.GetPageCount (); i ++)
		{
			CDbPropertyPage *p = (CDbPropertyPage *) dlg.GetPage (i);
			if (IsWindow (p->m_hWnd))
			{
				p->UpdatePage ();
			}
		}
	}
}

BOOL CProductPass::OnKillActive ()
{
	CMenu *menu = NULL;
	if (Page6Removed == TRUE)
	{
		/***
		if (strcmp((LPSTR) &Basis->A_bas.a_bas.shop_kz, "J") == 0)
		{
			dlg.AddPage (&Page6); //FS-161
			Page6Removed = FALSE; //FS-161
		}
		***/
	}

	if (Update != NULL)
	{
		menu = Update->GetFrameMenu ();
		if (::IsMenu (menu->m_hMenu))
		{
			menu->DeleteMenu (ID_PRODUKTPASS_COPY, MF_BYCOMMAND);
			menu->DeleteMenu (ID_PRODUKTPASS_INSERT, MF_BYCOMMAND);
		}
	}
	CDbPropertyPage *p = (CDbPropertyPage *) dlg.GetActivePage ();
	if (p != NULL) 
	{
		p->OnKillActive ();
	}
	return TRUE;
}


void CProductPass::OnDelete ()
{
}

BOOL CProductPass::Print ()
{
/*
	CProcess print;
	CString Command = "dr70001 -name prodpass";
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
*/
	CProcess print;
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\prodpass.llf", tmp);
	}
	else
	{
		dName = "prodpass.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME prodpass\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "a %.0lf %.0lf\n", Basis->A_bas.a_bas.a,
			                            Basis->A_bas.a_bas.a);
		fclose (fp);
//		Command.Format ("dr70001 -name prodpass -datei %s", dName.GetBuffer ());
		Command.Format ("prodpass.bat");
	}
	else
	{
		Command = "dr70001 -name prodpass";
	}

	if (tmp != NULL)
	{
		dName.Format ("%s\\prodpass2.llf", tmp);
	}
	else
	{
		dName = "prodpass2.llf";
	}
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME prodpass2\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "a %.0lf %.0lf\n", Basis->A_bas.a_bas.a,
			                            Basis->A_bas.a_bas.a);
		fclose (fp);
	}
	if (tmp != NULL)
	{
		dName.Format ("%s\\prodpass3.llf", tmp);
	}
	else
	{
		dName = "prodpass3.llf";
	}
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME prodpass3\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "a %.0lf %.0lf\n", Basis->A_bas.a_bas.a,
			                            Basis->A_bas.a_bas.a);
		fclose (fp);
	}

	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
//	HANDLE pid = print.Start (SW_HIDE);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CProductPass::OnCopy ()
{
	CDbPropertyPage *p = (CDbPropertyPage *) dlg.GetActivePage ();
	if (p != NULL) 
	{
		p->OnCopy ();
	}
}

void CProductPass::OnPaste ()
{
	CDbPropertyPage *p = (CDbPropertyPage *) dlg.GetActivePage ();
	if (p != NULL) 
	{
		p->OnPaste ();
	}
}
