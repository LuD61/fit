#include "StdAfx.h"
#include "EanList.h"

CEanList::CEanList(void)
{
	ean = 0.0;
	ean_bz = _T("");
    a = 0.0; 
	a_bz1 = _T("");
}

CEanList::CEanList(double ean, LPTSTR ean_bz, double a, LPTSTR a_bz1)
{
	this->ean    = ean;
	this->ean_bz = ean_bz;
	this->a      = a;
	this->a_bz1  = a_bz1;
}

CEanList::~CEanList(void)
{
}
