#pragma once
#include "a_bas.h"
#include "formtab.h"
#include "NumEdit.h"
#include "StaticButton.h"
#include "UstKtoDialog.h"


// CKtoDialog-Dialogfeld

class CKtoDialog : public CDialog
{
	DECLARE_DYNAMIC(CKtoDialog)

public:
	CKtoDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CKtoDialog();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

public:
	A_BAS_CLASS A_bas;
	CFormTab Form;
	CNumEdit m_WeKto1;
	CNumEdit m_WeKto2;
	CNumEdit m_WeKto3;
	CNumEdit m_ErlKto1;
	CNumEdit m_ErlKto2;
	CNumEdit m_ErlKto3;
	CStaticButton m_WeKto;

	virtual void OnOK ();
	afx_msg void OnWeKto ();
};
