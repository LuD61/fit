#include "stdafx.h"
#include "a_leer.h"

struct A_LEER a_leer, a_leer_null, a_leer_def;

void A_LEER_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_leer.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_leer.a,SQLDOUBLE,0);
    sqlout ((long *) &a_leer.a_krz,SQLLONG,0);
    sqlout ((double *) &a_leer.a_pfa,SQLDOUBLE,0);
    sqlout ((short *) &a_leer.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_leer.fil,SQLSHORT,0);
    sqlout ((short *) &a_leer.mdn,SQLSHORT,0);
    sqlout ((short *) &a_leer.sg1,SQLSHORT,0);
    sqlout ((short *) &a_leer.sg2,SQLSHORT,0);
    sqlout ((TCHAR *) a_leer.verk_art,SQLCHAR,2);
    sqlout ((TCHAR *) a_leer.mwst_ueb,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &a_leer.verk_beg,SQLDATE,0);
    sqlout ((double *) &a_leer.br_gew,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select a_leer.a,  ")
_T("a_leer.a_krz,  a_leer.a_pfa,  a_leer.delstatus,  a_leer.fil,  a_leer.mdn,  ")
_T("a_leer.sg1,  a_leer.sg2,  a_leer.verk_art,  a_leer.mwst_ueb,  ")
_T("a_leer.verk_beg,  a_leer.br_gew from a_leer ")

#line 12 "a_leer.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_leer.a,SQLDOUBLE,0);
    sqlin ((long *) &a_leer.a_krz,SQLLONG,0);
    sqlin ((double *) &a_leer.a_pfa,SQLDOUBLE,0);
    sqlin ((short *) &a_leer.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_leer.fil,SQLSHORT,0);
    sqlin ((short *) &a_leer.mdn,SQLSHORT,0);
    sqlin ((short *) &a_leer.sg1,SQLSHORT,0);
    sqlin ((short *) &a_leer.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) a_leer.verk_art,SQLCHAR,2);
    sqlin ((TCHAR *) a_leer.mwst_ueb,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_leer.verk_beg,SQLDATE,0);
    sqlin ((double *) &a_leer.br_gew,SQLDOUBLE,0);
            sqltext = _T("update a_leer set a_leer.a = ?,  ")
_T("a_leer.a_krz = ?,  a_leer.a_pfa = ?,  a_leer.delstatus = ?,  ")
_T("a_leer.fil = ?,  a_leer.mdn = ?,  a_leer.sg1 = ?,  a_leer.sg2 = ?,  ")
_T("a_leer.verk_art = ?,  a_leer.mwst_ueb = ?,  a_leer.verk_beg = ?,  ")
_T("a_leer.br_gew = ? ")

#line 14 "a_leer.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_leer.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_leer.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_leer ")
                                  _T("where a = ? for update"));
            sqlin ((double *)   &a_leer.a,  SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_leer ")
                                  _T("set delstatus = 0 where a = ? and delstatus = 0 for update"));
            sqlin ((double *)   &a_leer.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_leer ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_leer.a,SQLDOUBLE,0);
    sqlin ((long *) &a_leer.a_krz,SQLLONG,0);
    sqlin ((double *) &a_leer.a_pfa,SQLDOUBLE,0);
    sqlin ((short *) &a_leer.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_leer.fil,SQLSHORT,0);
    sqlin ((short *) &a_leer.mdn,SQLSHORT,0);
    sqlin ((short *) &a_leer.sg1,SQLSHORT,0);
    sqlin ((short *) &a_leer.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) a_leer.verk_art,SQLCHAR,2);
    sqlin ((TCHAR *) a_leer.mwst_ueb,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_leer.verk_beg,SQLDATE,0);
    sqlin ((double *) &a_leer.br_gew,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into a_leer (")
_T("a,  a_krz,  a_pfa,  delstatus,  fil,  mdn,  sg1,  sg2,  verk_art,  mwst_ueb,  verk_beg,  ")
_T("br_gew) ")

#line 28 "a_leer.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?)"));

#line 30 "a_leer.rpp"
}
