#include "StdAfx.h"
#include ".\etikett.h"
#include "DbUniCode.h"
#include "StrFuncs.h"
#include "Process.h"

CEtikett::CEtikett(void)
{
	Mode = Off;
	Type = No;
	ErfKz = _T("S");
	A_bas = NULL;
	A_hndw = NULL;
	A_eig = NULL;
	MdnCursor = -1;
	FilCursor = -1;
	InsertPrCursor = -1;
	DeleteA_hndw = FALSE;
	DeleteA_eig = FALSE;
	TestCompany = FALSE;
	GewAuto = 0.3;
	EtiType = 0;
	Anzahl = 0;
	EtikettTyp = _T("RegEti");
	testmode = 0;
	CString env;
	if (env.GetEnvironmentVariable (_T("testmode")))
	{    
		testmode = atoi (env.GetBuffer());
    }
}

CEtikett::~CEtikett(void)
{
	if (MdnCursor != -1)
	{
		Mdn.sqlclose (MdnCursor);
		MdnCursor = -1;
	}
	if (FilCursor != -1)
	{
		Fil.sqlclose (FilCursor);
		FilCursor = -1;
	}
	if (InsertPrCursor != -1)
	{
		Reg_eti.sqlclose (InsertPrCursor);
		InsertPrCursor = -1;
	}
	if (DeleteA_hndw) 
	{
		delete A_hndw;
	}
	if (DeleteA_eig) 
	{
		delete A_eig;
	}
}

void CEtikett::Prepare ()
{
	if (MdnCursor != -1) return;
	Mdn.sqlout ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);
	MdnCursor = Mdn.sqlcursor (_T("select mdn from mdn where mdn > 0"));
	Fil.sqlout ((short *) &Fil.fil.fil, SQLSHORT, 0);
	Mdn.sqlin ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);
	FilCursor = Fil.sqlcursor (_T("select fil from fil where mdn = ? and fil > 0"));
	Smt.A_bas = A_bas;
	if (A_hndw == NULL)
	{
		A_hndw = new A_HNDW_CLASS ();
		DeleteA_hndw = TRUE;
	}
	if (A_eig == NULL)
	{
		A_eig = new A_EIG_CLASS ();
		DeleteA_eig = TRUE;
	}
	strcpy ((LPSTR) sys_par.sys_par_nam, "gew_auto_par");
    int dsqlstatus = Sys_par.dbreadfirst ();
	if (dsqlstatus == 0 && atoi ((LPSTR) sys_par.sys_par_wrt) != 0)
	{
		CDbUniCode::DbToUniCode (sys_par.sys_par_besch, (LPSTR) sys_par.sys_par_besch);
		GewAuto = CStrFuncs::StrToDouble (sys_par.sys_par_besch);
	}
}

BOOL CEtikett::Write ()
{
	TestCompany = FALSE;
	EtiType = 0;
	if (Mode == Off) return TRUE;
	Prepare ();
	if (Mode > 1)
	{
		EtiTab.clear ();
	}
	if (A_bas->a_bas.a_typ == Hndw)
	{
		A_hndw->dbreadfirst ();
		if (_tstoi (A_hndw->a_hndw.reg_eti) == No) return TRUE;
		if (A_hndw->a_hndw.anz_reg_eti == 0) return TRUE;
	}
	else if (A_bas->a_bas.a_typ == Eig)
	{
		A_eig->dbreadfirst ();
		if (_tstoi (A_eig->a_eig.reg_eti) == No) return TRUE;
		if (A_eig->a_eig.anz_reg_eti == 0) return TRUE;
		_tcscpy (A_hndw->a_hndw.reg_eti, A_eig->a_eig.reg_eti);
		A_hndw->a_hndw.anz_reg_eti = A_eig->a_eig.anz_reg_eti;
	}

	memcpy (&Reg_eti.reg_eti, &reg_eti_null, sizeof (REG_ETI));
	Reg_eti.reg_eti.a = A_bas->a_bas.a;
	A_bas->commitwork ();
	AutoNr.GenAutoNr (0, 0, _T("reg_eti"));
	A_bas->beginwork ();
	Reg_eti.reg_eti.nr = AutoNr.Nr;
	_tcscpy (Reg_eti.reg_eti.erf_kz, ErfKz.GetBuffer ());
	CDbUniCode::DbFromUniCode (Reg_eti.reg_eti.erf_kz, (LPSTR) Reg_eti.reg_eti.erf_kz, sizeof (Reg_eti.reg_eti.erf_kz));
	DB_CLASS::Sysdate (Sysdate);
	DB_CLASS::ToDbDate (Sysdate, &Reg_eti.reg_eti.dat);
	DB_CLASS::ToDbDate (Sysdate, &Reg_eti.reg_eti.dat_akv);
	if (A_bas->a_bas.a_typ == Hndw || A_bas->a_bas.a_typ == Eig)
	{
		Reg_eti.reg_eti.reg = _tstoi (A_hndw->a_hndw.reg_eti);
		if (Anzahl > 0)
		{
			Reg_eti.reg_eti.anz = Anzahl;
		}
		else
		{
			Reg_eti.reg_eti.anz = A_hndw->a_hndw.anz_reg_eti;
		}
		if (Reg_eti.reg_eti.anz == 0) Reg_eti.reg_eti.anz = 1;
		WriteMdn ();
	}
    return TRUE;
}

BOOL CEtikett::WritePr (short mdn, short fil, double pr_vk)
{
	TestCompany = TRUE;
	EtiType = 0;
	StartMdn = mdn;
	StartFil = fil;
	if (Mode == Off) return TRUE;
	Prepare ();
	if (A_bas->a_bas.a_typ == Hndw)
	{
		A_hndw->a_hndw.a = A_bas->a_bas.a;
		A_hndw->dbreadfirst ();
		if (_tstoi (A_hndw->a_hndw.reg_eti) == No) return TRUE;
		if (A_hndw->a_hndw.anz_reg_eti == 0) return TRUE;
	}
	else if (A_bas->a_bas.a_typ == Eig)
	{
		A_eig->a_eig.a = A_bas->a_bas.a;
		A_eig->dbreadfirst ();
		if (_tstoi (A_eig->a_eig.reg_eti) == No) return TRUE;
		if (A_eig->a_eig.anz_reg_eti == 0) return TRUE;
		_tcscpy (A_hndw->a_hndw.reg_eti, A_eig->a_eig.reg_eti);
		A_hndw->a_hndw.anz_reg_eti = A_eig->a_eig.anz_reg_eti;
	}

	memcpy (&Reg_eti.reg_eti, &reg_eti_null, sizeof (REG_ETI));
	Reg_eti.reg_eti.a = A_bas->a_bas.a;
//	A_bas->commitwork ();
	AutoNr.GenAutoNr (0, 0, _T("reg_eti"));
//	A_bas->beginwork ();
	Reg_eti.reg_eti.nr = AutoNr.Nr;
	_tcscpy (Reg_eti.reg_eti.erf_kz, ErfKz.GetBuffer ());
	CDbUniCode::DbFromUniCode (Reg_eti.reg_eti.erf_kz, (LPSTR) Reg_eti.reg_eti.erf_kz, sizeof (Reg_eti.reg_eti.erf_kz));
	DB_CLASS::Sysdate (Sysdate);
	DB_CLASS::ToDbDate (Sysdate, &Reg_eti.reg_eti.dat);
	DB_CLASS::ToDbDate (Sysdate, &Reg_eti.reg_eti.dat_akv);
	if (A_bas->a_bas.a_typ == Hndw || A_bas->a_bas.a_typ == Eig)
	{
		Reg_eti.reg_eti.reg = _tstoi (A_hndw->a_hndw.reg_eti);
		if (Anzahl > 0)
		{
			Reg_eti.reg_eti.anz = Anzahl;
		}
		else
		{
			Reg_eti.reg_eti.anz = A_hndw->a_hndw.anz_reg_eti;
		}
		if (Reg_eti.reg_eti.anz == 0) Reg_eti.reg_eti.anz = 1;
		if (mdn == 0)
		{
			WriteMdn ();
		}
		else if (fil == 0)
		{
			Mdn.mdn.mdn = mdn;
			WriteFil ();
		}
		else
		{
			Mdn.mdn.mdn = mdn;
            Fil.fil.fil = fil;
			if (!Smt.TestSmt (Mdn.mdn.mdn, Fil.fil.fil)) return FALSE;
			if (!Smt.TestPrSmt (Mdn.mdn.mdn, Fil.fil.fil)) return FALSE;
			Reg_eti.reg_eti.mdn = Mdn.mdn.mdn;
			Reg_eti.reg_eti.fil = Fil.fil.fil;
			Reg_eti.reg_eti.vk_pr = pr_vk;
			Reg_eti.dbupdate ();
			if (Mode > 1)
			{
				EtiTab.push_back (Reg_eti.reg_eti);
			}
		}
	}
    return TRUE;
}

void CEtikett::WriteMdn ()
{
	Mdn.sqlopen (MdnCursor);
	while (Mdn.sqlfetch (MdnCursor) == 0)
	{
		WriteFil ();
	}
}

void CEtikett::WriteFil ()
{
	double pr_ek = 0.0;
	double pr_vk = 0.0;
	int sa;

	Mdn.dbreadfirst ();

	if (TestCompany && StartMdn == 0)
	{
		A_pr.a_pr.a = A_bas->a_bas.a;
		A_pr.a_pr.mdn_gr = Mdn.mdn.mdn_gr;
		A_pr.a_pr.mdn    = Mdn.mdn.mdn;
		A_pr.a_pr.fil_gr = 0;
		A_pr.a_pr.fil    = 0;
		if (A_pr.dbreadfirst () == 0) return;
	}
	Fil.sqlopen (FilCursor);
	while (Fil.sqlfetch (FilCursor) == 0)
	{
		if (!Smt.TestSmt (Mdn.mdn.mdn, Fil.fil.fil)) continue;
		if (!Smt.TestPrSmt (Mdn.mdn.mdn, Fil.fil.fil)) continue;
		Reg_eti.reg_eti.mdn = Mdn.mdn.mdn;
		Reg_eti.reg_eti.fil = Fil.fil.fil;
		if (DllPreise.PriceLib != NULL && DllPreise.fetch_preis_tag != NULL)
		{
			Fil.fil.mdn = Mdn.mdn.mdn;
			Fil.dbreadfirst ();
			if (TestCompany && StartFil == 0)
			{
				A_pr.a_pr.mdn_gr = Mdn.mdn.mdn_gr;
				A_pr.a_pr.mdn    = Mdn.mdn.mdn;
				A_pr.a_pr.fil_gr = Fil.fil.fil_gr;
				A_pr.a_pr.fil    = Fil.fil.fil;
				A_pr.a_pr.a = A_bas->a_bas.a;
				if (A_pr.dbreadfirst () == 0) continue;
			}
			sa = (*DllPreise.fetch_preis_tag) (Mdn.mdn.mdn_gr, Mdn.mdn.mdn, 
				                               Fil.fil.fil_gr, Fil.fil.fil,
											   A_bas->a_bas.a, Sysdate.GetBuffer (),
											   &pr_ek, &pr_vk);
			Reg_eti.reg_eti.vk_pr = pr_vk;
		}
		Reg_eti.dbupdate ();

		if (Mode > 1)
		{
			EtiTab.push_back (Reg_eti.reg_eti);
		}
	}
}

void CEtikett::Clear ()
{
	if (Mode > 1)
	{
		EtiTab.clear ();
	}
}

void CEtikett::TestPrint ()
{
	if (EtiTab.size () > 0)
	{
		if (Mode == AskForPrint)
		{
			INT_PTR ret = AfxMessageBox (_T("Etikett ausdrucken ?"),
				                            MB_YESNO | MB_ICONQUESTION, 0);
			if (ret == IDYES)
			{
				RunPrint ();
			}
		}

		else if (Mode == Print)
		{
			RunPrint ();
		}
	}
}

void CEtikett::RunPrint ()
{
	extern short sql_mode; 
	short sql_s;
    int dsqlstatus = 0;
	int count = 0;
	int reg = 0;

	if (InsertPrCursor == -1)
	{
		Reg_eti.sqlin ((short *) &reg_eti_pr.mdn, SQLSHORT, 0);
		Reg_eti.sqlin ((short *) &reg_eti_pr.fil, SQLSHORT, 0);
		Reg_eti.sqlin ((double *) &reg_eti_pr.a, SQLDOUBLE, 0);
		Reg_eti.sqlin ((short *) &reg_eti_pr.reg, SQLSHORT, 0);
		Reg_eti.sqlin ((double *) &reg_eti_pr.vk_pr, SQLDOUBLE, 0);
		Reg_eti.sqlin ((double *) &vk_gr, SQLDOUBLE, 0);
		Reg_eti.sqlin ((short *)  &vk_kz, SQLSHORT, 0);
		InsertPrCursor = Reg_eti.sqlcursor (_T("insert into reg_eti_pr ")
											_T("(mdn, fil, a, reg, vk_pr, vk_gr, vk_kz) ")
											_T("values ")
											_T("(?, ?, ?, ?, ?, ?, ?)"));
			CString Info; Info.Format (_T("reg_eti_pr InsertPrCursor=%d"), InsertPrCursor);
			if (testmode) AfxMessageBox (Info.GetBuffer(), MB_OK | MB_ICONINFORMATION, 0);


	}
    sql_s = sql_mode;
	sql_mode = 1;
	do
	{
		dsqlstatus = Reg_eti.sqlcomm (_T("delete from reg_eti_pr"));
		if (dsqlstatus == 0 || dsqlstatus == 100) break;
		Sleep (100);
		if (count == 50)
		{
			AfxMessageBox (_T("Etiketten k�nnen nicht gedruckt werden"),
				           MB_OK | MB_ICONERROR, 0);
			sql_mode = sql_s;
			return;
		}
		count ++;
	}
	while (dsqlstatus != 0);

	SortEtiTab ();
	int size = (int) EtiTab.size ();
	for (int i = 0; i < size; i ++)
	{
		vk_gr = 0.0;
		vk_kz = 0;
		memcpy (&reg_eti_pr, &EtiTab[i], sizeof (REG_ETI));

		if (reg == 0)
		{
			reg = reg_eti_pr.reg;
		}
/*
		if (reg != reg_eti_pr.reg)
		{
			RunLL (reg);
			reg = reg_eti_pr.reg;
			CString Message;
			_stprintf (Ptabn.ptabn.ptwert, _T("%hd"), reg);
			CDbUniCode::DbFromUniCode (Ptabn.ptabn.ptwert, (LPSTR) Ptabn.ptabn.ptwert); 
			_tcscpy (Ptabn.ptabn.ptitem, _T("reg_eti"));
			CDbUniCode::DbFromUniCode (Ptabn.ptabn.ptitem, (LPSTR) Ptabn.ptabn.ptitem);
			_tcscpy (Ptabn.ptabn.ptbez, Ptabn.ptabn.ptwert);
			Ptabn.dbreadfirst ();
			Message.Format (_T("Das Etikettenformat hat auf Format %s gewechsel\n")
				            _T("Bitte neues Etikett einlegen")), ptabn.ptbez);
		}
*/
		A_bas->a_bas.a = reg_eti_pr.a;
		if (A_bas->dbreadfirst () != 0) continue;
		if (A_bas->a_bas.me_einh == 2 && reg_eti_pr.vk_pr >= 1000.0)
		{
			vk_gr = reg_eti_pr.vk_pr / 10;
			vk_kz = 1;
		}
		if (A_bas->a_bas.me_einh != 2)
		{
			if (A_bas->a_bas.vk_gr == Auto)
			{
				vk_gr = reg_eti_pr.vk_pr / A_bas->a_bas.a_gew;
				if (A_bas->a_bas.a_gew < GewAuto)
				{
					vk_gr /= 10.0;
					vk_kz = 1;
				}
			}
			else if (A_bas->a_bas.vk_gr == Pr100g)
			{
				vk_gr = reg_eti_pr.vk_pr / A_bas->a_bas.a_gew / 10.0;
				vk_kz = 1;
			}
			else if (A_bas->a_bas.vk_gr == PrKg)
			{
				vk_gr = reg_eti_pr.vk_pr / A_bas->a_bas.a_gew;
			}
		}
		for (int j = 0; j < reg_eti_pr.anz; j ++)
		{
			Reg_eti.sqlexecute (InsertPrCursor);
		}
	}
    sql_mode = sql_s;

	CProcess Process;
	CString Command;
//	Command.Format (_T("dr70001 -name regeti%hd -l"), reg);
	Command.Format (_T("dr70001 -name %s%hd -l"), EtikettTyp.GetBuffer(),reg);
	Process.SetCommand (Command);
	HANDLE Pid = Process.Start ();
}

void CEtikett::RunPrintWithPr (CString& Date)
{
	extern short sql_mode; 
	short sql_s;
	double pr_ek = 0.0;
	double pr_vk = 0.0;
	int sa;
    int dsqlstatus = 0;
	int count = 0;

	if (InsertPrCursor == -1)
	{
		Reg_eti.sqlin ((short *) &reg_eti_pr.mdn, SQLSHORT, 0);
		Reg_eti.sqlin ((short *) &reg_eti_pr.fil, SQLSHORT, 0);
		Reg_eti.sqlin ((double *) &reg_eti_pr.a, SQLDOUBLE, 0);
		Reg_eti.sqlin ((short *) &reg_eti_pr.reg, SQLSHORT, 0);
		Reg_eti.sqlin ((double *) &reg_eti_pr.vk_pr, SQLDOUBLE, 0);
		Reg_eti.sqlin ((double *) &vk_gr, SQLDOUBLE, 0);
		Reg_eti.sqlin ((short *)  &vk_kz, SQLSHORT, 0);
		InsertPrCursor = Reg_eti.sqlcursor (_T("insert into reg_eti_pr ")
											_T("(mdn, fil, a, reg, vk_pr, vk_gr, vk_kz) ")
											_T("values ")
											_T("(?, ?, ?, ?, ?, ?, ?)"));

			CString Info; Info.Format (_T("reg_eti_pr InsertPrCursor=%d"), InsertPrCursor);
			if (testmode) AfxMessageBox (Info.GetBuffer(), MB_OK | MB_ICONINFORMATION, 0);
	}
    sql_s = sql_mode;
	sql_mode = 1;
	do
	{
		dsqlstatus = Reg_eti.sqlcomm (_T("delete from reg_eti_pr"));
		if (dsqlstatus == 0 || dsqlstatus == 100) break;
		Sleep (100);
		if (count == 50)
		{
			AfxMessageBox (_T("Etiketten k�nnen nicht gedruckt werden"),
				           MB_OK | MB_ICONERROR, 0);
			sql_mode = sql_s;
			return;
		}
		count ++;
	}
	while (dsqlstatus != 0);

	if (reg_eti_pr.anz == 0)
	{
			AfxMessageBox (_T("Etikettenanzahl ist 0 !"),
				           MB_OK | MB_ICONERROR, 0);
	}

// Hier:Etikett  reg_eti_pr best�cken 
	int size = (int) EtiTab.size ();
	double a_gew = A_bas->a_bas.a_gew; 
	if (A_bas->a_bas.a_gew == 0.0) a_gew = 1.0;
	for (int i = 0; i < size; i ++)
	{
		if (testmode) AfxMessageBox (_T("reg_eti_pr schreiben"), MB_OK | MB_ICONINFORMATION, 0);
		memcpy (&reg_eti_pr, &EtiTab[i], sizeof (REG_ETI));
		for (int j = 0; j < reg_eti_pr.anz; j ++)
		{
			if (EtiType != 0 && reg_eti_pr.reg != EtiType) continue;
			if (testmode) AfxMessageBox (_T("2reg_eti_pr schreiben"), MB_OK | MB_ICONINFORMATION, 0);
			Mdn.mdn.mdn = reg_eti_pr.mdn;
			Fil.fil.mdn = reg_eti_pr.mdn;
			Fil.fil.fil = reg_eti_pr.fil;
			Mdn.dbreadfirst ();
			Fil.dbreadfirst ();
			if (DllPreise.PriceLib != NULL && DllPreise.fetch_preis_tag != NULL)
			{
				sa = (*DllPreise.fetch_preis_tag) (Mdn.mdn.mdn_gr, reg_eti_pr.mdn, 
				                               Fil.fil.fil_gr, reg_eti_pr.fil,
											   reg_eti_pr.a, Date.GetBuffer (),
											   &pr_ek, &pr_vk);
				if (pr_vk != 0.0)
				{
					reg_eti_pr.vk_pr = pr_vk;
				}
			}
			vk_gr = 0.0;
			vk_kz = 0;
			A_bas->a_bas.a = reg_eti_pr.a;
			if (A_bas->dbreadfirst () != 0) continue;
			if (A_bas->a_bas.me_einh == 2 && reg_eti_pr.vk_pr >= 1000.0)
			{
				vk_gr = reg_eti_pr.vk_pr / 10;
				vk_kz = 1;
			}
			if (A_bas->a_bas.me_einh != 2)
			{
				if (A_bas->a_bas.vk_gr == Auto)
				{
					vk_gr = reg_eti_pr.vk_pr / a_gew;
					if (A_bas->a_bas.a_gew < GewAuto)
					{
						vk_gr /= 10.0;
						vk_kz = 1;
					}
				}
				else if (A_bas->a_bas.vk_gr == Pr100g)
				{
					vk_gr = reg_eti_pr.vk_pr / a_gew / 10.0;
					vk_kz = 1;
				}
				else if (A_bas->a_bas.vk_gr == PrKg)
				{
					vk_gr = reg_eti_pr.vk_pr / a_gew;
				}
			}
			CString Info; 
			Info.Format (_T("reg_eti_pr.mdn=%d"), reg_eti_pr.mdn); if (testmode) AfxMessageBox (Info.GetBuffer(), MB_OK | MB_ICONINFORMATION, 0);
			Info.Format (_T("reg_eti_pr.fil=%d"), reg_eti_pr.fil); if (testmode) AfxMessageBox (Info.GetBuffer(), MB_OK | MB_ICONINFORMATION, 0);
			Info.Format (_T("reg_eti_pr.a=%.2lf"), reg_eti_pr.a); if (testmode) AfxMessageBox (Info.GetBuffer(), MB_OK | MB_ICONINFORMATION, 0);
			Info.Format (_T("reg_eti_pr.reg=%d"), reg_eti_pr.reg); if (testmode) AfxMessageBox (Info.GetBuffer(), MB_OK | MB_ICONINFORMATION, 0);
			Info.Format (_T("reg_eti_pr.vk_pr=%.2lf"), reg_eti_pr.vk_pr); if (testmode) AfxMessageBox (Info.GetBuffer(), MB_OK | MB_ICONINFORMATION, 0);
			Info.Format (_T("reg_eti_pr.vk_gr=%.2lf"), vk_gr); if (testmode) AfxMessageBox (Info.GetBuffer(), MB_OK | MB_ICONINFORMATION, 0);
			Info.Format (_T("reg_eti_pr.vk_kz=%d"), vk_kz); if (testmode) AfxMessageBox (Info.GetBuffer(), MB_OK | MB_ICONINFORMATION, 0);

			int dsqlstatus = Reg_eti.sqlexecute (InsertPrCursor);
			Info.Format (_T("reg_eti_pr insert stat=%d"), dsqlstatus); if (testmode) AfxMessageBox (Info.GetBuffer(), MB_OK | MB_ICONINFORMATION, 0);

		}
	}
    sql_mode = sql_s;

	CProcess Process;
	CString Command;
//	Command.Format (_T("dr70001 -name regeti%hd -l"), EtiType);
	Command.Format (_T("dr70001 -name %s%hd -l"), EtikettTyp.GetBuffer(),EtiType);
	Process.SetCommand (Command);
	HANDLE Pid = Process.Start ();
}

void CEtikett::RePrint (CString& Where, CString& Date)
{
	BOOL DeleteAbas = FALSE;

	if (A_bas == NULL)
	{
		A_bas = new A_BAS_CLASS ();
		A_hndw = new A_HNDW_CLASS ();
		A_eig = new A_EIG_CLASS ();
		DeleteAbas = TRUE;
	}

	Reg_eti.sqlout ((short *) &Reg_eti.reg_eti.mdn, SQLSHORT, 0);
	Reg_eti.sqlout ((short *) &Reg_eti.reg_eti.fil, SQLSHORT, 0);
	Reg_eti.sqlout ((long *)  &Reg_eti.reg_eti.nr,  SQLLONG, 0);
	CString Sql = _T("select mdn, fil, nr from reg_eti ");
	Sql += Where;
	EtiTab.clear ();
	int cursor = Reg_eti.sqlcursor (Sql.GetBuffer ());
	while (Reg_eti.sqlfetch (cursor) == 0)
	{
		Reg_eti.dbreadfirst ();
		EtiTab.push_back (Reg_eti.reg_eti);
	}
	Reg_eti.sqlclose (cursor);
	RunPrintWithPr (Date);
	if (DeleteAbas)
	{
		delete A_bas;
		A_bas = NULL;
		delete A_hndw;
		A_hndw = NULL;
		delete A_eig;
		A_eig = NULL;
	}
}

void CEtikett::RunPrintFromStm (CString& Where, CString& Date, CString& Typ, int Anz )
{
	int cursor;
	BOOL DeleteAbas = FALSE;
	EtiTab.clear ();
	EtikettTyp = Typ;
	Anzahl = Anz;
	if (A_bas == NULL)
	{
		A_bas = new A_BAS_CLASS ();
		A_hndw = new A_HNDW_CLASS ();
		A_eig = new A_EIG_CLASS ();
		DeleteAbas = TRUE;
	}

	Smt.A_bas = A_bas;
	Prepare ();
	CString Sql;
	Sql = _T("select * from a_bas ");
	Sql += Where;
	A_bas->sqlout ((double *) &A_bas->a_bas.a, SQLDOUBLE, 0);
	cursor = A_bas->sqlcursor (Sql.GetBuffer ());

	while (A_bas->sqlfetch (cursor) == 0)
	{
		A_bas->dbreadfirst ();
		Reg_eti.reg_eti.a = A_bas->a_bas.a;
		if (A_bas->a_bas.a_typ == Hndw)
		{
			A_hndw->a_hndw.a = A_bas->a_bas.a;
			A_hndw->dbreadfirst ();
			if (_tstoi (A_hndw->a_hndw.reg_eti) == No) continue;
		}
		else if (A_bas->a_bas.a_typ == Eig)
		{
			A_eig->a_eig.a = A_bas->a_bas.a;
			A_eig->dbreadfirst ();
			if (_tstoi (A_eig->a_eig.reg_eti) == No) continue;
			if (A_eig->a_eig.anz_reg_eti == 0) continue;
			_tcscpy (A_hndw->a_hndw.reg_eti,  A_eig->a_eig.reg_eti);
			A_hndw->a_hndw.anz_reg_eti = A_eig->a_eig.anz_reg_eti;
		}
		else
		{
			continue;
		}

		StmWriteMdn ();   //Hier:Etikett Reg_eti  (EtiTab) wird erzeugt mit pr�fung smt_zuord und a_pr)
	}

	SortEtiTab ();
	RunPrintWithPr (Date);
    A_bas->sqlclose (cursor);
	if (DeleteAbas)
	{
		delete A_bas;
		A_bas = NULL;
		delete A_hndw;
		A_hndw = NULL;
		delete A_eig;
		A_eig = NULL;
	}

}

void CEtikett::StmWriteMdn ()
{
	Mdn.sqlopen (MdnCursor);
	while (Mdn.sqlfetch (MdnCursor) == 0)
	{
		StmWriteFil ();
	}
}

void CEtikett::StmWriteFil ()
{
	if (testmode) AfxMessageBox ("Aufruf: StmWriteFil");

	Mdn.dbreadfirst ();

	Fil.sqlopen (FilCursor);
	while (Fil.sqlfetch (FilCursor) == 0)
	{
		if (!Smt.TestSmt (Mdn.mdn.mdn, Fil.fil.fil)) continue;
		if (!Smt.TestPrSmt (Mdn.mdn.mdn, Fil.fil.fil)) continue;
		Reg_eti.reg_eti.mdn = Mdn.mdn.mdn;
		Reg_eti.reg_eti.fil = Fil.fil.fil;
		Reg_eti.reg_eti.reg = _tstoi (A_hndw->a_hndw.reg_eti);
		if (Anzahl > 0)
		{
			Reg_eti.reg_eti.anz = Anzahl;
		}
		else
		{
			Reg_eti.reg_eti.anz = A_hndw->a_hndw.anz_reg_eti;
		}
		Reg_eti.reg_eti.vk_pr = 0.0;
		EtiTab.push_back (Reg_eti.reg_eti);
		CString Info;
		Info.Format ("StmWriteFil EtiTab.push_back Anz=%d",Reg_eti.reg_eti.anz) ;
		if (testmode) AfxMessageBox (Info.GetBuffer());
	}
}

void CEtikett::SortEtiTab ()
{
	REG_ETI *tab;
	int size = (int) EtiTab.size ();
	tab = new REG_ETI [size];
	for (int i = 0; i < size; i ++)
	{
		memcpy (&tab[i], &EtiTab[i], sizeof (REG_ETI));
	}
	qsort (tab, size, sizeof (REG_ETI), Compare);
	for (int i = 0; i < size; i ++)
	{
		memcpy (&EtiTab[i], &tab[i], sizeof (REG_ETI));
	}
	delete tab;
}

int _cdecl CEtikett::Compare (const void *elem1, const void *elem2)
{
	REG_ETI *eti1 = (REG_ETI *) elem1;
	REG_ETI *eti2 = (REG_ETI *) elem2;

	return (eti1->reg - eti2->reg);
}
