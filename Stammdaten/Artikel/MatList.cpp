#include "StdAfx.h"
#include "MatList.h"

CMatList::CMatList(void)
{
	mat = 0l;
	a = 0.0; 
	a_bz1 = _T("");
}

CMatList::CMatList(long mat, double a, LPTSTR a_bz1)
{
	this->mat    = mat;
	this->a      = a;
	this->a_bz1  = a_bz1;
}

CMatList::~CMatList(void)
{
}
