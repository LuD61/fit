#pragma once
#include "afxwin.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "DbUniCode.h"
#include "FormTab.h"
#include "StaticButton.h"
#include "Mdn.h"
#include "Fil.h"
#include "Adr.h"
#include "A_bas.h"
#include "A_ust_kto.h"
#include "Ptabn.h"
#include "ChoiceMdn.h"
#include "ChoiceFil.h"

#define IDC_UST_SAVE 3010
#define IDC_UST_DELETE 3011

#ifndef IDC_MDNCHOICE
#define IDC_MDNCHOICE 3001
#endif

#ifndef IDC_FILCHOICE
#define IDC_FILCHOICE 3002
#endif

// CUstKtoDialog-Dialogfeld

class CUstKtoDialog : public CDialog
{
	DECLARE_DYNAMIC(CUstKtoDialog)

public:
	CUstKtoDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CUstKtoDialog();

// Dialogfelddaten
	enum { IDD = IDD_UST_KTO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
private:
    MDN_CLASS Mdn;
    FIL_CLASS Fil;
	ADR_CLASS MdnAdr;
	ADR_CLASS FilAdr;
    A_UST_KTO_CLASS A_ust_kto;
	PTABN_CLASS Ptabn;

public:
    A_BAS_CLASS *A_bas;

	CStaticButton m_Save;
	CStaticButton m_Delete;

	CStatic m_UstKtoGroup;

	CChoiceMdn *ChoiceMdn;
	CChoiceFil *ChoiceFil;

	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	CEdit m_MdnName;

	CStatic m_LFil;
	CNumEdit m_Fil;
	CButton m_FilChoice;
	CEdit m_FilName;

	CStatic m_LMwst;
	CComboBox m_Mwst;

	CStatic m_LWeKto;
	CNumEdit m_WeKto;

	CStatic m_LErlKto;
	CNumEdit m_ErlKto;

	CStatic m_LSktoKto;
	CNumEdit m_SktoKto;

	CFormTab Form;
	CFormTab Keys;
	CFont Font;
	CCtrlGrid CtrlGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid FilGrid;
	CCtrlGrid ToolGrid;
	virtual BOOL OnReturn ();
	virtual void OnSave ();
	virtual void Delete ();
	BOOL ReadMdn ();
	BOOL ReadFil ();
	BOOL Read ();
    void OnMdnchoice(); 
    void OnFilchoice(); 
   void FillCombo (LPTSTR Item, CWnd *control);

};
