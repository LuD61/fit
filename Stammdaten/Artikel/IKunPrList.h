#ifndef _IKUN_PR_LIST_DEF
#define _IKUN_PR_LIST_DEF
#pragma once

class CIKunPrList
{
public:
	short mdn;
	long kun_pr;
	CString zus_bz;
	CIKunPrList(void);
	CIKunPrList(short, long, LPTSTR);
	~CIKunPrList(void);
};
#endif
