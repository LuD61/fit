#pragma once
#include "vector"

class CUpdateEvent
{
public:
	std::vector<CUpdateEvent *> UpdateTab;
	CUpdateEvent(void);
	~CUpdateEvent(void);
	void AddUpdateEvent (CUpdateEvent *UpdateEvent);
	virtual void Update () {}
	virtual void Read () {}
	virtual void DeleteRow () {}
};
