#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include "token.h"
#include "mo_progitem.h"

static char rec [1024];
static char dm  [512];

PROG_ITEM::~PROG_ITEM ()
{
	     CloseItem (); 
}

BOOL PROG_ITEM::OpenItem (void)
/**
CFG-Datei oeffnen.
**/
{
         char *etc;

         etc = getenv ("BWSETC");
         if (etc == NULL)
         {
                     etc = "c:\\user\\fit\\etc";
         }
 
         sprintf (dm, "%s\\%s.itm", etc, "Artikel");

         progfp = fopen (dm, "r");
         if (progfp == NULL) return FALSE;
         return TRUE;
}
           
void PROG_ITEM::CloseItem (void)
/**
CFG-Datei schliessen.
**/
{
         if (progfp)
         {
             fclose (progfp);
             progfp = NULL;
         }
}

BOOL PROG_ITEM::GetItemValue (char *progitem, char *progvalue)
/**
Wert fuer progitem holen.
**/
{
//         int anz;
         BOOL itOK; 
         int len;
		 CToken token;

         itOK = FALSE;
         if (progfp == NULL)
         {
                   if (OpenItem () == FALSE) return FALSE;
         }

		 token.SetSep ("$");
         progvalue[0] = (char) 0;
		 fseek (progfp, 0l, 0);
         while (fgets (rec, 1023, progfp))
         {
                   if (rec[0] < (char) 0x30) continue;
				   token = rec;
                   if (token.GetAnzToken () < 2) continue;
				   char *wort1 = token.GetToken (0);
                   len = (int) max (strlen (progitem), (int) strlen (wort1));

				   clipped(wort1);
                   if (strupcmp (wort1, progitem, len) == 0)
                   {
					          char *wort2 = token.GetToken (1); 
                              strcpy (progvalue, wort2);
							  clipped (progvalue);
                              itOK = TRUE; 
                              break;
                   }

         }
         return itOK;
}


         

void PROG_ITEM::clipped (char *str)
{
	UCHAR *p = (UCHAR *) str;
	p += strlen (str);
	for (; (p >= (UCHAR *) str) && (*p < 0x21) ; p -= 1);
	*(p + 1) = 0;
}

void PROG_ITEM::cr_weg (char *str)
{
	UCHAR *p = (UCHAR *) str;
	p += strlen (str);
	for (; (p >= (UCHAR *) str) && (*p != 13) && (*p != 10); 
		 p -= 1);
	*p = 0;
	if (p == (UCHAR *) str) return;
	*p --;
	for (; (p >= (UCHAR *) str) && (*p != 13) && (*p != 10); 
		 p -= 1);
	*p = 0;
}

int PROG_ITEM::strupcmp (char *str1, char *str2, int len)
{
	CString Str1 = str1;
	CString Str2 = str2;
	Str2 = Str2.Left (len);
	return (Str1.CompareNoCase (Str2));
}
