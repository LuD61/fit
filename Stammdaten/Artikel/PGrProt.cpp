#include "StdAfx.h"
#include "pgrprot.h"
#include "StrFuncs.h"

CPGrProt::CPGrProt(void)
{
	PersName = "";
	Programm = "";
	Ipr = NULL;
}

CPGrProt::CPGrProt(CString& PersNam, CString& Programm, IPR_CLASS *Ipr)
{
	this->PersName = PersNam;
	this->Programm = Programm;
	this->Ipr = Ipr;
}

CPGrProt::~CPGrProt(void)
{
}

void CPGrProt::Construct (CString& PersNam, CString& Programm, IPR_CLASS *Ipr)
{
	this->PersName = PersNam;
	this->Programm = Programm;
	this->Ipr = Ipr;
}

void CPGrProt::Write (int loesch)
{
	if (Ipr == NULL) return;

	memcpy (&Prg_prot.prg_prot, &prg_prot_null, sizeof  (PRG_PROT));
	Prg_prot.prg_prot.propramm = _tstol (Programm.GetBuffer ());
	CString Date;
	CStrFuncs::SysDate (Date);
	Prg_prot.ToDbDate (Date, &Prg_prot.prg_prot.bearb);
	CString Time;
	CStrFuncs::SysTime (Time);

	strcpy (Prg_prot.prg_prot.zeit, Time.GetBuffer (8));
    strcpy (Prg_prot.prg_prot.pers_nam, PersName.GetBuffer (8));
    Prg_prot.prg_prot.mdn = Ipr->ipr.mdn;
    Prg_prot.prg_prot.pr_gr_stuf = Ipr->ipr.pr_gr_stuf;
    Prg_prot.prg_prot.kun = Ipr->ipr.kun;
    Prg_prot.prg_prot.kun_pr = Ipr->ipr.kun_pr;
    Prg_prot.prg_prot.a = Ipr->ipr.a;
    Prg_prot.prg_prot.vk_pr_i = Ipr->ipr.vk_pr_i;
    Prg_prot.prg_prot.vk_pr_eu = Ipr->ipr.vk_pr_eu;
    Prg_prot.prg_prot.ld_pr = Ipr->ipr.ld_pr;
    Prg_prot.prg_prot.ld_pr_eu = Ipr->ipr.ld_pr_eu;
    Prg_prot.prg_prot.aktion_nr = 0;
    strcpy (Prg_prot.prg_prot.tabelle, "ipr");
	Date = "31.12.1899";
	Prg_prot.ToDbDate (Date, &Prg_prot.prg_prot.aki_von);
	Prg_prot.ToDbDate (Date, &Prg_prot.prg_prot.aki_bis);
	Prg_prot.prg_prot.loesch = loesch;
	Prg_prot.dbupdate ();
}

