#ifndef _LGR_LIST_DEF
#define _LGR_LIST_DEF
#pragma once

class CLgrList
{
public:
	short lgr;
	short mdn;
	CString lgr_bz;
	CLgrList(void);
	CLgrList(short lgr, LPTSTR lgr_bz, short mdn=1);
	~CLgrList(void);
};
#endif
