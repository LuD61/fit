#pragma once
#include "Ean.h"
#include "Emb.h"
#include "SplitPane.h"
#include "BasisdatenPage.h"
#include "UpdateEvent.h"
#include "PageUpdate.h"


// CEanEmb-Dialogfeld

class CEanEmb : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CEanEmb)

public:
	CEanEmb(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CEanEmb();

// Dialogfelddaten
	enum { IDD = IDD_EAN_EMB_DATA };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	virtual BOOL OnInitDialog ();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnSize (UINT, int, int);

	DECLARE_MESSAGE_MAP()
public:
	CPageUpdate *PageUpdate;
	CBasisdatenPage *Basis;
	CEan m_Ean;
	CEmb m_Emb;
	CSplitPane SplitPane;
	void SetBasis (CBasisdatenPage *Basis);
	virtual void Update ();
	virtual void Read ();
	void AddUpdateEvent (CUpdateEvent *UpdateEvent);
};
