#pragma once
#include "BasisdatenPage.h"
#include "ProductPassSheet.h"
#include "ProductPage1.h"
#include "ProductPage2.h"
#include "ProductPage3.h"
#include "ProductPage4.h"
#include "HkstInfo.h"
#include "ProductPage6.h"


// CProductPass-Dialogfeld

class CProductPass : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CProductPass)

public:
	BOOL HideButtons;
	CBasisdatenPage *Basis;
	CWnd *Frame;
	CProductPass();
	virtual ~CProductPass();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCT_PASS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()

public:
	int tabx;
	int taby;
	BOOL Page6Removed; 
	CProductPassSheet dlg;
	CProductPage1 Page1;
	CProductPage2 Page2;
	CProductPage3 Page3;
	CProductPage4 Page4;
	CHkstInfo Page5;
	CProductPage6 Page6;
	void PrdkCopy ();
	void PrdkInsert ();
	void SetBasis (CBasisdatenPage *Basis);
	void SetBasis (CPageUpdate *Update);
	virtual void UpdatePage ();
	virtual BOOL OnSetActive ();
	virtual BOOL OnKillActive ();
	virtual void OnDelete ();
	virtual BOOL Print ();
	virtual void GetPage ();
    virtual void OnCopy ();
    virtual void OnPaste ();
};
