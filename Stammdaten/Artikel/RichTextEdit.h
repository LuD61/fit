#pragma once
#include "afxcmn.h"
#include "Properties.h"
#include "TxtSg.h"
#include "Vector.h"

#ifndef ID_TEXC_CENT
#define ID_TEXT_CENT 10001
#endif

#ifndef ID_TEXC_LEFT
#define ID_TEXT_LEFT 10002
#endif

#ifndef ID_TEXC_RIGHT
#define ID_TEXT_RIGHT 10003
#endif


class SgPos
{
public:
	int pos;
	CString Sg;
	SgPos ()
	{
		pos = 0;
		Sg = _T("");
	}

};


class CRichTextEdit :
	public CRichEditCtrl
{
	DECLARE_DYNCREATE(CRichTextEdit)
public:
	enum
	{
		Left = 1,
		Center = 2,
		Right = 3
	};

	CString DisplayText;
	int lastSize;
	int Alignment;
	SgPos **SgPosis;
	CVector TextFormat;
	CProperties SgTab;
	CRichTextEdit(void);
	~CRichTextEdit(void);
	void SetWindowText (LPCTSTR txt);
	void GetWindowText (CString& txt);
	void GetText (CString& txt);
    void CRichTextEdit::SetDefaultSize (int size);
    void SetSelectionCharSize (int size);
    void SetTextSg (int size);
	void GetTextSg (int row, CString *Txt);
	void GetLinePos (int pos, int& row, int& column);
    void GetPos (int& pos, int row, int column);
	void SetTextFormat (int row, int columns, int size);
    void DestroyFormat ();
	void Copy ();
	void Paste ();
    void ToClipboard (CString& Text);
    void FromClipboard (CString& Text);
	void SetAlignment (int Alignment);
	static int __cdecl ComparePos (const void *el1, const void *el2);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTextCent();
	afx_msg void OnTextLeft();
	afx_msg void OnTextRight();
};
