#include "StdAfx.h"
#include "imagectrl.h"
#include "BasisdatenPage.h"
#include "Process.h"

#ifndef ID_SHOW_IMAGE
#define ID_SHOW_IMAGE 10001
#endif

#ifndef ID_LINK_IMAGE
#define ID_LINK_IMAGE 10002
#endif

#ifndef ID_UNLINK_IMAGE
#define ID_UNLINK_IMAGE 10003
#endif

#ifndef ID_OPEN_IMAGE
#define ID_OPEN_IMAGE 10004
#endif

#ifndef ID_OPEN_IMAGE_WITH
#define ID_OPEN_IMAGE_WITH 10005
#endif

IMPLEMENT_DYNAMIC(CImageCtrl, CStatic)

CImageCtrl::CImageCtrl(void)
{
	ImageFile = "";
	Mode = ZoomToOrg;
	rx = 10;
	ry = 10;
	Rotation = No;
	IsLoading = FALSE;
	ShowImage = FALSE;
	ImageListener = NULL;
}

CImageCtrl::~CImageCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CImageCtrl, CStatic)
	ON_WM_VSCROLL ()
	ON_COMMAND (ID_SHOW_IMAGE, OnShowImage)
	ON_COMMAND (ID_LINK_IMAGE, OnLinkImage)
	ON_COMMAND (ID_UNLINK_IMAGE, OnUnlinkImage)
	ON_COMMAND (ID_OPEN_IMAGE, OnOpenImage)
	ON_COMMAND (ID_OPEN_IMAGE_WITH, OnOpenImageWith)
END_MESSAGE_MAP()

BOOL CImageCtrl::Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect,
                         CWnd* pParentWnd,   UINT nID)
{
	    Style = dwStyle;
        dwStyle |= SS_OWNERDRAW;

		BOOL ret = CStatic::Create (lpszText, dwStyle, rect, pParentWnd, nID);
		return ret;
}

HRESULT CImageCtrl::SetImageFile (CString &ImageFile)
{
	if (IsLoading) return NULL;
	IsLoading = TRUE;
	try
	{
		this->ImageFile = ImageFile;
		HRESULT res = Image.Load (ImageFile.GetBuffer ());
		Sleep (10);
		IsLoading = FALSE;
		return res;
	}
	catch (...) {}
	IsLoading = FALSE;
	return NULL;
}

HRESULT CImageCtrl::SetImageFile (LPTSTR ImageFile)
{
	if (IsLoading) return NULL;
	IsLoading = TRUE;
	try
	{
		this->ImageFile = ImageFile;
		HRESULT res = Image.Load (ImageFile);
		IsLoading = FALSE;
		return res;
	}
	catch (...) {}
	IsLoading = FALSE;
	return NULL;
}


CString& CImageCtrl::GetImageFile ()
{
	return ImageFile;
}

void CImageCtrl::DrawBlackRect (CDC& cDC)
{
	CRect rect;

	GetClientRect (&rect);
	CBrush brush (RGB (0, 0, 0));
	cDC.FrameRect (&rect, &brush);
}


void CImageCtrl::DrawText (CDC& cDC)
{
	CRect rect;

	CString Text;
	GetWindowText (Text);
	if (Text == "") return;
	GetClientRect (&rect);
	CSize size = cDC.GetTextExtent (Text);
	int x = (rect.right - size.cx) / 2;
	int y = (rect.bottom - size.cy) / 2;
	cDC.TextOut (x, y, Text);
}


void CImageCtrl::Draw (CDC& cDC)
{
	CRect rect;

	if (Image.IsNull ()) 
	{
		DrawText (cDC);
		return;
	}

//	cDC.Attach (hDC);
	COLORREF col = cDC.GetBkColor ();
	GetClientRect (&rect);

	if (Mode == OrgSize)
	{
		int cx = Image.GetWidth ();
		int cy = Image.GetHeight ();
		int wcx = rect.right;
		int wcy = rect.bottom;
		int x = (wcx - cx) / 2;
		int y = (wcy - cy) / 2;
		x = (x > 0) ? x : 0;
		y = (y > 0) ? y : 0;
		if (x > 0 || y > 0)
		{
			cDC.FillSolidRect (&rect, col);
		}
//		Image.BitBlt (hDC, x, y);
		Image.BitBlt (cDC.m_hDC, x, y);
	}
	else if (Mode == ZoomToOrg)
	{
        ShowScrollBar(SB_BOTH, FALSE);
	    cDC.FillSolidRect (&rect, col);
		int cx = Image.GetWidth ();
		int cy = Image.GetHeight ();
		int wcx = rect.right - rx;
		int wcy = rect.bottom - ry;
		double propf = (double) cy / cx;
		if (cx < wcx)
		{
			cx = wcx;
			cy = (int) (double) (propf * wcx);
		}
		if (cx > wcx)
		{
			cy = (int) (double) (propf * wcx);
			cx = wcx;
		}
		if (cy > wcy)
		{
			cx = (int) (double) ((double) wcy / propf);
			cy = wcy;
		}
		wcx += ry;
		wcy += ry;
		int x = (wcx - cx) / 2;
		int y = (wcy - cy) / 2;
		x = (x > 0) ? x : 0;
		y = (y > 0) ? y : 0;

		if (Rotation == No)
		{
			StretchBlt (cDC.m_hDC, x, y, cx, cy);
		}
		else
		{
			StretchBlt (cDC.m_hDC, x, y, cx, cy, Rotation);
		}
	}
	else if (Mode == ZoomToWindow)
	{

        ShowScrollBar(SB_BOTH, FALSE);
		GetClientRect (&rect);
		StretchBlt (cDC.m_hDC, rect.right, rect.bottom);
	}
}


void CImageCtrl::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC cDC;
	cDC.Attach (lpDrawItemStruct->hDC);
	CRect rect;
	GetClientRect (&rect);
	cDC.FillRect (&rect, &CBrush (cDC.GetBkColor ()));

	Draw (cDC);
	if ((Style & SS_BLACKRECT) != NULL)
	{
		DrawBlackRect (cDC);
	}
}

void CImageCtrl::StretchBlt (HDC hdc, int cx, int cy)
{

        HDC hdcMemory;
        HDC hbmOld;
        BITMAP bm;

		HBITMAP hBitmap = Image;

        GetObject (hBitmap, sizeof (BITMAP), &bm);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = (HDC) SelectObject (hdcMemory, hBitmap);

        SetViewportOrgEx (hdc, 0, 0, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
		::StretchBlt (hdc, 0, 0,
                         cx, cy,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, SRCCOPY);
        
        SelectObject (hdcMemory, hbmOld);
        DeleteDC (hdcMemory);        
}


void CImageCtrl::StretchBlt (HDC hdc, int x, int y, int cx, int cy)
{

        HDC hdcMemory;
        HDC hbmOld;
        BITMAP bm;

		HBITMAP hBitmap = Image;

        GetObject (hBitmap, sizeof (BITMAP), &bm);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = (HDC) SelectObject (hdcMemory, hBitmap);

        SetViewportOrgEx (hdc, 0, 0, NULL);
        SetStretchBltMode (hdc, COLORONCOLOR);
		::StretchBlt (hdc, x, y,
                         cx, cy,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, SRCCOPY);
        
        SelectObject (hdcMemory, hbmOld);
        DeleteDC (hdcMemory);        
}

void CImageCtrl::StretchBlt (HDC hdc, int x, int y, int cx, int cy, int draw)
{

        HDC hdcMemory;
        HDC hbmOld;
        BITMAP bm;
		int ox = 0;
		int oy = 0;
		int vx = 0;
		int vy = 0;
		CRect rect;
		GetClientRect (&rect);


		ox = rect.right / 2;
		oy = rect.bottom / 2;
		vx = rect.right;
		vy = rect.bottom;
		if (draw == Left)
		{

			ox = ox - cx / 4;
			oy = rect.bottom;
			y = 0;
			vy = (int) (double) ((double) vy * cy / cx);

		}
		else if (draw == Right)
		{
			ox = ox + cx / 4;
			oy = -30;
			y = 0;
			vy = (int) (double) ((double) vy * cy / cx);
			oy = -20;
		}

		HBITMAP hBitmap = Image;

        GetObject (hBitmap, sizeof (BITMAP), &bm);
        hdcMemory  = CreateCompatibleDC(hdc);
        hbmOld = (HDC) SelectObject (hdcMemory, hBitmap);

//		SetMapMode (hdc, MM_ANISOTROPIC);
		SetMapMode (hdc, MM_ISOTROPIC);

        XFORM xForm;

        SetGraphicsMode(hdc, GM_ADVANCED);
		SetWindowExtEx (hdc, rect.right, rect.bottom, NULL);
		SetViewportExtEx (hdc, vx, vy, NULL);
        SetViewportOrgEx (hdc, ox, oy, NULL);

		if (draw == Left)
		{
			xForm.eM11 = (FLOAT) 0.0; 
			xForm.eM12 = (FLOAT) -1.0; 
			xForm.eM21 = (FLOAT) 1.0; 
			xForm.eM22 = (FLOAT) 0.0; 
		}
		else if (draw == Right)
		{
			xForm.eM11 = (FLOAT) 0.0; 
			xForm.eM12 = (FLOAT) 1.0; 
			xForm.eM21 = (FLOAT) -1.0; 
			xForm.eM22 = (FLOAT) 0.0; 
		}
        xForm.eDx  = (FLOAT) 0.0; 
        xForm.eDy  = (FLOAT) 0.0; 
        SetWorldTransform(hdc, &xForm); 

        SetStretchBltMode (hdc, COLORONCOLOR);
     

		::StretchBlt (hdc, x,y,
                         cx, cy,
                         hdcMemory,0, 0,
                         bm.bmWidth, bm.bmHeight, SRCCOPY);
        
        SelectObject (hdcMemory, hbmOld);
        DeleteDC (hdcMemory);        
}

void CImageCtrl::OnVScroll(UINT nSBCode,  UINT nPos,CScrollBar* pScrollBar)
{
	int eins = 1;
}


BOOL CImageCtrl::Show (LPSTR File)
{
	if (!Image.IsNull ())
	{
		Image.Destroy ();
	}
	SetBitmap (NULL);
	HRESULT hRes = SetImageFile (File);
	Rotation = No;

	if (hRes == S_OK)
	{
	    ShowWindow (SW_SHOWNORMAL);
		return TRUE;
	}

	return FALSE;
}

void CImageCtrl::OnShowImage ()
{
	if (ShowImage)
	{
		ShowImage = FALSE;
	}
	else
	{
		ShowImage = TRUE;
	}
	if (ImageListener != NULL)
	{
		ImageListener->ActionPerformed (ImageShow);
	}
}

void CImageCtrl::OnLinkImage ()
{
	    CString Filter = _T("Bilder (*.bmp, *.jpg, *.gif)|*.bmp;*.jpg;*.gif|||");
	    CFileDialog FileDialog(TRUE,
		  					  NULL,
							  NULL,
				              OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
					          Filter,
							  NULL,
							  0);

		if (FileDialog.DoModal () == IDOK)
		{
			ImageFile = FileDialog.GetPathName ();
			Show (ImageFile.GetBuffer ());
			Invalidate ();
		}
		if (ImageListener != NULL)
		{
			ImageListener->ActionPerformed (Link);
		}
}

void CImageCtrl::OnUnlinkImage ()
{
	    ImageFile = ""; 
		if (ImageListener != NULL)
		{
			ImageListener->ActionPerformed (Unlink);
		}
}

BOOL CImageCtrl::OnRButtonDown( UINT flags, CPoint point)
{
	CRect rect;
	GetWindowRect (&rect);
	GetParent ()->ScreenToClient (&rect);
	if (point.x < rect.left) return FALSE;
	if (point.y < rect.top) return FALSE;
	if (point.x > rect.right) return FALSE;
	if (point.y > rect.bottom) return FALSE;


    CMenu menu;
	menu.CreatePopupMenu ();

//	if (ImageFile != "")
	{
		menu.AppendMenu (MF_POPUP | MF_STRING, ID_SHOW_IMAGE, _T("Bild anzeigen"));
		menu.AppendMenu (MF_POPUP | MF_SEPARATOR );
	}
	menu.AppendMenu (MF_POPUP | MF_STRING, ID_LINK_IMAGE, _T("Bild zuordnen"));
	if (ImageFile != "")
	{
		menu.AppendMenu (MF_POPUP | MF_STRING, ID_UNLINK_IMAGE, _T("Zuordnung l�schen"));
		menu.AppendMenu (MF_POPUP | MF_SEPARATOR );
		menu.AppendMenu (MF_POPUP | MF_STRING, ID_OPEN_IMAGE, _T("Bild �ffnen"));
		menu.AppendMenu (MF_POPUP | MF_STRING, ID_OPEN_IMAGE_WITH, _T("Bild �ffnen mit"));
	}
    CWnd *parent = GetParent (); 
	parent->ClientToScreen (&point);
	int x = point.x;
	int y = point.y;
	if (ShowImage)
	{
		menu.CheckMenuItem (ID_SHOW_IMAGE, MF_BYCOMMAND | MF_CHECKED);
	}
	else
	{
		menu.CheckMenuItem (ID_SHOW_IMAGE, MF_BYCOMMAND | MF_UNCHECKED);
	}

	menu.TrackPopupMenu (TPM_CENTERALIGN, x, y, this);    
	menu.DestroyMenu ();

    return TRUE;
}

BOOL CImageCtrl::OnLButtonDblClk( UINT flags, CPoint  point)
{
	CRect rect;
	GetWindowRect (&rect);
	GetParent ()->ScreenToClient (&rect);
	if (point.x < rect.left) return FALSE;
	if (point.y < rect.top) return FALSE;
	if (point.x > rect.right) return FALSE;
	if (point.y > rect.bottom) return FALSE;

	if (ImageFile == "") return FALSE;

    HINSTANCE h = ShellExecute (this->m_hWnd, "open", ImageFile.GetBuffer (), NULL,
								NULL, SW_SHOWNORMAL);
    return TRUE;
}

void CImageCtrl::OnOpenImage()
{
	if (ImageFile == "") return;

	ImageFile.Trim ();
    HINSTANCE h = ShellExecute (this->m_hWnd, "open", ImageFile.GetBuffer (), NULL,
								NULL, SW_SHOWNORMAL);
}


void CImageCtrl::OnOpenImageWith ()
{
	CProcess Process;
	CString Command;
	Command.Format ("rundll32 shell32.dll,OpenAs_RunDLL %s", ImageFile.GetBuffer ());
	Process.SetCommand (Command);
	Process.Start (SW_SHOWNORMAL);
}

