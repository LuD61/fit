// SpannDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "SpannDialog.h"

// CSpannDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CSpannDialog, CDialog)

CSpannDialog::CSpannDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CSpannDialog::IDD, pParent)
{
	A_bas = NULL;
	A_kalk_eig = NULL;
	StdCellHeight = 15;
	SpEkIst = 0.0;
	SpVkIst = 0.0;
	MarktSpPar = Aufschlag;
	mwst = 1.0;
	visible = FALSE;
}

CSpannDialog::~CSpannDialog()
{
}

void CSpannDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMAT_O_B, m_LMatoB);
	DDX_Control(pDX, IDC_LKOST, m_LKost);
	DDX_Control(pDX, IDC_LHK_VOLLK, m_LHkVollk);
	DDX_Control(pDX, IDC_LSPANNE, m_LSpanne);
	DDX_Control(pDX, IDC_LSK, m_LSk);
	DDX_Control(pDX, IDC_LFIL_EK, m_LFilEk);
	DDX_Control(pDX, IDC_LFIL_EK, m_LFilEk);
	DDX_Control(pDX, IDC_LVK, m_LVk);
	DDX_Control(pDX, IDC_LKALK, m_LKalk);
	DDX_Control(pDX, IDC_LKALK, m_LKalk);
	DDX_Control(pDX, IDC_LIST_PR, m_LIstPr);
	DDX_Control(pDX, IDC_LIST_SP, m_LIstSp);

	DDX_Control(pDX, IDC_MAT_O_B, m_MatoB);
	DDX_Control(pDX, IDC_KOST, m_Kost);
	DDX_Control(pDX, IDC_HK_VOLLK, m_HkVollk);

	DDX_Control(pDX, IDC_PSK, m_PSk);
	DDX_Control(pDX, IDC_PFIL_EK, m_PFilEk);
	DDX_Control(pDX, IDC_PVK, m_PVk);

	DDX_Control(pDX, IDC_SK, m_Sk);
	DDX_Control(pDX, IDC_FIL_EK, m_FilEk);
	DDX_Control(pDX, IDC_VK, m_Vk);

	DDX_Control(pDX, IDC_PR_EK,  m_PrEk);
	DDX_Control(pDX, IDC_APR_VK, m_APrVk);

	DDX_Control(pDX, IDC_SP_EK_IST, m_SpEkIst);
	DDX_Control(pDX, IDC_SP_VK_IST, m_SpVkIst);
}


BEGIN_MESSAGE_MAP(CSpannDialog, CDialog)
//	ON_WM_SIZE ()
END_MESSAGE_MAP()


// CSpannDialog-Meldungshandler

BOOL CSpannDialog::OnInitDialog ()
{
	CDialog::OnInitDialog ();

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	Form.Add (new CFormField (&m_MatoB,EDIT,    (double *) &A_kalk_eig->a_kalk_eig.mat_o_b, VDOUBLE, 7,3));
	Form.Add (new CFormField (&m_Kost,EDIT,     (double *) &A_kalk_eig->a_kalk_eig.kost, VDOUBLE, 7,3));
	Form.Add (new CFormField (&m_HkVollk,EDIT,  (double *) &A_kalk_eig->a_kalk_eig.hk_vollk, VDOUBLE, 7,3));

	Form.Add (new CFormField (&m_PSk,EDIT,     (double *) &A_kalk_eig->a_kalk_eig.sp_vk, VDOUBLE, 5,1));
	Form.Add (new CFormField (&m_PFilEk,EDIT,  (double *) &A_kalk_eig->a_kalk_eig.sp_fil, VDOUBLE, 5,1));
	Form.Add (new CFormField (&m_PVk,EDIT,     (double *) &A_kalk_eig->a_kalk_eig.sp_lad, VDOUBLE, 5,1));

	Form.Add (new CFormField (&m_Sk,EDIT,     (double *) &A_kalk_eig->a_kalk_eig.sk_vollk,     VDOUBLE, 7,3));
	Form.Add (new CFormField (&m_FilEk,EDIT,  (double *) &A_kalk_eig->a_kalk_eig.fil_ek_vollk, VDOUBLE, 7,3));
	Form.Add (new CFormField (&m_Vk,EDIT,     (double *) &A_kalk_eig->a_kalk_eig.lad_vk_vollk, VDOUBLE, 7,3));

	Form.Add (new CFormField (&m_PrEk,EDIT,  (double *) &A_pr->a_pr.pr_ek_euro, VDOUBLE, 8,4));
	Form.Add (new CFormField (&m_APrVk,EDIT, (double *) &A_pr->a_pr.pr_vk_euro, VDOUBLE, 6,2));

	Form.Add (new CFormField (&m_SpEkIst,EDIT, (double *) &SpEkIst, VDOUBLE, 7,3));
	Form.Add (new CFormField (&m_SpVkIst,EDIT, (double *) &SpVkIst, VDOUBLE, 7,3));

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (StdCellHeight);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LMatoB     = new CCtrlInfo (&m_LMatoB, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LMatoB);
	CCtrlInfo *c_LKost     = new CCtrlInfo (&m_LKost, 2, 0, 1, 1); 
	CtrlGrid.Add (c_LKost);
	CCtrlInfo *c_LHkVollk     = new CCtrlInfo (&m_LHkVollk, 3, 0, 1, 1); 
	CtrlGrid.Add (c_LHkVollk);

	CCtrlInfo *c_MatoB     = new CCtrlInfo (&m_MatoB, 1, 1, 1, 1); 
	CtrlGrid.Add (c_MatoB);
	CCtrlInfo *c_Kost     = new CCtrlInfo (&m_Kost, 2, 1, 1, 1); 
	CtrlGrid.Add (c_Kost);
	CCtrlInfo *c_HkVollk     = new CCtrlInfo (&m_HkVollk, 3, 1, 1, 1); 
	CtrlGrid.Add (c_HkVollk);

	CCtrlInfo *c_LSpanne     = new CCtrlInfo (&m_LSpanne, 1, 3, 1, 1); 
	CtrlGrid.Add (c_LSpanne);
	CCtrlInfo *c_LSk     = new CCtrlInfo (&m_LSk, 0, 4, 1, 1); 
	CtrlGrid.Add (c_LSk);
	CCtrlInfo *c_LFilEk     = new CCtrlInfo (&m_LFilEk, 0, 5, 1, 1); 
	CtrlGrid.Add (c_LFilEk);
	CCtrlInfo *c_LVk     = new CCtrlInfo (&m_LVk, 0, 6, 1, 1); 
	CtrlGrid.Add (c_LVk);
	CCtrlInfo *c_LKalk     = new CCtrlInfo (&m_LKalk, 2, 3, 1, 1); 
	CtrlGrid.Add (c_LKalk);
	CCtrlInfo *c_LIstPr     = new CCtrlInfo (&m_LIstPr, 3, 3, 1, 1); 
	CtrlGrid.Add (c_LIstPr);
	CCtrlInfo *c_LIstSp     = new CCtrlInfo (&m_LIstSp, 4, 3, 1, 1); 
	CtrlGrid.Add (c_LIstSp);

	CCtrlInfo *c_PSk     = new CCtrlInfo (&m_PSk, 1, 4, 1, 1); 
	CtrlGrid.Add (c_PSk);
	CCtrlInfo *c_PFilEk     = new CCtrlInfo (&m_PFilEk, 1, 5, 1, 1); 
	CtrlGrid.Add (c_PFilEk);
	CCtrlInfo *c_PVk     = new CCtrlInfo (&m_PVk, 1, 6, 1, 1); 
	CtrlGrid.Add (c_PVk);

	CCtrlInfo *c_Sk     = new CCtrlInfo (&m_Sk, 2, 4, 1, 1); 
	CtrlGrid.Add (c_Sk);
	CCtrlInfo *c_FilEk     = new CCtrlInfo (&m_FilEk, 2, 5, 1, 1); 
	CtrlGrid.Add (c_FilEk);
	CCtrlInfo *c_Vk     = new CCtrlInfo (&m_Vk, 2, 6, 1, 1); 
	CtrlGrid.Add (c_Vk);

	CCtrlInfo *c_PrEk     = new CCtrlInfo (&m_PrEk, 3, 5, 1, 1); 
	CtrlGrid.Add (c_PrEk);
	CCtrlInfo *c_APrVk     = new CCtrlInfo (&m_APrVk, 3, 6, 1, 1); 
	CtrlGrid.Add (c_APrVk);

	CCtrlInfo *c_SpEkIst     = new CCtrlInfo (&m_SpEkIst, 4, 5, 1, 1); 
	CtrlGrid.Add (c_SpEkIst);
	CCtrlInfo *c_SpVkIst     = new CCtrlInfo (&m_SpVkIst, 4, 6, 1, 1); 
	CtrlGrid.Add (c_SpVkIst);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display (); 
	return TRUE;
}

void CSpannDialog::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}


BOOL CSpannDialog::PreTranslateMessage(MSG* pMsg)
{
	return FALSE;
}

void CSpannDialog::Show ()
{
	if (!visible) return;
	Form.Show ();
	Calculate ();
}

void CSpannDialog::Get ()
{
	if (!visible) return;
	Calculate ();
	Form.Get ();
}

void CSpannDialog::SetVisible (BOOL visible)
{
	this->visible = visible;
	if (visible)
	{
		ShowWindow (SW_SHOWNORMAL);
	}
	else
	{
		ShowWindow (SW_HIDE);
	}
}

void CSpannDialog::Calculate ()
{
	double pr_vk = A_pr->a_pr.pr_vk_euro;

	Form.Get ();
	A_kalk_eig->a_kalk_eig.hk_vollk	= A_kalk_eig->a_kalk_eig.mat_o_b + 
									  A_kalk_eig->a_kalk_eig.kost;	

	switch (MarktSpPar)
	{
		case AufschlagPlus:
		case AbschlagPlus:
			pr_vk /= mwst;
			break;
	}

	switch (MarktSpPar)
	{
	case Aufschlag:
	case AufschlagPlus:
	//	if (A_kalk_eig->a_kalk_eig.sp_vk != 0.0)
		{
			A_kalk_eig->a_kalk_eig.sk_vollk = A_kalk_eig->a_kalk_eig.hk_vollk * (1 + A_kalk_eig->a_kalk_eig.sp_vk / 100);
		}
	//	if (A_kalk_eig->a_kalk_eig.sp_fil != 0.0)
		{
			A_kalk_eig->a_kalk_eig.fil_ek_vollk = A_kalk_eig->a_kalk_eig.sk_vollk * (1 + A_kalk_eig->a_kalk_eig.sp_fil / 100);
		}
	//	if (A_kalk_eig->a_kalk_eig.sp_lad != 0.0)
		{
			A_kalk_eig->a_kalk_eig.lad_vk_vollk = A_kalk_eig->a_kalk_eig.fil_ek_vollk * (1 + A_kalk_eig->a_kalk_eig.sp_lad / 100);
		}

	    if (A_kalk_eig->a_kalk_eig.sk_vollk > 0.0)
		{
			SpEkIst = (A_pr->a_pr.pr_ek_euro / A_kalk_eig->a_kalk_eig.sk_vollk - 1) * 100;
		}
		if (A_pr->a_pr.pr_ek_euro > 0.0)
		{
			SpVkIst = (pr_vk / A_pr->a_pr.pr_ek_euro - 1) * 100;
		}
		break;
	case Abschlag:
	case AbschlagPlus:
		if (A_kalk_eig->a_kalk_eig.sp_vk != 0.0)
		{
			if (A_kalk_eig->a_kalk_eig.sp_vk >= 100) 
			{
				A_kalk_eig->a_kalk_eig.sp_vk = 0.0;
			}
			A_kalk_eig->a_kalk_eig.sk_vollk = A_kalk_eig->a_kalk_eig.hk_vollk / (1 - A_kalk_eig->a_kalk_eig.sp_vk / 100);
		}
		if (A_kalk_eig->a_kalk_eig.sp_fil != 0.0)
		{
			if (A_kalk_eig->a_kalk_eig.sp_fil >= 100) 
			{
				A_kalk_eig->a_kalk_eig.sp_fil = 0.0;
			}
			A_kalk_eig->a_kalk_eig.fil_ek_vollk = A_kalk_eig->a_kalk_eig.sk_vollk / (1 - A_kalk_eig->a_kalk_eig.sp_fil / 100);
		}
		if (A_kalk_eig->a_kalk_eig.sp_lad != 0.0)
		{
			if (A_kalk_eig->a_kalk_eig.sp_lad >= 100) 
			{
				A_kalk_eig->a_kalk_eig.sp_lad = 0.0;
			}
			A_kalk_eig->a_kalk_eig.lad_vk_vollk = A_kalk_eig->a_kalk_eig.fil_ek_vollk / (1 - A_kalk_eig->a_kalk_eig.sp_lad / 100);
		}

		if (A_pr->a_pr.pr_ek_euro > 0.0)
		{
			SpEkIst = (1 - A_kalk_eig->a_kalk_eig.sk_vollk / A_pr->a_pr.pr_ek_euro) * 100;
		}
		if (pr_vk > 0.0)
		{
			SpVkIst = (1 - A_pr->a_pr.pr_ek_euro / pr_vk) * 100;
		}
		break;
	}

	switch (MarktSpPar)
	{
		case AufschlagPlus:
		case AbschlagPlus:
			if (A_kalk_eig->a_kalk_eig.sp_lad != 0.0)
			{
				A_kalk_eig->a_kalk_eig.lad_vk_vollk	*= mwst;
			}
			break;
	}
	Form.Show ();
}