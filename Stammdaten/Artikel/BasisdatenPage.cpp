// PrArtPreise.cpp : Implementierungsdatei
//
#include "stdafx.h"
#include "Artikel.h"
#include "Art1.h"
#include "BasisdatenPage.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Bmap.h"
#include "Process.h"
#include "basisdatenpage.h"
#include "BmpFunc.h"
#include "KtoDialog.h"
#include "Atyp2.h"
#include "choiceaquery.h"
#include "Token.h"
#include "ArtImages.h"
#include "SendToSystem.h"
#include "PrintRegEtiDlg.h"
#include "PrintThekeEtiDlg.h"
#include "LagerDialog.h"
#include "BestDialog.h"
#include "KalkDialog.h"
#include "PageHandler.h"
#include "DbTime.h"
#include "ChoicePtab.h"  //LAC-115

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
//Test�nderung SVN

// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

#define RECCHANGE_MAT                      WM_USER + 2000
#define RECCHANGE_ART                      WM_USER + 2001
#define ACTIVATE_MAT                       WM_USER + 2002
#define ACTIVATE_ART                       WM_USER + 2003
#define RECCHANGE                          WM_USER + 2004

extern CArtikelApp theApp;  //050214


int StdCellHeight = 15;
int grundartpreis = 0;
char* clipped (char *str)
/**
Blank am Ende eines Strings abschneiden.
**/
{
          size_t i;

          i = strlen (str);
          if (i == 0) return (str);

          for (i = i - 1; i > 0; i --)
          {
                    if ((unsigned char) str[i] > (unsigned char) ' ')
                    {
                              str [i + 1] = (char) 0;
                              break;
                    }
          }
          return (str);
}

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CBasisdatenPage Dialogfeld

HANDLE CBasisdatenPage::Write160Lib = NULL;
HANDLE CBasisdatenPage::InfoLib = NULL;

CBasisdatenPage::CBasisdatenPage(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage(CBasisdatenPage::IDD)
{
	PageType = Standard;
	Choice = NULL;
	ChoiceEan = NULL;
	ChoiceAkrz = NULL;
	ChoiceLiefBest = NULL;
	ChoiceMat = NULL;
	ChoiceM = NULL;
	ChoiceAG = NULL;
	ModalChoice = FALSE;
	ModalChoiceEan = FALSE;
	ModalChoiceAkrz = FALSE;
	ModalChoiceLiefBest = FALSE;
	ModalChoiceMat = FALSE;
	CloseChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceAG = TRUE;
	IprCursor = -1;
	IprGrStufkCursor = -1;
	IKunPrkCursor = -1;
	SaisCursor = -1;
	AGrundCursor = -1;
	GrundCursor = -1;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_A);
    ShowImage = FALSE; 
	CString Types = "";
	m_clrFarbe = RGB(0,0,0);


    HideButtons = FALSE;

	Frame = NULL;
	IprCursor = -1;
	SaisCursor = -1;
	NeuerArtikel = FALSE;
	DbRows.Init ();
	ListRows.Init ();
	SearchA = _T("");
	SearchEan = _T("");
	SearchAG = _T("");
	SearchLiefBest = _T("");
	Separator = _T(";");
	CellHeight = 0;
	Cfg.SetProgName( _T("Artikel"));
//	DlgBkColor = NULL;
	DlgBrush = NULL;
	ListBkColor = NULL;
	Where = _T(" and (a_bas.a_typ in (1,2,4,11,12) or a_bas.a_typ2 in (1,2,4,11,12))");
	FlatLayout = FALSE;
	ArchiveName = _T("Artikel.prp");
	Load ();
	CCtrlInfo::NewStyle = FlatLayout;
    dbild160 = NULL;
	dOpenDbase = NULL;
	Write160 = TRUE;
	AgRestriktion = 2; //FS-18
	a_ersatz_nicht_pruefen = FALSE;
	NeuerArtikelAnzTage = 30;
	ADel = NULL;
	InfoLib = NULL;
	ArtikelGesperrt = FALSE;
	_CallInfoEx = NULL;
	ProductPage1 = NULL;
	ProductPage2 = NULL;
	ProductPage3 = NULL;
	ProductPage4 = NULL;
	ProductPage5 = NULL;
	ProductPage6 = NULL;
	FontHeight = 95;
	TypeMode = Activate;
	NoRecChange = FALSE;
}

CBasisdatenPage::CBasisdatenPage(UINT IDD)
	: CDbPropertyPage(IDD)
{
	Choice = NULL;
	ChoiceM = NULL;
	ModalChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceAG = TRUE;
	Frame = NULL;
	IprCursor = -1;
	SaisCursor = -1;
	AGrundCursor = -1;
	GrundCursor = -1;
	NeuerArtikel = FALSE;
	DbRows.Init ();
	ListRows.Init ();
	Separator = _T(";");
	CellHeight = 0;
	ArchiveName = _T("Artikel.prp");
	Load ();
    dbild160 = NULL;
	dOpenDbase = NULL;
	Write160 = TRUE;
	ADel = NULL;
	InfoLib = NULL;
    _CallInfoEx = NULL;
	TypeMode = Activate;
	NoRecChange = FALSE;
}

CBasisdatenPage::~CBasisdatenPage()
{
	Save ();
	Font.DeleteObject ();
	A_bas.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	Ipr.dbclose ();
	IprGrStufk.dbclose ();
	Kun.dbclose ();
	KunAdr.dbclose ();
	if (IprCursor != -1)
	{
		A_bas.sqlclose (IprCursor);
	}
	if (SaisCursor == -1)
	{
		A_bas.sqlclose (SaisCursor);
	}
	if (GrundCursor == -1)
	{
		A_bas.sqlclose (GrundCursor);
	}
	if (GrundCursor == -1)
	{
		A_bas.sqlclose (AGrundCursor);
	}
	if (IprGrStufkCursor != -1)
	{
		A_bas.sqlclose (IprGrStufkCursor);
	}
	if (IKunPrkCursor != -1)
	{
		A_bas.sqlclose (IKunPrkCursor);
	}
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	if (ChoiceEan != NULL)
	{
		delete ChoiceEan;
		ChoiceEan = NULL;
	}
	if (ChoiceAkrz != NULL)
	{
		delete ChoiceAkrz;
		ChoiceAkrz = NULL;
	}
	if (ChoiceLiefBest != NULL)
	{
		delete ChoiceLiefBest;
		ChoiceLiefBest = NULL;
	}
	if (ChoiceMat != NULL)
	{
		delete ChoiceMat;
		ChoiceMat = NULL;
	}
	if (ChoiceM != NULL)
	{
		delete ChoiceM;
		ChoiceM = NULL;
	}
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
		ChoiceMdn = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	Keys.FirstPosition ();
	while ((f = (CFormField *) Keys.GetNext ()) != NULL)
	{
		delete f;
	}
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	if (DlgBrush != NULL)
	{
		DeleteObject (DlgBrush);
	}
	for (std::vector<CString *>::iterator valp = ATyp1Combo.begin (); 
											valp != ATyp1Combo.end ();
											valp ++)
	{
		CString *value = *valp;
		delete value;
	}
	ATyp1Combo.clear ();
	for (std::vector<CString *>::iterator valp = ATyp2Combo.begin (); 
											valp != ATyp2Combo.end ();
											valp ++)
	{
		CString *value = *valp;
		delete value;
	}
	ATyp2Combo.clear ();
}

void CBasisdatenPage::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);

// ZUTUN Bmp	DDX_Control(pDX, IDC_GESPERRT, m_Gesperrt);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LA, m_LA);
	DDX_Control(pDX, IDC_A, m_A);
//	DDX_Control(pDX, IDC_LA_BZ1, m_LA_bz1);
	DDX_Control(pDX, IDC_A_BZ1, m_A_bz1);
	DDX_Control(pDX, IDC_A_BZ2, m_A_bz2);
	DDX_Control(pDX, IDC_A_BZ3, m_A_bz3);
	DDX_Control(pDX, IDC_A_BZ3_HIDE, m_A_bz3_hide);
	DDX_Control(pDX, IDC_LMATCHCODE, m_LMatchcode);

	DDX_Control(pDX, IDC_STATIC_GRUPPEN, m_Gruppen);
	DDX_Control(pDX, IDC_STATIC_AG, m_Static_ag);
	DDX_Control(pDX, IDC_STATIC_WG, m_Static_wg);
	DDX_Control(pDX, IDC_STATIC_HWG, m_Static_hwg);
	DDX_Control(pDX, IDC_STATIC_ABT, m_Static_abt);
	DDX_Control(pDX, IDC_AG_BZ1, m_Ag_bz1);
	DDX_Control(pDX, IDC_WG_BEZ, m_Wg_bez);
	DDX_Control(pDX, IDC_HWG_BEZ, m_Hwg_bez);

	DDX_Control(pDX, IDC_EDIT_AG, m_Edit_ag);
	DDX_Control(pDX, IDC_EDIT_WG, m_Edit_wg);
	DDX_Control(pDX, IDC_EDIT_HWG, m_Edit_hwg);
	DDX_Control(pDX, IDC_EDIT_ABT, m_Edit_abt);
	DDX_Control(pDX, IDC_ABT_HIDE, m_Abt_hide);

	DDX_Control(pDX, IDC_STATIC_STUECKL, m_Static_stueckl);
	DDX_Control(pDX, IDC_COMBO_STUECKL, m_Combo_stueckl);
	DDX_Control(pDX, IDC_STUECKL_HIDE, m_Stueckl_hide);
	DDX_Control(pDX, IDC_STATIC_TEILSMT, m_Static_teilsmt);
	DDX_Control(pDX, IDC_COMBO_TEILSMT, m_Combo_teilsmt);
	DDX_Control(pDX, IDC_LSMT, m_LSmt);
	DDX_Control(pDX, IDC_SMT, m_Smt);
	DDX_Control(pDX, IDC_TEILSMT_HIDE, m_Teilsmt_hide);
	DDX_Control(pDX, IDC_SMT_HIDE, m_Smt_hide);
	DDX_Control(pDX, IDC_STATIC_GRUND, m_Static_grund);
	DDX_Control(pDX, IDC_STATIC_FARBE, m_Static_farbe);
	DDX_Control(pDX, IDC_EDIT_GRUND, m_Edit_grund);
	DDX_Control(pDX, IDC_GRUND_HIDE, m_Grund_hide);
	DDX_Control(pDX, IDC_CHECK_BESTAND, m_Check_bestand);
	DDX_Control(pDX, IDC_CHECK_PAKET, m_Check_paket);
	DDX_Control(pDX, IDC_STATIC_ERSATZ, m_Static_ersatz);
	DDX_Control(pDX, IDC_EDIT_ERSATZ, m_Edit_ersatz);
	DDX_Control(pDX, IDC_ERSATZ_HIDE, m_Ersatz_hide);
	DDX_Control(pDX, IDC_GRUPPE_BESTAND, m_Gruppen_bestand);
	DDX_Control(pDX, IDC_RADIO_BESTAND_J, m_Radio_bestand_j);
	DDX_Control(pDX, IDC_RADIO_BESTAND_N, m_Radio_bestand_n);
	DDX_Control(pDX, IDC_RADIO_BESTAND_A, m_Radio_bestand_a);

	DDX_Control(pDX, IDC_GRUPPEN_ZUORDNUNGEN, m_Zuordnungen);
	DDX_Control(pDX, IDC_LA_TYP, m_LAtyp);
	DDX_Control(pDX, IDC_A_TYP, m_Atyp);
	DDX_Control(pDX, IDC_LMWST, m_LMwst);
	DDX_Control(pDX, IDC_MWST, m_Mwst);
	DDX_Control(pDX, IDC_PERS_RAB, m_PersRab);
	DDX_Control(pDX, IDC_BEST_AUTO, m_BestAuto);
	DDX_Control(pDX, IDC_LBEST_AUTO2, m_LBestAuto2);
	DDX_Control(pDX, IDC_BEST_AUTO2, m_BestAuto2);

	DDX_Control(pDX, IDC_LWEKTO, m_LWe_kto);
	DDX_Control(pDX, IDC_WEKTO, m_We_kto);
	DDX_Control(pDX, IDC_LINTRA_STAT, m_LIntra_stat);
	DDX_Control(pDX, IDC_INTRA_STAT, m_Intra_stat);
	DDX_Control(pDX, IDC_LERLKTO, m_LErl_kto);
	DDX_Control(pDX, IDC_ERLKTO, m_Erl_kto);

	DDX_Control(pDX, IDC_LINK, m_Link);
	DDX_Control(pDX, IDC_LPFAND, m_LAPfa);
	DDX_Control(pDX, IDC_PFAND, m_APfa);
	DDX_Control(pDX, IDC_LLEERGUT, m_LALeer);
	DDX_Control(pDX, IDC_LEERGUT, m_ALeer);
	DDX_Control(pDX, IDC_LLEIH, m_LALeih);
	DDX_Control(pDX, IDC_LEIH, m_ALeih);

	DDX_Control(pDX, IDC_LCHARGE, m_LCharge);
	DDX_Control(pDX, IDC_CHARGE, m_Charge);

	DDX_Control(pDX, IDC_ZERL_ETI, m_ZerlEti);

	DDX_Control(pDX, IDC_LSCHWUND, m_LSchwund);
	DDX_Control(pDX, IDC_SCHWUND, m_Schwund);

	DDX_Control(pDX, IDC_GROUP_EINH, m_GroupEinh);
	DDX_Control(pDX, IDC_LHND_GEW, m_LHndGew);
	DDX_Control(pDX, IDC_HND_GEW, m_HndGew);
	DDX_Control(pDX, IDC_LME_EINH, m_LMeEinh);
	DDX_Control(pDX, IDC_ME_EINH, m_MeEinh);
	DDX_Control(pDX, IDC_LLIEF_EINH, m_LLiefEinh);
	DDX_Control(pDX, IDC_LIEF_EINH, m_LiefEinh);
	DDX_Control(pDX, IDC_LINH_LIEF, m_LInhLief);
	DDX_Control(pDX, IDC_INH_LIEF, m_InhLief);

	DDX_Control(pDX, IDC_LEINH_ABVERK, m_LEinhAbverk);
	DDX_Control(pDX, IDC_EINH_ABVERK, m_EinhAbverk);
	DDX_Control(pDX, IDC_LINH_ABVERK, m_LInhAbverk);
	DDX_Control(pDX, IDC_INH_ABVERK, m_InhAbverk);
	DDX_Control(pDX, IDC_LHND_GEW_ABVERK, m_LHndGewAbverk);
	DDX_Control(pDX, IDC_HND_GEW_ABVERK, m_HndGewAbverk);

	DDX_Control(pDX, IDC_LKUN_BEST_EINH, m_LKunBestEinh);
	DDX_Control(pDX, IDC_KUN_BEST_EINH, m_KunBestEinh);
	DDX_Control(pDX, IDC_LKUN_INH, m_LKunInh);
	DDX_Control(pDX, IDC_KUN_INH, m_KunInh);

	DDX_Control(pDX, IDC_HND_GEW_HIDE, m_HndGewHide);
	DDX_Control(pDX, IDC_ME_EINH_HIDE, m_MeEinhHide);
	DDX_Control(pDX, IDC_LIEF_EINH_HIDE, m_LiefEinhHide);
	DDX_Control(pDX, IDC_EINH_ABVERK_HIDE, m_EinhAbverkHide);
	DDX_Control(pDX, IDC_KUN_BEST_EINH_HIDE, m_KunBestEinhHide);
	DDX_Control(pDX, IDC_LGR, m_Lager);
	DDX_Control(pDX, IDC_BEST, m_Best);
	DDX_Control(pDX, IDC_KALK, m_Kalk);
	DDX_Control(pDX, IDC_FARBE, m_Farbe);
	DDX_Control(pDX, IDC_MORE_KTO, m_MoreKto);
	DDX_Control(pDX, IDC_ATYP2, m_Atyp2);
	DDX_Control(pDX, IDC_LBEREICH, m_LBereich);
	DDX_Control(pDX, IDC_BEREICH, m_Bereich);
	DDX_Control(pDX, IDC_LSAIS, m_LSais);
	DDX_Control(pDX, IDC_SAIS, m_Sais);
	DDX_Control(pDX, IDC_SHOPKZ, m_ShopKZ);
	DDX_Control(pDX, IDC_DEAKTIV, m_Deaktiv);
	DDX_Control(pDX, IDC_FILDEAKTIV, m_FilDeaktiv);
}

BEGIN_MESSAGE_MAP(CBasisdatenPage, CPropertyPage)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_RBUTTONDOWN ()
	ON_WM_LBUTTONDBLCLK( )
	ON_WM_CTLCOLOR ()
//	ON_WM_SIZE ()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_ACHOICE ,  OnAchoice)
	ON_BN_CLICKED(IDC_AGCHOICE ,  OnAGchoice)
	ON_BN_CLICKED(IDC_GRUNDCHOICE ,  OnAchoiceM_grund)
	ON_BN_CLICKED(IDC_ERSATZCHOICE ,  OnAchoiceM_ersatz)
	ON_BN_CLICKED(IDC_CHECK_BESTAND, OnBsdClicked)
	ON_BN_CLICKED(IDC_INHLIEFCHOICE ,  OnInhLiefChoice)
	ON_COMMAND (SELECTED, OnASelected)
	ON_COMMAND (CANCELED, OnACanceled)
	ON_COMMAND (EANSELECTED, OnEanSelected)
	ON_COMMAND (EANCANCELED, OnEanCanceled)
	ON_COMMAND (AKRZSELECTED, OnAkrzSelected)
	ON_COMMAND (LIEFBESTSELECTED, OnLiefBestSelected)
	ON_COMMAND (MATSELECTED, OnMatSelected)
	ON_COMMAND (AKRZCANCELED, OnAkrzCanceled)
	ON_COMMAND (LIEFBESTCANCELED, OnLiefBestCanceled)
	ON_COMMAND (MATCANCELED, OnMatCanceled)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
	ON_NOTIFY (HDN_ENDTRACK, 0, OnListEndTrack)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
	ON_EN_CHANGE(IDC_A, OnEnChangeA)
	ON_EN_KILLFOCUS(IDC_EDIT_AG, OnEnKillfocusEditAg)
	ON_EN_KILLFOCUS(IDC_MDN, OnEnKillfocusMdn)
	ON_EN_KILLFOCUS(IDC_A, OnEnKillfocusA)
	ON_EN_KILLFOCUS(IDC_EDIT_GRUND, OnEnKillfocusEditGrund)
	ON_EN_KILLFOCUS(IDC_EDIT_ERSATZ, OnEnKillfocusEditErsatz)
	ON_COMMAND(ID_LINK_IMAGE, OnLinkImage)
	ON_COMMAND(ID_UNLINK_IMAGE, OnUnlinkImage)
	ON_COMMAND(IDC_LGR, OnLager)
	ON_COMMAND(IDC_BEST, OnBest)
	ON_COMMAND(IDC_KALK, OnKalk)
	ON_COMMAND(IDC_FARBE, OnFarbe)
	ON_COMMAND(IDC_MORE_KTO, OnMoreKto)
	ON_COMMAND(IDC_APFACHOICE, OnChoiceAPfa)
	ON_COMMAND(IDC_ALEERCHOICE, OnChoiceALeer)
	ON_COMMAND(IDC_ALEIHCHOICE, OnChoiceALeih)
	ON_CBN_SELCHANGE(IDC_A_TYP, OnCbnSelchangeATyp)
	ON_COMMAND(IDC_ATYP2, OnAtyp2)
	ON_EN_SETFOCUS(IDC_A, &CBasisdatenPage::OnEnSetfocusA)
	ON_BN_CLICKED(IDC_FILDEAKTIV, &CBasisdatenPage::OnBnClickedFildeaktiv)
	ON_BN_CLICKED(IDC_DEAKTIV, &CBasisdatenPage::OnBnClickedDeaktiv)
	ON_CBN_SELCHANGE(IDC_HND_GEW, &CBasisdatenPage::OnCbnSelchangeHndGew)
END_MESSAGE_MAP()


// CBasisdatenPage Meldungshandler

BOOL CBasisdatenPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// TODO: Hier zus�tzliche Initialisierung einf�gen

    m_ImageArticle.Create (_T("Bild"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, CRect (0, 0, 250, 200),
		                   this, IDC_ARTIMAGE);
/*
    m_MoreKto.Create (_T("weitere Konten"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 120, 20),
		                          this, IDC_MOREKTO);
*/
	m_MoreKto.nID = IDC_MORE_KTO;
	m_MoreKto.SetWindowText (_T("weitere Konten"));
//	m_MoreKto.SetBkColor (GetSysColor (COLOR_3DFACE));

	m_Atyp2.nID = IDC_ATYP2;
	m_Atyp2.SetWindowText (_T("2. Artikeltyp"));

/*
    m_Lager.Create (_T("Lager"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 80, 20),
		                          this, IDC_LAGER);
*/
	m_Lager.nID = IDC_LGR;
	m_Lager.SetWindowText (_T("Lager"));
//    m_Lager.ShowWindow (SW_HIDE);

	m_Best.nID = IDC_BEST;
	m_Best.SetWindowText (_T("Bestelldaten"));
    m_Best.ShowWindow (SW_SHOWNORMAL);

	m_Kalk.nID = IDC_KALK;
	m_Kalk.SetWindowText (_T("Kalkulation"));
    m_Kalk.ShowWindow (SW_SHOWNORMAL);

	m_Farbe.nID = IDC_FARBE;
	m_Farbe.SetWindowText (_T("Auswahl"));
    m_Farbe.ShowWindow (SW_SHOWNORMAL);

	A_bas.opendbase (_T("bws"));

// 160-iger schreiben

	if (Write160 && Write160Lib == NULL)
	{
		CString bws;
		BOOL ret = bws.GetEnvironmentVariable (_T("bws"));
		CString W160Dll;
		if (ret)
		{
			W160Dll.Format (_T("%s\\bin\\bild160dll.dll"), bws.GetBuffer ());
		}
		else
		{
			W160Dll = _T("bild160dll.dll");
		}
		Write160Lib = LoadLibrary (W160Dll.GetBuffer ());
		if (Write160Lib != NULL && dbild160 == NULL)
		{
			dbild160 = (int (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "bild160");
			dOpenDbase = (BOOL (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "OpenDbase");
			if (dOpenDbase != NULL)
			{
				(*dOpenDbase) ("bws");
			}
		}
	}
	else if (dbild160 == NULL)
	{
		dbild160 = (int (*) (LPSTR))
					  GetProcAddress ((HMODULE) Write160Lib, "bild160");
	}

//	InfoLib = NULL;
//    _CallInfoEx = NULL;

	if (InfoLib == NULL) 
	{
		CString bws;
		BOOL ret = bws.GetEnvironmentVariable (_T("bws"));
		CString InfoDll;
		if (ret)
		{
			InfoDll.Format (_T("%s\\bin\\inflib.dll"), bws.GetBuffer ());
		}
		else
		{
			InfoDll = _T("inflib.dll.dll");
		}
		InfoLib = LoadLibrary (InfoDll.GetBuffer ());
		if (_CallInfoEx == NULL)
		{
			_CallInfoEx = (BOOL (*) (HWND, RECT *,char *, char *,DWORD))
					  GetProcAddress ((HMODULE) InfoLib, "_CallInfoEx");
		}
	}


	if (PageType == Material)
	{
		Where = _T(" and (a_bas.a_typ in (5,6,9) or a_bas.a_typ2 in (5,6,9))"); 
	}
	CUtil::GetPersName (PersName);
	PgrProt.Construct (PersName, CString ("12100"), &Ipr);
     
	ReadCfg ();
    if (AgRestriktion != Verbieten) Where = _T(" ");  //FS-18
	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (FontHeight, _T("Dlg"));
		lFont.CreatePointFont (FontHeight, _T("Courier New"));
	}
/*
	else if (GetSystemMetrics (SM_CXFULLSCREEN) <= 1200)
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	else
	{
		Font.CreatePointFont (105, _T("Dlg"));
		lFont.CreatePointFont (105, _T("Courier New"));
	}
*/

	currentAg = 0;

    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_A,EDIT,       (double *) &A_bas.a_bas.a, VDOUBLE, 13, 0));
    Form.Add (new CUniFormField (&m_A_bz1,EDIT,   (char *) A_bas.a_bas.a_bz1, VCHAR, sizeof (A_bas.a_bas.a_bz1) - 1));
    Form.Add (new CUniFormField (&m_A_bz2,EDIT,   (char *) A_bas.a_bas.a_bz2, VCHAR, sizeof (A_bas.a_bas.a_bz2) - 1));
    Form.Add (new CUniFormField (&m_A_bz3,EDIT,   (char *) A_bas.a_bas.a_bz3, VCHAR, sizeof (A_bas.a_bas.a_bz2) - 1));
    Form.Add (new CFormField (&m_A_bz3_hide,CHECKBOX,   (char *) da_bz3_hide, VCHAR));
    Form.Add (new CUniFormField (&m_Edit_ag,EDIT,   (long *) &A_bas.a_bas.ag, VLONG));
    Form.Add (new CUniFormField (&m_Edit_wg,EDIT,   (short *) &A_bas.a_bas.wg, VSHORT));
    Form.Add (new CUniFormField (&m_Edit_hwg,EDIT,   (short *) &A_bas.a_bas.hwg, VSHORT));
    Form.Add (new CUniFormField (&m_Edit_abt,EDIT,   (short *) &A_bas.a_bas.abt, VSHORT));
    Form.Add (new CFormField (&m_Abt_hide,CHECKBOX,   (char *) dabt_hide, VCHAR));
    Form.Add (new CUniFormField (&m_Ag_bz1,EDIT,   (char *) Ag.ag.ag_bz1, VCHAR));
	Form.Add (new CUniFormField (&m_Wg_bez,EDIT,   (char *) Work.wgw.wg_bz1, VCHAR));
	Form.Add (new CUniFormField (&m_Hwg_bez,EDIT,   (char *) Work.hwg.hwg_bz1, VCHAR));
	Form.Add (new CFormField (&m_Combo_stueckl,COMBOBOX, (char *) A_bas.a_bas.stk_lst_kz, VCHAR));
    Form.Add (new CFormField (&m_Stueckl_hide,CHECKBOX,   (char *) dstueckl_hide, VCHAR));
	Form.Add (new CFormField (&m_Smt,COMBOBOX,          (char *) &A_bas.a_bas.smt, VCHAR));
	Form.Add (new CFormField (&m_Combo_teilsmt,COMBOBOX, (short *) &A_bas.a_bas.teil_smt, VSHORT));
    Form.Add (new CFormField (&m_Smt_hide,CHECKBOX,   (char *) dsmt_hide, VCHAR));
    Form.Add (new CFormField (&m_Teilsmt_hide,CHECKBOX,   (char *) dteilsmt_hide, VCHAR));
	Form.Add (new CFormField (&m_Edit_grund,EDIT,   (double *) &A_bas.a_bas.a_grund, VDOUBLE, 13, 0));
    Form.Add (new CFormField (&m_Grund_hide,CHECKBOX,   (char *) dgrund_hide, VCHAR)); 
    Form.Add (new CUniFormField (&m_Check_bestand,CHECKBOX,   (char *) A_bas.a_bas.bsd_kz, VCHAR));
    Form.Add (new CUniFormField (&m_Check_paket,CHECKBOX,   (short *) &A_bas_erw.a_bas_erw.paketartikel, VSHORT));
	Form.Add (new CFormField (&m_Edit_ersatz,EDIT,   (double *) &A_bas.a_bas.a_ersatz, VDOUBLE, 13, 0));
    Form.Add (new CFormField (&m_Ersatz_hide,CHECKBOX,   (char *) dersatz_hide, VCHAR));
	Form.Add (new CUniFormField (&m_Hwg_bez,EDIT,   (char *) Work.hwg.hwg_bz1, VCHAR));
    Form.Add (new CFormField (&m_Radio_bestand_j,CHECKBOX,   (char *) dbestand_j, VCHAR));
    Form.Add (new CFormField (&m_Radio_bestand_n,CHECKBOX,   (char *) dbestand_n, VCHAR));
    Form.Add (new CFormField (&m_Radio_bestand_a,CHECKBOX,   (char *) dbestand_a, VCHAR));
//	Form.Add (new CFormField (&m_Atyp,COMBOBOX, (short *) &A_bas.a_bas.a_typ, VSHORT));
	Form.Add (new CFormField (&m_Atyp,COMBOBOX, (short *) &a_typ, VSHORT));
	Form.Add (new CFormField (&m_Mwst,COMBOBOX, (short *) &A_bas.a_bas.mwst, VSHORT));
	Form.Add (new CFormField (&m_PersRab,CHECKBOX, (char *) &A_bas.a_bas.pers_rab_kz, VCHAR));
	Form.Add (new CFormField (&m_BestAuto,CHECKBOX, (char *) &A_bas.a_bas.best_auto, VCHAR));
	Form.Add (new CFormField (&m_BestAuto2,COMBOBOX, (char *) &A_bas.a_bas.best_auto, VCHAR));
	Form.Add (new CFormField (&m_We_kto,EDIT,  (long *) &A_bas.a_bas.we_kto, VLONG));
	//231114 Form.Add (new CFormField (&m_Intra_stat,  EDIT,  (long *) &A_bas.a_bas.intra_stat, VLONG));
	Form.Add (new CFormField (&m_Intra_stat,  EDIT,  (char *) &A_bas_erw.a_bas_erw.zolltarifnr, VCHAR)); //231114
	Form.Add (new CFormField (&m_Erl_kto,EDIT,  (long *) &A_bas.a_bas.erl_kto, VLONG));
	Form.Add (new CFormField (&m_APfa,EDIT,  (double *) &A_hndw.a_hndw.a_pfa, VDOUBLE));
	Form.Add (new CFormField (&m_ALeer,EDIT,  (double *) &A_hndw.a_hndw.a_leer, VDOUBLE));
	Form.Add (new CFormField (&m_ALeih,EDIT,  (double *) &A_bas.a_bas.a_leih, VDOUBLE));
	Form.Add (new CFormField (&m_Charge,COMBOBOX,  (short *) &A_bas.a_bas.charg_hand, VSHORT));
	Form.Add (new CFormField (&m_ZerlEti,CHECKBOX,  (short *) &A_bas.a_bas.zerl_eti, VSHORT));
	Form.Add (new CFormField (&m_Schwund,EDIT,  (double *) &A_bas.a_bas.sw, VDOUBLE, 3, 1));
	Form.Add (new CFormField (&m_Bereich,COMBOBOX,  (short *) &A_bas.a_bas.bereich, VSHORT));
	Form.Add (new CFormField (&m_Sais,COMBOBOX,  (short *) &A_bas.a_bas.sais, VSHORT)); 

	Form.Add (new CFormField (&m_HndGew,COMBOBOX, (char *) A_bas.a_bas.hnd_gew, VCHAR));
	Form.Add (new CFormField (&m_MeEinh,COMBOBOX, (short *) &A_bas.a_bas.me_einh, VSHORT));
	Form.Add (new CFormField (&m_LiefEinh,COMBOBOX, (short *) &A_bas.a_bas.lief_einh, VSHORT));
	Form.Add (new CFormField (&m_InhLief, EDIT,     (double *) &A_bas.a_bas.inh_lief, VDOUBLE, 11,3));

	Form.Add (new CFormField (&m_EinhAbverk,COMBOBOX, (short *) &A_bas.a_bas.me_einh_abverk, VSHORT));
	Form.Add (new CFormField (&m_InhAbverk, EDIT,     (double *) &A_bas.a_bas.inh_abverk, VDOUBLE, 11,3));
	Form.Add (new CFormField (&m_HndGewAbverk,COMBOBOX, (char *) A_bas.a_bas.hnd_gew_abverk, VCHAR));

	Form.Add (new CFormField (&m_KunBestEinh,COMBOBOX,    (short *) &A_hndw.a_hndw.me_einh_kun, VSHORT));
	Form.Add (new CFormField (&m_KunInh, EDIT,        (double *) &A_hndw.a_hndw.inh, VDOUBLE, 11,3));

    Form.Add (new CFormField (&m_HndGewHide,CHECKBOX,   (char *) dhnd_gew_hide, VCHAR));
    Form.Add (new CFormField (&m_MeEinhHide,CHECKBOX,   (char *) dme_einh_hide, VCHAR));
    Form.Add (new CFormField (&m_LiefEinhHide,CHECKBOX,   (char *) dlief_einh_hide, VCHAR));
    Form.Add (new CFormField (&m_EinhAbverkHide,CHECKBOX, (char *) deinh_abverk_hide, VCHAR));
    Form.Add (new CFormField (&m_KunBestEinhHide,CHECKBOX,    (char *) dkun_einh_hide, VCHAR));
	Form.Add (new CFormField (&m_ShopKZ,CHECKBOX, (char *) A_bas.a_bas.shop_kz, VCHAR));
	Form.Add (new CFormField (&m_Deaktiv,CHECKBOX, (short *) &A_bas.a_bas.ghsperre, VSHORT));
	Form.Add (new CFormField (&m_FilDeaktiv,CHECKBOX, (short *) &A_bas.a_bas.filialsperre, VSHORT));

    Keys.Add (new CFormField (&m_A,EDIT,       (double *) &A_bas.a_bas.a, VDOUBLE, 13, 0));

	FillStuecklCombo ();
	FillSmtCombo ();
	FillTeilsmtCombo ();
	FillAtypCombo ();
	FillAtyp2Combo ();
	SetWhere ();
	SetAgWhere ();
	SetPeriTypes ();

	FillMwstCombo ();
	FillHndGewCombo ();

	FillMeEinhCombo ();
	FillLiefEinhCombo ();
	FillEinhAbverkCombo ();
	FillHndGewAbverkCombo ();
	FillKunEinhCombo ();
	FillCombo (_T("charg_hand"), &m_Charge);
	FillCombo (_T("best_auto"), &m_BestAuto2);


	_tcscpy (sys_par.sys_par_nam, _T("fisch_par"));
	if (Sys_par.dbreadfirst () == 0)
	{
		if ((sys_par.sys_par_wrt[0] == '1'))
		{
			fisch_par = TRUE;
		}
	}

	if (fisch_par)
	{
		FillComboEx (_T("fangart"), &m_Bereich);
	//	FillComboLong (_T("fanggebiet"), &m_Sais);
		FillComboEx (_T("fanggebiet"), &m_Sais);
	}
	else
	{
		FillCombo (_T("bereich"), &m_Bereich);
		FillSais ();
	}

	/**
	if (a_typ != Hndw)
	{
		m_APfa.EnableWindow (FALSE);
		m_ALeer.EnableWindow (FALSE);
		m_ALeih.EnableWindow (FALSE);
		m_APfaChoice.EnableWindow (FALSE);  
		m_ALeerChoice.EnableWindow (FALSE);
		m_ALeihChoice.EnableWindow (FALSE);
	}
	else	
	{
		m_APfa.EnableWindow (TRUE);
		m_ALeer.EnableWindow (TRUE);
		m_ALeih.EnableWindow (TRUE);
		m_APfaChoice.EnableWindow (TRUE);
		m_ALeerChoice.EnableWindow (TRUE);
		m_ALeihChoice.EnableWindow (TRUE);
	}
	**/
	theApp.SetBasisdatenPage (this); //WAL-130

	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
	}


    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (StdCellHeight);
    CtrlGrid.SetFontCellHeight (this, &Font);
//    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

// Grid fpr automatische Bestellung
    BestAutoGrid.Create (this, 2, 2);
	BestAutoGrid.SetFont (&Font);
    BestAutoGrid.SetBorder (0, 0);
    BestAutoGrid.SetCellHeight (StdCellHeight);
    BestAutoGrid.SetFontCellHeight (this, &Font);
    BestAutoGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

//Grid Mdn
	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

//Grid Artikel 
	AGrid.Create (this, 1, 2);
    AGrid.SetBorder (0, 0);
    AGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_A = new CCtrlInfo (&m_A, 0, 0, 1, 1);
	AGrid.Add (c_A);
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	AGrid.Add (c_AChoice);
//Grid Artikelgruppe
	AGGrid.Create (this, 1, 2);
    AGGrid.SetBorder (0, 0);
    AGGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Edit_ag = new CCtrlInfo (&m_Edit_ag, 0, 0, 1, 1);
	AGGrid.Add (c_Edit_ag);
	CtrlGrid.CreateChoiceButton (m_AGChoice, IDC_AGCHOICE, this);
	CCtrlInfo *c_AGChoice = new CCtrlInfo (&m_AGChoice, 1, 0, 1, 1);
	AGGrid.Add (c_AGChoice);
//Grid Grundartikel
	GrundGrid.Create (this, 2, 3);
    GrundGrid.SetBorder (0, 0);
    GrundGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Edit_grund = new CCtrlInfo (&m_Edit_grund, 0, 0, 1, 1);
	GrundGrid.Add (c_Edit_grund);
	CtrlGrid.CreateChoiceButton (m_GrundChoice, IDC_GRUNDCHOICE, this);
	CCtrlInfo *c_GrundChoice = new CCtrlInfo (&m_GrundChoice, 1, 0, 1, 1);
	GrundGrid.Add (c_GrundChoice);
//Grid ersatzartikel
	ErsatzGrid.Create (this, 1, 2);
    ErsatzGrid.SetBorder (0, 0);
    ErsatzGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Edit_ersatz = new CCtrlInfo (&m_Edit_ersatz, 0, 0, 1, 1);
	ErsatzGrid.Add (c_Edit_ersatz);
	CtrlGrid.CreateChoiceButton (m_ErsatzChoice, IDC_ERSATZCHOICE, this);
	CCtrlInfo *c_ErsatzChoice = new CCtrlInfo (&m_ErsatzChoice, 1, 0, 1, 1);
	ErsatzGrid.Add (c_ErsatzChoice);
//Grid Bestand 
	BestandGrid.Create (this, 1, 4);
    BestandGrid.SetBorder (0, 0);
    BestandGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Gruppe_bestand = new CCtrlInfo (&m_Gruppen_bestand, 0, 0, 4, 5);
	BestandGrid.Add (c_Gruppe_bestand);
	CCtrlInfo *c_Radio_bestand_j = new CCtrlInfo (&m_Radio_bestand_j, 1, 1, 1, 1);
	BestandGrid.Add (c_Radio_bestand_j);
	CCtrlInfo *c_Radio_bestand_n = new CCtrlInfo (&m_Radio_bestand_n, 2, 1, 1, 1);
	BestandGrid.Add (c_Radio_bestand_n);
	CCtrlInfo *c_Radio_bestand_a = new CCtrlInfo (&m_Radio_bestand_a, 3, 1, 1, 1);
	BestandGrid.Add (c_Radio_bestand_a);


//Mandant
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 1, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 3, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);
//Artikel
	CCtrlInfo *c_LA     = new CCtrlInfo (&m_LA, 1, 1, 1, 1); 
	CtrlGrid.Add (c_LA);
	CCtrlInfo *c_AGrid   = new CCtrlInfo (&AGrid, 2, 1, 1, 1); 
	CtrlGrid.Add (c_AGrid);
	CCtrlInfo *c_LMatchcode  = new CCtrlInfo (&m_LMatchcode, 2, 2, 1, 1); 
	CtrlGrid.Add (c_LMatchcode);
	CCtrlInfo *c_a_bz3_hide = new CCtrlInfo (&m_A_bz3_hide, 2, 2, 1, 1);
	CtrlGrid.Add (c_a_bz3_hide);
	CCtrlInfo *c_A_bz1  = new CCtrlInfo (&m_A_bz1, 3, 1, 3, 1); 
	CtrlGrid.Add (c_A_bz1);
	CCtrlInfo *c_A_bz2  = new CCtrlInfo (&m_A_bz2, 4, 1, 4, 1); 
	CtrlGrid.Add (c_A_bz2);
	CCtrlInfo *c_A_bz3  = new CCtrlInfo (&m_A_bz3, 3, 2, 3, 1); 
	CtrlGrid.Add (c_A_bz3);

// ==== GRUPPEN =====
/*** WAL-23
	CCtrlInfo *c_Gruppen = new CCtrlInfo (&m_Gruppen, 0, 3, 8, 9);
    c_Gruppen->rightspace = 30; 
	c_Gruppen->BottomControl = &m_Zuordnungen;
	CtrlGrid.Add (c_Gruppen)
***/

//Artikelgruppe
	CCtrlInfo *c_Static_ag = new CCtrlInfo (&m_Static_ag, 1, 3, 1, 1);
	CtrlGrid.Add (c_Static_ag);
	CCtrlInfo *c_AGGrid = new CCtrlInfo (&AGGrid, 2, 3, 1, 1);
	CtrlGrid.Add (c_AGGrid);
	CCtrlInfo *c_Ag_bz1 = new CCtrlInfo (&m_Ag_bz1, 3, 3, 20, 20);
	CtrlGrid.Add (c_Ag_bz1);
//Warengruppe
	CCtrlInfo *c_Static_wg = new CCtrlInfo (&m_Static_wg, 1, 4, 1, 1);
	CtrlGrid.Add (c_Static_wg);
	CCtrlInfo *c_Edit_wg = new CCtrlInfo (&m_Edit_wg, 2, 4, 1, 1);
	CtrlGrid.Add (c_Edit_wg);
	CCtrlInfo *c_Wg_bez = new CCtrlInfo (&m_Wg_bez, 3, 4, 1, 1);
	CtrlGrid.Add (c_Wg_bez);
//HauptWarengruppe
	CCtrlInfo *c_Static_hwg = new CCtrlInfo (&m_Static_hwg, 1, 5, 1, 1);
	CtrlGrid.Add (c_Static_hwg);
	CCtrlInfo *c_Edit_hwg = new CCtrlInfo (&m_Edit_hwg, 2, 5, 1, 1);
	CtrlGrid.Add (c_Edit_hwg);
	CCtrlInfo *c_Hwg_bez = new CCtrlInfo (&m_Hwg_bez, 3, 5, 1, 1);
	CtrlGrid.Add (c_Hwg_bez);
//Abteilung
	CCtrlInfo *c_Static_abt = new CCtrlInfo (&m_Static_abt, 1, 7, 1, 1);
	CtrlGrid.Add (c_Static_abt);
	CCtrlInfo *c_abt_hide = new CCtrlInfo (&m_Abt_hide, 1, 7, 1, 1);
	CtrlGrid.Add (c_abt_hide);
	CCtrlInfo *c_Edit_abt = new CCtrlInfo (&m_Edit_abt, 2, 7, 1, 1);
	CtrlGrid.Add (c_Edit_abt);

//FS-26
	CCtrlInfo *c_FilDeaktiv = new CCtrlInfo (&m_FilDeaktiv, 1, 8, 2, 1);
	CtrlGrid.Add (c_FilDeaktiv);
	CCtrlInfo *c_Deaktiv = new CCtrlInfo (&m_Deaktiv, 1, 9, 2, 1);
	CtrlGrid.Add (c_Deaktiv);

//Rezept.Stueckl.
	CCtrlInfo *c_Static_stueckl = new CCtrlInfo (&m_Static_stueckl, 4, 4, 1, 1);
	CtrlGrid.Add (c_Static_stueckl);
	CCtrlInfo *c_Stueckl_hide = new CCtrlInfo (&m_Stueckl_hide, 4, 4, 1, 1);
	CtrlGrid.Add (c_Stueckl_hide);
	CCtrlInfo *c_Combo_stueckl = new CCtrlInfo (&m_Combo_stueckl, 5, 4, 1, 1);
	CtrlGrid.Add (c_Combo_stueckl);

//Sortiment
	CCtrlInfo *c_LSmt = new CCtrlInfo (&m_LSmt, 4, 5, 1, 1);
	CtrlGrid.Add (c_LSmt);
	CCtrlInfo *c_Smt_hide = new CCtrlInfo (&m_Smt_hide, 4, 5, 1, 1);
	CtrlGrid.Add (c_Smt_hide);
	CCtrlInfo *c_Smt = new CCtrlInfo (&m_Smt, 5, 5, 1, 1);
	CtrlGrid.Add (c_Smt);

//Teilsortiment
	CCtrlInfo *c_Static_teilsmt = new CCtrlInfo (&m_Static_teilsmt, 4, 6, 1, 1);
	CtrlGrid.Add (c_Static_teilsmt);
	CCtrlInfo *c_Teilsmt_hide = new CCtrlInfo (&m_Teilsmt_hide, 4, 6, 1, 1);
	CtrlGrid.Add (c_Teilsmt_hide);
	CCtrlInfo *c_Combo_teilsmt = new CCtrlInfo (&m_Combo_teilsmt, 5, 6, 1, 1);
	CtrlGrid.Add (c_Combo_teilsmt);
//Ersatzartikel
	CCtrlInfo *c_Static_ersatz = new CCtrlInfo (&m_Static_ersatz, 4, 7, 1, 1);
	CtrlGrid.Add (c_Static_ersatz);
	CCtrlInfo *c_Ersatz_hide = new CCtrlInfo (&m_Ersatz_hide, 4, 7, 1, 1);
	CtrlGrid.Add (c_Ersatz_hide);
	CCtrlInfo *c_ErsatzGrid = new CCtrlInfo (&ErsatzGrid, 5, 7, 1, 1);
	CtrlGrid.Add (c_ErsatzGrid);
//Grundartikel
	CCtrlInfo *c_Static_grund = new CCtrlInfo (&m_Static_grund, 4, 8, 1, 1);
	CtrlGrid.Add (c_Static_grund);
	CCtrlInfo *c_Grund_hide = new CCtrlInfo (&m_Grund_hide, 4, 8, 1, 1);
	CtrlGrid.Add (c_Grund_hide);
	CCtrlInfo *c_GrundGrid = new CCtrlInfo (&GrundGrid, 5, 8, 1, 1);
	CtrlGrid.Add (c_GrundGrid);
	CCtrlInfo *c_Static_Farbe = new CCtrlInfo (&m_Static_farbe, 5, 9, 1, 1);
	CtrlGrid.Add (c_Static_Farbe);
	CCtrlInfo *c_Farbe = new CCtrlInfo (&m_Farbe, 6, 9, 1, 1);
	CtrlGrid.Add (c_Farbe);
	m_Farbe.Orientation = m_Farbe.Left;



	CCtrlInfo *c_Check_paket = new CCtrlInfo (&m_Check_paket, 3, 6, 1, 1);
	CtrlGrid.Add (c_Check_paket);

//Bestand

//Grid Bestand 
	BsdGrid.Create (this, 4, 4);
    BsdGrid.SetBorder (0, 0);
    BsdGrid.SetGridSpace (5, 5);

	CCtrlInfo *c_Check_bestand = new CCtrlInfo (&m_Check_bestand, 0, 0, 2, 1);
	BsdGrid.Add (c_Check_bestand);
	CCtrlInfo *c_ShopKZ = new CCtrlInfo (&m_ShopKZ, 0, 1, 2, 1);
	BsdGrid.Add (c_ShopKZ);
//Lager
	CCtrlInfo *c_Lager = new CCtrlInfo (&m_Lager, 0, 2, 1, 1);
	BsdGrid.Add (c_Lager);
	m_Lager.Orientation = m_Lager.Left;
//Bestelldaten
	CCtrlInfo *c_Best = new CCtrlInfo (&m_Best, 1, 2, 1, 1);
	BsdGrid.Add (c_Best);
	m_Best.Orientation = m_Best.Left;
//Kalkulationswerte
	CCtrlInfo *c_Kalk = new CCtrlInfo (&m_Kalk, 2, 2, 1, 1);
	BsdGrid.Add (c_Kalk);
	m_Kalk.Orientation = m_Best.Left;


	CCtrlInfo *c_BsdGrid = new CCtrlInfo (&BsdGrid, 3, 7, 1, 1);
	CtrlGrid.Add (c_BsdGrid);

//	CCtrlInfo *c_Check_bestand1 = new CCtrlInfo (&m_Check_bestand, 3, 7, 1, 1);
//	CtrlGrid.Add (c_Check_bestand1);
	CCtrlInfo *c_BestandGrid = new CCtrlInfo (&BestandGrid, 3, 6, 1, 1);
	CtrlGrid.Add (c_BestandGrid);

// ==== Zuordnungen =====

//Grid Zuordnungen 
	ZuordGrid.Create (this, 20, 20);
    ZuordGrid.SetBorder (12, 0);
    ZuordGrid.SetCellHeight (StdCellHeight);
    ZuordGrid.SetFontCellHeight (this, &Font);
//    ZuordGrid.SetFontCellHeight (this);
    ZuordGrid.SetGridSpace (5, 8);

// Rahmen Zuornungen
	CCtrlInfo *c_Zuordnungen = new CCtrlInfo (&m_Zuordnungen, 0, 0, 6, 7);
    c_Zuordnungen->rightspace = 30; 
    m_ZuordBottom.Create (_T(""), WS_CHILD | SS_BLACKRECT, 
		                          CRect (0, 0, 1, 1),
		                          this, IDC_ZUORDBOTTOM);
    CCtrlInfo *c_ZuordBottom = new CCtrlInfo (&m_ZuordBottom, 2, 14, 1, 1);
	c_ZuordBottom->SetCellPos (0, 10);
	ZuordGrid.Add (c_ZuordBottom);
	c_Zuordnungen->BottomControl = &m_ZuordBottom;
	ZuordGrid.Add (c_Zuordnungen);

//Artikeltyp
	CCtrlInfo *c_LAtyp = new CCtrlInfo (&m_LAtyp, 1, 1, 1, 1);
	ZuordGrid.Add (c_LAtyp);
	CCtrlInfo *c_Atyp = new CCtrlInfo (&m_Atyp, 2, 1, 1, 1);
	ZuordGrid.Add (c_Atyp);

//Mwst
	CCtrlInfo *c_LMwst = new CCtrlInfo (&m_LMwst, 1, 2, 1, 1);
	ZuordGrid.Add (c_LMwst);
	CCtrlInfo *c_Mwst = new CCtrlInfo (&m_Mwst, 2, 2, 1, 1);
	ZuordGrid.Add (c_Mwst);

//Personalrabett
	CCtrlInfo *c_PersRab = new CCtrlInfo (&m_PersRab, 3, 1, 3, 1);
	ZuordGrid.Add (c_PersRab);

//automatische Bestellung

	CCtrlInfo *c_LBestAuto2 = new CCtrlInfo (&m_LBestAuto2, 0, 0, 1, 1);
	BestAutoGrid.Add (c_LBestAuto2);
    
	CCtrlInfo *c_BestAuto2 = new CCtrlInfo (&m_BestAuto2, 1, 0, 1, 1);
	BestAutoGrid.Add (c_BestAuto2);

	CCtrlInfo *c_BestAutoGrid = new CCtrlInfo (&BestAutoGrid, 3, 2, 3, 1);
	ZuordGrid.Add (c_BestAutoGrid);

	CCtrlInfo *c_BestAuto = new CCtrlInfo (&m_BestAuto, 3, 2, 3, 1);
	ZuordGrid.Add (c_BestAuto);
	m_BestAuto.ShowWindow (SW_HIDE);

//Wareneingangskonto
	CCtrlInfo *c_LWe_kto = new CCtrlInfo (&m_LWe_kto, 1, 3, 1, 1);
	ZuordGrid.Add (c_LWe_kto);
	CCtrlInfo *c_We_kto = new CCtrlInfo (&m_We_kto, 2, 3, 1, 1);
	ZuordGrid.Add (c_We_kto);

//Intra-Stat
	CCtrlInfo *c_LIntra_stat = new CCtrlInfo (&m_LIntra_stat, 1, 4, 1, 1);
	ZuordGrid.Add (c_LIntra_stat);
	CCtrlInfo *c_Intra_stat = new CCtrlInfo (&m_Intra_stat, 2, 4, 1, 1);
	ZuordGrid.Add (c_Intra_stat);

//Erl�skonto
	CCtrlInfo *c_LErl_kto = new CCtrlInfo (&m_LErl_kto, 3, 3, 1, 1);
	ZuordGrid.Add (c_LErl_kto);
	CCtrlInfo *c_Erl_kto = new CCtrlInfo (&m_Erl_kto, 4, 3, 1, 1);
	ZuordGrid.Add (c_Erl_kto);

//2. Artikeltyp
	CCtrlInfo *c_MoreKto = new CCtrlInfo (&m_MoreKto, 5, 3, 1, 1);
	ZuordGrid.Add (c_MoreKto);

//Mehr Konten
	CCtrlInfo *c_Atyp2 = new CCtrlInfo (&m_Atyp2, 5, 1, 1, 1);
	m_Atyp2.Orientation = m_Lager.Left;
	c_Atyp2->SetCellPos (6, 0);
	ZuordGrid.Add (c_Atyp2);

// ==== Bild =====
	CCtrlInfo *c_ImageArticle = new CCtrlInfo (&m_ImageArticle, 1, 5, 2, 7);
	ZuordGrid.Add (c_ImageArticle);

//Grid Verkn�pfungen 
	LinkGrid.Create (this, 20, 20);
    LinkGrid.SetBorder (12, 0);
    LinkGrid.SetCellHeight (StdCellHeight);
    LinkGrid.SetFontCellHeight (this, &Font);
//    LinkGrid.SetFontCellHeight (this);
    LinkGrid.SetGridSpace (5, 8);

// Rahmen Verkn�pfungen
	CCtrlInfo *c_Link = new CCtrlInfo (&m_Link, 0, 0, 5, 3);
//    c_Link->rightspace = 0; 
	LinkGrid.Add (c_Link);

//Pfandartikel
	CCtrlInfo *c_LAPfa = new CCtrlInfo (&m_LAPfa, 1, 1, 1, 1);
	LinkGrid.Add (c_LAPfa);

//Grid Pfandartikel 
	APfaGrid.Create (this, 1, 2);
    APfaGrid.SetBorder (0, 0);
    APfaGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_APfa = new CCtrlInfo (&m_APfa, 0, 0, 1, 1);
	APfaGrid.Add (c_APfa);
	CtrlGrid.CreateChoiceButton (m_APfaChoice, IDC_APFACHOICE, this);
	CCtrlInfo *c_APfaChoice = new CCtrlInfo (&m_APfaChoice, 1, 0, 1, 1);
	APfaGrid.Add (c_APfaChoice);

	CCtrlInfo *c_APfaGrid = new CCtrlInfo (&APfaGrid, 2, 1, 1, 1);
	LinkGrid.Add (c_APfaGrid);

//Leergut
	CCtrlInfo *c_LALeer = new CCtrlInfo (&m_LALeer, 1, 2, 1, 1);
	LinkGrid.Add (c_LALeer);
//Grid Leergutartikel 
	ALeerGrid.Create (this, 1, 2);
    ALeerGrid.SetBorder (0, 0);
    ALeerGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_ALeer = new CCtrlInfo (&m_ALeer, 0, 0, 1, 1);
	ALeerGrid.Add (c_ALeer);
	CtrlGrid.CreateChoiceButton (m_ALeerChoice, IDC_ALEERCHOICE, this);
	CCtrlInfo *c_ALeerChoice = new CCtrlInfo (&m_ALeerChoice, 1, 0, 1, 1);
	ALeerGrid.Add (c_ALeerChoice);

	CCtrlInfo *c_ALeerGrid = new CCtrlInfo (&ALeerGrid, 2, 2, 1, 1);
	LinkGrid.Add (c_ALeerGrid);

//Leihartikel
	CCtrlInfo *c_LALeih = new CCtrlInfo (&m_LALeih, 1, 3, 1, 1);
	LinkGrid.Add (c_LALeih);
//Grid Leihgutartikel 
	ALeihGrid.Create (this, 1, 2);
    ALeihGrid.SetBorder (0, 0);
    ALeihGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_ALeih = new CCtrlInfo (&m_ALeih, 0, 0, 1, 1);
	ALeihGrid.Add (c_ALeih);
	CtrlGrid.CreateChoiceButton (m_ALeihChoice, IDC_ALEIHCHOICE, this);
	CCtrlInfo *c_ALeihChoice = new CCtrlInfo (&m_ALeihChoice, 1, 0, 1, 1);
	ALeihGrid.Add (c_ALeihChoice);

	CCtrlInfo *c_ALeihGrid = new CCtrlInfo (&ALeihGrid, 2, 3, 1, 1);
	LinkGrid.Add (c_ALeihGrid);

	CCtrlInfo *c_LinkGrid = new CCtrlInfo (&LinkGrid, 3, 5, 3, 5);
	ZuordGrid.Add (c_LinkGrid);

	CCtrlInfo *c_LCharge = new CCtrlInfo (&m_LCharge, 3, 10, 1, 1);
	ZuordGrid.Add (c_LCharge);
	CCtrlInfo *c_Charge = new CCtrlInfo (&m_Charge, 4, 10, 2, 1);
	ZuordGrid.Add (c_Charge);
	CCtrlInfo *c_ZerlEti = new CCtrlInfo (&m_ZerlEti, 4, 11, 2, 1);
	ZuordGrid.Add (c_ZerlEti);

	CCtrlInfo *c_LBereich = new CCtrlInfo (&m_LBereich, 3, 12, 1, 1);
	ZuordGrid.Add (c_LBereich);
	CCtrlInfo *c_Bereich = new CCtrlInfo (&m_Bereich, 4, 12, 2, 1);
	ZuordGrid.Add (c_Bereich);
	CCtrlInfo *c_LSais = new CCtrlInfo (&m_LSais, 3, 13, 1, 1);
	ZuordGrid.Add (c_LSais);
	CCtrlInfo *c_Sais = new CCtrlInfo (&m_Sais, 4, 13, 2, 1);
	ZuordGrid.Add (c_Sais);

	CCtrlInfo *c_ZuordGrid = new CCtrlInfo (&ZuordGrid, 1, 10, 7, 9);
	CtrlGrid.Add (c_ZuordGrid);

//Grid Einheiten 
	EinhGrid.Create (this, 20, 20);
    EinhGrid.SetBorder (12, 0);
    EinhGrid.SetCellHeight (StdCellHeight);
    EinhGrid.SetFontCellHeight (this, &Font);
//    EinhGrid.SetFontCellHeight (this);
    EinhGrid.SetGridSpace (5, 8);

// Rahmen Einheiten
	CCtrlInfo *c_GroupEinh = new CCtrlInfo (&m_GroupEinh, 0, 0, 4, 7);
//    c_Zuordnungen->rightspace = 30; 
	c_GroupEinh->BottomControl = &m_ZuordBottom;
	EinhGrid.Add (c_GroupEinh);

//Hand-Gewicht
	CCtrlInfo *c_LHndGew = new CCtrlInfo (&m_LHndGew, 1, 1, 1, 1);
	EinhGrid.Add (c_LHndGew);
	CCtrlInfo *c_HndGewHide = new CCtrlInfo (&m_HndGewHide, 1, 1, 1, 1);
	EinhGrid.Add (c_HndGewHide);
	CCtrlInfo *c_HndGew = new CCtrlInfo (&m_HndGew, 2, 1, 1, 1);
	EinhGrid.Add (c_HndGew);

//Mengeneinheit
	CCtrlInfo *c_LMeEinh = new CCtrlInfo (&m_LMeEinh, 1, 2, 1, 1);
	EinhGrid.Add (c_LMeEinh);
	CCtrlInfo *c_MeEinhHide = new CCtrlInfo (&m_MeEinhHide, 1, 2, 1, 1);
	EinhGrid.Add (c_MeEinhHide);
	CCtrlInfo *c_MeEinh = new CCtrlInfo (&m_MeEinh, 2, 2, 1, 1);
	EinhGrid.Add (c_MeEinh);

//Kundenbestelleinheit
	CCtrlInfo *c_LKunBestEinh = new CCtrlInfo (&m_LKunBestEinh, 1, 4, 1, 1);
	EinhGrid.Add (c_LKunBestEinh);
	CCtrlInfo *c_KunBestEinhHide = new CCtrlInfo (&m_KunBestEinhHide, 1, 4, 1, 1);
	EinhGrid.Add (c_KunBestEinhHide);
	CCtrlInfo *c_KunBestEinh = new CCtrlInfo (&m_KunBestEinh, 2, 4, 1, 1);
	EinhGrid.Add (c_KunBestEinh);

//Kundenbestellinhalt
	CCtrlInfo *c_LKunInh = new CCtrlInfo (&m_LKunInh, 1, 5, 1, 1);
	EinhGrid.Add (c_LKunInh);
	CCtrlInfo *c_KunInh = new CCtrlInfo (&m_KunInh, 2, 5, 1, 1);
	EinhGrid.Add (c_KunInh);

//Liefereinheit
	CCtrlInfo *c_LLiefEinh = new CCtrlInfo (&m_LLiefEinh, 1, 7, 1, 1);
	EinhGrid.Add (c_LLiefEinh);
	CCtrlInfo *c_LiefEinhHide = new CCtrlInfo (&m_LiefEinhHide, 1, 7, 1, 1);
	EinhGrid.Add (c_LiefEinhHide);
	CCtrlInfo *c_LiefEinh = new CCtrlInfo (&m_LiefEinh, 2, 7, 1, 1);
	EinhGrid.Add (c_LiefEinh);


//Grid Lieferinhalt    //LAC-115
	InhLiefGrid.Create (this, 2, 3);
    InhLiefGrid.SetBorder (0, 0);
    InhLiefGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Edit_InhLief = new CCtrlInfo (&m_InhLief, 0, 0, 1, 1);
	InhLiefGrid.Add (c_Edit_InhLief);
	CtrlGrid.CreateChoiceButton (m_InhLiefChoice, IDC_INHLIEFCHOICE, this);
	CCtrlInfo *c_InhLiefChoice = new CCtrlInfo (&m_InhLiefChoice, 1, 0, 1, 1);
	InhLiefGrid.Add (c_InhLiefChoice);


	
	
//Lieferinhalt    LAC-115 jetzt �ber InhLiefGrid
	CCtrlInfo *c_LInhLief = new CCtrlInfo (&m_LInhLief, 1, 8, 1, 1);
	EinhGrid.Add (c_LInhLief);
	CCtrlInfo *c_InhLief = new CCtrlInfo (&InhLiefGrid, 2, 8, 1, 1);
	EinhGrid.Add (c_InhLief);




//Abverkaufseinheit
	CCtrlInfo *c_LEinhAbverk = new CCtrlInfo (&m_LEinhAbverk, 1, 10, 1, 1);
	EinhGrid.Add (c_LEinhAbverk);
	CCtrlInfo *c_EinhAbverkHide = new CCtrlInfo (&m_EinhAbverkHide, 1, 10, 1, 1);
	EinhGrid.Add (c_EinhAbverkHide);
	CCtrlInfo *c_EinhAbverk = new CCtrlInfo (&m_EinhAbverk, 2, 10, 1, 1);
	EinhGrid.Add (c_EinhAbverk);

//Abverkaufsinhalt
	CCtrlInfo *c_LInhAbverk = new CCtrlInfo (&m_LInhAbverk, 1, 11, 1, 1);
	EinhGrid.Add (c_LInhAbverk);
	CCtrlInfo *c_InhAbverk = new CCtrlInfo (&m_InhAbverk, 2, 11, 1, 1);
	EinhGrid.Add (c_InhAbverk);

//Hand-Gewicht Abverkauf
	CCtrlInfo *c_LHndGewAbverk = new CCtrlInfo (&m_LHndGewAbverk, 1, 12, 1, 1);
	EinhGrid.Add (c_LHndGewAbverk);
	CCtrlInfo *c_HndGewAbverk = new CCtrlInfo (&m_HndGewAbverk, 2, 12, 1, 1);
	EinhGrid.Add (c_HndGewAbverk);

//Schwund
	CCtrlInfo *c_LSchwund = new CCtrlInfo (&m_LSchwund, 1, 13, 1, 1);
	EinhGrid.Add (c_LSchwund);
	CCtrlInfo *c_Schwund = new CCtrlInfo (&m_Schwund, 2, 13, 2, 1);
	EinhGrid.Add (c_Schwund);

	CCtrlInfo *c_EinhGrid = new CCtrlInfo (&EinhGrid, 5, 10, 7, 9);

	CtrlGrid.Add (c_EinhGrid);


	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	CtrlGrid.Display ();

	memcpy (&Sys_ben.sys_ben, &sys_ben_null, sizeof (Sys_ben.sys_ben));
	CString Msg;
	Msg.Format (_T("PersName = %s"), PersName.GetBuffer ());
//	AfxMessageBox (Msg.GetBuffer ());
	if (PersName.Trim () != "")
	{
		_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
		if (Sys_ben.dbreadfirstpers_nam () == 0)
		{
			if (Sys_ben.sys_ben.mdn != 0)
			{
				Mdn.mdn.mdn = Sys_ben.sys_ben.mdn;
				m_Mdn.SetReadOnly ();
				m_Mdn.ModifyStyle (WS_TABSTOP, 0);
			}
		}
	}

//    m_Lager.ShowWindow (SW_HIDE);
    m_Best.ShowWindow (SW_SHOWNORMAL);

//HIDE-Checkboxen deaktivieren

    m_A_bz3_hide.ShowWindow (SW_HIDE);

	strcpy (sys_par.sys_par_nam, "a_bz3_par");
	if (Sys_par.dbreadfirst () == 0)
	{
		if (!(sys_par.sys_par_wrt[0] == '1'))
		{
			m_A_bz3.ShowWindow (SW_HIDE);
			m_LMatchcode.ShowWindow (SW_HIDE);
		}
	}

	Etikett.ErfKz = _T("S");
	Etikett.A_bas = &A_bas;
	Etikett.A_hndw = &A_hndw;
	Etikett.A_eig = &A_eig;
	strcpy (sys_par.sys_par_nam, "reg_eti_par");
	if (Sys_par.dbreadfirst () == 0)
	{
		Etikett.Mode = _tstoi (sys_par.sys_par_wrt);
	}

	strcpy (sys_par.sys_par_nam, "grundartpreis");
	if (Sys_par.dbreadfirst () == 0)
	{
		grundartpreis = _tstoi (sys_par.sys_par_wrt);
	}

	ReadDefaultHide();
	sw_hide(FALSE);
	SetDefaultHide();

	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	memcpy (&Ipr.ipr, &ipr_null, sizeof (IPR));
	memcpy (&Ag.ag, &ag_null, sizeof (AG));
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = Sys_ben.sys_ben.mdn;

	Form.Show ();
	ReadMdn ();
	DisableTabControl (TRUE);
	EnableFields (FALSE);

	CString Message = _T("");
	CStrFuncs::FromClipboard (Message, CF_TEXT);
	CToken t (Message, _T("="));
//	/*** testtesttest
	/*
			CStrFuncs::ToClipboard (CString (""), CF_TEXT);
			A_bas.a_bas.a = 2;
			Form.Show ();
//			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			m_A.SetFocus ();
			Read ();
            //ProductPage4->OnInitDialog ();
//		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		PostMessage (WM_KEYDOWN, VK_CONTROL, 01);
		PostMessage (WM_KEYDOWN, VK_PRIOR, 01);
			return FALSE;
			*/
//****testtesttest  */
	if (t.GetAnzToken () > 1)
	{
		CString c = t.GetToken (0);
		if (c == _T("ArtikelNr"))
		{
			CStrFuncs::ToClipboard (CString (""), CF_TEXT);
			A_bas.a_bas.a = CStrFuncs::StrToDouble (t.GetToken (1));
			Form.Show ();
//			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			m_A.SetFocus ();
			Read ();
			return FALSE;
		}
	}

	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

HBRUSH CBasisdatenPage::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{

	if (   GetRValue(m_clrFarbe) == 0 && GetGValue(m_clrFarbe) == 0 && GetBValue(m_clrFarbe) == 0)
				m_Farbe.SetBkColor (RGB(255,255,255)); 
			else
				m_Farbe.SetBkColor (m_clrFarbe); 
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			m_MoreKto.SetBkColor (DlgBkColor);
			m_Atyp2.SetBkColor (DlgBkColor);
			m_Lager.SetBkColor (DlgBkColor);
			m_Best.SetBkColor (DlgBkColor);
			m_Kalk.SetBkColor (DlgBkColor);
			if (GetRValue(m_clrFarbe) == 0 && GetGValue(m_clrFarbe) == 0 && GetBValue(m_clrFarbe) == 0)
				m_Farbe.SetBkColor (DlgBkColor); 
			else
				m_Farbe.SetBkColor (m_clrFarbe); 

			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CBasisdatenPage::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CBasisdatenPage::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CBasisdatenPage::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CBasisdatenPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}


BOOL CBasisdatenPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) >= 0)
				{
					if (OnReturn ())
					{
						return TRUE;
					}
				}
				else
				{
					if (OnKeyup ())
					{
						return TRUE;
					}
				}
				break;
			}

			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					NextRec ();
				}
				else
				{
					LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					PriorRec ();
				}
				else
				{
					FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
//					OnAchoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_A)
				{
					OnAchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_InhLief) //LAC-115
				{
					OnInhLiefChoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Edit_grund)
				{
					OnAchoiceM_grund ();
					return TRUE;
				}
				if (GetFocus () == &m_Edit_ersatz)
				{
					OnAchoiceM_ersatz ();
					return TRUE;
				}
				if (GetFocus () == &m_Edit_ag)
				{
					OnAGchoice ();
					return TRUE;
				}
				return TRUE;
			}
	}
//    return CDbPropertyPage::PreTranslateMessage(pMsg);
	return FALSE;
}

BOOL CBasisdatenPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_A)
	{
		if (!Read ())
		{
			m_A.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_Edit_ag)
	{
		if (!ReadAG ())
		{
			m_Edit_ag.SetFocus (); 
			return FALSE;
		}
		m_Edit_ag.EnableWindow (FALSE); //FS-365 beim Rundlauf  auf AG kommen, da er sonst  (zumindest in der Neuanlage) neu gelesen wird und die Maskenvariablen �ber schreibt

	}

/*
	if (Control == &m_Check_bestand)
	{
		int checked = m_Check_bestand.GetCheck ();
		if (checked == BST_CHECKED)
		{
			m_Lager.SetFocus ();
			return TRUE;
		}
	}

	if (Control == &m_Lager)
	{
		m_Combo_stueckl.SetFocus ();
		return TRUE;
	}
*/

/*
	if (Control == &m_Erl_kto)
	{
		m_MoreKto.SetFocus ();
		return TRUE;
	}

	if (Control == &m_MoreKto)
	{
		m_APfa.SetFocus ();
		return TRUE;
	}
*/

	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}

BOOL CBasisdatenPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control == &m_MoreKto)
	{
		m_Erl_kto.SetFocus ();
		return TRUE;
	}

/*
	if (Control == &m_APfa)
	{
		m_MoreKto.SetFocus ();
		return TRUE;
	}
*/

	Control = GetNextDlgTabItem (Control, TRUE);
	if (Control != NULL)
	{
		Control->SetFocus ();
	}
	return TRUE;
}


BOOL CBasisdatenPage::ReadMdn ()
{
	int dsqlstatus = 0;

	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();

	if (Mdn.mdn.mdn == 0)
	{
		_tcscpy (MdnAdr.adr.adr_krz, _T("Zentrale"));
		Form.Show ();
		return TRUE;
	}
	
	dsqlstatus = Mdn.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
//		m_Mdn.SetFocus ();
//		m_Mdn.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden (sqlstatus %d) "),Mdn.mdn.mdn, dsqlstatus);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CBasisdatenPage::Read ()
{
	extern short sql_mode;
	a_krz_gen = 0l;
	ModalChoice = TRUE;
	memcpy (&Ag.ag, &ag_null, sizeof (AG));
	if (ModalChoice)
	{
		CString cA;
		m_A.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnAchoice ();
			SearchA = "";
			return TRUE;
		}
	}

//	LockWindowUpdate ();
	a_typ_pos = 1;
	memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
	memcpy (&A_bas_erw.a_bas_erw, &a_bas_erw_null, sizeof (A_BAS_ERW));
	memcpy (&A_hndw.a_hndw, &a_hndw_null, sizeof (A_HNDW));
	memcpy (&A_eig.a_eig, &a_eig_null, sizeof (A_EIG));
	memcpy (&A_pfa.a_pfa, &a_pfa_null, sizeof (A_PFA));
	memcpy (&A_leer.a_leer, &a_leer_null, sizeof (A_LEER));
	memcpy (&A_krz.a_krz, &a_krz_null, sizeof (A_KRZ));
	memcpy (&A_ean.a_ean, &a_ean_null, sizeof (A_EAN));
	memcpy (&A_emb.a_emb, &a_emb_null, sizeof (A_EMB));
	memcpy (&A_varb.a_varb, &a_varb_null, sizeof (A_VARB));
	memcpy (&A_zut.a_zut, &a_zut_null, sizeof (A_ZUT));
	memcpy (&A_huel.a_huel,&a_huel_null, sizeof (A_HUEL));
	memcpy (&A_mat.a_mat,  &a_mat_null, sizeof (A_MAT));
	memcpy (&A_grund.a_grund,  &a_grund_null, sizeof (A_GRUND));
	m_ImageArticle.Show (_T(""));

	Keys.Get ();
	if (A_bas.a_bas.a == 0.0)
	{
			MessageBox (_T("Artikel 0 kann nicht angelegt werden"), NULL, 
						MB_OK | MB_ICONERROR);
			m_A.SetFocus ();
			m_A.SetSel (0, -1);
//  			UnlockWindowUpdate ();
            return FALSE;
	}

// FS-337 A 
	/**
 		NeuerArtikel = FALSE;
		HoleArtikel(A_bas.a_bas.a);
		if (ArtikelGesperrt)
		{
			MessageBox (_T("Der Artikel wird von einem anderen Benutzer bearbeitet"), NULL, 
						MB_OK | MB_ICONERROR);
			m_A.SetFocus ();
			m_A.SetSel (0, -1);
//  			UnlockWindowUpdate ();
            return FALSE;
		}
		**/
// FS-337 E  


	if (A_bas.dbreadfirst () == 0)
	{
		NeuerArtikel = FALSE;
		if (A_bas.a_bas.delstatus != 0)
		{
			MessageBox (_T("Der Artikel ist zum L�schen gekennzeichnet"), NULL, 
						MB_OK | MB_ICONERROR);
			m_A.SetFocus ();
			m_A.SetSel (0, -1);
//  			UnlockWindowUpdate ();
            return FALSE;
		}
		memcpy (&a_bas, &A_bas.a_bas, sizeof (A_BAS));
		if (!TestType ())
		{
			if (TypeMode == Message)
			{
				MessageBox (_T("Falscher Artikeltyp"), NULL, MB_ICONERROR);
				m_A.SetFocus ();
				m_A.SetSel (0, -1);
//  				UnlockWindowUpdate ();
			}
			else
			{
				ActivateTypePage ();
				A_bas.a_bas.a = 0.0;
				Form.Show ();
			}
			return FALSE;
		}
		A_bas.beginwork (); //WAL-130
		LockArtikel (A_bas.a_bas.a); //WAL-130
		if (ArtikelGesperrt) A_bas.commitwork (); //WAL-130

		CWnd *mFrame = Frame->GetParent (); 
		NoRecChange = TRUE;
//		mFrame->SendMessage (WM_COMMAND, RECCHANGE, 0l);
		NoRecChange = FALSE;

		if (strcmp ((LPSTR) A_bas.a_bas.hbk_kz, " ") == 0) 
		{
			strcpy ((LPSTR) A_bas.a_bas.hbk_kz, "K");
		}



		Ag.ag.ag = A_bas.a_bas.ag;
		Ag.dbreadfirst ();
	    HoleAG(Ag.ag.ag); 

		A_ean.a_ean.a = A_bas.a_bas.a;

		A_bas_erw.a_bas_erw.a = A_bas.a_bas.a;
		if (A_bas_erw.dbreadfirst () != 0) //FS-148
		{
			_tcscpy (A_bas_erw.a_bas_erw.pp_a_bz1,A_bas.a_bas.a_bz1);
			_tcscpy (A_bas_erw.a_bas_erw.pp_a_bz2,A_bas.a_bas.a_bz2);
			CString Date;
			CStrFuncs::SysDate (Date);
			A_bas_erw.ToDbDate (Date, &A_bas_erw.a_bas_erw.shop_neu_bis);
		}
		else
		{
	//		_tcscpy (A_bas_erw.a_bas_erw.pp_a_bz1,A_bas.a_bas.a_bz1);
	//		_tcscpy (A_bas_erw.a_bas_erw.pp_a_bz2,A_bas.a_bas.a_bz2);
		}
		if (A_bas.a_bas.intra_stat > 0) //231114
		{
			if (A_bas.a_bas.intra_stat <= 9999999)
			{
				sprintf (A_bas_erw.a_bas_erw.zolltarifnr, "0%ld", A_bas.a_bas.intra_stat);
			}
			else
			{
				sprintf (A_bas_erw.a_bas_erw.zolltarifnr, "%ld", A_bas.a_bas.intra_stat);
			}

		}


		if (a_typ == Hndw && PageType == Standard)
		{
			A_hndw.a_hndw.mdn = A_bas.a_bas.mdn;
			A_hndw.a_hndw.fil = A_bas.a_bas.fil;
			A_hndw.a_hndw.a = A_bas.a_bas.a;
			if (A_hndw.dbreadfirst () != 0)
			{
				A_hndw.a_hndw.verk_beg.year = 1980;
				A_hndw.a_hndw.verk_beg.month = 1;
				A_hndw.a_hndw.verk_beg.day = 1;
				if (A_hndw.a_hndw.a_krz != 0l)
				{
					A_krz.a_krz.a_krz = A_hndw.a_hndw.a_krz;
					A_krz.dbreadfirst ();
				}
			}
		}
//		else if (A_bas.a_bas.a_typ == Eig && PageType == Standard)
		else if (a_typ == Eig && PageType == Standard)
		{
			A_eig.a_eig.mdn = A_bas.a_bas.mdn;
			A_eig.a_eig.fil = A_bas.a_bas.fil;
			A_eig.a_eig.a = A_bas.a_bas.a;
			if (A_eig.dbreadfirst () != 0)
			{
				A_eig.a_eig.verk_beg.year = 1980;
				A_eig.a_eig.verk_beg.month = 1;
				A_eig.a_eig.verk_beg.day = 1;
				if (A_eig.a_eig.a_krz != 0l)
				{
					A_krz.a_krz.a_krz = A_eig.a_eig.a_krz;
					A_krz.dbreadfirst ();
				}
			}
			A_hndw.a_hndw.a_krz       = A_eig.a_eig.a_krz;
			A_hndw.a_hndw.me_einh_kun = A_eig.a_eig.me_einh_ek;
			A_hndw.a_hndw.gew_bto = A_eig.a_eig.gew_bto;
			A_hndw.a_hndw.inh = A_eig.a_eig.inh;
			A_hndw.a_hndw.sg1  = A_eig.a_eig.sg1;
			A_hndw.a_hndw.sg2  = A_eig.a_eig.sg2;
			A_hndw.a_hndw.tara  = A_eig.a_eig.tara;
			_tcscpy (A_hndw.a_hndw.verk_art, A_eig.a_eig.verk_art);
			_tcscpy (A_hndw.a_hndw.mwst_ueb, A_eig.a_eig.mwst_ueb);
			_tcscpy (A_hndw.a_hndw.pr_ausz, A_eig.a_eig.pr_ausz);
			_tcscpy (A_hndw.a_hndw.pr_man, A_eig.a_eig.pr_man);
			_tcscpy (A_hndw.a_hndw.pr_ueb, A_eig.a_eig.pr_ueb);
			_tcscpy (A_hndw.a_hndw.reg_eti, A_eig.a_eig.reg_eti);
			_tcscpy (A_hndw.a_hndw.theke_eti, A_eig.a_eig.theke_eti);
			_tcscpy (A_hndw.a_hndw.waren_eti, A_eig.a_eig.waren_eti);
			A_hndw.a_hndw.anz_waren_eti  = A_eig.a_eig.anz_waren_eti;
			A_hndw.a_hndw.anz_reg_eti  = A_eig.a_eig.anz_reg_eti;
			A_hndw.a_hndw.anz_theke_eti  = A_eig.a_eig.anz_theke_eti;
		}
		else if (a_typ == Pfa && PageType == Standard)
		{
			A_pfa.a_pfa.mdn = A_bas.a_bas.mdn;
			A_pfa.a_pfa.fil = A_bas.a_bas.fil;
			A_pfa.a_pfa.a = A_bas.a_bas.a;
			if (A_pfa.dbreadfirst () != 0)
			{
				A_pfa.a_pfa.verk_beg.year = 1980;
				A_pfa.a_pfa.verk_beg.month = 1;
				A_pfa.a_pfa.verk_beg.day = 1;
				if (A_pfa.a_pfa.a_krz != 0l)
				{
					A_krz.a_krz.a_krz = A_pfa.a_pfa.a_krz;
					A_krz.dbreadfirst ();
				}
			}
			A_hndw.a_hndw.a_krz       = A_pfa.a_pfa.a_krz;
			A_hndw.a_hndw.sg1  = A_pfa.a_pfa.sg1;
			A_hndw.a_hndw.sg2  = A_pfa.a_pfa.sg2;
			_tcscpy (A_hndw.a_hndw.verk_art, A_pfa.a_pfa.verk_art);
			_tcscpy (A_hndw.a_hndw.mwst_ueb, A_pfa.a_pfa.mwst_ueb);
		}
		else if (a_typ == Leer && PageType == Standard)
		{
			A_leer.a_leer.mdn = A_bas.a_bas.mdn;
			A_leer.a_leer.fil = A_bas.a_bas.fil;
			A_leer.a_leer.a = A_bas.a_bas.a;
			if (A_leer.dbreadfirst () != 0)
			{
				A_leer.a_leer.verk_beg.year = 1980;
				A_leer.a_leer.verk_beg.month = 1;
				A_leer.a_leer.verk_beg.day = 1;
				if (A_leer.a_leer.a_krz != 0l)
				{
					A_krz.a_krz.a_krz = A_leer.a_leer.a_krz;
					A_krz.dbreadfirst ();
				}
			}
			A_hndw.a_hndw.a_krz       = A_leer.a_leer.a_krz;
			A_hndw.a_hndw.sg1  = A_leer.a_leer.sg1;
			A_hndw.a_hndw.sg2  = A_leer.a_leer.sg2;
			A_hndw.a_hndw.gew_bto  = A_leer.a_leer.br_gew; //FS-302
			_tcscpy (A_hndw.a_hndw.verk_art, A_leer.a_leer.verk_art);
			_tcscpy (A_hndw.a_hndw.mwst_ueb, A_leer.a_leer.mwst_ueb);
		}
//		else if (A_bas.a_bas.a_typ == Varb && PageType == Material)
		else if (a_typ == Varb && PageType == Material)
		{
			A_varb.a_varb.mdn = A_bas.a_bas.mdn;
			A_varb.a_varb.fil = A_bas.a_bas.fil;
			A_varb.a_varb.a = A_bas.a_bas.a;
			A_varb.dbreadfirst ();
			A_mat.a_mat.a = A_bas.a_bas.a;
			A_mat.dbreadfirsta ();
			A_varb.a_varb.mat = A_mat.a_mat.mat;
		}
//		else if (A_bas.a_bas.a_typ == Zut && PageType == Material)
		else if (a_typ == Zut && PageType == Material)
		{
			A_zut.a_zut.mdn = A_bas.a_bas.mdn;
			A_zut.a_zut.fil = A_bas.a_bas.fil;
			A_zut.a_zut.a = A_bas.a_bas.a;
			A_zut.dbreadfirst ();
			A_mat.a_mat.a = A_bas.a_bas.a;
			A_mat.dbreadfirsta ();
		}
//		else if (A_bas.a_bas.a_typ == Huel && PageType == Material)
		else if (a_typ == Huel && PageType == Material)
		{
			A_huel.a_huel.mdn = A_bas.a_bas.mdn;
			A_huel.a_huel.fil = A_bas.a_bas.fil;
			A_huel.a_huel.a = A_bas.a_bas.a;
			A_huel.dbreadfirst ();
			A_mat.a_mat.a = A_bas.a_bas.a;
			A_mat.dbreadfirsta ();
		}
		_tcscpy (m_VerkArt, A_hndw.a_hndw.verk_art);
		InsA_bas_Prot (1);

		if (ProductPage1Read == TRUE)
		{
			ProductPage1->Read ();
		}
		if (ProductPage2Read == TRUE)
		{
			ProductPage2->Read ();
		}
		if (ProductPage3Read == TRUE)
		{
			ProductPage3->Read ();
		}
		if (ProductPage4Read == TRUE)
		{
			ProductPage4->Read ();
		}
		if (ProductPage5Read == TRUE)
		{
			ProductPage5->Read ();
		}
		if (ProductPage6Read == TRUE)
		{
			ProductPage6->Read ();
		}
		if (A_bas.a_bas.a < 0.0)
		{
			sw_hide (TRUE);
			EnableFields (TRUE);
		} 
		else 
		{
			sw_hide (FALSE);
			EnableFields (TRUE);
		}
		currentAg = A_bas.a_bas.ag;

		FillInhLief (); //LAC-138
		A_grund.a_grund.a = A_bas.a_bas.a_grund;
//FS-337		if (A_grund.dbreadfirst () == 0)

		Form.Show ();
		if (!ArtikelGesperrt) Update->Show (); 
		if (ShowImage)
		{
			m_ImageArticle.Show (A_bas.a_bas.bild);
			m_ImageArticle.Invalidate ();
		}
//		m_A.SetFocus ();
//		m_A.SetSel (0, -1);
	    DisableTabControl (FALSE);
    	ModalChoice = FALSE;
		EnableLinks ();
 		NeuerArtikel = FALSE;
		HoleArtikel(A_bas.a_bas.a);
		if (ArtikelGesperrt)
		{
		    DisableTabControl (TRUE);
			EnableFields (FALSE);

			Form.Show();
			MessageBox (_T("Der Artikel wird von einem anderen Benutzer bearbeitet"), NULL, 
						MB_OK | MB_ICONERROR);
			m_A.SetFocus ();
			m_A.SetSel (0, -1);
//  			UnlockWindowUpdate ();
            return FALSE;
		}
		if (GrundArtikelGesperrt == FALSE)
		{
			if ( (A_grund.a_grund.rgb_r >= 0 && A_grund.a_grund.rgb_r <= 255) ||
				(A_grund.a_grund.rgb_g >= 0 && A_grund.a_grund.rgb_g <= 255) ||	
				(A_grund.a_grund.rgb_b >= 0 && A_grund.a_grund.rgb_b <= 255) )
			{
				m_clrFarbe = RGB (A_grund.a_grund.rgb_r,A_grund.a_grund.rgb_g,A_grund.a_grund.rgb_b);
			}
			else
			{
				m_clrFarbe = DlgBkColor;
			}
			m_Farbe.SetWindowText (_T("Auswahl"));
			m_Farbe.EnableWindow (TRUE);

		}
		else
		{
			m_clrFarbe = DlgBkColor;
			m_Farbe.SetWindowText (_T("Gesperrt!"));
			m_Farbe.EnableWindow (FALSE);
		}
		Invalidate ();

//		m_A_bz1.SetFocus ();
		theApp.SetABas (&A_bas);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Artikel %.0lf wird neu angelegt"),A_bas.a_bas.a);
		NeuerArtikel = TRUE;
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONINFORMATION);
		double da = A_bas.a_bas.a;
		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas.a_bas.mdn = Mdn.mdn.mdn;
		A_bas.a_bas.a = da;
		A_bas_erw.a_bas_erw.a = A_bas.a_bas.a; //LAC-100
		if (A_bas.a_bas.a < 0.0)
		{
			sw_hide (TRUE);
		} 
		else 
		{
			sw_hide (FALSE);
		    DisableTabControl (FALSE);
			EnableFields (TRUE);
			m_A.EnableWindow (FALSE); //FS-365 beim Rundlauf darf der Cursor nicht mehr auf Artikel kommen, da er sonds neu gelesen wird und die Maskenvariablen �ber schreibt

//			HeadControls.Enable (FALSE);
//			PosControls.Enable (TRUE);
		}
		if (A_bas.a_bas.a < 100000000)
		{
			A_mat.a_mat.mat = (long) A_bas.a_bas.a;
		}
		if (ProductPage4 != NULL)
		{
			ProductPage4->Read ();
		}
	    DisableTabControl (FALSE);
		CString Time;
		CString Date;
//		CStrFuncs::SysTime (Time);
//    	strcpy (Prg_prot.prg_prot.zeit, Time.GetBuffer (8));
		CStrFuncs::SysDate (Date);
		A_bas.ToDbDate (Date, &A_bas.a_bas.bearb);
		A_bas.ToDbDate (Date, &A_bas.a_bas.akv);
 		if (NeuerArtikel == TRUE) //LAC-168
		{
			CString Date;
			CStrFuncs::SysDate (Date);
			A_bas_erw.ToDbDate (Date, &A_bas_erw.a_bas_erw.shop_neu_bis);
			DbTime t;
			long ldat = t.ParseLong(&A_bas_erw.a_bas_erw.shop_neu_bis); 
			ldat +=NeuerArtikelAnzTage;
			t.ToDateStruct (&A_bas_erw.a_bas_erw.shop_neu_bis,ldat);
		}

 	    Form.Show ();
		EnableLinks ();
		m_A.SetFocus ();
		m_A.SetSel (0, -1);
//  	    UnlockWindowUpdate ();
		theApp.SetABas (&A_bas);
		return TRUE;
	}

	return FALSE;
}

BOOL CBasisdatenPage::TestGroups ()
{
	if (A_bas.a_bas.a == -1) return TRUE;
	Form.Get ();
	Ag.ag.ag = A_bas.a_bas.ag;
	if (Ag.ag.ag == 0)
	{
		CString Error;
		Error.Format (_T("Artikelgruppe darf nicht 0 sein"));
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONINFORMATION);
		m_Edit_ag.SetFocus ();
		m_Edit_ag.SetSel (0, -1);
		return FALSE;
	}
	int dsqlstatus = Ag.dbreadfirst () != 0;
	if (dsqlstatus)
	{
		CString Error;
		Error.Format (_T("Artikelgruppe %d ist nicht vorhanden"),Ag.ag.ag);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONINFORMATION);
		m_Edit_ag.SetFocus ();
		m_Edit_ag.SetSel (0, -1);
		return FALSE;
	}
	if (A_bas.a_bas.wg == 0 || A_bas.a_bas.hwg == 0)
	{
		CString Error;
		Error.Format (_T("Warengruppe oder Hauptwarengruppe d�rfen nicht 0 sein.\n")
			          _T("Artikelgruppe eingeben und mit <ENTER> best�tigen"),Ag.ag.ag);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONINFORMATION);
		m_Edit_ag.SetFocus ();
		m_Edit_ag.SetSel (0, -1);
		return FALSE;
	}
	return TRUE;
}



BOOL CBasisdatenPage::ReadAG ()
{
	ModalChoice = TRUE;
	if (ModalChoice)
	{
		CString cA;
		m_Edit_ag.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchAG = cA;
			OnAGchoice ();
			SearchAG = "";
			if (!AChoiceStatM)
			{
				m_Edit_ag.SetFocus ();
				m_Edit_ag.SetSel (0, -1);
				ModalChoice = FALSE;
				return FALSE;
			}
		}
	}

	Form.Get ();
	Ag.ag.ag = A_bas.a_bas.ag;
	if (Ag.ag.ag == 0)
	{
		CString Error;
		Error.Format (_T("Artikelgruppe darf nicht 0 sein"));
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONINFORMATION);
		m_Edit_ag.SetFocus ();
		m_Edit_ag.SetSel (0, -1);
		return FALSE;
	}
	int dsqlstatus = Ag.dbreadfirst () != 0;
	if (dsqlstatus)
	{
		CString Error;
		Error.Format (_T("Artikelgruppe %d ist nicht vorhanden"),Ag.ag.ag);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONINFORMATION);
		m_Edit_ag.SetFocus ();
		m_Edit_ag.SetSel (0, -1);
		return FALSE;
	}
	if (!TestAgType ())
	{
		if (AgRestriktion == Verbieten) //FS-18
		{
			CString Error;
			Error.Format (_T("Falscher Artikeltyp %hd"),Ag.ag.a_typ);
			MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONINFORMATION);
			m_Edit_ag.SetFocus ();
			m_Edit_ag.SetSel (0, -1);
			return FALSE;
		}
		if (AgRestriktion == Warnung) //FS-18
		{
			int ret = MessageBox (_T("Artikelgruppe hat den falschen Artikeltyp, trotzdem verwenden ?"), NULL, MB_YESNO | MB_ICONQUESTION);
			if (ret == IDNO)
			{
				m_Edit_ag.SetFocus ();
				m_Edit_ag.SetSel (0, -1);
				return FALSE;
			}
		}

	}
	if (currentAg != A_bas.a_bas.ag)
	{
//FS-295	A_bas.a_bas.wg = 0;  macht nur Sinn, wenn immer wieder bei Wechsel der AG dei Defaultwerden geholt werden sollen , dies darf aber nur bei Neuanlage passieren !! 
	}
    HoleAG(Ag.ag.ag);
	currentAg = A_bas.a_bas.ag;
	Form.Show ();
	Update->Show (); 
    EnableLinks ();
	return TRUE;
}


BOOL CBasisdatenPage::Read_a_grund ()
{
	ModalChoice = TRUE;
	if (ModalChoice)
	{
		CString cA;
		m_Edit_grund.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnAchoiceM_grund ();
			SearchA = "";
			if (!AChoiceStatM)
			{
				m_Edit_grund.SetFocus ();
				m_Edit_grund.SetSel (0, -1);
				ModalChoice = FALSE;
				return FALSE;
			}
		}
	}

	memcpy (&a_bas, &A_bas.a_bas, sizeof (A_BAS));
//WAL-130	Form.Get ();
	A_bas.a_bas.a = A_bas.a_bas.a_grund;
	int dsqlstatus = A_bas.dbreadfirst () != 0;
	a_bas.a_grund = A_bas.a_bas.a;
	memcpy (&A_bas.a_bas, &a_bas, sizeof (A_BAS));
//WAL-130	A_bas.a_bas.a_grund = a_bas.a_grund ;
	if (dsqlstatus && A_bas.a_bas.a_grund != 0.0)
	{
		CString Error;
		Error.Format (_T("Artikel %.0lf ist nicht vorhanden"),A_bas.a_bas.a_grund);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONINFORMATION);
		return FALSE;
	}
	return TRUE;

}

BOOL CBasisdatenPage::Read_a_ersatz ()
{
	ModalChoice = TRUE;
	if (ModalChoice)
	{
		CString cA;
		m_Edit_ersatz.GetWindowText (cA);
		if (!CStrFuncs::IsDecimal (cA))
		{
			SearchA = cA;
			OnAchoiceM_ersatz ();
			SearchA = "";
			if (!AChoiceStatM)
			{
				m_Edit_ersatz.SetFocus ();
				m_Edit_ersatz.SetSel (0, -1);
				ModalChoice = FALSE;
				return FALSE;
			}
		}
	}

	memcpy (&a_bas, &A_bas.a_bas, sizeof (A_BAS));
	Form.Get ();
	A_bas.a_bas.a = A_bas.a_bas.a_ersatz;
	int dsqlstatus = A_bas.dbreadfirst () != 0;
	a_bas.a_ersatz = A_bas.a_bas.a;
	memcpy (&A_bas.a_bas, &a_bas, sizeof (A_BAS));
	A_bas.a_bas.a_ersatz = a_bas.a_ersatz ;
	if (a_ersatz_nicht_pruefen == TRUE) return TRUE;
	if (dsqlstatus && A_bas.a_bas.a_ersatz != 0.0)
	{
		CString Error;
		Error.Format (_T("Artikel %.0lf ist nicht vorhanden"),A_bas.a_bas.a_ersatz);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONINFORMATION);
		return FALSE;
	}
	return TRUE;
}

BOOL CBasisdatenPage::IsChanged (CPreise *pIpr)
{
	DbRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ipr.ipr, &pIpr->ipr, sizeof (IPR));
		if (Ipr == ipr->ipr) break;
	} 	
	if (ipr == NULL)
	{
		return TRUE;
	}
	if (pIpr->cEk != ipr->cEk) return TRUE;
	if (pIpr->cVk != ipr->cVk) return TRUE;
	return FALSE;
}

BOOL CBasisdatenPage::InList (IPR_CLASS& Ipr)
{
	ListRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Ipr == ipr->ipr) return TRUE;
	}
    return FALSE;
}

void CBasisdatenPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ipr.ipr, &ipr->ipr, sizeof (IPR));
		if (!InList (Ipr))
		{
			Ipr.dbdelete ();
			PgrProt.Write (1);
		}
	}
}

BOOL CBasisdatenPage::WriteIpr () //240412
{
	int dsqlstatus = 0;
	double artikel;
	double grundartikel;
	if ((grundartpreis == 1 && A_bas.a_bas.me_einh != 2) || grundartpreis > 1)  
	{
		if (A_bas.a_bas.a == -1)
		{
			// Alle Grundartikel lesen a == a_grund , und pr�fen, ob in ipr vorhanden
			if (AGrundCursor == -1)
			{
				A_bas.sqlout ((double *) &grundartikel, SQLDOUBLE, 0);
				GrundCursor = Sais.sqlcursor (_T("select a from a_bas where a = a_grund and a_grund in (select a_grund from a_bas where a <> a_grund)"));
				if (grundartpreis == 1)
				{
					A_bas.sqlin ((double *) &grundartikel, SQLDOUBLE, 0);
					A_bas.sqlout ((double *) &artikel, SQLDOUBLE, 0);
					AGrundCursor = Sais.sqlcursor (_T("select a from a_bas where a <> a_grund and a = ? and me_einh <> 2"));
				}
				else
				{
					A_bas.sqlin ((double *) &grundartikel, SQLDOUBLE, 0);
					A_bas.sqlout ((double *) &artikel, SQLDOUBLE, 0);
					AGrundCursor = Sais.sqlcursor (_T("select a from a_bas where a <> a_grund and a_grund = ?"));
				}

			}
			A_bas.sqlopen (GrundCursor);
			while (A_bas.sqlfetch (GrundCursor) == 0)
			{
				A_bas.sqlopen (AGrundCursor);
				while (A_bas.sqlfetch (AGrundCursor) == 0)
				{
					Ipr.ipr.a = grundartikel;
					int dsqlstatus = Ipr.dbreadafirst ();
					while (dsqlstatus == 0)
					{
						Ipr.ipr.a = artikel;
						Ipr.dbupdate ();
						Ipr.ipr.a = A_bas.a_bas.a_grund;
						dsqlstatus = Ipr.dbreada ();
					}
				}
			}
		}
	}
	return dsqlstatus;
}

BOOL CBasisdatenPage::Write ()
{
	//ZUTUN speichern
	extern short sql_mode; 
	short sql_s;

	int dsqlstatus ; 
	//FS-337 A
	if (A_bas.a_bas.a == 0.0) return FALSE;
	if (m_Mdn.IsWindowEnabled ())  return FALSE ;  //LAC-176  in dem Fall nocht nochmal speichern, da Artikel nicht mehr gesperrt ist
	

	if (ArtikelGesperrt) 
	{
			MessageBox (_T("Der Artikel wird von einem anderen Benutzer bearbeitet"), NULL, 
						MB_OK | MB_ICONERROR);
			m_A.SetFocus ();
			m_A.SetSel (0, -1);
            return FALSE;
	}
	//FS-337 E


	CString Modif = _T("B");
	char verk_art[2];
	sql_s = sql_mode;
//	sql_mode = 1;
	Form.Get ();
	CString Date;
	CStrFuncs::SysDate (Date); 
	A_bas.ToDbDate (Date, &A_bas.a_bas.bearb);
	if (A_bas.a_bas.stand.day == 0)
	{
		A_bas.ToDbDate (Date, &A_bas.a_bas.stand);
	}

	if (A_bas.a_bas.a == -1) WriteDefaultHide();

	if (!TestGroups ()) return FALSE;

	//WAL-130 HoleGrundArtikel(A_bas.a_bas.a_grund); //FS-337
	A_grund.a_grund.a = A_bas.a_bas.a_grund;
    extern short sql_mode;
    sql_mode = 1; //FS-364
	dsqlstatus = A_grund.dblock () ; //WAL-130
	sql_mode = 0;
	if (dsqlstatus == 0) //FS-337
	{
		A_grund.dbupdate();
	}
	if (A_bas.a_bas.a == -1)
	{
		//  zur Sicherheit deaktiviert (bie Lackmann war sys_par grundartpreis aktiviert, mit fatalem ergebnis     WriteIpr();
	}
	CString ImageFile = m_ImageArticle.GetImageFile ();
	if (ImageFile != "")
	{
		CArtImages ArtImages;
		ArtImages.CopyToServer (ImageFile);
//		strcpy (A_bas.a_bas.bild, ImageFile.GetBuffer ());
		strcpy (A_bas.a_bas.bild, ArtImages.ServerPath.GetBuffer ());
	}

	if ((grundartpreis == 1 && A_bas.a_bas.me_einh != 2) || grundartpreis > 1)
	{
		if (A_bas.a_bas.a != A_bas.a_bas.a_grund)
		{

			Ipr.ipr.a = A_bas.a_bas.a_grund;
			int dsqlstatus = Ipr.dbreadafirst ();
			while (dsqlstatus == 0)
			{
				Ipr.ipr.a = A_bas.a_bas.a;
				Ipr.dbupdate ();
				Ipr.ipr.a = A_bas.a_bas.a_grund;
				dsqlstatus = Ipr.dbreada ();
			}
		}
	}
	if (a_typ == Hndw)
	{
		strcpy (A_hndw.a_hndw.verk_art, "");
		A_hndw.a_hndw.a = A_bas.a_bas.a;
		A_hndw.dbreadfirst ();
		strcpy (verk_art, A_hndw.a_hndw.verk_art);
	}
	else if (a_typ == Eig)
	{
		strcpy (A_eig.a_eig.verk_art, "");
		A_eig.a_eig.a = A_bas.a_bas.a;
		A_eig.dbreadfirst ();
		strcpy (verk_art,A_eig.a_eig.verk_art);
	}
	else if (a_typ == Pfa)
	{
		strcpy (A_pfa.a_pfa.verk_art, "");
		A_pfa.dbreadfirst ();
		strcpy (verk_art,A_pfa.a_pfa.verk_art);
	}
	else if (a_typ == Leer)
	{
		strcpy (A_leer.a_leer.verk_art, "");
		A_leer.dbreadfirst ();
		strcpy (verk_art,A_leer.a_leer.verk_art);
	}

	Form.Get ();
	Update->Get (); 
	if (A_bas.a_bas.a == -1) WriteDefaultHide();

	if ((strcmp (m_VerkArt, "R") != 0) && (strcmp (A_hndw.a_hndw.verk_art, "R") == 0))
	{
		Modif ="L";
	}

	if (a_typ_pos == 1)
	{
		A_bas.a_bas.a_typ = a_typ;
	}
	if (a_typ_pos == 2)
	{
		A_bas.a_bas.a_typ2 = a_typ;
	}
	if (!m_EinhAbverk.IsWindowVisible () && A_bas.a_bas.a != -1) 
	{
		A_bas.a_bas.me_einh_abverk = A_bas.a_bas.me_einh;
		strcpy ((LPSTR)A_bas.a_bas.hnd_gew_abverk, (LPSTR) A_bas.a_bas.hnd_gew);
		A_bas.a_bas.inh_abverk = 1.0;
	}
	strcpy (A_bas.a_bas.pers_nam, Sys_ben.sys_ben.pers_nam);
	A_bas_erw.dbupdate (); //FS-148   WAL-108  update auf a_bas_erw muss vor! dem update auf a_bas erfolgen,  wg. Trigger ti_a_bas und tu_a_bas
	A_bas.a_bas.intra_stat = atoi (A_bas_erw.a_bas_erw.zolltarifnr); //231114
	dsqlstatus = A_bas.dbupdate ();

	//FS-167
	if (A_bas_erw.a_bas_erw.tara2 > 0.0 || A_bas_erw.a_bas_erw.tara3 > 0.0  || A_bas_erw.a_bas_erw.tara4 > 0.0)
	{
		A_hndw.a_hndw.tara = A_bas_erw.a_bas_erw.tara2 + A_bas_erw.a_bas_erw.tara3 + A_bas_erw.a_bas_erw.tara4;
	}


	memcpy (&A_prot.a_prot, &a_prot_null, sizeof (A_PROT));
	A_prot.a_prot.a = A_bas.a_bas.a;
	strcpy (A_prot.a_prot.pers_nam, A_bas.a_bas.pers_nam);
	A_prot.a_prot.bearb = (DATE_STRUCT ) DbTime ();
	A_prot.dbupdate ();
//ZuTun	Ipr.Schreibegrund (A_bas.a_bas.a);
//	if (A_bas.a_bas.a_typ == Hndw || A_bas.a_bas.a == -1)
	if (a_typ == Hndw || A_bas.a_bas.a == -1)
	{
		if (A_hndw.a_hndw.verk_beg.year == 0)
		{
			A_hndw.a_hndw.verk_beg.year = 1980;
			A_hndw.a_hndw.verk_beg.month = 1;
			A_hndw.a_hndw.verk_beg.day = 1;
		}
	}
//	else if (A_bas.a_bas.a_typ == Eig)
	else if (a_typ == Eig)
	{
		if (A_eig.a_eig.verk_beg.year == 0)
		{
			A_eig.a_eig.verk_beg.year = 1980;
			A_eig.a_eig.verk_beg.month = 1;
			A_eig.a_eig.verk_beg.day = 1;
		}
	}
	else if (a_typ == Pfa)
	{
		if (A_pfa.a_pfa.verk_beg.year == 0)
		{
			A_pfa.a_pfa.verk_beg.year = 1980;
			A_pfa.a_pfa.verk_beg.month = 1;
			A_pfa.a_pfa.verk_beg.day = 1;
		}
	}
	else if (a_typ == Leer)
	{
		if (A_leer.a_leer.verk_beg.year == 0)
		{
			A_leer.a_leer.verk_beg.year = 1980;
			A_leer.a_leer.verk_beg.month = 1;
			A_leer.a_leer.verk_beg.day = 1;
		}
	}
//	if (A_bas.a_bas.a_typ == Hndw || A_bas.a_bas.a == -1)
	if (a_typ == Hndw || A_bas.a_bas.a == -1)
	{
		A_hndw.a_hndw.mdn = A_bas.a_bas.mdn;
		A_hndw.a_hndw.fil = A_bas.a_bas.fil;
		A_hndw.a_hndw.a = A_bas.a_bas.a;
		A_hndw.a_hndw.me_einh_abverk = A_bas.a_bas.me_einh_abverk;
		strcpy ((LPSTR)A_hndw.a_hndw.hnd_gew_abverk, (LPSTR) A_bas.a_bas.hnd_gew_abverk);
		A_hndw.a_hndw.inh_abverk = A_bas.a_bas.inh_abverk;
		A_hndw.dbupdate ();
		A_krz.a_krz.a_herk = A_hndw.a_hndw.a;
		dsqlstatus = A_krz.dbreada_herkfirst ();
		if (dsqlstatus == 0 && A_hndw.a_hndw.a_krz != 0)
		{
			A_krz.dbdelete ();
		}
		if (A_hndw.a_hndw.a_krz != 0l)
		{
			A_krz.a_krz.a_krz = A_hndw.a_hndw.a_krz;
			A_krz.a_krz.a = A_hndw.a_hndw.a;
			A_krz.a_krz.a_herk = A_hndw.a_hndw.a;
			strcpy ((LPSTR) A_krz.a_krz.herk_kz, "1");
//			LPSTR pos = (LPSTR) A_krz.a_krz.herk_kz;
//			CDbUniCode::DbFromUniCode (A_krz.a_krz.herk_kz, pos, sizeof (A_krz.a_krz.herk_kz));
			strcpy ((LPSTR) A_krz.a_krz.verk_art, (LPSTR) A_hndw.a_hndw.verk_art);
//			pos = (LPSTR) A_krz.a_krz.verk_art;
//			CDbUniCode::DbFromUniCode (A_krz.a_krz.verk_art, pos, sizeof (A_krz.a_krz.herk_kz));
			strcpy ((LPSTR) A_krz.a_krz.modif, "N");
			A_krz.a_krz.hwg = A_bas.a_bas.hwg;
			A_krz.a_krz.wg = A_bas.a_bas.wg;
			A_krz.a_krz.anz_a = 1;
			A_krz.dbupdate ();
		}
	}
	else if (a_typ == Eig)
	{
		A_eig.a_eig.mdn = A_bas.a_bas.mdn;
		A_eig.a_eig.fil = A_bas.a_bas.fil;
		A_eig.a_eig.a = A_bas.a_bas.a;
		A_eig.a_eig.a_krz = A_hndw.a_hndw.a_krz;
		A_eig.a_eig.sg1 = A_hndw.a_hndw.sg1;
		A_eig.a_eig.sg2 = A_hndw.a_hndw.sg2;
		A_eig.a_eig.tara = A_hndw.a_hndw.tara;
		A_eig.a_eig.me_einh_ek = A_hndw.a_hndw.me_einh_kun;
		A_eig.a_eig.inh = A_hndw.a_hndw.inh;
		A_eig.a_eig.gew_bto = A_hndw.a_hndw.gew_bto;
		_tcscpy (A_eig.a_eig.verk_art, A_hndw.a_hndw.verk_art);
		_tcscpy (A_eig.a_eig.mwst_ueb, A_hndw.a_hndw.mwst_ueb);
		_tcscpy (A_eig.a_eig.pr_ausz, A_hndw.a_hndw.pr_ausz);
		_tcscpy ( A_eig.a_eig.pr_man, A_hndw.a_hndw.pr_man);
		_tcscpy ( A_eig.a_eig.pr_ueb, A_hndw.a_hndw.pr_ueb);
		_tcscpy ( A_eig.a_eig.reg_eti, A_hndw.a_hndw.reg_eti);
		_tcscpy ( A_eig.a_eig.theke_eti, A_hndw.a_hndw.theke_eti);
		_tcscpy ( A_eig.a_eig.waren_eti, A_hndw.a_hndw.waren_eti);
		A_eig.a_eig.anz_waren_eti = A_hndw.a_hndw.anz_waren_eti;
		A_eig.a_eig.anz_reg_eti = A_hndw.a_hndw.anz_reg_eti;
		A_eig.a_eig.anz_theke_eti = A_hndw.a_hndw.anz_theke_eti;
		A_eig.a_eig.me_einh_abverk = A_bas.a_bas.me_einh_abverk;
		strcpy ((LPSTR)A_eig.a_eig.hnd_gew_abverk, (LPSTR) A_bas.a_bas.hnd_gew_abverk);
		A_eig.a_eig.inh_abverk = A_bas.a_bas.inh_abverk;
		A_eig.dbupdate ();
		A_krz.a_krz.a_herk = A_eig.a_eig.a;
		dsqlstatus = A_krz.dbreada_herkfirst ();
		if (dsqlstatus == 0 && A_eig.a_eig.a_krz != 0)
		{
			A_krz.dbdelete ();
		}
		if (A_eig.a_eig.a_krz != 0l)
		{
			A_krz.a_krz.a_krz = A_eig.a_eig.a_krz;
			A_krz.a_krz.a = A_eig.a_eig.a;
			strcpy ((LPSTR) A_krz.a_krz.herk_kz, "1");
//			LPSTR pos = (LPSTR) A_krz.a_krz.herk_kz;
//			CDbUniCode::DbFromUniCode (A_krz.a_krz.herk_kz, pos, sizeof (A_krz.a_krz.herk_kz));
			strcpy ((LPSTR) A_krz.a_krz.verk_art, (LPSTR) A_eig.a_eig.verk_art);
//			pos = (LPSTR) A_krz.a_krz.verk_art;
//			CDbUniCode::DbFromUniCode (A_krz.a_krz.verk_art, pos, sizeof (A_krz.a_krz.herk_kz));
			strcpy ((LPSTR) A_krz.a_krz.modif, "N");
			A_krz.a_krz.hwg = A_bas.a_bas.hwg;
			A_krz.a_krz.wg = A_bas.a_bas.wg;
			A_krz.a_krz.anz_a = 1;
			A_krz.dbupdate ();
		}
	}
	else if (a_typ == Pfa)
	{
		A_pfa.a_pfa.mdn = A_bas.a_bas.mdn;
		A_pfa.a_pfa.fil = A_bas.a_bas.fil;
		A_pfa.a_pfa.a = A_bas.a_bas.a;
		A_pfa.a_pfa.a_krz = A_hndw.a_hndw.a_krz;
		A_pfa.a_pfa.sg1 = A_hndw.a_hndw.sg1;
		A_pfa.a_pfa.sg2 = A_hndw.a_hndw.sg2;
		_tcscpy (A_pfa.a_pfa.verk_art, A_hndw.a_hndw.verk_art);
		_tcscpy (A_pfa.a_pfa.mwst_ueb, A_hndw.a_hndw.mwst_ueb);
		A_pfa.dbupdate ();
		A_krz.a_krz.a_herk = A_pfa.a_pfa.a;
		dsqlstatus = A_krz.dbreada_herkfirst ();
		if (dsqlstatus == 0 && A_pfa.a_pfa.a_krz != 0)
		{
			A_krz.dbdelete ();
		}
		if (A_pfa.a_pfa.a_krz != 0l)
		{
			A_krz.a_krz.a_krz = A_pfa.a_pfa.a_krz;
			A_krz.a_krz.a = A_pfa.a_pfa.a;
			strcpy ((LPSTR) A_krz.a_krz.herk_kz, "1");
//			LPSTR pos = (LPSTR) A_krz.a_krz.herk_kz;
//			CDbUniCode::DbFromUniCode (A_krz.a_krz.herk_kz, pos, sizeof (A_krz.a_krz.herk_kz));
			strcpy ((LPSTR) A_krz.a_krz.verk_art, (LPSTR) A_pfa.a_pfa.verk_art);
//			pos = (LPSTR) A_krz.a_krz.verk_art;
//			CDbUniCode::DbFromUniCode (A_krz.a_krz.verk_art, pos, sizeof (A_krz.a_krz.herk_kz));
			strcpy ((LPSTR) A_krz.a_krz.modif, "N");
			A_krz.a_krz.hwg = A_bas.a_bas.hwg;
			A_krz.a_krz.wg = A_bas.a_bas.wg;
			A_krz.a_krz.anz_a = 1;
			A_krz.dbupdate ();
		}
	}
	else if (a_typ == Leer)
	{
		A_leer.a_leer.mdn = A_bas.a_bas.mdn;
		A_leer.a_leer.fil = A_bas.a_bas.fil;
		A_leer.a_leer.a = A_bas.a_bas.a;
		A_leer.a_leer.a_krz = A_hndw.a_hndw.a_krz;
		A_leer.a_leer.sg1 = A_hndw.a_hndw.sg1;
		A_leer.a_leer.sg2 = A_hndw.a_hndw.sg2;
		_tcscpy (A_leer.a_leer.verk_art, A_hndw.a_hndw.verk_art);
		_tcscpy (A_leer.a_leer.mwst_ueb, A_hndw.a_hndw.mwst_ueb);
		A_leer.a_leer.br_gew = A_hndw.a_hndw.gew_bto; //FS-302
		A_leer.dbupdate ();
		A_krz.a_krz.a_herk = A_leer.a_leer.a;
		dsqlstatus = A_krz.dbreada_herkfirst ();
		if (dsqlstatus == 0 && A_leer.a_leer.a_krz != 0)
		{
			A_krz.dbdelete ();
		}
		if (A_leer.a_leer.a_krz != 0l)
		{
			A_krz.a_krz.a_krz = A_leer.a_leer.a_krz;
			A_krz.a_krz.a = A_leer.a_leer.a;
			strcpy ((LPSTR) A_krz.a_krz.herk_kz, "1");
//			LPSTR pos = (LPSTR) A_krz.a_krz.herk_kz;
//			CDbUniCode::DbFromUniCode (A_krz.a_krz.herk_kz, pos, sizeof (A_krz.a_krz.herk_kz));
			strcpy ((LPSTR) A_krz.a_krz.verk_art, (LPSTR) A_leer.a_leer.verk_art);
//			pos = (LPSTR) A_krz.a_krz.verk_art;
//			CDbUniCode::DbFromUniCode (A_krz.a_krz.verk_art, pos, sizeof (A_krz.a_krz.herk_kz));
			strcpy ((LPSTR) A_krz.a_krz.modif, "N");
			A_krz.a_krz.hwg = A_bas.a_bas.hwg;
			A_krz.a_krz.wg = A_bas.a_bas.wg;
			A_krz.a_krz.anz_a = 1;
			A_krz.dbupdate ();
		}
	}
	else if (a_typ == Varb)
	{
		A_varb.a_varb.mdn = A_bas.a_bas.mdn;
		A_varb.a_varb.fil = A_bas.a_bas.fil;
		A_varb.a_varb.a =   A_bas.a_bas.a;
		A_varb.a_varb.mat = A_mat.a_mat.mat;
		int dsqlstatus = A_varb.dbupdate ();
		A_mat.a_mat.a = A_bas.a_bas.a;
		if (!TestMat ()) return FALSE;
		A_mat.dbupdate ();
	}
	else if (a_typ == Zut)
	{
		A_zut.a_zut.mdn = A_bas.a_bas.mdn;
		A_zut.a_zut.fil = A_bas.a_bas.fil;
		A_zut.dbupdate ();
		if (!TestMat ()) return FALSE;
		A_mat.dbupdate ();
	}
	else if (a_typ == Huel)
	{
		A_huel.a_huel.mdn = A_bas.a_bas.mdn;
		A_huel.a_huel.fil = A_bas.a_bas.fil;
		A_huel.dbupdate ();
		if (!TestMat ()) return FALSE;
		A_mat.dbupdate ();
	}
	InsA_bas_Prot (2); 
	if (Write160 && dbild160 != NULL)
	{
		if (a_bas.delstatus == -1 ||  a_bas.filialsperre == 1) //FS-26
		{
			Modif ="L";
		}
		    CStringA Command;
			Command.Format ("bild160 %s %.0lf -1 0 0 0",
			                 Modif.GetBuffer (), A_bas.a_bas.a); 

			(*dbild160) (Command.GetBuffer ());
	}

	Etikett.Write ();
	if (ProductPage4 != NULL)
	{
		ProductPage4->Write ();
	}
	Etikett.TestPrint ();

	DestroyRows (DbRows);
	DestroyRows (ListRows);
	DisableTabControl (TRUE);
	EnableFields (FALSE);
	m_A.SetFocus ();
	a_krz_gen = 0l;
	A_bas.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

void CBasisdatenPage::AgDefaults ()
{
    Ag.ag.ag = A_bas.a_bas.ag;
    if (Ag.dbreadfirst () == 0)
    {
        A_bas.a_bas.wg      = Ag.ag.wg;
        A_bas.a_bas.a_typ   = Ag.ag.a_typ;
		a_typ = Ag.ag.a_typ;
        A_bas.a_bas.mwst    = Ag.ag.mwst;
        A_bas.a_bas.me_einh = Ag.ag.me_einh;

        A_bas.a_bas.erl_kto = Ag.ag.erl_kto;
        A_bas.a_bas.we_kto = Ag.ag.we_kto;
        A_bas.a_bas.teil_smt = Ag.ag.teil_smt;
		A_bas.a_bas.abt = Ag.ag.abt;

		strcpy ((char *) A_bas.a_bas.hnd_gew, (char *) Ag.ag.hnd_gew);
        strcpy ((char *) A_bas.a_bas.kost_kz, (char *) Ag.ag.kost_kz);
        strcpy ((char *) A_bas.a_bas.smt, (char *) Ag.ag.smt);
        A_bas.a_bas.we_kto_1 = Ag.ag.we_kto_1;
        A_bas.a_bas.we_kto_2 = Ag.ag.we_kto_2;
        A_bas.a_bas.we_kto_3 = Ag.ag.we_kto_3;
        A_bas.a_bas.erl_kto_1  = Ag.ag.erl_kto_1;
        A_bas.a_bas.erl_kto_2  = Ag.ag.erl_kto_2;
        A_bas.a_bas.erl_kto_3  = Ag.ag.erl_kto_3;
        strcpy ((char *) A_bas.a_bas.best_auto, (char *) Ag.ag.best_auto);
        strcpy ((char *) A_bas.a_bas.bsd_kz, (char *) Ag.ag.bsd_kz);
        A_bas.a_bas.sw       = Ag.ag.sw;
        strcpy ((char *) A_bas.a_bas.hbk_kz, (char *) Ag.ag.hbk_kz);
        A_bas.a_bas.hbk_ztr  = Ag.ag.hbk_ztr;
        strcpy ((char *) A_bas.a_bas.stk_lst_kz, (char *) Ag.ag.stk_lst_kz);

	    strcpy ((char *) A_hndw.a_hndw.smt, (char *) Ag.ag.smt);
	    strcpy ((char *) A_hndw.a_hndw.verk_art, (char *) Ag.ag.verk_art);
		strcpy ((char *) A_hndw.a_hndw.pr_ueb, (char *) Ag.ag.pr_ueb);
		A_hndw.a_hndw.sg1       = Ag.ag.sg1;
		A_hndw.a_hndw.sg2       = Ag.ag.sg2;
		A_hndw.a_hndw.tara      = Ag.ag.tara;
		strcpy ((char *) A_hndw.a_hndw.waren_eti, (char *) Ag.ag.waren_eti);
		strcpy ((char *) A_hndw.a_hndw.theke_eti, (char *) Ag.ag.theke_eti);
		strcpy ((char *) A_hndw.a_hndw.reg_eti, (char *) Ag.ag.reg_eti);
    }
}

BOOL CBasisdatenPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (m_Mdn.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Alle Eintr�ge l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	Ipr.beginwork ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);

	DisableTabControl (TRUE);
	EnableFields (FALSE);
	m_A.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	Ipr.commitwork ();
	sql_mode = sql_s;
	return TRUE;
}

void CBasisdatenPage::OnAchoiceM ()
{
	ModalChoice = TRUE;
	AChoiceStatM = TRUE;
	Form.Get ();
	if (ChoiceM == NULL)
	{
		ChoiceM = new CChoiceA (this);
	    ChoiceM->IsModal = ModalChoice;
		ChoiceM->CreateDlg ();
	}

    ChoiceM->SetDbClass (&A_bas);
	ChoiceM->SearchText = SearchA;
	ChoiceM->DoModal();
    if (ChoiceM->GetState ())
    {
		  CABasList *abl = ChoiceM->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_A.EnableWindow (TRUE);
		  m_A.SetSel (0, -1, TRUE);
		  m_A.SetFocus ();
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
		AChoiceStatM = FALSE;
	}
}

void CBasisdatenPage::OnAchoiceM_grund ()
{
	ModalChoice = TRUE;
	AChoiceStatM = TRUE;
	Form.Get ();
	if (ChoiceM == NULL)
	{
		ChoiceM = new CChoiceA (this);
	    ChoiceM->IsModal = ModalChoice;
		ChoiceM->CreateDlg ();
	}

    ChoiceM->SetDbClass (&A_bas);
	ChoiceM->SearchText = SearchA;
	ChoiceM->DoModal();
    if (ChoiceM->GetState ())
    {
		  CABasList *abl = ChoiceM->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&a_bas, &A_bas.a_bas, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () != 0)
		  {
			  //MELDUNG!!!
		  }
          memcpy (&A_bas.a_bas, &a_bas, sizeof (A_BAS));
          A_bas.a_bas.a_grund = abl->a;
		  Form.Show ();
		  m_Edit_grund.EnableWindow (TRUE);
		  m_Edit_grund.SetSel (0, -1, TRUE);
		  m_Edit_grund.SetFocus ();
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
		AChoiceStatM = FALSE;
	}
}
void CBasisdatenPage::OnAchoiceM_ersatz ()
{
	ModalChoice = TRUE;
	AChoiceStatM = TRUE;
	Form.Get ();
	if (ChoiceM == NULL)
	{
		ChoiceM = new CChoiceA (this);
	    ChoiceM->IsModal = ModalChoice;
		ChoiceM->CreateDlg ();
	}

    ChoiceM->SetDbClass (&A_bas);
	ChoiceM->SearchText = SearchA;
	ChoiceM->DoModal();
    if (ChoiceM->GetState ())
    {
		  CABasList *abl = ChoiceM->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&a_bas, &A_bas.a_bas, sizeof (A_BAS));
          A_bas.a_bas.a = abl->a;
		  if (A_bas.dbreadfirst () != 0)
		  {
			  //MELDUNG!!!
		  }
          memcpy (&A_bas.a_bas, &a_bas, sizeof (A_BAS));
          A_bas.a_bas.a_ersatz = abl->a;
		  Form.Show ();
		  m_Edit_ersatz.EnableWindow (TRUE);
		  m_Edit_ersatz.SetSel (0, -1, TRUE);
		  m_Edit_ersatz.SetFocus ();
		  if (SearchA == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
		AChoiceStatM = FALSE;
	}
}

void CBasisdatenPage::OnAchoice ()
{
	ModalChoice = FALSE;
	AChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		if (SearchA.Trim () != "")
		{
			Choice->SearchText = SearchA;
			Choice->SetSearchText ();
		}
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceA (this);
		if (PageType == Standard)
		{
			Choice->Title = _T("Auswahl Artikel");
			Choice->ChoiceType = CChoiceA::Standard;
		}
		else
		{
			Choice->Title = _T("Auswahl Material");
			Choice->ChoiceType = CChoiceA::Material;
		}
	    Choice->IsModal = FALSE;
		Choice->DlgBkColor = ListBkColor;
		Choice->Where = Where;
		Choice->IdArrDown = IDI_HARROWDOWN;
		Choice->IdArrUp   = IDI_HARROWUP;
		Choice->IdArrNo   = IDI_HARROWNO;
		Choice->HideOK    = HideOK;
		Choice->HideFilter = FALSE;
		Choice->Types     = Types;
		Choice->CreateDlg ();
	}

    Choice->SetDbClass (&A_bas);
	if (SearchA.Trim () != "")
	{
		Choice->SearchText = SearchA;
		Choice->SetSearchText ();
	}
	CRect mrect;
	GetParent ()->GetWindowRect (&mrect);
	CRect rect;
	Choice->GetWindowRect (&rect);
	int scx = GetSystemMetrics (SM_CXSCREEN);
	int scy = GetSystemMetrics (SM_CYSCREEN);
	rect.top = 50;
	rect.right = scx - 2;
	rect.left = rect.right - 300;
	rect.bottom = scy - 50;

	Choice->MoveWindow (&rect);
	Choice->SetListFocus ();

	return;
}

void CBasisdatenPage::OnInhLiefChoice ()//LAC-115
{
	Form.Get ();
	CChoicePtab *ChoicePtab = new CChoicePtab (this);
    ChoicePtab->IsModal = TRUE;
	ChoicePtab->IdArrDown = IDI_HARROWDOWN;
	ChoicePtab->IdArrUp   = IDI_HARROWUP;
	ChoicePtab->IdArrNo   = IDI_HARROWNO;
    ChoicePtab->HideEnter = FALSE;
	ChoicePtab->CreateDlg ();
	ChoicePtab->SingleSelection = FALSE;
    ChoicePtab->Ptitem = _T("inh_lief");
    ChoicePtab->Caption = _T("Auswahl Inhalt der Liefereinheit");

    ChoicePtab->SetDbClass (&A_bas);

	ChoicePtab->DoModal();

    if (ChoicePtab->GetState ())
    {
		  CPtabList *abl; 

		  CString Text = "";
		  if (ChoicePtab->SelectList.size () > 1)
		  {
				for (std::vector<CPtabList *>::iterator pabl = ChoicePtab->SelectList.begin (); pabl != ChoicePtab->SelectList.end (); ++pabl)
				{
					abl = *pabl;
					if (Text.GetLength () > 0)
					{
						Text += _T(",");
					}
					abl->ptwert.Trim ();
					Text += abl->ptwert;
				}
	   	  }
		  else
		  {
			  abl = ChoicePtab->GetSelectedText (); 
			  if (abl == NULL) return;
			  Text = abl->ptwer1.Trim ();
		  }
		  m_InhLief.SetWindowText (Text);
		  A_bas.a_bas.inh_lief = CStrFuncs::StrToDouble (Text);
		  Form.Show ();
		  m_InhLief.EnableWindow (TRUE);
		  m_InhLief.SetSel (0, -1, TRUE);
		  m_InhLief.SetFocus ();

		//  PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}



void CBasisdatenPage::OnAGchoice ()
{
    AGChoiceStat = TRUE;
	Form.Get ();
	if (ChoiceAG != NULL && !ModalChoiceAG)
	{
		ChoiceAG->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceAG == NULL)
	{
		ChoiceAG = new CChoiceAG (this);
	    ChoiceAG->IsModal = ModalChoiceAG;
		ChoiceAG->IdArrDown = IDI_HARROWDOWN;
		ChoiceAG->IdArrUp   = IDI_HARROWUP;
		ChoiceAG->IdArrNo   = IDI_HARROWNO;
	    ChoiceAG->HideEnter = FALSE;
		ChoiceAG->Where = AgWhere;
		ChoiceAG->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceAG->SetDbClass (&A_bas);
	ChoiceAG->SearchText = SearchAG;
	if (ModalChoiceAG)
	{
			ChoiceAG->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

		Choice->MoveWindow (&rect);
		Choice->SetListFocus ();
		return;
	}

    if (ChoiceAG->GetState ())
    {
		  CAGList *abl = ChoiceAG->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Ag.ag, &ag_null, sizeof (AG));
          Ag.ag.ag = abl->ag;
		  if (Ag.dbreadfirst () == 0)
		  {
			  A_bas.a_bas.ag = Ag.ag.ag;
		  }
		  Form.Show ();
		  m_Edit_ag.EnableWindow (TRUE);
		  m_Edit_ag.SetSel (0, -1, TRUE);
		  m_Edit_ag.SetFocus ();
		  if (SearchAG == "")
		  {
			PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
		  }
    }
	else
	{
	 	  AGChoiceStat = FALSE;	
	}
}

void CBasisdatenPage::OnEanchoice ()
{
//	AChoiceStat = TRUE;
	Form.Get ();
	if (ChoiceEan != NULL)
	{
		ChoiceEan->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceEan == NULL)
	{
		ChoiceEan = new CChoiceEan (this);
	    ChoiceEan->IsModal = FALSE;
		ChoiceEan->DlgBkColor = ListBkColor;
		ChoiceEan->Where = Where;
		ChoiceEan->IdArrDown = IDI_HARROWDOWN;
		ChoiceEan->IdArrUp   = IDI_HARROWUP;
		ChoiceEan->IdArrNo   = IDI_HARROWNO;
		ChoiceEan->HideOK    = HideOK;
		ChoiceEan->CreateDlg ();
	}

    ChoiceEan->SetDbClass (&A_bas);
	ChoiceEan->SearchText = SearchEan;

	if (!ModalChoiceEan)
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceEan->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

		ChoiceEan->MoveWindow (&rect);
		ChoiceEan->SetListFocus ();
		return;
	}

	ChoiceEan->DoModal();
    if (ChoiceEan->GetState ())
    {
		  CEanList *abl = ChoiceEan->GetSelectedText (); 
		  if (abl == NULL) return;
		  A_bas.a_bas.a = abl->a;
	      Form.GetFormField (&m_A)->Show ();
		  Read ();
		  Form.Show ();
		  PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CBasisdatenPage::OnAkrzchoice ()
{
	Form.Get ();
	if (ChoiceAkrz != NULL)
	{
		ChoiceAkrz->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceAkrz == NULL)
	{
		ChoiceAkrz = new CChoiceAkrz (this);
	    ChoiceAkrz->IsModal = FALSE;
		ChoiceAkrz->DlgBkColor = ListBkColor;
		ChoiceAkrz->Where = Where;
		ChoiceAkrz->IdArrDown = IDI_HARROWDOWN;
		ChoiceAkrz->IdArrUp   = IDI_HARROWUP;
		ChoiceAkrz->IdArrNo   = IDI_HARROWNO;
		ChoiceAkrz->HideOK    = HideOK;
		ChoiceAkrz->CreateDlg ();
	}

    ChoiceAkrz->SetDbClass (&A_bas);
	ChoiceAkrz->SearchText = SearchAkrz;

	if (!ModalChoiceAkrz)
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceAkrz->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

		ChoiceAkrz->MoveWindow (&rect);
		ChoiceAkrz->SetListFocus ();
		return;
	}

	ChoiceAkrz->DoModal();
    if (ChoiceAkrz->GetState ())
    {
		  CAkrzList *abl = ChoiceAkrz->GetSelectedText (); 
		  if (abl == NULL) return;
		  A_bas.a_bas.a = abl->a;
	      Form.GetFormField (&m_A)->Show ();
		  Read ();
		  Form.Show ();
		  PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CBasisdatenPage::OnLiefBestchoice ()
{
	Form.Get ();
	if (ChoiceLiefBest != NULL)
	{
		ChoiceLiefBest->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceLiefBest == NULL)
	{
		ChoiceLiefBest = new CChoiceLiefBest (this);
	    ChoiceLiefBest->IsModal = FALSE;
		ChoiceLiefBest->Where = Where;
		ChoiceLiefBest->DlgBkColor = ListBkColor;
	    ChoiceLiefBest->HideEnter = FALSE;
		ChoiceLiefBest->IdArrDown = IDI_HARROWDOWN;
		ChoiceLiefBest->IdArrUp   = IDI_HARROWUP;
		ChoiceLiefBest->IdArrNo   = IDI_HARROWNO;
		ChoiceLiefBest->CreateDlg ();
	}

    ChoiceLiefBest->SetDbClass (&A_bas);
	ChoiceLiefBest->SearchText = SearchLiefBest;

	if (!ModalChoiceLiefBest)
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceLiefBest->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

		ChoiceLiefBest->MoveWindow (&rect);
		ChoiceLiefBest->SetListFocus ();
		return;
	}

	ChoiceLiefBest->DoModal();
    if (ChoiceLiefBest->GetState ())
    {
		  CLiefBestList *abl = ChoiceLiefBest->GetSelectedText (); 
		  if (abl == NULL) return;
		  A_bas.a_bas.a = abl->a;
	      Form.GetFormField (&m_A)->Show ();
		  Read ();
		  Form.Show ();
		  PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CBasisdatenPage::OnMatchoice ()
{
	Form.Get ();
	if (ChoiceMat != NULL)
	{
		ChoiceMat->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMat == NULL)
	{
		ChoiceMat = new CChoiceMat (this);
	    ChoiceMat->IsModal = FALSE;
		ChoiceMat->DlgBkColor = ListBkColor;
		ChoiceMat->Where = Where;
		ChoiceMat->IdArrDown = IDI_HARROWDOWN;
		ChoiceMat->IdArrUp   = IDI_HARROWUP;
		ChoiceMat->IdArrNo   = IDI_HARROWNO;
		ChoiceMat->HideOK    = HideOK;
		ChoiceMat->CreateDlg ();
	}

    ChoiceMat->SetDbClass (&A_bas);
//	ChoiceMat->SearchText = SearchAkrz;

	if (!ModalChoiceMat)
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMat->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

		ChoiceMat->MoveWindow (&rect);
		ChoiceMat->SetListFocus ();
		return;
	}

	ChoiceMat->DoModal();
    if (ChoiceMat->GetState ())
    {
		  CMatList *abl = ChoiceMat->GetSelectedText (); 
		  if (abl == NULL) return;
		  A_bas.a_bas.a = abl->a;
	      Form.GetFormField (&m_A)->Show ();
		  Read ();
		  Form.Show ();
		  PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CBasisdatenPage::OnASelected ()
{
	if (Choice == NULL) return;
	if (!m_Mdn.IsWindowEnabled ())
	{
		Write (); //Hier: Speichern , wenn Artikel �ber das Suchfenster gewechselt wird 
	}
    CABasList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
	CPageHandler::GetInstance ()->RegisterPage (); 
    A_bas.a_bas.a = abl->a;
	Form.GetFormField (&m_A)->Show ();
	Read ();
    Form.Show ();
	CPageHandler::GetInstance ()->SetPage (); 
	if (Choice->FocusBack)
	{
		Choice->SetListFocus ();
	}
//	CPageHandler::GetInstance ()->SetPage (); 
//	ActivateTypePageEx ();
}

void CBasisdatenPage::OnEanSelected ()
{
	if (ChoiceEan == NULL) return;
    CEanList *abl = ChoiceEan->GetSelectedText (); 
    if (abl == NULL) return;
    A_bas.a_bas.a = abl->a;
	Form.GetFormField (&m_A)->Show ();
	Read ();
    Form.Show ();
	if (ChoiceEan->FocusBack)
	{
		ChoiceEan->SetListFocus ();
	}
}

void CBasisdatenPage::OnAkrzSelected ()
{
	if (ChoiceAkrz == NULL) return;
    CAkrzList *abl = ChoiceAkrz->GetSelectedText (); 
    if (abl == NULL) return;
    A_bas.a_bas.a = abl->a;
	Form.GetFormField (&m_A)->Show ();
	Read ();
    Form.Show ();
	if (ChoiceAkrz->FocusBack)
	{
		ChoiceAkrz->SetListFocus ();
	}
}

void CBasisdatenPage::OnLiefBestSelected ()
{
	if (ChoiceLiefBest == NULL) return;
    CLiefBestList *abl = ChoiceLiefBest->GetSelectedText (); 
    if (abl == NULL) return;
    A_bas.a_bas.a = abl->a;
	Form.GetFormField (&m_A)->Show ();
	Read ();
    Form.Show ();
	if (ChoiceLiefBest->FocusBack)
	{
		ChoiceLiefBest->SetListFocus ();
	}
}

void CBasisdatenPage::OnMatSelected ()
{
	if (ChoiceMat == NULL) return;
    CMatList *abl = ChoiceMat->GetSelectedText (); 
    if (abl == NULL) return;
    A_bas.a_bas.a = abl->a;
	Form.GetFormField (&m_A)->Show ();
	Read ();
    Form.Show ();
	if (ChoiceMat->FocusBack)
	{
		ChoiceMat->SetListFocus ();
	}
}

/**
void CBasisdatenPage::OnAShow ()
{
    CABasList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    A_bas.a_bas.a = abl->a;
    A_bas.dbreadfirst ();
	HoleArtikel(A_bas.a_bas.a);
    Form.Show ();
}
**/

void CBasisdatenPage::OnACanceled ()
{
	Choice->ShowWindow (SW_HIDE);
//	m_A_bz1.SetFocus ();
}

void CBasisdatenPage::OnEanCanceled ()
{
	ChoiceEan->ShowWindow (SW_HIDE);
	m_A_bz1.SetFocus ();
}

void CBasisdatenPage::OnAkrzCanceled ()
{
	ChoiceAkrz->ShowWindow (SW_HIDE);
	m_A_bz1.SetFocus ();
}

void CBasisdatenPage::OnLiefBestCanceled ()
{
	ChoiceLiefBest->ShowWindow (SW_HIDE);
	m_A_bz1.SetFocus ();
}

void CBasisdatenPage::OnMatCanceled ()
{
	ChoiceMat->ShowWindow (SW_HIDE);
	m_A_bz1.SetFocus ();
}


BOOL CBasisdatenPage::StepBack ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{

		int ret = MessageBox (_T("�nderungen speichern ?"), NULL, MB_YESNO | MB_ICONQUESTION);
		if (ret == IDYES)
		{
			Write ();
		}
		else
		{
			DisableTabControl (TRUE);
			EnableFields (FALSE);
			m_A.SetFocus ();
			DestroyRows (DbRows);
			DestroyRows (ListRows);
			A_bas.rollbackwork ();
			if (a_krz_gen != 0l)
			{
				AutoNr.nveinid (0, 0, "a_krz", a_krz_gen);
			}
		}
	}
	return TRUE;
}

void CBasisdatenPage::OnCancel ()
{
	StepBack ();
}

void CBasisdatenPage::OnSave ()
{
	Write ();
}

void CBasisdatenPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CBasisdatenPage::FillPrGrStufCombo ()
{
	CVector Values;
	Values.Init ();
//	m_List.m_Mdn = Mdn.mdn.mdn;
	IprGrStufk.iprgrstufk.mdn = Mdn.mdn.mdn;
    IprGrStufk.sqlopen (IprGrStufkCursor);
	while (IprGrStufk.sqlfetch (IprGrStufkCursor) == 0)
	{
		IprGrStufk.dbreadfirst ();
		CString *Value = new CString ();
		Value->Format (_T("%ld  %s"), IprGrStufk.iprgrstufk.pr_gr_stuf,
			                          IprGrStufk.iprgrstufk.zus_bz);
		Values.Add (Value);
	}
//	m_List.FillPrGrStufCombo (Values);
}

void CBasisdatenPage::FillKunPrCombo ()
{
	CVector Values;
	Values.Init ();
	I_kun_prk.i_kun_prk.mdn = Mdn.mdn.mdn;
    I_kun_prk.sqlopen (IKunPrkCursor);
	while (I_kun_prk.sqlfetch (IKunPrkCursor) == 0)
	{
		I_kun_prk.dbreadfirst ();
		CString *Value = new CString ();
		Value->Format (_T("%ld  %s"), I_kun_prk.i_kun_prk.kun_pr,
			                          I_kun_prk.i_kun_prk.zus_bz);
		Values.Add (Value);
	}
//	m_List.FillKunPrCombo (Values);
}

BOOL CBasisdatenPage::Delete ()
{
	OnDelete ();
	return DelOK;
}

void CBasisdatenPage::OnDelete ()
{
    extern short sql_mode;
	short sql_s = sql_mode;
    sql_mode = 1;
	DelOK = FALSE;
	if (m_Mdn.IsWindowEnabled ()) return;
	int ret = MessageBox (_T("Satz l�schen"), NULL, MB_YESNO | MB_ICONQUESTION);
	if (ret != IDYES) return;
	if (PageType == Standard && PeriData)
	{
		A_bas.a_bas.delstatus = 1;
		_tcscpy (A_bas.a_bas.modif,_T("L"));
		A_bas.dbupdate ();
		if (Write160 && dbild160 != NULL)
		{
		    CStringA Command;
			Command.Format ("bild160 L %.0lf -1 0 0 0",
			                 A_bas.a_bas.a); 

			(*dbild160) (Command.GetBuffer ());
		}
        if (Choice != NULL)
 	    {
			  Choice->FillList ();
		}
		MessageBox (_T("Der Artikel wurde zum L�schen gekennzeichnet"));
	}
	else
	{
		if (ADel != NULL) delete ADel;
		ADel = new CADel ();
		ADel->Create (IDD_ADEL, this);
		ADel->ShowWindow (SW_SHOWNORMAL);
		ADel->UpdateWindow ();
		if (ADel->Run (A_bas.a_bas.a))
		{
              if (Choice != NULL)
			  {
				  Choice->FillList ();
			  }
		}
		else
		{
			return;
		}
	}
	ReadDefaultHide();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	DisableTabControl (TRUE);
	EnableFields (FALSE);
	m_A.SetFocus ();
	a_krz_gen = 0l;
	A_bas.commitwork ();
	sql_mode = sql_s;
	DelOK = TRUE;
	return;
}

void CBasisdatenPage::OnInsert ()
{
//	m_List.InsertRow ();
}

BOOL CBasisdatenPage::Print ()
{
	CProcess print;
	CString Command = "dr70001 -name 12100";
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CBasisdatenPage::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
//	m_List.StartPauseEnter ();
}

void CBasisdatenPage::OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
//	m_List.EndPauseEnter ();
}

void CBasisdatenPage::DisableTabControl (BOOL disable)
{
	CPropertySheet *p = (CPropertySheet *) GetParent ();
	CTabCtrl *tab = p->GetTabControl ();
	if (disable)
	{
		tab->ShowWindow (SW_HIDE);
	}
	else
	{
		tab->ShowWindow (SW_SHOWNORMAL);
	}
}

void CBasisdatenPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
}

void CBasisdatenPage::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("a_ersatz_nicht_pruefen", cfg_v) == TRUE)
    {
			a_ersatz_nicht_pruefen = atoi (cfg_v);  //FS-18
    }
    if (Cfg.GetCfgValue ("NeuerArtikelAnzTage", cfg_v) == TRUE)
    {
			NeuerArtikelAnzTage = atoi (cfg_v);  //FS-18
    }
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
//			m_List.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("Write160", cfg_v) == TRUE)
    {
			if (atoi (cfg_v) == 0) Write160 = FALSE;
    }
    if (Cfg.GetCfgValue ("AgRestriktion", cfg_v) == TRUE)
    {
			AgRestriktion = atoi (cfg_v);  //FS-18
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("TypeMode", cfg_v) == TRUE)
    {
			TypeMode = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("FontHeight1", cfg_v) == TRUE)
    {
			FontHeight = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
//			m_List.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
//			m_List.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
//			m_List.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CBasisdatenPage::OnCopy ()
{
	CWnd *Control = GetFocus ();

	//if (Control == &m_List.ListEdit ||
 //       Control == &m_List.ListComboBox ||
	//	Control == &m_List.SearchListCtrl.Edit)	
	//{
	//	ListCopy ();
	//	return;
	//}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CBasisdatenPage::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

void CBasisdatenPage::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	//if (IsWindow (m_List.ListEdit.m_hWnd) ||
	//	IsWindow (m_List.ListComboBox) ||
	//	IsWindow (m_List.SearchListCtrl.Edit))
	//{
	//	m_List.StopEnter ();
	//	m_List.StartEnter (m_List.EditCol, m_List.EditRow);
	//}

	try
	{
		BOOL SaveAll = TRUE;
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
		//for (int i = 0; i < m_List.GetItemCount (); i ++)
		//{
		//	if (Buffer != "")
		//	{
		//		Buffer += sep;
		//	}
		//	CString Row = "";
		//	int cols = m_List.GetHeaderCtrl ()->GetItemCount ();
		//	for (int j = 0; j < cols; j ++)
		//	{
		//		if (Row != "")
		//		{
		//			Row += Separator;
		//		}
		//		CString Field = m_List.GetItemText (i, j);
		//		Field.TrimRight ();
		//		Row += Field; 
		//	}
		//	Buffer += Row;
		//}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}


void CBasisdatenPage::FillCombo (LPTSTR Item, CWnd *control)
{
	CFormField *f = Form.GetFormField (control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}


void CBasisdatenPage::FillComboLong (LPTSTR Item, CWnd *control)
{
	CFormField *f = Form.GetFormField (control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			CStrFuncs::Trim(Ptabn.ptabn.ptbezk);
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
											Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CBasisdatenPage::FillComboEx (LPTSTR Item, CWnd *control)
{
	CFormField *f = Form.GetFormField (control);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = Ptabn.sqlcursor (Sql.GetBuffer ());
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			CStrFuncs::Trim(Ptabn.ptabn.ptbezk);
			ComboValue->Format (_T("%s %s | %s"), Ptabn.ptabn.ptwert,
				                                Ptabn.ptabn.ptbezk,
												Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}



void CBasisdatenPage::FillSais ()
{
	if (SaisCursor == -1)
	{
		Sais.sqlout ((short *) &Sais.sais.jr, SQLSHORT, 0);
		Sais.sqlout ((short *) &Sais.sais.sais, SQLSHORT, 0);
		SaisCursor = Sais.sqlcursor (_T("select jr, sais from sais order by jr, sais"));
	}
	CFormField *f = Form.GetFormField (&m_Sais);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Sais.sqlopen (SaisCursor);
		while (Sais.sqlfetch (SaisCursor) == 0)
		{
			Sais.dbreadfirst ();

			LPSTR pos = (LPSTR) Sais.sais.st_bz_1;
			CDbUniCode::DbToUniCode (Sais.sais.st_bz_1, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%hd %hd %s"), Sais.sais.sais,
				                                  Sais.sais.jr,
												  Sais.sais.st_bz_1);
		    f->ComboValues.push_back (ComboValue); 
		}
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}


void CBasisdatenPage::FillStuecklCombo ()
{
	CFormField *f = Form.GetFormField (&m_Combo_stueckl);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"stk_lst_kz\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CBasisdatenPage::FillSmtCombo ()
{
	CFormField *f = Form.GetFormField (&m_Smt);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"smt\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CBasisdatenPage::FillTeilsmtCombo ()
{
	CFormField *f = Form.GetFormField (&m_Combo_teilsmt);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"teil_smt\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CBasisdatenPage::FillAtypCombo ()
{
	CFormField *f = Form.GetFormField (&m_Atyp);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
		int cursor = -1;
		if (PageType == Standard)
		{
			cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"a_typ\" ")
									  _T("and ptwer1 = \"standard\" ")
									  _T("order by ptlfnr"));
		}
		else
		{
			cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"a_typ\" ")
									  _T("and ptwer1 = \"material\" ")
									  _T("order by ptlfnr"));
		}
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbez);
		    f->ComboValues.push_back (ComboValue); 
			CString *Atyp1Value = new CString (*ComboValue);
		    ATyp1Combo.push_back (Atyp1Value); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		m_Atyp.SetDroppedWidth (300);
//		m_Atyp.SetMinVisibleItems (10);
		f->SetSel (0);
		f->Get ();
	}
}

void CBasisdatenPage::FillAtyp2Combo ()
{
	Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
	Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
	Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
	int cursor = -1;
	if (PageType == Material)
	{
			cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"a_typ\" ")
									  _T("and ptwer1 = \"standard\" ")
									  _T("order by ptlfnr"));
	}
	else
	{
			cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"a_typ\" ")
									  _T("and ptwer1 = \"material\" ")
									  _T("order by ptlfnr"));
	}
	while (Ptabn.sqlfetch (cursor) == 0)
	{
		Ptabn.dbreadfirst ();

		LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
		CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
		pos = (LPSTR) Ptabn.ptabn.ptbez;
		CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbez, pos);
		CString *Atyp1Value = new CString ();
		Atyp1Value->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
			                             Ptabn.ptabn.ptbez);
	    ATyp2Combo.push_back (Atyp1Value); 
	}
	Ptabn.sqlclose (cursor);
}

void CBasisdatenPage::FillMwstCombo ()
{
	CFormField *f = Form.GetFormField (&m_Mwst);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"mwst\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CBasisdatenPage::FillHndGewCombo ()
{
	CFormField *f = Form.GetFormField (&m_HndGew);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"hnd_gew\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CBasisdatenPage::FillMeEinhCombo ()
{
	CFormField *f = Form.GetFormField (&m_MeEinh);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"me_einh\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CBasisdatenPage::FillLiefEinhCombo ()
{
	CFormField *f = Form.GetFormField (&m_LiefEinh);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"me_einh\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CBasisdatenPage::FillKunEinhCombo ()
{
	CFormField *f = Form.GetFormField (&m_KunBestEinh);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"me_einh\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CBasisdatenPage::FillEinhAbverkCombo ()
{
	CFormField *f = Form.GetFormField (&m_EinhAbverk);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"me_einh\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}

void CBasisdatenPage::FillHndGewAbverkCombo ()
{
	CFormField *f = Form.GetFormField (&m_HndGewAbverk);
	if (f != NULL)
	{
		f->ComboValues.clear ();
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
		Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
		Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
        int cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"hnd_gew\" ")
									  _T("order by ptlfnr"));
		while (Ptabn.sqlfetch (cursor) == 0)
		{
			Ptabn.dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn.ptabn.ptbezk;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptbezk, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn.ptabn.ptwert,
				                             Ptabn.ptabn.ptbezk);
		    f->ComboValues.push_back (ComboValue); 
		}
		Ptabn.sqlclose (cursor);
		f->FillComboBox ();
		f->SetSel (0);
		f->Get ();
	}
}



void CBasisdatenPage::sw_hide (BOOL hide)
{

	if (hide)
	{
			m_Mdn.ShowWindow (SW_HIDE);
			m_MdnName.ShowWindow (SW_HIDE);
			m_A_bz1.ShowWindow (SW_HIDE);
			m_A_bz2.ShowWindow (SW_HIDE);
			m_A_bz3.ShowWindow (SW_HIDE);
			m_Ag_bz1.ShowWindow (SW_HIDE);
			m_Wg_bez.ShowWindow (SW_HIDE);
			m_Hwg_bez.ShowWindow (SW_HIDE);
			m_Edit_ag.ShowWindow (SW_HIDE);
			m_Edit_wg.ShowWindow (SW_HIDE);
			m_Edit_hwg.ShowWindow (SW_HIDE);
			m_Edit_abt.ShowWindow (SW_HIDE);
			m_Static_abt.ShowWindow (SW_HIDE);
			m_AChoice.ShowWindow (SW_HIDE);
			m_AGChoice.ShowWindow (SW_HIDE);
			m_GrundChoice.ShowWindow (SW_HIDE);
			m_InhLiefChoice.ShowWindow (SW_HIDE); //LAC-115
    		m_ErsatzChoice.ShowWindow (SW_HIDE);
			m_MdnChoice.ShowWindow (SW_HIDE); 
			m_LMatchcode.ShowWindow (SW_HIDE);

			m_Combo_stueckl.ShowWindow (SW_HIDE);
			m_Static_stueckl.ShowWindow (SW_HIDE);
			m_Smt.ShowWindow (SW_HIDE);
			m_LSmt.ShowWindow (SW_HIDE);
			m_Combo_teilsmt.ShowWindow (SW_HIDE);
			m_Static_teilsmt.ShowWindow (SW_HIDE);
			m_Static_grund.ShowWindow (SW_HIDE);
			m_Static_farbe.ShowWindow (SW_HIDE);
			m_Edit_grund.ShowWindow (SW_HIDE);
			m_Check_bestand.ShowWindow (SW_HIDE);
			m_Check_paket.ShowWindow (SW_HIDE);
			m_Static_ersatz.ShowWindow (SW_HIDE);
			m_Edit_ersatz.ShowWindow (SW_HIDE);

			m_Atyp2.ShowWindow (SW_HIDE);
			m_Lager.ShowWindow (SW_HIDE);
			m_Best.ShowWindow (SW_HIDE);
			m_Kalk.ShowWindow (SW_HIDE);
			m_Farbe.ShowWindow (SW_HIDE);
			m_MoreKto.ShowWindow (SW_HIDE);

			m_LHndGew.ShowWindow (SW_HIDE);
			m_HndGew.ShowWindow (SW_HIDE);
			m_LMeEinh.ShowWindow (SW_HIDE);
			m_MeEinh.ShowWindow (SW_HIDE);
			m_LLiefEinh.ShowWindow (SW_HIDE);
			m_LiefEinh.ShowWindow (SW_HIDE);
			m_LInhLief.ShowWindow (SW_HIDE);
			m_InhLief.ShowWindow (SW_HIDE);

			m_LEinhAbverk.ShowWindow (SW_HIDE);
			m_EinhAbverk.ShowWindow (SW_HIDE);
			m_LInhAbverk.ShowWindow (SW_HIDE);
			m_InhAbverk.ShowWindow (SW_HIDE);
			m_LHndGewAbverk.ShowWindow (SW_HIDE);
			m_HndGewAbverk.ShowWindow (SW_HIDE);

			m_KunBestEinh.ShowWindow (SW_HIDE);
			m_LKunBestEinh.ShowWindow (SW_HIDE);
			m_LKunInh.ShowWindow (SW_HIDE);
			m_KunInh.ShowWindow (SW_HIDE);
			m_Deaktiv.ShowWindow (SW_HIDE);
			m_FilDeaktiv.ShowWindow (SW_HIDE);
			m_ShopKZ.ShowWindow (SW_HIDE);

			m_A_bz3_hide.ShowWindow (SW_SHOWNORMAL);
			m_Abt_hide.ShowWindow (SW_SHOWNORMAL);
			m_Stueckl_hide.ShowWindow (SW_SHOWNORMAL);
			m_Teilsmt_hide.ShowWindow (SW_SHOWNORMAL);
			m_Smt_hide.ShowWindow (SW_SHOWNORMAL);
			m_Grund_hide.ShowWindow (SW_SHOWNORMAL);
			m_Ersatz_hide.ShowWindow (SW_SHOWNORMAL);

			m_Gruppen_bestand.ShowWindow (SW_SHOWNORMAL);
			m_Radio_bestand_j.ShowWindow (SW_SHOWNORMAL);
			m_Radio_bestand_n.ShowWindow (SW_SHOWNORMAL);
			m_Radio_bestand_a.ShowWindow (SW_SHOWNORMAL);

			m_HndGewHide.ShowWindow (SW_SHOWNORMAL);
			m_MeEinhHide.ShowWindow (SW_SHOWNORMAL);
			m_LiefEinhHide.ShowWindow (SW_SHOWNORMAL);
			m_EinhAbverkHide.ShowWindow (SW_SHOWNORMAL);
			m_KunBestEinhHide.ShowWindow (SW_SHOWNORMAL);

    		Form.Show();
			//test
	}
	else
	{
			m_Mdn.ShowWindow (SW_SHOWNORMAL);
			m_MdnName.ShowWindow (SW_SHOWNORMAL);
			m_A_bz1.ShowWindow (SW_SHOWNORMAL);
			m_A_bz2.ShowWindow (SW_SHOWNORMAL);
			m_A_bz3.ShowWindow (SW_SHOWNORMAL);
			m_Ag_bz1.ShowWindow (SW_SHOWNORMAL);
			m_Wg_bez.ShowWindow (SW_SHOWNORMAL);
			m_Hwg_bez.ShowWindow (SW_SHOWNORMAL);
			m_Edit_ag.ShowWindow (SW_SHOWNORMAL);
			m_Edit_wg.ShowWindow (SW_SHOWNORMAL);
			m_Edit_hwg.ShowWindow (SW_SHOWNORMAL);
			m_Edit_abt.ShowWindow (SW_SHOWNORMAL);
			m_AChoice.ShowWindow (SW_SHOWNORMAL);
			m_AGChoice.ShowWindow (SW_SHOWNORMAL);
			m_GrundChoice.ShowWindow (SW_SHOWNORMAL);
			m_InhLiefChoice.ShowWindow (SW_SHOWNORMAL); //LAC-115
    		m_ErsatzChoice.ShowWindow (SW_SHOWNORMAL);
			m_MdnChoice.ShowWindow (SW_SHOWNORMAL);
			m_LMatchcode.ShowWindow (SW_SHOWNORMAL);
			m_Static_abt.ShowWindow (SW_SHOWNORMAL);

			m_Combo_stueckl.ShowWindow (SW_SHOWNORMAL);
			m_Static_stueckl.ShowWindow (SW_SHOWNORMAL);
			m_Combo_teilsmt.ShowWindow (SW_SHOWNORMAL);
			m_Static_teilsmt.ShowWindow (SW_SHOWNORMAL);
			m_Static_grund.ShowWindow (SW_SHOWNORMAL);
			m_Static_farbe.ShowWindow (SW_SHOWNORMAL);
			m_Edit_grund.ShowWindow (SW_SHOWNORMAL);
			m_Check_bestand.ShowWindow (SW_SHOWNORMAL);
			m_Check_paket.ShowWindow (SW_SHOWNORMAL);
			m_Static_ersatz.ShowWindow (SW_SHOWNORMAL);
			m_Edit_ersatz.ShowWindow (SW_SHOWNORMAL);

			m_LHndGew.ShowWindow (SW_SHOWNORMAL);
			m_HndGew.ShowWindow (SW_SHOWNORMAL);
			m_LMeEinh.ShowWindow (SW_SHOWNORMAL);
			m_MeEinh.ShowWindow (SW_SHOWNORMAL);
			m_LLiefEinh.ShowWindow (SW_SHOWNORMAL);
			m_LiefEinh.ShowWindow (SW_SHOWNORMAL);
			m_LInhLief.ShowWindow (SW_SHOWNORMAL);
			m_InhLief.ShowWindow (SW_SHOWNORMAL);

			m_LEinhAbverk.ShowWindow (SW_SHOWNORMAL);
			m_EinhAbverk.ShowWindow (SW_SHOWNORMAL);
			m_LInhAbverk.ShowWindow (SW_SHOWNORMAL);
			m_InhAbverk.ShowWindow (SW_SHOWNORMAL);
			m_LHndGewAbverk.ShowWindow (SW_SHOWNORMAL);
			m_HndGewAbverk.ShowWindow (SW_SHOWNORMAL);

			m_KunBestEinh.ShowWindow (SW_SHOWNORMAL);
			m_LKunBestEinh.ShowWindow (SW_SHOWNORMAL);
			m_LKunInh.ShowWindow (SW_SHOWNORMAL);
			m_KunInh.ShowWindow (SW_SHOWNORMAL);
			m_Atyp2.ShowWindow (SW_SHOWNORMAL);
			m_Lager.ShowWindow (SW_SHOWNORMAL);
			m_Best.ShowWindow (SW_SHOWNORMAL);
			m_Kalk.ShowWindow (SW_SHOWNORMAL);
			m_Farbe.ShowWindow (SW_SHOWNORMAL);
			m_MoreKto.ShowWindow (SW_SHOWNORMAL);
			m_Deaktiv.ShowWindow (SW_SHOWNORMAL);
			m_FilDeaktiv.ShowWindow (SW_SHOWNORMAL);
			m_ShopKZ.ShowWindow (SW_SHOWNORMAL);

			m_A_bz3_hide.ShowWindow (SW_HIDE);
			m_Abt_hide.ShowWindow (SW_HIDE);
			m_Stueckl_hide.ShowWindow (SW_HIDE);
			m_Teilsmt_hide.ShowWindow (SW_HIDE);
			m_Smt_hide.ShowWindow (SW_HIDE);
			m_Grund_hide.ShowWindow (SW_HIDE);
			m_Ersatz_hide.ShowWindow (SW_HIDE);
			m_Gruppen_bestand.ShowWindow (SW_HIDE);
			m_Radio_bestand_j.ShowWindow (SW_HIDE);
			m_Radio_bestand_n.ShowWindow (SW_HIDE);
			m_Radio_bestand_a.ShowWindow (SW_HIDE);
			m_HndGewHide.ShowWindow (SW_HIDE);
			m_MeEinhHide.ShowWindow (SW_HIDE);
			m_LiefEinhHide.ShowWindow (SW_HIDE);
			m_EinhAbverkHide.ShowWindow (SW_HIDE);
			m_KunBestEinhHide.ShowWindow (SW_HIDE);

			SetDefaultHide();
	}
}

void CBasisdatenPage::MatHide ()
{
	if (PageType == Material)
	{
		m_LAPfa.ShowWindow (SW_HIDE);
		m_APfa.ShowWindow (SW_HIDE);
		m_LALeer.ShowWindow (SW_HIDE);
		m_ALeer.ShowWindow (SW_HIDE);
		m_LALeih.ShowWindow (SW_HIDE);
		m_ALeih.ShowWindow (SW_HIDE);
		m_APfaChoice.ShowWindow (SW_HIDE);
		m_ALeerChoice.ShowWindow (SW_HIDE);
		m_ALeihChoice.ShowWindow (SW_HIDE);
		m_Link.ShowWindow (SW_HIDE);

		m_LEinhAbverk.ShowWindow (SW_HIDE);
		m_EinhAbverk.ShowWindow (SW_HIDE);
		m_LInhAbverk.ShowWindow (SW_HIDE);
		m_InhAbverk.ShowWindow (SW_HIDE);
		m_LHndGewAbverk.ShowWindow (SW_HIDE);
		m_HndGewAbverk.ShowWindow (SW_HIDE);

		m_LKunBestEinh.ShowWindow (SW_HIDE);
		m_KunBestEinh.ShowWindow (SW_HIDE);
		m_LKunInh.ShowWindow (SW_HIDE);
		m_KunInh.ShowWindow (SW_HIDE);
	}
}

void CBasisdatenPage::SetDefaultHide ()
{
	//Matchcode
	if (strcmp(clipped(a_bas_def.a_bz3),"-1") == 0) 
	{
		strcpy(da_bz3_hide,"N");
		m_A_bz3.ShowWindow (SW_HIDE);
		m_LMatchcode.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(da_bz3_hide,"J");
		m_A_bz3.ShowWindow (SW_SHOWNORMAL);
		m_LMatchcode.ShowWindow (SW_SHOWNORMAL);
	}
	//Abteilung
	if (a_bas_def.abt == -1) 
	{
		strcpy(dabt_hide,"N");
		m_Edit_abt.ShowWindow (SW_HIDE);
		m_Static_abt.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(dabt_hide,"J");
		m_Edit_abt.ShowWindow (SW_SHOWNORMAL);
		m_Static_abt.ShowWindow (SW_SHOWNORMAL);
	}
	//Stueckliste
	if (strcmp(clipped(a_bas_def.stk_lst_kz),"-") == 0) 
	{
		strcpy(dstueckl_hide,"N");
		m_Combo_stueckl.ShowWindow (SW_HIDE);
		m_Static_stueckl.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(dstueckl_hide,"J");
		m_Combo_stueckl.ShowWindow (SW_SHOWNORMAL);
		m_Static_stueckl.ShowWindow (SW_SHOWNORMAL);
	}
	//Sortiment
	if (strcmp (a_bas_def.smt, "-") == 0) 
	{
		strcpy(dsmt_hide,"N");
		m_Smt.ShowWindow (SW_HIDE);
		m_LSmt.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(dsmt_hide,"J");
		m_Smt.ShowWindow (SW_SHOWNORMAL);
		m_LSmt.ShowWindow (SW_SHOWNORMAL);
	}

	//Teilsortiment
	if (a_bas_def.teil_smt == -1) 
	{
		strcpy(dteilsmt_hide,"N");
		m_Combo_teilsmt.ShowWindow (SW_HIDE);
		m_Static_teilsmt.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(dteilsmt_hide,"J");
		m_Combo_teilsmt.ShowWindow (SW_SHOWNORMAL);
		m_Static_teilsmt.ShowWindow (SW_SHOWNORMAL);
	}
	//Grundartikel
	if (a_bas_def.a_grund == -1) 
	{
		strcpy(dgrund_hide,"N");
		m_Edit_grund.ShowWindow (SW_HIDE);
		m_Check_bestand.ShowWindow (SW_HIDE);
		m_Check_paket.ShowWindow (SW_HIDE);
		m_GrundChoice.ShowWindow (SW_HIDE);
		m_Static_grund.ShowWindow (SW_HIDE);
		m_Static_farbe.ShowWindow (SW_HIDE);
		m_Farbe.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(dgrund_hide,"J");
		m_Edit_grund.ShowWindow (SW_SHOWNORMAL);
		m_Check_bestand.ShowWindow (SW_SHOWNORMAL);
		m_Check_paket.ShowWindow (SW_SHOWNORMAL);
		m_GrundChoice.ShowWindow (SW_SHOWNORMAL);
		m_GrundChoice.ShowWindow (SW_SHOWNORMAL);
		m_Static_grund.ShowWindow (SW_SHOWNORMAL);
		m_Static_farbe.ShowWindow (SW_SHOWNORMAL);
		m_Farbe.ShowWindow (SW_SHOWNORMAL);
	}
	//Ersatzartikel
	if (a_bas_def.a_ersatz == -1) 
	{
		strcpy(dersatz_hide,"N");
		m_Edit_ersatz.ShowWindow (SW_HIDE);
		m_ErsatzChoice.ShowWindow (SW_HIDE);
		m_Static_ersatz.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(dersatz_hide,"J");
		m_Edit_ersatz.ShowWindow (SW_SHOWNORMAL);
		m_ErsatzChoice.ShowWindow (SW_SHOWNORMAL);
		m_Static_ersatz.ShowWindow (SW_SHOWNORMAL);
	}
	//Bestand
	strcpy(dbestand_j,"N");
	strcpy(dbestand_n,"N");
	strcpy(dbestand_a,"N");
	if (strcmp(clipped(a_bas_def.bsd_kz),"J") == 0) strcpy(dbestand_j,"J");
	if (strcmp(clipped(a_bas_def.bsd_kz),"N") == 0) strcpy(dbestand_n,"J");
	if (strcmp(clipped(a_bas_def.bsd_kz),"A") == 0) strcpy(dbestand_a,"J");
	if (strcmp(clipped(a_bas_def.bsd_kz),"A") != 0) 
	{
		m_Check_bestand.ShowWindow (SW_SHOWNORMAL);
	}
	else
	{
		m_Check_bestand.ShowWindow (SW_HIDE);
	}

// Einheiten
	BOOL HideEinhGroup = TRUE;
	if (strcmp (a_bas_def.hnd_gew, "-") == 0) 
	{
		strcpy(dhnd_gew_hide,"N");
		m_HndGew.ShowWindow (SW_HIDE);
		m_LHndGew.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(dhnd_gew_hide,"J");
		m_HndGew.ShowWindow (SW_SHOWNORMAL);
		m_LHndGew.ShowWindow (SW_SHOWNORMAL);
		HideEinhGroup = FALSE;
	}
	if (a_bas_def.me_einh == -1)
	{
		strcpy(dme_einh_hide,"N");
		m_MeEinh.ShowWindow (SW_HIDE);
		m_LMeEinh.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(dme_einh_hide,"J");
		m_MeEinh.ShowWindow (SW_SHOWNORMAL);
		m_LMeEinh.ShowWindow (SW_SHOWNORMAL);
		HideEinhGroup = FALSE;
	}

	if (a_bas_def.lief_einh == -1)
	{
		strcpy(dlief_einh_hide,"N");
		m_LiefEinh.ShowWindow (SW_HIDE);
		m_LLiefEinh.ShowWindow (SW_HIDE);
		m_InhLief.ShowWindow (SW_HIDE);
		m_LInhLief.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(dlief_einh_hide,"J");
		m_LiefEinh.ShowWindow (SW_SHOWNORMAL);
		m_LLiefEinh.ShowWindow (SW_SHOWNORMAL);
		m_InhLief.ShowWindow (SW_SHOWNORMAL);
		m_LInhLief.ShowWindow (SW_SHOWNORMAL);
		HideEinhGroup = FALSE;
	}

	if (a_bas_def.me_einh_abverk == -1)
	{
		strcpy(deinh_abverk_hide,"N");
		m_EinhAbverk.ShowWindow (SW_HIDE);
		m_LEinhAbverk.ShowWindow (SW_HIDE);
		m_InhAbverk.ShowWindow (SW_HIDE);
		m_LInhAbverk.ShowWindow (SW_HIDE);
		m_HndGewAbverk.ShowWindow (SW_HIDE);
		m_LHndGewAbverk.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(deinh_abverk_hide,"J");
		m_EinhAbverk.ShowWindow (SW_SHOWNORMAL);
		m_LEinhAbverk.ShowWindow (SW_SHOWNORMAL);
		m_InhAbverk.ShowWindow (SW_SHOWNORMAL);
		m_LInhAbverk.ShowWindow (SW_SHOWNORMAL);
		m_HndGewAbverk.ShowWindow (SW_SHOWNORMAL);
		m_LHndGewAbverk.ShowWindow (SW_SHOWNORMAL);
		HideEinhGroup = FALSE;
	}

	if (a_hndw_def.me_einh_kun == -1)
	{
		strcpy(dkun_einh_hide,"N");
		m_KunBestEinh.ShowWindow (SW_HIDE);
		m_LKunBestEinh.ShowWindow (SW_HIDE);
		m_KunInh.ShowWindow (SW_HIDE);
		m_LKunInh.ShowWindow (SW_HIDE);
	}
	else
	{
		strcpy(dkun_einh_hide,"J");
		m_KunBestEinh.ShowWindow (SW_SHOWNORMAL);
		m_LKunBestEinh.ShowWindow (SW_SHOWNORMAL);
		m_KunInh.ShowWindow (SW_SHOWNORMAL);
		m_LKunInh.ShowWindow (SW_SHOWNORMAL);
		HideEinhGroup = FALSE;
	}

	if (HideEinhGroup)
	{
		m_GroupEinh.ShowWindow (SW_HIDE);
	}
	else
	{
		m_GroupEinh.ShowWindow (SW_SHOWNORMAL);
	}

	MatHide ();
	Form.Show();
}


void CBasisdatenPage::ReadDefaultHide ()
{
	A_bas.a_bas.a = -1;
	A_bas.dbreadfirst ();
	memcpy (&a_bas_def, &A_bas.a_bas, sizeof (A_BAS));
	A_hndw.a_hndw.a = -1;
	A_hndw.dbreadfirst ();
	memcpy (&a_hndw_def, &A_hndw.a_hndw, sizeof (A_HNDW));
}

void CBasisdatenPage::WriteDefaultHide ()
{
	if (strcmp(clipped(da_bz3_hide),"N") == 0)
	{
		strcpy(A_bas.a_bas.a_bz3,"-1");
	}
	else
	{
		strcpy(A_bas.a_bas.a_bz3,"1");
	}
	if (strcmp(clipped(da_bz3_hide),"J") == 0) strcpy(A_bas.a_bas.a_bz3,"1");

	if (strcmp(clipped(dabt_hide),"N") == 0) A_bas.a_bas.abt = -1;
	if (strcmp(clipped(dabt_hide),"J") == 0) A_bas.a_bas.abt = 1;

	if (strcmp(clipped(dstueckl_hide),"N") == 0) strcpy(A_bas.a_bas.stk_lst_kz,"-");
	if (strcmp(clipped(dstueckl_hide),"J") == 0) strcpy(A_bas.a_bas.stk_lst_kz,"1");

	if (strcmp(clipped(dsmt_hide),"N") == 0) strcpy (A_bas.a_bas.smt, "-");
	if (strcmp(clipped(dsmt_hide),"J") == 0) strcpy (A_bas.a_bas.smt, "1");

	if (strcmp(clipped(dteilsmt_hide),"N") == 0) A_bas.a_bas.teil_smt = -1;
	if (strcmp(clipped(dteilsmt_hide),"J") == 0) A_bas.a_bas.teil_smt = 1;

	if (strcmp(clipped(dgrund_hide),"N") == 0) A_bas.a_bas.a_grund = (double)-1;
	if (strcmp(clipped(dgrund_hide),"J") == 0) A_bas.a_bas.a_grund = (double)1;

	if (strcmp(clipped(dersatz_hide),"N") == 0) A_bas.a_bas.a_ersatz = (double)-1;
	if (strcmp(clipped(dersatz_hide),"J") == 0) A_bas.a_bas.a_ersatz = (double)1;

	if (strcmp(clipped(dbestand_j),"J") == 0) strcpy(A_bas.a_bas.bsd_kz,"J");
	if (strcmp(clipped(dbestand_n),"J") == 0) strcpy(A_bas.a_bas.bsd_kz,"N");
	if (strcmp(clipped(dbestand_a),"J") == 0) strcpy(A_bas.a_bas.bsd_kz,"A");

	if (strcmp(clipped(dhnd_gew_hide),"J") == 0) strcpy (A_bas.a_bas.hnd_gew, "1");
	if (strcmp(clipped(dhnd_gew_hide),"N") == 0) strcpy (A_bas.a_bas.hnd_gew, "-1");
	if (strcmp(clipped(dme_einh_hide),"J") == 0) A_bas.a_bas.me_einh = 1;
	if (strcmp(clipped(dme_einh_hide),"N") == 0) A_bas.a_bas.me_einh = -1;
	if (strcmp(clipped(dlief_einh_hide),"J") == 0) A_bas.a_bas.lief_einh = 1;
	if (strcmp(clipped(dlief_einh_hide),"N") == 0) A_bas.a_bas.lief_einh = -1;

	if (strcmp(clipped(deinh_abverk_hide),"J") == 0) A_bas.a_bas.me_einh_abverk = 1;
	if (strcmp(clipped(deinh_abverk_hide),"N") == 0) A_bas.a_bas.me_einh_abverk = -1;

	if (strcmp(clipped(dkun_einh_hide),"J") == 0) A_hndw.a_hndw.me_einh_kun = 1;
	if (strcmp(clipped(dkun_einh_hide),"N") == 0) A_hndw.a_hndw.me_einh_kun = -1;

	memcpy (&a_bas_def, &A_bas.a_bas, sizeof (A_BAS));
	memcpy (&a_hndw_def, &A_hndw.a_hndw, sizeof (A_HNDW));

}



void CBasisdatenPage::OnEnChangeA()
{
	if (A_bas.a_bas.a == 0.0) return;
	if (ArtikelGesperrt) 
	{
			sprintf(ctext,"Artikel %.0f : Ist von einem anderen Benutzer gesperrt ",A_bas.a_bas.a);
			GetParentOwner ()->SetWindowText (_T(ctext));
	
			sprintf(ctext,"%s :  Speichern z.Zeit nicht m�glich",A_bas.a_bas.a_bz1);
			GetParent()->GetParent()->GetParent()-> SetWindowText (_T(ctext));
	}
	else
	{
		if (NeuerArtikel == TRUE)
		{
			sprintf(ctext,"Artikel Neuanlage");
			GetParentOwner ()->SetWindowText (_T(ctext));
			GetParent()->GetParent()->GetParent()-> SetWindowText (_T(ctext));
		}
		else
		{
			sprintf(ctext,"Artikel %.0f",A_bas.a_bas.a);
			GetParentOwner ()->SetWindowText (_T(ctext));
//			GetParent()->GetParent()->GetParent()->GetParent()->GetParent()->SetWindowText (_T(ctext));
	
			sprintf(ctext,"%s",A_bas.a_bas.a_bz1);
			GetParent()->GetParent()->GetParent()-> SetWindowText (_T(ctext));
		}
	}

}

void CBasisdatenPage::LockArtikel(double a)
{
	    extern short sql_mode;
		short sql_s = sql_mode;
	    sql_mode = 1;
		ArtikelGesperrt = FALSE;
		if (A_bas.dblock () < 0)
		{
			ArtikelGesperrt = TRUE;
		} 
		sql_mode = sql_s;
}

void CBasisdatenPage::HoleArtikel(double a)
{
	    Work.holeArtikel(a); //FS-337
		HoleGrundArtikel (A_bas.a_bas.a_grund);

		if (Mdn.mdn.mdn != A_bas.a_bas.mdn)
		{
			Mdn.mdn.mdn = A_bas.a_bas.mdn;
			Form.Show ();
			ReadMdn ();
		}
		m_A.EnableWindow (FALSE); //WAL-130 beim Rundlauf darf der Cursor nicht mehr auf Artikel kommen, da er sonds neu gelesen wird und die Maskenvariablen �ber schreibt
}

long CBasisdatenPage::HoleGrundArtikel(double a) //FS-337
{
	    extern short sql_mode;
		int dsqlstatus; 
		short sql_s = sql_mode;
	    sql_mode = 1;

		A_grund.a_grund.a = a;
		dsqlstatus = A_grund.dblock () ;
		if (dsqlstatus < 0)
		{
			GrundArtikelGesperrt = TRUE;
		}
		else
		{
			GrundArtikelGesperrt = FALSE;
			dsqlstatus = A_grund.dbreadfirst () ;
		}
		sql_mode = sql_s;
	return dsqlstatus;
}

void CBasisdatenPage::HoleAG(long ag)
{
	    BOOL UpdateAbverk = FALSE;
		if (A_bas.a_bas.me_einh_abverk == A_bas.a_bas.me_einh)
		{
			UpdateAbverk = TRUE;
		}
//	    if (A_bas.a_bas.wg == 0) AgDefaults ();
	    if (NeuerArtikel) AgDefaults (); //FS-295
		Work.holeAG(ag);
		A_bas.a_bas.wg = Work.wgw.wg;
		A_bas.a_bas.hwg = Work.hwg.hwg;
		if (NeuerArtikel)
		{
			_tcscpy (A_hndw.a_hndw.verk_art, Ag.ag.verk_art);
			A_bas.a_bas.me_einh_abverk = A_bas.a_bas.me_einh;
			_tcscpy (A_bas.a_bas.hnd_gew_abverk, A_bas.a_bas.hnd_gew);
		}
		else if (UpdateAbverk)
		{
			A_bas.a_bas.me_einh_abverk = A_bas.a_bas.me_einh;
			_tcscpy (A_bas.a_bas.hnd_gew_abverk, A_bas.a_bas.hnd_gew);
		}
}


void CBasisdatenPage::OnEnKillfocusEditAg()
{
/*
		if (!ReadAG ())
		{
			m_Edit_ag.SetFocus ();
		}
*/
}

void CBasisdatenPage::OnEnKillfocusMdn()
{
}

void CBasisdatenPage::OnEnKillfocusA()
{
	int di = 0;
}

void CBasisdatenPage::OnEnKillfocusEditGrund()
{
	    Form.Get(); //WAL-130
		if (!Read_a_grund ())
		{
			m_Edit_grund.SetFocus ();
		}
		A_grund.a_grund.a = A_bas.a_bas.a_grund;
		A_grund.dbreadfirst ();  
		if (!ArtikelGesperrt) Update->Show (); //Zutat muss nochmal gelesen werden ! 050614 WAL-130
}

void CBasisdatenPage::OnEnKillfocusEditErsatz()
{
		if (!Read_a_ersatz ())
		{
			m_Edit_ersatz.SetFocus ();
		}
}

void CBasisdatenPage::EnableFields (BOOL b)
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
			f->Control->EnableWindow (b);
	}
	m_Atyp2.EnableWindow (b);
	m_MoreKto.EnableWindow (b);
	m_Lager.EnableWindow (b);
	m_Best.EnableWindow (b);
	m_Kalk.EnableWindow (b);
	m_Farbe.EnableWindow (b);
	m_AGChoice.EnableWindow (b);
	m_GrundChoice.EnableWindow (b);
	m_InhLiefChoice.EnableWindow (b); //LAC-15
	m_ErsatzChoice.EnableWindow (b);
	m_APfaChoice.EnableWindow (b);
	m_ALeerChoice.EnableWindow (b);
	m_ALeihChoice.EnableWindow (b);
	HeadControls.Enable (!b);
	if (b)
	{
		m_A.EnableWindow (b);
	}
	m_A.SetReadOnly (b);
	Update->UpdateAll ();
/*
	CView *parent = (CView *) GetParent ()->GetParent ();
	CDocument *doc = parent->GetDocument ();
	doc->UpdateAllViews (parent);
*/
}

void CBasisdatenPage::OnRButtonDown( UINT flags, CPoint point)
{
	if (!m_Mdn.IsWindowEnabled ())
	{
		m_ImageArticle.ShowImage = ShowImage;   
        m_ImageArticle.ImageListener = this;
		m_ImageArticle.ImageFile = A_bas.a_bas.bild;
		m_ImageArticle.ImageFile.Trim ();
		BOOL ret = m_ImageArticle.OnRButtonDown (flags, point);
	}
}

void CBasisdatenPage::OnLButtonDblClk( UINT flags, CPoint  point)
{
	if (!m_Mdn.IsWindowEnabled ())
	{
		m_ImageArticle.ShowImage = ShowImage;   
        m_ImageArticle.ImageListener = this;
		m_ImageArticle.ImageFile = A_bas.a_bas.bild;
		m_ImageArticle.ImageFile.Trim ();
		BOOL ret = m_ImageArticle.OnLButtonDblClk (flags, point);
	}
}

void CBasisdatenPage::DisplayImage ()
{
	m_ImageArticle.Show (_T(""));
	if (ShowImage)
	{
			m_ImageArticle.Show (A_bas.a_bas.bild);
	}
	m_ImageArticle.Invalidate ();
}

void CBasisdatenPage::ActionPerformed (int action)
{

	if (action == m_ImageArticle.Unlink)
	{
		    strcpy (A_bas.a_bas.bild, _T("")); 
	}
	else if (action == m_ImageArticle.Link)
	{
			CArtImages ArtImages;
            CString ServerPath;
			CString LocalPath = m_ImageArticle.GetImageFile ();
			if (ArtImages.MakeServerPath (LocalPath, ServerPath))
			{
			    strcpy (A_bas.a_bas.bild, ServerPath.GetBuffer ()); 
			}
			else
			{
				MessageBox (_T("Server wurde  nicht gefunden"), NULL, MB_OK | MB_ICONERROR);
			    strcpy (A_bas.a_bas.bild, LocalPath.GetBuffer ()); 
				return;
			}
			if (!ArtImages.CopyToServer (LocalPath))
			{
				MessageBox (_T("Bild kann nicht auf den Server kopiert werden"), NULL, MB_OK | MB_ICONERROR);
			}
	}
	else if (action == m_ImageArticle.ImageShow)
	{
			ShowImage = m_ImageArticle.ShowImage;
			CView *parent = (CView *) GetParent ()->GetParent ();
			CDocument *doc = parent->GetDocument ();
			doc->UpdateAllViews (parent);
	}
	DisplayImage ();
}

void CBasisdatenPage::OnLinkImage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	m_ImageArticle.OnLinkImage ();
}

void CBasisdatenPage::OnUnlinkImage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	m_ImageArticle.OnUnlinkImage ();
}

void CBasisdatenPage::OnOpenImage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	m_ImageArticle.ImageFile = A_bas.a_bas.bild;
	m_ImageArticle.ImageFile.Trim ();
	m_ImageArticle.OnOpenImage ();
}

void CBasisdatenPage::OnOpenImageWith()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	m_ImageArticle.ImageFile = A_bas.a_bas.bild;
	m_ImageArticle.ImageFile.Trim ();
	m_ImageArticle.OnOpenImageWith ();
}

void CBasisdatenPage::OnMoreKto()
{
	CKtoDialog dlg;
	memcpy (&dlg.A_bas.a_bas, &A_bas.a_bas, sizeof (A_BAS));
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
		memcpy (&A_bas.a_bas, &dlg.A_bas.a_bas, sizeof (A_BAS));
	}
}

void CBasisdatenPage::OnAtyp2()
{
	CAtyp2 dlg;
	dlg.A_bas = &A_bas;
	if (PageType == Standard)
	{
		if (a_typ_pos == 1)
		{
			dlg.Add0 = TRUE; // 0 ist zul�ssig
			dlg.ATyp2Combo = &ATyp2Combo;
		}
		else
		{
			dlg.Add0 = FALSE; // 0 ist nicht zul�ssig
//			dlg.ATyp2Combo = &ATyp1Combo;
			dlg.ATyp2Combo = &ATyp2Combo;
		}
	}
	else if (PageType == Material)
	{
		if (a_typ_pos == 1)
		{
			dlg.Add0 = TRUE; // 0 ist nicht zul�ssig
			dlg.ATyp2Combo = &ATyp2Combo;
		}
		else
		{
			dlg.Add0 = FALSE; // 0 ist zul�ssig
			dlg.ATyp2Combo = &ATyp2Combo;
		}
	}
	dlg.a_typ_pos = a_typ_pos;
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
	}
}

void CBasisdatenPage::OnLager()
{
	CLagerDialog dlg;
//	memcpy (&dlg.A_bas.a_bas, &A_bas.a_bas, sizeof (A_BAS));
	dlg.A_bas = &A_bas;
	dlg.Mdn.mdn.mdn = Mdn.mdn.mdn;
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
	}
}

void CBasisdatenPage::OnBest()
{
	CBestDialog dlg;
	dlg.A_bas = &A_bas;
	dlg.Mdn.mdn.mdn = Mdn.mdn.mdn;
//	dlg.Fil.fil.fil = Fil.fil.fil;
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
	}
}

void CBasisdatenPage::OnKalk()
{
	CKalkDialog dlg;
	dlg.A_bas = &A_bas;
	dlg.a_typ = a_typ;
	dlg.Mdn.mdn.mdn = Mdn.mdn.mdn;
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
	}
}
void CBasisdatenPage::OnFarbe()
{
	CColorDialog dlgColor(m_clrFarbe);
	if (dlgColor.DoModal() == IDOK)
	{
		m_clrFarbe = dlgColor.GetColor();
		Invalidate();
		A_grund.a_grund.rgb_r = GetRValue(m_clrFarbe);
		A_grund.a_grund.rgb_g = GetGValue(m_clrFarbe);
		A_grund.a_grund.rgb_b = GetBValue(m_clrFarbe);
//		m_nIDColor = IDM_CUSTOM;
	}
}

void CBasisdatenPage::OnChoiceAPfa()
{
	CChoiceAQuery Choice;

	Choice.Query = _T("where a_typ = 4");
	Choice.CreateDlg ();
    Choice.SetDbClass (&A_bas);
	Choice.DoModal ();
    if (Choice.GetState ())
    {
		CABasList *abl = Choice.GetSelectedText (); 
		if (abl == NULL) return;
		A_hndw.a_hndw.a_pfa = abl->a;
		Form.Show ();
		m_APfa.SetFocus ();
    }
}

void CBasisdatenPage::OnChoiceALeer()
{
	CChoiceAQuery Choice;

	Choice.Query = _T("where a_typ = 11");
	Choice.CreateDlg ();
    Choice.SetDbClass (&A_bas);
	Choice.DoModal ();
    if (Choice.GetState ())
    {
		CABasList *abl = Choice.GetSelectedText (); 
		if (abl == NULL) return;
		A_hndw.a_hndw.a_leer = abl->a;
		Form.Show ();
		m_ALeer.SetFocus ();
    }
}

void CBasisdatenPage::OnChoiceALeih()
{
	CChoiceAQuery Choice;

	Choice.Query = _T("where a_typ = 12");
	Choice.CreateDlg ();
    Choice.SetDbClass (&A_bas);
	Choice.DoModal ();
    if (Choice.GetState ())
    {
		CABasList *abl = Choice.GetSelectedText (); 
		if (abl == NULL) return;
		A_bas.a_bas.a_leih = abl->a;
		Form.Show ();
		m_ALeih.SetFocus ();
    }
}

void CBasisdatenPage::EnableLinks ()
{
	Form.GetFormField (&m_Atyp)->Get ();

	if (a_typ != Hndw)
	{
		m_APfa.EnableWindow (FALSE);
		m_ALeer.EnableWindow (FALSE);
		m_ALeih.EnableWindow (FALSE);
		m_APfaChoice.EnableWindow (FALSE);
		m_ALeerChoice.EnableWindow (FALSE);
		m_ALeihChoice.EnableWindow (FALSE);
	}
	else	
	{
		m_APfa.EnableWindow (TRUE);
		m_ALeer.EnableWindow (TRUE);
		m_ALeih.EnableWindow (TRUE);
		m_APfaChoice.EnableWindow (TRUE);
		m_ALeerChoice.EnableWindow (TRUE);
		m_ALeihChoice.EnableWindow (TRUE);
	}


	if (strcmp ((LPSTR) A_bas.a_bas.bsd_kz, "J") == 0) 
	{
//			m_Lager.ShowWindow (SW_SHOWNORMAL);
		m_Lager.EnableWindow ();
	}
	else
	{
//			m_Lager.ShowWindow (SW_HIDE);
		m_Lager.EnableWindow (FALSE);
	}

	if (A_bas.a_bas.a_typ == Eig)
	{
		m_Best.EnableWindow (FALSE);
	}
	else
	{
		m_Best.EnableWindow ();
	}
	if (A_bas.a_bas.a_typ != Hndw && A_bas.a_bas.a_typ != Eig)
	{
		m_KunBestEinh.EnableWindow (FALSE);
		m_KunInh.EnableWindow (FALSE);
	}
	else
	{
		m_KunBestEinh.EnableWindow (TRUE);
		m_KunInh.EnableWindow (TRUE);
	}
//FS-26 A
	if (A_bas.a_bas.filialsperre == 1)
	{
		m_FilDeaktiv.SetWindowTextA("Artikel ist in Filialen gel�scht");
	}
	else
	{
		m_FilDeaktiv.SetWindowTextA("Artikel aus Filialsystemen l�schen");
	}
	if (A_bas.a_bas.ghsperre == 1)
	{
		m_Deaktiv.SetWindowTextA("Artikel ist Gro�handel NICHT AKTIV");
	}
	else
	{
		m_Deaktiv.SetWindowTextA("Artikel im Gro�handel deaktivieren");
	}
//FS-26 E

	Form.Show();

}

void CBasisdatenPage::OnCbnSelchangeATyp()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.

    EnableLinks ();
}

BOOL CBasisdatenPage::ChoiceHasElements()
{
	if (Choice == NULL)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CBasisdatenPage::ChoiceEanHasElements()
{
	if (ChoiceEan == NULL)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CBasisdatenPage::ChoiceAkrzHasElements()
{
	if (ChoiceAkrz == NULL)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CBasisdatenPage::ChoiceLiefBestHasElements()
{
	if (ChoiceLiefBest == NULL)
	{
		return FALSE;
	}
	return TRUE;
}


void CBasisdatenPage::FirstRec()
{
	if (Choice == NULL) return;

	Choice->FirstSelection ();
	OnASelected ();
	if (!Choice->IsWindowVisible ())
	{
		m_A_bz1.SetFocus ();
	}
}

void CBasisdatenPage::PriorRec()
{
	if (Choice == NULL) return;

	Choice->DecSelection ();
	OnASelected ();
	if (!Choice->IsWindowVisible ())
	{
		m_A_bz1.SetFocus ();
	}
}

void CBasisdatenPage::NextRec()
{
	if (Choice == NULL) return;

	Choice->IncSelection ();
	OnASelected ();
	if (!Choice->IsWindowVisible ())
	{
		m_A_bz1.SetFocus ();
	}
}

void CBasisdatenPage::LastRec()
{
	if (Choice == NULL) return;

	Choice->LastSelection ();
	OnASelected ();
	if (!Choice->IsWindowVisible ())
	{
		m_A_bz1.SetFocus ();
	}
}

void CBasisdatenPage::OnDlgBackground()
{
/*
	CColorDialog dlg;
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	dlg.m_cc.rgbResult = Color;
	if (dlg.DoModal() == IDOK)
	{
		DlgBkColor = dlg.GetColor();
		DeleteObject (DlgBrush);
		DlgBrush = NULL;
		InvalidateRect (NULL);
	}
**/
	DeleteObject (DlgBrush);
	DlgBrush = NULL;
	InvalidateRect (NULL);
}

void CBasisdatenPage::OnChoicebackground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

	CColorDialog dlg;
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	dlg.m_cc.rgbResult = Color;
	if (dlg.DoModal() == IDOK)
	{
		ListBkColor = dlg.GetColor();
		if (Choice != NULL)
		{
			Choice->DlgBkColor = dlg.GetColor();
			DeleteObject (Choice->DlgBrush);
			Choice->DlgBrush = NULL;
			Choice->InvalidateRect (NULL);
		}
		if (ChoiceEan != NULL)
		{
			ChoiceEan->DlgBkColor = dlg.GetColor();
			DeleteObject (ChoiceEan->DlgBrush);
			ChoiceEan->DlgBrush = NULL;
			ChoiceEan->InvalidateRect (NULL);
		}
		if (ChoiceAkrz != NULL)
		{
			ChoiceAkrz->DlgBkColor = dlg.GetColor();
			DeleteObject (ChoiceEan->DlgBrush);
			ChoiceAkrz->DlgBrush = NULL;
			ChoiceAkrz->InvalidateRect (NULL);
		}
	}
}

void CBasisdatenPage::OnChoiceaDefault()
{
	if (Choice == NULL) return;
	Choice->SetDefault ();
}

void CBasisdatenPage::OnChoiceeanDefault()
{
	if (ChoiceEan == NULL) return;
	ChoiceEan->SetDefault ();
}

void CBasisdatenPage::OnChoiceakrzDefault()
{
	if (ChoiceAkrz == NULL) return;
	ChoiceAkrz->SetDefault ();
}

void CBasisdatenPage::OnChoiceLiefBestDefault()
{
	if (ChoiceLiefBest == NULL) return;
	ChoiceLiefBest->SetDefault ();
}

void CBasisdatenPage::OnFlatLayout ()
{
	if (FlatLayout)
	{
		FlatLayout = FALSE;
	}
	else
	{
		FlatLayout = TRUE;
	}
	CCtrlInfo::NewStyle = FlatLayout;
	CtrlGrid.SetNewStyle (FlatLayout);
//	Invalidate ();

	CView *parent = (CView *) GetParent ()->GetParent ();
	CDocument *doc = parent->GetDocument ();
	doc->UpdateAllViews (parent);
}

void CBasisdatenPage::SetWhere ()
{
	Types = "";
	int cursor = -1;
	Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
	Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
	Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
	if (PageType == Standard)
	{
		cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"a_typ\" ")
									  _T("and ptwer1 = \"standard\" ")
									  _T("order by ptlfnr"));
	}
	else
	{
		cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"a_typ\" ")
									  _T("and ptwer1 = \"material\" ")
									  _T("order by ptlfnr"));
	}

	while (Ptabn.sqlfetch (cursor) == 0)
	{
			Ptabn.dbreadfirst ();
			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
            if (Types.GetLength () > 0)
			{
				Types += _T(",");
			}
			Types += Ptabn.ptabn.ptwert;
	}
	Ptabn.sqlclose (cursor);
	Types.Trim ();
	if (AgRestriktion != Verbieten) //FS-18
	{
		Where = _T("");
		return;
	}

	if (Types == "")
	{
		Where = _T("and a_bas.a_typ = -1"); 
		return;
	}

	Where = _T("and (");
	Where += _T("a_bas.a_typ in (");
	Where += Types;
	Where += _T(") ");

	Where += _T("or ");
	Where += _T("a_bas.a_typ2 in (");
	Where += Types;
	Where += _T("))");
}

void CBasisdatenPage::SetAgWhere ()
{
	Types = "";
	int cursor = -1;

	Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
	Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
	Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
	if (PageType == Standard)
	{
		cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"a_typ\" ")
									  _T("and ptwer1 = \"standard\" ")
									  _T("order by ptlfnr"));
	}
	else
	{
		cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
			                          _T("where ptitem = \"a_typ\" ")
									  _T("and ptwer1 = \"material\" ")
									  _T("order by ptlfnr"));
	}

	while (Ptabn.sqlfetch (cursor) == 0)
	{
			Ptabn.dbreadfirst ();
			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
            if (Types.GetLength () > 0)
			{
				Types += _T(",");
			}
			Types += Ptabn.ptabn.ptwert;
	}
	Ptabn.sqlclose (cursor);
	Types.Trim ();
	if (AgRestriktion != Verbieten) //FS-18
	{
		AgWhere = _T("");
		return;
	}

	if (Types == "")
	{
//		Where = _T("");
		AgWhere = _T("and a_typ = -1");
		return;
	}

	AgWhere = _T("and ");
	AgWhere += _T("a_typ in (");
	AgWhere += Types;
	AgWhere += _T(")");

}


void CBasisdatenPage::SetPeriTypes ()
{
	PeriTypes = "";
	int cursor = -1;
	Ptabn.sqlout ((char *) Ptabn.ptabn.ptitem, SQLCHAR, sizeof (Ptabn.ptabn.ptitem)); 
	Ptabn.sqlout ((char *) Ptabn.ptabn.ptwert, SQLCHAR, sizeof (Ptabn.ptabn.ptwert)); 
	Ptabn.sqlout ((long *) &Ptabn.ptabn.ptlfnr, SQLLONG, 0); 
	cursor = Ptabn.sqlcursor (_T("select ptitem, ptwert, ptlfnr from ptabn ")
	                          _T("where ptitem = \"peri_typ\" ")
							  _T("and ptwer2 = \"1\" ")
							  _T("order by ptlfnr"));
	while (Ptabn.sqlfetch (cursor) == 0)
	{
			Ptabn.dbreadfirst ();
			LPSTR pos = (LPSTR) Ptabn.ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn.ptabn.ptwert, pos);
            if (PeriTypes.GetLength () > 0)
			{
				PeriTypes += _T(",");
			}
			PeriTypes += Ptabn.ptabn.ptwert;
	}
	Ptabn.sqlclose (cursor);
	PeriData = FALSE;
	PeriTypes.Trim ();
	if (PeriTypes == _T("")) return;
	CString Sql = _T("select count (*) from sys_peri where peri_typ in (");
	Sql += PeriTypes;
	Sql += _T(")");
	long count = 0l;
	Ptabn.sqlout ((long *) &count, SQLLONG, 0);
	int dsqlstatus = Ptabn.sqlcomm (Sql.GetBuffer ());
	if (count > 0) PeriData = TRUE;
}

BOOL CBasisdatenPage::TestType ()
{
	if (Types == "") return TRUE;
	CToken t (Types, ",");
	LPTSTR p;
	while ((p = t.NextToken ()) != NULL)
	{
		int type = _tstoi (p);
		if (A_bas.a_bas.a_typ == (short) type)
		{
			a_typ = A_bas.a_bas.a_typ;
			a_typ_pos = 1;
			return TRUE;
		}
	}
	t = Types;

	while ((p = t.NextToken ()) != NULL)
	{
		int type = _tstoi (p);
		if (A_bas.a_bas.a_typ2 == (short) type)
		{
			a_typ = A_bas.a_bas.a_typ2;
			a_typ_pos = 2;
			return TRUE;
		}
	}
	return FALSE;
}

void CBasisdatenPage::ActivateTypePage ()
{
	memcpy (&a_bas, 
		    &A_bas.a_bas, 
			sizeof (a_bas));
	CWnd *mFrame = Frame->GetParent ();
	if (PageType == Standard)
	{
		mFrame->PostMessage (WM_COMMAND, RECCHANGE_MAT, 0l);
	}
	else
	{
		mFrame->PostMessage (WM_COMMAND, RECCHANGE_ART, 0l);
	}
}

void CBasisdatenPage::ActivateTypePageEx ()
{
	CWnd *mFrame = Frame->GetParent ();
	if (PageType == Standard)
	{
		mFrame->PostMessage (WM_COMMAND, ACTIVATE_ART, 0l);
	}
	else
	{
		mFrame->PostMessage (WM_COMMAND, ACTIVATE_MAT, 0l);
	}
}

BOOL CBasisdatenPage::TestAgType ()
{
	if (Types == "") return TRUE;
	CToken t (Types, ",");
	LPTSTR p;
	while ((p = t.NextToken ()) != NULL)
	{
		int type = _tstoi (p);
		if (Ag.ag.a_typ == (short) type)
		{
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CBasisdatenPage::TestMat ()
{
	A_MAT_CLASS m;

	m.a_mat.mat = A_mat.a_mat.mat;
	int dsqlstatus = m.dbreadfirst ();
	if (dsqlstatus == 0 && m.a_mat.a == A_mat.a_mat.a)
	{
		return TRUE;
	}
	else if (dsqlstatus == 0)
	{
		int ret = MessageBox (_T("Die Materialnummer ist einem anderen Artikel zugeordnet\n")
			                  _T("Verkn�pfung l�schen ?"), NULL, MB_YESNO | MB_ICONQUESTION);
		if (ret == IDNO) return FALSE;
		m.dbdelete ();
	}
	m.a_mat.a = A_mat.a_mat.a;
	dsqlstatus = m.dbreadfirsta ();
	if (dsqlstatus == 0)
	{
		m.dbdeletea ();
	}
	return TRUE;
}

void CBasisdatenPage::OnInfo ()
{
	if (m_Mdn.IsWindowEnabled ())
	{
		return;
	}

	if (_CallInfoEx == NULL)
	{
		return;
	}

	Form.Get ();
	char Item[256] = {"a"};
	char where [256];

	sprintf (where, "where a = %.0lf", A_bas.a_bas.a);

	BOOL ret = (*_CallInfoEx) (this->m_hWnd, NULL, Item, where, 0);

}

BOOL CBasisdatenPage::SendImages ()
{
	CSendToSystem dlg;
	INT_PTR ret = dlg.DoModal ();
	return TRUE;
}

void CBasisdatenPage::PrintRegEti ()
{
	CPrintRegEtiDlg dlg;
	INT_PTR ret = dlg.DoModal ();
}
void CBasisdatenPage::PrintThekeEti ()
{
	CPrintThekeEtiDlg dlg;
	INT_PTR ret = dlg.DoModal ();
}
void CBasisdatenPage::OnBsdClicked ()
{
	Form.Get ();
	if (strcmp ((LPSTR) A_bas.a_bas.bsd_kz, "J") == 0) 
	{
//		m_Lager.ShowWindow (SW_SHOWNORMAL);
		m_Lager.EnableWindow ();
	}
	else
	{
//		m_Lager.ShowWindow (SW_HIDE);
		m_Lager.EnableWindow (FALSE);
	}
}

void CBasisdatenPage::OnRecChange()
{
	if (NoRecChange)
	{
		return;
	}
	A_bas.a_bas.a = a_bas.a;
	EnableFields (FALSE);
	m_A.SetFocus ();
	Form.Show ();
	Read ();
//	m_A_bz1.PostMessage (WM_SETFOCUS, 0, 0l);
}

BOOL CBasisdatenPage::OnKillActive ()
{
	Form.Get ();
	return TRUE;
}

void CBasisdatenPage::OnEnSetfocusA()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (!m_A.IsWindowEnabled ())
	{
		m_A_bz1.SetFocus ();
	}
}


void CBasisdatenPage::OnBnClickedFildeaktiv() //FS-26
{
	Form.Get ();
	if (A_bas.a_bas.filialsperre == 1)
	{
		m_FilDeaktiv.SetWindowTextA("Artikel ist in Filialen gel�scht");
	}
	else
	{
		m_FilDeaktiv.SetWindowTextA("Artikel aus Filialsystemen l�schen");
	}
}

void CBasisdatenPage::OnBnClickedDeaktiv() //FS-26
{
	Form.Get ();
	if (A_bas.a_bas.ghsperre == 1)
	{
		m_Deaktiv.SetWindowTextA("Artikel ist Gro�handel NICHT AKTIV");
	}
	else
	{
		m_Deaktiv.SetWindowTextA("Artikel im Gro�handel deaktivieren");
	}

}

void CBasisdatenPage::OnCbnSelchangeHndGew()
{
}

void CBasisdatenPage::FillInhLief ()
{
		Karton.karton.id = A_bas_erw.a_bas_erw.karton_id;
		if (Karton.dbreadfirst () == 0)
		{
			if (Karton.karton.inh > 0.0)
			{
				A_bas.a_bas.inh_lief = Karton.karton.inh;
				m_InhLief.EnableWindow (FALSE);
			}
		}
		else
		{
		  m_InhLief.EnableWindow (TRUE);
		}
}

void CBasisdatenPage::InsA_bas_Prot (short aktivitaet)
{
	memcpy (&A_bas_prot.a_bas_prot, &a_bas_prot_null, sizeof (A_BAS_PROT));

	A_bas_prot.a_bas_prot.me_einh_kun = A_hndw.a_hndw.me_einh_kun;

	A_bas_prot.a_bas_prot.a = A_bas.a_bas.a;
	strcpy (A_bas_prot.a_bas_prot.prot_pers, Sys_ben.sys_ben.pers_nam);




	CString Date;
	CStrFuncs::SysDate (Date); 
	A_bas.ToDbDate (Date, &A_bas.a_bas.bearb);
	if (A_bas.a_bas.akv.day == 0) A_bas.ToDbDate (Date, &A_bas.a_bas.akv);
	if (A_bas.a_bas.bearb.day == 0) A_bas.ToDbDate (Date, &A_bas.a_bas.bearb);
	if (A_bas.a_bas.stand.day == 0) A_bas.ToDbDate (Date, &A_bas.a_bas.stand);

	A_bas_prot.a_bas_prot.akv = A_bas.a_bas.akv;
	A_bas_prot.a_bas_prot.bearb = A_bas.a_bas.bearb;
	A_bas_prot.a_bas_prot.stand = A_bas.a_bas.stand;


	A_bas_prot.a_bas_prot.mdn = A_bas.a_bas.mdn;
	A_bas_prot.a_bas_prot.fil = A_bas.a_bas.fil;
	A_bas_prot.a_bas_prot.a_gew = A_bas.a_bas.a_gew;
	A_bas_prot.a_bas_prot.a_typ = A_bas.a_bas.a_typ;
	A_bas_prot.a_bas_prot.a_typ2 = A_bas.a_bas.a_typ2;
	strcpy (A_bas_prot.a_bas_prot.a_bz1, A_bas.a_bas.a_bz1);
	strcpy (A_bas_prot.a_bas_prot.a_bz2, A_bas.a_bas.a_bz2);
	strcpy (A_bas_prot.a_bas_prot.best_auto, A_bas.a_bas.best_auto);
	strcpy (A_bas_prot.a_bas_prot.cp_aufschl, A_bas.a_bas.cp_aufschl);
	A_bas_prot.a_bas_prot.abt = A_bas.a_bas.abt;
	A_bas_prot.a_bas_prot.ag = A_bas.a_bas.ag;
	A_bas_prot.a_bas_prot.delstatus = A_bas.a_bas.delstatus;
	A_bas_prot.a_bas_prot.dr_folge = A_bas.a_bas.dr_folge;
	A_bas_prot.a_bas_prot.erl_kto = A_bas.a_bas.erl_kto;
	strcpy (A_bas_prot.a_bas_prot.hbk_kz, A_bas.a_bas.hbk_kz);
	strcpy (A_bas_prot.a_bas_prot.hnd_gew, A_bas.a_bas.hnd_gew);
	strcpy (A_bas_prot.a_bas_prot.kost_kz, A_bas.a_bas.kost_kz);
	A_bas_prot.a_bas_prot.hbk_ztr = A_bas.a_bas.hbk_ztr;
	A_bas_prot.a_bas_prot.hwg = A_bas.a_bas.hwg;
	A_bas_prot.a_bas_prot.me_einh = A_bas.a_bas.me_einh;
	A_bas_prot.a_bas_prot.mwst = A_bas.a_bas.mwst;
	A_bas_prot.a_bas_prot.plak_div = A_bas.a_bas.plak_div;
	A_bas_prot.a_bas_prot.sw = A_bas.a_bas.sw;
	A_bas_prot.a_bas_prot.teil_smt = A_bas.a_bas.teil_smt;
	A_bas_prot.a_bas_prot.we_kto = A_bas.a_bas.we_kto;
	A_bas_prot.a_bas_prot.wg = A_bas.a_bas.wg;
	strcpy (A_bas_prot.a_bas_prot.modif, A_bas.a_bas.modif);
	strcpy (A_bas_prot.a_bas_prot.stk_lst_kz, A_bas.a_bas.stk_lst_kz);
	A_bas_prot.a_bas_prot.zu_stoff = A_bas.a_bas.zu_stoff;
	A_bas_prot.a_bas_prot.prod_zeit = A_bas.a_bas.prod_zeit;
	A_bas_prot.a_bas_prot.gn_pkt_gbr = A_bas.a_bas.gn_pkt_gbr;
	A_bas_prot.a_bas_prot.kost_st = A_bas.a_bas.kost_st;
	A_bas_prot.a_bas_prot.a_grund = A_bas.a_bas.a_grund;
	A_bas_prot.a_bas_prot.kost_st2 = A_bas.a_bas.kost_st2;
	A_bas_prot.a_bas_prot.we_kto2 = A_bas.a_bas.we_kto2;
	A_bas_prot.a_bas_prot.charg_hand = A_bas.a_bas.charg_hand;
	strcpy (A_bas_prot.a_bas_prot.pers_nam, A_bas.a_bas.pers_nam);
	strcpy (A_bas_prot.a_bas_prot.pers_rab_kz, A_bas.a_bas.pers_rab_kz);
	strcpy (A_bas_prot.a_bas_prot.sw_pr_kz, A_bas.a_bas.sw_pr_kz);
	A_bas_prot.a_bas_prot.intra_stat = A_bas.a_bas.intra_stat;
	A_bas_prot.a_bas_prot.lief_einh = A_bas.a_bas.lief_einh;
	A_bas_prot.a_bas_prot.inh_lief = A_bas.a_bas.inh_lief;
	A_bas_prot.a_bas_prot.erl_kto_1 = A_bas.a_bas.erl_kto_1;
	A_bas_prot.a_bas_prot.erl_kto_2 = A_bas.a_bas.erl_kto_2;
	A_bas_prot.a_bas_prot.erl_kto_3 = A_bas.a_bas.erl_kto_3;
	A_bas_prot.a_bas_prot.we_kto_1 = A_bas.a_bas.we_kto_1;
	A_bas_prot.a_bas_prot.we_kto_2 = A_bas.a_bas.we_kto_2;
	A_bas_prot.a_bas_prot.we_kto_3 = A_bas.a_bas.we_kto_3;
	strcpy (A_bas_prot.a_bas_prot.qual_kng, A_bas.a_bas.qual_kng);
	strcpy (A_bas_prot.a_bas_prot.a_bz3, A_bas.a_bas.a_bz3);
	A_bas_prot.a_bas_prot.sk_vollk = A_bas.a_bas.sk_vollk;
	A_bas_prot.a_bas_prot.a_ersatz = A_bas.a_bas.a_ersatz;
	A_bas_prot.a_bas_prot.a_ers_kz = A_bas.a_bas.a_ers_kz;
	A_bas_prot.a_bas_prot.me_einh_abverk = A_bas.a_bas.me_einh_abverk;
	A_bas_prot.a_bas_prot.inh_ek = A_bas.a_bas.inh_ek;
	A_bas_prot.a_bas_prot.a_leih = A_bas.a_bas.a_leih;
	A_bas_prot.a_bas_prot.vk_gr = A_bas.a_bas.vk_gr;
	A_bas_prot.a_bas_prot.txt_nr1 = A_bas.a_bas.txt_nr1;
	A_bas_prot.a_bas_prot.txt_nr2 = A_bas.a_bas.txt_nr2;
	A_bas_prot.a_bas_prot.txt_nr3 = A_bas.a_bas.txt_nr3;
	A_bas_prot.a_bas_prot.txt_nr4 = A_bas.a_bas.txt_nr4;
	A_bas_prot.a_bas_prot.txt_nr5 = A_bas.a_bas.txt_nr5;
	A_bas_prot.a_bas_prot.txt_nr6 = A_bas.a_bas.txt_nr6;
	A_bas_prot.a_bas_prot.txt_nr7 = A_bas.a_bas.txt_nr7;
	A_bas_prot.a_bas_prot.txt_nr8 = A_bas.a_bas.txt_nr8;
	A_bas_prot.a_bas_prot.txt_nr9 = A_bas.a_bas.txt_nr9;
	A_bas_prot.a_bas_prot.txt_nr10 = A_bas.a_bas.txt_nr10;
	strcpy (A_bas_prot.a_bas_prot.skto_f, A_bas.a_bas.skto_f);
	strcpy (A_bas_prot.a_bas_prot.hnd_gew_abverk, A_bas.a_bas.hnd_gew_abverk);
	strcpy (A_bas_prot.a_bas_prot.smt, A_bas.a_bas.smt);
	A_bas_prot.a_bas_prot.allgtxt_nr1 = A_bas.a_bas.allgtxt_nr1;
	A_bas_prot.a_bas_prot.allgtxt_nr2 = A_bas.a_bas.allgtxt_nr2;
	A_bas_prot.a_bas_prot.allgtxt_nr3 = A_bas.a_bas.allgtxt_nr3;
	A_bas_prot.a_bas_prot.allgtxt_nr4 = A_bas.a_bas.allgtxt_nr4;
	A_bas_prot.a_bas_prot.fil_ktrl = A_bas.a_bas.fil_ktrl;
	A_bas_prot.a_bas_prot.sais = A_bas.a_bas.sais;
	A_bas_prot.a_bas_prot.bereich = A_bas.a_bas.bereich;
	strcpy (A_bas_prot.a_bas_prot.bild, A_bas.a_bas.bild);
	strcpy (A_bas_prot.a_bas_prot.lgr_tmpr, A_bas.a_bas.lgr_tmpr);
	strcpy (A_bas_prot.a_bas_prot.prod_mass, A_bas.a_bas.prod_mass);
	strcpy (A_bas_prot.a_bas_prot.produkt_info, A_bas.a_bas.produkt_info);
	strcpy (A_bas_prot.a_bas_prot.huel_art, A_bas.a_bas.huel_art);
	strcpy (A_bas_prot.a_bas_prot.huel_mat, A_bas.a_bas.huel_mat);
	strcpy (A_bas_prot.a_bas_prot.ausz_art, A_bas.a_bas.ausz_art);
	strcpy (A_bas_prot.a_bas_prot.ean_packung, A_bas.a_bas.ean_packung);
	strcpy (A_bas_prot.a_bas_prot.ean_karton, A_bas.a_bas.ean_karton);
	strcpy (A_bas_prot.a_bas_prot.pr_ausz, A_bas.a_bas.pr_ausz);
	strcpy (A_bas_prot.a_bas_prot.huel_peel, A_bas.a_bas.huel_peel);
	strcpy (A_bas_prot.a_bas_prot.gruener_punkt, A_bas.a_bas.gruener_punkt);
	strcpy (A_bas_prot.a_bas_prot.gebinde_art, A_bas.a_bas.gebinde_art);
	A_bas_prot.a_bas_prot.gebinde_pack = A_bas.a_bas.gebinde_pack;
	A_bas_prot.a_bas_prot.gebindeprolage = A_bas.a_bas.gebindeprolage;
	A_bas_prot.a_bas_prot.gebindeanzlagen = A_bas.a_bas.gebindeanzlagen;
	A_bas_prot.a_bas_prot.gebindepalette = A_bas.a_bas.gebindepalette;
	A_bas_prot.a_bas_prot.eiweiss = A_bas.a_bas.eiweiss;
	A_bas_prot.a_bas_prot.kh = A_bas.a_bas.kh;
	A_bas_prot.a_bas_prot.fett = A_bas.a_bas.fett;
	strcpy (A_bas_prot.a_bas_prot.gebinde_mass, A_bas.a_bas.gebinde_mass);
	strcpy (A_bas_prot.a_bas_prot.gebinde_leer_gew, A_bas.a_bas.gebinde_leer_gew);
	strcpy (A_bas_prot.a_bas_prot.gebinde_brutto_gew, A_bas.a_bas.gebinde_brutto_gew);
	strcpy (A_bas_prot.a_bas_prot.beffe, A_bas.a_bas.beffe);
	strcpy (A_bas_prot.a_bas_prot.beffe_fe, A_bas.a_bas.beffe_fe);
	strcpy (A_bas_prot.a_bas_prot.brennwert, A_bas.a_bas.brennwert);
	strcpy (A_bas_prot.a_bas_prot.aerob, A_bas.a_bas.aerob);
	strcpy (A_bas_prot.a_bas_prot.coli, A_bas.a_bas.coli);
	strcpy (A_bas_prot.a_bas_prot.keime, A_bas.a_bas.keime);
	strcpy (A_bas_prot.a_bas_prot.listerien, A_bas.a_bas.listerien);
	strcpy (A_bas_prot.a_bas_prot.list_mono, A_bas.a_bas.list_mono);
	strcpy (A_bas_prot.a_bas_prot.prodpass_extra, A_bas.a_bas.prodpass_extra);
	strcpy (A_bas_prot.a_bas_prot.glg, A_bas.a_bas.glg);
	strcpy (A_bas_prot.a_bas_prot.krebs, A_bas.a_bas.krebs);
	strcpy (A_bas_prot.a_bas_prot.ei, A_bas.a_bas.ei);
	strcpy (A_bas_prot.a_bas_prot.fisch, A_bas.a_bas.fisch);
	strcpy (A_bas_prot.a_bas_prot.erdnuss, A_bas.a_bas.erdnuss);
	strcpy (A_bas_prot.a_bas_prot.soja, A_bas.a_bas.soja);
	A_bas_prot.a_bas_prot.zerl_eti = A_bas.a_bas.zerl_eti;
	A_bas_prot.a_bas_prot.restlaufzeit = A_bas.a_bas.restlaufzeit;
	strcpy (A_bas_prot.a_bas_prot.milch, A_bas.a_bas.milch);
	strcpy (A_bas_prot.a_bas_prot.schal, A_bas.a_bas.schal);
	strcpy (A_bas_prot.a_bas_prot.sellerie, A_bas.a_bas.sellerie);
	strcpy (A_bas_prot.a_bas_prot.senfsaat, A_bas.a_bas.senfsaat);
	strcpy (A_bas_prot.a_bas_prot.sesamsamen, A_bas.a_bas.sesamsamen);
	strcpy (A_bas_prot.a_bas_prot.sulfit, A_bas.a_bas.sulfit);
	strcpy (A_bas_prot.a_bas_prot.vakuumiert, A_bas.a_bas.vakuumiert);
	strcpy (A_bas_prot.a_bas_prot.loskennung, A_bas.a_bas.loskennung);
	strcpy (A_bas_prot.a_bas_prot.version_nr, A_bas.a_bas.version_nr);
	strcpy (A_bas_prot.a_bas_prot.gueltigkeit, A_bas.a_bas.gueltigkeit);
	strcpy (A_bas_prot.a_bas_prot.weichtier, A_bas.a_bas.weichtier);
	strcpy (A_bas_prot.a_bas_prot.sonstige, A_bas.a_bas.sonstige);
	strcpy (A_bas_prot.a_bas_prot.gmo_gvo, A_bas.a_bas.gmo_gvo);
	strcpy (A_bas_prot.a_bas_prot.shop_kz, A_bas.a_bas.shop_kz);
	A_bas_prot.a_bas_prot.min_bestellmenge = A_bas.a_bas.min_bestellmenge;
	A_bas_prot.a_bas_prot.staffelung = A_bas.a_bas.staffelung;
	A_bas_prot.a_bas_prot.inh_karton = A_bas.a_bas.inh_karton;
	A_bas_prot.a_bas_prot.filialsperre = A_bas.a_bas.filialsperre;
	A_bas_prot.a_bas_prot.ghsperre = A_bas.a_bas.ghsperre;
	A_bas_prot.a_bas_prot.pack_vol = A_bas.a_bas.pack_vol;


	A_bas_prot.dbupdate (aktivitaet);

	
}
