#ifndef _A_HUEL_DEF
#define _A_HUEL_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_HUEL {
   double         a;
   long           bem_offs;
   short          delstatus;
   short          farbe;
   short          fil;
   double         fuel_gew;
   long           fuel_vol;
   TCHAR          huel_txt[49];
   TCHAR          kal[9];
   short          mdn;
   double         sw;
   TCHAR          tmpr_max[9];
};
extern struct A_HUEL a_huel, a_huel_null;

#line 8 "a_huel.rh"

class A_HUEL_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_HUEL a_huel;  
               A_HUEL_CLASS () : DB_CLASS ()
               {
               }
};
#endif
