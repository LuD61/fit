// BestDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "BestDialog.h"
#include "UniFormField.h"

extern int StdCellHeight;

#define WM_MDN_FOCUS WM_USER+10

// CBestDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CBestDialog, CDialog)

CBestDialog::CBestDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CBestDialog::IDD, pParent)
{
	ChoiceMdn = NULL;
	ChoiceFil = NULL;
	A_bas = NULL;
}

CBestDialog::~CBestDialog()
{
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
		ChoiceMdn = NULL;
	}
	if (ChoiceFil != NULL)
	{
		delete ChoiceFil;
		ChoiceFil = NULL;
	}
}

void CBestDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BEST_GROUP, m_BestGroup);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LFIL, m_LFil);
	DDX_Control(pDX, IDC_FIL, m_Fil);
	DDX_Control(pDX, IDC_FIL_NAME, m_FilName);

	DDX_Control(pDX, IDC_LBSD_MIN, m_LBsdMin);
	DDX_Control(pDX, IDC_BSD_MIN,  m_BsdMin);
	DDX_Control(pDX, IDC_LBSD_MIN_EMP, m_LBsdMinEmp);
	DDX_Control(pDX, IDC_BSD_MIN_EMP,  m_BsdMinEmp);
	DDX_Control(pDX, IDC_LBSD_MAX_EMP, m_LBsdMaxEmp);
	DDX_Control(pDX, IDC_BSD_MAX_EMP,  m_BsdMaxEmp);
	DDX_Control(pDX, IDC_BEST_AUTO,    m_BestAuto);
	DDX_Control(pDX, IDC_LLIEF,        m_LLief);
	DDX_Control(pDX, IDC_LIEF,         m_Lief);
	DDX_Control(pDX, IDC_LLIEF_BEST,   m_LLiefBest);
	DDX_Control(pDX, IDC_LIEF_BEST,    m_LiefBest);
	DDX_Control(pDX, IDC_LLETZT_LIEF_NR,  m_LLetztLiefNr);
	DDX_Control(pDX, IDC_LETZT_LIEF_NR,   m_LetztLiefNr);
}

BEGIN_MESSAGE_MAP(CBestDialog, CDialog)
	ON_WM_SIZE ()
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
	ON_BN_CLICKED(IDC_FILCHOICE , OnFilchoice)
	ON_BN_CLICKED(IDC_BESTSAVE ,   OnSave)
	ON_BN_CLICKED(IDC_BESTDELETE , Delete)
END_MESSAGE_MAP()

// CBestDialog-Meldungshandler

BOOL CBestDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
    m_Save.Create (_T("Speichern"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_BESTSAVE);
	m_Save.LoadBitmap (IDB_SAVE);
	m_Save.SetToolTip (_T("speichern"));

    m_Delete.Create (_T("l�schen"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_BESTDELETE);
	m_Delete.LoadBitmap (IDB_DELETE);
	m_Delete.LoadMask (IDB_DELETE_MASK);
	m_Delete.SetToolTip (_T("l�schen"));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

    Keys.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Keys.Add (new CFormField (&m_Fil,EDIT,        (short *) &Fil.fil.fil, VSHORT));

    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Fil,EDIT,        (short *) &Fil.fil.fil, VSHORT));
    Form.Add (new CUniFormField (&m_FilName,EDIT, (char *) FilAdr.adr.adr_krz, VCHAR));

    Form.Add (new CFormField (&m_BsdMin,EDIT, (double *) &A_best.a_best.bsd_min, VDOUBLE, 9,3));
    Form.Add (new CFormField (&m_BsdMinEmp,EDIT, (double *) &A_best.a_best.bsd_min_emp, VDOUBLE, 9,3));
    Form.Add (new CFormField (&m_BsdMaxEmp,EDIT, (double *) &A_best.a_best.bsd_max_emp, VDOUBLE, 9,3));
    Form.Add (new CFormField (&m_BestAuto,CHECKBOX, (char *) A_best.a_best.best_auto, VCHAR));
    Form.Add (new CUniFormField (&m_Lief,EDIT,      (char *) A_best.a_best.lief, VCHAR));
    Form.Add (new CUniFormField (&m_LiefBest,EDIT,  (char *) A_best.a_best.lief_best, VCHAR));
    Form.Add (new CUniFormField (&m_LetztLiefNr,EDIT, (char *) A_best.a_best.letzt_lief_nr, VCHAR));

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 0, 12, 20);
    CtrlGrid.SetCellHeight (StdCellHeight);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    KeyGrid.Create (this, 20, 20);
	KeyGrid.SetFont (&Font);
    KeyGrid.SetBorder (12, 0, 12, 20);
    KeyGrid.SetCellHeight (StdCellHeight);
    KeyGrid.SetFontCellHeight (this, &Font);
    KeyGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

//Grid Mdn
	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

//Grid Fil
	FilGrid.Create (this, 2, 2);
    FilGrid.SetBorder (0, 0);
    FilGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Fil = new CCtrlInfo (&m_Fil, 0, 0, 1, 1);
	FilGrid.Add (c_Fil);
	CtrlGrid.CreateChoiceButton (m_FilChoice, IDC_FILCHOICE, this);
	CCtrlInfo *c_FilChoice = new CCtrlInfo (&m_FilChoice, 1, 0, 1, 1);
	FilGrid.Add (c_FilChoice);

//Grid Toolbar
	ToolGrid.Create (this, 2, 2);
    ToolGrid.SetBorder (0, 0);
    ToolGrid.SetGridSpace (0, 0);

	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 0, 1, 1);
	ToolGrid.Add (c_Save);

	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 1, 0, 1, 1);
	ToolGrid.Add (c_Delete);

	CCtrlInfo *c_ToolGrid = new CCtrlInfo (&ToolGrid, 1, 0, 1, 1);
	c_ToolGrid->SetCellPos (0, 0, 0, 0);
	CtrlGrid.Add (c_ToolGrid);

//Gruppe
	CCtrlInfo *c_BestGroup     = new CCtrlInfo (&m_BestGroup, 0, 1, DOCKRIGHT, DOCKBOTTOM); 
	c_BestGroup->rightspace = 5;
	CtrlGrid.Add (c_BestGroup);

//Mandant
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	KeyGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	KeyGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	KeyGrid.Add (c_MdnName);

	CCtrlInfo *c_LFil     = new CCtrlInfo (&m_LFil, 0, 1, 1, 1); 
	KeyGrid.Add (c_LFil);
	CCtrlInfo *c_FilGrid   = new CCtrlInfo (&FilGrid, 1, 1, 1, 1); 
	KeyGrid.Add (c_FilGrid);
	CCtrlInfo *c_FilName     = new CCtrlInfo (&m_FilName, 2, 1, 1, 1); 
	KeyGrid.Add (c_FilName);

	CCtrlInfo *c_KeyGrid     = new CCtrlInfo (&KeyGrid, 1, 2, 4, 1); 
	CtrlGrid.Add (c_KeyGrid);

//Mindestbestand
	CCtrlInfo *c_LBsdMin     = new CCtrlInfo (&m_LBsdMin, 1, 5, 1, 1); 
	CtrlGrid.Add (c_LBsdMin);
	CCtrlInfo *c_BsdMin   = new CCtrlInfo (&m_BsdMin, 2, 5, 1, 1); 
	CtrlGrid.Add (c_BsdMin);

//empfohlener Mindestbestand
	CCtrlInfo *c_LBsdMinEmp     = new CCtrlInfo (&m_LBsdMinEmp, 1, 6, 1, 1); 
	CtrlGrid.Add (c_LBsdMinEmp);
	CCtrlInfo *c_BsdMinEmp   = new CCtrlInfo (&m_BsdMinEmp, 2, 6, 1, 1); 
	CtrlGrid.Add (c_BsdMinEmp);

//empfohlener Maximalbestand
	CCtrlInfo *c_LBsdMaxEmp     = new CCtrlInfo (&m_LBsdMaxEmp, 1, 7, 1, 1); 
	CtrlGrid.Add (c_LBsdMaxEmp);
	CCtrlInfo *c_BsdMaxEmp   = new CCtrlInfo (&m_BsdMaxEmp, 2, 7, 1, 1); 
	CtrlGrid.Add (c_BsdMaxEmp);

// automatische Bestellung
	CCtrlInfo *c_BestAuto   = new CCtrlInfo (&m_BestAuto, 1, 9, 3, 1); 
	CtrlGrid.Add (c_BestAuto);

// Hauptlieferant
	CCtrlInfo *c_LLief     = new CCtrlInfo (&m_LLief, 1, 11, 1, 1); 
	CtrlGrid.Add (c_LLief);
	CCtrlInfo *c_Lief   = new CCtrlInfo (&m_Lief, 2, 11, 1, 1); 
	CtrlGrid.Add (c_Lief);

// Bestellnummer
	CCtrlInfo *c_LLiefBest     = new CCtrlInfo (&m_LLiefBest, 1, 12, 1, 1); 
	CtrlGrid.Add (c_LLiefBest);
	CCtrlInfo *c_LiefBest   = new CCtrlInfo (&m_LiefBest, 2, 12, 1, 1); 
	CtrlGrid.Add (c_LiefBest);

// Letzter Lieferant
	CCtrlInfo *c_LLetztLiefNr     = new CCtrlInfo (&m_LLetztLiefNr, 1, 13, 1, 1); 
	CtrlGrid.Add (c_LLetztLiefNr);
	CCtrlInfo *c_LetztLiefNr   = new CCtrlInfo (&m_LetztLiefNr, 2, 13, 1, 1); 
	CtrlGrid.Add (c_LetztLiefNr);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Save.Tooltip.SetFont (&Font);
	m_Save.Tooltip.SetSize ();
	m_Delete.Tooltip.SetFont (&Font);
	m_Delete.Tooltip.SetSize ();

	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	memcpy (&Fil.fil, &fil_null, sizeof (FIL));
	memcpy (&FilAdr.adr, &adr_null, sizeof (ADR));
	memcpy (&A_best.a_best, &a_best_null, sizeof (A_BEST));

	CtrlGrid.Display ();
	Form.Show ();

	ReadMdn ();
//	ReadFil ();
	if (Mdn.mdn.mdn != 0)
	{
		m_Mdn.EnableWindow (FALSE);
		m_MdnChoice.EnableWindow (FALSE);
	}

	if (Fil.fil.fil != 0)
	{
		m_Fil.EnableWindow (FALSE);
		m_FilChoice.EnableWindow (FALSE);
	}

	return TRUE;
}


void CBestDialog::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CBestDialog::PreTranslateMessage(MSG* pMsg)
{
//	CWnd *cWnd = NULL;
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
                if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				Control = GetNextDlgTabItem (Control, TRUE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				OnSave ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Fil)
				{
					OnFilchoice ();
					return TRUE;
				}
				return TRUE;
			}
			break;
		case WM_MDN_FOCUS:
			m_Mdn.SetFocus ();
			return TRUE;
	}
	return FALSE;
}

BOOL CBestDialog::OnReturn ()
{
	CWnd *Control = GetFocus ();
	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}
	if (Control == &m_Fil)
	{
		if (!Read ())
		{
			m_Fil.SetFocus ();
			return FALSE;
		}
	}
	if (Control == &m_BestAuto)
	{
		OnSave ();
		return TRUE;
	}
	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return FALSE;
	}
	return TRUE;
}

BOOL CBestDialog::ReadMdn ()
{
	int dsqlstatus = 0;

	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();

	if (Mdn.mdn.mdn == 0)
	{
		_tcscpy (MdnAdr.adr.adr_krz, _T("Zentrale"));
		m_Fil.EnableWindow (FALSE);
		m_FilChoice.EnableWindow (FALSE);
		Form.Show ();
		Read ();
		return TRUE;
	}
	
	dsqlstatus = Mdn.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Fil.EnableWindow ();
		m_FilChoice.EnableWindow ();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden (sqlstatus %d) "),Mdn.mdn.mdn, dsqlstatus);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CBestDialog::Read ()
{
	memcpy (&Fil.fil, &fil_null, sizeof (FIL));
	memcpy (&A_best.a_best, &a_best_null, sizeof (A_BEST));
	Keys.Get ();
    Fil.fil.mdn = Mdn.mdn.mdn;
	if (Fil.fil.fil != 0)
	{
		if (Fil.dbreadfirst () == 100)
		{
			MessageBox (_T("Filiale nicht angelegt"), _T(""), MB_OK | MB_ICONERROR);
			m_Fil.SetSel (0, -1, TRUE);
			m_Fil.SetFocus ();
			return FALSE;
		}
	}
	Form.Show ();

    A_best.a_best.mdn  = Mdn.mdn.mdn;
	A_best.a_best.fil  = Fil.fil.fil;
    A_best.a_best.a    = A_bas->a_bas.a;
	int dsqlstatus = A_best.dbreadfirst ();
	Form.Show ();
	return TRUE;
}

void CBestDialog::OnSave ()
{
	Form.Get ();
    A_best.a_best.mdn  = Mdn.mdn.mdn;
	A_best.a_best.fil  = Fil.fil.fil;
    A_best.a_best.a    = A_bas->a_bas.a;
	A_best.dbupdate ();
	Form.Show ();
	PostMessage (WM_MDN_FOCUS, 0, 0l);
}

void CBestDialog::Delete ()
{
	Form.Get ();
	if (MessageBox (_T("Bestelldaten l�schen ?"), "", MB_YESNO | MB_ICONQUESTION) != IDYES)
	{
		return;
	}
    A_best.a_best.mdn  = Mdn.mdn.mdn;
	A_best.a_best.fil  = Fil.fil.fil;
    A_best.a_best.a    = A_bas->a_bas.a;
	A_best.dbdelete ();
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&Fil.fil, &fil_null, sizeof (FIL));
	memcpy (&A_best.a_best, &a_best_null, sizeof (A_BEST));
	Form.Show ();
	PostMessage (WM_MDN_FOCUS, 0, 0l);
}

void CBestDialog::OnMdnchoice ()
{

	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = TRUE;
		ChoiceMdn->CreateDlg ();
	}

    ChoiceMdn->SetDbClass (&Mdn);
	ChoiceMdn->DoModal();
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CBestDialog::OnFilchoice ()
{

	if (ChoiceFil == NULL)
	{
		ChoiceFil = new CChoiceFil (this);
	    ChoiceFil->IsModal = TRUE;
		ChoiceFil->CreateDlg ();
	}

    ChoiceFil->SetDbClass (&Mdn);
	ChoiceFil->DoModal();
    if (ChoiceFil->GetState ())
    {
		  CFilList *abl = ChoiceFil->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Fil.fil, &fil_null, sizeof (FIL));
		  memcpy (&FilAdr.adr, &fil_null, sizeof (ADR));
		  Fil.fil.mdn = Mdn.mdn.mdn;
		  Fil.fil.fil = abl->fil;
		  Form.Show ();
		  Read ();
		  m_Fil.SetSel (0, -1, TRUE);
		  m_Fil.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}
