#include "StdAfx.h"
#include "prprot.h"
#include "StrFuncs.h"

CPrProt::CPrProt(void)
{
	PersName = "";
	Programm = "";
	A_pr = NULL;
}

CPrProt::CPrProt(CString& PersNam, CString& Programm, APR_CLASS *A_pr)
{
	this->PersName = PersNam;
	this->Programm = Programm;
	this->A_pr = A_pr;
}

CPrProt::~CPrProt(void)
{
}

void CPrProt::Construct (CString& PersNam, CString& Programm, APR_CLASS *A_pr)
{
	this->PersName = PersNam;
	this->Programm = Programm;
	this->A_pr = A_pr;
}

void CPrProt::Write (double pr_ek_alt, double pr_vk_alt)
{
	if (A_pr == NULL) return;

	memcpy (&Pr_prot.pr_prot, &pr_prot_null, sizeof  (PR_PROT));
	CString Date;
	CStrFuncs::SysDate (Date);
	Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.bearb);
	CString Time;
	CStrFuncs::SysTime (Time);

//	strcpy (Pr_prot.pr_prot.zeit, Time.GetBuffer (8));
	strcpy (Pr_prot.pr_prot.pers_nam, PersName.GetBuffer (8));
    Pr_prot.pr_prot.mdn_gr = A_pr->a_pr.mdn_gr;
    Pr_prot.pr_prot.mdn = A_pr->a_pr.mdn;
    Pr_prot.pr_prot.fil_gr = A_pr->a_pr.fil_gr;
    Pr_prot.pr_prot.fil = A_pr->a_pr.fil;
    Pr_prot.pr_prot.a = A_pr->a_pr.a;
    Pr_prot.pr_prot.pr_vk = A_pr->a_pr.pr_vk;
    Pr_prot.pr_prot.pr_vk_alt = pr_vk_alt;
    Pr_prot.pr_prot.pr_vk_euro = A_pr->a_pr.pr_vk_euro;
    Pr_prot.pr_prot.pr_vk_alt_euro = pr_vk_alt;
    Date = "31.12.1899";
    Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.lad_akt_von);
    Pr_prot.ToDbDate (Date, &Pr_prot.pr_prot.lad_akt_bis);
    Pr_prot.dbinsert ();
}

