#pragma once
#include "afxdtctl.h"
#include "afxwin.h"
#ifdef ARTIKEL
#include "Artikel.h"
#else
#include "AprMan.h"
#endif
#include "ChoiceAg.h"
#include "ChoiceWg.h"
#include "ChoiceHwg.h"
#include "ChoiceA.h"
#include "FormTab.h"
#include "Etikett.h"
#include "Ptabn.h"
#include "a_bas.h"


// CPrintRegEtiDlg-Dialogfeld

class CPrintRegEtiDlg : public CDialog
{
	DECLARE_DYNAMIC(CPrintRegEtiDlg)

public:
	CPrintRegEtiDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CPrintRegEtiDlg();

// Dialogfelddaten
	enum { IDD = IDD_PRINT_REG_ETI };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();    // DDX/DDV-Unterstützung
	virtual void OnOK ();

	DECLARE_MESSAGE_MAP()
public:
	CFormTab Form;
	CEtikett Etikett;
	CDateTimeCtrl m_EtiDat;
	CComboBox m_EtiType;
	CButton m_ArtStm;
	CButton m_Price;
	CButton m_Changes;
	CEdit m_Hwg;
	CEdit m_Wg;
	CEdit m_Ag;
	CEdit m_A;
	CEdit m_AFrom;
	CEdit m_ATo;
    A_BAS_CLASS *A_bas;
	short Anzahl;


	CString EtiDat;
	short EtiType;
	CString DatFrom;
	CString DatTo;
	CString Hwg;
	CString Wg;
	CString Ag;
	CString A;

	CString AFrom;
	CString ATo;

	CChoiceHWG *ChoiceHWG;
	CChoiceWG *ChoiceWG;
	CChoiceAG *ChoiceAG;
	CChoiceA *ChoiceA;

	PTABN_CLASS Ptabn;

	afx_msg void OnChoiceHwg();
	afx_msg void OnChoiceWg();
	afx_msg void OnChoiceAg();
	afx_msg void OnChoiceA();
	void EnableChoiceFields (BOOL b=TRUE);
	afx_msg void OnBnClickedChanges();
	CDateTimeCtrl m_DatFrom;
	CDateTimeCtrl m_DatTo;
	void FillEtiTypeCombo (CComboBox *Control, LPTSTR Item);
};
