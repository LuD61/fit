#pragma once
#include "BasisDatenPage.h"
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "a_bas.h"
#include "a_bas_erw.h"
#include "a_hndw.h"
#include "a_eig.h"
#include "ptabn.h"
#include "atext.h"
#include "DbUniCode.h"
#include "afxwin.h"
#include "vector"
#include "prp_mibwerte.h"
#include "prp_cpp.h"
#include "prp_vik.h"
#include "PrpUserListCtrl.h"
#include "mo_progcfg.h"

#define MAXENTRIES 30

// CProductPage2-Dialogfeld

class CProductPage2 : public CDbPropertyPage
{
	DECLARE_DYNAMIC(CProductPage2)

public:
	CProductPage2();
	virtual ~CProductPage2();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCT_PAGE2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()
public:
	int tabx;
	int taby;
	BOOL HideButtons;
	BOOL UserFields;
	CBasisdatenPage *Basis;
	CWnd *Frame;
	CFormTab Form;
	CFormTab DisableEigs;
	CCtrlGrid CtrlGrid;
	CFont Font;
	CFont BoldFont;
	COLORREF BoldColor;
	PROG_CFG Cfg;
	A_BAS_CLASS *A_bas;
	A_BAS_ERW_CLASS *A_bas_erw;
	PRP_MIBWERTE_CLASS Prp_mibwerte;
	PRP_CPP_CLASS Prp_cpp;
	PRP_VIK_CLASS Prp_vik;
	BOOL ProductPage2Read;
	PTABN_CLASS Ptabn;


	CStatic m_LHuel;
	CStatic m_LHuelArt;
	CTextEdit m_HuelArt;
	CStatic m_LHuelMat;
	CTextEdit m_HuelMat;
	CStatic m_LAuszArt;
	CTextEdit m_AuszArt;
	CStatic m_LEanPackung;
	CTextEdit m_EanPackung;
	CStatic m_LEanKarton;
	CTextEdit m_EanKarton;
	CStatic m_LPrAusz;
	CTextEdit m_PrAusz;
	CButton m_Vakuumiert;
	CButton m_HuelPeel;
	CButton m_GruenerPunkt;
	CStatic m_LGebinde;
	CStatic m_LGebindeArt;
	CTextEdit m_GebindeArt;
	CStatic m_LGebindeGroesse;
	CTextEdit m_GebindeGroesse;
	CStatic m_LGebindeLeerGew;
	CTextEdit m_GebindeLeerGew;
	CStatic m_LGebindeBruttoGew;
	CTextEdit m_GebindeBruttoGew;
	CStatic m_LGebindePack;
	CNumEdit m_GebindePack;
	CStatic m_LGebindeProLage;
	CNumEdit m_GebindeProLage;
	CStatic m_LGebindeAnzLagen;
	CNumEdit m_GebindeAnzLagen;
	CStatic m_LGebindePalette;
	CNumEdit m_GebindePalette;
	CStatic m_LChemPhysParam;
	CStatic m_LBeffe;
	CTextEdit m_Beffe;
	CStatic m_LBeffeFe;
	CTextEdit m_BeffeFe;
	CStatic m_LVik;

	CStatic m_LNaehrWert;
	CStatic m_LNaehrWertText;
	CStatic m_LBrennWert;
	CTextEdit m_BrennWert;
	CStatic m_LEiWeiss;
	CNumEdit m_EiWeiss;
	CStatic m_LKh;
	CNumEdit m_Kh;
	CStatic m_LFett;
	CNumEdit m_Fett;
	CStatic m_LMikro;
	CStatic m_LAerob;
	CTextEdit m_Aerob;
	CStatic m_LColi;
	CTextEdit m_Coli;
	CStatic m_LKeime;
	CTextEdit m_Keime;
	CStatic m_LListerien;
	CTextEdit m_Listerien;
	CStatic m_LListMono;
	CTextEdit m_ListMono;
	CStatic m_LProdPassExtra;
	CTextEdit m_ProdPassExtra;
	CTextEdit m_LGmoGvoStatus;
	CTextEdit m_GmoGvoStatus;
	CPrpUserListCtrl m_MibwList;
	CFillList FillList;
	CPrpUserListCtrl m_CppList;
	CFillList CppList;
	CPrpUserListCtrl m_VikList;
	CFillList VikList;

	CString MibNames[MAXENTRIES];
	char *MibRecordNames [MAXENTRIES];
	char *MibValues[MAXENTRIES];

	CString CppNames[MAXENTRIES];
	char *CppRecordNames [MAXENTRIES];
	char *CppValues[MAXENTRIES];

	CString VikNames[MAXENTRIES];
	char *VikRecordNames [MAXENTRIES];
	char *VikValues[MAXENTRIES];

	virtual void UpdatePage ();
	virtual void OnDelete ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeydown ();
    virtual BOOL OnKeyup ();
	virtual BOOL OnKillActive ();
	virtual BOOL OnSetActive ();
	BOOL Read ();
	void FillNames (CString Names[], char *RecordNames []);
	void ToRecordNames (char *RecordNames [], char *Values[], CString Names[]);
	void FromRecordNames (char *RecordNames[], CString Names[]);
	BOOL Write ();
	BOOL ListActive (WPARAM Key);
	void ReadCfg ();
    virtual void OnCopy ();
    virtual void OnPaste ();
	void FillHuelleCombo ();

	CButton m_Schutzgas;
	CStatic m_Huelle;
	CComboBox m_CHuelle;
	CStatic m_LSalz;
	CNumEdit m_Salz;
	CStatic m_LDavonFetts;
	CNumEdit m_DavonFetts;
	CStatic m_LDavonZucker;
	CNumEdit m_DavonZucker;
	CStatic m_LBallaststoff;
	CNumEdit m_Ballaststoff;
	CStatic m_LBH;
	afx_msg void OnStnClickedLdavonfetts();
};

