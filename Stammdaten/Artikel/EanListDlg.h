#pragma once
#include "afxcmn.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "afxwin.h"
#include "EanListCtrl.h"
#include "A_ean.h"
#include "A_krz.h"
#include "A_bas.h"
#include "A_hndw.h"
#include "BasisdatenPage.h"
#include "UpdateEvent.h"
#include "PageUpdate.h"
#include "vector"
#include "Sys_par.h"


// CEanListDlg-Dialogfeld

class CEanListDlg : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CEanListDlg)

public:
	CEanListDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CEanListDlg();

// Dialogfelddaten
	enum { IDD = IDD_EAN_LIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual void OnSize (UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
public:
	int dcs_a_par;
	int waa_a_par;
	CCtrlGrid CtrlGrid;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	CFormTab Form;
	CFont Font;
	CFont LFont;
	A_BAS_CLASS *A_bas;
	A_HNDW_CLASS *A_hndw;
	A_EAN_CLASS *A_ean;
	A_KRZ_CLASS *A_krz;
	SYS_PAR_CLASS *Sys_par;
	CBasisdatenPage *Basis;
	CPageUpdate *PageUpdate;

	CEanListCtrl m_EanList;

	CStatic m_LEanText;
	void SetBasis (CBasisdatenPage *Basis);
	void SetSelectedItem (int idx);
	virtual void Update ();
	virtual void Read ();
	virtual void Delete ();
	BOOL HasFocus ();
	afx_msg void OnLvnItemchangedEanList(NMHDR *pNMHDR, LRESULT *pResult);
	void SetSysPar();
	virtual void DeleteRow ();
};
