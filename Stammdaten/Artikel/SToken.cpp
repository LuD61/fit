#include "StdAfx.h"
#include "SToken.h"

CSToken::CSToken(void)
{
	m_Pos = NULL;
}

CSToken::~CSToken(void)
{
}

CSToken::CSToken (char *str, char *sep)
{

     Tokens = NULL;
     Buffer = str;
     Buffer.TrimRight ();
     this->sep = sep;
     GetTokens (str);
}

CSToken::CSToken (CString& Str, char *sep)
{

     Tokens = NULL;
     Buffer = Str;
     Buffer.TrimRight ();
     this->sep = sep;
     GetTokens (Str.GetBuffer ());
}

char *CSToken::strtok (char *Buffer, const char *sep)
{
	char *pos;
	BOOL IsQuot;
	BOOL IsSep;
	int i;
	if (Buffer != NULL)
	{
		m_Pos = Buffer;
	}

	if (m_Pos == NULL)
	{
		return NULL;
	}
	if (*m_Pos == 0)
	{
		return NULL;
	}

	pos = m_Pos;
	IsQuot = FALSE;
	IsSep = FALSE;
	while (*m_Pos != 0)
	{
		if (IsQuot)
		{
			if (*m_Pos == '\"')
			{
				IsQuot = FALSE;
				m_Pos += 1;
			}
			else
			{
				m_Pos += 1;
				continue;
			}
		}
		if (*m_Pos == '\"')
		{
			IsQuot = TRUE;
			m_Pos += 1;
			continue;
		}
		for (i = 0; sep[i] != 0; i ++)
		{
			if (*m_Pos == sep[i])
			{
				break;
			}
		}

		if (sep[i] == 0)
		{
			if (IsSep)
			{
				break;
			}
		}
		else
		{
			IsSep = TRUE;
			if (*m_Pos == 0)
			{
				break;
			}
			*m_Pos = 0;
		}
		m_Pos += 1;
		if ((*m_Pos != 0) && IsSep)
		{
			for (i = 0; sep[i] != 0; i ++)
			{
				if (*m_Pos != sep[i])
				{
					IsSep = FALSE;
					break;
				}
			}
			if (!IsSep)
			{
				break;
			}
		}
	}
	return pos;
}

void CSToken::GetTokens (char *Txt)
{
     AnzToken = 0;
     Buffer.TrimRight ();
     char *b = Buffer.GetBuffer (512);
     char *p = strtok (b, sep.GetBuffer (512));
     while (p != NULL)
     {
             AnzToken ++;
             p = strtok (NULL, sep.GetBuffer (512));
     }
     Tokens = new CString *[AnzToken];
     if (Tokens == NULL)
     {
             return;
     }

     Buffer = Txt;
     Buffer.TrimRight ();
     b = Buffer.GetBuffer (0);
     p = strtok (b, sep.GetBuffer (0));
	 if (*p == '\"')
	 {
		 p += 1;

	 }
	 if (p[strlen(p) - 1] == '\"')
	 {
		 p[strlen(p) - 1] = 0;
	 }
     int i = 0;
     while (p != NULL)
     {
             Tokens[i] = new CString (p);
             p = strtok (NULL, sep.GetBuffer (512));
			 if (p != 0)
			 {
				 if (*p == '\"')
				 {
					 p += 1;

				 }
				 if (p[strlen(p) - 1] == '\"')
				 {
					 p[strlen(p) - 1] = 0;
				 }
			 }
             i ++;
     }
     AktToken = 0;
}
