// LagerDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "LagerDialog.h"
#include "UniFormField.h"

extern int StdCellHeight;

#define WM_MDN_FOCUS WM_USER+10

// CLagerDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CLagerDialog, CDialog)

CLagerDialog::CLagerDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLagerDialog::IDD, pParent)
{
	ChoiceMdn = NULL;
	ChoiceLgr = NULL;
	ChoiceA = NULL;
	A_bas = NULL;
	FirstLgrCursor = -1;
	FirstLgrCursorMdn = -1;
	DelFlgHauptLgr = -1;
}

CLagerDialog::~CLagerDialog()
{
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
		ChoiceMdn = NULL;
	}
	if (ChoiceLgr != NULL)
	{
		delete ChoiceLgr;
		ChoiceLgr = NULL;
	}
	if (ChoiceA != NULL)
	{
		delete ChoiceA;
		ChoiceA = NULL;
	}
	if (FirstLgrCursor != -1)
	{
		A_lgr.sqlclose (FirstLgrCursor);
	}
	if (FirstLgrCursorMdn != -1)
	{
		A_lgr.sqlclose (FirstLgrCursorMdn);
	}
	if (DelFlgHauptLgr != -1)
	{
		A_lgr.sqlclose (DelFlgHauptLgr);
	}
}

void CLagerDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LGR_GROUP, m_LgrGroup);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LLGR, m_LLgr);
	DDX_Control(pDX, IDC_LGR, m_Lgr);
	DDX_Control(pDX, IDC_LGR_BZ, m_LgrBz);
	DDX_Control(pDX, IDC_LLGR_PLATZ, m_LLgrPlatz);
	DDX_Control(pDX, IDC_LGR_PLATZ, m_LgrPlatz);
	DDX_Control(pDX, IDC_LMIN_BESTAND, m_LMinBestand);
	DDX_Control(pDX, IDC_MIN_BESTAND, m_MinBestand);
	DDX_Control(pDX, IDC_LMELD_BESTAND, m_LMeldBestand);
	DDX_Control(pDX, IDC_MELD_BESTAND, m_MeldBestand);
	DDX_Control(pDX, IDC_LHOECHST_BESTAND, m_LHoechstBestand);
	DDX_Control(pDX, IDC_HOECHST_BESTAND, m_HoechstBestand);
	DDX_Control(pDX, IDC_LBUCH_ARTIKEL, m_LBuchArtikel);
	DDX_Control(pDX, IDC_BUCH_ARTIKEL, m_BuchArtikel);
	DDX_Control(pDX, IDC_ABZ1, m_BuchBz1);
	DDX_Control(pDX, IDC_HAUPTLAGER, m_hauptlager);
}

BEGIN_MESSAGE_MAP(CLagerDialog, CDialog)
	ON_WM_SIZE ()
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
	ON_BN_CLICKED(IDC_LGRCHOICE , OnLgrchoice)
	ON_BN_CLICKED(IDC_ACHOICE , OnAchoice)
	ON_BN_CLICKED(IDC_LGRSAVE ,   OnSave)
	ON_BN_CLICKED(IDC_LGRDELETE , Delete)
	ON_EN_KILLFOCUS(IDC_BUCH_ARTIKEL, &CLagerDialog::OnEnKillfocusBuchArtikel)
	ON_BN_CLICKED(IDC_HAUPTLAGER, &CLagerDialog::OnBnClickedHauptlager)
END_MESSAGE_MAP()

// CLagerDialog-Meldungshandler

BOOL CLagerDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	memcpy (&BuchA_bas.a_bas, &a_bas_null, sizeof (A_BAS));
    m_Save.Create (_T("Speichern"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_LGRSAVE);
	m_Save.LoadBitmap (IDB_SAVE);
	m_Save.SetToolTip (_T("speichern"));

    m_Delete.Create (_T("l�schen"), WS_CHILD | WS_VISIBLE | SS_BLACKRECT, 
		                          CRect (0, 0, 40, 32),
		                          this, IDC_LGRDELETE);
	m_Delete.LoadBitmap (IDB_DELETE);
	m_Delete.LoadMask (IDB_DELETE_MASK);
	m_Delete.SetToolTip (_T("l�schen"));

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

    Keys.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Keys.Add (new CFormField (&m_Lgr,EDIT,        (short *) &Lgr.lgr.lgr, VSHORT));

    Form.Add (new CFormField (&m_Mdn,EDIT,        (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT, (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_Lgr,EDIT,        (short *) &Lgr.lgr.lgr, VSHORT));
    Form.Add (new CUniFormField (&m_LgrBz,EDIT,   (char *) Lgr.lgr.lgr_bz, VCHAR));
	Form.Add (new CFormField (&m_hauptlager,CHECKBOX,   (char *) A_lgr.a_lgr.haupt_lgr, VCHAR));
    Form.Add (new CUniFormField (&m_LgrPlatz,EDIT,(char *) A_lgr.a_lgr.lgr_platz, VCHAR));
    Form.Add (new CFormField (&m_MinBestand,EDIT, (double *) &A_lgr.a_lgr.min_bestand, VDOUBLE, 9,3));
    Form.Add (new CFormField (&m_MeldBestand,EDIT, (double *) &A_lgr.a_lgr.meld_bestand, VDOUBLE, 9,3));
    Form.Add (new CFormField (&m_HoechstBestand,EDIT, (double *) &A_lgr.a_lgr.hoechst_bestand, VDOUBLE, 9,3));
	Form.Add (new CFormField (&m_BuchArtikel,EDIT, (double *) &A_lgr.a_lgr.buch_artikel, VDOUBLE, 13,0));
	Form.Add (new CFormField (&m_BuchBz1,EDIT,     (char *) BuchA_bas.a_bas.a_bz1, VCHAR));

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (12, 0, 12, 20);
    CtrlGrid.SetCellHeight (StdCellHeight);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

//Grid Mdn
	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

//Grid BuchArtikel
	BuchArtikelGrid.Create (this, 2, 2);
    BuchArtikelGrid.SetBorder (0, 0);
    BuchArtikelGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_BuchArtikel = new CCtrlInfo (&m_BuchArtikel, 0, 0, 1, 1);
	BuchArtikelGrid.Add (c_BuchArtikel);
	CtrlGrid.CreateChoiceButton (m_AChoice, IDC_ACHOICE, this);
	CCtrlInfo *c_AChoice = new CCtrlInfo (&m_AChoice, 1, 0, 1, 1);
	BuchArtikelGrid.Add (c_AChoice);

//Grid Lager
	LgrGrid.Create (this, 2, 2);
    LgrGrid.SetBorder (0, 0);
    LgrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Lgr = new CCtrlInfo (&m_Lgr, 0, 0, 1, 1);
	LgrGrid.Add (c_Lgr);
	CtrlGrid.CreateChoiceButton (m_LgrChoice, IDC_LGRCHOICE, this);
	CCtrlInfo *c_LgrChoice = new CCtrlInfo (&m_LgrChoice, 1, 0, 1, 1);
	LgrGrid.Add (c_LgrChoice);

//Grid Toolbar
	ToolGrid.Create (this, 2, 2);
    ToolGrid.SetBorder (0, 0);
    ToolGrid.SetGridSpace (0, 0);

	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 0, 1, 1);
	ToolGrid.Add (c_Save);

	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 1, 0, 1, 1);
	ToolGrid.Add (c_Delete);

	CCtrlInfo *c_ToolGrid = new CCtrlInfo (&ToolGrid, 1, 0, 1, 1);
	c_ToolGrid->SetCellPos (0, 0, 0, 0);
	CtrlGrid.Add (c_ToolGrid);

//Gruppe
	CCtrlInfo *c_LgrGroup     = new CCtrlInfo (&m_LgrGroup, 0, 1, DOCKRIGHT, DOCKBOTTOM); 
	c_LgrGroup->rightspace = 5;
	CtrlGrid.Add (c_LgrGroup);

//Mandant
	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 1, 2, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 2, 2, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 3, 2, 1, 1); 
	CtrlGrid.Add (c_MdnName);

//Lager
	CCtrlInfo *c_LLgr     = new CCtrlInfo (&m_LLgr, 1, 3, 1, 1); 
	CtrlGrid.Add (c_LLgr);
	CCtrlInfo *c_LgrGrid   = new CCtrlInfo (&LgrGrid, 2, 3, 1, 1); 
	CtrlGrid.Add (c_LgrGrid);
	CCtrlInfo *c_LgrBz     = new CCtrlInfo (&m_LgrBz, 3, 3, 1, 1); 
	CtrlGrid.Add (c_LgrBz);

//Hauptlager   
	CCtrlInfo *c_Hauptlager     = new CCtrlInfo (&m_hauptlager, 1, 4, 1, 1); 
	CtrlGrid.Add (c_Hauptlager);

//LagerPlatz
	CCtrlInfo *c_LLgrPlatz     = new CCtrlInfo (&m_LLgrPlatz, 1, 5, 1, 1); 
	CtrlGrid.Add (c_LLgrPlatz);
	CCtrlInfo *c_LgrPlatz   = new CCtrlInfo (&m_LgrPlatz, 2, 5, 1, 1); 
	CtrlGrid.Add (c_LgrPlatz);


/*
	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 3, 4, 1, 1);
	c_Save->SetCellPos (0, -5, 0, 0);
	CtrlGrid.Add (c_Save);

	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 4, 4, 1, 1);
	c_Delete->SetCellPos (0, -5, 0, 0);
	CtrlGrid.Add (c_Delete);
*/
//Mindestbestand
	CCtrlInfo *c_LMinBestand     = new CCtrlInfo (&m_LMinBestand, 1, 7, 1, 1); 
	CtrlGrid.Add (c_LMinBestand);
	CCtrlInfo *c_MinBestand   = new CCtrlInfo (&m_MinBestand, 2, 7, 1, 1); 
	CtrlGrid.Add (c_MinBestand);

//Meldebestand
	CCtrlInfo *c_LMeldBestand     = new CCtrlInfo (&m_LMeldBestand, 1, 8, 1, 1); 
	CtrlGrid.Add (c_LMeldBestand);
	CCtrlInfo *c_MeldBestand   = new CCtrlInfo (&m_MeldBestand, 2, 8, 1, 1); 
	CtrlGrid.Add (c_MeldBestand);

//H�chstbestand
	CCtrlInfo *c_LHoechstBestand     = new CCtrlInfo (&m_LHoechstBestand, 1, 9, 1, 1); 
	CtrlGrid.Add (c_LHoechstBestand);
	CCtrlInfo *c_HoechstBestand   = new CCtrlInfo (&m_HoechstBestand, 2, 9, 1, 1); 
	CtrlGrid.Add (c_HoechstBestand);

//Buchartikel
	CCtrlInfo *c_LBuchArtikel     = new CCtrlInfo (&m_LBuchArtikel, 1, 10, 1, 1); 
	CtrlGrid.Add (c_LBuchArtikel);
	CCtrlInfo *c_BuchArtikelGrid   = new CCtrlInfo (&BuchArtikelGrid, 2, 10, 1, 1); 
	CtrlGrid.Add (c_BuchArtikelGrid);
	CCtrlInfo *c_BuchBz1   = new CCtrlInfo (&m_BuchBz1, 3, 10, 1, 1); 
	CtrlGrid.Add (c_BuchBz1);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Save.Tooltip.SetFont (&Font);
	m_Save.Tooltip.SetSize ();
	m_Delete.Tooltip.SetFont (&Font);
	m_Delete.Tooltip.SetSize ();

	short mdn = Mdn.mdn.mdn;
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	Mdn.mdn.mdn = mdn;
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	memcpy (&A_lgr.a_lgr, &a_lgr_null, sizeof (A_LGR));
	memcpy (&Lgr.lgr, &lgr_null, sizeof (LGR));

	CtrlGrid.Display ();
	Form.Show ();

	ReadMdn ();
	if (Mdn.mdn.mdn != 0)
	{
		m_Mdn.EnableWindow (FALSE);
		m_MdnChoice.EnableWindow (FALSE);
	}
	FirstLager ();

	return TRUE;
}


void CLagerDialog::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CLagerDialog::PreTranslateMessage(MSG* pMsg)
{
//	CWnd *cWnd = NULL;
	CWnd *Control;

	Control = GetFocus ();
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
                if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				Control = GetNextDlgTabItem (Control, TRUE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				OnSave ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_Lgr)
				{
					OnLgrchoice ();
					return TRUE;
				}
				return TRUE;
			}
			break;
		case WM_MDN_FOCUS:
			m_Mdn.SetFocus ();
			return TRUE;
	}
	return FALSE;
}

BOOL CLagerDialog::OnReturn ()
{
	CWnd *Control = GetFocus ();
	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}
	if (Control == &m_Lgr)
	{
		if (!Read ())
		{
			m_Lgr.SetFocus ();
			return FALSE;
		}
	}
	if (Control == &m_HoechstBestand)
	{
		OnSave ();
		return TRUE;
	}
	Control = GetNextDlgTabItem (Control, FALSE);
	if (Control != NULL)
	{
			Control->SetFocus ();
			return FALSE;
	}
	return TRUE;
}

BOOL CLagerDialog::ReadMdn ()
{
	int dsqlstatus = 0;
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();

	if (Mdn.mdn.mdn == 0)
	{
		_tcscpy (MdnAdr.adr.adr_krz, _T("Zentrale"));
		Form.Show ();
		return TRUE;
	}
	
	dsqlstatus = Mdn.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
//		m_Mdn.SetFocus ();
//		m_Mdn.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden (sqlstatus %d) "),Mdn.mdn.mdn, dsqlstatus);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

void CLagerDialog::FirstLager ()
{
	int cursor;
	if (FirstLgrCursor == -1)
	{
		A_lgr.sqlin ((double *) &A_bas->a_bas.a, SQLDOUBLE, 0);
		A_lgr.sqlout ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);
		A_lgr.sqlout ((long *) &A_lgr.a_lgr.lgr, SQLLONG, 0);
		A_lgr.sqlout ((short *) &A_lgr.a_lgr.lgr_pla_kz, SQLSHORT, 0);
		FirstLgrCursor = A_lgr.sqlcursor (_T("select mdn, lgr, lgr_pla_kz from a_lgr ")
			                              _T("where a = ? ")
										  _T("order by mdn, lgr, lgr_pla_kz"));
		A_lgr.sqlin ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);
		A_lgr.sqlin ((double *) &A_bas->a_bas.a, SQLDOUBLE, 0);
		A_lgr.sqlout ((long *) &A_lgr.a_lgr.lgr, SQLLONG, 0);
		A_lgr.sqlout ((short *) &A_lgr.a_lgr.lgr_pla_kz, SQLSHORT, 0);
		FirstLgrCursorMdn = A_lgr.sqlcursor (_T("select lgr, lgr_pla_kz from a_lgr ")
			                              _T("where mdn = ? ")
			                              _T("and fil = 0 ")
			                              _T("and a = ? ")
										  _T("order by lgr, lgr_pla_kz"));
	}
	cursor = FirstLgrCursor;
	if (Mdn.mdn.mdn != 0)
	{
		cursor = FirstLgrCursorMdn;
	}
	A_lgr.sqlopen (cursor);
	if (A_lgr.sqlfetch (cursor) == 0)
	{
		Lgr.lgr.lgr = (short) A_lgr.a_lgr.lgr;
		Keys.Show ();
		Read ();
	}
}

BOOL CLagerDialog::Read ()
{
	memcpy (&Lgr.lgr, &lgr_null, sizeof (LGR));
	memcpy (&A_lgr.a_lgr, &a_lgr_null, sizeof (A_LGR));
	memcpy (&BuchA_bas.a_bas, &a_bas_null, sizeof (A_BAS));
    Lgr.lgr.mdn = Mdn.mdn.mdn;
	Keys.Get ();
	Form.Show ();

	int dsqlstatus = Lgr.dbreadfirst ();
	if (dsqlstatus == 100)
	{
		MessageBox (_T("Lager nicht angelegt"), _T(""), MB_OK | MB_ICONERROR);
	    m_Lgr.SetSel (0, -1, TRUE);
	    m_Lgr.SetFocus ();
		return FALSE;
	}

    A_lgr.a_lgr.mdn  = Mdn.mdn.mdn;
	A_lgr.a_lgr.fil  = 0;
	A_lgr.a_lgr.lgr_pla_kz = 0;
    A_lgr.a_lgr.a   = A_bas->a_bas.a;
    A_lgr.a_lgr.lgr = Lgr.lgr.lgr;
	dsqlstatus = A_lgr.dbreadfirst ();
	if (A_lgr.a_lgr.buch_artikel != 0.0)
	{
		BuchA_bas.a_bas.a = A_lgr.a_lgr.buch_artikel;
		BuchA_bas.dbreadfirst ();
	}
	Form.Show ();
	return TRUE;
}

void CLagerDialog::OnSave ()
{
	Form.Get ();
	if (Lgr.lgr.lgr == 0) return;
    A_lgr.a_lgr.mdn  = Mdn.mdn.mdn;
	A_lgr.a_lgr.fil  = 0;
	A_lgr.a_lgr.lgr_pla_kz = 0;
    A_lgr.a_lgr.a   = A_bas->a_bas.a;
    A_lgr.a_lgr.lgr = Lgr.lgr.lgr;
	A_lgr.dbupdate ();
	Form.Show ();
	PostMessage (WM_MDN_FOCUS, 0, 0l);
}

void CLagerDialog::Delete ()
{
	Form.Get ();
	if (Lgr.lgr.lgr == 0) return;
	if (MessageBox (_T("Lagerzuornung l�schen ?"), "", MB_YESNO | MB_ICONQUESTION) != IDYES)
	{
		return;
	}
    A_lgr.a_lgr.mdn  = Mdn.mdn.mdn;
	A_lgr.a_lgr.fil  = 0;
	A_lgr.a_lgr.lgr_pla_kz = 0;
    A_lgr.a_lgr.a   = A_bas->a_bas.a;
    A_lgr.a_lgr.lgr = Lgr.lgr.lgr;
	A_lgr.dbdelete ();
	memcpy (&Lgr.lgr, &lgr_null, sizeof (LGR));
	memcpy (&A_lgr.a_lgr, &a_lgr_null, sizeof (A_LGR));
	Form.Show ();
	PostMessage (WM_MDN_FOCUS, 0, 0l);
}

void CLagerDialog::OnMdnchoice ()
{

	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = TRUE;
		ChoiceMdn->CreateDlg ();
	}

    ChoiceMdn->SetDbClass (&Mdn);
	ChoiceMdn->DoModal();
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CLagerDialog::OnLgrchoice ()
{

	if (ChoiceLgr == NULL)
	{
		ChoiceLgr = new CChoiceLgr (this);
	    ChoiceLgr->IsModal = TRUE;
		ChoiceLgr->IdArrDown = IDI_HARROWDOWN;
		ChoiceLgr->IdArrUp   = IDI_HARROWUP;
		ChoiceLgr->IdArrNo   = IDI_HARROWNO;
		ChoiceLgr->CreateDlg ();
	}

	ChoiceLgr->Where.Format (_T("and mdn = %hd"), Mdn.mdn.mdn);
	ChoiceLgr->m_Artikel = A_bas->a_bas.a;
    ChoiceLgr->SetDbClass (&Mdn);
	ChoiceLgr->DoModal();
    if (ChoiceLgr->GetState ())
    {
		  CLgrList *abl = ChoiceLgr->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Lgr.lgr, &lgr_null, sizeof (LGR));
		  Lgr.lgr.mdn = Mdn.mdn.mdn;
		  Lgr.lgr.lgr = abl->lgr;
		  Form.Show ();
		  Read ();
		  m_Lgr.SetSel (0, -1, TRUE);
		  m_Lgr.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CLagerDialog::OnAchoice ()
{

	if (ChoiceA == NULL)
	{
		ChoiceA = new CChoiceA (this);
	    ChoiceA->IsModal = TRUE;
	    ChoiceA->HideFilter = FALSE;
		ChoiceA->IdArrDown = IDI_HARROWDOWN;
		ChoiceA->IdArrUp   = IDI_HARROWUP;
		ChoiceA->IdArrNo   = IDI_HARROWNO;
		ChoiceA->CreateDlg ();
	}

    ChoiceA->SetDbClass (&Mdn);
	ChoiceA->DoModal();
    if (ChoiceA->GetState ())
    {
   		  CABasList *abl = ChoiceA->GetSelectedText (); 
		  if (abl == NULL) return;
		  A_lgr.a_lgr.buch_artikel = abl->a;
		  Form.Show ();
		  m_BuchArtikel.SetSel (0, -1, TRUE);
		  m_BuchArtikel.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CLagerDialog::OnEnKillfocusBuchArtikel()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	BuchA_bas.a_bas.a = A_lgr.a_lgr.buch_artikel;
	if (BuchA_bas.dbreadfirst () != 0)
	{
		A_lgr.a_lgr.buch_artikel = 0.0;
		_tcscpy (BuchA_bas.a_bas.a_bz1, _T(""));
	}
	Form.Show ();
}

void CLagerDialog::OnBnClickedHauptlager() //WAL-159
{
	Form.Get ();
	if (strcmp(A_lgr.a_lgr.haupt_lgr,"J") == 0) 
	{
		if (DelFlgHauptLgr == -1)
		{
			A_lgr.sqlin ((short *) &Mdn.mdn.mdn, SQLSHORT, 0);
			A_lgr.sqlin ((double *) &A_bas->a_bas.a, SQLDOUBLE, 0);
			DelFlgHauptLgr = A_lgr.sqlcursor (_T("update a_lgr set haupt_lgr = \"N\" where mdn = ? and a = ? "));
		}
		A_lgr.sqlexecute (DelFlgHauptLgr);
	}

}
