#include "stdafx.h"
#include "ChoicePtab.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoicePtab::Sort1 = -1;
int CChoicePtab::Sort2 = -1;
int CChoicePtab::Sort3 = -1;
int CChoicePtab::Sort4 = -1;
int CChoicePtab::Sort5 = -1;

CChoicePtab::CChoicePtab(CWnd* pParent)
        : CChoiceX(pParent)
{
	Ptitem = _T("");
	Caption = _T("");
}

CChoicePtab::~CChoicePtab()
{
	DestroyList ();
}

void CChoicePtab::DestroyList()
{
	for (std::vector<CPtabList *>::iterator pabl = PtabList.begin (); pabl != PtabList.end (); ++pabl)
	{
		CPtabList *abl = *pabl;
		delete abl;
	}
    PtabList.clear ();
	SelectList.clear ();
}

void CChoicePtab::FillList ()
{
    TCHAR ptitem [19];
    TCHAR ptwert [50];
    TCHAR ptbez [50];
    TCHAR ptbezk [50];
    TCHAR ptwer1 [50];
    TCHAR ptwer2 [50];

    if (Ptitem == _T("")) return;
	_tcscpy (ptitem, Ptitem.GetBuffer ());
	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

	if (Caption == _T(""))
	{
		Caption = _T("Auswahl Prüftabelle");
	}
    SetWindowText (Caption.GetBuffer ());
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Nr."),        1, 50, LVCFMT_LEFT);
    SetCol (_T("Wert"),        2, 150, LVCFMT_LEFT);
    SetCol (_T("Bezeichnung 1"),  3, 250);

	if (PtabList.size () == 0)
	{
		DbClass->sqlin ((LPTSTR *) ptitem,    SQLCHAR, sizeof (ptitem));
		DbClass->sqlout ((LPTSTR *) ptwert,    SQLCHAR, sizeof (ptwert));
		DbClass->sqlout ((LPTSTR)   ptbez,     SQLCHAR, sizeof (ptbez));
		DbClass->sqlout ((LPTSTR)   ptbezk,    SQLCHAR, sizeof (ptbezk));
		DbClass->sqlout ((LPTSTR *) ptwer1,    SQLCHAR, sizeof (ptwer1));
		DbClass->sqlout ((LPTSTR *) ptwer2,    SQLCHAR, sizeof (ptwer2));
		int cursor = DbClass->sqlcursor (_T("select ptwert, ptbez, ptbezk, ptwer1, ptwer2 from ptabn ")
										 _T("where ptitem = ? order by ptwert"));
		while (DbClass->sqlfetch (cursor) == 0)
		{
			CDbUniCode::DbToUniCode (ptwert, (LPSTR) ptwert);
			CDbUniCode::DbToUniCode (ptbez, (LPSTR) ptbez);
			CDbUniCode::DbToUniCode (ptbezk, (LPSTR) ptbezk);
			CDbUniCode::DbToUniCode (ptwer1, (LPSTR) ptwer1);
			CDbUniCode::DbToUniCode (ptwer2, (LPSTR) ptwer2);
			CPtabList *abl = new CPtabList (ptwert, ptbez, ptbezk, ptwer1, ptwer2);
			PtabList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CPtabList *>::iterator pabl = PtabList.begin (); pabl != PtabList.end (); ++pabl)
	{
		CPtabList *abl = *pabl;
		_tcscpy (ptwert, abl->ptwert.GetBuffer ());
		_tcscpy (ptwer1, abl->ptwer1.GetBuffer ());
		_tcscpy (ptbez, abl->ptbez.GetBuffer ());
        int ret = InsertItem (i, -1);
       	ret = SetItemText (ptwert, i, 1);
       	ret = SetItemText (ptwer1, i, 2);     //LAC-115  in ptwer1 steht der Inhalt
        ret = SetItemText (ptbez, i, 3);
        i ++;
   	}

	SortRow = 1;
	Sort1 = -1;
   	Sort2 = -1;
   	Sort3 = -1;
   	Sort4 = -1;
   	Sort5 = -1;
   	Sort (listView);
}


void CChoicePtab::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoicePtab::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoicePtab::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoicePtab::SearchABz1 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoicePtab::SearchABz2 (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoicePtab::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchABz1 (ListBox, EditText.GetBuffer ());
             break;
        case 3 :
             EditText.MakeUpper ();
             SearchABz2 (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoicePtab::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoicePtab::CompareProc(LPARAM lParam1,
						 		     LPARAM lParam2,
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   return 0;
}


void CChoicePtab::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CPtabList *abl = PtabList [i];

		   abl->ptwert = ListBox->GetItemText (i, 1);
		   abl->ptbez = ListBox->GetItemText (i, 2);
	}
	for (int i = 1; i <= 9; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);
}

void CChoicePtab::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = PtabList [idx];
}

CPtabList *CChoicePtab::GetSelectedText ()
{
	CPtabList *abl = (CPtabList *) SelectedRow;
	return abl;
}

void CChoicePtab::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (PtabList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}
