#ifndef _ABAS_LIST_DEF
#define _ABAS_LIST_DEF
#pragma once

class CABasList
{
public:
	double a;
	CString a_bz1;
	CString a_bz2;
	CString a_typ;
	CString ag;
	CString wg;
	CString hwg;
	CString teil_smt;
	CString me_einh;
	CString a_bz3;

	CABasList(void);
	CABasList(double, LPTSTR, LPTSTR, LPTSTR, LPTSTR, LPTSTR, LPTSTR, LPTSTR, LPTSTR, LPTSTR);
	~CABasList(void);
};

class CTeilSmtList
{
public:
	CString ptwert;
	CString ptbez;
	CString ptbezk;

	CTeilSmtList(void);
	CTeilSmtList(LPTSTR, LPTSTR, LPTSTR);
	~CTeilSmtList(void);
};
class CATypList
{
public:
	CString ptwert;
	CString ptbez;
	CString ptbezk;

	CATypList(void);
	CATypList(LPTSTR, LPTSTR, LPTSTR);
	~CATypList(void);
};
class CMeEinhList
{
public:
	CString ptwert;
	CString ptbez;
	CString ptbezk;

	CMeEinhList(void);
	CMeEinhList(LPTSTR, LPTSTR, LPTSTR);
	~CMeEinhList(void);
};


class CDynDialogList  //FS-537
{
public:
	CString tab_nam;
	CString feld_nam;
	int range;
	int typ;
	CString item_bez;
	short feld_typ;
	int z;

	CDynDialogList(void);
	CDynDialogList(int,LPTSTR, LPTSTR, int, int, LPTSTR, short);
	~CDynDialogList(void);
};



#endif
