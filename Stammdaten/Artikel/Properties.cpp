#include "StdAfx.h"
#include ".\properties.h"

CProperties::CProperties(void)
{
}

CProperties::~CProperties(void)
{
}

CString CProperties::GetValue (CString Name)
{
	FirstPosition ();
	CPropertyItem *p;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Name.CompareNoCase (Name) == 0)
		{
			return p->Value;
		}
	}
	return Name;
}

CString CProperties::GetName (CString Value)
{
	FirstPosition ();
	CPropertyItem *p;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Value.CompareNoCase (Value) == 0)
		{
			return p->Name;
		}
	}
	return Value;
}

int CProperties::Find (CString Name)
{
	FirstPosition ();
	CPropertyItem *p;
	int i = 0;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Name.CompareNoCase (Name) == 0)
		{
			return i;
		}
		i ++;
	}
	return -1;
}

int CProperties::FindValue (CString Value)
{
	FirstPosition ();
	CPropertyItem *p;
	int i = 0;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Value.CompareNoCase (Value) == 0)
		{
			return i;
		}
		i ++;
	}
	return -1;
}

void CProperties::Set (CString Name, CString Value, int idx)
{
	CPropertyItem *p;
	p = (CPropertyItem *) Get (idx);
	if (p != NULL)
	{
		p->Name = Name;
		p->Value = Value;
	}
}

