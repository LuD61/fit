#pragma once
#include "NumEdit.h"
#include "TextEdit.h"
#include "a_bas.h"
#include "a_pr.h"
#include "a_kalkhndw.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "Ptabn.h"

// CBearbDialogHndw-Dialogfeld

class CBearbDialogHndw : public CDialog
{
	DECLARE_DYNAMIC(CBearbDialogHndw)

public:
	CBearbDialogHndw(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CBearbDialogHndw();

// Dialogfelddaten
	enum { IDD = IDD_BEARBWEG_HNDW };

protected:
	BOOL visible;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	CFont Font;
	CFormTab Form;
	CCtrlGrid CtrlGrid;

public:
	enum
	{
		Aufschlag = 1,
		AufschlagPlus = 2,
		Abschlag = 3,
		AbschlagPlus = 4,
	};

	int MarktSpPar;
	double mwst;
	double SpEkIst;
	double SpVkIst;
	double EkDurchschnitt;

	int StdCellHeight;
	A_BAS_CLASS *A_bas;
	A_KALKHNDW_CLASS *A_kalkhndw;
	APR_CLASS *A_pr;
	PTABN_CLASS Ptabn;

	CStatic m_LPrEk1;
	CStatic m_LEkDurchschnitt;
	CStatic m_LBearbWeg;
	CStatic m_LSk;
	CStatic m_LFilEk;
	CStatic m_LVk;
	CStatic m_LVollkosten;
	CStatic m_LIstPr;
	CStatic m_LIstSp;

	CNumEdit m_PrEk1;
	CNumEdit m_EkDurchschnitt;

	CNumEdit m_BearbSk;
	CNumEdit m_BearbFil;
	CNumEdit m_BearbLad;

	CNumEdit m_Sk;
	CNumEdit m_FilEk;
	CNumEdit m_Vk;

	CNumEdit m_PrEk;
	CNumEdit m_APrVk;

	CNumEdit m_SpEkIst;
	CNumEdit m_SpVkIst;

	void Calculate ();
	void Show ();
	void Get ();
	void SetVisible (BOOL visible);
};
