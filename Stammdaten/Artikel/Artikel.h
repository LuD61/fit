// Artikel.h : Hauptheaderdatei f�r die Artikel-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error 'stdafx.h' muss vor dieser Datei in PCH eingeschlossen werden.
#endif

#include "resource.h"       // Hauptsymbole
#include "Sys_ben.h" //LAC-133
#include "a_bas.h" //LAC-133
#include "Basisdatenpage.h" //WAL-130




// CArtikelApp:
// Siehe Artikel.cpp f�r die Implementierung dieser Klasse
//

class CArtikelApp : public CWinApp
{
public:
	CArtikelApp();


// �berschreibungen
public:
	enum
	{
		None = 0,
		Artikel = 1,
		Material = 2,
	};

	virtual BOOL InitInstance();
    virtual int ExitInstance(); 
	int StartPage;
	static CString EnterEk;
	static CString EnterSprache;
	static BOOL WithEK;
	static BOOL WithSpracheneditor;
	double a;
	SYS_BEN_CLASS Sys_ben; //LAC-133
	CString PersName; //LAC-133
	A_BAS_CLASS *A_bas;
	void SetABas (A_BAS_CLASS *A_bas)
	{
		this->A_bas = A_bas;
		a = A_bas->a_bas.a;
	}

	//WAL-130
	CBasisdatenPage *BasisdatenPage ;
	void SetBasisdatenPage (CBasisdatenPage *BasisdatenPage)
	{
		this->BasisdatenPage = BasisdatenPage;
	}



// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnAPr();
	afx_msg void OnStdPr();
	afx_msg void OnEnChangeA();
public:
	afx_msg void OnArtikelstamm();
	afx_msg void OnMaterialstamm();
	afx_msg void OnEkpr();
	afx_msg void OnSprache();
};

extern CArtikelApp theApp;
