#include "StdAfx.h"
#include "eanlistctrl.h"
#include "DbUniCode.h"

CEanListCtrl::CEanListCtrl(void)
{
	A_bas = NULL;
	A_ean = NULL;
	A_krz = NULL;
	cursor = -1;
}

CEanListCtrl::~CEanListCtrl(void)
{
}

void CEanListCtrl::Init ()
{

	CBitmap bmp;
	bmp.CreateBitmap (1,20, 1, 0, NULL);
	BITMAP bm;
	int ret = bmp.GetBitmap (&bm);
	if (ret != 0)
	{
		image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
		image.Add (&bmp, RGB (0,0,0));
	}
	SetImageList (&image, LVSIL_SMALL);   

	FillList = this;

	FillList.SetStyle (LVS_REPORT);
	FillList.SetExtendedStyle (LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
//	m_PosList.VLines = 1;

	FillList.SetCol ("", 0, 0);
	FillList.SetCol ("Ean-Nr", 1, 100, LVCFMT_RIGHT);
	FillList.SetCol ("Bezeichnung",  2, 150);
	FillList.SetCol ("Haupt EAN  ",  3, 80, LVCFMT_CENTER);
	FillList.SetCol ("Kurznummer  ", 4, 80, LVCFMT_RIGHT);
	ColType.Add (new CColType (3, CheckBox));
	EnableEdit = FALSE;
	Read ();
}

BOOL CEanListCtrl::Read ()
{
	if (cursor == -1)
	{
		A_ean->sqlin ((double *) &A_bas->a_bas.a, SQLDOUBLE, 0);
		A_ean->sqlout ((double *) &A_ean->a_ean.a, SQLDOUBLE, 0); //OKLE-9
		A_ean->sqlout ((double *) &A_ean->a_ean.ean, SQLDOUBLE, 0);
		cursor = A_ean->sqlcursor (_T("select a, ean from a_ean where a = ?"));
	}

	if (cursor == -1) return FALSE;

	DeleteAllItems ();
	memcpy (&A_ean->a_ean, &a_ean_null, sizeof (A_EAN));

	A_ean->sqlopen (cursor);
	int idx = 0;
	while (A_ean->sqlfetch (cursor) == 0)
	{
		FillList.InsertItem (idx, 0);
		A_ean->dbreadfirst ();
		CString Ean;
		Ean.Format (_T("%.0lf"), A_ean->a_ean.ean);
		FillList.SetItemText (Ean.GetBuffer (), idx, 1);

		CString EanBz = A_ean->a_ean.ean_bz;
		LPTSTR text = new TCHAR [(EanBz.GetLength () + 1) * 2];
		_tcscpy (text, EanBz.GetBuffer ());
		LPSTR pos = (LPSTR) text;
	    CDbUniCode::DbToUniCode (text, pos);
		EanBz = text;
		delete text;

		FillList.SetItemText (EanBz.GetBuffer (), idx, 2);
		if ((char) A_ean->a_ean.h_ean_kz[0] == '1')
		{
			FillList.SetItemText (_T("X"), idx, 3);
		}
		else
		{
			FillList.SetItemText (_T(" "), idx, 3);
		}

		A_krz->a_krz.a_herk = A_ean->a_ean.ean;
	    A_krz->a_krz.a_krz = 0l;
		A_krz->dbreada_herkfirst ();
		CString Akrz;
		Akrz.Format (_T("%ld"), A_krz->a_krz.a_krz);
		FillList.SetItemText (Akrz.GetBuffer (), idx, 4);
		idx ++; 
	}
	if (idx > 0) return TRUE;
	return FALSE;
}

