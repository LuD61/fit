// DialogTestDlg.h : Headerdatei
//

#pragma once
#include "DbPropertyPage.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "PrGrListCtrl.h"
#include "PrArtPageConst.h"
#include "PrGrPageConst.h"
#include "FillList.h"
#include "a_bas.h"
#include "mdn.h"
#include "adr.h"
#include "Ipr.h"
#include "PGrProt.h"
#include "Iprgrstufk.h"
#include "I_kun_prk.h"
#include "Kun.h"
#include "Sys_ben.h"
#include "ChoicePrGrStuf.h"
#include "ChoiceMdn.h"
#include "ChoiceKun.h"
#include "FormTab.h"
#include "mo_progcfg.h"
#include "Controls.h"
#include "Preise.h"
#include "GrundPreis.h"
#include "ListChangeHandler.h"

#define IDC_PRGRCHOICE 4000
#define IDC_MDNCHOICE 3001

// CPrArtPreise Dialogfeld
class CPrGrPage : public CDbPropertyPage, CListChangeHandler
{
// Konstruktion
public:
	CPrGrPage(CWnd* pParent = NULL);	// Standardkonstruktor
	CPrGrPage(UINT nIDTemplate);	// Standardkonstruktor

	~CPrGrPage ();

// Dialogfelddaten
//	enum { IDD = IDD_DIALOGTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung
    virtual BOOL PreTranslateMessage(MSG* pMsg);


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public :
    enum
	{
		START_NORMAL = 0,
		START_MAXIMIZED = 1,
		START_FITSIZE = 2,
	};

	PROG_CFG Cfg;
	BOOL RemoveKun;
	int StartSize;

	CControls HeadControls;
	CControls PosControls;
	CControls ButtonControls;
	BOOL HideButtons;
	int RightListSpace;
	CVector DbRows;
	CVector ListRows;
	int IprCursor;
	int IprGrStufkCursor;
	int IKunPrkCursor;
	int IprDelCursor;
	CWnd *Frame;
	CCtrlGrid CtrlGrid;
	CCtrlGrid PrGrGrid;
	CCtrlGrid MdnGrid;
	CButton m_MdnChoice;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CNumEdit m_MdnName;
	CButton m_PrGrChoice;
	CStatic m_LPrGrStuf;
	CNumEdit m_PrGrStuf;
	CEdit m_LZusBz;
	CTextEdit m_ZusBz;
	CButton m_AddEk;
	CFillList FillList;
	CPrGrListCtrl m_PrGrList;
	CButton m_Cancel;
	CButton m_Save;
	CButton m_Delete;
	CButton m_Insert;
	int RighListSpace;
	CCtrlGrid ButtonGrid;
	MDN_CLASS Mdn;
	ADR_CLASS MdnAdr;
	A_BAS_CLASS A_bas;
	IPR_CLASS Ipr;
	CPGrProt PgrProt;
	PRG_PROT_CLASS Prg_prot;
	IPRGRSTUFK_CLASS Iprgrstufk;
	I_KUN_PRK_CLASS I_kun_prk;
	KUN_CLASS Kun;
	ADR_CLASS KunAdr;
	SYS_BEN_CLASS Sys_ben;
	CGrundPreis GrundPreis;
	CFormTab Form;
	CChoicePrGrStuf *Choice;
	BOOL ModalChoice;
	BOOL CloseChoice;
	BOOL ChoiceStat;
	CString Search;
	CChoiceMdn *ChoiceMdn;
	BOOL ModalChoiceMdn;
	CChoiceKun *ChoiceKun;
	BOOL ModalChoiceKun;
	CFont Font;
	CFont lFont;
	CString PersName;
	CString Separator;
    CImageList image; 
	int CellHeight;

	virtual void OnSize (UINT, int, int);
	void OnPrGrchoice ();
    void OnPrGrSelected ();
    void OnPrGrCanceled ();
    void OnMdnchoice(); 
	virtual BOOL Read ();
	virtual BOOL read ();
	virtual BOOL Write ();
    virtual BOOL OnReturn ();
    virtual BOOL OnKeyup ();
	virtual BOOL StepBack ();
	virtual void OnCancel ();
	virtual void OnSave ();
	virtual void OnAddEk ();
	BOOL ReadMdn ();
	BOOL ReadList ();
    void FillPrGrStufCombo ();
    void FillKunPrCombo ();
	virtual BOOL DeleteAll ();
	virtual BOOL Print ();
	virtual BOOL PrintAll ();
	virtual void OnDelete ();
	virtual void OnInsert ();
    virtual void OnCopy ();
    virtual void OnPaste ();
    virtual void OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult);
    void EnableHeadControls (BOOL enable);
    void WriteRow (int row);
    void DestroyRows(CVector &Rows);
    void DeleteDbRows ();
    BOOL IsChanged (CPreise* Ipr);
    BOOL InList (IPR_CLASS& Ipr);
	void ReadCfg ();
    void ListCopy ();
    void ListPaste ();
	BOOL TestDecValues (IPR *ipr);
	void ShowGrundPreis ();
	void SetGrundPreis ();

//	ListChangeHandler

	virtual void RowChanged (int NewRow);
	virtual void ColChanged (int Row, int Col) {}
};
