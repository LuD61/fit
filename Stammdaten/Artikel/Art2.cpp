// Art2.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "Art2.h"
#include "art1.h"
#include "resource.h"
#include ".\art2.h"
#include "ChildFrm.h"

#define CXPLUS 38
#define CYPLUS 50

// CArt2

IMPLEMENT_DYNCREATE(CArt2, DbFormView)

CArt2::CArt2()
	: DbFormView(CArt2::IDD)
{
	tabx = 0;
	taby = 0;
	StartSize = START_NORMAL;
	Cfg.SetProgName( _T("Artikelstamm"));
	ReadCfg ();
	DlgBkColor = NULL;
	DlgBrush = NULL;
	ArchiveName = _T("Form.prp");
	Load ();
	dlg.DlgBkColor = DlgBkColor;
}

CArt2::~CArt2()
{
	Save ();
}

void CArt2::DoDataExchange(CDataExchange* pDX)
{
	DbFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CArt2, DbFormView)
//	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_BACK, OnBack)
//	ON_WM_DRAWITEM()
ON_COMMAND(ID_DELETE, OnDelete)
ON_COMMAND(ID_INSERT, OnInsert)
ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
ON_COMMAND(ID_DELETEALL, OnDeleteall)
ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
ON_COMMAND(ID_PRINT_ALL, OnPrintAll)
ON_COMMAND(ID_SHOW_IMAGE, OnShowImage)
ON_UPDATE_COMMAND_UI(ID_SHOW_IMAGE, OnUpdateShowImage)
ON_COMMAND(ID_LINK_IMAGE, OnLinkImage)
ON_COMMAND(ID_UNLINK_IMAGE, OnUnlinkImage)
ON_UPDATE_COMMAND_UI(ID_LINK_IMAGE, OnUpdateLinkImage)
ON_UPDATE_COMMAND_UI(ID_UNLINK_IMAGE, OnUpdateUnlinkImage)
ON_COMMAND(ID_OPEN_IMAGE, OnOpenImage)
ON_UPDATE_COMMAND_UI(ID_OPEN_IMAGE, OnUpdateOpenImage)
ON_COMMAND(ID_OPE_OMAGE_WITH_BILD, OnOpeOmageWithBild)
ON_UPDATE_COMMAND_UI(ID_OPE_OMAGE_WITH_BILD, OnUpdateOpeOmageWithBild)
ON_COMMAND(ID_FIRST, OnFirst)
ON_UPDATE_COMMAND_UI(ID_FIRST, OnUpdateFirst)
ON_COMMAND(ID_PRIOR, OnPrior)
ON_UPDATE_COMMAND_UI(ID_PRIOR, OnUpdatePrior)
ON_COMMAND(ID_NEXT, OnNext)
ON_UPDATE_COMMAND_UI(ID_NEXT, OnUpdateNext)
ON_COMMAND(ID_LAST, OnLast)
ON_UPDATE_COMMAND_UI(ID_LAST, OnUpdateLast)
ON_COMMAND(ID_EAN_CHOICE, OnEanChoice)
ON_UPDATE_COMMAND_UI(ID_EAN_CHOICE, OnUpdateEanChoice)
//ON_COMMAND(ID_PLU, OnPlu)
//ON_UPDATE_COMMAND_UI(ID_PLU, OnUpdatePlu)
ON_COMMAND(ID_DLG_BACKGROUND, OnDlgBackground)
ON_COMMAND(ID_CHOICEBACKGROUND, OnChoicebackground)
ON_UPDATE_COMMAND_UI(ID_CHOICEBACKGROUND, OnUpdateChoicebackground)
ON_COMMAND(ID_CHOICEA_DEFAULT, OnChoiceaDefault)
ON_UPDATE_COMMAND_UI(ID_CHOICEA_DEFAULT, OnUpdateChoiceaDefault)
ON_COMMAND(ID_CHOICEEAN_DEFAULT, OnChoiceeanDefault)
ON_UPDATE_COMMAND_UI(ID_CHOICEEAN_DEFAULT, OnUpdateChoiceeanDefault)
ON_COMMAND(ID_CHOICEAKRZ_DEFAULT, OnChoiceakrzDefault)
ON_UPDATE_COMMAND_UI(ID_CHOICEAKRZ_DEFAULT, OnUpdateChoiceakrzDefault)
ON_COMMAND(ID_FLAT_LAYOUT, OnFlatLayout)
ON_UPDATE_COMMAND_UI(ID_FLAT_LAYOUT, OnUpdateFlatLayout)
ON_COMMAND(ID_LIEFBEST, OnLiefbest)
ON_UPDATE_COMMAND_UI(ID_LIEFBEST, OnUpdateLiefbest)
ON_COMMAND(ID_CHOICELIEFBEST_DEFAULT, OnChoiceliefbestDefault)
ON_UPDATE_COMMAND_UI(ID_CHOICELIEFBEST_DEFAULT, OnUpdateChoiceliefbestDefault)
ON_COMMAND(ID_DLGFRAME_BACKGROUND, OnDlgframeBackground)
ON_COMMAND(ID_CHOICE_OK, OnChoiceOk)
ON_UPDATE_COMMAND_UI(ID_CHOICE_OK, OnUpdateChoiceOk)
ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
ON_UPDATE_COMMAND_UI(ID_DELETE, OnUpdateDateiL)
ON_COMMAND(ID_MAT, OnMat)
ON_COMMAND(ID_INFO, OnInfo)
ON_UPDATE_COMMAND_UI(ID_INFO, OnUpdateInfo)
END_MESSAGE_MAP()


// CArt2-Diagnose

#ifdef _DEBUG
void CArt2::AssertValid() const
{
	DbFormView::AssertValid();
}

void CArt2::Dump(CDumpContext& dc) const
{
	DbFormView::Dump(dc);
}
#endif //_DEBUG


// CArt2-Meldungshandler

void CArt2::OnInitialUpdate()
{
	DbFormView::OnInitialUpdate();


	dlg.Construct (_T(""), this);
	Page1.Construct (IDD_BASISDATEN_PAGE);
	Page1.Frame = this;
	Page1.Update = this;
	Page1.PageType = Page1.Material;
	Page1.HideButtons = TRUE;
	dlg.AddPage (&Page1);
    dlg.m_psh.pszCaption = _T("Simple");
    dlg.m_psh.nStartPage = 0;


	Page2.Construct (IDD_MATPAGE);
	Page2.Frame = this;
	Page2.Basis = &Page1;
	Page2.Update = this;
	Page2.HideButtons = TRUE;
	dlg.AddPage (&Page2);


	Page3.Construct (IDD_EAN_EMB);
	Page3.Frame = this;
	Page3.SetBasis (&Page1);
	Page3.Update = this;
	Page3.HideButtons = TRUE;
	dlg.AddPage (&Page3);

//    dlg.m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_PROPTITLE;
    dlg.m_psh.dwSize = sizeof (dlg.m_psh);
	dlg.Create (this, WS_CHILD | WS_VISIBLE);
    Page1.SetFocus (); 

	CRect cRect;
    dlg.GetClientRect (&cRect);
    dlg.MoveWindow (0, 0, cRect.right, cRect.bottom);
	CSize Size = GetTotalSize ();
	if (Size.cy < (cRect.bottom) + 10)
	{
		Size.cy = cRect.bottom + 10;
	}
	if (Size.cx < (cRect.right) + 10)
	{
		Size.cx = cRect.right + 10;
	}
	SetScrollSizes (MM_TEXT, Size);
	CRect pRect;
	CRect pcRect;
	CWnd *ChildFrame = GetParent ();
	CWnd *MainFrame = ChildFrame->GetParent ();
	ChildFrame->GetWindowRect (&pRect);
	MainFrame->ScreenToClient (&pRect);
	MainFrame->GetClientRect (&pcRect);
	BOOL Resize = FALSE;
	if (pcRect.right > pRect.left + cRect.right + CXPLUS)
	{
		pRect.right  = pRect.left + cRect.right + CXPLUS;
		Resize = TRUE;
	}
	if (pcRect.bottom > pRect.top + cRect.bottom + CYPLUS)
	{
		pRect.bottom = pRect.top + cRect.bottom + CYPLUS;
		Resize = TRUE;
	}
	if (Resize)
	{
		GetParent ()->MoveWindow (&pRect, TRUE);
	}
    ((CChildFrame *) GetParent ())->MDIMaximize ();
}


HBRUSH CArt2::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return DbFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CArt2::OnSize(UINT nType, int cx, int cy)
{
	if (m_hWnd == NULL || !IsWindow (m_hWnd))
	{
		return;
	}
	if (!IsWindow (dlg.m_hWnd))
	{
		return;
	}
	CRect frame;
	CRect pRect;
	dlg.GetWindowRect (&pRect);
	int newcy = pRect.bottom - pRect.top;
	dlg.MoveWindow (tabx, taby, cx, newcy);
	CTabCtrl *tab = dlg.GetTabControl ();
	Page1.Frame = this;
	Page1.GetWindowRect (&pRect);
	dlg.ScreenToClient (&pRect);

	frame = pRect;
	frame.top = 10;
	frame.left = 10;
    frame.right = cx - 10 - tabx;
//	frame.bottom = cy - 10 - taby;
	frame.bottom = newcy;
	tab->MoveWindow (&frame);
	frame.top += 20;
	frame.left += 2;
    frame.right -= 4;
	frame.bottom -= 4;

	int page = dlg.GetActiveIndex ();
	if (IsWindow (Page1.m_hWnd))
	{
		Page1.MoveWindow (&frame);
	}
/*
	if (IsWindow (Page2.m_hWnd))
	{
		Page2.MoveWindow (&frame);
	}
*/

/*
	if (page == 0)
	{
		Page1.MoveWindow (&frame);
	}
	else if (page == 1)
	{
		Page2.MoveWindow (&frame);
	}
	else if (page == 2)
	{
		Page3.MoveWindow (&frame);
	}
*/
}

void CArt2::ReadCfg ()
{
    char cfg_v [256];

    if (Cfg.GetCfgValue ("StartSize", cfg_v) == TRUE)
    {
			StartSize = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CArt2::OnFileSave()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     dlg.Write ();
//	 GetParent()-> SetWindowText (_T("Artikelstamm: gespeichert"));
}

void CArt2::OnBack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
     dlg.StepBack ();
//	 GetParent()-> SetWindowText (_T("Artikelstamm: Aktion r�ckg�ngig"));
}

void CArt2::OnDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.OnDelete ();
}

void CArt2::OnInsert()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.Insert ();
}

void CArt2::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.OnCopy ();
}

void CArt2::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.OnPaste ();
}

void CArt2::OnDeleteall()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.DeleteAll ();
}

void CArt2::OnFilePrint()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.Print ();
}

void CArt2::OnPrintAll()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	dlg.PrintAll ();
}

void CArt2::OnShowImage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

    CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
	if (Page->ShowImage)
	{
		Page->ShowImage = FALSE;
	}
	else
	{
		Page->ShowImage = TRUE;
	}
	Page->DisplayImage ();
}

void CArt2::OnUpdateShowImage(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
    CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
	if (Page->ShowImage)
	{
		pCmdUI->SetCheck (1);
	}
	else
	{
		pCmdUI->SetCheck (0);
	}
}

void CArt2::OnLinkImage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   Page->OnLinkImage ();
}

void CArt2::OnUnlinkImage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   Page->OnUnlinkImage ();
}

void CArt2::OnUpdateLinkImage(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page->m_A.IsWindowEnabled ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnUpdateUnlinkImage(CCmdUI *pCmdUI)
{
	   // TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page->m_A.IsWindowEnabled ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}
  

void CArt2::OnOpenImage()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   Page->OnOpenImage ();
}

void CArt2::OnUpdateOpenImage(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page->m_A.IsWindowEnabled ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnOpeOmageWithBild()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   Page->OnOpenImageWith ();
}

void CArt2::OnUpdateOpeOmageWithBild(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page->m_A.IsWindowEnabled ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnFirst()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   Page->FirstRec ();
}

void CArt2::OnUpdateFirst(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (!Page->ChoiceHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnPrior()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   Page->PriorRec ();
}

void CArt2::OnUpdatePrior(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (!Page->ChoiceHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnNext()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   Page->NextRec ();
}

void CArt2::OnUpdateNext(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (!Page->ChoiceHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnLast()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   Page->LastRec ();
}

void CArt2::OnUpdateLast(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (!Page->ChoiceHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnEanChoice()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
	   Page->OnEanchoice ();
   }
}

void CArt2::OnUpdateEanChoice(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnPlu()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
	   Page->OnAkrzchoice ();
   }
}

void CArt2::OnUpdatePlu(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnDlgBackground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
/*
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
		Page->OnDlgBackground ();
   }
*/

	CColorDialog cdlg;
	cdlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	COLORREF Color = GetSysColor (COLOR_3DFACE);
	cdlg.m_cc.rgbResult = Color;
	if (cdlg.DoModal() == IDOK)
	{
		COLORREF DlgBkColor = cdlg.GetColor();

	    for (int i = 0; i < 2; i ++)
		{
			CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (i);
			Page->DlgBkColor = DlgBkColor;
	  	    DeleteObject (Page->DlgBrush);
		    Page->DlgBrush = NULL;
			Page->InvalidateRect (NULL);
//			Page->OnDlgBackground ();
		}
	}

}

void CArt2::OnChoicebackground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
		Page->OnChoicebackground ();
   }
}

void CArt2::OnUpdateChoicebackground(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
/*
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page->Choice == NULL)
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
*/
}

void CArt2::OnChoiceaDefault()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
		Page->OnChoiceaDefault();
   }
}

void CArt2::OnUpdateChoiceaDefault(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (!Page->ChoiceHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnChoiceeanDefault()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
		Page->OnChoiceeanDefault();
   }
}

void CArt2::OnUpdateChoiceeanDefault(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (!Page->ChoiceEanHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnChoiceakrzDefault()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
		Page->OnChoiceakrzDefault();
   }
}

void CArt2::OnUpdateChoiceakrzDefault(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (!Page->ChoiceAkrzHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnFlatLayout()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.

   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
		Page->OnFlatLayout();
   }
}

void CArt2::OnUpdateFlatLayout(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
    CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
	if (Page->FlatLayout)
	{
		pCmdUI->SetCheck (1);
	}
	else
	{
		pCmdUI->SetCheck (0);
	}
}

void CArt2::OnLiefbest()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
	   Page->OnLiefBestchoice ();
   }
}

void CArt2::OnUpdateLiefbest(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
	   pCmdUI->Enable ();
   }
}


void CArt2::OnChoiceliefbestDefault()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
		Page->OnChoiceLiefBestDefault();
   }
}

void CArt2::OnUpdateChoiceliefbestDefault(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (!Page->ChoiceLiefBestHasElements ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnDlgframeBackground()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CColorDialog dlg;
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
//	dlg.m_cc.rgbResult = KprParams.LeftColor;
	if (dlg.DoModal() == IDOK)
	{
		DlgBkColor = dlg.GetColor();
		DeleteObject (DlgBrush);
		DlgBrush = NULL;
		this->dlg.DlgBkColor = DlgBkColor;
		this->dlg.DlgBrush = NULL;
		InvalidateRect (NULL);
		this->dlg.Invalidate ();
	}
}

void CArt2::OnChoiceOk()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
	    if (Page->HideOK)
		{
			Page->HideOK = FALSE;
		}
		else
		{
			Page->HideOK = TRUE;
		}
   }
}

void CArt2::OnUpdateChoiceOk(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
    CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
	if (Page->HideOK)
	{
		pCmdUI->SetCheck (1);
	}
	else
	{
		pCmdUI->SetCheck (0);
	}
}

// Abgeleitete Methoden von PageUpdate

void CArt2::Show ()
{
   for (int i = 1; i < dlg.GetPageCount (); i ++)
   {
		CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (i);
		if (IsWindow (Page->m_hWnd))
		{
			Page->UpdatePage ();
		}
   }
}

void CArt2::Get ()
{
   for (int i = 1; i < 2; i ++)
   {
		CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (i);
		if (IsWindow (Page->m_hWnd))
		{
			Page->GetPage ();
		}
		else
		{
			dlg.SetActivePage (i);
			Page->GetPage ();
		}
   }
}

void CArt2::Back ()
{
     dlg.StepBack ();
}

void CArt2::Write ()
{
     dlg.Write ();
}

void CArt2::Delete ()
{
	dlg.Delete ();
}

void CArt2::OnUpdateFileSave(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page->m_Mdn.IsWindowEnabled ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnUpdateDateiL(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page->m_Mdn.IsWindowEnabled ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::UpdateAll ()
{
	GetDocument ()->UpdateAllViews (this);
}
void CArt2::OnMat()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page != NULL)
   {
	   Page->OnMatchoice ();
   }
}

void CArt2::OnInfo()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	 CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
	 Page->OnInfo ();
}

void CArt2::OnUpdateInfo(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
   CBasisdatenPage *Page = (CBasisdatenPage *) dlg.GetPage (0);
   if (Page->m_Mdn.IsWindowEnabled ())
   {
	   pCmdUI->Enable (FALSE);
   }
   else
   {
	   pCmdUI->Enable ();
   }
}

void CArt2::OnRecChange()
{
	Page1.OnRecChange ();
}
