// MainFrm.cpp : Implementierung der Klasse CMainFrame
//

#include "stdafx.h"
#include "Artikel.h"

#include "MainFrm.h"

#ifdef _DEBUG 
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	ON_WM_CREATE()
	// Globale Hilfebefehle
//	ON_COMMAND(ID_HELP, CMDIFrameWnd::OnHelp)
	ON_COMMAND(ID_CONTEXT_HELP, CMDIFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CMDIFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_STD_PR, &CMainFrame::OnStdPr)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // Statusleistenanzeige
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame Erstellung/Zerst�rung

CMainFrame::CMainFrame()
{
	EnableActiveAccessibility();
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
	ArtWnd = NULL;
	MatWnd = NULL;
	FreiWnd = NULL;
	APrWnd = NULL;
	StdPrWnd = NULL;
	WithEK = TRUE;
	WithSpracheneditor = TRUE;
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Symbolleiste konnte nicht erstellt werden\n");
		return -1;      // Fehler bei Erstellung
	}

	if (!CArtikelApp::WithEK)
	{

		HMENU hMenu = lpCreateStruct->hMenu;
		if (hMenu != NULL)
		{
			CMenu Menu;
			Menu.Attach (hMenu);
			Menu.DeleteMenu (ID_EKPR, MF_BYCOMMAND);
			Menu.Detach ();
		}
	}

	if (!m_wndToolBar2.CreateEx(this, TBSTYLE_FLAT, 
		  WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar2.LoadToolBar(IDR_DLGMENU))
	{
		TRACE0("Symbolleiste konnte nicht erstellt werden\n");
		return -1;      // Fehler bei Erstellung
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Statusleiste konnte nicht erstellt werden\n");
		return -1;      // Fehler bei Erstellung
	}
	// TODO: L�schen Sie diese drei Zeilen, wenn Sie nicht m�chten, dass die Systemleiste andockbar ist
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndToolBar2.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

    m_wndToolBar2.SetButtonStyle (0, TBBS_AUTOSIZE);   
    m_wndToolBar2.SetButtonStyle (1, TBBS_AUTOSIZE);   
    m_wndToolBar2.SetButtonStyle (2, TBBS_AUTOSIZE);   
    m_wndToolBar2.SetButtonStyle (3, TBBS_AUTOSIZE);   
//    m_wndToolBar2.SetButtonStyle (4, TBBS_AUTOSIZE);   
    m_wndToolBar2.SetButtonStyle (5, TBBS_AUTOSIZE);   //050214
	VERIFY (m_wndToolBar2.SetButtonText (0, _T("Artikelstamm")));
	VERIFY (m_wndToolBar2.SetButtonText (1, _T("Materialstamm")));
	VERIFY (m_wndToolBar2.SetButtonText (2, _T("Ladenpreise")));
	VERIFY (m_wndToolBar2.SetButtonText (3, _T("Gro�handelspreise")));
	if (CArtikelApp::WithEK)
	{
		VERIFY (m_wndToolBar2.SetButtonText (4, _T("EK-Preise")));
	}
	if (CArtikelApp::WithSpracheneditor)
	{
		VERIFY (m_wndToolBar2.SetButtonText (5, _T("Spracheneditor"))); //050214
	}
	DockControlBar(&m_wndToolBar2);

    CRect temp;
    m_wndToolBar2.GetItemRect(0,&temp);

    m_wndToolBar2.GetToolBarCtrl().SetButtonSize(CSize(temp.Width() + 15,
      temp.Height()));
 
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE
		 | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE | WS_SYSMENU;

	return TRUE;
}


// CMainFrame Diagnose

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame Meldungshandler


void CMainFrame::OnStdPr()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
}
