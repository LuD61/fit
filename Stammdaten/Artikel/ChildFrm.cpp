// ChildFrm.cpp : Implementierung der Klasse CChildFrame
//
#include "stdafx.h"
#include "Artikel.h"
#include "Art1.h"
#include "Art2.h"
#include "Apr1.h"
#include "Ipr1.h"

#include "MainFrm.h"
#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define RECCHANGE_MAT                       WM_USER + 2000
#define RECCHANGE_ART                       WM_USER + 2001
#define ACTIVATE_MAT                        WM_USER + 2002
#define ACTIVATE_ART                        WM_USER + 2003
#define RECCHANGE                           WM_USER + 2004

extern CArtikelApp theApp;

// CChildFrame
CVector CChildFrame::ArtWnd;
CVector CChildFrame::MatWnd;
CVector CChildFrame::FreiWnd;
CVector CChildFrame::APrWnd;
CVector CChildFrame::StdPrWnd;


IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	ON_WM_SIZE ()
	ON_WM_DESTROY ()
	ON_COMMAND (RECCHANGE_MAT, OnRecChangeMat)
	ON_COMMAND (RECCHANGE_ART, OnRecChangeArt)
	ON_COMMAND (ACTIVATE_MAT, OnActivateMat)
	ON_COMMAND (ACTIVATE_ART, OnActivateArt)
	ON_COMMAND (RECCHANGE, OnRecChange)
END_MESSAGE_MAP()


// CChildFrame Erstellung/Zerst�rung

CChildFrame::CChildFrame()
{
	EnableActiveAccessibility();
	InitSize = FALSE;
	ArtWnd.Init ();
	MatWnd.Init ();
	FreiWnd.Init ();
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen
}

CChildFrame::~CChildFrame()
{
}


BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie die Fensterklasse oder die Stile hier, indem Sie CREATESTRUCT �ndern
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;
	return TRUE;
}

BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	lpcs->cy = 500;
	BOOL ret = CMDIChildWnd::OnCreateClient(lpcs,   pContext);	

	CStringA ClassName = pContext->m_pNewViewClass->m_lpszClassName;
	if (ClassName == "CArt1")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->ArtWnd == NULL)
		{
			MainFrm->ArtWnd = this;
		}
		else
		{
			ArtWnd.Add (this);
		}
	}
	if (ClassName == "CArt2")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->MatWnd == NULL)
		{
			MainFrm->MatWnd = this;
		}
		else
		{
			MatWnd.Add (this);
		}
	}
	else if (ClassName == "CApr1")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->APrWnd == NULL)
		{
			MainFrm->APrWnd = this;
		}
		else
		{
			APrWnd.Add (this);
		}
	}
	else if (ClassName == "CIpr1")
	{
		CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
		if (MainFrm->StdPrWnd == NULL)
		{
			MainFrm->StdPrWnd = this;
		}
		else
		{
			StdPrWnd.Add (this);
		}
	}

	return ret;
}

void CChildFrame::OnSize(UINT nType, int cx, int cy)
{
	CMDIChildWnd::OnSize (nType, cx, cy);
	if (!InitSize)
	{
		    CRect pRect;
			GetParent ()->GetClientRect (pRect);
			CRect rect;
			GetWindowRect (&rect);
			GetParent ()->ScreenToClient (&rect);
			rect.bottom = pRect.bottom ;
			rect.right += 55;
			rect.top = 0;
			InitSize = TRUE;
			MoveWindow (&rect, TRUE);
	}
}


// CChildFrame Diagnose

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


// CChildFrame Meldungshandler

void CChildFrame::OnDestroy()
{
/*
	ArtWnd = NULL;
	MatWnd = NULL;
	FreiWnd = NULL;

CVector CChildFrame::ArtWnd;
CVector CChildFrame::MatWnd;
CVector CChildFrame::FreiWnd;

*/

	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	if (MainFrm->ArtWnd == this)
	{
		MainFrm->ArtWnd = NULL;
		if (ArtWnd.GetCount () != 0)
		{
			MainFrm->ArtWnd = (CWnd *) ArtWnd.Get (0);
			ArtWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
	else if (MainFrm->MatWnd == this)
	{
		MainFrm->MatWnd = NULL;
		if (MatWnd.GetCount () != 0)
		{
			MainFrm->MatWnd = (CWnd *) MatWnd.Get (0);
			MatWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
	else if (MainFrm->APrWnd == this)
	{
		MainFrm->APrWnd = NULL;
		if (APrWnd.GetCount () != 0)
		{
			MainFrm->APrWnd = (CWnd *) APrWnd.Get (0);
			APrWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
	else if (MainFrm->StdPrWnd == this)
	{
		MainFrm->StdPrWnd = NULL;
		if (StdPrWnd.GetCount () != 0)
		{
			MainFrm->StdPrWnd = (CWnd *) StdPrWnd.Get (0);
			StdPrWnd.Drop (0);
		}
		InitSize = FALSE;
		return;
	}
	int i = -1;
	if ((i = ArtWnd.Find (this)) != -1)
	{
		ArtWnd.Drop (i);
		InitSize = FALSE;
	}
	else if ((i = MatWnd.Find (this)) != -1)
	{
		MatWnd.Drop (i);
		InitSize = FALSE;
	}
	else if ((i = APrWnd.Find (this)) != -1)
	{
		APrWnd.Drop (i);
		InitSize = FALSE;
	}
	else if ((i = StdPrWnd.Find (this)) != -1)
	{
		StdPrWnd.Drop (i);
		InitSize = FALSE;
	}
}

void CChildFrame::OnRecChangeMat()
{
	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	theApp.OnMaterialstamm ();
	if (MainFrm->MatWnd != NULL)
	{
		CArt2 *view = (CArt2 *) ((CChildFrame *) MainFrm->MatWnd)->GetActiveView ();
		if (view != NULL)
		{
			view->OnRecChange ();
		}
	}
}

void CChildFrame::OnRecChangeArt()
{
	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	theApp.OnArtikelstamm ();
	if (MainFrm->ArtWnd != NULL)
	{
		CArt1 *view = (CArt1 *) ((CChildFrame *) MainFrm->ArtWnd)->GetActiveView ();
		if (view != NULL)
		{
			view->OnRecChange ();
		}
	}
}

void CChildFrame::OnActivateMat()
{
	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	theApp.OnMaterialstamm ();
}

void CChildFrame::OnActivateArt()
{
	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	theApp.OnArtikelstamm ();
}

void CChildFrame::OnRecChange()
{
	CMainFrame *MainFrm = (CMainFrame *) GetParent ()->GetParent ();
	if (MainFrm->APrWnd != NULL)
	{
		CApr1 *aprview = (CApr1 *) ((CChildFrame *) MainFrm->APrWnd)->GetActiveView ();
		if (aprview != NULL)
		{
			aprview->OnRecChange ();
		}
	}
	if (MainFrm->StdPrWnd != NULL)

	{
		CIpr1 *iprview = (CIpr1 *) ((CChildFrame *) MainFrm->StdPrWnd)->GetActiveView ();
		if (iprview != NULL)
		{
			iprview->OnRecChange ();
		}
	}
	if (MainFrm->ArtWnd != NULL)
	{
		CArt1 *artview = (CArt1 *) ((CChildFrame *) MainFrm->ArtWnd)->GetActiveView ();
		if (artview != NULL)
		{
			artview->OnRecChange ();
		}
	}
}
