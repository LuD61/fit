#pragma once
#include "afxwin.h"
#include "QuikInfo.h"
#include "ControlContainer.h"

class CColorButton :
	public CStatic
//	public CButton
{
	DECLARE_DYNCREATE(CColorButton)
protected:
	DWORD Style;
	DECLARE_MESSAGE_MAP()
	virtual void OnSize (UINT, int, int);
	virtual void DrawItem (LPDRAWITEMSTRUCT  lpDrawItemStruct);
//	virtual BOOL PreTranslateMessage(MSG* pMsg ); 
public:
	enum
	{
		Center = 0,
		Left = 1,
		Right = 2,
	};
	enum
	{
		Standard = 0,
		Underline = 1,
		Bold = 2,
		UnderlineBold = 3,
	};

	enum
	{
		Solide = 0,
		Dot = 1,
		NoBorder = 2,
		Rounded = 3,
	};

	int Orientation;
	int TextStyle;
	int BorderStyle;
	BOOL DynamicColor;
	BOOL IsActive;
	BOOL IsSelected;
	static COLORREF DynColorGray;
	static COLORREF DynColorBlue;
	COLORREF DynColor;
	COLORREF RolloverColor;
	COLORREF ActiveColor;
//	CToolTipCtrl Tooltip;
//	CStatic Tooltip;
	CQuikInfo Tooltip;
	COLORREF TextColor;
	HCURSOR Hand;
	HCURSOR Arrow;
    BOOL ButtonCursor;
	CBitmap Bitmap;
	CBitmap TransBitmap;
	HBITMAP hTransBitmap;
	CBitmap Mask;
	HBITMAP hMask;
	UINT nID;
	COLORREF BkColor;
	int xSpace;
	CControlContainer *Container;

	BOOL ColorSet;
	BOOL NoUpdate;
	CFont Font;
	BOOL FontSet;
	CColorButton(void);
	~CColorButton (void);
    virtual BOOL Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect,
                        CWnd* pParentWnd,   UINT nID = 0xffff);
	void SetToolTip (LPTSTR Text);
	void LoadBitmap (UINT ID);
	void LoadMask (UINT ID);
	void SetBkColor (COLORREF color);
	void Draw (CDC&);
    void SetButtonCursor (BOOL b);
    void SetActive (BOOL b = TRUE);
    void SetSelected (BOOL b = TRUE);
    BOOL InClient (CPoint p);
    void SetButtonCursor (CPoint p);
	void SetFont ();
	virtual void SetFont (CFont *f, BOOL Redrae=TRUE);
    afx_msg void OnMouseMove(UINT nFlags,  CPoint p);
    afx_msg void OnLButtonDown(UINT nFlags,  CPoint p);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest,  UINT message );
	afx_msg void OnKeyDown(UINT nChar,  UINT nRepCnt,  UINT nFlags);
	afx_msg void OnSetFocus(CWnd *cWnd);
	afx_msg void OnKillFocus(CWnd *cWnd);
	afx_msg void OnShowWindow (BOOL bShow, UINT nStatus);
	void FillRectParts (CDC &cDC, COLORREF color, CRect rect);
	void FillRoundedRect (CDC& cDC, CRect *rect, COLORREF Color);
};
