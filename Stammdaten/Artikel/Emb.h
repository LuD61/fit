#pragma once
#include "afxwin.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "BasisdatenPage.h"
#include "UpdateEvent.h"
#include "A_bas.h"
#include "A_hndw.h"
#include "A_eig.h"
#include "A_emb.h"
#include "A_krz.h"
#include "A_pr.h"
#include "Sys_par.h"
#include "StaticButton.h"

#define IDC_EMBSAVE 3011


// CEmb-Dialogfeld

class CEmb : public CDialog, public CUpdateEvent
{
	DECLARE_DYNAMIC(CEmb)

public:
	CEmb(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CEmb();

// Dialogfelddaten
	enum { IDD = IDD_EMB_DATA };

protected:
    int MdnCursor;
	int FilCursor;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();    // DDX/DDV-Unterstützung
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 
	virtual void OnSize (UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
public:
	int dcs_a_par;
	int waa_a_par;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	CBasisdatenPage *Basis;
	CCtrlGrid CtrlGrid;
	CFormTab Form;
	CFont Font;
	BOOL AkrzGen;
	A_BAS_CLASS *A_bas;
	A_HNDW_CLASS *A_hndw;
	A_EIG_CLASS *A_eig;
	A_EMB_CLASS *A_emb;
	A_KRZ_CLASS *A_krz;
	APR_CLASS A_pr;
	SYS_PAR_CLASS *Sys_par;
	CStatic m_LUntEmb;
	CNumEdit m_UntEmb;
	CStatic m_LEmb;
	CNumEdit m_Emb;
	CStatic m_LEmbBz;
	CTextEdit m_EmbBz;
	CStatic m_LAnzEmb;
	CNumEdit m_AnzEmb;
	CStatic m_LAPfa;
	CNumEdit m_APfa;
	CButton m_EmbVkKz;
	CStatic m_LTaraProz;
	CNumEdit m_TaraProz;
	CStatic m_LTara;
	CNumEdit m_Tara;
	CStatic m_LAkrz;
	CNumEdit m_Akrz;
	CButton m_APfaChoice;
	CCtrlGrid APfaGrid;
	CNumEdit m_PrVk;
	CStaticButton m_Save;

	void SetBasis (CBasisdatenPage *Basis);
	virtual void Update ();
	virtual BOOL OnReturn ();
	void UpdateList ();
	BOOL TestEmb ();
	void SetAkrzField ();
	void FillAkrz ();
	BOOL GenAkrz ();
	CStatic m_EmbGroup;
	afx_msg void OnChoiceAPfa();
	afx_msg void OnSave ();
	afx_msg void OnBnClickedEmbVkKz();
};
