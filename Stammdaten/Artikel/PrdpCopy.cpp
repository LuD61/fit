#include "StdAfx.h"
#include "PrdpCopy.h"

LPTSTR CPrdpCopy::ErrorText[] = {_T ("OK"),
                                 _T ("Artikel zum  Kopieren nicht gefunden"),
                                 _T ("unbekannter Fehler"),
};

CPrdpCopy::CPrdpCopy(void)
{
}

CPrdpCopy::~CPrdpCopy(void)
{
}

void CPrdpCopy::Run (A_BAS_CLASS *dest,A_BAS_ERW_CLASS *dest_erw, double a_source, int ZutSource)
{
	error = NoError;
	A_bas_source.a_bas.a = a_source;
	if (A_bas_source.dbreadfirst () != 0)
	{
		error = SourceNotFound;
		return;
	}

	_tcscpy (dest->a_bas.lgr_tmpr,A_bas_source.a_bas.lgr_tmpr);
	_tcscpy (dest->a_bas.prod_mass,A_bas_source.a_bas.prod_mass);
	_tcscpy (dest->a_bas.produkt_info,A_bas_source.a_bas.produkt_info);
	_tcscpy (dest->a_bas.huel_art,A_bas_source.a_bas.huel_art);
	_tcscpy (dest->a_bas.huel_mat,A_bas_source.a_bas.huel_mat);
	_tcscpy (dest->a_bas.ausz_art,A_bas_source.a_bas.ausz_art);
	_tcscpy (dest->a_bas.ean_packung,A_bas_source.a_bas.ean_packung);
	_tcscpy (dest->a_bas.ean_karton,A_bas_source.a_bas.ean_karton);
	_tcscpy (dest->a_bas.pr_ausz,A_bas_source.a_bas.pr_ausz);
	_tcscpy (dest->a_bas.huel_peel,A_bas_source.a_bas.huel_peel);
	_tcscpy (dest->a_bas.gruener_punkt,A_bas_source.a_bas.gruener_punkt);
	_tcscpy (dest->a_bas.gebinde_art,A_bas_source.a_bas.gebinde_art);
	_tcscpy (dest->a_bas.gebinde_mass,A_bas_source.a_bas.gebinde_mass);
	_tcscpy (dest->a_bas.gebinde_leer_gew,A_bas_source.a_bas.gebinde_leer_gew);
	_tcscpy (dest->a_bas.gebinde_brutto_gew,A_bas_source.a_bas.gebinde_brutto_gew);
	dest->a_bas.gebinde_pack = A_bas_source.a_bas.gebinde_pack;
	dest->a_bas.gebindeprolage = A_bas_source.a_bas.gebindeprolage;
	dest->a_bas.gebindeanzlagen = A_bas_source.a_bas.gebindeanzlagen;
	dest->a_bas.gebindepalette = A_bas_source.a_bas.gebindepalette;
	_tcscpy (dest->a_bas.beffe,A_bas_source.a_bas.beffe);
	_tcscpy (dest->a_bas.beffe_fe,A_bas_source.a_bas.beffe_fe);
	_tcscpy (dest->a_bas.brennwert,A_bas_source.a_bas.brennwert);
	dest->a_bas.eiweiss = A_bas_source.a_bas.eiweiss;
	dest->a_bas.kh = A_bas_source.a_bas.kh;
	dest->a_bas.fett = A_bas_source.a_bas.fett;
	_tcscpy (dest->a_bas.aerob,A_bas_source.a_bas.aerob);
	_tcscpy (dest->a_bas.coli,A_bas_source.a_bas.coli);
	_tcscpy (dest->a_bas.keime,A_bas_source.a_bas.keime);
	_tcscpy (dest->a_bas.listerien,A_bas_source.a_bas.listerien);
	_tcscpy (dest->a_bas.list_mono,A_bas_source.a_bas.list_mono);
	_tcscpy (dest->a_bas.prodpass_extra,A_bas_source.a_bas.prodpass_extra);
	_tcscpy (dest->a_bas.glg,A_bas_source.a_bas.glg);
	_tcscpy (dest->a_bas.krebs,A_bas_source.a_bas.krebs);
	_tcscpy (dest->a_bas.ei,A_bas_source.a_bas.ei);
	_tcscpy (dest->a_bas.fisch,A_bas_source.a_bas.fisch);
	_tcscpy (dest->a_bas.erdnuss,A_bas_source.a_bas.erdnuss);
	_tcscpy (dest->a_bas.soja,A_bas_source.a_bas.soja);
	_tcscpy (dest->a_bas.milch,A_bas_source.a_bas.milch);
	_tcscpy (dest->a_bas.schal,A_bas_source.a_bas.schal);
	_tcscpy (dest->a_bas.sellerie,A_bas_source.a_bas.sellerie);
	_tcscpy (dest->a_bas.senfsaat,A_bas_source.a_bas.senfsaat);
	_tcscpy (dest->a_bas.sesamsamen,A_bas_source.a_bas.sesamsamen);
	_tcscpy (dest->a_bas.sulfit,A_bas_source.a_bas.sulfit);
	dest->a_bas.zerl_eti = A_bas_source.a_bas.zerl_eti;
// ???	_tcscpy (dest->a_bas.restlaufzeit,A_bas_source.a_bas.restlaufzeit);
	dest->a_bas.restlaufzeit = A_bas_source.a_bas.restlaufzeit;
	_tcscpy (dest->a_bas.vakuumiert,A_bas_source.a_bas.vakuumiert);
	_tcscpy (dest->a_bas.loskennung,A_bas_source.a_bas.loskennung);
	dest->a_bas.stand = A_bas_source.a_bas.stand;
	_tcscpy (dest->a_bas.version_nr,A_bas_source.a_bas.version_nr);
	_tcscpy (dest->a_bas.gueltigkeit,A_bas_source.a_bas.gueltigkeit);
	_tcscpy (dest->a_bas.weichtier,A_bas_source.a_bas.weichtier);
	_tcscpy (dest->a_bas.sonstige,A_bas_source.a_bas.sonstige);
	_tcscpy (dest->a_bas.gmo_gvo,A_bas_source.a_bas.gmo_gvo);
	dest->a_bas.pack_vol = A_bas_source.a_bas.pack_vol;
	dest->a_bas.min_bestellmenge = A_bas_source.a_bas.min_bestellmenge;
	dest->a_bas.staffelung = A_bas_source.a_bas.staffelung;
	dest->a_bas.inh_karton = A_bas_source.a_bas.inh_karton;
	dest->a_bas.packung_karton = A_bas_source.a_bas.packung_karton;

	A_bas_erw_source.a_bas_erw.a = a_source;
	if (A_bas_erw_source.dbreadfirst () != 0)
	{
	}
	_tcscpy (dest_erw->a_bas_erw.pp_a_bz1,A_bas_source.a_bas.a_bz1);
	_tcscpy (dest_erw->a_bas_erw.pp_a_bz2,A_bas_source.a_bas.a_bz2);
	dest_erw->a_bas_erw.salz = A_bas_erw_source.a_bas_erw.salz; //KUB-13
	dest_erw->a_bas_erw.davonfett = A_bas_erw_source.a_bas_erw.davonfett;//KUB-13
	dest_erw->a_bas_erw.davonzucker = A_bas_erw_source.a_bas_erw.davonzucker;//KUB-13
	dest_erw->a_bas_erw.ballaststoffe = A_bas_erw_source.a_bas_erw.ballaststoffe;//KUB-13
	_tcscpy (dest_erw->a_bas_erw.lgr_tmpr,A_bas_erw_source.a_bas_erw.lgr_tmpr); //FS-317
	_tcscpy (dest_erw->a_bas_erw.lupine,A_bas_erw_source.a_bas_erw.lupine); //FS-317
	_tcscpy (dest_erw->a_bas_erw.schutzgas,A_bas_erw_source.a_bas_erw.schutzgas); //FS-317
	dest_erw->a_bas_erw.huelle = A_bas_erw_source.a_bas_erw.huelle;//KUB-13
	dest_erw->a_bas_erw.shop_wg1 = A_bas_erw_source.a_bas_erw.shop_wg1;//KUB-13
	dest_erw->a_bas_erw.shop_wg2 = A_bas_erw_source.a_bas_erw.shop_wg2;//KUB-13
	dest_erw->a_bas_erw.shop_wg3 = A_bas_erw_source.a_bas_erw.shop_wg3;//KUB-13
	dest_erw->a_bas_erw.shop_wg4 = A_bas_erw_source.a_bas_erw.shop_wg4;//KUB-13
	dest_erw->a_bas_erw.shop_wg5 = A_bas_erw_source.a_bas_erw.shop_wg5;//KUB-13
	_tcscpy (dest_erw->a_bas_erw.shop_aktion,A_bas_erw_source.a_bas_erw.shop_aktion); //FS-317
	_tcscpy (dest_erw->a_bas_erw.shop_neu,A_bas_erw_source.a_bas_erw.shop_neu); //FS-317
	_tcscpy (dest_erw->a_bas_erw.shop_tv,A_bas_erw_source.a_bas_erw.shop_tv); //FS-317
	dest_erw->a_bas_erw.shop_agew = A_bas_erw_source.a_bas_erw.shop_agew;//KUB-13
	dest_erw->a_bas_erw.userdef1 = A_bas_erw_source.a_bas_erw.userdef1;//KUB-13
	dest_erw->a_bas_erw.userdef2 = A_bas_erw_source.a_bas_erw.userdef2;//KUB-13
	dest_erw->a_bas_erw.userdef3 = A_bas_erw_source.a_bas_erw.userdef3;//KUB-13
	if (ZutSource == this->ZutFromA_bas_erw) 
	{
		_tcscpy (dest_erw->a_bas_erw.zutat,A_bas_erw_source.a_bas_erw.zutat); //FS-317
	}


	Prp_mibwerte.prp_mibwerte.a = a_source;
	if (Prp_mibwerte.dbreadfirst () == 0)
	{
		Prp_mibwerte.prp_mibwerte.a = dest->a_bas.a;
		Prp_mibwerte.dbupdate ();
	}

	Prp_cpp.prp_cpp.a = a_source;
	if (Prp_cpp.dbreadfirst () == 0)
	{
		Prp_cpp.prp_cpp.a = dest->a_bas.a;
		Prp_cpp.dbupdate ();
	}

	Prp_vik.prp_vik.a = a_source;
	if (Prp_vik.dbreadfirst () == 0)
	{
		Prp_vik.prp_vik.a = dest->a_bas.a;
		Prp_vik.dbupdate ();
	}

	Prp_user.prp_user.a = a_source;
	if (Prp_user.dbreadfirst () == 0)
	{
		Prp_user.prp_user.a = dest->a_bas.a;
		Prp_user.dbupdate ();
	}

	Prp_zusatz.prp_zusatz.a = a_source;
	if (Prp_zusatz.dbreadfirst () == 0)
	{
		Prp_zusatz.prp_zusatz.a = dest->a_bas.a;
		Prp_zusatz.dbupdate ();
	}

	Hst_info.hst_info.a = a_source;
	if (Hst_info.dbreadfirst () == 0)
	{
		Hst_info.hst_info.a = dest->a_bas.a;
		Hst_info.dbupdate ();
	}

	if (ZutSource == this->ZutFromAtexte)
	{
		Atexte.atexte.a = a_source;
		if (a_source < 100000) 
		{
			Atexte.atexte.txt_nr = (long) a_source * 1000 + 1;
		}
		else
		{
			Atexte.atexte.txt_nr = (long) a_source ;
		}
		if (Atexte.dbreadfirst () == 0)
		{
			Atexte.atexte.a = dest->a_bas.a;
			if (dest->a_bas.a < 100000) 
			{
				Atexte.atexte.txt_nr = (long) dest->a_bas.a * 1000 + 1;
			}
			else
			{
				Atexte.atexte.txt_nr = (long) dest->a_bas.a ;
			}
			Atexte.dbupdate();
		}
	}

}
