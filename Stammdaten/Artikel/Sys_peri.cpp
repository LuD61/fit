#include "stdafx.h"
#include "sys_peri.h"

struct SYS_PERI sys_peri, sys_peri_null;

void SYS_PERI_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((long *)   &sys_peri.sys,  SQLLONG, 0);
    sqlout ((short *) &sys_peri.anschluss,SQLSHORT,0);
    sqlout ((short *) &sys_peri.anz_waehl_wied,SQLSHORT,0);
    sqlout ((short *) &sys_peri.auszeit,SQLSHORT,0);
    sqlout ((short *) &sys_peri.baudrate,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &sys_peri.dat_abruf,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &sys_peri.dat_update,SQLDATE,0);
    sqlout ((short *) &sys_peri.daten_bit,SQLSHORT,0);
    sqlout ((long *) &sys_peri.drv_pid,SQLLONG,0);
    sqlout ((short *) &sys_peri.fil,SQLSHORT,0);
    sqlout ((short *) &sys_peri.kanal,SQLSHORT,0);
    sqlout ((short *) &sys_peri.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) sys_peri.modem_typ,SQLCHAR,3);
    sqlout ((TCHAR *) sys_peri.parity,SQLCHAR,2);
    sqlout ((TCHAR *) sys_peri.peri_nam,SQLCHAR,21);
    sqlout ((short *) &sys_peri.peri_typ,SQLSHORT,0);
    sqlout ((short *) &sys_peri.port_typ,SQLSHORT,0);
    sqlout ((short *) &sys_peri.protokoll,SQLSHORT,0);
    sqlout ((short *) &sys_peri.retry_max,SQLSHORT,0);
    sqlout ((short *) &sys_peri.status_abruf,SQLSHORT,0);
    sqlout ((short *) &sys_peri.stop_bit,SQLSHORT,0);
    sqlout ((long *) &sys_peri.sys,SQLLONG,0);
    sqlout ((short *) &sys_peri.sys_abruf,SQLSHORT,0);
    sqlout ((short *) &sys_peri.sys_update,SQLSHORT,0);
    sqlout ((TCHAR *) sys_peri.tel,SQLCHAR,17);
    sqlout ((TCHAR *) sys_peri.term,SQLCHAR,13);
    sqlout ((TCHAR *) sys_peri.txt,SQLCHAR,61);
    sqlout ((TCHAR *) sys_peri.zeit_abruf,SQLCHAR,7);
    sqlout ((short *) &sys_peri.stat,SQLSHORT,0);
    sqlout ((TCHAR *) sys_peri.b_check,SQLCHAR,2);
    sqlout ((short *) &sys_peri.la_zahl,SQLSHORT,0);
            cursor = sqlcursor (_T("select ")
_T("sys_peri.anschluss,  sys_peri.anz_waehl_wied,  sys_peri.auszeit,  ")
_T("sys_peri.baudrate,  sys_peri.dat_abruf,  sys_peri.dat_update,  ")
_T("sys_peri.daten_bit,  sys_peri.drv_pid,  sys_peri.fil,  sys_peri.kanal,  ")
_T("sys_peri.mdn,  sys_peri.modem_typ,  sys_peri.parity,  ")
_T("sys_peri.peri_nam,  sys_peri.peri_typ,  sys_peri.port_typ,  ")
_T("sys_peri.protokoll,  sys_peri.retry_max,  sys_peri.status_abruf,  ")
_T("sys_peri.stop_bit,  sys_peri.sys,  sys_peri.sys_abruf,  ")
_T("sys_peri.sys_update,  sys_peri.tel,  sys_peri.term,  sys_peri.txt,  ")
_T("sys_peri.zeit_abruf,  sys_peri.stat,  sys_peri.b_check,  ")
_T("sys_peri.la_zahl from sys_peri ")

#line 12 "Sys_peri.rpp"
                                  _T("where sys = ?"));
    sqlin ((short *) &sys_peri.anschluss,SQLSHORT,0);
    sqlin ((short *) &sys_peri.anz_waehl_wied,SQLSHORT,0);
    sqlin ((short *) &sys_peri.auszeit,SQLSHORT,0);
    sqlin ((short *) &sys_peri.baudrate,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &sys_peri.dat_abruf,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sys_peri.dat_update,SQLDATE,0);
    sqlin ((short *) &sys_peri.daten_bit,SQLSHORT,0);
    sqlin ((long *) &sys_peri.drv_pid,SQLLONG,0);
    sqlin ((short *) &sys_peri.fil,SQLSHORT,0);
    sqlin ((short *) &sys_peri.kanal,SQLSHORT,0);
    sqlin ((short *) &sys_peri.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) sys_peri.modem_typ,SQLCHAR,3);
    sqlin ((TCHAR *) sys_peri.parity,SQLCHAR,2);
    sqlin ((TCHAR *) sys_peri.peri_nam,SQLCHAR,21);
    sqlin ((short *) &sys_peri.peri_typ,SQLSHORT,0);
    sqlin ((short *) &sys_peri.port_typ,SQLSHORT,0);
    sqlin ((short *) &sys_peri.protokoll,SQLSHORT,0);
    sqlin ((short *) &sys_peri.retry_max,SQLSHORT,0);
    sqlin ((short *) &sys_peri.status_abruf,SQLSHORT,0);
    sqlin ((short *) &sys_peri.stop_bit,SQLSHORT,0);
    sqlin ((long *) &sys_peri.sys,SQLLONG,0);
    sqlin ((short *) &sys_peri.sys_abruf,SQLSHORT,0);
    sqlin ((short *) &sys_peri.sys_update,SQLSHORT,0);
    sqlin ((TCHAR *) sys_peri.tel,SQLCHAR,17);
    sqlin ((TCHAR *) sys_peri.term,SQLCHAR,13);
    sqlin ((TCHAR *) sys_peri.txt,SQLCHAR,61);
    sqlin ((TCHAR *) sys_peri.zeit_abruf,SQLCHAR,7);
    sqlin ((short *) &sys_peri.stat,SQLSHORT,0);
    sqlin ((TCHAR *) sys_peri.b_check,SQLCHAR,2);
    sqlin ((short *) &sys_peri.la_zahl,SQLSHORT,0);
            sqltext = _T("update sys_peri set ")
_T("sys_peri.anschluss = ?,  sys_peri.anz_waehl_wied = ?,  ")
_T("sys_peri.auszeit = ?,  sys_peri.baudrate = ?,  ")
_T("sys_peri.dat_abruf = ?,  sys_peri.dat_update = ?,  ")
_T("sys_peri.daten_bit = ?,  sys_peri.drv_pid = ?,  sys_peri.fil = ?,  ")
_T("sys_peri.kanal = ?,  sys_peri.mdn = ?,  sys_peri.modem_typ = ?,  ")
_T("sys_peri.parity = ?,  sys_peri.peri_nam = ?,  ")
_T("sys_peri.peri_typ = ?,  sys_peri.port_typ = ?,  ")
_T("sys_peri.protokoll = ?,  sys_peri.retry_max = ?,  ")
_T("sys_peri.status_abruf = ?,  sys_peri.stop_bit = ?,  ")
_T("sys_peri.sys = ?,  sys_peri.sys_abruf = ?,  ")
_T("sys_peri.sys_update = ?,  sys_peri.tel = ?,  sys_peri.term = ?,  ")
_T("sys_peri.txt = ?,  sys_peri.zeit_abruf = ?,  sys_peri.stat = ?,  ")
_T("sys_peri.b_check = ?,  sys_peri.la_zahl = ? ")

#line 14 "Sys_peri.rpp"
                                  _T("where sys = ?");
            sqlin ((long *)   &sys_peri.sys,  SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &sys_peri.sys,  SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select sys from sys_peri ")
                                  _T("where sys = ?"));
            sqlin ((long *)   &sys_peri.sys,  SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from sys_peri ")
                                  _T("where sys = ?"));
    sqlin ((short *) &sys_peri.anschluss,SQLSHORT,0);
    sqlin ((short *) &sys_peri.anz_waehl_wied,SQLSHORT,0);
    sqlin ((short *) &sys_peri.auszeit,SQLSHORT,0);
    sqlin ((short *) &sys_peri.baudrate,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &sys_peri.dat_abruf,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &sys_peri.dat_update,SQLDATE,0);
    sqlin ((short *) &sys_peri.daten_bit,SQLSHORT,0);
    sqlin ((long *) &sys_peri.drv_pid,SQLLONG,0);
    sqlin ((short *) &sys_peri.fil,SQLSHORT,0);
    sqlin ((short *) &sys_peri.kanal,SQLSHORT,0);
    sqlin ((short *) &sys_peri.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) sys_peri.modem_typ,SQLCHAR,3);
    sqlin ((TCHAR *) sys_peri.parity,SQLCHAR,2);
    sqlin ((TCHAR *) sys_peri.peri_nam,SQLCHAR,21);
    sqlin ((short *) &sys_peri.peri_typ,SQLSHORT,0);
    sqlin ((short *) &sys_peri.port_typ,SQLSHORT,0);
    sqlin ((short *) &sys_peri.protokoll,SQLSHORT,0);
    sqlin ((short *) &sys_peri.retry_max,SQLSHORT,0);
    sqlin ((short *) &sys_peri.status_abruf,SQLSHORT,0);
    sqlin ((short *) &sys_peri.stop_bit,SQLSHORT,0);
    sqlin ((long *) &sys_peri.sys,SQLLONG,0);
    sqlin ((short *) &sys_peri.sys_abruf,SQLSHORT,0);
    sqlin ((short *) &sys_peri.sys_update,SQLSHORT,0);
    sqlin ((TCHAR *) sys_peri.tel,SQLCHAR,17);
    sqlin ((TCHAR *) sys_peri.term,SQLCHAR,13);
    sqlin ((TCHAR *) sys_peri.txt,SQLCHAR,61);
    sqlin ((TCHAR *) sys_peri.zeit_abruf,SQLCHAR,7);
    sqlin ((short *) &sys_peri.stat,SQLSHORT,0);
    sqlin ((TCHAR *) sys_peri.b_check,SQLCHAR,2);
    sqlin ((short *) &sys_peri.la_zahl,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into sys_peri (")
_T("anschluss,  anz_waehl_wied,  auszeit,  baudrate,  dat_abruf,  dat_update,  ")
_T("daten_bit,  drv_pid,  fil,  kanal,  mdn,  modem_typ,  parity,  peri_nam,  peri_typ,  ")
_T("port_typ,  protokoll,  retry_max,  status_abruf,  stop_bit,  sys,  sys_abruf,  ")
_T("sys_update,  tel,  term,  txt,  zeit_abruf,  stat,  b_check,  la_zahl) ")

#line 25 "Sys_peri.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 27 "Sys_peri.rpp"
}
