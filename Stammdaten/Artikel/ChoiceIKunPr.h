#ifndef _CHOICEIKUNPR_DEF
#define _CHOICEIKUNPR_DEF

#include "ChoiceX.h"
#include "IKunPrList.h"
#include "Vector.h"
#include <vector>

class CChoiceIKunPr : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
      
    public :
		long m_Mdn;
		CVector *Rows;
	    std::vector<CIKunPrList *> IKunPrList;
      	CChoiceIKunPr(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceIKunPr(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchKunPr (CListCtrl *,  LPTSTR);
        void SearchZusBz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CIKunPrList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
