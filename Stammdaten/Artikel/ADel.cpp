// ADel.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "ADel.h"
#include "DbUniCode.h"


// CADel-Dialogfeld

HANDLE CADel::ADel = NULL;
CADel *CADel::dlg;

void CADel::Printf (LPSTR format, va_list args)
{
	if (dlg == NULL) return;

	char buffer [256];
	TCHAR _tbuffer [256];

	vsprintf (buffer, format, args);
	CDbUniCode::DbToUniCode (_tbuffer, buffer);
	dlg->m_List.AddString (_tbuffer);
}

IMPLEMENT_DYNAMIC(CADel, CDialog)
CADel::CADel(CWnd* pParent /*=NULL*/)
	: CDialog(CADel::IDD, pParent)
{
	a_del = NULL;
	SetPrintProc = NULL;
}

CADel::~CADel()
{
}

void CADel::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_LIST1, m_List);
}


BEGIN_MESSAGE_MAP(CADel, CDialog)
END_MESSAGE_MAP()


// CADel-Meldungshandler

/*
BOOL CADel::Run (double a)
{
	if (ADel == NULL)
	{
		CString bws;
		BOOL ret = bws.GetEnvironmentVariable (_T("bws"));
		CString ADelDll;
		if (ret)
		{
			ADelDll.Format (_T("%s\\bin\\a_deld.dll"), bws.GetBuffer ());
		}
		else
		{
			ADelDll = _T("a_deld.dll");
		}
		ADel = LoadLibrary (ADelDll.GetBuffer ());
	}
	if (ADel != NULL && a_del == NULL)
	{
		a_del = (int (*) (int, char **))
					  GetProcAddress ((HMODULE) ADel, "a_del");
		SetPrintProc = (void (*) (void (CALLBACK *) (LPSTR, va_list)))
					  GetProcAddress ((HMODULE) ADel, "SetPrintProc");
	}
	if (a_del == NULL) return FALSE;
	if (SetPrintProc != NULL)
	{
		(*SetPrintProc) (Printf);
	}
	m_OK.EnableWindow (FALSE);
	char where[20];
	char *dbtab = "12100.all";
	sprintf (where, "-wwhere a = %.0lf", a);
	char *args[] = {"a_del", where, dbtab};
	(*a_del) (3, args);
	m_OK.EnableWindow (TRUE);
	return TRUE;
}
*/

BOOL CADel::Run (double a)
{
	m_OK.EnableWindow (FALSE);
	DelRec.FileName = _T("12100.del");
	DelRec.Output = &m_List;
	if (DelRec.Sections.size () == 0)
	{
		DelRec.Load ();
	}
	CString Where;
	Where.Format (_T("where a = %.0lf"), a);
	BOOL ret = DelRec.Run (Where);
	m_OK.EnableWindow (TRUE);
	return ret;
}