#ifndef _A_APR_DEF
#define _A_APR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_PR {
   double         a;
   short          akt;
   short          delstatus;
   short          fil;
   short          fil_gr;
   double         key_typ_dec13;
   short          key_typ_sint;
   TCHAR          lad_akv[2];
   TCHAR          lief_akv[2];
   short          mdn;
   short          mdn_gr;
   TCHAR          modif[2];
   double         pr_ek;
   double         pr_vk;
   DATE_STRUCT    bearb;
   TCHAR          pers_nam[9];
   double         pr_vk1;
   double         pr_vk2;
   double         pr_vk3;
   double         pr_ek_euro;
   double         pr_vk_euro;
};
extern struct A_PR a_pr, a_pr_null;

#line 8 "a_pr.rh"

class APR_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_PR a_pr;
               APR_CLASS () : DB_CLASS ()
               {
               }
               BOOL operator== (A_PR&);  
};
#endif
