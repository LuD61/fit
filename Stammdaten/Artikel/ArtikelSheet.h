#pragma once
#include "dbpropertysheet.h"

class CArtikelSheet :
	public CDbPropertySheet
{
	DECLARE_DYNAMIC(CArtikelSheet)
public:
	CArtikelSheet(void);
	~CArtikelSheet(void);
	virtual BOOL Write ();
    virtual void StepBack ();
	virtual BOOL Delete ();
//	virtual BOOL Print ();
};
