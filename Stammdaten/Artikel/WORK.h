#pragma once
#ifndef _WORK_DEF
#define _WORK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"
#include "a_bas.h"
#include "ag.h"

struct WGW {
   short          wg;
   TCHAR          wg_bz1[25];
   TCHAR          wg_bz2[25];
};
extern struct WGW wgw, wgw_null;
struct HWG {
   short          hwg;
   TCHAR          hwg_bz1[25];
   TCHAR          hwg_bz2[25];
};
extern struct HWG hwg, hwg_null;

class WORK :
	public DB_CLASS
{
public:
	WORK(void);
	virtual ~WORK(void);
	WGW wgw;
	HWG hwg;
	AG_CLASS Ag;

	long holeArtikel(double a);
	long holeAG(long ag);
};

#endif