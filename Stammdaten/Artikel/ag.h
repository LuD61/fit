#ifndef _AG_DEF
#define _AG_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct AG {
   short          abt;
   long           ag;
   TCHAR          ag_bz1[61];
   TCHAR          ag_bz2[61];
   short          a_typ;
   short          bearb_fil;
   short          bearb_lad;
   short          bearb_sk;
   TCHAR          best_auto[2];
   TCHAR          bsd_kz[2];
   short          delstatus;
   TCHAR          durch_fil[2];
   TCHAR          durch_sk[2];
   TCHAR          durch_sw[2];
   TCHAR          durch_vk[2];
   long           erl_kto;
   short          hbk;
   TCHAR          hbk_kz[2];
   short          hbk_ztr;
   TCHAR          hnd_gew[2];
   TCHAR          kost_kz[3];
   short          me_einh;
   short          mwst;
   TCHAR          pfa[2];
   TCHAR          pr_ueb[2];
   TCHAR          reg_eti[2];
   short          sais1;
   short          sais2;
   short          sg1;
   short          sg2;
   TCHAR          smt[2];
   double         sp_fil;
   TCHAR          sp_kz[2];
   double         sp_sk;
   double         sp_vk;
   TCHAR          stk_lst_kz[2];
   double         sw;
   double         tara;
   short          teil_smt;
   TCHAR          theke_eti[2];
   TCHAR          verk_art[2];
   DATE_STRUCT    verk_beg;
   DATE_STRUCT    verk_end;
   TCHAR          waren_eti[2];
   long           we_kto;
   short          wg;
   double         gn_pkt_gbr;
   TCHAR          a_krz_kz[2];
   TCHAR          tier_kz[3];
   long           erl_kto_1;
   long           erl_kto_2;
   long           erl_kto_3;
   long           we_kto_1;
   long           we_kto_2;
   long           we_kto_3;
};
extern struct AG ag, ag_null;

#line 8 "ag.rh"

class AG_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               AG ag;
               AG_CLASS () : DB_CLASS ()
               {
               }
};
#endif
