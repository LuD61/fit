#ifndef _AUTO_NR_DEF
#define _AUTO_NR_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct AUTO_NR {
   short          mdn;
   short          fil;
   TCHAR          nr_nam[11];
   long           nr_nr;
   short          satz_kng;
   long           max_wert;
   TCHAR          nr_char[11];
   long           nr_char_lng;
   TCHAR          fest_teil[11];
   TCHAR          nr_komb[21];
   short          delstatus;
};
extern struct AUTO_NR auto_nr, auto_nr_null;

#line 8 "auto_nr.rh"

class AUTO_NR_CLASS : public DB_CLASS 
{
       private :
               int cursor_0; 
               int cursor_1;
               int cursor_2;
               int cursor_del_nr; 
               int upd_cursor_2;
               int test_upd_cursor_2;
               int dsqlstatus;
               void prepare (void);
       public :
               AUTO_NR_CLASS () : DB_CLASS (), cursor_0 (-1), cursor_1 (-1), 
                                  cursor_2 (-1), cursor_del_nr (-1)
               {
               }
               int dbreadfirst (void);
               int dbreadfirst_0 (void);
               int dbread_0 (void);
               int dbreadfirst_1 (void);
               int dbread_1 (void);
               int dbreadfirst_2 (void);
               int dbread_2 (void);
               int dbdelete_nr_nam (void);
               int dbupdate_2 (void);
};
#endif
