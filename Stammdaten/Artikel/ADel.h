#pragma once
#include "afxwin.h"
#include "DelRec.h"


// CADel-Dialogfeld

class CADel : public CDialog
{
	DECLARE_DYNAMIC(CADel)

public:
	CADel(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CADel();

// Dialogfelddaten
	enum { IDD = IDD_ADEL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	CButton m_OK;
	CListBox m_List;
	CDelRec DelRec;
	static HANDLE ADel;
	static void CALLBACK Printf (LPSTR, va_list);
    static CADel *dlg;
    int (*a_del) (int anz, char **arg); 
    void (*SetPrintProc) (void (CALLBACK *p) (LPSTR, va_list));
//    void (*SetPrintProc) (LPSTR);

	BOOL Run (double a);
};
