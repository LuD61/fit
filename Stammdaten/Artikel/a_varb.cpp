#include "stdafx.h"
#include "a_varb.h"

struct A_VARB a_varb, a_varb_null, a_varb_def;

void A_VARB_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_varb.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_varb.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_varb.a_kz,SQLCHAR,3);
    sqlout ((double *) &a_varb.arb_zeit_fakt,SQLDOUBLE,0);
    sqlout ((double *) &a_varb.befe,SQLDOUBLE,0);
    sqlout ((double *) &a_varb.befe_i_fett,SQLDOUBLE,0);
    sqlout ((short *) &a_varb.delstatus,SQLSHORT,0);
    sqlout ((double *) &a_varb.eiw_ges,SQLDOUBLE,0);
    sqlout ((short *) &a_varb.fil,SQLSHORT,0);
    sqlout ((double *) &a_varb.fett,SQLDOUBLE,0);
    sqlout ((double *) &a_varb.fett_eiw,SQLDOUBLE,0);
    sqlout ((double *) &a_varb.fwp,SQLDOUBLE,0);
    sqlout ((double *) &a_varb.h2o,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_varb.leits,SQLCHAR,10);
    sqlout ((long *) &a_varb.mat,SQLLONG,0);
    sqlout ((short *) &a_varb.mdn,SQLSHORT,0);
    sqlout ((short *) &a_varb.tier,SQLSHORT,0);
    sqlout ((short *) &a_varb.vpk_typ,SQLSHORT,0);
    sqlout ((short *) &a_varb.quid_nr,SQLSHORT,0);
            cursor = sqlcursor (_T("select a_varb.a,  ")
_T("a_varb.a_kz,  a_varb.arb_zeit_fakt,  a_varb.befe,  a_varb.befe_i_fett,  ")
_T("a_varb.delstatus,  a_varb.eiw_ges,  a_varb.fil,  a_varb.fett,  ")
_T("a_varb.fett_eiw,  a_varb.fwp,  a_varb.h2o,  a_varb.leits,  a_varb.mat,  ")
_T("a_varb.mdn,  a_varb.tier,  a_varb.vpk_typ,  a_varb.quid_nr from a_varb ")

#line 12 "a_varb.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_varb.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_varb.a_kz,SQLCHAR,3);
    sqlin ((double *) &a_varb.arb_zeit_fakt,SQLDOUBLE,0);
    sqlin ((double *) &a_varb.befe,SQLDOUBLE,0);
    sqlin ((double *) &a_varb.befe_i_fett,SQLDOUBLE,0);
    sqlin ((short *) &a_varb.delstatus,SQLSHORT,0);
    sqlin ((double *) &a_varb.eiw_ges,SQLDOUBLE,0);
    sqlin ((short *) &a_varb.fil,SQLSHORT,0);
    sqlin ((double *) &a_varb.fett,SQLDOUBLE,0);
    sqlin ((double *) &a_varb.fett_eiw,SQLDOUBLE,0);
    sqlin ((double *) &a_varb.fwp,SQLDOUBLE,0);
    sqlin ((double *) &a_varb.h2o,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_varb.leits,SQLCHAR,10);
    sqlin ((long *) &a_varb.mat,SQLLONG,0);
    sqlin ((short *) &a_varb.mdn,SQLSHORT,0);
    sqlin ((short *) &a_varb.tier,SQLSHORT,0);
    sqlin ((short *) &a_varb.vpk_typ,SQLSHORT,0);
    sqlin ((short *) &a_varb.quid_nr,SQLSHORT,0);
            sqltext = _T("update a_varb set a_varb.a = ?,  ")
_T("a_varb.a_kz = ?,  a_varb.arb_zeit_fakt = ?,  a_varb.befe = ?,  ")
_T("a_varb.befe_i_fett = ?,  a_varb.delstatus = ?,  a_varb.eiw_ges = ?,  ")
_T("a_varb.fil = ?,  a_varb.fett = ?,  a_varb.fett_eiw = ?,  ")
_T("a_varb.fwp = ?,  a_varb.h2o = ?,  a_varb.leits = ?,  a_varb.mat = ?,  ")
_T("a_varb.mdn = ?,  a_varb.tier = ?,  a_varb.vpk_typ = ?,  ")
_T("a_varb.quid_nr = ? ")

#line 14 "a_varb.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_varb.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_varb.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_varb ")
                                  _T("where a = ? for update"));
            sqlin ((double *)   &a_varb.a,  SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_varb ")
                                  _T("set delstatus = 0 where a = ? and delstatus = 0 for update"));
            sqlin ((double *)   &a_varb.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_varb ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_varb.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_varb.a_kz,SQLCHAR,3);
    sqlin ((double *) &a_varb.arb_zeit_fakt,SQLDOUBLE,0);
    sqlin ((double *) &a_varb.befe,SQLDOUBLE,0);
    sqlin ((double *) &a_varb.befe_i_fett,SQLDOUBLE,0);
    sqlin ((short *) &a_varb.delstatus,SQLSHORT,0);
    sqlin ((double *) &a_varb.eiw_ges,SQLDOUBLE,0);
    sqlin ((short *) &a_varb.fil,SQLSHORT,0);
    sqlin ((double *) &a_varb.fett,SQLDOUBLE,0);
    sqlin ((double *) &a_varb.fett_eiw,SQLDOUBLE,0);
    sqlin ((double *) &a_varb.fwp,SQLDOUBLE,0);
    sqlin ((double *) &a_varb.h2o,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_varb.leits,SQLCHAR,10);
    sqlin ((long *) &a_varb.mat,SQLLONG,0);
    sqlin ((short *) &a_varb.mdn,SQLSHORT,0);
    sqlin ((short *) &a_varb.tier,SQLSHORT,0);
    sqlin ((short *) &a_varb.vpk_typ,SQLSHORT,0);
    sqlin ((short *) &a_varb.quid_nr,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into a_varb (")
_T("a,  a_kz,  arb_zeit_fakt,  befe,  befe_i_fett,  delstatus,  eiw_ges,  fil,  fett,  ")
_T("fett_eiw,  fwp,  h2o,  leits,  mat,  mdn,  tier,  vpk_typ,  quid_nr) ")

#line 28 "a_varb.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?)"));

#line 30 "a_varb.rpp"
}
