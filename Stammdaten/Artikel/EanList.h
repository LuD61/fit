#ifndef _EAN_LIST_DEF
#define _EAN_LIST_DEF
#pragma once

class CEanList
{
public:
	double ean;
	CString ean_bz;
	double a;
	CString a_bz1;
	CEanList(void);
	CEanList(double, LPTSTR, double, LPTSTR);
	~CEanList(void);
};
#endif
