#include "stdafx.h"
#include <share.h>
#include "propertieTable.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

namespace PropExtension
{

CProperties::CProperties(void)
{
	FileName = "";
	SectionName = "";
	AllTabMode = 0 ;	// 160109
}

CProperties::~CProperties(void)
{
	FirstPosition ();
	CPropertyItem *p;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		delete p;
	}
}

void CProperties::SetAllTabMode(int mode)	// 160109
{
	AllTabMode = mode ;
}


CString CProperties::GetValue (CString Name)
{
	FirstPosition ();
	CPropertyItem *p;
	if ( AllTabMode == 1 )	// 160109
	{
		while ((p = (CPropertyItem *) GetNext ()) != NULL)
		{
			if (p->Value.CompareNoCase (Name) == 0)
			{
				return p->Name;
			}
		}
	}
	else
	{
		while ((p = (CPropertyItem *) GetNext ()) != NULL)
		{
			if (p->Name.CompareNoCase (Name) == 0)
			{
				return p->Value;
			}
		}
	}
	return Name;
}

CString CProperties::GetValues (CString Name)
{
	CPropertyItem *p;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{

		if ( AllTabMode == 1 )
		{
			CString pValue = p->Value.Left (Name.GetLength ()); 
			if (pValue.CompareNoCase (Name) == 0)
			{
				return p->Name;
			}
		}
		else
		{
			CString pName = p->Name.Left (Name.GetLength ()); 
			if (pName.CompareNoCase (Name) == 0)
			{
				return p->Value;
			}
		}

	}
	return "";
}

CString CProperties::GetName (CString Value)
{
	// 160109 : AllTabMode nicht realisiert
	FirstPosition ();
	CPropertyItem *p;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Value.CompareNoCase (Value) == 0)
		{
			return p->Name;
		}
	}
	return Value;
}

int CProperties::Find (CString Name)
{
	FirstPosition ();
	CPropertyItem *p;
	int i = 0;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if ( AllTabMode == 1 )
		{
			if (p->Value.CompareNoCase (Name) == 0)
			{
				return i;
			}
			i ++;
		}
		else
		{
			if (p->Name.CompareNoCase (Name) == 0)
			{
				return i;
			}
			i ++;
		}
	}
	return -1;
}

int CProperties::FindValue (CString Value)
{
// 160109 : AllTabMode NICHT realisiert
	FirstPosition ();
	CPropertyItem *p;
	int i = 0;
	while ((p = (CPropertyItem *) GetNext ()) != NULL)
	{
		if (p->Value.CompareNoCase (Value) == 0)
		{
			return i;
		}
		i ++;
	}
	return -1;
}

void CProperties::Set (CString Name, CString Value, int idx)
{
// 160109 : AllTabMode NICHT realisiert
	CPropertyItem *p;
	p = (CPropertyItem *) Get (idx);
	if (p != NULL)
	{
		p->Name = Name;
		p->Value = Value;
	}
}

void CProperties::Set (CString& Record)
{
// 160109 : AllTabMode NICHT realisiert
	CToken t;
	t.SetSep ("=");
	t = Record;
	if (t.GetAnzToken () < 1) return;

	CString Name = t.GetToken (0);
    CString Value = "";
	if (t.GetAnzToken () > 1)
	{
		Value = t.GetToken (1);
	}
	Name.Trim ();
	Value.Trim ();

	CPropertyItem *p = new CPropertyItem (Name, Value);
    Add (p); 
}

void CProperties::SetWithSep (CString& Record, LPTSTR Sep)
{
// 160109 : AllTabMode NICHT realisiert
	CToken t;
	t.SetSep (Sep);
	t = Record;
	if (t.GetAnzToken () < 1) return;

	CString Name = t.GetToken (0);
    CString Value = "";
	if (t.GetAnzToken () > 1)
	{
		Value = t.GetToken (1);
	}
	Name.Trim ();
	Value.Trim ();

	CPropertyItem *p = new CPropertyItem (Name, Value);
    Add (p); 
}

void CProperties::SetWithSep (CString& Record, LPTSTR Sep1, LPTSTR Sep2)
{
// 160109 : AllTabMode NICHT realisiert
	CToken t;
	t.SetSep (Sep1);
	t = Record;
	for (int i = 0; i < t.GetAnzToken (); i ++)
	{
		CString Record2 = t.GetToken (i);
		SetWithSep (Record2, Sep2);
	}
}

BOOL CProperties::FindSection (FILE *fp)
{
	char buffer [512];

	if ( AllTabMode == 1 )	// 160109 : ohne Section-eigentlich redundant
		return TRUE ;

	while (fgets (buffer, 511, fp))
	{
		CString Record = buffer;
		Record.Trim ();
		if (Record.Left (1) != "[") continue;
		int len = Record.Find ("]");
		if (len < 0) continue;
		CString Name = Record.Mid (1, len - 1);
		if (Name.Trim () == SectionName) 
		{
			return TRUE;
		}
	}
    return FALSE;
}

void CProperties::Load ()
{
	char buffer [512];
	if (FileName == "") return;

	FILE *fp = _fsopen (FileName.GetBuffer (), "r", _SH_DENYNO);
	if (fp == NULL) return;


	BOOL TestSection = FALSE;
	if (SectionName != "")
	{
		TestSection = TRUE;
		AllTabMode = 0 ;	// 160109
		if (!FindSection (fp)) return;
	}
	else
		AllTabMode = 1 ;	// 160109

	DestroyAll ();
	while (fgets (buffer, 511, fp))
	{
		CString Record = buffer;
		Record.Trim ();
		if (TestSection && (Record.Left (1) == "["))
		{
			break;
		}

		Set (Record);
	}

	fclose (fp);
}
}