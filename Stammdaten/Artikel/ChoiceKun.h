#ifndef _CHOICEKUN_DEF
#define _CHOICEKUN_DEF

#include "ChoiceX.h"
#include "KunList.h"
#include <vector>

class CChoiceKun : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
      
    public :
		long m_Mdn;
	    std::vector<CKunList *> KunList;
      	CChoiceKun(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceKun(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchKun (CListCtrl *,  LPTSTR);
        void SearchAdrKrz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CKunList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
