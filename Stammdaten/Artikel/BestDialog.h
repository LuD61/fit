#pragma once
#include "NumEdit.h"
#include "TextEdit.h"
#include "CtrlGrid.h"
#include "DbUniCode.h"
#include "FormTab.h"
#include "StaticButton.h"
#include "Mdn.h"
#include "Fil.h"
#include "Adr.h"
#include "A_bas.h"
#include "A_best.h"
#include "ChoiceMdn.h"
#include "ChoiceFil.h"

#define IDC_BESTSAVE 3010
#define IDC_BESTDELETE 3011

#ifndef IDC_MDNCHOICE
#define IDC_MDNCHOICE 3001
#endif

#ifndef IDC_FILCHOICE
#define IDC_FILCHOICE 3002
#endif

// CBestDialog-Dialogfeld

class CBestDialog : public CDialog
{
	DECLARE_DYNAMIC(CBestDialog)

public:
	CBestDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CBestDialog();

// Dialogfelddaten
	enum { IDD = IDD_BEST_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
public:
    MDN_CLASS Mdn;
    FIL_CLASS Fil;
    ADR_CLASS MdnAdr;
    ADR_CLASS FilAdr;
    A_BEST_CLASS A_best;
    A_BAS_CLASS *A_bas;
	CChoiceMdn *ChoiceMdn;
	CChoiceFil *ChoiceFil;
 
	CStaticButton m_Save;
	CStaticButton m_Delete;
	CStatic m_BestGroup;
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CButton m_MdnChoice;
	CEdit m_MdnName;
	CStatic m_LFil;
	CNumEdit m_Fil;
	CButton m_FilChoice;
	CEdit m_FilName;
	CStatic m_LBsdMin;
	CNumEdit m_BsdMin;
	CStatic m_LBsdMinEmp;
	CNumEdit m_BsdMinEmp;
	CStatic m_LBsdMaxEmp;
	CNumEdit m_BsdMaxEmp;
    CButton  m_BestAuto;
	CStatic m_LLief;
	CTextEdit m_Lief;
	CStatic m_LLiefBest;
	CTextEdit m_LiefBest;
	CStatic m_LLetztLiefNr;
	CTextEdit m_LetztLiefNr;

	CFormTab Form;
	CFormTab Keys;
	CFont Font;
	CCtrlGrid CtrlGrid;
	CCtrlGrid KeyGrid;
	CCtrlGrid MdnGrid;
	CCtrlGrid FilGrid;
	CCtrlGrid ToolGrid;
	virtual BOOL OnReturn ();
	virtual void OnSave ();
	virtual void Delete ();
	BOOL ReadMdn ();
	BOOL ReadFil ();
	BOOL Read ();
    void OnMdnchoice(); 
    void OnFilchoice(); 
};
