#pragma once
#include "afxwin.h"
#include "artikel.h"
#include "sys_peri.h"
#include "fil.h"
#include "adr.h"
#include "FormTab.h"
#include "numedit.h"
#include "DllBild160.h"

// CSendToSystem-Dialogfeld

class CSendToSystem : public CDialog
{
	DECLARE_DYNAMIC(CSendToSystem)

public:
	CSendToSystem(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CSendToSystem();

// Dialogfelddaten
	enum { IDD = IDD_SEND_TO_SYS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();

	DECLARE_MESSAGE_MAP()
public:
	CDllBild160 Bild160;
	CFormTab Form;
	CNumEdit m_Sys;
	SYS_PERI_CLASS Sys_peri;
	FIL_CLASS Fil;
	ADR_CLASS Adr;
	afx_msg void OnBnClickedSyschoice();
	CStatic m_AdrKrz;
	afx_msg void OnBnClickedSend();
	afx_msg void OnOK ();
	BOOL ReadSys ();
	void SendSys (long sys);
};
