// PrArtPreise.cpp : Implementierungsdatei
//

#include "stdafx.h"
// #include "IPrDialog.h"
//#include "IprMan.h"
#include "PrKunPage.h"
#include "UniFormField.h"
#include "StrFuncs.h"
#include "Util.h"
#include "Bmap.h"
#include "Decimal.h"
#include "Process.h"
#include "AddEk.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'

// CPrKunPage Dialogfeld

CPrKunPage::CPrKunPage(CWnd* pParent /*=NULL*/)
	: CDbPropertyPage()
{
	Choice = NULL;
	ModalChoice = TRUE;
	CloseChoice = FALSE;
	ChoiceMdn = NULL;
	ChoicePrGrStuf = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
    ModalChoicePrGrStuf = TRUE;
	IprCursor = -1;
	IprDelCursor = -1;
	IprDelCursor2 = -1;
	UpdateIvCursor = -1;
//	IprGrStufkCursor = -1;
	IKunPrkCursor = -1;
	HeadControls.Add (&m_Mdn);
	HeadControls.Add (&m_KunPr);
	PosControls.Add (&m_KunZusBz);
	PosControls.Add (&m_PrGrStuf);
	PosControls.Add (&m_AddEk);
	PosControls.Add (&m_KunPrList);

	ButtonControls.Add (&m_Cancel);
	ButtonControls.Add (&m_Save);
	ButtonControls.Add (&m_Delete);
	ButtonControls.Add (&m_Insert);

    HideButtons = FALSE;

	Frame = NULL;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Search = _T("");
	Separator = _T(";");
	CellHeight = 0;
	Cfg.SetProgName( _T("IPrDialog"));
}

CPrKunPage::CPrKunPage(UINT IDD)
	: CDbPropertyPage(IDD)
{
	Choice = NULL;
	ModalChoice = FALSE;
	ChoiceMdn = NULL;
	ModalChoiceMdn = TRUE;
	ModalChoiceKun = TRUE;
	ChoicePrGrStuf = NULL;
    ModalChoicePrGrStuf = TRUE;
	Frame = NULL;
	IprCursor = -1;
	IprDelCursor = -1;
	IprDelCursor2 = -1;
	UpdateIvCursor = -1;
	RemoveKun = TRUE;
	DbRows.Init ();
	ListRows.Init ();
	Separator = _T(";");
	CellHeight = 0;
}

CPrKunPage::~CPrKunPage()
{

	Font.DeleteObject ();
	A_bas.dbclose ();
	Mdn.dbclose ();
	MdnAdr.dbclose ();
	Ipr.dbclose ();
	Iprgrstufk.dbclose ();
	Kun.dbclose ();
	KunAdr.dbclose ();
	if (IprCursor == -1)
	{
		Ipr.sqlclose (IprCursor);
	}
	if (IprDelCursor == -1)
	{
		Ipr.sqlclose (IprDelCursor);
	}
	if (IprDelCursor2 == -1)
	{
		Ipr.sqlclose (IprDelCursor2);
	}
	if (UpdateIvCursor == -1)
	{
		Ipr.sqlclose (UpdateIvCursor);
	}
	if (IKunPrkCursor == -1)
	{
		A_bas.sqlclose (IKunPrkCursor);
	}
	if (Choice != NULL)
	{
		delete Choice;
		Choice = NULL;
	}
	if (ChoiceMdn != NULL)
	{
		delete ChoiceMdn;
		ChoiceMdn = NULL;
	}
	if (ChoicePrGrStuf != NULL)
	{
		delete ChoicePrGrStuf;
		ChoicePrGrStuf = NULL;
	}
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	DestroyRows (m_KunPrList.ListRows);

}

void CPrKunPage::DoDataExchange(CDataExchange* pDX)
{
	CDbPropertyPage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LKUN_PR, m_LKunPr);
	DDX_Control(pDX, IDC_KUN_PR, m_KunPr);
	DDX_Control(pDX, IDC_LKUN_ZUS_BZ, m_LKunZusBz);
	DDX_Control(pDX, IDC_KUN_ZUS_BZ, m_KunZusBz);

	DDX_Control(pDX, IDC_LPR_GR_STUF, m_LPrGrStuf);
	DDX_Control(pDX, IDC_PR_GR_STUF, m_PrGrStuf);
	DDX_Control(pDX, IDC_LZUS_BZ, m_LZusBz);
	DDX_Control(pDX, IDC_ZUS_BZ, m_ZusBz);
	DDX_Control(pDX, IDC_ADDEK, m_AddEk);
	DDX_Control(pDX, IDC_KUNPR_LIST, m_KunPrList);
	DDX_Control(pDX, IDC_CANCEL, m_Cancel);
	DDX_Control(pDX, IDC_SAVE, m_Save);
	DDX_Control(pDX, IDC_DELETE, m_Delete);
	DDX_Control(pDX, IDC_INSERT, m_Insert);

}

BEGIN_MESSAGE_MAP(CPrKunPage, CPropertyPage)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_SIZE ()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_KUNPRCHOICE ,  OnKunPrchoice)
	ON_BN_CLICKED(IDC_PRGRCHOICE ,  OnPrGrchoice)
	ON_BN_CLICKED(IDC_CANCEL ,   OnCancel)
	ON_BN_CLICKED(IDC_SAVE ,     OnSave)
	ON_BN_CLICKED(IDC_DELETE ,   OnDelete)
	ON_BN_CLICKED(IDC_INSERT ,   OnInsert)
	ON_BN_CLICKED(IDC_ADDEK ,   OnAddEk)
	ON_COMMAND (SELECTED, OnKunPrSelected)
	ON_COMMAND (CANCELED, OnKunPrCanceled)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
	ON_NOTIFY (HDN_ENDTRACK, 0, OnListEndTrack)
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnchoice)
END_MESSAGE_MAP()


// CPrKunPage Meldungshandler

BOOL CPrKunPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
//	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
//	ASSERT(IDM_ABOUTBOX < 0xF000);

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist

	// TODO: Hier zus�tzliche Initialisierung einf�gen

	A_bas.opendbase (_T("bws"));

	CUtil::GetPersName (PersName);
	PgrProt.Construct (PersName, CString ("11121"), &Ipr);

	GrundPreis.Create (IDD_GRUND_PREIS, this);

	ReadCfg ();

	if (HideButtons)
	{
		ButtonControls.SetVisible (FALSE);
		RightListSpace = 15;
	}
	else
	{
		ButtonControls.SetVisible (TRUE);
		RightListSpace = 125;
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
		lFont.CreatePointFont (85, _T("Courier"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
		lFont.CreatePointFont (95, _T("Courier New"));
	}

	m_Cancel.SetWindowText (_T("Beenden"));
    HBITMAP HbF5 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F5T", "F5MASKT");
	m_Cancel.SetBitmap (HbF5);

    HBITMAP HbF12 = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "F12T", "F12MASKT");
	m_Save.SetBitmap (HbF12);

    HBITMAP HbDel = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "DELT", "DELMASKT");
	m_Delete.SetBitmap (HbDel);

    HBITMAP HbInsert = BMAP::LoadBitmap (AfxGetApp()->m_hInstance, "INSERTT", "INSERTMASKT");
	m_Insert.SetBitmap (HbInsert);

    Form.Add (new CFormField (&m_Mdn,EDIT,         (short *) &Mdn.mdn.mdn, VSHORT));
    Form.Add (new CUniFormField (&m_MdnName,EDIT,  (char *) MdnAdr.adr.adr_krz, VCHAR));
    Form.Add (new CFormField (&m_KunPr,EDIT,       (long *) &I_kun_prk.i_kun_prk.kun_pr, VLONG));
    Form.Add (new CUniFormField (&m_KunZusBz,EDIT, (char *) I_kun_prk.i_kun_prk.zus_bz, VCHAR));
    Form.Add (new CFormField (&m_PrGrStuf,EDIT,   (long *) &Iprgrstufk.iprgrstufk.pr_gr_stuf, VLONG));
    Form.Add (new CUniFormField (&m_ZusBz,EDIT,   (char *) Iprgrstufk.iprgrstufk.zus_bz, VCHAR));
    
	Ipr.sqlin ((short *)  &Ipr.ipr.mdn, SQLSHORT, 0);
	Ipr.sqlin ((long *)  &Ipr.ipr.kun_pr, SQLLONG, 0);
	Ipr.sqlout ((long *)   &Ipr.ipr.pr_gr_stuf, SQLLONG, 0);
	Ipr.sqlout ((double *) &Ipr.ipr.a,  SQLDOUBLE, 0);
	IprCursor = Ipr.sqlcursor (_T("select pr_gr_stuf, a from ipr ")
		                       _T("where mdn = ? ")
		                       _T("and kun_pr = ? ")
		                       _T("and kun = 0 ")
		                       _T("order by a"));
	Ipr.sqlin ((short *)  &Ipr.ipr.mdn, SQLSHORT, 0);
	Ipr.sqlin ((long *)  &Ipr.ipr.kun_pr, SQLLONG, 0);
	IprDelCursor = Ipr.sqlcursor (_T("delete from ipr ")
								  _T("where mdn = ? ")
								  _T("and kun_pr = ? ")
		                          _T("and kun = 0"));
	Ipr.sqlin ((short *)  &Ipr.ipr.mdn, SQLSHORT, 0);
	Ipr.sqlin ((long *)  &Ipr.ipr.kun_pr, SQLLONG, 0);
	Ipr.sqlin ((long *)  &Ipr.ipr.pr_gr_stuf, SQLLONG, 0);
	IprDelCursor2 = Ipr.sqlcursor (_T("delete from ipr ")
								  _T("where mdn = ? ")
								  _T("and kun_pr = ? ")
								  _T("and pr_gr_stuf != ? ")
		                          _T("and kun = 0"));
	I_kun_prk.sqlin ((short *) &I_kun_prk.i_kun_prk.pr_gr_stuf, SQLLONG, 0);
	I_kun_prk.sqlin ((short *) &I_kun_prk.i_kun_prk.mdn, SQLSHORT, 0);
	I_kun_prk.sqlin ((short *) &I_kun_prk.i_kun_prk.kun_pr, SQLLONG, 0);
	UpdateIvCursor = I_kun_prk.sqlcursor ("update iv_pr set pr_gr_stuf = ? "
		                                  "where mdn = ? "
										  "and kun_pr = ?");
	if (CellHeight > 0)
	{
		CBitmap bmp;
		bmp.CreateBitmap (1,CellHeight, 1, 0, NULL);
		BITMAP bm;
		int ret = bmp.GetBitmap (&bm);
		if (ret != 0)
		{
			image.Create (bm.bmWidth, bm.bmHeight, ILC_COLOR8, 0, 4);
			image.Add (&bmp, RGB (0,0,0));
		}
		m_KunPrList.SetImageList (&image, LVSIL_SMALL);   
	}

	FillList = m_KunPrList;
	FillList.SetStyle (LVS_REPORT);
	if (m_KunPrList.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Artikel"),     m_KunPrList.PosA, 130, LVCFMT_RIGHT);
	FillList.SetCol (_T("Bezeichnung"), m_KunPrList.PosABz1, 250, LVCFMT_LEFT);
	FillList.SetCol (_T("VK-Preis"),    m_KunPrList.PosVkPr, 80, LVCFMT_RIGHT);
	FillList.SetCol (_T("Laden-Preis"), m_KunPrList.PosLdPr, 100, LVCFMT_RIGHT);
	if (m_KunPrList.Aufschlag == m_KunPrList.LIST ||
		m_KunPrList.Aufschlag == m_KunPrList.ALL)
	{
		FillList.SetCol (_T("Aufschlag"),  m_KunPrList.PosEkAbs, 100, LVCFMT_RIGHT);
		FillList.SetCol (_T("Aufschlag %"),m_KunPrList.PosEkProz, 100, LVCFMT_RIGHT);
	}

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this);
    CtrlGrid.SetGridSpace (5, 8);

	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

	KunPrGrid.Create (this, 1, 2);
    KunPrGrid.SetBorder (0, 0);
    KunPrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_KunPr = new CCtrlInfo (&m_KunPr, 0, 0, 1, 1);
	KunPrGrid.Add (c_KunPr);
	CtrlGrid.CreateChoiceButton (m_KunPrChoice, IDC_KUNPRCHOICE, this);
	CCtrlInfo *c_KunPrChoice = new CCtrlInfo (&m_KunPrChoice, 1, 0, 1, 1);
	KunPrGrid.Add (c_KunPrChoice);

	PrGrGrid.Create (this, 1, 2);
    PrGrGrid.SetBorder (0, 0);
    PrGrGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_PrGrStuf = new CCtrlInfo (&m_PrGrStuf, 0, 0, 1, 1);
	PrGrGrid.Add (c_PrGrStuf);
	CtrlGrid.CreateChoiceButton (m_PrGrChoice, IDC_PRGRCHOICE, this);
	CCtrlInfo *c_PrGrChoice = new CCtrlInfo (&m_PrGrChoice, 1, 0, 1, 1);
	PrGrGrid.Add (c_PrGrChoice);

	ButtonGrid.Create (this, 5, 5);
    ButtonGrid.SetBorder (0, 0);
    ButtonGrid.SetCellHeight (20);
    ButtonGrid.SetGridSpace (0, 10);
	CCtrlInfo *c_Cancel = new CCtrlInfo (&m_Cancel, 0, 0, 1, 1);
	ButtonGrid.Add (c_Cancel);
	CCtrlInfo *c_Save = new CCtrlInfo (&m_Save, 0, 1, 1, 1);
	ButtonGrid.Add (c_Save);
	CCtrlInfo *c_Delete = new CCtrlInfo (&m_Delete, 0, 2, 1, 1);
	ButtonGrid.Add (c_Delete);
	CCtrlInfo *c_Insert = new CCtrlInfo (&m_Insert, 0, 3, 1, 1);
	ButtonGrid.Add (c_Insert);

	CCtrlInfo *c_LMdn     = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid   = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1); 
	CtrlGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName     = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1); 
	CtrlGrid.Add (c_MdnName);

	CCtrlInfo *c_GrundPreis = new CCtrlInfo (&GrundPreis, DOCKRIGHT, 1, 6, 1); 
	CtrlGrid.Add (c_GrundPreis);

	CCtrlInfo *c_LKunPr      = new CCtrlInfo (&m_LKunPr, 0, 1, 1, 1); 
	CtrlGrid.Add (c_LKunPr);
	CCtrlInfo *c_KunPrGrid   = new CCtrlInfo (&KunPrGrid, 1, 1, 1, 1); 
	CtrlGrid.Add (c_KunPrGrid);
	CCtrlInfo *c_LKunZusBz  = new CCtrlInfo (&m_LKunZusBz, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LKunZusBz);
	CCtrlInfo *c_KunZusBz  = new CCtrlInfo (&m_KunZusBz, 1, 2, 3, 1); 
	CtrlGrid.Add (c_KunZusBz);

	CCtrlInfo *c_LPrGrStuf     = new CCtrlInfo (&m_LPrGrStuf, 0, 3, 1, 1); 
	CtrlGrid.Add (c_LPrGrStuf);
	CCtrlInfo *c_PrGrGrid   = new CCtrlInfo (&PrGrGrid, 1, 3, 1, 1); 
	CtrlGrid.Add (c_PrGrGrid);
	CCtrlInfo *c_LZusBz  = new CCtrlInfo (&m_LZusBz, 0, 4, 1, 1); 
	CtrlGrid.Add (c_LZusBz);
	CCtrlInfo *c_ZusBz  = new CCtrlInfo (&m_ZusBz, 1, 4, 3, 1); 
	CtrlGrid.Add (c_ZusBz);


	CCtrlInfo *c_ButtonGrid = new CCtrlInfo (&ButtonGrid, DOCKRIGHT, 5, 1, 3); 
	CtrlGrid.Add (c_ButtonGrid);
	CCtrlInfo *c_AddEk  = new CCtrlInfo (&m_AddEk, 0, 5, 1, 1); 
	CtrlGrid.Add (c_AddEk);
	CCtrlInfo *c_KunPrList  = new CCtrlInfo (&m_KunPrList, 0, 6, DOCKRIGHT, DOCKBOTTOM); 
	c_KunPrList->rightspace = RightListSpace;
	CtrlGrid.Add (c_KunPrList);

	m_KunPrList.AddListChangeHandler (this);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	if (m_KunPrList.Aufschlag != m_KunPrList.BUTTON
		&& m_KunPrList.Aufschlag != m_KunPrList.ALL)
	{
		m_AddEk.ShowWindow (SW_HIDE);
	}

	CtrlGrid.Display ();
	memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk, sizeof (IPRGRSTUFK));
	memcpy (&I_kun_prk.i_kun_prk, &i_kun_prk, sizeof (I_KUN_PRK));
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Mdn.mdn.mdn = 1;
	if (PersName.Trim () != "")
	{
		_tcscpy (Sys_ben.sys_ben.pers_nam, PersName.GetBuffer ());
		if (Sys_ben.dbreadfirstpers_nam () == 0)
		{
			if (Sys_ben.sys_ben.mdn != 0)
			{
				Mdn.mdn.mdn = Sys_ben.sys_ben.mdn;
				m_Mdn.SetReadOnly ();
				m_Mdn.ModifyStyle (WS_TABSTOP, 0);
			}
		}
	}
	Form.Show ();
	ReadMdn ();
	EnableHeadControls (TRUE);
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}


void CPrKunPage::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CPrKunPage::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CPrKunPage::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CPrKunPage::OnSize (UINT nType, int cx, int cy)
{
		CRect rect (0, 0, cx, cy);
		CtrlGrid.pcx = 0;
		CtrlGrid.pcy = 0;
		CtrlGrid.DlgSize = &rect;
		CtrlGrid.Move (0, 0);
		CtrlGrid.DlgSize = NULL;
}

BOOL CPrKunPage::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
				m_KunPrList.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_KunPrList &&
					GetFocus ()->GetParent () != &m_KunPrList )
				{

					break;
			    }
				m_KunPrList.OnKeyD (VK_TAB);
				return TRUE;
			}
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnReturn ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				if (OnKeyup ())
				{
					return TRUE;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				StepBack ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F7)
			{
				OnDelete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F11)
			{
				if (GetKeyState (VK_CONTROL) < 0)
				{
					ShowGrundPreis ();
				}
				else
				{
					OnAddEk ();
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Choice != NULL)
				{
					if (Choice->IsWindowVisible ())
					{
						Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
				else
				{
					OnKunPrchoice ();
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				if (GetFocus () == &m_Mdn)
				{
					OnMdnchoice ();
					return TRUE;
				}
				if (GetFocus () == &m_KunPr)
				{
					OnKunPrchoice ();
					return TRUE;
				}
				m_KunPrList.OnKey9 ();
				return TRUE;
			}
	}
    return CDbPropertyPage::PreTranslateMessage(pMsg);
}

BOOL CPrKunPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_KunPr)
	{
		if (!Read ())
		{
			m_KunPr.SetFocus ();
			return FALSE;
		}
	}

	if (Control == &m_PrGrStuf)
	{
		if (!ReadPrGrStuf ())
		{
			return FALSE;
		}
	}

	if (Control != &m_KunPrList &&
		Control->GetParent ()!= &m_KunPrList )
	{
			Control = GetNextDlgTabItem (Control, FALSE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}

	return FALSE;
}

BOOL CPrKunPage::OnKeyup ()
{
	CWnd *Control = GetFocus ();
	if (Control != &m_KunPrList &&
		Control->GetParent ()!= &m_KunPrList )
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
/*
	else if (m_KunPrList.EditRow == 0)
	{
			Control = GetNextDlgTabItem (Control, TRUE);
			if (Control != NULL)
			{
				Control->SetFocus ();
			}
			return TRUE;
	}
*/
	return FALSE;
}


BOOL CPrKunPage::ReadMdn ()
{
	int mdn;
	int dsqlstatus = 0;

	mdn = Mdn.mdn.mdn;
	memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Choice != NULL && mdn != Mdn.mdn.mdn)
	{
		delete Choice;
		Choice = NULL;
	}
	dsqlstatus = Mdn.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		MdnAdr.adr.adr = Mdn.mdn.adr;
		MdnAdr.dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		FillPrGrStufCombo ();
		FillKunPrCombo ();
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden (sqlstatus %d) "),Mdn.mdn.mdn, dsqlstatus);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CPrKunPage::read ()
{
	if (m_KunPr.IsWindowEnabled ())
	{
		return FALSE;
	}
	return Read ();

}

BOOL CPrKunPage::Read ()
{
	if (ModalChoice)
	{
		CString cPrGrStuf;
		m_KunPr.GetWindowText (cPrGrStuf);
		if (!CStrFuncs::IsDecimal (cPrGrStuf))
		{
			Search = cPrGrStuf;
			OnKunPrchoice ();
			Search = "";
			if (!ChoiceStat)
			{
				m_KunPr.SetFocus ();
				m_KunPr.SetSel (0, -1);
				return FALSE;
			}
		}
	}

	memcpy (&I_kun_prk.i_kun_prk, &i_kun_prk_null, sizeof (I_KUN_PRK));
	Form.Get ();
	strcpy (I_kun_prk.i_kun_prk.zus_bz, ""); 
	strcpy (Iprgrstufk.iprgrstufk.zus_bz, ""); 
	if (I_kun_prk.i_kun_prk.kun_pr == 0l)
	{
		MessageBox (_T("Die Kundenpreislliste darf nicht 0 sein"),
			        NULL,
					MB_OK | MB_ICONERROR);
		m_KunPr.SetFocus ();
		m_KunPr.SetSel (0, -1);
		return FALSE;
	}
	I_kun_prk.i_kun_prk.mdn = Mdn.mdn.mdn;
	I_kun_prk.dbreadfirst ();
	Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
	Iprgrstufk.iprgrstufk.pr_gr_stuf= I_kun_prk.i_kun_prk.pr_gr_stuf;
	Iprgrstufk.dbreadfirst ();
	ReadList ();
    EnableHeadControls (FALSE);
	Form.Show ();
	return TRUE;
}

BOOL CPrKunPage::ReadPrGrStuf ()
{
	if (ModalChoicePrGrStuf)
	{
		CString cPrGrStuf;
		m_PrGrStuf.GetWindowText (cPrGrStuf);
		if (!CStrFuncs::IsDecimal (cPrGrStuf))
		{
			Search = cPrGrStuf;
			OnPrGrchoice ();
			Search = "";
			if (!ChoiceStat)
			{
				m_PrGrStuf.SetFocus ();
				m_PrGrStuf.SetSel (0, -1);
				return FALSE;
			}
		}
	}

	memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
	Form.Get ();
	Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
	if (Iprgrstufk.dbreadfirst () == 0)
	{
		if (ReadList () == FALSE) 
		{
			return FALSE;
		}
	    EnableHeadControls (FALSE);
		Form.Show ();
		m_PrGrStuf.SetFocus ();
		m_PrGrStuf.SetSel (0, -1);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Preisgruppenstufe %ld nicht gefunden"),
			Iprgrstufk.iprgrstufk.pr_gr_stuf);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);

		memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
		Form.Show ();
		m_PrGrStuf.SetFocus ();
		m_PrGrStuf.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}


BOOL CPrKunPage::ReadList ()
{
	if (I_kun_prk.i_kun_prk.kun_pr == 0l)
	{
		MessageBox (_T("Die Kundenpreislliste darf nicht 0 sein"),
			        NULL,
					MB_OK | MB_ICONERROR);
		m_KunPr.SetFocus ();
		m_KunPr.SetSel (0, -1);
		return FALSE;
	}
	m_KunPrList.DeleteAllItems ();
	m_KunPrList.vSelect.clear ();
	DestroyRows (DbRows);
	DestroyRows (ListRows);
	DestroyRows (m_KunPrList.ListRows);
	int i = 0;
	memcpy (&Ipr.ipr, &ipr_null, sizeof (IPR));
	Ipr.ipr.mdn = Mdn.mdn.mdn;
	Ipr.ipr.kun_pr   = I_kun_prk.i_kun_prk.kun_pr;
	Ipr.ipr.pr_gr_stuf   = I_kun_prk.i_kun_prk.pr_gr_stuf;
	m_KunPrList.mdn = Mdn.mdn.mdn;
	m_KunPrList.pr_gr_stuf = Iprgrstufk.iprgrstufk.pr_gr_stuf; 
	m_KunPrList.kun_pr = I_kun_prk.i_kun_prk.kun_pr; 
	m_KunPrList.kun = 0l; 
	Ipr.sqlopen (IprCursor);
	while (Ipr.sqlfetch (IprCursor) == 0)
	{
		Ipr.dbreadfirst ();
		memcpy (&A_bas.a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas.a_bas.a = Ipr.ipr.a;
		A_bas.dbreadfirst ();
		Ipr.ipr.a_grund = A_bas.a_bas.a_grund;	// 301111

		FillList.InsertItem (i, 0);
		CString A;
		A.Format (_T("%.0lf"), Ipr.ipr.a);
		FillList.SetItemText (A.GetBuffer (), i, m_KunPrList.PosA);
		CString ABz1;
		ABz1 = A_bas.a_bas.a_bz1;
		ABz1 += _T("   ");
		ABz1 += A_bas.a_bas.a_bz2;
		FillList.SetItemText (ABz1.GetBuffer (), i, m_KunPrList.PosABz1);
		CString VkPr;
		m_KunPrList.DoubleToString (Ipr.ipr.vk_pr_eu, VkPr, 4);
//		CDecimal *decVK = new CDecimal (VkPr);
//		delete decVK;
		Ipr.ipr.vk_pr_eu = CStrFuncs::StrToDouble (VkPr);
		FillList.SetItemText (VkPr.GetBuffer (), i, m_KunPrList.PosVkPr);
		CString LdPr;
		m_KunPrList.DoubleToString (Ipr.ipr.ld_pr_eu, LdPr, 2);
		FillList.SetItemText (LdPr.GetBuffer (), i, m_KunPrList.PosLdPr);

		CString EkAbs;
		m_KunPrList.DoubleToString (Ipr.ipr.add_ek_abs, EkAbs, 2);
		FillList.SetItemText (EkAbs.GetBuffer (), i, m_KunPrList.PosEkAbs);
		CString EkProz;
		m_KunPrList.DoubleToString (Ipr.ipr.add_ek_proz, EkProz, 2);
		FillList.SetItemText (EkProz.GetBuffer (), i, m_KunPrList.PosEkProz);

		long pr_gr_stuf = Ipr.ipr.pr_gr_stuf;
		Ipr.ipr.pr_gr_stuf   = Iprgrstufk.iprgrstufk.pr_gr_stuf;
		CPreise *ipr = new CPreise (VkPr, LdPr, Ipr.ipr);
		DbRows.Add (ipr);
		ipr = new CPreise (VkPr, LdPr, Ipr.ipr);
		m_KunPrList.ListRows.Add (ipr);
		Ipr.ipr.pr_gr_stuf   = pr_gr_stuf;
		i ++;
	}
	Ipr.ipr.pr_gr_stuf   = Iprgrstufk.iprgrstufk.pr_gr_stuf;
	return TRUE;
}

BOOL CPrKunPage::IsChanged (CPreise *pIpr)
{
	DbRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ipr.ipr, &pIpr->ipr, sizeof (IPR));
		if (Ipr == ipr->ipr) break;
	}
	if (ipr == NULL)
	{
		return TRUE;
	}
	if (pIpr->cEk != ipr->cEk) return TRUE;
	if (pIpr->cVk != ipr->cVk) return TRUE;
	return FALSE;
}

BOOL CPrKunPage::InList (IPR_CLASS& Ipr)
{
	ListRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) ListRows.GetNext ()) != NULL)
	{
		if (Ipr == ipr->ipr) return TRUE;
	}
    return FALSE;
}

void CPrKunPage::DeleteDbRows ()
{
	DbRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) DbRows.GetNext ()) != NULL)
	{
		memcpy (&Ipr.ipr, &ipr->ipr, sizeof (IPR));
		if (!InList (Ipr))
		{
			Ipr.dbdelete ();
			PgrProt.Write (1);
		}
	}
}

BOOL CPrKunPage::Write ()
{
	extern short sql_mode;
	short sql_s;
	if (m_KunPr.IsWindowEnabled ())
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	Form.Get ();
	Ipr.beginwork ();
	m_KunPrList.StopEnter ();
	int count = m_KunPrList.GetItemCount ();
	for (int i = 0; i < count; i ++)
	{
		 IPR *ipr = new IPR;
		 memcpy (ipr, &ipr_null, sizeof (IPR));
         CString Text;
		 Text = m_KunPrList.GetItemText (i, m_KunPrList.PosA);
		 ipr->a = CStrFuncs::StrToDouble (Text);
     	 CString VkPr =  m_KunPrList.GetItemText (i, m_KunPrList.PosVkPr);
		 ipr->vk_pr_eu = CStrFuncs::StrToDouble (VkPr);
		 ipr->vk_pr_i = ipr->vk_pr_eu;
		 CString LdPr =  m_KunPrList.GetItemText (i, m_KunPrList.PosLdPr);
		 ipr->ld_pr_eu = CStrFuncs::StrToDouble (LdPr);
		 ipr->ld_pr = ipr->ld_pr_eu;
		 if (m_KunPrList.Aufschlag == m_KunPrList.LIST ||
			 m_KunPrList.Aufschlag == m_KunPrList.ALL)
		 {
			CString EkAbs =  m_KunPrList.GetItemText (i, m_KunPrList.PosEkAbs);
			ipr->add_ek_abs = CStrFuncs::StrToDouble (EkAbs);
			CString EkProz =  m_KunPrList.GetItemText (i, m_KunPrList.PosEkProz);
			ipr->add_ek_proz = CStrFuncs::StrToDouble (EkProz);
		 }
		 else
		 {
			 CPreise *lpr = (CPreise *) m_KunPrList.ListRows.Get (i);
			 ipr->add_ek_abs = lpr->ipr.add_ek_abs;
			 ipr->add_ek_proz = lpr->ipr.add_ek_proz;
		 }

		 if (!TestDecValues (ipr)) 
		 {
			 MessageBox (_T("Mximalwert f�r Preisfeld �berschritten"));
			 return FALSE;
		 }
		 ipr->mdn = Mdn.mdn.mdn; 
		 ipr->pr_gr_stuf = Iprgrstufk.iprgrstufk.pr_gr_stuf; 
		 ipr->kun_pr = I_kun_prk.i_kun_prk.kun_pr;
		 ipr->kun = 0;
		 CPreise *pr = new CPreise (VkPr, LdPr, *ipr);
		 if (ipr->vk_pr_eu != 0.0 || 
			 ipr->ld_pr_eu != 0.0)
		 {
				ListRows.Add (pr);
		 }
		 delete ipr;
	}

	DeleteDbRows ();

	ListRows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) ListRows.GetNext ()) != NULL)
	{
		if (ipr->ipr.a == 0.0) continue;
	    if (ipr->ipr.vk_pr_eu != 0.0 || 
			 ipr->ipr.ld_pr_eu != 0.0 ||
			 ipr->ipr.add_ek_abs != 0 ||
			 ipr->ipr.add_ek_proz != 0)
		{
			memcpy (&Ipr.ipr, &ipr->ipr, sizeof (IPR));
			Ipr.dbupdate ();
			if (IsChanged (ipr))
			{
				PgrProt.Write ();
			}
		}
	}
	Ipr.sqlexecute (IprDelCursor2);
	EnableHeadControls (TRUE);
	m_KunPr.SetFocus ();
	DestroyRows (DbRows);
	DestroyRows (m_KunPrList.ListRows);
	DestroyRows (ListRows);
//	I_kun_prk.i_kun_prk.pr_gr_stuf = Iprgrstufk.iprgrstufk.pr_gr_stuf;
	I_kun_prk.i_kun_prk.pr_gr_stuf = Ipr.ipr.pr_gr_stuf;
	CString Date;
	CStrFuncs::SysDate (Date);
	I_kun_prk.ToDbDate (Date, &I_kun_prk.i_kun_prk.gue_ab);
	I_kun_prk.dbupdate ();
	I_kun_prk.sqlexecute (UpdateIvCursor);
	Ipr.commitwork ();
	sql_mode = sql_s;
	if (Choice != NULL)
	{
		Choice->FillList ();
	}
	GrundPreis.SetVisible (FALSE);
	return TRUE;
}

BOOL CPrKunPage::DeleteAll ()
{
	extern short sql_mode;
	short sql_s;

	if (m_KunPr.IsWindowEnabled ())
	{
		return FALSE;
	}
	if (MessageBox (_T("Kundenpreisliste l�schen ?"), NULL, 
		             MB_YESNO | MB_ICONQUESTION) ==
		IDNO)
	{
		return FALSE;
	}
	sql_s = sql_mode;
	sql_mode = 1;
	Ipr.beginwork ();
	m_KunPrList.StopEnter ();

	DeleteDbRows ();
	DestroyRows (DbRows);
	DestroyRows (m_KunPrList.ListRows);
	DestroyRows (ListRows);
	m_KunPrList.DeleteAllItems ();
	I_kun_prk.dbdelete ();
	memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
	memcpy (&I_kun_prk.i_kun_prk, &i_kun_prk_null, sizeof (I_KUN_PRK));
	Form.Show ();

	EnableHeadControls (TRUE);
	m_KunPr.SetFocus ();
	Ipr.commitwork ();
	sql_mode = sql_s;
	if (Choice != NULL)
	{
		Choice->FillList ();
	}
	return TRUE;
}

void CPrKunPage::OnKunPrchoice ()
{
    ChoiceStat = TRUE;
	Form.Get ();
	if (Choice != NULL && !ModalChoice)
	{
		Choice->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (Choice == NULL)
	{
		Choice = new CChoiceIKunPr (this);
	    Choice->IsModal = ModalChoice;
		Choice->m_Mdn = Mdn.mdn.mdn;
		Choice->SearchText = Search;
		Choice->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    Choice->SetDbClass (&A_bas);
	Choice->m_Mdn = Mdn.mdn.mdn;
	Choice->SearchText = Search;
	if (ModalChoice)
	{
			Choice->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;

/*
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		Choice->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		cy -= 100;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
*/
		Choice->MoveWindow (&rect);
		Choice->SetFocus ();

		return;
	}
    if (Choice->GetState ())
    {
		  CIKunPrList *abl = Choice->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
          I_kun_prk.i_kun_prk.mdn = Mdn.mdn.mdn;
          I_kun_prk.i_kun_prk.kun_pr = abl->kun_pr;
		  if (I_kun_prk.dbreadfirst () == 0)
		  {
			Form.Show ();
//			m_KunPr.EnableWindow (TRUE);
			EnableHeadControls (TRUE);
			m_PrGrStuf.SetSel (0, -1, TRUE);
			m_PrGrStuf.SetFocus ();
			if (Search == "")
			{
				PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			}
		  }
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}

void CPrKunPage::OnKunPrSelected ()
{
	if (Choice == NULL) return;
    CIKunPrList *abl = Choice->GetSelectedText (); 
    if (abl == NULL) return;
    I_kun_prk.i_kun_prk.mdn = Mdn.mdn.mdn;
    I_kun_prk.i_kun_prk.kun_pr = abl->kun_pr;
    if (I_kun_prk.dbreadfirst () == 0)
	{
		m_PrGrStuf.EnableWindow (TRUE);
		m_PrGrStuf.SetFocus ();
		PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	if (CloseChoice)
	{
		OnKunPrCanceled (); 
	}
    Form.Show ();
	if (Choice->FocusBack)
	{
		Read ();
		Form.Show ();
		m_KunZusBz.SetFocus ();
		Choice->SetListFocus ();
	}
}

void CPrKunPage::OnKunPrCanceled ()
{
	Choice->ShowWindow (SW_HIDE);
}

BOOL CPrKunPage::StepBack ()
{
	if (m_KunPr.IsWindowEnabled ())
	{
		if (Frame != NULL)
		{
			if (Frame->IsKindOf (RUNTIME_CLASS (CDialog)))
			{
				((CDialog *) Frame)->EndDialog (0);
			}
			else
			{
					Frame->GetParent ()->DestroyWindow ();
					return FALSE;
			}
		}
	}
	else
	{
		m_KunPrList.StopEnter ();
		EnableHeadControls (TRUE);
		m_KunPr.SetFocus ();
		DestroyRows (DbRows);
	    DestroyRows (m_KunPrList.ListRows);
		DestroyRows (ListRows);
		m_KunPrList.DeleteAllItems ();
		GrundPreis.SetVisible (FALSE);
	}
	return TRUE;
}

void CPrKunPage::OnCancel ()
{
	StepBack ();
}

void CPrKunPage::OnSave ()
{
	Write ();
}

void CPrKunPage::OnPrGrchoice ()
{
    ChoiceStat = TRUE;
	Form.Get ();
	if (ChoicePrGrStuf != NULL && !ModalChoicePrGrStuf)
	{
		ChoicePrGrStuf->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoicePrGrStuf == NULL)
	{
		ChoicePrGrStuf = new CChoicePrGrStuf (this);
	    ChoicePrGrStuf->IsModal = ModalChoicePrGrStuf;
		ChoicePrGrStuf->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoicePrGrStuf->SetDbClass (&A_bas);
	ChoicePrGrStuf->m_Mdn = Mdn.mdn.mdn;
	ChoicePrGrStuf->SearchText = Search;
	if (ModalChoicePrGrStuf)
	{
			ChoicePrGrStuf->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		int sx = GetSystemMetrics (SM_CXSCREEN);
		int sy = GetSystemMetrics (SM_CYSCREEN);
		CRect rect;
		ChoicePrGrStuf->GetWindowRect (&rect);
		int cx = rect.right - rect.left;
		int cy = rect.bottom - rect.top;
		cy -= 100;
		rect.right = sx;
		rect.left = rect.right - cx;
		rect.top = 0;
		rect.bottom = rect.top + cy;
		ChoicePrGrStuf->MoveWindow (&rect);
		ChoicePrGrStuf->SetFocus ();

		return;
	}
    if (ChoicePrGrStuf->GetState ())
    {
		  CPrGrStufList *abl = ChoicePrGrStuf->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
          Iprgrstufk.iprgrstufk.mdn = Mdn.mdn.mdn;
          Iprgrstufk.iprgrstufk.pr_gr_stuf = abl->pr_gr_stuf;
		  if (Iprgrstufk.dbreadfirst () == 0)
		  {
			Form.Show ();
			m_PrGrStuf.EnableWindow (TRUE);
			m_PrGrStuf.SetSel (0, -1, TRUE);
			m_PrGrStuf.SetFocus ();
			if (Search == "")
			{
				PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
			}
		  }
    }
	else
	{
	 	  ChoiceStat = FALSE;	
	}
}


void CPrKunPage::OnMdnchoice ()
{

	if (ChoiceMdn != NULL && !ModalChoiceMdn)
	{
		ChoiceMdn->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = ModalChoiceMdn;
		ChoiceMdn->CreateDlg ();
	}

//	Choice->IsModal = ModalChoice;
    ChoiceMdn->SetDbClass (&A_bas);
	if (ModalChoiceMdn)
	{
			ChoiceMdn->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceMdn->GetWindowRect (&rect);
		rect.right = rect.right - rect.left;
		rect.left = 0;
		rect.top = mrect.bottom - 300;
		rect.bottom = rect.top + 300;
		ChoiceMdn->MoveWindow (&rect);
		ChoiceMdn->SetFocus ();
		return;
	}
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
//		  a_kun_gx.mdn = abl->mdn;
          memcpy (&Mdn.mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr.adr, &adr_null, sizeof (ADR));
		  Mdn.mdn.mdn = abl->mdn;
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
}

void CPrKunPage::FillPrGrStufCombo ()
{
}

void CPrKunPage::FillKunPrCombo ()
{
}

void CPrKunPage::OnDelete ()
{
	if (m_KunPr.IsWindowEnabled ())
	{
		Form.Get ();
		if (I_kun_prk.i_kun_prk.kun_pr == 0)
		{
			return;
		}
		CString Message;
		Message.Format (_T("Kundebpreisliste %ld l�schen"), I_kun_prk.i_kun_prk.kun_pr);

		int ret = MessageBox (Message, NULL, 
			MB_YESNO | MB_DEFBUTTON2 | MB_ICONQUESTION);
		if (ret == IDYES)
		{
			I_kun_prk.dbdelete ();
			m_KunPr.SetFocus ();
 			memcpy (&Iprgrstufk.iprgrstufk, &iprgrstufk_null, sizeof (IPRGRSTUFK));
			memcpy (&I_kun_prk.i_kun_prk, &i_kun_prk_null, sizeof (I_KUN_PRK));
			Form.Show ();
			DestroyRows (DbRows);
			DestroyRows (m_KunPrList.ListRows);
			DestroyRows (ListRows);
			m_KunPrList.DeleteAllItems ();
			Ipr.sqlexecute (IprDelCursor);
		}
		m_KunPr.SetFocus ();
		m_KunPr.SetSel (0, -1);
		return;
	}
	m_KunPrList.DeleteRow ();
}

void CPrKunPage::OnInsert ()
{
	m_KunPrList.InsertRow ();
}

/*
BOOL CPrKunPage::Print ()
{
	CProcess print;
	CString Command = "70001 11112";
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}
*/

BOOL CPrKunPage::Print ()
{
// I_kun_prk.i_kun_prk.kun_pr
	CProcess print;
	Form.Get ();
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11131.llf", tmp);
	}
	else
	{
		dName = "11131.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11131\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "kun_pr %ld %ld\n", I_kun_prk.i_kun_prk.kun_pr,
			                             I_kun_prk.i_kun_prk.kun_pr);
		fclose (fp);
		Command.Format ("dr70001 -name 11131 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11131";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

BOOL CPrKunPage::PrintAll ()
{
	CProcess print;
	Form.Get ();
    LPTSTR tmp = getenv ("TMPPATH");
    CString dName;
	FILE *fp;
	if (tmp != NULL)
	{
		dName.Format ("%s\\11131.llf", tmp);
	}
	else
	{
		dName = "11131.llf";
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME 11131\n");
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");
		fprintf (fp, "MITRANGE 1\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn.mdn.mdn,Mdn.mdn.mdn);
		fprintf (fp, "kun_pr %ld %ld\n", (long) 1,
			                             (long) 99999999);
		fclose (fp);
		Command.Format ("dr70001 -name 11131 -datei %s", dName.GetBuffer ());
	}
	else
	{
		Command = "dr70001 -name 11131";
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
			MB_OK | MB_ICONERROR);
	}
	return TRUE;
}

void CPrKunPage::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_KunPrList.StartPauseEnter ();
}

void CPrKunPage::OnListEndTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_KunPrList.EndPauseEnter ();
}

void CPrKunPage::EnableHeadControls (BOOL enable)
{
	HeadControls.Enable (enable);
	PosControls.Enable (!enable);
}

void CPrKunPage::DestroyRows(CVector &Rows)
{
	Rows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
}

void CPrKunPage::ReadCfg ()
{
    char cfg_v [256];

/*
    if (Cfg.GetCfgValue ("UseOdbc", cfg_v) == TRUE)
    {
			DB_CLASS::UseOdbc = atoi (cfg_v);
    }
*/
    if (Cfg.GetCfgValue ("MaxComboEntries", cfg_v) == TRUE)
    {
			m_KunPrList.MaxComboEntries = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("RemoveKun", cfg_v) == TRUE)
    {
			RemoveKun = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("ModalChoice", cfg_v) == TRUE)
    {
			ModalChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CloseChoice", cfg_v) == TRUE)
    {
			CloseChoice = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("GridLines", cfg_v) == TRUE)
    {
			m_KunPrList.GridLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("HLines", cfg_v) == TRUE)
    {
			m_KunPrList.HLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("VLines", cfg_v) == TRUE)
    {
			m_KunPrList.VLines = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("CellHeight", cfg_v) == TRUE)
    {
			CellHeight = atoi (cfg_v);
    }
    if (Cfg.GetCfgValue ("Aufschlag", cfg_v) == TRUE)
    {
			m_KunPrList.Aufschlag = atoi (cfg_v);
    }
	Cfg.CloseCfg ();
}

void CPrKunPage::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_KunPrList.ListEdit ||
        Control == &m_KunPrList.ListComboBox ||
		Control == &m_KunPrList.SearchListCtrl.Edit)	
	{
		ListCopy ();
		return;
	}

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CPrKunPage::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

void CPrKunPage::ListCopy ()
{
    HGLOBAL hglbCopy;
    if ( !OpenClipboard() )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return;
    }

   // Remove the current Clipboard contents  
    if( !EmptyClipboard() )
    {
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return;  
    }

	if (IsWindow (m_KunPrList.ListEdit.m_hWnd) ||
		IsWindow (m_KunPrList.ListComboBox) ||
		IsWindow (m_KunPrList.SearchListCtrl.Edit))
	{
		m_KunPrList.StopEnter ();
		m_KunPrList.StartEnter (m_KunPrList.EditCol, m_KunPrList.EditRow);
	}

	try
	{
		BOOL SaveAll = TRUE;
		char sep [] = {13, 10, 0};
		CString Buffer = _T("");
		for (int i = 0; i < m_KunPrList.GetItemCount (); i ++)
		{
			if (Buffer != "")
			{
				Buffer += sep;
			}
			CString Row = "";
			int cols = m_KunPrList.GetHeaderCtrl ()->GetItemCount ();
			for (int j = 0; j < cols; j ++)
			{
				if (Row != "")
				{
					Row += Separator;
				}
				CString Field = m_KunPrList.GetItemText (i, j);
				Field.TrimRight ();
				Row += Field; 
			}
			Buffer += Row;
		}
		hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (Buffer.GetLength () + 1) * 2 ); 
        LPTSTR p = (LPTSTR) GlobalLock(hglbCopy);
        _tcscpy (p, Buffer.GetBuffer ());
        GlobalUnlock(hglbCopy); 
		HANDLE cData = NULL;
		cData = ::SetClipboardData( CF_TEXT, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
		}  
	}
	catch (...) {}
    CloseClipboard();
}

void CPrKunPage::OnAddEk ()
{

	if (m_KunPr.IsWindowEnabled ())
	{
		return;
	}

	if (m_KunPrList.Aufschlag != m_KunPrList.BUTTON &&
		m_KunPrList.Aufschlag != m_KunPrList.ALL)
	{
		return;
	}
	int Row = m_KunPrList.EditRow;
	if (Row < 0) return;
	CPreise *p = (CPreise *) m_KunPrList.ListRows.Get (Row);

	if (m_KunPrList.Aufschlag == m_KunPrList.ALL)
	{
		CString EkAbs =  m_KunPrList.GetItemText (Row, m_KunPrList.PosEkAbs);
		p->ipr.add_ek_abs = CStrFuncs::StrToDouble (EkAbs);
		CString EkProz =  m_KunPrList.GetItemText (Row, m_KunPrList.PosEkProz);
		p->ipr.add_ek_proz = CStrFuncs::StrToDouble (EkProz);
	}

	CAddEk dlg;
	dlg.EkProz.Format ("%.2lf", p->ipr.add_ek_proz);
	CStrFuncs::PointToKomma (dlg.EkProz); 
	dlg.EkAbs.Format ("%.2lf", p->ipr.add_ek_abs);
	CStrFuncs::PointToKomma (dlg.EkAbs); 
	dlg.Font = GetFont ();
	dlg.Parent = this;
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
		p->ipr.add_ek_proz = CStrFuncs::StrToDouble (dlg.EkProz);
		p->ipr.add_ek_abs = CStrFuncs::StrToDouble (dlg.EkAbs);
		m_KunPrList.DoubleToString (p->ipr.add_ek_abs, dlg.EkAbs, 2);
		m_KunPrList.DoubleToString (p->ipr.add_ek_proz, dlg.EkProz, 2);
		FillList.SetItemText (dlg.EkAbs.GetBuffer (), Row, m_KunPrList.PosEkAbs);
		FillList.SetItemText (dlg.EkProz.GetBuffer (), Row, m_KunPrList.PosEkProz);
	}

/*
	int Row = m_KunPrList.EditRow;
	if (Row < 0) return;
	CPreise *p = (CPreise *) m_KunPrList.ListRows.Get (Row);
	CAddEk dlg;
	dlg.EkProz.Format ("%.2lf", p->ipr.add_ek_proz);
	CStrFuncs::PointToKomma (dlg.EkProz); 
	dlg.EkAbs.Format ("%.2lf", p->ipr.add_ek_abs);
	CStrFuncs::PointToKomma (dlg.EkAbs); 
	dlg.Font = GetFont ();
	dlg.Parent = this;
	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
		p->ipr.add_ek_proz = CStrFuncs::StrToDouble (dlg.EkProz);
		p->ipr.add_ek_abs = CStrFuncs::StrToDouble (dlg.EkAbs);
	}
*/
}

BOOL CPrKunPage::TestDecValues (IPR *ipr)
{
	if (ipr->vk_pr_eu > 9999.9999 ||
		ipr->vk_pr_eu < -9999.9999)
	{
		return FALSE;
	}

	if (ipr->ld_pr_eu > 9999.99 ||
		ipr->ld_pr_eu < -9999.99)
	{
		return FALSE;
	}
    
	if (ipr->add_ek_proz > 9999.99 ||
		ipr->add_ek_proz < -9999.99)
	{
		return false;
	}

	if (ipr->add_ek_abs > 9999.99 ||
		ipr->add_ek_abs < -9999.99)
	{
		return false;
	}
	return TRUE;
}

void CPrKunPage::ShowGrundPreis ()
{
	if (m_KunPr.IsWindowEnabled ()) return;

    CString Text;
	Text = m_KunPrList.GetItemText (m_KunPrList.EditRow, m_KunPrList.PosA);
	Ipr.ipr.a = CStrFuncs::StrToDouble (Text);
	A_bas.a_bas.a = Ipr.ipr.a;
	int dsqlstatus = A_bas.dbreadfirst ();
	Ipr.ipr.a_grund = A_bas.a_bas.a_grund;	// 301111

	if (!GrundPreis.IsWindowVisible ())
	{
		GrundPreis.SetVisible ();
		if (dsqlstatus != 0) return;
		GrundPreis.Show (Ipr.ipr.mdn, 0, Ipr.ipr.a, A_bas.a_bas.a_typ, A_bas.a_bas.me_einh);
	}
	else
	{
		GrundPreis.SetVisible (FALSE);
	}
	CView *parent = (CView *) GetParent ()->GetParent ();
	CDocument *doc = parent->GetDocument ();
	doc->UpdateAllViews (parent);
}

void CPrKunPage::SetGrundPreis ()
{
	if (m_KunPr.IsWindowEnabled ()) return;

	if (GrundPreis.IsWindowVisible ())
	{
		GrundPreis.Show (Ipr.ipr.mdn, 0, Ipr.ipr.a, A_bas.a_bas.a_typ, A_bas.a_bas.me_einh);
	}
}

void CPrKunPage::RowChanged (int NewRow)
{
    CString Text;
	Text = m_KunPrList.GetItemText (NewRow, m_KunPrList.PosA);
	Ipr.ipr.a = CStrFuncs::StrToDouble (Text);
	A_bas.a_bas.a = Ipr.ipr.a;
	A_bas.dbreadfirst ();
	Ipr.ipr.a_grund = A_bas.a_bas.a_grund;	// 301111

	SetGrundPreis ();
}
