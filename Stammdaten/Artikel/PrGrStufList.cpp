#include "StdAfx.h"
#include "PrGrStufList.h"

CPrGrStufList::CPrGrStufList(void)
{
	mdn = 0;
	pr_gr_stuf = 0l;
	zus_bz = _T("");
}

CPrGrStufList::CPrGrStufList(short mdn, long pr_gr_stuf, LPTSTR zus_bz)
{
	this->mdn    = mdn;
	this->pr_gr_stuf = pr_gr_stuf;
	this->zus_bz = zus_bz;
}

CPrGrStufList::~CPrGrStufList(void)
{
}
