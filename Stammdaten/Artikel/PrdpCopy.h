#pragma once
#include "A_bas.h"
#include "A_bas_erw.h"
#include "prp_mibwerte.h"
#include "prp_cpp.h"
#include "prp_vik.h"
#include "prp_user.h"
#include "prp_zusatz.h"
#include "hst_info.h"
#include "atext.h"


class CPrdpCopy
{
private:
	enum Error
	{
		NoError,
		SourceNotFound,
		UnknownError
	};
	enum
	{
		ZutFromAtexte = 1,
		ZutFromPrAusz = 2,
		ZutFromA_bas_erw = 3,
		ZutFromRezepture = 4,
	};

	static LPTSTR ErrorText[];
	A_BAS_CLASS A_bas_source;
	A_BAS_ERW_CLASS A_bas_erw_source;
	PRP_MIBWERTE_CLASS Prp_mibwerte;
	PRP_CPP_CLASS Prp_cpp;
	PRP_VIK_CLASS Prp_vik;
	PRP_USER_CLASS Prp_user;
	PRP_ZUSATZ_CLASS Prp_zusatz;
	HST_INFO_CLASS Hst_info;
	ATEXTE_CLASS Atexte;
	int error;

	int GetErrror ()
	{
		return error;
	}

	LPTSTR GetErrorText ()
	{
		if (error < NoError || error > SourceNotFound)
		{
			error = UnknownError;
		}
		return ErrorText[error];
	}

public:
	CPrdpCopy(void);
public:
	~CPrdpCopy(void);
	void Run (A_BAS_CLASS *A_bas_dest,A_BAS_ERW_CLASS *A_bas_erw_dest, double a_source, int ZutSource);
};
