#include "stdafx.h"
#include "ChoiceAtxt.h"
#include "DbUniCode.h"
#include "Process.h"
#include "Atext.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

static struct ATEXTE atexte;

int CChoiceAtxt::Sort1 = -1;
int CChoiceAtxt::Sort2 = -1;
int CChoiceAtxt::Sort3 = -1;
int CChoiceAtxt::Sort4 = -1;

BEGIN_MESSAGE_MAP(CChoiceAtxt, CChoiceX)
END_MESSAGE_MAP()

CChoiceAtxt::CChoiceAtxt(CWnd* pParent) 
        : CChoiceX(pParent)
{
	Bean.ArchiveName = _T("AtxtList.prp");
}

CChoiceAtxt::~CChoiceAtxt() 
{
	DestroyList ();
}

void CChoiceAtxt::DestroyList() 
{
	for (std::vector<CATextList *>::iterator pabl = AtxtList.begin (); pabl != AtxtList.end (); ++pabl)
	{
		CATextList *abl = *pabl;
		delete abl;
	}
    AtxtList.clear ();
}

void CChoiceAtxt::FillList () 
{
    long  sys;
    long txt_nr;
    TCHAR txt [sizeof (atexte.txt)];

	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl allgeime Texte"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Text-Nr"),       1, 100, LVCFMT_RIGHT);
    SetCol (_T("Gerätenummer"),  2, 100, LVCFMT_RIGHT);
    SetCol (_T("Text"),          3, 400);

	if (AtxtList.size () == 0)
	{
		DbClass->sqlout ((long *) &sys,      SQLLONG, 0);
		DbClass->sqlout ((long *) &txt_nr,   SQLLONG, 0);
		DbClass->sqlout ((LPTSTR) txt,       SQLCHAR, sizeof (txt));
		int cursor = DbClass->sqlcursor (_T("select sys, txt_nr, txt ")
			                             _T("from atexte where txt_nr > 0"));

		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) txt;
			CDbUniCode::DbToUniCode (txt, pos);
			CATextList *abl = new CATextList (sys, txt_nr, txt);
			AtxtList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
		Load ();
	}

	for (std::vector<CATextList *>::iterator pabl = AtxtList.begin (); pabl != AtxtList.end (); ++pabl)
	{
		CATextList *abl = *pabl;
		CString Sys;
		CString TxtNr;
		CString Txt;
		Sys.Format (_T("%ld"), abl->sys); 
		TxtNr.Format (_T("%ld"), abl->txt_nr); 
		Txt = abl->txt;
        int ret = InsertItem (i, -1);
        ret = SetItemText (TxtNr.GetBuffer (), i, 1);
        ret = SetItemText (Sys.GetBuffer (), i, 2);
        ret = SetItemText (Txt.GetBuffer (), i, 3);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceAtxt::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceAtxt::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceAtxt::SearchSys (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAtxt::SearchTxtNr (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAtxt::SearchTxt (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 3);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAtxt::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchSys (ListBox, EditText.GetBuffer ());
             break;
        case 2 :
             SearchTxtNr (ListBox, EditText.GetBuffer ());
             break;
        case 3 :
             EditText.MakeUpper ();
             SearchTxt (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceAtxt::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceAtxt::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort3);
	   }
	   return 0;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   return 0;
}


void CChoiceAtxt::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CATextList *abl = AtxtList [i];
		   
		   abl->txt_nr     = _tstol (ListBox->GetItemText (i, 1));
		   abl->sys        = _tstol (ListBox->GetItemText (i, 2));
		   abl->txt        = ListBox->GetItemText (i, 3);
	}
	for (int i = 1; i <= 4; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);
}

void CChoiceAtxt::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = AtxtList [idx];
}

CATextList *CChoiceAtxt::GetSelectedText ()
{
	CATextList *abl = (CATextList *) SelectedRow;
	return abl;
}


void CChoiceAtxt::OnEnter ()
{
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cTextNr = m_List.GetItemText (idx, 1);  
		long txt_nr = _tstol (cTextNr.GetBuffer ());
		Message.Format (_T("TextNr=%ld"), txt_nr);
		ToClipboard (Message);
	}
	p.SetCommand (_T("atexte"));
	HANDLE Pid = p.Start ();
}
