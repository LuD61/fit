#pragma once
#include "ctrlgrid.h"
#include "ColorButton.h"

class CCtrlGridColor :
	public CCtrlGrid
{
public:
	CCtrlGridColor(void);
public:
	~CCtrlGridColor(void);
	virtual void CreateColorChoiceButton (CColorButton&, int, CWnd*);
	virtual void CreateChoiceButton (CColorButton&, int, CWnd*);
	virtual void CreateChoiceButton (CButton&, int, CWnd*);
};
