#include "stdafx.h"
#include "a_hndw.h"

struct A_HNDW a_hndw, a_hndw_null, a_hndw_def;

void A_HNDW_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_hndw.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_hndw.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_hndw.a_grnd,SQLCHAR,2);
    sqlout ((long *) &a_hndw.a_krz,SQLLONG,0);
    sqlout ((double *) &a_hndw.a_leer,SQLDOUBLE,0);
    sqlout ((double *) &a_hndw.a_pfa,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_hndw.a_pfa_kz,SQLCHAR,2);
    sqlout ((short *) &a_hndw.anz_reg_eti,SQLSHORT,0);
    sqlout ((short *) &a_hndw.anz_theke_eti,SQLSHORT,0);
    sqlout ((short *) &a_hndw.anz_waren_eti,SQLSHORT,0);
    sqlout ((short *) &a_hndw.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_hndw.fil,SQLSHORT,0);
    sqlout ((double *) &a_hndw.gew_bto,SQLDOUBLE,0);
    sqlout ((short *) &a_hndw.herk_land,SQLSHORT,0);
    sqlout ((TCHAR *) a_hndw.hdkl,SQLCHAR,5);
    sqlout ((double *) &a_hndw.inh,SQLDOUBLE,0);
    sqlout ((short *) &a_hndw.me_einh_kun,SQLSHORT,0);
    sqlout ((short *) &a_hndw.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) a_hndw.pr_ausz,SQLCHAR,2);
    sqlout ((TCHAR *) a_hndw.pr_man,SQLCHAR,2);
    sqlout ((TCHAR *) a_hndw.pr_ueb,SQLCHAR,2);
    sqlout ((TCHAR *) a_hndw.reg_eti,SQLCHAR,2);
    sqlout ((short *) &a_hndw.sg1,SQLSHORT,0);
    sqlout ((short *) &a_hndw.sg2,SQLSHORT,0);
    sqlout ((TCHAR *) a_hndw.smt,SQLCHAR,2);
    sqlout ((double *) &a_hndw.tara,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_hndw.theke_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_hndw.verk_art,SQLCHAR,2);
    sqlout ((TCHAR *) a_hndw.vk_typ,SQLCHAR,2);
    sqlout ((TCHAR *) a_hndw.vpk,SQLCHAR,2);
    sqlout ((TCHAR *) a_hndw.waren_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_hndw.mwst_ueb,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &a_hndw.verk_beg,SQLDATE,0);
    sqlout ((short *) &a_hndw.me_einh_abverk,SQLSHORT,0);
    sqlout ((double *) &a_hndw.inh_abverk,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_hndw.hnd_gew_abverk,SQLCHAR,2);
            cursor = sqlcursor (_T("select a_hndw.a,  ")
_T("a_hndw.a_grnd,  a_hndw.a_krz,  a_hndw.a_leer,  a_hndw.a_pfa,  ")
_T("a_hndw.a_pfa_kz,  a_hndw.anz_reg_eti,  a_hndw.anz_theke_eti,  ")
_T("a_hndw.anz_waren_eti,  a_hndw.delstatus,  a_hndw.fil,  a_hndw.gew_bto,  ")
_T("a_hndw.herk_land,  a_hndw.hdkl,  a_hndw.inh,  a_hndw.me_einh_kun,  ")
_T("a_hndw.mdn,  a_hndw.pr_ausz,  a_hndw.pr_man,  a_hndw.pr_ueb,  ")
_T("a_hndw.reg_eti,  a_hndw.sg1,  a_hndw.sg2,  a_hndw.smt,  a_hndw.tara,  ")
_T("a_hndw.theke_eti,  a_hndw.verk_art,  a_hndw.vk_typ,  a_hndw.vpk,  ")
_T("a_hndw.waren_eti,  a_hndw.mwst_ueb,  a_hndw.verk_beg,  ")
_T("a_hndw.me_einh_abverk,  a_hndw.inh_abverk,  a_hndw.hnd_gew_abverk from a_hndw ")

#line 12 "a_hndw.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_hndw.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_hndw.a_grnd,SQLCHAR,2);
    sqlin ((long *) &a_hndw.a_krz,SQLLONG,0);
    sqlin ((double *) &a_hndw.a_leer,SQLDOUBLE,0);
    sqlin ((double *) &a_hndw.a_pfa,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_hndw.a_pfa_kz,SQLCHAR,2);
    sqlin ((short *) &a_hndw.anz_reg_eti,SQLSHORT,0);
    sqlin ((short *) &a_hndw.anz_theke_eti,SQLSHORT,0);
    sqlin ((short *) &a_hndw.anz_waren_eti,SQLSHORT,0);
    sqlin ((short *) &a_hndw.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_hndw.fil,SQLSHORT,0);
    sqlin ((double *) &a_hndw.gew_bto,SQLDOUBLE,0);
    sqlin ((short *) &a_hndw.herk_land,SQLSHORT,0);
    sqlin ((TCHAR *) a_hndw.hdkl,SQLCHAR,5);
    sqlin ((double *) &a_hndw.inh,SQLDOUBLE,0);
    sqlin ((short *) &a_hndw.me_einh_kun,SQLSHORT,0);
    sqlin ((short *) &a_hndw.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) a_hndw.pr_ausz,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.pr_man,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.pr_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.reg_eti,SQLCHAR,2);
    sqlin ((short *) &a_hndw.sg1,SQLSHORT,0);
    sqlin ((short *) &a_hndw.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) a_hndw.smt,SQLCHAR,2);
    sqlin ((double *) &a_hndw.tara,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_hndw.theke_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.verk_art,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.vk_typ,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.vpk,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.waren_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.mwst_ueb,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_hndw.verk_beg,SQLDATE,0);
    sqlin ((short *) &a_hndw.me_einh_abverk,SQLSHORT,0);
    sqlin ((double *) &a_hndw.inh_abverk,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_hndw.hnd_gew_abverk,SQLCHAR,2);
            sqltext = _T("update a_hndw set a_hndw.a = ?,  ")
_T("a_hndw.a_grnd = ?,  a_hndw.a_krz = ?,  a_hndw.a_leer = ?,  ")
_T("a_hndw.a_pfa = ?,  a_hndw.a_pfa_kz = ?,  a_hndw.anz_reg_eti = ?,  ")
_T("a_hndw.anz_theke_eti = ?,  a_hndw.anz_waren_eti = ?,  ")
_T("a_hndw.delstatus = ?,  a_hndw.fil = ?,  a_hndw.gew_bto = ?,  ")
_T("a_hndw.herk_land = ?,  a_hndw.hdkl = ?,  a_hndw.inh = ?,  ")
_T("a_hndw.me_einh_kun = ?,  a_hndw.mdn = ?,  a_hndw.pr_ausz = ?,  ")
_T("a_hndw.pr_man = ?,  a_hndw.pr_ueb = ?,  a_hndw.reg_eti = ?,  ")
_T("a_hndw.sg1 = ?,  a_hndw.sg2 = ?,  a_hndw.smt = ?,  a_hndw.tara = ?,  ")
_T("a_hndw.theke_eti = ?,  a_hndw.verk_art = ?,  a_hndw.vk_typ = ?,  ")
_T("a_hndw.vpk = ?,  a_hndw.waren_eti = ?,  a_hndw.mwst_ueb = ?,  ")
_T("a_hndw.verk_beg = ?,  a_hndw.me_einh_abverk = ?,  ")
_T("a_hndw.inh_abverk = ?,  a_hndw.hnd_gew_abverk = ? ")

#line 14 "a_hndw.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_hndw.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_hndw.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_hndw ")
                                  _T("where a = ? for update"));
            sqlin ((double *)   &a_hndw.a,  SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update a_hndw ")
                                  _T("set delstatus = 0 where a = ? and delstatus = 0 for update"));
            sqlin ((double *)   &a_hndw.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_hndw ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_hndw.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_hndw.a_grnd,SQLCHAR,2);
    sqlin ((long *) &a_hndw.a_krz,SQLLONG,0);
    sqlin ((double *) &a_hndw.a_leer,SQLDOUBLE,0);
    sqlin ((double *) &a_hndw.a_pfa,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_hndw.a_pfa_kz,SQLCHAR,2);
    sqlin ((short *) &a_hndw.anz_reg_eti,SQLSHORT,0);
    sqlin ((short *) &a_hndw.anz_theke_eti,SQLSHORT,0);
    sqlin ((short *) &a_hndw.anz_waren_eti,SQLSHORT,0);
    sqlin ((short *) &a_hndw.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_hndw.fil,SQLSHORT,0);
    sqlin ((double *) &a_hndw.gew_bto,SQLDOUBLE,0);
    sqlin ((short *) &a_hndw.herk_land,SQLSHORT,0);
    sqlin ((TCHAR *) a_hndw.hdkl,SQLCHAR,5);
    sqlin ((double *) &a_hndw.inh,SQLDOUBLE,0);
    sqlin ((short *) &a_hndw.me_einh_kun,SQLSHORT,0);
    sqlin ((short *) &a_hndw.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) a_hndw.pr_ausz,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.pr_man,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.pr_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.reg_eti,SQLCHAR,2);
    sqlin ((short *) &a_hndw.sg1,SQLSHORT,0);
    sqlin ((short *) &a_hndw.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) a_hndw.smt,SQLCHAR,2);
    sqlin ((double *) &a_hndw.tara,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_hndw.theke_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.verk_art,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.vk_typ,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.vpk,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.waren_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_hndw.mwst_ueb,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_hndw.verk_beg,SQLDATE,0);
    sqlin ((short *) &a_hndw.me_einh_abverk,SQLSHORT,0);
    sqlin ((double *) &a_hndw.inh_abverk,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_hndw.hnd_gew_abverk,SQLCHAR,2);
            ins_cursor = sqlcursor (_T("insert into a_hndw (")
_T("a,  a_grnd,  a_krz,  a_leer,  a_pfa,  a_pfa_kz,  anz_reg_eti,  anz_theke_eti,  ")
_T("anz_waren_eti,  delstatus,  fil,  gew_bto,  herk_land,  hdkl,  inh,  me_einh_kun,  ")
_T("mdn,  pr_ausz,  pr_man,  pr_ueb,  reg_eti,  sg1,  sg2,  smt,  tara,  theke_eti,  verk_art,  ")
_T("vk_typ,  vpk,  waren_eti,  mwst_ueb,  verk_beg,  me_einh_abverk,  inh_abverk,  ")
_T("hnd_gew_abverk) ")

#line 28 "a_hndw.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 30 "a_hndw.rpp"
}
