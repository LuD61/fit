#include "stdafx.h"
#include "ChoiceLgr.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceLgr::Sort1 = -1;
int CChoiceLgr::Sort2 = -1;
int CChoiceLgr::Sort3 = -1;
int CChoiceLgr::Sort4 = -1;

BOOL CChoiceLgr::LagerImageCreated = FALSE;
int CChoiceLgr::LagerImagePos = 0;

CChoiceLgr::CChoiceLgr(CWnd* pParent)
        : CChoiceX(pParent)
{
	Where = "";
	m_Artikel = 0.0;
	a_lgr_cursor = -1;
}

CChoiceLgr::~CChoiceLgr()
{
	DestroyList ();
	if (a_lgr_cursor == -1)
	{
		A_lgr.sqlclose (a_lgr_cursor);
	}
}

void CChoiceLgr::DestroyList()
{
	for (std::vector<CLgrList *>::iterator pabl = LgrList.begin (); pabl != LgrList.end (); ++pabl)
	{
		CLgrList *abl = *pabl;
		delete abl;
	}
    LgrList.clear ();
	SelectList.clear ();
}

void CChoiceLgr::SetImages ()
{
	HICON m_IOk;
	int idx;

    m_IOk = LoadIcon(AfxGetApp()->m_hInstance, 
		                 MAKEINTRESOURCE(IDI_OK1)); 
    idx = ImageList_AddIcon(hSmall, m_IOk); 
	DeleteObject (m_IOk);
    m_IOk = LoadIcon(AfxGetApp()->m_hInstance, 
		                 MAKEINTRESOURCE(IDI_LAGER)); 
    idx = ImageList_AddIcon(hSmall, m_IOk); 
	DeleteObject (m_IOk);
}

void CChoiceLgr::SetHeaderImages ()
{

	HDITEM    curItem;

	CListCtrl *listView = GetListView ();
	CHeaderCtrl *m_pHdrCtrl = listView->GetHeaderCtrl ();

	curItem.mask=  HDI_FORMAT;
	m_pHdrCtrl->GetItem(0, &curItem);
	curItem.mask= HDI_IMAGE | HDI_FORMAT;
	curItem.iImage= LagerImagePos;
	curItem.fmt |= HDF_IMAGE | HDF_STRING;
	m_pHdrCtrl->SetItem(0, &curItem);

}

void CChoiceLgr::AddHeaderImageList ()
{
	if (!LagerImageCreated)
	{
		LagerImagePos = HeaderImageList.GetImageCount ();
		HeaderImageList.Add(LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE (IDI_LAGER)));
		LagerImageCreated = TRUE;
	}
}

void CChoiceLgr::FillList ()
{
	short  mdn;
	short  fil;
    short  lgr;
    TCHAR lgr_bz [50];
	BOOL HasEntry;
	int ret = 0;

	if (m_Artikel != 0.0 && a_lgr_cursor == -1)
	{
		A_lgr.sqlout ((short *) A_lgr.a_lgr.haupt_lgr, SQLCHAR, 2);
		A_lgr.sqlout ((short *) &A_lgr.a_lgr.lgr_pla_kz, SQLSHORT, 0);

		A_lgr.sqlin ((short *)  &A_lgr.a_lgr.mdn, SQLSHORT, 0);
		A_lgr.sqlin ((long *)   &A_lgr.a_lgr.lgr, SQLLONG, 0);
		A_lgr.sqlin ((double *) &A_lgr.a_lgr.a, SQLDOUBLE, 0);
		a_lgr_cursor = A_lgr.sqlcursor (_T("select haupt_lgr, lgr_pla_kz from a_lgr ")
			                            _T("where mdn = ? ")
			                            _T("and lgr = ? ")
			                            _T("and a = ? "));
	}
		
	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Lager"));
	if (m_Artikel != 0.0)
	{
		SetCol (_T(" "),      0, 26, LVCFMT_LEFT);
	}
	else
	{
		SetCol (_T(""),      0, 0, LVCFMT_LEFT);
	}
    SetCol (_T("Lagernummer"),        1, 150, LVCFMT_RIGHT);
    SetCol (_T("Bezeichnung"),  2, 250);

	if (LgrList.size () == 0)
	{
		DbClass->sqlout ((short *) &lgr,       SQLSHORT, 0);
		DbClass->sqlout ((LPTSTR)  lgr_bz,     SQLCHAR, sizeof (lgr_bz));
		DbClass->sqlout ((short *) &mdn,       SQLSHORT, 0);
		DbClass->sqlout ((short *) &fil,       SQLSHORT, 0);
		CString Sql = _T("select lgr, lgr_bz, mdn, fil from lgr where lgr > 0");
		Sql += " ";
		Sql += Where;
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) lgr_bz;
			CDbUniCode::DbToUniCode (lgr_bz, pos);
			CLgrList *abl = new CLgrList (lgr, lgr_bz, mdn);
			LgrList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CLgrList *>::iterator pabl = LgrList.begin (); pabl != LgrList.end (); ++pabl)
	{
		CLgrList *abl = *pabl;
		CString Lgr;
		Lgr.Format (_T("%d"), abl->lgr);
		CString Num;
		CString Bez;
		_tcscpy (lgr_bz, abl->lgr_bz.GetBuffer ());

		CString LText;
		LText.Format (_T("%d %s"), abl->lgr,  abl->lgr_bz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Lgr;
        }
        else
        {
                Num = LText;
        }
		HasEntry = FALSE;
		if (m_Artikel != 0.0)
		{
			 A_lgr.a_lgr.mdn = abl->mdn; 
			 A_lgr.a_lgr.lgr = abl->lgr;
			 A_lgr.a_lgr.a   = m_Artikel;
			 A_lgr.sqlopen (a_lgr_cursor);
			 if (A_lgr.sqlfetch (a_lgr_cursor) == 0)
			 {
				 if (strcmp(A_lgr.a_lgr.haupt_lgr,"J") == 0) //WAL-159
				 {
					ret = InsertItem (i, 1);
				 }
				 else ret = InsertItem (i, 2);

			 }
			else
			{
				ret = InsertItem (i, -1);
			}
		}
		else
		{
			ret = InsertItem (i, -1);
		}
        ret = SetItemText (Num.GetBuffer (), i, 1);
        ret = SetItemText (lgr_bz, i, 2);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceLgr::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceLgr::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceLgr::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceLgr::SearchName (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}


void CChoiceLgr::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchName (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceLgr::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceLgr::CompareProc(LPARAM lParam1,
						 		     LPARAM lParam2,
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {

	   double li1 = _tstof (strItem1.GetBuffer ());
	   double li2 = _tstof (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   return 0;
}


void CChoiceLgr::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CLgrList *abl = LgrList [i];

		   abl->lgr =   _tstoi (ListBox->GetItemText (i, 1));
		   abl->lgr_bz = ListBox->GetItemText (i, 2);
	}
	for (int i = 1; i <= 9; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);
}

void CChoiceLgr::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = LgrList [idx];
}

CLgrList *CChoiceLgr::GetSelectedText ()
{
	CLgrList *abl = (CLgrList *) SelectedRow;
	return abl;
}

void CChoiceLgr::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (LgrList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}

