#pragma once
#include "BasisdatenPage.h"
#include "SplitPane.h"
#include "EanEmbListDlg.h"
#include "EanEmb.h"
#include "afxcmn.h"
#include "BasisdatenPage.h"



// CEanEmbPage : Eigenschaftenseitendialog

class CEanEmbPage : public CDbPropertyPage
{
	DECLARE_DYNCREATE(CEanEmbPage)

// Konstruktoren
public:
	BOOL HideButtons;
	CBasisdatenPage *Basis;
	CWnd *Frame;
	CEanEmbPage();
	~CEanEmbPage();

// Dialogfelddaten
	enum { IDD = IDD_EAN_EMB };

// Implementierung
protected:
	virtual void DoDataExchange(CDataExchange* pDX);        // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual void OnSize (UINT, int, int);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

// Meldungszuordnungstabellen
protected:
	DECLARE_MESSAGE_MAP()
public:
	CSplitPane SplitPane;
	CEanEmbListDlg EanEmbListDlg;
	CEanEmb EanEmb;
//	CEan Ean;
	void SetBasis (CBasisdatenPage *Basis);
	void SetBasis (CPageUpdate *Update);
	virtual void UpdatePage ();
	virtual BOOL OnSetActive ();
	virtual void OnDelete ();
};
