// HkstInfo.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Artikel.h"
#include "HkstInfo.h"
#include "UniFormField.h"


// CHkstInfo-Dialogfeld

IMPLEMENT_DYNAMIC(CHkstInfo, CDbPropertyPage)

CHkstInfo::CHkstInfo()
	: CDbPropertyPage(CHkstInfo::IDD)
{
	cursor = -1;
	name_cursor = -1;
	delcursor = -1;
    ProductPage5Read = FALSE;

	BoldColor = RGB (0,128,192);
	ZusValues[0] = Prp_zusatz.prp_zusatz.wert1;
	ZusValues[1] = Prp_zusatz.prp_zusatz.wert2;
	ZusValues[2] = Prp_zusatz.prp_zusatz.wert3;
	ZusValues[3] = Prp_zusatz.prp_zusatz.wert4;
	ZusValues[4] = Prp_zusatz.prp_zusatz.wert5;
	ZusValues[5] = Prp_zusatz.prp_zusatz.wert6;
	ZusValues[6] = Prp_zusatz.prp_zusatz.wert7;
	ZusValues[7] = Prp_zusatz.prp_zusatz.wert8;
	ZusValues[8] = Prp_zusatz.prp_zusatz.wert9;
	ZusValues[9] = Prp_zusatz.prp_zusatz.wert10;
	ZusValues[10] = Prp_zusatz.prp_zusatz.wert11;
	ZusValues[11] = Prp_zusatz.prp_zusatz.wert12;
	ZusValues[12] = Prp_zusatz.prp_zusatz.wert13;
	ZusValues[13] = Prp_zusatz.prp_zusatz.wert14;
	ZusValues[14] = Prp_zusatz.prp_zusatz.wert15;
	ZusValues[15] = Prp_zusatz.prp_zusatz.wert16;
	ZusValues[16] = Prp_zusatz.prp_zusatz.wert17;
	ZusValues[17] = Prp_zusatz.prp_zusatz.wert18;
	ZusValues[18] = Prp_zusatz.prp_zusatz.wert19;
	ZusValues[19] = Prp_zusatz.prp_zusatz.wert20;
	ZusValues[20] = Prp_zusatz.prp_zusatz.wert21;
	ZusValues[21] = Prp_zusatz.prp_zusatz.wert22;
	ZusValues[22] = Prp_zusatz.prp_zusatz.wert23;
	ZusValues[23] = Prp_zusatz.prp_zusatz.wert24;
	ZusValues[24] = Prp_zusatz.prp_zusatz.wert25;
	ZusValues[25] = Prp_zusatz.prp_zusatz.wert26;
	ZusValues[26] = Prp_zusatz.prp_zusatz.wert27;
	ZusValues[27] = Prp_zusatz.prp_zusatz.wert28;
	ZusValues[28] = Prp_zusatz.prp_zusatz.wert29;
	ZusValues[29] = Prp_zusatz.prp_zusatz.wert30;

	ZusRecordNames[0] = Prp_zusatz.prp_zusatz.name1;
	ZusRecordNames[1] = Prp_zusatz.prp_zusatz.name2;
	ZusRecordNames[2] = Prp_zusatz.prp_zusatz.name3;
	ZusRecordNames[3] = Prp_zusatz.prp_zusatz.name4;
	ZusRecordNames[4] = Prp_zusatz.prp_zusatz.name5;
	ZusRecordNames[5] = Prp_zusatz.prp_zusatz.name6;
	ZusRecordNames[6] = Prp_zusatz.prp_zusatz.name7;
	ZusRecordNames[7] = Prp_zusatz.prp_zusatz.name8;
	ZusRecordNames[8] = Prp_zusatz.prp_zusatz.name9;
	ZusRecordNames[9] = Prp_zusatz.prp_zusatz.name10;
	ZusRecordNames[10] = Prp_zusatz.prp_zusatz.name11;
	ZusRecordNames[11] = Prp_zusatz.prp_zusatz.name12;
	ZusRecordNames[12] = Prp_zusatz.prp_zusatz.name13;
	ZusRecordNames[13] = Prp_zusatz.prp_zusatz.name14;
	ZusRecordNames[14] = Prp_zusatz.prp_zusatz.name15;
	ZusRecordNames[15] = Prp_zusatz.prp_zusatz.name16;
	ZusRecordNames[16] = Prp_zusatz.prp_zusatz.name17;
	ZusRecordNames[17] = Prp_zusatz.prp_zusatz.name18;
	ZusRecordNames[18] = Prp_zusatz.prp_zusatz.name19;
	ZusRecordNames[19] = Prp_zusatz.prp_zusatz.name20;
	ZusRecordNames[20] = Prp_zusatz.prp_zusatz.name21;
	ZusRecordNames[21] = Prp_zusatz.prp_zusatz.name22;
	ZusRecordNames[22] = Prp_zusatz.prp_zusatz.name23;
	ZusRecordNames[23] = Prp_zusatz.prp_zusatz.name24;
	ZusRecordNames[24] = Prp_zusatz.prp_zusatz.name25;
	ZusRecordNames[25] = Prp_zusatz.prp_zusatz.name26;
	ZusRecordNames[26] = Prp_zusatz.prp_zusatz.name27;
	ZusRecordNames[27] = Prp_zusatz.prp_zusatz.name28;
	ZusRecordNames[28] = Prp_zusatz.prp_zusatz.name29;
	ZusRecordNames[29] = Prp_zusatz.prp_zusatz.name30;

}

CHkstInfo::~CHkstInfo()
{
}

void CHkstInfo::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LRH_STOFFE, m_LRhStoffe);
	DDX_Control(pDX, IDC_RH_STOFFE, m_RhStoffe);
	DDX_Control(pDX, IDC_LHERSTELLUNG, m_LHerstellung);
	DDX_Control(pDX, IDC_HERSTELLUNG, m_Herstellung);
	DDX_Control(pDX, IDC_LZBR_EMPF, m_LZbrEmpf);
	DDX_Control(pDX, IDC_ZBR_EMPF, m_ZbrEmpf);
	DDX_Control(pDX, IDC_LBLG_EMPF, m_LBlgEmpf);
	DDX_Control(pDX, IDC_BLG_EMPF, m_BlgEmpf);
	DDX_Control(pDX, IDC_LDKL_PFLICHT, m_LDklPflicht);
	DDX_Control(pDX, IDC_DKL_PFLICHT, m_DklPflicht);
}


BEGIN_MESSAGE_MAP(CHkstInfo, CDbPropertyPage)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()



// CHkstInfo-Meldungshandler

BOOL CHkstInfo::OnInitDialog()
{
    ProductPage5Read = TRUE;

	CPropertyPage::OnInitDialog();
	this->A_bas  = &Basis->A_bas;

	this->DlgBkColor = Basis->DlgBkColor;

	memcpy (&Hst_info.hst_info, &hst_info_null, sizeof (HST_INFO));
	memcpy (&Prp_zusatz.prp_zusatz, &prp_zusatz_null, sizeof (PRP_ZUSATZ));
	Prp_zusatz.prp_zusatz.a  = -1.0;
	Prp_zusatz.dbreadfirst ();
	FillNames ();
    Basis->ProductPage5Read = TRUE;

/*
	Prp_zusatz.sqlout ((char *) &Prp_zusatz.prp_zusatz.name, SQLCHAR, sizeof (Prp_zusatz.prp_zusatz.name));
	name_cursor = Prp_zusatz.sqlcursor (_T("select distinct name from prp_zusatz order by name"));

	Prp_zusatz.sqlin ((double *) &Prp_zusatz.prp_zusatz.a, SQLDOUBLE, 0);
	delcursor = Prp_zusatz.sqlcursor (_T("delete from prp_zusatz where a = ?"));
*/
	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
/*
	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	BoldFont.CreateFontIndirect (&l);
*/

//	Form.Add (new CFormField (&m_ProdMass, EDIT,  (char *)    A_bas->a_bas.prod_mass , VCHAR));

	Form.Add (new CUniFormField (&m_RhStoffe,   EDIT, (char *)   Hst_info.hst_info.rh_stoff_gew , VCHAR));
	Form.Add (new CUniFormField (&m_Herstellung,EDIT, (char *)   Hst_info.hst_info.herstellung , VCHAR));
	Form.Add (new CUniFormField (&m_ZbrEmpf,EDIT,     (char *)   Hst_info.hst_info.zbr_empf , VCHAR));
	Form.Add (new CUniFormField (&m_BlgEmpf,EDIT,     (char *)   Hst_info.hst_info.blg_empf , VCHAR));

	FillList = m_DklPflicht;
	FillList.SetStyle (LVS_REPORT);
	if (m_DklPflicht.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}

	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Bezeichnung"), 1, 300, LVCFMT_LEFT);
	FillList.SetCol (_T("Wert"), 2, 600, LVCFMT_LEFT);


    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (12, 20);
    CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LRhStoffe  = new CCtrlInfo (&m_LRhStoffe, 0, 0, 1, 1); 
	CtrlGrid.Add (c_LRhStoffe);
	CCtrlInfo *c_RhStoffe  = new CCtrlInfo (&m_RhStoffe, 1, 0, 1, 1); 
	CtrlGrid.Add (c_RhStoffe);

	CCtrlInfo *c_LHerstellung  = new CCtrlInfo (&m_LHerstellung, 0, 2, 1, 1); 
	CtrlGrid.Add (c_LHerstellung);
	CCtrlInfo *c_Herstellung  = new CCtrlInfo (&m_Herstellung, 1, 2, 1, 1); 
	CtrlGrid.Add (c_Herstellung);

	CCtrlInfo *c_LZbrEmpf  = new CCtrlInfo (&m_LZbrEmpf, 0, 4, 1, 1); 
	CtrlGrid.Add (c_LZbrEmpf);
	CCtrlInfo *c_ZbrEmpf  = new CCtrlInfo (&m_ZbrEmpf, 1, 4, 1, 1); 
	CtrlGrid.Add (c_ZbrEmpf);

	CCtrlInfo *c_LBlgEmpf  = new CCtrlInfo (&m_LBlgEmpf, 0, 6, 1, 1); 
	CtrlGrid.Add (c_LBlgEmpf);
	CCtrlInfo *c_BlgEmpf  = new CCtrlInfo (&m_BlgEmpf, 1, 6, 1, 1); 
	CtrlGrid.Add (c_BlgEmpf);

	CCtrlInfo *c_LDklPflicht  = new CCtrlInfo (&m_LDklPflicht, 0, 8, 3, 1); 
	CtrlGrid.Add (c_LDklPflicht);
	CCtrlInfo *c_DklPflicht  = new CCtrlInfo (&m_DklPflicht, 0, 9, DOCKRIGHT, DOCKBOTTOM); 
	c_DklPflicht->BottomSpace = 20;
	CtrlGrid.Add (c_DklPflicht);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_LDklPflicht.SetFont (&BoldFont);

    Read ();
	Form.Show ();
	CtrlGrid.Display ();
	return FALSE;
}

HBRUSH CHkstInfo::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
		    if (pWnd == &m_LDklPflicht)
			{
				pDC->SetTextColor (BoldColor);
			}
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDbPropertyPage::OnCtlColor (pDC, pWnd,nCtlColor);
}


void CHkstInfo::OnSize(UINT nType, int cx, int cy)
{
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
}

BOOL CHkstInfo::PreTranslateMessage(MSG* pMsg)
{
	CWnd *cWnd = NULL;

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (GetFocus () != &m_DklPflicht &&
					GetFocus ()->GetParent () != &m_DklPflicht )
				{
					return FALSE;
				}
				m_DklPflicht.OnKeyD (VK_RETURN);
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_DklPflicht &&
					GetFocus ()->GetParent () != &m_DklPflicht )
				{

					break;
			    }
				m_DklPflicht.OnKeyD (VK_TAB);
				return TRUE;
			}

			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}
			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}
				break;
			}

			else if (pMsg->wParam == VK_F5)
			{
				Update->Back ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				Delete ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				Update->Write ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
			}
			else if (pMsg->wParam == VK_F9)
			{
				return TRUE;
			}
	}
	return FALSE;
}

BOOL CHkstInfo::Read ()
{

	if (!ProductPage5Read)
	{
		return FALSE;
	}

/*
	if (name_cursor == -1)
	{
		return FALSE;
	}
*/
	memcpy (&Hst_info.hst_info, &hst_info_null, sizeof (HST_INFO));
	Hst_info.hst_info.a = A_bas->a_bas.a;
	Hst_info.dbreadfirst ();
	m_DklPflicht.StopEnter ();
	m_DklPflicht.DeleteAllItems ();
	m_DklPflicht.vSelect.clear ();
	m_DklPflicht.ListRows.Init ();
	int i = 0;

	memcpy (&Prp_zusatz.prp_zusatz, &prp_zusatz_null, sizeof (PRP_ZUSATZ));
	Prp_zusatz.prp_zusatz.a  = A_bas->a_bas.a;
	Prp_zusatz.dbreadfirst ();
	ToRecordNames ();

	for (i = 0; i < MAXENTRIES; i ++)
	{
		if (ZusNames[i] != "")
		{
			FillList.InsertItem (i, 0);
			FillList.SetItemText (ZusNames[i].GetBuffer (), i, 1);
			CString value = ZusValues[i];
			value.TrimRight ();
			if (value != "")
			{
				FillList.SetItemText (value.GetBuffer (), i, 2);
			}
			else
			{
				FillList.SetItemText (_T(""), i, 2);
			}
		}
	}
	if (m_DklPflicht.GetItemCount () == 0)
	{
		FillList.InsertItem (0, 0);
	}


/*
	while (Prp_zusatz.sqlfetch (name_cursor) == 0)
	{
		FillList.InsertItem (i, 0);
		FillList.SetItemText (Prp_zusatz.prp_zusatz.name, i, 1);
		if (Prp_zusatz.dbreadfirst () == 0)
		{
			FillList.SetItemText (Prp_zusatz.prp_zusatz.wert, i, 2);
		}
		else
		{
			FillList.SetItemText (_T(""), i, 2);
		}
		i ++;
	}
*/
//	m_DklPflicht.StartEnter (1, 0);
	Form.Show ();
	return TRUE;
}

void CHkstInfo::FillNames ()
{
	for (int i = 0; i < MAXENTRIES; i ++)
	{
		ZusNames[i] = ZusRecordNames[i];
		ZusNames[i].Trim ();
	}
}

void CHkstInfo::ToRecordNames ()
{
	for (int i = 0; i < MAXENTRIES; i ++)
	{
		if (ZusNames[i] != "")
		{
			strncpy (ZusRecordNames[i], ZusNames[i].GetBuffer (), 48);
		}
	}
}

void CHkstInfo::FromRecordNames ()
{
	for (int i = 0; i < MAXENTRIES; i ++)
	{
		CString name = ZusRecordNames[i];
		name.Trim ();
//		if (name != "")
		{
			ZusNames[i] = name;
		}
	}
}

BOOL CHkstInfo::Write ()
{
	extern short sql_mode;
	short sql_s;

	if (!ProductPage5Read)
	{
		return FALSE;
	}

/*
	if (delcursor == -1)
	{
		return FALSE;
	}
*/
	Form.Get ();
	Hst_info.dbupdate ();
	sql_s = sql_mode;
	sql_mode = 1;
//	Prp_zusatz.sqlexecute (delcursor);
	m_DklPflicht.StopEnter ();
	int count = m_DklPflicht.GetItemCount ();
	memcpy (&Prp_zusatz.prp_zusatz, &prp_zusatz_null, sizeof (PRP_ZUSATZ));
	Prp_zusatz.prp_zusatz.a  = A_bas->a_bas.a;
	for (int i = 0, j = 0; i < count; i ++)
	{
         CString Name;
		 Name = m_DklPflicht.GetItemText (i, 1);
		 Name.TrimRight ();
		 if (Name.GetLength () == 0)
		 {
			 strcpy (ZusRecordNames[j], ""); 
			 continue;
		 }
		 strncpy (ZusRecordNames[j], Name.GetBuffer (), 48); 
         CString Wert;
		 Wert = m_DklPflicht.GetItemText (i, 2);
		 Wert.TrimRight ();
		 strncpy (ZusValues[j], Wert.GetBuffer (), 511); 
		 j ++;
	}
	Prp_zusatz.dbupdate ();

	Prp_zusatz.prp_zusatz.a  = -1.0;
	FromRecordNames ();
	Prp_zusatz.dbupdate ();

/*
	for (int i = 0; i < count; i ++)
	{
         CString Name;
		 Name = m_DklPflicht.GetItemText (i, 1);
		 Name.TrimRight ();
		 if (Name.GetLength () == 0)
		 {
			 continue;
		 }
         CString Wert;
		 Wert = m_DklPflicht.GetItemText (i, 2);
		 Wert.TrimRight ();
		 if (Wert.GetLength () == 0)
		 {
			 continue;
		 }
		 strcpy (Prp_zusatz.prp_zusatz.name, Name.GetBuffer ());
		 strcpy (Prp_zusatz.prp_zusatz.wert, Wert.GetBuffer ());
		 Prp_zusatz.dbupdate ();
	}
*/
	return TRUE;
}

BOOL CHkstInfo::OnSetActive ()
{
	if (Basis != NULL)
	{
		Basis->Form.Get ();
	}
	DlgBkColor = Basis->DlgBkColor;
    DeleteObject (DlgBrush);
    DlgBrush = NULL;
	Read ();
	return TRUE;
}

BOOL CHkstInfo::OnKillActive ()
{
	Write ();
	return TRUE;
}

BOOL CHkstInfo::Delete ()
{
    FillList.SetItemText ( _T(""), m_DklPflicht.EditRow, 2);
	if (m_DklPflicht.EditCol == 2 && 
		m_DklPflicht.ListEdit.m_hWnd != NULL)
	{
		m_DklPflicht.ListEdit.SetWindowText (_T(""));
	}
    return TRUE;
}

void CHkstInfo::UpdatePage ()
{
	Read ();
}

BOOL CHkstInfo::OnReturn ()
{
	CWnd *Control = GetFocus ();

	return FALSE;
}

BOOL CHkstInfo::OnKeydown ()
{
	CWnd *Control = GetFocus ();

	return FALSE;
}

BOOL CHkstInfo::OnKeyup ()
{
	CWnd *Control = GetFocus ();

	return FALSE;
}

void CHkstInfo::OnDelete ()
{
}


void CHkstInfo::OnCopy ()
{
	CWnd *Control = GetFocus ();

	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CHkstInfo::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}
