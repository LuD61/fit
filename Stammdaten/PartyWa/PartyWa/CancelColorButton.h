#pragma once
#include "colorbutton.h"
#include "Resource.h"

class CCancelColorButton :
	public CColorButton
{
public:
	CCancelColorButton(void);
public:
	~CCancelColorButton(void);
    void Init ();
private:
	BOOL Initialized;
	BOOL IsInizializing;
protected:
	virtual void DrawItem (LPDRAWITEMSTRUCT  lpDrawItemStruct);
};
