#ifndef _KUN_LIST_DEF
#define _KUN_LIST_DEF
#pragma once

class CKunList
{
public:
	short mdn;
	long kun;
	CString adr_krz;
	CString kun_bran2;
	CString adr_nam1;
	CString adr_nam2;
	CString str;
	CString plz;
	CString ort1;
	CString tel;
	CString fax;
	CString email;
	short staat;
	short land;
	short kun_typ;
	CKunList(void);
	CKunList(short, long, LPTSTR, LPTSTR);
	~CKunList(void);
};
#endif
