#pragma once
#include "token.h"

class CWrapper :
	public CToken
{
public:
	CWrapper(void);
    CWrapper (char * buffer, char *separator);
    CWrapper (CString& string, char *separator);
    CWrapper (CString& string, int LineLength);
    CWrapper (CString& string, char *separator, int LineLength);
public:
	~CWrapper(void);
protected :
	int LineLength;
public:
	void SetLineLength (int LineLength)
	{
		this->LineLength = LineLength;
	}
	virtual void GetTokens (char *Txt);
	BOOL IsSeparator (char c);
};
