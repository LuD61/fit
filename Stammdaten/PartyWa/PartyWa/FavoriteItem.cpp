#include "StdAfx.h"
#include "FavoriteItem.h"

CFavoriteItem::CFavoriteItem(void)
{
	Name = _T("");
	ProgrammCall = _T("");
}

CFavoriteItem::CFavoriteItem(LPTSTR Name, LPTSTR ProgrammCall)
{
	this->Name = Name;
	this->ProgrammCall = ProgrammCall;
}

CFavoriteItem::~CFavoriteItem(void)
{
}
