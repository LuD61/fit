#include "StdAfx.h"
#include "PersCollection.h"

CPersCollection::CPersCollection(void)
{
}

CPersCollection::~CPersCollection(void)
{
}

PERS *CPersCollection::Find (LPSTR pers)
{
	PERS **it;
	PERS *tpers;
	CString Pers;
    CString CPers; 
	Pers = pers;
	Pers.TrimRight ();
	for (int i = 0; i < anz; i ++)
	{
		it = Get (i);
		if (it != NULL)
		{
			tpers = *it;
			CPers = tpers->pers;
            CPers.TrimRight ();     
			if (CPers == Pers)
			{
				return tpers;
			}
		}
	}
	return NULL;
}

int CPersCollection::FindPosition (LPSTR pers)
{
	PERS **it;
	PERS *tpers;
	CString Pers;
    CString CPers; 
	Pers = pers;
	Pers.TrimRight ();
	for (int i = 0; i < anz; i ++)
	{
		it = Get (i);
		if (it != NULL)
		{
			tpers = *it;
			CPers = tpers->pers;
            CPers.TrimRight ();     
			if (CPers == Pers)
			{
				return i;
			}
		}
	}
	return -1;
}

void CPersCollection::Add (PERS *pers)
{
	PERS *pers_element;
	pers_element = Find (pers->pers);
    if (pers_element == NULL)
	{
		pers_element = new PERS ();
		CDataCollection<PERS *>::Add (pers_element);
	}
	memcpy (pers_element, pers, sizeof (PERS));
}

void CPersCollection::Drop (LPSTR pers)
{
	PERS *tpers = Find (pers);
	if (tpers != NULL)
	{
		CDataCollection <PERS *>::Drop (tpers);
		delete tpers;
	}
}

PERS *CPersCollection::GetElementAt (int idx)
{
	if (idx >= anz || idx < 0)
	{
		return NULL;
	}
	PERS **it = Get (idx);
	if (it != NULL)
	{
		return *it;
	}
	return NULL;
}
