#pragma once
#include "CtrlGrid.h"
#include "afxwin.h"
#include "RunMessage.h"
#include "MessageHandler.h"
#include "AngebotPage.h"
#include "AuftragPage.h"
#include "CtrlGrid.h"
#include "Label.h"
#include "VLabel.h"
#include "ChoicePartyk.h"
#include "Partyk.h"
#include "Datatables.h"
#include "AngebotCore.h"

// CAngebotView-Formularansicht

class CAngebotView : public CFormView
{
	DECLARE_DYNCREATE(CAngebotView)

protected:
	CAngebotView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CAngebotView();

	class CAngebotMessage : public CRunMessage
	{
	private: 
		CAngebotView *p;
	public: 
			CAngebotMessage (CAngebotView *p);
			virtual void Run ();
	};

	class CAuftragMessage : public CRunMessage
	{
	private: 
		CAngebotView *p;
	public: 
			CAuftragMessage (CAngebotView *p);
			virtual void Run ();
	};

	class CAngTypesMessage : public CRunMessage
	{
	private: 
		CAngebotView *p;
	public: 
			CAngTypesMessage (CAngebotView *p);
			virtual void Run ();
	};

	class CKAuftraegeMessage : public CRunMessage
	{
	private: 
		CAngebotView *p;
	public: 
			CKAuftraegeMessage (CAngebotView *p);
			virtual void Run ();
	};

	class CTakeAng : public CRunMessage
	{
	private: 
		CAngebotView *p;
	public: 
			CTakeAng (CAngebotView *p);
			virtual void Run ();
	};

public:
	enum { IDD = IDD_ANGEBOT };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()

private:
	CMessageHandler *MessageHandler;
	CAngebotMessage *AngebotMessage;
	CAuftragMessage *AuftragMessage;
	CAngTypesMessage *AngTypesMessage;
	CKAuftraegeMessage *KAuftraegeMessage;
	CTakeAng *TakeAng;
	CChoicePartyk *ChoiceAng;
	CChoicePartyk *ChoiceAuf;
	CFont Font;
	CFont TitleFont;
	int FontHeight;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	HBRUSH TitleBrush;
	CCtrlGrid TitleGrid;
	CCtrlGrid AngebotGrid;
	CCtrlGrid AuftragGrid;
	CCtrlGrid CtrlGrid;
	CVLabel m_AngebotTitle;
	CAngebotPage m_AngebotPage;
	CAuftragPage m_AuftragPage;
	CString AngChoiceAnd;
	BOOL FromResource;
	CAngebotCore::PAGE Page;

public:

	void OnAngebot ();
	void OnAuftrag ();
	void OnAngTypes ();
	void OnKAuftraege ();
	void OnTakeAng ();
};



