#pragma once
#include <vector>
#include <atlimage.h>
#include "editlistctrlcolor.h"
#include "A_bas.h"
#include "ChoiceA.h"
#include "Vector.h"
#include "AngebotCore.h"
#include "DataCollection.h"
#include "DataTables.h"

#define IDC_TAKE_ANG 6100
#define MAXLISTROWS 30

class CPartyListCtrl :
	public CEditListCtrlColor
{
protected:
	DECLARE_MESSAGE_MAP()
public:
	enum LISTPOS
	{
		POSPOS,
		POSSTAT,
		POSA,
		POSABZ1,
		POSME,
		POSEINH,
		POSBASINH,
		POSBASME,
		POSBASEINH,
		POSGEBINDE,
		POSLASTVK,
		POSVK,
		POSVKBRUTTO,
		POSVKME,
		POSLDPR,
		POSEINZELPR,
	};

    int *Position[20];
	CImage check;
	CImage uncheck;
	int Aufschlag;
	int Mode;
	int MaxComboEntries;
	short m_Mdn;
	short mdn;
    long pr_gr_stuf;
	int oldsel;
	std::vector<BOOL> vSelect;
	CChoiceA *ChoiceA;
	BOOL ModalChoiceA;
	BOOL AChoiceStat;
	BOOL NoTestForAppend;
//	CVector ListRows;
	CAngebotCore *AngebotCore;
	CDatatables *Datatables;
	CVector EinhCombo;
	CDataCollection<PARTYP *> *List;

	A_BAS_CLASS *A_bas;
	CPartyListCtrl(void);
	~CPartyListCtrl(void);
	virtual void FirstEnter ();
	virtual void StartEnter (int, int);
	virtual void StopEnter ();
	virtual void OnReturn ();
	virtual void NextRow ();
	virtual void PriorRow ();
	virtual void NextCol ();
	virtual void PriorCol ();
	virtual void FormatText (CString&);
	virtual void SetSel (CString&);
	virtual BOOL InsertRow ();
	virtual BOOL DeleteRow ();
	virtual BOOL AppendEmpty ();
	virtual void RunItemClicked (int);
	virtual void RunCtrlItemClicked (int);
	virtual void RunShiftItemClicked (int);
	virtual void HiLightItem (int);
	void OnAChoice (CString&);
	void OnChoice ();
	void OnKey9 ();
    BOOL ReadABz1 ();
	void FillEmptyRow ();
	void BeforeEinh (int row);
	void SetList ();
	void GetList ();
	void AppendList ();
	void InsertList ();
	void FillEinhCombo ();
	void RefreshPositions ();
	void RefreshPrices ();
    void GetColValue (int row, int col, CString& Text);
    void TestPartypIndex ();
	void ScrollPositions (int pos);
	void DestroyRows(CVector &Rows);
	BOOL LastCol ();
    afx_msg void OnPaint( );
    afx_msg void OnCloseUp( );
	void OnASelected ();
	void OnACanceled ();
	void EnterGebinde ();
	void OnPosText ();
};
