#pragma once
#include "afxwin.h"


// CNewAngDialog-Dialogfeld

class CNewAngDialog : public CDialog
{
	DECLARE_DYNAMIC(CNewAngDialog)

public:
	CNewAngDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CNewAngDialog();

// Dialogfelddaten
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
    afx_msg void OnOK ();
private:
	CString Name;
	CEdit m_NewAng;
public:
	CString& GetName ()
	{
		return Name;
	}
};
