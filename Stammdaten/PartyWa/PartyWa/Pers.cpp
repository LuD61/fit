#include "stdafx.h"
#include "pers.h"

struct PERS pers, pers_null;

void PERS_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &pers.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &pers.fil,  SQLSHORT, 0);
            sqlin ((char *)    pers.pers,   SQLCHAR, sizeof (pers.pers));
    sqlout ((short *) &pers.abt,SQLSHORT,0);
    sqlout ((long *) &pers.adr,SQLLONG,0);
    sqlout ((short *) &pers.anz_tag_wo,SQLSHORT,0);
    sqlout ((double *) &pers.arb_zeit_tag,SQLDOUBLE,0);
    sqlout ((double *) &pers.arb_zeit_wo,SQLDOUBLE,0);
    sqlout ((TCHAR *) pers.beschaef_kz,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &pers.eintr_dat,SQLDATE,0);
    sqlout ((short *) &pers.fil,SQLSHORT,0);
    sqlout ((short *) &pers.kost_st,SQLSHORT,0);
    sqlout ((short *) &pers.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) pers.pers,SQLCHAR,13);
    sqlout ((long *) &pers.pers_mde,SQLLONG,0);
    sqlout ((double *) &pers.pers_pkt,SQLDOUBLE,0);
    sqlout ((double *) &pers.rab_proz,SQLDOUBLE,0);
    sqlout ((double *) &pers.std_satz,SQLDOUBLE,0);
    sqlout ((TCHAR *) pers.taet,SQLCHAR,25);
    sqlout ((TCHAR *) pers.taet_kz,SQLCHAR,2);
    sqlout ((short *) &pers.tar_gr,SQLSHORT,0);
    sqlout ((TCHAR *) pers.zahlw,SQLCHAR,2);
    sqlout ((long *) &pers.txt_nr,SQLLONG,0);
    sqlout ((short *) &pers.delstatus,SQLSHORT,0);
            cursor = sqlcursor (_T("select pers.abt,  pers.adr,  ")
_T("pers.anz_tag_wo,  pers.arb_zeit_tag,  pers.arb_zeit_wo,  ")
_T("pers.beschaef_kz,  pers.eintr_dat,  pers.fil,  pers.kost_st,  pers.mdn,  ")
_T("pers.pers,  pers.pers_mde,  pers.pers_pkt,  pers.rab_proz,  ")
_T("pers.std_satz,  pers.taet,  pers.taet_kz,  pers.tar_gr,  pers.zahlw,  ")
_T("pers.txt_nr,  pers.delstatus from pers ")

#line 14 "Pers.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and pers = ? ")
				);
    sqlin ((short *) &pers.abt,SQLSHORT,0);
    sqlin ((long *) &pers.adr,SQLLONG,0);
    sqlin ((short *) &pers.anz_tag_wo,SQLSHORT,0);
    sqlin ((double *) &pers.arb_zeit_tag,SQLDOUBLE,0);
    sqlin ((double *) &pers.arb_zeit_wo,SQLDOUBLE,0);
    sqlin ((TCHAR *) pers.beschaef_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &pers.eintr_dat,SQLDATE,0);
    sqlin ((short *) &pers.fil,SQLSHORT,0);
    sqlin ((short *) &pers.kost_st,SQLSHORT,0);
    sqlin ((short *) &pers.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) pers.pers,SQLCHAR,13);
    sqlin ((long *) &pers.pers_mde,SQLLONG,0);
    sqlin ((double *) &pers.pers_pkt,SQLDOUBLE,0);
    sqlin ((double *) &pers.rab_proz,SQLDOUBLE,0);
    sqlin ((double *) &pers.std_satz,SQLDOUBLE,0);
    sqlin ((TCHAR *) pers.taet,SQLCHAR,25);
    sqlin ((TCHAR *) pers.taet_kz,SQLCHAR,2);
    sqlin ((short *) &pers.tar_gr,SQLSHORT,0);
    sqlin ((TCHAR *) pers.zahlw,SQLCHAR,2);
    sqlin ((long *) &pers.txt_nr,SQLLONG,0);
    sqlin ((short *) &pers.delstatus,SQLSHORT,0);
            sqltext = _T("update pers set pers.abt = ?,  ")
_T("pers.adr = ?,  pers.anz_tag_wo = ?,  pers.arb_zeit_tag = ?,  ")
_T("pers.arb_zeit_wo = ?,  pers.beschaef_kz = ?,  pers.eintr_dat = ?,  ")
_T("pers.fil = ?,  pers.kost_st = ?,  pers.mdn = ?,  pers.pers = ?,  ")
_T("pers.pers_mde = ?,  pers.pers_pkt = ?,  pers.rab_proz = ?,  ")
_T("pers.std_satz = ?,  pers.taet = ?,  pers.taet_kz = ?,  ")
_T("pers.tar_gr = ?,  pers.zahlw = ?,  pers.txt_nr = ?,  ")
_T("pers.delstatus = ? ")

#line 19 "Pers.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and pers = ? ");
            sqlin ((short *)   &pers.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &pers.fil,  SQLSHORT, 0);
            sqlin ((char *)    pers.pers,   SQLCHAR, sizeof (pers.pers));
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &pers.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &pers.fil,  SQLSHORT, 0);
            sqlin ((char *)    pers.pers,   SQLCHAR, sizeof (pers.pers));
            test_upd_cursor = sqlcursor (_T("select pers from pers ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and pers = ? ")
				);
            sqlin ((short *)   &pers.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &pers.fil,  SQLSHORT, 0);
            sqlin ((char *)    pers.pers,   SQLCHAR, sizeof (pers.pers));
            test_lock_cursor = sqlcursor (_T("update pers set delstatus = -1 ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and pers = ? ")
				);
            sqlin ((short *)   &pers.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &pers.fil,  SQLSHORT, 0);
            sqlin ((char *)    pers.pers,   SQLCHAR, sizeof (pers.pers));
            del_cursor = sqlcursor (_T("delete from pers ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and pers = ? ")
				);
    sqlin ((short *) &pers.abt,SQLSHORT,0);
    sqlin ((long *) &pers.adr,SQLLONG,0);
    sqlin ((short *) &pers.anz_tag_wo,SQLSHORT,0);
    sqlin ((double *) &pers.arb_zeit_tag,SQLDOUBLE,0);
    sqlin ((double *) &pers.arb_zeit_wo,SQLDOUBLE,0);
    sqlin ((TCHAR *) pers.beschaef_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &pers.eintr_dat,SQLDATE,0);
    sqlin ((short *) &pers.fil,SQLSHORT,0);
    sqlin ((short *) &pers.kost_st,SQLSHORT,0);
    sqlin ((short *) &pers.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) pers.pers,SQLCHAR,13);
    sqlin ((long *) &pers.pers_mde,SQLLONG,0);
    sqlin ((double *) &pers.pers_pkt,SQLDOUBLE,0);
    sqlin ((double *) &pers.rab_proz,SQLDOUBLE,0);
    sqlin ((double *) &pers.std_satz,SQLDOUBLE,0);
    sqlin ((TCHAR *) pers.taet,SQLCHAR,25);
    sqlin ((TCHAR *) pers.taet_kz,SQLCHAR,2);
    sqlin ((short *) &pers.tar_gr,SQLSHORT,0);
    sqlin ((TCHAR *) pers.zahlw,SQLCHAR,2);
    sqlin ((long *) &pers.txt_nr,SQLLONG,0);
    sqlin ((short *) &pers.delstatus,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into pers (")
_T("abt,  adr,  anz_tag_wo,  arb_zeit_tag,  arb_zeit_wo,  beschaef_kz,  eintr_dat,  ")
_T("fil,  kost_st,  mdn,  pers,  pers_mde,  pers_pkt,  rab_proz,  std_satz,  taet,  taet_kz,  ")
_T("tar_gr,  zahlw,  txt_nr,  delstatus) ")

#line 52 "Pers.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 54 "Pers.rpp"
}
