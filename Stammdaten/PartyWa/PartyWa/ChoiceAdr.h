#ifndef _CHOICEADR_DEF
#define _CHOICEADR_DEF

#include "ChoiceX.h"
#include "AdrList.h"
#include <vector>

class CChoiceAdr : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
        static int Sort7;
      
    public :
		long m_Adr;
		int m_AdrTyp;
		CString m_AndPart;
		void set_AndPart (LPTSTR AndPart)
		{
			m_AndPart = AndPart;
		}

		CString& AndPart ()
		{
			return m_AndPart;
		}

	    std::vector<CAdrList *> AdrList;
      	CChoiceAdr(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceAdr(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchCol (CListCtrl *, LPTSTR, int col);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CAdrList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
