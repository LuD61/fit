#include "stdafx.h"
#include "lsp.h"

struct LSP lsp, lsp_null;

void LSP_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &lsp.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lsp.fil,  SQLSHORT, 0);
            sqlin ((long *)   &lsp.ls,   SQLLONG, 0);
            sqlin ((long *)   &lsp.posi,   SQLLONG, 0);
            sqlin ((double *) &lsp.a,   SQLDOUBLE, 0);
    sqlout ((short *) &lsp.mdn,SQLSHORT,0);
    sqlout ((short *) &lsp.fil,SQLSHORT,0);
    sqlout ((long *) &lsp.ls,SQLLONG,0);
    sqlout ((double *) &lsp.a,SQLDOUBLE,0);
    sqlout ((double *) &lsp.auf_me,SQLDOUBLE,0);
    sqlout ((char *) lsp.auf_me_bz,SQLCHAR,7);
    sqlout ((double *) &lsp.lief_me,SQLDOUBLE,0);
    sqlout ((char *) lsp.lief_me_bz,SQLCHAR,7);
    sqlout ((double *) &lsp.ls_vk_pr,SQLDOUBLE,0);
    sqlout ((double *) &lsp.ls_lad_pr,SQLDOUBLE,0);
    sqlout ((short *) &lsp.delstatus,SQLSHORT,0);
    sqlout ((double *) &lsp.tara,SQLDOUBLE,0);
    sqlout ((long *) &lsp.posi,SQLLONG,0);
    sqlout ((long *) &lsp.lsp_txt,SQLLONG,0);
    sqlout ((short *) &lsp.pos_stat,SQLSHORT,0);
    sqlout ((short *) &lsp.sa_kz_sint,SQLSHORT,0);
    sqlout ((char *) lsp.erf_kz,SQLCHAR,2);
    sqlout ((double *) &lsp.prov_satz,SQLDOUBLE,0);
    sqlout ((short *) &lsp.leer_pos,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &lsp.hbk_date,SQLDATE,0);
    sqlout ((char *) lsp.ls_charge,SQLCHAR,31);
    sqlout ((double *) &lsp.auf_me_vgl,SQLDOUBLE,0);
    sqlout ((double *) &lsp.ls_vk_euro,SQLDOUBLE,0);
    sqlout ((double *) &lsp.ls_vk_fremd,SQLDOUBLE,0);
    sqlout ((double *) &lsp.ls_lad_euro,SQLDOUBLE,0);
    sqlout ((double *) &lsp.ls_lad_fremd,SQLDOUBLE,0);
    sqlout ((double *) &lsp.rab_satz,SQLDOUBLE,0);
    sqlout ((short *) &lsp.me_einh_kun,SQLSHORT,0);
    sqlout ((short *) &lsp.me_einh,SQLSHORT,0);
    sqlout ((short *) &lsp.me_einh_kun1,SQLSHORT,0);
    sqlout ((double *) &lsp.auf_me1,SQLDOUBLE,0);
    sqlout ((double *) &lsp.inh1,SQLDOUBLE,0);
    sqlout ((short *) &lsp.me_einh_kun2,SQLSHORT,0);
    sqlout ((double *) &lsp.auf_me2,SQLDOUBLE,0);
    sqlout ((double *) &lsp.inh2,SQLDOUBLE,0);
    sqlout ((short *) &lsp.me_einh_kun3,SQLSHORT,0);
    sqlout ((double *) &lsp.auf_me3,SQLDOUBLE,0);
    sqlout ((double *) &lsp.inh3,SQLDOUBLE,0);
    sqlout ((char *) lsp.lief_me_bz_ist,SQLCHAR,7);
    sqlout ((double *) &lsp.inh_ist,SQLDOUBLE,0);
    sqlout ((short *) &lsp.me_einh_ist,SQLSHORT,0);
    sqlout ((double *) &lsp.me_ist,SQLDOUBLE,0);
    sqlout ((short *) &lsp.lager,SQLSHORT,0);
    sqlout ((double *) &lsp.lief_me1,SQLDOUBLE,0);
    sqlout ((double *) &lsp.lief_me2,SQLDOUBLE,0);
    sqlout ((double *) &lsp.lief_me3,SQLDOUBLE,0);
    sqlout ((short *) &lsp.ls_pos_kz,SQLSHORT,0);
    sqlout ((double *) &lsp.a_grund,SQLDOUBLE,0);
    sqlout ((char *) lsp.kond_art,SQLCHAR,5);
    sqlout ((long *) &lsp.posi_ext,SQLLONG,0);
    sqlout ((long *) &lsp.kun,SQLLONG,0);
    sqlout ((short *) &lsp.na_lief_kz,SQLSHORT,0);
    sqlout ((long *) &lsp.nve_posi,SQLLONG,0);
    sqlout ((short *) &lsp.teil_smt,SQLSHORT,0);
    sqlout ((short *) &lsp.aufschlag,SQLSHORT,0);
    sqlout ((double *) &lsp.aufschlag_wert,SQLDOUBLE,0);
    sqlout ((char *) lsp.ls_ident,SQLCHAR,21);
    sqlout ((short *) &lsp.pos_txt_kz,SQLSHORT,0);
            cursor = sqlcursor (_T("select lsp.mdn,  lsp.fil,  "
"lsp.ls,  lsp.a,  lsp.auf_me,  lsp.auf_me_bz,  lsp.lief_me,  lsp.lief_me_bz,  "
"lsp.ls_vk_pr,  lsp.ls_lad_pr,  lsp.delstatus,  lsp.tara,  lsp.posi,  "
"lsp.lsp_txt,  lsp.pos_stat,  lsp.sa_kz_sint,  lsp.erf_kz,  lsp.prov_satz,  "
"lsp.leer_pos,  lsp.hbk_date,  lsp.ls_charge,  lsp.auf_me_vgl,  "
"lsp.ls_vk_euro,  lsp.ls_vk_fremd,  lsp.ls_lad_euro,  lsp.ls_lad_fremd,  "
"lsp.rab_satz,  lsp.me_einh_kun,  lsp.me_einh,  lsp.me_einh_kun1,  "
"lsp.auf_me1,  lsp.inh1,  lsp.me_einh_kun2,  lsp.auf_me2,  lsp.inh2,  "
"lsp.me_einh_kun3,  lsp.auf_me3,  lsp.inh3,  lsp.lief_me_bz_ist,  "
"lsp.inh_ist,  lsp.me_einh_ist,  lsp.me_ist,  lsp.lager,  lsp.lief_me1,  "
"lsp.lief_me2,  lsp.lief_me3,  lsp.ls_pos_kz,  lsp.a_grund,  lsp.kond_art,  "
"lsp.posi_ext,  lsp.kun,  lsp.na_lief_kz,  lsp.nve_posi,  lsp.teil_smt,  "
"lsp.aufschlag,  lsp.aufschlag_wert,  lsp.ls_ident,  lsp.pos_txt_kz from lsp ")

#line 16 "lsp.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ls = ? ")
                                  _T("and posi = ? ")
                                  _T("and a = ? ")
				);
    sqlin ((short *) &lsp.mdn,SQLSHORT,0);
    sqlin ((short *) &lsp.fil,SQLSHORT,0);
    sqlin ((long *) &lsp.ls,SQLLONG,0);
    sqlin ((double *) &lsp.a,SQLDOUBLE,0);
    sqlin ((double *) &lsp.auf_me,SQLDOUBLE,0);
    sqlin ((char *) lsp.auf_me_bz,SQLCHAR,7);
    sqlin ((double *) &lsp.lief_me,SQLDOUBLE,0);
    sqlin ((char *) lsp.lief_me_bz,SQLCHAR,7);
    sqlin ((double *) &lsp.ls_vk_pr,SQLDOUBLE,0);
    sqlin ((double *) &lsp.ls_lad_pr,SQLDOUBLE,0);
    sqlin ((short *) &lsp.delstatus,SQLSHORT,0);
    sqlin ((double *) &lsp.tara,SQLDOUBLE,0);
    sqlin ((long *) &lsp.posi,SQLLONG,0);
    sqlin ((long *) &lsp.lsp_txt,SQLLONG,0);
    sqlin ((short *) &lsp.pos_stat,SQLSHORT,0);
    sqlin ((short *) &lsp.sa_kz_sint,SQLSHORT,0);
    sqlin ((char *) lsp.erf_kz,SQLCHAR,2);
    sqlin ((double *) &lsp.prov_satz,SQLDOUBLE,0);
    sqlin ((short *) &lsp.leer_pos,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &lsp.hbk_date,SQLDATE,0);
    sqlin ((char *) lsp.ls_charge,SQLCHAR,31);
    sqlin ((double *) &lsp.auf_me_vgl,SQLDOUBLE,0);
    sqlin ((double *) &lsp.ls_vk_euro,SQLDOUBLE,0);
    sqlin ((double *) &lsp.ls_vk_fremd,SQLDOUBLE,0);
    sqlin ((double *) &lsp.ls_lad_euro,SQLDOUBLE,0);
    sqlin ((double *) &lsp.ls_lad_fremd,SQLDOUBLE,0);
    sqlin ((double *) &lsp.rab_satz,SQLDOUBLE,0);
    sqlin ((short *) &lsp.me_einh_kun,SQLSHORT,0);
    sqlin ((short *) &lsp.me_einh,SQLSHORT,0);
    sqlin ((short *) &lsp.me_einh_kun1,SQLSHORT,0);
    sqlin ((double *) &lsp.auf_me1,SQLDOUBLE,0);
    sqlin ((double *) &lsp.inh1,SQLDOUBLE,0);
    sqlin ((short *) &lsp.me_einh_kun2,SQLSHORT,0);
    sqlin ((double *) &lsp.auf_me2,SQLDOUBLE,0);
    sqlin ((double *) &lsp.inh2,SQLDOUBLE,0);
    sqlin ((short *) &lsp.me_einh_kun3,SQLSHORT,0);
    sqlin ((double *) &lsp.auf_me3,SQLDOUBLE,0);
    sqlin ((double *) &lsp.inh3,SQLDOUBLE,0);
    sqlin ((char *) lsp.lief_me_bz_ist,SQLCHAR,7);
    sqlin ((double *) &lsp.inh_ist,SQLDOUBLE,0);
    sqlin ((short *) &lsp.me_einh_ist,SQLSHORT,0);
    sqlin ((double *) &lsp.me_ist,SQLDOUBLE,0);
    sqlin ((short *) &lsp.lager,SQLSHORT,0);
    sqlin ((double *) &lsp.lief_me1,SQLDOUBLE,0);
    sqlin ((double *) &lsp.lief_me2,SQLDOUBLE,0);
    sqlin ((double *) &lsp.lief_me3,SQLDOUBLE,0);
    sqlin ((short *) &lsp.ls_pos_kz,SQLSHORT,0);
    sqlin ((double *) &lsp.a_grund,SQLDOUBLE,0);
    sqlin ((char *) lsp.kond_art,SQLCHAR,5);
    sqlin ((long *) &lsp.posi_ext,SQLLONG,0);
    sqlin ((long *) &lsp.kun,SQLLONG,0);
    sqlin ((short *) &lsp.na_lief_kz,SQLSHORT,0);
    sqlin ((long *) &lsp.nve_posi,SQLLONG,0);
    sqlin ((short *) &lsp.teil_smt,SQLSHORT,0);
    sqlin ((short *) &lsp.aufschlag,SQLSHORT,0);
    sqlin ((double *) &lsp.aufschlag_wert,SQLDOUBLE,0);
    sqlin ((char *) lsp.ls_ident,SQLCHAR,21);
    sqlin ((short *) &lsp.pos_txt_kz,SQLSHORT,0);
            sqltext = _T("update lsp set lsp.mdn = ?,  "
"lsp.fil = ?,  lsp.ls = ?,  lsp.a = ?,  lsp.auf_me = ?,  "
"lsp.auf_me_bz = ?,  lsp.lief_me = ?,  lsp.lief_me_bz = ?,  "
"lsp.ls_vk_pr = ?,  lsp.ls_lad_pr = ?,  lsp.delstatus = ?,  "
"lsp.tara = ?,  lsp.posi = ?,  lsp.lsp_txt = ?,  lsp.pos_stat = ?,  "
"lsp.sa_kz_sint = ?,  lsp.erf_kz = ?,  lsp.prov_satz = ?,  "
"lsp.leer_pos = ?,  lsp.hbk_date = ?,  lsp.ls_charge = ?,  "
"lsp.auf_me_vgl = ?,  lsp.ls_vk_euro = ?,  lsp.ls_vk_fremd = ?,  "
"lsp.ls_lad_euro = ?,  lsp.ls_lad_fremd = ?,  lsp.rab_satz = ?,  "
"lsp.me_einh_kun = ?,  lsp.me_einh = ?,  lsp.me_einh_kun1 = ?,  "
"lsp.auf_me1 = ?,  lsp.inh1 = ?,  lsp.me_einh_kun2 = ?,  "
"lsp.auf_me2 = ?,  lsp.inh2 = ?,  lsp.me_einh_kun3 = ?,  "
"lsp.auf_me3 = ?,  lsp.inh3 = ?,  lsp.lief_me_bz_ist = ?,  "
"lsp.inh_ist = ?,  lsp.me_einh_ist = ?,  lsp.me_ist = ?,  "
"lsp.lager = ?,  lsp.lief_me1 = ?,  lsp.lief_me2 = ?,  "
"lsp.lief_me3 = ?,  lsp.ls_pos_kz = ?,  lsp.a_grund = ?,  "
"lsp.kond_art = ?,  lsp.posi_ext = ?,  lsp.kun = ?,  "
"lsp.na_lief_kz = ?,  lsp.nve_posi = ?,  lsp.teil_smt = ?,  "
"lsp.aufschlag = ?,  lsp.aufschlag_wert = ?,  lsp.ls_ident = ?,  "
"lsp.pos_txt_kz = ? ")

#line 23 "lsp.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ls = ? ")
                                  _T("and posi = ? ")
                                  _T("and a = ? ");
            sqlin ((short *)   &lsp.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lsp.fil,  SQLSHORT, 0);
            sqlin ((long *)   &lsp.ls,   SQLLONG, 0);
            sqlin ((long *)   &lsp.posi,   SQLLONG, 0);
            sqlin ((double *) &lsp.a,   SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &lsp.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lsp.fil,  SQLSHORT, 0);
            sqlin ((long *)   &lsp.ls,   SQLLONG, 0);
            sqlin ((long *)   &lsp.posi,   SQLLONG, 0);
            sqlin ((double *) &lsp.a,   SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select ls from lsp ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ls = ? ")
                                  _T("and posi = ? ")
                                  _T("and a = ? ")
				);
            sqlin ((short *)   &lsp.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lsp.fil,  SQLSHORT, 0);
            sqlin ((long *)   &lsp.ls,   SQLLONG, 0);
            sqlin ((long *)   &lsp.posi,   SQLLONG, 0);
            sqlin ((double *) &lsp.a,   SQLDOUBLE, 0);
            test_lock_cursor = sqlcursor (_T("update lsp set delstatus = -1 ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ls = ? ")
                                  _T("and posi = ? ")
                                  _T("and a = ? ")
				);
            sqlin ((short *)   &lsp.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lsp.fil,  SQLSHORT, 0);
            sqlin ((long *)   &lsp.ls,   SQLLONG, 0);
            sqlin ((long *)   &lsp.posi,   SQLLONG, 0);
            sqlin ((double *) &lsp.a,   SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from lsp ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ls = ? ")
                                  _T("and posi = ? ")
                                  _T("and a = ? ")
				);
    sqlin ((short *) &lsp.mdn,SQLSHORT,0);
    sqlin ((short *) &lsp.fil,SQLSHORT,0);
    sqlin ((long *) &lsp.ls,SQLLONG,0);
    sqlin ((double *) &lsp.a,SQLDOUBLE,0);
    sqlin ((double *) &lsp.auf_me,SQLDOUBLE,0);
    sqlin ((char *) lsp.auf_me_bz,SQLCHAR,7);
    sqlin ((double *) &lsp.lief_me,SQLDOUBLE,0);
    sqlin ((char *) lsp.lief_me_bz,SQLCHAR,7);
    sqlin ((double *) &lsp.ls_vk_pr,SQLDOUBLE,0);
    sqlin ((double *) &lsp.ls_lad_pr,SQLDOUBLE,0);
    sqlin ((short *) &lsp.delstatus,SQLSHORT,0);
    sqlin ((double *) &lsp.tara,SQLDOUBLE,0);
    sqlin ((long *) &lsp.posi,SQLLONG,0);
    sqlin ((long *) &lsp.lsp_txt,SQLLONG,0);
    sqlin ((short *) &lsp.pos_stat,SQLSHORT,0);
    sqlin ((short *) &lsp.sa_kz_sint,SQLSHORT,0);
    sqlin ((char *) lsp.erf_kz,SQLCHAR,2);
    sqlin ((double *) &lsp.prov_satz,SQLDOUBLE,0);
    sqlin ((short *) &lsp.leer_pos,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &lsp.hbk_date,SQLDATE,0);
    sqlin ((char *) lsp.ls_charge,SQLCHAR,31);
    sqlin ((double *) &lsp.auf_me_vgl,SQLDOUBLE,0);
    sqlin ((double *) &lsp.ls_vk_euro,SQLDOUBLE,0);
    sqlin ((double *) &lsp.ls_vk_fremd,SQLDOUBLE,0);
    sqlin ((double *) &lsp.ls_lad_euro,SQLDOUBLE,0);
    sqlin ((double *) &lsp.ls_lad_fremd,SQLDOUBLE,0);
    sqlin ((double *) &lsp.rab_satz,SQLDOUBLE,0);
    sqlin ((short *) &lsp.me_einh_kun,SQLSHORT,0);
    sqlin ((short *) &lsp.me_einh,SQLSHORT,0);
    sqlin ((short *) &lsp.me_einh_kun1,SQLSHORT,0);
    sqlin ((double *) &lsp.auf_me1,SQLDOUBLE,0);
    sqlin ((double *) &lsp.inh1,SQLDOUBLE,0);
    sqlin ((short *) &lsp.me_einh_kun2,SQLSHORT,0);
    sqlin ((double *) &lsp.auf_me2,SQLDOUBLE,0);
    sqlin ((double *) &lsp.inh2,SQLDOUBLE,0);
    sqlin ((short *) &lsp.me_einh_kun3,SQLSHORT,0);
    sqlin ((double *) &lsp.auf_me3,SQLDOUBLE,0);
    sqlin ((double *) &lsp.inh3,SQLDOUBLE,0);
    sqlin ((char *) lsp.lief_me_bz_ist,SQLCHAR,7);
    sqlin ((double *) &lsp.inh_ist,SQLDOUBLE,0);
    sqlin ((short *) &lsp.me_einh_ist,SQLSHORT,0);
    sqlin ((double *) &lsp.me_ist,SQLDOUBLE,0);
    sqlin ((short *) &lsp.lager,SQLSHORT,0);
    sqlin ((double *) &lsp.lief_me1,SQLDOUBLE,0);
    sqlin ((double *) &lsp.lief_me2,SQLDOUBLE,0);
    sqlin ((double *) &lsp.lief_me3,SQLDOUBLE,0);
    sqlin ((short *) &lsp.ls_pos_kz,SQLSHORT,0);
    sqlin ((double *) &lsp.a_grund,SQLDOUBLE,0);
    sqlin ((char *) lsp.kond_art,SQLCHAR,5);
    sqlin ((long *) &lsp.posi_ext,SQLLONG,0);
    sqlin ((long *) &lsp.kun,SQLLONG,0);
    sqlin ((short *) &lsp.na_lief_kz,SQLSHORT,0);
    sqlin ((long *) &lsp.nve_posi,SQLLONG,0);
    sqlin ((short *) &lsp.teil_smt,SQLSHORT,0);
    sqlin ((short *) &lsp.aufschlag,SQLSHORT,0);
    sqlin ((double *) &lsp.aufschlag_wert,SQLDOUBLE,0);
    sqlin ((char *) lsp.ls_ident,SQLCHAR,21);
    sqlin ((short *) &lsp.pos_txt_kz,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into lsp (mdn,  "
"fil,  ls,  a,  auf_me,  auf_me_bz,  lief_me,  lief_me_bz,  ls_vk_pr,  ls_lad_pr,  "
"delstatus,  tara,  posi,  lsp_txt,  pos_stat,  sa_kz_sint,  erf_kz,  prov_satz,  "
"leer_pos,  hbk_date,  ls_charge,  auf_me_vgl,  ls_vk_euro,  ls_vk_fremd,  "
"ls_lad_euro,  ls_lad_fremd,  rab_satz,  me_einh_kun,  me_einh,  "
"me_einh_kun1,  auf_me1,  inh1,  me_einh_kun2,  auf_me2,  inh2,  me_einh_kun3,  "
"auf_me3,  inh3,  lief_me_bz_ist,  inh_ist,  me_einh_ist,  me_ist,  lager,  "
"lief_me1,  lief_me2,  lief_me3,  ls_pos_kz,  a_grund,  kond_art,  posi_ext,  kun,  "
"na_lief_kz,  nve_posi,  teil_smt,  aufschlag,  aufschlag_wert,  ls_ident,  "
"pos_txt_kz) ")

#line 72 "lsp.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 74 "lsp.rpp"
}
