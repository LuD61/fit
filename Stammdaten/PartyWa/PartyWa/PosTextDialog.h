#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "AngebotCore.h"


// CPosTextDialog dialog

class CPosTextDialog : public CDialog
{
	DECLARE_DYNAMIC(CPosTextDialog)

public:
	enum TEXT_TYPE
	{
		Position,
		HeadFoot,
	};

	CPosTextDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPosTextDialog();

// Dialog Data
	enum { IDD = IDD_POS_TEXT_DIALOG };
	enum {
		LineMaxLength = 60
	};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog ();
	BOOL PreTranslateMessage(MSG* pMsg);
	BOOL OnReturn ();

	DECLARE_MESSAGE_MAP()
private:
	CString Text;
	long TextNr;
	CFormTab Form;
	CString Title;
	TEXT_TYPE TextType;
	CAngebotCore *AngebotCore;
	BOOL AutoWrap;
public:
	void SetAutoWrap (BOOL AutoWrap)
	{
		this->AutoWrap = AutoWrap;
	}

	BOOL GetAutoWrap ()
	{
		return AutoWrap;
	}

	CString& GetText ()
	{
		return Text;
	}

	void SetText (CString& Text)
	{
		this->Text = Text;
	}

	void SetText (LPTSTR Text)
	{
		this->Text = Text;
	}
	long GetTextNr ()
	{
		return TextNr;
	}

	void SetTextNr (long TextNr)
	{
		this->TextNr = TextNr;
	}

	CString& GetTitle ()
	{
		return Title;
	}

	void SetTitle (CString& Title)
	{
		this->Title = Title;
	}

	void SetTitle (LPTSTR Title)
	{
		this->Title = Title;
	}

	void SetTextType (TEXT_TYPE TextType)
	{
		this->TextType = TextType;
	}

	TEXT_TYPE getTextType ()
	{
		return TextType;
	}

public:
	CStatic m_LTextNr;
	CNumEdit m_TextNr;
	CRichEditCtrl m_Text;
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnEnChangeText();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnEditCut();
	afx_msg void OnEditUndo();
	afx_msg void OnEditDelete();
	afx_msg void OnEditSelectAll();
public:
	void OnTextRButtonDown (LPARAM lParam);
};
