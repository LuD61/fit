#include "stdafx.h"
#include "partyk.h"

struct PARTYK partyk, partyk_null;

void PARTYK_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &partyk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &partyk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &partyk.ang,   SQLLONG, 0);
    sqlout ((short *) &partyk.mdn,SQLSHORT,0);
    sqlout ((short *) &partyk.fil,SQLSHORT,0);
    sqlout ((long *) &partyk.ang,SQLLONG,0);
    sqlout ((long *) &partyk.auf,SQLLONG,0);
    sqlout ((long *) &partyk.ls,SQLLONG,0);
    sqlout ((long *) &partyk.rech,SQLLONG,0);
    sqlout ((long *) &partyk.adr,SQLLONG,0);
    sqlout ((short *) &partyk.kun_fil,SQLSHORT,0);
    sqlout ((long *) &partyk.kun,SQLLONG,0);
    sqlout ((DATE_STRUCT *) &partyk.lieferdat,SQLDATE,0);
    sqlout ((TCHAR *) partyk.lieferzeit,SQLCHAR,6);
    sqlout ((TCHAR *) partyk.hinweis,SQLCHAR,513);
    sqlout ((short *) &partyk.ang_stat,SQLSHORT,0);
    sqlout ((TCHAR *) partyk.kun_krz1,SQLCHAR,17);
    sqlout ((TCHAR *) partyk.feld_bz1,SQLCHAR,20);
    sqlout ((TCHAR *) partyk.feld_bz2,SQLCHAR,12);
    sqlout ((TCHAR *) partyk.feld_bz3,SQLCHAR,8);
    sqlout ((short *) &partyk.delstatus,SQLSHORT,0);
    sqlout ((double *) &partyk.zeit_dec,SQLDOUBLE,0);
    sqlout ((long *) &partyk.kopf_txt,SQLLONG,0);
    sqlout ((long *) &partyk.fuss_txt,SQLLONG,0);
    sqlout ((long *) &partyk.vertr,SQLLONG,0);
    sqlout ((TCHAR *) partyk.auf_ext,SQLCHAR,17);
    sqlout ((long *) &partyk.tou,SQLLONG,0);
    sqlout ((TCHAR *) partyk.pers_nam,SQLCHAR,9);
    sqlout ((DATE_STRUCT *) &partyk.komm_dat,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &partyk.best_dat,SQLDATE,0);
    sqlout ((short *) &partyk.waehrung,SQLSHORT,0);
    sqlout ((short *) &partyk.auf_art,SQLSHORT,0);
    sqlout ((short *) &partyk.gruppe,SQLSHORT,0);
    sqlout ((short *) &partyk.ccmarkt,SQLSHORT,0);
    sqlout ((short *) &partyk.fak_typ,SQLSHORT,0);
    sqlout ((long *) &partyk.tou_nr,SQLLONG,0);
    sqlout ((TCHAR *) partyk.ueb_kz,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &partyk.fix_dat,SQLDATE,0);
    sqlout ((TCHAR *) partyk.komm_name,SQLCHAR,13);
    sqlout ((DATE_STRUCT *) &partyk.akv,SQLDATE,0);
    sqlout ((TCHAR *) partyk.akv_zeit,SQLCHAR,6);
    sqlout ((DATE_STRUCT *) &partyk.bearb,SQLDATE,0);
    sqlout ((TCHAR *) partyk.bearb_zeit,SQLCHAR,6);
    sqlout ((short *) &partyk.anz_pers,SQLSHORT,0);
    sqlout ((short *) &partyk.einzel_preis,SQLSHORT,0);
    sqlout ((short *) &partyk.nur_servicepreis,SQLSHORT,0);
    sqlout ((short *) &partyk.psteuer_kz,SQLSHORT,0);
    sqlout ((double *) &partyk.ang_gew,SQLDOUBLE,0);
    sqlout ((double *) &partyk.ang_vk,SQLDOUBLE,0);
    sqlout ((double *) &partyk.pauschal_vk,SQLDOUBLE,0);
    sqlout ((double *) &partyk.einzel_vk,SQLDOUBLE,0);
    sqlout ((double *) &partyk.einzel_vk_pausch,SQLDOUBLE,0);
    sqlout ((short *) &partyk.mwst_voll,SQLSHORT,0);
    sqlout ((double *) &partyk.mwst_wert,SQLDOUBLE,0);
    sqlout ((double *) &partyk.mwst_proz,SQLDOUBLE,0);
    sqlout ((short *) &partyk.typ,SQLSHORT,0);
    sqlout ((TCHAR *) partyk.kommang,SQLCHAR,257);
    sqlout ((TCHAR *) partyk.kommrech,SQLCHAR,257);
    sqlout ((TCHAR *) partyk.kommintern,SQLCHAR,257);
    sqlout ((TCHAR *) partyk.anlass,SQLCHAR,257);
    sqlout ((TCHAR *) partyk.pers,SQLCHAR,13);
    sqlout ((TCHAR *) partyk.pers_name,SQLCHAR,17);
    sqlout ((short *) &partyk.abholung,SQLSHORT,0);
    sqlout ((short *) &partyk.hide_service,SQLSHORT,0);
    sqlout ((short *) &partyk.hide_me,SQLSHORT,0);
    sqlout ((double *) &partyk.service_pausch_vk,SQLDOUBLE,0);
    sqlout ((long *) &partyk.kommangnr,SQLLONG,0);
    sqlout ((long *) &partyk.kommrechnr,SQLLONG,0);
    sqlout ((long *) &partyk.komminternnr,SQLLONG,0);
    sqlout ((long *) &partyk.lief_adr,SQLLONG,0);
            cursor = sqlcursor (_T("select partyk.mdn,  ")
_T("partyk.fil,  partyk.ang,  partyk.auf,  partyk.ls,  partyk.rech,  partyk.adr,  ")
_T("partyk.kun_fil,  partyk.kun,  partyk.lieferdat,  partyk.lieferzeit,  ")
_T("partyk.hinweis,  partyk.ang_stat,  partyk.kun_krz1,  partyk.feld_bz1,  ")
_T("partyk.feld_bz2,  partyk.feld_bz3,  partyk.delstatus,  ")
_T("partyk.zeit_dec,  partyk.kopf_txt,  partyk.fuss_txt,  partyk.vertr,  ")
_T("partyk.auf_ext,  partyk.tou,  partyk.pers_nam,  partyk.komm_dat,  ")
_T("partyk.best_dat,  partyk.waehrung,  partyk.auf_art,  partyk.gruppe,  ")
_T("partyk.ccmarkt,  partyk.fak_typ,  partyk.tou_nr,  partyk.ueb_kz,  ")
_T("partyk.fix_dat,  partyk.komm_name,  partyk.akv,  partyk.akv_zeit,  ")
_T("partyk.bearb,  partyk.bearb_zeit,  partyk.anz_pers,  ")
_T("partyk.einzel_preis,  partyk.nur_servicepreis,  partyk.psteuer_kz,  ")
_T("partyk.ang_gew,  partyk.ang_vk,  partyk.pauschal_vk,  ")
_T("partyk.einzel_vk,  partyk.einzel_vk_pausch,  partyk.mwst_voll,  ")
_T("partyk.mwst_wert,  partyk.mwst_proz,  partyk.typ,  partyk.kommang,  ")
_T("partyk.kommrech,  partyk.kommintern,  partyk.anlass,  partyk.pers,  ")
_T("partyk.pers_name,  partyk.abholung,  partyk.hide_service,  ")
_T("partyk.hide_me,  partyk.service_pausch_vk,  partyk.kommangnr,  ")
_T("partyk.kommrechnr,  partyk.komminternnr,  partyk.lief_adr from partyk ")

#line 14 "partyk.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ang = ? ")
				);
    sqlin ((short *) &partyk.mdn,SQLSHORT,0);
    sqlin ((short *) &partyk.fil,SQLSHORT,0);
    sqlin ((long *) &partyk.ang,SQLLONG,0);
    sqlin ((long *) &partyk.auf,SQLLONG,0);
    sqlin ((long *) &partyk.ls,SQLLONG,0);
    sqlin ((long *) &partyk.rech,SQLLONG,0);
    sqlin ((long *) &partyk.adr,SQLLONG,0);
    sqlin ((short *) &partyk.kun_fil,SQLSHORT,0);
    sqlin ((long *) &partyk.kun,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &partyk.lieferdat,SQLDATE,0);
    sqlin ((TCHAR *) partyk.lieferzeit,SQLCHAR,6);
    sqlin ((TCHAR *) partyk.hinweis,SQLCHAR,513);
    sqlin ((short *) &partyk.ang_stat,SQLSHORT,0);
    sqlin ((TCHAR *) partyk.kun_krz1,SQLCHAR,17);
    sqlin ((TCHAR *) partyk.feld_bz1,SQLCHAR,20);
    sqlin ((TCHAR *) partyk.feld_bz2,SQLCHAR,12);
    sqlin ((TCHAR *) partyk.feld_bz3,SQLCHAR,8);
    sqlin ((short *) &partyk.delstatus,SQLSHORT,0);
    sqlin ((double *) &partyk.zeit_dec,SQLDOUBLE,0);
    sqlin ((long *) &partyk.kopf_txt,SQLLONG,0);
    sqlin ((long *) &partyk.fuss_txt,SQLLONG,0);
    sqlin ((long *) &partyk.vertr,SQLLONG,0);
    sqlin ((TCHAR *) partyk.auf_ext,SQLCHAR,17);
    sqlin ((long *) &partyk.tou,SQLLONG,0);
    sqlin ((TCHAR *) partyk.pers_nam,SQLCHAR,9);
    sqlin ((DATE_STRUCT *) &partyk.komm_dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &partyk.best_dat,SQLDATE,0);
    sqlin ((short *) &partyk.waehrung,SQLSHORT,0);
    sqlin ((short *) &partyk.auf_art,SQLSHORT,0);
    sqlin ((short *) &partyk.gruppe,SQLSHORT,0);
    sqlin ((short *) &partyk.ccmarkt,SQLSHORT,0);
    sqlin ((short *) &partyk.fak_typ,SQLSHORT,0);
    sqlin ((long *) &partyk.tou_nr,SQLLONG,0);
    sqlin ((TCHAR *) partyk.ueb_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &partyk.fix_dat,SQLDATE,0);
    sqlin ((TCHAR *) partyk.komm_name,SQLCHAR,13);
    sqlin ((DATE_STRUCT *) &partyk.akv,SQLDATE,0);
    sqlin ((TCHAR *) partyk.akv_zeit,SQLCHAR,6);
    sqlin ((DATE_STRUCT *) &partyk.bearb,SQLDATE,0);
    sqlin ((TCHAR *) partyk.bearb_zeit,SQLCHAR,6);
    sqlin ((short *) &partyk.anz_pers,SQLSHORT,0);
    sqlin ((short *) &partyk.einzel_preis,SQLSHORT,0);
    sqlin ((short *) &partyk.nur_servicepreis,SQLSHORT,0);
    sqlin ((short *) &partyk.psteuer_kz,SQLSHORT,0);
    sqlin ((double *) &partyk.ang_gew,SQLDOUBLE,0);
    sqlin ((double *) &partyk.ang_vk,SQLDOUBLE,0);
    sqlin ((double *) &partyk.pauschal_vk,SQLDOUBLE,0);
    sqlin ((double *) &partyk.einzel_vk,SQLDOUBLE,0);
    sqlin ((double *) &partyk.einzel_vk_pausch,SQLDOUBLE,0);
    sqlin ((short *) &partyk.mwst_voll,SQLSHORT,0);
    sqlin ((double *) &partyk.mwst_wert,SQLDOUBLE,0);
    sqlin ((double *) &partyk.mwst_proz,SQLDOUBLE,0);
    sqlin ((short *) &partyk.typ,SQLSHORT,0);
    sqlin ((TCHAR *) partyk.kommang,SQLCHAR,257);
    sqlin ((TCHAR *) partyk.kommrech,SQLCHAR,257);
    sqlin ((TCHAR *) partyk.kommintern,SQLCHAR,257);
    sqlin ((TCHAR *) partyk.anlass,SQLCHAR,257);
    sqlin ((TCHAR *) partyk.pers,SQLCHAR,13);
    sqlin ((TCHAR *) partyk.pers_name,SQLCHAR,17);
    sqlin ((short *) &partyk.abholung,SQLSHORT,0);
    sqlin ((short *) &partyk.hide_service,SQLSHORT,0);
    sqlin ((short *) &partyk.hide_me,SQLSHORT,0);
    sqlin ((double *) &partyk.service_pausch_vk,SQLDOUBLE,0);
    sqlin ((long *) &partyk.kommangnr,SQLLONG,0);
    sqlin ((long *) &partyk.kommrechnr,SQLLONG,0);
    sqlin ((long *) &partyk.komminternnr,SQLLONG,0);
    sqlin ((long *) &partyk.lief_adr,SQLLONG,0);
            sqltext = _T("update partyk set partyk.mdn = ?,  ")
_T("partyk.fil = ?,  partyk.ang = ?,  partyk.auf = ?,  partyk.ls = ?,  ")
_T("partyk.rech = ?,  partyk.adr = ?,  partyk.kun_fil = ?,  ")
_T("partyk.kun = ?,  partyk.lieferdat = ?,  partyk.lieferzeit = ?,  ")
_T("partyk.hinweis = ?,  partyk.ang_stat = ?,  partyk.kun_krz1 = ?,  ")
_T("partyk.feld_bz1 = ?,  partyk.feld_bz2 = ?,  partyk.feld_bz3 = ?,  ")
_T("partyk.delstatus = ?,  partyk.zeit_dec = ?,  partyk.kopf_txt = ?,  ")
_T("partyk.fuss_txt = ?,  partyk.vertr = ?,  partyk.auf_ext = ?,  ")
_T("partyk.tou = ?,  partyk.pers_nam = ?,  partyk.komm_dat = ?,  ")
_T("partyk.best_dat = ?,  partyk.waehrung = ?,  partyk.auf_art = ?,  ")
_T("partyk.gruppe = ?,  partyk.ccmarkt = ?,  partyk.fak_typ = ?,  ")
_T("partyk.tou_nr = ?,  partyk.ueb_kz = ?,  partyk.fix_dat = ?,  ")
_T("partyk.komm_name = ?,  partyk.akv = ?,  partyk.akv_zeit = ?,  ")
_T("partyk.bearb = ?,  partyk.bearb_zeit = ?,  partyk.anz_pers = ?,  ")
_T("partyk.einzel_preis = ?,  partyk.nur_servicepreis = ?,  ")
_T("partyk.psteuer_kz = ?,  partyk.ang_gew = ?,  partyk.ang_vk = ?,  ")
_T("partyk.pauschal_vk = ?,  partyk.einzel_vk = ?,  ")
_T("partyk.einzel_vk_pausch = ?,  partyk.mwst_voll = ?,  ")
_T("partyk.mwst_wert = ?,  partyk.mwst_proz = ?,  partyk.typ = ?,  ")
_T("partyk.kommang = ?,  partyk.kommrech = ?,  partyk.kommintern = ?,  ")
_T("partyk.anlass = ?,  partyk.pers = ?,  partyk.pers_name = ?,  ")
_T("partyk.abholung = ?,  partyk.hide_service = ?,  partyk.hide_me = ?,  ")
_T("partyk.service_pausch_vk = ?,  partyk.kommangnr = ?,  ")
_T("partyk.kommrechnr = ?,  partyk.komminternnr = ?,  ")
_T("partyk.lief_adr = ? ")

#line 19 "partyk.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ang = ? ");
            sqlin ((short *)   &partyk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &partyk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &partyk.ang,   SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &partyk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &partyk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &partyk.ang,   SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select ang from partyk ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ang = ? ")
				);
            sqlin ((short *)   &partyk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &partyk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &partyk.ang,   SQLLONG, 0);
            test_lock_cursor = sqlcursor (_T("update partyk set delstatus = -1 ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ang = ? ")
				);
            sqlin ((short *)   &partyk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &partyk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &partyk.ang,   SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from partyk ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ang = ? ")
				);
    sqlin ((short *) &partyk.mdn,SQLSHORT,0);
    sqlin ((short *) &partyk.fil,SQLSHORT,0);
    sqlin ((long *) &partyk.ang,SQLLONG,0);
    sqlin ((long *) &partyk.auf,SQLLONG,0);
    sqlin ((long *) &partyk.ls,SQLLONG,0);
    sqlin ((long *) &partyk.rech,SQLLONG,0);
    sqlin ((long *) &partyk.adr,SQLLONG,0);
    sqlin ((short *) &partyk.kun_fil,SQLSHORT,0);
    sqlin ((long *) &partyk.kun,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &partyk.lieferdat,SQLDATE,0);
    sqlin ((TCHAR *) partyk.lieferzeit,SQLCHAR,6);
    sqlin ((TCHAR *) partyk.hinweis,SQLCHAR,513);
    sqlin ((short *) &partyk.ang_stat,SQLSHORT,0);
    sqlin ((TCHAR *) partyk.kun_krz1,SQLCHAR,17);
    sqlin ((TCHAR *) partyk.feld_bz1,SQLCHAR,20);
    sqlin ((TCHAR *) partyk.feld_bz2,SQLCHAR,12);
    sqlin ((TCHAR *) partyk.feld_bz3,SQLCHAR,8);
    sqlin ((short *) &partyk.delstatus,SQLSHORT,0);
    sqlin ((double *) &partyk.zeit_dec,SQLDOUBLE,0);
    sqlin ((long *) &partyk.kopf_txt,SQLLONG,0);
    sqlin ((long *) &partyk.fuss_txt,SQLLONG,0);
    sqlin ((long *) &partyk.vertr,SQLLONG,0);
    sqlin ((TCHAR *) partyk.auf_ext,SQLCHAR,17);
    sqlin ((long *) &partyk.tou,SQLLONG,0);
    sqlin ((TCHAR *) partyk.pers_nam,SQLCHAR,9);
    sqlin ((DATE_STRUCT *) &partyk.komm_dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &partyk.best_dat,SQLDATE,0);
    sqlin ((short *) &partyk.waehrung,SQLSHORT,0);
    sqlin ((short *) &partyk.auf_art,SQLSHORT,0);
    sqlin ((short *) &partyk.gruppe,SQLSHORT,0);
    sqlin ((short *) &partyk.ccmarkt,SQLSHORT,0);
    sqlin ((short *) &partyk.fak_typ,SQLSHORT,0);
    sqlin ((long *) &partyk.tou_nr,SQLLONG,0);
    sqlin ((TCHAR *) partyk.ueb_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &partyk.fix_dat,SQLDATE,0);
    sqlin ((TCHAR *) partyk.komm_name,SQLCHAR,13);
    sqlin ((DATE_STRUCT *) &partyk.akv,SQLDATE,0);
    sqlin ((TCHAR *) partyk.akv_zeit,SQLCHAR,6);
    sqlin ((DATE_STRUCT *) &partyk.bearb,SQLDATE,0);
    sqlin ((TCHAR *) partyk.bearb_zeit,SQLCHAR,6);
    sqlin ((short *) &partyk.anz_pers,SQLSHORT,0);
    sqlin ((short *) &partyk.einzel_preis,SQLSHORT,0);
    sqlin ((short *) &partyk.nur_servicepreis,SQLSHORT,0);
    sqlin ((short *) &partyk.psteuer_kz,SQLSHORT,0);
    sqlin ((double *) &partyk.ang_gew,SQLDOUBLE,0);
    sqlin ((double *) &partyk.ang_vk,SQLDOUBLE,0);
    sqlin ((double *) &partyk.pauschal_vk,SQLDOUBLE,0);
    sqlin ((double *) &partyk.einzel_vk,SQLDOUBLE,0);
    sqlin ((double *) &partyk.einzel_vk_pausch,SQLDOUBLE,0);
    sqlin ((short *) &partyk.mwst_voll,SQLSHORT,0);
    sqlin ((double *) &partyk.mwst_wert,SQLDOUBLE,0);
    sqlin ((double *) &partyk.mwst_proz,SQLDOUBLE,0);
    sqlin ((short *) &partyk.typ,SQLSHORT,0);
    sqlin ((TCHAR *) partyk.kommang,SQLCHAR,257);
    sqlin ((TCHAR *) partyk.kommrech,SQLCHAR,257);
    sqlin ((TCHAR *) partyk.kommintern,SQLCHAR,257);
    sqlin ((TCHAR *) partyk.anlass,SQLCHAR,257);
    sqlin ((TCHAR *) partyk.pers,SQLCHAR,13);
    sqlin ((TCHAR *) partyk.pers_name,SQLCHAR,17);
    sqlin ((short *) &partyk.abholung,SQLSHORT,0);
    sqlin ((short *) &partyk.hide_service,SQLSHORT,0);
    sqlin ((short *) &partyk.hide_me,SQLSHORT,0);
    sqlin ((double *) &partyk.service_pausch_vk,SQLDOUBLE,0);
    sqlin ((long *) &partyk.kommangnr,SQLLONG,0);
    sqlin ((long *) &partyk.kommrechnr,SQLLONG,0);
    sqlin ((long *) &partyk.komminternnr,SQLLONG,0);
    sqlin ((long *) &partyk.lief_adr,SQLLONG,0);
            ins_cursor = sqlcursor (_T("insert into partyk (")
_T("mdn,  fil,  ang,  auf,  ls,  rech,  adr,  kun_fil,  kun,  lieferdat,  lieferzeit,  hinweis,  ")
_T("ang_stat,  kun_krz1,  feld_bz1,  feld_bz2,  feld_bz3,  delstatus,  zeit_dec,  ")
_T("kopf_txt,  fuss_txt,  vertr,  auf_ext,  tou,  pers_nam,  komm_dat,  best_dat,  ")
_T("waehrung,  auf_art,  gruppe,  ccmarkt,  fak_typ,  tou_nr,  ueb_kz,  fix_dat,  ")
_T("komm_name,  akv,  akv_zeit,  bearb,  bearb_zeit,  anz_pers,  einzel_preis,  ")
_T("nur_servicepreis,  psteuer_kz,  ang_gew,  ang_vk,  pauschal_vk,  einzel_vk,  ")
_T("einzel_vk_pausch,  mwst_voll,  mwst_wert,  mwst_proz,  typ,  kommang,  ")
_T("kommrech,  kommintern,  anlass,  pers,  pers_name,  abholung,  hide_service,  ")
_T("hide_me,  service_pausch_vk,  kommangnr,  kommrechnr,  komminternnr,  ")
_T("lief_adr) ")

#line 52 "partyk.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 54 "partyk.rpp"
}
