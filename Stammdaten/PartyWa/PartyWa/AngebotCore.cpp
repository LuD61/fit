#include "StdAfx.h"
#include "AngebotCore.h"
#include "DbUniCode.h"
#include "StrFuncs.h"
#include "Process.h"
#include "Token.h"
#include "DbUnicode.h"
#include "DbTime.h"
#include "KunDiverse.h"
#include "Util.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define RSDIR "RSDIR"

#define AUFKC Datatables->GetAufk ()
#define AUFKT Datatables->GetAufk ()->aufk
#define AUFPC Datatables->GetAufp ()
#define AUFPT Datatables->GetAufp ()->aufp

#define LSKC Datatables->GetLsk ()
#define LSKT Datatables->GetLsk ()->lsk
#define LSPC Datatables->GetLsp ()
#define LSPT Datatables->GetLsp ()->lsp
#define LIEF_RETBAC Datatables->GetLiefRetBa ()
#define LIEF_RETBAT Datatables->GetLiefRetBa ()->lief_retba

#define KUNDIVERSE CKunDiverse::GetInstance ()

CAngebotCore *CAngebotCore::Instance = NULL;

CAngebotCore *CAngebotCore::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CAngebotCore ();
	}
	return Instance;
}

void CAngebotCore ::DeleteInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

CAngebotCore::CAngebotCore(void)
{
	Datatables = CDatatables::GetInstance ();
	Partyk = Datatables->GetPartyk ();
	Partyp = Datatables->GetPartyp ();
	Aufk   = Datatables->GetAufk ();
	Lsk   = Datatables->GetLsk ();
	A_bas = Datatables->GetABas ();
	Mdn = Datatables->GetMdn ();
	MdnAdr = Datatables->GetMdnAdr ();
	Fil = Datatables->GetFil ();
	FilAdr = Datatables->GetFilAdr ();
	Kun = Datatables->GetKun ();
	KunAdr = Datatables->GetKunAdr ();
	Kmb = Datatables->GetKmb ();
	Angpt = Datatables->GetAngpt ();
	Ls_txt = Datatables->GetLsTxt ();
	AutoNr = new AUTO_CLASS ();
	PartykTest = new PARTYK_CLASS ();
	Ptabn = new PTABN_CLASS ();
	List = new CDataCollection<PARTYP *>;
	DelList = new CDataCollection<long>;
	CalcSmtTab = new CDataCollection<CCalcSmt *>;
	ShowCalcSmt = new CDataCollection<CRunMessage *>;
	RepaintCalcSmt = new CDataCollection<CRunMessage *>;
	ABasList = new CAbasCollection;
	PersList = new CPersCollection;
	LastEks = new CDataCollection<double> ();
	ShowLastEks = FALSE;
	Stammdaten = NULL;
	Warenausgang = NULL;
	IsPartyService = TRUE;
	IsVatFull = FALSE;
	m_VatFull = 0.0;
	m_VatReduced = 0.0;
	AngGew = 0.0;
	AngVk = 0.0;
	OSGPrice = 0.0;
	OSSPrice = 0.0;
	CalculatePauschSingle = FALSE;
	PosReaded = FALSE;
	PersNam = _T("fit");
	CUtil::GetPersName (PersNam);
	Debug = _T("");
    KunToReadOnly = TRUE;

	Partyk->opendbase (_T("bws"));
	AngFormat = _T("519pa");
	AuftragFormat = _T("514pa");
	Psz01 = _T("psz01");
	LiefFormat = _T("531pa");
	RechFormat = _T("561pa");

	mdn = 1;
	fil = 0;
	kun_fil = 0;
	kun = 0;
	ang = 0;
	lieferdat.day = 1;
	lieferdat.month = 1;
	lieferdat.year = 1980;
	a = 0.0;
	AngNr = 0l;
	AufNr = 0l;
	ListRow = 0;
	Document = NULL;
	CalcWithTitle = TRUE;
	CalcForm = NULL;
	CalcFormAng = NULL;
	CalcFormAuf = NULL;
	Form = NULL;
	PartySelect = ActiveAngebot;
	Activate = NULL;
	ShowCalcView = NULL;
	HideOnlyCalcView = NULL;
	RepaintCalcForm = TRUE;
	HostIsFit = TRUE;
	ls_stat = 2;
	memcpy (&ListStruct, &partyp_null, sizeof (PARTYP));

    Partyp->sqlin ((short *) &Mdn->mdn.mdn, SQLSHORT, 0);
    Partyp->sqlin ((short *) &Fil->fil.fil, SQLSHORT, 0);
    Partyp->sqlin ((long *) &Partyk->partyk.ang, SQLLONG, 0);

    Partyp->sqlout ((short *)  &Partyp->partyp.mdn, SQLSHORT, 0);
    Partyp->sqlout ((short *)  &Partyp->partyp.fil, SQLSHORT, 0);
    Partyp->sqlout ((long *)   &Partyp->partyp.ang, SQLLONG, 0);
    Partyp->sqlout ((long *)   &Partyp->partyp.posi, SQLLONG, 0);
    Partyp->sqlout ((double *) &Partyp->partyp.a, SQLDOUBLE, 0);
	Partyp->sqlout ((long *)   &Partyp->partyp.row_id, SQLLONG, 0);
	ListCursor = Partyp->sqlcursor (_T("select mdn, fil, ang, posi, a, row_id ")
		                            _T("from partyp ")
									_T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and ang = ? ")
									_T("order by posi, a"));


    Partyp->sqlin ((long *) &Partyk->partyk.auf, SQLLONG, 0);
    Partyp->sqlin ((short *) &Mdn->mdn.mdn, SQLSHORT, 0);
    Partyp->sqlin ((short *) &Fil->fil.fil, SQLSHORT, 0);
    Partyp->sqlin ((long *) &Partyk->partyk.ang, SQLLONG, 0);
	SetAufpUpdateCursor = Partyp->sqlcursor (_T("update partyp set auf = ? ")
		                                     _T("where mdn = ? ")
		                                     _T("and fil = ? ")
		                                     _T("and ang = ? "));

    Partyp->sqlin ((long *) &Partyk->partyk.ls, SQLLONG, 0);
    Partyp->sqlin ((short *) &Mdn->mdn.mdn, SQLSHORT, 0);
    Partyp->sqlin ((short *) &Fil->fil.fil, SQLSHORT, 0);
    Partyp->sqlin ((long *) &Partyk->partyk.ang, SQLLONG, 0);
	SetLiefUpdateCursor = Partyp->sqlcursor (_T("update partyp set ls = ? ")
		                                     _T("where mdn = ? ")
		                                     _T("and fil = ? ")
		                                     _T("and ang = ? "));

    Partyp->sqlin ((short *) &Mdn->mdn.mdn, SQLSHORT, 0);
    Partyp->sqlin ((short *) &Fil->fil.fil, SQLSHORT, 0);
    Partyp->sqlin ((long *) &Partyk->partyk.auf, SQLLONG, 0);
	DeletePosCursor = Partyp->sqlcursor (_T("delete from partyp ")
									_T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and ang = ? "));
    Partyp->sqlin ((short *) &Mdn->mdn.mdn, SQLSHORT, 0);
    Partyp->sqlin ((short *) &Fil->fil.fil, SQLSHORT, 0);
    Partyp->sqlin ((long *) &Partyk->partyk.auf, SQLLONG, 0);
	DeleteAufPosCursor = Partyp->sqlcursor (_T("delete from aufp ")
									_T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and auf = ? "));
    Partyp->sqlin ((short *) &Mdn->mdn.mdn, SQLSHORT, 0);
    Partyp->sqlin ((short *) &Fil->fil.fil, SQLSHORT, 0);
    Partyp->sqlin ((long *) &Partyk->partyk.ls, SQLLONG, 0);
	DeleteLiefPosCursor = Partyp->sqlcursor (_T("delete from lsp ")
									_T("where mdn = ? ")
									_T("and fil = ? ")
									_T("and ls = ? "));
	Ptabn->sqlout ((long *) &Ptabn->ptabn.ptlfnr, SQLLONG, 0);
	Ptabn->sqlin ((char *)   Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem));
	MaxAngTypCursor = Ptabn->sqlcursor (_T("select max (ptlfnr) from ptabn ")
		                                _T("where ptitem = ?"));
	Ptabn->sqlout ((char *)  Ptabn->ptabn.ptwert, SQLCHAR, sizeof (Ptabn->ptabn.ptwert));
	Ptabn->sqlin ((char *)   Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem));
	Ptabn->sqlin ((long *)   &Ptabn->ptabn.ptlfnr, SQLLONG, 0);
	PtabLfdCursor = Ptabn->sqlcursor (_T("select ptwert from ptabn ")
		                                _T("where ptitem = ? ")
		                                _T("and ptlfnr = ?"));

	memcpy (&PartykAng.partyk, &partyk_null, sizeof (PARTYK));
	memcpy (&MdnAng.mdn, &mdn_null, sizeof (MDN));
	memcpy (&FilAng.fil, &fil_null, sizeof (FIL));
	memcpy (&KunAng.kun, &kun_null, sizeof (KUN));
	memcpy (&MdnAdrAng.adr, &adr_null, sizeof (ADR));
	memcpy (&FilAdrAng.adr, &adr_null, sizeof (ADR));
	memcpy (&KunAdrAng.adr, &adr_null, sizeof (ADR));

	memcpy (&PartykAuf.partyk, &partyk_null, sizeof (PARTYK));
	memcpy (&MdnAuf.mdn, &mdn_null, sizeof (MDN));
	memcpy (&FilAuf.fil, &fil_null, sizeof (FIL));
	memcpy (&KunAuf.kun, &kun_null, sizeof (KUN));
	memcpy (&MdnAdrAuf.adr, &adr_null, sizeof (ADR));
	memcpy (&FilAdrAuf.adr, &adr_null, sizeof (ADR));
	memcpy (&KunAdrAuf.adr, &adr_null, sizeof (ADR));
}

CAngebotCore::~CAngebotCore(void)
{
	if (AutoNr != NULL)
	{
		delete AutoNr;
	}
	if (PartykTest != NULL)
	{
		delete PartykTest;
	}
	Partyp->sqlclose (ListCursor);
	Partyp->sqlclose (DeletePosCursor);
	Partyp->sqlclose (SetAufpUpdateCursor);
	Partyp->sqlclose (DeleteAufPosCursor);
	Ptabn->sqlclose (MaxAngTypCursor);
	Ptabn->sqlclose (PtabLfdCursor);
	delete Ptabn;
	List->DestroyElements ();
	List->Destroy ();
	DelList->Destroy ();
	CalcSmtTab->DestroyElements ();
	CalcSmtTab->Destroy ();
	ABasList->DestroyElements ();
	ABasList->Destroy ();
	PersList->DestroyElements ();
	PersList->Destroy ();
	if (LastEks != NULL)
	{
		delete LastEks;
	}
	delete List;
	delete DelList; 
	delete CalcSmtTab;
	delete ShowCalcSmt;
	delete RepaintCalcSmt;
	delete ABasList;
	delete PersList;
}

long CAngebotCore::CreateNumber ()
{
	AngNr = 0l;
	BOOL ret = FALSE;
	int tries = 0;

	if (Form != NULL)
	{
		Form->Get ();
		ret = AutoNr->GenAutoNr (Mdn->mdn.mdn, 0, _T("party_ang"));
		if (ret)
		{
			AngNr = AutoNr->Nr;
			while (ret && AngExist (AngNr) && (tries < MaxTries))
			{
				tries ++;
				Sleep (50);
				ret = AutoNr->GenAutoNr (Mdn->mdn.mdn, 0, _T("party_ang"));
				if (ret)
				{
					AngNr = AutoNr->Nr;
				}
			}
			if (!ret || (tries >= MaxTries))
			{
	
				AngNr = 0l;
			}
		}
	}
	return AngNr;
}

BOOL CAngebotCore::AngExist (long AngNr)
{
	PartykTest->partyk.mdn = Mdn->mdn.mdn;
	PartykTest->partyk.fil = Fil->fil.fil;
	PartykTest->partyk.ang = AngNr;
	if (PartykTest->dbreadfirst () == 0)
	{
		return TRUE;
	}
	return FALSE;
}

void CAngebotCore::FreeNumber ()
{
	if (Form != NULL)
	{
		Form->Get ();
		if (Partyk->partyk.ang != 0l)
		{
			Partyk->beginwork ();
			AutoNr->nveinid (Mdn->mdn.mdn, 0, _T("party_ang"), Partyk->partyk.ang);
			Partyk->commitwork ();
		}
	}
}

void CAngebotCore::FreeNumber (long nr)
{
	if (nr != 0l && AngNr != 0l && nr == AngNr)
	{
		Partyk->beginwork ();
		AutoNr->nveinid (Mdn->mdn.mdn, 0, _T("party_ang"), nr);
		Partyk->commitwork ();
	}
}


long CAngebotCore::CreateAufNr ()
{
	AufNr = 0l;
	BOOL ret = FALSE;
	int tries = 0;

	Form->Get ();
	ret = AutoNr->GenAutoNr (Mdn->mdn.mdn, 0, _T("auf"));
	if (ret)
	{
		AufNr = AutoNr->Nr;
		while (ret && AufExist (AufNr) && (tries < MaxTries))
		{
			tries ++;
			Sleep (50);
			ret = AutoNr->GenAutoNr (Mdn->mdn.mdn, 0, _T("auf"));
			if (ret)
			{
				AufNr = AutoNr->Nr;
			}
		}
		if (!ret || (tries >= MaxTries))
		{

			AufNr = 0l;
		}
	}
	return AufNr;
}

BOOL CAngebotCore::AufExist (long AufNr)
{
	Aufk->aufk.mdn = Mdn->mdn.mdn;
	Aufk->aufk.fil = Fil->fil.fil;
	Aufk->aufk.auf = AufNr;
	if (Aufk->dbreadfirst () == 0)
	{
		return TRUE;
	}
	return FALSE;
}

long CAngebotCore::CreateLiefNr ()
{
	LiefNr = 0l;
	BOOL ret = FALSE;
	int tries = 0;

	Form->Get ();
	ret = AutoNr->GenAutoNr (Mdn->mdn.mdn, 0, _T("ls"));
	if (ret)
	{
		LiefNr = AutoNr->Nr;
		while (ret && LiefExist (LiefNr) && (tries < MaxTries))
		{
			tries ++;
			Sleep (50);
			ret = AutoNr->GenAutoNr (Mdn->mdn.mdn, 0, _T("ls"));
			if (ret)
			{
				LiefNr = AutoNr->Nr;
			}
		}
		if (!ret || (tries >= MaxTries))
		{

			LiefNr = 0l;
		}
	}
	return LiefNr;
}

BOOL CAngebotCore::LiefExist (long LiefNr)
{
	Lsk->lsk.mdn = Mdn->mdn.mdn;
	Lsk->lsk.fil = Fil->fil.fil;
	Lsk->lsk.ls = LiefNr;
	if (Lsk->dbreadfirst () == 0)
	{
		return TRUE;
	}
	return FALSE;
}

long CAngebotCore::CreateRechNr ()
{
	RechNr = 0l;
	BOOL ret = FALSE;
	int tries = 0;

	Form->Get ();
	ret = AutoNr->GenAutoNr (Mdn->mdn.mdn, 0, _T("rech"));
	if (ret)
	{
		RechNr = AutoNr->Nr;
		while (ret && LiefExist (RechNr) && (tries < MaxTries))
		{
			tries ++;
			Sleep (50);
			ret = AutoNr->GenAutoNr (Mdn->mdn.mdn, 0, _T("rech"));
			if (ret)
			{
				RechNr = AutoNr->Nr;
			}
		}
		if (!ret || (tries >= MaxTries))
		{

			RechNr = 0l;
		}
	}
	return RechNr;
}

BOOL CAngebotCore::RechExist (long RechNr)
{
/*
	Lsk->lsk.mdn = Mdn->mdn.mdn;
	Lsk->lsk.fil = Fil->fil.fil;
	Lsk->lsk.rech = RechNr;
	if (Lsk->dbreadfirst () == 0)
	{
		return TRUE;
	}
*/
	return FALSE;
}

BOOL CAngebotCore::GetPrice ()
{
   int dsqlstatus;
   int ret = TRUE;
   char ldat[12];

   pr_brutto = 0.0;
   if (DllPreise.PriceLib != NULL && 
    DllPreise.preise_holen != NULL)
   {
	          memset (ldat, 0, sizeof (ldat));
	  	      sprintf (ldat,"%02hd.%02hd.%02hd", lieferdat.day,
	 							  			     lieferdat.month,
											     lieferdat.year);
			  dsqlstatus = (DllPreise.preise_holen) (mdn, 
                                      fil,
                                      kun_fil,
                                      kun,
									  a,
                                      ldat,
                                      &sa,
                                      &pr_ek,
                                      &pr_vk);
			  GetVat ();
			  dsqlstatus = (dsqlstatus == 1) ? 0 : 100;
			  if (dsqlstatus == 0)
			  {
				  ret = TRUE;
			  }
   }
   return ret;
}


void CAngebotCore::GetVat ()
{

	if (IsPartyService)
	{
		if (m_VatFull == 0.0)
		{
			memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
			strcpy ((LPSTR) Ptabn->ptabn.ptitem, "mwst");
			_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatFull);
			Ptabn->dbreadfirst ();
			m_VatFull = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
			memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
			_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatReduced);
			Ptabn->dbreadfirst ();
			m_VatReduced = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
		}
		if (A_bas->a_bas.mwst == VatFull || IsVatFull)
		{
			if (BruttoKunde) //FS-360
			{
				pr_netto = CStrFuncs::RoundToDouble (pr_ek , 2);
				if (m_VatFull)	pr_netto = CStrFuncs::RoundToDouble (pr_ek / m_VatFull, 2);
				pr_brutto = CStrFuncs::RoundToDouble (pr_ek , 2);
				pr_ek = pr_netto;
			}
			else
			{
				pr_netto = CStrFuncs::RoundToDouble (pr_ek , 2);
				pr_brutto = CStrFuncs::RoundToDouble (pr_ek * m_VatFull, 2);
			}
			if (!IsVatFull)   //in diesem Fall wird das H�ckchen volle Mwst gesetzt und alles auf volle MWST gestellt , wenn nur ein Artikel die volle mwst hat
			{
				PARTYP **it = List->Get (ListRow);
				PARTYP *p = NULL;
				if (it != NULL)
				{
                    p = *it;
					p->a = A_bas->a_bas.a;
					p->auf_vk_euro = pr_ek;
					p->auf_vk_brutto = pr_brutto;
					SetVatToBrutto ();
					IsVatFull = TRUE;
					Form->Show ();
				}
			}
		}
		else if (A_bas->a_bas.mwst == VatReduced)
		{
			if (BruttoKunde) //FS-360
			{
				pr_netto = CStrFuncs::RoundToDouble (pr_ek , 2);
				if (m_VatFull)	pr_netto = CStrFuncs::RoundToDouble (pr_ek / m_VatReduced, 2);
				pr_brutto = CStrFuncs::RoundToDouble (pr_ek , 2);
				pr_ek = pr_netto;
			}
			else
			{
				pr_netto = CStrFuncs::RoundToDouble (pr_ek , 2);
				pr_brutto = CStrFuncs::RoundToDouble (pr_ek * m_VatReduced, 2);
			}
		}
	}
}

void CAngebotCore::SetVatToBrutto ()
{
	PARTYP **it;
	PARTYP *p;
	if (m_VatFull == 0.0)
	{
		memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
		strcpy ((LPSTR) Ptabn->ptabn.ptitem, "mwst");
		_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatFull);
		Ptabn->dbreadfirst ();
		m_VatFull = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
		memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
		_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatReduced);
		Ptabn->dbreadfirst ();
		m_VatReduced = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
	}
	int i = 0;
	List->Start ();
	while ((it = List->GetNext ()) != NULL)
	{
		p = *it;
		p->mwst = VatFull;
		p->auf_vk_brutto = p->auf_vk_euro * m_VatFull;
		SetA (p->a);
		SetMeEinhKun (p->me_einh_kun);
		GetAktEinh ();
		double inh = GetInh ();
		inh = (inh == 0.0) ? 1 : inh;
		p->pos_vk = p->auf_vk_brutto * inh * p->auf_me;
	}
	Calculate ();
	Partyk->partyk.ang_gew = GetAngGew ();
	Partyk->partyk.ang_vk = GetAngVk ();
	Form->Show ();
}

void CAngebotCore::SetVatToNetto ()
{
	PARTYP **it;
	PARTYP *p;
	A_BAS_CLASS A_bas;
	if (m_VatFull == 0.0)
	{
		memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
		strcpy ((LPSTR) Ptabn->ptabn.ptitem, "mwst");
		_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatFull);
		Ptabn->dbreadfirst ();
		m_VatFull = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
		memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
		_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatReduced);
		Ptabn->dbreadfirst ();
		m_VatReduced = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
	}

	BOOL Full = FALSE;
	/** FS-360  Pr�fung entf�llt 
	List->Start ();
	while ((it = List->GetNext ()) != NULL)
	{
		p = *it;
		A_bas.a_bas.a = p->a;
		A_bas.dbreadfirst ();
		if (A_bas.a_bas.mwst == VatFull)
		{
			Full = TRUE;
			break;
		}
	}
	****/

	if (!Full)
	{
		List->Start ();
		while ((it = List->GetNext ()) != NULL)
		{
			p = *it;
			p->mwst = VatReduced;
			p->auf_vk_brutto = p->auf_vk_euro * m_VatReduced;
			SetA (p->a);
			SetMeEinhKun (p->me_einh_kun);
			GetAktEinh ();
			double inh = GetInh ();
			inh = (inh == 0.0) ? 1 : inh;
			p->pos_vk = p->auf_vk_brutto * inh * p->auf_me;
		}
		Calculate ();
		Partyk->partyk.ang_gew = GetAngGew ();
		Partyk->partyk.ang_vk = GetAngVk ();
		Partyk->partyk.mwst_voll = FALSE;
		IsVatFull = FALSE;
		Form->Show ();
	}
}

double CAngebotCore::CalculateVkBrutto (double vk_netto)
{
		if (m_VatFull == 0.0)
		{
			memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
			strcpy ((LPSTR) Ptabn->ptabn.ptitem, "mwst");
			_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatFull);
			Ptabn->dbreadfirst ();
			m_VatFull = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
			memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
			_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatReduced);
			Ptabn->dbreadfirst ();
			m_VatReduced = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
		}
		if (IsVatFull)
		{
			return vk_netto * m_VatFull;
		}
		else
		{
			return vk_netto * m_VatReduced;
		}
}

double CAngebotCore::CalculateVkNetto (double vk_brutto)
{
		if (m_VatFull == 0.0)
		{
			memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
			strcpy ((LPSTR) Ptabn->ptabn.ptitem, "mwst");
			_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatFull);
			Ptabn->dbreadfirst ();
			m_VatFull = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
			memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
			_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatReduced);
			Ptabn->dbreadfirst ();
			m_VatReduced = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
		}
		if (IsVatFull)
		{
			return vk_brutto / m_VatFull;
		}
		else
		{
			return vk_brutto / m_VatReduced;
		}
}

int CAngebotCore::GetActVat ()
{
	if (IsVatFull)
	{
		return VatFull;
	}
	else
	{
		return A_bas->a_bas.mwst;
	}
}

BOOL CAngebotCore::GetEinh ()
{
    int ret = TRUE;

    Einh.SetAufEinh (1);

    KEINHEIT keinheit;
	memset (&keinheit, 0, sizeof (KEINHEIT));

	if (IsPartyService)
	{
		Einh.GetKunEinhBas (a, &keinheit);
		Einh.GetBasEinh (a, &keinheit);
	}
	else
	{
		Einh.GetKunEinh (mdn, fil, kun, a, &keinheit);
	}
    BasisMeBz = (LPSTR) keinheit.me_einh_bas_bez;
    MeBz = (LPSTR) keinheit.me_einh_kun_bez;
    me_einh_kun = keinheit.me_einh_kun;
    me_einh = keinheit.me_einh_bas;
    inh = keinheit.inh;
	if (me_einh_kun == 0)
	{
		me_einh_kun = me_einh;
		MeBz = BasisMeBz;
		inh = 1.0;
	}
    me_einh_kun1 = keinheit.me_einh1;
    me_einh_kun2 = keinheit.me_einh2;
    me_einh_kun3 = keinheit.me_einh3;

    inh1 = keinheit.inh1;
    inh2 = keinheit.inh2;
    inh3 = keinheit.inh3;

    return ret;
}

BOOL CAngebotCore::GetAktEinh ()
{
    int ret = TRUE;

	Einh.AktAufEinh (mdn, fil, kun, a, me_einh_kun);

    KEINHEIT keinheit;
	memset (&keinheit, 0, sizeof (KEINHEIT));

	if (IsPartyService)
	{
		Einh.GetKunEinhBas (a, &keinheit);
		Einh.GetBasEinh (a, &keinheit);
	}
	else
	{
		Einh.GetKunEinh (mdn, fil, kun, a, &keinheit);
	}
    BasisMeBz = (LPSTR) keinheit.me_einh_bas_bez;
    MeBz = (LPSTR) keinheit.me_einh_kun_bez;
    me_einh_kun = keinheit.me_einh_kun;
    me_einh = keinheit.me_einh_bas;
    inh = keinheit.inh;
    me_einh_kun1 = keinheit.me_einh1;
    me_einh_kun2 = keinheit.me_einh2;
    me_einh_kun3 = keinheit.me_einh3;

    inh1 = keinheit.inh1;
    inh2 = keinheit.inh2;
    inh3 = keinheit.inh3;
	return ret;
}

void CAngebotCore::FillEinh (CVector *v)
{
	CString *c;
	v->FirstPosition ();
	while ((c = (CString *) v->GetNext ()) != NULL)
	{
		delete c;
	}
	v->Init ();
	Kmb->kumebest.mdn = mdn;
	Kmb->kumebest.fil = fil;
	Kmb->kumebest.kun = kun;
	Kmb->kumebest.a   = a;
	if (IsPartyService)
	{
		Einh.InitPartyCombo (v);
	}
	else
	{
		Einh.InitCombo (v);
	}
}

void CAngebotCore::FindEinhCombo (CString *Item, CVector *v)
{
	CString *c;
	v->FirstPosition ();
	while ((c = (CString *) v->GetNext ()) != NULL)
	{
		CString Sub = c->Left (Item->GetLength ());
		if (Sub == *Item)
		{
			*Item = *c;
			return;
		}
	}
	return;
}

void CAngebotCore::GetEinhBez (short me_einh)
{
  memcpy (&Ptabn->ptabn, &ptabn_null, sizeof (struct PTABN));
  _tcscpy (Ptabn->ptabn.ptitem, _T("me_einh_kun"));
  _stprintf (Ptabn->ptabn.ptwert, _T("%hd"), me_einh);
  Ptabn->dbreadfirst ();
}

void CAngebotCore::FindDeletedRows ()
{
	long row_id;
	long *it;
	if (DelList != NULL && List != NULL)
	{
		DelList->Start ();
		while ((it = DelList->GetNext ()) != NULL)
		{
			row_id = *it;
			if (!FindInList (row_id))
			{
				Datatables->GetPartyp ()->partyp.row_id = row_id;
				Datatables->GetPartyp ()->dbdelete ();
			}
		}
	}
}

BOOL CAngebotCore::FindInList (long row_id)
{
	PARTYP *p;
	PARTYP **it;

	List->Start ();
	while ((it = List->GetNext ()) != NULL)
	{
		p = *it;
		if (p->row_id == row_id)
		{
			return TRUE;
		}
	}
	return FALSE;
}


BOOL CAngebotCore::ReadPositions ()
{
	PARTYP *p;
	int i = 0;

//	RepaintCalcForm = TRUE;
	CalcSmtTab->DestroyElements ();

	AngGew = 0.0;
	AngVk = 0.0;
	OSGPrice = 0.0;
	OSSPrice = 0.0;

	ABasList->DestroyElements ();
	IsVatFull = FALSE;
	Partyk->partyk.mwst_voll = FALSE;
	List->DestroyElements ();
	DelList->Clear ();
	Partyp->sqlopen (ListCursor);
	while (Partyp->sqlfetch (ListCursor) == 0)
	{
		Partyp->dbreadfirst ();
		p = new PARTYP;
		memcpy (p, &Partyp->partyp, sizeof (PARTYP));
		if (p->mwst == VatFull)
		{
			IsVatFull = TRUE;
			Partyk->partyk.mwst_voll = TRUE;
		}
		List->Add (p);
		DelList->Add (p->row_id);
	}
	Form->Show ();
	Calculate ();
	PosReaded = TRUE;
	return TRUE;
}

BOOL CAngebotCore::WriteAng ()
{
	double mwst;
	if (Kun->kun.kun != 0l)
	{
		Partyk->partyk.kun = Kun->kun.kun;
	}
	if (KunAdr->adr.adr != 0l && Partyk->partyk.adr == 0l)
	{
		Partyk->partyk.adr = KunAdr->adr.adr;
	}
	Partyk->partyk.mwst_voll = IsVatFull;
	strcpy (Ptabn->ptabn.ptitem, "mwst");
	if (IsVatFull)
	{
		Partyk->partyk.psteuer_kz = 1;
		strcpy (Ptabn->ptabn.ptwert, "1");
	}
	else
	{
		Partyk->partyk.psteuer_kz = 0;
		strcpy (Ptabn->ptabn.ptwert, "2");
	}

	Calculate ();

	if (Partyk->partyk.pauschal_vk == 0.0)
	{
		Partyk->partyk.pauschal_vk = Partyk->partyk.ang_vk;
	}

	if (Partyk->partyk.einzel_vk_pausch == 0.0)
	{
		Partyk->partyk.einzel_vk_pausch = Partyk->partyk.einzel_vk;
	}

	if (Ptabn->dbreadfirst () == 0)
	{
		mwst = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
		Partyk->partyk.mwst_proz = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer1) * 100;
		Partyk->partyk.mwst_wert = Partyk->partyk.pauschal_vk -
                                   (Partyk->partyk.pauschal_vk / mwst); 
	}

	strcpy ((LPSTR) Partyk->partyk.kun_krz1, (LPSTR) KunAdr->adr.adr_krz);
	if (Partyk->partyk.lieferdat.day == 0)
	{
		Partyk->partyk.lieferdat.day = 1;
		Partyk->partyk.lieferdat.month = 1;
		Partyk->partyk.lieferdat.year = 1980;
	}
	if (Partyk->partyk.komm_dat.day == 0)
	{
		memcpy (&Partyk->partyk.komm_dat, &Partyk->partyk.lieferdat, sizeof (DATE_STRUCT));
	}
	if (Partyk->partyk.best_dat.day == 0)
	{
		memcpy (&Partyk->partyk.best_dat, &Partyk->partyk.lieferdat, sizeof (DATE_STRUCT));
	}
	if (Partyk->partyk.fix_dat.day == 0)
	{
		memcpy (&Partyk->partyk.fix_dat, &Partyk->partyk.lieferdat, sizeof (DATE_STRUCT));
	}
	if (Partyk->partyk.akv.day == 0)
	{
		DbTime date;
		date.GetDateStruct (&Partyk->partyk.akv);
	}
	if (Partyk->partyk.bearb.day == 0)
	{
		DbTime date;
		date.GetDateStruct (&Partyk->partyk.bearb);
	}
	if (PersIdx != -1)
	{
		PERS *tpers = PersList->GetElementAt (PersIdx);

		if (tpers != NULL)
		{
			strcpy (Partyk->partyk.pers,  tpers->pers);
			memset (Partyk->partyk.pers_name, 0, sizeof (Partyk->partyk.pers_name));
			strncpy (Partyk->partyk.pers_name, tpers->taet, sizeof (Partyk->partyk.pers_name) - 1);
		}
	}
	CDbUniCode::DbFromUniCode (PersNam, Partyk->partyk.pers_nam, sizeof (Partyk->partyk.pers_nam));
	Partyk->dbupdate ();
	WritePositions ();
	Partyk->commitwork ();
//	List->DestroyElements ();
//	DelList->Clear ();
	return TRUE;
}
BOOL CAngebotCore::WritePositions ()
{
	PARTYP *p;
	PARTYP **it;
	long posi = 10;

// Gel�schte S�tze in Datenbank akualisieren

	FindDeletedRows ();

	List->Start ();
	while ((it = List->GetNext ()) != NULL)
	{
		p = *it;
		memcpy (&Partyp->partyp, p, sizeof (PARTYP));
		if (Partyp->partyp.a != 0.0)
		{
			Partyp->partyp.posi = posi;
			Partyp->dbupdate ();
			posi += 10;
		}
	}
	return TRUE;
}

BOOL CAngebotCore::StartFaProg (CString& Name)
{
	CFavoriteItem *Item;
	CFavoriteItem **it;
	if (Stammdaten != NULL)
	{
		Stammdaten->Start ();
		while ((it = Stammdaten->GetNext ()) != NULL)
		{
			Item = *it;
			if (Name == Item->GetName ())
			{
				CString Progname = Item->GetProgrammCall ();
				return StartProg (Progname);
			}
		}
	}

	if (Warenausgang != NULL)
	{
		Warenausgang->Start ();
		while ((it = Warenausgang->GetNext ()) != NULL)
		{
			Item = *it;
			if (Name == Item->GetName ())
			{
				CString Progname = Item->GetProgrammCall ();
				return StartProg (Progname);
			}
		}
	}
    return FALSE;
}

BOOL CAngebotCore::StartProg (CString& Name)
{
	CProcess p;

	p.SetCommand (Name.GetBuffer ());
	HANDLE Pid = p.Start ();
	if (Pid != NULL)
	{
		return TRUE;
	}
	return FALSE;
}

void CAngebotCore::FillCombo (LPTSTR Item, std::vector<CString *> *ComboValues)
{
		ComboValues->clear ();
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptitem, SQLCHAR, sizeof (Ptabn->ptabn.ptitem)); 
		Ptabn->sqlout ((char *) Ptabn->ptabn.ptwert, SQLCHAR, sizeof (Ptabn->ptabn.ptwert)); 
		Ptabn->sqlout ((long *) &Ptabn->ptabn.ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptitem, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
        int cursor = Ptabn->sqlcursor (Sql.GetBuffer ());
		while (Ptabn->sqlfetch (cursor) == 0)
		{
			Ptabn->dbreadfirst ();

			LPSTR pos = (LPSTR) Ptabn->ptabn.ptwert;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptwert, pos);
			pos = (LPSTR) Ptabn->ptabn.ptbez;
			CDbUniCode::DbToUniCode (Ptabn->ptabn.ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), Ptabn->ptabn.ptwert,
				                             Ptabn->ptabn.ptbez);
		    ComboValues->push_back (ComboValue); 
		}
		Ptabn->sqlclose (cursor);
}

void CAngebotCore::FillPers (std::vector<CString *> *ComboValues)
{
		PersList->DestroyElements ();
		ComboValues->clear ();
		Pers.sqlout ((short *) &Pers.pers.mdn, SQLSHORT, 0); 
		Pers.sqlout ((short *) &Pers.pers.fil, SQLSHORT, 0); 
		Pers.sqlout ((char *)  Pers.pers.pers, SQLCHAR, sizeof (Pers.pers.pers)); 
		Pers.sqlout ((long *) &Pers.pers.adr, SQLLONG, 0); 
		Pers.sqlout ((char *) PersAdr.adr.adr_krz, SQLCHAR, sizeof (PersAdr.adr.adr_krz)); 
		CString Sql;
        Sql.Format (_T("select pers.mdn, pers.fil, pers.pers, pers.adr, adr.adr_krz from pers, adr ")
                     _T("where adr.adr = pers.adr ")
                     _T("and adr.adr_typ = 10 ")
				     _T("order by adr.adr_krz"));
        int cursor = Pers.sqlcursor (Sql.GetBuffer ());
		while (Pers.sqlfetch (cursor) == 0)
		{
			Pers.dbreadfirst (); 
			LPSTR pos = (LPSTR) PersAdr.adr.adr_krz;
			CDbUniCode::DbToUniCode (PersAdr.adr.adr_krz, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s"), PersAdr.adr.adr_krz);
		    ComboValues->push_back (ComboValue); 
			strcpy (Pers.pers.taet, PersAdr.adr.adr_krz);
			PersList->Add (&Pers.pers);
		}
		Pers.sqlclose (cursor);
}


void CAngebotCore::Calculate ()
{
	PARTYP *p;
	PARTYP **it;
	A_BAS *a_bas;
	A_BAS_CLASS *A_bas = new A_BAS_CLASS;

	AngGew = 0.0;
	AngVk = 0.0;
	OSGPrice = 0.0;
	OSSPrice = 0.0;
    
	if (m_VatFull == 0.0)
	{
		memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
		strcpy ((LPSTR) Ptabn->ptabn.ptitem, "mwst");
		_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatFull);
		Ptabn->dbreadfirst ();
		m_VatFull = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
		memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
		_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatReduced);
		Ptabn->dbreadfirst ();
		m_VatReduced = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
	}
	if (Partyk->partyk.nur_servicepreis)
	{
		List->Start ();
		while ((it = List->GetNext ()) != NULL)
		{
			p = *it;
			if (p != NULL && p->a != 0.0)
			{
				if (p->inh == 0.0)
				{
					p->inh = 1.0;
				}
				if (p->me_einh == 2)
				{
					p->lief_me = p->auf_me * p->inh;
					p->pos_gew = p->lief_me;
				}
				else if (p->me_einh != 2 && p->a_gew != 0.0)
				{
					p->lief_me = p->auf_me * p->inh;
					p->pos_gew = p->lief_me * p->a_gew;
				}
				else
				{
					p->lief_me = p->auf_me * p->inh;
				}
				AngGew += p->pos_gew;
				p->pos_vk = p->auf_vk_brutto * p->lief_me;
				p->pos_vk_nto = CStrFuncs::RoundToDouble (p->auf_vk_euro * m_VatReduced, 2);
				p->pos_vk_nto *= p->lief_me;
				AngVk += p->pos_vk;
				a_bas = ABasList->Find (p->a);
				if (a_bas == NULL)
				{
					A_bas->a_bas.a = p->a;
					A_bas->dbreadfirst ();
					ABasList->Add (&A_bas->a_bas);
					a_bas = ABasList->Find (p->a);
				}
				if (IsVatFull)
				{
					if (a_bas->mwst == VatReduced)
					{
						OSGPrice += p->pos_vk_nto;
					}
				}
				else
				{
					OSGPrice += p->pos_vk;
				}
			}
		}
	}
	else
	{
		List->Start ();
		while ((it = List->GetNext ()) != NULL)
		{
			p = *it;
			if (p != NULL && p->a != 0.0)
			{
				if (p->inh == 0.0)
				{
					p->inh = 1.0;
				}
				if (p->me_einh == 2)
				{
					p->lief_me = p->auf_me * p->inh;
					p->pos_gew = p->lief_me;
				}
				else if (p->me_einh != 2 && p->a_gew != 0.0)
				{
					p->lief_me = p->auf_me * p->inh;
					p->pos_gew = p->lief_me * p->a_gew;
				}
				else
				{
					p->lief_me = p->auf_me * p->inh;
				}
				AngGew += p->pos_gew;
				p->pos_vk = p->auf_vk_euro * p->lief_me;
				p->pos_vk_nto = CStrFuncs::RoundToDouble (p->auf_vk_euro, 2);
				p->pos_vk_nto *= p->lief_me;
				AngVk += p->pos_vk;
				a_bas = ABasList->Find (p->a);
				if (a_bas == NULL)
				{
					A_bas->a_bas.a = p->a;
					A_bas->dbreadfirst ();
					ABasList->Add (&A_bas->a_bas);
					a_bas = ABasList->Find (p->a);
				}
				if (a_bas->mwst == VatReduced)
				{
					OSGPrice += p->pos_vk;
				}
			}
		}
	}
	CalculateSmt ();
	if (Form != NULL && Partyk != NULL)
	{
		Form->Get ();
		Partyk->partyk.ang_gew = AngGew;
		Partyk->partyk.ang_vk  = AngVk;
		if (Partyk->partyk.anz_pers == 0)
		{
			Partyk->partyk.anz_pers = 1;
		}
		if (Partyk->partyk.anz_pers > 0)
		{
			Partyk->partyk.einzel_vk = Partyk->partyk.ang_vk / Partyk->partyk.anz_pers;
			OSSPrice = OSGPrice / Partyk->partyk.anz_pers;  

			if (CalculatePauschSingle)
			{
				Partyk->partyk.pauschal_vk = Partyk->partyk.einzel_vk_pausch * Partyk->partyk.anz_pers;
			}
			else
			{
				Partyk->partyk.einzel_vk_pausch = Partyk->partyk.pauschal_vk / Partyk->partyk.anz_pers;
			}
			if (Partyk->partyk.einzel_vk_pausch != 0.0)
			{
				Partyk->partyk.einzel_preis = 0;
			}
		}
		else
		{
				Partyk->partyk.einzel_vk = 0.0;
				Partyk->partyk.einzel_vk_pausch = 0.0;
		}
		SingleGew = Partyk->partyk.ang_gew / Partyk->partyk.anz_pers;
		PauschVk = Partyk->partyk.pauschal_vk + Partyk->partyk.service_pausch_vk;
		PauschSingleVk = PauschVk / Partyk->partyk.anz_pers;
		Form->Show ();
		if (CalcForm != NULL)
		{
			CalcForm->Show ();
		}
	}
	RepaintCalcForm = FALSE;
	delete A_bas;
}

void CAngebotCore::CalculateSmt ()
{
	PARTYP **it;
	PARTYP *p;
	CCalcSmt *cs;
	BOOL RepaintSmt = FALSE;

	CRunMessage **iShowSmt;
	CRunMessage *ShowSmt;

	short *itsmt;
	short smt;
	CSortCollection<short> SmtTab;
	List->Start ();
	while ((it = List->GetNext ()) != NULL)
	{
		p = *it;
		if (p != NULL)
		{
			if (p->teil_smt != 0)
			{
				SmtTab.Insert (p->teil_smt);
			}
		}
	}
	SmtTab.Start ();
	while ((itsmt = SmtTab.GetNext ()) != NULL)
	{
		smt = *itsmt;
		cs = FindSmt (smt);
		if (cs == NULL)
		{
			cs = new CCalcSmt (smt);
			cs->Calculate ();
			CalcSmtTab->Add (cs);
			RepaintSmt = TRUE;
		}
		else
		{
			cs->Calculate ();
		}
	}
	if (ClearCalcSmt (&SmtTab))
	{
		RepaintSmt = TRUE;
	}
	if (RepaintSmt || RepaintCalcForm)
	{
		RepaintCalcSmt->Start ();
		while ((iShowSmt = RepaintCalcSmt->GetNext ()) != NULL)
		{
			ShowSmt = *iShowSmt;
			ShowSmt->Run ();
		}
	}
	else
	{
		ShowCalcSmt->Start ();
		while ((iShowSmt = ShowCalcSmt->GetNext ()) != NULL)
		{
			ShowSmt = *iShowSmt;
			ShowSmt->Run ();
		}
	}
}

CCalcSmt *CAngebotCore::FindSmt (short smt)
{
	for (int i = 0; i < CalcSmtTab->anz; i ++)
	{
		CCalcSmt **it = CalcSmtTab->Get (i);
		if (it != NULL)
		{
			CCalcSmt *c = *it;
			if (c->GetSmt () == smt)
			{
				return c;
			}
		}
	}
	return NULL;
}


BOOL CAngebotCore::ClearCalcSmt (CSortCollection<short> * SmtTab)
{
	short *itsmt;
	short smt;
	BOOL ret = FALSE;

	for (int i = 0; i < CalcSmtTab->anz;)
	{
		SmtTab->Start ();
		CCalcSmt **it = CalcSmtTab->Get (i);
		if (it != NULL)
		{
			CCalcSmt *c = *it;
			while ((itsmt = SmtTab->GetNext ()) != NULL)
			{
				smt = *itsmt;
				if (smt == c->GetSmt ())
				{
					i ++;
					break;
				}
			}
			if (itsmt == NULL)
			{
				CalcSmtTab->Drop (i);
				if (it != NULL)
				{
					delete c;
				}
				ret = TRUE;
			}
		}
	}
	return ret;
}

void CAngebotCore::CalculateSmt (short smt)
{
}

void CAngebotCore::SetListStruct (int row)
{
	PARTYP **it;

	if (List != NULL && List->anz > row)
	{
		it = List->Get (row);
		if (it != NULL && *it != NULL)
		{
			memcpy (&ListStruct, *it, sizeof (PARTYP));
		}
	}
}

void CAngebotCore::Calculate (int row, PARTYP *values)
{
	PARTYP *p;
	PARTYP **it;
    KEINHEIT keinheit;
	double PosPreis;
	double PosPreisNto;

	if (m_VatFull == 0.0)
	{
		memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
		strcpy ((LPSTR) Ptabn->ptabn.ptitem, "mwst");
		_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatFull);
		Ptabn->dbreadfirst ();
		m_VatFull = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
		memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
		_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatReduced);
		Ptabn->dbreadfirst ();
		m_VatReduced = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
	}
	if (row < List->anz)
	{
		it = List->Get (row);
		p = *it;
		if (p->a != 0.0)
		{
			memset (&keinheit, 0, sizeof (KEINHEIT));
			if (IsPartyService)
			{
				Einh.GetKunEinhBas (p->a, &keinheit);
				Einh.GetBasEinh (p->a, &keinheit);
			}
			else
			{
				Einh.AktAufEinh (mdn, fil, kun, p->a, p->me_einh_kun);
				Einh.GetKunEinh (mdn, fil, kun, p->a, &keinheit);
			}
			me_einh_kun = keinheit.me_einh_kun;
			me_einh = keinheit.me_einh_bas;
			inh = keinheit.inh;
			if (inh == 0.0) inh = 1.0;
			inh *= p->auf_me;
			AngGew += inh;
			PosPreis = p->auf_vk_brutto * inh;
			PosPreisNto = p->auf_vk_euro * m_VatReduced * inh;
			AngVk += PosPreis;
			if (A_bas->a_bas.mwst == VatReduced)
			{
				OSGPrice += PosPreisNto;
			}
		}
	}
	if (Form != NULL && Partyk != NULL)
	{
		Form->Get ();
		Partyk->partyk.ang_gew = AngGew;
		Partyk->partyk.ang_vk  = AngVk;
		if (Partyk->partyk.anz_pers > 0.0)
		{
			Partyk->partyk.einzel_vk = Partyk->partyk.ang_vk / Partyk->partyk.anz_pers;
			OSSPrice = OSGPrice / Partyk->partyk.anz_pers;  

			if (CalculatePauschSingle)
			{
				Partyk->partyk.pauschal_vk = Partyk->partyk.einzel_vk_pausch * Partyk->partyk.anz_pers;
			}
			else
			{
				Partyk->partyk.einzel_vk_pausch = Partyk->partyk.pauschal_vk / Partyk->partyk.anz_pers;
			}
			if (Partyk->partyk.einzel_vk_pausch != 0.0)
			{
				Partyk->partyk.einzel_preis = 0;
			}
		}
		else
		{
				Partyk->partyk.einzel_vk = 0.0;
				Partyk->partyk.einzel_vk_pausch = 0.0;
		}
		SingleGew = Partyk->partyk.ang_gew / Partyk->partyk.anz_pers;
		Form->Show ();
		if (CalcForm != NULL)
		{
			CalcForm->Show ();
		}
	}
}

void CAngebotCore::Calculate (int row, int column)
{
	PARTYP *p;
	PARTYP **it;

	if (m_VatFull == 0.0)
	{
		memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
		strcpy ((LPSTR) Ptabn->ptabn.ptitem, "mwst");
		_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatFull);
		Ptabn->dbreadfirst ();
		m_VatFull = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
		memset (Ptabn->ptabn.ptwer1, 0, sizeof (Ptabn->ptabn.ptwer2));
		_stprintf (Ptabn->ptabn.ptwert, _T("%d"), VatReduced);
		Ptabn->dbreadfirst ();
		m_VatReduced = CStrFuncs::StrToDouble (Ptabn->ptabn.ptwer2);
	}

	if (row < List->anz)
	{
		it = List->Get (row);
		p = *it;
		if (p != NULL)
		{
			if (p->a != 0.0)
			{
				if (Partyk->partyk.nur_servicepreis)
				{	
					AngGew  -= ListStruct.pos_gew;
					AngVk    -= ListStruct.pos_vk;
					OSGPrice -= ListStruct.pos_vk_nto;
					if (p->inh == 0.0)
					{
						p->inh = 1.0;
					}
					if (p->lief_me == 0.0)
					{
						p->lief_me = p->auf_me * p->inh;
					}
					else if (column == POSBASINH)
					{
						p->lief_me = p->auf_me * p->inh;
					}
					else if (column == POSME)
					{
						p->lief_me = p->auf_me * p->inh;
					}
					p->pos_gew = p->lief_me;
					if (p->me_einh != 2 && p->a_gew != 0.0)
					{
						p->pos_gew *= p->a_gew;
					}
					AngGew += p->pos_gew;
					p->pos_vk = p->auf_vk_brutto * p->lief_me;
					p->pos_vk_nto = CStrFuncs::RoundToDouble (p->auf_vk_euro * m_VatReduced, 2);
					p->pos_vk_nto *= p->lief_me;
					AngVk += p->pos_vk;
					if (A_bas->a_bas.mwst == VatReduced)
					{
						OSGPrice += p->pos_vk_nto;
					}
				}
				else
				{
					AngGew  -= ListStruct.pos_gew;
					AngVk    -= ListStruct.pos_vk;
					OSGPrice -= ListStruct.pos_vk_nto;
					if (p->inh == 0.0)
					{
						p->inh = 1.0;
					}
					if (p->lief_me == 0.0)
					{
						p->lief_me = p->auf_me * p->inh;
					}
					else if (column == POSBASINH)
					{
						p->lief_me = p->auf_me * p->inh;
					}
					else if (column == POSME)
					{
						p->lief_me = p->auf_me * p->inh;
					}
					p->pos_gew = p->lief_me;
					if (p->me_einh != 2 && p->a_gew != 0.0)
					{
						p->pos_gew *= p->a_gew;
					}
					AngGew += p->pos_gew;
					p->pos_vk = p->auf_vk_euro * p->lief_me;
					p->pos_vk_nto = CStrFuncs::RoundToDouble (p->auf_vk_euro, 2);
					p->pos_vk_nto *= p->lief_me;
					AngVk += p->pos_vk;
					if (A_bas->a_bas.mwst == VatReduced)
					{
						OSGPrice += p->pos_vk_nto;
					}
				}
				if (Form != NULL && Partyk != NULL)
				{
					Form->Get ();
					Partyk->partyk.ang_gew = AngGew;
					Partyk->partyk.ang_vk  = AngVk;
					if (Partyk->partyk.anz_pers > 0.0)
					{
						Partyk->partyk.einzel_vk = Partyk->partyk.ang_vk / Partyk->partyk.anz_pers;
						OSSPrice = OSGPrice / Partyk->partyk.anz_pers;  

						if (CalculatePauschSingle)
						{
							Partyk->partyk.pauschal_vk = Partyk->partyk.einzel_vk_pausch * Partyk->partyk.anz_pers;
						}
						else
						{
							Partyk->partyk.einzel_vk_pausch = Partyk->partyk.pauschal_vk / Partyk->partyk.anz_pers;
						}
						if (Partyk->partyk.einzel_vk_pausch != 0.0)
						{
							Partyk->partyk.einzel_preis = 0;
						}
					}
					else
					{
							Partyk->partyk.einzel_vk = 0.0;
							Partyk->partyk.einzel_vk_pausch = 0.0;
					}
					SingleGew = Partyk->partyk.ang_gew / Partyk->partyk.anz_pers;
					Form->Show ();
					if (CalcForm != NULL)
					{
						CalcForm->Show ();
					}
				}
			}
		}
	}
}

void CAngebotCore::DeleteAng ()
{
	Partyk->dbdelete ();
	Partyp->sqlexecute (DeletePosCursor);
	Partyk->commitwork ();
	List->DestroyElements ();
	DelList->Clear ();
}

BOOL CAngebotCore::PrintAng ()
{
	BOOL ret = TRUE;
	CProcess print;
	Form->Get ();
	WriteAng ();
	Form->Get ();
	CString Email = KunAdr->adr.email;
	Email.TrimRight ();
	CStrFuncs::ToClipboard (Email);

    CString Tmp = getenv ("TMPPATH");
	Tmp.GetEnvironmentVariable (_T("TMPPATH"));
    CString dName;
	FILE *fp;
	if (Tmp != "")
	{
		dName.Format ("%s\\%s.llf", Tmp.GetBuffer (), AngFormat.GetBuffer ());
	}
	else
	{
		dName.Format ("%s.llf", AngFormat.GetBuffer ());
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME %s\n", AngFormat.GetBuffer ());
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 0\n");


		fprintf (fp, "mdn %hd %hd\n",Mdn->mdn.mdn,Mdn->mdn.mdn);
		fprintf (fp, "fil %hd %hd\n",Fil->fil.fil,Fil->fil.fil);
		fprintf (fp, "ang %ld %ld\n", Partyk->partyk.ang,
			                          Partyk->partyk.ang);

		fclose (fp);

		Command.Format ("dr70001 -name %s -datei %s", 
			AngFormat.GetBuffer (),
			dName.GetBuffer ());
	}
	else
	{
		Command.Format ("dr70001 -name %s", 
			AngFormat.GetBuffer ());
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		ret = FALSE;
	}
	ReadPositions ();
	return ret;
}

BOOL CAngebotCore::MailAng ()
{
	BOOL ret = TRUE;
	CProcess print;
	Form->Get ();
	WriteAng ();
	Form->Get ();
	CString Email = KunAdr->adr.email;
	Email.TrimRight ();
	CStrFuncs::ToClipboard (Email);

    CString Tmp = getenv ("TMPPATH");
	Tmp.GetEnvironmentVariable (_T("TMPPATH"));
    CString dName;
	FILE *fp;
	if (Tmp != "")
	{
		dName.Format ("%s\\%s.llf", Tmp.GetBuffer (), AngFormat.GetBuffer ());
	}
	else
	{
		dName.Format ("%s.llf", AngFormat.GetBuffer ());
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME %s\n", AngFormat.GetBuffer ());
		fprintf (fp, "DRUCK 0\n");
		fprintf (fp, "LABEL 0\n");

		fprintf (fp, "ALTERNATETYP %s\n", _T("PDF"));
		fprintf (fp, "ALTERNATEQUIET 1\n");
		fprintf (fp, "ALTERNATEDIR %s\n", Tmp.GetBuffer ());
		fprintf (fp, "ALTERNATENAME %s%08ld.pdf\n", _T("Angebot"), Kun->kun.kun);
		fprintf (fp, "ALTERNATESHOW %1\n");

		fprintf (fp, "mdn %hd %hd\n",Mdn->mdn.mdn,Mdn->mdn.mdn);
		fprintf (fp, "fil %hd %hd\n",Fil->fil.fil,Fil->fil.fil);
		fprintf (fp, "ang %ld %ld\n", Partyk->partyk.ang,
			                          Partyk->partyk.ang);

		fclose (fp);
		Command.Format ("dr70001 -datei %s", 
			dName.GetBuffer ());
	}
	else
	{
		ret = FALSE;
	}
	if (ret)
	{
		print.SetCommand (Command);
		HANDLE pid = print.Start (SW_SHOWNORMAL);
		if (pid == NULL)
		{
			ret = FALSE;
		}
	}
	if (ret)
	{
		Mail.Init ();
		Mail.m_To = Email;
		CString *Path = new CString ();
		Path->Format (_T("%s\\%s%08ld.pdf"), Tmp.GetBuffer (),_T("Angebot"), Kun->kun.kun);
		KunAdr->adr.adr = Kun->kun.adr1;
		KunAdr->dbreadfirst ();
		Mail.AddFile (Path);
		Mail.m_Subject = _T("Angebot");
		CString Line;
		memcpy (&Ptabn->ptabn, &ptabn_null, sizeof (PTABN));
		strcpy (Ptabn->ptabn.ptitem, "anr");
		sprintf (Ptabn->ptabn.ptwert, "%hd", KunAdr->adr.anr);
		Ptabn->dbreadfirst ();
		CString Anr = Ptabn->ptabn.ptbez;
        Anr.Trim ();
		if (Anr.CompareNoCase (CString("HERR")) == 0)
		{
			Line.Format (_T("Sehr geehrter Herr %s\n\n"), KunAdr->adr.adr_nam1);
		}
		else if (Anr.CompareNoCase (CString("FRAU")) == 0)
		{
			Line.Format (_T("Sehr geehrte Frau %s\n\n"), KunAdr->adr.adr_nam1);
		}
		else if (Anr.CompareNoCase (CString("FIRMA")) == 0)
		{
			Line.Format (_T("An die Firma %s\n\n"), KunAdr->adr.adr_nam1);
		}
		else
		{
			Line.Format (_T("An die Firma %s\n\n"), KunAdr->adr.adr_nam1);
		}
		Mail.m_NoteText = Line;
		Mail.m_NoteText += _T("Anbei unser Angebot f�r Ihre Feier\n");
		Mail.Send ();
	}
	ReadPositions ();
	return ret;
}

BOOL CAngebotCore::PrintPack ()
{
	BOOL ret = TRUE;
	CProcess print;
	Form->Get ();
	WriteAng ();
	Form->Get ();
    CString Tmp = getenv ("TMPPATH");
	Tmp.GetEnvironmentVariable (_T("TMPPATH"));
    CString dName;
	FILE *fp;
	if (Tmp != "")
	{
		dName.Format ("%s\\%s.llf", Tmp.GetBuffer (), Psz01.GetBuffer ());
	}
	else
	{
		dName.Format ("%s.llf", Psz01.GetBuffer ());
	}
	CString Command;
	fp = fopen (dName.GetBuffer (), "w");
	if (fp != NULL)
	{
		fprintf (fp, "NAME %s\n", Psz01.GetBuffer ());
		fprintf (fp, "DRUCK 1\n");
		fprintf (fp, "LABEL 1\n");
		fprintf (fp, "mdn %hd %hd\n",Mdn->mdn.mdn,Mdn->mdn.mdn);
		fprintf (fp, "fil %hd %hd\n",Fil->fil.fil,Fil->fil.fil);
		fprintf (fp, "ang %ld %ld\n", Partyk->partyk.ang,
			                          Partyk->partyk.ang);
		fclose (fp);
		Command.Format ("dr70001 -name %s -datei %s", 
			Psz01.GetBuffer (),
			dName.GetBuffer ());
	}
	else
	{
		Command.Format ("dr70001 -name %s", 
			Psz01.GetBuffer ());
	}
	print.SetCommand (Command);
	HANDLE pid = print.Start (SW_SHOWNORMAL);
	if (pid == NULL)
	{
		ret = FALSE;
	}
	ReadPositions ();
	return ret;
}

BOOL CAngebotCore::PrintAuftrag ()
{
	CProcess print;

	BOOL ret = TRUE;
	Form->Get ();
	CString Email = KunAdr->adr.email;
	Email.TrimRight ();
	CStrFuncs::ToClipboard (Email);
	Partyk->partyk.auf = CreateAufNr ();
	if (Partyk->partyk.auf != 0l)
	{
		Partyk->partyk.ang_stat = POrder;
		WriteAng ();
		Partyp->sqlexecute (SetAufpUpdateCursor);
		ExportAuftrag ();
		CString Tmp = getenv ("TMPPATH");
		Tmp.GetEnvironmentVariable (_T("TMPPATH"));
		CString dName;
		FILE *fp;
		if (Tmp != "")
		{
			dName.Format ("%s\\%s.llf", Tmp.GetBuffer (), AuftragFormat.GetBuffer ());
		}
		else
		{
			dName.Format ("%s.llf", AuftragFormat.GetBuffer ());
		}
		CString Command;
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			fprintf (fp, "NAME %s\n", AuftragFormat.GetBuffer ());
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "mdn %hd %hd\n",Mdn->mdn.mdn,Mdn->mdn.mdn);
			fprintf (fp, "fil %hd %hd\n",Fil->fil.fil,Fil->fil.fil);
			fprintf (fp, "auf %ld %ld\n", Partyk->partyk.auf,
										  Partyk->partyk.auf);
			fclose (fp);
			Command.Format ("dr70001 -name %s -datei %s", 
				AuftragFormat.GetBuffer (),
				dName.GetBuffer ());
		}
		else
		{
			Command.Format ("dr70001 -name %s", 
				AuftragFormat.GetBuffer ());
		}
		print.SetCommand (Command);
		HANDLE pid = print.Start (SW_SHOWNORMAL);
		if (pid == NULL)
		{
			ret = FALSE;
		}
	}
	else
	{
		ret = FALSE;
	}
/*
	if (ret)
	{
		CString Tmp = getenv ("TMPPATH");
		Tmp.GetEnvironmentVariable (_T("TMPPATH"));
		CString dName;
		FILE *fp;
		if (Tmp != "")
		{
			dName.Format ("%s\\%s.llf", Tmp.GetBuffer (), Psz01.GetBuffer ());
		}
		else
		{
			dName.Format ("%s.llf", Psz01.GetBuffer ());
		}
		CString Command;
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			fprintf (fp, "NAME %s\n", Psz01.GetBuffer ());
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 1\n");
			fprintf (fp, "mdn %hd %hd\n",Mdn->mdn.mdn,Mdn->mdn.mdn);
			fprintf (fp, "fil %hd %hd\n",Fil->fil.fil,Fil->fil.fil);
			fprintf (fp, "auf %ld %ld\n", Partyk->partyk.auf,
										  Partyk->partyk.auf);
			fclose (fp);
			Command.Format ("dr70001 -name %s -datei %s", 
				Psz01.GetBuffer (),
				dName.GetBuffer ());
		}
		else
		{
			Command.Format ("dr70001 -name %s -l", 
				Psz01.GetBuffer ());
		}
		print.SetCommand (Command);
		HANDLE pid = print.Start (SW_SHOWNORMAL);
		if (pid == NULL)
		{
			ret = FALSE;
		}
	}
*/
	ReadPositions ();
	return ret;
}

BOOL CAngebotCore::MailAuftrag ()
{
	CProcess print;

	BOOL ret = TRUE;
	Form->Get ();
	CString Email = KunAdr->adr.email;
	Email.TrimRight ();
	CStrFuncs::ToClipboard (Email);
	Partyk->partyk.auf = CreateAufNr ();
	if (Partyk->partyk.auf != 0l)
	{
		Partyk->partyk.ang_stat = POrder;
		WriteAng ();
		Partyp->sqlexecute (SetAufpUpdateCursor);
		ExportAuftrag ();
		CString Tmp = getenv ("TMPPATH");
		Tmp.GetEnvironmentVariable (_T("TMPPATH"));
		CString dName;
		FILE *fp;
		if (Tmp != "")
		{
			dName.Format ("%s\\%s.llf", Tmp.GetBuffer (), AuftragFormat.GetBuffer ());
		}
		else
		{
			dName.Format ("%s.llf", AuftragFormat.GetBuffer ());
		}
		CString Command;
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			fprintf (fp, "NAME %s\n", AuftragFormat.GetBuffer ());
			fprintf (fp, "DRUCK 0\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "ALTERNATETYP %s\n", _T("PDF"));
			fprintf (fp, "ALTERNATEQUIET 1\n");
			fprintf (fp, "ALTERNATEDIR %s\n", Tmp.GetBuffer ());
			fprintf (fp, "ALTERNATENAME %s%08ld.pdf\n", _T("Auftrag"), Kun->kun.kun);
			fprintf (fp, "ALTERNATESHOW %1\n");

			fprintf (fp, "mdn %hd %hd\n",Mdn->mdn.mdn,Mdn->mdn.mdn);
			fprintf (fp, "fil %hd %hd\n",Fil->fil.fil,Fil->fil.fil);
			fprintf (fp, "auf %ld %ld\n", Partyk->partyk.auf,
										  Partyk->partyk.auf);
			fclose (fp);
			Command.Format ("dr70001 -datei %s", 
				dName.GetBuffer ());
		}
		else
		{
			ret = FALSE;
		}
		if (ret)
		{
			print.SetCommand (Command);
			HANDLE pid = print.Start (SW_SHOWNORMAL);
			if (pid == NULL)
			{
				ret = FALSE;
			}
		}
		if (ret)
		{
			Mail.Init ();
			Mail.m_To = Email;
			CString *Path = new CString ();
			Path->Format (_T("%s\\%s%08ld.pdf"), Tmp.GetBuffer (),_T("Auftrag"), Kun->kun.kun);
			KunAdr->adr.adr = Kun->kun.adr1;
			KunAdr->dbreadfirst ();
			Mail.AddFile (Path);
			Mail.m_Subject = _T("Auftag");
			CString Line;
			memcpy (&Ptabn->ptabn, &ptabn_null, sizeof (PTABN));
			strcpy (Ptabn->ptabn.ptitem, "anr");
			sprintf (Ptabn->ptabn.ptwert, "%hd", KunAdr->adr.anr);
			Ptabn->dbreadfirst ();
			CString Anr = Ptabn->ptabn.ptbez;
			Anr.Trim ();
			if (Anr.CompareNoCase (CString("HERR")) == 0)
			{
				Line.Format (_T("Sehr geehrter Herr %s\n\n"), KunAdr->adr.adr_nam1);
			}
			else if (Anr.CompareNoCase (CString("FRAU")) == 0)
			{
				Line.Format (_T("Sehr geehrte Frau %s\n\n"), KunAdr->adr.adr_nam1);
			}
			else if (Anr.CompareNoCase (CString("FIRMA")) == 0)
			{
				Line.Format (_T("An die Firma %s\n\n"), KunAdr->adr.adr_nam1);
			}
			else
			{
				Line.Format (_T("An die Firma %s\n\n"), KunAdr->adr.adr_nam1);
			}
			Mail.m_NoteText = Line;
			Mail.m_NoteText += _T("Anbei unser Auftrag f�r Ihre Feier\n");
			Mail.Send ();
		}
	}
	else
	{
		ret = FALSE;
	}
	ReadPositions ();
	return ret;
}

BOOL CAngebotCore::PrintLief ()
{
	CProcess print;
	BOOL ret = TRUE;
	Form->Get ();

	//FS-357 Direkt drucken , ohne Auftrag
	if (Partyk->partyk.auf == 0l)
	{
		Partyk->partyk.auf = CreateAufNr ();
		if (Partyk->partyk.auf != 0l)
		{
			Partyk->partyk.ang_stat = POrder;
			WriteAng ();
			Partyp->sqlexecute (SetAufpUpdateCursor);
			ExportAuftrag ();
		}
		else return FALSE;
	}
	ReadPositions ();



	if (Partyk->partyk.ls == 0l)
	{
		Partyk->partyk.ls = CreateLiefNr ();
	}
	if (Partyk->partyk.ls != 0l)
	{
		Form->Show ();
		Partyk->partyk.ang_stat = PPrinted;
		WriteAng ();
		Partyp->sqlexecute (SetLiefUpdateCursor);
		ls_stat = Printed;
		ExportLief ();
		Form->Get ();
		CString Tmp = getenv ("TMPPATH");
		Tmp.GetEnvironmentVariable (_T("TMPPATH"));
		CString dName;
		FILE *fp;
		if (Tmp != "")
		{
			dName.Format ("%s\\%s.llf", Tmp.GetBuffer (), LiefFormat.GetBuffer ());
		}
		else
		{
			dName.Format ("%s.llf", LiefFormat.GetBuffer ());
		}
		CString Command;
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			fprintf (fp, "NAME %s\n", LiefFormat.GetBuffer ());
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "mdn %hd %hd\n",Mdn->mdn.mdn,Mdn->mdn.mdn);
			fprintf (fp, "fil %hd %hd\n",Fil->fil.fil,Fil->fil.fil);
			fprintf (fp, "ang %ld %ld\n", Partyk->partyk.ang,
										  Partyk->partyk.ang);
			fclose (fp);
			Command.Format ("dr70001 -name %s -datei %s", 
				LiefFormat.GetBuffer (),
				dName.GetBuffer ());
		}
		else
		{
			Command.Format ("dr70001 -name %s", 
				LiefFormat.GetBuffer ());
		}
		print.SetCommand (Command);
		HANDLE pid = print.Start (SW_SHOWNORMAL);
		if (pid == NULL)
		{
			ret = FALSE;
		}
		ReadPositions ();
	}
	return ret;
}

void CAngebotCore::CalculateSinglePauschPrice ()
{
	A_BAS *a_bas;
	A_BAS_CLASS *A_bas = new A_BAS_CLASS;
	PARTYP **it;
	PARTYP *p;
    double factor = 1.0;		

	if ((Partyk->partyk.pauschal_vk != 0.0 || Partyk->partyk.service_pausch_vk != 0.0) &&
		(Partyk->partyk.pauschal_vk + Partyk->partyk.service_pausch_vk) != Partyk->partyk.ang_vk)
	{
		if (Partyk->partyk.pauschal_vk == 0.0)
		{
// Nur sevice Pauschal_vk ist != 0.0
			Partyk->partyk.pauschal_vk = Partyk->partyk.ang_vk;
		}

		factor = (Partyk->partyk.pauschal_vk + Partyk->partyk.service_pausch_vk) / 
					Partyk->partyk.ang_vk;
		List->Start ();
		while ((it = List->GetNext ()) != NULL)
		{
				p = *it;
				p->auf_vk_euro *= factor;
				p->auf_vk_pr *= factor;
				p->auf_vk_brutto *= factor;
				p->pos_vk *= factor;
				p->pos_vk_nto *= factor;
				AngVk += p->pos_vk;
				a_bas = ABasList->Find (p->a);
				if (Partyk->partyk.nur_servicepreis)
				{
					if (IsVatFull)
					{
						if (a_bas != NULL && a_bas->mwst == VatReduced)
						{
							OSGPrice += p->pos_vk_nto;
						}
					}
					else
					{
						OSGPrice += p->pos_vk;
					}
				}
				else
				{
					if (a_bas != NULL && a_bas->mwst == VatReduced)
					{
						OSGPrice += p->pos_vk;
					}
				}
			}
		}
	delete A_bas;
	}
		

BOOL CAngebotCore::PrintRech ()
{
	int ret = TRUE;
	CProcess print;
	CString Command;

	Form->Get ();


	//FS-357 A Direkt drucken , ohne Auftrag
	if (Partyk->partyk.auf == 0l)
	{
		Partyk->partyk.auf = CreateAufNr ();
		if (Partyk->partyk.auf != 0l)
		{
			Partyk->partyk.ang_stat = POrder;
			WriteAng ();
			Partyp->sqlexecute (SetAufpUpdateCursor);
			ExportAuftrag ();
		}
		else return FALSE;
		ReadPositions ();
	}

	if (Partyk->partyk.ls == 0l)
	{
		Partyk->partyk.ls = CreateLiefNr ();
		if (Partyk->partyk.ls != 0l)
		{
			Form->Show ();
			Partyk->partyk.ang_stat = PPrinted;
			WriteAng ();
			Partyp->sqlexecute (SetLiefUpdateCursor);
			ls_stat = Printed;
			ExportLief ();
			Form->Get ();
		}
		else return FALSE;
		ReadPositions ();
	}
	//FS-357 E



	CalculateSinglePauschPrice ();	
	if (Partyk->partyk.rech == 0l)
	{
		Partyk->partyk.rech = CreateRechNr ();
	}
	if (Partyk->partyk.rech != 0l)
	{
		Form->Show ();
		Partyk->partyk.ang_stat = PFacturated;
		WriteAng ();
//		Partyp->sqlexecute (SetLiefUpdateCursor);
		ls_stat = Facturated;
		ExportLief ();
		UpdateLiefRetBa ();
		Form->Get ();
		CString Tmp = getenv ("TMPPATH");
		Tmp.GetEnvironmentVariable (_T("TMPPATH"));
		CString dName;
		FILE *fp;
		if (Tmp != "")
		{
			dName.Format ("%s\\%s.llf", Tmp.GetBuffer (), RechFormat.GetBuffer ());
		}
		else
		{
			dName.Format ("%s.llf", RechFormat.GetBuffer ());
		}
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			fprintf (fp, "NAME %s\n", RechFormat.GetBuffer ());
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "mdn %hd %hd\n",Mdn->mdn.mdn,Mdn->mdn.mdn);
			fprintf (fp, "fil %hd %hd\n",Fil->fil.fil,Fil->fil.fil);
			fprintf (fp, "ang %ld %ld\n", Partyk->partyk.ang,
										  Partyk->partyk.ang);
			fclose (fp);
			Command.Format ("dr70001 -name %s -datei %s", 
				RechFormat.GetBuffer (),
				dName.GetBuffer ());
		}
		else
		{
			Command.Format ("dr70001 -name %s", 
				RechFormat.GetBuffer ());
		}
		print.SetCommand (Command);
		HANDLE pid = print.Start (SW_SHOWNORMAL);
		if (pid == NULL)
		{
			ret = FALSE;
		}
		ReadPositions ();
	}
	else
	{
		ret = FALSE;
	}
	return ret;
}

void CAngebotCore::UpdateLiefRetBa ()
{
	memcpy (&LIEF_RETBAT, &lief_retba_null, sizeof (LIEF_RETBA));
	strcpy (LIEF_RETBAT.blg_typ, _T("L"));
	LIEF_RETBAT.nr = Lsk->lsk.ls;
	LIEF_RETBAT.mdn = Lsk->lsk.mdn;
	LIEF_RETBAT.fil = Lsk->lsk.fil;
	LIEF_RETBAT.kun_fil = Lsk->lsk.kun_fil;
	memcpy (&LIEF_RETBAT.dat, &Lsk->lsk.lieferdat, sizeof (DATE_STRUCT));
	memset (LIEF_RETBAT.pers, 0, sizeof (LIEF_RETBAT.pers));
	strncpy (LIEF_RETBAT.pers, Lsk->lsk.pers_nam, sizeof (LIEF_RETBAT.pers) - 1);
	LIEF_RETBAC->dbupdate ();
}


BOOL CAngebotCore::RePrintRech ()
{
	int ret = TRUE;
	CProcess print;
	CString Command;

	if (Partyk->partyk.rech != 0l)
	{
		CString Tmp = getenv ("TMPPATH");
		Tmp.GetEnvironmentVariable (_T("TMPPATH"));
		CString dName;
		FILE *fp;
		if (Tmp != "")
		{
			dName.Format ("%s\\%s.llf", Tmp.GetBuffer (), RechFormat.GetBuffer ());
		}
		else
		{
			dName.Format ("%s.llf", RechFormat.GetBuffer ());
		}
		fp = fopen (dName.GetBuffer (), "w");
		if (fp != NULL)
		{
			fprintf (fp, "NAME %s\n", RechFormat.GetBuffer ());
			fprintf (fp, "DRUCK 1\n");
			fprintf (fp, "LABEL 0\n");
			fprintf (fp, "mdn %hd %hd\n",Mdn->mdn.mdn,Mdn->mdn.mdn);
			fprintf (fp, "fil %hd %hd\n",Fil->fil.fil,Fil->fil.fil);
			fprintf (fp, "ang %ld %ld\n", Partyk->partyk.ang,
										  Partyk->partyk.ang);
			fclose (fp);
			Command.Format ("dr70001 -name %s -datei %s", 
				RechFormat.GetBuffer (),
				dName.GetBuffer ());
		}
		else
		{
			Command.Format ("dr70001 -name %s", 
				RechFormat.GetBuffer ());
		}
		print.SetCommand (Command);
		HANDLE pid = print.Start (SW_SHOWNORMAL);
		if (pid == NULL)
		{
			ret = FALSE;
		}
	}
	else
	{
		ret = FALSE;
	}
	return ret;
}

BOOL CAngebotCore::ExportAuftrag ()
{
	if (HostIsFit)
	{
		return ExportAuftragToFit ();
	}
	return TRUE;
}

BOOL CAngebotCore::ExportAuftragToFit ()
{	
	BOOL ret = TRUE;

   AUFKT.mdn = Partyk->partyk.mdn;
   AUFKT.fil = Partyk->partyk.fil;
   AUFKT.ang = Partyk->partyk.ang;
   AUFKT.auf = Partyk->partyk.auf;
   AUFKT.adr = Partyk->partyk.adr;
   AUFKT.kun_fil = Partyk->partyk.kun_fil;
   AUFKT.kun = Partyk->partyk.kun;
   memcpy (&AUFKT.lieferdat, &Partyk->partyk.lieferdat, sizeof (DATE_STRUCT)); 
   _tcscpy (AUFKT.lieferzeit, Partyk->partyk.lieferzeit);
//   _tcscpy (AUFKT.hinweis, Partyk->partyk.hinweis);
   _tcscpy (AUFKT.hinweis, _T(""));
   AUFKT.auf_stat = 2;
   _tcscpy (AUFKT.kun_krz1,Partyk->partyk.kun_krz1);
   _tcscpy (AUFKT.feld_bz1,Partyk->partyk.feld_bz1);
   _tcscpy (AUFKT.feld_bz2,Partyk->partyk.feld_bz1);
   _tcscpy (AUFKT.feld_bz3,Partyk->partyk.feld_bz3);
   AUFKT.delstatus = Partyk->partyk.delstatus;
   AUFKT.zeit_dec = Partyk->partyk.zeit_dec;
   AUFKT.kopf_txt = Partyk->partyk.kopf_txt;
   AUFKT.fuss_txt = Partyk->partyk.fuss_txt;
   AUFKT.vertr = Partyk->partyk.vertr;
   _tcscpy (AUFKT.auf_ext,Partyk->partyk.auf_ext);
   AUFKT.tou = Partyk->partyk.tou;
   _tcscpy (AUFKT.pers_nam, Partyk->partyk.pers_nam);
   memcpy (&AUFKT.komm_dat, &Partyk->partyk.komm_dat, sizeof (DATE_STRUCT));
   memcpy (&AUFKT.best_dat, &Partyk->partyk.komm_dat, sizeof (DATE_STRUCT));
   AUFKT.waehrung = Partyk->partyk.waehrung;
   AUFKT.auf_art = Partyk->partyk.auf_art;
   AUFKT.gruppe = Partyk->partyk.gruppe;
   AUFKT.ccmarkt = Partyk->partyk.ccmarkt;
   AUFKT.fak_typ = Partyk->partyk.fak_typ;
   AUFKT.tou_nr = Partyk->partyk.tou_nr;
   _tcscpy (AUFKT.ueb_kz,Partyk->partyk.ueb_kz);
   memcpy (&AUFKT.fix_dat, &Partyk->partyk.fix_dat, sizeof (DATE_STRUCT));
   _tcscpy (AUFKT.komm_name, Partyk->partyk.komm_name);
   memcpy (&AUFKT.akv, &Partyk->partyk.akv, sizeof (DATE_STRUCT));
   _tcscpy (AUFKT.akv_zeit, Partyk->partyk.akv_zeit);
   memcpy (&AUFKT.bearb, &Partyk->partyk.bearb, sizeof (DATE_STRUCT));
   _tcscpy (AUFKT.bearb_zeit, Partyk->partyk.bearb_zeit);
   AUFKT.psteuer_kz = Partyk->partyk.psteuer_kz;
   AUFKC->dbupdate ();
   Partyp->sqlexecute (DeleteAufPosCursor);
   if (Partyp->sqlopen (ListCursor) == 0)
   {
	   while (Partyp->sqlfetch (ListCursor) == 0)
	   {
		   Partyp->dbreadfirst ();
		   AUFPT.mdn = Partyp->partyp.mdn;
		   AUFPT.fil = Partyp->partyp.fil;
		   AUFPT.auf = Partyp->partyp.auf;
		   AUFPT.posi = Partyp->partyp.posi;
		   AUFPT.aufp_txt = Partyp->partyp.aufp_txt;
		   AUFPT.a = Partyp->partyp.a;
		   AUFPT.auf_me = Partyp->partyp.auf_me;
		   _tcscpy (AUFPT.auf_me_bz,Partyp->partyp.auf_me_bz);
		   AUFPT.lief_me = Partyp->partyp.lief_me;
		   _tcscpy (AUFPT.lief_me_bz,Partyp->partyp.lief_me_bz);
		   AUFPT.auf_vk_pr = Partyp->partyp.auf_vk_pr;
		   AUFPT.auf_lad_pr = Partyp->partyp.auf_lad_pr;
		   AUFPT.delstatus = Partyp->partyp.delstatus;
		   AUFPT.sa_kz_sint = Partyp->partyp.sa_kz_sint;
		   AUFPT.prov_satz = Partyp->partyp.prov_satz;
		   AUFPT.ksys = Partyp->partyp.ksys;
		   AUFPT.pid = Partyp->partyp.pid;
		   AUFPT.auf_klst = Partyp->partyp.auf_klst;
		   AUFPT.teil_smt = Partyp->partyp.teil_smt;
		   AUFPT.dr_folge = Partyp->partyp.dr_folge;
		   AUFPT.inh = Partyp->partyp.inh;
		   AUFPT.auf_vk_euro = Partyp->partyp.auf_vk_euro;
		   AUFPT.auf_vk_fremd = Partyp->partyp.auf_vk_fremd;
		   AUFPT.auf_lad_euro = Partyp->partyp.auf_lad_euro;
		   AUFPT.auf_lad_fremd = Partyp->partyp.auf_lad_fremd;
		   AUFPT.rab_satz = Partyp->partyp.rab_satz;
		   AUFPT.me_einh_kun = Partyp->partyp.me_einh_kun;
		   AUFPT.me_einh = Partyp->partyp.me_einh;
		   AUFPT.me_einh_kun1 = Partyp->partyp.me_einh_kun1;
		   AUFPT.auf_me1 = Partyp->partyp.auf_me1;
		   AUFPT.inh1 = Partyp->partyp.inh1;
		   AUFPT.me_einh_kun2 = Partyp->partyp.me_einh_kun2;
		   AUFPT.auf_me2 = Partyp->partyp.auf_me2;
		   AUFPT.inh2 = Partyp->partyp.inh2;
		   AUFPT.me_einh_kun3 = Partyp->partyp.me_einh_kun3;
		   AUFPT.auf_me3 = Partyp->partyp.auf_me3;
		   AUFPT.inh3 = Partyp->partyp.inh3;
		   AUFPT.gruppe = Partyp->partyp.gruppe;
		   _tcscpy (AUFPT.kond_art,Partyp->partyp.kond_art);
		   AUFPT.a_grund = Partyp->partyp.a_grund;
		   AUFPT.ls_pos_kz = Partyp->partyp.ls_pos_kz;
		   AUFPT.posi_ext = Partyp->partyp.posi_ext;
		   AUFPT.kun = Partyp->partyp.kun;
		   AUFPT.a_ers = Partyp->partyp.a_ers;
		   _tcscpy (AUFPT.ls_charge,Partyp->partyp.ls_charge);
		   AUFPT.aufschlag = Partyp->partyp.aufschlag;
		   AUFPT.aufschlag_wert = Partyp->partyp.aufschlag_wert;
		   AUFPT.pos_txt_kz = Partyp->partyp.pos_txt_kz;
		   AUFPC->dbupdate ();
	   }
   }
   return ret;
}

BOOL CAngebotCore::ExportLief ()
{
	if (HostIsFit)
	{
		return ExportLiefToFit ();
	}
	return TRUE;
}

BOOL CAngebotCore::ExportLiefToFit ()
{	
	BOOL ret = TRUE;

   memcpy (&LSKT, &lsk_null, sizeof (LSK));
   LSKT.mdn = (short) Partyk->partyk.mdn;
   LSKT.fil = Partyk->partyk.fil;
   LSKT.ls = Partyk->partyk.ls;
   LSKT.rech = Partyk->partyk.rech;
   LSKT.auf = Partyk->partyk.auf;
   LSKT.adr = Partyk->partyk.adr;
   LSKT.rech_typ = RechPa;
   LSKT.kun_fil = Partyk->partyk.kun_fil;
   LSKT.kun = Partyk->partyk.kun;
   memcpy (&LSKT.lieferdat, &Partyk->partyk.lieferdat, sizeof (DATE_STRUCT)); 
   _tcscpy (LSKT.lieferzeit, Partyk->partyk.lieferzeit);
//   _tcscpy (LSKT.hinweis, Partyk->partyk.hinweis);
   _tcscpy (LSKT.hinweis, _T(""));
   LSKT.ls_stat = ls_stat;
   _tcscpy (LSKT.kun_krz1,Partyk->partyk.kun_krz1);
   _tcscpy (LSKT.feld_bz1,Partyk->partyk.feld_bz1);
   _tcscpy (LSKT.feld_bz2,Partyk->partyk.feld_bz2);
   _tcscpy (LSKT.feld_bz3,Partyk->partyk.feld_bz3);
   LSKT.delstatus = Partyk->partyk.delstatus;
   LSKT.zeit_dec = Partyk->partyk.zeit_dec;
   LSKT.kopf_txt = Partyk->partyk.kopf_txt;
   LSKT.fuss_txt = Partyk->partyk.fuss_txt;
   LSKT.vertr = Partyk->partyk.vertr;
   _tcscpy (LSKT.auf_ext,Partyk->partyk.auf_ext);
   LSKT.tou = Partyk->partyk.tou;
   _tcscpy (LSKT.pers_nam, Partyk->partyk.pers_nam);
   memcpy (&LSKT.komm_dat, &Partyk->partyk.komm_dat, sizeof (DATE_STRUCT));
   memcpy (&LSKT.best_dat, &Partyk->partyk.komm_dat, sizeof (DATE_STRUCT));
   LSKT.waehrung = Partyk->partyk.waehrung;
   LSKT.auf_art = Partyk->partyk.auf_art;
   LSKT.gruppe = Partyk->partyk.gruppe;
   LSKT.ccmarkt = Partyk->partyk.ccmarkt;
   LSKT.fak_typ = Partyk->partyk.fak_typ;
   LSKT.tou_nr = Partyk->partyk.tou_nr;
   _tcscpy (LSKT.ueb_kz,Partyk->partyk.ueb_kz);
   memcpy (&LSKT.fix_dat, &Partyk->partyk.fix_dat, sizeof (DATE_STRUCT));
   _tcscpy (LSKT.komm_name, Partyk->partyk.komm_name);
   LSKT.psteuer_kz = Partyk->partyk.psteuer_kz;
   LSKC->dbupdate ();
   Partyp->sqlexecute (DeleteLiefPosCursor);
   if (Partyp->sqlopen (ListCursor) == 0)
   {
	   while (Partyp->sqlfetch (ListCursor) == 0)
	   {
		   memcpy (&LSPT, &lsp_null, sizeof (LSP));
		   Partyp->dbreadfirst ();
		   LSPT.mdn = Partyp->partyp.mdn;
		   LSPT.fil = Partyp->partyp.fil;
		   LSPT.ls = Partyp->partyp.ls;
		   LSPT.posi = Partyp->partyp.posi;
//		   LSPT.lsp_txt = Partyp->partyp.aufp_txt;
		   LSPT.a = Partyp->partyp.a;
		   LSPT.auf_me = Partyp->partyp.auf_me;
		   _tcscpy (LSPT.auf_me_bz,Partyp->partyp.auf_me_bz);
		   LSPT.lief_me = Partyp->partyp.lief_me;
		   _tcscpy (LSPT.lief_me_bz,Partyp->partyp.lief_me_bz);
		   LSPT.ls_vk_pr = Partyp->partyp.auf_vk_pr;
		   LSPT.ls_lad_pr = Partyp->partyp.auf_lad_pr;
		   LSPT.delstatus = Partyp->partyp.delstatus;
		   LSPT.sa_kz_sint = Partyp->partyp.sa_kz_sint;
		   LSPT.prov_satz = Partyp->partyp.prov_satz;
//		   LSPT.auf_klst = Partyp->partyp.auf_klst;
		   LSPT.teil_smt = Partyp->partyp.teil_smt;
//		   LSPT.dr_folge = Partyp->partyp.dr_folge;
//		   LSPT.inh = Partyp->partyp.inh;
		   LSPT.ls_vk_euro = Partyp->partyp.auf_vk_euro;
		   LSPT.ls_vk_fremd = Partyp->partyp.auf_vk_fremd;
		   LSPT.ls_lad_euro = Partyp->partyp.auf_lad_euro;
		   LSPT.ls_lad_fremd = Partyp->partyp.auf_lad_fremd;
		   LSPT.rab_satz = Partyp->partyp.rab_satz;
		   LSPT.me_einh_kun = Partyp->partyp.me_einh_kun;
		   LSPT.me_einh = Partyp->partyp.me_einh;
		   LSPT.me_einh_kun1 = Partyp->partyp.me_einh_kun1;
		   LSPT.auf_me1 = Partyp->partyp.auf_me1;
		   LSPT.inh1 = Partyp->partyp.inh1;
		   LSPT.me_einh_kun2 = Partyp->partyp.me_einh_kun2;
		   LSPT.auf_me2 = Partyp->partyp.auf_me2;
		   LSPT.inh2 = Partyp->partyp.inh2;
		   LSPT.me_einh_kun3 = Partyp->partyp.me_einh_kun3;
		   LSPT.auf_me3 = Partyp->partyp.auf_me3;
		   LSPT.inh3 = Partyp->partyp.inh3;
//		   LSPT.gruppe = Partyp->partyp.gruppe;
		   _tcscpy (LSPT.kond_art,Partyp->partyp.kond_art);
		   LSPT.a_grund = Partyp->partyp.a_grund;
		   LSPT.ls_pos_kz = Partyp->partyp.ls_pos_kz;
		   LSPT.posi_ext = Partyp->partyp.posi_ext;
		   LSPT.kun = Partyp->partyp.kun;
//		   LSPT.a_ers = Partyp->partyp.a_ers;
		   _tcscpy (LSPT.ls_charge,Partyp->partyp.ls_charge);
		   LSPT.aufschlag = Partyp->partyp.aufschlag;
		   LSPT.aufschlag_wert = Partyp->partyp.aufschlag_wert;
		   LSPT.pos_txt_kz = Partyp->partyp.pos_txt_kz;
		   CalculateHbkDate ();
		   LSPC->dbupdate ();
	   }
   }
   return ret;
}

void CAngebotCore::CalculateHbkDate ()
{
	short hbk_ztr;
	if (LSPT.hbk_date.day == 0)
	{

		A_bas->a_bas.a = LSPT.a;
		A_bas->a_bas.hbk_ztr = 0;
		strcpy ((char *) A_bas->a_bas.hbk_kz, "T");
		A_bas->dbreadfirst ();
		hbk_ztr = A_bas->a_bas.hbk_ztr;
		if (strcmp ((char *) A_bas->a_bas.hbk_kz, "w") == 0)
		{
			hbk_ztr *= 7;
		}
		else if (strcmp ((char *) A_bas->a_bas.hbk_kz, "w") == 0)
		{
			hbk_ztr *= 30;
		}
/*
		CString Date;
		CStrFuncs::SysDate (Date);
		time_t timer;
		struct tm *ltime;

		time (&timer);
		ltime = localtime (&timer);
		DATE_STRUCT ToDay;
		ToDay.day = ltime->tm_mday;
		ToDay.month = ltime->tm_mon + 1;
		ToDay.year = ltime->ltime->tm_year + 1900;
*/
		DbTime Today;
		Today.Add (hbk_ztr);
		Today.GetDateStruct (&LSPT.hbk_date);
	}
}

BOOL CAngebotCore::KunOk()
{
	BOOL ret = TRUE;
	Form->Get ();
    if (Kun->kun.kun == 0l)
	{
		ret = FALSE;
	}
	else
	{
		if (!KUNDIVERSE->IsAdrDiverse ())
		{
			Kun->kun.mdn = Mdn->mdn.mdn;
			if (Kun->dbreadfirst () != 0)
			{
				ret = FALSE;
			}
			else
			{
				KunAdr->adr.adr = Kun->kun.adr1;
				KunAdr->dbreadfirst ();
				Form->Show ();
			}
		}
		else
		{
			Kun->kun.adr1 = KunAdr->adr.adr;
		}
	}
	BruttoKunde = FALSE;//FS-360
	if (atoi(Kun->kun.form_typ1) == 3) BruttoKunde = TRUE;  //From_typ3 == 3 : Vk-Preis incl. MWST   //FS-360
	return ret;
}

void CAngebotCore::InsertNewAngTyp (CString& Name)
{
	_tcscpy (Ptabn->ptabn.ptitem, _T("ang_typ"));
	Ptabn->sqlopen (MaxAngTypCursor);
	if (Ptabn->sqlfetch (MaxAngTypCursor) == 0)
	{
		Ptabn->sqlopen (PtabLfdCursor);
		if (Ptabn->sqlfetch (PtabLfdCursor) == 0)
		{
			Ptabn->dbreadfirst ();
			sprintf (Ptabn->ptabn.ptwert, "%d", Ptabn->ptabn.ptlfnr);
			_tcscpy (Ptabn->ptabn.ptbez, Name.GetBuffer ());
			memset (Ptabn->ptabn.ptbezk, 0, sizeof (Ptabn->ptabn.ptbezk));
			_tcsncpy (Ptabn->ptabn.ptbezk, Name.GetBuffer (), sizeof (Ptabn->ptabn.ptbezk) - 1);
			Ptabn->ptabn.ptlfnr ++;
			Ptabn->dbupdate ();
		}
		else
		{
			memcpy (&Ptabn->ptabn, &ptabn_null, sizeof (PTABN));
			Ptabn->ptabn.ptlfnr = 1;
			strcpy ((LPSTR) Ptabn->ptabn.ptitem, "ang_typ");
			sprintf (Ptabn->ptabn.ptwert, "%d", Ptabn->ptabn.ptlfnr);
			_tcscpy (Ptabn->ptabn.ptbez, Name.GetBuffer ());
			memset (Ptabn->ptabn.ptbezk, 0, sizeof (Ptabn->ptabn.ptbezk));
			_tcsncpy (Ptabn->ptabn.ptbezk, Name.GetBuffer (), sizeof (Ptabn->ptabn.ptbezk) - 1);
			DbTime Today;
			Today.GetDateStruct (&Ptabn->ptabn.bearb);
			Today.GetDateStruct (&Ptabn->ptabn.akv);
			Ptabn->dbupdate ();
		}
	}
}

long CAngebotCore::GenAngPosTxtNr0 (void)
/**
Nummer fuer Angebots-Positionstexte generieren.
**/
{
	int count; 
	long nr;

	nr = 0l;
    nr = AutoNr->GetAngTxtNr (nr);
	count = 0;
	while (nr == 0l)
	{
		         Sleep (10);
		         count ++;
				 if (count == 10) break;
                 nr = AutoNr->GetAngTxtNr (nr);
	}
	return nr;
}

BOOL CAngebotCore::AngTxtNrExist (long nr)
{
	   int dsqlstatus;
	   
	   AutoNr->DbClass.sqlin ((long *)  &nr, 2, 0);
	   dsqlstatus = AutoNr->DbClass.sqlcomm ("select nr from angpt where nr = ?");
	   if (dsqlstatus == 100) return FALSE;
	   return TRUE;
}


long CAngebotCore::GenAngPosTxtNr (void)
/**
Nummer fuer Angebots-Positionstexte generieren.
**/
{
	 long nr;

	 while (TRUE)
	 {
		 nr = GenAngPosTxtNr0 ();
		 if (AngTxtNrExist (nr) == FALSE) break;
		 if (nr > 99999999) 
		 {
			 return 0l;
		 }
	 }
	 return nr;
}

BOOL CAngebotCore::ReadAngpt (long nr, CString& Text)
{
	int ret = FALSE;
	int dsqlstatus;
	Text = _T("");
	Angpt->angpt.nr = nr;
	dsqlstatus = Angpt->dbreadfirst ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
		Text = Angpt->angpt.txt;
		Text.TrimRight ();
		while (Angpt->dbread () == 0)
		{
			Text += _T("\n");
			Text += Angpt->angpt.txt;
			Text.TrimRight ();
		}
	}
	return ret;
}

BOOL CAngebotCore::WriteAngpt (long nr, CString& Text)
{
	int ret = FALSE;
	LPTSTR p;

	Angpt->delete_angpposi ();
	Angpt->angpt.nr = nr;
	Angpt->angpt.zei = 1;
	CToken t (Text, _T("\n"));
	while ((p = t.NextToken ()) != NULL)
	{
//		_tcscpy (Angpt->angpt.txt, p);
		LPSTR up = (LPSTR) Angpt->angpt.txt;
		CDbUniCode::DbFromUniCode (p, up, sizeof (Angpt->angpt.txt) / 2);
		Angpt->dbupdate ();
		Angpt->angpt.zei ++;
	}
	return ret;
}

BOOL CAngebotCore::ReadText (long nr, CString& Text)
{
	int ret = FALSE;
	int dsqlstatus;
	Text = _T("");
	Ls_txt->ls_txt.nr = nr;
	dsqlstatus = Ls_txt->dbreadfirst ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
		Text = Ls_txt->ls_txt.txt;
		Text.TrimRight ();
		while (Ls_txt->dbread () == 0)
		{
			Text += _T("\n");
			Text += Ls_txt->ls_txt.txt;
			Text.TrimRight ();
		}
	}
	return ret;
}

BOOL CAngebotCore::WriteText (long nr, CString& Text)
{
	int ret = FALSE;
	LPTSTR p;

	Ls_txt->delete_lstxtposi ();
	Ls_txt->ls_txt.nr = nr;
	Ls_txt->ls_txt.zei = 1;
	CToken t (Text, _T("\n"));
	while ((p = t.NextToken ()) != NULL)
	{
//		_tcscpy (Angpt->angpt.txt, p);
		LPSTR up = (LPSTR) Ls_txt->ls_txt.txt;
		CDbUniCode::DbFromUniCode (p, up, sizeof (Ls_txt->ls_txt.txt) / 2);
		Ls_txt->dbupdate ();
		Ls_txt->ls_txt.zei ++;
	}
	return ret;
}

long CAngebotCore::GenLsTxtNr0 (void)
/**
Nummer fuer Angebots-Positionstexte generieren.
**/
{
	int count; 
	long nr;
	BOOL ret = FALSE;

	nr = 0l;
	ret = AutoNr->GenAutoNr (0, 0, _T("ls_txt"));
	if (ret)
	{
		count = 0;
		nr = AutoNr->Nr;
		while (ret && nr == 0l)
		{
					 Sleep (10);
					 count ++;
					 if (count == 10) break;
               		 ret = AutoNr->GenAutoNr (0, 0, _T("ls_txt"));
					 if (ret)
					 {
						nr = AutoNr->Nr;
					 }
		}
	}
	return nr;
}

BOOL CAngebotCore::LsNrExist (long nr)
{
	   int dsqlstatus;
	   
	   AutoNr->DbClass.sqlin ((long *)  &nr, 2, 0);
	   dsqlstatus = AutoNr->DbClass.sqlcomm ("select nr from ls_txt where nr = ?");
	   if (dsqlstatus == 100) return FALSE;
	   return TRUE;
}


long CAngebotCore::GenLsTxtNr (void)
/**
Nummer fuer Angebots-Positionstexte generieren.
**/
{
	 long nr;

	 while (TRUE)
	 {
		 nr = GenLsTxtNr0 ();
		 if (LsNrExist (nr) == FALSE) break;
		 if (nr > 99999999) 
		 {
			 return 0l;
		 }
	 }
	 return nr;
}


BOOL CAngebotCore::SetAngResource ()
{
	int ret = TRUE;
	long ang_nr = 0l;
	long CopyAng;
	CString Text;
	PARTYP *p;

	Form = FormAng;
	Mdn->mdn.mdn = mdn;
	Fil->fil.fil = fil;
	Partyk->partyk.mdn = mdn;
	Partyk->partyk.fil = fil;
	Form->Show ();
	CopyAng = ang;
	if (LastEks != NULL)
	{
		delete LastEks;
	}
	LastEks = new CDataCollection<double> ();
	ShowLastEks = TRUE;
	if (Partyk->partyk.ang == 0l)
	{
		Partyk->partyk.ang = CreateNumber ();
		if (Partyk->partyk.ang == 0l)
		{
			AfxMessageBox (_T("Es wurde keine Angebotsnummer erzeugt"), MB_OK | MB_ICONERROR);
			ret = FALSE;
		}
	}
	ang_nr = Partyk->partyk.ang;
	if (ret)
	{
		if (Partyk->dbreadfirst () == 0)
		{
			Text.Format (_T("Das Angebot kann nicht kopiert werden.\nEs existiert schon ein Angbot zu Angebotsnummer %ld\n")
				         _T("Sie m�ssen auf der Angebotsseite das aktuelle Angebot zuerst abschliessen"), ang_nr);
			AfxMessageBox (Text.GetBuffer (), MB_OK | MB_ICONERROR);
			ret = FALSE;
		}
	}
	if (ret)
	{
		Partyk->partyk.ang = CopyAng;
		if (Partyk->dbreadfirst () != 0)
		{
			Text.Format (_T("Das Angebot kann nicht kopiert werden.\nDie Vorlage %ld konnte nciht gelesen werden"), ang_nr);
			AfxMessageBox (Text.GetBuffer (), MB_OK | MB_ICONERROR);
			ret = FALSE;
		}
	}
	if (ret)
	{
		Partyk->partyk.ang = ang_nr;
		kun = Partyk->partyk.kun;
		Partyk->partyk.ang_stat = POffer;
		Partyk->partyk.auf = 0l;
		Partyk->partyk.ls = 0l;
		Partyk->partyk.rech = 0l;
		Partyk->partyk.adr = KUNDIVERSE->TestAdrOnDiverse (Partyk->partyk.adr);
		CDbUniCode::DbFromUniCode (PersNam, Partyk->partyk.pers_nam, sizeof (Partyk->partyk.pers_nam));
		Partyk->dbupdate ();
		Mdn->mdn.mdn = (short) Partyk->partyk.mdn;
		Fil->fil.fil = Partyk->partyk.fil;
		Kun->kun.kun = Partyk->partyk.kun;
		Form->Show ();
        Partyk->partyk.ang = CopyAng;
		if (Partyp->sqlopen (ListCursor) == 0)
		{
			List->DestroyElements ();
			DelList->Clear ();
			int row = 0;
			while (Partyp->sqlfetch (ListCursor) == 0)
			{
				Partyp->dbreadfirst ();
				LastEks->Add (Partyp->partyp.auf_vk_euro);
				Partyp->partyp.row_id = 0l;
				Partyp->partyp.ang = ang_nr;
				Partyp->partyp.auf = 0l;
				Partyp->partyp.ls = 0l;
				a = Partyp->partyp.a;
				p = new PARTYP;
				memcpy (p, &Partyp->partyp, sizeof (PARTYP));
				List->Add (p);
				DelList->Add (p->row_id);
				A_bas->a_bas.a = a;
				A_bas->dbreadfirst ();
				GetPrice ();
				p->auf_vk_euro = GetPrNetto ();
				p->auf_vk_pr = GetPrNetto ();
				p->auf_vk_brutto = GetPrBrutto ();
				p->a_gew = A_bas->a_bas.a_gew;
				Calculate (row, 0);
                memcpy (&Partyp->partyp, p, sizeof (PARTYP));
				Partyp->dbupdate ();
			}
			List->DestroyElements ();
			DelList->Clear ();
		}
		else
		{

			Text.Format (_T("Das Angebot kann nicht kopiert werden.\nFehler beim Lesen der Positionen"), ang_nr);
			AfxMessageBox (Text.GetBuffer (), MB_OK | MB_ICONERROR);
			ret = FALSE;
		}
	}
	KunToReadOnly = FALSE;
	memcpy (&PartykAng, Partyk, sizeof (PARTYK_CLASS));
	memcpy (&MdnAng, Mdn, sizeof (MDN_CLASS));
	memcpy (&FilAng, Fil, sizeof (FIL_CLASS));
	memcpy (&KunAng, Kun, sizeof (KUN_CLASS));
	memcpy (&MdnAdrAng, MdnAdr, sizeof (ADR_CLASS));
	memcpy (&FilAdrAng, FilAdr, sizeof (ADR_CLASS));
	memcpy (&KunAdrAng, KunAdr, sizeof (ADR_CLASS));
	if (ret)
	{
        Partyk->partyk.ang = ang_nr;
		Form->Show ();
		Partyk->commitwork ();

		if (Activate != NULL)
		{
			Activate->Run ();
		}
	}
	return ret;	
}

BOOL CAngebotCore::CalcPos ()
{
	double Factor;
	PARTYP **it;
	PARTYP *p;
	int row;

	Form->Get ();
	if (CurrentAnzPers == Partyk->partyk.anz_pers)
	{
		return FALSE;
	}

	Factor = (double) Partyk->partyk.anz_pers / CurrentAnzPers;
	CurrentAnzPers = Partyk->partyk.anz_pers;

	row = 0;
	List->Start ();
	while ((it = List->GetNext ()) != NULL)
	{
		p = *it;
		if (p != NULL && p->a != 0.0)
		{
			p->auf_me *= Factor;
			p->lief_me = 0.0;
		}
		row ++;
	}
	Calculate ();
	return TRUE;
}

void CAngebotCore::HideCalcView ()
{
	if (ShowCalcView != NULL)
	{
		ShowCalcView->Run ();
	}
}

void CAngebotCore::HideCalcViewOnly ()
{
	if (HideOnlyCalcView != NULL)
	{
		HideOnlyCalcView->Run ();
	}
}

void CAngebotCore::SetAng ()
{
	if (Page == PageAngebot)
	{
		Form->Get ();
		FormAng = Form;
	    CalcFormAng = CalcForm;
		memcpy (&PartykAng, Partyk, sizeof (PARTYK_CLASS));
		memcpy (&MdnAng, Mdn, sizeof (MDN_CLASS));
		memcpy (&FilAng, Fil, sizeof (FIL_CLASS));
		memcpy (&KunAng, Kun, sizeof (KUN_CLASS));
		memcpy (&MdnAdrAng, MdnAdr, sizeof (ADR_CLASS));
		memcpy (&FilAdrAng, FilAdr, sizeof (ADR_CLASS));
		memcpy (&KunAdrAng, KunAdr, sizeof (ADR_CLASS));
	}
	else if (Page == PageAuftrag)
	{
		Form->Get ();
		FormAuf = Form;
	    CalcFormAuf = CalcForm;
		memcpy (&PartykAuf, Partyk, sizeof (PARTYK_CLASS));
		memcpy (&MdnAuf, Mdn, sizeof (MDN_CLASS));
		memcpy (&FilAuf, Fil, sizeof (FIL_CLASS));
		memcpy (&KunAuf, Kun, sizeof (KUN_CLASS));
		memcpy (&MdnAdrAuf, MdnAdr, sizeof (ADR_CLASS));
		memcpy (&FilAdrAuf, FilAdr, sizeof (ADR_CLASS));
		memcpy (&KunAdrAuf, KunAdr, sizeof (ADR_CLASS));
	}
}

void CAngebotCore::TablesToAng ()
{
	Form->Get ();
	Form = FormAng;
	CalcForm = CalcFormAng;
	RepaintCalcForm = TRUE;

	memcpy (Partyk, &PartykAng, sizeof (PARTYK_CLASS));
	memcpy (Mdn, &MdnAng, sizeof (MDN_CLASS));
	memcpy (Fil, &FilAng, sizeof (FIL_CLASS));
	memcpy (Kun, &KunAng, sizeof (KUN_CLASS));
	memcpy (MdnAdr, &MdnAdrAng, sizeof (ADR_CLASS));
	memcpy (FilAdr, &FilAdrAng, sizeof (ADR_CLASS));
	memcpy (KunAdr, &KunAdrAng, sizeof (ADR_CLASS));
	if (PosReaded)
	{
		ReadPositions ();
	}
	Form->Show ();
}

void CAngebotCore::TablesToAuf ()
{
	Form->Get ();
	Form = FormAuf;
	CalcForm = CalcFormAuf;
	RepaintCalcForm = TRUE;

	memcpy (Partyk, &PartykAuf, sizeof (PARTYK_CLASS));
	memcpy (Mdn, &MdnAuf, sizeof (MDN_CLASS));
	memcpy (Fil, &FilAuf, sizeof (FIL_CLASS));
	memcpy (Kun, &KunAuf, sizeof (KUN_CLASS));
	memcpy (MdnAdr, &MdnAdrAuf, sizeof (ADR_CLASS));
	memcpy (FilAdr, &FilAdrAuf, sizeof (ADR_CLASS));
	memcpy (KunAdr, &KunAdrAuf, sizeof (ADR_CLASS));
	if (PosReaded)
	{
		ReadPositions ();
	}
	Form->Show ();
}

void CAngebotCore::RegisterShowCalcSmt (CRunMessage * ShowSmt)
{
	ShowCalcSmt->Add (ShowSmt);
}

void CAngebotCore::RegisterRepaintCalcSmt (CRunMessage * ShowSmt)
{
	RepaintCalcSmt->Add (ShowSmt);
}

void CAngebotCore::Drop (double a)
{
	PARTYP **it;
	PARTYP *p;

	for (int i = 0 ; i < List->anz; i ++)
	{
		it = List->Get (i);
		if (it != NULL)
		{
			p = *it;
			if (p->a == a)
			{
				return;
			}
		}
	}
	ABasList->Drop (a);
}

double CAngebotCore::GetLastEk (int pos)
{
	double LastEk = 0.0;
	if (LastEks != NULL)
	{
		int eks = LastEks->anz;
		if (pos < eks)
		{
			LastEk = LastEks->GetElement (pos);
		}
	}
	return LastEk;
}

CString& CAngebotCore::GetCheckBoxValue (LPSTR source, CString& target)
{
	CString Source = source;
	target = _T(" ");
	Source.MakeUpper ();
	if (Source == _T("J"))
	{
		target = _T("X");
	}
	return target;
}

CString& CAngebotCore::GetCheckBoxValue (int source, CString& target)
{
	target = _T(" ");
	if (source != 0)
	{
		target = _T("X");
	}
	return target;
}
