#pragma once

#include "Partyk.h"
#include "Partyp.h"
#include "A_bas.h"
#include "DataCollection.h"

class CCalcSmt
{
public:
	CCalcSmt(void);
	CCalcSmt (short smt);
public:
	~CCalcSmt(void);
private:
	short smt;
	CString SmtBez;
	double AngGew;
	double AngVk;
	double AngSingleVk;
	double OSGPrice;
	double OSSPrice;
	CDataCollection<PARTYP *> *List;
	PARTYK_CLASS *Partyk;
	PARTYP_CLASS *Partyp;
	A_BAS_CLASS *A_bas;
	double m_VatFull;
	double m_VatReduced;

public:
	void SetSmt (short smt);
	short GetSmt ()
	{
		return smt;
	}

	CString& GetSmtBez ()
	{
		return SmtBez;
	}

	LPTSTR GetSmtPBez ()
	{
		return SmtBez.GetBuffer ();
	}

	void SetAngGew (double AngGew)
	{
		this->AngGew = AngGew;
	}

	double GetAngGew ()
	{
		return AngGew;
	}

	void SetAngVk (double AngVk)
	{
		this->AngVk = AngVk;
	}

	double *GetAngVk ()
	{
		return &AngVk;
	}

	void SetAngSingleVk (double AngSingleVk)
	{
		this->AngSingleVk = AngSingleVk;
	}

	double *GetAngSingleVk ()
	{
		return &AngSingleVk;
	}

	void SetOSGPrice (double OSGPrice)
	{
		this->OSGPrice = OSGPrice;
	}

	double *GetOSGPrice ()
	{
		return &OSGPrice;
	}

	void SetOSSPrice (double OSGPrice)
	{
		this->OSSPrice = OSSPrice;
	}

	double *GetOSSPrice ()
	{
		return &OSSPrice;
	}
    void Calculate ();
};
