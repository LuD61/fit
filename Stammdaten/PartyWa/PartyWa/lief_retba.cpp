#include "stdafx.h"
#include "lief_retba.h"

struct LIEF_RETBA lief_retba, lief_retba_null, lief_retba_def;

void LIEF_RETBA_CLASS::prepare (void)
{
            TCHAR *sqltext;

    	    sqlin ((long *) &lief_retba.nr,SQLLONG,0);
    	    sqlin ((TCHAR *) lief_retba.blg_typ,SQLCHAR,2);
    	    sqlin ((short *) &lief_retba.mdn,SQLSHORT,0);
    	    sqlin ((short *) &lief_retba.fil,SQLSHORT,0);
	    sqlin ((short *) &lief_retba.kun_fil,SQLSHORT,0);
    sqlout ((short *) &lief_retba.delstatus,SQLSHORT,0);
    sqlout ((long *) &lief_retba.nr,SQLLONG,0);
    sqlout ((TCHAR *) lief_retba.blg_typ,SQLCHAR,2);
    sqlout ((short *) &lief_retba.mdn,SQLSHORT,0);
    sqlout ((short *) &lief_retba.fil,SQLSHORT,0);
    sqlout ((long *) &lief_retba.kun,SQLLONG,0);
    sqlout ((short *) &lief_retba.kun_fil,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &lief_retba.dat,SQLDATE,0);
    sqlout ((TCHAR *) lief_retba.pers,SQLCHAR,13);
    sqlout ((long *) &lief_retba.vertr,SQLLONG,0);
    sqlout ((TCHAR *) lief_retba.err_txt,SQLCHAR,17);
    sqlout ((short *) &lief_retba.zutyp,SQLSHORT,0);
            cursor = sqlcursor (_T("select ")
_T("lief_retba.delstatus,  lief_retba.nr,  lief_retba.blg_typ,  ")
_T("lief_retba.mdn,  lief_retba.fil,  lief_retba.kun,  lief_retba.kun_fil,  ")
_T("lief_retba.dat,  lief_retba.pers,  lief_retba.vertr,  ")
_T("lief_retba.err_txt,  lief_retba.zutyp from lief_retba ")

#line 16 "lief_retba.rpp"
                                  _T("where nr = ? ")
                                  _T("and blg_typ = ? ")
                                  _T("and mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ?"));
    sqlin ((short *) &lief_retba.delstatus,SQLSHORT,0);
    sqlin ((long *) &lief_retba.nr,SQLLONG,0);
    sqlin ((TCHAR *) lief_retba.blg_typ,SQLCHAR,2);
    sqlin ((short *) &lief_retba.mdn,SQLSHORT,0);
    sqlin ((short *) &lief_retba.fil,SQLSHORT,0);
    sqlin ((long *) &lief_retba.kun,SQLLONG,0);
    sqlin ((short *) &lief_retba.kun_fil,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &lief_retba.dat,SQLDATE,0);
    sqlin ((TCHAR *) lief_retba.pers,SQLCHAR,13);
    sqlin ((long *) &lief_retba.vertr,SQLLONG,0);
    sqlin ((TCHAR *) lief_retba.err_txt,SQLCHAR,17);
    sqlin ((short *) &lief_retba.zutyp,SQLSHORT,0);
            sqltext = _T("update lief_retba set ")
_T("lief_retba.delstatus = ?,  lief_retba.nr = ?,  ")
_T("lief_retba.blg_typ = ?,  lief_retba.mdn = ?,  lief_retba.fil = ?,  ")
_T("lief_retba.kun = ?,  lief_retba.kun_fil = ?,  lief_retba.dat = ?,  ")
_T("lief_retba.pers = ?,  lief_retba.vertr = ?,  ")
_T("lief_retba.err_txt = ?,  lief_retba.zutyp = ? ")

#line 22 "lief_retba.rpp"
                                  _T("where nr = ? ")
                                  _T("and blg_typ = ? ")
                                  _T("and mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ?");
    	    sqlin ((long *) &lief_retba.nr,SQLLONG,0);
    	    sqlin ((TCHAR *) lief_retba.blg_typ,SQLCHAR,2);
    	    sqlin ((short *) &lief_retba.mdn,SQLSHORT,0);
    	    sqlin ((short *) &lief_retba.fil,SQLSHORT,0);
    	    sqlin ((long *) &lief_retba.kun,SQLLONG,0);
	    sqlin ((short *) &lief_retba.kun_fil,SQLSHORT,0);
            upd_cursor = sqlcursor (sqltext);

    	    sqlin ((long *) &lief_retba.nr,SQLLONG,0);
    	    sqlin ((TCHAR *) lief_retba.blg_typ,SQLCHAR,2);
    	    sqlin ((short *) &lief_retba.mdn,SQLSHORT,0);
    	    sqlin ((short *) &lief_retba.fil,SQLSHORT,0);
    	    sqlin ((long *) &lief_retba.kun,SQLLONG,0);
	    sqlin ((short *) &lief_retba.kun_fil,SQLSHORT,0);
            test_upd_cursor = sqlcursor (_T("select nr from lief_retba ")
                                  _T("where nr = ? ")
                                  _T("and blg_typ = ? ")
                                  _T("and mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ?"));
    	    sqlin ((long *) &lief_retba.nr,SQLLONG,0);
    	    sqlin ((TCHAR *) lief_retba.blg_typ,SQLCHAR,2);
    	    sqlin ((short *) &lief_retba.mdn,SQLSHORT,0);
    	    sqlin ((short *) &lief_retba.fil,SQLSHORT,0);
    	    sqlin ((long *) &lief_retba.kun,SQLLONG,0);
	    sqlin ((short *) &lief_retba.kun_fil,SQLSHORT,0);
            test_lock_cursor = sqlcursor (_T("update lief_retba set delstatus = 0 ")
                                  _T("where nr = ? ")
                                  _T("and blg_typ = ? ")
                                  _T("and mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ?"));
    	    sqlin ((long *) &lief_retba.nr,SQLLONG,0);
    	    sqlin ((TCHAR *) lief_retba.blg_typ,SQLCHAR,2);
    	    sqlin ((short *) &lief_retba.mdn,SQLSHORT,0);
    	    sqlin ((short *) &lief_retba.fil,SQLSHORT,0);
    	    sqlin ((long *) &lief_retba.kun,SQLLONG,0);
	    sqlin ((short *) &lief_retba.kun_fil,SQLSHORT,0);
            del_cursor = sqlcursor (_T("delete from lief_retba ")
                                  _T("where nr = ? ")
                                  _T("and blg_typ = ? ")
                                  _T("and mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ?"));
    sqlin ((short *) &lief_retba.delstatus,SQLSHORT,0);
    sqlin ((long *) &lief_retba.nr,SQLLONG,0);
    sqlin ((TCHAR *) lief_retba.blg_typ,SQLCHAR,2);
    sqlin ((short *) &lief_retba.mdn,SQLSHORT,0);
    sqlin ((short *) &lief_retba.fil,SQLSHORT,0);
    sqlin ((long *) &lief_retba.kun,SQLLONG,0);
    sqlin ((short *) &lief_retba.kun_fil,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &lief_retba.dat,SQLDATE,0);
    sqlin ((TCHAR *) lief_retba.pers,SQLCHAR,13);
    sqlin ((long *) &lief_retba.vertr,SQLLONG,0);
    sqlin ((TCHAR *) lief_retba.err_txt,SQLCHAR,17);
    sqlin ((short *) &lief_retba.zutyp,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into lief_retba (")
_T("delstatus,  nr,  blg_typ,  mdn,  fil,  kun,  kun_fil,  dat,  pers,  vertr,  err_txt,  zutyp) ")

#line 72 "lief_retba.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?)"));

#line 74 "lief_retba.rpp"
}
