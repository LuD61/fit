// MainFrm.h : Schnittstelle der Klasse CMainFrame
//


#pragma once

#include "WaSplitterWnd.h"
#include "AngebotView.h"
#include "PartyService.h"
#include "FavoriteView.h"
#include "ToolbarButton.h"

class CMainFrame : public CFrameWnd
{
	
protected: // Nur aus Serialisierung erstellen
	class CExitMessage : public CRunMessage
	{
	private: 
		CMainFrame *p;
	public: 
			CExitMessage (CMainFrame *p);
			virtual void Run ();
	};

	class CShowCalcView : public CRunMessage
	{
	private: 
		CMainFrame *p;
	public: 
			CShowCalcView (CMainFrame *p);
			virtual void Run ();
	};

	class CHideCalcView : public CRunMessage
	{
	private: 
		CMainFrame *p;
	public: 
			CHideCalcView (CMainFrame *p);
			virtual void Run ();
	};

	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attribute
public:

// Vorg�nge
public:

// �berschreibungen
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementierung
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // Eingebettete Member der Steuerleiste
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CToolBar    m_wndToolBar1;
	CToolBar    m_wndToolBar2;
	CToolBar    m_wndToolBar3;
//	CButton     m_wndCalcView;
	CToolbarButton     m_wndCalcView;
	CColorButton m_wndPosText;
	CColorButton m_wndKopfText;
	CColorButton m_wndFussText;
	CColorButton m_wndPrintAng;
	CColorButton m_wndPrintPack;
	CColorButton m_wndPrintAuf;
	CColorButton m_wndPrintLief;
	CColorButton m_wndPrintRech;
	CFont Font;
	int m_ToolbarRows;

// Generierte Funktionen f�r die Meldungstabellen
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg BOOL OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext);
	DECLARE_MESSAGE_MAP()
private:
	CMessageHandler *MessageHandler;
	CExitMessage *ExitMessage;
	CShowCalcView *ShowCalcView;
	CHideCalcView *HideCalcView;
	CWaSplitterWnd *m_Splitter; 
	CWaSplitterWnd *m_LeftSplitter; 
	virtual void OnClose ();
	afx_msg void OnBack();
	afx_msg void OnFileSave();
	afx_msg void OnInsert();
	afx_msg void OnUpdateInsert(CCmdUI *pCmdUI);
	afx_msg void OnDelete();
	afx_msg void OnUpdateDelete(CCmdUI *pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateAppExit(CCmdUI *pCmdUI);
	afx_msg void OnDeleteAng();
	afx_msg void OnPrintAng();
	afx_msg void OnPrintPack();
	afx_msg void OnPrintAuftrag();
	afx_msg void OnUpdateDeleteAng(CCmdUI *pCmdUI);
	afx_msg void OnUpdatePrintAng(CCmdUI *pCmdUI);
	afx_msg void OnUpdatePrintPack(CCmdUI *pCmdUI);
	afx_msg void OnUpdatePrintAuftrag(CCmdUI *pCmdUI);
	afx_msg void OnCalcView();
	afx_msg void OnHideCalcView();
	afx_msg void OnHideOnlyCalcView();
	afx_msg void OnPosText();
	afx_msg void OnUpdatePosText(CCmdUI *pCmdUI);
	afx_msg void OnPrintLief();
	afx_msg void OnUpdatePrintLief(CCmdUI *pCmdUI);
	afx_msg void OnPrintRech();
	afx_msg void OnUpdatePrintRech(CCmdUI *pCmdUI);
	afx_msg void OnKopfText();
	afx_msg void OnUpdateKopfText(CCmdUI *pCmdUI);
	afx_msg void OnFussText();
	afx_msg void OnUpdateFussText(CCmdUI *pCmdUI);
	afx_msg BOOL OnBarCheck(UINT nID);
	afx_msg void OnUpdateToolBar1 ( CCmdUI * pCmdUI );
	afx_msg void OnUpdateToolBar2 ( CCmdUI * pCmdUI );
};


