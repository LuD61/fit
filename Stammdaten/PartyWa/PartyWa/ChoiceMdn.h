#ifndef _CHOICEMDN_DEF
#define _CHOICEMDN_DEF

#include "ChoiceX.h"
#include "MdnList.h"
#include <vector>

class CChoiceMdn : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
      
    public :
	    std::vector<CMdnList *> MdnList;
      	CChoiceMdn(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceMdn(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchAdrKrz (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CMdnList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
