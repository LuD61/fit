#pragma once
#include "colorbutton.h"
#include "Resource.h"

class COpenColorButton :
	public CColorButton
{
public:
	COpenColorButton(void);
public:
	~COpenColorButton(void);
    void Init ();
private:
	CString m_Text;
	BOOL Initialized;
	BOOL IsInizializing;
protected:
	virtual void DrawItem (LPDRAWITEMSTRUCT  lpDrawItemStruct);
public:
	void SetText (LPTSTR Text)
	{
		m_Text = Text;
	}
	void SetText (CString& Text)
	{
		m_Text = Text;
	}

};
