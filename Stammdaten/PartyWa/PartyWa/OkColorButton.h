#pragma once
#include "colorbutton.h"

class COkColorButton :
	public CColorButton
{
public:
	COkColorButton(void);
public:
	~COkColorButton(void);
    void Init ();
private:
	BOOL Initialized;
	BOOL IsInizializing;
protected:
	virtual void DrawItem (LPDRAWITEMSTRUCT  lpDrawItemStruct);
};
