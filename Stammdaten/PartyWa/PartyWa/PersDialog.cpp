// PersDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "PartyWa.h"
#include "PersDialog.h"


// CPersDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CPersDialog, CDialog)

CPersDialog::CPersDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPersDialog::IDD, pParent)
{
	DataPos = 2;
	m_DlgBrush = NULL;
	m_DlgColor = RGB (250, 250, 202);
	BkBitmap.SetBkType (BkBitmap.DynamicGrey);
//	BkBitmap.SetBkType (BkBitmap.DynamicMetal);
//	BkBitmap.SetDirection (BkBitmap.Vertical);
}

CPersDialog::~CPersDialog()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
}

void CPersDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAPTION, m_Caption);
	DDX_Control(pDX, IDC_LPERS, m_LPers);
	DDX_Control(pDX, IDC_PERS, m_Pers);
	DDX_Control(pDX, IDC_LNAME1, m_LName);
	DDX_Control(pDX, IDC_NAME1, m_Name1);
	DDX_Control(pDX, IDC_NAME2,  m_Name2);
	DDX_Control(pDX, IDC_LSTR1, m_LStr);
	DDX_Control(pDX, IDC_STR, m_Str);
	DDX_Control(pDX, IDC_LPLZ_ORT, m_LPlzOrt);
	DDX_Control(pDX, IDC_PLZ, m_Plz);
	DDX_Control(pDX, IDC_ORT1, m_Ort1);
	DDX_Control(pDX, IDC_ORT2, m_Ort2);
	DDX_Control(pDX, IDC_LTEL, m_LTel);
	DDX_Control(pDX, IDC_TEL, m_Tel);
	DDX_Control(pDX, IDC_LFAX1, m_LFax);
	DDX_Control(pDX, IDC_FAX, m_Fax);
	DDX_Control(pDX, IDC_LEMAIL, m_LEmail);
	DDX_Control(pDX, IDC_EMAIL, m_Email);
	DDX_Control(pDX, IDC_LGEB_DAT, m_LGebDat);
	DDX_Control(pDX, IDC_GEB_DAT, m_GebDat);
	DDX_Control(pDX, IDC_GROUP_ADR, m_GroupAdr);
	DDX_Control(pDX, IDC_GEB_DAT3, m_GebDat2);
	DDX_Control(pDX, IDC_OKEX, m_OkEx);
	DDX_Control(pDX, IDC_CANCELEX2, m_CancelEx);
	DDX_Control(pDX, IDC_NEW2, m_New);
}


BEGIN_MESSAGE_MAP(CPersDialog, CDialog)
	ON_WM_CTLCOLOR ()
	ON_BN_CLICKED(IDC_NEW, &CPersDialog::OnBnClickedNew)
	ON_EN_KILLFOCUS(IDC_PERS, &CPersDialog::OnEnKillfocusPers)
	ON_BN_CLICKED(IDC_OKEX, OnOK)
	ON_BN_CLICKED(IDC_CANCELEX, OnCancel)
END_MESSAGE_MAP()


// CPersDialog-Meldungshandler

BOOL CPersDialog::OnInitDialog ()
{
	CDialog::OnInitDialog();

	m_Caption.SetParts (60);
	m_Caption.SetWindowText (_T("Personal bearbeiten"));
	m_Caption.SetBoderStyle (m_Caption.Solide);
	m_Caption.SetOrientation (m_Caption.Left);
	m_Caption.SetDynamicColor (TRUE);
	m_Caption.SetOrientation (m_Caption.Center);
	m_Caption.SetTextColor (RGB (255, 255, 153));
	m_Caption.SetBkColor (RGB (192, 192, 192));

	COLORREF BkColor (RGB (0, 0, 255));
//	COLORREF DynColor = RGB (0, 0, 255);
	COLORREF RolloverColor = CColorButton::DynColorBlue;
	COLORREF DynColor = CColorButton::DynColorBlue;

	m_OkEx.SetBkColor (BkColor);
	m_OkEx.DynColor = DynColor;
	m_OkEx.RolloverColor = RolloverColor;

	m_CancelEx.SetBkColor (BkColor);
	m_CancelEx.DynColor = DynColor;
	m_CancelEx.RolloverColor = RolloverColor;

	m_New.SetWindowText (_T("Neu"));
	m_New.nID = IDC_NEW;
	m_New.TextStyle = m_New.Standard;
	m_New.BorderStyle = m_New.Solide;
	m_New.DynamicColor = TRUE;
	m_New.TextColor =RGB (0, 0, 0);
	m_New.SetBkColor (BkColor);
	m_New.DynColor = DynColor;
	m_New.RolloverColor = RolloverColor;
	m_New.LoadBitmap (IDB_NEW);

/*
	m_OkEx.Orientation = m_OkEx.Center;
	m_OkEx.TextStyle = m_OkEx.Standard;
	m_OkEx.BorderStyle = m_OkEx.Solide;
	m_OkEx.DynamicColor = TRUE;
	m_OkEx.TextColor =RGB (0, 0, 0);
	m_OkEx.SetBkColor (BkColor);
	m_OkEx.DynColor = DynColor;
	m_OkEx.RolloverColor = RolloverColor;
	m_OkEx.LoadBitmap (IDB_OKEX);
	m_OkEx.LoadMask (IDB_OKEX_MASK);
*/

	GetDlgItem (IDOK)->ShowWindow (SW_HIDE);
	GetDlgItem (IDCANCEL)->ShowWindow (SW_HIDE);
	GetDlgItem (IDC_NEW)->ShowWindow (SW_HIDE);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	memcpy (&Adr.adr, &adr_null, sizeof (ADR));
	m_LPers.ShowWindow (SW_HIDE);
	m_Pers.ShowWindow (SW_HIDE);
	Form.Add (new CFormField (&m_Pers,EDIT,      (char *)   Pers.pers.pers, VCHAR));
	Form.Add (new CFormField (&m_Name1,EDIT,     (char *)   Adr.adr.adr_nam1, VCHAR));
	Form.Add (new CFormField (&m_Name2,EDIT,     (char *)   Adr.adr.adr_nam2, VCHAR));
	Form.Add (new CFormField (&m_Str,EDIT,       (char *)   Adr.adr.str, VCHAR));
	Form.Add (new CFormField (&m_Plz,EDIT,       (char *)   Adr.adr.plz, VCHAR));
	Form.Add (new CFormField (&m_Ort1,EDIT,      (char *)   Adr.adr.ort1, VCHAR));
	Form.Add (new CFormField (&m_Ort2,EDIT,      (char *)   Adr.adr.ort2, VCHAR));
	Form.Add (new CFormField (&m_Tel,EDIT,       (char *)   Adr.adr.tel, VCHAR));
	Form.Add (new CFormField (&m_Fax,EDIT,       (char *)   Adr.adr.fax, VCHAR));
	Form.Add (new CFormField (&m_Email,EDIT,     (char *)   Adr.adr.email, VCHAR));
//	Form.Add (new CFormField (&m_GebDat,EDIT,    (DATE_STRUCT *)  &Adr.adr.geb_dat, VDATE));
	Form.Add (new CFormField (&m_GebDat2, DATETIMEPICKER,   (DATE_STRUCT *)  &Adr.adr.geb_dat, VDATE));
	m_GebDat.ShowWindow (SW_HIDE);
	m_GebDat2.ModifyStyle (DTS_UPDOWN, 0);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (20);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

//Grid Plz/Ort
	PlzOrtGrid.Create (this, 5, 5);
    PlzOrtGrid.SetBorder (0, 0);
    PlzOrtGrid.SetGridSpace (5, 8);
	CCtrlInfo *c_Plz = new CCtrlInfo (&m_Plz, 0, 0, 1, 1);
	PlzOrtGrid.Add (c_Plz);
	CCtrlInfo *c_Ort1 = new CCtrlInfo (&m_Ort1, 1, 0, 1, 1);
	PlzOrtGrid.Add (c_Ort1);
	CCtrlInfo *c_Ort2 = new CCtrlInfo (&m_Ort2, 1, 1, 1, 1);
	PlzOrtGrid.Add (c_Ort2);

//Grid Telefon Fax
	TeleGrid.Create (this, 5, 5);
    TeleGrid.SetBorder (0, 0);
    TeleGrid.SetGridSpace (5, 8);
	CCtrlInfo *c_Tel = new CCtrlInfo (&m_Tel, 0, 0, 1, 1);
	TeleGrid.Add (c_Tel);
	CCtrlInfo *c_LFax = new CCtrlInfo (&m_LFax, 1, 0, 1, 1);
	TeleGrid.Add (c_LFax);
	CCtrlInfo *c_Fax = new CCtrlInfo (&m_Fax, 2, 0, 1, 1);
	TeleGrid.Add (c_Fax);

	CString cPers = Pers.pers.pers;
	cPers.TrimRight ();
	if (cPers.GetLength () == 0)
	{
		PostMessage (WM_COMMAND, IDC_NEW, 0l);
	}

	CreateGrid ();

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	StaticFont.CreateFontIndirect (&l);
	l.lfHeight = 25;
	CaptionFont.CreateFontIndirect (&l);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Caption.SetFont (&CaptionFont);

	CtrlGrid.Display ();
	read ();
	m_Name1.SetFocus ();
	return FALSE;
}

BOOL CPersDialog::PreTranslateMessage(MSG* pMsg)
{
	CWnd *Control = GetFocus ();

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
					return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				OnOK ();
				return TRUE;
			}
	}
	return CDialog::PreTranslateMessage (pMsg);
}

HBRUSH CPersDialog::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	CRect rect;

	if (nCtlColor == CTLCOLOR_DLG)
	{
		    if (m_DlgBrush == NULL)
			{
//				m_DlgBrush = CreateSolidBrush (m_DlgColor);
                GetClientRect (&rect);
				BkBitmap.SetWidth (rect.right); 
				BkBitmap.SetHeight (rect.bottom);
				BkBitmap.SetPlanes (32);
				CBitmap *bitmap = BkBitmap.Create (); 
				m_DlgBrush = CreatePatternBrush (*bitmap);
				bitmap->DeleteObject ();
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC)
	{
		    if (m_DlgBrush == NULL)
			{
				m_DlgBrush = CreateSolidBrush (m_DlgColor);
                GetClientRect (&rect);
				BkBitmap.SetWidth (rect.right); 
				BkBitmap.SetHeight (rect.bottom);
				BkBitmap.SetPlanes (32);
				CBitmap *bitmap = BkBitmap.Create (); 
				m_DlgBrush = CreatePatternBrush (*bitmap);
				bitmap->DeleteObject ();
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}


BOOL CPersDialog::read ()
{	 
	 int dsqlstatus = 0;
	 BOOL ret = FALSE;
	 dsqlstatus = Pers.dbreadfirst ();
	 if (dsqlstatus == 0)
	 {
		 Adr.adr.adr = Pers.pers.adr;
		 Adr.dbreadfirst ();
		 if (Adr.adr.geb_dat.year < 0 ||
             Adr.adr.geb_dat.month < 1 ||  
             Adr.adr.geb_dat.day < 1 ||  
             Adr.adr.geb_dat.month > 12 ||  
             Adr.adr.geb_dat.day > 31  
			 )
		 {
			 Adr.adr.geb_dat.day = 1;
			 Adr.adr.geb_dat.month = 1;
			 Adr.adr.geb_dat.year = 1900;
		 }
		 ret = TRUE;
	 }
	 Form.Show ();
     return ret;
}

BOOL CPersDialog::write ()
{	
	BOOL ret = FALSE;
	Form.Get ();
	if (Pers.pers.eintr_dat.year < 0 ||
         Pers.pers.eintr_dat.month < 1 ||  
         Pers.pers.eintr_dat.day < 1 ||  
         Pers.pers.eintr_dat.month > 12 ||  
         Pers.pers.eintr_dat.day > 31  
		 )
	 {
		 Pers.pers.eintr_dat.day = 1;
		 Pers.pers.eintr_dat.month = 1;
		 Pers.pers.eintr_dat.year = 1900;
	}
	PERS_CLASS TPers;
	memcpy (&TPers.pers, &Pers.pers, sizeof (Pers.pers));
    int dsqlstatus = TPers.dbreadfirst ();
	if (dsqlstatus != 0)
	{
		Pers.pers.adr = atoi (Pers.pers.pers);
		ADR_CLASS TAdr;
		TAdr.adr.adr = Pers.pers.adr;
        dsqlstatus = TAdr.dbreadfirst ();  
		if (dsqlstatus != 100)
		{
			TAdr.sqlout ((long *) &Pers.pers.adr, SQLLONG, 0);
			TAdr.sqlcomm (_T("select max (adr) + 1 from adr"));
		}
	}
    Adr.adr.adr = Pers.pers.adr;
	Pers.dbupdate ();
	memset (Adr.adr.adr_krz, 0, sizeof (Adr.adr.adr_krz));
	strncpy (Adr.adr.adr_krz, Adr.adr.adr_nam1, sizeof (Adr.adr.adr_krz) - 1);
	Adr.adr.adr_typ = 10;
	Adr.dbupdate ();
    return ret;
}

void CPersDialog::OnOK ()
{
	write ();
	CDialog::OnOK ();
}


void CPersDialog::CreateGrid ()
{
//	CtrlGrid.DestroyGrid ();
	CtrlGrid.DestroyAll ();

	CCtrlInfo *c_Caption = new CCtrlInfo (&m_Caption, 0, 0, DOCKRIGHT, 1);
    CtrlGrid.Add (c_Caption);

	CCtrlInfo *c_LPers = new CCtrlInfo (&m_LPers, 2, 2, 1, 1);
	CtrlGrid.Add (c_LPers);
	CCtrlInfo *c_Pers = new CCtrlInfo (&m_Pers, 3, 2, 1, 1);
	CtrlGrid.Add (c_Pers);
	CCtrlInfo *c_GroupAdr = new CCtrlInfo (&m_GroupAdr, 1, DataPos + 0, 20, 20);
	CtrlGrid.Add (c_GroupAdr);
	CCtrlInfo *c_LName = new CCtrlInfo (&m_LName, 2, DataPos + 1, 1, 1);
	CtrlGrid.Add (c_LName);
	CCtrlInfo *c_Name1 = new CCtrlInfo (&m_Name1, 3, DataPos + 1, 1, 1);
	CtrlGrid.Add (c_Name1);
	CCtrlInfo *c_Name2 = new CCtrlInfo (&m_Name2, 4, DataPos + 1, 1, 1);
	CtrlGrid.Add (c_Name2);
	CCtrlInfo *c_LStr = new CCtrlInfo (&m_LStr, 2, DataPos + 2, 1, 1);
	CtrlGrid.Add (c_LStr);
	CCtrlInfo *c_Str = new CCtrlInfo (&m_Str, 3, DataPos + 2, 1, 1);
	CtrlGrid.Add (c_Str);
	CCtrlInfo *c_LPlzOrt = new CCtrlInfo (&m_LPlzOrt, 2, DataPos + 3, 1, 1);
	CtrlGrid.Add (c_LPlzOrt);
	CCtrlInfo *c_PlzOrtGrid = new CCtrlInfo (&PlzOrtGrid, 3, DataPos + 3, 5, 2);
	CtrlGrid.Add (c_PlzOrtGrid);
	CCtrlInfo *c_LTel = new CCtrlInfo (&m_LTel, 2, DataPos + 6, 1, 1);
	CtrlGrid.Add (c_LTel);
	CCtrlInfo *c_TeleGrid = new CCtrlInfo (&TeleGrid, 3, DataPos + 6, 5, 1);
	CtrlGrid.Add (c_TeleGrid);
	CCtrlInfo *c_LEmail = new CCtrlInfo (&m_LEmail, 2, DataPos + 7, 1, 1);
	CtrlGrid.Add (c_LEmail);
	CCtrlInfo *c_Email = new CCtrlInfo (&m_Email, 3, DataPos + 7, 1, 1);
	CtrlGrid.Add (c_Email);
	CCtrlInfo *c_LGebDat = new CCtrlInfo (&m_LGebDat, 2, DataPos + 8, 1, 1);
	CtrlGrid.Add (c_LGebDat);
//	CCtrlInfo *c_GebDat = new CCtrlInfo (&m_GebDat, 3, 8, 1, 1);
//	CtrlGrid.Add (c_GebDat	);
	CCtrlInfo *c_GebDat2 = new CCtrlInfo (&m_GebDat2, 3, DataPos + 8, 1, 1);
	CtrlGrid.Add (c_GebDat2	);

	CtrlGrid.SetCellHeight (20);
	if (DataPos > 0)
	{
		CRect rect;
		GetWindowRect (&rect);
		rect.bottom += DataPos * 20;
		MoveWindow (&rect);
	}

	CtrlGrid.Display ();
	Invalidate ();
}

void CPersDialog::SetPers (PERS *pers)
{
	memcpy (&Pers.pers, pers, sizeof (PERS));
}
void CPersDialog::OnBnClickedNew()
{
	// TODO: Add your control notification handler code here
	m_LPers.ShowWindow (SW_SHOWNORMAL);
	m_Pers.ShowWindow (SW_SHOWNORMAL);
	memcpy (&Pers.pers, &pers_null, sizeof (PERS));
	memcpy (&Adr.adr, &adr_null, sizeof (ADR));
	Form.Show ();
	DataPos = 1;
	CreateGrid ();
	m_Pers.SetFocus ();
}

void CPersDialog::OnEnKillfocusPers()
{
	// TODO: Add your control notification handler code here
	Form.Get ();
	short mdn = Pers.pers.mdn;
	short fil = Pers.pers.fil;
	CString cPers = Pers.pers.pers;
	memcpy (&Pers.pers, &pers_null, sizeof (PERS));
	memcpy (&Adr.adr, &adr_null, sizeof (ADR));
	Pers.pers.mdn = mdn;
	Pers.pers.fil = fil;
	strcpy (Pers.pers.pers, cPers.GetBuffer ());
	int dsqlstatus = Pers.dbreadfirst ();
	Form.Show ();
}
