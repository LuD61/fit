#ifndef _ADR_LIST_DEF
#define _ADR_LIST_DEF
#pragma once

class CAdrList
{
public:
	
	long adr;
	CString adr_krz;
	CString adr_nam1;
	CString adr_nam2;
	CString plz;
	CString ort1;
	CString str;
	CAdrList(void);
	CAdrList(long, LPTSTR);
	~CAdrList(void);
};
#endif
