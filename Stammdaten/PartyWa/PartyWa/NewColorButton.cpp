#include "StdAfx.h"
#include "NewColorButton.h"

CNewColorButton::CNewColorButton(void)
{
	Initialized = FALSE;
	IsInizializing = FALSE;
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = CColorButton::DynColorGray;
	COLORREF ActiveColor = CColorButton::DynColorGray;

	m_Text = _T("Neu");
	nID = IDC_NEW;
	Orientation = Center;
//	xSpace = 2;
	TextStyle = Standard;
	BorderStyle = Solide;
//	BorderStyle = NoBorder;
	DynamicColor = TRUE;
	TextColor =RGB (0, 0, 0);
	SetBkColor (BkColor);
	DynColor = DynColor;
	RolloverColor = RolloverColor;
//	Transparent = TRUE;
	LoadBitmap (IDB_NEW);
}

CNewColorButton::~CNewColorButton(void)
{
}

void CNewColorButton::Init() 
{
	if (!Initialized && !IsInizializing)
	{
		IsInizializing = TRUE;
		Initialized = TRUE;
		SetWindowText (m_Text);
		IsInizializing = FALSE;
	}
}

void CNewColorButton::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	Init ();
	CColorButton::DrawItem (lpDrawItemStruct);
}
