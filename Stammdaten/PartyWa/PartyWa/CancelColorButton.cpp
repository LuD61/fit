#include "StdAfx.h"
#include "CancelColorButton.h"

CCancelColorButton::CCancelColorButton(void)
{
	Initialized = FALSE;
	IsInizializing = FALSE;
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = CColorButton::DynColorGray;
	COLORREF ActiveColor = CColorButton::DynColorGray;

	nID = IDC_CANCELEX;
	Orientation = Center;
	TextStyle = Standard;
	BorderStyle = Solide;
	DynamicColor = TRUE;
	TextColor =RGB (0, 0, 0);
	SetBkColor (BkColor);
	DynColor = DynColor;
	RolloverColor = RolloverColor;
	LoadBitmap (IDB_CANCELEX);
}

CCancelColorButton::~CCancelColorButton(void)
{
}

void CCancelColorButton::Init() 
{
	if (!Initialized && !IsInizializing)
	{
		IsInizializing = TRUE;
		Initialized = TRUE;
		SetWindowText (_T("Abbrechen"));
		IsInizializing = FALSE;
	}
}

void CCancelColorButton::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	Init ();
	CColorButton::DrawItem (lpDrawItemStruct);
}
