#include "StdAfx.h"
#include <stdio.h>
#include "angpt.h"

struct ANGPT angpt, angpt_null;

void ANGPT_CLASS::prepare (void)
{
            char *sqltext;

            sqlin ((long *) &angpt.nr, SQLLONG, 0);
    sqlout ((long *) &angpt.nr,SQLLONG,0);
    sqlout ((long *) &angpt.zei,SQLLONG,0);
    sqlout ((TCHAR *) angpt.txt,SQLCHAR,61);
            cursor = sqlcursor (_T("select angpt.nr,  angpt.zei,  ")
_T("angpt.txt from angpt ")

#line 13 "angpt.rpp"
                                  _T("where nr = ? ")
                                  _T("order by zei"));

    sqlin ((long *) &angpt.nr,SQLLONG,0);
    sqlin ((long *) &angpt.zei,SQLLONG,0);
    sqlin ((TCHAR *) angpt.txt,SQLCHAR,61);
            sqltext = _T("update angpt set angpt.nr = ?,  ")
_T("angpt.zei = ?,  angpt.txt = ? ")

#line 17 "angpt.rpp"
                      _T("where nr = ? ")
                      _T("and zei  = ?");

            sqlin ((long *) &angpt.nr, SQLLONG, 0);
            sqlin ((long *) &angpt.zei, SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *) &angpt.nr, SQLLONG, 0);
            sqlin ((long *) &angpt.zei, SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select nr from angpt ")
                                  _T("where nr = ? ")
                                  _T("and   zei  = ?"));

            sqlin ((long *) &angpt.nr, SQLLONG, 0);
            sqlin ((long *) &angpt.zei, SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from angpt ")
                                  _T("where nr = ? ")
                                  _T("and   zei  = ?"));

            sqlin ((long *) &angpt.nr, SQLLONG, 0);
            del_cursor_posi = sqlcursor (_T("delete from angpt ")
                                  _T("where  nr = ?"));

    sqlin ((long *) &angpt.nr,SQLLONG,0);
    sqlin ((long *) &angpt.zei,SQLLONG,0);
    sqlin ((TCHAR *) angpt.txt,SQLCHAR,61);
            ins_cursor = sqlcursor (_T("insert into angpt (")
_T("nr,  zei,  txt) ")

#line 41 "angpt.rpp"
                                      _T("values ")
                                      _T("(?,?,?)")); 

#line 43 "angpt.rpp"
            sqlout ((long *) &angpt.nr, SQLLONG, 0);
            max_cursor = sqlcursor (_T("select max (nr) from angpt"));   
}
int ANGPT_CLASS::dbreadfirst (void)
/**

Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         return (this->DB_CLASS::dbreadfirst ());
}

int ANGPT_CLASS::delete_angpposi (void)
/**
Alle text fuer eine Position in aufp loeschen
**/
{
         if (del_cursor_posi == -1)
         {  
                this->prepare ();
         }
         int dsqlstatus = sqlexecute (del_cursor_posi);
         return (dsqlstatus);
}

void ANGPT_CLASS::dbclose (void)
{
         if (del_cursor_posi != -1)
	   {
		     sqlclose (del_cursor_posi);
		     del_cursor_posi = -1;
	   }
         if (max_cursor != -1)
	   {
		     sqlclose (max_cursor);
		     max_cursor = -1;
	   }
	   DB_CLASS::dbclose ();
}

long ANGPT_CLASS::GetMaxNr ()
{
	if (max_cursor == -1)
	{
		prepare ();
	}
		        
	angpt.nr = 0;
    sqlopen (max_cursor);
    sqlfetch (max_cursor);
	if (angpt.nr < 0) angpt.nr = 0;
    return angpt.nr;
}
