#pragma once
#include "searchlistctrl.h"
#include "ColorButton.h"

class CSearchListCtrlColor :
	public CSearchListCtrl
{
protected :
	DECLARE_DYNAMIC(CSearchListCtrlColor)
public:
	CColorButton Button;
	CSearchListCtrlColor(void);
public:
	~CSearchListCtrlColor(void);
	void Create (RECT &rect, CWnd *pParent, UINT nIDEdit, UINT nIDButton, int Align);
	void DestroyWindow ();

};
