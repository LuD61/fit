#ifndef _LS_TXT_DEF
#define _LS_TXT_DEF
#include "dbclass.h"

struct LS_TXT {
   long           nr;
   long           zei;
   TCHAR          txt[61];
};
extern struct LS_TXT ls_txt, ls_txt_null;

#line 6 "ls_txt.rh"

class LS_TXT_CLASS : public DB_CLASS 
{
       private :
               int del_cursor_posi;
               int max_cursor; 
               void prepare (void);
       public :
	       LS_TXT ls_txt;	
               LS_TXT_CLASS () : DB_CLASS ()
               {
                        del_cursor_posi = -1;
                        max_cursor = -1;
               }

               ~LS_TXT_CLASS ()
               {
	                    this->dbclose ();
               }
               int dbreadfirst (void);
               int delete_lstxtposi (void); 
               long GetMaxNr ();
		       void dbclose ();
};
#endif
