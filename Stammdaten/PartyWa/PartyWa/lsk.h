#ifndef _LSK_DEF
#define _LSK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct LSK {
   long           ls;
   long           adr;
   short          mdn;
   long           auf;
   short          kun_fil;
   long           kun;
   short          fil;
   TCHAR          feld_bz1[20];
   DATE_STRUCT    lieferdat;
   TCHAR          lieferzeit[6];
   TCHAR          hinweis[49];
   short          ls_stat;
   TCHAR          kun_krz1[17];
   double         auf_sum;
   TCHAR          feld_bz2[12];
   double         lim_er;
   TCHAR          partner[37];
   long           pr_lst;
   TCHAR          feld_bz3[8];
   short          pr_stu;
   long           vertr;
   long           tou;
   TCHAR          adr_nam1[37];
   TCHAR          adr_nam2[37];
   TCHAR          pf[17];
   TCHAR          str[37];
   TCHAR          plz[9];
   TCHAR          ort1[37];
   double         of_po;
   short          delstatus;
   long           rech;
   TCHAR          blg_typ[2];
   double         zeit_dec;
   long           kopf_txt;
   long           fuss_txt;
   long           inka_nr;
   TCHAR          auf_ext[17];
   long           teil_smt;
   TCHAR          pers_nam[9];
   double         brutto;
   DATE_STRUCT    komm_dat;
   double         of_ek;
   double         of_po_euro;
   double         of_po_fremd;
   double         of_ek_euro;
   double         of_ek_fremd;
   short          waehrung;
   TCHAR          ueb_kz[2];
   double         gew;
   short          gruppe;
   short          ccmarkt;
   short          auf_art;
   short          fak_typ;
   long           tou_nr;
   short          wieg_kompl;
   DATE_STRUCT    best_dat;
   TCHAR          hinweis2[31];
   TCHAR          hinweis3[31];
   DATE_STRUCT    fix_dat;
   TCHAR          komm_name[13];
   short          psteuer_kz;
   short          rech_typ;
};
extern struct LSK lsk, lsk_null;

#line 8 "lsk.rh"

class LSK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LSK lsk;  
               LSK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
