#pragma once
#include "afxwin.h"
#include "resource.h"

class CGebindeDialog :
	public CDialog
{
	DECLARE_DYNAMIC(CGebindeDialog)
public:
	CGebindeDialog(CWnd* pParent = NULL);
public:
	~CGebindeDialog(void);
// Dialogfelddaten
	enum { IDD = IDD_GEBINDE_DLG };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	BOOL PreTranslateMessage(MSG* pMsg);

private:
	CString Label;
	CString Text;
	int length;
	CRect rect;
    CStatic m_LGebinde;
	CEdit m_Gebinde;
	int BreakKey;

public:
	enum
	{
		For,
		Back,
		Up,
		Down
	};

	CString& GetText ()
	{
		return Text;
	}

	void SetText (CString& Text)
	{
		this->Text = Text;
	}

	void SetText (LPTSTR Text)
	{
		this->Text = Text;
	}

	void SetLabel (CString& Label)
	{
		this->Label = Label;
	}

	void SetLabel (LPTSTR Label)
	{
		this->Label = Label;
	}

	void SetLength (int length)
	{
		this->length = length;
	}

	void SetRect (CRect&rect)
	{
		this->rect = rect;
	}

	int GetBreakKey ()
	{
		return BreakKey;
	}

	afx_msg void OnOK ();
};
