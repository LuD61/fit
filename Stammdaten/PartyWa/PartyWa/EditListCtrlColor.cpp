#include "StdAfx.h"
#include "editlistctrlcolor.h"
#include "token.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_STARTENTER WM_USER + 10

/*
CColType::CColType (int Pos, int Type)
{
	this->Pos  = Pos;
	this->Type = Type;
}
*/

int CEditListCtrlColor::jrhstart = 70;
int CEditListCtrlColor::jrh1 = 1900;
int CEditListCtrlColor::jrh2 = 2000;
int CEditListCtrlColor::sjr = 1954;

BEGIN_MESSAGE_MAP(CEditListCtrlColor, CListCtrl)
	ON_WM_PAINT ()
	ON_WM_VSCROLL ()
	ON_WM_HSCROLL ()
	ON_WM_SETFOCUS ()
	ON_WM_KILLFOCUS ()
	ON_WM_LBUTTONDOWN ()
	ON_WM_MOUSEMOVE ()
	ON_WM_MOUSEWHEEL ()
//	ON_NOTIFY_REFLECT (HDN_BEGINTRACK, OnListBeginTrack)
	ON_NOTIFY (HDN_BEGINTRACK, 0, OnListBeginTrack)
END_MESSAGE_MAP()

CEditListCtrlColor::CEditListCtrlColor(void)
{
	EditRow = EditCol = 0;
	FillList = this;
	GridLines = TRUE;
	HLines = FALSE;
	VLines = FALSE;
	SetEditFocus = FALSE;
	dropTarget.Register (this);
	LimitText = -1;
	MustStart = TRUE;
	EditNumber = -1;
	FirstScroll = TRUE;
	ColType.Init ();
	Edit = LEDIT;
	Combo        = COMBO;
	SearchEdit   = SEARCHEDIT;
	SearchButton = SEARCHBUTTON;
	CheckBox     = LCHECKBOX;
	check.LoadFromResource (AfxGetInstanceHandle (), IDB_CHECK);
	uncheck.LoadFromResource (AfxGetInstanceHandle (), IDB_UNCHECK);
	EnableEdit = TRUE;
	IsListDialog = FALSE;
}

CEditListCtrlColor::~CEditListCtrlColor(void)
{
	ColType.DestroyAll ();
}

BOOL CEditListCtrlColor::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
	case WM_STARTENTER :
		StartEnter (EditCol, EditRow);
		return FALSE;
	}
	return CListCtrl::PreTranslateMessage(pMsg);
}

int CEditListCtrlColor::GetColType (int Pos)
{
	CColType *c;
	ColType.FirstPosition ();
	while ((c = (CColType *) ColType.GetNext ()) != 0)
	{
		if (Pos == c->Pos) return c->Type; 
	}
	return LEDIT;
}

void CEditListCtrlColor::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
}


void CEditListCtrlColor::OnSetFocus (CWnd *oldcWnd)
{
	if (!EnableEdit) 
	{
		CListCtrl::OnSetFocus (oldcWnd);
		return;
	}
	if (oldcWnd == &ListEdit)
	{
		return;
	}
	else if (oldcWnd == &ListComboBox)
	{
		return;
	}
	else if (oldcWnd == &ListCheckBox)
	{
		return;
	}
	else if (oldcWnd == &SearchListCtrl.Edit)
	{
		return;
	}
	else if (oldcWnd == &SearchListCtrl.Button)
	{
		return;
	}
	else if (oldcWnd == &ListDate)
	{
		return;
	}
	FirstEnter ();
}

void CEditListCtrlColor::FirstEnter ()
{
	StartEnter (1, 0);
}

void CEditListCtrlColor::OnKillFocus (CWnd *newcWnd)
{

	if (!EnableEdit) 
	{
		CListCtrl::OnKillFocus (newcWnd);
		return;
	}
	if (newcWnd == &ListEdit)
	{
		return;
	}
	else if (newcWnd == &ListComboBox)
	{
		return;
	}
	else if (newcWnd == &ListCheckBox)
	{
		return;
	}
	else if (newcWnd == &SearchListCtrl.Edit)
	{
		return;
	}
	else if (newcWnd == &ListDate)
	{
		return;
	}
	StopEnter ();
}


void CEditListCtrlColor::OnEnKillFocusEdit ()
{
}

void CEditListCtrlColor::StartEnterCombo (int col, int row, CVector *Values)
{
/*
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		SearchListCtrl.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		ListCheckBox.DestroyWindow ();
	}
	else if (IsWindow (ListEdit.m_hWnd))
	{
		ListEdit.DestroyWindow ();
	}
*/
	if (!EnableEdit) return;
	StopEnter ();
	EditRow = row;
	EditCol = col;
    CRect rect;
	GetSubItemRect (row, col, LVIR_BOUNDS , rect);
    HDITEM hdItem;
	rect.top -= 2;
	CRect textRect;
	textRect = rect;
	CDC *cDc = GetDC ();
	TEXTMETRIC tm;
	cDc->GetTextMetrics (&tm);
	ReleaseDC (cDc);
	int border = GetSystemMetrics (SM_CYBORDER);
	int textHeight = tm.tmHeight + 4 * border;
	int cellHeight = rect.bottom - rect.top;
	textRect.top = rect.top + (cellHeight - textHeight) / 2;
	if (textRect.top > rect.top)
	{
		textRect.bottom = textRect.top + textHeight;
		rect = textRect;
	}
	rect.bottom = rect.top + 140;
	ZeroMemory (&hdItem, sizeof (hdItem));
	hdItem.mask = HDI_FORMAT;
	GetHeaderCtrl ()->GetItem (col, &hdItem);
    if (hdItem.fmt & HDF_RIGHT)
	{
		ListComboBox.Create (WS_VISIBLE | CBS_DROPDOWNLIST | 
			                 WS_CHILD | WS_VSCROLL, 
			                 rect, this, COMBO);
	}
	else
	{
		ListComboBox.Create (WS_VISIBLE | CBS_DROPDOWNLIST |
			                 WS_CHILD | WS_VSCROLL, 
			                 rect, this, COMBO);
	}
	ListComboBox.Clear ();
	CString *value;
	Values->FirstPosition ();
	while ((value = (CString *) Values->GetNext ()) != NULL)
	{
		ListComboBox.AddString (value->GetBuffer (0));
	}
	ListComboBox.SetFont (GetFont ());

    CString Text = GetItemText (row, col);
	int idx = ListComboBox.FindString (-1, Text.TrimRight ().GetBuffer (0));
	if (idx < 0) idx = 0;
	ListComboBox.SetCurSel (idx);

//	ListEdit.SetWindowText (Text);

	SetEditFocus = TRUE;
	ListComboBox.SetFocus ();
//	SetSel (Text);
	SetEditFocus = FALSE;
}

void CEditListCtrlColor::StartEnterCheckBox (int col, int row)
{
/*
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		SearchListCtrl.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		ListCheckBox.DestroyWindow ();
	}
	else if (IsWindow (ListEdit.m_hWnd))
	{
		ListEdit.DestroyWindow ();
	}
*/

	if (!EnableEdit) return;
	StopEnter ();
	EditRow = row;
	EditCol = col;
    CRect Rect;
	int cyb = GetSystemMetrics (SM_CYBORDER);
	int cxb = GetSystemMetrics (SM_CXBORDER);
	GetSubItemRect (row, col, LVIR_BOUNDS , Rect);
	int cx = check.GetWidth () + 3;
	int cy = check.GetHeight () + 3;
	int x = (Rect.right - Rect.left - cx) / 2;
	x = (x > 0) ? x : 0;
	x += Rect.left;
	int y = (Rect.bottom - Rect.top - cy) / 2;
	y = (y > 0) ? y : 0;
	y += Rect.top;

	CRect rect (x, y, x + cx, y + cy);
	ListCheckBox.Create (WS_VISIBLE | WS_CHILD, 
			                 rect, this, LCHECKBOX);
    CString Text = GetItemText (row, col);
	if (Text == "X")
	{
		ListCheckBox.SetCheck (BST_CHECKED);
	}
	else
	{
		ListCheckBox.SetCheck (BST_UNCHECKED);
	}
	ListCheckBox.SetFont (GetFont ());

	SetEditFocus = TRUE;
	ListCheckBox.SetFocus ();
	SetEditFocus = FALSE;
}

void CEditListCtrlColor::StartSearchListCtrl (int col, int row)
{
/*
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		SearchListCtrl.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (ListEdit.m_hWnd))
	{
		ListEdit.DestroyWindow ();
	}
*/
	if (!EnableEdit) return;
	StopEnter ();
	EditRow = row;
	EditCol = col;
    CRect rect;
	GetSubItemRect (row, col, LVIR_BOUNDS , rect);
    HDITEM hdItem;
	rect.top -= 2;
	rect.bottom += 2;
	CRect textRect;
	textRect = rect;
	CDC *cDc = GetDC ();
	TEXTMETRIC tm;
	cDc->GetTextMetrics (&tm);
	ReleaseDC (cDc);
	int border = GetSystemMetrics (SM_CYBORDER);
	int textHeight = tm.tmHeight + 4 * border;
	int cellHeight = rect.bottom - rect.top;
	textRect.top = rect.top + (cellHeight - textHeight) / 2;
	if (textRect.top > rect.top)
	{
		textRect.bottom = textRect.top + textHeight;
		rect = textRect;
	}
	ZeroMemory (&hdItem, sizeof (hdItem));
	hdItem.mask = HDI_FORMAT;
	GetHeaderCtrl ()->GetItem (col, &hdItem);
    if (hdItem.fmt & HDF_RIGHT)
	{
		SearchListCtrl.Create (rect, this, SEARCHEDIT, SEARCHBUTTON, 
			SearchListCtrl.RIGHT);
	}
	else
	{
		SearchListCtrl.Create (rect, this, SEARCHEDIT, SEARCHBUTTON, SearchListCtrl.LEFT);
	}
	SearchListCtrl.Edit.SetFont (GetFont ());
    CString Text = GetItemText (row, col);
	if (EditNumber != -1)
	{
		CToken t;
		t.SetSep (" ");
		t = Text;
		if (EditNumber < t.GetAnzToken ())
		{
			Text = t.GetToken (EditNumber);
			Text.Trim ();
		}
	}
	SearchListCtrl.Edit.SetReadOnly (FALSE);
	SearchListCtrl.Edit.SetWindowText (Text);
	SetEditFocus = TRUE;
	SearchListCtrl.Edit.SetFocus ();
	SetSearchSel (Text);
	SetEditFocus = FALSE;
}

void CEditListCtrlColor::StartEnterDate (int col, int row)
{

	if (!EnableEdit) return;
	StopEnter ();
	EditRow = row;
	EditCol = col;
    CRect rect;
	GetSubItemRect (row, col, LVIR_BOUNDS , rect);
//    HDITEM hdItem;
	rect.top -= 2;
	rect.bottom += 2;
	CRect textRect;
	textRect = rect;
	CDC *cDc = GetDC ();
	TEXTMETRIC tm;
	cDc->GetTextMetrics (&tm);
	ReleaseDC (cDc);
	int border = GetSystemMetrics (SM_CYBORDER);
	int textHeight = tm.tmHeight + 4 * border;
	int cellHeight = rect.bottom - rect.top;
	textRect.top = rect.top + (cellHeight - textHeight) / 2;
	if (textRect.top > rect.top)
	{
		textRect.bottom = textRect.top + textHeight;
		rect = textRect;
	}
/*
	ZeroMemory (&hdItem, sizeof (hdItem));
	hdItem.mask = HDI_FORMAT;
	GetHeaderCtrl ()->GetItem (col, &hdItem);
    if (hdItem.fmt & HDF_RIGHT)
	{
		ListDate.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_RIGHT, 
			             rect, this, EDIT);
	}
	else
	{
		ListEdit.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_AUTOVSCROLL, 
			              rect, this, EDIT);
	}
*/
	DWORD style = WS_VISIBLE | WS_CHILD | WS_BORDER | DTS_SHORTDATEFORMAT; 
	ListDate.Create (style, rect, this, DATE);
	ListDate.SetFont (GetFont ());
    CString Text = GetItemText (row, col);
	Text.Trim ();
	int day = _tstoi (Text.Left (2).GetBuffer ());
	int month = _tstoi (Text.Mid (3, 2).GetBuffer ());
	int year = _tstoi (Text.Right (4).GetBuffer ());
	CTime Date (year, month, day, 0, 0, 0);
	ListDate.SetTime (&Date);
	SetEditFocus = TRUE;
	ListDate.SetFocus ();
//	SetSel (Text);
	SetEditFocus = FALSE;
}


void CEditListCtrlColor::StartEnter (int col, int row)
{
/*
	if (IsWindow (ListComboBox.m_hWnd))
	{
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (ListEdit.m_hWnd))
	{
		ListEdit.DestroyWindow ();
	}
*/
	if (!EnableEdit) return;
	StopEnter ();
	EditRow = row;
	EditCol = col;
    CRect rect;
	GetSubItemRect (row, col, LVIR_BOUNDS , rect);
    HDITEM hdItem;
	rect.top -= 2;
	rect.bottom += 2;
	CRect textRect;
	textRect = rect;
	CDC *cDc = GetDC ();
	TEXTMETRIC tm;
	cDc->GetTextMetrics (&tm);
	ReleaseDC (cDc);
	int border = GetSystemMetrics (SM_CYBORDER);
	int textHeight = tm.tmHeight + 4 * border;
	int cellHeight = rect.bottom - rect.top;
	textRect.top = rect.top + (cellHeight - textHeight) / 2;
	if (textRect.top > rect.top)
	{
		textRect.bottom = textRect.top + textHeight;
		rect = textRect;
	}
	ZeroMemory (&hdItem, sizeof (hdItem));
	hdItem.mask = HDI_FORMAT;
	GetHeaderCtrl ()->GetItem (col, &hdItem);
    if (hdItem.fmt & HDF_RIGHT)
	{
		ListEdit.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_RIGHT, 
			             rect, this, LEDIT);
	}
	else
	{
		ListEdit.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_AUTOHSCROLL, 
			              rect, this, LEDIT);
	}
	ListEdit.SetFont (GetFont ());
    CString Text = GetItemText (row, col);
	ListEdit.SetReadOnly (FALSE);
	if (LimitText != -1)
	{
		Text = Text.Left (LimitText);
		ListEdit.SetWindowText (Text.GetBuffer ());
		ListEdit.SetLimitText (LimitText);
		if (LimitText == 0)
		{
			ListEdit.SetReadOnly ();
		}
	}
	else
	{
		ListEdit.SetWindowText (Text);
	}
	SetEditFocus = TRUE;
	ListEdit.SetFocus ();
	SetSel (Text);
	SetEditFocus = FALSE;
}

void CEditListCtrlColor::SetSel (CString& Text)
{
   Text.TrimRight ();
   int cpos = Text.GetLength ();
   if (LimitText != -1)
   {
	ListEdit.SetSel (cpos, LimitText);
	ListEdit.ReplaceSel (_T(""));
   }
   ListEdit.SetSel (cpos, cpos);
}

void CEditListCtrlColor::SetSearchSel (CString& Text)
{
   try
   {
   HDITEM hdItem;
   ZeroMemory (&hdItem, sizeof (hdItem));
   GetHeaderCtrl ()->GetItem (EditCol, &hdItem);
   if ((hdItem.fmt & HDF_RIGHT) || EditNumber != -1)
   {
		SearchListCtrl.Edit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		SearchListCtrl.Edit.SetSel (cpos, cpos);
   }
   }
   catch (...) {}
}

void CEditListCtrlColor::GetEnter ()
{
	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		ListComboBox.GetLBText (idx, Text);

		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
	}
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		int check = ListCheckBox.GetCheck ();
        if (check == BST_CHECKED)
		{
			FillList.SetItemText (_T("X"), EditRow, EditCol);
		}
		else
		{
			FillList.SetItemText (_T(" "), EditRow, EditCol);
		}
	}
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		EditNumber = -1;
	}
	else if (IsWindow (ListDate.m_hWnd))
	{
		CString Text;
		ListDate.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
	}
}

void CEditListCtrlColor::StopEnter ()
{
	if (!EnableEdit) return;
	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		ListComboBox.GetLBText (idx, Text);

		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListComboBox.DestroyWindow ();
	}
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		int check = ListCheckBox.GetCheck ();
        if (check == BST_CHECKED)
		{
			FillList.SetItemText (_T("X"), EditRow, EditCol);
		}
		else
		{
			FillList.SetItemText (_T(" "), EditRow, EditCol);
		}
		ListCheckBox.DestroyWindow ();
	}
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
		EditNumber = -1;
	}
	else if (IsWindow (ListDate.m_hWnd))
	{
		CString Text;
		ListDate.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListDate.DestroyWindow ();
	}
}

void CEditListCtrlColor::SetEditText ()
{
	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
	}
}

void CEditListCtrlColor::FormatText (CString& Text)
{
}

void CEditListCtrlColor::NextRow ()
{
	int count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditRow ++;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CEditListCtrlColor::PriorRow ()
{
	int count = GetItemCount ();
	if (EditRow <= 0)
	{
		return;
	}
	StopEnter ();
	EditRow --;
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CEditListCtrlColor::OnReturn ()
{
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1)
	{
		if (EditRow >= rowCount - 1)
		{
			return;
		}
		EditCol = 2;
		EditRow ++;
	}
	else
	{
		StopEnter ();
		EditCol ++;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
}

void CEditListCtrlColor::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol >= count - 1)
	{
		return;
	}
	StopEnter ();
	EditCol ++;
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CEditListCtrlColor::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 0)
	{
		return;
	}
	StopEnter ();
	EditCol --;
    StartEnter (EditCol, EditRow);
}

void CEditListCtrlColor::OnKeyDown(UINT nTCHAR, UINT nRepCnt, UINT nFlags)
{
	int diff = 0;
}

BOOL CEditListCtrlColor::OnKeyD (WPARAM vKey)
{
	int diff = 0;
    if (GetItemCount () == 0) return FALSE;
	if (!IsWindow (ListEdit.m_hWnd) &&
		!IsWindow (ListComboBox.m_hWnd)&&
		!IsWindow (ListCheckBox.m_hWnd)&&
		!IsWindow (SearchListCtrl.Edit.m_hWnd) &&
		!IsWindow (ListDate.m_hWnd)) 
	{
		StartEnter (1, 0);
		return TRUE;
	}
	switch (vKey)
	{
	case VK_DOWN :
		NextRow ();
		return TRUE;
	case VK_UP :
		PriorRow ();
		return TRUE;
	case VK_RETURN :
		OnReturn ();
		return TRUE;
	case VK_TAB :
		if (GetKeyState (VK_SHIFT) < 0)
		{
			PriorCol ();
		}
		else
		{
			NextCol ();
		}
		return TRUE;
	case VK_F6 :
		InsertRow ();
		return TRUE;
	case VK_F7 :
		DeleteRow ();
		return TRUE;
	case VK_NEXT :
		diff = EditRow - GetTopIndex ();
		StopEnter ();
		SendMessage (WM_KEYDOWN, vKey, 0l);
		EditRow = GetTopIndex () + diff;
		EditRow = (EditRow < GetItemCount ()) ? EditRow : 
		           GetItemCount () - 1;
        StartEnter (EditCol, EditRow);
		return TRUE;
	case VK_PRIOR :
		diff = EditRow - GetTopIndex ();
		StopEnter ();
		SendMessage (WM_KEYDOWN, vKey, 0l);
		EditRow = GetTopIndex () + diff;
		EditRow = (EditRow >= 0) ? EditRow : 
		           0;
        StartEnter (EditCol, EditRow);
		return TRUE;
	}
	return FALSE;
}

void CEditListCtrlColor::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int diff = 0;

    if (GetItemCount ()  == 0) return;
	switch (nSBCode)
	{
	case SB_LINEDOWN :
	case SB_PAGEDOWN :
		diff = EditRow - GetTopIndex ();
		StopEnter ();
		CListCtrl::OnVScroll (nSBCode, nPos, pScrollBar);
		InvalidateRect (NULL, TRUE);
		EditRow = GetTopIndex () + diff;
		EditRow = (EditRow < GetItemCount ()) ? EditRow : 
		           GetItemCount () - 1;
        StartEnter (EditCol, EditRow);
		return;
	case SB_LINEUP :
	case SB_PAGEUP :
		diff = EditRow - GetTopIndex ();
		StopEnter ();
		CListCtrl::OnVScroll (nSBCode, nPos, pScrollBar);
		InvalidateRect (NULL, TRUE);
		EditRow = GetTopIndex () + diff;
		EditRow = (EditRow >= 0) ? EditRow : 
		           0;
        StartEnter (EditCol, EditRow);
		return;
	case SB_THUMBPOSITION :
	case SB_THUMBTRACK :
		diff = EditRow - GetTopIndex ();
		StopEnter ();
		CListCtrl::OnVScroll (nSBCode, nPos, pScrollBar);
		EditRow = GetTopIndex () + diff;
		EditRow = (EditRow < GetItemCount ()) ? EditRow : 
		           GetItemCount () - 1;
        StartEnter (EditCol, EditRow);
		return;
	}
}

void CEditListCtrlColor::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{

    if (GetItemCount ()  == 0) return;
	StopEnter ();
	CListCtrl::OnHScroll (nSBCode, nPos, pScrollBar);
    StartEnter (EditCol, EditRow);
}

BOOL CEditListCtrlColor::OnLBuDown (CPoint& p)
{
	CRect rect;
	GetWindowRect (&rect);
	if (p.x < rect.left || p.x > rect.right) return FALSE;
	if (p.y < rect.top  || p.y > rect.bottom) return FALSE;

	ScreenToClient (&p);
	if (p.x < 0 || p.y < 0) 
	{
		StopEnter ();
		return FALSE;
	}
    LVHITTESTINFO lvhti;	
    lvhti.pt = p;	
   
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (rowCount == 0) return FALSE;

	SubItemHitTest(&lvhti);
   
    if (lvhti.flags & LVHT_ONITEMLABEL)
	{
		StartEnter (lvhti.iSubItem, lvhti.iItem);
		return TRUE;
	}
	return FALSE;
}

void CEditListCtrlColor::OnLButtonDown (UINT flags,CPoint p)
{
	if (!EnableEdit) 
	{
		CListCtrl::OnLButtonDown (flags, p);
		return;
	}

	CRect rect;
	GetWindowRect (&rect);
	if (p.x < 0 || p.y < 0) 
	{
		return;
	}
    LVHITTESTINFO lvhti;	
    lvhti.pt = p;	

	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (rowCount == 0) return;

	SubItemHitTest(&lvhti);
	if (IsDisplayOnly (lvhti.iSubItem)) return;

    if ((flags & MK_SHIFT) != 0)
	{
		RunShiftItemClicked (lvhti.iItem);
		return;
	}
    if ((flags & MK_CONTROL) != 0)
	{
		RunCtrlItemClicked (lvhti.iItem);
		return;
	}

	if (lvhti.iSubItem == 0)
	{
		RunItemClicked (lvhti.iItem);
	}
   
    if (lvhti.flags & LVHT_ONITEMLABEL)
	{
		StartEnter (lvhti.iSubItem, lvhti.iItem);
		if (GetColType (lvhti.iSubItem) == LCHECKBOX)
		{
			if (IsWindow (ListCheckBox.m_hWnd))
			{
				CString Check = GetItemText (lvhti.iItem, lvhti.iSubItem);
				if (Check == _T("X")) 
				{
					Check = _T(" ");
					ListCheckBox.SetCheck (BST_UNCHECKED);
				}
				else
				{
					Check = _T("X");
					ListCheckBox.SetCheck (BST_CHECKED);
				}
				SetItemText (lvhti.iItem, lvhti.iSubItem, Check.GetBuffer ()); 
			}
		}
		ClearSelect ();
		return;
	}
}

BOOL CEditListCtrlColor::OnMouseWheel(UINT nFlags,  short zDelta,  CPoint pt)
{
	BOOL ret = CListCtrl::OnMouseWheel (nFlags, zDelta, pt);
	StartEnter (EditCol, EditRow);
	return ret;
}


void CEditListCtrlColor::OnMouseMove (UINT flags,CPoint p)
{
	CListCtrl::OnMouseMove (flags, p);
	if ((flags & MK_LBUTTON) == 0)
	{
		return;
	}
	CRect rect;
	GetWindowRect (&rect);
	if (p.x < 0 || p.y < 0) 
	{
		return;
	}
    LVHITTESTINFO lvhti;	
    lvhti.pt = p;	

	SubItemHitTest(&lvhti);

	HiLightItem (lvhti.iItem);
}

BOOL CEditListCtrlColor::DeleteRow ()
{
	if (!IsWindow (m_hWnd)) return FALSE;
	StopEnter ();
	DeleteItem (EditRow);
	if (GetItemCount () == 0)
	{
		AppendEmpty ();
		StartEnter (1, 0);
	}
	else if (EditRow == GetItemCount ())
	{
		PriorRow ();
	}
	else
	{
		StartEnter (EditCol, EditRow);
	}
	return TRUE;
}

BOOL CEditListCtrlColor::InsertRow ()
{
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	for (int i = 1; i < colCount; i ++)
	{
		FillList.SetItemText (_T(""), EditRow, i);
	}
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CEditListCtrlColor::AppendEmpty ()
{
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	FillList.InsertItem (rowCount, -1);
	for (int i = 1; i < colCount; i ++)
	{
		FillList.SetItemText (_T(""), rowCount, i);
	}
	return TRUE;
}

void CEditListCtrlColor::EnsureColVisible (int Col)
{
	int  cols = GetHeaderCtrl ()->GetItemCount ();
	if (Col >= cols) return;

    CRect rect;
	GetSubItemRect (EditRow, Col, LVIR_BOUNDS , rect);
	ClientToScreen (&rect);
    CRect wrect;
    GetWindowRect (&wrect);
	if (rect.left > wrect.left && rect.right < wrect.right)
	{
		return;
	}
	CRect r;
	if (rect.right >= wrect.right)
	{
		for (int i = 0; i < cols; i ++)
		{
			GetSubItemRect (EditRow, i, LVIR_BOUNDS , r);
			rect.right -= (r.right - r.left);
			if (rect.right < wrect.right) break;
		}
		Scroll (CSize (r.right, GetTopIndex ())); 
	}
	else if (rect.left <= wrect.left)
	{
		for (int i = 0; i < cols; i ++)
		{
			GetSubItemRect (EditRow, i, LVIR_BOUNDS , r);
			rect.left += (r.right - r.left);
			if (rect.left > wrect.left) break;
		}
		Scroll (CSize (r.left, GetTopIndex ())); 
	}
}
 

double CEditListCtrlColor::StrToDouble (LPTSTR string)
{
 double fl;
 double ziffer;
 double teiler;
 short minus;

 if (string == (LPTSTR) 0) return (double) 0.0;
 fl = 0;
 teiler = 10;
 minus = 1;
 while (*string < 0X30)
 {
  if (*string == 0)
    return (0.0);
  if (*string == '-')
    break;
  if (*string == '+')
    break;
  if (*string == '.')
    break;
  if (*string == ',')
    break;
  string ++;
 }

 if (*string == '-')
 {
  minus = -1;
  string ++;
 }
 else if (*string == '+')
 {
  string ++;
 }

 while (*string)
 {
  if (*string == '.')
  {
   break;
  }
  if (*string == ',')
  {
   break;
  }
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = (fl * teiler) + ziffer;
  string ++;
 }

 if (*string == '.')
   ;
 else if (*string == ',')
   ;
 else
 {
  fl *= minus;
  return (fl);
 }

 string ++;
 while (*string)
 {
  if (*string < 0X30)
  {
   break;
  }
  if (*string > 0X39)
  {
   break;
  }
  ziffer = *string - 0X30;
  fl = fl + (ziffer / teiler);
  teiler *= 10;
  string ++;
 }
 fl *= minus;
 return (fl);
}

void CEditListCtrlColor::DoubleToString (double dValue, CString& Value, int scale, CString Point)
{
	CString Format;
	Format.Format (_T("%c.%dlf"), '%', scale);
	Value.Format (Format.GetBuffer (), dValue);
	if (Point != ".")
	{
		int pos = Value.Find (".", 0);
		if (pos != -1)
		{
			Value.GetBuffer ()[pos] = Point.GetBuffer ()[0];
		}
	}
}


	

double CEditListCtrlColor::StrToDouble (CString& Text)
{
	return StrToDouble (Text.GetBuffer ());
}

void CEditListCtrlColor::DatFormat (CString &Date, const LPTSTR picture)
/**
Datumsfeld formatieren.
**/
{
      TCHAR tags [3];
      TCHAR mons [3];
      TCHAR jrs [3];
      short tag;
      short mon;
      short jr;
      int point;

	  Date.Trim ();
	  if (Date == "")
	  {
		         return;
	  }
	  if (Date.GetLength () < 6)
	  {
		         Date = "";
		         return;
	  }
	  if ((point = Date.Find (_T("."))) >= 0)
      {
                 tag = _tstoi (Date.Left (2));
                 point ++;
                 mon = _tstoi (Date.Mid (point, 2));
				 point = Date.Find (_T("."), point);
                 if (point < 0)
                 {
                              jr = jrh1;
                 }
                 else
                 {
                               point ++;
                               jr = _tstoi (Date.Mid (point));
                 }
      }
      else
      {
		         _tcscpy (tags, Date.Left (2).GetBuffer (0));
		         _tcscpy (mons, Date.Mid (2, 2).GetBuffer (0));
		         _tcscpy (jrs, Date.Mid (4).GetBuffer (0));
                 tag = _tstoi (tags);
                 mon = _tstoi (mons);
                 jr = _tstoi (jrs);
      }

      if (jr < 100)
      {
             if (jr < jrhstart)
             {
                  jr += jrh2;
             }
             else
             {
                  jr += jrh1;
             }
      }


      if (mon > 12 || mon < 1) 
      {
                 Date = "";
                 return;
      }

      if (tag < 1)
      {
		         Date = "";
                 return;
      }
                 
      if (IsMon31 (mon) && tag > 31) 
      {
		         Date = "";
                 return;
      }
      if (IsMon30 (mon) && tag > 30) 
      {
		         Date = "";
                 return;
      }

      if (IsMon29 (mon, jr) && tag > 29) 
      {
		         Date = "";
                 return;
      }

      if (IsMon28 (mon, jr) && tag > 28) 
      {
		         Date = "";
                 return;
      }

      if (jr < 100)
      {
             if (jr < jrhstart)
             {
                  jr += jrh2;
             }
             else
             {
                  jr += jrh1;
             }
      }

      if (memcmp (picture, "dd.mm.yyyy", 10) == 0)
      {
                Date.Format (_T("%02hd.%02hd.%04hd"),
                                  tag,mon,jr);
      }
      else if (memcmp (picture, _T("dd.mm.yy"), 8) == 0)
      {
                 Date.Format (_T("%02hd.%02hd.%02hd"),
                                tag,mon,jr % 100);
      }
                   
      else if (memcmp (picture, _T("ddmmyyyy"), 8) == 0)
      {
                 Date.Format (_T("%02hd%02hd%04hd"),
                                  tag,mon,jr);
      }
      else if (memcmp (picture, _T("ddmmyy"), 6) == 0)
      {
                 Date.Format (_T("%02hd%02hd%02hd"),
                                  tag,mon,jr % 100);
      }
      else
      {
		         Date = "";
      }
}

BOOL CEditListCtrlColor::IsMon31 (int mon)
/**
Monat im Datum pruefen.
**/
{
         static int mon31[] = {1,3,5,7,8,10,12};
         int i;

         for (i = 0; i < 7; i ++)
         {
                      if (mon == mon31[i]) return TRUE;
         }
         return FALSE;
}

BOOL CEditListCtrlColor::IsMon30 (int mon)
/**
Monat im Datum pruefen.
**/
{
         static int mon30[] = {4,6,9,11};
         int i;

         for (i = 0; i < 4; i ++)
         {
                      if (mon == mon30[i]) return TRUE;
         }
         return FALSE;
}

BOOL CEditListCtrlColor::IsMon29 (int mon, int jr)
/**
Monat im Datum pruefen.
**/
{
         static int sjr = 1954;
         int jrdiff; 

         if (mon != 2) return FALSE; 

         jrdiff = jr - sjr;
         if (jr % 4 == 0) return TRUE; 
         return FALSE;
}
 

BOOL CEditListCtrlColor::IsMon28 (int mon, int jr)
/**
Monat im Datum pruefen.
**/
{
         static int sjr = 1954;
         int jrdiff; 

         if (mon != 2) return FALSE; 

         jrdiff = jr - sjr;
         if (jr % 4) return TRUE; 
         return FALSE;
}
 
void CEditListCtrlColor::RunItemClicked (int Item)
{
}

void CEditListCtrlColor::RunCtrlItemClicked (int Item)
{
}
void CEditListCtrlColor::RunShiftItemClicked (int Item)
{
}
void CEditListCtrlColor::HiLightItem (int Item)
{
}

void CEditListCtrlColor::ClearSelect ()
{
}

void CEditListCtrlColor::OnListBeginTrack (NMHDR* pNMHDR, LRESULT* pResult)
{
	int eins = 1;
	*pResult = 0;
}

void CEditListCtrlColor::StartPauseEnter ()
{
	MustStart = FALSE;
	if (!IsWindow (ListEdit.m_hWnd) &&
		!IsWindow (ListComboBox.m_hWnd)&&
		!IsWindow (ListCheckBox.m_hWnd)&&
		!IsWindow (SearchListCtrl.Edit.m_hWnd) &&
		!IsWindow (ListDate.m_hWnd)) 
	{
		MustStart = TRUE;
	}
	StopEnter ();
}

void CEditListCtrlColor::EndPauseEnter ()
{
	if (MustStart)
	{
		StartEnter (EditCol, EditRow);
	}
}

void CEditListCtrlColor::OnPaint ()
{
	CListCtrl::OnPaint ();

	CRect listRect;
	CRect colRect;
	CRect hRect;
	int CellHeight;

    GetSubItemRect (0, 0, LVIR_BOUNDS, colRect);
	CellHeight = colRect .bottom - colRect.top;
	GetClientRect (&listRect);
	CHeaderCtrl *Header = GetHeaderCtrl (); 
	if (Header == NULL) return;
    Header->GetClientRect (&hRect);
	int Items = Header->GetItemCount ();

	CPen Pen;
//	Pen.CreatePen (PS_SOLID, 1, RGB (0, 0, 0));
	Pen.CreatePen (PS_SOLID, 1, RGB (120, 120, 120));
//	PAINTSTRUCT ps;
//	CDC *cDC = BeginPaint (&ps);
	CDC *cDC = GetDC ();
	if (!EnableEdit) 
	{
		GrPaint (cDC);
		ReleaseDC (cDC);
		return;
	}
	CPen *oldPen = cDC->SelectObject (&Pen);

	int ItemCount = GetItemCount ();
	if (ItemCount == 0) return;

	GrPaint (cDC);
	if (VLines)
	{
		for (int i = 0; i < Items; i ++)
		{
			GetSubItemRect (0, i, LVIR_BOUNDS, colRect);
			cDC->MoveTo (colRect.left, hRect.bottom + 2);
			cDC->LineTo (colRect.left, listRect.bottom);
		}
		cDC->MoveTo (colRect.right, hRect.bottom + 2);
		cDC->LineTo (colRect.right, listRect.bottom);
	}

    if (HLines == 1 && CellHeight >= 16)
	{
		GetSubItemRect (0, 1, LVIR_LABEL, colRect);
//		int ydiff = colRect.bottom - colRect.top;
		int ydiff = CellHeight;
		int y = ydiff + hRect.bottom;
		for (int i = 0; i < ItemCount; y += ydiff, i ++)
		{
			cDC->MoveTo (0, y);
			cDC->LineTo (listRect.right, y);
		}

		for (; y < listRect.bottom; y += ydiff)
		{
			cDC->MoveTo (0, y);
			cDC->LineTo (listRect.right, y);
		}
	}

    if (HLines == 2)
	{
		GetSubItemRect (0, 1, LVIR_LABEL, colRect);
		int ydiff = colRect.bottom - colRect.top;
		int ItemCount = GetItemCount ();
		int y = hRect.bottom;
		for (int i = 0; i < ItemCount; y += ydiff, i ++)
		{
			if (i == 0) 
			{
				continue;
			}
		    CString KunNr = GetItemText (i, 1);
			if (KunNr.Trim () == "") continue;
			cDC->MoveTo (0, y);
			cDC->LineTo (listRect.right, y);
		}
		cDC->MoveTo (0, y);
		cDC->LineTo (listRect.right, y);
	}
    cDC->SelectObject (oldPen);
//	EndPaint (&ps);
//	GrPaint (cDC);
	ReleaseDC (cDC);
}

void CEditListCtrlColor::GrPaint (CDC *cDC)
{
	CRect Rect;
	int ColCount = GetHeaderCtrl ()->GetItemCount ();
	int ItemCount = GetItemCount ();
	for (int pos = 0; pos < ColCount; pos ++)
	{
	 if (GetColType (pos) == LCHECKBOX)
	 {
		for (int i = 0; i < ItemCount; i ++)
		{
//			if (i == EditRow && pos == EditCol) continue;
			GetSubItemRect (i, pos, LVIR_BOUNDS, Rect);

			Rect.left += 2;
			if ((Rect.right - Rect.left) <= check.GetWidth ())
			{
				Rect.right = Rect.left + check.GetWidth ();
				Rect.top += 2;
				Rect.bottom -= 2;
				cDC->FillSolidRect (&Rect, cDC->GetBkColor ());
				continue;
			}

			CString IsChecked = GetItemText (i, pos);

			int cyb = GetSystemMetrics (SM_CYBORDER);
			int cxb = GetSystemMetrics (SM_CXBORDER);
			if (IsChecked == _T("X"))
			{
				int cx = check.GetWidth () + 2*cxb;
				int cy = check.GetHeight () + 2*cyb;
				int x = (Rect.right - Rect.left - cx) / 2;
				x = (x > 0) ? x : 0;
				x += Rect.left;
				int y = (Rect.bottom - Rect.top - cy) / 2;
				y = (y > 0) ? y : 0;
				y += Rect.top;
				check.Draw (*cDC, x, y, cx, cy);
			}
			else
			{
				int cx = uncheck.GetWidth () + 2*cxb;
				int cy = uncheck.GetHeight () + 2*cyb;
				int x = (Rect.right - Rect.left - cx) / 2;
				x = (x > 0) ? x : 0;
				x += Rect.left;
				int y = (Rect.bottom - Rect.top - cy) / 2;
				y = (y > 0) ? y : 0;
				y += Rect.top;
				uncheck.Draw (*cDC, x, y, cx, cy);
			}
		}
	 }
	}
}

void CEditListCtrlColor::AddListChangeHandler (CListChangeHandler *Handler)
{
	LsChHandlers.push_back (Handler);
}

void CEditListCtrlColor::PerformListChangeHandler ()
{
	for (std::vector<CListChangeHandler *>::iterator it = LsChHandlers.begin ();
		                                            it != LsChHandlers.end ();
													++it)
	{
		CListChangeHandler *Handler = *it;
		Handler->RowChanged (EditRow);
	}
}

void CEditListCtrlColor::PerformListChangeHandler (int Row, int Col)
{
	for (std::vector<CListChangeHandler *>::iterator it = LsChHandlers.begin ();
		                                            it != LsChHandlers.end ();
													++it)
	{
		CListChangeHandler *Handler = *it;
		Handler->ColChanged (Row, Col);
	}
}

void CEditListCtrlColor::AddDisplayOnly (int pos)
{
	DisplayOnlies.push_back (pos);
}

void CEditListCtrlColor::RemoveDisplayOnly (int pos)
{
	for (std::vector<int>::iterator it = DisplayOnlies.begin ();
		                                            it != DisplayOnlies.end ();
													)
	{
		int DisplayOnlyPos = *it;
		if (DisplayOnlyPos == pos)
		{
			DisplayOnlies.erase (it);
			break;
		}
		++it;
	}
}

BOOL CEditListCtrlColor::IsDisplayOnly (int pos)
{
	for (std::vector<int>::iterator it = DisplayOnlies.begin ();
                                    it != DisplayOnlies.end ();
									)
	{
		int DisplayOnlyPos = *it;
		if (DisplayOnlyPos == pos)
		{
			return TRUE;
		}
		++it;
	}
	return FALSE;
}

BOOL CEditListCtrlColor::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	LPNMHDR pnmh = (LPNMHDR) lParam;

	if (GetItemCount () > 0)
	{
		if (pnmh->code == (UINT)HDN_BEGINTRACKA || pnmh->code == (UINT)HDN_BEGINTRACKW) //begin track
		{
			StopEnter ();
		}
		if (pnmh->code == (UINT)HDN_ENDTRACKA || pnmh->code == (UINT)HDN_ENDTRACKW) //end track
		{
			PostMessage (WM_STARTENTER, 0, 0l);
		}
	}
	return CListCtrl::OnNotify(wParam, lParam, pResult);
}