#include "stdafx.h"
#include "a_kun.h"

struct A_KUN a_kun, a_kun_null, a_kun_def;

void A_KUN_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &a_kun.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &a_kun.fil,  SQLSHORT, 0);
            sqlin ((long *)    &a_kun.kun,  SQLLONG, 0);
            sqlin ((char *)    a_kun.kun_bran2,  SQLCHAR, sizeof (a_kun.kun_bran2));
            sqlin ((double *)   &a_kun.a,  SQLDOUBLE, 0);
    sqlout ((short *) &a_kun.mdn,SQLSHORT,0);
    sqlout ((short *) &a_kun.fil,SQLSHORT,0);
    sqlout ((long *) &a_kun.kun,SQLLONG,0);
    sqlout ((double *) &a_kun.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun.a_kun,SQLCHAR,13);
    sqlout ((TCHAR *) a_kun.a_bz1,SQLCHAR,25);
    sqlout ((short *) &a_kun.me_einh_kun,SQLSHORT,0);
    sqlout ((double *) &a_kun.inh,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun.kun_bran2,SQLCHAR,3);
    sqlout ((double *) &a_kun.tara,SQLDOUBLE,0);
    sqlout ((double *) &a_kun.ean,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun.a_bz2,SQLCHAR,25);
    sqlout ((short *) &a_kun.hbk_ztr,SQLSHORT,0);
    sqlout ((long *) &a_kun.kopf_text,SQLLONG,0);
    sqlout ((TCHAR *) a_kun.pr_rech_kz,SQLCHAR,2);
    sqlout ((TCHAR *) a_kun.modif,SQLCHAR,2);
    sqlout ((short *) &a_kun.pos_fill,SQLSHORT,0);
    sqlout ((short *) &a_kun.ausz_art,SQLSHORT,0);
    sqlout ((long *) &a_kun.text_nr2,SQLLONG,0);
    sqlout ((short *) &a_kun.cab,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.a_bz3,SQLCHAR,25);
    sqlout ((TCHAR *) a_kun.a_bz4,SQLCHAR,25);
    sqlout ((long *) &a_kun.text_nr,SQLLONG,0);
    sqlout ((short *) &a_kun.devise,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.geb_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_kun.geb_fill,SQLCHAR,2);
    sqlout ((long *) &a_kun.geb_anz,SQLLONG,0);
    sqlout ((TCHAR *) a_kun.pal_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_kun.pal_fill,SQLCHAR,2);
    sqlout ((short *) &a_kun.pal_anz,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.pos_eti,SQLCHAR,2);
    sqlout ((short *) &a_kun.sg1,SQLSHORT,0);
    sqlout ((short *) &a_kun.sg2,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.li_a,SQLCHAR,13);
    sqlout ((double *) &a_kun.geb_fakt,SQLDOUBLE,0);
    sqlout ((double *) &a_kun.ean_vk,SQLDOUBLE,0);
            cursor = sqlcursor (_T("select a_kun.mdn,  ")
_T("a_kun.fil,  a_kun.kun,  a_kun.a,  a_kun.a_kun,  a_kun.a_bz1,  ")
_T("a_kun.me_einh_kun,  a_kun.inh,  a_kun.kun_bran2,  a_kun.tara,  a_kun.ean,  ")
_T("a_kun.a_bz2,  a_kun.hbk_ztr,  a_kun.kopf_text,  a_kun.pr_rech_kz,  ")
_T("a_kun.modif,  a_kun.pos_fill,  a_kun.ausz_art,  a_kun.text_nr2,  ")
_T("a_kun.cab,  a_kun.a_bz3,  a_kun.a_bz4,  a_kun.text_nr,  a_kun.devise,  ")
_T("a_kun.geb_eti,  a_kun.geb_fill,  a_kun.geb_anz,  a_kun.pal_eti,  ")
_T("a_kun.pal_fill,  a_kun.pal_anz,  a_kun.pos_eti,  a_kun.sg1,  a_kun.sg2,  ")
_T("a_kun.li_a,  a_kun.geb_fakt,  a_kun.ean_vk from a_kun ")

#line 16 "a_kun.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and kun_bran2 = ? ")
                                  _T("and a = ? "));

            sqlin ((short *)   &a_kun.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &a_kun.fil,  SQLSHORT, 0);
            sqlin ((long *)    &a_kun.kun,  SQLLONG, 0);
            sqlin ((double *)   &a_kun.a,  SQLDOUBLE, 0);
    sqlout ((short *) &a_kun.mdn,SQLSHORT,0);
    sqlout ((short *) &a_kun.fil,SQLSHORT,0);
    sqlout ((long *) &a_kun.kun,SQLLONG,0);
    sqlout ((double *) &a_kun.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun.a_kun,SQLCHAR,13);
    sqlout ((TCHAR *) a_kun.a_bz1,SQLCHAR,25);
    sqlout ((short *) &a_kun.me_einh_kun,SQLSHORT,0);
    sqlout ((double *) &a_kun.inh,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun.kun_bran2,SQLCHAR,3);
    sqlout ((double *) &a_kun.tara,SQLDOUBLE,0);
    sqlout ((double *) &a_kun.ean,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun.a_bz2,SQLCHAR,25);
    sqlout ((short *) &a_kun.hbk_ztr,SQLSHORT,0);
    sqlout ((long *) &a_kun.kopf_text,SQLLONG,0);
    sqlout ((TCHAR *) a_kun.pr_rech_kz,SQLCHAR,2);
    sqlout ((TCHAR *) a_kun.modif,SQLCHAR,2);
    sqlout ((short *) &a_kun.pos_fill,SQLSHORT,0);
    sqlout ((short *) &a_kun.ausz_art,SQLSHORT,0);
    sqlout ((long *) &a_kun.text_nr2,SQLLONG,0);
    sqlout ((short *) &a_kun.cab,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.a_bz3,SQLCHAR,25);
    sqlout ((TCHAR *) a_kun.a_bz4,SQLCHAR,25);
    sqlout ((long *) &a_kun.text_nr,SQLLONG,0);
    sqlout ((short *) &a_kun.devise,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.geb_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_kun.geb_fill,SQLCHAR,2);
    sqlout ((long *) &a_kun.geb_anz,SQLLONG,0);
    sqlout ((TCHAR *) a_kun.pal_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_kun.pal_fill,SQLCHAR,2);
    sqlout ((short *) &a_kun.pal_anz,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.pos_eti,SQLCHAR,2);
    sqlout ((short *) &a_kun.sg1,SQLSHORT,0);
    sqlout ((short *) &a_kun.sg2,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.li_a,SQLCHAR,13);
    sqlout ((double *) &a_kun.geb_fakt,SQLDOUBLE,0);
    sqlout ((double *) &a_kun.ean_vk,SQLDOUBLE,0);
            cursor_kun = sqlcursor (_T("select a_kun.mdn,  ")
_T("a_kun.fil,  a_kun.kun,  a_kun.a,  a_kun.a_kun,  a_kun.a_bz1,  ")
_T("a_kun.me_einh_kun,  a_kun.inh,  a_kun.kun_bran2,  a_kun.tara,  a_kun.ean,  ")
_T("a_kun.a_bz2,  a_kun.hbk_ztr,  a_kun.kopf_text,  a_kun.pr_rech_kz,  ")
_T("a_kun.modif,  a_kun.pos_fill,  a_kun.ausz_art,  a_kun.text_nr2,  ")
_T("a_kun.cab,  a_kun.a_bz3,  a_kun.a_bz4,  a_kun.text_nr,  a_kun.devise,  ")
_T("a_kun.geb_eti,  a_kun.geb_fill,  a_kun.geb_anz,  a_kun.pal_eti,  ")
_T("a_kun.pal_fill,  a_kun.pal_anz,  a_kun.pos_eti,  a_kun.sg1,  a_kun.sg2,  ")
_T("a_kun.li_a,  a_kun.geb_fakt,  a_kun.ean_vk from a_kun ")

#line 27 "a_kun.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and a = ? "));

            sqlin ((short *)   &a_kun.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &a_kun.fil,  SQLSHORT, 0);
            sqlin ((char *)    a_kun.kun_bran2,  SQLCHAR, sizeof (a_kun.kun_bran2));
            sqlin ((double *)   &a_kun.a,  SQLDOUBLE, 0);
    sqlout ((short *) &a_kun.mdn,SQLSHORT,0);
    sqlout ((short *) &a_kun.fil,SQLSHORT,0);
    sqlout ((long *) &a_kun.kun,SQLLONG,0);
    sqlout ((double *) &a_kun.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun.a_kun,SQLCHAR,13);
    sqlout ((TCHAR *) a_kun.a_bz1,SQLCHAR,25);
    sqlout ((short *) &a_kun.me_einh_kun,SQLSHORT,0);
    sqlout ((double *) &a_kun.inh,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun.kun_bran2,SQLCHAR,3);
    sqlout ((double *) &a_kun.tara,SQLDOUBLE,0);
    sqlout ((double *) &a_kun.ean,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun.a_bz2,SQLCHAR,25);
    sqlout ((short *) &a_kun.hbk_ztr,SQLSHORT,0);
    sqlout ((long *) &a_kun.kopf_text,SQLLONG,0);
    sqlout ((TCHAR *) a_kun.pr_rech_kz,SQLCHAR,2);
    sqlout ((TCHAR *) a_kun.modif,SQLCHAR,2);
    sqlout ((short *) &a_kun.pos_fill,SQLSHORT,0);
    sqlout ((short *) &a_kun.ausz_art,SQLSHORT,0);
    sqlout ((long *) &a_kun.text_nr2,SQLLONG,0);
    sqlout ((short *) &a_kun.cab,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.a_bz3,SQLCHAR,25);
    sqlout ((TCHAR *) a_kun.a_bz4,SQLCHAR,25);
    sqlout ((long *) &a_kun.text_nr,SQLLONG,0);
    sqlout ((short *) &a_kun.devise,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.geb_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_kun.geb_fill,SQLCHAR,2);
    sqlout ((long *) &a_kun.geb_anz,SQLLONG,0);
    sqlout ((TCHAR *) a_kun.pal_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_kun.pal_fill,SQLCHAR,2);
    sqlout ((short *) &a_kun.pal_anz,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.pos_eti,SQLCHAR,2);
    sqlout ((short *) &a_kun.sg1,SQLSHORT,0);
    sqlout ((short *) &a_kun.sg2,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun.li_a,SQLCHAR,13);
    sqlout ((double *) &a_kun.geb_fakt,SQLDOUBLE,0);
    sqlout ((double *) &a_kun.ean_vk,SQLDOUBLE,0);
            cursor_bra = sqlcursor (_T("select a_kun.mdn,  ")
_T("a_kun.fil,  a_kun.kun,  a_kun.a,  a_kun.a_kun,  a_kun.a_bz1,  ")
_T("a_kun.me_einh_kun,  a_kun.inh,  a_kun.kun_bran2,  a_kun.tara,  a_kun.ean,  ")
_T("a_kun.a_bz2,  a_kun.hbk_ztr,  a_kun.kopf_text,  a_kun.pr_rech_kz,  ")
_T("a_kun.modif,  a_kun.pos_fill,  a_kun.ausz_art,  a_kun.text_nr2,  ")
_T("a_kun.cab,  a_kun.a_bz3,  a_kun.a_bz4,  a_kun.text_nr,  a_kun.devise,  ")
_T("a_kun.geb_eti,  a_kun.geb_fill,  a_kun.geb_anz,  a_kun.pal_eti,  ")
_T("a_kun.pal_fill,  a_kun.pal_anz,  a_kun.pos_eti,  a_kun.sg1,  a_kun.sg2,  ")
_T("a_kun.li_a,  a_kun.geb_fakt,  a_kun.ean_vk from a_kun ")

#line 37 "a_kun.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun_bran2 = ? ")
                                  _T("and a = ? "));

    sqlin ((short *) &a_kun.mdn,SQLSHORT,0);
    sqlin ((short *) &a_kun.fil,SQLSHORT,0);
    sqlin ((long *) &a_kun.kun,SQLLONG,0);
    sqlin ((double *) &a_kun.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun.a_kun,SQLCHAR,13);
    sqlin ((TCHAR *) a_kun.a_bz1,SQLCHAR,25);
    sqlin ((short *) &a_kun.me_einh_kun,SQLSHORT,0);
    sqlin ((double *) &a_kun.inh,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun.kun_bran2,SQLCHAR,3);
    sqlin ((double *) &a_kun.tara,SQLDOUBLE,0);
    sqlin ((double *) &a_kun.ean,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun.a_bz2,SQLCHAR,25);
    sqlin ((short *) &a_kun.hbk_ztr,SQLSHORT,0);
    sqlin ((long *) &a_kun.kopf_text,SQLLONG,0);
    sqlin ((TCHAR *) a_kun.pr_rech_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_kun.modif,SQLCHAR,2);
    sqlin ((short *) &a_kun.pos_fill,SQLSHORT,0);
    sqlin ((short *) &a_kun.ausz_art,SQLSHORT,0);
    sqlin ((long *) &a_kun.text_nr2,SQLLONG,0);
    sqlin ((short *) &a_kun.cab,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun.a_bz3,SQLCHAR,25);
    sqlin ((TCHAR *) a_kun.a_bz4,SQLCHAR,25);
    sqlin ((long *) &a_kun.text_nr,SQLLONG,0);
    sqlin ((short *) &a_kun.devise,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun.geb_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_kun.geb_fill,SQLCHAR,2);
    sqlin ((long *) &a_kun.geb_anz,SQLLONG,0);
    sqlin ((TCHAR *) a_kun.pal_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_kun.pal_fill,SQLCHAR,2);
    sqlin ((short *) &a_kun.pal_anz,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun.pos_eti,SQLCHAR,2);
    sqlin ((short *) &a_kun.sg1,SQLSHORT,0);
    sqlin ((short *) &a_kun.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun.li_a,SQLCHAR,13);
    sqlin ((double *) &a_kun.geb_fakt,SQLDOUBLE,0);
    sqlin ((double *) &a_kun.ean_vk,SQLDOUBLE,0);
            sqltext = _T("update a_kun set a_kun.mdn = ?,  ")
_T("a_kun.fil = ?,  a_kun.kun = ?,  a_kun.a = ?,  a_kun.a_kun = ?,  ")
_T("a_kun.a_bz1 = ?,  a_kun.me_einh_kun = ?,  a_kun.inh = ?,  ")
_T("a_kun.kun_bran2 = ?,  a_kun.tara = ?,  a_kun.ean = ?,  ")
_T("a_kun.a_bz2 = ?,  a_kun.hbk_ztr = ?,  a_kun.kopf_text = ?,  ")
_T("a_kun.pr_rech_kz = ?,  a_kun.modif = ?,  a_kun.pos_fill = ?,  ")
_T("a_kun.ausz_art = ?,  a_kun.text_nr2 = ?,  a_kun.cab = ?,  ")
_T("a_kun.a_bz3 = ?,  a_kun.a_bz4 = ?,  a_kun.text_nr = ?,  ")
_T("a_kun.devise = ?,  a_kun.geb_eti = ?,  a_kun.geb_fill = ?,  ")
_T("a_kun.geb_anz = ?,  a_kun.pal_eti = ?,  a_kun.pal_fill = ?,  ")
_T("a_kun.pal_anz = ?,  a_kun.pos_eti = ?,  a_kun.sg1 = ?,  a_kun.sg2 = ?,  ")
_T("a_kun.li_a = ?,  a_kun.geb_fakt = ?,  a_kun.ean_vk = ? ")

#line 43 "a_kun.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and kun_bran2 = ? ")
                                  _T("and a = ? ");
            sqlin ((short *)   &a_kun.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &a_kun.fil,  SQLSHORT, 0);
            sqlin ((long *)    &a_kun.kun,  SQLLONG, 0);
            sqlin ((char *)    a_kun.kun_bran2,  SQLCHAR, sizeof (a_kun.kun_bran2));
            sqlin ((double *)   &a_kun.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &a_kun.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &a_kun.fil,  SQLSHORT, 0);
            sqlin ((long *)    &a_kun.kun,  SQLLONG, 0);
            sqlin ((char *)    a_kun.kun_bran2,  SQLCHAR, sizeof (a_kun.kun_bran2));
            sqlin ((double *)   &a_kun.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_kun ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and kun_bran2 = ? ")
                                  _T("and a = ? "));
            sqlin ((short *)   &a_kun.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &a_kun.fil,  SQLSHORT, 0);
            sqlin ((long *)    &a_kun.kun,  SQLLONG, 0);
            sqlin ((char *)    a_kun.kun_bran2,  SQLCHAR, sizeof (a_kun.kun_bran2));
            sqlin ((double *)   &a_kun.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_kun ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and kun_bran2 = ? ")
                                  _T("and a = ? "));
    sqlin ((short *) &a_kun.mdn,SQLSHORT,0);
    sqlin ((short *) &a_kun.fil,SQLSHORT,0);
    sqlin ((long *) &a_kun.kun,SQLLONG,0);
    sqlin ((double *) &a_kun.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun.a_kun,SQLCHAR,13);
    sqlin ((TCHAR *) a_kun.a_bz1,SQLCHAR,25);
    sqlin ((short *) &a_kun.me_einh_kun,SQLSHORT,0);
    sqlin ((double *) &a_kun.inh,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun.kun_bran2,SQLCHAR,3);
    sqlin ((double *) &a_kun.tara,SQLDOUBLE,0);
    sqlin ((double *) &a_kun.ean,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun.a_bz2,SQLCHAR,25);
    sqlin ((short *) &a_kun.hbk_ztr,SQLSHORT,0);
    sqlin ((long *) &a_kun.kopf_text,SQLLONG,0);
    sqlin ((TCHAR *) a_kun.pr_rech_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_kun.modif,SQLCHAR,2);
    sqlin ((short *) &a_kun.pos_fill,SQLSHORT,0);
    sqlin ((short *) &a_kun.ausz_art,SQLSHORT,0);
    sqlin ((long *) &a_kun.text_nr2,SQLLONG,0);
    sqlin ((short *) &a_kun.cab,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun.a_bz3,SQLCHAR,25);
    sqlin ((TCHAR *) a_kun.a_bz4,SQLCHAR,25);
    sqlin ((long *) &a_kun.text_nr,SQLLONG,0);
    sqlin ((short *) &a_kun.devise,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun.geb_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_kun.geb_fill,SQLCHAR,2);
    sqlin ((long *) &a_kun.geb_anz,SQLLONG,0);
    sqlin ((TCHAR *) a_kun.pal_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_kun.pal_fill,SQLCHAR,2);
    sqlin ((short *) &a_kun.pal_anz,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun.pos_eti,SQLCHAR,2);
    sqlin ((short *) &a_kun.sg1,SQLSHORT,0);
    sqlin ((short *) &a_kun.sg2,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun.li_a,SQLCHAR,13);
    sqlin ((double *) &a_kun.geb_fakt,SQLDOUBLE,0);
    sqlin ((double *) &a_kun.ean_vk,SQLDOUBLE,0);
            ins_cursor = sqlcursor (_T("insert into a_kun (")
_T("mdn,  fil,  kun,  a,  a_kun,  a_bz1,  me_einh_kun,  inh,  kun_bran2,  tara,  ean,  a_bz2,  ")
_T("hbk_ztr,  kopf_text,  pr_rech_kz,  modif,  pos_fill,  ausz_art,  text_nr2,  cab,  ")
_T("a_bz3,  a_bz4,  text_nr,  devise,  geb_eti,  geb_fill,  geb_anz,  pal_eti,  pal_fill,  ")
_T("pal_anz,  pos_eti,  sg1,  sg2,  li_a,  geb_fakt,  ean_vk) ")

#line 78 "a_kun.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 80 "a_kun.rpp"
}

int A_KUN_CLASS::dbreadfirst_kun ()
{
        int dsqlstatus = 0;
	if (cursor_kun == -1)
        {
		prepare ();
        }
        dsqlstatus = sqlopen (cursor_kun);
        if (dsqlstatus == 0)
        {
                dsqlstatus = sqlfetch (cursor_kun); 
        }  
        return dsqlstatus;
}

int A_KUN_CLASS::dbread_kun ()
{
        return sqlfetch (cursor_kun); 

}

int A_KUN_CLASS::dbreadfirst_bra ()
{
        int dsqlstatus = 0;
	if (cursor_bra == -1)
        {
		prepare ();
        }
        dsqlstatus = sqlopen (cursor_bra);
        if (dsqlstatus == 0)
        {
                dsqlstatus = sqlfetch (cursor_bra); 
        }  
        return dsqlstatus;
}

int A_KUN_CLASS::dbread_bra ()
{
        return sqlfetch (cursor_bra); 

}
