#ifndef _TEXTPOOL_DEF
#define _TEXTPOOL_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct TEXTPOOL {
   long           nr;
   short          typ;
   char           text[513];
};
extern struct TEXTPOOL textpool, textpool_null;

#line 8 "Textpool.rh"

class TEXTPOOL_CLASS : public DB_CLASS
{
       private :
               void prepare (void);
       public :
               TEXTPOOL textpool;
               TEXTPOOL_CLASS () : DB_CLASS ()
               {
               }
};
#endif
