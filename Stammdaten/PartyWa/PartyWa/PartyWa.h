// PartyWa.h : Hauptheaderdatei f�r die PartyWa-Anwendung
//
#pragma once

#ifndef __AFXWIN_H__
	#error "\"stdafx.h\" vor dieser Datei f�r PCH einschlie�en"
#endif

#include "resource.h"       // Hauptsymbole


// CPartyWaApp:
// Siehe PartyWa.cpp f�r die Implementierung dieser Klasse
//

class CPartyWaApp : public CWinApp
{
public:
	CPartyWaApp();


// �berschreibungen
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementierung
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CPartyWaApp theApp;