#include "StdAfx.h"
#include "CalcView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//IMPLEMENT_DYNCREATE(CCalcView, CFormView)

CCalcView::CCalcView(void)
	: CFormView(CCalcView::IDD)
{
	WeightRow = 5;
	AngebotCore = CAngebotCore::GetInstance ();
	Partyk = CDatatables::GetInstance ()->GetPartyk ();
	Partyp = CDatatables::GetInstance ()->GetPartyp ();
	CtrlGrid = NULL;
}

CCalcView::~CCalcView(void)
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}

	SmtForm.FirstPosition ();
	while ((f = (CFormField *) SmtForm.GetNext ()) != NULL)
	{
		delete f;
	}
	CalcSmtLabels.DestroyElements ();
	delete ShowCalcSmt;
	delete RepaintCalcSmt;
	if (CtrlGrid != NULL)
	{
		delete CtrlGrid;
	}
}

void CCalcView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TITLE_BER, m_TitleBer);
	DDX_Control(pDX, IDC_TITLE_PAUSCH, m_TitlePausch);
	DDX_Control(pDX, IDC_TITLE_OSERVICE, m_TitleOService);
	DDX_Control(pDX, IDC_TITLE_BGESAMT, m_TitleBGesamt);
	DDX_Control(pDX, IDC_TITLE_BEINZEL, m_TitleBEinzel);
	DDX_Control(pDX, IDC_TITLE_PGESAMT, m_TitlePGesamt);
	DDX_Control(pDX, IDC_TITLE_PEINZEL, m_TitlePEinzel);
	DDX_Control(pDX, IDC_TITLE_OSGESAMT, m_TitleOSGesamt);
	DDX_Control(pDX, IDC_TITLE_OSEINZEL, m_TitleOSEinzel);
	DDX_Control(pDX, IDC_LTOTAL_PRICE, m_LTotalPrice);
	DDX_Control(pDX, IDC_TOTAL_BGPRICE, m_TotalBGPrice);
	DDX_Control(pDX, IDC_TOTAL_BSPRICE, m_TotalBSPrice);
	DDX_Control(pDX, IDC_TOTAL_PGPRICE, m_TotalPGPrice);
	DDX_Control(pDX, IDC_TOTAL_PSPRICE, m_TotalPSPrice);
	DDX_Control(pDX, IDC_TOTAL_OSGPRICE, m_TotalOSGPrice);
	DDX_Control(pDX, IDC_TOTAL_OSSPRICE, m_TotalOSSPrice);
	DDX_Control(pDX, IDC_LTOTAL_WEIGHT, m_LTotalWeight);
	DDX_Control(pDX, IDC_TOTAL_WEIGHT, m_TotalWeight);
	DDX_Control(pDX, IDC_LSINGLE_WEIGHT, m_LSingleWeight);
	DDX_Control(pDX, IDC_SINGLE_WEIGHT, m_SingleWeight);
}

BEGIN_MESSAGE_MAP(CCalcView, CFormView)
//	ON_WM_SIZE ()
//	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()

void CCalcView::Create (CFrameWnd *CalcFrame)
{
	CRect rect;
	CalcFrame->GetClientRect (&rect);
	CFormView::Create (NULL, NULL, WS_CHILD, rect, CalcFrame, 12345, NULL);
}

BOOL CCalcView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	BOOL ret = CFormView::PreCreateWindow(cs);
    return ret;
}

void CCalcView::OnInitialUpdate ()
{
	CFormView::OnInitialUpdate();

	m_TitleBer.Properties (_T(" Berechnet"), CLabel::None,
							  CLabel::Center, TRUE, RGB (0,0,255),
							  RGB (192, 192, 192));

	m_TitlePausch.Properties (_T(" Pauschal"), CLabel::None,
							  CLabel::Center, TRUE, RGB (0,0,255),
							  RGB (192, 192, 192));


	m_TitleOService.Properties (_T(" berechet (ohne Service)"), CLabel::None,
							  CLabel::Center, TRUE, RGB (0,0,255),
							  RGB (192, 192, 192));


	m_TitleBGesamt.Properties (_T("gesamt"), CLabel::None,
							  CLabel::Center, TRUE, RGB (0,0,0),
							  RGB (192, 192, 192));


	m_TitleBEinzel.Properties (_T("pro Person"), CLabel::None,
							  CLabel::Center, TRUE, RGB (0,0,0),
							  RGB (192, 192, 192));


	m_TitlePGesamt.Properties (_T("gesamt"), CLabel::None,
							  CLabel::Center, TRUE, RGB (0,0,0),
							  RGB (192, 192, 192));

	m_TitlePEinzel.Properties (_T("pro Person"), CLabel::None,
							  CLabel::Center, TRUE, RGB (0,0,0),
							  RGB (192, 192, 192));

	m_TitleOSGesamt.Properties (_T("gesamt"), CLabel::None,
							  CLabel::Center, TRUE, RGB (0,0,0),
							  RGB (192, 192, 192));

	m_TitleOSEinzel.Properties (_T("pro Person"), CLabel::None,
							  CLabel::Center, TRUE, RGB (0,0,0),
							  RGB (192, 192, 192));

	m_LTotalPrice.Properties (_T("Gesamtpreis"), CLabel::None,
							  CLabel::Left, FALSE, RGB (0,0,0),
							  GetSysColor (COLOR_3DFACE));

	int DataBorders = CLabel::None;
//	COLORREF DataBackground = GetSysColor (COLOR_3DFACE);
//	COLORREF DataBackground = RGB (255, 255, 255);
	COLORREF DataBackground = RGB (251, 251, 215);
	m_TotalBGPrice.Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);

	m_TotalBSPrice.Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);

	m_TotalPGPrice.Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);

	m_TotalPSPrice.Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);

	m_TotalOSGPrice.Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);

	m_TotalOSSPrice.Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);

	m_LTotalWeight.Properties (_T("Gesamtgewicht"), CLabel::None,
							  CLabel::Left, FALSE, RGB (0,0,0),
							  GetSysColor (COLOR_3DFACE));

	m_TotalWeight.Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);

	m_LSingleWeight.Properties (_T("Einzelgewicht"), CLabel::None,
							  CLabel::Left, FALSE, RGB (0,0,0),
							  GetSysColor (COLOR_3DFACE));

	m_SingleWeight.Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	RegisterCalcForm ();
	ShowCalcSmt = new CShowCalcSmt (this);
	AngebotCore->RegisterShowCalcSmt (ShowCalcSmt);
	RepaintCalcSmt = new CRepaintCalcSmt (this);
	AngebotCore->RegisterRepaintCalcSmt (RepaintCalcSmt);

//	AngebotCore->SetCalcForm (&Form);
	Form.Add (new CFormField (&m_TotalBGPrice, EDIT,      (double *)   &Partyk->partyk.ang_vk, VDOUBLE, 8, 2));
	Form.Add (new CFormField (&m_TotalBSPrice, EDIT,      (double *)   &Partyk->partyk.einzel_vk, VDOUBLE, 8, 2));
	Form.Add (new CFormField (&m_TotalPGPrice, EDIT,      (double *)   AngebotCore->GetPPauschVk (), VDOUBLE, 8, 2));
	Form.Add (new CFormField (&m_TotalPSPrice, EDIT,      (double *)   AngebotCore->GetPPauschSingleVk (), VDOUBLE, 8, 2));
//	Form.Add (new CFormField (&m_TotalPGPrice, EDIT,      (double *)   &Partyk->partyk.pauschal_vk, VDOUBLE, 8, 2));
//	Form.Add (new CFormField (&m_TotalPSPrice, EDIT,      (double *)   &Partyk->partyk.einzel_vk_pausch, VDOUBLE, 8, 2));
	Form.Add (new CFormField (&m_TotalOSGPrice, EDIT,     (double *)   AngebotCore->GetPOSGPrice (), VDOUBLE, 8, 2));
	Form.Add (new CFormField (&m_TotalOSSPrice, EDIT,     (double *)   AngebotCore->GetPOSSPrice (), VDOUBLE, 8, 2));
	Form.Add (new CFormField (&m_TotalWeight,   EDIT,     (double *)   &Partyk->partyk.ang_gew, VDOUBLE, 10, 3));
	Form.Add (new CFormField (&m_SingleWeight,  EDIT,     (double *)   AngebotCore->GetPSingleGew (), VDOUBLE, 10, 3));


//Grid MainTitle
	MainTitleGrid.Create (this, 5, 5);
    MainTitleGrid.SetBorder (0, 0);
    MainTitleGrid.SetGridSpace (1, 0);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);

	CreateGrid ();
	CRect rect;
	CRect OrgRect;
	CRect FrameRect; 
    this->GetParent ()->GetWindowRect (&FrameRect);
	int FrameHeight = FrameRect.bottom - FrameRect.top;
	GetWindowRect (&OrgRect);
	CtrlGrid->GetGridRect (&rect);
	int bottomdiff = FrameRect.bottom - OrgRect.bottom;
	OrgRect.bottom = rect.bottom - rect.top;
//	OrgRect.right = rect.right - rect.left;
	MoveWindow (OrgRect);
	FrameRect.bottom = rect.bottom + bottomdiff + 40;
	if ((FrameRect.bottom - FrameRect.top) < FrameHeight)
	{
//		this->GetParent ()->MoveWindow (FrameRect);
	}
}

void CCalcView::CreateGrid ()
{
    CCalcSmt **it;
    CCalcSmt *CalcSmt;
	DWORD SmtId;
	CRect rect;

	this->LockWindowUpdate ();
	WeightRow = 5;
	SmtId = 12000;

	int DataBorders = CLabel::None;
//	COLORREF DataBackground = GetSysColor (COLOR_3DFACE);
//	COLORREF DataBackground = RGB (255, 255, 255);
	COLORREF DataBackground = RGB (251, 251, 215);


	SmtForm.DestroyAll ();

	if (CtrlGrid != NULL)
	{
		delete CtrlGrid;
	}
	CtrlGrid = new CCtrlGrid ();
    CtrlGrid->Create (this, 80, 80);
	CtrlGrid->SetFont (&Font);
    CtrlGrid->SetBorder (2, 0);
	CtrlGrid->SetCellHeight (15);
    CtrlGrid->SetGridSpace (1, 8);  

	CalcSmtLabels.DestroyElements ();
	CalcSmtTab = AngebotCore->GetCalcSmtTab ();

	CCtrlInfo *c_TitleBer = new CCtrlInfo (&m_TitleBer, 1, 0, 2, 1);
	CtrlGrid->Add (c_TitleBer);

	CCtrlInfo *c_TitlePausch = new CCtrlInfo (&m_TitlePausch, 3, 0, 2, 1);
	CtrlGrid->Add (c_TitlePausch);

	CCtrlInfo *c_TitleOService = new CCtrlInfo (&m_TitleOService, 5, 0, 2, 1);
	CtrlGrid->Add (c_TitleOService);

	CCtrlInfo *c_TitleBGesamt = new CCtrlInfo (&m_TitleBGesamt, 1, 2, 1, 1);
	c_TitleBGesamt->SetCellPos (5, -15);
	CtrlGrid->Add (c_TitleBGesamt);

	CCtrlInfo *c_TitleBEinzel = new CCtrlInfo (&m_TitleBEinzel, 2, 2, 1, 1);
	c_TitleBEinzel->SetCellPos (5, -15);
	CtrlGrid->Add (c_TitleBEinzel);

	CCtrlInfo *c_TitlePGesamt = new CCtrlInfo (&m_TitlePGesamt, 3, 2, 1, 1);
	c_TitlePGesamt->SetCellPos (5, -15);
	CtrlGrid->Add (c_TitlePGesamt);

	CCtrlInfo *c_TitlePEinzel = new CCtrlInfo (&m_TitlePEinzel, 4, 2, 1, 1);
	c_TitlePEinzel->SetCellPos (5, -15);
	CtrlGrid->Add (c_TitlePEinzel);

	CCtrlInfo *c_TitleOSGesamt = new CCtrlInfo (&m_TitleOSGesamt, 5, 2, 1, 1);
	c_TitleOSGesamt->SetCellPos (5, -15);
	CtrlGrid->Add (c_TitleOSGesamt);

	CCtrlInfo *c_TitleOSEinzel = new CCtrlInfo (&m_TitleOSEinzel, 6, 2, 1, 1);
	c_TitleOSEinzel->SetCellPos (5, -15);
	CtrlGrid->Add (c_TitleOSEinzel);

	CCtrlInfo *c_LTotalPrice = new CCtrlInfo (&m_LTotalPrice, 0, 3, 1, 1);
	CtrlGrid->Add (c_LTotalPrice);
	CCtrlInfo *c_TotalBGPrice = new CCtrlInfo (&m_TotalBGPrice, 1, 3, 1, 1);
	CtrlGrid->Add (c_TotalBGPrice);
	CCtrlInfo *c_TotalBSPrice = new CCtrlInfo (&m_TotalBSPrice, 2, 3, 1, 1);
	CtrlGrid->Add (c_TotalBSPrice);
	CCtrlInfo *c_TotalPGPrice = new CCtrlInfo (&m_TotalPGPrice, 3, 3, 1, 1);
	CtrlGrid->Add (c_TotalPGPrice);
	CCtrlInfo *c_TotalPSPrice = new CCtrlInfo (&m_TotalPSPrice, 4, 3, 1, 1);
	CtrlGrid->Add (c_TotalPSPrice);

	CCtrlInfo *c_TotalOSGPrice = new CCtrlInfo (&m_TotalOSGPrice, 5, 3, 1, 1);
	CtrlGrid->Add (c_TotalOSGPrice);
	CCtrlInfo *c_TotalOSSPrice = new CCtrlInfo (&m_TotalOSSPrice, 6, 3, 1, 1);
	CtrlGrid->Add (c_TotalOSSPrice);

	if (CalcSmtTab != NULL)
	{
		CalcSmtTab->Start ();
		while ((it = CalcSmtTab->GetNext ()) != NULL)
		{
			CalcSmt = *it;
			m_LTotalPrice.GetWindowRect (&rect);
			CVLabel *Label = new CVLabel ();
			LPTSTR Bez = CalcSmt->GetSmtPBez ();
			CDC *cDC = GetDC ();
			CSize s = cDC->GetTextExtent (CalcSmt->GetSmtPBez ());
			rect.right = rect.left + s.cx + 10;
			Label->Create (Bez, SS_OWNERDRAW | WS_CHILD | WS_VISIBLE, rect, this, SmtId); 
 			Label->Properties (Bez, CLabel::None,
							  CLabel::Left, FALSE, RGB (0,0,0),
							  GetSysColor (COLOR_3DFACE));
			CalcSmtLabels.Add (Label);
			SmtId ++;
			CCtrlInfo *c_Label = new CCtrlInfo (Label, 0, WeightRow, 1, 1);
			CtrlGrid->Add (c_Label);

			m_TotalBGPrice.GetWindowRect (&rect);
			rect.right = rect.left + 80;
			CVLabel *Value = new CVLabel ();

// Errechneter Gesamtpreis f�r Sortiment
			Value->Create (_T(""), SS_OWNERDRAW | WS_CHILD | WS_VISIBLE, rect, this, SmtId); 
			Value->Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);
			CalcSmtLabels.Add (Value);
			SmtId ++;
			CCtrlInfo *c_Value = new CCtrlInfo (Value, 1, WeightRow, 1, 1);
			CtrlGrid->Add (c_Value);
 			SmtForm.Add (new CFormField (Value, EDIT,      (double *)  CalcSmt->GetAngVk (), VDOUBLE, 8, 2));

// Errechneter Einzelpreis f�r Sortiment
			Value = new CVLabel ();

			Value->Create (_T(""), SS_OWNERDRAW | WS_CHILD | WS_VISIBLE, rect, this, SmtId); 
			Value->Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);
			CalcSmtLabels.Add (Value);
			SmtId ++;
			c_Value = new CCtrlInfo (Value, 2, WeightRow, 1, 1);
			CtrlGrid->Add (c_Value);
 			SmtForm.Add (new CFormField (Value, EDIT,      (double *)  CalcSmt->GetAngSingleVk (), VDOUBLE, 8, 2));

// Errechneter Gesamtpreis (Service) f�r Sortiment

			Value = new CVLabel ();
			Value->Create (_T(""), SS_OWNERDRAW | WS_CHILD | WS_VISIBLE, rect, this, SmtId); 
			Value->Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);
			CalcSmtLabels.Add (Value);
			SmtId ++;
			c_Value = new CCtrlInfo (Value, 5, WeightRow, 1, 1);
			CtrlGrid->Add (c_Value);
			SmtForm.Add (new CFormField (Value, EDIT,      (double *)  CalcSmt->GetOSGPrice (), VDOUBLE, 8, 2));

// Errechneter Einzelpreis (Service) f�r Sortiment

			Value = new CVLabel ();
			Value->Create (_T(""), SS_OWNERDRAW | WS_CHILD | WS_VISIBLE, rect, this, SmtId); 
			Value->Properties (_T(""), DataBorders,
							  CLabel::Right, FALSE, RGB (0,0,0),
							  DataBackground);
			CalcSmtLabels.Add (Value);
			SmtId ++;
			c_Value = new CCtrlInfo (Value, 6, WeightRow, 1, 1);
			CtrlGrid->Add (c_Value);
			SmtForm.Add (new CFormField (Value, EDIT,      (double *)  CalcSmt->GetOSSPrice (), VDOUBLE, 8, 2));

			WeightRow ++;
		}
	}

	WeightRow ++;

	CCtrlInfo *c_LTotalWeight = new CCtrlInfo (&m_LTotalWeight, 0, WeightRow, 1, 1);
	CtrlGrid->Add (c_LTotalWeight);
	CCtrlInfo *c_TotalWeight = new CCtrlInfo (&m_TotalWeight, 1, WeightRow, 1, 1);
	CtrlGrid->Add (c_TotalWeight);

	CCtrlInfo *c_LSingleWeight = new CCtrlInfo (&m_LSingleWeight, 2, WeightRow, 1, 1);
	CtrlGrid->Add (c_LSingleWeight);
	CCtrlInfo *c_SingleWeight = new CCtrlInfo (&m_SingleWeight, 3, WeightRow, 1, 1);
	CtrlGrid->Add (c_SingleWeight);
	CtrlGrid->SetFont (&Font);

	m_TitlePGesamt.EnableWindow (TRUE);

    CtrlGrid->Display ();
	SmtForm.Show ();
	this->Invalidate ();
	this->UnlockWindowUpdate ();
}

void CCalcView::OnShowCalcSmt ()
{
//	if (IsWindowVisible ())
	{
		SmtForm.Show ();
	}
}

void CCalcView::OnRepaintCalcSmt ()
{
	CRect rect;
	CRect OrgRect;
	CRect FrameRect;
	CreateGrid ();
	GetWindowRect (&OrgRect);
	CtrlGrid->GetGridRect (&rect);
	OrgRect.bottom = rect.bottom;
	GetParent ()->ScreenToClient (&OrgRect);
	MoveWindow (&OrgRect);
	SetScrollSizes (MM_TEXT, CSize (OrgRect.right, OrgRect.bottom));
	GetParentFrame ()->RecalcLayout ();
}

CCalcView::CShowCalcSmt::CShowCalcSmt (CCalcView *p) : CRunMessage ()
{
	        SetId (NULL); 
			this->p = p;
}

void CCalcView::CShowCalcSmt::Run ()
{
	p->OnShowCalcSmt ();
}


CCalcView::CRepaintCalcSmt::CRepaintCalcSmt (CCalcView *p) : CRunMessage ()
{
	        SetId (NULL); 
			this->p = p;
}

void CCalcView::CRepaintCalcSmt::Run ()
{
	p->OnRepaintCalcSmt ();
}
