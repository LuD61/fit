#ifndef _ANGPT_DEF
#define _ANGPT_DEF
#include "dbclass.h"

struct ANGPT {
   long           nr;
   long           zei;
   TCHAR          txt[61];
};
extern struct ANGPT angpt, angpt_null;

#line 6 "angpt.rh"

class ANGPT_CLASS : public DB_CLASS 
{
       private :
               int del_cursor_posi;
               int max_cursor; 
               void prepare (void);
       public :
	       ANGPT angpt;	
               ANGPT_CLASS () : DB_CLASS ()
               {
                        del_cursor_posi = -1;
                        max_cursor = -1;
               }

               ~ANGPT_CLASS ()
               {
	                    this->dbclose ();
               }
               int dbreadfirst (void);
               int delete_angpposi (void); 
               long GetMaxNr ();
		       void dbclose ();
};
#endif
