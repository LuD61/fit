// PartyWaDoc.cpp : Implementierung der Klasse CPartyWaDoc
//

#include "stdafx.h"
#include "PartyWa.h"

#include "PartyWaDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPartyWaDoc

IMPLEMENT_DYNCREATE(CPartyWaDoc, CDocument)

BEGIN_MESSAGE_MAP(CPartyWaDoc, CDocument)
END_MESSAGE_MAP()


// CPartyWaDoc-Erstellung/Zerst�rung

CPartyWaDoc::CPartyWaDoc()
{
	// TODO: Hier Code f�r One-Time-Konstruktion einf�gen

}

CPartyWaDoc::~CPartyWaDoc()
{
}

BOOL CPartyWaDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: Hier Code zur Reinitialisierung einf�gen
	// (SDI-Dokumente verwenden dieses Dokument)

	return TRUE;
}




// CPartyWaDoc-Serialisierung

void CPartyWaDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: Hier Code zum Speichern einf�gen
	}
	else
	{
		// TODO: Hier Code zum Laden einf�gen
	}
}


// CPartyWaDoc-Diagnose

#ifdef _DEBUG
void CPartyWaDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPartyWaDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CPartyWaDoc-Befehle

