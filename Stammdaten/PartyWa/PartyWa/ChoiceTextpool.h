#ifndef _CHOICE_TEXTPOOL_DEF
#define _CHOICE_TEXTPOOL_DEF

#include "ChoiceX.h"
#include "TextpoolList.h"
#include "Textpool.h"
#include <vector>

class CChoiceTextpool : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
		TEXTPOOL_CLASS Textpool;
     
    public :
		enum TEXT_TYPE
		{
			All,
			CommetOffer,
			CommentAccount,
			CommentIntern,
			Comment,
		};
		short m_Type;
	    std::vector<CTextpoolList *> TextpoolList;
      	CChoiceTextpool(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceTextpool(); 
        virtual void FillList (void);


        void SearchItem (CListCtrl *ListBox, LPTSTR SearchText);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CTextpoolList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR,LPTSTR );
        int GetPtBez (LPTSTR, int, LPTSTR);
		void DestroyList ();
		virtual void OnEnter ();
	    virtual void OnFilter ();
	    afx_msg void OnMove (int x, int y);

	DECLARE_MESSAGE_MAP()
};
#endif
