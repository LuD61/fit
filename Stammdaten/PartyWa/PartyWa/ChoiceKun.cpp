#include "stdafx.h"
#include "ChoiceKun.h"
#include "DbUniCode.h"
#include "DynDialog.h"
#include "DbQuery.h"
#include "Process.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceKun::Sort1 = -1;
int CChoiceKun::Sort2 = -1;
int CChoiceKun::Sort3 = -1;
int CChoiceKun::Sort4 = -1;

CChoiceKun::CChoiceKun(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_Mdn = 0;
//	HideEnter = FALSE;
//	HideFilter = FALSE;
}

CChoiceKun::~CChoiceKun() 
{
	DestroyList ();
}

void CChoiceKun::DestroyList() 
{
	for (std::vector<CKunList *>::iterator pabl = KunList.begin (); pabl != KunList.end (); ++pabl)
	{
		CKunList *abl = *pabl;
		delete abl;
	}
    KunList.clear ();
}

void CChoiceKun::FillList () 
{
    short  mdn;
	long kun;
    TCHAR adr_krz [34];
    TCHAR kun_bran2 [10];
	TCHAR adr_nam1 [37];
	TCHAR adr_nam2 [37];
	TCHAR plz [9];
	TCHAR ort1 [37];
	TCHAR str [37];
	TCHAR tel [21];
	TCHAR fax [21];
	TCHAR email [37];
	short staat;
	short land;
	short kun_typ;
	char ptstaat [37];
	char ptland [37];
	char ptkun_typ [37];
	int cursor;

	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Kunden"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
//    SetCol (_T("Mandant"),        1, 50, LVCFMT_RIGHT);
    SetCol (_T("Kunde"),          1, 80, LVCFMT_RIGHT);
    SetCol (_T("Kurzname"),  2, 250);
    SetCol (_T("Branche"),  3, 100);
    SetCol (_T("Name1"),  4, 250);
    SetCol (_T("Name2"),  5, 250);
    SetCol (_T("PLZ"),  6, 100);
    SetCol (_T("Ort"),  7, 250);
    SetCol (_T("Strasse"),  8, 250);
    SetCol (_T("Telefon"),  9, 250);
    SetCol (_T("Fax"),  10, 250);
    SetCol (_T("Email"),  11, 250);
    SetCol (_T("Staat"),  12, 250);
    SetCol (_T("Land"),  13, 250);
    SetCol (_T("Kundentyp"),  14, 100);

	if (FirstRead && !HideFilter)
	{
		FirstRead = FALSE;
		Load ();
//		if (!IsModal)
		{
			PostMessage (WM_COMMAND, IDC_FILTER, 0l);
		}
		return;
	}

	if (KunList.size () == 0)
	{
		CString Sql = _T("");
		if (m_Mdn != 0)
		{
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((long *)&kun,      SQLLONG, 0);
			DbClass->sqlout ((LPTSTR)  adr_krz, SQLCHAR, sizeof (adr_krz));
			DbClass->sqlin ((LPTSTR)  &m_Mdn,   SQLSHORT, 0);
			DbClass->sqlout ((LPTSTR)  kun_bran2, SQLCHAR, sizeof (kun_bran2));
			DbClass->sqlout ((LPTSTR)  adr_nam1, SQLCHAR, sizeof (adr_nam1));
			DbClass->sqlout ((LPTSTR)  adr_nam2, SQLCHAR, sizeof (adr_nam2));
			DbClass->sqlout ((LPTSTR)  plz, SQLCHAR, sizeof (plz));
			DbClass->sqlout ((LPTSTR)  ort1, SQLCHAR, sizeof (ort1));
			DbClass->sqlout ((LPTSTR)  str, SQLCHAR, sizeof (str));
			DbClass->sqlout ((LPTSTR)  tel, SQLCHAR, sizeof (tel));
			DbClass->sqlout ((LPTSTR)  fax, SQLCHAR, sizeof (fax));
			DbClass->sqlout ((LPTSTR)  email, SQLCHAR, sizeof (email));
			DbClass->sqlout ((LPTSTR)  &staat, SQLSHORT, 0);
			DbClass->sqlout ((LPTSTR)  &land, SQLSHORT, 0);
			DbClass->sqlout ((LPTSTR)  &kun_typ, SQLSHORT, 0);
			Sql = _T("select kun.mdn, kun.kun, adr.adr_krz, kun.kun_bran2,")
				  _T("adr.adr_nam1, adr.adr_nam2, adr.plz, adr.ort1, adr.str,")
				  _T("adr.tel, adr.fax, adr.email, adr.staat, adr.land, ")
				  _T("kun.kun_typ ")
				  _T("from kun, adr where kun.kun > 0 ")
				  _T("and adr.adr = kun.adr1 ")
				  _T("and kun.mdn = ?");
		}
		else
		{
			DbClass->sqlout ((short *)&mdn,     SQLSHORT, 0);
			DbClass->sqlout ((long *)&kun,      SQLLONG, 0);
			DbClass->sqlout ((LPTSTR)  adr_krz, SQLCHAR, sizeof (adr_krz));
			DbClass->sqlout ((LPTSTR)  kun_bran2, SQLCHAR, sizeof (kun_bran2));
			DbClass->sqlout ((LPTSTR)  adr_nam1, SQLCHAR, sizeof (adr_nam1));
			DbClass->sqlout ((LPTSTR)  adr_nam2, SQLCHAR, sizeof (adr_nam2));
			DbClass->sqlout ((LPTSTR)  plz, SQLCHAR, sizeof (plz));
			DbClass->sqlout ((LPTSTR)  ort1, SQLCHAR, sizeof (ort1));
			DbClass->sqlout ((LPTSTR)  str, SQLCHAR, sizeof (str));
			DbClass->sqlout ((LPTSTR)  tel, SQLCHAR, sizeof (tel));
			DbClass->sqlout ((LPTSTR)  fax, SQLCHAR, sizeof (fax));
			DbClass->sqlout ((LPTSTR)  email, SQLCHAR, sizeof (email));
			DbClass->sqlout ((LPTSTR)  &staat, SQLSHORT, 0);
			DbClass->sqlout (
				(LPTSTR)  &land, SQLSHORT, 0);
			DbClass->sqlout ((LPTSTR)  &kun_typ, SQLSHORT, 0);
			Sql = _T("select kun.mdn, kun.kun, adr.adr_krz, kun.kun_bran2,")
				  _T("adr.adr_nam1, adr.adr_nam2, adr.plz, adr.ort1, adr.str,")
				  _T("adr.tel, adr.fax, adr.email, adr.staat, adr.land, ")
				  _T("kun.kun_typ ")
				  _T("from kun, adr where kun.kun > 0 ")
				  _T("and adr.adr = kun.adr1");
		}
		if (QueryString != _T(""))
		{
			Sql += _T(" and ");
			Sql += QueryString;
		}

		cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		if (cursor == -1)
		{
			AfxMessageBox (_T("Die Eingabe f�r den Filter kann nicht bearbeitet werden"), MB_OK | MB_ICONERROR, 0);
			return;
		}

		DbClass->sqlopen (cursor);
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) adr_krz;
			CDbUniCode::DbToUniCode (adr_krz, pos);
			CKunList *abl = new CKunList (mdn, kun, adr_krz, kun_bran2);
			abl->adr_nam1 = adr_nam1;
			abl->adr_nam2 = adr_nam2;
			abl->adr_nam2 = adr_nam2;
			abl->plz = plz;
			abl->ort1 = ort1;
			abl->str = str;
			abl->tel = tel;
			abl->fax = fax;
			abl->email = email;
			abl->staat = staat;
			abl->land = land;
			abl->kun_typ = kun_typ;
			KunList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CKunList *>::iterator pabl = KunList.begin (); pabl != KunList.end (); ++pabl)
	{
		CKunList *abl = *pabl;
		CString Mdn;
		Mdn.Format (_T("%hd"), abl->mdn); 
		CString Kun;
		Kun.Format (_T("%ld"), abl->kun); 
		CString Num;
		CString Bez;
		_tcscpy (adr_krz, abl->adr_krz.GetBuffer ());
		_tcscpy (kun_bran2, abl->kun_bran2.GetBuffer ());
		_tcscpy (adr_nam1, abl->adr_nam1.GetBuffer ());
		_tcscpy (adr_nam2, abl->adr_nam2.GetBuffer ());
		_tcscpy (plz, abl->plz.GetBuffer ());
		_tcscpy (ort1, abl->ort1.GetBuffer ());
		_tcscpy (str, abl->str.GetBuffer ());
		_tcscpy (tel, abl->tel.GetBuffer ());
		_tcscpy (fax, abl->fax.GetBuffer ());
		_tcscpy (email, abl->email.GetBuffer ());
		GetPtBez (_T("staat"), staat, ptstaat);
		GetPtBez (_T("land"), land, ptland);
		GetPtBez (_T("kun_typ"), kun_typ, ptkun_typ);

		CString LText;
		LText.Format (_T("%ld %s"), abl->kun, 
									abl->adr_krz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
//                Num = Mdn;
                Num = Kun;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
//        ret = SetItemText (Num.GetBuffer (), i, 1);
		int pos = 0; 
        ret = SetItemText (Kun.GetBuffer (), i, ++pos);
        ret = SetItemText (adr_krz, i, ++pos);
        ret = SetItemText (kun_bran2, i, ++pos);
        ret = SetItemText (adr_nam1, i, ++pos);
        ret = SetItemText (adr_nam2, i, ++pos);
        ret = SetItemText (plz, i, ++pos);
        ret = SetItemText (ort1, i, ++pos);
        ret = SetItemText (str, i, ++pos);
        ret = SetItemText (tel, i, ++pos);
        ret = SetItemText (fax, i, ++pos);
        ret = SetItemText (email, i, ++pos);
        ret = SetItemText (ptstaat, i, ++pos);
        ret = SetItemText (ptland, i, ++pos);
        ret = SetItemText (ptkun_typ, i, ++pos);
        i ++;
    }

	SortRow = 2;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort (listView);
}


void CChoiceKun::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceKun::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceKun::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceKun::SearchKun (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceKun::SearchAdrKrz (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();
		CString cSearch = Search;
		cSearch.MakeUpper ();

        if (_tmemcmp (cSearch.GetBuffer (), iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceKun::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
/*
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer (8));
             break;
*/
        case 1 :
             SearchKun (ListBox, EditText.GetBuffer (8));
             break;
        case 2 :
             EditText.MakeUpper ();
             SearchAdrKrz (ListBox, EditText.GetBuffer ());
             break;
    }
}

int CChoiceKun::GetPtBez (LPTSTR item, LPTSTR wert, LPTSTR bez)
{
	TCHAR ptitem [9];
	TCHAR ptwert [5];
	TCHAR ptbez [37];
	static int cursor = -1;

	_tcscpy (ptbez, _T(""));
	if (cursor == -1)
	{
		DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
		DbClass->sqlin ((LPTSTR) ptitem, SQLCHAR, 9);
		DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
		cursor = DbClass->sqlcursor (_T("select ptbez from ptabn where ptitem = ? ")
								_T("and ptwert = ?"));
	}
	strcpy (ptitem, item);
	strcpy (ptwert, wert);
	DbClass->sqlopen (cursor);
	int dsqlstatus = DbClass->sqlfetch (cursor);
	strcpy (bez, ptbez);
    return dsqlstatus;
}

int CChoiceKun::GetPtBez (LPTSTR item, int wert, LPTSTR bez)
{
	TCHAR ptitem [9];
	TCHAR ptwert [5];
	TCHAR ptbez [37];
	static int cursor = -1;

	_tcscpy (ptbez, _T(""));
	if (cursor == -1)
	{
		DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
		DbClass->sqlin ((LPTSTR) ptitem, SQLCHAR, 9);
		DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
		cursor = DbClass->sqlcursor (_T("select ptbez from ptabn where ptitem = ? ")
								_T("and ptwert = ?"));
	}
	strcpy (ptitem, item);
	sprintf (ptwert,"%d", wert);
	DbClass->sqlopen (cursor);
	int dsqlstatus = DbClass->sqlfetch (cursor);
	strcpy (bez, ptbez);
    return dsqlstatus;
}

int CALLBACK CChoiceKun::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
/*
   else if (SortRow == 1)
   {

	   short li1 = _tstoi (strItem1.GetBuffer ());
	   short li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
*/
   else if (SortRow == 1)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
    CString cItem1 = strItem1;
    CString cItem2 = strItem2;
	cItem1.MakeUpper ();
	cItem2.MakeUpper ();
	return (cItem2.CompareNoCase (cItem1)) * Sort3;
   }
   return 0;
}


void CChoiceKun::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CKunList *abl = KunList [i];
		   
//		   abl->mdn     = _tstoi (ListBox->GetItemText (i, 1));
		   abl->kun     = _tstol (ListBox->GetItemText (i, 1));
		   abl->adr_krz = ListBox->GetItemText (i, 2);
	}
	for (int i = 1; i <= 2; i ++)
	{
		SetItemSort (i, 2);
	}
	SetItemSort (SortRow, SortPos);
}

void CChoiceKun::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = KunList [idx];
}

CKunList *CChoiceKun::GetSelectedText ()
{
	CKunList *abl = (CKunList *) SelectedRow;
	return abl;
}


void CChoiceKun::OnEnter ()
{
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cA = m_List.GetItemText (idx, 1);  
		long kun = _tstol (cA.GetBuffer ());
		Message.Format (_T("KundenNr=%0lf"), kun);
		ToClipboard (Message);
	}
	p.SetCommand (_T("16500"));
	HANDLE Pid = p.Start ();
}

void CChoiceKun::OnFilter ()
{
	CDynDialog dlg;
	CDynDialogItem *Item;
	static int headercy = 200;

	dlg.DlgCaption = "Filter";
	dlg.UseFilter = UseFilter;

	if (vFilter.size () > 0)
	{
		for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
		{
			CDynDialogItem *i = *it;
			Item = new CDynDialogItem ();
			*Item = *i;
			if (Item->Name == _T("reset"))
			{
				if (UseFilter)
				{
					Item->Value = _T("aktiv");
				}
				else
				{
					Item->Value = _T("nicht aktiv");
				}
			}
			dlg.AddItem (Item);

		}
		dlg.Header.cy = headercy;	
	}
	else
	{
		int y = 10;
		Item = new CDynDialogItem ();
		Item->Name = _T("lkun");
		Item->CtrClass = "static";
		Item->Value = _T("Kunde");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1001;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("kun");
		Item->Type = Item->Decimal;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1002;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("ladr_krz");
		Item->CtrClass = "static";
		Item->Value = _T("Kurzname 1");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1003;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("adr.adr_krz");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1004;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lkun_bran2");
		Item->CtrClass = "static";
		Item->Value = _T("Branche");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1005;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("kun_bran2");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1006;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("ladr_nam1");
		Item->CtrClass = "static";
		Item->Value = _T("Name");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1007;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("adr_nam1");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1008;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lplz");
		Item->CtrClass = "static";
		Item->Value = _T("PLZ");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1009;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("plz");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1010;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lort1");
		Item->CtrClass = "static";
		Item->Value = _T("Ort");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1011;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("ort1");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1012;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lstr");
		Item->CtrClass = "static";
		Item->Value = _T("Strasse");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1013;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("str");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1014;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("ltel");
		Item->CtrClass = "static";
		Item->Value = _T("Telefon");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1015;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("tel");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1016;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lstaat");
		Item->CtrClass = "static";
		Item->Value = _T("Staat");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1017;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("staat");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1018;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lland");
		Item->CtrClass = "static";
		Item->Value = _T("Land");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1019;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("land");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1020;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lkun_typ");
		Item->CtrClass = "static";
		Item->Value = _T("Kundentyp");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1021;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("kun_typ");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1022;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		dlg.Header.cy = y + 30;
		headercy = dlg.Header.cy;

		Item = new CDynDialogItem ();
		Item->Name = _T("OK");
		Item->CtrClass = "button";
		Item->Value = _T("OK");
		Item->Template.x = 15;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDOK;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("cancel");
		Item->CtrClass = "button";
		Item->Value = _T("abbrechen");
		Item->Template.x = 70;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDCANCEL;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("reset");
		Item->CtrClass = "button";
		if (UseFilter)
		{
			Item->Value = _T("aktiv");
		}
		else
		{
			Item->Value = _T("nicht aktiv");
		}
		Item->Template.x = 125;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = ID_USE;
		dlg.AddItem (Item);
	}

	dlg.CreateModal ();
	INT_PTR ret = dlg.DoModal ();
	if (ret != IDOK) return;
 
	UseFilter = dlg.UseFilter;
	int count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = dlg.Items.begin (); it != dlg.Items.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (count >= (int) vFilter.size ())
			{
				Item = new CDynDialogItem ();
				*Item = *i;
				vFilter.push_back (Item);
			}
			*vFilter[count] = *i;
			count ++;
	}
    CString Query;
    CDbQuery DbQuery;

	QueryString = _T("");
	if (!UseFilter)
	{
		FillList ();
		return;
	}
	count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (strcmp (i->CtrClass, "edit") != 0) continue; 
			if (i->Value.Trim () != _T(""))
			{
				if (i->Type == i->String || i->Type == i->Date)
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, TRUE);
				}
				else
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, FALSE);
				}
				if (count > 0)
				{
					QueryString += _T(" and ");
				}
                QueryString += Query;
				count ++;
			}
	}
	FillList ();
}

void CChoiceKun::SendSelect ()
{
	pParent->SendMessage (WM_COMMAND, KUN_SELECTED, 0l);
}

void CChoiceKun::SendCancel ()
{
	pParent->SendMessage (WM_COMMAND, KUN_CANCELED, 0l);
}

