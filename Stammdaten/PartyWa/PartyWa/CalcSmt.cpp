#include "StdAfx.h"
#include "CalcSmt.h"
#include "DataTables.h"
#include "AngebotCore.h"
#include "StrFuncs.h"
//#include "Ptabn.h"

#define PTABNC CDatatables::GetInstance ()->GetPtabn ()
#define PTABNT CDatatables::GetInstance ()->GetPtabn ()->ptabn
#define ABASLIST CAngebotCore::GetInstance ()->GetABasList ()

CCalcSmt::CCalcSmt(void)
{
	smt = 0;
	Partyk = CDatatables::GetInstance ()->GetPartyk ();
	A_bas = CDatatables::GetInstance ()->GetABas ();
}

CCalcSmt::CCalcSmt(short smt)
{
	this->smt = smt;
	SmtBez = _T("");
	Partyk = CDatatables::GetInstance ()->GetPartyk ();
	A_bas = CDatatables::GetInstance ()->GetABas ();
	List = CAngebotCore::GetInstance ()->GetList ();
	strcpy ((LPSTR) PTABNT.ptitem, "teil_smt");
	sprintf ((LPSTR) PTABNT.ptwert, "%hd", smt);
    if (PTABNC->dbreadfirst () == 0)
	{
		SmtBez = PTABNT.ptbez;
		SmtBez.TrimRight ();
	}
}

void CCalcSmt::SetSmt(short smt)
{
	this->smt = smt;
	SmtBez = _T("");
	Partyk = CDatatables::GetInstance ()->GetPartyk ();
	A_bas = CDatatables::GetInstance ()->GetABas ();
	List = CAngebotCore::GetInstance ()->GetList ();
	strcpy ((LPSTR) PTABNT.ptitem, "teil_smt");
	sprintf ((LPSTR) PTABNT.ptwert, "%hd", smt);
    if (PTABNC->dbreadfirst () == 0)
	{
		SmtBez = PTABNT.ptbez;
	}
}

CCalcSmt::~CCalcSmt(void)
{
}

void CCalcSmt::Calculate ()
{
	PARTYP *p;
	PARTYP **it;
	A_BAS *a_bas;
	A_BAS_CLASS *A_bas = new A_BAS_CLASS ();

	if (smt == 0)
	{
		return;
	}
	AngGew = 0.0;
	AngVk = 0.0;
	OSGPrice = 0.0;
	OSSPrice = 0.0;

	List = CAngebotCore::GetInstance ()->GetList ();
	m_VatFull = CAngebotCore::GetInstance ()->GetVatFull ();
	m_VatReduced = CAngebotCore::GetInstance ()->GetVatReduced ();
    
	if (Partyk->partyk.nur_servicepreis)
	{
		List->Start ();
		while ((it = List->GetNext ()) != NULL)
		{
			p = *it;
			if (p->teil_smt == smt)
			{
				if (p != NULL && p->a != 0.0)
				{
					if (p->inh == 0.0)
					{
						p->inh = 1.0;
					}
					if (p->me_einh == 2)
					{
						p->lief_me = p->auf_me * p->inh;
						p->pos_gew = p->lief_me;
					}
					else if (p->me_einh != 2 && p->a_gew != 0.0)
					{
						p->lief_me = p->auf_me * p->inh;
						p->pos_gew = p->lief_me * p->a_gew;
					}
					else
					{
						p->lief_me = p->auf_me * p->inh;
					}
					AngGew += p->pos_gew;
					p->pos_vk = p->auf_vk_brutto * p->lief_me;
					p->pos_vk_nto = CStrFuncs::RoundToDouble (p->auf_vk_euro * m_VatReduced, 2);
					p->pos_vk_nto *= p->lief_me;
					AngVk += p->pos_vk;
					a_bas = ABASLIST->Find (p->a);
					if (a_bas == NULL)
					{
						A_bas->a_bas.a = p->a;
						A_bas->dbreadfirst ();
						ABASLIST->Add (&A_bas->a_bas);
						a_bas = ABASLIST->Find (p->a);
					}
					if (CAngebotCore::GetInstance ()->IsVatFull)
					{
						if (a_bas->mwst == CAngebotCore::VatReduced)
						{
							OSGPrice += p->pos_vk_nto;
						}
					}
					else
					{
							OSGPrice += p->pos_vk;
					}
				}
			}
		}
	}
	else
	{
		List->Start ();
		while ((it = List->GetNext ()) != NULL)
		{
			p = *it;
			if (p->teil_smt == smt)
			{
				if (p != NULL && p->a != 0.0)
				{
					if (p->inh == 0.0)
					{
						p->inh = 1.0;
					}
					if (p->me_einh == 2)
					{
						p->lief_me = p->auf_me * p->inh;
						p->pos_gew = p->lief_me;
					}
					else if (p->me_einh != 2 && p->a_gew != 0.0)
					{
						p->lief_me = p->auf_me * p->inh;
						p->pos_gew = p->lief_me * p->a_gew;
					}
					else
					{
						p->lief_me = p->auf_me * p->inh;
					}
					AngGew += p->pos_gew;
					p->pos_vk = p->auf_vk_euro * p->lief_me;
					p->pos_vk_nto = CStrFuncs::RoundToDouble (p->auf_vk_euro, 2);
					p->pos_vk_nto *= p->lief_me;
					AngVk += p->pos_vk;
					a_bas = ABASLIST->Find (p->a);
					if (a_bas == NULL)
					{
						A_bas->a_bas.a = p->a;
						A_bas->dbreadfirst ();
						ABASLIST->Add (&A_bas->a_bas);
						a_bas = ABASLIST->Find (p->a);
					}
					if (a_bas->mwst == CAngebotCore::VatReduced)
					{
							OSGPrice += p->pos_vk;
					}
				}
			}
		}
	}
	if (Partyk->partyk.anz_pers > 0.0)
	{
		AngSingleVk = AngVk / Partyk->partyk.anz_pers;
		OSSPrice = OSGPrice / Partyk->partyk.anz_pers;  

	}
	else
	{
			AngSingleVk = 0.0;
			OSSPrice = 0.0;
	}
	delete A_bas;
}
