#pragma once
#include "datacollection.h"
#include "Pers.h"

class CPersCollection :
	public CDataCollection<PERS *>
{
public:
	CPersCollection(void);
public:
	~CPersCollection(void);
	int FindPosition (LPSTR pers);
	PERS *GetElementAt (int idx);
	PERS *Find (LPSTR pers);
	void Add (PERS *pers);
	void Drop (LPSTR pers);
};
