#ifndef _LSP_DEF
#define _LSP_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct LSP {
   short          mdn;
   short          fil;
   long           ls;
   double         a;
   double         auf_me;
   char           auf_me_bz[7];
   double         lief_me;
   char           lief_me_bz[7];
   double         ls_vk_pr;
   double         ls_lad_pr;
   short          delstatus;
   double         tara;
   long           posi;
   long           lsp_txt;
   short          pos_stat;
   short          sa_kz_sint;
   char           erf_kz[2];
   double         prov_satz;
   short          leer_pos;
   DATE_STRUCT    hbk_date;
   char           ls_charge[31];
   double         auf_me_vgl;
   double         ls_vk_euro;
   double         ls_vk_fremd;
   double         ls_lad_euro;
   double         ls_lad_fremd;
   double         rab_satz;
   short          me_einh_kun;
   short          me_einh;
   short          me_einh_kun1;
   double         auf_me1;
   double         inh1;
   short          me_einh_kun2;
   double         auf_me2;
   double         inh2;
   short          me_einh_kun3;
   double         auf_me3;
   double         inh3;
   char           lief_me_bz_ist[7];
   double         inh_ist;
   short          me_einh_ist;
   double         me_ist;
   short          lager;
   double         lief_me1;
   double         lief_me2;
   double         lief_me3;
   short          ls_pos_kz;
   double         a_grund;
   char           kond_art[5];
   long           posi_ext;
   long           kun;
   short          na_lief_kz;
   long           nve_posi;
   short          teil_smt;
   short          aufschlag;
   double         aufschlag_wert;
   char           ls_ident[21];
   short          pos_txt_kz;
};
extern struct LSP lsp, lsp_null;

#line 8 "lsp.rh"

class LSP_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LSP lsp;  
               LSP_CLASS () : DB_CLASS ()
               {
               }
};
#endif
