// PartyWaView.cpp : Implementierung der Klasse CPartyWaView
//

#include "stdafx.h"
#include "PartyWa.h"

#include "PartyWaDoc.h"
#include "PartyWaView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPartyWaView

IMPLEMENT_DYNCREATE(CPartyWaView, CFormView)

BEGIN_MESSAGE_MAP(CPartyWaView, CFormView)
END_MESSAGE_MAP()

// CPartyWaView-Erstellung/Zerst�rung

CPartyWaView::CPartyWaView()
	: CFormView(CPartyWaView::IDD)
{
	// TODO: Hier Code zur Konstruktion einf�gen

}

CPartyWaView::~CPartyWaView()
{
}

void CPartyWaView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL CPartyWaView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return CFormView::PreCreateWindow(cs);
}

void CPartyWaView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

}


// CPartyWaView-Diagnose

#ifdef _DEBUG
void CPartyWaView::AssertValid() const
{
	CFormView::AssertValid();
}

void CPartyWaView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CPartyWaDoc* CPartyWaView::GetDocument() const // Nicht-Debugversion ist inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPartyWaDoc)));
	return (CPartyWaDoc*)m_pDocument;
}
#endif //_DEBUG


// CPartyWaView-Meldungshandler
