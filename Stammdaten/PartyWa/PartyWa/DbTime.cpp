/*
 * Copyright (c) 2004 Wilhelm Roth, 
 * Company  SETEC-GMBH Inc. All  Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * -Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduct the above copyright
 *  notice, this list of conditions and the following disclaimer in
 *  the documentation and/or other materials provided with the distribution.
 * 
 * Neither the name of SETEC-GMBH, Inc. or the names of contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING
 * ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND ITS LICENSORS SHALL NOT
 * BE LIABLE FOR ANY DAMAGES OR LIABILITIES SUFFERED BY LICENSEE AS A RESULT
 * OF OR RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THE SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST
 * REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL,
 * INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY
 * OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE SOFTWARE, EVEN
 * IF SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that Software is not designed, licensed or intended for
 * use in the design, construction, operation or maintenance of any nuclear
 * facility.
 */
#include "stdafx.h"
#include "DbTime.h"
#include "DbClass.h"


DayTime::DayTime (char *t)
{
	     Create (t);
}

DayTime::DayTime (CString *t)
{
	     Create (t);
}

DayTime *DayTime::Create (char *t)
{
	     CString txt (t);
	     Create (&txt);
		 return this;
}

DayTime *DayTime::Create (CString *t)
{
	     char hour[] = {"00"};
	     char min[]  = {"00"};
	     char sec[]  = {"00"};
         Time = *t;
		 int len = t->GetLength ();
		 if (len >=2)
		 {
			 memcpy (hour, Time.Mid (0, 2), 2);
		 }
		 if (len >=5)
		 {
			 memcpy (min, Time.Mid (3, 2), 2);
		 }
		 if (len >=8)
		 {
			 memcpy (sec, Time.Mid (6, 2), 2);
		 }
/*
		 lTime = atoi (hour) * 24 * 3600 + 
			     atoi (min) * 3600 +
				 atoi (sec);
*/
		 lTime = atoi (hour) * 3600 + 
			     atoi (min)  * 60 +
				 atoi (sec);
		 return this;
}

long DayTime::GetLongTime ()
{
	     return lTime;
}

CString *DayTime::GetTextTime ()
{
	     return &Time;
}

DbTime::DbTime ()
{
		 time_t timer;
		 struct tm *ltime;

		 time (&timer);
		 ltime = localtime (&timer);
		 ds.year = ltime->tm_year + 1900;
		 ds.month = ltime->tm_mon + 1;
 		 ds.day   = ltime->tm_mday;
		 st.wYear  = ds.year;
		 st.wMonth = ds.month;
		 st.wDay   = ds.day;
		 st.wHour  = 0;
		 st.wMinute = 0;
		 st.wSecond = 0;
		 st.wMilliseconds = 0;
		 tDate.Format ("%02hd.%02hd.%02hd", st.wDay, st.wMonth, st.wYear);
		 SystemTimeToFileTime (&st, &ft);
		 FileTimeToSystemTime (&ft, &st);
		 memcpy (&ul, &ft, sizeof (ft));
		 ldat = (time_t) (ul.QuadPart / (1000 * 3600 * 24));
		 ldat /= 10000;
}


DbTime::DbTime (char *date)
{
		 DB_CLASS::ToDbDate (date, &ds);
	     if (IsNull ())
		 {
/*
			 ldat = 0l;
			 memset (&st, 0, sizeof (SYSTEMTIME));
			 memset (&ft, 0, sizeof (FILETIME));
			 memset (&ul, 0, sizeof (ULARGE_INTEGER));
*/

			 ds.year = 1900;
			 ds.month = 1;
			 ds.day = 1;
		 }
//		 else
		 {
			 st.wYear  = ds.year;
			 st.wMonth = ds.month;
			 st.wDay   = ds.day;
			 st.wHour  = 0;
			 st.wMinute = 0;
			 st.wSecond = 0;
			 st.wMilliseconds = 0;
			 tDate.Format ("%02hd.%02hd.%02hd", st.wDay, st.wMonth, st.wYear);
			 SystemTimeToFileTime (&st, &ft);
			 FileTimeToSystemTime (&ft, &st);
			 memcpy (&ul, &ft, sizeof (ft));
			 ldat = (time_t) (ul.QuadPart / (1000 * 3600 * 24));
			 ldat /= 10000;
		 }
}


DbTime::DbTime (DATE_STRUCT *ds)
{
	     memcpy (&this->ds, ds, sizeof (DATE_STRUCT));  
		 if (IsNull ())
		 {

			 ldat = 0l;
			 memset (&st, 0, sizeof (SYSTEMTIME));
			 memset (&ft, 0, sizeof (FILETIME));
			 memset (&ul, 0, sizeof (ULARGE_INTEGER));

/*
			 ds->year = 1900;
			 ds->month = 1;
			 ds->day = 1;
			 memcpy (&this->ds, ds, sizeof (DATE_STRUCT));  
*/
		 }
		 else
		 {
			 st.wYear  = ds->year;
			 st.wMonth = ds->month;
			 st.wDay   = ds->day;
			 st.wHour  = 0;
			 st.wMinute = 0;
			 st.wSecond = 0;
			 st.wMilliseconds = 0;
			 tDate.Format ("%02hd.%02hd.%02hd", st.wDay, st.wMonth, st.wYear);
			 SystemTimeToFileTime (&st, &ft);
			 FileTimeToSystemTime (&ft, &st);
			 memcpy (&ul, &ft, sizeof (ft));
			 ldat = (time_t) (ul.QuadPart / (1000 * 3600 * 24));
			 ldat /= 10000;
		 }
}

DbTime::DbTime (long ldate)
{
	SetTime ((time_t) ldate);
}

BOOL DbTime::IsNull ()
{
	if (ds.year < 1900 || ds.month < 1 || ds.day < 1)
	{
		return TRUE;
	}
	return FALSE;
}

time_t DbTime::GetTime ()
{
	     return ldat;
}

int DbTime::GetWeekDay ()
{
	     return st.wDayOfWeek;
}

CString* DbTime::GetStringTime ()
{
	     return &tDate;
}

void DbTime::SetDateStruct ()
{
	     ds.year  = st.wYear;
	     ds.month = st.wMonth;
	     ds.day   = st.wDay;
}

DATE_STRUCT *DbTime::GetDateStruct (DATE_STRUCT *d)
{
	     memcpy (d, &ds, sizeof (ds));
         return d;
}

void DbTime::SetTime (time_t t)
{
	     ldat = t;
		 ldat *= 10000;
		 ul.QuadPart = ldat;
		 ul.QuadPart *= (1000 * 3600 * 24);
         memcpy (&ft, &ul,  sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
}

#ifdef WIN32
DbTime::operator time_t () const
{
		 return ldat;
}

DbTime::operator DATE_STRUCT () const
{
		 return ds;
}

BOOL DbTime::operator== (long time)
{
		 return time == ldat;
}

DbTime& DbTime::operator+ (int d)
{
	     time_t t1 = GetTime ();
         t1 += d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return *this;
}

DbTime& DbTime::operator- (int d)
{
	     time_t t1 = GetTime ();
         t1 -= d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return *this;
}
#endif

DbTime* DbTime::Add (int d)
{
	     time_t t1 = GetTime ();
         t1 += d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return this;
}

DbTime* DbTime::Sub (int d)
{
	     time_t t1 = GetTime ();
         t1 -= d;
		 SetTime (t1);
         memcpy (&ft, &ul, sizeof (ft));
	     FileTimeToSystemTime (&ft, &st);
		 tDate.Format (_T("%02hd.%02hd.%02hd"), st.wDay, st.wMonth, st.wYear);
		 SetDateStruct ();
         return this;
}

long DbTime::ParseLong (DATE_STRUCT *date)
{
	DbTime t (date);
	return (long) t.GetTime ();
}

void DbTime::ToDateStruct (DATE_STRUCT *date, long ldate)
{
	DbTime t (ldate);
	t.GetDateStruct (date);
}

void DbTime::TestDateValue (DATE_STRUCT *date)
{
	long ldat;

	DbTime t (date);
	ldat = (long) t.GetTime ();
	if (ldat <= 0)
	{
		date->year = 1900;
		date->month = 1;
		date->day = 1;
	}
}

