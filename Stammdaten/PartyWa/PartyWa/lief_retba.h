#ifndef _LIEF_RETBA_DEF
#define _LIEF_RETBA_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct LIEF_RETBA {
   short          delstatus;
   long           nr;
   TCHAR          blg_typ[2];
   short          mdn;
   short          fil;
   long           kun;
   short          kun_fil;
   DATE_STRUCT    dat;
   TCHAR          pers[13];
   long           vertr;
   TCHAR          err_txt[17];
   short          zutyp;
};
extern struct LIEF_RETBA lief_retba, lief_retba_null;

#line 8 "lief_retba.rh"

class LIEF_RETBA_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               LIEF_RETBA lief_retba;  
               LIEF_RETBA_CLASS () : DB_CLASS ()
               {
               }
};
#endif
