#include "StdAfx.h"
#include "KunDiverse.h"
#include "Datatables.h"

#define DATATABLES CDatatables::GetInstance ()

CKunDiverse *CKunDiverse::Instance = NULL;

CKunDiverse *CKunDiverse::GetInstance ()
{
	if (Instance == NULL)
	{
		Instance = new CKunDiverse ();
	}
	return Instance;
}

void CKunDiverse ::DeleteInstance ()
{
	if (Instance != NULL)
	{
		delete Instance;
		Instance = NULL;
	}
}

CKunDiverse::CKunDiverse(void)
{
	Ptabn = DATATABLES->GetPtabn ();
	Kun = DATATABLES->GetKun ();
	KunAdr = DATATABLES->GetKunAdr ();
    AutoNr = new AUTO_CLASS ();
    adr_von  = 1l;
    adr_bis  = 99999999l;
    sadr_von = 90000000l;
    sadr_bis = 99999999l;
    AdrBerOk = FALSE;
}

CKunDiverse::~CKunDiverse(void)
{
	delete AutoNr;
}

long CKunDiverse::GetAdrFrom ()
{
	FillSadr ();
	return sadr_von;
}

long CKunDiverse::GetAdrTo ()
{
	FillSadr ();
	return sadr_bis;
}

long CKunDiverse::TestAdrOnDiverse (long adr)
{
	long NewAdr = adr;
	FillSadr ();
	if (NewAdr >= sadr_von &&
		NewAdr <= sadr_bis)
	{
		NewAdr = GenAdr ();
		KunAdr->adr.adr = adr;
		if (KunAdr->dbreadfirst () == 0)
		{
			KunAdr->adr.adr = NewAdr;
			KunAdr->dbupdate ();
		}
	}
    return NewAdr;
}

BOOL CKunDiverse::IsDiverse ()
{
	BOOL ret = TRUE;
	FillSadr ();
	if (KunAdr->adr.adr_typ != Diverse)
	{
		ret = FALSE;
	}
	if (ret)
	{
		if (KunAdr->adr.adr >= sadr_von &&
			KunAdr->adr.adr <= sadr_bis)
		{
			ret = FALSE;
		}
	}
    return ret;
}

BOOL CKunDiverse::IsAdrDiverse ()
{
	BOOL ret = TRUE;
	FillSadr ();
	if (KunAdr->adr.adr_typ != Diverse)
	{
		ret = FALSE;
	}
	if (ret)
	{
		if (KunAdr->adr.adr < sadr_von &&
			KunAdr->adr.adr > sadr_bis)
		{
			ret = FALSE;
		}
	}
    return ret;
}

BOOL CKunDiverse::IsAdrDiverse (long adr)
{
	BOOL ret = TRUE;
	FillSadr ();
	if (ret)
	{
		if (adr < sadr_von ||
			adr > sadr_bis)
		{
			ret = FALSE;
		}
	}
    return ret;
}

void CKunDiverse::FillSadr ()
{
	  int dsqlstatus;
	  
	  if (!AdrBerOk)
	  {
		  AdrBerOk = TRUE;
		  memcpy (&Ptabn->ptabn, &ptabn_null, sizeof (PTABN));
		  strcpy (Ptabn->ptabn.ptitem, "adr");
		  strcpy (Ptabn->ptabn.ptwert, "1");
		  dsqlstatus = Ptabn->dbreadfirst ();
		  if (dsqlstatus == 0) 
		  {
			  adr_von = atol (Ptabn->ptabn.ptwer1);
		      adr_bis = atol (Ptabn->ptabn.ptwer2);
		  }
		  memcpy (&Ptabn->ptabn, &ptabn_null, sizeof (PTABN));
		  strcpy (Ptabn->ptabn.ptwert, "2");
		  dsqlstatus = Ptabn->dbreadfirst ();
		  if (dsqlstatus == 0) 
		  {
			sadr_von = atol (ptabn.ptwer2) + 1;
			sadr_bis = adr_bis;
		  }
	  }
}

long CKunDiverse::GenAdr ()
{
	long adr = 0l;

	BOOL ret = FALSE;
	int tries = 0;

	ret = AutoNr->GenAutoNr (0, 0, _T("adr"));
	if (ret)
	{
		adr = AutoNr->Nr;
		while (ret && AdrExist (adr) && (tries < MaxTries))
		{
			tries ++;
			Sleep (50);
			ret = AutoNr->GenAutoNr (0, 0, _T("adr"));
			if (ret)
			{
				adr = AutoNr->Nr;
			}
		}
	}
	return adr;
}

BOOL CKunDiverse::AdrExist (long adr)
{
	BOOL ret = FALSE;
	int dsqlstatus;

	Adr.adr.adr = adr;
	dsqlstatus = Adr.dbreadfirst ();
	if (dsqlstatus == 0)
	{
		ret = TRUE;
	}
	return ret;
}