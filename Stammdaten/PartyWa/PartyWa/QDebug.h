#pragma once

using namespace std;
#include <string>
#include <fstream>

class CQDebug
{
private:
	string msFileName;
	string msLine;
	fstream mfspOut;
	static CQDebug *instance;
public:
    void SetFileName (LPTSTR FileName)
	{
		if (FileName != msFileName)
		{
			close ();
		}
		msFileName = FileName;
	}

    void SetFileName (string FileName)
	{
		if (FileName != msFileName)
		{
			close ();
		}
		msFileName = FileName;
	}

	LPTSTR GetFileNamePtr ()
	{
		return (LPTSTR) msFileName.c_str ();
	}

	string& GetFileName ()
	{
		return msFileName;
	}

    void SetLine (LPTSTR Line)
	{
		msLine = Line;
	}

    void SetLine (string Line)
	{
		msFileName = Line;
	}

	LPTSTR GetLinePtr ()
	{
		return (LPTSTR) msLine.c_str ();
	}

	string& GetLine ()
	{
		return msLine;
	}

protected:
	CQDebug(void);
protected:
	~CQDebug(void);
public:
	static CQDebug *GetInstance ();
	static void DeleteInstance ();

	bool open ();
	bool write ();
	bool write (LPTSTR text);
	bool write (LPTSTR FileName, LPTSTR text);
	void close ();
};
