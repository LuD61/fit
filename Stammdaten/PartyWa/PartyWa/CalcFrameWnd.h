#pragma once
#include "afxwin.h"

class CCalcFrameWnd :
	public CFrameWnd
{
public:
	CCalcFrameWnd(void);
	DECLARE_DYNCREATE(CCalcFrameWnd)
public:
	~CCalcFrameWnd(void);
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg BOOL OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext);
	DECLARE_MESSAGE_MAP()

private:
	BOOL WithTitle;

public:
	void SetWithTitle (BOOL WithTitle)
	{
		this->WithTitle = WithTitle;
	}

	BOOL GetWithTitle ()
	{
		return WithTitle;
	}




};
