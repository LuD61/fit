#include "StdAfx.h"
#include "AbasCollection.h"

CAbasCollection::CAbasCollection(void)
{
}

CAbasCollection::~CAbasCollection(void)
{
}

A_BAS *CAbasCollection::Find (double a)
{
	A_BAS **it;
	A_BAS *a_bas;
	for (int i = 0; i < anz; i ++)
	{
		it = Get (i);
		if (it != NULL)
		{
			a_bas = *it;
			if (a_bas->a == a)
			{
				return a_bas;
			}
		}
	}
	return NULL;
}

void CAbasCollection::Add (A_BAS *a_bas)
{
	A_BAS *a_bas_element;
	a_bas_element = Find (a_bas->a);
    if (a_bas_element == NULL)
	{
		a_bas_element = new A_BAS ();
		CDataCollection<A_BAS *>::Add (a_bas_element);
	}
	memcpy (a_bas_element, a_bas, sizeof (A_BAS));
}

BOOL CAbasCollection::HasService ()
{
	A_BAS **it;
	A_BAS *a_bas;
	for (int i = 0; i < anz; i ++)
	{
		it = Get (i);
		if (it != NULL)
		{
			a_bas = *it;
			if (a_bas->mwst == VatFull)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

void CAbasCollection::Drop (double a)
{
	A_BAS *a_bas = Find (a);
	if (a_bas != NULL)
	{
		CDataCollection <A_BAS *>::Drop (a_bas);
		delete a_bas;
	}
}

