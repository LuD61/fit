// MainFrm.cpp : Implementierung der Klasse CMainFrame
//

#include "stdafx.h"
#include "PartyWa.h"
#include "AngebotCore.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_COMMAND(ID_BACK, &CMainFrame::OnBack)
	ON_COMMAND(ID_SAVE, &CMainFrame::OnFileSave)
	ON_COMMAND(ID_INSERT, &CMainFrame::OnInsert)
	ON_UPDATE_COMMAND_UI(ID_INSERT, &CMainFrame::OnUpdateInsert)
	ON_COMMAND(ID_DELETE, &CMainFrame::OnDelete)
	ON_UPDATE_COMMAND_UI(ID_DELETE, &CMainFrame::OnUpdateDelete)
	ON_COMMAND(ID_EDIT_CUT, &CMainFrame::OnEditCut)
	ON_COMMAND(ID_EDIT_COPY, &CMainFrame::OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, &CMainFrame::OnEditPaste)
	ON_COMMAND(ID_DELETE_ANG, &CMainFrame::OnDeleteAng)
	ON_COMMAND(ID_PRINT_ANG, &CMainFrame::OnPrintAng)
	ON_COMMAND(ID_PRINT_PACK, &CMainFrame::OnPrintPack)
	ON_COMMAND(ID_PRINT_AUFTRAG, &CMainFrame::OnPrintAuftrag)
	ON_UPDATE_COMMAND_UI(ID_DELETE_ANG, &CMainFrame::OnUpdateDeleteAng)
	ON_UPDATE_COMMAND_UI(ID_PRINT_ANG, &CMainFrame::OnUpdatePrintAng)
	ON_UPDATE_COMMAND_UI(ID_PRINT_PACK, &CMainFrame::OnUpdatePrintPack)
	ON_UPDATE_COMMAND_UI(ID_PRINT_AUFTRAG, &CMainFrame::OnUpdatePrintAuftrag)
	ON_COMMAND(ID_CALC_VIEW, &CMainFrame::OnCalcView)
	ON_COMMAND(ID_POS_TEXT, &CMainFrame::OnPosText)
	ON_UPDATE_COMMAND_UI(ID_POS_TEXT, &CMainFrame::OnUpdatePosText)
	ON_COMMAND(ID_PRINT_LIEF, &CMainFrame::OnPrintLief)
	ON_UPDATE_COMMAND_UI(ID_PRINT_LIEF, &CMainFrame::OnUpdatePrintLief)
	ON_COMMAND(ID_PRINT_RECH, &CMainFrame::OnPrintRech)
	ON_UPDATE_COMMAND_UI(ID_PRINT_RECH, &CMainFrame::OnUpdatePrintRech)
	ON_COMMAND(ID_KOPF_TEXT, &CMainFrame::OnKopfText)
	ON_UPDATE_COMMAND_UI(ID_KOPF_TEXT, &CMainFrame::OnUpdateKopfText)
	ON_COMMAND(ID_FUSS_TEXT, &CMainFrame::OnFussText)
	ON_UPDATE_COMMAND_UI(ID_FUSS_TEXT, &CMainFrame::OnUpdateFussText)
	ON_COMMAND_EX ( IDR_MAINFRAME1, &CMainFrame::OnBarCheck )
	ON_COMMAND_EX ( IDR_MAINFRAME2, &CMainFrame::OnBarCheck )
	ON_UPDATE_COMMAND_UI ( IDR_MAINFRAME1, &CMainFrame::OnUpdateToolBar1 )
	ON_UPDATE_COMMAND_UI ( IDR_MAINFRAME2, &CMainFrame::OnUpdateToolBar2 )
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // Statusleistenanzeige
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame-Erstellung/Zerst�rung

CMainFrame::CMainFrame()
{
	// TODO: Hier Code f�r die Memberinitialisierung einf�gen

	m_Splitter = NULL;
	ExitMessage = NULL;
	ShowCalcView = NULL;
	HideCalcView = NULL;
	m_ToolbarRows = 2;
}

CMainFrame::~CMainFrame()
{
	Font.DeleteObject ();
	if (m_Splitter != NULL)
	{
		delete m_Splitter;
	}
	if (m_LeftSplitter != NULL)
	{
		delete m_LeftSplitter;
	}
	if (ExitMessage != NULL)
	{
		MessageHandler->Unregister (ExitMessage);
		delete ExitMessage;
	}
	if (ShowCalcView != NULL)
	{
		delete ShowCalcView;
	}
	if (HideCalcView != NULL)
	{
		delete HideCalcView;
	}
}


CBitmap Bitmap;

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
/*
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Fehler beim Erstellen der Symbolleiste.\n");
		return -1;      // Fehler beim Erstellen
	}
*/

/*
	CFrameWnd* const pMainWnd = (CFrameWnd*)AfxGetMainWnd();
	pMainWnd->GetMenu()->GetSubMenu(0)->DeleteMenu(0,MF_BYPOSITION);
	pMainWnd->DrawMenuBar();
*/

/*
	CMenu *Menu = GetMenu()->GetSubMenu(0);
	MENUITEMINFO Info;
	Info.cbSize = sizeof (MENUITEMINFO);
    Info.fMask = MIIM_BITMAP;
	Bitmap.LoadBitmap (IDB_MEN_BACK);
	Info.hbmpItem = (HBITMAP) Bitmap;
	Menu->SetMenuItemInfo (ID_BACK, &Info);
	DrawMenuBar();
*/

//	COLORREF DynButtonColor = CColorButton::DynColorGray;
	COLORREF DynButtonColor = RGB (192, 192, 192);
//	COLORREF RolloverColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = RGB (192, 192, 192);
	int BorderStyle = CColorButton::Solide;

	if (m_ToolbarRows == 2)
	{
//		DynButtonColor = CColorButton::DynColorBlue;
//		RolloverColor = RGB (204, 204, 255);
		DynButtonColor = CColorButton::DynColorGray;
		RolloverColor = RGB (192, 192, 192);
		BorderStyle = CColorButton::NoBorder;
	}

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	if (m_ToolbarRows == 2)
	{
		if (!m_wndToolBar1.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
			| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
			!m_wndToolBar1.LoadToolBar(IDR_MAINFRAME3))
		{
			TRACE0("Fehler beim Erstellen der Symbolleiste.\n");
			return -1;      // Fehler beim Erstellen
		}
		if (!m_wndToolBar2.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
			| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
			!m_wndToolBar2.LoadToolBar(IDR_MAINFRAME2))
		{
			TRACE0("Fehler beim Erstellen der Symbolleiste.\n");
			return -1;      // Fehler beim Erstellen
		}
	}
	else
	{
		if (!m_wndToolBar1.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
			| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
			!m_wndToolBar1.LoadToolBar(IDR_MAINFRAME1))
		{
			TRACE0("Fehler beim Erstellen der Symbolleiste.\n");
			return -1;      // Fehler beim Erstellen
		}
	}

/*
	if (!m_wndToolBar3.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar3.LoadToolBar(IDR_MAINFRAME3))
	{
		TRACE0("Fehler beim Erstellen der Symbolleiste.\n");
		return -1;      // Fehler beim Erstellen
	}
*/

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Fehler beim Erstellen der Statusleiste.\n");
		return -1;      // Fehler beim Erstellen
	}

	CRect rect;
	int nIndex;

	if (m_ToolbarRows == 2)
	{

//Toolbarbutton Druck Angebot

		nIndex = m_wndToolBar1.GetToolBarCtrl().CommandToIndex(ID_PRINT_ANG);
		m_wndToolBar1.SetButtonInfo(nIndex, ID_PRINT_ANG, TBBS_SEPARATOR, 100);
		m_wndToolBar1.GetToolBarCtrl().GetItemRect(nIndex, &rect);

//        m_wndToolBar1.SetButtonText (nIndex, _T("Angebot"));
//        m_wndToolBar1.SetSizes (CSize (42,38), CSize (16,15));

		if (!m_wndPrintAng.Create (_T("Angebot "),
								   WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar1, ID_PRINT_ANG))
		{
			TRACE0("Fehler beim Erstellen der Checkbos.\n");
			return -1;      // Fehler beim Erstellen
		}

		m_wndPrintAng.nID = ID_PRINT_ANG;
		m_wndPrintAng.Orientation = m_wndKopfText.Left;
		m_wndPrintAng.TextStyle = m_wndPrintAng.Standard;
//		m_wndPrintAng.BorderStyle = BorderStyle;
		m_wndPrintAng.BorderStyle = m_wndPrintAng.LeftLine;
		m_wndPrintAng.DynamicColor = TRUE;
		m_wndPrintAng.TextColor =RGB (0, 0, 0);
		m_wndPrintAng.SetBkColor (DynButtonColor);
		m_wndPrintAng.DynColor = DynButtonColor;
		m_wndPrintAng.RolloverColor = RolloverColor;
		m_wndPrintAng.SetWindowText (_T(" Angebot"));
		m_wndPrintAng.LoadBitmap (IDB_PRINT);
		m_wndPrintAng.SetFont (&Font);

//Toolbarbutton Druck Arbeitsanweisung

		nIndex = m_wndToolBar1.GetToolBarCtrl().CommandToIndex(ID_PRINT_PACK);
		m_wndToolBar1.SetButtonInfo(nIndex, ID_PRINT_PACK, TBBS_SEPARATOR, 150);
		m_wndToolBar1.GetToolBarCtrl().GetItemRect(nIndex, &rect);


		if (!m_wndPrintPack.Create (_T("Arbeitsanweisung "),
								   WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar1, ID_PRINT_PACK))
		{
			TRACE0("Fehler beim Erstellen der Checkbox.\n");
			return -1;      // Fehler beim Erstellen
		}

		m_wndPrintPack.nID = ID_PRINT_PACK;
		m_wndPrintPack.Orientation = m_wndKopfText.Left;
		m_wndPrintPack.TextStyle = m_wndPrintPack.Standard;
		m_wndPrintPack.BorderStyle = BorderStyle;
		m_wndPrintPack.DynamicColor = TRUE;
		m_wndPrintPack.TextColor =RGB (0, 0, 0);
		m_wndPrintPack.SetBkColor (DynButtonColor);
		m_wndPrintPack.DynColor = DynButtonColor;
		m_wndPrintPack.RolloverColor = RolloverColor;
		m_wndPrintPack.SetWindowText (_T(" Arbeitsanweisung"));
		m_wndPrintPack.LoadBitmap (IDB_PRINT);
//		m_wndPrintPack.LoadMask (IDB_POS_TEXT_MASK);
		m_wndPrintPack.SetFont (&Font);

//Toolbarbutton Druck Auftrag

		nIndex = m_wndToolBar1.GetToolBarCtrl().CommandToIndex(ID_PRINT_AUFTRAG);
		m_wndToolBar1.SetButtonInfo(nIndex, ID_PRINT_AUFTRAG, TBBS_SEPARATOR, 100);
		m_wndToolBar1.GetToolBarCtrl().GetItemRect(nIndex, &rect);


		if (!m_wndPrintAuf.Create (_T("Auftrag "),
								   WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar1, ID_PRINT_AUFTRAG))
		{
			TRACE0("Fehler beim Erstellen der Checkbox.\n");
			return -1;      // Fehler beim Erstellen
		}

		m_wndPrintAuf.nID = ID_PRINT_AUFTRAG;
		m_wndPrintAuf.Orientation = m_wndKopfText.Left;
		m_wndPrintAuf.TextStyle = m_wndPrintAuf.Standard;
		m_wndPrintAuf.BorderStyle = BorderStyle;
		m_wndPrintAuf.DynamicColor = TRUE;
		m_wndPrintAuf.TextColor =RGB (0, 0, 0);
		m_wndPrintAuf.SetBkColor (DynButtonColor);
		m_wndPrintAuf.DynColor = DynButtonColor;
		m_wndPrintAuf.RolloverColor = RolloverColor;
		m_wndPrintAuf.SetWindowText (_T(" Auftrag"));
		m_wndPrintAuf.LoadBitmap (IDB_PRINT);
//		m_wndPrintAuf.LoadMask (IDB_POS_TEXT_MASK);
		m_wndPrintAuf.SetFont (&Font);

//Toolbarbutton Druck Lieferschein

		nIndex = m_wndToolBar1.GetToolBarCtrl().CommandToIndex(ID_PRINT_LIEF);
		m_wndToolBar1.SetButtonInfo(nIndex, ID_PRINT_LIEF, TBBS_SEPARATOR, 115);
		m_wndToolBar1.GetToolBarCtrl().GetItemRect(nIndex, &rect);


		if (!m_wndPrintLief.Create (_T("Lieferschein "),
								   WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar1, ID_PRINT_LIEF))
		{
			TRACE0("Fehler beim Erstellen der Checkbox.\n");
			return -1;      // Fehler beim Erstellen
		}

		m_wndPrintLief.nID = ID_PRINT_LIEF;
		m_wndPrintLief.Orientation = m_wndKopfText.Left;
		m_wndPrintLief.TextStyle = m_wndPrintLief.Standard;
		m_wndPrintLief.BorderStyle = BorderStyle;
		m_wndPrintLief.DynamicColor = TRUE;
		m_wndPrintLief.TextColor =RGB (0, 0, 0);
		m_wndPrintLief.SetBkColor (DynButtonColor);
		m_wndPrintLief.DynColor = DynButtonColor;
		m_wndPrintLief.RolloverColor = RolloverColor;
		m_wndPrintLief.SetWindowText (_T(" Lieferschein"));
		m_wndPrintLief.LoadBitmap (IDB_PRINT);
//		m_wndPrintLief.LoadMask (IDB_POS_TEXT_MASK);
		m_wndPrintLief.SetFont (&Font);

//Toolbarbutton Druck Rechnung

		nIndex = m_wndToolBar1.GetToolBarCtrl().CommandToIndex(ID_PRINT_RECH);
		m_wndToolBar1.SetButtonInfo(nIndex, ID_PRINT_RECH, TBBS_SEPARATOR, 120);
		m_wndToolBar1.GetToolBarCtrl().GetItemRect(nIndex, &rect);


		if (!m_wndPrintRech.Create (_T("Rechnung "),
								   WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar1, ID_PRINT_RECH))
		{
			TRACE0("Fehler beim Erstellen des Buttons.\n");
			return -1;      // Fehler beim Erstellen
		}

		m_wndPrintRech.nID = ID_PRINT_RECH;
		m_wndPrintRech.Orientation = m_wndKopfText.Left;
		m_wndPrintRech.TextStyle = m_wndPrintRech.Standard;
//		m_wndPrintRech.BorderStyle = BorderStyle;
		m_wndPrintRech.BorderStyle = m_wndPrintRech.RightLine;
		m_wndPrintRech.DynamicColor = TRUE;
		m_wndPrintRech.TextColor =RGB (0, 0, 0);
		m_wndPrintRech.SetBkColor (DynButtonColor);
		m_wndPrintRech.DynColor = DynButtonColor;
		m_wndPrintRech.RolloverColor = RolloverColor;
		m_wndPrintRech.SetWindowText (_T(" Rechnung"));
		m_wndPrintRech.LoadBitmap (IDB_PRINT);
//		m_wndPrintRech.LoadMask (IDB_POS_TEXT_MASK);
		m_wndPrintRech.SetFont (&Font);


	}

	if (m_ToolbarRows != 2)
	{
		nIndex = m_wndToolBar1.GetToolBarCtrl().CommandToIndex(ID_CALC_VIEW);
		m_wndToolBar1.SetButtonInfo(nIndex, ID_CALC_VIEW, TBBS_SEPARATOR, 140);
		m_wndToolBar1.GetToolBarCtrl().GetItemRect(nIndex, &rect);
	//	rect.top = 1;
	//	rect.bottom = rect.top + 250 

		if (!m_wndCalcView.Create (_T("Kalkulationsansicht "),
								   BS_AUTOCHECKBOX | WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar1, ID_CALC_VIEW))
		{
			TRACE0("Fehler beim Erstellen der Checkbos.\n");
			return -1;      // Fehler beim Erstellen
		}
	}
	else
	{
		int nIndex = m_wndToolBar2.GetToolBarCtrl().CommandToIndex(ID_CALC_VIEW);
		m_wndToolBar2.SetButtonInfo(nIndex, ID_CALC_VIEW, TBBS_SEPARATOR, 140);
		m_wndToolBar2.GetToolBarCtrl().GetItemRect(nIndex, &rect);
	//	rect.top = 1;
	//	rect.bottom = rect.top + 250 /*drop height*/;

		if (!m_wndCalcView.Create (_T("Kalkulationsansicht "),
								   BS_AUTOCHECKBOX | WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar2, ID_CALC_VIEW))
		{
			TRACE0("Fehler beim Erstellen der Checkbos.\n");
			return -1;      // Fehler beim Erstellen
		}
	}
	m_wndCalcView.SetFont (&Font);


	if (m_ToolbarRows != 2)
	{
		nIndex = m_wndToolBar1.GetToolBarCtrl().CommandToIndex(ID_KOPF_TEXT);
		m_wndToolBar1.SetButtonInfo(nIndex, ID_KOPF_TEXT, TBBS_SEPARATOR, 100);
		m_wndToolBar1.GetToolBarCtrl().GetItemRect(nIndex, &rect);
		if (!m_wndKopfText.Create (_T("Kopftext "),
								   WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar1, ID_KOPF_TEXT))
		{
			TRACE0("Fehler beim Erstellen der Checkbos.\n");
			return -1;      // Fehler beim Erstellen
		}
	}
	else
	{
		nIndex = m_wndToolBar2.GetToolBarCtrl().CommandToIndex(ID_KOPF_TEXT);
		m_wndToolBar2.SetButtonInfo(nIndex, ID_KOPF_TEXT, TBBS_SEPARATOR, 100);
		m_wndToolBar2.GetToolBarCtrl().GetItemRect(nIndex, &rect);

		if (!m_wndKopfText.Create (_T("Kopftext "),
								   WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar2, ID_KOPF_TEXT))
		{
			TRACE0("Fehler beim Erstellen der Checkbos.\n");
			return -1;      // Fehler beim Erstellen
		}
	}


	m_wndKopfText.nID = ID_KOPF_TEXT;
	m_wndKopfText.Orientation = m_wndKopfText.Left;
	m_wndKopfText.TextStyle = m_wndKopfText.Standard;
	if (m_ToolbarRows == 2)
	{
		m_wndKopfText.BorderStyle = m_wndKopfText.LeftLine;
	}
	else
	{
		m_wndKopfText.BorderStyle = BorderStyle;
	}
	m_wndKopfText.DynamicColor = TRUE;
	m_wndKopfText.TextColor =RGB (0, 0, 0);
	m_wndKopfText.SetBkColor (DynButtonColor);
	m_wndKopfText.DynColor = DynButtonColor;
	m_wndKopfText.RolloverColor = RolloverColor;
	m_wndKopfText.SetWindowText (_T(" Kopftext"));
	m_wndKopfText.LoadBitmap (IDB_POS_TEXT);
	m_wndKopfText.LoadMask (IDB_POS_TEXT_MASK);
	m_wndKopfText.SetFont (&Font);

	if (m_ToolbarRows != 2)
	{
		nIndex = m_wndToolBar1.GetToolBarCtrl().CommandToIndex(ID_FUSS_TEXT);
		m_wndToolBar1.SetButtonInfo(nIndex, ID_FUSS_TEXT, TBBS_SEPARATOR, 100);
		m_wndToolBar1.GetToolBarCtrl().GetItemRect(nIndex, &rect);
		if (!m_wndFussText.Create (_T("Fusstext "),
								   WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar1, ID_FUSS_TEXT))
		{
			TRACE0("Fehler beim Erstellen der Checkbos.\n");
			return -1;      // Fehler beim Erstellen
		}
	}
	else
	{
		nIndex = m_wndToolBar2.GetToolBarCtrl().CommandToIndex(ID_FUSS_TEXT);
		m_wndToolBar2.SetButtonInfo(nIndex, ID_FUSS_TEXT, TBBS_SEPARATOR, 100);
		m_wndToolBar2.GetToolBarCtrl().GetItemRect(nIndex, &rect);
		if (!m_wndFussText.Create (_T("Fusstext "),
								   WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar2, ID_FUSS_TEXT))
		{
			TRACE0("Fehler beim Erstellen der Checkbos.\n");
			return -1;      // Fehler beim Erstellen
		}
	}

	m_wndFussText.nID = ID_FUSS_TEXT;
	m_wndFussText.Orientation = m_wndFussText.Left;
	m_wndFussText.TextStyle = m_wndFussText.Standard;
	m_wndFussText.BorderStyle = BorderStyle;
	m_wndFussText.DynamicColor = TRUE;
	m_wndFussText.TextColor =RGB (0, 0, 0);
	m_wndFussText.SetBkColor (DynButtonColor);
	m_wndFussText.DynColor = DynButtonColor;
	m_wndFussText.RolloverColor = RolloverColor;
	m_wndFussText.SetWindowText (_T(" Fu�text"));
	m_wndFussText.LoadBitmap (IDB_POS_TEXT);
	m_wndFussText.LoadMask (IDB_POS_TEXT_MASK);
	m_wndFussText.SetFont (&Font);


	if (m_ToolbarRows != 2)
	{
		nIndex = m_wndToolBar1.GetToolBarCtrl().CommandToIndex(ID_POS_TEXT);
		m_wndToolBar1.SetButtonInfo(nIndex, ID_POS_TEXT, TBBS_SEPARATOR, 130);
		m_wndToolBar1.GetToolBarCtrl().GetItemRect(nIndex, &rect);

		if (!m_wndPosText.Create (_T("Positionstext "),
								   WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar1, ID_POS_TEXT))
		{
			TRACE0("Fehler beim Erstellen der Checkbos.\n");
			return -1;      // Fehler beim Erstellen
		}
	}
	else
	{
		nIndex = m_wndToolBar2.GetToolBarCtrl().CommandToIndex(ID_POS_TEXT);
		m_wndToolBar2.SetButtonInfo(nIndex, ID_POS_TEXT, TBBS_SEPARATOR, 130);
		m_wndToolBar2.GetToolBarCtrl().GetItemRect(nIndex, &rect);

		if (!m_wndPosText.Create (_T("Positionstext "),
								   WS_CHILD | WS_VISIBLE, rect,
								   &m_wndToolBar2, ID_POS_TEXT))
		{
			TRACE0("Fehler beim Erstellen der Checkbos.\n");
			return -1;      // Fehler beim Erstellen
		}
	}

	m_wndPosText.nID = ID_POS_TEXT;
	m_wndPosText.Orientation = m_wndPosText.Left;
	m_wndPosText.TextStyle = m_wndPosText.Standard;
	m_wndPosText.BorderStyle = BorderStyle;
	if (m_ToolbarRows == 2)
	{
		m_wndPosText.BorderStyle = m_wndPosText.RightLine;
	}
	else
	{
		m_wndPosText.BorderStyle = BorderStyle;
	}
	m_wndPosText.DynamicColor = TRUE;
	m_wndPosText.TextColor =RGB (0, 0, 0);
	m_wndPosText.SetBkColor (DynButtonColor);
	m_wndPosText.DynColor = DynButtonColor;
	m_wndPosText.RolloverColor = RolloverColor;
	m_wndPosText.SetWindowText (_T(" Positionstext"));
	m_wndPosText.LoadBitmap (IDB_POS_TEXT);
	m_wndPosText.LoadMask (IDB_POS_TEXT_MASK);
	m_wndPosText.SetFont (&Font);


	// TODO: L�schen Sie diese drei Zeilen, wenn Sie nicht m�chten, dass die Systemleiste andockbar ist
//	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndToolBar1.EnableDocking(CBRS_ALIGN_ANY);
	if (m_ToolbarRows == 2)
	{
		m_wndToolBar2.EnableDocking(CBRS_ALIGN_ANY);
	}
	EnableDocking(CBRS_ALIGN_ANY);
//	DockControlBar(&m_wndToolBar);
	DockControlBar(&m_wndToolBar1);
	if (m_ToolbarRows == 2)
	{
		DockControlBar(&m_wndToolBar2);
	}

//	m_wndToolBar1.SetButtonStyle (14, TBBS_CHECKBOX);
//	VERIFY (m_wndToolBar1.SetButtonText (14, _T("Kalulationsansicht")));
	MessageHandler = CMessageHandler::GetInstance ();
	ExitMessage = new CExitMessage (this);
	MessageHandler->Register (ExitMessage);
	ShowCalcView = new CShowCalcView (this);
	CAngebotCore::GetInstance ()->SetShowCalcView (ShowCalcView);
	HideCalcView = new CHideCalcView (this);
	CAngebotCore::GetInstance ()->SetHideCalcView (HideCalcView);
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	return TRUE;
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	CAngebotCore *AngebotCore = CAngebotCore::GetInstance ();
	AngebotCore->SetDocument (pContext->m_pCurrentDoc);
	m_Splitter = new CWaSplitterWnd ();
	BOOL ret = m_Splitter->CreateStatic(this, 1, 2,  WS_CHILD | WS_VISIBLE, AFX_IDW_PANE_FIRST); 
    m_Splitter->CreateView(0,1,RUNTIME_CLASS(CAngebotView), CSize(0,0), 
		      pContext);
	m_LeftSplitter = new CWaSplitterWnd ();
	ret = m_LeftSplitter->CreateStatic(m_Splitter, 2, 1,  WS_CHILD | WS_VISIBLE, m_Splitter->IdFromRowCol (0,0)); 
	m_LeftSplitter->CreateView(0,0,RUNTIME_CLASS(CPartyService), CSize(0,0), 
      pContext);
	m_LeftSplitter->CreateView(1,0,RUNTIME_CLASS(CFavoriteView), CSize(0,0), 
      pContext);

	m_Splitter->SetColumnInfo (0, 200, 10);
	m_Splitter->SetColumnInfo (1, 450, 10);
	m_LeftSplitter->SetRowInfo (0, 400, 10);
	m_LeftSplitter->SetRowInfo (1, 200, 10);
	CAngebotView *view = (CAngebotView *) m_Splitter->GetPane (0, 1);
	if (view != NULL && IsWindow (view->m_hWnd))
	{
		view->PostMessage (WM_SETFOCUS, 0, 0l);
	}
	return TRUE;
}

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame-Meldungshandler

void CMainFrame::OnClose ()
{
	MessageHandler->RunMessage (ID_APP_EXIT);
	CFrameWnd::OnClose ();
}


CMainFrame::CExitMessage::CExitMessage (CMainFrame *p) : CRunMessage ()
{
	        SetId (VK_F5); 
			this->p = p;
}

void CMainFrame::CExitMessage::Run ()
{
	p->OnClose ();
}

CMainFrame::CShowCalcView::CShowCalcView (CMainFrame *p) : CRunMessage ()
{
	        SetId (NULL); 
			this->p = p;
}

void CMainFrame::CShowCalcView::Run ()
{
	p->OnHideCalcView ();
}

CMainFrame::CHideCalcView::CHideCalcView (CMainFrame *p) : CRunMessage ()
{
	        SetId (NULL); 
			this->p = p;
}

void CMainFrame::CHideCalcView::Run ()
{
	p->OnHideOnlyCalcView ();
}

void CMainFrame::OnBack()
{
	// TODO: Add your command handler code here
	MessageHandler->RunMessage (ID_BACK);
}

void CMainFrame::OnFileSave()
{
	// TODO: Add your command handler code here
	MessageHandler->RunMessage (ID_FILE_SAVE);
}

void CMainFrame::OnInsert()
{
	// TODO: Add your command handler code here
	MessageHandler->RunMessage (ID_INSERT);
}

void CMainFrame::OnUpdateInsert(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	BOOL Enable = 	MessageHandler->TestMessage (ID_INSERT);
    pCmdUI->Enable (Enable);
}

void CMainFrame::OnDelete()
{
	// TODO: Add your command handler code here
	MessageHandler->RunMessage (ID_DELETE);
}

void CMainFrame::OnUpdateDelete(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	BOOL Enable = 	MessageHandler->TestMessage (ID_DELETE);
    pCmdUI->Enable (Enable);

}

void CMainFrame::OnEditCut()
{
	// TODO: Add your command handler code here
}

void CMainFrame::OnEditCopy()
{
	// TODO: Add your command handler code here
	MessageHandler->RunMessage (ID_EDIT_COPY);
}

void CMainFrame::OnEditPaste()
{
	// TODO: Add your command handler code here
	MessageHandler->RunMessage (ID_EDIT_PASTE);
}

void CMainFrame::OnDeleteAng()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	MessageHandler->RunMessage (ID_DELETE_ANG);
}

void CMainFrame::OnPrintAng()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	MessageHandler->RunMessage (ID_PRINT_ANG);
}

void CMainFrame::OnPrintPack()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	MessageHandler->RunMessage (ID_PRINT_PACK);
}

void CMainFrame::OnPrintAuftrag()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	MessageHandler->RunMessage (ID_PRINT_AUFTRAG);
}

void CMainFrame::OnUpdateDeleteAng(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	BOOL Enable = 	MessageHandler->TestMessage (ID_DELETE);
    pCmdUI->Enable (Enable);
}

void CMainFrame::OnUpdatePrintAng(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	BOOL Enable = 	MessageHandler->TestMessage (ID_PRINT_ANG);
    pCmdUI->Enable (Enable);
}

void CMainFrame::OnUpdatePrintPack(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	BOOL Enable = 	MessageHandler->TestMessage (ID_PRINT_PACK);
    pCmdUI->Enable (Enable);
}

void CMainFrame::OnUpdatePrintAuftrag(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	BOOL Enable = 	MessageHandler->TestMessage (ID_PRINT_AUFTRAG);
    pCmdUI->Enable (Enable);
}

void CMainFrame::OnCalcView()
{
	// TODO: Add your command handler code here
	MENUITEMINFO Info;
	CMenu *Menu = GetMenu()->GetSubMenu(2);
	UINT state = Menu->GetMenuState (ID_CALC_VIEW, MF_BYCOMMAND); 
	MessageHandler->RunMessage (ID_CALC_VIEW);
	Info.cbSize = sizeof (MENUITEMINFO);
	Info.fMask = MIIM_STATE;
	if ((state & MF_CHECKED) == MF_CHECKED)
	{
		Info.fState = MF_UNCHECKED;
		Menu->SetMenuItemInfo (ID_CALC_VIEW, &Info);
		m_wndCalcView.SetCheck (BST_UNCHECKED);	
	}
	else
	{
		Info.fState = MF_CHECKED;
		Menu->SetMenuItemInfo (ID_CALC_VIEW, &Info);
		m_wndCalcView.SetCheck (BST_CHECKED);	
	}
}

void CMainFrame::OnHideCalcView()
{
	// TODO: Add your command handler code here
	MessageHandler->RunMessage (ID_DATA_BACK);

	MENUITEMINFO Info;
	CMenu *Menu = GetMenu()->GetSubMenu(2);
	UINT state = Menu->GetMenuState (ID_CALC_VIEW, MF_BYCOMMAND); 
	Info.cbSize = sizeof (MENUITEMINFO);
	Info.fMask = MIIM_STATE;
	if ((state & MF_CHECKED) == MF_CHECKED)
	{
		MessageHandler->RunMessage (ID_CALC_VIEW);
		Info.fState = MF_UNCHECKED;
		Menu->SetMenuItemInfo (ID_CALC_VIEW, &Info);
		m_wndCalcView.SetCheck (BST_UNCHECKED);	
	}

}

void CMainFrame::OnHideOnlyCalcView()
{
	// TODO: Add your command handler code here

	MENUITEMINFO Info;
	CMenu *Menu = GetMenu()->GetSubMenu(2);
	UINT state = Menu->GetMenuState (ID_CALC_VIEW, MF_BYCOMMAND); 
	Info.cbSize = sizeof (MENUITEMINFO);
	Info.fMask = MIIM_STATE;
	if ((state & MF_CHECKED) == MF_CHECKED)
	{
		MessageHandler->RunMessage (ID_CALC_VIEW);
		Info.fState = MF_UNCHECKED;
		Menu->SetMenuItemInfo (ID_CALC_VIEW, &Info);
		m_wndCalcView.SetCheck (BST_UNCHECKED);	
	}

}

void CMainFrame::OnPosText()
{
	// TODO: Add your command handler code here
	MessageHandler->RunMessage (ID_POS_TEXT);
}

void CMainFrame::OnUpdatePosText(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	BOOL Enable = 	MessageHandler->TestMessage (ID_POS_TEXT);
    pCmdUI->Enable (Enable);
}

void CMainFrame::OnPrintLief()
{
	// TODO: Add your command handler code here
	MessageHandler->RunMessage (ID_PRINT_LIEF);
}

void CMainFrame::OnUpdatePrintLief(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	BOOL Enable = 	MessageHandler->TestMessage (ID_PRINT_LIEF);
    pCmdUI->Enable (Enable);
}

void CMainFrame::OnPrintRech()
{
	// TODO: Add your command handler code here
	MessageHandler->RunMessage (ID_PRINT_RECH);
}

void CMainFrame::OnUpdatePrintRech(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	BOOL Enable = 	MessageHandler->TestMessage (ID_PRINT_RECH);
    pCmdUI->Enable (Enable);
}

void CMainFrame::OnKopfText()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	MessageHandler->RunMessage (ID_KOPF_TEXT);
}

void CMainFrame::OnUpdateKopfText(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	BOOL Enable = 	MessageHandler->TestMessage (ID_KOPF_TEXT);
    pCmdUI->Enable (Enable);
}

void CMainFrame::OnFussText()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	MessageHandler->RunMessage (ID_FUSS_TEXT);
}

void CMainFrame::OnUpdateFussText(CCmdUI *pCmdUI)
{
	// TODO: F�gen Sie hier Ihren Befehlsaktualisierungs-UI-Behandlungscode ein.
	BOOL Enable = 	MessageHandler->TestMessage (ID_FUSS_TEXT);
    pCmdUI->Enable (Enable);
}

BOOL CMainFrame::OnBarCheck ( UINT nID ) {
	
	switch ( nID ) {

	case IDR_MAINFRAME1: 
		ShowControlBar ( &m_wndToolBar1, !m_wndToolBar1.IsVisible (), FALSE );
		break;

	case IDR_MAINFRAME2: 
		ShowControlBar ( &m_wndToolBar2, !m_wndToolBar2.IsVisible (), FALSE );
		break;

	default:
		break;

	}
	RecalcLayout ();
	return TRUE;
}

// ...

void CMainFrame::OnUpdateToolBar1 ( CCmdUI * pCmdUI ) {
	
	pCmdUI->SetCheck ( m_wndToolBar1.IsVisible () );
	
}

void CMainFrame::OnUpdateToolBar2 ( CCmdUI * pCmdUI ) {
	
	pCmdUI->SetCheck ( m_wndToolBar2.IsVisible () );
	
}