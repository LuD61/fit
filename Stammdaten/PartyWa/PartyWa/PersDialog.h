#pragma once
#include "afxwin.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "AngebotCore.h"
#include "Datatables.h"
#include "CtrlGrid.h"
#include "Adr.h"
#include "Kun.h"
#include "Pers.h"
#include "afxdtctl.h"
#include "OkColorButton.h"
#include "CancelColorButton.h"
#include "BkBitmap.h"
#include "VLabel.h"
#include "TextLabel.h"

// CPersDialog-Dialogfeld

class CPersDialog : public CDialog
{
	DECLARE_DYNAMIC(CPersDialog)

public:
	CPersDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CPersDialog();

// Dialogfelddaten
	enum { IDD = IDD_PERS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();    // DDX/DDV-Unterstützung
	HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) ;
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	DECLARE_MESSAGE_MAP()
private:
	COLORREF m_DlgColor;
	HBRUSH m_DlgBrush;
	CBkBitmap BkBitmap;

	CVLabel m_Caption;
	CStatic m_GroupAdr;
	CStatic m_LPers;
	CTextEdit m_Pers;
	CStatic m_LName;
	CTextEdit m_Name1;
	CTextEdit m_Name2;
	CStatic m_LStr;
	CTextEdit m_Str;
	CStatic m_LPlzOrt;
	CTextEdit m_Plz;
	CTextEdit m_Ort1;
	CTextEdit m_Ort2;
	CStatic m_LTel;
	CTextEdit m_Tel;
//	CStatic m_LFax;
	CTextLabel m_LFax;
	CTextEdit m_Fax;
	CStatic m_LEmail;
	CTextEdit m_Email;
	CStatic m_LGebDat;
	CTextEdit m_GebDat;
	CDateTimeCtrl m_GebDat2;

	COkColorButton m_OkEx;
	CCancelColorButton m_CancelEx;
	CColorButton m_New;

	CCtrlGrid CtrlGrid;
	CCtrlGrid PlzOrtGrid;
	CCtrlGrid TeleGrid;
	CFormTab Form;

	int AdrTyp;
	KUN_CLASS *Kun;
	ADR_CLASS Adr;
	PERS_CLASS Pers;
	CAngebotCore *AngebotCore;
	CFont Font;
	CFont CaptionFont;
	CFont StaticFont;
	int DataPos;

public:
	void SetPers (PERS *pers);
	BOOL read ();
	BOOL write ();
	afx_msg void OnOK ();
	void CreateGrid ();
public:
	afx_msg void OnBnClickedNew();
public:
	afx_msg void OnEnKillfocusPers();
};
