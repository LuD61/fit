#include "StdAfx.h"
#include "Label.h"
#include "Bmap.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CLabel, CStatic)

CLabel::CLabel(void) : CStatic ()
{
	Orientation = Left;
	TextStyle = Underline;
	BorderStyle = None;
	TextColor = RGB (255, 255, 255);
	BkColor = RGB (102,102,102);
	DynamicColor = TRUE;
	parts = 120;
}

CLabel::~CLabel(void)
{
}

void CLabel::Properties (LPTSTR Label, int BorderStyle, int Orientation, 
		             BOOL DynamicColor, COLORREF TextColor,
					 COLORREF BkColor)
{
	Value = Label;
	SetWindowText (Label);
	SetBoderStyle (BorderStyle);
	SetOrientation (Orientation);
	SetDynamicColor (DynamicColor);
	SetTextColor (TextColor);
	SetBkColor (BkColor);
}

BEGIN_MESSAGE_MAP(CLabel, CStatic)
	ON_WM_SIZE ()
END_MESSAGE_MAP()

void CLabel::OnSize (UINT nType, int cx, int cy)
{
	Invalidate ();
}

void CLabel::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC cDC;

//	SetFont ();
	cDC.Attach (lpDrawItemStruct->hDC);
	Draw (cDC);
}

void CLabel::Draw (CDC& cDC)
{
	int x = 1;
	int y = 0;
	CString Text;
	GetWindowText (Text);
	CRect rect;
	CRect fillRect;
	CSize Size;
	int ret = 0;

	GetClientRect (&rect);
	fillRect = rect;

//	BkColor = cDC.GetBkColor ();
//	TextColor = cDC.GetTextColor ();
	if (DynamicColor)
	{
			FillRectParts (cDC, BkColor, rect);
	}
	else
	{
			cDC.FillRect (&rect, &CBrush (BkColor));
	}
    CPen *oldPen = NULL;

	if (BorderStyle == Dot)
	{
			CPen Pen (PS_DOT, 1, RGB (0, 0, 0));
			oldPen = cDC.SelectObject (&Pen);
			cDC.Rectangle (&rect);
	}
	else if (BorderStyle == Solide)
	{
				CPen Pen (PS_SOLID, 1, RGB (128, 128, 128));
				oldPen = cDC.SelectObject (&Pen);
				cDC.Rectangle (&rect);
	}
	cDC.SelectObject (oldPen);
	if (BorderStyle != None)
	{
		fillRect = rect;
		fillRect.left ++;
		fillRect.top ++;
		fillRect.right --;
		fillRect.bottom --;
		if (DynamicColor)
		{
				FillRectParts (cDC, BkColor, fillRect);
				cDC.FrameRect (&rect, &CBrush (RGB (128, 128, 128)));
		}
		else
		{
				cDC.FillRect (&fillRect, &CBrush (BkColor));
		}
	}

	if ((HBITMAP) Bitmap != NULL)
	{
		POINT Size;
		BMAP Bmap;
		Bmap.BitmapSize (cDC, (HBITMAP) Bitmap, &Size);
		x = 10; 
		y = max (0, (rect.bottom - Size.y) / 2); 

		if ((HBITMAP) Mask != NULL)
		{
			Bmap.PrintTransparentBitmap ((HBITMAP) Bitmap, (HBITMAP) Mask, &cDC, x, y);
		}
		else if (hMask != NULL)
		{
			Bmap.PrintTransparentBitmap ((HBITMAP) Bitmap, hMask, &cDC, x, y);
		}
		else
		{
			Bmap.PrintTransparentBitmap ((HBITMAP) Bitmap, &cDC, x, y);
		}
		x += Size.x + 10;
	}

	CString sText = Text;
/*
	CString T = sText;
	if (T.TrimRight () == _T(""))
	{
		sText = Value;
	}
*/

	LPTSTR t = strtok (sText.GetBuffer (), "\n");
	y = 0;
	int row = 0;
	while (t != NULL)
	{
		CString T = t;
		Size = cDC.GetTextExtent (T);
		y = row * Size.cy;
		row ++;
	    t = strtok (NULL, "\n");
	}
	if (row > 0)
	{
		y = row * Size.cy;
	}
    
	int start = max (0, (rect.bottom - y) / 2);

	sText = Text;
	t = strtok (sText.GetBuffer (), "\n");
	row = 0;
	while (t != NULL)
	{
		CString T = t;
		Size = cDC.GetTextExtent (T);
		if (Orientation == Center)
		{
			x = max (x, (rect.right - Size.cx) / 2);
		}
		else if (Orientation == Right)
		{
			x = max (x, (rect.right - Size.cx - 2));
		}
		y = row * Size.cy + start;
		cDC.SetBkMode (TRANSPARENT);

		if (IsWindowEnabled ())
		{
			cDC.SetTextColor (TextColor);
			cDC.TextOut (x, y, t, (int) _tcslen (t));
		}
		else
		{
			cDC.SetTextColor (RGB (192, 192, 192));
			cDC.TextOut (x, y, t, (int) _tcslen (t));
			cDC.SetTextColor (RGB (255, 255, 255));
			cDC.TextOut (x + 1, y + 1, t, (int) _tcslen (t));
		}

		row ++;
	    t = strtok (NULL, "\n");
	}
}

void CLabel::FillRectParts (CDC &cDC, COLORREF BkColor, CRect rect)
{
	int blue = (BkColor >> 16) & 0xFF;
	int green = (BkColor >> 8) & 0xFF;
	int red = (BkColor) & 0xFF;
	int part = (rect.right) / parts;
	if (part < 1)
	{
		cDC.FillSolidRect (&rect, RGB (red, green, blue));
		return;
	}

	int colordiff = 2;
	part ++;

	CRect rect1 (rect.left, rect.top, part, rect.bottom); 

	for (int i = 0; i < parts / 3; i ++)
	{
		cDC.FillSolidRect (&rect1, RGB (red, green, blue));
		rect1.left += part - 1;
		rect1.right += part;
		if (red <= 255 - colordiff) red += colordiff;

		cDC.FillSolidRect (&rect1, RGB (red, green, blue));

		rect1.left += part - 1;
		rect1.right += part;
		if (green <= 255 - colordiff) green += colordiff;
		cDC.FillSolidRect (&rect1, RGB (red, green, blue));

		rect1.left += part - 1;
		rect1.right += part;
		if (blue <= 255 - colordiff) blue += colordiff;
	}
	rect1.left += part - 1;
	rect1.right = rect.right; 
	cDC.FillSolidRect (&rect1, RGB (red, green, blue));

}
