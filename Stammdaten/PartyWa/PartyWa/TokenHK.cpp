#include "StdAfx.h"
#include "TokenHK.h"

CTokenHK::CTokenHK() :
          CToken ()   
{
	m_p = NULL;

}

CTokenHK::CTokenHK (char *Txt, char *sep) :
                    CToken (Txt, sep)
{
}

CTokenHK::CTokenHK (CString &Txt, char *sep) :
                    CToken (Txt, sep)
{
}

CTokenHK::~CTokenHK()
{
}

void CTokenHK::GetTokens (char *Txt)
{
     AnzToken = 0;
     Buffer.TrimRight ();
     char *b = Buffer.GetBuffer (512);
     char *p = StrTok (b, sep.GetBuffer (512));
     while (p != NULL)
     {
             AnzToken ++;
             p = StrTok (NULL, sep.GetBuffer (512));
     }
     Tokens = new CString *[AnzToken];
     if (Tokens == NULL)
     {
             return;
     }

     Buffer = Txt;
     Buffer.TrimRight ();
     b = Buffer.GetBuffer (0);
     p = StrTok (b, sep.GetBuffer (0));
     int i = 0;
     while (p != NULL)
     {
             Tokens[i] = new CString (p);
             p = StrTok (NULL, sep.GetBuffer (512));
             i ++;
     }
     AktToken = 0;
}

char *CTokenHK::StrTok (char *p, char *sep)
{
	int i = 0;
	if (p != NULL)
	{
		m_p = p;
	}
	else
	{
		p = m_p;
	}
	if (m_p != NULL)
	{
		while (*m_p != 0)
		{
			if (*m_p == '\"')
			{
				m_p += 1;
				while (*m_p != '\"' && *m_p != 0) m_p += 1;
				if (*m_p == 0)
				{
					return NULL;
				}
				m_p += 1;
				if (*m_p == 0)
				{
					return NULL;
				}
			}
			for (i = 0; i < (int) strlen (sep); i ++)
			{
				if (*m_p == sep[i])
				{
					*m_p = 0;
					break;
				}
			}
			if (*m_p == 0)
			{
				m_p += 1;
				break;
			}
			m_p += 1;
		}
		while (*m_p != 0)
		{
			for (i = 0; i < (int) strlen (sep); i ++)
			{
				if (*m_p == sep[i])
				{
					*m_p = 0;
					break;
				}
			}
			if (*m_p != 0)
			{
				break;
			}
			m_p += 1;
		}
		if (*m_p == 0)
		{
			m_p = NULL;
		}
	}
	return p;
}
