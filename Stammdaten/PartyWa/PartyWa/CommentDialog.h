#pragma once
#include "afxwin.h"
#include "FormTab.h"
#include "AngebotCore.h"
#include "Datatables.h"
#include "TextEdit.h"
#include "OkColorButton.h"
#include "CancelColorButton.h"
#include "OpenColorButton.h"
#include "BkBitmap.h"
#include "VLabel.h"
#include "TextLabel.h"
#include "ChoiceTextpool.h"
#include "TextManager.h"


// CCommentDialog-Dialogfeld

class CCommentDialog : public CDialog
{
	DECLARE_DYNAMIC(CCommentDialog)

public:
	CCommentDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CCommentDialog();

// Dialogfelddaten
	enum { IDD = IDD_COMMENTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog ();
	HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) ;
    virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

private:
	CFormTab Form;
	COLORREF m_DlgColor;
	HBRUSH m_DlgBrush;
	CBkBitmap BkBitmap;
	CVLabel m_Caption;
	COkColorButton m_OkEx;
	CCancelColorButton m_CancelEx;
	COpenColorButton m_Open1;
	COpenColorButton m_Open2;
	COpenColorButton m_Open3;
	CFont Font;
	CFont CaptionFont;
	CFont StaticFont;
	CChoiceTextpool *ChoiceTextpool;
	CTextmanager m_Angmanager;
	CTextmanager m_Rechmanager;
	CTextmanager m_Internmanager;

public:
	CStatic m_LCommAngebot;
public:
	CTextEdit m_CommAngebot;
public:
	CStatic m_LCommRechnung;
public:
	CTextEdit m_CommRechnung;
public:
	CStatic m_LCommIntern;
public:
	CTextEdit m_CommIntern;
public:
	afx_msg void OnOK ();
	afx_msg void OnCancel ();
	afx_msg void OnOpen1 ();
	afx_msg void OnOpen2 ();
	afx_msg void OnOpen3 ();
};
