#include "StdAfx.h"
#include "ToolbarButton.h"

IMPLEMENT_DYNAMIC(CToolbarButton, CButton)

CToolbarButton::CToolbarButton(void) :
                CButton () 
{
	SetFocusActive = FALSE;
}

CToolbarButton::~CToolbarButton(void)
{
}

BEGIN_MESSAGE_MAP(CToolbarButton, CButton)
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()

void CToolbarButton::OnSetFocus (CWnd *oldFocus)
{
	if (SetFocusActive)
	{
		return;
	}

	SetFocusActive = TRUE;
	GetParent ()->SendMessage (WM_COMMAND, GetDlgCtrlID (), 0l);
	oldFocus->SetFocus ();
	SetFocusActive = FALSE;
}
