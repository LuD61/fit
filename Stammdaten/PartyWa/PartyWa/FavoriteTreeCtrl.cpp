#include "StdAfx.h"
#include "resource.h"
#include "FavoriteTreeCtrl.h"
#include "Bmap.h"

IMPLEMENT_DYNCREATE(CFavoriteTreeCtrl, CTreeCtrl)

BOOL CFavoriteTreeCtrl::ShowWa = FALSE;

CFavoriteTreeCtrl::CFavoriteTreeCtrl(void)
{
	CFavoriteItem *Item;
	CString Bws;
	CString ProgrammName;

	Bws.GetEnvironmentVariable (_T("bws"));
	ProgrammName.Format (_T("%s\\bin\\12100"), Bws.GetBuffer ());
	Item = new CFavoriteItem (_T("Artikel"),
		                      ProgrammName.GetBuffer ()); 
	Stammdaten.Add (Item);
	ProgrammName.Format (_T("%s\\bin\\16500"), Bws.GetBuffer ());
	Item = new CFavoriteItem (_T("Kunden"),
		                      ProgrammName.GetBuffer ()); 
	Stammdaten.Add (Item);
	ProgrammName.Format (_T("%s\\bin\\11200"), Bws.GetBuffer ());
	Item = new CFavoriteItem (_T("Ladenpreise"),
		                      ProgrammName.GetBuffer ()); 
	Stammdaten.Add (Item);
	ProgrammName.Format (_T("%s\\bin\\iprman"), Bws.GetBuffer ());
	Item = new CFavoriteItem (_T("Gro�handelspreise"),
		                      ProgrammName.GetBuffer ()); 
	Stammdaten.Add (Item);

	ProgrammName.Format (_T("%s\\bin\\51100"), Bws.GetBuffer ());
	Item = new CFavoriteItem (_T("Auftragsbearbeitung"),
		                      ProgrammName.GetBuffer ()); 
	Warenausgang.Add (Item);
	ProgrammName.Format (_T("%s\\bin\\53100"), Bws.GetBuffer ());
	Item = new CFavoriteItem (_T("Lieferscheinbearbeitung"),
		                      ProgrammName.GetBuffer ()); 
	Warenausgang.Add (Item);
	CAngebotCore *AngebotCore = CAngebotCore::GetInstance ();
	AngebotCore->SetStammdaten (&Stammdaten);
	AngebotCore->SetWarenausgang (&Warenausgang);
}

CFavoriteTreeCtrl::~CFavoriteTreeCtrl(void)
{
	Stammdaten.DestroyElements ();
	Warenausgang.DestroyElements ();
}

void CFavoriteTreeCtrl::Init ()
{
	HTREEITEM RootStamm;
	HTREEITEM RootWa;

	CFavoriteItem *Item;
	CFavoriteItem **it;

    CBitmap bitmaproot;
    CBitmap maskroot;
    CBitmap bitmapstamm;
    CBitmap maskstamm;
    CBitmap bitmapwa;
    CBitmap maskwa;
	ImageList.Create (GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), 
		              ILC_COLOR | ILC_MASK, 0, 4); 
	bitmaproot.LoadBitmap (IDB_ROOT);
	maskroot.LoadBitmap (IDB_ROOT_MASK);
	HBITMAP hBitmaproot = BMAP::PrintBitmapMem ((HBITMAP) bitmaproot, (HBITMAP) maskroot, RGB (255, 255, 255));

	bitmapstamm.LoadBitmap (IDB_STAMM);
	maskstamm.LoadBitmap (IDB_STAMM_MASK);
	HBITMAP hBitmapstamm = BMAP::PrintBitmapMem ((HBITMAP) bitmapstamm, (HBITMAP) maskstamm, RGB (255, 255, 255));

	bitmapwa.LoadBitmap (IDB_AUFTRAG);
	maskwa.LoadBitmap (IDB_AUFTRAG_MASK);
	HBITMAP hBitmapwa = BMAP::PrintBitmapMem ((HBITMAP) bitmapwa, (HBITMAP) maskwa, RGB (255, 255, 255));

	ImageList.Add (CBitmap::FromHandle (hBitmaproot), (CBitmap *) NULL);
	ImageList.Add (CBitmap::FromHandle (hBitmapstamm), (CBitmap *) NULL);
	ImageList.Add (CBitmap::FromHandle (hBitmapwa), (CBitmap *) NULL);

	SetImageList (&ImageList, TVSIL_NORMAL);   
    SetTreeStyle (TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS);
	CString Text;
	RootStamm = AddRootItem (_T("Stammdaten"), 0, 5);
	if (ShowWa)
	{
		RootWa = AddRootItem (_T("Warenausgang"), 0, 5);
	}
	Stammdaten.Start ();
	int idx = 0;
	while ((it = Stammdaten.GetNext ()) != NULL)
	{
		Item = *it;
		AddChildItem (RootStamm,  Item->GetName (),
					  idx, 0, 1, TVIS_STATEIMAGEMASK | TVIS_EXPANDED);
	}
	if (ShowWa)
	{
		Warenausgang.Start ();
		idx = 0;
		while ((it = Warenausgang.GetNext ()) != NULL)
		{
			Item = *it;
			AddChildItem (RootWa,  Item->GetName (),
						  idx, 0, 2, TVIS_STATEIMAGEMASK | TVIS_EXPANDED);
		}
	}
}

HTREEITEM CFavoriteTreeCtrl::AddRootItem (LPSTR Text, int idx, int Childs)
{
   TV_INSERTSTRUCT TvItem;

   TvItem.hParent     = TVI_ROOT;
   TvItem.hInsertAfter = TVI_LAST;

   TvItem.item.mask = TVIF_CHILDREN |  TVIF_CHILDREN | TVIF_HANDLE |
                      TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE |
                      TVIF_STATE | TVIF_TEXT;
   TvItem.item.hItem = NULL;
   TvItem.item.state      = TVIS_STATEIMAGEMASK | TVIS_EXPANDED; 
   TvItem.item.stateMask  = TVIS_STATEIMAGEMASK | TVIS_EXPANDED; 
   TvItem.item.pszText    = Text;
   TvItem.item.cchTextMax = (int) strlen (Text);      
   TvItem.item.iImage     = 0;          
   TvItem.item.iSelectedImage = 0;  
   TvItem.item.cChildren = Childs;       
   TvItem.item.lParam     = (LPARAM) idx;

   return InsertItem (&TvItem);
}

HTREEITEM CFavoriteTreeCtrl::AddChildItem (HTREEITEM hParent,  LPSTR Text, int idx, int Childs, 
                                   int iImage, DWORD state)
{
   TV_INSERTSTRUCT TvItem;

   TvItem.hParent     = hParent;
   TvItem.hInsertAfter = TVI_LAST;

   TvItem.item.mask = TVIF_CHILDREN | TVIF_HANDLE |
                      TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE |
                      TVIF_STATE | TVIF_TEXT;
   TvItem.item.hItem = NULL;
   TvItem.item.state      = state; 
   TvItem.item.stateMask  = state; 
   TvItem.item.pszText    = Text;
   TvItem.item.cchTextMax = (int) strlen (Text);      
   TvItem.item.iImage     = iImage;
   TvItem.item.iSelectedImage = iImage;  
   TvItem.item.cChildren = Childs;       
   TvItem.item.lParam     = (LPARAM) idx;

   return InsertItem (&TvItem);
}

void CFavoriteTreeCtrl::SetTreeStyle (DWORD style)
{
    DWORD oldStyle = GetWindowLong (m_hWnd, GWL_STYLE);

    SetWindowLong (m_hWnd, GWL_STYLE, oldStyle | style);
}
