#pragma once
#include "A_bas.h"
#include "Partyk.h"
#include "Partyp.h"
#include "Aufk.h"
#include "Aufp.h"
#include "Lsk.h"
#include "Lsp.h"
#include "Mdn.h"
#include "Fil.h"
#include "Kun.h"
#include "Adr.h"
#include "A_hndw.h"
#include "A_eig.h"
#include "A_kun.h"
#include "Kumebest.h"
#include "Ptabn.h"
#include "angpt.h"
#include "ls_txt.h"
#include "lief_retba.h"

class CDatatables
{
public:
	A_BAS_CLASS A_bas;
	PARTYK_CLASS Partyk;
	PARTYP_CLASS Partyp;
	AUFK_CLASS Aufk;
	AUFP_CLASS Aufp;
	LSK_CLASS Lsk;
	LSP_CLASS Lsp;
	MDN_CLASS Mdn;
	FIL_CLASS Fil;
	KUN_CLASS Kun;
	ADR_CLASS MdnAdr;
	ADR_CLASS FilAdr;
	ADR_CLASS KunAdr;
	A_HNDW_CLASS A_hndw;
	A_EIG_CLASS A_eig;
	A_KUN_CLASS A_kun;
	PTABN_CLASS Ptabn;
	ANGPT_CLASS Angpt;
	LS_TXT_CLASS Ls_txt;
	KMB_CLASS Kmb;
	LIEF_RETBA_CLASS Lief_retba;
	static CDatatables *Instance;
	CDatatables(void);
public:
	A_BAS_CLASS *GetABas ()
	{
		return &A_bas;
	}
	PARTYK_CLASS *GetPartyk ()
	{
		return &Partyk;
	}
	PARTYP_CLASS *GetPartyp ()
	{
		return &Partyp;
	}

	AUFK_CLASS *GetAufk ()
	{
		return &Aufk;
	}

	LSK_CLASS *GetLsk ()
	{
		return &Lsk;
	}

	MDN_CLASS *GetMdn ()
	{
		return &Mdn;
	}
	FIL_CLASS *GetFil ()
	{
		return &Fil;
	}

	KUN_CLASS *GetKun ()
	{
		return &Kun;
	}

	ADR_CLASS *GetMdnAdr ()
	{
		return &MdnAdr;
	}

	LIEF_RETBA_CLASS *GetLiefRetBa ()
	{
		return &Lief_retba;
	}

	ADR_CLASS *GetFilAdr ()
	{
		return &FilAdr;
	}

	ADR_CLASS *GetKunAdr ()
	{
		return &KunAdr;
	}

	KMB_CLASS *GetKmb ()
	{
		return &Kmb;
	}


	A_KUN_CLASS *GetAKun ()
	{
		return &A_kun;
	}

	A_HNDW_CLASS *GetAHndw ()
	{
		return &A_hndw;
	}

	A_EIG_CLASS *GetAEig ()
	{
		return &A_eig;
	}

	PTABN_CLASS *GetPtabn ()
	{
		return &Ptabn;
	}

	ANGPT_CLASS *GetAngpt ()
	{
		return &Angpt;
	}

	LS_TXT_CLASS *GetLsTxt ()
	{
		return &Ls_txt;
	}

	AUFP_CLASS *GetAufp ()
	{
		return &Aufp;
	}

	LSP_CLASS *GetLsp ()
	{
		return &Lsp;
	}

    static CDatatables *GetInstance ();
    static void DeleteInstance ();
};
