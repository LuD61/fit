#include "stdafx.h"
#include "ChoiceAdr.h"
#include "DbUniCode.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceAdr::Sort1 = -1;
int CChoiceAdr::Sort2 = -1;
int CChoiceAdr::Sort3 = -1;
int CChoiceAdr::Sort4 = -1;
int CChoiceAdr::Sort5 = -1;
int CChoiceAdr::Sort6 = -1;
int CChoiceAdr::Sort7 = -1;

CChoiceAdr::CChoiceAdr(CWnd* pParent) 
        : CChoiceX(pParent)
{
	m_AdrTyp = 0;
    m_AndPart = _T("");
}

CChoiceAdr::~CChoiceAdr() 
{
	DestroyList ();
}

void CChoiceAdr::DestroyList() 
{
	for (std::vector<CAdrList *>::iterator pabl = AdrList.begin (); pabl != AdrList.end (); ++pabl)
	{
		CAdrList *abl = *pabl;
		delete abl;
	}
    AdrList.clear ();
}

void CChoiceAdr::FillList () 
{
    long  adr;
    TCHAR adr_krz [34];
    TCHAR adr_nam1 [37];
    TCHAR adr_nam2 [37];
    TCHAR ort1 [37];
    TCHAR plz [9];
    TCHAR str [37];
	CString Sql;

	DestroyList ();
	CListCtrl *listView = GetListView ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Auswahl Adressen"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Adresse"),  1, 100, LVCFMT_RIGHT);
    SetCol (_T("Name"),  2, 250);
    SetCol (_T("Name"),  3, 250);
    SetCol (_T("PLZ"),   4, 250);
    SetCol (_T("Ort"),   5, 250);
    SetCol (_T("Strasse"),  6, 250);

	if (AdrList.size () == 0)
	{
		DbClass->sqlout ((long *)&adr,      SQLLONG, 0);
	    DbClass->sqlout ((LPTSTR)  adr_nam1,   SQLCHAR, sizeof (adr_nam1));
	    DbClass->sqlout ((LPTSTR)  adr_nam2,   SQLCHAR, sizeof (adr_nam2));
	    DbClass->sqlout ((LPTSTR)  plz,   SQLCHAR, sizeof (plz));
	    DbClass->sqlout ((LPTSTR)  ort1,   SQLCHAR, sizeof (ort1));
	    DbClass->sqlout ((LPTSTR)  str,   SQLCHAR, sizeof (str));
		if (m_AdrTyp == 0)
		{
			Sql =  (_T("select adr.adr, adr.adr_nam1, adr.adr_nam1, plz, ort1, str  ")
					 _T("from adr where adr.adr > 0 "));
		}
		else
		{
			Sql.Format (_T("select adr.adr, adr.adr_nam1, adr.adr_nam1, plz, ort1, str  ")
					    _T("from adr where adr.adr > 0 and adr_typ = %hd"), m_AdrTyp);
		}
		if (m_AndPart != _T(""))
		{
			Sql += _T(" ");
			Sql += m_AndPart;
		}
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
										 
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) adr_nam1;
			CDbUniCode::DbToUniCode (adr_nam1, pos);
			pos = (LPSTR) adr_nam2;
			CDbUniCode::DbToUniCode (adr_nam2, pos);
			pos = (LPSTR) plz;
			CDbUniCode::DbToUniCode (plz, pos);
			pos = (LPSTR) ort1;
			CDbUniCode::DbToUniCode (ort1, pos);
			pos = (LPSTR) str;
			CDbUniCode::DbToUniCode (str, pos);
			CAdrList *abl = new CAdrList ();
			abl->adr = adr;
			abl->adr_nam1 = adr_nam1;
			abl->adr_nam2 = adr_nam2;
			abl->plz = plz;
			abl->ort1 = ort1;
			abl->str = str;
			AdrList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CAdrList *>::iterator pabl = AdrList.begin (); pabl != AdrList.end (); ++pabl)
	{
		CAdrList *abl = *pabl;
		CString Adr;
		Adr.Format (_T("%ld"), abl->adr); 
		CString Num;
		CString Bez;
		_tcscpy (adr_krz, abl->adr_krz.GetBuffer ());

		CString LText;
		LText.Format (_T("%ld %s"), abl->adr, 
									abl->adr_krz.GetBuffer ());
		if (Style == LVS_REPORT)
		{
                Num = Adr;
        }
        else
        {
                Num = LText;
        }
        int ret = InsertItem (i, -1);
        ret = SetItemText (Num.GetBuffer (), i, 1);
		ret = SetItemText (abl->adr_nam1.GetBuffer (), i, 2);
		ret = SetItemText (abl->adr_nam2.GetBuffer (), i, 3);
		ret = SetItemText (abl->plz.GetBuffer (), i, 4);
		ret = SetItemText (abl->ort1.GetBuffer (), i, 5);
		ret = SetItemText (abl->str.GetBuffer (), i, 6);
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort6 = -1;
    Sort7 = -1;
    Sort (listView);
}


void CChoiceAdr::NumLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        LPTSTR p = aText.GetBuffer ();
        p = _tcstok (p, _T(" "));
        aText.Format (_T("%s"), p);
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}

void CChoiceAdr::BezLabel (CListCtrl *ListBox)
{
    int count = ListBox->GetItemCount ();

    for (int i = 0; i < count; i ++)
    {
        CString aText = ListBox->GetItemText (i, 0);
        aText.TrimRight ();
        CString iText = ListBox->GetItemText (i, 1);
        iText.TrimRight ();
        aText.Format (_T("%s %s"), aText.GetBuffer (), iText.GetBuffer ());
        ListBox->SetItemText (i, 0, aText.GetBuffer ());
    }
}


void CChoiceAdr::SearchNum (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();
	int i;

	for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 1);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAdr::SearchCol (CListCtrl *ListBox, LPTSTR Search, int col)
{
    int count = ListBox->GetItemCount ();
	int i;

    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, col);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceAdr::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    switch (SortRow)
    {
        case 0 :
//             SearchNum (ListBox, EditText.GetBuffer (16));
             break;
        case 1 :
             SearchNum (ListBox, EditText.GetBuffer ());
             break;
        case 2 :
        case 3 :
        case 4 :
        case 5 :
        case 6 :
             EditText.MakeUpper ();
             SearchCol (ListBox, EditText.GetBuffer (), SortRow);
             break;
    }
}

int CChoiceAdr::GetPtBez (LPTSTR ptwert, LPTSTR ptbez)
{
	_tcscpy (ptbez, _T(""));
	DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
	DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
	return DbClass->sqlcomm (_T("select ptbez from ptabn where ptitem = \"peri_typ\" ")
							_T("and ptwert = ?"));
}



int CALLBACK CChoiceAdr::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 0)
   {
//	return strcmp(strItem2.GetBuffer (0), strItem1.GetBuffer (0)) * Sort1;
   }
   else if (SortRow == 1)
   {
	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   else if (SortRow == 2)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort3;
   }
   else if (SortRow == 3)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort4;
   }
   else if (SortRow == 4)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort5;
   }
   else if (SortRow == 5)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort6;
   }
   else if (SortRow == 6)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort7;
   }
   return 0;
}


void CChoiceAdr::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
              break;
        case 2:
              Sort3 *= -1;
              break;
        case 3:
              Sort4 *= -1;
              break;
        case 4:
              Sort5 *= -1;
              break;
        case 5:
              Sort6 *= -1;
              break;
        case 6:
              Sort7 *= -1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CAdrList *abl = AdrList [i];
		   
		   abl->adr      = _tstoi (ListBox->GetItemText (i, 1));
		   abl->adr_nam1 = ListBox->GetItemText (i, 2);
		   abl->adr_nam2 = ListBox->GetItemText (i, 3);
		   abl->plz      = ListBox->GetItemText (i, 4);
		   abl->ort1     = ListBox->GetItemText (i, 5);
		   abl->str      = ListBox->GetItemText (i, 6);
	}
}

void CChoiceAdr::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = AdrList [idx];
}

CAdrList *CChoiceAdr::GetSelectedText ()
{
	CAdrList *abl = (CAdrList *) SelectedRow;
	return abl;
}

