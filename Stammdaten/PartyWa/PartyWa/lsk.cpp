#include "stdafx.h"
#include "lsk.h"

struct LSK lsk, lsk_null;

void LSK_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &lsk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lsk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &lsk.ls,   SQLLONG, 0);
    sqlout ((long *) &lsk.ls,SQLLONG,0);
    sqlout ((long *) &lsk.adr,SQLLONG,0);
    sqlout ((short *) &lsk.mdn,SQLSHORT,0);
    sqlout ((long *) &lsk.auf,SQLLONG,0);
    sqlout ((short *) &lsk.kun_fil,SQLSHORT,0);
    sqlout ((long *) &lsk.kun,SQLLONG,0);
    sqlout ((short *) &lsk.fil,SQLSHORT,0);
    sqlout ((TCHAR *) lsk.feld_bz1,SQLCHAR,20);
    sqlout ((DATE_STRUCT *) &lsk.lieferdat,SQLDATE,0);
    sqlout ((TCHAR *) lsk.lieferzeit,SQLCHAR,6);
    sqlout ((TCHAR *) lsk.hinweis,SQLCHAR,49);
    sqlout ((short *) &lsk.ls_stat,SQLSHORT,0);
    sqlout ((TCHAR *) lsk.kun_krz1,SQLCHAR,17);
    sqlout ((double *) &lsk.auf_sum,SQLDOUBLE,0);
    sqlout ((TCHAR *) lsk.feld_bz2,SQLCHAR,12);
    sqlout ((double *) &lsk.lim_er,SQLDOUBLE,0);
    sqlout ((TCHAR *) lsk.partner,SQLCHAR,37);
    sqlout ((long *) &lsk.pr_lst,SQLLONG,0);
    sqlout ((TCHAR *) lsk.feld_bz3,SQLCHAR,8);
    sqlout ((short *) &lsk.pr_stu,SQLSHORT,0);
    sqlout ((long *) &lsk.vertr,SQLLONG,0);
    sqlout ((long *) &lsk.tou,SQLLONG,0);
    sqlout ((TCHAR *) lsk.adr_nam1,SQLCHAR,37);
    sqlout ((TCHAR *) lsk.adr_nam2,SQLCHAR,37);
    sqlout ((TCHAR *) lsk.pf,SQLCHAR,17);
    sqlout ((TCHAR *) lsk.str,SQLCHAR,37);
    sqlout ((TCHAR *) lsk.plz,SQLCHAR,9);
    sqlout ((TCHAR *) lsk.ort1,SQLCHAR,37);
    sqlout ((double *) &lsk.of_po,SQLDOUBLE,0);
    sqlout ((short *) &lsk.delstatus,SQLSHORT,0);
    sqlout ((long *) &lsk.rech,SQLLONG,0);
    sqlout ((TCHAR *) lsk.blg_typ,SQLCHAR,2);
    sqlout ((double *) &lsk.zeit_dec,SQLDOUBLE,0);
    sqlout ((long *) &lsk.kopf_txt,SQLLONG,0);
    sqlout ((long *) &lsk.fuss_txt,SQLLONG,0);
    sqlout ((long *) &lsk.inka_nr,SQLLONG,0);
    sqlout ((TCHAR *) lsk.auf_ext,SQLCHAR,17);
    sqlout ((long *) &lsk.teil_smt,SQLLONG,0);
    sqlout ((TCHAR *) lsk.pers_nam,SQLCHAR,9);
    sqlout ((double *) &lsk.brutto,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &lsk.komm_dat,SQLDATE,0);
    sqlout ((double *) &lsk.of_ek,SQLDOUBLE,0);
    sqlout ((double *) &lsk.of_po_euro,SQLDOUBLE,0);
    sqlout ((double *) &lsk.of_po_fremd,SQLDOUBLE,0);
    sqlout ((double *) &lsk.of_ek_euro,SQLDOUBLE,0);
    sqlout ((double *) &lsk.of_ek_fremd,SQLDOUBLE,0);
    sqlout ((short *) &lsk.waehrung,SQLSHORT,0);
    sqlout ((TCHAR *) lsk.ueb_kz,SQLCHAR,2);
    sqlout ((double *) &lsk.gew,SQLDOUBLE,0);
    sqlout ((short *) &lsk.gruppe,SQLSHORT,0);
    sqlout ((short *) &lsk.ccmarkt,SQLSHORT,0);
    sqlout ((short *) &lsk.auf_art,SQLSHORT,0);
    sqlout ((short *) &lsk.fak_typ,SQLSHORT,0);
    sqlout ((long *) &lsk.tou_nr,SQLLONG,0);
    sqlout ((short *) &lsk.wieg_kompl,SQLSHORT,0);
    sqlout ((DATE_STRUCT *) &lsk.best_dat,SQLDATE,0);
    sqlout ((TCHAR *) lsk.hinweis2,SQLCHAR,31);
    sqlout ((TCHAR *) lsk.hinweis3,SQLCHAR,31);
    sqlout ((DATE_STRUCT *) &lsk.fix_dat,SQLDATE,0);
    sqlout ((TCHAR *) lsk.komm_name,SQLCHAR,13);
    sqlout ((short *) &lsk.psteuer_kz,SQLSHORT,0);
    sqlout ((short *) &lsk.rech_typ,SQLSHORT,0);
            cursor = sqlcursor (_T("select lsk.ls,  lsk.adr,  ")
_T("lsk.mdn,  lsk.auf,  lsk.kun_fil,  lsk.kun,  lsk.fil,  lsk.feld_bz1,  ")
_T("lsk.lieferdat,  lsk.lieferzeit,  lsk.hinweis,  lsk.ls_stat,  ")
_T("lsk.kun_krz1,  lsk.auf_sum,  lsk.feld_bz2,  lsk.lim_er,  lsk.partner,  ")
_T("lsk.pr_lst,  lsk.feld_bz3,  lsk.pr_stu,  lsk.vertr,  lsk.tou,  lsk.adr_nam1,  ")
_T("lsk.adr_nam2,  lsk.pf,  lsk.str,  lsk.plz,  lsk.ort1,  lsk.of_po,  ")
_T("lsk.delstatus,  lsk.rech,  lsk.blg_typ,  lsk.zeit_dec,  lsk.kopf_txt,  ")
_T("lsk.fuss_txt,  lsk.inka_nr,  lsk.auf_ext,  lsk.teil_smt,  lsk.pers_nam,  ")
_T("lsk.brutto,  lsk.komm_dat,  lsk.of_ek,  lsk.of_po_euro,  lsk.of_po_fremd,  ")
_T("lsk.of_ek_euro,  lsk.of_ek_fremd,  lsk.waehrung,  lsk.ueb_kz,  lsk.gew,  ")
_T("lsk.gruppe,  lsk.ccmarkt,  lsk.auf_art,  lsk.fak_typ,  lsk.tou_nr,  ")
_T("lsk.wieg_kompl,  lsk.best_dat,  lsk.hinweis2,  lsk.hinweis3,  ")
_T("lsk.fix_dat,  lsk.komm_name,  lsk.psteuer_kz,  lsk.rech_typ from lsk ")

#line 14 "lsk.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ls = ? ")
				);
    sqlin ((long *) &lsk.ls,SQLLONG,0);
    sqlin ((long *) &lsk.adr,SQLLONG,0);
    sqlin ((short *) &lsk.mdn,SQLSHORT,0);
    sqlin ((long *) &lsk.auf,SQLLONG,0);
    sqlin ((short *) &lsk.kun_fil,SQLSHORT,0);
    sqlin ((long *) &lsk.kun,SQLLONG,0);
    sqlin ((short *) &lsk.fil,SQLSHORT,0);
    sqlin ((TCHAR *) lsk.feld_bz1,SQLCHAR,20);
    sqlin ((DATE_STRUCT *) &lsk.lieferdat,SQLDATE,0);
    sqlin ((TCHAR *) lsk.lieferzeit,SQLCHAR,6);
    sqlin ((TCHAR *) lsk.hinweis,SQLCHAR,49);
    sqlin ((short *) &lsk.ls_stat,SQLSHORT,0);
    sqlin ((TCHAR *) lsk.kun_krz1,SQLCHAR,17);
    sqlin ((double *) &lsk.auf_sum,SQLDOUBLE,0);
    sqlin ((TCHAR *) lsk.feld_bz2,SQLCHAR,12);
    sqlin ((double *) &lsk.lim_er,SQLDOUBLE,0);
    sqlin ((TCHAR *) lsk.partner,SQLCHAR,37);
    sqlin ((long *) &lsk.pr_lst,SQLLONG,0);
    sqlin ((TCHAR *) lsk.feld_bz3,SQLCHAR,8);
    sqlin ((short *) &lsk.pr_stu,SQLSHORT,0);
    sqlin ((long *) &lsk.vertr,SQLLONG,0);
    sqlin ((long *) &lsk.tou,SQLLONG,0);
    sqlin ((TCHAR *) lsk.adr_nam1,SQLCHAR,37);
    sqlin ((TCHAR *) lsk.adr_nam2,SQLCHAR,37);
    sqlin ((TCHAR *) lsk.pf,SQLCHAR,17);
    sqlin ((TCHAR *) lsk.str,SQLCHAR,37);
    sqlin ((TCHAR *) lsk.plz,SQLCHAR,9);
    sqlin ((TCHAR *) lsk.ort1,SQLCHAR,37);
    sqlin ((double *) &lsk.of_po,SQLDOUBLE,0);
    sqlin ((short *) &lsk.delstatus,SQLSHORT,0);
    sqlin ((long *) &lsk.rech,SQLLONG,0);
    sqlin ((TCHAR *) lsk.blg_typ,SQLCHAR,2);
    sqlin ((double *) &lsk.zeit_dec,SQLDOUBLE,0);
    sqlin ((long *) &lsk.kopf_txt,SQLLONG,0);
    sqlin ((long *) &lsk.fuss_txt,SQLLONG,0);
    sqlin ((long *) &lsk.inka_nr,SQLLONG,0);
    sqlin ((TCHAR *) lsk.auf_ext,SQLCHAR,17);
    sqlin ((long *) &lsk.teil_smt,SQLLONG,0);
    sqlin ((TCHAR *) lsk.pers_nam,SQLCHAR,9);
    sqlin ((double *) &lsk.brutto,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &lsk.komm_dat,SQLDATE,0);
    sqlin ((double *) &lsk.of_ek,SQLDOUBLE,0);
    sqlin ((double *) &lsk.of_po_euro,SQLDOUBLE,0);
    sqlin ((double *) &lsk.of_po_fremd,SQLDOUBLE,0);
    sqlin ((double *) &lsk.of_ek_euro,SQLDOUBLE,0);
    sqlin ((double *) &lsk.of_ek_fremd,SQLDOUBLE,0);
    sqlin ((short *) &lsk.waehrung,SQLSHORT,0);
    sqlin ((TCHAR *) lsk.ueb_kz,SQLCHAR,2);
    sqlin ((double *) &lsk.gew,SQLDOUBLE,0);
    sqlin ((short *) &lsk.gruppe,SQLSHORT,0);
    sqlin ((short *) &lsk.ccmarkt,SQLSHORT,0);
    sqlin ((short *) &lsk.auf_art,SQLSHORT,0);
    sqlin ((short *) &lsk.fak_typ,SQLSHORT,0);
    sqlin ((long *) &lsk.tou_nr,SQLLONG,0);
    sqlin ((short *) &lsk.wieg_kompl,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &lsk.best_dat,SQLDATE,0);
    sqlin ((TCHAR *) lsk.hinweis2,SQLCHAR,31);
    sqlin ((TCHAR *) lsk.hinweis3,SQLCHAR,31);
    sqlin ((DATE_STRUCT *) &lsk.fix_dat,SQLDATE,0);
    sqlin ((TCHAR *) lsk.komm_name,SQLCHAR,13);
    sqlin ((short *) &lsk.psteuer_kz,SQLSHORT,0);
    sqlin ((short *) &lsk.rech_typ,SQLSHORT,0);
            sqltext = _T("update lsk set lsk.ls = ?,  ")
_T("lsk.adr = ?,  lsk.mdn = ?,  lsk.auf = ?,  lsk.kun_fil = ?,  lsk.kun = ?,  ")
_T("lsk.fil = ?,  lsk.feld_bz1 = ?,  lsk.lieferdat = ?,  ")
_T("lsk.lieferzeit = ?,  lsk.hinweis = ?,  lsk.ls_stat = ?,  ")
_T("lsk.kun_krz1 = ?,  lsk.auf_sum = ?,  lsk.feld_bz2 = ?,  ")
_T("lsk.lim_er = ?,  lsk.partner = ?,  lsk.pr_lst = ?,  lsk.feld_bz3 = ?,  ")
_T("lsk.pr_stu = ?,  lsk.vertr = ?,  lsk.tou = ?,  lsk.adr_nam1 = ?,  ")
_T("lsk.adr_nam2 = ?,  lsk.pf = ?,  lsk.str = ?,  lsk.plz = ?,  lsk.ort1 = ?,  ")
_T("lsk.of_po = ?,  lsk.delstatus = ?,  lsk.rech = ?,  lsk.blg_typ = ?,  ")
_T("lsk.zeit_dec = ?,  lsk.kopf_txt = ?,  lsk.fuss_txt = ?,  ")
_T("lsk.inka_nr = ?,  lsk.auf_ext = ?,  lsk.teil_smt = ?,  ")
_T("lsk.pers_nam = ?,  lsk.brutto = ?,  lsk.komm_dat = ?,  lsk.of_ek = ?,  ")
_T("lsk.of_po_euro = ?,  lsk.of_po_fremd = ?,  lsk.of_ek_euro = ?,  ")
_T("lsk.of_ek_fremd = ?,  lsk.waehrung = ?,  lsk.ueb_kz = ?,  lsk.gew = ?,  ")
_T("lsk.gruppe = ?,  lsk.ccmarkt = ?,  lsk.auf_art = ?,  lsk.fak_typ = ?,  ")
_T("lsk.tou_nr = ?,  lsk.wieg_kompl = ?,  lsk.best_dat = ?,  ")
_T("lsk.hinweis2 = ?,  lsk.hinweis3 = ?,  lsk.fix_dat = ?,  ")
_T("lsk.komm_name = ?,  lsk.psteuer_kz = ?,  lsk.rech_typ = ? ")

#line 19 "lsk.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ls = ? ");
            sqlin ((short *)   &lsk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lsk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &lsk.ls,   SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &lsk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lsk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &lsk.ls,   SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select ls from lsk ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ls = ? ")
				);
            sqlin ((short *)   &lsk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lsk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &lsk.ls,   SQLLONG, 0);
            test_lock_cursor = sqlcursor (_T("update lsk set delstatus = -1 ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ls = ? ")
				);
            sqlin ((short *)   &lsk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &lsk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &lsk.ls,   SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from lsk ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and ls = ? ")
				);
    sqlin ((long *) &lsk.ls,SQLLONG,0);
    sqlin ((long *) &lsk.adr,SQLLONG,0);
    sqlin ((short *) &lsk.mdn,SQLSHORT,0);
    sqlin ((long *) &lsk.auf,SQLLONG,0);
    sqlin ((short *) &lsk.kun_fil,SQLSHORT,0);
    sqlin ((long *) &lsk.kun,SQLLONG,0);
    sqlin ((short *) &lsk.fil,SQLSHORT,0);
    sqlin ((TCHAR *) lsk.feld_bz1,SQLCHAR,20);
    sqlin ((DATE_STRUCT *) &lsk.lieferdat,SQLDATE,0);
    sqlin ((TCHAR *) lsk.lieferzeit,SQLCHAR,6);
    sqlin ((TCHAR *) lsk.hinweis,SQLCHAR,49);
    sqlin ((short *) &lsk.ls_stat,SQLSHORT,0);
    sqlin ((TCHAR *) lsk.kun_krz1,SQLCHAR,17);
    sqlin ((double *) &lsk.auf_sum,SQLDOUBLE,0);
    sqlin ((TCHAR *) lsk.feld_bz2,SQLCHAR,12);
    sqlin ((double *) &lsk.lim_er,SQLDOUBLE,0);
    sqlin ((TCHAR *) lsk.partner,SQLCHAR,37);
    sqlin ((long *) &lsk.pr_lst,SQLLONG,0);
    sqlin ((TCHAR *) lsk.feld_bz3,SQLCHAR,8);
    sqlin ((short *) &lsk.pr_stu,SQLSHORT,0);
    sqlin ((long *) &lsk.vertr,SQLLONG,0);
    sqlin ((long *) &lsk.tou,SQLLONG,0);
    sqlin ((TCHAR *) lsk.adr_nam1,SQLCHAR,37);
    sqlin ((TCHAR *) lsk.adr_nam2,SQLCHAR,37);
    sqlin ((TCHAR *) lsk.pf,SQLCHAR,17);
    sqlin ((TCHAR *) lsk.str,SQLCHAR,37);
    sqlin ((TCHAR *) lsk.plz,SQLCHAR,9);
    sqlin ((TCHAR *) lsk.ort1,SQLCHAR,37);
    sqlin ((double *) &lsk.of_po,SQLDOUBLE,0);
    sqlin ((short *) &lsk.delstatus,SQLSHORT,0);
    sqlin ((long *) &lsk.rech,SQLLONG,0);
    sqlin ((TCHAR *) lsk.blg_typ,SQLCHAR,2);
    sqlin ((double *) &lsk.zeit_dec,SQLDOUBLE,0);
    sqlin ((long *) &lsk.kopf_txt,SQLLONG,0);
    sqlin ((long *) &lsk.fuss_txt,SQLLONG,0);
    sqlin ((long *) &lsk.inka_nr,SQLLONG,0);
    sqlin ((TCHAR *) lsk.auf_ext,SQLCHAR,17);
    sqlin ((long *) &lsk.teil_smt,SQLLONG,0);
    sqlin ((TCHAR *) lsk.pers_nam,SQLCHAR,9);
    sqlin ((double *) &lsk.brutto,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &lsk.komm_dat,SQLDATE,0);
    sqlin ((double *) &lsk.of_ek,SQLDOUBLE,0);
    sqlin ((double *) &lsk.of_po_euro,SQLDOUBLE,0);
    sqlin ((double *) &lsk.of_po_fremd,SQLDOUBLE,0);
    sqlin ((double *) &lsk.of_ek_euro,SQLDOUBLE,0);
    sqlin ((double *) &lsk.of_ek_fremd,SQLDOUBLE,0);
    sqlin ((short *) &lsk.waehrung,SQLSHORT,0);
    sqlin ((TCHAR *) lsk.ueb_kz,SQLCHAR,2);
    sqlin ((double *) &lsk.gew,SQLDOUBLE,0);
    sqlin ((short *) &lsk.gruppe,SQLSHORT,0);
    sqlin ((short *) &lsk.ccmarkt,SQLSHORT,0);
    sqlin ((short *) &lsk.auf_art,SQLSHORT,0);
    sqlin ((short *) &lsk.fak_typ,SQLSHORT,0);
    sqlin ((long *) &lsk.tou_nr,SQLLONG,0);
    sqlin ((short *) &lsk.wieg_kompl,SQLSHORT,0);
    sqlin ((DATE_STRUCT *) &lsk.best_dat,SQLDATE,0);
    sqlin ((TCHAR *) lsk.hinweis2,SQLCHAR,31);
    sqlin ((TCHAR *) lsk.hinweis3,SQLCHAR,31);
    sqlin ((DATE_STRUCT *) &lsk.fix_dat,SQLDATE,0);
    sqlin ((TCHAR *) lsk.komm_name,SQLCHAR,13);
    sqlin ((short *) &lsk.psteuer_kz,SQLSHORT,0);
    sqlin ((short *) &lsk.rech_typ,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into lsk (ls,  ")
_T("adr,  mdn,  auf,  kun_fil,  kun,  fil,  feld_bz1,  lieferdat,  lieferzeit,  hinweis,  ")
_T("ls_stat,  kun_krz1,  auf_sum,  feld_bz2,  lim_er,  partner,  pr_lst,  feld_bz3,  ")
_T("pr_stu,  vertr,  tou,  adr_nam1,  adr_nam2,  pf,  str,  plz,  ort1,  of_po,  delstatus,  rech,  ")
_T("blg_typ,  zeit_dec,  kopf_txt,  fuss_txt,  inka_nr,  auf_ext,  teil_smt,  ")
_T("pers_nam,  brutto,  komm_dat,  of_ek,  of_po_euro,  of_po_fremd,  of_ek_euro,  ")
_T("of_ek_fremd,  waehrung,  ueb_kz,  gew,  gruppe,  ccmarkt,  auf_art,  fak_typ,  ")
_T("tou_nr,  wieg_kompl,  best_dat,  hinweis2,  hinweis3,  fix_dat,  komm_name,  ")
_T("psteuer_kz,  rech_typ) ")

#line 52 "lsk.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 54 "lsk.rpp"
}
