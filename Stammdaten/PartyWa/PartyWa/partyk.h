#ifndef _PARTYK_DEF
#define _PARTYK_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct PARTYK {
   short          mdn;
   short          fil;
   long           ang;
   long           auf;
   long           ls;
   long           rech;
   long           adr;
   short          kun_fil;
   long           kun;
   DATE_STRUCT    lieferdat;
   TCHAR          lieferzeit[6];
   TCHAR          hinweis[513];
   short          ang_stat;
   TCHAR          kun_krz1[17];
   TCHAR          feld_bz1[20];
   TCHAR          feld_bz2[12];
   TCHAR          feld_bz3[8];
   short          delstatus;
   double         zeit_dec;
   long           kopf_txt;
   long           fuss_txt;
   long           vertr;
   TCHAR          auf_ext[17];
   long           tou;
   TCHAR          pers_nam[9];
   DATE_STRUCT    komm_dat;
   DATE_STRUCT    best_dat;
   short          waehrung;
   short          auf_art;
   short          gruppe;
   short          ccmarkt;
   short          fak_typ;
   long           tou_nr;
   TCHAR          ueb_kz[2];
   DATE_STRUCT    fix_dat;
   TCHAR          komm_name[13];
   DATE_STRUCT    akv;
   TCHAR          akv_zeit[6];
   DATE_STRUCT    bearb;
   TCHAR          bearb_zeit[6];
   short          anz_pers;
   short          einzel_preis;
   short          nur_servicepreis;
   short          psteuer_kz;
   double         ang_gew;
   double         ang_vk;
   double         pauschal_vk;
   double         einzel_vk;
   double         einzel_vk_pausch;
   short          mwst_voll;
   double         mwst_wert;
   double         mwst_proz;
   short          typ;
   TCHAR          kommang[257];
   TCHAR          kommrech[257];
   TCHAR          kommintern[257];
   TCHAR          anlass[257];
   TCHAR          pers[13];
   TCHAR          pers_name[17];
   short          abholung;
   short          hide_service;
   short          hide_me;
   double         service_pausch_vk;
   long           kommangnr;
   long           kommrechnr;
   long           komminternnr;
   long           lief_adr;
};
extern struct PARTYK partyk, partyk_null;

#line 8 "partyk.rh"

class PARTYK_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PARTYK partyk;  
               PARTYK_CLASS () : DB_CLASS ()
               {
               }
};
#endif
