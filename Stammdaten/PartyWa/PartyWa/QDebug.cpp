#include "StdAfx.h"
#include "QDebug.h"

CQDebug *CQDebug::instance = NULL;


CQDebug *CQDebug::GetInstance ()
{
	if (instance == NULL)
	{
		instance = new CQDebug ();
	}
	return instance;
}

void CQDebug::DeleteInstance ()
{
	if (instance != NULL)
	{
		instance->close ();
		delete instance;
		instance = NULL;
	}
}

CQDebug::CQDebug(void)
{
	msFileName = _T("");
}

CQDebug::~CQDebug(void)
{
}

bool CQDebug::open ()
{
	if (msFileName == _T(""))
	{
		return false;
	}
	if (mfspOut.is_open ())
	{
		mfspOut.close ();
	}
	mfspOut.open (msFileName.c_str (), ios_base::out);
	if (!mfspOut.is_open ())
	{
		return false;
	}
	return true;
}

bool CQDebug::write ()
{
	bool ret = true;
	if (!mfspOut.is_open ())
	{
		ret = open ();
	}
	if (ret)
	{
		mfspOut << msLine << endl;
		mfspOut.flush ();
	}
	return ret;
}

bool CQDebug::write (LPTSTR Line)
{
	msLine = Line;
	return write ();
}

bool CQDebug::write (LPTSTR FileName, LPTSTR Line)
{
	bool ret = true;

	if (msFileName == _T ("") || FileName != msFileName)
	{
		msFileName = FileName;
		ret = open ();
	}
	if (ret)
	{
		msLine = Line;
		ret = write ();
	}
	return ret;
}

void CQDebug::close ()
{
	if (mfspOut.is_open ())
	{
		mfspOut.close ();
	}
}



