#include "stdafx.h"
#include "partyp.h"

struct PARTYP partyp, partyp_null;

void PARTYP_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((long *)   &partyp.row_id,   SQLLONG, 0);
    sqlout ((long *) &partyp.row_id,SQLLONG,0);
    sqlout ((short *) &partyp.mdn,SQLSHORT,0);
    sqlout ((short *) &partyp.fil,SQLSHORT,0);
    sqlout ((long *) &partyp.ang,SQLLONG,0);
    sqlout ((long *) &partyp.auf,SQLLONG,0);
    sqlout ((long *) &partyp.ls,SQLLONG,0);
    sqlout ((long *) &partyp.posi,SQLLONG,0);
    sqlout ((long *) &partyp.aufp_txt,SQLLONG,0);
    sqlout ((double *) &partyp.a,SQLDOUBLE,0);
    sqlout ((double *) &partyp.auf_me,SQLDOUBLE,0);
    sqlout ((TCHAR *) partyp.auf_me_bz,SQLCHAR,7);
    sqlout ((double *) &partyp.lief_me,SQLDOUBLE,0);
    sqlout ((TCHAR *) partyp.lief_me_bz,SQLCHAR,7);
    sqlout ((double *) &partyp.auf_vk_pr,SQLDOUBLE,0);
    sqlout ((double *) &partyp.auf_lad_pr,SQLDOUBLE,0);
    sqlout ((short *) &partyp.delstatus,SQLSHORT,0);
    sqlout ((short *) &partyp.sa_kz_sint,SQLSHORT,0);
    sqlout ((double *) &partyp.prov_satz,SQLDOUBLE,0);
    sqlout ((long *) &partyp.ksys,SQLLONG,0);
    sqlout ((long *) &partyp.pid,SQLLONG,0);
    sqlout ((long *) &partyp.auf_klst,SQLLONG,0);
    sqlout ((short *) &partyp.teil_smt,SQLSHORT,0);
    sqlout ((short *) &partyp.dr_folge,SQLSHORT,0);
    sqlout ((double *) &partyp.inh,SQLDOUBLE,0);
    sqlout ((double *) &partyp.auf_vk_euro,SQLDOUBLE,0);
    sqlout ((double *) &partyp.auf_vk_fremd,SQLDOUBLE,0);
    sqlout ((double *) &partyp.auf_lad_euro,SQLDOUBLE,0);
    sqlout ((double *) &partyp.auf_lad_fremd,SQLDOUBLE,0);
    sqlout ((double *) &partyp.rab_satz,SQLDOUBLE,0);
    sqlout ((short *) &partyp.me_einh_kun,SQLSHORT,0);
    sqlout ((short *) &partyp.me_einh,SQLSHORT,0);
    sqlout ((short *) &partyp.me_einh_kun1,SQLSHORT,0);
    sqlout ((double *) &partyp.auf_me1,SQLDOUBLE,0);
    sqlout ((double *) &partyp.inh1,SQLDOUBLE,0);
    sqlout ((short *) &partyp.me_einh_kun2,SQLSHORT,0);
    sqlout ((double *) &partyp.auf_me2,SQLDOUBLE,0);
    sqlout ((double *) &partyp.inh2,SQLDOUBLE,0);
    sqlout ((short *) &partyp.me_einh_kun3,SQLSHORT,0);
    sqlout ((double *) &partyp.auf_me3,SQLDOUBLE,0);
    sqlout ((double *) &partyp.inh3,SQLDOUBLE,0);
    sqlout ((long *) &partyp.gruppe,SQLLONG,0);
    sqlout ((TCHAR *) partyp.kond_art,SQLCHAR,5);
    sqlout ((double *) &partyp.a_grund,SQLDOUBLE,0);
    sqlout ((short *) &partyp.ls_pos_kz,SQLSHORT,0);
    sqlout ((long *) &partyp.posi_ext,SQLLONG,0);
    sqlout ((long *) &partyp.kun,SQLLONG,0);
    sqlout ((double *) &partyp.a_ers,SQLDOUBLE,0);
    sqlout ((TCHAR *) partyp.ls_charge,SQLCHAR,31);
    sqlout ((short *) &partyp.aufschlag,SQLSHORT,0);
    sqlout ((double *) &partyp.aufschlag_wert,SQLDOUBLE,0);
    sqlout ((double *) &partyp.pos_vk,SQLDOUBLE,0);
    sqlout ((double *) &partyp.pos_gew,SQLDOUBLE,0);
    sqlout ((short *) &partyp.mwst,SQLSHORT,0);
    sqlout ((double *) &partyp.auf_vk_brutto,SQLDOUBLE,0);
    sqlout ((TCHAR *) partyp.gebinde_text,SQLCHAR,37);
    sqlout ((double *) &partyp.a_gew,SQLDOUBLE,0);
    sqlout ((short *) &partyp.pos_txt_kz,SQLSHORT,0);
    sqlout ((double *) &partyp.pos_vk_nto,SQLDOUBLE,0);
    sqlout ((short *) &partyp.preinzel_kz,SQLSHORT,0);
            cursor = sqlcursor (_T("select partyp.row_id,  ")
_T("partyp.mdn,  partyp.fil,  partyp.ang,  partyp.auf,  partyp.ls,  partyp.posi,  ")
_T("partyp.aufp_txt,  partyp.a,  partyp.auf_me,  partyp.auf_me_bz,  ")
_T("partyp.lief_me,  partyp.lief_me_bz,  partyp.auf_vk_pr,  ")
_T("partyp.auf_lad_pr,  partyp.delstatus,  partyp.sa_kz_sint,  ")
_T("partyp.prov_satz,  partyp.ksys,  partyp.pid,  partyp.auf_klst,  ")
_T("partyp.teil_smt,  partyp.dr_folge,  partyp.inh,  partyp.auf_vk_euro,  ")
_T("partyp.auf_vk_fremd,  partyp.auf_lad_euro,  partyp.auf_lad_fremd,  ")
_T("partyp.rab_satz,  partyp.me_einh_kun,  partyp.me_einh,  ")
_T("partyp.me_einh_kun1,  partyp.auf_me1,  partyp.inh1,  ")
_T("partyp.me_einh_kun2,  partyp.auf_me2,  partyp.inh2,  ")
_T("partyp.me_einh_kun3,  partyp.auf_me3,  partyp.inh3,  partyp.gruppe,  ")
_T("partyp.kond_art,  partyp.a_grund,  partyp.ls_pos_kz,  partyp.posi_ext,  ")
_T("partyp.kun,  partyp.a_ers,  partyp.ls_charge,  partyp.aufschlag,  ")
_T("partyp.aufschlag_wert,  partyp.pos_vk,  partyp.pos_gew,  partyp.mwst,  ")
_T("partyp.auf_vk_brutto,  partyp.gebinde_text,  partyp.a_gew,  ")
_T("partyp.pos_txt_kz,  partyp.pos_vk_nto,  partyp.preinzel_kz from partyp ")

#line 12 "partyp.rpp"
                                  _T("where row_id = ? ")
				);
    sqlin ((long *) &partyp.row_id,SQLLONG,0);
    sqlin ((short *) &partyp.mdn,SQLSHORT,0);
    sqlin ((short *) &partyp.fil,SQLSHORT,0);
    sqlin ((long *) &partyp.ang,SQLLONG,0);
    sqlin ((long *) &partyp.auf,SQLLONG,0);
    sqlin ((long *) &partyp.ls,SQLLONG,0);
    sqlin ((long *) &partyp.posi,SQLLONG,0);
    sqlin ((long *) &partyp.aufp_txt,SQLLONG,0);
    sqlin ((double *) &partyp.a,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_me,SQLDOUBLE,0);
    sqlin ((TCHAR *) partyp.auf_me_bz,SQLCHAR,7);
    sqlin ((double *) &partyp.lief_me,SQLDOUBLE,0);
    sqlin ((TCHAR *) partyp.lief_me_bz,SQLCHAR,7);
    sqlin ((double *) &partyp.auf_vk_pr,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_lad_pr,SQLDOUBLE,0);
    sqlin ((short *) &partyp.delstatus,SQLSHORT,0);
    sqlin ((short *) &partyp.sa_kz_sint,SQLSHORT,0);
    sqlin ((double *) &partyp.prov_satz,SQLDOUBLE,0);
    sqlin ((long *) &partyp.ksys,SQLLONG,0);
    sqlin ((long *) &partyp.pid,SQLLONG,0);
    sqlin ((long *) &partyp.auf_klst,SQLLONG,0);
    sqlin ((short *) &partyp.teil_smt,SQLSHORT,0);
    sqlin ((short *) &partyp.dr_folge,SQLSHORT,0);
    sqlin ((double *) &partyp.inh,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_vk_euro,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_vk_fremd,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_lad_euro,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_lad_fremd,SQLDOUBLE,0);
    sqlin ((double *) &partyp.rab_satz,SQLDOUBLE,0);
    sqlin ((short *) &partyp.me_einh_kun,SQLSHORT,0);
    sqlin ((short *) &partyp.me_einh,SQLSHORT,0);
    sqlin ((short *) &partyp.me_einh_kun1,SQLSHORT,0);
    sqlin ((double *) &partyp.auf_me1,SQLDOUBLE,0);
    sqlin ((double *) &partyp.inh1,SQLDOUBLE,0);
    sqlin ((short *) &partyp.me_einh_kun2,SQLSHORT,0);
    sqlin ((double *) &partyp.auf_me2,SQLDOUBLE,0);
    sqlin ((double *) &partyp.inh2,SQLDOUBLE,0);
    sqlin ((short *) &partyp.me_einh_kun3,SQLSHORT,0);
    sqlin ((double *) &partyp.auf_me3,SQLDOUBLE,0);
    sqlin ((double *) &partyp.inh3,SQLDOUBLE,0);
    sqlin ((long *) &partyp.gruppe,SQLLONG,0);
    sqlin ((TCHAR *) partyp.kond_art,SQLCHAR,5);
    sqlin ((double *) &partyp.a_grund,SQLDOUBLE,0);
    sqlin ((short *) &partyp.ls_pos_kz,SQLSHORT,0);
    sqlin ((long *) &partyp.posi_ext,SQLLONG,0);
    sqlin ((long *) &partyp.kun,SQLLONG,0);
    sqlin ((double *) &partyp.a_ers,SQLDOUBLE,0);
    sqlin ((TCHAR *) partyp.ls_charge,SQLCHAR,31);
    sqlin ((short *) &partyp.aufschlag,SQLSHORT,0);
    sqlin ((double *) &partyp.aufschlag_wert,SQLDOUBLE,0);
    sqlin ((double *) &partyp.pos_vk,SQLDOUBLE,0);
    sqlin ((double *) &partyp.pos_gew,SQLDOUBLE,0);
    sqlin ((short *) &partyp.mwst,SQLSHORT,0);
    sqlin ((double *) &partyp.auf_vk_brutto,SQLDOUBLE,0);
    sqlin ((TCHAR *) partyp.gebinde_text,SQLCHAR,37);
    sqlin ((double *) &partyp.a_gew,SQLDOUBLE,0);
    sqlin ((short *) &partyp.pos_txt_kz,SQLSHORT,0);
    sqlin ((double *) &partyp.pos_vk_nto,SQLDOUBLE,0);
    sqlin ((short *) &partyp.preinzel_kz,SQLSHORT,0);
            sqltext = _T("update partyp set ")
_T("partyp.row_id = ?,  partyp.mdn = ?,  partyp.fil = ?,  partyp.ang = ?,  ")
_T("partyp.auf = ?,  partyp.ls = ?,  partyp.posi = ?,  ")
_T("partyp.aufp_txt = ?,  partyp.a = ?,  partyp.auf_me = ?,  ")
_T("partyp.auf_me_bz = ?,  partyp.lief_me = ?,  partyp.lief_me_bz = ?,  ")
_T("partyp.auf_vk_pr = ?,  partyp.auf_lad_pr = ?,  ")
_T("partyp.delstatus = ?,  partyp.sa_kz_sint = ?,  ")
_T("partyp.prov_satz = ?,  partyp.ksys = ?,  partyp.pid = ?,  ")
_T("partyp.auf_klst = ?,  partyp.teil_smt = ?,  partyp.dr_folge = ?,  ")
_T("partyp.inh = ?,  partyp.auf_vk_euro = ?,  partyp.auf_vk_fremd = ?,  ")
_T("partyp.auf_lad_euro = ?,  partyp.auf_lad_fremd = ?,  ")
_T("partyp.rab_satz = ?,  partyp.me_einh_kun = ?,  partyp.me_einh = ?,  ")
_T("partyp.me_einh_kun1 = ?,  partyp.auf_me1 = ?,  partyp.inh1 = ?,  ")
_T("partyp.me_einh_kun2 = ?,  partyp.auf_me2 = ?,  partyp.inh2 = ?,  ")
_T("partyp.me_einh_kun3 = ?,  partyp.auf_me3 = ?,  partyp.inh3 = ?,  ")
_T("partyp.gruppe = ?,  partyp.kond_art = ?,  partyp.a_grund = ?,  ")
_T("partyp.ls_pos_kz = ?,  partyp.posi_ext = ?,  partyp.kun = ?,  ")
_T("partyp.a_ers = ?,  partyp.ls_charge = ?,  partyp.aufschlag = ?,  ")
_T("partyp.aufschlag_wert = ?,  partyp.pos_vk = ?,  partyp.pos_gew = ?,  ")
_T("partyp.mwst = ?,  partyp.auf_vk_brutto = ?,  ")
_T("partyp.gebinde_text = ?,  partyp.a_gew = ?,  partyp.pos_txt_kz = ?,  ")
_T("partyp.pos_vk_nto = ?,  partyp.preinzel_kz = ? ")

#line 15 "partyp.rpp"
                                  _T("where row_id = ? ");
            sqlin ((long *)   &partyp.row_id,   SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((long *)   &partyp.row_id,   SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select a from partyp ")
                                  _T("where row_id = ? "));
            sqlin ((long *)   &partyp.row_id,   SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from partyp ")
                                  _T("where row_id = ? ")
				);
    sqlin ((long *) &partyp.row_id,SQLLONG,0);
    sqlin ((short *) &partyp.mdn,SQLSHORT,0);
    sqlin ((short *) &partyp.fil,SQLSHORT,0);
    sqlin ((long *) &partyp.ang,SQLLONG,0);
    sqlin ((long *) &partyp.auf,SQLLONG,0);
    sqlin ((long *) &partyp.ls,SQLLONG,0);
    sqlin ((long *) &partyp.posi,SQLLONG,0);
    sqlin ((long *) &partyp.aufp_txt,SQLLONG,0);
    sqlin ((double *) &partyp.a,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_me,SQLDOUBLE,0);
    sqlin ((TCHAR *) partyp.auf_me_bz,SQLCHAR,7);
    sqlin ((double *) &partyp.lief_me,SQLDOUBLE,0);
    sqlin ((TCHAR *) partyp.lief_me_bz,SQLCHAR,7);
    sqlin ((double *) &partyp.auf_vk_pr,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_lad_pr,SQLDOUBLE,0);
    sqlin ((short *) &partyp.delstatus,SQLSHORT,0);
    sqlin ((short *) &partyp.sa_kz_sint,SQLSHORT,0);
    sqlin ((double *) &partyp.prov_satz,SQLDOUBLE,0);
    sqlin ((long *) &partyp.ksys,SQLLONG,0);
    sqlin ((long *) &partyp.pid,SQLLONG,0);
    sqlin ((long *) &partyp.auf_klst,SQLLONG,0);
    sqlin ((short *) &partyp.teil_smt,SQLSHORT,0);
    sqlin ((short *) &partyp.dr_folge,SQLSHORT,0);
    sqlin ((double *) &partyp.inh,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_vk_euro,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_vk_fremd,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_lad_euro,SQLDOUBLE,0);
    sqlin ((double *) &partyp.auf_lad_fremd,SQLDOUBLE,0);
    sqlin ((double *) &partyp.rab_satz,SQLDOUBLE,0);
    sqlin ((short *) &partyp.me_einh_kun,SQLSHORT,0);
    sqlin ((short *) &partyp.me_einh,SQLSHORT,0);
    sqlin ((short *) &partyp.me_einh_kun1,SQLSHORT,0);
    sqlin ((double *) &partyp.auf_me1,SQLDOUBLE,0);
    sqlin ((double *) &partyp.inh1,SQLDOUBLE,0);
    sqlin ((short *) &partyp.me_einh_kun2,SQLSHORT,0);
    sqlin ((double *) &partyp.auf_me2,SQLDOUBLE,0);
    sqlin ((double *) &partyp.inh2,SQLDOUBLE,0);
    sqlin ((short *) &partyp.me_einh_kun3,SQLSHORT,0);
    sqlin ((double *) &partyp.auf_me3,SQLDOUBLE,0);
    sqlin ((double *) &partyp.inh3,SQLDOUBLE,0);
    sqlin ((long *) &partyp.gruppe,SQLLONG,0);
    sqlin ((TCHAR *) partyp.kond_art,SQLCHAR,5);
    sqlin ((double *) &partyp.a_grund,SQLDOUBLE,0);
    sqlin ((short *) &partyp.ls_pos_kz,SQLSHORT,0);
    sqlin ((long *) &partyp.posi_ext,SQLLONG,0);
    sqlin ((long *) &partyp.kun,SQLLONG,0);
    sqlin ((double *) &partyp.a_ers,SQLDOUBLE,0);
    sqlin ((TCHAR *) partyp.ls_charge,SQLCHAR,31);
    sqlin ((short *) &partyp.aufschlag,SQLSHORT,0);
    sqlin ((double *) &partyp.aufschlag_wert,SQLDOUBLE,0);
    sqlin ((double *) &partyp.pos_vk,SQLDOUBLE,0);
    sqlin ((double *) &partyp.pos_gew,SQLDOUBLE,0);
    sqlin ((short *) &partyp.mwst,SQLSHORT,0);
    sqlin ((double *) &partyp.auf_vk_brutto,SQLDOUBLE,0);
    sqlin ((TCHAR *) partyp.gebinde_text,SQLCHAR,37);
    sqlin ((double *) &partyp.a_gew,SQLDOUBLE,0);
    sqlin ((short *) &partyp.pos_txt_kz,SQLSHORT,0);
    sqlin ((double *) &partyp.pos_vk_nto,SQLDOUBLE,0);
    sqlin ((short *) &partyp.preinzel_kz,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into partyp (")
_T("row_id,  mdn,  fil,  ang,  auf,  ls,  posi,  aufp_txt,  a,  auf_me,  auf_me_bz,  lief_me,  ")
_T("lief_me_bz,  auf_vk_pr,  auf_lad_pr,  delstatus,  sa_kz_sint,  prov_satz,  ")
_T("ksys,  pid,  auf_klst,  teil_smt,  dr_folge,  inh,  auf_vk_euro,  auf_vk_fremd,  ")
_T("auf_lad_euro,  auf_lad_fremd,  rab_satz,  me_einh_kun,  me_einh,  ")
_T("me_einh_kun1,  auf_me1,  inh1,  me_einh_kun2,  auf_me2,  inh2,  me_einh_kun3,  ")
_T("auf_me3,  inh3,  gruppe,  kond_art,  a_grund,  ls_pos_kz,  posi_ext,  kun,  a_ers,  ")
_T("ls_charge,  aufschlag,  aufschlag_wert,  pos_vk,  pos_gew,  mwst,  ")
_T("auf_vk_brutto,  gebinde_text,  a_gew,  pos_txt_kz,  pos_vk_nto,  ")
_T("preinzel_kz) ")

#line 27 "partyp.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 29 "partyp.rpp"
            test_count_cursor =sqlcursor (_T("select * from partyp")); 
            sqlout ((long *) &row_id, SQLLONG, 0); 
            row_id_cursor = sqlcursor (_T("select max (row_id) from partyp"));
}

long PARTYP_CLASS::NextRowId ()
{
            sqlopen (test_count_cursor);
            if (sqlfetch (test_count_cursor) != 0)
            {
                    row_id = (long) 1;
                    return row_id;
            }
	    sqlopen (row_id_cursor);
            if (sqlfetch (row_id_cursor) == 0)
            {
		return row_id + 1;
            }
            return 0;
}

int PARTYP_CLASS::dbupdate (void)
/**
Tabelle eti Updaten.
**/
{
         int dsqlstatus = 0;

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         if (partyp.row_id == 0)
         {
             int tries = 0; 
	     partyp.row_id = NextRowId ();
             while (partyp.row_id == 0l)
             {
                   if (tries > 200)
                   {
                       return -1; 
                   }
                   Sleep (10);
 	           partyp.row_id = NextRowId ();
                   tries ++;
             } 
             dsqlstatus = sqlexecute (ins_cursor);
         }
         else
         {
             dsqlstatus = sqlexecute (upd_cursor);
         }   
         return dsqlstatus;
}
    	
                
 	
	    
