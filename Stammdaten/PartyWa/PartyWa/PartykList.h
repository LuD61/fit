#ifndef _PARTYK_LIST_DEF
#define _PARTY_LIST_DEF
#pragma once
#include <odbcinst.h>

class CPartykList
{
public:
	short mdn;
	long ang;
	long auf;
	long ls;
	long rech;
	long kun;
	TCHAR lieferdat[12];
	short ang_stat;
	short typ;
	long inka_nr;
	long tou_nr;
	short ang_tsat;
	TCHAR hinweis[49];
	TCHAR kun_krz1[17];
	TCHAR kun_bran2[3];
	short anz_pers;

	CPartykList(void);
	CPartykList(short mdn, long ang, long auf, long kun, DATE_STRUCT lieferdat, short ang_stat,
		     LPTSTR kun_krz1, LPTSTR kun_bran2);
	CPartykList(short mdn, long ang, long auf, long kun, TCHAR lieferdat[], short ang_stat,
		     LPTSTR kun_krz1, LPTSTR kun_bran2);
	~CPartykList(void);
	void SetLieferdat (LPTSTR p);
	void SetLieferdat (CString& ld);
	void SetKunKrz1 (LPTSTR kun_krz1);
	void SetKunBran2 (LPTSTR kun_bran2);
	void SetKunKrz1 (CString kun_krz1);
	void SetKunBran2 (CString kun_bran2);
	void SetHinweis (LPTSTR hinweis);
	void SetHinweis (CString hinweis);
};
#endif
