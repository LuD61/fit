#include "StdAfx.h"
#include "PartykList.h"

CPartykList::CPartykList(void)
{
	mdn = 0;
	ang = 0l;
	auf = 0l;
	ls = 0l;
	rech = 0l;
	kun = 0l;
	ang_stat = 0;
	inka_nr = 0l;
	tou_nr = 0l;
	hinweis[0] = 0;
	kun_krz1[0] = 0;
	kun_bran2[0] = 0;
}

CPartykList::CPartykList(short mdn, long ang, long auf, long kun, DATE_STRUCT lieferdat, short ang_stat,
				   LPTSTR kun_krz1, LPTSTR kun_bran2)
{
	this->mdn = mdn;
	this->ang = ang;
	this->auf = auf;
	this->kun = kun;
	_stprintf (this->lieferdat, "%02hd.%02hd.%04hd", lieferdat.day, lieferdat.month, lieferdat.year);
	this->ang_stat = ang_stat;
	_tcsncpy (this->kun_krz1, kun_krz1, 16);
    this->kun_krz1[16] = 0;   
	_tcsncpy (this->kun_bran2, kun_bran2, 2);
	this->kun_bran2[2] = 0;
	inka_nr = 0l;
	tou_nr = 0l;
	hinweis[0] = 0;
}

CPartykList::CPartykList(short mdn, long ang, long auf, long kun, TCHAR lieferdat[], short ang_stat,
				   LPTSTR kun_krz1, LPTSTR kun_bran2)
{
    this->mdn = mdn;
	this->ang = ang;
	this->auf = auf;
	this->kun = kun;
	_tcscpy (this->lieferdat, lieferdat);
	this->ang_stat = ang_stat;
	_tcsncpy (this->kun_krz1, kun_krz1, 16);
    this->kun_krz1[16] = 0;   
	_tcsncpy (this->kun_bran2, kun_bran2, 2);
	this->kun_bran2[2] = 0;
	inka_nr = 0l;
	tou_nr = 0l;
	hinweis[0] = 0;
}

CPartykList::~CPartykList(void)
{
}

void CPartykList::SetLieferdat (LPTSTR p)
{
	strcpy (this->lieferdat, lieferdat);
}

void CPartykList::SetLieferdat (CString& ld)
{
	SetLieferdat (ld.GetBuffer ());
}

void CPartykList::SetKunKrz1 (LPTSTR kun_krz1)
{
	_tcsncpy (this->kun_krz1, kun_krz1, 16);
    this->kun_krz1[16] = 0;   
}

void CPartykList::SetKunBran2 (LPTSTR kun_bran2)
{
	_tcsncpy (this->kun_bran2, kun_bran2, 2);
	this->kun_bran2[2] = 0;
}

void CPartykList::SetHinweis (LPTSTR hinweis)
{
	_tcsncpy (this->hinweis, hinweis, 48);
	this->hinweis[48] = 0;
}

void CPartykList::SetKunKrz1 (CString kun_krz1)
{
	_tcsncpy (this->kun_krz1, kun_krz1.GetBuffer (), 16);
    this->kun_krz1[16] = 0;   
}

void CPartykList::SetKunBran2 (CString kun_bran2)
{
	_tcsncpy (this->kun_bran2, kun_bran2.GetBuffer (), 2);
	this->kun_bran2[2] = 0;
}

void CPartykList::SetHinweis (CString hinweis)
{
	_tcsncpy (this->hinweis, hinweis.GetBuffer (), 48);
	this->hinweis[48] = 0;
}

