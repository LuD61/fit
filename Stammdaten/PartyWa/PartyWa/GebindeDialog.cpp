#include "StdAfx.h"
#include "GebindeDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC(CGebindeDialog, CDialog)

CGebindeDialog::CGebindeDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CGebindeDialog::IDD, pParent)
{
	Label = _T("Gebinde");
	Text = _T("");
	length = 0;
	rect.top = 0;
	rect.bottom = 0;
	rect.left = 0;
	rect.right = 0;
	BreakKey = For;
}

CGebindeDialog::~CGebindeDialog(void)
{
}

void CGebindeDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LGEBINDE, m_LGebinde);
	DDX_Control(pDX, IDC_GEBINDE, m_Gebinde);
}

BOOL CGebindeDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_LGebinde.SetWindowText (Label);
	m_Gebinde.SetWindowText (Text);
	if (length != 0)
	{
		m_Gebinde.SetLimitText (length);
	}

	CRect dlgRect;
	CRect parentRect;
	if (rect.right != 0)
	{
		CWnd *parent = GetParent ();
		if (parent != NULL)
		{
			parent->GetWindowRect (parentRect);
		}
		GetWindowRect (dlgRect);
		int cx = dlgRect.right - dlgRect.left;
		int cy = dlgRect.bottom - dlgRect.top;
		dlgRect.left = rect.left;
		dlgRect.top  = rect.top;
		dlgRect.right = dlgRect.left + cx;
		dlgRect.bottom = dlgRect.top + cy;
		if (parent != NULL)
		{
			if (dlgRect.right > parentRect.right - 10)
			{
				int diff = dlgRect.right - parentRect.right + 10;
				dlgRect.left -= diff;
				dlgRect.right -= diff;
			}
		}
		MoveWindow (&dlgRect);
	}

	return TRUE;
}

BOOL CGebindeDialog::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_DOWN)
			{
				BreakKey = Down;
				OnOK ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_UP)
			{
				BreakKey = Up;
				OnOK ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_TAB)
			{
				if (GetKeyState (VK_SHIFT) < 0)
				{
					BreakKey = Back;
				}
				OnOK ();
				return TRUE;
			}
	}
	return CDialog::PreTranslateMessage (pMsg);
}

void CGebindeDialog::OnOK ()
{
	m_Gebinde.GetWindowText (Text);
	CDialog::OnOK ();
}

