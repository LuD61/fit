// NewAngDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "PartyWa.h"
#include "NewAngDialog.h"


// CNewAngDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CNewAngDialog, CDialog)

CNewAngDialog::CNewAngDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CNewAngDialog::IDD, pParent)
{
	Name = _T("");

}

CNewAngDialog::~CNewAngDialog()
{
}

void CNewAngDialog::OnOK ()
{
	m_NewAng.GetWindowText (Name); 
	CDialog::OnOK ();
}

void CNewAngDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NEW_ANG, m_NewAng);
}


BEGIN_MESSAGE_MAP(CNewAngDialog, CDialog)
END_MESSAGE_MAP()


// CNewAngDialog-Meldungshandler
