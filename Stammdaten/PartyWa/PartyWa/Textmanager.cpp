#include "StdAfx.h"
#include "Textmanager.h"

CTextmanager::CTextmanager(void)
{
	cursor = -1; 
	memcpy (&m_Textpool.textpool, &textpool_null, sizeof (TEXTPOOL));
}

CTextmanager::~CTextmanager(void)
{
	if (cursor != -1)
	{
		m_Textpool.sqlclose (cursor);
	}
}

long CTextmanager::CreateNumber ()
{
	BOOL ret = FALSE;
	int dsqlstatus = 0;

	m_Nr = 0l;
	if (cursor == -1)
	{
		m_Textpool.sqlout ((long *) &m_Nr, SQLLONG, 0l);
		cursor = m_Textpool.sqlcursor (_T("select max (nr) from textpool"));
	}
	dsqlstatus = m_Textpool.sqlopen (cursor);
	if (dsqlstatus == 0)
	{
		dsqlstatus = m_Textpool.sqlfetch (cursor);
	}
	if (dsqlstatus == 0)
	{
		m_Nr ++;
	}
	return m_Nr;
}

long CTextmanager::GetNumber (long nr)
{
	if (nr == 0)
	{
		nr = CreateNumber ();
	}
	return nr;
}

BOOL CTextmanager::IsTextChanged (CString& Text)
{
	BOOL ret = FALSE;
	CString PoolText = m_Textpool.textpool.text;
	PoolText.TrimRight ();
	Text.TrimRight ();
	if (PoolText != Text)
	{
		ret = TRUE;
	}
	return ret;
}

void CTextmanager::Read ()
{
	memset (m_Textpool.textpool.text, 0, sizeof (m_Textpool.textpool.text));
	if (m_Textpool.textpool.nr != 0l)
	{
		m_Textpool.dbreadfirst ();
	}
}

void CTextmanager::Write ()
{
	CString Text = m_Textpool.textpool.text;
	Text.TrimRight ();
	if (Text.Trim ().GetLength () != 0 && m_Textpool.textpool.nr == 0l)
	{	
		m_Textpool.textpool.nr = CreateNumber ();
	}
	if (m_Textpool.textpool.nr != 0l)
	{
		_tcscpy (m_Textpool.textpool.text, Text.GetBuffer ());
		if (Text.Trim ().GetLength () != 0)
		{
			m_Textpool.dbupdate ();
		}
		else
		{
			m_Textpool.dbdelete ();
			m_Textpool.textpool.nr = 0l;
		}
	}
}

