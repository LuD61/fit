#pragma once
#include "Adr.h"
#include "Ptabn.h"
#include "mo_auto.h"
#include "Kun.h"

class CKunDiverse
{
private:
	enum
	{
		MaxTries = 1000,
	};

	enum KUN_TYP
	{
		Diverse = 25,
	};
		
	ADR_CLASS Adr;
	PTABN_CLASS *Ptabn;
	AUTO_CLASS *AutoNr;
	KUN_CLASS *Kun;
	ADR_CLASS *KunAdr;
    BOOL AdrBerOk;
    long adr_von;
    long adr_bis;
    long sadr_von;
    long sadr_bis;
	static CKunDiverse *Instance;
protected:
	CKunDiverse(void);
	~CKunDiverse(void);
public:
	static CKunDiverse *GetInstance ();
	static void DeleteInstance ();
	long GetAdrFrom ();
	long GetAdrTo ();
	void FillSadr ();
	long GenAdr ();
	BOOL AdrExist (long adr);
	long TestAdrOnDiverse (long adr);
	BOOL IsDiverse ();
	BOOL IsAdrDiverse ();
	BOOL IsAdrDiverse (long adr);
};
