// SortCollection.h: Schnittstelle f�r die Klasse CSortCollection.
//
//////////////////////////////////////////////////////////////////////
#ifndef SORT_COLLECTION_DEF
#define SORT_COLLECTION_DEF

#include <windows.h>
#include "DataCollection.h"

template <class T> 
class CSortCollection : public CDataCollection<T>
{
public:

CSortCollection() : CDataCollection ()
{
	Arr = NULL;
	anz = 0;
	pos = 0;
	bSize = 0x100;
}



void Insert (T Element)
{
	T el;
	T *it;
	int idx = 0;

	if (Arr == NULL)
	{
		Arr = new T [bSize];
		memset (Arr, 0, bSize * sizeof (T));
	}
	if (anz == bSize) Expand ();
	Start ();
	while ((it = GetNext ()) != 0)
	{
		el = *it;
		if (Element == el)
		{
			return;
		}
		if (Element < el)
		{
			break;
		}
		idx ++;
	}

	if (idx < anz)
	{
		for (int i = anz; i > idx; i --)
		{
			memcpy (&Arr[i], &Arr[i - 1], sizeof (T));
		}
	}
    memcpy (&Arr[idx], &Element, sizeof (T));
    anz ++;
}

void Insert (T *Element)
{
	Insert (*Element);
}

void Add (T Element)
{
	Insert (Element);
}

void Add (T *Element)
{
		Insert (*Element);
}
};

#endif
