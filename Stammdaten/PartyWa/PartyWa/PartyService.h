#pragma once
#include "CtrlGrid.h"
#include "ColorButton.h"
#include "afxwin.h"
#include "MessageHandler.h"
#include "Label.h"
#include "VLabel.h"
#include "DataCollection.h"
#include "ControlContainer.h"


// CPartyService-Formularansicht

class CPartyService : public CFormView, 
							 CControlContainer
{
	DECLARE_DYNCREATE(CPartyService)

protected:
	CPartyService();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPartyService();

public:
	enum { IDD = IDD_PARTYSERVICE };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnSize (UINT, int, int);
    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()

private:

	class CActivate : public CRunMessage
	{
	private: 
		CPartyService *p;
	public: 
			CActivate (CPartyService *p);
			virtual void Run ();
	};

	CFont Font;
	CFont TitleFont;
	int FontHeight;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	HBRUSH TitleBrush;
	CCtrlGrid TitleGrid;
	CCtrlGrid DataGrid;
	CCtrlGrid CtrlGrid;
	CColorButton m_BAngebote;
	CColorButton m_BAuftraege;
	CColorButton m_BAngebotsTypen;
	CColorButton m_BKAuftraege;
	CVLabel m_PartyServiceTitle;
	CMessageHandler *MessageHandler;
	CActivate *Activate;
	CDataCollection<CColorButton *> Container;
public:
	afx_msg void OnAngebote();
	afx_msg void OnAuftraege();
	afx_msg void OnAngTypes();
	afx_msg void OnKAuftraege();
	virtual void ElementSetFocus (CWnd *cWnd);
};


