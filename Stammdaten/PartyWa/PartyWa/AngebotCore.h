#pragma once
#include "mo_auto.h"
#include "Partyk.h"
#include "Partyp.h"
#include "Aufk.h"
#include "Aufp.h"
#include "Lsk.h"
#include "Lsp.h"
#include "Ptabn.h"
#include "A_bas.h"
#include "Mdn.h"
#include "Fil.h"
#include "Kun.h"
#include "Adr.h"
#include "Pers.h"
#include "DataTables.h"
#include "FormTab.h"
#include "mo_einh.h"
#include "Vector.h"
#include "kumebest.h"
#include "DllPreise.h"
#include "SortCollection.h"
#include "AbasCollection.h"
#include "PersCollection.h"
#include "FavoriteItem.h"
#include "RunMessage.h"
#include "CalcSmt.h"
#include "Mail.h"
#include <vector>

class CAngebotCore
{
private:
	enum LISTPOS
	{
		POSPOS,
		POSSTAT,
		POSA,
		POSABZ1,
		POSME,
		POSEINH,
		POSBASINH,
		POSBASME,
		POSBASEINH,
		POSVK,
		POSVKBRUTTO,
		POSVKME,
		POSLDPR,
	};

	enum
	{
		MaxTries = 1000,
	};

	enum RECH_TYPE
	{
		RechPa = 1,
	};

	AUTO_CLASS *AutoNr;
	PARTYK_CLASS *Partyk;
	PARTYK_CLASS PartykAng;
	PARTYK_CLASS PartykAuf;
	PARTYP_CLASS *Partyp;
	PARTYP_CLASS PartypAng;
	PARTYP_CLASS PartypAuf;
	AUFK_CLASS *Aufk;
	LSK_CLASS *Lsk;
	A_BAS_CLASS *A_bas;
	MDN_CLASS *Mdn;
	MDN_CLASS MdnAng;
	MDN_CLASS MdnAuf;
	FIL_CLASS *Fil;
	FIL_CLASS FilAng;
	FIL_CLASS FilAuf;
	KUN_CLASS *Kun;
	KUN_CLASS KunAng;
	KUN_CLASS KunAuf;
	ADR_CLASS *MdnAdr;
	ADR_CLASS MdnAdrAng;
	ADR_CLASS MdnAdrAuf;
	ADR_CLASS *FilAdr;
	ADR_CLASS FilAdrAng;
	ADR_CLASS FilAdrAuf;
	ADR_CLASS *KunAdr;
	ADR_CLASS KunAdrAng;
	ADR_CLASS KunAdrAuf;
	ADR_CLASS PersAdr;
	PERS_CLASS Pers;
	KMB_CLASS *Kmb;
	PARTYK_CLASS *PartykTest;
	PTABN_CLASS *Ptabn;
	ANGPT_CLASS *Angpt;
	LS_TXT_CLASS *Ls_txt;
	CFormTab *Form;
	CFormTab *FormAng;
	CFormTab *FormAuf;
	CFormTab *CalcForm;
	CFormTab *CalcFormAng;
	CFormTab *CalcFormAuf;
	CDatatables *Datatables;
	EINH_CLASS Einh;
	CMail Mail;
	int ListCursor;
	int SetAufpUpdateCursor;
	int SetLiefUpdateCursor;
	int DeletePosCursor;
	int DeleteAufPosCursor;
	int DeleteLiefPosCursor;
	int MaxAngTypCursor;
	int PtabLfdCursor;
	CString AngFormat;
	CString AuftragFormat;
	CString LiefFormat;
	CString RechFormat;
	CString Psz01;
	BOOL HostIsFit;
	BOOL PosReaded;
	BOOL RepaintCalcForm;
	double SingleGew;
	double PauschVk;
	double PauschSingleVk;

	CDllPreise DllPreise;
	CAngebotCore();
	~CAngebotCore();
	short mdn;
	short fil;
	short kun_fil;
	long kun;
	long ang;
	DATE_STRUCT lieferdat;
	double a;
	short sa;
	double pr_ek;
	double pr_brutto;
	double pr_netto; //FS-360
	BOOL BruttoKunde; //FS-360
	double pr_vk;
	double AngVk;
	double AngGew;
	CString BasisMeBz;
	CString MeBz;
	short me_einh;
	short me_einh_kun;
	short me_einh_kun1;
	short me_einh_kun2;
	short me_einh_kun3;
	double inh;
	double inh1;
	double inh2;
	double inh3;
	int ListRow;
	PARTYP ListStruct;
	CDataCollection<PARTYP *> *List;
	CDataCollection<long> *DelList;
	CDataCollection<CCalcSmt *> *CalcSmtTab;
	CDataCollection<CFavoriteItem *> *Stammdaten;
	CDataCollection<CFavoriteItem *> *Warenausgang;
	CDataCollection<CRunMessage *> *ShowCalcSmt;
	CDataCollection<CRunMessage *> *RepaintCalcSmt;
	CAbasCollection *ABasList;
	CPersCollection *PersList;
	CDataCollection<double> *LastEks;
	BOOL ShowLastEks;
	long AngNr;
	long AufNr;
	long LiefNr;
	long RechNr;
	short ls_stat;
	BOOL IsPartyService;
	double m_VatFull;
	double m_VatReduced;
	BOOL CalculatePauschSingle;
	double OSGPrice;
	double OSSPrice;
	int CurrentAnzPers;
	CDocument *Document;
	CRunMessage *Activate;
	CRunMessage *ShowCalcView;
	CRunMessage *HideOnlyCalcView;
	CString PersNam;
	long PersIdx;
	CString Debug;
	BOOL KunToReadOnly;
private:
	BOOL CalcWithTitle;
	static CAngebotCore *Instance;

public:
	enum
	{
		VatFull    = 1,
		VatReduced = 2,
	};
	enum
	{
		ActiveAngebot,
		ActiveAuftrag,
		ActiveAngebotsTypen,
		ActiveKAuftraege,
	};

	enum ANG_STAT
	{
		POffer = 0,
		POrder = 1,
		PPrinted = 2,
		PFacturated = 3,
	};

	enum LS_STAT
	{
		Printed = 4,
		Facturated = 6,
	};

	enum PAGE
	{
		PageAngebot,
		PageAuftrag,
		PageAngResource,
		PageKunAuftraege,
	};

	int PartySelect;
	BOOL IsVatFull;
private:
	PAGE Page;
public:
	PAGE GetPage ()
	{
		return Page;
	}

	BOOL GetShowLastEks ()
	{
		return ShowLastEks;
	}

	void SetShowLastEks (BOOL ShowLastEks)
	{
		this->ShowLastEks = ShowLastEks;
	}

	void SetPage (PAGE Page)
	{
		this->Page = Page;
	}

	void SetPersIdx (long PersIdx)
	{
		this->PersIdx = PersIdx;
	}

	long GetPersIdx ()
	{
		return PersIdx;
	}

	long *GetPPersIdx ()
	{
		return &PersIdx;
	}

	void SetRepaintCalcForm (BOOL RepaintCalcForm)
	{
		this->RepaintCalcForm = RepaintCalcForm;
	}

	void SetKunToReadOnly (BOOL KunToReadOnly=TRUE)
	{
		this->KunToReadOnly = KunToReadOnly;
	}

	BOOL GetKunToReadOnly ()
	{
		return KunToReadOnly;
	}

	double GetVatFull ()
	{
		return m_VatFull;
	}

	double GetVatReduced ()
	{
		return m_VatReduced;
	}

	void SetActivate (CRunMessage *Activate)
	{
		this->Activate = Activate;
	}

	void SetShowCalcView (CRunMessage *ShowCalcView)
	{
		this->ShowCalcView = ShowCalcView;
	}

	void SetHideCalcView (CRunMessage *HideCalcView)
	{
		this->HideOnlyCalcView = HideCalcView;
	}

	void SetMdn (short mdn)
	{
		this->mdn = mdn;
	}

	short GetMdn ()
	{
		return mdn;
	}

	void SetFil (short fil)
	{
		this->fil = fil;
	}

	short GetFil ()
	{
		return fil;
	}

	void SetKunFil (short kun_fil)
	{
		this->kun_fil = kun_fil;
	}

	short GetKunFil ()
	{
		return kun_fil;
	}

	void SetKun (long kun)
	{
		this->kun = kun;
	}

	long GetKun ()
	{
		return kun;
	}

	void SetAng (long ang)
	{
		this->ang = ang;
	}

	long GetAng ()
	{
		return ang;
	}

	void SetLieferdat (DATE_STRUCT *lieferdat)
	{
		memcpy (&this->lieferdat, lieferdat, sizeof (DATE_STRUCT));
	}

	DATE_STRUCT* GetLieferdat ()
	{
		return &lieferdat;
	}

	void SetA (double a)
	{
		this->a = a;
	}

	double GetA ()
	{
		return a;
	}

	short GetSa ()
	{
		return sa;
	}

	double GetPrNetto ()
	{
		return pr_netto;
	}

	double GetPrBrutto ()
	{
		return pr_brutto;
	}

	double GetPrVk ()
	{
		return pr_vk;
	}

	double GetAngVk ()
	{
		return AngVk;
	}

	void SetAngVk (double AngVk)
	{
		this->AngVk = AngVk;
	}

	double GetAngGew ()
	{
		return AngGew;
	}

	void SetAngGew (double AngGew)
	{
		this->AngGew = AngGew;
	}

	CString& GetBasisMeBz ()
	{
		return BasisMeBz;
	}

	CString& GetMeBz ()
	{
		return MeBz;
	}

	short GetMeEinh ()
	{
		return me_einh;
	}

	short GetMeEinhKun ()
	{
		return me_einh_kun;
	}

	void SetMeEinhKun (short me_einh_kun)
	{
		this->me_einh_kun = me_einh_kun;
	}

	void SetListRow (int ListRow)
	{
		this->ListRow = ListRow;
	}


	short GetMeEinhKun1 ()
	{
		return me_einh_kun1;
	}

	short GetMeEinhKun2 ()
	{
		return me_einh_kun2;
	}

	short GetMeEinhKun3 ()
	{
		return me_einh_kun3;
	}

	double GetInh ()
	{
		return inh;
	}

	double GetInh1 ()
	{
		return inh1;
	}
	double GetInh2 ()
	{
		return inh2;
	}
	double GetInh3 ()
	{
		return inh3;
	}

	CAbasCollection*GetABasList ()
	{
		return ABasList;
	}

	CPersCollection*GetPersList ()
	{
		return PersList;
	}

	void SetForm (CFormTab *Form)
	{
		this->Form = Form;
	}

	void SetFormAng (CFormTab *Form)
	{
		this->FormAng = Form;
	}

	void SetFormAuf (CFormTab *Form)
	{
		this->FormAuf = Form;
	}

	CFormTab *GetForm ()
	{
		return Form;
	}

	void SetCalcForm (CFormTab *CalcForm)
	{
		this->CalcForm = CalcForm;

		if (Form == FormAng)
		{
			CalcFormAng = CalcForm;
		}
		else if (Form == FormAuf)
		{
			CalcFormAuf = CalcForm;
		}

	}

	CFormTab * GetCalcForm ()
	{
		return CalcForm;
	}

	void SetCalcFormAng (CFormTab *CalcForm)
	{
		this->CalcFormAng = CalcForm;
	}

	void SetCalcFormAuf (CFormTab *CalcForm)
	{
		this->CalcFormAuf = CalcForm;
	}

    CDataCollection<PARTYP *> *GetList ()
	{
		return List;
	}

	void SetCurrentAnzPers (int CurrentAnzPers)
	{
		this->CurrentAnzPers = CurrentAnzPers;
	}

	int GetCurrentAnzPers ()
	{
		return CurrentAnzPers;
	}

	void SetList (CDataCollection<PARTYP *> *List)
	{
		this->List = List;
	}

    CDataCollection<long> *GetDelList ()
	{
		return DelList;
	}

	void SetDelList (CDataCollection<long> *DelList)
	{
		this->DelList = DelList;
	}

	void SetCalculatePauschSingle (BOOL CalculatePauschSingle)
	{
		this->CalculatePauschSingle = CalculatePauschSingle;
	}

	BOOL GetCalculatePauschSingle ()
	{
		return CalculatePauschSingle;
	}

	void SetDocument (CDocument *Document)
	{
		this->Document = Document;
	}

	CDocument *GetDocument ()
	{
		return Document;
	}

    CDataCollection<CFavoriteItem *> *GetStammdaten ()
	{
		return Stammdaten;
	}

	void SetStammdaten (CDataCollection<CFavoriteItem *> *Stammdaten)
	{
		this->Stammdaten = Stammdaten;
	}

    CDataCollection<CFavoriteItem *> *GetWarenausgang ()
	{
		return Warenausgang;
	}

	void SetWarenausgang (CDataCollection<CFavoriteItem *> *Warenausgang)
	{
		this->Warenausgang = Warenausgang;
	}

	PARTYK_CLASS *GetPartykTest ()
	{
		return PartykTest;
	}

	PTABN_CLASS *GetPtabn ()
	{
		return Ptabn;
	}

	void SetCalcWithTitle (BOOL WithTitle)
	{
		this->CalcWithTitle = WithTitle;
	}

	BOOL GetCalcWithTitle ()
	{
		return CalcWithTitle;
	}

	double GetOSGPrice ()
	{
		return OSGPrice;
	}

	double *GetPOSGPrice ()
	{
		return &OSGPrice;
	}

	void SetOSGPrice (double OSGPrice)
	{
		this->OSGPrice = OSGPrice;
	}

	double GetOSSPrice ()
	{
		return OSSPrice;
	}

	double *GetPOSSPrice ()
	{
		return &OSSPrice;
	}

	void SetOSSPrice (double OSSPrice)
	{
		this->OSSPrice = OSSPrice;
	}

	void SetLsStat (short ls_stat)
	{
		this->ls_stat = ls_stat;
	}

	short GetLsStat ()
	{
		return ls_stat;
	}

	double GetSingleGew ()
	{
		return SingleGew;
	}

	double *GetPSingleGew ()
	{
		return &SingleGew;
	}

	double GetPauschVk ()
	{
		return PauschVk;
	}

	double *GetPPauschVk ()
	{
		return &PauschVk;
	}

	double GetPauschSingleVk ()
	{
		return PauschSingleVk;
	}

	double *GetPPauschSingleVk ()
	{
		return &PauschSingleVk;
	}

	void SetPersNam (CString& PersNam)
	{
		this->PersNam = PersNam;
	}

	CString& GetPersNam ()
	{
		return PersNam;
	}

	void SetDebug (CString& Debug)
	{
		this->Debug = Debug;
	}

	CString& GetDebug ()
	{
		return Debug;
	}

	CDataCollection<CCalcSmt *> *GetCalcSmtTab ()
	{
		return CalcSmtTab;
	}

    static CAngebotCore *GetInstance ();
    static void DeleteInstance ();
	long CreateNumber ();
	long CreateAufNr ();
	long CreateLiefNr ();
	long CreateRechNr ();
	BOOL AngExist (long AngNr);
	BOOL AufExist (long AufNr);
	BOOL LiefExist (long LiefNr);
	BOOL RechExist (long RechNr);
	void FreeNumber ();
	void FreeNumber (long nr);
	BOOL GetPrice ();
	void GetVat ();
	int GetActVat ();
	void SetVatToBrutto ();
	void SetVatToNetto ();

	BOOL GetEinh ();
	BOOL GetAktEinh ();
	void FillEinh (CVector *v);
	void FindEinhCombo (CString *Item, CVector *v);
	void GetEinhBez (short me_einh);
	void FindDeletedRows ();
	BOOL FindInList (long row_id);
	BOOL ReadPositions ();
	BOOL WriteAng ();
	BOOL WritePositions ();
	BOOL StartFaProg (CString& Name);
	BOOL StartProg (CString& Programm);
	double  CalculateVkNetto (double vk_brutto);
	double CalculateVkBrutto (double vk_netto);
	void Calculate ();
	void CalculateSmt ();
	void CalculateSmt (short smt);
	void Calculate (int row, PARTYP *values);
	void Calculate (int row, int column);
	void CalculateSinglePauschPrice ();
	void FillCombo (LPTSTR Item, std::vector<CString *> *ComboValues);
	void FillPers (std::vector<CString *> *ComboValues);
	void DeleteAng ();
	BOOL PrintAng ();
	BOOL MailAng ();
	BOOL PrintPack ();
	BOOL PrintAuftrag ();
	BOOL MailAuftrag ();
	BOOL PrintLief ();
	BOOL PrintRech ();
	BOOL RePrintRech ();
	BOOL KunOk();
	void InsertNewAngTyp (CString& Name);
	void SetListStruct (int ListRow);
	long GenAngPosTxtNr0 ();
	BOOL AngTxtNrExist (long nr);
	long GenAngPosTxtNr (void);
	long GenLsTxtNr0 ();
	BOOL LsNrExist (long nr);
	long GenLsTxtNr (void);
	BOOL ReadAngpt (long nr, CString& Text);
	BOOL WriteAngpt (long nr, CString& Text);
	BOOL ReadText (long nr, CString& Text);
	BOOL WriteText (long nr, CString& Text);
	void CalculateHbkDate ();
	BOOL SetAngResource ();
	BOOL ExportAuftrag ();
	BOOL ExportAuftragToFit ();
	BOOL ExportLief ();
	BOOL ExportLiefToFit ();
	BOOL CalcPos ();
	void SetAng ();
	void TablesToAng ();
	void TablesToAuf ();
	void HideCalcView ();
	void HideCalcViewOnly ();
	void RegisterShowCalcSmt (CRunMessage * ShowSmt);
	void RegisterRepaintCalcSmt (CRunMessage * ShowSmt);
	CCalcSmt *FindSmt (short smt);
	BOOL ClearCalcSmt (CSortCollection<short> * SmtTab);
	BOOL HasService ();
	void Drop (double a);
	double GetLastEk (int pos);
	CString & GetCheckBoxValue (LPSTR source, CString& target);
	CString & GetCheckBoxValue (int source, CString& target);
	void UpdateLiefRetBa ();
};
