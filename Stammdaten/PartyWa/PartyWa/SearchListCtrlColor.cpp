#include "StdAfx.h"
#include "SearchListCtrlColor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNAMIC(CSearchListCtrlColor, CSearchListCtrl)

CSearchListCtrlColor::CSearchListCtrlColor(void)
{
}

CSearchListCtrlColor::~CSearchListCtrlColor(void)
{
	DestroyWindow ();
}

void CSearchListCtrlColor::Create (RECT &rect, CWnd *pParent, UINT nIDEdit, UINT nIDButton, int Align)
{

    CRect ButtonRect;
	ButtonRect = rect;
    int ButtonWidth = 4 * (rect.bottom - rect.top);
	ButtonWidth /= 4;
	ButtonRect.left = ButtonRect.right - ButtonWidth;
	Button.Create (_T(".."), WS_CHILD | WS_VISIBLE, ButtonRect,
							 pParent, nIDButton);
	Button.ModifyStyle (WS_TABSTOP, 0);

//	COLORREF DynButtonColor = RGB (192, 192, 192);
	COLORREF DynButtonColor = CColorButton::DynColorGray;
//	COLORREF DynButtonColor = CColorButton::DynColorBlue;
//	COLORREF DynButtonColor = GetSysColor (COLOR_3DFACE);

	Button.nID = nIDButton;
	Button.Orientation = Button.Center;
	Button.TextStyle = Button.Standard;
	Button.BorderStyle = Button.Solide;
//	Button.BorderStyle = Button.Rounded;
	Button.DynamicColor = TRUE;
	Button.TextColor =RGB (0, 0, 0);
//	Button.SetBkColor (DynButtonColor);
	Button.SetBkColor (GetSysColor (COLOR_3DFACE));
	Button.DynColor = DynButtonColor;
	Button.RolloverColor = RGB (192, 192, 192);
	Button.SetWindowText (_T(".."));
//	Button.LoadBitmap (IDB_POS_TEXT);
//	Button.LoadMask (IDB_POS_TEXT_MASK);

	CRect EditRect;
	EditRect = rect;
	EditRect.right = ButtonRect.left - 1;
	if (Align == RIGHT)
	{
		Edit.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_RIGHT | ES_AUTOHSCROLL, 
			             EditRect, pParent, nIDEdit);
	}
	else
	{
		Edit.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_AUTOVSCROLL | ES_MULTILINE | ES_AUTOHSCROLL, 
			              EditRect, pParent, nIDEdit);
	}
}

void CSearchListCtrlColor::DestroyWindow ()
{
	if (IsWindow (Edit.m_hWnd))
	{
		Edit.DestroyWindow ();
	}
	if (IsWindow (Button.m_hWnd))
	{
		Button.DestroyWindow ();
	}
}
