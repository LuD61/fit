#pragma once

class CDbQuery
{
	enum
	{
		String = 0,
		Short = 1,
		Long = 2,
		Decimal = 3,
		Date = 4,
	};
protected:
	CString Q;
	CString Query;
public:
	CDbQuery(void);
	~CDbQuery(void);
	CString& ForStringColumn (CString& Value, CString& ColumnName, int Typ=String);
	CString& ForNumColumn (CString& Value, CString& ColumnName);
	CString& ForColumn (CString& Value, CString& ColumnName, BOOL Quote, int Typ=String);
};
