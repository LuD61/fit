#pragma once
#include "token.h"

class CSToken :
	virtual public CToken
{
private:
	char *m_Pos;
public:
	CSToken(void);
    CSToken (char *, char *);
    CSToken (CString&, char *);
public:
	~CSToken(void);
     virtual void GetTokens (char *);
	 virtual char *strtok (char *Buffer, const char *sep);
};
