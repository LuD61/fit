// PrintMail.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "PartyWa.h"
#include "PrintMail.h"


// CPrintMail-Dialogfeld

IMPLEMENT_DYNAMIC(CPrintMail, CDialog)

CPrintMail::CPrintMail(CWnd* pParent /*=NULL*/)
	: CDialog(CPrintMail::IDD, pParent)
{
	m_Result = Print;
}

CPrintMail::~CPrintMail()
{
}

void CPrintMail::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OKEX, m_OkEx);
	DDX_Control(pDX, IDC_CANCELEX, m_CancelEx);
}


BEGIN_MESSAGE_MAP(CPrintMail, CDialog)
	ON_BN_CLICKED(IDOK, &CPrintMail::OnBnClickedOk)
	ON_BN_CLICKED(IDC_OKEX, &CPrintMail::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CANCELEX, OnCancel)
END_MESSAGE_MAP()


// CPrintMail-Meldungshandler

BOOL CPrintMail::OnInitDialog ()
{
	BOOL ret = CDialog::OnInitDialog ();
	CButton *b = (CButton *) GetDlgItem (IDC_PRINT);
	if (b != NULL)
	{
		b->SetCheck (BST_CHECKED);
	}

	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = CColorButton::DynColorGray;
	COLORREF ActiveColor = CColorButton::DynColorGray;
//	COLORREF BkColor (RGB (0, 0, 255));
//	COLORREF DynColor = RGB (0, 0, 255);
//	COLORREF RolloverColor = CColorButton::DynColorBlue;
//	COLORREF DynColor = CColorButton::DynColorBlue;
//	COLORREF RolloverColor = RGB (192, 192, 192);

/*
	m_OkEx.SetWindowText (_T("OK"));

	m_OkEx.nID = IDC_OKEX;
	m_OkEx.Orientation = m_OkEx.Center;
	m_OkEx.TextStyle = m_OkEx.Standard;
	m_OkEx.BorderStyle = m_OkEx.Solide;
	m_OkEx.DynamicColor = TRUE;
	m_OkEx.TextColor =RGB (0, 0, 0);
	m_OkEx.SetBkColor (BkColor);
	m_OkEx.DynColor = DynColor;
	m_OkEx.RolloverColor = RolloverColor;
	m_OkEx.LoadBitmap (IDB_OKEX);


	m_CancelEx.SetWindowText (_T("Abbrech/*en"));
	m_CancelEx.nID = IDC_CANCELEX;
	m_CancelEx.Orientation = m_CancelEx.Center;
	m_CancelEx.TextStyle = m_CancelEx.Standard;
	m_CancelEx.BorderStyle = m_CancelEx.Solide;
	m_CancelEx.DynamicColor = TRUE;
	m_CancelEx.TextColor =RGB (0, 0, 0);
	m_CancelEx.SetBkColor (BkColor);
	m_CancelEx.DynColor = DynColor;
	m_CancelEx.RolloverColor = RolloverColor;
	m_CancelEx.LoadBitmap (IDB_CANCELEX);
*/

	GetDlgItem (IDOK)->ShowWindow (SW_HIDE);
	GetDlgItem (IDCANCEL)->ShowWindow (SW_HIDE);

    return ret;
}

void CPrintMail::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CButton *b = (CButton *) GetDlgItem (IDC_EMAIL);
	if (b != NULL)
	{
		if (b->GetCheck () == BST_CHECKED)
		{
			m_Result = EMail;
		}
	}
	OnOK();
}
