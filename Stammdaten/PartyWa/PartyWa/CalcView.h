#pragma once
#include "afxext.h"
#include "Resource.h"
#include "VLabel.h"
#include "CtrlGrid.h"
#include "FormTab.h"
#include "Partyk.h"
#include "Partyp.h"
#include "AngebotCore.h"
#include "DataCollection.h"
#include "CalcSmt.h"
#include "RunMessage.h"


class CCalcView :
	public CFormView
{
//	DECLARE_DYNCREATE(CCalcView)
public:
	enum { IDD = IDD_CALC_VIEW };
//	enum { IDD = IDD_ANGEBOT_PAGE};
	CCalcView(void);
	void Create (CFrameWnd *CalcFrame);
public:
	~CCalcView(void);

public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung
//	virtual void OnSize (UINT, int, int);
//    afx_msg HBRUSH OnCtlColor (CDC*, CWnd*,UINT); 

	DECLARE_MESSAGE_MAP()

protected:

// Handlerklasse zum Anzeigen der Sortimentskalkulation
	class CShowCalcSmt : public CRunMessage
	{
	private: 
		CCalcView *p;
	public: 
			CShowCalcSmt (CCalcView *p);
			virtual void Run ();
	};

    CShowCalcSmt *ShowCalcSmt;

// Handlerklasse zum Neuzeichnen der Sortimentskalkulation
	class CRepaintCalcSmt : public CRunMessage
	{
	private: 
		CCalcView *p;
	public: 
			CRepaintCalcSmt (CCalcView *p);
			virtual void Run ();
	};

    CRepaintCalcSmt *RepaintCalcSmt;

	CFont Font;
	CCtrlGrid *CtrlGrid;
	CCtrlGrid MainTitleGrid;
	int WeightRow;
	CFormTab Form;
	CFormTab SmtForm;
	PARTYK_CLASS *Partyk;
	PARTYP_CLASS *Partyp;
	CAngebotCore *AngebotCore;

	CVLabel m_TitleBer;
	CVLabel m_TitlePausch;
	CVLabel m_TitleOService;

	CVLabel m_TitleBGesamt;
	CVLabel m_TitleBEinzel;
	CVLabel m_TitlePGesamt;
	CVLabel m_TitlePEinzel;
	CVLabel m_TitleOSGesamt;
	CVLabel m_TitleOSEinzel;

	CVLabel m_LTotalPrice;
	CVLabel m_TotalBGPrice;
	CVLabel m_TotalBSPrice;
	CVLabel m_TotalPGPrice;
	CVLabel m_TotalPSPrice;
	CVLabel m_TotalOSGPrice;
	CVLabel m_TotalOSSPrice;

	CVLabel m_LTotalWeight;
	CVLabel m_TotalWeight;
	CVLabel m_LSingleWeight;
	CVLabel m_SingleWeight;
    CDataCollection<CCalcSmt *> *CalcSmtTab;
    CDataCollection<CVLabel *> CalcSmtLabels;

public:
	CFormTab *GetForm ()
	{
		return &Form;
	}

	virtual void RegisterCalcForm () = 0;
	void CreateGrid ();
	void OnShowCalcSmt ();
	void OnRepaintCalcSmt ();
};
