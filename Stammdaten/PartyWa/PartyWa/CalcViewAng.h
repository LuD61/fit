#pragma once
#include "calcview.h"

class CCalcViewAng :
	public CCalcView
{
	DECLARE_DYNCREATE(CCalcViewAng)
public:
	CCalcViewAng(void);
public:
	~CCalcViewAng(void);
	virtual void RegisterCalcForm ();
};
