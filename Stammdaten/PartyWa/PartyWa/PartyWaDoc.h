// PartyWaDoc.h : Schnittstelle der Klasse CPartyWaDoc
//


#pragma once


class CPartyWaDoc : public CDocument
{
protected: // Nur aus Serialisierung erstellen
	CPartyWaDoc();
	DECLARE_DYNCREATE(CPartyWaDoc)

// Attribute
public:

// Vorg�nge
public:

// �berschreibungen
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementierung
public:
	virtual ~CPartyWaDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen f�r die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};


