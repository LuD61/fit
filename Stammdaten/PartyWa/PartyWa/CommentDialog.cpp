// CommentDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "PartyWa.h"
#include "CommentDialog.h"


// CCommentDialog-Dialogfeld

#define PARTYK CDatatables::GetInstance ()->GetPartyk ()->partyk

IMPLEMENT_DYNAMIC(CCommentDialog, CDialog)

CCommentDialog::CCommentDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CCommentDialog::IDD, pParent)
{
	m_DlgBrush = NULL;
	m_DlgColor = RGB (250, 250, 202);
	BkBitmap.SetBkType (BkBitmap.DynamicGrey);
//	BkBitmap.SetBkType (BkBitmap.DynamicMetal);
//	BkBitmap.SetDirection (BkBitmap.Vertical);
	ChoiceTextpool = NULL;
}

CCommentDialog::~CCommentDialog()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	if (m_DlgBrush != NULL)
	{
		DeleteObject (m_DlgBrush);
		m_DlgBrush = NULL;
	}
}

void CCommentDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAPTION, m_Caption);
	DDX_Control(pDX, IDC_LCOMM_ANGEBOT, m_LCommAngebot);
	DDX_Control(pDX, IDC_COMM_ANGEBOT, m_CommAngebot);
	DDX_Control(pDX, IDC_LCOMM_RECHNUNG, m_LCommRechnung);
	DDX_Control(pDX, IDC_COMM_RECHNUNG, m_CommRechnung);
	DDX_Control(pDX, IDC_LCOMM_INTERN, m_LCommIntern);
	DDX_Control(pDX, IDC_COMM_INTERN, m_CommIntern);
	DDX_Control(pDX, IDC_OKEX, m_OkEx);
	DDX_Control(pDX, IDC_CANCELEX2, m_CancelEx);
	DDX_Control(pDX, IDC_OPEN, m_Open1);
	DDX_Control(pDX, IDC_OPEN2, m_Open2);
	DDX_Control(pDX, IDC_OPEN3, m_Open3);
}


BEGIN_MESSAGE_MAP(CCommentDialog, CDialog)
	ON_WM_CTLCOLOR ()
	ON_BN_CLICKED(IDC_OKEX, OnOK)
	ON_BN_CLICKED(IDC_CANCELEX, OnCancel)
	ON_BN_CLICKED(IDC_OPEN, OnOpen1)
	ON_BN_CLICKED(IDC_OPEN2, OnOpen2)
	ON_BN_CLICKED(IDC_OPEN3, OnOpen3)
END_MESSAGE_MAP()


// CCommentDialog-Meldungshandler

BOOL CCommentDialog::OnInitDialog()
{
	CDialog::OnInitDialog ();

	m_Caption.SetParts (60);
	m_Caption.SetWindowText (_T("Kommentare"));
	m_Caption.SetBoderStyle (m_Caption.Solide);
	m_Caption.SetOrientation (m_Caption.Left);
	m_Caption.SetDynamicColor (TRUE);
	m_Caption.SetOrientation (m_Caption.Center);
	m_Caption.SetTextColor (RGB (255, 255, 153));
	m_Caption.SetBkColor (RGB (192, 192, 192));

	COLORREF BkColor (RGB (0, 0, 255));
//	COLORREF DynColor = RGB (0, 0, 255);
	COLORREF RolloverColor = CColorButton::DynColorBlue;
	COLORREF DynColor = CColorButton::DynColorBlue;

	m_OkEx.SetBkColor (BkColor);
	m_OkEx.DynColor = DynColor;
	m_OkEx.RolloverColor = RolloverColor;

	m_CancelEx.SetBkColor (BkColor);
	m_CancelEx.DynColor = DynColor;
	m_CancelEx.RolloverColor = RolloverColor;

	m_Open1.SetText (_T(" Auswahl"));
	m_Open1.SetBkColor (BkColor);
	m_Open1.DynColor = DynColor;
	m_Open1.RolloverColor = RolloverColor;

	m_Open2.SetText (_T(" Auswahl"));
	m_Open2.nID = IDC_OPEN2;
	m_Open2.SetBkColor (BkColor);
	m_Open2.DynColor = DynColor;
	m_Open2.RolloverColor = RolloverColor;

	m_Open3.SetText (_T(" Auswahl"));
	m_Open3.nID = IDC_OPEN3;
	m_Open3.SetBkColor (BkColor);
	m_Open3.DynColor = DynColor;
	m_Open3.RolloverColor = RolloverColor;

	GetDlgItem (IDOK)->ShowWindow (SW_HIDE);
	GetDlgItem (IDCANCEL)->ShowWindow (SW_HIDE);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	Form.Add (new CFormField (&m_CommAngebot,EDIT,  (char *)   PARTYK.kommang, VCHAR));
	Form.Add (new CFormField (&m_CommRechnung,EDIT,  (char *)   PARTYK.kommrech, VCHAR));
	Form.Add (new CFormField (&m_CommIntern,EDIT,  (char *)   PARTYK.kommintern, VCHAR));
	m_CommAngebot.SetLimitText (255);
	m_CommRechnung.SetLimitText (255);
	m_CommIntern.SetLimitText (255);

	m_Angmanager.Textpool ()->textpool.nr = PARTYK.kommangnr;
	m_Angmanager.Textpool ()->textpool.typ = 1;
	m_Angmanager.Read ();

	m_Rechmanager.Textpool ()->textpool.nr = PARTYK.kommrechnr;
	m_Rechmanager.Textpool ()->textpool.typ = 2;
	m_Rechmanager.Read ();

	m_Internmanager.Textpool ()->textpool.nr = PARTYK.komminternnr;
	m_Internmanager.Textpool ()->textpool.typ = 3;
	m_Internmanager.Read ();

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	StaticFont.CreateFontIndirect (&l);
	l.lfHeight = 25;
	CaptionFont.CreateFontIndirect (&l);

	m_Caption.SetFont (&CaptionFont);

    Form.Show (); 
	return TRUE;
}

BOOL CCommentDialog::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
		case WM_KEYDOWN :
			if (pMsg->wParam == VK_F5)
			{
				OnCancel ();
				return TRUE;
			}
			if (pMsg->wParam == VK_F12)
			{
				OnOK ();
				return TRUE;
			}
			break;
	}
	return CDialog::PreTranslateMessage (pMsg);
}

HBRUSH CCommentDialog::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	CRect rect;

	if (nCtlColor == CTLCOLOR_DLG)
	{
		    if (m_DlgBrush == NULL)
			{
//				m_DlgBrush = CreateSolidBrush (m_DlgColor);
                GetClientRect (&rect);
				BkBitmap.SetWidth (rect.right); 
				BkBitmap.SetHeight (rect.bottom);
				BkBitmap.SetPlanes (32);
				CBitmap *bitmap = BkBitmap.Create (); 
				m_DlgBrush = CreatePatternBrush (*bitmap);
				bitmap->DeleteObject ();
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC)
	{
		    if (m_DlgBrush == NULL)
			{
				m_DlgBrush = CreateSolidBrush (m_DlgColor);
                GetClientRect (&rect);
				BkBitmap.SetWidth (rect.right); 
				BkBitmap.SetHeight (rect.bottom);
				BkBitmap.SetPlanes (32);
				CBitmap *bitmap = BkBitmap.Create (); 
				m_DlgBrush = CreatePatternBrush (*bitmap);
				bitmap->DeleteObject ();
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CCommentDialog::OnOK ()
{
	Form.Get ();
	m_Angmanager.Textpool()->textpool.nr = PARTYK.kommangnr;
	if (m_Angmanager.Textpool()->textpool.nr != 0)
	{
		if (m_Angmanager.IsTextChanged (CString (PARTYK.kommang)))
		{
			if (MessageBox (_T("Ge�nderten Angebotskommnetar �ndern ?"), _T(""), 
							 MB_YESNO | MB_ICONQUESTION) != 
							IDYES)
			{
				m_Angmanager.Textpool()->textpool.nr = 0l;
			}
		}
	}
	_tcscpy (m_Angmanager.Textpool()->textpool.text, PARTYK.kommang); 
	m_Angmanager.Textpool ()->textpool.typ = 1;
	m_Angmanager.Write ();
	PARTYK.kommangnr = m_Angmanager.Textpool()->textpool.nr;

	m_Rechmanager.Textpool()->textpool.nr = PARTYK.kommrechnr;
	if (m_Rechmanager.Textpool()->textpool.nr != 0)
	{
		if (m_Rechmanager.IsTextChanged (CString (PARTYK.kommrech)))
		{
			if (MessageBox (_T("Ge�nderten Rechnungskommnetar �ndern ?"), _T(""), 
							 MB_YESNO | MB_ICONQUESTION) != 
							IDYES)
			{
				m_Rechmanager.Textpool()->textpool.nr = 0l;
			}
		}
	}
	_tcscpy (m_Rechmanager.Textpool()->textpool.text, PARTYK.kommrech); 
	m_Rechmanager.Textpool ()->textpool.typ = 2;
	m_Rechmanager.Write ();
	PARTYK.kommrechnr = m_Rechmanager.Textpool()->textpool.nr;

	m_Internmanager.Textpool()->textpool.nr = PARTYK.komminternnr;
	if (m_Internmanager.Textpool()->textpool.nr != 0)
	{
		if (m_Internmanager.IsTextChanged (CString (PARTYK.kommintern)))
		{
			if (MessageBox (_T("Ge�nderten internen Kommnetar �ndern ?"), _T(""), 
							 MB_YESNO | MB_ICONQUESTION) != 
							IDYES)
			{
				m_Internmanager.Textpool()->textpool.nr = 0l;
			}
		}
	}
	_tcscpy (m_Internmanager.Textpool()->textpool.text, PARTYK.kommintern); 
	m_Internmanager.Textpool ()->textpool.typ = 3;
	m_Internmanager.Write ();
	PARTYK.komminternnr = m_Internmanager.Textpool()->textpool.nr;
	CDialog::OnOK ();
}

void CCommentDialog::OnCancel ()
{
	CDialog::OnCancel ();
}

void CCommentDialog::OnOpen1 ()
{
	if (ChoiceTextpool != NULL)
	{
		delete ChoiceTextpool;
	}
	ChoiceTextpool = new CChoiceTextpool ();
    ChoiceTextpool->IsModal = TRUE;
	ChoiceTextpool->HideEnter = TRUE;
	ChoiceTextpool->SetBitmapButton (TRUE);
    ChoiceTextpool->SetDlgSize (600, 400);
	ChoiceTextpool->CreateDlg ();
	ChoiceTextpool->DoModal();
    if (ChoiceTextpool->GetState ())
    {
		  CTextpoolList *abl = ChoiceTextpool->GetSelectedText (); 
		  if (abl != NULL)
		  {
			m_Angmanager.Textpool()->textpool.nr = abl->nr;
			m_Angmanager.Textpool()->dbreadfirst ();
			memset (PARTYK.kommang, 0, sizeof (PARTYK.kommang));
			PARTYK.kommangnr = m_Angmanager.Textpool()->textpool.nr;
			_tcsncpy (PARTYK.kommang, m_Angmanager.Textpool()->textpool.text, sizeof (PARTYK.kommang) - 1);
			Form.Show ();
		  }
    }
	delete ChoiceTextpool;
	ChoiceTextpool = NULL;
}

void CCommentDialog::OnOpen2 ()
{
	if (ChoiceTextpool != NULL)
	{
		delete ChoiceTextpool;
	}
	ChoiceTextpool = new CChoiceTextpool ();
    ChoiceTextpool->IsModal = TRUE;
	ChoiceTextpool->HideEnter = TRUE;
	ChoiceTextpool->SetBitmapButton (TRUE);
    ChoiceTextpool->SetDlgSize (600, 400);
	ChoiceTextpool->CreateDlg ();
	ChoiceTextpool->DoModal();
    if (ChoiceTextpool->GetState ())
    {
		  CTextpoolList *abl = ChoiceTextpool->GetSelectedText (); 
		  if (abl != NULL)
		  {
			m_Rechmanager.Textpool()->textpool.nr = abl->nr;
			m_Rechmanager.Textpool()->dbreadfirst ();
			memset (PARTYK.kommrech, 0, sizeof (PARTYK.kommrech));
			PARTYK.kommrechnr = m_Rechmanager.Textpool()->textpool.nr;
			_tcsncpy (PARTYK.kommrech, m_Rechmanager.Textpool()->textpool.text, sizeof (PARTYK.kommrech) - 1);
			Form.Show ();
		  }
    }
	delete ChoiceTextpool;
	ChoiceTextpool = NULL;
}

void CCommentDialog::OnOpen3 ()
{
	if (ChoiceTextpool != NULL)
	{
		delete ChoiceTextpool;
	}
	ChoiceTextpool = new CChoiceTextpool ();
    ChoiceTextpool->IsModal = TRUE;
	ChoiceTextpool->HideEnter = TRUE;
	ChoiceTextpool->SetBitmapButton (TRUE);
    ChoiceTextpool->SetDlgSize (600, 400);
	ChoiceTextpool->CreateDlg ();
	ChoiceTextpool->DoModal();
    if (ChoiceTextpool->GetState ())
    {
		  CTextpoolList *abl = ChoiceTextpool->GetSelectedText (); 
		  if (abl != NULL)
		  {
			m_Internmanager.Textpool()->textpool.nr = abl->nr;
			m_Internmanager.Textpool()->dbreadfirst ();
			memset (PARTYK.kommintern, 0, sizeof (PARTYK.kommintern));
			PARTYK.komminternnr = m_Internmanager.Textpool()->textpool.nr;
			_tcsncpy (PARTYK.kommintern, m_Internmanager.Textpool()->textpool.text, sizeof (PARTYK.kommintern) - 1);
			Form.Show ();
		  }
    }
	delete ChoiceTextpool;
	ChoiceTextpool = NULL;
}
