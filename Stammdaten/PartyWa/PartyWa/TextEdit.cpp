#include "StdAfx.h"
#include ".\textedit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CTextEdit::CTextEdit(void)
{
}

CTextEdit::~CTextEdit(void)
{
}

BEGIN_MESSAGE_MAP(CTextEdit, CEdit)
	ON_WM_SETFOCUS ()
END_MESSAGE_MAP()

void CTextEdit::OnSetFocus (CWnd *oldFocus)
{
	CEdit::OnSetFocus (oldFocus);
	CString Text; 
	GetWindowText (Text);
	int len = Text.GetLength ();
	PostMessage (EM_SETSEL, len, len);
}
