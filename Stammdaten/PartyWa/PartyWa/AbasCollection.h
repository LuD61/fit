#pragma once
#include "datacollection.h"
#include "A_bas.h"

class CAbasCollection :
	public CDataCollection<A_BAS *>
{
public:
	enum
	{
		VatFull    = 1,
		VatReduced = 2,
	};
	CAbasCollection(void);
public:
	~CAbasCollection(void);
	A_BAS *Find (double a);
	void Add (A_BAS *a_bas);
	BOOL HasService ();
	void Drop (double a);
};
