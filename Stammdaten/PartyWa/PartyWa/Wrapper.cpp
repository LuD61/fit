#include "StdAfx.h"
#include <vector>
#include "Wrapper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define TAB 9

CWrapper::CWrapper(void)
{
	sep = _T("\n"); 
	LineLength = 0; 
}

CWrapper::CWrapper (char * Txt, char *sep)
{
     Tokens = NULL;
     Buffer = Txt;
     Buffer.TrimRight ();
     this->sep = sep;
	 LineLength = 0; 
     GetTokens (Txt);
}
CWrapper::CWrapper (CString& Txt, char *separator)
{
     Tokens = NULL;
     Buffer = Txt;
     Buffer.TrimRight ();
     this->sep = sep;
	 LineLength = 0; 
     GetTokens (Txt.GetBuffer ());
}
CWrapper::CWrapper (CString& Txt, int LineLength)
{
     Tokens = NULL;
     Buffer = Txt;
     Buffer.TrimRight ();
	 sep = _T("\n"); 
	 this->LineLength = LineLength;
     GetTokens (Txt.GetBuffer ());
}

 CWrapper::CWrapper (CString& Txt, char *separator, int LineLength)
{
     Tokens = NULL;
     Buffer = Txt;
     Buffer.TrimRight ();
     this->sep = sep;
	 this->LineLength = LineLength;
     GetTokens (Txt.GetBuffer ());
}

CWrapper::~CWrapper(void)
{
}

BOOL CWrapper::IsSeparator (char c)
{
	for (int i = 0; i < sep.GetLength (); i ++)
	{
		if (c == sep.GetBuffer ()[i])
		{
			return TRUE;
		}
	}
	return FALSE;
}

void CWrapper::GetTokens (char *Txt)
{
	 std::vector<CString *> Lines;
	 CString *Line;
	 char *b;
	 char *p;
 	 int i;
 	 int pos;

	 if (LineLength == 0)
	 {
		 CToken::GetTokens (Txt);
		 return;
	 }

     AnzToken = 0;
     Buffer.TrimRight ();
	 b = new char [Buffer.GetLength () + 1];
	 memset (b, 0, Buffer.GetLength () + 1);
     p = Buffer.GetBuffer ();
	 pos = 0;
	 for (int i = 0; p[i] != 0; i ++, pos ++)
	 {
		 if (IsSeparator (p[i]))
		 {
			 b[pos] = 0;
             Line = new CString (b);
			 Lines.push_back (Line);
			 AnzToken ++;
			 pos = 0;
			 i ++;
			 while (IsSeparator (p [i]) && p[i] != 0) i ++;
			 memset (b, 0, Buffer.GetLength () + 1);
		 }
		 else if (pos == LineLength)
		 {
			 b[pos] = 0;
			 if (pos > 0)
			 {
				i --;
				pos --;
			 }
			 int j = i;
			 for (; pos > 0; pos --, j --)
			 {
				 if (b[pos] == ' ' ||
					 b[pos] == TAB ||
					 b[pos] == ',' ||
					 b[pos] == '.' ||
					 b[pos] == ';' ||
					 b[pos] == ':' ||
					 b[pos] == '-'
					 )
				 {
					 b[pos + 1] = 0;
					 i = j;
					 break;
				 }
			 }
             Line = new CString (b);
			 Line->TrimRight ();
  		     Lines.push_back (Line);
			 AnzToken ++;
			 pos = 0;
			 i ++;
			 memset (b, 0, Buffer.GetLength () + 1);
		 }
		 b[pos] = p[i];
	 }
	 if (strlen (b) > 0)
	 {
		 Line = new CString (b);
		 Lines.push_back (Line);
		 AnzToken ++;
	 }

	 delete b;
     Tokens = new CString *[AnzToken];
     if (Tokens == NULL)
     {
             return;
     }
	 i = 0;
	 for (std::vector<CString *>::iterator it = Lines.begin ();
		                                   it != Lines.end ();
										   ++it)
	 {
		 Tokens[i] = *it;
		 i ++;
	 }
     AktToken = 0;
}

