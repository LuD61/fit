#include "StdAfx.h"
#include "CalcViewAuf.h"

// IMPLEMENT_DYNCREATE(CCalcViewAuf, CCalcView)
IMPLEMENT_DYNCREATE(CCalcViewAuf, CFormView)

CCalcViewAuf::CCalcViewAuf(void) :
			  CCalcView ()	
{
}

CCalcViewAuf::~CCalcViewAuf(void)
{
}

void CCalcViewAuf::RegisterCalcForm ()
{
	AngebotCore->SetCalcForm (&Form);
	AngebotCore->SetCalcFormAuf (&Form);
}
