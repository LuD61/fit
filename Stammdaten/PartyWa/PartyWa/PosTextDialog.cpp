// PosTextDialog.cpp : implementation file
//

#include "stdafx.h"
#include "PartyWa.h"
#include "PosTextDialog.h"
#include "Wrapper.h"


// CPosTextDialog dialog

IMPLEMENT_DYNAMIC(CPosTextDialog, CDialog)

CPosTextDialog::CPosTextDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPosTextDialog::IDD, pParent)
{
	TextNr = 0;
	Text = _T("");
	Title = _T("Positionstext");
	TextType = Position;
	AngebotCore = CAngebotCore::GetInstance ();
	AutoWrap = TRUE;
}

CPosTextDialog::~CPosTextDialog()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
}

void CPosTextDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LTEXT_NR, m_LTextNr);
	DDX_Control(pDX, IDC_TEXT_NR, m_TextNr);
	DDX_Control(pDX, IDC_TEXT, m_Text);
}


BEGIN_MESSAGE_MAP(CPosTextDialog, CDialog)
	ON_BN_CLICKED(IDOK, &CPosTextDialog::OnBnClickedOk)
	ON_EN_CHANGE(IDC_TEXT, &CPosTextDialog::OnEnChangeText)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_COMMAND(ID_EDIT_DELETE, OnEditDelete)
	ON_COMMAND(ID_EDIT_SELECTALL, OnEditSelectAll)
END_MESSAGE_MAP()

BOOL CPosTextDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetWindowText (Title.GetBuffer ());
	m_Text.SetEventMask (m_Text.GetEventMask () | ENM_CHANGE | ENM_MOUSEEVENTS);
	Form.Add (new CFormField (&m_TextNr,EDIT,        (long *)   &TextNr, VLONG));
	Form.Add (new CFormField (&m_Text,EDIT,          (char *)   &Text,   VSTRING));

	Form.Show ();
	return TRUE;
}

// CPosTextDialog message handlers

BOOL CPosTextDialog::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F5 ||
					 pMsg->wParam == VK_ESCAPE)
			{
				OnCancel();	
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				OnBnClickedOk();	
				return TRUE;
			}
			break;
		case WM_RBUTTONDOWN : 
			if (pMsg->hwnd == m_Text.m_hWnd)
			{
				OnTextRButtonDown (pMsg->lParam);
				return TRUE;
			}
			break;
	}
	return CDialog::PreTranslateMessage (pMsg);;
}

BOOL CPosTextDialog::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_TextNr)
	{
		Form.Get ();
		if (TextType == Position)
		{
			AngebotCore->ReadAngpt (TextNr, Text);
		}
		else if (TextType == HeadFoot)
		{
			AngebotCore->ReadText (TextNr, Text);
		}
		Form.Show ();
	}
	if (Control != &m_Text)
	{
		Control = GetNextDlgTabItem (Control, FALSE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}
		return TRUE;
	}
	return FALSE;
}

void CPosTextDialog::OnBnClickedOk()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	OnOK();
}

void CPosTextDialog::OnEnChangeText()
{
	// TODO:  Falls dies ein RICHEDIT-Steuerelement ist, wird das Kontrollelement
	// diese Benachrichtigung nicht senden, es sei denn, Sie setzen den CDialog::OnInitDialog() au�er Kraft.
	// Funktion und Aufruf CRichEditCtrl().SetEventMask()
	// mit dem ENM_CHANGE-Flag ORed in der Eingabe.

	// TODO:  F�gen Sie hier Ihren Code f�r die Kontrollbenachrichtigungsbehandlung ein.

	_TCHAR Line [LineMaxLength + 2];
	memset (Line, (_TCHAR) 0, LineMaxLength + 2);

	if (AutoWrap)
	{
		if (m_Text.LineLength () > LineMaxLength)
		{
			CHARRANGE cr;
			m_Text.GetSel (cr);
			CString Text;
			m_Text.GetWindowText (Text);
			CWrapper w (Text, 60);
			char *l;
			Text = "";
			while ((l = w.NextToken ()) != NULL)
			{
				if (Text.GetLength () > 0)
				{
					Text += _T("\n");
				}
				Text += l;
			}
			m_Text.SetWindowText (Text);
			cr.cpMin ++;
			cr.cpMax = cr.cpMin;
			m_Text.SetSel (cr);
		}
	}
	else
	{
		if (m_Text.LineLength () > LineMaxLength)
		{
			int idx = m_Text.LineIndex ();
			m_Text.GetLine (idx, Line, LineMaxLength);
			m_Text.SendMessage (WM_KEYDOWN, VK_BACK, 0l);
		}
	}
}

void CPosTextDialog::OnEditCopy()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CWnd *control = GetFocus ();
	if (control->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) control)->Copy (); 
	}
	else if (control->IsKindOf (RUNTIME_CLASS (CRichEditCtrl)))
	{
		((CRichEditCtrl *) control)->Copy (); 
	}
}

void CPosTextDialog::OnEditCut()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CWnd *control = GetFocus ();
	if (control->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) control)->Cut (); 
	}
	else if (control->IsKindOf (RUNTIME_CLASS (CRichEditCtrl)))
	{
		((CRichEditCtrl *) control)->Cut (); 
	}
}

void CPosTextDialog::OnEditUndo()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CWnd *control = GetFocus ();
	if (control->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) control)->Undo (); 
	}
	else if (control->IsKindOf (RUNTIME_CLASS (CRichEditCtrl)))
	{
		((CRichEditCtrl *) control)->Undo (); 
	}
}

void CPosTextDialog::OnEditPaste()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CWnd *control = GetFocus ();
	if (control->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) control)->Paste (); 
	}
	else if (control->IsKindOf (RUNTIME_CLASS (CRichEditCtrl)))
	{
		((CRichEditCtrl *) control)->Paste (); 
		if (AutoWrap)
		{
			CHARRANGE cr;
			m_Text.GetSel (cr);
			CString Text;
			m_Text.GetWindowText (Text);
			CWrapper w (Text, 60);
			char *l;
			Text = "";
			while ((l = w.NextToken ()) != NULL)
			{
				if (Text.GetLength () > 0)
				{
					Text += _T("\n");
				}
				Text += l;
			}
			m_Text.SetWindowText (Text);
			cr.cpMin = Text.GetLength ();
			cr.cpMax = cr.cpMin;
			m_Text.SetSel (cr);
		}
	}
}

void CPosTextDialog::OnEditDelete()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CWnd *control = GetFocus ();
	if (control->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) control)->Clear ();
	}
	else if (control->IsKindOf (RUNTIME_CLASS (CRichEditCtrl)))
	{
		((CRichEditCtrl *) control)->Clear ();
	}
}

void CPosTextDialog::OnEditSelectAll()
{
	// TODO: F�gen Sie hier Ihren Befehlsbehandlungscode ein.
	CWnd *control = GetFocus ();
	if (control->IsKindOf (RUNTIME_CLASS (CEdit)))
	{
		((CEdit *) control)->SetSel (0,-1);
	}
	else if (control->IsKindOf (RUNTIME_CLASS (CRichEditCtrl)))
	{
		((CRichEditCtrl *) control)->SetSel (0,-1);
	}
}

void CPosTextDialog::OnTextRButtonDown (LPARAM lParam)
{
    CMenu Menu, *popupMenu;
	CPoint p;
	long nStartChar;
	long nEndChar;

    GetCursorPos (&p);
	Menu.LoadMenu (IDR_POS_TEXT_MENU);
	if (m_Text.CanPaste ())
	{
			Menu.EnableMenuItem (ID_EDIT_PASTE, MF_BYCOMMAND | MF_ENABLED);
	}
	else
	{
			Menu.EnableMenuItem (ID_EDIT_PASTE, MF_BYCOMMAND | MF_GRAYED);
	}
	if (m_Text.CanUndo ())
	{
			Menu.EnableMenuItem (ID_EDIT_UNDO, MF_BYCOMMAND | MF_ENABLED);
	}
	else
	{
			Menu.EnableMenuItem (ID_EDIT_UNDO, MF_BYCOMMAND | MF_GRAYED);
	}
	m_Text.GetSel (nStartChar, nEndChar);
	if (nStartChar != nEndChar)
	{
			Menu.EnableMenuItem (ID_EDIT_DELETE, MF_BYCOMMAND | MF_ENABLED);
	}
	else
	{
			Menu.EnableMenuItem (ID_EDIT_DELETE, MF_BYCOMMAND | MF_GRAYED);
	}



    popupMenu = Menu.GetSubMenu (0);
    popupMenu->TrackPopupMenu (TPM_CENTERALIGN, p.x, p.y, this, NULL);
    Menu.DestroyMenu ();
}
