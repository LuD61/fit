#pragma once
#include "DataCollection.h"
#include "mapi.h"

class CMail
{
public:
	enum MAIL_ERROR
	{
		NoError,
		NoLibrary,
		NoMailProc,
		SendError,
		GlobalError,
	};
private:
    HINSTANCE m_Library;
    ULONG (PASCAL *m_lpfnSendMail)(ULONG, ULONG, MapiMessage*, FLAGS, ULONG);


	MAIL_ERROR m_Error;
	CDataCollection<CString *> m_Files;
public:
	CString m_From;
	CString m_To;
	CString m_CC;
	CString m_Subject;
	CString m_NoteText;
	BOOL m_Dialog;
    MAIL_ERROR Error ()
	{
		return m_Error;
	}

	CMail(void);
	~CMail(void);
	void Init ();
	void AddFile (CString *FileName);
	BOOL Send ();
	CString& BaseName (CString& Path);
};
