#pragma once

class CFavoriteItem
{
private:
	CString Name;
	CString ProgrammCall;

public:
	void SetName (LPTSTR Name)
	{
		this->Name = Name;
	}

	LPTSTR GetName ()
	{
		return Name.GetBuffer ();
	}

	void SetProgrammCall (LPTSTR ProgrammCall)
	{
		this->ProgrammCall = ProgrammCall;
	}

	LPTSTR GetProgrammCall ()
	{
		return ProgrammCall.GetBuffer ();
	}

	CFavoriteItem(void);
	CFavoriteItem(LPTSTR Name, LPTSTR ProgarmmCall);
	CFavoriteItem(CString& Name, CString& ProgarmmCall);
public:
	~CFavoriteItem(void);
};
