#include "stdafx.h"
#include "OdbcClass.h"
#include <math.h>
#include "dbclass.h"


#ifndef INFORMIX
short sql_mode = 0;
int sqlstatus = 0;
#endif

#define SQLFEHLER(b, s, l) if (sqlstatus b) _SQLF(s,l);

ODBC::DB_CLASS DbClass;

BOOL DB_CLASS::DbIsOpened = FALSE;
short DB_CLASS::ShortNull = (short) 0x8000;
long DB_CLASS::LongNull = (long) 0x80000000;
double DB_CLASS::DoubleNull = (double) 0xffffffffffffffff;
BOOL DB_CLASS::UseOdbc = TRUE;
int DB_CLASS::SqlErrorMode = 0;

void DB_CLASS::opendbase (LPTSTR dbase)
{
	if (DbIsOpened)
	{
		return;
	}
	sql_mode = 0;
	if (UseOdbc)
	{
		DbClass.opendbase (dbase);
	}
	else
	{
#ifdef INFORMIX
	   ::opendbase (dbase);
       SQLFEHLER (!= 0, "Fehler beim �ffnen der Datenbank",__LINE__)
#endif
	}
    DbIsOpened = TRUE;
}

void DB_CLASS::closedbase (LPTSTR dbase)
{
	if (!DbIsOpened)
	{
		return;
	}
	sql_mode = 1;
	if (UseOdbc)
	{
		DbClass.closedbase (dbase);
	}
	else
	{
#ifdef INFORMIX
		::closedbase ();
		sqlclose (0);
#endif
	}
    DbIsOpened = FALSE;
}

#ifdef INFORMIX

int DB_CLASS::beginwork ()
{
	::beginwork ();
	return 0;
}

int  DB_CLASS::commitwork ()
{
	::commitwork ();
	return 0;
}

int  DB_CLASS::rollbackwork ()
{
	::rollbackwork ();
	return 0;
}
#else
int DB_CLASS::beginwork ()
{
	DbClass.beginwork ();
	return 0;
}

int  DB_CLASS::commitwork ()
{
	DbClass.commitwork ();
	return 0;
}

int  DB_CLASS::rollbackwork ()
{
	DbClass.rollbackwork ();
	return 0;
}

#endif

int DB_CLASS::dbreadfirst (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
	     
//  	     sql_mode = 1;
         if (cursor == -1)
         {
             prepare ();
         }
         int dsqlstatus = sqlopen (cursor);
		 sqlstatus = dsqlstatus;
         SQLFEHLER (!= 0, _T("Fehler beim �ffnen "),__LINE__)
         dsqlstatus = sqlfetch (cursor);
		 sqlstatus = dsqlstatus;
         SQLFEHLER (< 0, _T("Fehler beim Lesen "),__LINE__)
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int DB_CLASS::dbread (void)
/**
Naechsten Satz aus Tabelle lesen.
**/
{
         int dsqlstatus = sqlfetch (cursor);
		 sqlstatus = dsqlstatus;
         SQLFEHLER (< 0, _T("Fehler beim Lesen "),__LINE__)
         if (sqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int DB_CLASS::dbupdate (void)
/**
Tabelle eti Updaten.
**/
{

         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         int dsqlstatus = sqlopen (test_upd_cursor);
		 sqlstatus = dsqlstatus;
         SQLFEHLER (!= 0, _T("Fehler beim �ffnen "),__LINE__)
         dsqlstatus = sqlfetch (test_upd_cursor);
		 sqlstatus = dsqlstatus;
         SQLFEHLER (< 0, _T("Fehler beim Lesen "),__LINE__)
         if (sqlstatus == 100)
         {
                   dsqlstatus = sqlexecute (ins_cursor);
		           sqlstatus = dsqlstatus;
                   SQLFEHLER (!= 0, _T("Fehler beim Insert "),__LINE__)
         }  
         else if (sqlstatus == 0)
         {
                   dsqlstatus = sqlexecute (upd_cursor);
		           sqlstatus = dsqlstatus;
                   SQLFEHLER (!= 0, _T("Fehler beim Update "),__LINE__)
         }  
          
         return sqlstatus;
} 

int DB_CLASS::dblock (void)
/**
Tabelle eti Updaten.
**/
{

	if (test_upd_cursor == -1)
         {
             prepare ();
         }
         int dsqlstatus = sqlopen (test_upd_cursor);
		 sqlstatus = dsqlstatus;
         SQLFEHLER (!= 0, _T("Fehler beim �ffnen "),__LINE__)
         dsqlstatus = sqlfetch (test_upd_cursor);
		 sqlstatus = dsqlstatus;
         SQLFEHLER (< 0, _T("Fehler beim Lesen "),__LINE__)
         if (sqlstatus == 0 && test_lock_cursor != -1)
		 {
			 dsqlstatus = sqlexecute (test_lock_cursor);
			 sqlstatus = dsqlstatus;
			 SQLFEHLER (< 0, _T("Fehler beim Lesen "),__LINE__)
		 }
         return sqlstatus;
} 

int DB_CLASS::dbdelete (void)
/**
Tabelle eti lesen.
**/
{
         if (test_upd_cursor == -1)
         {
             prepare ();
         }
         int dsqlstatus = sqlopen (test_upd_cursor);
		 sqlstatus = dsqlstatus;
         SQLFEHLER (!= 0, _T("Fehler beim �ffnen "),__LINE__)
         dsqlstatus = sqlfetch (test_upd_cursor);
		 sqlstatus = dsqlstatus;
         SQLFEHLER (< 0, _T("Fehler beim Lesen "),__LINE__)
         if (sqlstatus == 0)
         {
                      dsqlstatus = sqlexecute (del_cursor);
		              sqlstatus = dsqlstatus;
                      SQLFEHLER (!= 0, _T("Fehler beim L�schen "),__LINE__)
         }
         return sqlstatus;
}

void DB_CLASS::dbclose (void)
/**
Cursor fuer eti schliessen.
**/
{
         if (cursor == -1) return;

         int dsqlstatus = sqlclose (cursor); 
		 sqlstatus = dsqlstatus;
         SQLFEHLER (!= 0, _T("Fehler beim Schliessen "),__LINE__)
         dsqlstatus = sqlclose (upd_cursor); 
		 sqlstatus = dsqlstatus;
         SQLFEHLER (!= 0, _T("Fehler beim Schliessen "),__LINE__)
         dsqlstatus = sqlclose (ins_cursor); 
		 sqlstatus = dsqlstatus;
         SQLFEHLER (!= 0, _T("Fehler beim Schliessen "),__LINE__)
         dsqlstatus = sqlclose (del_cursor); 
		 sqlstatus = dsqlstatus;
         SQLFEHLER (!= 0, _T("Fehler beim Schilessen "),__LINE__)
         dsqlstatus = sqlclose (test_upd_cursor);
		 sqlstatus = dsqlstatus;
         SQLFEHLER (!= 0, _T("Fehler beim Schliessen "),__LINE__)

         cursor = -1;
         upd_cursor = -1;
         ins_cursor = -1;
         del_cursor = -1;
         test_upd_cursor = -1;
         cursor_ausw = -1;
}

int DB_CLASS::dbmove (int mode)
/**
Scroll-Cursor lesen.
**/
{
		 if (UseOdbc)
		 {
			return DbClass.dbmove (mode);
		 }

#ifdef INFORMIX

         int scrollakt;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }

         scrollakt = scrollpos;
         switch (mode)
         {
             case FIRST :
                         scrollpos = 1;
                         break;
             case NEXT :     
                         scrollpos ++;
                         break;
             case PRIOR :
                         if (scrollpos > 1)
                         {
                                 scrollpos --;
                         }
                         break;
             case CURRENT :
                         break;
             default :
                   return (-1);
         }
         fetch_scroll (cursor_ausw, mode);
         if (sqlstatus != 0)
         {
             scrollpos = scrollakt;
         }
         return (sqlstatus);
#else
		 return 0;
#endif
}

int DB_CLASS::dbmove (int mode, int pos)
/**
Scroll-Cursor lesen.
**/
{
		 if (UseOdbc)
		 {
			return DbClass.dbmove (mode, pos);
		 }

#ifdef INFORMIX

         int scrollakt;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }
         scrollakt = scrollpos;
         switch (mode)
         {
             case DBABSOLUTE :      
             case RELATIVE :      
                    break;
             default :
                   return (-1);
         }
         fetch_scroll (cursor_ausw, mode, pos);
         if (sqlstatus != 0)
         {
             scrollpos = scrollakt;
         }
         return (sqlstatus);
#else
		 return 0;
#endif
}

int DB_CLASS::dbcanmove (int mode)
/**
Scroll-Cursor testen.
**/
{
		 if (UseOdbc)
		 {
			return DbClass.dbcanmove (mode);
		 }

#ifdef INFORMIX
         int status; 

         if (cursor_ausw == -1)
         {
                    return (-1);
         }

         switch (mode)
         {
             case FIRST :
                         scrollpos = 1;
                         break;
             case NEXT :     
                         scrollpos ++;
                         break;
             case PRIOR :
                         if (scrollpos > 1)
                         {
                                 scrollpos --;
                         }
                         break;
//             case LAST :      
             case CURRENT :
                         break;
             default :
                   return (-1);
         }
         fetch_scroll (cursor_ausw, mode);
         if (sqlstatus != 0)
         {
             status = FALSE;
         }
         else
         {
             status = TRUE;
         }
         dbmove (DBABSOLUTE, scrollpos);
         return (status);
#else
		 return 0;
#endif
}

int DB_CLASS::dbcanmove (int mode, int pos)
/**
Scroll-Cursor lesen.
**/
{
		 if (UseOdbc)
		 {
			return DbClass.dbcanmove (mode,pos);
		 }

#ifdef INFORMIX
         int status;

         if (cursor_ausw == -1)
         {
                    return (-1);
         }
         switch (mode)
         {
             case DBABSOLUTE :      
             case RELATIVE :      
                    break;
             default :
                   return (-1);
         }
         fetch_scroll (cursor_ausw, mode, pos);
         if (sqlstatus != 0)
         {
             status = FALSE;
         }
         else
         {
             status = TRUE;
         }
         dbmove (DBABSOLUTE, scrollpos);
         return (status);
#else
		 return 0;
#endif
}

void DB_CLASS::cleanin ()
{
//	   init_sqlin ();
}

void DB_CLASS::cleanout ()
{
//	   init_sqlout ();
}

int DB_CLASS::sqlin (void *vadr, int typ, int len)
/**
Eingabevariable fuer sqlcomm zuordnen.
**/
{
	if (UseOdbc)
	{
		DbClass.sqlin (vadr, typ, len);
		return 0;
	}
#ifdef INFORMIX
    return ins_quest ((LPTSTR) vadr, typ, len);
#else
	return 0;
#endif
}


int DB_CLASS::sqlout (void *vadr, int typ, int len)
/**
Eingabevariable fuer sqlcomm zuordnen.
**/
{
	if (UseOdbc)
	{
		DbClass.sqlout (vadr, typ, len);
		return 0;
	}
#ifdef INFORMIX
    return out_quest ((LPTSTR) vadr, typ, len);
#else
	return 0;
#endif
}

int DB_CLASS::sqlcursor (LPTSTR sql)
/**
Cursor vorbereiten.
**/
{
	if (UseOdbc)
	{
		return DbClass.sqlcursor (sql);
	}
#ifdef INFORMIX
    int dsqlstatus = prepare_sql (sql);
    SQLFEHLER (!= 0, "Fehler beim Prepare ",__LINE__)
    return dsqlstatus;
#else
	return 0;
#endif
}

int DB_CLASS::sqlopen (int cursor)
/**
Cursor vorbereiten.
**/
{
	if (UseOdbc)
	{
		return DbClass.sqlopen (cursor);
	}

#ifdef INFORMIX
    int dsqlstatus = open_sql (cursor);
    SQLFEHLER (!= 0, "Fehler beim �ffnen ",__LINE__)
    return dsqlstatus;
#else
	return 0;
#endif
}

int DB_CLASS::sqlfetch (int cursor)
/**
Cursor vorbereiten.
**/
{
	if (UseOdbc)
	{
		return DbClass.sqlfetch (cursor);
	}
#ifdef INFORMIX
    int dsqlstatus = fetch_sql (cursor);
    SQLFEHLER (< 0, "Fehler beim Lesen ",__LINE__)
    return dsqlstatus;
#else
	return 0;
#endif
}

int DB_CLASS::sqlexecute (int cursor)
/**
Cursor vorbereiten.
**/
{
	if (UseOdbc)
	{
		return DbClass.sqlexecute (cursor);
	}
#ifdef INFORMIX
    int dsqlstatus = execute_curs (cursor);
    SQLFEHLER (!= 0, "Fehler beim Execute ",__LINE__)
    return dsqlstatus;
#else
	return 0;
#endif
}


int DB_CLASS::sqlclose (int cursor)
/**
Cursor schliessen.
**/
{
	if (UseOdbc)
	{
		return DbClass.sqlclose (cursor);
	}
#ifdef INFORMIX
    int dsqlstatus = close_sql (cursor);
    SQLFEHLER (!= 0, "Fehler beim Schliessen ",__LINE__)
    return dsqlstatus;
#else
	return 0;
#endif
}
          

int DB_CLASS::sqlcomm (LPTSTR sql)
/**
SQL-Commando ausfuehren.
**/
{
	if (UseOdbc)
	{
		return DbClass.sqlcomm (sql);
	}
#ifdef INFORMIX
    execute_sql (sql);
    SQLFEHLER (!= 0, "Fehler beim Ausf�hren ",__LINE__)
    return sqlstatus;
#else
	return 0;
#endif
}
          
int DB_CLASS::IsShortnull (short swert)
{
		if (swert == ShortNull) return TRUE;
		return FALSE;
}


int DB_CLASS::IsLongnull (long lwert)
{
		if (lwert == LongNull) return TRUE;
		return FALSE;
}

int DB_CLASS::IsDoublenull (double dwert)
{
		if (dwert == DoubleNull) return TRUE;
		return FALSE;
}

short *DB_CLASS::GetShortnull ()
{
		return &ShortNull;
}


long *DB_CLASS::GetLongnull ()
{
		return &LongNull;
}

double *DB_CLASS::GetDoublenull ()
{
		return &DoubleNull;
}


int DB_CLASS::PrintColError ()
/**
Bei Fehler -217 falschen Feldnamen suchen.
**/
{
#ifdef INFORMIX
	        int pos;
			int i, j;
			TCHAR feld [21];

			pos = sqlca.sqlerrd[4];
			for (i = pos - 2; i > 0; i --)
			{
				if (sql[i] > ' ') break;
			}

			for (; i > 0; i --)
			{
				if (sql[i] <= ' ') break;
				if (sql[i] == ',') break;
			}

			if (i == 0) return (0);
			for (i = i + 1, j = 0; sql[i] > ' '; i ++, j ++)
			{
				if (sql[i] <= ',') break;
				feld [j] = sql[i];
			}
			feld[j] = 0;
			CString ErrorText;
			ErrorText.Format ("Fehler -217 : Feld %s nicht gefunden\n%s", feld, sql);
			MessageBox (NULL, ErrorText.GetBuffer (0), "SQL-Fehler",
					        MB_OK | MB_ICONERROR);
#endif
			return (1);
}

int DB_CLASS::PrintSyntaxError ()
/**
Bei Fehler -217 falschen Feldnamen suchen.
**/
{
#ifdef INFORMIX
	        int pos;

			pos = sqlca.sqlerrd[4];
			CString ErrorText;
			ErrorText.Format ("Syntaxfehler an Position %d\n%s", pos, sql);
			MessageBox (NULL, ErrorText.GetBuffer (0), "SQL-Fehler",
					        MB_OK | MB_ICONERROR);
#endif
			return (1);
}

void DB_CLASS::_SQLF (LPTSTR s, long l)
/**
Abbruch bei Sqlfehler.
**/
{

#ifdef INFORMIX
            if (SqlErrorMode == 0 || SqlErrorMode == 2)
            {
 		        int ret = 0;
				if (sqlca.sqlcode == -217)
				{
						    ret = PrintColError ();
				}
				else if (sqlca.sqlcode == -201)
				{
						    ret = PrintSyntaxError ();
				}
				if (ret == 0)
				{
					CString ErrorText;
					ErrorText.Format ("fehler : %d bei : %s\n",
                            sqlca.sqlcode,
                            s);
					MessageBox (NULL, ErrorText.GetBuffer (0), "SQL-Fehler",
					        MB_OK | MB_ICONERROR);
				}
				if (SqlErrorMode == 0)
				{
					rollbackwork(); 
					ExitProcess (0); 
				}
           }
#endif
}


void DB_CLASS::Sysdate (CString& Date)
{
 time_t timer;
 struct tm *ltime;

 time (&timer);
 ltime = localtime (&timer);
 Date.Format (_T("%02d.%02d.%04d"), ltime->tm_mday,
                                   ltime->tm_mon + 1,
                                   ltime->tm_year + 1900);
}

void DB_CLASS::ToDbDate (CString& Date, void *DbDate)
{

	if (UseOdbc)
	{
		DbClass.ToDbDate (Date, (DATE_STRUCT *) DbDate);
		return;
	}
#ifdef INFORMIX
    long ldate;
	Date.Trim ();
    if (Date.GetLength () < 6)
    {
			         ldate = 0x80000000;
	                 *((long*) DbDate) = ldate;
					 return;
    }
	ldate = dasc_to_long (Date.GetBuffer (0));
//	memset (&DbDate, 0, sizeof (DbDate));
	*((long*) DbDate) = ldate;
#endif
}

void DB_CLASS::ToDbDate (LPSTR date, void *DbDate)
{
	CString Date = date;
	ToDbDate (Date, DbDate);
}

void DB_CLASS::FromDbDate (CString& Date, void *DbDate)
{

	if (UseOdbc)
	{
		DbClass.FromDbDate (Date, (DATE_STRUCT *) DbDate);
		return;
	}
#ifdef INFORMIX
	if (*((long*) DbDate) <= 1l)
	{
		Date = "";
	}
	else
	{
		TCHAR datestr [20];
		datestr[0] = 0;
		dlong_to_asc (*((long*) DbDate), datestr);
		Date = datestr;
	}
#endif
}

void DB_CLASS::FromDbDate (LPSTR date, void *DbDate)
{
	CString Date;
	FromDbDate (Date, DbDate);
	strcpy (date, Date.GetBuffer ());
}

int DB_CLASS::CompareDate (DATE_STRUCT *Date1, DATE_STRUCT *Date2)
{
	if (UseOdbc)
	{
		return DbClass.CompareDate (Date1, Date2);
	}

#ifdef INFORMIX
	long t1 = *((long*) Date1);
	long t2 = *((long*) Date2);
	if (t1 > t2) return 1;
	if (t1 < t2) return -1;
#endif
	return 0;
}

long DB_CLASS::DAscToLong (LPSTR sdate)
{
	if (UseOdbc)
	{
		return DbClass.DAscToLong (sdate);
	}
#ifdef INFORMIX
	return dasc_to_long (sdate);
#endif
	return 0l;
}

LPSTR DB_CLASS::DLongToAsc (long ldate, LPSTR sdate)
{
	if (UseOdbc)
	{
		return DbClass.DLongToAsc (ldate, sdate);
	}
#ifdef INFORMIX
	dlong_to_asc (ldate, sdate);
	return sdate;
#endif
	return "01.01.1900";
}

int DB_CLASS::PrepareTables (LPSTR Table)
{
	if (UseOdbc)
	{
		return DbClass.PrepareTables (Table);
	}
	return -1;
}

int DB_CLASS::PrepareColumns (char *Table, char *Column)
{
	if (UseOdbc)
	{
		return DbClass.PrepareColumns (Table, Column);
	}
	return -1;
}

int DB_CLASS::GetColLength ()
{
	if (UseOdbc)
	{
		return DbClass.Length;
	}
	return 0;
}


LPSTR DB_CLASS::GetColName (LPSTR name)
{
	if (UseOdbc)
	{
		return (LPSTR) DbClass.szColName;
	}
	return NULL;
}

int DB_CLASS::GetColType ()
{
	if (UseOdbc)
	{
		return DbClass.DataType;
	}
	return 0;
}


int DB_CLASS::GetColLength (int colpos, int cursor)
{
	if (UseOdbc)
	{
		return DbClass.GetColLength (colpos, cursor);
	}
	return 0;
}


LPSTR DB_CLASS::GetColName (int colpos, int cursor, LPSTR name)
{
	if (UseOdbc)
	{
		return DbClass.GetColName (colpos, cursor, name);
	}
	return NULL;
}

int DB_CLASS::GetColType (int colpos, int cursor)
{
	if (UseOdbc)
	{
		return DbClass.GetColType (colpos, cursor);
	}
	return 0;
}


