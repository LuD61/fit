#include "StdAfx.h"
#include "VLabel.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CVLabel,CLabel)

CVLabel::CVLabel(void) : CLabel ()
{
}

CVLabel::~CVLabel(void)
{
}


void CVLabel::FillRectParts (CDC &cDC, COLORREF BkColor, CRect rect)
{
	int blue = (BkColor >> 16) & 0xFF;
	int green = (BkColor >> 8) & 0xFF;
	int red = (BkColor) & 0xFF;
	int parts = 15;
	int colordiff = 5;
	int part = (rect.bottom) / parts;
	CRect rect1 (rect.left, rect.top, rect.right, part); 
	int redplus = 240 - red;
	int greenplus = 240 - green;
	int blueplus = 240 - blue;
	int redstep = redplus / parts;
	int greenstep = greenplus / parts;
	int bluestep = blueplus / parts;
	red = min (red + redplus, 240);
	green = min (green + greenplus, 240);
	blue = min (blue + blueplus, 240);
	if (part == 1) 
	{
		part = 2;
	}

	for (int i = 0; i < parts; i ++)
	{
		cDC.FillRect (&rect1, &CBrush (RGB (red, green, blue)));
		rect1.top += part - 1;
		rect1.bottom += part;
		if (rect1.top >= rect.bottom - part) return;
		red -= redstep;
		green -= greenstep;
		blue -= bluestep;
	}
	rect1.bottom = rect.bottom; 
	cDC.FillRect (&rect1, &CBrush (RGB (red, green, blue)));
}
