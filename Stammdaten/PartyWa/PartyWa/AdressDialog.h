#pragma once
#include "afxwin.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "AngebotCore.h"
#include "Datatables.h"
#include "CtrlGrid.h"
#include "Adr.h"
#include "Kun.h"
#include "afxdtctl.h"
#include "OkColorButton.h"
#include "CancelColorButton.h"
#include "OpenColorButton.h"
#include "NewColorButton.h"
#include "BkBitmap.h"
#include "VLabel.h"
#include "TextLabel.h"
#include "ChoiceAdr.h"

// CAdressDialog-Dialogfeld

class CAdressDialog : public CDialog
{
	DECLARE_DYNAMIC(CAdressDialog)

public:
	CAdressDialog(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CAdressDialog();

// Dialogfelddaten
	enum { IDD = IDD_ADR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();    // DDX/DDV-Unterstützung
	HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) ;
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	DECLARE_MESSAGE_MAP()
private:
	COLORREF m_DlgColor;
	HBRUSH m_DlgBrush;
	CBkBitmap BkBitmap;

	CString m_CaptionText;
	CVLabel m_Caption;
	CStatic m_GroupAdr;
	CStatic m_LName;
	CTextEdit m_Name1;
	CTextEdit m_Name2;
	CStatic m_LStr;
	CTextEdit m_Str;
	CStatic m_LPlzOrt;
	CTextEdit m_Plz;
	CTextEdit m_Ort1;
	CTextEdit m_Ort2;
	CStatic m_LTel;
	CTextEdit m_Tel;
	CTextLabel m_LFax;
	CTextEdit m_Fax;
	CStatic m_LEmail;
	CTextEdit m_Email;
	CStatic m_LGebDat;
	CTextEdit m_GebDat;

	CCtrlGrid CtrlGrid;
	CCtrlGrid PlzOrtGrid;
	CCtrlGrid TeleGrid;
	CFormTab Form;
	COkColorButton m_OkEx;
	CCancelColorButton m_CancelEx;
	COpenColorButton m_AdrChoice;
	CNewColorButton m_New;

	int AdrTyp;
	KUN_CLASS *Kun;
	ADR_CLASS Adr;
	CAngebotCore *AngebotCore;
	CFont Font;
	CFont CaptionFont;
	CFont StaticFont;
	long m_LiefAdr;
	BOOL m_ChoiceVisible;
	CChoiceAdr *m_ChoiceAdr;

public:
	enum
	{
		KunAdrTyp,
		LiefAdrTyp,
		RechAdrTyp
	};

	void set_ChoiceVisible (BOOL ChoiceVisible=TRUE)
	{
		m_ChoiceVisible = ChoiceVisible;
	}

	BOOL ChoiceVisible ()
	{
		return m_ChoiceVisible;
	}

	void set_LiefAdr (long lief_adr)
	{
		m_LiefAdr = lief_adr;
	}

	long LiefAdr ()
	{
		return m_LiefAdr;
	}


	void SetAdrTyp (int AdrTyp)
	{
		this->AdrTyp = AdrTyp;
	}

	void SetCaptionText (LPTSTR CaptionText)
	{
		m_CaptionText = CaptionText;
	}

	BOOL read ();
	BOOL write ();
	afx_msg void OnOK ();
	afx_msg void OnAdrChoice ();
	afx_msg void OnNew ();
public:
	CDateTimeCtrl m_GebDat2;
	afx_msg void OnEnChangeOrt3();
	CTextEdit m_Name3;
	CTextEdit m_pf_plz;
	CTextEdit m_pf;
	CStatic m_Lpf;
};
