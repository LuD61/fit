#include "StdAfx.h"
#include "mdn.h"
#include "kumebest.h"

struct KUMEBEST kumebest, kumebest_null;

static char *sqltext;

void KMB_CLASS::prepare (void)
{
         sqlin ((short *) &kumebest.mdn, SQLSHORT, 0);
         sqlin ((short *) &kumebest.fil, SQLSHORT, 0);  
         sqlin ((long *) &kumebest.kun, SQLLONG, 0);
         sqlin ((double *) &kumebest.a, SQLDOUBLE, 0); 

    sqlout ((short *) &kumebest.mdn,SQLSHORT,0);
    sqlout ((short *) &kumebest.fil,SQLSHORT,0);
    sqlout ((long *) &kumebest.kun,SQLLONG,0);
    sqlout ((TCHAR *) kumebest.kun_bran2,SQLCHAR,3);
    sqlout ((double *) &kumebest.a,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara0,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh0,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh0,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh1,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh1,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara1,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh2,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh2,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara2,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh3,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh3,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara3,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh4,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh4,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara4,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh5,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh5,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara5,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh6,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh6,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara6,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.knuepf1,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf2,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf3,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf4,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf5,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf6,SQLSHORT,0);
    sqlout ((double *) &kumebest.ean_su1,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.ean_su2,SQLDOUBLE,0);
         cursor = sqlcursor (_T("select kumebest.mdn,  ")
_T("kumebest.fil,  kumebest.kun,  kumebest.kun_bran2,  kumebest.a,  ")
_T("kumebest.tara0,  kumebest.me_einh0,  kumebest.inh0,  ")
_T("kumebest.me_einh1,  kumebest.inh1,  kumebest.tara1,  ")
_T("kumebest.me_einh2,  kumebest.inh2,  kumebest.tara2,  ")
_T("kumebest.me_einh3,  kumebest.inh3,  kumebest.tara3,  ")
_T("kumebest.me_einh4,  kumebest.inh4,  kumebest.tara4,  ")
_T("kumebest.me_einh5,  kumebest.inh5,  kumebest.tara5,  ")
_T("kumebest.me_einh6,  kumebest.inh6,  kumebest.tara6,  kumebest.knuepf1,  ")
_T("kumebest.knuepf2,  kumebest.knuepf3,  kumebest.knuepf4,  ")
_T("kumebest.knuepf5,  kumebest.knuepf6,  kumebest.ean_su1,  ")
_T("kumebest.ean_su2 from kumebest ")

#line 17 "kumebest.rpp"
                             _T("where mdn = ? ")
                             _T("and   fil = ? ")
                             _T("and   kun = ? ")
                             _T("and   a   = ? "));
         sqlin ((short *) &kumebest.mdn, SQLSHORT, 0);
         sqlin ((short *) &kumebest.fil, SQLSHORT, 0);  
         sqlin ((char *) kumebest.kun_bran2, SQLCHAR, sizeof (kumebest.kun_bran2));
         sqlin ((double *) &kumebest.a, SQLDOUBLE, 0); 

    sqlout ((short *) &kumebest.mdn,SQLSHORT,0);
    sqlout ((short *) &kumebest.fil,SQLSHORT,0);
    sqlout ((long *) &kumebest.kun,SQLLONG,0);
    sqlout ((TCHAR *) kumebest.kun_bran2,SQLCHAR,3);
    sqlout ((double *) &kumebest.a,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara0,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh0,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh0,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh1,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh1,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara1,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh2,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh2,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara2,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh3,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh3,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara3,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh4,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh4,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara4,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh5,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh5,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara5,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh6,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh6,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara6,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.knuepf1,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf2,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf3,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf4,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf5,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf6,SQLSHORT,0);
    sqlout ((double *) &kumebest.ean_su1,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.ean_su2,SQLDOUBLE,0);
         cursor_bra = sqlcursor (_T("select kumebest.mdn,  ")
_T("kumebest.fil,  kumebest.kun,  kumebest.kun_bran2,  kumebest.a,  ")
_T("kumebest.tara0,  kumebest.me_einh0,  kumebest.inh0,  ")
_T("kumebest.me_einh1,  kumebest.inh1,  kumebest.tara1,  ")
_T("kumebest.me_einh2,  kumebest.inh2,  kumebest.tara2,  ")
_T("kumebest.me_einh3,  kumebest.inh3,  kumebest.tara3,  ")
_T("kumebest.me_einh4,  kumebest.inh4,  kumebest.tara4,  ")
_T("kumebest.me_einh5,  kumebest.inh5,  kumebest.tara5,  ")
_T("kumebest.me_einh6,  kumebest.inh6,  kumebest.tara6,  kumebest.knuepf1,  ")
_T("kumebest.knuepf2,  kumebest.knuepf3,  kumebest.knuepf4,  ")
_T("kumebest.knuepf5,  kumebest.knuepf6,  kumebest.ean_su1,  ")
_T("kumebest.ean_su2 from kumebest ")

#line 27 "kumebest.rpp"
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun_bran2 = ? ")
                               _T("and   a   = ? "));
         sqlin ((short *) &kumebest.mdn, SQLSHORT, 0);
         sqlin ((short *) &kumebest.fil, SQLSHORT, 0);  
         sqlin ((double *) &kumebest.a, SQLDOUBLE, 0); 

    sqlout ((short *) &kumebest.mdn,SQLSHORT,0);
    sqlout ((short *) &kumebest.fil,SQLSHORT,0);
    sqlout ((long *) &kumebest.kun,SQLLONG,0);
    sqlout ((TCHAR *) kumebest.kun_bran2,SQLCHAR,3);
    sqlout ((double *) &kumebest.a,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara0,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh0,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh0,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh1,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh1,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara1,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh2,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh2,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara2,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh3,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh3,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara3,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh4,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh4,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara4,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh5,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh5,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara5,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.me_einh6,SQLSHORT,0);
    sqlout ((double *) &kumebest.inh6,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.tara6,SQLDOUBLE,0);
    sqlout ((short *) &kumebest.knuepf1,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf2,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf3,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf4,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf5,SQLSHORT,0);
    sqlout ((short *) &kumebest.knuepf6,SQLSHORT,0);
    sqlout ((double *) &kumebest.ean_su1,SQLDOUBLE,0);
    sqlout ((double *) &kumebest.ean_su2,SQLDOUBLE,0);
         cursor_a = sqlcursor (_T("select kumebest.mdn,  ")
_T("kumebest.fil,  kumebest.kun,  kumebest.kun_bran2,  kumebest.a,  ")
_T("kumebest.tara0,  kumebest.me_einh0,  kumebest.inh0,  ")
_T("kumebest.me_einh1,  kumebest.inh1,  kumebest.tara1,  ")
_T("kumebest.me_einh2,  kumebest.inh2,  kumebest.tara2,  ")
_T("kumebest.me_einh3,  kumebest.inh3,  kumebest.tara3,  ")
_T("kumebest.me_einh4,  kumebest.inh4,  kumebest.tara4,  ")
_T("kumebest.me_einh5,  kumebest.inh5,  kumebest.tara5,  ")
_T("kumebest.me_einh6,  kumebest.inh6,  kumebest.tara6,  kumebest.knuepf1,  ")
_T("kumebest.knuepf2,  kumebest.knuepf3,  kumebest.knuepf4,  ")
_T("kumebest.knuepf5,  kumebest.knuepf6,  kumebest.ean_su1,  ")
_T("kumebest.ean_su2 from kumebest ")

#line 36 "kumebest.rpp"
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   (kun_bran2 <= \"0\" ")
			       _T("or    kun_bran2 is NULL) ")
                               _T("and   kun = 0 ") 
                               _T("and   a   = ? "));

    sqlin ((short *) &kumebest.mdn,SQLSHORT,0);
    sqlin ((short *) &kumebest.fil,SQLSHORT,0);
    sqlin ((long *) &kumebest.kun,SQLLONG,0);
    sqlin ((TCHAR *) kumebest.kun_bran2,SQLCHAR,3);
    sqlin ((double *) &kumebest.a,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara0,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh0,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh0,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh1,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh1,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara1,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh2,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh2,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara2,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh3,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh3,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara3,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh4,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh4,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara4,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh5,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh5,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara5,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh6,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh6,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara6,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.knuepf1,SQLSHORT,0);
    sqlin ((short *) &kumebest.knuepf2,SQLSHORT,0);
    sqlin ((short *) &kumebest.knuepf3,SQLSHORT,0);
    sqlin ((short *) &kumebest.knuepf4,SQLSHORT,0);
    sqlin ((short *) &kumebest.knuepf5,SQLSHORT,0);
    sqlin ((short *) &kumebest.knuepf6,SQLSHORT,0);
    sqlin ((double *) &kumebest.ean_su1,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.ean_su2,SQLDOUBLE,0);
         sqltext = _T("update kumebest set kumebest.mdn = ?,  ")
_T("kumebest.fil = ?,  kumebest.kun = ?,  kumebest.kun_bran2 = ?,  ")
_T("kumebest.a = ?,  kumebest.tara0 = ?,  kumebest.me_einh0 = ?,  ")
_T("kumebest.inh0 = ?,  kumebest.me_einh1 = ?,  kumebest.inh1 = ?,  ")
_T("kumebest.tara1 = ?,  kumebest.me_einh2 = ?,  kumebest.inh2 = ?,  ")
_T("kumebest.tara2 = ?,  kumebest.me_einh3 = ?,  kumebest.inh3 = ?,  ")
_T("kumebest.tara3 = ?,  kumebest.me_einh4 = ?,  kumebest.inh4 = ?,  ")
_T("kumebest.tara4 = ?,  kumebest.me_einh5 = ?,  kumebest.inh5 = ?,  ")
_T("kumebest.tara5 = ?,  kumebest.me_einh6 = ?,  kumebest.inh6 = ?,  ")
_T("kumebest.tara6 = ?,  kumebest.knuepf1 = ?,  kumebest.knuepf2 = ?,  ")
_T("kumebest.knuepf3 = ?,  kumebest.knuepf4 = ?,  kumebest.knuepf5 = ?,  ")
_T("kumebest.knuepf6 = ?,  kumebest.ean_su1 = ?,  kumebest.ean_su2 = ? ")

#line 44 "kumebest.rpp"
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_bran2 = ? ")
                               _T("and   a   = ? ");
  
         sqlin ((short *) &kumebest.mdn, SQLSHORT, 0);
         sqlin ((short *) &kumebest.fil, SQLSHORT, 0);  
         sqlin ((long *) &kumebest.kun, SQLLONG, 0);
         sqlin ((char *) kumebest.kun_bran2, SQLCHAR, sizeof (kumebest.kun_bran2));
         sqlin ((double *) &kumebest.a, SQLDOUBLE, 0); 

         upd_cursor = sqlcursor (sqltext);

    sqlin ((short *) &kumebest.mdn,SQLSHORT,0);
    sqlin ((short *) &kumebest.fil,SQLSHORT,0);
    sqlin ((long *) &kumebest.kun,SQLLONG,0);
    sqlin ((TCHAR *) kumebest.kun_bran2,SQLCHAR,3);
    sqlin ((double *) &kumebest.a,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara0,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh0,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh0,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh1,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh1,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara1,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh2,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh2,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara2,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh3,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh3,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara3,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh4,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh4,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara4,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh5,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh5,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara5,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.me_einh6,SQLSHORT,0);
    sqlin ((double *) &kumebest.inh6,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.tara6,SQLDOUBLE,0);
    sqlin ((short *) &kumebest.knuepf1,SQLSHORT,0);
    sqlin ((short *) &kumebest.knuepf2,SQLSHORT,0);
    sqlin ((short *) &kumebest.knuepf3,SQLSHORT,0);
    sqlin ((short *) &kumebest.knuepf4,SQLSHORT,0);
    sqlin ((short *) &kumebest.knuepf5,SQLSHORT,0);
    sqlin ((short *) &kumebest.knuepf6,SQLSHORT,0);
    sqlin ((double *) &kumebest.ean_su1,SQLDOUBLE,0);
    sqlin ((double *) &kumebest.ean_su2,SQLDOUBLE,0);
         ins_cursor = sqlcursor (_T("insert into kumebest (")
_T("mdn,  fil,  kun,  kun_bran2,  a,  tara0,  me_einh0,  inh0,  me_einh1,  inh1,  tara1,  ")
_T("me_einh2,  inh2,  tara2,  me_einh3,  inh3,  tara3,  me_einh4,  inh4,  tara4,  me_einh5,  ")
_T("inh5,  tara5,  me_einh6,  inh6,  tara6,  knuepf1,  knuepf2,  knuepf3,  knuepf4,  ")
_T("knuepf5,  knuepf6,  ean_su1,  ean_su2) ")

#line 59 "kumebest.rpp"
                                   _T("values ")
                                   _T("(?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));

#line 61 "kumebest.rpp"

         sqlin ((short *) &kumebest.mdn, SQLSHORT, 0);
         sqlin ((short *) &kumebest.fil, SQLSHORT, 0);  
         sqlin ((long *) &kumebest.kun, SQLLONG, 0);
         sqlin ((char *) kumebest.kun_bran2, SQLCHAR, sizeof (kumebest.kun_bran2));
         sqlin ((double *) &kumebest.a, SQLDOUBLE, 0); 
         del_cursor = sqlcursor (_T("delete from kumebest ")
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_bran2 = ? ")
                               _T("and   a   = ? "));

         sqlin ((short *) &kumebest.mdn, SQLSHORT, 0);
         sqlin ((short *) &kumebest.fil, SQLSHORT, 0);  
         sqlin ((long *) &kumebest.kun, SQLLONG, 0);
         sqlin ((char *) kumebest.kun_bran2, SQLCHAR, sizeof (kumebest.kun_bran2));
         sqlin ((double *) &kumebest.a, SQLDOUBLE, 0); 
         test_upd_cursor = sqlcursor (_T("select a from kumebest ")
                               _T("where mdn = ? ")
                               _T("and   fil = ? ")
                               _T("and   kun = ? ")
                               _T("and   kun_bran2 = ? ")
                               _T("and   a   = ? "));
}

int KMB_CLASS::dbreadfirst_bra (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         sqlopen (cursor_bra);
         int dsqlstatus = sqlfetch (cursor_bra);
         if (dsqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int KMB_CLASS::dbread_bra (void)
/**
Naechsten Satz aus Tabelle lesen.
**/
{
         int dsqlstatus = sqlfetch (cursor_bra);
         if (dsqlstatus == 0)
         {
                return 0;
         }
         return 100;
}
int KMB_CLASS::dbreadfirst_a (void)
/**
Ersten Satz aus Tabelle lesen.
**/
{
         if (cursor == -1)
         {
                this->prepare ();
         }
         sqlopen (cursor_a);
         int dsqlstatus = sqlfetch (cursor_a);
         if (dsqlstatus == 0)
         {
                return 0;
         }
         return 100;
}

int KMB_CLASS::dbread_a (void)
/**
Naechsten Satz aus Tabelle lesen.
**/
{
         int dsqlstatus = sqlfetch (cursor_a);
         if (dsqlstatus == 0)
         {
                return 0;
         }
         return 100;
}
 
 
