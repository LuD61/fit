#include "stdafx.h"
#include "aufk.h"

struct AUFK aufk, aufk_null;

void AUFK_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &aufk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &aufk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &aufk.auf,   SQLLONG, 0);
    sqlout ((long *) &aufk.mdn,SQLLONG,0);
    sqlout ((short *) &aufk.fil,SQLSHORT,0);
    sqlout ((long *) &aufk.ang,SQLLONG,0);
    sqlout ((long *) &aufk.auf,SQLLONG,0);
    sqlout ((long *) &aufk.adr,SQLLONG,0);
    sqlout ((short *) &aufk.kun_fil,SQLSHORT,0);
    sqlout ((long *) &aufk.kun,SQLLONG,0);
    sqlout ((DATE_STRUCT *) &aufk.lieferdat,SQLDATE,0);
    sqlout ((TCHAR *) aufk.lieferzeit,SQLCHAR,6);
    sqlout ((TCHAR *) aufk.hinweis,SQLCHAR,49);
    sqlout ((short *) &aufk.auf_stat,SQLSHORT,0);
    sqlout ((TCHAR *) aufk.kun_krz1,SQLCHAR,17);
    sqlout ((TCHAR *) aufk.feld_bz1,SQLCHAR,20);
    sqlout ((TCHAR *) aufk.feld_bz2,SQLCHAR,12);
    sqlout ((TCHAR *) aufk.feld_bz3,SQLCHAR,8);
    sqlout ((short *) &aufk.delstatus,SQLSHORT,0);
    sqlout ((double *) &aufk.zeit_dec,SQLDOUBLE,0);
    sqlout ((long *) &aufk.kopf_txt,SQLLONG,0);
    sqlout ((long *) &aufk.fuss_txt,SQLLONG,0);
    sqlout ((long *) &aufk.vertr,SQLLONG,0);
    sqlout ((TCHAR *) aufk.auf_ext,SQLCHAR,17);
    sqlout ((long *) &aufk.tou,SQLLONG,0);
    sqlout ((TCHAR *) aufk.pers_nam,SQLCHAR,9);
    sqlout ((DATE_STRUCT *) &aufk.komm_dat,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &aufk.best_dat,SQLDATE,0);
    sqlout ((short *) &aufk.waehrung,SQLSHORT,0);
    sqlout ((short *) &aufk.auf_art,SQLSHORT,0);
    sqlout ((short *) &aufk.gruppe,SQLSHORT,0);
    sqlout ((short *) &aufk.ccmarkt,SQLSHORT,0);
    sqlout ((short *) &aufk.fak_typ,SQLSHORT,0);
    sqlout ((long *) &aufk.tou_nr,SQLLONG,0);
    sqlout ((TCHAR *) aufk.ueb_kz,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &aufk.fix_dat,SQLDATE,0);
    sqlout ((TCHAR *) aufk.komm_name,SQLCHAR,13);
    sqlout ((DATE_STRUCT *) &aufk.akv,SQLDATE,0);
    sqlout ((TCHAR *) aufk.akv_zeit,SQLCHAR,6);
    sqlout ((DATE_STRUCT *) &aufk.bearb,SQLDATE,0);
    sqlout ((TCHAR *) aufk.bearb_zeit,SQLCHAR,6);
    sqlout ((short *) &aufk.psteuer_kz,SQLSHORT,0);
            cursor = sqlcursor (_T("select aufk.mdn,  aufk.fil,  ")
_T("aufk.ang,  aufk.auf,  aufk.adr,  aufk.kun_fil,  aufk.kun,  aufk.lieferdat,  ")
_T("aufk.lieferzeit,  aufk.hinweis,  aufk.auf_stat,  aufk.kun_krz1,  ")
_T("aufk.feld_bz1,  aufk.feld_bz2,  aufk.feld_bz3,  aufk.delstatus,  ")
_T("aufk.zeit_dec,  aufk.kopf_txt,  aufk.fuss_txt,  aufk.vertr,  ")
_T("aufk.auf_ext,  aufk.tou,  aufk.pers_nam,  aufk.komm_dat,  aufk.best_dat,  ")
_T("aufk.waehrung,  aufk.auf_art,  aufk.gruppe,  aufk.ccmarkt,  aufk.fak_typ,  ")
_T("aufk.tou_nr,  aufk.ueb_kz,  aufk.fix_dat,  aufk.komm_name,  aufk.akv,  ")
_T("aufk.akv_zeit,  aufk.bearb,  aufk.bearb_zeit,  aufk.psteuer_kz from aufk ")

#line 14 "Aufk.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and auf = ? ")
				);
    sqlin ((long *) &aufk.mdn,SQLLONG,0);
    sqlin ((short *) &aufk.fil,SQLSHORT,0);
    sqlin ((long *) &aufk.ang,SQLLONG,0);
    sqlin ((long *) &aufk.auf,SQLLONG,0);
    sqlin ((long *) &aufk.adr,SQLLONG,0);
    sqlin ((short *) &aufk.kun_fil,SQLSHORT,0);
    sqlin ((long *) &aufk.kun,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &aufk.lieferdat,SQLDATE,0);
    sqlin ((TCHAR *) aufk.lieferzeit,SQLCHAR,6);
    sqlin ((TCHAR *) aufk.hinweis,SQLCHAR,49);
    sqlin ((short *) &aufk.auf_stat,SQLSHORT,0);
    sqlin ((TCHAR *) aufk.kun_krz1,SQLCHAR,17);
    sqlin ((TCHAR *) aufk.feld_bz1,SQLCHAR,20);
    sqlin ((TCHAR *) aufk.feld_bz2,SQLCHAR,12);
    sqlin ((TCHAR *) aufk.feld_bz3,SQLCHAR,8);
    sqlin ((short *) &aufk.delstatus,SQLSHORT,0);
    sqlin ((double *) &aufk.zeit_dec,SQLDOUBLE,0);
    sqlin ((long *) &aufk.kopf_txt,SQLLONG,0);
    sqlin ((long *) &aufk.fuss_txt,SQLLONG,0);
    sqlin ((long *) &aufk.vertr,SQLLONG,0);
    sqlin ((TCHAR *) aufk.auf_ext,SQLCHAR,17);
    sqlin ((long *) &aufk.tou,SQLLONG,0);
    sqlin ((TCHAR *) aufk.pers_nam,SQLCHAR,9);
    sqlin ((DATE_STRUCT *) &aufk.komm_dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &aufk.best_dat,SQLDATE,0);
    sqlin ((short *) &aufk.waehrung,SQLSHORT,0);
    sqlin ((short *) &aufk.auf_art,SQLSHORT,0);
    sqlin ((short *) &aufk.gruppe,SQLSHORT,0);
    sqlin ((short *) &aufk.ccmarkt,SQLSHORT,0);
    sqlin ((short *) &aufk.fak_typ,SQLSHORT,0);
    sqlin ((long *) &aufk.tou_nr,SQLLONG,0);
    sqlin ((TCHAR *) aufk.ueb_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &aufk.fix_dat,SQLDATE,0);
    sqlin ((TCHAR *) aufk.komm_name,SQLCHAR,13);
    sqlin ((DATE_STRUCT *) &aufk.akv,SQLDATE,0);
    sqlin ((TCHAR *) aufk.akv_zeit,SQLCHAR,6);
    sqlin ((DATE_STRUCT *) &aufk.bearb,SQLDATE,0);
    sqlin ((TCHAR *) aufk.bearb_zeit,SQLCHAR,6);
    sqlin ((short *) &aufk.psteuer_kz,SQLSHORT,0);
            sqltext = _T("update aufk set aufk.mdn = ?,  ")
_T("aufk.fil = ?,  aufk.ang = ?,  aufk.auf = ?,  aufk.adr = ?,  ")
_T("aufk.kun_fil = ?,  aufk.kun = ?,  aufk.lieferdat = ?,  ")
_T("aufk.lieferzeit = ?,  aufk.hinweis = ?,  aufk.auf_stat = ?,  ")
_T("aufk.kun_krz1 = ?,  aufk.feld_bz1 = ?,  aufk.feld_bz2 = ?,  ")
_T("aufk.feld_bz3 = ?,  aufk.delstatus = ?,  aufk.zeit_dec = ?,  ")
_T("aufk.kopf_txt = ?,  aufk.fuss_txt = ?,  aufk.vertr = ?,  ")
_T("aufk.auf_ext = ?,  aufk.tou = ?,  aufk.pers_nam = ?,  ")
_T("aufk.komm_dat = ?,  aufk.best_dat = ?,  aufk.waehrung = ?,  ")
_T("aufk.auf_art = ?,  aufk.gruppe = ?,  aufk.ccmarkt = ?,  ")
_T("aufk.fak_typ = ?,  aufk.tou_nr = ?,  aufk.ueb_kz = ?,  ")
_T("aufk.fix_dat = ?,  aufk.komm_name = ?,  aufk.akv = ?,  ")
_T("aufk.akv_zeit = ?,  aufk.bearb = ?,  aufk.bearb_zeit = ?,  ")
_T("aufk.psteuer_kz = ? ")

#line 19 "Aufk.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and auf = ? ");
            sqlin ((short *)   &aufk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &aufk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &aufk.auf,   SQLLONG, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &aufk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &aufk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &aufk.auf,   SQLLONG, 0);
            test_upd_cursor = sqlcursor (_T("select auf from aufk ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and auf = ? ")
				);
            sqlin ((short *)   &aufk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &aufk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &aufk.auf,   SQLLONG, 0);
            test_lock_cursor = sqlcursor (_T("update aufk set delstatus = -1 ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and auf = ? ")
				);
            sqlin ((short *)   &aufk.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &aufk.fil,  SQLSHORT, 0);
            sqlin ((long *)   &aufk.auf,   SQLLONG, 0);
            del_cursor = sqlcursor (_T("delete from aufk ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and auf = ? ")
				);
    sqlin ((long *) &aufk.mdn,SQLLONG,0);
    sqlin ((short *) &aufk.fil,SQLSHORT,0);
    sqlin ((long *) &aufk.ang,SQLLONG,0);
    sqlin ((long *) &aufk.auf,SQLLONG,0);
    sqlin ((long *) &aufk.adr,SQLLONG,0);
    sqlin ((short *) &aufk.kun_fil,SQLSHORT,0);
    sqlin ((long *) &aufk.kun,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &aufk.lieferdat,SQLDATE,0);
    sqlin ((TCHAR *) aufk.lieferzeit,SQLCHAR,6);
    sqlin ((TCHAR *) aufk.hinweis,SQLCHAR,49);
    sqlin ((short *) &aufk.auf_stat,SQLSHORT,0);
    sqlin ((TCHAR *) aufk.kun_krz1,SQLCHAR,17);
    sqlin ((TCHAR *) aufk.feld_bz1,SQLCHAR,20);
    sqlin ((TCHAR *) aufk.feld_bz2,SQLCHAR,12);
    sqlin ((TCHAR *) aufk.feld_bz3,SQLCHAR,8);
    sqlin ((short *) &aufk.delstatus,SQLSHORT,0);
    sqlin ((double *) &aufk.zeit_dec,SQLDOUBLE,0);
    sqlin ((long *) &aufk.kopf_txt,SQLLONG,0);
    sqlin ((long *) &aufk.fuss_txt,SQLLONG,0);
    sqlin ((long *) &aufk.vertr,SQLLONG,0);
    sqlin ((TCHAR *) aufk.auf_ext,SQLCHAR,17);
    sqlin ((long *) &aufk.tou,SQLLONG,0);
    sqlin ((TCHAR *) aufk.pers_nam,SQLCHAR,9);
    sqlin ((DATE_STRUCT *) &aufk.komm_dat,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &aufk.best_dat,SQLDATE,0);
    sqlin ((short *) &aufk.waehrung,SQLSHORT,0);
    sqlin ((short *) &aufk.auf_art,SQLSHORT,0);
    sqlin ((short *) &aufk.gruppe,SQLSHORT,0);
    sqlin ((short *) &aufk.ccmarkt,SQLSHORT,0);
    sqlin ((short *) &aufk.fak_typ,SQLSHORT,0);
    sqlin ((long *) &aufk.tou_nr,SQLLONG,0);
    sqlin ((TCHAR *) aufk.ueb_kz,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &aufk.fix_dat,SQLDATE,0);
    sqlin ((TCHAR *) aufk.komm_name,SQLCHAR,13);
    sqlin ((DATE_STRUCT *) &aufk.akv,SQLDATE,0);
    sqlin ((TCHAR *) aufk.akv_zeit,SQLCHAR,6);
    sqlin ((DATE_STRUCT *) &aufk.bearb,SQLDATE,0);
    sqlin ((TCHAR *) aufk.bearb_zeit,SQLCHAR,6);
    sqlin ((short *) &aufk.psteuer_kz,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into aufk (")
_T("mdn,  fil,  ang,  auf,  adr,  kun_fil,  kun,  lieferdat,  lieferzeit,  hinweis,  auf_stat,  ")
_T("kun_krz1,  feld_bz1,  feld_bz2,  feld_bz3,  delstatus,  zeit_dec,  kopf_txt,  ")
_T("fuss_txt,  vertr,  auf_ext,  tou,  pers_nam,  komm_dat,  best_dat,  waehrung,  ")
_T("auf_art,  gruppe,  ccmarkt,  fak_typ,  tou_nr,  ueb_kz,  fix_dat,  komm_name,  akv,  ")
_T("akv_zeit,  bearb,  bearb_zeit,  psteuer_kz) ")

#line 52 "Aufk.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?)"));

#line 54 "Aufk.rpp"
}
