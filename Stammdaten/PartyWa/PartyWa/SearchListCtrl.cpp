#include "StdAfx.h"
#include "searchlistctrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNAMIC(CSearchListCtrl, CObject)

CSearchListCtrl::CSearchListCtrl(void)
{
}

CSearchListCtrl::~CSearchListCtrl(void)
{
	DestroyWindow ();
}

void CSearchListCtrl::Create (RECT &rect, CWnd *pParent, UINT nIDEdit, UINT nIDButton, int Align)
{

    CRect ButtonRect;
	ButtonRect = rect;
    int ButtonWidth = 4 * (rect.bottom - rect.top);
	ButtonWidth /= 4;
	ButtonRect.left = ButtonRect.right - ButtonWidth;
	Button.Create (_T(".."),
		           BS_PUSHBUTTON | BS_CENTER | 
				   BS_TEXT | BS_BOTTOM | WS_VISIBLE | WS_CHILD,
		           ButtonRect, pParent, nIDButton);
	Button.ModifyStyle (WS_TABSTOP, 0);
	CRect EditRect;
	EditRect = rect;
	EditRect.right = ButtonRect.left - 1;
	if (Align == RIGHT)
	{
		Edit.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_RIGHT | ES_AUTOHSCROLL, 
			             EditRect, pParent, nIDEdit);
	}
	else
	{
		Edit.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | ES_AUTOVSCROLL | ES_MULTILINE | ES_AUTOHSCROLL, 
			              EditRect, pParent, nIDEdit);
	}
}

void CSearchListCtrl::DestroyWindow ()
{
	if (IsWindow (Edit.m_hWnd))
	{
		Edit.DestroyWindow ();
	}
	if (IsWindow (Button.m_hWnd))
	{
		Button.DestroyWindow ();
	}
}
