// PartyService.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "PartyWa.h"
#include "PartyService.h"
#include "Datatables.h"
#include "AngebotCore.h"
#include "KunDiverse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


int StdCellHeight = 15;
int bsp = -6; 

// CPartyService

IMPLEMENT_DYNCREATE(CPartyService, CFormView)

CPartyService::CPartyService()
	: CFormView(CPartyService::IDD)
{
	FontHeight = 95;
//	DlgBkColor = RGB (51, 51, 51);
	DlgBkColor = RGB (153, 153, 153);
//	DlgBkColor = RGB (204, 204, 204);
	DlgBrush = NULL;
	TitleBrush = NULL;
	Activate = NULL;
}

CPartyService::~CPartyService()
{

	CDatatables::DeleteInstance ();
	CAngebotCore::DeleteInstance ();
	CKunDiverse::DeleteInstance ();
	if (Activate != NULL)
	{
		delete Activate;
	}
}

void CPartyService::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BANGEBOTE, m_BAngebote);
	DDX_Control(pDX, IDC_BAUFTRAEGE, m_BAuftraege);
	DDX_Control(pDX, IDC_BANGEBOTSTYPEN, m_BAngebotsTypen);
	DDX_Control(pDX, IDC_BKAUFTRAEGE, m_BKAuftraege);
	DDX_Control(pDX, IDC_PATRY_SERVICE_TITLE, m_PartyServiceTitle);
}

BEGIN_MESSAGE_MAP(CPartyService, CFormView)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_COMMAND (IDC_BANGEBOTE, OnAngebote)
	ON_COMMAND (ID_ANGEBOT, OnAngebote)
	ON_COMMAND (IDC_BAUFTRAEGE, OnAuftraege)
	ON_COMMAND (ID_AUFTRAEGE, OnAuftraege)
	ON_COMMAND (IDC_BANGEBOTSTYPEN, OnAngTypes)
	ON_COMMAND (ID_ANGEBOTSTYPEN, OnAngTypes)
	ON_COMMAND (IDC_BKAUFTRAEGE, OnKAuftraege)
	ON_COMMAND (ID_KAUFTRAEGE, OnKAuftraege)
END_MESSAGE_MAP()

void CPartyService::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	MessageHandler = CMessageHandler::GetInstance ();
	m_BAngebote.nID = IDC_BANGEBOTE;
	m_BAngebote.Orientation = m_BAngebote.Left;
	m_BAngebote.TextStyle = m_BAngebote.Bold;
	m_BAngebote.BorderStyle = m_BAngebote.Solide;
	m_BAngebote.DynamicColor = TRUE;
	m_BAngebote.TextColor =RGB (0, 0, 0);
	m_BAngebote.SetBkColor (RGB (100, 180, 255));
	m_BAngebote.DynColor = m_BAngebote.DynColorBlue;
//	m_BAngebote.RolloverColor = m_BAngebote.DynColorBlue;
	m_BAngebote.RolloverColor = RGB (204, 204, 255);
	m_BAngebote.SetWindowText (_T(" Angebot"));
	m_BAngebote.LoadBitmap (IDB_ANGEBOT);
	m_BAngebote.LoadMask (IDB_ANGEBOT_MASK);
	m_BAngebote.Container = this;
	Container.Add (&m_BAngebote);

	m_BAuftraege.nID = IDC_BAUFTRAEGE;
	m_BAuftraege.Orientation = m_BAngebote.Left;
	m_BAuftraege.TextStyle = m_BAngebote.Bold;
	m_BAuftraege.BorderStyle = m_BAuftraege.Solide;
	m_BAuftraege.DynamicColor = TRUE;
	m_BAuftraege.TextColor =RGB (0, 0, 0);
	m_BAuftraege.SetBkColor (RGB (100, 180, 255));
	m_BAuftraege.DynColor = m_BAngebote.DynColorBlue;
//	m_BAuftraege.RolloverColor = m_BAngebote.DynColorBlue;
	m_BAuftraege.RolloverColor = RGB (204, 204, 255);
	m_BAuftraege.SetWindowText (_T(" Auftrag"));
	m_BAuftraege.LoadBitmap (IDB_AUFTRAG);
	m_BAuftraege.LoadMask (IDB_AUFTRAG_MASK);
	m_BAuftraege.Container = this;
	Container.Add (&m_BAuftraege);

	m_BAngebotsTypen.nID = IDC_BANGEBOTSTYPEN;
	m_BAngebotsTypen.Orientation = m_BAngebote.Left;
	m_BAngebotsTypen.TextStyle = m_BAngebote.Bold;
	m_BAngebotsTypen.BorderStyle = m_BAngebote.Solide;
	m_BAngebotsTypen.DynamicColor = TRUE;
	m_BAngebotsTypen.TextColor =RGB (0, 0, 0);
	m_BAngebotsTypen.SetBkColor (RGB (100, 180, 255));
	m_BAngebotsTypen.DynColor = m_BAngebote.DynColorBlue;
//	m_BAngebote.RolloverColor = m_BAngebote.DynColorBlue;
	m_BAngebotsTypen.RolloverColor = RGB (204, 204, 255);
	m_BAngebotsTypen.SetWindowText (_T(" Angebotsvorlagen"));
	m_BAngebotsTypen.LoadBitmap (IDB_ANGEBOTTYPES);
	m_BAngebotsTypen.LoadMask (IDB_ANGEBOTTYPES_MASK);
	m_BAngebotsTypen.Container = this;
	Container.Add (&m_BAngebotsTypen);

	m_BKAuftraege.nID = IDC_BKAUFTRAEGE;
	m_BKAuftraege.Orientation = m_BAngebote.Left;
	m_BKAuftraege.TextStyle = m_BAngebote.Bold;
	m_BKAuftraege.BorderStyle = m_BAngebote.Solide;
	m_BKAuftraege.DynamicColor = TRUE;
	m_BKAuftraege.TextColor =RGB (0, 0, 0);
	m_BKAuftraege.SetBkColor (RGB (100, 180, 255));
	m_BKAuftraege.DynColor = m_BAngebote.DynColorBlue;
//	m_BAngebote.RolloverColor = m_BAngebote.DynColorBlue;
	m_BKAuftraege.RolloverColor = RGB (204, 204, 255);
	m_BKAuftraege.SetWindowText (_T(" Kundenaufträge"));
	m_BKAuftraege.LoadBitmap (IDB_KAUFTRAEGE);
	m_BKAuftraege.LoadMask (IDB_KAUFTRAEGE_MASK);
	m_BKAuftraege.Container = this;
	Container.Add (&m_BKAuftraege);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (FontHeight, _T("Dlg"));
	}

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	l.lfHeight = 21;
	TitleFont.CreateFontIndirect (&l);

    CRect ButtonRect;
	m_BAngebote.GetClientRect (&ButtonRect);

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (ButtonRect.bottom + 6);
//    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (0, 0);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_PartyServiceTitle = new CCtrlInfo (&m_PartyServiceTitle, 0, 0, DOCKRIGHT, 1);
	CtrlGrid.Add (c_PartyServiceTitle);
	CCtrlInfo *c_BTypes = new CCtrlInfo (&m_BAngebote, 0, 1, DOCKRIGHT, 1);
	c_BTypes->SetCellPos (0, -5);
	CtrlGrid.Add (c_BTypes);
	CCtrlInfo *c_BAuftraege = new CCtrlInfo (&m_BAuftraege, 0, 2, DOCKRIGHT, 1);
	c_BAuftraege->SetCellPos (0, bsp);
	CtrlGrid.Add (c_BAuftraege);
	CCtrlInfo *c_BAngebotsTypen = new CCtrlInfo (&m_BAngebotsTypen, 0, 3, DOCKRIGHT, 1);
	c_BAngebotsTypen->SetCellPos (0, bsp - 1);
	CtrlGrid.Add (c_BAngebotsTypen);
	CCtrlInfo *c_BKAuftraege = new CCtrlInfo (&m_BKAuftraege, 0, 4, DOCKRIGHT, 1);
	c_BKAuftraege->SetCellPos (0, bsp - 2);
	CtrlGrid.Add (c_BKAuftraege);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_PartyServiceTitle.SetFont (&TitleFont);

	CtrlGrid.Display ();
	Activate = new CActivate (this);
	CAngebotCore::GetInstance ()->SetActivate (Activate);
	OnAngebote ();
}


HBRUSH CPartyService::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
//			m_MoreKto.SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (pWnd == &m_PartyServiceTitle)
	{
		pDC->SetTextColor (RGB (255,255,255));
		pDC->SetBkColor (RGB (102,102,102));
		if (TitleBrush == NULL)
		{
			TitleBrush = CreateSolidBrush (RGB (102,102,102));
		}
		return TitleBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CPartyService::OnSize (UINT nType, int cx, int cy)
{
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
	CtrlGrid.Display ();
}

void CPartyService::OnAngebote ()
{
	m_BAngebote.SetActive (TRUE);
	m_BAuftraege.SetActive (FALSE);
	m_BAngebotsTypen.SetActive (FALSE);
	m_BKAuftraege.SetActive (FALSE);
	MessageHandler->RunMessage (IDC_BANGEBOTE);
}

void CPartyService::OnAuftraege ()
{
	m_BAngebote.SetActive (FALSE);
	m_BAuftraege.SetActive (TRUE);
	m_BAngebotsTypen.SetActive (FALSE);
	m_BKAuftraege.SetActive (FALSE);
	MessageHandler->RunMessage (IDC_BAUFTRAEGE);
}

void CPartyService::OnAngTypes ()
{
	m_BAngebote.SetActive (FALSE);
	m_BAuftraege.SetActive (FALSE);
	m_BAngebotsTypen.SetActive (TRUE);
	m_BKAuftraege.SetActive (FALSE);
	MessageHandler->RunMessage (IDC_BANGEBOTSTYPEN);
}

void CPartyService::OnKAuftraege ()
{
	m_BAngebote.SetActive (FALSE);
	m_BAuftraege.SetActive (FALSE);
	m_BAngebotsTypen.SetActive (FALSE);
	m_BKAuftraege.SetActive (TRUE);
	MessageHandler->RunMessage (IDC_BKAUFTRAEGE);
}

CPartyService::CActivate::CActivate (CPartyService *p)
{
	this->p = p;
}

void CPartyService::CActivate::Run ()
{
	CAngebotCore *AngebotCore =  CAngebotCore::GetInstance ();
	switch (AngebotCore->PartySelect)
	{
		case CAngebotCore::ActiveAngebot:
			p->OnAngebote ();
			break;
		case CAngebotCore::ActiveAuftrag:
			p->OnAuftraege ();
			break;
		case CAngebotCore::ActiveAngebotsTypen:
			p->OnAngTypes ();
			break;
		case CAngebotCore::ActiveKAuftraege:
			p->OnKAuftraege ();
			break;
	}
}

void CPartyService::ElementSetFocus (CWnd *cWnd)
{
	CColorButton **it;
	CColorButton *c;

	Container.Start ();
	while ((it = Container.GetNext ()) != NULL)
	{
		c = *it;
		if (c != cWnd)
		{
			c->SetActive (FALSE);
		}
	}
}


// CPartyService-Diagnose

#ifdef _DEBUG
void CPartyService::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPartyService::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPartyService-Meldungshandler

