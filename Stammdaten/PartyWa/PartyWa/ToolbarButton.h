#pragma once
#include "afxwin.h"


class CToolbarButton :
	public CButton
{
DECLARE_DYNCREATE(CToolbarButton)
public:
	CToolbarButton(void);
public:
	~CToolbarButton(void);
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSetFocus (CWnd *oldFocus);
private:
	BOOL SetFocusActive;
};
