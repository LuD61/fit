#pragma once
#include "afxwin.h"

class CListComboBox :
	public CComboBox
{
public:
	CListComboBox(void);
	~CListComboBox(void);
protected :
	DECLARE_MESSAGE_MAP()
    afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};
