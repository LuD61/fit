// AngebotView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "PartyWa.h"
#include "AngebotView.h"
#include "Util.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAngebotView

IMPLEMENT_DYNCREATE(CAngebotView, CFormView)

static int StdCellHeight = 15;

CAngebotView::CAngebotView()
	: CFormView(CAngebotView::IDD)
{
	FontHeight = 95;
	DlgBkColor = NULL;
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBkColor = RGB (168, 193, 218);
	DlgBkColor = RGB (235, 235, 245);
	DlgBrush = NULL;
	TitleBrush = NULL;
	AngebotMessage = NULL;
	AuftragMessage = NULL;
	AngTypesMessage = NULL;
	KAuftraegeMessage = NULL;
	TakeAng = NULL;
	ChoiceAng = NULL;
	ChoiceAuf = NULL;
	FromResource = FALSE;
	AngChoiceAnd = _T("");
}

CAngebotView::~CAngebotView()
{
	if (AngebotMessage != NULL)
	{
		MessageHandler->Unregister (AngebotMessage);
		delete AngebotMessage;
	}
	if (AuftragMessage != NULL)
	{
		MessageHandler->Unregister (AuftragMessage);
		delete AuftragMessage;
	}
	if (AngTypesMessage != NULL)
	{
		MessageHandler->Unregister (AngTypesMessage);
		delete AngTypesMessage;
	}
	if (KAuftraegeMessage != NULL)
	{
		MessageHandler->Unregister (KAuftraegeMessage);
		delete KAuftraegeMessage;
	}
	if (TakeAng != NULL)
	{
//		MessageHandler->Unregister (TakeAng);
		delete TakeAng;
	}
	if (ChoiceAng != NULL)
	{
		delete ChoiceAng;
		ChoiceAng = NULL;
	}
	if (ChoiceAuf != NULL)
	{
		delete ChoiceAuf;
		ChoiceAuf = NULL;
	}

}

void CAngebotView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ANGEBOT_TITLE, m_AngebotTitle);
}

BEGIN_MESSAGE_MAP(CAngebotView, CFormView)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
END_MESSAGE_MAP()


// CAngebotView-Diagnose

void CAngebotView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	CString PersNam;
	CUtil::GetPersName (PersNam);
	PersNam.Trim ();
	if (PersNam != _T(""))
	{
		CAngebotCore::GetInstance ()->SetPersNam (PersNam);
	}


	m_AngebotPage.Create (IDD_ANGEBOT_PAGE, this);
	m_AngebotPage.ShowWindow (SW_HIDE);
	m_AngebotPage.UpdateWindow ();
	Page = CAngebotCore::PageAngebot;
	CAngebotCore::GetInstance ()->SetPage (Page);
	CAngebotCore::GetInstance ()->SetAng ();

	m_AuftragPage.Create (IDD_AUFTRAG_PAGE, this);
	m_AuftragPage.ShowWindow (SW_HIDE);
	m_AuftragPage.UpdateWindow ();
	Page = CAngebotCore::PageAuftrag;
	CAngebotCore::GetInstance ()->SetPage (Page);
	CAngebotCore::GetInstance ()->SetAng ();

	MessageHandler = CMessageHandler::GetInstance ();
	AngebotMessage = new CAngebotMessage (this);
	AuftragMessage = new CAuftragMessage (this);
	AngTypesMessage = new CAngTypesMessage (this);
	KAuftraegeMessage = new CKAuftraegeMessage (this);
	TakeAng = new CTakeAng (this);
	MessageHandler->Register (AngebotMessage);
	MessageHandler->Register (AuftragMessage);
	MessageHandler->Register (AngTypesMessage);
	MessageHandler->Register (KAuftraegeMessage);
	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (FontHeight, _T("Dlg"));
	}
	m_AngebotTitle.SetBoderStyle (CLabel::Solide);
	m_AngebotTitle.SetDynamicColor (TRUE);
//	m_AngebotTitle.SetBkColor (RGB (100,180,255));

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	l.lfHeight = 21;
	TitleFont.CreateFontIndirect (&l);

    CtrlGrid.Create (this, 20, 20);
	CtrlGrid.SetFont (&Font);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (15);
//    CtrlGrid.SetFontCellHeight (this, &Font);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    AngebotGrid.Create (this, 20, 20);
    AngebotGrid.SetBorder (12, 20);
    AngebotGrid.SetCellHeight (15);
    AngebotGrid.SetFontCellHeight (this, &Font);
    AngebotGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

    AuftragGrid.Create (this, 20, 20);
    AuftragGrid.SetBorder (12, 20);
    AuftragGrid.SetCellHeight (15);
    AuftragGrid.SetFontCellHeight (this, &Font);
    AuftragGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_AngebotPage = new CCtrlInfo (&m_AngebotPage, 0, 0, 1, 1);
	AngebotGrid.Add (c_AngebotPage);

	CCtrlInfo *c_AuftragPage = new CCtrlInfo (&m_AuftragPage, 0, 0, 1, 1);
	AuftragGrid.Add (c_AuftragPage);

	CCtrlInfo *c_AngebotTitle = new CCtrlInfo (&m_AngebotTitle, 0, 0, DOCKRIGHT, 1);
	CtrlGrid.Add (c_AngebotTitle);

	CCtrlInfo *c_AngebotGrid = new CCtrlInfo (&AngebotGrid, 1, 2, DOCKRIGHT, DOCKBOTTOM);
	CtrlGrid.Add (c_AngebotGrid);

	CCtrlInfo *c_AuftragGrid = new CCtrlInfo (&AuftragGrid, 1, 2, DOCKRIGHT, DOCKBOTTOM);
	CtrlGrid.Add (c_AuftragGrid);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_AngebotTitle.SetFont (&TitleFont);

	CtrlGrid.Display ();
	m_AngebotPage.SetFirstFocus ();
}


HBRUSH CAngebotView::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (pWnd == &m_AngebotTitle)
	{
		pDC->SetTextColor (RGB (255,255,255));
//		pDC->SetBkColor (RGB (102,102,102));
		pDC->SetBkColor (RGB (100,180,255));
		if (TitleBrush == NULL)
		{
//			TitleBrush = CreateSolidBrush (RGB (102,102,102));
			TitleBrush = CreateSolidBrush (RGB (100,180,255));
		}
		return TitleBrush;
	}

	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CFormView::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CAngebotView::OnSize (UINT nType, int cx, int cy)
{
	if (IsWindow (m_AngebotPage.m_hWnd))
	{
		CRect r;
		m_AngebotPage.GetWindowRect (&r);
		ScreenToClient (&r);
		r.right = r.left + cx - 100;
		r.bottom = r.top + cy - 100;
		m_AngebotPage.MoveWindow (&r);
	}
	if (ChoiceAng != NULL && IsWindow (ChoiceAng->m_hWnd))
	{
		CRect mrect;
		GetWindowRect (&mrect);
		ScreenToClient (&mrect);
		mrect.top += 30;
		ChoiceAng->MoveWindow (&mrect);
	}
	if (ChoiceAuf != NULL && IsWindow (ChoiceAuf->m_hWnd))
	{
		CRect mrect;
		GetWindowRect (&mrect);
		ScreenToClient (&mrect);
		mrect.top += 30;
		ChoiceAuf->MoveWindow (&mrect);
	}
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
	CtrlGrid.Display ();
}

void CAngebotView::OnAngebot ()
{
	if (Page == CAngebotCore::PageAngebot)
	{
		return;
	}
	if (ChoiceAng != NULL && IsWindow (ChoiceAng->m_hWnd))
	{
		ChoiceAng->ShowWindow (SW_HIDE);
	}
	if (ChoiceAuf != NULL && IsWindow (ChoiceAuf->m_hWnd))
	{
		ChoiceAuf->ShowWindow (SW_HIDE);
	}
	CAngebotCore::GetInstance ()->SetAng ();
	CAngebotCore::GetInstance ()->TablesToAng ();
	if (!FromResource)
	{
		CAngebotCore::GetInstance ()->HideCalcView ();
	}
	CAngebotCore::GetInstance ()->Calculate ();
	switch (Page)
	{
		case CAngebotCore::PageAuftrag:
			m_AuftragPage.ShowWindow (SW_HIDE);
			break;
		case CAngebotCore::PageAngResource:
			if (ChoiceAng != NULL)
			{
				ChoiceAng->ShowWindow (SW_HIDE);
			}
			break;
		case CAngebotCore::PageKunAuftraege:
			if (ChoiceAuf != NULL)
			{
				ChoiceAuf->ShowWindow (SW_HIDE);
			}
			break;
	}
	Page = CAngebotCore::PageAngebot;
	CAngebotCore::GetInstance ()->SetPage (Page);
	m_AngebotTitle.SetWindowText (_T("  Angebot"));
    m_AngebotPage.ShowWindow (SW_SHOWNORMAL);
	m_AngebotPage.PostMessage (WM_SETFOCUS, 0, 0l);
}

void CAngebotView::OnAuftrag ()
{
	if (Page == CAngebotCore::PageAuftrag)
	{
		return;
	}
	if (ChoiceAng != NULL && IsWindow (ChoiceAng->m_hWnd))
	{
		ChoiceAng->ShowWindow (SW_HIDE);
	}
	if (ChoiceAuf != NULL && IsWindow (ChoiceAuf->m_hWnd))
	{
		ChoiceAuf->ShowWindow (SW_HIDE);
	}
	CAngebotCore::GetInstance ()->SetAng ();
	CAngebotCore::GetInstance ()->TablesToAuf ();
	CAngebotCore::GetInstance ()->HideCalcView ();
	CAngebotCore::GetInstance ()->Calculate ();
	switch (Page)
	{
		case CAngebotCore::PageAngebot:
			m_AngebotPage.ShowWindow (SW_HIDE);
			break;
		case CAngebotCore::PageAngResource:
			if (ChoiceAng != NULL)
			{
				ChoiceAng->ShowWindow (SW_HIDE);
			}
			break;
		case CAngebotCore::PageKunAuftraege:
			if (ChoiceAuf != NULL)
			{
				ChoiceAuf->ShowWindow (SW_HIDE);
			}
			break;
	}
	Page = CAngebotCore::PageAuftrag;
	CAngebotCore::GetInstance ()->SetPage (Page);
	m_AngebotTitle.SetWindowText (_T("  Auftrag"));
    m_AuftragPage.ShowWindow (SW_SHOWNORMAL);
	m_AuftragPage.PostMessage (WM_SETFOCUS, 0, 0l);
}

void CAngebotView::OnAngTypes ()
{
	short mdn = 0;
	short fil = 0;
	long kun = 0l;
	CString And;

	if (Page == CAngebotCore::PageAngResource)
	{
		return;
	}
	if (ChoiceAng != NULL && IsWindow (ChoiceAng->m_hWnd))
	{
		ChoiceAng->ShowWindow (SW_HIDE);
	}
	if (ChoiceAuf != NULL && IsWindow (ChoiceAuf->m_hWnd))
	{
		ChoiceAuf->ShowWindow (SW_HIDE);
	}
	CAngebotCore::GetInstance ()->HideCalcViewOnly ();
	if (CAngebotCore::GetInstance ()->GetForm () != NULL)
	{
		CAngebotCore::GetInstance ()->GetForm ()->Get ();
		mdn = CDatatables::GetInstance ()->GetMdn ()->mdn.mdn;
		kun = CDatatables::GetInstance ()->GetKun ()->kun.kun;
	}
	
	m_AngebotTitle.SetWindowText (_T("  Angebotsvorlagen"));
	switch (Page)
	{
		case CAngebotCore::PageAngebot:
			CAngebotCore::GetInstance ()->SetAng ();
			m_AngebotPage.ShowWindow (SW_HIDE);
			break;
		case CAngebotCore::PageAuftrag:
			CAngebotCore::GetInstance ()->SetAng ();
			m_AuftragPage.ShowWindow (SW_HIDE);
			break;
		case CAngebotCore::PageAngResource:
			if (ChoiceAng != NULL)
			{
				ChoiceAng->ShowWindow (SW_HIDE);
			}
			break;
		case CAngebotCore::PageKunAuftraege:
			if (ChoiceAuf != NULL)
			{
				ChoiceAuf->ShowWindow (SW_HIDE);
			}
			break;
	}
	CAngebotCore::GetInstance ()->TablesToAng ();
	Page = CAngebotCore::PageAngResource;
	CAngebotCore::GetInstance ()->SetPage (Page);
	if (ChoiceAng != NULL)
	{
		ChoiceAng->Where = _T("and ang_stat = 0 ");
		if (mdn != 0)
		{
			And.Format (_T("and partyk.mdn = %hd "), mdn);
			ChoiceAng->Where += And;
		}
		if (kun != 0l)
		{
			And.Format (_T("and partyk.kun = %ld "), kun);
			ChoiceAng->Where += And;
		}
		ChoiceAng->ShowWindow (SW_SHOWNORMAL);
		if (AngChoiceAnd != ChoiceAng->Where)
		{
			ChoiceAng->FillList ();
		}
		AngChoiceAnd = ChoiceAng->Where;
		return;
	}
	if (ChoiceAng == NULL)
	{
		ChoiceAng = new CChoicePartyk (this);
	    ChoiceAng->IsModal = FALSE;
		ChoiceAng->IdArrDown = IDI_HARROWDOWN;
		ChoiceAng->IdArrUp   = IDI_HARROWUP;
		ChoiceAng->IdArrNo   = IDI_HARROWNO;
		ChoiceAng->HideFilter = FALSE;
		ChoiceAng->HideEnter = TRUE;
		ChoiceAng->HideOK    = TRUE;
		ChoiceAng->HideCancel = TRUE;
		ChoiceAng->DlgBkColor = m_AngebotPage.GetDlgBkColor ();
//		ChoiceAng->SetQueryString (QueryPartyk);
//		ChoiceAng->FirstRead = FirstReadPartyk;
		ChoiceAng->Where = _T("and ang_stat = 0 ");
		if (mdn != 0)
		{
			And.Format (_T("and partyk.mdn = %hd "), mdn);
			ChoiceAng->Where += And;
		}
		if (kun != 0l)
		{
			And.Format (_T("and partyk.kun = %ld "), kun);
			ChoiceAng->Where += And;
		}
		AngChoiceAnd = ChoiceAng->Where;
		ChoiceAng->SetEmbeddedStyle (0);
		ChoiceAng->HideTake = FALSE;
		ChoiceAng->SetCFont (&Font);
		ChoiceAng->TakeAction = TakeAng;
		ChoiceAng->HideAuf = TRUE; 
		ChoiceAng->HideLs = TRUE; 
		ChoiceAng->CreateDlg ();
	}

	PARTYK_CLASS * Partyk = CDatatables::GetInstance ()->GetPartyk ();
    ChoiceAng->SetDbClass (Partyk);
	CRect mrect;
	GetWindowRect (&mrect);
	ScreenToClient (&mrect);
	mrect.top += 30;
	ChoiceAng->MoveWindow (&mrect);
	ChoiceAng->ShowWindow (SW_SHOWNORMAL);
	ChoiceAng->PostMessage (WM_SETFOCUS, 0, 0l);
}

void CAngebotView::OnKAuftraege ()
{
	if (Page == CAngebotCore::PageKunAuftraege)
	{
		return;
	}
	if (ChoiceAng != NULL && IsWindow (ChoiceAng->m_hWnd))
	{
		ChoiceAng->ShowWindow (SW_HIDE);
	}
	if (ChoiceAuf != NULL && IsWindow (ChoiceAuf->m_hWnd))
	{
		ChoiceAuf->ShowWindow (SW_HIDE);
	}
	CAngebotCore::GetInstance ()->HideCalcViewOnly ();
	m_AngebotTitle.SetWindowText (_T("  Kundenaufträge"));
    m_AngebotPage.ShowWindow (SW_HIDE);
    m_AuftragPage.ShowWindow (SW_HIDE);
	if (ChoiceAng != NULL)
	{
	    ChoiceAng->ShowWindow (SW_HIDE);
	}
	switch (Page)
	{
		case CAngebotCore::PageAngebot:
			CAngebotCore::GetInstance ()->SetAng ();
			m_AngebotPage.ShowWindow (SW_HIDE);
			break;
		case CAngebotCore::PageAuftrag:
			CAngebotCore::GetInstance ()->SetAng ();
			m_AuftragPage.ShowWindow (SW_HIDE);
			break;
		case CAngebotCore::PageAngResource:
			if (ChoiceAng != NULL)
			{
				ChoiceAng->ShowWindow (SW_HIDE);
			}
			break;
	}
	CAngebotCore::GetInstance ()->TablesToAng ();
	Page = CAngebotCore::PageKunAuftraege;
	CAngebotCore::GetInstance ()->SetPage (Page);
	if (ChoiceAuf == NULL)
	{
		ChoiceAuf = new CChoicePartyk (this);
	    ChoiceAuf->IsModal = FALSE;
		ChoiceAuf->IdArrDown = IDI_HARROWDOWN;
		ChoiceAuf->IdArrUp   = IDI_HARROWUP;
		ChoiceAuf->IdArrNo   = IDI_HARROWNO;
		ChoiceAuf->HideFilter = FALSE;
		ChoiceAuf->HideEnter = TRUE;
		ChoiceAuf->HideOK    = TRUE;
		ChoiceAuf->HideCancel = TRUE;
		ChoiceAuf->DlgBkColor = m_AngebotPage.GetDlgBkColor ();
//		ChoiceAuf->SetQueryString (QueryPartyk);
//		ChoiceAuf->FirstRead = FirstReadPartyk;
		ChoiceAuf->Where = _T("and ang_stat > 0 ");
		ChoiceAuf->SetEmbeddedStyle (0);
		ChoiceAuf->HideTake = FALSE;
		ChoiceAuf->TakeAction = TakeAng;
		ChoiceAuf->SetCFont (&Font);
		ChoiceAuf->CreateDlg ();
	}

	PARTYK_CLASS * Partyk = CDatatables::GetInstance ()->GetPartyk ();
    ChoiceAuf->SetDbClass (Partyk);
	CRect mrect;
	GetWindowRect (&mrect);
	ScreenToClient (&mrect);
	mrect.top += 30;
	ChoiceAuf->MoveWindow (&mrect);
	ChoiceAuf->ShowWindow (SW_SHOWNORMAL);
	ChoiceAuf->PostMessage (WM_SETFOCUS, 0, 0l);
}

#ifdef _DEBUG
void CAngebotView::AssertValid() const
{
	CFormView::AssertValid();
}
#ifndef _WIN32_WCE
void CAngebotView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CAngebotView-Meldungshandler

CAngebotView::CAngebotMessage::CAngebotMessage (CAngebotView *p) : CRunMessage ()
{
	        SetId (IDC_BANGEBOTE); 
			this->p = p;
}

void CAngebotView::CAngebotMessage::Run ()
{
	p->OnAngebot ();
}

CAngebotView::CAuftragMessage::CAuftragMessage (CAngebotView *p) : CRunMessage ()
{
	        SetId (IDC_BAUFTRAEGE); 
			this->p = p;
}

void CAngebotView::CAuftragMessage::Run ()
{
	p->OnAuftrag ();
}

CAngebotView::CAngTypesMessage::CAngTypesMessage (CAngebotView *p) : CRunMessage ()
{
	        SetId (IDC_BANGEBOTSTYPEN); 
			this->p = p;
}

void CAngebotView::CAngTypesMessage::Run ()
{
	p->OnAngTypes ();
}

CAngebotView::CKAuftraegeMessage::CKAuftraegeMessage (CAngebotView *p) : CRunMessage ()
{
	        SetId (IDC_BKAUFTRAEGE); 
			this->p = p;
}

void CAngebotView::CKAuftraegeMessage::Run ()
{
	p->OnKAuftraege ();
}

CAngebotView::CTakeAng::CTakeAng (CAngebotView *p) : CRunMessage ()
{
	        SetId (IDC_TAKE_ANG); 
			this->p = p;
}

void CAngebotView::CTakeAng::Run ()
{
	p->OnTakeAng ();
}

void CAngebotView::OnTakeAng ()
{
	if (CAngebotCore::GetInstance ()->SetAngResource ())
	{
		if (!m_AngebotPage.m_Mdn.IsWindowEnabled ())
		{
//			m_AngebotPage.OnF5 ();
		}
		m_AngebotPage.ReadAng ();
		FromResource = TRUE;
        OnAngebot ();
		FromResource = FALSE;
	}
}
