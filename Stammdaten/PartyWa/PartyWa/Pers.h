#ifndef _PERS_DEF
#define _PERS_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct PERS {
   short          abt;
   long           adr;
   short          anz_tag_wo;
   double         arb_zeit_tag;
   double         arb_zeit_wo;
   TCHAR          beschaef_kz[2];
   DATE_STRUCT    eintr_dat;
   short          fil;
   short          kost_st;
   short          mdn;
   TCHAR          pers[13];
   long           pers_mde;
   double         pers_pkt;
   double         rab_proz;
   double         std_satz;
   TCHAR          taet[25];
   TCHAR          taet_kz[2];
   short          tar_gr;
   TCHAR          zahlw[2];
   long           txt_nr;
   short          delstatus;
};
extern struct PERS pers, pers_null;

#line 8 "Pers.rh"

class PERS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               PERS pers;  
               PERS_CLASS () : DB_CLASS ()
               {
               }
};
#endif
