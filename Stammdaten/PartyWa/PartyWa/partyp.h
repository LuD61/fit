#ifndef _PARTYP_DEF
#define _PARTYP_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct PARTYP {
   long           row_id;
   short          mdn;
   short          fil;
   long           ang;
   long           auf;
   long           ls;
   long           posi;
   long           aufp_txt;
   double         a;
   double         auf_me;
   TCHAR          auf_me_bz[7];
   double         lief_me;
   TCHAR          lief_me_bz[7];
   double         auf_vk_pr;
   double         auf_lad_pr;
   short          delstatus;
   short          sa_kz_sint;
   double         prov_satz;
   long           ksys;
   long           pid;
   long           auf_klst;
   short          teil_smt;
   short          dr_folge;
   double         inh;
   double         auf_vk_euro;
   double         auf_vk_fremd;
   double         auf_lad_euro;
   double         auf_lad_fremd;
   double         rab_satz;
   short          me_einh_kun;
   short          me_einh;
   short          me_einh_kun1;
   double         auf_me1;
   double         inh1;
   short          me_einh_kun2;
   double         auf_me2;
   double         inh2;
   short          me_einh_kun3;
   double         auf_me3;
   double         inh3;
   long           gruppe;
   TCHAR          kond_art[5];
   double         a_grund;
   short          ls_pos_kz;
   long           posi_ext;
   long           kun;
   double         a_ers;
   TCHAR          ls_charge[31];
   short          aufschlag;
   double         aufschlag_wert;
   double         pos_vk;
   double         pos_gew;
   short          mwst;
   double         auf_vk_brutto;
   TCHAR          gebinde_text[37];
   double         a_gew;
   short          pos_txt_kz;
   double         pos_vk_nto;
   short          preinzel_kz;
};
extern struct PARTYP partyp, partyp_null;

#line 8 "partyp.rh"

class PARTYP_CLASS : public DB_CLASS 
{
       private :
               long row_id;
               int row_id_cursor;  
               int test_count_cursor;  
               void prepare (void);
       public :
               PARTYP partyp;  
               PARTYP_CLASS () : DB_CLASS ()
               {
		    row_id_cursor = -1;
		    test_count_cursor = -1;
               }
               long NextRowId ();
               int dbupdate ();
};
#endif
