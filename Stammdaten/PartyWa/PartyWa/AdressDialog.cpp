// AdressDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "PartyWa.h"
#include "AdressDialog.h"
#include "KunDiverse.h"
#include "Bmap.h"

#define KUNDIVERSE CKunDiverse::GetInstance ()

// CAdressDialog-Dialogfeld

IMPLEMENT_DYNAMIC(CAdressDialog, CDialog)
CAdressDialog::CAdressDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CAdressDialog::IDD, pParent)
{
	AdrTyp = KunAdrTyp;
	m_DlgBrush = NULL;
	m_DlgColor = RGB (250, 250, 202);
	BkBitmap.SetBkType (BkBitmap.DynamicGrey);
//	BkBitmap.SetBkType (BkBitmap.DynamicMetal);
//	BkBitmap.SetDirection (BkBitmap.Vertical);
	m_CaptionText = _T("Adresse");
	m_LiefAdr = 0l;
	//FS-358 m_ChoiceVisible = FALSE;
	m_ChoiceVisible = TRUE; //FS-358
	m_ChoiceAdr = NULL;
}

CAdressDialog::~CAdressDialog()
{
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	if (m_DlgBrush != NULL)
	{
		DeleteObject (m_DlgBrush);
		m_DlgBrush = NULL;
	}
	if (m_ChoiceAdr != NULL)
	{
		delete m_ChoiceAdr;
		m_ChoiceAdr = NULL;
	}
}

void CAdressDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAPTION, m_Caption);
	DDX_Control(pDX, IDC_LNAME1, m_LName);
	DDX_Control(pDX, IDC_NAME1, m_Name1);
	DDX_Control(pDX, IDC_NAME2,  m_Name2);
	DDX_Control(pDX, IDC_LSTR1, m_LStr);
	DDX_Control(pDX, IDC_STR, m_Str);
	DDX_Control(pDX, IDC_LPLZ_ORT, m_LPlzOrt);
	DDX_Control(pDX, IDC_PLZ, m_Plz);
	DDX_Control(pDX, IDC_ORT1, m_Ort1);
	DDX_Control(pDX, IDC_ORT2, m_Ort2);
	DDX_Control(pDX, IDC_LTEL, m_LTel);
	DDX_Control(pDX, IDC_TEL, m_Tel);
	DDX_Control(pDX, IDC_LFAX1, m_LFax);
	DDX_Control(pDX, IDC_FAX, m_Fax);
	DDX_Control(pDX, IDC_LEMAIL, m_LEmail);
	DDX_Control(pDX, IDC_EMAIL, m_Email);
	DDX_Control(pDX, IDC_LGEB_DAT, m_LGebDat);
	DDX_Control(pDX, IDC_GEB_DAT, m_GebDat);
	DDX_Control(pDX, IDC_GROUP_ADR, m_GroupAdr);
	DDX_Control(pDX, IDC_GEB_DAT2, m_GebDat2);
	DDX_Control(pDX, IDC_OKEX, m_OkEx);
	DDX_Control(pDX, IDC_CANCELEX2, m_CancelEx);
	DDX_Control(pDX, IDC_ADR_CHOICE, m_AdrChoice);
	DDX_Control(pDX, IDC_NEW, m_New);
	DDX_Control(pDX, IDC_NAME3, m_Name3);
	DDX_Control(pDX, IDC_PF_PLZ, m_pf_plz);
	DDX_Control(pDX, IDC_PF, m_pf);
	DDX_Control(pDX, IDC_LPF, m_Lpf);
}


BEGIN_MESSAGE_MAP(CAdressDialog, CDialog)
	ON_WM_CTLCOLOR ()
	ON_BN_CLICKED(IDC_OKEX, OnOK)
	ON_BN_CLICKED(IDC_CANCELEX, OnCancel)
	ON_BN_CLICKED(IDC_ADR_CHOICE, OnAdrChoice)
	ON_BN_CLICKED(IDC_NEW, OnNew)
END_MESSAGE_MAP()


// CAdressDialog-Meldungshandler

BOOL CAdressDialog::OnInitDialog ()
{
	CDialog::OnInitDialog();

	if (m_ChoiceVisible)
	{
		m_AdrChoice.ShowWindow (SW_SHOWNORMAL);
	}
	else
	{
		m_AdrChoice.ShowWindow (SW_HIDE);
	}
	m_New.ShowWindow (SW_HIDE);
	if (AdrTyp == LiefAdrTyp && m_LiefAdr != 0l)
	{
		m_New.ShowWindow (SW_SHOWNORMAL);
	}
	m_Caption.SetParts (60);
	m_Caption.SetWindowText (m_CaptionText);
	m_Caption.SetBoderStyle (m_Caption.Solide);
	m_Caption.SetOrientation (m_Caption.Left);
	m_Caption.SetDynamicColor (TRUE);
	m_Caption.SetOrientation (m_Caption.Center);
	m_Caption.SetTextColor (RGB (255, 255, 153));
	m_Caption.SetBkColor (RGB (192, 192, 192));

	COLORREF BkColor (RGB (0, 0, 255));
//	COLORREF DynColor = RGB (0, 0, 255);
	COLORREF RolloverColor = CColorButton::DynColorBlue;
	COLORREF DynColor = CColorButton::DynColorBlue;

	m_OkEx.SetBkColor (BkColor);
	m_OkEx.DynColor = DynColor;
	m_OkEx.RolloverColor = RolloverColor;

	m_CancelEx.SetBkColor (BkColor);
	m_CancelEx.DynColor = DynColor;
	m_CancelEx.RolloverColor = RolloverColor;

	m_AdrChoice.SetBkColor (BkColor);
	m_AdrChoice.DynColor = DynColor;
	m_AdrChoice.RolloverColor = RolloverColor;
	m_AdrChoice.nID = IDC_ADR_CHOICE;
	m_AdrChoice.SetWindowText (_T("Auswahl"));
	m_AdrChoice.SetText(_T("Auswahl"));

	m_New.SetBkColor (BkColor);
	m_New.DynColor = DynColor;
	m_New.RolloverColor = RolloverColor;

	GetDlgItem (IDOK)->ShowWindow (SW_HIDE);
	GetDlgItem (IDCANCEL)->ShowWindow (SW_HIDE);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}
	memcpy (&Adr.adr, &adr_null, sizeof (ADR));
	Form.Add (new CFormField (&m_Name1,EDIT,     (char *)   Adr.adr.adr_nam1, VCHAR, sizeof (Adr.adr.adr_nam1)));
	Form.Add (new CFormField (&m_Name2,EDIT,     (char *)   Adr.adr.adr_nam2, VCHAR, sizeof (Adr.adr.adr_nam2)));
	Form.Add (new CFormField (&m_Name3,EDIT,     (char *)   Adr.adr.adr_nam3, VCHAR, sizeof (Adr.adr.adr_nam3)));
	Form.Add (new CFormField (&m_Str,EDIT,       (char *)   Adr.adr.str, VCHAR, sizeof (Adr.adr.str)));
	Form.Add (new CFormField (&m_Plz,EDIT,       (char *)   Adr.adr.plz, VCHAR, sizeof (Adr.adr.plz)));
	Form.Add (new CFormField (&m_Ort1,EDIT,      (char *)   Adr.adr.ort1, VCHAR, sizeof (Adr.adr.ort1)));
	Form.Add (new CFormField (&m_Ort2,EDIT,      (char *)   Adr.adr.ort2, VCHAR, sizeof (Adr.adr.ort2)));
	Form.Add (new CFormField (&m_pf_plz,EDIT,    (char *)   Adr.adr.plz_pf, VCHAR, sizeof (Adr.adr.plz_pf)));
	Form.Add (new CFormField (&m_pf,EDIT,        (char *)   Adr.adr.pf, VCHAR, sizeof (Adr.adr.pf)));
	Form.Add (new CFormField (&m_Tel,EDIT,       (char *)   Adr.adr.tel, VCHAR, sizeof (Adr.adr.tel)));
	Form.Add (new CFormField (&m_Fax,EDIT,       (char *)   Adr.adr.fax, VCHAR, sizeof (Adr.adr.fax)));
	Form.Add (new CFormField (&m_Email,EDIT,     (char *)   Adr.adr.email, VCHAR, sizeof (Adr.adr.email)));
//	Form.Add (new CFormField (&m_GebDat,EDIT,    (DATE_STRUCT *)  &Adr.adr.geb_dat, VDATE));
	Form.Add (new CFormField (&m_GebDat2, DATETIMEPICKER,   (DATE_STRUCT *)  &Adr.adr.geb_dat, VDATE));
	m_GebDat.ShowWindow (SW_HIDE);
	m_GebDat2.ModifyStyle (DTS_UPDOWN, 0);
	if (AdrTyp != KunAdrTyp)
	{
		m_LGebDat.ShowWindow (SW_HIDE);
		m_GebDat2.ShowWindow (SW_HIDE);
	}

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (20);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

//Grid Plz/Ort
	PlzOrtGrid.Create (this, 5, 5);
    PlzOrtGrid.SetBorder (0, 0);
    PlzOrtGrid.SetGridSpace (5, 8);
	CCtrlInfo *c_Plz = new CCtrlInfo (&m_Plz, 0, 0, 1, 1);
	PlzOrtGrid.Add (c_Plz);
	CCtrlInfo *c_Ort1 = new CCtrlInfo (&m_Ort1, 1, 0, 1, 1);
	PlzOrtGrid.Add (c_Ort1);
	CCtrlInfo *c_Ort2 = new CCtrlInfo (&m_Ort2, 1, 1, 1, 1);
	PlzOrtGrid.Add (c_Ort2);
	CCtrlInfo *c_pf_plz = new CCtrlInfo (&m_pf_plz, 0, 2, 1, 1);
	PlzOrtGrid.Add (c_pf_plz);
	CCtrlInfo *c_pf = new CCtrlInfo (&m_pf, 1, 2, 1, 1);
	PlzOrtGrid.Add (c_pf);

//Grid Telefon Fax
	TeleGrid.Create (this, 5, 5);
    TeleGrid.SetBorder (0, 0);
    TeleGrid.SetGridSpace (5, 8);
	CCtrlInfo *c_Tel = new CCtrlInfo (&m_Tel, 0, 0, 1, 1);
	TeleGrid.Add (c_Tel);
	CCtrlInfo *c_LFax = new CCtrlInfo (&m_LFax, 1, 0, 1, 1);
	TeleGrid.Add (c_LFax);
	CCtrlInfo *c_Fax = new CCtrlInfo (&m_Fax, 2, 0, 1, 1);
	TeleGrid.Add (c_Fax);

	CCtrlInfo *c_Caption = new CCtrlInfo (&m_Caption, 0, 0, DOCKRIGHT, 1);
    CtrlGrid.Add (c_Caption);

	CCtrlInfo *c_GroupAdr = new CCtrlInfo (&m_GroupAdr, 1, 2, 20, 20);
	CtrlGrid.Add (c_GroupAdr);
	CCtrlInfo *c_LName = new CCtrlInfo (&m_LName, 2, 3, 1, 1);
	CtrlGrid.Add (c_LName);
	CCtrlInfo *c_Name1 = new CCtrlInfo (&m_Name1, 3, 3, 1, 1);
	CtrlGrid.Add (c_Name1);
	CCtrlInfo *c_Name2 = new CCtrlInfo (&m_Name2, 4, 3, 1, 1);
	CtrlGrid.Add (c_Name2);

	CCtrlInfo *c_Name3 = new CCtrlInfo (&m_Name3, 3, 4, 1, 1);
	CtrlGrid.Add (c_Name3);

	CCtrlInfo *c_LStr = new CCtrlInfo (&m_LStr, 2, 6, 1, 1);
	CtrlGrid.Add (c_LStr);
	CCtrlInfo *c_Str = new CCtrlInfo (&m_Str, 3, 6, 1, 1);
	CtrlGrid.Add (c_Str);
	CCtrlInfo *c_LPlzOrt = new CCtrlInfo (&m_LPlzOrt, 2, 7, 1, 1);
	CtrlGrid.Add (c_LPlzOrt);
	CCtrlInfo *c_PlzOrtGrid = new CCtrlInfo (&PlzOrtGrid, 3, 7, 5, 2);
	CtrlGrid.Add (c_PlzOrtGrid);
    CCtrlInfo *c_Lpf = new CCtrlInfo (&m_Lpf, 2, 9, 1, 1);
    CtrlGrid.Add (c_Lpf);
	CCtrlInfo *c_LTel = new CCtrlInfo (&m_LTel, 2, 11, 1, 1);
	CtrlGrid.Add (c_LTel);
	CCtrlInfo *c_TeleGrid = new CCtrlInfo (&TeleGrid, 3, 11, 5, 1);
	CtrlGrid.Add (c_TeleGrid);
	CCtrlInfo *c_LEmail = new CCtrlInfo (&m_LEmail, 2, 12, 1, 1);
	CtrlGrid.Add (c_LEmail);
	CCtrlInfo *c_Email = new CCtrlInfo (&m_Email, 3, 12, 1, 1);
	CtrlGrid.Add (c_Email);
	CCtrlInfo *c_LGebDat = new CCtrlInfo (&m_LGebDat, 2, 13, 1, 1);
	CtrlGrid.Add (c_LGebDat);
	CCtrlInfo *c_GebDat2 = new CCtrlInfo (&m_GebDat2, 3, 13 , 1, 1);
	CtrlGrid.Add (c_GebDat2	);

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	StaticFont.CreateFontIndirect (&l);
	l.lfHeight = 25;
	CaptionFont.CreateFontIndirect (&l);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);
	m_Caption.SetFont (&CaptionFont);

	CtrlGrid.Display ();
	Kun = CDatatables::GetInstance ()->GetKun ();
	read ();
   if (AdrTyp != LiefAdrTyp)//FS-358
   {
	if (!KUNDIVERSE->IsAdrDiverse (Adr.adr.adr)) //FS-358
	{
			m_AdrChoice.ShowWindow (SW_HIDE);
	}
   }
	m_Name1.SetFocus ();
	return FALSE;
}

BOOL CAdressDialog::PreTranslateMessage(MSG* pMsg)
{
	CWnd *Control = GetFocus ();

	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				Control = GetNextDlgTabItem (Control, FALSE);
				if (Control != NULL)
				{
					Control->SetFocus ();
					return TRUE;
				}
			}
			else if (pMsg->wParam == VK_F5 ||
					pMsg->wParam == VK_ESCAPE)
			{
				OnCancel ();
					return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				OnOK ();
				return TRUE;
			}
	}
	return CDialog::PreTranslateMessage (pMsg);
}

HBRUSH CAdressDialog::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	CRect rect;

	if (nCtlColor == CTLCOLOR_DLG)
	{
		    if (m_DlgBrush == NULL)
			{
//				m_DlgBrush = CreateSolidBrush (m_DlgColor);
                GetClientRect (&rect);
				BkBitmap.SetWidth (rect.right); 
				BkBitmap.SetHeight (rect.bottom);
				BkBitmap.SetPlanes (32);
				CBitmap *bitmap = BkBitmap.Create (); 
				m_DlgBrush = CreatePatternBrush (*bitmap);
				bitmap->DeleteObject ();
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	else if (nCtlColor == CTLCOLOR_STATIC)
	{
		    if (m_DlgBrush == NULL)
			{
				m_DlgBrush = CreateSolidBrush (m_DlgColor);
                GetClientRect (&rect);
				BkBitmap.SetWidth (rect.right); 
				BkBitmap.SetHeight (rect.bottom);
				BkBitmap.SetPlanes (32);
				CBitmap *bitmap = BkBitmap.Create (); 
				m_DlgBrush = CreatePatternBrush (*bitmap);
				bitmap->DeleteObject ();
			}
			pDC->SetBkMode (TRANSPARENT);
			return m_DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

BOOL CAdressDialog::read ()
{	 
	 int dsqlstatus = 0;
	 BOOL ret = FALSE;
     if (Kun != NULL)
	 {
		 if (AdrTyp == KunAdrTyp)
		 {
			 Adr.adr.adr = Kun->kun.adr1;
		 }
		 else if (AdrTyp == LiefAdrTyp)
		 {
			 if (m_LiefAdr == 0l)
			 {
				Adr.adr.adr = 0l;
				Form.Show ();
				return TRUE;
			 }
			 else
			 {
				Adr.adr.adr = m_LiefAdr;
			 }
		 }
		 else if (AdrTyp == RechAdrTyp)
		 {
			 Adr.adr.adr = Kun->kun.adr2;
		 }
		 dsqlstatus = Adr.dbreadfirst ();
		 if (dsqlstatus == 0)
		 {
			 ret = TRUE;
			 CString KunKrz = Adr.adr.adr_krz;
			 KunKrz.TrimRight ();
			 if (KunKrz == _T(""))
			 {
				 _tcscpy (Adr.adr.adr_krz, Kun->kun.kun_krz1);
			 }
		 }
		 else
		 {
			 if (KUNDIVERSE->IsAdrDiverse (Adr.adr.adr))
			 {
				 _tcscpy (Adr.adr.adr_krz, Kun->kun.kun_krz1);
			 }
		 }
	 }
	if (Adr.adr.geb_dat.year < 1900) //FS-358  schmiert sonst beim Form.Get() ab 
	{
			 Adr.adr.geb_dat.day = 1;
			 Adr.adr.geb_dat.month = 1;
			 Adr.adr.geb_dat.year = 1900;
	}

	 Form.Show ();
     return ret;
}

BOOL CAdressDialog::write ()
{	
	BOOL ret = FALSE;
	Form.Get ();
	if (Adr.adr.adr == 0)
	{
		Adr.adr.adr = KUNDIVERSE->GenAdr ();
	}
//FS-358	m_LiefAdr = Adr.adr.adr;
	if (AdrTyp == LiefAdrTyp)
	{
		Adr.adr.adr = Kun->kun.adr2; //FS-358
	}
	else
	{
		Adr.adr.adr = Kun->kun.adr1; //FS-358
	}
    Adr.dbupdate ();
    return ret;
}

void CAdressDialog::OnOK ()
{
	write ();
	CDialog::OnOK ();
}

void CAdressDialog::OnAdrChoice ()
{
	BOOL Modal = TRUE;
    
	if (m_ChoiceAdr != NULL)
	{
		m_ChoiceAdr->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (m_ChoiceAdr == NULL)
	{
		m_ChoiceAdr = new CChoiceAdr (this);
	    m_ChoiceAdr->IsModal = TRUE;
		m_ChoiceAdr->SetBitmapButton (TRUE);
		m_ChoiceAdr->AndPart ().Format (_T(" and adr >= %ld and adr <= %ld"),
								KUNDIVERSE->GetAdrFrom (), KUNDIVERSE->GetAdrTo ());
		m_ChoiceAdr->CreateDlg ();
	}

    m_ChoiceAdr->SetDbClass (&Adr);
	
	if (Modal)
	{
			m_ChoiceAdr->DoModal();
	}
	
    if (m_ChoiceAdr->GetState ())
    {
		  CAdrList *abl = m_ChoiceAdr->GetSelectedText (); 
		  if (abl == NULL) return;
		  Adr.adr.adr = abl->adr;
		  m_LiefAdr = abl->adr;
		  Adr.dbreadfirst ();
		  Form.Show ();	
		  m_Name1.SetFocus ();
		  m_New.ShowWindow (SW_SHOWNORMAL);
	}
    delete m_ChoiceAdr;
    m_ChoiceAdr = NULL;
}

void CAdressDialog::OnNew ()
{
	memcpy (&Adr.adr, &adr_null, sizeof (ADR));
	m_LiefAdr = 0l;
	Form.Show ();
}

