#pragma once
#include "OkColorButton.h"
#include "CancelColorButton.h"


// CPrintMail-Dialogfeld

class CPrintMail : public CDialog
{
	DECLARE_DYNAMIC(CPrintMail)

public:
	enum DLG_RESULT
	{
		Print,
		EMail,
	};
	CPrintMail(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CPrintMail();

// Dialogfelddaten
	enum { IDD = IDD_PRINTMAIL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
    virtual BOOL OnInitDialog ();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
private:
	DLG_RESULT m_Result;
	COkColorButton m_OkEx;
	CCancelColorButton m_CancelEx;
public :
    DLG_RESULT Result ()
	{
		return m_Result;
	}


};
