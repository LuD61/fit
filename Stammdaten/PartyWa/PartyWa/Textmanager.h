#pragma once
#include "Textpool.h"

class CTextmanager
{
public:
	CTextmanager(void);
public:
	~CTextmanager(void);

private:
	long m_Nr;
	int cursor;
	TEXTPOOL_CLASS m_Textpool;
	long CreateNumber ();
public:
	TEXTPOOL_CLASS *Textpool ()
	{
		return &m_Textpool;
	}
	long GetNumber (long nr);
	void Read ();
	void Write ();
	BOOL IsTextChanged (CString& Text);
};
