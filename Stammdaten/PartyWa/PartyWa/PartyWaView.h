// PartyWaView.h : Schnittstelle der Klasse CPartyWaView
//


#pragma once


class CPartyWaView : public CFormView
{
protected: // Nur aus Serialisierung erstellen
	CPartyWaView();
	DECLARE_DYNCREATE(CPartyWaView)

public:
	enum{ IDD = IDD_PARTYWA_FORM };

// Attribute
public:
	CPartyWaDoc* GetDocument() const;

// Vorgänge
public:

// Überschreibungen
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate(); // Erster Aufruf nach Erstellung

// Implementierung
public:
	virtual ~CPartyWaView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generierte Funktionen für die Meldungstabellen
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // Debugversion in PartyWaView.cpp
inline CPartyWaDoc* CPartyWaView::GetDocument() const
   { return reinterpret_cast<CPartyWaDoc*>(m_pDocument); }
#endif

