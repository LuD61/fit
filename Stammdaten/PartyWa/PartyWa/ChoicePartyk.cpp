#include "StdAfx.h"
#include "ChoicePartyk.h"
#include "DbUniCode.h"
#include "Process.h"
#include "StrFuncs.h"
#include "Token.h"
#include "DynDialog.h"
#include "DbQuery.h"
#include "DbTime.h"
#include "MessageHandler.h"
#include "QDebug.h"

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#define BLUE_COL 1

DLGITEMTEMPLATE CChoicePartyk::Take =
{
//    BS_PUSHBUTTON | WS_VISIBLE | WS_TABSTOP,
    WS_VISIBLE | WS_TABSTOP | SS_BLACKRECT | SS_OWNERDRAW | SS_NOTIFY,
    NULL,
    210,10,100,14,
    IDC_TAKE,
};

int CChoicePartyk::Sort1 = -1;
int CChoicePartyk::Sort2 = -1;
int CChoicePartyk::Sort3 = -1;
int CChoicePartyk::Sort4 = -1;
int CChoicePartyk::Sort5 = -1;
int CChoicePartyk::Sort6 = -1;
int CChoicePartyk::Sort7 = -1;
int CChoicePartyk::Sort8 = -1;
int CChoicePartyk::Sort9 = -1;
int CChoicePartyk::Sort10 = -1;
int CChoicePartyk::Sort11 = -1;
int CChoicePartyk::Sort12 = -1;
int CChoicePartyk::Sort13 = -1;
int CChoicePartyk::Sort14 = -1;
int CChoicePartyk::Sort15 = -1;

CChoicePartyk::CChoicePartyk(CWnd* pParent) 
        : CChoiceX(pParent)
{
	ptcursor = -1;
	Where = "";
	Types = "";
	EnterMdn = TRUE;
	Bean.ArchiveName = _T("PartykList.prp");
	KunFil = 0;
	NewKunFil = 0;
	FromFilter = FALSE;
	HideTake = TRUE;
	TakeAction = NULL;
	HideAuf = FALSE;
	HideLs  = FALSE;
	HideRech  = FALSE;
}

CChoicePartyk::~CChoicePartyk() 
{
	DestroyList ();
	if (ptcursor != -1) DbClass->sqlclose (ptcursor);
	ptcursor = -1;
}

void CChoicePartyk::DoDataExchange(CDataExchange* pDX)
{
	CChoiceX::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChoice)
	DDX_Control(pDX, IDC_TAKE, m_Take);
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChoicePartyk, CChoiceX)
	//{{AFX_MSG_MAP(CChoiceX)
	ON_BN_CLICKED(IDC_TAKE , OnTake)
	ON_NOTIFY(NM_DBLCLK, IDC_CHOICE, OnDblclkChoice)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CChoicePartyk::OnInitDialog() 
{
	CChoiceX::OnInitDialog ();

#ifdef BLUE_COL
	COLORREF BkColor (RGB (100, 180, 255));
	COLORREF DynColor = m_NewAngTyp.DynColorBlue;
	COLORREF RolloverColor = RGB (204, 204, 255);
#else
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = RGB (192, 192, 192);
#endif

	m_Take.Orientation = m_Take.Left;
	m_Take.TextStyle = m_Take.Standard;
	m_Take.BorderStyle = m_Take.Solide;
	m_Take.DynamicColor = TRUE;
	m_Take.TextColor =RGB (0, 0, 0);
	m_Take.SetBkColor (BkColor);
	m_Take.DynColor = DynColor;
	m_Take.RolloverColor = RolloverColor;
	m_Take.LoadBitmap (IDB_COPY_OFFER);
	m_Take.LoadMask (IDB_COPY_OFFER_MASK);
	if (HideTake)
	{
		m_Take.ShowWindow (SW_HIDE);
	}

	return TRUE;
}

void CChoicePartyk::AddControls ()
{
	DlgHeader.cdit ++;
    AddItem (&SearchEdit,   DLGEDIT, "");
    AddItem (&Take,   "Static", "Angebot kopieren");
	m_Take.nID = IDC_TAKE;
    AddItem (&SearchLabel,  DLGSTATIC, "Suchen");
    AddItem (&Enter,        "Static", "Bearbeiten");
    AddItem (&Filter,       "Static", "Filter");
	m_Enter.nID = IDC_ENTER;
	m_Filter.nID = IDC_FILTER;
    AddItem (&SearchList,  "SysListView32", "");
    AddItem (&SearchOK,     DLGBUTTON, "OK");
    AddItem (&SearchCancel, DLGBUTTON, CloseTxt);
    AddItem (&ListType,     "Button",  "");
}


void CChoicePartyk::DestroyList() 
{
	for (std::vector<CPartykList *>::iterator pabl = PartykList.begin (); pabl != PartykList.end (); ++pabl)
	{
		CPartykList *abl = *pabl;
		delete abl;
	}
    PartykList.clear ();
}

void CChoicePartyk::TestKunFil (short KunFil)
{
	NewKunFil = KunFil;
	if (NewKunFil != this->KunFil)
	{
		this->KunFil = KunFil;
		PostMessage (WM_COMMAND, IDC_FILTER, 0l);
	}
}

void CChoicePartyk::FillList () 
{
    short  mdn;
    long  ang;
    long  auf;
    long  ls;
    long  rech;
    long  kun;
    short  fil;
	DATE_STRUCT lieferdat;
    TCHAR kun_krz1 [50];
    TCHAR kun_bran2 [50];
    TCHAR hinweis [513];
	short typ;
	short ang_stat;
	long inka_nr;
	long tou_nr;
	short anz_pers;
	extern short sql_mode;
	short sql_s;
	CString Sql = _T("");

	sql_s = sql_mode;
	sql_mode = 2;
	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);

    int i = 0;

	KunFil = NewKunFil;
    SetWindowText (_T("Auswahl Angebote"));
	int p = 0;
    SetCol (_T(""),      p++, 0, LVCFMT_LEFT);
    SetCol (_T("Mandant"),           p++,  75, LVCFMT_RIGHT);
    SetCol (_T("Angebot"),           p++, 100, LVCFMT_RIGHT);
	AufPos = p;
    SetCol (_T("Auftrag"),           p++, 80, LVCFMT_RIGHT);
	LsPos = p;
    SetCol (_T("Lieferschein"),      p++, 80, LVCFMT_RIGHT);
	RechPos = p;
    SetCol (_T("Rechnung"),          p++, 80, LVCFMT_RIGHT);
    SetCol (_T("Lieferdatum"),       p++, 100, LVCFMT_RIGHT);
	if (KunFil == 0)
	{
		SetCol (_T("Kunden Nr"),         p++, 80, LVCFMT_RIGHT);
	}
	else
	{
		SetCol (_T("Filial Nr"),         p++, 80, LVCFMT_RIGHT);
	}
    SetCol (_T("Name"),              p++, 250);
	if (KunFil == 0)
	{
		SetCol (_T("Branche"),           p++, 150);
	}
    SetCol (_T("Kategorie"),               p++, 150);
    SetCol (_T("Hinweistext"),      p++, 150);
    SetCol (_T("Tour"),             p++, 40);
    SetCol (_T("Inkasso"),          p++, 40);
    SetCol (_T("Personen"),         p++, 100, LVCFMT_RIGHT);
	SortRow = 2;

//	if (!FromFilter && FirstRead && !HideFilter && QueryString == _T(""))
	if (!FromFilter && FirstRead && !HideFilter)
	{
		FirstRead = FALSE;
		Load ();
		PostMessage (WM_COMMAND, IDC_FILTER, 0l);
		return;
	}

	if (PartykList.size () == 0)
	{
		DbClass->sqlout ((short *)&mdn,              SQLSHORT, 0);
		DbClass->sqlout ((long *)&ang,               SQLLONG, 0);
		DbClass->sqlout ((long *)&auf,              SQLLONG, 0);
		DbClass->sqlout ((long *)&ls,              SQLLONG, 0);
		DbClass->sqlout ((long *)&rech,            SQLLONG, 0);
		DbClass->sqlout ((DATE_STRUCT *)&lieferdat, SQLDATE, 0);
		if (KunFil == 0)
		{
			DbClass->sqlout ((long *)&kun,              SQLLONG, 0);
		}
		else
		{
			DbClass->sqlout ((short *)&fil,              SQLSHORT, 0);
		}
		DbClass->sqlout ((TCHAR *)kun_krz1,         SQLCHAR, 17);
		if (KunFil == 0)
		{
			DbClass->sqlout ((TCHAR *)kun_bran2,     SQLCHAR, 3);
		}
		DbClass->sqlout ((short *)&ang_stat,         SQLSHORT, 0);
		DbClass->sqlout ((short *)&typ,              SQLSHORT, 0);
		DbClass->sqlout ((TCHAR *)hinweis,           SQLCHAR, sizeof (hinweis));
		DbClass->sqlout ((long *)&tou_nr,            SQLLONG, 0);
		DbClass->sqlout ((long *)&inka_nr,           SQLLONG, 0);
		DbClass->sqlout ((short *)&anz_pers,         SQLSHORT, 0);
		if (KunFil == 0)
		{
			Sql  =     _T("select partyk.mdn, partyk.ang, partyk.auf, partyk.ls, partyk.rech, partyk.lieferdat, kun.kun, kun.kun_krz1, kun.kun_bran2, partyk.ang_stat, partyk.typ, ")
					   _T("partyk.hinweis, partyk.tou_nr, kun.inka_nr, anz_pers ")						
					   _T("from partyk,kun where ")
					   _T("partyk.ang > 0 ") 
					   _T("and partyk.kun_fil = 0 ")
					   _T("and kun.mdn = partyk.mdn ")
					   _T("and partyk.kun = kun.kun ")
					   _T("and partyk.ang in (select ang from partyp where partyp.mdn = partyk.mdn and partyp.ang = partyk.ang)") ;
		}
		else
		{
			Sql  =     _T("select partyk.mdn, partyk.ang, partyk.auf, partyk.ls, partyk.ls, partyk.lieferdat, fil.fil, partyk.kun_krz1, partyk.ang_stat, partyk.typ, ")
					   _T("partyk.hinweis, partyk.tou_nr, partyk.inka_nr ")						
					   _T("from partyk,fil where ")
					   _T("partyk.ang > 0 ") 
					   _T("and partyk.kun_fil = 1 ")
					   _T("and partyk.ang_stat < 5 and fil.mdn = partyk.mdn ")
					   _T("and partyk.kun = fil.fil ")
					   _T("and partyk.ang in (select ang from partyp where partyp.mdn = partyk.mdn and partyp.ang = partyk.ang)") ;
		}
		Sql += " ";
		Sql += Where;
		if (QueryString != _T(""))
		{
			Sql += _T(" and ");
			Sql += QueryString;
		}
		CQDebug::GetInstance ()->write (_T("Sql.txt"), Sql.GetBuffer ());
		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		if (cursor == -1)
		{
			AfxMessageBox (_T("Die Eingabe f�r den Filter kann nicht bearbeitet werden"), MB_OK | MB_ICONERROR, 0);
			return;
		}
		strcpy (hinweis, _T(""));
		DbClass->sqlopen (cursor);
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) kun_krz1;
			CDbUniCode::DbToUniCode (kun_krz1, pos);
			pos = (LPSTR) kun_bran2;
			CDbUniCode::DbToUniCode (kun_bran2, pos);
            if (KunFil != 0)
			{
				kun = fil;
			}
			CPartykList *abl = new CPartykList (mdn, ang, auf, kun, lieferdat, ang_stat, kun_krz1, kun_bran2);
			abl->ls = ls;
			abl->rech = rech;
			abl->typ = typ;
			pos = (LPSTR) hinweis;
			CDbUniCode::DbToUniCode (hinweis, pos);
			abl->SetHinweis (hinweis);
			abl->inka_nr = inka_nr;
			abl->tou_nr = tou_nr;
			abl->anz_pers = anz_pers;
			PartykList.push_back (abl);
// Hinweis mu� jedesmal initialisiert werden, da das Feld beim select und leerem
// Feldinhalt nicht �berschrieben wird !!?
			strcpy (hinweis, _T(""));
		}
		DbClass->sqlclose (cursor);
		Load ();
	}
	sql_mode = sql_s;

	for (std::vector<CPartykList *>::iterator pabl = PartykList.begin (); pabl != PartykList.end (); ++pabl)
	{
		CPartykList *abl = *pabl;
		CString Mdn;
		Mdn.Format (_T("%hd"), abl->mdn); 
		CString Ang;
		Ang.Format (_T("%ld"), abl->ang); 
		CString Auf;
		Auf.Format (_T("%ld"), abl->auf); 
		CString Ls;
		Ls.Format (_T("%ld"), abl->ls); 
		CString Rech;
		Rech.Format (_T("%ld"), abl->rech); 
		CString Kun;
		CString Lieferdat;
		Lieferdat = abl->lieferdat;
		Kun.Format (_T("%ld"), abl->kun); 
		_tcscpy (kun_krz1, abl->kun_krz1);
		_tcscpy (kun_bran2, abl->kun_bran2);
		CString Hinweis;
		Hinweis = abl->hinweis;
		Hinweis.TrimRight ();
		_tcscpy (hinweis, Hinweis.GetBuffer ());
		CString InkaNr;
		InkaNr.Format (_T("%ld"), abl->inka_nr);
		CString TouNr;
		TouNr.Format (_T("%ld"), abl->tou_nr);
		TCHAR LsStat [37] = {""};
		TCHAR ptwert [5];
		_stprintf (ptwert, _T("%hd"), abl->ang_stat);
        GetPtBez (_T("ang_stat"), ptwert, LsStat);
		TCHAR AngTyp [37] = {""};
		_stprintf (ptwert, _T("%hd"), abl->typ);
        GetPtBez (_T("ang_typ"), ptwert, AngTyp);
		CString AnzPers;
		AnzPers.Format (_T("%ld"), abl->anz_pers);

		int pos = 1;
        int ret = InsertItem (i, -1);
        ret = SetItemText (Mdn.GetBuffer (), i, pos ++);
        ret = SetItemText (Ang.GetBuffer (), i, pos ++);
        ret = SetItemText (Auf.GetBuffer (), i, pos ++);
        ret = SetItemText (Ls.GetBuffer (), i, pos ++);
        ret = SetItemText (Rech.GetBuffer (), i, pos ++);
        ret = SetItemText (Lieferdat.GetBuffer (), i, pos ++);
        ret = SetItemText (Kun.GetBuffer (), i, pos ++);
        ret = SetItemText (kun_krz1, i, pos ++);
		if (KunFil == 0)
		{
			ret = SetItemText (kun_bran2, i, pos ++);
		}
//        ret = SetItemText (LsStat, i, pos ++);
        ret = SetItemText (AngTyp, i, pos ++);
        ret = SetItemText (hinweis, i, pos ++);
        ret = SetItemText (TouNr.GetBuffer (), i, pos ++);
        ret = SetItemText (InkaNr.GetBuffer (), i, pos ++);
        ret = SetItemText (AnzPers.GetBuffer (), i, pos ++);
        i ++;
    }
	HideListFields ();


    Sort1 = -1;
    Sort2 = -1;
    Sort3 = -1;
    Sort4 = -1;
    Sort5 = -1;
    Sort6 = -1;
    Sort7 = -1;
    Sort8 = -1;
    Sort9 = -1;
    Sort10 = -1;
    Sort11 = -1;
    Sort12 = -1;
    Sort13 = -1;
    Sort14 = -1;
    Sort15 = -1;
    Sort (listView);
	m_List.SetFocus ();
	if (PartykList.size () > 0)
	{
		m_List.SetItemState (0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
	}
}


void CChoicePartyk::SearchCol (CListCtrl *ListBox, LPTSTR Search, int col)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, col);
        iText.MakeUpper ();

        if (_tmemcmp (Search, iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoicePartyk::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
	if ((EditText.Find (_T("*")) != -1) || EditText.Find (_T("?")) != -1)
	{
		CChoiceX::SearchMatch (EditText);
		return;
	}

    SearchCol (ListBox, EditText.GetBuffer (), SortRow);
}

int CChoicePartyk::GetPtBez (LPTSTR ptitem, LPTSTR ptwert, LPTSTR ptbez)
{
	if (ptcursor == -1)
	{
		DbClass->sqlout ((LPTSTR) this->ptbez,  SQLCHAR, sizeof (this->ptbez));
		DbClass->sqlout ((LPTSTR) this->ptbezk, SQLCHAR, sizeof (this->ptbezk));
		DbClass->sqlin ((LPTSTR) this->ptitem, SQLCHAR, sizeof (this->ptitem));
		DbClass->sqlin  ((LPTSTR) this->ptwert, SQLCHAR, sizeof (this->ptwert));
	    ptcursor = DbClass->sqlcursor (_T("select ptbez,ptbezk from ptabn where ptitem = ? ")
							           _T("and ptwert = ?"));
	}
	_tcscpy (this->ptbez, _T(""));
	_tcscpy (this->ptbezk, _T(""));
	_tcscpy (this->ptitem, ptitem);
	_tcscpy (this->ptwert, ptwert);
	DbClass->sqlopen (ptcursor);
	int dsqlstatus = DbClass->sqlfetch (ptcursor);

	_tcscpy (ptbez, this->ptbez);
	LPSTR pos = (LPSTR) ptbez;
    CDbUniCode::DbToUniCode (ptbez, pos);
	return dsqlstatus;
}

int CALLBACK CChoicePartyk::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   if (SortRow == 1)
   {

	   short li1 = _tstoi (strItem1.GetBuffer ());
	   short li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort2;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort2);
	   }
	   return 0;
   }
   if (SortRow == 2)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort3;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort3);
	   }
	   return 0;
   }
   if (SortRow == 3)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort4;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort4);
	   }
	   return 0;
   }
   if (SortRow == 4)
   {
	   DATE_STRUCT ld1;
	   DATE_STRUCT ld2;
	   DB_CLASS::ToDbDate (CString (strItem1.GetBuffer ()), &ld1);
	   DB_CLASS::ToDbDate (CString (strItem2.GetBuffer ()), &ld2);
	   DbTime dt1 (&ld1);
	   DbTime dt2 (&ld2);
	   time_t li1 = dt1.GetTime ();
	   time_t li2 = dt2.GetTime ();
	   if (li1 < li2)
	   {
		   return Sort5;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort5);
	   }
	   return 0;
   }
   if (SortRow == 5)
   {

	   long li1 = _tstol (strItem1.GetBuffer ());
	   long li2 = _tstol (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort6;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort6);
	   }
	   return 0;
   }
   else if (SortRow == 6)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort7;
   }
   else if (SortRow == 7)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort8;
   }
   else if (SortRow == 8)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort9;
   }
   else if (SortRow == 9)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort10;
   }
   else if (SortRow == 10)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort11;
   }
   else if (SortRow == 11)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort12;
   }
   else if (SortRow == 12)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort13;
   }
   else if (SortRow == 13)
   {
	return _tcscmp(strItem2.GetBuffer (), strItem1.GetBuffer ()) * Sort14;
   }
   else if (SortRow == 14)
   {
	   int li1 = _tstoi (strItem1.GetBuffer ());
	   int li2 = _tstoi (strItem2.GetBuffer ());
	   if (li1 < li2)
	   {
		   return Sort15;
	   }
	   else if (li1 > li2)
	   {
		   return (-1 * Sort15);
	   }
	   return 0;
   }
   return 0;
}

void CChoicePartyk::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
        case 2:
              Sort3 *= -1;
			  if (Sort3 < 0) SortPos = 1;
              break;
        case 3:
              Sort4 *= -1;
			  if (Sort4 < 0) SortPos = 1;
              break;
        case 4:
              Sort5 *= -1;
			  if (Sort5 < 0) SortPos = 1;
              break;
        case 5:
              Sort6 *= -1;
			  if (Sort6 < 0) SortPos = 1;
              break;
        case 6:
              Sort7 *= -1;
			  if (Sort7 < 0) SortPos = 1;
              break;
        case 7:
              Sort8 *= -1;
			  if (Sort8 < 0) SortPos = 1;
              break;
        case 8:
              Sort9 *= -1;
			  if (Sort9 < 0) SortPos = 1;
              break;
        case 9:
              Sort10 *= -1;
			  if (Sort10 < 0) SortPos = 1;
              break;
        case 10:
              Sort11 *= -1;
			  if (Sort11 < 0) SortPos = 1;
              break;
        case 11:
              Sort12 *= -1;
			  if (Sort12 < 0) SortPos = 1;
              break;
        case 12:
              Sort13 *= -1;
			  if (Sort13 < 0) SortPos = 1;
              break;
        case 13:
              Sort14 *= -1;
			  if (Sort14 < 0) SortPos = 1;
              break;
        case 14:
              Sort15 *= -1;
			  if (Sort15 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CPartykList *abl = PartykList [i];
		   
		   abl->mdn       = _tstoi (ListBox->GetItemText (i, 1));
		   abl->ang        = _tstol (ListBox->GetItemText (i, 2));
		   abl->auf       = _tstol (ListBox->GetItemText (i, 3));
		   abl->ls       = _tstol (ListBox->GetItemText (i, 3));
		   abl->rech       = _tstol (ListBox->GetItemText (i, 5));
		   abl->SetLieferdat (ListBox->GetItemText (i,6));
		   abl->kun       = _tstol (ListBox->GetItemText (i, 7));
		   abl->SetKunKrz1 (ListBox->GetItemText (i, 8));
		   abl->SetKunBran2 (ListBox->GetItemText (i, 9));
//		   abl->ang_stat   = _tstoi (ListBox->GetItemText (i, 8));
		   abl->typ   = _tstoi (ListBox->GetItemText (i, 10));
		   abl->SetHinweis (ListBox->GetItemText (i, 11));
		   abl->tou_nr = _tstol (ListBox->GetItemText (i, 12));
		   abl->inka_nr = _tstol (ListBox->GetItemText (i, 13));
		   abl->anz_pers = _tstoi (ListBox->GetItemText (i, 14));
	}


	for (int i = 1; i <= 14; i ++)
	{
		SetItemSort (i, 2);
	}

	SetItemSort (SortRow, SortPos);

}

void CChoicePartyk::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = PartykList [idx];
}

CPartykList *CChoicePartyk::GetSelectedText ()
{
	CPartykList *abl = (CPartykList *) SelectedRow;
	return abl;
}

CPartykList *CChoicePartyk::GetNextSelectedText ()
{
	IncSelection ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
        return NULL;
    }
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	CPartykList *abl = (CPartykList *) SelectedRow;
	return abl;
}

CPartykList *CChoicePartyk::GetFirstSelectedText ()
{
	FirstSelection ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
        return NULL;
    }
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	CPartykList *abl = (CPartykList *) SelectedRow;
	return abl;
}

CPartykList *CChoicePartyk::GetLastSelectedText ()
{
	LastSelection ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
        return NULL;
    }
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	CPartykList *abl = (CPartykList *) SelectedRow;
	return abl;
}


CPartykList *CChoicePartyk::GetPriorSelectedText ()
{
	DecSelection ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
        return NULL;
    }
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	CPartykList *abl = (CPartykList *) SelectedRow;
	return abl;
}

CPartykList *CChoicePartyk::GetCurrentSelectedText ()
{
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
    if (idx == -1)
    {
        return NULL;
    }
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (0));
	SetSelText (ListBox, idx);
	CPartykList *abl = (CPartykList *) SelectedRow;
	return abl;
}

void CChoicePartyk::DeleteChoiceLs (short mdn, short fil, long ang)
{
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	if (idx == -1)
	{
		idx = 0;
	}

	for (std::vector<CPartykList *>::iterator pabl = PartykList.begin () + idx; pabl != PartykList.end (); ++pabl)
	{
		CPartykList *abl = *pabl;
		if (abl->mdn == mdn &&
			abl->ang == ang)
		{
			ListBox->DeleteItem (idx);
			PartykList.erase (pabl);
			delete abl;
			ListBox->SetItemState (idx, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
			return;
		}
		idx ++;
	}

	for (std::vector<CPartykList *>::iterator pabl = PartykList.begin (); pabl != PartykList.begin () + idx - 1; ++pabl)
	{
		CPartykList *abl = *pabl;
		if (abl->mdn == mdn &&
			abl->ang == ang)
		{
			ListBox->DeleteItem (idx);
			PartykList.erase (pabl);
			delete abl;
			ListBox->SetItemState (idx, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);  
			return;
		}
	}
}

void CChoicePartyk::SaveSelection (CListCtrl *ListBox)
{
	SelectList.clear ();
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	while (idx != -1)
	{
		SelectList.push_back (PartykList[idx]);
	    idx   = ListBox->GetNextItem (idx, LVNI_SELECTED);
	}
}

void CChoicePartyk::SetDefault ()
{
	CChoiceX::SetDefault ();
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
	/**
    ListBox->SetColumnWidth (0, 0);
    ListBox->SetColumnWidth (1, 150);
    ListBox->SetColumnWidth (2, 150);
    ListBox->SetColumnWidth (3, 150);
    ListBox->SetColumnWidth (4, 250);
    ListBox->SetColumnWidth (5, 250);
	**/
}

void CChoicePartyk::OnF5 ()
{
	OnCancel ();
}

void CChoicePartyk::OnEnter ()
{
	CProcess p;

    int idx   = m_List.GetNextItem (-1, LVNI_SELECTED);
	if (idx >= 0)
	{
		CString Message;
		CString cLs = m_List.GetItemText (idx, 1);  
		long ls = _tstol (cLs.GetBuffer ());
		Message.Format (_T("Lieferschein=%0ld"), ls);
		ToClipboard (Message);
	}
	p.SetCommand (_T("53100"));
	HANDLE Pid = p.Start ();
}

CDynDialogItem *AngItem = NULL;

void CChoicePartyk::OnFilter ()
{
	CDynDialog dlg;
	CDynDialogItem *Item;

	dlg.DlgCaption = "Filter";
	dlg.UseFilter = UseFilter;

	if (vFilter.size () > 0)
	{
		for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
		{
			CDynDialogItem *i = *it;
			Item = new CDynDialogItem ();
			*Item = *i;
			if (Item->Name == _T("reset"))
			{
				if (UseFilter)
				{
					Item->Value = _T("aktiv");
				}
				else
				{
					Item->Value = _T("nicht aktiv");
				}
			}
			dlg.AddItem (Item);
		}
		dlg.Header.cy = Item->Template.y + dlg.RowSpace + 5;
	}
	else
	{
		int y = 10;

		if (EnterMdn)
		{
			Item = new CDynDialogItem ();
			Item->Name = _T("lmdn");
			Item->CtrClass = "static";
			Item->Value = _T("Mandant");
			Item->Template.x = 10;
			Item->Template.y = y;
			Item->Template.cx = 60;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1001;
			dlg.AddItem (Item);


			Item = new CDynDialogItem ();
			Item->Name = _T("partyk.mdn");
			Item->Type = Item->Short;
			Item->CtrClass = "edit";
			Item->Value = _T("");
			Item->Template.x = 70;
			Item->Template.y = y;
			Item->Template.cx = 120;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1002;
			dlg.AddItem (Item);


			y += dlg.RowSpace;
		}

		Item = new CDynDialogItem ();
		Item->Name = _T("lang");
		Item->CtrClass = "static";
		Item->Value = _T("Angebot");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1003;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("partyk.ang");
		Item->Type = Item->Long;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1004;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lauf");
		Item->CtrClass = "static";
		Item->Value = _T("Auftrag");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1005;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("partyk.auf");
		Item->Type = Item->Long;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1006;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("llieferdat");
		Item->CtrClass = "static";
		Item->Value = _T("Lieferdatum");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1007;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("partyk.lieferdat");
		Item->Type = Item->Date;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1008;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lkun");
		Item->CtrClass = "static";
		Item->Value = _T("Kunde");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1009;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("kun.kun");
		Item->Type = Item->Long;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1010;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lkun_krz1");
		Item->CtrClass = "static";
		Item->Value = _T("Name");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1011;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("kun.kun_krz1");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1012;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		if (KunFil == 0)
		{
			Item = new CDynDialogItem ();
			Item->Name = _T("lkun_bran2");
			Item->CtrClass = "static";
			Item->Value = _T("Branche");
			Item->Template.x = 10;
			Item->Template.y = y;
			Item->Template.cx = 60;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1013;
			dlg.AddItem (Item);

			Item = new CDynDialogItem ();
			Item->Name = _T("kun.kun_bran2");
			Item->CtrClass = "edit";
			Item->Value = _T("");
			Item->Template.x = 70;
			Item->Template.y = y;
			Item->Template.cx = 120;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1014;
			dlg.AddItem (Item);

			y += dlg.RowSpace;
		}

		Item = new CDynDialogItem ();
		Item->Name = _T("lhinweis");
		Item->CtrClass = "static";
		Item->Value = _T("Hinweistext");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1015;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("partyk.hinweis");
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1016;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("ltou");
		Item->CtrClass = "static";
		Item->Value = _T("Tour");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1017;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("partyk.tou_nr");
		Item->Type = Item->Long;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1018;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		if (KunFil == 0)
		{
			Item = new CDynDialogItem ();
			Item->Name = _T("linka_nr");
			Item->CtrClass = "static";
			Item->Value = _T("Inkasso");
			Item->Template.x = 10;
			Item->Template.y = y;
			Item->Template.cx = 60;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1019;
			dlg.AddItem (Item);

			Item = new CDynDialogItem ();
			Item->Name = _T("kun.inka_nr");
			Item->Type = Item->Long;
			Item->CtrClass = "edit";
			Item->Value = _T("");
			Item->Template.x = 70;
			Item->Template.y = y;
			Item->Template.cx = 120;
			Item->Template.cy = 10;
			Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
			Item->Template.dwExtendedStyle = 0;
			Item->Template.id = 1020;
			dlg.AddItem (Item);

			y += dlg.RowSpace;
		}


		Item = new CDynDialogItem ();
		Item->Name = _T("lang_typ");
		Item->CtrClass = "static";
		Item->Value = _T("Kategorie");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1021;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("partyk.typ");
		Item->Type = Item->Short;
//		Item->CtrClass = "edit";
		Item->CtrClass = "combobox";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 80;
//		Item->Template.cy = 10;
		Item->Template.cy = 100;
		FillCombo (_T("ang_typ"), &Item->ComboBoxValues);
		AngItem = Item;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP | 
			CBS_DROPDOWNLIST | WS_VSCROLL;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1022;
		dlg.AddItem (Item);

/*
		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("lang_stat");
		Item->CtrClass = "static";
		Item->Value = _T("Status");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1021;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("partyk.ang_stat");
		Item->Type = Item->Short;
		Item->CtrClass = "edit";
		Item->Value = _T("<5");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1022;
		dlg.AddItem (Item);
*/

		dlg.Header.cy = y + 40;


		Item = new CDynDialogItem ();
		Item->Name = _T("OK");
		Item->CtrClass = "button";
		Item->Value = _T("OK");
		Item->Template.x = 15;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDOK;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("cancel");
		Item->CtrClass = "button";
		Item->Value = _T("abbrechen");
		Item->Template.x = 70;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDCANCEL;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("reset");
		Item->CtrClass = "button";
		if (UseFilter)
		{
			Item->Value = _T("aktiv");
		}
		else
		{
			Item->Value = _T("nicht aktiv");
		}
		Item->Template.x = 125;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = ID_USE;
		dlg.AddItem (Item);
	}

	dlg.CreateModal ();
	INT_PTR ret = dlg.DoModal ();
	if (ret != IDOK) return;
 
	UseFilter = dlg.UseFilter;
	int count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = dlg.Items.begin (); it != dlg.Items.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (count >= (int) vFilter.size ())
			{
				Item = new CDynDialogItem ();
				*Item = *i;
				vFilter.push_back (Item);
			}
			*vFilter[count] = *i;
			count ++;
	}
    CString Query;
    CDbQuery DbQuery;

	QueryString = _T("");
	if (!UseFilter)
	{
		FillList ();
		return;
	}
	count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (strcmp (i->CtrClass, "edit") != 0 &&
				strcmp (i->CtrClass, "combobox") != 0) 
			{
				continue; 
			}
			if (i->Value.Trim () != _T(""))
			{
				if (i->Type == i->String || i->Type == i->Date)
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, TRUE,i->Type);
				}
				else
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, FALSE);
				}
				if (count > 0)
				{
					QueryString += _T(" and ");
				}
                QueryString += Query;
				count ++;
			}
	}
	FromFilter = TRUE;
	FillList ();
	FromFilter = FALSE;
}

void CChoicePartyk::FillCombo (LPTSTR Item, std::vector<CString *> *ComboValues)
{
	    char ptbez[33];
	    char ptbezk [9];
	    char ptwert [4];
		long ptlfnr;

		ComboValues->clear ();
		DbClass->sqlout ((char *) ptbez, SQLCHAR, sizeof (ptbez)); 
		DbClass->sqlout ((char *) ptbezk, SQLCHAR, sizeof (ptbezk)); 
		DbClass->sqlout ((char *) ptwert, SQLCHAR, sizeof (ptwert)); 
		DbClass->sqlout ((long *) &ptlfnr, SQLLONG, 0); 
		CString Sql;
        Sql.Format (_T("select ptbez, ptbezk, ptwert, ptlfnr from ptabn ")
                     _T("where ptitem = \"%s\" ")
				     _T("order by ptlfnr"), Item);
	    ComboValues->push_back (new CString (_T(""))); 
        int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) ptwert;
			CDbUniCode::DbToUniCode (ptwert, pos);
			pos = (LPSTR) ptbez;
			CDbUniCode::DbToUniCode (ptbez, pos);
			CString *ComboValue = new CString ();
			ComboValue->Format (_T("%s %s"), ptwert,
				                             ptbez);
			ComboValue->Trim ();
		    ComboValues->push_back (ComboValue); 
		}
		DbClass->sqlclose (cursor);
}

void CChoicePartyk::OnOK ()
{
	if (m_Take.IsWindowVisible ())
	{
		OnTake ();
	}
	else
	{
		CChoiceX::OnOK ();
	}
}

void CChoicePartyk::OnDblclkChoice (NMHDR* pNMHDR, LRESULT* pResult) 
{
	if (m_Take.IsWindowVisible ())
	{
		OnTake ();
	}
	else
	{
		CChoiceX::OnDblclkChoice (pNMHDR, pResult);
	}
}

void CChoicePartyk::OnTake ()
{
	m_Take.SetSelected (FALSE);
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    int idx   = ListBox->GetNextItem (-1, LVNI_SELECTED);
	if (idx == -1)
	{
		return;
	}
    SelectedItem = idx;
    State = TRUE;
    CString Text = ListBox->GetItemText (idx, 0);
    _tcscpy (SelText, Text.GetBuffer (256));
	CPartykList *p = PartykList [idx];
	if (p == NULL)
	{
		return;
	}
	if (TakeAction != NULL)
	{
		CAngebotCore::GetInstance ()->SetAng (p->ang);
		CAngebotCore::GetInstance ()->SetMdn (p->mdn);
		CAngebotCore::GetInstance ()->SetFil (0);
		TakeAction->Run ();
	}
}

void CChoicePartyk::HideListFields ()
{

    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);

	if (HideAuf)
	{
		ListBox->SetColumnWidth (AufPos, 0);
	}
	else
	{
		ListBox->SetColumnWidth (AufPos, 80);
	}
	if (HideLs)
	{
		ListBox->SetColumnWidth (LsPos, 0);
	}
	else
	{
		ListBox->SetColumnWidth (LsPos, 80);
	}
	if (HideRech)
	{
		ListBox->SetColumnWidth (RechPos, 0);
	}
	else
	{
		ListBox->SetColumnWidth (RechPos, 80);
	}
}
