#pragma once
#include "choicex.h"
#include "PartykList.h"
#include "ColorButton.h"
#include "RunMessage.h"
#include "AngebotCore.h"
#include <vector>

#define IDC_TAKE 6006                      

class CChoicePartyk :
	public CChoiceX
{
public:
	CChoicePartyk(CWnd* pParent = NULL);
	~CChoicePartyk();

    protected :
	    static DLGITEMTEMPLATE Take;

        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
        static int Sort5;
        static int Sort6;
        static int Sort7;
        static int Sort8;
        static int Sort9;
        static int Sort10;
        static int Sort11;
        static int Sort12;
        static int Sort13;
        static int Sort14;
        static int Sort15;
		int ptcursor;
		TCHAR ptitem [19];
		TCHAR ptwert [4];
		TCHAR ptbez [37];
		TCHAR ptbezk [17];
		short KunFil;
		BOOL FromFilter;
		int AufPos;
		int LsPos;
		int RechPos;

	    CColorButton m_Take;

		DECLARE_MESSAGE_MAP()
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
		virtual BOOL OnInitDialog();

      
    public :
		CRunMessage *TakeAction;
		BOOL HideTake;
		BOOL HideAuf;
		BOOL HideLs;
		BOOL HideRech;
		short GetKunFil ()
		{
			return KunFil;
		}

		void SetKunFil (short KunFil)
		{
			this->KunFil = KunFil;
		}
		int NewKunFil;
		CString Where;
		CString Types;
		BOOL EnterMdn;
	    std::vector<CPartykList *> PartykList;
	    std::vector<CPartykList *> SelectList;
        virtual void FillList (void);
      	virtual void AddControls ();
	    void SearchCol (CListCtrl *, LPTSTR, int);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
	    virtual void SaveSelection (CListCtrl *);
		CPartykList *GetSelectedText ();
		CPartykList *GetNextSelectedText ();
		CPartykList *GetPriorSelectedText ();
		CPartykList *GetFirstSelectedText ();
		CPartykList *GetLastSelectedText ();
		CPartykList *GetCurrentSelectedText ();
		void DeleteChoiceLs (short mdn, short fil, long ls);
        int GetPtBez (LPTSTR, LPTSTR, LPSTR);
		void DestroyList ();
		virtual void SetDefault ();
		virtual void OnEnter ();
	    virtual void OnFilter ();
		virtual void OnF5 ();
		void TestKunFil (short kun_fil);
		void FillCombo (LPTSTR name, std::vector<CString *>*ComboValues);
		afx_msg void OnTake ();
		afx_msg void OnOK ();
		afx_msg void OnDblclkChoice (NMHDR* pNMHDR, LRESULT* pResult); 
		void HideListFields ();
};
