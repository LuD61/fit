#include "StdAfx.h"
#include "PartyListCtrl.h"
#include "StrFuncs.h"
#include "resource.h"
#include "Token.h"
#include "GebindeDialog.h"
#include "PosTextDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define ID_SELECT 5005
#define ID_CANCEL 5006

#define CEditListCtrl CEditListCtrlColor

extern CString ListAGew;

CPartyListCtrl::CPartyListCtrl(void)
{
	ChoiceA = NULL;
	ModalChoiceA = FALSE;
	m_Mdn = 1;

	MaxComboEntries = 20;
	mdn = 0;
//	ListRows.Init ();
	check.LoadFromResource (AfxGetInstanceHandle (), IDB_CHECK);
	uncheck.LoadFromResource (AfxGetInstanceHandle (), IDB_UNCHECK);
	AngebotCore = CAngebotCore::GetInstance ();
	List = NULL;
	Datatables = CDatatables::GetInstance ();
	A_bas = Datatables->GetABas ();
	NoTestForAppend = FALSE;
}

CPartyListCtrl::~CPartyListCtrl(void)
{
	CString *c;
	if (ChoiceA != NULL)
	{
		delete ChoiceA;
	}
//	DestroyRows (ListRows);
    EinhCombo.FirstPosition ();
	while ((c = (CString *) EinhCombo.GetNext ()) != NULL)
	{
		delete c;
	}
	EinhCombo.Init ();
}

BEGIN_MESSAGE_MAP(CPartyListCtrl, CEditListCtrl)
	ON_BN_CLICKED(SEARCHBUTTON ,  OnChoice)
	ON_BN_CLICKED(ID_SELECT ,  OnASelected)
	ON_BN_CLICKED(ID_CANCEL ,  OnACanceled)
	ON_BN_CLICKED(ID_CANCEL ,  OnACanceled)
	ON_CBN_CLOSEUP(COMBO ,  OnCloseUp)
//	ON_WM_PAINT ()
END_MESSAGE_MAP()


void CPartyListCtrl::OnPaint ()
{
//	CEditListCtrl::OnPaint ();
	CEditListCtrlColor::OnPaint ();

	CRect Rect;
	CDC *cDC = GetDC ();
	int ItemCount = GetItemCount ();
/*
	for (int i = 0; i < ItemCount; i ++)
	{
		GetSubItemRect (i, PosRabKz, LVIR_BOUNDS, Rect);

		CString IsChecked = GetItemText (i, PosRabKz);

		if (IsChecked == _T("X"))
		{
			int cx = check.GetWidth ();
			int cy = check.GetHeight ();
			int x = (Rect.right - Rect.left - cx) / 2;
			x = (x > 0) ? x : 0;
			x += Rect.left;
			int y = (Rect.bottom - Rect.top - cy) / 2;
			y = (y > 0) ? y : 0;
			y += Rect.top;
			check.BitBlt (*cDC, x, y);
		}
		else
		{
			int cx = uncheck.GetWidth ();
			int cy = uncheck.GetHeight ();
			int x = (Rect.right - Rect.left - cx) / 2;
			x = (x > 0) ? x : 0;
			x += Rect.left;
			int y = (Rect.bottom - Rect.top - cy) / 2;
			y = (y > 0) ? y : 0;
			y += Rect.top;
			uncheck.BitBlt (*cDC, x, y);
		}
	}
*/
	ReleaseDC (cDC);
}

void CPartyListCtrl::FirstEnter ()
{
	if (IsListDialog)
	{
		return;
	}
	BOOL ret = AngebotCore->KunOk ();
	if (!ret)
	{
//		AfxMessageBox (_T("Achtung!!\nDie Kundennummer ist nicht korrekt"));
		return;
	}
	if (GetItemCount () > 0)
	{
		StartEnter (POSME, 0);
	}
	else
	{
		if (!AppendEmpty ()) return; 
		StartEnter (POSA, 0);
	}
	PerformListChangeHandler ();
}

void CPartyListCtrl::StartEnter (int col, int row)
{
	AngebotCore->SetListStruct (EditRow);
	if (col < POSA) col = POSA;
	int count = GetHeaderCtrl ()->GetItemCount ();
	while (IsDisplayOnly (col) && col < count - 1) col ++;
	if (col == POSA)
	{
		CString A = GetItemText (EditRow, POSA);
		if (CStrFuncs::StrToDouble (A) != 0.0)
		{
			col = POSME;
		}
	}
	if (col == POSABZ1) return;
	if (col == POSA)
	{
			if (IsWindow (SearchListCtrl.Edit.m_hWnd))
			{
				StopEnter ();
			}
			EditNumber = 0;
			CEditListCtrl::StartSearchListCtrl (col, row);
	}
	else if (col == POSEINH)
	{
		if (IsWindow (ListComboBox.m_hWnd))
		{
				StopEnter ();
		}
		BeforeEinh (row);
		CEditListCtrl::StartEnterCombo (col, row, &EinhCombo);
		ListComboBox.SetDroppedWidth (200);
		oldsel = ListComboBox.GetCurSel ();
	}
	else if (col == POSGEBINDE)
	{
		StopEnter ();
		EditRow = row;
		EditCol = col;
		EnterGebinde ();
	}
	else if (col == POSEINZELPR)
	{
		if (IsWindow (ListCheckBox.m_hWnd))
		{
				StopEnter ();
		}
		CEditListCtrl::StartEnterCheckBox (col, row);
	}

	else
	{
		CEditListCtrl::StartEnter (col, row);
	}
}

void CPartyListCtrl::OnCloseUp ()
{
	if (EditCol == POSEINH)
	{
		GetEnter ();
		SetList ();
		AngebotCore->Calculate ();
	}
}

void CPartyListCtrl::StopEnter ()
{
//	CEditListCtrl::StopEnter ();
	CString VkNetto;
	CString VkBrutto;
	double vk_netto;
	double vk_brutto;
	BOOL stopped = FALSE;

	if (EditCol == POSGEBINDE)
	{
		return;
	}
	if (IsWindow (ListEdit.m_hWnd))
	{
		CString Text;
		ListEdit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		ListEdit.DestroyWindow ();
		stopped = TRUE;
	}
	else if (IsWindow (ListComboBox.m_hWnd))
	{
		CString Text;
		int idx = ListComboBox.GetCurSel ();
		if (idx < 0) 
		{
			Text = "";
		}
		if (ListComboBox.GetCount () > 0)
		{
			ListComboBox.GetLBText (idx, Text);
			FormatText (Text);
			FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		}
		ListComboBox.DestroyWindow ();
		stopped = TRUE;
	}
	else if (IsWindow (ListCheckBox.m_hWnd))
	{
		int check = ListCheckBox.GetCheck ();
        if (check == BST_CHECKED)
		{
			FillList.SetItemText (_T("X"), EditRow, EditCol);
		}
		else
		{
			FillList.SetItemText (_T(" "), EditRow, EditCol);
		}
		ListCheckBox.DestroyWindow ();
		stopped = TRUE;
	}
	else if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		CString Text;
		SearchListCtrl.Edit.GetWindowText (Text);
		FormatText (Text);
		FillList.SetItemText (Text.GetBuffer (0), EditRow, EditCol);
		SearchListCtrl.DestroyWindow ();
		stopped = TRUE;
	}
	if (stopped)
	{
		VkNetto  =  GetItemText (EditRow, POSVK);
		VkBrutto =  GetItemText (EditRow, POSVKBRUTTO);
		vk_netto  = CStrFuncs::StrToDouble (VkNetto);
		vk_brutto = CStrFuncs::StrToDouble (VkBrutto);
		if (EditCol == POSVK)
		{
			PARTYP **it = List->Get (EditRow);
			if (it != NULL)
			{
				PARTYP *p = *it;
				if (p->auf_vk_euro != vk_netto)
				{
					vk_brutto = AngebotCore->CalculateVkBrutto (vk_netto);
				}
				VkBrutto.Format (_T("%.2lf"), vk_brutto);
				FillList.SetItemText (VkBrutto.GetBuffer (), EditRow, POSVKBRUTTO);
			}
		}
		else if (EditCol == POSVKBRUTTO)
		{
			PARTYP **it = List->Get (EditRow);
			if (it != NULL)
			{
				PARTYP *p = *it;
				if (p->auf_vk_brutto != vk_brutto)
				{
					vk_netto = AngebotCore->CalculateVkNetto (vk_brutto);
				}
				VkNetto.Format (_T("%.4lf"), vk_netto);
				FillList.SetItemText (VkNetto.GetBuffer (), EditRow, POSVK);
			}
		}

		SetList ();
//		AngebotCore->Calculate (EditRow, EditCol);
		AngebotCore->Calculate ();
		GetList ();
//		AngebotCore->SetListStruct (EditRow);
	}
}

void CPartyListCtrl::SetList ()
{
	PARTYP **it;
	PARTYP *p = NULL;
	if (GetItemCount () > 0)
	{
		CString Posi =  GetItemText (EditRow, POSPOS);
		CString Stat =  GetItemText (EditRow, POSSTAT);
		CString A    =  GetItemText (EditRow, POSA);
		CString Me   =  GetItemText (EditRow, POSME);
		CString BasInh  =  GetItemText (EditRow, POSBASINH);
		CString BasMe  =  GetItemText (EditRow, POSBASME);
		CString VkPr =  GetItemText (EditRow, POSVK);
		CString VkBrutto =  GetItemText (EditRow, POSVKBRUTTO);
		CString PosVk =  GetItemText (EditRow, POSVKME);
		CString Einh =  GetItemText (EditRow, POSEINH);
		CString SinglePrice =  GetItemText (EditRow, POSEINZELPR);
		if (List->anz > EditRow)
		{
			it = List->Get (EditRow);
			if (it != NULL)
			{
				p = *it;
			}
		}
		if (p == NULL)
		{
			p = new PARTYP;
			memcpy (p, &partyp_null, sizeof (PARTYP));
			p->mdn = AngebotCore->GetMdn ();
			p->fil = AngebotCore->GetFil ();
			p->ang = Datatables->GetPartyk ()->partyk.ang;
			p->row_id = 0l;
			List->Add (p);
		}
		p->posi = _tstol (Posi);
		p->ls_pos_kz = _tstoi (Stat);
		p->a = CStrFuncs::StrToDouble (A);
		p->auf_me = CStrFuncs::StrToDouble (Me);
		if (p->me_einh == 2)
		{
			p->inh = CStrFuncs::StrToDouble (BasInh);
		}
		else
		{
			p->a_gew = CStrFuncs::StrToDouble (BasInh);
		}
		p->lief_me = CStrFuncs::StrToDouble (BasMe);
		p->auf_vk_euro = CStrFuncs::StrToDouble (VkPr);
		p->auf_vk_pr   = CStrFuncs::StrToDouble (VkPr);
		p->auf_vk_brutto = CStrFuncs::StrToDouble (VkBrutto);
		if (SinglePrice == _T("X"))
		{
			p->preinzel_kz = 1;
		}
		else
		{
			p->preinzel_kz = 0;
		}
		CToken t (Einh, " ");
		if (t.GetAnzToken ())
		{
			p->me_einh_kun = _tstoi (t.GetToken (0));
			AngebotCore->GetEinhBez (p->me_einh_kun);
			_tcscpy (p->auf_me_bz, AngebotCore->GetPtabn ()->ptabn.ptbezk);
		}

		AngebotCore->SetA (p->a);
		AngebotCore->SetMeEinhKun (p->me_einh_kun);
		AngebotCore->GetAktEinh ();
		double inh = AngebotCore->GetInh ();
		inh = (inh == 0.0) ? 1 : inh;
		if (p->inh == 0.0)
		{
			p->inh = inh;
		}
	}
	ListAGew = GetItemText (3, POSBASME);
}

void CPartyListCtrl::GetList ()
{
	PARTYP **it;
	PARTYP *p;
	CString Item;
	if (GetItemCount () > EditRow && List->anz > EditRow)
	{
			it = List->Get (EditRow);
			if (it != NULL)
			{
				p = *it;
				Item.Format (_T("%.3lf"), p->auf_me);
				FillList.SetItemText (Item.GetBuffer (), EditRow, POSME);
				if (p->me_einh == 2)
				{
					Item.Format (_T("%.3lf"), p->inh);
				}
				else
				{
					Item.Format (_T("%.3lf"), p->a_gew);
				}
				FillList.SetItemText (Item.GetBuffer (), EditRow, POSBASINH);
				Item.Format (_T("%.3lf"), p->lief_me);
				FillList.SetItemText (Item.GetBuffer (), EditRow, POSBASME);
				Item.Format (_T("%.4lf"), p->auf_vk_euro);
				FillList.SetItemText (Item.GetBuffer (), EditRow, POSVK);
				Item.Format (_T("%.2lf"), p->auf_vk_brutto);
				FillList.SetItemText (Item.GetBuffer (), EditRow, POSVKBRUTTO);
				Item.Format (_T("%.2lf"), p->pos_vk);
				FillList.SetItemText (Item.GetBuffer (), EditRow, POSVKME);
			}
	}
}

void CPartyListCtrl::AppendList ()
{
	PARTYP *p;
	if (GetItemCount () > 0)
	{
		int rowCount = GetItemCount () - 1;
		CString Posi =  GetItemText (rowCount, POSPOS);
		CString Stat =  GetItemText (rowCount, POSSTAT);
		CString A    =  GetItemText (rowCount, POSA);
		CString Me   =  GetItemText (rowCount, POSME);
		CString BasMe  =  GetItemText (rowCount, POSBASME);
		CString VkPr =  GetItemText (rowCount, POSVK);
		CString VkBrutto =  GetItemText (rowCount, POSVKBRUTTO);
		CString PosVk =  GetItemText (rowCount, POSVKME);
		CString LdPr =  GetItemText (rowCount, POSLDPR);
		CString Einh =  GetItemText (rowCount, POSEINH);

		p = new PARTYP;
		memcpy (p, &partyp_null, sizeof (PARTYP));
		p->mdn = AngebotCore->GetMdn ();
		p->fil = AngebotCore->GetFil ();
		p->ang = Datatables->GetPartyk ()->partyk.ang;
		p->row_id = 0l;
		List->Add (p);

		p->posi = _tstol (Posi);
		p->ls_pos_kz = _tstoi (Stat);
		p->a = CStrFuncs::StrToDouble (A);
		p->auf_me = CStrFuncs::StrToDouble (Me);
		p->lief_me = CStrFuncs::StrToDouble (BasMe);
		p->auf_vk_euro = CStrFuncs::StrToDouble (VkPr);
		p->auf_vk_pr   = CStrFuncs::StrToDouble (VkPr);
		p->auf_vk_brutto   = CStrFuncs::StrToDouble (VkBrutto);
		p->pos_vk   = CStrFuncs::StrToDouble (PosVk);
	//	p->auf_lad_euro = CStrFuncs::StrToDouble (LdPr);
	//	p->auf_lad_pr = CStrFuncs::StrToDouble (LdPr);
		CToken t (Einh, " ");
		if (t.GetAnzToken ())
		{
			p->me_einh_kun = _tstoi (t.GetToken (0));
			AngebotCore->GetEinhBez (p->me_einh_kun);
			_tcscpy (p->auf_me_bz, AngebotCore->GetPtabn ()->ptabn.ptbezk);
		}

		_tcscpy (p->lief_me_bz, _T(" "));
		AngebotCore->Calculate (rowCount, 0);
		GetList ();
		AngebotCore->SetListStruct (rowCount);
	}
}

void CPartyListCtrl::InsertList ()
{
	PARTYP *p;
	if (GetItemCount () > 0)
	{
		CString Posi =  GetItemText (EditRow, POSPOS);
		CString Stat =  GetItemText (EditRow, POSSTAT);
		CString A    =  GetItemText (EditRow, POSA);
		CString Me   =  GetItemText (EditRow, POSME);
		CString VkPr =  GetItemText (EditRow, POSVK);
		CString VkBrutto =  GetItemText (EditRow, POSVKBRUTTO);
		CString PosVk =  GetItemText (EditRow, POSVKME);
		CString LdPr =  GetItemText (EditRow, POSLDPR);
		CString Einh =  GetItemText (EditRow, POSEINH);

		p = new PARTYP;
		memcpy (p, &partyp_null, sizeof (PARTYP));
		p->mdn = AngebotCore->GetMdn ();
		p->fil = AngebotCore->GetFil ();
		p->ang = Datatables->GetPartyk ()->partyk.ang;
		p->row_id = 0l;
		List->Insert (p, EditRow);

		p->posi = _tstol (Posi);
		p->ls_pos_kz = _tstoi (Stat);
		p->a = CStrFuncs::StrToDouble (A);
		p->auf_me = CStrFuncs::StrToDouble (Me);
		p->auf_vk_euro = CStrFuncs::StrToDouble (VkPr);
		p->auf_vk_pr   = CStrFuncs::StrToDouble (VkPr);
		p->auf_vk_brutto   = CStrFuncs::StrToDouble (VkBrutto);
		p->pos_vk   = CStrFuncs::StrToDouble (PosVk);
//		p->auf_lad_euro = CStrFuncs::StrToDouble (LdPr);
//		p->auf_lad_pr = CStrFuncs::StrToDouble (LdPr);
		CToken t (Einh, " ");
		if (t.GetAnzToken ())
		{
			p->me_einh_kun = _tstoi (t.GetToken (0));
			AngebotCore->GetEinhBez (p->me_einh_kun);
			_tcscpy (p->auf_me_bz, AngebotCore->GetPtabn ()->ptabn.ptbezk);
		}
	}
}

void CPartyListCtrl::SetSel (CString& Text)
{

   if (EditCol == POSA || EditCol == POSME || EditCol == POSBASINH ||
	   EditCol == POSBASME ||
	   EditCol == POSVK || EditCol == POSVKBRUTTO)
   {
		ListEdit.SetSel (0, -1);
   }
   else
   {
		Text.TrimRight ();
		int cpos = Text.GetLength ();
		ListEdit.SetSel (cpos, cpos);
   }
}

void CPartyListCtrl::FormatText (CString& Text)
{
    if (EditCol == POSVK)
	{
		DoubleToString (StrToDouble (Text), Text, 4);
	}
    else if (EditCol == POSLDPR)
	{
		DoubleToString (StrToDouble (Text), Text, 2);
	}
    else if (EditCol == POSME)
	{
		DoubleToString (StrToDouble (Text), Text, 3);
	}
}

void CPartyListCtrl::NextRow ()
{
    GetEnter ();
	int count = GetItemCount ();
	if (EditRow >= count - 1)
	{
		    CString A = GetItemText (EditRow, POSA);
			if ((StrToDouble (A) == 0.0))
			{
				return;
			}
	}
	if (EditCol == POSA)
	{
		if (!ReadABz1 ())
		{
			return;
		}
	}
	SetEditText ();
//	TestIprIndex ();
    count = GetItemCount ();
	if (EditRow >= count - 1)
	{
	    StopEnter ();
		if (AppendEmpty () == FALSE)
		{
			return;
		}
		EditRow ++;
		EditCol = 0;
	}
	else
	{
		StopEnter ();
		EditRow ++;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	PerformListChangeHandler ();
	AngebotCore->SetListStruct (EditRow);
}

void CPartyListCtrl::PriorRow ()
{
    StopEnter ();

	int count = GetItemCount ();
	if (EditRow == count - 1)
	{
		GetEnter ();
		CString A = GetItemText (EditRow, POSA);
		A_bas->a_bas.a = StrToDouble (A);
		if (A_bas->a_bas.a == 0.0 || A_bas->dbreadfirst () != 0)
		{
			PARTYP **it;
			it = List->Get (EditRow);
			if (it != NULL)
			{
				List->Drop (EditRow);
				delete *it;
				*it = NULL;
			}
	        DeleteItem (EditRow);
		}
		if (EditRow <= 0)
		{
			GetParent ()->SetFocus ();
			return;
		}
	}
	else
	{
		if (EditRow <= 0)
		{
			GetParent ()->SetFocus ();
			return;
		}
		if (IsWindow (SearchListCtrl.Edit.m_hWnd) && EditCol == POSA)
		{
			if (!ReadABz1 ())
			{
				return;
			}
		}
//		TestIprIndex ();
	}

//	StopEnter ();
	if (EditRow > 0)
	{
		EditRow --;
	}
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	for (int i = 0; i < (int) vSelect.size (); i ++)
	{
		BOOL& b = vSelect[i];
		b = FALSE;
		FillList.SetItemImage (i,0);
	}
	PerformListChangeHandler ();
}

BOOL CPartyListCtrl::LastCol ()
{
//	if (EditCol < POSVKBRUTTO) return FALSE;
	if (EditCol < POSEINZELPR) return FALSE;
	return TRUE;
}

void CPartyListCtrl::OnReturn ()
{
    GetEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	int rowCount = GetItemCount ();
	if (EditCol >= colCount - 1 &&
		EditRow >= rowCount - 1)
	{
		CString A = GetItemText (EditRow, POSA);
			if ((StrToDouble (A) == 0.0))
			{
			return;
			}
	}
	if (EditCol == POSA)
	{
		if (!ReadABz1 ())
		{
			return;
		}
	}
	if (LastCol ())
	{
//		TestIprIndex ();
		if (EditRow >= rowCount - 1)
		{
			StopEnter ();
			if (AppendEmpty () == FALSE)
			{
				return;
			}
		}
		StopEnter ();
		EditRow ++;
		if (EditRow == rowCount)
		{
			EditCol = POSA;
		}
		else
		{
			EditCol = POSME;
		}
	}
	else
	{
	    StopEnter ();
		EditCol ++;
		while (IsDisplayOnly (EditCol))
		{
			EditCol ++;
		}
	}
	EnsureColVisible (EditCol);
	EnsureVisible (EditRow, FALSE);
    StartEnter (EditCol, EditRow);
	PerformListChangeHandler ();
	AngebotCore->SetListStruct (EditRow);
}

void CPartyListCtrl::NextCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol == POSLDPR)
	{
		return;
	}
	if (EditCol == POSA)
	{
		if (!ReadABz1 ())
		{
			return;
		}
	}
	StopEnter ();
	EditCol ++;
	if (EditCol == POSABZ1)
	{
		EditCol ++;
	}
	EnsureColVisible (EditCol);
    StartEnter (EditCol, EditRow);
}

void CPartyListCtrl::PriorCol ()
{
	int count = GetHeaderCtrl ()->GetItemCount ();
	if (EditCol <= 1)
	{
		return;
	}
	if (EditCol == POSA)
	{
		if (!ReadABz1 ())
		{
			return;
		}
	}
	StopEnter ();
	EditCol --;
	if (EditCol == POSABZ1)
	{
		EditCol --;
	}
	while (IsDisplayOnly (EditCol) && EditCol >= 0) EditCol --;
    StartEnter (EditCol, EditRow);
}

BOOL CPartyListCtrl::InsertRow ()
{
	CString A = GetItemText (EditRow, POSA);
	if (StrToDouble (A) == 0.0)
	{
			return FALSE;
	}

	CString SPos;
	GetColValue (EditRow, POSPOS, SPos);
	int pos2 = _tstoi (SPos.GetBuffer ());
	int posakt = pos2;
	if (EditRow > 0)
	{
		GetColValue (EditRow - 1, POSPOS, SPos);
		int pos1 = _tstoi (SPos.GetBuffer ());
		pos2 = pos1 + 1;
	}
	else
	{
		pos2 = 1;
	}
	SPos.Format (_T("%d"), pos2);
	StopEnter ();
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	FillList.InsertItem (EditRow, -1);
	FillList.SetItemText (SPos.GetBuffer (), EditRow, POSPOS);
	FillList.SetItemText (_T("0 "), EditRow, POSSTAT);
	FillList.SetItemText (_T("0 "), EditRow, POSA);
	FillList.SetItemText (_T(""), EditRow, POSABZ1);
	FillList.SetItemText (_T("0,00 "), EditRow, POSME);
	FillList.SetItemText (_T("0,00 "), EditRow, POSVK);
	FillList.SetItemText (_T("0,00 "), EditRow, POSVKBRUTTO);
	FillList.SetItemText (_T("0,00 "), EditRow, POSVKME);
//	FillList.SetItemText (_T("0,00 "), EditRow, POSLDPR);
	if ((pos2 % 10) == 0 || posakt == pos2)
	{
		int rowCount = GetItemCount ();
        for (int i = 0; i < rowCount; i ++)
		{
			GetColValue (i, POSPOS, SPos);
			int pos = (i  + 1) * 10;
			SPos.Format (_T("%d"), pos);
			FillList.SetItemText (SPos.GetBuffer (), i, POSPOS);
		}
	}
	InsertList ();
	StartEnter (0, EditRow);
	return TRUE;
}

BOOL CPartyListCtrl::DeleteRow ()
{
	BOOL ret = FALSE;
	PARTYP **it;

	if (!IsWindow (m_hWnd)) return FALSE;
	StopEnter ();
	it = List->Get (EditRow);
	if (it != NULL)
	{
		PARTYP *p = *it;
		double a = p->a;
		List->Drop (EditRow);
		delete p;
		AngebotCore->Drop (a);
	}
	ret = CEditListCtrl::DeleteRow ();
	AngebotCore->SetListStruct (EditRow);
	AngebotCore->SetVatToNetto ();
	RefreshPositions ();
	AngebotCore->Calculate ();
	return ret;
}

BOOL CPartyListCtrl::AppendEmpty ()
{

	int rowCount = GetItemCount ();
	if (rowCount > 0 && !NoTestForAppend)
	{
		CString A = GetItemText (EditRow, POSA);
		if (StrToDouble (A) == 0.0)
		{
			return FALSE;
		}
	}
	int colCount = GetHeaderCtrl ()->GetItemCount ();
	CString SPos = "10";
	if (rowCount > 0)
	{
		GetColValue (EditRow, POSPOS, SPos);
		int pos = _tstoi (SPos.GetBuffer ()) + 10;
		SPos.Format (_T("%d"), pos);
	}
	int anz_pers = Datatables->GetPartyk ()->partyk.anz_pers;
	CString Me;
	Me.Format (_T("%d"), anz_pers);
	FillList.InsertItem (rowCount, -1);
	FillList.SetItemText (SPos.GetBuffer (), rowCount, POSPOS);
	FillList.SetItemText (_T("0 "), rowCount, POSSTAT);
	FillList.SetItemText (_T("0 "), rowCount, POSA);
	FillList.SetItemText (_T(""), rowCount, POSABZ1);
//	FillList.SetItemText (_T("0,000"), rowCount, POSME);
	FillList.SetItemText (Me.GetBuffer (), rowCount, POSME);
	FillList.SetItemText (_T("0,000"), rowCount, POSBASME);
	FillList.SetItemText (_T("0,0000"), rowCount, POSVK);
	FillList.SetItemText (_T("0,00"), rowCount, POSVKBRUTTO);
	FillList.SetItemText (_T("0,00"), rowCount, POSVKME);
	AppendList ();
	return TRUE;
}

void CPartyListCtrl::HiLightItem (int Item)
{
/*
	    if (Item < 0) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CPartyListCtrl::RunItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		if (b)
		{
			b = FALSE;
			FillList.SetItemImage (Item,0);
		}
		else
		{
			b = TRUE;
			FillList.SetItemImage (Item,1);
		}

		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			if (i == Item) continue;
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}
*/
}

void CPartyListCtrl::RunCtrlItemClicked (int Item)
{
/*
	    if (Item == -1) return;
		BOOL& b = vSelect[Item];
		b = TRUE;
		FillList.SetItemImage (Item,1);
*/
}

void CPartyListCtrl::RunShiftItemClicked (int Item)
{
/*
	    int start;
		int end;
        int row = -1;

	    if (Item == -1) return;
		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			if (b)
			{
				row = i;
				break;
			}
		}

		if (row == -1)
		{
			RunItemClicked (Item);
			return;
		}


		for (int i = 0; i < (int) vSelect.size (); i ++)
		{
			BOOL& b = vSelect[i];
			b = FALSE;
			FillList.SetItemImage (i,0);
		}

		if (row < Item)
		{
			start = row;
			end = Item;
		}
		else
		{
			start = Item;
			end   = row;
		}
		for (int i = start; i <= end; i ++)
		{
		    BOOL& b = vSelect[i];
			b = TRUE;
			FillList.SetItemImage (i,1);
		}
*/
}

void CPartyListCtrl::OnChoice ()
{
	if (EditCol == POSA)
	{
		OnAChoice (CString (_T("")));
	}
}

void CPartyListCtrl::OnAChoice (CString& Search)
{
	AChoiceStat = TRUE;
	if (ChoiceA != NULL && !ModalChoiceA)
	{
		ChoiceA->SearchText = Search;
		ChoiceA->SetSearchText ();
		ChoiceA->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceA == NULL)
	{
		ChoiceA = new CChoiceA (this);
	    ChoiceA->IsModal = ModalChoiceA;
	    ChoiceA->HideEnter = FALSE;
	    ChoiceA->HideFilter = FALSE;
//		ChoiceA->SetBitmapButton (TRUE);
		ChoiceA->IdArrDown = IDI_HARROWDOWN;
		ChoiceA->IdArrUp   = IDI_HARROWUP;
		ChoiceA->IdArrNo   = IDI_HARROWNO;
		ChoiceA->SetIdSelect (ID_SELECT);
		ChoiceA->SetIdCancel (ID_CANCEL);
		ChoiceA->SingleSelection = FALSE;
		ChoiceA->WithSmt = TRUE;
		ChoiceA->DefaultSmt = _T("P");
		ChoiceA->CreateDlg ();
	}

    ChoiceA->SetDbClass (A_bas);
//	ChoiceA->Where = _T(" and smt = \"P\"");
	ChoiceA->SearchText = Search;
	if (ModalChoiceA)
	{
			ChoiceA->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceA->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;
		ChoiceA->MoveWindow (&rect);
		ChoiceA->SetFocus ();
		return;
	}
    if (ChoiceA->GetState ())
    {
		  CABasList *abl = ChoiceA->GetSelectedText (); 
		  if (abl == NULL) 
		  {
			  AChoiceStat = FALSE;
			  return;
		  }
		  memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
		  A_bas->a_bas.a = abl->a;
		  A_bas->dbreadfirst ();
		  AngebotCore->GetABasList ()->Add (&A_bas->a_bas);
		  CString Text;
		  Text.Format (_T("%.0lf"), A_bas->a_bas.a);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
          SetSearchSel (Text);
		  CString ABz1;
		  ABz1.Format (_T("%s"), A_bas->a_bas.a_bz1);
		  FillList.SetItemText (ABz1.GetBuffer (), EditRow, POSABZ1);
    }
	else
	{
		  AChoiceStat = FALSE;
		  SearchListCtrl.Edit.SetFocus ();
		  CString Text;
		  SearchListCtrl.Edit.GetWindowText (Text);
          SetSearchSel (Text);
	}
}


void CPartyListCtrl::FillEmptyRow ()
{
	int vat = 0;
	AngebotCore->SetA (A_bas->a_bas.a);
	AngebotCore->SetListRow (EditRow);
	BOOL ret = AngebotCore->GetPrice ();
	if (ret)
	{
		CString PrNetto;
		PrNetto.Format (_T("%.4lf"), AngebotCore->GetPrNetto ());
		CString PrBrutto;
		PrBrutto.Format (_T("%.2lf"), AngebotCore->GetPrBrutto ());

		FillList.SetItemText (PrNetto.GetBuffer (), EditRow, POSVK);
		FillList.SetItemText (PrBrutto.GetBuffer (), EditRow, POSVKBRUTTO);
		vat = AngebotCore->GetActVat ();
	}
	AngebotCore->SetA (A_bas->a_bas.a);
    FillEinhCombo ();
	ret = AngebotCore->GetEinh ();
	if (ret)
	{
		CString Item;
		Item.Format (_T("%hd %s"), AngebotCore->GetMeEinhKun (),
			                       AngebotCore->GetMeBz().GetBuffer ());
		PARTYP *p = *List->Get (EditRow);
		if (p != NULL)
		{
			p->me_einh = AngebotCore->GetMeEinh ();
			p->me_einh_kun = AngebotCore->GetMeEinhKun ();
			_tcscpy (p->lief_me_bz, AngebotCore->GetBasisMeBz ().Trim ().GetBuffer ());
			_tcscpy (p->auf_me_bz, AngebotCore->GetMeBz ().Trim ().GetBuffer ());
			p->auf_vk_euro = AngebotCore->GetPrNetto ();
			p->auf_vk_pr = AngebotCore->GetPrNetto ();
			p->auf_vk_brutto = AngebotCore->GetPrBrutto ();
			p->inh = AngebotCore->GetInh ();
			p->mwst = vat;
			p->a_gew = A_bas->a_bas.a_gew;

		}
		AngebotCore->FindEinhCombo (&Item, &EinhCombo);
		FillList.SetItemText (Item.GetBuffer (), 
			EditRow, POSEINH);
		GetList ();
	}
	SetList ();
}

void CPartyListCtrl::OnASelected ()
{
	  int vat = 0;	

	  std::vector<CABasList *> SelectList;
	  int CurrentRow;
	  int CurrentCol;
	  if (ChoiceA == NULL) return;
	  CurrentRow = EditRow;
	  CurrentCol = EditCol;
	  SelectList = ChoiceA->SelectList;
	  CABasList *abl = ChoiceA->GetSelectedText (); 
	  if (abl == NULL) 
	  {
		  AChoiceStat = FALSE;
		  return;
	  }
	  if (SelectList.size () <= 1)
	  {
		  memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
		  A_bas->a_bas.a = abl->a;
		  A_bas->dbreadfirst ();
		  AngebotCore->GetABasList ()->Add (&A_bas->a_bas);
		  CString Text;
		  Text.Format (_T("%.0lf"), A_bas->a_bas.a);
		  SearchListCtrl.Edit.SetWindowText (Text);
		  SearchListCtrl.Edit.SetFocus ();
		  SetSearchSel (Text);
		  CString ABz1;
		  ABz1.Format (_T("%s"), A_bas->a_bas.a_bz1);
		  FillList.SetItemText (ABz1.GetBuffer (), EditRow, POSABZ1);
	  }
	  else
	  {
		  StopEnter ();
		  BOOL IsVatFull = AngebotCore->IsVatFull;
		  EditRow = CurrentRow;
		  NoTestForAppend = TRUE;
		  EditCol = POSA;
		  memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
		  A_bas->a_bas.a = abl->a;
		  A_bas->dbreadfirst ();
	      AngebotCore->GetABasList ()->Add (&A_bas->a_bas);
		  CString A;
		  A.Format (_T("%.0lf"), A_bas->a_bas.a);
		  FillList.SetItemText (A.GetBuffer (), EditRow, POSA);
		  CString ABz1;
		  ABz1.Format (_T("%s"), A_bas->a_bas.a_bz1);
		  FillList.SetItemText (ABz1.GetBuffer (), EditRow, POSABZ1);
		  FillEmptyRow ();
 		  PARTYP *p = *List->Get (EditRow);

		  AngebotCore->SetA (A_bas->a_bas.a);
		  AngebotCore->SetListRow (EditRow);
		  BOOL ret = AngebotCore->GetPrice ();
		  if (ret)
		  {
				CString PrNetto;
				PrNetto.Format (_T("%.4lf"), AngebotCore->GetPrNetto ());
				CString PrBrutto;
				PrBrutto.Format (_T("%.2lf"), AngebotCore->GetPrBrutto ());

				FillList.SetItemText (PrNetto.GetBuffer (), EditRow, POSVK);
				FillList.SetItemText (PrBrutto.GetBuffer (), EditRow, POSVKBRUTTO);
				vat = AngebotCore->GetActVat ();
		  }
		  AngebotCore->SetA (A_bas->a_bas.a);
		  FillEinhCombo ();
		  ret = AngebotCore->GetEinh ();
		  if (ret)
		  {
				CString Item;
				Item.Format (_T("%hd %s"), AngebotCore->GetMeEinhKun (),
										   AngebotCore->GetMeBz().GetBuffer ());
				p->me_einh = AngebotCore->GetMeEinh ();
				p->me_einh_kun = AngebotCore->GetMeEinhKun ();
				_tcscpy (p->lief_me_bz, AngebotCore->GetBasisMeBz ().Trim ().GetBuffer ());
				_tcscpy (p->auf_me_bz, AngebotCore->GetMeBz ().Trim ().GetBuffer ());
				p->auf_vk_euro = AngebotCore->GetPrNetto ();
				p->auf_vk_pr = AngebotCore->GetPrNetto ();
				p->auf_vk_brutto = AngebotCore->GetPrBrutto ();
				p->inh = AngebotCore->GetInh ();
				if (p->inh == 0.0)
				{
					p->inh = 1.0;
				}
				p->lief_me = p->auf_me * p->inh;
				p->mwst = vat;
				p->teil_smt = A_bas->a_bas.teil_smt;
				p->a_gew = A_bas->a_bas.a_gew;
				AngebotCore->FindEinhCombo (&Item, &EinhCombo);
				FillList.SetItemText (Item.GetBuffer (), 
					EditRow, POSEINH);
				FillList.SetItemText (p->lief_me_bz, 
					EditRow, POSBASEINH);
				GetList ();
		  }

		  p->teil_smt = A_bas->a_bas.teil_smt;
//		  FillList.SetItemText (p->auf_me_bz, 
//				EditRow, POSEINH);
		  FillList.SetItemText (p->lief_me_bz, 
			EditRow, POSBASEINH);
//		  SetList ();
		  GetList ();
		  for (std::vector<CABasList *>::iterator it = SelectList.begin ();
												  it != SelectList.end ();
												  ++it)
		  {
			  if (it == SelectList.begin ()) continue;
			  abl = *it;
			  if (EditRow == GetItemCount () - 1)
			  {
					if (AppendEmpty () == FALSE)
					{
						break;
					}
					EditRow ++;
			  }
			  memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
			  A_bas->a_bas.a = abl->a;
			  A_bas->dbreadfirst ();
	          AngebotCore->GetABasList ()->Add (&A_bas->a_bas);
			  CString A;
			  A.Format (_T("%.0lf"), A_bas->a_bas.a);
			  FillList.SetItemText (A.GetBuffer (), EditRow, POSA);
			  CString ABz1;
			  ABz1.Format (_T("%s"), A_bas->a_bas.a_bz1);
			  FillList.SetItemText (ABz1.GetBuffer (), EditRow, POSABZ1);
			  FillEmptyRow ();
 			  p = *List->Get (EditRow);


			  AngebotCore->SetA (A_bas->a_bas.a);
			  AngebotCore->SetListRow (EditRow);
			  BOOL IsVatFull = AngebotCore->IsVatFull;
			  BOOL ret = AngebotCore->GetPrice ();
			  if (ret)
			  {
					CString PrNetto;
					PrNetto.Format (_T("%.4lf"), AngebotCore->GetPrNetto ());
					CString PrBrutto;
					PrBrutto.Format (_T("%.2lf"), AngebotCore->GetPrBrutto ());

					FillList.SetItemText (PrNetto.GetBuffer (), EditRow, POSVK);
					FillList.SetItemText (PrBrutto.GetBuffer (), EditRow, POSVKBRUTTO);
					vat = AngebotCore->GetActVat ();
			  }
			  AngebotCore->SetA (A_bas->a_bas.a);
			  FillEinhCombo ();
			  ret = AngebotCore->GetEinh ();
			  if (ret)
				{
					CString Item;
					Item.Format (_T("%hd %s"), AngebotCore->GetMeEinhKun (),
											   AngebotCore->GetMeBz().GetBuffer ());
					p->me_einh = AngebotCore->GetMeEinh ();
					p->me_einh_kun = AngebotCore->GetMeEinhKun ();
					_tcscpy (p->lief_me_bz, AngebotCore->GetBasisMeBz ().Trim ().GetBuffer ());
					_tcscpy (p->auf_me_bz, AngebotCore->GetMeBz ().Trim ().GetBuffer ());
					p->auf_vk_euro = AngebotCore->GetPrNetto ();
					p->auf_vk_pr = AngebotCore->GetPrNetto ();
					p->auf_vk_brutto = AngebotCore->GetPrBrutto ();
					p->inh = AngebotCore->GetInh ();
					if (p->inh == 0.0)
					{
						p->inh = 1.0;
					}
					p->lief_me = p->auf_me * p->inh;
					p->mwst = vat;
					p->teil_smt = A_bas->a_bas.teil_smt;
					p->a_gew = A_bas->a_bas.a_gew;
					AngebotCore->FindEinhCombo (&Item, &EinhCombo);
					FillList.SetItemText (Item.GetBuffer (), 
						EditRow, POSEINH);
					FillList.SetItemText (p->lief_me_bz, 
						EditRow, POSBASEINH);
					GetList ();
			  }

//			  p->teil_smt = A_bas->a_bas.teil_smt;
//			  FillList.SetItemText (p->auf_me_bz, 
//					EditRow, POSEINH);
			  FillList.SetItemText (p->lief_me_bz, 
					EditRow, POSBASEINH);
//			  SetList ();
			  GetList ();
		  }
		  NoTestForAppend = FALSE;
		  EditRow = CurrentRow;
		  EditCol = POSME;
		  if (IsVatFull != AngebotCore->IsVatFull)
		  {
				RefreshPositions ();
		  }
		  AngebotCore->Calculate ();
		  StartEnter (EditCol, EditRow);
	  }
		                  
	  if (ChoiceA->FocusBack)
	  {
		ChoiceA->SetListFocus ();
	  }
}

void CPartyListCtrl::OnACanceled ()
{
	ChoiceA->ShowWindow (SW_HIDE);
}

void CPartyListCtrl::OnKey9 ()
{
	if (IsWindow (SearchListCtrl.Edit.m_hWnd))
	{
		OnChoice ();
	}
}

BOOL CPartyListCtrl::ReadABz1 ()
{
	int vat = 0;

	if (EditCol != POSA) return FALSE;
    memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
    CString Text;
	SearchListCtrl.Edit.GetWindowText (Text);
	if (!CStrFuncs::IsDecimal (Text))
	{
		OnAChoice (Text);
		if (ModalChoiceA) 
		{
			SearchListCtrl.Edit.GetWindowText (Text);
			Text.Format (_T("%.0lf"), CStrFuncs::StrToDouble (Text.GetBuffer ()));
			SearchListCtrl.Edit.SetWindowText (Text);
			if (!AChoiceStat)
			{
				EditCol --;
				StopEnter ();
				FillList.SetItemText ("0", EditRow, POSA);
				StartEnter (EditCol, EditRow);
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	A_bas->a_bas.a = CStrFuncs::StrToDouble (Text);
	if (A_bas->dbreadfirst () != 0)
	{
		MessageBox (_T("Artikel nicht gefunden"), NULL, MB_OK | MB_ICONERROR); 
		StopEnter ();
		FillList.SetItemText ("0", EditRow, POSA);
		StartEnter (EditCol, EditRow);
		return FALSE;
	}
	AngebotCore->GetABasList ()->Add (&A_bas->a_bas);
    CString ABz1;
	ABz1.Format (_T("%s"), A_bas->a_bas.a_bz1);
	FillList.SetItemText (ABz1.GetBuffer (), EditRow, POSABZ1);
	AngebotCore->SetA (A_bas->a_bas.a);
	AngebotCore->SetListRow (EditRow);
	BOOL IsVatFull = AngebotCore->IsVatFull;
	BOOL ret = AngebotCore->GetPrice ();
	if (ret)
	{
		CString PrNetto;
		PrNetto.Format (_T("%.4lf"), AngebotCore->GetPrNetto ());
		CString PrBrutto;
		PrBrutto.Format (_T("%.2lf"), AngebotCore->GetPrBrutto ());

		FillList.SetItemText (PrNetto.GetBuffer (), EditRow, POSVK);
		FillList.SetItemText (PrBrutto.GetBuffer (), EditRow, POSVKBRUTTO);
		vat = AngebotCore->GetActVat ();
	}
	AngebotCore->SetA (A_bas->a_bas.a);
    FillEinhCombo ();
	ret = AngebotCore->GetEinh ();
	if (ret)
	{
		CString Item;
		Item.Format (_T("%hd %s"), AngebotCore->GetMeEinhKun (),
			                       AngebotCore->GetMeBz().GetBuffer ());
		PARTYP *p = *List->Get (EditRow);
		if (p != NULL)
		{
			p->me_einh = AngebotCore->GetMeEinh ();
			p->me_einh_kun = AngebotCore->GetMeEinhKun ();
			_tcscpy (p->lief_me_bz, AngebotCore->GetBasisMeBz ().Trim ().GetBuffer ());
			_tcscpy (p->auf_me_bz, AngebotCore->GetMeBz ().Trim ().GetBuffer ());
			p->auf_vk_euro = AngebotCore->GetPrNetto ();
			p->auf_vk_pr = AngebotCore->GetPrNetto ();
			p->auf_vk_brutto = AngebotCore->GetPrBrutto ();
			p->inh = AngebotCore->GetInh ();
			if (p->inh == 0.0)
			{
				p->inh = 1.0;
			}
			p->mwst = vat;
			p->teil_smt = A_bas->a_bas.teil_smt;
			p->a_gew = A_bas->a_bas.a_gew;
		}
		AngebotCore->FindEinhCombo (&Item, &EinhCombo);
		FillList.SetItemText (Item.GetBuffer (), 
			EditRow, POSEINH);
		FillList.SetItemText (p->lief_me_bz, 
			EditRow, POSBASEINH);
		GetList ();
		if (IsVatFull != AngebotCore->IsVatFull)
		{
			RefreshPositions ();
		}
	}
	return TRUE;
}

void CPartyListCtrl::BeforeEinh (int row)
{
	CString A;
	GetColValue (row, POSA, A);
	A_bas->a_bas.a = CStrFuncs::StrToDouble (A);
	AngebotCore->SetA (A_bas->a_bas.a);
    FillEinhCombo ();
}

void CPartyListCtrl::RefreshPositions ()
{
	PARTYP **it;
	PARTYP *p;

	int i = 0;
	List->Start ();
	while ((it = List->GetNext ()) != NULL)
	{
		p = *it;
		CString Item;
		Item.Format (_T("%hd"), p->posi);
		FillList.SetItemText (Item.GetBuffer (), i, POSPOS);
		Item.Format (_T("%hd"), p->ls_pos_kz);
		FillList.SetItemText (Item.GetBuffer (), i, POSSTAT);
		Item.Format (_T("%.0lf"), p->a);
		FillList.SetItemText (Item.GetBuffer (), i, POSA);
		memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas->a_bas.a = p->a;
		A_bas->dbreadfirst ();
		FillList.SetItemText (A_bas->a_bas.a_bz1, i, POSABZ1);
		Item.Format (_T("%.3lf"), p->auf_me);
		FillList.SetItemText (Item.GetBuffer (), i, POSME);
		AngebotCore->SetA (p->a);
	    FillEinhCombo ();
		AngebotCore->SetMeEinhKun (p->me_einh_kun);
		if (AngebotCore->GetAktEinh ())
		{
			Item.Format (_T("%hd %s"), p->me_einh_kun,
									   AngebotCore->GetMeBz().GetBuffer ());
			AngebotCore->FindEinhCombo (&Item, &EinhCombo);
			FillList.SetItemText (Item.GetBuffer (), 
				i,  POSEINH);
		}
		Item.Format (_T("%.4lf"), p->auf_vk_euro);
		FillList.SetItemText (Item.GetBuffer (), i, POSVK);
		Item.Format (_T("%.2lf"), p->auf_vk_brutto);
		FillList.SetItemText (Item.GetBuffer (), i, POSVKBRUTTO);
		Item.Format (_T("%.2lf"), p->pos_vk);
		FillList.SetItemText (Item.GetBuffer (), i, POSVKME);
		i ++;
	}
}

void CPartyListCtrl::RefreshPrices ()
{
	PARTYP **it;
	PARTYP *p;

	int i = 0;
	List->Start ();
	while ((it = List->GetNext ()) != NULL)
	{
		p = *it;
		AngebotCore->SetA (p->a);
		AngebotCore->SetListRow (i);
		BOOL ret = AngebotCore->GetPrice ();
		if (ret)
		{
			CString PrNetto;
			PrNetto.Format (_T("%.4lf"), AngebotCore->GetPrNetto ());
			CString PrBrutto;
			PrBrutto.Format (_T("%.2lf"), AngebotCore->GetPrBrutto ());

			FillList.SetItemText (PrNetto.GetBuffer (), i, POSVK);
			FillList.SetItemText (PrBrutto.GetBuffer (), i, POSVKBRUTTO);
		}

		i ++;
	}
}

void CPartyListCtrl::FillEinhCombo ()
{
	AngebotCore->FillEinh (&EinhCombo);
}

void CPartyListCtrl::GetColValue (int row, int col, CString& Text)
{
    CString cText = GetItemText (row, col);
	Text = cText.Trim ();
}

/*
void CPartyListCtrl::TestIprIndex ()
{

	int Items = GetItemCount ();
	if (Items <= 1) return;
	CString Value;
	GetColValue (EditRow, PosA, Value);
	double rA = CStrFuncs::StrToDouble (Value);
	for (int i = 0; i < Items; i ++)
	{
		if (i == EditRow) continue;
		GetColValue (i, PosA, Value);
 	    double lA = CStrFuncs::StrToDouble (Value);
		if (lA != rA) continue;
		ListRows.Drop (EditRow);
 	    DeleteItem (i);
		InvalidateRect (NULL);
		if ( i < EditRow) EditRow --;
		return;
	}

}
*/

void CPartyListCtrl::ScrollPositions (int pos)
{
	*Position[pos] = -1;
	for (int i = pos + 1; Position[i] != NULL; i ++)
	{
		*Position[i] -= 1;
	}
}

void CPartyListCtrl::DestroyRows(CVector &Rows)
{
/*
	Rows.FirstPosition ();
	CPreise *ipr;
	while ((ipr = (CPreise *) Rows.GetNext ()) != NULL)
	{
		delete ipr;
	}
	Rows.Init ();
*/
}

void CPartyListCtrl::EnterGebinde()
{
	CGebindeDialog dlg;
	PARTYP *p ;
	PARTYP **it ;

	IsListDialog = TRUE;
	it = List->Get (EditRow);
	if (it != NULL)
	{
		p = *it;
		if (p != NULL)
		{
			CRect rect;
			GetSubItemRect (EditRow, POSGEBINDE, LVIR_BOUNDS , rect);
			ClientToScreen (&rect);
			dlg.SetText (p->gebinde_text);
			dlg.SetLength (36);
			dlg.SetRect (rect);
			if (dlg.DoModal () == IDOK)
			{
				_tcscpy (p->gebinde_text, dlg.GetText ().GetBuffer ());
				SetItemText (EditRow, POSGEBINDE, p->gebinde_text);
				EditCol = POSGEBINDE;
				switch (dlg.GetBreakKey ())
				{
				case dlg.For :
						OnReturn ();
						break;
				case dlg.Back :
					    PriorCol ();
						break;
				case dlg.Up :
					    PriorRow ();
						break;
				case dlg.Down :
					    NextRow ();
						break;
				}
			}
			else
			{
				OnReturn ();
			}
		}
	}
	IsListDialog = FALSE;
}

void CPartyListCtrl::OnPosText()
{
	PARTYP **it;
	PARTYP *p;
	CString Text;
	CPosTextDialog dlg;
	if (GetItemCount () == 0)
	{
		return;
	}

	it = List->Get (EditRow);
	if (it == NULL || *it == NULL)
	{
		return;
	}
	p = *it;
	if (p->aufp_txt == 0l)
	{
		p->aufp_txt = AngebotCore->GenAngPosTxtNr ();
		if (p->aufp_txt == 0l)
		{
			return;
		}
	}
	AngebotCore->ReadAngpt (p->aufp_txt, Text);
	dlg.SetTextNr (p->aufp_txt);
	dlg.SetText(Text);
	if (dlg.DoModal () == IDOK)
	{
		p->aufp_txt = dlg.GetTextNr ();
		Text = dlg.GetText ();
		AngebotCore->WriteAngpt (p->aufp_txt, Text);
	}
}

