#include "StdAfx.h"
#include "OkColorButton.h"
#include "Resource.h"

COkColorButton::COkColorButton() 
			   : CColorButton ()
{
	Initialized = FALSE;
	IsInizializing = FALSE;
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = CColorButton::DynColorGray;
	COLORREF ActiveColor = CColorButton::DynColorGray;

	nID = IDC_OKEX;
	Orientation = Center;
	TextStyle = Standard;
	BorderStyle = Solide;
	DynamicColor = TRUE;
	TextColor =RGB (0, 0, 0);
	SetBkColor (BkColor);
	DynColor = DynColor;
	RolloverColor = RolloverColor;
	LoadBitmap (IDB_OKEX);
}

COkColorButton::~COkColorButton(void)
{
}

void COkColorButton::Init() 
{
	if (!Initialized && !IsInizializing)
	{
		IsInizializing = TRUE;
		Initialized = TRUE;
		SetWindowText (_T("OK"));
		IsInizializing = FALSE;
	}
}

void COkColorButton::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	Init ();
	CColorButton::DrawItem (lpDrawItemStruct);
}
