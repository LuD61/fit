#include "stdafx.h"
#include "ChoiceTextpool.h"
#include "DbUniCode.h"
#include "DynDialog.h"
#include "DbQuery.h"
#include "Process.h"
#include "StrFuncs.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef _UNICODE
#define _tmemcmp wmemcmp
#define _tmemcpy wmemcpy
#else
#define _tmemcmp memcmp
#define _tmemcpy memcpy
#endif

int CChoiceTextpool::Sort1 = -1;
int CChoiceTextpool::Sort2 = -1;

CChoiceTextpool::CChoiceTextpool(CWnd* pParent) 
        : CChoiceX(pParent)
{
	HideEnter = FALSE;
	HideFilter = FALSE;
	m_Type = Comment;
}

CChoiceTextpool::~CChoiceTextpool() 
{
	DestroyList ();
}

void CChoiceTextpool::DestroyList() 
{
	for (std::vector<CTextpoolList *>::iterator pabl = TextpoolList.begin (); pabl != TextpoolList.end (); ++pabl)
	{
		CTextpoolList *abl = *pabl;
		delete abl;
	}
    TextpoolList.clear ();
}

BEGIN_MESSAGE_MAP(CChoiceTextpool, CChoiceX)
	//{{AFX_MSG_MAP(CChoiceTextpool)
//	ON_WM_MOVE ()
END_MESSAGE_MAP()

void CChoiceTextpool::FillList () 
{

	DestroyList ();
	CListCtrl *listView = GetListView ();
	ClearList ();

    DWORD Style = SetStyle (LVS_REPORT);
	SetExtendedStyle (LVS_EX_FULLROWSELECT);
//	SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    int i = 0;

    SetWindowText (_T("Texte"));
    SetCol (_T(""),      0, 0, LVCFMT_LEFT);
    SetCol (_T("Text-Nr"),   1,  80, LVCFMT_RIGHT);
    SetCol (_T("Text"),      2, 512, LVCFMT_RIGHT);

/*
	if (FirstRead && !HideFilter)
	{
		FirstRead = FALSE;
		Load ();
//		if (!IsModal)
		{
			PostMessage (WM_COMMAND, IDC_FILTER, 0l);
		}
		return;
	}
*/

	if (TextpoolList.size () == 0)
	{
		CString Sql = _T("");
		DbClass->sqlout ((long *)  &Textpool.textpool.nr,   SQLLONG, 0);
		DbClass->sqlout ((char *)  Textpool.textpool.text, 0, sizeof (Textpool.textpool.text));
		if (m_Type == All)
		{
			Sql = _T("select nr,  text ")
				  _T("from textpool where nr > 0"); 
		}
		else if (m_Type == this->Comment)
		{
			Sql = _T("select nr,  text ")
				  _T("from textpool where nr > 0 and typ in (1,2,3)"); 
		}
		else
		{
			DbClass->sqlin ((short *)  &m_Type, 1, 0);
			Sql = _T("select nr,  text ")
				  _T("from textpool where nr > 0 and typ = ?"); 
		}
		if (QueryString != _T(""))
		{
			Sql += _T(" and ");
			Sql += QueryString;
		}
		QueryString += _T("order by nr");

		int cursor = DbClass->sqlcursor (Sql.GetBuffer ());
		if (cursor == -1)
		{
			AfxMessageBox (_T("Die Eingabe f�r den Filter kann nicht bearbeitet werden"), MB_OK | MB_ICONERROR, 0);
			return;
		}

		DbClass->sqlopen (cursor);
		while (DbClass->sqlfetch (cursor) == 0)
		{
			LPSTR pos = (LPSTR) Textpool.textpool.text;
			CDbUniCode::DbToUniCode (Textpool.textpool.text, pos);
			CTextpoolList *abl = new CTextpoolList ();
			abl->nr = Textpool.textpool.nr;
			abl->Text = Textpool.textpool.text;
			TextpoolList.push_back (abl);
		}
		DbClass->sqlclose (cursor);
	}

	for (std::vector<CTextpoolList *>::iterator pabl = TextpoolList.begin (); pabl != TextpoolList.end (); ++pabl)
	{
		CTextpoolList *abl = *pabl;
        CString Nr;
        TCHAR *buffer = new TCHAR [abl->Text.GetLength () + 1];
		_tcscpy (buffer , abl->Text.GetBuffer ());
		for (int p = 0; buffer[p] != (TCHAR) 0; p ++)
		{
			if (buffer[p] == (TCHAR) 10) buffer[p] = _T(' ');
			if (buffer[p] == (TCHAR) 13) buffer[p] = _T(' ');
		}
        Nr.Format (_T("%ld") ,abl->nr);   

        int ret = InsertItem (i, -1);
		int pos = 0; 
        ret = SetItemText (Nr.GetBuffer (), i, ++pos);
		ret = SetItemText (buffer, i, ++pos);
		delete buffer;
        i ++;
    }

	SortRow = 1;
    Sort1 = -1;
    Sort2 = -1;
    Sort (listView);
//    FirstSelection ();
	GetDlgItem (IDC_SEARCH)->SetFocus ();
//	m_Idx = -1;
}


void CChoiceTextpool::SearchItem (CListCtrl *ListBox, LPTSTR Search)
{
    int count = ListBox->GetItemCount ();

	int i = 0;
    for (i = 0; i < count; i ++)
    {
        CString iText = ListBox->GetItemText (i, 2);
        iText.MakeUpper ();
		CString cSearch = Search;
		cSearch.MakeUpper ();

        if (_tmemcmp (cSearch.GetBuffer (), iText.GetBuffer (), _tcslen (Search)) == 0)
        {
            break;
        }
    }
    if (i < count)
    {
        ScrolltoIdx (ListBox,i);
    }
}

void CChoiceTextpool::Search ()
{
    CString EditText;

    CEdit *Search = (CEdit *) GetDlgItem (IDC_SEARCH);
    if (Search == NULL)
    {
        return;
    }
    CListCtrl *ListBox = (CListCtrl *) GetDlgItem (IDC_CHOICE);
    if (ListBox == NULL)
    {
        return;
    }

    Search->GetWindowText (EditText);
    EditText.MakeUpper ();
    SearchItem (ListBox, EditText.GetBuffer ());
}

int CChoiceTextpool::GetPtBez (LPTSTR item, LPTSTR wert, LPTSTR bez)
{
	TCHAR ptitem [9];
	TCHAR ptwert [5];
	TCHAR ptbez [37];
	static int cursor = -1;

	_tcscpy (ptbez, _T(""));
	if (cursor == -1)
	{
		DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
		DbClass->sqlin ((LPTSTR) ptitem, SQLCHAR, 9);
		DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
		cursor = DbClass->sqlcursor (_T("select ptbez from ptabn where ptitem = ? ")
								_T("and ptwert = ?"));
	}
	strcpy (ptitem, item);
	strcpy (ptwert, wert);
	DbClass->sqlopen (cursor);
	int dsqlstatus = DbClass->sqlfetch (cursor);
	strcpy (bez, ptbez);
    return dsqlstatus;
}

int CChoiceTextpool::GetPtBez (LPTSTR item, int wert, LPTSTR bez)
{
	TCHAR ptitem [9];
	TCHAR ptwert [5];
	TCHAR ptbez [37];
	static int cursor = -1;

	_tcscpy (ptbez, _T(""));
	if (cursor == -1)
	{
		DbClass->sqlout ((LPTSTR) ptbez, SQLCHAR, 37);
		DbClass->sqlin ((LPTSTR) ptitem, SQLCHAR, 9);
		DbClass->sqlin  ((LPTSTR) ptwert, SQLCHAR, 4);
		cursor = DbClass->sqlcursor (_T("select ptbez from ptabn where ptitem = ? ")
								_T("and ptwert = ?"));
	}
	strcpy (ptitem, item);
	sprintf (ptwert,"%d", wert);
	DbClass->sqlopen (cursor);
	int dsqlstatus = DbClass->sqlfetch (cursor);
	strcpy (bez, ptbez);
    return dsqlstatus;
}

int CALLBACK CChoiceTextpool::CompareProc(LPARAM lParam1, 
						 		     LPARAM lParam2, 
									 LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.

   int SortRow = CmpRow;
   int ret = 0;
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText((int) lParam1, SortRow);
   CString    strItem2 = pListCtrl->GetItemText((int) lParam2, SortRow);

   ret = (strItem2.CompareNoCase (strItem1));
   switch (SortRow)
   {
	   case 0:
		   ret *= Sort1;
		   break;
	   case 1:
		   ret *= Sort2;
		   break;
   }
   return ret;
}


void CChoiceTextpool::Sort (CListCtrl *ListBox)
{
    CmpRow = SortRow;
    ListBox->SortItems(CompareProc, (LPARAM) ListBox);
	int SortPos = 0;
    switch (SortRow)
    {
        case 0 :
              Sort1 *= -1;
              break;
        case 1:
              Sort2 *= -1;
			  if (Sort2 < 0) SortPos = 1;
              break;
    }
    for (int i = 0; i < ListBox->GetItemCount (); i ++)
    {
           ListBox->SetItemData (i, (LPARAM) i);
           CTextpoolList *abl = TextpoolList [i];
		   
		   abl->nr         = _tstol (ListBox->GetItemText (i, 1));
		   abl->Text       = ListBox->GetItemText (i, 2);
	}
	for (int i = 1; i <= 2; i ++)
	{
		SetItemSort (i, 2);
	}
	SetItemSort (SortRow, SortPos);
}

void CChoiceTextpool::SetSelText (CListCtrl *ListBox, int idx)
{
    CString Text = ListBox->GetItemText (idx, 1);
    _tcscpy (SelText, Text.GetBuffer ());
	SelectedRow = TextpoolList [idx];
}

CTextpoolList *CChoiceTextpool::GetSelectedText ()
{
	CTextpoolList *abl = (CTextpoolList *) SelectedRow;
	return abl;
}


void CChoiceTextpool::OnEnter ()
{
}

void CChoiceTextpool::OnFilter ()
{
	CDynDialog dlg;
	CDynDialogItem *Item;
	static int headercy = 200;

	dlg.DlgCaption = "Filter";
	dlg.UseFilter = UseFilter;

	if (vFilter.size () > 0)
	{
		for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
		{
			CDynDialogItem *i = *it;
			Item = new CDynDialogItem ();
			*Item = *i;
			if (Item->Name == _T("reset"))
			{
				if (UseFilter)
				{
					Item->Value = _T("aktiv");
				}
				else
				{
					Item->Value = _T("nicht aktiv");
				}
			}
			dlg.AddItem (Item);

		}
		dlg.Header.cy = headercy;	
	}
	else
	{
		int y = 10;
		Item = new CDynDialogItem ();
		Item->Name = _T("lnr");
		Item->CtrClass = "static";
		Item->Value = _T("Text-Nr");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1001;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("nr");
		Item->Type = Item->Long;
		Item->CtrClass = "edit";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1002;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		Item = new CDynDialogItem ();
		Item->Name = _T("ltyp");
		Item->CtrClass = "static";
		Item->Value = _T("Typ");
		Item->Template.x = 10;
		Item->Template.y = y;
		Item->Template.cx = 60;
		Item->Template.cy = 10;
		Item->Template.style = WS_CHILD | WS_VISIBLE;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1003;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("typ");
		Item->Type = Item->Short;
		Item->CtrClass = "combobox";
		Item->Value = _T("");
		Item->Template.x = 70;
		Item->Template.y = y;
		Item->Template.cx = 120;
		Item->Template.cy = 100;
		Item->ComboBoxValues.push_back (new CString (_T("")));
		Item->ComboBoxValues.push_back (new CString (_T("1 Texte f�r Angebote")));
		Item->ComboBoxValues.push_back (new CString (_T("2 Texte f�r Rechnungen")));
		Item->ComboBoxValues.push_back (new CString (_T("3 interne Texte")));
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP | 
			CBS_DROPDOWNLIST | WS_VSCROLL;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = 1004;
		dlg.AddItem (Item);

		y += dlg.RowSpace;

		dlg.Header.cy = y + 30;
		headercy = dlg.Header.cy;

		Item = new CDynDialogItem ();
		Item->Name = _T("OK");
		Item->CtrClass = "button";
		Item->Value = _T("OK");
		Item->Template.x = 15;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDOK;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("cancel");
		Item->CtrClass = "button";
		Item->Value = _T("abbrechen");
		Item->Template.x = 70;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = IDCANCEL;
		dlg.AddItem (Item);

		Item = new CDynDialogItem ();
		Item->Name = _T("reset");
		Item->CtrClass = "button";
		if (UseFilter)
		{
			Item->Value = _T("aktiv");
		}
		else
		{
			Item->Value = _T("nicht aktiv");
		}
		Item->Template.x = 125;
		Item->Template.y = dlg.Header.cy - 15;
		Item->Template.cx = 50;
		Item->Template.cy = 11;
		Item->Template.style = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		Item->Template.dwExtendedStyle = 0;
		Item->Template.id = ID_USE;
		dlg.AddItem (Item);
	}

	dlg.CreateModal ();
	INT_PTR ret = dlg.DoModal ();
	if (ret != IDOK) return;
 
	UseFilter = dlg.UseFilter;
	int count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = dlg.Items.begin (); it != dlg.Items.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (count >= (int) vFilter.size ())
			{
				Item = new CDynDialogItem ();
				*Item = *i;
				vFilter.push_back (Item);
			}
			*vFilter[count] = *i;
			count ++;
	}
    CString Query;
    CDbQuery DbQuery;

	QueryString = _T("");
	if (!UseFilter)
	{
		FillList ();
		return;
	}
	count = 0;
	for (std::vector<CDynDialogItem *>::iterator it = vFilter.begin (); it != vFilter.end ();
			                                ++it)
	{
			CDynDialogItem *i = *it;
			if (strcmp (i->CtrClass, "edit") != 0 &&
				strcmp (i->CtrClass, "combobox") != 0) 
			{
				continue; 
			}
			if (i->Value.Trim () != _T(""))
			{
				if (i->Type == i->String || i->Type == i->Date)
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, TRUE);
				}
				else
				{
					Query = DbQuery.ForColumn (i->Value, i->Name, FALSE);
				}
				if (count > 0)
				{
					QueryString += _T(" and ");
				}
                QueryString += Query;
				count ++;
			}
	}
	FillList ();
}

void CChoiceTextpool::OnMove (int x, int y)
{
//	WINDOWHANDLER->IsDockPlace (x, y);

}
