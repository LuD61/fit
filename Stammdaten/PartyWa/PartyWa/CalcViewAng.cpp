#include "StdAfx.h"
#include "CalcViewAng.h"

//IMPLEMENT_DYNCREATE(CCalcViewAng, CCalcView)
IMPLEMENT_DYNCREATE(CCalcViewAng, CFormView)

CCalcViewAng::CCalcViewAng(void) :
			  CCalcView ()	
{
}

CCalcViewAng::~CCalcViewAng(void)
{
}

void CCalcViewAng::RegisterCalcForm ()
{
	AngebotCore->SetCalcForm (&Form);
	AngebotCore->SetCalcFormAng (&Form);
}
