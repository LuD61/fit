#include "StdAfx.h"
#include "Mail.h"

CMail::CMail(void)
{
	m_Error = NoError;
	Init ();
	m_Dialog = TRUE;
    m_Library = ::LoadLibraryA("MAPI32.DLL");
	if (m_Library != NULL)
	{
  	    (FARPROC&)m_lpfnSendMail = GetProcAddress(m_Library, "MAPISendMail");
	}
}

CMail::~CMail(void)
{
	::FreeLibrary (m_Library);
}

void CMail::Init ()
{
	m_From = _T("");
	m_To = _T("");
	m_CC = _T("");
	m_Subject = _T("");
	m_NoteText = _T("");
}

void CMail::AddFile (CString *FileName)
{
	m_Files.Add (FileName);
}

BOOL CMail::Send ()
{
	BOOL ret = TRUE;
	CDataCollection<CString *> BaseNames;
	MapiFileDesc *fileDesc = NULL;

	m_Error = NoError;
	if (m_Library == NULL)
	{
		m_Error = NoLibrary;
		ret = FALSE;

	}
	if (ret && m_lpfnSendMail == NULL)
	{
		m_Error = NoMailProc;
		ret = FALSE;
	}

	if (ret)
	{
		if (m_Files.anz > 0)
		{
			fileDesc = new MapiFileDesc [m_Files.anz];
			m_Files.Start ();
			CString **it;
			CString *File;
			int i = 0;
			while ((it = m_Files.GetNext ()) != NULL)
			{
				File = *it;
				if (File != NULL)
				{
					memset(&fileDesc[i], 0, sizeof(MapiFileDesc));
					fileDesc[i].nPosition = (ULONG)-1;
					fileDesc[i].lpszPathName = File->GetBuffer ();
					CString *Base = new CString ();
					*Base = BaseName (*File);
					BaseNames.Add (Base);
					fileDesc[i].lpszFileName = Base->GetBuffer ();
				}
				i ++;
			}
		}
        MapiRecipDesc Originator;

      	memset(&Originator, 0, sizeof(Originator));
        Originator.ulRecipClass = MAPI_ORIG;

		if (m_From.GetLength () != 0)
        {
               Originator.lpszName = m_From.GetBuffer ();
        }

        Originator.ulEIDSize = 0;
        Originator.lpEntryID = "";

        MapiRecipDesc Recips[1];
      	memset(&Recips[0], 0, sizeof(Recips));
        Recips[0].ulRecipClass = MAPI_TO;


		if (m_To.GetLength () != 0)
        {
			   m_To.Trim ();
               Recips[0].lpszName = m_To.GetBuffer ();
        }
/*
		if (m_To.GetLength () != 0)
        {
			   m_To.Trim ();
               CString Adr = "SMPT:";
               Adr += m_To.GetBuffer ();
               Recips[0].lpszAddress = Adr.GetBuffer (); //z. B. SMPT:Wilhelm.Roth@kabelbw.de
        }
*/

        Recips[0].ulEIDSize = 0;
        Recips[0].lpEntryID = "";

  	    MapiMessage message;
       
        memset(&message, 0, sizeof(message));
        message.flFlags = MAPI_SENT;

		if (m_Subject.GetLength () != 0)
        {
            message.lpszSubject = m_Subject.GetBuffer ();
        }
        if (m_NoteText.GetLength () != 0)
        {
            message.lpszNoteText = m_NoteText.GetBuffer (0);
        }

        message.lpOriginator = &Originator;
		if (m_To.GetLength () != 0)
        {
			message.nRecipCount = 1;
			message.lpRecips = Recips;
		}

		if (m_Files.anz > 0)
		{
			message.nFileCount   = m_Files.anz;
			message.lpFiles = fileDesc;
		}
        int nError = 0; 
        if (m_Dialog)
        {
                nError = m_lpfnSendMail(0, NULL,
		                                  &message, MAPI_LOGON_UI|MAPI_DIALOG, 0);
        }
        else
        {
                nError = m_lpfnSendMail(0, NULL,
		                           &message, 0, 0);

                if (nError == MAPI_E_LOGON_FAILURE)
                {
                        nError = m_lpfnSendMail(0, NULL,
		                                   &message, MAPI_LOGON_UI, 0);
                }
        }
		if (nError)
		{
			m_Error = SendError;
			ret = FALSE;
		}
	}
	m_Files.DestroyElements ();
	m_Files.Destroy ();
	BaseNames.DestroyElements ();
	BaseNames.Destroy ();
	return ret;
}

CString& CMail::BaseName (CString& Path)
{
	CString sep;
	static CString Base;

	Base = Path;
	int idx = Path.Find ("\\", 0);
	if (idx == -1)
	{
	     int idx = Path.Find ("/", 0);
		 if (idx != -1) sep = "/";
	}
	else
	{
		sep = "\\";
	}
	if (idx == -1)
	{
		return Base;
	}
	int pos = 0;
	CString C = Path.Tokenize (sep.GetBuffer (0), pos);
	while (C != "")
	{
		Base = C;
	    C = Path.Tokenize (sep.GetBuffer (0), pos);
	}
    return Base;
}
