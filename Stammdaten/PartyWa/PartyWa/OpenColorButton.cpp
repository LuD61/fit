#include "StdAfx.h"
#include "OpenColorButton.h"

COpenColorButton::COpenColorButton(void)
{
	Initialized = FALSE;
	IsInizializing = FALSE;
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = CColorButton::DynColorGray;
	COLORREF ActiveColor = CColorButton::DynColorGray;

	m_Text = _T("�ffnen");
	nID = IDC_OPEN;
	Orientation = Center;
//	xSpace = 2;
	TextStyle = Standard;
	BorderStyle = Solide;
//	BorderStyle = NoBorder;
	DynamicColor = TRUE;
	TextColor =RGB (0, 0, 0);
	SetBkColor (BkColor);
	DynColor = DynColor;
	RolloverColor = RolloverColor;
//	Transparent = TRUE;
	LoadBitmap (IDB_OPEN);
}

COpenColorButton::~COpenColorButton(void)
{
}

void COpenColorButton::Init() 
{
	if (!Initialized && !IsInizializing)
	{
		IsInizializing = TRUE;
		Initialized = TRUE;
		SetWindowText (m_Text);
		IsInizializing = FALSE;
	}
}

void COpenColorButton::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	Init ();
	CColorButton::DrawItem (lpDrawItemStruct);
}
