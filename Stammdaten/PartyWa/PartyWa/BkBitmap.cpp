#include "StdAfx.h"
#include "BkBitmap.h"

CBkBitmap::CBkBitmap(void)
{
	parts = 120;
	m_BkType = DynamicMetal;
	m_Direction = Horizontal;
}

CBkBitmap::~CBkBitmap(void)
{
}

CBitmap*  CBkBitmap::Create ()
{
	CDC cDC;
	CDC memDC;
	CBitmap b;
	CRect rect;

	HDC hdc = GetDC (NULL);
	cDC.Attach (hdc);
	b.CreateBitmap (m_Width, m_Height, m_Planes, 0, NULL);
	m_Bitmap.CreateCompatibleBitmap (&cDC, m_Width, m_Height);
	memDC.CreateCompatibleDC (&cDC);
	memDC.SelectObject (&m_Bitmap);

	rect.left = 0;
	rect.top = 0;
	rect.right = m_Width;
	rect.bottom = m_Height;

	if (m_BkType == this->DynamicMetal)
	{
		FillRectParts (memDC, RGB (223, 227, 236), rect);
	}
	else if (m_BkType == this->DynamicBlue)
	{
		FillRectParts (memDC, RGB (100, 180, 255), rect);
	}
	else  if (m_BkType == this->DynamicDarkBlue)
	{
		FillRectParts (memDC, RGB (0, 100, 255), rect);
	}
	else  if (m_BkType == this->DynamicGrey)
	{
		FillRectParts (memDC, RGB (150, 150, 150), rect);
	}
	else
	{
		FillRectParts (memDC, RGB (223, 227, 236), rect);
	}

//	memDC.FillSolidRect (0, 0, m_Width, m_Height, RGB (0, 0, 255));
	return &m_Bitmap;
}

void CBkBitmap::FillVRectParts (CDC &cDC, COLORREF BkColor, CRect rect)
{
	int blue = (BkColor >> 16) & 0xFF;
	int green = (BkColor >> 8) & 0xFF;
	int red = (BkColor) & 0xFF;
	int height = rect.bottom - rect.top;
    int parts = 10;
	if (height > 50)
	{
		parts = 70;
	}
	else
	{
	    parts = 15;
	}
	int colordiff = 5;
	int part = (rect.bottom) / parts;
	CRect rect1 (rect.left, rect.top, rect.right, part); 
	int redplus = 240 - red;
	int greenplus = 240 - green;
	int blueplus = 240 - blue;
	int redstep = redplus / parts;
	int greenstep = greenplus / parts;
	int bluestep = blueplus / parts;
	red = min (red + redplus, 240);
	green = min (green + greenplus, 240);
	blue = min (blue + blueplus, 240);
	if (part == 1) 
	{
		part = 2;
	}

	for (int i = 0; i < parts; i ++)
	{
		cDC.FillRect (&rect1, &CBrush (RGB (red, green, blue)));
		rect1.top += part - 1;
		rect1.bottom += part;
		if (rect1.top >= rect.bottom - part) return;
		red -= redstep;
		green -= greenstep;
		blue -= bluestep;
	}
	rect1.bottom = rect.bottom; 
	cDC.FillRect (&rect1, &CBrush (RGB (red, green, blue)));
}

void CBkBitmap::FillRectParts (CDC &cDC, COLORREF BkColor, CRect rect)
{
	if (m_Direction == Vertical)
	{
		FillVRectParts (cDC, BkColor, rect);
		return;
	}
	int blue = (BkColor >> 16) & 0xFF;
	int green = (BkColor >> 8) & 0xFF;
	int red = (BkColor) & 0xFF;
	int part = (rect.right) / parts;
	if (part < 1)
	{
		cDC.FillSolidRect (&rect, RGB (red, green, blue));
		return;
	}

	int colordiff = 2;
	part ++;

	CRect rect1 (rect.left, rect.top, part, rect.bottom); 

	for (int i = 0; i < parts / 3; i ++)
	{
		cDC.FillSolidRect (&rect1, RGB (red, green, blue));
		rect1.left += part - 1;
		rect1.right += part;
		if (red <= 255 - colordiff) red += colordiff;

		cDC.FillSolidRect (&rect1, RGB (red, green, blue));

		rect1.left += part - 1;
		rect1.right += part;
		if (green <= 255 - colordiff) green += colordiff;
		cDC.FillSolidRect (&rect1, RGB (red, green, blue));

		rect1.left += part - 1;
		rect1.right += part;
		if (blue <= 255 - colordiff) blue += colordiff;
	}
	rect1.left += part - 1;
	rect1.right = rect.right; 
	cDC.FillSolidRect (&rect1, RGB (red, green, blue));

}
