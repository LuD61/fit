#include "StdAfx.h"
#include "AuftragPage.h"
#include "CtrlLine.h"
#include "StrFuncs.h"
#include "NewAngDialog.h"
#include "PartyWaDoc.h"
#include "AdressDialog.h"
#include "PosTextDialog.h"
#include "CommentDialog.h"
#include "PersDialog.h"
#include "PrintMail.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#define BLUE_COL 1

IMPLEMENT_DYNAMIC(CAuftragPage, CDialog)

CAuftragPage::CAuftragPage(CWnd* pParent /*=NULL*/)
	: CDialog(CAuftragPage::IDD, pParent)
{
	DlgBkColor = GetSysColor (COLOR_3DFACE);
	DlgBkColor = RGB (168, 193, 218);
	DlgBkColor = RGB (235, 235, 245);
	m_NewAngTyp.SetBkColor (DlgBkColor);
	DlgBrush = NULL;
	ChoiceMdn = NULL;
	ChoiceFil = NULL;
	ChoicePartyk = NULL;
	QueryPartyk = _T("");
	FirstReadPartyk = TRUE;
	ChoiceKun = NULL;
	MustCreate = FALSE;
	IsCreated = FALSE;
	TestKun = FALSE;
	ExitMessage = NULL;
	BackMessage = NULL;
	DataBackMessage = NULL;
	SaveMessage = NULL;
	DeleteMessage = NULL;
	InsertMessage = NULL;
	CopyMessage = NULL;
	PasteMessage = NULL;
	DeleteAngMessage = NULL;
	PrintAuftragMessage = NULL;
	PrintLiefMessage = NULL;
	PrintRechMessage = NULL;
	CalcViewMessage = NULL;
	PosTextMessage = NULL;
	KopfTextMessage = NULL;
	FussTextMessage = NULL;
	CalcFrame = NULL;
	MustCalcView = FALSE;
	CalcWidth = 610;
	CalcHeight = 342;
	CalcCaption = FALSE;
}

CAuftragPage::~CAuftragPage(void)
{
	Font.DeleteObject ();
	Form.FirstPosition ();
	CFormField *f;
	while ((f = (CFormField *) Form.GetNext ()) != NULL)
	{
		delete f;
	}
	if (ExitMessage != NULL)
	{
		MessageHandler->Unregister (ExitMessage);
		delete ExitMessage;
	}
	if (BackMessage != NULL)
	{
		MessageHandler->Unregister (BackMessage);
		delete BackMessage;
	}
	if (DataBackMessage != NULL)
	{
		MessageHandler->Unregister (DataBackMessage);
		delete DataBackMessage;
	}
	if (SaveMessage != NULL)
	{
		MessageHandler->Unregister (SaveMessage);
		delete SaveMessage;
	}
	if (DeleteMessage != NULL)
	{
		MessageHandler->Unregister (DeleteMessage);
		delete DeleteMessage;
	}
	if (InsertMessage != NULL)
	{
		MessageHandler->Unregister (InsertMessage);
		delete InsertMessage;
	}
	if (CopyMessage != NULL)
	{
		MessageHandler->Unregister (CopyMessage);
		delete CopyMessage;
	}
	if (PasteMessage != NULL)
	{
		MessageHandler->Unregister (PasteMessage);
		delete PasteMessage;
	}
	if (DeleteAngMessage != NULL)
	{
		MessageHandler->Unregister (DeleteAngMessage);
		delete DeleteAngMessage;
	}
	if (PrintAuftragMessage != NULL)
	{
		MessageHandler->Unregister (PrintAuftragMessage);
		delete PrintAuftragMessage;
	}
	if (PrintLiefMessage != NULL)
	{
		MessageHandler->Unregister (PrintLiefMessage);
		delete PrintLiefMessage;
	}
	if (PrintRechMessage != NULL)
	{
		MessageHandler->Unregister (PrintRechMessage);
		delete PrintRechMessage;
	}

	if (CalcViewMessage != NULL)
	{
		MessageHandler->Unregister (CalcViewMessage);
		delete CalcViewMessage;
	}

	if (PosTextMessage != NULL)
	{
		MessageHandler->Unregister (PosTextMessage);
		delete PosTextMessage;
	}

	if (KopfTextMessage != NULL)
	{
		MessageHandler->Unregister (KopfTextMessage);
		delete KopfTextMessage;
	}

	if (FussTextMessage != NULL)
	{
		MessageHandler->Unregister (FussTextMessage);
		delete FussTextMessage;
	}

	if (ChoiceKun != NULL)
	{
		delete ChoiceKun;
	}
}

void CAuftragPage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LMDN, m_LMdn);
	DDX_Control(pDX, IDC_MDN, m_Mdn);
	DDX_Control(pDX, IDC_MDN_NAME, m_MdnName);
	DDX_Control(pDX, IDC_LFIL, m_LFil);
	DDX_Control(pDX, IDC_FIL, m_Fil);
	DDX_Control(pDX, IDC_FIL_NAME, m_FilName);
	DDX_Control(pDX, IDC_LANGEBOT_NR, m_LAngebotNr);
	DDX_Control(pDX, IDC_ANGEBOT_NR, m_AngebotNr);
	DDX_Control(pDX, IDC_LAUF, m_LAuf);
	DDX_Control(pDX, IDC_AUF, m_Auf);
	DDX_Control(pDX, IDC_LLS, m_LLs);
	DDX_Control(pDX, IDC_LS, m_Ls);
	DDX_Control(pDX, IDC_BKUN, m_BKun);
	DDX_Control(pDX, IDC_BFIL, m_BFil);

	DDX_Control(pDX, IDC_LKUN, m_LKun);
	DDX_Control(pDX, IDC_KUN, m_Kun);
	DDX_Control(pDX, IDC_KUN_NAME, m_KunName);
	DDX_Control(pDX, IDC_KUN_NAME2, m_KunName2);
	DDX_Control(pDX, IDC_LTEL, m_LTel);
	DDX_Control(pDX, IDC_TEL, m_Tel);
	DDX_Control(pDX, IDC_TEL2, m_Tel2);
	DDX_Control(pDX, IDC_LANG_TYP, m_LAngTyp);
	DDX_Control(pDX, IDC_ANG_TYP, m_AngTyp);
	DDX_Control(pDX, IDC_LPERS, m_LPers);
	DDX_Control(pDX, IDC_PERS, m_Pers);
	DDX_Control(pDX, IDC_ADDRESS, m_Address);
	DDX_Control(pDX, IDC_LIEF_ADR, m_LiefAddress);
	DDX_Control(pDX, IDC_COMMENTS, m_Comments);
	DDX_Control(pDX, IDC_MODIFY_PERS, m_ModifyPers);
	DDX_Control(pDX, IDC_NEW_ANG_TYP, m_NewAngTyp);
	DDX_Control(pDX, IDC_LANZ_PERS, m_LAnzPers);
	DDX_Control(pDX, IDC_ANZ_PERS, m_AnzPers);
	DDX_Control(pDX, IDC_CALC_POS, m_CalcPos);
	DDX_Control(pDX, IDC_EINZEL_PREIS, m_EinzelPreis);
	DDX_Control(pDX, IDC_HIDE_ME, m_HideMe);
	DDX_Control(pDX, IDC_HIDE_SERVICE, m_HideService);
	DDX_Control(pDX, IDC_NUR_SERVICE_PREIS, m_NurServicePreis);
	DDX_Control(pDX, IDC_MWST_VOLL, m_MwstVoll);
	DDX_Control(pDX, IDC_ABHOLUNG, m_Abholung);
	DDX_Control(pDX, IDC_LANG_GEW, m_LAngGew);
	DDX_Control(pDX, IDC_ANG_GEW, m_AngGew);
	DDX_Control(pDX, IDC_LANG_VK, m_LAngVk);
	DDX_Control(pDX, IDC_ANG_VK, m_AngVk);
	DDX_Control(pDX, IDC_LPAUSCHAL_VK, m_LPauschalVk);
	DDX_Control(pDX, IDC_PAUSCHAL_VK, m_PauschalVk);
	DDX_Control(pDX, IDC_LEINZEL_VK, m_LEinzelVk);
	DDX_Control(pDX, IDC_EINZEL_VK, m_EinzelVk);
	DDX_Control(pDX, IDC_LEINZEL_VK_PAUSCH, m_LEinzelVkPausch);
	DDX_Control(pDX, IDC_EINZEL_VK_PAUSCH, m_EinzelVkPausch);
	DDX_Control(pDX, IDC_LSERVICE_VK_PAUSCH, m_LServiceVkPausch);
	DDX_Control(pDX, IDC_SERVICE_VK_PAUSCH, m_ServiceVkPausch);
	DDX_Control(pDX, IDC_LHINWEIS, m_LHinweis);
	DDX_Control(pDX, IDC_HINWEIS, m_Hinweis);
	DDX_Control(pDX, IDC_PREIS_LABEL, m_PreisLabel);
	DDX_Control(pDX, IDC_CALC_LABEL, m_CalcLabel);
	DDX_Control(pDX, IDC_PAUSCH_LABEL, m_PauschLabel);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Control(pDX, IDC_LLIEFERDAT, m_LLieferdat);
	DDX_Control(pDX, IDC_LIEFERDAT, m_Lieferdat);
	DDX_Control(pDX, IDC_LLIEFERZEIT, m_LLieferzeit);
	DDX_Control(pDX, IDC_LIEFERZEIT, m_Lieferzeit);
}

BEGIN_MESSAGE_MAP(CAuftragPage, CDialog)
	ON_WM_SIZE ()
	ON_WM_CTLCOLOR ()
	ON_BN_CLICKED(IDC_MDNCHOICE , OnMdnChoice)
	ON_BN_CLICKED(IDC_FILCHOICE , OnFilChoice)
	ON_BN_CLICKED(IDC_ANGCHOICE , OnPartykChoice)
	ON_BN_CLICKED(IDC_KUNCHOICE , OnKunChoice)
	ON_COMMAND (KUN_SELECTED, OnKunSelected)
	ON_COMMAND (KUN_CANCELED, OnKunCanceled)
	ON_EN_SETFOCUS(IDC_ANGEBOT_NR, &CAuftragPage::OnEnSetfocusAngebotNr)
	ON_BN_CLICKED(IDC_BKUN, &CAuftragPage::OnBnClickedBkun)
	ON_BN_CLICKED(IDC_BFIL, &CAuftragPage::OnBnClickedBfil)
	ON_EN_KILLFOCUS(IDC_PAUSCHAL_VK, &CAuftragPage::OnEnKillfocusPauschalVk)
	ON_EN_KILLFOCUS(IDC_SERVICE_VK_PAUSCH, &CAuftragPage::OnEnKillfocusServicePauschVk)
	ON_EN_KILLFOCUS(IDC_ANZ_PERS, &CAuftragPage::OnEnKillfocusAnzPers)
	ON_BN_CLICKED(IDC_MWST_VOLL, &CAuftragPage::OnBnClickedMwstVoll)
//	ON_EN_KILLFOCUS(IDC_KUN, &CAuftragPage::OnEnKillfocusKun)
    ON_EN_KILLFOCUS(IDC_EINZEL_VK_PAUSCH, &CAuftragPage::OnEnKillfocusEinzelVkPausch)
	ON_BN_CLICKED(IDC_EINZEL_PREIS, &CAuftragPage::OnBnClickedEinzelPreis)
	ON_STN_CLICKED(IDC_NEW_ANG_TYP, &CAuftragPage::OnStnClickedNewAngTyp)
	ON_STN_CLICKED(IDC_ADDRESS, &CAuftragPage::OnStnClickedAddress)
	ON_STN_CLICKED(IDC_LIEF_ADR, &CAuftragPage::OnStnClickedLiefAdr)
	ON_STN_CLICKED(IDC_CALC_POS, &CAuftragPage::OnCalcPos)
	ON_STN_CLICKED(IDC_COMMENTS, &CAuftragPage::OnComments)
	ON_STN_CLICKED(IDC_MODIFY_PERS, &CAuftragPage::OnModifyPers)
	ON_EN_SETFOCUS(IDC_ANZ_PERS, &CAuftragPage::OnEnSetfocusAnzPers)
	ON_BN_CLICKED(IDC_NUR_SERVICE_PREIS, &CAuftragPage::OnBnClickedNurServicePreis)
END_MESSAGE_MAP()

BOOL CAuftragPage::OnInitDialog()
{
	CDialog::OnInitDialog();


#ifdef BLUE_COL
	COLORREF BkColor (RGB (100, 180, 255));
	COLORREF DynColor = m_NewAngTyp.DynColorBlue;
	COLORREF RolloverColor = RGB (204, 204, 255);
#else
	COLORREF BkColor (CColorButton::DynColorGray);
	COLORREF DynColor = CColorButton::DynColorGray;
	COLORREF RolloverColor = RGB (192, 192, 192);
#endif

	m_KunName.ShowWindow (SW_HIDE);
	m_Tel.ShowWindow (SW_HIDE);

	m_Hinweis.SetLimitText (512);

	KunDiverse = CKunDiverse::GetInstance ();

	MessageHandler = CMessageHandler::GetInstance ();
// Handler f�r beenden
	ExitMessage = new CExitMessage (this);
	MessageHandler->Register (ExitMessage);

// Handler f�r abbrechen
	BackMessage = new CBackMessage (this);
	MessageHandler->Register (BackMessage);
	DataBackMessage = new CDataBackMessage (this);
	MessageHandler->Register (DataBackMessage);

// Handler f�r speichern
	SaveMessage = new CSaveMessage (this);
	MessageHandler->Register (SaveMessage);

// Handler f�r Zeile l�schen
	DeleteMessage = new CDeleteMessage (this);
	MessageHandler->Register (DeleteMessage);

// Handler f�r Zeile einf�gen
	InsertMessage = new CInsertMessage (this);
	MessageHandler->Register (InsertMessage);

// Handler f�r Zeile copy
	CopyMessage = new CCopyMessage (this);
	MessageHandler->Register (CopyMessage);

// Handler f�r Zeile paste
	PasteMessage = new CPasteMessage (this);
	MessageHandler->Register (PasteMessage);

// Handler f�r Angebot komplett l�xchen
	DeleteAngMessage = new CDeleteAngMessage (this);
	MessageHandler->Register (DeleteAngMessage);

// Handler f�r Auftrag drucken
	PrintAuftragMessage = new CPrintAuftragMessage (this);
	MessageHandler->Register (PrintAuftragMessage);

// Handler f�r Liefderschein drucken
	PrintLiefMessage = new CPrintLiefMessage (this);
	MessageHandler->Register (PrintLiefMessage);

// Handler f�r Rechnung drucken
	PrintRechMessage = new CPrintRechMessage (this);
	MessageHandler->Register (PrintRechMessage);

// Handler f�r Kalkulationsfenster
	CalcViewMessage = new CCalcViewMessage (this);
	MessageHandler->Register (CalcViewMessage);

// Handler f�r Positionstexte
	PosTextMessage = new CPosTextMessage (this);
	MessageHandler->Register (PosTextMessage);

// Handler f�r Kopftexte
	KopfTextMessage = new CKopfTextMessage (this);
	MessageHandler->Register (KopfTextMessage);

// Handler f�r Fu�texte
	FussTextMessage = new CFussTextMessage (this);
	MessageHandler->Register (FussTextMessage);

	Datatables = CDatatables::GetInstance ();
	A_bas = Datatables->GetABas ();
	Partyk = Datatables->GetPartyk ();
	Partyp = Datatables->GetPartyp ();
	Mdn = Datatables->GetMdn ();
	Fil = Datatables->GetFil ();
	Kun = Datatables->GetKun ();
	MdnAdr = Datatables->GetMdnAdr ();
	FilAdr = Datatables->GetFilAdr ();
	KunAdr = Datatables->GetKunAdr ();
	AngebotCore = CAngebotCore::GetInstance ();
	AngebotCore->SetForm (&Form);
	AngebotCore->SetFormAuf (&Form);

	m_AngVk.SetReadOnly ();
	m_AngGew.SetReadOnly ();
	m_EinzelVk.SetReadOnly ();

	m_CalcPos.SetWindowText (_T(""));
	m_CalcPos.SetToolTip (_T("Einzelmengen neu Kalkulieren"));
	m_CalcPos.nID = IDC_CALC_POS;
	m_CalcPos.Orientation = m_CalcPos.Left;
	m_CalcPos.TextStyle = m_CalcPos.Standard;
	m_CalcPos.BorderStyle = m_CalcPos.NoBorder;
	m_CalcPos.DynamicColor = FALSE;
	m_CalcPos.TextColor =RGB (0, 0, 0);
//	m_CalcPos.SetBkColor (BkColor);
	m_CalcPos.SetBkColor (DlgBkColor);
	m_CalcPos.DynColor = DynColor;
//	m_CalcPos.RolloverColor = RolloverColor;
//	m_CalcPos.RolloverColor = RGB (255,255,255);;
	m_CalcPos.RolloverColor = RGB (230,230,255);;
	m_CalcPos.LoadBitmap (IDB_CALCPOS);
	m_CalcPos.LoadMask (IDB_CALCPOS_MASK);

	m_Address.SetWindowText (_T("Adresse"));
	m_Address.nID = IDC_ADDRESS;
	m_Address.Orientation = m_Address.Left;
	m_Address.TextStyle = m_Address.Standard;
	m_Address.BorderStyle = m_Address.Solide;
	m_Address.DynamicColor = TRUE;
	m_Address.TextColor =RGB (0, 0, 0);
	m_Address.SetBkColor (BkColor);
	m_Address.DynColor = DynColor;
	m_Address.RolloverColor = RolloverColor;
	m_Address.LoadBitmap (IDB_ADDRESS);
	m_Address.LoadMask (IDB_ADDRESS_MASK);

	m_LiefAddress.SetWindowText (_T("Lieferadresse"));
	m_LiefAddress.nID = IDC_LIEF_ADR;
	m_LiefAddress.Orientation = m_LiefAddress.Left;
	m_LiefAddress.TextStyle = m_LiefAddress.Standard;
	m_LiefAddress.BorderStyle = m_LiefAddress.Solide;
	m_LiefAddress.DynamicColor = TRUE;
	m_LiefAddress.TextColor =RGB (0, 0, 0);
	m_LiefAddress.SetBkColor (BkColor);
	m_LiefAddress.DynColor = DynColor;
	m_LiefAddress.RolloverColor = RolloverColor;
	m_LiefAddress.LoadBitmap (IDB_LIEF_ADR);
	m_LiefAddress.LoadMask (IDB_LIEF_ADR_MASK);

	m_NewAngTyp.SetWindowText (_T("Neu"));
	m_NewAngTyp.nID = IDC_NEW_ANG_TYP;
	m_NewAngTyp.Orientation = m_NewAngTyp.Left;
	m_NewAngTyp.TextStyle = m_NewAngTyp.Standard;
	m_NewAngTyp.BorderStyle = m_NewAngTyp.Solide;
	m_NewAngTyp.DynamicColor = TRUE;
	m_NewAngTyp.TextColor =RGB (0, 0, 0);
	m_NewAngTyp.SetBkColor (BkColor);
	m_NewAngTyp.DynColor = DynColor;
	m_NewAngTyp.RolloverColor = RolloverColor;
	m_NewAngTyp.LoadBitmap (IDB_ANGEBOTTYPES);
	m_NewAngTyp.LoadMask (IDB_ANGEBOTTYPES_MASK);
	m_NewAngTyp.ShowWindow (SW_HIDE);

	m_ModifyPers.SetWindowText (_T("Bearbeiten"));
	m_ModifyPers.nID = IDC_MODIFY_PERS;
	m_ModifyPers.SetToolTip (_T("Bearbeiten"));
	m_ModifyPers.Orientation = m_ModifyPers.Left;
	m_ModifyPers.TextStyle = m_ModifyPers.Standard;
	m_ModifyPers.BorderStyle = m_ModifyPers.Solide;
	m_ModifyPers.DynamicColor = TRUE;
	m_ModifyPers.TextColor =RGB (0, 0, 0);
	m_ModifyPers.SetBkColor (BkColor);
	m_ModifyPers.DynColor = DynColor;
	m_ModifyPers.RolloverColor = RolloverColor;
	m_ModifyPers.LoadBitmap (IDB_ANGEBOTTYPES);
	m_ModifyPers.LoadMask (IDB_ANGEBOTTYPES_MASK);

	m_Comments.SetWindowText (_T("Kommentare"));
	m_Comments.nID = IDC_COMMENTS;
	m_Comments.SetToolTip (_T("Kommentare erfassen"));
	m_Comments.Orientation = m_Comments.Left;
	m_Comments.TextStyle = m_Comments.Underline;
	m_Comments.BorderStyle = m_Comments.NoBorder;
	m_Comments.DynamicColor = FALSE;
	m_Comments.TextColor =RGB (0, 0, 0);
//	m_Comments.SetBkColor (BkColor);
	m_Comments.SetBkColor (DlgBkColor);
	m_Comments.DynColor = DynColor;
//	m_Comments.RolloverColor = RGB (255,255,255);;
	m_Comments.RolloverColor = RGB (230,230,255);;
	m_Comments.LoadBitmap (IDB_POS_TEXT);
	m_Comments.LoadMask (IDB_POS_TEXT_MASK);

	m_PreisLabel.SetWindowText (_T(" Preise und Gewicht f�r Auftrag"));
	m_PreisLabel.SetBoderStyle (m_PreisLabel.Solide);
	m_PreisLabel.SetBoderStyle (m_PreisLabel.None);
	m_PreisLabel.SetOrientation (m_PreisLabel.Left);
	m_PreisLabel.SetDynamicColor (TRUE);
	m_PreisLabel.SetTextColor (RGB (255, 255, 255));
	m_PreisLabel.SetBkColor (RGB (128, 128, 128));

	m_CalcLabel.SetWindowText (_T(" berechnet"));
	m_CalcLabel.SetBoderStyle (m_PreisLabel.None);
	m_CalcLabel.SetOrientation (m_PreisLabel.Left);
//	m_CalcLabel.SetDynamicColor (TRUE);
	m_CalcLabel.SetTextColor (RGB (0, 0, 255));
	m_CalcLabel.SetBkColor (RGB (192, 192, 192));

	m_PauschLabel.SetWindowText (_T(" pauschal"));
	m_PauschLabel.SetBoderStyle (m_PreisLabel.None);
	m_PauschLabel.SetOrientation (m_PreisLabel.Left);
//	m_PauschLabel.SetDynamicColor (TRUE);
	m_PauschLabel.SetTextColor (RGB (0, 0, 255));
	m_PauschLabel.SetBkColor (RGB (192, 192, 192));

	memcpy (&Mdn->mdn, &mdn_null, sizeof (MDN));
	memcpy (&Fil->fil, &fil_null, sizeof (FIL));
	memcpy (&Kun->kun, &kun_null, sizeof (KUN));
	memcpy (&MdnAdr->adr, &adr_null, sizeof (ADR));
	memcpy (&FilAdr->adr, &adr_null, sizeof (ADR));
	memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
	memcpy (&Partyk->partyk, &partyk_null, sizeof (PARTYK));
	AngebotCore->SetPersIdx (0);

	if (GetSystemMetrics (SM_CXFULLSCREEN) <= 800)
	{
		Font.CreatePointFont (85, _T("Dlg"));
	}
	else
	{
		Font.CreatePointFont (95, _T("Dlg"));
	}

	LOGFONT l; 
	Font.GetLogFont (&l);
	l.lfWeight = FW_BOLD;
	l.lfHeight = 17;
	TitleFont.CreateFontIndirect (&l);

	m_AngKun = TRUE;
	m_AngFil = FALSE;

	Form.Add (new CFormField (&m_Mdn,EDIT,        (short *)   &Mdn->mdn.mdn, VSHORT));
	Form.Add (new CFormField (&m_MdnName,EDIT,    (char *)    MdnAdr->adr.adr_krz, VCHAR));
	Form.Add (new CFormField (&m_Fil,EDIT,        (short *)   &Fil->fil.fil, VSHORT));
	Form.Add (new CFormField (&m_FilName,EDIT,    (char *)    FilAdr->adr.adr_krz, VCHAR));
	Form.Add (new CFormField (&m_BKun,CHECKBOX,   (short *)   &m_AngKun, VSHORT));
	Form.Add (new CFormField (&m_BFil,CHECKBOX,   (short *)   &m_AngFil, VSHORT));
	Form.Add (new CFormField (&m_AngebotNr,EDIT,  (long *)    &Partyk->partyk.ang, VLONG));
	Form.Add (new CFormField (&m_Auf,EDIT,        (long *)    &Partyk->partyk.auf, VLONG));
	Form.Add (new CFormField (&m_Ls,EDIT,         (long *)    &Partyk->partyk.ls, VLONG));
	Form.Add (new CFormField (&m_Kun,EDIT,        (long *)    &Kun->kun.kun, VLONG));
	Form.Add (new CFormField (&m_KunName,EDIT,    (char *)    KunAdr->adr.adr_krz, VCHAR));
	Form.Add (new CFormField (&m_Tel,EDIT,        (char *)    KunAdr->adr.tel, VCHAR));
	Form.Add (new CFormField (&m_KunName2,EDIT,    (char *)    KunAdr->adr.adr_krz, VCHAR));
	Form.Add (new CFormField (&m_Tel2,EDIT,        (char *)    KunAdr->adr.tel, VCHAR));
	Form.Add (new CFormField (&m_Hinweis,EDIT,     (char *)    Partyk->partyk.hinweis, VCHAR));
	Form.Add (new CFormField (&m_AnzPers,EDIT,     (short *)   &Partyk->partyk.anz_pers, VSHORT));
	Form.Add (new CFormField (&m_EinzelPreis,CHECKBOX, (short *)   &Partyk->partyk.einzel_preis, VSHORT));
	Form.Add (new CFormField (&m_NurServicePreis,CHECKBOX, (short *)   &Partyk->partyk.nur_servicepreis, VSHORT));
	Form.Add (new CFormField (&m_MwstVoll,CHECKBOX, (short *)   &AngebotCore->IsVatFull, VSHORT));
	Form.Add (new CFormField (&m_AngVk,EDIT,  (double *)   &Partyk->partyk.ang_vk, VDOUBLE, 10,2));
	Form.Add (new CFormField (&m_PauschalVk,EDIT,  (double *)   &Partyk->partyk.pauschal_vk, VDOUBLE, 10,2));
	Form.Add (new CFormField (&m_EinzelVk,EDIT,  (double *)   &Partyk->partyk.einzel_vk, VDOUBLE, 10,2));
	Form.Add (new CFormField (&m_EinzelVkPausch,EDIT,  (double *)   &Partyk->partyk.einzel_vk_pausch, VDOUBLE, 10,2));
	Form.Add (new CFormField (&m_ServiceVkPausch,EDIT,  (double *)   &Partyk->partyk.service_pausch_vk, VDOUBLE, 10,2));
	Form.Add (new CFormField (&m_AngGew,EDIT, (double *)   &Partyk->partyk.ang_gew, VDOUBLE, 10,3));
	Form.Add (new CFormField (&m_Lieferdat,DATETIMEPICKER,  (DATE_STRUCT *) &Partyk->partyk.lieferdat, VDATE));
	Form.Add (new CFormField (&m_Lieferzeit,TIMEPICKER,  (char *) Partyk->partyk.lieferzeit, VCHAR));
	Form.Add (new CFormField (&m_Abholung,CHECKBOX, (short *)   &Partyk->partyk.abholung, VSHORT));
	Form.Add (new CFormField (&m_HideMe,CHECKBOX, (short *)   &Partyk->partyk.hide_me, VSHORT));
	Form.Add (new CFormField (&m_HideService,CHECKBOX, (short *)   &Partyk->partyk.hide_service, VSHORT));
	CFormField *TypField = new CFormField (&m_AngTyp,COMBOBOX,  (short *) &Partyk->partyk.typ, VSHORT);
	Form.Add (TypField);
	CFormField *PersField = new CFormField (&m_Pers,COMBOBOXIDX,  (long *) AngebotCore->GetPPersIdx (), VLONG);
	Form.Add (PersField);
	AngebotCore->FillCombo (_T("ang_typ"), &TypField->ComboValues);
	AngebotCore->FillPers ( &PersField->ComboValues);
	TypField->FillComboBox ();
	TypField->SetSel (0);
//	TypField->Get ();
	PersField->FillComboBox ();
	PersField->SetSel (0);

	m_List.List = AngebotCore->GetList ();
	FillList = m_List;
	FillList.SetStyle (LVS_REPORT);
	if (m_List.GridLines)
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	}
	else
	{
		FillList.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	}
	FillList.SetCol (_T(""), 0, 0);
	FillList.SetCol (_T("Pos"),         m_List.POSPOS,       30,  LVCFMT_RIGHT);
	FillList.SetCol (_T("Stat"),        m_List.POSSTAT,      20,  LVCFMT_RIGHT);
	FillList.SetCol (_T("Artikel"),     m_List.POSA,        100, LVCFMT_RIGHT);
	FillList.SetCol (_T("Bezeichnung"), m_List.POSABZ1,     120, LVCFMT_LEFT);
	FillList.SetCol (_T("Menge"),       m_List.POSME,        60, LVCFMT_RIGHT);
	FillList.SetCol (_T("Einheit"),     m_List.POSEINH,     100, LVCFMT_LEFT);
	FillList.SetCol (_T("Gewicht"),     m_List.POSBASINH,     60, LVCFMT_RIGHT);
	FillList.SetCol (_T("L.Menge"),     m_List.POSBASME,     60, LVCFMT_RIGHT);
	FillList.SetCol (_T("Einheit"),     m_List.POSBASEINH,   60, LVCFMT_LEFT);
	FillList.SetCol (_T("Gebinde"),     m_List.POSGEBINDE,   80, LVCFMT_LEFT);
	FillList.SetCol (_T("Letzter Preis"), m_List.POSLASTVK,     0, LVCFMT_RIGHT);
	FillList.SetCol (_T("Netto-Preis"), m_List.POSVK,        80, LVCFMT_RIGHT);
	FillList.SetCol (_T("Brutto-Preis"), m_List.POSVKBRUTTO, 90, LVCFMT_RIGHT);
	FillList.SetCol (_T("Pos.Preis"), m_List.POSVKME,  80, LVCFMT_RIGHT);
	FillList.SetCol (_T("Einzelpreis"),   m_List.POSEINZELPR,  80, LVCFMT_CENTER);

	m_List.ColType.Add (new CColType (m_List.POSEINZELPR, m_List.LCHECKBOX));

	m_List.AddDisplayOnly (m_List.POSABZ1);
	m_List.AddDisplayOnly (m_List.POSBASME);
//	m_List.AddDisplayOnly (m_List.POSEINH);
	m_List.AddDisplayOnly (m_List.POSSTAT);
	m_List.AddDisplayOnly (m_List.POSVKME);
	m_List.AddDisplayOnly (m_List.POSBASEINH);

    CtrlGrid.Create (this, 20, 20);
    CtrlGrid.SetBorder (0, 0);
	CtrlGrid.SetCellHeight (15);
    CtrlGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

//Grid Mdn
	MdnGrid.Create (this, 2, 2);
    MdnGrid.SetBorder (0, 0);
    MdnGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Mdn = new CCtrlInfo (&m_Mdn, 0, 0, 1, 1);
	MdnGrid.Add (c_Mdn);
	CtrlGrid.CreateChoiceButton (m_MdnChoice, IDC_MDNCHOICE, this);
	CCtrlInfo *c_MdnChoice = new CCtrlInfo (&m_MdnChoice, 1, 0, 1, 1);
	MdnGrid.Add (c_MdnChoice);

//Grid Fil
	FilGrid.Create (this, 2, 2);
    FilGrid.SetBorder (0, 0);
    FilGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Fil = new CCtrlInfo (&m_Fil, 0, 0, 1, 1);
	FilGrid.Add (c_Fil);
	CtrlGrid.CreateChoiceButton (m_FilChoice, IDC_FILCHOICE, this);
	CCtrlInfo *c_FilChoice = new CCtrlInfo (&m_FilChoice, 1, 0, 1, 1);
	FilGrid.Add (c_FilChoice);

//Grid Angebotsnummer

	AngGrid.Create (this, 5, 8);
    AngGrid.SetBorder (0, 0);
    AngGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_AngebotNr = new CCtrlInfo (&m_AngebotNr, 0, 0, 1, 1);
	AngGrid.Add (c_AngebotNr);
	CtrlGrid.CreateChoiceButton (m_AngChoice, IDC_ANGCHOICE, this);
	CCtrlInfo *c_AngChoice = new CCtrlInfo (&m_AngChoice, 1, 0, 1, 1);
	AngGrid.Add (c_AngChoice);
	CCtrlInfo *c_LAuf = new CCtrlInfo (&m_LAuf, 2, 0, 1, 1);
	c_LAuf->SetCellPos (5, 0);
	AngGrid.Add (c_LAuf);
	CCtrlInfo *c_Auf = new CCtrlInfo (&m_Auf, 3, 0, 1, 1);
	AngGrid.Add (c_Auf);
	CCtrlInfo *c_LLs = new CCtrlInfo (&m_LLs, 4, 0, 1, 1);
	c_LLs->SetCellPos (5, 0);
	AngGrid.Add (c_LLs);
	CCtrlInfo *c_Ls = new CCtrlInfo (&m_Ls, 5, 0, 1, 1);
	AngGrid.Add (c_Ls);

// Grid KunFil
	KunFilGrid.Create (this, 2, 2);
    KunFilGrid.SetBorder (0, 0);
    KunFilGrid.SetGridSpace (5, 8);
	CCtrlInfo *c_BKun = new CCtrlInfo (&m_BKun, 0, 0, 1, 1);
	KunFilGrid.Add (c_BKun);
	CCtrlInfo *c_BFil = new CCtrlInfo (&m_BFil, 1, 0, 1, 1);
	KunFilGrid.Add (c_BFil);

//Grid Kunde

	KunGrid.Create (this, 2, 2);
    KunGrid.SetBorder (0, 0);
    KunGrid.SetGridSpace (0, 0);
	CCtrlInfo *c_Kun = new CCtrlInfo (&m_Kun, 0, 0, 1, 1);
	KunGrid.Add (c_Kun);
	CtrlGrid.CreateChoiceButton (m_KunChoice, IDC_KUNCHOICE, this);
	CCtrlInfo *c_KunChoice = new CCtrlInfo (&m_KunChoice, 1, 0, 1, 1);
	KunGrid.Add (c_KunChoice);

//Grid Adresse

	AdressGrid.Create (this, 1, 8);
    AdressGrid.SetBorder (0, 0);
    AdressGrid.SetGridSpace (5, 8);

	CCtrlInfo *c_AnzPers = new CCtrlInfo (&m_AnzPers, 1, 0, 1, 1);
	AdressGrid.Add (c_AnzPers);
	CCtrlInfo *c_CalcPos = new CCtrlInfo (&m_CalcPos, 2, 0, 1, 1);
	c_CalcPos->SetCellPos (-5, 0);
	AdressGrid.Add (c_CalcPos);
	CCtrlInfo *c_Address = new CCtrlInfo (&m_Address, 3, 0, 1, 1);
	c_Address->SetCellPos (15, 0);
	AdressGrid.Add (c_Address);
	CCtrlInfo *c_LLieferdat = new CCtrlInfo (&m_LLieferdat, 4, 0, 1, 1);
	AdressGrid.Add (c_LLieferdat);
	CCtrlInfo *c_Lieferdat = new CCtrlInfo (&m_Lieferdat, 5, 0, 1, 1);
	AdressGrid.Add (c_Lieferdat);
	CCtrlInfo *c_LLieferzeit = new CCtrlInfo (&m_LLieferzeit, 6, 0, 1, 1);
	AdressGrid.Add (c_LLieferzeit);
	CCtrlInfo *c_Lieferzeit = new CCtrlInfo (&m_Lieferzeit, 7, 0, 1, 1);
	AdressGrid.Add (c_Lieferzeit);

/*
	CCtrlInfo *c_LiefAddress = new CCtrlInfo (&m_LiefAddress, 6, 0, 1, 1);
	AdressGrid.Add (c_LiefAddress);
*/

//Grid Angebotstyp
	TypGrid.Create (this, 8, 8);
    TypGrid.SetBorder (0, 0);
    TypGrid.SetGridSpace (5, 8);
	CCtrlInfo *c_AngTyp = new CCtrlInfo (&m_AngTyp, 0, 0, 1, 1);
	TypGrid.Add (c_AngTyp);
	CCtrlInfo *c_NewAngTyp = new CCtrlInfo (&m_NewAngTyp, 1, 0, 1, 1);
	TypGrid.Add (c_NewAngTyp);
	CCtrlInfo *c_LiefAddress = new CCtrlInfo (&m_LiefAddress, 2, 0, 3, 1);
	TypGrid.Add (c_LiefAddress);
/*
	CCtrlInfo *c_LAnzPers = new CCtrlInfo (&m_LAnzPers, 2, 0, 1, 1);
	TypGrid.Add (c_LAnzPers);
	CCtrlInfo *c_AnzPers = new CCtrlInfo (&m_AnzPers, 3, 0, 1, 1);
	TypGrid.Add (c_AnzPers);
*/

	PriceGrid.Create (this, 8, 8);
    PriceGrid.SetBorder (0, 0);
    PriceGrid.SetGridSpace (1, 8);
	CCtrlInfo *c_CalcLabel = new CCtrlInfo (&m_CalcLabel, 0, 0, 2, 1);
	PriceGrid.Add (c_CalcLabel);
	CCtrlInfo *c_PauschLabel = new CCtrlInfo (&m_PauschLabel, 2, 0, 2, 1);
	PriceGrid.Add (c_PauschLabel);
	CCtrlInfo *c_LAngVk = new CCtrlInfo (&m_LAngVk, 0, 1, 1, 1);
	PriceGrid.Add (c_LAngVk);
	CCtrlInfo *c_AngVk = new CCtrlInfo (&m_AngVk, 1, 1, 1, 1);
	PriceGrid.Add (c_AngVk);
	CCtrlInfo *c_LEinzelVk = new CCtrlInfo (&m_LEinzelVk, 0, 2, 1, 1);
	PriceGrid.Add (c_LEinzelVk);
	CCtrlInfo *c_EinzelVk = new CCtrlInfo (&m_EinzelVk, 1, 2, 1, 1);
	PriceGrid.Add (c_EinzelVk);
	CCtrlInfo *c_LAngGew = new CCtrlInfo (&m_LAngGew, 0, 3, 1, 1);
	PriceGrid.Add (c_LAngGew);
	CCtrlInfo *c_AngGew = new CCtrlInfo (&m_AngGew, 1, 3, 1, 1);
	PriceGrid.Add (c_AngGew);
	CCtrlInfo *c_LPauschalVk = new CCtrlInfo (&m_LPauschalVk, 2, 1, 1, 1);
	PriceGrid.Add (c_LPauschalVk);
	CCtrlInfo *c_PauschalVk = new CCtrlInfo (&m_PauschalVk, 3, 1, 1, 1);
	PriceGrid.Add (c_PauschalVk);
	CCtrlInfo *c_LEinzelVkPausch = new CCtrlInfo (&m_LEinzelVkPausch, 2, 2, 1, 1);
	PriceGrid.Add (c_LEinzelVkPausch);
	CCtrlInfo *c_EinzelVkPausch = new CCtrlInfo (&m_EinzelVkPausch, 3, 2, 1, 1);
	PriceGrid.Add (c_EinzelVkPausch);

	CCtrlInfo *c_LServiceVkPausch = new CCtrlInfo (&m_LServiceVkPausch, 2, 3, 1, 1);
	PriceGrid.Add (c_LServiceVkPausch);
	CCtrlInfo *c_ServiceVkPausch = new CCtrlInfo (&m_ServiceVkPausch, 3, 3, 1, 1);
	PriceGrid.Add (c_ServiceVkPausch);

	CCtrlInfo *c_HideMe = new CCtrlInfo (&m_HideMe, 4, 0, 1, 1);
	PriceGrid.Add (c_HideMe);
	CCtrlInfo *c_HideService = new CCtrlInfo (&m_HideService, 5, 0, 1, 1);
	PriceGrid.Add (c_HideService);

	CCtrlInfo *c_EinzelPreis = new CCtrlInfo (&m_EinzelPreis, 4, 1, 1, 1);
	PriceGrid.Add (c_EinzelPreis);
	CCtrlInfo *c_NurServicePreis = new CCtrlInfo (&m_NurServicePreis, 4, 2, 1, 1);
	PriceGrid.Add (c_NurServicePreis);
	CCtrlInfo *c_MwstVoll = new CCtrlInfo (&m_MwstVoll, 4, 3, 1, 1);
	PriceGrid.Add (c_MwstVoll);

    HeadGrid.Create (this, 20, 20);
    HeadGrid.SetBorder (0, 0);
	HeadGrid.SetCellHeight (15);
    HeadGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand
	CCtrlInfo *c_LMdn = new CCtrlInfo (&m_LMdn, 0, 0, 1, 1);
	HeadGrid.Add (c_LMdn);
	CCtrlInfo *c_MdnGrid = new CCtrlInfo (&MdnGrid, 1, 0, 1, 1);
	HeadGrid.Add (c_MdnGrid);
	CCtrlInfo *c_MdnName = new CCtrlInfo (&m_MdnName, 2, 0, 1, 1);
	HeadGrid.Add (c_MdnName);

	CCtrlInfo *c_LFil = new CCtrlInfo (&m_LFil, 3, 0, 1, 1);
	HeadGrid.Add (c_LFil);
	CCtrlInfo *c_FilGrid = new CCtrlInfo (&FilGrid, 4, 0, 1, 1);
	HeadGrid.Add (c_FilGrid);
	CCtrlInfo *c_FilName = new CCtrlInfo (&m_FilName, 5, 0, 1, 1);
	HeadGrid.Add (c_FilName);

	CCtrlInfo *c_LAngebotNr = new CCtrlInfo (&m_LAngebotNr, 0, 1, 1, 1);
	HeadGrid.Add (c_LAngebotNr);
	CCtrlInfo *c_AngGrid = new CCtrlInfo (&AngGrid, 1, 1, 6, 1);
	HeadGrid.Add (c_AngGrid);
	CCtrlInfo *c_KunFilGrid = new CCtrlInfo (&KunFilGrid, 1, 2, 2, 1);
	HeadGrid.Add (c_KunFilGrid);

    DataGrid.Create (this, 20, 20);
    DataGrid.SetBorder (0, 0);
	DataGrid.SetCellHeight (15);
    DataGrid.SetGridSpace (5, 8);  //Spaltenabstand und Zeilenabstand

	CCtrlInfo *c_LKun = new CCtrlInfo (&m_LKun, 0, 0, 1, 1);
	DataGrid.Add (c_LKun);
	CCtrlInfo *c_KunGrid = new CCtrlInfo (&KunGrid, 1, 0, 1, 1);
	DataGrid.Add (c_KunGrid);
	CCtrlInfo *c_KunName = new CCtrlInfo (&m_KunName, 2, 0, 1, 1);
	DataGrid.Add (c_KunName);
	CCtrlInfo *c_KunName2 = new CCtrlInfo (&m_KunName2, 2, 0, 1, 1);
	DataGrid.Add (c_KunName2);
	CCtrlInfo *c_LTel = new CCtrlInfo (&m_LTel, 3, 0, 1, 1);
	DataGrid.Add (c_LTel);
	CCtrlInfo *c_Tel = new CCtrlInfo (&m_Tel, 4, 0, 1, 1);
	DataGrid.Add (c_Tel);
	CCtrlInfo *c_Tel2 = new CCtrlInfo (&m_Tel2, 4, 0, 1, 1);
	DataGrid.Add (c_Tel2);
	CCtrlInfo *c_LAnzPers = new CCtrlInfo (&m_LAnzPers, 0, 1, 1, 1);
	DataGrid.Add (c_LAnzPers);
	CCtrlInfo *c_AdressGrid = new CCtrlInfo (&AdressGrid, 1, 1, 4, 1);
	DataGrid.Add (c_AdressGrid);
	CCtrlInfo *c_LAngTyp = new CCtrlInfo (&m_LAngTyp, 0, 2, 1, 1);
	c_LAngTyp->SetCellPos (0, 8);
	DataGrid.Add (c_LAngTyp);
	CCtrlInfo *c_TypGrid = new CCtrlInfo (&TypGrid, 1, 2, 4, 1);
	c_TypGrid->SetCellPos (0, 8);
	DataGrid.Add (c_TypGrid);

	CCtrlInfo *c_LPers = new CCtrlInfo (&m_LPers, 0, 4, 1, 1);
	DataGrid.Add (c_LPers);

//Grid Pers
	PersGrid.Create (this, 8, 8);
    PersGrid.SetBorder (0, 0);
    PersGrid.SetGridSpace (5, 8);
	CCtrlInfo *c_Pers = new CCtrlInfo (&m_Pers, 0, 0, 1, 1);
	PersGrid.Add (c_Pers);
	CCtrlInfo *c_ModifyPers = new CCtrlInfo (&m_ModifyPers, 1, 0, 1, 1);
	PersGrid.Add (c_ModifyPers);
	CCtrlInfo *c_Abholung = new CCtrlInfo (&m_Abholung, 2, 0, 1, 1);
	PersGrid.Add (c_Abholung);
	CCtrlInfo *c_Comments = new CCtrlInfo (&m_Comments, 3, 0, 1, 1);
	PersGrid.Add (c_Comments);
	CCtrlInfo *c_PersGrid = new CCtrlInfo (&PersGrid, 1, 4, 5, 1);
	DataGrid.Add (c_PersGrid);

	CCtrlInfo *c_LHinweis = new CCtrlInfo (&m_LHinweis, 0, 5, 1, 1);
	DataGrid.Add (c_LHinweis);
	CCtrlInfo *c_Hinweis = new CCtrlInfo (&m_Hinweis, 1, 5, 5, 3);
	DataGrid.Add (c_Hinweis);

	CCtrlInfo *c_PriceGrid = new CCtrlInfo (&PriceGrid, 0, 9, 5, 3);
	c_PriceGrid->SetCellPos (0, -8);
	DataGrid.Add (c_PriceGrid);

	CCtrlInfo *c_HeadGrid = new CCtrlInfo (&HeadGrid, 0, 0, 1, 1);
	CtrlGrid.Add (c_HeadGrid);
	CCtrlLine *Line1 = new CCtrlLine (this, HORIZONTAL, 200, 1, 0, 3, DOCKSIZE, 1);
    CtrlGrid.Add (Line1);
	CCtrlInfo *c_DataGrid = new CCtrlInfo (&DataGrid, 0, 4, 1, 1);
	CtrlGrid.Add (c_DataGrid);
	CCtrlInfo *c_PreisLabel = new CCtrlInfo (&m_PreisLabel, 0, 11, DOCKRIGHT, 1);
	c_PreisLabel->SetCellPos (0, 5);
	c_PreisLabel->rightspace = 5;
	CtrlGrid.Add (c_PreisLabel);
	CCtrlInfo *c_List = new CCtrlInfo (&m_List, 0, 17, DOCKRIGHT, DOCKBOTTOM);
	c_List->rightspace = 5;
	CtrlGrid.Add (c_List);

	SetFont (&Font);
    CDC *cDC = GetDC ();
	cDC->SelectObject (&Font);
	ReleaseDC (cDC);
	SetFont (&Font, FALSE);
	CtrlGrid.SetFont (&Font);

	m_CalcPos.Tooltip.SetFont (&Font);
	m_CalcPos.Tooltip.SetSize ();
	m_Comments.Tooltip.SetFont (&Font);
	m_Comments.Tooltip.SetSize ();
	m_ModifyPers.Tooltip.SetFont (&Font);
	m_ModifyPers.Tooltip.SetSize ();

	m_PreisLabel.SetFont (&TitleFont);

	Form.Show ();
	CtrlGrid.Display ();
	DataGrid.Enable (FALSE);
	m_Mdn.SetFocus ();
	m_Fil.EnableWindow (FALSE);
	m_FilChoice.EnableWindow (FALSE);
	return FALSE;
}

HBRUSH CAuftragPage::OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	else if (pWnd == &m_KunName)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			pDC->SetTextColor (RGB (0,0,255));
			return DlgBrush;
	}
	else if (pWnd == &m_Tel)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			pDC->SetTextColor (RGB (0,0,255));
			return DlgBrush;
	}
/*
	else if (pWnd == &m_NewAngTyp && DlgBkColor != NULL)
	{
		    if (DlgBrush == NULL)
			{
				DlgBrush = CreateSolidBrush (DlgBkColor);
			}
			pDC->SetBkMode (TRANSPARENT);
			pDC->SetBkColor (DlgBkColor);
			return DlgBrush;
	}
*/
	else if (nCtlColor == CTLCOLOR_STATIC && DlgBkColor != NULL)
	{
            pDC->SetBkColor (DlgBkColor);
			pDC->SetBkMode (TRANSPARENT);
			return DlgBrush;
	}
	return CDialog::OnCtlColor (pDC, pWnd,nCtlColor);
}

void CAuftragPage::OnSize (UINT nType, int cx, int cy)
{
	CRect rect (0, 0, cx, cy);
	CtrlGrid.pcx = 0;
	CtrlGrid.pcy = 0;
	CtrlGrid.DlgSize = &rect;
	CtrlGrid.Move (0, 0);
	CtrlGrid.DlgSize = NULL;
	CtrlGrid.Display ();
}

BOOL CAuftragPage::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{

		case WM_KEYDOWN :
			if (pMsg->wParam == VK_RETURN)
			{
				if (OnReturn ())
				{
					return TRUE;
				}
/*
				if (GetFocus () == &m_Hinweis)
				{
					return FALSE;
				}
				m_List.OnKeyD (VK_RETURN);
*/
				break;
			}
			else if (pMsg->wParam == VK_TAB)
			{

				if (GetFocus () != &m_List &&
					GetFocus ()->GetParent () != &m_List )
				{
						break;
			    }
				if (GetKeyState (VK_CONTROL) < 0)
				{
					m_AnzPers.SetFocus ();
				}
				else
				{
					m_List.OnKeyD (VK_TAB);
				}
				return TRUE;
			}

/*
			else if (pMsg->wParam == VK_F3)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->NextRec ();
				}
				else
				{
					Basis->LastRec ();
				}
				return TRUE;

			}

			else if (pMsg->wParam == VK_F2)
			{
				if (GetKeyState (VK_CONTROL) >= 0)
				{
					Basis->PriorRec ();
				}
				else
				{
					Basis->FirstRec ();
				}
				return TRUE;

			}
*/
			else if (pMsg->wParam == VK_DOWN)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}

				if (OnKeyDown ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_UP)
			{
				if (GetFocus ()->IsKindOf (RUNTIME_CLASS (CComboBox)))
				{
					break;
				}

				if (OnKeyUp ())
				{
					return TRUE;
				}
				break;
			}
			else if (pMsg->wParam == VK_F5)
			{
				if (GetFocus () != &m_List &&
					GetFocus ()->GetParent () != &m_List )
				{
					OnF5 ();
				}
				else
				{
					m_AnzPers.SetFocus ();
				}
				return TRUE;
			}
			else if (pMsg->wParam == VK_ESCAPE)
			{
				OnF5 ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F6)
			{

				OnF6 ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F7)
			{
				m_List.OnKeyD (VK_F7);
				return TRUE;
			}
			else if (pMsg->wParam == VK_F11)
			{
				OnPosText ();
				return TRUE;
			}
			else if (pMsg->wParam == VK_F12)
			{
				OnF12 ();
				return TRUE;
			}

			else if (pMsg->wParam == VK_F8)
			{
/*
				if (Basis->Choice != NULL)
				{
					if (Basis->Choice->IsWindowVisible ())
					{
						Basis->Choice->ShowWindow (SW_HIDE);
					}
					else
					{
						Basis->Choice->ShowWindow (SW_SHOWNORMAL);
					}
				}
*/
			}
			else if (pMsg->wParam == VK_F9)
			{
				OnF9 ();
				return TRUE;
			}
	}
	return FALSE;
}

BOOL CAuftragPage::OnReturn ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Mdn)
	{
		if (!ReadMdn ())
		{
			m_Mdn.SetFocus ();
			return FALSE;
		}
	}
	else if (Control == &m_Fil)
	{
		if (!ReadFil ())
		{
			m_Fil.SetFocus ();
			return FALSE;
		}
	}
	else if (Control == &m_AngebotNr)
	{
		if (!ReadAng ())
		{
			return FALSE;
		}
	}

	else if (Control == &m_Kun)
	{
		TestKun = FALSE;
		if (!ReadKun ())
		{
			TestKun = TRUE;
			m_Kun.SetFocus ();
			return FALSE;
		}
	}

	else if (Control == &m_Hinweis)
	{
		return FALSE;
	}
	if (Control != &m_List.ListEdit &&
		Control != &m_List.ListComboBox &&
		Control != &m_List.SearchListCtrl.Edit)
	{
		Control = GetNextDlgTabItem (Control, FALSE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}
		return TRUE;
	}
    m_List.OnKeyD (VK_RETURN);
	return TRUE;
}


BOOL CAuftragPage::OnKeyDown ()
{
	CWnd *Control = GetFocus ();
	if (Control == &m_Hinweis)
	{
		return FALSE;
	}
	if (Control != &m_List.ListEdit &&
		Control != &m_List.ListComboBox &&
		Control != &m_List.SearchListCtrl.Edit)
	{
		Control = GetNextDlgTabItem (Control, FALSE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}
		return TRUE;
	}
	return FALSE;
}

BOOL CAuftragPage::OnKeyUp ()
{
	CWnd *Control = GetFocus ();

	if (Control == &m_Hinweis)
	{
		return FALSE;
	}
	if (Control != &m_List.ListEdit &&
		Control != &m_List.ListComboBox &&
		Control != &m_List.SearchListCtrl.Edit)
	{
		Control = GetNextDlgTabItem (Control, TRUE);
		if (Control != NULL)
		{
			Control->SetFocus ();
		}
		return TRUE;
	}
	return FALSE;
}

BOOL CAuftragPage::OnDataBack ()
{
	if (!IsWindowVisible ())
	{
		return FALSE;
	}
	if (m_Mdn.IsWindowEnabled ())
	{
		return FALSE;
	}
	else
	{
		TestKun = FALSE;
		m_List.StopEnter ();
		AngebotCore->GetList ()->DestroyElements ();
		AngebotCore->GetDelList ()->Clear ();
		AngebotCore->IsVatFull = FALSE;
		Partyk->partyk.mwst_voll = FALSE;
		Partyk->rollbackwork ();
		long ang = Partyk->partyk.ang;
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
		memcpy (&Partyk->partyk, &partyk_null, sizeof (PARTYK));
		AngebotCore->SetPersIdx (0);
		Partyk->partyk.ang = ang;
		m_List.DeleteAllItems ();
		AngebotCore->GetList ()->DestroyElements ();
		AngebotCore->GetDelList ()->Clear ();
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
		HeadGrid.Enable (TRUE);
		DataGrid.Enable (FALSE);
		HideCalcView ();
		Form.Show ();
		m_AngebotNr.SetFocus ();
	}
	return TRUE;
}


BOOL CAuftragPage::OnF5 ()
{
	if (!IsWindowVisible ())
	{
		return FALSE;
	}
	if (m_Mdn.IsWindowEnabled ())
	{
		MessageHandler->RunMessage (VK_F5);
	}
	else
	{
		if (MessageBox (_T("�nderungen speichern ?"), NULL, 
						 MB_YESNO | MB_ICONQUESTION) ==
			IDYES)
		{
			OnF12 ();
			return TRUE;
		}
		TestKun = FALSE;
		m_List.StopEnter ();
		AngebotCore->GetList ()->DestroyElements ();
		AngebotCore->GetDelList ()->Clear ();
		AngebotCore->IsVatFull = FALSE;
		Partyk->partyk.mwst_voll = FALSE;
		Partyk->rollbackwork ();
		InitForm ();
		m_List.DeleteAllItems ();
		AngebotCore->GetList ()->DestroyElements ();
		AngebotCore->GetDelList ()->Clear ();
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
		HeadGrid.Enable (TRUE);
		DataGrid.Enable (FALSE);
		HideCalcView ();
		Form.Show ();
		m_AngebotNr.SetFocus ();
	}
	return TRUE;
}

void CAuftragPage::InitForm ()
{
	long ang = Partyk->partyk.ang;
	memcpy (&Kun->kun, &kun_null, sizeof (KUN));
	memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
	memcpy (&Partyk->partyk, &partyk_null, sizeof (PARTYK));
	AngebotCore->SetPersIdx (0);
	Partyk->partyk.ang = ang;
	Form.Show ();
}

BOOL CAuftragPage::OnF6 ()
{
	if (!m_Mdn.IsWindowEnabled ())
	{
		CWnd *Control = GetFocus ();
		if (Control != &m_List.ListEdit &&
			Control != &m_List.ListComboBox &&
			Control != &m_List.SearchListCtrl.Edit)
		{
			if (KunOk ())
			{
				m_List.SetFocus ();
				return TRUE;
			}
			return FALSE;
		}
		else
		{
			m_List.OnKeyD (VK_F6);
		}
	}
	return FALSE;
}

BOOL CAuftragPage::OnF9 ()
{
	CWnd *Control = GetFocus ();
	if (Control == &m_Mdn)
	{
		OnMdnChoice ();
	}
	else if (Control == &m_Fil)
	{
		OnFilChoice ();
	}
	else if (Control == &m_AngebotNr)
	{
		OnPartykChoice ();
	}
	else if (Control == &m_Kun)
	{
		OnKunChoice ();
	}
	else
	{
		m_List.OnKey9 ();
	}
	return TRUE;
}

BOOL CAuftragPage::OnF12 ()
{
	if (!m_Mdn.IsWindowEnabled ())
	{
// Schreiben
		WriteAng ();
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
		HeadGrid.Enable (TRUE);
		DataGrid.Enable (FALSE);
		HideCalcView ();
		MustCreate = FALSE;
		m_AngebotNr.SetFocus ();
	}
	return TRUE;
}

void CAuftragPage::SetFirstFocus ()
{
	m_Mdn.SetFocus ();
}

BOOL CAuftragPage::ReadMdn ()
{
	int mdn;

	mdn = Mdn->mdn.mdn;
	memcpy (&Mdn->mdn, &mdn_null, sizeof (MDN));
	memcpy (&MdnAdr->adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Mdn->mdn.mdn == 0)
	{
		CString Error;
		Error.Format (_T("Mandant 0 nicht erlaubt"));
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn->mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr->adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		m_Fil.EnableWindow (FALSE);
		m_FilChoice.EnableWindow (FALSE);
		return FALSE;
	}
	if (Mdn->dbreadfirst () == 0)
	{
		MdnAdr->adr.adr = Mdn->mdn.adr;
		MdnAdr->dbreadfirst ();
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		m_Fil.EnableWindow ();
		m_FilChoice.EnableWindow ();
		AngebotCore->SetMdn (Mdn->mdn.mdn);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Mandant %hd nicht gefunden"),Mdn->mdn.mdn);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn->mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr->adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		m_Fil.EnableWindow (FALSE);
		m_FilChoice.EnableWindow (FALSE);
		return FALSE;
	}
	return FALSE;
}

BOOL CAuftragPage::ReadFil ()
{
	memcpy (&Fil->fil, &fil_null, sizeof (FIL));
	memcpy (&FilAdr->adr, &adr_null, sizeof (ADR));
	Form.Get ();
	if (Mdn->mdn.mdn == 0)
	{
		return FALSE;
	}
	if (Fil->fil.fil == 0)
	{
		_tcscpy (FilAdr->adr.adr_krz, _T("Mandant"));
		Form.Show ();
		m_Fil.SetFocus ();
		m_Fil.SetSel (0, -1);
		AngebotCore->SetFil (Fil->fil.fil);
		return TRUE;
	}
	Fil->fil.mdn = Mdn->mdn.mdn;
	if (Fil->dbreadfirst () == 0)
	{
		FilAdr->adr.adr = Fil->fil.adr;
		FilAdr->dbreadfirst ();
		Form.Show ();
		m_Fil.SetFocus ();
		m_Fil.SetSel (0, -1);
		AngebotCore->SetFil (Fil->fil.fil);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Filiale %hd nicht gefunden"),Fil->fil.fil);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Fil->fil, &fil_null, sizeof (FIL));
		memcpy (&FilAdr->adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Fil.SetFocus ();
		m_Fil.SetSel (0, -1);
		return FALSE;
	}
	return FALSE;
}

BOOL CAuftragPage::ReadAng ()
{
	long angnr;
	BOOL Enable = FALSE;
	angnr = Partyk->partyk.ang;
	memcpy (&Partyk->partyk, &partyk_null, sizeof (PARTYK));
	AngebotCore->SetPersIdx (0);
	Form.Get ();
	if (Mdn->mdn.mdn == 0)
	{
		CString Error;
		Error.Format (_T("Mandant 0 nicht erlaubt"));
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Mdn->mdn, &mdn_null, sizeof (MDN));
		memcpy (&MdnAdr->adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Fil.EnableWindow (FALSE);
		m_FilChoice.EnableWindow (FALSE);
		m_Mdn.SetFocus ();
		m_Mdn.SetSel (0, -1);
		return FALSE;
	}
/*
	if (angnr != Partyk->partyk.ang)
	{
		AngebotCore->FreeNumber (angnr);
		MustCreate = FALSE;
	}
*/
	AngebotCore->IsVatFull = FALSE;
	AngebotCore->SetAngVk (0.0);
	AngebotCore->SetAngGew (0.0);
	AngebotCore->SetOSGPrice (0.0);
	AngebotCore->SetOSSPrice (0.0);
	CFormTab *CalcForm = AngebotCore->GetCalcForm ();
	if (CalcForm != NULL)
	{
		CalcForm->Show ();
	}
	Partyk->partyk.mwst_voll = FALSE;
	Partyk->beginwork ();
	Partyk->partyk.mdn = Mdn->mdn.mdn;
	Partyk->partyk.fil = Fil->fil.fil;
	if (Partyk->dbreadfirst () == 0 && (Partyk->partyk.ang_stat == CAngebotCore::POrder ||
										Partyk->partyk.ang_stat == CAngebotCore::PPrinted ||
										Partyk->partyk.ang_stat == CAngebotCore::PFacturated))
	{
		if (Partyk->partyk.ang_stat == CAngebotCore::PFacturated)
		{

			if (MessageBox (_T("F�r diese Angebot wurde schon eine Rechnung gedruckt\n")
				            _T("Rechnung nachdrucken ?"), _T("Hinweis"), 
							 MB_YESNO | MB_ICONQUESTION) == IDYES)
			{
				Kun->kun.kun = Partyk->partyk.kun;
				Form.Show ();
				ReadKun ();
				HeadGrid.Enable (FALSE);
				DataGrid.Enable (TRUE);
                OnRePrintRech ();
				HeadGrid.Enable (TRUE);
				DataGrid.Enable (FALSE);
//				m_List.EnableWindow (FALSE);
				return FALSE;
			}
			else
			{
				m_AngebotNr.SetFocus ();
				m_AngebotNr.SetSel (0, -1);
				return FALSE;
			}
		}
		else
		{
			m_List.EnableWindow ();
		}
		MustCreate = FALSE;
		AngebotCore->SetCurrentAnzPers (Partyk->partyk.anz_pers);
/*
		if (Partyk->partyk.ang_stat == CAngebotCore::PFacturated)
		{
			AfxMessageBox (_T("Der Auftrag wurde schon abgeschlossen"));
			m_AngebotNr.SetFocus ();
			return FALSE;
		}

		if (Partyk->partyk.ang_stat != CAngebotCore::POrder && 
			Partyk->partyk.ang_stat != CAngebotCore::PPrinted)
		{
			AfxMessageBox (_T("Auftrag nicht gefunden"));
			m_AngebotNr.SetFocus ();
			return FALSE;
		}
*/
		int idx = AngebotCore->GetPersList ()->FindPosition (Partyk->partyk.pers);
		if (idx == -1) idx = 0;
		AngebotCore->SetPersIdx (idx);
		if (Partyk->partyk.pauschal_vk == Partyk->partyk.ang_vk)
		{
			Partyk->partyk.pauschal_vk = 0.0;
			Partyk->partyk.einzel_vk_pausch = 0.0;
		}
		Kun->kun.kun = Partyk->partyk.kun;
		Form.Show ();
		ReadKun ();
		ReadPositions ();
		Kun->kun.adr1 = Partyk->partyk.adr;
		m_Kun.SetReadOnly ();
		m_Kun.ModifyStyle (WS_TABSTOP, 0, 0);
		Enable = FALSE;
	}
	else
	{
		MessageBox (_T("Auftrag nicht gefunden"), NULL, MB_OK | MB_ICONERROR);
		m_AngebotNr.SetFocus ();
		return FALSE;
	}
	HeadGrid.Enable (FALSE);
	DataGrid.Enable (TRUE);
	m_KunChoice.EnableWindow (Enable);
	ShowCalcView ();
	TestKun = TRUE;
	return TRUE;
}

BOOL CAuftragPage::ReadPositions ()
{
	PARTYP *p;
	PARTYP **it;
	int i = 0;
	int ret = TRUE;
	CDataCollection<PARTYP *> *List;
 
	m_List.EditRow = 0;
 	List = AngebotCore->GetList ();
	ret = AngebotCore->ReadPositions ();
	List->Start ();
	while ((it = List->GetNext ()) != NULL)
	{
		p = *it;
		FillList.InsertItem (i, 0);
		CString Item;
		Item.Format (_T("%hd"), p->posi);
		FillList.SetItemText (Item.GetBuffer (), i, m_List.POSPOS);
		Item.Format (_T("%hd"), p->ls_pos_kz);
		FillList.SetItemText (Item.GetBuffer (), i, m_List.POSSTAT);
		Item.Format (_T("%.0lf"), p->a);
		FillList.SetItemText (Item.GetBuffer (), i, m_List.POSA);
		memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
		A_bas->a_bas.a = p->a;
		A_bas->dbreadfirst ();
		FillList.SetItemText (A_bas->a_bas.a_bz1, i, m_List.POSABZ1);
		Item.Format (_T("%.3lf"), p->auf_me);
		FillList.SetItemText (Item.GetBuffer (), i, m_List.POSME);
		Item.Format (_T("%.3lf"), p->inh);
		if (p->me_einh == 2)
		{
//			Item.Format (_T("%.3lf"), p->lief_me);
			Item.Format (_T("%.3lf"), p->inh);
		}
		else
		{
			Item.Format (_T("%.3lf"), p->a_gew);
		}
		FillList.SetItemText (Item.GetBuffer (), i, m_List.POSBASINH);
		Item.Format (_T("%.3lf"), p->lief_me);
		FillList.SetItemText (Item.GetBuffer (), i, m_List.POSBASME);
		FillList.SetItemText (p->lief_me_bz, i, m_List.POSBASEINH);
		AngebotCore->SetA (p->a);
	    m_List.FillEinhCombo ();
		AngebotCore->SetMeEinhKun (p->me_einh_kun);
		if (AngebotCore->GetAktEinh ())
		{
			Item.Format (_T("%hd %s"), p->me_einh_kun,
									   AngebotCore->GetMeBz().GetBuffer ());
			AngebotCore->FindEinhCombo (&Item, &m_List.EinhCombo);
			FillList.SetItemText (Item.GetBuffer (), 
				i,  m_List.POSEINH);
		}
		FillList.SetItemText (p->gebinde_text, i, m_List.POSGEBINDE);
		Item.Format (_T("%.4lf"), p->auf_vk_euro);
		FillList.SetItemText (Item.GetBuffer (), i, m_List.POSVK);
		Item.Format (_T("%.2lf"), p->auf_vk_brutto);
		FillList.SetItemText (Item.GetBuffer (), i, m_List.POSVKBRUTTO);
		Item.Format (_T("%.2lf"), p->pos_vk);
		FillList.SetItemText (Item.GetBuffer (), i, m_List.POSVKME);
		CString SinglePrice;
		AngebotCore->GetCheckBoxValue (p->preinzel_kz, SinglePrice);
		FillList.SetItemText (SinglePrice.GetBuffer (), i, m_List.POSEINZELPR);
		i ++;
	}
	Partyk->partyk.ang_gew = AngebotCore->GetAngGew ();
	Partyk->partyk.ang_vk = AngebotCore->GetAngVk ();
	Form.Show ();
	return TRUE;
}

BOOL CAuftragPage::WriteAng ()
{
	Form.Get ();
	m_List.StopEnter ();
	if (Partyk->partyk.ang_stat < 2)
	{
		Partyk->partyk.ang_stat = 1;
	}
	AngebotCore->WriteAng ();
	if (Partyk->partyk.ls > 0l)
	{
		AngebotCore->ExportLief ();
	}
	else if (Partyk->partyk.auf > 0l)
	{
		AngebotCore->ExportAuftrag ();
	}
	m_List.DeleteAllItems ();
	InitForm ();
	AngebotCore->GetList ()->DestroyElements ();
	AngebotCore->GetDelList ()->Clear ();
	AngebotCore->IsVatFull = FALSE;
	Partyk->partyk.mwst_voll = FALSE;
	Form.Show ();
	return TRUE;
}

BOOL CAuftragPage::ReadKun ()
{
	memcpy (&Kun->kun, &kun_null, sizeof (KUN));
	memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
	Form.GetFormField (&m_Mdn)->Get ();
	Form.GetFormField (&m_Kun)->Get ();
	if (Mdn->mdn.mdn == 0)
	{
		return FALSE;
	}
	Kun->kun.mdn = Mdn->mdn.mdn;
	if (Kun->dbreadfirst () == 0)
	{
		if (!KunDiverse->IsAdrDiverse (Partyk->partyk.adr))
		{
			KunAdr->adr.adr = Kun->kun.adr1;
			if (Partyk->partyk.lief_adr == 0l)
			{
				Partyk->partyk.lief_adr = Kun->kun.adr1; 
			}
			KunAdr->dbreadfirst ();
			if (KunDiverse->IsDiverse ())
			{
				Kun->kun.adr1 = KunDiverse->GenAdr ();
				KunAdr->adr.adr = Kun->kun.adr1;
				Partyk->partyk.adr = Kun->kun.adr1; 
				if (Partyk->partyk.lief_adr == 0l)
				{
					Partyk->partyk.lief_adr = Kun->kun.adr1; 
				}
				OnStnClickedAddress ();
				m_Kun.EnableWindow (FALSE);
				m_KunChoice.EnableWindow (FALSE);
			}
		}
		else
		{
			Kun->kun.adr1 = Partyk->partyk.adr;
			KunAdr->adr.adr = Partyk->partyk.adr;
			KunAdr->dbreadfirst ();
		}

		Form.Show ();
		m_Kun.SetFocus ();
		m_Kun.SetSel (0, -1);
		AngebotCore->SetKun (Kun->kun.kun);
		return TRUE;
	}
	else
	{
		CString Error;
		Error.Format (_T("Kunde %ld nicht gefunden"),Kun->kun.kun);
		MessageBox (Error.GetBuffer (), NULL, MB_OK | MB_ICONERROR);
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
		Form.Show ();
		m_Kun.SetFocus ();
		m_Kun.SetSel (0, -1);
	}
	return FALSE;
}

void CAuftragPage::OnMdnChoice ()
{
	if (ChoiceMdn == NULL)
	{
		ChoiceMdn = new CChoiceMdn (this);
	    ChoiceMdn->IsModal = TRUE;
		ChoiceMdn->CreateDlg ();
	}

    ChoiceMdn->SetDbClass (A_bas);
	ChoiceMdn->DoModal();
    if (ChoiceMdn->GetState ())
    {
		  CMdnList *abl = ChoiceMdn->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Mdn->mdn, &mdn_null, sizeof (MDN));
		  memcpy (&MdnAdr->adr, &adr_null, sizeof (ADR));
		  Mdn->mdn.mdn = abl->mdn;
		  Form.Show ();
		  ReadMdn ();
/*
		  if (Mdn.dbreadfirst () == 0)
		  {
			  MdnAdr.adr.adr = Mdn.mdn.adr;
			  MdnAdr.dbreadfirst ();
		  }
		  if (Mdn.dbreadfirst () == 0)
		  {
		  }
		  Form.Show ();
*/
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	else
	{
		  m_Mdn.SetSel (0, -1, TRUE);
		  m_Mdn.SetFocus ();
	}
	delete ChoiceMdn;
	ChoiceMdn = NULL;
}


void CAuftragPage::OnFilChoice ()
{
	Form.Get ();
	if (ChoiceFil == NULL)
	{
		ChoiceFil = new CChoiceFil (this);
	    ChoiceFil->IsModal = TRUE;
		ChoiceFil->m_Mdn = Mdn->mdn.mdn;
		ChoiceFil->CreateDlg ();
	}

    ChoiceFil->SetDbClass (A_bas);
	ChoiceFil->DoModal();
    if (ChoiceFil->GetState ())
    {
		  CFilList *abl = ChoiceFil->GetSelectedText (); 
		  if (abl == NULL) return;
          memcpy (&Fil->fil, &fil_null, sizeof (FIL));
		  memcpy (&FilAdr->adr, &adr_null, sizeof (ADR));
		  Fil->fil.fil = abl->fil;
		  Form.Show ();
//		  ReadFil ();
		  m_Fil.SetSel (0, -1, TRUE);
		  m_Fil.SetFocus ();
          PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	else
	{
		  m_Fil.SetSel (0, -1, TRUE);
		  m_Fil.SetFocus ();
	}
	delete ChoiceFil;
	ChoiceFil = NULL;
}

void CAuftragPage::OnPartykChoice ()
{
	CString MdnNr;
	m_Mdn.GetWindowText (MdnNr);
//    ChoiceStat = TRUE;
	if (ChoicePartyk != NULL)
	{
		ChoicePartyk->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoicePartyk == NULL)
	{
		ChoicePartyk = new CChoicePartyk (this);
	    ChoicePartyk->IsModal = TRUE;
		ChoicePartyk->IdArrDown = IDI_HARROWDOWN;
		ChoicePartyk->IdArrUp   = IDI_HARROWUP;
		ChoicePartyk->IdArrNo   = IDI_HARROWNO;
		ChoicePartyk->HideFilter = FALSE;
//		ChoicePartyk->HideEnter = FALSE;
		ChoicePartyk->HideOK    = FALSE;
		ChoicePartyk->SetQueryString (QueryPartyk);
		ChoicePartyk->FirstRead = FirstReadPartyk;
//		ChoicePartyk->Where = _T("and ang_stat > 0 and ang_stat < 3");
		ChoicePartyk->Where = _T("and ang_stat > 0");
		ChoicePartyk->SetDlgSize (840, 400);
		ChoicePartyk->CreateDlg ();
	}

    ChoicePartyk->SetDbClass (Partyk);
//	ChoiceKun->SearchText = Search;
	if (ChoicePartyk->IsModal)
	{
			ChoicePartyk->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoicePartyk->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;
		ChoicePartyk->MoveWindow (&rect);
		ChoicePartyk->SetListFocus ();
		return;
	}
    if (ChoicePartyk->GetState ())
    {
	  	  QueryPartyk = ChoicePartyk->GetQueryString ();
		  CPartykList *abl = ChoicePartyk->GetSelectedText (); 
		  if (abl == NULL) return;
		  Form.Get ();
		  AngebotCore->FreeNumber (Partyk->partyk.ang);
		  memcpy (&Partyk->partyk, &partyk_null, sizeof (PARTYK));
		  AngebotCore->SetPersIdx (0);
		  Partyk->partyk.mdn = abl->mdn;
		  Partyk->partyk.ang = abl->ang;
		  Mdn->mdn.mdn = abl->mdn;
		  Form.Show ();
		  MustCreate = FALSE;
          m_AngebotNr.SetSel (0, -1, TRUE);
		  m_AngebotNr.SetFocus ();
		  MustCreate = FALSE;
		  PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	else
	{
		  Form.GetFormField(&m_Mdn)->Get ();
		  if (Mdn->mdn.mdn == 0)
		  {
			m_Mdn.SetSel (0, -1, TRUE);
			m_Mdn.SetFocus ();
		  }
		  else
		  {
			  m_AngebotNr.SetSel (0, -1, TRUE);
			  m_AngebotNr.SetFocus ();
		  }
//	 	  ChoiceStat = FALSE;	
	}
	FirstReadPartyk = ChoicePartyk->FirstRead;
    delete ChoicePartyk;
    ChoicePartyk = NULL;
}

void CAuftragPage::OnKunChoice ()
{
	CString MdnNr;
	if (!m_KunChoice.IsWindowEnabled ())
	{
		return;
	}
	m_Mdn.GetWindowText (MdnNr);
//    ChoiceStat = TRUE;
	if (ChoiceKun != NULL)
	{
		ChoiceKun->ShowWindow (SW_SHOWNORMAL);
		return;
	}
	if (ChoiceKun == NULL)
	{
		ChoiceKun = new CChoiceKun (this);
	    ChoiceKun->IsModal = FALSE;
		ChoiceKun->m_Mdn =  _tstoi (MdnNr);
		ChoiceKun->IdArrDown = IDI_HARROWDOWN;
		ChoiceKun->IdArrUp   = IDI_HARROWUP;
		ChoiceKun->IdArrNo   = IDI_HARROWNO;
		ChoiceKun->HideFilter = FALSE;
		ChoiceKun->HideEnter = FALSE;
		ChoiceKun->HideOK    = FALSE;
		ChoiceKun->CreateDlg ();
	}

    ChoiceKun->SetDbClass (Kun);
//	ChoiceKun->SearchText = Search;
	if (ChoiceKun->IsModal)
	{
			ChoiceKun->DoModal();
	}
	else
	{
		CRect mrect;
		GetParent ()->GetWindowRect (&mrect);
		CRect rect;
		ChoiceKun->GetWindowRect (&rect);
		int scx = GetSystemMetrics (SM_CXSCREEN);
		int scy = GetSystemMetrics (SM_CYSCREEN);
		rect.top = 50;
		rect.right = scx - 2;
		rect.left = rect.right - 300;
		rect.bottom = scy - 50;
		ChoiceKun->MoveWindow (&rect);
		ChoiceKun->SetListFocus ();
		return;
	}
    if (ChoiceKun->GetState ())
    {
		  CKunList *abl = ChoiceKun->GetSelectedText (); 
		  if (abl == NULL) return;
		  memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		  memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
		  Kun->kun.mdn = abl->mdn;
		  Kun->kun.kun = abl->kun;

		  if (Kun->dbreadfirst () == 0)
		  {
			  KunAdr->adr.adr = Kun->kun.adr1;
			  KunAdr->dbreadfirst ();
		  }
		  Form.Show ();
          m_Kun.SetSel (0, -1, TRUE);
		  m_Kun.SetFocus ();
		  PostMessage (WM_KEYDOWN, VK_RETURN, 0l);
    }
	else
	{
          m_Kun.SetSel (0, -1, TRUE);
		  m_Kun.SetFocus ();
//	 	  ChoiceStat = FALSE;	
	}
}

void CAuftragPage::OnKunSelected ()
{
	if (ChoiceKun == NULL) return;
    CKunList *abl = ChoiceKun->GetSelectedText (); 
    if (abl == NULL) return;
    memcpy (&Kun->kun, &kun_null, sizeof (KUN));
    Kun->kun.kun = abl->kun;
    Form.Show ();
	ReadKun ();
	if (ChoiceKun->FocusBack)
	{
		ChoiceKun->SetListFocus ();
	}
}

void CAuftragPage::OnKunCanceled ()
{
	ChoiceKun->ShowWindow (SW_HIDE);
    m_Kun.SetSel (0, -1, TRUE);
    m_Kun.SetFocus ();
}

void CAuftragPage::OnEnSetfocusAngebotNr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	if (!MustCreate)
	{
		return;
	}
/*
	Partyk->partyk.ang = AngebotCore->CreateNumber ();
	if (Partyk->partyk.ang == 0l)
	{
		AfxMessageBox (_T("Es konnte keine Angebotsnummer generiert werden"));
		m_AngebotNr.SetFocus ();
		return;
	}
	MustCreate = FALSE;
	IsCreated = TRUE;
	Form.Show ();
*/
}

void CAuftragPage::OnExit ()
{
	Partyk->rollbackwork ();
	AngebotCore->FreeNumber ();
	if (m_List.ChoiceA != NULL)
	{
		delete m_List.ChoiceA;
		m_List.ChoiceA = NULL;
	}
}

void CAuftragPage::OnBack ()
{
	if (IsWindowVisible ())
	{
		OnF5 ();
	}
}

void CAuftragPage::OnSave ()
{
	if (IsWindowVisible ())
	{
		OnF12 ();
	}
}

void CAuftragPage::OnDelete ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		m_List.OnKeyD (VK_F7);
	}
}

BOOL CAuftragPage::TestInsertDelete ()
{
	if (IsWindowVisible () && !m_Mdn.IsWindowEnabled ())
	{
		return TRUE;
	}
	return FALSE;
}


void CAuftragPage::OnCopy ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Copy ();
	}
}

void CAuftragPage::OnPaste ()
{
	CWnd *Control = GetFocus ();
	if (Control->IsKindOf( RUNTIME_CLASS( CEdit )))
	{
		((CEdit *) Control)->Paste ();
	}
}

BOOL CAuftragPage::TestPaste ()
{
	return CStrFuncs::CanPaste ();
}

void CAuftragPage::OnInsert ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		m_List.OnKeyD (VK_F6);
	}
}

void CAuftragPage::OnDeleteAng ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		if (MessageBox (_T("Angebot komplett l�schen l�schen ?"), NULL, 
						 MB_YESNO | MB_ICONQUESTION) ==
			IDYES)
		{
			AngebotCore->DeleteAng ();
			m_List.DeleteAllItems ();
			AngebotCore->GetList ()->DestroyElements ();
			AngebotCore->GetDelList ()->Clear ();
			AngebotCore->IsVatFull = FALSE;
			Partyk->partyk.mwst_voll = FALSE;
			Form.Show ();
			memcpy (&Kun->kun, &kun_null, sizeof (KUN));
			memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
			HeadGrid.Enable (TRUE);
			DataGrid.Enable (FALSE);
			MustCreate = TRUE;
			m_List.StopEnter ();
			m_AngebotNr.SetFocus ();
		}
	}
}


BOOL CAuftragPage::TestDeleteAng ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		return TRUE;
	}
	return FALSE;
}

void CAuftragPage::OnPrintAuftrag ()
{
	CPrintMail dlg;
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		m_List.StopEnter ();
		INT_PTR ret = dlg.DoModal ();
		if (ret != IDOK)
		{
			return;
		}
		if (dlg.Result () == CPrintMail::Print)
		{
			if (!AngebotCore->PrintAuftrag ())
			{
				MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
					MB_OK | MB_ICONERROR);
			}
		}
		else
		{
			if (!AngebotCore->MailAuftrag ())
			{
				MessageBox (_T("Email kann nicht gesendet werden"), NULL, 
					MB_OK | MB_ICONERROR);
			}
		}
		m_List.DeleteAllItems ();
		AngebotCore->GetList ()->DestroyElements ();
		AngebotCore->GetDelList ()->Clear ();
		AngebotCore->IsVatFull = FALSE;
		Partyk->partyk.mwst_voll = FALSE;
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
		HideCalcView ();
		HeadGrid.Enable (TRUE);
		DataGrid.Enable (FALSE);
		MustCreate = TRUE;
		m_AngebotNr.SetFocus ();
	}
}

BOOL CAuftragPage::TestPrintAuftrag ()
{
	BOOL ret = FALSE;
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		Form.GetFormField (&m_Ls)->Get ();
		if (Partyk->partyk.ls == 0l)
		{
			ret = TRUE;
		}
	}
	return ret;
}


void CAuftragPage::OnPrintLief ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		CWnd *Focus = GetFocus ();
		if (!AngebotCore->PrintLief ())
		{
			MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
				MB_OK | MB_ICONERROR);
		}
		Partyk->beginwork ();
		MustCreate = TRUE;
		if (Focus != NULL)
		{
			Focus->SetFocus ();
		}
	}
}


BOOL CAuftragPage::TestPrintLief ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		return TRUE;
	}
	return FALSE;
}

void CAuftragPage::OnPrintRech ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		Form.Get ();
		if (Partyk->partyk.ang_stat < AngebotCore->PPrinted)
		{
			if (MessageBox (_T("Der Lieferschein wurde noch nicht gedruckt!  \nTrotzdem jetzt die Rechnung drucken ?"), _T(""), 
						 MB_YESNO | MB_ICONQUESTION) ==	IDNO)
			{
				return;
			}
		}

		m_List.StopEnter ();
		if (!AngebotCore->PrintRech ())
		{
			MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
				MB_OK | MB_ICONERROR);
			return;
		}
		m_List.DeleteAllItems ();
		AngebotCore->GetList ()->DestroyElements ();
		AngebotCore->GetDelList ()->Clear ();
		InitForm ();
		AngebotCore->IsVatFull = FALSE;
		Partyk->partyk.mwst_voll = FALSE;
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
		HideCalcView ();
		HeadGrid.Enable (TRUE);
		DataGrid.Enable (FALSE);
		MustCreate = TRUE;
		m_AngebotNr.SetFocus ();
	}
}

void CAuftragPage::OnRePrintRech ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		Form.Get ();
		if (!AngebotCore->RePrintRech ())
		{
			MessageBox (_T("Druck kann nicht gestartet werden"), NULL, 
				MB_OK | MB_ICONERROR);
			return;
		}
		m_List.DeleteAllItems ();
		AngebotCore->GetList ()->DestroyElements ();
		AngebotCore->GetDelList ()->Clear ();
		InitForm ();
		AngebotCore->IsVatFull = FALSE;
		Partyk->partyk.mwst_voll = FALSE;
		memcpy (&Kun->kun, &kun_null, sizeof (KUN));
		memcpy (&KunAdr->adr, &adr_null, sizeof (ADR));
		HideCalcView ();
		HeadGrid.Enable (TRUE);
		DataGrid.Enable (FALSE);
		MustCreate = TRUE;
		m_AngebotNr.SetFocus ();
	}
}

BOOL CAuftragPage::TestPrintRech ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		return TRUE;
	}
	return FALSE;
}

void CAuftragPage::OnPosText ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		CWnd *control = GetFocus ();
		m_List.OnPosText ();
		if (control != NULL)
		{
			control->SetFocus ();
		}
	}
}

BOOL CAuftragPage::TestPosText ()
{
	if (IsWindowVisible () && !m_Mdn.IsWindowEnabled ())
	{
		if (m_List.GetItemCount () > 0)
		{
			return TRUE;
		}
	}
	return FALSE;
}

void CAuftragPage::OnKopfText ()
{
	CPosTextDialog dlg;
	CString Text;

	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		CWnd *control = GetFocus ();

		Form.Get ();
//		AngebotCore->WriteAng ();
		Partyk->commitwork ();
		if (Partyk->partyk.kopf_txt == 0l)
		{
			Partyk->partyk.kopf_txt = AngebotCore->GenLsTxtNr ();
		}
		Partyk->beginwork ();
		if (Partyk->partyk.kopf_txt == 0l)
		{
			return;
		}

		AngebotCore->ReadText (Partyk->partyk.kopf_txt, Text);
		dlg.SetTitle (_T("Kopftext"));
		dlg.SetTextNr (Partyk->partyk.kopf_txt);
		dlg.SetText(Text);
		dlg.SetTextType (CPosTextDialog::HeadFoot);
		if (dlg.DoModal () == IDOK)
		{
			Partyk->partyk.kopf_txt = dlg.GetTextNr ();
			Text = dlg.GetText ();
			AngebotCore->WriteText (Partyk->partyk.kopf_txt, Text);
		}
		if (control != NULL)
		{
			control->SetFocus ();
		}
	}
}

BOOL CAuftragPage::TestKopfText ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
			return TRUE;
	}
	return FALSE;
}


void CAuftragPage::OnFussText ()
{
	CPosTextDialog dlg;
	CString Text;

	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
		CWnd *control = GetFocus ();

//		Form.Get ();
//		AngebotCore->WriteAng ();
		Partyk->commitwork ();
		if (Partyk->partyk.fuss_txt == 0l)
		{
			Partyk->partyk.fuss_txt = AngebotCore->GenLsTxtNr ();
		}
		Partyk->beginwork ();
		if (Partyk->partyk.fuss_txt == 0l)
		{
			return;
		}

		AngebotCore->ReadText (Partyk->partyk.fuss_txt, Text);
		dlg.SetTitle (_T("Fu�text"));
		dlg.SetTextNr (Partyk->partyk.fuss_txt);
		dlg.SetText(Text);
		dlg.SetTextType (CPosTextDialog::HeadFoot);
		if (dlg.DoModal () == IDOK)
		{
			Partyk->partyk.fuss_txt = dlg.GetTextNr ();
			Text = dlg.GetText ();
			AngebotCore->WriteText (Partyk->partyk.fuss_txt, Text);
		}
		if (control != NULL)
		{
			control->SetFocus ();
		}
	}
}

BOOL CAuftragPage::TestFussText ()
{
	if (IsWindowVisible () &&!m_Mdn.IsWindowEnabled ())
	{
			return TRUE;
	}
	return FALSE;
}


// Messagehandler Klassen

CAuftragPage::CExitMessage::CExitMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_APP_EXIT); 
			this->p = p;
}

void CAuftragPage::CExitMessage::Run ()
{
	p->OnExit ();
}

CAuftragPage::CBackMessage::CBackMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_BACK); 
			this->p = p;
}

void CAuftragPage::CBackMessage::Run ()
{
			p->OnBack ();
}

CAuftragPage::CDataBackMessage::CDataBackMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_DATA_BACK); 
			this->p = p;
}

void CAuftragPage::CDataBackMessage::Run ()
{
			p->OnDataBack ();
}

CAuftragPage::CSaveMessage::CSaveMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_FILE_SAVE); 
			this->p = p;
}

void CAuftragPage::CSaveMessage::Run ()
{
	p->OnSave ();
}

CAuftragPage::CDeleteMessage::CDeleteMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_DELETE); 
			this->p = p;
}

void CAuftragPage::CDeleteMessage::Run ()
{
	p->OnDelete ();
}

BOOL CAuftragPage::CDeleteMessage::Test ()
{
	return p->TestInsertDelete ();
}

CAuftragPage::CInsertMessage::CInsertMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_INSERT); 
			this->p = p;
}

void CAuftragPage::CInsertMessage::Run ()
{
	p->OnInsert ();
}

BOOL CAuftragPage::CInsertMessage::Test ()
{
	return p->TestInsertDelete ();
}

CAuftragPage::CCopyMessage::CCopyMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_EDIT_COPY); 
			this->p = p;
}

void CAuftragPage::CCopyMessage::Run ()
{
	p->OnCopy ();
}

CAuftragPage::CPasteMessage::CPasteMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_EDIT_PASTE); 
			this->p = p;
}

void CAuftragPage::CPasteMessage::Run ()
{
	p->OnPaste ();
}

BOOL CAuftragPage::CPasteMessage::Test ()
{
	return p->TestPaste ();
}


CAuftragPage::CDeleteAngMessage::CDeleteAngMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_DELETE_ANG); 
			this->p = p;
}

void CAuftragPage::CDeleteAngMessage::Run ()
{
	p->OnDeleteAng ();
}

BOOL CAuftragPage::CDeleteAngMessage::Test ()
{
	return p->TestDeleteAng ();
}

CAuftragPage::CPrintAuftragMessage::CPrintAuftragMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_PRINT_AUFTRAG); 
			this->p = p;
}

void CAuftragPage::CPrintAuftragMessage::Run ()
{
	p->OnPrintAuftrag ();
}

BOOL CAuftragPage::CPrintAuftragMessage::Test ()
{
	return p->TestPrintAuftrag ();
}

CAuftragPage::CPrintLiefMessage::CPrintLiefMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_PRINT_LIEF); 
			this->p = p;
}

void CAuftragPage::CPrintLiefMessage::Run ()
{
	p->OnPrintLief ();
}

BOOL CAuftragPage::CPrintLiefMessage::Test ()
{
	return p->TestPrintLief ();
}


CAuftragPage::CPrintRechMessage::CPrintRechMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_PRINT_RECH); 
			this->p = p;
}

void CAuftragPage::CPrintRechMessage::Run ()
{
	p->OnPrintRech ();
}

BOOL CAuftragPage::CPrintRechMessage::Test ()
{
	return p->TestPrintRech ();
}

CAuftragPage::CCalcViewMessage::CCalcViewMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_CALC_VIEW); 
			this->p = p;
}

void CAuftragPage::CCalcViewMessage::Run ()
{
	p->OnCalcView ();
}

CAuftragPage::CPosTextMessage::CPosTextMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_POS_TEXT); 
			this->p = p;
}

void CAuftragPage::CPosTextMessage::Run ()
{
	p->OnPosText ();
}

BOOL CAuftragPage::CPosTextMessage::Test ()
{
	return p->TestPosText ();
}

CAuftragPage::CKopfTextMessage::CKopfTextMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_KOPF_TEXT); 
			this->p = p;
}

void CAuftragPage::CKopfTextMessage::Run ()
{
	p->OnKopfText ();
}

BOOL CAuftragPage::CKopfTextMessage::Test ()
{
	return p->TestKopfText ();
}


CAuftragPage::CFussTextMessage::CFussTextMessage (CAuftragPage *p) : CRunMessage ()
{
	        SetId (ID_FUSS_TEXT); 
			this->p = p;
}

void CAuftragPage::CFussTextMessage::Run ()
{
	p->OnFussText ();
}

BOOL CAuftragPage::CFussTextMessage::Test ()
{
	return p->TestFussText ();
}


void CAuftragPage::OnBnClickedBkun()
{
	// TODO: Add your control notification handler code here
	if (m_BKun.GetCheck () == BST_CHECKED)
	{
		AngebotCore->SetKunFil (0);
	}
	else
	{
		AngebotCore->SetKunFil (1);
	}
}

void CAuftragPage::OnBnClickedBfil()
{
	// TODO: Add your control notification handler code here
	if (m_BFil.GetCheck () == BST_CHECKED)
	{
		AngebotCore->SetKunFil (1);
	}
	else
	{
		AngebotCore->SetKunFil (0);
	}
}


void CAuftragPage::OnEnKillfocusPauschalVk()
{
	// TODO: Add your control notification handler code here
	double value = Partyk->partyk.pauschal_vk;
	Form.Get ();
	if (value != Partyk->partyk.pauschal_vk)
	{
		AngebotCore->SetCalculatePauschSingle (FALSE);
		AngebotCore->Calculate ();
	}
}

void CAuftragPage::OnEnKillfocusServicePauschVk()
{
	// TODO: Add your control notification handler code here
	double value = Partyk->partyk.service_pausch_vk;
	value = CStrFuncs::RoundToDouble (value, 2);
	Form.Get ();
	if (value != Partyk->partyk.service_pausch_vk)
	{
		AngebotCore->SetCalculatePauschSingle (FALSE);
		AngebotCore->Calculate ();
	}
}

void CAuftragPage::OnEnKillfocusAnzPers()
{
	// TODO: Add your control notification handler code here
	AngebotCore->Calculate ();
}

void CAuftragPage::OnBnClickedMwstVoll()
{
	if (m_MwstVoll.GetCheck () == BST_CHECKED)
	{
		AngebotCore->SetVatToBrutto ();
		m_List.RefreshPositions ();
	}
	else
	{
		if (!AngebotCore->GetABasList ()->HasService ())
		{
			AngebotCore->SetVatToNetto ();
			m_List.RefreshPositions ();
		}
		else
		{
			m_MwstVoll.SetCheck (BST_CHECKED);
		}
	}
/*
	if (AngebotCore->IsVatFull)
	{
		m_MwstVoll.SetCheck (BST_CHECKED);
	}

	else
	{
		m_MwstVoll.SetCheck (BST_UNCHECKED);
	}
*/
}

BOOL CAuftragPage::KunOk()
{
	BOOL ret = AngebotCore->KunOk ();
	if (!ret)
	{
		if (Kun->kun.kun == 0l)
		{
			AfxMessageBox (_T("Kunde darf nicht 0 sein"));
		}
		else
		{
			AfxMessageBox (_T("Kundennummer nicht gefunden"));
			m_Kun.SetFocus ();
		}
		m_Kun.SetFocus ();
	}
	return ret;
}

void CAuftragPage::OnEnKillfocusKun()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CWnd *control = GetFocus ();
	if (TestKun && control != &m_KunChoice)
	{
		Form.Get ();
        if (Kun->kun.kun == 0l)
		{
			AfxMessageBox (_T("Kunde darf nicht 0 sein"));
			m_Kun.SetFocus ();
		}
		Kun->kun.mdn = Mdn->mdn.mdn;
		if (Kun->dbreadfirst () != 0)
		{
			m_Kun.SetFocus ();
		}
		else
		{
			KunAdr->adr.adr = Kun->kun.adr1;
			KunAdr->dbreadfirst ();
			Form.Show ();
		}
	}
}

void CAuftragPage::OnEnKillfocusEinzelVkPausch()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	double value = Partyk->partyk.einzel_vk_pausch;
	Form.Get ();
	if (value != Partyk->partyk.einzel_vk_pausch)
	{
		AngebotCore->SetCalculatePauschSingle (TRUE);
		AngebotCore->Calculate ();
	}
}

void CAuftragPage::OnBnClickedEinzelPreis()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	if (m_EinzelPreis.GetCheck () == BST_CHECKED &&
		Partyk->partyk.pauschal_vk != 0.0)
	{
		if (MessageBox (_T("Pauschalpreis zur�cksetzen ?"), NULL, 
						 MB_YESNO | MB_ICONQUESTION) ==
			IDYES)
		{
			Partyk->partyk.pauschal_vk = 0.0;
			Partyk->partyk.einzel_vk_pausch = 0.0;
		}
		else
		{
			m_EinzelPreis.SetCheck (BST_UNCHECKED);
		}
		Form.Show ();
	}
}

void CAuftragPage::OnStnClickedNewAngTyp()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CNewAngDialog dlg;
	CString Name;

	INT_PTR ret = dlg.DoModal ();
	if (ret == IDOK)
	{
		Name = dlg.GetName ();
		AngebotCore->InsertNewAngTyp (Name);
		CFormField *TypField = Form.GetFormField (&m_AngTyp);
		if (TypField != NULL)
		{
			AngebotCore->FillCombo (_T("ang_typ"), &TypField->ComboValues);
			TypField->FillComboBox ();
			Form.Show ();
		}
	}
}

void CAuftragPage::ShowCalcWnd ()
{
	struct CCreateContext cc;
	CRect rect;
	CWnd *focus = GetFocus ();
    memset (&cc, 0, sizeof (CCreateContext));
	cc.m_pNewViewClass = RUNTIME_CLASS (CCalcViewAuf);

	if (AngebotCore->GetCalcWithTitle ())
	{
		int cxscreen = GetSystemMetrics (SM_CXSCREEN);
		int x = cxscreen - CalcWidth;
		int y = GetSystemMetrics (SM_CYCAPTION);
		rect.left = x; 
		rect.top = y;
		rect.right = cxscreen;
		rect.bottom = y + CalcHeight;
	}
	else
	{
		GetParent ()->GetWindowRect (&rect);
		rect.left  = rect.right - CalcWidth;
		rect.top += 23;
		rect.bottom = rect.top + CalcHeight;
	}

	CalcFrame = new CCalcFrameWnd ();
	if (AngebotCore->GetCalcWithTitle ())
	{
		CalcFrame->SetWithTitle (TRUE);
	}
	cc.m_pCurrentFrame = CalcFrame;
//	cc.m_pCurrentDoc = AngebotCore->GetDocument ();
	CalcFrame->Create (NULL, _T("Kalkulationsansicht"), WS_THICKFRAME, rect, this, NULL, NULL, &cc);
//    CalcFrame->InitialUpdateFrame (AngebotCore->GetDocument (), TRUE);
    CalcFrame->InitialUpdateFrame (NULL, TRUE);
	CalcFrame->ShowWindow(SW_SHOWNORMAL);
	CalcFrame->UpdateWindow();


/*
	CSingleDocTemplate *temp = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CPartyWaDoc),
		RUNTIME_CLASS(CCalcFrameWnd),       // Haupt-SDI-Rahmenfenster
		RUNTIME_CLASS(CCalcView));
	if (temp != NULL)
	{
		CFrameWnd *CalcFrame = temp->CreateNewFrame (AngebotCore->GetDocument (), NULL);
		if (CalcFrame != NULL)
		{
			temp->InitialUpdateFrame (CalcFrame, AngebotCore->GetDocument (),TRUE);
		}
	}
*/

	if (focus != NULL)
	{
		focus->SetFocus ();
	}
}

void CAuftragPage::ShowCalcView ()
{
	if (MustCalcView)
	{
		if (CalcFrame == NULL)
		{
			ShowCalcWnd ();
		}
		else
		{
			CalcFrame->ShowWindow (SW_SHOWNORMAL);
		}
	}
}


void CAuftragPage::HideCalcView ()
{
	if (CalcFrame != NULL && CalcFrame->IsWindowVisible ())
	{
		CalcFrame->ShowWindow (SW_HIDE);
	}
}

void CAuftragPage::OnCalcView()
{
	// TODO: Add your command handler code here
	MustCalcView = !MustCalcView;
	if (m_Mdn.IsWindowEnabled ())
	{
		return;
	}
	if (MustCalcView)
	{
		ShowCalcView ();
		CFormTab *CalcForm = AngebotCore->GetCalcForm ();
		if (CalcForm != NULL)
		{
			CalcForm->Show ();
		}
	}
	else
	{
		HideCalcView ();
	}
}

void CAuftragPage::OnStnClickedAddress()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CAdressDialog dlg;
	CWnd * control = GetFocus ();

	dlg.SetCaptionText (_T("Adresse"));
	dlg.SetAdrTyp (CAdressDialog::KunAdrTyp);
	dlg.DoModal ();
	if (control != NULL)
	{
		control->SetFocus ();
	}
}

void CAuftragPage::OnStnClickedLiefAdr()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	CAdressDialog dlg;
	dlg.set_ChoiceVisible ();
	CWnd * control = GetFocus ();


	if (Partyk->partyk.lief_adr == 0l ||
		Partyk->partyk.lief_adr == Kun->kun.adr1)
	{
//			Partyk->partyk.lief_adr = KunDiverse->GenAdr ();
			dlg.set_LiefAdr (0);

	}
	else
	{
		dlg.set_LiefAdr (Partyk->partyk.lief_adr);
	}
	dlg.SetCaptionText (_T("Lieferadresse"));
	dlg.SetAdrTyp (CAdressDialog::LiefAdrTyp);
	dlg.DoModal ();
	Partyk->partyk.lief_adr = dlg.LiefAdr ();
	if (control != NULL)
	{
		control->SetFocus ();
	}
}

void CAuftragPage::OnEnSetfocusAnzPers()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.GetFormField (&m_AnzPers)->Get ();
	AngebotCore->SetCurrentAnzPers (Partyk->partyk.anz_pers);
}

void CAuftragPage::OnCalcPos()
{
	PARTYP **it;
	PARTYP *p;
	int i = 0;
	int ret = TRUE;
	CDataCollection<PARTYP *> *List;

	if (AngebotCore->CalcPos ())
	{
 
		m_List.EditRow = 0;
		m_List.DeleteAllItems ();
		m_List.StopEnter ();
 		List = AngebotCore->GetList ();
		List->Start ();
		while ((it = List->GetNext ()) != NULL)
		{
			p = *it;
			FillList.InsertItem (i, 0);
			CString Item;
			Item.Format (_T("%hd"), p->posi);
			FillList.SetItemText (Item.GetBuffer (), i, m_List.POSPOS);
			Item.Format (_T("%hd"), p->ls_pos_kz);
			FillList.SetItemText (Item.GetBuffer (), i, m_List.POSSTAT);
			Item.Format (_T("%.0lf"), p->a);
			FillList.SetItemText (Item.GetBuffer (), i, m_List.POSA);
			memcpy (&A_bas->a_bas, &a_bas_null, sizeof (A_BAS));
			A_bas->a_bas.a = p->a;
			A_bas->dbreadfirst ();
			FillList.SetItemText (A_bas->a_bas.a_bz1, i, m_List.POSABZ1);
			Item.Format (_T("%.3lf"), p->auf_me);
			FillList.SetItemText (Item.GetBuffer (), i, m_List.POSME);
			Item.Format (_T("%.3lf"), p->inh);
			FillList.SetItemText (Item.GetBuffer (), i, m_List.POSBASINH);
			if (p->me_einh == 2)
			{
				Item.Format (_T("%.3lf"), p->lief_me);
			}
			else
			{
				Item.Format (_T("%.3lf"), p->a_gew);
			}
			FillList.SetItemText (Item.GetBuffer (), i, m_List.POSBASME);
			FillList.SetItemText (p->lief_me_bz, i, m_List.POSBASEINH);
			AngebotCore->SetA (p->a);
			m_List.FillEinhCombo ();
			AngebotCore->SetMeEinhKun (p->me_einh_kun);
			if (AngebotCore->GetAktEinh ())
			{
				Item.Format (_T("%hd %s"), p->me_einh_kun,
										   AngebotCore->GetMeBz().GetBuffer ());
				AngebotCore->FindEinhCombo (&Item, &m_List.EinhCombo);
				FillList.SetItemText (Item.GetBuffer (), 
					i,  m_List.POSEINH);
			}
			FillList.SetItemText (p->gebinde_text, i, m_List.POSGEBINDE);
			Item.Format (_T("%.4lf"), p->auf_vk_euro);
			FillList.SetItemText (Item.GetBuffer (), i, m_List.POSVK);
			Item.Format (_T("%.2lf"), p->auf_vk_brutto);
			FillList.SetItemText (Item.GetBuffer (), i, m_List.POSVKBRUTTO);
			Item.Format (_T("%.2lf"), p->pos_vk);
			FillList.SetItemText (Item.GetBuffer (), i, m_List.POSVKME);
			i ++;
		}
		Partyk->partyk.ang_gew = AngebotCore->GetAngGew ();
		Partyk->partyk.ang_vk = AngebotCore->GetAngVk ();
		Form.Show ();
	}
}

void CAuftragPage::OnComments()
{
	CCommentDialog dlg;

	dlg.DoModal ();
}

void CAuftragPage::OnModifyPers()
{
	CPersDialog dlg;

	Form.Get ();
	PERS *Pers = AngebotCore->GetPersList ()->GetElementAt (AngebotCore->GetPersIdx());
	if (Pers != NULL)
	{
		dlg.SetPers (Pers);
		dlg.DoModal ();
		CFormField *PersField = Form.GetFormField (&m_Pers);
		AngebotCore->FillPers (&PersField->ComboValues);
		PersField->FillComboBox ();
		PersField->Show ();
	}
}

void CAuftragPage::OnBnClickedNurServicePreis()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	Form.Get ();
	AngebotCore->Calculate ();
	m_List.RefreshPositions ();
}

