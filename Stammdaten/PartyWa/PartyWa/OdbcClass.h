#ifndef _DBCLASS_DEF
#define  _DBCLASS_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "PropertieTable.h"

namespace ODBC
{
#ifndef MAXCURS
#define MAXCURS 500
#endif

#ifndef MAXVARS
#define MAXVARS 0x1000
#endif

#define SQLCHAR   0
#define SQLSHORT  1
#define SQLLONG   2
#define SQLDOUBLE 3
#define SQLDECIMAL 6
#define SQLDATE 7
#define SQLTIME 8
#define SQLTIMESTAMP 9

#define NEXT 1
#define PRIOR 2
#define PREVIOUS 2
#define FIRST 3
#define LAST 4
#define CURRENT 5
#define RELATIV 7
#define DBABSOLUTE 6

#define STR_LEN 128+1 

#define REM_LEN 254+1 

class SQLVAR
{
       public :
           void *var;
           int typ;
           int len;
       SQLVAR ()
       {
           var = NULL;
           typ = 0;
           len = 0;
       }

       void SetVar (void *var)
       {
           this->var = var;
       }

       void *GetVar (void)
       {
           return var;
       }

       void SetTyp (int typ)
       {
           this->typ = typ;
       }

       int GetTyp (void)
       {
           return typ;
       }

       void SetLen (int len)
       {
           this->len = len;
       }

       int GetLen (void)
       {
           return len;
       }
};

class DataBase
{
   public :
        HENV henv;
        HDBC hdbc;
        CString Name;

        DataBase ()
        {
            henv = NULL;
            hdbc = NULL;
        }

        DataBase (HENV henv, HDBC hdbc)
        {
            this->henv = henv;
            this->hdbc = hdbc;
        }

        ~DataBase ()
        {
        }

        HENV GetHenv ()
        {
            return henv;
        }

        HDBC GetHdbc ()
        {
            return hdbc;
        }

        CString GetName ()
        {
            return Name;
        }

        HENV GetHenv (CString Name)
        {
            if (this->Name == Name)
            {
                 return henv;
            }
            return NULL;
        }

        HDBC GetHdbc (CString Name)
        {
            if (this->Name == Name)
            {
                 return hdbc;
            }
            return NULL;
        }

        void SetDbase (CString Name, HENV henv, HDBC hdbc) 
        {
            this->Name = Name;
            this->henv = henv;
            this->hdbc = hdbc;
        }
/*
        const Database& Database::operator=(Database& Database)
        {
                SetBuffer (Txt.GetBuffer ());  
                return *this;
        }
*/
};


class DB_CLASS
{
       public :
			   PropExtension::CProperties DbIni;      
               HENV    henv;
               HDBC    hdbc;
               CString name;
               DataBase DBase;
               CString User;
               CString Passw;
               CString DbName;
               HSTMT   hstmDirect;
               HSTMT   hstm;
               int     CursTab[MAXCURS];  
               HSTMT   HstmtTab[MAXCURS];  
               SQLVAR  OutVars[MAXVARS];
               SQLVAR  InVars[MAXVARS];
               int     OutAnz;
               int     InAnz;
               int     sqlstatus;
               BOOL    InWork;
               SDWORD cbLen;
//               int sql_mode;
               BOOL (*SqlErrorProc) (SDWORD, CString&);

               short cursor;
               short test_upd_cursor;
               short test_lock_cursor;
               short upd_cursor;
               short ins_cursor;
               short del_cursor;
               short cursor_ausw;
               int   scrollpos;
               static short ShortNull;
               static long LongNull;
               static double DoubleNull;

               DATE_STRUCT m_Today;


/// Ausgabevariablen f�r SQLColumns

  			   UCHAR szQualifier[STR_LEN]; 
			   UCHAR szOwner[STR_LEN]; 
			   UCHAR szTableName[STR_LEN];
			   UCHAR szColName[STR_LEN]; 
			   UCHAR szTypeName[STR_LEN];
			   UCHAR szRemarks[REM_LEN]; 
			   SDWORD Precision;
			   SWORD Length; 
			   SWORD DataType;
			   SWORD Scale;
			   SWORD Radix;
			   SWORD Nullable; 


/* Declare storage locations for bytes available to return */ 

SDWORD cbQualifier, cbOwner, cbTableName, cbColName; 

SDWORD cbTypeName, cbRemarks, cbDataType, cbPrecision; 

SDWORD cbLength, cbScale, cbRadix, cbNullable; 




               DB_CLASS ();
               ~DB_CLASS ();

               void SetSqlErrorProc (BOOL (*ErrProc) (SDWORD, CString&))
               {
                   this->SqlErrorProc = ErrProc;
               }

               BOOL opendbase (LPTSTR);
               BOOL closedbase (LPTSTR);
               int sqlconnectdbase (LPTSTR, LPTSTR, LPTSTR, LPTSTR);
               int sqlconnect (LPTSTR, LPTSTR, LPTSTR);

               void SetDatabase (DataBase& DBase)
               {
                   this->DBase.henv = DBase.GetHenv ();
                   this->DBase.hdbc = DBase.GetHdbc ();
                   this->DBase.Name = DBase.GetName ();
                   henv = DBase.GetHenv ();
                   hdbc = DBase.GetHdbc ();
                   name = DBase.GetName ();
               }

               DataBase &GetDatabase (void)
               {
                   return DBase;
               }

               void LoadDbIni ();  

               int beginwork ();
               int commitwork ();
               int rollbackwork ();
               BOOL sqlcomm (LPTSTR);
               int sqlcursor (LPTSTR);
               int sqlopen (int);
               int sqlfetch (int);
               int sqlexecute (int);
               int sqlclose (int);
               void sqlout (void *, int , int);
               void sqlin  (void *, int , int);
               int TestOut (HSTMT);
               int TestIn  (HSTMT);
               static void ToDbDate (CString&, DATE_STRUCT *);
               static void FromDbDate (CString&, DATE_STRUCT *);
               void GetError (HSTMT);
               static BOOL ErrProc (SDWORD, CString&);

               virtual void prepare (void) 
               {
               }
               int dbreadfirst (void);
               int dbread (void);
               int dbupdate (void);
               int dblock (void);
               int dbdelete (void);
               void dbclose (void);

               int dbmove (int);
               int dbmove (int, int);
               int dbcanmove (int);
               int dbcanmove (int, int);

			   int PrepareTables (char *Table);
			   int PrepareColumns (char *Table, char *Column);
			   int GetColLength (int colpos, int cursor);
			   LPSTR GetColName (int colpos, int cursor, LPSTR name);
			   int GetColType (int position, int cursor);

               static int FromRecDate (DATE_STRUCT *, LPTSTR);
               static int FromOdbcDate (DATE_STRUCT *, LPTSTR);
               static int FromGerDate (DATE_STRUCT *, LPTSTR);
               static int ToGerDate (DATE_STRUCT *, LPTSTR);
               static int FromOdbcTime (TIME_STRUCT *, LPTSTR);
               static int FromGerTime (TIME_STRUCT *, LPTSTR);
               static int ToGerTime (TIME_STRUCT *, LPTSTR);
               static int FromOdbcTimestamp (TIMESTAMP_STRUCT *, LPTSTR);
               static int FromGerTimestamp (TIMESTAMP_STRUCT *, LPTSTR);
               static int ToGerTimestamp (TIMESTAMP_STRUCT *, LPTSTR);
			   static int CompareDate (DATE_STRUCT *,DATE_STRUCT *);

			   static long DAscToLong (LPSTR sdate);
			   static DATE_STRUCT DAscToLongEx (LPSTR sdate);
			   static LPSTR DLongToAsc (long ldate, LPSTR date);
			   static LPSTR DLongToAsc (DATE_STRUCT& ldate, LPSTR sdate);
			   void SetToday (int diff, DATE_STRUCT *today=NULL, BOOL dbset=FALSE);

			   static FILE *sqldebug;
			   static int SQLDEBUG;
			   static char DebugFile [512];
			   static void SetCSqlDebug (BOOL mode);
			   static void CreateDebugFile ();

			   static BOOL IsShortnull (short value);
			   static BOOL IsLongnull (long value);
			   static BOOL IsDoublenull (double value);

};
}
#endif