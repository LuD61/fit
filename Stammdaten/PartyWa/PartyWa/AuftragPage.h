#pragma once
#include "afxwin.h"
#include "PartyWa.h"
#include "RunMessage.h"
#include "MessageHandler.h"
#include "CtrlGridColor.h"
#include "FormTab.h"
#include "NumEdit.h"
#include "TextEdit.h"
#include "A_bas.h"
#include "Partyk.h"
#include "Partyp.h"
#include "Mdn.h"
#include "Fil.h"
#include "Kun.h"
#include "Adr.h"
#include "DataTables.h"
#include "PartyListCtrl.h"
#include "FillList.h"
#include "ChoiceMdn.h"
#include "ChoiceFil.h"
#include "ChoicePartyk.h"
#include "ChoiceKun.h"
#include "AngebotCore.h"
#include "DataCollection.h"
#include "StaticButton.h"
#include "ColorButton.h"
#include "Label.h"
#include "VLabel.h"
#include "CalcFrameWnd.h"
#include "CalcViewAuf.h"
#include "afxdtctl.h"
#include "KunDiverse.h"

#define IDC_MDNCHOICE 3001
#define IDC_FILCHOICE 3002
#define IDC_ANGCHOICE 3003
#define IDC_KUNCHOICE 3004

class CAuftragPage :
	public CDialog
{
	DECLARE_DYNAMIC(CAuftragPage)
public:
	CAuftragPage(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CAuftragPage();

// Dialogfelddaten
	enum { IDD = IDD_AUFTRAG_PAGE };

protected:

// Handlerklasse zum Programm beenden
	class CExitMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CExitMessage (CAuftragPage *p);
			virtual void Run ();
	};

// Handlerklasse f�r Aktion abbrechen
	class CBackMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CBackMessage (CAuftragPage *p);
			virtual void Run ();
	};

	class CDataBackMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CDataBackMessage (CAuftragPage *p);
			virtual void Run ();
	};

// Handlerklasse zum Speichern
	class CSaveMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CSaveMessage (CAuftragPage *p);
			virtual void Run ();
	};

// Handlerklasse zum L�schen von Zeilen
	class CDeleteMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CDeleteMessage (CAuftragPage *p);
			virtual void Run ();
			virtual BOOL Test ();
	};

// Handlerklasse zum Einf�gen von Zeilen
	class CInsertMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CInsertMessage (CAuftragPage *p);
			virtual void Run ();
			virtual BOOL Test ();
	};

// Handlerklasse f�r Copy
	class CCopyMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CCopyMessage (CAuftragPage *p);
			virtual void Run ();
	};

// Handlerklasse f�r Paste
	class CPasteMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CPasteMessage (CAuftragPage *p);
			virtual void Run ();
			virtual BOOL Test ();
	};


// Handlerklasse zum L�schen des Angebots
	class CDeleteAngMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CDeleteAngMessage (CAuftragPage *p);
			virtual void Run ();
			virtual BOOL Test ();
	};

// Handlerklasse zum Drucken der Auftragsbest�tigung
	class CPrintAuftragMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CPrintAuftragMessage (CAuftragPage *p);
			virtual void Run ();
			virtual BOOL Test ();
	};


// Handlerklasse zum Drucken des Lieferscheins
	class CPrintLiefMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CPrintLiefMessage (CAuftragPage *p);
			virtual void Run ();
			virtual BOOL Test ();
	};

// Handlerklasse zum Drucken einer Rechnung
	class CPrintRechMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CPrintRechMessage (CAuftragPage *p);
			virtual void Run ();
			virtual BOOL Test ();
	};

// Handlerklasse f�r Kalkulationsfenster
	class CCalcViewMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CCalcViewMessage (CAuftragPage *p);
			virtual void Run ();
	};

// Handlerklasse f�r PositionsTexte
	class CPosTextMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CPosTextMessage (CAuftragPage *p);
			virtual void Run ();
			virtual BOOL Test ();
	};

// Handlerklasse f�r KopfTexte
	class CKopfTextMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CKopfTextMessage (CAuftragPage *p);
			virtual void Run ();
			virtual BOOL Test ();
	};

// Handlerklasse f�r FussTexte
	class CFussTextMessage : public CRunMessage
	{
	private: 
		CAuftragPage *p;
	public: 
			CFussTextMessage (CAuftragPage *p);
			virtual void Run ();
			virtual BOOL Test ();
	};

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

	virtual BOOL OnInitDialog ();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd,UINT nCtlColor);
	virtual void OnSize (UINT, int, int);

	DECLARE_MESSAGE_MAP()

private:
	CMessageHandler *MessageHandler;
	CExitMessage *ExitMessage;
	CBackMessage *BackMessage;
	CDataBackMessage *DataBackMessage;
	CSaveMessage *SaveMessage;
	CDeleteMessage *DeleteMessage;
	CInsertMessage *InsertMessage;
	CCopyMessage *CopyMessage;
	CPasteMessage *PasteMessage;
	CDeleteAngMessage *DeleteAngMessage;
	CPrintAuftragMessage *PrintAuftragMessage;
	CPrintLiefMessage *PrintLiefMessage;
	CPrintRechMessage *PrintRechMessage;
	CCalcViewMessage *CalcViewMessage;
	CPosTextMessage *PosTextMessage;
	CKopfTextMessage *KopfTextMessage;
	CFussTextMessage *FussTextMessage;
	CFont Font;
	CFont TitleFont;
	int FontHeight;
	COLORREF DlgBkColor;
	HBRUSH DlgBrush;
	BOOL MustCreate;
	BOOL IsCreated;
	BOOL TestKun;
	CCtrlGridColor CtrlGrid;
	CCtrlGrid HeadGrid;
	CCtrlGridColor MdnGrid;
	CCtrlGridColor FilGrid;
	CCtrlGridColor AngGrid;
	CCtrlGrid KunFilGrid;
	CCtrlGrid DataGrid;
	CCtrlGridColor KunGrid;
	CCtrlGrid AdressGrid;
	CCtrlGrid TypGrid;
	CCtrlGrid PersGrid;
	CCtrlGrid PriceGrid;
	CFillList FillList;
	CFormTab Form;
	CAngebotCore *AngebotCore;
public:
	CStatic m_LMdn;
	CNumEdit m_Mdn;
	CColorButton m_MdnChoice;
	CEdit m_MdnName;
	CStatic m_LFil;
	CNumEdit m_Fil;
	CColorButton m_FilChoice;
	CEdit m_FilName;
	CStatic m_LAngebotNr;
	CNumEdit m_AngebotNr;
	CStatic m_LAuf;
	CNumEdit m_Auf;
	CStatic m_LLs;
	CNumEdit m_Ls;
//	CButton m_AngChoice;
	CColorButton m_AngChoice;
	CButton m_BKun;
	CButton m_BFil;
	CStatic m_LKun;
	CNumEdit m_Kun;
	CColorButton m_KunChoice;
	CStatic m_KunName;
	CEdit m_KunName2;
	CStatic m_LTel;
	CStatic m_Tel;
	CEdit m_Tel2;
	CStatic m_LAngTyp;
	CComboBox m_AngTyp;
	CStatic m_LPers;
	CComboBox m_Pers;
	CColorButton m_NewAngTyp;
	CColorButton m_Address;
	CColorButton m_LiefAddress;
	CColorButton m_ModifyPers;
	CColorButton m_Comments;
	CStatic m_LAnzPers;
    CNumEdit m_AnzPers;
	CColorButton m_CalcPos;
	CButton m_EinzelPreis;
	CButton m_NurServicePreis;
	CButton m_HideMe;
	CButton m_HideService;
	CButton m_MwstVoll;
	CButton m_Abholung;
	CStatic m_LAngGew;
    CNumEdit m_AngGew;
	CStatic m_LAngVk;
    CNumEdit m_AngVk;
	CStatic m_LPauschalVk;
    CNumEdit m_PauschalVk;
	CStatic m_LEinzelVk;
    CNumEdit m_EinzelVk;
	CStatic m_LEinzelVkPausch;
    CNumEdit m_EinzelVkPausch;
	CStatic m_LServiceVkPausch;
    CNumEdit m_ServiceVkPausch;
   	CStatic m_LHinweis;
	CEdit m_Hinweis;
	CVLabel m_PreisLabel;
	CVLabel m_CalcLabel;
	CVLabel m_PauschLabel;
	CPartyListCtrl m_List;

private:
	BOOL m_AngKun;
	BOOL m_AngFil;
	A_BAS_CLASS *A_bas;
	PARTYK_CLASS *Partyk;
	PARTYK_CLASS *PartykTest;
	PARTYP_CLASS *Partyp;
	MDN_CLASS *Mdn;
	FIL_CLASS *Fil;
	KUN_CLASS *Kun;
	ADR_CLASS *MdnAdr;
	ADR_CLASS *FilAdr;
	ADR_CLASS *KunAdr;
	CDatatables *Datatables;
	CChoiceMdn *ChoiceMdn;
	CChoiceFil *ChoiceFil;
	CChoicePartyk *ChoicePartyk;
	CString QueryPartyk;
	BOOL FirstReadPartyk;
	CChoiceKun *ChoiceKun;
	CKunDiverse *KunDiverse;
	CCalcFrameWnd *CalcFrame;	
	BOOL MustCalcView;
	int CalcWidth;
	int CalcHeight;
	BOOL CalcCaption;
public:
	COLORREF GetDlgBkColor ()
	{
		return DlgBkColor;
	}
	BOOL OnReturn ();
	BOOL OnKeyDown ();
	BOOL OnKeyUp ();
	BOOL OnDataBack ();
	BOOL OnF5 ();
	BOOL OnF6 ();
	BOOL OnF9 ();
	BOOL OnF12 ();
	void SetFirstFocus ();
	BOOL ReadMdn ();
	BOOL ReadFil ();
	BOOL ReadAng ();
	BOOL ReadPositions ();
	BOOL ReadKun ();
	BOOL WriteAng ();
	void OnMdnChoice ();
	void OnFilChoice ();
	void OnPartykChoice ();
	void OnKunChoice ();
	void OnKunSelected ();
	void OnKunCanceled ();
	void OnExit ();
	void OnBack ();
	void OnSave ();
	void OnDelete ();
	void OnInsert ();
	void OnCopy ();
	void OnPaste ();
	void OnDeleteAng ();
	void OnPrintAuftrag ();
	void OnPrintLief ();
	void OnPrintRech ();
	void OnRePrintRech ();
	BOOL TestInsertDelete ();
	BOOL TestPaste ();
	BOOL TestDeleteAng ();
	BOOL TestPrintAng ();
	BOOL TestPrintAuftrag ();
	BOOL TestPrintLief ();
	BOOL TestPrintRech ();
	void OnCalcView ();
	void OnPosText ();
	void OnKopfText ();
	void OnFussText ();
	BOOL TestPosText ();
	BOOL TestKopfText ();
	BOOL TestFussText ();
	BOOL KunOk();
public:
	afx_msg void OnEnSetfocusAngebotNr();
public:
	afx_msg void OnBnClickedBkun();
public:
	afx_msg void OnBnClickedBfil();
public:
	afx_msg void OnEnKillfocusPauschalVk();
public:
	afx_msg void OnEnKillfocusServicePauschVk ();
public:
	afx_msg void OnEnKillfocusAnzPers();
public:
	afx_msg void OnBnClickedMwstVoll();
public:
	afx_msg void OnEnKillfocusKun();
public:
	afx_msg void OnEnKillfocusEinzelVkPausch();
public:
	afx_msg void OnBnClickedEinzelPreis();
public:
	afx_msg void OnStnClickedNewAngTyp();
	void ShowCalcWnd ();
	void ShowCalcView ();
	void HideCalcView ();
public:
	CStatic m_LLieferdat;
public:
	CDateTimeCtrl m_Lieferdat;
public:
	CStatic m_LLieferzeit;
public:
	CDateTimeCtrl m_Lieferzeit;
public:
	afx_msg void OnStnClickedAddress();
public:
	afx_msg void OnStnClickedLiefAdr();
public:
	afx_msg void OnCalcPos();
public:
	afx_msg void OnComments();
public:
	afx_msg void OnModifyPers();
public:
	afx_msg void OnEnSetfocusAnzPers();
	void InitForm ();
public:
	afx_msg void OnBnClickedNurServicePreis();
};
