#pragma once

class CStrFuncs
{
public:
	static TCHAR *Separator;
	static CString Format;
	CStrFuncs(void);
	~CStrFuncs(void);
	static void FillString (LPTSTR m_Source, LPTSTR m_Target);
	static void Round (double, CString&);
	static void Round (double, int, CString&);
	static double RoundToDouble (double d, int scale); 
	static BOOL IsDoubleEqual (double, double);
    static double StrToDoubleA (LPSTR);
    static double StrToDouble (LPTSTR);
    static double StrToDouble (CString&);
    static void DoubleToStr (CString& c, double d, LPTSTR format);
	static void PointToKomma (CString&);
	static BOOL IsDecimal (CString&);
	static void SysDate (CString& Date);
	static void SysTime (CString& Time);
    static void ToClipboard (CString& Text);
    static void FromClipboard (CString& Text);
    static BOOL CanPaste ();
    static void FromClipboard (CString& Text, UINT format);
    static void FromClipboardMByte (CString& Text);
    static void FromClipboardUni (CString& Text);
    static void ToClipboard (CString& Text, UINT format);
    static void ToClipboardMByte (CString& Text);
    static void ToClipboardUni (CString& Text);
	static void Trim (LPSTR);
	static void ChangePart (LPTSTR String, LPTSTR Source, LPTSTR Target, int len);
	static void ChangeString (LPTSTR String, LPTSTR Source, LPTSTR Target, int len);
};
