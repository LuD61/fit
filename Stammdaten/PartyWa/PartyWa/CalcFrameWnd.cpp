#include "StdAfx.h"
#include "CalcFrameWnd.h"
#include "CalcView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CCalcFrameWnd, CFrameWnd)

BEGIN_MESSAGE_MAP(CCalcFrameWnd, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()

CCalcFrameWnd::CCalcFrameWnd(void)
{
	WithTitle = FALSE;
}

CCalcFrameWnd::~CCalcFrameWnd(void)
{
}

int CCalcFrameWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
	{
		return -1;
	}
	return 0;
}

BOOL CCalcFrameWnd::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: �ndern Sie hier die Fensterklasse oder die Darstellung, indem Sie
	//  CREATESTRUCT cs modifizieren.

	if (!WithTitle)
	{
		cs.style = WS_POPUP | WS_BORDER;
	}
	return TRUE;
}

BOOL CCalcFrameWnd::OnCreateClient(LPCREATESTRUCT lpcs,   CCreateContext* pContext)
{
	CFrameWnd::OnCreateClient (lpcs, pContext);
	return TRUE;
}
