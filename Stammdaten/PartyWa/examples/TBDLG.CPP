// TbDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TBCUST.h"
#include "toolbar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTbDlg dialog


CTbDlg::CTbDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTbDlg::IDD, pParent) , m_sizeIcon(22,21)
{
	//{{AFX_DATA_INIT(CTbDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
      m_nIDClipFormat      = CEnexsysCustomToolbar::GetClipboardFormat();
      m_nToolbarsCount     = 0;
      m_nCurrentToolbar    = 0;
      m_nCommands          = 0;
      m_pCurrentImageList  = NULL;
      m_strToolTip         =_T("");
}

BOOL  CTbDlg::DoCustomize()
{
      if( m_hWnd ) return TRUE;  // already running...
      //
      m_bContinue =TRUE;
      BOOL  bRtnCode=Create(CTbDlg::IDD);
      if( bRtnCode ) ShowWindow (SW_SHOW);
      return bRtnCode;
}

CTbDlg::~CTbDlg()
{
      if( m_pCurrentImageList ) delete m_pCurrentImageList;
      m_ctrlList.Destroy();
      Close();
}

void  CTbDlg::OnCancel()
{
      m_bContinue=FALSE;
      Close();
}

BOOL  CTbDlg::WaitMy()
{
      if( m_bContinue ){
         MSG msg;
         //
         // Retrieve and dispatch any waiting messages.
         //
         while( ::PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE) ){
            if( !AfxGetApp()->PumpMessage() ){
               ::PostQuitMessage(0);
               return (m_bContinue=FALSE);
            }
         }
         //
         // Simulate the framework's idle processing mechanism.
         //
         LONG  lIdle=0;
         while( AfxGetApp()->OnIdle(lIdle++) );
      }
      return m_bContinue;
}

void  CTbDlg::Close()
{
      if( ::IsWindow(m_hWnd) )  DestroyWindow();
}

void CTbDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTbDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	   DDX_Control(pDX, IDC_COMBO1         , m_cbToolbars       );
	   DDX_Control(pDX, IDC_ICON_FRAMES    , m_controlsFrame    );
	   DDX_Text(   pDX, IDC_TOOLTIP_ECHO   , m_strToolTip       );
	   DDX_CBIndex(pDX, IDC_COMBO1         , m_nCurrentToolbar  );
}


BEGIN_MESSAGE_MAP(CTbDlg, CDialog)
	//{{AFX_MSG_MAP(CTbDlg)
	ON_WM_LBUTTONDOWN()
	ON_WM_PAINT()
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTbDlg message handlers

BOOL CTbDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
      //
      // Get client area...
      //
		HWND  hWnd=(HWND)::GetDlgItem(m_hWnd,IDC_ICON_FRAMES );
		ASSERT(hWnd != NULL);
		::GetWindowRect(hWnd,&m_rcFrame);
		ScreenToClient(m_rcFrame);
      //
      if( m_nToolbarsCount ){
         //
         // Init combo...
         //
         for( UINT i=0 ; i < m_nToolbarsCount ; i++ )
            m_cbToolbars.AddString(m_strToolbarsID[i]);
         //
         m_cbToolbars.SetCurSel(0);
         SetImageList(m_nToolbarsID[m_nCurrentToolbar]);
      }
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void  CTbDlg::AddToolbarID(UINT nToolbarID,UINT nToolbarIDW)
{
      if( m_nToolbarsCount < MaxToolbars ){
         CString  str;
         str.LoadString(nToolbarIDW);
         AfxExtractSubString(m_strToolbarsID[m_nToolbarsCount],str,1,'\n');
         //
         m_nToolbarsIDW[m_nToolbarsCount] = nToolbarIDW;
         m_nToolbarsID[m_nToolbarsCount++]= nToolbarID;
      }
}

struct CToolBarData
{
	   WORD wVersion;
	   WORD wWidth;
	   WORD wHeight;
	   WORD wItemCount;
	   //WORD aItems[wItemCount]

	   WORD* items()
		   { return (WORD*)(this+1); }
};

void  CTbDlg::SetImageList(UINT nToolbarID)
{
      if( m_pCurrentImageList ) delete m_pCurrentImageList;
      //
      if( m_nToolbarsCount && nToolbarID ){
         //
         m_pCurrentImageList  =new CImageList();
         if( m_pCurrentImageList ){
            //
            //
            // Load Toolbar ID & image list...
            //
	         HINSTANCE   hInst=AfxFindResourceHandle((LPCTSTR)nToolbarID, RT_TOOLBAR);
	         HRSRC       hRsrc=::FindResource(hInst,(LPCTSTR)nToolbarID, RT_TOOLBAR);
            if( hRsrc == NULL ) return;
            //
	         HGLOBAL     hGlobal=LoadResource(hInst, hRsrc);
            if( hGlobal == NULL ) return;
            //
	         CToolBarData*  pData=(CToolBarData*)LockResource(hGlobal);
	         if( pData == NULL ) return;
	         ASSERT( pData->wVersion == 1 );
            //
	         m_nCommands = 0;
            for( int i=0 ; i < pData->wItemCount ; i++ )
               if( pData->items()[i] )
                   m_nCommandsID[m_nCommands++] =(int)pData->items()[i];
            //
            m_sizeIcon.cx  =pData->wWidth  + 7;
            m_sizeIcon.cy  =pData->wHeight + 7;
            //
	         UnlockResource(hGlobal);
	         FreeResource(hGlobal);
            //
            // Load Bitmap...
            //
            m_pCurrentImageList->Create(nToolbarID,pData->wWidth,4, CLR_NONE); //0);
         }
      }
}

void CTbDlg::OnLButtonDown(UINT nFlags, CPoint pt) 
{
      if( m_pCurrentImageList ){
         //
         CRect rcImage;
         int   nIndex=HitImage(pt,rcImage);
         //
         if( nIndex > -1 ){
            //
            m_strToolTip.LoadString(m_nCommandsID[nIndex]);
	         int index = m_strToolTip.Find (_T('\n'));
	         if( index != -1 ) m_strToolTip = m_strToolTip.Left( index );
            ::SetDlgItemText(m_hWnd,IDC_TOOLTIP_ECHO,(LPCTSTR)m_strToolTip);
            //
	         COleDataSource          srcItem;
		      CSharedFile             globFile;
		      CArchive                ar(&globFile,CArchive::store);
            //
            // Construct a TBBUTTON to export info...
            //
            TBBUTTON tb;
            //
            tb.iBitmap   =nIndex;
            tb.idCommand =m_nCommandsID[nIndex];
            tb.fsState   =TBSTATE_ENABLED;  
            tb.fsStyle   =TBSTYLE_BUTTON; 
            tb.dwData    =m_nToolbarsID[m_nCurrentToolbar];
            tb.iString   =m_nCommandsID[nIndex];
            //
            CCustomToolbarDragItem  dragItem(
                                       m_nToolbarsID[m_nCurrentToolbar],
                                       nIndex   ,
                                       tb       ,
                  m_ctrlList.FindIndex(tb.idCommand) );
            //
            dragItem.Serialize(ar);
            //             
		      ar.Close();
            //
            ClientToScreen(rcImage);
            //
		      srcItem.CacheGlobalData(m_nIDClipFormat,globFile.Detach());
            srcItem.DoDragDrop(DROPEFFECT_COPY,&rcImage);
         }
      }
	   CDialog::OnLButtonDown(nFlags, pt);
}

void CTbDlg::OnPaint() 
{
      if( m_pCurrentImageList ){
         //
         CPaintDC dc(this); // device context for painting
         //
         int      nCount=m_pCurrentImageList->GetImageCount();
         CRect    rcBound;
         CPoint   ptInsert;
         //
         for( int i=0 ; i < nCount ; i++ ){
            GetItemRect(i,rcBound);
            ptInsert.x  =rcBound.left+3;
            ptInsert.y  =rcBound.top+3;
            m_pCurrentImageList->Draw(&dc,i,ptInsert,ILD_NORMAL);
         }
	   }
}

int   CTbDlg::HitImage(CPoint pt,CRect &rcImage)
{
      int   nImageIndex=-1;
      //
      if( m_pCurrentImageList ){
         //
         int   nCount=m_pCurrentImageList->GetImageCount();
         //
         for( int i=0 ; i < nCount ; i++ ){
            GetItemRect(i,rcImage);
            if( rcImage.PtInRect(pt) ){   nImageIndex=i;   break; }
         }
      }
      return nImageIndex;
}

void  CTbDlg::GetItemRect(int nImageCount,CRect &rcIcon)
{
      //      
      // Ogni icona in 22 x 21      
      //      
      rcIcon.SetRect(m_rcFrame.left,m_rcFrame.top,
                     m_rcFrame.left+m_sizeIcon.cx,
                     m_rcFrame.top +m_sizeIcon.cy);
      //
      for( int i=0 ; i < nImageCount ; i++ ){
         //                              
         // Vai a destra..                              
         //                              
         rcIcon.left+=m_sizeIcon.cx;
         rcIcon.right+=m_sizeIcon.cx;
         if( rcIcon.right >=  m_rcFrame.right ){
            //
            // Go Down
            //
            rcIcon.left    =m_rcFrame.left;
            rcIcon.right   =m_rcFrame.left+m_sizeIcon.cx;
            rcIcon.top+=    m_sizeIcon.cy;
            rcIcon.bottom+= m_sizeIcon.cy;
         }
      }
}

void CTbDlg::OnSelchangeCombo() 
{
      UpdateData( TRUE );
      //
      if( m_nCurrentToolbar != CB_ERR ){
         SetImageList( m_nToolbarsID[m_nCurrentToolbar] );
         Invalidate();
      }
}

CToolBarButton*   CTbDlg::AddButton(CToolBarButton* pButton)
{
      if( pButton ){
         m_ctrlList.Destroy(m_ctrlList.FindIndex(pButton->GetCmdID()));
         m_ctrlList.Add(pButton);
      }
      return pButton;
}

CToolBarButton*   CTbDlg::AddButton(int    commandID   ,
                                    int    index       ,   // lo cerca...
                                    UINT   nBitmapID   ,  // bitmap ID ( 0 is native...)
                                    int    nImage      ,
                                    UINT   nMenuID     )
{
      CToolBarButton* pButton=new CToolBarButton(commandID,index,nBitmapID,nImage);
      if( nMenuID ) pButton->SetMenuID(nMenuID);
      return AddButton( pButton );
}

CToolBarEditButton*  CTbDlg::AddEdit(int     commandID,  // command id
                                     int    index     ,  // lo cerca...
                                     int  width       ,  //
                                     LONG style       )
{
      return (CToolBarEditButton*)AddButton( new CToolBarEditButton(commandID,index,width,style) );
}

CToolBarComboBoxButton* CTbDlg::AddCombo(int  commandID,
                                 int  index ,
                                 int  width ,
                                 int  height,
                                 LONG style )
{
      return (CToolBarComboBoxButton*)AddButton( new CToolBarComboBoxButton(commandID,index,width,height,style) );
}

CToolBarProgressButton* CTbDlg::AddProgress(int  commandID ,
                                    int  index     ,
                                    int  iWidth    ,
                                    int  nLower    ,
                                    int  nUpper    ,
                                    int  nStep     )
{
      return (CToolBarProgressButton*)AddButton( new CToolBarProgressButton(commandID,index,iWidth,nLower,nUpper,nStep) );
}
