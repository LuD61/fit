// TBCUST.h : main header file for the TBCUST application
//

#if !defined(AFX_TBCUST_H__FB46B1B6_E3D1_4900_BDC5_1EFD758CEC5F__INCLUDED_)
#define AFX_TBCUST_H__FB46B1B6_E3D1_4900_BDC5_1EFD758CEC5F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CTBCUSTApp:
// See TBCUST.cpp for the implementation of this class
//

class CTBCUSTApp : public CWinApp
{
public:
	CTBCUSTApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTBCUSTApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CTBCUSTApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TBCUST_H__FB46B1B6_E3D1_4900_BDC5_1EFD758CEC5F__INCLUDED_)
