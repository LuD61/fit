// Doc.h : interface of the CTBCUSTDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOC_H__6AF81C21_9E4E_49BD_BFEF_A0B889DBF6F7__INCLUDED_)
#define AFX_DOC_H__6AF81C21_9E4E_49BD_BFEF_A0B889DBF6F7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTBCUSTDoc : public CDocument
{
protected: // create from serialization only
	CTBCUSTDoc();
	DECLARE_DYNCREATE(CTBCUSTDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTBCUSTDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTBCUSTDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTBCUSTDoc)
	afx_msg void OnUpdateCustomizeDropdwonmenu(CCmdUI* pCmdUI);
	afx_msg void OnCustomizeDropdwonmenu();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOC_H__6AF81C21_9E4E_49BD_BFEF_A0B889DBF6F7__INCLUDED_)
