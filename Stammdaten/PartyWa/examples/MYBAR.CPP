#include "stdafx.h"
#include "resource.h"
#include "mybar.h"

IMPLEMENT_DYNCREATE(CMyCustomToolbar,CEnexsysCustomToolbar)

BEGIN_MESSAGE_MAP(CMyCustomToolbar,CEnexsysCustomToolbar)
END_MESSAGE_MAP()

void CMyCustomToolbar::OnAddControls(CTbDlg &dlg)
{
      dlg.AddButton(    ID_CUSTOMIZE_DROPDWONMENU,-1,0,-1,IDR_MENU1); 
      dlg.AddCombo(     ID_CUSTOMIZE_COMBO                  ); 
      dlg.AddEdit(      ID_CUSTOMIZE_EDIT                   );
      dlg.AddProgress(  ID_CUSTOMIZE_PROGRESS,-1,50         );
}

BOOL CMyCustomToolbar::OnCreateControls()
{
	   int               nButtons = GetToolBarCtrl().GetButtonCount(),nID;
      CToolBarButton*   pButton;
      BOOL  bResult=TRUE;
      //
	   for( int i = 0; i < nButtons && bResult ; i++ ){
         switch( (nID=GetItemID(i)) ){
            case ID_CUSTOMIZE_DROPDWONMENU:
               if( !(pButton=CreateButton(nID,-1,0,-1,IDR_MENU1)) )
                  bResult=FALSE;
               break;
            case ID_CUSTOMIZE_COMBO    :
               if( !(pButton=CreateCombo(nID)) )
                  bResult=FALSE;
               else{
                  pButton->SetLocked(TRUE);  // Dont destroy this from toolbar !
                  if( pButton->GetWindowCtrl() ){
                     ((CComboBox*)pButton->GetWindowCtrl())->ResetContent();
                     CString  str;
                     for( int i=0 ; i < 10 ; i++ ){
                        str.Format("Opzione %d\n",i);
                        ((CComboBox*)pButton->GetWindowCtrl())->AddString(str);
                     }
                  }
               }
               break;
            case ID_CUSTOMIZE_EDIT     :
               if( !CreateEdit(nID) )
                  bResult=FALSE;
               break;
            case ID_CUSTOMIZE_PROGRESS :
               if( !CreateProgress(nID,-1,50) )
                  bResult=FALSE;
               break;
         }
      }
      return bResult;
}

