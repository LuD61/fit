// View.h : interface of the CTBCUSTView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_VIEW_H__427B715C_0588_495C_B4C8_EC62DA16B21E__INCLUDED_)
#define AFX_VIEW_H__427B715C_0588_495C_B4C8_EC62DA16B21E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTBCUSTView : public CView
{
protected: // create from serialization only
	CTBCUSTView();
	DECLARE_DYNCREATE(CTBCUSTView)

// Attributes
public:
	CTBCUSTDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTBCUSTView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTBCUSTView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
   BOOL  m_bCombo    ;
   BOOL  m_bEdit     ;
   BOOL  m_bProgress ;
// Generated message map functions
protected:
	//{{AFX_MSG(CTBCUSTView)
	afx_msg void OnCustomizeCombo();
	afx_msg void OnUpdateCustomizeCombo(CCmdUI* pCmdUI);
	afx_msg void OnCustomizeEdit();
	afx_msg void OnUpdateCustomizeEdit(CCmdUI* pCmdUI);
	afx_msg void OnCustomizeProgress();
	afx_msg void OnUpdateCustomizeProgress(CCmdUI* pCmdUI);
	afx_msg void OnCustomizeEnablecombo();
	afx_msg void OnCustomizeEnableedit();
	afx_msg void OnCustomizeEnableprogress();
	afx_msg void OnUpdateCustomizeEnablecombo(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCustomizeEnableedit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCustomizeEnableprogress(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in View.cpp
inline CTBCUSTDoc* CTBCUSTView::GetDocument()
   { return (CTBCUSTDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIEW_H__427B715C_0588_495C_B4C8_EC62DA16B21E__INCLUDED_)
