//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TBCUST.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_TBCUSTTYPE                  129
#define IDR_MAINFRAME1                  129
#define IDR_MENU1                       130
#define IDC_ICON_FRAMES                 1004
#define IDC_TOOLTIP_ECHO                1008
#define IDC_COMBO1                      1116
#define IDD_CUSTOMIZE_TOOLBAR_DLG       8354
#define ID_CUSTOMIZE_ENABLE             32771
#define ID_CUSTOMIZE_COMBO              32772
#define ID_CUSTOMIZE_EDIT               32773
#define ID_CUSTOMIZE_PROGRESS           32774
#define ID_CUSTOMIZE_DROPDWONMENU       32775
#define ID_CUSTOMIZE_ENABLECOMBO        32776
#define ID_CUSTOMIZE_ENABLEEDIT         32777
#define ID_CUSTOMIZE_ENABLEPROGRESS     32778
#define ID_VIEW_MAINFRAME1              59394

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
