// Doc.cpp : implementation of the CTBCUSTDoc class
//

#include "stdafx.h"
#include "TBCUST.h"

#include "Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTBCUSTDoc

IMPLEMENT_DYNCREATE(CTBCUSTDoc, CDocument)

BEGIN_MESSAGE_MAP(CTBCUSTDoc, CDocument)
	//{{AFX_MSG_MAP(CTBCUSTDoc)
	ON_UPDATE_COMMAND_UI(ID_CUSTOMIZE_DROPDWONMENU, OnUpdateCustomizeDropdwonmenu)
	ON_COMMAND(ID_CUSTOMIZE_DROPDWONMENU, OnCustomizeDropdwonmenu)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTBCUSTDoc construction/destruction

CTBCUSTDoc::CTBCUSTDoc()
{
	// TODO: add one-time construction code here

}

CTBCUSTDoc::~CTBCUSTDoc()
{
}

BOOL CTBCUSTDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTBCUSTDoc serialization

void CTBCUSTDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTBCUSTDoc diagnostics

#ifdef _DEBUG
void CTBCUSTDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTBCUSTDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTBCUSTDoc commands

void CTBCUSTDoc::OnUpdateCustomizeDropdwonmenu(CCmdUI* pCmdUI) 
{
      pCmdUI->Enable( TRUE );	
}

void CTBCUSTDoc::OnCustomizeDropdwonmenu() 
{
	// TODO: Add your command handler code here
	
}
