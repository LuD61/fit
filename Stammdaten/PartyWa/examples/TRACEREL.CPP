#include "stdafx.h"
//
// Paul DiLascia docet...
//
#ifdef _TRACEWIN_FROM_PAUL_DI_LASCIA_

void TRACEWIN(LPCTSTR lpszFormat,... )
{
	   HWND hTraceWnd                = ::FindWindow(TRACEWND_CLASSNAME, NULL);
	   if (hTraceWnd==NULL) hTraceWnd= ::FindWindow(OLDTRACEWND_CLASSNAME, NULL);
      //
	   if (hTraceWnd) {
         //
		   // Found Trace window: send string with WM_COPYDATA
		   // Must copy to make me the owner of the string; otherwise
		   // barfs when called from MFC with traceMultiApp on
		   //
		   static char mybuf[1024];
         //
         va_list  ArgPtr;
         //
         va_start(ArgPtr,lpszFormat);
         vsprintf(mybuf,lpszFormat,ArgPtr);
         va_end(ArgPtr);
         //
		   COPYDATASTRUCT cds;
		   cds.dwData = ID_COPYDATA_TRACEMSG;
		   cds.cbData = ::lstrlen(mybuf);
		   cds.lpData = mybuf;
		   CWinApp* pApp = AfxGetApp();
		   HWND hWnd = pApp ? pApp->m_pMainWnd->GetSafeHwnd() : NULL;
		   ::SendMessage(hTraceWnd, WM_COPYDATA, (WPARAM)hWnd, (LPARAM)&cds);
	   }
}

#endif