; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CTBCUSTView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "TBCUST.h"
LastPage=0

ClassCount=7
Class1=CTBCUSTApp
Class2=CTBCUSTDoc
Class3=CTBCUSTView
Class4=CMainFrame

ResourceCount=6
Resource1=IDD_CUSTOMIZE_TOOLBAR_DLG (Neutral)
Resource2=IDR_MAINFRAME
Class5=CChildFrame
Class6=CAboutDlg
Resource3=IDR_MENU1
Resource4=IDR_MAINFRAME1
Resource5=IDD_ABOUTBOX
Class7=CTbDlg
Resource6=IDR_TBCUSTTYPE

[CLS:CTBCUSTApp]
Type=0
HeaderFile=TBCUST.h
ImplementationFile=TBCUST.cpp
Filter=N

[CLS:CTBCUSTDoc]
Type=0
HeaderFile=Doc.h
ImplementationFile=Doc.cpp
Filter=N
BaseClass=CDocument
VirtualFilter=DC
LastObject=CTBCUSTDoc

[CLS:CTBCUSTView]
Type=0
HeaderFile=View.h
ImplementationFile=View.cpp
Filter=C
LastObject=ID_CUSTOMIZE_ENABLEPROGRESS
BaseClass=CView
VirtualFilter=VWC


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
BaseClass=CMDIFrameWnd
VirtualFilter=fWC
LastObject=CMainFrame


[CLS:CChildFrame]
Type=0
HeaderFile=ChildFrm.h
ImplementationFile=ChildFrm.cpp
Filter=M


[CLS:CAboutDlg]
Type=0
HeaderFile=TBCUST.cpp
ImplementationFile=TBCUST.cpp
Filter=D
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_PRINT_SETUP
Command4=ID_FILE_MRU_FILE1
Command5=ID_APP_EXIT
Command6=ID_VIEW_TOOLBAR
Command7=ID_VIEW_STATUS_BAR
Command8=ID_APP_ABOUT
CommandCount=8

[TB:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

[MNU:IDR_TBCUSTTYPE]
Type=1
Class=CTBCUSTView
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_PRINT
Command7=ID_FILE_PRINT_PREVIEW
Command8=ID_FILE_PRINT_SETUP
Command9=ID_FILE_MRU_FILE1
Command10=ID_APP_EXIT
Command11=ID_EDIT_UNDO
Command12=ID_EDIT_CUT
Command13=ID_EDIT_COPY
Command14=ID_EDIT_PASTE
Command15=ID_VIEW_TOOLBAR
Command16=ID_VIEW_STATUS_BAR
Command17=ID_WINDOW_NEW
Command18=ID_WINDOW_CASCADE
Command19=ID_WINDOW_TILE_HORZ
Command20=ID_WINDOW_ARRANGE
Command21=ID_APP_ABOUT
Command22=ID_CUSTOMIZE_ENABLE
Command23=ID_CUSTOMIZE_COMBO
Command24=ID_CUSTOMIZE_EDIT
Command25=ID_CUSTOMIZE_PROGRESS
Command26=ID_CUSTOMIZE_DROPDWONMENU
Command27=ID_CUSTOMIZE_ENABLECOMBO
Command28=ID_CUSTOMIZE_ENABLEEDIT
Command29=ID_CUSTOMIZE_ENABLEPROGRESS
CommandCount=29

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[TB:IDR_MAINFRAME1]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
Command9=ID_CUSTOMIZE_ENABLECOMBO
Command10=ID_CUSTOMIZE_COMBO
Command11=ID_CUSTOMIZE_ENABLEPROGRESS
Command12=ID_CUSTOMIZE_PROGRESS
Command13=ID_CUSTOMIZE_ENABLEEDIT
Command14=ID_CUSTOMIZE_EDIT
Command15=ID_CUSTOMIZE_DROPDWONMENU
CommandCount=15

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_WINDOW_NEW
Command2=ID_WINDOW_CASCADE
Command3=ID_WINDOW_TILE_HORZ
Command4=ID_WINDOW_ARRANGE
CommandCount=4

[DLG:IDD_CUSTOMIZE_TOOLBAR_DLG (Neutral)]
Type=1
Class=CTbDlg
ControlCount=7
Control1=IDC_COMBO1,combobox,1344339971
Control2=IDC_TOOLTIP_ECHO,static,1342308352
Control3=IDCANCEL,button,1342242816
Control4=IDC_STATIC,button,1342177287
Control5=IDC_ICON_FRAMES,static,1073741831
Control6=IDC_STATIC,button,1342177287
Control7=IDC_STATIC,static,1342308352

[CLS:CTbDlg]
Type=0
HeaderFile=TbDlg.h
ImplementationFile=TbDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=ID_CUSTOMIZE_ENABLE
VirtualFilter=dWC

