// View.cpp : implementation of the CTBCUSTView class
//

#include "stdafx.h"
#include "TBCUST.h"

#include "Doc.h"
#include "View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTBCUSTView

IMPLEMENT_DYNCREATE(CTBCUSTView, CView)

BEGIN_MESSAGE_MAP(CTBCUSTView, CView)
	//{{AFX_MSG_MAP(CTBCUSTView)
	ON_COMMAND(ID_CUSTOMIZE_COMBO, OnCustomizeCombo)
	ON_UPDATE_COMMAND_UI(ID_CUSTOMIZE_COMBO, OnUpdateCustomizeCombo)
	ON_COMMAND(ID_CUSTOMIZE_EDIT, OnCustomizeEdit)
	ON_UPDATE_COMMAND_UI(ID_CUSTOMIZE_EDIT, OnUpdateCustomizeEdit)
	ON_COMMAND(ID_CUSTOMIZE_PROGRESS, OnCustomizeProgress)
	ON_UPDATE_COMMAND_UI(ID_CUSTOMIZE_PROGRESS, OnUpdateCustomizeProgress)
	ON_COMMAND(ID_CUSTOMIZE_ENABLECOMBO, OnCustomizeEnablecombo)
	ON_COMMAND(ID_CUSTOMIZE_ENABLEEDIT, OnCustomizeEnableedit)
	ON_COMMAND(ID_CUSTOMIZE_ENABLEPROGRESS, OnCustomizeEnableprogress)
	ON_UPDATE_COMMAND_UI(ID_CUSTOMIZE_ENABLECOMBO, OnUpdateCustomizeEnablecombo)
	ON_UPDATE_COMMAND_UI(ID_CUSTOMIZE_ENABLEEDIT, OnUpdateCustomizeEnableedit)
	ON_UPDATE_COMMAND_UI(ID_CUSTOMIZE_ENABLEPROGRESS, OnUpdateCustomizeEnableprogress)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTBCUSTView construction/destruction

CTBCUSTView::CTBCUSTView()
{
   m_bCombo    = FALSE;
   m_bEdit     = FALSE;
   m_bProgress = FALSE;
}

CTBCUSTView::~CTBCUSTView()
{
}

BOOL CTBCUSTView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTBCUSTView drawing

void CTBCUSTView::OnDraw(CDC* pDC)
{
	CTBCUSTDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CTBCUSTView printing

BOOL CTBCUSTView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTBCUSTView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTBCUSTView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTBCUSTView diagnostics

#ifdef _DEBUG
void CTBCUSTView::AssertValid() const
{
	CView::AssertValid();
}

void CTBCUSTView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTBCUSTDoc* CTBCUSTView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTBCUSTDoc)));
	return (CTBCUSTDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTBCUSTView message handlers

void CTBCUSTView::OnCustomizeCombo() 
{
	// TODO: Add your command handler code here
	
}

void CTBCUSTView::OnUpdateCustomizeCombo(CCmdUI* pCmdUI) 
{
      pCmdUI->Enable( m_bCombo );
}

void CTBCUSTView::OnCustomizeEdit() 
{
}

void CTBCUSTView::OnUpdateCustomizeEdit(CCmdUI* pCmdUI) 
{
      pCmdUI->Enable( m_bEdit );
}

void CTBCUSTView::OnCustomizeProgress() 
{
	// TODO: Add your command handler code here
	
}

void CTBCUSTView::OnUpdateCustomizeProgress(CCmdUI* pCmdUI) 
{
      pCmdUI->Enable( m_bProgress );
}

void CTBCUSTView::OnCustomizeEnablecombo() 
{
      m_bCombo = !m_bCombo;
}

void CTBCUSTView::OnCustomizeEnableedit() 
{
      m_bEdit=!m_bEdit;
}

void CTBCUSTView::OnCustomizeEnableprogress() 
{
      m_bProgress=!m_bProgress;
}

void CTBCUSTView::OnUpdateCustomizeEnablecombo(CCmdUI* pCmdUI) 
{
      pCmdUI->Enable( TRUE );
      pCmdUI->SetCheck( m_bCombo );
}

void CTBCUSTView::OnUpdateCustomizeEnableedit(CCmdUI* pCmdUI) 
{
      pCmdUI->Enable( TRUE );
      pCmdUI->SetCheck( m_bEdit );
}

void CTBCUSTView::OnUpdateCustomizeEnableprogress(CCmdUI* pCmdUI) 
{
      pCmdUI->Enable( TRUE );
      pCmdUI->SetCheck( m_bProgress );
}
