alter table partyk add (
anz_pers smallint,
einzel_preis smallint,
nur_servicepreis smallint,
ang_gew decimal (10,3),
ang_vk decimal (10,2),
pauschal_vk decimal (10,2),
einzel_vk decimal (10,2),
mwst_voll smallint
);
