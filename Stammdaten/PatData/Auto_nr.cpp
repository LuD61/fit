#include "stdafx.h"
#include "auto_nr.h"

struct AUTO_NR auto_nr, auto_nr_null;

void AUTO_NR_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &auto_nr.fil, SQLSHORT, 0);
            sqlin ((long *)    &auto_nr.nr_nr, SQLLONG, 0);
            sqlin ((short *)   &auto_nr.satz_kng, SQLSHORT, 0);
            sqlin ((char *)    auto_nr.nr_nam, SQLCHAR, sizeof (auto_nr.nr_nam));
    sqlout ((short *) &auto_nr.mdn,SQLSHORT,0);
    sqlout ((short *) &auto_nr.fil,SQLSHORT,0);
    sqlout ((TCHAR *) auto_nr.nr_nam,SQLCHAR,11);
    sqlout ((long *) &auto_nr.nr_nr,SQLLONG,0);
    sqlout ((short *) &auto_nr.satz_kng,SQLSHORT,0);
    sqlout ((long *) &auto_nr.max_wert,SQLLONG,0);
    sqlout ((TCHAR *) auto_nr.nr_char,SQLCHAR,11);
    sqlout ((long *) &auto_nr.nr_char_lng,SQLLONG,0);
    sqlout ((TCHAR *) auto_nr.fest_teil,SQLCHAR,11);
    sqlout ((TCHAR *) auto_nr.nr_komb,SQLCHAR,21);
    sqlout ((short *) &auto_nr.delstatus,SQLSHORT,0);
            cursor = sqlcursor (_T("select auto_nr.mdn,  ")
_T("auto_nr.fil,  auto_nr.nr_nam,  auto_nr.nr_nr,  auto_nr.satz_kng,  ")
_T("auto_nr.max_wert,  auto_nr.nr_char,  auto_nr.nr_char_lng,  ")
_T("auto_nr.fest_teil,  auto_nr.nr_komb,  auto_nr.delstatus from auto_nr ")

#line 16 "auto_nr.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and nr_nr = ? ")
                                  _T("and satz_kng = ? ")
                                  _T("and nr_nam = ? "));
    sqlin ((short *) &auto_nr.mdn,SQLSHORT,0);
    sqlin ((short *) &auto_nr.fil,SQLSHORT,0);
    sqlin ((TCHAR *) auto_nr.nr_nam,SQLCHAR,11);
    sqlin ((long *) &auto_nr.nr_nr,SQLLONG,0);
    sqlin ((short *) &auto_nr.satz_kng,SQLSHORT,0);
    sqlin ((long *) &auto_nr.max_wert,SQLLONG,0);
    sqlin ((TCHAR *) auto_nr.nr_char,SQLCHAR,11);
    sqlin ((long *) &auto_nr.nr_char_lng,SQLLONG,0);
    sqlin ((TCHAR *) auto_nr.fest_teil,SQLCHAR,11);
    sqlin ((TCHAR *) auto_nr.nr_komb,SQLCHAR,21);
    sqlin ((short *) &auto_nr.delstatus,SQLSHORT,0);
            sqltext = _T("update auto_nr set auto_nr.mdn = ?,  ")
_T("auto_nr.fil = ?,  auto_nr.nr_nam = ?,  auto_nr.nr_nr = ?,  ")
_T("auto_nr.satz_kng = ?,  auto_nr.max_wert = ?,  auto_nr.nr_char = ?,  ")
_T("auto_nr.nr_char_lng = ?,  auto_nr.fest_teil = ?,  ")
_T("auto_nr.nr_komb = ?,  auto_nr.delstatus = ? ")

#line 22 "auto_nr.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and nr_nr = ? ")
                                  _T("and satz_kng = ? ")
                                  _T("and nr_nam = ? ");
            sqlin ((short *)   &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &auto_nr.fil, SQLSHORT, 0);
            sqlin ((long *)    &auto_nr.nr_nr, SQLLONG, 0);
            sqlin ((short *)   &auto_nr.satz_kng, SQLSHORT, 0);
            sqlin ((char *)    auto_nr.nr_nam, SQLCHAR, sizeof (auto_nr.nr_nam));
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &auto_nr.fil, SQLSHORT, 0);
            sqlin ((long *)    &auto_nr.nr_nr, SQLLONG, 0);
            sqlin ((short *)   &auto_nr.satz_kng, SQLSHORT, 0);
            sqlin ((char *)    auto_nr.nr_nam, SQLCHAR, sizeof (auto_nr.nr_nam));
            test_upd_cursor = sqlcursor (_T("select nr_nr from auto_nr ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and nr_nr = ? ")
                                  _T("and satz_kng = ? ")
                                  _T("and nr_nam = ? "));
            sqlin ((short *)   &auto_nr.mdn, SQLSHORT, 0);
            sqlin ((short *)   &auto_nr.fil, SQLSHORT, 0);
            sqlin ((long *)    &auto_nr.nr_nr, SQLLONG, 0);
            sqlin ((short *)   &auto_nr.satz_kng, SQLSHORT, 0);
            sqlin ((char *)    auto_nr.nr_nam, SQLCHAR, sizeof (auto_nr.nr_nam));
            del_cursor = sqlcursor (_T("delete from auto_nr ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and nr_nr = ? ")
                                  _T("and satz_kng = ? ")
                                  _T("and nr_nam = ? "));
    sqlin ((short *) &auto_nr.mdn,SQLSHORT,0);
    sqlin ((short *) &auto_nr.fil,SQLSHORT,0);
    sqlin ((TCHAR *) auto_nr.nr_nam,SQLCHAR,11);
    sqlin ((long *) &auto_nr.nr_nr,SQLLONG,0);
    sqlin ((short *) &auto_nr.satz_kng,SQLSHORT,0);
    sqlin ((long *) &auto_nr.max_wert,SQLLONG,0);
    sqlin ((TCHAR *) auto_nr.nr_char,SQLCHAR,11);
    sqlin ((long *) &auto_nr.nr_char_lng,SQLLONG,0);
    sqlin ((TCHAR *) auto_nr.fest_teil,SQLCHAR,11);
    sqlin ((TCHAR *) auto_nr.nr_komb,SQLCHAR,21);
    sqlin ((short *) &auto_nr.delstatus,SQLSHORT,0);
            ins_cursor = sqlcursor (_T("insert into auto_nr (")
_T("mdn,  fil,  nr_nam,  nr_nr,  satz_kng,  max_wert,  nr_char,  nr_char_lng,  fest_teil,  ")
_T("nr_komb,  delstatus) ")

#line 57 "auto_nr.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?)")); 

#line 59 "auto_nr.rpp"
}

