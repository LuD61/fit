#include "stdafx.h"
#include "a_eig_div.h"

struct A_EIG_DIV a_eig_div, a_eig_div_null;

void A_EIG_DIV_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_eig_div.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_eig_div.a,SQLDOUBLE,0);
    sqlout ((long *) &a_eig_div.a_krz,SQLLONG,0);
    sqlout ((short *) &a_eig_div.anz_reg_eti,SQLSHORT,0);
    sqlout ((short *) &a_eig_div.anz_theke_eti,SQLSHORT,0);
    sqlout ((short *) &a_eig_div.anz_waren_eti,SQLSHORT,0);
    sqlout ((short *) &a_eig_div.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_eig_div.fil,SQLSHORT,0);
    sqlout ((double *) &a_eig_div.gew_bto,SQLDOUBLE,0);
    sqlout ((double *) &a_eig_div.inh,SQLDOUBLE,0);
    sqlout ((short *) &a_eig_div.mdn,SQLSHORT,0);
    sqlout ((short *) &a_eig_div.me_einh_ek,SQLSHORT,0);
    sqlout ((TCHAR *) a_eig_div.pr_ausz,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig_div.pr_man,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig_div.pr_ueb,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig_div.reg_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig_div.rez,SQLCHAR,9);
    sqlout ((short *) &a_eig_div.sg1,SQLSHORT,0);
    sqlout ((short *) &a_eig_div.sg2,SQLSHORT,0);
    sqlout ((double *) &a_eig_div.tara,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_eig_div.theke_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig_div.verk_art,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig_div.vpk_kz,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig_div.waren_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig_div.mwst_ueb,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &a_eig_div.verk_beg,SQLDATE,0);
            cursor = sqlcursor (_T("select a_eig_div.a,  ")
_T("a_eig_div.a_krz,  a_eig_div.anz_reg_eti,  a_eig_div.anz_theke_eti,  ")
_T("a_eig_div.anz_waren_eti,  a_eig_div.delstatus,  a_eig_div.fil,  ")
_T("a_eig_div.gew_bto,  a_eig_div.inh,  a_eig_div.mdn,  ")
_T("a_eig_div.me_einh_ek,  a_eig_div.pr_ausz,  a_eig_div.pr_man,  ")
_T("a_eig_div.pr_ueb,  a_eig_div.reg_eti,  a_eig_div.rez,  a_eig_div.sg1,  ")
_T("a_eig_div.sg2,  a_eig_div.tara,  a_eig_div.theke_eti,  ")
_T("a_eig_div.verk_art,  a_eig_div.vpk_kz,  a_eig_div.waren_eti,  ")
_T("a_eig_div.mwst_ueb,  a_eig_div.verk_beg from a_eig_div ")

#line 12 "a_eig_div.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_eig_div.a,SQLDOUBLE,0);
    sqlin ((long *) &a_eig_div.a_krz,SQLLONG,0);
    sqlin ((short *) &a_eig_div.anz_reg_eti,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.anz_theke_eti,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.anz_waren_eti,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.fil,SQLSHORT,0);
    sqlin ((double *) &a_eig_div.gew_bto,SQLDOUBLE,0);
    sqlin ((double *) &a_eig_div.inh,SQLDOUBLE,0);
    sqlin ((short *) &a_eig_div.mdn,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.me_einh_ek,SQLSHORT,0);
    sqlin ((TCHAR *) a_eig_div.pr_ausz,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.pr_man,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.pr_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.reg_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.rez,SQLCHAR,9);
    sqlin ((short *) &a_eig_div.sg1,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.sg2,SQLSHORT,0);
    sqlin ((double *) &a_eig_div.tara,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_eig_div.theke_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.verk_art,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.vpk_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.waren_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.mwst_ueb,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_eig_div.verk_beg,SQLDATE,0);
            sqltext = _T("update a_eig_div set ")
_T("a_eig_div.a = ?,  a_eig_div.a_krz = ?,  a_eig_div.anz_reg_eti = ?,  ")
_T("a_eig_div.anz_theke_eti = ?,  a_eig_div.anz_waren_eti = ?,  ")
_T("a_eig_div.delstatus = ?,  a_eig_div.fil = ?,  ")
_T("a_eig_div.gew_bto = ?,  a_eig_div.inh = ?,  a_eig_div.mdn = ?,  ")
_T("a_eig_div.me_einh_ek = ?,  a_eig_div.pr_ausz = ?,  ")
_T("a_eig_div.pr_man = ?,  a_eig_div.pr_ueb = ?,  ")
_T("a_eig_div.reg_eti = ?,  a_eig_div.rez = ?,  a_eig_div.sg1 = ?,  ")
_T("a_eig_div.sg2 = ?,  a_eig_div.tara = ?,  a_eig_div.theke_eti = ?,  ")
_T("a_eig_div.verk_art = ?,  a_eig_div.vpk_kz = ?,  ")
_T("a_eig_div.waren_eti = ?,  a_eig_div.mwst_ueb = ?,  ")
_T("a_eig_div.verk_beg = ? ")

#line 14 "a_eig_div.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_eig_div.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_eig_div.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_eig_div ")
                                  _T("where a = ?"));
            sqlin ((double *)   &a_eig_div.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_eig_div ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_eig_div.a,SQLDOUBLE,0);
    sqlin ((long *) &a_eig_div.a_krz,SQLLONG,0);
    sqlin ((short *) &a_eig_div.anz_reg_eti,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.anz_theke_eti,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.anz_waren_eti,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.fil,SQLSHORT,0);
    sqlin ((double *) &a_eig_div.gew_bto,SQLDOUBLE,0);
    sqlin ((double *) &a_eig_div.inh,SQLDOUBLE,0);
    sqlin ((short *) &a_eig_div.mdn,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.me_einh_ek,SQLSHORT,0);
    sqlin ((TCHAR *) a_eig_div.pr_ausz,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.pr_man,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.pr_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.reg_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.rez,SQLCHAR,9);
    sqlin ((short *) &a_eig_div.sg1,SQLSHORT,0);
    sqlin ((short *) &a_eig_div.sg2,SQLSHORT,0);
    sqlin ((double *) &a_eig_div.tara,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_eig_div.theke_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.verk_art,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.vpk_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.waren_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig_div.mwst_ueb,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_eig_div.verk_beg,SQLDATE,0);
            ins_cursor = sqlcursor (_T("insert into a_eig_div (")
_T("a,  a_krz,  anz_reg_eti,  anz_theke_eti,  anz_waren_eti,  delstatus,  fil,  ")
_T("gew_bto,  inh,  mdn,  me_einh_ek,  pr_ausz,  pr_man,  pr_ueb,  reg_eti,  rez,  sg1,  sg2,  ")
_T("tara,  theke_eti,  verk_art,  vpk_kz,  waren_eti,  mwst_ueb,  verk_beg) ")

#line 25 "a_eig_div.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 27 "a_eig_div.rpp"
}
