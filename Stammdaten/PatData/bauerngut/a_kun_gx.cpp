#include "stdafx.h"
#include "a_kun_gx.h"

struct A_KUN_GX a_kun_gx, a_kun_gx_null;

void A_KUN_GX_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &a_kun_gx.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &a_kun_gx.fil,  SQLSHORT, 0);
            sqlin ((long *)    &a_kun_gx.kun,  SQLLONG, 0);
            sqlin ((char *)    a_kun_gx.kun_bran2,  SQLCHAR, sizeof a_kun_gx.kun_bran2);
            sqlin ((double *)  &a_kun_gx.a,  SQLDOUBLE, 0);
    sqlout ((short *) &a_kun_gx.mdn,SQLSHORT,0);
    sqlout ((short *) &a_kun_gx.fil,SQLSHORT,0);
    sqlout ((long *) &a_kun_gx.kun,SQLLONG,0);
    sqlout ((double *) &a_kun_gx.a,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun_gx.a_kun,SQLCHAR,13);
    sqlout ((TCHAR *) a_kun_gx.a_bz1,SQLCHAR,25);
    sqlout ((short *) &a_kun_gx.me_einh_kun,SQLSHORT,0);
    sqlout ((double *) &a_kun_gx.inh,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun_gx.kun_bran2,SQLCHAR,3);
    sqlout ((double *) &a_kun_gx.tara,SQLDOUBLE,0);
    sqlout ((double *) &a_kun_gx.ean,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_kun_gx.a_bz2,SQLCHAR,25);
    sqlout ((short *) &a_kun_gx.hbk_ztr,SQLSHORT,0);
    sqlout ((long *) &a_kun_gx.kopf_text,SQLLONG,0);
    sqlout ((TCHAR *) a_kun_gx.pr_rech_kz,SQLCHAR,2);
    sqlout ((TCHAR *) a_kun_gx.modif,SQLCHAR,2);
    sqlout ((long *) &a_kun_gx.text_nr,SQLLONG,0);
    sqlout ((short *) &a_kun_gx.devise,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun_gx.geb_eti,SQLCHAR,2);
    sqlout ((short *) &a_kun_gx.geb_fill,SQLSHORT,0);
    sqlout ((long *) &a_kun_gx.geb_anz,SQLLONG,0);
    sqlout ((TCHAR *) a_kun_gx.pal_eti,SQLCHAR,2);
    sqlout ((short *) &a_kun_gx.pal_fill,SQLSHORT,0);
    sqlout ((long *) &a_kun_gx.pal_anz,SQLLONG,0);
    sqlout ((TCHAR *) a_kun_gx.pos_eti,SQLCHAR,2);
    sqlout ((short *) &a_kun_gx.sg1,SQLSHORT,0);
    sqlout ((short *) &a_kun_gx.sg2,SQLSHORT,0);
    sqlout ((short *) &a_kun_gx.pos_fill,SQLSHORT,0);
    sqlout ((short *) &a_kun_gx.ausz_art,SQLSHORT,0);
    sqlout ((long *) &a_kun_gx.text_nr2,SQLLONG,0);
    sqlout ((short *) &a_kun_gx.cab,SQLSHORT,0);
    sqlout ((TCHAR *) a_kun_gx.a_bz3,SQLCHAR,100);
    sqlout ((TCHAR *) a_kun_gx.a_bz4,SQLCHAR,100);
    sqlout ((short *) &a_kun_gx.eti_typ,SQLSHORT,0);
    sqlout ((long *) &a_kun_gx.mhd_text,SQLLONG,0);
    sqlout ((long *) &a_kun_gx.freitext1,SQLLONG,0);
    sqlout ((long *) &a_kun_gx.freitext2,SQLLONG,0);
    sqlout ((long *) &a_kun_gx.freitext3,SQLLONG,0);
    sqlout ((short *) &a_kun_gx.sg3,SQLSHORT,0);
    sqlout ((short *) &a_kun_gx.eti_sum1,SQLSHORT,0);
    sqlout ((short *) &a_kun_gx.eti_sum2,SQLSHORT,0);
    sqlout ((short *) &a_kun_gx.eti_sum3,SQLSHORT,0);
    sqlout ((long *) &a_kun_gx.ampar,SQLLONG,0);
    sqlout ((short *) &a_kun_gx.sonder_eti,SQLSHORT,0);
    sqlout ((long *) &a_kun_gx.text_nr_a,SQLLONG,0);
    sqlout ((double *) &a_kun_gx.ean1,SQLDOUBLE,0);
    sqlout ((short *) &a_kun_gx.cab1,SQLSHORT,0);
    sqlout ((double *) &a_kun_gx.ean2,SQLDOUBLE,0);
    sqlout ((short *) &a_kun_gx.cab2,SQLSHORT,0);
    sqlout ((double *) &a_kun_gx.ean3,SQLDOUBLE,0);
    sqlout ((short *) &a_kun_gx.cab3,SQLSHORT,0);
    sqlout ((long *) &a_kun_gx.gwpar,SQLLONG,0);
    sqlout ((long *) &a_kun_gx.vppar,SQLLONG,0);
    sqlout ((short *) &a_kun_gx.devise2,SQLSHORT,0);
    sqlout ((short *) &a_kun_gx.eti_nve1,SQLSHORT,0);
    sqlout ((double *) &a_kun_gx.ean_nve1,SQLDOUBLE,0);
    sqlout ((short *) &a_kun_gx.cab_nve1,SQLSHORT,0);
    sqlout ((short *) &a_kun_gx.eti_nve2,SQLSHORT,0);
    sqlout ((double *) &a_kun_gx.ean_nve2,SQLDOUBLE,0);
    sqlout ((short *) &a_kun_gx.cab_nve2,SQLSHORT,0);
    sqlout ((double *) &a_kun_gx.zut_gew,SQLDOUBLE,0);
    sqlout ((double *) &a_kun_gx.zut_proz,SQLDOUBLE,0);
    sqlout ((long *) &a_kun_gx.freitext4,SQLLONG,0);
    sqlout ((long *) &a_kun_gx.freitext5,SQLLONG,0);
    sqlout ((long *) &a_kun_gx.freitext6,SQLLONG,0);
    sqlout ((long *) &a_kun_gx.freitext7,SQLLONG,0);
            cursor = sqlcursor (_T("select a_kun_gx.mdn,  ")
_T("a_kun_gx.fil,  a_kun_gx.kun,  a_kun_gx.a,  a_kun_gx.a_kun,  ")
_T("a_kun_gx.a_bz1,  a_kun_gx.me_einh_kun,  a_kun_gx.inh,  ")
_T("a_kun_gx.kun_bran2,  a_kun_gx.tara,  a_kun_gx.ean,  a_kun_gx.a_bz2,  ")
_T("a_kun_gx.hbk_ztr,  a_kun_gx.kopf_text,  a_kun_gx.pr_rech_kz,  ")
_T("a_kun_gx.modif,  a_kun_gx.text_nr,  a_kun_gx.devise,  ")
_T("a_kun_gx.geb_eti,  a_kun_gx.geb_fill,  a_kun_gx.geb_anz,  ")
_T("a_kun_gx.pal_eti,  a_kun_gx.pal_fill,  a_kun_gx.pal_anz,  ")
_T("a_kun_gx.pos_eti,  a_kun_gx.sg1,  a_kun_gx.sg2,  a_kun_gx.pos_fill,  ")
_T("a_kun_gx.ausz_art,  a_kun_gx.text_nr2,  a_kun_gx.cab,  a_kun_gx.a_bz3,  ")
_T("a_kun_gx.a_bz4,  a_kun_gx.eti_typ,  a_kun_gx.mhd_text,  ")
_T("a_kun_gx.freitext1,  a_kun_gx.freitext2,  a_kun_gx.freitext3,  ")
_T("a_kun_gx.sg3,  a_kun_gx.eti_sum1,  a_kun_gx.eti_sum2,  ")
_T("a_kun_gx.eti_sum3,  a_kun_gx.ampar,  a_kun_gx.sonder_eti,  ")
_T("a_kun_gx.text_nr_a,  a_kun_gx.ean1,  a_kun_gx.cab1,  a_kun_gx.ean2,  ")
_T("a_kun_gx.cab2,  a_kun_gx.ean3,  a_kun_gx.cab3,  a_kun_gx.gwpar,  ")
_T("a_kun_gx.vppar,  a_kun_gx.devise2,  a_kun_gx.eti_nve1,  ")
_T("a_kun_gx.ean_nve1,  a_kun_gx.cab_nve1,  a_kun_gx.eti_nve2,  ")
_T("a_kun_gx.ean_nve2,  a_kun_gx.cab_nve2,  a_kun_gx.zut_gew,  ")
_T("a_kun_gx.zut_proz,  a_kun_gx.freitext4,  a_kun_gx.freitext5,  ")
_T("a_kun_gx.freitext6,  a_kun_gx.freitext7 from a_kun_gx ")

#line 16 "a_kun_gx.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and kun_bran2 = ? ")
                                  _T("and a = ?"));
    sqlin ((short *) &a_kun_gx.mdn,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.fil,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.kun,SQLLONG,0);
    sqlin ((double *) &a_kun_gx.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun_gx.a_kun,SQLCHAR,13);
    sqlin ((TCHAR *) a_kun_gx.a_bz1,SQLCHAR,25);
    sqlin ((short *) &a_kun_gx.me_einh_kun,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.inh,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun_gx.kun_bran2,SQLCHAR,3);
    sqlin ((double *) &a_kun_gx.tara,SQLDOUBLE,0);
    sqlin ((double *) &a_kun_gx.ean,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun_gx.a_bz2,SQLCHAR,25);
    sqlin ((short *) &a_kun_gx.hbk_ztr,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.kopf_text,SQLLONG,0);
    sqlin ((TCHAR *) a_kun_gx.pr_rech_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_kun_gx.modif,SQLCHAR,2);
    sqlin ((long *) &a_kun_gx.text_nr,SQLLONG,0);
    sqlin ((short *) &a_kun_gx.devise,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun_gx.geb_eti,SQLCHAR,2);
    sqlin ((short *) &a_kun_gx.geb_fill,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.geb_anz,SQLLONG,0);
    sqlin ((TCHAR *) a_kun_gx.pal_eti,SQLCHAR,2);
    sqlin ((short *) &a_kun_gx.pal_fill,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.pal_anz,SQLLONG,0);
    sqlin ((TCHAR *) a_kun_gx.pos_eti,SQLCHAR,2);
    sqlin ((short *) &a_kun_gx.sg1,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.sg2,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.pos_fill,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.ausz_art,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.text_nr2,SQLLONG,0);
    sqlin ((short *) &a_kun_gx.cab,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun_gx.a_bz3,SQLCHAR,100);
    sqlin ((TCHAR *) a_kun_gx.a_bz4,SQLCHAR,100);
    sqlin ((short *) &a_kun_gx.eti_typ,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.mhd_text,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext1,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext2,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext3,SQLLONG,0);
    sqlin ((short *) &a_kun_gx.sg3,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.eti_sum1,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.eti_sum2,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.eti_sum3,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.ampar,SQLLONG,0);
    sqlin ((short *) &a_kun_gx.sonder_eti,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.text_nr_a,SQLLONG,0);
    sqlin ((double *) &a_kun_gx.ean1,SQLDOUBLE,0);
    sqlin ((short *) &a_kun_gx.cab1,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.ean2,SQLDOUBLE,0);
    sqlin ((short *) &a_kun_gx.cab2,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.ean3,SQLDOUBLE,0);
    sqlin ((short *) &a_kun_gx.cab3,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.gwpar,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.vppar,SQLLONG,0);
    sqlin ((short *) &a_kun_gx.devise2,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.eti_nve1,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.ean_nve1,SQLDOUBLE,0);
    sqlin ((short *) &a_kun_gx.cab_nve1,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.eti_nve2,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.ean_nve2,SQLDOUBLE,0);
    sqlin ((short *) &a_kun_gx.cab_nve2,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.zut_gew,SQLDOUBLE,0);
    sqlin ((double *) &a_kun_gx.zut_proz,SQLDOUBLE,0);
    sqlin ((long *) &a_kun_gx.freitext4,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext5,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext6,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext7,SQLLONG,0);
            sqltext = _T("update a_kun_gx set ")
_T("a_kun_gx.mdn = ?,  a_kun_gx.fil = ?,  a_kun_gx.kun = ?,  ")
_T("a_kun_gx.a = ?,  a_kun_gx.a_kun = ?,  a_kun_gx.a_bz1 = ?,  ")
_T("a_kun_gx.me_einh_kun = ?,  a_kun_gx.inh = ?,  ")
_T("a_kun_gx.kun_bran2 = ?,  a_kun_gx.tara = ?,  a_kun_gx.ean = ?,  ")
_T("a_kun_gx.a_bz2 = ?,  a_kun_gx.hbk_ztr = ?,  a_kun_gx.kopf_text = ?,  ")
_T("a_kun_gx.pr_rech_kz = ?,  a_kun_gx.modif = ?,  ")
_T("a_kun_gx.text_nr = ?,  a_kun_gx.devise = ?,  a_kun_gx.geb_eti = ?,  ")
_T("a_kun_gx.geb_fill = ?,  a_kun_gx.geb_anz = ?,  ")
_T("a_kun_gx.pal_eti = ?,  a_kun_gx.pal_fill = ?,  ")
_T("a_kun_gx.pal_anz = ?,  a_kun_gx.pos_eti = ?,  a_kun_gx.sg1 = ?,  ")
_T("a_kun_gx.sg2 = ?,  a_kun_gx.pos_fill = ?,  a_kun_gx.ausz_art = ?,  ")
_T("a_kun_gx.text_nr2 = ?,  a_kun_gx.cab = ?,  a_kun_gx.a_bz3 = ?,  ")
_T("a_kun_gx.a_bz4 = ?,  a_kun_gx.eti_typ = ?,  a_kun_gx.mhd_text = ?,  ")
_T("a_kun_gx.freitext1 = ?,  a_kun_gx.freitext2 = ?,  ")
_T("a_kun_gx.freitext3 = ?,  a_kun_gx.sg3 = ?,  a_kun_gx.eti_sum1 = ?,  ")
_T("a_kun_gx.eti_sum2 = ?,  a_kun_gx.eti_sum3 = ?,  a_kun_gx.ampar = ?,  ")
_T("a_kun_gx.sonder_eti = ?,  a_kun_gx.text_nr_a = ?,  ")
_T("a_kun_gx.ean1 = ?,  a_kun_gx.cab1 = ?,  a_kun_gx.ean2 = ?,  ")
_T("a_kun_gx.cab2 = ?,  a_kun_gx.ean3 = ?,  a_kun_gx.cab3 = ?,  ")
_T("a_kun_gx.gwpar = ?,  a_kun_gx.vppar = ?,  a_kun_gx.devise2 = ?,  ")
_T("a_kun_gx.eti_nve1 = ?,  a_kun_gx.ean_nve1 = ?,  ")
_T("a_kun_gx.cab_nve1 = ?,  a_kun_gx.eti_nve2 = ?,  ")
_T("a_kun_gx.ean_nve2 = ?,  a_kun_gx.cab_nve2 = ?,  ")
_T("a_kun_gx.zut_gew = ?,  a_kun_gx.zut_proz = ?,  ")
_T("a_kun_gx.freitext4 = ?,  a_kun_gx.freitext5 = ?,  ")
_T("a_kun_gx.freitext6 = ?,  a_kun_gx.freitext7 = ? ")

#line 22 "a_kun_gx.rpp"
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and kun_bran2 = ? ")
                                  _T("and a = ?");
            sqlin ((short *)   &a_kun_gx.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &a_kun_gx.fil,  SQLSHORT, 0);
            sqlin ((long *)    &a_kun_gx.kun,  SQLLONG, 0);
            sqlin ((char *)    a_kun_gx.kun_bran2,  SQLCHAR, sizeof a_kun_gx.kun_bran2);
            sqlin ((double *)  &a_kun_gx.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &a_kun_gx.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &a_kun_gx.fil,  SQLSHORT, 0);
            sqlin ((long *)    &a_kun_gx.kun,  SQLLONG, 0);
            sqlin ((char *)    a_kun_gx.kun_bran2,  SQLCHAR, sizeof a_kun_gx.kun_bran2);
            sqlin ((double *)  &a_kun_gx.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_kun_gx ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and kun_bran2 = ? ")
                                  _T("and a = ?"));
            sqlin ((short *)   &a_kun_gx.mdn,  SQLSHORT, 0);
            sqlin ((short *)   &a_kun_gx.fil,  SQLSHORT, 0);
            sqlin ((long *)    &a_kun_gx.kun,  SQLLONG, 0);
            sqlin ((char *)    a_kun_gx.kun_bran2,  SQLCHAR, sizeof a_kun_gx.kun_bran2);
            sqlin ((double *)  &a_kun_gx.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_kun_gx ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and kun = ? ")
                                  _T("and kun_bran2 = ? ")
                                  _T("and a = ?"));
    sqlin ((short *) &a_kun_gx.mdn,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.fil,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.kun,SQLLONG,0);
    sqlin ((double *) &a_kun_gx.a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun_gx.a_kun,SQLCHAR,13);
    sqlin ((TCHAR *) a_kun_gx.a_bz1,SQLCHAR,25);
    sqlin ((short *) &a_kun_gx.me_einh_kun,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.inh,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun_gx.kun_bran2,SQLCHAR,3);
    sqlin ((double *) &a_kun_gx.tara,SQLDOUBLE,0);
    sqlin ((double *) &a_kun_gx.ean,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_kun_gx.a_bz2,SQLCHAR,25);
    sqlin ((short *) &a_kun_gx.hbk_ztr,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.kopf_text,SQLLONG,0);
    sqlin ((TCHAR *) a_kun_gx.pr_rech_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_kun_gx.modif,SQLCHAR,2);
    sqlin ((long *) &a_kun_gx.text_nr,SQLLONG,0);
    sqlin ((short *) &a_kun_gx.devise,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun_gx.geb_eti,SQLCHAR,2);
    sqlin ((short *) &a_kun_gx.geb_fill,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.geb_anz,SQLLONG,0);
    sqlin ((TCHAR *) a_kun_gx.pal_eti,SQLCHAR,2);
    sqlin ((short *) &a_kun_gx.pal_fill,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.pal_anz,SQLLONG,0);
    sqlin ((TCHAR *) a_kun_gx.pos_eti,SQLCHAR,2);
    sqlin ((short *) &a_kun_gx.sg1,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.sg2,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.pos_fill,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.ausz_art,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.text_nr2,SQLLONG,0);
    sqlin ((short *) &a_kun_gx.cab,SQLSHORT,0);
    sqlin ((TCHAR *) a_kun_gx.a_bz3,SQLCHAR,100);
    sqlin ((TCHAR *) a_kun_gx.a_bz4,SQLCHAR,100);
    sqlin ((short *) &a_kun_gx.eti_typ,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.mhd_text,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext1,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext2,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext3,SQLLONG,0);
    sqlin ((short *) &a_kun_gx.sg3,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.eti_sum1,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.eti_sum2,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.eti_sum3,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.ampar,SQLLONG,0);
    sqlin ((short *) &a_kun_gx.sonder_eti,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.text_nr_a,SQLLONG,0);
    sqlin ((double *) &a_kun_gx.ean1,SQLDOUBLE,0);
    sqlin ((short *) &a_kun_gx.cab1,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.ean2,SQLDOUBLE,0);
    sqlin ((short *) &a_kun_gx.cab2,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.ean3,SQLDOUBLE,0);
    sqlin ((short *) &a_kun_gx.cab3,SQLSHORT,0);
    sqlin ((long *) &a_kun_gx.gwpar,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.vppar,SQLLONG,0);
    sqlin ((short *) &a_kun_gx.devise2,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.eti_nve1,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.ean_nve1,SQLDOUBLE,0);
    sqlin ((short *) &a_kun_gx.cab_nve1,SQLSHORT,0);
    sqlin ((short *) &a_kun_gx.eti_nve2,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.ean_nve2,SQLDOUBLE,0);
    sqlin ((short *) &a_kun_gx.cab_nve2,SQLSHORT,0);
    sqlin ((double *) &a_kun_gx.zut_gew,SQLDOUBLE,0);
    sqlin ((double *) &a_kun_gx.zut_proz,SQLDOUBLE,0);
    sqlin ((long *) &a_kun_gx.freitext4,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext5,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext6,SQLLONG,0);
    sqlin ((long *) &a_kun_gx.freitext7,SQLLONG,0);
            ins_cursor = sqlcursor (_T("insert into a_kun_gx (")
_T("mdn,  fil,  kun,  a,  a_kun,  a_bz1,  me_einh_kun,  inh,  kun_bran2,  tara,  ean,  a_bz2,  ")
_T("hbk_ztr,  kopf_text,  pr_rech_kz,  modif,  text_nr,  devise,  geb_eti,  geb_fill,  ")
_T("geb_anz,  pal_eti,  pal_fill,  pal_anz,  pos_eti,  sg1,  sg2,  pos_fill,  ausz_art,  ")
_T("text_nr2,  cab,  a_bz3,  a_bz4,  eti_typ,  mhd_text,  freitext1,  freitext2,  ")
_T("freitext3,  sg3,  eti_sum1,  eti_sum2,  eti_sum3,  ampar,  sonder_eti,  text_nr_a,  ")
_T("ean1,  cab1,  ean2,  cab2,  ean3,  cab3,  gwpar,  vppar,  devise2,  eti_nve1,  ean_nve1,  ")
_T("cab_nve1,  eti_nve2,  ean_nve2,  cab_nve2,  zut_gew,  zut_proz,  freitext4,  ")
_T("freitext5,  freitext6,  freitext7) ")

#line 57 "a_kun_gx.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 59 "a_kun_gx.rpp"
}

void A_KUN_GX_CLASS::clone (void *a_kun_gx)
{
            memcpy ((A_KUN_GX *) a_kun_gx, &this->a_kun_gx, sizeof (A_KUN_GX));
}

void A_KUN_GX_CLASS::CopyData (void *a_kun_gx)
{
/*
            short mdn = this->a_kun_gx.mdn;
            short fil = this->a_kun_gx.fil;
            long kun = this->a_kun_gx.kun;
            _TCHAR kun_bran2 [sizeof (this->a_kun_gx.kun_bran2)];
            _TCHAR a_bz3 [sizeof (this->a_kun_gx.a_bz3)];
            _TCHAR a_bz4 [sizeof (this->a_kun_gx.a_bz4)];
            _tcscpy (kun_bran2, this->a_kun_gx.kun_bran2);
            _tcscpy (a_bz3, this->a_kun_gx.a_bz3);
            _tcscpy (a_bz4, this->a_kun_gx.a_bz4);
            memcpy (&this->a_kun_gx, (A_KUN_GX *) a_kun_gx, sizeof (A_KUN_GX));    
            this->a_kun_gx.mdn = mdn;
            this->a_kun_gx.fil = fil;
            this->a_kun_gx.kun = kun;
            _tcscpy (this->a_kun_gx.kun_bran2, kun_bran2);
            _tcscpy (this->a_kun_gx.a_bz3, a_bz3);
            _tcscpy (this->a_kun_gx.a_bz4, a_bz4);
*/
	    this->a_kun_gx.me_einh_kun = ((A_KUN_GX *) a_kun_gx)->me_einh_kun;
	    this->a_kun_gx.inh = ((A_KUN_GX *) a_kun_gx)->inh;
	    this->a_kun_gx.tara = ((A_KUN_GX *) a_kun_gx)->tara;
//            this->a_kun_gx.ean = ((A_KUN_GX *) a_kun_gx)->ean;
            this->a_kun_gx.hbk_ztr = ((A_KUN_GX *) a_kun_gx)->hbk_ztr;
            this->a_kun_gx.kopf_text = ((A_KUN_GX *) a_kun_gx)->kopf_text;
            _tcscpy (this->a_kun_gx.pr_rech_kz,((A_KUN_GX *) a_kun_gx)->pr_rech_kz);
//            this->a_kun_gx.text_nr = ((A_KUN_GX *) a_kun_gx)->text_nr;
            this->a_kun_gx.devise = ((A_KUN_GX *) a_kun_gx)->devise;
            _tcscpy (this->a_kun_gx.geb_eti,((A_KUN_GX *) a_kun_gx)->geb_eti);
            this->a_kun_gx.geb_fill = ((A_KUN_GX *) a_kun_gx)->geb_fill;
            this->a_kun_gx.geb_anz = ((A_KUN_GX *) a_kun_gx)->geb_anz;
            _tcscpy (this->a_kun_gx.pal_eti,((A_KUN_GX *) a_kun_gx)->pal_eti);
            this->a_kun_gx.pal_fill = ((A_KUN_GX *) a_kun_gx)->pal_fill;
            this->a_kun_gx.pal_anz = ((A_KUN_GX *) a_kun_gx)->pal_anz;
            _tcscpy (this->a_kun_gx.pos_eti,((A_KUN_GX *) a_kun_gx)->pos_eti);
            this->a_kun_gx.sg1 = ((A_KUN_GX *) a_kun_gx)->sg1;
            this->a_kun_gx.sg2 = ((A_KUN_GX *) a_kun_gx)->sg2;
            this->a_kun_gx.pos_fill = ((A_KUN_GX *) a_kun_gx)->pos_fill;
            this->a_kun_gx.ausz_art = ((A_KUN_GX *) a_kun_gx)->ausz_art;
//            this->a_kun_gx.text_nr2 = ((A_KUN_GX *) a_kun_gx)->text_nr2;
            this->a_kun_gx.cab = ((A_KUN_GX *) a_kun_gx)->cab;
            this->a_kun_gx.eti_typ = ((A_KUN_GX *) a_kun_gx)->eti_typ;
            this->a_kun_gx.mhd_text = ((A_KUN_GX *) a_kun_gx)->mhd_text;
            this->a_kun_gx.freitext1 = ((A_KUN_GX *) a_kun_gx)->freitext1;
            this->a_kun_gx.freitext2 = ((A_KUN_GX *) a_kun_gx)->freitext2;
            this->a_kun_gx.freitext3 = ((A_KUN_GX *) a_kun_gx)->freitext3;
            this->a_kun_gx.sg3 = ((A_KUN_GX *) a_kun_gx)->sg3;
            this->a_kun_gx.eti_sum1 = ((A_KUN_GX *) a_kun_gx)->eti_sum1;
            this->a_kun_gx.eti_sum2 = ((A_KUN_GX *) a_kun_gx)->eti_sum2;
            this->a_kun_gx.eti_sum3 = ((A_KUN_GX *) a_kun_gx)->eti_sum3;
            this->a_kun_gx.ampar = ((A_KUN_GX *) a_kun_gx)->ampar;
            this->a_kun_gx.sonder_eti = ((A_KUN_GX *) a_kun_gx)->sonder_eti;
            this->a_kun_gx.text_nr_a = ((A_KUN_GX *) a_kun_gx)->text_nr_a;
//            this->a_kun_gx.ean1 = ((A_KUN_GX *) a_kun_gx)->ean1;
            this->a_kun_gx.cab1 = ((A_KUN_GX *) a_kun_gx)->cab1;
//            this->a_kun_gx.ean2 = ((A_KUN_GX *) a_kun_gx)->ean2;
            this->a_kun_gx.cab2 = ((A_KUN_GX *) a_kun_gx)->cab2;
//            this->a_kun_gx.ean3 = ((A_KUN_GX *) a_kun_gx)->ean3;
            this->a_kun_gx.cab3 = ((A_KUN_GX *) a_kun_gx)->cab3;
            this->a_kun_gx.gwpar = ((A_KUN_GX *) a_kun_gx)->gwpar;
            this->a_kun_gx.vppar = ((A_KUN_GX *) a_kun_gx)->vppar;
            this->a_kun_gx.devise2 = ((A_KUN_GX *) a_kun_gx)->devise2;
            this->a_kun_gx.eti_nve1 = ((A_KUN_GX *) a_kun_gx)->eti_nve1;
//            this->a_kun_gx.ean_nve1 = ((A_KUN_GX *) a_kun_gx)->ean_nve1;
            this->a_kun_gx.cab_nve1 = ((A_KUN_GX *) a_kun_gx)->cab_nve1;
            this->a_kun_gx.eti_nve2 = ((A_KUN_GX *) a_kun_gx)->eti_nve2;
//            this->a_kun_gx.ean_nve2 = ((A_KUN_GX *) a_kun_gx)->ean_nve2;
            this->a_kun_gx.cab_nve2 = ((A_KUN_GX *) a_kun_gx)->cab_nve2;
}

BOOL A_KUN_GX_CLASS::Copy ()
{
    HGLOBAL hglbCopy;
    if ( !::OpenClipboard(NULL) )
    {
      AfxMessageBox( _T("Cannot open the Clipboard" ));
      return FALSE;
    }

   // Remove the current Clipboard contents  
    if( !::EmptyClipboard() )
    {
      int err = GetLastError ();
      AfxMessageBox( _T("Cannot empty the Clipboard") );
      return FALSE;  
    }

	try
	{
        UINT CF_A_KUN_GX = RegisterClipboardFormat (_T("a_kun_gx")); 
	hglbCopy = GlobalAlloc(GMEM_MOVEABLE, sizeof (A_KUN_GX)); 
        char *p = (char *) GlobalLock(hglbCopy);
        memcpy (p, &a_kun_gx, sizeof (a_kun_gx));
        GlobalUnlock(hglbCopy); 
	HANDLE cData;
	cData = ::SetClipboardData( CF_A_KUN_GX, hglbCopy );  
        if (cData == NULL )  
        {
			 throw 1;
	}  
	}
	catch (...) {}
        CloseClipboard();
        return TRUE;
}


BOOL A_KUN_GX_CLASS::Paste ()
{
    if (!OpenClipboard(NULL) )
    {
      AfxMessageBox(_T("Cannot open the Clipboard" ));
      return FALSE;
    }

    HGLOBAL hglbCopy;
    UINT CF_A_KUN_GX;
    _TCHAR name [256];
    CF_A_KUN_GX = EnumClipboardFormats (0);
    int ret;
    while ((ret = GetClipboardFormatName (CF_A_KUN_GX, name, sizeof (name))) != 0)
    { 
         CString Name = name;
         if (Name == _T("a_kun_gx"))
         {
              break;
         }  
	 CF_A_KUN_GX = EnumClipboardFormats (CF_A_KUN_GX);
    }
    if (ret == 0)
    {
         CloseClipboard();
         return FALSE;
    }    

    try
    {	
  	     hglbCopy =  ::GetClipboardData(CF_A_KUN_GX);
	     if (hglbCopy == NULL)
	     {
		    throw 1;
	     }
 	     LPSTR p = (LPSTR) GlobalLock ((HGLOBAL) hglbCopy);
             memcpy ( &a_kun_gx, p, sizeof (a_kun_gx));
  	     GlobalUnlock ((HGLOBAL) hglbCopy);
    }
    catch (...) {};
    CloseClipboard();
    return TRUE; 
}

	

