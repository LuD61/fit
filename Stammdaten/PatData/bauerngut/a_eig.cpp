#include "stdafx.h"
#include "a_eig.h"

struct A_EIG a_eig, a_eig_null;

void A_EIG_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_eig.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_eig.a,SQLDOUBLE,0);
    sqlout ((long *) &a_eig.a_krz,SQLLONG,0);
    sqlout ((short *) &a_eig.anz_theke_eti,SQLSHORT,0);
    sqlout ((long *) &a_eig.bem_offs,SQLLONG,0);
    sqlout ((short *) &a_eig.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_eig.fil,SQLSHORT,0);
    sqlout ((double *) &a_eig.gew_bto,SQLDOUBLE,0);
    sqlout ((double *) &a_eig.inh,SQLDOUBLE,0);
    sqlout ((long *) &a_eig.mat,SQLLONG,0);
    sqlout ((short *) &a_eig.mdn,SQLSHORT,0);
    sqlout ((short *) &a_eig.me_einh_ek,SQLSHORT,0);
    sqlout ((TCHAR *) a_eig.mwst_ueb,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig.pr_ausz,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig.pr_man,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig.pr_ueb,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig.rez,SQLCHAR,9);
    sqlout ((short *) &a_eig.sg1,SQLSHORT,0);
    sqlout ((short *) &a_eig.sg2,SQLSHORT,0);
    sqlout ((double *) &a_eig.tara,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_eig.theke_eti,SQLCHAR,2);
    sqlout ((TCHAR *) a_eig.verk_art,SQLCHAR,2);
    sqlout ((DATE_STRUCT *) &a_eig.verk_beg,SQLDATE,0);
    sqlout ((double *) &a_eig.fuellgrad,SQLDOUBLE,0);
    sqlout ((short *) &a_eig.me_einh_abverk,SQLSHORT,0);
    sqlout ((double *) &a_eig.inh_abverk,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_eig.hnd_gew_abverk,SQLCHAR,2);
            cursor = sqlcursor (_T("select a_eig.a,  ")
_T("a_eig.a_krz,  a_eig.anz_theke_eti,  a_eig.bem_offs,  a_eig.delstatus,  ")
_T("a_eig.fil,  a_eig.gew_bto,  a_eig.inh,  a_eig.mat,  a_eig.mdn,  ")
_T("a_eig.me_einh_ek,  a_eig.mwst_ueb,  a_eig.pr_ausz,  a_eig.pr_man,  ")
_T("a_eig.pr_ueb,  a_eig.rez,  a_eig.sg1,  a_eig.sg2,  a_eig.tara,  ")
_T("a_eig.theke_eti,  a_eig.verk_art,  a_eig.verk_beg,  a_eig.fuellgrad,  ")
_T("a_eig.me_einh_abverk,  a_eig.inh_abverk,  a_eig.hnd_gew_abverk from a_eig ")

#line 12 "a_eig.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_eig.a,SQLDOUBLE,0);
    sqlin ((long *) &a_eig.a_krz,SQLLONG,0);
    sqlin ((short *) &a_eig.anz_theke_eti,SQLSHORT,0);
    sqlin ((long *) &a_eig.bem_offs,SQLLONG,0);
    sqlin ((short *) &a_eig.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_eig.fil,SQLSHORT,0);
    sqlin ((double *) &a_eig.gew_bto,SQLDOUBLE,0);
    sqlin ((double *) &a_eig.inh,SQLDOUBLE,0);
    sqlin ((long *) &a_eig.mat,SQLLONG,0);
    sqlin ((short *) &a_eig.mdn,SQLSHORT,0);
    sqlin ((short *) &a_eig.me_einh_ek,SQLSHORT,0);
    sqlin ((TCHAR *) a_eig.mwst_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig.pr_ausz,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig.pr_man,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig.pr_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig.rez,SQLCHAR,9);
    sqlin ((short *) &a_eig.sg1,SQLSHORT,0);
    sqlin ((short *) &a_eig.sg2,SQLSHORT,0);
    sqlin ((double *) &a_eig.tara,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_eig.theke_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig.verk_art,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_eig.verk_beg,SQLDATE,0);
    sqlin ((double *) &a_eig.fuellgrad,SQLDOUBLE,0);
    sqlin ((short *) &a_eig.me_einh_abverk,SQLSHORT,0);
    sqlin ((double *) &a_eig.inh_abverk,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_eig.hnd_gew_abverk,SQLCHAR,2);
            sqltext = _T("update a_eig set a_eig.a = ?,  ")
_T("a_eig.a_krz = ?,  a_eig.anz_theke_eti = ?,  a_eig.bem_offs = ?,  ")
_T("a_eig.delstatus = ?,  a_eig.fil = ?,  a_eig.gew_bto = ?,  ")
_T("a_eig.inh = ?,  a_eig.mat = ?,  a_eig.mdn = ?,  a_eig.me_einh_ek = ?,  ")
_T("a_eig.mwst_ueb = ?,  a_eig.pr_ausz = ?,  a_eig.pr_man = ?,  ")
_T("a_eig.pr_ueb = ?,  a_eig.rez = ?,  a_eig.sg1 = ?,  a_eig.sg2 = ?,  ")
_T("a_eig.tara = ?,  a_eig.theke_eti = ?,  a_eig.verk_art = ?,  ")
_T("a_eig.verk_beg = ?,  a_eig.fuellgrad = ?,  ")
_T("a_eig.me_einh_abverk = ?,  a_eig.inh_abverk = ?,  ")
_T("a_eig.hnd_gew_abverk = ? ")

#line 14 "a_eig.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_eig.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_eig.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_eig ")
                                  _T("where a = ?"));
            sqlin ((double *)   &a_eig.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_eig ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_eig.a,SQLDOUBLE,0);
    sqlin ((long *) &a_eig.a_krz,SQLLONG,0);
    sqlin ((short *) &a_eig.anz_theke_eti,SQLSHORT,0);
    sqlin ((long *) &a_eig.bem_offs,SQLLONG,0);
    sqlin ((short *) &a_eig.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_eig.fil,SQLSHORT,0);
    sqlin ((double *) &a_eig.gew_bto,SQLDOUBLE,0);
    sqlin ((double *) &a_eig.inh,SQLDOUBLE,0);
    sqlin ((long *) &a_eig.mat,SQLLONG,0);
    sqlin ((short *) &a_eig.mdn,SQLSHORT,0);
    sqlin ((short *) &a_eig.me_einh_ek,SQLSHORT,0);
    sqlin ((TCHAR *) a_eig.mwst_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig.pr_ausz,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig.pr_man,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig.pr_ueb,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig.rez,SQLCHAR,9);
    sqlin ((short *) &a_eig.sg1,SQLSHORT,0);
    sqlin ((short *) &a_eig.sg2,SQLSHORT,0);
    sqlin ((double *) &a_eig.tara,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_eig.theke_eti,SQLCHAR,2);
    sqlin ((TCHAR *) a_eig.verk_art,SQLCHAR,2);
    sqlin ((DATE_STRUCT *) &a_eig.verk_beg,SQLDATE,0);
    sqlin ((double *) &a_eig.fuellgrad,SQLDOUBLE,0);
    sqlin ((short *) &a_eig.me_einh_abverk,SQLSHORT,0);
    sqlin ((double *) &a_eig.inh_abverk,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_eig.hnd_gew_abverk,SQLCHAR,2);
            ins_cursor = sqlcursor (_T("insert into a_eig (a,  ")
_T("a_krz,  anz_theke_eti,  bem_offs,  delstatus,  fil,  gew_bto,  inh,  mat,  mdn,  ")
_T("me_einh_ek,  mwst_ueb,  pr_ausz,  pr_man,  pr_ueb,  rez,  sg1,  sg2,  tara,  theke_eti,  ")
_T("verk_art,  verk_beg,  fuellgrad,  me_einh_abverk,  inh_abverk,  ")
_T("hnd_gew_abverk) ")

#line 25 "a_eig.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 27 "a_eig.rpp"
}
