#include "stdafx.h"
#include "mdn.h"

struct MDN mdn, mdn_null;

void MDN_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((short *)   &mdn.mdn,  SQLSHORT, 0);
    sqlout ((TCHAR *) mdn.abr_period,SQLCHAR,2);
    sqlout ((long *) &mdn.adr,SQLLONG,0);
    sqlout ((long *) &mdn.adr_lief,SQLLONG,0);
    sqlout ((DATE_STRUCT *) &mdn.dat_ero,SQLDATE,0);
    sqlout ((short *) &mdn.daten_mnp,SQLSHORT,0);
    sqlout ((short *) &mdn.delstatus,SQLSHORT,0);
    sqlout ((TCHAR *) mdn.fil_bel_o_sa,SQLCHAR,2);
    sqlout ((double *) &mdn.fl_lad,SQLDOUBLE,0);
    sqlout ((double *) &mdn.fl_nto,SQLDOUBLE,0);
    sqlout ((double *) &mdn.fl_vk_ges,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &mdn.iakv,SQLDATE,0);
    sqlout ((TCHAR *) mdn.inv_rht,SQLCHAR,2);
    sqlout ((long *) &mdn.kun,SQLLONG,0);
    sqlout ((TCHAR *) mdn.lief,SQLCHAR,17);
    sqlout ((TCHAR *) mdn.lief_rht,SQLCHAR,2);
    sqlout ((long *) &mdn.lief_s,SQLLONG,0);
    sqlout ((short *) &mdn.mdn,SQLSHORT,0);
    sqlout ((TCHAR *) mdn.mdn_kla,SQLCHAR,2);
    sqlout ((short *) &mdn.mdn_gr,SQLSHORT,0);
    sqlout ((TCHAR *) mdn.pers,SQLCHAR,13);
    sqlout ((short *) &mdn.pers_anz,SQLSHORT,0);
    sqlout ((TCHAR *) mdn.pr_bel_entl,SQLCHAR,2);
    sqlout ((short *) &mdn.pr_lst,SQLSHORT,0);
    sqlout ((double *) &mdn.reg_bed_theke_lng,SQLDOUBLE,0);
    sqlout ((double *) &mdn.reg_kt_lng,SQLDOUBLE,0);
    sqlout ((double *) &mdn.reg_kue_lng,SQLDOUBLE,0);
    sqlout ((double *) &mdn.reg_lng,SQLDOUBLE,0);
    sqlout ((double *) &mdn.reg_tks_lng,SQLDOUBLE,0);
    sqlout ((double *) &mdn.reg_tkt_lng,SQLDOUBLE,0);
    sqlout ((TCHAR *) mdn.smt_kz,SQLCHAR,2);
    sqlout ((short *) &mdn.sonst_einh,SQLSHORT,0);
    sqlout ((TCHAR *) mdn.sp_kz,SQLCHAR,2);
    sqlout ((short *) &mdn.sprache,SQLSHORT,0);
    sqlout ((TCHAR *) mdn.sw_kz,SQLCHAR,2);
    sqlout ((TCHAR *) mdn.tou,SQLCHAR,9);
    sqlout ((TCHAR *) mdn.umlgr,SQLCHAR,2);
    sqlout ((TCHAR *) mdn.verk_st_kz,SQLCHAR,2);
    sqlout ((short *) &mdn.vrs_typ,SQLSHORT,0);
    sqlout ((TCHAR *) mdn.inv_akv,SQLCHAR,2);
    sqlout ((double *) &mdn.gbr,SQLDOUBLE,0);
    sqlout ((TCHAR *) mdn.waehr_prim,SQLCHAR,2);
    sqlout ((TCHAR *) mdn.waehr_sek,SQLCHAR,2);
    sqlout ((double *) &mdn.konversion,SQLDOUBLE,0);
    sqlout ((TCHAR *) mdn.iln,SQLCHAR,17);
    sqlout ((TCHAR *) mdn.ust_id,SQLCHAR,12);
            cursor = sqlcursor (_T("select mdn.abr_period,  ")
_T("mdn.adr,  mdn.adr_lief,  mdn.dat_ero,  mdn.daten_mnp,  mdn.delstatus,  ")
_T("mdn.fil_bel_o_sa,  mdn.fl_lad,  mdn.fl_nto,  mdn.fl_vk_ges,  mdn.iakv,  ")
_T("mdn.inv_rht,  mdn.kun,  mdn.lief,  mdn.lief_rht,  mdn.lief_s,  mdn.mdn,  ")
_T("mdn.mdn_kla,  mdn.mdn_gr,  mdn.pers,  mdn.pers_anz,  mdn.pr_bel_entl,  ")
_T("mdn.pr_lst,  mdn.reg_bed_theke_lng,  mdn.reg_kt_lng,  mdn.reg_kue_lng,  ")
_T("mdn.reg_lng,  mdn.reg_tks_lng,  mdn.reg_tkt_lng,  mdn.smt_kz,  ")
_T("mdn.sonst_einh,  mdn.sp_kz,  mdn.sprache,  mdn.sw_kz,  mdn.tou,  mdn.umlgr,  ")
_T("mdn.verk_st_kz,  mdn.vrs_typ,  mdn.inv_akv,  mdn.gbr,  mdn.waehr_prim,  ")
_T("mdn.waehr_sek,  mdn.konversion,  mdn.iln,  mdn.ust_id from mdn ")

#line 12 "mdn.rpp"
                                  _T("where mdn = ?"));
    sqlin ((TCHAR *) mdn.abr_period,SQLCHAR,2);
    sqlin ((long *) &mdn.adr,SQLLONG,0);
    sqlin ((long *) &mdn.adr_lief,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &mdn.dat_ero,SQLDATE,0);
    sqlin ((short *) &mdn.daten_mnp,SQLSHORT,0);
    sqlin ((short *) &mdn.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.fil_bel_o_sa,SQLCHAR,2);
    sqlin ((double *) &mdn.fl_lad,SQLDOUBLE,0);
    sqlin ((double *) &mdn.fl_nto,SQLDOUBLE,0);
    sqlin ((double *) &mdn.fl_vk_ges,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &mdn.iakv,SQLDATE,0);
    sqlin ((TCHAR *) mdn.inv_rht,SQLCHAR,2);
    sqlin ((long *) &mdn.kun,SQLLONG,0);
    sqlin ((TCHAR *) mdn.lief,SQLCHAR,17);
    sqlin ((TCHAR *) mdn.lief_rht,SQLCHAR,2);
    sqlin ((long *) &mdn.lief_s,SQLLONG,0);
    sqlin ((short *) &mdn.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.mdn_kla,SQLCHAR,2);
    sqlin ((short *) &mdn.mdn_gr,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.pers,SQLCHAR,13);
    sqlin ((short *) &mdn.pers_anz,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.pr_bel_entl,SQLCHAR,2);
    sqlin ((short *) &mdn.pr_lst,SQLSHORT,0);
    sqlin ((double *) &mdn.reg_bed_theke_lng,SQLDOUBLE,0);
    sqlin ((double *) &mdn.reg_kt_lng,SQLDOUBLE,0);
    sqlin ((double *) &mdn.reg_kue_lng,SQLDOUBLE,0);
    sqlin ((double *) &mdn.reg_lng,SQLDOUBLE,0);
    sqlin ((double *) &mdn.reg_tks_lng,SQLDOUBLE,0);
    sqlin ((double *) &mdn.reg_tkt_lng,SQLDOUBLE,0);
    sqlin ((TCHAR *) mdn.smt_kz,SQLCHAR,2);
    sqlin ((short *) &mdn.sonst_einh,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.sp_kz,SQLCHAR,2);
    sqlin ((short *) &mdn.sprache,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.sw_kz,SQLCHAR,2);
    sqlin ((TCHAR *) mdn.tou,SQLCHAR,9);
    sqlin ((TCHAR *) mdn.umlgr,SQLCHAR,2);
    sqlin ((TCHAR *) mdn.verk_st_kz,SQLCHAR,2);
    sqlin ((short *) &mdn.vrs_typ,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.inv_akv,SQLCHAR,2);
    sqlin ((double *) &mdn.gbr,SQLDOUBLE,0);
    sqlin ((TCHAR *) mdn.waehr_prim,SQLCHAR,2);
    sqlin ((TCHAR *) mdn.waehr_sek,SQLCHAR,2);
    sqlin ((double *) &mdn.konversion,SQLDOUBLE,0);
    sqlin ((TCHAR *) mdn.iln,SQLCHAR,17);
    sqlin ((TCHAR *) mdn.ust_id,SQLCHAR,12);
            sqltext = _T("update mdn set mdn.abr_period = ?,  ")
_T("mdn.adr = ?,  mdn.adr_lief = ?,  mdn.dat_ero = ?,  mdn.daten_mnp = ?,  ")
_T("mdn.delstatus = ?,  mdn.fil_bel_o_sa = ?,  mdn.fl_lad = ?,  ")
_T("mdn.fl_nto = ?,  mdn.fl_vk_ges = ?,  mdn.iakv = ?,  mdn.inv_rht = ?,  ")
_T("mdn.kun = ?,  mdn.lief = ?,  mdn.lief_rht = ?,  mdn.lief_s = ?,  ")
_T("mdn.mdn = ?,  mdn.mdn_kla = ?,  mdn.mdn_gr = ?,  mdn.pers = ?,  ")
_T("mdn.pers_anz = ?,  mdn.pr_bel_entl = ?,  mdn.pr_lst = ?,  ")
_T("mdn.reg_bed_theke_lng = ?,  mdn.reg_kt_lng = ?,  ")
_T("mdn.reg_kue_lng = ?,  mdn.reg_lng = ?,  mdn.reg_tks_lng = ?,  ")
_T("mdn.reg_tkt_lng = ?,  mdn.smt_kz = ?,  mdn.sonst_einh = ?,  ")
_T("mdn.sp_kz = ?,  mdn.sprache = ?,  mdn.sw_kz = ?,  mdn.tou = ?,  ")
_T("mdn.umlgr = ?,  mdn.verk_st_kz = ?,  mdn.vrs_typ = ?,  ")
_T("mdn.inv_akv = ?,  mdn.gbr = ?,  mdn.waehr_prim = ?,  ")
_T("mdn.waehr_sek = ?,  mdn.konversion = ?,  mdn.iln = ?,  mdn.ust_id = ? ")

#line 14 "mdn.rpp"
                                  _T("where mdn = ?");
            sqlin ((short *)   &mdn.mdn,  SQLSHORT, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((short *)   &mdn.mdn,  SQLSHORT, 0);
            test_upd_cursor = sqlcursor (_T("select mdn from mdn ")
                                  _T("where mdn = ?"));
            sqlin ((short *)   &mdn.mdn,  SQLSHORT, 0);
            del_cursor = sqlcursor (_T("delete from mdn ")
                                  _T("where mdn = ?"));
    sqlin ((TCHAR *) mdn.abr_period,SQLCHAR,2);
    sqlin ((long *) &mdn.adr,SQLLONG,0);
    sqlin ((long *) &mdn.adr_lief,SQLLONG,0);
    sqlin ((DATE_STRUCT *) &mdn.dat_ero,SQLDATE,0);
    sqlin ((short *) &mdn.daten_mnp,SQLSHORT,0);
    sqlin ((short *) &mdn.delstatus,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.fil_bel_o_sa,SQLCHAR,2);
    sqlin ((double *) &mdn.fl_lad,SQLDOUBLE,0);
    sqlin ((double *) &mdn.fl_nto,SQLDOUBLE,0);
    sqlin ((double *) &mdn.fl_vk_ges,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &mdn.iakv,SQLDATE,0);
    sqlin ((TCHAR *) mdn.inv_rht,SQLCHAR,2);
    sqlin ((long *) &mdn.kun,SQLLONG,0);
    sqlin ((TCHAR *) mdn.lief,SQLCHAR,17);
    sqlin ((TCHAR *) mdn.lief_rht,SQLCHAR,2);
    sqlin ((long *) &mdn.lief_s,SQLLONG,0);
    sqlin ((short *) &mdn.mdn,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.mdn_kla,SQLCHAR,2);
    sqlin ((short *) &mdn.mdn_gr,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.pers,SQLCHAR,13);
    sqlin ((short *) &mdn.pers_anz,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.pr_bel_entl,SQLCHAR,2);
    sqlin ((short *) &mdn.pr_lst,SQLSHORT,0);
    sqlin ((double *) &mdn.reg_bed_theke_lng,SQLDOUBLE,0);
    sqlin ((double *) &mdn.reg_kt_lng,SQLDOUBLE,0);
    sqlin ((double *) &mdn.reg_kue_lng,SQLDOUBLE,0);
    sqlin ((double *) &mdn.reg_lng,SQLDOUBLE,0);
    sqlin ((double *) &mdn.reg_tks_lng,SQLDOUBLE,0);
    sqlin ((double *) &mdn.reg_tkt_lng,SQLDOUBLE,0);
    sqlin ((TCHAR *) mdn.smt_kz,SQLCHAR,2);
    sqlin ((short *) &mdn.sonst_einh,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.sp_kz,SQLCHAR,2);
    sqlin ((short *) &mdn.sprache,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.sw_kz,SQLCHAR,2);
    sqlin ((TCHAR *) mdn.tou,SQLCHAR,9);
    sqlin ((TCHAR *) mdn.umlgr,SQLCHAR,2);
    sqlin ((TCHAR *) mdn.verk_st_kz,SQLCHAR,2);
    sqlin ((short *) &mdn.vrs_typ,SQLSHORT,0);
    sqlin ((TCHAR *) mdn.inv_akv,SQLCHAR,2);
    sqlin ((double *) &mdn.gbr,SQLDOUBLE,0);
    sqlin ((TCHAR *) mdn.waehr_prim,SQLCHAR,2);
    sqlin ((TCHAR *) mdn.waehr_sek,SQLCHAR,2);
    sqlin ((double *) &mdn.konversion,SQLDOUBLE,0);
    sqlin ((TCHAR *) mdn.iln,SQLCHAR,17);
    sqlin ((TCHAR *) mdn.ust_id,SQLCHAR,12);
            ins_cursor = sqlcursor (_T("insert into mdn (")
_T("abr_period,  adr,  adr_lief,  dat_ero,  daten_mnp,  delstatus,  fil_bel_o_sa,  ")
_T("fl_lad,  fl_nto,  fl_vk_ges,  iakv,  inv_rht,  kun,  lief,  lief_rht,  lief_s,  mdn,  ")
_T("mdn_kla,  mdn_gr,  pers,  pers_anz,  pr_bel_entl,  pr_lst,  reg_bed_theke_lng,  ")
_T("reg_kt_lng,  reg_kue_lng,  reg_lng,  reg_tks_lng,  reg_tkt_lng,  smt_kz,  ")
_T("sonst_einh,  sp_kz,  sprache,  sw_kz,  tou,  umlgr,  verk_st_kz,  vrs_typ,  inv_akv,  ")
_T("gbr,  waehr_prim,  waehr_sek,  konversion,  iln,  ust_id) ")

#line 25 "mdn.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?)")); 

#line 27 "mdn.rpp"
}
