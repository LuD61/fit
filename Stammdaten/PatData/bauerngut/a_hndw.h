#ifndef _A_HNDW_DEF
#define _A_HNDW_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_HNDW {
   double         a;
   TCHAR          a_grnd[2];
   long           a_krz;
   double         a_leer;
   double         a_pfa;
   TCHAR          a_pfa_kz[2];
   short          anz_reg_eti;
   short          anz_theke_eti;
   short          anz_waren_eti;
   short          delstatus;
   short          fil;
   double         gew_bto;
   short          herk_land;
   TCHAR          hdkl[3];
   double         inh;
   TCHAR          mwst_ueb[2];
   short          me_einh_kun;
   short          mdn;
   TCHAR          pr_ausz[2];
   TCHAR          pr_man[2];
   TCHAR          pr_ueb[2];
   TCHAR          reg_eti[2];
   short          sg1;
   short          sg2;
   TCHAR          smt[2];
   double         tara;
   TCHAR          theke_eti[2];
   DATE_STRUCT    verk_beg;
   TCHAR          verk_art[2];
   TCHAR          vk_typ[2];
   TCHAR          vpk[2];
   TCHAR          waren_eti[2];
   double         fuellgrad;
   short          me_einh_abverk;
   double         inh_abverk;
   TCHAR          hnd_gew_abverk[2];
};
extern struct A_HNDW a_hndw, a_hndw_null;

#line 8 "a_hndw.rh"

class A_HNDW_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_HNDW a_hndw;  
               A_HNDW_CLASS () : DB_CLASS ()
               {
               }
};
#endif
