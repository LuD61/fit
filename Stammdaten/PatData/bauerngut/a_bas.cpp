#include "stdafx.h"
#include "a_bas.h"

struct A_BAS a_bas, a_bas_null;

void A_BAS_CLASS::prepare (void)
{
            TCHAR *sqltext;

            sqlin ((double *)   &a_bas.a,  SQLDOUBLE, 0);
    sqlout ((double *) &a_bas.a,SQLDOUBLE,0);
    sqlout ((DATE_STRUCT *) &a_bas.akv,SQLDATE,0);
    sqlout ((DATE_STRUCT *) &a_bas.bearb,SQLDATE,0);
    sqlout ((short *) &a_bas.mdn,SQLSHORT,0);
    sqlout ((short *) &a_bas.fil,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.a_bz1,SQLCHAR,25);
    sqlout ((TCHAR *) a_bas.a_bz2,SQLCHAR,25);
    sqlout ((double *) &a_bas.a_gew,SQLDOUBLE,0);
    sqlout ((short *) &a_bas.a_typ,SQLSHORT,0);
    sqlout ((short *) &a_bas.a_typ2,SQLSHORT,0);
    sqlout ((short *) &a_bas.abt,SQLSHORT,0);
    sqlout ((short *) &a_bas.ag,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.best_auto,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas.bsd_kz,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas.cp_aufschl,SQLCHAR,2);
    sqlout ((short *) &a_bas.delstatus,SQLSHORT,0);
    sqlout ((short *) &a_bas.dr_folge,SQLSHORT,0);
    sqlout ((long *) &a_bas.erl_kto,SQLLONG,0);
    sqlout ((double *) &a_bas.gn_pkt_gbr,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas.hbk_kz,SQLCHAR,2);
    sqlout ((short *) &a_bas.hbk_ztr,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.hnd_gew,SQLCHAR,2);
    sqlout ((short *) &a_bas.hwg,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.kost_kz,SQLCHAR,3);
    sqlout ((short *) &a_bas.me_einh,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.modif,SQLCHAR,2);
    sqlout ((short *) &a_bas.mwst,SQLSHORT,0);
    sqlout ((short *) &a_bas.plak_div,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.pers_nam,SQLCHAR,9);
    sqlout ((TCHAR *) a_bas.pers_rab_kz,SQLCHAR,2);
    sqlout ((double *) &a_bas.prod_zeit,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas.stk_lst_kz,SQLCHAR,2);
    sqlout ((double *) &a_bas.sw,SQLDOUBLE,0);
    sqlout ((short *) &a_bas.teil_smt,SQLSHORT,0);
    sqlout ((long *) &a_bas.we_kto,SQLLONG,0);
    sqlout ((short *) &a_bas.wg,SQLSHORT,0);
    sqlout ((short *) &a_bas.zu_stoff,SQLSHORT,0);
    sqlout ((long *) &a_bas.kost_st,SQLLONG,0);
    sqlout ((short *) &a_bas.hbk_ztr_wa,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.norm_ex,SQLCHAR,2);
    sqlout ((double *) &a_bas.dm_abschl,SQLDOUBLE,0);
    sqlout ((double *) &a_bas.proz_abschl,SQLDOUBLE,0);
    sqlout ((double *) &a_bas.a_tara,SQLDOUBLE,0);
    sqlout ((double *) &a_bas.a_gew_brutto,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas.bz_kz,SQLCHAR,2);
    sqlout ((double *) &a_bas.ehg_a,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas.a_gew_kz,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas.prod_kz,SQLCHAR,3);
    sqlout ((TCHAR *) a_bas.a_bz3,SQLCHAR,41);
    sqlout ((double *) &a_bas.abg_eh,SQLDOUBLE,0);
    sqlout ((double *) &a_bas.plan_me,SQLDOUBLE,0);
    sqlout ((double *) &a_bas.kosten1,SQLDOUBLE,0);
    sqlout ((double *) &a_bas.kosten2,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas.kz1,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas.kz2,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas.pr_kz,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas.pr_kz1,SQLCHAR,2);
    sqlout ((TCHAR *) a_bas.pr_kz2,SQLCHAR,3);
    sqlout ((short *) &a_bas.ka_lage,SQLSHORT,0);
    sqlout ((short *) &a_bas.la_pal,SQLSHORT,0);
    sqlout ((short *) &a_bas.up_ka,SQLSHORT,0);
    sqlout ((long *) &a_bas.inh_ek,SQLLONG,0);
    sqlout ((double *) &a_bas.plan_me_koe,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas.order_bz,SQLCHAR,25);
    sqlout ((TCHAR *) a_bas.sort_grp,SQLCHAR,11);
    sqlout ((short *) &a_bas.dr_folge_cc,SQLSHORT,0);
    sqlout ((double *) &a_bas.a_ehg,SQLDOUBLE,0);
    sqlout ((TCHAR *) a_bas.sw_pr_kz,SQLCHAR,2);
    sqlout ((long *) &a_bas.kost_tr,SQLLONG,0);
    sqlout ((double *) &a_bas.a_grund,SQLDOUBLE,0);
    sqlout ((long *) &a_bas.kost_st2,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto2,SQLLONG,0);
    sqlout ((long *) &a_bas.charg_hand,SQLLONG,0);
    sqlout ((long *) &a_bas.intra_stat,SQLLONG,0);
    sqlout ((TCHAR *) a_bas.qual_kng,SQLCHAR,5);
    sqlout ((short *) &a_bas.lief_einh,SQLSHORT,0);
    sqlout ((double *) &a_bas.inh_lief,SQLDOUBLE,0);
    sqlout ((long *) &a_bas.erl_kto_1,SQLLONG,0);
    sqlout ((long *) &a_bas.erl_kto_2,SQLLONG,0);
    sqlout ((long *) &a_bas.erl_kto_3,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto_1,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto_2,SQLLONG,0);
    sqlout ((long *) &a_bas.we_kto_3,SQLLONG,0);
    sqlout ((short *) &a_bas.drk_anz,SQLSHORT,0);
    sqlout ((TCHAR *) a_bas.pnw_lkng,SQLCHAR,13);
    sqlout ((TCHAR *) a_bas.betr_kz,SQLCHAR,6);
            cursor = sqlcursor (_T("select a_bas.a,  a_bas.akv,  ")
_T("a_bas.bearb,  a_bas.mdn,  a_bas.fil,  a_bas.a_bz1,  a_bas.a_bz2,  ")
_T("a_bas.a_gew,  a_bas.a_typ,  a_bas.a_typ2,  a_bas.abt,  a_bas.ag,  ")
_T("a_bas.best_auto,  a_bas.bsd_kz,  a_bas.cp_aufschl,  a_bas.delstatus,  ")
_T("a_bas.dr_folge,  a_bas.erl_kto,  a_bas.gn_pkt_gbr,  a_bas.hbk_kz,  ")
_T("a_bas.hbk_ztr,  a_bas.hnd_gew,  a_bas.hwg,  a_bas.kost_kz,  ")
_T("a_bas.me_einh,  a_bas.modif,  a_bas.mwst,  a_bas.plak_div,  ")
_T("a_bas.pers_nam,  a_bas.pers_rab_kz,  a_bas.prod_zeit,  ")
_T("a_bas.stk_lst_kz,  a_bas.sw,  a_bas.teil_smt,  a_bas.we_kto,  a_bas.wg,  ")
_T("a_bas.zu_stoff,  a_bas.kost_st,  a_bas.hbk_ztr_wa,  a_bas.norm_ex,  ")
_T("a_bas.dm_abschl,  a_bas.proz_abschl,  a_bas.a_tara,  ")
_T("a_bas.a_gew_brutto,  a_bas.bz_kz,  a_bas.ehg_a,  a_bas.a_gew_kz,  ")
_T("a_bas.prod_kz,  a_bas.a_bz3,  a_bas.abg_eh,  a_bas.plan_me,  ")
_T("a_bas.kosten1,  a_bas.kosten2,  a_bas.kz1,  a_bas.kz2,  a_bas.pr_kz,  ")
_T("a_bas.pr_kz1,  a_bas.pr_kz2,  a_bas.ka_lage,  a_bas.la_pal,  a_bas.up_ka,  ")
_T("a_bas.inh_ek,  a_bas.plan_me_koe,  a_bas.order_bz,  a_bas.sort_grp,  ")
_T("a_bas.dr_folge_cc,  a_bas.a_ehg,  a_bas.sw_pr_kz,  a_bas.kost_tr,  ")
_T("a_bas.a_grund,  a_bas.kost_st2,  a_bas.we_kto2,  a_bas.charg_hand,  ")
_T("a_bas.intra_stat,  a_bas.qual_kng,  a_bas.lief_einh,  a_bas.inh_lief,  ")
_T("a_bas.erl_kto_1,  a_bas.erl_kto_2,  a_bas.erl_kto_3,  a_bas.we_kto_1,  ")
_T("a_bas.we_kto_2,  a_bas.we_kto_3,  a_bas.drk_anz,  a_bas.pnw_lkng,  ")
_T("a_bas.betr_kz from a_bas ")

#line 12 "a_bas.rpp"
                                  _T("where a = ?"));
    sqlin ((double *) &a_bas.a,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_bas.akv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &a_bas.bearb,SQLDATE,0);
    sqlin ((short *) &a_bas.mdn,SQLSHORT,0);
    sqlin ((short *) &a_bas.fil,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.a_bz1,SQLCHAR,25);
    sqlin ((TCHAR *) a_bas.a_bz2,SQLCHAR,25);
    sqlin ((double *) &a_bas.a_gew,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.a_typ,SQLSHORT,0);
    sqlin ((short *) &a_bas.a_typ2,SQLSHORT,0);
    sqlin ((short *) &a_bas.abt,SQLSHORT,0);
    sqlin ((short *) &a_bas.ag,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.best_auto,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.bsd_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.cp_aufschl,SQLCHAR,2);
    sqlin ((short *) &a_bas.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_bas.dr_folge,SQLSHORT,0);
    sqlin ((long *) &a_bas.erl_kto,SQLLONG,0);
    sqlin ((double *) &a_bas.gn_pkt_gbr,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.hbk_kz,SQLCHAR,2);
    sqlin ((short *) &a_bas.hbk_ztr,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.hnd_gew,SQLCHAR,2);
    sqlin ((short *) &a_bas.hwg,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.kost_kz,SQLCHAR,3);
    sqlin ((short *) &a_bas.me_einh,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.modif,SQLCHAR,2);
    sqlin ((short *) &a_bas.mwst,SQLSHORT,0);
    sqlin ((short *) &a_bas.plak_div,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.pers_nam,SQLCHAR,9);
    sqlin ((TCHAR *) a_bas.pers_rab_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas.prod_zeit,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.stk_lst_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas.sw,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.teil_smt,SQLSHORT,0);
    sqlin ((long *) &a_bas.we_kto,SQLLONG,0);
    sqlin ((short *) &a_bas.wg,SQLSHORT,0);
    sqlin ((short *) &a_bas.zu_stoff,SQLSHORT,0);
    sqlin ((long *) &a_bas.kost_st,SQLLONG,0);
    sqlin ((short *) &a_bas.hbk_ztr_wa,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.norm_ex,SQLCHAR,2);
    sqlin ((double *) &a_bas.dm_abschl,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.proz_abschl,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.a_tara,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.a_gew_brutto,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.bz_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas.ehg_a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.a_gew_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.prod_kz,SQLCHAR,3);
    sqlin ((TCHAR *) a_bas.a_bz3,SQLCHAR,41);
    sqlin ((double *) &a_bas.abg_eh,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.plan_me,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.kosten1,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.kosten2,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.kz1,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.kz2,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.pr_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.pr_kz1,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.pr_kz2,SQLCHAR,3);
    sqlin ((short *) &a_bas.ka_lage,SQLSHORT,0);
    sqlin ((short *) &a_bas.la_pal,SQLSHORT,0);
    sqlin ((short *) &a_bas.up_ka,SQLSHORT,0);
    sqlin ((long *) &a_bas.inh_ek,SQLLONG,0);
    sqlin ((double *) &a_bas.plan_me_koe,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.order_bz,SQLCHAR,25);
    sqlin ((TCHAR *) a_bas.sort_grp,SQLCHAR,11);
    sqlin ((short *) &a_bas.dr_folge_cc,SQLSHORT,0);
    sqlin ((double *) &a_bas.a_ehg,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.sw_pr_kz,SQLCHAR,2);
    sqlin ((long *) &a_bas.kost_tr,SQLLONG,0);
    sqlin ((double *) &a_bas.a_grund,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.kost_st2,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto2,SQLLONG,0);
    sqlin ((long *) &a_bas.charg_hand,SQLLONG,0);
    sqlin ((long *) &a_bas.intra_stat,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.qual_kng,SQLCHAR,5);
    sqlin ((short *) &a_bas.lief_einh,SQLSHORT,0);
    sqlin ((double *) &a_bas.inh_lief,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.erl_kto_1,SQLLONG,0);
    sqlin ((long *) &a_bas.erl_kto_2,SQLLONG,0);
    sqlin ((long *) &a_bas.erl_kto_3,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_1,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_2,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_3,SQLLONG,0);
    sqlin ((short *) &a_bas.drk_anz,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.pnw_lkng,SQLCHAR,13);
    sqlin ((TCHAR *) a_bas.betr_kz,SQLCHAR,6);
            sqltext = _T("update a_bas set a_bas.a = ?,  ")
_T("a_bas.akv = ?,  a_bas.bearb = ?,  a_bas.mdn = ?,  a_bas.fil = ?,  ")
_T("a_bas.a_bz1 = ?,  a_bas.a_bz2 = ?,  a_bas.a_gew = ?,  a_bas.a_typ = ?,  ")
_T("a_bas.a_typ2 = ?,  a_bas.abt = ?,  a_bas.ag = ?,  a_bas.best_auto = ?,  ")
_T("a_bas.bsd_kz = ?,  a_bas.cp_aufschl = ?,  a_bas.delstatus = ?,  ")
_T("a_bas.dr_folge = ?,  a_bas.erl_kto = ?,  a_bas.gn_pkt_gbr = ?,  ")
_T("a_bas.hbk_kz = ?,  a_bas.hbk_ztr = ?,  a_bas.hnd_gew = ?,  ")
_T("a_bas.hwg = ?,  a_bas.kost_kz = ?,  a_bas.me_einh = ?,  ")
_T("a_bas.modif = ?,  a_bas.mwst = ?,  a_bas.plak_div = ?,  ")
_T("a_bas.pers_nam = ?,  a_bas.pers_rab_kz = ?,  a_bas.prod_zeit = ?,  ")
_T("a_bas.stk_lst_kz = ?,  a_bas.sw = ?,  a_bas.teil_smt = ?,  ")
_T("a_bas.we_kto = ?,  a_bas.wg = ?,  a_bas.zu_stoff = ?,  ")
_T("a_bas.kost_st = ?,  a_bas.hbk_ztr_wa = ?,  a_bas.norm_ex = ?,  ")
_T("a_bas.dm_abschl = ?,  a_bas.proz_abschl = ?,  a_bas.a_tara = ?,  ")
_T("a_bas.a_gew_brutto = ?,  a_bas.bz_kz = ?,  a_bas.ehg_a = ?,  ")
_T("a_bas.a_gew_kz = ?,  a_bas.prod_kz = ?,  a_bas.a_bz3 = ?,  ")
_T("a_bas.abg_eh = ?,  a_bas.plan_me = ?,  a_bas.kosten1 = ?,  ")
_T("a_bas.kosten2 = ?,  a_bas.kz1 = ?,  a_bas.kz2 = ?,  a_bas.pr_kz = ?,  ")
_T("a_bas.pr_kz1 = ?,  a_bas.pr_kz2 = ?,  a_bas.ka_lage = ?,  ")
_T("a_bas.la_pal = ?,  a_bas.up_ka = ?,  a_bas.inh_ek = ?,  ")
_T("a_bas.plan_me_koe = ?,  a_bas.order_bz = ?,  a_bas.sort_grp = ?,  ")
_T("a_bas.dr_folge_cc = ?,  a_bas.a_ehg = ?,  a_bas.sw_pr_kz = ?,  ")
_T("a_bas.kost_tr = ?,  a_bas.a_grund = ?,  a_bas.kost_st2 = ?,  ")
_T("a_bas.we_kto2 = ?,  a_bas.charg_hand = ?,  a_bas.intra_stat = ?,  ")
_T("a_bas.qual_kng = ?,  a_bas.lief_einh = ?,  a_bas.inh_lief = ?,  ")
_T("a_bas.erl_kto_1 = ?,  a_bas.erl_kto_2 = ?,  a_bas.erl_kto_3 = ?,  ")
_T("a_bas.we_kto_1 = ?,  a_bas.we_kto_2 = ?,  a_bas.we_kto_3 = ?,  ")
_T("a_bas.drk_anz = ?,  a_bas.pnw_lkng = ?,  a_bas.betr_kz = ? ")

#line 14 "a_bas.rpp"
                                  _T("where a = ?");
            sqlin ((double *)   &a_bas.a,  SQLDOUBLE, 0);
            upd_cursor = sqlcursor (sqltext);

            sqlin ((double *)   &a_bas.a,  SQLDOUBLE, 0);
            test_upd_cursor = sqlcursor (_T("select a from a_bas ")
                                  _T("where a = ?"));
            sqlin ((double *)   &a_bas.a,  SQLDOUBLE, 0);
            del_cursor = sqlcursor (_T("delete from a_bas ")
                                  _T("where a = ?"));
    sqlin ((double *) &a_bas.a,SQLDOUBLE,0);
    sqlin ((DATE_STRUCT *) &a_bas.akv,SQLDATE,0);
    sqlin ((DATE_STRUCT *) &a_bas.bearb,SQLDATE,0);
    sqlin ((short *) &a_bas.mdn,SQLSHORT,0);
    sqlin ((short *) &a_bas.fil,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.a_bz1,SQLCHAR,25);
    sqlin ((TCHAR *) a_bas.a_bz2,SQLCHAR,25);
    sqlin ((double *) &a_bas.a_gew,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.a_typ,SQLSHORT,0);
    sqlin ((short *) &a_bas.a_typ2,SQLSHORT,0);
    sqlin ((short *) &a_bas.abt,SQLSHORT,0);
    sqlin ((short *) &a_bas.ag,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.best_auto,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.bsd_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.cp_aufschl,SQLCHAR,2);
    sqlin ((short *) &a_bas.delstatus,SQLSHORT,0);
    sqlin ((short *) &a_bas.dr_folge,SQLSHORT,0);
    sqlin ((long *) &a_bas.erl_kto,SQLLONG,0);
    sqlin ((double *) &a_bas.gn_pkt_gbr,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.hbk_kz,SQLCHAR,2);
    sqlin ((short *) &a_bas.hbk_ztr,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.hnd_gew,SQLCHAR,2);
    sqlin ((short *) &a_bas.hwg,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.kost_kz,SQLCHAR,3);
    sqlin ((short *) &a_bas.me_einh,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.modif,SQLCHAR,2);
    sqlin ((short *) &a_bas.mwst,SQLSHORT,0);
    sqlin ((short *) &a_bas.plak_div,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.pers_nam,SQLCHAR,9);
    sqlin ((TCHAR *) a_bas.pers_rab_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas.prod_zeit,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.stk_lst_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas.sw,SQLDOUBLE,0);
    sqlin ((short *) &a_bas.teil_smt,SQLSHORT,0);
    sqlin ((long *) &a_bas.we_kto,SQLLONG,0);
    sqlin ((short *) &a_bas.wg,SQLSHORT,0);
    sqlin ((short *) &a_bas.zu_stoff,SQLSHORT,0);
    sqlin ((long *) &a_bas.kost_st,SQLLONG,0);
    sqlin ((short *) &a_bas.hbk_ztr_wa,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.norm_ex,SQLCHAR,2);
    sqlin ((double *) &a_bas.dm_abschl,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.proz_abschl,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.a_tara,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.a_gew_brutto,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.bz_kz,SQLCHAR,2);
    sqlin ((double *) &a_bas.ehg_a,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.a_gew_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.prod_kz,SQLCHAR,3);
    sqlin ((TCHAR *) a_bas.a_bz3,SQLCHAR,41);
    sqlin ((double *) &a_bas.abg_eh,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.plan_me,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.kosten1,SQLDOUBLE,0);
    sqlin ((double *) &a_bas.kosten2,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.kz1,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.kz2,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.pr_kz,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.pr_kz1,SQLCHAR,2);
    sqlin ((TCHAR *) a_bas.pr_kz2,SQLCHAR,3);
    sqlin ((short *) &a_bas.ka_lage,SQLSHORT,0);
    sqlin ((short *) &a_bas.la_pal,SQLSHORT,0);
    sqlin ((short *) &a_bas.up_ka,SQLSHORT,0);
    sqlin ((long *) &a_bas.inh_ek,SQLLONG,0);
    sqlin ((double *) &a_bas.plan_me_koe,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.order_bz,SQLCHAR,25);
    sqlin ((TCHAR *) a_bas.sort_grp,SQLCHAR,11);
    sqlin ((short *) &a_bas.dr_folge_cc,SQLSHORT,0);
    sqlin ((double *) &a_bas.a_ehg,SQLDOUBLE,0);
    sqlin ((TCHAR *) a_bas.sw_pr_kz,SQLCHAR,2);
    sqlin ((long *) &a_bas.kost_tr,SQLLONG,0);
    sqlin ((double *) &a_bas.a_grund,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.kost_st2,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto2,SQLLONG,0);
    sqlin ((long *) &a_bas.charg_hand,SQLLONG,0);
    sqlin ((long *) &a_bas.intra_stat,SQLLONG,0);
    sqlin ((TCHAR *) a_bas.qual_kng,SQLCHAR,5);
    sqlin ((short *) &a_bas.lief_einh,SQLSHORT,0);
    sqlin ((double *) &a_bas.inh_lief,SQLDOUBLE,0);
    sqlin ((long *) &a_bas.erl_kto_1,SQLLONG,0);
    sqlin ((long *) &a_bas.erl_kto_2,SQLLONG,0);
    sqlin ((long *) &a_bas.erl_kto_3,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_1,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_2,SQLLONG,0);
    sqlin ((long *) &a_bas.we_kto_3,SQLLONG,0);
    sqlin ((short *) &a_bas.drk_anz,SQLSHORT,0);
    sqlin ((TCHAR *) a_bas.pnw_lkng,SQLCHAR,13);
    sqlin ((TCHAR *) a_bas.betr_kz,SQLCHAR,6);
            ins_cursor = sqlcursor (_T("insert into a_bas (a,  ")
_T("akv,  bearb,  mdn,  fil,  a_bz1,  a_bz2,  a_gew,  a_typ,  a_typ2,  abt,  ag,  best_auto,  bsd_kz,  ")
_T("cp_aufschl,  delstatus,  dr_folge,  erl_kto,  gn_pkt_gbr,  hbk_kz,  hbk_ztr,  ")
_T("hnd_gew,  hwg,  kost_kz,  me_einh,  modif,  mwst,  plak_div,  pers_nam,  pers_rab_kz,  ")
_T("prod_zeit,  stk_lst_kz,  sw,  teil_smt,  we_kto,  wg,  zu_stoff,  kost_st,  ")
_T("hbk_ztr_wa,  norm_ex,  dm_abschl,  proz_abschl,  a_tara,  a_gew_brutto,  bz_kz,  ")
_T("ehg_a,  a_gew_kz,  prod_kz,  a_bz3,  abg_eh,  plan_me,  kosten1,  kosten2,  kz1,  kz2,  ")
_T("pr_kz,  pr_kz1,  pr_kz2,  ka_lage,  la_pal,  up_ka,  inh_ek,  plan_me_koe,  order_bz,  ")
_T("sort_grp,  dr_folge_cc,  a_ehg,  sw_pr_kz,  kost_tr,  a_grund,  kost_st2,  ")
_T("we_kto2,  charg_hand,  intra_stat,  qual_kng,  lief_einh,  inh_lief,  ")
_T("erl_kto_1,  erl_kto_2,  erl_kto_3,  we_kto_1,  we_kto_2,  we_kto_3,  drk_anz,  ")
_T("pnw_lkng,  betr_kz) ")

#line 25 "a_bas.rpp"
                                      _T("values ")
                                      _T("(?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,")
_T("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")); 

#line 27 "a_bas.rpp"
}
