#ifndef _MDN_DEF
#define _MDN_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct MDN {
   TCHAR          abr_period[2];
   long           adr;
   long           adr_lief;
   DATE_STRUCT    dat_ero;
   short          daten_mnp;
   short          delstatus;
   TCHAR          fil_bel_o_sa[2];
   double         fl_lad;
   double         fl_nto;
   double         fl_vk_ges;
   DATE_STRUCT    iakv;
   TCHAR          inv_rht[2];
   long           kun;
   TCHAR          lief[17];
   TCHAR          lief_rht[2];
   long           lief_s;
   short          mdn;
   TCHAR          mdn_kla[2];
   short          mdn_gr;
   TCHAR          pers[13];
   short          pers_anz;
   TCHAR          pr_bel_entl[2];
   short          pr_lst;
   double         reg_bed_theke_lng;
   double         reg_kt_lng;
   double         reg_kue_lng;
   double         reg_lng;
   double         reg_tks_lng;
   double         reg_tkt_lng;
   TCHAR          smt_kz[2];
   short          sonst_einh;
   TCHAR          sp_kz[2];
   short          sprache;
   TCHAR          sw_kz[2];
   TCHAR          tou[9];
   TCHAR          umlgr[2];
   TCHAR          verk_st_kz[2];
   short          vrs_typ;
   TCHAR          inv_akv[2];
   double         gbr;
   TCHAR          waehr_prim[2];
   TCHAR          waehr_sek[2];
   double         konversion;
   TCHAR          iln[17];
   TCHAR          ust_id[12];
};
extern struct MDN mdn, mdn_null;

#line 8 "mdn.rh"

class MDN_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               MDN mdn;  
               MDN_CLASS () : DB_CLASS ()
               {
               }
};
#endif
