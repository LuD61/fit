#ifndef _A_BAS_DEF
#define _A_BAS_DEF
#include <odbcinst.h>
#include <sqlext.h>
#include "dbclass.h"

struct A_BAS {
   double         a;
   DATE_STRUCT    akv;
   DATE_STRUCT    bearb;
   short          mdn;
   short          fil;
   TCHAR          a_bz1[25];
   TCHAR          a_bz2[25];
   double         a_gew;
   short          a_typ;
   short          a_typ2;
   short          abt;
   short          ag;
   TCHAR          best_auto[2];
   TCHAR          bsd_kz[2];
   TCHAR          cp_aufschl[2];
   short          delstatus;
   short          dr_folge;
   long           erl_kto;
   double         gn_pkt_gbr;
   TCHAR          hbk_kz[2];
   short          hbk_ztr;
   TCHAR          hnd_gew[2];
   short          hwg;
   TCHAR          kost_kz[3];
   short          me_einh;
   TCHAR          modif[2];
   short          mwst;
   short          plak_div;
   TCHAR          pers_nam[9];
   TCHAR          pers_rab_kz[2];
   double         prod_zeit;
   TCHAR          stk_lst_kz[2];
   double         sw;
   short          teil_smt;
   long           we_kto;
   short          wg;
   short          zu_stoff;
   long           kost_st;
   short          hbk_ztr_wa;
   TCHAR          norm_ex[2];
   double         dm_abschl;
   double         proz_abschl;
   double         a_tara;
   double         a_gew_brutto;
   TCHAR          bz_kz[2];
   double         ehg_a;
   TCHAR          a_gew_kz[2];
   TCHAR          prod_kz[3];
   TCHAR          a_bz3[41];
   double         abg_eh;
   double         plan_me;
   double         kosten1;
   double         kosten2;
   TCHAR          kz1[2];
   TCHAR          kz2[2];
   TCHAR          pr_kz[2];
   TCHAR          pr_kz1[2];
   TCHAR          pr_kz2[3];
   short          ka_lage;
   short          la_pal;
   short          up_ka;
   long           inh_ek;
   double         plan_me_koe;
   TCHAR          order_bz[25];
   TCHAR          sort_grp[11];
   short          dr_folge_cc;
   double         a_ehg;
   TCHAR          sw_pr_kz[2];
   long           kost_tr;
   double         a_grund;
   long           kost_st2;
   long           we_kto2;
   long           charg_hand;
   long           intra_stat;
   TCHAR          qual_kng[5];
   short          lief_einh;
   double         inh_lief;
   long           erl_kto_1;
   long           erl_kto_2;
   long           erl_kto_3;
   long           we_kto_1;
   long           we_kto_2;
   long           we_kto_3;
   short          drk_anz;
   TCHAR          pnw_lkng[13];
   TCHAR          betr_kz[6];
};
extern struct A_BAS a_bas, a_bas_null;

#line 8 "a_bas.rh"

class A_BAS_CLASS : public DB_CLASS 
{
       private :
               void prepare (void);
       public :
               A_BAS a_bas;  
               A_BAS_CLASS () : DB_CLASS ()
               {
               }
};
#endif
