#include "StdAfx.h"
#include "prauszlist.h"

CPrAuszList::CPrAuszList(void)
{
	txt_nr = 0;
	eti_typ = 0;
	txt1 = _T("");
	txt2 = _T("");
}

CPrAuszList::CPrAuszList(long txt_nr, short eti_typ,LPTSTR txt1, LPTSTR txt2)
{
	this->txt_nr  = txt_nr;
	this->eti_typ = eti_typ;
	this->txt1    = txt1;
	this->txt2    = txt2;
}

CPrAuszList::~CPrAuszList(void)
{
}
