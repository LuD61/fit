#include "StdAfx.h"
#include "AKunGxList.h"

CAKunGxList::CAKunGxList(void)
{
	mdn = 0;
	kun_bran2 = "";
	kun = 0l;
	a   = 0.0;
	a_bz1 = "";
}

CAKunGxList::CAKunGxList(short mdn, CString& kun_bran2, 
						 long kun, double a, CString& a_bz1)
{
	this->mdn       = mdn;
	this->kun_bran2 = kun_bran2;
	this->kun       = kun;
	this->a         = a;
	this->a_bz1     = a_bz1;
}

CAKunGxList::CAKunGxList(short mdn, LPTSTR kun_bran2, 
				   long kun, double a, LPTSTR a_bz1)
{
	this->mdn       = mdn;
	this->kun_bran2 = kun_bran2;
	this->kun       = kun;
	this->a         = a;
	this->a_bz1     = a_bz1;
}

CAKunGxList::~CAKunGxList(void)
{
}
