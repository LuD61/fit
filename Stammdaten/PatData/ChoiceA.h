#ifndef _CHOICEA_DEF
#define _CHOICEA_DEF

#include "ChoiceX.h"
#include "ABasList.h"
#include <vector>

class CChoiceA : public CChoiceX
{
    private :
        static int CALLBACK CompareProc(LPARAM, LPARAM, LPARAM);
        static int Sort1;
        static int Sort2;
        static int Sort3;
        static int Sort4;
      
    public :
	    std::vector<CABasList *> ABasList;
      	CChoiceA(CWnd* pParent = NULL);   // Standardkonstruktor
      	~CChoiceA(); 
        virtual void BezLabel (CListCtrl *);
        virtual void NumLabel (CListCtrl *);
        virtual void FillList (void);
        void SearchNum (CListCtrl *,  LPTSTR);
        void SearchABz1 (CListCtrl *, LPTSTR);
        void SearchABz2 (CListCtrl *, LPTSTR);
        virtual void Search (void);
        virtual void Sort (CListCtrl *);
	    virtual void SetSelText (CListCtrl *, int);
		CABasList *GetSelectedText ();
        int GetPtBez (LPTSTR, LPTSTR);
		void DestroyList ();
};
#endif
