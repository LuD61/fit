#ifndef _PROG_CFG
#define _PROG_CFG


class PROG_CFG 
         {
          private :
                FILE *progfp;
          public :
			  static _TCHAR cfg_v [];
              _TCHAR progname [80];
			  PROG_CFG () 
			  {
				  progfp = NULL;
				  _tcscpy (progname, _T(""));
			  }

              PROG_CFG (LPTSTR progname) : progfp (NULL)
              {
                       if (progname)
                       {
                               _tcscpy (this->progname, progname);
                       }
              }

			  ~PROG_CFG ();
              void SetProgName (LPTSTR progname)
              {
                       if (progname)
                       {
                               _tcscpy (this->progname, progname);
                       }
              }
              BOOL OpenCfg (void);
              void CloseCfg (void);
              BOOL GetCfgValue (LPTSTR , LPTSTR );
              BOOL ReadCfgItem (LPTSTR , LPTSTR , LPTSTR , LPTSTR *, LPTSTR );
              BOOL ReadCfgValues (LPTSTR *, LPTSTR );
              BOOL ReadCfgHelp (LPTSTR );
              BOOL  GetGlobDefault (LPTSTR , LPTSTR );
              BOOL  GetGroupDefault (LPTSTR , LPTSTR );
			  static void clipped (LPTSTR );
			  static void cr_weg (LPTSTR );
			  static int strupcmp (LPTSTR , LPTSTR , int);
         };
#endif         
                          
                               

