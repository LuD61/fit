#include "StdAfx.h"
#include "gennr.h"
#include "DbUnicode.h"

CGenNr::CGenNr(void)
{
	auto_nr.mdn = 0;
	auto_nr.fil = 0;
	_tcscpy (auto_nr.nr_nam, _T(""));
	cursor1 = -1;
	cursor2 = -1;
	DelCursor = -1;
	min = 1;
	max = 99999999;
}

CGenNr::CGenNr(short mdn, short fil, LPTSTR nr_nam)
{
	auto_nr.mdn = mdn;
	auto_nr.fil = fil;
	_tcscpy (auto_nr.nr_nam, nr_nam);
	LPSTR p = (LPSTR) auto_nr.nr_nam;
	CString pt = auto_nr.nr_nam;
	CDbUniCode::DbFromUniCode (pt.GetBuffer (), p, sizeof (auto_nr.nr_nam) / 2);

	cursor1 = -1;
	cursor2 = -1;
	DelCursor = -1;
	min = 1;
	max = 99999999;
}

CGenNr::~CGenNr(void)
{
	Close ();
}

void CGenNr::Prepare ()
{
    sqlin ((short *)   &auto_nr.mdn, SQLSHORT, 0);
    sqlin ((short *)   &auto_nr.fil, SQLSHORT, 0);
    sqlin ((short *)   &auto_nr.satz_kng, SQLSHORT, 0);
    sqlin ((char *)    auto_nr.nr_nam, SQLCHAR, sizeof (auto_nr.nr_nam));
    sqlout ((long *)    &auto_nr.nr_nr, SQLLONG, 0);
	cursor1 = sqlcursor (_T("select nr_nr from auto_nr ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and satz_kng = ? ")
                                  _T("and nr_nam = ? "));
    sqlin ((short *)   &auto_nr.mdn, SQLSHORT, 0);
    sqlin ((short *)   &auto_nr.fil, SQLSHORT, 0);
    sqlin ((char *)    auto_nr.nr_nam, SQLCHAR, sizeof (auto_nr.nr_nam));
	DelCursor = sqlcursor (_T("delete from auto_nr ")
                                  _T("where mdn = ? ")
                                  _T("and fil = ? ")
                                  _T("and nr_nam = ? "));
}

long CGenNr::NextNr ()
{
	if (cursor1 == -1)
	{
		Prepare ();
	}
	if (cursor1 == -1)
	{
		return 0l;
	}


	auto_nr.satz_kng = 1;
	sqlopen (cursor1);
	int dsqlstatus = sqlfetch (cursor1);
	if (dsqlstatus == 0)
	{
		dbreadfirst ();
		dbdelete ();
		return auto_nr.nr_nr;
	}
	auto_nr.satz_kng = 2;
	sqlopen (cursor1);
	dsqlstatus = sqlfetch (cursor1);
    if (dsqlstatus == 0)
	{
		dbreadfirst ();
	}
	else
	{
		Create ();
		sqlopen (cursor1);
		dsqlstatus = sqlfetch (cursor1);
		if (dsqlstatus == 0)
		{
			return 0l;
		}
		dbreadfirst ();
	}
    long nr_nr = auto_nr.nr_nr;
	dbdelete ();
 	auto_nr.nr_nr ++;
	dbupdate ();
	return nr_nr;
}

void CGenNr::FreeNr (long nr)
{
	auto_nr.nr_nr = nr;
	auto_nr.satz_kng = 1;
	if (dbreadfirst () == 0)
	{
		return;
	}
	auto_nr.satz_kng = 2;
	if (dbreadfirst () == 0)
	{
		return;
	}

	auto_nr.satz_kng = 1;
	dbupdate ();
}

BOOL CGenNr::Create ()
/**
Neu Nummer generieren.
**/
{
	if (DelCursor == -1)
	{
		Prepare ();
	}
	if (DelCursor == -1)
	{
		return FALSE;
	}
	sqlexecute (DelCursor);
    auto_nr.nr_nr    = min;             
    auto_nr.max_wert = max;             
    auto_nr.nr_char_lng  = max;             
    sprintf ((LPSTR)auto_nr.nr_char, "%ld", max);
    strcpy ((LPSTR)auto_nr.fest_teil, " ");
    strcpy ((LPSTR)auto_nr.nr_komb, " ");
    auto_nr.delstatus = 0;
    auto_nr.satz_kng = 2;
    int dsqlstatus = dbupdate ();
	if (dsqlstatus == 0)
	{
		return TRUE;
	}
    return FALSE;
}

void CGenNr::Close ()
{
	if (cursor1 != -1)
	{
		sqlclose (cursor1);
		cursor1 = -1;
	}
	if (cursor2 != -1)
	{
		sqlclose (cursor2);
		cursor2 = -1;
	}
	if (DelCursor != -1)
	{
		sqlclose (DelCursor);
		DelCursor = -1;
	}
	dbclose ();
}
