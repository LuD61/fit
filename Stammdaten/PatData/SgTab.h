#pragma once
#include "vector.h"
#include "Sg.h"

class CSgTab :
	public CVector
{
public:
	CSgTab(void);
	~CSgTab(void);
    CSg *GetNext ();
    CSg *Get (int);
	CSg *Get (short);
};
