#include "StdAfx.h"
#include "flatbutton.h"

CFlatButton::CFlatButton(void)
{
}

CFlatButton::~CFlatButton(void)
{
}

void CFlatButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CRect rect;
	GetClientRect (&rect);
	int cx = image.GetWidth ();
	int cy = image.GetHeight ();
	int x = max (0,(rect.right - cx) / 2);
	int y = max (0,(rect.bottom - cy) / 2);
	image.Draw (lpDrawItemStruct->hDC,x, y);
}

void CFlatButton::SetBitmap (HBITMAP hBitmap)
{
	image.Attach (hBitmap);
//	CButtom::SetBitmap (hBitmap);
}
